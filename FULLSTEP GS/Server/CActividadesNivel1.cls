VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CActividadesNivel1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CActividadesNivel1 **********************************
'*             Autor : Javier Arana
'*             Creada : 18/11/98
'****************************************************************

Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum

Private mCol As Collection
Private mvarConexion As CConexion
Private mvarEOF As Boolean

''' <summary>Carga todas las actividades</summary>
''' <param name="CaracteresInicialesCod">Caracteres iniciales de cod. para b�squeda</param>
''' <param name="CaracteresInicialesDen">Caracteres iniciales de den. para b�squeda</param>
''' <param name="TipoBusqueda">Tipo de b�squeda</param>
''' <param name="OrdenadosPorDen">Orden por denominaci�n</param>
''' <param name="UsarIndice">Indica si se usa �ndice</param>
''' <remarks>Llamada desde: FSGClient Tiempo m�ximo: 1 sec </remarks>
''' <revision>LTG 22/12/2011</revision>

Public Sub CargarTodasLasActividades(Optional ByVal CaracteresInicialesCod As String, Optional ByVal CaracteresInicialesDen As String, _
        Optional ByVal TipoBusqueda As Integer, Optional ByVal OrdenadosPorDen As Boolean, Optional ByVal UsarIndice As Boolean)
    Dim rs As New adodb.Recordset
    Dim fldId As adodb.Field
    Dim fldCod As adodb.Field
    Dim fldDen As adodb.Field
    Dim sConsulta As String
    Dim lIndice As Long
    Dim sDen As String
    Dim sFSP As String
    
    '********* Precondicion **************************************
    If mvarConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CDestinos.CargarTodosLosDestinos", "No se ha establecido la conexion"
        Exit Sub
    End If
    '*************************************************************
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sFSP = basParametros.gParametrosGenerales.gsFSP_SRV & "." & basParametros.gParametrosGenerales.gsFSP_BD & ".dbo."
        
    'CALIDAD: No se usa WITH (NOLOCK) porque es una consulta a otra BD
    sConsulta = "SELECT ID,COD," & sDen & " FROM " & sFSP & "ACT1 AS ACT1"
    
    If CaracteresInicialesCod = "" And CaracteresInicialesDen = "" Then
     
    Else
        If Not (CaracteresInicialesCod = "") And Not (CaracteresInicialesDen = "") Then
            Select Case TipoBusqueda
                Case -1
                    sConsulta = sConsulta & " WHERE COD ='" & basUtilidades.DblQuote(CaracteresInicialesCod) & "'"
                    sConsulta = sConsulta & " AND " & sDen & "='" & basUtilidades.DblQuote(CaracteresInicialesDen) & "'"
                Case 0
                    sConsulta = sConsulta & " WHERE COD LIKE '" & DblQuote(CaracteresInicialesCod) & "%'"
                    sConsulta = sConsulta & " AND DEN LIKE '" & DblQuote(CaracteresInicialesDen) & "%'"
                Case 1
                    sConsulta = sConsulta & " WHERE COD >= '" & DblQuote(CaracteresInicialesCod) & "'"
                    sConsulta = sConsulta & " AND " & sDen & ">= '" & DblQuote(CaracteresInicialesDen) & "'"
            End Select
        Else
            If Not (CaracteresInicialesCod = "") Then
                Select Case TipoBusqueda
                    Case -1
                        sConsulta = sConsulta & " WHERE COD ='" & DblQuote(CaracteresInicialesCod) & "'"
                    Case 0
                       sConsulta = sConsulta & " WHERE COD LIKE '" & DblQuote(CaracteresInicialesCod) & "%'"
                    Case 1
                       sConsulta = sConsulta & " WHERE COD >= '" & DblQuote(CaracteresInicialesCod) & "'"
                End Select
            Else
                Select Case TipoBusqueda
                    Case -1
                        sConsulta = sConsulta & " WHERE " & sDen & "='" & DblQuote(CaracteresInicialesDen) & "'"
                    Case 0
                        sConsulta = sConsulta & " WHERE DEN LIKE '" & DblQuote(CaracteresInicialesDen) & "%'"
                    Case 1
                        sConsulta = sConsulta & " WHERE " & sDen & ">= '" & DblQuote(CaracteresInicialesDen) & "'"
                End Select
            End If
        End If
    End If
    
    If OrdenadosPorDen Then
        sConsulta = sConsulta & " ORDER BY DEN,COD"
    Else
        sConsulta = sConsulta & " ORDER BY COD,DEN"
    End If
          
    rs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
                  
    If Not rs.eof Then
        Set fldId = rs.Fields(0)
        Set fldCod = rs.Fields(1)
        Set fldDen = rs.Fields(2)
    
        Set mCol = Nothing
        Set mCol = New Collection
            
        If UsarIndice Then
            
            lIndice = 0
            
            While Not rs.eof
                ' Dejamos en manos del programador cuando asignar la conexion al obejto usuario
                Me.Add fldId.Value, fldCod.Value, fldDen.Value, lIndice
                rs.MoveNext
                lIndice = lIndice + 1
            Wend
            
        Else
            
            While Not rs.eof
                ' Dejamos en manos del programador cuando asignar la conexion al obejto usuario
                Me.Add fldId.Value, fldCod.Value, fldDen.Value
                rs.MoveNext
            Wend
        End If
        
        Set fldId = Nothing
        Set fldCod = Nothing
        Set fldDen = Nothing
    End If
    
    rs.Close
    Set rs = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CActividadesNivel1", "CargarTodasLasActividades", ERR, Erl)
        Exit Sub
    End If
End Sub

''' <summary>Carga de actividades</summary>
''' <param name="iNumMaximo">N� m�x de registros a devolver</param>
''' <param name="CaracteresInicialesCod">Caracteres iniciales de cod. para b�squeda</param>
''' <param name="CaracteresInicialesDen">Caracteres iniciales de den. para b�squeda</param>
''' <param name="OrdenadosPorDen">Orden por denominaci�n</param>
''' <param name="UsarIndice">Indixca si se usa �ndice</param>
''' <remarks>Llamada desde: FSGClient Tiempo m�ximo: 1 sec </remarks>
''' <revision>LTG 22/12/2011</revision>

Public Sub CargarTodasLasActividadesDesde(ByVal iNumMaximo As Integer, Optional ByVal CaracteresInicialesCod As String, _
        Optional ByVal CaracteresInicialesDen As String, Optional ByVal OrdenadosPorDen As Boolean, Optional ByVal UsarIndice As Boolean)
    Dim rs As New adodb.Recordset
    Dim fldId As adodb.Field
    Dim fldCod As adodb.Field
    Dim fldDen As adodb.Field
    Dim sConsulta As String
    Dim lIndice As Long
    '''''''''''''Dim iNumGrups As Integer
    Dim sDen As String
    Dim sFSP As String
    
    '********* Precondicion **************************************
    If mvarConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CDestinos.CargarTodosLosDestinos", "No se ha establecido la conexion"
        Exit Sub
    End If
    '*************************************************************
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
        
    sFSP = basParametros.gParametrosGenerales.gsFSP_SRV & "." & basParametros.gParametrosGenerales.gsFSP_BD & ".dbo."
    sDen = "DEN_" & basParametros.gParametrosGenerales.gIdioma
    
    'CALIDAD: No se usa WITH (NOLOCK) porque es una consulta a otra BD
    sConsulta = "SELECT TOP " & iNumMaximo & " ID,COD," & sDen & " FROM " & sFSP & "ACT1 AS ACT1 "
    
    If CaracteresInicialesCod = "" And CaracteresInicialesDen = "" Then
               
    Else
        If Not (CaracteresInicialesCod = "") And Not (CaracteresInicialesDen = "") Then
            sConsulta = sConsulta & " WHERE COD >='" & DblQuote(CaracteresInicialesCod) & "'"
            sConsulta = sConsulta & " AND " & sDen & ">='" & DblQuote(CaracteresInicialesDen) & "'"
        Else
            If Not (CaracteresInicialesCod = "") Then
                sConsulta = sConsulta & " WHERE COD >='" & DblQuote(CaracteresInicialesCod) & "'"
            Else
                sConsulta = sConsulta & " WHERE " & sDen & ">='" & DblQuote(CaracteresInicialesDen) & "'"
            End If
        End If
    End If
    
    If OrdenadosPorDen Then
        sConsulta = sConsulta & " ORDER BY DEN,COD"
    Else
        sConsulta = sConsulta & " ORDER BY COD,DEN"
    End If
          
    rs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
        
    Set mCol = Nothing
    Set mCol = New Collection
        
    mvarEOF = rs.eof
    
    If Not rs.eof Then
        Set fldId = rs.Fields(0)
        Set fldCod = rs.Fields(1)
        Set fldDen = rs.Fields(2)
            
        '''''''''''''''iNumGrups = 0
        
        If UsarIndice Then
            lIndice = 0
            
            While Not rs.eof ''''''''''''''''And iNumGrups < iNumMaximo
                ' Dejamos en manos del programador cuando asignar la conexion al obejto usuario
                Me.Add fldId.Value, fldCod.Value, fldDen.Value, lIndice
                rs.MoveNext
                lIndice = lIndice + 1
                '''''''''''''''''''''''iNumGrups = iNumGrups + 1
            Wend
        Else
            While Not rs.eof '''''''''''''''''''''''''And iNumGrups < iNumMaximo
                ' Dejamos en manos del programador cuando asignar la conexion al obejto usuario
                Me.Add fldId.Value, fldCod.Value, fldDen.Value
                rs.MoveNext
                '''''''''''''''''''''''''iNumGrups = iNumGrups + 1
            Wend
        End If
            
        Set fldId = Nothing
        Set fldCod = Nothing
        Set fldDen = Nothing
    End If
    
    rs.Close
    Set rs = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CActividadesNivel1", "CargarTodasLasActividadesDesde", ERR, Erl)
        Exit Sub
    End If
End Sub

''' <summary>Genera la estructura de grupos de material</summary>
''' <param name="OrdPorDen">Orden</param>
''' <param name="Idioma">Idioma</param>
''' <param name="sOrigen">Origen</param>
''' <param name="sDenBuscar">Denominaci�n</param>
''' <remarks>Llamada desde: FSGClient Tiempo m�ximo: 1 sec </remarks>
''' <revision>LTG 22/12/2011</revision>

Public Sub GenerarEstructuraActividades(ByVal OrdPorDen As Boolean, Optional ByVal Idioma As String, Optional ByVal sOrigen As String, _
        Optional ByVal sDenBuscar As String)
    Dim oACN1 As CActividadNivel1
    Dim oACN2 As CActividadNivel2
    Dim oACN3 As CActividadNivel3
    Dim oACN4 As CActividadNivel4
    Dim oACN5 As CActividadNivel5
    Dim sConsulta As String
    Dim rs As New adodb.Recordset
    Dim fldId As adodb.Field
    Dim fldCod As adodb.Field
    Dim fldDen As adodb.Field
    Dim fldCount As adodb.Field
    Dim fldAct1 As adodb.Field
    Dim fldAct2 As adodb.Field
    Dim fldAct3 As adodb.Field
    Dim fldAct4 As adodb.Field
    Dim sCod As String
    Dim sDen As String
    Dim sFSP As String
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set mCol = Nothing
    Set mCol = New Collection
        
    sFSP = basParametros.gParametrosGenerales.gsFSP_SRV & "." & basParametros.gParametrosGenerales.gsFSP_BD & ".dbo."
    
    ' Generamos la coleccion de grupos ACT nivel1
    If Idioma <> "" Then
       sDen = "DEN_" & Idioma
    Else
      sDen = "DEN_" & basParametros.gParametrosGenerales.gIdiomaPortal
    End If
    
    sConsulta = "SELECT ID,COD," & sDen
    'CALIDAD: No se usa WITH (NOLOCK) porque es una consulta a otra BD
    sConsulta = sConsulta & ",COUNT(CIAS_ACT1.CIA) AS PROVE  FROM " & sFSP & "ACT1 AS ACT1"
    sConsulta = sConsulta & " LEFT JOIN " & sFSP & "CIAS_ACT1 AS CIAS_ACT1 ON CIAS_ACT1.ACT1=ACT1.ID"
    
    sConsulta = sConsulta & " GROUP BY ID, COD, " & sDen
    
    If OrdPorDen Then
       sConsulta = sConsulta & " ORDER BY " & sDen
    Else
       sConsulta = sConsulta & " ORDER BY COD"
    End If
    
    rs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    
    If rs.eof Then
       rs.Close
       Set rs = Nothing
       Exit Sub
    End If
    
    Set fldId = rs.Fields(0)
    Set fldCod = rs.Fields(1)
    Set fldDen = rs.Fields(2)
    Set fldCount = rs.Fields(3)
    
    While Not rs.eof
       Set oACN1 = Me.Add(fldId.Value, fldCod.Value, fldDen.Value, , fldCount.Value)
       Set oACN1.ActividadesNivel2 = New CActividadesNivel2
       Set oACN1.ActividadesNivel2.Conexion = mvarConexion
       
       rs.MoveNext
       Set oACN1 = Nothing
    Wend
    
    rs.Close
           
    ' Generamos la coleccion de grupos act2 nivel2
    
    sConsulta = "SELECT ACT2.ACT1,ACT2.ID,ACT2.COD,ACT2." & sDen
    'CALIDAD: No se usa WITH (NOLOCK) porque es una consulta a otra BD
    sConsulta = sConsulta & ",COUNT(CIAS_ACT2.CIA) AS PROVE  FROM " & sFSP & "ACT2 AS ACT2"
    sConsulta = sConsulta & " LEFT JOIN " & sFSP & "CIAS_ACT2 AS CIAS_ACT2 ON CIAS_ACT2.ACT2=ACT2.ID AND CIAS_ACT2.ACT1=ACT2.ACT1"
    sConsulta = sConsulta & " GROUP BY ACT2.ACT1,ACT2.ID,ACT2.COD,ACT2." & sDen
    
    If OrdPorDen Then
       sConsulta = sConsulta & " ORDER BY ACT2." & sDen
    Else
       sConsulta = sConsulta & " ORDER BY ACT2.COD"
    End If
    
    Set rs = New adodb.Recordset
    rs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    
           
    If rs.eof Then
       rs.Close
       Set rs = Nothing
       Exit Sub
    End If
    
    Set fldAct1 = rs.Fields(0)
    Set fldId = rs.Fields(1)
    Set fldCod = rs.Fields(2)
    Set fldDen = rs.Fields(3)
    Set fldCount = rs.Fields(4)
    
    While Not rs.eof
       sCod = CStr(fldAct1.Value) & Mid$("                         ", 1, 10 - Len(CStr(fldAct1.Value)))
       Set oACN1 = Me.Item(sCod)
       Set oACN2 = oACN1.ActividadesNivel2.Add(CStr(fldAct1.Value), fldId.Value, fldCod.Value, fldDen.Value, , , , fldCount.Value)
       ' Creo una nueva coleccion para cada uno
       Set oACN2.ActividadesNivel3 = New CActividadesNivel3
       Set oACN2.ActividadesNivel3.Conexion = mvarConexion
       
       rs.MoveNext
       Set oACN1 = Nothing
       Set oACN2 = Nothing
       
    Wend
    rs.Close
                  
    ' Generamos la coleccion de grupos mat nivel3
    
    sConsulta = "SELECT ACT3.ACT1,ACT3.ACT2,ACT3.ID,ACT3.COD,ACT3." & sDen
    'CALIDAD: No se usa WITH (NOLOCK) porque es una consulta a otra BD
    sConsulta = sConsulta & ",COUNT(CIAS_ACT3.CIA) AS PROVE  FROM " & sFSP & "ACT3 AS ACT3"
    sConsulta = sConsulta & " LEFT JOIN " & sFSP & "CIAS_ACT3 AS CIAS_ACT3 ON CIAS_ACT3.ACT3=ACT3.ID AND CIAS_ACT3.ACT2=ACT3.ACT2 AND CIAS_ACT3.ACT1=ACT3.ACT1"
    sConsulta = sConsulta & " GROUP BY ACT3.ACT1,ACT3.ACT2,ACT3.ID,ACT3.COD,ACT3." & sDen
  
    If OrdPorDen Then
       sConsulta = sConsulta & " ORDER BY ACT3." & sDen
    Else
       sConsulta = sConsulta & " ORDER BY ACT3.COD"
    End If
    
    Set rs = New adodb.Recordset
    rs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    
           
    If rs.eof Then
       Set rs = Nothing
       Exit Sub
    End If
    
    Set fldAct1 = rs.Fields(0)
    Set fldAct2 = rs.Fields(1)
    Set fldId = rs.Fields(2)
    Set fldCod = rs.Fields(3)
    Set fldDen = rs.Fields(4)
    Set fldCount = rs.Fields(5)
    
    While Not rs.eof
       sCod = CStr(fldAct1.Value) & Mid$("                         ", 1, 10 - Len(CStr(fldAct1.Value)))
       Set oACN1 = Me.Item(sCod)
       sCod = sCod & CStr(fldAct2.Value) & Mid$("                         ", 1, 10 - Len(CStr(fldAct2.Value)))
       Set oACN2 = oACN1.ActividadesNivel2.Item(sCod)
       Set oACN3 = oACN2.ActividadesNivel3.Add(fldAct1.Value, fldAct2.Value, fldId.Value, fldCod.Value, fldDen.Value, , , , , , fldCount.Value)
       ' Creo una nueva coleccion para cada uno
       Set oACN3.ActividadesNivel4 = New CActividadesNivel4
       Set oACN3.ActividadesNivel4.Conexion = mvarConexion
       
       rs.MoveNext
       Set oACN1 = Nothing
       Set oACN2 = Nothing
       Set oACN3 = Nothing
    Wend
    
    rs.Close
    Set rs = Nothing
    
    ' Generamos la coleccion de grupos mat nivel4
    
    sConsulta = "SELECT ACT4.ACT1,ACT4.ACT2,ACT4.ACT3,ACT4.ID,ACT4.COD,ACT4." & sDen
    'CALIDAD: No se usa WITH (NOLOCK) porque es una consulta a otra BD
    sConsulta = sConsulta & ",COUNT(CIAS_ACT4.CIA) AS PROVE  FROM " & sFSP & "ACT4 AS ACT4"
    sConsulta = sConsulta & " LEFT JOIN " & sFSP & "CIAS_ACT4 AS CIAS_ACT4 ON CIAS_ACT4.ACT4=ACT4.ID AND CIAS_ACT4.ACT3=ACT4.ACT3 AND CIAS_ACT4.ACT2=ACT4.ACT2 AND CIAS_ACT4.ACT1=ACT4.ACT1"
    sConsulta = sConsulta & " GROUP BY ACT4.ACT1,ACT4.ACT2,ACT4.ACT3,ACT4.ID,ACT4.COD,ACT4." & sDen
    
    If OrdPorDen Then
       sConsulta = sConsulta & " ORDER BY ACT4." & sDen
    Else
       sConsulta = sConsulta & " ORDER BY ACT4.COD"
    End If
    
    Set rs = New adodb.Recordset
    rs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    
    If rs.eof Then
       rs.Close
       Set rs = Nothing
       Exit Sub
    End If
    
    Set fldAct1 = rs.Fields(0)
    Set fldAct2 = rs.Fields(1)
    Set fldAct3 = rs.Fields(2)
    Set fldId = rs.Fields(3)
    Set fldCod = rs.Fields(4)
    Set fldDen = rs.Fields(5)
    Set fldCount = rs.Fields(6)
    
    While Not rs.eof
       sCod = CStr(fldAct1.Value) & Mid$("                         ", 1, 10 - Len(CStr(fldAct1.Value)))
       Set oACN1 = Me.Item(sCod)
       sCod = sCod & CStr(fldAct2.Value) & Mid$("                         ", 1, 10 - Len(CStr(fldAct2.Value)))
       Set oACN2 = oACN1.ActividadesNivel2.Item(sCod)
       sCod = sCod & CStr(fldAct3.Value) & Mid$("                         ", 1, 10 - Len(CStr(fldAct3.Value)))
       Set oACN3 = oACN2.ActividadesNivel3.Item(sCod)
       Set oACN4 = oACN3.ActividadesNivel4.Add(fldAct1.Value, fldAct2.Value, fldAct3.Value, fldId.Value, fldCod.Value, fldDen.Value, , , , , , , , fldCount.Value)
       ' Creo una nueva coleccion para cada uno
       Set oACN4.ActividadesNivel5 = New CActividadesNivel5
       Set oACN4.ActividadesNivel5.Conexion = mvarConexion
       
       rs.MoveNext
       Set oACN1 = Nothing
       Set oACN2 = Nothing
       Set oACN3 = Nothing
    Wend
      
    rs.Close
    Set rs = Nothing
    
    ' Generamos la coleccion de grupos mat nivel5
    
    sConsulta = "SELECT ACT5.ACT1,ACT5.ACT2,ACT5.ACT3,ACT5.ACT4,ACT5.ID,ACT5.COD,ACT5." & sDen
    'CALIDAD: No se usa WITH (NOLOCK) porque es una consulta a otra BD
    sConsulta = sConsulta & ",COUNT(CIAS_ACT5.CIA) AS PROVE  FROM " & sFSP & "ACT5 AS ACT5"
    sConsulta = sConsulta & " LEFT JOIN " & sFSP & "CIAS_ACT5 AS CIAS_ACT5 ON CIAS_ACT5.ACT5 = ACT5.ID AND CIAS_ACT5.ACT4=ACT5.ACT4 AND CIAS_ACT5.ACT3=ACT5.ACT3 AND CIAS_ACT5.ACT2=ACT5.ACT2 AND CIAS_ACT5.ACT1=ACT5.ACT1"
    If sOrigen = "frmAdmProvePortal" Then
       sConsulta = sConsulta & " inner join " & sFSP & "ACT1 AS ACT1  ON ACT1.ID = ACT5.ACT1 "
       sConsulta = sConsulta & " inner join " & sFSP & "ACT2 AS ACT2  ON ACT2.ID = ACT5.ACT2 AND ACT2.ACT1 = ACT5.ACT1 "
       sConsulta = sConsulta & " inner join " & sFSP & "ACT3 AS ACT3  ON ACT3.ID = ACT5.ACT3 AND ACT3.ACT2 = ACT5.ACT2 AND ACT3.ACT1 = ACT5.ACT1 "
       sConsulta = sConsulta & " inner join " & sFSP & "ACT4 AS ACT4  ON ACT4.ID = ACT5.ACT4 AND ACT4.ACT3 = ACT5.ACT3 AND ACT4.ACT2 = ACT5.ACT2 AND ACT4.ACT1 = ACT5.ACT1 "
       sConsulta = sConsulta & " WHERE ACT5." & sDen & " LIKE '" & sDenBuscar & "%'"
    End If
    
    sConsulta = sConsulta & " GROUP BY ACT5.ACT1,ACT5.ACT2,ACT5.ACT3,ACT5.ACT4,ACT5.ID,ACT5.COD,ACT5." & sDen
    
    If OrdPorDen Then
       sConsulta = sConsulta & " ORDER BY ACT5." & sDen
    Else
       sConsulta = sConsulta & " ORDER BY ACT5.COD"
    End If
    
    Set rs = New adodb.Recordset
    rs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
       
           
    If rs.eof Then
       rs.Close
       Set rs = Nothing
       Exit Sub
    End If
    
    Set fldAct1 = rs.Fields(0)
    Set fldAct2 = rs.Fields(1)
    Set fldAct3 = rs.Fields(2)
    Set fldAct4 = rs.Fields(3)
    Set fldId = rs.Fields(4)
    Set fldCod = rs.Fields(5)
    Set fldDen = rs.Fields(6)
    Set fldCount = rs.Fields(7)
    
    While Not rs.eof
       sCod = CStr(fldAct1.Value) & Mid$("                         ", 1, 10 - Len(CStr(fldAct1.Value)))
       Set oACN1 = Me.Item(sCod)
       sCod = sCod & CStr(fldAct2.Value) & Mid$("                         ", 1, 10 - Len(CStr(fldAct2.Value)))
       Set oACN2 = oACN1.ActividadesNivel2.Item(sCod)
       sCod = sCod & CStr(fldAct3.Value) & Mid$("                         ", 1, 10 - Len(CStr(fldAct3.Value)))
       Set oACN3 = oACN2.ActividadesNivel3.Item(sCod)
       sCod = sCod & CStr(fldAct4.Value) & Mid$("                         ", 1, 10 - Len(CStr(fldAct4.Value)))
       Set oACN4 = oACN3.ActividadesNivel4.Item(sCod)
       oACN4.ActividadesNivel5.Add fldAct1.Value, fldAct2.Value, fldAct3.Value, fldAct4.Value, fldId.Value, fldCod.Value, fldDen.Value, , , , , , , , , , fldCount.Value
       rs.MoveNext
       Set oACN1 = Nothing
       Set oACN2 = Nothing
       Set oACN3 = Nothing
       Set oACN4 = Nothing
       
    Wend
      
    Set fldAct1 = Nothing
    Set fldAct2 = Nothing
    Set fldAct3 = Nothing
    Set fldAct4 = Nothing
    Set fldId = Nothing
    Set fldCod = Nothing
    Set fldDen = Nothing
    Set fldCount = Nothing
    rs.Close
    Set rs = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CActividadesNivel1", "GenerarEstructuraActividades", ERR, Erl)
        Exit Sub
    End If

End Sub

Public Function Add(ByVal Id As Long, ByVal Cod As String, ByVal Den As String, Optional ByVal varIndice As Variant, Optional ByVal numProve As Integer) As CActividadNivel1
    'create a new object
    Dim objnewmember As CActividadNivel1
    Dim sCod As String
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New CActividadNivel1
   
    With objnewmember
        .Cod = Cod
        .Den = Den
        .Id = Id
        .numProve = numProve
    End With
    
    Set objnewmember.Conexion = mvarConexion
    
    If Not IsMissing(varIndice) And Not IsNull(varIndice) Then
        objnewmember.Indice = varIndice
        mCol.Add objnewmember, CStr(varIndice)
    Else
        sCod = CStr(Id) & Mid$("                         ", 1, 10 - Len(CStr(Id)))
        mCol.Add objnewmember, sCod
    End If
      
    Set Add = objnewmember
    Set objnewmember = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CActividadesNivel1", "Add", ERR, Erl)
        Exit Function
    End If

End Function
Public Function BusquedaActividades(ByVal OrdPorDen As Boolean, Optional ByVal Idioma As String, Optional ByVal sDenBuscar As String) As adodb.Recordset
'   Llama a la store para que este devuelva en un recorset desconectado todas las actividades que comience
'   su denominacion por los caracteres que llegan en el parametro de entrada sDenBuscar

Dim adocom As adodb.Command
Dim ador As adodb.Recordset
Dim oParam As adodb.Parameter
Dim sFSP As String
Dim sDen As String


If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sFSP = basParametros.gParametrosGenerales.gsFSP_SRV & "." & basParametros.gParametrosGenerales.gsFSP_BD & ".dbo."
    
    If Idioma <> "" Then
       sDen = "DEN_" & Idioma
    Else
      sDen = "DEN_" & basParametros.gParametrosGenerales.gIdiomaPortal
    End If

''' Preparar la SP y sus parametros
    
    Set adocom = New adodb.Command
    Set adocom.ActiveConnection = mvarConexion.ADOCon
    
    adocom.CommandType = adCmdStoredProc
    adocom.CommandText = sFSP & "SP_BUSCAR_ACTIVIDADES_PROVE_PORTAL"
    
    Set oParam = adocom.CreateParameter("SDEN", adVarChar, adParamInput, 15, sDen)
    adocom.Parameters.Append oParam
    Set oParam = adocom.CreateParameter("SBUSCAR", adVarChar, adParamInput, 50, DblQuote(sDenBuscar))
    adocom.Parameters.Append oParam
    Set oParam = adocom.CreateParameter("SCOMP", adInteger, adParamInput, , basParametros.gParametrosGenerales.giFSP_CIA)
    adocom.Parameters.Append oParam
    
    
    ''' Ejecutar la SP
    
    Set ador = adocom.Execute
    Set adocom.ActiveConnection = Nothing
    Set adocom = Nothing
    
    Set BusquedaActividades = ador
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CActividadesNivel1", "BusquedaActividades", ERR, Erl)
        Exit Function
    End If

End Function
Public Property Get Item(vntIndexKey As Variant) As CActividadNivel1
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Friend Property Set Conexion(ByVal con As CConexion)
Set mvarConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = mvarConexion
End Property

''' <summary>Carga en la coleccion las actividades de Nivel1</summary>
''' <param name="Prove">el codigo de un proveedor</param>
''' <param name="Idioma">el Idioma en el que se devolvera la denominacion</param>
''' <param name="OrdPorDen">si es True se ordena por Denominacion sino por Cod.</param>
''' <param name="UsarIndice">si es True la coleccion se cargara con un indice correlativo, sino el indice sera el ID de la actividad</param>
''' <remarks>Llamada desde: FSGClient Tiempo m�ximo: 1 sec </remarks>
''' <revision>LTG 22/12/2011</revision>

Public Sub CargarActividadesN1Proveedor(ByVal Prove As Long, Optional ByVal Idioma As String, Optional OrdPorDen As Boolean, Optional ByVal UsarIndice As Boolean)
    Dim rs As New adodb.Recordset
    Dim fldAct1Id As adodb.Field
    Dim fldAct1Cod As adodb.Field
    Dim fldAct1sDen As adodb.Field
    Dim lIndice As Long
    Dim sDen As String
    Dim sFSP As String
    Dim sConsulta As String
        
    '********* Precondicion **************************************
    If mvarConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CDestinos.CargarTodosLosDestinos", "No se ha establecido la conexion"
        Exit Sub
    End If
    '*************************************************************
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

    If Idioma = "" Then
        sDen = "DEN_" & basParametros.gParametrosGenerales.gIdiomaPortal
    Else
        sDen = "DEN_" & Idioma
    End If
    
    sFSP = basParametros.gParametrosGenerales.gsFSP_SRV & "." & basParametros.gParametrosGenerales.gsFSP_BD & ".dbo."
 
    sConsulta = "SELECT ACT1.ID, ACT1.COD AS Cod1,ACT1." & sDen & " AS sDen1"
    'CALIDAD: No se usa WITH (NOLOCK) porque es una consulta a otra BD
    sConsulta = sConsulta & " FROM " & sFSP & "CIAS_ACT1 AS CIAS_ACT1"
    sConsulta = sConsulta & " INNER JOIN " & sFSP & "ACT1 AS ACT1 ON"
    sConsulta = sConsulta & " ACT1.ID = CIAS_ACT1.ACT1"
    sConsulta = sConsulta & " WHERE CIAS_ACT1.CIA=" & Prove & " "
        
    If OrdPorDen Then
        sConsulta = sConsulta & "ORDER BY sDen1"
    Else
        sConsulta = sConsulta & "ORDER BY Cod1"
    End If
        
    rs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    
    If Not rs.eof Then
        Set fldAct1Id = rs.Fields(0)
        Set fldAct1Cod = rs.Fields(1)
        Set fldAct1sDen = rs.Fields(2)
                         
        Set mCol = Nothing
        Set mCol = New Collection
        
        If UsarIndice Then
            lIndice = 0
          
            While Not rs.eof
                Me.Add fldAct1Id.Value, fldAct1Cod.Value, fldAct1sDen.Value, lIndice
                rs.MoveNext
                lIndice = lIndice + 1
            Wend
        Else
            While Not rs.eof
                Me.Add fldAct1Id.Value, fldAct1Cod.Value, fldAct1sDen.Value
                rs.MoveNext
            Wend
        End If
           
        Set fldAct1Id = Nothing
        Set fldAct1Cod = Nothing
        Set fldAct1sDen = Nothing
    End If
        
    rs.Close
    Set rs = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CActividadesNivel1", "CargarActividadesN1Proveedor", ERR, Erl)
        Exit Sub
    End If
End Sub

Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
     Count = mCol.Count
End If


End Property


Public Sub Remove(vntIndexKey As Variant)
    mCol.Remove vntIndexKey
End Sub


Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
     Set NewEnum = mCol.[_NewEnum]
End Property


Private Sub Class_Initialize()

        Set mCol = New Collection
End Sub


Private Sub Class_Terminate()
    'destroys collection when this class is terminated
    Set mCol = Nothing
    Set mvarConexion = Nothing
End Sub

Public Function DevolverLosCodigos() As TipoDatosCombo
   
   Dim Codigos As TipoDatosCombo
   Dim iCont As Integer
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
   ReDim Codigos.Cod(mCol.Count)
   ReDim Codigos.Den(mCol.Count)
   
    For iCont = 0 To mCol.Count - 1
        
        Codigos.Cod(iCont) = mCol(iCont + 1).Cod
        Codigos.Den(iCont) = mCol(iCont + 1).Den
    
    Next

   DevolverLosCodigos = Codigos
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CActividadesNivel1", "DevolverLosCodigos", ERR, Erl)
        Exit Function
    End If
   
End Function
Public Property Get eof() As Boolean
    eof = mvarEOF
End Property
Friend Property Let eof(ByVal b As Boolean)
    mvarEOF = b
End Property

