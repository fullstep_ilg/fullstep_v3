VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CReglasSubasta"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private mCol As Collection
Private mvarEOF As Boolean
Private mvarConexion As CConexion

Friend Property Set Conexion(ByVal con As CConexion)
Set mvarConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = mvarConexion
End Property

Public Property Get eof() As Boolean
    eof = mvarEOF
End Property
Friend Property Let eof(ByVal b As Boolean)
    mvarEOF = b
End Property

Public Property Get Item(vntIndexKey As Variant) As CReglaSubasta
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
     Set NewEnum = mCol.[_NewEnum]
End Property
Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
     Count = mCol.Count
End If


End Property

Public Sub Remove(vntIndexKey As Variant)
   
    mCol.Remove vntIndexKey
End Sub

Public Function Add(ByVal iAnyo As Integer, ByVal sGMN1 As String, ByVal iProce As Long, ByVal iID As Integer, ByVal sNom As String, ByVal lngAdjunProce As Long, Optional ByVal vCOM As Variant) As CReglaSubasta
    'create a new object
    Dim oNewMember As CReglaSubasta

    Set oNewMember = New CReglaSubasta

    Set oNewMember.Conexion = mvarConexion
    oNewMember.Anyo = iAnyo
    oNewMember.GMN1 = sGMN1
    oNewMember.Proce = iProce
    oNewMember.ID = iID
    oNewMember.Nom = sNom
    oNewMember.AdjunProce = lngAdjunProce
    If Not IsMissing(vCOM) And Not IsNull(vCOM) Then oNewMember.COM = lngAdjunProce
        
    mCol.Add oNewMember, CStr(iID)

    'return the object created
    Set Add = oNewMember
    Set oNewMember = Nothing
End Function

Private Sub Class_Initialize()
    Set mCol = New Collection
End Sub

Private Sub Class_Terminate()
'destroys collection when this class is terminated
    Set mCol = Nothing
    Set mvarConexion = Nothing
End Sub

