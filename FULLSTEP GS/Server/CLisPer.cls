VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "cLisPer"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private mvarLis As Integer
Private mvarPerfLis As String
Private mvarProducto As String

Public Property Let Lis(ByVal d As Integer)
    mvarLis = d
End Property
Public Property Get Lis() As Integer
    Lis = mvarLis
End Property

Public Property Let PerfLis(ByVal d As String)
    mvarPerfLis = d
End Property
Public Property Get PerfLis() As String
    PerfLis = mvarPerfLis
End Property

Public Property Let Producto(ByVal d As String)
    mvarProducto = d
End Property
Public Property Get Producto() As String
    Producto = mvarProducto
End Property


