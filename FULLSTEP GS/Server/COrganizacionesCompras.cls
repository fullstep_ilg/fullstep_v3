VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "COrganizacionesCompras"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum
Private mCol As Collection
Private mvarConexion As CConexion
Private oOrgCompras As COrganizacionCompras

Friend Property Set Conexion(ByVal con As CConexion)
    
    Set mvarConexion = con
    
End Property
Friend Property Get Conexion() As CConexion

    Set Conexion = mvarConexion
    
End Property

Public Property Get Item(vntIndexKey As Variant) As COrganizacionCompras
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)

    Exit Property

NoSeEncuentra:
    Set Item = Nothing

End Property

Public Property Get NewEnum() As IUnknown
       Set NewEnum = mCol.[_NewEnum]
End Property

Public Sub Remove(vntIndexKey As Variant)

    mCol.Remove vntIndexKey

End Sub

Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
     Count = mCol.Count
End If

End Property

Public Function Add(Optional ByVal sCod As String, Optional ByVal sDen As String, Optional ByVal ProveCod As String _
, Optional ByVal varIndice As Variant) As COrganizacionCompras

    Dim objnewmember As COrganizacionCompras
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New COrganizacionCompras
    
    objnewmember.Cod = sCod
    objnewmember.Den = sDen
    Set objnewmember.Conexion = mvarConexion
    
    If IsNull(varIndice) Or IsMissing(varIndice) Then
        If mCol.Count > 0 Then
            'If mCol.Item(sCod) Is Nothing Then
                On Error Resume Next
                mCol.Add objnewmember, CStr(sCod)
                On Error GoTo 0
            'End If
        Else
            mCol.Add objnewmember, CStr(sCod)
        End If
    Else
        mCol.Add objnewmember, CStr(varIndice)
    End If


    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "COrganizacionesCompras", "Add", ERR, Erl)
        Exit Function
    End If

End Function

''' <summary>
''' Obtener la Unidad Organizativa
''' </summary>
'''<param optional name="sCodUON1">C�digo UON1</param>
'''<param optional name="sCodUON2">C�digo UON2</param>
'''<param optional name="sCodUON3">C�digo UON3</param>
''' <returns></returns>
''' <remarks>Llamada desde: ; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 17/01/2012</revision>
Public Function ObtenerUON(Optional ByVal sCodUON1 As String, Optional ByVal sCodUON2 As String, Optional ByVal sCodUON3 As String) As ADODB.Recordset
    Dim rs As ADODB.Recordset
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set rs = New ADODB.Recordset
    Dim sSQL As String
    
    If sCodUON3 <> "" Then
        sSQL = "SELECT UON2.UON1,UON2.COD,UON2.DEN,UON2.FECACT,UON2.EMPRESA,UON2.ORGCOMPRAS,UON2.CENTROS,UON2.ALMACEN,UON2.BAJALOG FROM UON2 WITH (NOLOCK) INNER JOIN UON3 WITH (NOLOCK) ON UON3.UON2=UON2.COD AND UON3.COD='" & sCodUON3 & "'"
        If sCodUON2 <> "" Then
            sSQL = sSQL & " AND UON3.UON2 ='" & sCodUON2 & "'"
            If sCodUON1 <> "" Then
                sSQL = sSQL & " AND UON3.UON1='" & sCodUON1 & "'"
                sSQL = sSQL & " AND UON2.UON1='" & sCodUON1 & "'"
            End If
        End If
    Else
        If sCodUON2 <> "" Then
            sSQL = "SELECT UON1.COD,UON1.DEN,UON1.FECACT,UON1.EMPRESA,UON1.ORGCOMPRAS,UON1.CENTROS,UON1.ALMACEN,UON1.BAJALOG,UON1.PYME FROM UON1 WITH (NOLOCK) INNER JOIN UON2 WITH (NOLOCK)  ON UON2.UON1= UON1.COD AND UON2.COD='" & sCodUON2 & "'"
            If sCodUON1 <> "" Then
                sSQL = sSQL & " AND UON2.UON1='" & sCodUON1 & "'"
            End If
        Else
            If sCodUON1 <> "" Then
                sSQL = "SELECT COD,DEN,FECACT,EMPRESA,ORGCOMPRAS,CENTROS,ALMACEN,BAJALOG,PYME FROM UON1 WITH (NOLOCK) WHERE COD='" & sCodUON1 & "'"
            End If
        End If
    End If
    rs.Open sSQL, Conexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    If Not rs.eof Then
        Set ObtenerUON = rs
    Else
        Set ObtenerUON = Nothing
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "COrganizacionesCompras", "ObtenerUON", ERR, Erl)
        Exit Function
    End If
  
End Function


Public Sub CargarOrganizacionesCompra(Optional ByVal sCod As String, Optional ByVal bCaracsInics As Boolean = False)
    Dim sConsulta As String
    Dim rs As New ADODB.Recordset
    Dim fldCod As ADODB.Field
    Dim fldDen As ADODB.Field
    
    '********* Precondicion **************************************
    If mvarConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "COrganizacionesCompras.CargarOrganizacionesCompra", "No se ha establecido la conexion"
        Exit Sub
    End If
    '*************************************************************
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
    sConsulta = "SELECT * FROM ORGCOMPRAS WITH (NOLOCK) WHERE 1 = 1"
    
    If sCod <> "" Then
        If bCaracsInics Then
            sConsulta = sConsulta & " AND DEN LIKE '" & sCod & "%'"
        Else
            sConsulta = sConsulta & " AND COD ='" & sCod & "'"
        End If
    End If
    sConsulta = sConsulta & " ORDER BY DEN"
    
    rs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
      
    Set mCol = Nothing
    Set mCol = New Collection
      
    If rs.eof Then
            
        rs.Close
        Set rs = Nothing
        Exit Sub
          
    Else
        
        Set fldCod = rs.Fields("COD")
        Set fldDen = rs.Fields("DEN")
    
        While Not rs.eof
            
            Me.Add fldCod.Value, fldDen.Value
            rs.MoveNext
        Wend
        
        rs.Close
        Set rs = Nothing
        Set fldCod = Nothing
        Set fldDen = Nothing
        
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "COrganizacionesCompras", "CargarOrganizacionesCompra", ERR, Erl)
        Exit Sub
    End If
    
End Sub

''' <summary>devuelve datos para la pantalla frmESTRORGDetalle</summary>
''' <param name="rs">rs a devolver</param>
''' <param name="HayEmpresa"></param>
''' <param name="sCodUON1"></param>
''' <param name="sCodUON2"></param>
''' <param name="sCodUON3"></param>
''' <returns>Objeto de tipo TipoErrorSummit</returns>
''' <remarks>Llamada desde frmESTRORGDetalle</remarks>
''' <remarks>Revisado por: LTG  Fecha: 11/05/2011</remarks>

Public Function CargarOrgCompraEmpresa(ByRef rs As Recordset, ByVal HayEmpresa As Boolean, Optional ByVal sCodUON1 As String, Optional ByVal sCodUON2 As String, Optional ByVal sCodUON3 As String) As TipoErrorSummit
    'Cargar los datos de la Organizacion de compras
     Dim sSQL As String
     Dim TESError As TipoErrorSummit
     Dim sCod As String
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sCodUON3 <> "" Then
        sSQL = "SELECT U3.ORGCOMPRAS U3COMPRAS,O.DEN DENORG,U3.CENTROS U3CENTRO,C.DEN DENCENTRO,U3.ALMACEN U3ALMACEN, A.DEN DENALMACEN, "
        sSQL = sSQL & " U2.ORGCOMPRAS U2COMPRAS, U2.CENTROS U2CENTRO,U2.ALMACEN U2ALMACEN,"
        sSQL = sSQL & " U1.ORGCOMPRAS U1COMPRAS, U1.CENTROS U1CENTRO,U1.ALMACEN U1ALMACEN,"
    Else
         If sCodUON2 <> "" Then
             sSQL = "SELECT U2.ORGCOMPRAS U2COMPRAS , O.DEN DENORG, U2.CENTROS U2CENTRO, C.DEN DENCENTRO,U2.ALMACEN U2ALMACEN, A.DEN DENALMACEN, "
             sSQL = sSQL & " U1.ORGCOMPRAS U1COMPRAS, U1.CENTROS U1CENTRO,U1.ALMACEN U1ALMACEN,"
         Else
             If sCodUON1 <> "" Then
                 sSQL = "SELECT U1.ORGCOMPRAS U1COMPRAS,O.DEN DENORG,U1.CENTROS U1CENTRO,C.DEN DENCENTRO, U1.ALMACEN U1ALMACEN, A.DEN DENALMACEN, "
             End If
         End If
     End If
    
     If HayEmpresa Then
         sSQL = sSQL & "E.ID, "
     End If
     
     If (sCodUON3 <> "") Then
         sSQL = sSQL & "U3.* FROM UON3 U3 WITH (NOLOCK)"
         sSQL = sSQL & " INNER JOIN UON2 U2 WITH (NOLOCK) ON U2.COD=U3.UON2 AND U2.UON1=U3.UON1 "
         sSQL = sSQL & " INNER JOIN UON1 U1 WITH (NOLOCK) ON U1.COD=U3.UON1 "
         If HayEmpresa Then
             sSQL = sSQL & " INNER JOIN EMP E WITH (NOLOCK) ON E.ID=U3.EMPRESA "
         End If
         sSQL = sSQL & " LEFT JOIN ORGCOMPRAS O WITH (NOLOCK) ON O.COD=U3.ORGCOMPRAS"
         sSQL = sSQL & " LEFT JOIN CENTROS C WITH (NOLOCK) ON C.COD=U3.CENTROS"
         sSQL = sSQL & " LEFT JOIN ALMACEN A WITH (NOLOCK) ON A.ID = U3.ALMACEN"
     Else
        If (sCodUON2 <> "") Then
            sSQL = sSQL & "U2.* FROM UON2 U2 WITH (NOLOCK) "
            sSQL = sSQL & " INNER JOIN UON1 U1 WITH (NOLOCK) ON U1.COD= U2.UON1 "
            If HayEmpresa Then
                sSQL = sSQL & " INNER JOIN EMP E WITH (NOLOCK) ON E.ID=U2.EMPRESA "
            End If
            sSQL = sSQL & " LEFT JOIN ORGCOMPRAS O WITH (NOLOCK) ON O.COD=U2.ORGCOMPRAS"
            sSQL = sSQL & " LEFT JOIN CENTROS C WITH (NOLOCK) ON C.COD=U2.CENTROS"
            sSQL = sSQL & " LEFT JOIN ALMACEN A WITH (NOLOCK) ON A.ID = U2.ALMACEN"
        Else
            If sCodUON1 <> "" Then
                sSQL = sSQL & "U1.* FROM UON1 U1 WITH (NOLOCK) "
                If HayEmpresa Then
                    sSQL = sSQL & " INNER JOIN EMP E WITH (NOLOCK) ON E.ID=U1.EMPRESA "
                End If
                sSQL = sSQL & " LEFT JOIN ORGCOMPRAS O WITH (NOLOCK) ON O.COD=U1.ORGCOMPRAS"
                sSQL = sSQL & " LEFT JOIN CENTROS C WITH (NOLOCK) ON C.COD=U1.CENTROS"
                sSQL = sSQL & " LEFT JOIN ALMACEN A WITH (NOLOCK) ON A.ID = U1.ALMACEN"
            End If
        End If
     End If
     
    If sCodUON3 <> "" Then
        sSQL = sSQL & " WHERE U3.COD='" & sCodUON3 & "'"
        If sCodUON2 <> "" Then
            sSQL = sSQL & " AND U3.UON2='" & sCodUON2 & "'"
            If sCodUON1 <> "" Then
                sSQL = sSQL & " AND U3.UON1 ='" & sCodUON1 & "'"
            End If
        End If
    Else
        If sCodUON2 <> "" Then
            sSQL = sSQL & " WHERE U2.COD='" & sCodUON2 & "'"
            If sCodUON1 <> "" Then
                sSQL = sSQL & " AND U2.UON1 ='" & sCodUON1 & "'"
            End If
        Else
            If sCodUON1 <> "" Then
                sSQL = sSQL & " WHERE U1.COD='" & sCodUON1 & "'"
            End If
        End If
    End If
     
    Set rs = New ADODB.Recordset
    
    rs.Open sSQL, Conexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    CargarOrgCompraEmpresa = TESError
    Exit Function
 
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "COrganizacionesCompras", "CargarOrgCompraEmpresa", ERR, Erl)
        Exit Function
    End If

End Function


Public Property Set OrganizacionesCompras(ByVal vData As Collection)
    Set mCol = vData
End Property

Public Property Get OrganizacionesCompras() As Collection
    Set OrganizacionesCompras = mCol
End Property

Private Sub Class_Initialize()

    Set mCol = New Collection

End Sub

Private Sub Class_Terminate()
    Set mCol = Nothing
    Set mvarConexion = Nothing
End Sub
''' <summary>Carga las organizaciones de compras para la empresa indicada. Si adem�s se indica la denominaci�n, filtrar� las organizaciones de compra con denominaci�n LIKE sDen</summary>
''' <param name="lEmpresa">Id de la empresa</param>
''' <param name="sDen">Denominacion de la organizaci�n de compras</param>
''' <returns></returns>
''' <remarks>Llamada desde frmPedidos.VisibilidadOrgcomprasy sdbcOrgCompras_DropDown. Tiempo < 0.1</remarks>
''' <remarks>Revisado por: NGO</remarks>
Public Sub CargarOrgCompraDesdeEmpresa(ByVal lEmpresa As Long, Optional ByVal sDen As String = "")
    Dim rs As ADODB.Recordset
    Dim fldCod As ADODB.Field
    Dim fldDen As ADODB.Field
    Dim fldEmpresa As ADODB.Field
    Dim oComm As ADODB.Command
    Dim oParam As ADODB.Parameter
    
    '********* Precondicion **************************************
    If mvarConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "COrganizacionesCompras.CargarOrganizacionesCompra", "No se ha establecido la conexion"
        Exit Sub
    End If
    '*************************************************************
    
    If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
    
    Set oComm = New ADODB.Command
    With oComm
        Set .ActiveConnection = mvarConexion.ADOCon
        .CommandType = adCmdStoredProc
        .CommandText = "FSEP_DEVOLVER_ORGANIZACIONES_COMPRAS"
        Set oParam = .CreateParameter("EMPRESA", adInteger, adParamInput, , lEmpresa)
        .Parameters.Append oParam
        If sDen <> "" Then
            Set oParam = .CreateParameter("DEN", adVarChar, adParamInput, Len(sDen), sDen)
            .Parameters.Append oParam
        End If
        
        Set rs = .Execute
    End With
      
    Set mCol = Nothing
    Set mCol = New Collection
      
    If Not rs.eof Then
        Set fldCod = rs.Fields("ORGCOMPRAS")
        Set fldDen = rs.Fields("DEN")
        Set fldEmpresa = rs.Fields("EMPRESA")
        While Not rs.eof
            Me.Add fldCod.Value, fldDen.Value
            rs.MoveNext
        Wend
        
        Set fldCod = Nothing
        Set fldDen = Nothing
        Set fldEmpresa = Nothing
    End If
    
    rs.Close
    
Salir:
    Set oParam = Nothing
    Set oComm = Nothing
    Set rs = Nothing
    Exit Sub
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "COrganizacionesCompras", "CargarOrgCompraDesdeEmpresa", ERR, Erl)
        Resume Salir
    End If
End Sub



