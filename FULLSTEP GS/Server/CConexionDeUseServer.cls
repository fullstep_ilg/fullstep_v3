VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CConexionDeUseServer"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True


Private mvarADOSummitCon As ADODB.Connection
'Private mvarRdoSummitErs As rdoErrors

Public Property Set ADOSummitCon(ByVal vData As ADODB.Connection)
    Set mvarADOSummitCon = vData
End Property


Public Property Get ADOSummitCon() As ADODB.Connection
    Set ADOSummitCon = mvarADOSummitCon
End Property


