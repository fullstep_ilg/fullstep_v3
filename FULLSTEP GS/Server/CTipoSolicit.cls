VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CTipoSolicit"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
''' *** Clase: CTipoSolicit
''' *** Creacion: 14/04/2005 (MMV)

Option Explicit

Implements IBaseDatos

''' Variables privadas con la informacion de un Tipo contrato

Private m_lID As Long
Private m_sCod As String
Private m_oDenominaciones As CMultiidiomas
Private m_vFecAct As Variant
Private m_lIndice As Long
Private m_iTipo As Integer

Private m_oCampos As CCamposPredef

''' Conexion
Private m_oConexion As CConexion

''' Control de errores
Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum

Public Property Let Id(ByVal vData As Long)
    m_lID = vData
End Property

Public Property Get Id() As Long
    Id = m_lID
End Property

Public Property Let Cod(ByVal vData As String)
    m_sCod = vData
End Property

Public Property Get Cod() As String
    Cod = m_sCod
End Property

Public Property Get Denominaciones() As CMultiidiomas
    Set Denominaciones = m_oDenominaciones
End Property

Public Property Set Denominaciones(ByVal dato As CMultiidiomas)
    Set m_oDenominaciones = dato
End Property

Public Property Get Indice() As Long
    Indice = m_lIndice
End Property

Public Property Let Indice(ByVal lInd As Long)
    m_lIndice = lInd
End Property

Public Property Get FecAct() As Variant
    FecAct = m_vFecAct
End Property

Public Property Let FecAct(ByVal vVar As Variant)
    m_vFecAct = vVar
End Property

Friend Property Set Conexion(ByVal vData As CConexion)
    Set m_oConexion = vData
End Property

Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property

Public Property Get Campos() As CCamposPredef
    Set Campos = m_oCampos
End Property
Public Property Set Campos(ByVal oCampos As CCamposPredef)
    Set m_oCampos = oCampos
End Property

Public Property Get Tipo() As Integer
    Tipo = m_iTipo
End Property

Public Property Let Tipo(ByVal dato As Integer)
    m_iTipo = dato
End Property

Private Sub Class_Terminate()
    Set m_oDenominaciones = Nothing
    Set m_oCampos = Nothing
    
    Set m_oConexion = Nothing
End Sub

''' <summary>Esta funci�n inserta un registro en la tabla</summary>
''' <returns>TipoErrorSummit con el error si lo hay</returns>
''' <remarks>Llamada desde: FSGClient
''' Tiempo m�ximo: 1 sec </remarks>
''' <revision>JVS 09/01/2012</revision>
Private Function IBaseDatos_AnyadirABaseDatos() As TipoErrorSummit
Dim sConsulta As String
Dim TESError As TipoErrorSummit
Dim rs As adodb.Recordset
Dim sCod As String
Dim sDen As String
Dim oDen As CMultiidioma


    TESError.NumError = TESnoerror
    
    ''' Precondicion
    
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CTipoSolicit.AnyadirABAseDatos", "No se ha establecido la conexion"
    End If
    
    If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:

    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
    
    If Not m_oDenominaciones Is Nothing Then
        For Each oDen In m_oDenominaciones
            sCod = sCod & "," & "DESCR_" & oDen.Cod
            sDen = sDen & "," & StrToSQLNULL(oDen.Den)
        Next
    End If
    
    'Inserta el campo en BD:
    sConsulta = "INSERT INTO TIPO_SOLICITUDES (COD " & sCod & ") VALUES ('" & DblQuote(m_sCod) & "'" & sDen & ")"
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    
    'Obtiene el id del campo introducido:
    Set rs = New adodb.Recordset
    'CALIDAD Sin WITH(NOLOCK) en el FROM para asegurar la recuperaci�n del mayor valor de la tabla
    sConsulta = "SELECT MAX(ID) AS ID FROM TIPO_SOLICITUDES"
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
    m_lID = rs("ID").Value
    rs.Close
    
    'CALIDAD Se obtiene la Fecha de Actualizaci�n con bloqueo para recuperar la verdadera �ltima fecha de actualizaci�n
    sConsulta = "SELECT FECACT FROM TIPO_SOLICITUDES WHERE ID=" & m_lID
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
    m_vFecAct = rs(0).Value
    rs.Close
    Set rs = Nothing
    
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    IBaseDatos_AnyadirABaseDatos = TESError
    Exit Function
    
Error_Cls:
     
    IBaseDatos_AnyadirABaseDatos = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CTipoSolicit", "IBaseDatos_AnyadirABaseDatos", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If
End Function

Private Function IBaseDatos_CambiarCodigo(CodigoNuevo As Variant) As TipoErrorSummit

End Function

Private Sub IBaseDatos_CancelarEdicion()
    ''' ! No usado por ahora
End Sub

Private Function IBaseDatos_ComprobarExistenciaEnBaseDatos() As Boolean

    ''' Comprueba que no exista un tipo de solicitud con ese c�digo:
Dim sConsulta As String
Dim AdoRes As adodb.Recordset



If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set AdoRes = New adodb.Recordset
    sConsulta = "SELECT ID FROM TIPO_SOLICITUDES WITH (NOLOCK) WHERE COD='" & DblQuote(m_sCod) & "'"
    
    AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    
    If AdoRes.eof Then
        IBaseDatos_ComprobarExistenciaEnBaseDatos = False
    Else
        IBaseDatos_ComprobarExistenciaEnBaseDatos = True
    End If
    
    AdoRes.Close
    Set AdoRes = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CTipoSolicit", "IBaseDatos_ComprobarExistenciaEnBaseDatos", ERR, Erl)
      Exit Function
   End If
    
End Function

''' <summary>
''' Elimina un registro
''' </summary>
''' <returns>Un errorsummit</returns>
''' <remarks>Llamada desde: FSGSClient;
''' Tiempo m�ximo: <1 seg</remarks>
''' <revision>JVS 09/01/2012</revision>
Private Function IBaseDatos_EliminarDeBaseDatos() As TipoErrorSummit
Dim sConsulta As String
Dim TESError As TipoErrorSummit

'********* Precondicion *******************
If m_oConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CTipoSolicit.EliminarDeBaseDatos", "No se ha establecido la conexion"
End If

'******************************************

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
    
    TESError.NumError = TESnoerror
    
    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
    
    sConsulta = "DELETE FROM CAMPO_PREDEF_VALOR_LISTA FROM CAMPO_PREDEF_VALOR_LISTA C WITH(NOLOCK) INNER JOIN CAMPOS_PREDEF WITH(NOLOCK) ON C.CAMPO=CAMPOS_PREDEF.ID WHERE CAMPOS_PREDEF.TIPO_SOLICITUD=" & m_lID
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    sConsulta = "DELETE FROM DESGLOSE_CAMPOS_PREDEF FROM DESGLOSE_CAMPOS_PREDEF D WITH(NOLOCK) INNER JOIN CAMPOS_PREDEF WITH(NOLOCK) ON D.CAMPO_PADRE=CAMPOS_PREDEF.ID WHERE CAMPOS_PREDEF.TIPO_SOLICITUD=" & m_lID
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    sConsulta = "DELETE FROM CAMPOS_PREDEF WHERE TIPO_SOLICITUD=" & m_lID
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    sConsulta = "DELETE FROM TIPO_SOLICITUDES WHERE ID=" & m_lID
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    IBaseDatos_EliminarDeBaseDatos = TESError
    Exit Function

Error_Cls:
    
    IBaseDatos_EliminarDeBaseDatos = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    
    m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CTipoSolicit", "IBaseDatos_EliminarDeBaseDatos", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If
    
End Function

Private Function IBaseDatos_FinalizarEdicionEliminando() As TipoErrorSummit

    ''' ! No usado por ahora
    
End Function

''' <summary>Esta funci�n finaliza la edici�n del resulset modificando</summary>
''' <returns>TipoErrorSummit con el error si lo hay</returns>
''' <remarks>Llamada desde: FSGClient
''' Tiempo m�ximo: <1 seg </remarks>
''' <revision>JVS 09/01/2012</revision>
Private Function IBaseDatos_FinalizarEdicionModificando() As TipoErrorSummit
Dim TESError As TipoErrorSummit
Dim btrans As Boolean
Dim sConsulta As String
Dim rs As adodb.Recordset
Dim sDen As String
Dim oDen As CMultiidioma


    TESError.NumError = TESnoerror

    ''' Precondicion
    
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CTipoSolicit.FinalizarEdicionModificando", "No se ha establecido la conexion"
    End If
    
    If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
    
    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
    btrans = True
    
    'CALIDAD Se obtiene la Fecha de Actualizaci�n con bloqueo para recuperar la verdadera �ltima fecha de actualizaci�n
    sConsulta = "SELECT FECACT FROM TIPO_SOLICITUDES WHERE ID=" & m_lID
    Set rs = New adodb.Recordset
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
    
    If rs.eof Then
        rs.Close
        Set rs = Nothing
        TESError.NumError = TESDatoEliminado
        TESError.Arg1 = 170 ' Tipo de solicitud
        IBaseDatos_FinalizarEdicionModificando = TESError
        m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
        Exit Function
    End If
    
    'Comprobamos si ha habido cambios en otra sesi�n
    If m_vFecAct <> rs("FECACT") Then
        rs.Close
        Set rs = Nothing
        TESError.NumError = TESInfActualModificada
        IBaseDatos_FinalizarEdicionModificando = TESError
        m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
        Exit Function
    End If
    
    sDen = ""
    If Not m_oDenominaciones Is Nothing Then
        For Each oDen In m_oDenominaciones
            sDen = sDen & ",DESCR_" & oDen.Cod & "=" & StrToSQLNULL(oDen.Den)
        Next
    End If

    sConsulta = "UPDATE TIPO_SOLICITUDES SET COD='" & DblQuote(m_sCod) & "'" & sDen & " WHERE ID=" & m_lID
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    btrans = False
    rs.Requery
    m_vFecAct = rs(0).Value
    rs.Close
    Set rs = Nothing
    TESError.NumError = TESnoerror
    IBaseDatos_FinalizarEdicionModificando = TESError
    
    Exit Function

Error_Cls:
    TESError = basErrores.TratarError(m_oConexion.ADOCon.Errors)
     
    If btrans Then
        m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
    End If

    Resume Salir:
    
Salir:

    On Error Resume Next
    If Not rs Is Nothing Then
        rs.Close
    End If
    
    IBaseDatos_FinalizarEdicionModificando = TESError
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CTipoSolicit", "IBaseDatos_FinalizarEdicionModificando", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If

End Function

''' <summary>Esta funci�n inicia la edici�n del resulset</summary>
'''<param optional name="Bloquear">Si se bloquea o no</param>
'''<param optional name="UsuarioBloqueo">Usuario del bloqueo</param>
''' <returns>TipoErrorSummit con el error si lo hay</returns>
''' <remarks>Llamada desde: FSGClient
''' Tiempo m�ximo: <1 seg </remarks>
''' <revision>JVS 09/01/2012</revision>
Private Function IBaseDatos_IniciarEdicion(Optional ByVal Bloquear As Boolean, Optional ByVal UsuarioBloqueo As String) As TipoErrorSummit
    Dim TESError As TipoErrorSummit
    Dim sSQL As String
    Dim rs As adodb.Recordset
        

    TESError.NumError = TESnoerror
    
    ''' Precondicion
    
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CTipoSolicit.IniciarEdicion", "No se ha establecido la conexion"
    End If
    
    If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
    Set rs = New adodb.Recordset
    'CALIDAD Se obtiene la Fecha de Actualizaci�n con bloqueo para recuperar la verdadera �ltima fecha de actualizaci�n
    rs.Open "SELECT FECACT FROM TIPO_SOLICITUDES WITH (NOLOCK) WHERE ID=" & m_lID, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
    
    If rs.eof Then
        rs.Close
        Set rs = Nothing
        TESError.NumError = TESDatoEliminado
        TESError.Arg1 = 170 ' Tipo de solicitud
        IBaseDatos_IniciarEdicion = TESError
        Exit Function
    End If
     
     ''' Si hay datos diferentes, devolver la condicion
    
    If m_vFecAct <> rs(0).Value Then
        TESError.NumError = TESInfActualModificada
        rs.Close
        Set rs = Nothing
        IBaseDatos_IniciarEdicion = TESError
        Exit Function
    End If
    
    rs.Close
    Set rs = Nothing
    TESError.NumError = TESnoerror
    IBaseDatos_IniciarEdicion = TESError
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CTipoSolicit", "IBaseDatos_IniciarEdicion", ERR, Erl)
      Exit Function
   End If
    
End Function

''' Revisado por: Jbg. Fecha: 27/09/2011
''' <summary>
''' Carga los campos del tipo de solicitud
''' </summary>
''' <param name="UsarIndice">Si se usa o no, indice en la coleccion</param>
''' <param name="IdiomasAplicacion">Si se usan solo los idiomas de la aplicaci�n o todos</param>
''' <param name="bMostrarSoloDesglose">Indica si se cargan �nicamente aquellos campos que se ven s�lo en el desglose</param>
''' <remarks>Llamada desde: frmCamposSolic.CargarCampos     frmPARTipoSolicit.TipoSeleccionado; Tiempo m�ximo:0,1</remarks>
Public Sub CargarCamposTipo(Optional ByVal UsarIndice As Boolean, Optional ByVal IdiomasAplicacion As Boolean = False, Optional ByVal bMostrarSoloDesglose As Boolean)
Dim rs As New adodb.Recordset
Dim sConsulta As String
Dim lIndice As Long
Dim oDenominaciones As CMultiidiomas
Dim oAyudas As CMultiidiomas
Dim oGestorParametros As CGestorParametros
Dim oIdiomas As CIdiomas
Dim oIdi As CIdioma
Dim vValorMax As Variant
Dim vValorMin As Variant


    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oGestorParametros = New CGestorParametros
    Set oGestorParametros.Conexion = m_oConexion
    Set oIdiomas = oGestorParametros.DevolverIdiomas(False, False, True)
    
    Dim cden_general, ayuda_general As String
    cden_general = ""
    ayuda_general = ""
    
    For Each oIdi In oIdiomas
        If (cden_general <> "") Then
           cden_general = cden_general & ","
           ayuda_general = ayuda_general & ","
        End If
        cden_general = cden_general & "C.DEN_" & oIdi.Cod
        ayuda_general = ayuda_general & "AYUDA_" & oIdi.Cod
    Next
    ' cden_general = C.DEN_SPA,C.DEN_ENG,C.DEN_GER,C.DEN_FRA
    ' ayuda_general = AYUDA_SPA,AYUDA_ENG,AYUDA_GER,AYUDA_FRA


    
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CTipoSolicit.CargarCamposTipo", "No se ha establecido la conexion"
        Exit Sub
    End If

    sConsulta = "SELECT T.TIPO TIPOSOL,C.ID,TIPO_SOLICITUD,INTRO,C.TIPO,SUBTIPO,MINNUM,MAXNUM,MINFEC,MAXFEC,ID_ATRIB_GS,TIPO_CAMPO_GS,ORDEN,ES_SUBCAMPO,C.FECACT," & cden_general & "," & ayuda_general & ""
    sConsulta = sConsulta & " FROM CAMPOS_PREDEF C WITH(NOLOCK)"
    sConsulta = sConsulta & " INNER JOIN TIPO_SOLICITUDES T WITH(NOLOCK) ON T.ID=C.TIPO_SOLICITUD"
    sConsulta = sConsulta & " WHERE TIPO_SOLICITUD=" & m_lID & " AND ES_SUBCAMPO=" & IIf(bMostrarSoloDesglose, 1, 0)

    sConsulta = sConsulta & " ORDER BY ORDEN"
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly

    If rs.eof Then
        rs.Close
        Set rs = Nothing

        Set m_oCampos = Nothing
        Set m_oCampos = New CCamposPredef
        Set m_oCampos.Conexion = m_oConexion
        Exit Sub

    Else
        Set m_oCampos = Nothing
        Set m_oCampos = New CCamposPredef
        Set m_oCampos.Conexion = m_oConexion

        Me.Tipo = rs.Fields("TIPOSOL").Value

        If UsarIndice Then
            lIndice = 0
        End If
    
        While Not rs.eof
            Set oDenominaciones = Nothing
            Set oDenominaciones = New CMultiidiomas
            Set oDenominaciones.Conexion = m_oConexion
            
            Set oAyudas = Nothing
            Set oAyudas = New CMultiidiomas
            Set oAyudas.Conexion = m_oConexion
            
            For Each oIdi In oIdiomas
                oDenominaciones.Add oIdi.Cod, rs.Fields("DEN_" & oIdi.Cod).Value
                oAyudas.Add oIdi.Cod, rs.Fields("AYUDA_" & oIdi.Cod).Value
            Next
                
            Select Case rs("TIPO").Value
                Case TiposDeAtributos.TipoNumerico
                    vValorMin = rs("MINNUM").Value
                    vValorMax = rs("MAXNUM").Value
                Case TiposDeAtributos.TipoFecha
                    vValorMin = rs("MINFEC").Value
                    vValorMax = rs("MAXFEC").Value
                Case Else
                    vValorMin = Null
                    vValorMax = Null
            End Select
            

            If UsarIndice Then
                m_oCampos.Add rs.Fields("ID").Value, m_lID, oDenominaciones, oAyudas, rs.Fields("TIPO").Value, rs.Fields("SUBTIPO").Value, rs.Fields("INTRO").Value, rs.Fields("MINNUM").Value, rs.Fields("MAXNUM").Value, rs.Fields("MINFEC").Value, rs.Fields("MAXFEC").Value, rs.Fields("TIPO_CAMPO_GS").Value, rs.Fields("ID_ATRIB_GS").Value, rs.Fields("ORDEN").Value, rs.Fields("ES_SUBCAMPO").Value, rs.Fields("FECACT").Value, , lIndice
                lIndice = lIndice + 1
            Else
                m_oCampos.Add rs.Fields("ID").Value, m_lID, oDenominaciones, oAyudas, rs.Fields("TIPO").Value, rs.Fields("SUBTIPO").Value, rs.Fields("INTRO").Value, rs.Fields("MINNUM").Value, rs.Fields("MAXNUM").Value, rs.Fields("MINFEC").Value, rs.Fields("MAXFEC").Value, rs.Fields("TIPO_CAMPO_GS").Value, rs.Fields("ID_ATRIB_GS").Value, rs.Fields("ORDEN").Value, rs.Fields("ES_SUBCAMPO").Value, rs.Fields("FECACT").Value
            End If
            

            rs.MoveNext
        Wend
        
        
        rs.Close
        Set rs = Nothing

        Set oGestorParametros = Nothing
        Set oIdiomas = Nothing
    
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CTipoSolicit", "CargarCamposTipo", ERR, Erl)
      Exit Sub
   End If
End Sub


Public Function CargarDenominacionTipo() As String
    Dim rs As New adodb.Recordset
    Dim sConsulta As String
    

    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CTipoSolicit.CargarDenominacionTipo", "No se ha establecido la conexion"
        Exit Function
    End If
    
    If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

    sConsulta = "SELECT DESCR_" & gParametrosInstalacion.gIdioma & " FROM TIPO_SOLICITUDES WITH (NOLOCK) WHERE ID=" & m_lID
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly

    If rs.eof Then
        CargarDenominacionTipo = ""
    Else
        CargarDenominacionTipo = rs.Fields("DESCR_" & gParametrosInstalacion.gIdioma).Value
    End If
    
    rs.Close
    Set rs = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CTipoSolicit", "CargarDenominacionTipo", ERR, Erl)
      Exit Function
   End If
        
End Function

Public Sub CargarTipoSolicitud(ByVal lCampoGS As Long, ByVal iTipo As Integer, ByVal iTipoPredef As Integer, Optional ByVal UsarIndice As Boolean)
Dim rs As New adodb.Recordset
Dim sConsulta As String
Dim lIndice As Long
Dim oDenominaciones As CMultiidiomas
Dim oAyudas As CMultiidiomas
Dim oGestorParametros As CGestorParametros
Dim oIdiomas As CIdiomas
Dim oIdi As CIdioma
Dim vValorMax As Variant
Dim vValorMin As Variant



If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oGestorParametros = New CGestorParametros
    Set oGestorParametros.Conexion = m_oConexion
    Set oIdiomas = oGestorParametros.DevolverIdiomas(False, False, True)
    
    Dim den_general, ayuda_general As String
    den_general = ""
    ayuda_general = ""
    
    For Each oIdi In oIdiomas
        If (den_general <> "") Then
           den_general = den_general & ","
           ayuda_general = ayuda_general & ","
        End If
        den_general = den_general & "DEN_" & oIdi.Cod
        ayuda_general = ayuda_general & "AYUDA_" & oIdi.Cod
    Next
    ' den_general = DEN_SPA,DEN_ENG,DEN_GER,DEN_FRA
    ' ayuda_general = AYUDA_SPA,AYUDA_ENG,AYUDA_GER,AYUDA_FRA
    

    
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CTipoSolicit.CargarCamposTipo", "No se ha establecido la conexion"
        Exit Sub
    End If

    sConsulta = "SELECT ID,TIPO_SOLICITUD,INTRO,TIPO,SUBTIPO,MINNUM,MAXNUM,MINFEC,MAXFEC,ID_ATRIB_GS,TIPO_CAMPO_GS,ORDEN,ES_SUBCAMPO,FECACT,PRES5," & den_general & "," & ayuda_general & " FROM CAMPOS_PREDEF WITH (NOLOCK) "
    sConsulta = sConsulta & " WHERE TIPO_CAMPO_GS = " & lCampoGS
    sConsulta = sConsulta & " AND TIPO = " & iTipo & " AND SUBTIPO = " & iTipoPredef
    
    sConsulta = sConsulta & " ORDER BY ORDEN"
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly

    If rs.eof Then
        rs.Close
        Set rs = Nothing

        Set m_oCampos = Nothing
        Set m_oCampos = New CCamposPredef
        Set m_oCampos.Conexion = m_oConexion
        Exit Sub

    Else
        Set m_oCampos = Nothing
        Set m_oCampos = New CCamposPredef
        Set m_oCampos.Conexion = m_oConexion

        If UsarIndice Then
            lIndice = 0
        End If
    
        While Not rs.eof
            Set oDenominaciones = Nothing
            Set oDenominaciones = New CMultiidiomas
            Set oDenominaciones.Conexion = m_oConexion
            
            Set oAyudas = Nothing
            Set oAyudas = New CMultiidiomas
            Set oAyudas.Conexion = m_oConexion
            
            For Each oIdi In oIdiomas
                oDenominaciones.Add oIdi.Cod, rs.Fields("DEN_" & oIdi.Cod).Value
                oAyudas.Add oIdi.Cod, rs.Fields("AYUDA_" & oIdi.Cod).Value
            Next
                
            Select Case rs("TIPO").Value
                Case TiposDeAtributos.TipoNumerico
                    vValorMin = rs("MINNUM").Value
                    vValorMax = rs("MAXNUM").Value
                Case TiposDeAtributos.TipoFecha
                    vValorMin = rs("MINFEC").Value
                    vValorMax = rs("MAXFEC").Value
                Case Else
                    vValorMin = Null
                    vValorMax = Null
            End Select
            

            If UsarIndice Then
                m_oCampos.Add rs.Fields("ID").Value, m_lID, oDenominaciones, oAyudas, rs.Fields("TIPO").Value, rs.Fields("SUBTIPO").Value, rs.Fields("INTRO").Value, rs.Fields("MINNUM").Value, rs.Fields("MAXNUM").Value, rs.Fields("MINFEC").Value, rs.Fields("MAXFEC").Value, rs.Fields("TIPO_CAMPO_GS").Value, rs.Fields("ID_ATRIB_GS").Value, rs.Fields("ORDEN").Value, rs.Fields("ES_SUBCAMPO").Value, rs.Fields("FECACT").Value, , lIndice
                lIndice = lIndice + 1
            Else
                m_oCampos.Add rs.Fields("ID").Value, m_lID, oDenominaciones, oAyudas, rs.Fields("TIPO").Value, rs.Fields("SUBTIPO").Value, rs.Fields("INTRO").Value, rs.Fields("MINNUM").Value, rs.Fields("MAXNUM").Value, rs.Fields("MINFEC").Value, rs.Fields("MAXFEC").Value, rs.Fields("TIPO_CAMPO_GS").Value, rs.Fields("ID_ATRIB_GS").Value, rs.Fields("ORDEN").Value, rs.Fields("ES_SUBCAMPO").Value, rs.Fields("FECACT").Value
            End If
            

            rs.MoveNext
        Wend
        
        
        rs.Close
        Set rs = Nothing

        Set oGestorParametros = Nothing
        Set oIdiomas = Nothing
    
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CTipoSolicit", "CargarTipoSolicitud", ERR, Erl)
      Exit Sub
   End If
End Sub

