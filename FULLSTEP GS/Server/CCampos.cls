VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CCampos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Private mCol As Collection

Private m_oConexion As CConexion

''' A�ade un objeto CCampo (campo de pedido) a la instancia de CCampos y tambi�n lo devuelve
''' Id: Id del campo de pedido
''' Cod: C�digo del atributo al que corresponde el campo de pedido
''' Den: Descripci�n del atributo al que corresponde el campo de pedido
''' vObl: Obligatoriedad de dar valor al campo de pedido o no en la emisi�n de un pedido con �tems de esta categor�a
''' vDato: Valor por defecto del campo de pedido
''' vDatoNum: Valor Num�rico por defecto del campo de pedido
''' vDatoText: Valor de texto por defecto del campo de pedido
''' vDatoFec: Valor Fecha por defecto del campo de pedido
''' vDatoBool: Valor Booleano por defecto del campo de pedido
''' bInterno: determina si el campo de pedido visible o no en portal
''' Ambito: En qu� �mbito del pedido va el campo de pedido: Cabecera o L�nea
''' Tipo_introduccion: c�mo se introduce el valor: 1 de una lista, 0: libre
''' minimo: valor minimo
''' maximo: valor m�ximo
''' Tipo: Tipo de atributo: 1. Texto, 2. Num�rico, 3. Fecha, 4. Booleano
''' AtribID: ID del atributo
Public Function Add(ByVal Id As Long, ByVal Cod As String, Optional ByVal Den As String, Optional ByVal bObl As Boolean = False, _
        Optional ByVal vDato As Variant, Optional ByVal bInterno As Boolean = False, Optional ByVal Ambito As AmbitoDelCampoDePedido, _
        Optional ByVal Tipo_Introduccion As TAtributoIntroduccion, Optional ByVal Minimo As Variant, Optional Maximo As Variant, _
        Optional Tipo As TiposDeAtributos, Optional AtribID As Variant, Optional ByVal Origen As AtributosLineaCatalogo, _
        Optional ByVal valor_num As Variant, Optional ByVal valor_text As Variant, Optional ByVal valor_fec As Variant, _
        Optional ByVal valor_bool As Variant, Optional ByVal Lista_Externa As Boolean, Optional ByVal MostrarEnRecep As Boolean) As CCampo
    Dim objnewmember As CCampo
    Set objnewmember = New CCampo
    objnewmember.Id = Id
    objnewmember.Cod = Cod
    objnewmember.Den = Den
    objnewmember.Obligatorio = bObl
    objnewmember.Interno = bInterno
    If Not IsMissing(vDato) Then
        objnewmember.Valor = vDato
    Else
        objnewmember.Valor = Null
    End If
    objnewmember.TipoIntroduccion = Tipo_Introduccion
    objnewmember.Tipo = Tipo
    objnewmember.Maximo = Maximo
    objnewmember.Minimo = Minimo
    Select Case Tipo
        Case TiposDeAtributos.TipoNumerico
            objnewmember.ValorNum = vDato
        Case TiposDeAtributos.TipoFecha
            objnewmember.ValorFec = vDato
        Case TiposDeAtributos.TipoBoolean
            objnewmember.ValorBool = vDato
        Case TiposDeAtributos.TipoString
            objnewmember.ValorText = vDato
        Case Else
            objnewmember.ValorText = vDato
    End Select
    If Not IsMissing(valor_num) Then
        objnewmember.ValorNum = valor_num
    Else
        objnewmember.ValorNum = Null
    End If
    If Not IsMissing(valor_text) Then
        objnewmember.ValorText = valor_text
    Else
        objnewmember.ValorText = Null
    End If
    If Not IsMissing(valor_fec) Then
        objnewmember.ValorFec = valor_fec
    Else
        objnewmember.ValorFec = Null
    End If
    If Not IsMissing(valor_bool) Then
        objnewmember.ValorBool = valor_bool
    Else
        objnewmember.ValorBool = Null
    End If
    If Not IsMissing(Origen) Then
        objnewmember.Origen = Origen
    Else
        objnewmember.Origen = SinOrigen
    End If
    objnewmember.Ambito = Ambito
    objnewmember.AtribID = AtribID
    objnewmember.Lista_Externa = Lista_Externa
    objnewmember.MostrarEnRecep = MostrarEnRecep
     
    mCol.Add objnewmember, Cod
    Set Add = objnewmember
    Set objnewmember = Nothing
End Function
Friend Property Set Conexion(ByVal vData As CConexion)
    Set m_oConexion = vData
End Property

Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property

Public Property Get Item(vntIndexKey As Variant) As CCampo

    ''' * Objetivo: Recuperar una moneda de la coleccion
    ''' * Recibe: Indice de la moneda a recuperar
    ''' * Devuelve: Moneda correspondiente
        
On Error GoTo NoSeEncuentra:

    ''' Devolvemos el item de la coleccion privada
    
    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:

    Set Item = Nothing
    
End Property

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"

    ''' ! Pendiente de revision, objetivo desconocido
    
    Set NewEnum = mCol.[_NewEnum]

End Property
Public Sub Remove(vntIndexKey As Variant)

    ''' * Objetivo: Eliminar una moneda de la coleccion
    ''' * Recibe: Indice de la moneda a eliminar
    ''' * Devuelve: Nada
   
    mCol.Remove vntIndexKey
    
End Sub
Private Sub Class_Initialize()

    ''' * Objetivo: Crear la coleccion
    
   
    Set mCol = New Collection
    
End Sub
Private Sub Class_Terminate()

    ''' * Objetivo: Limpiar memoria
    
    Set mCol = Nothing
        
End Sub

Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
     Count = mCol.Count
End If


End Property

''' Carga los datos de los campos de pedido vinculados a la categor�a pasada
''' lCat: C�digo de la Categor�a
''' iNivel: Nivel de la categor�a
''' bRecep: para sacar tambien lod campos de tipo recepcion
''' Llamada desde frmCatalogo. Max 0,3 seg.
''' Revisado por: jbg; Fecha: 10/06/2015
Public Function CargarDatos(ByVal lCat As Long, iNivel As Integer, Optional bRecep As Boolean = False) As CCampos
Dim AdoRes As adodb.Recordset
Dim sConsulta As String
Dim iError As Integer

Set mCol = Nothing
Set mCol = New Collection

Set AdoRes = New adodb.Recordset

iError = 101 + iNivel

sConsulta = "SELECT C.ID,D.COD,D.DEN,C.OBLIGATORIO,C.INTERNO,C.VALOR_NUM,C.VALOR_TEXT,C.VALOR_FEC,C.VALOR_BOOL,"
sConsulta = sConsulta & "C.AMBITO,D.MINNUM,D.MAXNUM,D.MINFEC,D.MAXFEC,D.TIPO,D.INTRO,D.ID ATRIBID,D.LISTA_EXTERNA,C.MOSTRARENRECEP "
sConsulta = sConsulta & " FROM CATN" & iNivel & "_ATRIB C WITH (NOLOCK)"
sConsulta = sConsulta & " INNER JOIN DEF_ATRIB D WITH (NOLOCK) ON C.ATRIB = D.ID "
sConsulta = sConsulta & " WHERE CATN" & iNivel & "_ID = " & lCat
If bRecep Then
    sConsulta = sConsulta & " AND C.TIPO=" & TipoAtributosLineaCatalogo.CamposRecepcion
Else
    sConsulta = sConsulta & " AND C.TIPO=" & TipoAtributosLineaCatalogo.CamposPedido
End If
sConsulta = sConsulta & " ORDER BY D.COD ASC"

AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenKeyset, adLockReadOnly
If AdoRes.eof Then
        AdoRes.Close
        Set AdoRes = Nothing
        Set CargarDatos = Nothing
        Exit Function
End If

While Not AdoRes.eof
    Dim Val As Variant
    Select Case AdoRes("TIPO").Value
        Case TiposDeAtributos.TipoNumerico
            Val = AdoRes("VALOR_NUM").Value
        Case TiposDeAtributos.TipoFecha
            Val = AdoRes("VALOR_FEC").Value
        Case TiposDeAtributos.TipoBoolean
            Val = AdoRes("VALOR_BOOL").Value
        Case TiposDeAtributos.TipoString
            Val = AdoRes("VALOR_TEXT").Value
        Case Else
            Val = AdoRes("VALOR_TEXT").Value
    End Select
    Dim Minimo As Variant
    Dim Maximo As Variant
    If AdoRes("TIPO").Value = TipoNumerico Then
        Minimo = NullToStr(AdoRes.Fields("MINNUM"))
        Maximo = NullToStr(AdoRes.Fields("MAXNUM"))
    Else
        Minimo = NullToStr(AdoRes.Fields("MINFEC"))
        Maximo = NullToStr(AdoRes.Fields("MAXFEC"))
    End If
    
    Me.Add AdoRes("ID").Value, AdoRes("COD").Value, AdoRes("DEN").Value, SQLBinaryToBoolean(AdoRes("OBLIGATORIO").Value), Val, SQLBinaryToBoolean(AdoRes("INTERNO").Value), AdoRes("AMBITO").Value, AdoRes("INTRO").Value, Minimo, Maximo, AdoRes("TIPO").Value, AdoRes("ATRIBID").Value, , , , , , AdoRes("LISTA_EXTERNA").Value, SQLBinaryToBoolean(AdoRes("MOSTRARENRECEP").Value)
    AdoRes.MoveNext
Wend
AdoRes.Close
Set AdoRes = Nothing
Set CargarDatos = Me

End Function

''' Revisado por: blp. Fecha: 19/12/2013
''' <summary>
''' A�ade los campos personalizados: Crea el campo en CATN1_ATRIB, 2, 3, 4 o 5
''' </summary>
''' <param name="lCat">Id de la categor�a</param>
''' <param name="iNivel">Nivel de la categor�a</param>
''' <param name="oCampos">Info de los campos personalizados a a�adir SOLO LOS CAMPOS NUEVOS</param>
''' <returns>devuelve el resultado de la inserci�n</returns>
''' <remarks>Llamada desde: frmCatalogo Tiempo m�ximo: 1 seg </remarks>
Public Function AnyadirCampos(ByVal lCat As Long, ByVal iNivel As Integer, ByVal oCampos As CCampos, Optional bRecep As Boolean = False) As TipoErrorSummit
    Dim TESError As TipoErrorSummit
    Dim sConsulta As String
    Dim btrans As Boolean
    Dim bInsertar As Boolean
    
    TESError.NumError = TESnoerror
    
    '********* Precondicion *******************
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CCampos.AnyadirCampos", "No se ha establecido la conexion"
    End If

On Error GoTo ERROR

    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
    btrans = True
    Dim oCampo As CCampo
    For Each oCampo In oCampos
        bInsertar = False
        If Me.Count > 0 Then
            If Not Me.Item(oCampo.Cod) Is Nothing Then
                If Me.Item(oCampo.Cod).Id <> 0 Then 'Exist�a, tiene ID
                    If Not oCampos.Item(oCampo.Cod) Is Nothing Then
                        'Si es distinto el nombre
                        If Me.Item(oCampo.Cod).Den <> oCampos.Item(oCampo.Cod).Den Then
                            bInsertar = True
                            Exit For
                        End If
                    End If
                Else 'No existe, el ID es 0
                    bInsertar = True
                End If
            Else
                bInsertar = True
            End If
        Else
            bInsertar = True
        End If
        If bInsertar Then
            'A�ADIR ABDD
            sConsulta = "INSERT INTO CATN" & iNivel & "_ATRIB (CATN" & iNivel & "_ID, ATRIB, OBLIGATORIO, INTERNO, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL, AMBITO,TIPO) "
            sConsulta = sConsulta & "VALUES (" & lCat & "," & oCampo.AtribID & "," & BooleanToSQLBinary(oCampo.Obligatorio) & "," & BooleanToSQLBinary(oCampo.Interno) & "," & StrToSQLNULL(oCampo.ValorNum) & "," & StrToSQLNULL(oCampo.ValorText) & "," & StrToSQLNULL(oCampo.ValorFec) & "," & StrToSQLNULL(oCampo.ValorBool) & "," & oCampo.Ambito & "," & IIf(bRecep, TipoAtributosLineaCatalogo.CamposRecepcion, TipoAtributosLineaCatalogo.CamposPedido) & ")"
            m_oConexion.ADOCon.Execute sConsulta
            If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR
        End If
    Next
    
    m_oConexion.ADOCon.Execute "COMMIT TRAN"

    AnyadirCampos = TESError
        
    Exit Function
    
Finalizar:
    On Error Resume Next
    Exit Function
ERROR:

    'IBaseDatos_AnyadirABaseDatos = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    TESError.NumError = TESErrorInesperado
    ActualizarCamposPersonalizados = TESError
    If btrans Then
        m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
    End If
    Resume Finalizar
End Function

''' Revisado por: blp. Fecha: 20/12/2013
''' <summary>
''' Elimina los campos de pedido pasados com par�metro
''' </summary>
''' <param name="lCat">Id de la categor�a</param>
''' <param name="iNivel">Nivel de la categor�a</param>
''' <param name="oCampos">Info de los campos de pedido a eliminar</param>
''' <returns>devuelve el resultado de la eliminaci�n</returns>
''' <remarks>Llamada desde: frmCatalogo
'''         Tiempo m�ximo: 1 seg </remarks>
Public Function EliminarCampos(ByVal lCat As Long, ByVal iNivel As Integer) As TipoErrorSummit
    Dim TESError As TipoErrorSummit
    Dim sConsulta As String
    Dim btrans As Boolean
    
    TESError.NumError = TESnoerror
    
    '********* Precondicion *******************
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CCampos.EliminarCampos", "No se ha establecido la conexion"
    End If

On Error GoTo ERROR

    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
    btrans = True
    Dim oCampo As CCampo
    For Each oCampo In mCol
        sConsulta = "DELETE CATN" & iNivel & "_ATRIB WHERE ID = " & oCampo.Id
        m_oConexion.ADOCon.Execute sConsulta
        If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR
    Next
    m_oConexion.ADOCon.Execute "COMMIT TRAN"
    EliminarCampos = TESError

Exit Function
    
Finalizar:
    On Error Resume Next
    Exit Function
ERROR:

    'IBaseDatos_AnyadirABaseDatos = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    TESError.NumError = TESErrorInesperado
    ActualizarCamposPersonalizados = TESError
    If btrans Then
        m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
    End If
    Resume Finalizar
End Function

Public Function ObtenerCodCAT(ByVal lCat As Long, iNivel As Integer) As String
    Dim sSQL As String
    Dim adoRs As adodb.Recordset
    
    Set adoRs = New adodb.Recordset
    sSQL = "SELECT COD FROM CATN" & iNivel & " WITH (NOLOCK) WHERE ID=" & lCat
    adoRs.Open sSQL, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    If Not adoRs.eof Then
        ObtenerCodCAT = adoRs.Fields(0).Value
    Else
        ObtenerCodCAT = ""
    End If
    Set adoRs = Nothing
    
End Function


''' Revisado por: blp. Fecha: 30/12/2013
''' <summary>
''' Actualiza en bdd el valor de las propiedades de un campo de pedido
''' </summary>
''' <param name="cant_id">Id del campo de pedido</param>
''' <param name="iNivel">Nivel de la categor�a</param>
''' <param name="iObligatorio">Indica si ser� obligatorio dar valor al campo de pedido cuando se emita un pedido</param>
''' <param name="Valor">Valor num�rico por defecto del campo de pedido</param>
''' <param name="oAmbito">�mbito del campo de pedido cuando se emita un pedido: cabecera o l�nea</param>
''' <param name="iInterno">visibilidad del campo de pedido en portal 0->No visible, 1->visible</param>
''' <returns>devuelve el resultado de la actualizaci�n</returns>
''' <remarks>Llamada desde: frmCatalogo
'''         Tiempo m�ximo: 1 seg </remarks>
Public Function ActualizarCampo(ByVal cant_id As Integer, ByVal iNivel As Integer, ByVal iObligatorio As Integer, ByVal Valor As Variant, ByVal oAmbito As AmbitoDelCampoDePedido, ByVal iInterno As Integer _
, ByVal TipoDatos As TiposDeAtributos, ByVal bMostrarEnRecep As Boolean) As TipoErrorSummit
    Dim TESError As TipoErrorSummit
    Dim sConsulta As String
    Dim btrans As Boolean
    
    TESError.NumError = TESnoerror
    
    '********* Precondicion *******************
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CCampos.ActualizarCampo", "No se ha establecido la conexion"
    End If

On Error GoTo ERROR

    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
    btrans = True
    
    Dim vValorNum As Variant, vValorText As Variant, vValorFec As Variant, vValorBool As Variant
    vValorNum = Null
    vValorText = Null
    vValorFec = Null
    vValorBool = Null
    Select Case TipoDatos
        Case TiposDeAtributos.TipoNumerico:
            vValorNum = Valor
        Case TiposDeAtributos.TipoFecha:
            vValorFec = Valor
        Case TiposDeAtributos.TipoBoolean:
            vValorBool = Valor
        Case TiposDeAtributos.TipoString, TiposDeAtributos.TipoTextoCorto, TiposDeAtributos.TipoTextoLargo, TiposDeAtributos.TipoTextoMedio:
            vValorText = Valor
    End Select
    sConsulta = "UPDATE CATN" & iNivel & "_ATRIB SET OBLIGATORIO = " & BooleanToSQLBinary(iObligatorio) & ", VALOR_NUM = " & StrToSQLNULL(vValorNum) & ", VALOR_TEXT = " & StrToSQLNULL(vValorText) _
        & ", VALOR_FEC = " & StrToSQLNULL(vValorFec) & ", VALOR_BOOL = " & StrToSQLNULL(vValorBool) & ", AMBITO = " & oAmbito & ", INTERNO = " & BooleanToSQLBinary(iInterno) & ", MOSTRARENRECEP = " & BooleanToSQLBinary(bMostrarEnRecep) _
        & " WHERE ID = " & cant_id
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR
    m_oConexion.ADOCon.Execute "COMMIT TRAN"

    ActualizarCampo = TESError
        
    Exit Function
Finalizar:
    On Error Resume Next
    Exit Function
ERROR:
    'IBaseDatos_AnyadirABaseDatos = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    TESError.NumError = TESErrorInesperado
    ActualizarCampo = TESError
    If btrans Then
        m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
    End If
    Resume Finalizar
End Function
