VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CPMConfCumplimentaciones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

''' Variables que almacenan las propiedades de los objetos
Private m_Col As Collection
Private m_bEOF As Boolean
Private m_oConexion As CConexion
Private m_bVinculadoOrdenado As Boolean


''' Control de errores
Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum

''' <summary>
''' Crear un objeto configuraci�n de la cumplimentaci�n para un bloque, rol y campo.
''' </summary>
''' <param name="lBloque">Bloque</param>
''' <param name="lRol">Rol</param>
''' <param name="lCampo">Id de Campo</param>
''' <param name="oCampo">Objeto Campo q esta siendo configurado</param>
''' <param name="bIntro">indica si es combo</param>
''' <param name="bVisible">indica si es visible</param>
''' <param name="bEscritura">indica si es editable</param>
''' <param name="bObl">indica si es obligatorio</param>
''' <param name="iOrden">indica la posici�n en pantalla</param>
''' <param name="FecAct">Fecha ultima grabaci�n de la configuraci�n</param>
''' <param name="varIndice">si se usa indice o no</param>
''' <param name="CodAtrib">Id de atributo</param>
''' <param name="bEscrituraFormula">si se puiede escribir o no la formula</param>
''' <param name="bPermitirMover">si se permiter mover lineas vinculadas</param>
''' <returns>Un objeto CPMConfCumplimentacion</returns>
''' <remarks>Llamada desde: frmFlujosCumplDesglose; Tiempo m�ximo: 0,1</remarks>
Public Function Add(ByVal lBloque As Long, ByVal lRol As Long, ByVal lCampo As Long, _
                    Optional ByVal oCampo As CFormItem, Optional ByVal bIntro As Boolean, _
                    Optional ByVal bVisible As Boolean, Optional ByVal bEscritura As Boolean, Optional ByVal bObl As Boolean, Optional ByVal iOrden As Integer, _
                    Optional ByVal FecAct As Variant, Optional ByVal varIndice As Variant, _
                    Optional ByVal CodAtrib As Variant, Optional ByVal bEscrituraFormula As Boolean, Optional ByVal bPermitirMover As Boolean _
                    ) As CPMConfCumplimentacion
                    
    Dim objnewmember As CPMConfCumplimentacion
    
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo Error
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New CPMConfCumplimentacion
    
    objnewmember.Bloque = lBloque
    objnewmember.Rol = lRol
    objnewmember.IdCampo = lCampo
    
    Set objnewmember.Campo = oCampo
    
    objnewmember.Intro = bIntro
    objnewmember.Visible = bVisible
    objnewmember.Escritura = bEscritura
    objnewmember.Obligatorio = bObl
    objnewmember.Orden = iOrden
    objnewmember.EscrituraFormula = bEscrituraFormula
    objnewmember.PermitirMover = bPermitirMover
    
    If Not IsMissing(FecAct) Then
        objnewmember.FecAct = FecAct
    Else
        objnewmember.FecAct = Null
    End If
    
    If Not IsMissing(CodAtrib) Then
        objnewmember.CodAtrib = CodAtrib
    Else
        objnewmember.CodAtrib = Null
    End If
    
    Set objnewmember.Conexion = m_oConexion
    
    If Not IsMissing(varIndice) And Not IsNull(varIndice) Then
        objnewmember.Indice = varIndice
        m_Col.Add objnewmember, CStr(varIndice)
    Else
        m_Col.Add objnewmember, CStr(lCampo)
    End If
            
    Set Add = objnewmember
    
    Set objnewmember = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CPMConfCumplimentaciones", "Add", ERR, Erl)
      Exit Function
   End If
    
End Function
''' <summary>
''' Crear un objeto configuraci�n de la cumplimentaci�n para un rol y campo. Estas en configuraci�n por defecto de roles.
''' </summary>
''' <param name="lRol">Rol</param>
''' <param name="lCampo">Id de Campo</param>
''' <param name="oCampo">Objeto Campo q esta siendo configurado</param>
''' <param name="bIntro">indica si es combo</param>
''' <param name="bVisible">indica si es visible</param>
''' <param name="bEscritura">indica si es editable</param>
''' <param name="bObl">indica si es obligatorio</param>
''' <param name="iOrden">indica la posici�n en pantalla</param>
''' <param name="FecAct">Fecha ultima grabaci�n de la configuraci�n</param>
''' <param name="varIndice">si se usa indice o no</param>
''' <param name="bPermitirMover">si se permiter mover lineas vinculadas</param>
''' <returns>Un objeto CPMConfCumplimentacion</returns>
''' <remarks>Llamada desde: CPMRol/CargarCumplimentacionDef; Tiempo m�ximo: 0,1</remarks>
Public Function AddDef(ByVal lRol As Long, ByVal lCampo As Long, _
                    Optional ByVal oCampo As CFormItem, Optional ByVal bIntro As Boolean, _
                    Optional ByVal bVisible As Boolean, Optional ByVal bEscritura As Boolean, Optional ByVal bObl As Boolean, Optional ByVal iOrden As Integer, _
                    Optional ByVal FecAct As Variant, Optional ByVal varIndice As Variant, Optional ByVal bPermitirMover As Boolean _
                    ) As CPMConfCumplimentacion
                    
    Dim objnewmember As CPMConfCumplimentacion
    
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo Error
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New CPMConfCumplimentacion
    
    objnewmember.Rol = lRol
    objnewmember.IdCampo = lCampo
    
    Set objnewmember.Campo = oCampo
    
    objnewmember.Intro = bIntro
    objnewmember.Visible = bVisible
    objnewmember.Escritura = bEscritura
    objnewmember.Obligatorio = bObl
    objnewmember.Orden = iOrden
    objnewmember.PermitirMover = bPermitirMover
    
    If Not IsMissing(FecAct) Then
        objnewmember.FecAct = FecAct
    Else
        objnewmember.FecAct = Null
    End If
    
    Set objnewmember.Conexion = m_oConexion
    
    If Not IsMissing(varIndice) And Not IsNull(varIndice) Then
        objnewmember.Indice = varIndice
        m_Col.Add objnewmember, CStr(varIndice)
    Else
        m_Col.Add objnewmember, CStr(lCampo)
    End If
            
    Set AddDef = objnewmember
    
    Set objnewmember = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CPMConfCumplimentaciones", "AddDef", ERR, Erl)
      Exit Function
   End If
    
End Function
                    

Public Property Get Item(vntIndexKey As Variant) As CPMConfCumplimentacion
On Error GoTo NoSeEncuentra:

    ''' Devolvemos el item de la coleccion privada
    
    Set Item = m_Col(vntIndexKey)
     
    Exit Property

NoSeEncuentra:

    Set Item = Nothing
    
End Property
Public Property Get eof() As Boolean

    ''' * Objetivo: Devolver variable privada EOF
    ''' * Recibe: Nada
    ''' * Devuelve: Variable privada EOF
    
    eof = m_bEOF
    
End Property
Friend Property Set Conexion(ByVal con As CConexion)

    ''' * Objetivo: Dar valor a la variable privada Conexion
    ''' * Recibe: Conexion
    ''' * Devuelve: Nada
    
    Set m_oConexion = con
    
End Property
Friend Property Get Conexion() As CConexion

    ''' * Objetivo: Devolver variable privada Conexion
    ''' * Recibe: Nada
    ''' * Devuelve: Conexion
    
    Set Conexion = m_oConexion
    
End Property
Public Property Let Vinculado_Ordenado(ByVal Value As Boolean)
    
     m_bVinculadoOrdenado = Value
    
End Property
Public Property Get Vinculado_Ordenado() As Boolean
    
     Vinculado_Ordenado = m_bVinculadoOrdenado
    
End Property
Public Property Get Count() As Long

    ''' * Objetivo: Devolver variable privada Count
    ''' * Recibe: Nada
    ''' * Devuelve: Numero de elementos de la coleccion

    If m_Col Is Nothing Then
        Count = 0
    Else
        Count = m_Col.Count
    End If

End Property
Public Sub Remove(vntIndexKey As Variant)


    ''' * Objetivo: Eliminar una solicitud de la coleccion
    ''' * Recibe: Indice de la solicitud a eliminar
    ''' * Devuelve: Nada
   
If Not g_oErrores.fg_bProgramando Then On Error GoTo Error
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_Col.Remove vntIndexKey
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CPMConfCumplimentaciones", "Remove", ERR, Erl)
      Exit Sub
   End If
    
End Sub
Private Sub Class_Initialize()

    ''' * Objetivo: Crear la coleccion

    Set m_Col = New Collection
    
End Sub
Private Sub Class_Terminate()

    ''' * Objetivo: Limpiar memoria
    Set m_Col = Nothing
    Set m_oConexion = Nothing
End Sub
Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4

    ''' ! Pendiente de revision, objetivo desconocido
    
    Set NewEnum = m_Col.[_NewEnum]

End Property


Public Function GuardarOrdenCampos() As TipoErrorSummit
'***********************************************************************************************
'Descripci�n:   Funci�n para guardar el orden del campo en la configuraci�n de cumplimentaci�n
'Valor que devuelve: Error
'***********************************************************************************************

Dim sConsulta As String
Dim TESError As TipoErrorSummit
Dim oConf As CPMConfCumplimentacion

Dim rs As ADODB.Recordset

'******************* Precondicion *******************
If m_oConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CPMConfCumplimentaciones.GuardarOrdenCampos", "No se ha establecido la conexion"
End If
'*****************************************************

On Error GoTo Error_Cls:

    TESError.NumError = TESnoerror

    m_oConexion.BEGINTRANSACTION
    Set rs = New ADODB.Recordset
    For Each oConf In Me
        sConsulta = "UPDATE PM_CONF_CUMP_BLOQUE SET ORDEN=" & oConf.Orden
        sConsulta = sConsulta & " WHERE BLOQUE = " & oConf.Bloque & " AND ROL = " & oConf.Rol & " AND CAMPO = " & oConf.IdCampo
        m_oConexion.ADOCon.Execute sConsulta
        If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    
    Next
    Set rs = Nothing
    m_oConexion.COMMITTRANSACTION
    GuardarOrdenCampos = TESError

    Exit Function

Error_Cls:
    If m_oConexion.ADOCon.Errors.Count > 0 Then
        GuardarOrdenCampos = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    ElseIf ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CPMConfCumplimentaciones", "GuardarOrdenCampos ", ERR, Erl)
    End If
    m_oConexion.ROLLBACKTRANSACTION
    

End Function




