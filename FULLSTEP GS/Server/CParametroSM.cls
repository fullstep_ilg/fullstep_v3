VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CParametroSM"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True

Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

Private m_oConexion As CConexion

Private m_sPRES5 As String
Private m_iImpModo As Integer
Private m_iImpSC As Integer
Private m_iImpPedido As Integer
Private m_iImpRecep As Integer
Private m_iImpNivel As Integer
Private m_iVigencia As Integer
Private m_iDispSC As Integer
Private m_iComprAcum As Integer
Private m_sNomNivelImputacion As String
Private m_bAcumularImpuestos As Boolean
Private m_bControlDispConPres As Boolean
Private m_bPlurianual As Boolean

Public Property Let PRES5(ByVal dato As String)
    m_sPRES5 = dato
End Property
Public Property Get PRES5() As String
    PRES5 = m_sPRES5
End Property

Public Property Let ImpModo(ByVal dato As Integer)
    m_iImpModo = dato
End Property
Public Property Get ImpModo() As Integer
    ImpModo = m_iImpModo
End Property
Public Property Let NomNivelImputacion(ByVal dato As String)
    m_sNomNivelImputacion = dato
End Property
Public Property Get NomNivelImputacion() As String
    NomNivelImputacion = m_sNomNivelImputacion
End Property

Public Property Let ImpSC(ByVal dato As Integer)
    m_iImpSC = dato
End Property
Public Property Get ImpSC() As Integer
    ImpSC = m_iImpSC
End Property

Public Property Let ImpPedido(ByVal dato As Integer)
    m_iImpPedido = dato
End Property
Public Property Get ImpPedido() As Integer
    ImpPedido = m_iImpPedido
End Property

Public Property Let ImpRecep(ByVal dato As Integer)
    m_iImpRecep = dato
End Property
Public Property Get ImpRecep() As Integer
    ImpRecep = m_iImpRecep
End Property

Public Property Let ImpNivel(ByVal dato As Integer)
    m_iImpNivel = dato
End Property
Public Property Get ImpNivel() As Integer
    ImpNivel = m_iImpNivel
End Property

Public Property Let Vigencia(ByVal dato As Integer)
    m_iVigencia = dato
End Property
Public Property Get Vigencia() As Integer
    Vigencia = m_iVigencia
End Property

Public Property Let DispSC(ByVal dato As Integer)
    m_iDispSC = dato
End Property
Public Property Get DispSC() As Integer
    DispSC = m_iDispSC
End Property

Public Property Let ComprAcum(ByVal dato As Integer)
    m_iComprAcum = dato
End Property
Public Property Get ComprAcum() As Integer
    ComprAcum = m_iComprAcum
End Property


Friend Property Set Conexion(ByVal vData As CConexion)

    ''' * Objetivo: Dar valor a la conexion de la Destino
    ''' * Recibe: Conexion
    ''' * Devuelve: Nada

    Set m_oConexion = vData
    
End Property
Friend Property Get Conexion() As CConexion

    ''' * Objetivo: Devolver la conexion de la Destino
    ''' * Recibe: Nada
    ''' * Devuelve: Conexion

    Set Conexion = m_oConexion
    
End Property

Private Sub Class_Terminate()
    
    On Error GoTo ERROR:
    
    Set m_oConexion = Nothing
    Exit Sub
ERROR:
    Resume Next
    
End Sub

Public Property Get acumularImpuestos() As Boolean

    acumularImpuestos = m_bAcumularImpuestos

End Property

Public Property Let acumularImpuestos(ByVal bacumularImpuestos As Boolean)

    m_bAcumularImpuestos = bacumularImpuestos

End Property

Public Property Get ControlDispConPres() As Boolean

    ControlDispConPres = m_bControlDispConPres

End Property

Public Property Let ControlDispConPres(ByVal bControlDispConPres As Boolean)

    m_bControlDispConPres = bControlDispConPres

End Property

Public Property Get Plurianual() As Boolean

    Plurianual = m_bPlurianual

End Property

Public Property Let Plurianual(ByVal bPlurianual As Boolean)

    m_bPlurianual = bPlurianual

End Property

