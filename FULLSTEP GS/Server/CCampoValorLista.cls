VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CCampoValorLista"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

Implements IBaseDatos

Private m_oConexion As CConexion

Private m_oCampo As CFormItem
Private m_oCampoPredef As CCampoPredef
Private m_lOrden As Long
Private m_vValorNum As Variant
Private m_vValorFec As Variant
Private m_oValorText As CMultiidiomas
Private m_lOrdenAnterior As Long
Private m_vIndice As Variant
Private m_vFecAct As Variant
Private m_lIdCampoPadre As Long
Private m_iOrdenCampoPadre As Integer
Private m_iAccion As Integer   '(1.- Elemento nuevo // 2.- Modificacion // 3.- Delete)
Private m_sGMN1 As String
Private m_sGMN2 As String
Private m_sGMN3 As String
Private m_sGMN4 As String
Private m_vGMNDen As CMultiidiomas

Private m_adores As adodb.Recordset

Public Property Let ValorNum(ByVal vData As Variant)
    m_vValorNum = vData
End Property
Public Property Get ValorNum() As Variant
    ValorNum = m_vValorNum
End Property

Public Property Set ValorText(ByVal vData As CMultiidiomas)
    Set m_oValorText = vData
End Property
Public Property Get ValorText() As CMultiidiomas
    Set ValorText = m_oValorText
End Property

Public Property Let ValorFec(ByVal vData As Variant)
    m_vValorFec = vData
End Property
Public Property Get ValorFec() As Variant
    ValorFec = m_vValorFec
End Property

Public Property Get Orden() As Long
    Orden = m_lOrden
End Property
Public Property Let Orden(ByVal Data As Long)
    m_lOrden = Data
End Property

Public Property Get OrdenAnterior() As Long
    OrdenAnterior = m_lOrdenAnterior
End Property
Public Property Let OrdenAnterior(ByVal Data As Long)
    m_lOrdenAnterior = Data
End Property

Public Property Get FecAct() As Variant
    FecAct = m_vFecAct
End Property
Public Property Let FecAct(ByVal Data As Variant)
    m_vFecAct = Data
End Property

Public Property Let Indice(ByVal varIndice As Variant)
    m_vIndice = varIndice
End Property
Public Property Get Indice() As Variant
    Indice = m_vIndice
End Property

Public Property Get Campo() As CFormItem
    Set Campo = m_oCampo
End Property
Public Property Set Campo(ByVal oCampo As CFormItem)
    Set m_oCampo = oCampo
End Property

Public Property Get CampoPredef() As CCampoPredef
    Set CampoPredef = m_oCampoPredef
End Property
Public Property Set CampoPredef(ByVal oCampo As CCampoPredef)
    Set m_oCampoPredef = oCampo
End Property

Public Property Get OrdenCampoPadre() As Integer
    OrdenCampoPadre = m_iOrdenCampoPadre
End Property
Public Property Let OrdenCampoPadre(ByVal Data As Integer)
    m_iOrdenCampoPadre = Data
End Property

Public Property Get IdCampoPadre() As Long
    IdCampoPadre = m_lIdCampoPadre
End Property
Public Property Let IdCampoPadre(ByVal Data As Long)
    m_lIdCampoPadre = Data
End Property

Public Property Get AccionElementoLista() As Integer
    AccionElementoLista = m_iAccion
End Property
Public Property Let AccionElementoLista(ByVal Data As Integer)
    m_iAccion = Data
End Property
Public Property Let GMN1(ByVal vData As String)
    m_sGMN1 = vData
End Property
Public Property Get GMN1() As String
    GMN1 = m_sGMN1
End Property
Public Property Let GMN2(ByVal vData As String)
    m_sGMN2 = vData
End Property
Public Property Get GMN2() As String
    GMN2 = m_sGMN2
End Property
Public Property Let GMN3(ByVal vData As String)
    m_sGMN3 = vData
End Property
Public Property Get GMN3() As String
    GMN3 = m_sGMN3
End Property
Public Property Let GMN4(ByVal vData As String)
    m_sGMN4 = vData
End Property
Public Property Get GMN4() As String
    GMN4 = m_sGMN4
End Property
Public Property Set GMNDen(ByVal vData As CMultiidiomas)
    Set m_vGMNDen = vData
End Property
Public Property Get GMNDen() As CMultiidiomas
    Set GMNDen = m_vGMNDen
End Property

Friend Property Set Conexion(ByVal vData As CConexion)

    ''' * Objetivo: Dar valor a la conexion de la Destino
    ''' * Recibe: Conexion
    ''' * Devuelve: Nada

    Set m_oConexion = vData
    
End Property
Friend Property Get Conexion() As CConexion

    ''' * Objetivo: Devolver la conexion de la Destino
    ''' * Recibe: Nada
    ''' * Devuelve: Conexion

    Set Conexion = m_oConexion
    
End Property

Private Sub Class_Terminate()
    Set m_oCampo = Nothing
    Set m_oCampoPredef = Nothing
    Set m_oValorText = Nothing
    Set m_oConexion = Nothing
    Set m_adores = Nothing
End Sub

Private Function IBaseDatos_AnyadirABaseDatos() As TipoErrorSummit

End Function

Private Function IBaseDatos_CambiarCodigo(CodigoNuevo As Variant) As TipoErrorSummit

End Function

Private Sub IBaseDatos_CancelarEdicion()

End Sub

Private Function IBaseDatos_ComprobarExistenciaEnBaseDatos() As Boolean
    
End Function

Private Function IBaseDatos_EliminarDeBaseDatos() As TipoErrorSummit
Dim sConsulta As String
Dim TESError As TipoErrorSummit

'********* Precondicion *******************
If m_oConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CCampoValorLista.EliminarDeBaseDatos", "No se ha establecido la conexion"
End If

'******************************************

On Error GoTo ERROR:

    TESError.NumError = TESnoerror

    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"

    If Not m_oCampo Is Nothing Then
        sConsulta = "DELETE FROM CAMPO_VALOR_LISTA WHERE CAMPO=" & m_oCampo.Id & " AND ORDEN=" & m_lOrden
    Else
        sConsulta = "DELETE FROM CAMPO_PREDEF_VALOR_LISTA WHERE CAMPO=" & m_oCampoPredef.Id & " AND ORDEN=" & m_lOrden
    End If
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR
    
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    IBaseDatos_EliminarDeBaseDatos = TESError
    Exit Function

ERROR:

    IBaseDatos_EliminarDeBaseDatos = basErrores.TratarError(m_oConexion.ADOCon.Errors)

    m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"

End Function

Private Function IBaseDatos_FinalizarEdicionEliminando() As TipoErrorSummit

End Function

Private Function IBaseDatos_FinalizarEdicionModificando() As TipoErrorSummit
End Function

Private Function IBaseDatos_IniciarEdicion(Optional ByVal Bloquear As Boolean, Optional ByVal UsuarioBloqueo As String) As TipoErrorSummit
Dim TESError As TipoErrorSummit
    
    TESError.NumError = TESnoerror
     
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CCampoValorLista.IniciarEdicion", "No se ha establecido la conexion"
    End If
    
    Set m_adores = New adodb.Recordset
    
    If Not m_oCampo Is Nothing Then
        m_adores.Open ("SELECT FECACT FROM CAMPO_VALOR_LISTA WITH (NOLOCK) WHERE CAMPO=" & m_oCampo.Id & " AND ORDEN = " & m_lOrden), m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
        
        If m_adores.eof Then
            m_adores.Close
            Set m_adores = Nothing
            TESError.NumError = TESDatoEliminado
            TESError.Arg1 = 164  ' Tabla CAMPO_VALOR_LISTA
            IBaseDatos_IniciarEdicion = TESError
            Exit Function
        End If
           
    Else
        m_adores.Open ("SELECT FECACT FROM CAMPO_PREDEF_VALOR_LISTA WITH (NOLOCK) WHERE CAMPO=" & m_oCampoPredef.Id & " AND ORDEN = " & m_lOrden), m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
        
        If m_adores.eof Then
            m_adores.Close
            Set m_adores = Nothing
            TESError.NumError = TESDatoEliminado
            TESError.Arg1 = 172  ' Tabla CAMPO_PREDEF_VALOR_LISTA
            IBaseDatos_IniciarEdicion = TESError
            Exit Function
        End If
    End If
    
    If m_vFecAct <> NullToStr(m_adores("FECACT")) Then
        m_adores.Close
        Set m_adores = Nothing
        TESError.NumError = TESInfActualModificada
    End If
    
    IBaseDatos_IniciarEdicion = TESError

End Function




