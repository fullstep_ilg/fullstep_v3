VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CEnlaceExtraPoint"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private mlOrden As Long
Private mX As Single
Private mY As Single

Public Property Get Orden() As Long
    Orden = mlOrden
End Property
Public Property Let Orden(ByVal Value As Long)
    mlOrden = Value
End Property

Public Property Get X() As Single
    X = mX
End Property
Public Property Let X(ByVal Value As Single)
    mX = Value
End Property

Public Property Get Y() As Single
    Y = mY
End Property
Public Property Let Y(ByVal Value As Single)
    mY = Value
End Property


