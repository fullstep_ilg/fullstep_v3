VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CProveERP"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private m_sCod As String
Private m_sDen As String
Private m_sNif As Variant
Private m_vCampo1 As Variant
Private m_vCampo2 As Variant
Private m_vCampo3 As Variant
Private m_vCampo4 As Variant
Private m_bBajaLogica As Boolean
Private m_vPag As Variant
Private m_vViaPag As Variant
Private m_vContacto As Variant



Public Property Let BajaLogica(ByVal bBajaLogica As Boolean)
    Let m_bBajaLogica = bBajaLogica
End Property
Public Property Get BajaLogica() As Boolean
    BajaLogica = m_bBajaLogica
End Property


Public Property Let Cod(ByVal sCod As String)
    Let m_sCod = sCod
End Property
Public Property Get Cod() As String
    Cod = m_sCod
End Property

Public Property Let Den(ByVal sDen As String)
    Let m_sDen = sDen
End Property
Public Property Get Den() As String
    Den = m_sDen
End Property


Public Property Let Nif(ByVal sNif As Variant)
    Let m_sNif = sNif
End Property
Public Property Get Nif() As Variant
    Nif = m_sNif
End Property

Public Property Let Campo1(ByVal vCampo1 As Variant)
    m_vCampo1 = vCampo1
End Property
Public Property Get Campo1() As Variant
    Campo1 = m_vCampo1
End Property

Public Property Let Campo2(ByVal vCampo2 As Variant)
    m_vCampo2 = vCampo2
End Property
Public Property Get Campo2() As Variant
    Campo2 = m_vCampo2
End Property

Public Property Let Campo3(ByVal vCampo3 As Variant)
    m_vCampo3 = vCampo3
End Property
Public Property Get Campo3() As Variant
    Campo3 = m_vCampo3
End Property

Public Property Let Campo4(ByVal vCampo4 As Variant)
    m_vCampo4 = vCampo4
End Property
Public Property Get Campo4() As Variant
    Campo4 = m_vCampo4
End Property

Public Property Let Pag(ByVal vPag As Variant)
    Let m_vPag = vPag
End Property
Public Property Get Pag() As Variant
    Pag = m_vPag
End Property

Public Property Let ViaPag(ByVal vViaPag As Variant)
    Let m_vViaPag = vViaPag
End Property
Public Property Get ViaPag() As Variant
    ViaPag = m_vViaPag
End Property
Public Property Let Contacto(ByVal vContacto As Variant)
    Let m_vContacto = vContacto
End Property
Public Property Get Contacto() As Variant
    Contacto = m_vContacto
End Property
