VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CVinculaciones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

Private mCol As Collection
Private moCampos As CCamposVinculado

Private m_oConexion As CConexion

    ''' * Objetivo: Dar valor a la conexion de la Destino
    ''' * Recibe: Conexion
    ''' * Devuelve: Nada
Friend Property Set Conexion(ByVal vData As CConexion)
    Set m_oConexion = vData
End Property

    ''' * Objetivo: Devolver la conexion de la Destino
    ''' * Recibe: Nada
    ''' * Devuelve: Conexion
Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property
    
''' <summary>
''' Establecer los campos vinculados
''' </summary>
''' <param name="vData">objeto con los campo</param>
''' <remarks>Llamada desde:frmFormularioVincularDesglose y frmFormularioVincularCampos; Tiempo m�ximo:0</remarks>
Public Property Set Campos(ByVal vData As CCamposVinculado)
    Set moCampos = vData
End Property

''' <summary>
''' Devolver los campos vinculados
''' </summary>
''' <remarks>Llamada desde:frmFormularioVincularDesglose y frmFormularioVincularCampos; Tiempo m�ximo:0</remarks>
Public Property Get Campos() As CCamposVinculado
    Set Campos = moCampos
End Property

    ''' * Objetivo: Recuperar un campo de la coleccion
    ''' * Recibe: Indice del campo a recuperar
    ''' * Devuelve: campo correspondiente
Public Property Get Item(vntIndexKey As Variant) As CVinculado
       
On Error GoTo NoSeEncuentra:

    ''' Devolvemos el item de la coleccion privada
    
    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:

    Set Item = Nothing
    
End Property

    ''' * Objetivo: Eliminar un campo de la coleccion
    ''' * Recibe: Indice del campo a eliminar
    ''' * Devuelve: Nada
Public Sub Remove(vntIndexKey As Variant)

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    mCol.Remove vntIndexKey
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CVinculaciones", "Remove", ERR, Erl)
      Exit Sub
   End If
End Sub

Public Property Get NewEnum() As IUnknown

    ''' ! Pendiente de revision, objetivo desconocido
    
    Set NewEnum = mCol.[_NewEnum]

End Property

''' * Objetivo: Crear la coleccion
Private Sub Class_Initialize()
    Set mCol = New Collection
End Sub

Private Sub Class_Terminate()
    Set mCol = Nothing
    Set m_oConexion = Nothing
End Sub

Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
    Count = mCol.Count
End If

End Property
''' <summary>
''' Devolver las vinculaciones de un desglose
''' </summary>
''' <param name="lDesgloseVinculado">Id de form_campo del desglose</param>
''' <remarks>Llamada desde:frmFormularioVincularDesglose; Tiempo m�ximo:0</remarks>
Public Sub CargarVinculados(ByVal lDesgloseVinculado As Long)
    Dim sConsulta As String
    Dim ador As adodb.Recordset
    

    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CVinculaciones.CargarVinculados", "No se ha establecido la conexion"
        Exit Sub
    End If
    
    If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
    Set ador = New adodb.Recordset
    
    sConsulta = "SELECT D.ID,D.CAMPO_VINCULADO,D.CAMPO_ORIGEN,D.SOLICITUD,D.FECACT" & _
        ",ISNULL('(' + FG.DEN_" & gParametrosInstalacion.gIdioma & " + ') ' + ORI.DEN_" & gParametrosInstalacion.gIdioma & ",'') DORI" & _
        ",S.DEN_" & gParametrosInstalacion.gIdioma & " SOL" & _
        " FROM DESGLOSE_VINCULADO D WITH (NOLOCK) " & _
        " LEFT JOIN FORM_CAMPO ORI WITH(NOLOCK) ON ORI.ID=D.CAMPO_ORIGEN AND ORI.BAJALOG=0 " & _
        " LEFT JOIN FORM_GRUPO FG WITH(NOLOCK) ON FG.ID=ORI.GRUPO AND FG.BAJALOG=0 " & _
        " INNER JOIN SOLICITUD S WITH(NOLOCK) ON S.ID=D.SOLICITUD" & _
        " WHERE CAMPO_VINCULADO=" & lDesgloseVinculado
    ador.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
               
    Set mCol = Nothing
    Set mCol = New Collection
               
    While Not ador.eof
        Me.Add ador.Fields("ID").Value, ador.Fields("SOLICITUD").Value, lDesgloseVinculado, ador.Fields("CAMPO_ORIGEN").Value, ador.Fields("FECACT").Value, ador.Fields("DORI").Value, ador.Fields("SOL").Value
        
        ador.MoveNext
    Wend
    
    ador.Close
    Set ador = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CVinculaciones", "CargarVinculados", ERR, Erl)
      Exit Sub
   End If
End Sub
''' <summary>
''' Devolver los campos vinculados de un desglose. Como puedes configurar los campos sin haber guardado la vinculaci�n a nivel solicitud-desglose
''' se pasa vDesgloseVinculadoId para:
'''         Entras en pantalla vincular desglose no aceptas pero cambias campos y aceptas. Ya hay DESGLOSE_VINCULADO.ID pero no lo tienes en el grid
'''         de pantalla vincular desglose, si vuelves a campos vDesgloseVinculadoId ="" cuando deber�a tener un valor
''' </summary>
''' <param name="vDesgloseVinculadoId">Id de DESGLOSE_VINCULADO de la vinculaci�n actual</param>
''' <param name="lIdDesgloseVinculado">Id de form_campo del desglose vinculado</param>
''' <param name="lIdDesgloseOrigen">Id de form_campo del desglose origen</param>
''' <remarks>Llamada desde:frmFormularioVincularCampos; Tiempo m�ximo:0</remarks>
Public Function CargarCamposVinculados(ByVal vDesgloseVinculadoId As Variant, ByVal lIdDesgloseVinculado As Long, ByVal lIdDesgloseOrigen As Long) As adodb.Recordset
    Dim sConsulta As String
    Dim ador As adodb.Recordset
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CFormItem.CargarCamposVinculados", "No se ha establecido la conexion"
        Exit Function
    End If
    
    If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
    Set ador = New adodb.Recordset
    
    sConsulta = "SELECT FORM_CAMPO.ID, FORM_CAMPO.TIPO_CAMPO_GS,FORM_CAMPO.TIPO, FORM_CAMPO.DEN_" & gParametrosInstalacion.gIdioma & " DEN " & _
        " , FORM_CAMPO.TABLA_EXTERNA, DEF_ATRIB.COD COD_ATRIB, DEF_ATRIB.DEN DEN_ATRIB,LV.CAMPO_HIJO_ORIGEN, FLV.DEN_" & gParametrosInstalacion.gIdioma & " DENLV " & _
        " ," & IIf(DblToSQLFloat(vDesgloseVinculadoId) = "NULL", "DV.ID", DblToSQLFloat(vDesgloseVinculadoId)) & " DVID, DV.SOLICITUD,DV.CAMPO_VINCULADO,ISNULL(DV.CAMPO_ORIGEN,0) CAMPO_ORIGEN, FORM_CAMPO.SUBTIPO, DEF_ATRIB.ID ID_ATRIB, FORM_CAMPO.INTRO" & _
        " FROM FORM_CAMPO WITH (NOLOCK) " & _
        " LEFT JOIN DEF_ATRIB WITH (NOLOCK) ON FORM_CAMPO.ID_ATRIB_GS = DEF_ATRIB.ID " & _
        " INNER JOIN DESGLOSE WITH (NOLOCK) ON DESGLOSE.CAMPO_PADRE=" & lIdDesgloseVinculado & " AND FORM_CAMPO.ID=DESGLOSE.CAMPO_HIJO "
    If DblToSQLFloat(vDesgloseVinculadoId) = "NULL" Then
        'Entras en pantalla vincular desglose no aceptas pero cambias campos y aceptas. Ya hay DESGLOSE_VINCULADO.ID pero no lo tienes en el grid
        'de pantalla vincular desglose, si vuelves a campos vDesgloseVinculadoId ="" cuando deber�a tener un valor
        sConsulta = sConsulta & " LEFT JOIN DESGLOSE_VINCULADO DV WITH(NOLOCK) ON DV.CAMPO_VINCULADO=DESGLOSE.CAMPO_PADRE AND DV.CAMPO_ORIGEN=" & lIdDesgloseOrigen
    Else
        sConsulta = sConsulta & " LEFT JOIN DESGLOSE_VINCULADO DV WITH(NOLOCK) ON DV.ID=" & DblToSQLFloat(vDesgloseVinculadoId) & " AND DV.CAMPO_VINCULADO=DESGLOSE.CAMPO_PADRE AND DV.CAMPO_ORIGEN=" & lIdDesgloseOrigen
    End If
        
    sConsulta = sConsulta & " LEFT JOIN LINEA_DESGLOSE_VINCULADO LV WITH(NOLOCK) ON LV.DESGLOSE_VINCULADO=DV.ID AND LV.CAMPO_HIJO_VINCULADO=FORM_CAMPO.ID " & _
        " LEFT JOIN FORM_CAMPO FLV WITH(NOLOCK) ON FLV.ID=LV.CAMPO_HIJO_ORIGEN" & _
        " WHERE (FORM_CAMPO.TIPO < " & TipoCampoPredefinido.Calculado & " OR FORM_CAMPO.TIPO =" & TipoCampoPredefinido.externo & ") AND FORM_CAMPO.BAJALOG=0 "
    
    sConsulta = sConsulta & " ORDER BY FORM_CAMPO.ORDEN"
    
    ador.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    
    Set ador.ActiveConnection = Nothing

    Set CargarCamposVinculados = ador
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CVinculaciones", "CargarCamposVinculados", ERR, Erl)
      Exit Function
   End If
End Function

''' <summary>
''' Crear un objeto vinculacion entre desgloses
''' </summary>
''' <param name="vId">DESGLOSE_VINCULADO.ID</param>
''' <param name="lSolicitud">Solicitud</param>
''' <param name="lDesgloseVinculado">Id de form_campo de Desglose Vinculado</param>
''' <param name="vDesgloseOrigen">Id de form_campo de Desglose origen, de haberlo</param>
''' <param name="FecAct">Fecha ultima grabaci�n</param>
''' <param name="DescrDesglOrigen">Descr del Desglose origen</param>
''' <param name="DescrSolic">Descr de Solicitud</param>
''' <param name="vIndice">si se usa indice o no</param>
''' <returns>Un objeto CVinculado</returns>
''' <remarks>Llamada desde: frmFormularioVincularDesglose; Tiempo m�ximo: 0,1</remarks>
Public Function Add(ByVal vId As Variant, ByVal lSolicitud As Long, ByVal lDesgloseVinculado As Long, ByVal vDesgloseOrigen As Variant, Optional ByVal FecAct As Variant _
, Optional ByVal DescrDesglOrigen As Variant, Optional ByVal DescrSolic As Variant, Optional ByVal vIndice As Variant) As CVinculado

    Dim objnewmember As CVinculado
    
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New CVinculado
    Set objnewmember.Conexion = m_oConexion
    
    objnewmember.Id = vId
    objnewmember.Solicitud = lSolicitud
    objnewmember.DesgloseVinculado = lDesgloseVinculado
    If IsNumeric(vDesgloseOrigen) Then
        objnewmember.DesgloseOrigen = vDesgloseOrigen
    Else
        objnewmember.DesgloseOrigen = Null
    End If
    If Not IsMissing(FecAct) Then
        objnewmember.FecAct = FecAct
    Else
        objnewmember.FecAct = Null
    End If
    If Not IsMissing(DescrDesglOrigen) Then
        objnewmember.DescrDesgloseOrigen = DescrDesglOrigen
    Else
        objnewmember.DescrDesgloseOrigen = ""
    End If
    If Not IsMissing(DescrSolic) Then
        objnewmember.DescrSolicitud = DescrSolic
    Else
        objnewmember.DescrSolicitud = ""
    End If
    
    If Not IsMissing(vIndice) And Not IsNull(vIndice) Then
        objnewmember.Indice = vIndice
        mCol.Add objnewmember, CStr(vIndice)
    Else
        mCol.Add objnewmember, CStr(lDesgloseVinculado) & "#" & CStr(lSolicitud) & "#" & NullToStr(vDesgloseOrigen)
    End If

    Set Add = objnewmember
    
    Set objnewmember = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CVinculaciones", "Add", ERR, Erl)
      Exit Function
   End If
End Function
''' <summary>
''' Grabar las vinculaciones del desglose en curso
''' </summary>
''' <returns>Error de haberlo</returns>
''' <remarks>Llamada desde: frmFormularioVincularDesglose; Tiempo m�ximo: 0,1</remarks>
Public Function GrabarVinculaciones() As TipoErrorSummit
    Dim udtTESError As TipoErrorSummit
    Dim oVinculado As CVinculado
    Dim sConsulta As String
    Dim ador As adodb.Recordset
    Dim lDVID As Long
    
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    udtTESError.NumError = TESnoerror
    
    On Error GoTo Error_Cls
    
    For Each oVinculado In mCol
        If Not (oVinculado.Id = "") Then
            sConsulta = "UPDATE DESGLOSE_VINCULADO SET SOLICITUD=" & oVinculado.Solicitud & ",CAMPO_ORIGEN=" & DblToSQLFloat(oVinculado.DesgloseOrigen)
            sConsulta = sConsulta & " WHERE ID=" & oVinculado.Id
            
            m_oConexion.ADOCon.Execute sConsulta
            
            'updatar a null los origenes de LINEA_DESGLOSE_VINCULADO, si es necesario, lo hace [DESGLOSE_VINCULADO_TG_UPDCAMPOS]
            'Si a una vinculacion ya existente se le modifica el campo_origen y antes de grabar se le cambian
            'los campos. Se actualizara de DESGLOSE_VINCULADO el campo CAMPO_ORIGEN y en consecuencia el trigger
            'no detectara cambios en campo_origen y no updatara.
        Else
            'Metes la vinculaci�n a otras solicitudes y ya puedes vincular campos. Si vinculas campos se graba DESGLOSE_VINCULADO
            Set ador = New adodb.Recordset
            sConsulta = "SELECT 1 FROM DESGLOSE_VINCULADO WITH(NOLOCK)"
            sConsulta = sConsulta & " WHERE CAMPO_VINCULADO=" & oVinculado.DesgloseVinculado & " AND SOLICITUD=" & oVinculado.Solicitud
            sConsulta = sConsulta & " AND CAMPO_ORIGEN=" & DblToSQLFloat(oVinculado.DesgloseOrigen)
            ador.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
        
            If ador.eof Then
                sConsulta = "INSERT INTO DESGLOSE_VINCULADO (SOLICITUD,CAMPO_VINCULADO,CAMPO_ORIGEN) VALUES"
                sConsulta = sConsulta & "(" & oVinculado.Solicitud & "," & oVinculado.DesgloseVinculado & "," & DblToSQLFloat(oVinculado.DesgloseOrigen) & ")"
                
                m_oConexion.ADOCon.Execute sConsulta
                
                Set ador = New adodb.Recordset
                'Sin with(nolock) pq interesa el recien insertado
                sConsulta = "SELECT MAX(ID) DVID FROM DESGLOSE_VINCULADO"
                ador.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
                
                lDVID = ador.Fields("DVID").Value
                
                sConsulta = "INSERT INTO LINEA_DESGLOSE_VINCULADO(DESGLOSE_VINCULADO,CAMPO_HIJO_VINCULADO,CAMPO_HIJO_ORIGEN)"
                sConsulta = sConsulta & " SELECT " & lDVID & ",D.CAMPO_HIJO,NULL"
                sConsulta = sConsulta & " FROM DESGLOSE D WITH(NOLOCK) WHERE D.CAMPO_PADRE=" & oVinculado.DesgloseVinculado
                
                m_oConexion.ADOCon.Execute sConsulta
            End If
        End If
    Next
    
    GrabarVinculaciones = udtTESError
    Exit Function

Error_Cls:
    udtTESError = basErrores.TratarError(m_oConexion.ADOCon.Errors)
        
    GrabarVinculaciones = udtTESError
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CVinculaciones", "GrabarVinculaciones", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If
End Function
''' <summary>
''' Eliminar las vinculaciones del desglose en curso
''' </summary>
''' <param name="Codigos">vinculaciones a eliminar</param>
''' <returns>Error de haberlo</returns>
''' <remarks>Llamada desde: frmFormularioVincularDesglose; Tiempo m�ximo: 0,1</remarks>
Public Function EliminarVinculaciones(ByVal Codigos As Variant) As TipoErrorSummit
    Dim i As Integer
    Dim udtTESError As TipoErrorSummit
    Dim ador As adodb.Recordset
    
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    udtTESError.NumError = TESnoerror
        
    On Error GoTo Error_Cls
        
    For i = 1 To UBound(Codigos)
        m_oConexion.ADOCon.Execute "DELETE FROM LINEA_DESGLOSE_VINCULADO WHERE DESGLOSE_VINCULADO=" & CLng(Codigos(i))
        
        m_oConexion.ADOCon.Execute "DELETE FROM DESGLOSE_VINCULADO WHERE ID=" & CLng(Codigos(i))
    Next
    
    Set ador = New adodb.Recordset
'    sConsulta = "IF (SELECT COUNT(1) FROM DESGLOSE_VINCULADO WHERE CAMPO_VINCULADO=" & XX & ")=0 BEGIN "
    
    EliminarVinculaciones = udtTESError
    Exit Function

Error_Cls:
    udtTESError = basErrores.TratarError(m_oConexion.ADOCon.Errors)
        
    EliminarVinculaciones = udtTESError
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CVinculaciones", "EliminarVinculaciones", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If
End Function

