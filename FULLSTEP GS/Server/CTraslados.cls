VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CTraslados"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

''' *** Clase: CTraslados
''' *** Creacion: 11/06/2004 (MMV)

''' Variables que almacenan las propiedades de los objetos
Private m_Col As Collection
Private m_bEOF As Boolean
Private m_oConexion As CConexion

''' Control de errores
Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum


'--<summary>
'--Crea un objeto de tipo Traslado y lo a�ade a la colecci�n.
'--</summary>
'--<param name="dFecCierre">Fecha de cierre</param>
'--<param name="sProve">Proveedor</param>
'--<param name="sProveERP">Proveedor ERP</param>
'--<param name="oProceso">objeto Proceso</param>
'--<param name="lEmpresa">Empresa</param>
'--<param optional name="lID">Identificador</param>
'--<param name="varIndice"></param>
'--<returns>Devuelve el objeto Traslado tras a�adirlo a la colecci�n</returns>
'--<remarks>Llamada desde frmADJTraspasoERP</remarks>
'--<revision>JVS 21/11/2011</revision>

Public Function Add(ByVal dFecCierre As Date, ByVal sProve As String, ByVal sProveERP As String, ByVal oProceso As cProceso, _
                    ByVal lEmpresa As Long, Optional ByVal lId As Long, Optional ByVal varIndice As Variant, Optional ByVal varEstado As Variant) As CTraslado
                    
    Dim objnewmember As CTraslado
    Dim sCod1 As String
    Dim sCod2 As String
    
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New CTraslado
    
    objnewmember.Prove = sProve
    objnewmember.ProveERP = sProveERP
    objnewmember.Empresa = lEmpresa
    
    If Not oProceso Is Nothing Then
        Set objnewmember.Proceso = oProceso
    End If
    
    objnewmember.FecCierre = dFecCierre
    objnewmember.Id = lId
    If IsMissing(varEstado) Then
        objnewmember.Estado = 0
    Else
        objnewmember.Estado = varEstado
    End If
    
    Set objnewmember.Conexion = m_oConexion
        
    If Not IsMissing(varIndice) And Not IsNull(varIndice) Then
        objnewmember.Indice = varIndice
        m_Col.Add objnewmember, CStr(varIndice)
        
    Else
        sCod1 = CStr(sProve) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(CStr(sProve)))
        sCod2 = CStr(lEmpresa)
        
        m_Col.Add objnewmember, sCod1 & sCod2
    End If
            
    Set Add = objnewmember
    
    Set objnewmember = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CTraslados", "Add", ERR, Erl)
      Exit Function
   End If
    
End Function
                    


Public Property Get Item(vntIndexKey As Variant) As CTraslado

On Error GoTo NoSeEncuentra:

    ''' Devolvemos el item de la coleccion privada
    
    Set Item = m_Col(vntIndexKey)
     
    Exit Property

NoSeEncuentra:

    Set Item = Nothing
    
End Property
Public Property Get eof() As Boolean

    ''' * Objetivo: Devolver variable privada EOF
    ''' * Recibe: Nada
    ''' * Devuelve: Variable privada EOF
    
    eof = m_bEOF
    
End Property
Friend Property Set Conexion(ByVal con As CConexion)

    ''' * Objetivo: Dar valor a la variable privada Conexion
    ''' * Recibe: Conexion
    ''' * Devuelve: Nada
    
    Set m_oConexion = con
    
End Property
Friend Property Get Conexion() As CConexion

    ''' * Objetivo: Devolver variable privada Conexion
    ''' * Recibe: Nada
    ''' * Devuelve: Conexion
    
    Set Conexion = m_oConexion
    
End Property
Public Property Get Count() As Long

    ''' * Objetivo: Devolver variable privada Count
    ''' * Recibe: Nada
    ''' * Devuelve: Numero de elementos de la coleccion

    If m_Col Is Nothing Then
        Count = 0
    Else
        Count = m_Col.Count
    End If

End Property
Public Sub Remove(vntIndexKey As Variant)

    ''' * Objetivo: Eliminar una solicitud de la coleccion
    ''' * Recibe: Indice de la solicitud a eliminar
    ''' * Devuelve: Nada
   
    m_Col.Remove vntIndexKey
    
End Sub
Private Sub Class_Initialize()

    ''' * Objetivo: Crear la coleccion

    Set m_Col = New Collection
    
End Sub
Private Sub Class_Terminate()

    ''' * Objetivo: Limpiar memoria
    Set m_Col = Nothing
    Set m_oConexion = Nothing
End Sub
Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4

    ''' ! Pendiente de revision, objetivo desconocido
    
    Set NewEnum = m_Col.[_NewEnum]

End Property


''' <summary>
''' Esta funci�n obtiene el estado de integraci�n del proceso.
''' Consulta el estado de LOG_GRAL por cada empresa y proveedor del proceso.
''' Filtra por traspasos posteriores a la �ltima reapertura del proceso (LOG_ADJ.ACCION=D), y recupera el estado del �ltimo traspaso (LOG_ADJ.ACCION=I) para ese proceso-empresa-proveedor
''' </summary>
''' <param name="oProce"> Objeto de la clase CProceso (se utiliza para obtener el ANYO, GMN1 y COD del proceso)</param>
''' <returns>Recordset con los diferentes estados de la integraci�n de las adjudicaciones del proceso</returns>
''' <remarks>Llamada desde: FSGSClient.frmRESREU.ProcesoSeleccionado, FSGSClient.frmRESREU.ReconfigurarProcesoCerrado y FSGSClient.frmADJ.ProcesoSeleccionado </remarks>
''' <remarks>Tiempo m�ximo: < 0,1seg </remarks>
''' <remarks>Revisado por: mmv(22/11/2011) </remarks>
Public Function DevolverEstadoTraslado(ByVal oProce As cProceso) As adodb.Recordset
Dim TESError As TipoErrorSummit
Dim sConsulta As String
Dim AdoRes As New adodb.Recordset

'********* Precondicion *******************
If m_oConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CTraslados.DevolverEstadoTraslado", "No se ha establecido la conexion"
End If
'******************************************

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
    TESError.NumError = TESnoerror
    
    sConsulta = " SELECT ESTADO FROM PROCE_INT WITH(NOLOCK) WHERE BAJA=0 AND ANYO=" & oProce.Anyo & _
        " AND GMN1='" & DblQuote(oProce.GMN1Cod) & "' AND PROCE=" & oProce.Cod & _
        " AND PROCE_INT.PROVE IS NULL AND PROCE_INT.EMPRESA IS NULL AND PROCE_INT.GRUPO IS NULL"
    
    AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly

    AdoRes.ActiveConnection = Nothing
    
    Set DevolverEstadoTraslado = AdoRes

    Exit Function
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CTraslados", "DevolverEstadoTraslado", ERR, Erl)
      Exit Function
   End If
    
End Function

Public Function DevolverEstadoPedidosDirectos(ByVal oProce As cProceso) As adodb.Recordset
Dim TESError As TipoErrorSummit
Dim sConsulta As String
Dim AdoRes As New adodb.Recordset

'********* Precondicion *******************
If m_oConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CTraslados.DevolverEstadoPedidosDirectos", "No se ha establecido la conexion"
End If
'******************************************

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
   
    TESError.NumError = TESnoerror
        
    sConsulta = "SELECT TOP 1 LOG_GRAL.ESTADO FROM LOG_GRAL WITH (NOLOCK) "
    sConsulta = sConsulta & " INNER JOIN LOG_ORDEN_ENTREGA WITH (NOLOCK) ON LOG_ORDEN_ENTREGA.ID=LOG_GRAL.ID_TABLA AND TABLA=" & EntidadIntegracion.PED_directo
    sConsulta = sConsulta & " INNER JOIN LINEAS_PEDIDO WITH (NOLOCK) ON LINEAS_PEDIDO.ORDEN = LOG_ORDEN_ENTREGA.ID_ORDEN_ENTREGA"
    sConsulta = sConsulta & " WHERE LINEAS_PEDIDO.ANYO=" & oProce.Anyo & " AND LINEAS_PEDIDO.GMN1='" & DblQuote(oProce.GMN1Cod) & "' AND LINEAS_PEDIDO.PROCE=" & oProce.Cod & " AND LOG_ORDEN_ENTREGA.ORIGEN =" & OrigenIntegracion.FSGSInt & " AND (LOG_ORDEN_ENTREGA.ACCION='" & Accion_Alta & "' OR LOG_ORDEN_ENTREGA.ACCION='" & Accion_Modificacion & "' OR LOG_ORDEN_ENTREGA.ACCION='" & Accion_Reenvio & "') ORDER BY LOG_GRAL.FECACT DESC "
    AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly

    AdoRes.ActiveConnection = Nothing
    
    Set DevolverEstadoPedidosDirectos = AdoRes

    Exit Function
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CTraslados", "DevolverEstadoPedidosDirectos", ERR, Erl)
      Exit Function
   End If
    
End Function


        

