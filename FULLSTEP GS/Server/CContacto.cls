VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CContacto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CContacto **********************************
'*             Autor : Javier Arana
'*             Creada : 8/12/98
'* **************************************************************

Option Explicit

Private mvarId As Integer
Private mvarTfnoMovil As Variant
Private mvarNombre As Variant 'local copy
Private mvarApellidos As String 'local copy
Private mvarCargo As Variant 'local copy
Private mvarTfno As Variant 'local copy
Private mvarTfno2 As Variant 'local copy
Private mvarFax As Variant 'local copy
Private mvarMail As Variant 'local copy
Private mvarNif As Variant 'local copy
Private mvarDepartamento As Variant
Private mvarDef As Boolean 'Indica si es el contacto por defecto para el envio de peticiones de oferta
Private mvarPort As Boolean 'Indica si es el contacto proviene del portal
Private mvarIndice As Long
Private mvarAprovisionador As Boolean 'Indica si el contacto es aprovisionador
Private mvarNotifSubasta As Boolean
'*********** INTEGRACION ******
Private m_vEstIntegracion As Variant ' Estado integración

Private miTipoEmail As Variant
Private msIdioma As Variant
Private mbCalidad As Boolean

Private mbPrincipal As Boolean

Public Property Let TipoEmail(ByVal vData As Variant)
miTipoEmail = vData
End Property

Public Property Get TipoEmail() As Variant
TipoEmail = miTipoEmail
End Property

Public Property Let Idioma(ByVal vData As Variant)
msIdioma = vData
End Property

Public Property Get Idioma() As Variant
Idioma = msIdioma
End Property


Public Property Let EstadoIntegracion(ByVal vData As Variant)

    ''' * Objetivo: Dar valor al estado de integración del contacto
    ''' * Recibe: el valor
    ''' * Devuelve: Nada

    m_vEstIntegracion = vData
    
End Property
Public Property Get EstadoIntegracion() As Variant

    ''' * Objetivo: Devolver el estado de integración del proveedor
    ''' * Recibe: Nada
    ''' * Devuelve: El estado de integración del contacto

    EstadoIntegracion = m_vEstIntegracion
    
End Property



Public Property Get Port() As Boolean
  Port = mvarPort
End Property
Public Property Let Port(ByVal vData As Boolean)
    Let mvarPort = vData
End Property

Public Property Get Def() As Boolean
  Def = mvarDef
End Property
Public Property Let Def(ByVal vData As Boolean)
    Let mvarDef = vData
End Property
Public Property Get Id() As Integer
  Id = mvarId
End Property
Public Property Let Id(ByVal vData As Integer)
    Let mvarId = vData
End Property

Public Property Let Departamento(ByVal vData)
    mvarDepartamento = vData
End Property

Public Property Get Departamento() As Variant
    Departamento = mvarDepartamento
End Property

Public Property Let Mail(ByVal vData)
    mvarMail = vData
End Property

Public Property Get Mail() As Variant
    Mail = mvarMail
End Property

Public Property Let NIF(ByVal vData)
    mvarNif = vData
End Property

Public Property Get NIF() As Variant
    NIF = mvarNif
End Property

Public Property Let Fax(ByVal vData)
    mvarFax = vData
End Property
Public Property Get Fax() As Variant
    Fax = mvarFax
End Property

Public Property Let Tfno(ByVal vData)
    mvarTfno = vData
End Property
Public Property Get Tfno() As Variant
    Tfno = mvarTfno
End Property
Public Property Let Tfno2(ByVal vData)
    mvarTfno2 = vData
End Property
Public Property Get Tfno2() As Variant
    Tfno2 = mvarTfno2
End Property

Public Property Let Cargo(ByVal vData)
    mvarCargo = vData
End Property
Public Property Get Cargo() As Variant
    Cargo = mvarCargo
End Property

Public Property Let Nombre(ByVal vData)
    mvarNombre = vData
End Property
Public Property Get Nombre() As Variant
    Nombre = mvarNombre
End Property

Public Property Let Apellidos(ByVal vData As String)
    mvarApellidos = vData
End Property
Public Property Get Apellidos() As String
   Apellidos = mvarApellidos
End Property

Public Property Get Indice() As Long
    Indice = mvarIndice
End Property
Public Property Let Indice(ByVal Ind As Long)
    mvarIndice = Ind
End Property

Public Property Get tfnomovil() As Variant
    tfnomovil = mvarTfnoMovil
End Property
Public Property Let tfnomovil(ByVal vData)
    mvarTfnoMovil = vData
End Property

Public Property Get Aprovisionador() As Boolean
    Aprovisionador = mvarAprovisionador
End Property
Public Property Let Aprovisionador(ByVal vData As Boolean)
    mvarAprovisionador = vData
End Property

Public Property Get NotifSubasta() As Boolean
    NotifSubasta = mvarNotifSubasta
End Property
Public Property Let NotifSubasta(ByVal vNotif As Boolean)
    mvarNotifSubasta = vNotif
End Property

Public Property Get Calidad() As Boolean
    Calidad = mbCalidad
End Property
Public Property Let Calidad(ByVal vNotif As Boolean)
    mbCalidad = vNotif
End Property

Public Property Get Principal() As Boolean
    Principal = mbPrincipal
End Property
Public Property Let Principal(ByVal vPrincipal As Boolean)
    mbPrincipal = vPrincipal
End Property

