VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CNotificacionPedido"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit


Private mvarId As Variant
Private mvarPedido As Variant
Private mvarOrden As Variant
Private mvarCodProve As String
Private mvarDenProve As String
Private mvarCon As Variant
Private mvarApellidos As String
Private mvarNombre As String
Private mvarEMail As String
Private mvarTipo As Integer
Private mvarFecha As Date
Private mvarWeb As Boolean
Private mvarMail As Boolean
Private mvarCarta As Boolean
Private mvarFormatoPedido As Variant
Private mvarIndice As Variant
Private m_lIDRegistroMail As Long    'Aqu� guardamos el ID de REGISTRO_EMAIL
Private m_bFallido As Boolean        'Aqu� guardamos si el envio ha sido fallido o no
Private m_bMailCancel As Boolean     'Aqu� guardamos si se cancela el mail o no

''' Conexion

Private mvarConexion As CConexion


''' Control de errores

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

Friend Property Set Conexion(ByVal con As CConexion)
Set mvarConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = mvarConexion
End Property
Public Property Get ID() As Variant
    ID = mvarId
End Property

Public Property Let ID(ByVal Data As Variant)
    mvarId = Data
End Property
Public Property Get Pedido() As Variant
    Pedido = mvarPedido
End Property

Public Property Let Pedido(ByVal Data As Variant)
    mvarPedido = Data
End Property
Public Property Get Orden() As Variant
    Orden = mvarOrden
End Property

Public Property Let Orden(ByVal Data As Variant)
    mvarOrden = Data
End Property
Public Property Get CodProve() As String
    CodProve = mvarCodProve
End Property

Public Property Let CodProve(ByVal Data As String)
    mvarCodProve = Data
End Property
Public Property Get DenProve() As String
    DenProve = mvarDenProve
End Property

Public Property Let DenProve(ByVal Data As String)
    mvarDenProve = Data
End Property
Public Property Get con() As Variant
    con = mvarCon
End Property
Public Property Let con(ByVal Data As Variant)
    mvarCon = Data
End Property
Public Property Get Apellidos() As String
    Apellidos = mvarApellidos
End Property

Public Property Let Apellidos(ByVal Data As String)
    mvarApellidos = Data
End Property
Public Property Get Nombre() As String
    Nombre = mvarNombre
End Property

Public Property Let Nombre(ByVal Data As String)
    mvarNombre = Data
End Property
Public Property Get Email() As String
    Email = mvarEMail
End Property

Public Property Let Email(ByVal Data As String)
    mvarEMail = Data
End Property
Public Property Get Tipo() As Integer
    Tipo = mvarTipo
End Property

Public Property Let Tipo(ByVal Data As Integer)
    mvarTipo = Data
End Property
Public Property Get Fecha() As String
    Fecha = mvarFecha
End Property

Public Property Let Fecha(ByVal Data As String)
    mvarFecha = Data
End Property
Public Property Get Web() As Boolean
    Web = mvarWeb
End Property

Public Property Let Web(ByVal Data As Boolean)
    mvarWeb = Data
End Property
Public Property Get Mail() As Boolean
    Mail = mvarMail
End Property

Public Property Let Mail(ByVal Data As Boolean)
    mvarMail = Data
End Property
Public Property Get Carta() As Boolean
    Carta = mvarCarta
End Property

Public Property Let Carta(ByVal Data As Boolean)
    mvarCarta = Data
End Property
Public Property Get FormatoPedido() As Variant
    FormatoPedido = mvarFormatoPedido
End Property
Public Property Let FormatoPedido(ByVal Data As Variant)
    mvarFormatoPedido = Data
End Property
Public Property Get Indice() As Variant
    Indice = mvarIndice
End Property
Public Property Let Indice(ByVal Data As Variant)
    mvarIndice = Data
End Property

Public Property Get IDRegistroMail() As Long
    IDRegistroMail = m_lIDRegistroMail
End Property
Public Property Let IDRegistroMail(ByVal Data As Long)
    m_lIDRegistroMail = Data
End Property

Public Property Get Fallido() As Boolean
    Fallido = m_bFallido
End Property
Public Property Let Fallido(ByVal Data As Boolean)
    m_bFallido = Data
End Property

Public Property Get MailCancel() As Boolean
    MailCancel = m_bMailCancel
End Property
Public Property Let MailCancel(ByVal Data As Boolean)
    m_bMailCancel = Data
End Property

Private Sub Class_Terminate()
    Set mvarConexion = Nothing
End Sub


''' <summary>
''' Actualiza la comunicaci�n con el ultimo id del mail enviado y la fecha.
''' </summary>
''' <param name></param>
''' <returns>Devuelve si se ha producido error</returns>
''' <remarks>Llamada desde:frmSeguimComun.ReenviarMail(); Tiempo m�ximo: 0.2 seg</remarks>
''' <revision>LTG 09/01/2012</revision>

Public Function ActualizarComunicacion() As TipoErrorSummit
    Dim AdoRes As New ADODB.Recordset
    Dim sConsulta As String
    Dim TESError As TipoErrorSummit

    TESError.NumError = TESnoerror
    On Error GoTo Error:
    
    mvarConexion.BEGINTRANSACTION
    
    sConsulta = "UPDATE ORDEN_COMUNIC SET REGISTRO_EMAIL=" & m_lIDRegistroMail & ", ERROR=" & BooleanToSQLBinary(m_bFallido) & ", FECHA=(SELECT FECHA FROM REGISTRO_EMAIL WITH (NOLOCK) WHERE ID=" & m_lIDRegistroMail & ") WHERE ID=" & mvarId
    mvarConexion.ADOCon.Execute sConsulta
    
    'marcar el pedido como notificado
    sConsulta = "UPDATE ORDEN_ENTREGA SET NOTIFICADO=" & OrdenEntregaNotificacion.Notificado & " WHERE ID=" & mvarOrden
    mvarConexion.ADOCon.Execute sConsulta
    
    mvarConexion.COMMITTRANSACTION
    
    If mvarConexion.ADOCon.Errors.Count > 0 Then
        GoTo Error
    End If
    
Salir:
    ActualizarComunicacion = TESError
    Exit Function
Error:
    TESError = basErrores.TratarError(mvarConexion.ADOCon.Errors)
    mvarConexion.ROLLBACKTRANSACTION
    Resume Salir
End Function


''' <summary>Realiza la comunicaci�n de la notificaci�n de un pedido.</summary>
''' <param name></param>
''' <returns>El error si se produce.</returns>
''' <remarks>Llamada desde: frmPedidosEmision1; Tiempo m�ximo:2sg</remarks>

Public Function RealizarComunicacion() As TipoErrorSummit
    Dim sConsulta As String
    Dim TESError As TipoErrorSummit
    
    '********* Precondicion *******************
    If mvarConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CNotificacionPedido.RealizarComunicacion", "No se ha establecido la conexion"
    End If
    '******************************************
    
    On Error GoTo Error:
        
    TESError.NumError = TESnoerror

    mvarConexion.BEGINTRANSACTION

    sConsulta = "INSERT INTO ORDEN_COMUNIC (PEDIDO, ORDEN, APE, NOM, EMAILDIR, TIPO, FECHA, WEB, EMAIL, IMP, REGISTRO_EMAIL, ERROR) VALUES (" & mvarPedido & ", " & mvarOrden & ", '" & DblQuote(mvarApellidos) & "', " & StrToSQLNULL(mvarNombre) & ", " & StrToSQLNULL(mvarEMail) & ", " & mvarTipo & ", " & DateToSQLDate(Date) & ", " & BooleanToSQLBinary(mvarWeb) & ", " & BooleanToSQLBinary(mvarMail) & ", " & BooleanToSQLBinary(mvarCarta) & ", " & m_lIDRegistroMail & ", " & BooleanToSQLBinary(m_bFallido) & ")"
    mvarConexion.ADOCon.Execute sConsulta
    If mvarConexion.ADOCon.Errors.Count > 0 Then GoTo Error
    
    'marcar el pedido como notificado
    sConsulta = "UPDATE ORDEN_ENTREGA SET NOTIFICADO=" & OrdenEntregaNotificacion.Notificado & " WHERE ID=" & mvarOrden
    mvarConexion.ADOCon.Execute sConsulta
    If mvarConexion.ADOCon.Errors.Count > 0 Then GoTo Error
    
    mvarConexion.COMMITTRANSACTION
        
Salir:
    RealizarComunicacion = TESError
    Exit Function
Error:
    TESError = basErrores.TratarError(mvarConexion.ADOCon.Errors)
    mvarConexion.ROLLBACKTRANSACTION
    Resume Salir
End Function

