VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CAcciones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Collection" ,"CAccion"
Attribute VB_Ext_KEY = "Member0" ,"CAccion"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum

Private mCol As Collection
Private mvarConexion As CConexion

Friend Property Set Conexion(ByVal con As CConexion)
    Set mvarConexion = con
End Property

Friend Property Get Conexion() As CConexion
    Set Conexion = mvarConexion
End Property

Public Function Add(ByVal Id As Long, ByVal Den As String, Optional ByVal lOrden As Long, Optional ByVal bUsarOrden As Boolean = False) As CAccion
    Dim oAccion As CAccion
    Dim sClave As String
    
    On Error GoTo ElementoRepetido
    
    Set oAccion = New CAccion
    
    Select Case bUsarOrden
        Case True
            sClave = CStr(lOrden)
        Case False
            sClave = CStr(Id)
    End Select
    
    oAccion.Id = Id
    oAccion.Den = Den
    oAccion.Orden = lOrden
    
    mCol.Add oAccion, sClave
    Set Add = oAccion
    Set oAccion = Nothing
    Exit Function
    
ElementoRepetido:
    Set Add = Nothing

End Function

Public Property Get Item(vntIndexKey As Variant) As CAccion
Attribute Item.VB_UserMemId = 0
 On Error GoTo vacio:
 
  Set Item = mCol(vntIndexKey)
  Exit Property

vacio:
  Set Item = Nothing

End Property



Public Property Get Count() As Long
    If mCol Is Nothing Then
        Count = 0
    Else
        Count = mCol.Count
    End If
End Property


Public Sub Remove(vntIndexKey As Variant)
On Error GoTo NoExisteLaClave:
    
    mCol.Remove vntIndexKey

NoExisteLaClave:
Resume Next

End Sub


Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
    Set NewEnum = mCol.[_NewEnum]
End Property


Private Sub Class_Initialize()

  
    Set mCol = New Collection
End Sub


Private Sub Class_Terminate()
    Set mCol = Nothing
    Set mvarConexion = Nothing
End Sub

''' <summary>Elimina acciones</summary>
''' <param name="sCodUsu">Cod. usuario</param>
''' <returns>Var. con el error si se ha producido</returns>
''' <remarks>Llamada desde: CUsuario.IBaseDatos_FinalizarEdicionModificando</remarks>
''' <revision>LTG 03/01/2013</revision>

Public Function EliminarAccionesUsuario(ByVal sCodUsu As String) As TipoErrorSummit
    Dim TESError As TipoErrorSummit
    Dim oComm As adodb.Command
    Dim oParam As adodb.Parameter
    Dim sConsulta As String
        
    
    TESError.NumError = TESnoerror
    
    '******************* Precondicion *******************
    If mvarConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CAcciones.EliminarAccionesUsuario", "No se ha establecido la conexion"
    End If
    
    If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:

    Set oComm = New adodb.Command
    With oComm
        sConsulta = "DELETE FROM USU_ACC WHERE USU=?"
        
        .ActiveConnection = mvarConexion.ADOCon
        .CommandType = adCmdText
        .CommandText = sConsulta
        
        Set oParam = .CreateParameter("USU", adVarChar, adParamInput, Len(sCodUsu), sCodUsu)
        .Parameters.Append oParam
        
        .Execute
    End With

Salir:
    EliminarAccionesUsuario = TESError
    Set oComm = Nothing
    Set oParam = Nothing
    Exit Function
Error_Cls:
    TESError = basErrores.TratarError(mvarConexion.ADOCon.Errors)
    Resume Salir
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CAcciones", "EliminarAccionesUsuario", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If
End Function
