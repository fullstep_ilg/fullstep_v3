VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CCampoPredef"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements IBaseDatos

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

Private m_oConexion As CConexion

Private m_lTipoSolicitud As Long
Private m_lID As Long
Private m_vIndice As Variant
Private m_vFecAct As Variant

Private m_oDenominaciones As CMultiidiomas
Private m_oAyudas As CMultiidiomas

Private m_udtTipoPredef As TipoCampoPredefinido
Private m_udtTipo As TiposDeAtributos
Private m_udtTipoIntroduccion As TAtributoIntroduccion
Private m_vMinNum As Variant
Private m_vMaxNum As Variant
Private m_vMinFec As Variant
Private m_vMaxFec As Variant
Private m_vTipoCampoGS As Variant
Private m_vIdAtrib As Variant
Private m_bEsSubCampo As Boolean
Private m_vOrden As Variant
Private m_sPRES5 As String

Private m_oValoresLista As CCampoValorListas

Private m_oCampoPadre As CCampoPredef

Public Property Let Id(ByVal dato As Long)
    m_lID = dato
End Property
Public Property Get Id() As Long
    Id = m_lID
End Property

Public Property Let TipoSolicitud(ByVal dato As Long)
    m_lTipoSolicitud = dato
End Property
Public Property Get TipoSolicitud() As Long
    TipoSolicitud = m_lTipoSolicitud
End Property

Public Property Let IdAtrib(ByVal vData As Variant)
    m_vIdAtrib = vData
End Property
Public Property Get IdAtrib() As Variant
    IdAtrib = m_vIdAtrib
End Property

Public Property Let TipoCampoGS(ByVal vData As Variant)
    m_vTipoCampoGS = vData
End Property
Public Property Get TipoCampoGS() As Variant
    TipoCampoGS = m_vTipoCampoGS
End Property

Public Property Let TipoPredef(ByVal vData As TipoCampoPredefinido)
    m_udtTipoPredef = vData
End Property
Public Property Get TipoPredef() As TipoCampoPredefinido
    TipoPredef = m_udtTipoPredef
End Property

Public Property Let Tipo(ByVal vData As TiposDeAtributos)
    m_udtTipo = vData
End Property
Public Property Get Tipo() As TiposDeAtributos
    Tipo = m_udtTipo
End Property

Public Property Get MinNum() As Variant
    MinNum = m_vMinNum
End Property
Public Property Let MinNum(ByVal vData As Variant)
    m_vMinNum = vData
End Property

Public Property Get MaxNum() As Variant
    MaxNum = m_vMaxNum
End Property
Public Property Let MaxNum(ByVal vData As Variant)
    m_vMaxNum = vData
End Property

Public Property Get MinFec() As Variant
    MinFec = m_vMinFec
End Property
Public Property Let MinFec(ByVal vData As Variant)
    m_vMinFec = vData
End Property

Public Property Get MaxFec() As Variant
    MaxFec = m_vMaxFec
End Property
Public Property Let MaxFec(ByVal vData As Variant)
    m_vMaxFec = vData
End Property

Public Property Let TipoIntroduccion(ByVal vData As TAtributoIntroduccion)
    m_udtTipoIntroduccion = vData
End Property
Public Property Get TipoIntroduccion() As TAtributoIntroduccion
    TipoIntroduccion = m_udtTipoIntroduccion
End Property

Public Property Get FecAct() As Variant
    FecAct = m_vFecAct
End Property
Public Property Let FecAct(ByVal Data As Variant)
    m_vFecAct = Data
End Property

Public Property Let Indice(ByVal varIndice As Variant)
    m_vIndice = varIndice
End Property
Public Property Get Indice() As Variant
    Indice = m_vIndice
End Property

Public Property Get Denominaciones() As CMultiidiomas
    Set Denominaciones = m_oDenominaciones
End Property
Public Property Set Denominaciones(ByVal dato As CMultiidiomas)
    Set m_oDenominaciones = dato
End Property

Public Property Get Ayudas() As CMultiidiomas
    Set Ayudas = m_oAyudas
End Property
Public Property Set Ayudas(ByVal dato As CMultiidiomas)
    Set m_oAyudas = dato
End Property

Public Property Get Orden() As Variant
    Orden = m_vOrden
End Property
Public Property Let Orden(ByVal vData As Variant)
    m_vOrden = vData
End Property

Public Property Let EsSubCampo(ByVal vData As Boolean)
    m_bEsSubCampo = vData
End Property
Public Property Get EsSubCampo() As Boolean
    EsSubCampo = m_bEsSubCampo
End Property

Public Property Get ValoresLista() As CCampoValorListas
    Set ValoresLista = m_oValoresLista
End Property
Public Property Set ValoresLista(ByVal dato As CCampoValorListas)
    Set m_oValoresLista = dato
End Property

Public Property Get CampoPadre() As CCampoPredef
    Set CampoPadre = m_oCampoPadre
End Property
Public Property Set CampoPadre(ByVal oCampoPadre As CCampoPredef)
    Set m_oCampoPadre = oCampoPadre
End Property
Public Property Get PRES5() As String
    PRES5 = m_sPRES5
End Property
Public Property Let PRES5(ByVal dato As String)
    m_sPRES5 = dato
End Property

Friend Property Set Conexion(ByVal vData As CConexion)

    ''' * Objetivo: Dar valor a la conexion de la Destino
    ''' * Recibe: Conexion
    ''' * Devuelve: Nada

    Set m_oConexion = vData
    
End Property

Friend Property Get Conexion() As CConexion

    ''' * Objetivo: Devolver la conexion de la Destino
    ''' * Recibe: Nada
    ''' * Devuelve: Conexion

    Set Conexion = m_oConexion
    
End Property

Private Sub Class_Terminate()
    Set m_oDenominaciones = Nothing
    Set m_oAyudas = Nothing
    Set m_oValoresLista = Nothing
    Set m_oCampoPadre = Nothing
    Set m_oConexion = Nothing
End Sub

Private Function IBaseDatos_AnyadirABaseDatos() As TipoErrorSummit
    Dim sConsulta As String
    Dim TESError As TipoErrorSummit
    Dim AdoRes As adodb.Recordset
    Dim sCod As String
    Dim sDen As String
    Dim oDen As CMultiidioma
    Dim sAyuda As String
    Dim sAyudaDen As String
    Dim oValorLista As CCampoValorLista
    Dim sLista As String
    Dim sListaDen As String
    Dim lOrden As Long
    
    '********* Precondicion *******************
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CCampoPredef.IBaseDatos_AnyadirABaseDatos", "No se ha establecido la conexion"
    End If
    
    '******************************************
    
    On Error GoTo ERROR:
    
    TESError.NumError = TESnoerror
       
    m_oConexion.BEGINTRANSACTION
    
    If Not m_oDenominaciones Is Nothing Then
        For Each oDen In m_oDenominaciones
            sCod = sCod & "," & "DEN_" & oDen.Cod
            sDen = sDen & "," & StrToSQLNULL(oDen.Den)
        Next
    End If
    
    If Not m_oAyudas Is Nothing Then
        For Each oDen In m_oAyudas
            sAyuda = sAyuda & "," & "AYUDA_" & oDen.Cod
            sAyudaDen = sAyudaDen & "," & StrToSQLNULL(oDen.Den)
        Next
    End If
    
    Set AdoRes = New adodb.Recordset
    
    If Not m_oCampoPadre Is Nothing Then
        sConsulta = "SELECT MAX(CAMPOS_PREDEF.ORDEN) AS ORDEN FROM CAMPOS_PREDEF INNER JOIN DESGLOSE_CAMPOS_PREDEF WITH (NOLOCK)"
        sConsulta = sConsulta & " ON DESGLOSE_CAMPOS_PREDEF.CAMPO_HIJO=CAMPOS_PREDEF.ID AND DESGLOSE_CAMPOS_PREDEF.CAMPO_PADRE=" & m_oCampoPadre.Id
        sConsulta = sConsulta & " WHERE TIPO_SOLICITUD=" & m_lTipoSolicitud & " AND ES_SUBCAMPO=1"
    Else
        sConsulta = "SELECT MAX(ORDEN) AS ORDEN FROM CAMPOS_PREDEF WHERE TIPO_SOLICITUD=" & m_lTipoSolicitud & " AND ES_SUBCAMPO=0"
    End If
    AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
    If IsNull(AdoRes.Fields("ORDEN").Value) Then
        m_vOrden = 1
    Else
        m_vOrden = AdoRes.Fields("ORDEN").Value + 1
    End If
    AdoRes.Close
    
    'Inserta el campo en BD:
    sConsulta = "INSERT INTO CAMPOS_PREDEF (TIPO_SOLICITUD " & sCod & sAyuda & ",TIPO,SUBTIPO,INTRO," & _
                "MINNUM,MAXNUM,MINFEC,MAXFEC,TIPO_CAMPO_GS,ORDEN,ES_SUBCAMPO) " & _
                " VALUES (" & m_lTipoSolicitud & sDen & sAyudaDen & "," & m_udtTipoPredef & "," & m_udtTipo & "," & m_udtTipoIntroduccion & _
                "," & DblToSQLFloat(m_vMinNum) & "," & DblToSQLFloat(m_vMaxNum) & "," & DblToSQLFloat(m_vMinFec) & "," & DblToSQLFloat(m_vMaxFec) & _
                "," & IIf(IsNull(m_vTipoCampoGS), "NULL", m_vTipoCampoGS) & "," & m_vOrden & "," & BooleanToSQLBinary(m_bEsSubCampo) & ")"
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR
        
    'Obtiene el id del campo introducido:
    sConsulta = "SELECT MAX(ID) AS ID FROM CAMPOS_PREDEF WHERE TIPO_SOLICITUD=" & m_lTipoSolicitud
    AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
    m_lID = AdoRes("ID").Value
    AdoRes.Close
    
    'si tiene valores de lista:
    If Not m_oValoresLista Is Nothing Then
        For Each oValorLista In m_oValoresLista
            Select Case m_udtTipo
                Case TipoFecha
                    sConsulta = "INSERT INTO CAMPO_PREDEF_VALOR_LISTA (CAMPO,ORDEN,VALOR_FEC) VALUES ("
                    sConsulta = sConsulta & m_lID & "," & oValorLista.Orden & "," & DateToSQLTimeDate(oValorLista.ValorFec) & ")"

                Case TipoNumerico
                    sConsulta = "INSERT INTO CAMPO_PREDEF_VALOR_LISTA (CAMPO,ORDEN,VALOR_NUM) VALUES ("
                    sConsulta = sConsulta & m_lID & "," & oValorLista.Orden & "," & DblToSQLFloat(oValorLista.ValorNum) & ")"
                    
                Case TipoTextoCorto, TipoTextoLargo, TipoTextoMedio
                    sLista = ""
                    sListaDen = ""
                    For Each oDen In oValorLista.ValorText
                        sLista = sLista & ",VALOR_TEXT_" & oDen.Cod
                        sListaDen = sListaDen & "," & StrToSQLNULL(oDen.Den)
                    Next
                    sConsulta = "INSERT INTO CAMPO_PREDEF_VALOR_LISTA (CAMPO,ORDEN " & sLista & ") VALUES ("
                    sConsulta = sConsulta & m_lID & "," & oValorLista.Orden & sListaDen & ")"
            End Select
            m_oConexion.ADOCon.Execute sConsulta
            If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR
        Next
    End If
    
    'Si es un campo de desglose se a�ade en la tabla DESGLOSE_CAMPOS_PREDEF
    If Not m_oCampoPadre Is Nothing Then
        sConsulta = "SELECT MAX(ORDEN) AS ORDEN FROM DESGLOSE_CAMPOS_PREDEF WHERE CAMPO_PADRE=" & m_oCampoPadre.Id
        AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
        If IsNull(AdoRes.Fields("ORDEN").Value) Then
            lOrden = 1
        Else
            lOrden = AdoRes.Fields("ORDEN").Value + 1
        End If
        AdoRes.Close
    
        sConsulta = "INSERT INTO DESGLOSE_CAMPOS_PREDEF (CAMPO_PADRE,CAMPO_HIJO,ORDEN) VALUES (" & m_oCampoPadre.Id & "," & m_lID & "," & lOrden & ")"
        m_oConexion.ADOCon.Execute sConsulta
        If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR
    End If
    
    AdoRes.Open "SELECT FECACT FROM CAMPOS_PREDEF WITH (NOLOCK) WHERE ID=" & m_lID, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
    m_vFecAct = AdoRes(0).Value
    AdoRes.Close
    Set AdoRes = Nothing
    
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    
    IBaseDatos_AnyadirABaseDatos = TESError
    Exit Function

ERROR:
    
    IBaseDatos_AnyadirABaseDatos = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    
    m_oConexion.ROLLBACKTRANSACTION

    Set AdoRes = Nothing
    
End Function

Private Function IBaseDatos_CambiarCodigo(CodigoNuevo As Variant) As TipoErrorSummit

End Function

Private Sub IBaseDatos_CancelarEdicion()

End Sub

Private Function IBaseDatos_ComprobarExistenciaEnBaseDatos() As Boolean

End Function


Private Function IBaseDatos_EliminarDeBaseDatos() As TipoErrorSummit
Dim sConsulta As String
Dim TESError As TipoErrorSummit

'********* Precondicion *******************
If m_oConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CCampoPredef.EliminarDeBaseDatos", "No se ha establecido la conexion"
End If

'******************************************

On Error GoTo ERROR:
    
    TESError.NumError = TESnoerror
    
    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
    
    'Borra el campo predefinido:
    sConsulta = "DELETE FROM DESGLOSE_CAMPOS_PREDEF WHERE CAMPO_PADRE=" & m_lID & " OR CAMPO_HIJO=" & m_lID
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR
    sConsulta = "DELETE FROM CAMPO_PREDEF_VALOR_LISTA WHERE CAMPO=" & m_lID
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR
    sConsulta = "DELETE FROM CAMPOS_PREDEF WHERE ID=" & m_lID
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR
    'Actualiza el orden de los campos del mismo grupo:
    sConsulta = "UPDATE CAMPOS_PREDEF SET ORDEN=ORDEN - 1"
    sConsulta = sConsulta & " WHERE ES_SUBCAMPO=0 AND TIPO_SOLICITUD=" & m_lTipoSolicitud & " AND ORDEN >" & m_vOrden
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR
    
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    IBaseDatos_EliminarDeBaseDatos = TESError
    Exit Function

ERROR:
    
    IBaseDatos_EliminarDeBaseDatos = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    
    m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"

End Function

Private Function IBaseDatos_FinalizarEdicionEliminando() As TipoErrorSummit

End Function


Private Function IBaseDatos_FinalizarEdicionModificando() As TipoErrorSummit
Dim sConsulta As String
Dim TESError As TipoErrorSummit
Dim oDen As CMultiidioma
Dim sDen As String
Dim AdoRes As adodb.Recordset

'********* Precondicion *******************
If m_oConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CFormItem.IBaseDatos_FinalizarEdicionModificando", "No se ha establecido la conexion"
End If
'******************************************

On Error GoTo ERROR:
    
    TESError.NumError = TESnoerror
    

    'Comprobamos si ha habido cambios en  otra sesi�n
    Set AdoRes = New adodb.Recordset
    sConsulta = "SELECT FECACT FROM CAMPOS_PREDEF WITH (NOLOCK) WHERE ID=" & m_lID
    AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
    If AdoRes.eof Then
        AdoRes.Close
        Set AdoRes = Nothing
        TESError.NumError = TESDatoEliminado
        TESError.Arg1 = 179 ''"CAMPOS_PREDEF"
        IBaseDatos_FinalizarEdicionModificando = TESError
        m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
        Exit Function
    Else
        AdoRes.Close
    End If
    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
       
    sDen = ""
    If Not m_oDenominaciones Is Nothing Then
        For Each oDen In m_oDenominaciones
            sDen = sDen & "DEN_" & oDen.Cod & "=" & StrToSQLNULL(oDen.Den) & ","
        Next
        sDen = Mid(sDen, 1, Len(sDen) - 1)
    End If

    'Actualiza el campo
    sConsulta = "UPDATE CAMPOS_PREDEF SET "
    sConsulta = sConsulta & sDen & " WHERE ID=" & m_lID
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR
    
    'Recogemos el nuevo valor de FECACT
    AdoRes.Open "SELECT FECACT FROM CAMPOS_PREDEF WITH (NOLOCK) WHERE ID=" & m_lID, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
    m_vFecAct = AdoRes("FECACT").Value
    AdoRes.Close
    Set AdoRes = Nothing

    
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    IBaseDatos_FinalizarEdicionModificando = TESError
    Exit Function

ERROR:
    IBaseDatos_FinalizarEdicionModificando = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    
    If Not AdoRes Is Nothing Then
        AdoRes.Close
        Set AdoRes = Nothing
    End If
    
    m_oConexion.ROLLBACKTRANSACTION
End Function


Private Function IBaseDatos_IniciarEdicion(Optional ByVal Bloquear As Boolean, Optional ByVal UsuarioBloqueo As String) As TipoErrorSummit

End Function

Public Function ModificarAyuda() As TipoErrorSummit
Dim sConsulta As String
Dim oAyuda As CMultiidioma
Dim TESError As TipoErrorSummit

'******************* Precondicion *******************
If m_oConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CCampoPredef.ModificarAyuda", "No se ha establecido la conexion"
End If
'*****************************************************

On Error GoTo ERROR:

    TESError.NumError = TESnoerror
    
    m_oConexion.BEGINTRANSACTION
    
    sConsulta = "UPDATE CAMPOS_PREDEF SET "
    
    For Each oAyuda In m_oAyudas
        sConsulta = sConsulta & "AYUDA_" & oAyuda.Cod & " = " & StrToSQLNULL(oAyuda.Den) & ","
    Next
    sConsulta = Mid(sConsulta, 1, Len(sConsulta) - 1)
    sConsulta = sConsulta & " WHERE ID=" & m_lID
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR

    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    ModificarAyuda = TESError
    
    Exit Function

ERROR:
    ModificarAyuda = basErrores.TratarError(m_oConexion.ADOCon.Errors)
        
    m_oConexion.ROLLBACKTRANSACTION
End Function


Public Sub CargarValoresLista(Optional ByVal UsarIndice As Boolean)
    Dim rs As New adodb.Recordset
    Dim sConsulta As String
    Dim lIndice As Long
    Dim oValoresText As CMultiidiomas
    Dim oGestorParametros As CGestorParametros
    Dim oIdiomas As CIdiomas
    Dim oIdioma As CIdioma
    
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CCampoPredef.CargarValoresLista", "No se ha establecido la conexion"
        Exit Sub
    End If

    
    'Tarea 3290'    'Carga todos los idiomas de la aplicaci�n:
    Set oGestorParametros = New CGestorParametros
    Set oGestorParametros.Conexion = m_oConexion
    Set oIdiomas = oGestorParametros.DevolverIdiomas(False, False, True)
    
    Dim valortext_general As String
    valortext_general = ""
    
    For Each oIdioma In oIdiomas
        If (valortext_general <> "") Then
           valortext_general = valortext_general & ","
        End If
        valortext_general = valortext_general & "VALOR_TEXT_" & oIdioma.Cod
    Next
    ' valortext_general = VALOR_TEXT_SPA,VALOR_TEXT_ENG,VALOR_TEXT_GER,VALOR_TEXT_FRA

    sConsulta = "SELECT CAMPO,ORDEN,VALOR_NUM,VALOR_FEC,FECACT," & valortext_general & "  FROM CAMPO_PREDEF_VALOR_LISTA WITH (NOLOCK) WHERE CAMPO=" & m_lID & " ORDER BY ORDEN"
    
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly

    If rs.eof Then
        rs.Close
        Set rs = Nothing

        Set m_oValoresLista = Nothing
        Set m_oValoresLista = New CCampoValorListas
        Set m_oValoresLista.Conexion = m_oConexion
        Set oGestorParametros = Nothing
        Set oIdiomas = Nothing
        Exit Sub

    Else
        Set m_oValoresLista = Nothing
        Set m_oValoresLista = New CCampoValorListas
        Set m_oValoresLista.Conexion = m_oConexion
        
       
        If UsarIndice Then
            lIndice = 0
        End If
        
        While Not rs.eof
            Set oValoresText = Nothing
            Set oValoresText = New CMultiidiomas
            If m_udtTipo = TipoTextoCorto Or m_udtTipo = TipoTextoMedio Or m_udtTipo = TipoTextoLargo Then
                For Each oIdioma In oIdiomas
                    oValoresText.Add oIdioma.Cod, rs.Fields("VALOR_TEXT_" & oIdioma.Cod).Value
                Next
            End If
        
            If UsarIndice Then
                m_oValoresLista.Add rs.Fields("ORDEN").Value, , rs.Fields("VALOR_NUM").Value, oValoresText, rs.Fields("VALOR_FEC").Value, rs.Fields("FECACT").Value, lIndice, Me
                lIndice = lIndice + 1
            Else
                m_oValoresLista.Add rs.Fields("ORDEN").Value, , rs.Fields("VALOR_NUM").Value, oValoresText, rs.Fields("VALOR_FEC").Value, rs.Fields("FECACT").Value, , Me
            End If
            
            rs.MoveNext
        Wend

        rs.Close
        Set rs = Nothing
        
    End If
    Set oGestorParametros = Nothing
    Set oIdiomas = Nothing

End Sub

Public Function ActualizarValoresLista() As TipoErrorSummit
    Dim sConsulta As String
    Dim TESError As TipoErrorSummit
    Dim oCom As adodb.Command
    Dim oParam As adodb.Parameter
    Dim oValorLista As CCampoValorLista
    
    '******************* Precondicion *******************
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CCampoPredef.ActualizarValoresLista", "No se ha establecido la conexion"
    End If
    '*****************************************************
    
    On Error GoTo ERROR:
    
    m_oConexion.BEGINTRANSACTION
    
    'Borrar los valores existentes
    Set oCom = New adodb.Command
    With oCom
        sConsulta = "DELETE FROM CAMPO_PREDEF_VALOR_LISTA WHERE CAMPO=?"
        
        Set .ActiveConnection = m_oConexion.ADOCon
        .CommandType = adCmdText
        .CommandText = sConsulta
        Set oParam = .CreateParameter("@CAMPO", adInteger, adParamInput, , m_lID)
        .Parameters.Append oParam
        
        .Execute
        If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR
    End With
    
    'Insertar los nuevos valores
    If Not m_oValoresLista Is Nothing Then
        For Each oValorLista In m_oValoresLista
            Set oCom = New adodb.Command
            With oCom
                Set .ActiveConnection = m_oConexion.ADOCon
                .CommandType = adCmdText
                
                Select Case m_udtTipo
                    Case TipoFecha
                        sConsulta = "INSERT INTO CAMPO_PREDEF_VALOR_LISTA (CAMPO,ORDEN,VALOR_FEC) VALUES (?,?,?)"
                        
                        Set oParam = .CreateParameter("@CAMPO", adInteger, adParamInput, , m_lID)
                        .Parameters.Append oParam
                        Set oParam = .CreateParameter("@ORDEN", adInteger, adParamInput, , oValorLista.Orden)
                        .Parameters.Append oParam
                        Set oParam = .CreateParameter("@VALOR", adDate, adParamInput, , oValorLista.ValorFec)
                        .Parameters.Append oParam
                    Case TipoNumerico
                        sConsulta = "INSERT INTO CAMPO_PREDEF_VALOR_LISTA (CAMPO,ORDEN,VALOR_NUM) VALUES (?,?,?)"
                        
                        Set oParam = .CreateParameter("@CAMPO", adInteger, adParamInput, , m_lID)
                        .Parameters.Append oParam
                        Set oParam = .CreateParameter("@ORDEN", adInteger, adParamInput, , oValorLista.Orden)
                        .Parameters.Append oParam
                        Set oParam = .CreateParameter("@VALOR", adDouble, adParamInput, , oValorLista.ValorNum)
                        .Parameters.Append oParam
                    Case TipoTextoCorto, TipoTextoLargo, TipoTextoMedio
                        Dim i As Integer
                        Dim sLista As String
                        Dim sListaDen As String
                        Dim oDen As CMultiidioma
                        
                        sLista = ""
                        sListaDen = ""
                        i = 0
                        For Each oDen In oValorLista.ValorText
                            sLista = sLista & ",VALOR_TEXT_" & oDen.Cod
                            sListaDen = sListaDen & ",?"
                            
                            i = i + 1
                        Next
                        sConsulta = "INSERT INTO CAMPO_PREDEF_VALOR_LISTA (CAMPO,ORDEN " & sLista & ") VALUES (?,?" & sListaDen & ")"
                        
                        Set oParam = .CreateParameter("@CAMPO", adInteger, adParamInput, , m_lID)
                        .Parameters.Append oParam
                        Set oParam = .CreateParameter("@ORDEN", adInteger, adParamInput, , oValorLista.Orden)
                        .Parameters.Append oParam
                        i = 0
                        For Each oDen In oValorLista.ValorText
                            Set oParam = .CreateParameter("@VALOR" & i, adVarChar, adParamInput, Len(oDen.Den), oDen.Den)
                            .Parameters.Append oParam
                            
                            i = i + 1
                        Next
                End Select
            
                .CommandText = sConsulta
                
                .Execute
                If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR
            End With
        Next
    End If
        
    m_oConexion.COMMITTRANSACTION
Salir:
    ActualizarValoresLista = TESError
    Set oDen = Nothing
    Set oValorLista = Nothing
    Set oCom = Nothing
    Set oParam = Nothing
    Exit Function
ERROR:
    ActualizarValoresLista = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    m_oConexion.ROLLBACKTRANSACTION
    Resume Salir
End Function


