VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cPresConcep5Nivel2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements IBaseDatos


Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

Private m_lIndice As Long 'Nos indica la posicion secuencial dentro de una Coleccion

Private m_sPRES0 As String
Private m_sPRES1 As String
Private m_sCod As String
Private m_sDen As String
Private m_sDenVigencia As String
Private m_bBajaLog As Boolean
Private m_bNodoConHermanos As Boolean
Private m_iSeleccionado As Integer
Private m_vFecIni As Variant
Private m_vFecFin As Variant
Private m_bVigente As Boolean
Private m_vGestor As Variant

Private m_oPresConceptos5Nivel3 As cPresConceptos5Nivel3
Private m_oConexion As CConexion 'local copy
Private m_oConexionServer As CConexionDeUseServer
Private m_adoresRS As adodb.Recordset
Private m_oDenominaciones As CMultiidiomas

Public Property Get Indice() As Long
    Indice = m_lIndice
End Property
Public Property Let Indice(ByVal Ind As Long)
    m_lIndice = Ind
End Property

Friend Property Set Conexion(ByVal vData As CConexion)
    Set m_oConexion = vData
End Property

Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property

Friend Property Set ConexionServer(ByVal con As CConexionDeUseServer)
Set m_oConexionServer = con
End Property

Friend Property Get ConexionServer() As CConexionDeUseServer
Set ConexionServer = m_oConexionServer
End Property

Public Property Set PresConceptos5Nivel3(ByVal vData As cPresConceptos5Nivel3)
    Set m_oPresConceptos5Nivel3 = vData
End Property


Public Property Get PresConceptos5Nivel3() As cPresConceptos5Nivel3
    Set PresConceptos5Nivel3 = m_oPresConceptos5Nivel3
End Property


Public Property Let PRES0(ByVal vData As String)
    m_sPRES0 = vData
End Property

Public Property Get PRES0() As String
    PRES0 = m_sPRES0
End Property

Public Property Let PRES1(ByVal vData As String)
    m_sPRES1 = vData
End Property

Public Property Get PRES1() As String
    PRES1 = m_sPRES1
End Property

Public Property Let Cod(ByVal vData As String)
    m_sCod = vData
End Property

Public Property Get Cod() As String
    Cod = m_sCod
End Property

Public Property Let Den(ByVal vData As String)
    m_sDen = vData
End Property

Public Property Get Den() As String
    Den = m_sDen
End Property

Public Property Get BajaLog() As Boolean
    BajaLog = m_bBajaLog
End Property

Public Property Let BajaLog(ByVal Data As Boolean)
    m_bBajaLog = Data
End Property

Public Property Get NodoConHermanos() As Boolean
    NodoConHermanos = m_bNodoConHermanos
End Property

Public Property Let NodoConHermanos(ByVal Data As Boolean)
    m_bNodoConHermanos = Data
End Property

Public Property Let Seleccionado(ByVal vData As Integer)
    m_iSeleccionado = vData
End Property


Public Property Get Seleccionado() As Integer
    Seleccionado = m_iSeleccionado
End Property

Public Property Get Denominaciones() As CMultiidiomas
    Set Denominaciones = m_oDenominaciones
End Property
Public Property Set Denominaciones(ByVal dato As CMultiidiomas)
    Set m_oDenominaciones = dato
End Property

Public Property Let DenVigencia(ByVal vData As String)
    m_sDenVigencia = vData
End Property

Public Property Get DenVigencia() As String
    DenVigencia = m_sDenVigencia
End Property

Public Property Get FecIni() As Variant
    FecIni = m_vFecIni
End Property
Public Property Let FecIni(ByVal dato As Variant)
    m_vFecIni = dato
End Property

Public Property Get FecFin() As Variant
    FecFin = m_vFecFin
End Property
Public Property Let FecFin(ByVal dato As Variant)
    m_vFecFin = dato
End Property

Public Property Get Vigente() As Boolean
    Vigente = m_bVigente
End Property

Public Property Let Vigente(ByVal Data As Boolean)
    m_bVigente = Data
End Property

Public Property Let Gestor(ByVal dato As Variant)
    m_vGestor = dato
End Property
Public Property Get Gestor() As Variant
    Gestor = m_vGestor
End Property

Private Sub Class_Terminate()
    
    Set m_oConexion = Nothing
    Set m_oConexionServer = Nothing
    Set m_oPresConceptos5Nivel3 = Nothing
    
End Sub

''' <summary>
''' Guarda una partida presupuestaria de nivel 2 en la BD
''' </summary>
''' <returns>La funci�n devuelve un Tipo TipoErrorSummit que s�lo tiene un valor: el NumError, 0 si todo va bien, 3 si no se encuentra la partida presupuestaria</returns>
''' <remarks>Llamada desde frmprescon5Detalle.cmdAceptar_click(); Tiempo m�ximo < 1 seg.</remarks>

Private Function IBaseDatos_AnyadirABaseDatos() As TipoErrorSummit
Dim sConsulta As String
Dim TESError As TipoErrorSummit
Dim rs As Recordset
Dim rs2 As Recordset
Dim l_id As Long
Dim sCod As String
Dim sDen As String
Dim btrans As Boolean
Dim oDen As CMultiidioma

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
TESError.NumError = TESnoerror

If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:

    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"

    btrans = True

    sConsulta = "INSERT INTO PRES5_NIV2 (PRES0,PRES1,COD) "
    sConsulta = sConsulta & " VALUES ('" & DblQuote(m_sPRES0) & "','" & DblQuote(m_sPRES1) & "','" & DblQuote(m_sCod) & "')"
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    sConsulta = ""
    If Not m_oDenominaciones Is Nothing Then
        For Each oDen In m_oDenominaciones
            sConsulta = sConsulta & "INSERT INTO PRES5_IDIOMAS (PRES0,PRES1,PRES2,IDIOMA,DEN) "
            sConsulta = sConsulta & " VALUES ('" & DblQuote(m_sPRES0) & "','" & DblQuote(m_sPRES1) & "','" & DblQuote(m_sCod) & "','" & DblQuote(oDen.Cod) & "','" & DblQuote(oDen.Den) & "')"
        Next
        m_oConexion.ADOCon.Execute sConsulta
        If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    End If
    
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    btrans = False

    IBaseDatos_AnyadirABaseDatos = TESError
    Exit Function
    
Error_Cls:

    IBaseDatos_AnyadirABaseDatos = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    If btrans Then m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Function
ERROR:
   If ERR.Number <> 0 Then
      'Call g_oErrores.TratarError("Class Module", "cPresConcep5Nivel2", "IBaseDatos_AnyadirABaseDatos", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If
End Function

Private Function IBaseDatos_CambiarCodigo(CodigoNuevo As Variant) As TipoErrorSummit
End Function

Private Sub IBaseDatos_CancelarEdicion()
End Sub

Private Function IBaseDatos_ComprobarExistenciaEnBaseDatos() As Boolean
End Function

''' <summary>
''' Elimina una partida presupuestaria de nivel 2 en la BD
''' </summary>
''' <returns>La funci�n devuelve un Tipo TipoErrorSummit que s�lo tiene un valor: el NumError, 0 si todo va bien, 3 si no se encuentra la partida presupuestaria</returns>
''' <remarks>Llamada desde frmprescon5Detalle.cmdEliminar_click(); Tiempo m�ximo < 1 seg.</remarks>

Private Function IBaseDatos_EliminarDeBaseDatos() As TipoErrorSummit
    Dim sConsulta As String
    Dim TESError As TipoErrorSummit
    Dim btrans As Boolean
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    TESError.NumError = TESnoerror
        
    If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
    
    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
    btrans = True


    sConsulta = "DELETE FROM PRES5_IDIOMAS WHERE 1 = 1"
    
    If m_sPRES0 <> "" Then
        sConsulta = sConsulta & " AND PRES0 = '" & m_sPRES0 & "'"
    End If
    
    If m_sPRES1 <> "" Then
        sConsulta = sConsulta & " AND PRES1 = '" & m_sPRES1 & "'"
    End If

    sConsulta = sConsulta & " AND PRES2 = '" & m_sCod & "'"


    sConsulta = sConsulta & " DELETE FROM PRES5_NIV2 WHERE 1 = 1"
    
    
    If m_sPRES0 <> "" Then
        sConsulta = sConsulta & " AND PRES0 = '" & m_sPRES0 & "'"
    End If
    
    If m_sPRES1 <> "" Then
        sConsulta = sConsulta & " AND PRES1 = '" & m_sPRES1 & "'"
    End If

    sConsulta = sConsulta & " AND COD = '" & m_sCod & "'"
    
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    btrans = False

    IBaseDatos_EliminarDeBaseDatos = TESError
    Exit Function
        
Error_Cls:
    IBaseDatos_EliminarDeBaseDatos = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    If btrans Then m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      'Call g_oErrores.TratarError("Class Module", "cPresConcep5Nivel2", "IBaseDatos_EliminarDeBaseDatos", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If
End Function

Private Function IBaseDatos_FinalizarEdicionEliminando() As TipoErrorSummit
End Function

''' <summary>
''' Actualiza los datos una partida presupuestaria de nivel 2 en la BD
''' </summary>
''' <returns>La funci�n devuelve un Tipo TipoErrorSummit que s�lo tiene un valor: el NumError, 0 si todo va bien, 3 si no se encuentra la partida presupuestaria</returns>
''' <remarks>Llamada desde frmprescon5Detalle.cmdAceptar_click(); Tiempo m�ximo < 1 seg.</remarks>

Private Function IBaseDatos_FinalizarEdicionModificando() As TipoErrorSummit
    Dim TESError As TipoErrorSummit
    Dim sConsulta As String
    Dim btrans As Boolean
    Dim oDen As CMultiidioma
    

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    TESError.NumError = TESnoerror

    If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:

    If m_oDenominaciones Is Nothing Then
        Exit Function
    End If
    
    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
    btrans = True
    
    sConsulta = ""
    If Not m_oDenominaciones Is Nothing Then
        For Each oDen In m_oDenominaciones
        
            sConsulta = sConsulta & "UPDATE PRES5_IDIOMAS SET DEN='" & DblQuote(oDen.Den) & "'"
            sConsulta = sConsulta & " WHERE PRES0 = '" & DblQuote(PRES0) & "' AND PRES1 = '" & DblQuote(PRES1) & "' AND PRES2='" & DblQuote(m_sCod) & "' AND PRES3 IS NULL AND PRES4 IS NULL AND IDIOMA= '" & DblQuote(oDen.Cod) & "'"
        
        Next
        m_oConexion.ADOCon.Execute sConsulta
        If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    End If
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    btrans = False
    Exit Function


Error_Cls:
    IBaseDatos_FinalizarEdicionModificando = TratarError(m_oConexion.ADOCon.Errors)
    If btrans Then m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      'Call g_oErrores.TratarError("Class Module", "cPresConcep5Nivel2", "IBaseDatos_FinalizarEdicionModificando", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If

End Function

''' <summary>
''' Procedimiento que recupera los datos de una partida presupuestaria y los guarda en una serie de variables a nivel de m�dulo
''' Devuelve un tipo TipoErrorSummit definido en CTiposDeDatos, que contendr� un valor 0 como numError si todo ha ido bien
''' o un n�mero de error 3 (TESDatoEliminado) si no encuentra en base de datos la partida presup buscada
''' </summary>
''' <param name="Bloquear">Boolean. Variable definida en la funci�n IniciarEdicion de la clase IBasedeDatos que no se usa en este caso</param>
''' <param name="UsuarioBloqueo">String. Variable definida en la funci�n IniciarEdicion de la clase IBasedeDatos que no se usa en este caso</param>
''' <returns>La funci�n devuelve un Tipo TipoErrorSummit que s�lo tiene un valor: el NumError, 0 si todo va bien, 3 si no se encuentra la partida presupuestaria</returns>
''' <remarks>Llamada desde frmpresupuestos5.cmdBajalog_click,frmpresupuestos5.cmdEli_click,frmpresupuestos5.cmdModif_click; Tiempo m�ximo < 1 seg.</remarks>
''' <revisado por>mmv(08/11/2011)</revisado por>
Private Function IBaseDatos_IniciarEdicion(Optional ByVal Bloquear As Boolean, Optional ByVal UsuarioBloqueo As String) As TipoErrorSummit
    Dim TESError As TipoErrorSummit
    Dim sConsulta As String
    Dim rs As Recordset
    'Dim bConInner As Boolean
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    TESError.NumError = TESnoerror
    
    sConsulta = "SELECT DISTINCT PRES5_NIV2.PRES0,PRES5_NIV2.PRES1,PRES5_NIV2.COD,PRES5_IDIOMAS.DEN,PRES5_NIV2.BAJALOG"
    sConsulta = sConsulta & " FROM PRES5_NIV2 WITH (NOLOCK) "
    sConsulta = sConsulta & " INNER JOIN PRES5_IDIOMAS WITH (NOLOCK) "
    sConsulta = sConsulta & " ON PRES5_NIV2.PRES0 = PRES5_IDIOMAS.PRES0 AND "
    sConsulta = sConsulta & "  PRES5_NIV2.PRES1 = PRES5_IDIOMAS.PRES1 AND PRES5_NIV2.COD = PRES5_IDIOMAS.PRES2 AND PRES5_IDIOMAS.PRES3 IS NULL AND PRES5_IDIOMAS.PRES4 IS NULL"
    
    sConsulta = sConsulta & " WHERE 1 = 1"
        
    
    If m_sPRES0 <> "" Then
        sConsulta = sConsulta & " AND PRES5_NIV2.PRES0 = '" & DblQuote(m_sPRES0) & "'"
    End If
    If m_sPRES1 <> "" Then
        sConsulta = sConsulta & " AND PRES5_NIV2.PRES1 = '" & DblQuote(m_sPRES1) & "'"
    End If
        
    sConsulta = sConsulta & " AND PRES5_NIV2.COD='" & DblQuote(m_sCod) & "'"
    
    sConsulta = sConsulta & " AND PRES5_IDIOMAS.IDIOMA='" & DblQuote(gParametrosInstalacion.gIdioma) & "' "
    
    Set rs = New adodb.Recordset
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    If rs.eof Then
        rs.Close
        Set rs = Nothing
        TESError.NumError = TESDatoEliminado
        IBaseDatos_IniciarEdicion = TESError
        Exit Function
    End If
    
    m_sPRES0 = rs("PRES0").Value
    m_sPRES1 = rs("PRES1").Value
    m_sCod = rs("COD").Value
    m_sDen = rs("DEN").Value
    m_bBajaLog = rs("BAJALOG").Value

    rs.Close
    Set rs = Nothing
    
    IBaseDatos_IniciarEdicion = TESError
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "cPresConcep5Nivel2", "IBaseDatos_IniciarEdicion", ERR, Erl)
      Exit Function
   End If

End Function

Function BajaAltaLogica(ByVal bBaja As Boolean) As TipoErrorSummit
    Dim TESError As TipoErrorSummit
    Dim sConsulta As String
    Dim btrans As Boolean
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    TESError.NumError = TESnoerror
    
    If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
    
    sConsulta = "UPDATE PRES5_NIV2 SET BAJALOG=" & BooleanToSQLBinary(bBaja)
    sConsulta = sConsulta & " WHERE PRES0 = '" & m_sPRES0 & "'  AND PRES1 = '" & m_sPRES1 & "' AND COD='" & DblQuote(m_sCod) & "'"
    m_oConexion.ADOCon.Execute sConsulta

    
    m_bBajaLog = bBaja
    
    BajaAltaLogica = TESError
    Exit Function
    
Error_Cls:
    If m_oConexion.ADOCon.Errors.Count > 0 Then
        BajaAltaLogica = basErrores.TratarError(m_oConexion.ADOCon.Errors)
        Exit Function
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      'Call g_oErrores.TratarError("Class Module", "cPresConcep5Nivel2", "BajaAltaLogica", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If
        
End Function


''' <summary>
''' Devuelve un recordset con todas las denominaciones de la partida de nivel 2
''' </summary>
''' <returns>La funci�n devuelve un Tipo TipoErrorSummit que s�lo tiene un valor: el NumError, 0 si todo va bien, 3 si no se encuentra la partida presupuestaria</returns>
''' <remarks>Llamada desde frmprescon5Detalle.CargarGridDenIdiomas; Tiempo m�ximo < 1 seg.</remarks>

Public Function DevolverDenominaciones() As adodb.Recordset
    Dim sConsulta As String
    Dim rs As adodb.Recordset
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sConsulta = "SELECT PRES5_IDIOMAS.IDIOMA,PRES5_IDIOMAS.DEN"
    sConsulta = sConsulta & " FROM PRES5_IDIOMAS WITH (NOLOCK) "
    sConsulta = sConsulta & " WHERE PRES5_IDIOMAS.PRES0 = '" & DblQuote(m_sPRES0) & "'"
    sConsulta = sConsulta & " AND PRES5_IDIOMAS.PRES1 = '" & DblQuote(m_sPRES1) & "'"
    sConsulta = sConsulta & "  AND PRES5_IDIOMAS.PRES2='" & DblQuote(m_sCod) & "'"
    sConsulta = sConsulta & "  AND PRES5_IDIOMAS.PRES3 IS NULL AND PRES5_IDIOMAS.PRES4 IS NULL"
    
    Set rs = New adodb.Recordset
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    
    Set DevolverDenominaciones = rs
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "cPresConcep5Nivel2", "DevolverDenominaciones", ERR, Erl)
      Exit Function
   End If
        
End Function

