VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "IPeticiones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Public Function DevolverProveedoresSinPeticiones(Optional ByVal CodEqp As Variant, Optional ByVal CodComp As Variant, Optional ByVal UsarIndice As Boolean, Optional ByVal udtOrden As TipoOrdenacionPeticiones) As CProveedores

End Function
Public Function DevolverPeticiones(ByVal CriterioOrd As TipoOrdenacionPeticiones, Optional ByVal CodEqp As Variant, Optional ByVal CodComp As Variant, Optional ByVal UsarIndice As Boolean, Optional ByVal CodProve As Variant) As CPetOfertas

End Function

Public Function DevolverPeticionesPotenciales(Optional ByVal CodEqp As Variant, Optional ByVal CodComp As Variant, Optional ByVal UsarIndice As Boolean) As ADODB.Recordset

End Function
Public Function DevolverNotificacionesPotenciales(Optional ByVal CodEqp As Variant, Optional ByVal CodComp As Variant, Optional ByVal UsarIndice As Boolean) As CPetOfertas

End Function

Public Function EliminarPeticiones(ByVal oPets As CPetOfertas) As TipoErrorSummit

End Function

Public Function RealizarPeticiones(ByVal oPets As CPetOfertas) As TipoErrorSummit

End Function

Public Function ModificarFechaLimiteDeOfertas(ByVal Fecha As Variant) As TipoErrorSummit

End Function
Public Function DevolverComunicacionObjPotenciales(Optional ByVal CodEqp As Variant, Optional ByVal CodComp As Variant, Optional ByVal UsarIndice As Boolean) As CPetOfertas

End Function
Public Function RealizarComunicacion(ByVal oPets As CPetOfertas) As TipoErrorSummit

End Function
