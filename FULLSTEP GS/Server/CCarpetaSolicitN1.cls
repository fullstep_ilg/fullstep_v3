VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CCarpetaSolicitN1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CCarpetaSolicitN1 **********************************
'*             Autor : Gorka Areitioaurtena
'*             Creada : 13/02/06
'****************************************************************


Option Explicit

Implements IBaseDatos

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

Private m_oConexion As CConexion 'local copy
Private m_lID As Long
Private m_sDen As String
Private m_oCarpetasSolicitN2 As CCarpetasSolicitN2

Private m_adores As adodb.Recordset

Public Property Get Id() As Long
    Id = m_lID
End Property
Public Property Let Id(ByVal Ind As Long)
    m_lID = Ind
End Property


Friend Property Set Conexion(ByVal vData As CConexion)
    Set m_oConexion = vData
End Property
Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property

Public Property Set CarpetasSolicitN2(ByVal vData As CCarpetasSolicitN2)
    Set m_oCarpetasSolicitN2 = vData
End Property
Public Property Get CarpetasSolicitN2() As CCarpetasSolicitN2
    Set CarpetasSolicitN2 = m_oCarpetasSolicitN2
End Property

Public Property Let Den(ByVal vData As String)
    m_sDen = vData
End Property
Public Property Get Den() As String
    Den = m_sDen
End Property

Public Property Get Nivel() As Integer
    Nivel = 1
End Property

Private Sub Class_Terminate()
    Set m_oConexion = Nothing
    Set m_adores = Nothing
End Sub

Private Function IBaseDatos_AnyadirABaseDatos() As TipoErrorSummit
    Dim sConsulta As String
    Dim TESError As TipoErrorSummit
    Dim rs As New adodb.Recordset
    Dim btrans As Boolean
    
    TESError.NumError = TESnoerror
    
    '********* Precondicion *******************
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CCarpetaSolicitN1.AnyadirABAseDatos", "No se ha establecido la conexion"
    End If
    ' *****************************************
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
    If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
        
        m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
        btrans = True
        
        sConsulta = "INSERT INTO CARPETAS_SOLICIT_N1 (DEN) VALUES ('" & DblQuote(m_sDen) & "')"
        m_oConexion.ADOCon.Execute sConsulta
        If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
        
        sConsulta = "SELECT MAX(ID) FROM CARPETAS_SOLICIT_N1"
        rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
        
        m_lID = rs.Fields(0).Value
                
        m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
        btrans = False
        
        rs.Close
        Set rs = Nothing
        
        IBaseDatos_AnyadirABaseDatos = TESError
        Exit Function
Error_Cls:
    
        IBaseDatos_AnyadirABaseDatos = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    
        If btrans Then
            m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
        End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CCarpetaSolicitN1", "IBaseDatos_AnyadirABaseDatos", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If
End Function

Private Function IBaseDatos_CambiarCodigo(CodigoNuevo As Variant) As TipoErrorSummit

End Function

Private Sub IBaseDatos_CancelarEdicion()

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not m_adores Is Nothing Then
        m_adores.Close
        Set m_adores = Nothing
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CCarpetaSolicitN1", "IBaseDatos_CancelarEdicion", ERR, Erl)
      Exit Sub
   End If
End Sub

Private Function IBaseDatos_ComprobarExistenciaEnBaseDatos() As Boolean

End Function

Private Function IBaseDatos_EliminarDeBaseDatos() As TipoErrorSummit
    Dim sConsulta As String
    Dim TESError As TipoErrorSummit
    Dim rs As New adodb.Recordset
    Dim btrans As Boolean
    
    TESError.NumError = TESnoerror
    
    '********* Precondicion *******************
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CCarpetaSolicitN1.EliminarDeBaseDatos", "No se ha establecido la conexion"
    End If
    ' *****************************************
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
    If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
        
    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
    btrans = True
    
    sConsulta = "UPDATE SOLICITUD SET CSN1 = NULL WHERE CSN1 = " & m_lID
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    sConsulta = "DELETE FROM CARPETAS_SOLICIT_N1 WHERE ID =" & m_lID
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    btrans = False
    
    Set rs = Nothing
    
    IBaseDatos_EliminarDeBaseDatos = TESError
    Exit Function

Error_Cls:
    
    IBaseDatos_EliminarDeBaseDatos = basErrores.TratarError(m_oConexion.ADOCon.Errors)

    If btrans Then
        m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CCarpetaSolicitN1", "IBaseDatos_EliminarDeBaseDatos", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If
End Function

Private Function IBaseDatos_FinalizarEdicionEliminando() As TipoErrorSummit

End Function

Private Function IBaseDatos_FinalizarEdicionModificando() As TipoErrorSummit
    Dim TESError As TipoErrorSummit
    Dim btrans As Boolean
    Dim sConsulta As String
    
    TESError.NumError = TESnoerror
    
    '********* Precondicion *******************
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CCategoriaN1.FinalizarEdicionModificando", "No se ha establecido la conexion"
    End If
    '******************************************
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
    If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
        
        m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
        btrans = True
        
        'Actualizamos la bd
        sConsulta = "UPDATE CARPETAS_SOLICIT_N1 SET DEN='" & DblQuote(m_sDen) & "' WHERE ID=" & m_lID
        m_oConexion.ADOCon.Execute sConsulta
        If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
        
        m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
        btrans = False
        IBaseDatos_FinalizarEdicionModificando = TESError
        Exit Function
    
Error_Cls:
        
        IBaseDatos_FinalizarEdicionModificando = basErrores.TratarError(m_oConexion.ADOCon.Errors)
        If Not m_adores Is Nothing Then
            m_adores.Close
            Set m_adores = Nothing
        End If
        If btrans Then
            m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
        End If

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CCarpetaSolicitN1", "IBaseDatos_FinalizarEdicionModificando", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If

End Function

Private Function IBaseDatos_IniciarEdicion(Optional ByVal Bloquear As Boolean, Optional ByVal UsuarioBloqueo As String) As TipoErrorSummit

End Function
