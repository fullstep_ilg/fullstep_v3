VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CRelacTipo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True

''' *** Clase: CRelacTipo
''' *** Creacion: 4/08/2011 (JVS)

Option Explicit

Implements IBaseDatos

''' Variables privadas con la informacion de un Tipo relaci�n

Private m_lID As Long
Private m_sCod As String
Private m_oDescripciones As CMultiidiomas
Private m_bPedido As Boolean
Private m_vFecAct As Variant
Private m_lIndice As Long


''' Conexion
Private m_oConexion As CConexion

''' Control de errores
Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum

Public Property Let Id(ByVal vData As Long)
    m_lID = vData
End Property

Public Property Get Id() As Long
    Id = m_lID
End Property

Public Property Let Cod(ByVal vData As String)
    m_sCod = vData
End Property

Public Property Get Cod() As String
    Cod = m_sCod
End Property

Public Property Get Descripciones() As CMultiidiomas
    Set Descripciones = m_oDescripciones
End Property

Public Property Set Descripciones(ByVal dato As CMultiidiomas)
    Set m_oDescripciones = dato
End Property

Public Property Get PermitePedidos() As Boolean
    PermitePedidos = m_bPedido
End Property
Public Property Let PermitePedidos(ByVal b As Boolean)
    m_bPedido = b
End Property


Public Property Get Indice() As Long
    Indice = m_lIndice
End Property

Public Property Let Indice(ByVal lInd As Long)
    m_lIndice = lInd
End Property

Public Property Get FecAct() As Variant
    FecAct = m_vFecAct
End Property

Public Property Let FecAct(ByVal vVar As Variant)
    m_vFecAct = vVar
End Property

Friend Property Set Conexion(ByVal vData As CConexion)
    Set m_oConexion = vData
End Property

Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property

Private Sub Class_Terminate()
    Set m_oDescripciones = Nothing
    
    Set m_oConexion = Nothing
End Sub

''' <summary>
''' Inserta los tipos de relaciones entre proveedores
''' </summary>
''' <returns>error</returns>
''' <revision>JVS 24/08/2011</revision>
Private Function IBaseDatos_AnyadirABaseDatos() As TipoErrorSummit
Dim sConsulta As String
Dim TESError As TipoErrorSummit
Dim rs As adodb.Recordset
Dim sCod As String
Dim sDen As String
Dim oDen As CMultiidioma


    TESError.NumError = TESnoerror
    
    ''' Precondicion
    
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CRelacTipo.AnyadirABAseDatos", "No se ha establecido la conexion"
    End If
    
    If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    

If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:

    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
    
    'Inserta el campo en BD:
    sConsulta = "INSERT INTO GS_PROVETREL (COD,PEDIDO) VALUES ('" & DblQuote(m_sCod) & "'," & BooleanToSQLBinary(m_bPedido) & ")"
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    'Obtiene el id del campo introducido:
    Set rs = New adodb.Recordset
    sConsulta = "SELECT MAX(ID) AS ID FROM GS_PROVETREL"
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
    m_lID = rs("ID").Value
    rs.Close
    
    sConsulta = "SELECT FECACT FROM GS_PROVETREL WHERE ID=" & m_lID
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
    m_vFecAct = rs(0).Value
    
    'Inserta la denominaci�n en BD:
     If Not m_oDescripciones Is Nothing Then
        For Each oDen In m_oDescripciones
            sConsulta = "INSERT INTO GS_PROVETREL_IDI (PROVETREL, IDI, DEN) VALUES (" & m_lID & ", '" & oDen.Cod & "', " & StrToSQLNULL(oDen.DEN) & ")"
            m_oConexion.ADOCon.Execute sConsulta
            If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
        Next
    End If
    
    
    rs.Close
    Set rs = Nothing
    
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    IBaseDatos_AnyadirABaseDatos = TESError
    Exit Function
    
Error_Cls:
     
    IBaseDatos_AnyadirABaseDatos = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CRelacTipo", "IBaseDatos_AnyadirABaseDatos", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If
End Function

''' <summary>
''' Cambio del c�digo del tipo de relaci�n entre proveedores
''' </summary>
''' <param name="CodigoNuevo">C�digo a guardar</param>
''' <returns>error</returns>
''' <revision>JVS 24/08/2011</revision>
Private Function IBaseDatos_CambiarCodigo(CodigoNuevo As Variant) As TipoErrorSummit
Dim TESError As TipoErrorSummit
Dim btrans As Boolean
Dim sConsulta As String
Dim rs As adodb.Recordset
Dim sDen As String
Dim oDen As CMultiidioma


    TESError.NumError = TESnoerror

    ''' Precondicion
    
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CRelacTipo.FinalizarEdicionModificando", "No se ha establecido la conexion"
    End If
    
    If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
    
    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
    btrans = True
    
    sConsulta = "SELECT FECACT FROM GS_PROVETREL WHERE ID=" & m_lID
    Set rs = New adodb.Recordset
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
    
    If rs.eof Then
        rs.Close
        Set rs = Nothing
        TESError.NumError = TESDatoEliminado
        TESError.Arg1 = 170 ' Tipo de solicitud
        IBaseDatos_CambiarCodigo = TESError
        m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
        Exit Function
    End If
    
    
    'Comprobamos si ha habido cambios en otra sesi�n
    If m_vFecAct <> rs("FECACT") Then
        rs.Close
        Set rs = Nothing
        TESError.NumError = TESInfActualModificada
        IBaseDatos_CambiarCodigo = TESError
        m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
        Exit Function
    End If
    
    sConsulta = "UPDATE GS_PROVETREL SET COD='" & DblQuote(CodigoNuevo) & "' WHERE ID=" & m_lID
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    m_sCod = CodigoNuevo
    
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    btrans = False
    rs.Requery
    m_vFecAct = rs(0).Value
    rs.Close
    Set rs = Nothing
    TESError.NumError = TESnoerror
    IBaseDatos_CambiarCodigo = TESError
    
    Exit Function
    

Error_Cls:
    TESError = basErrores.TratarError(m_oConexion.ADOCon.Errors)
     
    If btrans Then
        m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
    End If

    On Error Resume Next
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    
    IBaseDatos_CambiarCodigo = TESError
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CRelacTipo", "IBaseDatos_CambiarCodigo", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If
End Function

Private Sub IBaseDatos_CancelarEdicion()
    ''' ! No usado por ahora
End Sub


Private Function IBaseDatos_ComprobarExistenciaEnBaseDatos() As Boolean
'
'    ''' Comprueba que no exista un tipo de solicitud con ese c�digo:
'Dim sConsulta As String
'Dim AdoRes As adodb.Recordset
'
'    Set AdoRes = New adodb.Recordset
'    sConsulta = "SELECT ID, COD, PEDIDO, FECACT FROM GS_PROVETREL WHERE COD='" & DblQuote(m_sCod) & "'"
'
'    AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
'
'    If AdoRes.eof Then
'        IBaseDatos_ComprobarExistenciaEnBaseDatos = False
'    Else
'        IBaseDatos_ComprobarExistenciaEnBaseDatos = True
'    End If
'
'    AdoRes.Close
'    Set AdoRes = Nothing
'
End Function

Private Function IBaseDatos_EliminarDeBaseDatos() As TipoErrorSummit
'Dim sConsulta As String
'Dim TESError As TipoErrorSummit
'
''********* Precondicion *******************
'If m_oConexion Is Nothing Then
'    Err.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CRelacTipo.EliminarDeBaseDatos", "No se ha establecido la conexion"
'End If
'
''******************************************
'
'on error goto error_cls:
'
'    TESError.NumError = TESnoerror
'
'    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
'
'    sConsulta = "DELETE FROM GS_PROVETREL_IDI WHERE PROVETREL=" & m_lID
'    m_oConexion.ADOCon.Execute sConsulta
'
'    sConsulta = "DELETE FROM GS_PROVETREL WHERE ID=" & m_lID
'    m_oConexion.ADOCon.Execute sConsulta
'
'    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
'    IBaseDatos_EliminarDeBaseDatos = TESError
'    Exit Function
'
'error_cls:
'
'    IBaseDatos_EliminarDeBaseDatos = basErrores.TratarError(m_oConexion.ADOCon.Errors)
'
'    m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
'
End Function

Private Function IBaseDatos_FinalizarEdicionEliminando() As TipoErrorSummit

    ''' ! No usado por ahora
    
End Function

''' <summary>
''' Actualiza los tipos de relaciones entre proveedores
''' </summary>
''' <returns>error</returns>
''' <revision>JVS 24/08/2011</revision>
Private Function IBaseDatos_FinalizarEdicionModificando() As TipoErrorSummit
Dim TESError As TipoErrorSummit
Dim btrans As Boolean
Dim sConsulta As String
Dim rs As adodb.Recordset
Dim sDen As String
Dim oDen As CMultiidioma


    TESError.NumError = TESnoerror

    ''' Precondicion
    
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CRelacTipo.FinalizarEdicionModificando", "No se ha establecido la conexion"
    End If
    
    If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
    
    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
    btrans = True
    
    sConsulta = "SELECT FECACT FROM GS_PROVETREL WHERE ID=" & m_lID
    Set rs = New adodb.Recordset
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
    
    If rs.eof Then
        rs.Close
        Set rs = Nothing
        TESError.NumError = TESDatoEliminado
        TESError.Arg1 = 170 ' Tipo de solicitud
        IBaseDatos_FinalizarEdicionModificando = TESError
        m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
        Exit Function
    End If
    
    
    'Comprobamos si ha habido cambios en otra sesi�n
    If m_vFecAct <> rs("FECACT") Then
        rs.Close
        Set rs = Nothing
        TESError.NumError = TESInfActualModificada
        IBaseDatos_FinalizarEdicionModificando = TESError
        m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
        Exit Function
    End If
    
    sConsulta = "UPDATE GS_PROVETREL SET PEDIDO=" & BooleanToSQLBinary(m_bPedido) & " WHERE ID=" & m_lID
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    sDen = ""
    If Not m_oDescripciones Is Nothing Then
        For Each oDen In m_oDescripciones
            sConsulta = "UPDATE GS_PROVETREL_IDI SET DEN=" & StrToSQLNULL(oDen.DEN) & " WHERE PROVETREL=" & m_lID & " AND IDI='" & oDen.Cod & "'"
            m_oConexion.ADOCon.Execute sConsulta
            If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
        Next
    End If
        
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    btrans = False
    rs.Requery
    m_vFecAct = rs(0).Value
    rs.Close
    Set rs = Nothing
    TESError.NumError = TESnoerror
    IBaseDatos_FinalizarEdicionModificando = TESError
    
    Exit Function
    

Error_Cls:
    TESError = basErrores.TratarError(m_oConexion.ADOCon.Errors)
     
    If btrans Then
        m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
    End If

    On Error Resume Next
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    
    IBaseDatos_FinalizarEdicionModificando = TESError
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CRelacTipo", "IBaseDatos_FinalizarEdicionModificando", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If

End Function

''' <summary>
''' Comprueba fecha de actualizaci�n
''' </summary>
''' <param optional name="Bloquear">Con bloqueo</param>
''' <param optional name="UsuarioBloqueo">Usuario de bloqueo</param>
''' <returns>error</returns>
''' <revision>JVS 24/08/2011</revision>
Private Function IBaseDatos_IniciarEdicion(Optional ByVal Bloquear As Boolean, Optional ByVal UsuarioBloqueo As String) As TipoErrorSummit
    Dim TESError As TipoErrorSummit
    Dim sSQL As String
    Dim rs As adodb.Recordset
        
    TESError.NumError = TESnoerror
    
    ''' Precondicion
    
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CRelacTipo.IniciarEdicion", "No se ha establecido la conexion"
    End If
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
    Set rs = New adodb.Recordset
    rs.Open "SELECT FECACT FROM GS_PROVETREL WHERE ID=" & m_lID, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
    
    If rs.eof Then
        rs.Close
        Set rs = Nothing
        TESError.NumError = TESDatoEliminado
        TESError.Arg1 = 170 ' Tipo de solicitud
        IBaseDatos_IniciarEdicion = TESError
        Exit Function
    End If
     
     ''' Si hay datos diferentes, devolver la condicion
    
    If m_vFecAct <> rs(0).Value Then
        TESError.NumError = TESInfActualModificada
        rs.Close
        Set rs = Nothing
        IBaseDatos_IniciarEdicion = TESError
        Exit Function
    End If
    
    rs.Close
    Set rs = Nothing
    TESError.NumError = TESnoerror
    IBaseDatos_IniciarEdicion = TESError
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CRelacTipo", "IBaseDatos_IniciarEdicion", ERR, Erl)
      Exit Function
   End If
    
End Function



