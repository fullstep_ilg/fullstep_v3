VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CConfCumplimentaciones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

''' *** Clase: CConfCumplimentaciones
''' *** Creacion: 30/03/2005 (MMV)

''' Variables que almacenan las propiedades de los objetos
Private m_Col As Collection
Private m_bEOF As Boolean
Private m_oConexion As CConexion

''' Control de errores
Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum

                    
Public Function Add(ByVal lId As Long, ByVal oSolicitud As CSolicitud, ByVal oCampo As CFormItem, Optional ByVal vPaso As Variant, Optional ByVal vSolicProve As Variant, _
                    Optional ByVal bVisible As Boolean, Optional ByVal bEscritura As Boolean, Optional ByVal bObl As Boolean, _
                    Optional ByVal FecAct As Variant, Optional ByVal Orden As Variant, Optional ByVal varIndice As Variant, _
                    Optional ByVal EsEstado As Variant) As CConfCumplimentacion
                    
    Dim objnewmember As CConfCumplimentacion
    
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New CConfCumplimentacion
    
    objnewmember.Id = lId
    Set objnewmember.Solicitud = oSolicitud
    Set objnewmember.Campo = oCampo
    
    If IsMissing(vPaso) Then
        objnewmember.Paso = Null
    Else
        objnewmember.Paso = vPaso
    End If
    
    If IsMissing(vSolicProve) Then
        objnewmember.SolicitanteProv = Null
    Else
        objnewmember.SolicitanteProv = vSolicProve
    End If
    
    objnewmember.Visible = bVisible
    objnewmember.Escritura = bEscritura
    objnewmember.Obligatorio = bObl
    
    If Not IsMissing(Orden) Then
        objnewmember.Orden = Orden
    Else
        objnewmember.Orden = Null
    End If
    
    If Not IsMissing(FecAct) Then
        objnewmember.FecAct = FecAct
    Else
        objnewmember.FecAct = Null
    End If
    
    Set objnewmember.Conexion = m_oConexion
    
    If Not IsMissing(varIndice) And Not IsNull(varIndice) Then
        objnewmember.Indice = varIndice
        m_Col.Add objnewmember, CStr(varIndice)
    Else
        m_Col.Add objnewmember, CStr(lId)
    End If
    
    If Not IsMissing(EsEstado) Then
        objnewmember.EsEstado = EsEstado
    Else
        objnewmember.EsEstado = 0
    End If
    
    Set Add = objnewmember
    
    Set objnewmember = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CConfCumplimentaciones", "Add", ERR, Erl)
      Exit Function
   End If
    
End Function
                    

Public Property Get Item(vntIndexKey As Variant) As CConfCumplimentacion
On Error GoTo NoSeEncuentra:

    ''' Devolvemos el item de la coleccion privada
    
    Set Item = m_Col(vntIndexKey)
     
    Exit Property

NoSeEncuentra:

    Set Item = Nothing
    
End Property
Public Property Get eof() As Boolean

    ''' * Objetivo: Devolver variable privada EOF
    ''' * Recibe: Nada
    ''' * Devuelve: Variable privada EOF
    
    eof = m_bEOF
    
End Property
Friend Property Set Conexion(ByVal con As CConexion)

    ''' * Objetivo: Dar valor a la variable privada Conexion
    ''' * Recibe: Conexion
    ''' * Devuelve: Nada
    
    Set m_oConexion = con
    
End Property
Friend Property Get Conexion() As CConexion

    ''' * Objetivo: Devolver variable privada Conexion
    ''' * Recibe: Nada
    ''' * Devuelve: Conexion
    
    Set Conexion = m_oConexion
    
End Property
Public Property Get Count() As Long

    ''' * Objetivo: Devolver variable privada Count
    ''' * Recibe: Nada
    ''' * Devuelve: Numero de elementos de la coleccion

    If m_Col Is Nothing Then
        Count = 0
    Else
        Count = m_Col.Count
    End If

End Property
Public Sub Remove(vntIndexKey As Variant)

    ''' * Objetivo: Eliminar una solicitud de la coleccion
    ''' * Recibe: Indice de la solicitud a eliminar
    ''' * Devuelve: Nada
   
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_Col.Remove vntIndexKey
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CConfCumplimentaciones", "Remove", ERR, Erl)
      Exit Sub
   End If
    
End Sub
Private Sub Class_Initialize()

    ''' * Objetivo: Crear la coleccion

    Set m_Col = New Collection
    
End Sub
Private Sub Class_Terminate()

    ''' * Objetivo: Limpiar memoria
    Set m_Col = Nothing
    Set m_oConexion = Nothing
End Sub
Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4

    ''' ! Pendiente de revision, objetivo desconocido
    
    Set NewEnum = m_Col.[_NewEnum]

End Property


Public Function GuardarOrdenCampos(ByVal bPasos As Boolean) As TipoErrorSummit
'***********************************************************************************************
'Descripci�n:   Funci�n para guardar el orden del campo en la configuraci�n de cumplimentaci�n
'Valor que devuelve: Error
'***********************************************************************************************

Dim sConsulta As String
Dim TESError As TipoErrorSummit
Dim oConf As CConfCumplimentacion
Dim btrans As Boolean
Dim rs As adodb.Recordset

'******************* Precondicion *******************
If m_oConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CConfCumplimentaciones.GuardarOrdenCampos", "No se ha establecido la conexion"
End If
'*****************************************************


If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:

    TESError.NumError = TESnoerror
    
    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
    btrans = True
    Set rs = New adodb.Recordset
    For Each oConf In Me
        sConsulta = "UPDATE CONF_CUMPLIMENTACION SET ORDEN=" & StrToSQLNULL(oConf.Orden) & " WHERE ID=" & oConf.Id
        m_oConexion.ADOCon.Execute sConsulta
        If m_oConexion.ADOCon.Errors.Count > 0 Then
            GoTo Error_Cls
        End If
        
        If bPasos = True Then
            sConsulta = "UPDATE CONF_CUMPLIMENTACION SET ORDEN=" & StrToSQLNULL(oConf.Orden) & " WHERE SOLICITUD=" & oConf.Solicitud.Id & " AND CAMPO=" & oConf.Campo.Id
            m_oConexion.ADOCon.Execute sConsulta
            If m_oConexion.ADOCon.Errors.Count > 0 Then
                GoTo Error_Cls
            End If
        End If
        
        sConsulta = "SELECT FECACT FROM CONF_CUMPLIMENTACION WITH (NOLOCK) WHERE ID=" & oConf.Id
        rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
        If Not rs.eof Then
            oConf.FecAct = rs(0).Value
        End If
        rs.Close
        
    Next
    Set rs = Nothing
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    btrans = False
    GuardarOrdenCampos = TESError
    
    Exit Function

Error_Cls:
    GuardarOrdenCampos = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    
    If btrans Then
        m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CConfCumplimentaciones", "GuardarOrdenCampos", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If

End Function


