VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CItemProve"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CItemProve **********************************
'*             Autor : JVS
'*             Creada : 19/09/2011
'***************************************************************

Option Explicit
Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

Private m_lIDTraslado As Long
Private m_lIDItem As Long
Private m_sCodProveedor As String
Private m_sDenProveedor As String
Private m_sCodERP As String
Private m_bTransferErp As Boolean
Private m_sPorcen As String
Private m_vCantidad As Variant
Private m_vCantAdj As Variant
Private m_oItems As CItemProves
Private m_bItemModificado As Boolean

Public Property Get Items() As CItemProves
    Set Items = m_oItems
End Property
Public Property Set Items(ByVal oItems As CItemProves)
    Set m_oItems = oItems
End Property

Public Property Get idTraslado() As Long
    idTraslado = m_lIDTraslado
End Property

Public Property Let idTraslado(ByVal Data As Long)
    m_lIDTraslado = Data
End Property

Public Property Get idItem() As Long
    idItem = m_lIDItem
End Property

Public Property Let idItem(ByVal Data As Long)
    m_lIDItem = Data
End Property

Public Property Get CodProveedor() As String
    CodProveedor = m_sCodProveedor
End Property

Public Property Let CodProveedor(ByVal Data As String)
    m_sCodProveedor = Data
End Property

Public Property Get DenProveedor() As String
    DenProveedor = m_sDenProveedor
End Property

Public Property Let DenProveedor(ByVal Data As String)
    m_sDenProveedor = Data
End Property

Public Property Get CodERP() As String
    CodERP = m_sCodERP
End Property

Public Property Let CodERP(ByVal Data As String)
    m_sCodERP = Data
End Property

Public Property Get TransferErp() As Boolean
    TransferErp = m_bTransferErp
End Property
Public Property Let TransferErp(ByVal b As Boolean)
    m_bTransferErp = b
End Property

Public Property Get Porcen() As String
    Porcen = m_sPorcen
End Property

Public Property Let Porcen(ByVal Data As String)
    m_sPorcen = Data
End Property

Public Property Get Cantidad() As Variant
    Cantidad = m_vCantidad
End Property

Public Property Let Cantidad(ByVal Data As Variant)
    m_vCantidad = Data
End Property
Public Property Get CantAdj() As Variant
    CantAdj = m_vCantAdj
End Property

Public Property Let CantAdj(ByVal Data As Variant)
    m_vCantAdj = Data
End Property

Public Property Get ItemModificado() As Boolean
    ItemModificado = m_bItemModificado
End Property
Public Property Let ItemModificado(ByVal b As Boolean)
    m_bItemModificado = b
End Property


