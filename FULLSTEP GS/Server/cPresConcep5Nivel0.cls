VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cPresConcep5Nivel0"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

Implements IBaseDatos

Private m_oConexion As CConexion

Private m_sCod As String
Private m_sDen As String
Private m_sNom_Nivel1 As String
Private m_sNom_Nivel2 As String
Private m_sNom_Nivel3 As String
Private m_sNom_Nivel4 As String
Private m_vIndice As Variant

'-----
Private m_sPerfilCod As String
Private m_sUsu As String
Private m_oDenominaciones As CMultiidiomas
Private m_oNomNivel1 As CMultiidiomas
Private m_oNomNivel2 As CMultiidiomas
Private m_oNomNivel3 As CMultiidiomas
Private m_oNomNivel4 As CMultiidiomas

Private m_iSeleccionado As Integer
Private m_sUON1 As String
Private m_sUON2 As String
Private m_sUON3 As String
Private m_sUON4 As String

Public Property Let Cod(ByVal dato As String)
    m_sCod = dato
End Property
Public Property Get Cod() As String
    Cod = m_sCod
End Property

Public Property Let Den(ByVal dato As String)
    m_sDen = dato
End Property
Public Property Get Den() As String
    Den = m_sDen
End Property

Public Property Let Nom_Nivel1(ByVal dato As String)
    m_sNom_Nivel1 = dato
End Property
Public Property Get Nom_Nivel1() As String
    Nom_Nivel1 = m_sNom_Nivel1
End Property

Public Property Let Nom_Nivel2(ByVal dato As String)
    m_sNom_Nivel2 = dato
End Property
Public Property Get Nom_Nivel2() As String
    Nom_Nivel2 = m_sNom_Nivel2
End Property

Public Property Let Nom_Nivel3(ByVal dato As String)
    m_sNom_Nivel3 = dato
End Property
Public Property Get Nom_Nivel3() As String
    Nom_Nivel3 = m_sNom_Nivel3
End Property

Public Property Let Nom_Nivel4(ByVal dato As String)
    m_sNom_Nivel4 = dato
End Property
Public Property Get Nom_Nivel4() As String
    Nom_Nivel4 = m_sNom_Nivel4
End Property

Public Property Let Indice(ByVal varIndice As Variant)
    m_vIndice = varIndice
End Property
Public Property Get Indice() As Variant
    Indice = m_vIndice
End Property

Public Property Let PerfilCod(ByVal dato As String)
    m_sPerfilCod = dato
End Property

Public Property Get PerfilCod() As String
    PerfilCod = m_sPerfilCod
End Property

Public Property Let USU(ByVal dato As String)
    m_sUsu = dato
End Property
Public Property Get USU() As String
    USU = m_sUsu
End Property

Public Property Let Seleccionado(ByVal vData As Integer)
    m_iSeleccionado = vData
End Property

Public Property Get Seleccionado() As Integer
    Seleccionado = m_iSeleccionado
End Property

Public Property Let UON1(ByVal dato As String)
    m_sUON1 = dato
End Property
Public Property Get UON1() As String
    Cod = m_sUON1
End Property

Public Property Let UON2(ByVal dato As String)
    m_sUON2 = dato
End Property
Public Property Get UON2() As String
    Cod = m_sUON3
End Property

Public Property Let UON3(ByVal dato As String)
    m_sUON3 = dato
End Property
Public Property Get UON3() As String
    Cod = m_sUON3
End Property

Public Property Let UON4(ByVal dato As String)
    m_sUON4 = dato
End Property
Public Property Get UON4() As String
    Cod = m_sUON4
End Property

Friend Property Set Conexion(ByVal vData As CConexion)

    ''' * Objetivo: Dar valor a la conexion de la Destino
    ''' * Recibe: Conexion
    ''' * Devuelve: Nada

    Set m_oConexion = vData
    
End Property
Friend Property Get Conexion() As CConexion

    ''' * Objetivo: Devolver la conexion de la Destino
    ''' * Recibe: Nada
    ''' * Devuelve: Conexion

    Set Conexion = m_oConexion
    
End Property

Private Sub Class_Terminate()
    
    On Error GoTo Error_Cls:
    
    Set m_oConexion = Nothing
    Exit Sub
Error_Cls:
    Resume Next
    
End Sub


Public Property Get Denominaciones() As CMultiidiomas
    Set Denominaciones = m_oDenominaciones
End Property
Public Property Set Denominaciones(ByVal dato As CMultiidiomas)
    Set m_oDenominaciones = dato
End Property

Public Property Get NomNivel1() As CMultiidiomas
    Set NomNivel1 = m_oNomNivel1
End Property
Public Property Set NomNivel1(ByVal dato As CMultiidiomas)
    Set m_oNomNivel1 = dato
End Property

Public Property Get NomNivel2() As CMultiidiomas
    Set NomNivel2 = m_oNomNivel2
End Property
Public Property Set NomNivel2(ByVal dato As CMultiidiomas)
    Set m_oNomNivel2 = dato
End Property

Public Property Get NomNivel3() As CMultiidiomas
    Set NomNivel3 = m_oNomNivel3
End Property
Public Property Set NomNivel3(ByVal dato As CMultiidiomas)
    Set m_oNomNivel3 = dato
End Property

Public Property Get NomNivel4() As CMultiidiomas
    Set NomNivel4 = m_oNomNivel4
End Property
Public Property Set NomNivel4(ByVal dato As CMultiidiomas)
    Set m_oNomNivel4 = dato
End Property

''' <summary>
''' Guarda una partida presupuestaria de nivel 0 en la BD
''' </summary>
''' <returns>La funci�n devuelve un Tipo TipoErrorSummit que s�lo tiene un valor: el NumError, 0 si todo va bien, 3 si no se encuentra la partida presupuestaria</returns>
''' <remarks>Llamada desde frmprescon5Arbol.cmdAceptar_click(); Tiempo m�ximo < 1 seg.</remarks>
Private Function IBaseDatos_AnyadirABaseDatos() As TipoErrorSummit

Dim sConsulta As String
Dim TESError As TipoErrorSummit
Dim btrans As Boolean
Dim oDen As CMultiidioma

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
TESError.NumError = TESnoerror

If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:

    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
    btrans = True

    sConsulta = "INSERT INTO PRES5_NIV0 (COD) " ',DEN,NOM_NIVEL1,NOM_NIVEL2,NOM_NIVEL3,NOM_NIVEL4) "
    sConsulta = sConsulta & " VALUES ('" & DblQuote(m_sCod) & "' )"
    
    
    If Not m_oDenominaciones Is Nothing Then
        For Each oDen In m_oDenominaciones
            sConsulta = sConsulta & " INSERT INTO PRES5_IDIOMAS (PRES0,IDIOMA,DEN,NOM_NIVEL1,NOM_NIVEL2,NOM_NIVEL3,NOM_NIVEL4) "
            sConsulta = sConsulta & " VALUES ('" & DblQuote(m_sCod) & "','" & DblQuote(oDen.Cod) & "','" & DblQuote(oDen.Den) & "'"
            
            If Not m_oNomNivel1 Is Nothing Then
                If Not m_oNomNivel1.Item(CStr(oDen.Cod)) Is Nothing Then
                    sConsulta = sConsulta & ",'" & m_oNomNivel1.Item(oDen.Cod).Den & "' "
                Else
                    sConsulta = sConsulta & ",''"
                End If
            Else
                sConsulta = sConsulta & ",''"
            End If
            
            If Not m_oNomNivel2 Is Nothing Then
                If Not m_oNomNivel2.Item(oDen.Cod) Is Nothing Then
                    sConsulta = sConsulta & ",'" & m_oNomNivel2.Item(oDen.Cod).Den & "' "
                Else
                    sConsulta = sConsulta & ",''"
                End If
            Else
                sConsulta = sConsulta & ",''"
            End If
            
            
            If Not m_oNomNivel3 Is Nothing Then
                If Not m_oNomNivel3.Item(oDen.Cod) Is Nothing Then
                    sConsulta = sConsulta & ",'" & m_oNomNivel3.Item(oDen.Cod).Den & "' "
                Else
                    sConsulta = sConsulta & ",''"
                End If
            Else
                sConsulta = sConsulta & ",''"
            End If
            
            If Not m_oNomNivel4 Is Nothing Then
                If Not m_oNomNivel4.Item(oDen.Cod) Is Nothing Then
                    sConsulta = sConsulta & ",'" & m_oNomNivel4.Item(oDen.Cod).Den & "' "
                Else
                    sConsulta = sConsulta & ",''"
                End If
            Else
                sConsulta = sConsulta & ",''"
            End If
            
            sConsulta = sConsulta & " ) "
        Next
        
    End If
    
    'Si es un usuario que tiene permiso para gestionar arboles presupuestarios, a�adirlo a su tabla de usuario o perfiles
    If m_sUsu <> "" Then
        If m_sPerfilCod <> "" Then
            sConsulta = sConsulta & " INSERT INTO PERF_PRES5 (PERF,PRES5) "
            sConsulta = sConsulta & " VALUES ('" & m_sPerfilCod & "','" & m_sCod & "') "
        Else
            sConsulta = sConsulta & " INSERT INTO USU_PRES5 (USU,PRES5)"
            sConsulta = sConsulta & " VALUES ('" & DblQuote(m_sUsu) & "','" & DblQuote(m_sCod) & "')"
        End If
    End If
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    btrans = False
    
    IBaseDatos_AnyadirABaseDatos = TESError
    Exit Function
Error_Cls:

    IBaseDatos_AnyadirABaseDatos = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    If btrans Then m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      'Call g_oErrores.TratarError("Class Module", "cPresConcep5Nivel0", "IBaseDatos_AnyadirABaseDatos", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If
End Function

Private Function IBaseDatos_CambiarCodigo(CodigoNuevo As Variant) As TipoErrorSummit
End Function

Private Sub IBaseDatos_CancelarEdicion()
End Sub

Private Function IBaseDatos_ComprobarExistenciaEnBaseDatos() As Boolean
End Function

''' <summary>
''' Elimina una partida presupuestaria de nivel 0 en la BD
''' </summary>
''' <returns>La funci�n devuelve un Tipo TipoErrorSummit que s�lo tiene un valor: el NumError, 0 si todo va bien, 3 si no se encuentra la partida presupuestaria</returns>
''' <remarks>Llamada desde frmprescon5Arbol.cmdAceptar_click(); Tiempo m�ximo < 1 seg.</remarks>

Private Function IBaseDatos_EliminarDeBaseDatos() As TipoErrorSummit
    Dim sConsulta As String
    Dim TESError As TipoErrorSummit
    Dim btrans As Boolean

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    TESError.NumError = TESnoerror
    If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:

    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
    btrans = True
    
    sConsulta = "DELETE FROM USU_PRES5"
    sConsulta = sConsulta & " WHERE PRES5='" & DblQuote(m_sCod) & "'"
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    
    sConsulta = "DELETE FROM PERF_PRES5"
    sConsulta = sConsulta & " WHERE PRES5='" & DblQuote(m_sCod) & "'"
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    
    sConsulta = "DELETE FROM PRES5_IDIOMAS"
    sConsulta = sConsulta & " WHERE PRES0='" & DblQuote(m_sCod) & "'"
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    
    sConsulta = "DELETE FROM PRES5_NIV4 WHERE PRES0 = '" & DblQuote(m_sCod) & "'"
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    sConsulta = "DELETE FROM PRES5_NIV3 WHERE PRES0 = '" & DblQuote(m_sCod) & "'"
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    sConsulta = "DELETE FROM PRES5_NIV2 WHERE PRES0 = '" & DblQuote(m_sCod) & "'"
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    sConsulta = "DELETE FROM PRES5_NIV1 WHERE PRES0 = '" & DblQuote(m_sCod) & "'"
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    sConsulta = "DELETE FROM PARGEN_SM WHERE PRES5 = '" & DblQuote(m_sCod) & "'"
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    
    sConsulta = "DELETE FROM PRES5_NIV0"
    sConsulta = sConsulta & " WHERE COD='" & DblQuote(m_sCod) & "'"
    m_oConexion.ADOCon.Execute sConsulta

    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    btrans = False

    IBaseDatos_EliminarDeBaseDatos = TESError
    Exit Function

Error_Cls:
    TESError.NumError = TESImposibleEliminar
    'TESError.Arg1 = 205
    IBaseDatos_EliminarDeBaseDatos = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    'IBaseDatos_EliminarDeBaseDatos = TESError
    If btrans Then m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      'Call g_oErrores.TratarError("Class Module", "cPresConcep5Nivel0", "IBaseDatos_EliminarDeBaseDatos", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If
                        
End Function

Private Function IBaseDatos_FinalizarEdicionEliminando() As TipoErrorSummit
End Function

''' <summary>
''' Actualiza los datos una partida presupuestaria de nivel 0 en la BD
''' </summary>
''' <returns>La funci�n devuelve un Tipo TipoErrorSummit que s�lo tiene un valor: el NumError, 0 si todo va bien, 3 si no se encuentra la partida presupuestaria</returns>
''' <remarks>Llamada desde frmprescon5Arbol.cmdAceptar_click(); Tiempo m�ximo < 1 seg.</remarks>

Private Function IBaseDatos_FinalizarEdicionModificando() As TipoErrorSummit
    Dim TESError As TipoErrorSummit
    Dim sConsulta As String
    Dim btrans As Boolean
    Dim oDen As CMultiidioma

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    TESError.NumError = TESnoerror
    If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:

    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
    btrans = True
    If Not m_oDenominaciones Is Nothing Then
        For Each oDen In m_oDenominaciones
            sConsulta = sConsulta & " UPDATE PRES5_IDIOMAS SET DEN='" & DblQuote(oDen.Den) & "'"
            If Not m_oNomNivel1 Is Nothing Then
                If Not m_oNomNivel1.Item(CStr(oDen.Cod)) Is Nothing Then
                    sConsulta = sConsulta & ", NOM_NIVEL1 = '" & m_oNomNivel1.Item(oDen.Cod).Den & "' "
                Else
                    sConsulta = sConsulta & ", NOM_NIVEL1 = ''"
                End If
            Else
                sConsulta = sConsulta & ", NOM_NIVEL1 = ''"
            End If

            If Not m_oNomNivel2 Is Nothing Then
                If Not m_oNomNivel2.Item(CStr(oDen.Cod)) Is Nothing Then
                    sConsulta = sConsulta & ", NOM_NIVEL2 = '" & m_oNomNivel2.Item(oDen.Cod).Den & "' "
                Else
                    sConsulta = sConsulta & ", NOM_NIVEL2 = ''"
                End If
            Else
                sConsulta = sConsulta & ", NOM_NIVEL2 = ''"
            End If

            If Not m_oNomNivel3 Is Nothing Then
                If Not m_oNomNivel3.Item(CStr(oDen.Cod)) Is Nothing Then
                    sConsulta = sConsulta & ", NOM_NIVEL3 = '" & m_oNomNivel3.Item(oDen.Cod).Den & "' "
                Else
                    sConsulta = sConsulta & ", NOM_NIVEL3 = ''"
                End If
            Else
                sConsulta = sConsulta & ", NOM_NIVEL3 = ''"
            End If
            
            If Not m_oNomNivel4 Is Nothing Then
                If Not m_oNomNivel4.Item(CStr(oDen.Cod)) Is Nothing Then
                    sConsulta = sConsulta & ", NOM_NIVEL4 = '" & m_oNomNivel4.Item(oDen.Cod).Den & "' "
                Else
                    sConsulta = sConsulta & ", NOM_NIVEL4 = ''"
                End If
            Else
                sConsulta = sConsulta & ", NOM_NIVEL4 = ''"
            End If

            sConsulta = sConsulta & " WHERE PRES0='" & DblQuote(m_sCod) & "' AND PRES1 Is Null And PRES2 Is Null And PRES3 Is Null And PRES4 Is Null AND IDIOMA = '" & oDen.Cod & "' "
        Next
    End If
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    btrans = False
    Exit Function

Error_Cls:
    IBaseDatos_FinalizarEdicionModificando = TratarError(m_oConexion.ADOCon.Errors)
    If btrans Then m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      'Call g_oErrores.TratarError("Class Module", "cPresConcep5Nivel0", "IBaseDatos_FinalizarEdicionModificando", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If
End Function

''' <summary>
''' Procedimiento que recupera los datos de una partida presupuestaria y los guarda en una serie de variables a nivel de m�dulo
''' Devuelve un tipo TipoErrorSummit definido en CTiposDeDatos, que contendr� un valor 0 como numError si todo ha ido bien
''' o un n�mero de error 3 (TESDatoEliminado) si no encuentra en base de datos la partida presup buscada
''' </summary>
''' <param name="Bloquear">Boolean. Variable definida en la funci�n IniciarEdicion de la clase IBasedeDatos que no se usa en este caso</param>
''' <param name="UsuarioBloqueo">String. Variable definida en la funci�n IniciarEdicion de la clase IBasedeDatos que no se usa en este caso</param>
''' <returns>La funci�n devuelve un Tipo TipoErrorSummit que s�lo tiene un valor: el NumError, 0 si todo va bien, 3 si no se encuentra la partida presupuestaria</returns>
''' <remarks>Llamada desde frmESTRORGCCOSTE.PartidaSeleccionada, frmRecepGral.sdbgOrdenes_Change, frmRecepcion.cmdModificar_Click, frmArticuloEsp.cmdModificarEsp_Click, frmItemsWizard.sdbgArticulos_BtnClick. En frmPresupuestos1: .cmdEli_Click, .cmdModif_Click, .BajaLogica, .CambiarCodigo. Los mismos en frmPresupuestos2, frmPresupuestos3 y frmPresupuestos4. frmCONFIntegracion.sdbgIntegracion_Change, frmOFERecAdj.cmdModificarEsp_Click, frmSolicitudComentarios.cmdWorkFlowgrafico_click, frmCatalogoAdjun.cmdModificarEsp_Click, frmCatalogoAdjun.CargarEsp, etc.; Tiempo m�ximo < 1 seg.</remarks>
''' <revisado por>mmv(08/11/2011)</revisado por>
Private Function IBaseDatos_IniciarEdicion(Optional ByVal Bloquear As Boolean, Optional ByVal UsuarioBloqueo As String) As TipoErrorSummit
Dim TESError As TipoErrorSummit
Dim sConsulta As String
Dim rs As Recordset
Dim sIdioma As String
Dim adoParam As adodb.Parameter
Dim adoComm As adodb.Command
Dim sConsultaLeftJoinDen As String
    
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    TESError.NumError = TESnoerror
    
    sIdioma = gParametrosInstalacion.gIdioma
    
    Set adoComm = New adodb.Command
    Set adoComm.ActiveConnection = m_oConexion.ADOCon
    
    
    sConsulta = "SELECT DISTINCT P50.COD, P5I.NOM_NIVEL1, P5I.NOM_NIVEL2, P5I.NOM_NIVEL3, P5I.NOM_NIVEL4, P5I.DEN,P5I.IDIOMA "
    sConsulta = sConsulta & " ,CASE WHEN PU.PRES0 IS NULL THEN 0 ELSE 1 END AS SELECCIONADO "
    sConsulta = sConsulta & " FROM PRES5_NIV0 P50 WITH (NOLOCK) "
    sConsulta = sConsulta & " INNER JOIN PRES5_IDIOMAS P5I WITH (NOLOCK) ON P50.COD = P5I.PRES0 AND P5I.PRES1 IS NULL AND P5I.PRES2 IS NULL AND P5I.PRES3 IS NULL AND P5I.PRES4 IS NULL "
    sConsulta = sConsulta & " LEFT JOIN PRES5_UON PU WITH(NOLOCK) ON P50.COD=PU.PRES0 AND PU.PRES1 IS NULL AND PU.PRES2 IS NULL AND PU.PRES3 IS NULL AND PU.PRES4 IS NULL "
        
    If m_sUON1 <> "" Then
        sConsulta = sConsulta & " AND UON1='" & DblQuote(m_sUON1) & "'"
        
        If m_sUON2 <> "" Then
            sConsulta = sConsulta & " AND UON2='" & DblQuote(m_sUON2) & "'"
        Else
            sConsulta = sConsulta & " AND UON2 IS NULL"
        End If
        
        If m_sUON3 <> "" Then
            sConsulta = sConsulta & " AND UON3='" & DblQuote(m_sUON3) & "'"
        Else
            sConsulta = sConsulta & " AND UON3 IS NULL"
        End If
        
        If m_sUON4 <> "" Then
            sConsulta = sConsulta & " AND UON4='" & DblQuote(m_sUON4) & "'"
        Else
            sConsulta = sConsulta & " AND UON4 IS NULL"
        End If
    End If
    
    sConsulta = sConsulta & " WHERE COD=?"
    
    Set adoParam = adoComm.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(DblQuote(m_sCod)) + 1, Value:=DblQuote(m_sCod))
    adoComm.Parameters.Append adoParam
    
    adoComm.CommandText = sConsulta
    adoComm.CommandType = adCmdText
    adoComm.Prepared = True
    
    Set rs = New adodb.Recordset
    Set rs = adoComm.Execute
    'xxxxxxxxxxxxxxx

    If rs.eof Then
        rs.Close
        Set rs = Nothing
        TESError.NumError = TESDatoEliminado
        IBaseDatos_IniciarEdicion = TESError
        Exit Function
    End If
    
    While Not rs.eof
    
        If UCase(rs("IDIOMA").Value) = gParametrosInstalacion.gIdioma Then
        
            m_sCod = rs("COD")
            m_sDen = rs("DEN")
            m_sNom_Nivel1 = NullToStr(rs("NOM_NIVEL1"))
            m_sNom_Nivel2 = NullToStr(rs("NOM_NIVEL2"))
            m_sNom_Nivel3 = NullToStr(rs("NOM_NIVEL3"))
            m_sNom_Nivel4 = NullToStr(rs("NOM_NIVEL4"))
                                
        End If
        
        
            If m_oDenominaciones Is Nothing Then
                Set m_oDenominaciones = New CMultiidiomas
            End If
        
            If m_oNomNivel1 Is Nothing Then
                Set m_oNomNivel1 = New CMultiidiomas
            End If
            
            If m_oNomNivel2 Is Nothing Then
                Set m_oNomNivel2 = New CMultiidiomas
            End If
            If m_oNomNivel3 Is Nothing Then
                Set m_oNomNivel3 = New CMultiidiomas
            End If
            If m_oNomNivel4 Is Nothing Then
                Set m_oNomNivel4 = New CMultiidiomas
            End If
            
        
        m_oDenominaciones.Add rs("IDIOMA").Value, NullToStr(rs("DEN").Value)
        m_oNomNivel1.Add rs("IDIOMA").Value, NullToStr(rs("NOM_NIVEL1").Value)
        m_oNomNivel2.Add rs("IDIOMA").Value, NullToStr(rs("NOM_NIVEL2").Value)
        m_oNomNivel3.Add rs("IDIOMA").Value, NullToStr(rs("NOM_NIVEL3").Value)
        m_oNomNivel4.Add rs("IDIOMA").Value, NullToStr(rs("NOM_NIVEL4").Value)
        
        m_iSeleccionado = rs("SELECCIONADO")
        
        rs.MoveNext
    Wend
    

    rs.Close
    Set rs = Nothing
    IBaseDatos_IniciarEdicion = TESError
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "cPresConcep5Nivel0", "IBaseDatos_IniciarEdicion", ERR, Erl)
      Exit Function
   End If

End Function

''' <summary>
''' Devuelve un recordset con todas las denominaciones de la partida de nivel 0
''' </summary>
''' <returns>La funci�n devuelve un Tipo TipoErrorSummit que s�lo tiene un valor: el NumError, 0 si todo va bien, 3 si no se encuentra la partida presupuestaria</returns>
''' <remarks>Llamada desde frmprescon5Detalle.CargarGridDenIdiomas; Tiempo m�ximo < 1 seg.</remarks>

Public Function DevolverDenominaciones() As adodb.Recordset
    Dim sConsulta As String
    Dim rs As adodb.Recordset
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sConsulta = "SELECT PRES5_IDIOMAS.IDIOMA,PRES5_IDIOMAS.DEN,PRES5_IDIOMAS.NOM_NIVEL1,PRES5_IDIOMAS.NOM_NIVEL2,PRES5_IDIOMAS.NOM_NIVEL3,PRES5_IDIOMAS.NOM_NIVEL4"
    sConsulta = sConsulta & " FROM PRES5_IDIOMAS WITH (NOLOCK) "
    sConsulta = sConsulta & " WHERE PRES5_IDIOMAS.PRES0 = '" & DblQuote(m_sCod) & "' AND PRES5_IDIOMAS.PRES1 IS NULL AND PRES5_IDIOMAS.PRES2 IS NULL AND PRES5_IDIOMAS.PRES3 IS NULL AND PRES5_IDIOMAS.PRES4 IS NULL"
        
    Set rs = New adodb.Recordset
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    
    Set DevolverDenominaciones = rs
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "cPresConcep5Nivel0", "DevolverDenominaciones", ERR, Erl)
      Exit Function
   End If
        
End Function

