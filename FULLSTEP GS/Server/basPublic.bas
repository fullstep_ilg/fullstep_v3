Attribute VB_Name = "basPublic"
Option Explicit

Public gInstancia As String
Public oFSIS As CFSISService
Public g_iProgramando As Integer
Public g_oErrores As CErrores
''' <summary>Construye el criterio en caso de restricción a la UON del usuario y/o UONs del perfil del usuario para procesos</summary>
''' <param name="bRUsuUon">Restricción a la UON del usuario</param>
''' <param name="bRPerfUON">Restricción a las UON del perfil del usuario</param>
''' <param name="sUON1Usu">UON1 si hay restricción a la UON del usuario</param>
''' <param name="sUON2Usu">UON2 si hay restricción a la UON del usuario</param>
''' <param name="sUON3Usu">UON3 si hay restricción a la UON del usuario</param>
''' <param name="lIdPerfil">Id. perfil usuario</param>
''' <returns>Criterio</returns>
''' <remarks>Llamada desde: CProcesos.CargarTodosLosProcesosDesde</remarks>
''' <revision>LTG 03/01/2013</revision>

''' <summary>Almacena los presupuestos anuales de tipo 3 para una linea de pedido</summary>
''' <param name="IDLinea">El ID de LINEAS_PEDIDO</param>
''' <param name="bSinTrans">si es True no se abre la transacción, significa que ya hay
'''  una abierta desde otra función</param>
''' <param name="iAnyo">OPCIONAL.Año del proceso</param>
''' <param name="sGMN1">OPCIONAL.Codigo del GMN1 del proceso</param>
''' <param name="iProce">OPCIONAL.Codigo del proceso</param>
''' <param name="lItem">OPCIONAL.Id del item dentro del proceso</param>
''' <param name="vPer">OPCIONAL.Codigo de la persona</param>
''' <param name="vUsu">OPCIONAL.Codigo del usuario</param>
''' <returns>TipoErrorSummit</returns>
''' <remarks>Llamada desde: FSGSClient.frmPRESAsig.AlmacenarPres3,
''' FSGSServer.COrdenEntrega.EmitirPedidoConLineasTemporales, FSGSServer.COrdenEntrega.BucleEmitirPedidoConLineasTemporales
''' Tiempo máximo: < 1seg </remarks>
''' <revision>aam 03/02/2014</revision>
Public Function AlmacenarPresDeLineaPedido(m_oConexion As CConexion, CPresup As Object, iNivel As Integer, _
                                        ByVal IdLinea As Long, ByVal bSinTrans As Boolean, Optional ByVal iAnyo As Integer, Optional ByVal sGMN1 As String, Optional ByVal iProce As Long, Optional ByVal lItem As Variant, Optional ByVal vPer As Variant, Optional ByVal vUsu As Variant) As TipoErrorSummit

Dim sConsulta As String
Dim udtTESError As TipoErrorSummit
Dim oPresNivel4 As Object
Dim adoComm As adodb.Command
Dim adoParam As adodb.Parameter
Dim AdoRes As adodb.Recordset
Dim iResultado As Long
Dim oLineaPedido As CLineaPedido
Dim lIdLogOrden As Long
Dim lIdLogLinea As Long
Dim rs As adodb.Recordset
Dim udtEstado As String
Dim cErp As CERPInt
Dim bPedAcepProvActivo As Boolean
Dim vlsCampo As String
Dim vlb As Boolean

udtTESError.NumError = TESnoerror
Set AdoRes = New adodb.Recordset

On Error GoTo Error_Cls

If Not bSinTrans Then
    m_oConexion.BEGINTRANSACTION
    Set oLineaPedido = New CLineaPedido
    Set oLineaPedido.Conexion = m_oConexion
    sConsulta = "SELECT ORDEN FROM LINEAS_PEDIDO WITH(NOLOCK) WHERE ID=" & IdLinea
    AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    oLineaPedido.Orden = AdoRes("ORDEN").Value
    AdoRes.Close
    oLineaPedido.Id = IdLinea
    If NoHayParametro(vPer) Then vPer = Null
    If NoHayParametro(vUsu) Then vUsu = Null
End If
'Comprobacion para los diferentes niveles, en los que no haya que controlarlo se pondra la variable a true para que cumpla sin problemas
vlb = False
Select Case iNivel
    Case 3
        If Not oLineaPedido Is Nothing Then
            If oLineaPedido.OrigenInt <> FSGSReg Then
                vlb = True
            End If
        End If
    Case Else
        vlb = True
End Select
sConsulta = "DELETE FROM LINEAS_PEDIDO_PRES" & iNivel & " WHERE LINEA =" & IdLinea
m_oConexion.ADOCon.Execute sConsulta
If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls

For Each oPresNivel4 In CPresup
    sConsulta = "INSERT INTO LINEAS_PEDIDO_PRES" & iNivel & " (LINEA,PRES1,PRES2,PRES3,PRES4,PORCEN)"
    sConsulta = sConsulta & " VALUES(" & IdLinea
    Select Case oPresNivel4.IDNivel
        Case 1
                sConsulta = sConsulta & "," & oPresNivel4.Id & ",NULL,NULL,NULL"
        Case 2
                sConsulta = sConsulta & ",NULL," & oPresNivel4.Id & ",NULL,NULL"
        Case 3
                sConsulta = sConsulta & ",NULL,NULL," & oPresNivel4.Id & ",NULL"
        Case 4
                sConsulta = sConsulta & ",NULL,NULL,NULL," & oPresNivel4.Id
    End Select
    sConsulta = sConsulta & ",?)"
    Set adoComm = New adodb.Command
    Set adoComm.ActiveConnection = m_oConexion.ADOCon
    Set adoParam = adoComm.CreateParameter("PORCEN", adDouble, adParamInput, , oPresNivel4.Porcen)
    adoComm.Parameters.Append adoParam
    adoComm.CommandText = sConsulta
    adoComm.Execute
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
Next

If Not bSinTrans Then
    If oLineaPedido.GrabarEnLog Then
        If vlb Then
            Set rs = New adodb.Recordset
            ''Obtenemos el ERP :
            '' 1º buscando a partir de ORDEN_ENTREGA.ORGCOMPRAS, si este campo ERP es NULL entonces
            '' 2º a partir de EMP.SOCIEDAD
            sConsulta = "SELECT DISTINCT ERP = CASE WHEN EO.ERP IS NULL THEN ERP_SOCIEDAD.ERP ELSE EO.ERP END, "
            sConsulta = sConsulta & " ORDEN_ENTREGA.EST FROM ORDEN_ENTREGA WITH(NOLOCK) "
            sConsulta = sConsulta & " INNER JOIN EMP WITH(NOLOCK) ON ORDEN_ENTREGA.EMPRESA = EMP.ID"
            sConsulta = sConsulta & " LEFT JOIN ERP_SOCIEDAD WITH(NOLOCK) ON ERP_SOCIEDAD.SOCIEDAD = EMP.SOCIEDAD"
            sConsulta = sConsulta & " LEFT JOIN (ERP_ORGCOMPRAS EO WITH(NOLOCK) INNER JOIN SOCIEDAD_ORGCOMPRAS SO WITH(NOLOCK)"
            sConsulta = sConsulta & " ON SO.ORGCOMPRAS=EO.ORGCOMPRAS) ON EO.ORGCOMPRAS = ORDEN_ENTREGA.ORGCOMPRAS AND SO.SOCIEDAD=EMP.SOCIEDAD"
            sConsulta = sConsulta & " WHERE ORDEN_ENTREGA.ID=" & oLineaPedido.Orden
            Dim sERP As String
            rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
            If Not rs.eof Then
                sERP = NullToStr(rs.Fields("ERP").Value)
                udtEstado = NullToStr(rs.Fields("EST").Value)
            End If
            rs.Close

            If sERP <> "" Then
                Set cErp = New CERPInt
                Set cErp.Conexion = m_oConexion
                bPedAcepProvActivo = cErp.IntPedTrasAcepActivado(sERP)
                If Not bPedAcepProvActivo Or (bPedAcepProvActivo And udtEstado >= AceptadoPorProveedor) Then
                    If oLineaPedido.OrigenInt = OrigenIntegracion.FSGSIntReg Or oLineaPedido.OrigenInt = OrigenIntegracion.FSGSInt Then
                        'Actuaizo el tag de no integradado de la orden
                        sConsulta = "UPDATE ORDEN_ENTREGA SET EST_INT = 0 WHERE ID=" & oLineaPedido.Orden
                        m_oConexion.ADOCon.Execute sConsulta
                        If m_oConexion.ADOCon.Errors.Count <> 0 Then GoTo Error_Cls
                    
                        ''MultiERP:
                        sConsulta = "INSERT INTO LOG_ORDEN_ENTREGA (ACCION,ID_ORDEN_ENTREGA,ID_PEDIDO,ANYO,NUM_PEDIDO,PROVE,NUM_ORDEN,NUMEXT,TIPO,FECHA," & _
                                    "IMPORTE,MON,CAMBIO,EST,PAG,INCORRECTA,COMENT,ORIGEN,PER,USU,REFERENCIA,EMPRESA,COD_PROVE_ERP,RECEPTOR,OBS,ORGCOMPRAS," & _
                                    "ERP,VIA_PAG,TIPOPEDIDO,FAC_DIR_ENVIO,FEC_ALTA,ID_ORDEN_EST,IM_RETGARANTIA,ABONO_DESDE,ABONO_HASTA,FACTURA,IM_AUTOFACTURA," & _
                                    "COMEN_INT,COD_PROVE_ERP_FACTURA,PED_ABIERTO_TIPO,PED_ABIERTO_FECINI,PED_ABIERTO_FECFIN,NUM_PED_ERP,ACEP_PROVE,VISITADO_P," & _
                                    "ABONO,PLAN_ENTREGA,COSTES,DESCUENTOS,NOTIFICADO,TIPO_LOG_PEDIDO)"
                        sConsulta = sConsulta & " SELECT '" & Accion_Reenvio & "',ORDEN_ENTREGA.ID,ORDEN_ENTREGA.PEDIDO,ORDEN_ENTREGA.ANYO,PEDIDO.NUM,ORDEN_ENTREGA.PROVE," & _
                                    "ORDEN_ENTREGA.NUM,NUMEXT,ORDEN_ENTREGA.TIPO,ORDEN_ENTREGA.FECHA,IMPORTE,ORDEN_ENTREGA.MON,ORDEN_ENTREGA.CAMBIO,ORDEN_ENTREGA.EST,ORDEN_ENTREGA.PAG,INCORRECTA,COMENT,"
                        sConsulta = sConsulta & oLineaPedido.OrigenInt & ",PEDIDO.PER," & StrToSQLNULL(vUsu) & ",ORDEN_ENTREGA.NUM_PED_ERP,ORDEN_ENTREGA.EMPRESA," & _
                                    "ORDEN_ENTREGA.COD_PROVE_ERP,ORDEN_ENTREGA.RECEPTOR,ORDEN_ENTREGA.OBS,ORDEN_ENTREGA.ORGCOMPRAS," & StrToSQLNULL(sERP) & ",ORDEN_ENTREGA.VIA_PAG,TIPOPEDIDO,ORDEN_ENTREGA.FAC_DIR_ENVIO,ORDEN_ENTREGA.FEC_ALTA" & _
                                    ",ORDEN_ENTREGA.ORDEN_EST,ORDEN_ENTREGA.IM_RETGARANTIA,ORDEN_ENTREGA.ABONO_DESDE,ORDEN_ENTREGA.ABONO_HASTA,ORDEN_ENTREGA.FACTURA,ORDEN_ENTREGA.IM_AUTOFACTURA," & _
                                    "ORDEN_ENTREGA.COMEN_INT,ORDEN_ENTREGA.COD_PROVE_ERP_FACTURA,ORDEN_ENTREGA.PED_ABIERTO_TIPO,ORDEN_ENTREGA.PED_ABIERTO_FECINI,ORDEN_ENTREGA.PED_ABIERTO_FECFIN,ORDEN_ENTREGA.NUM_PED_ERP,ORDEN_ENTREGA.ACEP_PROVE,ORDEN_ENTREGA.VISITADO_P," & _
                                    "ORDEN_ENTREGA.ABONO,ORDEN_ENTREGA.PLAN_ENTREGA,ORDEN_ENTREGA.COSTES,ORDEN_ENTREGA.DESCUENTOS,ORDEN_ENTREGA.NOTIFICADO," & NullToDbl0(Imputacion_GS)
                        sConsulta = sConsulta & " FROM ORDEN_ENTREGA WITH(NOLOCK) INNER JOIN ORDEN_EST WITH(NOLOCK) ON ORDEN_EST.ID = ORDEN_ENTREGA.ORDEN_EST "
                        sConsulta = sConsulta & " INNER JOIN PEDIDO WITH(NOLOCK) ON PEDIDO.ID = ORDEN_ENTREGA.PEDIDO "
                        sConsulta = sConsulta & " INNER JOIN PROVE WITH(NOLOCK) ON PROVE.COD = ORDEN_ENTREGA.PROVE "
                        sConsulta = sConsulta & " WHERE  ORDEN_ENTREGA.ID = " & oLineaPedido.Orden
                        m_oConexion.ADOCon.Execute sConsulta
                        If m_oConexion.ADOCon.Errors.Count <> 0 Then GoTo Error_Cls
                        rs.Open "SELECT SCOPE_IDENTITY()", m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
                        lIdLogOrden = rs.Collect(0)
                        rs.Close
                                            
                        ''mpb : ANTES SE HACIAN JOINS CON ITEM Y ART4 para obtener GMN2,GMN3,GMN4 ya no se almacenan en LOG_LINEAS_PEDIDO (VERSION 31.600.7
                        sConsulta = "INSERT INTO LOG_LINEAS_PEDIDO (ACCION,ID_LOG_ORDEN_ENTREGA,ID_LINEAS_PEDIDO,ART_INT,UP,PREC_UP,CANT_PED,DEST,ANYO,GMN1,PROCE,ITEM,ENTREGA_OBL,FECENTREGA,FECENTREGAPROVE,EST,CAT1,CAT2,CAT3,CAT4,CAT5,OBS,FC,SOLICIT,LINEA_SOLICIT,DESCR_LIBRE,CAMPO1,VALOR1,CAMPO2,VALOR2,OBSADJUN,CENTROS,ALMACEN,PEDIDOABIERTO,ACTIVO," & _
                                    "LINEAS_EST,LINCAT,PROVEADJ,PORCEN_DESVIO,COSTES,DESCUENTOS,SM_IMPORTECOMPR,SM_IMPORTECOMPRIMPUESTOS,CAMPO_SOLICIT,CANTIDAD_PED_ABIERTO," & _
                                    "IMPORTE_PED_ABIERTO,FEC_ALTA,ORDEN_PED_ABIERTO,ADD_MATR_ART,BLOQ_MOD,LINEA_PED_ABIERTO,ERP,NUM,TIPORECEPCION,IMPORTE_PED,BAJA_LOG," & _
                                    "DEST_GENERICO,DEST_DEN,DEST_DIR,DEST_POB,DEST_CP,DEST_PROVI,DEST_PAI,DEST_TFNO,DEST_FAX,DEST_EMAIL,CLASE_HITOS_FAC) "
                        sConsulta = sConsulta & " SELECT '" & iNivel & "'," & lIdLogOrden & ",LP.ID,LP.ART_INT,LP.UP,LP.PREC_UP,LP.CANT_PED,LP.DEST,LP.ANYO,LP.GMN1,LP.PROCE,LP.ITEM,LP.ENTREGA_OBL,LP.FECENTREGA,LP.FECENTREGAPROVE,LP.EST,LP.CAT1,LP.CAT2,LP.CAT3,LP.CAT4,LP.CAT5,LP.OBS,LP.FC,LP.SOLICIT,LP.LINEA_SOLICIT,LP.DESCR_LIBRE,LP.CAMPO1,LP.VALOR1,LP.CAMPO2,LP.VALOR2,LP.OBSADJUN,LP.CENTROS,LP.ALMACEN,LP.PEDIDOABIERTO,LP.ACTIVO," & _
                                    "LP.LINEAS_EST,LP.LINCAT,LP.PROVEADJ,LP.PORCEN_DESVIO,LP.COSTES,LP.DESCUENTOS,LP.SM_IMPORTECOMPR,LP.SM_IMPORTECOMPRIMPUESTOS,LP.CAMPO_SOLICIT,LP.CANTIDAD_PED_ABIERTO," & _
                                    "LP.IMPORTE_PED_ABIERTO,LP.FEC_ALTA,LP.ORDEN_PED_ABIERTO,LP.ADD_MATR_ART,LP.BLOQ_MOD,LP.LINEA_PED_ABIERTO," & StrToSQLNULL(sERP) & ",LP.NUM,LP.TIPORECEPCION," & _
                                    "LP.IMPORTE_PED,LP.BAJA_LOG,LP.DEST_GENERICO,LP.DEST_DEN,LP.DEST_DIR,LP.DEST_POB,LP.DEST_CP,LP.DEST_PROVI,LP.DEST_PAI,LP.DEST_TFNO,LP.DEST_FAX,LP.DEST_EMAIL,LP.CLASE_HITOS_FAC "
                        sConsulta = sConsulta & " FROM LINEAS_PEDIDO LP WITH(NOLOCK) WHERE LP.ID=" & IdLinea
                   
                        m_oConexion.ADOCon.Execute sConsulta
                        If m_oConexion.ADOCon.Errors.Count <> 0 Then GoTo Error_Cls
                        
                        rs.Open "SELECT SCOPE_IDENTITY()", m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
                        lIdLogLinea = rs.Collect(0)
                        rs.Close
                        
                        sConsulta = "INSERT INTO LOG_PEDIDO_PRES" & iNivel & " (ID_LOG_ORDEN_ENTREGA,ID_LOG_LINEAS_PEDIDO,ID_LINEA,PRES1,PRES2,PRES3,PRES4,PORCEN) "
                        sConsulta = sConsulta & " SELECT " & lIdLogOrden & "," & lIdLogLinea & "," & IdLinea & ",PRES1,PRES2,PRES3,PRES4,PORCEN FROM LINEAS_PEDIDO_PRES" & iNivel & " WITH(NOLOCK) WHERE LINEA=" & IdLinea
                        m_oConexion.ADOCon.Execute sConsulta
                        If m_oConexion.ADOCon.Errors.Count <> 0 Then GoTo Error_Cls
                        
                    End If
                End If
            End If
        End If
    End If
End If

'Solo se deben realizar los procesos de sincronizacion si está activada la configuración y la linea de pedido NO se a creado desde un artículo del propio pedido
If gParametrosGenerales.gbSincrPresItem And iAnyo > 0 Then
    Set AdoRes = New adodb.Recordset
    Select Case iNivel
        Case 3, 4
            vlsCampo = "PRES" & iNivel - 2
        Case Else
            vlsCampo = "PRESANU" & iNivel
    End Select
    sConsulta = "SELECT " & vlsCampo & " FROM PROCE_DEF WITH(NOLOCK) WHERE ANYO=" & iAnyo & " AND GMN1='" & DblQuote(sGMN1) & "' AND PROCE=" & iProce
    AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    If AdoRes(0).Value <> 3 Then 'Cambio lo del proceso
        sConsulta = "UPDATE PROCE_DEF SET " & vlsCampo & "=3 WHERE ANYO=" & iAnyo & " AND GMN1='" & DblQuote(sGMN1) & "' AND PROCE=" & iProce
        m_oConexion.ADOCon.Execute sConsulta
        If m_oConexion.ADOCon.Errors.Count <> 0 Then GoTo Error_Cls
        sConsulta = "DELETE FROM PROCE_GRUPO_DEF WHERE ANYO=" & iAnyo & " AND GMN1='" & DblQuote(sGMN1) & "' AND PROCE=" & iProce & " AND DATO='" & vlsCampo & "'"
        m_oConexion.ADOCon.Execute sConsulta
        If m_oConexion.ADOCon.Errors.Count <> 0 Then GoTo Error_Cls
        If AdoRes(0).Value = 1 Then
            sConsulta = "DELETE FROM PROCE_PRES" & iNivel & " WHERE ANYO=" & iAnyo & " AND GMN1='" & DblQuote(sGMN1) & "' AND PROCE=" & iProce
            m_oConexion.ADOCon.Execute sConsulta
            If m_oConexion.ADOCon.Errors.Count <> 0 Then GoTo Error_Cls
        End If
        If AdoRes(0).Value = 2 Then
            sConsulta = "DELETE FROM PROCE_GR_PRES" & iNivel & " WHERE ANYO=" & iAnyo & " AND GMN1='" & DblQuote(sGMN1) & "' AND PROCE=" & iProce
            m_oConexion.ADOCon.Execute sConsulta
            If m_oConexion.ADOCon.Errors.Count <> 0 Then GoTo Error_Cls
        End If
    End If
    AdoRes.Close
    Set AdoRes = Nothing

    udtTESError = CPresup.AlmacenarPresDeItem(iAnyo, sGMN1, iProce, lItem, True)
    If udtTESError.NumError <> TESnoerror Then
        AlmacenarPresDeLineaPedido = udtTESError
        If Not bSinTrans Then
            m_oConexion.ROLLBACKTRANSACTION
        End If
        Set adoComm = Nothing
        Set adoParam = Nothing
        Exit Function
    End If
    
    vlsCampo = ""
    Select Case iNivel
        Case 1
            vlsCampo = "AR_ITEM_MES_PARPROY"
        Case 2
            vlsCampo = "AR_ITEM_MES_PARCON"
        Case 3, 4
            vlsCampo = "AR_ITEM_MES_PRESCON" & iNivel & "_"
            sConsulta = "DELETE FROM AR_ITEM_PRESCON" & iNivel & " WHERE ANYO=" & iAnyo & " AND GMN1='" & DblQuote(sGMN1) & "' AND PROCE=" & iProce & " AND ITEM =" & lItem
            m_oConexion.ADOCon.Execute sConsulta
            If m_oConexion.ADOCon.Errors.Count <> 0 Then GoTo Error_Cls
    End Select
    
    sConsulta = "DELETE FROM " & vlsCampo & "1 WHERE ANYO=" & iAnyo & " AND GMN1='" & DblQuote(sGMN1) & "' AND PROCE=" & iProce & " AND ITEM =" & lItem
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count <> 0 Then GoTo Error_Cls
    sConsulta = "DELETE FROM " & vlsCampo & "2 WHERE ANYO=" & iAnyo & " AND GMN1='" & DblQuote(sGMN1) & "' AND PROCE=" & iProce & " AND ITEM =" & lItem
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count <> 0 Then GoTo Error_Cls
    sConsulta = "DELETE FROM " & vlsCampo & "3 WHERE ANYO=" & iAnyo & " AND GMN1='" & DblQuote(sGMN1) & "' AND PROCE=" & iProce & " AND ITEM =" & lItem
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count <> 0 Then GoTo Error_Cls
    sConsulta = "DELETE FROM " & vlsCampo & "4 WHERE ANYO=" & iAnyo & " AND GMN1='" & DblQuote(sGMN1) & "' AND PROCE=" & iProce & " AND ITEM =" & lItem
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count <> 0 Then GoTo Error_Cls
    Set adoComm = New adodb.Command
    Set adoComm.ActiveConnection = m_oConexion.ADOCon
    Set adoParam = adoComm.CreateParameter("ANYO", adSmallInt, adParamInput, , iAnyo)
    adoComm.Parameters.Append adoParam
    Set adoParam = adoComm.CreateParameter("GMN1", adVarChar, adParamInput, 50, sGMN1)
    adoComm.Parameters.Append adoParam
    Set adoParam = adoComm.CreateParameter("PROCE", adInteger, adParamInput, , iProce)
    adoComm.Parameters.Append adoParam
    Set adoParam = adoComm.CreateParameter("ITEM", adInteger, adParamInput, , lItem)
    adoComm.Parameters.Append adoParam
    Set adoParam = adoComm.CreateParameter("RES", adInteger, adParamOutput)
    adoComm.Parameters.Append adoParam
    adoComm.CommandType = adCmdStoredProc
    adoComm.CommandText = "SP_AR_CALCULAR_PRES" & iNivel
    adoComm.CommandTimeout = 1800
    adoComm.Execute
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    iResultado = NullToDbl0(adoComm.Parameters(4).Value)
    Set adoComm = Nothing
    
    If iResultado <> 0 Then
        udtTESError.NumError = TESErrorAhorrosCalculando
        AlmacenarPresDeLineaPedido = udtTESError
        If Not bSinTrans Then
            m_oConexion.ROLLBACKTRANSACTION
        End If
        Exit Function
    End If
End If
        
If Not bSinTrans Then
    m_oConexion.COMMITTRANSACTION
End If

If Not bSinTrans Then
    If oLineaPedido.GrabarEnLog Then
        If vlb Then
            'Llamada al servicio de integración WCF.
            Set basUtilidades.Conexion = m_oConexion
            basUtilidades.LlamarFSIS EntidadIntegracion.PED_directo, oLineaPedido.Orden
        End If
    End If
End If

AlmacenarPresDeLineaPedido = udtTESError
Exit Function

Error_Cls:
    
    If m_oConexion.ADOCon.Errors.Count > 0 Then
        AlmacenarPresDeLineaPedido = basErrores.TratarError(m_oConexion.ADOCon.Errors)
        m_oConexion.ROLLBACKTRANSACTION
        Set adoComm = Nothing
        Set adoParam = Nothing
    ElseIf ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Module", "basPublic", "AlmacenarPresDeLineaPedido", ERR, Erl)
    End If
End Function



Public Function ConstruirCriterioUONUsuPerfProce(ByVal bRUsuUon As Boolean, ByVal bRPerfUON As Boolean, ByVal sUON1Usu As String, ByVal sUON2Usu As String, _
        ByVal sUON3Usu As String, ByVal lIdPerfil As Long) As String
    Dim sConsulta As String
    Dim sConsulta1 As String
    Dim sConsulta2 As String
        
    If bRUsuUon Then
        sConsulta1 = "SELECT DISTINCT PROCE.ANYO,PROCE.GMN1,PROCE.COD"
        sConsulta1 = sConsulta1 & " FROM PROCE WITH (NOLOCK)"
        sConsulta1 = sConsulta1 & " INNER JOIN VW_DIST_PROCE A ON A.ANYO=PROCE.ANYO AND A.GMN1=PROCE.GMN1 AND A.PROCE=PROCE.COD"
        If sUON1Usu <> "" Then
            sConsulta1 = sConsulta1 & " AND A.UON1=" & StrToSQLNULL(sUON1Usu)
            If sUON2Usu <> "" Then
                sConsulta1 = sConsulta1 & " AND COALESCE(A.UON2," & StrToSQLNULL(sUON2Usu) & ")= " & StrToSQLNULL(sUON2Usu)
                If sUON3Usu <> "" Then
                    sConsulta1 = sConsulta1 & " AND COALESCE(A.UON3," & StrToSQLNULL(sUON3Usu) & ")= " & StrToSQLNULL(sUON3Usu)
                End If
            End If
        End If
    End If
        
    If bRPerfUON Then
        sConsulta2 = "SELECT DISTINCT PROCE.ANYO,PROCE.GMN1,PROCE.COD"
        sConsulta2 = sConsulta2 & " FROM PROCE WITH (NOLOCK)"
        sConsulta2 = sConsulta2 & " INNER JOIN VW_DIST_PROCE A ON A.ANYO=PROCE.ANYO AND A.GMN1=PROCE.GMN1 AND A.PROCE=PROCE.COD"
        sConsulta2 = sConsulta2 & " INNER JOIN PERF_UON PU WITH (NOLOCK) ON A.UON1=PU.UON1 AND COALESCE(A.UON2,PU.UON2,'')=COALESCE(PU.UON2,A.UON2,'') AND COALESCE(A.UON3,PU.UON3,'')=COALESCE(PU.UON3,A.UON3,'')  "
        sConsulta2 = sConsulta2 & " AND PU.PERF=" & lIdPerfil & " "
    End If
        
    If bRUsuUon And bRPerfUON Then
        sConsulta = sConsulta1 & " UNION " & sConsulta2
    ElseIf bRUsuUon And Not bRPerfUON Then
        sConsulta = sConsulta1
    Else
        sConsulta = sConsulta2
    End If
            
    ConstruirCriterioUONUsuPerfProce = sConsulta
End Function

''' <summary>Construye el criterio en caso de restricción a la UON del usuario y/o UONs del perfil del usuario para contratos</summary>
''' <param name="bRUsuUon">Restricción a la UON del usuario</param>
''' <param name="bRPerfUON">Restricción a las UON del perfil del usuario</param>
''' <param name="UON1">UON1 si hay restricción a la UON del usuario</param>
''' <param name="UON2">UON2 si hay restricción a la UON del usuario</param>
''' <param name="UON3">UON3 si hay restricción a la UON del usuario</param>
''' <param name="UON4">UON4 si hay restricción a la UON del usuario</param>
''' <param name="lIdPerfil">Id. perfil usuario</param>
''' <returns>Criterio</returns>
''' <remarks>Llamada desde: CContratosPres.CargarTodosLosContratos</remarks>
''' <revision>LTG 03/01/2013</revision>

Public Function ConstruirCriterioUONUsuPerfContratos(ByVal bRUsuUon As Boolean, ByVal bRPerfUON As Boolean, ByVal UON1 As String, ByVal UON2 As String, _
        ByVal UON3 As String, ByVal UON4 As String, ByVal lIdPerfil As Long) As String
    Dim sConsulta As String
    Dim sConsulta1 As String
    Dim sConsulta2 As String
        
    If bRUsuUon Then
        sConsulta1 = "SELECT PU.PRES0,PU.PRES1,PU.PRES2,PU.PRES3,PU.PRES4,PU.UON1,PU.UON2,PU.UON3,PU.UON4"
        sConsulta1 = sConsulta1 & " FROM PRES5_UON PU WITH (NOLOCK)"
        sConsulta1 = sConsulta1 & " WHERE 1=1"
        If UON1 <> "" Then
            sConsulta1 = sConsulta1 & " AND PU.UON1='" & DblQuote(UON1) & "'"
            If UON2 <> "" Then
                sConsulta1 = sConsulta1 & " AND PU.UON2='" & DblQuote(UON2) & "'"
                If UON3 <> "" Then
                    sConsulta1 = sConsulta1 & " AND PU.UON3='" & DblQuote(UON3) & "'"
                    If UON4 <> "" Then
                        sConsulta1 = sConsulta1 & " AND PU.UON4='" & DblQuote(UON4) & "'"
                    Else
                        sConsulta1 = sConsulta1 & " AND PU.UON4 IS NULL"
                    End If
                Else
                    sConsulta1 = sConsulta1 & " AND PU.UON3 IS NULL"
                End If
            Else
                sConsulta1 = sConsulta1 & " AND PU.UON2 IS NULL"
            End If
        End If
    End If
        
    If bRPerfUON Then
        sConsulta2 = "SELECT PU.PRES0,PU.PRES1,PU.PRES2,PU.PRES3,PU.PRES4,PU.UON1,PU.UON2,PU.UON3,PU.UON4"
        sConsulta2 = sConsulta2 & " FROM PRES5_UON PU WITH (NOLOCK)"
        sConsulta2 = sConsulta2 & " INNER JOIN PERF_UON PUN WITH (NOLOCK) ON PUN.UON1=PU.UON1 AND (PUN.UON2=PU.UON2 OR PUN.UON2 IS NULL)"
        sConsulta2 = sConsulta2 & " AND (PUN.UON3=PU.UON3 OR PUN.UON3 IS NULL) AND PUN.PERF=" & lIdPerfil & " "
    End If
        
    If bRUsuUon And bRPerfUON Then
        sConsulta = sConsulta1 & " UNION " & sConsulta2
    ElseIf bRUsuUon And Not bRPerfUON Then
        sConsulta = sConsulta1
    Else
        sConsulta = sConsulta2
    End If
            
    ConstruirCriterioUONUsuPerfContratos = sConsulta
End Function

''' <summary>Construye el criterio en caso de restricción a la UON del usuario y/o UONs del perfil del usuario para personas</summary>
''' <param name="bRUsuUon">Restricción a la UON del usuario</param>
''' <param name="bRPerfUON">Restricción a las UON del perfil del usuario</param>
''' <param name="sUON1Usu">UON1 si hay restricción a la UON del usuario</param>
''' <param name="sUON2Usu">UON2 si hay restricción a la UON del usuario</param>
''' <param name="sUON3Usu">UON3 si hay restricción a la UON del usuario</param>
''' <param name="lIdPerfil">Id. perfil usuario</param>
''' <param name="vUON1Filtro">Uon 1 del filtro pantalla</param>
''' <param name="vUON2Filtro">Uon 2 del filtro pantalla</param>
''' <param name="vUON3Filtro">Uon 3 del filtro pantalla</param>
''' <returns>Criterio</returns>
''' <remarks>Llamada desde: CInstancias.BuscarSolicituds</remarks>
''' <revision>LTG 03/01/2013</revision>

Public Function ConstruirCriterioUONUsuPerfPer(ByVal bRUsuUon As Boolean, ByVal bRPerfUON As Boolean, ByVal sUON1Usu As String, ByVal sUON2Usu As String, _
        ByVal sUON3Usu As String, ByVal lIdPerfil As Long, Optional ByVal sUON1Filtro As String = "", Optional ByVal sUON2Filtro As String = "", _
        Optional ByVal sUON3Filtro As String = "") As String
    Dim sConsulta As String
    Dim sConsulta1 As String
    Dim sConsulta2 As String
    Dim sConsultaComun As String
       
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sUON1Filtro <> "" Then
        sConsultaComun = " AND P.UON1='" & DblQuote(sUON1Filtro) & "'"
        
        If sUON2Filtro <> "" Then
            sConsultaComun = sConsultaComun & " AND P.UON2='" & DblQuote(sUON2Filtro) & "'"
            
            If sUON3Filtro <> "" Then
                sConsultaComun = sConsultaComun & " AND P.UON3='" & DblQuote(sUON3Filtro) & "'"
            End If
        End If
    End If
       
    If bRUsuUon Then
        sConsulta1 = "SELECT P.COD,P.APE,P.NOM,P.DEP,P.UON1,P.UON2,P.UON3"
        sConsulta1 = sConsulta1 & " FROM PER P WITH (NOLOCK)"
        If sUON1Usu <> "" Then
            sConsulta1 = sConsulta1 & " WHERE P.UON1='" & DblQuote(sUON1Usu) & "'"
            
            If sUON2Usu <> "" Then
                sConsulta1 = sConsulta1 & " AND P.UON2='" & DblQuote(sUON2Usu) & "'"
                
                If sUON3Usu <> "" Then
                    sConsulta1 = sConsulta1 & " AND P.UON3='" & DblQuote(sUON3Usu) & "'"
                End If
            End If
        Else
            sConsulta1 = sConsulta1 & " WHERE 1=1"
        End If
        
        sConsulta1 = sConsulta1 & sConsultaComun
    End If

    If bRPerfUON Then
        sConsulta2 = "SELECT P.COD,P.APE,P.NOM,P.DEP,P.UON1,P.UON2,P.UON3"
        sConsulta2 = sConsulta2 & " FROM PER P WITH (NOLOCK)"
        sConsulta2 = sConsulta2 & " INNER JOIN PERF_UON PU WITH (NOLOCK) ON (PU.UON1=P.UON1 OR PU.UON1 IS NULL)"
        sConsulta2 = sConsulta2 & " AND (PU.UON2=P.UON2 OR PU.UON2 IS NULL) AND (PU.UON3=P.UON3 OR PU.UON3 IS NULL)"
        sConsulta2 = sConsulta2 & " AND PU.PERF=" & lIdPerfil
        sConsulta2 = sConsulta2 & " WHERE 1=1 " & sConsultaComun
    End If
    
    If bRUsuUon And bRPerfUON Then
        sConsulta = sConsulta1 & " UNION " & sConsulta2
    ElseIf bRUsuUon And Not bRPerfUON Then
        sConsulta = sConsulta1
    Else
        sConsulta = sConsulta2
    End If
    
    ConstruirCriterioUONUsuPerfPer = sConsulta
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Módulo", "basPublic", "ConstruirCriterioUONUsuPerfPer", ERR, Erl)
      Exit Function
   End If

End Function

''' <summary>Construye el criterio en caso de restricción a la UON del usuario y/o UONs del perfil del usuario para compradores</summary>
''' <param name="bRUsuUon">Restricción a la UON del usuario</param>
''' <param name="bRPerfUON">Restricción a las UON del perfil del usuario</param>
''' <param name="sUON1Usu">UON1 si hay restricción a la UON del usuario</param>
''' <param name="sUON2Usu">UON2 si hay restricción a la UON del usuario</param>
''' <param name="sUON3Usu">UON3 si hay restricción a la UON del usuario</param>
''' <param name="lIdPerfil">Id. perfil usuario</param>
''' <param name="bCommand">Indica si la consulta se ejecutará en un command</param>
''' <returns>Criterio</returns>
''' <remarks>Llamada desde: CInstancias.BuscarSolicitudes</remarks>
''' <revision>LTG 03/01/2013</revision>

Public Function ConstruirCriterioUONUsuPerfCom(ByVal bRUsuUon As Boolean, ByVal bRPerfUON As Boolean, ByVal sUON1Usu As String, ByVal sUON2Usu As String, _
        ByVal sUON3Usu As String, ByVal lIdPerfil As Long, Optional ByVal bCommand As Boolean) As String
    Dim sConsulta As String
    Dim sConsulta1 As String
    Dim sConsulta2 As String
    Dim sCadVacia As String

    If bRUsuUon Then
        sConsulta1 = "SELECT C.EQP,C.COD,C.NOM,C.APE,C.FAX,C.EMAIL,C.TFNO,C.CAR,C.TFNO2"
        sConsulta1 = sConsulta1 & " FROM COM C WITH (NOLOCK)"
        sConsulta1 = sConsulta1 & " WHERE 1=1"
        If bCommand Then
            If sUON3Usu <> "" Then
                sConsulta1 = sConsulta1 & " AND C.UON1=? AND C.UON2=? AND C.UON3=?"
            ElseIf sUON2Usu <> "" Then
                sConsulta1 = sConsulta1 & " AND C.UON1=? AND C.UON2=? AND C.UON3 IS NULL"
            ElseIf sUON1Usu <> "" Then
                sConsulta1 = sConsulta1 & " AND C.UON1=? AND C.UON2 IS NULL AND C.UON3 IS NULL"
            Else
                sConsulta1 = sConsulta1 & " AND C.UON1 IS NULL AND C.UON2 IS NULL AND C.UON3 IS NULL"
            End If
        Else
            If sUON3Usu <> "" Then
                sConsulta1 = sConsulta1 & " AND C.UON1= '" & DblQuote(sUON1Usu) & "' AND C.UON2 ='" & DblQuote(sUON2Usu) & "' AND C.UON3= '" & DblQuote(sUON3Usu) & "'"
            ElseIf sUON2Usu <> "" Then
                sConsulta1 = sConsulta1 & " AND C.UON1= '" & DblQuote(sUON1Usu) & "' AND C.UON2 ='" & DblQuote(sUON2Usu) & "' AND C.UON3 IS NULL"
            ElseIf sUON1Usu <> "" Then
                sConsulta1 = sConsulta1 & " AND C.UON1= '" & DblQuote(sUON1Usu) & "' AND C.UON2 IS NULL AND C.UON3 IS NULL"
            Else
                sConsulta1 = sConsulta1 & " AND C.UON1 IS NULL AND C.UON2 IS NULL AND C.UON3 IS NULL"
            End If
        End If
    End If

    If bRPerfUON Then
        If bCommand Then
            sCadVacia = "''''"
        Else
            sCadVacia = "''"
        End If
    
        sConsulta2 = "SELECT C.EQP,C.COD,C.NOM,C.APE,C.FAX,C.EMAIL,C.TFNO,C.CAR,C.TFNO2"
        sConsulta2 = sConsulta2 & " FROM COM C WITH (NOLOCK)"
        sConsulta2 = sConsulta2 & " INNER JOIN PERF_UON PU WITH (NOLOCK) ON C.UON1=PU.UON1"
        sConsulta2 = sConsulta2 & " AND coalesce(C.UON2,PU.UON2," & sCadVacia & ")=coalesce(PU.UON2,c.UON2," & sCadVacia & ")"
        sConsulta2 = sConsulta2 & " AND coalesce(C.UON3,PU.UON3," & sCadVacia & ")=coalesce(PU.UON3,C.UON3," & sCadVacia & ")"
        sConsulta2 = sConsulta2 & " AND PU.PERF=" & lIdPerfil & " "
    End If
    
    If bRUsuUon And bRPerfUON Then
        sConsulta = sConsulta1 & " UNION " & sConsulta2
    ElseIf bRUsuUon And Not bRPerfUON Then
        sConsulta = sConsulta1
    Else
        sConsulta = sConsulta2
    End If
    
    ConstruirCriterioUONUsuPerfCom = sConsulta
End Function

''' <summary>Construye el criterio en caso de restricción a la UON del usuario y/o UONs del perfil del usuario para presupuestos</summary>
''' <param name="sTablaPresAlias">Alias tabla presupuestos</param>
''' <param name="bRUsuUon">Restricción a las UON del usuario</param>
''' <param name="bRPerfUON">Restricción a las UON del perfil del usuario</param>
''' <param name="lIdPerfil">Id perfil usuario</param>
''' <param name="UON1">UON1 si hay restricción a la UON del usuario</param>
''' <param name="UON2">UON2 si hay restricción a la UON del usuario</param>
''' <param name="UON3">UON3 si hay restricción a la UON del usuario</param>
''' <returns>Criterio</returns>
''' <remarks>Llamada desde: CInstancias.BuscarSolicituds</remarks>
''' <revision>LTG 24/01/2013</revision>

Public Function ConstruirCriterioUONUsuPerfPres(ByVal sTablaPresAlias As String, ByVal bRUsuUon As Boolean, ByVal bRPerfUON As Boolean, _
        ByVal lIdPerfil As Long, Optional ByVal UON1 As String, Optional ByVal UON2 As String, Optional ByVal UON3 As String) As String
    Dim sConsulta As String
    Dim sConsulta1 As String
    Dim sConsulta2 As String
       
    If bRUsuUon Then
        sConsulta1 = "SELECT P.UON1,P.UON2,P.UON3"
        sConsulta1 = sConsulta1 & " FROM " & sTablaPresAlias & " P WITH (NOLOCK)"
        sConsulta1 = sConsulta1 & " WHERE 1=1 "
         
        If Not NoHayParametro(UON1) Then
            sConsulta1 = sConsulta1 & " AND P.UON1='" & DblQuote(UON1) & "'"
        End If
        If Not NoHayParametro(UON2) Then
            sConsulta1 = sConsulta1 & " AND P.UON2='" & DblQuote(UON2) & "'"
        End If
        If Not NoHayParametro(UON3) Then
            sConsulta1 = sConsulta1 & " AND P.UON3='" & DblQuote(UON3) & "'"
        End If
    End If

    If bRPerfUON Then
        sConsulta2 = "SELECT P.UON1,P.UON2,P.UON3"
        sConsulta2 = sConsulta2 & " FROM " & sTablaPresAlias & " P WITH (NOLOCK)"
        sConsulta2 = sConsulta2 & " INNER JOIN PERF_UON PU WITH (NOLOCK) ON (PU.UON1=P.UON1 OR PU.UON1 IS NULL)"
        sConsulta2 = sConsulta2 & " AND (PU.UON2=P.UON2 OR PU.UON2 IS NULL) AND (PU.UON3=P.UON3 OR PU.UON3 IS NULL)"
        sConsulta2 = sConsulta2 & " AND PU.PERF=" & lIdPerfil & " "
    End If
    
    If bRUsuUon And bRPerfUON Then
        sConsulta = sConsulta1 & " UNION " & sConsulta2
    ElseIf bRUsuUon And Not bRPerfUON Then
        sConsulta = sConsulta1
    Else
        sConsulta = sConsulta2
    End If
    
    ConstruirCriterioUONUsuPerfPres = sConsulta
End Function

''' <summary>Construye el criterio en caso de restricción a la UON del usuario y/o UONs del perfil del usuario para presupuestos</summary>
''' <param name="bRUsuUon">Restricción a las UON del usuario</param>
''' <param name="bRPerfUON">Restricción a las UON del perfil del usuario</param>
''' <param name="lIdPerfil">Id perfil usuario</param>
''' <param name="UON1">UON1 si hay restricción a la UON del usuario</param>
''' <param name="UON2">UON2 si hay restricción a la UON del usuario</param>
''' <param name="UON3">UON3 si hay restricción a la UON del usuario</param>
''' <returns>Criterio</returns>
''' <remarks>Llamada desde: CInstancias.BuscarSolicituds</remarks>
''' <revision>LTG 24/01/2013</revision>

Public Function ConstruirCriterioUONUsuPerfCentros(ByVal bRUsuUon As Boolean, ByVal bRPerfUON As Boolean, ByVal lIdPerfil As Long, _
        ByVal bCoincidencia As Boolean, Optional ByVal UON1 As String, Optional ByVal UON2 As String, Optional ByVal UON3 As String, _
        Optional ByVal UON4 As String) As String
    Dim sConsulta As String
    Dim sConsulta1 As String
    Dim sConsulta2 As String
       
    If bRUsuUon Then
        sConsulta1 = "SELECT CSU.UON1,CSU.UON2,CSU.UON3,CSU.UON4,CSU.CENTRO_SM"
        sConsulta1 = sConsulta1 & " FROM CENTRO_SM_UON CSU WITH (NOLOCK)"
        sConsulta1 = sConsulta1 & " WHERE 1=1"
        
        If UON1 <> "" Then
            sConsulta1 = sConsulta1 & " AND CSU.UON1=@UON1"
            If bCoincidencia And UON2 = "" Then
                sConsulta1 = sConsulta1 & " AND CSU.UON2 IS NULL"
            End If
        End If
        If UON2 <> "" Then
            sConsulta1 = sConsulta1 & " AND CSU.UON2=@UON2"
            If bCoincidencia And UON3 = "" Then
                sConsulta1 = sConsulta1 & " AND CSU.UON3 IS NULL"
            End If
        End If
        If UON3 <> "" Then
            sConsulta1 = sConsulta1 & " AND CSU.UON3=@UON3"
            If bCoincidencia And UON4 = "" Then
                sConsulta1 = sConsulta1 & " AND CSU.UON4 IS NULL"
            End If
        End If
        If UON4 <> "" Then
            sConsulta1 = sConsulta1 & " AND CSU.UON4=@UON4"
        End If
    End If

    If bRPerfUON Then
        sConsulta2 = "SELECT CSU.UON1,CSU.UON2,CSU.UON3,CSU.UON4,CSU.CENTRO_SM"
        sConsulta2 = sConsulta2 & " FROM CENTRO_SM_UON CSU WITH (NOLOCK)"
        sConsulta2 = sConsulta2 & " INNER JOIN PERF_UON PU WITH (NOLOCK) ON (PU.UON1=CSU.UON1 OR PU.UON1 IS NULL)"
        sConsulta2 = sConsulta2 & " AND (PU.UON2=CSU.UON2 OR PU.UON2 IS NULL) AND (PU.UON3=CSU.UON3 OR PU.UON3 IS NULL)"
        sConsulta2 = sConsulta2 & " AND PU.PERF=" & lIdPerfil & " "
    End If
    
    If bRUsuUon And bRPerfUON Then
        sConsulta = sConsulta1 & " UNION " & sConsulta2
    ElseIf bRUsuUon And Not bRPerfUON Then
        sConsulta = sConsulta1
    Else
        sConsulta = sConsulta2
    End If
    
    ConstruirCriterioUONUsuPerfCentros = sConsulta
End Function


''' <summary>Construye el criterio en caso de restricción a la UON del usuario y/o UONs del perfil del usuario para presupuestos</summary>
''' <param name="sTablaPresAlias">Alias tabla presupuestos</param>
''' <param name="bRUsuUon">Restricción a las UON del usuario</param>
''' <param name="bRPerfUON">Restricción a las UON del perfil del usuario</param>
''' <param name="lIdPerfil">Id perfil usuario</param>
''' <param name="UON1">UON1 si hay restricción a la UON del usuario</param>
''' <param name="UON2">UON2 si hay restricción a la UON del usuario</param>
''' <param name="UON3">UON3 si hay restricción a la UON del usuario</param>
''' <returns>Criterio</returns>
''' <remarks>Llamada desde: CInstancias.BuscarSolicituds</remarks>
''' <revision>LTG 24/01/2013</revision>

Public Function ConstruirCriterioUONUsuPerfUON2(ByVal iNivel As Integer, ByVal bRUsuUon As Boolean, ByVal bRPerfUON As Boolean, _
        ByVal lIdPerfil As Long, Optional ByVal UON1 As String, Optional ByVal UON2 As String, Optional ByVal UON3 As String) As String
    Dim sConsulta As String
    Dim sConsulta1 As String
    Dim sConsulta2 As String
       
    If bRUsuUon Then
        sConsulta1 = "SELECT U.COD,U.DEN,U.ORGCOMPRAS,U.CENTROS,U.ALMACEN,U.BAJALOG,U.EMPRESA, "
        sConsulta1 = sConsulta1 & IIf(iNivel > 1, ",U.UON1", "") & IIf(iNivel = 3, ",U.UON2", "")
        sConsulta1 = sConsulta1 & " , ISNULL(U1.ACTIVA,1) ACTIVA "
        Select Case iNivel
            Case 1
                sConsulta1 = sConsulta1 & " FROM UON1 U WITH (NOLOCK)"
                sConsulta1 = sConsulta1 & " WHERE U.COD='" & DblQuote(UON1) & "'"
            Case 2
                sConsulta1 = sConsulta1 & " FROM UON2 U WITH (NOLOCK)"
                sConsulta1 = sConsulta1 & " WHERE U.UON1='" & DblQuote(UON1) & " AND U.COD='" & DblQuote(UON2) & "'"
            Case 3
                sConsulta1 = sConsulta1 & " FROM UON3 U WITH (NOLOCK)"
                sConsulta1 = sConsulta1 & " WHERE U.UON1='" & DblQuote(UON1) & " AND U.UON2='" & DblQuote(UON2) & "'" & " AND U.COD='" & DblQuote(UON3) & "'"
        End Select
    End If

    If bRPerfUON Then

        Select Case iNivel
            Case 1
                sConsulta2 = "SELECT U.COD,U.DEN,U.ORGCOMPRAS,U.CENTROS,U.ALMACEN,U.BAJALOG,U.EMPRESA, 1 ACTIVA"
                sConsulta2 = sConsulta2 & " FROM  UON1 U WITH (NOLOCK) "
                sConsulta2 = sConsulta2 & " INNER JOIN PERF_UON PU WITH (NOLOCK) ON PU.UON1=U.COD AND ISNULL(PU.UON2,'')='' AND PU.PERF=" & lIdPerfil & " "
                sConsulta2 = sConsulta2 & " AND PU.PERF=" & lIdPerfil & " "
                sConsulta2 = sConsulta2 & " UNION"
                sConsulta2 = sConsulta2 & " SELECT U.COD,U.DEN,U.ORGCOMPRAS,U.CENTROS,U.ALMACEN,U.BAJALOG,U.EMPRESA, 0 ACTIVA"
                sConsulta2 = sConsulta2 & " FROM  UON1 U WITH (NOLOCK) "
                sConsulta2 = sConsulta2 & " INNER JOIN PERF_UON PU WITH (NOLOCK) ON PU.UON1=U.COD AND ISNULL(PU.UON2,'')<>'' "
                sConsulta2 = sConsulta2 & " AND PU.PERF=" & lIdPerfil & " "

            Case 2
                'Tienen que salir las UON que no tengan la UON2 a NULL o en su defecto las que no tengan la UON1 a NULL
                sConsulta2 = " SELECT U.COD,U.DEN,U.ORGCOMPRAS,U.CENTROS,U.ALMACEN,U.BAJALOG,U.EMPRESA, 1 ACTIVA"
                sConsulta2 = sConsulta2 & " From UON2 U WITH(NOLOCK)"
                sConsulta2 = sConsulta2 & " INNER JOIN PERF_UON PU WITH(NOLOCK) ON U.UON1=PU.UON1 AND UON2.COD=PU.UON2 AND ISNULL(PU.UON3,'')='' "
                sConsulta2 = sConsulta2 & " AND PU.PERF=" & lIdPerfil & " "
                sConsulta2 = sConsulta2 & " UNION"
                sConsulta2 = sConsulta2 & " SELECT U.COD,U.DEN,U.ORGCOMPRAS,U.CENTROS,U.ALMACEN,U.BAJALOG,U.EMPRESA, 1 ACTIVA"
                sConsulta2 = sConsulta2 & " From UON2 WITH(NOLOCK)"
                sConsulta2 = sConsulta2 & " INNER JOIN PERF_UON PU WITH(NOLOCK) ON U.UON1=PU.UON1 AND ISNULL(PU.UON2,'')=''  "
                sConsulta2 = sConsulta2 & " AND PU.PERF=" & lIdPerfil & " "
                sConsulta2 = sConsulta2 & " UNION "
                sConsulta2 = sConsulta2 & " SELECT U.COD,U.DEN,U.ORGCOMPRAS,U.CENTROS,U.ALMACEN,U.BAJALOG,U.EMPRESA, 0 ACTIVA"
                sConsulta2 = sConsulta2 & " From UON2 U WITH(NOLOCK)"
                sConsulta2 = sConsulta2 & " INNER JOIN PERF_UON PU WITH(NOLOCK) ON U.UON1=PU.UON1 AND U.COD=PU.UON2 AND ISNULL(PU.UON3,'')<>'' "
                sConsulta2 = sConsulta2 & " AND PU.PERF=" & lIdPerfil & " "
                
            Case 3
                'Tienen que salir las UON que no tengan la UON3 a NULL o en su defecto las que no tengan la UON2 a NULL
                'o en su defecto las que no tengan la UON1 a NULL
                sConsulta2 = " SELECT U.COD,U.DEN,U.ORGCOMPRAS,U.CENTROS,U.ALMACEN,U.BAJALOG,U.EMPRESA, 1 ACTIVA "
                sConsulta2 = sConsulta2 & " FROM  UON3 U WITH (NOLOCK) "
                sConsulta2 = sConsulta2 & " INNER JOIN PERF_UON PU WITH (NOLOCK) ON PU.UON3=UON3.COD AND PU.UON2=UON3.UON2 AND PU.UON1=UON3.UON1"
                sConsulta2 = sConsulta2 & " AND PU.PERF=" & lIdPerfil & " "
                sConsulta2 = sConsulta2 & " UNION "
                sConsulta2 = sConsulta2 & " SELECT U.COD,U.DEN,U.ORGCOMPRAS,U.CENTROS,U.ALMACEN,U.BAJALOG,U.EMPRESA, 0 ACTIVA"
                sConsulta2 = sConsulta2 & " FROM  UON3 U WITH (NOLOCK) "
                sConsulta2 = sConsulta2 & " INNER JOIN PERF_UON PU ON U.UON1=PU.UON1 AND U.UON2=PU.UON2 AND  ISNULL(PU.UON3,'')=''"
                sConsulta2 = sConsulta2 & " AND PU.PERF=" & lIdPerfil & " "
                sConsulta2 = sConsulta2 & " Union"
                sConsulta2 = sConsulta2 & " SELECT U.COD,U.DEN,U.ORGCOMPRAS,U.CENTROS,U.ALMACEN,U.BAJALOG,U.EMPRESA, 0 ACTIVA "
                sConsulta2 = sConsulta2 & " FROM  UON3 U WITH (NOLOCK) "
                sConsulta2 = sConsulta2 & " INNER JOIN PERF_UON PU ON U.UON1=PU.UON1 AND ISNULL(PU.UON2,'')='' AND ISNULL(PU.UON3,'')=''"
                sConsulta2 = sConsulta2 & " AND PU.PERF=" & lIdPerfil & " "
            End Select
        
        
    End If
    
    If bRUsuUon And bRPerfUON Then
        sConsulta = sConsulta1 & " UNION " & sConsulta2
    ElseIf bRUsuUon And Not bRPerfUON Then
        sConsulta = sConsulta1
    Else
        sConsulta = sConsulta2
    End If
    
    ConstruirCriterioUONUsuPerfUON2 = sConsulta
End Function

''' <summary>Construye el criterio en caso de restricción a la UON del usuario y/o UONs del perfil del usuario para presupuestos</summary>
''' <param name="sTablaPresAlias">Alias tabla presupuestos</param>
''' <param name="bRUsuUon">Restricción a las UON del usuario</param>
''' <param name="bRPerfUON">Restricción a las UON del perfil del usuario</param>
''' <param name="lIdPerfil">Id perfil usuario</param>
''' <param name="UON1">UON1 si hay restricción a la UON del usuario</param>
''' <param name="UON2">UON2 si hay restricción a la UON del usuario</param>
''' <param name="UON3">UON3 si hay restricción a la UON del usuario</param>
''' <returns>Criterio</returns>
''' <remarks>Llamada desde: CInstancias.BuscarSolicituds</remarks>
''' <revision>LTG 24/01/2013</revision>

Public Function ConstruirCriterioUONUsuPerfUON(ByVal iNivel As Integer, ByVal bRUsuUon As Boolean, ByVal bRPerfUON As Boolean, _
        ByVal lIdPerfil As Long, Optional ByVal UON1 As String, Optional ByVal UON2 As String, Optional ByVal UON3 As String, Optional ByVal bProceDistrItem As Boolean = False) As String
    Dim sConsulta As String
    Dim sConsulta1 As String
    Dim sConsulta2 As String
    
    If bRUsuUon Then
        Dim iNivelUsu As Integer
        iNivelUsu = 3
        If UON3 = "" Then
            iNivelUsu = 2
            If UON2 = "" Then
                iNivelUsu = 1
                If UON1 = "" Then
                    'nivel 0
                    iNivelUsu = 0
                End If
            End If
        End If
        
        Select Case iNivel
            Case 1
                sConsulta1 = "SELECT U.COD,U.DEN,U.ORGCOMPRAS,U.CENTROS,U.ALMACEN,U.BAJALOG,U.EMPRESA, " & CInt(iNivel >= iNivelUsu) & " ACTIVA," & StrToSQLNULL(UON2) & " UON2," & StrToSQLNULL(UON3) & " UON3"
                sConsulta1 = sConsulta1 & " FROM UON1 U WITH (NOLOCK)"
                sConsulta1 = sConsulta1 & " WHERE U.COD='" & UON1 & "'"
            Case 2
                sConsulta1 = "SELECT U.UON1,U.COD,U.DEN,U.ORGCOMPRAS,U.CENTROS,U.ALMACEN,U.BAJALOG,U.EMPRESA, " & CInt(iNivel >= iNivelUsu) & " ACTIVA," & StrToSQLNULL(UON3) & " UON3"
                sConsulta1 = sConsulta1 & " FROM UON2 U WITH (NOLOCK)"
                If bProceDistrItem Then
                    sConsulta1 = sConsulta1 & " WHERE U.UON1 ='" & UON1 & "'"
                    If Not UON2 = "" Then sConsulta1 = sConsulta1 & " AND U.COD='" & UON2 & "'"
                Else
                    sConsulta1 = sConsulta1 & " WHERE U.UON1 ='" & UON1 & "' AND U.COD='" & UON2 & "'"
                End If
            Case 3
                sConsulta1 = "SELECT U.UON1,U.UON2,U.COD,U.DEN,U.ORGCOMPRAS,U.CENTROS,U.ALMACEN,U.BAJALOG,U.EMPRESA, " & CInt(iNivel >= iNivelUsu) & " ACTIVA "
                sConsulta1 = sConsulta1 & " FROM UON3 U WITH (NOLOCK)"
                If bProceDistrItem Then
                    sConsulta1 = sConsulta1 & " WHERE U.UON1 ='" & UON1 & "'"
                    If Not UON2 = "" Then sConsulta1 = sConsulta1 & " AND U.UON2='" & UON2 & "'"
                    If Not UON3 = "" Then sConsulta1 = sConsulta1 & " AND U.COD='" & UON3 & "'"
                Else
                    sConsulta1 = sConsulta1 & " WHERE U.UON1 ='" & UON1 & "' AND U.UON2='" & UON2 & "' AND U.COD='" & UON3 & "'"
                End If
        End Select
    End If

    If bRPerfUON Then
        
        Select Case iNivel
            Case 1
                sConsulta2 = "SELECT U.COD,U.DEN,U.ORGCOMPRAS,U.CENTROS,U.ALMACEN,U.BAJALOG,U.EMPRESA,1 ACTIVA,PU.UON2,PU.UON3"
                sConsulta2 = sConsulta2 & " FROM  UON1 U WITH (NOLOCK) "
                sConsulta2 = sConsulta2 & " INNER JOIN PERF_UON PU WITH (NOLOCK) ON PU.UON1=U.COD AND PU.UON2 IS NULL AND PU.PERF=" & lIdPerfil & " "
                sConsulta2 = sConsulta2 & " UNION"
                sConsulta2 = sConsulta2 & " SELECT U.COD,U.DEN,U.ORGCOMPRAS,U.CENTROS,U.ALMACEN,U.BAJALOG,U.EMPRESA, 0 ACTIVA,PU.UON2,PU.UON3"
                sConsulta2 = sConsulta2 & " FROM  UON1 U WITH (NOLOCK) "
                sConsulta2 = sConsulta2 & " INNER JOIN PERF_UON PU WITH (NOLOCK) ON PU.UON1=U.COD AND PU.UON2 IS NOT NULL AND PU.PERF=" & lIdPerfil & " "
            Case 2
                'Tienen que salir las UON que no tengan la UON2 a NULL o en su defecto las que no tengan la UON1 a NULL
                sConsulta2 = " SELECT U.UON1,U.COD,U.DEN,U.ORGCOMPRAS,U.CENTROS,U.ALMACEN,U.BAJALOG,U.EMPRESA, 1 ACTIVA,PU.UON3"
                sConsulta2 = sConsulta2 & " From UON2 U WITH(NOLOCK)"
                sConsulta2 = sConsulta2 & " INNER JOIN PERF_UON PU WITH(NOLOCK) ON U.UON1=PU.UON1 AND U.COD=PU.UON2 AND PU.UON3 IS NULL "
                sConsulta2 = sConsulta2 & " AND PU.PERF=" & lIdPerfil & " "
                sConsulta2 = sConsulta2 & " UNION"
                sConsulta2 = sConsulta2 & " SELECT U.UON1,U.COD,U.DEN,U.ORGCOMPRAS,U.CENTROS,U.ALMACEN,U.BAJALOG,U.EMPRESA, 1 ACTIVA,PU.UON3"
                sConsulta2 = sConsulta2 & " From UON2 U WITH(NOLOCK)"
                sConsulta2 = sConsulta2 & " INNER JOIN PERF_UON PU WITH(NOLOCK) ON U.UON1=PU.UON1  AND PU.UON2 IS NULL  AND PU.PERF=" & lIdPerfil
                sConsulta2 = sConsulta2 & " UNION "
                sConsulta2 = sConsulta2 & " SELECT U.UON1,U.COD,U.DEN,U.ORGCOMPRAS,U.CENTROS,U.ALMACEN,U.BAJALOG,U.EMPRESA, 0 ACTIVA,PU.UON3"
                sConsulta2 = sConsulta2 & " From UON2 U WITH(NOLOCK)"
                sConsulta2 = sConsulta2 & " INNER JOIN PERF_UON PU WITH(NOLOCK) ON U.UON1=PU.UON1 AND U.COD=PU.UON2 AND PU.UON3 IS NOT NULL "
                sConsulta2 = sConsulta2 & " AND PU.PERF=" & lIdPerfil & " "
            Case 3
                'Tienen que salir las UON que no tengan la UON3 a NULL o en su defecto las que no tengan la UON2 a NULL
                'o en su defecto las que no tengan la UON1 a NULL
                sConsulta2 = " SELECT U.UON1,U.UON2,U.COD,U.DEN,U.ORGCOMPRAS,U.CENTROS,U.ALMACEN,U.BAJALOG,U.EMPRESA, 1 ACTIVA "
                sConsulta2 = sConsulta2 & " FROM  UON3 U WITH (NOLOCK) "
                sConsulta2 = sConsulta2 & " INNER JOIN PERF_UON PU WITH (NOLOCK) ON PU.UON3=U.COD AND PU.UON2=U.UON2 AND PU.UON1=U.UON1"
                sConsulta2 = sConsulta2 & " AND PU.PERF=" & lIdPerfil & " "
                sConsulta2 = sConsulta2 & " UNION "
                sConsulta2 = sConsulta2 & " SELECT U.UON1,U.UON2,U.COD,U.DEN,U.ORGCOMPRAS,U.CENTROS,U.ALMACEN,U.BAJALOG,U.EMPRESA, 1 ACTIVA "
                sConsulta2 = sConsulta2 & " FROM  UON3 U WITH (NOLOCK) "
                sConsulta2 = sConsulta2 & " INNER JOIN PERF_UON PU ON U.UON1=PU.UON1 AND U.UON2=PU.UON2 AND  PU.UON3 IS NULL"
                sConsulta2 = sConsulta2 & " AND PU.PERF=" & lIdPerfil & " "
                sConsulta2 = sConsulta2 & " Union"
                sConsulta2 = sConsulta2 & " SELECT U.UON1,U.UON2,U.COD,U.DEN,U.ORGCOMPRAS,U.CENTROS,U.ALMACEN,U.BAJALOG,U.EMPRESA, 1 ACTIVA "
                sConsulta2 = sConsulta2 & " FROM  UON3 U WITH (NOLOCK) "
                sConsulta2 = sConsulta2 & " INNER JOIN PERF_UON PU ON U.UON1=PU.UON1 AND PU.UON2 IS NULL AND PU.UON3 IS NULL"
                sConsulta2 = sConsulta2 & " AND PU.PERF=" & lIdPerfil & " "

        End Select
    End If
    
    If bRUsuUon And bRPerfUON Then
        sConsulta = sConsulta1 & " UNION " & sConsulta2
    ElseIf bRUsuUon And Not bRPerfUON Then
        sConsulta = sConsulta1
    Else
        sConsulta = sConsulta2
    End If
    
    ConstruirCriterioUONUsuPerfUON = sConsulta
End Function


''' <summary>Construye el criterio en caso de restricción a la UON del usuario y/o UONs del perfil del usuario para presupuestos</summary>
''' <param name="sUONDepAlias">Alias tabla UON_DEP</param>
''' <param name="iNivel">Nivel</param>
''' <param name="bRUsuUon">Restricción a las UON del usuario</param>
''' <param name="bRPerfUON">Restricción a las UON del perfil del usuario</param>
''' <param name="lIdPerfil">Id perfil usuario</param>
''' <param name="UON1">UON1 si hay restricción a la UON del usuario</param>
''' <param name="UON2">UON2 si hay restricción a la UON del usuario</param>
''' <param name="UON3">UON3 si hay restricción a la UON del usuario</param>
''' <returns>Criterio</returns>
''' <remarks>Llamada desde: CInstancias.BuscarSolicituds</remarks>
''' <revision>LTG 24/01/2013</revision>

Public Function ConstruirCriterioUONUsuPerfUONDep(ByVal sUONDepAlias As String, ByVal iNivel As Integer, ByVal bRUsuUon As Boolean, ByVal bRPerfUON As Boolean, _
        ByVal lIdPerfil As Long, Optional ByVal UON1 As String, Optional ByVal UON2 As String, Optional ByVal UON3 As String, Optional ByVal bProceDistrItem As Boolean = False) As String
    Dim sConsulta As String
    Dim sConsulta1 As String
    Dim sConsulta2 As String
    Dim sConsultaActiva As String
    Dim sConsultaSelect As String
       
    sConsultaActiva = ", 1 ACTIVA"
    If bProceDistrItem Then 'Copiado de ConstruirCriterioUONUsuPerfUON
        Dim iNivelUsu As Integer
        iNivelUsu = 3
        If UON3 = "" Then
            iNivelUsu = 2
            If UON2 = "" Then
                iNivelUsu = 1
                If UON1 = "" Then
                    'nivel 0
                    iNivelUsu = 0
                End If
            End If
        End If
        
        sConsultaActiva = ", " & Abs(CInt(iNivel >= iNivelUsu)) & " ACTIVA"
    End If
       
    If bRUsuUon Then
        sConsulta1 = "SELECT UD.UON1,UD.DEP,UD.BAJALOG" & IIf(iNivel > 1, ",UD.UON2", "") & IIf(iNivel = 3, ",UD.UON3", "") & sConsultaActiva
        sConsulta1 = sConsulta1 & " FROM " & sUONDepAlias & " UD WITH (NOLOCK)"
        sConsulta1 = sConsulta1 & " WHERE ISNULL(UD.UON1,'')=ISNULL(" & IIf(UON1 = "", "NULL", "'" & DblQuote(UON1) & "'") & ",'')"
        If iNivel > 1 Then
            sConsulta1 = sConsulta1 & " AND ISNULL(UD.UON2,'')=ISNULL(" & IIf(UON2 = "", "NULL", "'" & DblQuote(UON2) & "'") & ",ISNULL(UD.UON2,''))"
            If iNivel = 3 Then
                sConsulta1 = sConsulta1 & " AND ISNULL(UD.UON3,'')=ISNULL(" & IIf(UON3 = "", "NULL", "'" & DblQuote(UON3) & "'") & ",ISNULL(UD.UON3,''))"
            End If
        End If
    End If

    If bRPerfUON Then
    
    sConsultaSelect = "SELECT UD.UON1,UD.DEP,UD.BAJALOG" & IIf(iNivel > 1, ",UD.UON2", "") & IIf(iNivel = 3, ",UD.UON3", "")
    
    sConsultaActiva = ", 1 ACTIVA"
    If bProceDistrItem Then
        Select Case iNivel
        Case 1
            sConsultaActiva = ", CASE WHEN PU.UON2 IS NOT NULL THEN 0 ELSE 1 END ACTIVA "
        Case 2
            sConsultaActiva = ", CASE WHEN PU.UON3 IS NOT NULL THEN 0 ELSE 1 END ACTIVA "
        Case 3
            sConsultaActiva = ", 1 ACTIVA "
        End Select
    End If
    
    sConsultaSelect = sConsultaSelect & sConsultaActiva
    
    
    If iNivel = 1 Then
        sConsulta2 = sConsultaSelect & " FROM " & sUONDepAlias & " UD WITH (NOLOCK)"
        sConsulta2 = sConsulta2 & " INNER JOIN PERF_UON PU WITH (NOLOCK) ON ISNULL(PU.UON1,'')=ISNULL(UD.UON1,'') "
        sConsulta2 = sConsulta2 & " AND PU.PERF=" & lIdPerfil & " "
    End If
    If iNivel = 2 Then
        'Seleccionada a Nivel 1. Sólo rellena UON1. UON2 son nulos.
        sConsulta2 = sConsultaSelect & " FROM " & sUONDepAlias & "  UD WITH (NOLOCK) "
        sConsulta2 = sConsulta2 & " INNER JOIN PERF_UON PU WITH (NOLOCK) ON ISNULL(PU.UON1,'')=ISNULL(UD.UON1,'') AND PU.UON2 IS NULL "
        sConsulta2 = sConsulta2 & " AND PU.PERF=" & lIdPerfil & " "
        sConsulta2 = sConsulta2 & " UNION "
        'Seleccionada a Nivel 2. Rellena UON1, UON2
        sConsulta2 = sConsulta2 & sConsultaSelect & " FROM " & sUONDepAlias & "  UD WITH (NOLOCK) "
        sConsulta2 = sConsulta2 & " INNER JOIN PERF_UON PU WITH (NOLOCK) ON ISNULL(PU.UON1,'')=ISNULL(UD.UON1,'') AND ISNULL(PU.UON2,'')=ISNULL(UD.UON2,'') "
        sConsulta2 = sConsulta2 & " AND PU.PERF=" & lIdPerfil & " "
    End If
    If iNivel = 3 Then
        'Seleccionada a Nivel 1. Sólo rellena UON1. UON2 y UON3 son nulos.
        sConsulta2 = sConsultaSelect & " FROM  " & sUONDepAlias & " UD WITH (NOLOCK) "
        sConsulta2 = sConsulta2 & " INNER JOIN PERF_UON PU WITH (NOLOCK) ON ISNULL(PU.UON1,'')=ISNULL(UD.UON1,'') AND PU.UON2 IS NULL "
        sConsulta2 = sConsulta2 & " AND PU.PERF=" & lIdPerfil & " "
        sConsulta2 = sConsulta2 & " UNION "
        'Seleccionada a Nivel 2. Rellena UON1, UON2 y deja vacío UON3.
        sConsulta2 = sConsulta2 & sConsultaSelect & " FROM  " & sUONDepAlias & " UD WITH (NOLOCK) "
        sConsulta2 = sConsulta2 & " INNER JOIN PERF_UON PU WITH (NOLOCK) ON ISNULL(PU.UON1,'')=ISNULL(UD.UON1,'') AND ISNULL(PU.UON2,'')=ISNULL(UD.UON2,'') AND PU.UON3 IS NULL "
        sConsulta2 = sConsulta2 & " AND PU.PERF=" & lIdPerfil & " "
        sConsulta2 = sConsulta2 & " UNION "
        'Seleccionada a Nivel 3. Rellena UON1, UON2 y UON3
        sConsulta2 = sConsulta2 & sConsultaSelect & " FROM  " & sUONDepAlias & "  UD WITH (NOLOCK) "
        sConsulta2 = sConsulta2 & " INNER JOIN PERF_UON PU WITH (NOLOCK) ON ISNULL(PU.UON1,'')=ISNULL(UD.UON1,'') AND ISNULL(PU.UON2,'')=ISNULL(UD.UON2,'') AND ISNULL(PU.UON3,'')=ISNULL(UD.UON3,'') "
        sConsulta2 = sConsulta2 & " AND PU.PERF=" & lIdPerfil & " "
    End If

End If
    
    If bRUsuUon And bRPerfUON Then
        sConsulta = sConsulta1 & " UNION " & sConsulta2
    ElseIf bRUsuUon And Not bRPerfUON Then
        sConsulta = sConsulta1
    Else
        sConsulta = sConsulta2
    End If
    
    ConstruirCriterioUONUsuPerfUONDep = sConsulta
End Function

''' <summary>Construye el criterio en caso de restricción a la UON del usuario y/o UONs del perfil del usuario para presupuestos</summary>
''' <param name="sUONDepAlias">Alias tabla UON_DEP</param>
''' <param name="iNivel">Nivel</param>
''' <param name="bRUsuUon">Restricción a las UON del usuario</param>
''' <param name="bRPerfUON">Restricción a las UON del perfil del usuario</param>
''' <param name="lIdPerfil">Id perfil usuario</param>
''' <param name="UON1">UON1 si hay restricción a la UON del usuario</param>
''' <param name="UON2">UON2 si hay restricción a la UON del usuario</param>
''' <param name="UON3">UON3 si hay restricción a la UON del usuario</param>
''' <returns>Criterio</returns>
''' <remarks>Llamada desde: CInstancias.BuscarSolicituds</remarks>
''' <revision>LTG 24/01/2013</revision>

Public Function ConstruirCriterioUONUsuPerfUONDest(ByVal sUONDepAlias As String, ByVal iNivel As Integer, ByVal bRUsuUon As Boolean, ByVal bRPerfUON As Boolean, _
        ByVal lIdPerfil As Long, Optional ByVal UON1 As String, Optional ByVal UON2 As String, Optional ByVal UON3 As String) As String
    Dim sConsulta As String
    Dim sConsulta1 As String
    Dim sConsulta2 As String
       
    If bRUsuUon Then
        sConsulta1 = "SELECT UD.DEST"
        sConsulta1 = sConsulta1 & " FROM " & sUONDepAlias & " UD WITH (NOLOCK)"
        If UON1 <> "" And UON2 <> "" And UON3 <> "" Then
            sConsulta1 = sConsulta1 & " WHERE UD.UON1='" & DblQuote(UON1) & "' AND UD.UON2='" & DblQuote(UON2) & "' AND UD.UON3='" & DblQuote(UON3) & "'"
        ElseIf UON1 <> "" And UON2 <> "" Then
            sConsulta1 = sConsulta1 & " WHERE UD.UON1='" & DblQuote(UON1) & "' AND UD.UON2='" & DblQuote(UON2) & "'"
        Else
            sConsulta1 = sConsulta1 & " WHERE UD.UON1='" & DblQuote(UON1) & "'"
        End If
    End If

    If bRPerfUON Then
        sConsulta2 = "SELECT UD.DEST"
        sConsulta2 = sConsulta2 & " FROM " & sUONDepAlias & " UD WITH (NOLOCK)"
        sConsulta2 = sConsulta2 & " INNER JOIN PERF_UON PU WITH (NOLOCK) ON PU.UON1=UD.UON1"
        If iNivel > 1 Then
            sConsulta2 = sConsulta2 & " AND PU.UON2=UD.UON2"
            If iNivel = 3 Then
                sConsulta2 = sConsulta2 & " AND PU.UON3=UD.UON3"
            End If
        End If
        sConsulta2 = sConsulta2 & " AND PU.PERF=" & lIdPerfil & " "
    End If
    
    If bRUsuUon And bRPerfUON Then
        sConsulta = sConsulta1 & " UNION " & sConsulta2
    ElseIf bRUsuUon And Not bRPerfUON Then
        sConsulta = sConsulta1
    Else
        sConsulta = sConsulta2
    End If
    
    ConstruirCriterioUONUsuPerfUONDest = sConsulta
End Function

''' <summary>Construye el criterio en caso de restricción a la UON del usuario y/o UONs del perfil del usuario para presupuestos</summary>
''' <param name="sUONDepAlias">Alias tabla UON_DEP</param>
''' <param name="bRUsuUon">Restricción a las UON del usuario</param>
''' <param name="bRPerfUON">Restricción a las UON del perfil del usuario</param>
''' <param name="lIdPerfil">Id perfil usuario</param>
''' <param name="UON1">UON1 si hay restricción a la UON del usuario</param>
''' <param name="UON2">UON2 si hay restricción a la UON del usuario</param>
''' <param name="UON3">UON3 si hay restricción a la UON del usuario</param>
''' <returns>Criterio</returns>
''' <remarks>Llamada desde: CInstancias.BuscarSolicituds</remarks>
''' <revision>LTG 24/01/2013</revision>

Public Function ConstruirCriterioUONUsuPerfUONDepPorUON(ByVal sUONDepAlias As String, ByVal bRUsuUon As Boolean, ByVal bRPerfUON As Boolean, _
        ByVal lIdPerfil As Long, Optional ByVal UON1 As String, Optional ByVal UON2 As String, Optional ByVal UON3 As String) As String
    Dim sConsulta As String
    Dim sConsulta1 As String
    Dim sConsulta2 As String
       
    If bRUsuUon Then
        sConsulta1 = "SELECT UD.DEP,UD.BAJALOG"
        sConsulta1 = sConsulta1 & " FROM " & sUONDepAlias & " UD WITH (NOLOCK)"
        sConsulta1 = sConsulta1 & " WHERE "
                
        If UON1 <> "" And UON2 <> "" And UON3 <> "" Then
            sConsulta1 = sConsulta1 & " UD.UON1=@UON1 AND UD.UON2=@UON2 AND UD.UON3=@UON3"
        ElseIf UON1 <> "" And UON2 <> "" And UON3 = "" Then
            sConsulta1 = sConsulta1 & " UD.UON1=@UON1 AND UD.UON2=@UON2"
        Else
            sConsulta1 = sConsulta1 & " UD.UON1=@UON1"
        End If
    End If

    If bRPerfUON Then
        sConsulta2 = "SELECT ISNULL(UD1.DEP,ISNULL(UD2.DEP,UD3.DEP)) DEP,ISNULL(UD1.BAJALOG,ISNULL(UD2.BAJALOG,UD3.BAJALOG)) BAJALOG"
        sConsulta2 = sConsulta2 & " FROM PERF_UON PU WITH (NOLOCK)"
        sConsulta2 = sConsulta2 & " LEFT JOIN UON1_DEP UD1 WITH (NOLOCK) ON UD1.UON1=PU.UON1 AND PU.UON2 IS NULL AND PU.UON3 IS NULL"
        sConsulta2 = sConsulta2 & " LEFT JOIN UON2_DEP UD2 WITH (NOLOCK) ON UD2.UON1=PU.UON1 AND UD2.UON2=PU.UON2 AND PU.UON3 IS NULL"
        sConsulta2 = sConsulta2 & " LEFT JOIN UON3_DEP UD3 WITH (NOLOCK) ON UD3.UON1=PU.UON1 AND UD3.UON2=PU.UON2 AND UD3.UON3=PU.UON3"
        sConsulta2 = sConsulta2 & " WHERE PU.PERF=" & lIdPerfil & " "
    End If
    
    If bRUsuUon And bRPerfUON Then
        sConsulta = sConsulta1 & " UNION " & sConsulta2
    ElseIf bRUsuUon And Not bRPerfUON Then
        sConsulta = sConsulta1
    Else
        sConsulta = sConsulta2
    End If
    
    ConstruirCriterioUONUsuPerfUONDepPorUON = sConsulta
End Function

''' <summary>Construye el criterio en caso de restricción a la rama de la UON del usuario y/o UONs del perfil del usuario para presupuestos</summary>
''' <param name="oConexion">Objeto con la conexión a la BD</param>
''' <param name="bRPerfUON">Restricción a las UON del perfil del usuario</param>
''' <param name="lIdPerf">Id perfil usuario</param>
''' <param name="UON1">UON1 si hay restricción a la UON del usuario</param>
''' <param name="UON2">UON2 si hay restricción a la UON del usuario</param>
''' <param name="UON3">UON3 si hay restricción a la UON del usuario</param>
''' <returns>Criterio</returns>
''' <remarks>Llamada desde: CInstancias.BuscarSolicituds</remarks>
''' <revision>LTG 24/01/2013</revision>

Public Function ConstruirCriterioRamaUONUsuPerfPres(ByVal oConexion As CConexion, ByVal sTablaPresAlias As String, ByVal bRPerfUON As Boolean, _
        ByVal lIdPerf As Long, Optional ByVal UON1 As String, Optional ByVal UON2 As String, Optional ByVal UON3 As String) As String
    Dim sUON As String
    Dim oPerfil As CPerfil
    Dim rstUONsPerf As adodb.Recordset
    Dim sRama1 As String
    Dim sRama2 As String
                  
    If Not IsMissing(UON1) Or Not IsMissing(UON2) Or Not IsMissing(UON3) Then
        If NullToStr(UON3) <> "" Then
            sRama1 = "'" & DblQuote(UON1) & "'"
            sRama2 = "(" & sTablaPresAlias & ".UON1 ='" & DblQuote(UON1) & "' AND " & sTablaPresAlias & ".UON2='" & DblQuote(UON2) & "' AND " & sTablaPresAlias & ".UON3 IS NULL) OR "
        ElseIf NullToStr(UON2) <> "" Then
            sRama1 = "'" & DblQuote(UON1) & "'"
        End If
    End If

    If bRPerfUON Then
        If lIdPerf > 0 Then
            Set oPerfil = New CPerfil
            Set oPerfil.Conexion = oConexion
            oPerfil.Id = lIdPerf
            Set rstUONsPerf = oPerfil.DevolverUONsPerfil
            Set oPerfil = Nothing

            If Not rstUONsPerf Is Nothing Then
                If rstUONsPerf.RecordCount > 0 Then
                    rstUONsPerf.MoveFirst
                    While Not rstUONsPerf.eof
                        If NullToStr(rstUONsPerf("UON3")) <> "" Then
                            sRama1 = sRama1 & "'" & DblQuote(rstUONsPerf("UON1")) & "'"
                            sRama2 = sRama2 & "(" & sTablaPresAlias & ".UON1 ='" & DblQuote(rstUONsPerf("UON1")) & "' AND " & sTablaPresAlias & ".UON2='" & DblQuote(rstUONsPerf("UON2")) & "' AND " & sTablaPresAlias & ".UON3 IS NULL) OR "
                        ElseIf NullToStr(rstUONsPerf("UON2")) <> "" Then
                            sRama1 = sRama1 & "'" & DblQuote(rstUONsPerf("UON1")) & "'"
                        End If

                        rstUONsPerf.MoveNext
                    Wend
                End If
            End If
        End If
    End If
    
    sUON = "((" & sTablaPresAlias & ".UON1 IS NULL AND " & sTablaPresAlias & ".UON2 IS NULL AND " & sTablaPresAlias & ".UON3 IS NULL)"
    If sRama1 <> "" Then
        sUON = sUON & " OR (" & sTablaPresAlias & ".UON1 IN (" & sRama1 & ") AND " & sTablaPresAlias & ".UON2 IS NULL AND " & sTablaPresAlias & ".UON3 IS NULL)"
    End If
    If sRama2 <> "" Then
        sRama2 = Left(sRama2, Len(sRama2) - 4)
        sUON = sUON & " OR (" & sRama2 & ")"
    End If
    sUON = sUON & ")"
        
    ConstruirCriterioRamaUONUsuPerfPres = sUON
End Function

''' <summary>Construye el criterio en caso de restricción a la UON del usuario y/o UONs del perfil del usuario para mantenimiento de materiales</summary>
''' <param name="sUONDepAlias">Alias tabla UON_DEP</param>
''' <param name="iNivel">Nivel</param>
''' <param name="bRUsuUon">Restricción a las UON del usuario</param>
''' <param name="bRPerfUON">Restricción a las UON del perfil del usuario</param>
''' <param name="lIdPerfil">Id perfil usuario</param>
''' <param name="UON1">UON1 si hay restricción a la UON del usuario</param>
''' <param name="UON2">UON2 si hay restricción a la UON del usuario</param>
''' <param name="UON3">UON3 si hay restricción a la UON del usuario</param>
''' <returns>Criterio</returns>
''' <remarks>Llamada desde: CInstancias.BuscarSolicituds</remarks>
''' <revision>LTG 24/01/2013</revision>

Public Function ConstruirCriterioUONUsuPerfUONArt(ByVal bRUsuUon As Boolean, ByVal bRPerfUON As Boolean, _
        ByVal lIdPerfil As Long, Optional ByVal UON1 As String, Optional ByVal UON2 As String, Optional ByVal UON3 As String) As String
    Dim sConsulta As String
    Dim sConsulta1 As String
    Dim sConsulta2 As String
    
    Dim iNivel As Integer
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    iNivel = 3
    
    If UON3 = "" Then
        iNivel = 2
        If UON2 = "" Then
            iNivel = 1
            If UON1 = "" Then
                'nivel 0
                iNivel = 0
            End If
        End If
    End If
       
    If iNivel > 0 Then
        If bRUsuUon Then
            sConsulta1 = "SELECT AU.ART4 "
            sConsulta1 = sConsulta1 & " FROM ART4_UON AU WITH (NOLOCK) "
            sConsulta1 = sConsulta1 & " WHERE AU.UON1='" & DblQuote(UON1) & "'"
            If iNivel > 1 Then
                sConsulta1 = sConsulta1 & " AND AU.UON2='" & DblQuote(UON2) & "'"
                If iNivel = 3 Then
                    sConsulta1 = sConsulta1 & " AND AU.UON3='" & DblQuote(UON3) & "'"
                End If
            End If
        End If
    End If
    
    If bRPerfUON Then
        sConsulta2 = " SELECT A.ART4 "
        sConsulta2 = sConsulta2 & " FROM ART4_UON A WITH (NOLOCK)"
        sConsulta2 = sConsulta2 & " INNER JOIN PERF_UON PU WITH (NOLOCK) ON PU.UON1= A.UON1 AND PU.UON2 IS NULL AND PU.PERF=" & lIdPerfil & " "
        sConsulta2 = sConsulta2 & " Union"
        sConsulta2 = sConsulta2 & " SELECT A.ART4"
        sConsulta2 = sConsulta2 & " FROM ART4_UON A WITH (NOLOCK)"
        sConsulta2 = sConsulta2 & " INNER JOIN PERF_UON PU WITH (NOLOCK) ON PU.UON1= A.UON1 AND  PU.UON2= A.UON2 AND PU.UON3 IS NULL AND PU.PERF=" & lIdPerfil & " "
        sConsulta2 = sConsulta2 & " Union"
        sConsulta2 = sConsulta2 & " SELECT A.ART4"
        sConsulta2 = sConsulta2 & " FROM ART4_UON A WITH (NOLOCK)"
        sConsulta2 = sConsulta2 & " INNER JOIN PERF_UON PU WITH (NOLOCK) ON PU.UON1= A.UON1 AND PU.UON2= A.UON2 AND PU.UON3= A.UON3 AND PU.PERF=" & lIdPerfil & " "
    End If

    If bRUsuUon Then
        sConsulta = sConsulta1
    End If
        
    If bRPerfUON Then
        If bRUsuUon Then
            sConsulta = sConsulta1 & " UNION " & sConsulta2
        Else
            sConsulta = sConsulta2
        End If
    End If
    ConstruirCriterioUONUsuPerfUONArt = sConsulta
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Módulo", "basPublic", "ConstruirCriterioUONUsuPerfUONArt", ERR, Erl)
      Exit Function
   End If

End Function


'<summary>construye subconsulta para generar campo denominación de Uons (titulo de uon si es única), Multiples* o todas ('')</summary>
Function construirDenominacionesUons(Optional ByVal sWhereTablaART4 As String) As String

    Dim sInnerDenominacion As String
        
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sInnerDenominacion = "SELECT DISTINCT AU.ART4 COD,CASE COUNT(UONDEN) WHEN 1 THEN UONDEN ELSE 'Multiples*' END UON_DEN"
    sInnerDenominacion = sInnerDenominacion & " FROM ART4_UON AU WITH(NOLOCK) INNER JOIN"
    sInnerDenominacion = sInnerDenominacion & " (SELECT ART4,UON1 + ' - ' + UON1.DEN UONDEN"
    sInnerDenominacion = sInnerDenominacion & " FROM ART4_UON AU WITH(NOLOCK)"
    sInnerDenominacion = sInnerDenominacion & " INNER JOIN UON1 WITH(NOLOCK) ON AU.UON1=UON1.COD AND AU.UON2 IS NULL "
    If sWhereTablaART4 <> "" Then
        sInnerDenominacion = sInnerDenominacion & " INNER JOIN ART4 WITH(NOLOCK) ON ART4.COD=AU.ART4"
    End If
    sInnerDenominacion = sInnerDenominacion & " WHERE UON1.BAJALOG=0 " & sWhereTablaART4
    
    sInnerDenominacion = sInnerDenominacion & " Union"
    sInnerDenominacion = sInnerDenominacion & " SELECT ART4,AU.UON1 + '/' + AU.UON2 + ' - ' + UON2.DEN UONDEN"
    sInnerDenominacion = sInnerDenominacion & " FROM ART4_UON AU WITH(NOLOCK)"
    sInnerDenominacion = sInnerDenominacion & " INNER JOIN UON2 WITH(NOLOCK) ON AU.UON1=UON2.UON1 AND AU.UON2 =UON2.COD AND AU.UON3 IS NULL "
    If sWhereTablaART4 <> "" Then
        sInnerDenominacion = sInnerDenominacion & " INNER JOIN ART4 WITH(NOLOCK) ON ART4.COD=AU.ART4"
    End If
    sInnerDenominacion = sInnerDenominacion & " WHERE UON2.BAJALOG=0 " & sWhereTablaART4
    
    sInnerDenominacion = sInnerDenominacion & " Union"
    sInnerDenominacion = sInnerDenominacion & " SELECT ART4,AU.UON1 + '/' +AU.UON2 + '/' + AU.UON3 + ' - ' + UON3.DEN UONDEN"
    sInnerDenominacion = sInnerDenominacion & " FROM ART4_UON AU WITH(NOLOCK)"
    sInnerDenominacion = sInnerDenominacion & " INNER JOIN UON3 WITH(NOLOCK) ON AU.UON1=UON3.UON1 AND AU.UON2=UON3.UON2 AND AU.UON3=UON3.COD "
    If sWhereTablaART4 <> "" Then
        sInnerDenominacion = sInnerDenominacion & " INNER JOIN ART4 WITH(NOLOCK) ON ART4.COD=AU.ART4"
    End If
    sInnerDenominacion = sInnerDenominacion & " WHERE UON3.BAJALOG=0 " & sWhereTablaART4
    
    sInnerDenominacion = sInnerDenominacion & " )ARTDEN ON AU.ART4=ARTDEN.ART4"
    sInnerDenominacion = sInnerDenominacion & " GROUP BY AU.ART4, UONDEN"
    
    construirDenominacionesUons = sInnerDenominacion
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Módulo", "basPublic", "construirDenominacionesUons", ERR, Erl)
      Exit Function
   End If

End Function
'<summary>filtra por uons en una determinada selección (pertenencia a cualquiera de ellas)</summary>
'<param name="oUons">Colección de uons de cualquier nivel</param>
'<param name="bIncluirAscendientes">Indica si debemos incluir los ascendientes de una uon </param>
Public Function construirRestriccionPorUons(oUons As CUnidadesOrganizativas, Optional ByVal bIncluirAscendientes As Boolean, Optional ByVal bIncluirDescendientes As Boolean) As String
    'filtrar por un conjunto de uons
    Dim swhereuons As String
    Dim oUON As IUon
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not oUons Is Nothing Then
    If oUons.Count > 0 Then
    
        swhereuons = " AND ((1=2 "
        
        For Each oUON In oUons
            If oUON.Nivel = 3 Then
                'Nivel 3
                swhereuons = swhereuons & " OR AU.ART4 IN ("
                swhereuons = swhereuons & " SELECT DISTINCT AU2.ART4"
                swhereuons = swhereuons & " FROM ART4_UON AU2 WITH (NOLOCK) "
                swhereuons = swhereuons & " WHERE ((AU2.UON1='" & DblQuote(oUON.CodUnidadOrgNivel1) & "' AND AU2.UON2 ='" & DblQuote(oUON.CodUnidadOrgNivel2) & "' AND AU2.UON3 ='" & DblQuote(oUON.CodUnidadOrgNivel3) & "' )"
                If bIncluirAscendientes Then
                    swhereuons = swhereuons & " OR  (AU2.UON1='" & DblQuote(oUON.CodUnidadOrgNivel1) & "' AND AU2.UON2 IS NULL)"
                    swhereuons = swhereuons & " OR  (AU2.UON1='" & DblQuote(oUON.CodUnidadOrgNivel1) & "' AND AU2.UON2 ='" & DblQuote(oUON.CodUnidadOrgNivel2) & "' AND AU2.UON3 IS NULL)"
                End If
                swhereuons = swhereuons & "))"
                
            ElseIf oUON.Nivel = 2 Then
                'NIVEL 2
                swhereuons = swhereuons & " OR AU.ART4 IN ("
                swhereuons = swhereuons & " SELECT DISTINCT AU2.ART4"
                swhereuons = swhereuons & " FROM ART4_UON AU2 WITH (NOLOCK) "
                swhereuons = swhereuons & " WHERE ((AU2.UON1='" & oUON.CodUnidadOrgNivel1 & "' AND AU2.UON2 ='" & oUON.CodUnidadOrgNivel2 & "' "
                If Not bIncluirDescendientes Then
                    swhereuons = swhereuons & " AND AU2.UON3 IS NULL"
                End If
                swhereuons = swhereuons & ")"
                If bIncluirAscendientes Then
                    swhereuons = swhereuons & " OR (AU2.UON1='" & DblQuote(oUON.CodUnidadOrgNivel1) & "' AND AU2.UON2 IS NULL)"
                End If
                swhereuons = swhereuons & "))"
            ElseIf oUON.Nivel = 1 Then
                swhereuons = swhereuons & " OR AU.ART4 IN ("
                swhereuons = swhereuons & " SELECT DISTINCT AU2.ART4"
                swhereuons = swhereuons & " FROM ART4_UON AU2 WITH (NOLOCK) "
                swhereuons = swhereuons & " WHERE AU2.UON1='" & DblQuote(oUON.CodUnidadOrgNivel1) & "'"
                If Not bIncluirDescendientes Then
                    swhereuons = swhereuons & " AND AU2.UON2 IS NULL"
                End If
                swhereuons = swhereuons & ") "
            End If
            
        Next
        swhereuons = swhereuons & " ))"
    End If
    End If
    construirRestriccionPorUons = swhereuons
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Módulo", "basPublic", "construirRestriccionPorUons", ERR, Erl)
      Exit Function
   End If

End Function

Public Function ConstruirCriterioUONUsuPerfUONAtrib(ByVal bRUsuUon As Boolean, ByVal bRPerfUON As Boolean, _
        ByVal lIdPerfil As Long, Optional ByVal UON1 As String, Optional ByVal UON2 As String, Optional ByVal UON3 As String, _
        Optional bMostrarUonsIncluidas As Boolean = True) As String
    Dim sConsulta As String
    Dim sConsulta1 As String
    Dim sConsulta2 As String
    Dim sConsultaPerf As String
    Dim iNivel As Integer
    iNivel = 3
    
    If UON3 = "" Then
        iNivel = 2
        If UON2 = "" Then
            iNivel = 1
            If UON1 = "" Then
                'nivel 0
                iNivel = 0
            End If
        End If
    End If
       
    sConsulta = "SELECT ATRIB"
    sConsulta = sConsulta & " FROM DEF_ATRIB DA WITH(NOLOCK)"
    If iNivel > 0 Then
        If bRUsuUon Then
            'COINCIDENCIA PERFECTA
            sConsulta1 = " SELECT ATRIB"
            sConsulta1 = sConsulta1 & " FROM UON_ATRIB UA WITH(NOLOCK)"
            sConsulta1 = sConsulta1 & " WHERE UA.UON1='" & UON1 & "' AND ISNULL(UA.UON2,'')='" & UON2 & "' AND ISNULL(UA.UON3,'')='" & UON3 & "'"
            If bMostrarUonsIncluidas Then
                If iNivel = 1 Then
                    sConsulta1 = sConsulta1 & " UNION "
                    sConsulta1 = sConsulta1 & " SELECT ATRIB"
                    sConsulta1 = sConsulta1 & " FROM UON_ATRIB UA WITH(NOLOCK)"
                    sConsulta1 = sConsulta1 & " WHERE UA.UON1='" & UON1 & "'"
                ElseIf iNivel = 2 Then
                        sConsulta1 = sConsulta1 & " UNION "
                        sConsulta1 = sConsulta1 & " SELECT ATRIB"
                        sConsulta1 = sConsulta1 & " FROM UON_ATRIB UA WITH(NOLOCK)"
                        sConsulta1 = sConsulta1 & " WHERE UA.UON1='" & UON1 & "' AND UA.UON2='" & UON2 & "'"
                End If
                
            End If
        End If
    End If
    
    If bRPerfUON Then
        'coincidencia completa
        sConsulta2 = " SELECT UA.ATRIB"
        sConsulta2 = sConsulta2 & " FROM UON_ATRIB UA WITH (NOLOCK)"
        sConsulta2 = sConsulta2 & " INNER JOIN PERF_UON PU WITH (NOLOCK) ON PU.UON1= UA.UON1 AND ISNULL(PU.UON2,'')=ISNULL(UA.UON2,'') AND ISNULL(PU.UON3,'')=ISNULL(UA.UON3,'')"
        sConsulta2 = sConsulta2 & " Where PU.PERF = 93"
        'Incluir descendientes
        If bMostrarUonsIncluidas Then
            sConsulta2 = " SELECT UA.ATRIB"
            sConsulta2 = sConsulta2 & " FROM UON_ATRIB UA WITH (NOLOCK)"
            sConsulta2 = sConsulta2 & " INNER JOIN PERF_UON PU WITH (NOLOCK) ON PU.UON1= UA.UON1 AND PU.UON2 IS NULL "
            sConsulta2 = sConsulta2 & " WHERE PU.PERF=" & lIdPerfil
            sConsulta2 = sConsulta2 & " UNION"
            sConsulta2 = sConsulta2 & " SELECT UA.ATRIB"
            sConsulta2 = sConsulta2 & " FROM UON_ATRIB UA WITH (NOLOCK)"
            sConsulta2 = sConsulta2 & " INNER JOIN PERF_UON PU WITH (NOLOCK) ON PU.UON1= UA.UON1 AND  PU.UON2= UA.UON2 AND PU.UON3 IS NULL AND PU.PERF=" & lIdPerfil
        End If
    End If

    If bRUsuUon Then
        sConsultaPerf = sConsulta1
    End If
    
    If bRPerfUON Then
        If bRUsuUon Then
            sConsultaPerf = sConsulta1 & " UNION " & sConsulta2
        Else
            sConsultaPerf = sConsulta2
        End If
    End If
    If sConsultaPerf <> "" Then
        sConsulta = sConsulta & " INNER JOIN ( " & sConsultaPerf & " )UP ON UP.ATRIB=DA.ID"
    End If
    ConstruirCriterioUONUsuPerfUONAtrib = sConsulta
End Function



Public Function construirRestriccionPorUonsAtrib(oUons As CUnidadesOrganizativas, Optional ByVal bNoMostrarDescendientes As Boolean) As String
    'filtrar por un conjunto de uons
    Dim swhereuons As String
    Dim oUON As IUon
    If oUons.Count > 0 Then
    
        swhereuons = " AND ( 1=2 "
        
        For Each oUON In oUons
            If oUON.Nivel = 3 Then
                'Nivel 3
                swhereuons = swhereuons & " OR DEF_ATRIB.ID IN ("
                swhereuons = swhereuons & " SELECT DISTINCT UA.ATRIB"
                swhereuons = swhereuons & " FROM UON_ATRIB UA WITH (NOLOCK) "
                swhereuons = swhereuons & " WHERE UA.UON1='" & oUON.CodUnidadOrgNivel1 & "' AND UA.UON2 ='" & oUON.CodUnidadOrgNivel2 & "' AND UA.UON3 ='" & oUON.CodUnidadOrgNivel3 & "' )"
                
            ElseIf oUON.Nivel = 2 Then
                'NIVEL 2
                swhereuons = swhereuons & " OR DEF_ATRIB.ID IN ("
                swhereuons = swhereuons & " SELECT DISTINCT UA.ATRIB"
                swhereuons = swhereuons & " FROM UON_ATRIB UA WITH (NOLOCK) "
                swhereuons = swhereuons & " WHERE UA.UON1='" & oUON.CodUnidadOrgNivel1 & "' AND UA.UON2 ='" & oUON.CodUnidadOrgNivel2 & "' "
                If bNoMostrarDescendientes Then
                    swhereuons = swhereuons & " AND UA.UON3 IS NULL "
                End If
                swhereuons = swhereuons & ")"
                
            ElseIf oUON.Nivel = 1 Then
                swhereuons = swhereuons & " OR DEF_ATRIB.ID IN ("
                swhereuons = swhereuons & " SELECT DISTINCT UA.ATRIB"
                swhereuons = swhereuons & " FROM UON_ATRIB UA WITH (NOLOCK) "
                swhereuons = swhereuons & " WHERE UA.UON1='" & oUON.CodUnidadOrgNivel1 & "'"
                If bNoMostrarDescendientes Then
                    swhereuons = swhereuons & " AND UA.UON2 IS NULL "
                End If
                swhereuons = swhereuons & ") "
            End If
            
        Next
        swhereuons = swhereuons & " )"
    End If
    construirRestriccionPorUonsAtrib = swhereuons
End Function

'<summary>Crea un objeto Uon (de cualquier nivel) en función de los codigos </summary>
'<returns>Objeto de tipo CUnidadOrgNivel1, 2 o 3
Public Function createUon(ByVal sUON1 As String, ByVal sUON2 As Variant, ByVal sUON3 As Variant) As Variant
    
    Dim oUON As IUon
    If NullToStr(sUON3) <> "" Then
        Set oUON = New CUnidadOrgNivel3
        oUON.CodUnidadOrgNivel3 = sUON3
        oUON.CodUnidadOrgNivel2 = sUON2
        oUON.CodUnidadOrgNivel1 = sUON1
    Else
        If NullToStr(sUON2) <> "" Then
            Set oUON = New CUnidadOrgNivel2
            oUON.CodUnidadOrgNivel1 = sUON1
            oUON.CodUnidadOrgNivel2 = sUON2
        Else
            If NullToStr(sUON1) <> "" Then
                Set oUON = New CUnidadOrgNivel1
                oUON.CodUnidadOrgNivel1 = sUON1
            Else
                Set oUON = New CUnidadOrgNivel0
            End If
        End If
    End If
    Set createUon = oUON
    Set oUON = Nothing
End Function

Public Function construirFiltroArticulosPorAtributo(ByRef oAtributos As CAtributos, Optional ByRef oUons As CUnidadesOrganizativas, Optional ByVal bIncluirDescendientes As Boolean) As String
    Dim sConsulta As String
    Dim oUON As IUon
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not oAtributos Is Nothing Then
        Dim oAtributo As CAtributo
        If oAtributos.Count > 0 Then
            For Each oAtributo In oAtributos
                Dim sWhereValor As String
                Select Case oAtributo.Tipo
                    Case TiposDeAtributos.TipoString, TiposDeAtributos.TipoTextoCorto, TiposDeAtributos.TipoTextoLargo, TiposDeAtributos.TipoTextoMedio
                        If Not IsEmpty(oAtributo.ValorText) And Not IsNull(oAtributo.ValorText) Then
                            sWhereValor = sWhereValor & " AND AA.VALOR_TEXT LIKE '" & DblQuote(oAtributo.ValorText) & "%'"
                        End If
                    Case TiposDeAtributos.TipoNumerico
                        If Not IsEmpty(oAtributo.ValorText) And Not IsNull(oAtributo.ValorNum) Then
                            sWhereValor = sWhereValor & " AND AA.VALOR_NUM" & oAtributo.Formula & oAtributo.ValorNum
                        End If
                    Case TiposDeAtributos.TipoFecha
                        If Not IsEmpty(oAtributo.ValorText) And Not IsNull(oAtributo.ValorFec) Then
                            sWhereValor = sWhereValor & " AND AA.VALOR_FEC=" & DateToSQLTimeDate(oAtributo.ValorFec)
                        End If
                    Case TiposDeAtributos.TipoBoolean
                        If Not IsEmpty(oAtributo.ValorText) And Not IsNull(oAtributo.ValorBool) Then
                            sWhereValor = sWhereValor & " AND AA.VALOR_BOOL=" & BooleanToSQLBinary(oAtributo.ValorBool)
                        End If
                End Select
                
                If oAtributo.AmbitoAtributo <> TAmbitoUon Then
                    sConsulta = sConsulta & " AND (EXISTS(SELECT AA.ART"
                    sConsulta = sConsulta & " FROM ART4_ATRIB AA WITH (NOLOCK)"
                    sConsulta = sConsulta & " WHERE AA.ART=ART4.COD"
                    sConsulta = sConsulta & " AND AA.ATRIB=" & oAtributo.Id
                    If sWhereValor <> "" Then
                        sConsulta = sConsulta & sWhereValor
                    End If
                    sConsulta = sConsulta & "))"
                End If
            
                'atributos UON
                If oAtributo.AmbitoAtributo = TAmbitoUon Then
                    If oUons.Count > 0 Then
                        sConsulta = sConsulta & " AND ART4.COD IN ( "
                        sConsulta = sConsulta & _
                            " SELECT AA.ART FROM ART4_UON_ATRIB AA WITH(NOLOCK) WHERE AA.ATRIB= " _
                            & oAtributo.Id
                        For Each oUON In oUons
                            If oUON.Nivel = 1 Then
                                sConsulta = sConsulta & " AND (AA.UON1='" & oUON.CodUnidadOrgNivel1 & "' "
                                If Not bIncluirDescendientes Then
                                    sConsulta = sConsulta & " AND AA.UON2 IS NULL"
                                End If
                                sConsulta = sConsulta & " )"
                            End If
                                
                            If oUON.Nivel = 2 Then
                                sConsulta = sConsulta & " AND (AA.UON1='" & oUON.CodUnidadOrgNivel1 & "' AND AA.UON2='" & oUON.CodUnidadOrgNivel2 & "' "
                                If Not bIncluirDescendientes Then
                                    sConsulta = sConsulta & " AND AA.UON3 IS NULL"
                                End If
                                sConsulta = sConsulta & " OR AA.UON1='" & oUON.CodUnidadOrgNivel1 & "' AND AA.UON2 IS NULL)"
                            End If
                            
                            If oUON.Nivel >= 3 Then
                                sConsulta = sConsulta & " AND (AA.UON1='" & oUON.CodUnidadOrgNivel1 & "' AND AA.UON2 IS NULL"
                                sConsulta = sConsulta & " OR AA.UON1='" & oUON.CodUnidadOrgNivel1 & "' AND AA.UON2='" & oUON.CodUnidadOrgNivel2 & "' AND AA.UON3 IS NULL"
                                sConsulta = sConsulta & " OR AA.UON1='" & oUON.CodUnidadOrgNivel1 & "' AND AA.UON2='" & oUON.CodUnidadOrgNivel2 & "' AND AA.UON3='" & oUON.CodUnidadOrgNivel3 & "')"
                            End If
                            If sWhereValor <> "" Then
                                sConsulta = sConsulta & sWhereValor
                            End If
                        Next
                        
                        sConsulta = sConsulta & ")"
                    Else
                        sConsulta = sConsulta & " AND ART4.COD IN ( "
                        sConsulta = sConsulta & _
                            " SELECT AA.ART FROM ART4_UON_ATRIB AA WITH(NOLOCK) WHERE AA.ATRIB= " _
                            & oAtributo.Id & sWhereValor & " )"
                    End If
                End If
                
            Next
        End If
    End If
    construirFiltroArticulosPorAtributo = sConsulta
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Módulo", "basPublic", "construirFiltroArticulosPorAtributo", ERR, Erl)
      Exit Function
   End If

End Function

''' <summary>
''' En el momento de crear pedidos directos se usan los datos sacados de la solicitud, estan llegando con espacios/tabuladores/saltos de linea
''' algunos codigos de articulo y/o denominacion de articulo. Esto provoca q se diga q no existe y se pida darlo de alta ... lo q no es correcto
''' </summary>
''' <param name="Texto">Texto a formatear</param>
''' <returns>Texto sin espacios ni tabuladores ni saltos de linea ni por delante ni por detras</returns>
''' <remarks>Llamada desde: frmSOLAbrirProc/GenerarProcesoCompra ; Tiempo máximo: 0,2</remarks>
Public Function QuitarCaracteresRarosDeSolicitud(ByVal texto As String) As String
    Dim str As String
    Dim mirar As Boolean
    
    str = texto
    
    str = LTrim(str)
    str = RTrim(str)
    
    mirar = True
    While mirar
        If (Left(str, 1) = vbTab) Then
            str = Mid(str, 2)
        ElseIf (Left(str, 2) = vbCrLf) Then
            str = Mid(str, 3)
        ElseIf (Left(str, 1) = vbCr) Then
            str = Mid(str, 2)
        ElseIf (Right(str, 1) = vbTab) Then
            str = Mid(str, 1, Len(str) - 1)
        ElseIf (Right(str, 2) = vbCrLf) Then
            str = Mid(str, 1, Len(str) - 2)
        ElseIf (Right(str, 1) = vbCr) Then
            str = Mid(str, 1, Len(str) - 1)
        Else
            mirar = False
        End If
    Wend
    
    QuitarCaracteresRarosDeSolicitud = str
End Function

''' <summary>
''' Obtiene la empresa asociada al centro de coste de la solicitud
''' </summary>
''' <param name="CentroCoste">Centreo de coste</param>
''' <returns>El id d ela empresa para pasarselo a la pantalla frmpedidos</returns>
''' <remarks>Llamada desde: cmdContinuar_Click(); Tiempo máximo: 0,1</remarks>

Public Function BuscarEmpresaDeCC(ByVal CentroCoste As String, ByRef oConexion As CConexion) As CEmpresa
    Dim arrUons() As String
    Dim oEmpresa As CEmpresa
    Dim UON1 As String
    Dim UON2 As String
    Dim UON3 As String
    Dim UON4 As String
    Dim iNivel As Integer
    Dim i As Integer
    Dim oUON1 As CUnidadOrgNivel1
    Dim oUON2 As CUnidadOrgNivel2
    Dim oUON3 As CUnidadOrgNivel3
    Dim oUON4 As CUnidadOrgNivel4
        
    arrUons = Split(CentroCoste, "#")
    iNivel = 1
    For i = 0 To UBound(arrUons) - 1
        Select Case iNivel
            Case 1
                UON1 = arrUons(i)
            Case 2
                UON2 = arrUons(i)
            Case 3
                UON3 = arrUons(i)
            Case 4
                UON4 = arrUons(i)
        End Select
        
        iNivel = iNivel + 1
    Next
     
    If iNivel > 1 Then iNivel = iNivel - 1
    
    Select Case iNivel
        Case 1
            Set oUON1 = New CUnidadOrgNivel1
            Set oUON1.Conexion = oConexion
            oUON1.Cod = UON1
            Set oEmpresa = oUON1.CargarEmpresa
        
        Case 2
            Set oUON2 = New CUnidadOrgNivel2
            Set oUON2.Conexion = oConexion
            oUON2.CodUnidadOrgNivel1 = UON1
            oUON2.Cod = UON2
            Set oEmpresa = oUON2.CargarEmpresa
            
            If oEmpresa Is Nothing Then
                Set oUON1 = New CUnidadOrgNivel1
                Set oUON1.Conexion = oConexion
                oUON1.Cod = UON1
                Set oEmpresa = oUON1.CargarEmpresa
            End If
        
        Case 3
            Set oUON3 = New CUnidadOrgNivel3
            Set oUON3.Conexion = oConexion
            oUON3.CodUnidadOrgNivel1 = UON1
            oUON3.CodUnidadOrgNivel2 = UON2
            oUON3.Cod = UON3
            Set oEmpresa = oUON3.CargarEmpresa
            
            If oEmpresa Is Nothing Then
                Set oUON2 = New CUnidadOrgNivel2
                Set oUON2.Conexion = oConexion
                oUON2.CodUnidadOrgNivel1 = UON1
                oUON2.Cod = UON2
                Set oEmpresa = oUON2.CargarEmpresa
            
                If oEmpresa Is Nothing Then
                    Set oUON1 = New CUnidadOrgNivel1
                    Set oUON1.Conexion = oConexion
                    oUON1.Cod = UON1
                    Set oEmpresa = oUON1.CargarEmpresa
                End If
            End If
        
        Case 4
            Set oUON4 = New CUnidadOrgNivel4
            Set oUON4.Conexion = oConexion
            oUON4.CodUnidadOrgNivel1 = UON1
            oUON4.CodUnidadOrgNivel2 = UON2
            oUON4.CodUnidadOrgNivel3 = UON3
            oUON4.Cod = UON4
            Set oEmpresa = oUON4.CargarEmpresa
        
            If oEmpresa Is Nothing Then
                Set oUON3 = New CUnidadOrgNivel3
                Set oUON3.Conexion = oConexion
                oUON3.CodUnidadOrgNivel1 = UON1
                oUON3.CodUnidadOrgNivel2 = UON2
                oUON3.Cod = UON3
                Set oEmpresa = oUON3.CargarEmpresa
            
                If oEmpresa Is Nothing Then
                    Set oUON2 = New CUnidadOrgNivel2
                    Set oUON2.Conexion = oConexion
                    oUON2.CodUnidadOrgNivel1 = UON1
                    oUON2.Cod = UON2
                    Set oEmpresa = oUON2.CargarEmpresa
                
                    If oEmpresa Is Nothing Then
                        Set oUON1 = New CUnidadOrgNivel1
                        Set oUON1.Conexion = oConexion
                        oUON1.Cod = UON1
                        Set oEmpresa = oUON1.CargarEmpresa
                    End If
                End If
            End If
    End Select
    
    Set BuscarEmpresaDeCC = oEmpresa
End Function
