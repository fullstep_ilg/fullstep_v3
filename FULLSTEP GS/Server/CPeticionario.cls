VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CPeticionario"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CPersona **********************************
'*             Autor : Javier Arana
'*             Creada : 27/7/98
'* **************************************************************
' PENDIENTE DE DESARROLLO

Option Explicit

' *********************** Variables privadas de la clase *********
Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
  
End Enum

Implements IBaseDatos

Private m_lID As Long
Private m_lSolicitud As Long
Private m_vCod As Variant
Private m_vNombre As Variant
Private m_vApellidos As Variant
Private m_vUON1 As Variant
Private m_vUON2 As Variant
Private m_vUON3 As Variant
Private m_vCodDep As Variant
Private m_vDenDep As Variant
Private m_vIndice As Long
Private m_vFecAct As Variant

Private m_oConexion As CConexion

Friend Property Set Conexion(ByVal con As CConexion)
    Set m_oConexion = con
End Property

Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property

Public Property Get Id() As Long
  Id = m_lID
End Property
Public Property Let Id(ByVal vData As Long)
    m_lID = vData
End Property

Public Property Get Solicitud() As Long
  Solicitud = m_lSolicitud
End Property
Public Property Let Solicitud(ByVal vData As Long)
    m_lSolicitud = vData
End Property

Public Property Get Cod() As Variant
  Cod = m_vCod
End Property
Public Property Let Cod(ByVal vData As Variant)
    m_vCod = vData
End Property

Public Property Let CodDep(ByVal vData As Variant)
    m_vCodDep = vData
End Property

Public Property Get CodDep() As Variant
    CodDep = m_vCodDep
End Property

Public Property Let DenDep(ByVal vData As Variant)
    m_vDenDep = vData
End Property

Public Property Get DenDep() As Variant
    DenDep = m_vDenDep
End Property

Public Property Let UON1(ByVal vData As Variant)
    m_vUON1 = vData
End Property
Public Property Get UON1() As Variant
    UON1 = m_vUON1
End Property

Public Property Let UON2(ByVal vData As Variant)
    m_vUON2 = vData
End Property
Public Property Get UON2() As Variant
    UON2 = m_vUON2
End Property

Public Property Let UON3(ByVal vData As Variant)
    m_vUON3 = vData
End Property
Public Property Get UON3() As Variant
    UON3 = m_vUON3
End Property

Public Property Let Apellidos(ByVal vData As Variant)
    m_vApellidos = vData
End Property
Public Property Get Apellidos() As Variant
    Apellidos = m_vApellidos
End Property

Public Property Let Nombre(ByVal vData As Variant)
    m_vNombre = vData
End Property
Public Property Get Nombre() As Variant
    Nombre = m_vNombre
End Property

Public Property Get Indice() As Long
    Indice = m_vIndice
End Property
Public Property Let Indice(ByVal Ind As Long)
    m_vIndice = Ind
End Property

Public Property Let FecAct(ByVal vData As Variant)
    m_vFecAct = vData
End Property
Public Property Get FecAct() As Variant
    FecAct = m_vFecAct
End Property

Private Sub Class_Terminate()
    Set m_oConexion = Nothing
End Sub

Private Function IBaseDatos_AnyadirABaseDatos() As TipoErrorSummit
Dim sConsulta As String
Dim TESError As TipoErrorSummit
Dim rs As adodb.Recordset
Dim iTipo As Integer


TESError.NumError = TESnoerror

'********* Precondicion *******************

If m_oConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CPeticionario.AnyadirABAseDatos", "No se ha establecido la conexion"
End If


If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
   
If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
    
    sConsulta = "INSERT INTO PETICIONARIOS (SOLICITUD,UON1,UON2,UON3,DEP,PER) VALUES (" & m_lSolicitud & "," & StrToSQLNULL(m_vUON1)
    sConsulta = sConsulta & "," & StrToSQLNULL(m_vUON2) & "," & StrToSQLNULL(m_vUON3) & "," & StrToSQLNULL(m_vCodDep) & "," & StrToSQLNULL(m_vCod) & ")"
    m_oConexion.ADOCon.Execute sConsulta
    
    'Si la solicitud no es de tipo certificado o no conformidad se le dar�n permisos de acceso al PM , sino de acceso al QA.
    Set rs = New adodb.Recordset
    sConsulta = "SELECT TS.TIPO FROM SOLICITUD S INNER JOIN TIPO_SOLICITUDES TS ON S.TIPO=TS.ID WHERE S.ID=" & m_lSolicitud
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
    iTipo = rs.Fields("TIPO").Value
    rs.Close
    
    If iTipo = TipoSolicitud.Certificados Or iTipo = TipoSolicitud.NoConformidades Then
        sConsulta = "UPDATE USU SET FSQA=1"
    Else
        sConsulta = "UPDATE USU SET SOL_COMPRA=1"
    End If
    
    If m_vCod <> "" And Not IsNull(m_vCod) Then
        sConsulta = sConsulta & " WHERE COD='" & DblQuote(m_vCod) & "'"
    Else
        sConsulta = sConsulta & " FROM USU U INNER JOIN PER ON U.PER=PER.COD WHERE "
        If m_vUON1 <> "" And Not IsNull(m_vUON1) Then
            sConsulta = sConsulta & " PER.UON1='" & DblQuote(m_vUON1) & "'"
        Else
            sConsulta = sConsulta & " PER.UON1 IS NULL"
        End If
        If m_vUON2 <> "" And Not IsNull(m_vUON2) Then
            sConsulta = sConsulta & " AND PER.UON2='" & DblQuote(m_vUON2) & "'"
        Else
            sConsulta = sConsulta & " AND PER.UON2 IS NULL"
        End If
        If m_vUON3 <> "" And Not IsNull(m_vUON3) Then
            sConsulta = sConsulta & " AND PER.UON3='" & DblQuote(m_vUON3) & "'"
        Else
            sConsulta = sConsulta & " AND PER.UON3 IS NULL"
        End If
        If m_vCodDep <> "" And Not IsNull(m_vCodDep) Then
            sConsulta = sConsulta & " AND PER.DEP='" & DblQuote(m_vCodDep) & "'"
        End If
    End If
    m_oConexion.ADOCon.Execute sConsulta
    
    
    
    sConsulta = "SELECT MAX(ID) AS ID FROM PETICIONARIOS "
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
    m_lID = rs("ID").Value
    rs.Close
    
    rs.Open "SELECT FECACT FROM PETICIONARIOS WHERE ID=" & m_lID, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
    m_vFecAct = rs(0).Value
    rs.Close
    Set rs = Nothing
    
    
    IBaseDatos_AnyadirABaseDatos = TESError
    Exit Function
    
Error_Cls:

    IBaseDatos_AnyadirABaseDatos = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    
    Set rs = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CPeticionario", "IBaseDatos_AnyadirABaseDatos", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If
End Function


'*********************************************************************
'FSN_DES_QA_PRT_3252 - M�ltiples notificados al cumplimentar certificados.
'Se indica que no se debe crear una clase CNotificados y reutilizar CPeticionario
'*********************************************************************
Public Function AnyadirABaseDatosComoNotificado() As TipoErrorSummit
Dim sConsulta As String
Dim TESError As TipoErrorSummit
Dim rs As adodb.Recordset
Dim iTipo As Integer


TESError.NumError = TESnoerror

'********* Precondicion *******************

If m_oConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CPeticionario.AnyadirABaseDatosComoNotificado", "No se ha establecido la conexion"
End If

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
   
If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
    
    sConsulta = "INSERT INTO NOTIFICADOS (SOLICITUD,UON1,UON2,UON3,DEP,PER) VALUES (" & m_lSolicitud & "," & StrToSQLNULL(m_vUON1)
    sConsulta = sConsulta & "," & StrToSQLNULL(m_vUON2) & "," & StrToSQLNULL(m_vUON3) & "," & StrToSQLNULL(m_vCodDep) & "," & StrToSQLNULL(m_vCod) & ")"
    m_oConexion.ADOCon.Execute sConsulta
    
    'Si la solicitud no es de tipo certificado o no conformidad se le dar�n permisos de acceso al PM , sino de acceso al QA.
    Set rs = New adodb.Recordset
    sConsulta = "SELECT TS.TIPO FROM SOLICITUD S WITH (NOLOCK) INNER JOIN TIPO_SOLICITUDES TS WITH (NOLOCK) ON S.TIPO=TS.ID WHERE S.ID=" & m_lSolicitud
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
    iTipo = rs.Fields("TIPO").Value
    rs.Close
    
    If iTipo = TipoSolicitud.Certificados Or iTipo = TipoSolicitud.NoConformidades Then
        sConsulta = "UPDATE USU SET FSQA=1"
    Else
        sConsulta = "UPDATE USU SET SOL_COMPRA=1"
    End If
    
    If m_vCod <> "" And Not IsNull(m_vCod) Then
        sConsulta = sConsulta & " WHERE COD='" & DblQuote(m_vCod) & "'"
    Else
        sConsulta = sConsulta & " FROM USU U INNER JOIN PER ON U.PER=PER.COD WHERE "
        If m_vUON1 <> "" And Not IsNull(m_vUON1) Then
            sConsulta = sConsulta & " PER.UON1='" & DblQuote(m_vUON1) & "'"
        Else
            sConsulta = sConsulta & " PER.UON1 IS NULL"
        End If
        If m_vUON2 <> "" And Not IsNull(m_vUON2) Then
            sConsulta = sConsulta & " AND PER.UON2='" & DblQuote(m_vUON2) & "'"
        Else
            sConsulta = sConsulta & " AND PER.UON2 IS NULL"
        End If
        If m_vUON3 <> "" And Not IsNull(m_vUON3) Then
            sConsulta = sConsulta & " AND PER.UON3='" & DblQuote(m_vUON3) & "'"
        Else
            sConsulta = sConsulta & " AND PER.UON3 IS NULL"
        End If
        If m_vCodDep <> "" And Not IsNull(m_vCodDep) Then
            sConsulta = sConsulta & " AND PER.DEP='" & DblQuote(m_vCodDep) & "'"
        End If
    End If
    m_oConexion.ADOCon.Execute sConsulta
    
    
    
    sConsulta = "SELECT MAX(ID) AS ID FROM NOTIFICADOS WITH (NOLOCK) "
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
    m_lID = rs("ID").Value
    rs.Close
    
    rs.Open "SELECT FECACT FROM NOTIFICADOS WITH (NOLOCK) WHERE ID=" & m_lID, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
    m_vFecAct = rs(0).Value
    rs.Close
    Set rs = Nothing
    
    
    AnyadirABaseDatosComoNotificado = TESError
    Exit Function
    
Error_Cls:

    AnyadirABaseDatosComoNotificado = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    
    Set rs = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CPeticionario", "AnyadirABaseDatosComoNotificado", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If
End Function
'*********************************************************************

'*********************************************************************
'FSN_DES_QA_PRT_3252 - M�ltiples notificados al cumplimentar certificados.
'Se indica que no se debe crear una clase CNotificados y reutilizar CPeticionario
'*********************************************************************
Public Function EliminarDeBaseDatosComoNotificado() As TipoErrorSummit
Dim TESError As TipoErrorSummit
Dim sConsulta As String
Dim btrans As Boolean


TESError.NumError = TESnoerror

'******************* Precondicion *******************
If m_oConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CPeticionario.EliminarDeBaseDatosComoNotificado", "No se ha establecido la conexion"
End If
'*****************************************************

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
    
    '******************************************************************
    ' Codigo pendiente de desarrollo
    ' Aqui iria el codigo para comprobar si esa persona esta asociada algu proceso
    '****************************************************************
    
    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
    btrans = True
    m_oConexion.ADOCon.Execute "DELETE FROM NOTIFICADOS WHERE ID=" & m_lID
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    btrans = False
    EliminarDeBaseDatosComoNotificado = TESError
    Exit Function
    
Error_Cls:
    
    EliminarDeBaseDatosComoNotificado = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    If btrans Then
        m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CPeticionario", "EliminarDeBaseDatosComoNotificado", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If

End Function
'*********************************************************************



Private Function IBaseDatos_CambiarCodigo(CodigoNuevo As Variant) As TipoErrorSummit

    
End Function

Private Sub IBaseDatos_CancelarEdicion()

End Sub

Private Function IBaseDatos_ComprobarExistenciaEnBaseDatos() As Boolean

End Function

Private Function IBaseDatos_EliminarDeBaseDatos() As TipoErrorSummit
Dim TESError As TipoErrorSummit
Dim sConsulta As String
Dim btrans As Boolean


TESError.NumError = TESnoerror

'******************* Precondicion *******************
If m_oConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CPeticionario.EliminarDeBaseDatos", "No se ha establecido la conexion"
End If
'*****************************************************

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
    
    '******************************************************************
    ' Codigo pendiente de desarrollo
    ' Aqui iria el codigo para comprobar si esa persona esta asociada algu proceso
    '****************************************************************
    
    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
    btrans = True
    m_oConexion.ADOCon.Execute "DELETE FROM PETICIONARIOS WHERE ID=" & m_lID
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    btrans = False
    IBaseDatos_EliminarDeBaseDatos = TESError
    Exit Function
    
Error_Cls:
    
    IBaseDatos_EliminarDeBaseDatos = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    If btrans Then
        m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CPeticionario", "IBaseDatos_EliminarDeBaseDatos", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If

End Function

Private Function IBaseDatos_FinalizarEdicionEliminando() As TipoErrorSummit

End Function

Private Function IBaseDatos_FinalizarEdicionModificando() As TipoErrorSummit
Dim TESError As TipoErrorSummit
Dim sConsulta As String
Dim btrans As Boolean
Dim rs As adodb.Recordset
Dim iTipo As Integer


TESError.NumError = TESnoerror

'********* Precondicion *******************
If m_oConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CPeticionario.FinalizarEdicionModificando", "No se ha establecido la conexion"
End If
'******************************************

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:

    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
    btrans = True

    Set rs = New adodb.Recordset
    rs.Open "SELECT FECACT FROM PETICIONARIO WHERE ID=" & m_lID, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly

   If rs.eof Then
        rs.Close
        Set rs = Nothing
        m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
        TESError.NumError = TESDatoEliminado
        TESError.Arg1 = 163 '"Peticionario de solicitud"
        IBaseDatos_FinalizarEdicionModificando = TESError
        Exit Function
    End If

    If m_vFecAct <> rs("FECACT") Then
        rs.Close
        Set rs = Nothing
        m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
        TESError.NumError = TESInfActualModificada
        IBaseDatos_FinalizarEdicionModificando = TESError
        Exit Function
    End If

    sConsulta = "UPDATE PETICIONARIOS SET UON1=" & StrToSQLNULL(m_vUON1) & ",UON2=" & StrToSQLNULL(m_vUON2) & ",UON3=" & StrToSQLNULL(m_vUON3)
    sConsulta = sConsulta & ",DEP=" & StrToSQLNULL(m_vCodDep) & ",PER=" & StrToSQLNULL(m_vCod) & " WHERE ID=" & m_lID
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    
    'Si la solicitud no es de tipo certificado o no conformidad se le dar�n permisos de acceso al PM , sino de acceso al QA.
    Set rs = New adodb.Recordset
    sConsulta = "SELECT TS.TIPO FROM SOLICITUD S INNER JOIN TIPO_SOLICITUDES TS ON S.TIPO=TS.ID WHERE S.ID=" & m_lSolicitud
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
    iTipo = rs.Fields("TIPO").Value
    rs.Close
    
    If iTipo = TipoSolicitud.Certificados Or iTipo = TipoSolicitud.NoConformidades Then
        sConsulta = "UPDATE USU SET FSQA=1"
    Else
        sConsulta = "UPDATE USU SET SOL_COMPRA=1"
    End If
    
    If m_vCod <> "" And Not IsNull(m_vCod) Then
        sConsulta = sConsulta & " WHERE COD='" & DblQuote(m_vCod) & "'"
    Else
        sConsulta = sConsulta & " FROM USU U INNER JOIN PER ON U.PER=PER.COD WHERE "
        If m_vUON1 <> "" And Not IsNull(m_vUON1) Then
            sConsulta = sConsulta & " PER.UON1='" & DblQuote(m_vUON1) & "'"
        Else
            sConsulta = sConsulta & " PER.UON1 IS NULL"
        End If
        If m_vUON2 <> "" And Not IsNull(m_vUON2) Then
            sConsulta = sConsulta & " AND PER.UON2='" & DblQuote(m_vUON2) & "'"
        Else
            sConsulta = sConsulta & " AND PER.UON2 IS NULL"
        End If
        If m_vUON3 <> "" And Not IsNull(m_vUON3) Then
            sConsulta = sConsulta & " AND PER.UON3='" & DblQuote(m_vUON3) & "'"
        Else
            sConsulta = sConsulta & " AND PER.UON3 IS NULL"
        End If
        If m_vCodDep <> "" And Not IsNull(m_vCodDep) Then
            sConsulta = sConsulta & " AND PER.DEP='" & DblQuote(m_vCodDep) & "'"
        End If
    End If
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    
    rs.Open "SELECT FECACT FROM PETICIONARIOS WHERE ID=" & m_lID, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
    m_vFecAct = rs(0).Value
    rs.Close
    Set rs = Nothing


    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    btrans = False
    IBaseDatos_FinalizarEdicionModificando = TESError

    Exit Function


Error_Cls:

    IBaseDatos_FinalizarEdicionModificando = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    If btrans Then
        m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
    End If

    On Error Resume Next
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing

    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CPeticionario", "IBaseDatos_FinalizarEdicionModificando", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If
    
End Function

Private Function IBaseDatos_IniciarEdicion(Optional ByVal Bloquear As Boolean, Optional ByVal UsuarioBloqueo As String) As TipoErrorSummit

End Function

Private Function IBaseDatos_ModificarEnBaseDatos() As Boolean


End Function


