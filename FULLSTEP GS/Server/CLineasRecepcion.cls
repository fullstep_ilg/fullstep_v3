VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CLineasRecepcion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private mCol As Collection
Private mvarEOF As Boolean
Private m_oConexion As CConexion
Public Property Get eof() As Boolean
    eof = mvarEOF
End Property
Friend Property Let eof(ByVal b As Boolean)
    mvarEOF = b
End Property

Friend Property Set Conexion(ByVal con As CConexion)
Set m_oConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = m_oConexion
End Property
Public Property Get Item(vntIndexKey As Variant) As CLineaRecepcion
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
     Set NewEnum = mCol.[_NewEnum]
End Property
Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
     Count = mCol.Count
End If


End Property

Public Sub Remove(vntIndexKey As Variant)
   
    mCol.Remove vntIndexKey
End Sub

''' <summary>A�ade una nueva l�nea a la colecci�n</summary>
''' <param name="IdLineaPed">Identificador LINEAS_PEDIDO.ID</param>
''' <param name="Dest">C�digo destino</param>
''' <param name="Uni">C�digo unidad</param>
''' <param name="Cant_Ped">Cantidad pedida</param>
''' <param name="Precio">Precio</param>
''' <param name="FecEnt">Fecha entrega</param>
''' <param name="FecAct">Fecha actualizaci�n</param>
''' <param name="ArtiCod">C�digo del art�culo</param>
''' <param name="Descr">Decripci�n de la l�nea de pedido</param>
''' <param name="CantRecepcionada">Cantidad recepcionada</param>
''' <param name="CantRecibida">Cantidad recibida</param>
''' <param name="Estado">Estado</param>
''' <param name="ID">Indice</param>
''' <param name="PedidoAbierto">Indicador de pedido abierto</param>
''' <param name="ArtConcepto">TipoArtConcepto</param>
''' <param name="ArtAlmacenar">TipoArtAlmacenable</param>
''' <param name="ArtRecepcionar">TipoArtRecpcionable</param>
''' <param name="FecIni">Fecha inicio de suministro</param>
''' <param name="FecFin">Fecha fin de suministro</param>
''' <param name="Centro">C�digo del centro de aprovisionamiento</param>
''' <param name="Num">Numero de linea de pedido</param>
''' <param name="RecepAutom">Planes de entrega automaticos de linea de pedido</param>
''' <param name="Desvio">Porcentaje del desvio a recepcionar</param>
''' <remarks>Llamada desde:CRecepcion.CargarLineasPedido Tiempo m�ximo<1 seg</remarks>
''' <remarks>Revisado por: NGO </remarks>
Public Function Add(ByVal IdLineaPed As Long, ByVal Dest As Variant, ByVal Uni As String, ByVal Cant_Ped As Double, ByVal Precio As Double, ByVal FecEnt As Variant, ByVal FecAct As Variant, _
   Optional ByVal ArtiCod As Variant, Optional ByVal Descr As String, Optional ByVal CantRecepcionada As Variant, Optional ByVal CantRecibida As Variant, Optional ByVal Estado As Variant, _
   Optional ByVal Id As Variant, Optional ByVal PedidoAbierto As Variant, Optional ByVal ArtConcepto As TipoConcepto, Optional ByVal ArtAlmacenar As TipoArtAlmacenable, Optional ByVal ArtRecepcionar As TipoArtRecepcionable, _
   Optional ByVal FecIni As Variant, Optional ByVal FecFin As Variant, Optional ByVal Centro As Variant, Optional ByVal Num As Integer, Optional RecepAutom As Boolean, Optional ByVal Desvio As Variant, _
    Optional ByVal tipoRecepcion As Integer, Optional ByVal ImportePedido As Double, Optional ByVal ImporteRecibido As Double) As CLineaRecepcion
    'create a new object
    Dim objnewmember As CLineaRecepcion
    
    Set objnewmember = New CLineaRecepcion
   
    objnewmember.IdLineaPed = IdLineaPed
    objnewmember.UniCod = Uni
    objnewmember.DestCod = Dest
    objnewmember.CantPedida = Cant_Ped
    objnewmember.Precio = Precio
    objnewmember.FechaEntrega = FecEnt
    
    If IsMissing(FecAct) Then
        objnewmember.FechaAct = Null
    Else
        objnewmember.FechaAct = FecAct
    End If
        
    If IsMissing(CantRecepcionada) Then
        objnewmember.CantRecepcionada = Null
    Else
        objnewmember.CantRecepcionada = CantRecepcionada
    End If
    
    If IsMissing(CantRecibida) Then
        objnewmember.CantRecibida = Null
    Else
        objnewmember.CantRecibida = CantRecibida
    End If
    
    If IsMissing(ArtiCod) Then
        objnewmember.ArticuloCod = Null
    Else
        objnewmember.ArticuloCod = ArtiCod
    End If
    
    If IsMissing(Descr) Then
        objnewmember.Descr = Null
    Else
        objnewmember.Descr = Descr
    End If
    
    If IsMissing(Estado) Then
        objnewmember.LineaEstado = Null
    Else
        objnewmember.LineaEstado = Estado
    End If

    If IsMissing(Id) Then
        objnewmember.Id = Null
    Else
        objnewmember.Id = Id
    End If
    
    If IsMissing(PedidoAbierto) Then
        objnewmember.PedidoAbierto = 0
    Else
        objnewmember.PedidoAbierto = PedidoAbierto
    End If
    objnewmember.ArtConcepto = ArtConcepto
    objnewmember.ArtAlmacenar = ArtAlmacenar
    objnewmember.ArtRecepcionar = ArtRecepcionar
    If IsMissing(FecIni) Then
        objnewmember.FechaInicioSuministro = Null
    Else
        objnewmember.FechaInicioSuministro = FecIni
    End If
    If IsMissing(FecIni) Then
        objnewmember.FechaFinSuministro = Null
    Else
        objnewmember.FechaFinSuministro = FecFin
    End If
    
    If IsMissing(Centro) Then
        objnewmember.Centro = Null
    Else
        objnewmember.Centro = Centro
    End If

    If IsMissing(Num) Then
    Else
        objnewmember.Num = Num
    End If

    If IsMissing(RecepAutom) Then
    Else
        objnewmember.RecepAutom = RecepAutom
    End If
    
    If IsMissing(Desvio) Then
        objnewmember.Desvio = Null
    Else
        objnewmember.Desvio = Desvio
    End If
    objnewmember.tipoRecepcion = tipoRecepcion
    objnewmember.ImportePedido = ImportePedido
    objnewmember.ImporteRecibido = ImporteRecibido
    
    Set objnewmember.Conexion = m_oConexion
    
    mCol.Add objnewmember, CStr(IdLineaPed)
    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing


End Function

Private Sub Class_Initialize()
''' * Objetivo: Crear la coleccion
   
    Set mCol = New Collection
    
End Sub

Private Sub Class_Terminate()

    Set mCol = Nothing
    
End Sub

'''<summary>devuelve el total de importe recepcionado cuando la recepci�n es por importe</summary>
'''<returns>Importe</returns>
Public Function getTotalImporteRecepcionado() As Double
    Dim dImporte As Double
    Dim oLinea As CLineaRecepcion
    For Each oLinea In mCol
        If oLinea.isRecepcionImporte Then
            dImporte = dImporte + (NullToDbl0(oLinea.ImporteRecibido))
        Else 'Recepcion por cantidad
            dImporte = dImporte + oLinea.Precio * NullToDbl0(oLinea.CantRecibida)
        End If
    Next
    getTotalImporteRecepcionado = dImporte
End Function

Public Function getCantidadRecepcionada() As Double
    Dim Cantidad As Double
    Dim oLinea As CLineaRecepcion
    
    For Each oLinea In mCol
        Cantidad = Cantidad + (NullToDbl0(oLinea.CantRecepcionada))
    Next
    getCantidadRecepcionada = Cantidad
End Function
