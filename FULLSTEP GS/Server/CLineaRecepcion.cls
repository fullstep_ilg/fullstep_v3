VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CLineaRecepcion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

Private mvarId As Variant
Private mvarIdLineaPed As Long
Private mvarArtCod As Variant
Private mvarDescr As Variant
Private mvarDestCod As String
Private mvarUniCod As String
Private mvarPrecio As Double
Private mvarCantPedida As Double
Private mvarCantRecepcionada As Variant
Private mvarCantRecibida As Variant
Private mvarFecEntrega As Variant
Private mvarLineaEstado As Variant
Private mvarFecAct As Variant
Private mvarPedidoAbierto As Variant
Private mvarRecep As CRecepcion
Private mvarConexion As CConexion
Private m_oActivo As CActivo
Private m_oAlmacen As CAlmacen
Private m_udtConcepto As TipoConcepto
Private m_udtAlmacen As TipoArtAlmacenable
Private m_udtRecepcion As TipoArtRecepcionable
Private m_vFecInicio As Variant
Private m_vFecFin As Variant
Private mvarCentro As Variant
Private mvarDesvio As Variant
Private m_iNum As Integer
Private m_bRecepAutom As Boolean
Private m_fImporteRecibido As Double
Private m_oImputaciones As CImputaciones
Private m_iTipoRecepcion As Integer
Private m_dImportePedido As Double
Private m_dImporteRecepcionado As Double
Private m_bSalidaAlmacen As Boolean

Public Property Let Desvio(ByVal Dat As Variant)
    mvarDesvio = Dat
End Property
Public Property Get Desvio() As Variant
    Desvio = mvarDesvio
End Property

Public Property Let FechaInicioSuministro(ByVal Dat As Variant)
    m_vFecInicio = Dat
End Property
Public Property Get FechaInicioSuministro() As Variant
    FechaInicioSuministro = m_vFecInicio
End Property
Public Property Let FechaFinSuministro(ByVal Dat As Variant)
    m_vFecFin = Dat
End Property
Public Property Get FechaFinSuministro() As Variant
    FechaFinSuministro = m_vFecFin
End Property


Public Property Get Imputaciones() As CImputaciones
Set Imputaciones = m_oImputaciones
End Property
Public Property Set Imputaciones(ByVal Data As CImputaciones)
  Set m_oImputaciones = Data
End Property
Public Property Get Activo() As CActivo
Set Activo = m_oActivo
End Property
Public Property Set Activo(ByVal Data As CActivo)
  Set m_oActivo = Data
End Property
Public Property Get Almacen() As CAlmacen
Set Almacen = m_oAlmacen
End Property
Public Property Set Almacen(ByVal Data As CAlmacen)
  Set m_oAlmacen = Data
End Property
Public Property Get ArtConcepto() As TipoConcepto
    ArtConcepto = m_udtConcepto
End Property

Public Property Let ArtConcepto(ByVal Data As TipoConcepto)
    m_udtConcepto = Data
End Property

Public Property Get ArtAlmacenar() As TipoArtAlmacenable
    ArtAlmacenar = m_udtAlmacen
End Property

Public Property Let ArtAlmacenar(ByVal Data As TipoArtAlmacenable)
    m_udtAlmacen = Data
End Property

Public Property Get ArtRecepcionar() As TipoArtRecepcionable
    ArtRecepcionar = m_udtRecepcion
End Property

Public Property Let ArtRecepcionar(ByVal Data As TipoArtRecepcionable)
    m_udtRecepcion = Data
End Property

Public Property Get Id() As Variant
    Id = mvarId
End Property

Public Property Let Id(ByVal Data As Variant)
    mvarId = Data
End Property
Public Property Get LineaEstado() As Variant
    LineaEstado = mvarLineaEstado
End Property

Public Property Let LineaEstado(ByVal Data As Variant)
    mvarLineaEstado = Data
End Property

Public Property Get IdLineaPed() As Long
    IdLineaPed = mvarIdLineaPed
End Property

Public Property Let IdLineaPed(ByVal Data As Long)
    mvarIdLineaPed = Data
End Property

Public Property Get ArticuloCod() As Variant
    ArticuloCod = mvarArtCod
End Property
Public Property Let ArticuloCod(ByVal v As Variant)
    mvarArtCod = v
End Property
Public Property Get Descr() As Variant
    Descr = mvarDescr
End Property
Public Property Let Descr(ByVal vDescr As Variant)
    mvarDescr = vDescr
End Property
Public Property Let UniCod(ByVal s As String)
    mvarUniCod = s
End Property
Public Property Get UniCod() As String
    UniCod = mvarUniCod
End Property

Public Property Let Precio(ByVal v As Double)
    mvarPrecio = v
End Property
Public Property Get Precio() As Double
    Precio = mvarPrecio
End Property
Public Property Let FechaEntrega(ByVal Dat As Variant)
    mvarFecEntrega = Dat
End Property
Public Property Get FechaEntrega() As Variant
    FechaEntrega = mvarFecEntrega
End Property
Public Property Let FechaAct(ByVal Dat As Variant)
    mvarFecAct = Dat
End Property
Public Property Get FechaAct() As Variant
    FechaAct = mvarFecAct
End Property
Public Property Let PedidoAbierto(ByVal Dat As Variant)
    mvarPedidoAbierto = Dat
End Property
Public Property Get PedidoAbierto() As Variant
    PedidoAbierto = mvarPedidoAbierto
End Property
Public Property Let DestCod(ByVal var As String)
    mvarDestCod = var
End Property
Public Property Get DestCod() As String
    DestCod = mvarDestCod
End Property
Public Property Let CantPedida(ByVal d As Double)
    mvarCantPedida = d
End Property
Public Property Get CantPedida() As Double
    CantPedida = mvarCantPedida
End Property
Public Property Let CantRecepcionada(ByVal d As Variant)
    mvarCantRecepcionada = d
End Property
Public Property Get CantRecepcionada() As Variant
    CantRecepcionada = NullToDbl0(mvarCantRecepcionada)
End Property
Public Property Let CantRecibida(ByVal d As Variant)
    mvarCantRecibida = d
End Property
Public Property Get CantRecibida() As Variant
    CantRecibida = NullToDbl0(mvarCantRecibida)
End Property
Public Property Get CantidadPendienteRecepcionar() As Double
    If Me.isRecepcionCantidad Then
        CantidadPendienteRecepcionar = Me.CantPedida - NullToDbl0(Me.CantRecibida)
    Else
        CantidadPendienteRecepcionar = 0
    End If
End Property

Public Property Get ImportePendienteRecepcionar() As Double
    If Me.isRecepcionCantidad Then
        ImportePendienteRecepcionar = Me.CantidadPendienteRecepcionar * Me.Precio
    Else
        ImportePendienteRecepcionar = Me.ImportePedido - Me.ImporteRecibido
    End If
End Property
Public Property Get ImporteRecibido() As Double
    If Me.isRecepcionCantidad Then
        ImporteRecibido = NullToDbl0(Me.CantRecibida) * Me.Precio
    Else
        '''pendiente de cargar de BBDD o calcularlo
        ImporteRecibido = m_fImporteRecibido
    End If
End Property
Public Property Let ImporteRecibido(ByVal Value As Double)
    If Me.isRecepcionCantidad Then
        m_fImporteRecibido = Me.Precio * NullToDbl0(mvarCantRecibida)
    Else
        m_fImporteRecibido = Value
    End If
End Property
Public Property Let tipoRecepcion(ByVal Value As Integer)
    m_iTipoRecepcion = Value
End Property
Public Property Get tipoRecepcion() As Integer
    tipoRecepcion = m_iTipoRecepcion
End Property

Public Property Get ImportePedido() As Double
    If Me.isRecepcionCantidad Then
        ImportePedido = Me.Precio * NullToDbl0(Me.CantPedida)
    Else
        ImportePedido = m_dImportePedido
    End If
End Property
Public Property Let ImportePedido(Value As Double)
    m_dImportePedido = Value
End Property

Public Property Let ImporteRecepcionado(Value As Double)
    If Me.isRecepcionImporte Then
        m_dImporteRecepcionado = Value
    End If
End Property
Public Property Let SalidaAlmacen(ByVal Value As Boolean)
    m_bSalidaAlmacen = Value
End Property
Public Property Get SalidaAlmacen() As Boolean
    SalidaAlmacen = m_bSalidaAlmacen
End Property

'''<summary>Devuelve el importe total recepcionado de la linea (suma de importes)
Public Property Get ImporteRecepcionado() As Double
    If Me.isRecepcionCantidad Then
        ImporteRecepcionado = Me.CantRecepcionada * Me.Precio
    Else
        ImporteRecepcionado = m_dImporteRecepcionado
    End If
End Property

Public Function isRecepcionCantidad() As Boolean
    If m_iTipoRecepcion = 1 Then
        isRecepcionCantidad = False
    Else
        isRecepcionCantidad = True
    End If
End Function
Public Function isRecepcionImporte() As Boolean
    If m_iTipoRecepcion = 1 Then
        isRecepcionImporte = True
    Else
        isRecepcionImporte = False
    End If
End Function
Public Property Get Centro() As Variant
    Centro = mvarCentro
End Property

Public Property Let Centro(ByVal Data As Variant)
    mvarCentro = Data
End Property
Friend Property Set Conexion(ByVal con As CConexion)
Set mvarConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = mvarConexion
End Property
Public Property Set Recepcion(ByVal con As CRecepcion)
Set mvarRecep = con
End Property

Public Property Get Recepcion() As CRecepcion
Set Recepcion = mvarRecep
End Property


Private Sub Class_Terminate()
    
    Set mvarConexion = Nothing
        
End Sub

''' <summary>
''' Valida un codigo, den o c�digo ERP de activo con los centros de coste de las imputaciones
''' </summary>
''' <param name="lEmpresa">Id de empresa</param>
''' <param name="sUsu">C�digo de usuario para los permisos, si administrador viene en blanco</param>
''' <param name="sCod">C�digo de activo</param>
''' <param name="sDen">Denominaci�n de activo</param>
''' <param name="sCodERP">C�digo ERP de activo</param>
''' <returns>Boolean, true si existe, False si no</returns>
''' <remarks>Llamada desde:frmREcepcion.sdbgPrecios_BeforeRowColChange</remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>
Public Function ValidarActivo(ByVal lEmpresa As Long, Optional ByVal sUsu As String = "", Optional ByVal sCod As String = "", Optional ByVal sDen As String = "", Optional ByVal sCodERP As String = "") As String
    Dim oImp As CImputacion
    '*************************************************************
    If mvarConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CLineaREcepcion.ValidarActivo", "No se ha establecido la conexion"
        Exit Function
    End If
    '*************************************************************
    If sCod = "" And sDen = "" And sCodERP = "" Then
        ValidarActivo = False
        Set m_oActivo = Nothing
        Set m_oActivo = New CActivo
        Set m_oActivo.Conexion = mvarConexion
        Exit Function
    End If
    
    
    If m_oImputaciones.Count > 0 Then
        For Each oImp In m_oImputaciones
            ValidarActivo = m_oActivo.ValidarActivo(lEmpresa, sUsu, sCod, sDen, sCodERP, StrToVbNULL(oImp.CC1), StrToVbNULL(oImp.CC2), StrToVbNULL(oImp.CC3), StrToVbNULL(oImp.CC4))
            If ValidarActivo Then
               Exit Function
            End If
        Next
    Else
        ValidarActivo = m_oActivo.ValidarActivo(lEmpresa, sUsu, sCod, sDen, sCodERP)
        If ValidarActivo Then
           Exit Function
        End If
    End If
    
    ValidarActivo = False
    Set m_oActivo = Nothing
    Set m_oActivo = New CActivo
    Set m_oActivo.Conexion = mvarConexion
End Function


''' <summary>
''' Acumula el comprometido para las imputaciones de la linea de recepci�n
''' </summary>
''' <param name="iModo">Modo de acumular: 1-Anadir, 2-Modificar (restar-sumar), 3-Eliminar</param>
''' <param name="Importe">Importe a acumular</param>
''' <param name="Mon">Moneda del pedido</param>
''' <param name="Cambio">Cambio de pedido</param>
''' <returns>TipoErrorSummit</returns>
''' <remarks>Llamada desde:CRecepcion.IbaseDatos_AnyadirABaseDatos, FinalizarEdicionModificando, ELiminarDeBaseDatos</remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>
''' <revision>LTG 09/01/2012</revision>

Friend Function AcumularComprometido(ByVal iModo As Byte, ByVal Importe As Double, ByVal Mon As String, ByVal Cambio As Double) As TipoErrorSummit
    Dim sConsulta As String
    Dim adoComm As ADODB.Command
    Dim adoParam As ADODB.Parameter
    Dim adoRs As ADODB.Recordset
    Dim oImp As CImputacion
    Dim TESError As TipoErrorSummit
    
    '*************************************************************
    If mvarConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CLineaREcepcion.ValidarActivo", "No se ha establecido la conexion"
        Exit Function
    End If
    '*************************************************************
    On Error GoTo Error
    TESError.NumError = TESnoerror

    If m_oImputaciones.Count > 0 Then
        For Each oImp In m_oImputaciones
            If g_oParametrosSM.Item(oImp.PRES5).ComprAcum = 2 Then
                Select Case iModo
                Case 1 'Anadir
                    If NoHayParametro(mvarId) Or mvarId = 0 Then
                        Set adoComm = New ADODB.Command
                        sConsulta = "SELECT ID FROM LINEAS_RECEP WITH (NOLOCK) WHERE PEDIDO_RECEP=? AND LINEA=?"
                        Set adoParam = adoComm.CreateParameter("PEDIDO_RECEP", adInteger, adParamInput, , mvarRecep.Id)
                        adoComm.Parameters.Append adoParam
                        Set adoParam = adoComm.CreateParameter("LINEA", adInteger, adParamInput, , mvarIdLineaPed)
                        adoComm.Parameters.Append adoParam
                        Set adoComm.ActiveConnection = mvarConexion.ADOCon
                        adoComm.CommandText = sConsulta
                        adoComm.CommandType = adCmdText
                        
                        Set adoRs = adoComm.Execute
                    
                        If adoRs.eof Then
                           TESError.NumError = TESDatoEliminado
                           TESError.Arg1 = 98 ' Recepci�n
                            AcumularComprometido = TESError
                            Exit Function
                        End If
                        mvarId = adoRs("ID").Value
                        adoRs.Close
                        Set adoRs = Nothing
                        Set adoComm = Nothing
                    End If
                
                    oImp.Cambio = 0
                    TESError = oImp.ActualizarImportesPartida(Mon, Cambio, Importe, 2, IIf(g_oParametrosSM.Item(oImp.PRES5).ImpRecep, mvarId, mvarIdLineaPed))
                    If TESError.NumError = TESnoerror And Not IsNull(TESError.Arg1) And g_oParametrosSM.Item(oImp.PRES5).ImpRecep Then
                        Set adoComm = New ADODB.Command
                        sConsulta = "INSERT INTO LINEAS_RECEP_IMPUTACION (LINEA_RECEP,UON1,UON2,UON3,UON4,PRES5_IMP,CAMBIO) VALUES (?,?,?,?,?,?,?)"
                        Set adoParam = adoComm.CreateParameter("LINEA_RECEP", adInteger, adParamInput, , mvarId)
                        adoComm.Parameters.Append adoParam
                        Set adoParam = adoComm.CreateParameter("UON1", adVarChar, adParamInput, gLongitudesDeCodigos.giLongCodUON1, oImp.CC1)
                        adoComm.Parameters.Append adoParam
                        Set adoParam = adoComm.CreateParameter("UON2", adVarChar, adParamInput, gLongitudesDeCodigos.giLongCodUON2, StrToVbNULL(oImp.CC2))
                        adoComm.Parameters.Append adoParam
                        Set adoParam = adoComm.CreateParameter("UON3", adVarChar, adParamInput, gLongitudesDeCodigos.giLongCodUON3, StrToVbNULL(oImp.CC3))
                        adoComm.Parameters.Append adoParam
                        Set adoParam = adoComm.CreateParameter("UON4", adVarChar, adParamInput, gLongitudesDeCodigos.giLongCodUON4, StrToVbNULL(oImp.CC4))
                        adoComm.Parameters.Append adoParam
                        Set adoParam = adoComm.CreateParameter("PRES5_IMP", adInteger, adParamInput, , TESError.Arg1)
                        adoComm.Parameters.Append adoParam
                        Set adoParam = adoComm.CreateParameter("CAMBIO", adDouble, adParamInput, , oImp.Cambio)
                        adoComm.Parameters.Append adoParam
                        Set adoComm.ActiveConnection = mvarConexion.ADOCon
                        adoComm.CommandText = sConsulta
                        adoComm.CommandType = adCmdText
                        
                        adoComm.Execute
                        Set adoComm = Nothing
                        Set adoParam = Nothing
                    End If
                Case 2 'Modificar
                    TESError = oImp.ActualizarImportesPartida(Mon, Cambio, Importe, 2, IIf(g_oParametrosSM.Item(oImp.PRES5).ImpRecep, mvarId, mvarIdLineaPed))
                    
                Case 3 'Eliminar
                    
                    TESError = oImp.ActualizarImportesPartida(Mon, Cambio, CDec(Importe * (-1)), 3, IIf(g_oParametrosSM.Item(oImp.PRES5).ImpRecep, mvarId, mvarIdLineaPed))
                
                End Select
                If TESError.NumError <> TESnoerror Then
                    AcumularComprometido = TESError
                    Exit Function
                End If
                
                
            End If
        Next
    End If
    AcumularComprometido = TESError
    Exit Function
    
Error:
    AcumularComprometido = basErrores.TratarError(mvarConexion.ADOCon.Errors)
    If Not adoRs Is Nothing Then
        adoRs.Close
        Set adoRs = Nothing
    End If
    Set adoComm = Nothing
    Set adoParam = Nothing


End Function
''' <summary>
''' Obtener el valor de m_iNum
''' </summary>
''' <remarks>Llamada desde: frmrecepciones/CargarGridConRecep; Tiempo m�ximo: 0</remarks>
Public Property Get Num() As Integer
   Num = m_iNum
End Property
''' <summary>
''' Establecer el valor de m_iNum
''' </summary>
''' <remarks>Llamada desde: clineasrcepciones/add; Tiempo m�ximo: 0</remarks>
Public Property Let Num(ByVal Data As Integer)
    m_iNum = Data
End Property
''' <summary>
''' Obtener el valor de m_bRecepAutom
''' </summary>
''' <remarks>Llamada desde: frmrecepciones/CargarGridConRecep  frmrecepciones/sdbgPrecios_RowColChange; Tiempo m�ximo: 0</remarks>
Public Property Get RecepAutom() As Boolean
   RecepAutom = m_bRecepAutom
End Property
''' <summary>
''' Establecer el valor de m_bRecepAutom
''' </summary>
''' <remarks>Llamada desde: clineasrcepciones/add; Tiempo m�ximo: 0</remarks>
Public Property Let RecepAutom(ByVal Data As Boolean)
    m_bRecepAutom = Data
End Property


