VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CPrecioItems"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CPrecioItems **********************************
'*             Autor : Javier Arana
'*             Creada : 30/3/99
'****************************************************************


Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum

Private mCol As Collection
Private m_oConexion As CConexion
Private mvarEOF As Boolean

Public Property Get eof() As Boolean
    eof = mvarEOF
End Property
Friend Property Let eof(ByVal b As Boolean)
    mvarEOF = b
End Property

Public Property Get Item(vntIndexKey As Variant) As CPrecioItem
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Friend Property Set Conexion(ByVal con As CConexion)
Set m_oConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = m_oConexion
End Property

Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
     Count = mCol.Count
End If


End Property

'''<summary>A�ade un objeto de tipo CPrecioItem a la colecci�n</summary>
'''<param name="oOfer"></param>
'''<param name="ID"></param>
'''<param name="Dest"></param>
'''<param name="Uni">Unidad</param>
'''<param name="Pag"></param>
'''<param name="Cant">Cantidad item</param>
'''<param name="FecIni"></param>
'''<param name="FecFin"></param>
'''<param name="Hom"></param>
'''<param name="CantMax"></param>
'''<param name="ArtiCod">C�digo del art�culo</param>
'''<param name="Descr">Descripci�n</param>
'''<param name="PrecioApertura"></param>
'''<param name="Presupuesto">Presupuesto</param>
'''<param name="PrecioOferta"></param>
'''<param name="varIndice"></param>
'''<param name="Cerrado"></param>
'''<param name="Precio">Precio</param>
'''<param name="Precio2">Precio2</param>
'''<param name="Precio3">Precio3</param>
'''<param name="Usar"></param>
'''<param name="lGrupo"></param>
'''<param name="vFecAct">Fecha actualizaci�n</param>
'''<param name="vObsPre1">Observaciones precio 1</param>
'''<param name="vObsPre2">Observaciones precio 2</param>
'''<param name="vObsPre3">Observaciones precio 3</param>
'''<param name="iNumeroAdjuns"></param>
'''<param name="sCodGrupo">Codigo grupo</param>
'''<returns>Objeto de tipo CPrecioItem a�adido</returns>
'''<remarks>Llamada desde: CProceso.CargarPreciosOfertaGrupos; Tiempo m�ximo:1sg
'''<revision>LTG 03/10/2011</revision>

Public Function Add(ByVal oOfer As COferta, ByVal Id As Integer, ByVal Dest As Variant, ByVal Uni As String, ByVal Pag As String, ByVal Cant As Variant, _
                    ByVal FecIni As Date, ByVal FecFin As Date, Optional ByVal Hom As Variant, Optional ByVal CantMax As Variant, Optional ByVal ArtiCod As Variant, _
                    Optional ByVal Descr As String, Optional ByVal PrecioApertura As Variant, Optional ByVal Presupuesto As Variant, _
                    Optional ByVal PrecioOferta As Variant, Optional ByVal varIndice As Variant, Optional ByVal Cerrado As Variant, _
                    Optional ByVal Precio As Variant, Optional ByVal Precio2 As Variant, Optional ByVal Precio3 As Variant, Optional ByVal Usar As Variant, _
                    Optional ByVal lGrupo As Long, Optional ByVal vFecAct As Variant, Optional ByVal vObsPre1 As Variant, Optional ByVal vObsPre2 As Variant, _
                    Optional ByVal vObsPre3 As Variant, Optional ByVal iNumeroAdjuns As Integer, Optional ByVal vObsAdjun As Variant, Optional ByVal sCodGrupo As String, _
                    Optional ByVal AnyoImputacion As Integer) As CPrecioItem
                    
    'create a new object
    Dim sCod As String
    Dim objnewmember As CPrecioItem
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New CPrecioItem
   
    Set objnewmember.Oferta = oOfer
    objnewmember.Id = Id
    objnewmember.UniCod = Uni
    objnewmember.PagCod = Pag
    objnewmember.DestCod = Dest
    objnewmember.FechaInicioSuministro = FecIni
    objnewmember.FechaFinSuministro = FecFin
    objnewmember.Cantidad = Cant
    objnewmember.NumAdjuns = iNumeroAdjuns
    
    Set objnewmember.Conexion = m_oConexion
        
    If IsMissing(lGrupo) Then
        objnewmember.Grupo = Null
    Else
        objnewmember.Grupo = lGrupo
    End If
    
    If Not IsMissing(sCodGrupo) And (sCodGrupo <> "") Then
        objnewmember.GrupoCod = sCodGrupo
        
    Else
        objnewmember.GrupoCod = Null
    End If
    
    If IsMissing(vFecAct) Then
        objnewmember.FecAct = Null
    Else
        objnewmember.FecAct = vFecAct
    End If
    
    If IsMissing(Hom) Then
        objnewmember.Hom = Null
    Else
        objnewmember.Hom = Hom
    End If
    
    If IsMissing(CantMax) Then
        objnewmember.CantidadMaxima = Null
    Else
        objnewmember.CantidadMaxima = CantMax
    End If
    
    If IsMissing(PrecioOferta) Then
        objnewmember.PrecioOferta = Null
    Else
        objnewmember.PrecioOferta = PrecioOferta
    End If
    
    If IsMissing(ArtiCod) Then
        objnewmember.ArticuloCod = Null
    Else
        objnewmember.ArticuloCod = ArtiCod
    End If
    
    If IsMissing(Descr) Then
        objnewmember.Descr = Null
    Else
        objnewmember.Descr = Descr
    End If
    If IsMissing(PrecioApertura) Then
        objnewmember.PrecioApertura = Null
    Else
        objnewmember.PrecioApertura = PrecioApertura
    End If
    
    If IsMissing(Presupuesto) Then
        objnewmember.Presupuesto = Null
    Else
        objnewmember.Presupuesto = Presupuesto
    End If
    
    If IsMissing(Cerrado) Then
        objnewmember.Cerrado = False
    Else
        objnewmember.Cerrado = SQLBinaryToBoolean(Cerrado)
    End If
    If IsMissing(vObsPre1) Then
        objnewmember.Precio1Obs = ""
    Else
        objnewmember.Precio1Obs = NullToStr(vObsPre1)
    End If
    If IsMissing(vObsPre2) Then
        objnewmember.Precio2Obs = ""
    Else
        objnewmember.Precio2Obs = NullToStr(vObsPre2)
    End If
    If IsMissing(vObsPre3) Then
        objnewmember.Precio3Obs = ""
    Else
        objnewmember.Precio3Obs = NullToStr(vObsPre3)
    End If
    
    If IsMissing(Precio) Then
        objnewmember.Precio = Null
    Else
        objnewmember.Precio = Precio
    End If
    
    If IsMissing(Precio2) Then
        objnewmember.Precio2 = Null
    Else
        objnewmember.Precio2 = Precio2
    End If
    
    If IsMissing(Precio3) Then
        objnewmember.Precio3 = Null
    Else
        objnewmember.Precio3 = Precio3
    End If
    
    If IsMissing(Usar) Then
        Usar = 0
    End If
    If IsNull(Usar) Then
        objnewmember.Usar = 1
    Else
        objnewmember.Usar = Usar
    End If
    
    If IsMissing(vObsAdjun) Then
        objnewmember.ObsAdjun = Null
    Else
        objnewmember.ObsAdjun = vObsAdjun
    End If
    
    If Not IsMissing(varIndice) And Not IsNull(varIndice) Then
        objnewmember.Indice = varIndice
        mCol.Add objnewmember, CStr(varIndice)
    Else
        mCol.Add objnewmember, CStr(Id)
    End If
    
    If AnyoImputacion > 0 Then objnewmember.AnyoImputacion = AnyoImputacion
    
    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CPrecioItems", "Add", ERR, Erl)
      Exit Function
   End If

End Function

Public Sub Remove(vntIndexKey As Variant)

    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
        mCol.Remove vntIndexKey
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CPrecioItems", "Remove", ERR, Erl)
      Exit Sub
   End If
    
End Sub


Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
     Set NewEnum = mCol.[_NewEnum]
End Property


Private Sub Class_Initialize()
  

    Set mCol = New Collection
End Sub


Private Sub Class_Terminate()
    'Itemroys collection when this class is terminated
    Set mCol = Nothing
    Set m_oConexion = Nothing
End Sub

Public Function BorrarEnModoIndice(ByVal iIndice As Integer)
Dim IndFor As Integer
Dim oItem As CItem


If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    For IndFor = iIndice To mCol.Count - 2
            
        mCol.Remove (CStr(IndFor))
        Set oItem = mCol.Item(CStr(IndFor + 1))
        mCol.Add oItem, CStr(IndFor)
        Set oItem = Nothing
        
    Next IndFor
    
    mCol.Remove CStr(IndFor)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CPrecioItems", "BorrarEnModoIndice", ERR, Erl)
      Exit Function
   End If
    
End Function

Public Sub AddPrecio(ByVal oPrecio As CPrecioItem)
    'create a new object
    Dim sCod As String
    Dim objnewmember As CPrecioItem
    
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = oPrecio
   
    
    If Not IsEmpty(oPrecio.Indice) And Not IsNull(oPrecio.Indice) Then
        objnewmember.Indice = oPrecio.Indice
        mCol.Add objnewmember, CStr(oPrecio.Indice)
    Else
       mCol.Add objnewmember, CStr(oPrecio.Id)
    End If
    
    
    Set objnewmember = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CPrecioItems", "AddPrecio", ERR, Erl)
      Exit Sub
   End If

End Sub
