VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CCarpetasSolicitN1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CCarpetasSolicitN1 **********************************
'*             Autor : Gorka Areitioaurtena
'*             Creada : 13/02/06
'****************************************************************

Option Explicit

Private mCol As Collection
Private mvarConexion As CConexion
Private mvarConexionServer As CConexionDeUseServer
Private mvarEOF As Boolean

Friend Property Set Conexion(ByVal con As CConexion)
    Set mvarConexion = con
End Property
Friend Property Get Conexion() As CConexion
    Set Conexion = mvarConexion
End Property

Friend Property Set ConexionServer(ByVal con As CConexionDeUseServer)
    Set mvarConexionServer = con
End Property
Friend Property Get ConexionServer() As CConexionDeUseServer
    Set ConexionServer = mvarConexionServer
End Property

Public Property Get eof() As Boolean
    eof = mvarEOF
End Property
Friend Property Let eof(ByVal b As Boolean)
    mvarEOF = b
End Property

Public Property Get Item(vntIndexKey As Variant) As CCarpetaSolicitN1
    On Error GoTo NoSeEncuentra:
        Set Item = mCol(vntIndexKey)
    Exit Property
NoSeEncuentra:
        Set Item = Nothing
End Property

Public Property Get Count() As Long
    If mCol Is Nothing Then
        Count = 0
    Else
        Count = mCol.Count
    End If
End Property

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
     Set NewEnum = mCol.[_NewEnum]
End Property

Private Sub Class_Initialize()

        Set mCol = New Collection
End Sub

Private Sub Class_Terminate()
    'destroys collection when this class is terminated
    Set mCol = Nothing
    Set mvarConexion = Nothing
    Set mvarConexionServer = Nothing

End Sub

Public Function DevolverEstructuraCarpetas(ByVal OrdPorDen As Boolean) As CCarpetasSolicitN1

Dim oCarpetasSN1 As CCarpetasSolicitN1
Dim oCSN1 As CCarpetaSolicitN1
Dim oCSN2 As CCarpetaSolicitN2
Dim oCSN3 As CCarpetaSolicitN3
Dim sConsulta As String
Dim rs As New adodb.Recordset
Dim lId As Long
Dim fldCsn1 As adodb.Field
Dim fldCsn2 As adodb.Field
Dim fldCsn3 As adodb.Field
Dim fldId As adodb.Field
Dim fldDen As adodb.Field



If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
Set oCarpetasSN1 = Nothing
Set oCarpetasSN1 = New CCarpetasSolicitN1
Set oCarpetasSN1.Conexion = mvarConexion
Set oCarpetasSN1.ConexionServer = mvarConexionServer

        ' Generamos la coleccion de Carpetas de Solicitudes nivel1
        
        sConsulta = "SELECT * FROM CARPETAS_SOLICIT_N1 WITH (NOLOCK)"
        
        If OrdPorDen Then
            sConsulta = sConsulta & " ORDER BY DEN"
        Else
            sConsulta = sConsulta & " ORDER BY ID"
        End If
        
        rs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
                
        If rs.eof Then
            Set DevolverEstructuraCarpetas = oCarpetasSN1
            Set rs = Nothing
            Exit Function
        End If
        
        Set fldId = rs.Fields("ID")
        Set fldDen = rs.Fields("DEN")
        
        While Not rs.eof
            Set oCSN1 = oCarpetasSN1.Add(fldId.Value, fldDen.Value)
            Set oCSN1.CarpetasSolicitN2 = New CCarpetasSolicitN2
            Set oCSN1.CarpetasSolicitN2.Conexion = mvarConexion
            Set oCSN1.CarpetasSolicitN2.ConexionServer = mvarConexionServer
            rs.MoveNext
            Set oCSN1 = Nothing
        Wend
        
        Set fldId = Nothing
        Set fldDen = Nothing
        
        rs.Close
        Set rs = Nothing
        
        If basParametros.gParametrosGenerales.giNEM <= 1 Then
            Set DevolverEstructuraCarpetas = oCarpetasSN1
            Exit Function
        End If
        
        ' Generamos la coleccion de Carpetas de Solicitudes nivel2
        
        sConsulta = "SELECT * FROM CARPETAS_SOLICIT_N2 WITH (NOLOCK)"
        
        If OrdPorDen Then
            sConsulta = sConsulta & " ORDER BY DEN"
        Else
            sConsulta = sConsulta & " ORDER BY ID"
        End If
        
        rs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
        
        If rs.eof Then
            Set DevolverEstructuraCarpetas = oCarpetasSN1
            Set rs = Nothing
            Exit Function
        End If
        
        Set fldCsn1 = rs.Fields("CSN1")
        Set fldId = rs.Fields("ID")
        Set fldDen = rs.Fields("DEN")
        
        While Not rs.eof
            lId = fldCsn1.Value
            Set oCSN1 = oCarpetasSN1.Item(CStr(lId))
            Set oCSN2 = oCSN1.CarpetasSolicitN2.Add(fldId.Value, fldCsn1.Value, fldDen.Value)
            ' Creo una nueva coleccion para cada uno
            Set oCSN2.CarpetasSolicitN3 = New CCarpetasSolicitN3
            Set oCSN2.CarpetasSolicitN3.Conexion = mvarConexion
            Set oCSN2.CarpetasSolicitN3.ConexionServer = mvarConexionServer
            
            rs.MoveNext
            Set oCSN1 = Nothing
            Set oCSN2 = Nothing
        Wend
        
        Set fldCsn1 = Nothing
        Set fldId = Nothing
        Set fldDen = Nothing

        rs.Close
        Set rs = Nothing
        
        If basParametros.gParametrosGenerales.giNEM <= 2 Then
            Set DevolverEstructuraCarpetas = oCarpetasSN1
            Exit Function
        End If
        
        ' Generamos la coleccion de Carpetas de Solicitudes nivel3
        
        sConsulta = "SELECT * FROM CARPETAS_SOLICIT_N3 WITH (NOLOCK)"
        
        If OrdPorDen Then
            sConsulta = sConsulta & " ORDER BY DEN"
        Else
            sConsulta = sConsulta & " ORDER BY ID"
        End If
        
        rs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
        
        If rs.eof Then
            Set DevolverEstructuraCarpetas = oCarpetasSN1
            Set rs = Nothing
            Exit Function
        End If
        
        Set fldCsn1 = rs.Fields("CSN1")
        Set fldCsn2 = rs.Fields("CSN2")
        Set fldId = rs.Fields("ID")
        Set fldDen = rs.Fields("DEN")
        
        While Not rs.eof
            lId = fldCsn1.Value
            Set oCSN1 = oCarpetasSN1.Item(CStr(lId))
            lId = fldCsn2.Value
            Set oCSN2 = oCSN1.CarpetasSolicitN2.Item(CStr(lId))
            Set oCSN3 = oCSN2.CarpetasSolicitN3.Add(fldId.Value, fldCsn1.Value, fldCsn2.Value, fldDen.Value)
            ' Creo una nueva coleccion para cada uno
            Set oCSN3.CarpetasSolicitN4 = New CCarpetasSolicitN4
            Set oCSN3.CarpetasSolicitN4.Conexion = mvarConexion
            Set oCSN3.CarpetasSolicitN4.ConexionServer = mvarConexionServer
            
            rs.MoveNext
            Set oCSN1 = Nothing
            Set oCSN2 = Nothing
            Set oCSN3 = Nothing
        Wend
        
        
        Set fldCsn1 = Nothing
        Set fldCsn2 = Nothing
        Set fldId = Nothing
        Set fldDen = Nothing
        
        rs.Close
        Set rs = Nothing
        
        If basParametros.gParametrosGenerales.giNEM <= 3 Then
            Set DevolverEstructuraCarpetas = oCarpetasSN1
            Exit Function
        End If
        
        ' Generamos la coleccion de Carpetas de Solicitudes nivel4
        
        sConsulta = "SELECT * FROM CARPETAS_SOLICIT_N4 WITH (NOLOCK)"
        
        If OrdPorDen Then
            sConsulta = sConsulta & " ORDER BY DEN"
        Else
            sConsulta = sConsulta & " ORDER BY ID"
        End If
        
        rs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
            
        If rs.eof Then
            Set DevolverEstructuraCarpetas = oCarpetasSN1
            Set rs = Nothing
            Exit Function
        End If
        
        Set fldCsn1 = rs.Fields("CSN1")
        Set fldCsn2 = rs.Fields("CSN2")
        Set fldCsn3 = rs.Fields("CSN3")
        Set fldId = rs.Fields("ID")
        Set fldDen = rs.Fields("DEN")
        
        
        While Not rs.eof
            lId = fldCsn1.Value
            Set oCSN1 = oCarpetasSN1.Item(CStr(lId))
            lId = fldCsn2.Value
            Set oCSN2 = oCSN1.CarpetasSolicitN2.Item(CStr(lId))
            lId = fldCsn3.Value
            Set oCSN3 = oCSN2.CarpetasSolicitN3.Item(CStr(lId))
            oCSN3.CarpetasSolicitN4.Add fldId.Value, fldCsn1.Value, fldCsn2.Value, fldCsn3.Value, fldDen.Value
            rs.MoveNext
            Set oCSN1 = Nothing
            Set oCSN2 = Nothing
            Set oCSN3 = Nothing
        Wend
                   
        Set fldCsn1 = Nothing
        Set fldCsn2 = Nothing
        Set fldCsn1 = Nothing
        Set fldId = Nothing
        Set fldDen = Nothing
          
                  
        rs.Close
        Set rs = Nothing
 
Set DevolverEstructuraCarpetas = oCarpetasSN1
Set oCarpetasSN1 = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CCarpetasSolicitN1", "DevolverEstructuraCarpetas", ERR, Erl)
      Exit Function
   End If

End Function

Public Sub GenerarEstructuraCarpetas(ByVal OrdPorDen As Boolean)


If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set mCol = Nothing
    Set mCol = New Collection

    Set mCol = Me.DevolverEstructuraCarpetas(OrdPorDen)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CCarpetasSolicitN1", "GenerarEstructuraCarpetas", ERR, Erl)
      Exit Sub
   End If

End Sub

Public Function Add(ByVal Id As Long, ByVal Den As String) As CCarpetaSolicitN1
    'create a new object
    Dim objnewmember As CCarpetaSolicitN1
    Dim lId As Long
    
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New CCarpetaSolicitN1
   
    objnewmember.Id = Id
    objnewmember.Den = Den
    Set objnewmember.Conexion = mvarConexion
        
    lId = Id
    mCol.Add objnewmember, CStr(lId)
    
    Set Add = objnewmember
    Set objnewmember = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CCarpetasSolicitN1", "Add", ERR, Erl)
      Exit Function
   End If

End Function

Public Sub Remove(vntIndexKey As Variant)

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    mCol.Remove vntIndexKey
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CCarpetasSolicitN1", "Remove", ERR, Erl)
      Exit Sub
   End If
End Sub

Public Function DevolverNumeroEstructuraCarpetas(ByVal iNivel As Integer) As Integer

Dim sConsulta As String
Dim rs As New adodb.Recordset


        ' Generamos la coleccion de Carpetas de Solicitudes nivel1
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
        sConsulta = "SELECT COUNT(*) FROM CARPETAS_SOLICIT_N" & iNivel & " WITH (NOLOCK)"
        
        
        rs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
                        
                  
        DevolverNumeroEstructuraCarpetas = rs.Fields(0).Value

        rs.Close
        Set rs = Nothing

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CCarpetasSolicitN1", "DevolverNumeroEstructuraCarpetas", ERR, Erl)
      Exit Function
   End If

End Function


Public Function DevolverCarpetasN1() As adodb.Recordset

Dim sConsulta As String
Dim rs As New adodb.Recordset


        ' Generamos la coleccion de Carpetas de Solicitudes nivel1
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
        sConsulta = "SELECT ID,DEN FROM CARPETAS_SOLICIT_N1 WITH (NOLOCK)"
        
        rs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
                  
        Set DevolverCarpetasN1 = rs
      
        Set rs.ActiveConnection = Nothing
                
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CCarpetasSolicitN1", "DevolverCarpetasN1", ERR, Erl)
      Exit Function
   End If

End Function

