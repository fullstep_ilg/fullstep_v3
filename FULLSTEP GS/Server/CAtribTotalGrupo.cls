VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CAtribTotalGrupo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'**********************CAtributo*******************************
'*                 Autor: Mertxe Martin
'*                 Creada: 07/06/2002
'**************************************************************

Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum


Private m_vCodGrupo As Variant
Private m_vIdGrupo As Variant
Private m_vUsarPrec As Variant
Private m_bFlagAplicar As Boolean  'Indica si se puede aplicar el atributo en la comparativa
Private m_oAtributo As CAtributo
Private m_vIndice As Variant

Private m_oConexion As CConexion

Private m_dblImpParcial As Variant 'Se usa en el recalculo de ofertas portal para los importes parciales por grupo

Public Property Set Atributo(ByVal vData As CAtributo)
    Set m_oAtributo = vData
End Property
Public Property Get Atributo() As CAtributo
    Set Atributo = m_oAtributo
End Property

Friend Property Set Conexion(ByVal vData As CConexion)
    Set m_oConexion = vData
End Property
Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property


Public Property Get CodGrupo() As Variant
    CodGrupo = m_vCodGrupo
End Property
Public Property Let CodGrupo(ByVal vData As Variant)
    m_vCodGrupo = vData
End Property


Public Property Get IdGrupo() As Variant
    IdGrupo = m_vIdGrupo
End Property
Public Property Let IdGrupo(ByVal vData As Variant)
    m_vIdGrupo = vData
End Property


Public Property Get UsarPrec() As Variant
    UsarPrec = m_vUsarPrec
End Property
Public Property Let UsarPrec(ByVal vData As Variant)
    m_vUsarPrec = vData
End Property

Public Property Let FlagAplicar(ByVal vData As Boolean)
    m_bFlagAplicar = vData
End Property
Public Property Get FlagAplicar() As Boolean
    FlagAplicar = m_bFlagAplicar
End Property

Public Property Get ImpParcial() As Variant
    ImpParcial = m_dblImpParcial
End Property
Public Property Let ImpParcial(ByVal vData As Variant)
    m_dblImpParcial = vData
End Property

Public Property Let Indice(ByVal vData As Variant)
    m_vIndice = vData
End Property
Public Property Get Indice() As Variant
    Indice = m_vIndice
End Property

Private Sub Class_Terminate()
    Set m_oConexion = Nothing
End Sub
