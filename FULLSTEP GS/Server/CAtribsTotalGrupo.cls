VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CAtribsTotalGrupo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CAtribsTotalGrupo **********************************
'*             Autor : Mertxe Martin
'*             Creada : 07/06/02
'****************************************************************

Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum

Private mCol As Collection
Private m_oConexion As CConexion
Private mvarEOF As Boolean

Friend Property Set Conexion(ByVal con As CConexion)
Set m_oConexion = con
End Property
Friend Property Get Conexion() As CConexion
Set Conexion = m_oConexion
End Property
Public Property Get Item(vntIndexKey As Variant) As CAtribTotalGrupo
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
     Count = mCol.Count
End If

End Property
Public Property Get eof() As Boolean
    eof = mvarEOF
End Property

Friend Property Let eof(ByVal b As Boolean)
    mvarEOF = b
End Property
Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
    Set NewEnum = mCol.[_NewEnum]
End Property
Public Function Add(ByVal oAtributo As CAtributo, ByVal sCodGrupo As String, Optional ByVal UsarPrec As Variant, Optional ByVal lIdGrupo As Long, Optional ByVal dblImpParc As Double, Optional ByVal bUsarIndiceGrupoAtrib As Boolean = False) As CAtribTotalGrupo
           
    Dim objnewmember As CAtribTotalGrupo
    
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New CAtribTotalGrupo
    Set objnewmember.Conexion = m_oConexion

    Set objnewmember.Atributo = oAtributo
    
    If Not IsMissing(sCodGrupo) Then
        objnewmember.CodGrupo = sCodGrupo
    Else
        objnewmember.CodGrupo = Null
    End If
    
    If Not IsMissing(lIdGrupo) Then
        objnewmember.IdGrupo = lIdGrupo
    Else
        objnewmember.IdGrupo = 0
    End If
    
    If Not IsMissing(UsarPrec) Then
        objnewmember.UsarPrec = UsarPrec
    Else
        objnewmember.UsarPrec = Null
    End If
    
    If Not IsMissing(dblImpParc) Then
        objnewmember.ImpParcial = dblImpParc
    Else
        objnewmember.ImpParcial = Null
    End If

    
    Set objnewmember.Conexion = m_oConexion
    
    If Not bUsarIndiceGrupoAtrib Then
        If Not IsMissing(sCodGrupo) Then
            objnewmember.Indice = sCodGrupo
            mCol.Add objnewmember, sCodGrupo
        End If
    Else
        objnewmember.Indice = sCodGrupo
        mCol.Add objnewmember, sCodGrupo & "$" & CStr(oAtributo.IdAtribProce)
    End If
    
    Set Add = objnewmember
    Set objnewmember = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CAtribsTotalGrupo", "Add", ERR, Erl)
      Exit Function
   End If

End Function


Public Sub Remove(vntIndexKey As Variant)
 
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    mCol.Remove vntIndexKey
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CAtribsTotalGrupo", "Remove", ERR, Erl)
      Exit Sub
   End If
End Sub

Private Sub Class_Initialize()
    Set mCol = New Collection
End Sub

Private Sub Class_Terminate()
    'destroys collection when this class is terminated
    Set mCol = Nothing
    Set m_oConexion = Nothing
End Sub





