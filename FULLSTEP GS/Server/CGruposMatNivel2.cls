VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CGruposMatNivel2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CGruposMatNivel1 **********************************
'*             Autor : Javier Arana
'*             Creada : 18/11/98
'****************************************************************

Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum

Private mCol As Collection
Private mvarConexion As CConexion
Private mvarEOF As Boolean
Public Property Get eof() As Boolean
    eof = mvarEOF
End Property
Friend Property Let eof(ByVal b As Boolean)
    mvarEOF = b
End Property

''' <summary>Carga los grupos de material</summary>
''' <param name="CaracteresInicialesCod">Caracteres iniciales de c�digo</param>
''' <param name="CaracteresInicialesDen">Caracteres iniciales de la descripci�n</param>
''' <param name="CoincidenciaTotal">Coincidencia total de c�digo y denominaci�n</param>
''' <param name="OrdenadosPorDen">ordenar por denominaci�n</param>
''' <param name="UsarIndice">usar �ndice</param>
''' <param name="sGMN1">GMN1</param>
''' <param name="CodFinalizanPor">Indica si los caracteres pasados son los finales del c�digo</param>
''' <param name="DenFinalizanPor">Indica si los caracteres pasados son los finales de la denominaci�n</param>
''' <param name="bCoincidenciaCod">Indica si los caracteres pasados son los finales de la denominaci�n</param>
''' <param name="bCoincidenciaDen">Indica si debe haber coincidencia total por denominaci�n</param>
''' <remarks>Llamada desde: frmESTRMATBuscar
''' Tiempo m�ximo: 1 sec </remarks>
''' <revision>LTG 12/08/2011</revision>

Public Sub CargarTodosLosGruposMat(Optional ByVal CaracteresInicialesCod As String, _
        Optional ByVal CaracteresInicialesDen As String, Optional ByVal CoincidenciaTotal As Boolean, _
        Optional ByVal OrdenadosPorDen As Boolean, Optional ByVal UsarIndice As Boolean, Optional ByVal sGMN1 As String, _
        Optional ByVal CodFinalizanPor As Boolean, Optional ByVal DenFinalizanPor As Boolean, _
        Optional ByVal bCoincidenciaCod As Boolean, Optional ByVal bCoincidenciaDen As Boolean)
    Dim rs As New ADODB.Recordset
    Dim sConsulta As String
    Dim lIndice As Long
    Dim fldGmn1 As ADODB.Field
    Dim fldCod As ADODB.Field
    Dim fldDen As ADODB.Field
    
    '********* Precondicion **************************************
    If mvarConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CDestinos.CargarTodosLosDestinos", "No se ha establecido la conexion"
        Exit Sub
    End If
    '*************************************************************
    sConsulta = "SELECT GMN1,COD,FECACT," & DevolverDenGMN & ",QA_NOTIF_USU,TIPORECEPCION FROM GMN2 WITH (NOLOCK) "
    sConsulta = sConsulta & "WHERE 1=1"
    
    If CaracteresInicialesCod <> "" Or CaracteresInicialesDen <> "" Then
        If Not (CaracteresInicialesCod = "") And Not (CaracteresInicialesDen = "") Then
            If CoincidenciaTotal Then
                sConsulta = sConsulta & " AND COD ='" & DblQuote(CaracteresInicialesCod) & "'"
                sConsulta = sConsulta & " AND " & DevolverDenGMN & "='" & DblQuote(CaracteresInicialesDen) & "'"
            Else
                If bCoincidenciaCod Then
                    sConsulta = sConsulta & " AND COD ='" & DblQuote(CaracteresInicialesCod) & "'"
                    If bCoincidenciaDen Then
                        sConsulta = sConsulta & " AND " & DevolverDenGMN & "='" & DblQuote(CaracteresInicialesDen) & "'"
                    Else
                        If DenFinalizanPor Then
                            sConsulta = sConsulta & " AND " & DevolverDenGMN & " LIKE '%" & DblQuote(CaracteresInicialesDen) & "'"
                        Else
                            sConsulta = sConsulta & " AND " & DevolverDenGMN & " LIKE '" & DblQuote(CaracteresInicialesDen) & "%'"
                        End If
                    End If
                Else
                    If CodFinalizanPor Then
                        sConsulta = sConsulta & " AND COD LIKE '%" & DblQuote(CaracteresInicialesCod) & "'"
                        If bCoincidenciaDen Then
                            sConsulta = sConsulta & " AND " & DevolverDenGMN & "='" & DblQuote(CaracteresInicialesDen) & "'"
                        Else
                            If DenFinalizanPor Then
                                sConsulta = sConsulta & " AND " & DevolverDenGMN & " LIKE '%" & DblQuote(CaracteresInicialesDen) & "'"
                            Else
                               sConsulta = sConsulta & " AND " & DevolverDenGMN & " LIKE '" & DblQuote(CaracteresInicialesDen) & "%'"
                            End If
                        End If
                    Else
                        sConsulta = sConsulta & " AND COD LIKE '" & DblQuote(CaracteresInicialesCod) & "%'"
                        If bCoincidenciaDen Then
                            sConsulta = sConsulta & " AND " & DevolverDenGMN & "='" & DblQuote(CaracteresInicialesDen) & "'"
                        Else
                            If DenFinalizanPor Then
                                sConsulta = sConsulta & " AND " & DevolverDenGMN & " LIKE '%" & DblQuote(CaracteresInicialesDen) & "'"
                            Else
                                sConsulta = sConsulta & " AND " & DevolverDenGMN & " LIKE '" & DblQuote(CaracteresInicialesDen) & "%'"
                            End If
                        End If
                    End If
                End If
            End If
        Else
            If CaracteresInicialesCod <> "" Then
                If CoincidenciaTotal Then
                    sConsulta = sConsulta & "WHERE COD ='" & DblQuote(CaracteresInicialesCod) & "'"
                Else
                    If bCoincidenciaCod Then
                        sConsulta = sConsulta & " AND COD ='" & DblQuote(CaracteresInicialesCod) & "'"
                    Else
                        If CodFinalizanPor Then
                            sConsulta = sConsulta & " AND COD LIKE '%" & DblQuote(CaracteresInicialesCod) & "'"
                        Else
                            sConsulta = sConsulta & " AND COD LIKE '" & DblQuote(CaracteresInicialesCod) & "%'"
                        End If
                    End If
                End If
            Else
                If CoincidenciaTotal Then
                    sConsulta = sConsulta & "WHERE " & DevolverDenGMN & "='" & DblQuote(CaracteresInicialesDen) & "'"
                Else
                    If bCoincidenciaDen Then
                        sConsulta = sConsulta & " AND " & DevolverDenGMN & "='" & DblQuote(CaracteresInicialesDen) & "'"
                    Else
                        If DenFinalizanPor Then
                            sConsulta = sConsulta & " AND " & DevolverDenGMN & " LIKE '%" & DblQuote(CaracteresInicialesDen) & "'"
                        Else
                            sConsulta = sConsulta & " AND " & DevolverDenGMN & " LIKE '" & DblQuote(CaracteresInicialesDen) & "%'"
                        End If
                    End If
                End If
            End If
        End If
    End If
    
    If sGMN1 <> vbNullString Then
        sConsulta = sConsulta & " AND GMN1='" & DblQuote(sGMN1) & "'"
    End If
    
    If OrdenadosPorDen Then
        sConsulta = sConsulta & " ORDER BY " & DevolverDenGMN & ",COD"
    Else
        sConsulta = sConsulta & " ORDER BY COD," & DevolverDenGMN
    End If
          
    rs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockOptimistic
          
    Set mCol = Nothing
    Set mCol = New Collection
    
    If rs.eof Then
        mvarEOF = True
    Else
        Set fldGmn1 = rs.Fields("GMN1")
        Set fldCod = rs.Fields("COD")
        Set fldDen = rs.Fields(DevolverDenGMN)
            
        If UsarIndice Then
            lIndice = 0
            
            While Not rs.eof
                ' Dejamos en manos del programador cuando asignar la conexion al obejto usuario
                Me.Add fldGmn1.Value, "", fldCod.Value, fldDen.Value, lIndice
                rs.MoveNext
                lIndice = lIndice + 1
            Wend
        Else
            While Not rs.eof
                ' Dejamos en manos del programador cuando asignar la conexion al obejto usuario
                Me.Add fldGmn1.Value, "", fldCod.Value, fldDen.Value
                rs.MoveNext
            Wend
        End If
        
        Set fldGmn1 = Nothing
        Set fldCod = Nothing
        Set fldDen = Nothing
    End If
    
    rs.Close
    Set rs = Nothing
End Sub

Public Function Add(ByVal GMN1Cod As String, ByVal GMN1Den As String, ByVal Cod As String, ByVal Den As String, Optional ByVal varIndice As Variant) As CGrupoMatNivel2
    'create a new object
    Dim objnewmember As CGrupoMatNivel2
    Dim sCod As String
    
    Set objnewmember = New CGrupoMatNivel2
   
    objnewmember.GMN1Cod = GMN1Cod
    objnewmember.GMN1Den = GMN1Den
    objnewmember.Cod = Cod
    objnewmember.Den = Den
    Set objnewmember.Conexion = mvarConexion
    
    If Not IsMissing(varIndice) And Not IsNull(varIndice) Then
        objnewmember.Indice = varIndice
        mCol.Add objnewmember, CStr(varIndice)
    Else
       sCod = GMN1Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(GMN1Cod))
       sCod = sCod & Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(Cod))
       mCol.Add objnewmember, sCod
    End If
    
    Set Add = objnewmember
    Set objnewmember = Nothing

End Function

Public Property Get Item(vntIndexKey As Variant) As CGrupoMatNivel2
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Friend Property Set Conexion(ByVal con As CConexion)
Set mvarConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = mvarConexion
End Property

Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
     Count = mCol.Count
End If


End Property


Public Sub Remove(vntIndexKey As Variant)
    mCol.Remove vntIndexKey
End Sub


Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
     Set NewEnum = mCol.[_NewEnum]
End Property


Private Sub Class_Initialize()
   

    Set mCol = New Collection
End Sub


Private Sub Class_Terminate()
    'destroys collection when this class is terminated
    Set mCol = Nothing
    Set mvarConexion = Nothing
End Sub

Public Function DevolverLosCodigos() As TipoDatosCombo
   
   Dim Codigos As TipoDatosCombo
   Dim iCont As Integer
   ReDim Codigos.Cod(mCol.Count)
   ReDim Codigos.Den(mCol.Count)
   
    For iCont = 0 To mCol.Count - 1
        
        Codigos.Cod(iCont) = mCol(iCont + 1).Cod
        Codigos.Den(iCont) = mCol(iCont + 1).Den
    
    Next

   DevolverLosCodigos = Codigos
   
End Function

''' <summary>Carga las denominaciones de los materiales existentes en la colecci�n</summary>
''' <param name="sIdi">Idioma de la denominaci�n</param>
''' <remarks>Llamada desde: frmEST</remarks>
''' <revision>LTG 19/04/2013</revision>

Public Sub CargarDenGMN2s(ByVal sIdi As String)
    Dim sSQL As String
    Dim adoRs As ADODB.Recordset
    Dim oCom As ADODB.Command
    Dim oParam As ADODB.Parameter
    Dim oGMN2 As CGrupoMatNivel2
    Dim sCod As String
        
    sSQL = " SELECT GMN1,COD,DEN_" & sIdi & " FROM GMN2 WITH (NOLOCK) WHERE "
    For Each oGMN2 In Me
        sSQL = sSQL & "(GMN1=? AND COD=?) OR "
    Next
    Set oGMN2 = Nothing
    sSQL = Left(sSQL, Len(sSQL) - 4)
    
    Set oCom = New ADODB.Command
    With oCom
        Set .ActiveConnection = mvarConexion.ADOCon
    
        For Each oGMN2 In Me
            Set oParam = .CreateParameter(, adVarChar, adParamInput, 50, oGMN2.GMN1Cod)
            .Parameters.Append oParam
            Set oParam = .CreateParameter(, adVarChar, adParamInput, 50, oGMN2.Cod)
            .Parameters.Append oParam
        Next
        Set oGMN2 = Nothing
    
        .CommandType = adCmdText
        .CommandText = sSQL
        .Prepared = True
        Set adoRs = .Execute
    End With
    Set oCom = Nothing
    Set oParam = Nothing
    
    If Not adoRs.eof Then
        adoRs.MoveFirst
        While Not adoRs.eof
            sCod = adoRs("GMN1") & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(adoRs("GMN1")))
            sCod = sCod & adoRs("COD") & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(adoRs("COD")))
            
            If Not Me.Item(sCod) Is Nothing Then Me.Item(sCod).Den = adoRs("DEN_" & sIdi)
            
            adoRs.MoveNext
        Wend
    End If
    
    Set adoRs = Nothing
End Sub

''' <summary>Devuelve una cadena de caracteres con los c�digos de los materiales de la colecci�n</summary>
''' <remarks>Llamada desde: CConfVistaVisor.AnyadirABaseDatos</remarks>
''' <revision>LTG 19/04/2013</revision>

Public Function GMNCodToStr() As String
    Dim oGMN2 As CGrupoMatNivel2
    Dim sGMN As String
    
    For Each oGMN2 In Me
        sGMN = sGMN & oGMN2.GMNCodToStr
    Next
    Set oGMN2 = Nothing
    
    GMNCodToStr = Left(sGMN, Len(sGMN) - 1)
End Function

