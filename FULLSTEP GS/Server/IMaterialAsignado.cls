VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "IMaterialAsignado"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
Public Function DevolverGruposMN1VisiblesDesde(ByVal iNumMaximo As Integer, Optional ByVal CaracteresInicialesCod As String, Optional ByVal CaracteresInicialesDen As String, Optional ByVal CoincidenciaTotal As Boolean, Optional ByVal OrdenadosPorDen As Boolean, Optional ByVal UsarIndice As Boolean = False, Optional ByVal bRUsuAper As Boolean, Optional ByVal bRCompResp As Boolean, Optional ByVal sCodUsu As String, Optional ByVal bInvitado As Boolean, Optional ByVal Anyo As Integer) As CGruposMatNivel1

End Function
Public Function DevolverGruposMN1Visibles(Optional ByVal CaracteresInicialesCod As String, Optional ByVal CaracteresInicialesDen As String, Optional ByVal CoincidenciaTotal As Boolean, Optional ByVal OrdenadosPorDen As Boolean, Optional ByVal UsarIndice As Boolean = False) As CGruposMatNivel1

End Function
Public Function DevolverGruposMN2VisiblesDesde(ByVal iNumMaximo As Integer, ByVal CodGmn1Padre As String, Optional ByVal CaracteresInicialesCod As String, Optional ByVal CaracteresInicialesDen As String, Optional ByVal CoincidenciaTotal As Boolean, Optional ByVal OrdenadosPorDen As Boolean, Optional ByVal UsarIndice As Boolean = False, Optional ByVal bRUsuAper As Boolean, Optional ByVal bRCompResp As Boolean, Optional ByVal sCodUsu As String, Optional ByVal bInvitado As Boolean) As CGruposMatNivel2

End Function
Public Function DevolverGruposMN2Visibles(ByVal CodGmn1Padre As String, Optional ByVal CaracteresInicialesCod As String, Optional ByVal CaracteresInicialesDen As String, Optional ByVal CoincidenciaTotal As Boolean, Optional ByVal OrdenadosPorDen As Boolean, Optional ByVal UsarIndice As Boolean = False, Optional ByVal bRUsuAper As Boolean, Optional ByVal bRCompResp As Boolean, Optional ByVal sCodUsu As String, Optional ByVal bInvitado As Boolean) As CGruposMatNivel2

End Function
Public Function DevolverGruposMN3VisiblesDesde(ByVal iNumMaximo As Integer, ByVal CodGmn1Padre As String, ByVal CodGmn2Padre As String, Optional ByVal CaracteresInicialesCod As String, Optional ByVal CaracteresInicialesDen As String, Optional ByVal CoincidenciaTotal As Boolean, Optional ByVal OrdenadosPorDen As Boolean, Optional ByVal UsarIndice As Boolean = False, Optional ByVal bRUsuAper As Boolean, Optional ByVal bRCompResp As Boolean, Optional ByVal sCodUsu As String, Optional ByVal bInvitado As Boolean) As CGruposMatNivel3

End Function
Public Function DevolverGruposMN3Visibles(ByVal CodGmn1Padre As String, ByVal CodGmn2Padre As String, Optional ByVal CaracteresInicialesCod As String, Optional ByVal CaracteresInicialesDen As String, Optional ByVal CoincidenciaTotal As Boolean, Optional ByVal OrdenadosPorDen As Boolean, Optional ByVal UsarIndice As Boolean = False, Optional ByVal bRUsuAper As Boolean, Optional ByVal bRCompResp As Boolean, Optional ByVal sCodUsu As String, Optional ByVal bInvitado As Boolean) As CGruposMatNivel3

End Function
Public Function DevolverGruposMN4VisiblesDesde(ByVal iNumMaximo As Integer, ByVal CodGmn1Padre As String, ByVal CodGmn2Padre As String, ByVal CodGmn3Padre As String, Optional ByVal CaracteresInicialesCod As String, Optional ByVal CaracteresInicialesDen As String, Optional ByVal CoincidenciaTotal As Boolean, Optional ByVal OrdenadosPorDen As Boolean, Optional ByVal UsarIndice As Boolean = False, Optional ByVal bRUsuAper As Boolean, Optional ByVal bRCompResp As Boolean, Optional ByVal sCodUsu As String, Optional ByVal bInvitado As Boolean) As CGruposMatNivel4

End Function
Public Function DevolverGruposMN4Visibles(ByVal CodGmn1Padre As String, ByVal CodGmn2Padre As String, ByVal CodGmn3Padre As String, Optional ByVal CaracteresInicialesCod As String, Optional ByVal CaracteresInicialesDen As String, Optional ByVal CoincidenciaTotal As Boolean, Optional ByVal OrdenadosPorDen As Boolean, Optional ByVal UsarIndice As Boolean = False, Optional ByVal bRUsuAper As Boolean, Optional ByVal bRCompResp As Boolean, Optional ByVal sCodUsu As String, Optional ByVal bInvitado As Boolean) As CGruposMatNivel4

End Function
Public Function DevolverGruposMN1Asignados(Optional ByVal CaracteresInicialesCod As String, Optional ByVal CaracteresInicialesDen As String, Optional ByVal CoincidenciaTotal As Boolean, Optional ByVal OrdenadosPorDen As Boolean, Optional ByVal UsarIndice As Boolean = False, Optional ByVal bCargarTodosNiveles As Boolean, Optional ByVal bRUsuAper As Boolean, Optional ByVal bRCompResp As Boolean, Optional ByVal sCodUsu As String, Optional ByVal CodFinalizanPor As Boolean, Optional ByVal DenFinalizanPor As Boolean, Optional ByVal bCoincidenciaCod As Boolean, Optional ByVal bCoincidenciaDen As Boolean) As CGruposMatNivel1

End Function
Public Function DevolverGruposMN2Asignados(Optional ByVal CaracteresInicialesCod As String, Optional ByVal CaracteresInicialesDen As String, Optional ByVal CoincidenciaTotal As Boolean, Optional ByVal OrdenadosPorDen As Boolean, Optional ByVal UsarIndice As Boolean = False, Optional ByVal bCargarTodosNiveles As Boolean, Optional ByVal bRUsuAper As Boolean, Optional ByVal bRCompResp As Boolean, Optional ByVal sCodUsu As String, Optional ByVal sGMN1 As String, Optional ByVal CodFinalizanPor As Boolean, Optional ByVal DenFinalizanPor As Boolean, Optional ByVal bCoincidenciaCod As Boolean, Optional ByVal bCoincidenciaDen As Boolean) As CGruposMatNivel2

End Function
Public Function DevolverGruposMN3Asignados(Optional ByVal CaracteresInicialesCod As String, Optional ByVal CaracteresInicialesDen As String, Optional ByVal CoincidenciaTotal As Boolean, Optional ByVal OrdenadosPorDen As Boolean, Optional ByVal UsarIndice As Boolean = False, Optional ByVal bCargarTodosNiveles As Boolean, Optional ByVal bRUsuAper As Boolean, Optional ByVal bRCompResp As Boolean, Optional ByVal sCodUsu As String, Optional ByVal sGMN1 As String, Optional ByVal sGMN2 As String, Optional ByVal CodFinalizanPor As Boolean, Optional ByVal DenFinalizanPor As Boolean, Optional ByVal bCoincidenciaCod As Boolean, Optional ByVal bCoincidenciaDen As Boolean) As CGruposMatNivel3

End Function
Public Function DevolverGruposMN4Asignados(Optional ByVal CaracteresInicialesCod As String, Optional ByVal CaracteresInicialesDen As String, Optional ByVal CoincidenciaTotal As Boolean, Optional ByVal OrdenadosPorDen As Boolean, Optional ByVal UsarIndice As Boolean = False, Optional ByVal bCargarTodosNiveles As Boolean, Optional ByVal bRUsuAper As Boolean, Optional ByVal bRCompResp As Boolean, Optional ByVal sCodUsu As String, Optional ByVal IdPlantilla As Variant, Optional ByVal sGMN1 As String, Optional ByVal sGMN2 As String, Optional ByVal sGMN3 As String, Optional ByVal oAtributos As CAtributos, Optional ByVal CodFinalizanPor As Boolean, Optional ByVal DenFinalizanPor As Boolean, Optional ByVal bCoincidenciaCod As Boolean, Optional ByVal bCoincidenciaDen As Boolean) As CGruposMatNivel4

End Function
Public Function DevolverGruposMN1AsignadosDesde(ByVal iNumMaximo As Integer, Optional ByVal CaracteresInicialesCod As String, Optional ByVal CaracteresInicialesDen As String, Optional ByVal OrdenadosPorDen As Boolean, Optional ByVal UsarIndice As Boolean = False) As CGruposMatNivel1

End Function
Public Function DevolverGruposMN2AsignadosDesde(ByVal iNumMaximo As Integer, Optional ByVal CaracteresInicialesCod As String, Optional ByVal CaracteresInicialesDen As String, Optional ByVal OrdenadosPorDen As Boolean, Optional ByVal UsarIndice As Boolean = False) As CGruposMatNivel2

End Function
Public Function DevolverGruposMN3AsignadosDesde(ByVal iNumMaximo As Integer, Optional ByVal CaracteresInicialesCod As String, Optional ByVal CaracteresInicialesDen As String, Optional ByVal OrdenadosPorDen As Boolean, Optional ByVal UsarIndice As Boolean = False) As CGruposMatNivel3

End Function
Public Function DevolverGruposMN4AsignadosDesde(ByVal iNumMaximo As Integer, Optional ByVal CaracteresInicialesCod As String, Optional ByVal CaracteresInicialesDen As String, Optional ByVal OrdenadosPorDen As Boolean, Optional ByVal UsarIndice As Boolean = False) As CGruposMatNivel4

End Function

Public Property Set GruposMN1(ByVal oGrups As CGruposMatNivel1)

End Property
Public Property Set GruposMN2(ByVal oGrups As CGruposMatNivel2)

End Property
Public Property Set GruposMN3(ByVal oGrups As CGruposMatNivel3)

End Property
Public Property Set GruposMN4(ByVal oGrups As CGruposMatNivel4)

End Property

Public Property Get GruposMN4() As CGruposMatNivel4

End Property

Public Property Get GruposMN3() As CGruposMatNivel3

End Property

Public Property Get GruposMN2() As CGruposMatNivel2

End Property

Public Property Get GruposMN1() As CGruposMatNivel1

End Property
Public Property Get Articulos() As CArticulos

End Property
Public Property Set Articulos(ByVal oArtis As CArticulos)

End Property

Public Function AsignarMaterial() As TipoErrorSummit

End Function

Public Function DesAsignarMaterial() As TipoErrorSummit

End Function
Public Function DevolverEstructuraMat(Optional ByVal IdPlantilla As Variant) As CGruposMatNivel1

End Function
Public Function DevolverArticulos(Optional ByVal CaracteresInicialesCod As String, Optional ByVal CaracteresInicialesDen As String, Optional ByVal CoincidenciaTotal As Boolean, Optional ByVal OrdenadosPorDen As Boolean, Optional ByVal UsarIndice As Boolean, Optional ByVal ArticuloGenerico As Boolean, Optional ByVal vConcepto As Variant, Optional ByVal vAlmacen As Variant, Optional ByVal vRecep As Variant, Optional ByVal oAtributos As CAtributos, Optional ByVal sGMN1 As String, Optional ByVal sGMN2 As String, Optional ByVal sGMN3 As String, Optional ByVal sGMN4 As String, Optional ByVal CodFinalizanPor As Boolean, Optional ByVal DenFinalizanPor As Boolean, Optional ByVal bCoincidenciaCod As Boolean, Optional ByVal bCoincidenciaDen As Boolean, Optional ByVal bSoloCentrales As Boolean, _
            Optional ByVal sUON1 As String, Optional ByVal sUon2 As String, Optional ByVal sUon3 As String, Optional ByVal bRUsuUon As Boolean, Optional ByVal bRPerfUON As Boolean, Optional ByVal lIdPerf As Long, Optional oUons As CUnidadesOrganizativas, Optional ByVal bIncluirAscencientes As Boolean, Optional ByVal bMostrarUons As Boolean, Optional ByVal bIncluirDescendientes As Boolean, Optional ByVal bMostrarArtCentrales As Boolean = True) As CArticulos
End Function

Public Function ExistenValoresDeAtributos(Optional ByVal sGMN1 As String, Optional ByVal sGMN2 As String, Optional ByVal sGMN3 As String, Optional ByVal sGMN4 As String, Optional ByVal Articulo As String) As Boolean

End Function

