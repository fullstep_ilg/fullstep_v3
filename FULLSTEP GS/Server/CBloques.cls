VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CBloques"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum

Private m_Col As Collection
Private m_oConexion As CConexion
Private m_bEOF As Boolean

Public Property Get eof() As Boolean
    eof = m_bEOF
End Property
Friend Property Let eof(ByVal b As Boolean)
    m_bEOF = b
End Property

Public Property Get Item(vntIndexKey As Variant) As CBloque
On Error GoTo NoSeEncuentra:

    Set Item = m_Col(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Friend Property Set Conexion(ByVal con As CConexion)
Set m_oConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = m_oConexion
End Property

Public Property Get Count() As Long
    If m_Col Is Nothing Then
        Count = 0
    Else
         Count = m_Col.Count
    End If
End Property

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
     Set NewEnum = m_Col.[_NewEnum]
End Property

Public Function Add(ByVal lId As Long, ByVal oDenominaciones As CMultiidiomas, ByVal iTipo As TipoBloque, ByVal Top As Single, ByVal Left As Single, ByVal Height As Single, ByVal Width As Single, Optional ByVal lWorkFlow As Long, Optional ByVal iEstado As Integer, Optional ByVal dtFecAct As Variant, Optional ByVal vIndice As Variant) As CBloque
    
    'create a new object
    Dim sCod As String
    Dim objnewmember As CBloque
    
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New CBloque
    With objnewmember
        Set .Conexion = m_oConexion
    
        .Id = lId
        Set .Denominaciones = oDenominaciones
        .Workflow = lWorkFlow
        .Tipo = iTipo
        .Top = Top
        .Left = Left
        .Height = Height
        .Width = Width
        .Estado = iEstado
        .FecAct = dtFecAct
        
    
        If Not IsMissing(vIndice) And Not IsNull(vIndice) Then
            .Indice = vIndice
            m_Col.Add objnewmember, CStr(vIndice)
        Else
            m_Col.Add objnewmember, CStr(lId)
        End If
    
    End With
    
    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CBloques", "Add", ERR, Erl)
      Exit Function
   End If

End Function

Public Sub AddBloque(ByVal oBloque As CBloque, Optional ByVal vIndice As Variant)

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not IsMissing(vIndice) And Not IsNull(vIndice) Then
        oBloque.Indice = vIndice
        m_Col.Add oBloque, CStr(vIndice)
    Else
        m_Col.Add oBloque, CStr(oBloque.Id)
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CBloques", "AddBloque", ERR, Erl)
      Exit Sub
   End If
End Sub

Public Sub Remove(vntIndexKey As Variant)

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_Col.Remove vntIndexKey
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CBloques", "Remove", ERR, Erl)
      Exit Sub
   End If

End Sub


Private Sub Class_Initialize()
    Set m_Col = New Collection
End Sub


Private Sub Class_Terminate()
    'Proceroys collection when this class is terminated
    On Error Resume Next
    Set m_Col = Nothing
    Set m_oConexion = Nothing
    
End Sub

Public Function DevolverBloques(ByVal sIdi As String, Optional ByVal lIdWF As Long = 0) As ADODB.Recordset
Dim oadorecordset As ADODB.Recordset
Dim sql As String

    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sql = "SELECT ID, '" & DblQuote(sIdi) & "' AS IDI, ISNULL(DEN,'') AS DEN, WORKFLOW, TIPO, [TOP], [LEFT], HEIGHT, WIDTH, FECACT, INT FROM PM_BLOQUE B WITH (NOLOCK) " & _
        "LEFT JOIN PM_BLOQUE_DEN BD WITH (NOLOCK) ON B.ID = BD.BLOQUE AND IDI = '" & DblQuote(sIdi) & "' "

    If lIdWF > 0 Then
        sql = sql & "WHERE WORKFLOW = " & lIdWF
    End If
        
    Set oadorecordset = New ADODB.Recordset
    oadorecordset.CursorLocation = adUseClient
    oadorecordset.Open sql, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    oadorecordset.ActiveConnection = Nothing
        
    Set DevolverBloques = oadorecordset
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CBloques", "DevolverBloques", ERR, Erl)
      Exit Function
   End If
    
End Function

Public Sub CargarBloques(ByVal sIdi As String, Optional ByVal lIdWF As Long = 0)
    Dim oRS As ADODB.Recordset
    Dim oDenominaciones As CMultiidiomas
    
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oRS = DevolverBloques(sIdi, lIdWF)
    While Not oRS.eof
        Set oDenominaciones = New CMultiidiomas
        Set oDenominaciones.Conexion = m_oConexion
        oDenominaciones.Add oRS("IDI").Value, oRS("DEN").Value
        Add NullToDbl0(oRS("ID").Value), oDenominaciones, NullToDbl0(oRS("TIPO").Value), NullToDbl0(oRS("TOP").Value), NullToDbl0(oRS("LEFT").Value), NullToDbl0(oRS("HEIGHT").Value), NullToDbl0(oRS("WIDTH").Value), NullToDbl0(oRS("WORKFLOW").Value), , oRS("FECACT").Value
        oRS.MoveNext
    Wend
    
    Set oDenominaciones = Nothing
    oRS.Close
    Set oRS = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CBloques", "CargarBloques", ERR, Erl)
      Exit Sub
   End If
End Sub

Public Sub CargarBloquesInstancia(ByVal sIdi As String, ByVal lInstancia As Long, ByVal lWorkFlow As Long)
    Dim sql As String
    Dim oRS As ADODB.Recordset
    Dim oDenominaciones As CMultiidiomas
    
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sql = "SELECT DISTINCT PM_COPIA_BLOQUE.ID, '" & DblQuote(sIdi) & "' AS IDI, ISNULL(DEN,'') AS DEN, TIPO, [TOP], [LEFT], HEIGHT, WIDTH, FECACT, INSTANCIA_BLOQUE.ESTADO " & _
            "FROM PM_COPIA_BLOQUE WITH (NOLOCK) " & _
            "LEFT JOIN PM_COPIA_BLOQUE_DEN WITH (NOLOCK) ON PM_COPIA_BLOQUE_DEN.BLOQUE=PM_COPIA_BLOQUE.ID AND PM_COPIA_BLOQUE_DEN.IDI='" & DblQuote(sIdi) & "'" & _
            "LEFT JOIN INSTANCIA_BLOQUE WITH (NOLOCK) ON  INSTANCIA_BLOQUE.ID=(SELECT MAX(ID) FROM INSTANCIA_BLOQUE WHERE INSTANCIA=" & lInstancia & " AND BLOQUE=PM_COPIA_BLOQUE.ID) " & _
            "LEFT JOIN INSTANCIA_CAMINO WITH (NOLOCK) ON INSTANCIA_BLOQUE.ID=INSTANCIA_CAMINO.INSTANCIA_BLOQUE_DESTINO " & _
            "AND FECHA=(SELECT MAX(FECHA) FROM INSTANCIA_CAMINO WHERE INSTANCIA_BLOQUE_DESTINO=INSTANCIA_BLOQUE.ID) " & _
            "WHERE PM_COPIA_BLOQUE.WORKFLOW=" & lWorkFlow & " "
    
    Set oRS = New ADODB.Recordset
    oRS.CursorLocation = adUseClient
    oRS.Open sql, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    oRS.ActiveConnection = Nothing
    
    While Not oRS.eof
        Set oDenominaciones = New CMultiidiomas
        Set oDenominaciones.Conexion = m_oConexion
        oDenominaciones.Add oRS("IDI").Value, oRS("DEN").Value
        Add NullToDbl0(oRS("ID").Value), oDenominaciones, NullToDbl0(oRS("TIPO").Value), NullToDbl0(oRS("TOP").Value), NullToDbl0(oRS("LEFT").Value), NullToDbl0(oRS("HEIGHT").Value), NullToDbl0(oRS("WIDTH").Value), , NullToDbl0(oRS("ESTADO").Value), oRS("FECACT").Value
        oRS.MoveNext
    Wend
    
    Set oDenominaciones = Nothing
    oRS.Close
    Set oRS = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CBloques", "CargarBloquesInstancia", ERR, Erl)
      Exit Sub
   End If
End Sub

''' <summary>
''' Devuelve las etapas que puede ver un observador cuando la visibilidad es parcial
''' </summary>
''' <param name="lRol">id del rol</param>
''' <param name="sIdi">idioma</param>
''' <param name="lIdWF">id del workflow</param>
''' <returns>Las etapas que puede ver un observador cuando la visibilidad es parcial</returns>
''' <remarks>Llamada desde: frmFlujosEtapasObservador --> CargarEtapas; Tiempo m�ximo:0,5</remarks>
Public Function DevolverBloquesObservador(ByVal lRol As Long, ByVal sIdi As String, Optional ByVal lIdWF As Long = 0) As ADODB.Recordset
Dim rs As ADODB.Recordset
Dim sConsulta As String


    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sConsulta = "SELECT CASE WHEN O.BLOQUE = B.ID THEN 1 ELSE 0 END SELEC, ID, ISNULL(DEN,'') AS DEN"
    sConsulta = sConsulta & " FROM PM_BLOQUE B WITH (NOLOCK)"
    sConsulta = sConsulta & " LEFT JOIN PM_BLOQUE_DEN BD WITH (NOLOCK) ON B.ID = BD.BLOQUE AND IDI = '" & DblQuote(sIdi) & "'"
    sConsulta = sConsulta & " LEFT JOIN PM_ROL_OBS_ETAPAS O WITH (NOLOCK)  ON O.BLOQUE = B.ID AND O.ROL = " & lRol
    
    If lIdWF > 0 Then
        sConsulta = sConsulta & " WHERE WORKFLOW = " & lIdWF
    End If
        
    Set rs = New ADODB.Recordset
    rs.CursorLocation = adUseClient
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    rs.ActiveConnection = Nothing
        
    Set DevolverBloquesObservador = rs
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CBloques", "DevolverBloquesObservador", ERR, Erl)
      Exit Function
   End If
    
End Function

Public Function AsignarEtapasObservador(ByVal lIdRol As Long, ByVal lIDWfl As Long, Optional ByVal lIdBloque As Long = 0, Optional bInicializar As Boolean = False) As TipoErrorSummit
Dim sConsulta As String
Dim TESError As TipoErrorSummit
Dim bTransaccionEnCurso As Boolean
    

TESError.NumError = TESnoerror

If m_oConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CBloques.AsignarEtapasObservador", "No se ha establecido la conexion"
End If
' *****************************************

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
    
    m_oConexion.ADOCon.Execute "BEGIN DISTRIBUTED TRAN"
    bTransaccionEnCurso = True
    m_oConexion.ADOCon.Execute "SET XACT_ABORT OFF"
    
        
    If bInicializar Then
        sConsulta = "DELETE FROM PM_ROL_OBS_ETAPAS WHERE ROL =" & lIdRol
        m_oConexion.ADOCon.Execute sConsulta
        If m_oConexion.ADOCon.Errors.Count > 0 Then
            GoTo Error_Cls
        End If
    End If
    
    'Insertar Las etapas a las que el observador tiene visibilidad.
    'En la SELECT se obtienen las ETAPAs del flujo seleccionada y a la SELECT se le a�aden el ROL y el BLOQUE
    sConsulta = "INSERT INTO PM_ROL_OBS_ETAPAS (ROL,BLOQUE) "
    sConsulta = sConsulta & " SELECT " & lIdRol & ", ID AS BLOQUE FROM PM_BLOQUE B WITH (NOLOCK) WHERE WORKFLOW = " & lIDWfl
    
    If lIdBloque > 0 Then
        sConsulta = sConsulta & " AND ID = " & lIdBloque
    End If
    
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then
        GoTo Error_Cls
    End If
    
    m_oConexion.ADOCon.Execute "COMMIT TRAN"
    bTransaccionEnCurso = False
       
    AsignarEtapasObservador = TESError
    Exit Function
Error_Cls:
    
    AsignarEtapasObservador = basErrores.TratarError(m_oConexion.ADOCon.Errors)
                 
    If bTransaccionEnCurso Then
        m_oConexion.ADOCon.Execute "ROLLBACK TRAN"
        bTransaccionEnCurso = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      'Call g_oErrores.TratarError("M�dulo de clase", "CBloques", "AsignarEtapasObservador", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If
End Function

Public Function DesAsignarEtapaObservador(ByVal lIdRol As Long, Optional ByVal lIdEtapa As Long = 0) As TipoErrorSummit
Dim sConsulta As String
Dim TESError As TipoErrorSummit
Dim bTransaccionEnCurso As Boolean
    

TESError.NumError = TESnoerror

If m_oConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CBloques.DesAsignarEtapaObservador", "No se ha establecido la conexion"
End If
' *****************************************

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
    
    m_oConexion.ADOCon.Execute "BEGIN DISTRIBUTED TRAN"
    bTransaccionEnCurso = True
    m_oConexion.ADOCon.Execute "SET XACT_ABORT OFF"
    
    sConsulta = "DELETE FROM PM_ROL_OBS_ETAPAS "
    
    sConsulta = sConsulta & " WHERE ROL = " & lIdRol
    
    If lIdEtapa > 0 Then
        sConsulta = sConsulta & " AND BLOQUE = " & lIdEtapa
    End If
    
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then
        GoTo Error_Cls
    End If
    
    m_oConexion.ADOCon.Execute "COMMIT TRAN"
    bTransaccionEnCurso = False
       
    DesAsignarEtapaObservador = TESError
    Exit Function
Error_Cls:
    
    DesAsignarEtapaObservador = basErrores.TratarError(m_oConexion.ADOCon.Errors)
                 
    If bTransaccionEnCurso Then
        m_oConexion.ADOCon.Execute "ROLLBACK TRAN"
        bTransaccionEnCurso = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      'Call g_oErrores.TratarError("M�dulo de clase", "CBloques", "DesAsignarEtapaObservador", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If
End Function
