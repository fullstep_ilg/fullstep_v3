VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CTiposPedido"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

''' Variables que almacenan las propiedades de los objetos

Private mCol As Collection
Private m_bEOF As Boolean
Private m_oConexion As CConexion

''' Control de errores
Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum


''' <summary>
''' Carga todos los tipos de pedido.
''' </summary>
''' <param name="sIdi">Es el idioma de la aplicaci�n</param>
''' <returns></returns>
''' <remarks>Llamada desde:frmMantPedidos.Form_Load</remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>
Public Sub CargarTodosLosTiposPedidos(ByVal sIdi As String)

    Dim sConsulta As String
    Dim rs As adodb.Recordset
    Dim fldId As adodb.Field
    Dim fldCod As adodb.Field
    Dim fldDen As adodb.Field
    Dim fldCodConcep As adodb.Field
    Dim fldCodAlmac As adodb.Field
    Dim fldCodRecep As adodb.Field
        
    ''' * Objetivo: Cargar la coleccion de pedidos
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sConsulta = "SELECT P.ID,P.COD,D.DEN,P.CONCEPTO,P.RECEPCIONAR,P.ALMACENAR FROM TIPOPEDIDO P WITH (NOLOCK) "
    sConsulta = sConsulta & "INNER JOIN TIPOPEDIDO_DEN D WITH (NOLOCK) ON P.ID=D.TIPOPEDIDO "
    sConsulta = sConsulta & "WHERE P.BAJALOG=0 AND D.IDI = '" & DblQuote(sIdi) & "' "
    sConsulta = sConsulta & "ORDER BY P.COD"
    
    Set rs = New adodb.Recordset
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    
    
    If rs.eof Then
        rs.Close
        Set rs = Nothing
        Set mCol = Nothing
        Set mCol = New Collection
        Exit Sub
    Else
        Set mCol = Nothing
        Set mCol = New Collection
        Set fldId = rs.Fields("ID")
        Set fldCod = rs.Fields("COD")
        Set fldDen = rs.Fields("DEN")
        Set fldCodConcep = rs.Fields("CONCEPTO")
        Set fldCodRecep = rs.Fields("RECEPCIONAR")
        Set fldCodAlmac = rs.Fields("ALMACENAR")
                
        While Not rs.eof
            Me.Add fldId.Value, fldCod.Value, fldDen.Value, fldCodConcep.Value, fldCodAlmac.Value, fldCodRecep.Value, , fldId.Value
            rs.MoveNext
        Wend

        rs.Close
        Set rs = Nothing
        Set fldCod = Nothing
        Set fldDen = Nothing
        Set fldCodConcep = Nothing
        Set fldCodRecep = Nothing
        Set fldCodAlmac = Nothing
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CTiposPedido", "CargarTodosLosTiposPedidos", ERR, Erl)
      Exit Sub
   End If
        
End Sub

Public Function Add(ByVal lId As Long, ByVal sCod As String, ByVal sDen As String, ByVal udtCodConcep As TipoConcepto, ByVal udtCodAlmac As TipoArtAlmacenable, ByVal udtCodRecep As TipoArtRecepcionable, Optional ByVal vFecAct As Variant, Optional ByVal varIndice As Variant, Optional ByVal vDenominaciones As CMultiidiomas, Optional ByVal vAtributos As CAtributos, Optional ByVal vBaja As Boolean) As CTipoPedido
    
    ''' * Objetivo: Anyadir un tipo de pedido a la coleccion
    
    Dim objnewmember As CTipoPedido
    
    ''' Creacion de objeto arbol
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New CTipoPedido
   
    ''' Paso de los parametros al nuevo objeto
    objnewmember.Id = lId
    objnewmember.Cod = sCod
    objnewmember.Den = sDen
    objnewmember.CodConcep = udtCodConcep
    objnewmember.CodAlmac = udtCodAlmac
    objnewmember.CodRecep = udtCodRecep
                
    Set objnewmember.Conexion = m_oConexion
        
    ''' Anyadir el objeto a la coleccion
    ''' Si no se especifica indice, se anyade al final
    
    If NoHayParametro(vFecAct) Then
        objnewmember.FecAct = Null
    Else
        objnewmember.FecAct = vFecAct
    End If
    
    
    '3328
    If Not IsMissing(vBaja) Then
        objnewmember.Baja = vBaja
    Else
        objnewmember.Baja = False
    End If
    
    '3328
    If Not IsMissing(vAtributos) And Not IsNull(vAtributos) Then
        Set objnewmember.Atributos = vAtributos
    Else
        objnewmember.Atributos = Nothing
    End If
    
    '3328
    If Not IsMissing(vDenominaciones) And Not IsNull(vDenominaciones) Then
        Set objnewmember.Denominaciones = vDenominaciones
    Else
        objnewmember.Denominaciones = Nothing
    End If
    
    If Not IsMissing(varIndice) And Not IsNull(varIndice) Then
        objnewmember.Indice = varIndice
        mCol.Add objnewmember, CStr(varIndice)
    Else
        mCol.Add objnewmember, sCod
    End If
            
    Set Add = objnewmember
    
    Set objnewmember = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CTiposPedido", "Add", ERR, Erl)
      Exit Function
   End If

End Function


Friend Property Set Conexion(ByVal con As CConexion)

    ''' * Objetivo: Dar valor a la variable privada Conexion
    ''' * Recibe: Conexion
    ''' * Devuelve: Nada
    
    Set m_oConexion = con
    
End Property
Friend Property Get Conexion() As CConexion

    ''' * Objetivo: Devolver variable privada Conexion
    ''' * Recibe: Nada
    ''' * Devuelve: Conexion
    
    Set Conexion = m_oConexion
    
End Property


Public Property Get Count() As Long

    ''' * Objetivo: Devolver variable privada Count
    ''' * Recibe: Nada
    ''' * Devuelve: Numero de elementos de la coleccion

    If mCol Is Nothing Then
        Count = 0
    Else
        Count = mCol.Count
    End If

End Property

Public Sub Remove(vntIndexKey As Variant)
    mCol.Remove vntIndexKey
End Sub

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
     Set NewEnum = mCol.[_NewEnum]
End Property

Public Property Get Item(vntIndexKey As Variant) As CTipoPedido

    ''' * Objetivo: Recuperar un �rbol de la coleccion
    ''' * Recibe: Indice del �rbol a recuperar
    ''' * Devuelve: �rbol correspondiente
        
On Error GoTo NoSeEncuentra:

    ''' Devolvemos el item de la coleccion privada
    
    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:

    Set Item = Nothing
    
End Property


Private Sub Class_Initialize()
    Set mCol = New Collection
End Sub

Private Sub Class_Terminate()
    Set mCol = Nothing
    Set m_oConexion = Nothing
End Sub


''' <summary>
''' Carga todos los tipos de pedido con sus denominaciones en todos los idiomas y sus atributos.
''' </summary>
''' <param name="sIdi">Es el idioma de la aplicaci�n</param>
''' <returns></returns>
''' <remarks>Llamada desde:frmPedidos.Form_Load</remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>
Public Sub CargarTodosLosTiposPedidosYDatos(ByVal sIdi As String, Optional ByVal columna As String)

    Dim sConsulta As String
    Dim rs As adodb.Recordset
    Dim fldId As adodb.Field
    Dim fldCod As adodb.Field
    Dim fldDen As adodb.Field
    Dim fldCodConcep As adodb.Field
    Dim fldCodAlmac As adodb.Field
    Dim fldCodRecep As adodb.Field
    Dim fldBaja As adodb.Field
    Dim m_iIndice As Integer
        
    ''' * Objetivo: Cargar la coleccion de pedidos
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sConsulta = "SELECT P.ID,P.COD,D.DEN,P.CONCEPTO,P.RECEPCIONAR,P.ALMACENAR, P.BAJALOG FROM TIPOPEDIDO P WITH (NOLOCK) "
    sConsulta = sConsulta & "INNER JOIN TIPOPEDIDO_DEN D WITH (NOLOCK) ON P.ID=D.TIPOPEDIDO "
    If IsMissing(columna) Or columna = "" Or IsNull(columna) Then
        sConsulta = sConsulta & "WHERE D.IDI = '" & DblQuote(sIdi) & "' "
        sConsulta = sConsulta & "ORDER BY P.COD"
    Else
        Select Case (columna)
            Case "COD"
                sConsulta = sConsulta & "WHERE D.IDI = '" & DblQuote(sIdi) & "' "
                sConsulta = sConsulta & "ORDER BY P.COD"
            Case "CONCEP"
                sConsulta = sConsulta & "WHERE D.IDI = '" & DblQuote(sIdi) & "' "
                sConsulta = sConsulta & "ORDER BY P.CONCEPTO"
            Case "RECEP"
                sConsulta = sConsulta & "WHERE D.IDI = '" & DblQuote(sIdi) & "' "
                sConsulta = sConsulta & "ORDER BY P.RECEPCIONAR"
            Case "ALMAC"
                sConsulta = sConsulta & "WHERE D.IDI = '" & DblQuote(sIdi) & "' "
                sConsulta = sConsulta & "ORDER BY P.ALMACENAR"
            Case "BAJA"
                sConsulta = sConsulta & "WHERE D.IDI = '" & DblQuote(sIdi) & "' "
                sConsulta = sConsulta & "ORDER BY P.BAJALOG"
            Case "CATEG"
            Case "ATRIBUTOS"
            Case Else 'Por denominacion
                sConsulta = sConsulta & "WHERE D.IDI = '" & DblQuote(columna) & "' "
                sConsulta = sConsulta & "ORDER BY D.DEN"
        End Select
    End If
    
    Set rs = New adodb.Recordset
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    
    
    If rs.eof Then
        rs.Close
        Set rs = Nothing
        Set mCol = Nothing
        Set mCol = New Collection
        Exit Sub
    Else
        m_iIndice = 0
        Set mCol = Nothing
        Set mCol = New Collection
        Set fldId = rs.Fields("ID")
        Set fldCod = rs.Fields("COD")
        Set fldDen = rs.Fields("DEN")
        Set fldCodConcep = rs.Fields("CONCEPTO")
        Set fldCodRecep = rs.Fields("RECEPCIONAR")
        Set fldCodAlmac = rs.Fields("ALMACENAR")
        Set fldBaja = rs.Fields("BAJALOG")
                
        While Not rs.eof
            Me.Add fldId.Value, fldCod.Value, fldDen.Value, fldCodConcep.Value, fldCodAlmac.Value, fldCodRecep.Value, , m_iIndice, , , fldBaja
            rs.MoveNext
            m_iIndice = m_iIndice + 1
        Wend

        rs.Close
        Set rs = Nothing
        Set fldCod = Nothing
        Set fldDen = Nothing
        Set fldCodConcep = Nothing
        Set fldCodRecep = Nothing
        Set fldCodAlmac = Nothing
        Set fldBaja = Nothing
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CTiposPedido", "CargarTodosLosTiposPedidosYDatos", ERR, Erl)
      Exit Sub
   End If
        
End Sub

''' <summary>Actualiza Concepto, Almacenar o Recepcionar de ART4</summary>
''' <param name="iTipo">Tipo</param>
''' <param name="iValor">Valor</param>
''' <param name="vBookmarks">Bookmarks</param>
''' <returns>Variable tipo TipoErrorSummit con el error si se ha producido</returns>
''' <remarks>Llamada desde: FSGSClient; Tiempo m�ximo: < 1 sec </remarks>
''' <revision>LTG 02/01/2012</revision>
'EPB/ Esta funci�n ha sido creada en la tarea 1517 y corresponde al programador cumplir la normativa

Public Function ActualizarConcepAlmacRecep(ByVal iTipo As TipoConcepAlmacRecep, ByVal iValor As Integer, ByVal vBookmarks As Variant) As TipoErrorSummit
    Dim TESError As TipoErrorSummit
    Dim sConsulta As String
    Dim bTransaccionEnCurso As Boolean
    Dim oTipoPedido As CArticulo
    Dim bHayErrores As Boolean
    Dim vErrorItems() As Variant
    Dim lIdItem As Integer
    Dim sSetValue As String
    Dim bk As Variant
    Dim m_adores As adodb.Recordset
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    TESError.NumError = TESnoerror

    If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
    
    Set m_adores = New adodb.Recordset
    bTransaccionEnCurso = True
    lIdItem = 0
    For Each bk In vBookmarks
        Set oTipoPedido = Me.Item(CStr(bk))
        If oTipoPedido Is Nothing Then
            GoTo Continuar
        End If
        m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
        
        'CALIDAD: Sin WITH (NOLOCK) porque es para evitar pisar los cambios de otro
        sConsulta = "SELECT FECACT FROM TIPOPEDIDO WHERE COD='" & DblQuote(oTipoPedido.Cod) & "'" '3328 tabla TIPOPEDIDO?

        m_adores.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
    
        If m_adores.eof Then
            m_adores.Close
            bHayErrores = True
            ReDim Preserve vErrorItems(2, lIdItem)
            vErrorItems(0, lIdItem) = NullToStr(oTipoPedido.Cod) & " " & NullToStr(oTipoPedido.Den)
            vErrorItems(1, lIdItem) = TESDatoEliminado
            vErrorItems(2, lIdItem) = 202 'Tipo de pedido
            If bTransaccionEnCurso Then m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
            GoTo Continuar
        End If
    
        If oTipoPedido.FecAct <> m_adores("FECACT").Value Then
            m_adores.Close
            bHayErrores = True
            ReDim Preserve vErrorItems(2, lIdItem)
            vErrorItems(0, lIdItem) = NullToStr(oTipoPedido.Cod) & " " & NullToStr(oTipoPedido.Den)
            vErrorItems(1, lIdItem) = TESInfActualModificada
            vErrorItems(2, lIdItem) = 202 'Tipo de pedido

            If bTransaccionEnCurso Then m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
            GoTo Continuar
        End If
        
        Select Case iTipo
        Case 0  'Concepto
            sSetValue = " CONCEPTO = " & iValor
        Case 1  'Almacenar
            sSetValue = " ALMACENAR = " & iValor
        Case 2  'Recepcionar
            sSetValue = " RECEPCIONAR = " & iValor
        End Select
        
        sConsulta = "UPDATE TIPOPEDIDO SET " & sSetValue & " WHERE COD='" & DblQuote(oTipoPedido.Cod) & "'" '3328 antes ART4
        m_oConexion.ADOCon.Execute sConsulta
        m_adores.Close
        If m_oConexion.ADOCon.Errors.Count > 0 Then
            GoTo Error_Cls
        End If
        
        'Recogemos el nuevo valor de FECACT
        m_adores.Open
        m_adores.Requery
        oTipoPedido.FecAct = m_adores("FECACT").Value
           
        Select Case iTipo
        Case 0  'Concepto
            'Recogemos el nuevo valor de CONCEPTO
            oTipoPedido.Concepto = iValor
        Case 1  'Almacenar
            'Recogemos el nuevo valor de ALMACENAR
            oTipoPedido.Almacenable = iValor
        Case 2  'Recepcionar
            'Recogemos el nuevo valor de RECEPCIONAR
            oTipoPedido.Recepcionable = iValor
        End Select
        m_adores.Close

        'Uso la propiedad ConImagen para saber si se ha actualizado el art�culo
        oTipoPedido.ConImagen = True
        m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
        bTransaccionEnCurso = False
Continuar:
    Next
    
    TESError.NumError = TESnoerror
    TESError.Arg2 = bHayErrores
    TESError.Arg1 = vErrorItems
    
    ActualizarConcepAlmacRecep = TESError
    Set m_adores = Nothing
    Exit Function

Error_Cls:

    bHayErrores = True
    ReDim Preserve vErrorItems(2, lIdItem)
    vErrorItems(0, lIdItem) = NullToStr(oTipoPedido.Cod) & " " & NullToStr(oTipoPedido.Den)
    vErrorItems(1, lIdItem) = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    vErrorItems(2, lIdItem) = 202 'Tipo de pedido
    If bTransaccionEnCurso Then m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
    
    GoTo Continuar
'    Resume Next
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      'Call g_oErrores.TratarError("Class Module", "CTiposPedido", "ActualizarConcepAlmacRecep", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If
End Function



''' <summary>
''' Eliminar tipos de pedido
''' </summary>
''' <param name="aCodigos">lista de Codigos a eliminar</param>
''' <returns>Error de haberlo</returns>
''' <remarks>Llamada desde:  frmEstrMat.cmdEliminarTipoPedido_Click; Tiempo m�ximo:0,1</remarks>
''' <revision></revision>
Public Function EliminarMultiplesTiposPedidoDeBaseDatos(ByVal aCodigos As Variant) As TipoErrorSummit

Dim udtTESError As TipoErrorSummit
Dim bTransaccion As Boolean
Dim sConsulta As String
Dim ContRegBorrados As Long
Dim AdoRes As adodb.Recordset
Dim rs As adodb.Recordset
Dim adoComm As adodb.Command
Dim adoParam As adodb.Parameter
Dim i As Integer

Dim aNoEliminados() As Variant
Dim iRes As Integer

Dim m_lID As Long

'******************************************
'***** M U Y   I M P O R T A N T E     ****
'*****                                 ****
'***** SI SE REALIZAN CAMBIOS          ****
'***** DESPUES DEL 21/06/2002          ****
'***** MIRAR SI HAY QUE PASARLOS A     ****
'***** FSGSINTEGRATION.DLL             ****
'******************************************

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:

    iRes = 0
    udtTESError.NumError = TESnoerror
    
    For i = 1 To UBound(aCodigos)
    
        m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
        bTransaccion = True
        
        'Recuperar el ID del TIPO de PEDIDO a Borrar
        sConsulta = "SELECT ID FROM TIPOPEDIDO WHERE COD =?"
        Set adoComm = New adodb.Command
        Set adoComm.ActiveConnection = m_oConexion.ADOCon
        adoComm.CommandType = adCmdText
        Set adoParam = adoComm.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=basParametros.gLongitudesDeCodigos.giLongCodTIPPED, Value:=StrToVbNULL(aCodigos(i, 1)))
        adoComm.Parameters.Append adoParam
        adoComm.CommandText = sConsulta
        Set AdoRes = adoComm.Execute
        m_lID = AdoRes.Collect(0)
        AdoRes.Close
        
        'Borrar las denominaciones del TIPO de PEDIDO
        sConsulta = "DELETE FROM TIPOPEDIDO_DEN WHERE TIPOPEDIDO=?"
        Set adoComm = New adodb.Command
        Set adoComm.ActiveConnection = m_oConexion.ADOCon
        adoComm.CommandText = sConsulta
        adoComm.CommandType = adCmdText
        adoComm.Prepared = True
        Set adoParam = adoComm.CreateParameter(Type:=adInteger, Direction:=adParamInput, Value:=m_lID)
        adoComm.Parameters.Append adoParam
        adoComm.Execute
        If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
        'Borrar los atributos del TIPO de PEDIDO
        sConsulta = "DELETE FROM TIPOPEDIDO_ATRIB WHERE TIPOPEDIDO_ID=?"
        Set adoComm = New adodb.Command
        Set adoComm.ActiveConnection = m_oConexion.ADOCon
        adoComm.CommandText = sConsulta
        adoComm.CommandType = adCmdText
        adoComm.Prepared = True
        Set adoParam = adoComm.CreateParameter(Type:=adInteger, Direction:=adParamInput, Value:=m_lID)
        adoComm.Parameters.Append adoParam
        adoComm.Execute
        If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
        'Borrar catalogo del TIPO de PEDIDO
        sConsulta = "DELETE FROM TIPOPEDIDO_CATN WHERE TIPOPEDIDO_ID=?"
        Set adoComm = New adodb.Command
        Set adoComm.ActiveConnection = m_oConexion.ADOCon
        adoComm.CommandText = sConsulta
        adoComm.CommandType = adCmdText
        adoComm.Prepared = True
        Set adoParam = adoComm.CreateParameter(Type:=adInteger, Direction:=adParamInput, Value:=m_lID)
        adoComm.Parameters.Append adoParam
        adoComm.Execute
        If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
        'Borrar el TIPO de PEDIDO
        sConsulta = "DELETE FROM TIPOPEDIDO WHERE COD=?"
        Set adoComm = New adodb.Command
        Set adoComm.ActiveConnection = m_oConexion.ADOCon
        adoComm.CommandText = sConsulta
        adoComm.CommandType = adCmdText
        adoComm.Prepared = True
        Set adoParam = adoComm.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=basParametros.gLongitudesDeCodigos.giLongCodTIPPED, Value:=StrToVbNULL(aCodigos(i, 1)))
        adoComm.Parameters.Append adoParam
        adoComm.Execute
        If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
        
        If bTransaccion Then
            m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
        End If
        bTransaccion = False
    
Seguir:

    Next
     
    If iRes > 0 Then
        udtTESError.NumError = TESImposibleEliminar
        udtTESError.Arg1 = aNoEliminados
    Else
        udtTESError.NumError = TESnoerror
    End If
    
    bTransaccion = False
    EliminarMultiplesTiposPedidoDeBaseDatos = udtTESError
    
    Set adoParam = Nothing
    Set adoComm = Nothing
    
    Exit Function

Error_Cls:

    iRes = iRes + 1
    udtTESError = basErrores.TratarError(m_oConexion.ADOCon.Errors)
            
    ReDim Preserve aNoEliminados(2, iRes)
    aNoEliminados(1, iRes) = aCodigos(i, 1)
    aNoEliminados(2, iRes) = udtTESError.Arg1
    If bTransaccion Then
        m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
    End If
    
    bTransaccion = False
    Resume Seguir
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      'Call g_oErrores.TratarError("Class Module", "CTiposPedido", "EliminarMultiplesTiposPedidoDeBaseDatos", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If

End Function

''' <summary>
''' Carga todos los tipos de pedido para los flujos de aprobacion
''' </summary>
''' <param name="sIdi">Es el idioma de la aplicaci�n</param>
''' <param name="Categoria">Id de la categoria del catalogo</param>
''' <param name="NivelCat">Nivel de la categoria del catalogo</param>
''' <returns></returns>
''' <remarks>Llamada desde:frmPedidos.Form_Load</remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>
Public Sub CargarTiposPedidosPorFlujoDeAprobacion(ByVal sIdi As String, Optional ByVal Categoria As Integer, Optional ByVal NivelCat As Integer)

    Dim sConsulta As String
    Dim rs As adodb.Recordset
    Dim fldId As adodb.Field
    Dim fldCATN As adodb.Field
    Dim fldCod As adodb.Field
    Dim fldDen As adodb.Field
    Dim fldCodConcep As adodb.Field
    Dim fldCodAlmac As adodb.Field
    Dim fldCodRecep As adodb.Field
        
    ''' * Objetivo: Cargar la coleccion de pedidos
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sConsulta = "SELECT DISTINCT CTES.CAT,P.ID,P.COD,D.DEN,P.CONCEPTO,P.RECEPCIONAR,P.ALMACENAR FROM TIPOPEDIDO P WITH (NOLOCK) "
    sConsulta = sConsulta & "INNER JOIN CATN_TIPOPEDIDO_EMP_SOLICITUD CTES WITH (NOLOCK) ON P.ID=CTES.TIPOPEDIDO "
    sConsulta = sConsulta & "INNER JOIN TIPOPEDIDO_DEN D WITH (NOLOCK) ON P.ID=D.TIPOPEDIDO "
    sConsulta = sConsulta & "WHERE P.BAJALOG=0 AND D.IDI = '" & DblQuote(sIdi) & "'"
    If Categoria > 0 Then
        sConsulta = sConsulta & " AND CTES.CAT=" & Categoria
    End If
    If NivelCat > 0 Then
        sConsulta = sConsulta & " AND CTES.NIVEL=" & NivelCat
    End If
    sConsulta = sConsulta & "ORDER BY P.COD"
    
    Set rs = New adodb.Recordset
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    If rs.eof Then
        rs.Close
        Set rs = Nothing
        Set mCol = Nothing
        Set mCol = New Collection
        Exit Sub
    Else
        Set mCol = Nothing
        Set mCol = New Collection
        Set fldId = rs.Fields("ID")
        Set fldCod = rs.Fields("COD")
        Set fldDen = rs.Fields("DEN")
        Set fldCodConcep = rs.Fields("CONCEPTO")
        Set fldCodRecep = rs.Fields("RECEPCIONAR")
        Set fldCodAlmac = rs.Fields("ALMACENAR")
        Set fldCATN = rs.Fields("CAT")
                
        While Not rs.eof
            Me.Add fldId.Value, fldCod.Value, fldDen.Value, fldCodConcep.Value, fldCodAlmac.Value, fldCodRecep.Value, , fldId.Value
            rs.MoveNext
        Wend

        rs.Close
        Set rs = Nothing
        Set fldCod = Nothing
        Set fldDen = Nothing
        Set fldCodConcep = Nothing
        Set fldCodRecep = Nothing
        Set fldCodAlmac = Nothing
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CTiposPedido", "CargarTiposPedidosPorFlujoDeAprobacion", ERR, Erl)
      Exit Sub
   End If
        
End Sub



''' <summary>
''' Carga todos los tipos de pedido por categoria.
''' </summary>
''' <param name="sIdi">Es el idioma de la aplicaci�n</param>
''' <param name="Categoria">Id de la categoria del catalogo</param>
''' <param name="NivelCat">Nivel de la categoria del catalogo</param>
''' <returns></returns>
''' <remarks>Llamada desde:frmPedidos.Form_Load</remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>
Public Sub CargarTodosLosTiposPedidosPorCategoria(ByVal sIdi As String, ByVal Categoria As Integer, ByVal NivelCat As Integer)

    Dim sConsulta As String
    Dim rs As adodb.Recordset
    Dim fldId As adodb.Field
    Dim fldCATN As adodb.Field
    Dim fldCod As adodb.Field
    Dim fldDen As adodb.Field
    Dim fldCodConcep As adodb.Field
    Dim fldCodAlmac As adodb.Field
    Dim fldCodRecep As adodb.Field
        
    ''' * Objetivo: Cargar la coleccion de pedidos
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sConsulta = "SELECT TPC.CATN,P.ID,P.COD,D.DEN,P.CONCEPTO,P.RECEPCIONAR,P.ALMACENAR FROM TIPOPEDIDO P WITH (NOLOCK) "
    sConsulta = sConsulta & "INNER JOIN TIPOPEDIDO_DEN D WITH (NOLOCK) ON P.ID=D.TIPOPEDIDO "
    sConsulta = sConsulta & "LEFT JOIN TIPOPEDIDO_CATN TPC WITH (NOLOCK) ON TPC.TIPOPEDIDO_ID=D.TIPOPEDIDO AND TPC.CATN=" & Categoria & " AND TPC.NIVEL=" & NivelCat
    sConsulta = sConsulta & "WHERE P.BAJALOG=0 AND D.IDI = '" & DblQuote(sIdi) & "' "
    sConsulta = sConsulta & "ORDER BY P.COD"
    
    Set rs = New adodb.Recordset
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    'Esta variable indica que existen Tipos de pedido por categoria con lo que no se deberian sacar todos los tipos
    ' de pedido de la tabla, sino solo los que esten en la tabla TIPOPEDIDO_CATN, si en esa tabla no esta la categoria, se sacaran todos
    Dim bExisteRestriccion As Boolean
    'Recorremos el recordset para ver si la categoria tienen tipos de pedido asociados
    While Not rs.eof
        If NullToStr(rs.Fields("CATN").Value) <> "" Then
            bExisteRestriccion = True
        End If
        rs.MoveNext
    Wend
    
    rs.MoveFirst
    If rs.eof Then
        rs.Close
        Set rs = Nothing
        Set mCol = Nothing
        Set mCol = New Collection
        Exit Sub
    Else
        Set mCol = Nothing
        Set mCol = New Collection
        Set fldId = rs.Fields("ID")
        Set fldCod = rs.Fields("COD")
        Set fldDen = rs.Fields("DEN")
        Set fldCodConcep = rs.Fields("CONCEPTO")
        Set fldCodRecep = rs.Fields("RECEPCIONAR")
        Set fldCodAlmac = rs.Fields("ALMACENAR")
        Set fldCATN = rs.Fields("CATN")
                
        While Not rs.eof
            If bExisteRestriccion Then
                If NullToStr(fldCATN.Value) <> "" Then
                    'Como la categoria tiene sus tipos de pedido, solo se sacan esos
                    Me.Add fldId.Value, fldCod.Value, fldDen.Value, fldCodConcep.Value, fldCodAlmac.Value, fldCodRecep.Value, , fldId.Value
                End If
            Else
                'Esta categoria no tiene tipos de pedidos en la tabla TIPOPEDIDO_CATN, por lo que se sacan todos
                Me.Add fldId.Value, fldCod.Value, fldDen.Value, fldCodConcep.Value, fldCodAlmac.Value, fldCodRecep.Value, , fldId.Value
            End If
            rs.MoveNext
        Wend

        rs.Close
        Set rs = Nothing
        Set fldCod = Nothing
        Set fldDen = Nothing
        Set fldCodConcep = Nothing
        Set fldCodRecep = Nothing
        Set fldCodAlmac = Nothing
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CTiposPedido", "CargarTodosLosTiposPedidosPorCategoria", ERR, Erl)
      Exit Sub
   End If
        
End Sub
