VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CDepartamento"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'********************* CDepartamento **********************************
'*             Autor : Javier Arana
'*             Creada : 28/7/98
'***************************************************************

Option Explicit

'****************** Variables privadas de la clase **********
Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
End Enum

Implements IBaseDatos

Private mvarCod As String 'local copy
Private mvarDen As String 'local copy
Private mvarIndice As Long
Private mvarConexion As CConexion
Private mvarConexionServer As CConexionDeUseServer
Private mvarRecordset As adodb.Recordset
Private mvarBajaLogica As Integer

Public Property Let Indice(ByVal lInd As Long)
    mvarIndice = lInd
End Property
Public Property Get Indice() As Long
    Indice = mvarIndice
End Property
Friend Property Set Conexion(ByVal con As CConexion)
Set mvarConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = mvarConexion
End Property
Friend Property Set ConexionServer(ByVal con As CConexionDeUseServer)
Set mvarConexionServer = con
End Property
Friend Property Get ConexionServer() As CConexionDeUseServer
Set ConexionServer = mvarConexionServer
End Property
Public Property Let Den(ByVal vData As String)
    mvarDen = vData
End Property


Public Property Get Den() As String
    Den = mvarDen
End Property

Public Property Let Cod(ByVal vData As String)
    mvarCod = vData
End Property


Public Property Get Cod() As String
    Cod = mvarCod
End Property

Public Property Get BajaLog() As Integer
    BajaLog = mvarBajaLogica
End Property
Public Property Let BajaLog(ByVal varBajaLog As Integer)
    mvarBajaLogica = varBajaLog
End Property


Private Sub ConectarDeUseServer()
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If mvarConexionServer.ADOSummitCon Is Nothing Then
        Set mvarConexionServer.ADOSummitCon = New adodb.Connection
        mvarConexionServer.ADOSummitCon.Open "Provider=SQLOLEDB.1;Auto Translate=FALSE;Server=" & basParametros.ServidorGS & ";Database=" & basParametros.BaseDatosGS & ";", LICDBLogin, LICDBContra
        mvarConexionServer.ADOSummitCon.CursorLocation = adUseServer
        mvarConexionServer.ADOSummitCon.CommandTimeout = 120
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CDepartamento", "ConectarDeUseServer", ERR, Erl)
        Exit Sub
    End If
End Sub

Private Sub Class_Terminate()

Set mvarConexion = Nothing
Set mvarConexionServer = Nothing

End Sub


Private Function IBaseDatos_AnyadirABaseDatos() As TipoErrorSummit
Dim sConsulta As String
Dim TESError As TipoErrorSummit


'********* Precondicion *******************
If mvarConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CDepartamento.AnyadirABAseDatos", "No se ha establecido la conexion"
    Exit Function
End If
'******************************************
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
TESError.NumError = TESnoerror
   
If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
    
    sConsulta = "INSERT INTO DEP (COD,DEN) VALUES ('" & DblQuote(mvarCod) & "','" & DblQuote(mvarDen) & "')"
    mvarConexion.ADOCon.Execute sConsulta
    IBaseDatos_AnyadirABaseDatos = TESError
    Exit Function
    
Error_Cls:

    IBaseDatos_AnyadirABaseDatos = basErrores.TratarError(mvarConexion.ADOCon.Errors)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        'Call g_oErrores.TratarError("Class Module", "CDepartamento", "IBaseDatos_AnyadirABaseDatos", ERR, Erl)
        GoTo Error_Cls
        Exit Function
    End If

End Function

Private Function IBaseDatos_CambiarCodigo(CodigoNuevo As Variant) As TipoErrorSummit

End Function

Private Sub IBaseDatos_CancelarEdicion()

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If Not mvarRecordset Is Nothing Then
    mvarRecordset.Close
    Set mvarRecordset = Nothing
End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CDepartamento", "IBaseDatos_CancelarEdicion", ERR, Erl)
        Exit Sub
    End If

End Sub

''' <summary>Comprueba la existencia de un departamento</summary>
''' <returns>Booleano indicando la existencia</returns>
''' <remarks>Llamada desde: FSGClient; Tiempo m�ximo: 1 sec </remarks>
''' <revision>LTG 04/01/2012</revision>

Private Function IBaseDatos_ComprobarExistenciaEnBaseDatos() As Boolean
    Dim rs As New adodb.Recordset
    
    '******************* Precondicion *******************
    If mvarConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CDepartamento.ComprobarExistenciaEnBaseDatos", "No se ha establecido la conexion"
        Exit Function
    End If
    '*****************************************************
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
    rs.Open "SELECT ID FROM DEP WITH (NOLOCK) WHERE COD='" & DblQuote(mvarCod) & "'", mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
        
    If rs.eof Then
        IBaseDatos_ComprobarExistenciaEnBaseDatos = False
    Else
        IBaseDatos_ComprobarExistenciaEnBaseDatos = True
    End If
        
    rs.Close
    Set rs = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CDepartamento", "IBaseDatos_ComprobarExistenciaEnBaseDatos", ERR, Erl)
        Exit Function
    End If
End Function

''' <summary>Elimina un departamento de la BD</summary>
''' <returns>Variable tipo TipoErrorSummit con el error si se ha producido</returns>
''' <remarks>Llamada desde: FSGClient; Tiempo m�ximo: 1 sec </remarks>
''' <revision>LTG 04/01/2012</revision>

Private Function IBaseDatos_EliminarDeBaseDatos() As TipoErrorSummit
    Dim sConsulta As String
    Dim rs As New adodb.Recordset
    Dim TESError As TipoErrorSummit
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    TESError.NumError = TESnoerror
    
    If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
    
        rs.Open "SELECT DEP_PERS.PERS FROM DEP_PERS WITH (NOLOCK) WHERE DEP=" & DblQuote(mvarCod), mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
        
        If Not rs.eof Then
            IBaseDatos_EliminarDeBaseDatos = TESError
            rs.Close
            Set rs = Nothing
            Exit Function
        End If
        rs.Close
        
        sConsulta = "SELECT COUNT(*) FROM PM_COPIA_ROL CR WITH (NOLOCK) WHERE CR.DEP = '" & DblQuote(mvarCod) & "'"
            Set rs = New adodb.Recordset
            rs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
            If rs(0).Value > 0 Then
                TESError.NumError = TESImposibleEliminar
                TESError.Arg1 = 197
                rs.Close
                Set rs = Nothing
                mvarConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
                IBaseDatos_EliminarDeBaseDatos = TESError
                Exit Function
            End If
            rs.Close
 
    
    ' La relacion como hijo de una division se borrara mediante el trigger de la BD
    mvarConexion.ADOCon.Execute "DELETE FROM DEP WHERE COD=" & mvarCod
    
    IBaseDatos_EliminarDeBaseDatos = TESError
    
    Set rs = Nothing
    Exit Function
    
Error_Cls:
    
    IBaseDatos_EliminarDeBaseDatos = basErrores.TratarError(mvarConexion.ADOCon.Errors)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        'Call g_oErrores.TratarError("Class Module", "CDepartamento", "IBaseDatos_EliminarDeBaseDatos", ERR, Erl)
        GoTo Error_Cls
        Exit Function
    End If
End Function

''' <summary>Finaliza la eliminaci�n de un registro</summary>
''' <returns>Variable tipo TipoErrorSummit con el error si se ha producido</returns>
''' <remarks>Llamada desde: FSGClient; Tiempo m�ximo: 1 sec </remarks>
''' <revision>LTG 04/01/2012</revision>

Private Function IBaseDatos_FinalizarEdicionEliminando() As TipoErrorSummit
    Dim rs As New adodb.Recordset
    Dim TESError As TipoErrorSummit
    
    '******************* Precondicion *******************
    If mvarConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CDepartamento.EliminarDeBaseDatos", "No se ha establecido la conexion"
        Exit Function
    End If
    '*****************************************************
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    TESError.NumError = TESnoerror
    
    If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:

    rs.Open "SELECT DEP_PERS.PERS FROM DEP_PERS WITH (NOLOCK) WHERE DEP=" & mvarCod, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    
    If Not rs.eof Then
        IBaseDatos_FinalizarEdicionEliminando = TESError
        rs.Close
        Set rs = Nothing
        mvarRecordset.Close
        Set mvarRecordset = Nothing
        Exit Function
    End If
    
    ' La relacion como hijo de una division se borrara mediante el trigger de la BD
    
    mvarRecordset.Delete
    mvarRecordset.Close
    Set mvarRecordset = Nothing
    rs.Close
    Set rs = Nothing
    IBaseDatos_FinalizarEdicionEliminando = TESError
    Exit Function
    
Error_Cls:
    
    IBaseDatos_FinalizarEdicionEliminando = basErrores.TratarError(mvarConexionServer.ADOSummitCon.Errors)
    
On Error Resume Next
    If mvarRecordset.EditMode <> adEditNone Then
        mvarRecordset.CancelUpdate
    End If
    mvarRecordset.Close
    Set mvarRecordset = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        'Call g_oErrores.TratarError("Class Module", "CDepartamento", "IBaseDatos_FinalizarEdicionEliminando", ERR, Erl)
        GoTo Error_Cls
        Exit Function
    End If
    
End Function

Private Function IBaseDatos_FinalizarEdicionModificando() As TipoErrorSummit
Dim TESError As TipoErrorSummit
'********* Precondicion *******************
If mvarConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CDepartamento.ModificarEnBaseDatos", "No se ha establecido la conexion"
    Exit Function
End If
'******************************************

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
TESError.NumError = TESnoerror

' ****************** Validacion de datos ***************************

If mvarCod = "" Then
    IBaseDatos_FinalizarEdicionModificando = TESError
    Exit Function
End If

If mvarDen = "" Then
    IBaseDatos_FinalizarEdicionModificando = TESError
    Exit Function
End If

' ***********************************************************

If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
    
    mvarRecordset("COD") = mvarCod
    mvarRecordset("DEN") = mvarDen
      
    mvarRecordset.Update
    mvarRecordset.Close
    Set mvarRecordset = Nothing
    
    IBaseDatos_FinalizarEdicionModificando = TESError
    Exit Function
    

Error_Cls:

    IBaseDatos_FinalizarEdicionModificando = basErrores.TratarError(mvarConexionServer.ADOSummitCon.Errors)
    Resume Salir:
    
Salir:

    On Error Resume Next
    If mvarRecordset.EditMode <> adEditNone Then
        mvarRecordset.CancelUpdate
        
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        'Call g_oErrores.TratarError("Class Module", "CDepartamento", "IBaseDatos_FinalizarEdicionModificando", ERR, Erl)
        GoTo Error_Cls
        Exit Function
    End If
    
End Function

''' <summary>Inicia la edici�n de un registro</summary>
''' <param name="Bloquear">No se usa</param>
''' <param name="UsuarioBloqueo">No se usa</param>
''' <returns>Variable tipo TipoErrorSummit con el error si se ha producido</returns>
''' <remarks>Llamada desde: FSGClient; Tiempo m�ximo: 1 sec </remarks>
''' <revision>LTG 04/01/2012</revision>

Private Function IBaseDatos_IniciarEdicion(Optional ByVal Bloquear As Boolean, Optional ByVal UsuarioBloqueo As String) As TipoErrorSummit
    Dim TESError As TipoErrorSummit
    
    '******************* Precondicion *******************
    If mvarConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CDepartamento.IniciarEdicion", "No se ha establecido la conexion"
        Exit Function
    End If
    '*****************************************************
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    TESError.NumError = TESnoerror
    
    ConectarDeUseServer
    Set mvarRecordset = New adodb.Recordset
    'mvarRecordset.CursorLocation = adUseServer
    'CALIDAD: Sin WITH (NOLOCK) porque se rellena el recordset mvarRecordset que posteriormente se utiliza para las actualizaciones
    'en otros m�todos de la clase. Si se hace la lectura con WITH (NOLOCK) no se permite la llamada al m�todo Update del recordset.
    mvarRecordset.Open "SELECT COD,DEN,FECACT FROM DEP WHERE COD=" & StrToSQLNULL(mvarCod), mvarConexionServer.ADOSummitCon, adOpenKeyset, adLockOptimistic
    
    If mvarRecordset.eof Then
        mvarRecordset.Close
        IBaseDatos_IniciarEdicion = TESError
        Set mvarRecordset = Nothing
        Exit Function
    End If
    
    mvarCod = mvarRecordset("COD")
    mvarDen = mvarRecordset("DEN")
    
    IBaseDatos_IniciarEdicion = TESError
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CDepartamento", "IBaseDatos_IniciarEdicion", ERR, Erl)
        Exit Function
    End If
End Function

Private Function IBaseDatos_ModificarEnBaseDatos() As Boolean
End Function


