VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CConfSegPedidos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements IBaseDatos

Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum

Private m_bHayCambios As Boolean

'Valores
Private m_vUsu As Variant
Private m_vAbierto As Variant
Private m_vAnyo As Variant
Private m_vPedidoNum As Variant
Private m_vOrdenNum As Variant
Private m_vEst As Variant
Private m_vFecDesde As Variant
Private m_vFecHasta As Variant
Private m_vNumPedErp As Variant
Private m_vNumExt As Variant
Private m_vTipoPedido As Variant
Private m_vTipo As Variant
Private m_vAbono As Variant
Private m_vPedAbiertoAnyo As Variant
Private m_vPedAbiertoPedidoNum As Variant
Private m_vPedAbiertoOrdenNum As Variant
Private m_vProve As Variant
Private m_vCodProveErp As Variant
Private m_vGMN1 As Variant
Private m_vGMN2 As Variant
Private m_vGMN3 As Variant
Private m_vGMN4 As Variant
Private m_vArtInt As Variant
Private m_vDest As Variant
Private m_vFecEntregaDesde As Variant
Private m_vFecEntregaHasta As Variant
Private m_vEmpresa As Variant
Private m_vReceptor As Variant
Private m_vAprovisionador As Variant
Private m_vGestor As Variant
Private m_vUON1 As Variant
Private m_vUON2 As Variant
Private m_vUON3 As Variant
Private m_vUON4 As Variant
Private m_vCat1 As Variant
Private m_vCat2 As Variant
Private m_vCat3 As Variant
Private m_vCat4 As Variant
Private m_vCat5 As Variant
Private m_vImAutofactura As Variant
Private m_vImBloqueofactura As Variant
Private m_vRecepAutomatica As Variant
Private m_vTipoFacturacion As Variant 'no esta en el dibujo
Private m_vFecPlanEntregaDesde As Variant
Private m_vFecPlanEntregaHasta As Variant 'no esta en el dibujo
Private m_vAnyoFac As Variant
Private m_vNumFac As Variant
Private m_vEstFac As Variant
Private m_vPagoNum As Variant
Private m_vPagoEst As Variant
Private m_vFecFacDesde As Variant
Private m_vFecFacHasta As Variant
Private m_vFecPagoDesde As Variant
Private m_vFecPagoHasta As Variant
Private m_vProceAnyo As Variant
Private m_vProceGmn1 As Variant
Private m_vProceCod As Variant
Private m_vProveAdj As Variant
Private m_vSolicit As Variant
Private m_vPedAbiertoFecIni As Variant
Private m_vPedAbiertoFecFin As Variant
Private m_vPedAbiertoPorImporte As Variant
Private m_vPedAbiertoPorCantidad As Variant

Private m_oConexion As CConexion
Private m_vIndice As Variant
Private m_adores As adodb.Recordset
Private m_oAtrBuscar As CAtributosBuscar
Private m_bVistaAvanzada As Boolean

Private m_bPedBajaLogica As Boolean

Public Property Get Abierto() As Variant

    Abierto = m_vAbierto

End Property

Public Property Let Abierto(ByVal vAbierto As Variant)

    m_vAbierto = vAbierto

End Property

Public Property Get Anyo() As Variant

    Anyo = m_vAnyo

End Property

Public Property Let Anyo(ByVal vAnyo As Variant)

    m_vAnyo = vAnyo

End Property

Public Property Get PedidoNum() As Variant

    PedidoNum = m_vPedidoNum

End Property

Public Property Let PedidoNum(ByVal vPedidoNum As Variant)

    m_vPedidoNum = vPedidoNum

End Property

Public Property Get OrdenNum() As Variant

    OrdenNum = m_vOrdenNum

End Property

Public Property Let OrdenNum(ByVal vOrdenNum As Variant)

    m_vOrdenNum = vOrdenNum

End Property

Public Property Get Est() As Variant

    Est = m_vEst

End Property

Public Property Let Est(ByVal vEst As Variant)

    m_vEst = vEst

End Property

Public Property Get FecDesde() As Variant

    FecDesde = m_vFecDesde

End Property

Public Property Let FecDesde(ByVal vFecDesde As Variant)

    m_vFecDesde = vFecDesde

End Property

Public Property Get FecHasta() As Variant

    FecHasta = m_vFecHasta

End Property

Public Property Let FecHasta(ByVal vFecHasta As Variant)

    m_vFecHasta = vFecHasta

End Property

Public Property Get NumPedErp() As Variant

    NumPedErp = m_vNumPedErp

End Property

Public Property Let NumPedErp(ByVal vNumPedErp As Variant)

    m_vNumPedErp = vNumPedErp

End Property

Public Property Get NumExt() As Variant

    NumExt = m_vNumExt

End Property

Public Property Let NumExt(ByVal vNumExt As Variant)

    m_vNumExt = vNumExt

End Property

Public Property Get TipoPedido() As Variant

    TipoPedido = m_vTipoPedido

End Property

Public Property Let TipoPedido(ByVal vTipoPedido As Variant)

    m_vTipoPedido = vTipoPedido

End Property

Public Property Get Tipo() As Variant

    Tipo = m_vTipo

End Property

Public Property Let Tipo(ByVal vTipo As Variant)

    m_vTipo = vTipo

End Property

Public Property Get Abono() As Variant

    Abono = m_vAbono

End Property

Public Property Let Abono(ByVal vAbono As Variant)

    m_vAbono = vAbono

End Property

Public Property Get PedAbiertoAnyo() As Variant

    PedAbiertoAnyo = m_vPedAbiertoAnyo

End Property

Public Property Let PedAbiertoAnyo(ByVal vPedAbiertoAnyo As Variant)

    m_vPedAbiertoAnyo = vPedAbiertoAnyo

End Property

Public Property Get PedAbiertoPedidoNum() As Variant

    PedAbiertoPedidoNum = m_vPedAbiertoPedidoNum

End Property

Public Property Let PedAbiertoPedidoNum(ByVal vPedAbiertoPedidoNum As Variant)

    m_vPedAbiertoPedidoNum = vPedAbiertoPedidoNum

End Property

Public Property Get PedAbiertoOrdenNum() As Variant

    PedAbiertoOrdenNum = m_vPedAbiertoOrdenNum

End Property

Public Property Let PedAbiertoOrdenNum(ByVal vPedAbiertoOrdenNum As Variant)

    m_vPedAbiertoOrdenNum = vPedAbiertoOrdenNum

End Property

Public Property Get Prove() As Variant

    Prove = m_vProve

End Property

Public Property Let Prove(ByVal vProve As Variant)

    m_vProve = vProve

End Property

Public Property Get CodProveErp() As Variant

    CodProveErp = m_vCodProveErp

End Property

Public Property Let CodProveErp(ByVal vCodProveErp As Variant)

    m_vCodProveErp = vCodProveErp

End Property

Public Property Get GMN1() As Variant

    GMN1 = m_vGMN1

End Property

Public Property Let GMN1(ByVal vGMN1 As Variant)

    m_vGMN1 = vGMN1

End Property

Public Property Get GMN2() As Variant

    GMN2 = m_vGMN2

End Property

Public Property Let GMN2(ByVal vGMN2 As Variant)

    m_vGMN2 = vGMN2

End Property

Public Property Get GMN3() As Variant

    GMN3 = m_vGMN3

End Property

Public Property Let GMN3(ByVal vGMN3 As Variant)

    m_vGMN3 = vGMN3

End Property

Public Property Get GMN4() As Variant

    GMN4 = m_vGMN4

End Property

Public Property Let GMN4(ByVal vGMN4 As Variant)

    m_vGMN4 = vGMN4

End Property

Public Property Get ArtInt() As Variant

    ArtInt = m_vArtInt

End Property

Public Property Let ArtInt(ByVal vArtInt As Variant)

    m_vArtInt = vArtInt

End Property

Public Property Get Dest() As Variant

    Dest = m_vDest

End Property

Public Property Let Dest(ByVal vDest As Variant)

    m_vDest = vDest

End Property

Public Property Get FecEntregaDesde() As Variant

    FecEntregaDesde = m_vFecEntregaDesde

End Property

Public Property Let FecEntregaDesde(ByVal vFecEntregaDesde As Variant)

    m_vFecEntregaDesde = vFecEntregaDesde

End Property

Public Property Get FecEntregaHasta() As Variant

    FecEntregaHasta = m_vFecEntregaHasta

End Property

Public Property Let FecEntregaHasta(ByVal vFecEntregaHasta As Variant)

    m_vFecEntregaHasta = vFecEntregaHasta

End Property

Public Property Get Empresa() As Variant

    Empresa = m_vEmpresa

End Property

Public Property Let Empresa(ByVal vEmpresa As Variant)

    m_vEmpresa = vEmpresa

End Property

Public Property Get Receptor() As Variant

    Receptor = m_vReceptor

End Property

Public Property Let Receptor(ByVal vReceptor As Variant)

    m_vReceptor = vReceptor

End Property

Public Property Get Aprovisionador() As Variant

    Aprovisionador = m_vAprovisionador

End Property

Public Property Let Aprovisionador(ByVal vAprovisionador As Variant)

    m_vAprovisionador = vAprovisionador

End Property

Public Property Get Gestor() As Variant

    Gestor = m_vGestor

End Property

Public Property Let Gestor(ByVal vGestor As Variant)

    m_vGestor = vGestor

End Property

Public Property Get ImAutofactura() As Variant
    ImAutofactura = m_vImAutofactura
End Property

Public Property Let ImAutofactura(ByVal vImAutofactura As Variant)
    m_vImAutofactura = vImAutofactura
End Property

Public Property Set AtrBuscar(ByVal Data As CAtributosBuscar)
    Set m_oAtrBuscar = Data
End Property
Public Property Get AtrBuscar() As CAtributosBuscar
    Set AtrBuscar = m_oAtrBuscar
End Property

Public Property Let VistaAvanzada(ByVal Data As Boolean)
    m_bVistaAvanzada = Data
End Property
Public Property Get VistaAvanzada() As Boolean
    VistaAvanzada = m_bVistaAvanzada
End Property

Public Property Get ImBloqueofactura() As Variant
    ImBloqueofactura = m_vImBloqueofactura
End Property
Public Property Let ImBloqueofactura(ByVal vImBloqueofactura As Variant)
    m_vImBloqueofactura = vImBloqueofactura
End Property

Public Property Get RecepAutomatica() As Variant

    RecepAutomatica = m_vRecepAutomatica

End Property

Public Property Let RecepAutomatica(ByVal vRecepAutomatica As Variant)

    m_vRecepAutomatica = vRecepAutomatica

End Property

Public Property Get TipoFacturacion() As Variant

    TipoFacturacion = m_vTipoFacturacion

End Property

Public Property Let TipoFacturacion(ByVal vTipoFacturacion As Variant)

    m_vTipoFacturacion = vTipoFacturacion

End Property

Public Property Get FecPlanEntregaDesde() As Variant

    FecPlanEntregaDesde = m_vFecPlanEntregaDesde

End Property

Public Property Let FecPlanEntregaDesde(ByVal vFecPlanEntregaDesde As Variant)

    m_vFecPlanEntregaDesde = vFecPlanEntregaDesde

End Property

Public Property Get FecPlanEntregaHasta() As Variant

    FecPlanEntregaHasta = m_vFecPlanEntregaHasta

End Property

Public Property Let FecPlanEntregaHasta(ByVal vFecPlanEntregaHasta As Variant)

    m_vFecPlanEntregaHasta = vFecPlanEntregaHasta

End Property

Public Property Get AnyoFac() As Variant

    AnyoFac = m_vAnyoFac

End Property

Public Property Let AnyoFac(ByVal vAnyoFac As Variant)

    m_vAnyoFac = vAnyoFac

End Property

Public Property Get NumFac() As Variant

    NumFac = m_vNumFac

End Property

Public Property Let NumFac(ByVal vNumFac As Variant)

    m_vNumFac = vNumFac

End Property

Public Property Get EstFac() As Variant

    EstFac = m_vEstFac

End Property

Public Property Let EstFac(ByVal vEstFac As Variant)

    m_vEstFac = vEstFac

End Property

Public Property Get PagoNum() As Variant

    PagoNum = m_vPagoNum

End Property

Public Property Let PagoNum(ByVal vPagoNum As Variant)

    m_vPagoNum = vPagoNum

End Property

Public Property Get PagoEst() As Variant

    PagoEst = m_vPagoEst

End Property

Public Property Let PagoEst(ByVal vPagoEst As Variant)

    m_vPagoEst = vPagoEst

End Property

Public Property Get FecFacDesde() As Variant

    FecFacDesde = m_vFecFacDesde

End Property

Public Property Let FecFacDesde(ByVal vFecFacDesde As Variant)

    m_vFecFacDesde = vFecFacDesde

End Property

Public Property Get FecFacHasta() As Variant

    FecFacHasta = m_vFecFacHasta

End Property

Public Property Let FecFacHasta(ByVal vFecFacHasta As Variant)

    m_vFecFacHasta = vFecFacHasta

End Property

Public Property Get FecPagoDesde() As Variant

    FecPagoDesde = m_vFecPagoDesde

End Property

Public Property Let FecPagoDesde(ByVal vFecPagoDesde As Variant)

    m_vFecPagoDesde = vFecPagoDesde

End Property

Public Property Get FecPagoHasta() As Variant

    FecPagoHasta = m_vFecPagoHasta

End Property

Public Property Let FecPagoHasta(ByVal vFecPagoHasta As Variant)

    m_vFecPagoHasta = vFecPagoHasta

End Property

Public Property Get proceAnyo() As Variant

    proceAnyo = m_vProceAnyo

End Property

Public Property Let proceAnyo(ByVal vProceAnyo As Variant)

    m_vProceAnyo = vProceAnyo

End Property

Public Property Get ProceGmn1() As Variant

    ProceGmn1 = m_vProceGmn1

End Property

Public Property Let ProceGmn1(ByVal vProceGmn1 As Variant)

    m_vProceGmn1 = vProceGmn1

End Property

Public Property Get ProceCod() As Variant

    ProceCod = m_vProceCod

End Property

Public Property Let ProceCod(ByVal vProceCod As Variant)

    m_vProceCod = vProceCod

End Property

Public Property Get ProveAdj() As Variant

    ProveAdj = m_vProveAdj

End Property

Public Property Let ProveAdj(ByVal vProveAdj As Variant)

    m_vProveAdj = vProveAdj

End Property

Public Property Get Solicit() As Variant

    Solicit = m_vSolicit

End Property

Public Property Let Solicit(ByVal vSolicit As Variant)

    m_vSolicit = vSolicit

End Property

Friend Property Set Conexion(ByVal con As CConexion)
    Set m_oConexion = con
End Property

Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property

Public Property Let Indice(ByVal vIndice As Variant)
    m_vIndice = vIndice
End Property
Public Property Get Indice() As Variant
    Indice = m_vIndice
End Property

Private Function IBaseDatos_AnyadirABaseDatos() As TipoErrorSummit
Dim sConsulta As String
Dim TESError As TipoErrorSummit

'********* Precondicion *******************
If m_oConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CConfSegPedidos.AnyadirABAseDatos", "No se ha establecido la conexion"
    Exit Function
End If
'******************************************
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
    
    TESError.NumError = TESnoerror
    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
            

    sConsulta = "INSERT INTO CONF_SEG_PEDIDOS (USU, ABIERTO, ANYO, PEDIDO_NUM, ORDEN_NUM, EST, FEC_DESDE," _
    & "FEC_HASTA, NUM_PED_ERP, NUMEXT , TIPOPEDIDO, TIPO, ABONO, PED_ABIERTO_ANYO, PED_ABIERTO_PEDIDO_NUM," _
    & "PED_ABIERTO_ORDEN_NUM, PROVE, COD_PROVE_ERP, GMN1, GMN2, GMN3," _
    & "GMN4,ART_INT,DEST,FECENTREGA_DESDE,FECENTREGA_HASTA,EMPRESA,APROVISIONADOR,RECEPTOR,GESTOR,UON1_CC,UON2_CC,UON3_CC,UON4_CC," _
    & "CAT1,CAT2,CAT3,CAT4,CAT5,IM_AUTOFACTURA,RECEP_AUTOMATICA,TIPO_FACTURACION,FECPLANENTREGA_DESDE," _
    & "FECPLANENTREGA_HASTA,ANYO_FAC,NUM_FAC,EST_FAC,FECFACT_DESDE,FECFACT_HASTA,PAGO_NUM,PAGO_EST,FECPAGO_DESDE,FECPAGO_HASTA,PROCE_ANYO,PROCE_GMN1,PROCE_COD,PROVE_ADJ," _
    & "SOLICIT,PED_ABIERTO_FECINI,PED_ABIERTO_FECFIN,PED_ABIERTO_PORIMPORTE,PED_ABIERTO_PORCANTIDAD,IM_BLOQUEOFACT,VISTA_AVANZADA,VER_PEDIDO_DE_BAJA)"

    sConsulta = sConsulta & " VALUES ('" & DblQuote(m_vUsu) & "'," & BooleanToSQLBinary(m_vAbierto)
    sConsulta = sConsulta & "," & DblToSQLFloat(m_vAnyo)
    sConsulta = sConsulta & "," & DblToSQLFloat(m_vPedidoNum)
    sConsulta = sConsulta & "," & DblToSQLFloat(m_vOrdenNum)
    sConsulta = sConsulta & "," & DblToSQLFloat(m_vEst)
    sConsulta = sConsulta & "," & DateToSQLDate(m_vFecDesde)
    sConsulta = sConsulta & "," & DateToSQLDate(m_vFecHasta)
    sConsulta = sConsulta & "," & StrToSQLNULL(m_vNumPedErp)
    sConsulta = sConsulta & "," & StrToSQLNULL(m_vNumExt)
    sConsulta = sConsulta & "," & DblToSQLFloat(m_vTipoPedido)
    sConsulta = sConsulta & "," & StrToSQLNULL(m_vTipo)
    sConsulta = sConsulta & "," & DblToSQLFloat(m_vAbono)
    sConsulta = sConsulta & "," & DblToSQLFloat(m_vPedAbiertoAnyo)
    sConsulta = sConsulta & "," & DblToSQLFloat(m_vPedAbiertoPedidoNum)
    sConsulta = sConsulta & "," & DblToSQLFloat(m_vPedAbiertoOrdenNum)
    sConsulta = sConsulta & "," & StrToSQLNULL(m_vProve)
    sConsulta = sConsulta & "," & StrToSQLNULL(m_vCodProveErp)
    sConsulta = sConsulta & "," & StrToSQLNULL(m_vGMN1)
    sConsulta = sConsulta & "," & StrToSQLNULL(m_vGMN2)
    sConsulta = sConsulta & "," & StrToSQLNULL(m_vGMN3)
    sConsulta = sConsulta & "," & StrToSQLNULL(m_vGMN4)
    sConsulta = sConsulta & "," & StrToSQLNULL(m_vArtInt)
    sConsulta = sConsulta & "," & StrToSQLNULL(m_vDest)
    sConsulta = sConsulta & "," & DateToSQLDate(m_vFecEntregaDesde)
    sConsulta = sConsulta & "," & DateToSQLDate(m_vFecEntregaHasta)
    sConsulta = sConsulta & "," & DblToSQLFloat(m_vEmpresa)
    sConsulta = sConsulta & "," & StrToSQLNULL(m_vAprovisionador)
    sConsulta = sConsulta & "," & StrToSQLNULL(m_vReceptor)
    sConsulta = sConsulta & "," & StrToSQLNULL(m_vGestor)
    sConsulta = sConsulta & "," & StrToSQLNULL(m_vUON1)
    sConsulta = sConsulta & "," & StrToSQLNULL(m_vUON2)
    sConsulta = sConsulta & "," & StrToSQLNULL(m_vUON3)
    sConsulta = sConsulta & "," & StrToSQLNULL(m_vUON4)
    sConsulta = sConsulta & "," & DblToSQLFloat(m_vCat1)
    sConsulta = sConsulta & "," & DblToSQLFloat(m_vCat2)
    sConsulta = sConsulta & "," & DblToSQLFloat(m_vCat3)
    sConsulta = sConsulta & "," & DblToSQLFloat(m_vCat4)
    sConsulta = sConsulta & "," & DblToSQLFloat(m_vCat5)
    sConsulta = sConsulta & "," & DblToSQLFloat(m_vImAutofactura)
    sConsulta = sConsulta & "," & DblToSQLFloat(m_vRecepAutomatica)
    sConsulta = sConsulta & "," & DblToSQLFloat(m_vTipoFacturacion)
    sConsulta = sConsulta & "," & DateToSQLDate(m_vFecPlanEntregaDesde)
    sConsulta = sConsulta & "," & DateToSQLDate(m_vFecPlanEntregaHasta)
    sConsulta = sConsulta & "," & DblToSQLFloat(m_vAnyoFac)
    sConsulta = sConsulta & "," & StrToSQLNULL(m_vNumFac)
    sConsulta = sConsulta & "," & DblToSQLFloat(m_vEstFac)
    sConsulta = sConsulta & "," & DateToSQLDate(m_vFecFacDesde)
    sConsulta = sConsulta & "," & DateToSQLDate(m_vFecFacHasta)
    sConsulta = sConsulta & "," & StrToSQLNULL(m_vPagoNum)
    sConsulta = sConsulta & "," & DblToSQLFloat(m_vPagoEst)
    sConsulta = sConsulta & "," & DateToSQLDate(m_vFecPagoDesde)
    sConsulta = sConsulta & "," & DateToSQLDate(m_vFecPagoHasta)
    sConsulta = sConsulta & "," & DblToSQLFloat(m_vProceAnyo)
    sConsulta = sConsulta & "," & StrToSQLNULL(m_vProceGmn1)
    sConsulta = sConsulta & "," & DblToSQLFloat(m_vProceCod)
    sConsulta = sConsulta & "," & StrToSQLNULL(m_vProveAdj)
    sConsulta = sConsulta & "," & DblToSQLFloat(m_vSolicit)
    sConsulta = sConsulta & "," & DateToSQLDate(m_vPedAbiertoFecIni)
    sConsulta = sConsulta & "," & DateToSQLDate(m_vPedAbiertoFecFin)
    sConsulta = sConsulta & "," & DblToSQLFloat(m_vPedAbiertoPorImporte)
    sConsulta = sConsulta & "," & DblToSQLFloat(m_vPedAbiertoPorCantidad)
    sConsulta = sConsulta & "," & DblToSQLFloat(m_vImBloqueofactura)
    sConsulta = sConsulta & "," & BooleanToSQLBinary(m_bVistaAvanzada)
    sConsulta = sConsulta & "," & BooleanToSQLBinary(m_bPedBajaLogica)
    sConsulta = sConsulta & ")"
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    
    If ActualizarAtrBuscar.NumError <> TESnoerror Then
        GoTo Error_Cls
    End If
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    IBaseDatos_AnyadirABaseDatos = TESError
    Exit Function
    
Error_Cls:
    
    IBaseDatos_AnyadirABaseDatos = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
    On Error Resume Next
    If Not m_adores Is Nothing Then
        m_adores.Close
        Set m_adores = Nothing
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CConfSegPedidos", "IBaseDatos_AnyadirABaseDatos", ERR, Erl)
        GoTo Error_Cls
        Exit Function
    End If

End Function



Public Property Get PedAbiertoFecIni() As Variant

    PedAbiertoFecIni = m_vPedAbiertoFecIni

End Property

Public Property Let PedAbiertoFecIni(ByVal vPedAbiertoFecIni As Variant)

    m_vPedAbiertoFecIni = vPedAbiertoFecIni

End Property

Public Property Get PedAbiertoFecFin() As Variant

    PedAbiertoFecFin = m_vPedAbiertoFecFin

End Property

Public Property Let PedAbiertoFecFin(ByVal vPedAbiertoFecFin As Variant)

    m_vPedAbiertoFecFin = vPedAbiertoFecFin

End Property

Public Property Get PedAbiertoPorImporte() As Variant

    PedAbiertoPorImporte = m_vPedAbiertoPorImporte

End Property

Public Property Let PedAbiertoPorImporte(ByVal vPedAbiertoPorImporte As Variant)

    m_vPedAbiertoPorImporte = vPedAbiertoPorImporte

End Property

Public Property Get PedAbiertoPorCantidad() As Variant

    PedAbiertoPorCantidad = m_vPedAbiertoPorCantidad

End Property

Public Property Let PedAbiertoPorCantidad(ByVal vPedAbiertoPorCantidad As Variant)

    m_vPedAbiertoPorCantidad = vPedAbiertoPorCantidad

End Property

Private Function IBaseDatos_ComprobarExistenciaEnBaseDatos() As Boolean
    Dim sConsulta As String
    Dim AdoRes As New adodb.Recordset
    
    '******************* Precondicion *******************
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CConfVisor.ComprobarExistenciaEnBaseDatos", "No se ha establecido la conexion"
        Exit Function
    End If
    '****************************************************
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
    sConsulta = "SELECT COUNT(*) FROM CONF_SEG_PEDIDOS WITH (NOLOCK) WHERE USU='" & DblQuote(m_vUsu) & "' AND ABIERTO=" & m_vAbierto
        
    Set AdoRes = New adodb.Recordset
    AdoRes.CursorLocation = adUseServer
    AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    If AdoRes.eof Then
        IBaseDatos_ComprobarExistenciaEnBaseDatos = False
    Else
        If AdoRes.Fields(0).Value > 0 Then
            IBaseDatos_ComprobarExistenciaEnBaseDatos = True
        Else
            IBaseDatos_ComprobarExistenciaEnBaseDatos = False
        End If
    End If

    AdoRes.Close
    Set AdoRes = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CConfSegPedidos", "IBaseDatos_ComprobarExistenciaEnBaseDatos", ERR, Erl)
        Exit Function
    End If
End Function

Private Function IBaseDatos_EliminarDeBaseDatos() As TipoErrorSummit
Dim sConsulta As String
Dim TESError As TipoErrorSummit
    '''Precondicion
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "cConfVistaVisorSol.EliminarDeBaseDatos", "No se ha establecido la conexion"
        Exit Function
    End If
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
    
If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls

    TESError.NumError = TESnoerror
     
    sConsulta = "DELETE FROM CONF_SEG_PEDIDOS WHERE USU='" & DblQuote(m_vUsu) & "'"
    
    
    m_oConexion.ADOCon.Execute sConsulta

    IBaseDatos_EliminarDeBaseDatos = TESError

    Exit Function

Error_Cls:
    IBaseDatos_EliminarDeBaseDatos = basErrores.TratarError(m_oConexion.ADOCon.Errors)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CConfSegPedidos", "IBaseDatos_EliminarDeBaseDatos", ERR, Erl)
        GoTo Error_Cls
        Exit Function
    End If

End Function

Private Function IBaseDatos_CambiarCodigo(CodigoNuevo As Variant) As TipoErrorSummit

End Function

Private Function IBaseDatos_FinalizarEdicionEliminando() As TipoErrorSummit

End Function

Private Function IBaseDatos_IniciarEdicion(Optional ByVal Bloquear As Boolean, Optional ByVal UsuarioBloqueo As String) As TipoErrorSummit

End Function


Private Function IBaseDatos_FinalizarEdicionModificando() As TipoErrorSummit
Dim TESError As TipoErrorSummit
Dim sConsulta As String

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls
    TESError.NumError = TESnoerror

    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"

    
    sConsulta = "UPDATE CONF_SEG_PEDIDOS SET "
    sConsulta = sConsulta & " ANYO = " & DblToSQLFloat(m_vAnyo)
    sConsulta = sConsulta & " ,PEDIDO_NUM = " & DblToSQLFloat(m_vPedidoNum)
    sConsulta = sConsulta & " ,ORDEN_NUM = " & DblToSQLFloat(m_vOrdenNum)
    sConsulta = sConsulta & " ,EST = " & DblToSQLFloat(m_vEst)
    sConsulta = sConsulta & " ,FEC_DESDE = " & DateToSQLDate(m_vFecDesde)
    sConsulta = sConsulta & " ,FEC_HASTA = " & DateToSQLDate(m_vFecHasta)
    sConsulta = sConsulta & " ,NUM_PED_ERP = " & StrToSQLNULL(m_vNumPedErp)
    sConsulta = sConsulta & " ,NUMEXT = " & StrToSQLNULL(m_vNumExt)
    sConsulta = sConsulta & " ,TIPOPEDIDO = " & DblToSQLFloat(m_vTipoPedido)
    sConsulta = sConsulta & " ,TIPO = " & StrToSQLNULL(m_vTipo)
    sConsulta = sConsulta & " ,ABONO = " & DblToSQLFloat(m_vAbono)
    sConsulta = sConsulta & " ,PED_ABIERTO_ANYO = " & DblToSQLFloat(m_vPedAbiertoAnyo)
    sConsulta = sConsulta & " ,PED_ABIERTO_PEDIDO_NUM = " & DblToSQLFloat(m_vPedAbiertoPedidoNum)
    sConsulta = sConsulta & " ,PED_ABIERTO_ORDEN_NUM = " & DblToSQLFloat(m_vPedAbiertoOrdenNum)
    sConsulta = sConsulta & " ,PROVE = " & StrToSQLNULL(m_vProve)
    sConsulta = sConsulta & " ,COD_PROVE_ERP = " & StrToSQLNULL(m_vCodProveErp)
    sConsulta = sConsulta & " ,GMN1 = " & StrToSQLNULL(m_vGMN1)
    sConsulta = sConsulta & " ,GMN2 = " & StrToSQLNULL(m_vGMN2)
    sConsulta = sConsulta & " ,GMN3 = " & StrToSQLNULL(m_vGMN3)
    sConsulta = sConsulta & " ,GMN4 = " & StrToSQLNULL(m_vGMN4)
    sConsulta = sConsulta & " ,ART_INT = " & StrToSQLNULL(m_vArtInt)
    sConsulta = sConsulta & " ,DEST = " & StrToSQLNULL(m_vDest)
    sConsulta = sConsulta & " ,FECENTREGA_DESDE = " & DateToSQLDate(m_vFecEntregaDesde)
    sConsulta = sConsulta & " ,FECENTREGA_HASTA = " & DateToSQLDate(m_vFecEntregaHasta)
    sConsulta = sConsulta & " ,EMPRESA = " & DblToSQLFloat(m_vEmpresa)
    sConsulta = sConsulta & " ,APROVISIONADOR = " & StrToSQLNULL(m_vAprovisionador)
    sConsulta = sConsulta & " ,RECEPTOR = " & StrToSQLNULL(m_vReceptor)
    sConsulta = sConsulta & " ,GESTOR = " & StrToSQLNULL(m_vGestor)
    sConsulta = sConsulta & " ,UON1_CC = " & StrToSQLNULL(m_vUON1)
    sConsulta = sConsulta & " ,UON2_CC = " & StrToSQLNULL(m_vUON2)
    sConsulta = sConsulta & " ,UON3_CC = " & StrToSQLNULL(m_vUON3)
    sConsulta = sConsulta & " ,UON4_CC = " & StrToSQLNULL(m_vUON4)
    sConsulta = sConsulta & " ,CAT1 = " & DblToSQLFloat(m_vCat1)
    sConsulta = sConsulta & " ,CAT2 = " & DblToSQLFloat(m_vCat2)
    sConsulta = sConsulta & " ,CAT3 = " & DblToSQLFloat(m_vCat3)
    sConsulta = sConsulta & " ,CAT4 = " & DblToSQLFloat(m_vCat4)
    sConsulta = sConsulta & " ,CAT5 = " & DblToSQLFloat(m_vCat5)
    sConsulta = sConsulta & " ,IM_AUTOFACTURA = " & DblToSQLFloat(m_vImAutofactura)
    sConsulta = sConsulta & " ,RECEP_AUTOMATICA = " & DblToSQLFloat(m_vRecepAutomatica)
    sConsulta = sConsulta & " ,TIPO_FACTURACION = " & DblToSQLFloat(m_vTipoFacturacion)
    sConsulta = sConsulta & " ,FECPLANENTREGA_DESDE = " & DateToSQLDate(m_vFecPlanEntregaDesde)
    sConsulta = sConsulta & " ,FECPLANENTREGA_HASTA = " & DateToSQLDate(m_vFecPlanEntregaHasta)
    sConsulta = sConsulta & " ,ANYO_FAC = " & DblToSQLFloat(m_vAnyoFac)
    sConsulta = sConsulta & " ,NUM_FAC = " & StrToSQLNULL(m_vNumFac)
    sConsulta = sConsulta & " ,EST_FAC = " & DblToSQLFloat(m_vEstFac)
    sConsulta = sConsulta & " ,FECFACT_DESDE = " & DateToSQLDate(m_vFecFacDesde)
    sConsulta = sConsulta & " ,FECFACT_HASTA = " & DateToSQLDate(m_vFecFacHasta)
    sConsulta = sConsulta & " ,PAGO_NUM = " & StrToSQLNULL(m_vPagoNum)
    sConsulta = sConsulta & " ,PAGO_EST = " & DblToSQLFloat(m_vPagoEst)
    sConsulta = sConsulta & " ,FECPAGO_DESDE = " & DateToSQLDate(m_vFecPagoDesde)
    sConsulta = sConsulta & " ,FECPAGO_HASTA = " & DateToSQLDate(m_vFecPagoHasta)
    sConsulta = sConsulta & " ,PROCE_ANYO = " & DblToSQLFloat(m_vProceAnyo)
    sConsulta = sConsulta & " ,PROCE_GMN1 = " & StrToSQLNULL(m_vProceGmn1)
    sConsulta = sConsulta & " ,PROCE_COD = " & DblToSQLFloat(m_vProceCod)
    sConsulta = sConsulta & " ,PROVE_ADJ = " & StrToSQLNULL(m_vProveAdj)
    sConsulta = sConsulta & " ,SOLICIT = " & DblToSQLFloat(m_vSolicit)
    sConsulta = sConsulta & " ,PED_ABIERTO_FECINI = " & DateToSQLDate(m_vPedAbiertoFecIni)
    sConsulta = sConsulta & " ,PED_ABIERTO_FECFIN = " & DateToSQLDate(m_vPedAbiertoFecFin)
    sConsulta = sConsulta & " ,PED_ABIERTO_PORIMPORTE = " & DblToSQLFloat(m_vPedAbiertoPorImporte)
    sConsulta = sConsulta & " ,PED_ABIERTO_PORCANTIDAD = " & DblToSQLFloat(m_vPedAbiertoPorCantidad)
    sConsulta = sConsulta & " ,IM_BLOQUEOFACT = " & DblToSQLFloat(m_vImBloqueofactura)
    sConsulta = sConsulta & " ,VISTA_AVANZADA = " & BooleanToSQLBinary(m_bVistaAvanzada)
    sConsulta = sConsulta & " ,VER_PEDIDO_DE_BAJA = " & BooleanToSQLBinary(m_bPedBajaLogica)
    sConsulta = sConsulta & " WHERE USU='" & DblQuote(m_vUsu) & "' AND ABIERTO=" & BooleanToSQLBinary(m_vAbierto)
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    
    If ActualizarAtrBuscar.NumError <> TESnoerror Then
        GoTo Error_Cls
    End If
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    
    IBaseDatos_FinalizarEdicionModificando = TESError

    Exit Function

Error_Cls:
    IBaseDatos_FinalizarEdicionModificando = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"

    On Error Resume Next
    If Not m_adores Is Nothing Then
        m_adores.Close
        Set m_adores = Nothing
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CConfSegPedidos", "IBaseDatos_FinalizarEdicionModificando", ERR, Erl)
        GoTo Error_Cls
        Exit Function
    End If

End Function

Private Function ActualizarAtrBuscar() As TipoErrorSummit
Dim sConsulta As String
Dim oAtrib As CAtributoBuscar
Dim sCampo As String
Dim vValor As Variant
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls
ActualizarAtrBuscar.NumError = TESnoerror
sConsulta = "DELETE FROM CONF_SEG_PEDIDOS_ATRIB WHERE USU = " & StrToSQLNULL(m_vUsu) & " AND ABIERTO = " & BooleanToSQLBinary(m_vAbierto)
m_oConexion.ADOCon.Execute sConsulta
If Not m_oAtrBuscar Is Nothing Then
    For Each oAtrib In m_oAtrBuscar
        sCampo = ""
        vValor = ""
        Select Case oAtrib.Tipo
            Case TipoNumerico
                sCampo = "VALOR_NUM"
                vValor = DblToSQLFloat(oAtrib.Valor)
            Case TipoString, TipoTextoCorto, TipoTextoMedio, TipoTextoLargo
                sCampo = "VALOR_TEXT"
                vValor = StrToSQLNULL(oAtrib.Valor)
            Case TipoBoolean
                sCampo = "VALOR_BOOL"
                vValor = BooleanToSQLBinary(oAtrib.Valor)
            Case TipoFecha
                sCampo = "VALOR_FEC"
                vValor = DateToSQLDateNull(oAtrib.Valor)
        End Select
        If IsNull(vValor) Then
            vValor = "NULL"
        End If
        'Grabamos en la tabla cada atributo
        sConsulta = "INSERT INTO CONF_SEG_PEDIDOS_ATRIB (USU, ATRIB, " & sCampo & ",OPERADOR, ABIERTO, POSICION, AMBITO) VALUES (" & StrToSQLNULL(m_vUsu) & _
                    "," & DblToSQLFloat(oAtrib.IdAtrib) & "," & vValor & "," & StrToSQLNULL(oAtrib.Operador) & "," & _
                    BooleanToSQLBinary(m_vAbierto) & "," & oAtrib.Posicion & "," & oAtrib.AmbitoBuscar & ")"
        m_oConexion.ADOCon.Execute sConsulta
            
    Next
End If
Exit Function
Error_Cls:
    ActualizarAtrBuscar = basErrores.TratarError(m_oConexion.ADOCon.Errors)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CConfSegPedidos", "ActualizarAtrBuscar", ERR, Erl)
        GoTo Error_Cls
        Exit Function
    End If
End Function

Public Sub CargarAtrBuscar()
Dim sConsulta As String
Dim AdoRes As New adodb.Recordset
Dim vValor As Variant
Dim vMax As Variant
Dim vMin As Variant
Dim oAtributo As CAtributoBuscar
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
sConsulta = "SELECT D.LISTA_EXTERNA, D.ID IDATRIB, D.COD, D.DEN, D.TIPO, D.INTRO, D.MINNUM, D.MAXNUM, D.MINFEC, D.MAXFEC, C.VALOR_NUM, C.VALOR_TEXT," & _
            " C.VALOR_FEC, C.VALOR_BOOL, C.OPERADOR, C.AMBITO,C.POSICION FROM CONF_SEG_PEDIDOS_ATRIB C WITH (NOLOCK) " & _
            "INNER JOIN DEF_ATRIB D WITH (NOLOCK) ON D.ID = C.ATRIB WHERE USU = " & StrToSQLNULL(m_vUsu) & _
            " AND ABIERTO = " & BooleanToSQLBinary(m_vAbierto) & " ORDER BY C.POSICION"
AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
Do While Not AdoRes.eof
    Select Case AdoRes("TIPO").Value
        Case TipoNumerico
            vValor = AdoRes("VALOR_NUM").Value
            vMax = AdoRes("MAXNUM").Value
            vMin = AdoRes("MINNUM").Value
        Case TipoString, TipoTextoCorto, TipoTextoMedio, TipoTextoLargo
            vValor = AdoRes("VALOR_TEXT").Value
        Case TipoBoolean
            vValor = AdoRes("VALOR_BOOL").Value
        Case TipoFecha
            vValor = AdoRes("VALOR_FEC").Value
            vMax = AdoRes("MAXFEC").Value
            vMin = AdoRes("MINFEC").Value
    End Select
    Set oAtributo = m_oAtrBuscar.Add(AdoRes("IDATRIB").Value, AdoRes("COD").Value, AdoRes("DEN").Value, AdoRes("TIPO").Value, _
                   vValor, AdoRes("INTRO").Value, vMin, vMax, NullToStr(AdoRes("OPERADOR").Value), NullToDbl0(AdoRes("AMBITO").Value), _
                   NullToDbl0(AdoRes("LISTA_EXTERNA").Value), NullToDbl0(AdoRes("POSICION").Value))
                   
    AdoRes.MoveNext
Loop
AdoRes.Close
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CConfSegPedidos", "CargarAtrBuscar", ERR, Erl)
        Exit Sub
    End If
End Sub

Private Sub Class_Terminate()
On Error GoTo Error_Cls:
    
    Set m_oConexion = Nothing
    Exit Sub
Error_Cls:
    Resume Next
End Sub

Public Property Let HayCambios(ByVal dato As Boolean)
    m_bHayCambios = dato
End Property

Public Property Get HayCambios() As Boolean
    HayCambios = m_bHayCambios
End Property

Private Sub IBaseDatos_CancelarEdicion()

End Sub


Public Property Get USU() As Variant

    USU = m_vUsu

End Property

Public Property Let USU(ByVal vUsu As Variant)

    m_vUsu = vUsu

End Property

Public Property Get Cat1() As Variant

    Cat1 = m_vCat1

End Property

Public Property Let Cat1(ByVal vCat1 As Variant)

    m_vCat1 = vCat1

End Property

Public Property Get Cat2() As Variant

    Cat2 = m_vCat2

End Property

Public Property Let Cat2(ByVal vCat2 As Variant)

    m_vCat2 = vCat2

End Property

Public Property Get Cat3() As Variant

    Cat3 = m_vCat3

End Property

Public Property Let Cat3(ByVal vCat3 As Variant)

    m_vCat3 = vCat3

End Property

Public Property Get Cat4() As Variant

    Cat4 = m_vCat4

End Property

Public Property Let Cat4(ByVal vCat4 As Variant)

    m_vCat4 = vCat4

End Property

Public Property Get Cat5() As Variant

    Cat5 = m_vCat5

End Property

Public Property Let Cat5(ByVal vCat5 As Variant)

    m_vCat5 = vCat5

End Property

Public Property Get UON1() As Variant

    UON1 = m_vUON1

End Property

Public Property Let UON1(ByVal vUON1 As Variant)

    m_vUON1 = vUON1

End Property

Public Property Get UON2() As Variant

    UON2 = m_vUON2

End Property

Public Property Let UON2(ByVal vUON2 As Variant)

    m_vUON2 = vUON2

End Property

Public Property Get UON3() As Variant

    UON3 = m_vUON3

End Property

Public Property Let UON3(ByVal vUON3 As Variant)

    m_vUON3 = vUON3

End Property

Public Property Get UON4() As Variant

    UON4 = m_vUON4

End Property

Public Property Let UON4(ByVal vUON4 As Variant)

    m_vUON4 = vUON4

End Property

''' <summary>Rellena las propiedades del objeto con los datos por defecto</summary>
''' <remarks>Llamada desde frmSeguimiento.CargarConfiguracionSegPedidos</remarks>

Public Sub DatosPorDecto(ByVal vAbierto As Variant)
    m_vAnyo = Null
    m_vAbierto = vAbierto
    m_vPedidoNum = Null
    m_vOrdenNum = Null
    m_vEst = Null
    m_vFecDesde = Null
    m_vFecHasta = Null
    m_vNumPedErp = Null
    m_vNumExt = Null
    m_vTipoPedido = Null
    m_vTipo = Null
    m_vAbono = Null
    m_vPedAbiertoAnyo = Null
    m_vPedAbiertoPedidoNum = Null
    m_vPedAbiertoOrdenNum = Null
    m_vProve = Null
    m_vCodProveErp = Null
    m_vGMN1 = Null
    m_vGMN2 = Null
    m_vGMN3 = Null
    m_vGMN4 = Null
    m_vArtInt = Null
    m_vDest = Null
    m_vFecEntregaDesde = Null
    m_vFecEntregaHasta = Null
    m_vEmpresa = Null
    m_vReceptor = Null
    m_vAprovisionador = Null
    m_vGestor = Null
    m_vUON1 = Null
    m_vUON2 = Null
    m_vUON3 = Null
    m_vUON4 = Null
    m_vImAutofactura = Null
    m_vRecepAutomatica = Null
    m_vTipoFacturacion = Null
    m_vFecPlanEntregaDesde = Null
    m_vFecPlanEntregaHasta = Null
    m_vAnyoFac = Null
    m_vNumFac = Null
    m_vEstFac = Null
    m_vPagoNum = Null
    m_vPagoEst = Null
    m_vFecFacDesde = Null
    m_vFecFacHasta = Null
    m_vFecPagoDesde = Null
    m_vFecPagoHasta = Null
    m_vProceAnyo = Null
    m_vProceGmn1 = Null
    m_vProceCod = Null
    m_vProveAdj = Null
    m_vSolicit = Null
    m_vPedAbiertoFecIni = Null
    m_vPedAbiertoFecFin = Null
    m_vPedAbiertoPorImporte = Null
    m_vPedAbiertoPorCantidad = Null
    Set m_oAtrBuscar = Nothing
    m_bVistaAvanzada = False
    m_bPedBajaLogica = False
End Sub

Public Property Get VerPedBajaLogica() As Boolean

    VerPedBajaLogica = m_bPedBajaLogica

End Property

Public Property Let VerPedBajaLogica(ByVal BajaLogica As Boolean)

    m_bPedBajaLogica = BajaLogica

End Property
