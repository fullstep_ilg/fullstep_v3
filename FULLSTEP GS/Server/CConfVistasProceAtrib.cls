VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CConfVistasProceAtrib"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'********************* CConfVistasProceAtrib **********************************
'*             Autor : Mertxe Martin
'*             Creada : 19/02/2002
'****************************************************************

Private mCol As Collection
Private m_oConexion As CConexion
Private m_oConexionServer As CConexionDeUseServer
Private m_bEOF As Boolean

Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum

Public Function Add(ByVal oConfVistaProc As CConfVistaProce, ByVal lAtrib As Long, ByVal Ambito As Integer, ByVal Cod As String, ByVal Den As String, Optional ByVal bVisible As Boolean, Optional ByVal iPos As Integer, Optional ByVal dblWidth As Double, Optional ByVal vIndice As Variant) As CConfVistaProceAtrib
    
    'create a new object
    Dim sCodGmn1 As String
    Dim objnewmember As CConfVistaProceAtrib
    
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New CConfVistaProceAtrib
   
    Set objnewmember.ConfVistaProc = oConfVistaProc
    
    objnewmember.Posicion = iPos
    objnewmember.Visible = bVisible
    objnewmember.Width = dblWidth
    objnewmember.Atributo = lAtrib
    objnewmember.Ambito = Ambito
    
    objnewmember.CodAtributo = Cod
    objnewmember.DenAtributo = Den
    
    Set objnewmember.Conexion = m_oConexion
           
    If Not IsMissing(vIndice) And Not IsNull(vIndice) Then
        objnewmember.Indice = vIndice
        mCol.Add objnewmember, CStr(vIndice)
    Else
       sCodGmn1 = oConfVistaProc.Proceso.GMN1Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oConfVistaProc.Proceso.GMN1Cod))
'''       mCol.Add objnewmember, CStr(oConfVistaProc.Proceso.Anyo) & sCodGmn1 & CStr(oConfVistaProc.Proceso.Cod) & CStr(sProv)
       mCol.Add objnewmember, CStr(lAtrib)
    End If
    
    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CConfVistasProceAtrib", "Add", ERR, Erl)
      Exit Function
   End If

End Function

Friend Property Set Conexion(ByVal con As CConexion)
    Set m_oConexion = con
End Property

Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property

Friend Property Set ConexionServer(ByVal con As CConexionDeUseServer)
    Set m_oConexionServer = con
End Property

Friend Property Get ConexionServer() As CConexionDeUseServer
    Set ConexionServer = m_oConexionServer
End Property

Public Property Get Count() As Long
    If mCol Is Nothing Then
        Count = 0
    Else
         Count = mCol.Count
    End If
End Property

Public Property Get eof() As Boolean
    eof = m_bEOF
End Property

Friend Property Let eof(ByVal b As Boolean)
    m_bEOF = b
End Property

Public Property Get Item(vntIndexKey As Variant) As CConfVistaProceAtrib
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
     Set NewEnum = mCol.[_NewEnum]
End Property

Public Sub Remove(vntIndexKey As Variant)

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    mCol.Remove vntIndexKey

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CConfVistasProceAtrib", "Remove", ERR, Erl)
      Exit Sub
   End If

End Sub

Private Sub Class_Initialize()
    Set mCol = New Collection
End Sub

Private Sub Class_Terminate()
    On Error Resume Next
    Set mCol = Nothing
    Set m_oConexion = Nothing
End Sub





