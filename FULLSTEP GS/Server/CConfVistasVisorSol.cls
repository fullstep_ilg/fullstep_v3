VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CConfVistasVisorSol"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private mCol As Collection
Private m_oConexion As CConexion
Private m_oConexionServer As CConexionDeUseServer
Private m_bEOF As Boolean

Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum

''' <summary>
''' A�ade la configuraci�n de solicitudes de un usuario al objeto CConfVistaVisorSol
''' </summary>
''' <param name="sUsuario">Cod del usuario</param>
''' <param name="ConfigVisorSol">Configuraci�n del visor de solicitudes</param>
''' <param name="vIndice">si se usa indice o no</param>
''' <returns>Un objeto CConfVistaVisorSol</returns>
''' <remarks>Llamada desde:CargarConfVisorSol   frmSolicitudes/CargarConfiguracionVisorSol; Tiempo m�ximo:0,1</remarks>
Public Function Add(ByVal sUsuario As String, ConfigVisorSol As TipoConfigVisorSol, Optional ByVal vIndice As Variant) As CConfVistaVisorSol
    
    'create a new object
    Dim objnewmember As CConfVistaVisorSol
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New CConfVistaVisorSol
    
    objnewmember.Usuario = sUsuario
    
    objnewmember.TipoWidth = ConfigVisorSol.dblTipoWidth
    objnewmember.AltaWidth = ConfigVisorSol.dblAltaWidth
    objnewmember.NecesidadWidth = ConfigVisorSol.dblNecesidadWidth
    objnewmember.IdentWidth = ConfigVisorSol.dblIdentWidth
    objnewmember.DenominacionWidth = ConfigVisorSol.dblDenominacionWidth
    objnewmember.ImporteWidth = ConfigVisorSol.dblImporteWidth
    objnewmember.PeticionarioWidth = ConfigVisorSol.dblPeticionarioWidth
    objnewmember.UONWidth = ConfigVisorSol.dblUONWidth
    objnewmember.EstadoWidth = ConfigVisorSol.dblEstadoWidth
    objnewmember.AsignadoAWidth = ConfigVisorSol.dblAsignadoAWidth
    objnewmember.DepartamentoWidth = ConfigVisorSol.dblDepartamentoWidth
    objnewmember.SinProcesoWidth = ConfigVisorSol.dblSinProcesoWidth
    objnewmember.SinPedidoWidth = ConfigVisorSol.dblSinPedidoWidth
    objnewmember.SinIntegracionWidth = ConfigVisorSol.dblSinIntegracionWidth
    objnewmember.NumSolErpWidth = ConfigVisorSol.dblNumSolErpWidth
        
    objnewmember.Tipo = ConfigVisorSol.vTipo
    objnewmember.Identificador = ConfigVisorSol.vIdentificador
    objnewmember.Descripcion = ConfigVisorSol.vDescripcion
    objnewmember.Peticionario = ConfigVisorSol.vPeticionario
    objnewmember.Desde = ConfigVisorSol.vDesde
    objnewmember.Hasta = ConfigVisorSol.vHasta
    objnewmember.UON1 = ConfigVisorSol.vUON1
    objnewmember.UON2 = ConfigVisorSol.vUON2
    objnewmember.UON3 = ConfigVisorSol.vUON3
    objnewmember.EnCursoAprobacion = ConfigVisorSol.vEnCursoAprobacion
    objnewmember.Pendientes = ConfigVisorSol.vPendientes
    objnewmember.Aprobadas = ConfigVisorSol.vAprobadas
    objnewmember.Rechazadas = ConfigVisorSol.vRechazadas
    objnewmember.Anuladas = ConfigVisorSol.vAnuladas
    objnewmember.Cerradas = ConfigVisorSol.vCerradas
    objnewmember.Departamento = ConfigVisorSol.sDepartamento
    objnewmember.Empresa = ConfigVisorSol.lEmpresa
    objnewmember.ImporteDesde = ConfigVisorSol.dImporteDesde
    objnewmember.ImporteHasta = ConfigVisorSol.dImporteHasta
    objnewmember.CodArticulo = ConfigVisorSol.sCodArticulo
    objnewmember.DenArticulo = ConfigVisorSol.sDenArticulo
    objnewmember.GMN1 = ConfigVisorSol.sGMN1
    objnewmember.GMN2 = ConfigVisorSol.sGMN2
    objnewmember.GMN3 = ConfigVisorSol.sGMN3
    objnewmember.GMN4 = ConfigVisorSol.sGMN4
    objnewmember.Comprador = ConfigVisorSol.sComprador
    objnewmember.AlertaSinProceso = ConfigVisorSol.bAlertaSinProceso
    objnewmember.AlertaSinPedido = ConfigVisorSol.bAlertaSinPedido
    objnewmember.AlertaIntegracion = ConfigVisorSol.bAlertaIntegracion
    objnewmember.UON1_CC = ConfigVisorSol.sUON1_CC
    objnewmember.UON2_CC = ConfigVisorSol.sUON2_CC
    objnewmember.UON3_CC = ConfigVisorSol.sUON3_CC
    objnewmember.UON4_CC = ConfigVisorSol.sUON4_CC
    objnewmember.Proveedor = ConfigVisorSol.Proveedor
       
    Set objnewmember.Conexion = m_oConexion
    Set objnewmember.ConexionServer = m_oConexionServer
       
    If Not IsMissing(vIndice) And Not IsNull(vIndice) Then
        objnewmember.Indice = vIndice
        mCol.Add objnewmember, CStr(vIndice)
    Else
        mCol.Add objnewmember, CStr(sUsuario)
    End If
    
    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CConfVistasVisorSol", "Add", ERR, Erl)
        Exit Function
    End If

End Function

Friend Property Set Conexion(ByVal con As CConexion)
    Set m_oConexion = con
End Property

Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property

Friend Property Set ConexionServer(ByVal con As CConexionDeUseServer)
    Set m_oConexionServer = con
End Property

Friend Property Get ConexionServer() As CConexionDeUseServer
    Set ConexionServer = m_oConexionServer
End Property

Public Property Get Count() As Long
    If mCol Is Nothing Then
        Count = 0
    Else
         Count = mCol.Count
    End If
End Property

Public Property Get eof() As Boolean
    eof = m_bEOF
End Property

Friend Property Let eof(ByVal b As Boolean)
    m_bEOF = b
End Property

Public Property Get Item(vntIndexKey As Variant) As CConfVistaVisorSol
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
     Set NewEnum = mCol.[_NewEnum]
End Property

Public Sub Remove(vntIndexKey As Variant)
On Error GoTo ERROR

    mCol.Remove vntIndexKey

ERROR:

End Sub

Private Sub Class_Initialize()
    Set mCol = New Collection
End Sub

Private Sub Class_Terminate()
    'Proceroys collection when this class is terminated
    On Error Resume Next
    Set mCol = Nothing
    Set m_oConexion = Nothing
End Sub


''' <summary>
''' Cargar todos los campos de configuraci�n del visor de solicitudes de un usuario
''' </summary>
''' <param name="sUsuario">Cod del usuario</param>
''' <param name="vIndice">si se usa indice o no</param>
''' <remarks>Llamada desde:frmSolicitudes.CargarConfiguracionVisorSol; Tiempo m�ximo:1</remarks>
''' Revisado por: jbg; Fecha: 07/02/2013
Public Sub CargarConfVisorSol(ByVal sUsuario As String, Optional ByVal UsarIndice As Boolean)
    Dim AdoRes As New adodb.Recordset
    Dim sConsulta As String
    Dim lIndice As Long
    Dim TpConfVisorSol As TipoConfigVisorSol
    
    
    '********* Precondicion **************************************
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CConfVistasVisor.CargarConfVisor", "No se ha establecido la conexion"
        Exit Sub
    End If
    '*************************************************************
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

    sConsulta = "SELECT USU,TIPO_WIDTH, ALTA_WIDTH, NECESIDAD_WIDTH, IDENT_WIDTH, DENOMINACION_WIDTH, IMPORTE_WIDTH," _
    & "PETICIONARIO_WIDTH, UON_WIDTH, ESTADO_WIDTH , ASIGNADOA_WIDTH, DEP_WIDTH, SIN_PROCESO_WIDTH, SIN_PEDIDO_WIDTH, SIN_INTEGRACION_WIDTH," _
    & "TIPO, IDENTIFICADOR, DESCRIPCION, PETICIONARIO, DESDE, HASTA," _
    & "UON1,UON2,UON3,ENCURSOAPROBACION,PENDIENTES,APROBADAS,RECHAZADAS,ANULADAS,CERRADAS,DEPARTAMENTO,EMPRESA,IMPORTE_DESDE,IMPORTE_HASTA,COD_ART," _
    & "DEN_ART,GMN1,GMN2,GMN3,GMN4,COMPRADOR,ALERTA_SINPROC,ALERTA_SINPED,ALERTA_INT,UON1_CC,UON2_CC,UON3_CC,UON4_CC,PROVE, NUM_SOL_ERP_WIDTH " _
    & "FROM CONF_VISOR_SOL WITH (NOLOCK) WHERE USU='" & DblQuote(sUsuario) & "'"
    AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly

    If AdoRes.eof Then
        AdoRes.Close
        Set AdoRes = Nothing
        If mCol Is Nothing Then
            Set mCol = Nothing
            Set mCol = New Collection
        End If
        Exit Sub

    Else
        If mCol Is Nothing Then
            Set mCol = Nothing
            Set mCol = New Collection
        End If
        
        TpConfVisorSol.dblTipoWidth = AdoRes.Fields("TIPO_WIDTH").Value
        TpConfVisorSol.dblAltaWidth = AdoRes.Fields("ALTA_WIDTH").Value
        TpConfVisorSol.dblNecesidadWidth = AdoRes.Fields("NECESIDAD_WIDTH").Value
        TpConfVisorSol.dblIdentWidth = AdoRes.Fields("IDENT_WIDTH").Value
        TpConfVisorSol.dblDenominacionWidth = AdoRes.Fields("DENOMINACION_WIDTH").Value
        TpConfVisorSol.dblImporteWidth = AdoRes.Fields("IMPORTE_WIDTH").Value
        TpConfVisorSol.dblPeticionarioWidth = AdoRes.Fields("PETICIONARIO_WIDTH").Value
        TpConfVisorSol.dblUONWidth = AdoRes.Fields("UON_WIDTH").Value
        TpConfVisorSol.dblEstadoWidth = AdoRes.Fields("ESTADO_WIDTH").Value
        TpConfVisorSol.dblAsignadoAWidth = AdoRes.Fields("ASIGNADOA_WIDTH").Value
        If IsNull(AdoRes.Fields("DEP_WIDTH").Value) Then
            TpConfVisorSol.dblDepartamentoWidth = 1400
        Else
            TpConfVisorSol.dblDepartamentoWidth = AdoRes.Fields("DEP_WIDTH").Value
        End If
        If IsNull(AdoRes.Fields("SIN_PROCESO_WIDTH").Value) Then
            TpConfVisorSol.dblSinProcesoWidth = 300
        Else
            TpConfVisorSol.dblSinProcesoWidth = AdoRes.Fields("SIN_PROCESO_WIDTH").Value
        End If
        If IsNull(AdoRes.Fields("SIN_PEDIDO_WIDTH").Value) Then
            TpConfVisorSol.dblSinPedidoWidth = 300
        Else
            TpConfVisorSol.dblSinPedidoWidth = AdoRes.Fields("SIN_PEDIDO_WIDTH").Value
        End If
        If IsNull(AdoRes.Fields("SIN_INTEGRACION_WIDTH").Value) Then
            TpConfVisorSol.dblSinIntegracionWidth = 300
        Else
            TpConfVisorSol.dblSinIntegracionWidth = AdoRes.Fields("SIN_INTEGRACION_WIDTH").Value
        End If
        
        If IsNull(AdoRes.Fields("NUM_SOL_ERP_WIDTH").Value) Then
            TpConfVisorSol.dblNumSolErpWidth = 300
        Else
            TpConfVisorSol.dblNumSolErpWidth = AdoRes.Fields("NUM_SOL_ERP_WIDTH").Value
        End If
        
        TpConfVisorSol.vTipo = AdoRes.Fields("TIPO").Value
        TpConfVisorSol.vIdentificador = AdoRes.Fields("IDENTIFICADOR").Value
        TpConfVisorSol.vDescripcion = AdoRes.Fields("DESCRIPCION").Value
        TpConfVisorSol.vPeticionario = AdoRes.Fields("PETICIONARIO").Value
        TpConfVisorSol.vDesde = AdoRes.Fields("DESDE").Value
        TpConfVisorSol.vHasta = AdoRes.Fields("HASTA").Value
        TpConfVisorSol.vUON1 = AdoRes.Fields("UON1").Value
        TpConfVisorSol.vUON2 = AdoRes.Fields("UON2").Value
        TpConfVisorSol.vUON3 = AdoRes.Fields("UON3").Value
        TpConfVisorSol.vEnCursoAprobacion = AdoRes.Fields("ENCURSOAPROBACION").Value
        TpConfVisorSol.vPendientes = AdoRes.Fields("PENDIENTES").Value
        TpConfVisorSol.vAprobadas = AdoRes.Fields("APROBADAS").Value
        TpConfVisorSol.vRechazadas = AdoRes.Fields("RECHAZADAS").Value
        TpConfVisorSol.vAnuladas = AdoRes.Fields("ANULADAS").Value
        TpConfVisorSol.vCerradas = AdoRes.Fields("CERRADAS").Value
        TpConfVisorSol.sDepartamento = NullToStr(AdoRes.Fields("DEPARTAMENTO"))
        TpConfVisorSol.lEmpresa = NullToDbl0(AdoRes.Fields("EMPRESA"))
        TpConfVisorSol.dImporteDesde = NullToDbl0(AdoRes.Fields("IMPORTE_DESDE"))
        TpConfVisorSol.dImporteHasta = NullToDbl0(AdoRes.Fields("IMPORTE_HASTA"))
        TpConfVisorSol.sCodArticulo = NullToStr(AdoRes.Fields("COD_ART"))
        TpConfVisorSol.sDenArticulo = NullToStr(AdoRes.Fields("DEN_ART"))
        TpConfVisorSol.sGMN1 = NullToStr(AdoRes.Fields("GMN1"))
        TpConfVisorSol.sGMN2 = NullToStr(AdoRes.Fields("GMN2"))
        TpConfVisorSol.sGMN3 = NullToStr(AdoRes.Fields("GMN3"))
        TpConfVisorSol.sGMN4 = NullToStr(AdoRes.Fields("GMN4"))
        TpConfVisorSol.sComprador = NullToStr(AdoRes.Fields("COMPRADOR"))
        TpConfVisorSol.bAlertaSinProceso = SQLBinaryToBoolean(AdoRes.Fields("ALERTA_SINPROC"))
        TpConfVisorSol.bAlertaSinPedido = AdoRes.Fields("ALERTA_SINPED")
        TpConfVisorSol.bAlertaIntegracion = AdoRes.Fields("ALERTA_INT")
        TpConfVisorSol.sUON1_CC = NullToStr(AdoRes.Fields("UON1_CC").Value)
        TpConfVisorSol.sUON2_CC = NullToStr(AdoRes.Fields("UON2_CC").Value)
        TpConfVisorSol.sUON3_CC = NullToStr(AdoRes.Fields("UON3_CC").Value)
        TpConfVisorSol.sUON4_CC = NullToStr(AdoRes.Fields("UON4_CC").Value)
        TpConfVisorSol.Proveedor = NullToStr(AdoRes.Fields("PROVE").Value)
                
        If UsarIndice Then
            lIndice = 0

            While Not AdoRes.eof
                ' Dejamos en manos del programador cuando asignar la conexion al obejto usuario
                Me.Add sUsuario, TpConfVisorSol, lIndice
                AdoRes.MoveNext
                lIndice = lIndice + 1
            Wend

        Else
            While Not AdoRes.eof
                ' Dejamos en manos del programador cuando asignar la conexion al obejto usuario
                Me.Add sUsuario, TpConfVisorSol
                AdoRes.MoveNext
            Wend
        End If
        
        AdoRes.Close
        Set AdoRes = Nothing
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CConfVistasVisorSol", "CargarConfVisorSol", ERR, Erl)
        Exit Sub
    End If
End Sub



