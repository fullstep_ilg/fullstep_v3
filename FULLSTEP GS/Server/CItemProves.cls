VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CItemProves"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CItemProves **********************************
'*             Autor : Javier Arana
'*             Creada : 22/2/98
'****************************************************************


Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum

Private mCol As Collection
Private m_oConexion As CConexion
Private m_oConexionServer As CConexionDeUseServer
Private m_bEOF As Boolean

''' <summary>
''' A�adir elementos a la coleccion
''' </summary>
''' <param name="idTraslado">Id de traslado</param>
''' <param name="idItem">Id de �tem o 0 si solo se carga proveedor</param>
''' <param name="CodProveedor">Cod prove</param>
''' <param name="DenProveedor">Den prove</param>
''' <param name="CodERP">Cod ERP prove</param>
''' <param name="TransferErp">Si se transfiere</param>
''' <param name="Porcen">% trasferido</param>
''' <param name="Cantidad">Cantidad del item</param>
''' <param name="CantAdj">Cantidad del traspaso</param>
''' <returns>CItemProve elemento a�adido</returns>
''' <remarks>Llamada desde: FSGSClient.frmADJTraspasoERP.cmdAceptar_Click </remarks>
''' <remarks>Tiempo m�ximo: < 0,1seg </remarks>
''' <revision>JVS 21/12/2011</revision>
Public Function Add(ByVal idTraslado As Long, ByVal idItem As Long, ByVal CodProveedor As String, ByVal DenProveedor As String, _
ByVal CodERP As String, ByVal TransferErp As Boolean, _
ByVal Porcen As String, Optional ByVal Cantidad As Variant, Optional ByVal CantAdj As Variant) As CItemProve
    
    'create a new object
    Dim objnewmember As CItemProve
    Set objnewmember = New CItemProve
   
    objnewmember.idTraslado = idTraslado
    objnewmember.idItem = idItem
    objnewmember.CodProveedor = CodProveedor
    objnewmember.DenProveedor = DenProveedor
    objnewmember.CodERP = CodERP
    objnewmember.TransferErp = TransferErp
    objnewmember.Porcen = Porcen
    If IsMissing(Cantidad) Then
        objnewmember.Cantidad = Null
    Else
        objnewmember.Cantidad = Cantidad
    End If
    If IsMissing(CantAdj) Then
        objnewmember.CantAdj = Null
    Else
        objnewmember.CantAdj = CantAdj
    End If
   

    If idItem <> 0 Then
        mCol.Add objnewmember, CStr(idItem)
        objnewmember.ItemModificado = False
    Else
        Set objnewmember.Items = New CItemProves
        mCol.Add objnewmember, CStr(idItem) & "_" & CStr(CodProveedor)
    End If
    
    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing


End Function

Public Sub Remove(vntIndexKey As Variant)
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
        mCol.Remove vntIndexKey
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CItemProves", "Remove", ERR, Erl)
      Exit Sub
   End If
    
End Sub


Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
     Set NewEnum = mCol.[_NewEnum]
End Property


Private Sub Class_Initialize()
  

    Set mCol = New Collection
End Sub


Private Sub Class_Terminate()
    'Itemroys collection when this class is terminated
    Set mCol = Nothing
    Set m_oConexion = Nothing
    Set m_oConexionServer = Nothing
End Sub

Public Function BorrarEnModoIndice(ByVal iIndice As Integer)
Dim IndFor As Integer
Dim oItemProve As CItemProve

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    For IndFor = iIndice To mCol.Count - 2
            
        mCol.Remove (CStr(IndFor))
        Set oItemProve = mCol.Item(CStr(IndFor + 1))
        mCol.Add oItemProve, CStr(IndFor)
        Set oItemProve = Nothing
        
    Next IndFor
    
    mCol.Remove CStr(IndFor)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CItemProves", "BorrarEnModoIndice", ERR, Erl)
      Exit Function
   End If
    
End Function



Public Property Get Item(vntIndexKey As Variant) As CItemProve
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property


Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
     Count = mCol.Count
End If


End Property


