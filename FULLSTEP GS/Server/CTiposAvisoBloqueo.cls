VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CTiposAvisoBloqueo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

Private m_oConexion As CConexion
Private m_bEOF As Boolean

Friend Property Set Conexion(ByVal vData As CConexion)
    Set m_oConexion = vData
End Property

Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property

Private Sub Class_Terminate()

    ''' * Objetivo: Limpiar memoria

    Set m_oConexion = Nothing
End Sub


Public Function DevolverTiposAvisoBloqueo() As ADODB.Recordset
'***************************************************************************************
'Descripción:   Devuelve los tipos de campos prefenidos que hay en la tabla AVISO_BLOQUEO
'Valor que devuelve: Un recordset desconectado
'**************************************************************************************
Dim sConsulta As String
Dim adoRecordset As New ADODB.Recordset

'*************************************************************
If m_oConexion Is Nothing Then
    Err.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CTiposSolicit.DevolverTiposSolicitudes", "No se ha establecido la conexion"
End If
'*************************************************************

    Set adoRecordset = New ADODB.Recordset

    sConsulta = "SELECT TIPO,DESCR_" & gParametrosInstalacion.gIdioma & " FROM PM_TIPO_AVISO_BLOQUEO"
    
    adoRecordset.CursorLocation = adUseClient
    
    adoRecordset.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
            
    Set adoRecordset.ActiveConnection = Nothing

    Set DevolverTiposAvisoBloqueo = adoRecordset

End Function




