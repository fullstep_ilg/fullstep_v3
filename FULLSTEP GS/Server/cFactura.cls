VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cFactura"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

Private m_oConexion As CConexion

Private m_lIDFactura As Long
Private m_sNum As String
Private m_iAnyo As Integer
Private m_dFecha As Date
Private m_iEstado As Integer
Private m_sEstadoDen As String
Private m_oPagos As cPagosFactura
Private m_vImporte As Variant
Private m_vFechaConta As Variant
Private m_vMoneda As Variant

Public Property Let IdFactura(ByVal dato As Long)
    m_lIDFactura = dato
End Property
Public Property Get IdFactura() As Long
    IdFactura = m_lIDFactura
End Property

Public Property Let Numero(ByVal dato As String)
    m_sNum = dato
End Property
Public Property Get Numero() As String
    Numero = m_sNum
End Property

Public Property Let Anyo(ByVal dato As Integer)
    m_iAnyo = dato
End Property
Public Property Get Anyo() As Integer
    Anyo = m_iAnyo
End Property

Public Property Let Fecha(ByVal dato As Date)
    m_dFecha = dato
End Property
Public Property Get Fecha() As Date
    Fecha = m_dFecha
End Property

Public Property Let EstadoDen(ByVal dato As String)
    m_sEstadoDen = dato
End Property
Public Property Get EstadoDen() As String
    EstadoDen = m_sEstadoDen
End Property


Public Property Let Pagos(ByVal dato As cPagosFactura)
    Set m_oPagos = dato
End Property
Public Property Get Pagos() As cPagosFactura
    Set Pagos = m_oPagos
End Property

Public Property Let Importe(ByVal dato As Variant)
    m_vImporte = dato
End Property
Public Property Get Importe() As Variant
    Importe = m_vImporte
End Property

Public Property Let FechaConta(ByVal dato As Variant)
    m_vFechaConta = dato
End Property
Public Property Get FechaConta() As Variant
    FechaConta = m_vFechaConta
End Property

Public Property Let Moneda(ByVal dato As Variant)
    m_vMoneda = dato
End Property
Public Property Get Moneda() As Variant
    Moneda = m_vMoneda
End Property


Friend Property Set Conexion(ByVal vData As CConexion)

    ''' * Objetivo: Dar valor a la conexion de la Destino
    ''' * Recibe: Conexion
    ''' * Devuelve: Nada

    Set m_oConexion = vData
    
End Property
Friend Property Get Conexion() As CConexion

    ''' * Objetivo: Devolver la conexion de la Destino
    ''' * Recibe: Nada
    ''' * Devuelve: Conexion

    Set Conexion = m_oConexion
    
End Property

Private Sub Class_Terminate()
    
    On Error GoTo Error:
    
    Set m_oConexion = Nothing
    Exit Sub
Error:
    Resume Next
    
End Sub

''' <summary>
''' Establecer el Estado de la factura
''' </summary>
''' <param name="dato">Estado de la factura</param>
''' <remarks>Llamada desde: cFacturas/Add ; Tiempo m�ximo: 0</remarks>
Public Property Let Estado(ByVal dato As Integer)
    m_iEstado = dato
End Property
''' <summary>
''' Obtener el Estado de la factura
''' </summary>
''' <remarks>Llamada desde: frmsSeguimiento/cmdReabrir_Click ; Tiempo m�ximo: 0</remarks>
Public Property Get Estado() As Integer
    Estado = m_iEstado
End Property
