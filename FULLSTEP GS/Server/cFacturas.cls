VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cFacturas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

''' *** Clase: cFacturas

''' Variables que almacenan las propiedades de los objetos
Private mCol As Collection
Private m_bEOF As Boolean
Private m_oConexion As CConexion


''' Control de errores
Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum

''' <summary>Crea y carga un objeto de tipo factura.</summary>
''' <param name="lIDFactura">ID de Factura</param>
''' <param name="sNumero">Numero de Factura</param>
''' <param name="iAnyo">Anyo de Factura</param>
''' <param name="dFecha">Fecha de Factura</param>
''' <param name="sEstadoDen">Denominacion del estado de Factura</param>
''' <param name="iEstado">Estado de Factura</param>
''' <param name="vImporte">Importe</param>
''' <param name="vFechaConta">Fecha contabilizaci�n</param>
''' <param name="vMoneda">Moneda</param>
''' <returns>Objeto factura</returns>
''' <remarks>Llamada desde: COrdenDeEntrega/CargarLineasDePedido ; Tiempo m�ximo: 0</remarks>
''' <revision>LTG 02/07/2012</revision>

Public Function Add(ByVal lIDFactura As Long, ByVal sNumero As String, ByVal iAnyo As Integer, ByVal dFecha As Date, ByVal sEstadoDen As String, _
        ByVal iEstado As Integer, Optional ByVal vImporte As Variant, Optional ByVal vFechaConta As Variant, Optional ByVal vMoneda As Variant) As cFactura
    Dim objnewmember As cFactura
    Dim Indice As String
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Indice = lIDFactura
    Set objnewmember = New cFactura
    
    objnewmember.IdFactura = lIDFactura
    objnewmember.Numero = sNumero
    objnewmember.Anyo = iAnyo
    objnewmember.Fecha = dFecha
    objnewmember.EstadoDen = sEstadoDen
    objnewmember.Estado = iEstado
    If Not IsMissing(vImporte) Then objnewmember.Importe = vImporte
    If Not IsMissing(vFechaConta) Then objnewmember.FechaConta = vFechaConta
    If Not IsMissing(vMoneda) Then objnewmember.Moneda = vMoneda
    
    Set objnewmember.Conexion = m_oConexion
        
    ''' A�adir el objeto arbol a la coleccion
    ''' Si no se especifica indice, se a�ade al final
    mCol.Add objnewmember, Indice
            
    Set Add = objnewmember
    
    Set objnewmember = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "cFacturas", "Add", ERR, Erl)
      Exit Function
   End If

End Function


Friend Property Set Conexion(ByVal con As CConexion)

    ''' * Objetivo: Dar valor a la variable privada Conexion
    ''' * Recibe: Conexion
    ''' * Devuelve: Nada
    
    Set m_oConexion = con
    
End Property
Friend Property Get Conexion() As CConexion

    ''' * Objetivo: Devolver variable privada Conexion
    ''' * Recibe: Nada
    ''' * Devuelve: Conexion
    
    Set Conexion = m_oConexion
    
End Property


Public Property Get Count() As Long

    ''' * Objetivo: Devolver variable privada Count
    ''' * Recibe: Nada
    ''' * Devuelve: Numero de elementos de la coleccion

    If mCol Is Nothing Then
        Count = 0
    Else
        Count = mCol.Count
    End If

End Property

Public Property Get Item(vntIndexKey As Variant) As cFactura

    ''' * Objetivo: Recuperar un �rbol de la coleccion
    ''' * Recibe: Indice del �rbol a recuperar
    ''' * Devuelve: �rbol correspondiente
        
On Error GoTo NoSeEncuentra:

    ''' Devolvemos el item de la coleccion privada
    
    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:

    Set Item = Nothing
    
End Property


Private Sub Class_Initialize()

    Set mCol = New Collection

End Sub

Private Sub Class_Terminate()
    Set mCol = Nothing
    Set m_oConexion = Nothing
End Sub

Public Sub Remove(vntIndexKey As Variant)
   
    mCol.Remove vntIndexKey
End Sub


Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
     Set NewEnum = mCol.[_NewEnum]
End Property

''' <summary>
''' Devuelve un recordset con las facturas y sus pagos.
''' </summary>
''' <param name="lIDLineaPedido">ID de la linea del pedido</param>
''' <param name="sIdi">C�digo idioma</param>
''' <returns>recordset con las facturas y sus pagos</returns>
''' <remarks>Llamada desde=frmFacturasPagos.frm --> CargarGridFacturas(); Tiempo m�ximo=0,2seg.</remarks>
Public Function DevolverFacturasYPagos(ByVal lIDLineaPedido As Long, ByVal sIdi As String) As adodb.Recordset
Dim rsOrigen As adodb.Recordset
Dim cn As adodb.Connection
Dim sql As String
Dim stm As adodb.Stream
Dim rs As adodb.Recordset

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set cn = New adodb.Connection
    cn.Open "Provider=MSDataShape.1;Persist Security Info=True;Data Source=" & basParametros.ServidorGS & ";User ID=" & LICDBLogin & ";Password=" & LICDBContra & ";Initial Catalog=" & basParametros.BaseDatosGS & ";Data Provider=SQLOLEDB.1"
    cn.CursorLocation = adUseClient
    cn.CommandTimeout = 120
        

    sql = "SHAPE {"
    'Facturas de la linea de pedido
    sql = sql & " SELECT DISTINCT LPF.LINEA_PEDIDO LINEA_PEDIDO"
    sql = sql & " ,LPF.FACTURA FACID ,CASE WHEN NOT F.ANYO IS NULL THEN substring(convert(nvarchar(4),F.ANYO,4), 3,2)  + '/' + F.NUM ELSE '00' + '/' + F.NUM END NUM, F.ANYO,F.FECHA, EFD.DEN ESTADODEN"
    sql = sql & " ,LF.IMPORTE IMPORTE, F.MON MONEDA"
    sql = sql & " FROM LINEAS_PEDIDO_FACTURA LPF WITH (NOLOCK)"
    sql = sql & " INNER JOIN LIN_FACTURA LF WITH (NOLOCK) ON LF.FACTURA = LPF.FACTURA"
    sql = sql & " LEFT JOIN FACTURA F WITH (NOLOCK) ON F.ID = LPF.FACTURA"
    sql = sql & " LEFT JOIN EST_FACTURA EF WITH (NOLOCK) ON EF.ID = F.ESTADO"
    sql = sql & " LEFT JOIN EST_FACTURA_DEN EFD WITH (NOLOCK) ON EFD.EST_FACTURA = EF.ID AND EFD.IDI = " & StrToSQLNULL(sIdi)
    
    sql = sql & " WHERE LPF.LINEA_PEDIDO = " & lIDLineaPedido
    
    sql = sql & "}  AS FACTURAS"
    'Pagos
    sql = sql & " APPEND ({"
    sql = sql & " SELECT DISTINCT P.NUM, P.FECHA, EPD.DEN"
    sql = sql & " ,P.ID_FACTURA, LPF.LINEA_PEDIDO "
    sql = sql & " FROM PAGO P WITH (NOLOCK)"
    sql = sql & " INNER JOIN LINEAS_PEDIDO_FACTURA LPF WITH (NOLOCK) ON LPF.FACTURA = P.ID_FACTURA"
    sql = sql & " LEFT JOIN EST_PAGO_DEN EPD WITH (NOLOCK) ON EPD.EST_PAGO = P.ESTADO AND IDI = " & StrToSQLNULL(sIdi)
       
    sql = sql & " } AS PAGOS"
    sql = sql & " RELATE 'FACID' TO 'ID_FACTURA') AS FACTURAS_PAGOS"

    
    Set rsOrigen = New adodb.Recordset
    rsOrigen.CursorLocation = adUseClient
    rsOrigen.Open sql, cn, adOpenForwardOnly, adLockBatchOptimistic

    
    rsOrigen.ActiveConnection = Nothing
    cn.Close
        
    'Paso el recordset a stream y lo vuelvo a abrir desde ah�.Se hace as� porque sino no se desconecta:
    Set stm = New adodb.Stream
    rsOrigen.Save stm, adPersistXML
    
    Set rs = New adodb.Recordset
    rs.Open stm, , adOpenKeyset, adLockOptimistic 'Para que se pueda abrir en modo editable
    rs.ActiveConnection = Nothing
    
    Set DevolverFacturasYPagos = rs
    
    stm.Close
    Set stm = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "cFacturas", "DevolverFacturasYPagos", ERR, Erl)
      Exit Function
   End If
    
End Function

''' <summary>
''' Carga los distintos estados que tiene una factura.
''' </summary>
''' <param name="sIdi">Codigo Idioma</param>
''' <returns>RecordSet con los estados que tiene una factura</returns>
''' <remarks>Llamada desde=frmSeguimiento --> sdbcEstadoFactura_DropDown; Tiempo m�ximo=0,1seg.</remarks>
Public Function devolverTipoEstados(sIdi As String) As adodb.Recordset
Dim sConsulta As String
Dim rs As adodb.Recordset
Dim adoComm As adodb.Command
Dim adoParam As adodb.Parameter

        
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sConsulta = "SELECT EF.ID, EFD.DEN"
    sConsulta = sConsulta & " FROM EST_FACTURA EF WITH (NOLOCK)"
    sConsulta = sConsulta & " INNER JOIN EST_FACTURA_DEN EFD WITH (NOLOCK) ON EFD.EST_FACTURA = EF.ID AND EFD.IDI=?"
    sConsulta = sConsulta & " WHERE EF.INTEGRACION = 0"
    
    Set adoComm = New adodb.Command
    Set adoComm.ActiveConnection = m_oConexion.ADOCon
    Set adoParam = adoComm.CreateParameter("IDI", adVarChar, adParamInput, 20, sIdi)
    adoComm.Parameters.Append adoParam
    
    adoComm.CommandText = sConsulta
    Set rs = adoComm.Execute

    Set adoComm = Nothing
    Set adoParam = Nothing
    Set rs.ActiveConnection = Nothing
    Set devolverTipoEstados = rs
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "cFacturas", "devolverTipoEstados", ERR, Erl)
      Exit Function
   End If

End Function

''' <summary>Devuelve un recordset con las facturas y sus pagos.</summary>
''' <param name="lIDOrden">ID de la orden de entrega</param>
''' <param name="sIdi">C�digo idioma</param>
''' <returns>recordset con las facturas y sus pagos</returns>
''' <remarks>Llamada desde=frmFacturasPagos.frm --> CargarGridFacturas(); Tiempo m�ximo=0,2seg.</remarks>
''' <revision>LTG 16/04/2014</revision>

Public Function DevolverFacturasYPagosOrden(ByVal lIDOrden As Long, ByVal sIdi As String) As adodb.Recordset
    Dim rsOrigen As adodb.Recordset
    Dim cn As adodb.Connection
    Dim sql As String
    Dim stm As adodb.Stream
    Dim rs As adodb.Recordset

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set cn = New adodb.Connection
    cn.Open "Provider=MSDataShape.1;Persist Security Info=True;Data Source=" & basParametros.ServidorGS & ";User ID=" & LICDBLogin & ";Password=" & LICDBContra & ";Initial Catalog=" & basParametros.BaseDatosGS & ";Data Provider=SQLOLEDB.1"
    cn.CursorLocation = adUseClient
    cn.CommandTimeout = 120
        

    sql = "SHAPE {"
    'Facturas de la linea de pedido
    sql = sql & " SELECT DISTINCT LPF.FACTURA FACID"
    sql = sql & " ,CASE WHEN NOT F.ANYO IS NULL THEN substring(convert(nvarchar(4),F.ANYO,4), 3,2)  + '/' + F.NUM ELSE '00' + '/' + F.NUM END NUM, F.ANYO,F.FECHA, EFD.DEN ESTADODEN"
    sql = sql & " ,SUM(LF.IMPORTE) IMPORTE, F.MON MONEDA"
    sql = sql & " FROM LINEAS_PEDIDO_FACTURA LPF WITH (NOLOCK)"
    sql = sql & " INNER JOIN LINEAS_PEDIDO LP WITH (NOLOCK) ON LP.ID = LPF.LINEA_PEDIDO AND LP.ORDEN = " & lIDOrden
    sql = sql & " INNER JOIN LIN_FACTURA LF WITH (NOLOCK) ON LF.FACTURA = LPF.FACTURA AND LF.LINEA = LPF.LINEA_FACTURA"
    sql = sql & " LEFT JOIN FACTURA F WITH (NOLOCK) ON F.ID = LPF.FACTURA"
    sql = sql & " LEFT JOIN EST_FACTURA EF WITH (NOLOCK) ON EF.ID = F.ESTADO"
    sql = sql & " LEFT JOIN EST_FACTURA_DEN EFD WITH (NOLOCK) ON EFD.EST_FACTURA = EF.ID AND EFD.IDI = " & StrToSQLNULL(sIdi)
    sql = sql & " GROUP BY LPF.FACTURA, F.NUM, F.ANYO, F.FECHA, EFD.DEN, F.MON"
    sql = sql & "}  AS FACTURAS"
    'Pagos
    sql = sql & " APPEND ({"
    sql = sql & " SELECT DISTINCT P.NUM, P.FECHA, EPD.DEN"
    sql = sql & " ,P.ID_FACTURA, LPF.LINEA_PEDIDO "
    sql = sql & " FROM PAGO P WITH (NOLOCK)"
    sql = sql & " INNER JOIN LINEAS_PEDIDO_FACTURA LPF WITH (NOLOCK) ON LPF.FACTURA = P.ID_FACTURA"
    sql = sql & " INNER JOIN LINEAS_PEDIDO LP WITH (NOLOCK) ON LP.ID = LPF.LINEA_PEDIDO AND LP.ORDEN = " & lIDOrden
    sql = sql & " LEFT JOIN EST_PAGO_DEN EPD WITH (NOLOCK) ON EPD.EST_PAGO = P.ESTADO AND IDI = " & StrToSQLNULL(sIdi)
    sql = sql & " } AS PAGOS"
    sql = sql & " RELATE 'FACID' TO 'ID_FACTURA') AS FACTURAS_PAGOS"
    
    Set rsOrigen = New adodb.Recordset
    rsOrigen.CursorLocation = adUseClient
    rsOrigen.Open sql, cn, adOpenForwardOnly, adLockBatchOptimistic

    
    rsOrigen.ActiveConnection = Nothing
    cn.Close
        
    'Paso el recordset a stream y lo vuelvo a abrir desde ah�.Se hace as� porque sino no se desconecta:
    Set stm = New adodb.Stream
    rsOrigen.Save stm, adPersistXML
    
    Set rs = New adodb.Recordset
    rs.Open stm, , adOpenKeyset, adLockOptimistic 'Para que se pueda abrir en modo editable
    rs.ActiveConnection = Nothing
    
    Set DevolverFacturasYPagosOrden = rs
    
    stm.Close
    Set stm = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "cFacturas", "DevolverFacturasYPagosOrden", ERR, Erl)
      Exit Function
   End If
End Function

''' <summary>Carga la colecci�n de facturas con las facturas que cumplan los criterios</summary>
''' <param name="sIdi">Idioma</param>
''' <param name="sNumFactura">Num. factura</param>
''' <param name="vFechaFactDesde">Fecha factura desde</param>
''' <param name="vFechaFactHasta">Fecha factura hasta</param>
''' <param name="vFechaContaDesde">Fecha contabilizaci�n desde</param>
''' <param name="vFechaContaHasta">Fecha contabilizaci�n hasta</param>
''' <param name="vFactERP">Factura ERP</param>
''' <param name="vEstado">Estado</param>
''' <param name="vImpDesde">Importe desde</param>
''' <param name="vImpHasta">Importe hasta</param>
''' <param name="sAlbaran">Gestor</param>
''' <param name="vAnyo">Anyo</param>
''' <param name="vNumPedido">Num. pedido</param>
''' <param name="vOrden">Orden</param>
''' <param name="sNumERP">Num ERP</param>
''' <param name="sArtCod">Cod. art�culo</param>
''' <param name="sUONCC">Centro de coste</param>
''' <param name="vFiltrosImp">Estructura de las distintas partidas (PRES5-PRES1-PRES2-PRES3-PRES4)</param>
''' <param name="vIdFactura">Id. factura</param>
''' <remarks>Llamada desde: frmFacturaBuscar</remarks>
''' <revision>LTG 29/06/2012</revision>

Public Sub CargarFacturas(ByVal sIdi As String, Optional ByVal sNumFactura As String, Optional ByVal vFechaFactDesde As Variant, _
        Optional ByVal vFechaFactHasta As Variant, Optional vFechaContaDesde As Variant, Optional vFechaContaHasta As Variant, Optional ByVal sNumFactERP As String, _
        Optional ByVal vEstado As Variant, Optional ByVal vImpDesde As Variant, Optional ByVal vImpHasta As Variant, Optional ByVal sAlbaran As String, _
        Optional ByVal sGestor As String, Optional ByVal vAnyo As Variant, Optional ByVal vNumPedido As Variant, Optional ByVal vOrden As Variant, _
        Optional ByVal sNumERP As String, Optional ByVal sArtCod As String, Optional ByVal sUONCC As String, Optional ByVal vFiltrosImp As Variant, _
        Optional ByVal vIdFactura As Variant)
    Dim oCom As adodb.Command
    Dim oParam As adodb.Parameter
    Dim rsFacturas As adodb.Recordset
    Dim oFactura As cFactura
    Dim sConsulta As String
    Dim sNumFact As String
    Dim sFactERP As String
    Dim sAlb As String
    Dim sERP As String
    Dim sArt As String
    Dim sAux() As String
    Dim bHayFiltroImp As Boolean
    Dim i As Integer
            
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    bHayFiltroImp = False
    If Not IsMissing(vFiltrosImp) Then
        For i = 0 To UBound(vFiltrosImp)
            If vFiltrosImp(i) <> "" Then
                bHayFiltroImp = True
                Exit For
            End If
        Next
    End If
    
    'SELECT
    sConsulta = "SELECT DISTINCT F.ID,F.NUM,F.ANYO,F.PROVE,F.ESTADO,EFD.DEN AS ESTDEN,F.FECHA,F.IMPORTE,F.FEC_CONTA,F.MON"
    
    'FROM
    sConsulta = sConsulta & " FROM FACTURA F WITH (NOLOCK)"
    sConsulta = sConsulta & " LEFT JOIN EST_FACTURA_DEN EFD WITH (NOLOCK) ON F.ESTADO=EFD.EST_FACTURA AND IDI=?"
    If sAlbaran <> "" Or IsNumeric(vAnyo) Or IsNumeric(vNumPedido) Or IsNumeric(vOrden) Or sGestor <> "" Or sNumERP <> "" Or sArtCod <> "" Or sGestor <> "" Or _
        sUONCC <> "" Or bHayFiltroImp Then
        
        sConsulta = sConsulta & " LEFT JOIN LINEAS_PEDIDO_FACTURA LPF WITH (NOLOCK) ON LPF.FACTURA=F.ID"
        sConsulta = sConsulta & " LEFT JOIN LINEAS_PEDIDO LP WITH (NOLOCK) ON LP.ID=LPF.LINEA_PEDIDO"
        If sAlbaran <> "" Or IsNumeric(vAnyo) Or IsNumeric(vNumPedido) Or IsNumeric(vOrden) Or sGestor <> "" Or sNumERP <> "" Or sArtCod <> "" Then
            sConsulta = sConsulta & " LEFT JOIN ORDEN_ENTREGA OE WITH (NOLOCK) ON OE.ID=LP.ORDEN"
            sConsulta = sConsulta & " LEFT JOIN PEDIDO P WITH (NOLOCK) ON P.ID=OE.PEDIDO"
        End If
        If sGestor <> "" Or sUONCC <> "" Or bHayFiltroImp Then
            sConsulta = sConsulta & " LEFT JOIN LINEAS_PED_IMPUTACION LPI WITH (NOLOCK) ON LPI.LINEA=LP.ID"
            sConsulta = sConsulta & " LEFT JOIN PRES5_IMPORTES P5I WITH (NOLOCK) ON P5I.ID=LPI.PRES5_IMP"
        End If
    End If
    
    'WHERE
    sConsulta = sConsulta & " WHERE 1=1"
    If sNumFactura <> "" Then
        If InStr(sNumFactura, "*") <> 0 Then
            sNumFact = Replace(sNumFactura, "*", "%")
            sConsulta = sConsulta & " AND F.NUM LIKE ?"
        Else
            sNumFact = sNumFactura
            sConsulta = sConsulta & " AND F.NUM=?"
        End If
    End If
    If IsDate(vFechaFactDesde) Then sConsulta = sConsulta & " AND F.FECHA>=?"
    If IsDate(vFechaFactHasta) Then sConsulta = sConsulta & " AND F.FECHA<=?"
    If IsDate(vFechaContaDesde) Then sConsulta = sConsulta & " AND F.FEC_CONTA>=?"
    If IsDate(vFechaContaHasta) Then sConsulta = sConsulta & " AND F.FEC_CONTA<=?"
    If sNumFactERP <> "" Then
        If InStr(sNumFactERP, "*") <> 0 Then
            sFactERP = Replace(sNumFactERP, "*", "%")
            sConsulta = sConsulta & " AND F.NUM_ERP LIKE ?"
        Else
            sFactERP = sNumFactERP
            sConsulta = sConsulta & " AND F.NUM_ERP=?"
        End If
    End If
    If IsNumeric(vEstado) Then sConsulta = sConsulta & " AND F.ESTADO=?"
    If IsNumeric(vImpDesde) Then sConsulta = sConsulta & " AND F.IMPORTE>=?"
    If IsNumeric(vImpHasta) Then sConsulta = sConsulta & " AND F.IMPORTE<=?"
    If sAlbaran <> "" Then
        If InStr(sAlbaran, "*") <> 0 Then
            sAlb = Replace(sAlbaran, "*", "%")
            sConsulta = sConsulta & " AND LPF.ALBARAN LIKE ?"
        Else
            sAlb = sAlbaran
            sConsulta = sConsulta & " AND LPF.ALBARAN=?"
        End If
    End If
    If sGestor <> "" Then sConsulta = sConsulta & " AND P5I.GESTOR=?"
    If IsNumeric(vAnyo) Then sConsulta = sConsulta & " AND OE.ANYO=?"
    If IsNumeric(vNumPedido) Then sConsulta = sConsulta & " AND P.NUM=?"
    If IsNumeric(vOrden) Then sConsulta = sConsulta & " AND OE.NUM=?"
    If sNumERP <> "" Then
        If InStr(sNumERP, "*") <> 0 Then
            sERP = Replace(sNumERP, "*", "%")
            sConsulta = sConsulta & " AND OE.NUMEXT LIKE ?"
        Else
            sERP = sNumERP
            sConsulta = sConsulta & " AND OE.NUMEXT=?"
        End If
    End If
    If sArtCod <> "" Then
        If InStr(sArtCod, "*") <> 0 Then
            sArt = Replace(sArtCod, "*", "%")
            sConsulta = sConsulta & " AND LP.ART_INT LIKE ?"
        Else
            sArt = sArtCod
            sConsulta = sConsulta & " AND LP.ART_INT=?"
        End If
    End If
    If sUONCC <> "" Then
        sAux = Split(sUONCC, "-")
        
        If sAux(0) <> "" Then
            sConsulta = sConsulta & " AND LPI.UON1=?"
            If sAux(1) <> "" Then
                sConsulta = sConsulta & " AND LPI.UON2=?"
            Else
                sConsulta = sConsulta & " AND LPI.UON2 IS NULL"
            End If
            If sAux(2) <> "" Then
                sConsulta = sConsulta & " AND LPI.UON3=?"
            Else
                sConsulta = sConsulta & " AND LPI.UON3 IS NULL"
            End If
            If sAux(3) <> "" Then
                sConsulta = sConsulta & " AND LPI.UON4=?"
            Else
                sConsulta = sConsulta & " AND LPI.UON4 IS NULL"
            End If
        End If
    End If
    If bHayFiltroImp Then
        For i = 0 To UBound(vFiltrosImp)
            If vFiltrosImp(i) <> "" Then
                sAux = Split(vFiltrosImp(i), "-")
                If sAux(0) <> "" Then
                    sConsulta = sConsulta & " AND LPI.PRES0=?"
                    sConsulta = sConsulta & " AND LPI.PRES1=?"
                    If sAux(2) <> "" Then
                        sConsulta = sConsulta & " AND LPI.PRES2=?"
                    Else
                        sConsulta = sConsulta & " AND LPI.PRES2 IS NULL"
                    End If
                    If sAux(3) <> "" Then
                        sConsulta = sConsulta & " AND LPI.PRES3=?"
                    Else
                        sConsulta = sConsulta & " AND LPI.PRES3 IS NULL"
                    End If
                    If sAux(4) <> "" Then
                        sConsulta = sConsulta & " AND LPI.PRES4=?"
                    Else
                        sConsulta = sConsulta & " AND LPI.PRES4 IS NULL"
                    End If
                End If
            End If
        Next
    End If
    If IsNumeric(vIdFactura) Then sConsulta = sConsulta & " AND F.ID=?"
    
    Set oCom = New adodb.Command
    With oCom
        .ActiveConnection = m_oConexion.ADOCon
        .CommandType = adCmdText
        .CommandText = sConsulta
        
        Set oParam = .CreateParameter("@IDI", adVarChar, adParamInput, Len(sIdi), sIdi)
            .Parameters.Append oParam
        If sNumFact <> "" Then
            Set oParam = .CreateParameter("@NUM", adVarChar, adParamInput, Len(sNumFact), sNumFact)
            .Parameters.Append oParam
        End If
        If IsDate(vFechaFactDesde) Then
            Set oParam = .CreateParameter("@FFACTDESDE", adDBDate, adParamInput, , vFechaFactDesde)
            .Parameters.Append oParam
        End If
        If IsDate(vFechaFactHasta) Then
            Set oParam = .CreateParameter("@FFACTHASTA", adDBDate, adParamInput, , vFechaFactHasta)
            .Parameters.Append oParam
        End If
        If IsDate(vFechaContaDesde) Then
            Set oParam = .CreateParameter("@FCONTADESDE", adDBDate, adParamInput, , vFechaContaDesde)
            .Parameters.Append oParam
        End If
        If IsDate(vFechaContaHasta) Then
            Set oParam = .CreateParameter("@FCONTAHASTA", adDBDate, adParamInput, , vFechaContaHasta)
            .Parameters.Append oParam
        End If
        If sFactERP <> "" Then
            Set oParam = .CreateParameter("@FERP", adVarChar, adParamInput, Len(sFactERP), sFactERP)
            .Parameters.Append oParam
        End If
        If IsNumeric(vEstado) Then
            Set oParam = .CreateParameter("@EST", adInteger, adParamInput, , vEstado)
            .Parameters.Append oParam
        End If
        If IsNumeric(vImpDesde) Then
            Set oParam = .CreateParameter("@IMPDESDE", adDouble, adParamInput, , vImpDesde)
            .Parameters.Append oParam
        End If
        If IsNumeric(vImpHasta) Then
            Set oParam = .CreateParameter("@IMPHASTA", adDouble, adParamInput, , vImpHasta)
            .Parameters.Append oParam
        End If
        If sAlbaran <> "" Then
            Set oParam = .CreateParameter("@ALBARAN", adVarChar, adParamInput, Len(sAlb), sAlb)
            .Parameters.Append oParam
        End If
        If sGestor <> "" Then
            Set oParam = .CreateParameter("@GESTOR", adVarChar, adParamInput, Len(sGestor), sGestor)
            .Parameters.Append oParam
        End If
        If IsNumeric(vAnyo) Then
            Set oParam = .CreateParameter("@ANYO", adInteger, adParamInput, , vAnyo)
            .Parameters.Append oParam
        End If
        If IsNumeric(vNumPedido) Then
            Set oParam = .CreateParameter("@NUMPED", adInteger, adParamInput, , vNumPedido)
            .Parameters.Append oParam
        End If
        If IsNumeric(vOrden) Then
            Set oParam = .CreateParameter("@ORDEN", adInteger, adParamInput, , vOrden)
            .Parameters.Append oParam
        End If
        If sERP <> "" Then
            Set oParam = .CreateParameter("@NUMERP", adVarChar, adParamInput, Len(sERP), sERP)
            .Parameters.Append oParam
        End If
        If sArt <> "" Then
            Set oParam = .CreateParameter("@ARTCOD", adVarChar, adParamInput, Len(sArt), sArt)
            .Parameters.Append oParam
        End If
        If sUONCC <> "" Then
            sAux = Split(sUONCC, "-")
            If sAux(0) <> "" Then
                Set oParam = .CreateParameter("@UON1", adVarChar, adParamInput, Len(sAux(0)), sAux(0))
                .Parameters.Append oParam
                
                If sAux(1) <> "" Then
                    Set oParam = .CreateParameter("@UON2", adVarChar, adParamInput, Len(sAux(1)), sAux(1))
                    .Parameters.Append oParam
                End If
                If sAux(2) <> "" Then
                    Set oParam = .CreateParameter("@UON3", adVarChar, adParamInput, Len(sAux(2)), sAux(2))
                    .Parameters.Append oParam
                End If
                If sAux(3) <> "" Then
                    Set oParam = .CreateParameter("@UON4", adVarChar, adParamInput, Len(sAux(3)), sAux(3))
                    .Parameters.Append oParam
                End If
            End If
        End If
        If bHayFiltroImp Then
            For i = 0 To UBound(vFiltrosImp)
                If vFiltrosImp(i) <> "" Then
                    sAux = Split(vFiltrosImp(i), "-")
                    If sAux(0) <> "" Then
                        Set oParam = .CreateParameter("@PRES0", adVarChar, adParamInput, Len(sAux(0)), sAux(0))
                        .Parameters.Append oParam
                        Set oParam = .CreateParameter("@PRES1", adVarChar, adParamInput, Len(sAux(1)), sAux(1))
                        .Parameters.Append oParam
                                                                        
                        If sAux(2) <> "" Then
                            Set oParam = .CreateParameter("@PRES2", adVarChar, adParamInput, Len(sAux(2)), sAux(2))
                            .Parameters.Append oParam
                        End If
                        If sAux(3) <> "" Then
                            Set oParam = .CreateParameter("@PRES3", adVarChar, adParamInput, Len(sAux(3)), sAux(3))
                            .Parameters.Append oParam
                        End If
                        If sAux(4) <> "" Then
                            Set oParam = .CreateParameter("@PRES4", adVarChar, adParamInput, Len(sAux(4)), sAux(4))
                            .Parameters.Append oParam
                        End If
                    End If
                End If
            Next
        End If
        If IsNumeric(vIdFactura) Then
            Set oParam = .CreateParameter("@FACID", adInteger, adParamInput, , vIdFactura)
            .Parameters.Append oParam
        End If
        
        Set rsFacturas = .Execute
    End With
    Set oParam = Nothing
    Set oCom = Nothing
    
    If Not rsFacturas.eof Then
        While Not rsFacturas.eof
            Set oFactura = Me.Add(rsFacturas("ID"), rsFacturas("NUM"), rsFacturas("ANYO"), rsFacturas("FECHA"), rsFacturas("ESTDEN"), rsFacturas("ESTADO"), _
                rsFacturas("IMPORTE"), rsFacturas("FEC_CONTA"), rsFacturas("MON"))
                
            rsFacturas.MoveNext
        Wend
    End If
    
    Set rsFacturas = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "cFacturas", "CargarFacturas", ERR, Erl)
      Exit Sub
   End If
End Sub

