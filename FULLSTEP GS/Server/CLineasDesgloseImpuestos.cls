VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CLineasDesgloseImpuesto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Collection" ,"CLineaDesgloseImpuesto"
Attribute VB_Ext_KEY = "Member0" ,"CLineaDesgloseImpuesto"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'local variable to hold collection
Private mCol As Collection

Private m_oConexion As CConexion

Public Function addLinea(oLinea As CLineaDesgloseImpuesto, Optional sKey As String) As CLineaDesgloseImpuesto
    If Len(sKey) = 0 Then
        mCol.Add oLinea
    Else
        mCol.Add oLinea, sKey
    End If
End Function
Public Function Add(TipoValor As Double, TipoImpuesto As String, BaseImponible As Double, Optional ByVal dCambio As Double = 1) As CLineaDesgloseImpuesto
    'create a new object
    Dim objnewmember As CLineaDesgloseImpuesto
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New CLineaDesgloseImpuesto


    'set the properties passed into the method
    objnewmember.TipoValor = TipoValor
    objnewmember.TipoImpuesto = TipoImpuesto
    objnewmember.BaseImponible = BaseImponible
    objnewmember.BaseImponibleMonProce = (BaseImponible / dCambio)
    
    mCol.Add objnewmember, CStr(objnewmember.TipoImpuesto & " " & objnewmember.TipoValor)

    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CLineasDesgloseImpuesto", "Add", ERR, Erl)
      Exit Function
   End If

End Function
'A�ade una linea desglose, si ya existiera una con la misma clave la suma a la fila ya creada
Public Function anyadirDesglose(TipoValor As Double, TipoImpuesto As String, BaseImponible As Double, Optional ByVal dCambio As Double = 1) As CLineaDesgloseImpuesto
    Dim oLinea As CLineaDesgloseImpuesto
    
    If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Me.Existe(TipoImpuesto & " " & TipoValor) Then
        Set oLinea = mCol.Item(TipoImpuesto & " " & TipoValor)
        oLinea.BaseImponible = oLinea.BaseImponible + BaseImponible
        oLinea.BaseImponibleMonProce = oLinea.BaseImponibleMonProce + (BaseImponible / dCambio)
    Else
        Set oLinea = Me.Add(TipoValor, TipoImpuesto, BaseImponible, dCambio)
    End If
    Set anyadirDesglose = oLinea
    Set oLinea = Nothing
    
    '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CLineasDesgloseImpuesto", "anyadirDesglose", ERR, Erl)
        Exit Function
    End If
End Function

Public Property Get Item(vntIndexKey As Variant) As CLineaDesgloseImpuesto
Attribute Item.VB_UserMemId = 0
    'used when referencing an element in the collection
    'vntIndexKey contains either the Index or Key to the collection,
    'this is why it is declared as a Variant
    'Syntax: Set foo = x.Item(xyz) or Set foo = x.Item(5)
  Set Item = mCol(vntIndexKey)
End Property

'''<summary>Devuelve el valor de impuestos total que es la suma de las cuotas de los impuestos de la orden</summary>
Public Function getImporteImpuesto() As Double
    Dim oLIneaDesglose As CLineaDesgloseImpuesto
    Dim impuesto As Double

    If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    impuesto = 0
    For Each oLIneaDesglose In mCol
        impuesto = impuesto + oLIneaDesglose.cuota
    Next
    Set oLIneaDesglose = Nothing
    getImporteImpuesto = impuesto
    
    '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CLineasDesgloseImpuesto", "getImporteImpuesto", ERR, Erl)
        Exit Function
    End If
End Function

'''<summary>Devuelve el valor de impuestos total que es la suma de las cuotas de los impuestos de la orden</summary>
Public Function getImporteImpuestoMonProce() As Double
    Dim oLIneaDesglose As CLineaDesgloseImpuesto
    Dim impuesto As Double

    If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    impuesto = 0
    For Each oLIneaDesglose In mCol
        impuesto = impuesto + oLIneaDesglose.cuotaMonProce
    Next
    Set oLIneaDesglose = Nothing
    getImporteImpuestoMonProce = impuesto
    
    '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CLineasDesgloseImpuesto", "getImporteImpuestoMonProce", ERR, Erl)
        Exit Function
    End If
End Function


Public Property Get Count() As Long
    'used when retrieving the number of elements in the
    'collection. Syntax: Debug.Print x.Count
    Count = mCol.Count
End Property


Public Sub Remove(vntIndexKey As Variant)
    'used when removing an element from the collection
    'vntIndexKey contains either the Index or Key, which is why
    'it is declared as a Variant
    'Syntax: x.Remove(xyz)


    mCol.Remove vntIndexKey
End Sub


Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
    'this property allows you to enumerate
    'this collection with the For...Each syntax
    Set NewEnum = mCol.[_NewEnum]
End Property


Private Sub Class_Initialize()
    'creates the collection when this class is created
    Set mCol = New Collection
End Sub


Private Sub Class_Terminate()
    'destroys collection when this class is terminated
    Set mCol = Nothing
    Set m_oConexion = Nothing
End Sub

Public Function Existe(Key As String)
    Dim o As CLineaDesgloseImpuesto  '
    On Error Resume Next
    '
    Set o = mCol.Item(Key)
    ' si se produce un error es que no existe ese elemento
    If ERR.Number <> 0 Then
        Existe = False
    Else
        Existe = True
    End If
    Set o = Nothing
    
End Function

Public Function toString() As String
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    toString = ""
    Dim oLinea As CLineaDesgloseImpuesto
    For Each oLinea In mCol
        toString = toString & oLinea.toString & vbCrLf
    Next
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CLineasDesgloseImpuesto", "toString", ERR, Erl)
      Exit Function
   End If
End Function

Public Sub cargarLineasDesgloseImpuestoOrden(ByVal IdOrden As Long)

    Dim rs As New adodb.Recordset
    Dim sConsulta As String
    Dim adocom As adodb.Command
    Dim oParam As adodb.Parameter
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sConsulta = "EXEC FSGS_DESGLOSE_IMPUESTOS_ORDEN ?,? "
    Set adocom = New adodb.Command
    Set adocom.ActiveConnection = Me.Conexion.ADOCon
    
    Set adoParam = adocom.CreateParameter("ORDEN", adInteger, adParamInput, , IdOrden)
    adocom.Parameters.Append adoParam
    Set adoParam = adocom.CreateParameter("IDIOMA", adVarChar, adParamInput, 20, gParametrosInstalacion.gIdioma)
    adocom.Parameters.Append adoParam
    
    adocom.CommandText = sConsulta
    adocom.CommandType = adCmdText

    
    Set rs = adocom.Execute
    
    Set mCol = Nothing
    Set mCol = New Collection
        
    While Not rs.eof
        Me.Add rs("TIPO").Value, rs("DEN").Value, rs("BI").Value
        rs.MoveNext
    Wend
    
    rs.Close
    Set rs = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CLineasDesgloseImpuesto", "cargarLineasDesgloseImpuestoOrden", ERR, Erl)
      Exit Sub
   End If
End Sub


Public Property Get Conexion() As CConexion

    Set Conexion = m_oConexion

End Property

Public Property Set Conexion(oConexion As CConexion)

    Set m_oConexion = oConexion

End Property
