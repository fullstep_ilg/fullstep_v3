VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CNotificadoAccionCampo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum

Implements IBaseDatos

Private moConexion As CConexion

Private m_lCampo As Long
Private m_lNotificado As Long
Private m_bVisible As Boolean
Private m_oDenominacionesCampo As CMultiidiomas
Private m_iOrigen As OrigenNotificadoAccion
Private m_bDesglose As Boolean


Private m_adores As adodb.Recordset

Friend Property Set Conexion(ByVal con As CConexion)
    Set moConexion = con
End Property
Friend Property Get Conexion() As CConexion
    Set Conexion = moConexion
End Property

Public Property Get Campo() As Long
    Campo = m_lCampo
End Property
Public Property Let Campo(ByVal Data As Long)
    m_lCampo = Data
End Property

Public Property Get Notificado() As Long
    Notificado = m_lNotificado
End Property
Public Property Let Notificado(ByVal Data As Long)
    m_lNotificado = Data
End Property

Public Property Get Visible() As Boolean
    Visible = m_bVisible
End Property
Public Property Let Visible(ByVal Data As Boolean)
    m_bVisible = Data
End Property

Public Property Get Origen() As OrigenNotificadoAccion
    Origen = m_iOrigen
End Property
Public Property Let Origen(ByVal Data As OrigenNotificadoAccion)
    m_iOrigen = Data
End Property

Public Property Get DenominacionesCampo() As CMultiidiomas
    Set DenominacionesCampo = m_oDenominacionesCampo
End Property
Public Property Set DenominacionesCampo(ByVal dato As CMultiidiomas)
    Set m_oDenominacionesCampo = dato
End Property

Public Property Get Desglose() As Boolean
    Desglose = m_bDesglose
End Property
Public Property Let Desglose(ByVal Data As Boolean)
    m_bDesglose = Data
End Property

Private Sub Class_Terminate()
    Set moConexion = Nothing
End Sub


Private Function IBaseDatos_AnyadirABaseDatos() As TipoErrorSummit

End Function

Private Function IBaseDatos_CambiarCodigo(CodigoNuevo As Variant) As TipoErrorSummit

End Function

Private Sub IBaseDatos_CancelarEdicion()

End Sub

Private Function IBaseDatos_ComprobarExistenciaEnBaseDatos() As Boolean

End Function

Private Function IBaseDatos_EliminarDeBaseDatos() As TipoErrorSummit

End Function

Private Function IBaseDatos_FinalizarEdicionEliminando() As TipoErrorSummit

End Function

Private Function IBaseDatos_FinalizarEdicionModificando() As TipoErrorSummit
Dim TESError As TipoErrorSummit
Dim sConsulta As String
Dim tableName As String
Dim sVisible As String


TESError.NumError = TESnoerror

'********* Precondicion *******************
If moConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CNotificadoAccionCampo.FinalizarEdicionModificando", "No se ha establecido la conexion"
End If
'******************************************

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:

    If m_iOrigen = OrigenNotificadoAccion.Enlace Then
        tableName = "PM_NOTIFICADO_ENLACE_CAMPOS"
    ElseIf m_iOrigen = OrigenNotificadoAccion.Accion Then
        tableName = "PM_NOTIFICADO_ACCION_CAMPOS"
    Else
        tableName = "PM_NOTIFICADO_AVISO_EXPIRACION_CAMPOS"
    End If

    ''' Actualizar
    If m_bVisible Then sVisible = "1" Else sVisible = "0"
    sConsulta = "UPDATE " & tableName & " SET VISIBLE=" & sVisible & " WHERE CAMPO=" & m_lCampo & " AND NOTIFICADO=" & m_lNotificado
    
    moConexion.ADOCon.Execute sConsulta
    If moConexion.ADOCon.Errors.Count > 0 Then
        GoTo ERROR
    End If
 
    IBaseDatos_FinalizarEdicionModificando = TESError
    
    Exit Function
    

Error_Cls:
    
    IBaseDatos_FinalizarEdicionModificando = basErrores.TratarError(moConexion.ADOCon.Errors)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CNotificadoAccionCampo", "IBaseDatos_FinalizarEdicionModificando", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If
    
End Function

Private Function IBaseDatos_IniciarEdicion(Optional ByVal Bloquear As Boolean, Optional ByVal UsuarioBloqueo As String) As TipoErrorSummit
 Dim TESError As TipoErrorSummit
Dim tableName As String

TESError.NumError = TESnoerror

'******************* Precondicion *******************
If moConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CNotificadoAccionCampo.IniciarEdicion", "No se ha establecido la conexion"
End If
'*****************************************************

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

    If m_iOrigen = OrigenNotificadoAccion.Enlace Then
        tableName = "PM_NOTIFICADO_ENLACE_CAMPOS"
    ElseIf m_iOrigen = OrigenNotificadoAccion.Accion Then
        tableName = "PM_NOTIFICADO_ACCION_CAMPOS"
    Else
        tableName = "PM_NOTIFICADO_AVISO_EXPIRACION_CAMPOS"
    End If

    Set m_adores = New adodb.Recordset
    m_adores.Open "SELECT NC.VISIBLE, C.SUBTIPO FROM " & tableName & " NC WITH (NOLOCK)" & _
        " INNER JOIN FORM_CAMPO C WITH (NOLOCK) ON C.ID=NC.CAMPO AND C.BAJALOG=0 " & _
        " WHERE NC.CAMPO=" & m_lCampo & " AND NC.NOTIFICADO=" & m_lNotificado, moConexion.ADOCon, adOpenKeyset, adLockReadOnly
    
    If m_adores.eof Then
        m_adores.Close
        Set m_adores = Nothing
        TESError.NumError = TESDatoEliminado
        TESError.Arg1 = 180
        IBaseDatos_IniciarEdicion = TESError
        Exit Function
    End If
    m_bVisible = (m_adores.Fields("VISIBLE").Value = 1)
    m_bDesglose = (m_adores.Fields("SUBTIPO").Value = 9)
    
    m_adores.Close
    Set m_adores = Nothing
    
    IBaseDatos_IniciarEdicion = TESError
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CNotificadoAccionCampo", "IBaseDatos_IniciarEdicion", ERR, Erl)
      Exit Function
   End If

End Function






