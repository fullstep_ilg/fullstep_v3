VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CLineaDesglose"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

Implements IBaseDatos

Private m_oConexion As CConexion

Private m_lLinea As Long
Private m_vIndice As Variant
Private m_vFecAct As Variant
Private m_vValorNum As Variant
Private m_vValorText As Variant
Private m_vValorFec As Variant
Private m_vValorBool As Variant

Private m_oCampoHijo As CFormItem

Private m_adores As adodb.Recordset

Private m_oAdjuntos As CCampoAdjuntos

'Para las instancias:
Private m_oInstancia As CInstancia
Private m_bModificado As Boolean
Private m_bEliminado As Boolean
Private m_bAnyadido As Boolean

Public Property Get Modificado() As Boolean
    Modificado = m_bModificado
End Property
Public Property Let Modificado(ByVal dato As Boolean)
    m_bModificado = dato
End Property

Public Property Get Eliminado() As Boolean
    Eliminado = m_bEliminado
End Property
Public Property Let Eliminado(ByVal dato As Boolean)
    m_bEliminado = dato
End Property

Public Property Get Anyadido() As Boolean
    Anyadido = m_bAnyadido
End Property
Public Property Let Anyadido(ByVal dato As Boolean)
    m_bAnyadido = dato
End Property

Public Property Get Instancia() As CInstancia
    Set Instancia = m_oInstancia
End Property
Public Property Set Instancia(ByVal oDato As CInstancia)
    Set m_oInstancia = oDato
End Property

Public Property Get Adjuntos() As CCampoAdjuntos
    Set Adjuntos = m_oAdjuntos
End Property
Public Property Set Adjuntos(ByVal dato As CCampoAdjuntos)
    Set m_oAdjuntos = dato
End Property

Public Property Get CampoHijo() As CFormItem
    Set CampoHijo = m_oCampoHijo
End Property
Public Property Set CampoHijo(ByVal oCampoHijo As CFormItem)
    Set m_oCampoHijo = oCampoHijo
End Property

Public Property Let ValorNum(ByVal vData As Variant)
    m_vValorNum = vData
End Property
Public Property Get ValorNum() As Variant
    ValorNum = m_vValorNum
End Property

Public Property Let ValorText(ByVal vData As Variant)
    m_vValorText = vData
End Property
Public Property Get ValorText() As Variant
    ValorText = m_vValorText
End Property

Public Property Let ValorFec(ByVal vData As Variant)
    m_vValorFec = vData
End Property
Public Property Get ValorFec() As Variant
    ValorFec = m_vValorFec
End Property

Public Property Let ValorBool(ByVal vData As Variant)
    m_vValorBool = vData
End Property
Public Property Get ValorBool() As Variant
    ValorBool = m_vValorBool
End Property

Public Property Let Linea(ByVal dato As Long)
    m_lLinea = dato
End Property
Public Property Get Linea() As Long
    Linea = m_lLinea
End Property

Public Property Get FecAct() As Variant
    FecAct = m_vFecAct
End Property
Public Property Let FecAct(ByVal Data As Variant)
    m_vFecAct = Data
End Property

Public Property Let Indice(ByVal varIndice As Variant)
    m_vIndice = varIndice
End Property
Public Property Get Indice() As Variant
    Indice = m_vIndice
End Property

Friend Property Set Conexion(ByVal vData As CConexion)

    ''' * Objetivo: Dar valor a la conexion de la Destino
    ''' * Recibe: Conexion
    ''' * Devuelve: Nada

    Set m_oConexion = vData
    
End Property
Friend Property Get Conexion() As CConexion

    ''' * Objetivo: Devolver la conexion de la Destino
    ''' * Recibe: Nada
    ''' * Devuelve: Conexion

    Set Conexion = m_oConexion
    
End Property

Private Sub Class_Terminate()
    Set m_oInstancia = Nothing
    Set m_oCampoHijo = Nothing
    Set m_oAdjuntos = Nothing
    Set m_oConexion = Nothing
End Sub

Private Function IBaseDatos_AnyadirABaseDatos() As TipoErrorSummit
Dim sConsulta As String
Dim TESError As TipoErrorSummit
Dim AdoRes As adodb.Recordset

'********* Precondicion *******************
If m_oConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CLineaDesglose.IBaseDatos_AnyadirABaseDatos", "No se ha establecido la conexion"
    Exit Function
End If

'******************************************
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
    
    TESError.NumError = TESnoerror
    
    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
    
    'Inserta el campo en BD:
    
    If Not m_oInstancia Is Nothing Then
        sConsulta = "INSERT INTO COPIA_LINEA_DESGLOSE (LINEA,CAMPO_PADRE,CAMPO_HIJO,VALOR_NUM,VALOR_TEXT,VALOR_FEC,VALOR_BOOL)"
        sConsulta = sConsulta & " VALUES (" & m_lLinea & "," & m_oCampoHijo.CampoPadre.Id & "," & m_oCampoHijo.Id & "," & DblToSQLFloat(m_vValorNum)
        sConsulta = sConsulta & "," & StrToSQLNULL(m_vValorText) & "," & DateToSQLDate(m_vValorFec) & "," & BooleanToSQLBinary(m_vValorBool) & ")"
        m_oConexion.ADOCon.Execute sConsulta
        If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
        Set AdoRes = New adodb.Recordset
        AdoRes.Open "SELECT FECACT FROM COPIA_LINEA_DESGLOSE WHERE LINEA=" & m_lLinea & " AND CAMPO_PADRE=" & m_oCampoHijo.CampoPadre.Id & " AND CAMPO_HIJO=" & m_oCampoHijo.Id, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
        
    Else
        sConsulta = "INSERT INTO LINEA_DESGLOSE (LINEA,CAMPO_PADRE,CAMPO_HIJO,VALOR_NUM,VALOR_TEXT,VALOR_FEC,VALOR_BOOL)"
        sConsulta = sConsulta & " VALUES (" & m_lLinea & "," & m_oCampoHijo.CampoPadre.Id & "," & m_oCampoHijo.Id & "," & DblToSQLFloat(m_vValorNum)
        sConsulta = sConsulta & "," & StrToSQLNULL(m_vValorText) & "," & DateToSQLDate(m_vValorFec) & "," & BooleanToSQLBinary(m_vValorBool) & ")"
        m_oConexion.ADOCon.Execute sConsulta
        If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
        Set AdoRes = New adodb.Recordset
        AdoRes.Open "SELECT FECACT FROM LINEA_DESGLOSE WHERE LINEA=" & m_lLinea & " AND CAMPO_PADRE=" & m_oCampoHijo.CampoPadre.Id & " AND CAMPO_HIJO=" & m_oCampoHijo.Id, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
     
    End If
    
    m_vFecAct = AdoRes(0).Value
    AdoRes.Close
    Set AdoRes = Nothing
    
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    IBaseDatos_AnyadirABaseDatos = TESError
    Exit Function

Error_Cls:
    IBaseDatos_AnyadirABaseDatos = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    
    m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"

    Set AdoRes = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        'Call g_oErrores.TratarError("Class Module", "CLineaDesglose", "IBaseDatos_AnyadirABaseDatos", ERR, Erl)
        GoTo Error_Cls
        Exit Function
    End If

End Function

Private Function IBaseDatos_CambiarCodigo(CodigoNuevo As Variant) As TipoErrorSummit

End Function

Private Sub IBaseDatos_CancelarEdicion()
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If Not m_adores Is Nothing Then
    m_adores.Close
    Set m_adores = Nothing
End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CLineaDesglose", "IBaseDatos_CancelarEdicion", ERR, Erl)
        Exit Sub
    End If
End Sub

Private Function IBaseDatos_ComprobarExistenciaEnBaseDatos() As Boolean
    
End Function

Private Function IBaseDatos_EliminarDeBaseDatos() As TipoErrorSummit
Dim sConsulta As String
Dim TESError As TipoErrorSummit
Dim AdoRes As adodb.Recordset

'********* Precondicion *******************
If m_oConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CLineaDesglose.EliminarDeBaseDatos", "No se ha establecido la conexion"
    Exit Function
End If

'******************************************
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
    
    TESError.NumError = TESnoerror
    
    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"

    If Not m_oInstancia Is Nothing Then
        sConsulta = "DELETE FROM COPIA_LINEA_DESGLOSE WHERE LINEA=" & m_lLinea & " AND CAMPO_PADRE=" & m_oCampoHijo.CampoPadre.Id
    Else
        sConsulta = "DELETE FROM LINEA_DESGLOSE WHERE LINEA=" & m_lLinea & " AND CAMPO_PADRE=" & m_oCampoHijo.CampoPadre.Id
    End If
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    IBaseDatos_EliminarDeBaseDatos = TESError
    Exit Function

Error_Cls:
    
    IBaseDatos_EliminarDeBaseDatos = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    
    m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
    On Error Resume Next
    If Not AdoRes Is Nothing Then
        AdoRes.Close
        Set AdoRes = Nothing
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        'Call g_oErrores.TratarError("Class Module", "CLineaDesglose", "IBaseDatos_EliminarDeBaseDatos", ERR, Erl)
        GoTo Error_Cls
        Exit Function
    End If
End Function

Private Function IBaseDatos_FinalizarEdicionEliminando() As TipoErrorSummit

End Function

Private Function IBaseDatos_FinalizarEdicionModificando() As TipoErrorSummit
Dim sConsulta As String
Dim TESError As TipoErrorSummit
Dim AdoRes As adodb.Recordset
Dim oValorLista As CCampoValorLista
Dim sLista As String
Dim sListaDen As String
Dim bEliminarLista As Boolean
Dim sConsultaValor As String
Dim oDen As CMultiidioma
Dim sDen As String

'********* Precondicion *******************
If m_oConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CLineaDesglose.IBaseDatos_FinalizarEdicionModificando", "No se ha establecido la conexion"
    Exit Function
End If
'******************************************
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
    
    TESError.NumError = TESnoerror
    
    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"

    'Comprobamos si ha habido cambios en  otra sesi�n
    Set m_adores = New adodb.Recordset
    sConsulta = "SELECT FECACT,VALOR_TEXT,VALOR_NUM,VALOR_FEC,VALOR_BOOL FROM LINEA_DESGLOSE WITH (NOLOCK) WHERE LINEA=" & m_lLinea & " AND CAMPO_PADRE=" & m_oCampoHijo.CampoPadre.Id & " AND CAMPO_HIJO=" & m_oCampoHijo.Id
    m_adores.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
    
    If m_adores.eof Then
        m_adores.Close
        Set m_adores = Nothing
        TESError.NumError = TESDatoEliminado
        TESError.Arg1 = 166 ''"Linea desglose
        IBaseDatos_FinalizarEdicionModificando = TESError
        m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
        Exit Function
    End If
    
    If DateToSQLTimeDate(m_vFecAct) <> DateToSQLTimeDate(m_adores("FECACT").Value) Then
        m_adores.Close
        Set m_adores = Nothing
        TESError.NumError = TESInfActualModificada
        TESError.Arg1 = 166 ''"Linea desglose
        IBaseDatos_FinalizarEdicionModificando = TESError
        m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
        Exit Function
    End If
    
    
    'Comprueba si tiene que eliminar los valores:
    sConsultaValor = ""
    If NullToDbl0(m_adores("VALOR_NUM").Value) <> NullToDbl0(m_vValorNum) Then
        sConsultaValor = ",VALOR_NUM=" & DblToSQLFloat(m_vValorNum)
    End If
    If NullToStr(m_adores("VALOR_TEXT").Value) <> NullToStr(m_vValorText) Then
        sConsultaValor = sConsultaValor & ",VALOR_TEXT=" & StrToSQLNULL(m_vValorText)
    End If
    If IsNull(m_vValorFec) And Not IsNull(m_adores("VALOR_FEC").Value) Then
        sConsultaValor = sConsultaValor & ",VALOR_FEC=NULL"
    ElseIf IsNull(m_adores("VALOR_FEC").Value) And Not IsNull(m_vValorFec) Then
        sConsultaValor = sConsultaValor & ",VALOR_FEC=" & DateToSQLDate(m_vValorFec)
    ElseIf m_adores("VALOR_FEC").Value <> m_vValorFec Then
        sConsultaValor = sConsultaValor & ",VALOR_FEC=" & DateToSQLDate(m_vValorFec)
    End If
    If IsNull(m_vValorBool) And Not IsNull(m_adores("VALOR_BOOL").Value) Then
        sConsultaValor = sConsultaValor & ",VALOR_BOOL=NULL"
    ElseIf IsNull(m_adores("VALOR_BOOL").Value) And Not IsNull(m_vValorBool) Then
        sConsultaValor = sConsultaValor & ",VALOR_BOOL=" & BooleanToSQLBinary(m_vValorBool)
    ElseIf m_adores("VALOR_BOOL").Value <> m_vValorBool Then
        sConsultaValor = sConsultaValor & ",VALOR_BOOL=" & BooleanToSQLBinary(m_vValorBool)
    End If

    sConsultaValor = Mid(sConsultaValor, 2)
    'Actualiza el campo
    If sConsultaValor <> "" Then
        sConsulta = "UPDATE LINEA_DESGLOSE SET " & sConsultaValor & " WHERE LINEA=" & m_lLinea & " AND CAMPO_PADRE=" & m_oCampoHijo.CampoPadre.Id & " AND CAMPO_HIJO=" & m_oCampoHijo.Id
        m_oConexion.ADOCon.Execute sConsulta
        If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    End If
    
    'Recogemos el nuevo valor de FECACT
    m_adores.Requery
    m_vFecAct = m_adores("FECACT").Value
    m_adores.Close
    Set m_adores = Nothing
    
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    IBaseDatos_FinalizarEdicionModificando = TESError
    Exit Function

Error_Cls:
    
    IBaseDatos_FinalizarEdicionModificando = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    
     If Not m_adores Is Nothing Then
        m_adores.Close
        Set m_adores = Nothing
    End If
    
    m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
    On Error Resume Next
    If Not AdoRes Is Nothing Then
        AdoRes.Close
        Set AdoRes = Nothing
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        'Call g_oErrores.TratarError("Class Module", "CLineaDesglose", "IBaseDatos_FinalizarEdicionModificando", ERR, Erl)
        GoTo Error_Cls
        Exit Function
    End If
End Function

Private Function IBaseDatos_IniciarEdicion(Optional ByVal Bloquear As Boolean, Optional ByVal UsuarioBloqueo As String) As TipoErrorSummit
Dim TESError As TipoErrorSummit

'******************* Precondicion *******************
If m_oConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CLineaDesglose.IniciarEdicion", "No se ha establecido la conexion"
    Exit Function
End If
'*****************************************************
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
TESError.NumError = TESnoerror


    Set m_adores = New adodb.Recordset
    
    If Not m_oInstancia Is Nothing Then
        m_adores.Open "SELECT FECACT FROM COPIA_LINEA_DESGLOSE WHERE LINEA =" & m_lLinea & " AND CAMPO_PADRE=" & m_oCampoHijo.CampoPadre.Id & " AND CAMPO_HIJO=" & m_oCampoHijo.Id, m_oConexion.ADOCon, adOpenKeyset, adLockOptimistic
        
        If m_adores.eof Then
            m_adores.Close
            Set m_adores = Nothing
            TESError.NumError = TESDatoEliminado
            TESError.Arg1 = 170 ''"COPIA Linea desglose
            TESError.Arg2 = 1
            IBaseDatos_IniciarEdicion = TESError
            Exit Function
        End If
    Else
        m_adores.Open "SELECT FECACT FROM LINEA_DESGLOSE WHERE LINEA =" & m_lLinea & " AND CAMPO_PADRE=" & m_oCampoHijo.CampoPadre.Id & " AND CAMPO_HIJO=" & m_oCampoHijo.Id, m_oConexion.ADOCon, adOpenKeyset, adLockOptimistic
        
        If m_adores.eof Then
            m_adores.Close
            Set m_adores = Nothing
            TESError.NumError = TESDatoEliminado
            TESError.Arg1 = 166 ''"Linea desglose
            TESError.Arg2 = 1
            IBaseDatos_IniciarEdicion = TESError
            Exit Function
        End If
    End If
    
    m_vFecAct = m_adores.Fields("FECACT").Value
    
    IBaseDatos_IniciarEdicion = TESError
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CLineaDesglose", "IBaseDatos_IniciarEdicion", ERR, Erl)
        Exit Function
    End If
End Function

Public Sub CargarAdjuntos()
Dim ador As New adodb.Recordset
Dim sConsulta As String
Dim lIndice As Long
Dim oPersona As CPersona

    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CLineaDesglose.CargarAdjuntos", "No se ha establecido la conexion"
        Exit Sub
    End If
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

    If Not m_oInstancia Is Nothing Then
        sConsulta = "SELECT ID,CA.FECALTA,CA.NOM,COMENT,CA.IDIOMA,DATASIZE,CA.PER,PER.NOM AS USUNOM,PER.APE AS USUAPE"
        sConsulta = sConsulta & " FROM COPIA_LINEA_DESGLOSE_ADJUN CA WITH (NOLOCK)"
    Else
        sConsulta = "SELECT ID,CA.FECALTA,CA.NOM,COMENT,RUTA,CA.IDIOMA,DATASIZE,CA.PER,PER.NOM AS USUNOM,PER.APE AS USUAPE"
        sConsulta = sConsulta & " FROM LINEA_DESGLOSE_ADJUN CA WITH (NOLOCK)"
    End If
    
    sConsulta = sConsulta & " LEFT JOIN PER WITH (NOLOCK) ON CA.PER=PER.COD"
    sConsulta = sConsulta & " WHERE LINEA=" & m_lLinea & " AND CAMPO_PADRE=" & m_oCampoHijo.CampoPadre.Id & " AND CAMPO_HIJO=" & m_oCampoHijo.Id
        
    If m_oInstancia Is Nothing Then
        sConsulta = sConsulta & " AND FAVORITO = 0"
    End If
        
    ador.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly

    Set m_oAdjuntos = Nothing
    Set m_oAdjuntos = New CCampoAdjuntos
    Set m_oAdjuntos.Conexion = m_oConexion
        
    While Not ador.eof
        If Not IsNull(ador("PER").Value) Then
            Set oPersona = New CPersona
            With oPersona
                .Cod = ador("PER").Value
                .Nombre = ador.Fields("USUNOM")
                If Not IsNull(ador.Fields("USUAPE")) Then
                    .Apellidos = ador("USUAPE").Value
                Else
                    .Apellidos = Null
                End If
            End With
        End If
        
        If Not m_oInstancia Is Nothing Then
            m_oAdjuntos.Add ador("ID").Value, ador("FECALTA").Value, ador("NOM").Value, NullToDbl0(ador("DATASIZE").Value), ador("COMENT").Value, , ador("IDIOMA").Value, oPersona, , Me, , , , m_oInstancia
        Else
            m_oAdjuntos.Add ador("ID").Value, ador("FECALTA").Value, ador("NOM").Value, NullToDbl0(ador("DATASIZE").Value), ador("COMENT").Value, ador("RUTA").Value, ador("IDIOMA").Value, oPersona, , Me
        End If
        
        Set oPersona = Nothing
        
        ador.MoveNext
    Wend
    
    ador.Close
    Set ador = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CLineaDesglose", "CargarAdjuntos", ERR, Erl)
        Exit Sub
    End If
            
End Sub


