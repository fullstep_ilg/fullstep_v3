VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CPlantConfVistasProce"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CPlantConfVistasProce **********************************
'*             Autor : Mertxe Martin
'*             Creada : 01/12/2003
'****************************************************************

Option Explicit

Private mCol As Collection
Private m_oConexion As CConexion
Private m_oConexionServer As CConexionDeUseServer
Private m_bEOF As Boolean

Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum

''' <summary>
''' Carga los combos de las vistas
''' </summary>
'''<param name="oPlantilla">objeto que contiene la plantilla</param>
'''<param optional name="sUsuario">Usuario</param>
''' <remarks>Llamada desde: frmADJ.VistaDespuesDeEliminar;
''' frmConfiguracionVista.CargarComboVistas; frmADJItem.CargarComboVistas;
''' Tiempo m�ximo: 1 sec </remarks>
''' <revision>JVS 26/12/2011</revision>
Public Sub CargarCombosVistas(Optional ByVal m_oPlantilla As CPlantilla, Optional ByVal sUsuario As String)

 Dim sConsulta As String
 Dim AdoRes As New adodb.Recordset
 Dim adofldNombre As adodb.Field
 Dim adofldVista As adodb.Field

 
 '********* Precondicion **************************************
 If m_oConexion Is Nothing Then
    ERR.Raise vbObjectError + 613, "CProceso", "No se ha establecido la conexion"
    Exit Sub
 End If
 '*************************************************************
 
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
  
   ' vista inicial
     sConsulta = "SELECT VISTA=0,USU AS NOM FROM CONF_VISTA_INICIAL_PROCE WITH(NOLOCK) WHERE USU='" & DblQuote(sUsuario) & "' "
     sConsulta = sConsulta & " UNION "

    'vista de plantilla
    sConsulta = sConsulta & " SELECT VISTA,NOM FROM PLANTILLA_CONF_VISTAS_PROCE WITH(NOLOCK) "
    sConsulta = sConsulta & " WHERE PLANTILLA_CONF_VISTAS_PROCE.PLANT ='" & DblQuote(m_oPlantilla.Id) & "'"
 


 AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly

 Set adofldNombre = AdoRes.Fields("NOM")
 Set adofldVista = AdoRes.Fields("VISTA")

 If Not AdoRes.eof Then
    AdoRes.MoveFirst
 End If
 
 While Not AdoRes.eof
     Me.AddComboVista adofldNombre.Value, adofldVista.Value
     AdoRes.MoveNext
 Wend

 AdoRes.Close
 Set adofldNombre = Nothing
 Set adofldVista = Nothing
 Set AdoRes = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CPlantConfVistasProce", "CargarCombosVistas", ERR, Erl)
      Exit Sub
   End If
End Sub

Public Function AddComboVista(Optional ByVal sNombre As String, _
Optional ByVal iVista As Integer) As CPlantConfVistaProce
    
    'create a new object
    Dim objnewmember As CPlantConfVistaProce
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New CPlantConfVistaProce
           
    objnewmember.Vista = iVista
    objnewmember.NombreVista = sNombre
    
    Set objnewmember.Conexion = m_oConexion
    Set objnewmember.ConexionServer = m_oConexionServer
       
    
    mCol.Add objnewmember, CStr(iVista)
        
    'return the object created
    Set AddComboVista = objnewmember
    Set objnewmember = Nothing

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CPlantConfVistasProce", "AddComboVista", ERR, Erl)
      Exit Function
   End If
End Function

''' <summary>
''' A�ade una nueva vista a la colecci�n
''' </summary>
''' <param name="oPlantilla">objeto Plantilla</param>
''' <param name="iVista">numero de Vista</param>
''' <param name="vIndice">Indice</param>
''' <param name="iConsumidoPos">Posici�n de la columna Consumido</param>
''' <param name="bConsumidoVisible">Visibilidad de la columna Consumido</param>
''' <param name="dblConsumidoWidth">Ancho de la columna Consumido</param>
''' <param name="iImportePos">Posici�n de la columna Importe</param>
''' <param name="bImporteVisible">Visibilidad de la columna Importe</param>
''' <param name="dblImporteWidth">Ancho de la columna Importe</param>
''' <param name="iAdjPos">Posici�n de la columna Adjudicado</param>
''' <param name="bAdjVisible">Visibilidad de la columna Adjudicado</param>
''' <param name="dblAdjWidth">Ancho de la columna Adjudicado</param>
''' <param name="iAhorroAdjPorcenPos">Posici�n de la columna % Ahorro Adjudicacion</param>
''' <param name="bAhorroAdjPorcenVisible">Visibilidad de la columna % Ahorro Adjudicacion</param>
''' <param name="dblAhorroAdjPorcenWidth">Ancho de la columna % Ahorro Adjudicacion</param>
''' <param name="iAhorroAdjPos">Posici�n de la columna Ahorro Adjudicacion</param>
''' <param name="bAhorroAdjVisible">Visibilidad de la columna Ahorro Adjudicacion</param>
''' <param name="dblAhorroAdjWidth">Ancho de la columna Ahorro Adjudicacion</param>
''' <param name="iAhorroOfePorcenPos">Posici�n de la columna % Ahorro sobre Ofertado</param>
''' <param name="bAhorroOfePorcenVisible">Visibilidad de la columna % Ahorro sobre Ofertado</param>
''' <param name="dblAhorroOfePorcenWidth">Ancho de la columna % Ahorro sobre Ofertado</param>
''' <param name="iAhorroOfePos">Posici�n de la columna Ahorro sobre Ofertado</param>
''' <param name="bAhorroOfeVisible">Visibilidad de la columna Ahorro sobre Ofertado</param>
''' <param name="dblAhorroOfeWidth">Ancho de la columna Ahorro sobre Ofertado</param>
''' <param name="iTipoVision">Tipo vista</param>
''' <param name="dblGrupoWidth">Ancho del grupo</param>
''' <param name="vFecAct">Fecha de actualizacion</param>
''' <param name="oConfVistasAtribs">objeto vista atributos</param>
''' <param name="dblAnchoFila">Ancho de la fila</param>
''' <param name="dblGrupo0Width">Ancho del grupo 0</param>
''' <param name="dblCodWidth">Ancho del codigo</param>
''' <param name="dblDenWidth">Ancho de la denominaci�n</param>
''' <param name="sNombreVista">Nombre vista</param>
''' <param name="iOcultarGrCerrados">si se incluyen grupos cerrados</param>
''' <param name="iExcluirGrCerradosResult">si se exluyen grupos cerrados de resulatdos</param>
''' <param name="iOcultarNoAdj">si se ocultan los no Adjudicados</param>
''' <param name="iOcultarProvSinOfe">Ocultar Prove Sin Ofe</param>
''' <param name="iDecResult">numero decimales resultados</param>
''' <param name="iDecPorcen">numero decimales porcentala</param>
''' <param name="iSolicVinculadaPos">Posici�n de la columna solic vinculada</param>
''' <param name="bSolicVinculadaVisible">Visibilidad de la columna solic vinculada</param>
''' <param name="dblSolicVinculadaWidth">Ancho de la columna solic vinculada</param>
''' <returns>nueva vista</returns>
''' <remarks>Llamada desde: frmADJ/frmRESREU ; Tiempo m�ximo: 0,2</remarks>
Public Function Add(Optional ByVal oPlantilla As CPlantilla, Optional ByVal iVista As Integer, Optional ByVal vIndice As Variant, _
Optional ByVal iConsumidoPos As Integer, Optional ByVal bConsumidoVisible As Boolean, Optional ByVal dblConsumidoWidth As Double, _
Optional ByVal iImportePos As Integer, Optional ByVal bImporteVisible As Boolean, Optional ByVal dblImporteWidth As Double, _
Optional ByVal iAdjPos As Integer, Optional ByVal bAdjVisible As Boolean, Optional ByVal dblAdjWidth As Double, _
Optional ByVal iAhorroAdjPorcenPos As Integer, Optional ByVal bAhorroAdjPorcenVisible As Boolean, Optional ByVal dblAhorroAdjPorcenWidth As Double, _
Optional ByVal iAhorroAdjPos As Integer, Optional ByVal bAhorroAdjVisible As Boolean, Optional ByVal dblAhorroAdjWidth As Double, _
Optional ByVal iAhorroOfePorcenPos As Integer, Optional ByVal bAhorroOfePorcenVisible As Boolean, Optional ByVal dblAhorroOfePorcenWidth As Double, _
Optional ByVal iAhorroOfePos As Integer, Optional ByVal bAhorroOfeVisible As Boolean, Optional ByVal dblAhorroOfeWidth As Double, _
Optional ByVal iTipoVision As Integer, _
Optional ByVal dblGrupoWidth As Double, _
Optional ByVal vFecAct As Variant, Optional ByVal oConfVistasAtribs As CPlantConfVistasProceAtrib, _
Optional ByVal dblAnchoFila As Double, Optional ByVal dblGrupo0Width As Double, _
Optional ByVal dblCodWidth As Double, Optional ByVal dblDenWidth As Double, _
Optional ByVal sNombreVista As String, Optional ByVal iOcultarGrCerrados As Integer, Optional ByVal iExcluirGrCerradosResult As Integer, _
Optional ByVal iOcultarNoAdj As Integer, Optional ByVal iOcultarProvSinOfe As Integer, Optional ByVal iDecResult As Integer, _
Optional ByVal iDecPorcen As Integer, Optional ByVal iSolicVinculadaPos As Integer, Optional ByVal bSolicVinculadaVisible As Boolean, _
Optional ByVal dblSolicVinculadaWidth As Double) As CPlantConfVistaProce
    
    'create a new object
    Dim objnewmember As CPlantConfVistaProce
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New CPlantConfVistaProce
   
    If Not oPlantilla Is Nothing Then
        Set objnewmember.Plantilla = oPlantilla
    End If
    
    objnewmember.ConsumidoPos = iConsumidoPos
    objnewmember.ConsumidoVisible = bConsumidoVisible
    objnewmember.ConsumidoWidth = dblConsumidoWidth
    
    objnewmember.ImportePos = iImportePos
    objnewmember.ImporteVisible = bImporteVisible
    objnewmember.ImporteWidth = dblImporteWidth
    
    objnewmember.AdjPos = iAdjPos
    objnewmember.AdjVisible = bAdjVisible
    objnewmember.AdjWidth = dblAdjWidth
    
    objnewmember.AhorroAdjPorcenPos = iAhorroAdjPorcenPos
    objnewmember.AhorroAdjPorcenVisible = bAhorroAdjPorcenVisible
    objnewmember.AhorroAdjPorcenWidth = dblAhorroAdjPorcenWidth
    
    objnewmember.AhorroAdjPos = iAhorroAdjPos
    objnewmember.AhorroAdjVisible = bAhorroAdjVisible
    objnewmember.AhorroAdjWidth = dblAhorroAdjWidth
    
    objnewmember.AhorroOfePorcenPos = iAhorroOfePorcenPos
    objnewmember.AhorroOfePorcenVisible = bAhorroOfePorcenVisible
    objnewmember.AhorroOfePorcenWidth = dblAhorroOfePorcenWidth
    
    objnewmember.AhorroOfePos = iAhorroOfePos
    objnewmember.AhorroOfeVisible = bAhorroOfeVisible
    objnewmember.AhorroOfeWidth = dblAhorroOfeWidth
    
    objnewmember.GrupoWidth = dblGrupoWidth
    
    objnewmember.TipoVision = iTipoVision
    
    objnewmember.AnchoFila = dblAnchoFila
    objnewmember.Grupo0Width = dblGrupo0Width
    objnewmember.CodWidth = dblCodWidth
    objnewmember.DenWidth = dblDenWidth
    
    objnewmember.Vista = iVista
    
    objnewmember.NombreVista = sNombreVista
    objnewmember.OcultarGrCerrados = iOcultarGrCerrados
    objnewmember.ExcluirGrCerradosResult = iExcluirGrCerradosResult
    objnewmember.OcultarNoAdj = iOcultarNoAdj
    objnewmember.OcultarProvSinOfe = iOcultarProvSinOfe
    objnewmember.DecResult = iDecResult
    objnewmember.DecPorcen = iDecPorcen
    
    objnewmember.SolicVinculadaPos = iSolicVinculadaPos
    objnewmember.SolicVinculadaVisible = bSolicVinculadaVisible
    objnewmember.SolicVinculadaWidth = dblSolicVinculadaWidth
    
    Set objnewmember.Conexion = m_oConexion
    Set objnewmember.ConexionServer = m_oConexionServer
       
    If Not IsMissing(vIndice) And Not IsNull(vIndice) Then
        objnewmember.Indice = vIndice
        mCol.Add objnewmember, CStr(vIndice)
    Else
        mCol.Add objnewmember, CStr(iVista)
    End If
    
    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CPlantConfVistasProce", "Add", ERR, Erl)
      Exit Function
   End If

End Function

Friend Property Set Conexion(ByVal con As CConexion)
    Set m_oConexion = con
End Property

Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property

Friend Property Set ConexionServer(ByVal con As CConexionDeUseServer)
    Set m_oConexionServer = con
End Property

Friend Property Get ConexionServer() As CConexionDeUseServer
    Set ConexionServer = m_oConexionServer
End Property

Public Property Get Count() As Long
    If mCol Is Nothing Then
        Count = 0
    Else
         Count = mCol.Count
    End If
End Property

Public Property Get eof() As Boolean
    eof = m_bEOF
End Property

Friend Property Let eof(ByVal b As Boolean)
    m_bEOF = b
End Property

Public Property Get Item(vntIndexKey As Variant) As CPlantConfVistaProce
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
     Set NewEnum = mCol.[_NewEnum]
End Property

Public Sub Remove(vntIndexKey As Variant)

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    mCol.Remove vntIndexKey
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CPlantConfVistasProce", "Remove", ERR, Erl)
      Exit Sub
   End If

End Sub

Private Sub Class_Initialize()
    Set mCol = New Collection
End Sub

Private Sub Class_Terminate()
    'Proceroys collection when this class is terminated
    On Error Resume Next
    Set mCol = Nothing
    Set m_oConexion = Nothing
End Sub


''' <summary>
''' Carga las vistas del proceso
''' </summary>
'''<param name="oPlantilla">objeto que contiene la plantilla</param>
'''<param optional name="sUsuario">Usuario</param>
''' <remarks>Llamada desde: frmADJ.ConfiguracionVistaActual;
''' frmConfiguracionVista.ConfiguracionVistaActual;
''' Tiempo m�ximo: 1 sec </remarks>
''' <revision>JVS 26/12/2011</revision>
Public Sub CargarTodasLasVistas(ByVal oPlantilla As CPlantilla, Optional ByVal iVista As Integer, Optional ByVal UsarIndice As Boolean)
    Dim AdoRes As New adodb.Recordset
    Dim sConsulta As String
    Dim lIndice As Long
    Dim adofldVista As adodb.Field
    Dim adofldConsumidoPos As adodb.Field
    Dim adofldConsumidoVisible As adodb.Field
    Dim adofldConsumidoWidth As adodb.Field
    Dim adofldImportePos As adodb.Field
    Dim adofldImporteVisible As adodb.Field
    Dim adofldImporteWidth As adodb.Field
    Dim adofldAdjPos As adodb.Field
    Dim adofldAdjVisible As adodb.Field
    Dim adofldAdjWidth As adodb.Field
    Dim adofldAhorroAdjPos As adodb.Field
    Dim adofldAhorroAdjVisible As adodb.Field
    Dim adofldAhorroAdjWidth As adodb.Field
    Dim adofldAhorroAdjPorcenPos As adodb.Field
    Dim adofldAhorroAdjPorcenVisible As adodb.Field
    Dim adofldAhorroAdjPorcenWidth As adodb.Field
    Dim adofldAhorroOfePos As adodb.Field
    Dim adofldAhorroOfeVisible As adodb.Field
    Dim adofldAhorroOfeWidth As adodb.Field
    Dim adofldAhorroOfePorcenPos As adodb.Field
    Dim adofldAhorroOfePorcenVisible As adodb.Field
    Dim adofldAhorroOfePorcenWidth As adodb.Field
    Dim adofldTipoVision As adodb.Field
    Dim adofldGrupoWidth As adodb.Field
    Dim adofldAnchoFila As adodb.Field
    Dim adofldGrupo0Width As adodb.Field
    Dim adofldCodWidth As adodb.Field
    Dim adofldDenWidth As adodb.Field
    Dim adofldNombreVista As adodb.Field
    Dim adofldOcultarGrCerrados As adodb.Field
    Dim adofldExcluirGrCerradosResult As adodb.Field
    Dim adofldOcultarNoAdj As adodb.Field
    Dim adofldOcultarProvSinOfe As adodb.Field
    Dim adofldDecResult As adodb.Field
    Dim adofldDecPorcen As adodb.Field
    Dim adofldSolicVinculadaPos As adodb.Field
    Dim adofldSolicVinculadaVisible As adodb.Field
    Dim adofldSolicVinculadaWidth As adodb.Field
    
    '********* Precondicion **************************************
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CConfVistasProce.CargarTodasLasVistas", "No se ha establecido la conexion"
        Exit Sub
    End If
    '*************************************************************
    
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sConsulta = "SELECT PLANT,VISTA,CONSUMIDO_POS,CONSUMIDO_VISIBLE,CONSUMIDO_WIDTH,ADJ_POS,ADJ_VISIBLE,ADJ_WIDTH,AHORR_ADJ_POS,AHORR_ADJ_VISIBLE,AHORR_ADJ_WIDTH,AHORR_ADJ_PORCEN_POS,AHORR_ADJ_PORCEN_VISIBLE,AHORR_ADJ_PORCEN_WIDTH,IMP_POS,IMP_VISIBLE,IMP_WIDTH,AHORR_OFE_POS,AHORR_OFE_VISIBLE,AHORR_OFE_WIDTH,AHORR_OFE_PORCEN_POS,AHORR_OFE_PORCEN_VISIBLE,AHORR_OFE_PORCEN_WIDTH,TIPOVISION,GRUPO_WIDTH,ANCHO_FILA,GRUPO0_WIDTH,COD_WIDTH,DEN_WIDTH,NOM,OCULTAR_GR_CERRADOS,EXCLUIR_GR_CERRADOS_RESULT,OCULTAR_NOADJ,OCULTAR_PROV_SIN_OFE,DEC_RESULT,DEC_PORCEN,SOLIC_VINCULADA_POS,SOLIC_VINCULADA_VISIBLE,SOLIC_VINCULADA_WIDTH FROM PLANTILLA_CONF_VISTAS_PROCE WITH(NOLOCK) WHERE "
    sConsulta = sConsulta & " PLANT=" & oPlantilla.Id
    
    If Not IsMissing(iVista) Then
        sConsulta = sConsulta & " AND VISTA=" & iVista
    End If
    
    sConsulta = sConsulta & " ORDER BY VISTA"
    
    AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
          
    If AdoRes.eof Then
        AdoRes.Close
        Set AdoRes = Nothing
        If mCol Is Nothing Then
            Set mCol = Nothing
            Set mCol = New Collection
        End If
        Exit Sub
          
    Else
        If mCol Is Nothing Then
            Set mCol = Nothing
            Set mCol = New Collection
        End If
        
        Set adofldVista = AdoRes.Fields("VISTA")
        Set adofldConsumidoPos = AdoRes.Fields("CONSUMIDO_POS")
        Set adofldConsumidoVisible = AdoRes.Fields("CONSUMIDO_VISIBLE")
        Set adofldConsumidoWidth = AdoRes.Fields("CONSUMIDO_WIDTH")
        Set adofldImportePos = AdoRes.Fields("IMP_POS")
        Set adofldImporteVisible = AdoRes.Fields("IMP_VISIBLE")
        Set adofldImporteWidth = AdoRes.Fields("IMP_WIDTH")
        Set adofldAdjPos = AdoRes.Fields("ADJ_POS")
        Set adofldAdjVisible = AdoRes.Fields("ADJ_VISIBLE")
        Set adofldAdjWidth = AdoRes.Fields("ADJ_WIDTH")
        Set adofldAhorroAdjPos = AdoRes.Fields("AHORR_ADJ_POS")
        Set adofldAhorroAdjVisible = AdoRes.Fields("AHORR_ADJ_VISIBLE")
        Set adofldAhorroAdjWidth = AdoRes.Fields("AHORR_ADJ_WIDTH")
        Set adofldAhorroAdjPorcenPos = AdoRes.Fields("AHORR_ADJ_PORCEN_POS")
        Set adofldAhorroAdjPorcenVisible = AdoRes.Fields("AHORR_ADJ_PORCEN_VISIBLE")
        Set adofldAhorroAdjPorcenWidth = AdoRes.Fields("AHORR_ADJ_PORCEN_WIDTH")
        Set adofldAhorroOfePos = AdoRes.Fields("AHORR_OFE_POS")
        Set adofldAhorroOfeVisible = AdoRes.Fields("AHORR_OFE_VISIBLE")
        Set adofldAhorroOfeWidth = AdoRes.Fields("AHORR_OFE_WIDTH")
        Set adofldAhorroOfePorcenPos = AdoRes.Fields("AHORR_OFE_PORCEN_POS")
        Set adofldAhorroOfePorcenVisible = AdoRes.Fields("AHORR_OFE_PORCEN_VISIBLE")
        Set adofldAhorroOfePorcenWidth = AdoRes.Fields("AHORR_OFE_PORCEN_WIDTH")
        Set adofldTipoVision = AdoRes.Fields("TIPOVISION")
        Set adofldGrupoWidth = AdoRes.Fields("GRUPO_WIDTH")
        Set adofldAnchoFila = AdoRes.Fields("ANCHO_FILA")
        Set adofldGrupo0Width = AdoRes.Fields("GRUPO0_WIDTH")
        Set adofldCodWidth = AdoRes.Fields("COD_WIDTH")
        Set adofldDenWidth = AdoRes.Fields("DEN_WIDTH")
        Set adofldNombreVista = AdoRes.Fields("NOM")
        Set adofldOcultarGrCerrados = AdoRes.Fields("OCULTAR_GR_CERRADOS")
        Set adofldExcluirGrCerradosResult = AdoRes.Fields("EXCLUIR_GR_CERRADOS_RESULT")
        Set adofldOcultarNoAdj = AdoRes.Fields("OCULTAR_NOADJ")
        Set adofldOcultarProvSinOfe = AdoRes.Fields("OCULTAR_PROV_SIN_OFE")
        Set adofldDecResult = AdoRes.Fields("DEC_RESULT")
        Set adofldDecPorcen = AdoRes.Fields("DEC_PORCEN")
        Set adofldSolicVinculadaPos = AdoRes.Fields("SOLIC_VINCULADA_POS")
        Set adofldSolicVinculadaVisible = AdoRes.Fields("SOLIC_VINCULADA_VISIBLE")
        Set adofldSolicVinculadaWidth = AdoRes.Fields("SOLIC_VINCULADA_WIDTH")
        
        If UsarIndice Then
            
            lIndice = 0
            
            While Not AdoRes.eof
                ' Dejamos en manos del programador cuando asignar la conexion al obejto usuario
                Me.Add oPlantilla, adofldVista.Value, lIndice, adofldConsumidoPos.Value, adofldConsumidoVisible.Value, adofldConsumidoWidth.Value, adofldImportePos.Value, adofldImporteVisible.Value, adofldImporteWidth.Value, adofldAdjPos.Value, adofldAdjVisible.Value, adofldAdjWidth.Value, adofldAhorroAdjPorcenPos.Value, adofldAhorroAdjPorcenVisible.Value, adofldAhorroAdjPorcenWidth.Value, adofldAhorroAdjPos.Value, adofldAhorroAdjVisible.Value, adofldAhorroAdjWidth.Value, adofldAhorroOfePorcenPos.Value, adofldAhorroOfePorcenVisible.Value, adofldAhorroOfePorcenWidth.Value, adofldAhorroOfePos.Value, adofldAhorroOfeVisible.Value, adofldAhorroOfeWidth.Value, adofldTipoVision.Value, adofldGrupoWidth.Value, , , NullToDbl0(adofldAnchoFila.Value), NullToDbl0(adofldGrupo0Width.Value), NullToDbl0(adofldCodWidth.Value), NullToDbl0(adofldDenWidth.Value), _
                adofldNombreVista.Value, adofldOcultarGrCerrados.Value, adofldExcluirGrCerradosResult.Value, adofldOcultarNoAdj.Value, adofldOcultarProvSinOfe.Value, adofldDecResult.Value, adofldDecPorcen.Value, NullToDbl0(adofldSolicVinculadaPos.Value), adofldSolicVinculadaVisible.Value, NullToDbl0(adofldSolicVinculadaWidth.Value)
                AdoRes.MoveNext
                lIndice = lIndice + 1
            Wend
            
        Else
            
            While Not AdoRes.eof
                ' Dejamos en manos del programador cuando asignar la conexion al obejto usuario
                Me.Add oPlantilla, adofldVista.Value, , adofldConsumidoPos.Value, adofldConsumidoVisible.Value, adofldConsumidoWidth.Value, adofldImportePos.Value, adofldImporteVisible.Value, adofldImporteWidth.Value, adofldAdjPos.Value, adofldAdjVisible.Value, adofldAdjWidth.Value, adofldAhorroAdjPorcenPos.Value, adofldAhorroAdjPorcenVisible.Value, adofldAhorroAdjPorcenWidth.Value, adofldAhorroAdjPos.Value, adofldAhorroAdjVisible.Value, adofldAhorroAdjWidth.Value, adofldAhorroOfePorcenPos.Value, adofldAhorroOfePorcenVisible.Value, adofldAhorroOfePorcenWidth.Value, adofldAhorroOfePos.Value, adofldAhorroOfeVisible.Value, adofldAhorroOfeWidth.Value, adofldTipoVision.Value, adofldGrupoWidth.Value, , , NullToDbl0(adofldAnchoFila.Value), NullToDbl0(adofldGrupo0Width.Value), NullToDbl0(adofldCodWidth.Value), NullToDbl0(adofldDenWidth.Value), _
                adofldNombreVista.Value, adofldOcultarGrCerrados.Value, adofldExcluirGrCerradosResult.Value, adofldOcultarNoAdj.Value, adofldOcultarProvSinOfe.Value, adofldDecResult.Value, adofldDecPorcen.Value, NullToDbl0(adofldSolicVinculadaPos.Value), adofldSolicVinculadaVisible.Value, NullToDbl0(adofldSolicVinculadaWidth.Value)
                AdoRes.MoveNext
            Wend
        End If
        
        'Libera los objetos
        Set adofldVista = Nothing
        Set adofldConsumidoPos = Nothing
        Set adofldConsumidoVisible = Nothing
        Set adofldConsumidoWidth = Nothing
        Set adofldImportePos = Nothing
        Set adofldImporteVisible = Nothing
        Set adofldImporteWidth = Nothing
        Set adofldAdjPos = Nothing
        Set adofldAdjVisible = Nothing
        Set adofldAdjWidth = Nothing
        Set adofldAhorroAdjPos = Nothing
        Set adofldAhorroAdjVisible = Nothing
        Set adofldAhorroAdjWidth = Nothing
        Set adofldAhorroAdjPorcenPos = Nothing
        Set adofldAhorroAdjPorcenVisible = Nothing
        Set adofldAhorroAdjPorcenWidth = Nothing
        Set adofldAhorroOfePos = Nothing
        Set adofldAhorroOfeVisible = Nothing
        Set adofldAhorroOfeWidth = Nothing
        Set adofldAhorroOfePorcenPos = Nothing
        Set adofldAhorroOfePorcenVisible = Nothing
        Set adofldAhorroOfePorcenWidth = Nothing
        Set adofldTipoVision = Nothing
        Set adofldGrupoWidth = Nothing
        Set adofldAnchoFila = Nothing
        Set adofldGrupo0Width = Nothing
        Set adofldCodWidth = Nothing
        Set adofldDenWidth = Nothing
        Set adofldNombreVista = Nothing
        Set adofldOcultarGrCerrados = Nothing
        Set adofldExcluirGrCerradosResult = Nothing
        Set adofldOcultarNoAdj = Nothing
        Set adofldOcultarProvSinOfe = Nothing
        Set adofldDecResult = Nothing
        Set adofldDecPorcen = Nothing
        Set adofldSolicVinculadaPos = Nothing
        Set adofldSolicVinculadaVisible = Nothing
        Set adofldSolicVinculadaWidth = Nothing
        
        AdoRes.Close
        Set AdoRes = Nothing
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CPlantConfVistasProce", "CargarTodasLasVistas", ERR, Erl)
      Exit Sub
   End If
End Sub


