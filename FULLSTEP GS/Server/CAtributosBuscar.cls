VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CAtributosBuscar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum

Private mCol As Collection
Private m_oConexion As CConexion
Private m_oConexionServer As CConexionDeUseServer

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
    Set NewEnum = mCol.[_NewEnum]
End Property

''' <summary> A�ade un elemento a la colecci�n con sus propiedades</summary>
''' <param name="IDAtrib">ID de atributo</param>
''' <param name="Cod">C�digo del atributo</param>
''' <param name="Den">Denominaci�n del atributo</param>
''' <param name="Tipo">Tipo de atributo</param>
''' <param name="Valor">Valor</param>
''' <param name="TipoIntroduccion">Tipo introducci�n</param>
''' <param name="Minimo">Valor m�nimo</param>
''' <param name="Maximo">Valor m�ximo</param>
''' <param name="Operador">Operador para comparar cuando el atributo es num�rico</param>
''' <param name="Ambito">Ambito en el que buscar</param>
''' <param name="ListaExterna">Si es de Lista Externa</param>
''' <param name="Posicion">Posici�n del atributo</param>
''' <returns>Atributo de b�squeda con las propiedades actualizadas</returns>
''' <remarks>Llamada desde: frmProce.ComprobarValidezItemsConNuevaDistribucion
'''     frmProce.CargarGridItems; Tiempo m�ximo < 0,1</remarks>
Public Function Add(ByVal IdAtrib As Long, ByVal Cod As String, ByVal Den As String, ByVal Tipo As TiposDeAtributos, Optional ByVal Valor As Variant, _
                    Optional ByVal TipoIntroduccion As TAtributoIntroduccion, Optional ByVal Minimo As Variant, Optional ByVal Maximo As Variant, _
                    Optional ByVal Operador As String, Optional Ambito As TAtributoAmbito, Optional ListaExterna As Integer, Optional Posicion As Integer) As CAtributoBuscar

    Dim objnewmember As CAtributoBuscar
    Dim sCod As String
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New CAtributoBuscar
    Set objnewmember.Conexion = m_oConexion
       
    objnewmember.IdAtrib = IdAtrib
    objnewmember.Cod = Cod
    objnewmember.Den = Den
    objnewmember.Tipo = Tipo
    If Not IsMissing(Valor) Then
        objnewmember.Valor = Valor
    Else
        objnewmember.Valor = Null
    End If
    objnewmember.TipoIntroduccion = TipoIntroduccion
    If Not IsMissing(Minimo) Then
        objnewmember.Minimo = Minimo
    Else
        objnewmember.Minimo = Null
    End If
    If Not IsMissing(Maximo) Then
        objnewmember.Maximo = Maximo
    Else
        objnewmember.Maximo = Null
    End If
    objnewmember.Operador = Operador
    objnewmember.AmbitoBuscar = Ambito
    objnewmember.ListaExterna = ListaExterna
    objnewmember.Posicion = Posicion
    
    If Not Existe(CStr(Posicion)) Then
        mCol.Add objnewmember, CStr(Posicion)
    End If
        
    Set Add = objnewmember
    Set objnewmember = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CAtributosBuscar", "Add", ERR, Erl)
      Exit Function
   End If

End Function
 
Public Function Existe(ByVal Id As Variant) As Boolean
    Dim o As CAtributo
    On Error GoTo ERR
    Set o = mCol(CStr(Id))
    
    Existe = True
    Exit Function
ERR:
    Existe = False
End Function

Public Sub Remove(vntIndexKey As Variant)
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
   mCol.Remove CStr(vntIndexKey)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CAtributosBuscar", "Remove", ERR, Erl)
      Exit Sub
   End If

End Sub

Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
     Count = mCol.Count
End If

End Property

Friend Property Set Conexion(ByVal con As CConexion)
Set m_oConexion = con
End Property
Friend Property Get Conexion() As CConexion
Set Conexion = m_oConexion
End Property
Friend Property Set ConexionServer(ByVal con As CConexionDeUseServer)
Set m_oConexionServer = con
End Property
Friend Property Get ConexionServer() As CConexionDeUseServer
Set ConexionServer = m_oConexionServer
End Property

Private Sub Class_Initialize()
Set mCol = New Collection
End Sub

Private Sub Class_Terminate()
Set mCol = Nothing
Set m_oConexion = Nothing
Set m_oConexionServer = Nothing
End Sub
