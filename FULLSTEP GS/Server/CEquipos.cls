VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CEquipos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'********************* CEquipos **********************************
'*             Autor : Javier Arana
'*             Creada : 7/9/98
'****************************************************************

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum
Private mCol As Collection
Private mvarConexion As CConexion
Private mvarConexionServer As CConexionDeUseServer
Private mvarEOF As Boolean


Public Sub CargarTodosLosEquipos(Optional ByVal CarIniCod As String, _
                                Optional ByVal CarIniDen As String, _
                                Optional ByVal CoincidenciaTotal As Boolean, _
                                Optional ByVal OrdenadosPorDen As Boolean = False, _
                                Optional ByVal UsarIndice As Boolean = False, _
                                Optional ByVal Pyme As Variant, _
                                Optional ByVal Equipo As Variant _
                                )


Dim oEqp As CEquipo
Dim rs As New adodb.Recordset
Dim sConsulta As String
Dim iIndice As Integer
Dim fldCod As adodb.Field
Dim fldDen As adodb.Field
'********* Precondicion **************************************
If mvarConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CDivisiones.CargarTodasLasdivisiones", "No se ha establecido la conexion"
    Exit Sub
End If
'*************************************************************
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

sConsulta = "SELECT * FROM EQP WITH (NOLOCK)"
sConsulta = sConsulta & " WHERE 1=1 "

If Not (CarIniCod = "") Then
    If CoincidenciaTotal Then
        sConsulta = sConsulta & " AND COD ='" & DblQuote(CarIniCod) & "'"
    Else
        sConsulta = sConsulta & " AND COD LIKE '" & DblQuote(CarIniCod) & "%'"
    End If
End If

If Not (CarIniDen = "") Then
    If CoincidenciaTotal Then
        sConsulta = sConsulta & " AND DEN='" & DblQuote(CarIniDen) & "'"
    Else
        sConsulta = sConsulta & " AND DEN LIKE '" & DblQuote(CarIniDen) & "%'"
    End If
End If

If Not IsEmpty(Pyme) And Not IsMissing(Pyme) Then
    If Not IsEmpty(Equipo) And Not IsMissing(Equipo) Then
        If Not (Equipo = "") Then
            sConsulta = sConsulta & " AND COD = '" & DblQuote(Equipo) & "'"
        End If
    End If
End If

If OrdenadosPorDen Then
    sConsulta = sConsulta & " ORDER BY DEN,COD"
Else
    sConsulta = sConsulta & " ORDER BY COD,DEN"
End If
      
rs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly

If rs.eof Then
        
    rs.Close
    Set rs = Nothing
    Set mCol = Nothing
    Set mCol = New Collection
    Exit Sub
      
Else
    
    Set fldCod = rs.Fields(0)
    Set fldDen = rs.Fields(1)
    
    Set mCol = Nothing
    Set mCol = New Collection
    
    If UsarIndice Then
        iIndice = 0
        While Not rs.eof
            Set oEqp = Nothing
            Set oEqp = New CEquipo
            oEqp.Cod = rs(0).Value
            oEqp.Den = rs(1).Value
            Set oEqp.Conexion = mvarConexion
            
            mCol.Add oEqp, iIndice
            rs.MoveNext
            iIndice = iIndice + 1
        Wend
    Else
        While Not rs.eof
            Set oEqp = Nothing
            Set oEqp = New CEquipo
            oEqp.Cod = rs(0).Value
            oEqp.Den = rs(1).Value
            Set oEqp.Conexion = mvarConexion
            
            mCol.Add oEqp, fldCod.Value
            rs.MoveNext
        Wend
    End If
    
    Set fldCod = Nothing
    Set fldDen = Nothing

    rs.Close
    Set rs = Nothing
    Exit Sub
      
End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CEquipos", "CargarTodosLosEquipos", ERR, Erl)
        Exit Sub
    End If

End Sub
Public Function Add(ByVal Cod As String, ByVal Den As String, Optional ByVal varIndice As Variant) As CEquipo
    'create a new object
    Dim objnewmember As CEquipo
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New CEquipo
    
    objnewmember.Cod = Cod
    objnewmember.Den = Den
    Set objnewmember.Conexion = mvarConexion
    Set objnewmember.ConexionServer = mvarConexionServer
    
    If Not IsMissing(varIndice) And Not IsNull(varIndice) Then
        objnewmember.Indice = varIndice
        mCol.Add objnewmember, CStr(varIndice)
    Else
    'set the properties passed into the method
       mCol.Add objnewmember, Cod
    End If
    
    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CEquipos", "Add", ERR, Erl)
        Exit Function
    End If

End Function

Public Property Get Item(vntIndexKey As Variant) As CEquipo
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Friend Property Set Conexion(ByVal con As CConexion)
Set mvarConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = mvarConexion
End Property
Friend Property Set ConexionServer(ByVal con As CConexionDeUseServer)
Set mvarConexionServer = con
End Property
Friend Property Get ConexionServer() As CConexionDeUseServer
Set ConexionServer = mvarConexionServer
End Property

Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
     Count = mCol.Count
End If


End Property


Public Sub Remove(vntIndexKey As Variant)
On Error GoTo NoSeEncuentra
    
    mCol.Remove vntIndexKey
    Exit Sub
NoSeEncuentra:

End Sub


Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
     Set NewEnum = mCol.[_NewEnum]
End Property


Private Sub Class_Initialize()
  
    Set mCol = New Collection
End Sub


Private Sub Class_Terminate()
    'Eqproys collection when this class is terminated
    Set mCol = Nothing
    Set mvarConexion = Nothing
    Set mvarConexionServer = Nothing
End Sub

Public Function DevolverEquipo(ByVal Cod As String) As CEquipo
Dim oEqp  As CEquipo
Dim iIndice As Integer
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
On Error Resume Next

For iIndice = 1 To mCol.Count
    
    Set oEqp = mCol.Item(iIndice)
    If oEqp.Cod = Cod Then
        Set DevolverEquipo = oEqp
        Set oEqp = Nothing
        Exit Function
    End If

Next

Set oEqp = Nothing
Set DevolverEquipo = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CEquipos", "DevolverEquipo", ERR, Erl)
        Exit Function
    End If

End Function

Public Sub CargarTodosLosEquiposDesde(ByVal NumMaximo As Integer, _
                                    Optional ByVal CarIniCod As String, _
                                    Optional ByVal CarIniDen As String, _
                                    Optional ByVal OrdenadosPorDen As Boolean = False, _
                                    Optional ByVal UsarIndice As Boolean = False)

Dim lIndice As Long
Dim rs As New adodb.Recordset
Dim sConsulta As String
Dim oCom As CComprador
''''''''''''Dim iNumProve As Integer
Dim fldCod As adodb.Field
Dim fldDen As adodb.Field

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
sConsulta = "SELECT  TOP " & NumMaximo & " * FROM EQP WITH (NOLOCK)"
sConsulta = sConsulta & " WHERE 1=1 "

If Not (CarIniCod = "") Then
    sConsulta = sConsulta & " AND COD >='" & DblQuote(CarIniCod) & "'"
End If

If Not (CarIniDen = "") Then
    sConsulta = sConsulta & " AND DEN >='" & DblQuote(CarIniDen) & "'"
End If

If OrdenadosPorDen Then
    sConsulta = sConsulta & " ORDER BY DEN,COD"
Else
    sConsulta = sConsulta & " ORDER BY COD,DEN"
End If
      
rs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
      
If rs.eof Then
        
    rs.Close
    Set rs = Nothing
    Set mCol = Nothing
    Set mCol = New Collection
    Exit Sub
      
Else

    Set fldCod = rs.Fields(0)
    Set fldDen = rs.Fields(1)
    
    Set mCol = Nothing
    Set mCol = New Collection
    
        
    If UsarIndice Then
        
        lIndice = 0
        '''''''''''''''iNumProve = 0
                
        While Not rs.eof '''''''''''''And iNumProve < NumMaximo
            ' Dejamos en manos del programador cuando asignar la conexion al obejto usuario
            Me.Add fldCod.Value, fldDen.Value, lIndice
            rs.MoveNext
            lIndice = lIndice + 1
            '''''''''''''''''''iNumProve = iNumProve + 1
        Wend
        
        If Not rs.eof Then
            mvarEOF = False
        Else
            mvarEOF = True
        End If
        
    Else
        
        ''''''''''''''''''iNumProve = 0
        
        While Not rs.eof '''''''''''''''''And iNumProve < NumMaximo
            ' Dejamos en manos del programador cuando asignar la conexion al obejto usuario
            Me.Add fldCod.Value, fldDen.Value
            rs.MoveNext
            '''''''''''''''''''''iNumProve = iNumProve + 1
        Wend
        
        If Not rs.eof Then
            mvarEOF = False
        Else
            mvarEOF = True
        End If
        
    End If
    
    Set fldCod = Nothing
    Set fldDen = Nothing
    
    rs.Close
    Set rs = Nothing
    Exit Sub
     
End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CEquipos", "CargarTodosLosEquiposDesde", ERR, Erl)
        Exit Sub
    End If

End Sub


Public Property Get eof() As Boolean
    eof = mvarEOF
End Property
Public Function DevolverLosCodigos() As TipoDatosCombo
   
   Dim Codigos As TipoDatosCombo
   Dim iCont As Integer
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
   ReDim Codigos.Cod(mCol.Count)
   ReDim Codigos.Den(mCol.Count)
   
    For iCont = 0 To mCol.Count - 1
        
        Codigos.Cod(iCont) = mCol(iCont + 1).Cod
        Codigos.Den(iCont) = mCol(iCont + 1).Den
    
    Next

   DevolverLosCodigos = Codigos
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CEquipos", "DevolverLosCodigos", ERR, Erl)
        Exit Function
    End If
   
End Function



