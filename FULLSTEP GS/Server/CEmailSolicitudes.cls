VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CEmailSolicitudes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CEMailSolicitudes **********************************
'*              Autor : Javier Arana
'*              Creada : 3/12/2002

'*              Descripci�n: Clase que se encargar� de gestionar
'*              los textos de los mensajes que hay que enviar al
'*              cambiar los estados de las solicitudes de compra

'*              Nota: Se dise�ar� la clase lo menos acoplada que se pueda
'*              para permitir una posterior migraci�n a un componente separado
'*              de las clases del cliente de GS.
'**************************************************************************

Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

Private mvarRecordset As adodb.Recordset
Private mvarConexion As CConexion
Friend Property Set Conexion(ByVal vData As CConexion)
    Set mvarConexion = vData
End Property


Friend Property Get Conexion() As CConexion
    Set Conexion = mvarConexion
End Property

Public Function Mensaje_Solicitud(ByVal lId As Long, ByVal sPath As String, ByVal iTipoMensaje As Integer) As Variant
'******************************************************************************
'   Descripci�n:
'   Entrada:
'   lId:    el identificador de una solicitud de compra
'   sPath:  ruta de las plantillas de emails
'   iTipoMensaje:
'                                        VALOR             TIPO DE E-MAIL               ***
'                                        -----    -----------------------------------   ***
'                                          0       Aprobaci�n para comprador            ***
'                                          1       Aprobaci�n para peticionario         ***
'                                          2       Rechazo                              ***
'                                          3       Anulaci�n                            ***
'                                          4       Cierre                               ***
'                                          5       Reapertura                           ***
'                                          6       Alta de solicitud al comprador       ***
'
'   Salida:
'   un array de tres posiciones.
'   ayMensaje(0) = 'Destino
'   ayMensaje(1) = 'Asunto
'   ayMensaje(2) = 'Mensaje
'******************************************************************************
Dim arMensaje(5) As String
Dim sConsulta As String
Dim adocom As adodb.Command
Dim oParam As adodb.Parameter
Dim sIdioma As String

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
    If iTipoMensaje < 0 Or iTipoMensaje > 6 Then
        ERR.Raise 10000, "FULLSTEP GS", "Bad message type. Numbers must be beetwen 0 and 6"
    End If
    
    arMensaje(0) = ""
    arMensaje(1) = ""
    arMensaje(2) = ""
    arMensaje(4) = 0
    arMensaje(5) = ""
    
    ''' Preparar la SP y sus parametros
    Set adocom = New adodb.Command
    Set adocom.ActiveConnection = mvarConexion.ADOCon
    
    Set oParam = adocom.CreateParameter("ID", adInteger, adParamInput, , lId)
    adocom.Parameters.Append oParam
    Set oParam = adocom.CreateParameter("TYPE", adTinyInt, adParamInput, , iTipoMensaje)
    adocom.Parameters.Append oParam
    Set oParam = adocom.CreateParameter("EMAIL", adVarChar, adParamOutput, 100)
    adocom.Parameters.Append oParam
    Set oParam = adocom.CreateParameter("IDIOMA", adVarChar, adParamOutput, 50)
    adocom.Parameters.Append oParam
    Set oParam = adocom.CreateParameter("SUBJECT", adVarChar, adParamOutput, 2000)
    adocom.Parameters.Append oParam
    Set oParam = adocom.CreateParameter("TIPOEMAIL", adTinyInt, adParamOutput)
    adocom.Parameters.Append oParam
    Set oParam = adocom.CreateParameter("SUBJECTFROM", adVarChar, adParamOutput, 2000)
    adocom.Parameters.Append oParam
    
    adocom.CommandType = adCmdStoredProc
    adocom.CommandText = "SP_SOLICITUD_DATOS_EMAIL"
    ''' Ejecutar la SP
    adocom.Execute
    arMensaje(0) = NullToStr(adocom.Parameters("EMAIL").Value)
    sIdioma = NullToStr(adocom.Parameters("IDIOMA").Value)
    arMensaje(1) = NullToStr(adocom.Parameters("SUBJECT").Value)
    arMensaje(4) = NullToDbl0(adocom.Parameters("TIPOEMAIL").Value)
    arMensaje(5) = NullToStr(adocom.Parameters("SUBJECTFROM").Value)
    
    Set adocom = Nothing
    
    'Componer el mensaje
    arMensaje(2) = ObtenerCuerpoMail(iTipoMensaje, sIdioma, arMensaje(4), sPath, lId)
       
    Mensaje_Solicitud = arMensaje
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CEmailSolicitudes", "Mensaje_Solicitud", ERR, Erl)
      Exit Function
   End If

    
End Function
Private Sub Class_Terminate()
 Set mvarConexion = Nothing
End Sub
Private Function ObtenerCuerpoMail(ByVal bytTipoMail As Byte, ByVal sIdioma As String, ByVal iTipoCodificacionMail As Integer, ByVal sPath As String, ByVal lId As Long) As String
'******************************************************************************************
'*** Descripci�n: Esta funci�n devuelve el cuerpo del mail ha ser enviado. Para ello    ***
'***              dependi�ndo de los argumentos delega en las funciones que realmente   ***
'***              generan el cuerpo del mensaje para obtenerlo.                         ***
'***                                                                                    ***
'*** Par�metros : bytTipoMail ::> Representa el tipo de e-mail cuyo cuerpo queremos     ***
'***                              obtener. Los valores que toma este argumento son:     ***
'***                                                                                    ***
'***                                     VALOR             TIPO DE E-MAIL               ***
'***                                     -----    -----------------------------------   ***
'***                                       0       Aprobaci�n para comprador            ***
'***                                       1       Aprobaci�n para peticionario         ***
'***                                       2       Rechazo                              ***
'***                                       3       Anulaci�n                            ***
'***                                       4       Cierre                               ***
'***                                       5       Reapertura                           ***
'***                                       6       Alta de solicitud al comprador       ***
'***                                                                                    ***
'***              sIdioma ::> De tipo string, representa el c�digo del idioma en que se ***
'***                          enviar� el mail. Este argumento nos sirve para abrir la   ***
'***                          plantilla correspondiente al idioma adecuado.             ***
'***                                                                                    ***
'***              iTipoCodificacionMail ::> Representa el tipo de codificaci�n del      ***
'***                                        mail: 0 - Texto plano ; 1 - HTML            ***
'***              sPath ::>   Ruta donde se encuentran los ficheros de plantillas
'***              lId ::>     Identificador de la solicitud
'***              adocom ::>   Comando que contendr� los datos para rellenar la plantilla
'***                                                                                    ***
'*** Valor que devuelve: Un string que ser� el cuerpo del mensaje, adecuado al valor de ***
'***                     los par�metros de entrada introducidos.                        ***
'******************************************************************************************
    Dim sCuerpo As String
    Dim adocom As adodb.Command
    Dim oParam As adodb.Parameter

    'Primero recuperamos los datos necesarios para el mensaje
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set adocom = New adodb.Command
    Set adocom.ActiveConnection = mvarConexion.ADOCon
    'Par�metros de entrada
    Set oParam = adocom.CreateParameter("ID", adInteger, adParamInput, , lId)
    adocom.Parameters.Append oParam
    Set oParam = adocom.CreateParameter("TYPE", adTinyInt, adParamInput, , bytTipoMail)
    adocom.Parameters.Append oParam
    Set oParam = adocom.CreateParameter("IDI", adVarChar, adParamInput, 50, sIdioma)
    adocom.Parameters.Append oParam
    Set oParam = adocom.CreateParameter("FORMATOFECHA", adVarChar, adParamInput, 20, "dd/mm/yyyy")
    adocom.Parameters.Append oParam
    
    'Par�metros de salida
    Set oParam = adocom.CreateParameter("DESCR_BREVE", adVarChar, adParamOutput, 50)
    adocom.Parameters.Append oParam
    Set oParam = adocom.CreateParameter("FECHA_ALTA", adDate, adParamOutput)
    adocom.Parameters.Append oParam
    Set oParam = adocom.CreateParameter("FECHA_NECESIDAD", adDate, adParamOutput)
    adocom.Parameters.Append oParam
    Set oParam = adocom.CreateParameter("IMPORTE", adDouble, adParamOutput)
    adocom.Parameters.Append oParam
    Set oParam = adocom.CreateParameter("MON_COD", adVarChar, adParamOutput, 50)
    adocom.Parameters.Append oParam
    Set oParam = adocom.CreateParameter("MON_DEN", adVarChar, adParamOutput, 100)
    adocom.Parameters.Append oParam
    Set oParam = adocom.CreateParameter("APROBADOR", adVarChar, adParamOutput, 150)
    adocom.Parameters.Append oParam
    Set oParam = adocom.CreateParameter("APROBADOR_COD", adVarChar, adParamOutput, 50)
    adocom.Parameters.Append oParam
    Set oParam = adocom.CreateParameter("APROBADOR_NOM", adVarChar, adParamOutput, 50)
    adocom.Parameters.Append oParam
    Set oParam = adocom.CreateParameter("APROBADOR_APE", adVarChar, adParamOutput, 100)
    adocom.Parameters.Append oParam
    Set oParam = adocom.CreateParameter("APROBADOR_TFNO", adVarChar, adParamOutput, 20)
    adocom.Parameters.Append oParam
    Set oParam = adocom.CreateParameter("APROBADOR_EMAIL", adVarChar, adParamOutput, 100)
    adocom.Parameters.Append oParam
    Set oParam = adocom.CreateParameter("APROBADOR_FAX", adVarChar, adParamOutput, 20)
    adocom.Parameters.Append oParam
    Set oParam = adocom.CreateParameter("APROBADOR_UON", adVarChar, adParamOutput, 250)
    adocom.Parameters.Append oParam
    Set oParam = adocom.CreateParameter("APROBADOR_DEP_COD", adVarChar, adParamOutput, 50)
    adocom.Parameters.Append oParam
    Set oParam = adocom.CreateParameter("APROBADOR_DEP_DEN", adVarChar, adParamOutput, 100)
    adocom.Parameters.Append oParam
    Set oParam = adocom.CreateParameter("COMPRADOR_COD", adVarChar, adParamOutput, 50)
    adocom.Parameters.Append oParam
    Set oParam = adocom.CreateParameter("COMPRADOR_NOM", adVarChar, adParamOutput, 50)
    adocom.Parameters.Append oParam
    Set oParam = adocom.CreateParameter("COMPRADOR_APE", adVarChar, adParamOutput, 100)
    adocom.Parameters.Append oParam
    Set oParam = adocom.CreateParameter("COMPRADOR_TFNO", adVarChar, adParamOutput, 20)
    adocom.Parameters.Append oParam
    Set oParam = adocom.CreateParameter("COMPRADOR_EMAIL", adVarChar, adParamOutput, 100)
    adocom.Parameters.Append oParam
    Set oParam = adocom.CreateParameter("COMPRADOR_FAX", adVarChar, adParamOutput, 20)
    adocom.Parameters.Append oParam
    Set oParam = adocom.CreateParameter("COMPRADOR_UON", adVarChar, adParamOutput, 250)
    adocom.Parameters.Append oParam
    Set oParam = adocom.CreateParameter("COMPRADOR_DEP_COD", adVarChar, adParamOutput, 50)
    adocom.Parameters.Append oParam
    Set oParam = adocom.CreateParameter("COMPRADOR_DEP_DEN", adVarChar, adParamOutput, 100)
    adocom.Parameters.Append oParam
    Set oParam = adocom.CreateParameter("PET_COD", adVarChar, adParamOutput, 50)
    adocom.Parameters.Append oParam
    Set oParam = adocom.CreateParameter("PET_NOM", adVarChar, adParamOutput, 50)
    adocom.Parameters.Append oParam
    Set oParam = adocom.CreateParameter("PET_APE", adVarChar, adParamOutput, 100)
    adocom.Parameters.Append oParam
    Set oParam = adocom.CreateParameter("PET_TFNO", adVarChar, adParamOutput, 20)
    adocom.Parameters.Append oParam
    Set oParam = adocom.CreateParameter("PET_EMAIL", adVarChar, adParamOutput, 100)
    adocom.Parameters.Append oParam
    Set oParam = adocom.CreateParameter("PET_FAX", adVarChar, adParamOutput, 20)
    adocom.Parameters.Append oParam
    Set oParam = adocom.CreateParameter("PET_UON", adVarChar, adParamOutput, 250)
    adocom.Parameters.Append oParam
    Set oParam = adocom.CreateParameter("PET_DEP_COD", adVarChar, adParamOutput, 50)
    adocom.Parameters.Append oParam
    Set oParam = adocom.CreateParameter("PET_DEP_DEN", adVarChar, adParamOutput, 100)
    adocom.Parameters.Append oParam
    Set oParam = adocom.CreateParameter("COMENTARIO", adVarChar, adParamOutput, 500)
    adocom.Parameters.Append oParam
    Set oParam = adocom.CreateParameter("TIPOCOD", adVarChar, adParamOutput, 50)
    adocom.Parameters.Append oParam
    Set oParam = adocom.CreateParameter("TIPODEN", adVarChar, adParamOutput, 100)
    adocom.Parameters.Append oParam
    
    adocom.CommandType = adCmdStoredProc
    adocom.CommandText = "SP_SOLICITUD_DATOS_MENSAJE"
    
    ''' Ejecutar la SP
    adocom.Execute
    
    Select Case bytTipoMail
        Case 0  'Aprobaci�n para comprador
            If iTipoCodificacionMail = 0 Then
                sCuerpo = GenerarMailTEXTO(sPath & "\" & sIdioma & "_FSGS_AsignacionSolicitudCom.txt", adocom)
            Else
                sCuerpo = GenerarMailHTML(sPath & "\" & sIdioma & "_FSGS_AsignacionSolicitudCom.html", adocom)
            End If
        Case 1  'Aprobaci�n para peticionario
            If iTipoCodificacionMail = 0 Then
                sCuerpo = GenerarMailTEXTO(sPath & "\" & sIdioma & "_FSGS_AprobacionSolicitudPet.txt", adocom)
            Else
                sCuerpo = GenerarMailHTML(sPath & "\" & sIdioma & "_FSGS_AprobacionSolicitudPet.html", adocom)
            End If
        Case 2  'Rechazo
            If iTipoCodificacionMail = 0 Then
                sCuerpo = GenerarMailTEXTO(sPath & "\" & sIdioma & "_FSGS_RechazoSolicitudPet.txt", adocom)
            Else
                sCuerpo = GenerarMailHTML(sPath & "\" & sIdioma & "_FSGS_RechazoSolicitudPet.html", adocom)
            End If
        Case 3  'Anulaci�n
            If iTipoCodificacionMail = 0 Then
                sCuerpo = GenerarMailTEXTO(sPath & "\" & sIdioma & "_FSGS_AnulacionSolicitudPet.txt", adocom)
            Else
                sCuerpo = GenerarMailHTML(sPath & "\" & sIdioma & "_FSGS_AnulacionSolicitudPet.html", adocom)
            End If
        Case 4  'Cierre
            If iTipoCodificacionMail = 0 Then
                sCuerpo = GenerarMailTEXTO(sPath & "\" & sIdioma & "_FSGS_CierreSolicitudPet.txt", adocom)
            Else
                sCuerpo = GenerarMailHTML(sPath & "\" & sIdioma & "_FSGS_CierreSolicitudPet.html", adocom)
            End If
        Case 5  'Reapertura
            If iTipoCodificacionMail = 0 Then
                sCuerpo = GenerarMailTEXTO(sPath & "\" & sIdioma & "_FSGS_ReaperturaSolicitudPet.txt", adocom)
            Else
                sCuerpo = GenerarMailHTML(sPath & "\" & sIdioma & "_FSGS_ReaperturaSolicitudPet.html", adocom)
            End If
        Case 6  'Alta de solicitud al comprador
            If iTipoCodificacionMail = 0 Then
                sCuerpo = GenerarMailTEXTO(sPath & "\" & sIdioma & "_FSGS_AsignacionSolicitudCom.txt", adocom)
            Else
                sCuerpo = GenerarMailHTML(sPath & "\" & sIdioma & "_FSGS_AsignacionSolicitudCom.html", adocom)
            End If
            
    End Select
    ObtenerCuerpoMail = sCuerpo
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CEmailSolicitudes", "ObtenerCuerpoMail", ERR, Erl)
      Exit Function
   End If

End Function
Private Function GenerarMailTEXTO(ByVal sNombreArchivo As String, ByVal adocom As adodb.Command) As String
'******************************************************************************************
'*** Descripci�n: Genera el cuerpo del mensaje codificado en texto plano.               ***
'***                                                                                    ***
'*** Par�metros : sNombreArchivo ::> Es el nombre del archivo que es la plantilla del   ***
'***                                 mail que se quiere generar.                        ***
'***                                                                                    ***
'*** Valor que devuelve: Un string que ser� el cuerpo del mensaje.                      ***
'******************************************************************************************
    Dim oFos As FileSystemObject
    Dim oFile As File
    Dim oStream As Scripting.TextStream
    Dim sTXT As String


If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR

'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error GoTo ERROR
    Set oFos = New FileSystemObject
    
    If Not oFos.FileExists(sNombreArchivo) Then
        Set oFos = Nothing
        GenerarMailTEXTO = "1010"
        Exit Function
    End If
    
    Set oFile = oFos.GetFile(sNombreArchivo)
    Set oStream = oFile.OpenAsTextStream(ForReading, TristateUseDefault)
    sTXT = oStream.ReadAll
    
    
    ReemplazarTXT sTXT, "@ID", adocom.Parameters("ID").Value
    ReemplazarTXT sTXT, "@DESCR_BREVE", adocom.Parameters("DESCR_BREVE").Value
    ReemplazarTXT sTXT, "@FECHA_ALTA", adocom.Parameters("FECHA_ALTA").Value
    ReemplazarTXT sTXT, "@FECHA_NECESIDAD", adocom.Parameters("FECHA_NECESIDAD").Value
    ReemplazarTXT sTXT, "@IMPORTE", adocom.Parameters("IMPORTE").Value
    ReemplazarTXT sTXT, "@MON_COD", adocom.Parameters("MON_COD").Value
    ReemplazarTXT sTXT, "@MON_DEN", adocom.Parameters("MON_DEN").Value
    
    ReemplazarTXT sTXT, "@APROBADOR_TITULO", adocom.Parameters("APROBADOR").Value
    ReemplazarTXT sTXT, "@APROBADOR_COD", adocom.Parameters("APROBADOR_COD").Value
    ReemplazarTXT sTXT, "@APROBADOR_NOM", adocom.Parameters("APROBADOR_NOM").Value
    ReemplazarTXT sTXT, "@APROBADOR_APE", adocom.Parameters("APROBADOR_APE").Value
    ReemplazarTXT sTXT, "@APROBADOR_TFNO", adocom.Parameters("APROBADOR_TFNO").Value
    ReemplazarTXT sTXT, "@APROBADOR_EMAIL", adocom.Parameters("APROBADOR_EMAIL").Value
    ReemplazarTXT sTXT, "@APROBADOR_FAX", adocom.Parameters("APROBADOR_FAX").Value
    ReemplazarTXT sTXT, "@APROBADOR_UON", adocom.Parameters("APROBADOR_UON").Value
    ReemplazarTXT sTXT, "@APROBADOR_DEP_COD", adocom.Parameters("APROBADOR_DEP_COD").Value
    ReemplazarTXT sTXT, "@APROBADOR_DEP_DEN", adocom.Parameters("APROBADOR_DEP_DEN").Value
    
    ReemplazarTXT sTXT, "@COMPRADOR_COD", adocom.Parameters("COMPRADOR_COD").Value
    ReemplazarTXT sTXT, "@COMPRADOR_NOM", adocom.Parameters("COMPRADOR_NOM").Value
    ReemplazarTXT sTXT, "@COMPRADOR_APE", adocom.Parameters("COMPRADOR_APE").Value
    ReemplazarTXT sTXT, "@COMPRADOR_TFNO", adocom.Parameters("COMPRADOR_TFNO").Value
    ReemplazarTXT sTXT, "@COMPRADOR_EMAIL", adocom.Parameters("COMPRADOR_EMAIL").Value
    ReemplazarTXT sTXT, "@COMPRADOR_FAX", adocom.Parameters("COMPRADOR_FAX").Value
    ReemplazarTXT sTXT, "@COMPRADOR_UON", adocom.Parameters("COMPRADOR_UON").Value
    ReemplazarTXT sTXT, "@COMPRADOR_DEP_COD", adocom.Parameters("COMPRADOR_DEP_COD").Value
    ReemplazarTXT sTXT, "@COMPRADOR_DEP_DEN", adocom.Parameters("COMPRADOR_DEP_DEN").Value
    
    ReemplazarTXT sTXT, "@PET_COD", adocom.Parameters("PET_COD").Value
    ReemplazarTXT sTXT, "@PET_NOM", adocom.Parameters("PET_NOM").Value
    ReemplazarTXT sTXT, "@PET_APE", adocom.Parameters("PET_APE").Value
    ReemplazarTXT sTXT, "@PET_TFNO", adocom.Parameters("PET_TFNO").Value
    ReemplazarTXT sTXT, "@PET_EMAIL", adocom.Parameters("PET_EMAIL").Value
    ReemplazarTXT sTXT, "@PET_FAX", adocom.Parameters("PET_FAX").Value
    ReemplazarTXT sTXT, "@PET_UON", adocom.Parameters("PET_UON").Value
    ReemplazarTXT sTXT, "@PET_DEP_COD", adocom.Parameters("PET_DEP_COD").Value
    ReemplazarTXT sTXT, "@PET_DEP_DEN", adocom.Parameters("PET_DEP_DEN").Value
    ReemplazarTXT sTXT, "@COMENTARIO", adocom.Parameters("COMENTARIO").Value
    ReemplazarTXT sTXT, "@TIPOCOD", adocom.Parameters("TIPOCOD").Value
    ReemplazarTXT sTXT, "@TIPODEN", adocom.Parameters("TIPODEN").Value

    Set adocom = Nothing
    oStream.Close
    Set oFile = Nothing
    Set oFos = Nothing
    GenerarMailTEXTO = sTXT
    Exit Function
    
Fin:
    Set oStream = Nothing
    If Not oFile Is Nothing Then
        Set oFile = Nothing
    End If
    If Not oFos Is Nothing Then
        Set oFos = Nothing
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CEmailSolicitudes", "GenerarMailTEXTO", ERR, Erl)
      Resume Fin
   End If

End Function
Private Function GenerarMailHTML(ByVal sNombreArchivo As String, ByVal adocom As adodb.Command) As String
'******************************************************************************************
'*** Descripci�n: Genera el cuerpo del mensaje si�ndo �ste de tipo recordatorio y       ***
'***              codificado en HTML.                                                   ***
'***                                                                                    ***
'*** Par�metros : sNombreArchivo ::> Es el nombre del archivo que es la plantilla del   ***
'***                                 mail que se quiere generar.                        ***
'***                                                                                    ***
'*** Valor que devuelve: Un string que ser� el cuerpo del mensaje.                      ***
'******************************************************************************************
    Dim oFos As FileSystemObject
    Dim oFile As File
    Dim oStream As Scripting.TextStream
    Dim sHTML As String


If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error GoTo ERROR
    Set oFos = New FileSystemObject
    Set oFile = oFos.GetFile(sNombreArchivo)
    Set oStream = oFile.OpenAsTextStream(ForReading, TristateUseDefault)
    sHTML = oStream.ReadAll
    
    
    ReemplazarHTML sHTML, "@ID", adocom.Parameters("ID").Value
    ReemplazarHTML sHTML, "@DESCR_BREVE", adocom.Parameters("DESCR_BREVE").Value
    ReemplazarHTML sHTML, "@FECHA_ALTA", adocom.Parameters("FECHA_ALTA").Value
    ReemplazarHTML sHTML, "@FECHA_NECESIDAD", adocom.Parameters("FECHA_NECESIDAD").Value
    ReemplazarHTML sHTML, "@IMPORTE", adocom.Parameters("IMPORTE").Value
    ReemplazarHTML sHTML, "@MON_COD", adocom.Parameters("MON_COD").Value
    ReemplazarHTML sHTML, "@MON_DEN", adocom.Parameters("MON_DEN").Value

    ReemplazarHTML sHTML, "@APROBADOR_TITULO", adocom.Parameters("APROBADOR").Value
    ReemplazarHTML sHTML, "@APROBADOR_COD", adocom.Parameters("APROBADOR_COD").Value
    ReemplazarHTML sHTML, "@APROBADOR_NOM", adocom.Parameters("APROBADOR_NOM").Value
    ReemplazarHTML sHTML, "@APROBADOR_APE", adocom.Parameters("APROBADOR_APE").Value
    ReemplazarHTML sHTML, "@APROBADOR_TFNO", adocom.Parameters("APROBADOR_TFNO").Value
    ReemplazarHTML sHTML, "@APROBADOR_EMAIL", adocom.Parameters("APROBADOR_EMAIL").Value
    ReemplazarHTML sHTML, "@APROBADOR_FAX", adocom.Parameters("APROBADOR_FAX").Value
    ReemplazarHTML sHTML, "@APROBADOR_UON", adocom.Parameters("APROBADOR_UON").Value
    ReemplazarHTML sHTML, "@APROBADOR_DEP_COD", adocom.Parameters("APROBADOR_DEP_COD").Value
    ReemplazarHTML sHTML, "@APROBADOR_DEP_DEN", adocom.Parameters("APROBADOR_DEP_DEN").Value

    ReemplazarHTML sHTML, "@COMPRADOR_COD", adocom.Parameters("COMPRADOR_COD").Value
    ReemplazarHTML sHTML, "@COMPRADOR_NOM", adocom.Parameters("COMPRADOR_NOM").Value
    ReemplazarHTML sHTML, "@COMPRADOR_APE", adocom.Parameters("COMPRADOR_APE").Value
    ReemplazarHTML sHTML, "@COMPRADOR_TFNO", adocom.Parameters("COMPRADOR_TFNO").Value
    ReemplazarHTML sHTML, "@COMPRADOR_EMAIL", adocom.Parameters("COMPRADOR_EMAIL").Value
    ReemplazarHTML sHTML, "@COMPRADOR_FAX", adocom.Parameters("COMPRADOR_FAX").Value
    ReemplazarHTML sHTML, "@COMPRADOR_UON", adocom.Parameters("COMPRADOR_UON").Value
    ReemplazarHTML sHTML, "@COMPRADOR_DEP_COD", adocom.Parameters("COMPRADOR_DEP_COD").Value
    ReemplazarHTML sHTML, "@COMPRADOR_DEP_DEN", adocom.Parameters("COMPRADOR_DEP_DEN").Value

    ReemplazarHTML sHTML, "@PET_COD", adocom.Parameters("PET_COD").Value
    ReemplazarHTML sHTML, "@PET_NOM", adocom.Parameters("PET_NOM").Value
    ReemplazarHTML sHTML, "@PET_APE", adocom.Parameters("PET_APE").Value
    ReemplazarHTML sHTML, "@PET_TFNO", adocom.Parameters("PET_TFNO").Value
    ReemplazarHTML sHTML, "@PET_EMAIL", adocom.Parameters("PET_EMAIL").Value
    ReemplazarHTML sHTML, "@PET_FAX", adocom.Parameters("PET_FAX").Value
    ReemplazarHTML sHTML, "@PET_UON", adocom.Parameters("PET_UON").Value
    ReemplazarHTML sHTML, "@PET_DEP_COD", adocom.Parameters("PET_DEP_COD").Value
    ReemplazarHTML sHTML, "@PET_DEP_DEN", adocom.Parameters("PET_DEP_DEN").Value
    ReemplazarHTML sHTML, "@COMENTARIO", adocom.Parameters("COMENTARIO").Value
    ReemplazarHTML sHTML, "@TIPOCOD", adocom.Parameters("TIPOCOD").Value
    ReemplazarHTML sHTML, "@TIPODEN", adocom.Parameters("TIPODEN").Value
    
    
    oStream.Close
    Set oFile = Nothing
    Set oFos = Nothing
    GenerarMailHTML = sHTML
    Exit Function

Fin:
    Set oStream = Nothing
    If Not oFile Is Nothing Then
        Set oFile = Nothing
    End If
    If Not oFos Is Nothing Then
        Set oFos = Nothing
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CEmailSolicitudes", "GenerarMailHTML", ERR, Erl)
      Resume Fin
   End If

End Function
Private Sub ReemplazarTXT(ByRef sTextoFuente As String, ByVal sEtiqueta As String, ByVal vTextoAPoner As Variant)
'**************************************************************************************************
'*** Descripci�n: Reemplaza en el texto dado por el String sTextoFuente un valor de tipo String ***
'***              contenido en sTextoAPoner por la etiqueta contenida en el par�metro sEtiqueta.***
'***                                                                                            ***
'*** Par�metros : sTextoFuente ::>> Texto de origen, que contiene las etiquetas de la plantilla ***
'***              sEtiqueta    ::>> Contiene el nombre de la etiqueta de la plantilla           ***
'***              sTextoAPoner ::>> Contiene el texto que reemplazar� a la etiqueta             ***
'***                                                                                            ***
'*** Valor que devuelve: En la variable de tipo String sTextoFuente pasada por referencia       ***
'***                     quedar� el texto fuente en el que se habr� producido el cambio tras la ***
'***                     ejecuci�n del procedimiento.                                           ***
'**************************************************************************************************
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If IsEmpty(vTextoAPoner) Then
        sTextoFuente = Replace(sTextoFuente, sEtiqueta, "")
    Else
        If IsNull(vTextoAPoner) Then
            sTextoFuente = Replace(sTextoFuente, sEtiqueta, "")
        Else
            If vTextoAPoner <> "" Then
                sTextoFuente = Replace(sTextoFuente, sEtiqueta, vTextoAPoner)
            Else
                sTextoFuente = Replace(sTextoFuente, sEtiqueta, "")
            End If
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CEmailSolicitudes", "ReemplazarTXT", ERR, Erl)
      Exit Sub
   End If

    
End Sub

Private Sub ReemplazarHTML(ByRef sTextoFuente As String, ByVal sEtiqueta As String, ByVal vTextoAPoner As Variant)
'**************************************************************************************************
'*** Descripci�n: Reemplaza en el texto dado por el String sTextoFuente un valor de tipo String ***
'***              contenido en sTextoAPoner por la etiqueta contenida en el par�metro sEtiqueta.***
'***                                                                                            ***
'*** Par�metros : sTextoFuente ::>> Texto de origen, que contiene las etiquetas de la plantilla ***
'***              sEtiqueta    ::>> Contiene el nombre de la etiqueta de la plantilla           ***
'***              sTextoAPoner ::>> Contiene el texto que reemplazar� a la etiqueta             ***
'***                                                                                            ***
'*** Valor que devuelve: En la variable de tipo String sTextoFuente pasada por referencia       ***
'***                     quedar� el texto fuente en el que se habr� producido el cambio tras la ***
'***                     ejecuci�n del procedimiento.                                           ***
'**************************************************************************************************
    If IsEmpty(vTextoAPoner) Then
        sTextoFuente = Replace(sTextoFuente, sEtiqueta, "")
    Else
        If IsNull(vTextoAPoner) Then
            sTextoFuente = Replace(sTextoFuente, sEtiqueta, "")
        Else
            If vTextoAPoner <> "" Then
                sTextoFuente = Replace(sTextoFuente, sEtiqueta, vTextoAPoner)
            Else
                sTextoFuente = Replace(sTextoFuente, sEtiqueta, "")
            End If
        End If
    End If
    
End Sub
