VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CCamposPredef"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

Private mCol As Collection

Private m_oConexion As CConexion
Private m_bEOF As Boolean

Public Property Get eof() As Boolean
    eof = m_bEOF
End Property
Friend Property Let eof(ByVal b As Boolean)
    m_bEOF = b
End Property

''' <summary>
''' A�ade un campo predefinido al objeto CCampoPredef
''' </summary>
''' <param name="ID">Id del campo</param>
''' <param name="TipoSolicitud">Tipo de la solicitud</param>
''' <param name="DEN">Denominaci�n del campo</param>
''' <param name="Ayudas">Ayuda del campo</param>
''' <param name="TipoPredef">Tipo de campo predefinido</param>
''' <param name="Tipo">Tipo de atributos</param>
''' <param name="Tipo">Tipo de atributo de introducci�n</param>
''' <param name="MinNum">Valor m�nimo</param>
''' <param name="MaxNum">Valor m�ximo</param>
''' <param name="MinFec">Fecha m�nima</param>
''' <param name="MaxFec">Fecha m�xima</param>
''' <param name="TipoCampoGS">Tipo de campo GS</param>
''' <param name="IdAtrib">Id atributo</param>
''' <param name="vOrden">Posici�n del campo</param>
''' <param name="bEsSubcampo">Si est� dentro de un desglose o no</param>
''' <param name="FechaActual">Fecha actual</param>
''' <param name="oCampoPadre">Objeto CCampoPredef</param>
''' <param name="vIndice">Indice</param>
''' <param name="sPres5">C�digo de la raiz presupuestaria</param>
''' <returns>Un objeto CCampoPredef</returns>
''' <remarks>Llamada desde:CargarCampoPredef; Tiempo m�ximo:0,1</remarks>
Public Function Add(ByVal Id As Long, ByVal TipoSolicitud As Long, ByVal Den As CMultiidiomas, Optional ByVal Ayudas As CMultiidiomas, _
            Optional ByVal TipoPredef As TipoCampoPredefinido, Optional ByVal Tipo As TiposDeAtributos, Optional ByVal TipoIntro As TAtributoIntroduccion, _
            Optional ByVal MinNum As Variant, Optional ByVal MaxNum As Variant, Optional ByVal MinFec As Variant, Optional ByVal MaxFec As Variant, _
            Optional ByVal TipoCampoGS As Variant, Optional ByVal IdAtrib As Variant, Optional ByVal vOrden As Variant, Optional ByVal bEsSubcampo As Boolean, _
            Optional ByVal FechaActual As Variant, Optional ByVal oCampoPadre As CCampoPredef, Optional ByVal vIndice As Variant, Optional ByVal sPRES5 As String) As CCampoPredef

    Dim objnewmember As CCampoPredef
    
    Set objnewmember = New CCampoPredef
    Set objnewmember.Conexion = m_oConexion
    
    objnewmember.Id = Id
    objnewmember.TipoSolicitud = TipoSolicitud
    Set objnewmember.Denominaciones = Den
    Set objnewmember.Ayudas = Ayudas
    
    objnewmember.TipoPredef = TipoPredef
    objnewmember.Tipo = Tipo
    objnewmember.TipoIntroduccion = TipoIntro
        
    If Not IsMissing(TipoCampoGS) Then
        objnewmember.TipoCampoGS = TipoCampoGS
    Else
        objnewmember.TipoCampoGS = Null
    End If
    
    If Not IsMissing(IdAtrib) Then
        objnewmember.IdAtrib = IdAtrib
    Else
        objnewmember.IdAtrib = Null
    End If
    
    If Not IsMissing(MinNum) Then
        objnewmember.MinNum = MinNum
    Else
        objnewmember.MinNum = Null
    End If
    
    If Not IsMissing(MaxNum) Then
        objnewmember.MaxNum = MaxNum
    Else
        objnewmember.MaxNum = Null
    End If

    If Not IsMissing(MinFec) Then
        objnewmember.MinFec = MinFec
    Else
        objnewmember.MinFec = Null
    End If
    
    If Not IsMissing(MaxFec) Then
        objnewmember.MaxFec = MaxFec
    Else
        objnewmember.MaxFec = Null
    End If
    
    objnewmember.EsSubCampo = bEsSubcampo
    
    If IsMissing(vOrden) Then
        objnewmember.Orden = Null
    Else
        objnewmember.Orden = vOrden
    End If
    
    If IsMissing(FechaActual) Then
        objnewmember.FecAct = Null
    Else
        objnewmember.FecAct = FechaActual
    End If
    
    If Not IsMissing(sPRES5) Then
        objnewmember.PRES5 = sPRES5
    Else
        objnewmember.PRES5 = Null
    End If
    
    Set objnewmember.CampoPadre = oCampoPadre
    
    If Not IsMissing(vIndice) And Not IsNull(vIndice) Then
        objnewmember.Indice = vIndice
        mCol.Add objnewmember, CStr(vIndice)
    Else
        mCol.Add objnewmember, CStr(Id)
    End If
    
    Set Add = objnewmember
    Set objnewmember = Nothing

End Function

Friend Property Set Conexion(ByVal vData As CConexion)
    Set m_oConexion = vData
End Property

Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property

Public Property Get Item(vntIndexKey As Variant) As CCampoPredef

    ''' * Objetivo: Recuperar una moneda de la coleccion
    ''' * Recibe: Indice de la moneda a recuperar
    ''' * Devuelve: Moneda correspondiente
        
On Error GoTo NoSeEncuentra:

    ''' Devolvemos el item de la coleccion privada
    
    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:

    Set Item = Nothing
    
End Property

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4

    ''' ! Pendiente de revision, objetivo desconocido
    
    Set NewEnum = mCol.[_NewEnum]

End Property
Public Sub Remove(vntIndexKey As Variant)

    ''' * Objetivo: Eliminar una moneda de la coleccion
    ''' * Recibe: Indice de la moneda a eliminar
    ''' * Devuelve: Nada
   
    mCol.Remove vntIndexKey
    
End Sub
Private Sub Class_Initialize()

    ''' * Objetivo: Crear la coleccion
    Set mCol = New Collection
    
End Sub
Private Sub Class_Terminate()

    ''' * Objetivo: Limpiar memoria
    
    Set mCol = Nothing
    Set m_oConexion = Nothing
End Sub

Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
     Count = mCol.Count
End If

End Property

Public Sub CargarTodosLosCamposDesglose(ByVal lCampo As Long, Optional ByVal UsarIndice As Boolean = False)
    Dim rs As New adodb.Recordset
    Dim sConsulta As String
    Dim lIndice As Long
    Dim oAyudas As CMultiidiomas
    Dim oDenominaciones As CMultiidiomas
    Dim oGestorParametros As CGestorParametros
    Dim oIdiomas As CIdiomas
    Dim oIdi As CIdioma
    Dim oCampoPadre As CCampoPredef
    
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CCamposPredef.CargarTodosLosCamposDesglose", "No se ha establecido la conexion"
        Exit Sub
    End If

    
   'Tarea 3290'
    'Carga todos los idiomas de la aplicaci�n:
    Set oGestorParametros = New CGestorParametros
    Set oGestorParametros.Conexion = m_oConexion
    Set oIdiomas = oGestorParametros.DevolverIdiomas(False, False, True)
    
    
    Dim cden_general As String
    cden_general = ""
    Dim cayuda_general As String
    cayuda_general = ""
    
    For Each oIdi In oIdiomas
        If (cden_general <> "") Then
           cden_general = cden_general & ","
        End If
        cden_general = cden_general & "C.DEN_" & oIdi.Cod
        
        If (cayuda_general <> "") Then
           cayuda_general = cayuda_general & ","
        End If
        cayuda_general = cayuda_general & "C.AYUDA_" & oIdi.Cod
    Next
    ' cden_general = C.DEN_SPA,C.DEN_ENG,C.DEN_GER,C.DEN_FRA
    ' cayuda_general = C.AYUDA_SPA,C.AYUDA_ENG,C.AYUDA_GER,C.AYUDA_FRA
    'Tarea 3290'


    sConsulta = "SELECT C.ID,C.TIPO_SOLICITUD,C.INTRO,C.TIPO,C.SUBTIPO,C.MINNUM,C.MAXNUM,C.MINFEC,C.MAXFEC,C.ID_ATRIB_GS,C.TIPO_CAMPO_GS,C.ORDEN,C.ES_SUBCAMPO,C.FECACT,C.PRES5," & cden_general & "," & cayuda_general & " FROM CAMPOS_PREDEF C WITH (NOLOCK) INNER JOIN DESGLOSE_CAMPOS_PREDEF D WITH (NOLOCK) ON C.ID=D.CAMPO_HIJO AND D.CAMPO_PADRE=" & lCampo
    sConsulta = sConsulta & " ORDER BY D.ORDEN"
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly

    If rs.eof Then
        rs.Close
        Set rs = Nothing
        
        Set mCol = Nothing
        Set mCol = New Collection
        Set oGestorParametros = Nothing
        Set oIdiomas = Nothing
        Exit Sub

    Else
        Set mCol = Nothing
        Set mCol = New Collection
               
        lIndice = 0
        
        Set oCampoPadre = New CCampoPredef
        Set oCampoPadre.Conexion = m_oConexion
        oCampoPadre.Id = lCampo
        
        While Not rs.eof
            Set oDenominaciones = Nothing
            Set oDenominaciones = New CMultiidiomas
            Set oDenominaciones.Conexion = m_oConexion
            
            Set oAyudas = Nothing
            Set oAyudas = New CMultiidiomas
            Set oAyudas.Conexion = m_oConexion
            
            For Each oIdi In oIdiomas
                oDenominaciones.Add oIdi.Cod, rs.Fields("DEN_" & oIdi.Cod).Value
                'If Not IsNull(rs.Fields("AYUDA_" & oIdi.Cod).Value) Then
                    oAyudas.Add oIdi.Cod, rs.Fields("AYUDA_" & oIdi.Cod).Value
                'End If
            Next
            
            If UsarIndice Then
                Me.Add rs.Fields("ID").Value, rs.Fields("TIPO_SOLICITUD").Value, oDenominaciones, oAyudas, rs.Fields("TIPO").Value, rs.Fields("SUBTIPO").Value, rs.Fields("INTRO").Value, rs.Fields("MINNUM").Value, rs.Fields("MAXNUM").Value, rs.Fields("MINFEC").Value, rs.Fields("MAXFEC").Value, rs.Fields("TIPO_CAMPO_GS").Value, rs.Fields("ID_ATRIB_GS").Value, rs.Fields("ORDEN").Value, rs.Fields("ES_SUBCAMPO").Value, rs.Fields("FECACT").Value, oCampoPadre, lIndice
                lIndice = lIndice + 1
            Else
                Me.Add rs.Fields("ID").Value, rs.Fields("TIPO_SOLICITUD").Value, oDenominaciones, oAyudas, rs.Fields("TIPO").Value, rs.Fields("SUBTIPO").Value, rs.Fields("INTRO").Value, rs.Fields("MINNUM").Value, rs.Fields("MAXNUM").Value, rs.Fields("MINFEC").Value, rs.Fields("MAXFEC").Value, rs.Fields("TIPO_CAMPO_GS").Value, rs.Fields("ID_ATRIB_GS").Value, rs.Fields("ORDEN").Value, rs.Fields("ES_SUBCAMPO").Value, rs.Fields("FECACT").Value, oCampoPadre
            End If
            
            rs.MoveNext
            
        Wend
        
        rs.Close
        Set rs = Nothing
        
    
    End If
    Set oGestorParametros = Nothing
    Set oIdiomas = Nothing
    
End Sub

Public Function AnyadirCamposDeAtributos() As TipoErrorSummit
    Dim sConsulta As String
    Dim TESError As TipoErrorSummit
    Dim oCampo As CCampoPredef
    Dim btrans As Boolean
    Dim rs As adodb.Recordset
    Dim oDen As CMultiidioma
    Dim sCod As String
    Dim sDen As String
    Dim sLista As String
    Dim sListaDen As String
    Dim lOrden As Long
    
    '******************* Precondicion *******************
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CCamposPredef.AnyadirCamposDeAtributos", "No se ha establecido la conexion"
    End If
    '*****************************************************
    
    On Error GoTo Error:

    TESError.NumError = TESnoerror

    m_oConexion.BeginTransaction
    btrans = True
    Set rs = New adodb.Recordset

    For Each oCampo In Me
        sCod = ""
        sDen = ""
        If Not oCampo.Denominaciones Is Nothing Then
            For Each oDen In oCampo.Denominaciones
                sCod = sCod & "DEN_" & oDen.Cod & ","
                sDen = sDen & "DEN,"
            Next
        End If
    
        If Not oCampo.CampoPadre Is Nothing Then
            sConsulta = "SELECT MAX(CAMPOS_PREDEF.ORDEN) AS ORDEN FROM CAMPOS_PREDEF INNER JOIN DESGLOSE_CAMPOS_PREDEF WITH (NOLOCK)"
            sConsulta = sConsulta & " ON DESGLOSE_CAMPOS_PREDEF.CAMPO_HIJO=CAMPOS_PREDEF.ID AND DESGLOSE_CAMPOS_PREDEF.CAMPO_PADRE=" & oCampo.CampoPadre.Id
            sConsulta = sConsulta & " WHERE TIPO_SOLICITUD=" & oCampo.TipoSolicitud & " AND ES_SUBCAMPO=1"
        Else
            sConsulta = "SELECT MAX(ORDEN) AS ORDEN FROM CAMPOS_PREDEF WHERE TIPO_SOLICITUD=" & oCampo.TipoSolicitud & " AND ES_SUBCAMPO=0"
        End If
        rs.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
        If IsNull(rs.Fields("ORDEN").Value) Then
            oCampo.Orden = 1
        Else
            oCampo.Orden = rs.Fields("ORDEN").Value + 1
        End If
        rs.Close

        sConsulta = "INSERT INTO CAMPOS_PREDEF (TIPO_SOLICITUD," & sCod & "TIPO,SUBTIPO,INTRO,MINNUM,MAXNUM,MINFEC,MAXFEC,ID_ATRIB_GS,ORDEN,ES_SUBCAMPO)"
        sConsulta = sConsulta & " SELECT " & oCampo.TipoSolicitud & "," & sDen & TipoCampoPredefinido.Atributo & "," & oCampo.Tipo
        sConsulta = sConsulta & ",CASE WHEN LISTA_EXTERNA=1 THEN 2 ELSE INTRO END,MINNUM,MAXNUM,MINFEC,MAXFEC," & oCampo.IdAtrib & "," & oCampo.Orden & "," & BooleanToSQLBinary(oCampo.EsSubCampo) & " FROM DEF_ATRIB WITH (NOLOCK) WHERE ID=" & oCampo.IdAtrib
        m_oConexion.ADOCon.Execute sConsulta
        If m_oConexion.ADOCon.Errors.Count > 0 Then
            GoTo Error
        End If

        'Obtiene el id del campo introducido:
        sConsulta = "SELECT MAX(ID) AS ID FROM CAMPOS_PREDEF"
        rs.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
        oCampo.Id = rs("ID").Value
        rs.Close

        'Si el atributo es de lista insertamos la lista tambi�n:
        sConsulta = "SELECT COUNT(*) AS SUMA FROM DEF_ATRIB_LISTA WITH (NOLOCK) WHERE ATRIB=" & oCampo.IdAtrib
        rs.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
        If Not rs.eof Then
            If rs.Fields("SUMA") > 0 Then
                sLista = ""
                sListaDen = ""
                For Each oDen In oCampo.Denominaciones
                    sLista = sLista & ",VALOR_TEXT_" & oDen.Cod
                    sListaDen = sListaDen & ",VALOR_TEXT"
                Next
                sConsulta = "INSERT INTO CAMPO_PREDEF_VALOR_LISTA (CAMPO,ORDEN,VALOR_NUM,VALOR_FEC" & sLista & ") SELECT "
                sConsulta = sConsulta & oCampo.Id & ",ORDEN,VALOR_NUM,VALOR_FEC" & sListaDen & " FROM DEF_ATRIB_LISTA WITH (NOLOCK) WHERE ATRIB=" & oCampo.IdAtrib
                m_oConexion.ADOCon.Execute sConsulta
            End If
        End If
        rs.Close

        'Si es un campo de desglose se a�ade en la tabla DESGLOSE_CAMPOS_PREDEF
        If Not oCampo.CampoPadre Is Nothing Then
            sConsulta = "SELECT MAX(ORDEN) AS ORDEN FROM DESGLOSE_CAMPOS_PREDEF WHERE CAMPO_PADRE=" & oCampo.CampoPadre.Id
            rs.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
            If IsNull(rs.Fields("ORDEN").Value) Then
                lOrden = 1
            Else
                lOrden = rs.Fields("ORDEN").Value + 1
            End If
            rs.Close
        
            sConsulta = "INSERT INTO DESGLOSE_CAMPOS_PREDEF (CAMPO_PADRE,CAMPO_HIJO,ORDEN) VALUES (" & oCampo.CampoPadre.Id & "," & oCampo.Id & "," & lOrden & ")"
            m_oConexion.ADOCon.Execute sConsulta
        End If
    
        sConsulta = "SELECT FECACT FROM CAMPOS_PREDEF WITH (NOLOCK) WHERE ID=" & oCampo.Id
        rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
        If Not rs.eof Then
            oCampo.FecAct = rs(0).Value
        End If
        rs.Close
    Next
    
    m_oConexion.CommitTransaction
    btrans = False
    
Salir:
    Set rs = Nothing
    AnyadirCamposDeAtributos = TESError
    Exit Function
Error:
    TESError = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    If btrans Then m_oConexion.RollbackTransaction
    Resume Salir
End Function


Public Function EliminarCamposPredefDeBaseDatos(Codigos As Variant) As TipoErrorSummit
'*********************************************************************
'*** Descripci�n: Elimina el/los campo/s seleccionados de la       ***
'***              base de datos.                                   ***
'*** Par�metros:  Codigos ::>  Es un array con los id de los       ***
'***                           campos a eliminar                   ***
'*** Valor que devuelve: Un TipoErrorSummit que es una estructura  ***
'***                     de datos definida en CTiposDeDatos, y, en ***
'***                     caso de ocurrir alg�n error al borrar, el ***
'***                     campo Arg1 de TipoErrorSummit contendr�   ***
'***                     un array bidimensional con los datos      ***
'***                     (c�digo,codigo de datos relacionado,      ***
'***                     bookmark de la linea de denominaci�n del  ***
'***                     destino actual) de los campos que no      ***
'***                     pudieron ser eliminados                   ***
'*********************************************************************

Dim udtTESError As TipoErrorSummit
Dim i As Integer
Dim rs As adodb.Recordset
Dim sConsulta As String

udtTESError.NumError = TESnoerror

On Error GoTo Error
    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"

    For i = 1 To UBound(Codigos)
        sConsulta = "DELETE FROM DESGLOSE_CAMPOS_PREDEF WHERE CAMPO_PADRE=" & Codigos(i) & " OR CAMPO_HIJO=" & Codigos(i)
        m_oConexion.ADOCon.Execute sConsulta
        If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error
        sConsulta = "DELETE FROM CAMPO_PREDEF_VALOR_LISTA WHERE CAMPO=" & Codigos(i)
        m_oConexion.ADOCon.Execute sConsulta
        If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error
        sConsulta = "DELETE FROM CAMPOS_PREDEF WHERE ID=" & Codigos(i)
        m_oConexion.ADOCon.Execute sConsulta
        If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error
        'Actualiza el orden de los campos del mismo grupo:
        If Not mCol.Item(CStr(Codigos(i))).CampoPadre Is Nothing Then
            sConsulta = "UPDATE CAMPOS_PREDEF SET ORDEN=ORDEN - 1 WHERE ID IN "
            sConsulta = sConsulta & " (SELECT ID FROM CAMPOS_PREDEF C WITH (NOLOCK) INNER JOIN DESGLOSE_CAMPOS_PREDEF D WITH (NOLOCK) ON C.ID=D.CAMPO_HIJO AND D.CAMPO_PADRE=" & mCol.Item(CStr(Codigos(i))).CampoPadre.Id
            sConsulta = sConsulta & " WHERE ES_SUBCAMPO=1 AND TIPO_SOLICITUD=" & mCol.Item(CStr(Codigos(i))).TipoSolicitud & " AND C.ORDEN >" & mCol.Item(CStr(Codigos(i))).Orden & ")"
        Else
            sConsulta = "UPDATE CAMPOS_PREDEF SET ORDEN=ORDEN - 1"
            sConsulta = sConsulta & " WHERE ES_SUBCAMPO=0 AND TIPO_SOLICITUD=" & mCol.Item(CStr(Codigos(i))).TipoSolicitud & " AND ORDEN >" & mCol.Item(CStr(Codigos(i))).Orden
        End If
        m_oConexion.ADOCon.Execute sConsulta
        If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error
    Next i
    
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    EliminarCamposPredefDeBaseDatos = udtTESError
    Exit Function

Error:
    udtTESError = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
    EliminarCamposPredefDeBaseDatos = udtTESError

End Function

'*********************************************************************
'*** Descripci�n: A�ade el/los campo/s seleccionados a la          ***
'***              base de datos.                                   ***
'*** Valor que devuelve: Un TipoErrorSummit que es una estructura  ***
'***                     de datos definida en CTiposDeDatos, y, en ***
'***                     caso de ocurrir alg�n error al borrar, el ***
'***                     campo Arg1 de TipoErrorSummit contendr�   ***
'***                     un array bidimensional con los datos      ***
'***                     (c�digo,codigo de datos relacionado,      ***
'***                     bookmark de la linea de denominaci�n del  ***
'***                     destino actual) de los campos que no      ***
'***                     pudieron ser a�adidos                     ***
'*********************************************************************
Public Function AnyadirCamposPredefABaseDatos() As TipoErrorSummit
Dim sConsulta As String
Dim TESError As TipoErrorSummit
Dim AdoRes As adodb.Recordset
Dim sCod As String
Dim sDen As String
Dim oDen As CMultiidioma
Dim sAyuda As String
Dim sAyudaDen As String
Dim oCampo As CCampoPredef
Dim lCampoPadre As Long
Dim lOrden As Long

'********* Precondicion *******************
If m_oConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CCamposPredef.AnyadirCamposPredefABaseDatos", "No se ha establecido la conexion"
End If

'******************************************

On Error GoTo Error:
    
    TESError.NumError = TESnoerror
    
    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
    
    For Each oCampo In mCol
        sCod = ""
        sDen = ""
        sAyuda = ""
        sAyudaDen = ""
        
        If Not oCampo.Denominaciones Is Nothing Then
            For Each oDen In oCampo.Denominaciones
                sCod = sCod & "," & "DEN_" & oDen.Cod
                sDen = sDen & "," & StrToSQLNULL(oDen.Den)
            Next
        End If
        
        If Not oCampo.Ayudas Is Nothing Then
            For Each oDen In oCampo.Ayudas
                sAyuda = sAyuda & "," & "AYUDA_" & oDen.Cod
                sAyudaDen = sAyudaDen & "," & StrToSQLNULL(oDen.Den)
            Next
        End If
    
        Set AdoRes = New adodb.Recordset
        
        If Not oCampo.CampoPadre Is Nothing Then
            sConsulta = "SELECT MAX(CAMPOS_PREDEF.ORDEN) AS ORDEN FROM CAMPOS_PREDEF INNER JOIN DESGLOSE_CAMPOS_PREDEF WITH (NOLOCK)"
            sConsulta = sConsulta & " ON DESGLOSE_CAMPOS_PREDEF.CAMPO_HIJO=CAMPOS_PREDEF.ID AND DESGLOSE_CAMPOS_PREDEF.CAMPO_PADRE=" & oCampo.CampoPadre.Id
            sConsulta = sConsulta & " WHERE TIPO_SOLICITUD=" & oCampo.TipoSolicitud & " AND ES_SUBCAMPO=1"
        Else
            sConsulta = "SELECT MAX(ORDEN) AS ORDEN FROM CAMPOS_PREDEF WHERE TIPO_SOLICITUD=" & oCampo.TipoSolicitud & " AND ES_SUBCAMPO=0"
        End If
        
        AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
        If IsNull(AdoRes.Fields("ORDEN").Value) Then
            oCampo.Orden = 1
        Else
            oCampo.Orden = AdoRes.Fields("ORDEN").Value + 1
        End If
        AdoRes.Close
    
        'Inserta el campo en BD:
        sConsulta = "INSERT INTO CAMPOS_PREDEF (TIPO_SOLICITUD " & sCod & sAyuda & ",TIPO,SUBTIPO,INTRO," & _
                    "MINNUM,MAXNUM,MINFEC,MAXFEC,TIPO_CAMPO_GS,ORDEN,ES_SUBCAMPO,PRES5) " & _
                    " VALUES (" & oCampo.TipoSolicitud & sDen & sAyudaDen & "," & oCampo.TipoPredef & "," & oCampo.Tipo & "," & oCampo.TipoIntroduccion & _
                    "," & DblToSQLFloat(oCampo.MinNum) & "," & DblToSQLFloat(oCampo.MaxNum) & "," & DblToSQLFloat(oCampo.MinFec) & "," & DblToSQLFloat(oCampo.MaxFec) & _
                    "," & IIf(IsNull(oCampo.TipoCampoGS), "NULL", oCampo.TipoCampoGS) & "," & oCampo.Orden & "," & BooleanToSQLBinary(oCampo.EsSubCampo) & "," & StrToSQLNULL(oCampo.PRES5) & ")"
        m_oConexion.ADOCon.Execute sConsulta
        If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error
        'Obtiene el id y la fecha de actualizaci�n del campo insertado:
        sConsulta = "SELECT MAX(ID) AS ID FROM CAMPOS_PREDEF WHERE TIPO_SOLICITUD=" & oCampo.TipoSolicitud
        AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
        oCampo.Id = AdoRes("ID").Value
        AdoRes.Close
        
        'Si es un campo de desglose se a�ade en la tabla DESGLOSE_CAMPOS_PREDEF
        If Not oCampo.CampoPadre Is Nothing Then
            sConsulta = "SELECT MAX(ORDEN) AS ORDEN FROM DESGLOSE_CAMPOS_PREDEF WHERE CAMPO_PADRE=" & oCampo.CampoPadre.Id
            AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
            If IsNull(AdoRes.Fields("ORDEN").Value) Then
                lOrden = 1
            Else
                lOrden = AdoRes.Fields("ORDEN").Value + 1
            End If
            AdoRes.Close
        
            sConsulta = "INSERT INTO DESGLOSE_CAMPOS_PREDEF (CAMPO_PADRE,CAMPO_HIJO,ORDEN) VALUES (" & oCampo.CampoPadre.Id & "," & oCampo.Id & "," & lOrden & ")"
            m_oConexion.ADOCon.Execute sConsulta
            If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error
        Else
            If oCampo.TipoCampoGS = TipoCampoGS.Desglose Then
                lCampoPadre = oCampo.Id
            Else
                'Es un campo de desglose de material:
                If oCampo.EsSubCampo = True Then
                    sConsulta = "SELECT MAX(ORDEN) AS ORDEN FROM DESGLOSE_CAMPOS_PREDEF WHERE CAMPO_PADRE=" & lCampoPadre
                    AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
                    If IsNull(AdoRes.Fields("ORDEN").Value) Then
                        lOrden = 1
                    Else
                        lOrden = AdoRes.Fields("ORDEN").Value + 1
                    End If
                    AdoRes.Close
            
                    sConsulta = "INSERT INTO DESGLOSE_CAMPOS_PREDEF (CAMPO_PADRE,CAMPO_HIJO,ORDEN) VALUES (" & lCampoPadre & "," & oCampo.Id & "," & lOrden & ")"
                    m_oConexion.ADOCon.Execute sConsulta
                    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error
                End If
            End If
        End If
        
        'Obtiene la fecha de actualizaci�n:
        AdoRes.Open "SELECT FECACT FROM CAMPOS_PREDEF WITH (NOLOCK) WHERE ID=" & oCampo.Id, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
        oCampo.FecAct = AdoRes(0).Value
        AdoRes.Close
        Set AdoRes = Nothing
        
    Next
    
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    AnyadirCamposPredefABaseDatos = TESError
    Exit Function

Error:
    
    AnyadirCamposPredefABaseDatos = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    
    m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"

    Set AdoRes = Nothing
End Function
