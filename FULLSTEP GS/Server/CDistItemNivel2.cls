VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CDistItemNivel2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CDistItemNivel2 **********************************
'*             Autor : Javier Arana
'*             Creada : 16/6/99
'***************************************************************

Option Explicit


Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

Private mvarItem As CItem
Private mvarCodUON1 As String
Private mvarCodUON2 As String
Private mvarDen As Variant
Private mvarPorcentaje As Double
Private mvarIndice As Variant
Private mvarConexion As CConexion

Public Property Let Indice(ByVal varIndice As Variant)
    mvarIndice = varIndice
End Property
Public Property Get Indice() As Variant
    Indice = mvarIndice
End Property
Public Property Let Porcentaje(ByVal d As Double)
    mvarPorcentaje = d
End Property
Public Property Get Porcentaje() As Double
    Porcentaje = mvarPorcentaje
End Property
Public Property Get Item() As CItem
    Set Item = mvarItem
End Property
Public Property Set Item(ByVal oItem As CItem)
    Set mvarItem = oItem
End Property
Public Property Get CodUON1() As String
    CodUON1 = mvarCodUON1
End Property
Public Property Let CodUON1(ByVal varCod As String)
    mvarCodUON1 = varCod
End Property
Public Property Get Den() As Variant
    Den = mvarDen
End Property
Public Property Let Den(ByVal var As Variant)
    mvarDen = var
End Property

Public Property Let CodUON2(ByVal varCod As String)
    mvarCodUON2 = varCod
End Property
Public Property Get CodUON2() As String
    CodUON2 = mvarCodUON2
End Property
Friend Property Set Conexion(ByVal con As CConexion)
Set mvarConexion = con
End Property

Public Property Get Conexion() As CConexion
Set Conexion = mvarConexion
End Property

Private Sub Class_Terminate()
    Set mvarConexion = Nothing
End Sub

