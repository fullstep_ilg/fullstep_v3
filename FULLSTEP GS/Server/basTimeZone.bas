Attribute VB_Name = "basTimeZone"
Option Explicit

Public Declare Function SystemTimeToTzSpecificLocalTime Lib "kernel32" (lpTimeZoneInformation As TIME_ZONE_INFORMATION, lpUniversalTime As SYSTEMTIME, lpLocalTime As SYSTEMTIME) As Long
Public Declare Function TzSpecificLocalTimeToSystemTime Lib "kernel32" (lpTimeZoneInformation As TIME_ZONE_INFORMATION, ByRef lpLocalTime As SYSTEMTIME, ByRef lpUniversalTime As SYSTEMTIME) As Long
Public Declare Function MultiByteToWideChar Lib "kernel32" (ByVal CodePage As Long, ByVal dwFlags As Long, lpMultiByteStr As Any, ByVal cchMultiByte As Long, lpWideCharStr As Any, ByVal cchWideChar As Long) As Long
Public Declare Function GetVersionEx Lib "kernel32" Alias "GetVersionExA" (lpVersionInformation As OSVERSIONINFO) As Long

Private Type OSVERSIONINFO
        dwOSVersionInfoSize As Long
        dwMajorVersion As Long
        dwMinorVersion As Long
        dwBuildNumber As Long
        dwPlatformId As Long
        szCSDVersion As String * 128
End Type

Private Type SYSTEMTIME
   wYear As Integer
   wMonth As Integer
   wDayOfWeek As Integer
   wDay As Integer
   wHour As Integer
   wMinute As Integer
   wSecond As Integer
   wMilliseconds As Integer
End Type

Private Type TIME_ZONE_INFORMATION
   Bias As Long
   StandardName(0 To 63) As Byte
   StandardDate As SYSTEMTIME
   StandardBias As Long
   DaylightName(0 To 63) As Byte
   DaylightDate As SYSTEMTIME
   DaylightBias As Long
End Type

Private Type REGTIMEZONEINFORMATION
   Bias As Long
   StandardBias As Long
   DaylightBias As Long
   StandardDate As SYSTEMTIME
   DaylightDate As SYSTEMTIME
End Type

Private Const CP_ACP = 0
Private Const MB_PRECOMPOSED = &H1

Private Const TIME_ZONE_ID_INVALID = &HFFFFFFFF
Private Const TIME_ZONE_ID_UNKNOWN = 0
Private Const TIME_ZONE_ID_STANDARD = 1
Private Const TIME_ZONE_ID_DAYLIGHT = 2
Private Const VER_PLATFORM_WIN32_NT = 2
Private Const VER_PLATFORM_WIN32_WINDOWS = 1
Private Const SKEY_NT = "SOFTWARE\Microsoft\Windows NT\CurrentVersion\Time Zones"
Private Const SKEY_9X = "SOFTWARE\Microsoft\Windows\CurrentVersion\Time Zones"
Public Const GMT1_TZ = "Romance Standard Time"

''' <summary>Convierte una fecha en una zona horaria a UTC</summary>
''' <param name="sTZOrgKey">Clave principal del registro de la zona horaria de origen</param>
''' <param name="dtFechaTZ">Fecha en la zona horaria origen</param>
''' <param name="dtFechaUTC">Fecha UTC</param>
''' <remarks>Llamada desde </remarks>

Public Function ConvertirTZaUTC(ByVal sTZOrgKey As String, ByVal dtFechaTZ As Date) As Date
    Dim tzOrg As TIME_ZONE_INFORMATION
    Dim lRet As Long
    Dim stOrg As SYSTEMTIME
    Dim stUTC As SYSTEMTIME
    Dim dtFechaUTC As Date
    Dim dtHoraUTC As Date
    
    If Len(sTZOrgKey) > 0 Then
        'Pasar de TZ a UTC
        tzOrg = GetTZData(sTZOrgKey)
        stOrg.wYear = Year(dtFechaTZ)
        stOrg.wMonth = Month(dtFechaTZ)
        stOrg.wDay = Day(dtFechaTZ)
        stOrg.wHour = Hour(dtFechaTZ)
        stOrg.wMinute = Minute(dtFechaTZ)
        stOrg.wSecond = Second(dtFechaTZ)
        stOrg.wMilliseconds = 0
        lRet = TzSpecificLocalTimeToSystemTime(tzOrg, stOrg, stUTC)
        
        dtFechaUTC = DateSerial(stUTC.wYear, stUTC.wMonth, stUTC.wDay)
        dtHoraUTC = TimeSerial(stUTC.wHour, stUTC.wMinute, stUTC.wSecond)
        
        ConvertirTZaUTC = dtFechaUTC + dtHoraUTC
    End If
End Function

''' <summary>Convierte una fecha UTC a una zona horaria</summary>
''' <param name="dtFechaUTC">Fecha UTC</param>
''' <param name="sTZDestKey">Clave principal del registro de la zona horaria destino</param>
''' <param name="dtFechaTZ">Fecha en la zona horaria destino</param>
''' <remarks>Llamada desde </remarks>

Public Function ConvertirUTCaTZ(ByVal dtFechaUTC As Date, ByVal sTZDestKey As String) As Date
    Dim tzDest As TIME_ZONE_INFORMATION
    Dim lRet As Long
    Dim stUTC As SYSTEMTIME
    Dim stDest As SYSTEMTIME
    Dim dtFechaTZ As Date
    Dim dtHoraTZ As Date
    
    If Len(sTZDestKey) > 0 Then
        ''Pasar de UTC a TZ
        stUTC.wYear = Year(dtFechaUTC)
        stUTC.wMonth = Month(dtFechaUTC)
        stUTC.wDay = Day(dtFechaUTC)
        stUTC.wHour = Hour(dtFechaUTC)
        stUTC.wMinute = Minute(dtFechaUTC)
        stUTC.wSecond = Second(dtFechaUTC)
        stUTC.wMilliseconds = 0
            
        tzDest = GetTZData(sTZDestKey)
        lRet = SystemTimeToTzSpecificLocalTime(tzDest, stUTC, stDest)
        
        dtFechaTZ = DateSerial(stDest.wYear, stDest.wMonth, stDest.wDay)
        dtHoraTZ = TimeSerial(stDest.wHour, stDest.wMinute, stDest.wSecond)
        
        ConvertirUTCaTZ = dtFechaTZ + dtHoraTZ
    End If
End Function

''' <summary>Obtiene la estructura Time Zone guardada en el valor TZ de la clave pasada</summary>
''' <param name="strTZKey">Clave de la zona horaria</param>
''' <returns>Estructura TIME_ZONE_INFORMATION con los datos de la zona horaria</returns>
''' <remarks>Llamada desde ConvertirUTCaTZ y ConvertirTZaUTC</remarks>

Private Function GetTZData(ByVal strTZKey As String) As TIME_ZONE_INFORMATION
    Dim lRetVal As Long
    Dim lKey As Long
    Dim rTZI As REGTIMEZONEINFORMATION
    Dim tzInfo As TIME_ZONE_INFORMATION
    Dim sStd As String
    Dim bytDLTName(32) As Byte
    Dim bytSTDName(32) As Byte
    Dim lKeyValSize As Long
    Dim lKeyValType  As Long
    Dim sTimeZonesKey As String
    
    sTimeZonesKey = GetTimeZonesKey
        
    lRetVal = RegOpenKeyEx(HKEY_LOCAL_MACHINE, sTimeZonesKey & "\" & strTZKey, 0, KEY_QUERY_VALUE, lKey)
    If lRetVal = ERROR_NONE Then
        lRetVal = RegQueryValueExAny(lKey, "TZI", 0&, ByVal 0&, rTZI, Len(rTZI))
        If lRetVal = ERROR_NONE Then
            tzInfo.Bias = rTZI.Bias
            tzInfo.StandardBias = rTZI.StandardBias
            tzInfo.DaylightBias = rTZI.DaylightBias
            tzInfo.StandardDate = rTZI.StandardDate
            tzInfo.DaylightDate = rTZI.DaylightDate
        End If
        
        lKeyValSize = 32
        lKeyValType = REG_SZ
        lRetVal = RegQueryValueExAny(lKey, "Std", 0&, lKeyValType, bytSTDName(0), lKeyValSize)
        If lRetVal = ERROR_NONE Then
            MultiByteToWideChar CP_ACP, MB_PRECOMPOSED, bytSTDName(0), lKeyValSize, tzInfo.StandardName(0), 32
        End If
        
        lKeyValSize = 32
        lKeyValType = REG_SZ
        lRetVal = RegQueryValueExAny(lKey, "Dlt", 0&, lKeyValType, bytDLTName(0), lKeyValSize)
        If lRetVal = ERROR_NONE Then
            MultiByteToWideChar CP_ACP, MB_PRECOMPOSED, bytDLTName(0), lKeyValSize, tzInfo.DaylightName(0), 32
        End If
        
        lRetVal = RegCloseKey(lKey)
    End If
    
    GetTZData = tzInfo
End Function

''' <summary>Obtiene la clave del registro en la que se encuentran las zonas horarias</summary>
''' <returns>String con la clave del registro</returns>
''' <remarks>Llamada desde GetTimeZoneDisplayName, ObtenerZonasHorarias y GetTZData</remarks>

Private Function GetTimeZonesKey() As String
    Dim osV As OSVERSIONINFO
    
    GetTimeZonesKey = vbNullString
    
    osV.dwOSVersionInfoSize = Len(osV)
    GetVersionEx osV
    If osV.dwPlatformId = VER_PLATFORM_WIN32_NT Then
       GetTimeZonesKey = SKEY_NT
    Else
       GetTimeZonesKey = SKEY_9X
    End If
End Function
