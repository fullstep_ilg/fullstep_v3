VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CPetOfertas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CAsiganciones **********************************
'*             Autor : Javier Arana
'*             Creada : 24/3/99
'****************************************************************


Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum
Private mCol As Collection
Private mvarConexion As CConexion
Private mvarConexionServer As CConexionDeUseServer

Public Function Add(ByVal CodProve As String, ByVal DenProve As String, ByVal bWeb As Boolean, ByVal bEMail As Boolean, ByVal bImpreso As Boolean, ByVal Fecha As Date, ByVal Id As Integer, ByVal Apellidos As String, Optional ByVal Nombre As Variant, Optional ByVal Email As Variant, Optional ByVal varIndice As Variant, _
 Optional ByVal Tfno As Variant, Optional ByVal Tfno2 As Variant, Optional ByVal Fax As Variant, Optional ByVal FechaReu As Variant, Optional ByVal TipoComunicacion As Variant, Optional ByVal tfnomovil As Variant, Optional ByVal bAntesDeCierreParcial As Boolean, Optional ByVal bContactoPortal As Boolean) As CPetOferta

    'create a new object
    Dim objnewmember As CPetOferta
    Dim sCod As String
    
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New CPetOferta
   
    objnewmember.CodProve = CodProve
    objnewmember.DenProve = DenProve
    objnewmember.viaEMail = bEMail
    objnewmember.ViaWeb = bWeb
    objnewmember.ViaImp = bImpreso
    objnewmember.Fecha = Fecha
    objnewmember.Id = Id
    objnewmember.Apellidos = Apellidos
    Set objnewmember.Conexion = mvarConexion
    Set objnewmember.ConexionServer = mvarConexionServer
    
    If IsMissing(Email) Then
        objnewmember.Email = Null
    Else
        objnewmember.Email = Email
    End If
    
    If IsMissing(Tfno) Then
        objnewmember.Tfno = Null
    Else
        objnewmember.Tfno = Tfno
    End If
    
    If IsMissing(Tfno2) Then
        objnewmember.Tfno2 = Null
    Else
        objnewmember.Tfno2 = Tfno2
    End If
    
    If IsMissing(tfnomovil) Then
        objnewmember.tfnomovil = Null
    Else
        objnewmember.tfnomovil = tfnomovil
    End If

    
    If IsMissing(Fax) Then
        objnewmember.Fax = Null
    Else
        objnewmember.Fax = Fax
    End If
    
    If IsMissing(Nombre) Then
        objnewmember.Nombre = Null
    Else
        objnewmember.Nombre = Nombre
    End If
    
    If IsMissing(FechaReu) Then
        objnewmember.fechareunion = Null
    Else
        objnewmember.fechareunion = FechaReu
    End If
    
    If IsMissing(TipoComunicacion) Then
        objnewmember.TipoComunicacion = Null
    Else
        objnewmember.TipoComunicacion = TipoComunicacion
    End If
    
    If IsMissing(bAntesDeCierreParcial) Then
        objnewmember.AntesDeCierreParcial = False
    Else
        objnewmember.AntesDeCierreParcial = bAntesDeCierreParcial
    End If
    
    If IsMissing(bContactoPortal) Then
        objnewmember.PortContacto = False
    Else
        objnewmember.PortContacto = bContactoPortal
    End If
    
    If Not IsMissing(varIndice) And Not IsNull(varIndice) Then
        objnewmember.Indice = varIndice
        mCol.Add objnewmember, CStr(varIndice)
    Else
    'set the properties passed into the method
        mCol.Add objnewmember, CodProve & CStr(Id)
    End If
    
    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CPetOfertas", "Add", ERR, Erl)
      Exit Function
   End If

End Function

Public Property Get Item(vntIndexKey As Variant) As CPetOferta
    On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Friend Property Set Conexion(ByVal con As CConexion)
    Set mvarConexion = con
End Property

Friend Property Get Conexion() As CConexion
    Set Conexion = mvarConexion
End Property

Friend Property Set ConexionServer(ByVal con As CConexionDeUseServer)
    Set mvarConexionServer = con
End Property

Friend Property Get ConexionServer() As CConexionDeUseServer
    Set ConexionServer = mvarConexionServer
End Property

Public Property Get Count() As Long
    If mCol Is Nothing Then
        Count = 0
    Else
         Count = mCol.Count
    End If
End Property

Public Sub Remove(vntIndexKey As Variant)
    If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
    mCol.Remove vntIndexKey

    Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CPetOfertas", "Remove", ERR, Erl)
      Exit Sub
   End If
End Sub

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
     Set NewEnum = mCol.[_NewEnum]
End Property

Private Sub Class_Initialize()
    Set mCol = New Collection
End Sub

Private Sub Class_Terminate()
    'destroys collection when this class is terminated
    Set mCol = Nothing
    Set mvarConexion = Nothing
    Set mvarConexionServer = Nothing
End Sub



