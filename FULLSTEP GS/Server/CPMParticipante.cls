VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CPMParticipante"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum


'Implements IBaseDatos

'Private moConexion As CConexion

Private mlID As Long
Private mlRol As Long
Private msPer As String
Private msGMN1 As String
Private msGMN2 As String
Private msGMN3 As String
Private msGMN4 As String

'Private m_adores As ADODB.Recordset

Private mvarIndice As Variant

'Friend Property Set Conexion(ByVal con As CConexion)
'Set moConexion = con
'End Property
'
'Friend Property Get Conexion() As CConexion
'Set Conexion = moConexion
'End Property

Public Property Get ID() As Long
    ID = mlID
End Property
Public Property Let ID(ByVal Value As Long)
    mlID = Value
End Property

Public Property Get Rol() As Long
    Rol = mlRol
End Property
Public Property Let Rol(ByVal Value As Long)
    mlRol = Value
End Property

Public Property Get Per() As String
     Per = msPer
End Property
Public Property Let Per(ByVal Value As String)
    msPer = Value
End Property

Public Property Get GMN1() As String
     GMN1 = msGMN1
End Property
Public Property Let GMN1(ByVal Value As String)
    msGMN1 = Value
End Property

Public Property Get GMN2() As String
     GMN2 = msGMN2
End Property
Public Property Let GMN2(ByVal Value As String)
    msGMN2 = Value
End Property

Public Property Get GMN3() As String
     GMN3 = msGMN3
End Property
Public Property Let GMN3(ByVal Value As String)
    msGMN3 = Value
End Property

Public Property Get GMN4() As String
     GMN4 = msGMN4
End Property
Public Property Let GMN4(ByVal Value As String)
    msGMN4 = Value
End Property


Public Property Let Indice(ByVal varIndice As Variant)
    mvarIndice = varIndice
End Property
Public Property Get Indice() As Variant
    Indice = mvarIndice
End Property


Private Sub Class_Terminate()
    'Set moConexion = Nothing
End Sub

