VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CCondBloqueo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum


Implements IBaseDatos

Private moConexion As CConexion

Private mlID As Long
Private msCod As String
Private miTipo As Integer
Private msMensajeSpa As String
Private msMensajeEng As String
Private msMensajeGer As String
Private msMensajeFra As String
Private miTipoBloqueo As Integer
Private mlWorkflow As Long
Private mdtFecAct As Variant
Private msFormula As String
Private m_adores As adodb.Recordset
'Private moAcciones As CAcciones

Private mvarIndice As Variant


Friend Property Set Conexion(ByVal con As CConexion)
Set moConexion = con
End Property
Friend Property Get Conexion() As CConexion
Set Conexion = moConexion
End Property

Public Property Get Id() As Long
    Id = mlID
End Property
Public Property Let Id(ByVal Data As Long)
    mlID = Data
End Property

Public Property Get Cod() As String
    Cod = msCod
End Property
Public Property Let Cod(ByVal Data As String)
    msCod = Data
End Property
Public Property Get Tipo() As Integer
    Tipo = miTipo
End Property
Public Property Let Tipo(ByVal Data As Integer)
    miTipo = Data
End Property
Public Property Get MensajeSpa() As String
    MensajeSpa = msMensajeSpa
End Property
Public Property Let MensajeSpa(ByVal Data As String)
    msMensajeSpa = Data
End Property

Public Property Get MensajeEng() As String
    MensajeEng = msMensajeEng
End Property
Public Property Let MensajeEng(ByVal Data As String)
    msMensajeEng = Data
End Property
Public Property Get MensajeGer() As String
    MensajeGer = msMensajeGer
End Property
Public Property Let MensajeGer(ByVal Data As String)
    msMensajeGer = Data
End Property
Public Property Get MensajeFra() As String
    MensajeFra = msMensajeFra
End Property
Public Property Let MensajeFra(ByVal Data As String)
    msMensajeFra = Data
End Property
Public Property Get TipoBloqueo() As Integer
    TipoBloqueo = miTipoBloqueo
End Property
Public Property Let TipoBloqueo(ByVal Data As Integer)
    miTipoBloqueo = Data
End Property
Public Property Get Workflow() As Long
    Workflow = mlWorkflow
End Property
Public Property Let Workflow(ByVal Data As Long)
    mlWorkflow = Data
End Property

Public Property Get FecAct() As Variant
    FecAct = mdtFecAct
End Property
Public Property Let FecAct(ByVal Data As Variant)
    mdtFecAct = Data
End Property
Public Property Get Formula() As String
    Formula = msFormula
End Property
Public Property Let Formula(ByVal Data As String)
    msFormula = Data
End Property


Public Property Let Indice(ByVal varIndice As Variant)
    mvarIndice = varIndice
End Property
Public Property Get Indice() As Variant
    Indice = mvarIndice
End Property


Private Sub Class_Terminate()
    Set moConexion = Nothing
End Sub

Private Function IBaseDatos_AnyadirABaseDatos() As TipoErrorSummit
Dim sConsulta As String
Dim sConsulta2 As String
Dim TESError As TipoErrorSummit
Dim AdoRes As adodb.Recordset
Dim bTransaccionEnCurso As Boolean
Dim foreingKeyValue As Long
Dim foreingKeyName As String
Dim tableName As String
Dim tipoCampoCondicion As TiposDeAtributos

TESError.NumError = TESnoerror

If moConexion Is Nothing Then
    Err.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CCondicionParticularesBloqueo.AnyadirABAseDatos", "No se ha establecido la conexion"
End If
' *****************************************

On Error GoTo Error:
    
    moConexion.ADOCon.Execute "BEGIN DISTRIBUTED TRAN"
    bTransaccionEnCurso = True
    moConexion.ADOCon.Execute "SET XACT_ABORT OFF"
       
             
    'Insetar la Condici�n
    sConsulta = "INSERT INTO PM_CONDICIONES_BLOQUEO" & _
        "(COD, TIPO, MENSAJE_SPA, MENSAJE_ENG, MENSAJE_GER, MENSAJE_FRA, TIPO_BLOQUEO, WORKFLOW, FORMULA) " & _
        "VALUES " & _
        "(" & StrToSQLNULL(msCod) & "," & miTipo & "," & StrToSQLNULL(msMensajeSpa) & "," & StrToSQLNULL(msMensajeEng) & "," & StrToSQLNULL(msMensajeGer) & "," & StrToSQLNULL(msMensajeFra) & ", " & miTipoBloqueo & " , " & mlWorkflow & "," & StrToSQLNULL(msFormula) & ")"
    
    moConexion.ADOCon.Execute sConsulta
    If moConexion.ADOCon.Errors.Count > 0 Then
        GoTo Error
    End If
        
    moConexion.ADOCon.Execute "COMMIT TRAN"
    bTransaccionEnCurso = False
              
    IBaseDatos_AnyadirABaseDatos = TESError
    
    sConsulta = "SELECT MAX(ID) ID FROM PM_CONDICIONES_BLOQUEO"
    Set AdoRes = New adodb.Recordset
    AdoRes.Open sConsulta, moConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    mlID = AdoRes("ID").Value
    AdoRes.Close
    Set AdoRes = Nothing
    
    ''' Recuperar la fecha de insercion
    sConsulta = "SELECT FECACT FROM PM_CONDICIONES_BLOQUEO WHERE ID = " & mlID
    Set AdoRes = New adodb.Recordset
    AdoRes.Open sConsulta, moConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    mdtFecAct = AdoRes("FECACT").Value
    AdoRes.Close
    Set AdoRes = Nothing
    
    Exit Function
Error:

    If Not AdoRes Is Nothing Then
        AdoRes.Close
        Set AdoRes = Nothing
    End If
                 
    IBaseDatos_AnyadirABaseDatos = basErrores.TratarError(moConexion.ADOCon.Errors)
                 
                 
    If bTransaccionEnCurso Then
        moConexion.ADOCon.Execute "ROLLBACK TRAN"
        bTransaccionEnCurso = False
    End If
    

End Function

Private Function IBaseDatos_CambiarCodigo(CodigoNuevo As Variant) As TipoErrorSummit

End Function

Private Sub IBaseDatos_CancelarEdicion()

If Not m_adores Is Nothing Then
    m_adores.Close
    Set m_adores = Nothing
End If

End Sub

Private Function IBaseDatos_ComprobarExistenciaEnBaseDatos() As Boolean

End Function

Private Function IBaseDatos_EliminarDeBaseDatos() As TipoErrorSummit
Dim TESError As TipoErrorSummit
Dim btrans As Boolean
Dim sConsulta As String
Dim AdoRes As adodb.Recordset

TESError.NumError = TESnoerror

'******************* Precondicion *******************
If moConexion Is Nothing Then
    Err.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CCondicionEnlace.EliminarDeBaseDatos", "No se ha establecido la conexion"
End If
'*****************************************************
On Error GoTo Error:
    
    moConexion.ADOCon.Execute "BEGIN TRAN"
    moConexion.ADOCon.Execute "SET XACT_ABORT OFF"
    btrans = True
    
    sConsulta = "DELETE FROM PM_CONDICIONES_BLOQUEO_CONDICIONES WHERE ID_COD = " & mlID
    moConexion.ADOCon.Execute sConsulta
    If moConexion.ADOCon.Errors.Count > 0 Then
        GoTo Error
    End If
    
    sConsulta = "DELETE FROM PM_CONDICIONES_BLOQUEO WHERE ID = " & mlID
            
    moConexion.ADOCon.Execute sConsulta
    If moConexion.ADOCon.Errors.Count > 0 Then
        GoTo Error
    End If
        
    moConexion.ADOCon.Execute "COMMIT TRAN"
    btrans = False
    Set m_adores = Nothing
    
    IBaseDatos_EliminarDeBaseDatos = TESError
    
    Exit Function
    
Error:
    
    If moConexion.ADOCon.Errors.Count > 0 Then
        IBaseDatos_EliminarDeBaseDatos = basErrores.TratarError(moConexion.ADOCon.Errors)
        If btrans Then
            moConexion.ADOCon.Execute "ROLLBACK TRAN"
            btrans = False
        End If
        Exit Function
    End If
    
    If m_adores.EditMode = 1 Then
        m_adores.CancelUpdate
    End If
End Function

Private Function IBaseDatos_FinalizarEdicionEliminando() As TipoErrorSummit

End Function

Private Function IBaseDatos_FinalizarEdicionModificando() As TipoErrorSummit
Dim TESError As TipoErrorSummit
Dim sConsulta As String
Dim sConsulta2 As String
Dim bTransaccionEnCurso As Boolean
Dim AdoRes As adodb.Recordset
Dim oExtraPoint As CEnlaceExtraPoint
Dim i As Integer
Dim tipoCampoCondicion As TipoCampoCondicionEnlace
Dim tableName As String
Dim foreingKeyName As String
Dim foreingKeyValue As Long


TESError.NumError = TESnoerror

'********* Precondicion *******************
If moConexion Is Nothing Then
    Err.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CCondicionEnlace.FinalizarEdicionModificando", "No se ha establecido la conexion"
End If
'******************************************

On Error GoTo Error:

    moConexion.ADOCon.Execute "BEGIN TRAN"
    bTransaccionEnCurso = True
    moConexion.ADOCon.Execute "SET XACT_ABORT OFF"
    
    'If miOrigen = OrigenCondicionEnlace.Enlace Then
    '    foreingKeyName = "ENLACE"
    '    foreingKeyValue = mlEnlace
    '    tableName = "PM_ENLACE_CONDICIONES"
    'Else
    '    foreingKeyName = "ACCION_PRECOND"
    '    foreingKeyValue = mlPrecondicion
    '    tableName = "PM_ACCION_CONDICIONES"
    'End If
    
    'foreingKeyName = "ID_COD"
    'foreingKeyValue = mlIDCOD
    
    tableName = "PM_CONDICIONES_BLOQUEO"
    
    'Comprobamos si ha habido cambios en otra sesi�n
    Set m_adores = New adodb.Recordset
    sConsulta = "SELECT FECACT FROM " & tableName & " WHERE ID = " & mlID
    m_adores.Open sConsulta, moConexion.ADOCon, adOpenStatic, adLockReadOnly
    
    If m_adores.eof Then
        m_adores.Close
        Set m_adores = Nothing
        TESError.NumError = TESDatoEliminado
        TESError.Arg1 = 181
        IBaseDatos_FinalizarEdicionModificando = TESError
        moConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
        Exit Function
    End If
    
    If mdtFecAct <> m_adores("FECACT").Value Then
        m_adores.Close
        Set m_adores = Nothing
        TESError.NumError = TESInfActualModificada
        moConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
        IBaseDatos_FinalizarEdicionModificando = TESError
        Exit Function
    End If
    
    ''' Actualizar Condicion
   
    sConsulta = "UPDATE " & tableName & " SET " & _
        "COD = " & StrToSQLNULL(msCod) & ", " & _
        "TIPO = " & miTipo & ", " & _
        "MENSAJE_SPA = " & StrToSQLNULL(msMensajeSpa) & ", " & _
        "MENSAJE_ENG = " & StrToSQLNULL(msMensajeEng) & ", " & _
        "MENSAJE_GER = " & StrToSQLNULL(msMensajeGer) & ", " & _
        "MENSAJE_FRA = " & StrToSQLNULL(msMensajeFra) & ", " & _
        "TIPO_BLOQUEO = " & miTipoBloqueo & ", " & _
        "WORKFLOW = " & mlWorkflow & ", " & _
        "FORMULA = " & StrToSQLNULL(msFormula)
    
    sConsulta = sConsulta & " WHERE ID = " & mlID
    moConexion.ADOCon.Execute sConsulta
    If moConexion.ADOCon.Errors.Count > 0 Then
        GoTo Error
    End If
    
    'Recogemos el nuevo valor de FECACT
    m_adores.Requery
    mdtFecAct = m_adores("FECACT").Value
    m_adores.Close
    Set m_adores = Nothing
    
    moConexion.ADOCon.Execute "COMMIT TRAN"
    bTransaccionEnCurso = False
    
    IBaseDatos_FinalizarEdicionModificando = TESError
    
    Exit Function
    

Error:
    
    IBaseDatos_FinalizarEdicionModificando = basErrores.TratarError(moConexion.ADOCon.Errors)
    
    If Not m_adores Is Nothing Then
        m_adores.Close
        Set m_adores = Nothing
    End If
    
    If bTransaccionEnCurso Then
        moConexion.ADOCon.Execute "ROLLBACK"
        moConexion.ADOCon.Execute "SET XACT_ABORT OFF"
        bTransaccionEnCurso = False
    End If

End Function

Private Function IBaseDatos_IniciarEdicion(Optional ByVal Bloquear As Boolean, Optional ByVal UsuarioBloqueo As String) As TipoErrorSummit
Dim TESError As TipoErrorSummit
Dim i As Integer

TESError.NumError = TESnoerror

'******************* Precondicion *******************
If moConexion Is Nothing Then
    Err.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CCondicionEnlace.IniciarEdicion", "No se ha establecido la conexion"
End If
'*****************************************************

'''Cargar Condicion
Set m_adores = New adodb.Recordset
m_adores.Open "select ID,COD,TIPO, TIPO_BLOQUEO, WORKFLOW, FECACT, FORMULA, MENSAJE_SPA,MENSAJE_ENG,MENSAJE_GER,MENSAJE_FRA from PM_CONDICIONES_BLOQUEO WITH (NOLOCK) where ID=" & mlID, moConexion.ADOCon, adOpenKeyset, adLockReadOnly
If m_adores.eof Then
        m_adores.Close
        Set m_adores = Nothing
        TESError.NumError = TESDatoEliminado
        TESError.Arg1 = 181
        IBaseDatos_IniciarEdicion = TESError
        Exit Function
End If


msCod = NullToStr(m_adores.Fields("COD").Value)
miTipo = NullToDbl0(m_adores.Fields("TIPO").Value)
msMensajeSpa = NullToStr(m_adores.Fields("MENSAJE_SPA").Value)
msMensajeEng = NullToStr(m_adores.Fields("MENSAJE_ENG").Value)
msMensajeGer = NullToStr(m_adores.Fields("MENSAJE_GER").Value)
msMensajeFra = NullToStr(m_adores.Fields("MENSAJE_FRA").Value)
miTipoBloqueo = NullToDbl0(m_adores.Fields("TIPO_BLOQUEO").Value)
mlWorkflow = NullToDbl0(m_adores.Fields("WORKFLOW").Value)
mdtFecAct = m_adores.Fields("FECACT").Value
msFormula = NullToStr(m_adores.Fields("FORMULA").Value)
m_adores.Close
Set m_adores = Nothing

IBaseDatos_IniciarEdicion = TESError
    
End Function






