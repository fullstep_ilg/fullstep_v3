VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CCalificaciones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CCalificaciones **********************************
'*             Autor : Javier Arana
'*             Creada : 13/8/98
'****************************************************************


Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum
Private mCol As Collection
Private mvarConexion As CConexion
Private mvarConexionServer As CConexionDeUseServer
Private mvarEOF As Boolean

Public Sub CargarTodasLasCalificaciones(ByVal iNum As Integer, Optional ByVal CaracteresInicialesCod As String, Optional ByVal CaracteresInicialesDen As String, Optional ByVal CoincidenciaTotal As Boolean, Optional ByVal OrdenadosPorDen As Boolean, Optional ByVal OrdenadosPorValorInf As Boolean, Optional ByVal OrdenadosPorValorSup As Boolean, Optional ByVal UsarIndice As Boolean, Optional ByVal Idi As String)
Dim rs As New adodb.Recordset
Dim fldCod As adodb.Field
Dim fldDen As adodb.Field
Dim fldInf As adodb.Field
Dim fldSup As adodb.Field
Dim fldIdioma As adodb.Field
Dim sConsulta As String
Dim lIndice As Long

'********* Precondicion **************************************
If mvarConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CDestinos.CargarTodosLosDestinos", "No se ha establecido la conexion"
    Exit Sub
End If
'*************************************************************

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

sConsulta = "SELECT CAL.*,CAL_IDIOMA.DEN,CAL_IDIOMA.IDIOMA FROM CAL WITH (NOLOCK) "
sConsulta = sConsulta & "LEFT JOIN CAL_IDIOMA WITH (NOLOCK) ON CAL.COD = CAL_IDIOMA.COD AND CAL.NUM = CAL_IDIOMA.NUM"

'Filtra por el idioma
If Not IsMissing(Idi) And Idi <> "" Then
    sConsulta = sConsulta & " AND IDIOMA = '" & Idi & "'"
End If

sConsulta = sConsulta & " WHERE CAL.NUM=" & iNum

If CaracteresInicialesCod = "" And CaracteresInicialesDen = "" Then
    
   
Else
    If Not (CaracteresInicialesCod = "") And Not (CaracteresInicialesDen = "") Then
        
        If CoincidenciaTotal Then
            
            
            sConsulta = sConsulta & " AND CAL.COD ='" & DblQuote(CaracteresInicialesCod) & "'"
            sConsulta = sConsulta & " AND DEN='" & DblQuote(CaracteresInicialesDen) & "'"
            
        Else
            
            
            sConsulta = sConsulta & " AND CAL.COD LIKE '" & DblQuote(CaracteresInicialesCod) & "%'"
            sConsulta = sConsulta & " DEN LIKE '" & DblQuote(CaracteresInicialesDen) & "%'"
                       
        End If
                    
    Else
        
        If Not (CaracteresInicialesCod = "") Then
            
            If CoincidenciaTotal Then
            
                sConsulta = sConsulta & " AND CAL.COD ='" & DblQuote(CaracteresInicialesCod) & "'"
                
            Else
                    
                
                sConsulta = sConsulta & " AND CAL.COD LIKE '" & DblQuote(CaracteresInicialesCod) & "%'"
                
            End If
            
        Else
            
            If CoincidenciaTotal Then
            
                sConsulta = sConsulta & " AND DEN ='" & DblQuote(CaracteresInicialesDen) & "'"
                
            Else
           
                
                sConsulta = sConsulta & " AND DEN LIKE '" & DblQuote(CaracteresInicialesDen) & "%'"
                  
            End If
            
        End If
    
    End If
           
End If


If OrdenadosPorDen Then
    sConsulta = sConsulta & " ORDER BY DEN,CAL.COD"
Else
    If OrdenadosPorValorInf Then
        sConsulta = sConsulta & " ORDER BY INF,CAL.COD,DEN"
    Else
        If OrdenadosPorValorSup Then
            sConsulta = sConsulta & " ORDER BY SUP,CAL.COD,DEN"
        Else
            sConsulta = sConsulta & " ORDER BY CAL.COD,DEN"
        End If
    End If
End If

rs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
      
If rs.eof Then
        
    rs.Close
    Set rs = Nothing
    Exit Sub
      
Else
    
    Set fldCod = rs.Fields(0)
    Set fldDen = rs.Fields(5)
    Set fldInf = rs.Fields(1)
    Set fldSup = rs.Fields(2)
    Set fldIdioma = rs.Fields(6)
    
    Set mCol = Nothing
    Set mCol = New Collection
        
    If UsarIndice Then
        
        lIndice = 0
        
        While Not rs.eof
            If Not IsMissing(Idi) Then
                Me.Add fldCod.Value, IIf(IsNull(fldDen.Value), "", fldDen.Value), iNum, Idi, fldInf.Value, fldSup.Value, lIndice
            Else
                ' Dejamos en manos del programador cuando asignar la conexion al obejto usuario
                Me.Add fldCod.Value, IIf(IsNull(fldDen.Value), "", fldDen.Value), iNum, fldIdioma.Value, fldInf.Value, fldSup.Value, lIndice
            End If
            rs.MoveNext
            lIndice = lIndice + 1
        Wend
        
    Else
        
        While Not rs.eof
            If Not IsMissing(Idi) Then
                Me.Add fldCod.Value, IIf(IsNull(fldDen.Value), "", fldDen.Value), iNum, Idi, fldInf.Value, fldSup.Value
            Else
                ' Dejamos en manos del programador cuando asignar la conexion al obejto usuario
                Me.Add fldCod.Value, IIf(IsNull(fldDen.Value), "", fldDen.Value), iNum, fldIdioma.Value, fldInf.Value, fldSup.Value
            End If
            
            rs.MoveNext
        Wend
    End If
    
    Set fldCod = Nothing
    Set fldDen = Nothing
    Set fldInf = Nothing
    Set fldSup = Nothing
    Set fldIdioma = Nothing

    rs.Close
    Set rs = Nothing
    Exit Sub
      
End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CCalificaciones", "CargarTodasLasCalificaciones", ERR, Erl)
      Exit Sub
   End If
End Sub
Public Function Add(ByVal Cod As String, ByVal Den As String, ByVal Num As Integer, ByVal Idi As String, Optional ByVal ValInf As Double, Optional ByVal ValSup As Double, Optional ByVal varIndice As Variant) As CCalificacion
    'create a new object
    Dim objnewmember As CCalificacion
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New CCalificacion
   
    objnewmember.Cod = Cod
    objnewmember.Den = Den
    objnewmember.Num = Num
    objnewmember.ValorInf = ValInf
    objnewmember.ValorSup = ValSup
    objnewmember.Idioma = Idi
    Set objnewmember.Conexion = mvarConexion
    Set objnewmember.ConexionServer = mvarConexionServer
    
    If Not IsMissing(varIndice) And Not IsNull(varIndice) Then
        objnewmember.Indice = varIndice
        mCol.Add objnewmember, CStr(varIndice)
    Else
    'set the properties passed into the method
        mCol.Add objnewmember, CStr(Cod)
       'mCol.Add objnewmember, CStr(Cod) & "-" & CStr(Num) & "-" & Idi
    End If
    
    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CCalificaciones", "Add", ERR, Erl)
      Exit Function
   End If

End Function

Public Property Get Item(vntIndexKey As Variant) As CCalificacion
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Friend Property Set Conexion(ByVal con As CConexion)
Set mvarConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = mvarConexion
End Property
Friend Property Set ConexionServer(ByVal con As CConexionDeUseServer)
Set mvarConexionServer = con
End Property
Friend Property Get ConexionServer() As CConexionDeUseServer
Set ConexionServer = mvarConexionServer
End Property


Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
     Count = mCol.Count
End If


End Property


Public Sub Remove(vntIndexKey As Variant)
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    mCol.Remove vntIndexKey

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CCalificaciones", "Remove", ERR, Erl)
      Exit Sub
   End If

End Sub


Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
     Set NewEnum = mCol.[_NewEnum]
End Property


Private Sub Class_Initialize()
  
    Set mCol = New Collection
End Sub


Private Sub Class_Terminate()
    'destroys collection when this class is terminated
    Set mCol = Nothing
    Set mvarConexion = Nothing
    Set mvarConexionServer = Nothing
    
End Sub

Public Sub CargarTodasLasCalificacionesDesde(ByVal NumMaximo As Integer, Optional ByVal iNum As Integer, Optional ByVal CaracteresInicialesCod As String, Optional ByVal CaracteresInicialesDen As String, Optional ByVal OrdenadosPorDen As Boolean, Optional ByVal OrdenadosPorValorInf As Boolean = False, Optional ByVal OrdenadosPorValorSup As Boolean = False, Optional ByVal UsarIndice As Boolean, Optional ByVal Idi As String)
Dim rs As New adodb.Recordset
Dim fldCod As adodb.Field
Dim fldDen As adodb.Field
Dim fldIdioma As adodb.Field
Dim fldInf As adodb.Field
Dim fldSup As adodb.Field
Dim sConsulta As String
Dim lIndice As Long
''''''''''''''''''''''''''Dim iNumCalifs As Integer

'********* Precondicion **************************************
If mvarConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CDestinos.CargarTodosLosDestinos", "No se ha establecido la conexion"
    Exit Sub
End If
'*************************************************************


If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

sConsulta = "SELECT TOP " & NumMaximo & " CAL.*,CAL_IDIOMA.DEN,CAL_IDIOMA.IDIOMA FROM CAL WITH (NOLOCK) INNER JOIN CAL_IDIOMA WITH (NOLOCK)"
sConsulta = sConsulta & " ON CAL.NUM= CAL_IDIOMA.NUM AND CAL.COD= CAL_IDIOMA.COD"
sConsulta = sConsulta & " WHERE CAL.NUM=" & iNum

If CaracteresInicialesCod = "" And CaracteresInicialesDen = "" Then
    
   
Else
    If Not (CaracteresInicialesCod = "") And Not (CaracteresInicialesDen = "") Then
        
            sConsulta = sConsulta & " AND CAL.COD >='" & DblQuote(CaracteresInicialesCod) & "'"
            sConsulta = sConsulta & " AND DEN >='" & DblQuote(CaracteresInicialesDen) & "'"
                    
    Else
        
        If Not (CaracteresInicialesCod = "") Then
            
           
                sConsulta = sConsulta & " AND CAL.COD >='" & DblQuote(CaracteresInicialesCod) & "'"
            
        Else
                sConsulta = sConsulta & " AND DEN >='" & DblQuote(CaracteresInicialesDen) & "'"
            
        End If
    
    End If
           
End If

'Filtra por el idioma
If Not IsMissing(Idi) And Idi <> "" Then
    sConsulta = sConsulta & " AND IDIOMA = '" & Idi & "'"
End If

If OrdenadosPorDen Then
    sConsulta = sConsulta & " ORDER BY DEN,CAL.COD"
Else
    If OrdenadosPorValorInf Then
        sConsulta = sConsulta & " ORDER BY INF,CAL.COD,DEN"
    Else
        If OrdenadosPorValorSup Then
            sConsulta = sConsulta & " ORDER BY SUP,CAL.COD,DEN"
        Else
            sConsulta = sConsulta & " ORDER BY CAL.COD,DEN"
        End If
    End If
End If
   
rs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
      
If rs.eof Then
        
    rs.Close
    Set rs = Nothing
    Exit Sub
      
Else
    
    Set fldCod = rs.Fields(0)
    Set fldInf = rs.Fields(1)
    Set fldSup = rs.Fields(2)
    Set fldIdioma = rs.Fields(6)
    Set fldDen = rs.Fields(5)
    Set mCol = Nothing
    Set mCol = New Collection
        
    If UsarIndice Then
        
        lIndice = 0
        '''''''''''''''iNumCalifs = 0
                
        While Not rs.eof ''''''''''''''''''''And iNumCalifs < NumMaximo
            If Not IsMissing(Idi) Then
                Me.Add fldCod.Value, IIf(IsNull(fldDen.Value), "", fldDen.Value), iNum, Idi, basUtilidades.NullToDbl0(fldInf.Value), basUtilidades.NullToDbl0(fldSup.Value), lIndice
            Else
                ' Dejamos en manos del programador cuando asignar la conexion al obejto usuario
                Me.Add fldCod.Value, IIf(IsNull(fldDen.Value), "", fldDen.Value), iNum, fldIdioma.Value, basUtilidades.NullToDbl0(fldInf.Value), basUtilidades.NullToDbl0(fldSup.Value), lIndice
            End If
            
            rs.MoveNext
            lIndice = lIndice + 1
            '''''''''''''''''''''''''iNumCalifs = iNumCalifs + 1
        Wend
        
        If Not rs.eof Then
            mvarEOF = False
            rs.Close
        Else
            mvarEOF = True
        End If
        
        rs.Close
        Set rs = Nothing
            
    Else
        
        ''''''''''''''''''''''''''iNumCalifs = 0
        
        While Not rs.eof ''''''''''''''''''''''And iNumCalifs < NumMaximo
            ' Dejamos en manos del programador cuando asignar la conexion al obejto usuario
            If Not IsMissing(Idi) Then
                Me.Add fldCod.Value, IIf(IsNull(fldDen.Value), "", fldDen.Value), iNum, Idi, basUtilidades.NullToDbl0(fldInf.Value), basUtilidades.NullToDbl0(fldSup.Value)
            Else
                Me.Add fldCod.Value, IIf(IsNull(fldDen.Value), "", fldDen.Value), iNum, fldIdioma.Value, basUtilidades.NullToDbl0(fldInf.Value), basUtilidades.NullToDbl0(fldSup.Value)
            End If
            rs.MoveNext
            '''''''''''''''''''NumCalifs = iNumCalifs + 1
        Wend
        
        If Not rs.eof Then
            mvarEOF = False
        Else
            mvarEOF = True
        End If
        
    End If
    
    Set fldCod = Nothing
    Set fldInf = Nothing
    Set fldSup = Nothing
    Set fldIdioma = Nothing
    Set fldDen = Nothing

    rs.Close
    Set rs = Nothing
    Exit Sub
      
End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CCalificaciones", "CargarTodasLasCalificacionesDesde", ERR, Erl)
      Exit Sub
   End If
End Sub
Public Property Get eof() As Boolean
    eof = mvarEOF
End Property


