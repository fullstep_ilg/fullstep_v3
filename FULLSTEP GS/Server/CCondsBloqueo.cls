VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CCondsBloqueo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private m_Col As Collection
Private m_oConexion As CConexion
Private m_bEOF As Boolean

Public Property Get eof() As Boolean
    eof = m_bEOF
End Property
Friend Property Let eof(ByVal b As Boolean)
    m_bEOF = b
End Property

Public Property Get Item(vntIndexKey As Variant) As CCondBloqueo
On Error GoTo NoSeEncuentra:

    Set Item = m_Col(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Friend Property Set Conexion(ByVal con As CConexion)
Set m_oConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = m_oConexion
End Property

Public Property Get Count() As Long
    If m_Col Is Nothing Then
        Count = 0
    Else
         Count = m_Col.Count
    End If
End Property

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
     Set NewEnum = m_Col.[_NewEnum]
End Property

Public Function Add(ByVal lId As Long, ByVal sCod As String, ByVal iTipo As Integer, ByVal lIdWorkflow As Long, ByVal iTipo_Bloqueo As Integer, Optional ByVal sMensajeSpa As String, Optional ByVal sMensajeEng As String, Optional ByVal sMensajeGer As String, Optional ByVal dtFecAct As Variant, Optional ByVal sFormula As Variant, Optional ByVal vIndice As Variant, Optional ByVal sMensajeFra As String) As CCondBloqueo
    
    'create a new object
    Dim objnewmember As CCondBloqueo
    
    Set objnewmember = New CCondBloqueo
    With objnewmember
        Set .Conexion = m_oConexion
        
        .Id = lId
        .Cod = sCod
        .Tipo = iTipo
        .MensajeSpa = sMensajeSpa
        .MensajeEng = sMensajeEng
        .MensajeGer = sMensajeGer
        .Workflow = lIdWorkflow
        .TipoBloqueo = iTipo_Bloqueo
        .FecAct = dtFecAct
        .Formula = sFormula
        .MensajeFra = sMensajeFra
        
        If Not IsMissing(vIndice) And Not IsNull(vIndice) Then
            .Indice = vIndice
            m_Col.Add objnewmember, CStr(vIndice)
        Else
            m_Col.Add objnewmember, CStr(lId)
        End If
    
    End With
    
    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing


End Function

Public Sub AddCondicion(ByVal oCondicion As CCondBloqueo, Optional ByVal vIndice As Variant)
    If Not IsMissing(vIndice) And Not IsNull(vIndice) Then
        oCondicion.Indice = vIndice
        m_Col.Add oCondicion, CStr(vIndice)
    Else
        m_Col.Add oCondicion, CStr(oCondicion.Id)
    End If
End Sub

Public Sub Remove(vntIndexKey As Variant)
On Error GoTo Error

    m_Col.Remove vntIndexKey

Error:

End Sub


Private Sub Class_Initialize()
    Set m_Col = New Collection
End Sub


Private Sub Class_Terminate()
    'Proceroys collection when this class is terminated
    On Error Resume Next
    Set m_Col = Nothing
    Set m_oConexion = Nothing
End Sub

Public Function DevolverCondiciones(ByVal lIdWorkflow As Long, ByVal lTipoBloqueo As Long) As adodb.Recordset
    Dim rs As New adodb.Recordset
    Dim sConsulta As String
    Dim oGestorParametros As CGestorParametros
    Dim oIdiomas As CIdiomas

    'Carga todos los idiomas de la aplicación:
    Set oGestorParametros = New CGestorParametros
    Set oGestorParametros.Conexion = m_oConexion
    Set oIdiomas = oGestorParametros.DevolverIdiomas(False, False, True)

    sConsulta = "select ID,COD,TIPO, TIPO_BLOQUEO, WORKFLOW, FECACT, FORMULA,MENSAJE_SPA,MENSAJE_ENG,MENSAJE_GER,MENSAJE_FRA from PM_CONDICIONES_BLOQUEO WITH (NOLOCK) where WORKFLOW =" & lIdWorkflow & " AND TIPO_BLOQUEO = " & lTipoBloqueo
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockBatchOptimistic
          
    Set DevolverCondiciones = rs
        
    Set rs = Nothing
End Function

Public Sub CargarCondiciones(ByVal lIdWorkflow As Long, ByVal lTipoBloqueo As Long)
    Dim oRS As adodb.Recordset
        
    Set oRS = DevolverCondiciones(lIdWorkflow, lTipoBloqueo)
    While Not oRS.eof
        Add NullToDbl0(oRS("ID").Value), NullToStr(oRS("COD").Value), NullToDbl0(oRS("TIPO").Value), NullToDbl0(oRS("WORKFLOW").Value), NullToDbl0(oRS("TIPO_BLOQUEO").Value), NullToStr(oRS("MENSAJE_SPA").Value), NullToStr(oRS("MENSAJE_ENG").Value), NullToStr(oRS("MENSAJE_GER").Value), oRS("FECACT").Value, NullToStr(oRS("FORMULA").Value), , NullToStr(oRS("MENSAJE_FRA").Value)
        oRS.MoveNext
    Wend
    oRS.Close
    Set oRS = Nothing
End Sub

