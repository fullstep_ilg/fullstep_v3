VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CAdjudicacion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CAdjudicacion **********************************
'*             Autor : Javier Arana
'*             Creada : 4/5/99
'***************************************************************

Option Explicit

Private mvarProve As String
Private mvarNumOfe As Integer
Private mvarCantidad As Double
Private mvarPrecio As Variant
Private mvarPorcentaje As Double
Private mvarItemId As Integer
Private mvarIndice As Integer
Private mvarPresUnitario As Variant
Private mvarObjetivo As Variant
Private mvarObjetivoNuevo As Boolean
Private mvarGrupo As Variant
Private mvarEscalado As Variant
Private mvarCantidadEscalado As Variant
Private mvarPrecioEscalado As Variant
Private mvarAdjudicadoEscalado As Variant
Private mvarCat As Variant
Private mvarPed As Variant
Private mvarAdjudicado As Variant
Private m_dblCambio As Double

'Se usan para la comparativa:
Private mvarImporteAdj As Variant   'Importe adjudicado SIN atributos de total de �tem
Private mvarImporteAdjTot As Variant    'Importe adjudicado CON atributos de total de �tem
Private mvarImporte As Variant
Private mvarConsumidoItemProve As Variant 'Importe consumido para un �tem-proveedor en moneda de OFERTA o para un item-proveedor-escalado en moneda de proceso (este ultimo se calcula pero no se usa en otros calculos)

'9064
Private mvarPartida As Variant
Private mvarAnyoPartida As Variant

Public Property Let Cambio(ByVal dato As Double)
    m_dblCambio = dato
End Property
Public Property Get Cambio() As Double
    Cambio = m_dblCambio
End Property

Public Property Let Catalogo(ByVal dato As Variant)
    mvarCat = dato
End Property
Public Property Get Catalogo() As Variant
    Catalogo = mvarCat
End Property

Public Property Let Pedido(ByVal dato As Variant)
    mvarPed = dato
End Property
Public Property Get Pedido() As Variant
    Pedido = mvarPed
End Property

Public Property Let ImporteAdj(ByVal dato As Variant)
    mvarImporteAdj = dato
End Property
Public Property Get ImporteAdj() As Variant
    ImporteAdj = mvarImporteAdj
End Property

Public Property Let Grupo(ByVal dato As Variant)
    mvarGrupo = dato
End Property
Public Property Get Grupo() As Variant
    Grupo = mvarGrupo
End Property

Public Property Let Escalado(ByVal dato As Variant)
    mvarEscalado = dato
End Property
Public Property Get Escalado() As Variant
    Escalado = mvarEscalado
End Property

Public Property Let Indice(ByVal varIndice As Variant)
    mvarIndice = varIndice
End Property
Public Property Get Indice() As Variant
    Indice = mvarIndice
End Property
Public Property Let NumOfe(ByVal varNumOfe As Variant)
    mvarNumOfe = varNumOfe
End Property
Public Property Get NumOfe() As Variant
    NumOfe = mvarNumOfe
End Property

Public Property Let PresUnitario(ByVal varIndice As Variant)
    mvarPresUnitario = varIndice
End Property
Public Property Get PresUnitario() As Variant
    PresUnitario = mvarPresUnitario
End Property
Public Property Let Objetivo(ByVal varIndice As Variant)
    mvarObjetivo = varIndice
End Property
Public Property Get Objetivo() As Variant
    Objetivo = mvarObjetivo
End Property
Public Property Let ObjetivoNuevo(ByVal varIndice As Boolean)
    mvarObjetivoNuevo = varIndice
End Property
Public Property Get ObjetivoNuevo() As Boolean
    ObjetivoNuevo = mvarObjetivoNuevo
End Property

Public Property Let ProveCod(ByVal S As String)
    mvarProve = S
End Property
Public Property Get ProveCod() As String
    ProveCod = mvarProve
End Property

Public Property Let Id(ByVal i As Integer)
    mvarItemId = i
End Property
Public Property Get Id() As Integer
    Id = mvarItemId
End Property

Public Property Let Precio(ByVal d As Variant)
    mvarPrecio = d
End Property
Public Property Get Precio() As Variant
    Precio = mvarPrecio
End Property
Public Property Let Porcentaje(ByVal d As Double)
    mvarPorcentaje = d
End Property
Public Property Get Porcentaje() As Double
    Porcentaje = mvarPorcentaje
End Property
Public Property Let ConsumidoItemProve(ByVal dato As Variant)
    mvarConsumidoItemProve = dato
End Property
Public Property Get ConsumidoItemProve() As Variant
    ConsumidoItemProve = mvarConsumidoItemProve
End Property

Public Property Let Importe(ByVal dato As Variant)
    mvarImporte = dato
End Property
Public Property Get Importe() As Variant
    Importe = mvarImporte
End Property

Public Property Let ImporteAdjTot(ByVal dato As Variant)
    mvarImporteAdjTot = dato
End Property
Public Property Get ImporteAdjTot() As Variant
    ImporteAdjTot = mvarImporteAdjTot
End Property

Public Property Let Adjudicado(ByVal dato As Variant)
    mvarAdjudicado = dato
End Property
Public Property Get Adjudicado() As Variant
    Adjudicado = mvarAdjudicado
End Property

''' <summary>
''' Establece mvarCantidadEscalado (cantidad adjudicado si se utilizan escalados)
''' </summary>
''' <param name="d">cantidad</param>
''' <returns>Llamada desde: frmadjanyo/Ordencompra; Tiempo maximo:0</returns>
Public Property Let CantidadEsc(ByVal d As Variant)
    mvarCantidadEscalado = d
End Property
''' <summary>
''' Devuelve mvarCantidadEscalado (cantidad adjudicado si se utilizan escalados)
''' </summary>
''' <returns>Llamada desde: frmadjanyo/Ordencompra; Tiempo maximo:0</returns>
Public Property Get CantidadEsc() As Variant
    CantidadEsc = mvarCantidadEscalado
End Property
''' <summary>
''' Establece mvarPrecioEscalado (precio adjudicado si se utilizan escalados)
''' </summary>
''' <param name="d">precio</param>
''' <returns>Llamada desde: frmadjanyo/Ordencompra; Tiempo maximo:0</returns>
Public Property Let PrecioEsc(ByVal d As Variant)
    mvarPrecioEscalado = d
End Property
''' <summary>
''' Devuelve mvarPrecioEscalado (precio adjudicado si se utilizan escalados)
''' </summary>
''' <returns>Llamada desde: frmadjanyo/Ordencompra; Tiempo maximo:0</returns>
Public Property Get PrecioEsc() As Variant
    PrecioEsc = mvarPrecioEscalado
End Property
''' <summary>
''' Establece mvarAdjudicadoEscalado (importe adjudicado si se utilizan escalados)
''' </summary>
''' <param name="d">importe</param>
''' <returns>Llamada desde: frmadjanyo/Ordencompra; Tiempo maximo:0</returns>
Public Property Let AdjudicadoEsc(ByVal d As Variant)
    mvarAdjudicadoEscalado = d
End Property
''' <summary>
''' Devuelve mvarAdjudicadoEscalado (importe adjudicado si se utilizan escalados)
''' </summary>
''' <returns>Llamada desde: frmadjanyo/Ordencompra; Tiempo maximo:0</returns>
Public Property Get AdjudicadoEsc() As Variant
    AdjudicadoEsc = mvarAdjudicadoEscalado
End Property

Public Property Let Partida(ByVal d As Variant)
    mvarPartida = d
End Property
Public Property Get Partida() As Variant
    Partida = mvarPartida
End Property
Public Property Let AnyoPartida(ByVal d As Variant)
    mvarAnyoPartida = d
End Property
Public Property Get AnyoPartida() As Variant
    AnyoPartida = mvarAnyoPartida
End Property
