VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CCalificacion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CCalificacion **********************************
'*             Autor : Javier Arana
'*             Creada : 23/12/98
'****************************************************************

Option Explicit

Implements IBaseDatos

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

Private mvarRecordsetIdioma As adodb.Recordset

Private mvarRecordset As adodb.Recordset
Private mvarIndice As Variant 'Nos indica la posicion secuencial dentro de una Coleccion
Private mvarCod As String 'local copy
Private mvarDen As String 'local copy
Private mvarValorSup As Double
Private mvarValorInf As Double
Private mvarNum As Integer
Private mvarIdioma As Variant
Private mvarConexion As CConexion 'local copy
Private mvarConexionServer As CConexionDeUseServer  'local copy


Public Function ActualizarCalificaciones() As TipoErrorSummit

    ''' * Objetivo: Cambiar de codigo la calificaci�n
    ''' * Recibe: Codigo nuevo
    ''' * Devuelve: TipoErrorSummit con el error si lo hay
    Dim adocom As adodb.Command
    Dim oParam As adodb.Parameter
    Dim TESError As TipoErrorSummit
    

    TESError.NumError = TESnoerror
    
    ''' Precondicion
    
    If mvarConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CCalificaciones.ActualizarCalificaciones", "No se ha establecido la conexion"
    End If
    
    If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
        
If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
    
    Set adocom = New adodb.Command
    Set adocom.ActiveConnection = mvarConexion.ADOCon
    ''' Preparar la SP y sus parametros
    Set oParam = adocom.CreateParameter("COD", adVarChar, adParamInput, 50, mvarCod)
    adocom.Parameters.Append oParam
    adocom.CommandType = adCmdStoredProc
    adocom.CommandText = "ACT_CAL" & CStr(Me.Num)
        
    'sql = "{ CALL ACT_CAL" & Me.Num & " (?) }"
     
    adocom.Execute
    ''' Ejecutar la SP
    
    
    Set adocom = Nothing
       
    ActualizarCalificaciones = TESError
    
    Exit Function
    
Error_Cls:
    
    If Not adocom Is Nothing Then
        Set adocom = Nothing
    End If
    
    ActualizarCalificaciones = basErrores.TratarError(mvarConexion.ADOCon.Errors)

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CCalificacion", "ActualizarCalificaciones", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If
    
End Function


Public Property Get Indice() As Variant
    Indice = mvarIndice
End Property
Public Property Let Indice(ByVal varInd As Variant)
    mvarIndice = varInd
End Property

Public Property Get Num() As Integer
    Num = mvarNum
End Property
Public Property Let Num(ByVal varInd As Integer)
    mvarNum = varInd
End Property
Public Property Get ValorSup() As Double
    ValorSup = mvarValorSup
End Property
Public Property Let ValorSup(ByVal varInd As Double)
    mvarValorSup = varInd
End Property
Public Property Get ValorInf() As Double
    ValorInf = mvarValorInf
End Property
Public Property Let ValorInf(ByVal varInd As Double)
    mvarValorInf = varInd
End Property
Public Property Get Idioma() As Variant
    Idioma = mvarIdioma
End Property
Public Property Let Idioma(ByVal VarID As Variant)
    mvarIdioma = VarID
End Property


Friend Property Set Conexion(ByVal vData As CConexion)
    Set mvarConexion = vData
End Property


Friend Property Get Conexion() As CConexion
    Set Conexion = mvarConexion
End Property

Friend Property Set ConexionServer(ByVal con As CConexionDeUseServer)
Set mvarConexionServer = con
End Property
Friend Property Get ConexionServer() As CConexionDeUseServer
Set ConexionServer = mvarConexionServer
End Property


Public Property Let Den(ByVal vData As String)
    mvarDen = vData
End Property


Public Property Get Den() As String
    Den = mvarDen
End Property



Public Property Let Cod(ByVal vData As String)
    mvarCod = vData
End Property


Public Property Get Cod() As String
    Cod = mvarCod
End Property
Private Sub ConectarDeUseServer()

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If mvarConexionServer.ADOSummitCon Is Nothing Then
        Set mvarConexionServer.ADOSummitCon = New adodb.Connection
        mvarConexionServer.ADOSummitCon.Open "Provider=SQLOLEDB.1;Auto Translate=FALSE;Server=" & basParametros.ServidorGS & ";Database=" & basParametros.BaseDatosGS & ";", LICDBLogin, LICDBContra
        mvarConexionServer.ADOSummitCon.CursorLocation = adUseServer
        mvarConexionServer.ADOSummitCon.CommandTimeout = 120
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CCalificacion", "ConectarDeUseServer", ERR, Erl)
      Exit Sub
   End If
End Sub


Private Sub Class_Terminate()
    Set mvarConexion = Nothing
    Set mvarConexionServer = Nothing
End Sub

Private Function IBaseDatos_AnyadirABaseDatos() As TipoErrorSummit
Dim sConsulta As String
Dim TESError As TipoErrorSummit


'********* Precondicion *******************
If mvarConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CDestino.AnyadirABAseDatos", "No se ha establecido la conexion"
End If

'******************************************

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

    TESError.NumError = TESnoerror

If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
    mvarConexion.ADOCon.Execute "BEGIN TRANSACTION"
    'cal
    sConsulta = "INSERT INTO CAL (COD,INF,SUP,NUM) VALUES ('" & DblQuote(mvarCod) & "'," & DblToSQLFloat(mvarValorInf) & "," & DblToSQLFloat(mvarValorSup) & "," & mvarNum & ")"
    mvarConexion.ADOCon.Execute sConsulta
    If mvarConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    'cal_idioma
    sConsulta = "INSERT INTO CAL_IDIOMA (COD,NUM,IDIOMA,DEN) VALUES ('" & DblQuote(mvarCod) & "'," & mvarNum & ",'" & DblQuote(mvarIdioma) & "','" & DblQuote(mvarDen) & "')"
    mvarConexion.ADOCon.Execute sConsulta
    If mvarConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    mvarConexion.ADOCon.Execute "COMMIT TRANSACTION"
    
    IBaseDatos_AnyadirABaseDatos = TESError
    
    Exit Function
    
Error_Cls:

    IBaseDatos_AnyadirABaseDatos = basErrores.TratarError(mvarConexion.ADOCon.Errors)
    mvarConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CCalificacion", "IBaseDatos_AnyadirABaseDatos", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If
    
End Function

Private Function IBaseDatos_CambiarCodigo(CodigoNuevo As Variant) As TipoErrorSummit

    ''' * Objetivo: Cambiar de codigo la calificaci�n
    ''' * Recibe: Codigo nuevo
    ''' * Devuelve: TipoErrorSummit con el error si lo hay
    
    Dim adocom As adodb.Command
    Dim oParam As adodb.Parameter
    Dim TESError As TipoErrorSummit
    

    TESError.NumError = TESnoerror
    
    ''' Precondicion
    
    If mvarConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CCalificaci�n.CambiarCodigo", "No se ha establecido la conexion"
    End If
    
    If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
        
If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
    
    ''' Preparar la SP y sus parametros
    Set adocom = New adodb.Command
    Set adocom.ActiveConnection = mvarConexion.ADOCon
      
    Set oParam = adocom.CreateParameter("OLD", adVarChar, adParamInput, 50, Me.Cod)
    adocom.Parameters.Append oParam
    Set oParam = adocom.CreateParameter("NEW", adVarChar, adParamInput, 50, CodigoNuevo)
    adocom.Parameters.Append oParam
    Set oParam = adocom.CreateParameter("NUM", adInteger, adParamInput, , Me.Num)
    adocom.Parameters.Append oParam
 
    adocom.CommandType = adCmdStoredProc
    adocom.CommandText = "CAL_COD"
    
    adocom.Execute
 
    Set adocom = Nothing   'si no devuelve
        
    mvarCod = CodigoNuevo
        
    IBaseDatos_CambiarCodigo = TESError
    
    Exit Function
    
Error_Cls:
    
    IBaseDatos_CambiarCodigo = basErrores.TratarError(mvarConexion.ADOCon.Errors)
    If Not adocom Is Nothing Then
        Set adocom = Nothing
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CCalificacion", "IBaseDatos_CambiarCodigo", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If
    
End Function

Private Sub IBaseDatos_CancelarEdicion()

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If Not mvarRecordset Is Nothing Then
    mvarRecordset.Close
    Set mvarRecordset = Nothing
End If

If Not mvarRecordsetIdioma Is Nothing Then
    mvarRecordsetIdioma.Close
    Set mvarRecordset = Nothing
End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CCalificacion", "IBaseDatos_CancelarEdicion", ERR, Erl)
      Exit Sub
   End If

End Sub

''' <summary>Comprueba la existencia de una calificaci�n</summary>
''' <returns>Booleano indicando la existencia de la calificaci�n</returns>
''' <remarks>Llamada desde: GSClient</remarks>
''' <revision>LTG 02/01/2012</revision>

Private Function IBaseDatos_ComprobarExistenciaEnBaseDatos() As Boolean
    Dim rs As New adodb.Recordset
    
    '******************* Precondicion *******************
    If mvarConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CDestino.ComprobarExistenciaEnBaseDatos", "No se ha establecido la conexion"
    End If
    '*****************************************************
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
    rs.Open "SELECT COD FROM CAL WITH (NOLOCK) WHERE COD='" & DblQuote(mvarCod) & "' AND NUM = " & mvarNum, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    
    If rs.eof Then
        rs.Close
        Set rs = Nothing
        IBaseDatos_ComprobarExistenciaEnBaseDatos = False
    Else
        rs.Close
        Set rs = Nothing
        IBaseDatos_ComprobarExistenciaEnBaseDatos = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CCalificacion", "IBaseDatos_ComprobarExistenciaEnBaseDatos", ERR, Erl)
      Exit Function
   End If
End Function

Private Function IBaseDatos_EliminarDeBaseDatos() As TipoErrorSummit

Dim TESError As TipoErrorSummit

'******************* Precondicion *******************
If mvarConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CDestino.EliminarDeBaseDatos", "No se ha establecido la conexion"
End If
'*****************************************************
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

    TESError.NumError = TESnoerror

If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
    mvarConexion.ADOCon.Execute "BEGIN TRANSACTION"
    'Cal_idioma
    mvarConexion.ADOCon.Execute "DELETE FROM CAL_IDIOMA WHERE COD='" & DblQuote(mvarCod) & "' AND NUM =" & mvarNum
    If mvarConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    'Cal
    mvarConexion.ADOCon.Execute "DELETE FROM CAL WHERE COD='" & DblQuote(mvarCod) & "' AND NUM =" & mvarNum
    If mvarConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    mvarConexion.ADOCon.Execute "COMMIT TRANSACTION"
    IBaseDatos_EliminarDeBaseDatos = TESError
    Exit Function
    
Error_Cls:
    
    IBaseDatos_EliminarDeBaseDatos = basErrores.TratarError(mvarConexion.ADOCon.Errors)
    mvarConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CCalificacion", "IBaseDatos_EliminarDeBaseDatos", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If

End Function

Private Function IBaseDatos_FinalizarEdicionEliminando() As TipoErrorSummit
Dim TESError As TipoErrorSummit
'PArece que no se usa (epb 09/2001)
'******************* Precondicion *******************
If mvarConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CDestino.EliminarDeBaseDatos", "No se ha establecido la conexion"
End If
'*****************************************************

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
    
    mvarRecordset.Delete
    mvarRecordset.Close
    Set mvarRecordset = Nothing
    TESError.NumError = TESnoerror
    IBaseDatos_FinalizarEdicionEliminando = TESError
    Exit Function
    
Error_Cls:
    
    IBaseDatos_FinalizarEdicionEliminando = basErrores.TratarError(mvarConexionServer.ADOSummitCon.Errors)
    If mvarRecordset.EditMode <> adEditNone Then
        mvarRecordset.CancelUpdate
    End If
    mvarRecordset.Close
    Set mvarRecordset = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CCalificacion", "IBaseDatos_FinalizarEdicionEliminando", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If
    
End Function

Private Function IBaseDatos_FinalizarEdicionModificando() As TipoErrorSummit

Dim TESError As TipoErrorSummit
Dim btrans As Boolean
Dim sConsulta As String

'********* Precondicion *******************
If mvarConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CDestino.FinalizarEdicionModificando", "No se ha establecido la conexion"
End If
'******************************************

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
    
    
    mvarConexion.ADOCon.Execute "BEGIN TRANSACTION"
    btrans = True
    
    'Actualiza la tabla CAL
    mvarRecordset("INF") = mvarValorInf
    mvarRecordset("SUP") = mvarValorSup
    mvarRecordset.Update
    
    'Actualiza la tabla CAL_IDIOMA
    If mvarRecordsetIdioma.eof Then
        sConsulta = "INSERT INTO CAL_IDIOMA (COD,NUM,IDIOMA,DEN) VALUES ('" & DblQuote(mvarCod) & "'," & mvarNum & ",'" & DblQuote(mvarIdioma) & "','" & DblQuote(mvarDen) & "')"
        mvarConexion.ADOCon.Execute sConsulta
        If mvarConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
        TESError.NumError = TESnoerror
    Else
        mvarRecordsetIdioma("DEN") = mvarDen
        mvarRecordsetIdioma.Update
    End If
    
    ActualizarCalificaciones
    
    mvarConexion.ADOCon.Execute "COMMIT TRANSACTION"
    btrans = False
    
    mvarRecordset.Close
    Set mvarRecordset = Nothing
    
    mvarRecordsetIdioma.Close
    Set mvarRecordsetIdioma = Nothing
    
    TESError.NumError = TESnoerror
    IBaseDatos_FinalizarEdicionModificando = TESError
    
    Exit Function
    

Error_Cls:
    If mvarConexion.ADOCon.Errors.Count > 0 Then
        TESError = basErrores.TratarError(mvarConexion.ADOCon.Errors)
    Else
        TESError = basErrores.TratarError(mvarConexionServer.ADOSummitCon.Errors)
    End If

    IBaseDatos_FinalizarEdicionModificando = TESError
    
    If btrans Then
        mvarConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
    End If

    Resume Salir:
    
Salir:

    On Error Resume Next
    If mvarRecordset.EditMode > 0 Then
        mvarRecordset.CancelUpdate
    End If
    
    If mvarRecordsetIdioma.EditMode > 0 Then
        mvarRecordsetIdioma.CancelUpdate
    End If
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CCalificacion", "IBaseDatos_FinalizarEdicionModificando", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If
  
End Function

''' <summary>Inicia la edici�n de un registro</summary>
''' <param name="Bloquear">No se usa</param>
''' <param name="UsuarioBloqueo">No se usa</param>
''' <returns>Variable tipo TipoErrorSummit con el error si se ha producido</returns>
''' <remarks>Llamada desde: GSClient</remarks>
''' <revision>LTG 12/01/2012</revision>

Private Function IBaseDatos_IniciarEdicion(Optional ByVal Bloquear As Boolean, Optional ByVal UsuarioBloqueo As String) As TipoErrorSummit
    Dim rs As New adodb.Recordset
    Dim fldCod As adodb.Field
    Dim fldInf As adodb.Field
    Dim fldSup As adodb.Field
    Dim fldDen As adodb.Field
    Dim TESError As TipoErrorSummit
    Dim sSQL As String
    
    '******************* Precondicion *******************
    If mvarConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CDestino.IniciarEdicion", "No se ha establecido la conexion"
    End If
    '*****************************************************
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
    ConectarDeUseServer
    Set mvarRecordset = New adodb.Recordset
    'mvarRecordset.CursorLocation = adUseServer
    'CALIDAD: Sin WITH (NOLOCK) porque se rellena el recordset mvarRecordset que posteriormente se utiliza para las actualizaciones
    'en otros m�todos de la clase. Si se hace la lectura con WITH (NOLOCK) no se permite la llamada al m�todo Update del recordset.
    mvarRecordset.Open "SELECT COD,INF,SUP,NUM,FECACT FROM CAL WHERE COD='" & DblQuote(mvarCod) & "' AND NUM =" & mvarNum, mvarConexionServer.ADOSummitCon, adOpenKeyset, adLockOptimistic
    
    If mvarRecordset.eof Then
        mvarRecordset.Close
        Set mvarRecordset = Nothing
        TESError.NumError = TESDatoEliminado
        TESError.Arg1 = 91 '"calificacion"
        IBaseDatos_IniciarEdicion = TESError
        Exit Function
    End If
    
    Set fldCod = mvarRecordset.Fields(0)
    Set fldInf = mvarRecordset.Fields(1)
    Set fldSup = mvarRecordset.Fields(2)
    
    mvarCod = fldCod.Value
    mvarValorInf = fldInf.Value
    mvarValorSup = fldSup.Value
    
    'Obtiene el idioma
    'sSQL = "SELECT DEN FROM CAL_IDIOMA WHERE COD='" & DblQuote(mvarCod) & "' AND NUM=" & mvarNum & " AND IDIOMA = '" & mvarIdioma & "'"
    'CALIDAD: Sin WITH (NOLOCK) porque se rellena el recordset mvarRecordset que posteriormente se utiliza para las actualizaciones
    'en otros m�todos de la clase. Si se hace la lectura con WITH (NOLOCK) no se permite la llamada al m�todo Update del recordset.
    sSQL = "SELECT NUM,COD,IDIOMA,DEN FROM CAL_IDIOMA WHERE COD='" & DblQuote(mvarCod) & "' AND NUM=" & mvarNum & " AND IDIOMA = '" & mvarIdioma & "'"
    Set mvarRecordsetIdioma = New adodb.Recordset
    mvarRecordsetIdioma.Open sSQL, mvarConexionServer.ADOSummitCon, adOpenKeyset, adLockOptimistic
    If mvarRecordsetIdioma.eof Then
        'mvarResulsetIdioma.Close
        'Set mvarResulsetIdioma = Nothing
        'TESError.NumError = TESDatoEliminado
        'TESError.Arg1 = "calificacion"
    Else
    
        Set fldDen = mvarRecordset.Fields(3)
        mvarDen = fldDen.Value
    
    End If
    
    Set fldCod = Nothing
    Set fldInf = Nothing
    Set fldSup = Nothing
    Set fldDen = Nothing
    
    
    TESError.NumError = TESnoerror
    IBaseDatos_IniciarEdicion = TESError
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CCalificacion", "IBaseDatos_IniciarEdicion", ERR, Erl)
      Exit Function
   End If

End Function

Private Function IBaseDatos_ModificarEnBaseDatos() As Boolean

End Function



