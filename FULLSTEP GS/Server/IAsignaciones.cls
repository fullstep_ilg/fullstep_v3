VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "IAsignaciones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Public Function AsignarResponsable(ByVal CodEqp As Variant, ByVal CodComp As Variant, Optional ByVal bAsignar As Boolean) As TipoErrorSummit

End Function
Public Function DevolverResponsable() As CComprador
End Function
Public Function DevolverCompradoresAsignados(Optional ByVal CriterioOrdenacion As TipoOrdenacionPersonas) As CCompradores

End Function
Public Function DevolverAsignaciones(Optional ByVal CriterioOrdenacion As TipoOrdenacionAsignaciones, Optional ByVal CodEqp As Variant, _
                                     Optional ByVal CodComp As Variant, Optional ByVal CodProve As Variant, Optional ByVal UsarIndice As Boolean, _
                                     Optional ByVal bCargarGrupos As Boolean, Optional ByVal bBloquear As Boolean = False) As CAsignaciones

End Function
Public Function DevolverPosiblesAsignaciones(Optional ByVal CriterioOrdenacion As TipoOrdenacionAsignaciones, Optional ByVal CodEqp As Variant, Optional ByVal CodComp As Variant, Optional ByVal UsarIndice As Boolean, Optional ByVal bRComp As Boolean, Optional ByVal bREqp As Boolean, Optional ByVal codUsuario As String) As CAsignaciones

End Function

Public Function DevolverPosiblesAsignacionesRs(Optional ByVal CriterioOrdenacion As TipoOrdenacionAsignaciones, Optional ByVal CodEqp As Variant, Optional ByVal CodComp As Variant, Optional ByVal UsarIndice As Boolean, Optional ByVal bRComp As Boolean, Optional ByVal bREqp As Boolean, Optional ByVal codUsuario As String) As adodb.Recordset

End Function

Public Function DevolverEquiposAsignables(ByVal CodProve As String, ByVal CarIniCod As String, Optional ByVal CoincidenciaTotal As Boolean, Optional ByVal UsarIndice As Boolean) As CEquipos

End Function
Public Function DevolverCompDeEqpAsignables(ByVal CodProve As String, ByVal CodEqp As String, ByVal CarIniCod As String, Optional ByVal CoincidenciaTotal As Boolean, Optional ByVal UsarIndice As Boolean) As CCompradores

End Function
''' <summary>
''' Cargar proveedores
''' </summary>
''' <param name="CarIniCod">caracteres iniciales para el codigo proveedor</param>
''' <param name="CarIniDen">caracteres iniciales para la denominacion</param>
''' <param name="CoincidenciaTotal">si se usa like o igual</param>
''' <param name="CriterioOrdenacion">por que se ordena</param>
''' <param name="sCodEqp">equipo</param>
''' <param name="sCodCom">comprador</param>
''' <param name="bUsarIndice">si se usa indices o no</param>
''' <param name="sCodPais">pais</param>
''' <param name="sCodProvi">provincia</param>
''' <param name="sCalif1">calificacion 1</param>
''' <param name="sCalif2">calificacion 2</param>
''' <param name="sCalif3">calificacion 3</param>
''' <param name="bSoloPremium">si solo premium o no</param>
''' <param name="bSoloActivos">si solo activos o no</param>
''' <param name="bSubasta">si solo de procesos de subasta o no</param>
''' <param name="iAutoFactura">si solo se permite aquellos con autofacturaci�n (1) o sin autofacturacion (2) o todos (0)</param>
''' <param optional name="sVAT">NIF</param>
''' <param optional name="iBusquedaAvanzada">B�aqueda avanzada (proveedores relacionados) -> iBusquedaAvanzada = 0 => Todos los proveedores  /  iBusquedaAvanzada = 1 => Proveedores Primarios  /  iBusquedaAvanzada = 2 => Proveedores Subordinados</param>
''' <param optional name="sTipoRel">Tipo de relaci�n</param>
''' <param optional name="bConTipoRel">Con Tipo de relaci�n (s/n)</param>
''' <param optional name="sCodProvePriSub">Cod proveedor relacionado (principal o subordinado)</param>
''' <param optional name="sCodProve">C�digo proveedor seleccionado (para evitar que nos aparezca en la lista de proveedores a seleccionar como relacionados)</param>
''' <param optional name="sCodERP">C�digo del proveedor en el ERP</param>
''' <param optional name="iEmpresa">Id de empresa del usuario</param>
''' <remarks>Llamada desde: CProceso ; Tiempo m�ximo: 0</remarks>
Public Function DevolverProveedoresDesde(Optional ByVal CarIniCod As String, Optional ByVal CarIniDen As String, Optional ByVal CoincidenciaTotal As Boolean, Optional ByVal CriterioOrdenacion As TipoOrdenacionAsignaciones, Optional ByVal CodEqp As Variant, Optional ByVal CodComp As Variant, Optional ByVal UsarIndice As Boolean, Optional ByVal sCodPais As String, Optional ByVal sCodProvi As String, Optional ByVal sCalif1 As String, Optional ByVal sCalif2 As String, Optional ByVal sCalif3 As String, Optional ByVal bSoloPremium As Boolean, Optional ByVal bSoloActivos As Boolean, Optional ByVal bSubasta As Boolean, Optional ByVal iAutoFactura As Integer = 0, _
                                        Optional ByVal sVAT As String, Optional ByVal iBusquedaAvanzada As Integer, _
                                        Optional ByVal sTipoRel As String, Optional ByVal bConTipoRel As Boolean, Optional ByVal sCodProvePriSub As String, _
                                        Optional ByVal sCodProve As String, Optional ByVal sCodERP As String, Optional ByVal iEmpresa As Integer = 0) As CProveedores

End Function

Public Function DevolverProveedoresComparablesDesde(ByVal CarIniCod As String, ByVal CarIniDen As String, Optional ByVal CoincidenciaTotal As Boolean, Optional ByVal CriterioOrdenacion As TipoOrdenacionAsignaciones, Optional ByVal CodEqp As Variant, Optional ByVal CodComp As Variant, Optional ByVal UsarIndice As Boolean) As CProveedores

End Function
Public Function DevolverAsignacionesComparablesDesde(ByVal CarIniCod As String, ByVal CarIniDen As String, Optional ByVal CoincidenciaTotal As Boolean, Optional ByVal CriterioOrdenacion As TipoOrdenacionAsignaciones, Optional ByVal CodEqp As Variant, Optional ByVal CodComp As Variant, Optional ByVal UsarIndice As Boolean) As CAsignaciones

End Function

Public Function DevolverProveedoresAdjudicablesDesde(ByVal CarIniCod As String, ByVal CarIniDen As String, Optional ByVal CoincidenciaTotal As Boolean, Optional ByVal CriterioOrdenacion As TipoOrdenacionAsignaciones, Optional ByVal CodEqp As Variant, Optional ByVal CodComp As Variant, Optional ByVal UsarIndice As Boolean) As CProveedores

End Function

Public Function IniciarAsignacion(ByVal UsuarioBloqueo As String) As TipoErrorSummit

End Function
Public Function CancelarAsignacion() As TipoErrorSummit

End Function
Public Function FinalizarAsignacion() As TipoErrorSummit

End Function
Public Function Asignar(ByVal oAsigsSelec As CAsignaciones, ByVal oAsigsDesSelec As CAsignaciones, Optional ByVal CodEqp As Variant, Optional ByVal CodComp As Variant, Optional ByVal OtroProve As Boolean) As TipoErrorSummit

End Function
Public Function Validar(ByVal CodUsu As String) As TipoErrorSummit

End Function
Public Function AsignarOtroProve(ByVal oAsigsSelec As CAsignaciones, Optional ByVal CodEqp As Variant, Optional ByVal CodComp As Variant) As TipoErrorSummit

End Function

Public Function DevolverProveedoresOFEBuzon(Optional ByVal CarIniCod As String, Optional ByVal CarIniDen As String, Optional ByVal CoincidenciaTotal As Boolean, Optional ByVal CriterioOrdenacion As TipoOrdenacionAsignaciones, Optional ByVal CodEqp As Variant, Optional ByVal CodComp As Variant, Optional ByVal UsarIndice As Boolean, Optional ByVal sCodPais As String, Optional ByVal sCodProvi As String, Optional ByVal sCalif1 As String, Optional ByVal sCalif2 As String, Optional ByVal sCalif3 As String, Optional ByVal bSoloPremium As Boolean, Optional ByVal bSoloActivos As Boolean, Optional ByVal bROfeTodas As Boolean, Optional ByVal bROfeEqp As Boolean, Optional ByVal bRestProvMatComp As Boolean, Optional ByVal bRestProvEquComp As Boolean) As CProveedores

End Function
Public Function Eliminar(ByVal oAsigsSelec As CAsignaciones) As TipoErrorSummit

End Function


