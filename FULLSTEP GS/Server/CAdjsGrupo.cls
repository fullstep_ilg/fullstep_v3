VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CAdjsGrupo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CAdjsGrupo **********************************
'*             Autor : Mertxe Mart�n
'*             Creada : 06/03/2002
'****************************************************************


Option Explicit

Private mCol As Collection

Private m_oConexion As CConexion
Private m_oConexionServer As CConexionDeUseServer
Private m_bEOF As Boolean

Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum

Friend Property Set Conexion(ByVal con As CConexion)
    Set m_oConexion = con
End Property

Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property

Friend Property Set ConexionServer(ByVal con As CConexionDeUseServer)
    Set m_oConexionServer = con
End Property

Friend Property Get ConexionServer() As CConexionDeUseServer
    Set ConexionServer = m_oConexionServer
End Property

Public Property Get Item(vntIndexKey As Variant) As CAdjGrupo
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property


Public Property Get Count() As Long
    If mCol Is Nothing Then
        Count = 0
    Else
         Count = mCol.Count
    End If
End Property

Public Property Get eof() As Boolean
    eof = m_bEOF
End Property

Friend Property Let eof(ByVal b As Boolean)
    m_bEOF = b
End Property

''' <summary>A�ade una adjudicaci�n a la colecci�n</summary>
''' <param name="oProce">Objeto proceso</param>
''' <param name="CodGrupo">C�digo del grupo</param>
''' <param name="Prove">Cod. proveedor</param>
''' <param name="NumOfe">Num. oferta</param>
''' <param name="dblAdjudicado">dblAdjudicado</param>
''' <param name="dblAhorrado">dblAhorrado</param>
''' <param name="dblAhorroOfe">dblAhorroOfe</param>
''' <param name="dblConsumido">dblConsumido</param>
''' <param name="dblImporte">dblImporte</param>
''' <param name="Abierto">Abierto</param>
''' <param name="AhorradoPorcen">AhorradoPorcen</param>
''' <param name="AhorroOfePorcen">AhorroOfePorcen</param>
''' <param name="Estado">Estado</param>
''' <param name="Indice">Indice</param>
''' <param name="Cambio">Cambio</param>
''' <param name="lGrupoID">Id del grupo</param>
''' <param name="dblAdjudicadoMonProce">dblAdjudicadoMonProce</param>
''' <param name="dblAhorradoMonProce">dblAhorradoMonProce</param>
''' <param name="dblAhorroOfeMonProce">dblAhorroOfeMonProce</param>
''' <param name="dblConsumidoMonProce">dblConsumidoMonProce</param>
''' <param name="dblImporteMonProce">dblImporteMonProce</param>
''' <param name="dblAbiertoMonProce">dblAbiertoMonProce</param>
''' <returns>Adjudicaci�n a�adida<returns>
''' <remarks>Llamada desde: CargarAdjudicaciones</remarks>

Public Function Add(ByVal oProce As cProceso, ByVal CodGrupo As String, ByVal Prove As String, ByVal NumOfe As Integer, ByVal dblAdjudicado As Double, ByVal dblAhorrado As Double, ByVal dblAhorroOfe As Double, _
        Optional ByVal dblConsumido As Double, Optional ByVal dblImporte As Double, Optional ByVal Abierto As Double, Optional ByVal AhorradoPorcen As Double, Optional ByVal AhorroOfePorcen As Double, _
        Optional ByVal Estado As String, Optional ByVal Indice As Variant, Optional ByVal Cambio As Double, Optional ByVal lGrupoID As Long, Optional ByVal dblAdjudicadoMonProce As Double, _
        Optional ByVal dblAhorradoMonProce As Double, Optional ByVal dblAhorroOfeMonProce As Double, Optional ByVal dblConsumidoMonProce As Double, Optional ByVal dblImporteMonProce As Double, _
        Optional ByVal dblAbiertoMonProce As Double) As CAdjGrupo
    'create a new object
    Dim sCod As String
    Dim objnewmember As CAdjGrupo

    If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New CAdjGrupo
    
    Set objnewmember.Proceso = oProce
    objnewmember.Grupo = CodGrupo
    objnewmember.GrupoID = lGrupoID
    
    objnewmember.Prove = Prove
    objnewmember.NumOfe = NumOfe

    'Importes en moneda de oferta
    objnewmember.Adjudicado = dblAdjudicado
    objnewmember.Ahorrado = dblAhorrado
    objnewmember.AhorroOfe = dblAhorroOfe
    objnewmember.Consumido = dblConsumido
    objnewmember.Importe = dblImporte
    objnewmember.Abierto = Abierto
    
    'Importes en moneda de proceso
    objnewmember.AdjudicadoMonProce = dblAdjudicadoMonProce
    objnewmember.AhorradoMonProce = dblAhorradoMonProce
    objnewmember.ConsumidoMonProce = dblConsumidoMonProce
    objnewmember.AhorroOfeMonProce = dblAhorroOfeMonProce
    objnewmember.AbiertoMonProce = dblAbiertoMonProce
    objnewmember.ImporteMonProce = dblImporteMonProce
    
    objnewmember.AhorradoPorcen = AhorradoPorcen
    objnewmember.AhorroOfePorcen = AhorroOfePorcen

    objnewmember.Estado = Estado
    If Not IsMissing(Cambio) Then
        objnewmember.Cambio = Cambio
    Else
        objnewmember.Cambio = 1
    End If
    
    If Not IsMissing(Indice) And Not IsNull(Indice) Then
        objnewmember.Indice = Indice
        mCol.Add objnewmember, CStr(Indice)
    Else
        sCod = Prove & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(Prove))
        mCol.Add objnewmember, CStr(CodGrupo) & sCod
    End If

    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing

    '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CAdjsGrupo", "Add", ERR, Erl)
      Exit Function
   End If
End Function

Public Sub Remove(vntIndexKey As Variant)
    
        mCol.Remove vntIndexKey
    
End Sub


Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
     Set NewEnum = mCol.[_NewEnum]
End Property


Private Sub Class_Initialize()
   
    Set mCol = New Collection
End Sub


Private Sub Class_Terminate()
    'Itemroys collection when this class is terminated
    Set mCol = Nothing
    
End Sub

Public Function BorrarEnModoIndice(ByVal iIndice As Integer)
Dim IndFor As Integer
Dim oItem As CItem

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    For IndFor = iIndice To mCol.Count - 2
            
        mCol.Remove (CStr(IndFor))
        Set oItem = mCol.Item(CStr(IndFor + 1))
        mCol.Add oItem, CStr(IndFor)
        Set oItem = Nothing
        
    Next IndFor
    
    mCol.Remove CStr(IndFor)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CAdjsGrupo", "BorrarEnModoIndice", ERR, Erl)
      Exit Function
   End If
    
End Function

''' <summary>Carga las adjudicaciones</summary>
''' <param "oProce">Objeto proceso</param>
''' <param "UsarIndice">Indica si hay que usar �ndice</param>
''' <param "Idi">Idioma</param>
''' <param "vFecReu">Fecha reuni�n</param>
''' <remarks>Llamada desde: frmProce.CargarProvActual </remarks>
''' <remarks>Tiempo m�ximo: < 1 sec </remarks>
''' <revision>LTG 22/12/2011</revision>

Public Sub CargarAdjudicaciones(ByVal oProce As cProceso, Optional ByVal UsarIndice As Boolean, Optional ByVal Idi As String, Optional ByVal vFecReu As Variant)
    Dim AdoRes As adodb.Recordset
    Dim lIndice As Long
    Dim fldConsumido As adodb.Field
    Dim fldAdjudicado As adodb.Field
    Dim fldAhorrado As adodb.Field
    Dim fldImporte As adodb.Field
    Dim fldAhorroOfe As adodb.Field
    Dim fldProve As adodb.Field
    Dim fldNumOfe As adodb.Field
    Dim fldAbierto As adodb.Field
    Dim fldAhorroOfePorcen As adodb.Field
    Dim fldAhorradoPorcen As adodb.Field
    Dim fldEstado As adodb.Field
    Dim fldCambio As adodb.Field
    Dim dblAbierto As Double
    Dim oCom As adodb.Command
    Dim oParam As adodb.Parameter
             
    '********* Precondicion **************************************
    If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
    Set oCom = New adodb.Command
    With oCom
        Set .ActiveConnection = m_oConexion.ADOCon
        .CommandType = adCmdStoredProc
        .CommandText = "FSGS_DEVOLVER_ADJUDICACIONES_GRUPO"
        
        Set oParam = .CreateParameter("@ANYO", adSmallInt, adParamInput, , oProce.Anyo)
        .Parameters.Append oParam
        Set oParam = .CreateParameter("@GMN1", adVarChar, adParamInput, 50, oProce.GMN1Cod)
        .Parameters.Append oParam
        Set oParam = .CreateParameter("@PROCE", adInteger, adParamInput, , oProce.Cod)
        .Parameters.Append oParam
        Set oParam = .CreateParameter("@IDIOMA", adVarChar, adParamInput, 20, Idi)
        .Parameters.Append oParam
        If Not IsMissing(vFecReu) Then
            Set oParam = .CreateParameter("@FECREU", adDate, adParamInput, , vFecReu)
            .Parameters.Append oParam
        End If
        
        Set AdoRes = .Execute
    End With
        
    Set mCol = Nothing
    Set mCol = New Collection
        
    If Not AdoRes.eof Then
        Set fldProve = AdoRes.Fields("PROVE")
        Set fldNumOfe = AdoRes.Fields("OFE")
        Set fldConsumido = AdoRes.Fields("CONSUMIDO")
        Set fldAdjudicado = AdoRes.Fields("ADJUDICADO")
        Set fldAhorrado = AdoRes.Fields("AHORRADO")
        Set fldImporte = AdoRes.Fields("IMPORTE")
        Set fldAhorroOfe = AdoRes.Fields("AHORRO_OFE")
        Set fldAbierto = AdoRes.Fields("ABIERTO")
        Set fldAhorroOfePorcen = AdoRes.Fields("AHORRO_OFE_PORCEN")
        Set fldAhorradoPorcen = AdoRes.Fields("AHORRADO_PORCEN")
        Set fldEstado = AdoRes.Fields("EST")
        Set fldCambio = AdoRes.Fields("CAMBIO")

        If UsarIndice Then
            lIndice = 0
            
            While Not AdoRes.eof
                Me.Add oProce, AdoRes.Fields("GRUPOCOD").Value, fldProve.Value, fldNumOfe.Value, NullToDbl0(fldAdjudicado.Value), NullToDbl0(fldAhorrado.Value), NullToDbl0(fldAhorroOfe.Value), NullToDbl0(fldConsumido.Value), _
                    NullToDbl0(fldImporte.Value), NullToDbl0(fldAbierto.Value), NullToDbl0(fldAhorradoPorcen.Value), NullToDbl0(fldAhorroOfePorcen.Value), fldEstado.Value, lIndice, fldCambio.Value, _
                    AdoRes.Fields("GRUPO").Value
                AdoRes.MoveNext
                lIndice = lIndice + 1
            Wend
        Else
            While Not AdoRes.eof
                Me.Add oProce, AdoRes.Fields("GRUPOCOD").Value, fldProve.Value, fldNumOfe.Value, NullToDbl0(fldAdjudicado.Value), NullToDbl0(fldAhorrado.Value), NullToDbl0(fldAhorroOfe.Value), NullToDbl0(fldConsumido.Value), _
                    NullToDbl0(fldImporte.Value), NullToDbl0(fldAbierto.Value), NullToDbl0(fldAhorradoPorcen.Value), NullToDbl0(fldAhorroOfePorcen.Value), fldEstado.Value, , fldCambio.Value, AdoRes.Fields("GRUPO").Value
                AdoRes.MoveNext
            Wend
        End If
        
        'Cierra todos los objetos
        Set fldProve = Nothing
        Set fldNumOfe = Nothing
        Set fldConsumido = Nothing
        Set fldAdjudicado = Nothing
        Set fldAhorrado = Nothing
        Set fldImporte = Nothing
        Set fldAhorroOfe = Nothing
        Set fldAbierto = Nothing
        Set fldAhorroOfePorcen = Nothing
        Set fldAhorradoPorcen = Nothing
        Set fldEstado = Nothing
        Set fldCambio = Nothing
    End If
    
    AdoRes.Close
    Set AdoRes = Nothing
    Set oCom = Nothing
    Set oParam = Nothing
    '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    
    Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CAdjsGrupo", "CargarAdjudicaciones", ERR, Erl)
      Exit Sub
   End If
End Sub
