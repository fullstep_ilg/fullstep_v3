VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CActivo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

Private m_oConexion As CConexion

Private m_lID As Long
Private m_sCod As String
Private m_sDen As String
Private m_sCodERP As String
Private m_lEmpresa As Long
Private m_sCentroSM As String
Private m_sCentroSMDen As String

Public Property Let ID(ByVal dato As Long)
    m_lID = dato
End Property

Public Property Get ID() As Long
    ID = m_lID
End Property

Public Property Let Cod(ByVal dato As String)
    m_sCod = dato
End Property
Public Property Get Cod() As String
    Cod = m_sCod
End Property

Public Property Let Den(ByVal dato As String)
    m_sDen = dato
End Property

Public Property Get Den() As String
    Den = m_sDen
End Property

Public Property Let Empresa(ByVal dato As Long)
    m_lEmpresa = dato
End Property

Public Property Get Empresa() As Long
    Empresa = m_lEmpresa
End Property

Public Property Let CentroSM(ByVal dato As String)
    m_sCentroSM = dato
End Property

Public Property Get CentroSM() As String
    CentroSM = m_sCentroSM
End Property

Public Property Let CodERP(ByVal dato As String)
    m_sCodERP = dato
End Property

Public Property Get CodERP() As String
    CodERP = m_sCodERP
End Property

Public Property Let CentroSMDen(ByVal dato As String)
    m_sCentroSMDen = dato
End Property

Public Property Get CentroSMDen() As String
    CentroSMDen = m_sCentroSMDen
End Property

Friend Property Set Conexion(ByVal vData As CConexion)

    ''' * Objetivo: Dar valor a la conexion
    ''' * Recibe: Conexion
    ''' * Devuelve: Nada

    Set m_oConexion = vData
    
End Property
Friend Property Get Conexion() As CConexion

    ''' * Objetivo: Devolver la conexion
    ''' * Recibe: Nada
    ''' * Devuelve: Conexion

    Set Conexion = m_oConexion
    
End Property

Private Sub Class_Terminate()
    
    On Error GoTo ERROR:
    
    Set m_oConexion = Nothing
    Exit Sub
ERROR:
    Resume Next
    
End Sub




''' <summary>
''' Valida un codigo, den o c�digo ERP de activo con los centros de coste de las imputaciones
''' </summary>
''' <param name="lEmpresa">Id de empresa</param>
''' <param name="sUsu">C�digo de usuario para los permisos, si administrador viene en blanco</param>
''' <param name="sCod">C�digo de activo</param>
''' <param name="sDen">Denominaci�n de activo</param>
''' <param name="sCodERP">C�digo ERP de activo</param>
''' <param name="vUON1">UON1 contra la que validar, optional</param>
''' <param name="vUON2">UON2 contra la que validar, optional</param>
''' <param name="vUON3">UON3 contra la que validar, optional</param>
''' <param name="vUON4">UON4 contra la que validar, optional</param>
''' <returns>Boolean, true si existe, False si no</returns>
''' <remarks>Llamada desde:CLineaRecepcion.ValidarActivo </remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>
Public Function ValidarActivo(ByVal lEmpresa As Long, Optional ByVal sUsu As String = "", Optional ByVal sCod As String = "", Optional ByVal sDen As String = "", Optional ByVal sCodERP As String = "", Optional ByVal vUON1 As Variant, Optional ByVal vUON2 As Variant, Optional ByVal vUON3 As Variant, Optional ByVal vUON4 As Variant) As Boolean
Dim sConsulta As String
Dim adoComm As adodb.Command
Dim adoParam As adodb.Parameter
Dim adoRs As New adodb.Recordset
Dim oImp As CImputacion
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If IsMissing(vUON1) Or IsEmpty(vUON1) Then vUON1 = Null
    If IsMissing(vUON2) Or IsEmpty(vUON2) Then vUON2 = Null
    If IsMissing(vUON3) Or IsEmpty(vUON3) Then vUON3 = Null
    If IsMissing(vUON4) Or IsEmpty(vUON4) Then vUON4 = Null
    
    If sCod = "" And sDen = "" And sCodERP = "" Then
        ValidarActivo = False
        Exit Function
    End If

    Set adoComm = New adodb.Command

    sConsulta = "SELECT DISTINCT A.ID, A.COD, D.DEN, A.EMPRESA, S.CENTRO_SM, A.COD_ERP FROM ACTIVO A WITH (NOLOCK) "
    sConsulta = sConsulta & " INNER JOIN ACTIVO_DEN D WITH (NOLOCK) ON A.ID=D.ACTIVO "
    sConsulta = sConsulta & " INNER JOIN CENTRO_SM_UON S WITH (NOLOCK) ON S.CENTRO_SM=A.CENTRO_SM "
    If sUsu <> "" Then
        sConsulta = sConsulta & " INNER JOIN USU_CC_IMPUTACION U WITH (NOLOCK) ON S.UON1=U.UON1 AND ISNULL(S.UON2,0)=ISNULL(U.UON2,0) AND ISNULL(S.UON3,0)=ISNULL(U.UON3,0) AND ISNULL(S.UON4,0)=ISNULL(U.UON4,0) "
    End If
    sConsulta = sConsulta & " WHERE A.EMPRESA=? "
    Set adoParam = adoComm.CreateParameter("EMPRESA", adInteger, adParamInput, , lEmpresa)
    adoComm.Parameters.Append adoParam

    If sCod <> "" Then
        sConsulta = sConsulta & " AND A.COD=? "
        Set adoParam = adoComm.CreateParameter("COD", adVarChar, adParamInput, 50, sCod)
        adoComm.Parameters.Append adoParam
    End If
    If sDen <> "" Then
        sConsulta = sConsulta & " AND D.DEN=? "
        Set adoParam = adoComm.CreateParameter("DEN", adVarChar, adParamInput, 500, sDen)
        adoComm.Parameters.Append adoParam
    End If
    If sCodERP <> "" Then
        sConsulta = sConsulta & " AND A.COD_ERP=? "
        Set adoParam = adoComm.CreateParameter("CODERP", adVarChar, adParamInput, 200, sCodERP)
        adoComm.Parameters.Append adoParam
    End If
    sConsulta = sConsulta & " AND A.BAJALOG=0 AND D.IDI=? "
    Set adoParam = adoComm.CreateParameter("IDIOMA", adVarChar, adParamInput, 20, gParametrosInstalacion.gIdioma)
    adoComm.Parameters.Append adoParam
    If sUsu <> "" Then
        sConsulta = sConsulta & " AND U.USU=? "
        Set adoParam = adoComm.CreateParameter("USU", adVarChar, adParamInput, 50, sUsu)
        adoComm.Parameters.Append adoParam
    End If
    If Not IsNull(vUON1) Then
        sConsulta = sConsulta & " AND S.UON1=? AND ISNULL(S.UON2,0)=ISNULL(?,0) AND ISNULL(S.UON3,0)=ISNULL(?,0) AND ISNULL(S.UON4,0)=ISNULL(?,0) "
        Set adoParam = adoComm.CreateParameter("UON1", adVarChar, adParamInput, 50, vUON1)
        adoComm.Parameters.Append adoParam
        Set adoParam = adoComm.CreateParameter("UON2", adVarChar, adParamInput, 50, vUON2)
        adoComm.Parameters.Append adoParam
        Set adoParam = adoComm.CreateParameter("UON3", adVarChar, adParamInput, 50, vUON3)
        adoComm.Parameters.Append adoParam
        Set adoParam = adoComm.CreateParameter("UON4", adVarChar, adParamInput, 50, vUON4)
        adoComm.Parameters.Append adoParam
    End If
    Set adoComm.ActiveConnection = m_oConexion.ADOCon
    adoComm.CommandText = sConsulta
    adoComm.CommandType = adCmdText


    Set adoRs = adoComm.Execute

    If Not adoRs.eof Then
        If basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Activos) Then
            If IsNull(adoRs.Fields("COD_ERP").Value) Or adoRs.Fields("COD_ERP").Value = "" Then
                ValidarActivo = False
            Else
                ValidarActivo = True
            End If
        Else
            ValidarActivo = True
        End If
        If ValidarActivo Then
            m_lID = adoRs.Fields("ID").Value
            m_sCod = adoRs.Fields("COD").Value
            m_sDen = adoRs.Fields("DEN").Value
            m_sCodERP = NullToStr(adoRs.Fields("COD_ERP").Value)
            m_lEmpresa = adoRs.Fields("EMPRESA").Value
            m_sCentroSM = NullToStr(adoRs.Fields("CENTRO_SM").Value)
        End If
    Else
        ValidarActivo = False
    End If

    
    adoRs.Close
    Set adoRs = Nothing
    Set adoComm = Nothing
    Set adoParam = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If Err.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CActivo", "ValidarActivo", Err, Erl)
      Exit Function
   End If

End Function



