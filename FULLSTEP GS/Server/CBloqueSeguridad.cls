VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CBloqueSeguridad"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum


Implements IBaseDatos

Private moConexion As CConexion

Private mlId As Long
Private msDen As String
Private mdtFecAct As Variant
Private m_adores As ADODB.Recordset

Private mvarIndice As Variant


Friend Property Set Conexion(ByVal con As CConexion)
Set moConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = moConexion
End Property

Public Property Get ID() As Long
    ID = mlId
End Property

Public Property Let ID(ByVal Data As Long)
    mlId = Data
End Property
Public Property Get Den() As String
    Den = msDen
End Property

Public Property Let Den(ByVal Data As String)
    msDen = Data
End Property

Public Property Let Indice(ByVal varIndice As Variant)
    mvarIndice = varIndice
End Property
Public Property Get Indice() As Variant
    Indice = mvarIndice
End Property
Public Property Get FecAct() As Variant
    FecAct = mdtFecAct
End Property

Public Property Let FecAct(ByVal Data As Variant)
    mdtFecAct = Data
End Property

Private Sub Class_Terminate()
    Set moConexion = Nothing
End Sub

Private Function IBaseDatos_AnyadirABaseDatos() As TipoErrorSummit
Dim sConsulta As String
Dim TESError As TipoErrorSummit
Dim AdoRes As ADODB.Recordset
Dim bTransaccionEnCurso As Boolean

TESError.NumError = TESnoerror

If moConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CWorkflow.AnyadirABAseDatos", "No se ha establecido la conexion"
End If
' *****************************************

On Error GoTo Error:
    
    moConexion.ADOCon.Execute "BEGIN TRAN"
    bTransaccionEnCurso = True
    moConexion.ADOCon.Execute "SET XACT_ABORT OFF"
    
    
    sConsulta = "SELECT MAX(ID) ID FROM BLOQUE_SEGURIDAD"
    Set AdoRes = New ADODB.Recordset
    AdoRes.Open sConsulta, moConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    If IsNull(AdoRes("id").Value) Then
        mlId = 1
    Else
        mlId = AdoRes("ID").Value + 1
    End If
    AdoRes.Close
    Set AdoRes = Nothing
    
    
    sConsulta = "INSERT INTO BLOQUE_SEGURIDAD (ID, DEN) VALUES (" & mlId & ",'" & DblQuote(msDen) & "')"
    moConexion.ADOCon.Execute sConsulta
    If moConexion.ADOCon.Errors.Count > 0 Then GoTo Error
         
    
    ''' Recuperar la fecha de insercion
    sConsulta = "SELECT FECACT FROM BLOQUE_SEGURIDAD WHERE ID =" & mlId
    Set AdoRes = New ADODB.Recordset
    AdoRes.Open sConsulta, moConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    mdtFecAct = AdoRes("FECACT").Value
    AdoRes.Close
    Set AdoRes = Nothing
    
    moConexion.ADOCon.Execute "COMMIT TRAN"
    bTransaccionEnCurso = False
       
    IBaseDatos_AnyadirABaseDatos = TESError
    Exit Function
Error:

    If Not AdoRes Is Nothing Then
        AdoRes.Close
        Set AdoRes = Nothing
    End If
                 
    IBaseDatos_AnyadirABaseDatos = basErrores.TratarError(moConexion.ADOCon.Errors)
                
    If bTransaccionEnCurso Then
        moConexion.ADOCon.Execute "ROLLBACK TRAN"
        bTransaccionEnCurso = False
    End If
End Function

Private Function IBaseDatos_CambiarCodigo(CodigoNuevo As Variant) As TipoErrorSummit

End Function

Private Sub IBaseDatos_CancelarEdicion()

If Not m_adores Is Nothing Then
    m_adores.Close
    Set m_adores = Nothing
End If

End Sub

Private Function IBaseDatos_ComprobarExistenciaEnBaseDatos() As Boolean

End Function

Private Function IBaseDatos_EliminarDeBaseDatos() As TipoErrorSummit

End Function

Private Function IBaseDatos_FinalizarEdicionEliminando() As TipoErrorSummit
Dim TESError As TipoErrorSummit
Dim bTrans As Boolean
Dim sConsulta As String

TESError.NumError = TESnoerror

'******************* Precondicion *******************
If moConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CUnidadOrgNivel2.FinalizarEdicionEliminando", "No se ha establecido la conexion"
End If
'*****************************************************
On Error GoTo Error:
    
    moConexion.ADOCon.Execute "BEGIN TRAN"
    moConexion.ADOCon.Execute "SET XACT_ABORT OFF"
    bTrans = True
    
    sConsulta = "IF EXISTS (SELECT * FROM PASO WHERE BLOQUE_SEGURIDAD = " & mlId & ") " & vbCrLf
    sConsulta = sConsulta & " UPDATE BLOQUE_SEGURIDAD SET BAJA = 1 WHERE ID = " & mlId & vbCrLf
    sConsulta = sConsulta & " ELSE " & vbCrLf
    sConsulta = sConsulta & " BEGIN" & vbCrLf
    sConsulta = sConsulta & " DELETE FROM APROBADORES WHERE BLOQUE_SEGURIDAD = " & mlId & vbCrLf
    sConsulta = sConsulta & " DELETE FROM BLOQUE_SEGURIDAD WHERE ID = " & mlId & vbCrLf
    sConsulta = sConsulta & " END" & vbCrLf
    
    moConexion.ADOCon.Execute sConsulta
    If moConexion.ADOCon.Errors.Count > 0 Then
        GoTo Error
    End If
    moConexion.ADOCon.Execute "COMMIT TRAN"
    bTrans = False
    Set m_adores = Nothing
    
    IBaseDatos_FinalizarEdicionEliminando = TESError
    
    Exit Function
    
Error:
    
    If moConexion.ADOCon.Errors.Count > 0 Then
        IBaseDatos_FinalizarEdicionEliminando = basErrores.TratarError(moConexion.ADOCon.Errors)
        If bTrans Then
            moConexion.ADOCon.Execute "ROLLBACK TRAN"
            bTrans = False
        End If
        Exit Function
    End If
    
    If m_adores.EditMode = 1 Then
        m_adores.CancelUpdate
    End If
End Function

Private Function IBaseDatos_FinalizarEdicionModificando() As TipoErrorSummit
Dim TESError As TipoErrorSummit
Dim sConsulta As String
Dim bTransaccionEnCurso As Boolean

TESError.NumError = TESnoerror

'********* Precondicion *******************
If moConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CBloqueSeguridad.FinalizarEdicionModificando", "No se ha establecido la conexion"
End If
'******************************************

On Error GoTo Error:

    moConexion.ADOCon.Execute "BEGIN TRAN"
    bTransaccionEnCurso = True
    moConexion.ADOCon.Execute "SET XACT_ABORT OFF"
    
    'Comprobamos si ha habido cambios en otra sesi�n
    Set m_adores = New ADODB.Recordset
    sConsulta = "SELECT FECACT FROM BLOQUE_SEGURIDAD WHERE ID=" & mlId
    m_adores.Open sConsulta, moConexion.ADOCon, adOpenStatic, adLockReadOnly
    
    If m_adores.eof Then
        m_adores.Close
        Set m_adores = Nothing
        TESError.NumError = TESDatoEliminado
        TESError.Arg1 = 149
        IBaseDatos_FinalizarEdicionModificando = TESError
        moConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
        Exit Function
    End If
    
    If mdtFecAct <> m_adores("FECACT").Value Then
        m_adores.Close
        Set m_adores = Nothing
        TESError.NumError = TESInfActualModificada
        moConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
        IBaseDatos_FinalizarEdicionModificando = TESError
        Exit Function
    End If
    
    ''' Actualizar
    sConsulta = "UPDATE BLOQUE_SEGURIDAD SET DEN = '" & DblQuote(msDen) & "' WHERE ID = " & mlId
    moConexion.ADOCon.Execute sConsulta
    If moConexion.ADOCon.Errors.Count > 0 Then
        GoTo Error
    End If
 
    'Recogemos el nuevo valor de FECACT
    m_adores.Requery
    mdtFecAct = m_adores("FECACT").Value
    m_adores.Close
    Set m_adores = Nothing
    
    moConexion.ADOCon.Execute "COMMIT TRAN"
    bTransaccionEnCurso = False
    
    IBaseDatos_FinalizarEdicionModificando = TESError
    
    Exit Function

Error:
    
    IBaseDatos_FinalizarEdicionModificando = basErrores.TratarError(moConexion.ADOCon.Errors)
    
    If Not m_adores Is Nothing Then
        m_adores.Close
        Set m_adores = Nothing
    End If
    
    If bTransaccionEnCurso Then
        moConexion.ADOCon.Execute "ROLLBACK"
        moConexion.ADOCon.Execute "SET XACT_ABORT OFF"
        bTransaccionEnCurso = False
    End If

End Function

Private Function IBaseDatos_IniciarEdicion(Optional ByVal Bloquear As Boolean, Optional ByVal UsuarioBloqueo As String) As TipoErrorSummit
Dim TESError As TipoErrorSummit

TESError.NumError = TESnoerror

'******************* Precondicion *******************
If moConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CBloqueSeguridad.IniciarEdicion", "No se ha establecido la conexion"
End If
'*****************************************************

Set m_adores = New ADODB.Recordset
m_adores.Open "SELECT ID,DEN, FECACT FROM BLOQUE_SEGURIDAD WHERE ID =" & mlId, moConexion.ADOCon, adOpenKeyset, adLockOptimistic

If m_adores.eof Then
        m_adores.Close
        Set m_adores = Nothing
        TESError.NumError = TESDatoEliminado
        TESError.Arg1 = 148 ''"Workflow"
        TESError.Arg2 = 1
        IBaseDatos_IniciarEdicion = TESError
        Exit Function
End If

msDen = m_adores.Fields("DEN").Value
mdtFecAct = m_adores.Fields("FECACT").Value

IBaseDatos_IniciarEdicion = TESError

End Function

