VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CDepAsociados"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum
'local variable to hold collection
Private mCol As Collection
Private mvarConexion As CConexion
Private mvarConexionServer As CConexionDeUseServer


Public Function Add(ByVal CodUON1 As Variant, ByVal CodUON2 As Variant, ByVal CodUON3 As Variant, ByVal Cod As Variant, ByVal Den As String, Optional ByVal varIndice As Variant) As CDepAsociado
    
    'create a new object
    Dim objnewmember As CDepAsociado
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New CDepAsociado

    'set the properties passed into the method
    objnewmember.Cod = Cod
    objnewmember.Den = Den
    objnewmember.CodUON1 = CodUON1
    objnewmember.CodUON2 = CodUON2
    objnewmember.CodUON3 = CodUON3
    Set objnewmember.Conexion = mvarConexion
    Set objnewmember.ConexionServer = mvarConexionServer
    If IsNull(varIndice) Or IsMissing(varIndice) Then
        mCol.Add objnewmember, CStr(Cod)
    Else
        mCol.Add objnewmember, CStr(varIndice)
    End If

    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CDepAsociados", "Add", ERR, Erl)
        Exit Function
    End If

End Function

Public Property Get Item(vntIndexKey As Variant) As CDepAsociado
   On Error GoTo ERROR:
   
    Set Item = mCol(vntIndexKey)
    Exit Property
ERROR:
    Set Item = Nothing
    
End Property
Public Property Get Count() As Long
    
    If mCol Is Nothing Then
        Count = 0
    Else
        Count = mCol.Count
    End If
    
End Property

Friend Property Set Conexion(ByVal con As CConexion)
Set mvarConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = mvarConexion
End Property
Friend Property Set ConexionServer(ByVal con As CConexionDeUseServer)
Set mvarConexionServer = con
End Property
Friend Property Get ConexionServer() As CConexionDeUseServer
Set ConexionServer = mvarConexionServer
End Property
Public Sub Remove(vntIndexKey As Variant)
    mCol.Remove vntIndexKey
End Sub


Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
    Set NewEnum = mCol.[_NewEnum]
End Property


Private Sub Class_Initialize()
   

    'creates the collection when this class is created
    Set mCol = New Collection
End Sub


Private Sub Class_Terminate()
    'destroys collection when this class is terminated
    Set mCol = Nothing
    Set mvarConexion = Nothing
    Set mvarConexionServer = Nothing
End Sub

''' <summary>Carga los departamentos asociados seg�n los criterios indicados</summary>
''' <param name="UON1">Unidad Organizativa de nivel 1</param>
''' <param name="UON2">Unidad Organizativa de nivel 2</param>
''' <param name="UON3">Unidad Organizativa de nivel 3</param>
''' <param name="CodDep">Cod. Departamento</param>
''' <param name="RUO">Restricci�n a la UON del usuario</param>
''' <param name="RDep">RDep</param>
''' <param name="NivelACargar">NivelACargar</param>
''' <param name="CarIniCod">Caracteres iniciales del c�digo</param>
''' <param name="CoincidenciaTotal">Coincidencia total</param>
''' <param name="OrdenadosPorDen">Orden por denominaci�n</param>
''' <param name="UsarIndice">Usar �ndice</param>
''' <param name="BajaLog">BajaLog</param>
''' <param name="Pyme">Pyme</param>
''' <param name="bRPerfUON">Restringir a las UON del perfil del usuario</param>
''' <param name="lIdPerf">Id. perfil</param>
''' <revision>LTG 25/01/2013</revision>

Public Sub CargarTodosLosDepAsociados(Optional ByVal UON1 As Variant, Optional ByVal UON2 As Variant, Optional ByVal UON3 As Variant, _
        Optional ByVal CodDep As Variant, Optional ByVal RUO As Boolean = False, Optional ByVal RDep As Boolean, Optional ByVal NivelACargar As Integer, _
        Optional ByVal CarIniCod As String, Optional ByVal CarIniDen As String, Optional ByVal CoincidenciaTotal As Boolean, _
        Optional ByVal OrdenadosPorDen As Boolean = False, Optional ByVal UsarIndice As Boolean = False, Optional ByVal BajaLog As Boolean, _
        Optional ByVal Pyme As Variant, Optional ByVal bRPerfUON As Boolean, Optional ByVal lIdPerf As Long, Optional ByVal OrdenadasPorEstructOrg As Boolean = False)
    Dim rs As New adodb.Recordset
    Dim fldCod As adodb.Field
    Dim fldDen As adodb.Field
    Dim fldUon1Cod As adodb.Field
    Dim fldUon2Cod As adodb.Field
    Dim fldUon3Cod As adodb.Field
    Dim sConsulta0 As String
    Dim sConsulta1 As String
    Dim sConsulta2 As String
    Dim sConsulta3 As String
    Dim sCad As String
    Dim lIndice As Long
    Dim oDepAsociado As CDepAsociado
    Dim fldBajaLog As adodb.Field
    Dim sUON As String
    
    '********* Precondicion **************************************
    If mvarConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CUnidadeOrgNivel0CargarTodosLosDep", "No se ha establecido la conexion"
        Exit Sub
    End If
    '*************************************************************
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
        
    If Not IsMissing(UON1) Then
        If IsEmpty(UON1) Then
            UON1 = Null
        End If
        If Trim(UON1) = "" Then
            UON1 = Null
        End If
    Else
        UON1 = Null
    End If
    
    If Not IsMissing(UON2) Then
        If IsEmpty(UON2) Then
            UON2 = Null
        End If
        If Trim(UON2) = "" Then
            UON2 = Null
        End If
    Else
        UON2 = Null
    End If
    
    If Not IsMissing(UON3) Then
        If IsEmpty(UON3) Then
            UON3 = Null
        End If
        If Trim(UON3) = "" Then
            UON3 = Null
        End If
    Else
        UON3 = Null
    End If
    If Not IsMissing(CodDep) Then
        If IsEmpty(CodDep) Then
            CodDep = ""
        End If
        If Trim(CodDep) = "" Then
            CodDep = ""
        End If
    Else
        CodDep = ""
    End If
    
    If Not IsMissing(Pyme) Then
        If IsEmpty(Pyme) Then
            Pyme = 0
        End If
    Else
        Pyme = 0
    End If
        
    If Not RUO And Not RDep And Not bRPerfUON Then
        If Pyme <> 0 Then
            sConsulta0 = "SELECT DEP.COD , DEP.DEN, 0, UON0_DEP.BAJALOG FROM UON0_DEP WITH (NOLOCK) INNER JOIN DEP WITH (NOLOCK) ON UON0_DEP.DEP=DEP.COD"
            If Not IsNull(UON1) Then
                sConsulta1 = "SELECT DEP.COD , DEP.DEN ,UON1_DEP.UON1 AS UON1COD, UON1_DEP.BAJALOG FROM UON1_DEP WITH (NOLOCK) INNER JOIN DEP WITH (NOLOCK) ON UON1_DEP.DEP=DEP.COD AND UON1_DEP.UON1='" & DblQuote(UON1) & "'"
                sConsulta2 = "SELECT DEP.COD , DEP.DEN ,UON2_DEP.UON1 AS UON1COD, UON2_DEP.UON2 AS UON2COD, UON2_DEP.BAJALOG FROM UON2_DEP WITH (NOLOCK) INNER JOIN DEP WITH (NOLOCK) ON UON2_DEP.DEP=DEP.COD AND UON2_DEP.UON1='" & DblQuote(UON1) & "'"
                sConsulta3 = "SELECT DEP.COD , DEP.DEN ,UON3_DEP.UON1 AS UON1COD, UON3_DEP.UON2 AS UON2COD, UON3_DEP.UON3 AS UON3COD, UON3_DEP.BAJALOG FROM UON3_DEP WITH (NOLOCK) INNER JOIN DEP WITH (NOLOCK) ON UON3_DEP.DEP=DEP.COD AND UON3_DEP.UON1='" & DblQuote(UON1) & "'"
            End If
        Else
            ' No hay restricciones
            sConsulta0 = "SELECT DEP.COD , DEP.DEN, 0, UON0_DEP.BAJALOG FROM UON0_DEP WITH (NOLOCK) INNER JOIN DEP WITH (NOLOCK) ON UON0_DEP.DEP=DEP.COD"
            sConsulta1 = "SELECT DEP.COD , DEP.DEN ,UON1_DEP.UON1 AS UON1COD, UON1_DEP.BAJALOG FROM UON1_DEP WITH (NOLOCK) INNER JOIN DEP WITH (NOLOCK) ON UON1_DEP.DEP=DEP.COD"
            sConsulta2 = "SELECT DEP.COD , DEP.DEN ,UON2_DEP.UON1 AS UON1COD, UON2_DEP.UON2 AS UON2COD, UON2_DEP.BAJALOG FROM UON2_DEP WITH (NOLOCK) INNER JOIN DEP WITH (NOLOCK) ON UON2_DEP.DEP=DEP.COD"
            sConsulta3 = "SELECT DEP.COD , DEP.DEN ,UON3_DEP.UON1 AS UON1COD, UON3_DEP.UON2 AS UON2COD, UON3_DEP.UON3 AS UON3COD, UON3_DEP.BAJALOG FROM UON3_DEP WITH (NOLOCK) INNER JOIN DEP WITH (NOLOCK) ON UON3_DEP.DEP=DEP.COD"
        End If
    Else
        Select Case NivelACargar
            Case 0  'Cargar los departamentos del nivel 0
                sConsulta0 = "SELECT DEP.COD,DEP.DEN,0,0 FROM DEP WITH (NOLOCK) WHERE 1=1"
                
                If RDep Then
                    'Cargamos su departamento
                    sConsulta0 = sConsulta0 & " AND COD='" & DblQuote(CodDep) & "'"
                End If
                
                If RUO Or bRPerfUON Then
                    If Not (IsNull(UON1) And IsNull(UON2) And IsNull(UON3)) Then
                        Set mCol = Nothing
                        Set mCol = New Collection
                        Exit Sub
                    End If
                End If
                    
            Case 1
                sConsulta1 = "SELECT DEP.COD,DEP.DEN,UON1_DEP.UON1 AS UON1COD,UON1_DEP.BAJALOG"
                If RUO Or bRPerfUON Then
                    sUON = ConstruirCriterioUONUsuPerfUONDep("UON1_DEP", 1, RUO, bRPerfUON, lIdPerf, NullToStr(UON1))
                    sConsulta1 = sConsulta1 & " FROM (" & sUON & ") UON1_DEP "
                Else
                    sConsulta1 = sConsulta1 & " FROM UON1_DEP WITH (NOLOCK) "
                End If
                sConsulta1 = sConsulta1 & " INNER JOIN DEP WITH (NOLOCK) ON UON1_DEP.DEP=DEP.COD"
                sConsulta1 = sConsulta1 & " WHERE 1=1"
                
                If RDep Then
                    'Cargamos su departamento
                    sConsulta1 = sConsulta1 & " AND DEP.COD='" & DblQuote(CodDep) & "'"
                End If
                
                If RDep And RUO And bRPerfUON Then
                    If Not IsNull(UON1) And (Not IsNull(UON2) Or Not IsNull(UON3)) Then
                        'Es de nivel 2 o 3
                        Set mCol = Nothing
                        Set mCol = New Collection
                        Exit Sub
                    End If
                End If
                    
            Case 2
                sConsulta2 = "SELECT DEP.COD,DEP.DEN,UON2_DEP.UON1 AS UON1COD,UON2_DEP.UON2 AS UON2COD,UON2_DEP.BAJALOG"
                If RUO Or bRPerfUON Then
                    sUON = ConstruirCriterioUONUsuPerfUONDep("UON2_DEP", 2, RUO, bRPerfUON, lIdPerf, NullToStr(UON1), NullToStr(UON2))
                    sConsulta2 = sConsulta2 & " FROM (" & sUON & ") UON2_DEP "
                Else
                    sConsulta2 = sConsulta2 & " FROM UON2_DEP WITH (NOLOCK) "
                End If
                sConsulta2 = sConsulta2 & " INNER JOIN DEP WITH (NOLOCK) ON UON2_DEP.DEP=DEP.COD"
                sConsulta2 = sConsulta2 & " WHERE 1=1"
                
                'Cargar los departamentos del nivel 2
                If RDep Then
                    'Cargamos su departamento
                    sConsulta2 = sConsulta2 & " AND DEP.COD='" & DblQuote(CodDep) & "'"
                End If
                
                If RDep And RUO And bRPerfUON Then
                    If Not IsNull(UON1) And Not IsNull(UON2) And Not IsNull(UON3) Then
                        'Es de nivel 3
                        Set mCol = Nothing
                        Set mCol = New Collection
                        Exit Sub
                    End If
                End If
                    
            Case 3
                'Cargar los departamentos del nivel 3
                sConsulta3 = "SELECT DEP.COD,DEP.DEN,UON3_DEP.UON1 AS UON1COD,UON3_DEP.UON2 AS UON2COD,UON3_DEP.UON3 AS UON3COD,UON3_DEP.BAJALOG"
                If RUO Or bRPerfUON Then
                    sUON = ConstruirCriterioUONUsuPerfUONDep("UON3_DEP", 3, RUO, bRPerfUON, lIdPerf, NullToStr(UON1), NullToStr(UON2), NullToStr(UON3))
                    sConsulta3 = sConsulta3 & " FROM (" & sUON & ") UON3_DEP "
                Else
                    sConsulta3 = sConsulta3 & " FROM UON3_DEP WITH (NOLOCK) "
                End If
                sConsulta3 = sConsulta3 & " INNER JOIN DEP WITH (NOLOCK) ON UON3_DEP.DEP=DEP.COD"
                sConsulta3 = sConsulta3 & " WHERE 1=1"
                
                If RDep Then
                    'Cargamos su departamento
                    sConsulta3 = sConsulta3 & " AND DEP.COD='" & DblQuote(CodDep) & "'"
                End If
        End Select
    End If
    
    If CarIniCod <> "" Then
        sCad = "1"
    Else
        sCad = "0"
    End If
    
    If CarIniDen <> "" Then
        sCad = sCad & "1"
    Else
        sCad = sCad & "0"
    End If

    Select Case sCad
                
        Case "01"
            If CoincidenciaTotal Then
                sConsulta0 = sConsulta0 & " AND DEP.DEN = '" & DblQuote(CarIniDen) & "'"
                sConsulta1 = sConsulta1 & " AND DEP.DEN = '" & DblQuote(CarIniDen) & "'"
                sConsulta2 = sConsulta2 & " AND DEP.DEN = '" & DblQuote(CarIniDen) & "'"
                sConsulta3 = sConsulta3 & " AND DEP.DEN = '" & DblQuote(CarIniDen) & "'"
            Else
                sConsulta0 = sConsulta0 & " AND DEP.DEN LIKE '" & DblQuote(CarIniDen) & "%'"
                sConsulta1 = sConsulta1 & " AND DEP.DEN LIKE '" & DblQuote(CarIniDen) & "%'"
                sConsulta2 = sConsulta2 & " AND DEP.DEN LIKE '" & DblQuote(CarIniDen) & "%'"
                sConsulta3 = sConsulta3 & " AND DEP.DEN LIKE '" & DblQuote(CarIniDen) & "%'"
            End If
                          
        Case "10"
            If CoincidenciaTotal Then
                sConsulta0 = sConsulta0 & " AND DEP.COD = '" & DblQuote(CarIniCod) & "'"
                sConsulta1 = sConsulta1 & " AND DEP.COD = '" & DblQuote(CarIniCod) & "'"
                sConsulta2 = sConsulta2 & " AND DEP.COD = '" & DblQuote(CarIniCod) & "'"
                sConsulta3 = sConsulta3 & " AND DEP.COD = '" & DblQuote(CarIniCod) & "'"
            Else
                sConsulta0 = sConsulta0 & " AND DEP.COD LIKE '" & DblQuote(CarIniCod) & "%'"
                sConsulta1 = sConsulta1 & " AND DEP.COD LIKE '" & DblQuote(CarIniCod) & "%'"
                sConsulta2 = sConsulta2 & " AND DEP.COD LIKE '" & DblQuote(CarIniCod) & "%'"
                sConsulta3 = sConsulta3 & " AND DEP.COD LIKE '" & DblQuote(CarIniCod) & "%'"
            End If
                       
        Case "11"
            If CoincidenciaTotal Then
                sConsulta0 = sConsulta0 & " AND DEP.COD = '" & DblQuote(CarIniCod) & "'"
                sConsulta0 = sConsulta0 & " AND DEP.DEN = '" & DblQuote(CarIniDen) & "'"
                sConsulta1 = sConsulta1 & " AND DEP.COD = '" & DblQuote(CarIniCod) & "'"
                sConsulta1 = sConsulta1 & " AND DEP.DEN = '" & DblQuote(CarIniDen) & "'"
                sConsulta2 = sConsulta2 & " AND DEP.COD = '" & DblQuote(CarIniCod) & "'"
                sConsulta2 = sConsulta2 & " AND DEP.DEN = '" & DblQuote(CarIniDen) & "'"
                sConsulta3 = sConsulta3 & " AND DEP.COD = '" & DblQuote(CarIniCod) & "'"
                sConsulta3 = sConsulta3 & " AND DEP.DEN = '" & DblQuote(CarIniDen) & "'"
            Else
                sConsulta0 = sConsulta0 & " AND DEP.COD LIKE '" & DblQuote(CarIniCod) & "%'"
                sConsulta0 = sConsulta0 & " AND DEP.DEN LIKE '" & DblQuote(CarIniDen) & "%'"
                sConsulta1 = sConsulta1 & " AND DEP.COD LIKE '" & DblQuote(CarIniCod) & "%'"
                sConsulta1 = sConsulta1 & " AND DEP.DEN LIKE '" & DblQuote(CarIniDen) & "%'"
                sConsulta2 = sConsulta2 & " AND DEP.COD LIKE '" & DblQuote(CarIniCod) & "%'"
                sConsulta2 = sConsulta2 & " AND DEP.DEN LIKE '" & DblQuote(CarIniDen) & "%'"
                sConsulta3 = sConsulta3 & " AND DEP.COD LIKE '" & DblQuote(CarIniCod) & "%'"
                sConsulta3 = sConsulta3 & " AND DEP.DEN LIKE '" & DblQuote(CarIniDen) & "%'"
            End If
        End Select
  
    If Not BajaLog Then
        'sConsulta0 = sConsulta0 & " AND UON0_DEP.BAJALOG =0"
        sConsulta1 = sConsulta1 & " AND UON1_DEP.BAJALOG =0"
        sConsulta2 = sConsulta2 & " AND UON2_DEP.BAJALOG =0"
        sConsulta3 = sConsulta3 & " AND UON3_DEP.BAJALOG =0"
    End If
    
    If OrdenadosPorDen And Not OrdenadasPorEstructOrg Then
        sConsulta0 = sConsulta0 & " ORDER BY DEP.DEN"
        sConsulta1 = sConsulta1 & " ORDER BY DEP.DEN"
        sConsulta2 = sConsulta2 & " ORDER BY DEP.DEN"
        sConsulta3 = sConsulta3 & " ORDER BY DEP.DEN"
    Else
        If OrdenadosPorDen And OrdenadasPorEstructOrg Then
            sConsulta0 = sConsulta0 & " ORDER BY DEP.DEN"
            sConsulta1 = sConsulta1 & " ORDER BY UON1_DEP.UON1,DEP.DEN"
            sConsulta2 = sConsulta2 & " ORDER BY UON2_DEP.UON1,UON2_DEP.UON2,DEP.DEN"
            sConsulta3 = sConsulta3 & " ORDER BY UON3_DEP.UON1,UON3_DEP.UON2,UON3_DEP.UON3,DEP.DEN"
        Else
            If OrdenadasPorEstructOrg Then
                sConsulta0 = sConsulta0 & " ORDER BY DEP.COD"
                sConsulta1 = sConsulta1 & " ORDER BY UON1_DEP.UON1,DEP.COD"
                sConsulta2 = sConsulta2 & " ORDER BY UON2_DEP.UON1,UON2_DEP.UON2,DEP.COD"
                sConsulta3 = sConsulta3 & " ORDER BY UON3_DEP.UON1,UON3_DEP.UON2,UON3_DEP.UON3,DEP.COD"
            Else
                sConsulta0 = sConsulta0 & " ORDER BY DEP.COD"
                sConsulta1 = sConsulta1 & " ORDER BY DEP.COD"
                sConsulta2 = sConsulta2 & " ORDER BY DEP.COD"
                sConsulta3 = sConsulta3 & " ORDER BY DEP.COD"
            End If
        End If
    End If
          
    Set mCol = Nothing
    Set mCol = New Collection
    
    Select Case NivelACargar
        Case 0
            rs.Open sConsulta0, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
            
            If Not rs.eof Then
                Set fldCod = rs.Fields(0)
                Set fldDen = rs.Fields(1)
                Set fldBajaLog = rs.Fields(3)
                
                If UsarIndice Then
                    lIndice = 0
                    
                    Set oDepAsociado = Nothing
                    While Not rs.eof
                        Set oDepAsociado = New CDepAsociado
                        oDepAsociado.Cod = fldCod.Value
                        oDepAsociado.Den = fldDen.Value
                        oDepAsociado.BajaLog = fldBajaLog.Value
                        oDepAsociado.Indice = lIndice
                        Set oDepAsociado.Conexion = mvarConexion
                        Set oDepAsociado.ConexionServer = mvarConexionServer
                        
                        mCol.Add oDepAsociado, CStr(lIndice)
                        Set oDepAsociado = Nothing
                        rs.MoveNext
                        lIndice = lIndice + 1
                    Wend
                Else
                     While Not rs.eof
                        Set oDepAsociado = New CDepAsociado
                        Set oDepAsociado.Conexion = mvarConexion
                        Set oDepAsociado.ConexionServer = mvarConexionServer
                        
                        oDepAsociado.Cod = fldCod.Value
                        oDepAsociado.Den = fldDen.Value
                        oDepAsociado.BajaLog = fldBajaLog.Value
                        mCol.Add oDepAsociado, fldCod.Value
                        Set oDepAsociado = Nothing
                        rs.MoveNext
                    Wend
                End If
            End If
    
        Case 1
            rs.Open sConsulta1, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
            
            If Not rs.eof Then
                Set fldCod = rs.Fields(0)
                Set fldDen = rs.Fields(1)
                Set fldUon1Cod = rs.Fields(2)
                Set fldBajaLog = rs.Fields(3)
                Set mCol = Nothing
                Set mCol = New Collection
                
                If UsarIndice Then
                    lIndice = 0
                    
                    Set oDepAsociado = Nothing
                    While Not rs.eof
                        Set oDepAsociado = New CDepAsociado
                        Set oDepAsociado.Conexion = mvarConexion
                        Set oDepAsociado.ConexionServer = mvarConexionServer
                        
                        oDepAsociado.Cod = fldCod.Value
                        oDepAsociado.Den = fldDen.Value
                        oDepAsociado.CodUON1 = fldUon1Cod.Value
                        oDepAsociado.BajaLog = fldBajaLog.Value
                        oDepAsociado.Indice = lIndice
                        mCol.Add oDepAsociado, CStr(lIndice)
                        Set oDepAsociado = Nothing
                        rs.MoveNext
                        lIndice = lIndice + 1
                    Wend
                Else
                     While Not rs.eof
                        Set oDepAsociado = New CDepAsociado
                        Set oDepAsociado.Conexion = mvarConexion
                        Set oDepAsociado.ConexionServer = mvarConexionServer
                        
                        oDepAsociado.Cod = fldCod.Value
                        oDepAsociado.Den = fldDen.Value
                        oDepAsociado.CodUON1 = fldUon1Cod.Value
                        oDepAsociado.BajaLog = fldBajaLog.Value
                        mCol.Add oDepAsociado, fldUon1Cod.Value & fldCod.Value
                        Set oDepAsociado = Nothing
                        rs.MoveNext
                    Wend
                End If
            End If
        
        Case 2
            rs.Open sConsulta2, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
            
            If Not rs.eof Then
                Set fldCod = rs.Fields(0)
                Set fldDen = rs.Fields(1)
                Set fldUon1Cod = rs.Fields(2)
                Set fldUon2Cod = rs.Fields(3)
                Set fldBajaLog = rs.Fields(4)
                
                If UsarIndice Then
                    lIndice = 0
                    
                    Set oDepAsociado = Nothing
                    While Not rs.eof
                        Set oDepAsociado = New CDepAsociado
                        Set oDepAsociado.Conexion = mvarConexion
                        Set oDepAsociado.ConexionServer = mvarConexionServer
                        
                        oDepAsociado.Cod = fldCod.Value
                        oDepAsociado.Den = fldDen.Value
                        oDepAsociado.CodUON1 = fldUon1Cod.Value
                        oDepAsociado.CodUON2 = fldUon2Cod.Value
                        oDepAsociado.BajaLog = fldBajaLog.Value
                        oDepAsociado.Indice = lIndice
                        mCol.Add oDepAsociado, CStr(lIndice)
                        Set oDepAsociado = Nothing
                        rs.MoveNext
                        lIndice = lIndice + 1
                    Wend
                Else
                     While Not rs.eof
                        Set oDepAsociado = New CDepAsociado
                        Set oDepAsociado.Conexion = mvarConexion
                        Set oDepAsociado.ConexionServer = mvarConexionServer
                                            
                        oDepAsociado.Cod = fldCod.Value
                        oDepAsociado.Den = fldDen.Value
                        oDepAsociado.CodUON1 = fldUon1Cod.Value
                        oDepAsociado.CodUON2 = fldUon2Cod.Value
                        oDepAsociado.BajaLog = fldBajaLog.Value
                        mCol.Add oDepAsociado, fldUon1Cod.Value & fldUon2Cod.Value & fldCod.Value
                        Set oDepAsociado = Nothing
                        rs.MoveNext
                    Wend
                End If
            End If
            
        Case 3
            rs.Open sConsulta3, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
            
            If Not rs.eof Then
                Set fldCod = rs.Fields(0)
                Set fldDen = rs.Fields(1)
                Set fldUon1Cod = rs.Fields(2)
                Set fldUon2Cod = rs.Fields(3)
                Set fldUon3Cod = rs.Fields(4)
                Set fldBajaLog = rs.Fields(5)
                
                If UsarIndice Then
                    lIndice = 0
                    
                    Set oDepAsociado = Nothing
                    While Not rs.eof
                        Set oDepAsociado = New CDepAsociado
                        Set oDepAsociado.Conexion = mvarConexion
                        Set oDepAsociado.ConexionServer = mvarConexionServer
                                            
                        oDepAsociado.Cod = fldCod.Value
                        oDepAsociado.Den = fldDen.Value
                        oDepAsociado.CodUON1 = fldUon1Cod.Value
                        oDepAsociado.CodUON2 = fldUon2Cod.Value
                        oDepAsociado.CodUON3 = fldUon3Cod.Value
                        oDepAsociado.BajaLog = fldBajaLog.Value
                        oDepAsociado.Indice = lIndice
                        mCol.Add oDepAsociado, CStr(lIndice)
                        Set oDepAsociado = Nothing
                        rs.MoveNext
                        lIndice = lIndice + 1
                    Wend
                Else
                     While Not rs.eof
                        Set oDepAsociado = New CDepAsociado
                        Set oDepAsociado.Conexion = mvarConexion
                        Set oDepAsociado.ConexionServer = mvarConexionServer
                        
                        oDepAsociado.Cod = fldCod.Value
                        oDepAsociado.Den = fldDen.Value
                        oDepAsociado.CodUON1 = fldUon1Cod.Value
                        oDepAsociado.CodUON2 = fldUon2Cod.Value
                        oDepAsociado.CodUON3 = fldUon3Cod.Value
                        oDepAsociado.BajaLog = fldBajaLog.Value
                        mCol.Add oDepAsociado, fldUon1Cod.Value & fldUon2Cod.Value & fldUon3Cod.Value & fldCod.Value
                        Set oDepAsociado = Nothing
                        rs.MoveNext
                    Wend
                End If
            End If
                                    
    End Select

    rs.Close
    Set rs = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CDepAsociados", "CargarTodosLosDepAsociados", ERR, Erl)
        Exit Sub
    End If
End Sub


