VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CConfSegsPedidos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private mCol As Collection
Private m_oConexion As CConexion
Private m_oConexionServer As CConexionDeUseServer
Private m_bEOF As Boolean

Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum


Public Function Add(ByVal sUsuario As String, ByRef oConfSegPedidos As CConfSegPedidos, Optional ByVal vIndice As Variant) As CConfSegPedidos
    If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
                
    Set oConfSegPedidos.Conexion = m_oConexion
       
    If Not IsMissing(vIndice) And Not IsNull(vIndice) Then
        oConfSegPedidos.Indice = vIndice
        mCol.Add oConfSegPedidos, CStr(vIndice)
    Else
        mCol.Add oConfSegPedidos, CStr(sUsuario)
    End If
    
    'return the object created
    Set Add = oConfSegPedidos

    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CConfSegsPedidos", "Add", ERR, Erl)
        Exit Function
    End If
End Function

Friend Property Set Conexion(ByVal con As CConexion)
    Set m_oConexion = con
End Property

Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property

Friend Property Set ConexionServer(ByVal con As CConexionDeUseServer)
    Set m_oConexionServer = con
End Property

Friend Property Get ConexionServer() As CConexionDeUseServer
    Set ConexionServer = m_oConexionServer
End Property

Public Property Get Count() As Long
    If mCol Is Nothing Then
        Count = 0
    Else
         Count = mCol.Count
    End If
End Property

Public Property Get eof() As Boolean
    eof = m_bEOF
End Property

Friend Property Let eof(ByVal b As Boolean)
    m_bEOF = b
End Property

Public Property Get Item(vntIndexKey As Variant) As CConfSegPedidos
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Public Property Get NewEnum() As IUnknown
     Set NewEnum = mCol.[_NewEnum]
End Property

Public Sub Remove(vntIndexKey As Variant)
On Error GoTo ERROR

    mCol.Remove vntIndexKey

ERROR:

End Sub

Private Sub Class_Initialize()
    Set mCol = New Collection
End Sub

Private Sub Class_Terminate()
    'Proceroys collection when this class is terminated
    On Error Resume Next
    Set mCol = Nothing
    Set m_oConexion = Nothing
End Sub


Public Sub CargarConfSegPedidos(ByVal sUsuario As String, ByVal iAbierto As Integer, Optional ByVal UsarIndice As Boolean)
    Dim AdoRes As New adodb.Recordset
    Dim sConsulta As String
    Dim lIndice As Long
    Dim oConfSegPedidos As CConfSegPedidos
    
    
    '********* Precondicion **************************************
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CConfSegsPedidos.CargarConfSegPedidos", "No se ha establecido la conexion"
        Exit Sub
    End If
    '*************************************************************
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

    sConsulta = "SELECT ABIERTO, ANYO, PEDIDO_NUM, ORDEN_NUM, EST, FEC_DESDE," _
    & "FEC_HASTA, NUM_PED_ERP, NUMEXT , TIPOPEDIDO, TIPO, ABONO, PED_ABIERTO_ANYO, PED_ABIERTO_PEDIDO_NUM," _
    & "PED_ABIERTO_ORDEN_NUM, PROVE, COD_PROVE_ERP, GMN1, GMN2, GMN3," _
    & "GMN4,ART_INT,DEST,FECENTREGA_DESDE,FECENTREGA_HASTA,EMPRESA,APROVISIONADOR,RECEPTOR,GESTOR,UON1_CC,UON2_CC,UON3_CC,UON4_CC," _
    & "CAT1,CAT2,CAT3,CAT4,CAT5,IM_AUTOFACTURA,RECEP_AUTOMATICA,TIPO_FACTURACION,FECPLANENTREGA_DESDE," _
    & "FECPLANENTREGA_HASTA,ANYO_FAC,NUM_FAC,EST_FAC,FECFACT_DESDE,FECFACT_HASTA,PAGO_NUM,PAGO_EST,FECPAGO_DESDE,FECPAGO_HASTA,PROCE_ANYO,PROCE_GMN1,PROCE_COD,PROVE_ADJ," _
    & "SOLICIT,PED_ABIERTO_FECINI,PED_ABIERTO_FECFIN,PED_ABIERTO_PORIMPORTE,PED_ABIERTO_PORCANTIDAD,IM_BLOQUEOFACT,VISTA_AVANZADA,VER_PEDIDO_DE_BAJA " _
    & " FROM CONF_SEG_PEDIDOS WITH (NOLOCK) WHERE USU='" & DblQuote(sUsuario) & "' AND ABIERTO=" & iAbierto
    AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly

    If mCol Is Nothing Then
        Set mCol = Nothing
        Set mCol = New Collection
    End If
        
    If Not AdoRes.eof Then
        If UsarIndice Then lIndice = 0

        While Not AdoRes.eof
            Set oConfSegPedidos = New CConfSegPedidos
            With oConfSegPedidos
                .USU = sUsuario
                .Abierto = AdoRes.Fields("ABIERTO").Value
                .Abono = AdoRes.Fields("ABONO").Value
                .Anyo = AdoRes.Fields("ANYO").Value
                .AnyoFac = AdoRes.Fields("ANYO_FAC").Value
                .Aprovisionador = AdoRes.Fields("APROVISIONADOR").Value
                .ArtInt = AdoRes.Fields("ART_INT").Value
                .CodProveErp = AdoRes.Fields("COD_PROVE_ERP").Value
                .Dest = AdoRes.Fields("DEST").Value
                .Empresa = AdoRes.Fields("EMPRESA").Value
                .Est = AdoRes.Fields("EST").Value
                .EstFac = AdoRes.Fields("EST_FAC").Value
                .FecDesde = AdoRes.Fields("FEC_DESDE").Value
                .FecEntregaDesde = AdoRes.Fields("FECENTREGA_DESDE").Value
                .FecEntregaHasta = AdoRes.Fields("FECENTREGA_HASTA").Value
                .FecFacDesde = AdoRes.Fields("FECFACT_DESDE").Value
                .FecFacHasta = AdoRes.Fields("FECFACT_HASTA").Value
                .FecHasta = AdoRes.Fields("FEC_HASTA").Value
                .FecPagoDesde = AdoRes.Fields("FECPAGO_DESDE").Value
                .FecPagoHasta = AdoRes.Fields("FECPAGO_HASTA").Value
                .FecPlanEntregaDesde = AdoRes.Fields("FECPLANENTREGA_DESDE").Value
                .FecPlanEntregaHasta = AdoRes.Fields("FECPLANENTREGA_HASTA").Value
                .Gestor = AdoRes.Fields("GESTOR").Value
                .GMN1 = AdoRes.Fields("GMN1").Value
                .GMN2 = AdoRes.Fields("GMN2").Value
                .GMN3 = AdoRes.Fields("GMN3").Value
                .GMN4 = AdoRes.Fields("GMN4").Value
                .ImAutofactura = AdoRes.Fields("IM_AUTOFACTURA").Value
                .ImBloqueofactura = AdoRes.Fields("IM_BLOQUEOFACT").Value
                .NumExt = AdoRes.Fields("NUMEXT").Value
                .NumFac = AdoRes.Fields("NUM_FAC").Value
                .NumPedErp = AdoRes.Fields("NUM_PED_ERP").Value
                .OrdenNum = AdoRes.Fields("ORDEN_NUM").Value
                .PagoEst = AdoRes.Fields("PAGO_EST").Value
                .PagoNum = AdoRes.Fields("PAGO_NUM").Value
                .PedAbiertoAnyo = AdoRes.Fields("PED_ABIERTO_ANYO").Value
                .PedAbiertoFecFin = AdoRes.Fields("PED_ABIERTO_FECFIN").Value
                .PedAbiertoFecIni = AdoRes.Fields("PED_ABIERTO_FECINI").Value
                .PedAbiertoOrdenNum = AdoRes.Fields("PED_ABIERTO_ORDEN_NUM").Value
                .PedAbiertoPedidoNum = AdoRes.Fields("PED_ABIERTO_PEDIDO_NUM").Value
                .PedAbiertoPorCantidad = AdoRes.Fields("PED_ABIERTO_PORCANTIDAD").Value
                .PedAbiertoPorImporte = AdoRes.Fields("PED_ABIERTO_PORIMPORTE").Value
                .PedidoNum = AdoRes.Fields("PEDIDO_NUM").Value
                .UON1 = AdoRes.Fields("UON1_CC").Value
                .UON2 = AdoRes.Fields("UON2_CC").Value
                .UON3 = AdoRes.Fields("UON3_CC").Value
                .UON4 = AdoRes.Fields("UON4_CC").Value
                .Cat1 = AdoRes.Fields("CAT1").Value
                .Cat2 = AdoRes.Fields("CAT2").Value
                .Cat3 = AdoRes.Fields("CAT3").Value
                .Cat4 = AdoRes.Fields("CAT4").Value
                .Cat5 = AdoRes.Fields("CAT5").Value
                .proceAnyo = AdoRes.Fields("PROCE_ANYO").Value
                .ProceCod = AdoRes.Fields("PROCE_COD").Value
                .ProceGmn1 = AdoRes.Fields("PROCE_GMN1").Value
                .Prove = AdoRes.Fields("PROVE").Value
                .ProveAdj = AdoRes.Fields("PROVE_ADJ").Value
                .RecepAutomatica = AdoRes.Fields("RECEP_AUTOMATICA").Value
                .Receptor = AdoRes.Fields("RECEPTOR").Value
                .Solicit = AdoRes.Fields("SOLICIT").Value
                .Tipo = AdoRes.Fields("TIPO").Value
                .TipoFacturacion = AdoRes.Fields("TIPO_FACTURACION").Value
                .TipoPedido = AdoRes.Fields("TIPOPEDIDO").Value
                .VistaAvanzada = SQLBinaryToBoolean(AdoRes.Fields("VISTA_AVANZADA").Value)
                .VerPedBajaLogica = SQLBinaryToBoolean(AdoRes.Fields("VER_PEDIDO_DE_BAJA").Value)
            End With
            If UsarIndice Then
                Me.Add sUsuario, oConfSegPedidos, lIndice
                lIndice = lIndice + 1
            Else
                Me.Add sUsuario, oConfSegPedidos
            End If
            
            AdoRes.MoveNext
        Wend
    End If
    
    AdoRes.Close
    Set AdoRes = Nothing
    
    Exit Sub
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CConfSegsPedidos", "CargarConfSegPedidos", ERR, Erl)
        Exit Sub
    End If
End Sub







