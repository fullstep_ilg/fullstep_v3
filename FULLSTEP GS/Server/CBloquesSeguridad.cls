VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CBloquesSeguridad"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True

Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum

Private m_Col As Collection
Private m_oConexion As CConexion
Private m_bEOF As Boolean

Public Property Get eof() As Boolean
    eof = m_bEOF
End Property
Friend Property Let eof(ByVal b As Boolean)
    m_bEOF = b
End Property

Public Property Get Item(vntIndexKey As Variant) As CBloqueSeguridad
On Error GoTo NoSeEncuentra:

    Set Item = m_Col(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Friend Property Set Conexion(ByVal con As CConexion)
Set m_oConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = m_oConexion
End Property

Public Property Get Count() As Long

If m_Col Is Nothing Then
    Count = 0
Else
     Count = m_Col.Count
End If


End Property

Public Function Add(ByVal lId As Long, ByVal sDen As String, Optional ByVal dtFecAct As Variant, Optional ByVal vIndice As Variant) As CBloqueSeguridad
    
    'create a new object
    Dim sCod As String
    Dim objnewmember As CBloqueSeguridad
    
    Set objnewmember = New CBloqueSeguridad
    With objnewmember
        Set .Conexion = m_oConexion
    
        .Id = lId
        .Den = sDen
        .FecAct = dtFecAct
    
        If Not IsMissing(vIndice) And Not IsNull(vIndice) Then
            .Indice = vIndice
            m_Col.Add objnewmember, CStr(vIndice)
        Else
            m_Col.Add objnewmember, CStr(lId)
        End If
    
    End With
    
    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing


End Function

Public Sub Remove(vntIndexKey As Variant)
On Error GoTo Error

    m_Col.Remove vntIndexKey

Error:

End Sub


Public Property Get NewEnum() As IUnknown
     Set NewEnum = m_Col.[_NewEnum]
End Property


Private Sub Class_Initialize()
    

    Set m_Col = New Collection
End Sub


Private Sub Class_Terminate()
    'Proceroys collection when this class is terminated
    On Error Resume Next
    Set m_Col = Nothing
    Set m_oConexion = Nothing
    
End Sub

Public Function DevolverBloquesSeguridad(Optional ByVal bConAprobadores As Boolean = True, Optional ByVal lId As Variant, Optional ByVal sDen As Variant) As ADODB.Recordset
Dim oadorecordset As ADODB.Recordset
Dim oADOConnection As ADODB.Connection
Dim sql As String
Dim sOrden As String
Dim oIdiomas As CIdiomas
Dim oIdi As CIdioma
Dim sFrom As String
Dim oGestorParametros As CGestorParametros
Dim stm As ADODB.Stream
Dim rs As ADODB.Recordset



    
    If IsMissing(lId) Then
        lId = Null
    ElseIf lId = "" Then
        lId = Null
    End If
    If IsMissing(sDen) Then
        sDen = Null
    ElseIf sDen = "" Then
        sDen = Null
    End If
    


If bConAprobadores Then
    Set oADOConnection = New ADODB.Connection
    oADOConnection.Open "Provider=MSDataShape.1;Persist Security Info=True;Data Source=" & basParametros.ServidorGS & ";User ID=" & LICDBLogin & ";Password=" & LICDBContra & ";Initial Catalog=" & basParametros.BaseDatosGS & ";Data Provider=SQLOLEDB.1"
    oADOConnection.CursorLocation = adUseClient
    oADOConnection.CommandTimeout = 120
    
    sql = "  SHAPE {SELECT ID, DEN FROM BLOQUE_SEGURIDAD WHERE BAJA = 0 " _
    
    
    If Not IsNull(lId) Then
        sql = sql & " AND ID=" & lId
    End If
    If Not IsNull(sDen) Then
        sql = sql & " AND DEN='" & DblQuote(sDen) & "'"
    End If
    
    sql = sql & "}  AS BS " _
        & "  APPEND ({SELECT A.ID, A.ORDEN, A.BLOQUE_SEGURIDAD, A.PER,A.UON1,A.UON2,A.UON3,A.DEP " _
        & " ,CASE WHEN A.PER IS NOT NULL THEN P.NOM + ' ' + P.APE " _
        & " WHEN A.DEP IS NOT NULL AND A.UON3 IS NOT NULL AND A.UON2 IS NOT NULL AND A.UON1 IS NOT NULL THEN A.UON1 + '-' + A.UON2 + '-' + A.UON3 + '-' + A.DEP + '-' + DEP.DEN" _
        & " WHEN A.DEP IS NOT NULL AND A.UON3 IS NULL AND A.UON2 IS NOT NULL AND A.UON1 IS NOT NULL THEN A.UON1 + '-' + A.UON2 + '-' + A.DEP + '-' + DEP.DEN" _
        & " WHEN A.DEP IS NOT NULL AND A.UON3 IS NULL AND A.UON2 IS NULL AND A.UON1 IS NOT NULL THEN A.UON1 + '-' + A.DEP + '-' + DEP.DEN" _
        & " WHEN A.DEP IS NOT NULL AND A.UON3 IS NULL AND A.UON2 IS NULL AND A.UON1 IS NULL THEN A.DEP + '-' + DEP.DEN" _
        & " WHEN A.UON3 IS NOT NULL THEN A.UON1 + '-' + A.UON2 + '-' + A.UON3 + '-' + UON3.DEN" _
        & " WHEN A.UON2 IS NOT NULL THEN A.UON1 + '-' + A.UON2 + '-' + UON2.DEN " _
        & " WHEN A.UON1 IS NOT NULL THEN A.UON1 + '-' + UON1.DEN END NOMBRE" _
        & ", A.LIMITE FROM APROBADORES A INNER JOIN BLOQUE_SEGURIDAD BS ON A.BLOQUE_SEGURIDAD= BS.ID " _
        & " LEFT JOIN PER P ON A.PER = P.COD " _
        & " LEFT JOIN UON1 ON A.UON1=UON1.COD" _
        & " LEFT JOIN UON2 ON A.UON1=UON2.UON1 AND A.UON2=UON2.COD " _
        & " LEFT JOIN UON3 ON A.UON1=UON3.UON1 AND A.UON2=UON3.UON2 AND A.UON3=UON3.COD " _
        & " LEFT JOIN DEP ON A.DEP=DEP.COD " _
        & " ORDER BY A.ORDEN} AS Command4 RELATE 'ID' TO 'BLOQUE_SEGURIDAD') AS Command4"
    
    Set oadorecordset = New ADODB.Recordset
    oadorecordset.CursorLocation = adUseClient
    oadorecordset.Open sql, oADOConnection, adOpenForwardOnly, adLockBatchOptimistic
    oadorecordset.ActiveConnection = Nothing
    oADOConnection.Close
        
    'Paso el recordset a stream y lo vuelvo a abrir desde ah�.Se hace as� porque sino no se desconecta:
    Set stm = New ADODB.Stream
    oadorecordset.Save stm, adPersistXML
    Set rs = New ADODB.Recordset
    rs.Open stm
    rs.ActiveConnection = Nothing
Else
    sql = "SELECT ID, DEN FROM BLOQUE_SEGURIDAD  WHERE BAJA = 0  "
    
    
    If Not IsNull(lId) Then
        sql = sql & " AND ID=" & lId
    End If
    If Not IsNull(sDen) Then
        sql = sql & " AND DEN='" & DblQuote(sDen) & "'"
    End If
    
    sql = sql & " ORDER BY DEN"
    Set oadorecordset = New ADODB.Recordset
    oadorecordset.CursorLocation = adUseClient
    oadorecordset.Open sql, m_oConexion.ADOCon, adOpenForwardOnly, adLockBatchOptimistic
    oadorecordset.ActiveConnection = Nothing
        
    'Paso el recordset a stream y lo vuelvo a abrir desde ah�.Se hace as� porque sino no se desconecta:
    Set stm = New ADODB.Stream
    oadorecordset.Save stm, adPersistXML
    Set rs = New ADODB.Recordset
    rs.Open stm
    rs.ActiveConnection = Nothing
    
End If
    
Set DevolverBloquesSeguridad = rs

stm.Close
Set stm = Nothing
    
End Function






