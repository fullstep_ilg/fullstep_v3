VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CEnlace"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum


Implements IBaseDatos

Private moConexion As CConexion

Private mlId As Long
Private mlBloqueOrig As Long
Private mlBloqueDest As Long
Private mlAccion As Long
Private moDenAccion As CMultiidiomas
Private miTipoAccion As TipoAccionBloque
Private msFormula As String
Private miBloquea As TipoBloqueoPMEnlace
Private moSubject As CMultiidiomas
Private mdtFecAct As Variant
Private mbLlamadaExterna As Boolean
Private miEstadoFactura As Integer

Private m_oExtraPoints As CEnlaceExtraPoints

Private m_adores As adodb.Recordset

Private mvarIndice As Variant


Friend Property Set Conexion(ByVal con As CConexion)
Set moConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = moConexion
End Property

Public Property Get Id() As Long
    Id = mlId
End Property
Public Property Let Id(ByVal Value As Long)
    mlId = Value
End Property

Public Property Get BloqueOrigen() As Long
    BloqueOrigen = mlBloqueOrig
End Property
Public Property Let BloqueOrigen(ByVal Value As Long)
    mlBloqueOrig = Value
End Property

Public Property Get BloqueDestino() As Long
    BloqueDestino = mlBloqueDest
End Property
Public Property Let BloqueDestino(ByVal Value As Long)
    mlBloqueDest = Value
End Property

Public Property Get Accion() As Long
    Accion = mlAccion
End Property
Public Property Let Accion(ByVal Value As Long)
    mlAccion = Value
End Property

Public Property Get DenAccion() As CMultiidiomas
    Set DenAccion = moDenAccion
End Property
Public Property Set DenAccion(ByVal dato As CMultiidiomas)
    Set moDenAccion = dato
End Property

Public Property Get TipoAccion() As TipoAccionBloque
    TipoAccion = miTipoAccion
End Property
Public Property Let TipoAccion(ByVal Value As TipoAccionBloque)
    miTipoAccion = Value
End Property

Public Property Get Formula() As String
    Formula = msFormula
End Property
Public Property Let Formula(ByVal Value As String)
    msFormula = Value
End Property

Public Property Get Bloquea() As TipoBloqueoPMEnlace
    Bloquea = miBloquea
End Property
Public Property Let Bloquea(ByVal Value As TipoBloqueoPMEnlace)
    miBloquea = Value
End Property

Public Property Get Subject() As CMultiidiomas
    Set Subject = moSubject
End Property
Public Property Set Subject(ByVal dato As CMultiidiomas)
    Set moSubject = dato
End Property

Public Property Let Indice(ByVal varIndice As Variant)
    mvarIndice = varIndice
End Property
Public Property Get Indice() As Variant
    Indice = mvarIndice
End Property

Public Property Get FecAct() As Variant
    FecAct = mdtFecAct
End Property

Public Property Let FecAct(ByVal Data As Variant)
    mdtFecAct = Data
End Property

Public Property Get ExtraPoints() As CEnlaceExtraPoints
    Set ExtraPoints = m_oExtraPoints
End Property

Public Property Set ExtraPoints(ByVal dato As CEnlaceExtraPoints)
    Set m_oExtraPoints = dato
End Property
Public Property Get LlamadaExterna() As Boolean
    LlamadaExterna = mbLlamadaExterna
End Property
Public Property Let LlamadaExterna(ByVal Value As Boolean)
    mbLlamadaExterna = Value
End Property
Public Property Get EstadoFactura() As Integer
    EstadoFactura = miEstadoFactura
End Property
Public Property Let EstadoFactura(ByVal Value As Integer)
    miEstadoFactura = Value
End Property



Private Sub Class_Initialize()
    Set m_oExtraPoints = New CEnlaceExtraPoints
End Sub

Private Sub Class_Terminate()
    Set m_oExtraPoints = Nothing
    Set moDenAccion = Nothing
    Set moConexion = Nothing
End Sub

Public Sub CargarExtraPoints()

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Me.ExtraPoints.Clear
    '''Cargar ExtraPoints
    Set m_adores = New adodb.Recordset
    m_adores.Open "SELECT ORDEN, X, Y FROM PM_ENLACE_EXTRAPOINTS WITH (NOLOCK) WHERE ENLACE = " & mlId & " ORDER BY ORDEN", moConexion.ADOCon, adOpenKeyset, adLockReadOnly

    While Not m_adores.eof
        Me.ExtraPoints.Add NullToDbl0(m_adores("X").Value), NullToDbl0(m_adores("Y").Value), NullToDbl0(m_adores("ORDEN").Value)
        m_adores.MoveNext
    Wend
    m_adores.Close
    Set m_adores = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CEnlace", "CargarExtraPoints", ERR, Erl)
      Exit Sub
   End If
End Sub

Public Sub CargarExtraPointsInstancia()

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Me.ExtraPoints.Clear
    '''Cargar ExtraPoints
    Set m_adores = New adodb.Recordset
    m_adores.Open "SELECT ORDEN, X, Y FROM PM_COPIA_ENLACE_EXTRAPOINTS WITH (NOLOCK) WHERE ENLACE = " & mlId & " ORDER BY ORDEN", moConexion.ADOCon, adOpenKeyset, adLockReadOnly

    While Not m_adores.eof
        Me.ExtraPoints.Add NullToDbl0(m_adores("X").Value), NullToDbl0(m_adores("Y").Value), NullToDbl0(m_adores("ORDEN").Value)
        m_adores.MoveNext
    Wend
    m_adores.Close
    Set m_adores = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CEnlace", "CargarExtraPointsInstancia", ERR, Erl)
      Exit Sub
   End If
End Sub


'' <summary>
''' Revisado por: SRA (15/11/2011)
''' A�ade los enlaces a base de datos
''' </summary>
''' <returns>Devuelve si ha habido o no error</returns>
''' <remarks>Llamada desde=frmFlujosDetalleAccion.frm; Tiempo m�ximo=0,61seg.</remarks>
Private Function IBaseDatos_AnyadirABaseDatos() As TipoErrorSummit
Dim sConsulta As String
Dim TESError As TipoErrorSummit
Dim AdoRes As adodb.Recordset
Dim bTransaccionEnCurso As Boolean
Dim oExtraPoint As CEnlaceExtraPoint
Dim i As Integer
Dim oDen As CMultiidioma


TESError.NumError = TESnoerror

If moConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CEnlace.AnyadirABAseDatos", "No se ha establecido la conexion"
End If

On Error GoTo ERROR
    
    moConexion.ADOCon.Execute "BEGIN DISTRIBUTED TRAN"
    bTransaccionEnCurso = True
    moConexion.ADOCon.Execute "SET XACT_ABORT OFF"
    
    
    'Insetar el Enlace
    sConsulta = "INSERT INTO PM_ENLACE " & _
        "(BLOQUE_ORIGEN, BLOQUE_DESTINO, ACCION, FORMULA, BLOQUEA, EST_FACTURA) " & _
        "VALUES " & _
        "(" & mlBloqueOrig & "," & mlBloqueDest & "," & mlAccion & ","
    If msFormula = "" Then
        sConsulta = sConsulta & "NULL,"
    Else
        sConsulta = sConsulta & "'" & DblQuote(msFormula) & "',"
    End If
    sConsulta = sConsulta & miBloquea & ","
    If miEstadoFactura = 0 Then
        sConsulta = sConsulta & "NULL"
    Else
        sConsulta = sConsulta & miEstadoFactura
    End If
    sConsulta = sConsulta & ")"
    moConexion.ADOCon.Execute sConsulta
    If moConexion.ADOCon.Errors.Count > 0 Then
        GoTo ERROR
    End If
    
    sConsulta = "SELECT MAX(ID) ID FROM PM_ENLACE"
    Set AdoRes = New adodb.Recordset
    AdoRes.Open sConsulta, moConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    mlId = AdoRes("ID").Value
    AdoRes.Close
    Set AdoRes = Nothing
    
    'Insertar los ExtraPoints
    If Not m_oExtraPoints Is Nothing Then
        For Each oExtraPoint In m_oExtraPoints
            sConsulta = "INSERT INTO PM_ENLACE_EXTRAPOINTS " & _
                "(ENLACE, ORDEN, X, Y) " & _
                "VALUES " & _
                "(" & mlId & "," & oExtraPoint.Orden & "," & oExtraPoint.X & "," & oExtraPoint.Y & ")"
            moConexion.ADOCon.Execute sConsulta
            If moConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR
        Next
        Set oExtraPoint = Nothing
    End If
    
    'Insertar el asunto de los mensajes
    If Not moSubject Is Nothing Then
        For Each oDen In moSubject
            sConsulta = "INSERT INTO PM_ENLACE_EMAIL (ENLACE, IDI, SUBJECT) VALUES (" & mlId & ",'" & DblQuote(oDen.Cod) & "','" & DblQuote(oDen.Den) & "')"
            moConexion.ADOCon.Execute sConsulta
            If moConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR
        Next
    End If
    
    ''' Recuperar la fecha de insercion
    sConsulta = "SELECT FECACT FROM PM_ENLACE WITH (NOLOCK) WHERE ID =" & mlId
    Set AdoRes = New adodb.Recordset
    AdoRes.Open sConsulta, moConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    mdtFecAct = AdoRes("FECACT").Value
    AdoRes.Close
    Set AdoRes = Nothing
    
    moConexion.ADOCon.Execute "COMMIT TRAN"
    bTransaccionEnCurso = False
       
    IBaseDatos_AnyadirABaseDatos = TESError
    Exit Function
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
ERROR:

    If moConexion.ADOCon.Errors.Count > 0 Then
                 
        IBaseDatos_AnyadirABaseDatos = basErrores.TratarError(moConexion.ADOCon.Errors)
                     
                     
        If bTransaccionEnCurso Then
            moConexion.ADOCon.Execute "ROLLBACK TRAN"
            bTransaccionEnCurso = False
        End If
        If Not AdoRes Is Nothing Then
            AdoRes.Close
            Set AdoRes = Nothing
        End If
    Else
      Call g_oErrores.TratarError("M�dulo de clase", "CEnlace", "IBaseDatos_AnyadirABaseDatos", ERR, Erl)
    End If

End Function

Private Function IBaseDatos_CambiarCodigo(CodigoNuevo As Variant) As TipoErrorSummit

End Function

Private Sub IBaseDatos_CancelarEdicion()

If Not m_adores Is Nothing Then
    m_adores.Close
    Set m_adores = Nothing
End If

End Sub

Private Function IBaseDatos_ComprobarExistenciaEnBaseDatos() As Boolean

End Function

''' <summary>Elimina un registro de BD</summary>
''' <returns>Variable tipo TipoErrorSummit con el error si se ha producido</returns>
''' <remarks>Llamada desde: FSGSClient; Tiempo m�ximo: 1 sec </remarks>
''' <revision>LTG 04/01/2012</revision>

Private Function IBaseDatos_EliminarDeBaseDatos() As TipoErrorSummit
    Dim TESError As TipoErrorSummit
    Dim btrans As Boolean
    Dim sConsulta As String
    Dim AdoRes As adodb.Recordset
    

    TESError.NumError = TESnoerror
    
    '******************* Precondicion *******************
    If moConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CEnlace.EliminarDeBaseDatos", "No se ha establecido la conexion"
    End If
    '*****************************************************
    
    If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
    
    moConexion.ADOCon.Execute "BEGIN TRAN"
    moConexion.ADOCon.Execute "SET XACT_ABORT OFF"
    btrans = True
    
    sConsulta = "DELETE FROM PM_NOTIFICADO_ENLACE_CAMPOS WHERE NOTIFICADO IN (SELECT ID FROM PM_NOTIFICADO_ENLACE WITH (NOLOCK) WHERE ENLACE=" & mlId & ")" & vbCrLf
    sConsulta = sConsulta & "DELETE FROM PM_NOTIFICADO_ENLACE WHERE ENLACE = " & mlId & vbCrLf
    sConsulta = sConsulta & " DELETE FROM PM_ENLACE_CONDICIONES WHERE ENLACE = " & mlId & vbCrLf
    sConsulta = sConsulta & " DELETE FROM PM_ENLACE_EXTRAPOINTS WHERE ENLACE = " & mlId & vbCrLf
    sConsulta = sConsulta & " DELETE FROM PM_ENLACE_EMAIL WHERE ENLACE=" & mlId & vbCrLf
    sConsulta = sConsulta & " DELETE FROM PM_ENLACE WHERE ID = " & mlId & vbCrLf
        
    moConexion.ADOCon.Execute sConsulta
    If moConexion.ADOCon.Errors.Count > 0 Then
        GoTo ERROR
    End If
    
    moConexion.ADOCon.Execute "COMMIT TRAN"
    btrans = False
    Set m_adores = Nothing
    
    IBaseDatos_EliminarDeBaseDatos = TESError
    
    Exit Function
    
Error_Cls:
    
    If moConexion.ADOCon.Errors.Count > 0 Then
        IBaseDatos_EliminarDeBaseDatos = basErrores.TratarError(moConexion.ADOCon.Errors)
        If btrans Then
            moConexion.ADOCon.Execute "ROLLBACK TRAN"
            btrans = False
        End If
        Exit Function
    End If
    
    If m_adores.EditMode = 1 Then
        m_adores.CancelUpdate
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CEnlace", "IBaseDatos_EliminarDeBaseDatos", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If
End Function

Private Function IBaseDatos_FinalizarEdicionEliminando() As TipoErrorSummit

End Function

''' <summary>Finaliza una edici�n</summary>
''' <returns>Variable tipo TipoErrorSummit con el error si se ha producido</returns>
''' <remarks>Llamada desde: FSGSClient; Tiempo m�ximo: 1 sec </remarks>
''' <revision>LTG 04/01/2012</revision>

Private Function IBaseDatos_FinalizarEdicionModificando() As TipoErrorSummit
    Dim TESError As TipoErrorSummit
    Dim sConsulta As String
    Dim bTransaccionEnCurso As Boolean
    Dim AdoRes As adodb.Recordset
    Dim oExtraPoint As CEnlaceExtraPoint
    Dim i As Integer
    Dim oDen As CMultiidioma
    

    TESError.NumError = TESnoerror
    
    '********* Precondicion *******************
    If moConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CEnlace.FinalizarEdicionModificando", "No se ha establecido la conexion"
    End If
    '******************************************
    
    If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
    If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:

    moConexion.ADOCon.Execute "BEGIN TRAN"
    bTransaccionEnCurso = True
    moConexion.ADOCon.Execute "SET XACT_ABORT OFF"
    
    'Comprobamos si ha habido cambios en otra sesi�n
    Set m_adores = New adodb.Recordset
    'CALIDAD: Sin WITH (NOLOCK) porque es para evitar pisar los cambios de otro
    sConsulta = "SELECT FECACT FROM PM_ENLACE WHERE ID=" & mlId
    m_adores.Open sConsulta, moConexion.ADOCon, adOpenStatic, adLockReadOnly
    
    If m_adores.eof Then
        m_adores.Close
        Set m_adores = Nothing
        TESError.NumError = TESDatoEliminado
        TESError.Arg1 = 178
        IBaseDatos_FinalizarEdicionModificando = TESError
        moConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
        Exit Function
    End If
    
    If mdtFecAct <> m_adores("FECACT").Value Then
        m_adores.Close
        Set m_adores = Nothing
        TESError.NumError = TESInfActualModificada
        moConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
        IBaseDatos_FinalizarEdicionModificando = TESError
        Exit Function
    End If
    
    ''' Actualizar Enlace
    sConsulta = "UPDATE PM_ENLACE SET " & _
        "BLOQUE_ORIGEN = " & mlBloqueOrig & ", " & _
        "BLOQUE_DESTINO = " & mlBloqueDest & ", " & _
        "ACCION = " & mlAccion & ", " & _
        "FORMULA = "
    If msFormula = "" Then
        sConsulta = sConsulta & "NULL, "
    Else
        sConsulta = sConsulta & "'" & DblQuote(msFormula) & "', "
    End If
    sConsulta = sConsulta & "BLOQUEA = " & miBloquea & ", EST_FACTURA ="
    If miEstadoFactura = 0 Then
        sConsulta = sConsulta & "NULL "
    Else
        sConsulta = sConsulta & miEstadoFactura
    End If
        
    sConsulta = sConsulta & " WHERE ID = " & mlId
    moConexion.ADOCon.Execute sConsulta
    If moConexion.ADOCon.Errors.Count > 0 Then
        GoTo ERROR
    End If
    
    '''Borramos todos los ExtraPoint Que tenia
    sConsulta = "DELETE FROM PM_ENLACE_EXTRAPOINTS WHERE ENLACE = " & mlId
    moConexion.ADOCon.Execute sConsulta
    If moConexion.ADOCon.Errors.Count > 0 Then
        GoTo ERROR
    End If
    '''Y Ahora metemos los que tiene en este momento
    For Each oExtraPoint In m_oExtraPoints
        sConsulta = "INSERT INTO PM_ENLACE_EXTRAPOINTS " & _
            "(ENLACE, ORDEN, X, Y) " & _
            "VALUES " & _
            "(" & mlId & "," & oExtraPoint.Orden & "," & oExtraPoint.X & "," & oExtraPoint.Y & ")"
        moConexion.ADOCon.Execute sConsulta
        If moConexion.ADOCon.Errors.Count > 0 Then
            GoTo ERROR
        End If
    Next
    Set oExtraPoint = Nothing
    
    '''Actualizar asunto emails
    If Not moSubject Is Nothing Then
        For Each oDen In moSubject
            sConsulta = "UPDATE PM_ENLACE_EMAIL SET SUBJECT='" & DblQuote(oDen.Den) & "' WHERE ENLACE=" & mlId & " AND IDI='" & DblQuote(oDen.Cod) & "'"
            moConexion.ADOCon.Execute sConsulta
            If moConexion.ADOCon.Errors.Count > 0 Then
                GoTo ERROR
            End If
        Next
    End If
    
    'Recogemos el nuevo valor de FECACT
    m_adores.Requery
    mdtFecAct = m_adores("FECACT").Value
    m_adores.Close
    Set m_adores = Nothing
    
    moConexion.ADOCon.Execute "COMMIT TRAN"
    bTransaccionEnCurso = False
    
    IBaseDatos_FinalizarEdicionModificando = TESError
    
    Exit Function
    

Error_Cls:
    
    IBaseDatos_FinalizarEdicionModificando = basErrores.TratarError(moConexion.ADOCon.Errors)
    
    If Not m_adores Is Nothing Then
        m_adores.Close
        Set m_adores = Nothing
    End If
    
    If bTransaccionEnCurso Then
        moConexion.ADOCon.Execute "ROLLBACK"
        moConexion.ADOCon.Execute "SET XACT_ABORT OFF"
        bTransaccionEnCurso = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CEnlace", "IBaseDatos_FinalizarEdicionModificando", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If

End Function

Private Function IBaseDatos_IniciarEdicion(Optional ByVal Bloquear As Boolean, Optional ByVal UsuarioBloqueo As String) As TipoErrorSummit
Dim TESError As TipoErrorSummit
Dim i As Integer
Dim oSubject As CMultiidiomas

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
TESError.NumError = TESnoerror

'******************* Precondicion *******************
If moConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CEnlace.IniciarEdicion", "No se ha establecido la conexion"
End If
'*****************************************************



'''Cargar Enlace
Set m_adores = New adodb.Recordset
m_adores.Open "SELECT ID, BLOQUE_ORIGEN, BLOQUE_DESTINO, ACCION, FORMULA, BLOQUEA, FECACT, EST_FACTURA FROM PM_ENLACE WITH (NOLOCK) WHERE ID =" & mlId, moConexion.ADOCon, adOpenKeyset, adLockReadOnly

If m_adores.eof Then
        m_adores.Close
        Set m_adores = Nothing
        TESError.NumError = TESDatoEliminado
        TESError.Arg1 = 178
        IBaseDatos_IniciarEdicion = TESError
        Exit Function
End If

mlBloqueOrig = NullToDbl0(m_adores.Fields("BLOQUE_ORIGEN").Value)
mlBloqueDest = NullToDbl0(m_adores.Fields("BLOQUE_DESTINO").Value)
mlAccion = NullToDbl0(m_adores.Fields("ACCION").Value)
msFormula = NullToStr(m_adores.Fields("FORMULA").Value)
miBloquea = NullToDbl0(m_adores.Fields("BLOQUEA").Value)
mdtFecAct = m_adores.Fields("FECACT").Value
miEstadoFactura = NullToDbl0(m_adores.Fields("EST_FACTURA").Value)

m_adores.Close

'''Cargar asunto emails
m_adores.Open "SELECT I.COD AS IDI, ISNULL(EE.SUBJECT,'') AS SUBJECT FROM IDIOMAS I LEFT JOIN PM_ENLACE_EMAIL EE ON EE.IDI=I.COD AND EE.ENLACE=" & mlId, moConexion.ADOCon, adOpenKeyset, adLockReadOnly

If m_adores.eof Then
    m_adores.Close
    Set m_adores = Nothing
    TESError.NumError = TESDatoEliminado
    TESError.Arg1 = 188 ' Asunto del email
    IBaseDatos_IniciarEdicion = TESError
    Exit Function
End If
Set oSubject = New CMultiidiomas
Set oSubject.Conexion = moConexion

While Not m_adores.eof
    oSubject.Add NullToStr(m_adores("IDI").Value), NullToStr(m_adores("SUBJECT").Value)
    m_adores.MoveNext
Wend
Set moSubject = oSubject
m_adores.Close

'''Cargar ExtraPoints
Me.ExtraPoints.Clear
m_adores.Open "SELECT ORDEN, X, Y FROM PM_ENLACE_EXTRAPOINTS WITH (NOLOCK) WHERE ENLACE = " & mlId & " ORDER BY ORDEN", moConexion.ADOCon, adOpenKeyset, adLockReadOnly

While Not m_adores.eof
    Me.ExtraPoints.Add NullToDbl0(m_adores("X").Value), NullToDbl0(m_adores("Y").Value), NullToDbl0(m_adores("ORDEN").Value)
    m_adores.MoveNext
Wend
m_adores.Close
Set m_adores = Nothing

IBaseDatos_IniciarEdicion = TESError
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CEnlace", "IBaseDatos_IniciarEdicion", ERR, Erl)
      Exit Function
   End If
    
End Function

Public Function ObtenerExpiracionMail(ByVal IdAviso As Long) As String

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set m_adores = New adodb.Recordset
    Dim cuentaId As String
    cuentaId = ""
    m_adores.Open "SELECT COUNT(*) AS NUM FROM PM_AVISO_EXPIRACION_EMAIL WITH (NOLOCK) WHERE AVISO_EXPIRACION_CAMPOS = " & IdAviso, moConexion.ADOCon, adOpenKeyset, adLockReadOnly

    If Not m_adores.eof Then
        cuentaId = NullToStr(m_adores("NUM").Value)
    End If
    
    m_adores.Close
    Set m_adores = Nothing
    
    ObtenerExpiracionMail = cuentaId
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CEnlace", "ObtenerExpiracionMail", ERR, Erl)
      Exit Function
   End If
End Function

Public Sub InsertarExpiracionMail(ByVal IdAviso As Long)

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set m_adores = New adodb.Recordset
    
    Dim m_Idi As Variant
    Dim sConsulta As String
    
    For Each m_Idi In Me.Subject
    sConsulta = "INSERT INTO PM_AVISO_EXPIRACION_EMAIL " & _
                "VALUES " & _
                "(" & IdAviso & ",'" & DblQuote(m_Idi.Cod) & "','" & DblQuote(m_Idi.Den) & "')"
    moConexion.ADOCon.Execute sConsulta
    If moConexion.ADOCon.Errors.Count > 0 Then
        GoTo ERROR
    End If
    Next
        
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CEnlace", "InsertarExpiracionMail", ERR, Erl)
      Exit Sub
   End If
End Sub

Public Sub ModificarExpiracionMail(ByVal IdAviso As Long)

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set m_adores = New adodb.Recordset
    
    Dim m_Idi As Variant
    Dim sConsulta As String
    
    For Each m_Idi In Me.Subject
    sConsulta = "UPDATE PM_AVISO_EXPIRACION_EMAIL " & _
                "SET " & _
                "SUBJECT='" & DblQuote(m_Idi.Den) & "' " & _
                "WHERE AVISO_EXPIRACION_CAMPOS=" & IdAviso & " AND IDI='" & DblQuote(m_Idi.Cod) & "'"
    moConexion.ADOCon.Execute sConsulta
    If moConexion.ADOCon.Errors.Count > 0 Then
        GoTo ERROR
    End If
    Next
        
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CEnlace", "ModificarExpiracionMail", ERR, Erl)
      Exit Sub
   End If
End Sub

Public Function DevolverTextosExpiracion(ByVal avisoId As Long, ByVal Idioma As String) As String
   
    Dim salidaDen As String
    Set m_adores = New adodb.Recordset
    
    m_adores.Open "SELECT SUBJECT FROM PM_AVISO_EXPIRACION_EMAIL WHERE AVISO_EXPIRACION_CAMPOS=" & avisoId & " AND IDI='" & Idioma & "'", moConexion.ADOCon, adOpenKeyset, adLockReadOnly

    If Not m_adores.eof Then
        salidaDen = NullToStr(m_adores("SUBJECT").Value)
    End If
    
    m_adores.Close
    Set m_adores = Nothing
    
    DevolverTextosExpiracion = salidaDen

End Function

Public Function ModificarEnlace_GrabarSubject() As TipoErrorSummit
Dim sConsulta As String
Dim TESError As TipoErrorSummit
Dim btrans As Boolean
Dim oSubject As CMultiidioma
Dim AdoRes As adodb.Recordset


If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
    TESError.NumError = TESnoerror
    moConexion.ADOCon.Execute "BEGIN TRANSACTION"
    btrans = True
    
    '''Actualizar los subjects
    If Not moSubject Is Nothing Then
        For Each oSubject In moSubject
            'Se mira en BD si ya existe asunto en ese idioma
            sConsulta = "SELECT SUBJECT FROM PM_ENLACE_EMAIL WITH(NOLOCK) WHERE ENLACE=" & mlId & " AND IDI='" & DblQuote(oSubject.Cod) & "'"
            Set AdoRes = New adodb.Recordset
            AdoRes.Open sConsulta, moConexion.ADOCon, adOpenStatic, adLockReadOnly
            If AdoRes.eof Then
                'AdoRes.eof = True, no existe en BD. Se introduce lo nuevo a�adido
                sConsulta = "INSERT INTO PM_ENLACE_EMAIL (ENLACE, IDI, SUBJECT) VALUES (" & mlId & ",'" & DblQuote(oSubject.Cod) & "','" & DblQuote(oSubject.Den) & "')"
                moConexion.ADOCon.Execute sConsulta
                If moConexion.ADOCon.Errors.Count > 0 Then
                    GoTo ERROR
                End If
            Else
                'Ya existia en BD, bien vacio bien con valor
                sConsulta = "UPDATE PM_ENLACE_EMAIL SET SUBJECT = '" & DblQuote(oSubject.Den) & "' WHERE ENLACE=" & mlId & " AND IDI = '" & DblQuote(oSubject.Cod) & "'"
                moConexion.ADOCon.Execute sConsulta
                If moConexion.ADOCon.Errors.Count > 0 Then
                    GoTo ERROR
                End If
            End If
            AdoRes.Close
            Set AdoRes = Nothing
        Next
    End If
    
    moConexion.ADOCon.Execute "COMMIT TRANSACTION"
    btrans = False
    ModificarEnlace_GrabarSubject = TESError
    Exit Function
    
Error_Cls:

    If Not AdoRes Is Nothing Then
        AdoRes.Close
        Set AdoRes = Nothing
    End If

    ModificarEnlace_GrabarSubject = basErrores.TratarError(moConexion.ADOCon.Errors)
    If btrans Then
        moConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CEnlace", "ModificarEnlace_GrabarSubject", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If
End Function
