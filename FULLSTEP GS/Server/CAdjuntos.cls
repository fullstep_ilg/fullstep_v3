VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CAdjuntos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CAdjuntos ********************************
'*             Autor : Hilario Barrenkua
'*             Creada : 20-11-2000
'****************************************************************

Option Explicit

Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum

Private mCol As Collection 'Contendra los archivos adjuntos de una oferta
Private m_oConexion As CConexion
Private m_lSize As Long

Public Property Get Item(vntIndexKey As Variant) As CAdjunto
    On Error GoTo NoSeEncuentra:
    Set Item = mCol(vntIndexKey)
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
End Property

Friend Property Set Conexion(ByVal con As CConexion)
    Set m_oConexion = con
End Property

Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property

Public Property Get Size() As Long
    Size = m_lSize
End Property

Public Property Get Count() As Long
    If mCol Is Nothing Then
        Count = 0
    Else
        Count = mCol.Count
    End If
End Property

Public Function Add(ByVal Id As Variant, _
                    ByVal Nombre As String, _
                    ByVal DataSize As Long, _
                    Optional ByVal Comentario As Variant, _
                    Optional ByVal IDPortal As Variant, _
                    Optional ByVal vIndice As Variant, _
                    Optional ByVal sRuta As String = "", _
                    Optional eTipo As TipoAdjunto) As CAdjunto
    'create a new object
    Dim sCod As String
    Dim objnewmember As CAdjunto
If Not g_oErrores.fg_bProgramando Then On Error GoTo Error
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New CAdjunto
   
    With objnewmember
   
        .Id = Id
        .Nombre = Nombre
        .DataSize = DataSize
    
        m_lSize = m_lSize + .DataSize
    
        If IsMissing(Comentario) Then
            .Comentario = Null
        Else
            .Comentario = Comentario
        End If
        
        If IsMissing(IDPortal) Then
            .IDPortal = Null
        Else
            .IDPortal = IDPortal
        End If
        .Ruta = sRuta
        .Tipo = eTipo
        Set .Conexion = m_oConexion
            
    End With
    
    If Not IsMissing(vIndice) And Not IsNull(vIndice) Then
        objnewmember.Indice = vIndice
        mCol.Add objnewmember, CStr(vIndice)
    Else
        mCol.Add objnewmember, CStr(Id)
    End If
    
    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CAdjuntos", "Add", ERR, Erl)
      Exit Function
   End If

End Function

Public Sub Remove(vntIndexKey As Variant)
    m_lSize = m_lSize - mCol(vntIndexKey).DataSize
    mCol.Remove vntIndexKey
End Sub


Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
    Set NewEnum = mCol.[_NewEnum]
End Property

Private Sub Class_Initialize()
    m_lSize = 0
    Set mCol = New Collection
End Sub

Private Sub Class_Terminate()
    'EspProceroys collection when this class is terminated
    Set mCol = Nothing
    Set m_oConexion = Nothing
End Sub

''' <summary>Carga los datos relativos a los archivos adjuntos de una determinada oferta de un proveedor para un proceso. Los adjuntos
''' pueden estar a nivel de proceso, de grupo o de item; como dependi�ndo de a qu� nivel est�n se guardan los datos en tablas
''' diferentes, entonces se hace una uni�n de sentencias de selecci�n.</summary>
''' <param name="iAnyo">A�o<param>
''' <param name="sGMN1">GMN1<param>
''' <param name="iProceCod">Cod. proceso<param>
''' <param name="sProveCod">Cod. proveedor<param>
''' <param name="iNumOfe">Num. oferta<param>
''' <param name="lGrupo">Id. grupo<param>
''' <remarks>Llamada desde: FSGSClient; Tiempo m�ximo: < 1 sec </remarks>
''' <revision>LTG 02/01/2012</revision>

Public Sub CargarTodosLosAdjuntosOferta(ByVal iAnyo As Integer, ByVal sGMN1 As String, ByVal iProceCod As Long, ByVal sProveCod As String, _
        Optional ByVal iNumOfe As Integer, Optional ByVal lGrupo As Long)
    Dim adores_RS As ADODB.Recordset
    Dim sConsulta As String
    Dim iIndice As Integer
        
If Not g_oErrores.fg_bProgramando Then On Error GoTo Error
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sConsulta = ""
    sConsulta = sConsulta & "SELECT NULL AS IDGRUPO, NULL AS COD, NULL AS DEN, NOM, ID, IDPORTAL, COM, DATASIZE, ANYO, GMN1, PROCE, PROVE, OFE, 1 AS TIPO_ADJ, NULL AS ITEM_ID "
    sConsulta = sConsulta & "FROM PROCE_OFE_ADJUN WITH (NOLOCK) "
    sConsulta = sConsulta & "WHERE ANYO=" & iAnyo & " AND GMN1='" & DblQuote(sGMN1) & "' AND PROCE=" & iProceCod & " AND"
    sConsulta = sConsulta & "      PROVE='" & DblQuote(sProveCod) & "'"
    If Not IsMissing(iNumOfe) And iNumOfe <> 0 Then
        sConsulta = sConsulta & "      AND PROCE_OFE_ADJUN.OFE=" & iNumOfe
    End If
    sConsulta = sConsulta & " "
    sConsulta = sConsulta & "UNION"
    sConsulta = sConsulta & " "
    sConsulta = sConsulta & "SELECT PROCE_GRUPO.ID AS IDGRUPO, PROCE_GRUPO.COD, PROCE_GRUPO.DEN AS DEN, OFE_GR_ADJUN.NOM, OFE_GR_ADJUN.ID, OFE_GR_ADJUN.IDPORTAL, OFE_GR_ADJUN.COM, OFE_GR_ADJUN.DATASIZE, OFE_GR_ADJUN.ANYO, OFE_GR_ADJUN.GMN1, OFE_GR_ADJUN.PROCE, OFE_GR_ADJUN.PROVE, OFE_GR_ADJUN.OFE, 2 AS TIPO_ADJ, NULL AS ITEM_ID "
    sConsulta = sConsulta & "FROM OFE_GR_ADJUN WITH (NOLOCK) "
    sConsulta = sConsulta & "INNER JOIN PROCE_GRUPO WITH (NOLOCK) ON PROCE_GRUPO.ANYO=OFE_GR_ADJUN.ANYO AND"
    sConsulta = sConsulta & "                          PROCE_GRUPO.GMN1=OFE_GR_ADJUN.GMN1 AND"
    sConsulta = sConsulta & "                          PROCE_GRUPO.PROCE=OFE_GR_ADJUN.PROCE AND"
    sConsulta = sConsulta & "                          PROCE_GRUPO.ID=OFE_GR_ADJUN.GRUPO "
    sConsulta = sConsulta & "WHERE OFE_GR_ADJUN.ANYO=" & iAnyo & " AND OFE_GR_ADJUN.GMN1='" & DblQuote(sGMN1) & "' AND"
    sConsulta = sConsulta & "      OFE_GR_ADJUN.PROCE=" & iProceCod & " AND OFE_GR_ADJUN.PROVE='" & DblQuote(sProveCod) & "'"
    If Not IsMissing(iNumOfe) And iNumOfe <> 0 Then
        sConsulta = sConsulta & "      AND OFE_GR_ADJUN.OFE=" & iNumOfe
    End If
    If Not IsMissing(lGrupo) And lGrupo > 0 Then
        sConsulta = sConsulta & "      AND OFE_GR_ADJUN.GRUPO=" & (lGrupo)
    End If
    
    sConsulta = sConsulta & " "
    sConsulta = sConsulta & "UNION"
    sConsulta = sConsulta & " "
    sConsulta = sConsulta & "SELECT NULL AS IDGRUPO, ITEM.ART AS COD, ITEM.DESCR AS DEN, OFE_ITEM_ADJUN.NOM, OFE_ITEM_ADJUN.ID, OFE_ITEM_ADJUN.IDPORTAL, OFE_ITEM_ADJUN.COM, OFE_ITEM_ADJUN.DATASIZE, OFE_ITEM_ADJUN.ANYO, OFE_ITEM_ADJUN.GMN1, OFE_ITEM_ADJUN.PROCE, OFE_ITEM_ADJUN.PROVE, OFE_ITEM_ADJUN.OFE, 3 AS TIPO_ADJ, OFE_ITEM_ADJUN.ITEM AS ITEM_ID "
    sConsulta = sConsulta & "FROM OFE_ITEM_ADJUN  WITH (NOLOCK) "
    sConsulta = sConsulta & "INNER JOIN ITEM WITH (NOLOCK) ON OFE_ITEM_ADJUN.ANYO=ITEM.ANYO AND OFE_ITEM_ADJUN.PROCE=ITEM.PROCE AND"
    sConsulta = sConsulta & "                   OFE_ITEM_ADJUN.GMN1=ITEM.GMN1_PROCE AND OFE_ITEM_ADJUN.ITEM=ITEM.ID "
    sConsulta = sConsulta & "WHERE OFE_ITEM_ADJUN.ANYO=" & iAnyo & " AND OFE_ITEM_ADJUN.GMN1='" & DblQuote(sGMN1) & "' AND"
    sConsulta = sConsulta & "      OFE_ITEM_ADJUN.PROCE=" & iProceCod & " AND OFE_ITEM_ADJUN.PROVE='" & DblQuote(sProveCod) & "'"
    If Not IsMissing(iNumOfe) And iNumOfe <> 0 Then
        sConsulta = sConsulta & "      AND OFE_ITEM_ADJUN.OFE=" & iNumOfe
    End If
    If Not IsMissing(lGrupo) And lGrupo > 0 Then
        sConsulta = sConsulta & "      AND ITEM.GRUPO=" & lGrupo
    End If
    sConsulta = sConsulta & " "
    sConsulta = sConsulta & " ORDER BY TIPO_ADJ "
    iIndice = 1
    Set adores_RS = New ADODB.Recordset
    adores_RS.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    While Not adores_RS.eof
        Me.Add adores_RS.Fields.Item("ID").Value, adores_RS.Fields.Item("NOM").Value, adores_RS.Fields.Item("DATASIZE").Value, adores_RS.Fields.Item("COM").Value, adores_RS.Fields.Item("IDPORTAL").Value, iIndice
        Set Me.Item(iIndice).Oferta = New COferta
        
        With Me.Item(iIndice).Oferta
            .Anyo = adores_RS.Fields.Item("ANYO").Value
            .GMN1Cod = adores_RS.Fields.Item("GMN1").Value
            .Proce = adores_RS.Fields.Item("PROCE").Value
            .Prove = adores_RS.Fields.Item("PROVE").Value
            .Num = adores_RS.Fields.Item("OFE").Value
        End With
        
        Select Case adores_RS.Fields.Item("TIPO_ADJ").Value
            Case 1 ' Ambito = proceso
                Set Me.Item(iIndice).PrecioItem = Nothing
                Set Me.Item(iIndice).Grupo = Nothing
            Case 2 ' Ambito = grupo
                Set Me.Item(iIndice).PrecioItem = Nothing
                Set Me.Item(iIndice).Grupo = New CGrupo
                Me.Item(iIndice).Grupo.Id = adores_RS.Fields.Item("IDGRUPO").Value
                Me.Item(iIndice).Grupo.Codigo = adores_RS.Fields.Item("COD").Value
                Me.Item(iIndice).Grupo.Den = adores_RS.Fields.Item("DEN").Value
            Case 3 ' Ambito = item
                Set Me.Item(iIndice).PrecioItem = New CPrecioItem
                Me.Item(iIndice).PrecioItem.Id = adores_RS.Fields.Item("ITEM_ID").Value
                Me.Item(iIndice).PrecioItem.ArticuloCod = adores_RS.Fields.Item("COD").Value
                Me.Item(iIndice).PrecioItem.Descr = adores_RS.Fields.Item("DEN").Value
        End Select
        
        adores_RS.MoveNext
        iIndice = iIndice + 1
    Wend
    
    adores_RS.Close
    Set adores_RS = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CAdjuntos", "CargarTodosLosAdjuntosOferta", ERR, Erl)
      Exit Sub
   End If
End Sub
