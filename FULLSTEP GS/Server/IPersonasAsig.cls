VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "IPersonasAsig"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True

Public Function AsignarPersonas(ByVal opers As CPersonas) As TipoErrorSummit

End Function

Public Function AsignarPersona(ByVal CodPer As String, ByVal CodRol As String, Optional ByVal Invitado As Boolean) As TipoErrorSummit

End Function
Public Function CambiarRol(ByVal CodPer As String, ByVal CodRolNuevo As String, ByVal CodRolViejo As String, Optional ByVal Invitado As Boolean) As TipoErrorSummit

End Function
Public Function CambiarNotificacion(ByVal Notificado As Boolean, ByVal CodPer As String) As TipoErrorSummit

End Function
Public Function DesAsignarPersona(ByVal CodPer As String, ByVal CodRol As String) As TipoErrorSummit

End Function

Public Function DevolverTodasLasPersonasAsignadas(ByVal CriterioOrdenacion As TipoOrdenacionPersonas, Optional ByVal UsarIndice As Boolean) As CPersonas

End Function

Public Property Get Personas() As CPersonas

End Property

Public Property Set Personas(ByVal opers As CPersonas)
    
End Property
