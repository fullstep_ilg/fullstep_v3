VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CLiteral"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Private mvarId As Integer
Private mvarDen As String
Private mvarIdioma As String

Public Property Let ID(ByVal Data As Integer)
    Let mvarId = Data
End Property

Public Property Get ID() As Integer
    ID = mvarId
End Property

Public Property Let Den(ByVal Data As String)
    Let mvarDen = Data
End Property

Public Property Get Den() As String
    Den = mvarDen
End Property

Public Property Let Idioma(ByVal Data As String)
    Let mvarIdioma = Data
End Property

Public Property Get Idioma() As String
    Idioma = mvarIdioma
End Property


