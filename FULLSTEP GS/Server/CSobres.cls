VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CSobres"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CSobres **********************************
'*             Autor : epb
'*             Creada : 13/01/2003
'***************************************************************

Option Explicit

Private m_oConexion As CConexion
Private m_bEOF As Boolean
Private mCol As Collection

Public Function Add(ByVal oProce As cProceso, ByVal iSobre As Integer, Optional ByVal vFechaCry As Variant, Optional ByVal vFechaReal As Variant, Optional ByVal sUsu As String, _
Optional ByVal vIndice As Variant, Optional ByVal vEstCry As Variant, Optional ByVal vObs As Variant, Optional ByVal vFecAct As Variant, Optional ByVal vFechaPrev As Variant, Optional ByVal vEstado As Variant) As CSobre
    
    'create a new object
    Dim objnewmember As CSobre
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New CSobre
   
    Set objnewmember.Proceso = oProce
    objnewmember.Sobre = iSobre
    If Not IsMissing(vFechaCry) Then
        objnewmember.FechaEncriptada = vFechaCry
        objnewmember.FechaAperturaPrevista = Fecha(EncriptarAperturaSobre(CStr(oProce.Anyo) & CStr(oProce.Cod) & CStr(iSobre), vFechaCry, False), "dd/mm/yyyy")
    ElseIf Not IsMissing(vFechaPrev) Then
        objnewmember.FechaAperturaPrevista = vFechaPrev
    Else
        objnewmember.FechaEncriptada = ""
        objnewmember.FechaAperturaPrevista = Null
    End If
    If Not IsMissing(vEstCry) Then
        objnewmember.EstadoEncriptado = vEstCry
        
        Select Case EncriptarAperturaSobre(CStr(oProce.Anyo) & CStr(oProce.Cod) & CStr(iSobre), vEstCry, False)
        Case "Abierto"
            objnewmember.Estado = 1
        Case "Cerrado"
            objnewmember.Estado = 0
        Case Else
            objnewmember.Estado = 99
        End Select
    ElseIf Not IsMissing(vEstado) Then
        objnewmember.Estado = vEstado
    Else
        objnewmember.Estado = 0
    End If
    If IsNull(objnewmember.FechaAperturaPrevista) Then
        objnewmember.Estado = 99
        objnewmember.EstadoEncriptado = ""
    End If
    If objnewmember.Estado = 99 Then
        objnewmember.FechaEncriptada = ""
        objnewmember.FechaAperturaPrevista = Null
    End If
    objnewmember.Usuario = sUsu
    
    Set objnewmember.Conexion = m_oConexion
    
    If IsMissing(vFechaReal) Then
        objnewmember.FechaAperturaReal = Null
    Else
        objnewmember.FechaAperturaReal = vFechaReal
    End If
    
    If IsMissing(vObs) Then
        objnewmember.Observaciones = Null
    Else
        objnewmember.Observaciones = vObs
    End If
        
    If IsMissing(vFecAct) Then
        objnewmember.FecAct = Null
    Else
        objnewmember.FecAct = vFecAct
    End If
    
    If Not IsMissing(vIndice) And Not IsNull(vIndice) Then
        objnewmember.Indice = vIndice
        mCol.Add objnewmember, CStr(vIndice)
    Else
        objnewmember.Indice = mCol.Count
        mCol.Add objnewmember, CStr(iSobre)
    End If
    
    
    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CSobres", "Add", ERR, Erl)
      Exit Function
   End If

End Function

Friend Property Set Conexion(ByVal con As CConexion)
    Set m_oConexion = con
End Property

Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property

Public Property Get Count() As Long
    If mCol Is Nothing Then
        Count = 0
    Else
         Count = mCol.Count
    End If
End Property

Public Property Get eof() As Boolean
    eof = m_bEOF
End Property

Friend Property Let eof(ByVal b As Boolean)
    m_bEOF = b
End Property

Public Property Get Item(vntIndexKey As Variant) As CSobre
Attribute Item.VB_MemberFlags = "200"
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
     Set NewEnum = mCol.[_NewEnum]
End Property

Public Sub Remove(vntIndexKey As Variant)

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    mCol.Remove vntIndexKey
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CSobres", "Remove", ERR, Erl)
      Exit Sub
   End If

End Sub

Private Sub Class_Initialize()
    Set mCol = New Collection
End Sub

Private Sub Class_Terminate()
    'Proceroys collection when this class is terminated
    On Error Resume Next
    Set mCol = Nothing
    Set m_oConexion = Nothing
End Sub

