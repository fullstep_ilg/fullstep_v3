VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CCondicionSolicitud"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements IBaseDatos

Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum

Private moConexion As CConexion
Private mlID As Long
Private mlSolicitud As Long
Private msCod As String
Private mlCampo As Long
Private msOperador As String
Private miTipoCampo As TipoCampoCondicionEnlace
Private miTipoValor As TipoValorCondicionEnlace
Private mvCampoValor As Variant
Private mvValor As Variant
Private mdtFecAct As Variant
Private mvarIndice As Variant
Private m_oCampo As CFormItem
Private m_oCampoValor As CFormItem

Friend Property Set Conexion(ByVal con As CConexion)
    Set moConexion = con
End Property
Friend Property Get Conexion() As CConexion
    Set Conexion = moConexion
End Property

Public Property Get Id() As Long
    Id = mlID
End Property
Public Property Let Id(ByVal Data As Long)
    mlID = Data
End Property

Public Property Get Cod() As String
    Cod = msCod
End Property
Public Property Let Cod(ByVal Data As String)
    msCod = Data
End Property

Public Property Get Campo() As Long
    Campo = mlCampo
End Property
Public Property Let Campo(ByVal Data As Long)
    mlCampo = Data
End Property

Public Property Get Operador() As String
    Operador = msOperador
End Property
Public Property Let Operador(ByVal Data As String)
    msOperador = Data
End Property

Public Property Get TipoCampo() As TipoCampoCondicionEnlace
    TipoCampo = miTipoCampo
End Property
Public Property Let TipoCampo(ByVal Data As TipoCampoCondicionEnlace)
    miTipoCampo = Data
End Property

Public Property Get TipoValor() As TipoValorCondicionEnlace
    TipoValor = miTipoValor
End Property
Public Property Let TipoValor(ByVal Data As TipoValorCondicionEnlace)
    miTipoValor = Data
End Property

Public Property Get CampoValor() As Variant
    CampoValor = mvCampoValor
End Property
Public Property Let CampoValor(ByVal Data As Variant)
    mvCampoValor = Data
End Property

Public Property Get Valor() As Variant
    Valor = mvValor
End Property
Public Property Let Valor(ByVal Data As Variant)
    mvValor = Data
End Property

Public Property Get FecAct() As Variant
    FecAct = mdtFecAct
End Property
Public Property Let FecAct(ByVal Data As Variant)
    mdtFecAct = Data
End Property

Public Property Let Indice(ByVal varIndice As Variant)
    mvarIndice = varIndice
End Property
Public Property Get Indice() As Variant
    Indice = mvarIndice
End Property

Public Property Get CampoForm() As CFormItem
    If m_oCampo Is Nothing Then cargarCampo
    Set CampoForm = m_oCampo
End Property
Public Property Set CampoForm(ByRef oCampo As CFormItem)
    Set m_oCampo = oCampo
End Property

Public Property Get CampoValorForm() As CFormItem
    Set CampoValorForm = m_oCampoValor
End Property
Public Property Set CampoValorForm(ByRef oCampoValor As CFormItem)
    Set m_oCampoValor = oCampoValor
End Property

Private Sub Class_Terminate()
    Set moConexion = Nothing
    Set m_oCampo = Nothing
End Sub

Private Function IBaseDatos_AnyadirABaseDatos() As TipoErrorSummit
End Function

Private Function IBaseDatos_CambiarCodigo(CodigoNuevo As Variant) As TipoErrorSummit
End Function

Private Sub IBaseDatos_CancelarEdicion()
End Sub

Private Function IBaseDatos_ComprobarExistenciaEnBaseDatos() As Boolean
End Function

Private Function IBaseDatos_EliminarDeBaseDatos() As TipoErrorSummit
End Function

Private Function IBaseDatos_FinalizarEdicionEliminando() As TipoErrorSummit
End Function

Private Function IBaseDatos_FinalizarEdicionModificando() As TipoErrorSummit
End Function

Private Function IBaseDatos_IniciarEdicion(Optional ByVal Bloquear As Boolean, Optional ByVal UsuarioBloqueo As String) As TipoErrorSummit
End Function

''' <summary>Carga el objeto campo</summary>
''' <remarks>Llamada desde: CampoForm</remarks>

Public Sub cargarCampo()
    Set m_oCampo = New CFormItem
    Set m_oCampo.Conexion = Me.Conexion
    m_oCampo.Id = mlCampo
    m_oCampo.CargarDatosFormCampo
End Sub

''' <summary>Indica si el campo es de tipo importe</summary>
''' <remarks>Llamada desde: frmCertifCondicionOtroCertif</remarks>

Public Function incluyeCampoImporte() As Boolean
    incluyeCampoImporte = CampoForm.esTipoImporteoCalculado
End Function
