VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CIdiomas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Private mCol As Collection
Public Function Add(ByVal Cod As String, ByVal Den As String, Optional ByVal Offset As Variant) As CIdioma
   Dim objnewmember As CIdioma
   
   Set objnewmember = New CIdioma
    
   objnewmember.Cod = Cod
   objnewmember.Den = Den
   
   If Not IsMissing(Offset) Then
        objnewmember.Offset = Offset
    Else
        objnewmember.Offset = Null
    End If
    

   
   mCol.Add objnewmember, Cod
   
End Function
Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
     Count = mCol.Count
End If


End Property
Public Property Get Item(vntIndexKey As Variant) As CIdioma
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property
Public Sub Remove(vntIndexKey As Variant)
    
        mCol.Remove vntIndexKey
    
End Sub
Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
     Set NewEnum = mCol.[_NewEnum]
End Property

Private Sub Class_Initialize()
    
    Set mCol = New Collection

End Sub
