VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CHitoFact"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum
    
Implements IBaseDatos

Private m_oConexion As CConexion
Private m_lId As Long
Private m_dFecha As Date
Private m_sDen As String
Private m_sClase As String
Private m_iTipoPlan As Integer
Private m_vPorcen As Variant
Private m_dblValor As Double
Private m_dFecAct As Date
Private m_dblValorMonProce As Double
Private m_iAccionEdicion As TipoAccionEdicion
Private m_bEditable As Boolean

''' <summary>
''' Establecer el valor de m_oConexion
''' </summary>
''' <param name="Data">Valor de la propiedad</param>
''' <remarks>Llamada desde: CImpuestos/Add ; Tiempo m�ximo: 0</remarks>
Friend Property Set Conexion(ByVal con As CConexion)
Set m_oConexion = con
End Property
''' <summary>
''' Obtener el valor de m_oConexion
''' </summary>
''' <remarks>Llamada desde: Nadie, usan la privada IBaseDatos_AnyadirABaseDatos y IBaseDatos_FinalizarEdicionModificando ; Tiempo m�ximo: 0</remarks>
Friend Property Get Conexion() As CConexion
Set Conexion = m_oConexion
End Property

Private Sub Class_Initialize()
    m_bEditable = True
End Sub

''' <summary>
''' destroys collection when this class is terminated
''' </summary>
''' <remarks>Llamada desde: sistema ; Tiempo m�ximo: 0</remarks>
Private Sub Class_Terminate()
    Set m_oConexion = Nothing
End Sub

''' ID del Hito
Public Property Let Id(ByVal dato As Long)
    m_lId = dato
End Property
''' ID del Hito
Public Property Get Id() As Long
    Id = m_lId
End Property

''' Fecha del Hito
Public Property Let Fecha(ByVal dato As Date)
    m_dFecha = dato
End Property
''' Fecha del Hito
Public Property Get Fecha() As Date
    Fecha = m_dFecha
End Property

''' Denominaci�n del Hito
Public Property Let Den(ByVal dato As String)
    m_sDen = dato
End Property
''' Denominaci�n del Hito
Public Property Get Den() As String
    Den = m_sDen
End Property

''' Clase del Hito
Public Property Let Clase(ByVal dato As String)
    m_sClase = dato
End Property
''' Clase del Hito
Public Property Get Clase() As String
    Clase = m_sClase
End Property

''' Tipo Plan del Hito
Public Property Let TipoPlan(ByVal dato As Integer)
    m_iTipoPlan = dato
End Property
''' Tipo Plan del Hito
Public Property Get TipoPlan() As Integer
    TipoPlan = m_iTipoPlan
End Property

''' Porcentaje del Hito
Public Property Let Porcen(ByVal dato As Variant)
    m_vPorcen = dato
End Property
''' Porcentaje del Hito
Public Property Get Porcen() As Variant
    Porcen = m_vPorcen
End Property

''' Valor del Hito
Public Property Let Valor(ByVal dato As Double)
    m_dblValor = dato
End Property
''' Valor del Hito
Public Property Get Valor() As Double
    Valor = m_dblValor
End Property
''' Valor del Hito en Monedad el proceso
Public Property Let ValorMonProce(ByVal dato As Double)
    m_dblValorMonProce = dato
End Property
''' Valor del Hito
Public Property Get ValorMonProce() As Double
    ValorMonProce = m_dblValorMonProce
End Property

''' Fecha ultima actualizaci�n del Hito
Public Property Let FecAct(ByVal dato As Date)
    m_dFecAct = dato
End Property
''' Fecha ultima actualizaci�n del Hito
Public Property Get FecAct() As Date
    FecAct = m_dFecAct
End Property

Public Property Let AccionEdicion(ByVal dato As TipoAccionEdicion)
    m_iAccionEdicion = dato
End Property
Public Property Get AccionEdicion() As TipoAccionEdicion
    AccionEdicion = m_iAccionEdicion
End Property

Public Property Let Editable(ByVal dato As Boolean)
    m_bEditable = dato
End Property
Public Property Get Editable() As Boolean
    Editable = m_bEditable
End Property

''' <summary>Esta funci�n a�ade el Hito de facturaci�n a la l�nea de pedido </summary>
''' <returns>TipoErrorSummit con el error si lo hay</returns>
''' <remarks>Llamada desde: FRMPEDIDO
''' Tiempo m�ximo: 0,1 sec </remarks>

Private Function IBaseDatos_AnyadirABaseDatos() As TipoErrorSummit
    Dim sConsulta As String
    
    Dim TESError As TipoErrorSummit

    On Error GoTo Error_Cls:
    
    TESError.NumError = TESnoerror
    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
    
    
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    IBaseDatos_AnyadirABaseDatos = TESError
    
    Exit Function
Error_Cls:
    IBaseDatos_AnyadirABaseDatos = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
End Function

Private Function IBaseDatos_CambiarCodigo(CodigoNuevo As Variant) As TipoErrorSummit
End Function

Private Sub IBaseDatos_CancelarEdicion()
End Sub

Private Function IBaseDatos_ComprobarExistenciaEnBaseDatos() As Boolean
End Function

Private Function IBaseDatos_EliminarDeBaseDatos() As TipoErrorSummit
End Function

Private Function IBaseDatos_FinalizarEdicionEliminando() As TipoErrorSummit
End Function

Private Function IBaseDatos_FinalizarEdicionModificando() As TipoErrorSummit
End Function

Private Function IBaseDatos_IniciarEdicion(Optional ByVal Bloquear As Boolean, Optional ByVal UsuarioBloqueo As String) As TipoErrorSummit
End Function

Public Function InsertarHitoFacturacion(ByVal lIDLinea As Long) As TipoErrorSummit
    Dim oCom As ADODB.Command
    Dim oParam As ADODB.Parameter
    Dim TESError As TipoErrorSummit
    
    On Error GoTo TratarError
    
    TESError.NumError = TESnoerror
    
    Set oCom = New ADODB.Command
    With oCom
        Set .ActiveConnection = m_oConexion.ADOCon
        .CommandType = adCmdStoredProc
        .CommandText = "FSGS_INSERTAR_LIN_PED_HITO_FAC"
        Set oParam = .CreateParameter("LINEA", adInteger, adParamInput, , lIDLinea)
        .Parameters.Append oParam
        Set oParam = .CreateParameter("FECHA", adDBDate, adParamInput, , m_dFecha)
        .Parameters.Append oParam
        Set oParam = .CreateParameter("DEN", adVarChar, adParamInput, Len(m_sDen), m_sDen)
        .Parameters.Append oParam
        Set oParam = .CreateParameter("CLASE", adVarChar, adParamInput, Len(m_sClase), m_sClase)
        .Parameters.Append oParam
        Set oParam = .CreateParameter("TIPO_PLAN", adTinyInt, adParamInput, , m_iTipoPlan)
        .Parameters.Append oParam
        Set oParam = .CreateParameter("PORCEN", adDouble, adParamInput, , IIf(m_iTipoPlan = TipoPlanHitoFac.AnticipoParcialPorcentual Or m_iTipoPlan = TipoPlanHitoFac.FacturacionParcialPorcentual, m_vPorcen, Null))
        .Parameters.Append oParam
        Set oParam = .CreateParameter("VALOR", adDouble, adParamInput, , m_dblValor)
        .Parameters.Append oParam
        Set oParam = .CreateParameter("ID", adInteger, adParamOutput)
        .Parameters.Append oParam
        
        .Execute
        If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo TratarError
        m_lId = .Parameters("ID").Value
    End With
    
Salir:
    Set oCom = Nothing
    InsertarHitoFacturacion = TESError
    Exit Function
TratarError:
    If m_oConexion.ADOCon.Errors.Count > 0 Then
        TESError = basErrores.TratarError(m_oConexion.ADOCon.Errors)
        Resume Salir
    ElseIf ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CHitoFact", "InsertarHitoFacturacion", ERR, Erl)
    End If
End Function

Public Function ActualizarHitoFacturacion(ByVal lIDLinea As Long) As TipoErrorSummit
    Dim oCom As ADODB.Command
    Dim oParam As ADODB.Parameter
    Dim TESError As TipoErrorSummit
    
    On Error GoTo TratarError
    
    TESError.NumError = TESnoerror
    
    Set oCom = New ADODB.Command
    With oCom
        Set .ActiveConnection = m_oConexion.ADOCon
        .CommandType = adCmdStoredProc
        .CommandText = "FSGS_ACTUALIZAR_LIN_PED_HITO_FAC"
        Set oParam = .CreateParameter("LINEA", adInteger, adParamInput, , lIDLinea)
        .Parameters.Append oParam
        Set oParam = .CreateParameter("ID", adInteger, adParamInput, , m_lId)
        .Parameters.Append oParam
        Set oParam = .CreateParameter("FECHA", adDBDate, adParamInput, , m_dFecha)
        .Parameters.Append oParam
        Set oParam = .CreateParameter("DEN", adVarChar, adParamInput, Len(m_sDen), m_sDen)
        .Parameters.Append oParam
        Set oParam = .CreateParameter("CLASE", adVarChar, adParamInput, Len(m_sClase), m_sClase)
        .Parameters.Append oParam
        Set oParam = .CreateParameter("TIPO_PLAN", adTinyInt, adParamInput, , m_iTipoPlan)
        .Parameters.Append oParam
        Set oParam = .CreateParameter("PORCEN", adDouble, adParamInput, , IIf(m_iTipoPlan = TipoPlanHitoFac.AnticipoParcialPorcentual Or m_iTipoPlan = TipoPlanHitoFac.FacturacionParcialPorcentual, m_vPorcen, Null))
        .Parameters.Append oParam
        Set oParam = .CreateParameter("VALOR", adDouble, adParamInput, , m_dblValor)
        .Parameters.Append oParam
    
        .Execute
        If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo TratarError
    End With
    
Salir:
    Set oCom = Nothing
    ActualizarHitoFacturacion = TESError
    Exit Function
TratarError:
    If m_oConexion.ADOCon.Errors.Count > 0 Then
        TESError = basErrores.TratarError(m_oConexion.ADOCon.Errors)
        Resume Salir
    ElseIf ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CHitoFact", "ActualizarHitoFacturacion", ERR, Erl)
    End If
End Function

Public Function EliminarHitoFacturacion(ByVal lIDLinea As Long) As TipoErrorSummit
    Dim oCom As ADODB.Command
    Dim oParam As ADODB.Parameter
    Dim TESError As TipoErrorSummit
    
    On Error GoTo TratarError
    
    TESError.NumError = TESnoerror
    
    Set oCom = New ADODB.Command
    With oCom
        Set .ActiveConnection = m_oConexion.ADOCon
        .CommandType = adCmdStoredProc
        .CommandText = "FSGS_ELIMINAR_LIN_PED_HITO_FAC"
        Set oParam = .CreateParameter("LINEA", adInteger, adParamInput, , lIDLinea)
        .Parameters.Append oParam
        Set oParam = .CreateParameter("ID", adInteger, adParamInput, , m_lId)
        .Parameters.Append oParam
    
        .Execute
        If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo TratarError
    End With
    
Salir:
    Set oCom = Nothing
    EliminarHitoFacturacion = TESError
    Exit Function
TratarError:
    If m_oConexion.ADOCon.Errors.Count > 0 Then
        TESError = basErrores.TratarError(m_oConexion.ADOCon.Errors)
        Resume Salir
    ElseIf ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CHitoFact", "EliminarHitoFacturacion", ERR, Erl)
    End If
End Function

