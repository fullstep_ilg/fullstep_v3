VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CPlanEntrega"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum
    
Implements IBaseDatos

Private m_oLineaPedido As CLineaPedido
Private m_oOrdenEntrega As COrdenEntrega
Private m_oConexion As CConexion
Private m_vId As Variant
Private m_vIDLineaPedido As Variant
Private m_vIDOrdenEntrega As Variant
Private m_vFecha As Variant
Private m_vCantidad As Variant
Private m_vAlbaran As Variant
Private m_vFecAct As Variant
Private m_vImporte As Variant

Public Property Get LineaPedido() As CLineaPedido
    Set LineaPedido = m_oLineaPedido
End Property
Public Property Set LineaPedido(ByVal Data As CLineaPedido)
    Set m_oLineaPedido = Data
End Property
Public Property Get OrdenEntrega() As COrdenEntrega
    Set OrdenEntrega = m_oOrdenEntrega
End Property
Public Property Set OrdenEntrega(ByVal Data As COrdenEntrega)
    Set m_oOrdenEntrega = Data
End Property

''' <summary>
''' Obtener el valor de m_vID
''' </summary>
''' <remarks>Llamada desde: frmplanesentrega/sdbgPlanes_BeforeUpdate     frmplanesentrega/sdbgPlanes_Change
''' frmplanesentrega/cmdEliminar_Click       frmplanesentrega/CargarGrid; Tiempo m�ximo: 0</remarks>
Public Property Get Id() As Variant
    Id = m_vId
End Property

''' <summary>
''' Establecer el valor de m_vID
''' </summary>
''' <remarks>Llamada desde: IBaseDatos_AnyadirABaseDatos        CPlanesEntrega/Add; Tiempo m�ximo: 0</remarks>
Public Property Let Id(ByVal Data As Variant)
    m_vId = Data
End Property
''' <summary>
''' Obtener el valor de m_vIDLineaPedido
''' </summary>
''' <remarks>Llamada desde: IBaseDatos_AnyadirABaseDatos    UnaInsercion  ; Tiempo m�ximo: 0</remarks>
Public Property Get idLineaPedido() As Variant
    idLineaPedido = m_vIDLineaPedido
End Property
''' <summary>
''' Establecer el valor de m_vIDLineaPedido
''' </summary>
''' <remarks>Llamada desde: frmplanesentrega/sdbgPlanes_BeforeUpdate    CPlanesEntrega/Add; Tiempo m�ximo: 0</remarks>
Public Property Let idLineaPedido(ByVal Data As Variant)
    m_vIDLineaPedido = Data
End Property
''' <summary>
''' Obtener el valor de m_vIDOrdenEntrega
''' </summary>
''' <remarks>Llamada desde: IBaseDatos_AnyadirABaseDatos    UnaInsercion ; Tiempo m�ximo: 0</remarks>
Public Property Get IDOrdenEntrega() As Variant
    IDOrdenEntrega = m_vIDOrdenEntrega
End Property
''' <summary>
''' Establecer el valor de m_vIDOrdenEntrega
''' </summary>
''' <remarks>Llamada desde: frmplanesentrega/sdbgPlanes_BeforeUpdate    CPlanesEntrega/Add; Tiempo m�ximo: 0</remarks>
Public Property Let IDOrdenEntrega(ByVal Data As Variant)
    m_vIDOrdenEntrega = Data
End Property
''' <summary>
''' Obtener el valor de m_vFecha
''' </summary>
''' <remarks>Llamada desde: IBaseDatos_AnyadirABaseDatos    UnaInsercion    UnaUpdate   UnaDelete
''' frmplanesentrega/sdbgPlanes_BeforeUpdate     frmplanesentrega/CargarGrid
''' frmseguimiento/AplicarUnPlanConfigurado; Tiempo m�ximo: 0</remarks>
Public Property Get Fecha() As Variant
    Fecha = m_vFecha
End Property
''' <summary>
''' Establecer el valor de m_vFecha
''' </summary>
''' <remarks>Llamada desde: frmplanesentrega/sdbgPlanes_BeforeUpdate    frmplanesentrega/sdbgPlanes_BeforeColUpdate     CPlanesEntrega/Add
''' ; Tiempo m�ximo: 0</remarks>
Public Property Let Fecha(ByVal Data As Variant)
    m_vFecha = Data
End Property
''' <summary>
''' Obtener el valor de m_vCantidad
''' </summary>
''' <remarks>Llamada desde: IBaseDatos_AnyadirABaseDatos    UnaInsercion    UnaUpdate
''' frmplanesentrega/sdbgPlanes_BeforeUpdate     frmplanesentrega/CargarGrid    frmseguimiento/AplicarUnPlanConfigurado; Tiempo m�ximo: 0</remarks>
Public Property Get Cantidad() As Variant
    Cantidad = m_vCantidad
End Property
''' <summary>
''' Establecer el valor de m_vCantidad
''' </summary>
''' <remarks>Llamada desde: frmplanesentrega/sdbgPlanes_BeforeUpdate    frmplanesentrega/sdbgPlanes_BeforeColUpdate     CPlanesEntrega/Add
''' ; Tiempo m�ximo: 0</remarks>
Public Property Let Cantidad(ByVal Data As Variant)
    m_vCantidad = Data
End Property
''' <summary>
''' Obtener el valor de m_vAlbaran
''' </summary>
''' <remarks>Llamada desde: frmplanesentrega/sdbgPlanes_BeforeUpdate     frmplanesentrega/CargarGrid; Tiempo m�ximo: 0</remarks>
Public Property Get Albaran() As Variant
    Albaran = m_vAlbaran
End Property
''' <summary>
''' Establecer el valor de m_vAlbaran
''' </summary>
''' <remarks>Llamada desde: CPlanesEntrega/Add ; Tiempo m�ximo: 0</remarks>
Public Property Let Albaran(ByVal Data As Variant)
    m_vAlbaran = Data
End Property
''' <summary>
''' Obtener el valor de m_vFecAct
''' </summary>
''' <remarks>Llamada desde: Nadie, usan la privada IBaseDatos_AnyadirABaseDatos y IBaseDatos_FinalizarEdicionModificando ; Tiempo m�ximo: 0</remarks>
Public Property Get FecAct() As Variant
    FecAct = m_vFecAct
End Property
''' <summary>
''' Establecer el valor de m_vFecAct
''' </summary>
''' <remarks>Llamada desde: CPlanesEntrega/Add ; Tiempo m�ximo: 0</remarks>
Public Property Let FecAct(ByVal Data As Variant)
    m_vFecAct = Data
End Property
''' <summary>
''' Obtener el valor de m_vImporte
''' </summary>
''' <remarks>Llamada desde: IBaseDatos_AnyadirABaseDatos    UnaInsercion    UnaUpdate
''' frmplanesentrega/sdbgPlanes_BeforeUpdate     frmplanesentrega/CargarGrid    frmseguimiento/AplicarUnPlanConfigurado; Tiempo m�ximo: 0</remarks>
Public Property Get Importe() As Variant
    Importe = m_vImporte
End Property
''' <summary>
''' Establecer el valor de m_vImporte
''' </summary>
''' <remarks>Llamada desde: frmplanesentrega/sdbgPlanes_BeforeUpdate    frmplanesentrega/sdbgPlanes_BeforeColUpdate     CPlanesEntrega/Add
''' ; Tiempo m�ximo: 0</remarks>
Public Property Let Importe(ByVal Data As Variant)
    m_vImporte = Data
End Property
''' <summary>
''' Establecer el valor de m_oConexion
''' </summary>
''' <remarks>Llamada desde: CPlanesEntrega/Add ; Tiempo m�ximo: 0</remarks>
Friend Property Set Conexion(ByVal con As CConexion)
Set m_oConexion = con
End Property
''' <summary>
''' Obtener el valor de m_oConexion
''' </summary>
''' <remarks>Llamada desde: Nadie, usan la privada IBaseDatos_AnyadirABaseDatos y IBaseDatos_FinalizarEdicionModificando ; Tiempo m�ximo: 0</remarks>
Friend Property Get Conexion() As CConexion
Set Conexion = m_oConexion
End Property
''' <summary>
''' destroys collection when this class is terminated
''' </summary>
''' <remarks>Llamada desde: sistema ; Tiempo m�ximo: 0</remarks>
Private Sub Class_Terminate()
    Set m_oConexion = Nothing
End Sub

''' <summary>Esta funci�n a�ade el plan </summary>
''' <returns>TipoErrorSummit con el error si lo hay</returns>
''' <remarks>Llamada desde: frmPlnesEntrega/sdbgPlanes_BeforeUpdate
''' Tiempo m�ximo: 0,1 sec </remarks>
''' Revisado por: Jbg. Fecha: 21/05/2012
Private Function IBaseDatos_AnyadirABaseDatos() As TipoErrorSummit
    Dim sConsulta As String
    Dim TESError As TipoErrorSummit
    Dim rs As adodb.Recordset
    Dim idLogOrden As Long
    Dim lIdLogLineasPedido As Long
    Dim sERP As String
    Dim oOrdenesEntrega As COrdenesEntrega
    Dim oOrdenEntrega As COrdenEntrega
    TESError.NumError = TESnoerror
    
    ''' Precondicion
    
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CPLanEntrega.AnyadirABAseDatos", "No se ha establecido la conexion"
    End If
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:

    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
    
    sConsulta = "INSERT INTO LINEAS_PEDIDO_ENTREGAS (LINEA_PEDIDO,ORDEN,FECHA,CANTIDAD,IMPORTE, FECACT) "
    sConsulta = sConsulta & "VALUES (" & m_vIDLineaPedido & "," & m_vIDOrdenEntrega & "," & DateToSQLDate(m_vFecha) & "," & DblToSQLFloat(m_vCantidad) & "," & DblToSQLFloat(m_vImporte) & ", GETDATE())"
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    
    'Eliminar la fecha de entrega y la obligaci�n de la l�nea de pedido
    sConsulta = "UPDATE LINEAS_PEDIDO SET FECENTREGA=NULL,ENTREGA_OBL=0 WHERE ID=" & m_vIDLineaPedido
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    
    'Cargamos el ID de lo recien insertado por eso no se pone WITH (NOLOCk)
    Set rs = New adodb.Recordset
    sConsulta = "SELECT MAX(ID) AS ID FROM LINEAS_PEDIDO_ENTREGAS"
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
    m_vId = rs("ID").Value
    rs.Close
    
    'Cargamos el FECACT de lo recien insertado por eso no se pone WITH (NOLOCk)
    sConsulta = "SELECT FECACT FROM LINEAS_PEDIDO_ENTREGAS WHERE ID=" & m_vId
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
    m_vFecAct = rs(0).Value
    rs.Close
    
    Set rs = Nothing
    
    'Cargamos el FECACT de lo recien insertado en la l�nea de pedido, por eso no se pone WITH (NOLOCK)
    sConsulta = "SELECT FECACT FROM LINEAS_PEDIDO WHERE ID=" & m_vIDLineaPedido
    Set rs = New adodb.Recordset
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    If rs.eof Then
        rs.Close
        Set rs = Nothing
        TESError.NumError = TESDatoEliminado
        TESError.Arg1 = 133 ' Tabla LINEAS_PEDIDO
        IBaseDatos_AnyadirABaseDatos = TESError
        Exit Function
    Else
        m_oLineaPedido.FecAct = rs("FECACT").Value
    End If
    If m_oLineaPedido.GrabarEnLog Then
        Set m_oLineaPedido.ObjetoOrden = m_oOrdenEntrega
        m_oOrdenEntrega.TipoLogPedido = PlanEntrega_LineaPedido
        'Grabar en LOG_ORDEN_ENTREGA s�lo un registro para todas las l�neas
        TESError = m_oOrdenEntrega.GrabarLogOrdenEntrega(idLogOrden, sERP, Accion_Reenvio)
        If TESError.NumError <> TESnoerror Then
            GoTo Error_Cls
        End If
    
        TESError = m_oLineaPedido.GrabarLogLineaPedido(idLogOrden, lIdLogLineasPedido, Accion_Modificacion, sERP)
        If TESError.NumError <> TESnoerror Then
            GoTo Error_Cls
        End If
        m_oLineaPedido.IdCod_PlanEntrega = CLng(m_vId)
        TESError = m_oLineaPedido.GrabarLogResto(idLogOrden, lIdLogLineasPedido)
        If TESError.NumError <> TESnoerror Then
            GoTo Error_Cls
        End If
    End If
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    IBaseDatos_AnyadirABaseDatos = TESError
    Exit Function
    
Error_Cls:
     
    IBaseDatos_AnyadirABaseDatos = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CPlanEntrega", "IBaseDatos_AnyadirABaseDatos", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If
End Function

Private Function IBaseDatos_CambiarCodigo(CodigoNuevo As Variant) As TipoErrorSummit

End Function

Private Sub IBaseDatos_CancelarEdicion()

End Sub

Private Function IBaseDatos_ComprobarExistenciaEnBaseDatos() As Boolean

End Function

Private Function IBaseDatos_EliminarDeBaseDatos() As TipoErrorSummit

End Function

Private Function IBaseDatos_FinalizarEdicionEliminando() As TipoErrorSummit

End Function

''' <summary>Esta funci�n modifica el plan </summary>
''' <returns>TipoErrorSummit con el error si lo hay</returns>
''' <remarks>Llamada desde: frmPlnesEntrega/sdbgPlanes_BeforeUpdate
''' Tiempo m�ximo: 0,1 sec </remarks>
''' Revisado por: Jbg. Fecha: 21/05/2012
Private Function IBaseDatos_FinalizarEdicionModificando() As TipoErrorSummit
    Dim TESError As TipoErrorSummit
    Dim btrans As Boolean
    Dim sConsulta As String
    Dim m_adores As adodb.Recordset
    Dim idLogOrden As Long
    Dim lIdLogLineasPedido As Long
    Dim sERP As String
    TESError.NumError = TESnoerror

    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CPlanEntrega.FinalizarEdicionModificando", "No se ha establecido la conexion"
    End If
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
    
    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
    btrans = True
    
    'Comprobamos si ha habido cambios en  otra sesi�n por eso no se pone WITH (NOLOCk)
    Set m_adores = New adodb.Recordset
    m_adores.Open ("SELECT FECACT FROM LINEAS_PEDIDO_ENTREGAS WHERE ID =" & m_vId), m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
    If m_adores.eof Then
        m_adores.Close
        Set m_adores = Nothing
        TESError.NumError = TESDatoEliminado
        TESError.Arg1 = 193 'LINEAS_PEDIDO_ENTREGAS
        IBaseDatos_FinalizarEdicionModificando = TESError
        m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
        Exit Function
    End If
       
    If m_vFecAct <> m_adores("FECACT") Then
        m_adores.Close
        Set m_adores = Nothing
        TESError.NumError = TESInfActualModificada
        IBaseDatos_FinalizarEdicionModificando = TESError
        m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
        Exit Function
    End If
    
    sConsulta = "UPDATE LINEAS_PEDIDO_ENTREGAS SET FECHA=" & DateToSQLDate(m_vFecha)
    sConsulta = sConsulta & ",CANTIDAD=" & DblToSQLFloat(m_vCantidad)
    sConsulta = sConsulta & ",IMPORTE=" & DblToSQLFloat(m_vImporte)
    sConsulta = sConsulta & " WHERE ID = " & m_vId
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    If m_oLineaPedido.GrabarEnLog Then
        Set m_oLineaPedido.ObjetoOrden = m_oOrdenEntrega
        m_oOrdenEntrega.TipoLogPedido = PlanEntrega_LineaPedido
        'Grabar en LOG_ORDEN_ENTREGA s�lo un registro para todas las l�neas
        TESError = m_oOrdenEntrega.GrabarLogOrdenEntrega(idLogOrden, sERP, Accion_Reenvio)
        If TESError.NumError <> TESnoerror Then
            GoTo Error_Cls
        End If
    
        TESError = m_oLineaPedido.GrabarLogLineaPedido(idLogOrden, lIdLogLineasPedido, Accion_Modificacion, sERP)
        If TESError.NumError <> TESnoerror Then
            GoTo Error_Cls
        End If
        m_oLineaPedido.IdCod_PlanEntrega = CLng(m_vId)
        TESError = m_oLineaPedido.GrabarLogResto(idLogOrden, lIdLogLineasPedido)
        If TESError.NumError <> TESnoerror Then
            GoTo Error_Cls
        End If
    End If
    'Recogemos el nuevo valor de FECACT
    m_adores.Requery
    m_vFecAct = m_adores("FECACT").Value
    m_adores.Close
    Set m_adores = Nothing
    
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    btrans = False
    IBaseDatos_FinalizarEdicionModificando = TESError
    Exit Function
    
Error_Cls:
    IBaseDatos_FinalizarEdicionModificando = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    If Not m_adores Is Nothing Then
        m_adores.Close
        Set m_adores = Nothing
    End If
    If btrans Then
        m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CPlanEntrega", "IBaseDatos_FinalizarEdicionModificando", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If
End Function

''' <summary>Esta funci�n inicia la edici�n del resulset</summary>
'''<param optional name="Bloquear">Si se bloquea o no</param>
'''<param optional name="UsuarioBloqueo">Usuario del bloqueo</param>
''' <returns>TipoErrorSummit con el error si lo hay</returns>
''' <remarks>Llamada desde: frmPlnesEntrega/sdbgPlanes_Change
''' Tiempo m�ximo: 0,1 seg </remarks>
''' Revisado por: Jbg. Fecha: 21/05/2012
Private Function IBaseDatos_IniciarEdicion(Optional ByVal Bloquear As Boolean, Optional ByVal UsuarioBloqueo As String) As TipoErrorSummit
    Dim TESError As TipoErrorSummit
    Dim sSQL As String
    Dim m_adores As adodb.Recordset
    

    TESError.NumError = TESnoerror
     
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CLineaPedido.IniciarEdicion", "No se ha establecido la conexion"
    End If
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
    Set m_adores = New adodb.Recordset
    'Calidad sin With(nolock) para asegurarse de q realmente no se ha modificado
    m_adores.Open ("SELECT FECACT FROM LINEAS_PEDIDO_ENTREGAS WHERE ID=" & m_vId), m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
    
    If m_adores.eof Then
        m_adores.Close
        Set m_adores = Nothing
        TESError.NumError = TESDatoEliminado
        TESError.Arg1 = 193 'LINEAS_PEDIDO_ENTREGAS
        IBaseDatos_IniciarEdicion = TESError
        Exit Function
    End If
       
    If m_vFecAct <> m_adores("FECACT") Then
        m_adores.Close
        Set m_adores = Nothing
        TESError.NumError = TESInfActualModificada
        IBaseDatos_IniciarEdicion = TESError
        Exit Function
    End If
    
    m_adores.Close
    Set m_adores = Nothing
    
    IBaseDatos_IniciarEdicion = TESError
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CPlanEntrega", "IBaseDatos_IniciarEdicion", ERR, Erl)
      Exit Function
   End If
End Function

''' <summary>
''' Construir un string con la sql corrrespondiente a una insert del plan
''' </summary>
''' <param name="idLineaPedido">Id de la linea pedido del plan</param>
''' <param name="Cantidad">Cantidad del plan</param>
''' <returns>String con la sql construida</returns>
''' <remarks>Llamada desde: frmSeguimiento/AplicarUnPlanConfigurado; Tiempo m�ximo: 0</remarks>
''' Revisado por: Jbg. Fecha: 21/05/2012
Public Function UnaInsercion(Optional ByVal idLineaPedido As Long = 0, Optional ByVal Cantidad As Variant, Optional ByVal Importe As Variant) As String
    Dim sConsulta As String
    
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If idLineaPedido > 0 Then
        sConsulta = "INSERT INTO LINEAS_PEDIDO_ENTREGAS (LINEA_PEDIDO,ORDEN,FECHA,CANTIDAD,IMPORTE, FECACT) VALUES (" _
            & idLineaPedido & "," & m_vIDOrdenEntrega & "," & DateToSQLDate(m_vFecha) & "," & DblToSQLFloat(Cantidad) & "," & DblToSQLFloat(Importe) & ", GETDATE())" & vbCrLf
    Else
        sConsulta = "INSERT INTO LINEAS_PEDIDO_ENTREGAS (LINEA_PEDIDO,ORDEN,FECHA,CANTIDAD,IMPORTE, FECACT) VALUES (" _
            & m_vIDLineaPedido & "," & m_vIDOrdenEntrega & "," & DateToSQLDate(m_vFecha) & "," & DblToSQLFloat(m_vCantidad) & "," & DblToSQLFloat(m_vImporte) & ", GETDATE())" & vbCrLf
    End If
    
    UnaInsercion = sConsulta
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CPlanEntrega", "UnaInsercion", ERR, Erl)
      Exit Function
   End If
End Function

''' <summary>
''' Construir un string con la sql corrrespondiente a una update del plan
''' </summary>
''' <param name="idLineaPedido">Id de la linea pedido del plan</param>
''' <param name="Cantidad">Cantidad del plan</param>
''' <returns>String con la sql construida</returns>
''' <remarks>Llamada desde: frmSeguimiento/AplicarUnPlanConfigurado; Tiempo m�ximo: 0</remarks>
''' Revisado por: Jbg. Fecha: 21/05/2012
Public Function UnaUpdate(ByVal idLineaPedido As Long, Optional ByVal Cantidad As Variant, Optional ByVal Importe As Variant) As String
    Dim sConsulta As String
    

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sConsulta = " UPDATE LINEAS_PEDIDO_ENTREGAS SET CANTIDAD=" & DblToSQLFloat(Cantidad) & ",IMPORTE=" & DblToSQLFloat(Importe) & " WHERE LINEA_PEDIDO = " & idLineaPedido _
                & " AND FECHA=" & DateToSQLDate(m_vFecha) & vbCrLf

    UnaUpdate = sConsulta
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CPlanEntrega", "UnaUpdate", ERR, Erl)
      Exit Function
   End If
End Function

''' <summary>
''' Construir un string con la sql corrrespondiente a una delete del plan
''' </summary>
''' <param name="idLineaPedido">Id de la linea pedido del plan</param>
''' <param name="Fecha">Fecha del plan</param>
''' <returns>String con la sql construida</returns>
''' <remarks>Llamada desde: frmSeguimiento/AplicarUnPlanConfigurado    frmSeguimiento/EliminarPlanesDeLineaPedido; Tiempo m�ximo: 0</remarks>
''' Revisado por: Jbg. Fecha: 21/05/2012
Public Function UnaDelete(ByVal idLineaPedido As Long, ByVal Fecha As Variant) As String
    Dim sConsulta As String
    

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sConsulta = " DELETE LINEAS_PEDIDO_ENTREGAS WHERE LINEA_PEDIDO = " & idLineaPedido _
                    & " AND FECHA=" & DateToSQLDate(Fecha) & vbCrLf

    UnaDelete = sConsulta
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CPlanEntrega", "UnaDelete", ERR, Erl)
      Exit Function
   End If
End Function

