VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CNotificadoAccion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum

Implements IBaseDatos

Private moConexion As CConexion

Private m_lID As Long
Private m_iOrigen As OrigenNotificadoAccion
Private m_lEnlace As Long
Private m_lAccion As Long
Private m_iTipo As TipoNotificadoAccion
Private m_sPer As String
Private m_lRol As Long
Private m_oCamposVisibles As CNotificadoAccionCampos
Private m_bConfiguracion_Rol As Boolean
Private m_lRolNotificacion As Long
Private m_lBloqueNotificacion As Long
Private mdtFecAct As Variant
Private mvarIndice As Variant

Private m_adores As adodb.Recordset
Private m_lExpiracion As Long

Friend Property Set Conexion(ByVal con As CConexion)
    Set moConexion = con
End Property
Friend Property Get Conexion() As CConexion
    Set Conexion = moConexion
End Property

Public Property Get Id() As Long
    Id = m_lID
End Property
Public Property Let Id(ByVal Data As Long)
    m_lID = Data
End Property

Public Property Get Origen() As OrigenNotificadoAccion
    Origen = m_iOrigen
End Property
Public Property Let Origen(ByVal Data As OrigenNotificadoAccion)
    m_iOrigen = Data
End Property

Public Property Get Enlace() As Long
    Enlace = m_lEnlace
End Property
Public Property Let Enlace(ByVal Data As Long)
    m_lEnlace = Data
End Property

Public Property Get Accion() As Long
    Accion = m_lAccion
End Property
Public Property Let Accion(ByVal Data As Long)
    m_lAccion = Data
End Property

Public Property Get Tipo() As TipoNotificadoAccion
    Tipo = m_iTipo
End Property
Public Property Let Tipo(ByVal Data As TipoNotificadoAccion)
    m_iTipo = Data
End Property

Public Property Get Per() As String
    Per = m_sPer
End Property
Public Property Let Per(ByVal Data As String)
    m_sPer = Data
End Property

Public Property Get Rol() As Long
    Rol = m_lRol
End Property
Public Property Let Rol(ByVal Data As Long)
    m_lRol = Data
End Property

Public Property Let Indice(ByVal varIndice As Variant)
    mvarIndice = varIndice
End Property
Public Property Get Indice() As Variant
    Indice = mvarIndice
End Property

Public Property Get FecAct() As Variant
    FecAct = mdtFecAct
End Property
Public Property Let FecAct(ByVal Data As Variant)
    mdtFecAct = Data
End Property

Public Property Get CamposVisibles() As CNotificadoAccionCampos
    Set CamposVisibles = m_oCamposVisibles
End Property
Public Property Set CamposVisibles(ByVal oCamposVisibles As CNotificadoAccionCampos)
    Set m_oCamposVisibles = oCamposVisibles
End Property

Public Property Get Configuracion_Rol() As Boolean
    Configuracion_Rol = m_bConfiguracion_Rol
End Property
Public Property Let Configuracion_Rol(ByVal bConfiguracionRol As Boolean)
    m_bConfiguracion_Rol = bConfiguracionRol
End Property

Public Property Get Rol_Notificacion() As Long
    Rol_Notificacion = m_lRolNotificacion
End Property
Public Property Let Rol_Notificacion(ByVal lRolNotificacion As Long)
    m_lRolNotificacion = lRolNotificacion
End Property

Public Property Get Bloque_Notificacion() As Long
    Bloque_Notificacion = m_lBloqueNotificacion
End Property
Public Property Let Bloque_Notificacion(ByVal lBloqueNotificacion As Long)
    m_lBloqueNotificacion = lBloqueNotificacion
End Property

Public Property Get Expiracion() As Long
    Expiracion = m_lExpiracion
End Property
Public Property Let Expiracion(ByVal Data As Long)
    m_lExpiracion = Data
End Property


Private Sub Class_Terminate()
    Set moConexion = Nothing
End Sub


Private Function IBaseDatos_AnyadirABaseDatos() As TipoErrorSummit
Dim sConsulta As String
Dim TESError As TipoErrorSummit
Dim AdoRes As adodb.Recordset
Dim bTransaccionEnCurso As Boolean
Dim foreingKeyValue As Long
Dim foreingKeyName As String
Dim tableName As String
Dim foreingKeyTable As String
Dim foreingKeyBloque As String


TESError.NumError = TESnoerror

If moConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CNotificadoAccion.AnyadirABAseDatos", "No se ha establecido la conexion"
End If
' *****************************************

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
    moConexion.ADOCon.Execute "BEGIN TRAN"
    bTransaccionEnCurso = True
    moConexion.ADOCon.Execute "SET XACT_ABORT OFF"
    If m_iOrigen = OrigenNotificadoAccion.Enlace Then
        foreingKeyName = "ENLACE"
        foreingKeyValue = m_lEnlace
        tableName = "PM_NOTIFICADO_ENLACE"
        foreingKeyTable = "PM_ENLACE"
        foreingKeyBloque = "BLOQUE_ORIGEN"
    ElseIf m_iOrigen = OrigenNotificadoAccion.Accion Then
        foreingKeyName = "ACCION"
        foreingKeyValue = m_lAccion
        tableName = "PM_NOTIFICADO_ACCION"
        foreingKeyTable = "PM_ACCIONES"
        foreingKeyBloque = "BLOQUE"
    Else
        foreingKeyName = "AVISO_EXPIRACION_CAMPOS"
        foreingKeyValue = m_lExpiracion
        tableName = "PM_NOTIFICADO_AVISO_EXPIRACION"
        foreingKeyTable = "PM_AVISO_EXPIRACION_CAMPOS"
        foreingKeyBloque = "WORKFLOW"
    End If
    
    
    sConsulta = "INSERT INTO " & tableName & " (" & foreingKeyName & ", TIPO_NOTIFICADO,CONFIGURACION_ROL, PER, ROL) VALUES ("
    sConsulta = sConsulta & foreingKeyValue & "," & m_iTipo & "," & IIf(m_bConfiguracion_Rol = True, 1, 0) & ","
    Select Case m_iTipo
        Case TipoNotificadoAccion.Especifico
            sConsulta = sConsulta & StrToSQLNULL(m_sPer) & ",NULL)"
        Case TipoNotificadoAccion.Rol
            sConsulta = sConsulta & "NULL," & m_lRol & ")"
        Case Else
            sConsulta = sConsulta & "NULL,NULL)"
    End Select
    
    moConexion.ADOCon.Execute sConsulta
    If moConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    
    sConsulta = "SELECT MAX(ID) ID FROM " & tableName
    Set AdoRes = New adodb.Recordset
    AdoRes.Open sConsulta, moConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    m_lID = AdoRes("ID").Value
    AdoRes.Close
    Set AdoRes = Nothing

    ''' Recuperar la fecha de insercion
    sConsulta = "SELECT FECACT FROM " & tableName & " WITH (NOLOCK) WHERE ID =" & m_lID
    Set AdoRes = New adodb.Recordset
    AdoRes.Open sConsulta, moConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    mdtFecAct = AdoRes("FECACT").Value
    AdoRes.Close
    Set AdoRes = Nothing
    
    ''' Establecer campos visibles
    sConsulta = "SELECT MAX(ID) FROM " & tableName & _
        " WHERE " & foreingKeyName & "=" & foreingKeyValue & " AND ID<>" & m_lID
    Set AdoRes = New adodb.Recordset
    AdoRes.Open sConsulta, moConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    If IsNull(AdoRes(0).Value) Then
        If m_iOrigen = OrigenNotificadoAccion.Expiracion Then
        sConsulta = "INSERT INTO " & tableName & "_CAMPOS (CAMPO, NOTIFICADO, VISIBLE)" & _
            " SELECT FORM_CAMPO.ID, " & m_lID & ", 1 FROM FORM_CAMPO WITH (NOLOCK)" & _
            " INNER JOIN FORM_GRUPO WITH (NOLOCK) ON FORM_GRUPO.ID=FORM_CAMPO.GRUPO AND FORM_GRUPO.BAJALOG=0" & _
            " INNER JOIN SOLICITUD WITH (NOLOCK) ON SOLICITUD.FORMULARIO=FORM_GRUPO.FORMULARIO" & _
            " INNER JOIN PM_AVISO_EXPIRACION_CAMPOS WITH (NOLOCK) ON PM_AVISO_EXPIRACION_CAMPOS.WORKFLOW = SOLICITUD.WORKFLOW" & _
            " INNER JOIN PM_NOTIFICADO_AVISO_EXPIRACION WITH (NOLOCK) ON PM_NOTIFICADO_AVISO_EXPIRACION.AVISO_EXPIRACION_CAMPOS = PM_AVISO_EXPIRACION_CAMPOS.ID" & _
            " WHERE PM_NOTIFICADO_AVISO_EXPIRACION.AVISO_EXPIRACION_CAMPOS = " & foreingKeyValue & " AND FORM_CAMPO.BAJALOG=0 "
        Else
        
        sConsulta = "INSERT INTO " & tableName & "_CAMPOS (CAMPO, NOTIFICADO, VISIBLE)" & _
            " SELECT FORM_CAMPO.ID, " & m_lID & ", 1 FROM FORM_CAMPO WITH (NOLOCK)" & _
            " INNER JOIN FORM_GRUPO WITH (NOLOCK) ON FORM_GRUPO.ID=FORM_CAMPO.GRUPO AND FORM_GRUPO.BAJALOG=0" & _
            " INNER JOIN SOLICITUD WITH (NOLOCK) ON SOLICITUD.FORMULARIO=FORM_GRUPO.FORMULARIO" & _
            " INNER JOIN PM_BLOQUE WITH (NOLOCK) ON PM_BLOQUE.WORKFLOW=SOLICITUD.WORKFLOW" & _
            " INNER JOIN " & foreingKeyTable & " WITH (NOLOCK) ON " & foreingKeyTable & "." & foreingKeyBloque & "=PM_BLOQUE.ID" & _
            " WHERE " & foreingKeyTable & ".ID = " & foreingKeyValue & " AND FORM_CAMPO.BAJALOG=0 " & _
            " UNION" & _
            " SELECT FORM_CAMPO.ID, " & m_lID & ", 1 FROM FORM_CAMPO WITH (NOLOCK)" & _
            " INNER JOIN FORM_GRUPO WITH (NOLOCK) ON FORM_GRUPO.ID=FORM_CAMPO.GRUPO AND FORM_GRUPO.BAJALOG=0" & _
            " INNER JOIN SOLICITUD WITH (NOLOCK) ON SOLICITUD.FORMULARIO=FORM_GRUPO.FORMULARIO" & _
            " INNER JOIN PM_BLOQUE WITH (NOLOCK) ON PM_BLOQUE.WORKFLOW=SOLICITUD.WORKFLOW_MODIF" & _
            " INNER JOIN " & foreingKeyTable & " WITH (NOLOCK) ON " & foreingKeyTable & "." & foreingKeyBloque & "=PM_BLOQUE.ID" & _
            " WHERE " & foreingKeyTable & ".ID = " & foreingKeyValue & " AND FORM_CAMPO.BAJALOG=0 " & _
            " UNION" & _
            " SELECT FORM_CAMPO.ID, " & m_lID & ", 1 FROM FORM_CAMPO WITH (NOLOCK)" & _
            " INNER JOIN FORM_GRUPO WITH (NOLOCK) ON FORM_GRUPO.ID=FORM_CAMPO.GRUPO AND FORM_GRUPO.BAJALOG=0" & _
            " INNER JOIN SOLICITUD WITH (NOLOCK) ON SOLICITUD.FORMULARIO=FORM_GRUPO.FORMULARIO" & _
            " INNER JOIN PM_BLOQUE WITH (NOLOCK) ON PM_BLOQUE.WORKFLOW=SOLICITUD.WORKFLOW_BAJA" & _
            " INNER JOIN " & foreingKeyTable & " WITH (NOLOCK) ON " & foreingKeyTable & "." & foreingKeyBloque & "=PM_BLOQUE.ID" & _
            " WHERE " & foreingKeyTable & ".ID = " & foreingKeyValue & " AND FORM_CAMPO.BAJALOG=0 "
        End If
    Else
        sConsulta = "INSERT INTO " & tableName & "_CAMPOS (CAMPO, NOTIFICADO, VISIBLE)" & _
            "SELECT CAMPO, " & m_lID & ", VISIBLE FROM " & tableName & "_CAMPOS WITH (NOLOCK) WHERE NOTIFICADO=" & AdoRes(0).Value
    End If
    AdoRes.Close
    Set AdoRes = Nothing
    moConexion.ADOCon.Execute sConsulta
    If moConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    
    moConexion.ADOCon.Execute "COMMIT TRAN"
    bTransaccionEnCurso = False
       
    IBaseDatos_AnyadirABaseDatos = TESError
    Exit Function
Error_Cls:

    If Not AdoRes Is Nothing Then
        AdoRes.Close
        Set AdoRes = Nothing
    End If
                 
    IBaseDatos_AnyadirABaseDatos = basErrores.TratarError(moConexion.ADOCon.Errors)
                 
    If bTransaccionEnCurso Then
        moConexion.ROLLBACKTRANSACTION
        bTransaccionEnCurso = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CNotificadoAccion", "IBaseDatos_AnyadirABaseDatos", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If
    
End Function

Private Function IBaseDatos_CambiarCodigo(CodigoNuevo As Variant) As TipoErrorSummit

End Function

Private Function IBaseDatos_CambiarTipoConfiguracion(Tipo As Integer) As TipoErrorSummit

End Function

Private Sub IBaseDatos_CancelarEdicion()

End Sub

Private Function IBaseDatos_ComprobarExistenciaEnBaseDatos() As Boolean

End Function

Private Function IBaseDatos_EliminarDeBaseDatos() As TipoErrorSummit
Dim TESError As TipoErrorSummit
Dim btrans As Boolean
Dim sConsulta As String
Dim AdoRes As adodb.Recordset
Dim tableName As String


TESError.NumError = TESnoerror

'******************* Precondicion *******************
If moConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CNotificadoAccion.FinalizarEdicionEliminando", "No se ha establecido la conexion"
End If
'*****************************************************

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:

    If m_iOrigen = OrigenNotificadoAccion.Enlace Then
        tableName = "PM_NOTIFICADO_ENLACE"
    ElseIf m_iOrigen = OrigenNotificadoAccion.Accion Then
        tableName = "PM_NOTIFICADO_ACCION"
    Else
        tableName = "PM_NOTIFICADO_AVISO_EXPIRACION"
    End If
    
    moConexion.ADOCon.Execute "BEGIN TRAN"
    moConexion.ADOCon.Execute "SET XACT_ABORT OFF"
    btrans = True
    moConexion.ADOCon.Execute "DELETE FROM " & tableName & "_CAMPOS WHERE NOTIFICADO=" & m_lID
    If moConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    moConexion.ADOCon.Execute "DELETE FROM " & tableName & " WHERE ID = " & m_lID
    If moConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    moConexion.ADOCon.Execute "COMMIT TRAN"
    btrans = False
    Set m_adores = Nothing
    
    IBaseDatos_EliminarDeBaseDatos = TESError
    
    Exit Function
    
Error_Cls:
    
    If moConexion.ADOCon.Errors.Count > 0 Then
        IBaseDatos_EliminarDeBaseDatos = basErrores.TratarError(moConexion.ADOCon.Errors)
        If btrans Then
            moConexion.ROLLBACKTRANSACTION
            btrans = False
        End If
        Exit Function
    End If
    
    If m_adores.EditMode = 1 Then
        m_adores.CancelUpdate
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CNotificadoAccion", "IBaseDatos_EliminarDeBaseDatos", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If
End Function

Private Function IBaseDatos_FinalizarEdicionEliminando() As TipoErrorSummit

End Function

Private Function IBaseDatos_FinalizarEdicionModificando() As TipoErrorSummit
Dim TESError As TipoErrorSummit
Dim sConsulta As String
Dim bTransaccionEnCurso As Boolean
Dim tableName As String
Dim foreingKeyName As String
Dim foreingKeyValue As Long
Dim sFSP As String
Dim i As Integer
Dim sTotalStr As String
Dim lIdACT1 As Long
Dim sCod As String
Dim sDen As String
Dim AdoRes As adodb.Recordset
Dim sDenAnterior As String


TESError.NumError = TESnoerror

'********* Precondicion *******************
If moConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CNotificadoAccion.FinalizarEdicionModificando", "No se ha establecido la conexion"
End If
'******************************************

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:

    If m_iOrigen = OrigenNotificadoAccion.Enlace Then
        foreingKeyName = "ENLACE"
        foreingKeyValue = m_lEnlace
        tableName = "PM_NOTIFICADO_ENLACE"
    ElseIf m_iOrigen = OrigenNotificadoAccion.Accion Then
        foreingKeyName = "ACCION"
        foreingKeyValue = m_lAccion
        tableName = "PM_NOTIFICADO_ACCION"
    Else
        foreingKeyName = "AVISO_EXPIRACION_CAMPOS"
        foreingKeyValue = m_lExpiracion
        tableName = "PM_NOTIFICADO_AVISO_EXPIRACION"
    End If

    moConexion.ADOCon.Execute "BEGIN TRAN"
    bTransaccionEnCurso = True
    moConexion.ADOCon.Execute "SET XACT_ABORT OFF"
    
    'Comprobamos si ha habido cambios en otra sesi�n
    Set m_adores = New adodb.Recordset
    sConsulta = "SELECT FECACT FROM " & tableName & " WHERE ID=" & m_lID
    m_adores.Open sConsulta, moConexion.ADOCon, adOpenStatic, adLockReadOnly
    
    If m_adores.eof Then
        m_adores.Close
        Set m_adores = Nothing
        TESError.NumError = TESDatoEliminado
        TESError.Arg1 = 180
        IBaseDatos_FinalizarEdicionModificando = TESError
        moConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
        Exit Function
    End If
    
    If mdtFecAct <> m_adores("FECACT").Value Then
        m_adores.Close
        Set m_adores = Nothing
        TESError.NumError = TESInfActualModificada
        moConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
        IBaseDatos_FinalizarEdicionModificando = TESError
        Exit Function
    End If
    
    ''' Actualizar
    sConsulta = "UPDATE " & tableName & " SET " & _
        foreingKeyName & " = " & foreingKeyValue & ", " & _
        "TIPO_NOTIFICADO = " & m_iTipo & ", CONFIGURACION_ROL = " & IIf(m_bConfiguracion_Rol = True, 1, 0) & ","
    Select Case m_iTipo
        Case TipoNotificadoAccion.Especifico
            sConsulta = sConsulta & _
                "PER = " & StrToSQLNULL(m_sPer) & ", " & _
                "ROL = NULL "
        Case TipoNotificadoAccion.Rol
            sConsulta = sConsulta & _
                "PER = NULL, " & _
                "ROL = " & m_lRol & " "
        Case Else
            sConsulta = sConsulta & _
                "PER = NULL, " & _
                "ROL = NULL "
    End Select
    sConsulta = sConsulta & "WHERE ID = " & m_lID
    
    moConexion.ADOCon.Execute sConsulta
    If moConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    
    'Recogemos el nuevo valor de FECACT
    m_adores.Requery
    mdtFecAct = m_adores("FECACT").Value
    m_adores.Close
    Set m_adores = Nothing
    
    moConexion.ADOCon.Execute "COMMIT TRAN"
    bTransaccionEnCurso = False
    
    IBaseDatos_FinalizarEdicionModificando = TESError
    
    Exit Function
    
Error_Cls:
    
    IBaseDatos_FinalizarEdicionModificando = basErrores.TratarError(moConexion.ADOCon.Errors)
    
    If Not m_adores Is Nothing Then
        m_adores.Close
        Set m_adores = Nothing
    End If
    
    If bTransaccionEnCurso Then
        moConexion.ROLLBACKTRANSACTION
        bTransaccionEnCurso = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CNotificadoAccion", "IBaseDatos_FinalizarEdicionModificando", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If

End Function

Private Function IBaseDatos_IniciarEdicion(Optional ByVal Bloquear As Boolean, Optional ByVal UsuarioBloqueo As String) As TipoErrorSummit
Dim TESError As TipoErrorSummit
Dim tableName As String
Dim foreingKeyName As String
Dim foreingKeyValue As Long


TESError.NumError = TESnoerror

'******************* Precondicion *******************
If moConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CNotificadoAccion.IniciarEdicion", "No se ha establecido la conexion"
End If
'*****************************************************

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

    If m_iOrigen = OrigenNotificadoAccion.Enlace Then
        foreingKeyName = "ENLACE"
        foreingKeyValue = m_lEnlace
        tableName = "PM_NOTIFICADO_ENLACE"
    ElseIf m_iOrigen = OrigenNotificadoAccion.Accion Then
        foreingKeyName = "ACCION"
        foreingKeyValue = m_lAccion
        tableName = "PM_NOTIFICADO_ACCION"
    Else
        foreingKeyName = "AVISO_EXPIRACION_CAMPOS"
        foreingKeyValue = m_lExpiracion
        tableName = "PM_NOTIFICADO_AVISO_EXPIRACION"
    End If

    Set m_adores = New adodb.Recordset
    m_adores.Open "SELECT ID, " & foreingKeyName & ", TIPO_NOTIFICADO, PER, ROL, CONFIGURACION_ROL,FECACT FROM " & tableName & " WITH (NOLOCK) WHERE ID =" & m_lID, moConexion.ADOCon, adOpenKeyset, adLockReadOnly
    
    If m_adores.eof Then
        m_adores.Close
        Set m_adores = Nothing
        TESError.NumError = TESDatoEliminado
        TESError.Arg1 = 180
        IBaseDatos_IniciarEdicion = TESError
        Exit Function
    End If
    If m_iOrigen = OrigenNotificadoAccion.Enlace Then
        m_lEnlace = NullToDbl0(m_adores.Fields(foreingKeyName).Value)
    ElseIf m_iOrigen = OrigenNotificadoAccion.Accion Then
        m_lAccion = NullToDbl0(m_adores.Fields(foreingKeyName).Value)
    Else
        m_lExpiracion = NullToDbl0(m_adores.Fields(foreingKeyName).Value)
    End If
    m_iTipo = NullToDbl0(m_adores.Fields("TIPO_NOTIFICADO").Value)
    m_sPer = NullToStr(m_adores.Fields("PER").Value)
    m_lRol = NullToDbl0(m_adores.Fields("ROL").Value)
    m_bConfiguracion_Rol = NullToDbl0(m_adores.Fields("CONFIGURACION_ROL").Value)
    mdtFecAct = m_adores.Fields("FECACT").Value
    
    m_adores.Close
    Set m_adores = Nothing
    
    IBaseDatos_IniciarEdicion = TESError
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CNotificadoAccion", "IBaseDatos_IniciarEdicion", ERR, Erl)
      Exit Function
   End If

End Function

'Revisado por: SRA; Fecha: 24/05/2011
'Carga los campos visibles para que el usuario lo seleccione en las notificaciones
'Llamada desde: frmFlujosConfigCampos.frm --> CargarCamposGrupo; Tiempo m�ximo: 0,1 sg
Public Sub CargarCamposVisibles(Optional ByVal Grupo As Long = 0)
    Dim rs As New adodb.Recordset
    Dim sConsulta As String
    Dim oGestorParametros As CGestorParametros
    Dim oIdiomas As CIdiomas
    Dim oIdi As CIdioma
    Dim oDenominaciones As CMultiidiomas
    Dim stableName As String
    Dim bVisible As Boolean

    If moConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CFormGrupo.CargarTodosLosCampos", "No se ha establecido la conexion"
        Exit Sub
    End If
    
    If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
    If m_iOrigen = OrigenNotificadoAccion.Enlace Then
        stableName = "PM_NOTIFICADO_ENLACE_CAMPOS"
    ElseIf m_iOrigen = OrigenNotificadoAccion.Accion Then
        stableName = "PM_NOTIFICADO_ACCION_CAMPOS"
    Else
        stableName = "PM_NOTIFICADO_AVISO_EXPIRACION_CAMPOS"
    End If
    
    
    'Carga todos los idiomas de la aplicaci�n:
    Set oGestorParametros = New CGestorParametros
    Set oGestorParametros.Conexion = moConexion
    Set oIdiomas = oGestorParametros.DevolverIdiomas(False, False, True)
    
    Dim formcampoden_general As String
    formcampoden_general = ""
    
    For Each oIdi In oIdiomas
        If (formcampoden_general <> "") Then
           formcampoden_general = formcampoden_general & ","
        End If
        formcampoden_general = formcampoden_general & "FORM_CAMPO.DEN_" & oIdi.Cod
    Next
    sConsulta = "SELECT " & stableName & ".CAMPO, " & stableName & ".NOTIFICADO, " & stableName & ".VISIBLE," & formcampoden_general & ", FORM_CAMPO.SUBTIPO" & _
    " FROM " & stableName & " WITH (NOLOCK)" & _
    " INNER JOIN FORM_CAMPO WITH (NOLOCK) ON FORM_CAMPO.ID=" & stableName & ".CAMPO AND FORM_CAMPO.BAJALOG=0 " & _
    " WHERE " & stableName & ".NOTIFICADO=" & m_lID & " AND FORM_CAMPO.ES_SUBCAMPO=0 AND ISNULL(FORM_CAMPO.TIPO_CAMPO_GS,0)<>136 AND FORM_CAMPO.SUBTIPO <> " & TiposDeAtributos.TipoEditor

    If Grupo > 0 Then sConsulta = sConsulta & " AND FORM_CAMPO.GRUPO=" & Grupo
    sConsulta = sConsulta & " ORDER BY FORM_CAMPO.ORDEN"
    
    rs.Open sConsulta, moConexion.ADOCon, adOpenForwardOnly, adLockReadOnly

    Set m_oCamposVisibles = Nothing
    Set m_oCamposVisibles = New CNotificadoAccionCampos
    Set m_oCamposVisibles.Conexion = moConexion

    If rs.eof Then
        rs.Close
        Set rs = Nothing
        Set oGestorParametros = Nothing
        Set oIdiomas = Nothing
        Exit Sub

    Else
        Do While Not rs.eof
            Set oDenominaciones = Nothing
            Set oDenominaciones = New CMultiidiomas
            Set oDenominaciones.Conexion = moConexion
            For Each oIdi In oIdiomas
                oDenominaciones.Add oIdi.Cod, rs.Fields("DEN_" & oIdi.Cod).Value
            Next
            bVisible = (rs.Fields("VISIBLE").Value = 1)
            m_oCamposVisibles.Add rs.Fields("CAMPO").Value, rs.Fields("NOTIFICADO").Value, oDenominaciones, bVisible, (rs.Fields("SUBTIPO") = 9)
            rs.MoveNext
        Loop

        rs.Close
        Set rs = Nothing

    End If
        Set oGestorParametros = Nothing
        Set oIdiomas = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CNotificadoAccion", "CargarCamposVisibles", ERR, Erl)
      Exit Sub
   End If

End Sub

Public Sub CargarCamposDesgloseVisibles(ByVal CampoID As Long)
    Dim rs As New adodb.Recordset
    Dim sConsulta As String
    Dim oGestorParametros As CGestorParametros
    Dim oIdiomas As CIdiomas
    Dim oIdi As CIdioma
    Dim oDenominaciones As CMultiidiomas
    Dim stableName As String
    Dim bVisible As Boolean

    If moConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CFormGrupo.CargarTodosLosCampos", "No se ha establecido la conexion"
        Exit Sub
    End If
    
    If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
    If m_iOrigen = OrigenNotificadoAccion.Enlace Then
        stableName = "PM_NOTIFICADO_ENLACE_CAMPOS"
    ElseIf m_iOrigen = OrigenNotificadoAccion.Accion Then
        stableName = "PM_NOTIFICADO_ACCION_CAMPOS"
    Else
        stableName = "PM_NOTIFICADO_AVISO_EXPIRACION_CAMPOS"
    End If
    
    'Carga todos los idiomas de la aplicaci�n:
    Set oGestorParametros = New CGestorParametros
    Set oGestorParametros.Conexion = moConexion
    Set oIdiomas = oGestorParametros.DevolverIdiomas(False, False, True)
    
    Dim formcampoden_general As String
    formcampoden_general = ""
    
    For Each oIdi In oIdiomas
        If (formcampoden_general <> "") Then
           formcampoden_general = formcampoden_general & ","
        End If
        formcampoden_general = formcampoden_general & "FORM_CAMPO.DEN_" & oIdi.Cod
    Next
    
    sConsulta = "SELECT " & stableName & ".CAMPO, " & stableName & ".NOTIFICADO, " & stableName & ".VISIBLE," & formcampoden_general & ", FORM_CAMPO.SUBTIPO" & _
        " FROM " & stableName & " WITH (NOLOCK)" & _
        " INNER JOIN FORM_CAMPO WITH (NOLOCK) ON FORM_CAMPO.ID=" & stableName & ".CAMPO AND FORM_CAMPO.BAJALOG=0 " & _
        " INNER JOIN DESGLOSE WITH (NOLOCK) ON DESGLOSE.CAMPO_HIJO=FORM_CAMPO.ID" & _
        " WHERE " & stableName & ".NOTIFICADO=" & m_lID & " AND DESGLOSE.CAMPO_PADRE=" & CampoID & " AND FORM_CAMPO.SUBTIPO <> " & TiposDeAtributos.TipoEditor
    sConsulta = sConsulta & " ORDER BY FORM_CAMPO.ORDEN"
    
    rs.Open sConsulta, moConexion.ADOCon, adOpenForwardOnly, adLockReadOnly

    Set m_oCamposVisibles = Nothing
    Set m_oCamposVisibles = New CNotificadoAccionCampos
    Set m_oCamposVisibles.Conexion = moConexion

    If rs.eof Then
        rs.Close
        Set rs = Nothing
        Set oGestorParametros = Nothing
        Set oIdiomas = Nothing
        Exit Sub

    Else
        
        Do While Not rs.eof
            Set oDenominaciones = Nothing
            Set oDenominaciones = New CMultiidiomas
            Set oDenominaciones.Conexion = moConexion
            For Each oIdi In oIdiomas
                oDenominaciones.Add oIdi.Cod, rs.Fields("DEN_" & oIdi.Cod).Value
            Next
            bVisible = (rs.Fields("VISIBLE").Value = 1)
            m_oCamposVisibles.Add rs.Fields("CAMPO").Value, rs.Fields("NOTIFICADO").Value, oDenominaciones, bVisible, (rs.Fields("SUBTIPO") = 9)
            rs.MoveNext
        Loop

        rs.Close
        Set rs = Nothing

    End If
        Set oGestorParametros = Nothing
        Set oIdiomas = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CNotificadoAccion", "CargarCamposDesgloseVisibles", ERR, Erl)
      Exit Sub
   End If

End Sub

Public Sub DevolverRolBloqueNotificacion(ByVal idEnlace As Long, ByVal iOrigen As Integer)
    Dim adoComm As adodb.Command
    Dim adoParam As adodb.Parameter
    Dim RolNotificacion As Integer
    Dim BloqueNotificacion As Integer
    
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set adoComm = New adodb.Command
    adoComm.CommandType = adCmdStoredProc
    adoComm.CommandText = "FSPM_DEVOLVER_ROLBLOQUE_NOTIFICACION"
    Set adoComm.ActiveConnection = moConexion.ADOCon
    Set adoParam = adoComm.CreateParameter("NOTIFICADO", adInteger, adParamInput, , idEnlace)
    adoComm.Parameters.Append adoParam
    Set adoParam = adoComm.CreateParameter("ORIGEN", adInteger, adParamInput, , iOrigen)
    adoComm.Parameters.Append adoParam
    Set adoParam = adoComm.CreateParameter("ROL", adInteger, adParamOutput)
    adoComm.Parameters.Append adoParam
    Set adoParam = adoComm.CreateParameter("BLOQUE", adInteger, adParamOutput)
    adoComm.Parameters.Append adoParam
    
    adoComm.Execute
    If moConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR
    
    
    m_lRolNotificacion = adoComm.Parameters(2).Value
    m_lBloqueNotificacion = adoComm.Parameters(3).Value
    
    Set adoParam = Nothing
    Set adoComm = Nothing
    Exit Sub

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CNotificadoAccion", "DevolverRolBloqueNotificacion", ERR, Erl)
      Exit Sub
   End If
    
End Sub

Public Function ValidarVisibilidadConfiguracionCamposRol(ByVal idNotificado As Long, ByVal iTipo As Integer, ByVal iOrigen As Integer)
    Dim adoComm As adodb.Command
    Dim adoParam As adodb.Parameter
    Dim RolNotificacion As Integer
    Dim BloqueNotificacion As Integer
    
    
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set adoComm = New adodb.Command
    adoComm.CommandType = adCmdStoredProc
    adoComm.CommandText = "FSPM_VALIDAR_VISIBILIDAD_CONFIGURACIONCAMPOSROL"
    Set adoComm.ActiveConnection = moConexion.ADOCon
    Set adoParam = adoComm.CreateParameter("NOTIFICADO", adInteger, adParamInput, , idNotificado)
    adoComm.Parameters.Append adoParam
    Set adoParam = adoComm.CreateParameter("TIPO", adInteger, adParamInput, , iTipo)
    adoComm.Parameters.Append adoParam
    Set adoParam = adoComm.CreateParameter("ORIGEN", adInteger, adParamInput, , iOrigen)
    adoComm.Parameters.Append adoParam
    Set adoParam = adoComm.CreateParameter("VISIBLE", adInteger, adParamOutput)
    adoComm.Parameters.Append adoParam
    
    
    adoComm.Execute
    If moConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR
    
    ValidarVisibilidadConfiguracionCamposRol = adoComm.Parameters(3).Value
    Set adoParam = Nothing
    Set adoComm = Nothing
    Exit Function
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CNotificadoAccion", "ValidarVisibilidadConfiguracionCamposRol", ERR, Erl)
      Exit Function
   End If

End Function

Public Sub CambioDeRoloTipoNotificado(ByVal idNotificado As Long, ByVal iOrigen As Integer)
    Dim oadorecordset As adodb.Recordset
    Dim sql As String
    Dim tableName As String
    Dim foreingKeyName As String
    

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If iOrigen = OrigenNotificadoAccion.Enlace Then
        tableName = "PM_NOTIFICADO_ENLACE"
        foreingKeyName = "ENLACE"
    ElseIf iOrigen = OrigenNotificadoAccion.Accion Then
        tableName = "PM_NOTIFICADO_ACCION"
        foreingKeyName = "ACCION"
    Else
        tableName = "PM_NOTIFICADO_AVISO_EXPIRACION"
        foreingKeyName = "AVISO_EXPIRACION_CAMPOS"
    End If
    
    
    'sql = "UPDATE " & tableName & " SET CONFIGURACION_ROL=0 "
    'If idNotificado > 0 Then
        'sql = sql & " WHERE ID= " & idNotificado
    'End If
        
    'Set oadorecordset = New ADODB.Recordset
    'oadorecordset.CursorLocation = adUseClient
    'oadorecordset.Open sql, moConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    
    sql = "UPDATE " & tableName & "_CAMPOS SET VISIBLE=1 "
    If idNotificado > 0 Then
        sql = sql & " WHERE NOTIFICADO= " & idNotificado
    End If
        
    Set oadorecordset = New adodb.Recordset
    oadorecordset.CursorLocation = adUseClient
    oadorecordset.Open sql, moConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    
    oadorecordset.ActiveConnection = Nothing
        
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CNotificadoAccion", "CambioDeRoloTipoNotificado", ERR, Erl)
      Exit Sub
   End If

End Sub
