VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CFormularios"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

Private mCol As Collection

Private m_oConexion As CConexion
Private m_bEOF As Boolean

Public Property Get eof() As Boolean
    eof = m_bEOF
End Property
Friend Property Let eof(ByVal b As Boolean)
    m_bEOF = b
End Property

Public Function Add(ByVal Id As Long, ByVal Den As String, Optional ByVal bMultiidioma As Boolean = False, Optional ByVal sMon As String, Optional ByVal FechaActual As Variant, Optional ByVal vIndice As Variant) As CFormulario
        
    Dim objnewmember As CFormulario
    
    Set objnewmember = New CFormulario
    
    Set objnewmember.Conexion = m_oConexion
    
    objnewmember.Id = Id
    objnewmember.Den = Den
    objnewmember.Multiidioma = bMultiidioma
    
    'objnewmember.Moneda = sMon
    
    If IsMissing(FechaActual) Then
        objnewmember.FecAct = Null
    Else
        objnewmember.FecAct = FechaActual
    End If

    If Not IsMissing(vIndice) And Not IsNull(vIndice) Then
        objnewmember.Indice = vIndice
        mCol.Add objnewmember, CStr(vIndice)
    Else
        mCol.Add objnewmember, CStr(Id)
    End If

    Set Add = objnewmember
    
    Set objnewmember = Nothing

End Function

Friend Property Set Conexion(ByVal vData As CConexion)
    Set m_oConexion = vData
End Property

Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property

Public Property Get Item(vntIndexKey As Variant) As CFormulario

    ''' * Objetivo: Recuperar una moneda de la coleccion
    ''' * Recibe: Indice de la moneda a recuperar
    ''' * Devuelve: Moneda correspondiente
        
On Error GoTo NoSeEncuentra:

    ''' Devolvemos el item de la coleccion privada
    
    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:

    Set Item = Nothing
    
End Property

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4

    ''' ! Pendiente de revision, objetivo desconocido
    
    Set NewEnum = mCol.[_NewEnum]

End Property
Public Sub Remove(vntIndexKey As Variant)

    ''' * Objetivo: Eliminar una moneda de la coleccion
    ''' * Recibe: Indice de la moneda a eliminar
    ''' * Devuelve: Nada
   
    mCol.Remove vntIndexKey
    
End Sub
Private Sub Class_Initialize()

    ''' * Objetivo: Crear la coleccion
    Set mCol = New Collection
    
End Sub
Private Sub Class_Terminate()

    ''' * Objetivo: Limpiar memoria
    
    Set mCol = Nothing
    Set m_oConexion = Nothing
End Sub

Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
     Count = mCol.Count
End If

End Property


Public Sub CargarTodosFormularios(Optional ByVal CaracteresInicialesCod As String, Optional ByVal UsarIndice As Boolean, Optional ByVal CoincidenciaTotal As Boolean)
    Dim rs As New adodb.Recordset
    Dim sConsulta As String
    Dim fldId As adodb.Field
    Dim fldDen As adodb.Field
    Dim fldMultiidi As adodb.Field
    Dim fldFecAct As adodb.Field
    Dim lIndice As Long
    
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CFormularios.CargarTodosFormularios", "No se ha establecido la conexion"
        Exit Sub
    End If
    
    sConsulta = "SELECT ID,COD,FECACT,MULTIIDIOMA FROM FORMULARIO WITH (NOLOCK) WHERE BAJALOG = 0 "
    
    If Not (CaracteresInicialesCod = "") Then
        If CoincidenciaTotal Then
            sConsulta = sConsulta & " AND COD ='" & DblQuote(CaracteresInicialesCod) & "'"
        Else
            sConsulta = sConsulta & " AND COD >='" & DblQuote(CaracteresInicialesCod) & "'"
        End If
    End If
    sConsulta = sConsulta & " ORDER BY COD"
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
         
    Set mCol = Nothing
    Set mCol = New Collection
        
    If Not rs.eof Then
        Set fldId = rs.Fields("ID")
        Set fldDen = rs.Fields("COD")
        Set fldFecAct = rs.Fields("FECACT")
        Set fldMultiidi = rs.Fields("MULTIIDIOMA")
        
        If UsarIndice Then
            lIndice = 0
            While Not rs.eof
                Me.Add fldId.Value, fldDen.Value, SQLBinaryToBoolean(fldMultiidi.Value), , fldFecAct.Value, lIndice
                rs.MoveNext
                lIndice = lIndice + 1
            Wend
        Else
            While Not rs.eof
                Me.Add fldId.Value, fldDen.Value, SQLBinaryToBoolean(fldMultiidi.Value), , fldFecAct.Value
                rs.MoveNext
            Wend
        End If
                
        Set fldId = Nothing
        Set fldDen = Nothing
        Set fldMultiidi = Nothing
        Set fldFecAct = Nothing
    End If
    
    rs.Close
    Set rs = Nothing
End Sub
