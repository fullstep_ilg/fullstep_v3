VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CConfCumplimentacion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
''' *** Clase: CConfCumplimentacion
''' *** Creacion: 30/03/2005 (MMV)

Option Explicit

Implements IBaseDatos

Private m_lID As Long
Private m_lIndice As Long
Private m_bVisible As Boolean
Private m_bEscritura As Boolean
Private m_bObl As Boolean
Private m_vFecAct As Variant
Private m_vOrden As Variant

Private m_oSolicitud As CSolicitud
Private m_oCampo As CFormItem

Private m_iSolicitanteOProv As Variant
Private m_vPaso As Variant

Private m_EsEstado As Variant

''' Conexion
Private m_oConexion As CConexion

''' Control de errores
Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum

Public Property Get Id() As Long
    Id = m_lID
End Property

Public Property Let Id(ByVal lDato As Long)
    m_lID = lDato
End Property

Public Property Get Visible() As Boolean
    Visible = m_bVisible
End Property

Public Property Let Visible(ByVal bVisible As Boolean)
    m_bVisible = bVisible
End Property

Public Property Get Escritura() As Boolean
    Escritura = m_bEscritura
End Property

Public Property Let Escritura(ByVal bEscr As Boolean)
    m_bEscritura = bEscr
End Property

Public Property Get Obligatorio() As Boolean
    Obligatorio = m_bObl
End Property

Public Property Let Obligatorio(ByVal bObl As Boolean)
    m_bObl = bObl
End Property

Public Property Let Orden(ByVal vData As Variant)
    m_vOrden = vData
End Property

Public Property Get Orden() As Variant
    Orden = m_vOrden
End Property

Public Property Get Indice() As Long
    Indice = m_lIndice
End Property

Public Property Let Indice(ByVal lInd As Long)
    m_lIndice = lInd
End Property

Public Property Let FecAct(ByVal vData As Variant)
    m_vFecAct = vData
End Property

Public Property Get FecAct() As Variant
    FecAct = m_vFecAct
End Property

Friend Property Set Conexion(ByVal vData As CConexion)
    Set m_oConexion = vData
End Property

Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property

Public Property Get Solicitud() As CSolicitud
    Set Solicitud = m_oSolicitud
End Property
Public Property Set Solicitud(ByVal dato As CSolicitud)
    Set m_oSolicitud = dato
End Property

Public Property Get Paso() As Variant
    Paso = m_vPaso
End Property
Public Property Let Paso(ByVal dato As Variant)
    m_vPaso = dato
End Property

Public Property Get Campo() As CFormItem
    Set Campo = m_oCampo
End Property
Public Property Set Campo(ByVal dato As CFormItem)
    Set m_oCampo = dato
End Property

Public Property Get SolicitanteProv() As Variant
    SolicitanteProv = m_iSolicitanteOProv
End Property
Public Property Let SolicitanteProv(ByVal dato As Variant)
    m_iSolicitanteOProv = dato
End Property

Public Property Get EsEstado() As Variant
    EsEstado = m_EsEstado
End Property
Public Property Let EsEstado(ByVal dato As Variant)
    m_EsEstado = dato
End Property

Private Sub Class_Terminate()
    Set m_oSolicitud = Nothing
    Set m_oCampo = Nothing
    Set m_oConexion = Nothing
End Sub

Private Function IBaseDatos_AnyadirABaseDatos() As TipoErrorSummit
    'No se usa
End Function

Private Function IBaseDatos_CambiarCodigo(CodigoNuevo As Variant) As TipoErrorSummit
    'No se usa
End Function

Private Sub IBaseDatos_CancelarEdicion()
    'No se usa
End Sub

Private Function IBaseDatos_ComprobarExistenciaEnBaseDatos() As Boolean
    'No se usa
End Function

Private Function IBaseDatos_EliminarDeBaseDatos() As TipoErrorSummit
    'No se usa
End Function

Private Function IBaseDatos_FinalizarEdicionEliminando() As TipoErrorSummit
    'No se usa
End Function

Private Function IBaseDatos_FinalizarEdicionModificando() As TipoErrorSummit
Dim TESError As TipoErrorSummit
Dim btrans As Boolean
Dim sConsulta As String
Dim rs As adodb.Recordset

    TESError.NumError = TESnoerror

    ''' Precondicion
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CConfCumplimentacion.FinalizarEdicionModificando", "No se ha establecido la conexion"
    End If
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:


    sConsulta = "SELECT FECACT FROM CONF_CUMPLIMENTACION WITH (NOLOCK) WHERE ID=" & m_lID
    
    Set rs = New adodb.Recordset
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly

    If rs.eof Then
        rs.Close
        Set rs = Nothing
        TESError.NumError = TESDatoEliminado
        TESError.Arg1 = 152
        IBaseDatos_FinalizarEdicionModificando = TESError
        Exit Function
    End If
    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
    btrans = True

    sConsulta = "UPDATE CONF_CUMPLIMENTACION SET VISIBLE=" & BooleanToSQLBinary(m_bVisible)
    sConsulta = sConsulta & ",ESCRITURA=" & BooleanToSQLBinary(m_bEscritura)
    sConsulta = sConsulta & ",OBLIGATORIO=" & BooleanToSQLBinary(m_bObl)
    sConsulta = sConsulta & " WHERE ID=" & m_lID

    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    btrans = False
    
    rs.Requery
    m_vFecAct = rs(0).Value
    rs.Close
    Set rs = Nothing
    TESError.NumError = TESnoerror
    IBaseDatos_FinalizarEdicionModificando = TESError

    Exit Function


Error_Cls:
    TESError = basErrores.TratarError(m_oConexion.ADOCon.Errors)

    If btrans Then
        m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
    End If

    Resume Salir:

Salir:

    On Error Resume Next
    If Not rs Is Nothing Then
        rs.Close
    End If

    IBaseDatos_FinalizarEdicionModificando = TESError
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CConfCumplimentacion", "IBaseDatos_FinalizarEdicionModificando", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If

End Function

Private Function IBaseDatos_IniciarEdicion(Optional ByVal Bloquear As Boolean, Optional ByVal UsuarioBloqueo As String) As TipoErrorSummit
    
End Function

