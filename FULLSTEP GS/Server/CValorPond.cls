VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CValorPond"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CValorPond **********************************
'*             Creada : 26/2/2002
'***************************************************************

Option Explicit
Implements IBaseDatos

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

Private m_lIdAtributo As Long
Private m_vAnyoProce As Variant
Private m_vGMN1Proce  As Variant
Private m_vCodProce As Variant
Private m_vCodGrupo As Variant
Private m_lIdOrden As Long
Private m_vIndice As Variant
Private m_vValorLista As Variant
Private m_vValorPond As Variant
Private m_vValorDesde As Variant
Private m_vValorHasta As Variant
Private m_vFechaActualizacion As Variant

Private m_adores As adodb.Recordset

Private m_oConexion As CConexion
Private m_AdoRecordset As adodb.Recordset
Public Property Let IDAtributo(ByVal var As Long)
    m_lIdAtributo = var
End Property
Public Property Get IDAtributo() As Long
    IDAtributo = m_lIdAtributo
End Property
Public Property Let AnyoProce(ByVal var As Variant)
    m_vAnyoProce = var
End Property
Public Property Get AnyoProce() As Variant
    AnyoProce = m_vAnyoProce
End Property
Public Property Let GMN1Proce(ByVal var As Variant)
    m_vGMN1Proce = var
End Property
Public Property Get GMN1Proce() As Variant
    GMN1Proce = m_vGMN1Proce
End Property
Public Property Let CodProce(ByVal var As Variant)
    m_vCodProce = var
End Property
Public Property Get CodProce() As Variant
    CodProce = m_vCodProce
End Property
Public Property Let CodGrupo(ByVal var As Variant)
    m_vCodGrupo = var
End Property
Public Property Get CodGrupo() As Variant
    CodGrupo = m_vCodGrupo
End Property
Public Property Let IdOrden(ByVal var As Long)
    m_lIdOrden = var
End Property
Public Property Get IdOrden() As Long
    IdOrden = m_lIdOrden
End Property
Public Property Let Indice(ByVal varIndice As Variant)
    m_vIndice = varIndice
End Property
Public Property Get Indice() As Variant
    Indice = m_vIndice
End Property
Public Property Let ValorLista(ByVal var As Variant)
    m_vValorLista = var
End Property
Public Property Get ValorLista() As Variant
    ValorLista = m_vValorLista
End Property
Public Property Let ValorPond(ByVal var As Variant)
    m_vValorPond = var
End Property
Public Property Get ValorPond() As Variant
    ValorPond = m_vValorPond
End Property
Public Property Let ValorDesde(ByVal var As Variant)
    m_vValorDesde = var
End Property
Public Property Get ValorDesde() As Variant
    ValorDesde = m_vValorDesde
End Property
Public Property Let ValorHasta(ByVal var As Variant)
    m_vValorHasta = var
End Property
Public Property Get ValorHasta() As Variant
    ValorHasta = m_vValorHasta
End Property
Public Property Let FechaActualizacion(ByVal var As Variant)
    m_vFechaActualizacion = var
End Property
Public Property Get FechaActualizacion() As Variant
    FechaActualizacion = m_vFechaActualizacion
End Property
Friend Property Set Conexion(ByVal con As CConexion)
Set m_oConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = m_oConexion
End Property

Private Sub Class_Terminate()
    
    Set m_AdoRecordset = Nothing
    Set m_oConexion = Nothing
   
End Sub

Private Function IBaseDatos_AnyadirABaseDatos() As TipoErrorSummit

End Function

Private Function IBaseDatos_CambiarCodigo(CodigoNuevo As Variant) As TipoErrorSummit

End Function

Private Sub IBaseDatos_CancelarEdicion()

End Sub

Private Function IBaseDatos_ComprobarExistenciaEnBaseDatos() As Boolean

End Function

Private Function IBaseDatos_EliminarDeBaseDatos() As TipoErrorSummit
Dim TESError As TipoErrorSummit
'Dim bTrans As Boolean
Dim sConsulta As String
'Dim rs As ADODB.Recordset
'Dim adoCom As ADODB.Command
'Dim ador As ADODB.Recordset
'Dim oParam As ADODB.Parameter

'******************* Precondicion *******************
If m_oConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CLineaCatalogo.EliminarDeBaseDatos", "No se ha establecido la conexion"
    Exit Function
End If
'*****************************************************
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
TESError.NumError = TESnoerror

If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
    
    
    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
    
    If Not IsNull(m_vValorDesde) And Not IsNull(m_vValorHasta) Then
        sConsulta = "DELETE FROM DEF_ATRIB_POND WHERE ATRIB=" & m_lIdAtributo & " AND ((DESDE_NUM=" & m_vValorDesde & " AND HASTA_NUM=" & m_vValorHasta & ") OR (DESDE_FEC=" & m_vValorDesde & " AND HASTA_FEC=" & m_vValorHasta & "))"
        m_oConexion.ADOCon.Execute sConsulta
        If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    Else
        Dim ador As adodb.Recordset
        Dim Valor As Variant
        Dim Tipo As Integer
        Set ador = New adodb.Recordset
        sConsulta = "SELECT DA.TIPO, "
        sConsulta = sConsulta & " CASE"
        sConsulta = sConsulta & " WHEN DA.TIPO = 1 THEN CONVERT(VARCHAR(300),DAL.VALOR_TEXT)"
        sConsulta = sConsulta & " WHEN DA.TIPO = 2 THEN CONVERT(VARCHAR(300),DAL.VALOR_NUM)"
        sConsulta = sConsulta & " WHEN DA.TIPO = 3 THEN CONVERT(VARCHAR(300),DAL.VALOR_FEC)"
        sConsulta = sConsulta & " End"
        sConsulta = sConsulta & " FROM DEF_ATRIB_LISTA DAL WITH(NOLOCK)"
        sConsulta = sConsulta & " INNER JOIN DEF_ATRIB DA WITH(NOLOCK) ON DAL.ATRIB=DA.ID"
        sConsulta = sConsulta & " WHERE DAL.ATRIB = " & m_lIdAtributo & " AND DAL.ORDEN=" & m_lIdOrden
        ador.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
        If Not ador.eof Then
            Valor = ador(0).Value
            Tipo = ador(1).Value
        Else
            Valor = 0
            Tipo = 0
        End If
        ador.Close
        If Tipo > 0 Then
            Dim sConsulta2 As String
            Select Case Tipo
                Case TiposDeAtributos.TipoString:
                    sConsulta2 = " AND VALOR_TEXT = " & Valor
                Case TiposDeAtributos.TipoNumerico:
                    sConsulta2 = " AND VALOR_NUM = " & Valor
                Case TiposDeAtributos.TipoFecha:
                    sConsulta2 = " AND VALOR_FEC = " & Valor
                Case TiposDeAtributos.TipoBoolean:
                    sConsulta2 = " AND VALOR_BOOL = " & Valor
            End Select
        End If
        m_oConexion.ADOCon.Execute "DELETE FROM DEF_ATRIB_LISTA WHERE ATRIB=" & m_lIdAtributo & " AND ORDEN=" & m_lIdOrden
        If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    End If
    
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    
    IBaseDatos_EliminarDeBaseDatos = TESError
    Exit Function
        
Error_Cls:
        
    IBaseDatos_EliminarDeBaseDatos = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        'Call g_oErrores.TratarError("Class Module", "CValorPond", "IBaseDatos_EliminarDeBaseDatos", ERR, Erl)
        GoTo Error_Cls
        Exit Function
    End If

End Function

Private Function IBaseDatos_FinalizarEdicionEliminando() As TipoErrorSummit

End Function

Private Function IBaseDatos_FinalizarEdicionModificando() As TipoErrorSummit

End Function

''' <summary>Esta funci�n inicia la edici�n del resulset</summary>
'''<param optional name="Bloquear">Si se bloquea o no</param>
'''<param optional name="UsuarioBloqueo">Usuario del bloqueo</param>
''' <returns>TipoErrorSummit con el error si lo hay</returns>
''' <remarks>Llamada desde: FSGClient
''' Tiempo m�ximo: <1 seg </remarks>
''' <revision>JVS 13/01/2012</revision>
Private Function IBaseDatos_IniciarEdicion(Optional ByVal Bloquear As Boolean, Optional ByVal UsuarioBloqueo As String) As TipoErrorSummit
    Dim TESError As TipoErrorSummit
    Dim sSQL As String
    
    
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CVALORPOND.IniciarEdicion", "No se ha establecido la conexion"
        Exit Function
    End If
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    TESError.NumError = TESnoerror
     
    
    Set m_adores = New adodb.Recordset
    'CALIDAD Se obtiene la Fecha de Actualizaci�n con bloqueo para recuperar la verdadera �ltima fecha de actualizaci�n
    m_adores.Open ("SELECT FECACT FROM DEF_ATRIB_LISTA WHERE ATRIB=" & m_lIdAtributo & " AND ORDEN = " & m_lIdOrden), m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
    
    If m_adores.eof Then
        m_adores.Close
        Set m_adores = Nothing
        TESError.NumError = TESDatoEliminado
        TESError.Arg1 = 139  ' Tabla DEF_ATRIB_LISTA
        IBaseDatos_IniciarEdicion = TESError
        Exit Function
    End If
       
    If m_vFechaActualizacion <> NullToStr(m_adores("FECACT")) Then
        m_adores.Close
        Set m_adores = Nothing
        TESError.NumError = TESInfActualModificada
    End If
    
    IBaseDatos_IniciarEdicion = TESError
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CValorPond", "IBaseDatos_IniciarEdicion", ERR, Erl)
        Exit Function
    End If

End Function
