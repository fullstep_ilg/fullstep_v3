VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CAlmacenes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum

Private mCol As Collection
Private mvarConexion As CConexion
Private mvarEOF As Boolean

Public Function CargarTodosLosAlmacenes(Optional ByVal sCodAlmacen As String = "" _
, Optional ByVal sCodCentro As Variant, Optional ByVal bOrdenadoPorDen As Boolean = True _
, Optional ByVal bObligJoin As Boolean = False, Optional ByVal lIDAlmacen As Long = -1) As adodb.Recordset

    Dim rs As New adodb.Recordset
    Dim fldId As adodb.Field
    Dim fldCod As adodb.Field
    Dim fldDen As adodb.Field
    Dim sConsulta As String
    
    '********* Precondicion **************************************
    If mvarConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CAlmacenes", "No se ha establecido la conexion"
        Exit Function
    End If
    '*************************************************************
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

    If IsMissing(sCodCentro) And Not bObligJoin Then
        sConsulta = "SELECT A.ID, A.COD, A.DEN "
        sConsulta = sConsulta & " FROM ALMACEN A WITH (NOLOCK) "
        sConsulta = sConsulta & " WHERE 1 = 1 "
        If lIDAlmacen <> -1 Then
            sConsulta = sConsulta & " AND A.ID = " & lIDAlmacen
        End If
        
        If sCodAlmacen <> "" Then
            sConsulta = sConsulta & " AND A.COD='" & sCodAlmacen & "'"
        End If
    Else
        sConsulta = "SELECT A.ID, A.COD, A.DEN"
        sConsulta = sConsulta & " FROM CENTROS_ALMACEN CA WITH (NOLOCK)"
        sConsulta = sConsulta & " INNER JOIN ALMACEN A WITH (NOLOCK) ON A.ID = CA.ALMACEN"
        sConsulta = sConsulta & " INNER JOIN CENTROS C WITH (NOLOCK) ON C.COD = CA.CENTROS"
        sConsulta = sConsulta & " WHERE CA.CENTROS = '" & sCodCentro & "'"
        If sCodAlmacen <> "" Then
            sConsulta = sConsulta & " AND A.COD='" & sCodAlmacen & "'"
        End If
        
        If lIDAlmacen <> -1 Then
            sConsulta = sConsulta & " AND A.ID = " & lIDAlmacen
        End If
    End If
    
    If bOrdenadoPorDen Then
        sConsulta = sConsulta & " ORDER BY A.DEN"
    Else
        sConsulta = sConsulta & " ORDER BY A.ID"
    End If
    
    rs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
          
    If Not rs.eof Then
        
        Set fldId = rs.Fields("ID")
        Set fldCod = rs.Fields("COD")
        Set fldDen = rs.Fields("DEN")
        
        Set mCol = Nothing
        Set mCol = New Collection
        
        While Not rs.eof
            Me.Add fldId.Value, fldCod.Value, fldDen.Value
            rs.MoveNext
        Wend
        
        Set fldId = Nothing
        Set fldCod = Nothing
        Set fldDen = Nothing
        
        rs.MoveFirst
    End If
    
    Set CargarTodosLosAlmacenes = rs
    Set rs.ActiveConnection = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CAlmacenes", "CargarTodosLosAlmacenes", ERR, Erl)
        Exit Function
    End If
End Function


Public Function CargarAlmacenes(Optional ByVal bOrdenadosPor As TipoOrdenacionAlmacen = OrdPorCodigo) As adodb.Recordset

    ' <summary>
    ' Seleccionar todos los almacenes existentes en la tabla correspondiente
    ' </summary>
    ' <param name="bOrdenadoPor">Orden en el cual se realiza la selecci�n</param>
    ' <returns>Recordset con el conjunto de almacenes</returns>
    ' <remarks>frmESTRORG.frm</remarks>


    Dim rs As New adodb.Recordset
    Dim fldId As adodb.Field
    Dim fldCod As adodb.Field
    Dim fldDen As adodb.Field
    Dim sConsulta As String
    
    '********* Precondici�n **************************************
    If mvarConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CAlmacenes", "No se ha establecido la conexi�n"
        Exit Function
    End If
    '*************************************************************
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

    sConsulta = "SELECT A.ID, A.COD, A.DEN "
    sConsulta = sConsulta & " FROM ALMACEN A WITH (NOLOCK) "
    sConsulta = sConsulta & " WHERE 1 = 1 "
            
    Select Case bOrdenadosPor
    
        Case bOrdenadosPor = OrdPorCodigo
            sConsulta = sConsulta & "ORDER BY A.COD"
        
        Case bOrdenadosPor = OrdPorDenominacion
            sConsulta = sConsulta & "ORDER BY A.DEN"
    
        Case Else
            sConsulta = sConsulta & "ORDER BY A.ID"
            
    End Select
    
    rs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
              
    If Not rs.eof Then

        Set fldId = rs.Fields("ID")
        Set fldCod = rs.Fields("COD")
        Set fldDen = rs.Fields("DEN")

        Set mCol = Nothing
        Set mCol = New Collection

        While Not rs.eof
            Me.Add fldId.Value, fldCod.Value, fldDen.Value
            rs.MoveNext
        Wend

        Set fldId = Nothing
        Set fldCod = Nothing
        Set fldDen = Nothing

        rs.MoveFirst
    End If
    
    Set CargarAlmacenes = rs
    Set rs.ActiveConnection = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CAlmacenes", "CargarAlmacenes", ERR, Erl)
        Exit Function
    End If
End Function

'EPB: ESTA FUNCI�N LA HA HECHO JRM, YO SE LA HE CORREGIDO EN MI EQUIPO EXPLICANDOSELO, PERO LA TIENE QUE COMENTAR EL
Public Sub CargarAlmacenesDestino(ByVal sDestino As String)

    Dim rs As New adodb.Recordset
    Dim fldId As adodb.Field
    Dim fldCod As adodb.Field
    Dim fldDen As adodb.Field
    Dim sConsulta As String
    
    '********* Precondici�n **************************************
    If mvarConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CAlmacenes", "No se ha establecido la conexi�n"
        Exit Sub
    End If
    '*************************************************************
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
    sConsulta = ""
    
    sConsulta = "SELECT A.ID, A.COD, A.DEN"
    sConsulta = sConsulta & " FROM DEST_ALMACEN DA WITH (NOLOCK) INNER JOIN ALMACEN A WITH (NOLOCK) ON DA.ALMACEN=A.ID"
    sConsulta = sConsulta & " WHERE DA.DEST = '" & DblQuote(sDestino) & "'"
                    
    rs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
                  
    If Not rs.eof Then

        Set fldId = rs.Fields("ID")
        Set fldCod = rs.Fields("COD")
        Set fldDen = rs.Fields("DEN")

        Set mCol = Nothing
        Set mCol = New Collection

        While Not rs.eof
            Me.Add fldId.Value, fldCod.Value, fldDen.Value
            rs.MoveNext
        Wend

        Set fldId = Nothing
        Set fldCod = Nothing
        Set fldDen = Nothing
    End If
    
    rs.Close
    Set rs = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CAlmacenes", "CargarAlmacenesDestino", ERR, Erl)
        Exit Sub
    End If
    
End Sub
''' <summary>Esta funci�n carga los almacenes del centro de aprovisionamiento (siempre que UsarOrgCompras=1) </summary>
''' <param name="sCentro">C�digo del centro de aprovisionamiento de la l�nea de pedido</param>
''' <returns></returns>
''' <remarks>Llamada desde: frmSeguimiento FSGClient
''' Tiempo m�ximo: 0.1 sec </remarks>
''' <remarks>Revisado por ngo</remarks>
Public Sub CargarAlmacenesCentro(ByVal sCentro As String)

    Dim rs As New adodb.Recordset
    Dim fldId As adodb.Field
    Dim fldCod As adodb.Field
    Dim fldDen As adodb.Field
    Dim sConsulta As String
    
    '********* Precondici�n **************************************
    If mvarConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CAlmacenes", "No se ha establecido la conexi�n"
        Exit Sub
    End If
    '*************************************************************
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
    sConsulta = ""
    
    sConsulta = "SELECT A.ID, A.COD, A.DEN"
    sConsulta = sConsulta & " FROM CENTROS_ALMACEN CA WITH (NOLOCK) INNER JOIN ALMACEN A WITH (NOLOCK) ON CA.ALMACEN=A.ID"
    sConsulta = sConsulta & " WHERE CA.CENTROS = '" & DblQuote(sCentro) & "'"
                    
    rs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
                  
    If Not rs.eof Then

        Set fldId = rs.Fields("ID")
        Set fldCod = rs.Fields("COD")
        Set fldDen = rs.Fields("DEN")

        Set mCol = Nothing
        Set mCol = New Collection

        While Not rs.eof
            Me.Add fldId.Value, fldCod.Value, fldDen.Value
            rs.MoveNext
        Wend

        Set fldId = Nothing
        Set fldCod = Nothing
        Set fldDen = Nothing
    End If
    
    rs.Close
    Set rs = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CAlmacenes", "CargarAlmacenesCentro", ERR, Erl)
        Exit Sub
    End If
    
End Sub

Public Sub Add(ByVal iID As Variant, ByVal sCod As Variant, ByVal sDen As String, Optional ByVal varIndice As Variant)
    
    'create a new object
    Dim objnewmember As CAlmacen
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New CAlmacen


    'set the properties passed into the method
    objnewmember.Id = iID
    objnewmember.Cod = sCod
    objnewmember.Den = sDen
    
    Set objnewmember.Conexion = mvarConexion
    
    If IsNull(varIndice) Or IsMissing(varIndice) Then
        mCol.Add objnewmember, CStr(iID)
    Else
        mCol.Add objnewmember, CStr(varIndice)
    End If


    'return the object created
    Set objnewmember = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CAlmacenes", "Add", ERR, Erl)
        Exit Sub
    End If

End Sub

Public Property Get Item(vntIndexKey As Variant) As CAlmacen
    On Error GoTo NoSeEncuentra:
    Set Item = mCol(vntIndexKey)
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
End Property

Public Property Get eof() As Boolean
    eof = mvarEOF
End Property

Public Property Get Count() As Long
    
    If mCol Is Nothing Then
        Count = 0
    Else
        Count = mCol.Count
    End If
    
End Property


Public Sub Remove(vntIndexKey As Variant)
    mCol.Remove vntIndexKey
End Sub

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
    Set NewEnum = mCol.[_NewEnum]
End Property

Friend Property Set Conexion(ByVal con As CConexion)
Set mvarConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = mvarConexion
End Property

Private Sub Class_Initialize()
    'creates the collection when this class is created
  
    Set mCol = New Collection
End Sub

Private Sub Class_Terminate()
    'destroys collection when this class is terminated
    Set mCol = Nothing
    Set mvarConexion = Nothing
End Sub

Public Function AsignarAlmacenesADestinos(ByVal sDest As String, ByVal oSelec As CAlmacenes, ByVal oDesel As CAlmacenes) As TipoErrorSummit
Dim TESError As TipoErrorSummit
Dim oAlm As CAlmacen
Dim adoComm As adodb.Command
Dim adoParam As adodb.Parameter
Dim sConsulta As String

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    TESError.NumError = TESnoerror
    
    ''' Precondicion
    
    If mvarConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CAlmacenes.AsignarAlmacenesADestinos", "No se ha establecido la conexion"
        Exit Function
    End If
    
If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
    
    
    mvarConexion.ADOCon.Execute "BEGIN TRANSACTION"
       
    For Each oAlm In oSelec
        ''' Ejecutar insercion
        sConsulta = "INSERT INTO DEST_ALMACEN(DEST,ALMACEN) VALUES (?,?)"
        Set adoComm = New adodb.Command
        Set adoComm.ActiveConnection = mvarConexion.ADOCon
        
        Set adoParam = adoComm.CreateParameter("DEST", adVarChar, adParamInput, gLongitudesDeCodigos.giLongCodDEST, sDest)
        adoComm.Parameters.Append adoParam
        Set adoParam = adoComm.CreateParameter("ALMACEN", adInteger, adParamInput, , oAlm.Id)
        adoComm.Parameters.Append adoParam
        adoComm.CommandText = sConsulta
        adoComm.Execute
        If mvarConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
        Set adoComm = Nothing
    Next

    For Each oAlm In oDesel
        ''' Ejecutar delete
        sConsulta = "DELETE FROM DEST_ALMACEN WHERE DEST=? AND ALMACEN=?"
        
        Set adoComm = New adodb.Command
        Set adoComm.ActiveConnection = mvarConexion.ADOCon
        
        Set adoParam = adoComm.CreateParameter("DEST", adVarChar, adParamInput, gLongitudesDeCodigos.giLongCodDEST, sDest)
        adoComm.Parameters.Append adoParam
        Set adoParam = adoComm.CreateParameter("ALMACEN", adInteger, adParamInput, , oAlm.Id)
        adoComm.Parameters.Append adoParam
        adoComm.CommandText = sConsulta
        adoComm.Execute
        If mvarConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
        Set adoComm = Nothing
    Next


    ''' Terminar transaccion
    mvarConexion.ADOCon.Execute "COMMIT TRANSACTION"
   
    AsignarAlmacenesADestinos = TESError
    
fin:
    Set adoComm = Nothing
    Set adoParam = Nothing
    Exit Function
    
Error_Cls:
         
                 
    AsignarAlmacenesADestinos = basErrores.TratarError(mvarConexion.ADOCon.Errors)
    mvarConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
    Resume fin
        
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        'Call g_oErrores.TratarError("Class Module", "CAlmacenes", "AsignarAlmacenesADestinos", ERR, Erl)
        GoTo Error_Cls
        Exit Function
    End If

End Function
