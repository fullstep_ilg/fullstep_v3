VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CAprobadoresSolicitud"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True

Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum

Private m_Col As Collection
Private m_oConexion As CConexion
Private m_bEOF As Boolean

Public Property Get eof() As Boolean
    eof = m_bEOF
End Property
Friend Property Let eof(ByVal b As Boolean)
    m_bEOF = b
End Property

Public Property Get Item(vntIndexKey As Variant) As CAprobadorSolicitud
On Error GoTo NoSeEncuentra:

    Set Item = m_Col(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Friend Property Set Conexion(ByVal con As CConexion)
Set m_oConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = m_oConexion
End Property

Public Property Get Count() As Long

If m_Col Is Nothing Then
    Count = 0
Else
     Count = m_Col.Count
End If


End Property

Public Function Add(ByVal lId As Long, Optional ByVal vPer As Variant, Optional ByVal dLimite As Variant, Optional ByVal iOrden As Integer, _
                    Optional ByVal vUON1 As Variant, Optional ByVal vUON2 As Variant, Optional ByVal vUON3 As Variant, Optional ByVal vDEP As Variant, _
                    Optional ByVal dtFecAct As Variant, Optional ByVal vIndice As Variant) As CAprobadorSolicitud
    
    'create a new object
    Dim sCod As String
    Dim objnewmember As CAprobadorSolicitud
    
    Set objnewmember = New CAprobadorSolicitud
    With objnewmember
        Set .Conexion = m_oConexion
    
        .Id = lId
        If IsMissing(dLimite) Then
            .Limite = Null
        Else
            .Limite = dLimite
        End If
        .Orden = iOrden
        .FecAct = dtFecAct
        .Persona = vPer
        .UON1 = vUON1
        .UON2 = vUON2
        .UON3 = vUON3
        .CodDep = vDEP
        
        If Not IsMissing(vIndice) And Not IsNull(vIndice) Then
            .Indice = vIndice
            m_Col.Add objnewmember, CStr(vIndice)
        Else
            m_Col.Add objnewmember, CStr(lId)
        End If
    
    End With
    
    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing


End Function

Public Sub Remove(vntIndexKey As Variant)
On Error GoTo Error

    m_Col.Remove vntIndexKey

Error:

End Sub


Public Property Get NewEnum() As IUnknown
     Set NewEnum = m_Col.[_NewEnum]
End Property


Private Sub Class_Initialize()
    

    Set m_Col = New Collection
End Sub


Private Sub Class_Terminate()
    'Proceroys collection when this class is terminated
    On Error Resume Next
    Set m_Col = Nothing
    Set m_oConexion = Nothing
    
End Sub


