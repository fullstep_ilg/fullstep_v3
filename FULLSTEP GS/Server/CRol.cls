VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CRol"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
''' *** Clase: CRol
''' *** Creacion: 9/09/1998 (Javier Arana)
''' *** Ultima revision: 18/01/1999 (Alfredo Magallon)

Option Explicit

Implements IBaseDatos

''' Variables privadas con la informacion de una Rol

Private mvarCod As String
Private mvarDen As String
Private mvarConv As Boolean
Private mvarFectAct As Variant
Private mvarIdioma As Variant
Private mvarInvi As Boolean ' a�adido por Kepa Mu�oz 20/01/2006

Private mvarRecordsetIdioma As adodb.Recordset

''' Conexion

Private mvarConexion As CConexion
Private mvarConexionServer As CConexionDeUseServer

''' Control de errores

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

''' Resultset para la Rol a editar

Private mvarRecordset As adodb.Recordset

''' Indice de la Rol en la coleccion

Private mvarIndice As Long
Public Property Get Indice() As Integer

    ''' * Objetivo: Devolver el indice de la Rol en la coleccion
    ''' * Recibe: Nada
    ''' * Devuelve: Indice de la Rol en la coleccion

    Indice = mvarIndice
    
End Property
Public Property Let Indice(ByVal iInd As Integer)

    ''' * Objetivo: Dar valor al indice de la Rol en la coleccion
    ''' * Recibe: Indice de la Rol en la coleccion
    ''' * Devuelve: Nada

    mvarIndice = iInd
    
End Property
Friend Property Set Conexion(ByVal vData As CConexion)

    ''' * Objetivo: Dar valor a la conexion de la Rol
    ''' * Recibe: Conexion
    ''' * Devuelve: Nada

    Set mvarConexion = vData
    
End Property

Friend Property Get ConexionServer() As CConexionDeUseServer
Set ConexionServer = mvarConexionServer
End Property

Friend Property Set ConexionServer(ByVal con As CConexionDeUseServer)
Set mvarConexionServer = con
End Property
Private Sub ConectarDeUseServer()
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If mvarConexionServer.ADOSummitCon Is Nothing Then
        Set mvarConexionServer.ADOSummitCon = New adodb.Connection
        mvarConexionServer.ADOSummitCon.Open "Provider=SQLOLEDB.1;Auto Translate=FALSE;Server=" & basParametros.ServidorGS & ";Database=" & basParametros.BaseDatosGS & ";", LICDBLogin, LICDBContra
        mvarConexionServer.ADOSummitCon.CursorLocation = adUseServer
        mvarConexionServer.ADOSummitCon.CommandTimeout = 120
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CRol", "ConectarDeUseServer", ERR, Erl)
        Exit Sub
    End If
End Sub
Friend Property Get Conexion() As CConexion

    ''' * Objetivo: Devolver la conexion de la Rol
    ''' * Recibe: Nada
    ''' * Devuelve: Conexion

    Set Conexion = mvarConexion
    
End Property
Public Property Let Den(ByVal vData As String)

    ''' * Objetivo: Dar valor a la variable privada DEN
    ''' * Recibe: Denominacion de la Rol
    ''' * Devuelve: Nada

    mvarDen = vData
    
End Property
Public Property Get Den() As String

    ''' * Objetivo: Devolver la variable privada DEN
    ''' * Recibe: Nada
    ''' * Devuelve: Denominacion de la Rol

    Den = mvarDen
    
End Property

Public Property Get Idioma() As Variant
    Idioma = mvarIdioma
End Property
Public Property Let Idioma(ByVal VarID As Variant)
    mvarIdioma = VarID
End Property

Public Property Let Cod(ByVal vData As String)

    ''' * Objetivo: Dar valor a la variable privada COD
    ''' * Recibe: Codigo de la Rol
    ''' * Devuelve: Nada

    mvarCod = vData
    
End Property
Public Function ActualizarRoles() As TipoErrorSummit

'    ''' * Objetivo: Cambiar de codigo la calificaci�n
'    ''' * Recibe: Codigo nuevo
'    ''' * Devuelve: TipoErrorSummit con el error si lo hay
'
'    Dim q As rdoQuery, sql As String
'    Dim TESError As TipoErrorSummit
'
'    TESError.NumError = TESnoerror
'
'    ''' Precondicion
'
'    If mvarConexion Is Nothing Then
'        Err.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CRoles.ActualizarRoles", "No se ha establecido la conexion"
'    End If
'
'On Error GoTo Error:
'
'    ''' Preparar la SP y sus parametros
'    sql = "{ CALL ACT_ROL" & Me.Cod & " (?) }"
'
'    Set q = mvarConexion.rdoSummitCon.CreateQuery("ActualizarRoles", sql)
'
'    q.rdoParameters(0) = Me.Cod
'
'    ''' Ejecutar la SP
'
'    q.Execute
'
'    Set q = Nothing
'
'    ActualizarRoles = TESError
'
'    Exit Function
'
'Error:
'
'    If Not q Is Nothing Then
'        Set q = Nothing
'    End If
'
'    ActualizarRoles = basErrores.TratarError(mvarConexion.ADOCon.Errors)

    
End Function

Public Property Get Cod() As String

    ''' * Objetivo: Devolver la variable privada COD
    ''' * Recibe: Nada
    ''' * Devuelve: Codigo de la Rol

    Cod = mvarCod
    
End Property
' a�adido por Kepa Mu�oz (20/01/2006)
Public Property Let Invi(ByVal vData As Boolean)

    ''' * Objetivo: Dar valor a la variable privada Invi
    ''' * Recibe: valor del invitado
    ''' * Devuelve: Nada

    mvarInvi = vData
    
End Property
Public Property Get Invi() As Boolean

    ''' * Objetivo: Devolver la variable privada Invi
    ''' * Recibe: Nada
    ''' * Devuelve: valor del invitado

    Invi = mvarInvi
    
End Property
'-----------------------------------------------------

Public Property Let Conv(ByVal vData As Boolean)

    ''' * Objetivo: Dar valor a la variable privada COD
    ''' * Recibe: Codigo de la Rol
    ''' * Devuelve: Nada

    mvarConv = vData
    
End Property
Public Property Get Conv() As Boolean

    ''' * Objetivo: Devolver la variable privada COD
    ''' * Recibe: Nada
    ''' * Devuelve: Codigo de la Rol

    Conv = mvarConv
    
End Property

Private Sub Class_Terminate()

    ''' * Objetivo: Limpiar la memoria
    
    Set mvarRecordset = Nothing
    Set mvarConexion = Nothing
    Set mvarConexionServer = Nothing
    
End Sub
''' <revision>GFA 30/01/2017</revision> (Control TRANSACCIONES)
Private Function IBaseDatos_AnyadirABaseDatos() As TipoErrorSummit

    ''' * Objetivo: Anyadir la Rol a la tabla
    ''' * Recibe: Nada
    ''' * Devuelve: TipoErrorSummit con el error si lo hay
    
    Dim sConsulta As String
    Dim TESError As TipoErrorSummit
    
    ''' Precondicion
    
    If mvarConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CRol.AnyadirABAseDatos", "No se ha establecido la conexion"
        Exit Function
    End If

 On Error GoTo Error_Cls:
    TESError.NumError = TESnoerror
    mvarConexion.BEGINTRANSACTION
    ''' Ejecutar insercion
    'ROL
    On Error Resume Next
    sConsulta = "INSERT INTO ROL (COD,CONV,INVITADO) VALUES ('" & DblQuote(mvarCod) & "', " & BooleanToSQLBinary(mvarConv) & ", " & BooleanToSQLBinary(mvarInvi) & ")" ' a�ado mvarInvi a la SQL Kepa Mu�oz 20/01/2006
    mvarConexion.ADOCon.Execute sConsulta
    If mvarConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    'ROL_IDIOMA
    sConsulta = "INSERT INTO ROL_IDIOMA (COD,IDIOMA,DEN) VALUES ('" & DblQuote(mvarCod) & "','" & DblQuote(mvarIdioma) & "','" & DblQuote(mvarDen) & "')"
    mvarConexion.ADOCon.Execute sConsulta
    If mvarConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    mvarConexion.COMMITTRANSACTION
    IBaseDatos_AnyadirABaseDatos = TESError
    
    Exit Function
    
Error_Cls:
    If mvarConexion.ADOCon.Errors.Count > 0 Then
        IBaseDatos_AnyadirABaseDatos = basErrores.TratarError(mvarConexion.ADOCon.Errors)
    ElseIf ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CRol", "IBaseDatos_AnyadirABaseDatos", ERR, Erl)
    End If
    mvarConexion.ROLLBACKTRANSACTION
End Function
Private Function IBaseDatos_CambiarCodigo(CodigoNuevo As Variant) As TipoErrorSummit

    ''' * Objetivo: Cambiar de codigo el rol
    ''' * Recibe: Codigo nuevo
    ''' * Devuelve: TipoErrorSummit con el error si lo hay
    
    Dim TESError As TipoErrorSummit
    Dim adocom As adodb.Command
    Dim oParam As adodb.Parameter
   
    ''' Precondicion
    
    If mvarConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CRol.CambiarCodigo", "No se ha establecido la conexion"
        Exit Function
    End If
        
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    TESError.NumError = TESnoerror
    
If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
    
    ''' Preparar la SP y sus parametros
    
    Set adocom = New adodb.Command
    Set adocom.ActiveConnection = mvarConexion.ADOCon
    
    Set oParam = adocom.CreateParameter("OLD", adVarChar, adParamInput, 50, Me.Cod)
    adocom.Parameters.Append oParam
    Set oParam = adocom.CreateParameter("NEW", adVarChar, adParamInput, 50, CodigoNuevo)
    adocom.Parameters.Append oParam
    
    adocom.CommandType = adCmdStoredProc
    adocom.CommandText = "ROL_COD"

    ''' Ejecutar la SP
    
    adocom.Execute
    
    Set adocom = Nothing
    
    mvarCod = CodigoNuevo
        
    IBaseDatos_CambiarCodigo = TESError
    
    Exit Function
    
Error_Cls:
    
    If Not adocom Is Nothing Then
        Set adocom = Nothing
    End If
    
    IBaseDatos_CambiarCodigo = basErrores.TratarError(mvarConexion.ADOCon.Errors)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        'Call g_oErrores.TratarError("Class Module", "CRol", "IBaseDatos_CambiarCodigo", Err, Erl)
        GoTo Error_Cls
        Exit Function
    End If
    
End Function
Private Sub IBaseDatos_CancelarEdicion()

    ''' * Objetivo: Cancelar la edicion, cerrando el Resultset
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not mvarRecordset Is Nothing Then
        mvarRecordset.Close
        Set mvarRecordset = Nothing
    End If

    If Not mvarRecordsetIdioma Is Nothing Then
        mvarRecordsetIdioma.Close
        Set mvarRecordsetIdioma = Nothing
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CRol", "IBaseDatos_CancelarEdicion", ERR, Erl)
        Exit Sub
    End If

End Sub
Private Function IBaseDatos_ComprobarExistenciaEnBaseDatos() As Boolean

    ''' ! No usado por ahora
    
End Function
''' <revision>GFA 30/01/2017</revision> (Control TRANSACCIONES)
Private Function IBaseDatos_EliminarDeBaseDatos() As TipoErrorSummit

    ''' * Objetivo: Eliminar la Rol de la tabla
    ''' * Recibe: Nada
    ''' * Devuelve: TipoErrorSummit con el error si lo hay

    Dim TESError As TipoErrorSummit
    
    ''' Precondicion
    
    If mvarConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CRol.EliminarDeBaseDatos", "No se ha establecido la conexion"
        Exit Function
    End If
    
On Error GoTo Error_Cls:
    TESError.NumError = TESnoerror
    mvarConexion.BEGINTRANSACTION
    ''' Borrado
    'ROL_IDIOMA
    mvarConexion.ADOCon.Execute "DELETE FROM ROL_IDIOMA WHERE COD='" & DblQuote(mvarCod) & "'"
    If mvarConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    'ROL
    mvarConexion.ADOCon.Execute "DELETE FROM ROL WHERE COD='" & DblQuote(mvarCod) & "'"
    If mvarConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    mvarConexion.COMMITTRANSACTION
    
    IBaseDatos_EliminarDeBaseDatos = TESError
    
    Exit Function
        
Error_Cls:
    If mvarConexion.ADOCon.Errors.Count > 0 Then
        IBaseDatos_EliminarDeBaseDatos = basErrores.TratarError(mvarConexion.ADOCon.Errors)
    ElseIf ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CRol", "IBaseDatos_EliminarDeBaseDatos", ERR, Erl)
    End If
    mvarConexion.ROLLBACKTRANSACTION
End Function

Private Function IBaseDatos_FinalizarEdicionEliminando() As TipoErrorSummit

    ''' ! No usado por ahora
    
End Function
Private Function IBaseDatos_FinalizarEdicionModificando() As TipoErrorSummit

    ''' * Objetivo: Finalizar la edicion del Resultset modificando
    ''' * Recibe: Nada
    ''' * Devuelve: TipoErrorSummit con el error si lo hay

    Dim TESError As TipoErrorSummit
    Dim sConsulta As String

    ''' Precondicion
    
    If mvarConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CRol.FinalizarEdicionModificando", "No se ha establecido la conexion"
        Exit Function
    End If
On Error GoTo Error_Cls:
    TESError.NumError = TESnoerror
    
    mvarConexion.BEGINTRANSACTION
    'Actualiza la tabla ROL_IDIOMA
    If mvarRecordsetIdioma.eof Then
        sConsulta = "INSERT INTO ROL_IDIOMA (COD,IDIOMA,DEN) VALUES ('" & DblQuote(mvarCod) & "','" & DblQuote(mvarIdioma) & "','" & DblQuote(mvarDen) & "')"
        mvarConexion.ADOCon.Execute sConsulta
        If mvarConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
        TESError.NumError = TESnoerror
    Else
        mvarRecordsetIdioma("DEN") = mvarDen
        mvarRecordsetIdioma.Update
    End If
    
    'Actualiza la tabla ROL
    mvarRecordset("COD") = mvarCod
    mvarRecordset("FECACT") = mvarFectAct
    mvarRecordset("CONV") = Abs(mvarConv)
    mvarRecordset("INVITADO") = Abs(mvarInvi) 'a�ado el invitado Kepa Mu�oz 20/01/2006
    mvarRecordset.Update
    
    '''ActualizarRoles
    ''' Cierre
    
    mvarConexion.COMMITTRANSACTION
    
    mvarRecordset.Close
    Set mvarRecordset = Nothing
    
    mvarRecordsetIdioma.Close
    Set mvarRecordsetIdioma = Nothing
    
    TESError.NumError = TESnoerror
    IBaseDatos_FinalizarEdicionModificando = TESError
    
    Exit Function
    

Error_Cls:
    If mvarConexion.ADOCon.Errors.Count > 0 Then
        TESError = basErrores.TratarError(mvarConexion.ADOCon.Errors)
        mvarConexion.ROLLBACKTRANSACTION
        Resume Salir:
    ElseIf mvarConexionServer.ADOSummitCon.Errors.Count > 0 Then
        TESError = basErrores.TratarError(mvarConexionServer.ADOSummitCon.Errors)
        mvarConexion.ROLLBACKTRANSACTION
        Resume Salir:
    ElseIf ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CRol", "IBaseDatos_FinalizarEdicionModificando", ERR, Erl)
    End If
    Exit Function
Salir:

    On Error Resume Next
    If mvarRecordset.EditMode > 0 Then
        mvarRecordset.CancelUpdate
    End If
    
    If mvarRecordsetIdioma.EditMode > 0 Then
        mvarRecordsetIdioma.CancelUpdate
    End If
    
    IBaseDatos_FinalizarEdicionModificando = TESError

End Function

''' <summary>Inicia la edici�n de los datos</summary>
''' <returns>Error</returns>
''' <remarks>Llamada desde: GSClient</remarks>
''' <revision>LTG 02/01/2012</revision>
Private Function IBaseDatos_IniciarEdicion(Optional ByVal Bloquear As Boolean, Optional ByVal UsuarioBloqueo As String) As TipoErrorSummit

    ''' * Objetivo: Abrir la edicion en la Rol actual
    ''' * Objetivo: creando un Resultset para ello
    ''' * Recibe: Nada
    ''' * Devuelve: TipoErrorSummit con el error si lo hay

    Dim TESError As TipoErrorSummit
    Dim sSQL As String
        
    ''' Precondicion
    
    If mvarConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CRol.IniciarEdicion", "No se ha establecido la conexion"
        Exit Function
    End If
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    TESError.NumError = TESnoerror
    
    
    ''' Abrir Resultset
    ConectarDeUseServer
    Set mvarRecordset = New adodb.Recordset
    'sin NOLOCK por ser un recordset en modo bloqueo
    mvarRecordset.Open ("SELECT COD,FECACT,INVITADO,CONV FROM ROL WHERE COD='" & DblQuote(mvarCod) & "'"), mvarConexionServer.ADOSummitCon, adOpenKeyset, adLockOptimistic
    
    ''' Si no esta la Rol, alguien la ha eliminado
    
    If mvarRecordset.eof Then
    
        mvarRecordset.Close
        Set mvarRecordset = Nothing
        TESError.NumError = TESDatoEliminado
        TESError.Arg1 = 90 '"Rol"
        IBaseDatos_IniciarEdicion = TESError
        Exit Function
        
    End If
     
     ''' Si hay datos diferentes, devolver la condicion
    
'    If mvarCod <> mvarRecordset("COD") Then
'        mvarCod = mvarRecordset("COD")
'        TESError.NumError = TESInfActualModificada
'        'TESError.Arg1 = "Rol"
'    End If
'    If mvarDen <> mvarRecordset("DEN") Then
'        mvarDen = mvarRecordset("DEN")
'        TESError.NumError = TESInfActualModificada
'       'TESError.Arg1 = "Rol"
'    End If
    If mvarConv <> SQLBinaryToBoolean(mvarRecordset("CONV")) Then
        mvarConv = mvarRecordset("CONV")
        TESError.NumError = TESInfActualModificada
       'TESError.Arg1 = "Rol"
    End If
    If mvarInvi <> SQLBinaryToBoolean(mvarRecordset("INVITADO")) Then
        mvarInvi = mvarRecordset("INVITADO")
        TESError.NumError = TESInfActualModificada
       'TESError.Arg1 = "Rol"
    End If
   
    'Obtiene el idioma
    'sin NOLOCK por ser un recordset en modo bloqueo
    sSQL = "SELECT COD,IDIOMA,DEN FROM ROL_IDIOMA WHERE COD='" & DblQuote(mvarCod) & "' AND IDIOMA = '" & mvarIdioma & "'"
    Set mvarRecordsetIdioma = New adodb.Recordset
    mvarRecordsetIdioma.Open sSQL, mvarConexionServer.ADOSummitCon, adOpenKeyset, adLockOptimistic
    If mvarRecordsetIdioma.eof Then
    
    Else
        If mvarDen <> mvarRecordsetIdioma("DEN") Then
                mvarDen = mvarRecordsetIdioma("DEN")
                TESError.NumError = TESInfActualModificada
               'TESError.Arg1 = "Rol"
        End If
    End If
    
    IBaseDatos_IniciarEdicion = TESError
    TESError.NumError = TESnoerror
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CRol", "IBaseDatos_IniciarEdicion", ERR, Erl)
        Exit Function
    End If

End Function
Private Function IBaseDatos_ModificarEnBaseDatos() As Boolean

    ''' ! No usado por ahora
    
End Function





