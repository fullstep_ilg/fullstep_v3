VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CPrecondiciones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum

Private m_Col As Collection
Private m_oConexion As CConexion
Private m_bEOF As Boolean

Public Property Get eof() As Boolean
    eof = m_bEOF
End Property
Friend Property Let eof(ByVal b As Boolean)
    m_bEOF = b
End Property

Public Property Get Item(vntIndexKey As Variant) As CPrecondicion
On Error GoTo NoSeEncuentra:

    Set Item = m_Col(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Friend Property Set Conexion(ByVal con As CConexion)
Set m_oConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = m_oConexion
End Property

Public Property Get Count() As Long
    If m_Col Is Nothing Then
        Count = 0
    Else
         Count = m_Col.Count
    End If
End Property

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
     Set NewEnum = m_Col.[_NewEnum]
End Property

Public Function Add(ByVal lId As Long, ByVal iOrden As Integer, ByVal sCod As String, Optional ByVal oDenominaciones As CMultiidiomas = Nothing, Optional ByVal lAccion As Long = 0, Optional ByVal iTipo As TipoPrecondicion = 0, Optional ByVal sFormula As String = "", Optional ByVal dtFecAct As Variant, Optional ByVal vIndice As Variant) As CPrecondicion
    
    'create a new object
    Dim objnewmember As CPrecondicion
    
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New CPrecondicion
    With objnewmember
        Set .Conexion = m_oConexion
    
        .Id = lId
        .Orden = iOrden
        .Codigo = sCod
        .Accion = lAccion
        Set .Denominaciones = oDenominaciones
        .TipoPrecondicion = iTipo
        .Formula = sFormula
        .FecAct = dtFecAct
        
    
        If Not IsMissing(vIndice) And Not IsNull(vIndice) Then
            .Indice = vIndice
            m_Col.Add objnewmember, CStr(vIndice)
        Else
            m_Col.Add objnewmember, CStr(lId)
        End If
    
    End With
    
    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CPrecondiciones", "Add", ERR, Erl)
      Exit Function
   End If

End Function

Public Sub AddPrecondicion(ByVal oPrecondicion As CPrecondicion, Optional ByVal vIndice As Variant)
    If Not IsMissing(vIndice) And Not IsNull(vIndice) Then
        oPrecondicion.Indice = vIndice
        m_Col.Add oPrecondicion, CStr(vIndice)
    Else
        m_Col.Add oPrecondicion, CStr(oPrecondicion.Id)
    End If
End Sub

Public Sub Remove(vntIndexKey As Variant)


If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_Col.Remove vntIndexKey
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CPrecondiciones", "Remove", ERR, Erl)
      Exit Sub
   End If

End Sub

Public Sub Clear()
    Dim i As Integer
    Dim Count As Integer
    Count = m_Col.Count
    For i = 1 To Count
        m_Col.Remove 1
    Next
End Sub


Private Sub Class_Initialize()
    Set m_Col = New Collection
End Sub


Private Sub Class_Terminate()
    'Proceroys collection when this class is terminated
    On Error Resume Next
    Set m_Col = Nothing
    Set m_oConexion = Nothing
    
End Sub

Public Function DevolverPrecondiciones(ByVal oIdiomas As CIdiomas, Optional ByVal lIdAccion As Long = 0) As adodb.Recordset
Dim oIdioma As CIdioma
Dim oadorecordset As adodb.Recordset
Dim sql As String
Dim sFrom As String


If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sql = "SELECT P.ID, P.ORDEN, P.COD, P.ACCION, P.TIPO, P.FORMULA, "
    sFrom = "FROM PM_ACCION_PRECOND P WITH (NOLOCK) "
    For Each oIdioma In oIdiomas
        sql = sql & oIdioma.Cod & ".DEN AS DEN_" & oIdioma.Cod & ", "
        sFrom = sFrom & " LEFT JOIN PM_ACCION_PRECOND_DEN " & oIdioma.Cod & " WITH (NOLOCK) ON " & oIdioma.Cod & ".ACCION_PRECOND = P.ID AND " & oIdioma.Cod & ".IDI = '" & oIdioma.Cod & "' "
    Next
    sql = sql & "P.FECACT " & sFrom
    If lIdAccion > 0 Then
        sql = sql & " WHERE P.ACCION = " & lIdAccion
    End If
    sql = sql & " ORDER BY P.ORDEN"

    Set oadorecordset = New adodb.Recordset
    oadorecordset.CursorLocation = adUseClient
    oadorecordset.Open sql, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    oadorecordset.ActiveConnection = Nothing

    Set DevolverPrecondiciones = oadorecordset
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CPrecondiciones", "DevolverPrecondiciones", ERR, Erl)
      Exit Function
   End If

End Function

Public Sub CargarPrecondiciones(ByVal oIdiomas As CIdiomas, Optional ByVal lIdAccion As Long = 0)
    Dim oIdioma As CIdioma
    Dim oRS As adodb.Recordset
    Dim oDenominaciones As CMultiidiomas
    

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oRS = DevolverPrecondiciones(oIdiomas, lIdAccion)
    While Not oRS.eof
        Set oDenominaciones = New CMultiidiomas
        Set oDenominaciones.Conexion = m_oConexion
        For Each oIdioma In oIdiomas
            oDenominaciones.Add oIdioma.Cod, NullToStr(oRS("DEN_" & oIdioma.Cod).Value)
        Next
        Add NullToDbl0(oRS("ID").Value), NullToDbl0(oRS("ORDEN").Value), NullToStr(oRS("COD").Value), oDenominaciones, NullToDbl0(oRS("ACCION").Value), NullToDbl0(oRS("TIPO").Value), NullToStr(oRS("FORMULA").Value), oRS("FECACT").Value
        oRS.MoveNext
    Wend
    oRS.Close
    Set oDenominaciones = Nothing
    Set oRS = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CPrecondiciones", "CargarPrecondiciones", ERR, Erl)
      Exit Sub
   End If
End Sub





