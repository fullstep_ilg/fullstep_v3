VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CActivos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

''' *** Clase: CParametrosSM

''' Variables que almacenan las propiedades de los objetos
Private mCol As Collection
Private m_bEOF As Boolean
Private m_oConexion As CConexion

''' Control de errores
Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum



''' <summary>
''' Carga todos los activos que pueden estar filtrados por distintos conceptos.
''' </summary>
''' <param name="lEmpresa">Id de empresa.</param>
''' <param name="TipoUsu">Tipo de usuario</param>
''' <param name="sUsu">Usuario</param>
''' <param name="sCenSM">Centro SM</param>
''' <param name="sCod">C�digo de activo</param>
''' <param name="sDen">Den de activo</param>
''' <param name="sCodERP">Cod ERP</param>
''' <param name="sLike">3 bits para indicar si se busca por like en el Cod, Den o CodERP</param>
''' <returns></returns>
''' <remarks>Llamada desde:frmPedidos.lstCenCostes_Click, frmPedidos.sdbcCenEmpresa_CloseUp, frmPedidos.txtCenCoste_Validate, frmPedidos.txtContrato_Validate, frmPedidos.CargarDatosSiUnicos, frmSelActivo.CargarActivos. </remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>

Public Sub CargarTodosLosActivos(ByVal lEmpresa As Long, Optional ByVal TipoUsu As TipoDeUsuario, Optional ByVal sUsu As String = "", _
        Optional ByVal sCenSM As String, Optional ByVal sCod As String = "", Optional ByVal sDen As String = "", Optional ByVal sCodERP As String = "", _
        Optional ByVal sLike As String)
Dim sConsulta As String
Dim adoComm As adodb.Command
Dim adoParam As adodb.Parameter
Dim adoRs As New adodb.Recordset

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sConsulta = "SELECT DISTINCT A.ID, A.COD, D.DEN DEN_ACTIVO, A.EMPRESA, A.CENTRO_SM, A.COD_ERP, E.DEN DEN_CENTROSM FROM ACTIVO A WITH (NOLOCK)"
    sConsulta = sConsulta & " INNER JOIN ACTIVO_DEN D WITH (NOLOCK) ON A.ID=D.ACTIVO "
    sConsulta = sConsulta & " INNER JOIN CENTRO_SM_DEN E WITH (NOLOCK) ON A.CENTRO_SM=E.CENTRO_SM "
    If TipoUsu <> Administrador Then
        sConsulta = sConsulta & " INNER JOIN CENTRO_SM_UON S WITH (NOLOCK) ON S.CENTRO_SM=A.CENTRO_SM "
        sConsulta = sConsulta & " INNER JOIN USU_CC_IMPUTACION U WITH (NOLOCK) ON S.UON1=U.UON1 AND ISNULL(S.UON2,0)=ISNULL(U.UON2,0) AND ISNULL(S.UON3,0)=ISNULL(U.UON3,0) AND ISNULL(S.UON4,0)=ISNULL(U.UON4,0) "
    End If
    sConsulta = sConsulta & " WHERE D.IDI=? AND E.IDI=? AND A.BAJALOG=0 "
    If lEmpresa <> 0 Then
        sConsulta = sConsulta & " AND A.EMPRESA=? "
    End If
    If TipoUsu <> Administrador Then
        sConsulta = sConsulta & " AND U.USU=? "
    End If
    If sCenSM <> "" Then
        sConsulta = sConsulta & " AND A.CENTRO_SM=? "
    End If
    If sCod <> "" Then
        If sLike <> "" Then
            If Left(sLike, 1) = "1" Then
                sConsulta = sConsulta & " AND A.COD LIKE ? "
            Else
                sConsulta = sConsulta & " AND A.COD=? "
            End If
        Else
            sConsulta = sConsulta & " AND A.COD=? "
        End If
    End If
    If sDen <> "" Then
        If sLike <> "" Then
            If Left(Mid(sLike, 2), 1) = "1" Then
                sConsulta = sConsulta & " AND D.DEN LIKE ? "
            Else
                sConsulta = sConsulta & " AND D.DEN=? "
            End If
        Else
            sConsulta = sConsulta & " AND D.DEN=? "
        End If
    End If
    If sCodERP <> "" Then
        If sLike <> "" Then
            If Right(sLike, 1) = "1" Then
                sConsulta = sConsulta & " AND A.COD_ERP LIKE ? "
            Else
                sConsulta = sConsulta & " AND A.COD_ERP=? "
            End If
        Else
            sConsulta = sConsulta & " AND A.COD_ERP=? "
        End If
    End If

    Set adoComm = New adodb.Command
    Set adoComm.ActiveConnection = m_oConexion.ADOCon
    adoComm.CommandText = sConsulta
    adoComm.CommandType = adCmdText
    
    
    Set adoParam = adoComm.CreateParameter("IDIOMA", adVarChar, adParamInput, 3, gParametrosInstalacion.gIdioma)
    adoComm.Parameters.Append adoParam
    Set adoParam = adoComm.CreateParameter("IDI", adVarChar, adParamInput, 3, gParametrosInstalacion.gIdioma)
    adoComm.Parameters.Append adoParam
    If lEmpresa <> 0 Then
        Set adoParam = adoComm.CreateParameter("EMPRESA", adInteger, adParamInput, , lEmpresa)
        adoComm.Parameters.Append adoParam
    End If
    If sUsu <> "" Then
        If TipoUsu <> Administrador Then
            Set adoParam = adoComm.CreateParameter("USU", adVarChar, adParamInput, 50, sUsu)
            adoComm.Parameters.Append adoParam
        End If
    End If
    If sCenSM <> "" Then
            Set adoParam = adoComm.CreateParameter("CENTRO_SM", adVarChar, adParamInput, 20, sCenSM)
            adoComm.Parameters.Append adoParam
    End If
    If sCod <> "" Then
            Set adoParam = adoComm.CreateParameter("COD", adVarChar, adParamInput, 50, sCod)
            adoComm.Parameters.Append adoParam
    End If
    If sDen <> "" Then
            Set adoParam = adoComm.CreateParameter("DEN", adVarChar, adParamInput, 200, sDen)
            adoComm.Parameters.Append adoParam
    End If
    If sCodERP <> "" Then
            Set adoParam = adoComm.CreateParameter("COD_ERP", adVarChar, adParamInput, 100, sCodERP)
            adoComm.Parameters.Append adoParam
    End If

    Set adoRs = adoComm.Execute

    Set mCol = Nothing
    Set mCol = New Collection
    If Not adoRs.eof Then

        While Not adoRs.eof
            Me.Add adoRs.Fields("ID").Value, adoRs.Fields("COD").Value, adoRs.Fields("DEN_ACTIVO").Value, adoRs.Fields("EMPRESA").Value, adoRs.Fields("CENTRO_SM").Value, NullToStr(adoRs.Fields("COD_ERP").Value), adoRs.Fields("DEN_CENTROSM").Value
            adoRs.MoveNext
        Wend
    End If
    
    adoRs.Close
    Set adoRs = Nothing
    Set adoComm = Nothing
    Set adoParam = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If Err.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CActivos", "CargarTodosLosActivos", Err, Erl)
      Exit Sub
   End If

End Sub


Public Function Add(ByVal lId As Long, ByVal sCod As String, ByVal sDen As String, ByVal lEmpresa As Long, ByVal sCentroSM As String, ByVal sCodERP As String, ByVal sCentroSMDen As String) As CActivo

    '' * Objetivo: A�adir un elemento a la coleccion
    '' * Recibe: Datos del elemento
    '' * Devuelve: Elemento a�adido
    
    Dim objnewmember As CActivo
    
    '' Creacion de objeto activo
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New CActivo
   
    '' Paso de los parametros al nuevo objeto.
    objnewmember.ID = lId
    objnewmember.Cod = sCod
    objnewmember.Den = sDen
    objnewmember.Empresa = lEmpresa
    objnewmember.CentroSM = sCentroSM
    objnewmember.CodERP = sCodERP
    objnewmember.CentroSMDen = sCentroSMDen
        
    Set objnewmember.Conexion = m_oConexion
        
    '' A�adir el objeto arbol a la coleccion
    '' Si no se especifica indice, se a�ade al final
    mCol.Add objnewmember, CStr(sCod)
            
    Set Add = objnewmember
    
    Set objnewmember = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If Err.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CActivos", "Add", Err, Erl)
      Exit Function
   End If

End Function


Friend Property Set Conexion(ByVal con As CConexion)

    ''' * Objetivo: Dar valor a la variable privada Conexion
    ''' * Recibe: Conexion
    ''' * Devuelve: Nada
    
    Set m_oConexion = con
    
End Property
Friend Property Get Conexion() As CConexion

    ''' * Objetivo: Devolver variable privada Conexion
    ''' * Recibe: Nada
    ''' * Devuelve: Conexion
    
    Set Conexion = m_oConexion
    
End Property


Public Property Get Count() As Long

    ''' * Objetivo: Devolver variable privada Count
    ''' * Recibe: Nada
    ''' * Devuelve: Numero de elementos de la coleccion

    If mCol Is Nothing Then
        Count = 0
    Else
        Count = mCol.Count
    End If

End Property
Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
     Set NewEnum = mCol.[_NewEnum]
End Property
Public Sub Remove(vntIndexKey As Variant)
On Error GoTo NoSeEncuentra:

    mCol.Remove vntIndexKey
    Exit Sub

NoSeEncuentra:
    
End Sub

Public Property Get Item(vntIndexKey As Variant) As CActivo

    ''' * Objetivo: Recuperar un elemento de la coleccion
    ''' * Recibe: Indice del elemento a recuperar
    ''' * Devuelve: Elemento correspondiente
        
On Error GoTo NoSeEncuentra:

    ''' Devolvemos el item de la coleccion privada
    
    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:

    Set Item = Nothing
    
End Property



Private Sub Class_Initialize()
    Set mCol = New Collection
End Sub

Private Sub Class_Terminate()
    'destroys collection when this class is terminated
    Set mCol = Nothing
    Set m_oConexion = Nothing

End Sub

