VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CCondBloqueoCond"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum


Implements IBaseDatos

Private moConexion As CConexion

Private mlID As Long
Private msCod As String
Private mlIDCOD As Long
Private miTipoCampo As TipoCampoCondicionEnlace
Private mlCampo As Long
Private msOperador As String
Private miTipoValor As TipoValorCondicionEnlace
Private mlCampoValor As Long
Private mvValor As Variant
Private mdtFecAct As Variant
Private msMoneda As String
Private m_adores As adodb.Recordset

Private mvarIndice As Variant
Private m_oCampo As CFormItem

Friend Property Set Conexion(ByVal con As CConexion)
Set moConexion = con
End Property
Friend Property Get Conexion() As CConexion
Set Conexion = moConexion
End Property

Public Property Get Id() As Long
    Id = mlID
End Property
Public Property Let Id(ByVal Data As Long)
    mlID = Data
End Property

Public Property Get Cod() As String
    Cod = msCod
End Property
Public Property Let Cod(ByVal Data As String)
    msCod = Data
End Property
Public Property Get IdCod() As Long
    Id = mlIDCOD
End Property
Public Property Let IdCod(ByVal Data As Long)
    mlIDCOD = Data
End Property

Public Property Get TipoCampo() As TipoCampoCondicionEnlace
    TipoCampo = miTipoCampo
End Property
Public Property Let TipoCampo(ByVal Data As TipoCampoCondicionEnlace)
    miTipoCampo = Data
End Property

Public Property Get Campo() As Long
    Campo = mlCampo
End Property
Public Property Let Campo(ByVal Data As Long)
    mlCampo = Data
End Property

Public Property Get Operador() As String
    Operador = msOperador
End Property
Public Property Let Operador(ByVal Data As String)
    msOperador = Data
End Property

Public Property Get TipoValor() As TipoValorCondicionEnlace
    TipoValor = miTipoValor
End Property
Public Property Let TipoValor(ByVal Data As TipoValorCondicionEnlace)
    miTipoValor = Data
End Property

Public Property Get CampoValor() As Long
    CampoValor = mlCampoValor
End Property
Public Property Let CampoValor(ByVal Data As Long)
    mlCampoValor = Data
End Property

Public Property Get Valor() As Variant
    Valor = mvValor
End Property
Public Property Let Valor(ByVal Data As Variant)
    mvValor = Data
End Property

Public Property Get FecAct() As Variant
    FecAct = mdtFecAct
End Property
Public Property Let FecAct(ByVal Data As Variant)
    mdtFecAct = Data
End Property

Public Property Let Indice(ByVal varIndice As Variant)
    mvarIndice = varIndice
End Property
Public Property Get Indice() As Variant
    Indice = mvarIndice
End Property

Public Property Get CampoForm() As CFormItem
    If m_oCampo Is Nothing Then
        cargarCampo
    End If
    Set CampoForm = m_oCampo
End Property

Public Property Set CampoForm(ByRef oCampo As CFormItem)
    Set m_oCampo = oCampo
End Property

Public Property Get Moneda() As String
    Moneda = msMoneda
End Property

Public Property Let Moneda(ByVal sMoneda As String)
    msMoneda = sMoneda
End Property


Private Sub Class_Terminate()
    Set moConexion = Nothing
    Set m_oCampo = Nothing
End Sub

Private Function IBaseDatos_AnyadirABaseDatos() As TipoErrorSummit
Dim sConsulta As String
Dim sConsulta2 As String
Dim TESError As TipoErrorSummit
Dim AdoRes As adodb.Recordset
Dim bTransaccionEnCurso As Boolean
Dim foreingKeyValue As Long
Dim foreingKeyName As String
Dim tableName As String

Dim tipoCampoCondicion As TiposDeAtributos

TESError.NumError = TESnoerror

If moConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CCondicionEnlace.AnyadirABAseDatos", "No se ha establecido la conexion"
End If
' *****************************************

On Error GoTo ERROR:
    
    moConexion.ADOCon.Execute "BEGIN DISTRIBUTED TRAN"
    bTransaccionEnCurso = True
    moConexion.ADOCon.Execute "SET XACT_ABORT OFF"
        
    foreingKeyName = "ID_COD"
    foreingKeyValue = mlIDCOD
    tableName = "PM_CONDICIONES_BLOQUEO_CONDICIONES"
    
    'Insetar la Condici�n
    sConsulta = "INSERT INTO " & tableName & " " & _
        "(COD, " & foreingKeyName & ", TIPO_CAMPO, CAMPO, OPERADOR, TIPO_VALOR, CAMPO_VALOR, VALOR_TEXT, VALOR_NUM, VALOR_FEC, VALOR_BOOL,MON) " & _
        "VALUES " & _
        "(" & StrToSQLNULL(msCod) & "," & foreingKeyValue & "," & miTipoCampo & ","
    If miTipoCampo = TipoCampoCondicionEnlace.CampoDeFormulario Then
        sConsulta = sConsulta & mlCampo & ",'"
    Else
        sConsulta = sConsulta & "NULL,'"
    End If
    sConsulta = sConsulta & DblQuote(msOperador) & "'," & miTipoValor & ","
    If miTipoValor = TipoValorCondicionEnlace.CampoDeFormulario Then
        sConsulta = sConsulta & mlCampoValor & ",NULL,NULL,NULL,NULL"
    ElseIf miTipoValor = TipoValorCondicionEnlace.ValorEstatico Then
        sConsulta = sConsulta & "NULL,"
        Select Case miTipoCampo
            Case TipoCampoCondicionEnlace.CampoDeFormulario
                'Mirar el tipo del Campo y poner consecuentemente
                sConsulta2 = "SELECT SUBTIPO FROM FORM_CAMPO WITH (NOLOCK) WHERE ID = " & mlCampo
                Set AdoRes = New adodb.Recordset
                AdoRes.Open sConsulta2, moConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
                tipoCampoCondicion = AdoRes("SUBTIPO").Value
                AdoRes.Close
                Set AdoRes = Nothing
                Select Case tipoCampoCondicion
                    Case TiposDeAtributos.TipoString, TiposDeAtributos.TipoTextoCorto, TiposDeAtributos.TipoTextoMedio, TiposDeAtributos.TipoTextoLargo
                        sConsulta = sConsulta & "'" & DblQuote(CStr(mvValor)) & "',NULL,NULL,NULL"
                    Case TiposDeAtributos.TipoNumerico
                        sConsulta = sConsulta & "NULL," & CLng(mvValor) & ",NULL,NULL"
                    Case TiposDeAtributos.TipoFecha
                        sConsulta = sConsulta & "NULL,NULL,'" & CDate(mvValor) & "',NULL"
                    Case TiposDeAtributos.TipoBoolean
                        sConsulta = sConsulta & "NULL,NULL,NULL," & BooleanToSQLBinary(CBool(mvValor))
                End Select
            Case TipoCampoCondicionEnlace.Peticionario, TipoCampoCondicionEnlace.DepPetcionario, TipoCampoCondicionEnlace.UONPeticionario
                sConsulta = sConsulta & "'" & DblQuote(CStr(mvValor)) & "',NULL,NULL,NULL"
            Case TipoCampoCondicionEnlace.NumProcesAbiertos, TipoCampoCondicionEnlace.ImporteAdjudicado, TipoCampoCondicionEnlace.ImporteSolicitud, TipoCampoCondicionEnlace.ImporteAbierto, TipoCampoCondicionEnlace.ImporteEmitido
                sConsulta = sConsulta & "NULL," & CLng(mvValor) & ",NULL,NULL"
        End Select
    ElseIf miTipoValor = TipoValorCondicionEnlace.Peticionario Or miTipoValor = TipoValorCondicionEnlace.DepPetcionario Or _
        miTipoValor = TipoValorCondicionEnlace.UONPeticionario Or miTipoValor = TipoValorCondicionEnlace.NumProcesAbiertos Or _
        miTipoValor = TipoValorCondicionEnlace.ImporteAdjudicado Or miTipoValor = TipoValorCondicionEnlace.ImporteSolicitud Or _
        miTipoValor = TipoValorCondicionEnlace.ImporteAbierto Or miTipoValor = TipoValorCondicionEnlace.ImporteEmitido Then
            sConsulta = sConsulta & "NULL,NULL,NULL,NULL,NULL"
    End If
    sConsulta = sConsulta & "," & StrToSQLNULL(msMoneda)
    sConsulta = sConsulta & ")"
    moConexion.ADOCon.Execute sConsulta
    If moConexion.ADOCon.Errors.Count > 0 Then
        GoTo ERROR
    End If
    
    sConsulta = "SELECT MAX(ID) ID FROM " & tableName
    Set AdoRes = New adodb.Recordset
    AdoRes.Open sConsulta, moConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    mlID = AdoRes("ID").Value
    AdoRes.Close
    Set AdoRes = Nothing
    
    ''' Recuperar la fecha de insercion
    sConsulta = "SELECT FECACT FROM " & tableName & " WITH (NOLOCK) WHERE ID = " & mlID
    Set AdoRes = New adodb.Recordset
    AdoRes.Open sConsulta, moConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    mdtFecAct = AdoRes("FECACT").Value
    AdoRes.Close
    Set AdoRes = Nothing
    
    moConexion.ADOCon.Execute "COMMIT TRAN"
    bTransaccionEnCurso = False
       
    IBaseDatos_AnyadirABaseDatos = TESError
    Exit Function
ERROR:

    If Not AdoRes Is Nothing Then
        AdoRes.Close
        Set AdoRes = Nothing
    End If
                 
    IBaseDatos_AnyadirABaseDatos = basErrores.TratarError(moConexion.ADOCon.Errors)
                 
                 
    If bTransaccionEnCurso Then
        moConexion.ADOCon.Execute "ROLLBACK TRAN"
        bTransaccionEnCurso = False
    End If
    

End Function

Private Function IBaseDatos_CambiarCodigo(CodigoNuevo As Variant) As TipoErrorSummit

End Function

Private Sub IBaseDatos_CancelarEdicion()

If Not m_adores Is Nothing Then
    m_adores.Close
    Set m_adores = Nothing
End If

End Sub

Private Function IBaseDatos_ComprobarExistenciaEnBaseDatos() As Boolean

End Function

Private Function IBaseDatos_EliminarDeBaseDatos() As TipoErrorSummit
Dim TESError As TipoErrorSummit
Dim btrans As Boolean
Dim sConsulta As String
Dim tableName As String

TESError.NumError = TESnoerror

'******************* Precondicion *******************
If moConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CCondicionEnlace.EliminarDeBaseDatos", "No se ha establecido la conexion"
End If
'*****************************************************
On Error GoTo ERROR:
    
    moConexion.ADOCon.Execute "BEGIN TRAN"
    moConexion.ADOCon.Execute "SET XACT_ABORT OFF"
    btrans = True
    
    tableName = "PM_CONDICIONES_BLOQUEO_CONDICIONES"
     
    sConsulta = "DELETE FROM " & tableName & " WHERE ID = " & mlID
            
    moConexion.ADOCon.Execute sConsulta
    If moConexion.ADOCon.Errors.Count > 0 Then
        GoTo ERROR
    End If
    moConexion.ADOCon.Execute "COMMIT TRAN"
    btrans = False
    Set m_adores = Nothing
    
    IBaseDatos_EliminarDeBaseDatos = TESError
    
    Exit Function
    
ERROR:
    
    If moConexion.ADOCon.Errors.Count > 0 Then
        IBaseDatos_EliminarDeBaseDatos = basErrores.TratarError(moConexion.ADOCon.Errors)
        If btrans Then
            moConexion.ADOCon.Execute "ROLLBACK TRAN"
            btrans = False
        End If
        Exit Function
    End If
    
    If m_adores.EditMode = 1 Then
        m_adores.CancelUpdate
    End If
End Function

Private Function IBaseDatos_FinalizarEdicionEliminando() As TipoErrorSummit

End Function

Private Function IBaseDatos_FinalizarEdicionModificando() As TipoErrorSummit
Dim TESError As TipoErrorSummit
Dim sConsulta As String
Dim sConsulta2 As String
Dim bTransaccionEnCurso As Boolean
Dim AdoRes As adodb.Recordset
Dim tipoCampoCondicion As TipoCampoCondicionEnlace
Dim tableName As String
Dim foreingKeyName As String
Dim foreingKeyValue As Long


TESError.NumError = TESnoerror

'********* Precondicion *******************
If moConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CCondicionEnlace.FinalizarEdicionModificando", "No se ha establecido la conexion"
End If
'******************************************

On Error GoTo ERROR:

    moConexion.ADOCon.Execute "BEGIN TRAN"
    bTransaccionEnCurso = True
    moConexion.ADOCon.Execute "SET XACT_ABORT OFF"
    
    foreingKeyName = "ID_COD"
    foreingKeyValue = mlIDCOD
    tableName = "PM_CONDICIONES_BLOQUEO_CONDICIONES"
    
    'Comprobamos si ha habido cambios en otra sesi�n
    Set m_adores = New adodb.Recordset
    sConsulta = "SELECT FECACT FROM " & tableName & " WITH (NOLOCK) WHERE ID = " & mlID
    m_adores.Open sConsulta, moConexion.ADOCon, adOpenStatic, adLockReadOnly
    
    If m_adores.eof Then
        m_adores.Close
        Set m_adores = Nothing
        TESError.NumError = TESDatoEliminado
        TESError.Arg1 = 181
        IBaseDatos_FinalizarEdicionModificando = TESError
        moConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
        Exit Function
    End If
    
    If mdtFecAct <> m_adores("FECACT").Value Then
        m_adores.Close
        Set m_adores = Nothing
        TESError.NumError = TESInfActualModificada
        moConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
        IBaseDatos_FinalizarEdicionModificando = TESError
        Exit Function
    End If
    
    ''' Actualizar Condicion
    sConsulta = "UPDATE " & tableName & " SET " & _
        "COD = " & StrToSQLNULL(msCod) & ", " & _
        foreingKeyName & " = " & foreingKeyValue & ", " & _
        "TIPO_CAMPO = " & miTipoCampo & ", "
    If miTipoCampo = TipoCampoCondicionEnlace.CampoDeFormulario Then
        sConsulta = sConsulta & "CAMPO = " & mlCampo & ", "
    Else
        sConsulta = sConsulta & "CAMPO = NULL, "
    End If
    sConsulta = sConsulta & "OPERADOR = '" & DblQuote(msOperador) & "', " & _
        "TIPO_VALOR = " & miTipoValor & ", "
    If miTipoValor = TipoValorCondicionEnlace.CampoDeFormulario Then
        sConsulta = sConsulta & "CAMPO_VALOR = " & mlCampoValor & ", " & _
        "VALOR_TEXT = NULL, " & _
        "VALOR_NUM = NULL, " & _
        "VALOR_FEC = NULL, " & _
        "VALOR_BOOL = NULL "
    ElseIf miTipoValor = TipoValorCondicionEnlace.ValorEstatico Then
        sConsulta = sConsulta & "CAMPO_VALOR = NULL, "
        Select Case miTipoCampo
            Case TipoCampoCondicionEnlace.CampoDeFormulario
                'Mirar el tipo del Campo y poner consecuentemente
                sConsulta2 = "SELECT SUBTIPO FROM FORM_CAMPO WITH (NOLOCK) WHERE ID = " & mlCampo
                Set AdoRes = New adodb.Recordset
                AdoRes.Open sConsulta2, moConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
                tipoCampoCondicion = AdoRes("SUBTIPO").Value
                AdoRes.Close
                Set AdoRes = Nothing
                Select Case tipoCampoCondicion
                    Case TiposDeAtributos.TipoString, TiposDeAtributos.TipoTextoCorto, TiposDeAtributos.TipoTextoMedio, TiposDeAtributos.TipoTextoLargo
                        sConsulta = sConsulta & "VALOR_TEXT = '" & DblQuote(CStr(mvValor)) & "', " & _
                            "VALOR_NUM = NULL, " & _
                            "VALOR_FEC = NULL, " & _
                            "VALOR_BOOL = NULL "
                    Case TiposDeAtributos.TipoNumerico
                        sConsulta = sConsulta & "VALOR_TEXT = NULL, " & _
                            "VALOR_NUM = " & CLng(mvValor) & ", " & _
                            "VALOR_FEC = NULL, " & _
                            "VALOR_BOOL = NULL "
                    Case TiposDeAtributos.TipoFecha
                        sConsulta = sConsulta & "VALOR_TEXT = NULL, " & _
                            "VALOR_NUM = NULL, " & _
                            "VALOR_FEC = " & DateToSQLDate(mvValor) & ", " & _
                            "VALOR_BOOL = NULL "
                    Case TiposDeAtributos.TipoBoolean
                        sConsulta = sConsulta & "VALOR_TEXT = NULL, " & _
                            "VALOR_NUM = NULL, " & _
                            "VALOR_FEC = NULL, " & _
                            "VALOR_BOOL = " & BooleanToSQLBinary(CBool(mvValor))
                End Select
            Case TipoCampoCondicionEnlace.Peticionario, TipoCampoCondicionEnlace.DepPetcionario, TipoCampoCondicionEnlace.UONPeticionario
                sConsulta = sConsulta & "VALOR_TEXT = '" & DblQuote(CStr(mvValor)) & "', " & _
                    "VALOR_NUM = NULL, " & _
                    "VALOR_FEC = NULL, " & _
                    "VALOR_BOOL = NULL "
            Case TipoCampoCondicionEnlace.NumProcesAbiertos, TipoCampoCondicionEnlace.ImporteAdjudicado, TipoCampoCondicionEnlace.ImporteSolicitud, TipoCampoCondicionEnlace.ImporteEmitido, TipoCampoCondicionEnlace.ImporteAbierto
                sConsulta = sConsulta & "VALOR_TEXT = NULL, " & _
                    "VALOR_NUM = " & CLng(mvValor) & ", " & _
                    "VALOR_FEC = NULL, " & _
                    "VALOR_BOOL = NULL "
        End Select
    ElseIf miTipoValor = TipoValorCondicionEnlace.DepPetcionario Or miTipoValor = TipoValorCondicionEnlace.ImporteAbierto Or _
        miTipoValor = TipoValorCondicionEnlace.ImporteAdjudicado Or miTipoValor = TipoValorCondicionEnlace.ImporteEmitido Or _
        miTipoValor = TipoValorCondicionEnlace.ImporteSolicitud Or miTipoValor = TipoValorCondicionEnlace.NumProcesAbiertos Or _
        miTipoValor = TipoValorCondicionEnlace.Peticionario Or miTipoValor = TipoValorCondicionEnlace.UONPeticionario Then
        sConsulta = sConsulta & "CAMPO_VALOR = NULL, " & _
        "VALOR_TEXT = NULL, " & _
        "VALOR_NUM = NULL, " & _
        "VALOR_FEC = NULL, " & _
        "VALOR_BOOL = NULL "

    End If
    sConsulta = sConsulta & ",MON = " & StrToSQLNULL(msMoneda) & ""
    sConsulta = sConsulta & " WHERE ID = " & mlID
    moConexion.ADOCon.Execute sConsulta
    If moConexion.ADOCon.Errors.Count > 0 Then
        GoTo ERROR
    End If
    
    'Recogemos el nuevo valor de FECACT
    m_adores.Requery
    mdtFecAct = m_adores("FECACT").Value
    m_adores.Close
    Set m_adores = Nothing
    
    moConexion.ADOCon.Execute "COMMIT TRAN"
    bTransaccionEnCurso = False
    
    IBaseDatos_FinalizarEdicionModificando = TESError
    
    Exit Function
    

ERROR:
    
    IBaseDatos_FinalizarEdicionModificando = basErrores.TratarError(moConexion.ADOCon.Errors)
    
    If Not m_adores Is Nothing Then
        m_adores.Close
        Set m_adores = Nothing
    End If
    
    If bTransaccionEnCurso Then
        moConexion.ADOCon.Execute "ROLLBACK"
        moConexion.ADOCon.Execute "SET XACT_ABORT OFF"
        bTransaccionEnCurso = False
    End If

End Function

Private Function IBaseDatos_IniciarEdicion(Optional ByVal Bloquear As Boolean, Optional ByVal UsuarioBloqueo As String) As TipoErrorSummit
Dim TESError As TipoErrorSummit
Dim tableName As String
Dim foreingKeyName As String
Dim foreingKeyValue As Long

TESError.NumError = TESnoerror

'******************* Precondicion *******************
If moConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CCondicionEnlace.IniciarEdicion", "No se ha establecido la conexion"
End If
'*****************************************************

foreingKeyName = "ID_COD"
foreingKeyValue = mlID
tableName = "PM_CONDICIONES_BLOQUEO_CONDICIONES"

'''Cargar Condicion
Set m_adores = New adodb.Recordset
m_adores.Open "SELECT COD, " & foreingKeyName & ", TIPO_CAMPO, CAMPO, OPERADOR, TIPO_VALOR, CAMPO_VALOR, VALOR_TEXT, VALOR_NUM, VALOR_FEC, VALOR_BOOL, FECACT FROM " & tableName & " WHERE ID = " & mlID, moConexion.ADOCon, adOpenKeyset, adLockReadOnly

If m_adores.eof Then
        m_adores.Close
        Set m_adores = Nothing
        TESError.NumError = TESDatoEliminado
        TESError.Arg1 = 181
        IBaseDatos_IniciarEdicion = TESError
        Exit Function
End If

msCod = NullToStr(m_adores.Fields("COD").Value)
miTipoCampo = NullToDbl0(m_adores.Fields("TIPO_CAMPO").Value)
mlCampo = NullToDbl0(m_adores.Fields("CAMPO").Value)
msOperador = NullToStr(m_adores.Fields("OPERADOR").Value)
miTipoValor = NullToDbl0(m_adores.Fields("TIPO_VALOR").Value)
mlCampoValor = NullToDbl0(m_adores.Fields("CAMPO_VALOR").Value)
If Not IsNull(m_adores.Fields("VALOR_TEXT").Value) Then
    mvValor = NullToStr(m_adores.Fields("VALOR_TEXT").Value)
ElseIf Not IsNull(m_adores.Fields("VALOR_NUM").Value) Then
    mvValor = NullToDbl0(m_adores.Fields("VALOR_NUM").Value)
ElseIf Not IsNull(m_adores.Fields("VALOR_FEC").Value) Then
    mvValor = m_adores.Fields("VALOR_FEC").Value
ElseIf Not IsNull(m_adores.Fields("VALOR_BOOL").Value) Then
    mvValor = SQLBinaryToBoolean(m_adores.Fields("VALOR_BOOL").Value)
End If
mdtFecAct = m_adores.Fields("FECACT").Value

m_adores.Close
Set m_adores = Nothing

IBaseDatos_IniciarEdicion = TESError
    
End Function

Public Function incluyeCampoImporte() As Boolean
    incluyeCampoImporte = CampoForm.esTipoImporteoCalculado
End Function

Public Sub cargarCampo()
    Set m_oCampo = New CFormItem
    Set m_oCampo.Conexion = Me.Conexion
    m_oCampo.Id = Campo
    m_oCampo.CargarDatosFormCampo
End Sub

