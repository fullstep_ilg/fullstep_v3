VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CAprovisionador"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CAprovisionador **********************************
'*             Autor : EPB
'*             Creada : 10/09/2001
'****************************************************************

Option Explicit

Implements IBaseDatos

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum
Private mvarConexion As CConexion
Private mvarRecordset As adodb.Recordset
Private mvarIndice As Variant

Private mvarId As Long
Private mvarUON1 As String
Private mvarUON2 As String
Private mvarUON3 As String
Private mvarDenUon1 As String
Private mvarDenUon2 As String
Private mvarDenUon3 As String
Private mvarPersona As CPersona
Private mvarDepartamento As CDepartamento
Private mvarPadre As Variant 'es un codigo de persona
Private mvarPadreID As Variant 'id del padre
Private mvarNotifsLimPed As CPersonas
Private mvarSeguridad As Long
Private mvarCategoria As Long
Private mvarNivel As Integer 'Nivel dentro del arbol de aprovisionadores
Private mvarImporte As Double
Private mvarHijos As CAprovisionadores
Private mvarFecAct As Date
Private mvarDarAcceso As Boolean

Friend Property Set Conexion(ByVal con As CConexion)
Set mvarConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = mvarConexion
End Property

Public Property Get Id() As Long
    Id = mvarId
End Property

Public Property Let Id(ByVal Ind As Long)
    mvarId = Ind
End Property

Public Property Get UON1() As String
    UON1 = mvarUON1
End Property

Public Property Let UON1(ByVal Data As String)
    mvarUON1 = Data
End Property

Public Property Get UON2() As String
    UON2 = mvarUON2
End Property

Public Property Let UON2(ByVal Data As String)
    mvarUON2 = Data
End Property

Public Property Get UON3() As String
    UON3 = mvarUON3
End Property

Public Property Let UON3(ByVal Data As String)
    mvarUON3 = Data
End Property

Public Property Get DenUON1() As String
    DenUON1 = mvarDenUon1
End Property

Public Property Let DenUON1(ByVal Data As String)
    mvarDenUon1 = Data
End Property

Public Property Get DenUON2() As String
    DenUON2 = mvarDenUon2
End Property

Public Property Let DenUON2(ByVal Data As String)
    mvarDenUon2 = Data
End Property

Public Property Get DenUON3() As String
    DenUON3 = mvarDenUon3
End Property

Public Property Let DenUON3(ByVal Data As String)
    mvarDenUon3 = Data
End Property

Public Property Get Persona() As CPersona
    Set Persona = mvarPersona
End Property

Public Property Set Persona(ByVal vData As CPersona)
    Set mvarPersona = vData
End Property

Public Property Get Departamento() As CDepartamento
    Set Departamento = mvarDepartamento
End Property

Public Property Set Departamento(ByVal vData As CDepartamento)
    Set mvarDepartamento = vData
End Property

Public Property Set Notificados(ByVal vData As CPersonas)
    Set mvarNotifsLimPed = vData
End Property

Public Property Get Notificados() As CPersonas
    Set Notificados = mvarNotifsLimPed
End Property

Public Property Get Seguridad() As Long
    Seguridad = mvarSeguridad
End Property

Public Property Let Seguridad(ByVal vData As Long)
    mvarSeguridad = vData
End Property

Public Property Get Categoria() As Long
    Categoria = mvarCategoria
End Property

Public Property Let Categoria(ByVal vData As Long)
    mvarCategoria = vData
End Property

Public Property Get Nivel() As Integer
    Nivel = mvarNivel
End Property

Public Property Let Nivel(ByVal vData As Integer)
    mvarNivel = vData
End Property
Public Property Get Padre() As Variant
    Padre = mvarPadre
End Property

Public Property Let Padre(ByVal vData As Variant)
    mvarPadre = vData
End Property
Public Property Get PadreID() As Variant
    PadreID = mvarPadreID
End Property

Public Property Let PadreID(ByVal vData As Variant)
    mvarPadreID = vData
End Property

Public Property Get Importe() As Double
    Importe = mvarImporte
End Property

Public Property Let Importe(ByVal vData As Double)
    mvarImporte = vData
End Property

Public Property Set AprovisionadoresHijos(ByVal vData As CAprovisionadores)
    Set mvarHijos = vData
End Property

Public Property Get AprovisionadoresHijos() As CAprovisionadores
    Set AprovisionadoresHijos = mvarHijos
End Property
Public Property Get Indice() As Variant
    Indice = mvarIndice
End Property
Public Property Let Indice(ByVal varInd As Variant)
    mvarIndice = varInd
End Property
Public Property Get DarAcceso() As Boolean
    DarAcceso = mvarDarAcceso
End Property

Public Property Let DarAcceso(ByVal vVal As Boolean)
    mvarDarAcceso = vVal
End Property

Private Sub Class_Terminate()
    Set mvarConexion = Nothing
    Set mvarPersona = Nothing
    Set mvarNotifsLimPed = Nothing
    Set mvarHijos = Nothing

End Sub

''' <summary>Carga los aprovisionadores</summary>
''' <remarks>Llamada desde: FSGSClient; Tiempo m�ximo: < 1 sec </remarks>
''' <returns>Error de haberlo</returns>
''' Revisado por: jbg; Fecha: 03/06/2015
Public Function CargarTodosLosDatos() As TipoErrorSummit
Dim sConsulta As String
Dim rs As adodb.Recordset
Dim TESError As TipoErrorSummit
Dim oPer As CPersona


'******************* Precondicion *******************
If mvarConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CPersona.CargarTodosLosDatos", "No se ha establecido la conexion"
End If
'*****************************************************
sConsulta = "SELECT USU.COD,AC.NIVEL,AC.ID,ISNULL(AC.UON1,PER.UON1) UON1,ISNULL(AC.UON2,PER.UON2) UON2,ISNULL(AC.UON3,PER.UON3) UON3,ISNULL(AC.DEP,PER.DEP) DEP,AC.PER,AC.IMPORTE,AC.FECACT,PER.APE,PER.NOM,PER.BAJALOG,DEP.COD COD_DEP,DEP.DEN DEN_DEP,UON1.DEN DEN_UON1,UON2.DEN DEN_UON2,UON3.DEN DEN_UON3,USU.FSEP " & _
            " FROM APROVISIONADOR_CATN AC WITH (NOLOCK) " & _
            " LEFT JOIN PER WITH (NOLOCK) " & _
            " ON PER.COD = AC.PER " & _
            " LEFT JOIN USU WITH (NOLOCK) " & _
            " ON USU.PER = PER.COD " & _
            " LEFT JOIN DEP WITH (NOLOCK) " & _
            " ON DEP.COD = ISNULL(AC.DEP,PER.DEP) " & _
            " LEFT JOIN UON1 WITH (NOLOCK) ON ISNULL(AC.UON1,PER.UON1)=UON1.COD " & _
            " LEFT JOIN UON2 WITH (NOLOCK) ON ISNULL(AC.UON1,PER.UON1)=UON2.UON1 AND ISNULL(AC.UON2,PER.UON2)=UON2.COD " & _
            " LEFT JOIN UON3 WITH (NOLOCK) ON ISNULL(AC.UON1,PER.UON1)=UON3.UON1 AND ISNULL(AC.UON2,PER.UON2)=UON3.UON2 AND ISNULL(AC.UON3,PER.UON3)=UON3.COD " & _
            " WHERE AC.ID = " & mvarId
Set rs = New adodb.Recordset
rs.Open sConsulta, mvarConexion.ADOCon, adOpenStatic, adLockReadOnly

If rs.eof Then
    rs.Close
    Set rs = Nothing
    TESError.NumError = TESDatoEliminado
    TESError.Arg1 = 100 'Aprovisionador
    CargarTodosLosDatos = TESError
    Exit Function
End If

mvarNivel = rs("NIVEL")
mvarImporte = rs("IMPORTE")
mvarFecAct = rs("FECACT")
mvarUON1 = NullToStr(rs("UON1").Value)
mvarUON2 = NullToStr(rs("UON2").Value)
mvarUON3 = NullToStr(rs("UON3").Value)
If NullToStr(rs("PER")) <> "" Then
    'Si el aprovisionador es persona
    Set mvarPersona = New CPersona
    Set mvarPersona.Conexion = mvarConexion
    mvarPersona.Cod = rs("PER")
    mvarPersona.Apellidos = rs("APE")
    mvarPersona.Nombre = rs("NOM")
    mvarPersona.Usuario = rs("COD").Value
    mvarPersona.AccesoFSEP = SQLBinaryToBoolean(rs("FSEP").Value)
    mvarPersona.BajaLog = rs("BAJALOG").Value
    mvarPersona.UON1 = rs("UON1").Value
    mvarPersona.UON2 = rs("UON2").Value
    mvarPersona.UON3 = rs("UON3").Value
    mvarPersona.CodDep = rs("DEP").Value
Else
    Set mvarPersona = Nothing
End If

If NullToStr(rs("DEP")) <> "" And NullToStr(rs("PER")) = "" Then
    'Si el aprovisionador es un departamento
    Set mvarDepartamento = New CDepartamento
    Set mvarDepartamento.Conexion = mvarConexion
    mvarDepartamento.Cod = rs("DEP")
    mvarDepartamento.Den = rs("DEN_DEP")
Else
    Set mvarDepartamento = Nothing
End If

rs.Close
Set rs = Nothing
TESError.NumError = TESnoerror
CargarTodosLosDatos = TESError


End Function


Private Function IBaseDatos_CambiarCodigo(CodigoNuevo As Variant) As TipoErrorSummit

End Function

Private Sub IBaseDatos_CancelarEdicion()

End Sub

Private Function IBaseDatos_ComprobarExistenciaEnBaseDatos() As Boolean

End Function

'****************************************************************************************************************************
'Descripci�n:   Funci�n para eliminar aprovisionadores de l�neas del cat�logo

'Devuelve:      Un error
'Revisador por: MMV     Fecha: 22/03/2012
Private Function IBaseDatos_EliminarDeBaseDatos() As TipoErrorSummit
Dim TESError As TipoErrorSummit
Dim rs As adodb.Recordset
Dim sConsulta As String
Dim btrans As Boolean

TESError.NumError = TESnoerror

'******************* Precondicion *******************
If mvarConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CAprovisionador.IBaseDatos_EliminarDeBaseDatos", "No se ha establecido la conexion"
End If
'*****************************************************
On Error GoTo ERROR
    Set mvarRecordset = New adodb.Recordset
    mvarRecordset.Open "SELECT FECACT FROM APROVISIONADOR_CATN WHERE ID=" & mvarId, mvarConexion.ADOCon, adOpenStatic, adLockReadOnly
    If mvarRecordset.eof Or mvarRecordset("FECACT").Value <> mvarFecAct Then
        mvarRecordset.Close
        Set mvarRecordset = Nothing
        TESError.NumError = TESInfActualModificada
        TESError.Arg1 = 100 '"Aprovisionador"
        IBaseDatos_EliminarDeBaseDatos = TESError
        Exit Function
    End If
    mvarConexion.ADOCon.Execute "BEGIN TRANSACTION"
    btrans = True
    'Eliminamos el aprovisionador
    sConsulta = "DELETE FROM APROVISIONADOR_CATN WHERE ID =" & mvarId
    mvarConexion.ADOCon.Execute sConsulta
    If mvarConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR
    
    If Not mvarPersona Is Nothing Then
        'Si el aprovisionador es una persona se miraria si tenia acceso a EP, si tenia se le quita
        sConsulta = "SELECT COD,FSEP FROM USU WHERE PER = '" & DblQuote(mvarPersona.Cod) & "'"
        Set rs = New adodb.Recordset
        rs.Open sConsulta, mvarConexion.ADOCon, adOpenStatic, adLockReadOnly
        If Not rs.eof Then
            If rs("FSEP").Value = 1 And mvarDarAcceso Then
                sConsulta = "UPDATE USU SET FSEP=0 WHERE COD ='" & DblQuote(rs("COD").Value) & "'"
                mvarConexion.ADOCon.Execute sConsulta
                If mvarConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR
            End If
        End If
    End If
    

    mvarRecordset.Close
    Set mvarRecordset = Nothing
    Set rs = Nothing
        
    mvarConexion.ADOCon.Execute "COMMIT TRANSACTION"
    btrans = False
    IBaseDatos_EliminarDeBaseDatos = TESError
    Exit Function

ERROR:
    IBaseDatos_EliminarDeBaseDatos = TratarError(mvarConexion.ADOCon.Errors)
    If btrans Then
        mvarConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
    End If
    If Not mvarRecordset Is Nothing Then
        mvarRecordset.Close
        Set mvarRecordset = Nothing
    End If
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
        
End Function



Private Function IBaseDatos_FinalizarEdicionEliminando() As TipoErrorSummit

End Function

''' <summary>Finaliza una edici�n a�adiendo el registro</summary>
''' <remarks>Llamada desde: FSGSClient; Tiempo m�ximo: < 1 sec </remarks>
''' Revisado por: jbg; Fecha: 03/06/2015
Private Function IBaseDatos_AnyadirABaseDatos() As TipoErrorSummit
Dim TESError As TipoErrorSummit
Dim rs As adodb.Recordset
Dim sConsulta As String
Dim btrans As Boolean

TESError.NumError = TESnoerror

'******************* Precondicion *******************
If mvarConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CAprovisionador.IBaseDatos_AnyadirABaseDatos", "No se ha establecido la conexion"
End If
'*****************************************************
On Error GoTo ERROR
    If Not mvarPersona Is Nothing Then
        'Si el aprovisionador es una persona
        sConsulta = "SELECT USU.COD,CASE WHEN USU.FSEP=1 THEN 1 ELSE ISNULL(P.FSEP,0) END FSEP FROM USU WITH (NOLOCK) LEFT JOIN PERF P WITH (NOLOCK) ON USU.PERF=P.ID WHERE PER = '" & DblQuote(mvarPersona.Cod) & "'"
        Set rs = New adodb.Recordset
        rs.Open sConsulta, mvarConexion.ADOCon, adOpenStatic, adLockReadOnly
        If rs.eof Then
            rs.Close
            Set rs = Nothing
            TESError.NumError = TESImposibleMarcarAprob
            TESError.Arg1 = "USUELI"
            TESError.Arg2 = 2
            IBaseDatos_AnyadirABaseDatos = TESError
            Exit Function
        End If
    End If
    
    mvarConexion.ADOCon.Execute "BEGIN TRANSACTION"
    btrans = True
    
    sConsulta = "INSERT INTO APROVISIONADOR_CATN (CATN,NIVEL,UON1,UON2,UON3,DEP,PER,IMPORTE) VALUES (" & mvarCategoria & "," & mvarNivel & ","
    
    If mvarPersona Is Nothing Then
        If mvarUON1 <> "" Then
            sConsulta = sConsulta & "'" & DblQuote(mvarUON1) & "',"
        Else
            sConsulta = sConsulta & "NULL,"
        End If
        If mvarUON2 <> "" Then
            sConsulta = sConsulta & "'" & DblQuote(mvarUON2) & "',"
        Else
            sConsulta = sConsulta & "NULL,"
        End If
        If mvarUON3 <> "" Then
            sConsulta = sConsulta & "'" & DblQuote(mvarUON3) & "',"
        Else
            sConsulta = sConsulta & "NULL,"
        End If
        If mvarDepartamento Is Nothing Then
            sConsulta = sConsulta & "NULL,"
        Else
            sConsulta = sConsulta & "'" & DblQuote(mvarDepartamento.Cod) & "',"
        End If
        sConsulta = sConsulta & "NULL,"
    Else
        sConsulta = sConsulta & "NULL,"
        sConsulta = sConsulta & "NULL,"
        sConsulta = sConsulta & "NULL,"
        sConsulta = sConsulta & "NULL,"
        sConsulta = sConsulta & "'" & DblQuote(mvarPersona.Cod) & "',"
    End If
    
    sConsulta = sConsulta & DblToSQLFloat(mvarImporte) & ")"
    
    mvarConexion.ADOCon.Execute sConsulta
    If mvarConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR
    If Not mvarPersona Is Nothing Then
        'Si el aprovisionador es una persona y se le quiere dar acceso al EP
        If rs("FSEP").Value = 0 And mvarDarAcceso Then
            sConsulta = "UPDATE USU SET FSEP=1 WHERE COD ='" & DblQuote(rs("COD").Value) & "'"
            mvarConexion.ADOCon.Execute sConsulta
        End If
    
        rs.Close
        Set rs = Nothing
    End If
    
    If Not mvarRecordset Is Nothing Then
        mvarRecordset.Close
        Set mvarRecordset = Nothing
    End If
    
    'Calidad Sin with(nolock)
    Set mvarRecordset = New adodb.Recordset
    mvarRecordset.Open "SELECT @@IDENTITY", mvarConexion.ADOCon, adOpenStatic, adLockReadOnly
    mvarId = mvarRecordset(0).Value
    mvarRecordset.Close
    
    'Calidad Sin with(nolock)
    Set mvarRecordset = New adodb.Recordset
    mvarRecordset.Open "SELECT FECACT FROM APROVISIONADOR_CATN WHERE ID=" & mvarId, mvarConexion.ADOCon, adOpenStatic, adLockReadOnly
    mvarFecAct = mvarRecordset("FECACT").Value
    mvarRecordset.Close
    Set mvarRecordset = Nothing
    
    mvarConexion.ADOCon.Execute "COMMIT TRANSACTION"
    btrans = False
    IBaseDatos_AnyadirABaseDatos = TESError
    Exit Function

ERROR:
    IBaseDatos_AnyadirABaseDatos = TratarError(mvarConexion.ADOCon.Errors)
    If btrans Then
        mvarConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
    End If
    If Not mvarRecordset Is Nothing Then
        mvarRecordset.Close
        Set mvarRecordset = Nothing
    End If
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
        
End Function

''' <summary>Finaliza una edici�n</summary>
''' <remarks>Llamada desde: FSGSClient; Tiempo m�ximo: < 1 sec </remarks>
''' <revision>LTG 02/01/2012</revision>

Private Function IBaseDatos_FinalizarEdicionModificando() As TipoErrorSummit
    Dim TESError As TipoErrorSummit
    Dim rs As adodb.Recordset
    Dim sConsulta As String
    Dim btrans As Boolean
    
    TESError.NumError = TESnoerror
    
    '******************* Precondicion *******************
    If mvarConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CAprovisionador.IBaseDatos_FinalizarEdicionModificando", "No se ha establecido la conexion"
    End If
    '*****************************************************
    On Error GoTo ERROR
    
    'Compruebo el bloqueo
    'CALIDAD Sin with(nolock) por la importancia de m_vFecact para saber cuando se va a "pisar" cambios de otro
    sConsulta = "SELECT FECACT,PER FROM APROVISIONADOR_CATN WHERE ID=" & mvarId
    Set mvarRecordset = New adodb.Recordset
    mvarRecordset.Open sConsulta, mvarConexion.ADOCon, adOpenStatic, adLockReadOnly
    If mvarRecordset.eof Then
        mvarRecordset.Close
        Set mvarRecordset = Nothing
        TESError.NumError = TESDatoEliminado
        TESError.Arg1 = 100 '"Aprovisionador"
        IBaseDatos_FinalizarEdicionModificando = TESError
        Exit Function
    End If
    
    mvarConexion.ADOCon.Execute "BEGIN TRANSACTION"
    btrans = True

    sConsulta = "UPDATE APROVISIONADOR_CATN SET "
    If mvarPersona Is Nothing Then
        If mvarUON1 <> "" Then
            sConsulta = sConsulta & " UON1='" & DblQuote(mvarUON1) & "',"
        Else
            sConsulta = sConsulta & " UON1=NULL,"
        End If
        If mvarUON2 <> "" Then
            sConsulta = sConsulta & " UON2='" & DblQuote(mvarUON2) & "',"
        Else
            sConsulta = sConsulta & " UON2=NULL,"
        End If
        If mvarUON3 <> "" Then
            sConsulta = sConsulta & " UON3='" & DblQuote(mvarUON3) & "',"
        Else
            sConsulta = sConsulta & " UON3=NULL,"
        End If
        If mvarDepartamento Is Nothing Then
            sConsulta = sConsulta & " DEP=NULL,"
        Else
            sConsulta = sConsulta & " DEP='" & DblQuote(mvarDepartamento.Cod) & "',"
        End If
    
        sConsulta = sConsulta & " PER=NULL,"
    Else
        sConsulta = sConsulta & " UON1=NULL,"
        sConsulta = sConsulta & " UON2=NULL,"
        sConsulta = sConsulta & " UON3=NULL,"
        sConsulta = sConsulta & " DEP=NULL,"
        sConsulta = sConsulta & " PER='" & DblQuote(mvarPersona.Cod) & "',"
    End If
    sConsulta = sConsulta & "IMPORTE=" & DblToSQLFloat(mvarImporte) & " WHERE ID=" & mvarId
        
    mvarConexion.ADOCon.Execute sConsulta
    If mvarConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR
    If Not mvarPersona Is Nothing Then
        'Si han sustituido el aprovisionador por una persona se le da acceso al EP si asi se ha marcado
        sConsulta = "SELECT USU.COD,CASE WHEN USU.FSEP=1 THEN 1 ELSE ISNULL(P.FSEP,0) END FSEP FROM USU LEFT JOIN PERF P WITH (NOLOCK) ON USU.PERF=P.ID WHERE PER = '" & DblQuote(mvarPersona.Cod) & "'"
        Set rs = New adodb.Recordset
        rs.Open sConsulta, mvarConexion.ADOCon, adOpenStatic, adLockReadOnly
        If rs.eof Then
            rs.Close
            Set rs = Nothing
            mvarRecordset.Close
            Set mvarRecordset = Nothing
            mvarConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
            TESError.NumError = TESImposibleMarcarAprob
            TESError.Arg1 = "USUELI"
            TESError.Arg2 = 2
            IBaseDatos_FinalizarEdicionModificando = TESError
            Exit Function
        End If
        
        If rs("FSEP").Value = 0 And mvarDarAcceso Then
            sConsulta = "UPDATE USU SET FSEP=1 WHERE COD ='" & DblQuote(rs("COD").Value) & "'"
            mvarConexion.ADOCon.Execute sConsulta
            If mvarConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR
        End If

        rs.Close
        Set rs = Nothing
    End If
    mvarRecordset.Requery
    mvarFecAct = mvarRecordset("FECACT").Value
    
    mvarRecordset.Close
    Set mvarRecordset = Nothing
    mvarConexion.ADOCon.Execute "COMMIT TRANSACTION"
    btrans = False
    IBaseDatos_FinalizarEdicionModificando = TESError
    Exit Function

ERROR:
    IBaseDatos_FinalizarEdicionModificando = TratarError(mvarConexion.ADOCon.Errors)
    If btrans Then
        mvarConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
    End If
    If Not mvarRecordset Is Nothing Then
        mvarRecordset.Close
        Set mvarRecordset = Nothing
    End If
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If


End Function

Private Function IBaseDatos_IniciarEdicion(Optional ByVal Bloquear As Boolean, Optional ByVal UsuarioBloqueo As String) As TipoErrorSummit

End Function

''' <summary>Comprueba seguridad</summary>
''' <returns>Booleano indicando si hay seguridad</returns>
''' <remarks>Llamada desde: FSGSClient; Tiempo m�ximo: < 1 sec </remarks>
''' <revision>LTG 02/01/2012</revision>

Public Function ExisteAproviEnOtraSeguridad() As Boolean
    Dim rs2 As adodb.Recordset
    Dim sConsulta As String
    
    sConsulta = "SELECT APROBADOR FROM CATN1 WITH(NOLOCK) WHERE APROBADOR='" & DblQuote(mvarPersona.Cod) & "'"
    sConsulta = sConsulta & " UNION "
    sConsulta = sConsulta & "SELECT APROBADOR FROM CATN2 WITH(NOLOCK) WHERE APROBADOR='" & DblQuote(mvarPersona.Cod) & "'"
    sConsulta = sConsulta & " UNION "
    sConsulta = sConsulta & "SELECT APROBADOR FROM CATN3 WITH(NOLOCK) WHERE APROBADOR='" & DblQuote(mvarPersona.Cod) & "'"
    sConsulta = sConsulta & " UNION "
    sConsulta = sConsulta & "SELECT APROBADOR FROM CATN4 WITH(NOLOCK) WHERE APROBADOR='" & DblQuote(mvarPersona.Cod) & "'"
    sConsulta = sConsulta & " UNION "
    sConsulta = sConsulta & "SELECT APROBADOR FROM CATN5 WITH(NOLOCK) WHERE APROBADOR='" & DblQuote(mvarPersona.Cod) & "'"
    Set rs2 = New adodb.Recordset
    rs2.Open sConsulta, mvarConexion.ADOCon, adOpenStatic, adLockReadOnly
    If Not rs2.eof Then
        rs2.Close
        Set rs2 = Nothing
        ExisteAproviEnOtraSeguridad = True
        Exit Function
    End If
    rs2.Close
        
    sConsulta = "SELECT COUNT(*) FROM VIEW_APROVISONADORES WITH (NOLOCK) WHERE PER = '" & DblQuote(mvarPersona.Cod) & "' AND CATN <> " & mvarCategoria & " AND NIVEL<>" & mvarNivel
    rs2.Open sConsulta, mvarConexion.ADOCon, adOpenStatic, adLockReadOnly
    If rs2(0).Value > 0 Then
        rs2.Close
        Set rs2 = Nothing
        ExisteAproviEnOtraSeguridad = True
    Else
        rs2.Close
        Set rs2 = Nothing
        ExisteAproviEnOtraSeguridad = False
    End If
End Function

''' <summary>Comprueba si el usuario(aprovisionador) tiene perfil o no</summary>
''' <returns>Booleano indicando si hay tiene perfil o no ese usuario</returns>
''' <remarks>Llamada desde: FSGSClient; Tiempo m�ximo: < 1 sec </remarks>
''' <revision></revision>
Public Function TienePerfil() As Boolean
    Dim rs2 As adodb.Recordset
    Dim sConsulta As String
    
    sConsulta = "SELECT ISNULL(P.ID,0) ID FROM USU U WITH(NOLOCK) LEFT JOIN PERF P WITH (NOLOCK) ON U.PERF=P.ID WHERE U.PER='" & DblQuote(mvarPersona.Cod) & "'"
    Set rs2 = New adodb.Recordset
    rs2.Open sConsulta, mvarConexion.ADOCon, adOpenStatic, adLockReadOnly
    If Not rs2.eof Then
        If rs2("ID") > 0 Then
            TienePerfil = True
        Else
            TienePerfil = False
        End If
        Set rs2 = Nothing
        Exit Function
    Else
        TienePerfil = False
        Set rs2 = Nothing
        Exit Function
    End If
End Function

Public Function ExistenPedidosDenegados() As Boolean
Dim rs As adodb.Recordset
Dim sConsulta As String

On Error Resume Next

ExistenPedidosDenegados = False

sConsulta = "SELECT SUM(CASE WHEN OE.EST IN (1,22) THEN 1 ELSE 0 END) DENEGADAS,  SUM(CASE WHEN OE.EST in( 0,1,20,21,22) THEN 1 ELSE 0 END) TODAS " _
              & "  FROM ORDEN_ENTREGA OE WITH (NOLOCK) " _
              & "       INNER JOIN (SELECT DISTINCT LP.ORDEN " _
              & "                     FROM LINEAS_PEDIDO LP WITH (NOLOCK) " _
              & "                        INNER JOIN PEDIDO P WITH (NOLOCK) " _
              & "                                ON LP.PEDIDO = P.ID " _
              & "                    WHERE LP.BAJA_LOG=0 AND P.PER='" & DblQuote(mvarPersona.Cod) & "' " _
              & "                      AND LP.SEGURIDAD = " & mvarSeguridad & ") LPP " _
              & "               ON OE.ID = LPP.ORDEN "

Set rs = New adodb.Recordset
rs.Open sConsulta, mvarConexion.ADOCon, adOpenStatic, adLockReadOnly
If rs.Fields("DENEGADAS").Value > 0 And rs.Fields("DENEGADAS").Value = rs.Fields("TODAS").Value Then
    ExistenPedidosDenegados = True
Else
    ExistenPedidosDenegados = False
End If
 
rs.Close

Set rs = Nothing
 
End Function
''' <summary>Devuelve las categorias configuradas para un aprovisionador(persona)</summary>
''' <returns>Recordset con los ids de las categorias y su nivel</returns>
Public Function DevolverCategoriasDeAprovisionador() As adodb.Recordset
    Dim rs As adodb.Recordset
    Dim sConsulta As String
    
    On Error Resume Next
    
    
    sConsulta = "SELECT DISTINCT NIVEL,CATN FROM VIEW_APROVISONADORES WHERE PER='" & Persona.Cod & "'"
    
    Set rs = New adodb.Recordset
    rs.Open sConsulta, mvarConexion.ADOCon, adOpenStatic, adLockReadOnly
    
    Set DevolverCategoriasDeAprovisionador = rs
End Function

Public Function ExistenUsuariosSinAccesoEP() As Boolean
    Dim rs As adodb.Recordset
    Dim sConsulta As String
    
    On Error Resume Next
    
    ExistenUsuariosSinAccesoEP = False
    
    sConsulta = "SELECT COUNT(U.COD) AS USUARIOS " _
            & "  FROM PER P WITH(NOLOCK) " _
            & "  INNER JOIN USU U WITH(NOLOCK) ON P.COD=U.PER " _
            & "  LEFT JOIN PERF PF WITH(NOLOCK) ON U.PERF = PF.ID " _
            & "  WHERE ISNULL(P.UON1,'')=ISNULL('" & DblQuote(mvarUON1) & "','') AND ISNULL(P.UON2,'')=ISNULL(" & DblQuote(mvarUON2) & ",'') " _
            & "  AND ISNULL(P.UON3,'')=ISNULL(" & DblQuote(mvarUON3) & ",'') AND "
    If mvarDepartamento Is Nothing Then
        sConsulta = sConsulta & " ISNULL(P.DEP,'')=ISNULL(Null,'') "
    Else
        sConsulta = sConsulta & " ISNULL(P.DEP,'')=ISNULL(" & mvarDepartamento.Cod & ",'') "
    End If
    sConsulta = sConsulta & " AND (U.FSEP | ISNULL(PF.FSEP,0))=0 AND P.BAJALOG=0"
    
    Set rs = New adodb.Recordset
    rs.Open sConsulta, mvarConexion.ADOCon, adOpenStatic, adLockReadOnly
    If rs.Fields("USUARIOS").Value > 0 Then
        ExistenUsuariosSinAccesoEP = True
    Else
        ExistenUsuariosSinAccesoEP = False
    End If
     
    rs.Close
    
    Set rs = Nothing
End Function

