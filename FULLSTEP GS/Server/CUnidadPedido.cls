VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CUnidadPedido"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CUnidadPedido **********************************
'*             Autor : Elena Mu�oz
'*             Creada : 22/8/2001
'****************************************************************

Option Explicit

Implements IBaseDatos

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum

Private mvarFecAct As Variant

Private mvarIdLinea As Variant
Private mvarIndice As Variant
Private mvarProveCod As String
Private mvarProveDen As String
Private mvarArtCod_Interno As String
Private mvarArtCod_Externo As String
Private mvarArtDen As String
Private mvarUnidadCompra As String
Private mvarUCDen As String
Private mvarUnidadPedido As String
Private mvarUPDen As String
Private mvarFactorConversion As Double
Private mvarCantidadMinima As Variant
Private mvarCantidadMaxPed As Variant
Private mvarCantidadMaxTotal As Variant
Private mvarCantidadConsumida As Variant
Private mvarCantidadConsumida_UC As Variant
Private mvarCantidadMaxTotal_UC As Variant
Private mvarDefecto As Boolean
Private mvarGMN1Cod As String
Private mvarGMN2Cod As String
Private mvarGMN3Cod As String
Private mvarGMN4Cod As String
Private mvarConexion As CConexion 'local copy
Private mvarRecordset As adodb.Recordset



Friend Property Set Conexion(ByVal vData As CConexion)
    Set mvarConexion = vData
End Property

Friend Property Get Conexion() As CConexion
    Set Conexion = mvarConexion
End Property


Public Property Let IDLinea(ByVal vData As Variant)
    mvarIdLinea = vData
End Property

Public Property Get IDLinea() As Variant
    IDLinea = mvarIdLinea
End Property

Public Property Let Indice(ByVal varIndice As Variant)
    mvarIndice = varIndice
End Property
Public Property Get Indice() As Variant
    Indice = mvarIndice
End Property

Public Property Let ProveCod(ByVal vData As String)
    mvarProveCod = vData
End Property

Public Property Get ProveCod() As String
    ProveCod = mvarProveCod
End Property

Public Property Let ProveDen(ByVal vData As String)
    mvarProveDen = vData
End Property

Public Property Get ProveDen() As String
    ProveDen = mvarProveDen
End Property

Public Property Let ArtCod_Interno(ByVal vData As String)
    mvarArtCod_Interno = vData
End Property

Public Property Get ArtCod_Interno() As String
    ArtCod_Interno = mvarArtCod_Interno
End Property

Public Property Let ArtCod_Externo(ByVal vData As String)
    mvarArtCod_Externo = vData
End Property

Public Property Get ArtCod_Externo() As String
    ArtCod_Externo = mvarArtCod_Externo
End Property

Public Property Let ArtDen(ByVal vData As Variant)
    mvarArtDen = vData
End Property

Public Property Get ArtDen() As Variant
    ArtDen = mvarArtDen
End Property

Public Property Let UnidadCompra(ByVal vData As String)
    mvarUnidadCompra = vData
End Property

Public Property Get UnidadCompra() As String
    UnidadCompra = mvarUnidadCompra
End Property

Public Property Let UCDen(ByVal vData As String)
    mvarUCDen = vData
End Property

Public Property Get UCDen() As String
    UCDen = mvarUCDen
End Property
Public Property Let FechaAct(ByVal Dat As Variant)
    mvarFecAct = Dat
End Property
Public Property Get FechaAct() As Variant
    FechaAct = mvarFecAct
End Property
Public Property Let UnidadPedido(ByVal vData As String)
    mvarUnidadPedido = vData
End Property

Public Property Get UnidadPedido() As String
    UnidadPedido = mvarUnidadPedido
End Property

Public Property Let UPDen(ByVal vData As String)
    mvarUPDen = vData
End Property

Public Property Get UPDen() As String
    UPDen = mvarUPDen
End Property

Public Property Let FactorConversion(ByVal vData As Double)
    mvarFactorConversion = vData
End Property

Public Property Get FactorConversion() As Double
    FactorConversion = mvarFactorConversion
End Property

Public Property Let CantidadMinima(ByVal vData As Variant)
    mvarCantidadMinima = vData
End Property

Public Property Get CantidadMinima() As Variant
    CantidadMinima = mvarCantidadMinima
End Property
Public Property Let CantidadMaximaPed(ByVal vData As Variant)
    mvarCantidadMaxPed = vData
End Property

Public Property Get CantidadMaximaPed() As Variant
    CantidadMaximaPed = mvarCantidadMaxPed
End Property

Public Property Let CantidadMaximaTotal(ByVal vData As Variant)
    mvarCantidadMaxTotal = vData
End Property

Public Property Get CantidadMaximaTotal() As Variant
    CantidadMaximaTotal = mvarCantidadMaxTotal
End Property
Public Property Let CantidadMaximaTotal_UC(ByVal vData As Variant)
    mvarCantidadMaxTotal_UC = vData
End Property

Public Property Get CantidadMaximaTotal_UC() As Variant
    CantidadMaximaTotal_UC = mvarCantidadMaxTotal_UC
End Property

Public Property Let CantidadConsumida(ByVal vData As Variant)
    mvarCantidadConsumida = vData
End Property

Public Property Get CantidadConsumida() As Variant
    CantidadConsumida = mvarCantidadConsumida
End Property

Public Property Let CantidadConsumida_UC(ByVal vData As Variant)
    mvarCantidadConsumida_UC = vData
End Property

Public Property Get CantidadConsumida_UC() As Variant
    CantidadConsumida_UC = mvarCantidadConsumida_UC
End Property

Public Property Get Defecto() As Boolean
    Defecto = mvarDefecto
End Property
Public Property Let Defecto(ByVal Data As Boolean)
    mvarDefecto = Data
End Property

Public Property Get GMN1Cod() As String
    GMN1Cod = mvarGMN1Cod
End Property
Public Property Let GMN1Cod(ByVal Data As String)
    mvarGMN1Cod = Data
End Property
Public Property Get GMN2Cod() As String
    GMN2Cod = mvarGMN2Cod
End Property
Public Property Let GMN2Cod(ByVal Data As String)
    mvarGMN2Cod = Data
End Property
Public Property Get GMN3Cod() As String
    GMN3Cod = mvarGMN3Cod
End Property
Public Property Let GMN3Cod(ByVal Data As String)
    mvarGMN3Cod = Data
End Property
Public Property Get GMN4Cod() As String
    GMN4Cod = mvarGMN4Cod
End Property
Public Property Let GMN4Cod(ByVal Data As String)
    mvarGMN4Cod = Data
End Property


Private Function IBaseDatos_AnyadirABaseDatos() As TipoErrorSummit
Dim sConsulta As String
Dim TESError As TipoErrorSummit


TESError.NumError = TESnoerror

'********* Precondicion *******************
If mvarConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CUnidadPedido.AnyadirABAseDatos", "No se ha establecido la conexion"
End If
' *****************************************

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:

    sConsulta = "INSERT INTO PROVE_ART_UP (PROVE,ART,UC,UP,FC)"
    sConsulta = sConsulta & " VALUES ('" & DblQuote(mvarProveCod) & "','" & DblQuote(mvarArtCod_Interno) & "','" & DblQuote(mvarUnidadCompra) & "','" & DblQuote(mvarUnidadPedido) & "'," & DblToSQLFloat(mvarFactorConversion) & ")"
    
    mvarConexion.ADOCon.Execute sConsulta

    IBaseDatos_AnyadirABaseDatos = TESError
    
    Exit Function

Error_Cls:
    
    IBaseDatos_AnyadirABaseDatos = basErrores.TratarError(mvarConexion.ADOCon.Errors)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CUnidadPedido", "IBaseDatos_AnyadirABaseDatos", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If

End Function

Private Function IBaseDatos_CambiarCodigo(CodigoNuevo As Variant) As TipoErrorSummit

End Function


Private Sub IBaseDatos_CancelarEdicion()

End Sub


Private Function IBaseDatos_ComprobarExistenciaEnBaseDatos() As Boolean

End Function


Private Function IBaseDatos_EliminarDeBaseDatos() As TipoErrorSummit

    ''' * Objetivo: Eliminar
    ''' * Recibe: Nada
    ''' * Devuelve: TipoErrorSummit con el error si lo hay

    Dim TESError As TipoErrorSummit
    Dim sConsulta As String
    
    TESError.NumError = TESnoerror
    
    ''' Precondicion
    
    If mvarConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CUnidad.EliminarDeBaseDatos", "No se ha establecido la conexion"
    End If
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
        
    ''' Borrado
    sConsulta = "DELETE FROM PROVE_ART_UP WHERE PROVE='" & DblQuote(mvarProveCod) & "' AND ART='" & DblQuote(mvarArtCod_Interno) & "' AND UC='" & DblQuote(mvarUnidadCompra) & "' AND UP='" & DblQuote(mvarUnidadPedido) & "'"
    mvarConexion.ADOCon.Execute sConsulta
    
    IBaseDatos_EliminarDeBaseDatos = TESError
    
    Exit Function
        
Error_Cls:
        
    IBaseDatos_EliminarDeBaseDatos = basErrores.TratarError(mvarConexion.ADOCon.Errors)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CUnidadPedido", "IBaseDatos_EliminarDeBaseDatos", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If
End Function


Private Function IBaseDatos_FinalizarEdicionEliminando() As TipoErrorSummit

End Function

''' <summary>Esta funci�n finaliza la edici�n del resulset modificando</summary>
''' <returns>TipoErrorSummit con el error si lo hay</returns>
''' <remarks>Llamada desde: FSGClient
''' Tiempo m�ximo: <1 seg </remarks>
''' <revision>JVS 10/01/2012</revision>
Private Function IBaseDatos_FinalizarEdicionModificando() As TipoErrorSummit
    ''' * Objetivo: Finalizar la edicion del Resultset modificando
    ''' * Recibe: Nada
    ''' * Devuelve: TipoErrorSummit con el error si lo hay

Dim TESError As TipoErrorSummit
Dim sConsulta As String
Dim btrans As Boolean


TESError.NumError = TESnoerror

'********* Precondicion *******************
    If mvarConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CUnidadPedido.FinalizarEdicionModificando", "No se ha establecido la conexion"
    End If
'******************************************

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
    
    mvarConexion.ADOCon.Execute "BEGIN TRANSACTION"
    btrans = True
    
    'Comprobamos si ha habido cambios en otra sesi�n
    Set mvarRecordset = New adodb.Recordset
    'CALIDAD Se obtiene la Fecha de Actualizaci�n con bloqueo para recuperar la verdadera �ltima fecha de actualizaci�n
    mvarRecordset.Open ("SELECT FECACT FROM UNI WHERE COD='" & mvarUnidadPedido & "'"), mvarConexion.ADOCon, adOpenStatic, adLockReadOnly
    
    If mvarRecordset.eof Then
        mvarRecordset.Close
        Set mvarRecordset = Nothing
        TESError.NumError = TESDatoEliminado
        TESError.Arg1 = 105 ' Tabla UNI
        IBaseDatos_FinalizarEdicionModificando = TESError
        mvarConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
        Exit Function
    End If
    
    'Comprobamos si ha habido cambios en otra sesi�n
    If mvarFecAct <> mvarRecordset("FECACT") Then
        mvarRecordset.Close
        Set mvarRecordset = Nothing
        TESError.NumError = TESInfActualModificada
        IBaseDatos_FinalizarEdicionModificando = TESError
        mvarConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
        Exit Function
    End If
    
    ''' Actualizar la bd
    If mvarArtCod_Interno = "" Then
        sConsulta = "UPDATE CAT_LIN_MIN SET FC = " & DblToSQLFloat(mvarFactorConversion) & ""
        sConsulta = sConsulta & " WHERE UP= '" & DblQuote(mvarUnidadPedido) & "' AND LINEA=" & mvarIdLinea
    Else
        sConsulta = "UPDATE PROVE_ART_UP SET FC = " & DblToSQLFloat(mvarFactorConversion) & ""
        sConsulta = sConsulta & " WHERE PROVE='" & DblQuote(mvarProveCod) & "' AND ART= '" & DblQuote(mvarArtCod_Interno) & "' AND UC= '" & DblQuote(mvarUnidadCompra) & "' AND UP= '" & DblQuote(mvarUnidadPedido) & "'"
    End If
    
    mvarConexion.ADOCon.Execute sConsulta
    If mvarConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    
    'Recogemos el nuevo valor de FECACT
    mvarRecordset.Requery
    mvarFecAct = mvarRecordset("FECACT").Value
    mvarRecordset.Close
    Set mvarRecordset = Nothing
    
    mvarConexion.ADOCon.Execute "COMMIT TRANSACTION"
    btrans = False
    IBaseDatos_FinalizarEdicionModificando = TESError
    Exit Function
    
Error_Cls:
    
    IBaseDatos_FinalizarEdicionModificando = basErrores.TratarError(mvarConexion.ADOCon.Errors)
    If Not mvarRecordset Is Nothing Then
        mvarRecordset.Close
        Set mvarRecordset = Nothing
    End If
    If btrans Then
        mvarConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CUnidadPedido", "IBaseDatos_FinalizarEdicionModificando", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If

End Function

''' <summary>Esta funci�n inicia la edici�n del resulset</summary>
'''<param optional name="Bloquear">Si se bloquea o no</param>
'''<param optional name="UsuarioBloqueo">Usuario del bloqueo</param>
''' <returns>TipoErrorSummit con el error si lo hay</returns>
''' <remarks>Llamada desde: FSGClient
''' Tiempo m�ximo: <1 seg </remarks>
''' <revision>JVS 10/01/2012</revision>
Private Function IBaseDatos_IniciarEdicion(Optional ByVal Bloquear As Boolean, Optional ByVal UsuarioBloqueo As String) As TipoErrorSummit
Dim TESError As TipoErrorSummit


TESError.NumError = TESnoerror
     
'********* Precondicion *******************
If mvarConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CUnidadPedido.IniciarEdicion", "No se ha establecido la conexion"
End If
'******************************************

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
    Set mvarRecordset = New adodb.Recordset
    'CALIDAD Se obtiene la Fecha de Actualizaci�n con bloqueo para recuperar la verdadera �ltima fecha de actualizaci�n
    mvarRecordset.Open ("SELECT FECACT FROM UNI WHERE COD='" & mvarUnidadPedido & "'"), mvarConexion.ADOCon, adOpenStatic, adLockReadOnly
    
    If mvarRecordset.eof Then
        mvarRecordset.Close
        Set mvarRecordset = Nothing
        TESError.NumError = TESDatoEliminado
        TESError.Arg1 = 105 ' Tabla UNI
        IBaseDatos_IniciarEdicion = TESError
        Exit Function
    End If
       
    If mvarFecAct <> mvarRecordset("FECACT") Then
        mvarRecordset.Close
        Set mvarRecordset = Nothing
        TESError.NumError = TESInfActualModificada
    End If
    
    IBaseDatos_IniciarEdicion = TESError
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CUnidadPedido", "IBaseDatos_IniciarEdicion", ERR, Erl)
      Exit Function
   End If
End Function


