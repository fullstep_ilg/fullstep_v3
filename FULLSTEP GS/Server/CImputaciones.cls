VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CImputaciones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

''' *** Clase: CImputaciones

''' Variables que almacenan las propiedades de los objetos
Private mCol As Collection
Private m_bEOF As Boolean
Private m_oConexion As CConexion

''' Control de errores
Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum

''' <summary>A�ade una nueva imputaci�n a la colecci�n</summary>
''' <param name="sPRES5">PRES5</param>
''' <param name="sPRES1">PRES1</param>
''' <param name="sPRES2">PRES2</param>
''' <param name="sPRES3">PRES3</param>
''' <param name="sPRES4">PRES4</param>
''' <param name="sCC1">CC1</param>
''' <param name="sCC2">CC2</param>
''' <param name="sCC3">CC3</param>
''' <param name="sCC4">CC4</param>
''' <param name="sCCDen">CCDen</param>
''' <param name="Modo">Modo</param>
''' <param name="lEmp">Emp</param>
''' <param name="sPresDen">Den. Pres.</param>
''' <param name="sCenSM">Cen. SM</param>
''' <param name="dFecIni">Fec. Ini.</param>
''' <param name="dFecFin">Fec. Fin</param>
''' <param name="bCerrado">Cerrado</param>
''' <param name="bIndicePorPres">Indice por Pres.</param>
''' <param name="Cambio">Cambio</param>
''' <param name="lIdEmpresa">Id empresa</param>
''' <param name="vMoneda">Moneda</param>
''' <param name="vGestor">Gestor</param>
''' <remarks>Llamada desde: CProceso.DevolverAdjudicacionesParaPedido</remarks>
''' <returns>Imputaci�n a�adida</returns>
''' <revision>LTG 16/04/2012</revision>

Public Function Add(ByVal sPRES5 As String, Optional ByVal sPRES1 As String = "", Optional ByVal sPRES2 As String = "", Optional ByVal sPRES3 As String = "", _
        Optional ByVal sPRES4 As String = "", Optional ByVal sCC1 As String = "", Optional ByVal sCC2 As String = "", Optional ByVal sCC3 As String = "", _
        Optional ByVal sCC4 As String = "", Optional ByVal sCCDen As String, Optional ByVal Modo As Integer, Optional ByVal lEmp As Long, _
        Optional ByVal sPresDen As String, Optional ByVal sCenSM As String = "", Optional ByVal dFecIni As Date, Optional ByVal dFecFin As Date, _
        Optional ByVal bCerrado As Boolean, Optional ByVal bIndicePorPres As Boolean, Optional ByVal Cambio As Variant, Optional ByVal lIdEmpresa As Long = 0, _
        Optional ByVal vMoneda As Variant, Optional ByVal vGestor As Variant, Optional ByVal sPRES5Id As String = "") As CImputacion
    Dim objnewmember As CImputacion
    Dim sCod As String
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New CImputacion
    
    objnewmember.PRES5 = sPRES5
    objnewmember.PRES5Id = sPRES5Id
    objnewmember.PRES1 = sPRES1
    objnewmember.PRES2 = sPRES2
    objnewmember.Pres3 = sPRES3
    objnewmember.Pres4 = sPRES4
    objnewmember.CC1 = sCC1
    objnewmember.CC2 = sCC2
    objnewmember.CC3 = sCC3
    objnewmember.CC4 = sCC4
    objnewmember.Empresa = lIdEmpresa
    'objnewmember.CentroCoste = lCentroCoste
    objnewmember.Modo = Modo
    objnewmember.Empresa = lEmp
    objnewmember.CCDen = sCCDen
    objnewmember.PresDen = sPresDen
    If sPRES4 <> "" Then
        objnewmember.CodContrato = sPRES4
    ElseIf sPRES3 <> "" Then
        objnewmember.CodContrato = sPRES3
    ElseIf sPRES2 <> "" Then
        objnewmember.CodContrato = sPRES2
    ElseIf sPRES1 <> "" Then
        objnewmember.CodContrato = sPRES1
    Else
        objnewmember.CodContrato = ""
    End If
    If sCC4 <> "" Then
        objnewmember.CodCenCoste = sCC4
    ElseIf sCC3 <> "" Then
        objnewmember.CodCenCoste = sCC3
    ElseIf sCC2 <> "" Then
        objnewmember.CodCenCoste = sCC2
    ElseIf sCC1 <> "" Then
        objnewmember.CodCenCoste = sCC1
    Else
        objnewmember.CodCenCoste = ""
    End If
    
    If sCC2 <> "" Then
        objnewmember.CodConcat = sCC1
        If sCC3 <> "" Then
            objnewmember.CodConcat = sCC1 & "-" & sCC2
            If sCC4 <> "" Then
                objnewmember.CodConcat = sCC1 & "-" & sCC2 & "-" & sCC3
            End If
        End If
    End If
    
    objnewmember.CenSM = sCenSM
    If Not IsMissing(dFecIni) Then
        objnewmember.FecInicio = dFecIni
    Else
        objnewmember.FecInicio = Null
    End If
    If Not IsMissing(dFecFin) Then
        objnewmember.FecFin = dFecFin
    Else
        objnewmember.FecFin = Null
    End If
    objnewmember.Cerrado = bCerrado
    
    If Not IsMissing(Cambio) Then
        objnewmember.Cambio = Cambio
    End If
    
    If Not IsMissing(vMoneda) Then
        objnewmember.Moneda = NullToStr(vMoneda)
    End If
    
    If Not IsMissing(vGestor) Then
        objnewmember.Gestor = NullToStr(vGestor)
    End If
    
    Set objnewmember.Conexion = m_oConexion
        
    If bIndicePorPres Then
        sCod = sPRES5 & "|" & sPRES1 & "|" & sPRES2 & "|" & sPRES3 & "|" & sPRES4
        mCol.Add objnewmember, sCod
    Else
        mCol.Add objnewmember, sPRES5
    End If
            
    Set Add = objnewmember
    
    Set objnewmember = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If Err.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CImputaciones", "Add", Err, Erl)
      Exit Function
   End If

End Function


Friend Property Set Conexion(ByVal con As CConexion)

    ''' * Objetivo: Dar valor a la variable privada Conexion
    ''' * Recibe: Conexion
    ''' * Devuelve: Nada
    
    Set m_oConexion = con
    
End Property
Friend Property Get Conexion() As CConexion

    ''' * Objetivo: Devolver variable privada Conexion
    ''' * Recibe: Nada
    ''' * Devuelve: Conexion
    
    Set Conexion = m_oConexion
    
End Property


Public Property Get Count() As Long

    ''' * Objetivo: Devolver variable privada Count
    ''' * Recibe: Nada
    ''' * Devuelve: Numero de elementos de la coleccion

    If mCol Is Nothing Then
        Count = 0
    Else
        Count = mCol.Count
    End If

End Property

Public Property Get Item(vntIndexKey As Variant) As CImputacion

    ''' * Objetivo: Recuperar un �rbol de la coleccion
    ''' * Recibe: Indice del �rbol a recuperar
    ''' * Devuelve: �rbol correspondiente
        
On Error GoTo NoSeEncuentra:

    ''' Devolvemos el item de la coleccion privada
    
    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:

    Set Item = Nothing
    
End Property


Private Sub Class_Initialize()

    Set mCol = New Collection

End Sub

Private Sub Class_Terminate()
    Set mCol = Nothing
    Set m_oConexion = Nothing
End Sub

Public Sub Remove(vntIndexKey As Variant)
   
    mCol.Remove vntIndexKey
End Sub


Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
     Set NewEnum = mCol.[_NewEnum]
End Property

