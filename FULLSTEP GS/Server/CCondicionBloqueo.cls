VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CCondicionBloqueo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements IBaseDatos


Private Enum TipoDeError

    ConexionNoEstablecida = 613

End Enum


Private moConexion As CConexion
Private m_lID As Long
Private m_lBloque As Long
Private m_lRol As Long
Private m_lCampo As Long
Private m_sCod As String
Private m_iTipoCampo As TipoCampoCondicionEnlace
Private m_lCampoDato As Long
Private m_sOperador As String
Private m_iTipoValor As TipoValorCondicionEnlace
Private m_lCampoValor As Long
Private m_vValor As Variant
Private mvarIndice As Variant
Private msMoneda As String
Private m_oCampo As CFormItem

Friend Property Set Conexion(ByVal con As CConexion)
Set moConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = moConexion
End Property

Public Property Get Id() As Long
    Id = m_lID
End Property
Public Property Let Id(ByVal Data As Long)
    m_lID = Data
End Property

Public Property Get Bloque() As Long
    Bloque = m_lBloque
End Property
Public Property Let Bloque(ByVal Data As Long)
    m_lBloque = Data
End Property

Public Property Get Rol() As Long
    Rol = m_lRol
End Property
Public Property Let Rol(ByVal Data As Long)
    m_lRol = Data
End Property

Public Property Get Campo() As Long
    Campo = m_lCampo
End Property
Public Property Let Campo(ByVal Data As Long)
    m_lCampo = Data
End Property

Public Property Get Cod() As String
    Cod = m_sCod
End Property
Public Property Let Cod(ByVal Data As String)
    m_sCod = Data
End Property

Public Property Get TipoCampo() As TipoCampoCondicionEnlace
    TipoCampo = m_iTipoCampo
End Property
Public Property Let TipoCampo(ByVal Data As TipoCampoCondicionEnlace)
    m_iTipoCampo = Data
End Property

Public Property Get CampoDato() As Long
    CampoDato = m_lCampoDato
End Property
Public Property Let CampoDato(ByVal Data As Long)
    m_lCampoDato = Data
End Property

Public Property Get Operador() As String
    Operador = m_sOperador
End Property
Public Property Let Operador(ByVal Data As String)
    m_sOperador = Data
End Property

Public Property Get TipoValor() As TipoValorCondicionEnlace
    TipoValor = m_iTipoValor
End Property
Public Property Let TipoValor(ByVal Data As TipoValorCondicionEnlace)
    m_iTipoValor = Data
End Property

Public Property Get CampoValor() As Long
    CampoValor = m_lCampoValor
End Property
Public Property Let CampoValor(ByVal Data As Long)
    m_lCampoValor = Data
End Property

Public Property Get Valor() As Variant
    Valor = m_vValor
End Property
Public Property Let Valor(ByVal Data As Variant)
    m_vValor = Data
End Property

Public Property Let Indice(ByVal varIndice As Variant)
    mvarIndice = varIndice
End Property
Public Property Get Indice() As Variant
    Indice = mvarIndice
End Property

Public Property Get CampoForm() As CFormItem
    If m_oCampo Is Nothing Then
        cargarCampo
    End If
    Set CampoForm = m_oCampo
End Property

Public Property Set CampoForm(ByRef oCampo As CFormItem)
    Set m_oCampo = oCampo
End Property

Public Property Get Moneda() As String
    Moneda = msMoneda
End Property

Public Property Let Moneda(ByVal sMoneda As String)
    msMoneda = sMoneda
End Property


Private Sub Class_Terminate()
    Set moConexion = Nothing
End Sub

Private Function IBaseDatos_AnyadirABaseDatos() As TipoErrorSummit
Dim sConsulta As String
Dim sConsulta2 As String
Dim TESError As TipoErrorSummit
Dim AdoRes As adodb.Recordset
Dim bTransaccionEnCurso As Boolean

Dim tipoCampoCondicion As TiposDeAtributos

TESError.NumError = TESnoerror

If moConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CCondicionEnlace.AnyadirABAseDatos", "No se ha establecido la conexion"
End If
' *****************************************

On Error GoTo ERROR:

    moConexion.ADOCon.Execute "BEGIN DISTRIBUTED TRAN"
    bTransaccionEnCurso = True
    moConexion.ADOCon.Execute "SET XACT_ABORT OFF"


    'Insetar la Condición
    sConsulta = "INSERT INTO PM_CONF_CUMP_BLOQUE_COND " & _
        "(BLOQUE, ROL, CAMPO, COD, TIPO_CAMPO, CAMPO_DATO, OPERADOR, TIPO_VALOR, CAMPO_VALOR, VALOR_TEXT,VALOR_FEC,VALOR_BOOL,VALOR_NUM, MON) " & _
        "VALUES " & _
        "(" & Dbl0ToSQLNull(m_lBloque) & "," & Dbl0ToSQLNull(m_lRol) & "," & Dbl0ToSQLNull(m_lCampo) & "," & StrToSQLNULL(m_sCod) & "," & m_iTipoCampo

    If m_iTipoCampo = TipoCampoCondicionEnlace.CampoDeFormulario Then
        sConsulta = sConsulta & "," & m_lCampoDato
    Else
        sConsulta = sConsulta & "," & "NULL"
    End If
    sConsulta = sConsulta & ",'" & DblQuote(m_sOperador) & "'," & m_iTipoValor
    If m_iTipoValor = TipoValorCondicionEnlace.CampoDeFormulario Then
        sConsulta = sConsulta & "," & m_lCampoValor & ",NULL,NULL,NULL,NULL"
    ElseIf m_iTipoValor = TipoValorCondicionEnlace.ValorEstatico Then
        sConsulta = sConsulta & ",NULL"
        Select Case m_iTipoCampo
            Case TipoCampoCondicionEnlace.CampoDeFormulario
                'Mirar el tipo del Campo y poner consecuentemente
                sConsulta2 = "SELECT SUBTIPO FROM FORM_CAMPO WITH (NOLOCK) WHERE ID = " & m_lCampoDato
                Set AdoRes = New adodb.Recordset
                AdoRes.Open sConsulta2, moConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
                tipoCampoCondicion = AdoRes("SUBTIPO").Value
                AdoRes.Close
                Set AdoRes = Nothing
                Select Case tipoCampoCondicion
                    Case TiposDeAtributos.TipoString, TiposDeAtributos.TipoTextoCorto, TiposDeAtributos.TipoTextoMedio, TiposDeAtributos.TipoTextoLargo
                        sConsulta = sConsulta & ",'" & DblQuote(CStr(m_vValor)) & "',NULL,NULL,NULL"
                    Case TiposDeAtributos.TipoNumerico
                        sConsulta = sConsulta & ",NULL,NULL,NULL," & CLng(m_vValor) & ""
                    Case TiposDeAtributos.TipoFecha
                        sConsulta = sConsulta & ",NULL,'" & CDate(m_vValor) & "',NULL,NULL"
                    Case TiposDeAtributos.TipoBoolean
                        sConsulta = sConsulta & ",NULL,NULL," & BooleanToSQLBinary(CBool(m_vValor)) & ",NULL "
                End Select
            Case TipoCampoCondicionEnlace.Peticionario, TipoCampoCondicionEnlace.DepPetcionario, TipoCampoCondicionEnlace.UONPeticionario
                sConsulta = sConsulta & ",'" & DblQuote(CStr(m_vValor)) & "',NULL,NULL,NULL"
            Case TipoCampoCondicionEnlace.NumProcesAbiertos, TipoCampoCondicionEnlace.ImporteAdjudicado, TipoCampoCondicionEnlace.ImporteSolicitud
                sConsulta = sConsulta & ",NULL,NULL,NULL," & CLng(m_vValor)
        End Select
    ElseIf m_iTipoValor = TipoValorCondicionEnlace.Peticionario Or m_iTipoValor = TipoValorCondicionEnlace.DepPetcionario Or _
        m_iTipoValor = TipoValorCondicionEnlace.UONPeticionario Or m_iTipoValor = TipoValorCondicionEnlace.NumProcesAbiertos Or _
        m_iTipoValor = TipoValorCondicionEnlace.ImporteAdjudicado Or m_iTipoValor = TipoValorCondicionEnlace.ImporteSolicitud Then
            sConsulta = sConsulta & ",NULL,NULL,NULL,NULL,NULL"
    End If
    sConsulta = sConsulta & " , " & StrToSQLNULL(msMoneda)
    sConsulta = sConsulta & ")"
    moConexion.ADOCon.Execute sConsulta
    If moConexion.ADOCon.Errors.Count > 0 Then
        GoTo ERROR
    End If

    sConsulta = "SELECT MAX(ID) ID FROM PM_CONF_CUMP_BLOQUE_COND WITH (NOLOCK) "
    Set AdoRes = New adodb.Recordset
    AdoRes.Open sConsulta, moConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    m_lID = AdoRes("ID").Value
    AdoRes.Close
    Set AdoRes = Nothing

    moConexion.ADOCon.Execute "COMMIT TRAN"
    bTransaccionEnCurso = False

    IBaseDatos_AnyadirABaseDatos = TESError
    Exit Function
ERROR:

    If Not AdoRes Is Nothing Then
        AdoRes.Close
        Set AdoRes = Nothing
    End If

    IBaseDatos_AnyadirABaseDatos = basErrores.TratarError(moConexion.ADOCon.Errors)


    If bTransaccionEnCurso Then
        moConexion.ADOCon.Execute "ROLLBACK TRAN"
        bTransaccionEnCurso = False
    End If


End Function

Private Function IBaseDatos_CambiarCodigo(CodigoNuevo As Variant) As TipoErrorSummit

End Function
'
Private Sub IBaseDatos_CancelarEdicion()

End Sub
'
Private Function IBaseDatos_ComprobarExistenciaEnBaseDatos() As Boolean

End Function
'
Private Function IBaseDatos_EliminarDeBaseDatos() As TipoErrorSummit

Dim TESError As TipoErrorSummit
Dim btrans As Boolean
Dim sConsulta As String

TESError.NumError = TESnoerror

'******************* Precondicion *******************
If moConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CCondicionBloqueo.EliminarDeBaseDatos", "No se ha establecido la conexion"
End If
'*****************************************************
On Error GoTo ERROR:

    moConexion.ADOCon.Execute "BEGIN TRAN"
    moConexion.ADOCon.Execute "SET XACT_ABORT OFF"
    btrans = True

    sConsulta = "DELETE FROM PM_CONF_CUMP_BLOQUE_COND WHERE ID = " & m_lID

    moConexion.ADOCon.Execute sConsulta
    If moConexion.ADOCon.Errors.Count > 0 Then
        GoTo ERROR
    End If
    moConexion.ADOCon.Execute "COMMIT TRAN"
    btrans = False


    IBaseDatos_EliminarDeBaseDatos = TESError

    Exit Function

ERROR:

    If moConexion.ADOCon.Errors.Count > 0 Then
        IBaseDatos_EliminarDeBaseDatos = basErrores.TratarError(moConexion.ADOCon.Errors)
        If btrans Then
            moConexion.ADOCon.Execute "ROLLBACK TRAN"
            btrans = False
        End If
        Exit Function
    End If

End Function
'
Private Function IBaseDatos_FinalizarEdicionEliminando() As TipoErrorSummit
'
End Function

Private Function IBaseDatos_FinalizarEdicionModificando() As TipoErrorSummit

Dim TESError As TipoErrorSummit
Dim sConsulta As String
Dim sConsulta2 As String
Dim bTransaccionEnCurso As Boolean
Dim AdoRes As adodb.Recordset
Dim tipoCampoCondicion As TipoCampoCondicionEnlace


TESError.NumError = TESnoerror

'********* Precondicion *******************
If moConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CCondicionBloqueo.FinalizarEdicionModificando", "No se ha establecido la conexion"
End If
'******************************************

On Error GoTo ERROR:

    moConexion.ADOCon.Execute "BEGIN TRAN"
    bTransaccionEnCurso = True
    moConexion.ADOCon.Execute "SET XACT_ABORT OFF"


    ''' Actualizar Condicion
    sConsulta = "UPDATE PM_CONF_CUMP_BLOQUE_COND SET "

    sConsulta = sConsulta & " COD = " & StrToSQLNULL(m_sCod) & _
    ",  TIPO_CAMPO = " & m_iTipoCampo

    If m_iTipoCampo = TipoCampoCondicionEnlace.CampoDeFormulario Then
        sConsulta = sConsulta & ",CAMPO_DATO = " & m_lCampoDato
    Else
        sConsulta = sConsulta & ",CAMPO_DATO = NULL "
    End If
        


    sConsulta = sConsulta & ", OPERADOR = " & StrToSQLNULL(m_sOperador) & ", " & _
        "TIPO_VALOR = " & m_iTipoValor
    If m_iTipoValor = TipoValorCondicionEnlace.CampoDeFormulario Then
        sConsulta = sConsulta & ", CAMPO_VALOR = " & m_lCampoValor & _
        ", VALOR_TEXT = NULL, VALOR_NUM = NULL, VALOR_FEC = NULL, VALOR_BOOL = NULL "
    ElseIf m_iTipoValor = TipoValorCondicionEnlace.ValorEstatico Then
        sConsulta = sConsulta & ", CAMPO_VALOR = NULL "
        Select Case m_iTipoCampo
            Case TipoCampoCondicionEnlace.CampoDeFormulario
                'Mirar el tipo del Campo y poner consecuentemente
                sConsulta2 = "SELECT SUBTIPO FROM FORM_CAMPO WITH (NOLOCK) WHERE ID = " & m_lCampo
                Set AdoRes = New adodb.Recordset
                AdoRes.Open sConsulta2, moConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
                tipoCampoCondicion = AdoRes("SUBTIPO").Value
                AdoRes.Close
                Set AdoRes = Nothing
                Select Case tipoCampoCondicion
                    Case TiposDeAtributos.TipoString, TiposDeAtributos.TipoTextoCorto, TiposDeAtributos.TipoTextoMedio, TiposDeAtributos.TipoTextoLargo
                        sConsulta = sConsulta & ", VALOR_TEXT = '" & DblQuote(CStr(m_vValor)) & "'" & _
                            ", VALOR_NUM = NULL, VALOR_FEC = NULL, VALOR_BOOL = NULL "
                    Case TiposDeAtributos.TipoNumerico
                        sConsulta = sConsulta & ", VALOR_TEXT = NULL " & _
                            ", VALOR_NUM = " & CLng(m_vValor) & _
                            ", VALOR_FEC = NULL, VALOR_BOOL = NULL "
                    Case TiposDeAtributos.TipoFecha
                        sConsulta = sConsulta & ",VALOR_TEXT = NULL, VALOR_NUM = NULL " & _
                            ", VALOR_FEC = '" & CDate(m_vValor) & "'" & _
                            ", VALOR_BOOL = NULL "
                    Case TiposDeAtributos.TipoBoolean
                        sConsulta = sConsulta & ", VALOR_TEXT = NULL,VALOR_NUM = NULL, VALOR_FEC = NULL " & _
                            ", VALOR_BOOL = " & BooleanToSQLBinary(CBool(m_vValor))
                End Select
            Case TipoCampoCondicionEnlace.Peticionario, TipoCampoCondicionEnlace.DepPetcionario, TipoCampoCondicionEnlace.UONPeticionario
                sConsulta = sConsulta & ", VALOR_TEXT = '" & DblQuote(CStr(m_vValor)) & "'" & _
                    ", VALOR_NUM = NULL, VALOR_FEC = NULL, VALOR_BOOL = NULL "
            Case TipoCampoCondicionEnlace.NumProcesAbiertos, TipoCampoCondicionEnlace.ImporteAdjudicado, TipoCampoCondicionEnlace.ImporteSolicitud
                sConsulta = sConsulta & ", VALOR_TEXT = NULL " & _
                    ", VALOR_NUM = " & CLng(m_vValor) & _
                    ", VALOR_FEC = NULL, VALOR_BOOL = NULL "
        End Select
    ElseIf m_iTipoValor = TipoValorCondicionEnlace.DepPetcionario Or m_iTipoValor = TipoValorCondicionEnlace.ImporteAdjudicado Or _
        m_iTipoValor = TipoValorCondicionEnlace.ImporteSolicitud Or m_iTipoValor = TipoValorCondicionEnlace.NumProcesAbiertos Or _
        m_iTipoValor = TipoValorCondicionEnlace.Peticionario Or m_iTipoValor = TipoValorCondicionEnlace.UONPeticionario Then
            sConsulta = sConsulta & ", CAMPO_VALOR = NULL, VALOR_TEXT = NULL, VALOR_NUM = NULL, VALOR_FEC = NULL, VALOR_BOOL = NULL "
    End If
    sConsulta = sConsulta & ",MON = " & StrToSQLNULLNVar(DblQuote(msMoneda)) & ""
    sConsulta = sConsulta & " WHERE ID = " & m_lID
    moConexion.ADOCon.Execute sConsulta
    If moConexion.ADOCon.Errors.Count > 0 Then
        GoTo ERROR
    End If


    moConexion.ADOCon.Execute "COMMIT TRAN"
    bTransaccionEnCurso = False

    IBaseDatos_FinalizarEdicionModificando = TESError

    Exit Function


ERROR:

    IBaseDatos_FinalizarEdicionModificando = basErrores.TratarError(moConexion.ADOCon.Errors)

    If bTransaccionEnCurso Then
        moConexion.ADOCon.Execute "ROLLBACK"
        moConexion.ADOCon.Execute "SET XACT_ABORT OFF"
        bTransaccionEnCurso = False
    End If

End Function
'
Private Function IBaseDatos_IniciarEdicion(Optional ByVal Bloquear As Boolean, Optional ByVal UsuarioBloqueo As String) As TipoErrorSummit

Dim TESError As TipoErrorSummit
Dim sConsulta As String
Dim rs As Recordset


TESError.NumError = TESnoerror

'******************* Precondicion *******************
If moConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CCondicionBloqueo.IniciarEdicion", "No se ha establecido la conexion"
End If
'*****************************************************


'''Cargar Condicion
Set rs = New adodb.Recordset


sConsulta = "SELECT BLOQUE, ROL, CAMPO,COD, TIPO_CAMPO, CAMPO_DATO, OPERADOR, TIPO_VALOR, CAMPO_VALOR, VALOR_TEXT, VALOR_FEC, VALOR_BOOL, VALOR_NUM"
sConsulta = sConsulta & " FROM PM_CONF_CUMP_BLOQUE_COND WITH (NOLOCK) WHERE ID = " & m_lID
rs.Open sConsulta, moConexion.ADOCon, adOpenKeyset, adLockReadOnly

If rs.eof Then
        rs.Close
        Set rs = Nothing
        TESError.NumError = TESDatoEliminado
        TESError.Arg1 = 181
        IBaseDatos_IniciarEdicion = TESError
        Exit Function
End If
m_lBloque = NullToDbl0(rs.Fields("BLOQUE").Value)
m_lRol = NullToDbl0(rs.Fields("ROL").Value)
m_lCampo = NullToDbl0(rs.Fields("CAMPO").Value)
m_sCod = NullToStr(rs.Fields("COD").Value)
m_iTipoCampo = NullToDbl0(rs.Fields("TIPO_CAMPO").Value)
m_lCampoDato = NullToDbl0(rs.Fields("CAMPO_DATO").Value)
m_sOperador = NullToStr(rs.Fields("OPERADOR").Value)

m_iTipoValor = NullToDbl0(rs.Fields("TIPO_VALOR").Value)
m_lCampoValor = NullToDbl0(rs.Fields("CAMPO_VALOR").Value)
If Not IsNull(rs.Fields("VALOR_TEXT").Value) Then
    m_vValor = NullToStr(rs.Fields("VALOR_TEXT").Value)
ElseIf Not IsNull(rs.Fields("VALOR_NUM").Value) Then
    m_vValor = NullToDbl0(rs.Fields("VALOR_NUM").Value)
ElseIf Not IsNull(rs.Fields("VALOR_FEC").Value) Then
    m_vValor = rs.Fields("VALOR_FEC").Value
ElseIf Not IsNull(rs.Fields("VALOR_BOOL").Value) Then
    m_vValor = SQLBinaryToBoolean(rs.Fields("VALOR_BOOL").Value)
End If

rs.Close
Set rs = Nothing

IBaseDatos_IniciarEdicion = TESError

End Function


Public Function incluyeCampoImporte() As Boolean
    incluyeCampoImporte = CampoForm.esTipoImporteoCalculado
End Function



Public Sub cargarCampo()
    Set m_oCampo = New CFormItem
    Set m_oCampo.Conexion = Me.Conexion
    m_oCampo.Id = Campo
    m_oCampo.CargarDatosFormCampo
End Sub


