VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CCarpetasSolicitN3"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CCarpetasSolicitN3 **********************************
'*             Autor : Gorka Areitioaurtena
'*             Creada : 13/02/06
'****************************************************************

Option Explicit

Private mCol As Collection
Private mvarConexion As CConexion
Private mvarConexionServer As CConexionDeUseServer
Private mvarEOF As Boolean

Friend Property Set Conexion(ByVal con As CConexion)
    Set mvarConexion = con
End Property
Friend Property Get Conexion() As CConexion
    Set Conexion = mvarConexion
End Property

Friend Property Set ConexionServer(ByVal con As CConexionDeUseServer)
    Set mvarConexionServer = con
End Property
Friend Property Get ConexionServer() As CConexionDeUseServer
    Set ConexionServer = mvarConexionServer
End Property

Public Property Get eof() As Boolean
    eof = mvarEOF
End Property
Friend Property Let eof(ByVal b As Boolean)
    mvarEOF = b
End Property

Public Property Get Item(vntIndexKey As Variant) As CCarpetaSolicitN3
    On Error GoTo NoSeEncuentra:
        Set Item = mCol(vntIndexKey)
    Exit Property
NoSeEncuentra:
        Set Item = Nothing
End Property

Public Property Get Count() As Long
    If mCol Is Nothing Then
        Count = 0
    Else
        Count = mCol.Count
    End If
End Property

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
     Set NewEnum = mCol.[_NewEnum]
End Property

Private Sub Class_Initialize()

        Set mCol = New Collection
End Sub

Private Sub Class_Terminate()
    'destroys collection when this class is terminated
    Set mCol = Nothing
    Set mvarConexion = Nothing
    Set mvarConexionServer = Nothing

End Sub

Public Function Add(ByVal Id As Long, ByVal CSN1 As Long, ByVal CSN2 As Long, ByVal Den As String) As CCarpetaSolicitN3
    'create a new object
    Dim objnewmember As CCarpetaSolicitN3
    Dim lId As Long
    
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New CCarpetaSolicitN3
   
    objnewmember.Id = Id
    objnewmember.CSN1 = CSN1
    objnewmember.CSN2 = CSN2
    objnewmember.Den = Den
    Set objnewmember.Conexion = mvarConexion
    
    lId = Id
    mCol.Add objnewmember, CStr(lId)
    
    Set Add = objnewmember
    Set objnewmember = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CCarpetasSolicitN3", "Add", ERR, Erl)
      Exit Function
   End If

End Function


Public Sub Remove(vntIndexKey As Variant)

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    mCol.Remove vntIndexKey
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CCarpetasSolicitN3", "Remove", ERR, Erl)
      Exit Sub
   End If
End Sub



