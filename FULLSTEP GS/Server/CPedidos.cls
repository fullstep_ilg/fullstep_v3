VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CPedidos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum
Private mCol As Collection
Private mvarConexion As CConexion

Friend Property Set Conexion(ByVal con As CConexion)
Set mvarConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = mvarConexion
End Property


Public Property Get Item(vntIndexKey As Variant) As CPedido
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property
Public Property Get NewEnum() As IUnknown
       Set NewEnum = mCol.[_NewEnum]
End Property
Public Sub Remove(vntIndexKey As Variant)
   
    mCol.Remove vntIndexKey
End Sub
Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
     Count = mCol.Count
End If


End Property
Public Function Add(Optional ByVal ID As Long, Optional ByVal Anyo As Integer, Optional ByVal Numero As Long, Optional ByVal estado As TipoEstadoOrdenEntrega, Optional ByVal Tipo As TipoPedido, Optional ByVal persona As Variant, Optional ByVal Fecha As Date, Optional ByVal oOrdenesEntrega As COrdenesEntrega, Optional ByVal oNotificaciones As CNotificacionesPedido, Optional ByVal varIndice As Variant) As CPedido
   
    Dim objnewmember As CPedido
    
    Set objnewmember = New CPedido
    
    objnewmember.ID = ID
    objnewmember.Anyo = Anyo
    objnewmember.Numero = Numero
    objnewmember.estado = estado
    objnewmember.Tipo = Tipo
    'objnewmember.Usuario = Usuario
    objnewmember.persona = persona
    objnewmember.Fecha = Fecha
    
    Set objnewmember.OrdenesEntrega = oOrdenesEntrega
    
    If oNotificaciones Is Nothing Then
        Set objnewmember.OrdenesEntrega = New COrdenesEntrega
    Else
        Set objnewmember.OrdenesEntrega = oOrdenesEntrega
    End If
    
    
    If oNotificaciones Is Nothing Then
        Set objnewmember.Notificaciones = New CNotificacionesPedido
    Else
        Set objnewmember.Notificaciones = oNotificaciones
    End If
    
    
    
    Set objnewmember.Conexion = mvarConexion
        
    If Not IsMissing(varIndice) And Not IsNull(varIndice) Then
        objnewmember.Indice = varIndice
        mCol.Add objnewmember, CStr(varIndice)
    Else
        mCol.Add objnewmember, ID
    End If
    
    
    Set Add = objnewmember
    
    Set objnewmember = Nothing

End Function


Private Sub Class_Initialize()
    Set mCol = New Collection
End Sub
Private Sub Class_Terminate()
    'destroys collection when this class is terminated
    Set mCol = Nothing
    Set mvarConexion = Nothing
End Sub
