VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CPerfiles"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Collection" ,"CPerfil"
Attribute VB_Ext_KEY = "Member0" ,"CPerfil"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

'local variable to hold collection
Private mCol As Collection
Private mvarConexion As CConexion
Private mvarConexionServer As CConexionDeUseServer

''' <summary>A�ade un nuevo perfil a la colecci�n de perfiles</summary>
''' <param name="lID">ID</param>
''' <param name="Cod">C�digo</param>
''' <param name="Den">deniminaci�n</param>
''' <param name="bFSGS">acceso a FSGS</param>
''' <param name="bFSPM">acceso a FSPM</param>
''' <param name="bFSQA">acceso a FSQA</param>
''' <param name="bFSEP">acceso a FSEP</param>
''' <param name="bFSSM">acceso a FSSM</param>
''' <param name="bFSCM">acceso a FSCM</param>
''' <param name="bFSIS">acceso a FSIS</param>
''' <param name="bFSIM">acceso a FSIM</param>
''' <param name="bFSBI">acceso a FSBI</param>
''' <param name="varIndice">�ndice</param>
''' <remarks>Llamada desde: CGestorSeguridad.devolverTodosLosPerfiles</remarks>
''' <revision>LTG 17/12/2012</revision>

Public Function Add(ByVal lId As Long, ByVal Cod As String, ByVal Den As String, ByVal bFSGS As Boolean, ByVal bFSPM As Boolean, ByVal bFSQA As Boolean, _
        ByVal bFSEP As Boolean, ByVal bFSSM As Boolean, ByVal bFSCM As Boolean, ByVal bFSIS As Boolean, ByVal bFSIM As Boolean, ByVal bFSBI As Boolean, _
        Optional ByVal varIndice As Variant) As CPerfil
    Dim oPerfil As CPerfil
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oPerfil = New CPerfil
    
    oPerfil.Id = lId
    oPerfil.Cod = Cod
    oPerfil.Den = Den
    oPerfil.FSGS = bFSGS
    oPerfil.FSPM = bFSPM
    oPerfil.FSQA = bFSQA
    oPerfil.FSEP = bFSEP
    oPerfil.FSSM = bFSSM
    oPerfil.FSCM = bFSCM
    oPerfil.FSIS = bFSIS
    oPerfil.FSIM = bFSIM
    oPerfil.FSBI = bFSBI
    
    Set oPerfil.Conexion = mvarConexion
    Set oPerfil.ConexionServer = mvarConexionServer
    
    If Not IsMissing(varIndice) And Not IsNull(varIndice) Then
        mCol.Add oPerfil, CStr(varIndice)
    Else
        mCol.Add oPerfil, Cod
    End If
    
    Set Add = oPerfil
    Set oPerfil = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CPerfiles", "Add", ERR, Erl)
        Exit Function
    End If
End Function

Public Property Get Item(vntIndexKey As Variant) As CPerfil
Attribute Item.VB_UserMemId = 0
On Error GoTo vacio:
 
  Set Item = mCol(vntIndexKey)
  Exit Property

vacio:
  Set Item = Nothing

End Property

Friend Property Get Conexion() As CConexion
    
    Set Conexion = mvarConexion

End Property

Friend Property Set Conexion(ByVal Data As CConexion)
    Set mvarConexion = Data
End Property
Friend Property Set ConexionServer(ByVal con As CConexionDeUseServer)
Set mvarConexionServer = con
End Property

Friend Property Get ConexionServer() As CConexionDeUseServer
Set ConexionServer = mvarConexionServer
End Property

Public Property Get Count() As Long
      Count = mCol.Count
End Property


Public Sub Remove(vntIndexKey As Variant)
    mCol.Remove vntIndexKey
End Sub


Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
       Set NewEnum = mCol.[_NewEnum]
End Property


Private Sub Class_Initialize()
    
     
    Set mCol = New Collection
End Sub


Private Sub Class_Terminate()
    
    Set mCol = Nothing
    Set mvarConexion = Nothing
    Set mvarConexionServer = Nothing
    
End Sub


