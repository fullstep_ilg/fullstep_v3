VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CNotificadosAccion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum

Private m_Col As Collection
Private m_oConexion As CConexion
Private m_bEOF As Boolean

Public Property Get eof() As Boolean
    eof = m_bEOF
End Property
Friend Property Let eof(ByVal b As Boolean)
    m_bEOF = b
End Property

Public Property Get Item(vntIndexKey As Variant) As CNotificadoAccion
On Error GoTo NoSeEncuentra:

    Set Item = m_Col(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Friend Property Set Conexion(ByVal con As CConexion)
Set m_oConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = m_oConexion
End Property

Public Property Get Count() As Long
    If m_Col Is Nothing Then
        Count = 0
    Else
         Count = m_Col.Count
    End If
End Property

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
     Set NewEnum = m_Col.[_NewEnum]
End Property

Public Function Add(ByVal lId As Long, ByVal iOrigen As OrigenNotificadoAccion, ByVal iTipo As Integer, Optional ByVal lEnlace As Long = 0, Optional ByVal lAccion As Long = 0, Optional ByVal sPer As String, Optional ByVal lRol As Long, Optional ByVal dtFecAct As Variant, Optional ByVal vIndice As Variant, Optional ByVal bConfiguracion_Rol As Boolean) As CNotificadoAccion
    
    'create a new object
    Dim objnewmember As CNotificadoAccion
    
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New CNotificadoAccion
    With objnewmember
        Set .Conexion = m_oConexion
    
        .Id = lId
        .Origen = iOrigen
        .Enlace = lEnlace
        .Accion = lAccion
        .Tipo = iTipo
        .Per = sPer
        .Rol = lRol
        .FecAct = dtFecAct
        .Configuracion_Rol = bConfiguracion_Rol
    
        If Not IsMissing(vIndice) And Not IsNull(vIndice) Then
            .Indice = vIndice
            m_Col.Add objnewmember, CStr(vIndice)
        Else
            m_Col.Add objnewmember, CStr(lId)
        End If
    
    End With
    
    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CNotificadosAccion", "Add", ERR, Erl)
      Exit Function
   End If

End Function

Public Sub AddNotificadoEnlace(ByVal oNotifEnlace As CNotificadoAccion, Optional ByVal vIndice As Variant)

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not IsMissing(vIndice) And Not IsNull(vIndice) Then
        oNotifEnlace.Indice = vIndice
        m_Col.Add oNotifEnlace, CStr(vIndice)
    Else
        m_Col.Add oNotifEnlace, CStr(oNotifEnlace.Id)
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CNotificadosAccion", "AddNotificadoEnlace", ERR, Erl)
      Exit Sub
   End If
End Sub

Public Sub Remove(vntIndexKey As Variant)


If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_Col.Remove vntIndexKey
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CNotificadosAccion", "Remove", ERR, Erl)
      Exit Sub
   End If

End Sub


Private Sub Class_Initialize()
    Set m_Col = New Collection
End Sub


Private Sub Class_Terminate()
    'Proceroys collection when this class is terminated
    On Error Resume Next
    Set m_Col = Nothing
    Set m_oConexion = Nothing
    
End Sub

Public Function DevolverNotificados(ByVal iOrigen As OrigenNotificadoAccion, Optional ByVal lId As Long = 0) As adodb.Recordset
Dim oadorecordset As adodb.Recordset
Dim sql As String
Dim tableName As String
Dim foreingKeyName As String


If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If iOrigen = OrigenNotificadoAccion.Enlace Then
        tableName = "PM_NOTIFICADO_ENLACE WITH (NOLOCK)"
        foreingKeyName = "ENLACE"
    ElseIf iOrigen = OrigenNotificadoAccion.Accion Then
        tableName = "PM_NOTIFICADO_ACCION WITH (NOLOCK)"
        foreingKeyName = "ACCION"
    Else
        tableName = "PM_NOTIFICADO_AVISO_EXPIRACION WITH (NOLOCK)"
        foreingKeyName = "AVISO_EXPIRACION_CAMPOS"
    End If
    
    
    sql = "SELECT ID, " & foreingKeyName & ", TIPO_NOTIFICADO, PER, ROL, CONFIGURACION_ROL , FECACT FROM " & tableName
    If lId > 0 Then
        sql = sql & " WHERE " & foreingKeyName & " = " & lId
    End If
        
    Set oadorecordset = New adodb.Recordset
    oadorecordset.CursorLocation = adUseClient
    oadorecordset.Open sql, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    oadorecordset.ActiveConnection = Nothing
        
    Set DevolverNotificados = oadorecordset
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CNotificadosAccion", "DevolverNotificados", ERR, Erl)
      Exit Function
   End If
    
End Function

Public Sub CargarNotificados(ByVal iOrigen As OrigenNotificadoAccion, Optional ByVal lId As Long)
    Dim oRS As adodb.Recordset
    
            
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oRS = DevolverNotificados(iOrigen, lId)
    While Not oRS.eof
        If iOrigen = OrigenNotificadoAccion.Enlace Then
            Add NullToDbl0(oRS("ID").Value), iOrigen, NullToDbl0(oRS("TIPO_NOTIFICADO").Value), NullToDbl0(oRS("ENLACE").Value), , NullToStr(oRS("PER").Value), NullToDbl0(oRS("ROL").Value), oRS("FECACT").Value, , NullToDbl0(oRS("CONFIGURACION_ROL").Value)
        ElseIf iOrigen = OrigenNotificadoAccion.Accion Then
            Add NullToDbl0(oRS("ID").Value), iOrigen, NullToDbl0(oRS("TIPO_NOTIFICADO").Value), , NullToDbl0(oRS("ACCION").Value), NullToStr(oRS("PER").Value), NullToDbl0(oRS("ROL").Value), oRS("FECACT").Value, , NullToDbl0(oRS("CONFIGURACION_ROL").Value)
        Else
            Add NullToDbl0(oRS("ID").Value), iOrigen, NullToDbl0(oRS("TIPO_NOTIFICADO").Value), , NullToDbl0(oRS("AVISO_EXPIRACION_CAMPOS").Value), NullToStr(oRS("PER").Value), NullToDbl0(oRS("ROL").Value), oRS("FECACT").Value, , NullToDbl0(oRS("CONFIGURACION_ROL").Value)
        End If
        oRS.MoveNext
    Wend
    oRS.Close
    Set oRS = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CNotificadosAccion", "CargarNotificados", ERR, Erl)
      Exit Sub
   End If
End Sub



