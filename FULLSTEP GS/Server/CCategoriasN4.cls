VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CCategoriasN4"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CCategoriasN4 **********************************
'*             Autor : Elena Mu�oz
'*             Creada : 12/07/2001
'****************************************************************

Option Explicit

'Implements IBaseDatos

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

Private mCol As Collection
Private mvarConexion As CConexion
Private mvarEOF As Boolean

Public Function Add(ByVal ID As Long, ByVal Cod As String, ByVal Den As String, Optional ByVal Cat1 As Long, Optional ByVal Cat1Cod As String, Optional ByVal Cat2 As Long, Optional ByVal Cat2Cod As String, Optional ByVal Cat3 As Long, Optional ByVal Cat3Cod As String, Optional ByVal BajaLogica As TipoBajaLogica, _
Optional ByVal varIndice As Variant, Optional ByVal IDSeguridad As Long, Optional ByVal FechaActual As Date, Optional ByVal Integracion As Boolean, Optional ByVal vObs As Variant, _
  Optional ByVal bModifEmision As Boolean, Optional ByVal bModifEmisionCatalogo As Boolean) As CCategoriaN4
    'create a new object
    Dim objNewMember As CCategoriaN4
    Dim sCod As String
    
    Set objNewMember = New CCategoriaN4
   
    objNewMember.ID = ID
    objNewMember.Cod = Cod
    objNewMember.Den = Den
    objNewMember.Cat1 = Cat1
    objNewMember.Cat1Cod = Cat1Cod
    objNewMember.Cat2 = Cat2
    objNewMember.Cat2Cod = Cat2Cod
    objNewMember.Cat3 = Cat3
    objNewMember.Cat3Cod = Cat3Cod
    objNewMember.BajaLogica = BajaLogica
    objNewMember.Integracion = Integracion
    If IDSeguridad <> 0 Then
        objNewMember.Seguridad = IDSeguridad
    End If
    
    If IsMissing(FechaActual) Then
        objNewMember.FecAct = Null
    Else
        objNewMember.FecAct = FechaActual
    End If
    If IsMissing(vObs) Then
        objNewMember.Observaciones = Null
    Else
        objNewMember.Observaciones = vObs
    End If
    objNewMember.ModificarPrecEmision = bModifEmision
    objNewMember.ModificarPrecEmisionCat = bModifEmisionCatalogo
    
    Set objNewMember.Conexion = mvarConexion
    Set objNewMember.CategoriasN5 = New CCategoriasN5
    Set objNewMember.CategoriasN5.Conexion = mvarConexion
        
    If Not IsMissing(varIndice) And Not IsNull(varIndice) Then
        objNewMember.Indice = varIndice
        mCol.Add objNewMember, CStr(varIndice)
    Else
        sCod = Cat1Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(Cat1Cod))
        sCod = sCod & Cat2Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT2 - Len(Cat2Cod))
        sCod = sCod & Cat3Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT3 - Len(Cat3Cod))
        sCod = sCod & Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT4 - Len(Cod))
        mCol.Add objNewMember, sCod
    End If
      
    Set Add = objNewMember
    Set objNewMember = Nothing

End Function

Friend Property Set Conexion(ByVal con As CConexion)
Set mvarConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = mvarConexion
End Property

Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
     Count = mCol.Count
End If

End Property

Public Function DevolverTodasLasCategorias(Optional ByVal CaracteresInicialesCod As String, Optional ByVal CaracteresInicialesDen As String, Optional ByVal CoincidenciaTotal As Boolean, Optional ByVal OrdenadosPorDen As Boolean, Optional ByVal PersonaAprov As String, Optional ByVal VerBajas As Boolean) As ADODB.Recordset
Dim adoRecordset As ADODB.Recordset
Dim sConsulta As String

'********* Precondicion **************************************
If mvarConexion Is Nothing Then
    Err.Raise vbObjectError + 613, "CCategoriasN1DevolverTodasLasCategorias", "No se ha establecido la conexion"
    Exit Function
End If
'*************************************************************
  
 Set adoRecordset = New ADODB.Recordset
    
    If CoincidenciaTotal Then
        If PersonaAprov = "" Then
            sConsulta = "SELECT CATN4.ID,CATN4.COD,CATN4.DEN,CATN1.COD AS CAT1, CATN2.COD AS CAT2, CATN3.COD AS CAT3 FROM CATN4 WITH (NOLOCK)"
            sConsulta = sConsulta & " INNER JOIN CATN1 WITH (NOLOCK) ON CATN1.ID=CATN4.CAT1"
            sConsulta = sConsulta & " INNER JOIN CATN2 WITH (NOLOCK) ON CATN2.ID=CATN4.CAT2"
            sConsulta = sConsulta & " INNER JOIN CATN3 WITH (NOLOCK) ON CATN3.ID=CATN4.CAT3 WHERE 1=1"
            If CaracteresInicialesCod <> "" Then
                 sConsulta = sConsulta & " AND CATN4.COD='" & DblQuote(CaracteresInicialesCod) & "'"
            End If
            If CaracteresInicialesDen <> "" Then
                sConsulta = sConsulta & " AND CATN4.DEN='" & DblQuote(CaracteresInicialesDen) & "'"
            End If
        Else
            sConsulta = "SELECT distinct CAT_PER.CAT4,CATN4.COD,CATN4.DEN,CATN1.COD AS CAT1, CATN2.COD AS CAT2, CATN3.COD AS CAT3 FROM CAT_PER WITH (NOLOCK)"
            sConsulta = sConsulta & " INNER JOIN CATN4 WITH (NOLOCK) ON CAT_PER.CAT4=CATN4.ID"
            sConsulta = sConsulta & " INNER JOIN CATN1 WITH (NOLOCK) ON CATN1.ID=CATN4.CAT1"
            sConsulta = sConsulta & " INNER JOIN CATN2 WITH (NOLOCK) ON CATN2.ID=CATN4.CAT2"
            sConsulta = sConsulta & " INNER JOIN CATN3 WITH (NOLOCK) ON CATN3.ID=CATN4.CAT3"
            sConsulta = sConsulta & " WHERE PER='" & DblQuote(PersonaAprov) & "'"
            If CaracteresInicialesCod <> "" Then
                 sConsulta = sConsulta & " AND CATN4.COD='" & DblQuote(CaracteresInicialesCod) & "'"
            End If
            If CaracteresInicialesDen <> "" Then
                sConsulta = sConsulta & " AND CATN4.DEN='" & DblQuote(CaracteresInicialesDen) & "'"
            End If
        End If
    Else
        If PersonaAprov = "" Then
            sConsulta = "SELECT CATN4.ID,CATN4.COD,CATN4.DEN,CATN1.COD AS CAT1, CATN2.COD AS CAT2, CATN3.COD AS CAT3 FROM CATN4 WITH (NOLOCK)"
            sConsulta = sConsulta & " INNER JOIN CATN1 WITH (NOLOCK) ON CATN1.ID=CATN4.CAT1"
            sConsulta = sConsulta & " INNER JOIN CATN2 WITH (NOLOCK) ON CATN2.ID=CATN4.CAT2"
            sConsulta = sConsulta & " INNER JOIN CATN3 WITH (NOLOCK) ON CATN3.ID=CATN4.CAT3 WHERE 1=1"
            If CaracteresInicialesCod <> "" Then
                 sConsulta = sConsulta & " AND CATN4.COD LIKE '" & DblQuote(CaracteresInicialesCod) & "%'"
            End If
            If CaracteresInicialesDen <> "" Then
                sConsulta = sConsulta & " AND CATN4.DEN LIKE '" & DblQuote(CaracteresInicialesDen) & "%'"
            End If
        Else
            sConsulta = "SELECT distinct CAT_PER.CAT4,CATN4.COD,CATN4.DEN,CATN1.COD AS CAT1, CATN2.COD AS CAT2, CATN3.COD AS CAT3 FROM CAT_PER WITH (NOLOCK)"
            sConsulta = sConsulta & " INNER JOIN CATN4 WITH (NOLOCK) ON CAT_PER.CAT4=CATN4.ID"
            sConsulta = sConsulta & " INNER JOIN CATN1 WITH (NOLOCK) ON CATN1.ID=CATN4.CAT1"
            sConsulta = sConsulta & " INNER JOIN CATN2 WITH (NOLOCK) ON CATN2.ID=CATN4.CAT2"
            sConsulta = sConsulta & " INNER JOIN CATN3 WITH (NOLOCK) ON CATN3.ID=CATN4.CAT3"
            sConsulta = sConsulta & " WHERE PER='" & DblQuote(PersonaAprov) & "'"
            If CaracteresInicialesCod <> "" Then
                 sConsulta = sConsulta & " AND CATN4.COD LIKE '" & DblQuote(CaracteresInicialesCod) & "%'"
            End If
            If CaracteresInicialesDen <> "" Then
                sConsulta = sConsulta & " AND CATN4.DEN LIKE '" & DblQuote(CaracteresInicialesDen) & "%'"
            End If
        End If
    End If
    
    If Not VerBajas Then
        sConsulta = sConsulta & " AND CATN4.BAJALOG<2"
    End If
    If OrdenadosPorDen Then
        sConsulta = sConsulta & " ORDER BY CAT1,CAT2,CAT3,CATN4.DEN"
    Else
        sConsulta = sConsulta & " ORDER BY CAT1,CAT2,CAT3,CATN4.COD"
    End If

    adoRecordset.Open sConsulta, mvarConexion.ADOCon, adOpenKeyset, adLockOptimistic
    If adoRecordset.eof Then
        adoRecordset.Close
        Set adoRecordset = Nothing
        Exit Function
    Else
        Set adoRecordset.ActiveConnection = Nothing
        Set DevolverTodasLasCategorias = adoRecordset
    End If
    
End Function

Public Property Get eof() As Boolean
    eof = mvarEOF
End Property
Friend Property Let eof(ByVal b As Boolean)
    mvarEOF = b
End Property

Public Property Get Item(vntIndexKey As Variant) As CCategoriaN4
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
     Set NewEnum = mCol.[_NewEnum]
End Property

Public Sub Remove(vntIndexKey As Variant)
    mCol.Remove vntIndexKey
End Sub

Private Sub Class_Initialize()
    Set mCol = New Collection
End Sub

Private Sub Class_Terminate()
    'destroys collection when this class is terminated
    Set mCol = Nothing
    Set mvarConexion = Nothing
End Sub
