VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CTipoPedidoCat"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CTipoPedidoCat **********************************


Option Explicit

Implements IBaseDatos

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

Private m_lID As Long
Private m_lTipoPedidoId As Long
Private m_lCATN As Long
Private m_vNivel As Variant
Private m_vFecAct As Variant
Private m_adores As adodb.Recordset
Private m_oConexion As CConexion 'local copy




Public Property Get Id() As Long
    Id = m_lID
End Property

Public Property Let Id(ByVal Ind As Long)
    m_lID = Ind
End Property

Public Property Let TipoPedidoId(ByVal vData As Long)
    m_lTipoPedidoId = vData
End Property

Public Property Get TipoPedidoId() As Long
    TipoPedidoId = m_lTipoPedidoId
End Property

Public Property Let CATN(ByVal vData As Long)
    m_lCATN = vData
End Property

Public Property Get CATN() As Long
    CATN = m_lCATN
End Property

Public Property Get Nivel() As Variant
    Nivel = m_vNivel
End Property

Public Property Let Nivel(ByVal Data As Variant)
    m_vNivel = Data
End Property

Public Property Get FecAct() As Variant
    FecAct = m_vFecAct
End Property

Public Property Let FecAct(ByVal Data As Variant)
    m_vFecAct = Data
End Property

Friend Property Set Conexion(ByVal vData As CConexion)
    Set m_oConexion = vData
End Property

Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property


Private Sub Class_Terminate()
    Set m_oConexion = Nothing
End Sub


'3328
Private Function IBaseDatos_AnyadirABaseDatos() As TipoErrorSummit
Dim sConsulta As String
Dim TESError As TipoErrorSummit
Dim rs As New adodb.Recordset
Dim btrans As Boolean


TESError.NumError = TESnoerror

'********* Precondicion *******************
If m_oConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CTipoPedidoCat.AnyadirABAseDatos", "No se ha establecido la conexion"
End If
' *****************************************

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------


If g_oErrores.fg_bProgramando Then On Error GoTo error_cls:
    
    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
    btrans = True
    
    sConsulta = "INSERT INTO TIPOPEDIDO_CATN (TIPOPEDIDO_ID,CATN,NIVEL) VALUES ('" & DblQuote(m_lTipoPedidoId) & "','" & DblQuote(m_lCATN) & "','" & DblQuote(m_vNivel) & "')"
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo error_cls
    sConsulta = "SELECT ID,FECACT FROM TIPOPEDIDO_CATN WITH (NOLOCK) WHERE TIPOPEDIDO_ID='" & DblQuote(m_lTipoPedidoId) & "' AND CATN='" & DblQuote(m_lCATN) & "' AND NIVEL ='" & DblQuote(m_vNivel) & "'"
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    
    m_lID = rs.Fields(0).Value
    m_vFecAct = rs.Fields(1).Value
    
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    btrans = False
    
    rs.Close
    Set rs = Nothing
    
    IBaseDatos_AnyadirABaseDatos = TESError
    Exit Function
error_cls:

    IBaseDatos_AnyadirABaseDatos = basErrores.TratarError(m_oConexion.ADOCon.Errors)

    If btrans Then
        m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CTipoPedidoCat", "IBaseDatos_AnyadirABaseDatos", ERR, Erl)
      GoTo error_cls
      Exit Function
   End If

End Function




Private Function IBaseDatos_CambiarCodigo(CodigoNuevo As Variant) As TipoErrorSummit

End Function




Private Sub IBaseDatos_CancelarEdicion()

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not m_adores Is Nothing Then
        m_adores.Close
        Set m_adores = Nothing
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CTipoPedidoCat", "IBaseDatos_CancelarEdicion", ERR, Erl)
      Exit Sub
   End If
End Sub




Private Function IBaseDatos_ComprobarExistenciaEnBaseDatos() As Boolean
End Function




Private Function IBaseDatos_EliminarDeBaseDatos() As TipoErrorSummit
Dim sConsulta As String
Dim TESError As TipoErrorSummit
Dim btrans As Boolean
Dim rs As New adodb.Recordset
Dim fldLinea As adodb.Field
Dim arCampos(1 To 2) As Variant


TESError.NumError = TESnoerror

'******************* Precondicion *******************
If m_oConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CTipoPedidoCat.EliminarDeBaseDatos", "No se ha establecido la conexion"
End If
'*****************************************************

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

If g_oErrores.fg_bProgramando Then On Error GoTo error_cls:

    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
    btrans = True
        
    sConsulta = "DELETE FROM TIPOPEDIDO_CATN WHERE TIPOPEDIDO_ID=" & m_lTipoPedidoId & " AND CATN=" & m_lCATN & " AND NIVEL = " & m_vNivel & ""
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo error_cls
    
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    btrans = False

    IBaseDatos_EliminarDeBaseDatos = TESError
    Exit Function


error_cls:
    
    IBaseDatos_EliminarDeBaseDatos = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    
    If btrans Then
        m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CTipoPedidoCat", "IBaseDatos_EliminarDeBaseDatos", ERR, Erl)
      GoTo error_cls
      Exit Function
   End If

End Function




Private Function IBaseDatos_FinalizarEdicionEliminando() As TipoErrorSummit
End Function




Private Function IBaseDatos_FinalizarEdicionModificando() As TipoErrorSummit
End Function




Private Function IBaseDatos_IniciarEdicion(Optional ByVal Bloquear As Boolean, Optional ByVal UsuarioBloqueo As String) As TipoErrorSummit
End Function







