VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CWorkflows"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True

Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum

Private m_Col As Collection
Private m_oConexion As CConexion
Private m_bEOF As Boolean

Public Sub CargarWorkflowsCarpetaSolicitudYHuerfanos(ByVal lCSN1 As Long, ByVal lCSN2 As Long, ByVal lCSN3 As Long, ByVal lCSN4 As Long)
    Dim rs As New adodb.Recordset
    Dim sql As String
    Dim fldId As adodb.Field
    Dim fldDen As adodb.Field
    Dim fldFecAct As adodb.Field
    
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CWorkflows.CargarWorkflowsCarpetaSolicitud", "No se ha establecido la conexion"
        Exit Sub
    End If
    
    sql = CargarWorkflowsCarpetaSolicitud(lCSN1, lCSN2, lCSN3, lCSN4, , , , True)
    sql = sql & " UNION " & CargarWorkflowsHuerfanos(, , , True)
    sql = sql & " ORDER BY DEN"
        
    rs.Open sql, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    
    If rs.eof Then
        rs.Close
        Set rs = Nothing
        Set m_Col = Nothing
        Set m_Col = New Collection
        Exit Sub
        
    Else
        Set m_Col = Nothing
        Set m_Col = New Collection
        
        Set fldId = rs.Fields("ID")
        Set fldDen = rs.Fields("DEN")
        Set fldFecAct = rs.Fields("FECACT")
        
        While Not rs.eof
            Me.Add fldId.Value, fldDen.Value, fldFecAct.Value
            rs.MoveNext
        Wend
                
        rs.Close
        Set rs = Nothing
        Set fldId = Nothing
        Set fldDen = Nothing
        Set fldFecAct = Nothing
    End If
End Sub

Public Function Eliminar_Copia(ByVal IdFlujo As Long, ByVal sCampo As String) As TipoErrorSummit
Dim adoComm As adodb.Command
Dim adoParam As adodb.Parameter
Dim TESnoerror As TipoErrorSummit
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
Eliminar_Copia = TESnoerror
m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
m_oConexion.ADOCon.CommandTimeout = 0
Set adoComm = New adodb.Command
adoComm.CommandType = adCmdStoredProc
adoComm.CommandText = "FSPM_WORKFLOW_ELIMINAR_COPIA"
Set adoComm.ActiveConnection = m_oConexion.ADOCon
Set adoParam = adoComm.CreateParameter("WORKFLOW_ORIGEN", adInteger, adParamInput, , IdFlujo)
adoComm.Parameters.Append adoParam
Set adoParam = adoComm.CreateParameter("CAMPO", adVarChar, adParamInput, 20, sCampo)
adoComm.Parameters.Append adoParam
adoComm.Execute
If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR
m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
    m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
    If ERR.Number <> 0 Then
       Eliminar_Copia = basErrores.TratarError(m_oConexion.ADOCon.Errors)
       Call g_oErrores.TratarError("M�dulo de clase", "CWorkflows", "Eliminar_Copia", ERR, Erl)
       Exit Function
    End If
    
End Function

Public Function Crear_Copia(ByVal IdFlujo As Long) As TipoErrorSummit
Dim adoComm As adodb.Command
Dim adoParam As adodb.Parameter
Dim TESnoerror As TipoErrorSummit
On Error GoTo Error_Cls
    Crear_Copia = TESnoerror
    m_oConexion.BEGINTRANSACTION
    Set adoComm = New adodb.Command
    adoComm.CommandType = adCmdStoredProc
    adoComm.CommandText = "FSPM_WORKFLOW_CREAR_COPIA"
    Set adoComm.ActiveConnection = m_oConexion.ADOCon
    Set adoParam = adoComm.CreateParameter("WORKFLOW_ORIGEN", adInteger, adParamInput, , IdFlujo)
    adoComm.Parameters.Append adoParam
    adoComm.Execute
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    m_oConexion.COMMITTRANSACTION
Exit Function
Error_Cls:
    If m_oConexion.ADOCon.Errors.Count > 0 Then
        Crear_Copia = basErrores.TratarError(m_oConexion.ADOCon.Errors)
        m_oConexion.ROLLBACKTRANSACTION
    Else
        Call g_oErrores.TratarError("M�dulo de clase", "CWorkflows", "Crear_Copia", ERR, Erl)
    End If
End Function


Public Function Restaurar_desde_Copia(ByVal IdFlujo As Long) As TipoErrorSummit
Dim adoComm As adodb.Command
Dim adoParam As adodb.Parameter
Dim TESnoerror As TipoErrorSummit
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
Restaurar_desde_Copia = TESnoerror
Set adoComm = New adodb.Command
adoComm.CommandType = adCmdStoredProc
adoComm.CommandText = "FSPM_WORKFLOW_ELIMINAR"
Set adoComm.ActiveConnection = m_oConexion.ADOCon
Set adoParam = adoComm.CreateParameter("WORKFLOW", adInteger, adParamInput, , IdFlujo)
adoComm.Parameters.Append adoParam
adoComm.Execute
If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR
Set adoComm = Nothing

Set adoComm = New adodb.Command
adoComm.CommandType = adCmdStoredProc
adoComm.CommandText = "FSPM_WORKFLOW_CREAR_DESDE_COPIA"
Set adoComm.ActiveConnection = m_oConexion.ADOCon
Set adoParam = adoComm.CreateParameter("WORKFLOW_ORIGEN", adInteger, adParamInput, , IdFlujo)
adoComm.Parameters.Append adoParam
adoComm.Execute
If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR
Set adoComm = Nothing

m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:

If m_oConexion.ADOCon.Errors.Count > 0 Then
    Restaurar_desde_Copia = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    m_oConexion.ADOCon.Execute "  IF @@TRANCOUNT>0 ROLLBACK TRANSACTION"
Else
    Call g_oErrores.TratarError("M�dulo de clase", "CWorkflows", "Restaurar_desde_Copia", ERR, Erl)
End If
    
End Function


Public Property Get eof() As Boolean
    eof = m_bEOF
End Property
Friend Property Let eof(ByVal b As Boolean)
    m_bEOF = b
End Property

Public Property Get Item(vntIndexKey As Variant) As CWorkflow
On Error GoTo NoSeEncuentra:

    Set Item = m_Col(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Friend Property Set Conexion(ByVal con As CConexion)
Set m_oConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = m_oConexion
End Property

Public Property Get Count() As Long

If m_Col Is Nothing Then
    Count = 0
Else
     Count = m_Col.Count
End If


End Property

Public Function Add(ByVal lId As Long, ByVal sDen As String, Optional ByVal dtFecha As Variant, Optional ByVal vIndice As Variant) As CWorkflow
    
    'create a new object
    Dim objnewmember As CWorkflow
    
    Set objnewmember = New CWorkflow
    With objnewmember
        Set .Conexion = m_oConexion
    
        .Id = lId
        .Den = sDen
        .FecAct = dtFecha
    
        If Not IsMissing(vIndice) And Not IsNull(vIndice) Then
            .Indice = vIndice
            m_Col.Add objnewmember, CStr(vIndice)
        Else
            m_Col.Add objnewmember, CStr(lId)
        End If
    
    End With
    
    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing


End Function

Public Sub Remove(vntIndexKey As Variant)
On Error GoTo ERROR

    m_Col.Remove vntIndexKey

ERROR:

End Sub


Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
     Set NewEnum = m_Col.[_NewEnum]
End Property


Public Function SacarDen(ByVal lId As Long, Optional ByVal bCopia As Boolean = False) As String
Dim rs As adodb.Recordset
Dim sTabla As String
Dim sConsulta As String
If m_oConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CWorkflows.CargarTodosWorkflows", "No se ha establecido la conexion"
    Exit Function
End If

If Not bCopia Then
    sTabla = "WORKFLOW"
Else
    sTabla = "PM_COPIA_WORKFLOW"
End If
sConsulta = "SELECT DEN FROM " & sTabla & " WITH (NOLOCK) WHERE ID = " & lId
Set rs = New adodb.Recordset
rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
If Not rs.eof Then
    SacarDen = NullToStr(rs("DEN").Value)
End If
rs.Close
Set rs = Nothing

End Function

''' <summary>
''' Marca en la BBDD en la tabla WORKFLOW el campo PENDIENTE_COPIA = 1 para que se sepa que ha habido cambios relacionados con el Flujo
''' Si por un casual ha habido cambios en datos relacionados y se cerrara la aplicaci�n o la ventana, si estubiera el campo con valor 1
''' se lanzaria un mensaje al intentar acceder al flujo de nuevo para saber si aplicar o descartar los cambios.
''' Siempre que haya algun cambio se marcar� y solo se liberar� el campo cuando se acepten o cancelen los cambios de todo el Flujo
''' </summary>
''' <param name="Id">Id del WORKFLOW</param name>
''' <param name="iMarca">1=Activado con cambios en datos relacionados del flujo; 0=Desactivar, cancelaci�n o guardado del flujo</param name>
''' <returns>Si ha habido errores a la hora de actualizar el campo PENDIENTE_COPIA</returns>
''' <remarks>Llamada desde: frmFlujos.frm
Public Function TratarPendienteCopia(ByVal Id As Long, ByVal iMarca As Integer) As TipoErrorSummit
Dim TESError As TipoErrorSummit
Dim sConsulta As String
If m_oConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CWorkflows.CargarTodosWorkflows", "No se ha establecido la conexion"
    Exit Function
End If
On Error GoTo ERROR
sConsulta = "UPDATE WORKFLOW SET PENDIENTE_COPIA = " & iMarca & " WHERE ID = " & Id
m_oConexion.ADOCon.Execute sConsulta
TESError.NumError = TESnoerror
If m_oConexion.ADOCon.Errors.Count > 0 Then
    GoTo ERROR
End If

TratarPendienteCopia = TESError
Exit Function

ERROR:
    TESError = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    TratarPendienteCopia = TESError
End Function

''' <summary>
''' Marca en la BBDD en la tabla WORKFLOW el campo PENDIENTE_COPIA = 1 para que se sepa que ha habido cambios relacionados con el Flujo
''' Si por un casual ha habido cambios en datos relacionados y se cerrara la aplicaci�n o la ventana, si estubiera el campo con valor 1
''' se lanzaria un mensaje al intentar acceder al flujo de nuevo para saber si aplicar o descartar los cambios.
''' Siempre que haya algun cambio se marcar� y solo se liberar� el campo cuando se acepten o cancelen los cambios de todo el Flujo
''' </summary>
''' <param name="Id">Id del WORKFLOW</param name>
''' <param name="iMarca">1=Activado con cambios en datos relacionados del flujo; 0=Desactivar, cancelaci�n o guardado del flujo</param name>
''' <returns>Si ha habido errores a la hora de actualizar el campo PENDIENTE_COPIA</returns>
''' <remarks>Llamada desde: frmFlujos.frm
Public Function EstaPendienteCopia(ByVal Id As Long) As Boolean
Dim TESError As TipoErrorSummit
Dim rs As adodb.Recordset
Dim sConsulta As String
If m_oConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CWorkflows.EstaPendienteCopia", "No se ha establecido la conexion"
    Exit Function
End If
On Error GoTo ERROR:
EstaPendienteCopia = False
Set rs = New adodb.Recordset
sConsulta = "SELECT PENDIENTE_COPIA FROM WORKFLOW WHERE PENDIENTE_COPIA = 1 AND ID = " & Id
rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
TESError.NumError = TESnoerror

If m_oConexion.ADOCon.Errors.Count > 0 Then
    GoTo ERROR
ElseIf rs.RecordCount > 0 Then
    EstaPendienteCopia = True
End If
Set rs = Nothing
Exit Function

ERROR:
    TESError = basErrores.TratarError(m_oConexion.ADOCon.Errors)
End Function


Private Sub Class_Initialize()
    Set m_Col = New Collection
End Sub


Private Sub Class_Terminate()
    'Proceroys collection when this class is terminated
    On Error Resume Next
    Set m_Col = Nothing
    Set m_oConexion = Nothing
End Sub


Public Function DevolverWorkflows(Optional ByVal lId As Variant, Optional ByVal sDen As Variant) As adodb.Recordset
Dim oadorecordset As adodb.Recordset
Dim oADOConnection As adodb.Connection
Dim sql As String
Dim stm As adodb.Stream
Dim rs As adodb.Recordset

    Set oADOConnection = New adodb.Connection
    oADOConnection.Open "Provider=MSDataShape.1;Persist Security Info=True;Data Source=" & basParametros.ServidorGS & ";User ID=" & LICDBLogin & ";Password=" & LICDBContra & ";Initial Catalog=" & basParametros.BaseDatosGS & ";Data Provider=SQLOLEDB.1"
    oADOConnection.CursorLocation = adUseClient
    oADOConnection.CommandTimeout = 120
    
    If IsMissing(lId) Then lId = Null
    If IsMissing(sDen) Then sDen = Null
        
    sql = " SHAPE {SELECT ID, DEN FROM WORKFLOW WHERE 1=1 "
    If Not IsNull(lId) Then
        sql = sql & " AND ID=" & lId
    End If
    If Not IsNull(sDen) Then
        sql = sql & " AND DEN='" & DblQuote(sDen) & "'"
    End If
    sql = sql & "}  AS W " _
        & " APPEND ((SHAPE {SELECT PASO.ID, ORDEN, WORKFLOW, BLOQUE_SEGURIDAD, BS.DEN,  LIMITE FROM PASO INNER JOIN BLOQUE_SEGURIDAD BS ON PASO.BLOQUE_SEGURIDAD = BS.ID" _
        & " ORDER BY ORDEN}  AS Command2 " _
        & " APPEND ({SELECT PASO_NOTIF.ID, PASO_NOTIF.PASO, PASO_NOTIF.PER, PER.NOM, PER.APE, PER.EMAIL " _
        & " FROM PASO_NOTIF INNER JOIN PER ON PASO_NOTIF.PER=PER.COD ORDER BY PASO_NOTIF.ID}  AS N " _
        & " RELATE 'ID' TO 'PASO') AS N) AS Command2 RELATE 'ID' TO 'WORKFLOW') AS Command2 "
                
    Set oadorecordset = New adodb.Recordset
    oadorecordset.CursorLocation = adUseClient
    oadorecordset.Open sql, oADOConnection, adOpenForwardOnly, adLockReadOnly
    oadorecordset.ActiveConnection = Nothing
    oADOConnection.Close
        
    'Paso el recordset a stream y lo vuelvo a abrir desde ah�.Se hace as� porque sino no se desconecta:
    Set stm = New adodb.Stream
    
    oadorecordset.Save stm, adPersistXML
    
    Set rs = New adodb.Recordset
    
    rs.Open stm
    rs.ActiveConnection = Nothing
    
    Set DevolverWorkflows = rs
    
    stm.Close
    Set stm = Nothing
    
End Function



Public Sub CargarTodosWorkflows(Optional ByVal CaracteresInicialesDen As String, Optional ByVal UsarIndice As Boolean, Optional ByVal CoincidenciaTotal As Boolean)
    Dim rs As New adodb.Recordset
    Dim sConsulta As String
    Dim fldId As adodb.Field
    Dim fldDen As adodb.Field
    Dim fldFecAct As adodb.Field
    Dim lIndice As Long
    
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CWorkflows.CargarTodosWorkflows", "No se ha establecido la conexion"
        Exit Sub
    End If
    
    sConsulta = "SELECT * FROM WORKFLOW WITH (NOLOCK)"
    
    If Not (CaracteresInicialesDen = "") Then
        If CoincidenciaTotal = False Then
            sConsulta = sConsulta & " WHERE DEN >='" & DblQuote(CaracteresInicialesDen) & "'"
        Else
            sConsulta = sConsulta & " WHERE DEN ='" & DblQuote(CaracteresInicialesDen) & "'"
        End If
    End If
        
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
          
    If rs.eof Then
        rs.Close
        Set rs = Nothing
        Set m_Col = Nothing
        Set m_Col = New Collection
        Exit Sub
        
    Else
        Set m_Col = Nothing
        Set m_Col = New Collection
        
        Set fldId = rs.Fields("ID")
        Set fldDen = rs.Fields("DEN")
        Set fldFecAct = rs.Fields("FECACT")
        
        If UsarIndice Then
            lIndice = 0
            While Not rs.eof
                Me.Add fldId.Value, fldDen.Value, fldFecAct.Value, lIndice
                rs.MoveNext
                lIndice = lIndice + 1
            Wend
            
        Else
            While Not rs.eof
                Me.Add fldId.Value, fldDen.Value, fldFecAct.Value
                rs.MoveNext
            Wend
        End If
        
        rs.Close
        Set rs = Nothing
        Set fldId = Nothing
        Set fldDen = Nothing
        Set fldFecAct = Nothing
    End If
    
End Sub

Public Function CargarWorkflowsCarpetaSolicitud(ByVal lCSN1 As Long, ByVal lCSN2 As Long, ByVal lCSN3 As Long, ByVal lCSN4 As Long, Optional ByVal CaracteresInicialesDen As String, Optional ByVal UsarIndice As Boolean, Optional ByVal CoincidenciaTotal As Boolean, Optional ByVal pbSoloSQL As Boolean = False) As String
    Dim rs As New adodb.Recordset
    Dim sConsulta As String
    Dim fldId As adodb.Field
    Dim fldDen As adodb.Field
    Dim fldFecAct As adodb.Field
    Dim lIndice As Long
    
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CWorkflows.CargarWorkflowsCarpetaSolicitud", "No se ha establecido la conexion"
        Exit Function
    End If
    
    sConsulta = "SELECT * FROM WORKFLOW W WITH (NOLOCK) "
    sConsulta = sConsulta & "WHERE W.ID IN "
    sConsulta = sConsulta & "(SELECT WORKFLOW FROM SOLICITUD WITH (NOLOCK) WHERE "
    If lCSN1 > 0 Then
        sConsulta = sConsulta & "CSN1 = " & lCSN1 & " AND CSN2 IS NULL AND CSN3 IS NULL AND CSN4 IS NULL)"
    ElseIf lCSN2 > 0 Then
        sConsulta = sConsulta & "CSN1 IS NULL AND CSN2 = " & lCSN2 & " AND CSN3 IS NULL AND CSN4 IS NULL)"
    ElseIf lCSN3 > 0 Then
        sConsulta = sConsulta & "CSN1 IS NULL AND CSN2 IS NULL AND CSN3 = " & lCSN3 & " AND CSN4 IS NULL)"
    ElseIf lCSN4 > 0 Then
        sConsulta = sConsulta & "CSN1 IS NULL AND CSN2 IS NULL AND CSN3 IS NULL AND CSN4 = " & lCSN4 & ")"
    Else 'Nunca deber�a entrar en esta condici�n
        sConsulta = sConsulta & "CSN1 IS NULL AND CSN2 IS NULL AND CSN3 IS NULL AND CSN4 IS NULL)"
    End If
    
    If Not (CaracteresInicialesDen = "") Then
        If CoincidenciaTotal = False Then
            sConsulta = sConsulta & " AND W.DEN >='" & DblQuote(CaracteresInicialesDen) & "'"
        Else
            sConsulta = sConsulta & " AND W.DEN ='" & DblQuote(CaracteresInicialesDen) & "'"
        End If
    End If
        
    If pbSoloSQL Then
        CargarWorkflowsCarpetaSolicitud = sConsulta
        Exit Function
    End If
        
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
                   
    If rs.eof Then
        rs.Close
        Set rs = Nothing
        Set m_Col = Nothing
        Set m_Col = New Collection
        Exit Function
        
    Else
        Set m_Col = Nothing
        Set m_Col = New Collection
        
        Set fldId = rs.Fields("ID")
        Set fldDen = rs.Fields("DEN")
        Set fldFecAct = rs.Fields("FECACT")
        
        If UsarIndice Then
            lIndice = 0
            While Not rs.eof
                Me.Add fldId.Value, fldDen.Value, fldFecAct.Value, lIndice
                rs.MoveNext
                lIndice = lIndice + 1
            Wend
            
        Else
            While Not rs.eof
                Me.Add fldId.Value, fldDen.Value, fldFecAct.Value
                rs.MoveNext
            Wend
        End If
        
        rs.Close
        Set rs = Nothing
        Set fldId = Nothing
        Set fldDen = Nothing
        Set fldFecAct = Nothing
    End If
    
End Function

Public Function CargarWorkflowsHuerfanos(Optional ByVal CaracteresInicialesDen As String, Optional ByVal UsarIndice As Boolean, Optional ByVal CoincidenciaTotal As Boolean, Optional ByVal pbSoloSQL As Boolean = False) As String
    Dim rs As New adodb.Recordset
    Dim sConsulta As String
    Dim fldId As adodb.Field
    Dim fldDen As adodb.Field
    Dim fldFecAct As adodb.Field
    Dim lIndice As Long
    
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CWorkflows.CargarWorkflowsHuerfanos", "No se ha establecido la conexion"
        Exit Function
    End If
    
    sConsulta = "SELECT * FROM WORKFLOW W WITH (NOLOCK) "
    sConsulta = sConsulta & "WHERE W.ID NOT IN "
    sConsulta = sConsulta & "(SELECT DISTINCT(ISNULL(WORKFLOW,0)) FROM SOLICITUD WITH (NOLOCK))"
    
    If Not (CaracteresInicialesDen = "") Then
        If CoincidenciaTotal = False Then
            sConsulta = sConsulta & " AND W.DEN >='" & DblQuote(CaracteresInicialesDen) & "'"
        Else
            sConsulta = sConsulta & " AND W.DEN ='" & DblQuote(CaracteresInicialesDen) & "'"
        End If
    End If
        
    If pbSoloSQL Then
        CargarWorkflowsHuerfanos = sConsulta
        Exit Function
    End If
        
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
          
    If rs.eof Then
        rs.Close
        Set rs = Nothing
        Set m_Col = Nothing
        Set m_Col = New Collection
        Exit Function
        
    Else
        Set m_Col = Nothing
        Set m_Col = New Collection
        
        Set fldId = rs.Fields("ID")
        Set fldDen = rs.Fields("DEN")
        Set fldFecAct = rs.Fields("FECACT")
        
        If UsarIndice Then
            lIndice = 0
            While Not rs.eof
                Me.Add fldId.Value, fldDen.Value, fldFecAct.Value, lIndice
                rs.MoveNext
                lIndice = lIndice + 1
            Wend
            
        Else
            While Not rs.eof
                Me.Add fldId.Value, fldDen.Value, fldFecAct.Value
                rs.MoveNext
            Wend
        End If
        
        rs.Close
        Set rs = Nothing
        Set fldId = Nothing
        Set fldDen = Nothing
        Set fldFecAct = Nothing
    End If
    
End Function

