VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CVarPonderacion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

Private m_oConexion As CConexion
Private m_lVarID As Long
Private m_iVarNivel As Integer
Private m_oCampo As CFormItem
Private m_oGrupo As CFormGrupo
Private m_lOrden As Long
Private m_vFecact As Variant
Private m_vIndice As Variant
Private m_lFormulario As Long 'Para saber a que formulario pertenecen los campos
'Para las puntuaciones de variables
Private m_vCodPond As Variant
Private m_udtTipoPond As TAtributoPonderacion
Private m_vFormulaPond As Variant
Private m_vPondSi As Variant
Private m_vPondNo As Variant
Private m_oListaPond As CValoresPond

Public Property Get Formulario() As Long
    Formulario = m_lFormulario
End Property
Public Property Let Formulario(ByVal Data As Long)
    m_lFormulario = Data
End Property

Public Property Get Orden() As Long
    Orden = m_lOrden
End Property
Public Property Let Orden(ByVal Data As Long)
    m_lOrden = Data
End Property

Public Property Get VarID() As Long
    VarID = m_lVarID
End Property
Public Property Let VarID(ByVal Data As Long)
    m_lVarID = Data
End Property

Public Property Get VarNivel() As Integer
    VarNivel = m_iVarNivel
End Property
Public Property Let VarNivel(ByVal Data As Integer)
    m_iVarNivel = Data
End Property

'*** Para las puntuaciones de variables  ***
Public Property Get PondCod() As Variant
    PondCod = m_vCodPond
End Property
Public Property Let PondCod(ByVal Data As Variant)
    m_vCodPond = Data
End Property
Public Property Get PondTipo() As TAtributoPonderacion
    PondTipo = m_udtTipoPond
End Property
Public Property Let PondTipo(ByVal Data As TAtributoPonderacion)
    m_udtTipoPond = Data
End Property
Public Property Get PondLista() As CValoresPond
    Set PondLista = m_oListaPond
End Property
Public Property Set PondLista(ByVal oLista As CValoresPond)
    Set m_oListaPond = oLista
End Property
Public Property Get PondFormula() As Variant
    PondFormula = m_vFormulaPond
End Property
Public Property Let PondFormula(ByVal Data As Variant)
    m_vFormulaPond = Data
End Property
Public Property Get PondSi() As Variant
    PondSi = m_vPondSi
End Property
Public Property Let PondSi(ByVal Data As Variant)
    m_vPondSi = Data
End Property
Public Property Get PondNo() As Variant
    PondNo = m_vPondNo
End Property
Public Property Let PondNo(ByVal Data As Variant)
    m_vPondNo = Data
End Property
'*** FIN Para las puntuaciones de variables  ***

Public Property Get FecAct() As Variant
    FecAct = m_vFecact
End Property
Public Property Let FecAct(ByVal Data As Variant)
    m_vFecact = Data
End Property
Public Property Let Indice(ByVal varIndice As Variant)
    m_vIndice = varIndice
End Property
Public Property Get Indice() As Variant
    Indice = m_vIndice
End Property

Public Property Get Campo() As CFormItem
    Set Campo = m_oCampo
End Property
Public Property Set Campo(ByVal oGrupo As CFormItem)
    Set m_oCampo = oGrupo
End Property

Public Property Get Grupo() As CFormGrupo
    Set Grupo = m_oGrupo
End Property
Public Property Set Grupo(ByVal oGrupo As CFormGrupo)
    Set m_oGrupo = oGrupo
End Property

Friend Property Set Conexion(ByVal vData As CConexion)

    ''' * Objetivo: Dar valor a la conexion de la Destino
    ''' * Recibe: Conexion
    ''' * Devuelve: Nada

    Set m_oConexion = vData
    
End Property
Friend Property Get Conexion() As CConexion

    ''' * Objetivo: Devolver la conexion de la Destino
    ''' * Recibe: Nada
    ''' * Devuelve: Conexion

    Set Conexion = m_oConexion
    
End Property

Private Sub Class_Terminate()
    Set m_oGrupo = Nothing
    Set m_oCampo = Nothing
    Set m_oListaPond = Nothing
    Set m_oConexion = Nothing
End Sub



