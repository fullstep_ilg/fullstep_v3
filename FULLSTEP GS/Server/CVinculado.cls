VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CVinculado"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

Private m_oConexion As CConexion

Private m_vIndice As Variant

Private m_vId As Variant
Private m_lSolicitud As Long
Private m_lDesgloseVinculado As Long
Private m_vDesgloseOrigen As Variant
Private m_vFecAct As Variant

Private m_vCampos As Variant

Private m_sDesgloseOrigen As String
Private m_sSolicitud As String

''' <summary>
''' Obtener el DESGLOSE_VINCULADO.ID
''' </summary>
''' <remarks>Llamada desde: frmFormularioVincularDesglose; Tiempo m�ximo: 0</remarks>
Public Property Get ID() As Variant
    ID = m_vId
End Property
''' <summary>
''' Establecer el DESGLOSE_VINCULADO.ID
''' </summary>
''' <remarks>Llamada desde: frmFormularioVincularDesglose; Tiempo m�ximo: 0</remarks>
Public Property Let ID(ByVal Data As Variant)
    m_vId = Data
End Property

''' <summary>
''' Obtener la Solicitud
''' </summary>
''' <remarks>Llamada desde: frmFormularioVincularDesglose; Tiempo m�ximo: 0</remarks>
Public Property Get Solicitud() As Long
    Solicitud = m_lSolicitud
End Property
''' <summary>
''' Establecer la Solicitud
''' </summary>
''' <remarks>Llamada desde: frmFormularioVincularDesglose; Tiempo m�ximo: 0</remarks>
Public Property Let Solicitud(ByVal Data As Long)
    m_lSolicitud = Data
End Property
''' <summary>
''' Obtener el Id de form_campo de Desglose Vinculado
''' </summary>
''' <remarks>Llamada desde: frmFormularioVincularDesglose; Tiempo m�ximo: 0</remarks>
Public Property Get DesgloseVinculado() As Long
    DesgloseVinculado = m_lDesgloseVinculado
End Property
''' <summary>
''' Establecer el Id de form_campo de Desglose Vinculado
''' </summary>
''' <remarks>Llamada desde: frmFormularioVincularDesglose; Tiempo m�ximo: 0</remarks>
Public Property Let DesgloseVinculado(ByVal Data As Long)
    m_lDesgloseVinculado = Data
End Property

''' <summary>
''' Obtener el Id de form_campo de Desglose origen, de haberlo
''' </summary>
''' <remarks>Llamada desde: frmFormularioVincularDesglose; Tiempo m�ximo: 0</remarks>
Public Property Get DesgloseOrigen() As Variant
    DesgloseOrigen = m_vDesgloseOrigen
End Property
''' <summary>
''' Establecer el Id de form_campo de Desglose origen, de haberlo
''' </summary>
''' <remarks>Llamada desde: frmFormularioVincularDesglose; Tiempo m�ximo: 0</remarks>
Public Property Let DesgloseOrigen(ByVal Data As Variant)
    m_vDesgloseOrigen = Data
End Property
''' <summary>
''' Obtener la Fecha ultima grabaci�n
''' </summary>
''' <remarks>Llamada desde: frmFormularioVincularDesglose; Tiempo m�ximo: 0</remarks>
Public Property Get FecAct() As Variant
    FecAct = m_vFecAct
End Property
''' <summary>
''' Establecer la Fecha ultima grabaci�n
''' </summary>
''' <remarks>Llamada desde: frmFormularioVincularDesglose; Tiempo m�ximo: 0</remarks>
Public Property Let FecAct(ByVal Data As Variant)
    m_vFecAct = Data
End Property

''' <summary>
''' Obtener la Descr del Desglose origen
''' </summary>
''' <remarks>Llamada desde: frmFormularioVincularDesglose; Tiempo m�ximo: 0</remarks>
Public Property Get DescrDesgloseOrigen() As String
    DescrDesgloseOrigen = m_sDesgloseOrigen
End Property
''' <summary>
''' Establecer la Descr del Desglose origen
''' </summary>
''' <remarks>Llamada desde: frmFormularioVincularDesglose; Tiempo m�ximo: 0</remarks>
Public Property Let DescrDesgloseOrigen(ByVal Data As String)
    m_sDesgloseOrigen = Data
End Property

''' <summary>
''' Obtener la Descr de Solicitud
''' </summary>
''' <remarks>Llamada desde: frmFormularioVincularDesglose; Tiempo m�ximo: 0</remarks>
Public Property Get DescrSolicitud() As String
    DescrSolicitud = m_sSolicitud
End Property
''' <summary>
''' Establecer la Descr de Solicitud
''' </summary>
''' <remarks>Llamada desde: frmFormularioVincularDesglose; Tiempo m�ximo: 0</remarks>
Public Property Let DescrSolicitud(ByVal Data As String)
    m_sSolicitud = Data
End Property

''' <summary>
''' Obtener el objeto campos del Desglose Vinculado
''' </summary>
''' <remarks>Llamada desde: frmFormularioVincularDesglose; Tiempo m�ximo: 0</remarks>
Public Property Get Campos() As Variant
    Campos = m_vCampos
End Property
''' <summary>
''' Establecer el objeto campos del Desglose Vinculado
''' </summary>
''' <remarks>Llamada desde: frmFormularioVincularDesglose; Tiempo m�ximo: 0</remarks>
Public Property Let Campos(ByVal Data As Variant)
    m_vCampos = Data
End Property
''' <summary>
''' Establecer el indice
''' </summary>
''' <remarks>Llamada desde: frmFormularioVincularDesglose; Tiempo m�ximo: 0</remarks>
Public Property Let Indice(ByVal varIndice As Variant)
    m_vIndice = varIndice
End Property
''' <summary>
''' Obtener el indice
''' </summary>
''' <remarks>Llamada desde: frmFormularioVincularDesglose; Tiempo m�ximo: 0</remarks>
Public Property Get Indice() As Variant
    Indice = m_vIndice
End Property

Friend Property Set Conexion(ByVal vData As CConexion)

    ''' * Objetivo: Dar valor a la conexion de la Destino
    ''' * Recibe: Conexion
    ''' * Devuelve: Nada

    Set m_oConexion = vData
    
End Property
Friend Property Get Conexion() As CConexion

    ''' * Objetivo: Devolver la conexion de la Destino
    ''' * Recibe: Nada
    ''' * Devuelve: Conexion

    Set Conexion = m_oConexion
    
End Property
