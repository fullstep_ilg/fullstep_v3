VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CRecepciones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
Private mCol As Collection
Private mvarEOF As Boolean
Private mvarConexion As CConexion
Friend Property Set Conexion(ByVal con As CConexion)
Set mvarConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = mvarConexion
End Property


Public Property Get eof() As Boolean
    eof = mvarEOF
End Property
Friend Property Let eof(ByVal b As Boolean)
    mvarEOF = b
End Property

Public Property Get Item(vntIndexKey As Variant) As CRecepcion
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
     Set NewEnum = mCol.[_NewEnum]
End Property
Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
     Count = mCol.Count
End If


End Property

Public Sub Remove(vntIndexKey As Variant)
   
    mCol.Remove vntIndexKey
End Sub

''' <summary>
''' Crea y carga un objeto de la clase CRecepcion
''' </summary>
''' <param name="ID">ID de la recepci�n</param>
''' <param name="Pedido">Pedido</param>
''' <param name="Orden">Orden de entrega</param>
''' <param name="OrdenEstado">Estado de la Orden</param>
''' <param name="Fecha">Fecha de la recepci�n</param>
''' <param name="albaran">albaran de la recepci�n</param>
''' <param name="Correcto">recepci�n correcta/incorrecta</param>
''' <param name="coment">comentario  de la recepci�n</param>
''' <param name="FecAct">Fecha ultima actualizacion de la recepccion</param>
''' <param name="OrdenIncorrecta">correcta/incorrecta de la orden</param>
''' <param name="TipoOrden">Tipo de la orden</param>
''' <param name="varIndice">Indice</param>
''' <param name="Completada">si La ultima recepcion es la que cerr� el pedido</param>
''' <param name="Prove">Proveedor de la orden</param>
''' <param name="Receptor">Persona q hace la recepci�n (""->adm)</param>
''' <param name="BloqueoFactura">si hay o no bloqueo de albar�n para factutaci�n</param>
''' <param name="NumErp">Numero de recepcion del ERP </param>
''' <param name="FechaContable"> Fecha Contable </param>
''' <returns>Objeto de la clase CRecepcion</returns>
''' <remarks>Llamada desde: COrdenEntrega/DevolverTodasLasRecepciones ; Tiempo m�ximo: 0</remarks>
Public Function Add(ByVal ID As Long, ByVal Pedido As Long, ByVal Orden As Long, ByVal OrdenEstado As Long, ByVal Fecha As Date, _
ByVal albaran As String, ByVal Correcto As Boolean, ByVal coment As Variant, ByVal FecAct As Date, ByVal OrdenIncorrecta As Variant, _
ByVal TipoOrden As TipoPedido, Optional ByVal varIndice As Variant, Optional ByVal Completada As Boolean, Optional ByVal Prove As String, _
Optional ByVal Receptor As String, Optional BloqueoFactura As Variant = 0, Optional ByVal NumErp As Variant, Optional ByVal FechaContable As Variant) As CRecepcion
    'create a new object
    Dim objnewmember As CRecepcion
    
    Set objnewmember = New CRecepcion
    
    Set objnewmember.Conexion = mvarConexion
    objnewmember.ID = ID
    objnewmember.Pedido = Pedido
    'objnewmember.Anyo = Anyo
    objnewmember.Orden = Orden
    objnewmember.OrdenEstado = OrdenEstado
    objnewmember.FechaRec = Fecha
    objnewmember.albaran = albaran
    objnewmember.Correcto = Correcto
    objnewmember.Completo = Completada
    objnewmember.coment = coment
    objnewmember.FechaAct = FecAct
    objnewmember.OrdenEstado = OrdenEstado
    objnewmember.TipoOrden = TipoOrden
    objnewmember.OrdenIncorrecta = SQLBinaryToBoolean(OrdenIncorrecta)
    objnewmember.CodProve = Prove
    objnewmember.Receptor = Receptor
    objnewmember.BloqueoFactura = SQLBinaryToBoolean(BloqueoFactura)
    objnewmember.NumErp = NumErp
    objnewmember.FechaContable = FechaContable
    If Not IsMissing(varIndice) And Not IsNull(varIndice) Then
        objnewmember.Indice = varIndice
        mCol.Add objnewmember, CStr(varIndice)
    Else
    'set the properties passed into the method
        mCol.Add objnewmember, CStr(ID)
    End If
    
    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing

End Function

Private Sub Class_Initialize()
    Set mCol = New Collection
End Sub

Private Sub Class_Terminate()
'destroys collection when this class is terminated
    Set mCol = Nothing
    Set mvarConexion = Nothing
End Sub
