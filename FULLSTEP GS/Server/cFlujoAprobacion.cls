VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "cFlujoAprobacion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private m_sDenCategoria As String
Private m_sCodCategoria As String
Private m_sCodCompletoCategoria As String
Private m_lCategoria As Long
Private m_lNivel As Long
Private m_lTipoPedido As Long
Private m_sTipoPedidoDen As String
Private m_lEmp As Long
Private m_sEmpDen As String
Private m_lSolicitud As Long
Private m_sSolicitudDen As String
Private m_lTipoSolicitud As Long
Private m_lFormulario As Long
Private m_lWorkflow As Long
''' Conexion
Private m_oConexion As CConexion

''' Control de errores
Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum


Friend Property Set Conexion(ByVal vData As CConexion)
    Set m_oConexion = vData
End Property

Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property

Public Property Let Workflow(ByVal dato As Long)
    Let m_lWorkflow = dato
End Property
Public Property Get Workflow() As Long
    Workflow = m_lWorkflow
End Property

Public Property Let Formulario(ByVal dato As Long)
    Let m_lFormulario = dato
End Property
Public Property Get Formulario() As Long
    Formulario = m_lFormulario
End Property

Public Property Let TipoSolicitud(ByVal dato As Long)
    Let m_lTipoSolicitud = dato
End Property
Public Property Get TipoSolicitud() As Long
    TipoSolicitud = m_lTipoSolicitud
End Property

Public Property Let Categoria(ByVal dato As Long)
    Let m_lCategoria = dato
End Property
Public Property Get Categoria() As Long
    Categoria = m_lCategoria
End Property

Public Property Let NivelCat(ByVal dato As Long)
    Let m_lNivel = dato
End Property
Public Property Get NivelCat() As Long
    NivelCat = m_lNivel
End Property

Public Property Let TipoPedido(ByVal dato As Long)
    Let m_lTipoPedido = dato
End Property
Public Property Get TipoPedido() As Long
    TipoPedido = m_lTipoPedido
End Property

Public Property Let TipoPedidoDen(ByVal dato As String)
    Let m_sTipoPedidoDen = dato
End Property
Public Property Get TipoPedidoDen() As String
    TipoPedidoDen = m_sTipoPedidoDen
End Property


Public Property Let CodCategoria(ByVal dato As String)
    Let m_sCodCategoria = dato
End Property
Public Property Get CodCategoria() As String
    CodCategoria = m_sCodCategoria
End Property

Public Property Let CodCompletoCategoria(ByVal dato As String)
    Let m_sCodCompletoCategoria = dato
End Property
Public Property Get CodCompletoCategoria() As String
    CodCompletoCategoria = m_sCodCompletoCategoria
End Property

Public Property Let DenCategoria(ByVal dato As String)
    Let m_sDenCategoria = dato
End Property
Public Property Get DenCategoria() As String
    DenCategoria = m_sDenCategoria
End Property

Public Property Let Emp(ByVal dato As Long)
    Let m_lEmp = dato
End Property
Public Property Get Emp() As Long
    Emp = m_lEmp
End Property

Public Property Let EmpDen(ByVal dato As String)
    Let m_sEmpDen = dato
End Property
Public Property Get EmpDen() As String
    EmpDen = m_sEmpDen
End Property

Public Property Let Solicitud(ByVal dato As Long)
    Let m_lSolicitud = dato
End Property
Public Property Get Solicitud() As Long
    Solicitud = m_lSolicitud
End Property

Public Property Let SolicitudDen(ByVal dato As String)
    Let m_sSolicitudDen = dato
End Property
Public Property Get SolicitudDen() As String
    SolicitudDen = m_sSolicitudDen
End Property


''' <summary>
''' Aņade los flujos de aprobacion(solicitud) para una categoria, tipo de pedido y empresa
''' </summary>
Public Function AņadirABaseDeDatos() As TipoErrorSummit
    Dim sConsulta As String
    
    On Error GoTo Error:

    sConsulta = "INSERT INTO CATN_TIPOPEDIDO_EMP_SOLICITUD (CAT,NIVEL,TIPOPEDIDO,EMP,SOLICITUD) "
    sConsulta = sConsulta & " VALUES (" & Me.Categoria & "," & Me.NivelCat & "," & Me.TipoPedido & "," & Me.Emp & "," & Me.Solicitud & ")"
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then
        GoTo Error
    End If
    
    Exit Function
Error:
    If Not m_oConexion.ADOCon.Errors.Item(1).NativeError <> 2627 Then
        AņadirABaseDeDatos = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    End If

End Function

''' <summary>
''' Elimina el flujo de aprobacion(solicitud) para una categoria, tipo de pedido y empresa
''' </summary>
Public Function EliminarDeBaseDeDatos() As TipoErrorSummit
    Dim sConsulta As String
    
    On Error GoTo Error:

    sConsulta = "DELETE FROM CATN_TIPOPEDIDO_EMP_SOLICITUD WHERE CAT=" & Me.Categoria & " AND NIVEL=" & Me.NivelCat & " AND TIPOPEDIDO=" & Me.TipoPedido & " AND EMP=" & Me.Emp & " AND SOLICITUD=" & Me.Solicitud
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then
        GoTo Error
    End If
    
    Exit Function
Error:
    
    EliminarDeBaseDeDatos = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    

End Function
