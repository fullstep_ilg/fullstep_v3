VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CExpReg"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Private m_lSolicitud As Long
Private m_sCampoProve As String
Private m_sExpRegDescr As String
Private m_sExpReg As String
Private m_iOperador As Integer

Public Property Get IdSolicitud() As Long
    IdSolicitud = m_lSolicitud
End Property
Public Property Let IdSolicitud(ByVal p_lSolicitud As Long)
    m_lSolicitud = p_lSolicitud
End Property

Public Property Get CampoProve() As String
    CampoProve = m_sCampoProve
End Property
Public Property Let CampoProve(ByVal p_sCampoProve As String)
    m_sCampoProve = p_sCampoProve
End Property

Public Property Get ExpRegDescr() As String
    ExpRegDescr = m_sExpRegDescr
End Property
Public Property Let ExpRegDescr(ByVal p_sExpRegDescr As String)
    m_sExpRegDescr = p_sExpRegDescr
End Property

Public Property Get ExpReg() As String
    ExpReg = m_sExpReg
End Property
Public Property Let ExpReg(ByVal p_sExpReg As String)
    m_sExpReg = p_sExpReg
End Property

Public Property Get Operador() As Integer
    Operador = m_iOperador
End Property
Public Property Let Operador(ByVal p_iOperador As Integer)
    m_iOperador = p_iOperador
End Property
