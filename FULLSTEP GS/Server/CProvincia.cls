VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CProvincia"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Implements IBaseDatos

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    FaltaObjetoPais = 614
    
End Enum
Private m_oConexion As CConexion 'local copy
Private m_lIndice As Long 'local copy
Private m_adores As adodb.Recordset
Private m_sCod As String 'local copy
Private m_sDen As String 'local copy
Private m_oDen As CMultiidiomas
Private m_oPais As CPais 'local copy
Private m_iID As Integer
Private m_vFecAct As Variant
''' Estado integraci�n
Private m_vEstIntegracion As Variant

' Variables para el control de cambios del Log e Integraci�n
Private m_sUsuario As String             'Usuario que realiza los cambios.
Private m_udtOrigen As OrigenIntegracion 'Origen del mvto en la tabla de LOG

Public Property Let FecAct(ByVal vData As Variant)

    ''' * Objetivo: Dar valor a la variable privada FECACT
    ''' * Recibe: Fecha de ultima actualizacion de la moneda
    ''' * Devuelve: Nada

    m_vFecAct = vData
    
End Property
Public Property Get FecAct() As Variant

    ''' * Objetivo: Devolver la variable privada FECACT
    ''' * Recibe: Nada
    ''' * Devuelve: Fecha de ultima actualizacion de la moneda

    FecAct = m_vFecAct
    
End Property

Public Property Let Id(ByVal Data As Integer)
    Let m_iID = Data
End Property
Public Property Get Id() As Integer
    Id = m_iID
End Property


Public Property Set Pais(ByVal vData As CPais)
    Set m_oPais = vData
End Property


Public Property Get Pais() As CPais
    Set Pais = m_oPais
End Property

Public Property Let Indice(ByVal vData As Long)
    m_lIndice = vData
End Property


Public Property Get Indice() As Long
    Indice = m_lIndice
End Property



Friend Property Set Conexion(ByVal vData As CConexion)
    Set m_oConexion = vData
End Property


Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property



Public Property Let Den(ByVal vData As String)
    m_sDen = vData
End Property


Public Property Get Den() As String
    Den = m_sDen
End Property

Public Property Get Denominaciones() As CMultiidiomas

    ''' * Objetivo: Devolver la denominacion
    ''' * Recibe: Nada
    ''' * Devuelve: Conexion
    Set Denominaciones = m_oDen
    
End Property

Public Property Set Denominaciones(ByVal dato As CMultiidiomas)

    ''' * Objetivo: Dar valor al objeto Denominaciones
    ''' * Recibe: Codigo de la Unidad
    ''' * Devuelve: Nada
    Set m_oDen = dato
    
End Property

Public Property Let Cod(ByVal vData As String)
    m_sCod = vData
End Property


Public Property Get Cod() As String
    Cod = m_sCod
End Property
Public Property Let Usuario(ByVal vData As String)

    ''' * Objetivo: Dar valor a la variable privada Usuario
    ''' * Recibe: Codigo del usuario conectado
    ''' * Devuelve: Nada

    m_sUsuario = vData
    
End Property
Public Property Let EstadoIntegracion(ByVal vData As Variant)

    ''' * Objetivo: Dar valor al estado de integraci�n de la moneda
    ''' * Recibe: el valor
    ''' * Devuelve: Nada

    m_vEstIntegracion = vData
    
End Property
Public Property Get EstadoIntegracion() As Variant

    ''' * Objetivo: Devolver el estado de integraci�n de la moneda
    ''' * Recibe: Nada
    ''' * Devuelve: El estado de integraci�n de la moneda

    EstadoIntegracion = m_vEstIntegracion
    
End Property
Private Function GrabarEnLog() As Boolean
'***********************************************************************************
'*** Descripci�n: Nos indica si hay que grabar o no el cambio en el log de       ***
'***              monedas (tabla LOG_PROVI) y carga en la variable               ***
'***              del m�dulo m_udtOrigen el origen del movimiento                ***
'*** Parametros:  Ninguno                                                        ***
'*** Devuelve:    True (hay que grabar) False (No hay que grabar)                ***
'***********************************************************************************

    If gParametrosGenerales.gbACTIVLOG Or gParametrosIntegracion.gaExportar(EntidadIntegracion.Provi) Then
        If gParametrosGenerales.gbACTIVLOG And gParametrosIntegracion.gaExportar(EntidadIntegracion.Provi) Then
            m_udtOrigen = FSGSIntReg
        Else
            If gParametrosGenerales.gbACTIVLOG Then
                m_udtOrigen = FSGSReg
            Else
                m_udtOrigen = FSGSInt
            End If
        End If
        GrabarEnLog = True
    Else
        GrabarEnLog = False
    End If

End Function





Private Sub Class_Terminate()
    Set m_oConexion = Nothing

End Sub

''' <summary>Esta funci�n a�ade la provincia de la tabla </summary>
''' <returns>TipoErrorSummit con el error si lo hay</returns>
''' <remarks>Llamada desde: FSGClient
''' Tiempo m�ximo: 1 sec </remarks>
''' <revision>JVS 05/01/2012</revision>
Private Function IBaseDatos_AnyadirABaseDatos() As TipoErrorSummit

Dim sConsulta As String
Dim TESError As TipoErrorSummit
Dim bTransaccionEnCurso As Boolean
Dim AdoRes As adodb.Recordset
Dim adoComm As adodb.Command
Dim adoParam As adodb.Parameter
Dim sFSP As String
Dim oMulti As CMultiidioma
Dim lIdLogPROVI As Long

TESError.NumError = TESnoerror
'******************************************
'***** M U Y   I M P O R T A N T E     ****
'*****                                 ****
'***** SI SE REALIZAN CAMBIOS          ****
'***** DESPUES DEL 21/06/2002          ****
'***** MIRAR SI HAY QUE PASARLOS A     ****
'***** FSGSINTEGRATION.DLL             ****
'******************************************

'********* Precondicion *******************
If m_oConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CProvincia.AnyadirABAseDatos", "No se ha establecido la conexion"
End If

If m_oPais Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.FaltaObjetoPais, "CProvincia.AnyadirABAseDatos", "No se ha indicado el Pais de la provincia"
End If

'******************************************

On Error GoTo ERROR:

    m_oConexion.ADOCon.Execute "BEGIN DISTRIBUTED TRAN"
    m_oConexion.ADOCon.Execute "SET XACT_ABORT ON"
    bTransaccionEnCurso = True
 
    sConsulta = "INSERT INTO PROVI(PAI,COD,DEN) VALUES ('" & DblQuote(m_oPais.Cod) & "','" & DblQuote(m_sCod) & "','" & DblQuote(m_sDen) & "')"
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR
    
    ''' Recuperar la fecha de insercion
    'CALIDAD Se obtiene la Fecha de Actualizaci�n con bloqueo para recuperar la verdadera �ltima fecha de actualizaci�n
    sConsulta = "SELECT FECACT FROM PROVI WHERE COD ='" & DblQuote(m_sCod) & "' AND PAI ='" & DblQuote(m_oPais.Cod) & "'"
    Set AdoRes = New adodb.Recordset
    AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    m_vFecAct = AdoRes("FECACT").Value
    AdoRes.Close
    
    'Inserta las denominaciones en PROVI_DEN
    If Not m_oDen Is Nothing Then
        For Each oMulti In m_oDen
            If Not IsEmpty(oMulti.Den) Then
                sConsulta = "INSERT INTO PROVI_DEN (PROVI,DEN,IDIOMA) VALUES (N'" & DblQuote(m_sCod) & "',"
                sConsulta = sConsulta & StrToSQLNULLNVar(oMulti.Den) & ",'" & DblQuote(oMulti.Cod) & "')"
                m_oConexion.ADOCon.Execute sConsulta
                If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR
            End If
        Next
    End If
    
    'A�ade la provincia tambi�n en el portal:
    If basParametros.gParametrosGenerales.giINSTWEB = ConPortal Then
        If basParametros.gParametrosGenerales.gbUnaCompradora = True Then
            sFSP = basParametros.gParametrosGenerales.gsFSP_SRV & "." & basParametros.gParametrosGenerales.gsFSP_BD & ".dbo."
            Set adoComm = New adodb.Command
            
            Set adoParam = adoComm.CreateParameter("PAI", adVarChar, adParamInput, 50, m_oPais.Cod)
            adoComm.Parameters.Append adoParam
            Set adoParam = adoComm.CreateParameter("COD", adVarChar, adParamInput, 50, m_sCod)
            adoComm.Parameters.Append adoParam
            Set adoParam = adoComm.CreateParameter("DEN", adVarChar, adParamInput, 100, m_sDen)
            adoComm.Parameters.Append adoParam
            
            Set adoComm.ActiveConnection = m_oConexion.ADOCon
            adoComm.CommandType = adCmdStoredProc
            adoComm.CommandText = sFSP & "SP_ANYA_PROVI"
            adoComm.Prepared = True
            adoComm.Execute
            If m_oConexion.ADOCon.Errors.Count > 0 Then
                GoTo ERROR
            End If
            Set adoComm = Nothing
            sConsulta = "UPDATE PROVI SET FSP_PAI='" & DblQuote(m_oPais.Cod) & "', FSP_COD='" & DblQuote(m_sCod) & "' WHERE PAI='" & DblQuote(m_oPais.Cod) & "' AND COD='" & DblQuote(m_sCod) & "'"
            m_oConexion.ADOCon.Execute sConsulta
            If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR
        End If
    End If
    
    ' Tratamiento para el LOG de cambios y la Integraci�n
    If GrabarEnLog Then
        ''10/09/2007(ngo):MultiERP
        sConsulta = "SELECT COD FROM ERP WITH(NOLOCK)"
        AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
        While Not AdoRes.eof
            sConsulta = "INSERT INTO LOG_PROVI (ACCION,PAI,COD,ORIGEN,USU,ERP) VALUES ('" & Accion_Alta & "',N'" & DblQuote(m_oPais.Cod) & "',N'" & DblQuote(m_sCod) & "'," & m_udtOrigen & ",'" & DblQuote(m_sUsuario) & "','" & DblQuote(AdoRes.Fields("COD").Value) & "')"
            m_oConexion.ADOCon.Execute sConsulta
            If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR
            
            Dim ador As adodb.Recordset
            Set ador = New adodb.Recordset
            'CALIDAD Sin WITH(NOLOCK) en el FROM para asegurar la recuperaci�n del mayor valor de la tabla
            ador.Open "SELECT SCOPE_IDENTITY()", m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
            If Not ador.eof Then
                lIdLogPROVI = ador.Collect(0)
            End If
            ador.Close
            Set ador = Nothing
            
            If Not m_oDen Is Nothing Then
                For Each oMulti In m_oDen
                    sConsulta = "INSERT INTO LOG_PROVI_DEN (ID_LOG_PROVI,DEN,IDI) VALUES (" & lIdLogPROVI & ","
                    sConsulta = sConsulta & StrToSQLNULLNVar(oMulti.Den) & ",'" & DblQuote(oMulti.Cod) & "')"
                    m_oConexion.ADOCon.Execute sConsulta
                    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR
                Next
            End If
            
            m_vEstIntegracion = 0 'EstadoIntegracion.PendienteDeTratar
            AdoRes.MoveNext
        Wend
        AdoRes.Close
    End If
   
    m_oConexion.ADOCon.Execute "SET XACT_ABORT OFF"
    m_oConexion.ADOCon.Execute "COMMIT TRAN"
    bTransaccionEnCurso = False
    
    Set AdoRes = Nothing
    
    IBaseDatos_AnyadirABaseDatos = TESError
    Exit Function
    
ERROR:
    IBaseDatos_AnyadirABaseDatos = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    
    If bTransaccionEnCurso Then
        m_oConexion.ADOCon.Execute "SET XACT_ABORT OFF"
        bTransaccionEnCurso = False
    End If
    
End Function

''' <summary>Cambiar de codigo la provincia </summary>
''' <param name="CodigoNuevo">Codigo nuevo</param>
''' <returns>TipoErrorSummit con el error si lo hay</returns>
''' <remarks>Llamada desde: FSGClient
''' Tiempo m�ximo: 1 sec </remarks>
''' <revision>JVS 05/01/2012</revision>
Private Function IBaseDatos_CambiarCodigo(CodigoNuevo As Variant) As TipoErrorSummit

 '******************************************
'***** M U Y   I M P O R T A N T E     ****
'*****                                 ****
'***** SI SE REALIZAN CAMBIOS          ****
'***** DESPUES DEL 21/06/2002          ****
'***** MIRAR SI HAY QUE PASARLOS A     ****
'***** FSGSINTEGRATION.DLL             ****
'******************************************
   
    Dim TESError As TipoErrorSummit
    Dim adoComm As adodb.Command
    Dim adoParam As adodb.Parameter
    Dim sConsulta  As String
    Dim ContRegActualizados As Long
    Dim sFSP As String
    Dim oMulti As CMultiidioma
    Dim lIdLogPROVI  As Long
    
    TESError.NumError = TESnoerror
    
    ''' Precondicion
    
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CProvincia.EliminarDeBaseDatos", "No se ha establecido la conexion"
    End If
        
On Error GoTo ERROR:
    
    m_oConexion.ADOCon.Execute "BEGIN DISTRIBUTED TRAN"
    m_oConexion.ADOCon.Execute "SET XACT_ABORT ON"
    
    ''' Preparar la SP y sus parametros
    
    Set adoComm = New adodb.Command
    Set adoComm.ActiveConnection = m_oConexion.ADOCon
    
    Set adoParam = adoComm.CreateParameter("OLD", adVarChar, adParamInput, 50, m_sCod)
    adoComm.Parameters.Append adoParam
    Set adoParam = adoComm.CreateParameter("NEW", adVarChar, adParamInput, 50, CodigoNuevo)
    adoComm.Parameters.Append adoParam
    Set adoParam = adoComm.CreateParameter("PAI", adVarChar, adParamInput, 50, Pais.Cod)
    adoComm.Parameters.Append adoParam
    
    adoComm.CommandType = adCmdStoredProc
    adoComm.CommandText = "PROVI_COD"
    
    ''' Ejecutar la SP
    
    adoComm.Execute ContRegActualizados
    If m_oConexion.ADOCon.Errors.Count > 0 Then
        GoTo ERROR
    End If
    Set adoComm = Nothing
 
    'Modifica el c�digo de la provincia correspondiente en el portal:
    If basParametros.gParametrosGenerales.giINSTWEB = ConPortal Then
        If basParametros.gParametrosGenerales.gbUnaCompradora = True Then
            sConsulta = "UPDATE PROVI SET FSP_COD='" & DblQuote(CodigoNuevo) & "' WHERE FSP_PAI='" & DblQuote(Pais.Cod) & "' AND FSP_COD='" & DblQuote(m_sCod) & "'"
            m_oConexion.ADOCon.Execute sConsulta
            If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR
            
            sFSP = basParametros.gParametrosGenerales.gsFSP_SRV & "." & basParametros.gParametrosGenerales.gsFSP_BD & ".dbo."
            Set adoComm = New adodb.Command
    
            Set adoParam = adoComm.CreateParameter("PAIS", adVarChar, adParamInput, 50, Pais.Cod)
            adoComm.Parameters.Append adoParam
            Set adoParam = adoComm.CreateParameter("OLD", adVarChar, adParamInput, 50, m_sCod)
            adoComm.Parameters.Append adoParam
            Set adoParam = adoComm.CreateParameter("NEW", adVarChar, adParamInput, 50, CodigoNuevo)
            adoComm.Parameters.Append adoParam
            
            Set adoComm.ActiveConnection = m_oConexion.ADOCon
            adoComm.CommandType = adCmdStoredProc
            adoComm.CommandText = sFSP & "PROVI_COD"
            adoComm.Prepared = True
            adoComm.Execute
            If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR
            Set adoComm = Nothing
        End If
    End If
    
    'Recuperamos el valor de FecAct
    Set m_adores = New adodb.Recordset
    'CALIDAD Se obtiene la Fecha de Actualizaci�n con bloqueo para recuperar la verdadera �ltima fecha de actualizaci�n
    sConsulta = "SELECT FECACT FROM PROVI WHERE COD='" & DblQuote(CodigoNuevo) & "' AND PAI='" & DblQuote(Pais.Cod) & "'"
    m_adores.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
    m_vFecAct = m_adores("FECACT").Value
    m_adores.Close
    Set m_adores = Nothing
    
    ''' Tratamiento para el LOG de cambios y la Integraci�n
    If GrabarEnLog And ContRegActualizados <> -1 Then
        ''10/09/2007(ngo):MultiERP
        Dim AdoRes As adodb.Recordset
        Set AdoRes = New adodb.Recordset
        sConsulta = "SELECT COD FROM ERP WITH(NOLOCK)"
        AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
        While Not AdoRes.eof
            sConsulta = "INSERT INTO LOG_PROVI (ACCION,PAI,COD,COD_NEW,ORIGEN,USU,ERP) VALUES ('" & Accion_CambioCodigo & "',N'" & DblQuote(Pais.Cod) & "',N'" & DblQuote(m_sCod) & "',N'" & DblQuote(CodigoNuevo) & "'," & m_udtOrigen & ",'" & DblQuote(m_sUsuario) & "','" & DblQuote(AdoRes.Fields("COD").Value) & "')"
            m_oConexion.ADOCon.Execute sConsulta
            If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR
            
            Dim ador As adodb.Recordset
            Set ador = New adodb.Recordset
            'CALIDAD Sin WITH(NOLOCK) en el FROM para asegurar la recuperaci�n del mayor valor de la tabla
            ador.Open "SELECT SCOPE_IDENTITY()", m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
            If Not ador.eof Then
                lIdLogPROVI = ador.Collect(0)
            End If
            ador.Close
            Set ador = Nothing
            
            If Not m_oDen Is Nothing Then
                For Each oMulti In m_oDen
                    sConsulta = "INSERT INTO LOG_PROVI_DEN (ID_LOG_PROVI,DEN,IDI) VALUES (" & lIdLogPROVI & ","
                    sConsulta = sConsulta & StrToSQLNULLNVar(oMulti.Den) & ",'" & DblQuote(oMulti.Cod) & "')"
                    m_oConexion.ADOCon.Execute sConsulta
                    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR
                Next
            End If
            
            m_vEstIntegracion = 0 'EstadoIntegracion.PendienteDeTratar
            AdoRes.MoveNext
        Wend
        AdoRes.Close
        Set AdoRes = Nothing
    End If
    
    m_sCod = CodigoNuevo
        
    m_oConexion.ADOCon.Execute "SET XACT_ABORT OFF"
    m_oConexion.ADOCon.Execute "COMMIT TRAN"

    IBaseDatos_CambiarCodigo = TESError
    
    Exit Function
    
ERROR:
    
    IBaseDatos_CambiarCodigo = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    
    If Not adoComm Is Nothing Then
        Set adoComm = Nothing
    End If
    
    m_oConexion.ADOCon.Execute "SET XACT_ABORT OFF"
    
End Function

Private Sub IBaseDatos_CancelarEdicion()

End Sub

Private Function IBaseDatos_ComprobarExistenciaEnBaseDatos() As Boolean

End Function

''' <summary>Eliminar la provincia </summary>
''' <returns>TipoErrorSummit con el error si lo hay</returns>
''' <remarks>Llamada desde: FSGClient
''' Tiempo m�ximo: 1 sec </remarks>
''' <revision>JVS 05/01/2012</revision>
Private Function IBaseDatos_EliminarDeBaseDatos() As TipoErrorSummit
Dim TESError As TipoErrorSummit
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim ContRegBorrados As Long
Dim lPais As Long
Dim sFSP As String
Dim AdoRes As adodb.Recordset
Dim adoComm As adodb.Command
Dim adoParam As adodb.Parameter
Dim oMulti As CMultiidioma
Dim lIdLogPROVI As Long

'******************************************
'***** M U Y   I M P O R T A N T E     ****
'*****                                 ****
'***** SI SE REALIZAN CAMBIOS          ****
'***** DESPUES DEL 21/06/2002          ****
'***** MIRAR SI HAY QUE PASARLOS A     ****
'***** FSGSINTEGRATION.DLL             ****
'******************************************

TESError.NumError = TESnoerror

'******************* Precondicion *******************
If m_oConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CProvincia.EliminarDeBaseDatos", "No se ha establecido la conexion"
End If
'*****************************************************
On Error GoTo ERROR:
    ''' Borrado
    m_oConexion.ADOCon.Execute "BEGIN DISTRIBUTED TRAN"
    m_oConexion.ADOCon.Execute "SET XACT_ABORT ON"
    bTransaccionEnCurso = True
    
    ''MultiERP: Elimina de PROVI_ERP
    m_oConexion.ADOCon.Execute "DELETE FROM PROVI_ERP WHERE COD_GS='" & DblQuote(m_sCod) & "' AND PAI_GS ='" & DblQuote(m_oPais.Cod) & "'"
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR
    m_oConexion.ADOCon.Execute "DELETE FROM PROVI WHERE COD='" & DblQuote(m_sCod) & "' AND PAI ='" & DblQuote(m_oPais.Cod) & "'", ContRegBorrados
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR

    'Elimina la provincia correspondiente en el portal:
    If basParametros.gParametrosGenerales.giINSTWEB = ConPortal Then
        If basParametros.gParametrosGenerales.gbUnaCompradora = True Then
            Set adoComm = New adodb.Command
    
            Set adoParam = adoComm.CreateParameter("PAI_COD", adVarChar, adParamInput, 20, m_oPais.Cod)
            adoComm.Parameters.Append adoParam
            Set adoParam = adoComm.CreateParameter("PROVI_COD", adVarChar, adParamInput, 20, m_sCod)
            adoComm.Parameters.Append adoParam
            
            Set adoComm.ActiveConnection = m_oConexion.ADOCon
            adoComm.CommandType = adCmdStoredProc
            adoComm.CommandText = sFSP & "SP_ELIM_PROVI"
            adoComm.Prepared = True
            adoComm.Execute
            If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR
            Set adoComm = Nothing
        End If
    End If
        
    ''' Tratamiento para el LOG de cambios y la Integraci�n
    If GrabarEnLog And ContRegBorrados > 0 Then
        ''10/09/2007(ngo):MultiERP
        Set AdoRes = New adodb.Recordset
        sConsulta = "SELECT COD FROM ERP WITH(NOLOCK)"
        AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
        While Not AdoRes.eof
            sConsulta = "INSERT INTO LOG_PROVI (ACCION,PAI,COD,ORIGEN,USU,ERP) VALUES ('" & Accion_Baja & "',N'" & DblQuote(m_oPais.Cod) & "',N'" & DblQuote(m_sCod) & "'," & m_udtOrigen & ",'" & DblQuote(m_sUsuario) & "','" & DblQuote(AdoRes.Fields("COD").Value) & ")"
            m_oConexion.ADOCon.Execute sConsulta
            If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR
            
            Dim ador As adodb.Recordset
            Set ador = New adodb.Recordset
            'CALIDAD Sin WITH(NOLOCK) en el FROM para asegurar la recuperaci�n del mayor valor de la tabla
            ador.Open "SELECT SCOPE_IDENTITY()", m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
            If Not ador.eof Then
                lIdLogPROVI = ador.Collect(0)
            End If
            ador.Close
            Set ador = Nothing
            
            If Not m_oDen Is Nothing Then
                For Each oMulti In m_oDen
                    sConsulta = "INSERT INTO LOG_PROVI_DEN (ID_LOG_PROVI,DEN,IDI) VALUES (" & lIdLogPROVI & ","
                    sConsulta = sConsulta & StrToSQLNULLNVar(oMulti.Den) & ",'" & DblQuote(oMulti.Cod) & "')"
                    m_oConexion.ADOCon.Execute sConsulta
                    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR
                Next
            End If
            
            AdoRes.MoveNext
        Wend
        AdoRes.Close
        Set AdoRes = Nothing
    End If
    
    ''' Terminar transaccion
    m_oConexion.ADOCon.Execute "SET XACT_ABORT OFF"
    m_oConexion.ADOCon.Execute "COMMIT TRAN"

    bTransaccionEnCurso = False
           
    IBaseDatos_EliminarDeBaseDatos = TESError
    Exit Function
    
ERROR:

    IBaseDatos_EliminarDeBaseDatos = basErrores.TratarError(m_oConexion.ADOCon.Errors)

    If bTransaccionEnCurso Then
        m_oConexion.ADOCon.Execute "SET XACT_ABORT OFF"
        bTransaccionEnCurso = False
    End If
      
    
End Function

Private Function IBaseDatos_FinalizarEdicionEliminando() As TipoErrorSummit

End Function

''' <summary>Esta funci�n modifica la provincia de la tabla </summary>
''' <returns>TipoErrorSummit con el error si lo hay</returns>
''' <remarks>Llamada desde: FSGClient
''' Tiempo m�ximo: 1 sec </remarks>
''' <revision>JVS 05/01/2012</revision>
Private Function IBaseDatos_FinalizarEdicionModificando() As TipoErrorSummit
Dim TESError As TipoErrorSummit
Dim sConsulta As String
Dim bTransaccionEnCurso As Boolean
Dim adoComm As adodb.Command
Dim adoParam As adodb.Parameter
Dim sFSP As String
Dim oMulti As CMultiidioma
Dim lIdLogPROVI As Long

TESError.NumError = TESnoerror
'******************************************
'***** M U Y   I M P O R T A N T E     ****
'*****                                 ****
'***** SI SE REALIZAN CAMBIOS          ****
'***** DESPUES DEL 21/06/2002          ****
'***** MIRAR SI HAY QUE PASARLOS A     ****
'***** FSGSINTEGRATION.DLL             ****
'******************************************

'********* Precondicion *******************
If m_oConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CProvincia.FinalizarEdicionModificando", "No se ha establecido la conexion"
End If
'******************************************
On Error GoTo ERROR:
    
    m_oConexion.ADOCon.Execute "BEGIN DISTRIBUTED TRAN"
    m_oConexion.ADOCon.Execute "SET XACT_ABORT ON"
    bTransaccionEnCurso = True
    
    'Comprobamos si ha habido cambios en otra sesi�n
    Set m_adores = New adodb.Recordset
    'CALIDAD Se obtiene la Fecha de Actualizaci�n con bloqueo para recuperar la verdadera �ltima fecha de actualizaci�n
    sConsulta = "SELECT FECACT FROM PROVI WHERE COD='" & DblQuote(m_sCod) & "' AND PAI ='" & DblQuote(m_oPais.Cod) & "'"
    m_adores.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
    
    If m_adores.eof Then
        m_adores.Close
        Set m_adores = Nothing
        TESError.NumError = TESDatoEliminado
        TESError.Arg1 = 7
        IBaseDatos_FinalizarEdicionModificando = TESError
        m_oConexion.ADOCon.Execute "SET XACT_ABORT OFF"
        m_oConexion.ADOCon.Execute "ROLLBACK TRAN"
        Exit Function
    End If
    
    If m_vFecAct <> m_adores("FECACT").Value Then
        m_adores.Close
        Set m_adores = Nothing
        TESError.NumError = TESInfActualModificada
        IBaseDatos_FinalizarEdicionModificando = TESError
        m_oConexion.ADOCon.Execute "SET XACT_ABORT OFF"
        m_oConexion.ADOCon.Execute "ROLLBACK TRAN"
        Exit Function
    End If
    
    ''' Actualizar
    sConsulta = "UPDATE PROVI SET DEN= '" & DblQuote(m_sDen) & "'"
    sConsulta = sConsulta & " WHERE COD = '" & DblQuote(m_sCod) & "' AND PAI ='" & DblQuote(m_oPais.Cod) & "'"
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR
   
    'Recogemos el nuevo valor de FECACT
    m_adores.Requery
    m_vFecAct = m_adores("FECACT").Value
    m_adores.Close
    Set m_adores = Nothing
    
    'Modifica la provincia correspondiente en el portal:
    If basParametros.gParametrosGenerales.giINSTWEB = ConPortal Then
        If basParametros.gParametrosGenerales.gbUnaCompradora = True Then
            sFSP = basParametros.gParametrosGenerales.gsFSP_SRV & "." & basParametros.gParametrosGenerales.gsFSP_BD & ".dbo."
            Set adoComm = New adodb.Command
    
            Set adoParam = adoComm.CreateParameter("PAIS", adVarChar, adParamInput, 50, m_oPais.Cod)
            adoComm.Parameters.Append adoParam
            Set adoParam = adoComm.CreateParameter("COD", adVarChar, adParamInput, 50, m_sCod)
            adoComm.Parameters.Append adoParam
            Set adoParam = adoComm.CreateParameter("DEN", adVarChar, adParamInput, 100, m_sDen)
            adoComm.Parameters.Append adoParam
            
            Set adoComm.ActiveConnection = m_oConexion.ADOCon
            adoComm.CommandType = adCmdStoredProc
            adoComm.CommandText = sFSP & "SP_MODIF_PROVI"
            adoComm.Prepared = True
            adoComm.Execute
            If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR
            Set adoComm = Nothing
        End If
    End If
        
    ''' Tratamiento para el LOG de cambios y la Integraci�n
    If GrabarEnLog Then
        ''10/09/2007(ngo):MultiERP
        Dim AdoRes As adodb.Recordset
        Set AdoRes = New adodb.Recordset
        sConsulta = "SELECT COD FROM ERP WITH(NOLOCK)"
        AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
        While Not AdoRes.eof
            sConsulta = "INSERT INTO LOG_PROVI (ACCION,PAI,COD,ORIGEN,USU,ERP) VALUES ('" & Accion_Modificacion & "',N'" & DblQuote(m_oPais.Cod) & "',N'" & DblQuote(m_sCod) & "'," & m_udtOrigen & ",'" & DblQuote(m_sUsuario) & "','" & DblQuote(AdoRes.Fields("COD").Value) & "')"
            m_oConexion.ADOCon.Execute sConsulta
            If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR
            
            Dim ador As adodb.Recordset
            Set ador = New adodb.Recordset
            'CALIDAD Sin WITH(NOLOCK) en el FROM para asegurar la recuperaci�n del mayor valor de la tabla
            ador.Open "SELECT SCOPE_IDENTITY()", m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
            If Not ador.eof Then
                lIdLogPROVI = ador.Collect(0)
            End If
            ador.Close
            Set ador = Nothing
            
            If Not m_oDen Is Nothing Then
                For Each oMulti In m_oDen
                    sConsulta = "INSERT INTO LOG_PROVI_DEN (ID_LOG_PROVI,DEN,IDI) VALUES (" & lIdLogPROVI & ","
                    sConsulta = sConsulta & StrToSQLNULLNVar(oMulti.Den) & ",'" & DblQuote(oMulti.Cod) & "')"
                    m_oConexion.ADOCon.Execute sConsulta
                    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR
                Next
            End If
            
            m_vEstIntegracion = 0 'EstadoIntegracion.PendienteDeTratar
            AdoRes.MoveNext
        Wend
        AdoRes.Close
        Set AdoRes = Nothing
    End If
    
    m_oConexion.ADOCon.Execute "SET XACT_ABORT OFF"
    m_oConexion.ADOCon.Execute "COMMIT TRAN"
    bTransaccionEnCurso = False
 
    IBaseDatos_FinalizarEdicionModificando = TESError
    
    Exit Function
    
ERROR:
    IBaseDatos_FinalizarEdicionModificando = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    
    If Not m_adores Is Nothing Then
        m_adores.Close
        Set m_adores = Nothing
    End If
    
    If bTransaccionEnCurso Then
        m_oConexion.ADOCon.Execute "SET XACT_ABORT OFF"
        bTransaccionEnCurso = False
    End If
   
    
End Function

''' <summary>
''' Esta funci�n inicia la edici�n del resulset
''' </summary>
'''<param optional name="Bloquear">Si se bloquea o no</param>
'''<param optional name="UsuarioBloqueo">Usuario del bloqueo</param>
''' <returns>TipoErrorSummit con el error si lo hay</returns>
''' <remarks>Llamada desde: FSGClient
''' Tiempo m�ximo: 05/01/2012</revision>
Private Function IBaseDatos_IniciarEdicion(Optional ByVal Bloquear As Boolean, Optional ByVal UsuarioBloqueo As String) As TipoErrorSummit
Dim TESError As TipoErrorSummit
Dim rs As New adodb.Recordset
Dim sConsulta As String

TESError.NumError = TESnoerror

'******************* Precondicion *******************
If m_oConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CProvincia.IniciarEdicion", "No se ha establecido la conexion"
End If
'*****************************************************
'CALIDAD Se obtiene la Fecha de Actualizaci�n con bloqueo para recuperar la verdadera �ltima fecha de actualizaci�n
sConsulta = "SELECT COD,DEN,PAI,FECACT FROM PROVI WHERE COD='" & DblQuote(m_sCod) & "' AND PAI='" & DblQuote(m_oPais.Cod) & "'"
Set m_adores = New adodb.Recordset
m_adores.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly

If m_adores.eof Then
    m_adores.Close
    Set m_adores = Nothing
    TESError.NumError = TESDatoEliminado
    TESError.Arg1 = 13 ''"Provincia"
    IBaseDatos_IniciarEdicion = TESError
    Exit Function
End If

If m_vFecAct <> m_adores("FECACT").Value Then
    m_adores.Close
    Set m_adores = Nothing
    TESError.NumError = TESInfActualModificada
    IBaseDatos_IniciarEdicion = TESError
    Exit Function
End If

m_sCod = m_adores("COD").Value
m_sDen = m_adores("DEN").Value

Set m_oPais = New CPais
m_oPais.Cod = m_adores("PAI").Value
Set m_oPais.Conexion = m_oConexion

IBaseDatos_IniciarEdicion = TESError

End Function


