VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CDepartamentos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Collection" ,"CDepartamento"
Attribute VB_Ext_KEY = "Member0" ,"CDepartamento"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit
Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
End Enum
'local variable to hold collection
Private mCol As Collection
Private mvarConexion As CConexion
Private mvarConexionServer As CConexionDeUseServer
Private mvarEOF As Boolean

Public Function Add(ByVal Cod As Variant, ByVal Den As String, Optional ByVal varIndice As Variant) As CDepartamento
    
    'create a new object
    Dim objnewmember As CDepartamento
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New CDepartamento


    'set the properties passed into the method
    objnewmember.Cod = Cod
    objnewmember.Den = Den
    Set objnewmember.Conexion = mvarConexion
    Set objnewmember.ConexionServer = mvarConexionServer
    If IsNull(varIndice) Or IsMissing(varIndice) Then
        mCol.Add objnewmember, CStr(Cod)
    Else
        mCol.Add objnewmember, CStr(varIndice)
    End If


    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CDepartamentos", "Add", ERR, Erl)
        Exit Function
    End If

End Function

Public Property Get Item(vntIndexKey As Variant) As CDepartamento
Attribute Item.VB_UserMemId = 0
   On Error GoTo ERROR:
   
    Set Item = mCol(vntIndexKey)
    Exit Property
ERROR:
    Set Item = Nothing
    
End Property

Public Property Get eof() As Boolean
    eof = mvarEOF
End Property

Public Property Get Count() As Long
    
    If mCol Is Nothing Then
        Count = 0
    Else
        Count = mCol.Count
    End If
    
End Property


Public Sub Remove(vntIndexKey As Variant)
    mCol.Remove vntIndexKey
End Sub


Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
    Set NewEnum = mCol.[_NewEnum]
End Property
Friend Property Set Conexion(ByVal con As CConexion)
Set mvarConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = mvarConexion
End Property
Friend Property Set ConexionServer(ByVal con As CConexionDeUseServer)
Set mvarConexionServer = con
End Property
Friend Property Get ConexionServer() As CConexionDeUseServer
Set ConexionServer = mvarConexionServer
End Property

Private Sub Class_Initialize()
    'creates the collection when this class is created
        '*-*'
  
    Set mCol = New Collection
End Sub


Private Sub Class_Terminate()
    'destroys collection when this class is terminated
    Set mCol = Nothing
    Set mvarConexion = Nothing
    Set mvarConexionServer = Nothing
End Sub

''' <summary>
''' Carga la colecci�n interna de objetos CEmpresa con los elementos que satisfacen las condiciones establecidas
''' en los par�metros.
''' </summary>
''' <param name="sCod">C�digo de la empresa</param>
''' <param name="sDen">Nombre de la empresa</param>
''' <param name="sUON1">C�digo de la UON1</param>
''' <param name="sUON2">C�digo de la UON2</param>
''' <param name="sUON3">C�digo de la UON3</param>
''' <param name="CoincidenciaTotal">Indica si los filtros por c�digo y denominaci�n, se van a realizar con un like o con un operador de igualdad</param>
''' <param name="OrdenadosPorDen">Indica si los elementos de la colecci�n se va a ordenar por denominaci�n</param>
''' <param name="UsarIndice">Indica si la colecci�m interna se va a indexar</param>
''' <param name="IncluirBajaLogica">Indica si en la colecci�n se incluyen los registros dados de baja l�gica</param>
''' <param name="bRPerfUON">Restringir a las UON del perfil del usuario</param>
''' <param name="lIdPerf">Id perfil</param>
''' <remarks>Llamada desde: frmSolicitudes</remarks>
''' <revision>LTG 16/01/2013</revision>

Public Sub CargarTodosLosDepartamentosPorUON(Optional ByVal sCod As String, Optional ByVal sDen As String, Optional ByVal sUON1 As String, _
        Optional ByVal sUON2 As String, Optional ByVal sUON3 As String, Optional ByVal CoincidenciaTotal As Boolean, _
        Optional ByVal OrdenadosPorDen As Boolean = False, Optional ByVal UsarIndice As Boolean = False, Optional ByVal IncluirBajaLogica As Boolean = False, _
        Optional ByVal bRPerfUON As Boolean, Optional ByVal lIdPerf As Long)
    Dim sConsulta As String
    Dim sWhere As String
    Dim rs As New adodb.Recordset
    Dim adoComm As adodb.Command
    Dim adoParam As adodb.Parameter
    Dim fldCod As adodb.Field
    Dim fldDen As adodb.Field
    Dim lIndice As Long
    Dim oDep As CDepartamento
    Dim sUON As String
    Dim sParamDef As String
    Dim sParamVal As String
    Dim i As Integer
    
    '********* Precondicion **************************************
    If mvarConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CargarTodosLosDepartamentos", "No se ha establecido la conexion"
        Exit Sub
    End If
    '*************************************************************
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
    Set adoComm = New adodb.Command
    Set adoComm.ActiveConnection = mvarConexion.ADOCon
        
    sConsulta = "SELECT DISTINCT COD,DEN"
    sConsulta = sConsulta & " FROM DEP WITH (NOLOCK)"
    
    If sUON1 <> "" And sUON2 <> "" And sUON3 <> "" Then
        sUON = ConstruirCriterioUONUsuPerfUONDepPorUON("UON3_DEP", True, bRPerfUON, lIdPerf, sUON1, sUON2, sUON3)
        sConsulta = sConsulta & " INNER JOIN (" & sUON & ") UD3 ON DEP.COD=UD3.DEP "
    ElseIf sUON1 <> "" And sUON2 <> "" And sUON3 = "" Then
        sUON = ConstruirCriterioUONUsuPerfUONDepPorUON("UON2_DEP", True, bRPerfUON, lIdPerf, sUON1, sUON2)
        sConsulta = sConsulta & " INNER JOIN (" & sUON & ") UD2 ON DEP.COD=UD2.DEP "
    ElseIf sUON1 <> "" And sUON2 = "" And sUON3 = "" Then
        sUON = ConstruirCriterioUONUsuPerfUONDepPorUON("UON1_DEP", True, bRPerfUON, lIdPerf, sUON1)
        sConsulta = sConsulta & " INNER JOIN (" & sUON & ") UD1 ON DEP.COD=UD1.DEP "
    End If
    
    sWhere = " WHERE 1=1"
    If sUON1 <> "" Or sUON2 <> "" Or sUON3 <> "" Then
        If sUON1 <> "" And sUON2 <> "" And sUON3 <> "" Then
            If Not IncluirBajaLogica Then sWhere = sWhere & " AND UD3.BAJALOG=0"
            
            sParamDef = "@UON1 VARCHAR(50),@UON2 VARCHAR(50),@UON3 VARCHAR(50)"
            sParamVal = "?,?,?"
    
            Set adoParam = adoComm.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(sUON1) + 1, Value:=sUON1)
            adoComm.Parameters.Append adoParam
            Set adoParam = adoComm.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(sUON2) + 1, Value:=sUON2)
            adoComm.Parameters.Append adoParam
            Set adoParam = adoComm.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(sUON3) + 1, Value:=sUON3)
            adoComm.Parameters.Append adoParam
        ElseIf sUON1 <> "" And sUON2 <> "" And sUON3 = "" Then
            If Not IncluirBajaLogica Then sWhere = sWhere & " AND UD2.BAJALOG=0"
           
            sParamDef = "@UON1 VARCHAR(50),@UON2 VARCHAR(50)"
            sParamVal = "?,?"
            
            Set adoParam = adoComm.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(sUON1) + 1, Value:=sUON1)
            adoComm.Parameters.Append adoParam
            Set adoParam = adoComm.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(sUON2) + 1, Value:=sUON2)
            adoComm.Parameters.Append adoParam
        Else
            If Not IncluirBajaLogica Then sWhere = sWhere & " AND UD1.BAJALOG=0"
            
            sParamDef = "@UON1 VARCHAR(50)"
            sParamVal = "?"
            
            Set adoParam = adoComm.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(sUON1) + 1, Value:=sUON1)
            adoComm.Parameters.Append adoParam
        End If
    End If
    
    If Len(sCod) > 0 Then
        If CoincidenciaTotal Then
            sWhere = " AND COD=@COD"
            sParamDef = sParamDef & ",@COD VARCHAR(50)"
            sParamVal = sParamVal & ",?"
            
            Set adoParam = adoComm.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(sCod) + 1, Value:=sCod)
            adoComm.Parameters.Append adoParam
        Else
            sWhere = " AND COD LIKE '%" & sCod & "%'"
        End If
    End If
    
    If Len(sDen) > 0 Then
        If CoincidenciaTotal Then
            sWhere = sWhere & " AND DEN=@DEN"
            Set adoParam = adoComm.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(sDen) + 1, Value:=sDen)
            adoComm.Parameters.Append adoParam
        Else
            sWhere = sWhere & " AND DEN LIKE '%" & sDen & "%'"
        End If
    End If
    
    If OrdenadosPorDen Then
        sConsulta = sConsulta & sWhere & " ORDER BY DEN"
    Else
        sConsulta = sConsulta & sWhere & " ORDER BY COD"
    End If
    
    sConsulta = "EXEC SP_EXECUTESQL N'" & sConsulta & "',N'" & sParamDef & "'," & sParamVal
        
    adoComm.CommandText = sConsulta
    adoComm.CommandType = adCmdText
    adoComm.Prepared = True
    Set rs = adoComm.Execute
    
    Set mCol = Nothing
    Set mCol = New Collection
        
    If Not rs.eof Then
        Set fldCod = rs.Fields(0)
        Set fldDen = rs.Fields(1)
                
        If UsarIndice Then
            lIndice = 0
            
            Set oDep = Nothing
            
            While Not rs.eof
                Set oDep = New CDepartamento
                oDep.Cod = fldCod.Value
                oDep.Den = fldDen.Value
                oDep.Indice = lIndice
                Set oDep.Conexion = mvarConexion
                Set oDep.ConexionServer = mvarConexionServer
                
                mCol.Add oDep, CStr(lIndice)
                rs.MoveNext
                lIndice = lIndice + 1
                Set oDep = Nothing
            Wend
        Else
             While Not rs.eof
                Set oDep = New CDepartamento
                oDep.Cod = fldCod.Value
                oDep.Den = fldDen.Value
                oDep.Indice = lIndice
                Set oDep.Conexion = mvarConexion
                Set oDep.ConexionServer = mvarConexionServer
                
                mCol.Add oDep, CStr(fldCod.Value)
                rs.MoveNext
                Set oDep = Nothing
            Wend
        End If
        
        Set fldCod = Nothing
        Set fldDen = Nothing
    End If
    
    rs.Close
    Set rs = Nothing
    Set adoParam = Nothing
    Set adoComm = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CDepartamentos", "CargarTodosLosDepartamentosPorUON", ERR, Erl)
        Exit Sub
    End If
End Sub

''' <summary>
''' Carga la colecci�n interna de objetos CEmpresa con los elementos que satisfacen las condiciones establecidas
''' en los par�metros.
''' </summary>
''' <param name="CarIniCod">C�digo de la empresa</param>
''' <param name="CarIniDen">Denominaci�n de la empresa</param>
''' <param name="CoincidenciaTotal">Indica si los filtros por c�digo y denominaci�n, se van a realizar con un like o con un operador de igualdad</param>
''' <param name="OrdenadosPorDen">Indica si los elementos de la colecci�n se va a ordenar por denominaci�n</param>
''' <param name="UsarIndice">Indica si la colecci�m interna se va a indexar</param>
''' <param name="BajaLogica">Indica si en la colecci�n se incluyen los registros dados de baja l�gica</param>
''' <param name="iPYME">C�digo de la empresa PYME a la que pertence el usuario actual</param>
''' <param name="bRPerfUON">Restringir a las UON del perfil del usuario</param>
''' <param name="lIdPerf">Id perfil</param>
''' <remarks>Tiempo m�ximo: 0 seg </remarks>
''' <revision>LTG 04/01/2012</revision>

Public Sub CargarTodosLosDepartamentos(Optional ByVal CarIniCod As String, Optional ByVal CarIniDen As String, Optional ByVal CoincidenciaTotal As Boolean, _
        Optional ByVal OrdenadosPorDen As Boolean = False, Optional ByVal UsarIndice As Boolean = False, Optional ByVal BajaLogica As Boolean = False, _
        Optional ByVal iPyme As Integer, Optional ByVal bRPerfUON As Boolean, Optional ByVal lIdPerf As Long)
    Dim rs As New adodb.Recordset
    Dim fldCod As adodb.Field
    Dim fldDen As adodb.Field
    Dim sConsulta As String
    Dim sConsulta1 As String
    Dim sConsulta2 As String
    Dim sConsulta3 As String
    Dim sConsulta4 As String
    Dim sConsultaWhere As String
    Dim sConsultaOrder As String
    Dim sCad As String
    Dim lIndice As Long
    Dim oDep As CDepartamento
    Dim sUON As String
    
    '********* Precondicion **************************************
    If mvarConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CUnidadeOrgNivel0CargarTodosLosDep", "No se ha establecido la conexion"
        Exit Sub
    End If
    '*************************************************************
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If CarIniCod <> "" Then
        sCad = "1"
    Else
        sCad = "0"
    End If
    
    If CarIniDen <> "" Then
        sCad = sCad & "1"
    Else
        sCad = sCad & "0"
    End If
    
    sConsulta1 = "SELECT DISTINCT DEP.COD,DEP.DEN FROM DEP WITH (NOLOCK) "
    sConsulta1 = sConsulta1 & " INNER JOIN UON0_DEP UOND WITH (NOLOCK) ON DEP.COD = UOND.DEP "
    If Not BajaLogica Then
        sConsulta1 = sConsulta1 & " AND UOND.BAJALOG=0 "
    End If
    
    sConsulta2 = "SELECT DISTINCT DEP.COD,DEP.DEN FROM DEP WITH (NOLOCK) "
    If bRPerfUON Then
        sUON = ConstruirCriterioUONUsuPerfUONDep("UON1_DEP", 1, False, bRPerfUON, lIdPerf)
        sConsulta2 = sConsulta2 & " INNER JOIN (" & sUON & ") UOND ON DEP.COD = UOND.DEP "
    Else
        sConsulta2 = sConsulta2 & " INNER JOIN UON1_DEP UOND WITH (NOLOCK) ON DEP.COD = UOND.DEP "
    End If
    If Not BajaLogica Then
        sConsulta2 = sConsulta2 & " AND UOND.BAJALOG=0 "
    End If
    If iPyme <> 0 Then
        sConsulta2 = sConsulta2 & " INNER JOIN UON1 WITH (NOLOCK) ON UOND.UON1 = UON1.COD AND UON1.PYME=" & iPyme
    End If
    
    sConsulta3 = "SELECT DISTINCT DEP.COD,DEP.DEN FROM DEP WITH (NOLOCK) "
    If bRPerfUON Then
        sUON = ConstruirCriterioUONUsuPerfUONDep("UON2_DEP", 2, False, bRPerfUON, lIdPerf)
        sConsulta3 = sConsulta3 & " INNER JOIN (" & sUON & ") UOND ON DEP.COD = UOND.DEP "
    Else
        sConsulta3 = sConsulta3 & " INNER JOIN UON2_DEP UOND WITH (NOLOCK) ON DEP.COD = UOND.DEP "
    End If
    If Not BajaLogica Then
        sConsulta3 = sConsulta3 & " AND UOND.BAJALOG=0 "
    End If
    If iPyme <> 0 Then
        sConsulta3 = sConsulta3 & " INNER JOIN UON1 WITH (NOLOCK) ON UOND.UON1 = UON1.COD AND UON1.PYME=" & iPyme
    End If
    
    sConsulta4 = "SELECT DISTINCT DEP.COD,DEP.DEN FROM DEP WITH (NOLOCK) "
    If bRPerfUON Then
        sUON = ConstruirCriterioUONUsuPerfUONDep("UON3_DEP", 3, False, bRPerfUON, lIdPerf)
        sConsulta4 = sConsulta4 & " INNER JOIN (" & sUON & ") UOND ON DEP.COD = UOND.DEP "
    Else
        sConsulta4 = sConsulta4 & " INNER JOIN UON3_DEP UOND WITH (NOLOCK) ON DEP.COD = UOND.DEP "
    End If
    If Not BajaLogica Then
        sConsulta4 = sConsulta4 & " AND UOND.BAJALOG=0 "
    End If
    If iPyme <> 0 Then
        sConsulta4 = sConsulta4 & " INNER JOIN UON1 WITH (NOLOCK) ON UOND.UON1 = UON1.COD AND UON1.PYME=" & iPyme
    End If
    
    sConsulta = sConsulta & " WHERE 1=1"
    
    Select Case sCad
        Case "01"
            If CoincidenciaTotal Then
                sConsultaWhere = sConsultaWhere & " AND DEP.DEN = '" & DblQuote(CarIniDen) & "'"
            Else
                sConsultaWhere = sConsultaWhere & " AND DEP.DEN LIKE '" & DblQuote(CarIniDen) & "%'"
            End If
                      
        Case "10"
            If CoincidenciaTotal Then
                sConsultaWhere = sConsultaWhere & " AND DEP.COD = '" & DblQuote(CarIniCod) & "'"
            Else
                sConsultaWhere = sConsultaWhere & " AND DEP.COD LIKE '" & DblQuote(CarIniCod) & "%'"
            End If
                   
        Case "11"
            If CoincidenciaTotal Then
                sConsultaWhere = sConsultaWhere & " AND DEP.COD = '" & DblQuote(CarIniCod) & "'"
                sConsultaWhere = sConsultaWhere & "AND DEP.DEN = '" & DblQuote(CarIniDen) & "'"
            Else
                sConsulta = sConsultaWhere & " AND DEP.COD LIKE '" & DblQuote(CarIniCod) & "%'"
                sConsulta = sConsultaWhere & " AND DEP.DEN LIKE '" & DblQuote(CarIniDen) & "%'"
            End If
    End Select
    
    If OrdenadosPorDen Then
        sConsultaOrder = sConsultaOrder & " ORDER BY DEP.DEN"
    Else
        sConsultaOrder = sConsultaOrder & " ORDER BY DEP.COD"
    End If
          
    sConsulta = sConsulta1 & sConsultaWhere
    sConsulta = sConsulta & " union " & sConsulta2 & sConsultaWhere
    sConsulta = sConsulta & " union " & sConsulta3 & sConsultaWhere
    sConsulta = sConsulta & " union " & sConsulta4 & sConsultaWhere & sConsultaOrder
    
    rs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
       
    Set mCol = Nothing
    Set mCol = New Collection
    
    If Not rs.eof Then
        Set fldCod = rs.Fields(0)
        Set fldDen = rs.Fields(1)
            
        If UsarIndice Then
            lIndice = 0
            
            Set oDep = Nothing
            
            While Not rs.eof
                Set oDep = New CDepartamento
                oDep.Cod = fldCod.Value
                oDep.Den = fldDen.Value
                oDep.Indice = lIndice
                Set oDep.Conexion = mvarConexion
                Set oDep.ConexionServer = mvarConexionServer
                
                mCol.Add oDep, CStr(lIndice)
                rs.MoveNext
                lIndice = lIndice + 1
                Set oDep = Nothing
            Wend
        Else
             While Not rs.eof
                Set oDep = New CDepartamento
                oDep.Cod = fldCod.Value
                oDep.Den = fldDen.Value
                oDep.Indice = lIndice
                Set oDep.Conexion = mvarConexion
                Set oDep.ConexionServer = mvarConexionServer
                
                mCol.Add oDep, CStr(fldCod.Value)
                rs.MoveNext
                Set oDep = Nothing
            Wend
        End If
        
        Set fldCod = Nothing
        Set fldDen = Nothing
    End If
    
    rs.Close
    Set rs = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CDepartamentos", "CargarTodosLosDepartamentos", ERR, Erl)
        Exit Sub
    End If
End Sub

''' <summary>Carga la colecci�n interna de objetos con los elementos que satisfacen las condiciones</summary>
''' <param name="NumMaximo">N� m�x. de registros a cargar</param>
''' <param name="CaracteresInicialesCod">C�digo del departamento</param>
''' <param name="CaracteresInicialesDen">Denominaci�n del departamento</param>
''' <param name="OrdenadosPorDen">Indica si los elementos de la colecci�n se va a ordenar por denominaci�n</param>
''' <param name="UsarIndice">Indica si la colecci�m interna se va a indexar</param>
''' <param name="BajaLogica">Indica si en la colecci�n se incluyen los registros dados de baja l�gica</param>
''' <remarks>Llamada desde: GSClient; Tiempo m�ximo: 0 seg </remarks>
''' <revision>LTG 04/01/2012</revision>

Public Sub CargarTodosLosDepartamentosDesde(ByVal NumMaximo As Integer, Optional ByVal CaracteresInicialesCod As String, _
        Optional ByVal CaracteresInicialesDen As String, Optional ByVal OrdenadosPorDen As Boolean, Optional ByVal UsarIndice As Boolean, _
        Optional ByVal BajaLog As Boolean)
    Dim rs As New adodb.Recordset
    Dim fldCod As adodb.Field
    Dim fldDen As adodb.Field
    Dim sConsulta As String
    Dim lIndice As Long
    '''''''''''''''''''Dim iNumDeps As Integer
    
    '********* Precondicion **************************************
    If mvarConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CDestinos.CargarTodosLosDestinos", "No se ha establecido la conexion"
        Exit Sub
    End If
    '*************************************************************
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
       
    If CaracteresInicialesCod = "" And CaracteresInicialesDen = "" Then
        
        sConsulta = "SELECT TOP " & NumMaximo & " DEP.COD,DEP.DEN FROM DEP WITH (NOLOCK) "
        sConsulta = sConsulta & " inner join uon0_dep u0d WITH (NOLOCK) on u0d.dep=dep.cod "
        If Not BajaLog Then
            sConsulta = sConsulta & " and u0d.bajalog=0 "
        End If
        
        sConsulta = sConsulta & " union "
        
        sConsulta = sConsulta & "SELECT TOP " & NumMaximo & " DEP.COD,DEP.DEN FROM DEP WITH (NOLOCK) "
        sConsulta = sConsulta & " inner join uon1_dep u1d WITH (NOLOCK) on u1d.dep=dep.cod "
        If Not BajaLog Then
            sConsulta = sConsulta & " and u1d.bajalog=0 "
        End If
        
        sConsulta = sConsulta & " union "
        
        sConsulta = sConsulta & "SELECT TOP " & NumMaximo & " DEP.COD,DEP.DEN FROM DEP WITH (NOLOCK) "
        sConsulta = sConsulta & " inner join uon2_dep u2d WITH (NOLOCK) on u2d.dep=dep.cod "
        If Not BajaLog Then
            sConsulta = sConsulta & " and u2d.bajalog=0 "
        End If
        
        sConsulta = sConsulta & " union "
        
        sConsulta = sConsulta & "SELECT TOP " & NumMaximo & " DEP.COD,DEP.DEN FROM DEP WITH (NOLOCK) "
        sConsulta = sConsulta & " inner join uon3_dep u3d WITH (NOLOCK) on u3d.dep=dep.cod "
        If Not BajaLog Then
            sConsulta = sConsulta & " and u3d.bajalog=0 "
        End If
        
    
    Else
        
        sConsulta = "SELECT TOP " & NumMaximo & " DEP.COD,DEP.DEN FROM DEP WITH (NOLOCK) "
        
        sConsulta = sConsulta & " inner join uon0_dep u0d WITH (NOLOCK) on u0d.dep=dep.cod "
        If Not BajaLog Then
            sConsulta = sConsulta & " and u0d.bajalog=0 "
        End If
        sConsulta = sConsulta & " WHERE "
        If Not (CaracteresInicialesCod = "") And Not (CaracteresInicialesDen = "") Then
                
                sConsulta = sConsulta & " COD >='" & DblQuote(CaracteresInicialesCod) & "'"
                sConsulta = sConsulta & " AND DEN >='" & DblQuote(CaracteresInicialesDen) & "'"
                        
        Else
            
            If Not (CaracteresInicialesCod = "") Then
                    sConsulta = sConsulta & " COD >='" & DblQuote(CaracteresInicialesCod) & "'"
            Else
                    sConsulta = sConsulta & " DEN >='" & DblQuote(CaracteresInicialesDen) & "'"
            End If
        
        End If
        
        sConsulta = sConsulta & " union "
        
        sConsulta = sConsulta & "SELECT TOP " & NumMaximo & " DEP.COD,DEP.DEN FROM DEP WITH (NOLOCK) "
      
        sConsulta = sConsulta & " inner join uon1_dep u1d WITH (NOLOCK) on u1d.dep=dep.cod "
        If Not BajaLog Then
            sConsulta = sConsulta & " and u1d.bajalog=0 "
        End If
        sConsulta = sConsulta & " WHERE "
        If Not (CaracteresInicialesCod = "") And Not (CaracteresInicialesDen = "") Then
                
                sConsulta = sConsulta & " COD >='" & DblQuote(CaracteresInicialesCod) & "'"
                sConsulta = sConsulta & " AND DEN >='" & DblQuote(CaracteresInicialesDen) & "'"
                        
        Else
            
            If Not (CaracteresInicialesCod = "") Then
                    sConsulta = sConsulta & " COD >='" & DblQuote(CaracteresInicialesCod) & "'"
            Else
                    sConsulta = sConsulta & " DEN >='" & DblQuote(CaracteresInicialesDen) & "'"
            End If
        
        End If
    
        sConsulta = sConsulta & " union "
        
        sConsulta = sConsulta & "SELECT TOP " & NumMaximo & " DEP.COD,DEP.DEN FROM DEP WITH (NOLOCK) "
      
        sConsulta = sConsulta & " inner join uon2_dep u2d WITH (NOLOCK) on u2d.dep=dep.cod "
        If Not BajaLog Then
            sConsulta = sConsulta & " and u2d.bajalog=0 "
        End If
        sConsulta = sConsulta & " WHERE "
        If Not (CaracteresInicialesCod = "") And Not (CaracteresInicialesDen = "") Then
                
                sConsulta = sConsulta & " COD >='" & DblQuote(CaracteresInicialesCod) & "'"
                sConsulta = sConsulta & " AND DEN >='" & DblQuote(CaracteresInicialesDen) & "'"
                        
        Else
            
            If Not (CaracteresInicialesCod = "") Then
                    sConsulta = sConsulta & " COD >='" & DblQuote(CaracteresInicialesCod) & "'"
            Else
                    sConsulta = sConsulta & " DEN >='" & DblQuote(CaracteresInicialesDen) & "'"
            End If
            
        End If
        
            sConsulta = sConsulta & " union "
        
        sConsulta = sConsulta & "SELECT TOP " & NumMaximo & " DEP.COD,DEP.DEN FROM DEP WITH (NOLOCK) "
      
        sConsulta = sConsulta & " inner join uon3_dep u3d WITH (NOLOCK) on u3d.dep=dep.cod "
        If Not BajaLog Then
            sConsulta = sConsulta & " and u3d.bajalog=0 "
        End If
        sConsulta = sConsulta & " WHERE "
        If Not (CaracteresInicialesCod = "") And Not (CaracteresInicialesDen = "") Then
                
                sConsulta = sConsulta & " COD >='" & DblQuote(CaracteresInicialesCod) & "'"
                sConsulta = sConsulta & " AND DEN >='" & DblQuote(CaracteresInicialesDen) & "'"
                        
        Else
            
            If Not (CaracteresInicialesCod = "") Then
                    sConsulta = sConsulta & " COD >='" & DblQuote(CaracteresInicialesCod) & "'"
            Else
                    sConsulta = sConsulta & " DEN >='" & DblQuote(CaracteresInicialesDen) & "'"
            End If
        
        End If
    
    
               
    End If
    
    If OrdenadosPorDen Then
        sConsulta = sConsulta & " ORDER BY DEN,COD"
    Else
        sConsulta = sConsulta & " ORDER BY COD,DEN"
    End If
          
    rs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
          
    If rs.eof Then
            
        rs.Close
        Set rs = Nothing
        Exit Sub
          
    Else
        
        Set fldCod = rs.Fields(0)
        Set fldDen = rs.Fields(1)
        
        Set mCol = Nothing
        Set mCol = New Collection
            
        If UsarIndice Then
            
            lIndice = 0
            '''''''''''''''''iNumDeps = 0
                    
            While Not rs.eof ''''''''''''''And iNumDeps < NumMaximo
                ' Dejamos en manos del programador cuando asignar la conexion al obejto usuario
                Me.Add fldCod.Value, fldDen.Value, lIndice
                rs.MoveNext
                lIndice = lIndice + 1
                '''''''''''''''''''''iNumDeps = iNumDeps + 1
            Wend
            
            If Not rs.eof Then
                mvarEOF = False
            Else
                mvarEOF = True
            End If
            
            
                
        Else
            
            '''''''''''''''iNumDeps = 0
            
            While Not rs.eof ''''''''''''''''''''And iNumDeps < NumMaximo
                ' Dejamos en manos del programador cuando asignar la conexion al obejto usuario
                Me.Add fldCod.Value, fldDen.Value
                rs.MoveNext
                ''''''''''''''''''''''''iNumDeps = iNumDeps + 1
            Wend
            
            If Not rs.eof Then
                mvarEOF = False
            Else
                mvarEOF = True
            End If
            
        End If
        
        Set fldCod = Nothing
        Set fldDen = Nothing
    
        rs.Close
        Set rs = Nothing
    
        Exit Sub
          
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CDepartamentos", "CargarTodosLosDepartamentosDesde", ERR, Erl)
        Exit Sub
    End If
End Sub
Public Function DevolverLosCodigos() As TipoDatosCombo
   
   Dim Codigos As TipoDatosCombo
   Dim iCont As Integer
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
   ReDim Codigos.Cod(mCol.Count)
   ReDim Codigos.Den(mCol.Count)
   
    For iCont = 0 To mCol.Count - 1
        
        Codigos.Cod(iCont) = mCol(iCont + 1).Cod
        Codigos.Den(iCont) = mCol(iCont + 1).Den
    
    Next

   DevolverLosCodigos = Codigos
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CDepartamentos", "DevolverLosCodigos", ERR, Erl)
        Exit Function
    End If
   
End Function

Public Function EstadeBajaDep(ByVal sCod As String) As Boolean

Dim rs As New adodb.Recordset
Dim sConsulta As String
Dim sConsulta1 As String
Dim sConsulta2 As String
Dim sConsulta3 As String
Dim sConsulta4 As String
'********* Precondicion **************************************
If mvarConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CDepartamentosEstadeBajaDep", "No se ha establecido la conexion"
    Exit Function
End If
'*************************************************************

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
EstadeBajaDep = True


    sConsulta1 = "SELECT UON0D.BAJALOG FROM DEP WITH (NOLOCK) "
    sConsulta1 = sConsulta1 & " INNER JOIN UON0_DEP UON0D WITH (NOLOCK) ON DEP.COD = UON0D.DEP "
    sConsulta1 = sConsulta1 & " WHERE DEP.COD = '" & DblQuote(sCod) & "'"
    
    sConsulta2 = "SELECT UON1D.BAJALOG FROM DEP WITH (NOLOCK) "
    sConsulta2 = sConsulta2 & " INNER JOIN UON1_DEP UON1D WITH (NOLOCK) ON DEP.COD = UON1D.DEP "
    sConsulta2 = sConsulta2 & " WHERE DEP.COD = '" & DblQuote(sCod) & "'"
    
    sConsulta3 = "SELECT UON2D.BAJALOG FROM DEP WITH (NOLOCK) "
    sConsulta3 = sConsulta3 & " INNER JOIN UON2_DEP UON2D WITH (NOLOCK) ON DEP.COD = UON2D.DEP "
    sConsulta3 = sConsulta3 & " WHERE DEP.COD = '" & DblQuote(sCod) & "'"


    sConsulta4 = "SELECT UON3D.BAJALOG FROM DEP WITH (NOLOCK) "
    sConsulta4 = sConsulta4 & " INNER JOIN UON3_DEP UON3D WITH (NOLOCK) ON DEP.COD = UON3D.DEP "
    sConsulta4 = sConsulta4 & " WHERE DEP.COD = '" & DblQuote(sCod) & "'"
      
sConsulta = sConsulta1 & " union " & sConsulta2 & " union " & sConsulta3 & " union " & sConsulta4

rs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
      
If rs.eof Then
        
    rs.Close
    Set rs = Nothing
    EstadeBajaDep = False
    Exit Function
      
Else
                    
    While Not rs.eof
        If rs.Fields(0) = 0 Then
            EstadeBajaDep = False
        End If
        rs.MoveNext
    Wend
    
    rs.Close
    Set rs = Nothing
    Exit Function
      
End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CDepartamentos", "EstadeBajaDep", ERR, Erl)
        Exit Function
    End If
End Function



