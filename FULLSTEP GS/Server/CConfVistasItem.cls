VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CConfVistasItem"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CConfVistasItem **********************************
'*             Autor : Elena Mu�oz
'*             Creada : 08/07/2002
'****************************************************************

Option Explicit

Private mCol As Collection
Private m_oConexion As CConexion
Private m_oConexionServer As CConexionDeUseServer
Private m_bEOF As Boolean

Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum


Friend Property Set Conexion(ByVal con As CConexion)
    Set m_oConexion = con
End Property

Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property

Friend Property Set ConexionServer(ByVal con As CConexionDeUseServer)
    Set m_oConexionServer = con
End Property

Friend Property Get ConexionServer() As CConexionDeUseServer
    Set ConexionServer = m_oConexionServer
End Property

Public Property Get Count() As Long
    If mCol Is Nothing Then
        Count = 0
    Else
         Count = mCol.Count
    End If
End Property

Public Property Get eof() As Boolean
    eof = m_bEOF
End Property

Friend Property Let eof(ByVal b As Boolean)
    m_bEOF = b
End Property

Public Property Get Item(vntIndexKey As Variant) As CConfVistaItem
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
     Set NewEnum = mCol.[_NewEnum]
End Property

Public Sub Remove(vntIndexKey As Variant)
On Error GoTo ERROR

    mCol.Remove vntIndexKey

ERROR:

End Sub

Private Sub Class_Initialize()
    Set mCol = New Collection
End Sub

Private Sub Class_Terminate()
    'Proceroys collection when this class is terminated
    On Error Resume Next
    Set mCol = Nothing
    Set m_oConexion = Nothing
End Sub

''' <summary>A�ade un elemento a la colecci�n</summary>
''' <param name="oGrupo">Objeto grupo</param>
''' <param name="iVista">Vista</param>
''' <param name="vIndice">Indice</param>
''' <param name="iPrecioPos">posici�n columna precio</param>
''' <param name="bPrecioVisible">visibilidad columna precio</param>
''' <param name="dblPrecioWidth">anchura columna precio</param>
''' <param name="iCantAdjPos">posici�n columna cant. adjudicada</param>
''' <param name="bCantAdjVisible">visibilidad columna cant. adjudicada</param>
''' <param name="dblCantAdjWidth">anchura columna cant. adjudicada</param>
''' <param name="iCantMaxPos">posici�n columna cantidad max.</param>
''' <param name="bCantMaxVisible">visibilidad columna cant. max.</param>
''' <param name="dblCantMaxWidth">anchura columna cant. max.</param>
''' <param name="iAhorroPorcenPos">posici�n columna porcen. ahorro</param>
''' <param name="bAhorroPorcenVisible">visibilidad columna porcen. ahorro</param>
''' <param name="dblAhorroPorcenWidth">anchura columna porcen.ahorro</param>
''' <param name="iAhorroPos">posici�n columna ahorro</param>
''' <param name="bAhorroVisible">visibilidad columna ahorro</param>
''' <param name="dblAhorroWidth">anchura columna ahorro</param>
''' <param name="iAdjudPos">posici�n columna adjudicado</param>
''' <param name="bAdjudVisible">visibilidad columna adjudicado</param>
''' <param name="dblAdjudWidth">anchura columna adjudicado</param>
''' <param name="iConsumPos">posici�n columna consumido</param>
''' <param name="bConsumVisible">visibilidad columna consumido</param>
''' <param name="dblConsumWidth">anchura columna consumido</param>
''' <param name="dblProveDescrWidth">anchura columna descr. prove.</param>
''' <param name="sUsuario">usuario</param>
''' <param name="bOcultarNoAdj">Ocultar proveedores no adjudicados</param>
''' <param name="bOcultarProvSinOfe">Ocultar proveedores sin ofertas</param>
''' <param name="iPondPos">posici�n columna pond.</param>
''' <param name="bPondVisible">visibilidad columna pond.</param>
''' <param name="dblPondWidth">anchura columna pond.</param>
''' <param name="dblAnchoFila">ancho de las filas del grid</param>
''' <param name="iDecResult">decimales result.</param>
''' <param name="iDecPorcen">decimales porcentajes</param>
''' <param name="iDecPrecios">decimales precios</param>
''' <param name="iDecCant">decimales cantidades</param>
''' <param name="iImporteOfePos">posici�n columna importe ofe.</param>
''' <param name="bImporteOfeVisible">visibilidad columna importe ofe.</param>
''' <param name="dblImporteOfeWidth">anchura columna importe ofe.</param>
''' <param name="iAhorroOfePos">posici�n columna ahorro ofe.</param>
''' <param name="bAhorroOfeVisible">visibilidad columna ahorro ofe.</param>
''' <param name="dblAhorroOfeWidth">anchura columna ahorro ofe.</param>
''' <param name="iAhorroPorcenOfePos">posici�n columna porcen. ofe.</param>
''' <param name="bAhorroPorcenOfeVisible">visibilidad columna ahorro porcen. ofe.</param>
''' <param name="dblAhorroPorcenOfeWidth">anchura columna ahorro porcen. ofe.</param>
''' <param name="udtTipoVista">tipo vista</param>
''' <param name="sUsuarioVista">usuario vista</param>
''' <param name="sUsuarioNombre">nombre usuario</param>
''' <param name="vGruposEscWidth">Ancho de los grupos de escalados</param>
''' <param name="dblGrupo0Width">Ancho Grupo 0</param>
''' <param name="iPrecioEscPos">Pos. precio esc.</param>
''' <param name="bPrecioEscVisible">precio esc. visible</param>
''' <param name="dblPrecioEscWidth">Ancho precio esc.</param>
''' <param name="iAhorroEscPos">Pos. ahorro esc.</param>
''' <param name="bAhorroEscVisible">Ahorro esc. visible</param>
''' <param name="dblAhorroEscWidth">Ancho ahorro esc.</param>
''' <param name="iAdjEscPos">Posici�n adj. esc.</param>
''' <param name="bAdjEscVisible">adj. esc. visible</param>
''' <param name="dblAdjEscWidth">Ancho adj. esc.</param>
''' <remarks>Llamada desde: Cgrupo.CargarTodasLasConfVistasItem; Tiempo m�ximo<1 seg</remarks>
''' <revision>LTG 27/10/2011</revision>

Public Function Add(ByVal oGrupo As CGrupo, ByVal iVista As Integer, Optional ByVal vIndice As Variant, _
        Optional ByVal iPrecioPos As Integer, Optional ByVal bPrecioVisible As Boolean, Optional ByVal dblPrecioWidth As Double, _
        Optional ByVal iCantAdjPos As Integer, Optional ByVal bCantAdjVisible As Boolean, Optional ByVal dblCantAdjWidth As Double, _
        Optional ByVal iCantMaxPos As Integer, Optional ByVal bCantMaxVisible As Boolean, Optional ByVal dblCantMaxWidth As Double, _
        Optional ByVal iAhorroPorcenPos As Integer, Optional ByVal bAhorroPorcenVisible As Boolean, Optional ByVal dblAhorroPorcenWidth As Double, _
        Optional ByVal iAhorroPos As Integer, Optional ByVal bAhorroVisible As Boolean, Optional ByVal dblAhorroWidth As Double, _
        Optional ByVal iAdjudPos As Integer, Optional ByVal bAdjudVisible As Boolean, Optional ByVal dblAdjudWidth As Double, _
        Optional ByVal iConsumPos As Integer, Optional ByVal bConsumVisible As Boolean, Optional ByVal dblConsumWidth As Double, _
        Optional ByVal dblProveDescrWidth As Double, Optional ByVal sUsuario As String, _
        Optional ByVal bOcultarNoAdj As Boolean, Optional ByVal bOcultarProvSinOfe As Boolean, _
        Optional ByVal iPondPos As Integer, Optional ByVal bPondVisible As Boolean, Optional ByVal dblPondWidth As Double, _
        Optional ByVal dblAnchoFila As Double, Optional ByVal iDecResult As Integer, Optional ByVal iDecPorcen As Integer, _
        Optional ByVal iDecPrecios As Integer, Optional ByVal iDecCant As Integer, _
        Optional ByVal iImporteOfePos As Variant, Optional ByVal bImporteOfeVisible As Boolean, Optional ByVal dblImporteOfeWidth As Double, _
        Optional ByVal iAhorroOfePos As Variant, Optional ByVal bAhorroOfeVisible As Boolean, Optional ByVal dblAhorroOfeWidth As Double, _
        Optional ByVal iAhorroPorcenOfePos As Variant, Optional ByVal bAhorroPorcenOfeVisible As Boolean, Optional ByVal dblAhorroPorcenOfeWidth As Double, _
        Optional ByVal udtTipoVista As TipoDeVistaDefecto, Optional ByVal sUsuarioVista As String, Optional sUsuarioNombre As Variant, _
        Optional ByVal dblGruposEscWidth As Double, Optional ByVal dblGrupo0Width As Double, Optional ByVal iPrecioEscPos As Integer, _
        Optional ByVal bPrecioEscVisible As Boolean, Optional ByVal dblPrecioEscWidth As Double, Optional ByVal iAhorroEscPos As Integer, _
        Optional ByVal bAhorroEscVisible As Boolean, Optional ByVal dblAhorroEscWidth As Double, Optional ByVal iAdjEscPos As Integer, _
        Optional ByVal bAdjEscVisible As Boolean, Optional ByVal dblAdjEscWidth As Double) As CConfVistaItem
    
    'create a new object
    Dim sCodGmn1 As String
    Dim objnewmember As CConfVistaItem
    Dim sCod As String
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New CConfVistaItem
   
    Set objnewmember.Grupo = oGrupo

    objnewmember.ProveDescrWidth = dblProveDescrWidth
    
    objnewmember.PrecioPos = iPrecioPos
    objnewmember.PrecioVisible = bPrecioVisible
    objnewmember.PrecioWidth = dblPrecioWidth
    
    objnewmember.CantAdjPos = iCantAdjPos
    objnewmember.CantAdjVisible = bCantAdjVisible
    objnewmember.CantAdjWidth = dblCantAdjWidth
    
    objnewmember.CantMaxPos = iCantMaxPos
    objnewmember.CantMaxVisible = bCantMaxVisible
    objnewmember.CantMaxWidth = dblCantMaxWidth
    
    objnewmember.AhorroPorcenPos = iAhorroPorcenPos
    objnewmember.AhorroPorcenVisible = bAhorroPorcenVisible
    objnewmember.AhorroPorcenWidth = dblAhorroPorcenWidth
    
    objnewmember.AhorroPos = iAhorroPos
    objnewmember.AhorroVisible = bAhorroVisible
    objnewmember.AhorroWidth = dblAhorroWidth
    
    objnewmember.AdjudPos = iAdjudPos
    objnewmember.AdjudVisible = bAdjudVisible
    objnewmember.AdjudWidth = dblAdjudWidth
        
    objnewmember.ConsumPos = iConsumPos
    objnewmember.ConsumVisible = bConsumVisible
    objnewmember.ConsumWidth = dblConsumWidth
    
    objnewmember.OcultarNoAdj = bOcultarNoAdj
    objnewmember.OcultarProvSinOfe = bOcultarProvSinOfe
    
    objnewmember.PondPos = iPondPos
    objnewmember.PondVisible = bPondVisible
    objnewmember.PondWidth = dblPondWidth
    
    objnewmember.AnchoFila = dblAnchoFila
    objnewmember.DecCant = iDecCant
    objnewmember.DecPorcen = iDecPorcen
    objnewmember.DecPrecios = iDecPrecios
    objnewmember.DecResult = iDecResult
    
    objnewmember.ImporteOfePos = iImporteOfePos
    objnewmember.ImporteOfeVisible = bImporteOfeVisible
    objnewmember.ImporteOfeWidth = dblImporteOfeWidth
    
    objnewmember.AhorroOfePos = iAhorroOfePos
    objnewmember.AhorroOfeVisible = bAhorroOfeVisible
    objnewmember.AhorroOfeWidth = dblAhorroOfeWidth
    
    objnewmember.AhorroOfePorcenPos = iAhorroPorcenOfePos
    objnewmember.AhorroOfePorcenVisible = bAhorroPorcenOfeVisible
    objnewmember.AhorroOfePorcenWidth = dblAhorroPorcenOfeWidth
    
    objnewmember.GruposEscWidth = dblGruposEscWidth
    objnewmember.Grupo0Width = dblGrupo0Width
    objnewmember.PrecioEscPos = iPrecioEscPos
    objnewmember.PrecioEscVisible = bPrecioEscVisible
    objnewmember.PrecioEscWidth = dblPrecioEscWidth
    objnewmember.AhorroEscPos = iAhorroEscPos
    objnewmember.AhorroEscVisible = bAhorroEscVisible
    objnewmember.AhorroEscWidth = dblAhorroEscWidth
    objnewmember.AdjEscPos = iAdjEscPos
    objnewmember.AdjEscVisible = bAdjEscVisible
    objnewmember.AdjEscWidth = dblAdjEscWidth
    
    objnewmember.Usuario = sUsuario
    objnewmember.Vista = iVista
    objnewmember.TipoVista = udtTipoVista
    objnewmember.UsuarioVista = sUsuarioVista
    objnewmember.UsuarioNombre = sUsuarioNombre
    
    Set objnewmember.Conexion = m_oConexion
    Set objnewmember.ConexionServer = m_oConexionServer
       
    If Not IsMissing(vIndice) And Not IsNull(vIndice) And Not IsEmpty(vIndice) Then
        objnewmember.Indice = vIndice
        mCol.Add objnewmember, CStr(vIndice)
    Else
       sCod = sUsuarioVista & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUSU - Len(sUsuarioVista))
       mCol.Add objnewmember, CStr(iVista) & CStr(udtTipoVista) & sCod
    End If
    
    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CConfVistasItem", "Add", ERR, Erl)
      Exit Function
   End If

End Function

