Attribute VB_Name = "basAtributos"
Option Explicit

''' <summary>Aplica un atributo</summary>
''' <param name="sOp">Operación</param>
''' <param name="dblImporte">Importe al que aplicar el atributo</param>
''' <param name="vValorNum">Valor numérico del atributo</param>
''' <param name="dCambio">Cambio</param>
''' <remarks>Llamada desde: CProceso.AplicamosAtributo</remarks>
''' <revision>LTG 04/05/2012</revision>

Public Function AplicarAtributo(ByVal sOp As String, ByVal dblImporte As Double, ByVal vValorNum As Variant, Optional ByVal dCambio As Double = 1) As Double
    If Not IsNull(vValorNum) And Not IsEmpty(vValorNum) Then
        'Aplicamos el atributo al precio unitario.
        Select Case sOp
            Case "+"
                dblImporte = dblImporte + (vValorNum / dCambio)
                
            Case "-"
                dblImporte = dblImporte - (vValorNum / dCambio)
                
            Case "/"
                If vValorNum > 0 Then dblImporte = CDec(dblImporte / vValorNum)
                
            Case "*"
                dblImporte = CDec(dblImporte * vValorNum)
                
            Case "+%"
                dblImporte = dblImporte + CDec(dblImporte * CDec(vValorNum / 100))
                
            Case "-%"
                dblImporte = dblImporte - CDec(dblImporte * CDec(vValorNum / 100))
        End Select
    End If
    
    AplicarAtributo = dblImporte
End Function
