VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "IBaseDatos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit
Public Function CambiarCodigo(CodigoNuevo As Variant) As TipoErrorSummit

End Function
Public Function AnyadirABaseDatos() As TipoErrorSummit

End Function

Public Function FinalizarEdicionModificando() As TipoErrorSummit

End Function
Public Function FinalizarEdicionEliminando() As TipoErrorSummit

End Function


Public Function IniciarEdicion(Optional ByVal Bloquear As Boolean, Optional ByVal UsuarioBloqueo As String) As TipoErrorSummit

End Function
Public Function EliminarDeBaseDatos() As TipoErrorSummit

End Function
Public Sub CancelarEdicion()

End Sub
Public Function ComprobarExistenciaEnBaseDatos() As Boolean

End Function
