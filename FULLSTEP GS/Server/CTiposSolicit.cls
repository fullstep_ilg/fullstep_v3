VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CTiposSolicit"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

Private mCol As Collection

Private m_oConexion As CConexion
Private m_bEOF As Boolean

Public Property Get eof() As Boolean
    eof = m_bEOF
End Property
Friend Property Let eof(ByVal b As Boolean)
    m_bEOF = b
End Property

Public Function Add(ByVal Id As Long, ByVal Cod As String, ByVal Den As CMultiidiomas, _
            Optional ByVal FechaActual As Variant, Optional ByVal Tipo As Integer, Optional ByVal vIndice As Variant) As CTipoSolicit
        
    Dim objnewmember As CTipoSolicit
    
    
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New CTipoSolicit
    Set objnewmember.Conexion = m_oConexion
    
    objnewmember.Id = Id
    objnewmember.Cod = Cod
    Set objnewmember.Denominaciones = Den
    
    If IsMissing(FechaActual) Then
        objnewmember.FecAct = Null
    Else
        objnewmember.FecAct = FechaActual
    End If
    
    If IsMissing(Tipo) Then
        objnewmember.Tipo = 0
    Else
        objnewmember.Tipo = Tipo
    End If
    
    If Not IsMissing(vIndice) And Not IsNull(vIndice) Then
        objnewmember.Indice = vIndice
        mCol.Add objnewmember, CStr(vIndice)
    Else
        mCol.Add objnewmember, CStr(Id)
    End If

    Set Add = objnewmember
    
    Set objnewmember = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CTiposSolicit", "Add", ERR, Erl)
      Exit Function
   End If

End Function

Friend Property Set Conexion(ByVal vData As CConexion)
    Set m_oConexion = vData
End Property

Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property

Public Property Get Item(vntIndexKey As Variant) As CTipoSolicit

    ''' * Objetivo: Recuperar un campo de la coleccion
    ''' * Recibe: Indice del campo a recuperar
    ''' * Devuelve: campo correspondiente
        
On Error GoTo NoSeEncuentra:

    ''' Devolvemos el item de la coleccion privada
    
    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:

    Set Item = Nothing
    
End Property

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4

    ''' ! Pendiente de revision, objetivo desconocido
    
    Set NewEnum = mCol.[_NewEnum]

End Property
Public Sub Remove(vntIndexKey As Variant)

    ''' * Objetivo: Eliminar un campo de la coleccion
    ''' * Recibe: Indice del campo a eliminar
    ''' * Devuelve: Nada
   
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    mCol.Remove vntIndexKey
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CTiposSolicit", "Remove", ERR, Erl)
      Exit Sub
   End If
    
End Sub
Private Sub Class_Initialize()

    ''' * Objetivo: Crear la coleccion
    Set mCol = New Collection
    
End Sub
Private Sub Class_Terminate()

    ''' * Objetivo: Limpiar memoria
    
    Set mCol = Nothing
    Set m_oConexion = Nothing
End Sub

Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
     Count = mCol.Count
End If

End Property

Public Sub CargarTodosTiposSolicitud(Optional ByVal CaracteresInicialesCod As String, Optional ByVal CaracteresInicialesDen As String, Optional ByVal UsarIndice As Boolean, Optional ByVal CoincidenciaTotal As Boolean)
    Dim rs As New adodb.Recordset
    Dim sConsulta As String
    Dim fldId As adodb.Field
    Dim fldCod As adodb.Field
    Dim fldFecAct As adodb.Field
    Dim fldTipo As adodb.Field
    Dim lIndice As Long
    Dim oMultiIdis As CMultiidiomas
    Dim oIdi As CIdioma
    Dim oIdiomas As CIdiomas
    Dim oGestorParametros As CGestorParametros
    

    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CTiposSolicit.CargarTodosTiposSolicitud", "No se ha establecido la conexion"
        Exit Sub
    End If
    
    If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
    'Carga todos los idiomas de la aplicaci�n:
    Set oGestorParametros = New CGestorParametros
    Set oGestorParametros.Conexion = m_oConexion
    Set oIdiomas = oGestorParametros.DevolverIdiomas(False, False, True)
    Set oGestorParametros = Nothing
        
    sConsulta = "SELECT * FROM TIPO_SOLICITUDES WITH (NOLOCK)"
    
    If Not (CaracteresInicialesCod = "") Then
        If CoincidenciaTotal Then
            sConsulta = sConsulta & " WHERE COD ='" & DblQuote(CaracteresInicialesCod) & "'"
        Else
            sConsulta = sConsulta & " WHERE COD >='" & DblQuote(CaracteresInicialesCod) & "'"
        End If
    End If
        
        
    If Not (CaracteresInicialesDen = "") Then
        If CoincidenciaTotal Then
            sConsulta = sConsulta & " WHERE DESCR_" & gParametrosInstalacion.gIdioma & " ='" & DblQuote(CaracteresInicialesDen) & "'"
        Else
            sConsulta = sConsulta & " WHERE DESCR_" & gParametrosInstalacion.gIdioma & " >='" & DblQuote(CaracteresInicialesDen) & "'"
        End If
    End If
    
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
          
    If rs.eof Then
        rs.Close
        Set rs = Nothing
        Set mCol = Nothing
        Set mCol = New Collection
        Exit Sub
        
    Else
        Set mCol = Nothing
        Set mCol = New Collection
        
        Set fldId = rs.Fields("ID")
        Set fldCod = rs.Fields("COD")
        Set fldFecAct = rs.Fields("FECACT")
        Set fldTipo = rs.Fields("TIPO")
        
        lIndice = 0
    
        While Not rs.eof
            Set oMultiIdis = Nothing
            Set oMultiIdis = New CMultiidiomas
            Set oMultiIdis.Conexion = m_oConexion
            
            For Each oIdi In oIdiomas
                oMultiIdis.Add oIdi.Cod, rs.Fields("DESCR_" & oIdi.Cod).Value
            Next
        
            If UsarIndice Then
                Me.Add fldId.Value, fldCod.Value, oMultiIdis, fldFecAct.Value, fldTipo.Value, lIndice
            Else
                Me.Add fldId.Value, fldCod.Value, oMultiIdis, fldFecAct.Value, fldTipo.Value
            End If
        
            rs.MoveNext
            
            If UsarIndice Then
                lIndice = lIndice + 1
            End If
        Wend
        
        
        rs.Close
        Set rs = Nothing
        Set fldId = Nothing
        Set fldCod = Nothing
        Set fldFecAct = Nothing
        Set fldTipo = Nothing
        
        Set oIdiomas = Nothing
        Set oMultiIdis = Nothing
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CTiposSolicit", "CargarTodosTiposSolicitud", ERR, Erl)
      Exit Sub
   End If
    
End Sub

Public Function DevolverTiposSolicitudes(Optional ByVal bNoCompras As Boolean = False) As adodb.Recordset
'***************************************************************************************
'Descripci�n:   Devuelve los tipos de campos prefenidos que hay en la tabla TIPO_CAMPO
'Valor que devuelve: Un recordset desconectado
'**************************************************************************************
Dim sConsulta As String
Dim adoRecordset As New adodb.Recordset

'*************************************************************
If m_oConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CTiposSolicit.DevolverTiposSolicitudes", "No se ha establecido la conexion"
End If
'*************************************************************

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

    Set adoRecordset = New adodb.Recordset

    sConsulta = "SELECT ID,TIPO,DESCR_" & gParametrosInstalacion.gIdioma & " FROM TIPO_SOLICITUDES WITH (NOLOCK)"
    If bNoCompras = True Then
        sConsulta = sConsulta & " WHERE ID<>1"
    End If
    
    adoRecordset.CursorLocation = adUseClient
    
    adoRecordset.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
            
    Set adoRecordset.ActiveConnection = Nothing

    Set DevolverTiposSolicitudes = adoRecordset
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CTiposSolicit", "DevolverTiposSolicitudes", ERR, Erl)
      Exit Function
   End If

End Function


