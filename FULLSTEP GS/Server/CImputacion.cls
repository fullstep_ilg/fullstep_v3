VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CImputacion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

Private m_oConexion As CConexion

Private m_sPRES5Id As String
Private m_sPRES5 As String
Private m_sPRES1 As String
Private m_sPRES2 As String
Private m_sPRES3 As String
Private m_sPRES4 As String
Private m_sCC1 As String
Private m_sCC3 As String
Private m_sCC2 As String
Private m_sCC4 As String
Private m_sCC As Variant
Private m_iModo As Integer
Private m_sPRESDen As String
Private m_sCCDen As String
Private m_vEmpresa As Variant
Private m_sCodContrato As String
Private m_sCodCenCoste As String
Private m_sCodConcat As String
Private m_sCenSM As String
Private m_dFecInicio As Date
Private m_dFecFin As Date
Private m_bCerrado As Boolean
Private m_dCambio As Double
Private m_sMoneda As String
Private m_vGestor As Variant

Public Function ExistePartidaPlurianual(ByVal Anyo As Integer) As Boolean
Dim sConsulta As String
Dim adoComm As adodb.Command
Dim adoParam As adodb.Parameter
Dim adoRs As adodb.Recordset
On Error GoTo Error_Cls
'------------------------------------------------------------------------------------------
ExistePartidaPlurianual = False

Set adoComm = New adodb.Command
With adoComm
    .CommandType = adCmdStoredProc
    .CommandText = "FSSM_EXISTE_PARTIDA_PLURIANUAL"
    Set .ActiveConnection = m_oConexion.ADOCon

    Set adoParam = .CreateParameter("IDPRES5", adInteger, adParamInput, , PRES5Id)
    .Parameters.Append adoParam
    Set adoParam = .CreateParameter("ANYO", adInteger, adParamInput, , Anyo)
    .Parameters.Append adoParam
    Set adoRs = adoComm.Execute
End With
If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls

ExistePartidaPlurianual = (adoRs("NUM").Value > 0)
adoRs.Close
Set adoRs = Nothing
Set adoComm = Nothing
Set adoParam = Nothing
Exit Function
'------------------------------------------------------------------------------------------

Error_Cls:
    If m_oConexion.ADOCon.Errors.Count > 0 Then
        Call basErrores.TratarError(m_oConexion.ADOCon.Errors)
        
        If Not adoRs Is Nothing Then
            adoRs.Close
            Set adoRs = Nothing
        End If
        Set adoComm = Nothing
        Set adoParam = Nothing
    ElseIf ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CImputacion", "ExistePartidaPlurianual", ERR, Erl)
    End If
End Function

Public Property Let Moneda(ByVal dato As String)
    m_sMoneda = dato
End Property
Public Property Get Moneda() As String
    Moneda = m_sMoneda
End Property

Public Property Let FecInicio(ByVal dato As Date)
    m_dFecInicio = dato
End Property
Public Property Get FecInicio() As Date
    FecInicio = m_dFecInicio
End Property
Public Property Let FecFin(ByVal dato As Date)
    m_dFecFin = dato
End Property
Public Property Get FecFin() As Date
    FecFin = m_dFecFin
End Property
Public Property Let Cerrado(ByVal dato As Boolean)
    m_bCerrado = dato
End Property
Public Property Get Cerrado() As Boolean
    Cerrado = m_bCerrado
End Property

Public Property Let Modo(ByVal dato As Integer)
    m_iModo = dato
End Property
Public Property Get Modo() As Integer
    Modo = m_iModo
End Property

Public Property Let PresDen(ByVal dato As String)
    m_sPRESDen = dato
End Property
Public Property Get PresDen() As String
    PresDen = m_sPRESDen
End Property
Public Property Let CCDen(ByVal dato As String)
    m_sCCDen = dato
End Property
Public Property Get CCDen() As String
    CCDen = m_sCCDen
End Property

Public Property Let PRES5(ByVal dato As String)
    m_sPRES5 = dato
End Property
Public Property Get PRES5() As String
    PRES5 = m_sPRES5
End Property

Public Property Let PRES1(ByVal dato As String)
    m_sPRES1 = dato
End Property
Public Property Get PRES1() As String
    PRES1 = m_sPRES1
End Property

Public Property Let PRES2(ByVal dato As String)
    m_sPRES2 = dato
End Property
Public Property Get PRES2() As String
    PRES2 = m_sPRES2
End Property

Public Property Let Pres3(ByVal dato As String)
    m_sPRES3 = dato
End Property
Public Property Get Pres3() As String
    Pres3 = m_sPRES3
End Property

Public Property Let Pres4(ByVal dato As String)
    m_sPRES4 = dato
End Property
Public Property Get Pres4() As String
    Pres4 = m_sPRES4
End Property

Public Property Let CC1(ByVal dato As String)
    m_sCC1 = dato
End Property
Public Property Get CC1() As String
    CC1 = m_sCC1
End Property

Public Property Let CC2(ByVal dato As String)
    m_sCC2 = dato
End Property
Public Property Get CC2() As String
    CC2 = m_sCC2
End Property

Public Property Let CC3(ByVal dato As String)
    m_sCC3 = dato
End Property
Public Property Get CC3() As String
    CC3 = m_sCC3
End Property

Public Property Let CC4(ByVal dato As String)
    m_sCC4 = dato
End Property
Public Property Get CC4() As String
    CC4 = m_sCC4
End Property


Public Property Let CentroCoste(ByVal dato As Variant)
    m_sCC = dato
End Property
Public Property Get CentroCoste() As Variant
    CentroCoste = m_sCC
End Property

Public Property Let Empresa(ByVal dato As Variant)
    m_vEmpresa = dato
End Property
Public Property Get Empresa() As Variant
    Empresa = m_vEmpresa
End Property

Public Property Let CodContrato(ByVal dato As String)
    m_sCodContrato = dato
End Property
Public Property Get CodContrato() As String
    CodContrato = m_sCodContrato
End Property

Public Property Let CodCenCoste(ByVal dato As String)
    m_sCodCenCoste = dato
End Property
Public Property Get CodCenCoste() As String
    CodCenCoste = m_sCodCenCoste
End Property

Public Property Let CenSM(ByVal dato As String)
    m_sCenSM = dato
End Property
Public Property Get CenSM() As String
    CenSM = m_sCenSM
End Property

Public Property Let Cambio(ByVal dato As Double)
    m_dCambio = dato
End Property
Public Property Get Cambio() As Double
    Cambio = m_dCambio
End Property

Public Property Let CodConcat(ByVal dato As String)
    m_sCodConcat = dato
End Property
Public Property Get CodConcat() As String
    CodConcat = m_sCodConcat
End Property

Public Property Let Gestor(ByVal dato As Variant)
    m_vGestor = dato
End Property
Public Property Get Gestor() As Variant
    Gestor = m_vGestor
End Property


Friend Property Set Conexion(ByVal vData As CConexion)

    ''' * Objetivo: Dar valor a la conexion de la Destino
    ''' * Recibe: Conexion
    ''' * Devuelve: Nada

    Set m_oConexion = vData
    
End Property
Friend Property Get Conexion() As CConexion

    ''' * Objetivo: Devolver la conexion de la Destino
    ''' * Recibe: Nada
    ''' * Devuelve: Conexion

    Set Conexion = m_oConexion
    
End Property

Private Sub Class_Terminate()
    
     On Error GoTo Error_Cls:
    
    Set m_oConexion = Nothing
    Exit Sub
Error_Cls:
    Resume Next
    
End Sub


''' <summary>
''' Acumula el importe pasado en el comprometido de la partida, no hay transacci�n pq estar� en la funci�n que la llame
''' </summary>
''' <param name="MonPedido">Moneda del pedido</param>
''' <param name="CambioPedido">Cambio de la moneda del pedido</param>
''' <param name="Importe">Importe a sumar</param>
''' <param name="iRecep">Si se llama desde recepciones</param>
''' <param name="LineaRecep">Id de la linea de recepci�n</param>
''' <param name="CantPedida">Cantidad pedida</param>
''' <param name="IdSolicit">Id de la instancia vinculada al pedido</param>
''' <param name="IdCampoSolicit">Id de copia_campo de la linea vinculada al pedido</param>
''' <param name="TipoRecepcion">Si se recepciona por cantidad=0 o por importe=1</param>
''' <returns>TipoErrorSummit, true si existe, False si no</returns>
''' <remarks>Llamada desde:COrdenesEntrga.EmitirPedidoConLineasTemporales</remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>
Public Function ActualizarImportesPartida(ByVal MonPedido As String, Optional ByVal CambioPedido As Double, Optional ByVal Importe As Double, _
                Optional ByVal iRecep As Integer, Optional ByVal LineaRecep As Long, Optional ByVal CantPedida As Double, Optional ByVal ImportePedido As Double, _
                Optional IdSolicit As Long, Optional IdCampoSolicit As Long, Optional ByVal LineaPedido As Integer, Optional tipoRecepcion As Integer, _
                Optional ByVal FechaEntrega As Variant, Optional Planes As CPlanesEntrega, Optional idLineaPedido As Long = 0, _
                Optional ByVal CantPedidaSeguim As Double, Optional ByVal ImportePedidoSeguim As Double) As TipoErrorSummit
    Dim sConsulta As String
    Dim adoComm As adodb.Command
    Dim adoParam As adodb.Parameter
    Dim adoRs As New adodb.Recordset
    Dim oImp As CImputacion
    Dim udtError As TipoErrorSummit
    Dim dblComprometido As Double
    Dim AnyoImputacion As Integer
    Dim bPlanes As Boolean
    Dim oPlan As CPlanEntrega
    Dim CantidadImputacion As Double
    Dim ImporteImputacion As Double
    Dim ImporteComprometido As Double
    Dim Factor As Double
    
    udtError.NumError = TESnoerror
    On Error GoTo Error_Cls
    
    
    If (iRecep = 0 And g_oParametrosSM.Item(m_sPRES5).ComprAcum = 1) Or (iRecep <> 0 And g_oParametrosSM.Item(m_sPRES5).ComprAcum = 2) Then
        'Obtenemos la moneda de la partida si no est� cargada.
        'Antes se hacia de manera similar para el caso de la emision y la recepcion.
        'Por falta de horas para la tarea no se contempla liberar solicitado cuando acumula comprometido en la recepcion
        'Ahora para la emision llamamos al stored FSSM_ACTUALIZAR_IMPORTES_PARTIDA que gestionara tb el liberar el solicitado
        Set adoComm = New adodb.Command
        If iRecep > 1 Then
            sConsulta = "SELECT COMP,MON,EQUIV,PRES5_IMPORTES.ID,LRI.CAMBIO FROM PRES5_IMPORTES WITH (NOLOCK) LEFT JOIN MON WITH (NOLOCK) ON PRES5_IMPORTES.MON=MON.COD  "
            If g_oParametrosSM.Item(m_sPRES5).ImpRecep Then
                sConsulta = sConsulta & " INNER JOIN LINEAS_RECEP_IMPUTACION LRI WITH (NOLOCK) ON LRI.LINEA_RECEP=? AND LRI.PRES5_IMP=PRES5_IMPORTES.ID "
            Else
                sConsulta = sConsulta & " INNER JOIN LINEAS_PED_IMPUTACION LRI WITH (NOLOCK) ON LRI.LINEA=? AND LRI.PRES5_IMP=PRES5_IMPORTES.ID "
            End If
            sConsulta = sConsulta & " WHERE PRES5_IMPORTES.PRES0=?"
            Set adoParam = adoComm.CreateParameter("LINEA_RECEP", adInteger, adParamInput, , LineaRecep)
            adoComm.Parameters.Append adoParam
            Set adoParam = adoComm.CreateParameter("PRES5", adVarChar, adParamInput, gLongitudesDeCodigos.giLongCodPres5Niv0, m_sPRES5)
            adoComm.Parameters.Append adoParam

            Set adoComm.ActiveConnection = m_oConexion.ADOCon
            adoComm.CommandText = sConsulta
            adoComm.CommandType = adCmdText
            Set adoRs = adoComm.Execute

            If Not adoRs.eof Then
                udtError.Arg1 = adoRs.Fields("ID").Value 'Para la recepci�n devuelve el id de pres5_importes
                dblComprometido = NullToDbl0(adoRs.Fields("COMP").Value)
                If IsNull(adoRs.Fields("MON").Value) Then
                    m_sMoneda = gParametrosGenerales.gsMONCEN
                    m_dCambio = 1
                Else
                    m_sMoneda = adoRs.Fields("MON").Value
                    m_dCambio = NullToDbl0(adoRs.Fields("CAMBIO").Value)
                    If m_dCambio = 0 Then 'S�lo cargamos el cambio de moneda si no estaba ya cargado
                        m_dCambio = adoRs.Fields("EQUIV").Value
                    End If
                End If
            Else
                m_sMoneda = gParametrosGenerales.gsMONCEN
                m_dCambio = 1
                adoRs.Close
                Set adoRs = Nothing
                Set adoComm = Nothing
                udtError.Arg1 = Null
                ActualizarImportesPartida = udtError
                Exit Function
            End If
            adoRs.Close
            Set adoRs = Nothing
            Set adoComm = Nothing

            'El importe llega en moneda central. En pantalla Recepciones/Seguimiento se ve en la moneda correspondiente pero llega
            'como en bbdd moneda central.
            'Los cambios son respecto moneda central. Luego si llega 250, son 250� y si la moneda comprometido es ADP(cambio 2), se graba 500ADP.
            dblComprometido = dblComprometido + CDec(Importe * m_dCambio)

            If Importe <> 0 And iRecep > 0 Then
                Set adoComm = New adodb.Command

                sConsulta = "UPDATE PRES5_IMPORTES SET COMP=? WHERE ID=? "
                Set adoParam = adoComm.CreateParameter("COMP", adDouble, adParamInput, , dblComprometido)
                adoComm.Parameters.Append adoParam
                Set adoParam = adoComm.CreateParameter("ID", adInteger, adParamInput, , udtError.Arg1)
                adoComm.Parameters.Append adoParam

                Set adoComm.ActiveConnection = m_oConexion.ADOCon
                adoComm.CommandText = sConsulta
                adoComm.CommandType = adCmdText

                adoComm.Execute
                If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
                Set adoComm = Nothing
                Set adoParam = Nothing
            End If
        Else
            Dim rs As adodb.Recordset
            'Ejecutamos el stored FSSM_ACTUALIZAR_IMPORTES_PARTIDA que acumulara el comprometido y liberara el solicitado
            'En el caso de tener activo el parametro de la partidas plurianuales hay que calcular cual es el
            If g_oParametrosSM.Item(m_sPRES5).PluriAnual Then
                'Sacamos el a�o en el que se tiene que imputar
                If IsDate(FechaEntrega) Then
                    AnyoImputacion = Year(FechaEntrega)
                    CantidadImputacion = CantPedida
                    ImporteImputacion = ImportePedido
                    ImporteComprometido = Importe
                    GoTo SP_ActualizarImporte
                Else
                    For Each oPlan In Planes
                        AnyoImputacion = Year(oPlan.Fecha)
                        CantidadImputacion = NullToDbl0(StrToVbNULL(oPlan.Cantidad))
                        ImporteImputacion = NullToDbl0(StrToVbNULL(oPlan.Importe))
                        Factor = 0
                        'Hay que calcular el importe de cada plan porque hay que imputar al a�o de la fecha del plan
                        'y al poder ser diferentes a�os el importe de la linea no debe ser el total de la misma, sino lo que corresponde
                        If tipoRecepcion = 0 Then
                            'Sacamos la fraccion de la cantidad pedida que le corresponde a este plan
                            If Not (CantPedida = 0) Then
                                Factor = CantidadImputacion / CantPedida
                            ElseIf Not (CantPedidaSeguim = 0) Then
                                Factor = CantidadImputacion / CantPedidaSeguim
                            End If
                        Else
                            'Sacamos la fraccion del importe pedido que le corresponde a este plan
                            If Not (ImportePedido = 0) Then
                                Factor = ImporteImputacion / ImportePedido
                            ElseIf Not (ImportePedidoSeguim = 0) Then
                                Factor = ImporteImputacion / ImportePedidoSeguim
                            End If
                        End If
                        'Si el importe o cantidad pedida es la misma que la del plan el resultado es que el factor ser� 0 con lo cual no hay que realizar la operaci�n
                        If Factor = 0 Then
                            ImporteComprometido = Importe
                        Else
                            ImporteComprometido = Importe * Factor
                        End If
                        GoTo SP_ActualizarImporte
Siguiente:
                    Next
                End If
            Else
                CantidadImputacion = CantPedida
                ImporteImputacion = ImportePedido
                ImporteComprometido = Importe
                GoTo SP_ActualizarImporte
            End If
        End If
    End If
    
fin:
    ActualizarImportesPartida = udtError
    Exit Function
  
SP_ActualizarImporte:
    Set adoComm = New adodb.Command
    adoComm.CommandType = adCmdStoredProc
    adoComm.CommandText = "FSSM_ACTUALIZAR_IMPORTES_PARTIDA" & IIf(g_oParametrosSM.Item(m_sPRES5).PluriAnual, "_ANYO", "")
    Set adoComm.ActiveConnection = m_oConexion.ADOCon
    
    Set adoParam = adoComm.CreateParameter("IDIOMA", adVarWChar, adParamInput, 10, gParametrosInstalacion.gIdioma) 'El numero de la linea de la solicitud de compra
    adoComm.Parameters.Append adoParam
    If g_oParametrosSM.Item(m_sPRES5).PluriAnual Then
        Set adoParam = adoComm.CreateParameter("ANYO", adInteger, adParamInput, , AnyoImputacion)
        adoComm.Parameters.Append adoParam
        'Se necesita la linea de pedido para sacar el a�o de partida de item_adj
        Set adoParam = adoComm.CreateParameter("IDLINEAPEDIDO", adInteger, adParamInput, , idLineaPedido)
        adoComm.Parameters.Append adoParam
    End If
    Set adoParam = adoComm.CreateParameter("PRES0", adVarWChar, adParamInput, 100, StrToVbNULL(m_sPRES5))
    adoComm.Parameters.Append adoParam
    Set adoParam = adoComm.CreateParameter("PRES1", adVarWChar, adParamInput, 100, StrToVbNULL(m_sPRES1))
    adoComm.Parameters.Append adoParam
    Set adoParam = adoComm.CreateParameter("PRES2", adVarWChar, adParamInput, 100, StrToVbNULL(m_sPRES2))
    adoComm.Parameters.Append adoParam
    Set adoParam = adoComm.CreateParameter("PRES3", adVarWChar, adParamInput, 100, StrToVbNULL(m_sPRES3))
    adoComm.Parameters.Append adoParam
    Set adoParam = adoComm.CreateParameter("PRES4", adVarWChar, adParamInput, 100, StrToVbNULL(m_sPRES4))
    adoComm.Parameters.Append adoParam
    Set adoParam = adoComm.CreateParameter("TIENECONTROLDISPONIBLE", adInteger, adParamInput, , 0)
    adoComm.Parameters.Append adoParam
    'CONTROLDISPCONPRES
    Set adoParam = adoComm.CreateParameter("CONTROLDISPCONPRES", adInteger, adParamInput, , 0)
    adoComm.Parameters.Append adoParam
    Set adoParam = adoComm.CreateParameter("INSTANCIA", adInteger, adParamInput, , IdSolicit) 'El id de la instancia de la solicitud de compra
    adoComm.Parameters.Append adoParam
    Set adoParam = adoComm.CreateParameter("MONEDA", adVarWChar, adParamInput, 10, gParametrosGenerales.gsMONCEN)
    adoComm.Parameters.Append adoParam
    Set adoParam = adoComm.CreateParameter("CANT_PEDIDA", adDouble, adParamInput, , IIf(tipoRecepcion = 0 And Not CantidadImputacion = 0 And Not IdSolicit = 0, CantidadImputacion, Null))
    adoComm.Parameters.Append adoParam
    Set adoParam = adoComm.CreateParameter("IMPORTE_PEDIDO", adDouble, adParamInput, , IIf(tipoRecepcion = 1 And Not ImporteImputacion = 0 And Not IdSolicit = 0, ImporteImputacion, Null))  'El importe que pasaremos esta en moneda central
    adoComm.Parameters.Append adoParam
    Set adoParam = adoComm.CreateParameter("IDCAMPOSOLICIT", adInteger, adParamInput, , IdCampoSolicit) 'El id de copia_campo de la linea de la solicitud de compra
    adoComm.Parameters.Append adoParam
    Set adoParam = adoComm.CreateParameter("SOLICITADO", adDouble, adParamInput, , Null)
    adoComm.Parameters.Append adoParam
    Set adoParam = adoComm.CreateParameter("LINEAPEDIDO", adInteger, adParamInput, , LineaPedido)
    adoComm.Parameters.Append adoParam
    Set adoParam = adoComm.CreateParameter("COMPROMETIDO", adDouble, adParamInput, , IIf(Importe = 0, Null, ImporteComprometido)) 'El importe comprometido que pasaremos esta en moneda central
    adoComm.Parameters.Append adoParam
    
    'Definimos las variables que obtendremos como output
    Set adoParam = adoComm.CreateParameter("OK", adInteger, adParamInputOutput, , 0)
    adoComm.Parameters.Append adoParam
    Set adoParam = adoComm.CreateParameter("ID_PRES5_IMPORTES", adInteger, adParamOutput)
    adoComm.Parameters.Append adoParam
    Set adoParam = adoComm.CreateParameter("CODPARTIDA", adVarWChar, adParamOutput, 100, Null)
    adoComm.Parameters.Append adoParam
    Set adoParam = adoComm.CreateParameter("DENPARTIDA", adVarWChar, adParamOutput, 300, Null)
    adoComm.Parameters.Append adoParam
    Set adoParam = adoComm.CreateParameter("PRESUPUESTADO", adDouble, adParamOutput, , Null)
    adoComm.Parameters.Append adoParam
    Set adoParam = adoComm.CreateParameter("DISPONIBLE", adDouble, adParamOutput, , Null)
    adoComm.Parameters.Append adoParam
    Set adoParam = adoComm.CreateParameter("EQUIV", adDouble, adParamOutput, , Null)
    adoComm.Parameters.Append adoParam
    Set adoParam = adoComm.CreateParameter("MON", adVarWChar, adParamOutput, 10, Null)
    adoComm.Parameters.Append adoParam
    
    adoComm.Execute
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    udtError.Arg1 = adoComm.Parameters("MON").Value
    'Si es una fecha de entrega finalizamos, si no lo es, estaremos en el bucle de pklanes de entrega, as� que tenemos que ir al siguiente registro
    If IsDate(FechaEntrega) Or Not g_oParametrosSM.Item(m_sPRES5).PluriAnual Then
        GoTo fin
    Else
        GoTo Siguiente
    End If
    
Error_Cls:
    If m_oConexion.ADOCon.Errors.Count > 0 Then
        ActualizarImportesPartida = basErrores.TratarError(m_oConexion.ADOCon.Errors)
        If Not adoRs Is Nothing Then
            adoRs.Close
            Set adoRs = Nothing
        End If
        Set adoComm = Nothing
        Set adoParam = Nothing
    ElseIf ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CImputacion", "AcumularComprometido", ERR, Erl)
   End If
End Function

''' <summary>Acumula el importe pasado en el solicitado de la partida plurianual, NO hay transacci�n pq estar� en la funci�n llamante</summary>
''' <param name="MonPedido">Moneda del pedido</param>
''' <param name="CambioPedido">Cambio de la moneda del pedido</param>
''' <param name="Importe">Importe a sumar</param>
''' <param name="LineaRecep">Id de la linea de recepci�n</param>
''' <param name="CantPedida">Cantidad pedida</param>
''' <param name="IdSolicit">Id de la instancia vinculada al pedido</param>
''' <param name="IdCampoSolicit">Id de copia_campo de la linea vinculada al pedido</param>
''' <param name="TipoRecepcion">Si se recepciona por cantidad=0 o por importe=1</param>
''' <returns>TipoErrorSummit</returns>
''' <remarks>Llamada desde: CProceso.ValidacionYCierre</remarks>

Public Function ActualizarImportesPartidaPlurianual(ByVal AnyoProce As Integer, ByVal GMN1Proce As String, ByVal Proce As Long, ByVal idItem As Long, ByVal NumOfe As Integer, ByVal sProve As String, _
        ByVal IdPres5Imp As Long, ByVal AnyoImp As Integer, ByVal lInstancia As Long, ByVal lIdLineaSolicit As Long, ByVal lIdCampoSolicit As Long, ByVal dCantAdj As Double, Optional ByVal bReabrir As Boolean = False) As TipoErrorSummit
    Dim sConsulta As String
    Dim adoComm As adodb.Command
    Dim adoParam As adodb.Parameter
    Dim udtError As TipoErrorSummit
       
    udtError.NumError = TESnoerror
    On Error GoTo Error_Cls
       
    'Obtenemos la moneda de la partida si no est� cargada.
    'Se resta del solicitado el importe de la l�nea de solicitud y se suma el adjudicado en la l�nea de proceso (en la moneda de la partida)
    Set adoComm = New adodb.Command
    With adoComm
        .CommandType = adCmdStoredProc
        .CommandText = "FSSM_ACTUALIZAR_IMPORTES_PARTIDA_PLURIANUAL"
        Set .ActiveConnection = m_oConexion.ADOCon
    
        Set adoParam = .CreateParameter("ANYO_PROCE", adSmallInt, adParamInput, , AnyoProce)
        .Parameters.Append adoParam
        Set adoParam = .CreateParameter("GMN1_PROCE", adVarChar, adParamInput, 50, GMN1Proce)
        .Parameters.Append adoParam
        Set adoParam = .CreateParameter("PROCE", adInteger, adParamInput, , Proce)
        .Parameters.Append adoParam
        Set adoParam = .CreateParameter("ITEM", adInteger, adParamInput, , idItem)
        .Parameters.Append adoParam
        Set adoParam = .CreateParameter("OFE", adInteger, adParamInput, , NumOfe)
        .Parameters.Append adoParam
        Set adoParam = .CreateParameter("PROVE", adVarChar, adParamInput, 50, sProve)
        .Parameters.Append adoParam
        Set adoParam = .CreateParameter("PRES5_IMP", adInteger, adParamInput, , IdPres5Imp)  'El numero de la linea de la solicitud de compra
        .Parameters.Append adoParam
        Set adoParam = .CreateParameter("ANYO", adSmallInt, adParamInput, , AnyoImp)
        .Parameters.Append adoParam
        Set adoParam = .CreateParameter("INSTANCIA", adInteger, adParamInput, , lInstancia)
        .Parameters.Append adoParam
        Set adoParam = .CreateParameter("LINEA_SOLICIT", adInteger, adParamInput, , lIdLineaSolicit)
        .Parameters.Append adoParam
        Set adoParam = .CreateParameter("IDCAMPOSOLICIT", adInteger, adParamInput, , lIdCampoSolicit)
        .Parameters.Append adoParam
        Set adoParam = .CreateParameter("CANT_ADJ", adDouble, adParamInput, , dCantAdj)
        .Parameters.Append adoParam
        Set adoParam = .CreateParameter("REABRIR", adBoolean, adParamInput, , bReabrir)
        .Parameters.Append adoParam
                                
        adoComm.Execute
    End With
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    
    ActualizarImportesPartidaPlurianual = udtError
Salir:
    Set adoComm = Nothing
    Set adoParam = Nothing
    Exit Function
Error_Cls:
    If m_oConexion.ADOCon.Errors.Count > 0 Then
        ActualizarImportesPartidaPlurianual = basErrores.TratarError(m_oConexion.ADOCon.Errors)
        GoTo Salir  'No Resume Salir porque no hay error de sistema (err no contiene ning�n error), si no de BD
    ElseIf ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CImputacion", "AcumularComprometido", ERR, Erl)
   End If
End Function

''' <summary>Modifica el comprometido de la partida. Resta del comprometido un importe y a�ade su correspondiente nuevo</summary>
''' <param name="MonPedido">Moneda del pedido</param>
''' <param name="CambioPedido">Cambio de la moneda del pedido</param>
''' <param name="ImporteOld">Comprometido antiguo</param>
''' <param name="ImporteNew">Nuevo Comprometido</param>
''' <returns>Variable TipoErrorSummit con el error si lo hay</returns>
''' <remarks>Llamada desde: CAtributo</remarks>
''' <revision>LTG 13/06/2012</revision>

Public Function ModificarComprometido(ByVal MonPedido As String, ByVal CambioPedido As Double, ByVal ImporteOld As Double, ByVal ImporteNew As Double) As TipoErrorSummit
    Dim sConsulta As String
    Dim adoComm As adodb.Command
    Dim adoParam As adodb.Parameter
    Dim adoRs As New adodb.Recordset
    Dim oImp As CImputacion
    Dim udtError As TipoErrorSummit
    Dim dblComprometido As Double

On Error GoTo Error_Cls
    
    udtError.NumError = TESnoerror
    
    Set adoComm = New adodb.Command
    
    sConsulta = "SELECT COMP,MON,EQUIV,PRES5_IMPORTES.ID FROM PRES5_IMPORTES WITH (NOLOCK) LEFT JOIN MON WITH (NOLOCK) ON PRES5_IMPORTES.MON=MON.COD  WHERE CERRADO=0 AND PRES0=?"
    Set adoParam = adoComm.CreateParameter("PRES5", adVarChar, adParamInput, gLongitudesDeCodigos.giLongCodPres5Niv0, m_sPRES5)
    adoComm.Parameters.Append adoParam
    
    If m_sPRES4 <> "" Then
        sConsulta = sConsulta & " AND PRES1=? AND PRES2=? AND PRES3=? AND PRES4=?"
        Set adoParam = adoComm.CreateParameter("PRES1", adVarChar, adParamInput, gLongitudesDeCodigos.giLongCodPres5Niv1, m_sPRES1)
        adoComm.Parameters.Append adoParam
        Set adoParam = adoComm.CreateParameter("PRES2", adVarChar, adParamInput, gLongitudesDeCodigos.giLongCodPres5Niv2, m_sPRES2)
        adoComm.Parameters.Append adoParam
        Set adoParam = adoComm.CreateParameter("PRES3", adVarChar, adParamInput, gLongitudesDeCodigos.giLongCodPres5Niv3, m_sPRES3)
        adoComm.Parameters.Append adoParam
        Set adoParam = adoComm.CreateParameter("PRES4", adVarChar, adParamInput, gLongitudesDeCodigos.giLongCodPres5Niv4, m_sPRES4)
        adoComm.Parameters.Append adoParam
    Else
        If m_sPRES3 <> "" Then
            sConsulta = sConsulta & " AND PRES1=? AND PRES2=? AND PRES3=? AND PRES4 IS NULL"
            Set adoParam = adoComm.CreateParameter("PRES1", adVarChar, adParamInput, gLongitudesDeCodigos.giLongCodPres5Niv1, m_sPRES1)
            adoComm.Parameters.Append adoParam
            Set adoParam = adoComm.CreateParameter("PRES2", adVarChar, adParamInput, gLongitudesDeCodigos.giLongCodPres5Niv2, m_sPRES2)
            adoComm.Parameters.Append adoParam
            Set adoParam = adoComm.CreateParameter("PRES3", adVarChar, adParamInput, gLongitudesDeCodigos.giLongCodPres5Niv3, m_sPRES3)
            adoComm.Parameters.Append adoParam
        Else
            If m_sPRES2 <> "" Then
                sConsulta = sConsulta & " AND PRES1=? AND PRES2=? AND PRES3 IS NULL AND PRES4 IS NULL"
                Set adoParam = adoComm.CreateParameter("PRES1", adVarChar, adParamInput, gLongitudesDeCodigos.giLongCodPres5Niv1, m_sPRES1)
                adoComm.Parameters.Append adoParam
                Set adoParam = adoComm.CreateParameter("PRES2", adVarChar, adParamInput, gLongitudesDeCodigos.giLongCodPres5Niv2, m_sPRES2)
                adoComm.Parameters.Append adoParam
            Else
                sConsulta = sConsulta & " AND PRES1=? AND PRES2 IS NULL AND PRES3 IS NULL AND PRES4 IS NULL"
                Set adoParam = adoComm.CreateParameter("PRES1", adVarChar, adParamInput, gLongitudesDeCodigos.giLongCodPres5Niv1, m_sPRES1)
                adoComm.Parameters.Append adoParam
            End If
        End If
    End If
    
    Set adoComm.ActiveConnection = m_oConexion.ADOCon
    adoComm.CommandText = sConsulta
    adoComm.CommandType = adCmdText
    
    Set adoRs = adoComm.Execute

    If Not adoRs.eof Then
        udtError.Arg1 = adoRs.Fields("MON").Value 'Devuelve la moneda de la partida porque se usa en el seguimiento para comparar
        
        dblComprometido = NullToDbl0(adoRs.Fields("COMP").Value)
        If IsNull(adoRs.Fields("MON").Value) Then
            m_sMoneda = gParametrosGenerales.gsMONCEN
            m_dCambio = 1
        Else
            m_sMoneda = adoRs.Fields("MON").Value
            If m_dCambio = 0 Then 'S�lo cargamos el cambio de moneda si no estaba ya cargado
                m_dCambio = adoRs.Fields("EQUIV").Value
            End If
        End If
    Else
        m_sMoneda = gParametrosGenerales.gsMONCEN
        m_dCambio = 1
        adoRs.Close
        Set adoRs = Nothing
        Set adoComm = Nothing
        udtError.Arg1 = Null
        ModificarComprometido = udtError
        Exit Function
    End If
    adoRs.Close
    Set adoRs = Nothing
    Set adoComm = Nothing


    'El importe llega en moneda central. En pantalla Recepciones/Seguimiento se ve en la moneda correspondiente pero llega
    'como en bbdd moneda central.
    'Los cambios son respecto moneda central. Luego si llega 250, son 250� y si la moneda comprometido es ADP(cambio 2), se graba 500ADP.
    dblComprometido = dblComprometido + CDec((ImporteNew - ImporteOld) * m_dCambio)
    
    If (ImporteNew - ImporteOld) <> 0 Then
        Set adoComm = New adodb.Command
        
        sConsulta = "UPDATE PRES5_IMPORTES SET COMP=? WHERE CERRADO=0 AND PRES0=? "
        Set adoParam = adoComm.CreateParameter("COMP", adDouble, adParamInput, , dblComprometido)
        adoComm.Parameters.Append adoParam
        Set adoParam = adoComm.CreateParameter("PRES5", adVarChar, adParamInput, gLongitudesDeCodigos.giLongCodPres5Niv0, m_sPRES5)
        adoComm.Parameters.Append adoParam
        If m_sPRES4 <> "" Then
            sConsulta = sConsulta & " AND PRES1=? AND PRES2=? AND PRES3=? AND PRES4=?"
            Set adoParam = adoComm.CreateParameter("PRES1", adVarChar, adParamInput, gLongitudesDeCodigos.giLongCodPres5Niv1, m_sPRES1)
            adoComm.Parameters.Append adoParam
            Set adoParam = adoComm.CreateParameter("PRES2", adVarChar, adParamInput, gLongitudesDeCodigos.giLongCodPres5Niv2, m_sPRES2)
            adoComm.Parameters.Append adoParam
            Set adoParam = adoComm.CreateParameter("PRES3", adVarChar, adParamInput, gLongitudesDeCodigos.giLongCodPres5Niv3, m_sPRES3)
            adoComm.Parameters.Append adoParam
            Set adoParam = adoComm.CreateParameter("PRES4", adVarChar, adParamInput, gLongitudesDeCodigos.giLongCodPres5Niv4, m_sPRES4)
            adoComm.Parameters.Append adoParam
        Else
            If m_sPRES3 <> "" Then
                sConsulta = sConsulta & " AND PRES1=? AND PRES2=? AND PRES3=? AND PRES4 IS NULL"
                Set adoParam = adoComm.CreateParameter("PRES1", adVarChar, adParamInput, gLongitudesDeCodigos.giLongCodPres5Niv1, m_sPRES1)
                adoComm.Parameters.Append adoParam
                Set adoParam = adoComm.CreateParameter("PRES2", adVarChar, adParamInput, gLongitudesDeCodigos.giLongCodPres5Niv2, m_sPRES2)
                adoComm.Parameters.Append adoParam
                Set adoParam = adoComm.CreateParameter("PRES3", adVarChar, adParamInput, gLongitudesDeCodigos.giLongCodPres5Niv3, m_sPRES3)
                adoComm.Parameters.Append adoParam
            Else
                If m_sPRES2 <> "" Then
                    sConsulta = sConsulta & " AND PRES1=? AND PRES2=? AND PRES3 IS NULL AND PRES4 IS NULL"
                    Set adoParam = adoComm.CreateParameter("PRES1", adVarChar, adParamInput, gLongitudesDeCodigos.giLongCodPres5Niv1, m_sPRES1)
                    adoComm.Parameters.Append adoParam
                    Set adoParam = adoComm.CreateParameter("PRES2", adVarChar, adParamInput, gLongitudesDeCodigos.giLongCodPres5Niv2, m_sPRES2)
                    adoComm.Parameters.Append adoParam
                Else
                    sConsulta = sConsulta & " AND PRES1=? AND PRES2 IS NULL AND PRES3 IS NULL AND PRES4 IS NULL"
                    Set adoParam = adoComm.CreateParameter("PRES1", adVarChar, adParamInput, gLongitudesDeCodigos.giLongCodPres5Niv1, m_sPRES1)
                    adoComm.Parameters.Append adoParam
                End If
            End If
        End If

        Set adoComm.ActiveConnection = m_oConexion.ADOCon
        adoComm.CommandText = sConsulta
        adoComm.CommandType = adCmdText
    
        adoComm.Execute
        If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    End If
    
Salir:
    Set adoRs = Nothing
    Set adoComm = Nothing
    Set adoParam = Nothing
    ModificarComprometido = udtError
    Exit Function
Error_Cls:
    If m_oConexion.ADOCon.Errors.Count > 0 Then
        udtError = basErrores.TratarError(m_oConexion.ADOCon.Errors)
        Resume Salir
    ElseIf ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CImputacion", "ModificarComprometido", ERR, Erl)
   End If
End Function

''' <summary>
''' Devolver el Id de PRES5_IMPORTES
''' </summary>
''' <param name="ParametroEntrada1">Id de PRES5_IMPORTES</param>
''' <remarks>Llamada desde: cImputaciones.add   cLineaPedido/IBaseDatos_FinalizarEdicionModificando  cOrdenEntrega/IBaseDatos_FinalizarEdicionModificando
''' frmpedidos/sdbgMiPedido_BeforeRowColChange      frmpedidos/sdbgMiPedido_BtnClick    frmpedidos/CambiaContratoCabeceraMisPedidos
''' frmpedidos/CambiaCentroCosteCabeceraMisPedidos  frmpedidos/ActualizarCabeceraDesdeLineas    frmseguimiento/sdbgItemPorProve_BtnClick
''' frmseguimiento/sdbgSeguimiento_BtnClick   ; Tiempo m�ximo: 0,2</remarks>
Public Property Let PRES5Id(ByVal dato As String)
    m_sPRES5Id = dato
End Property
''' <summary>
''' Establecer el Id de PRES5_IMPORTES
''' </summary>
''' <returns>PRES5_IMPORTES.ID</returns>
''' <remarks>Llamada desde: cLineaPedido/IBaseDatos_FinalizarEdicionModificando  cOrdenEntrega/IBaseDatos_FinalizarEdicionModificando
''' cRecepcion/ValidarVigenciaPartidas  cOrdenesEntrega/EmitirPedidoConLineasTemporales     frmpedidos/ActualizarCabeceraDesdeLineas
''' ; Tiempo m�ximo: 0,2</remarks>
Public Property Get PRES5Id() As String
    PRES5Id = m_sPRES5Id
End Property
