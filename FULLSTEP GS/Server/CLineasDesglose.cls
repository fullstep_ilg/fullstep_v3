VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CLineasDesglose"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

Private mCol As Collection

Private m_oConexion As CConexion
Private m_bEOF As Boolean

Public Property Get eof() As Boolean
    eof = m_bEOF
End Property
Friend Property Let eof(ByVal b As Boolean)
    m_bEOF = b
End Property

Public Function Add(ByVal Linea As Long, ByVal oCampoHijo As CFormItem, Optional ByVal ValorNum As Variant, _
            Optional ByVal ValorText As Variant, Optional ByVal ValorFec As Variant, Optional ByVal ValorBool As Variant, _
            Optional ByVal FechaActual As Variant, Optional ByVal vIndice As Variant, Optional ByVal oInstancia As CInstancia) As CLineaDesglose
        
    Dim objnewmember As CLineaDesglose
    
    Set objnewmember = New CLineaDesglose
    Set objnewmember.Conexion = m_oConexion
    
    objnewmember.Linea = Linea
    Set objnewmember.CampoHijo = oCampoHijo
    
    If Not IsMissing(ValorNum) Then
        objnewmember.ValorNum = ValorNum
    Else
        objnewmember.ValorNum = Null
    End If
    
    If Not IsMissing(ValorText) Then
        objnewmember.ValorText = ValorText
    Else
        objnewmember.ValorText = Null
    End If
    
    If Not IsMissing(ValorFec) Then
        objnewmember.ValorFec = ValorFec
    Else
        objnewmember.ValorFec = Null
    End If
    
    If Not IsMissing(ValorBool) Then
        objnewmember.ValorBool = ValorBool
    Else
        objnewmember.ValorBool = Null
    End If
    
    If IsMissing(FechaActual) Then
        objnewmember.FecAct = Null
    Else
        objnewmember.FecAct = FechaActual
    End If
    
    Set objnewmember.Instancia = oInstancia
    
    If Not IsMissing(vIndice) And Not IsNull(vIndice) Then
        objnewmember.Indice = vIndice
        mCol.Add objnewmember, CStr(vIndice)
    Else
        mCol.Add objnewmember, CStr(Linea) & "$" & CStr(oCampoHijo.CampoPadre.Id) & "$" & CStr(oCampoHijo.Id)
    End If

    Set Add = objnewmember
    
    Set objnewmember = Nothing

End Function

Friend Property Set Conexion(ByVal vData As CConexion)
    Set m_oConexion = vData
End Property

Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property

Public Property Get Item(vntIndexKey As Variant) As CLineaDesglose

    ''' * Objetivo: Recuperar una moneda de la coleccion
    ''' * Recibe: Indice de la moneda a recuperar
    ''' * Devuelve: Moneda correspondiente
        
On Error GoTo NoSeEncuentra:

    ''' Devolvemos el item de la coleccion privada
    
    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:

    Set Item = Nothing
    
End Property

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4

    ''' ! Pendiente de revision, objetivo desconocido
    
    Set NewEnum = mCol.[_NewEnum]

End Property
Public Sub Remove(vntIndexKey As Variant)

    ''' * Objetivo: Eliminar una moneda de la coleccion
    ''' * Recibe: Indice de la moneda a eliminar
    ''' * Devuelve: Nada
   
    mCol.Remove vntIndexKey
    
End Sub
Private Sub Class_Initialize()

    ''' * Objetivo: Crear la coleccion
    Set mCol = New Collection
    
End Sub
Private Sub Class_Terminate()

    ''' * Objetivo: Limpiar memoria
    
    Set mCol = Nothing
    Set m_oConexion = Nothing
End Sub

Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
     Count = mCol.Count
End If

End Property

Public Function EliminarLineasDesglose(ByVal Linea As Long, ByVal Padre As Long) As TipoErrorSummit
Dim sConsulta As String
Dim TESError As TipoErrorSummit

    Dim i, nLineas As Long
    Dim rs As adodb.Recordset

    Set rs = New adodb.Recordset

'********* Precondicion *******************
If m_oConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CLineasDesglose.EliminarLineasDesglose", "No se ha establecido la conexion"
End If

'******************************************

On Error GoTo Error:
    
    TESError.NumError = TESnoerror
    
    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
    
    sConsulta = "DELETE FROM LINEA_DESGLOSE_ADJUN WHERE LINEA=" & Linea & " AND CAMPO_PADRE=" & Padre
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error
    
    sConsulta = "DELETE FROM LINEA_DESGLOSE WHERE LINEA=" & Linea & " AND CAMPO_PADRE=" & Padre
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error
    
    sConsulta = " SELECT COUNT(*) nLineas FROM LINEA_DESGLOSE_ADJUN WHERE LINEA > " & Linea & " AND CAMPO_PADRE=" & Padre
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    
    nLineas = rs("nLineas")
    
    If nLineas > 0 Then
        'El desglose tiene adjuntos
        For i = Linea + 1 To Linea + nLineas
            sConsulta = "INSERT INTO LINEA_DESGLOSE (LINEA,CAMPO_PADRE, CAMPO_HIJO, VALOR_NUM, VALOR_FEC, VALOR_TEXT, VALOR_BOOL)"
            sConsulta = sConsulta & " SELECT " & (i - 1) & ", CAMPO_PADRE, CAMPO_HIJO, VALOR_NUM, VALOR_FEC, VALOR_TEXT, VALOR_BOOL"
            sConsulta = sConsulta & " FROM LINEA_DESGLOSE WITH (nolock)"
            sConsulta = sConsulta & " where CAMPO_PADRE=" & Padre & " AND LINEA = " & i & " ORDER BY LINEA,CAMPO_PADRE,CAMPO_HIJO"
            m_oConexion.ADOCon.Execute sConsulta
            If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error
            
            sConsulta = "INSERT INTO LINEA_DESGLOSE_ADJUN (CAMPO_PADRE, CAMPO_HIJO,LINEA, NOM, RUTA,IDIOMA,DATASIZE,DATA,DGUID,PER,COMENT,FECALTA)"
            sConsulta = sConsulta & " SELECT CAMPO_PADRE, CAMPO_HIJO," & (i - 1) & ", NOM, RUTA,IDIOMA,DATASIZE,DATA,NEWID(),PER,COMENT,FECALTA"
            sConsulta = sConsulta & " FROM LINEA_DESGLOSE_ADJUN WITH (nolock)"
            sConsulta = sConsulta & " where CAMPO_PADRE=" & Padre & " AND LINEA = " & i & " ORDER BY LINEA,CAMPO_PADRE,CAMPO_HIJO"
            m_oConexion.ADOCon.Execute sConsulta
            If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error
            
            sConsulta = "DELETE FROM LINEA_DESGLOSE_ADJUN WHERE LINEA=" & i & " AND CAMPO_PADRE=" & Padre
            m_oConexion.ADOCon.Execute sConsulta
            If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error
            
            sConsulta = "DELETE FROM LINEA_DESGLOSE WHERE LINEA=" & i & " AND CAMPO_PADRE=" & Padre
            m_oConexion.ADOCon.Execute sConsulta
            If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error
        
        Next
    
    Else
        
        sConsulta = "UPDATE LINEA_DESGLOSE SET LINEA=(LINEA - 1) WHERE LINEA > " & Linea & " AND CAMPO_PADRE=" & Padre
        m_oConexion.ADOCon.Execute sConsulta
        If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error
    End If

    rs.Close
    Set rs = Nothing
    
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    EliminarLineasDesglose = TESError
    Exit Function

Error:
    
    EliminarLineasDesglose = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    
    rs.Close
    Set rs = Nothing
    
    m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"

End Function


Public Function ModificarValoresCalculados() As TipoErrorSummit
Dim sConsulta As String
Dim TESError As TipoErrorSummit
Dim oLinea As CLineaDesglose
Dim adoRes As New adodb.Recordset
'********* Precondicion *******************
If m_oConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CLineasDesglose.EliminarLineasDesglose", "No se ha establecido la conexion"
End If

'******************************************

On Error GoTo Error:
    
    TESError.NumError = TESnoerror
    
    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"

    For Each oLinea In Me
        If oLinea.Modificado = True Or oLinea.Anyadido = True Then
            If oLinea.Modificado = True Then
                sConsulta = "UPDATE LINEA_DESGLOSE SET VALOR_NUM=" & DblToSQLFloat(oLinea.ValorNum) & " WHERE LINEA=" & oLinea.Linea & " AND CAMPO_HIJO=" & oLinea.CampoHijo.Id & " AND CAMPO_PADRE=" & oLinea.CampoHijo.CampoPadre.Id
            Else
                sConsulta = "INSERT INTO LINEA_DESGLOSE (LINEA,CAMPO_PADRE,CAMPO_HIJO,VALOR_NUM)"
                sConsulta = sConsulta & " VALUES (" & oLinea.Linea & "," & oLinea.CampoHijo.CampoPadre.Id & "," & oLinea.CampoHijo.Id & "," & DblToSQLFloat(oLinea.ValorNum) & ")"
            End If
            m_oConexion.ADOCon.Execute sConsulta
            If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error
            
            sConsulta = "SELECT FECACT FROM LINEA_DESGLOSE WHERE LINEA=" & oLinea.Linea & " AND CAMPO_HIJO=" & oLinea.CampoHijo.Id & " AND CAMPO_PADRE=" & oLinea.CampoHijo.CampoPadre.Id
            adoRes.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
            oLinea.FecAct = adoRes.Fields("FECACT").Value
            adoRes.Close
        End If
    Next
    
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    ModificarValoresCalculados = TESError
    
    Set adoRes = Nothing
    Exit Function

Error:
    
    ModificarValoresCalculados = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    
    m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
    
    Set adoRes = Nothing
End Function

