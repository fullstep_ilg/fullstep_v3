VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CPlantConfVistaAllAtrib"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CPlantConfVistaAllAtrib **********************************
'*             Autor : Mertxe Mart�n
'*             Creada : 01/12/2003
'***************************************************************

Option Explicit

Implements IBaseDatos

Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum

Private m_oPlantConfVistaAll As CPlantConfVistaAll
Private m_iVista As Integer

Private m_lAtrib As Long
Private m_bVisible As Boolean
Private m_dblWidth As Double
Private m_iPosicion As Variant
Private m_iFila As Integer

Private m_sCodAtrib As String
Private m_sDenAtrib As String

Private m_oConexion As CConexion
Private m_oConexionServer As CConexionDeUseServer
Private m_vIndice As Variant
Private m_adores As adodb.Recordset

Public Property Let Atributo(ByVal lAtrib As Long)
    m_lAtrib = lAtrib
End Property

Public Property Get Atributo() As Long
    Atributo = m_lAtrib
End Property

Public Property Let Vista(ByVal iVista As Integer)
    m_iVista = iVista
End Property

Public Property Get Vista() As Integer
    Vista = m_iVista
End Property

Public Property Set PlantConfVistaAll(ByVal oDato As CPlantConfVistaAll)
    Set m_oPlantConfVistaAll = oDato
End Property
Public Property Get PlantConfVistaAll() As CPlantConfVistaAll
    Set PlantConfVistaAll = m_oPlantConfVistaAll
End Property

Public Property Let Visible(ByVal dato As Boolean)
    m_bVisible = dato
End Property

Public Property Get Visible() As Boolean
    Visible = m_bVisible
End Property

Public Property Let Width(ByVal dato As Double)
    m_dblWidth = dato
End Property

Public Property Get Width() As Double
    Width = m_dblWidth
End Property

Public Property Let Posicion(ByVal dato As Variant)
    m_iPosicion = dato
End Property

Public Property Get Posicion() As Variant
    Posicion = m_iPosicion
End Property

Public Property Let Fila(ByVal dato As Integer)
    m_iFila = dato
End Property

Public Property Get Fila() As Integer
    Fila = m_iFila
End Property


Public Property Let CodAtributo(ByVal dato As String)
    m_sCodAtrib = dato
End Property

Public Property Get CodAtributo() As String
    CodAtributo = m_sCodAtrib
End Property

Public Property Let DenAtributo(ByVal dato As String)
    m_sDenAtrib = dato
End Property

Public Property Get DenAtributo() As String
    DenAtributo = m_sDenAtrib
End Property

Friend Property Set ConexionServer(ByVal con As CConexionDeUseServer)
    Set m_oConexionServer = con
End Property

Friend Property Get ConexionServer() As CConexionDeUseServer
    Set ConexionServer = m_oConexionServer
End Property

Friend Property Set Conexion(ByVal con As CConexion)
    Set m_oConexion = con
End Property

Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property

Public Property Let Indice(ByVal vIndice As Variant)
    m_vIndice = vIndice
End Property
Public Property Get Indice() As Variant
    Indice = m_vIndice
End Property

Private Function IBaseDatos_AnyadirABaseDatos() As TipoErrorSummit
Dim sConsulta As String
Dim TESError As TipoErrorSummit

'********* Precondicion *******************
If m_oConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CPlantillaConfVistaAllAtrib.AnyadirABAseDatos", "No se ha establecido la conexion"
End If

'******************************************

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------


If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
    
    TESError.NumError = TESnoerror
    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
    
    sConsulta = "INSERT INTO PLANTILLA_CONF_VISTAS_ALL_ATRIB (PLANT,VISTA,ATRIB,VISIBLE,WIDTH,POSICION,FILA)"
    sConsulta = sConsulta & " VALUES (" & m_oPlantConfVistaAll.Plantilla.Id
    sConsulta = sConsulta & "," & m_oPlantConfVistaAll.Vista & "," & m_lAtrib
    sConsulta = sConsulta & "," & BooleanToSQLBinary(m_bVisible) & "," & DblToSQLFloat(m_dblWidth)
    sConsulta = sConsulta & "," & NullToDbl0(m_iPosicion) & "," & m_iFila & ")"
    
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    IBaseDatos_AnyadirABaseDatos = TESError
    Exit Function
    
Error_Cls:
    
    IBaseDatos_AnyadirABaseDatos = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
    On Error Resume Next
    If Not m_adores Is Nothing Then
        m_adores.Close
        Set m_adores = Nothing
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CPlantConfVistaAllAtrib", "IBaseDatos_AnyadirABaseDatos", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If

End Function

Private Function IBaseDatos_CambiarCodigo(CodigoNuevo As Variant) As TipoErrorSummit

End Function

Private Sub IBaseDatos_CancelarEdicion()

End Sub

''' <summary>
''' Funci�n que comprueba si est�n la plantilla, vista y atributo en la tabla
''' </summary>
''' <returns>Boolean</returns>
''' <remarks>Llamada desde: FSGSClient; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 26/12/2011</revision>
Private Function IBaseDatos_ComprobarExistenciaEnBaseDatos() As Boolean
    Dim sConsulta As String
    Dim AdoRes As New adodb.Recordset
    
    '******************* Precondicion *******************
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CPlantillaConfVistaAllAtrib.ComprobarExistenciaEnBaseDatos", "No se ha establecido la conexion"
    End If
    '*****************************************************
    
    If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
    
    sConsulta = "SELECT PLANT,VISTA,ATRIB FROM PLANTILLA_CONF_VISTAS_ALL_ATRIB WITH(NOLOCK) WHERE "
    sConsulta = sConsulta & " PLANT=" & m_oPlantConfVistaAll.Plantilla.Id & " AND VISTA=" & m_oPlantConfVistaAll.Vista
    sConsulta = sConsulta & " AND ATRIB=" & (m_lAtrib)

    Set AdoRes = New adodb.Recordset
    AdoRes.CursorLocation = adUseServer
    AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    If AdoRes.eof Then
        IBaseDatos_ComprobarExistenciaEnBaseDatos = False
    Else
        IBaseDatos_ComprobarExistenciaEnBaseDatos = True
    End If

    AdoRes.Close
    Set AdoRes = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CPlantConfVistaAllAtrib", "IBaseDatos_ComprobarExistenciaEnBaseDatos", ERR, Erl)
      Exit Function
   End If
End Function


Private Function IBaseDatos_EliminarDeBaseDatos() As TipoErrorSummit
Dim sConsulta As String
Dim TESError As TipoErrorSummit

    '''Precondicion
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CPlantillaConfVistaAllAtrib.EliminarDeBaseDatos", "No se ha establecido la conexion"
    End If
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:

    TESError.NumError = TESnoerror
     
    sConsulta = "DELETE FROM PLANTILLA_CONF_VISTAS_ALL_ATRIB WHERE "
    sConsulta = sConsulta & " PLANT=" & m_oPlantConfVistaAll.Plantilla.Id
    sConsulta = sConsulta & " AND VISTA=" & m_oPlantConfVistaAll.Vista & " AND ATRIB =" & m_lAtrib
    
    m_oConexion.ADOCon.Execute sConsulta

    IBaseDatos_EliminarDeBaseDatos = TESError

    Exit Function

Error_Cls:
    IBaseDatos_EliminarDeBaseDatos = basErrores.TratarError(m_oConexion.ADOCon.Errors)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CPlantConfVistaAllAtrib", "IBaseDatos_EliminarDeBaseDatos", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If

End Function


Private Function IBaseDatos_FinalizarEdicionEliminando() As TipoErrorSummit

End Function


Private Function IBaseDatos_FinalizarEdicionModificando() As TipoErrorSummit
Dim TESError As TipoErrorSummit
Dim sConsulta As String

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
    TESError.NumError = TESnoerror

    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"


    sConsulta = "UPDATE PLANTILLA_CONF_VISTAS_ALL_ATRIB SET "
    sConsulta = sConsulta & " VISIBLE=" & BooleanToSQLBinary(m_bVisible) & ",WIDTH=" & DblToSQLFloat(m_dblWidth)
    sConsulta = sConsulta & " POSICION=" & m_iPosicion & ",FILA=" & m_iFila
    sConsulta = sConsulta & " WHERE "
    sConsulta = sConsulta & " PLANT=" & m_oPlantConfVistaAll.Plantilla.Id
    sConsulta = sConsulta & " AND VISTA=" & m_oPlantConfVistaAll.Vista & " AND ATRIB=" & m_lAtrib

    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls

    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"

    IBaseDatos_FinalizarEdicionModificando = TESError

    Exit Function

Error_Cls:
    IBaseDatos_FinalizarEdicionModificando = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"

    On Error Resume Next
    If Not m_adores Is Nothing Then
        m_adores.Close
        Set m_adores = Nothing
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CPlantConfVistaAllAtrib", "IBaseDatos_FinalizarEdicionModificando", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If

End Function

Private Function IBaseDatos_IniciarEdicion(Optional ByVal Bloquear As Boolean, Optional ByVal UsuarioBloqueo As String) As TipoErrorSummit

End Function

Private Sub ConectarDeUseServer()

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If m_oConexionServer.ADOSummitCon Is Nothing Then
        Set m_oConexionServer.ADOSummitCon = New adodb.Connection
        m_oConexionServer.ADOSummitCon.Open "Provider=SQLOLEDB.1;Server=" & basParametros.ServidorGS & ";Database=" & basParametros.BaseDatosGS & ";", LICDBLogin, LICDBContra
        m_oConexionServer.ADOSummitCon.CursorLocation = adUseServer
        m_oConexionServer.ADOSummitCon.CommandTimeout = 120
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CPlantConfVistaAllAtrib", "ConectarDeUseServer", ERR, Erl)
      Exit Sub
   End If
End Sub


Private Sub Class_Terminate()
On Error GoTo Error_Cls:
    Set m_oConexionServer = Nothing
    Set m_oConexion = Nothing
    Exit Sub
Error_Cls:
    Resume Next
End Sub


Public Function VistaAnyadirABaseDatos(ByVal iVista As Integer) As TipoErrorSummit
Dim sConsulta As String
Dim sConsultaVista As String
Dim TESError As TipoErrorSummit
Dim AdoRes As New adodb.Recordset


'********* Precondicion *******************
If m_oConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CConfVistaProce.AnyadirABAseDatos", "No se ha establecido la conexion"
End If
'******************************************

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------


If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
    
    TESError.NumError = TESnoerror
    
    sConsulta = "INSERT INTO PLANTILLA_CONF_VISTAS_ALL_ATRIB (PLANT,VISTA,ATRIB,VISIBLE,WIDTH,POSICION,FILA)"
    sConsulta = sConsulta & " VALUES (" & m_oPlantConfVistaAll.Plantilla.Id
    sConsulta = sConsulta & "," & iVista & "," & m_lAtrib
    sConsulta = sConsulta & "," & BooleanToSQLBinary(m_bVisible) & "," & DblToSQLFloat(m_dblWidth)
    sConsulta = sConsulta & "," & NullToDbl0(m_iPosicion) & "," & m_iFila & ")"
    
    m_oConexion.ADOCon.Execute sConsulta
    
    VistaAnyadirABaseDatos = TESError
    
    Exit Function
    
Error_Cls:
    
    VistaAnyadirABaseDatos = basErrores.TratarError(m_oConexion.ADOCon.Errors)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CPlantConfVistaAllAtrib", "VistaAnyadirABaseDatos", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If

End Function








