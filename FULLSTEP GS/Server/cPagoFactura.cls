VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cPagoFactura"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

Private m_oConexion As CConexion

Private m_lID As Long
Private m_sNum As String
Private m_lIDFactura As Long
Private m_sEstadoDen As String
Private m_dFecha As Date

Public Property Let Id(ByVal dato As Long)
    m_lID = dato
End Property
Public Property Get Id() As Long
    Id = m_lID
End Property

Public Property Let Numero(ByVal dato As String)
    m_sNum = dato
End Property
Public Property Get Numero() As String
    Numero = m_sNum
End Property

Public Property Let IdFactura(ByVal dato As Long)
    m_lIDFactura = dato
End Property
Public Property Get IdFactura() As Long
    IdFactura = m_lIDFactura
End Property

Public Property Let EstadoDen(ByVal dato As String)
    m_sEstadoDen = dato
End Property
Public Property Get EstadoDen() As String
    EstadoDen = m_sEstadoDen
End Property

Public Property Let Fecha(ByVal dato As Date)
    m_dFecha = dato
End Property
Public Property Get Fecha() As Date
    Fecha = m_dFecha
End Property





Friend Property Set Conexion(ByVal vData As CConexion)

    ''' * Objetivo: Dar valor a la conexion de la Destino
    ''' * Recibe: Conexion
    ''' * Devuelve: Nada

    Set m_oConexion = vData
    
End Property
Friend Property Get Conexion() As CConexion

    ''' * Objetivo: Devolver la conexion de la Destino
    ''' * Recibe: Nada
    ''' * Devuelve: Conexion

    Set Conexion = m_oConexion
    
End Property

Private Sub Class_Terminate()
    
    On Error GoTo Error:
    
    Set m_oConexion = Nothing
    Exit Sub
Error:
    Resume Next
    
End Sub






