VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CContratosPres"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

''' *** Clase: CContratosPres

''' Variables que almacenan las propiedades de los objetos
Private mCol As Collection
Private m_bEOF As Boolean
Private m_oConexion As CConexion

''' Control de errores
Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum

''' <summary>A�ade un contrato a la coleccion</summary>
''' <param name="lId">Id</param>
''' <param name="sCod">Cod</param>
''' <param name="sPRES0">PRES0</param>
''' <param name="sPRES1">PRES1</param>
''' <param name="sPRES2">PRES2</param>
''' <param name="sPRES3">PRES3</param>
''' <param name="sPRES4">PRES4</param>
''' <param name="sDen">Den</param>
''' <param name="vFecIni">Fec. Ini.</param>
''' <param name="vFecFin">Fec. Fin</param>
''' <param name="sUON1">UON1</param>
''' <param name="sUON2">UON2</param>
''' <param name="sUON3">UON3</param>
''' <param name="sUON4">UON4</param>
''' <param name="sDenUON">Den. UON</param>
''' <param name="Cambio">Cambio</param>
''' <param name="Moneda">Moneda</param>
''' <param name="Gestor">Gestor</param>
''' <param name="sPRES5Id">PRES5_IMPORTES.ID</param>
''' <returns>Contrato a�adido</returns>
''' <remarks>Llamada desde: CargarTodosLosContratos</remarks>
''' <revision>LTG 17/04/2012</revision>

Public Function Add(ByVal lId As Long, ByVal sCod As String, ByVal sPRES0 As String, ByVal sPRES1 As String, ByVal sPRES2 As String, ByVal sPRES3 As String, _
        ByVal sPRES4 As String, Optional ByVal sDen As String, Optional ByVal vFecIni As Variant, Optional ByVal vFecFin As Variant, _
        Optional ByVal sUON1 As String, Optional ByVal sUON2 As String, Optional ByVal sUON3 As String, Optional ByVal sUON4 As String, _
        Optional ByVal sDenUON As String, Optional ByVal Cambio As Double, Optional ByVal Moneda As String, Optional ByVal Gestor As String, _
        Optional ByVal sPRES5Id As String = "") As CContratoPres
    Dim objnewmember As CContratoPres

    ''' Creacion de objeto �rbol
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New CContratoPres
   
    ''' Paso de los parametros al nuevo objeto �rbol.
    objnewmember.Cod = sCod
    objnewmember.PRES0 = sPRES0
    objnewmember.PRES1 = sPRES1
    objnewmember.PRES2 = sPRES2
    objnewmember.Pres3 = sPRES3
    objnewmember.Pres4 = sPRES4
    objnewmember.PRES5Id = sPRES5Id
    objnewmember.UON1 = sUON1
    objnewmember.UON2 = sUON2
    objnewmember.UON3 = sUON3
    objnewmember.UON4 = sUON4
    objnewmember.DenUON = sDenUON
    
    objnewmember.FecIni = vFecIni
    objnewmember.FecFin = vFecFin
    
    If Not IsMissing(sDen) And Not IsNull(sDen) Then
        objnewmember.Den = sDen
    Else
        objnewmember.Den = ""
    End If
    
    If Not IsMissing(Cambio) And Not IsNull(Cambio) Then
        objnewmember.Cambio = Cambio
    Else
        objnewmember.Cambio = 1
    End If
    
    If Not IsMissing(Moneda) And Not IsNull(Moneda) Then
        objnewmember.Moneda = Moneda
    End If
    
    If Not IsMissing(Gestor) Then
        objnewmember.Gestor = Gestor
    End If
    
    Set objnewmember.Conexion = m_oConexion
        
    ''' A�adir el objeto contrato a la coleccion
    ''' Si no se especifica indice, se a�ade al final
    If lId <> 0 Then
        mCol.Add objnewmember, CStr(lId)
    Else
        mCol.Add objnewmember
    End If
    
    Set Add = objnewmember
    
    Set objnewmember = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CContratosPres", "Add", ERR, Erl)
        Exit Function
    End If

End Function


Friend Property Set Conexion(ByVal con As CConexion)

    ''' * Objetivo: Dar valor a la variable privada Conexion
    ''' * Recibe: Conexion
    ''' * Devuelve: Nada
    
    Set m_oConexion = con
    
End Property
Friend Property Get Conexion() As CConexion

    ''' * Objetivo: Devolver variable privada Conexion
    ''' * Recibe: Nada
    ''' * Devuelve: Conexion
    
    Set Conexion = m_oConexion
    
End Property


Public Property Get Count() As Long

    ''' * Objetivo: Devolver variable privada Count
    ''' * Recibe: Nada
    ''' * Devuelve: Numero de elementos de la coleccion

    If mCol Is Nothing Then
        Count = 0
    Else
        Count = mCol.Count
    End If

End Property

Public Property Get Item(vntIndexKey As Variant) As CContratoPres

    ''' * Objetivo: Recuperar un contrato de la coleccion
    ''' * Recibe: Indice del contrato a recuperar
    ''' * Devuelve: Contrato correspondiente
        
On Error GoTo NoSeEncuentra:

    ''' Devolvemos el item de la coleccion privada
    
    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:

    Set Item = Nothing
    
End Property

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
     Set NewEnum = mCol.[_NewEnum]
End Property


''' <summary>Carga todos los contratos que pueden estar filtrados por distintos conceptos.</summary>
''' <param name="sArbol">�rbol de partidas que estamos manejando.</param>
''' <param name="iNivel">Nivel al que se est� trabajando para los contratos.</param>
''' <param name="sUsu">Usuario. Para ver si hay restricciones</param>
''' <param name="sIdi">Idioma</param>
''' <param name="UON1">Centro coste nivel 1</param>
''' <param name="UON2">Centro coste nivel 2</param>
''' <param name="UON3">Centro coste nivel 3</param>
''' <param name="UON4">Centro coste nivel 4</param>
''' <param name="sContrato">Contrato</param>
''' <param name="lIdEmpresa">Id Empresa</param>
''' <param name="bSoloAbiertos">para sacar Solo por los contratos abiertos</param>
''' <param name="bRPerfUON">Restringir a las UON del perfil del usuario</param>
''' <param name="lIdPerf">Id perfil</param>
''' <param name="bSaberSiContrato">Saber si hay mas de un contrato para el arbol</param>
''' <remarks>Llamada desde:frmPedidos; frmSeguimiento; frmLstSolicitud; frmSolicitudBuscar; frmSolicitudes. </remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>
''' <revision>LTG 17/04/2012</revision>

Public Sub CargarTodosLosContratos(ByVal sArbol As String, ByVal iNivel As Integer, ByVal sUsu As String, ByVal sIdi As String, Optional ByVal UON1 As String = "", _
        Optional ByVal UON2 As String = "", Optional ByVal UON3 As String = "", Optional ByVal UON4 As String = "", Optional ByVal sContrato As String = "", _
        Optional ByVal lIdEmpresa As Long, Optional ByVal bSoloAbiertos As Boolean = True, Optional ByVal bRPerfUON As Boolean, Optional ByVal lIdPerf As Long, _
        Optional ByVal bSaberSiContrato As Boolean = False)
    Dim rs As New adodb.Recordset
    Dim fldCod As adodb.Field
    Dim fldDen As adodb.Field
    Dim fldPres0 As adodb.Field
    Dim fldPres1 As adodb.Field
    Dim fldPres2 As adodb.Field
    Dim fldPres3 As adodb.Field
    Dim fldPres4 As adodb.Field
    Dim fldPres5Id As adodb.Field
    Dim fldFecini As adodb.Field
    Dim fldFecfin As adodb.Field
    Dim fldCambio As adodb.Field
    Dim fldMoneda As adodb.Field
    Dim fldGestor As adodb.Field
    Dim sUON As String
    Dim bRUO As Boolean
    Dim sSelectTodos As String
    Dim sSelectUnico As String
    Dim sConsulta As String
    Dim sOrder As String
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set mCol = Nothing
    Set mCol = New Collection
    
    sSelectTodos = " SELECT DISTINCT  N.COD COD ,PRES_IDI.DEN DEN , PU.PRES0, PU.PRES1, PU.PRES2, PU.PRES3, PU.PRES4"
    sSelectTodos = sSelectTodos & ",M.FECINI, M.FECFIN, MON.EQUIV AS CAMBIO, M.MON AS MONEDA, M.GESTOR, M.ID AS PRES5_IMP"

    sSelectUnico = "SELECT COUNT(1) AS N"

    sConsulta = ""
    bRUO = (UON1 <> "" Or UON2 <> "" Or UON3 <> "" Or UON4 <> "")
    If bRUO Or bRPerfUON Then
        sUON = ConstruirCriterioUONUsuPerfContratos(bRUO, bRPerfUON, UON1, UON2, UON3, UON4, lIdPerf)
        sConsulta = sConsulta & " FROM (" & sUON & ") PU"
    Else
        sConsulta = sConsulta & " FROM PRES5_UON PU WITH (NOLOCK)"
    End If
    If sUsu <> "" Then
        sConsulta = sConsulta & " INNER JOIN USU_CC_IMPUTACION S WITH (NOLOCK) ON S.USU = '" & DblQuote(sUsu) & "' and PU.UON1=S.UON1 AND ISNULL(PU.UON2,0)=ISNULL(S.UON2,0) AND ISNULL(PU.UON3,0)=ISNULL(S.UON3,0) AND ISNULL(PU.UON4,0)=ISNULL(S.UON4,0) "
    End If

    sConsulta = sConsulta & " INNER JOIN PRES5_NIV" & iNivel & " N WITH (NOLOCK) ON PU.PRES0=N.PRES0 "
    Select Case iNivel
    Case 1
        sConsulta = sConsulta & " AND PU.PRES1=N.COD "
    Case 2
        sConsulta = sConsulta & " AND PU.PRES1=N.PRES1 AND PU.PRES2=N.COD "
    Case 3
        sConsulta = sConsulta & " AND PU.PRES1=N.PRES1 AND PU.PRES2=N.PRES2 AND PU.PRES3=N.COD "
    Case 4
        sConsulta = sConsulta & " AND PU.PRES1=N.PRES1 AND PU.PRES2=N.PRES2 AND PU.PRES3=N.PRES3 AND PU.PRES4=N.COD "
    End Select

    sConsulta = sConsulta & " INNER JOIN PRES5_IMPORTES M WITH (NOLOCK) ON PU.PRES0=M.PRES0 AND ISNULL(PU.PRES1,0)=ISNULL(M.PRES1,0) AND ISNULL(PU.PRES2,0)=ISNULL(M.PRES2,0) AND ISNULL(PU.PRES3,0)=ISNULL(M.PRES3,0) AND ISNULL(PU.PRES4,0)=ISNULL(M.PRES4,0)"
    
    sConsulta = sConsulta & " LEFT JOIN MON WITH (NOLOCK) ON M.MON = MON.COD"
    
    If lIdEmpresa > 0 Then
        sConsulta = sConsulta & " LEFT JOIN UON1 U1 WITH (NOLOCK) ON U1.COD = PU.UON1 AND U1.EMPRESA = " & lIdEmpresa

        sConsulta = sConsulta & " LEFT JOIN UON2 U2 WITH (NOLOCK) ON U2.UON1 = PU.UON1 AND U2.COD = PU.UON2 AND (U2.EMPRESA = " & lIdEmpresa & " OR U1.EMPRESA = " & lIdEmpresa & ") "

        sConsulta = sConsulta & " LEFT JOIN UON3 U3 WITH (NOLOCK) ON U3.UON1 = PU.UON1 AND U3.UON2 = PU.UON2 AND U3.COD = PU.UON3 AND (U3.EMPRESA = " & lIdEmpresa & " OR U2.EMPRESA = " & lIdEmpresa & " OR U1.EMPRESA = " & lIdEmpresa & " ) "

        sConsulta = sConsulta & " LEFT JOIN UON4 U4 WITH (NOLOCK) ON U4.UON1 = PU.UON1 AND U4.UON2 = PU.UON2 AND U4.UON3 = PU.UON3 AND U4.COD = PU.UON4 AND (U4.EMPRESA = " & lIdEmpresa & " OR U3.EMPRESA = " & lIdEmpresa & " OR U2.EMPRESA = " & lIdEmpresa & " OR U1.EMPRESA = " & lIdEmpresa & " ) "
    End If
    
    sConsulta = sConsulta & " LEFT JOIN PRES5_IDIOMAS PRES_IDI WITH (NOLOCK) ON PU.PRES0=PRES_IDI.PRES0 AND PU.PRES1=PRES_IDI.PRES1 AND ISNULL(PU.PRES2,0)=ISNULL(PRES_IDI.PRES2,0) AND ISNULL(PU.PRES3,0)=ISNULL(PRES_IDI.PRES3,0) AND ISNULL(PU.PRES4,0)=ISNULL(PRES_IDI.PRES4,0) AND PRES_IDI.IDIOMA='" & DblQuote(sIdi) & "'"

    sConsulta = sConsulta & " WHERE 1 = 1"
    
    If bSoloAbiertos Then
        sConsulta = sConsulta & " AND M.CERRADO=0 "
    End If

    Select Case iNivel
        Case 1
            If sContrato <> "" Then
                sConsulta = sConsulta & " AND PU.PRES1='" & DblQuote(sContrato) & "' "
            End If
            sConsulta = sConsulta & " AND PU.PRES2 IS NULL AND PU.PRES3 IS NULL AND PU.PRES4 IS NULL "
        Case 2
            If sContrato <> "" Then
                sConsulta = sConsulta & " AND PU.PRES2='" & DblQuote(sContrato) & "' "
            End If
            sConsulta = sConsulta & " AND PU.PRES3 IS NULL AND PU.PRES4 IS NULL "
        Case 3
            If sContrato <> "" Then
                sConsulta = sConsulta & " AND PU.PRES3='" & DblQuote(sContrato) & "' "
            End If
            sConsulta = sConsulta & " AND PU.PRES4 IS NULL "
        Case 4
            If sContrato <> "" Then
                sConsulta = sConsulta & " AND PU.PRES4='" & DblQuote(sContrato) & "'"
            End If
    End Select
    
    sConsulta = sConsulta & " AND PU.PRES0='" & DblQuote(sArbol) & "' "

    sOrder = " ORDER BY PU.PRES1,PU.PRES2,PU.PRES3,PU.PRES4"

    If bSaberSiContrato Then
        
        rs.Open sSelectUnico & sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
        
        If Not rs.eof Then
            If rs.Fields("N").Value > 1 Then
                rs.Close
                Set rs = Nothing
                Exit Sub
            End If
        Else
            Set rs = Nothing
            Exit Sub
        End If
        
        rs.Close
        Set rs = New adodb.Recordset
    End If
    
    rs.Open sSelectTodos & sConsulta & sOrder, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    
    Set mCol = Nothing
    Set mCol = New Collection
        
    If Not rs.eof Then
        Set fldCod = rs.Fields("COD")
        Set fldDen = rs.Fields("DEN")
        Set fldPres0 = rs.Fields("PRES0")
        Set fldPres1 = rs.Fields("PRES1")
        Set fldPres2 = rs.Fields("PRES2")
        Set fldPres3 = rs.Fields("PRES3")
        Set fldPres4 = rs.Fields("PRES4")
        Set fldPres5Id = rs.Fields("PRES5_IMP")
        Set fldFecini = rs.Fields("FECINI")
        Set fldFecfin = rs.Fields("FECFIN")
        Set fldCambio = rs.Fields("CAMBIO")
        Set fldMoneda = rs.Fields("MONEDA")
        Set fldGestor = rs.Fields("GESTOR")
                
        While Not rs.eof
            'Se a�ade el contrato a la colecci�n.
            Me.Add 0, fldCod.Value, fldPres0.Value, NullToStr(fldPres1.Value), NullToStr(fldPres2.Value), NullToStr(fldPres3.Value), NullToStr(fldPres4.Value), NullToStr(fldDen.Value), fldFecini.Value, fldFecfin.Value, , , , , , NullToDbl0(fldCambio), NullToStr(fldMoneda), NullToStr(fldGestor.Value), NullToStr(fldPres5Id.Value)
            rs.MoveNext
        Wend

        Set fldCod = Nothing
        Set fldPres0 = Nothing
        Set fldPres1 = Nothing
        Set fldPres2 = Nothing
        Set fldPres3 = Nothing
        Set fldPres4 = Nothing
        Set fldPres5Id = Nothing
        Set fldCambio = Nothing
        Set fldMoneda = Nothing
        Set fldGestor = Nothing
    End If
    
    rs.Close
    Set rs = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CContratosPres", "CargarTodosLosContratos", ERR, Erl)
        Exit Sub
    End If
End Sub


''' <summary>
''' Carga todos los contratos que pueden estar filtrados por distintos conceptos.
''' </summary>
''' <param name="sArbol">�rbol de partidas que estamos manejando.</param>
''' <param name="iNivel">Nivel al que se est� trabajando para los contratos.</param>
''' <param name="sUsu">Usuario. Para ver si hay restricciones</param>
''' <param name="sIdi">Idioma</param>
''' <param name="UON1">Centro coste nivel 1</param>
''' <param name="UON2">Centro coste nivel 2</param>
''' <param name="UON3">Centro coste nivel 3</param>
''' <param name="UON4">Centro coste nivel 4</param>
''' <param name="sContrato">Contrato</param>
''' <param name="lIdEmpresa">Id Empresa</param>
''' <param name="bSoloAbiertos">para sacar Solo por los contratos abiertos</param>
''' <returns></returns>
''' <remarks>Llamada desde: cLineaPedido.cls --> ValidarPart; COrdenEntrega.ValidaPart; frmSeguimiento.txtPartida_Validate; frmSeguimiento.sdbgPartidas_BeforeRowColChange. </remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>
''' Revisado por: jbg; Fecha: 08/03/2013
Public Sub CargarTodosLosContratosCentros(ByVal sArbol As String, ByVal iNivel As Integer, ByVal sUsu As String, ByVal sIdi As String, Optional ByVal UON1 As String = "", Optional ByVal UON2 As String = "", Optional ByVal UON3 As String = "", Optional ByVal UON4 As String = "", Optional ByVal sContrato As String = "", Optional ByVal lIdEmpresa As Long, Optional ByVal bSoloAbiertos As Boolean = True)
    Dim rs As New adodb.Recordset
    
    Dim fldId As adodb.Field
    Dim fldCod As adodb.Field
    Dim fldDen As adodb.Field
    Dim fldPres0 As adodb.Field
    Dim fldPres1 As adodb.Field
    Dim fldPres2 As adodb.Field
    Dim fldPres3 As adodb.Field
    Dim fldPres4 As adodb.Field
    Dim fldPres5Id As adodb.Field
    
    Dim fldUON1 As adodb.Field
    Dim fldUON2 As adodb.Field
    Dim fldUON3 As adodb.Field
    Dim fldUON4 As adodb.Field
    Dim fldU1Den As adodb.Field
    Dim fldU2Den As adodb.Field
    Dim fldU3Den As adodb.Field
    Dim fldU4Den As adodb.Field
        
    Dim fldFecini As adodb.Field
    Dim fldFecfin As adodb.Field
    Dim sConsulta As String
    Dim fldCambio As adodb.Field
    Dim fldMoneda As adodb.Field
    

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set mCol = Nothing
    Set mCol = New Collection
    
    sConsulta = " SELECT DISTINCT PU.ID, PU.UON1, PU.UON2, PU.UON3,PU.UON4, PU.PRES0, PU.PRES1, PU.PRES2, PU.PRES3, PU.PRES4"
    sConsulta = sConsulta & " ,U1.DEN U1DEN,U2.DEN U2DEN,U3.DEN U3DEN,U4.DEN U4DEN"
    sConsulta = sConsulta & " ,N.COD ,PRES_IDI.DEN , M.FECINI, M.FECFIN, MON.EQUIV AS CAMBIO, M.MON AS MONEDA, M.ID AS PRES5_IMP"

    sConsulta = sConsulta & " FROM PRES5_UON  PU WITH (NOLOCK)"
    If sUsu <> "" Then
        sConsulta = sConsulta & " INNER JOIN USU_CC_IMPUTACION S WITH (NOLOCK) ON S.USU = '" & DblQuote(sUsu) & "' and PU.UON1=S.UON1 AND ISNULL(PU.UON2,0)=ISNULL(S.UON2,0) AND ISNULL(PU.UON3,0)=ISNULL(S.UON3,0) AND ISNULL(PU.UON4,0)=ISNULL(S.UON4,0) "
    End If

    sConsulta = sConsulta & " INNER JOIN PRES5_NIV" & iNivel & " N WITH (NOLOCK) ON PU.PRES0=N.PRES0 "
    Select Case iNivel
    Case 1
        sConsulta = sConsulta & " AND PU.PRES1=N.COD "
    Case 2
        sConsulta = sConsulta & " AND PU.PRES1=N.PRES1 AND PU.PRES2=N.COD "
    Case 3
        sConsulta = sConsulta & " AND PU.PRES1=N.PRES1 AND PU.PRES2=N.PRES2 AND PU.PRES3=N.COD "
    Case 4
        sConsulta = sConsulta & " AND PU.PRES1=N.PRES1 AND PU.PRES2=N.PRES2 AND PU.PRES3=N.PRES3 AND PU.PRES4=N.COD "
    End Select


    sConsulta = sConsulta & " INNER JOIN PRES5_IMPORTES M WITH (NOLOCK) ON PU.PRES0=M.PRES0 AND ISNULL(PU.PRES1,0)=ISNULL(M.PRES1,0) AND ISNULL(PU.PRES2,0)=ISNULL(M.PRES2,0) AND ISNULL(PU.PRES3,0)=ISNULL(M.PRES3,0) AND ISNULL(PU.PRES4,0)=ISNULL(M.PRES4,0)"
    
    sConsulta = sConsulta & " LEFT JOIN MON WITH (NOLOCK) ON M.MON = MON.COD"
    
    sConsulta = sConsulta & " LEFT JOIN UON1 U1 WITH (NOLOCK) ON U1.COD = PU.UON1"
    If lIdEmpresa > 0 Then
        sConsulta = sConsulta & " AND U1.EMPRESA = " & lIdEmpresa
    End If
    sConsulta = sConsulta & " LEFT JOIN UON2 U2 WITH (NOLOCK) ON U2.UON1 = PU.UON1 AND U2.COD = PU.UON2"
    If lIdEmpresa > 0 Then
        sConsulta = sConsulta & " AND (U2.EMPRESA = " & lIdEmpresa & " OR U1.EMPRESA = " & lIdEmpresa & ") "
    End If
    sConsulta = sConsulta & " LEFT JOIN UON3 U3 WITH (NOLOCK) ON U3.UON1 = PU.UON1 AND U3.UON2 = PU.UON2 AND U3.COD = PU.UON3"
    If lIdEmpresa > 0 Then
        sConsulta = sConsulta & " AND (U3.EMPRESA = " & lIdEmpresa & " OR U2.EMPRESA = " & lIdEmpresa & " OR U1.EMPRESA = " & lIdEmpresa & " ) "
    End If
    sConsulta = sConsulta & " LEFT JOIN UON4 U4 WITH (NOLOCK) ON U4.UON1 = PU.UON1 AND U4.UON2 = PU.UON2 AND U4.UON3 = PU.UON3 AND U4.COD = PU.UON4"
    If lIdEmpresa > 0 Then
        sConsulta = sConsulta & " AND (U4.EMPRESA = " & lIdEmpresa & " OR U3.EMPRESA = " & lIdEmpresa & " OR U2.EMPRESA = " & lIdEmpresa & " OR U1.EMPRESA = " & lIdEmpresa & " ) "
    End If

    sConsulta = sConsulta & " LEFT JOIN PRES5_IDIOMAS PRES_IDI WITH (NOLOCK) ON PU.PRES0=PRES_IDI.PRES0 AND PU.PRES1=PRES_IDI.PRES1 AND ISNULL(PU.PRES2,0)=ISNULL(PRES_IDI.PRES2,0) AND ISNULL(PU.PRES3,0)=ISNULL(PRES_IDI.PRES3,0) AND ISNULL(PU.PRES4,0)=ISNULL(PRES_IDI.PRES4,0) AND PRES_IDI.IDIOMA='" & DblQuote(sIdi) & "'"


    sConsulta = sConsulta & " WHERE 1 = 1"
    
    If bSoloAbiertos Then
        sConsulta = sConsulta & " AND M.CERRADO=0 "
    End If
    
    If UON1 <> "" Then
        sConsulta = sConsulta & " AND PU.UON1 = " & StrToSQLNULL(UON1)
        If UON2 <> "" Then
            sConsulta = sConsulta & " AND PU.UON2 = " & StrToSQLNULL(UON2)
            If UON3 <> "" Then
                sConsulta = sConsulta & " AND PU.UON3 = " & StrToSQLNULL(UON3)
                If UON4 <> "" Then
                    sConsulta = sConsulta & " AND PU.UON4 = " & StrToSQLNULL(UON4)
                Else
                    sConsulta = sConsulta & " AND PU.UON4 IS NULL "
                End If
            Else
                sConsulta = sConsulta & " AND PU.UON3 IS NULL "
            End If
        Else
            sConsulta = sConsulta & " AND PU.UON2 IS NULL "
        End If
    End If
'    If UON2 <> "" Then
'        sConsulta = sConsulta & " AND PU.UON2 = " & StrToSQLNULL(UON2)
'    End If
'    If UON3 <> "" Then
'        sConsulta = sConsulta & " AND PU.UON3 = " & StrToSQLNULL(UON3)
'    End If
'    If UON4 <> "" Then
'        sConsulta = sConsulta & " AND PU.UON4 = " & StrToSQLNULL(UON4)
'    End If

    Select Case iNivel
    Case 1
        If sContrato <> "" Then
            sConsulta = sConsulta & " AND PU.PRES1='" & DblQuote(sContrato) & "' "
        End If
        sConsulta = sConsulta & " AND PU.PRES2 IS NULL AND PU.PRES3 IS NULL AND PU.PRES4 IS NULL "
    Case 2
        If sContrato <> "" Then
            sConsulta = sConsulta & " AND PU.PRES2='" & DblQuote(sContrato) & "' "
        End If
        sConsulta = sConsulta & " AND PU.PRES3 IS NULL AND PU.PRES4 IS NULL "
    Case 3
        If sContrato <> "" Then
            sConsulta = sConsulta & " AND PU.PRES3='" & DblQuote(sContrato) & "' "
        End If
        sConsulta = sConsulta & " AND PU.PRES4 IS NULL "
    Case 4
        If sContrato <> "" Then
            sConsulta = sConsulta & " AND PU.PRES4='" & DblQuote(sContrato) & "'"
        End If
    End Select
    
    sConsulta = sConsulta & " AND PU.PRES0='" & DblQuote(sArbol) & "' "

    sConsulta = sConsulta & " ORDER BY PU.PRES1,PU.PRES2,PU.PRES3,PU.PRES4"

    rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
        
    If rs.eof Then
        rs.Close
        Set rs = Nothing
        Set mCol = Nothing
        Set mCol = New Collection
        Exit Sub
    Else
        Set mCol = Nothing
        Set mCol = New Collection
        Set fldId = rs.Fields("ID")
        Set fldCod = rs.Fields("COD")
        Set fldDen = rs.Fields("DEN")
        Set fldPres0 = rs.Fields("PRES0")
        Set fldPres1 = rs.Fields("PRES1")
        Set fldPres2 = rs.Fields("PRES2")
        Set fldPres3 = rs.Fields("PRES3")
        Set fldPres4 = rs.Fields("PRES4")
        Set fldPres5Id = rs.Fields("PRES5_IMP")
        
        Set fldUON1 = rs.Fields("UON1")
        Set fldUON2 = rs.Fields("UON2")
        Set fldUON3 = rs.Fields("UON3")
        Set fldUON4 = rs.Fields("UON4")
        Set fldU1Den = rs.Fields("U1DEN")
        Set fldU2Den = rs.Fields("U2DEN")
        Set fldU3Den = rs.Fields("U3DEN")
        Set fldU4Den = rs.Fields("U4DEN")
        Set fldFecini = rs.Fields("FECINI")
        Set fldFecfin = rs.Fields("FECFIN")
        Set fldCambio = rs.Fields("CAMBIO")
        Set fldMoneda = rs.Fields("MONEDA")
        
        Dim sDenCC As String
        While Not rs.eof
            'Se a�ade el contrato a la colecci�n.
            sDenCC = DevolverDenCC(fldUON1.Value, fldUON2.Value, fldUON3.Value, fldUON4.Value, NullToStr(fldU1Den.Value), NullToStr(fldU2Den.Value), NullToStr(fldU3Den.Value), NullToStr(fldU4Den.Value))
            Me.Add fldId.Value, fldCod.Value, fldPres0.Value, NullToStr(fldPres1.Value), NullToStr(fldPres2.Value), NullToStr(fldPres3.Value), NullToStr(fldPres4.Value), NullToStr(fldDen.Value), fldFecini.Value, fldFecfin.Value, NullToStr(fldUON1.Value), NullToStr(fldUON2.Value), NullToStr(fldUON3.Value), NullToStr(fldUON4.Value), sDenCC, NullToDbl0(fldCambio), NullToStr(fldMoneda), , NullToStr(fldPres5Id.Value)
            rs.MoveNext
        Wend

        rs.Close
        Set rs = Nothing
        Set fldId = Nothing
        Set fldCod = Nothing
        Set fldPres0 = Nothing
        Set fldPres1 = Nothing
        Set fldPres2 = Nothing
        Set fldPres3 = Nothing
        Set fldPres4 = Nothing
        Set fldPres5Id = Nothing
        Set fldUON1 = Nothing
        Set fldUON2 = Nothing
        Set fldUON3 = Nothing
        Set fldUON4 = Nothing
        Set fldU1Den = Nothing
        Set fldU2Den = Nothing
        Set fldU3Den = Nothing
        Set fldU4Den = Nothing
        Set fldCambio = Nothing
        Set fldMoneda = Nothing
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CContratosPres", "CargarTodosLosContratosCentros", ERR, Erl)
        Exit Sub
    End If
    
End Sub

''' <summary>
''' Devuelve la denominaci�n de una partida.
''' </summary>
''' <param name="sPres0">Pres0: Arbol que estamos manejando.</param>
''' <param name="sPres1">Pres1</param>
''' <param name="sPres2">Pres2</param>
''' <param name="sPres3">Pres3</param>
''' <param name="sPres4">Pres4</param>
''' <returns>Un string con la denominaci�n de la partida.</returns>
''' <remarks>Llamada desde: frmSolicitudes.ComprobarYAplicarFiltros </remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>

Public Function DevolverDenominacion(ByVal sPRES0 As String, ByVal sPRES1 As String, ByVal sPRES2 As String, ByVal sPRES3 As String, ByVal sPRES4 As String) As String
    Dim sConsulta As String
    Dim rs As adodb.Recordset
    Dim cmd As adodb.Command
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set cmd = New adodb.Command
    Dim pmt As adodb.Parameter
        
    Set pmt = cmd.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(gParametrosInstalacion.gIdioma), Value:=gParametrosInstalacion.gIdioma)
    cmd.Parameters.Append pmt
    Set pmt = cmd.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(sPRES0), Value:=sPRES0)
    cmd.Parameters.Append pmt
    If sPRES1 <> "" And sPRES2 <> "" And sPRES3 <> "" And sPRES4 <> "" Then
        sConsulta = "SELECT DEN FROM PRES5_IDIOMAS WITH(NOLOCK) WHERE IDIOMA=? AND PRES0=? AND PRES1=? AND PRES2=? AND PRES3=? AND PRES4=?"
        Set pmt = cmd.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(sPRES1), Value:=sPRES1)
        cmd.Parameters.Append pmt
        Set pmt = cmd.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(sPRES2), Value:=sPRES2)
        cmd.Parameters.Append pmt
        Set pmt = cmd.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(sPRES3), Value:=sPRES3)
        cmd.Parameters.Append pmt
        Set pmt = cmd.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(sPRES4), Value:=sPRES4)
        cmd.Parameters.Append pmt
    ElseIf sPRES1 <> "" And sPRES2 <> "" And sPRES3 <> "" And sPRES4 = "" Then
        sConsulta = "SELECT DEN FROM PRES5_IDIOMAS WITH(NOLOCK) WHERE IDIOMA=? AND PRES0=? AND PRES1=? AND PRES2=? AND PRES3=? AND PRES4 IS NULL"
        Set pmt = cmd.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(sPRES1), Value:=sPRES1)
        cmd.Parameters.Append pmt
        Set pmt = cmd.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(sPRES2), Value:=sPRES2)
        cmd.Parameters.Append pmt
        Set pmt = cmd.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(sPRES3), Value:=sPRES3)
        cmd.Parameters.Append pmt
    ElseIf sPRES1 <> "" And sPRES2 <> "" And sPRES3 = "" And sPRES4 = "" Then
        sConsulta = "SELECT DEN FROM PRES5_IDIOMAS WITH(NOLOCK) WHERE IDIOMA=? AND PRES0=? AND PRES1=? AND PRES2=? AND PRES3 IS NULL AND PRES4 IS NULL"
        Set pmt = cmd.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(sPRES1), Value:=sPRES1)
        cmd.Parameters.Append pmt
        Set pmt = cmd.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(sPRES2), Value:=sPRES2)
        cmd.Parameters.Append pmt
    ElseIf sPRES1 <> "" And sPRES2 = "" And sPRES3 = "" And sPRES4 = "" Then
        sConsulta = "SELECT DEN FROM PRES5_IDIOMAS WITH(NOLOCK) WHERE IDIOMA=? AND PRES0=? AND PRES1=? AND PRES2 IS NULL AND PRES3 IS NULL AND PRES4 IS NULL"
        Set pmt = cmd.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(sPRES1), Value:=sPRES1)
        cmd.Parameters.Append pmt
    End If
    
    If Len(sConsulta) > 0 Then
        cmd.CommandText = sConsulta
        cmd.CommandType = adCmdText
        Set cmd.ActiveConnection = m_oConexion.ADOCon
        cmd.Prepared = True
        Set rs = cmd.Execute
        If Not rs.eof Then
            DevolverDenominacion = rs.Fields("DEN").Value
        End If
        rs.Close
    End If
    Set pmt = Nothing
    Set cmd = Nothing
    Set rs = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CContratosPres", "DevolverDenominacion", ERR, Erl)
        Exit Function
    End If
End Function
