VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CCondicionesBloqueo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Enum TipoDeError

    ConexionNoEstablecida = 613

End Enum

Private m_Col As Collection
Private m_oConexion As CConexion
Private m_bEOF As Boolean

Public Property Get eof() As Boolean
    eof = m_bEOF
End Property
Friend Property Let eof(ByVal b As Boolean)
    m_bEOF = b
End Property

Public Property Get Item(vntIndexKey As Variant) As CCondicionBloqueo
On Error GoTo NoSeEncuentra:

    Set Item = m_Col(vntIndexKey)

    Exit Property

NoSeEncuentra:
    Set Item = Nothing

End Property

Friend Property Set Conexion(ByVal con As CConexion)
Set m_oConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = m_oConexion
End Property

Public Property Get Count() As Long
    If m_Col Is Nothing Then
        Count = 0
    Else
         Count = m_Col.Count
    End If
End Property
'
Public Property Get NewEnum() As IUnknown
     Set NewEnum = m_Col.[_NewEnum]
End Property

Public Function Add(ByVal lId As Long, ByVal lBloque As Long, ByVal lRol As Long, ByVal lCampo As Long _
        , ByVal sCod As String, ByVal iTipoCampo As TipoCampoCondicionEnlace, Optional ByVal lCampoDato As Long, Optional ByVal sOperador As String, Optional ByVal iTipoValor As TipoValorCondicionEnlace, Optional ByVal lCampoValor As Long, Optional ByVal vValor As Variant, Optional ByVal vIndice As Variant, Optional ByVal sMoneda As String) As CCondicionBloqueo
'Public Sub Add(ByVal lId As Long, ByVal lBloque As Long)

    'create a new object
    Dim objnewmember As CCondicionBloqueo
    Set objnewmember = New CCondicionBloqueo

    With objnewmember
        Set .Conexion = m_oConexion
        .Id = lId
        .Bloque = lBloque
        .Rol = lRol
        .Campo = lCampo
        .Cod = sCod
        .TipoCampo = iTipoCampo
        .CampoDato = lCampoDato
        .Operador = sOperador
        .TipoValor = iTipoValor
        .CampoValor = lCampoValor
        .Valor = vValor
        .Moneda = sMoneda
        
        If Not IsMissing(vIndice) And Not IsNull(vIndice) Then
            .Indice = vIndice
            m_Col.Add objnewmember, CStr(vIndice)
        Else
            m_Col.Add objnewmember, CStr(lId)
        End If
    'm_Col.Add objnewmember, CStr(lId)

    End With

'    'return the object created
'    Set Add = objnewmember
    Set objnewmember = Nothing


End Function

Public Sub AddCondicion(ByVal oCondicion As CCondicionBloqueo, Optional ByVal vIndice As Variant)
    If Not IsMissing(vIndice) And Not IsNull(vIndice) Then
        oCondicion.Indice = vIndice
        m_Col.Add oCondicion, CStr(vIndice)
    Else
        m_Col.Add oCondicion, CStr(oCondicion.Id)
    End If
End Sub

Public Sub Remove(vntIndexKey As Variant)
On Error GoTo ERROR
    m_Col.Remove vntIndexKey
ERROR:

End Sub


Private Sub Class_Initialize()
    Set m_Col = New Collection
End Sub


Private Sub Class_Terminate()
    'Proceroys collection when this class is terminated
    On Error Resume Next
    Set m_Col = Nothing
    Set m_oConexion = Nothing

End Sub
'
Public Sub CargarCondiciones(ByVal lBloque As Long, lRol As Long, lCampo As Long)
    Dim vValor As Variant
    Dim rs As adodb.Recordset
    Dim sConsulta As String

    sConsulta = "SELECT ID, BLOQUE, ROL, CAMPO, COD, TIPO_CAMPO, CAMPO_DATO, OPERADOR, TIPO_VALOR, CAMPO_VALOR, VALOR_TEXT, VALOR_NUM, VALOR_FEC, VALOR_BOOL, MON"
    sConsulta = sConsulta & " FROM PM_CONF_CUMP_BLOQUE_COND WITH (NOLOCK) "
    sConsulta = sConsulta & " WHERE BLOQUE = " & lBloque & " AND ROL =" & lRol & "  AND CAMPO = " & lCampo

    Set rs = New adodb.Recordset
    rs.CursorLocation = adUseClient
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly

    While Not rs.eof
        If Not IsNull(rs.Fields("VALOR_TEXT").Value) Then
            vValor = NullToStr(rs.Fields("VALOR_TEXT").Value)
        ElseIf Not IsNull(rs.Fields("VALOR_NUM").Value) Then
            vValor = NullToDbl0(rs.Fields("VALOR_NUM").Value)
        ElseIf Not IsNull(rs.Fields("VALOR_FEC").Value) Then
            vValor = rs.Fields("VALOR_FEC").Value
        ElseIf Not IsNull(rs.Fields("VALOR_BOOL").Value) Then
            vValor = rs.Fields("VALOR_BOOL").Value
        End If
        Add NullToDbl0(rs("ID").Value), NullToDbl0(rs("BLOQUE").Value), NullToDbl0(rs("ROL").Value), NullToDbl0(rs("CAMPO").Value), NullToStr(rs("COD").Value), NullToDbl0(rs("TIPO_CAMPO").Value), NullToDbl0(rs("CAMPO_DATO").Value), NullToStr(rs("OPERADOR").Value), NullToDbl0(rs("TIPO_VALOR").Value), NullToDbl0(rs("CAMPO_VALOR").Value), vValor, , NullToStr(rs("MON").Value)
        'Add NullToDbl0(rs("ID").Value), NullToDbl0(rs("BLOQUE").Value)

        rs.MoveNext
    Wend
    rs.Close
    Set rs = Nothing
End Sub

Public Property Set CondicionesBloqueo(ByVal vData As Collection)
    Set m_Col = vData
End Property

Public Property Get CondicionesBloqueo() As Collection
    Set CondicionesBloqueo = m_Col
End Property

Public Function DevolverFormula(ByVal lBloque As Long, ByVal lRol As Long, ByVal lCampo As Long) As String
    Dim rs As adodb.Recordset
    Dim sConsulta As String
    Dim sFormula As String
    
    sConsulta = "SELECT ESCRITURA_FORMULA FROM PM_CONF_CUMP_BLOQUE WITH (NOLOCK)"
    sConsulta = sConsulta & " WHERE BLOQUE = " & lBloque & " And Rol = " & lRol & " AND CAMPO = " & lCampo

    Set rs = New adodb.Recordset
    rs.CursorLocation = adUseClient
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    
    sFormula = ""
    If Not rs.eof Then
        sFormula = NullToStr(rs("ESCRITURA_FORMULA"))
    End If
    
    DevolverFormula = sFormula
    

End Function

Public Function GuardarFormula(ByVal lBloque As Long, ByVal lRol As Long, ByVal lCampo As Long, ByVal sFormula As String) As TipoErrorSummit


Dim sConsulta As String
Dim TESError As TipoErrorSummit

'******************* Precondicion *******************
If m_oConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CCondicionesBloqueo.GuardarFormula", "No se ha establecido la conexion"
End If
'*****************************************************

On Error GoTo ERROR:

    TESError.NumError = TESnoerror

    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
    
    sConsulta = "UPDATE PM_CONF_CUMP_BLOQUE SET ESCRITURA_FORMULA = " & StrToSQLNULL(sFormula)
    sConsulta = sConsulta & " WHERE BLOQUE = " & lBloque & " And Rol = " & lRol & " AND CAMPO = " & lCampo
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR

    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    GuardarFormula = TESError
    
    Exit Function

ERROR:
    GuardarFormula = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    
    m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
End Function

Public Function ObtenerID(ByVal lBloque As Long, ByVal lRol As Long, ByVal lCampo As Long) As Long
Dim sConsulta As String
Dim AdoRes As adodb.Recordset

    'sConsulta = "SELECT MAX(CAST(RIGHT(COD,LEN(COD)-1) AS INTEGER)) AS COD "
    
    sConsulta = "SELECT max(case ISNUMERIC(RIGHT(COD,LEN(COD)-1)) when 1 then CAST(RIGHT(COD,LEN(COD)-1) AS INTEGER) else 0 end) AS COD"
    sConsulta = sConsulta & " FROM PM_CONF_CUMP_BLOQUE_COND WITH (NOLOCK) "
    'sConsulta = sConsulta & " INNER JOIN FORM_GRUPO ON FORM_CAMPO.GRUPO=FORM_GRUPO.ID "
    sConsulta = sConsulta & " WHERE BLOQUE = " & lBloque & " AND ROL = " & lRol & "AND CAMPO = " & lCampo
    sConsulta = sConsulta & " and cod like 'x%'"
    
    Set AdoRes = New adodb.Recordset
    AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
    
    If IsNull(AdoRes.Fields("COD").Value) Then
        ObtenerID = 1
    Else
        If IsNumeric(AdoRes.Fields("COD").Value) Then
            ObtenerID = AdoRes.Fields("COD").Value + 1
        Else
            ObtenerID = 1
        End If
        
    End If
    
    AdoRes.Close
    Set AdoRes = Nothing
    
End Function



