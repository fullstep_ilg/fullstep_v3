VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CActividadNivel1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CActividadNivel1 **********************************
'*             Autor : Javier Arana
'*             Creada : 18/11/98
'****************************************************************


Option Explicit
Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

Private mvarIndice As Long 'Nos indica la posicion secuencial dentro de una Coleccion
Private mvarCod As String
Private mvarDen As String
Private mvarId As Long
Private mvarNumProve As Integer
Private mvarActividadesNivel2 As CActividadesNivel2
Private mvarConexion As CConexion 'local copy

Public Property Get ID() As Long
    ID = mvarId
End Property
Public Property Let ID(ByVal Ind As Long)
    mvarId = Ind
End Property
Public Property Get Indice() As Long
    Indice = mvarIndice
End Property
Public Property Let Indice(ByVal Ind As Long)
    mvarIndice = Ind
End Property


Friend Property Set Conexion(ByVal vData As CConexion)
    Set mvarConexion = vData
End Property


Friend Property Get Conexion() As CConexion
    Set Conexion = mvarConexion
End Property

Public Property Set ActividadesNivel2(ByVal vData As CActividadesNivel2)
    Set mvarActividadesNivel2 = vData
End Property


Public Property Get ActividadesNivel2() As CActividadesNivel2
    Set ActividadesNivel2 = mvarActividadesNivel2
End Property

Public Property Let Den(ByVal vData As String)
    mvarDen = vData
End Property


Public Property Get Den() As String
    Den = mvarDen
End Property



Public Property Let Cod(ByVal vData As String)
    mvarCod = vData
End Property


Public Property Get Cod() As String
    Cod = mvarCod
End Property

Public Property Let numProve(ByVal vData As Integer)
    mvarNumProve = vData
End Property


Public Property Get numProve() As Integer
    numProve = mvarNumProve
End Property

Private Sub Class_Terminate()
    
    Set mvarConexion = Nothing
       
End Sub


