VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CSolCumplimentacionesGs"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

''' Variables que almacenan las propiedades de los objetos
Private m_Col As Collection
Private m_oConexion As CConexion

''' Control de errores
Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum

Public Function Add(ByVal lId As Long, ByVal lSolicitud As Long, ByVal lCampo As Long, ByVal bEscritura As Boolean, ByVal bObligatorio As Boolean, _
        Optional ByVal oCampo As CFormItem, Optional ByVal CodAtrib As Variant, Optional ByVal FecAct As Variant) As CSolCumplimentacionGs
    Dim objnewmember As CSolCumplimentacionGs
        
    If Not g_oErrores.fg_bProgramando Then On Error GoTo Error
   
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New CSolCumplimentacionGs
    
    objnewmember.Id = lId
    objnewmember.Solicitud = lSolicitud
    objnewmember.IdCampo = lCampo
    
    Set objnewmember.Campo = oCampo
    
    objnewmember.Escritura = bEscritura
    objnewmember.Obligatorio = bObligatorio
                    
    If Not IsMissing(CodAtrib) Then
        objnewmember.CodAtrib = CodAtrib
    Else
        objnewmember.CodAtrib = Null
    End If
                    
    If Not IsMissing(FecAct) Then
        objnewmember.FecAct = FecAct
    Else
        objnewmember.FecAct = Null
    End If
    
    Set objnewmember.Conexion = m_oConexion
    
    m_Col.Add objnewmember, CStr(lCampo)
    
    Set Add = objnewmember
    
    Set objnewmember = Nothing

    '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
Error:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("M�dulo de clase", "CSolCumplimentacionesGs", "Add", ERR, Erl)
        Exit Function
    End If
End Function

Public Property Get Item(vntIndexKey As Variant) As CSolCumplimentacionGs
    On Error GoTo NoSeEncuentra:

    ''' Devolvemos el item de la coleccion privada
    
    Set Item = m_Col(vntIndexKey)
     
    Exit Property

NoSeEncuentra:

    Set Item = Nothing
    
End Property

Friend Property Set Conexion(ByVal con As CConexion)

    ''' * Objetivo: Dar valor a la variable privada Conexion
    ''' * Recibe: Conexion
    ''' * Devuelve: Nada
    
    Set m_oConexion = con
    
End Property

Friend Property Get Conexion() As CConexion

    ''' * Objetivo: Devolver variable privada Conexion
    ''' * Recibe: Nada
    ''' * Devuelve: Conexion
    
    Set Conexion = m_oConexion
    
End Property

Public Property Get Count() As Long

    ''' * Objetivo: Devolver variable privada Count
    ''' * Recibe: Nada
    ''' * Devuelve: Numero de elementos de la coleccion

    If m_Col Is Nothing Then
        Count = 0
    Else
        Count = m_Col.Count
    End If

End Property

Public Sub Remove(vntIndexKey As Variant)


    ''' * Objetivo: Eliminar una solicitud de la coleccion
    ''' * Recibe: Indice de la solicitud a eliminar
    ''' * Devuelve: Nada
   
    If Not g_oErrores.fg_bProgramando Then On Error GoTo Error
   
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_Col.Remove vntIndexKey
    '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
    
Error:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("M�dulo de clase", "CSolCumplimentacionesGs", "Remove", ERR, Erl)
        Exit Sub
    End If
End Sub

Private Sub Class_Initialize()

    ''' * Objetivo: Crear la coleccion

    Set m_Col = New Collection
    
End Sub

Private Sub Class_Terminate()

    ''' * Objetivo: Limpiar memoria
    Set m_Col = Nothing
    Set m_oConexion = Nothing
End Sub

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"

    ''' ! Pendiente de revision, objetivo desconocido
    
    Set NewEnum = m_Col.[_NewEnum]

End Property

Public Sub CargarCumplimentacion(ByVal lSolicitud As Long, ByVal bSeIncluyenBajas As Boolean, Optional ByVal lGrupo As Long = 0, Optional ByVal lPadre As Long = 0)
    Dim rs As New ADODB.Recordset
    Dim sConsulta As String
    Dim oCampo As CFormItem
        
    If Not g_oErrores.fg_bProgramando Then On Error GoTo Error
    
    sConsulta = "SELECT CG.ID,CG.SOLICITUD,CG.CAMPO,CG.ESCRITURA,CG.OBLIGATORIO,CG.FECACT, DF.COD ATRIB_GS, FC.TIPO, FC.SUBTIPO, FC.TIPO_CAMPO_GS, FC.DEN_" & gParametrosInstalacion.gIdioma & " DEN_CAMPO"
    sConsulta = sConsulta & " FROM SOLICITUD_CUMPLIMENTACION_GS CG WITH(NOLOCK)"
    sConsulta = sConsulta & " INNER JOIN SOLICITUD S WITH(NOLOCK) ON S.ID=CG.SOLICITUD"
    If lGrupo > 0 Then
        sConsulta = sConsulta & " INNER JOIN FORM_GRUPO FG WITH(NOLOCK) ON FG.FORMULARIO=S.FORMULARIO AND FG.BAJALOG=0 AND FG.ID=" & lGrupo
    End If
    sConsulta = sConsulta & " INNER JOIN FORM_CAMPO FC WITH(NOLOCK) ON FC.ID=CG.CAMPO"
    If Not bSeIncluyenBajas Then
        sConsulta = sConsulta & " AND FC.BAJALOG=0"
    End If
    If lGrupo > 0 Then
        sConsulta = sConsulta & " AND FG.ID=FC.GRUPO AND FC.ES_SUBCAMPO=0"
    ElseIf lPadre > 0 Then
        sConsulta = sConsulta & " INNER JOIN DESGLOSE D WITH(NOLOCK) ON D.CAMPO_HIJO=FC.ID AND D.CAMPO_PADRE=" & lPadre
    End If
    sConsulta = sConsulta & " LEFT JOIN DEF_ATRIB DF WITH(NOLOCK) ON DF.ID=FC.ID_ATRIB_GS"
    sConsulta = sConsulta & " WHERE CG.SOLICITUD=" & lSolicitud
    sConsulta = sConsulta & " ORDER BY FC.ORDEN"
    
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    
    If Not rs.eof Then
        ''' Carga del Resultset en la coleccion
        Set m_Col = Nothing
        Set m_Col = New Collection
                        
        While Not rs.eof
            Set oCampo = New CFormItem
            Set oCampo.Conexion = m_oConexion
            
            oCampo.Id = rs.Fields("CAMPO").Value
            oCampo.TipoPredef = rs.Fields("TIPO").Value
            oCampo.Tipo = rs.Fields("SUBTIPO").Value
            oCampo.CampoGS = rs.Fields("TIPO_CAMPO_GS").Value
            Set oCampo.Denominaciones = New CMultiidiomas
            oCampo.Denominaciones.Add gParametrosInstalacion.gIdioma, rs.Fields("DEN_CAMPO").Value
        
            Add rs.Fields("ID"), rs.Fields("SOLICITUD"), rs.Fields("CAMPO"), SQLBinaryToBoolean(rs.Fields("ESCRITURA")), SQLBinaryToBoolean(rs.Fields("OBLIGATORIO")), oCampo, rs.Fields("ATRIB_GS"), rs.Fields("FECACT")
                        
            rs.MoveNext
        Wend
        
        rs.Close
        Set rs = Nothing
    End If
    
    '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
Error:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("M�dulo de clase", "CSolCumplimentacionesGs", "CargarCumplimentacion", ERR, Erl)
        Exit Sub
    End If
End Sub
