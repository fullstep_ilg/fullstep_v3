VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CVarCalificaciones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CCalificaciones **********************************
'*             Autor : Javier Arana
'*             Creada : 13/8/98
'****************************************************************


Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum
Private mCol As Collection
Private m_oConexion As CConexion
Private m_bEOF As Boolean

Public Function Add(ByVal lVarID As Long, ByVal iNivel As Integer, ByVal Num As Long, ByVal oDen As CMultiidiomas, Optional ByVal ValInf As Double, Optional ByVal ValSup As Double, Optional ByVal varIndice As Variant) As CVarCalificacion
    'create a new object
    Dim objnewmember As CVarCalificacion
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New CVarCalificacion
   
    objnewmember.VarID = lVarID
    objnewmember.Nivel = iNivel
    objnewmember.Id = Num
    Set objnewmember.Denominaciones = oDen
    objnewmember.ValorInf = ValInf
    objnewmember.ValorSup = ValSup
    
    Set objnewmember.Conexion = m_oConexion
    
    If Not IsMissing(varIndice) And Not IsNull(varIndice) Then
        objnewmember.Indice = varIndice
        mCol.Add objnewmember, CStr(varIndice)
    Else
        mCol.Add objnewmember, CStr(Num)
    End If
    
    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CVarCalificaciones", "Add", ERR, Erl)
      Exit Function
   End If

End Function

Public Property Get Item(vntIndexKey As Variant) As CVarCalificacion
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Friend Property Set Conexion(ByVal con As CConexion)
Set m_oConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = m_oConexion
End Property


Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
     Count = mCol.Count
End If


End Property


Public Sub Remove(vntIndexKey As Variant)
On Error GoTo NoSeEncuentra:
    
    mCol.Remove vntIndexKey

NoSeEncuentra:

End Sub


Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
     Set NewEnum = mCol.[_NewEnum]
End Property


Private Sub Class_Initialize()
  
    Set mCol = New Collection
End Sub


Private Sub Class_Terminate()
    'destroys collection when this class is terminated
    Set mCol = Nothing
    Set m_oConexion = Nothing
    
End Sub

Public Property Get eof() As Boolean
    eof = m_bEOF
End Property
''' Revisado por: Jbg. Fecha: 13/12/2011
''' <summary>
''' Guarda la calificaci�n de la Variable de Calidad seleccionada, a�ade las denominacines (multiIdioma) de las calificaciones
''' </summary>
''' <param name="lVarID">Id de la VC</param>
''' <param name="iNivel">nivel de la VC</param>
''' <param name="lIdPyme">Id de la pyme si la aplicacion esta trabajando en modo pyme, lIDPyme = 0 si no.</param>
''' <returns>Error de haberlo</returns>
''' <remarks>Llamada desde: frmVarCalificaciones.frm --> cmdAceptar_Click  ; Tiempo m�ximo: 0,8seg.</remarks>
Public Function GuardarCalificaciones(ByVal lVarID As Long, ByVal iNivel As Integer, ByVal lIdPyme As Long) As TipoErrorSummit

Dim TESError As TipoErrorSummit
Dim btrans As Boolean
Dim sConsulta As String
Dim rs As adodb.Recordset
Dim oCal As CVarCalificacion

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    TESError.NumError = TESnoerror
If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
    
    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
    btrans = True
    
    'Comprobamos si ha habido cambios en  otra sesi�n
    Set rs = New adodb.Recordset
    
    If iNivel <> 0 Then
        'Calidad: NO hay poner WITH (NOLOCK) ya que no hay que hacer la lectura en sucio, y hay que hacer la lectura con los datos actuales.
        sConsulta = "SELECT FECACT FROM VAR_CAL" & iNivel & " WHERE ID =" & lVarID
        rs.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
        If rs.eof Then
            rs.Close
            Set rs = Nothing
            TESError.NumError = TESDatoEliminado
            Select Case iNivel
            Case 1
                TESError.Arg1 = 171 ' VAR_CAL1
            Case 2
                TESError.Arg1 = 172 ' VAR_CAL2
            Case 3
                TESError.Arg1 = 173 ' VAR_CAL3
            Case 4
                TESError.Arg1 = 175 ' VAR_CAL4
            Case 5
                TESError.Arg1 = 176 ' VAR_CAL5
            End Select
            GuardarCalificaciones = TESError
            m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
            Exit Function
        End If
        
        rs.Close
        Set rs = Nothing
    End If
    

    sConsulta = "DELETE CAL_PUNTUACION_IDIOMA"
    sConsulta = sConsulta & " WHERE VARCAL=" & lVarID & " AND NIVEL=" & iNivel
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    
    sConsulta = "DELETE FROM CAL_PUNTUACION WHERE VARCAL=" & lVarID & " AND NIVEL=" & iNivel
    sConsulta = sConsulta & " AND ISNULL(PYME,0) = " & lIdPyme
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    Dim i As Integer
    For Each oCal In mCol
        sConsulta = "INSERT INTO CAL_PUNTUACION (VARCAL,NIVEL,ID,INF,SUP, PYME)"
        sConsulta = sConsulta & " VALUES (" & lVarID & "," & iNivel & "," & oCal.Id & "," & DblToSQLFloat(oCal.ValorInf) & "," & DblToSQLFloat(oCal.ValorSup)
        sConsulta = sConsulta & "," & Dbl0ToSQLNull(lIdPyme) & ")"
        m_oConexion.ADOCon.Execute sConsulta
        If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
        
        For i = 1 To oCal.Denominaciones.Count
            sConsulta = "INSERT INTO CAL_PUNTUACION_IDIOMA (VARCAL,NIVEL,CAL_PUNTUACION,IDIOMA,DEN)"
            sConsulta = sConsulta & " VALUES (" & lVarID & "," & iNivel & "," & oCal.Id & ",'" & DblQuote(oCal.Denominaciones.Item(i).Cod) & "','" & DblQuote(oCal.Denominaciones.Item(i).Den) & "'"
            sConsulta = sConsulta & ")"
            m_oConexion.ADOCon.Execute sConsulta
            If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
        Next
    Next
     
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    btrans = False

    GuardarCalificaciones = TESError
    
    Exit Function
    
Error_Cls:
    GuardarCalificaciones = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    
    If btrans Then
        m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      'Call g_oErrores.TratarError("Class Module", "CVarCalificaciones", "GuardarCalificaciones", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If

End Function

Public Sub CargarCalificacionTotalProve(Optional ByVal lIdPyme As Long = 0)
'Descripcion:   Carga y a�ade a la coleccion las calificaciones de la variable de calidad de nivel 0 con todos los idiomas almacenadas --> Total del proveedores
'               Carga en el recordset todas las calificaciones que haya y luego solo introduce una calificacion y los idiomas los a�ade en otra coleccion
'Parametros:    lIdPyme = Id de la pyme a cargar, lIDPyme = 0 si no esta en modo PYME
'Llamadas: frmVarCALIFICACION.frm
'Tiempo ejecucion:= 0seg.

Dim rs As adodb.Recordset
Dim sConsulta As String
Dim lIDAnt As Long


If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set rs = New adodb.Recordset
    
    sConsulta = "SELECT CP.ID,CPI.IDIOMA, CPI.DEN,CP.INF,CP.SUP"
    sConsulta = sConsulta & " FROM CAL_PUNTUACION CP WITH (NOLOCK)"
    sConsulta = sConsulta & " LEFT JOIN CAL_PUNTUACION_IDIOMA CPI WITH (NOLOCK) ON CPI.VARCAL =CP.VARCAL AND CPI.NIVEL = CP.NIVEL AND CPI.CAL_PUNTUACION = CP.ID"
    sConsulta = sConsulta & " WHERE CP.NIVEL=0 "

    If lIdPyme > 0 Then
        sConsulta = sConsulta & " AND PYME = " & lIdPyme
    Else
        sConsulta = sConsulta & " AND PYME IS NULL"
    End If

    
    sConsulta = sConsulta & " ORDER BY ID"
    
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    
    Set mCol = Nothing
    Set mCol = New Collection
    lIDAnt = 0
    
    Dim lId As Long
    Dim sCodIdioma, sDen As String
    Dim dInf, dSup As Double
    Dim oDenominaciones As CMultiidiomas
    
    If Not rs.eof Then
    
        lId = rs.Fields("ID").Value
        lIDAnt = lId
        sCodIdioma = rs.Fields("IDIOMA").Value
        sDen = rs.Fields("DEN").Value
        dInf = rs.Fields("INF").Value
        dSup = rs.Fields("SUP").Value
        
        Set oDenominaciones = New CMultiidiomas
    
        While Not rs.eof
            lId = rs.Fields("ID").Value
            sCodIdioma = rs.Fields("IDIOMA").Value
            sDen = rs.Fields("DEN").Value
            
            If lId <> lIDAnt Then
                Me.Add 0, 0, lIDAnt, oDenominaciones, dInf, dSup
                Set oDenominaciones = New CMultiidiomas
                dInf = rs.Fields("INF").Value
                dSup = rs.Fields("SUP").Value
                oDenominaciones.Add sCodIdioma, sDen
                lIDAnt = lId
            Else
                oDenominaciones.Add sCodIdioma, sDen
            End If
            
            rs.MoveNext
        Wend
        Me.Add 0, 0, lIDAnt, oDenominaciones, dInf, dSup
        Set oDenominaciones = Nothing
    
        
    End If
    
    rs.Close
    Set rs = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CVarCalificaciones", "CargarCalificacionTotalProve", ERR, Erl)
      Exit Sub
   End If

End Sub

