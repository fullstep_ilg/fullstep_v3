VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CContratoPres"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

Private m_oConexion As CConexion

Private m_lID As Long
Private m_sCod As String
Private m_sDen As String
Private m_sPRES0 As String
Private m_sPRES1 As String
Private m_sPRES2 As String
Private m_sPRES3 As String
Private m_sPRES4 As String
Private m_sPRES5Id As String
Private m_sUON1 As String
Private m_sUON2 As String
Private m_sUON3 As String
Private m_sUON4 As String
Private m_sDenUON As String
Private m_vFecIni As Variant
Private m_vFecFin As Variant
Private m_dCambio As Double
Private m_sMoneda As String
Private m_vGestor As Variant

Public Property Let Cambio(ByVal dato As Double)
    m_dCambio = dato
End Property
Public Property Get Cambio() As Double
    Cambio = m_dCambio
End Property

Public Property Let Id(ByVal dato As Long)
    m_lID = dato
End Property
Public Property Get Id() As Long
    Id = m_lID
End Property

Public Property Let Moneda(ByVal dato As String)
    m_sMoneda = dato
End Property
Public Property Get Moneda() As String
    Moneda = m_sMoneda
End Property

Public Property Let Gestor(ByVal dato As Variant)
    m_vGestor = dato
End Property
Public Property Get Gestor() As Variant
    Gestor = m_vGestor
End Property

Public Property Let Cod(ByVal dato As String)
    m_sCod = dato
End Property
Public Property Get Cod() As String
    Cod = m_sCod
End Property

Public Property Let Den(ByVal dato As String)
    m_sDen = dato
End Property
Public Property Get Den() As String
    Den = m_sDen
End Property

Public Property Let PRES0(ByVal dato As String)
    m_sPRES0 = dato
End Property
Public Property Get PRES0() As String
    PRES0 = m_sPRES0
End Property

Public Property Let PRES1(ByVal dato As String)
    m_sPRES1 = dato
End Property
Public Property Get PRES1() As String
    PRES1 = m_sPRES1
End Property

Public Property Let PRES2(ByVal dato As String)
    m_sPRES2 = dato
End Property
Public Property Get PRES2() As String
    PRES2 = m_sPRES2
End Property

Public Property Let Pres3(ByVal dato As String)
    m_sPRES3 = dato
End Property
Public Property Get Pres3() As String
    Pres3 = m_sPRES3
End Property

Public Property Let Pres4(ByVal dato As String)
    m_sPRES4 = dato
End Property
Public Property Get Pres4() As String
    Pres4 = m_sPRES4
End Property

Public Property Let UON1(ByVal dato As String)
    m_sUON1 = dato
End Property
Public Property Get UON1() As String
    UON1 = m_sUON1
End Property

Public Property Let UON2(ByVal dato As String)
    m_sUON2 = dato
End Property
Public Property Get UON2() As String
    UON2 = m_sUON2
End Property

Public Property Let UON3(ByVal dato As String)
    m_sUON3 = dato
End Property
Public Property Get UON3() As String
    UON3 = m_sUON3
End Property

Public Property Let UON4(ByVal dato As String)
    m_sUON4 = dato
End Property
Public Property Get UON4() As String
    UON4 = m_sUON4
End Property

Public Property Let DenUON(ByVal dato As String)
    m_sDenUON = dato
End Property
Public Property Get DenUON() As String
    DenUON = m_sDenUON
End Property

Public Property Get FecIni() As Variant
    FecIni = m_vFecIni
End Property
Public Property Let FecIni(ByVal dato As Variant)
    m_vFecIni = dato
End Property

Public Property Get FecFin() As Variant
    FecFin = m_vFecFin
End Property
Public Property Let FecFin(ByVal dato As Variant)
    m_vFecFin = dato
End Property


Friend Property Set Conexion(ByVal vData As CConexion)

    ''' * Objetivo: Dar valor a la conexion de la Destino
    ''' * Recibe: Conexion
    ''' * Devuelve: Nada

    Set m_oConexion = vData
    
End Property
Friend Property Get Conexion() As CConexion

    ''' * Objetivo: Devolver la conexion de la Destino
    ''' * Recibe: Nada
    ''' * Devuelve: Conexion

    Set Conexion = m_oConexion
    
End Property

Private Sub Class_Terminate()
    
    On Error GoTo ERROR:
    
    Set m_oConexion = Nothing
    Exit Sub
ERROR:
    Resume Next
    
End Sub

''' <summary>
''' Devolver el Id de PRES5_IMPORTES
''' </summary>
''' <param name="ParametroEntrada1">Id de PRES5_IMPORTES</param>
''' <remarks>Llamada desde: CContratosPres.add  ; Tiempo m�ximo: 0,2</remarks>
Public Property Let PRES5Id(ByVal dato As String)
    m_sPRES5Id = dato
End Property
''' <summary>
''' Establecer el Id de PRES5_IMPORTES
''' </summary>
''' <returns>PRES5_IMPORTES.ID</returns>
''' <remarks>Llamada desde: frmPedidos/gridPedido.BeforeRowColChange gridPedido.BtnClick frmSeguimiento/gridItem.BeforeRowColChange gridItem.BtnClick ; Tiempo m�ximo: 0,2</remarks>
Public Property Get PRES5Id() As String
    PRES5Id = m_sPRES5Id
End Property

''' <summary>
''' Cargar el Id de PRES5_IMPORTES
''' </summary>
''' <returns>PRES5_IMPORTES.ID</returns>
''' <remarks>Llamada desde: frmSelCContables/cmdAceptar_Click ; Tiempo m�ximo: 0,2</remarks>
''' Revisado por: jbg; Fecha: 07/03/2013
Public Function CargarPres5Id() As String
    Dim rs As New adodb.Recordset
    Dim sConsulta As String

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sConsulta = "SELECT ID FROM PRES5_IMPORTES WITH(NOLOCK) WHERE CERRADO=0 AND PRES0=" & StrToSQLNULL(m_sPRES0)
    If m_sPRES1 <> "" Then
        sConsulta = sConsulta & " AND PRES1=" & StrToSQLNULL(m_sPRES1)
    Else
        sConsulta = sConsulta & " AND PRES1 IS NULL"
    End If
    If m_sPRES2 <> "" Then
        sConsulta = sConsulta & " AND PRES2=" & StrToSQLNULL(m_sPRES2)
    Else
        sConsulta = sConsulta & " AND PRES2 IS NULL"
    End If
    If m_sPRES3 <> "" Then
        sConsulta = sConsulta & " AND PRES3=" & StrToSQLNULL(m_sPRES3)
    Else
        sConsulta = sConsulta & " AND PRES3 IS NULL"
    End If
    If m_sPRES4 <> "" Then
        sConsulta = sConsulta & " AND PRES4=" & StrToSQLNULL(m_sPRES4)
    Else
        sConsulta = sConsulta & " AND PRES4 IS NULL"
    End If

    rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    If rs.eof Then
        CargarPres5Id = ""
    Else
        CargarPres5Id = NullToStr(rs("ID").Value)
    End If
    
    rs.Close
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CContratoPres", "CargarPres5Id", ERR, Erl)
        Exit Function
    End If
End Function


