VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CCampoVinculado"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

Private m_oConexion As CConexion

Private m_vIndice As Variant

Private m_vDesglVinculadoId As Variant
Private m_lCampoVinculado As Long
Private m_lCampoOrigen As Variant
Private m_vFecAct As Variant

Private m_lSolicitud As Long
Private m_lDesgloseVinculado As Long
Private m_lDesgloseOrigen As Long
''' <summary>
''' Obtener el DESGLOSE_VINCULADO.ID
''' </summary>
''' <remarks>Llamada desde: frmFormularioVincularCampos; Tiempo m�ximo: 0</remarks>
Public Property Get DesglVinculadoId() As Variant
    DesglVinculadoId = m_vDesglVinculadoId
End Property
''' <summary>
''' Establecer el DESGLOSE_VINCULADO.ID
''' </summary>
''' <remarks>Llamada desde: frmFormularioVincularCampos; Tiempo m�ximo: 0</remarks>
Public Property Let DesglVinculadoId(ByVal Data As Variant)
    m_vDesglVinculadoId = Data
End Property
''' <summary>
''' Obtener el Id de form_campo del campo del Desglose Vinculado
''' </summary>
''' <remarks>Llamada desde: frmFormularioVincularCampos; Tiempo m�ximo: 0</remarks>
Public Property Get CampoVinculado() As Long
    CampoVinculado = m_lCampoVinculado
End Property
''' <summary>
''' Establecer el Id de form_campo del campo del Desglose Vinculado
''' </summary>
''' <remarks>Llamada desde: frmFormularioVincularCampos; Tiempo m�ximo: 0</remarks>
Public Property Let CampoVinculado(ByVal Data As Long)
    m_lCampoVinculado = Data
End Property
''' <summary>
''' Obtener el Id de form_campo del campo del Desglose Vinculado origen, de haberlo
''' </summary>
''' <remarks>Llamada desde: frmFormularioVincularCampos; Tiempo m�ximo: 0</remarks>
Public Property Get campoOrigen() As Variant
    campoOrigen = m_lCampoOrigen
End Property
''' <summary>
''' Establecer el Id de form_campo del campo del Desglose Vinculado origen, de haberlo
''' </summary>
''' <remarks>Llamada desde: frmFormularioVincularCampos; Tiempo m�ximo: 0</remarks>
Public Property Let campoOrigen(ByVal Data As Variant)
    m_lCampoOrigen = Data
End Property
''' <summary>
''' Obtener la Fecha ultima grabaci�n
''' </summary>
''' <remarks>Llamada desde: frmFormularioVincularCampos; Tiempo m�ximo: 0</remarks>
Public Property Get FecAct() As Variant
    FecAct = m_vFecAct
End Property
''' <summary>
''' Establecer la Fecha ultima grabaci�n
''' </summary>
''' <remarks>Llamada desde: frmFormularioVincularCampos; Tiempo m�ximo: 0</remarks>
Public Property Let FecAct(ByVal Data As Variant)
    m_vFecAct = Data
End Property
''' <summary>
''' Establecer el indice
''' </summary>
''' <remarks>Llamada desde: frmFormularioVincularCampos; Tiempo m�ximo: 0</remarks>
Public Property Let Indice(ByVal varIndice As Variant)
    m_vIndice = varIndice
End Property
''' <summary>
''' Obtener el indice
''' </summary>
''' <remarks>Llamada desde: frmFormularioVincularCampos; Tiempo m�ximo: 0</remarks>
Public Property Get Indice() As Variant
    Indice = m_vIndice
End Property

Friend Property Set Conexion(ByVal vData As CConexion)

    ''' * Objetivo: Dar valor a la conexion de la Destino
    ''' * Recibe: Conexion
    ''' * Devuelve: Nada

    Set m_oConexion = vData
    
End Property
Friend Property Get Conexion() As CConexion

    ''' * Objetivo: Devolver la conexion de la Destino
    ''' * Recibe: Nada
    ''' * Devuelve: Conexion

    Set Conexion = m_oConexion
    
End Property
''' <summary>
''' Obtener la Solicitud
''' </summary>
''' <remarks>Llamada desde: frmFormularioVincularCampos; Tiempo m�ximo: 0</remarks>
Public Property Get Solicitud() As Long
    Solicitud = m_lSolicitud
End Property
''' <summary>
''' Establecer la Solicitud
''' </summary>
''' <remarks>Llamada desde: frmFormularioVincularCampos; Tiempo m�ximo: 0</remarks>
Public Property Let Solicitud(ByVal Data As Long)
    m_lSolicitud = Data
End Property
''' <summary>
''' Obtener el Id de form_campo de Desglose origen, de haberlo
''' </summary>
''' <remarks>Llamada desde: frmFormularioVincularCampos; Tiempo m�ximo: 0</remarks>
Public Property Get DesgloseOrigen() As Long
    DesgloseOrigen = m_lDesgloseOrigen
End Property
''' <summary>
''' Establecer el Id de form_campo de Desglose origen, de haberlo
''' </summary>
''' <remarks>Llamada desde: frmFormularioVincularCampos; Tiempo m�ximo: 0</remarks>
Public Property Let DesgloseOrigen(ByVal Data As Long)
    m_lDesgloseOrigen = Data
End Property
''' <summary>
''' Obtener el Id de form_campo de Desglose Vinculado
''' </summary>
''' <remarks>Llamada desde: frmFormularioVincularCampos; Tiempo m�ximo: 0</remarks>
Public Property Get DesgloseVinculado() As Long
    DesgloseVinculado = m_lDesgloseVinculado
End Property
''' <summary>
''' Establecer el Id de form_campo de Desglose Vinculado
''' </summary>
''' <remarks>Llamada desde: frmFormularioVincularCampos; Tiempo m�ximo: 0</remarks>
Public Property Let DesgloseVinculado(ByVal Data As Long)
    m_lDesgloseVinculado = Data
End Property
