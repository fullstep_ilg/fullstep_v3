VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CHistoricos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CHistoricos **********************************
'*             Autor : Javier Arana
'*             Creada : 29/5/99
'****************************************************************


Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum
Private mCol As Collection
Public Function Add(Optional ByVal Item As Integer, Optional ByVal PrecioInicial As Variant, Optional ByVal ObjetivoInicial As Variant, Optional ByVal PostObjetivoInicial As Variant, Optional ByVal ObjetivoFinal As Variant, Optional ByVal PostObjetivoFinal As Variant, Optional ByVal varIndice As Variant, Optional ByVal AhorroPorcentual As Variant, Optional ByVal AhorroImporte As Variant) As CHistorico

    'create a new object
    Dim objNewMember As CHistorico
    
    Set objNewMember = New CHistorico
   
    If IsMissing(AhorroPorcentual) Then
        objNewMember.AhorroPorcentual = Null
    Else
        objNewMember.AhorroPorcentual = AhorroPorcentual
    End If
    
    If IsMissing(AhorroImporte) Then
        objNewMember.AhorroImporte = Null
    Else
        objNewMember.AhorroImporte = AhorroImporte
    End If
    
    objNewMember.ItemId = Item
    If IsMissing(PrecioInicial) Then
        objNewMember.PrecioInicial = Null
    Else
         objNewMember.PrecioInicial = PrecioInicial
    End If
    
    If IsMissing(ObjetivoInicial) Then
        objNewMember.ObjetivoInicial = Null
    Else
         objNewMember.ObjetivoInicial = ObjetivoInicial
    End If
    
    If IsMissing(PostObjetivoInicial) Then
        objNewMember.PostObjetivoInicial = Null
    Else
         objNewMember.PostObjetivoInicial = PostObjetivoInicial
    End If
    
    If IsMissing(ObjetivoFinal) Then
        objNewMember.ObjetivoUltimo = Null
    Else
         objNewMember.ObjetivoUltimo = ObjetivoFinal
    End If
    
    If IsMissing(PostObjetivoFinal) Then
        objNewMember.PostObjetivoUltimo = Null
    Else
         objNewMember.PostObjetivoUltimo = PostObjetivoFinal
    End If
    
    If Not IsMissing(varIndice) And Not IsNull(varIndice) Then
        objNewMember.Indice = varIndice
        mCol.Add objNewMember, CStr(varIndice)
    Else
    'set the properties passed into the method
       mCol.Add objNewMember, CStr(Item)
    End If
    
    'return the object created
    Set Add = objNewMember
    Set objNewMember = Nothing


End Function

Public Property Get Item(vntIndexKey As Variant) As CHistorico
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
     Count = mCol.Count
End If


End Property


Public Sub Remove(vntIndexKey As Variant)
   
    mCol.Remove vntIndexKey
End Sub


Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
     Set NewEnum = mCol.[_NewEnum]
End Property


Private Sub Class_Initialize()
   

    Set mCol = New Collection
End Sub


Private Sub Class_Terminate()
    'destroys collection when this class is terminated
    Set mCol = Nothing
End Sub


