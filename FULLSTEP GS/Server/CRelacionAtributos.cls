VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CRelacionAtributos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True

'********************* CRelacionAtributos ***********************
'*             Autor : NSA
'*             Creada : 29/01/2013
'****************************************************************
Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum

Private m_Col As Collection
Private m_oConexion As CConexion
Public m_bEOF As Boolean

Public Property Get Item(vntIndexKey As Variant) As CRelacionAtributo
On Error GoTo NoSeEncuentra:

    Set Item = m_Col(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property
Public Property Get Count() As Long
    If m_Col Is Nothing Then
        Count = 0
    Else
         Count = m_Col.Count
    End If
End Property
Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
    Set NewEnum = m_Col.[_NewEnum]
End Property
Public Sub Remove(vntIndexKey As Variant)

   
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_Col.Remove vntIndexKey
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CRelacionAtributos", "Remove", ERR, Erl)
      Exit Sub
   End If
End Sub
Private Sub Class_Initialize()
   
    Set m_Col = New Collection
End Sub
Private Sub Class_Terminate()
    'destroys collection when this class is terminated
    Set m_Col = Nothing
    Set m_oConexion = Nothing
End Sub

Friend Property Set Conexion(ByVal con As CConexion)
Set m_oConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = m_oConexion
End Property

Public Property Get eof() As Boolean
    eof = m_bEOF
End Property

Public Property Let eof(ByVal vNewValue As Boolean)
    m_bEOF = vNewValue
End Property


Public Function Add(ByVal Atrib As Variant, ByVal AtribRel As Variant, Optional ByVal Id As Integer, Optional ByVal Cod As Variant, Optional ByVal ValorNum As Variant, _
                    Optional ByVal ValorBool As Variant, Optional ByVal ValorText As Variant, Optional ByVal ValorFec As Variant, _
                    Optional ByVal FecAct As Variant, Optional ByVal AtribRelCod As Variant, Optional ByVal AtribRelDen As Variant, _
                    Optional ByVal AtribRelDescr As Variant, Optional ByVal AtribRelTipo As Variant, Optional ByVal AtribRelIntro As Variant, Optional ByVal AtribMinimo As Variant, Optional ByVal AtribMaximo As Variant) As CRelacionAtributo
    
    'create a new object
    Dim objnewmember As CRelacionAtributo
    
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New CRelacionAtributo
    Set objnewmember.Conexion = m_oConexion
        
        objnewmember.Id = Id
        objnewmember.Cod = Cod
        objnewmember.AtribID = Atrib
        objnewmember.AtribIdRel = AtribRel
        
    If Not IsMissing(AtribMinimo) Then
        objnewmember.Minimo = AtribMinimo
    Else
        objnewmember.Minimo = Null
    End If
    If Not IsMissing(AtribMaximo) Then
        objnewmember.Maximo = AtribMaximo
    Else
        objnewmember.Maximo = Null
    End If
    
    If Not IsMissing(ValorNum) Then
        objnewmember.ValorNum = ValorNum
    Else
        objnewmember.ValorNum = Null
    End If
    
    If Not IsMissing(ValorBool) Then
        objnewmember.ValorBool = ValorBool
    Else
        objnewmember.ValorBool = Null
    End If
    
    If Not IsMissing(ValorText) Then
        objnewmember.ValorText = ValorText
    Else
        objnewmember.ValorText = Null
    End If
    
    If Not IsMissing(ValorFec) Then
        objnewmember.ValorFec = ValorFec
    Else
        objnewmember.ValorFec = Null
    End If
        
    If Not IsMissing(AtribRelCod) Then
        objnewmember.AtribRelCod = AtribRelCod
    Else
        objnewmember.AtribRelCod = Null
    End If
    
    If Not IsMissing(AtribRelDen) Then
        objnewmember.AtribRelDen = AtribRelDen
    Else
        objnewmember.AtribRelDen = Null
    End If
    
    If Not IsMissing(AtribRelDescr) Then
        objnewmember.AtribRelDescr = AtribRelDescr
    Else
        objnewmember.AtribRelDescr = Null
    End If
    
    If Not IsMissing(AtribRelTipo) Then
        objnewmember.AtribRelTipo = AtribRelTipo
    Else
        objnewmember.AtribRelTipo = Null
    End If
    
    If Not IsMissing(AtribRelIntro) Then
        objnewmember.AtribRelSeleccion = AtribRelIntro
    Else
        objnewmember.AtribRelSeleccion = Null
    End If
    
    If Not IsMissing(FecAct) Then
        objnewmember.FecAct = FecAct
    Else
        objnewmember.FecAct = Null
    End If
        
        If Not IsMissing(AtribRel) And Not IsNull(AtribRel) Then
            m_Col.Add objnewmember, CStr(AtribRel)
        Else
            m_Col.Add objnewmember, CStr(AtribRel)
        End If
    
    
    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CRelacionAtributos", "Add", ERR, Erl)
      Exit Function
   End If

End Function

Public Sub CargarRelacionesAtributo(ByVal IdAtrib As Integer)

Dim lIndice As Long

Dim adocom As adodb.Command
Dim AdoRes As adodb.Recordset
Dim oParam As adodb.Parameter

Dim adoflID As adodb.Field
Dim adoflCod As adodb.Field
Dim adoflAtribRel As adodb.Field
Dim adoflValNum As adodb.Field
Dim adoflValBool As adodb.Field
Dim adoflValText As adodb.Field
Dim adoflValFec As adodb.Field
Dim adoflFecAct As adodb.Field
Dim adoflAtribRelCod As adodb.Field
Dim adoflAtribRelDen As adodb.Field
Dim adoflAtribRelDescr As adodb.Field
Dim adoflAtribRelTipo As adodb.Field
Dim adoflAtribRelIntro As adodb.Field
Dim adoflAtribRelMinimo As adodb.Field
Dim adoflAtribRelMaximo As adodb.Field


'********Precondici�n******************************************************************************************************************
If m_oConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CDestino.AnyadirABAseDatos", "No se ha establecido la conexion"
End If
'**************************************************************************************************************************************

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

''' Preparar la SP y sus parametros
    
    Set adocom = New adodb.Command
    Set adocom.ActiveConnection = m_oConexion.ADOCon
    
    
    adocom.CommandText = "ATR_CARGAR_RELACIONES"
    'Pasamos el id del atributo de integracion al stored para buscar sus relaciones
    Set oParam = adocom.CreateParameter("ATRIBID", adInteger, adParamInput, , IdAtrib)
    adocom.Parameters.Append oParam
 
    
    adocom.CommandType = adCmdStoredProc
    ''' Ejecutar la SP
    Set AdoRes = adocom.Execute
   
    Set adocom.ActiveConnection = Nothing
    Set adocom = Nothing
    
    If AdoRes.eof Then
        AdoRes.Close
        Set AdoRes = Nothing
        Exit Sub
    
    Else
   
        Set adoflID = AdoRes.Fields("ID") 'ID de INT_ATRIB_REL
        Set adoflCod = AdoRes.Fields("COD") 'COD Operando de la formula
        Set adoflAtribRel = AdoRes.Fields("ATRIB_REL") 'DEF_ATRIB.ID
        Set adoflValNum = AdoRes.Fields("VALOR_NUM")
        Set adoflValBool = AdoRes.Fields("VALOR_BOOL")
        Set adoflValText = AdoRes.Fields("VALOR_TEXT")
        Set adoflValFec = AdoRes.Fields("VALOR_FEC")
        Set adoflFecAct = AdoRes.Fields("FECACT")
        Set adoflAtribRelCod = AdoRes.Fields("CODATRIB")
        Set adoflAtribRelDen = AdoRes.Fields("DEN")
        Set adoflAtribRelDescr = AdoRes.Fields("DESCR")
        Set adoflAtribRelTipo = AdoRes.Fields("TIPO") 'TIPO de DEF_ATRIB del Atributo relacionado
        Set adoflAtribRelIntro = AdoRes.Fields("INTRO") 'Si el atributo con el que se hace relacion tiene una lista de valores
        Set adoflAtribRelMinimo = AdoRes.Fields("MINIMO")
        Set adoflAtribRelMaximo = AdoRes.Fields("MAXIMO")
        lIndice = 1
        While Not AdoRes.eof
            
                Me.Add IdAtrib, adoflAtribRel.Value, adoflID.Value, adoflCod.Value, adoflValNum.Value, SQLBinaryToBoolean(adoflValBool.Value), adoflValText.Value, adoflValFec.Value, adoflFecAct, adoflAtribRelCod.Value, adoflAtribRelDen.Value, adoflAtribRelDescr.Value, adoflAtribRelTipo.Value, adoflAtribRelIntro.Value, adoflAtribRelMinimo.Value, adoflAtribRelMaximo.Value
           
            lIndice = lIndice + 1
            AdoRes.MoveNext
        Wend
        
        
        Set adoflID = Nothing
        Set adoflCod = Nothing
        Set adoflAtribRel = Nothing
        Set adoflValNum = Nothing
        Set adoflValBool = Nothing
        Set adoflValText = Nothing
        Set adoflValFec = Nothing
        Set adoflFecAct = Nothing
        Set adoflAtribRelCod = Nothing
        Set adoflAtribRelDen = Nothing
        Set adoflAtribRelDescr = Nothing
        Set adoflAtribRelTipo = Nothing
        Set adoflAtribRelIntro = Nothing
        Set adoflAtribRelMinimo = Nothing
        Set adoflAtribRelMaximo = Nothing
        
        AdoRes.Close
        Set AdoRes = Nothing
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CRelacionAtributos", "CargarRelacionesAtributo", ERR, Erl)
      Exit Sub
   End If
    
End Sub

Public Function ObtenerCod(ByVal IdAtrib As Long) As Long
Dim sConsulta As String
Dim AdoRes As adodb.Recordset



If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sConsulta = "SELECT max(case ISNUMERIC(RIGHT(COD,LEN(COD)-1)) when 1 then CAST(RIGHT(COD,LEN(COD)-1) AS INTEGER) else 0 end) AS COD FROM INT_ATRIB_RELACION "
    sConsulta = sConsulta & " WHERE ATRIB_ID=" & IdAtrib & " AND COD like 'X%'"
    
    Set AdoRes = New adodb.Recordset
    AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
    
    If IsNull(AdoRes.Fields("COD").Value) Then
        ObtenerCod = 1
    Else
        ObtenerCod = AdoRes.Fields("COD").Value + 1
    End If

    AdoRes.Close
    Set AdoRes = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CRelacionAtributos", "ObtenerCod", ERR, Erl)
      Exit Function
   End If
    
End Function




