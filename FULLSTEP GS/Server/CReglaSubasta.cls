VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CReglaSubasta"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private m_iAnyo As Integer
Private m_sGMN1 As String
Private m_iProce As Long
Private m_iID As Integer
Private m_sNom As String
Private m_vCOM As Variant
Private m_lngAdjunProce As Long

Private m_adoComm As adodb.Command
Private m_adores As adodb.Recordset
Private m_lDataSize As Long

Private m_oConexion As CConexion

Friend Property Set Conexion(ByVal con As CConexion)
    Set m_oConexion = con
End Property

Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property


Public Property Get Anyo() As Integer
    Anyo = m_iAnyo
End Property

Public Property Let Anyo(ByVal iNewValue As Integer)
    m_iAnyo = iNewValue
End Property

Public Property Get GMN1() As String
    GMN1 = m_sGMN1
End Property

Public Property Let GMN1(ByVal sNewValue As String)
    m_sGMN1 = sNewValue
End Property

Public Property Get Proce() As Long
    Proce = m_iProce
End Property

Public Property Let Proce(ByVal iNewValue As Long)
    m_iProce = iNewValue
End Property

Public Property Get Id() As Integer
    Id = m_iID
End Property

Public Property Let Id(ByVal iNewValue As Integer)
    m_iID = iNewValue
End Property

Public Property Get Nom() As String
    Nom = m_sNom
End Property

Public Property Let Nom(ByVal sNewValue As String)
    m_sNom = sNewValue
End Property

Public Property Get COM() As Variant
    COM = m_vCOM
End Property

Public Property Let COM(ByVal vNewValue As Variant)
    m_vCOM = vNewValue
End Property

Public Property Get AdjunProce() As Long
    AdjunProce = m_lngAdjunProce
End Property

Public Property Let AdjunProce(ByVal lngNewValue As Long)
    m_lngAdjunProce = lngNewValue
End Property

Public Property Get DataSize() As Long
    DataSize = m_lDataSize
End Property
Public Property Let DataSize(ByVal lvar As Long)
    m_lDataSize = lvar
End Property

''' <summary>Lleva a cabo las operaciones de lectura de BD de una regla de subasta</summary>
''' <returns>Variable de tipo TipoErrorSummit con el error si se ha producido</returns>
''' <remarks>Llamada desde: FSGSClient; Tiempo m�ximo: < 1seg </remarks>
''' <revision>LTG 20/01/2011</revision>

Public Function ComenzarLecturaData() As TipoErrorSummit
    Dim sConsulta As String
    Dim TESError As TipoErrorSummit
    Dim iNum As Integer
    Dim sFSP As String
    Dim AdoRes As adodb.Recordset
    
    '********* Precondicion **************************************
    If m_oConexion Is Nothing Then
        Err.Raise vbObjectError + 613, "CEspecificacion.ComenzarLecturaData", "No se ha establecido la conexion"
        Exit Function
    End If
    '*************************************************************
        
    TESError.NumError = TESnoerror
    
    On Error GoTo Error
      
    sConsulta = "SELECT DATALENGTH(DATA) TAMANYO "
    sConsulta = sConsulta & " FROM ADJUN_PROCE WITH (NOLOCK) INNER JOIN PROCE_SUBASTA_REGLAS PSR WITH (NOLOCK) ON PSR.ADJUN_PROCE =ADJUN_PROCE.ID"
    sConsulta = sConsulta & " WHERE ADJUN_PROCE.ID =?"
        
    Set m_adoComm = New adodb.Command
    
    m_adoComm.ActiveConnection = m_oConexion.ADOCon
    m_adoComm.CommandText = sConsulta
    m_adoComm.Parameters.Item(0).Value = m_lngAdjunProce
    m_adoComm.CommandType = adCmdText
    
    Set m_adores = m_adoComm.Execute
    
    If m_adores.eof Then
        TESError.NumError = TESDatoEliminado
        TESError.Arg1 = 65 '"fichero adjunto"
        m_lDataSize = 0
        m_adores.Close
        Set m_adores = Nothing
        Set m_adoComm = Nothing
        ComenzarLecturaData = TESError
        Exit Function
    End If
    
    m_lDataSize = m_adores("TAMANYO").Value
    ComenzarLecturaData = TESError
    Exit Function

Error:
    ComenzarLecturaData = basErrores.TratarError(m_oConexion.ADOCon)
    If Not m_adores Is Nothing Then
        m_adores.Close
        Set m_adores = Nothing
        Set m_adoComm = Nothing
    End If
End Function

''' <summary>Lleva a cabo las operaciones de fim de lectura de BD de una regla de subasta</summary>
''' <remarks>Llamada desde: FSGSClient; Tiempo m�ximo: < 1seg </remarks>
''' <revision>LTG 20/01/2011</revision>

Public Sub FinalizarLecturaData()
    On Error Resume Next

    If Not m_adores Is Nothing Then
        m_adores.Close
    End If
    Set m_adores = Nothing
    Set m_adoComm = Nothing
End Sub

''' <summary>Lee de BD una serie de bytes de una regla de subasta</summary>
''' <param name="lBytes">n� de bytes a leer</param>
''' <returns>Datos le�dos</returns>
''' <remarks>Llamada desde: FSGSClient; Tiempo m�ximo: < 1seg </remarks>
''' <revision>LTG 20/01/2011</revision>

Public Function ReadData(ByVal lBytes As Long) As Variant
    Dim bytChunk() As Byte
    
    On Error GoTo Error:

    bytChunk() = m_adores("DATA").GetChunk(lBytes)
    ReadData = bytChunk
    Exit Function
    
Error:
    
    If Not m_adores Is Nothing Then
        m_adores.Close
    End If
    Set m_adoComm = Nothing
    Set m_adores = Nothing
    ReadData = Null
End Function

Private Sub Class_Initialize()
    m_lDataSize = 0
End Sub

Private Sub Class_Terminate()
    Set m_adores = Nothing
    Set m_adoComm = Nothing
    Set m_oConexion = Nothing
End Sub
