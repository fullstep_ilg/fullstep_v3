VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CUnidadesNegQA"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

''' Variables privadas con la informacion de un material qa
Private m_oConexion As CConexion
Private m_oConexionServer As CConexionDeUseServer

Private mCol As Collection

Public Function Add(ByVal Id As String, ByVal unQA1 As Integer, ByVal unQA2 As Integer, ByVal Cod As String, ByVal Den As String, ByVal Seleccionada As Boolean, Optional ByVal Permiso As Integer = 0) As CUnidadNegQA
    Dim objnewmember As CUnidadNegQA
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New CUnidadNegQA
    
    objnewmember.Id = CInt(0 & Id)
    objnewmember.Cod = Cod
    objnewmember.unQA1 = unQA1
    objnewmember.unQA2 = unQA2
    objnewmember.Den = Den
    objnewmember.Seleccionada = Seleccionada
    If Not Permiso = 0 Then
        objnewmember.Permiso = Permiso
    End If
    Set objnewmember.UnidadesNegQA = Nothing
    Set objnewmember.UnidadesNegQA = New CUnidadesNegQA
    Set objnewmember.UnidadesNegQA.Conexion = m_oConexion
    Set objnewmember.UnidadesNegQA.ConexionServer = m_oConexionServer
    
    Set objnewmember.Conexion = m_oConexion
    Set objnewmember.ConexionServer = m_oConexionServer
        
    mCol.Add objnewmember, Id & IIf(Permiso = 0, "", "#" & Permiso)
    
    Set objnewmember = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CUnidadesNegQA", "Add", ERR, Erl)
      Exit Function
   End If
End Function

Public Sub Remove(vntIndexKey As Variant)
On Error GoTo NoExisteLaClave:
    
    mCol.Remove vntIndexKey

NoExisteLaClave:
Resume Next

End Sub

Public Property Set Conexion(ByVal vData As CConexion)

    ''' * Objetivo: Dar valor a la conexion de la moneda
    ''' * Recibe: Conexion
    ''' * Devuelve: Nada

    Set m_oConexion = vData
    
End Property

Public Property Get Conexion() As CConexion

       ''' * Objetivo: Devolver la conexion de la moneda
       ''' * Recibe: Nada
       ''' * Devuelve: Conexion

       Set Conexion = m_oConexion
    
End Property

Public Function SacarNivelEscalacion(ByVal iUNQA_Nivel As Integer) As String
Dim sConsulta As String
Dim rs As New adodb.Recordset
Dim s As String
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
sConsulta = "SELECT NIVEL FROM NIVEL_ESCALACION WITH (NOLOCK) WHERE UNQA_NIVEL=" & iUNQA_Nivel
rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
Do While Not rs.eof
    If s <> "" Then
        s = s & "#"
    End If
    s = s & rs("NIVEL").Value
    rs.MoveNext
Loop
rs.Close
Set rs = Nothing
SacarNivelEscalacion = s
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CUnidadesNegQA", "SacarNivelEscalacion", ERR, Erl)
      Exit Function
   End If

End Function

Private Sub Class_Initialize()
    Set mCol = New Collection
End Sub

Private Sub Class_Terminate()
''' * Objetivo: Limpiar la memoria
    Set mCol = Nothing
    Set m_oConexion = Nothing
    Set m_oConexionServer = Nothing
End Sub


Public Property Get Item(vntIndexKey As Variant) As CUnidadNegQA
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Public Property Set ConexionServer(ByVal con As CConexionDeUseServer)
    Set m_oConexionServer = con
End Property

Public Property Get ConexionServer() As CConexionDeUseServer
    Set ConexionServer = m_oConexionServer
End Property

Public Property Get Count() As Long
    If mCol Is Nothing Then
        Count = 0
    Else
         Count = mCol.Count
    End If
End Property

Public Property Get Unidades() As Collection
    Set Unidades = mCol
End Property


'************************************************************
'*** Descripci�n: Devuelve el nodo raiz del arbol               *****
'*** Par�metros de entrada:           *****
' Usu: Codigo Usuario seleccionado
' Idioma: idioma del usuario
' bPyme: Indica si estamos en una instalacion de Pymes
'iPymes: Es el id de la Pyme en caso de estar en pymes
'*** Par�metros de salida: Explicaci�n de que es lo que    *****
'***                     devuelve                          *****
'*** Llamada desde: frmUsuarios.GenerarEstructuraUnidadesNegocio                      *****
'*** Tiempo m�ximo: XXXXX                       *****
'************************************************************

Public Sub NodoRaiz(ByVal USU As String, ByVal Idioma As String, ByVal bPyme As Boolean, ByVal iPyme As Integer)

Dim oUnQA1 As CUnidadNegQA
Dim sConsulta As String
Dim rs As New adodb.Recordset
Dim fldId As adodb.Field
Dim fldCod As adodb.Field
Dim fldDen As adodb.Field
Dim fldUNQA1 As adodb.Field
Dim fldUNQA2 As adodb.Field
Dim fldSel As adodb.Field

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
        Set mCol = Nothing
        Set mCol = New Collection


        ' Generamos la coleccion de grupos mat nivel1
        sConsulta = "SELECT distinct UNQA.ID,0 AS UNQA1, 0 AS UNQA2,UNQA.COD,I.DEN, CASE WHEN UU.USU IS NULL THEN 0 ELSE 1 END AS SELECCIONADA FROM UNQA with (nolock)"
        sConsulta = sConsulta & " INNER JOIN UNQA_IDIOMAS I with (nolock) ON UNQA.ID = I.UNQA AND IDIOMA= '" & Idioma & "'"
        sConsulta = sConsulta & " left join USU_UNQA UU with (nolock) on UU.UNQA = ID and usu= '" & USU & "'"
        sConsulta = sConsulta & " WHERE NIVEL = 0"
        If bPyme Then
            sConsulta = sConsulta & " AND PYME= " & iPyme
        End If
        
        rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
                
        If rs.eof Then
            Set rs = Nothing
            Exit Sub
        End If
                
               
        While Not rs.eof
        
            Set fldId = rs.Fields("ID")
            Set fldUNQA1 = rs.Fields("UNQA1")
            Set fldUNQA2 = rs.Fields("UNQA2")
            Set fldCod = rs.Fields("COD")
            Set fldDen = rs.Fields("DEN")
            Set fldSel = rs.Fields("SELECCIONADA")

           
            Set oUnQA1 = Me.Add(CStr(fldId.Value), fldUNQA1.Value, fldUNQA2.Value, fldCod.Value, fldDen.Value, fldSel)
            
            rs.MoveNext
            Set oUnQA1 = Nothing
        Wend
                                
        Set fldId = Nothing
        Set fldUNQA1 = Nothing
        Set fldUNQA2 = Nothing
        Set fldCod = Nothing
        Set fldDen = Nothing
        Set fldSel = Nothing

        
        Set oUnQA1 = Nothing
        
        rs.Close
        Set rs = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CUnidadesNegQA", "NodoRaiz", ERR, Erl)
      Exit Sub
   End If
        
End Sub


''' <summary>
''' Devuelve todas las unidades de negocio qa a partir del nivel1 para generar el arbol
''' </summary>
''' <param name="Usu">Codigo Usuario seleccionado</param>
''' <param name="Idioma">idioma del usuario</param>
''' <param name="bPyme">Indica si estamos en una instalacion de Pymes</param>
''' <param name="iPymes">Es el id de la Pyme en caso de estar en pymes</param>
''' <param name="bNivel0">Cargar o no la unidad de nivel 0 </param>
''' <remarks>Llamada desde: frmUsuarios.GenerarEstructuraUnidadesNegocio
''' frmComparativaQA/Form_Load; Tiempo m�ximo: 0,1</remarks>
Public Sub GenerarEstructuraUnidadesNegQA(ByVal USU As String, ByVal Idioma As String, ByVal bPyme As Boolean _
, ByVal iPyme As Integer, Optional ByVal bNivel0 As Boolean = False)
Dim oUnQA0 As CUnidadNegQA
Dim oUnQA1 As CUnidadNegQA
Dim oUnQA2 As CUnidadNegQA
Dim oUnQA3 As CUnidadNegQA
Dim sConsulta As String
Dim sCod As String
Dim rs As New adodb.Recordset
Dim fldId As adodb.Field
Dim fldCod As adodb.Field
Dim fldDen As adodb.Field
Dim fldUNQA1 As adodb.Field
Dim fldUNQA2 As adodb.Field
Dim fldSel As adodb.Field

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
Set mCol = Nothing
Set mCol = New Collection

    'Nivel 0
    If bNivel0 Then
        sConsulta = "SELECT DISTINCT UNQA.ID,-1 AS UNQA1, 0 AS UNQA2,UNQA.COD,I.DEN, CASE WHEN UU.USU IS NULL THEN 0 ELSE 1 END AS SELECCIONADA FROM UNQA with (nolock)"
        sConsulta = sConsulta & " INNER JOIN UNQA_IDIOMAS I with (nolock) ON UNQA.ID = I.UNQA AND IDIOMA= '" & DblQuote(Idioma) & "'"
        sConsulta = sConsulta & " LEFT JOIN USU_UNQA UU with (nolock) on UU.UNQA = ID AND USU= '" & DblQuote(USU) & "'"
        sConsulta = sConsulta & " WHERE NIVEL = 0 AND BAJALOG=0 "
        If bPyme Then
            sConsulta = sConsulta & " AND PYME = " & iPyme
        End If
        
        rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
                
        If rs.eof Then
            Set rs = Nothing
            Exit Sub
        End If
                
               
        While Not rs.eof
        
            Set fldId = rs.Fields("ID")
            Set fldUNQA1 = rs.Fields("UNQA1")
            Set fldUNQA2 = rs.Fields("UNQA2")
            Set fldCod = rs.Fields("COD")
            Set fldDen = rs.Fields("DEN")
            Set fldSel = rs.Fields("SELECCIONADA")
        
           
            Set oUnQA0 = Me.Add(CStr(fldId.Value), fldUNQA1.Value, fldUNQA2.Value, fldCod.Value, fldDen.Value, fldSel)
            
            rs.MoveNext
            Set oUnQA0 = Nothing
        Wend
                        
        Set fldId = Nothing
        Set fldUNQA1 = Nothing
        Set fldUNQA2 = Nothing
        Set fldCod = Nothing
        Set fldDen = Nothing
        Set fldSel = Nothing
                        
        rs.Close
        Set rs = Nothing
    End If

     ' Generamos la coleccion de grupos mat nivel1
     sConsulta = "SELECT distinct UNQA.ID,0 AS UNQA1, 0 AS UNQA2,UNQA.COD,I.DEN, CASE WHEN UU.USU IS NULL THEN 0 ELSE 1 END AS SELECCIONADA FROM UNQA with (nolock)"
     sConsulta = sConsulta & " INNER JOIN UNQA_IDIOMAS I with (nolock) ON UNQA.ID = I.UNQA AND IDIOMA= '" & DblQuote(Idioma) & "'"
     sConsulta = sConsulta & " left join USU_UNQA UU with (nolock) on UU.UNQA = ID and usu= '" & DblQuote(USU) & "'"
     sConsulta = sConsulta & " WHERE NIVEL = 1 AND BAJALOG=0 "
     If bPyme Then
         sConsulta = sConsulta & " AND PYME = " & iPyme
     End If
     
     rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
             
     If rs.eof Then
         Set rs = Nothing
         Exit Sub
     End If
             
            
     While Not rs.eof
     
         Set fldId = rs.Fields("ID")
         Set fldUNQA1 = rs.Fields("UNQA1")
         Set fldUNQA2 = rs.Fields("UNQA2")
         Set fldCod = rs.Fields("COD")
         Set fldDen = rs.Fields("DEN")
         Set fldSel = rs.Fields("SELECCIONADA")

        
         Set oUnQA1 = Me.Add(CStr(fldId.Value), fldUNQA1.Value, fldUNQA2.Value, fldCod.Value, fldDen.Value, fldSel)
         
         rs.MoveNext
         Set oUnQA1 = Nothing
     Wend
                     
     Set fldId = Nothing
     Set fldUNQA1 = Nothing
     Set fldUNQA2 = Nothing
     Set fldCod = Nothing
     Set fldDen = Nothing
     Set fldSel = Nothing
                     
     rs.Close
     Set rs = Nothing
             
     ' Generamos la coleccion de grupos mat nivel2
     
     sConsulta = "SELECT distinct UNQA.ID,UNQA.UNQA1, 0 AS UNQA2,COD,I.DEN, CASE WHEN UU.USU IS NULL THEN 0 ELSE 1 END AS SELECCIONADA FROM UNQA with (nolock)"
     sConsulta = sConsulta & " INNER JOIN UNQA_IDIOMAS I with (nolock) ON UNQA.ID = I.UNQA AND IDIOMA='" & DblQuote(Idioma) & "'"
     sConsulta = sConsulta & " left join USU_UNQA UU with (nolock) on UU.UNQA = ID and usu='" & DblQuote(USU) & "'"
     sConsulta = sConsulta & " WHERE NIVEL = 2 AND BAJALOG=0 "
     
     If bPyme Then
         sConsulta = sConsulta & " AND UNQA.UNQA1 IN "
         sConsulta = sConsulta & " (SELECT distinct UNQA.ID FROM UNQA with (nolock) INNER JOIN UNQA_IDIOMAS I with (nolock) ON UNQA.ID = I.UNQA AND IDIOMA= '" & DblQuote(Idioma) & "'"
         sConsulta = sConsulta & "  LEFT JOIN USU_UNQA UU with (nolock) on UU.UNQA = UNQA.ID and usu= '" & DblQuote(USU) & "'"
         sConsulta = sConsulta & "   WHERE NIVEL = 1 AND BAJALOG=0 AND PYME =" & iPyme & ")"
     End If
     
             
     rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    
     If rs.eof Then
         Set rs = Nothing
         Exit Sub
     End If
     
     
     While Not rs.eof
     
         Set fldId = rs.Fields("ID")
         Set fldUNQA1 = rs.Fields("UNQA1")
         Set fldUNQA2 = rs.Fields("UNQA2")
         Set fldCod = rs.Fields("COD")
         Set fldDen = rs.Fields("DEN")
         Set fldSel = rs.Fields("SELECCIONADA")

         sCod = fldUNQA1.Value
         Set oUnQA1 = Me.Unidades.Item(sCod)
         Set oUnQA2 = oUnQA1.UnidadesNegQA.Add(CStr(fldId.Value), fldUNQA1, fldUNQA2, fldCod.Value, fldDen.Value, fldSel)
         
         rs.MoveNext
         Set oUnQA1 = Nothing
         Set oUnQA2 = Nothing
         
     Wend
                     
     Set fldId = Nothing
     Set fldUNQA1 = Nothing
     Set fldUNQA2 = Nothing
     Set fldCod = Nothing
     Set fldDen = Nothing
     Set fldSel = Nothing
     
     rs.Close
     Set rs = Nothing
     
     
     ' Generamos la coleccion de grupos mat nivel3
     
     sConsulta = "SELECT distinct ID,UNQA.UNQA1,UNQA.UNQA2,COD,I.DEN, CASE WHEN UU.USU IS NULL THEN 0 ELSE 1 END AS SELECCIONADA FROM UNQA with (nolock)"
     sConsulta = sConsulta & " INNER JOIN UNQA_IDIOMAS I with (nolock) ON UNQA.ID = I.UNQA AND IDIOMA='" & DblQuote(Idioma) & "'"
     sConsulta = sConsulta & " left join USU_UNQA UU with (nolock) on UU.UNQA = ID and usu= '" & DblQuote(USU) & "'"
     sConsulta = sConsulta & " WHERE NIVEL = 3 AND BAJALOG=0 "
                 
     If bPyme Then
         sConsulta = sConsulta & " AND UNQA.UNQA2 IN "
         sConsulta = sConsulta & " (SELECT distinct UNQA.ID FROM UNQA with (nolock) INNER JOIN UNQA_IDIOMAS I with (nolock) ON UNQA.ID = I.UNQA AND IDIOMA= '" & DblQuote(Idioma) & "'"
         sConsulta = sConsulta & "  LEFT JOIN USU_UNQA UU with (nolock) on UU.UNQA = UNQA.ID and usu= '" & DblQuote(USU) & "'"
         sConsulta = sConsulta & "   WHERE NIVEL = 1 AND BAJALOG=0 AND PYME = " & iPyme & ")"
     End If
     
     
     rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    
     If rs.eof Then
         Set rs = Nothing
         Exit Sub
     End If
             
     
     While Not rs.eof
     
         Set fldId = rs.Fields("ID")
         Set fldUNQA1 = rs.Fields("UNQA1")
         Set fldUNQA2 = rs.Fields("UNQA2")
         Set fldCod = rs.Fields("COD")
         Set fldDen = rs.Fields("DEN")
         Set fldSel = rs.Fields("SELECCIONADA")
     
         sCod = fldUNQA2.Value
         Set oUnQA1 = Me.Unidades(sCod)
         sCod = fldUNQA1.Value
         Set oUnQA2 = oUnQA1.UnidadesNegQA.Unidades(sCod)
         Set oUnQA3 = oUnQA2.UnidadesNegQA.Add(CStr(fldId.Value), fldUNQA1, fldUNQA2, fldCod.Value, fldDen.Value, fldSel)
                     
         rs.MoveNext
         Set oUnQA1 = Nothing
         Set oUnQA2 = Nothing
         Set oUnQA3 = Nothing
         
                    
     Wend
     
     Set fldId = Nothing
     Set fldUNQA1 = Nothing
     Set fldUNQA2 = Nothing
     Set fldCod = Nothing
     Set fldDen = Nothing
     Set fldSel = Nothing
     
     rs.Close
     Set rs = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CUnidadesNegQA", "GenerarEstructuraUnidadesNegQA", ERR, Erl)
      Exit Sub
   End If
        
End Sub



'************************************************************
'*** Descripci�n:A�ade un permiso del usuario sobre una de las unidades de negocio         *****
'*** Par�metros de entrada:           *****
' Usu: Codigo Usuario seleccionado
'*** Par�metros de salida: Devuelve si se ha producido error en la insercion     *****
'***                                               *****
'*** Llamada desde:                       *****
'*** Tiempo m�ximo: XXXXX                       *****
'************************************************************

Public Function AniadirUnidades(ByVal USU As String) As TipoErrorSummit

Dim sConsulta As String
Dim TESError As TipoErrorSummit
Dim AdoRes As adodb.Recordset
Dim bTransaccionEnCurso As Boolean
Dim i As Integer

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
TESError.NumError = TESnoerror

If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:

    If Me.Count = 0 Then
        Exit Function
        AniadirUnidades = TESError
    End If
    
    
    m_oConexion.ADOCon.Execute "BEGIN DISTRIBUTED TRAN"
    bTransaccionEnCurso = True
    m_oConexion.ADOCon.Execute "SET XACT_ABORT ON"
    
    For i = 1 To Me.Count
        sConsulta = sConsulta & " INSERT INTO USU_UNQA (UNQA,USU,Permiso) VALUES (" & Me.Unidades.Item(i).Id & ",'" & DblQuote(USU) & "'," & Me.Unidades.Item(i).Permiso & ") "
    Next
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    
    m_oConexion.ADOCon.Execute "SET XACT_ABORT OFF"
    m_oConexion.ADOCon.Execute "COMMIT TRAN"
    bTransaccionEnCurso = False
       
    AniadirUnidades = TESError
    Exit Function
Error_Cls:

    If Not AdoRes Is Nothing Then
        AdoRes.Close
        Set AdoRes = Nothing
    End If
                 
    AniadirUnidades = basErrores.TratarError(m_oConexion.ADOCon.Errors)
                 
                 
    If bTransaccionEnCurso Then
        m_oConexion.RollbackTransaction
        bTransaccionEnCurso = False
    End If
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      'Call g_oErrores.TratarError("Class Module", "CUnidadesNegQA", "AniadirUnidades", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If

End Function


'************************************************************
'*** Descripci�n: Elimina las unidades seleccionadas en el arbol, en la pagina se filtran las q ya estaban seleccionadas        *****
'*** Par�metros de entrada:           *****
' Usu: Codigo Usuario seleccionado
'*** Par�metros de salida: Devuelve si se ha producido error en la insercion     *****
'***                                               *****
'*** Llamada desde:                       *****
'*** Tiempo m�ximo: XXXXX                       *****
'************************************************************


Public Function EliminarUnidades(ByVal USU As String) As TipoErrorSummit

Dim sConsulta As String
Dim TESError As TipoErrorSummit
Dim AdoRes As adodb.Recordset
Dim bTransaccionEnCurso As Boolean
Dim i As Integer

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
TESError.NumError = TESnoerror

If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:

    If Me.Count = 0 Then
        Exit Function
        EliminarUnidades = TESError
    End If
    
    
    m_oConexion.ADOCon.Execute "BEGIN DISTRIBUTED TRAN"
    bTransaccionEnCurso = True
    m_oConexion.ADOCon.Execute "SET XACT_ABORT ON"
    
    For i = 1 To Me.Count
        sConsulta = sConsulta & " DELETE FROM USU_UNQA WHERE UNQA = " & Me.Unidades.Item(i).Id & " AND USU ='" & DblQuote(USU) & "' AND Permiso=" & Me.Unidades.Item(i).Permiso
    Next
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    
    m_oConexion.ADOCon.Execute "SET XACT_ABORT OFF"
    m_oConexion.ADOCon.Execute "COMMIT TRAN"
    bTransaccionEnCurso = False
       
    EliminarUnidades = TESError
    Exit Function
    
Error_Cls:

    If Not AdoRes Is Nothing Then
        AdoRes.Close
        Set AdoRes = Nothing
    End If
                 
    EliminarUnidades = basErrores.TratarError(m_oConexion.ADOCon.Errors)
                 
                 
    If bTransaccionEnCurso Then
        m_oConexion.ADOCon.Execute "SET XACT_ABORT OFF"
        bTransaccionEnCurso = False
    End If
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      'Call g_oErrores.TratarError("Class Module", "CUnidadesNegQA", "EliminarUnidades", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If

End Function



'************************************************************
'*** Descripci�n: Devuelve todas las unidades de negocio a las que tiene acceso el usuario en el nivel q se le indica             *****
'*** Par�metros de entrada:           *****
' Usu: Codigo Usuario seleccionado
' Idioma: idioma del usuario
' Nivel: Nivel dentro del arbol desde 0 hasta 3
'*** Par�metros de salida: Explicaci�n de que es lo que    *****
'***                     devuelve                          *****
'*** Llamada desde: frmUsuarios.GenerarEstructuraUnidadesNegocio                      *****
'*** Tiempo m�ximo: XXXXX                       *****
'************************************************************


Public Sub CargarTodosLasUnidadesNeg(ByVal USU As String, ByVal Idioma As String, ByVal Nivel As Integer)
    Dim rs As New adodb.Recordset
    Dim fldId As adodb.Field
    Dim fldUNQA1 As adodb.Field
    Dim fldUNQA2 As adodb.Field
    Dim fldCod As adodb.Field
    Dim fldDen As adodb.Field
    Dim sConsulta As String
        
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Select Case Nivel
    
    Case 0
        sConsulta = "SELECT distinct ID,0 AS UNQA1, 0 AS UNQA2,COD,I.DEN, CASE WHEN UU.USU IS NULL THEN 0 ELSE 1 END AS SELECCIONADA FROM UNQA with (nolock)"
        sConsulta = sConsulta & " INNER JOIN UNQA_IDIOMAS I with (nolock) ON UNQA.ID = I.UNQA AND IDIOMA= '" & Idioma & "'"
        sConsulta = sConsulta & " INNER join USU_UNQA UU with (nolock) on UU.UNQA = ID and usu= '" & USU & "'"
        sConsulta = sConsulta & " WHERE NIVEL = 0 AND BAJALOG=0 "

    Case 1
    
        sConsulta = "SELECT distinct ID,0 AS UNQA1, 0 AS UNQA2,COD,I.DEN, CASE WHEN UU.USU IS NULL THEN 0 ELSE 1 END AS SELECCIONADA FROM UNQA with (nolock)"
        sConsulta = sConsulta & " INNER JOIN UNQA_IDIOMAS I with (nolock) ON UNQA.ID = I.UNQA AND IDIOMA= '" & Idioma & "'"
        sConsulta = sConsulta & " INNER join USU_UNQA UU with (nolock) on UU.UNQA = ID and usu= '" & USU & "'"
        sConsulta = sConsulta & " WHERE NIVEL = 1 AND BAJALOG=0 "

    Case 2
        sConsulta = "SELECT distinct ID,UNQA.UNQA1, 0 AS UNQA2,COD,I.DEN, CASE WHEN UU.USU IS NULL THEN 0 ELSE 1 END AS SELECCIONADA FROM UNQA with (nolock)"
        sConsulta = sConsulta & " INNER JOIN UNQA_IDIOMAS I with (nolock) ON UNQA.ID = I.UNQA AND IDIOMA='" & Idioma & "'"
        sConsulta = sConsulta & " INNER join USU_UNQA UU with (nolock) on UU.UNQA = ID and usu='" & USU & "'"
        sConsulta = sConsulta & " WHERE NIVEL = 2 AND BAJALOG=0 "
    
    
    Case 3
        sConsulta = "SELECT distinct ID,UNQA.UNQA1,UNQA.UNQA2,COD,I.DEN, CASE WHEN UU.USU IS NULL THEN 0 ELSE 1 END AS SELECCIONADA FROM UNQA with (nolock)"
        sConsulta = sConsulta & " INNER JOIN UNQA_IDIOMAS I with (nolock) ON UNQA.ID = I.UNQA AND IDIOMA='" & Idioma & "'"
        sConsulta = sConsulta & " INNER join USU_UNQA UU with (nolock) on UU.UNQA = ID and usu= '" & USU & "'"
        sConsulta = sConsulta & " WHERE NIVEL = 3 AND BAJALOG=0"
        
    End Select
    
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    
    Set mCol = Nothing
    Set mCol = New Collection
    
    
    If rs.eof Then
        
        rs.Close
        Set rs = Nothing
        Exit Sub
      
    Else
            Set fldId = rs.Fields("ID")
            Set fldUNQA1 = rs.Fields("UNQA1")
            Set fldUNQA2 = rs.Fields("UNQA2")
            Set fldCod = rs.Fields("COD")
            Set fldDen = rs.Fields("DEN")
            'Set fldSel = rs.Fields("SELECCIONADA")
                
        While Not rs.eof
            Me.Add CStr(fldId.Value), fldUNQA1, fldUNQA2, fldCod.Value, "", True
            rs.MoveNext
        Wend
        
        Set fldId = Nothing
        Set fldUNQA1 = Nothing
        Set fldUNQA2 = Nothing
        Set fldCod = Nothing
        Set fldDen = Nothing
        'Set fldSel = Nothing
        
        
        rs.Close
        Set rs = Nothing
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CUnidadesNegQA", "CargarTodosLasUnidadesNeg", ERR, Erl)
      Exit Sub
   End If
End Sub

'********************************************************************
'*** Descripci�n: Devuelve el listado de los permisos que tiene *****
'*** el usuario sobre las distintas unidades de negocion QA     *****
'*** Par�metros de entrada:                                     *****
' Usu: Codigo Usuario seleccionado                              *****
'*** Par�metros de salida: Devuelve el listado de permisos      *****
'*** del usuario                                                *****
'*** Llamada desde: frmUsuarios                                 *****
'*** Tiempo m�ximo: XXXXX                                       *****
'********************************************************************

Public Sub PermisosUndNegUsuIniciales(ByVal USU As String)

Dim oUnQA1 As CUnidadNegQA
Dim sConsulta As String
Dim rs As New adodb.Recordset
Dim fldId As adodb.Field
Dim fldCod As adodb.Field
Dim fldDen As adodb.Field
Dim fldUNQA1 As adodb.Field
Dim fldUNQA2 As adodb.Field
Dim fldSel As adodb.Field
Dim fldPermiso As adodb.Field

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
        Set mCol = Nothing
        Set mCol = New Collection

        sConsulta = "SELECT UNQA AS ID,0 AS UNQA1,0 AS UNQA2,'' AS COD,'' AS DEN,0 AS Seleccionada,Permiso "
        sConsulta = sConsulta & "FROM USU_UNQA WITH (NOLOCK) WHERE USU='" & USU & "'"
        
        rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
                
        If rs.eof Then
            Set rs = Nothing
            Exit Sub
        End If
                
               
        While Not rs.eof
        
            Set fldId = rs.Fields("ID")
            Set fldUNQA1 = rs.Fields("UNQA1")
            Set fldUNQA2 = rs.Fields("UNQA2")
            Set fldCod = rs.Fields("COD")
            Set fldDen = rs.Fields("DEN")
            Set fldSel = rs.Fields("SELECCIONADA")
            Set fldPermiso = rs.Fields("Permiso")
           
            Set oUnQA1 = Me.Add(CStr(fldId.Value), fldUNQA1.Value, fldUNQA2.Value, fldCod.Value, fldDen.Value, fldSel, fldPermiso)
            
            rs.MoveNext
            Set oUnQA1 = Nothing
        Wend
                                
        Set fldId = Nothing
        Set fldUNQA1 = Nothing
        Set fldUNQA2 = Nothing
        Set fldCod = Nothing
        Set fldDen = Nothing
        Set fldSel = Nothing
        Set fldPermiso = Nothing

        
        Set oUnQA1 = Nothing
        
        rs.Close
        Set rs = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CUnidadesNegQA", "PermisosUndNegUsuIniciales", ERR, Erl)
      Exit Sub
   End If
        
End Sub

