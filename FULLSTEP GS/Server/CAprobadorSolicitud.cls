VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CAprobadorSolicitud"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum


Implements IBaseDatos

Private moConexion As CConexion

Private mlId As Long
Private mdLimite As Variant
Private miOrden As Integer
Private mlBloque As Long

Private m_vPersona As Variant
Private m_vUON1 As Variant
Private m_vUON2 As Variant
Private m_vUON3 As Variant
Private m_vCodDep As Variant

Private mdtFecAct As Variant
Private mvarIndice As Variant


Private m_adores As ADODB.Recordset


Friend Property Set Conexion(ByVal con As CConexion)
    Set moConexion = con
End Property

Friend Property Get Conexion() As CConexion
    Set Conexion = moConexion
End Property

Public Property Get Id() As Long
    Id = mlId
End Property

Public Property Let Id(ByVal Data As Long)
    mlId = Data
End Property

Public Property Get Bloque() As Long
    Bloque = mlBloque
End Property

Public Property Let Bloque(ByVal Data As Long)
    mlBloque = Data
End Property

Public Property Let Limite(ByVal dLimite As Variant)
    mdLimite = dLimite
End Property
Public Property Get Limite() As Variant
    Limite = mdLimite
End Property

Public Property Get Orden() As Integer
    Orden = miOrden
End Property

Public Property Let Orden(ByVal Data As Integer)
    miOrden = Data
End Property

Public Property Let Indice(ByVal varIndice As Variant)
    mvarIndice = varIndice
End Property
Public Property Get Indice() As Variant
    Indice = mvarIndice
End Property
Public Property Get FecAct() As Variant
    FecAct = mdtFecAct
End Property

Public Property Let FecAct(ByVal Data As Variant)
    mdtFecAct = Data
End Property

Public Property Get Persona() As Variant
    Persona = m_vPersona
End Property

Public Property Let Persona(sPer As Variant)
    m_vPersona = sPer
End Property

Public Property Let UON1(ByVal vData As Variant)
    m_vUON1 = vData
End Property
Public Property Get UON1() As Variant
    UON1 = m_vUON1
End Property

Public Property Let UON2(ByVal vData As Variant)
    m_vUON2 = vData
End Property
Public Property Get UON2() As Variant
    UON2 = m_vUON2
End Property

Public Property Let UON3(ByVal vData As Variant)
    m_vUON3 = vData
End Property
Public Property Get UON3() As Variant
    UON3 = m_vUON3
End Property

Public Property Let CodDep(ByVal vData As Variant)
    m_vCodDep = vData
End Property
Public Property Get CodDep() As Variant
    CodDep = m_vCodDep
End Property

Private Sub Class_Terminate()
    Set moConexion = Nothing
End Sub


Private Function IBaseDatos_AnyadirABaseDatos() As TipoErrorSummit
Dim sConsulta As String
Dim TESError As TipoErrorSummit
Dim AdoRes As ADODB.Recordset
Dim bTransaccionEnCurso As Boolean
Dim sFSP As String
Dim i As Integer
Dim sDen As String

TESError.NumError = TESnoerror

If moConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CWorkflow.AnyadirABAseDatos", "No se ha establecido la conexion"
End If
' *****************************************

On Error GoTo Error:
    
    moConexion.ADOCon.Execute "BEGIN TRAN"
    bTransaccionEnCurso = True
    moConexion.ADOCon.Execute "SET XACT_ABORT OFF"
    
    
    sConsulta = "SELECT MAX(ID) ID FROM APROBADORES"
    Set AdoRes = New ADODB.Recordset
    AdoRes.Open sConsulta, moConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    If IsNull(AdoRes("id").Value) Then
        mlId = 1
    Else
        mlId = AdoRes("ID").Value + 1
    End If
    AdoRes.Close
    Set AdoRes = Nothing
    
    
    sConsulta = "INSERT INTO APROBADORES (ID, BLOQUE_SEGURIDAD, PER,UON1,UON2,UON3,DEP, LIMITE, ORDEN) VALUES (" & mlId
    sConsulta = sConsulta & "," & mlBloque & "," & StrToSQLNULL(m_vPersona) & "," & StrToSQLNULL(m_vUON1) & "," & StrToSQLNULL(m_vUON2) & "," & StrToSQLNULL(m_vUON3) & "," & StrToSQLNULL(m_vCodDep)
    sConsulta = sConsulta & "," & DblToSQLFloat(mdLimite) & "," & miOrden & " )"
    moConexion.ADOCon.Execute sConsulta
    If moConexion.ADOCon.Errors.Count > 0 Then
        GoTo Error
    End If
    
    If Not IsNull(m_vPersona) And m_vPersona <> "" Then
        sConsulta = "UPDATE USU SET SOL_COMPRA=1 FROM USU U INNER JOIN PER P ON U.PER = P.COD WHERE P.COD = '" & DblQuote(m_vPersona) & "' AND U.SOL_COMPRA=0"
        moConexion.ADOCon.Execute sConsulta
        If moConexion.ADOCon.Errors.Count > 0 Then
            GoTo Error
        End If
    End If
    

    ''' Recuperar la fecha de insercion
    sConsulta = "SELECT FECACT FROM APROBADORES WHERE ID =" & mlId
    Set AdoRes = New ADODB.Recordset
    AdoRes.Open sConsulta, moConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    mdtFecAct = AdoRes("FECACT").Value
    AdoRes.Close
    Set AdoRes = Nothing
    
    moConexion.ADOCon.Execute "COMMIT TRAN"
    bTransaccionEnCurso = False
       
    IBaseDatos_AnyadirABaseDatos = TESError
    Exit Function
Error:

                 
    IBaseDatos_AnyadirABaseDatos = basErrores.TratarError(moConexion.ADOCon.Errors)
                 
                 
    If bTransaccionEnCurso Then
        moConexion.ADOCon.Execute "ROLLBACK TRAN"
        bTransaccionEnCurso = False
    End If
    If Not AdoRes Is Nothing Then
        AdoRes.Close
        Set AdoRes = Nothing
    End If


End Function

Private Function IBaseDatos_CambiarCodigo(CodigoNuevo As Variant) As TipoErrorSummit

End Function

Private Sub IBaseDatos_CancelarEdicion()

End Sub

Private Function IBaseDatos_ComprobarExistenciaEnBaseDatos() As Boolean

End Function

Private Function IBaseDatos_EliminarDeBaseDatos() As TipoErrorSummit

End Function

Private Function IBaseDatos_FinalizarEdicionEliminando() As TipoErrorSummit

Dim TESError As TipoErrorSummit
Dim bTrans As Boolean
Dim sConsulta As String
Dim AdoRes As ADODB.Recordset
Dim i As Integer

TESError.NumError = TESnoerror

'******************* Precondicion *******************
If moConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CUnidadOrgNivel2.FinalizarEdicionEliminando", "No se ha establecido la conexion"
End If
'*****************************************************
On Error GoTo Error:
    
    moConexion.ADOCon.Execute "BEGIN TRAN"
    moConexion.ADOCon.Execute "SET XACT_ABORT OFF"
    bTrans = True
    moConexion.ADOCon.Execute "UPDATE APROBADORES SET ORDEN = ORDEN - 1 FROM APROBADORES WHERE BLOQUE_SEGURIDAD= " & mlBloque & " AND ORDEN > " & miOrden
    
    moConexion.ADOCon.Execute "DELETE FROM APROBADORES WHERE ID = " & mlId
    moConexion.ADOCon.Execute "COMMIT TRAN"
    bTrans = False
    Set m_adores = Nothing
    
    IBaseDatos_FinalizarEdicionEliminando = TESError
    
    Exit Function
    
Error:
    
    If moConexion.ADOCon.Errors.Count > 0 Then
        IBaseDatos_FinalizarEdicionEliminando = basErrores.TratarError(moConexion.ADOCon.Errors)
        If bTrans Then
            moConexion.ADOCon.Execute "ROLLBACK TRAN"
            bTrans = False
        End If
        Exit Function
    End If
    
    If m_adores.EditMode = 1 Then
        m_adores.CancelUpdate
    End If

End Function

Private Function IBaseDatos_FinalizarEdicionModificando() As TipoErrorSummit
Dim TESError As TipoErrorSummit
Dim sConsulta As String
Dim bTransaccionEnCurso As Boolean
Dim sFSP As String
Dim i As Integer
Dim sTotalStr As String
Dim lIdACT1 As Long
Dim sCod As String
Dim sDen As String
Dim AdoRes As ADODB.Recordset
Dim sDenAnterior As String

TESError.NumError = TESnoerror

'********* Precondicion *******************
If moConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CAprobadorSolicitud.FinalizarEdicionModificando", "No se ha establecido la conexion"
End If
'******************************************

On Error GoTo Error:

    moConexion.ADOCon.Execute "BEGIN TRAN"
    bTransaccionEnCurso = True
    moConexion.ADOCon.Execute "SET XACT_ABORT OFF"
    
    'Comprobamos si ha habido cambios en otra sesi�n
    Set m_adores = New ADODB.Recordset
    sConsulta = "SELECT FECACT FROM APROBADORES WHERE ID=" & mlId
    m_adores.Open sConsulta, moConexion.ADOCon, adOpenStatic, adLockReadOnly
    
    If m_adores.eof Then
        m_adores.Close
        Set m_adores = Nothing
        TESError.NumError = TESDatoEliminado
        TESError.Arg1 = 150 ''"Aprobador"
        IBaseDatos_FinalizarEdicionModificando = TESError
        moConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
        Exit Function
    End If
    
    If mdtFecAct <> m_adores("FECACT").Value Then
        m_adores.Close
        Set m_adores = Nothing
        TESError.NumError = TESInfActualModificada
        moConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
        IBaseDatos_FinalizarEdicionModificando = TESError
        Exit Function
    End If
    
    ''' Actualizar
    sConsulta = "UPDATE APROBADORES SET BLOQUE_SEGURIDAD = " & mlBloque & ",LIMITE = " & DblToSQLFloat(mdLimite) & ", PER = " & StrToSQLNULL(m_vPersona)
    sConsulta = sConsulta & ", UON1=" & StrToSQLNULL(m_vUON1) & ", UON2=" & StrToSQLNULL(m_vUON2) & ", UON3=" & StrToSQLNULL(m_vUON3) & ", DEP=" & StrToSQLNULL(m_vCodDep)
    sConsulta = sConsulta & " WHERE ID = " & mlId
    moConexion.ADOCon.Execute sConsulta
    If moConexion.ADOCon.Errors.Count > 0 Then
        GoTo Error
    End If
 
    If Not IsNull(m_vPersona) And m_vPersona <> "" Then
        sConsulta = "UPDATE USU SET SOL_COMPRA=1 FROM USU U INNER JOIN PER P ON U.PER = P.COD WHERE P.COD = '" & DblQuote(m_vPersona) & "' AND U.SOL_COMPRA=0"
        moConexion.ADOCon.Execute sConsulta
        If moConexion.ADOCon.Errors.Count > 0 Then
            GoTo Error
        End If
    End If
 
    'Recogemos el nuevo valor de FECACT
    m_adores.Requery
    mdtFecAct = m_adores("FECACT").Value
    m_adores.Close
    Set m_adores = Nothing
    
    moConexion.ADOCon.Execute "COMMIT TRAN"
    bTransaccionEnCurso = False
    
    IBaseDatos_FinalizarEdicionModificando = TESError
    
    Exit Function
    

Error:
    
    IBaseDatos_FinalizarEdicionModificando = basErrores.TratarError(moConexion.ADOCon.Errors)
    
    If Not m_adores Is Nothing Then
        m_adores.Close
        Set m_adores = Nothing
    End If
    
    If bTransaccionEnCurso Then
        moConexion.ADOCon.Execute "ROLLBACK"
        moConexion.ADOCon.Execute "SET XACT_ABORT OFF"
        bTransaccionEnCurso = False
    End If

End Function

Private Function IBaseDatos_IniciarEdicion(Optional ByVal Bloquear As Boolean, Optional ByVal UsuarioBloqueo As String) As TipoErrorSummit
Dim TESError As TipoErrorSummit

TESError.NumError = TESnoerror

'******************* Precondicion *******************
If moConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CWorkflow.IniciarEdicion", "No se ha establecido la conexion"
End If
'*****************************************************

Set m_adores = New ADODB.Recordset
m_adores.Open "SELECT ID,BLOQUE_SEGURIDAD, PER,UON1,UON2,UON3,DEP, ORDEN, LIMITE, FECACT FROM APROBADORES WHERE ID =" & mlId, moConexion.ADOCon, adOpenKeyset, adLockOptimistic

If m_adores.eof Then
    m_adores.Close
    Set m_adores = Nothing
    TESError.NumError = TESDatoEliminado
    TESError.Arg1 = 150
    TESError.Arg2 = 1
    IBaseDatos_IniciarEdicion = TESError
    Exit Function
End If

mdLimite = m_adores.Fields("LIMITE").Value
miOrden = m_adores.Fields("ORDEN").Value
m_vPersona = m_adores.Fields("PER").Value
mlBloque = m_adores.Fields("BLOQUE_SEGURIDAD").Value
mdtFecAct = m_adores.Fields("FECACT").Value
m_vUON1 = m_adores.Fields("UON1").Value
m_vUON2 = m_adores.Fields("UON2").Value
m_vUON3 = m_adores.Fields("UON3").Value
m_vCodDep = m_adores.Fields("DEP").Value

IBaseDatos_IniciarEdicion = TESError


End Function


