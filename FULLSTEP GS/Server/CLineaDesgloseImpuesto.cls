VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CLineaDesgloseImpuesto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Private m_dBaseImponible As Double
Private m_dBaseImponibleMonProce As Double
Private m_stipoImpuesto As String
Private m_dtipovalor As Double

Private m_oConexion As CConexion

Public Property Get Key() As String
    Key = m_stipoImpuesto & " " & m_dtipovalor
End Property

Public Property Get Conexion() As CConexion

    Set Conexion = m_oConexion

End Property

Public Property Set Conexion(oConexion As CConexion)

    Set m_oConexion = oConexion

End Property


Public Function cuota() As Double
    If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    cuota = BaseImponible * Me.TipoValor / 100
    '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CLineaDesgloseImpuesto", "cuota", ERR, Erl)
        Exit Function
    End If
End Function

Public Function cuotaMonProce() As Double
    If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    cuotaMonProce = BaseImponibleMonProce * Me.TipoValor / 100
    '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CLineaDesgloseImpuesto", "cuotaMonProce", ERR, Erl)
        Exit Function
    End If
End Function

Public Property Get BaseImponible() As Double
    BaseImponible = m_dBaseImponible
End Property
Public Property Let BaseImponible(ByVal dBaseImponible As Double)
    m_dBaseImponible = dBaseImponible
End Property

Public Property Get BaseImponibleMonProce() As Double
    BaseImponibleMonProce = m_dBaseImponibleMonProce
End Property
Public Property Let BaseImponibleMonProce(ByVal dBaseImponible As Double)
    m_dBaseImponibleMonProce = dBaseImponible
End Property

Public Property Get TipoImpuesto() As String

    TipoImpuesto = m_stipoImpuesto

End Property

Public Property Let TipoImpuesto(ByVal stipoImpuesto As String)

    m_stipoImpuesto = stipoImpuesto

End Property

Public Property Get TipoValor() As Double

    TipoValor = m_dtipovalor

End Property

Public Property Let TipoValor(ByVal dtipovalor As Double)

    m_dtipovalor = dtipovalor

End Property

Public Function toString() As String
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    toString = Me.TipoImpuesto & " " & Me.TipoValor & " % :" & Me.cuota & vbCrLf & "Base imponible " & Me.TipoImpuesto & " : " & Me.BaseImponible
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CLineaDesgloseImpuesto", "toString", ERR, Erl)
      Exit Function
   End If
End Function
