VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CContactos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'local variable to hold collection
Private mCol As Collection

Public Function Add(ByVal Id As Integer, Optional ByVal Nombre As Variant, Optional ByVal Apellidos As String, Optional ByVal NIF As Variant, Optional ByVal Dep As Variant, Optional ByVal Cargo As Variant _
, Optional ByVal Tfno As Variant, Optional ByVal Fax As Variant, Optional ByVal Mail As Variant, Optional ByVal Def As Boolean, Optional ByVal varIndice As Variant, Optional ByVal Tfno2 As Variant _
, Optional ByVal Port As Variant, Optional ByVal tfnomovil As Variant, Optional ByVal Aprovisionador As Boolean, Optional ByVal NotifSubasta As Boolean, Optional ByVal EstadoIntegracion As Variant _
, Optional ByVal iTipoEmail As Integer, Optional ByVal sIdioma As Variant, Optional ByVal bCalidad As Boolean, Optional ByVal bPrincipal As Boolean) As CContacto
    'create a new object
    Dim objnewmember As CContacto
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New CContacto

    objnewmember.Id = Id
    
    If IsMissing(Nombre) Then
        objnewmember.Nombre = Null
    Else
        objnewmember.Nombre = Nombre
    End If
    
    objnewmember.Apellidos = Apellidos
    
    If IsMissing(NIF) Then
        objnewmember.NIF = Null
    Else
        objnewmember.NIF = NIF
    End If
    
    If IsMissing(Dep) Then
        objnewmember.Departamento = Null
    Else
        objnewmember.Departamento = Dep
    End If
    
    If IsMissing(Cargo) Then
        objnewmember.Cargo = Null
    Else
        objnewmember.Cargo = Cargo
    End If
    
    If IsMissing(Tfno) Then
        objnewmember.Tfno = Null
    Else
        objnewmember.Tfno = Tfno
    End If
    
    If IsMissing(Tfno2) Then
        objnewmember.Tfno2 = Null
    Else
        objnewmember.Tfno2 = Tfno2
    End If
    
    If IsMissing(tfnomovil) Then
        objnewmember.tfnomovil = Null
    Else
        objnewmember.tfnomovil = tfnomovil
    End If
    
    If IsMissing(Fax) Then
        objnewmember.Fax = Null
    Else
        objnewmember.Fax = Fax
    End If
    
    If IsMissing(Mail) Then
        objnewmember.Mail = Null
    Else
        objnewmember.Mail = Mail
    End If
    
    If IsMissing(Aprovisionador) Then
        objnewmember.Aprovisionador = Null
    Else
        objnewmember.Aprovisionador = Aprovisionador
    End If
    
    If IsMissing(NotifSubasta) Then
        objnewmember.NotifSubasta = Null
    Else
        objnewmember.NotifSubasta = NotifSubasta
    End If
    
    If IsMissing(Port) Then
        objnewmember.Port = False
    Else
        objnewmember.Port = SQLBinaryToBoolean(Port)
    End If
    
    objnewmember.Def = Def
    objnewmember.EstadoIntegracion = EstadoIntegracion
    If IsNull(iTipoEmail) Or IsMissing(iTipoEmail) Then
        iTipoEmail = 0
    End If
    objnewmember.TipoEmail = iTipoEmail
    
    If IsMissing(sIdioma) Then
        objnewmember.Idioma = Null
    Else
        objnewmember.Idioma = sIdioma
    End If
        
    If IsMissing(bCalidad) Then
        objnewmember.Calidad = False
    Else
        objnewmember.Calidad = bCalidad
    End If
    
    If IsMissing(bPrincipal) Then
        objnewmember.Principal = False
    Else
        objnewmember.Principal = bPrincipal
    End If
    
    If Not IsMissing(varIndice) And Not IsNull(varIndice) Then
        objnewmember.Indice = varIndice
        mCol.Add objnewmember, CStr(varIndice)
    Else
        mCol.Add objnewmember, CStr(Id)
    End If
    
        
    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CContactos", "Add", ERR, Erl)
      Exit Function
   End If

End Function

Public Property Get Item(vntIndexKey As Variant) As CContacto
On Error GoTo vacio:
 
    Set Item = mCol(vntIndexKey)
    Exit Property

vacio:
  Set Item = Nothing

End Property



Public Property Get Count() As Long
    
    If mCol Is Nothing Then
        Count = 0
    Else
        Count = mCol.Count
    End If
    
End Property


Public Sub Remove(vntIndexKey As Variant)
    

    mCol.Remove vntIndexKey
End Sub


Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
       Set NewEnum = mCol.[_NewEnum]
End Property

Private Sub Class_Initialize()

   

    Set mCol = New Collection
End Sub


Private Sub Class_Terminate()
      Set mCol = Nothing
End Sub


Public Function BorrarEnModoIndice(ByVal iIndice As Integer)
Dim IndFor As Integer
Dim oCon As CContacto

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    For IndFor = iIndice To mCol.Count - 2
            
        mCol.Remove (CStr(IndFor))
        Set oCon = mCol.Item(CStr(IndFor + 1))
        mCol.Add oCon, CStr(IndFor)
        Set oCon = Nothing
        
    Next IndFor
    
    mCol.Remove CStr(IndFor)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CContactos", "BorrarEnModoIndice", ERR, Erl)
      Exit Function
   End If
    
End Function

