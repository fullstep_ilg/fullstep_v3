Attribute VB_Name = "basParametros"
' Tamanyo en bytes del buffer de lectura de datos binarios
Public Const ChunkSize As Integer = 16384
Public Const LargeChunkSize As Long = 1048576
    
' PARAMETROS GENERALES

Public gParametrosGenerales As ParametrosGenerales
Public gLongitudesDeCodigos As LongitudesDeCodigos
Public gParametrosInstalacion As ParametrosInstalacion
'PARAMETROS INTEGRACION
Public gParametrosIntegracion As ParametrosIntegracion

'PARAMETROS SM
Public g_oParametrosSM As CParametrosSM

' REGIONAL SETTINGS
Public gsSETTINGSDecimalSymbol As String
Public gsSETTINGSThousandSymbol As String

Public ServidorGS As String
Public BaseDatosGS As String

' CONSTANTES PARA EL LOG DE CAMBIOS E INTEGRACION
Public Const Accion_Alta            As String = "I"
Public Const Accion_Baja            As String = "D"
Public Const Accion_Modificacion    As String = "U"
Public Const Accion_CambioCodigo    As String = "C"
Public Const Accion_Reenvio         As String = "R"
Public Const Accion_CambioEstado    As String = "E"
Public Const Accion_Homologacion    As String = "H" 'Esta acci�n es exclusiva para PROVEEDORES, en la adjudicacion de procesos
Public Const Accion_CambioNivel4    As String = "4" 'Esta acci�n es exclusiva para ARTICULOS, implica una reubicaci�n del art�culo
Public Const Accion_Reapertura      As String = "A" 'Reapertura de procesos

'Variable para la administraci�n p�blica.Coger� su valor del cliente
Public ADMIN_OBLIGATORIA  As Boolean

'para saber quien es el usuario y guardarlo con el error
Public g_sCodUsuConectado As String






