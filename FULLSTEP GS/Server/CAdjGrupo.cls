VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CAdjGrupo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CAdjGrupo **********************************
'*             Autor : Mertxe Mart�n
'*             Creada : 06/03/2002
'***************************************************************

'@@@@
Option Explicit

Implements IBaseDatos

Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum

Private m_dblAbierto As Double  '?? Donde see usa?
Private m_dblAbiertoMonProce As Double
Private m_dblConsumido As Double        'Consumido de un prove para un grupo
Private m_dblConsumidoMonProce As Double    'Consumido de un prove para un grupo en moneda de proceso
Private m_dblAdjudicadoSin As Double    'Adjudicado del prove - grupo sin aplicar los atributos de grupo
Private m_dblAdjudicadoSinMonProce As Double  'Adjudicado del prove - grupo sin aplicar los atributos de grupo (en moneda proceso)
Private m_dblAdjudicado As Double       'Este lleva los atributos de total grupo aplicados
Private m_dblAdjudicadoMonProce As Double   'Adjudicado del prove - grupo con los atributos de total grupo aplicados en moneda de proceso
Private m_dblAhorrado As Double         'Ahorrado del prove-grupo
Private m_dblAhorradoMonProce As Double     'Ahorrado del prove-grupo en moneda de proceso
Private m_dblAhorradoPorcen As Double

Private m_dblConsumidoReal As Double
Private m_dblConsumidoRealMonProce As Double
Private m_dblAdjudicadoReal As Double   'Este lleva los atributos de total grupo aplicados, es el total total
Private m_dblAdjudicadoRealMonProce As Double
Private m_dblAdjudicadoSinReal As Double
Private m_dblAdjudicadoSinRealMonProce As Double

Private m_dblAbiertoOfe As Double 'Abierto si fuera al 100% al proveedor (s�lo los que tienen precios)
Private m_dblAbiertoOfeMonProce As Double
Private m_dblAdjudicadoOfe As Double 'Adjudicado si fuera al 100% al proveedor pero sin aplicar los atributos de grupo
Private m_dblAdjudicadoOfeMonProce As Double 'Adjudicado si fuera al 100% al proveedor pero sin aplicar los atributos de grupo (en moneda de proceso)
Private m_dblImporte As Double 'Este lleva los atributos de total grupo aplicados
Private m_dblAhorroOfe As Double
Private m_dblAhorroOfePorcen As Double
Private m_dblAdjudicadoItems As Double 'Suma del adjudicado de los items sin atributos de total de item aplicados
Private m_dblAdjudicadoItemsMonProce As Double    'Suma del adjudicado de los items sin atributos de total de item aplicados (en moneda de proceso)

Private m_dblImporteMonProce As Double      'Importe adjudicado al grupo con los atributos de total grupo aplicados en moneda de proceso
Private m_dblAhorroOfeMonProce As Double    'Ahorrado en moneda de proceso
                                     
Private m_bPrecios As Boolean

Private m_sEstado As String
Private m_oProceso As cProceso
Private m_oGrupo As String  'As CGrupo
Private m_lGrupoID As Long
Private m_dblCambio As Double

'NOTA:ser�n los atributos a nivel de oferta,no de proceso.
Private m_oAtributos As CAtributos
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Private m_sProve As String
Private m_iNumOfe As Integer

Private m_oConexion As CConexion
Private m_vIndice As Variant
Private m_adores As adodb.Recordset

Public Property Let Estado(ByVal dato As String)
    m_sEstado = dato
End Property

Public Property Get Estado() As String
    Estado = m_sEstado
End Property

Public Property Set Proceso(ByVal dato As cProceso)
    Set m_oProceso = dato
End Property

Public Property Get Proceso() As cProceso
    Set Proceso = m_oProceso
End Property

Public Property Let Grupo(ByVal dato As String)
    m_oGrupo = dato   'As CGrupo
End Property

Public Property Get Grupo() As String
    Grupo = m_oGrupo   'As CGrupo
End Property

Public Property Let GrupoID(ByVal dato As String)
    m_lGrupoID = dato
End Property

Public Property Get GrupoID() As String
    GrupoID = m_lGrupoID
End Property


Public Property Set Atributos(ByVal dato As CAtributos)
    Set m_oAtributos = dato
End Property

Public Property Get Atributos() As CAtributos
    Set Atributos = m_oAtributos
End Property

Public Property Let Prove(ByVal dato As String)
    m_sProve = dato
End Property

Public Property Get Prove() As String
    Prove = m_sProve
End Property

Public Property Let NumOfe(ByVal dato As Integer)
    m_iNumOfe = dato
End Property

Public Property Get NumOfe() As Integer
    NumOfe = m_iNumOfe
End Property
Public Property Let Cambio(ByVal dato As Double)
    m_dblCambio = dato
End Property

Public Property Get Cambio() As Double
    Cambio = m_dblCambio
End Property

Public Property Let Abierto(ByVal dato As Double)
    m_dblAbierto = dato
End Property
Public Property Get Abierto() As Double
    Abierto = m_dblAbierto
End Property

Public Property Let AbiertoMonProce(ByVal dato As Double)
    m_dblAbiertoMonProce = dato
End Property
Public Property Get AbiertoMonProce() As Double
    AbiertoMonProce = m_dblAbiertoMonProce
End Property

Public Property Let Consumido(ByVal dato As Double)
    m_dblConsumido = dato
End Property
Public Property Get Consumido() As Double
    Consumido = m_dblConsumido
End Property

Public Property Let ConsumidoMonProce(ByVal dato As Double)
    m_dblConsumidoMonProce = dato
End Property
Public Property Get ConsumidoMonProce() As Double
    ConsumidoMonProce = m_dblConsumidoMonProce
End Property

Public Property Let ConsumidoReal(ByVal dato As Double)
    m_dblConsumidoReal = dato
End Property
Public Property Get ConsumidoReal() As Double
    ConsumidoReal = m_dblConsumidoReal
End Property

Public Property Let ConsumidoRealMonProce(ByVal dato As Double)
    m_dblConsumidoRealMonProce = dato
End Property
Public Property Get ConsumidoRealMonProce() As Double
    ConsumidoRealMonProce = m_dblConsumidoRealMonProce
End Property

Public Property Let Adjudicado(ByVal dato As Double)
    m_dblAdjudicado = dato
End Property
Public Property Get Adjudicado() As Double
    Adjudicado = m_dblAdjudicado
End Property

Public Property Let AdjudicadoMonProce(ByVal dato As Double)
    m_dblAdjudicadoMonProce = dato
End Property
Public Property Get AdjudicadoMonProce() As Double
    AdjudicadoMonProce = m_dblAdjudicadoMonProce
End Property

Public Property Let AdjudicadoReal(ByVal dato As Double)
    m_dblAdjudicadoReal = dato
End Property
Public Property Get AdjudicadoReal() As Double
    AdjudicadoReal = m_dblAdjudicadoReal
End Property

Public Property Let AdjudicadoRealMonProce(ByVal dato As Double)
    m_dblAdjudicadoRealMonProce = dato
End Property
Public Property Get AdjudicadoRealMonProce() As Double
    AdjudicadoRealMonProce = m_dblAdjudicadoRealMonProce
End Property

Public Property Let Ahorrado(ByVal dato As Double)
    m_dblAhorrado = dato
End Property
Public Property Get Ahorrado() As Double
    Ahorrado = m_dblAhorrado
End Property

Public Property Let AhorradoMonProce(ByVal dato As Double)
    m_dblAhorradoMonProce = dato
End Property
Public Property Get AhorradoMonProce() As Double
    AhorradoMonProce = m_dblAhorradoMonProce
End Property

Public Property Let AhorradoPorcen(ByVal dato As Double)
    m_dblAhorradoPorcen = dato
End Property

Public Property Get AhorradoPorcen() As Double
    AhorradoPorcen = m_dblAhorradoPorcen
End Property

Public Property Let AdjudicadoSin(ByVal dato As Double)
    m_dblAdjudicadoSin = dato
End Property
Public Property Get AdjudicadoSin() As Double
    AdjudicadoSin = m_dblAdjudicadoSin
End Property

Public Property Let AdjudicadoSinMonProce(ByVal dato As Double)
    m_dblAdjudicadoSinMonProce = dato
End Property
Public Property Get AdjudicadoSinMonProce() As Double
    AdjudicadoSinMonProce = m_dblAdjudicadoSinMonProce
End Property

Public Property Let AdjudicadoSinReal(ByVal dato As Double)
    m_dblAdjudicadoSinReal = dato
End Property
Public Property Get AdjudicadoSinReal() As Double
    AdjudicadoSinReal = m_dblAdjudicadoSinReal
End Property

Public Property Let AdjudicadoSinRealMonProce(ByVal dato As Double)
    m_dblAdjudicadoSinRealMonProce = dato
End Property
Public Property Get AdjudicadoSinRealMonProce() As Double
    AdjudicadoSinRealMonProce = m_dblAdjudicadoSinRealMonProce
End Property

Public Property Let Importe(ByVal dato As Double)
    m_dblImporte = dato
End Property
Public Property Get Importe() As Double
    Importe = m_dblImporte
End Property

Public Property Let ImporteMonProce(ByVal dato As Double)
    m_dblImporteMonProce = dato
End Property
Public Property Get ImporteMonProce() As Double
    ImporteMonProce = m_dblImporteMonProce
End Property

Public Property Let AhorroOfe(ByVal dato As Double)
    m_dblAhorroOfe = dato
End Property
Public Property Get AhorroOfe() As Double
    AhorroOfe = m_dblAhorroOfe
End Property

Public Property Let AhorroOfeMonProce(ByVal dato As Double)
    m_dblAhorroOfeMonProce = dato
End Property
Public Property Get AhorroOfeMonProce() As Double
    AhorroOfeMonProce = m_dblAhorroOfeMonProce
End Property

Public Property Let AhorroOfePorcen(ByVal dato As Double)
    m_dblAhorroOfePorcen = dato
End Property

Public Property Get AhorroOfePorcen() As Double
    AhorroOfePorcen = m_dblAhorroOfePorcen
End Property

Public Property Let AdjudicadoOfe(ByVal dato As Double)
    m_dblAdjudicadoOfe = dato
End Property
Public Property Get AdjudicadoOfe() As Double
    AdjudicadoOfe = m_dblAdjudicadoOfe
End Property

Public Property Let AdjudicadoOfeMonProce(ByVal dato As Double)
    m_dblAdjudicadoOfeMonProce = dato
End Property
Public Property Get AdjudicadoOfeMonProce() As Double
    AdjudicadoOfeMonProce = m_dblAdjudicadoOfeMonProce
End Property

Public Property Let AdjudicadoItems(ByVal dato As Double)
    m_dblAdjudicadoItems = dato
End Property
Public Property Get AdjudicadoItems() As Double
    AdjudicadoItems = m_dblAdjudicadoItems
End Property

Public Property Let AdjudicadoItemsMonProce(ByVal dato As Double)
    m_dblAdjudicadoItemsMonProce = dato
End Property
Public Property Get AdjudicadoItemsMonProce() As Double
    AdjudicadoItemsMonProce = m_dblAdjudicadoItemsMonProce
End Property

Friend Property Set Conexion(ByVal con As CConexion)
    Set m_oConexion = con
End Property

Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property

Public Property Let Indice(ByVal vIndice As Variant)
    m_vIndice = vIndice
End Property
Public Property Get Indice() As Variant
    Indice = m_vIndice
End Property

Public Property Let AbiertoOfe(ByVal dato As Double)
    m_dblAbiertoOfe = dato
End Property
Public Property Get AbiertoOfe() As Double
    AbiertoOfe = m_dblAbiertoOfe
End Property

Public Property Let AbiertoOfeMonProce(ByVal dato As Double)
    m_dblAbiertoOfeMonProce = dato
End Property
Public Property Get AbiertoOfeMonProce() As Double
    AbiertoOfeMonProce = m_dblAbiertoOfeMonProce
End Property

Public Property Let HayPrecios(ByVal dato As Boolean)
    m_bPrecios = dato
End Property

Public Property Get HayPrecios() As Boolean
    HayPrecios = m_bPrecios
End Property

Private Function IBaseDatos_AnyadirABaseDatos() As TipoErrorSummit
Dim sConsulta As String
Dim TESError As TipoErrorSummit

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:

    TESError.NumError = TESnoerror
    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"

    sConsulta = "INSERT INTO GRUPO_OFE (ANYO,GMN1,PROCE,GRUPO,PROVE,OFE,CONSUMIDO,ADJUDICADO,"
    sConsulta = sConsulta & "AHORRADO,AHORRADO_PORCEN,IMPORTE,AHORRO_OFE,AHORRO_OFE_PORCEN,ABIERTO)"
    sConsulta = sConsulta & " VALUES (" & m_oProceso.Anyo & ",'" & DblQuote(m_oProceso.GMN1Cod) & "'," & m_oProceso.Cod
    sConsulta = sConsulta & "," & (m_lGrupoID) & ",'" & DblQuote(m_sProve) & "'," & m_iNumOfe
    sConsulta = sConsulta & "," & m_dblConsumido & "," & m_dblAdjudicado & "," & m_dblAhorrado & "," & m_dblAhorradoPorcen
    sConsulta = sConsulta & "," & m_dblImporte & "," & m_dblAhorroOfe & "," & m_dblAhorroOfePorcen & "," & m_dblAbierto & ")"
   
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    IBaseDatos_AnyadirABaseDatos = TESError
    Exit Function

Error_Cls:

    IBaseDatos_AnyadirABaseDatos = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
    On Error Resume Next
    If Not m_adores Is Nothing Then
        m_adores.Close
        Set m_adores = Nothing
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      'Call g_oErrores.TratarError("Class Module", "CAdjGrupo", "IBaseDatos_AnyadirABaseDatos", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If

End Function


Private Function IBaseDatos_CambiarCodigo(CodigoNuevo As Variant) As TipoErrorSummit

End Function

Private Sub IBaseDatos_CancelarEdicion()

End Sub


Private Function IBaseDatos_ComprobarExistenciaEnBaseDatos() As Boolean

End Function


Private Function IBaseDatos_EliminarDeBaseDatos() As TipoErrorSummit
Dim sConsulta As String
Dim TESError As TipoErrorSummit

    '''Precondicion
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CAdjGrupo.EliminarDeBaseDatos", "No se ha establecido la conexion"
    End If

If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls

    TESError.NumError = TESnoerror

    sConsulta = "DELETE FROM GRUPO_OFE WHERE ANYO=" & m_oProceso.Anyo & " AND GMN1='" & DblQuote(m_oProceso.GMN1Cod) & "'"
    sConsulta = sConsulta & " AND PROCE=" & m_oProceso.Cod & " AND GRUPO='" & DblQuote(m_oGrupo) & "'"
    sConsulta = sConsulta & " AND PROVE='" & DblQuote(m_sProve) & "' AND OFE=" & m_iNumOfe
    
    m_oConexion.ADOCon.Execute sConsulta

    IBaseDatos_EliminarDeBaseDatos = TESError

    Exit Function

Error_Cls:
    IBaseDatos_EliminarDeBaseDatos = basErrores.TratarError(m_oConexion.ADOCon.Errors)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      'Call g_oErrores.TratarError("Class Module", "CAdjGrupo", "IBaseDatos_EliminarDeBaseDatos", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If

End Function


Private Function IBaseDatos_FinalizarEdicionEliminando() As TipoErrorSummit

End Function


Private Function IBaseDatos_FinalizarEdicionModificando() As TipoErrorSummit
Dim TESError As TipoErrorSummit
Dim sConsulta As String

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls
    TESError.NumError = TESnoerror

    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"

    sConsulta = "UPDATE GRUPO_OFE SET "
    sConsulta = sConsulta & " CONSUMIDO=" & m_dblConsumido & ",ADJUDICADO=" & m_dblAdjudicado
    sConsulta = sConsulta & ",AHORRADO=" & m_dblAhorrado & ",IMPORTE=" & m_dblImporte & ",IMPORTE=" & m_dblImporteMonProce
    sConsulta = sConsulta & ",AHORRO_OFE=" & m_dblAhorroOfe & ",ABIERTO=" & m_dblAbierto
    sConsulta = sConsulta & ",AHORRADO_PORCEN=" & m_dblAhorradoPorcen & ",AHORRO_OFE_PORCEN=" & m_dblAhorroOfePorcen
    sConsulta = sConsulta & " WHERE ANYO=" & m_oProceso.Anyo & " AND GMN1='" & DblQuote(m_oProceso.GMN1Cod) & "'"
    sConsulta = sConsulta & " AND PROCE=" & m_oProceso.Cod & " AND GRUPO='" & DblQuote(m_oGrupo) & "'"
    sConsulta = sConsulta & " AND PROVE='" & DblQuote(m_sProve) & "' AND OFE=" & m_iNumOfe

    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"

    IBaseDatos_FinalizarEdicionModificando = TESError

    Exit Function

Error_Cls:
    IBaseDatos_FinalizarEdicionModificando = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"

    On Error Resume Next
    If Not m_adores Is Nothing Then
        m_adores.Close
        Set m_adores = Nothing
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      'Call g_oErrores.TratarError("Class Module", "CAdjGrupo", "IBaseDatos_FinalizarEdicionModificando", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If

End Function

Private Function IBaseDatos_IniciarEdicion(Optional ByVal Bloquear As Boolean, Optional ByVal UsuarioBloqueo As String) As TipoErrorSummit

End Function

Private Sub Class_Terminate()
On Error GoTo Error_Cls

    Set m_oConexion = Nothing
    Exit Sub
Error_Cls:
    Resume Next
End Sub

''' <summary>Carga los atributos</summary>
''' <param name="UsarIndice">Indica si hay que usar �ndice</param>
''' <remarks>Llamada desde: FSGClient Tiempo m�ximo: 1 sec </remarks>
''' <revision>LTG 22/12/2011</revision>

Public Sub CargarAtributos(Optional ByVal UsarIndice As Boolean)
    Dim AdoRes As New adodb.Recordset
    Dim sConsulta As String
    Dim lIndice As Long
    
    'Carga los atributos a nivel de proceso y a nivel de grupo,porque en la 1� pantalla de la comparativa
    'se muestran ambos.
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sConsulta = "SELECT ANYO,GMN1,PROCE,PROVE,OFE,ATRIB_ID,VALOR_NUM,VALOR_TEXT,VALOR_FEC,VALOR_BOOL,VALOR_POND,FECACT,IMPPARCIAL"
    sConsulta = sConsulta & " FROM OFE_ATRIB WITH (NOLOCK)"
    sConsulta = sConsulta & " INNER JOIN PROCE_ATRIB WITH (NOLOCK) ON OFE_ATRIB.ATRIB_ID=PROCE_ATRIB.ID"
    sConsulta = sConsulta & " WHERE OFE_ATRIB.ANYO=" & m_oProceso.Anyo & " AND OFE_ATRIB.GMN1='" & DblQuote(m_oProceso.GMN1Cod) & "'"
    sConsulta = sConsulta & " AND OFE_ATRIB.PROCE=" & m_oProceso.Cod & " AND OFE_ATRIB.OFE=" & m_iNumOfe
    sConsulta = sConsulta & " AND PROVE='" & DblQuote(m_sProve) & "'"
    sConsulta = sConsulta & " UNION"
    sConsulta = sConsulta & " SELECT ANYO,GMN1,PROCE,GRUPO,PROVE,OFE,ATRIB_ID,VALOR_NUM,VALOR_TEXT,VALOR_FEC,VALOR_BOOL,VALOR_POND,FECACT"
    sConsulta = sConsulta & " FROM OFE_GR_ATRIB WITH (NOLOCK)"
    sConsulta = sConsulta & " INNER JOIN PROCE_ATRIB WITH (NOLOCK) ON OFE_GR_ATRIB.ATRIB_ID=PROCE_ATRIB.ID"
    sConsulta = sConsulta & " WHERE OFE_GR_ATRIB.ANYO=" & m_oProceso.Anyo & " AND OFE_GR_ATRIB.GMN1='" & DblQuote(m_oProceso.GMN1Cod) & "'"
    sConsulta = sConsulta & " AND OFE_GR_ATRIB.PROCE=" & m_oProceso.Cod & " AND OFE_GR_ATRIB.OFE=" & m_iNumOfe
    sConsulta = sConsulta & " AND PROVE='" & DblQuote(m_sProve) & "'"
     
    AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
        
    Set m_oAtributos = Nothing
    Set m_oAtributos = New Collection
        
    If Not AdoRes.eof Then
        If UsarIndice Then
            lIndice = 0
            
            While Not AdoRes.eof
                'Dejamos en manos del programador cuando asignar la conexion al objeto usuario
                'm_oAtributos.Add oProce, AdoRes.Fields("GRUPO").Value, fldProve.Value, fldNumOfe.Value, NullToDbl0(fldAdjudicado.Value), NullToDbl0(fldAhorrado.Value), NullToDbl0(fldAhorroOfe.Value), NullToDbl0(fldConsumido.Value), NullToDbl0(fldImporte.Value), NullToDbl0(fldAbierto.Value), NullToDbl0(fldAhorradoPorcen.Value), NullToDbl0(fldAhorroOfePorcen.Value), lIndice
                AdoRes.MoveNext
                lIndice = lIndice + 1
            Wend
        Else
            While Not AdoRes.eof
                'Dejamos en manos del programador cuando asignar la conexion al objeto usuario
                'Me.Add oProce, AdoRes.Fields("GRUPO").Value, fldProve.Value, fldNumOfe.Value, NullToDbl0(fldAdjudicado.Value), NullToDbl0(fldAhorrado.Value), NullToDbl0(fldAhorroOfe.Value), NullToDbl0(fldConsumido.Value), NullToDbl0(fldImporte.Value), NullToDbl0(fldAbierto.Value), NullToDbl0(fldAhorradoPorcen.Value), NullToDbl0(fldAhorroOfePorcen.Value)
                AdoRes.MoveNext
            Wend
        End If
    End If
    
    AdoRes.Close
    Set AdoRes = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CAdjGrupo", "CargarAtributos", ERR, Erl)
      Exit Sub
   End If
End Sub
