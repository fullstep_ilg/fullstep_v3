VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "COfertaGrupos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* COfertaGrupos *****************************
'*             Autor : Hilario Barrenkua
'*             Creada : 15/05/2002
'****************************************************************


Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum
Private mCol As Collection
Private m_oConexion  As CConexion

Friend Property Set Conexion(ByVal con As CConexion)
Set m_oConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = m_oConexion
End Property

Public Function Add(ByRef oOfe As COferta, ByRef oGrupo As CGrupo, Optional ByVal vObsAdjun As Variant, _
                    Optional ByVal vIndice As Variant, Optional ByVal lOfertados As Long, _
                    Optional ByVal lAdjuntos As Long, Optional ByVal lPrecios As Long) As COfertaGrupo



    'create a new object
    Dim objnewmember As COfertaGrupo
    
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New COfertaGrupo
   
    With objnewmember
        Set .Oferta = oOfe
        Set .Grupo = oGrupo
        Set .Conexion = oOfe.Conexion
        If Not IsMissing(vObsAdjun) Then
            .ObsAdjun = vObsAdjun
        Else
            .ObsAdjun = Null
        End If
        .NumAtribOfer = lOfertados
        .NumAdjuntos = lAdjuntos
        .NumPrecios = lPrecios
        
        If IsMissing(vIndice) Then
            '.Indice = "#" & oGrupo.ID
            .Indice = oGrupo.Codigo
        Else
            .Indice = vIndice
        End If
        
        Set .Conexion = m_oConexion
        
        mCol.Add objnewmember, CStr(.Indice)
    
    End With
    
    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing
        
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "COfertaGrupos", "Add", ERR, Erl)
      Exit Function
   End If
    
End Function

Public Property Get Item(vntIndexKey As Variant) As COfertaGrupo
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
    'Set Item = mCol("#" & vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
     Count = mCol.Count
End If


End Property


Public Sub Remove(vntIndexKey As Variant)

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    mCol.Remove vntIndexKey
    Exit Sub


'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "COfertaGrupos", "Remove", ERR, Erl)
      Exit Sub
   End If
    
End Sub


Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
     Set NewEnum = mCol.[_NewEnum]
End Property


Private Sub Class_Initialize()
    Set mCol = New Collection
End Sub


Private Sub Class_Terminate()
    'destroys collection when this class is terminated
    Set mCol = Nothing
End Sub


