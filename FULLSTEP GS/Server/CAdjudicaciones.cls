VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CAdjudicaciones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CAdjudicaciones **********************************
'*             Autor : Javier Arana
'*             Creada : 4/5/99
'****************************************************************


Option Explicit


Private mCol As Collection
Private mDic As Dictionary
Private m_oConexion As CConexion

Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum

Friend Property Set Conexion(ByVal con As CConexion)
    Set m_oConexion = con
End Property

Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property

Public Property Get Item(vntIndexKey As Variant) As CAdjudicacion
On Error GoTo NoSeEncuentra:

    If TypeName(vntIndexKey) = "String" Then
        If mDic.Exists(vntIndexKey) Then
            Set Item = mCol(vntIndexKey)
        Else
            Set Item = Nothing
        End If
    Else
        Set Item = mCol(vntIndexKey)
    End If
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property


Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
     Count = mCol.Count
End If


End Property

''' <summary>A�ade un adjudicaci�n a la colecci�n de adjudicaciones</summary>
''' <param name="ProveCod">C�digo de proveedor</param>
''' <param name="NumOfe">Oferta</param>
''' <param name="ItemId">Id. de item</param>
''' <param name="Porcentaje">porcentaje</param>
''' <param name="Indice">Indice si se quiere a�adir con �ndice</param>
''' <param name="PresUnitario">presupuesto unitario</param>
''' <param name="Objetivo">objetivo</param>
''' <param name="ObjetivoNuevo">objetivo nuevo</param>
''' <param name="Grupo">grupo</param>
''' <param name="vCat">cat�logo</param>
''' <param name="vPed">pedido</param>
''' <param name="vAdjudicado">importe adjudicado</param>
''' <param name="vEscalado">escalado si se utilizan escalados</param>
''' <param name="vCantidadEsc">cantidad adjudicado si se utilizan escalados</param>
''' <param name="vPrecioEsc">precio adjudicado si se utilizan escalados</param>
''' <param name="vAdjudicadoEsc">importe adjudicado si se utilizan escalados</param>
''' <param name="vCambio">Cambio utilizado en la oferta de la adjudicaci�n</param>
''' <remarks>Llamada desde: CProceso.CargarAdjudicacionesGrupos</remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>
''' <remarks>Revisado por: LTG  Fecha: 06/09/2011</remarks>

Public Function Add(ByVal ProveCod As String, ByVal NumOfe As Integer, ByVal ItemId As Integer, Optional ByVal Precio As Variant, _
    Optional ByVal Porcentaje As Double, Optional ByVal Indice As Variant, Optional ByVal PresUnitario As Variant, _
    Optional ByVal Objetivo As Variant, Optional ByVal ObjetivoNuevo As Boolean, Optional ByVal Grupo As Variant, _
    Optional ByVal vCat As Variant, Optional ByVal vPed As Variant, Optional ByVal vAdjudicado As Variant, _
    Optional ByVal vEscalado As Variant, Optional ByVal vCantidadEsc As Variant, Optional ByVal vPrecioEsc As Variant, _
    Optional ByVal vAdjudicadoEsc As Variant, Optional ByVal vCambio As Variant, Optional ByVal Partida As Variant, Optional ByVal AnyoPartida As Variant) As CAdjudicacion

    'create a new object
    Dim sCod As String
    Dim objnewmember As CAdjudicacion
    
       
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New CAdjudicacion
   
    objnewmember.Id = ItemId
    objnewmember.Porcentaje = Porcentaje
    If IsMissing(Precio) Then
        objnewmember.Precio = Null
    Else
        objnewmember.Precio = Precio
    End If
    objnewmember.ProveCod = ProveCod
    objnewmember.NumOfe = NumOfe
    objnewmember.ObjetivoNuevo = ObjetivoNuevo
        
    If IsMissing(PresUnitario) Then
        objnewmember.PresUnitario = Null
    Else
        objnewmember.PresUnitario = PresUnitario
    End If
    
    If IsMissing(Objetivo) Then
        objnewmember.Objetivo = Null
    Else
        objnewmember.Objetivo = Objetivo
    End If
    
    If IsMissing(Grupo) Then
        objnewmember.Grupo = Null
    Else
        objnewmember.Grupo = Grupo
    End If
    
    If IsMissing(vCat) Then
        objnewmember.Catalogo = Null
    Else
        objnewmember.Catalogo = vCat
    End If
    
    If IsMissing(vPed) Then
        objnewmember.Pedido = Null
    Else
        objnewmember.Pedido = vPed
    End If
    
    If IsMissing(vAdjudicado) Then
        objnewmember.Adjudicado = Null
    Else
        objnewmember.Adjudicado = vAdjudicado
    End If
    
    If IsMissing(vEscalado) Then
        objnewmember.Escalado = Null
    Else
        objnewmember.Escalado = vEscalado
    End If
    
    If IsMissing(vCantidadEsc) Then
        objnewmember.CantidadEsc = Null
    Else
        objnewmember.CantidadEsc = vCantidadEsc
    End If
    
    If IsMissing(vPrecioEsc) Then
        objnewmember.PrecioEsc = Null
    Else
        objnewmember.PrecioEsc = vPrecioEsc
    End If
        
    If IsMissing(vAdjudicadoEsc) Then
        objnewmember.AdjudicadoEsc = Null
    Else
        objnewmember.AdjudicadoEsc = vAdjudicadoEsc
    End If
    
    If IsMissing(vCambio) Then
        objnewmember.Cambio = 1
    Else
        objnewmember.Cambio = vCambio
    End If
    
    If IsMissing(Partida) Then
        objnewmember.Partida = Null
    Else
        objnewmember.Partida = Partida
    End If
    
    If IsMissing(AnyoPartida) Then
        objnewmember.AnyoPartida = Null
    ElseIf (AnyoPartida = 0 Or AnyoPartida = "") Then
        objnewmember.AnyoPartida = Null
    Else
        objnewmember.AnyoPartida = AnyoPartida
    End If
    
    If Not IsMissing(Indice) And Not IsNull(Indice) Then
        objnewmember.Indice = Indice
        mCol.Add objnewmember, CStr(Indice)
        mDic.Add CStr(Indice), CStr(Indice)
    ElseIf IsNumeric(vEscalado) Then
       sCod = ProveCod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(ProveCod))
       mCol.Add objnewmember, CStr(ItemId) & sCod & CStr(vEscalado)
       mDic.Add CStr(ItemId) & sCod & CStr(vEscalado), CStr(ItemId) & sCod & CStr(vEscalado)
    Else
       sCod = ProveCod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(ProveCod))
       mCol.Add objnewmember, CStr(ItemId) & sCod
       mDic.Add CStr(ItemId) & sCod, CStr(ItemId) & sCod
    End If
    
    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CAdjudicaciones", "Add", ERR, Erl)
      Exit Function
   End If
End Function

Public Sub Remove(vntIndexKey As Variant)
Dim sCod As String


If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If TypeName(vntIndexKey) = "String" Then
        mDic.Remove vntIndexKey
    Else
        If Not IsEmpty(mCol.Item(vntIndexKey).Indice) And Not IsNull(mCol.Item(vntIndexKey).Indice) Then
            mDic.Remove CStr(mCol.Item(vntIndexKey).Indice)
        Else
            sCod = mCol.Item(vntIndexKey).ProveCod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(mCol.Item(vntIndexKey).ProveCod))
            mDic.Remove CStr(mCol.Item(vntIndexKey).Id) & sCod
        End If
    End If
    mCol.Remove vntIndexKey
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CAdjudicaciones", "Remove", ERR, Erl)
      Exit Sub
   End If
    
End Sub


Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
     Set NewEnum = mCol.[_NewEnum]
End Property


Private Sub Class_Initialize()
   
    Set mCol = New Collection
    Set mDic = New Dictionary
End Sub


Private Sub Class_Terminate()
    'Itemroys collection when this class is terminated
    Set mCol = Nothing
    Set mDic = Nothing
End Sub

Public Function BorrarEnModoIndice(ByVal iIndice As Integer)
Dim IndFor As Integer
Dim oItem As CItem


If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    For IndFor = iIndice To mCol.Count - 2
            
        mCol.Remove (CStr(IndFor))
        mDic.Remove (CStr(IndFor))
        Set oItem = mCol.Item(CStr(IndFor + 1))
        mCol.Add oItem, CStr(IndFor)
        mDic.Add CStr(IndFor), CStr(IndFor)
        Set oItem = Nothing
    Next IndFor
    
    mCol.Remove CStr(IndFor)
    mDic.Remove CStr(IndFor)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CAdjudicaciones", "BorrarEnModoIndice", ERR, Erl)
      Exit Function
   End If
    
End Function

''' <summary>Carga las adjudicaciones</summary>
''' <param "lAnyo">A�o</param>
''' <param "sGMN1">GMN1</param>
''' <param "lProce">Cod. proceso</param>
''' <param "UsarIndice">Indica si hay que usar �ndice</param>
''' <remarks>Llamada desde: FSGSClient; Tiempo m�ximo: < 1 sec </remarks>
''' <revision>LTG 22/12/2011</revision>

Public Sub CargarAdjudicaciones(ByVal lAnyo As Long, ByVal sGMN1 As String, ByVal lProce As Long, Optional ByVal UsarIndice As Boolean)
    Dim AdoRes As adodb.Recordset
    Dim lIndice As Long
    Dim oCom As adodb.Command
    Dim oParam As adodb.Parameter
    
    'PARA USAR DESDE LA GENERACI�N DE PEDIDOS DIRECTOS DE LA SOLICITUD:
    '********* Precondicion **************************************
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CAdjudicaciones.CargarAdjudicaciones", "No se ha establecido la conexion"
        Exit Sub
    End If
    '*************************************************************
    
    If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
            
    Set oCom = New adodb.Command
    With oCom
        Set .ActiveConnection = m_oConexion.ADOCon
        .CommandType = adCmdStoredProc
        .CommandText = "FSGS_DEVOLVER_ADJUDICACIONES_ITEMS"
        
        Set oParam = .CreateParameter("@ANYO", adSmallInt, adParamInput, , lAnyo)
        .Parameters.Append oParam
        Set oParam = .CreateParameter("@GMN1", adVarChar, adParamInput, 50, sGMN1)
        .Parameters.Append oParam
        Set oParam = .CreateParameter("@PROCE", adInteger, adParamInput, , lProce)
        .Parameters.Append oParam
        
        Set AdoRes = .Execute
    End With
        
    Set mCol = New Collection
    
    If Not AdoRes.eof Then
        Dim sCod As String
        
        If UsarIndice Then
            lIndice = 0
            
            While Not AdoRes.eof
                Me.Add AdoRes.Fields("PROVE").Value, AdoRes.Fields("OFE").Value, AdoRes.Fields("ITEM").Value, NullToDbl0(AdoRes.Fields("PREC_VALIDO").Value), NullToDbl0(AdoRes.Fields("PORCEN").Value), lIndice, , , , AdoRes.Fields("GRUPO").Value
                sCod = CStr(AdoRes.Fields("ITEM").Value) & AdoRes.Fields("PROVE").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(AdoRes.Fields("PROVE").Value))
                Me.Item(sCod).ImporteAdjTot = AdoRes.Fields("ADJUDICADO").Value
                AdoRes.MoveNext
                lIndice = lIndice + 1
            Wend
        Else
            While Not AdoRes.eof
                Me.Add AdoRes.Fields("PROVE").Value, AdoRes.Fields("OFE").Value, AdoRes.Fields("ITEM").Value, NullToDbl0(AdoRes.Fields("PREC_VALIDO").Value), NullToDbl0(AdoRes.Fields("PORCEN").Value), , , , , AdoRes.Fields("GRUPO").Value
                sCod = CStr(AdoRes.Fields("ITEM").Value) & AdoRes.Fields("PROVE").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(AdoRes.Fields("PROVE").Value))
                Me.Item(sCod).ImporteAdjTot = AdoRes.Fields("ADJUDICADO").Value
                AdoRes.MoveNext
            Wend
        End If
    End If
    
    AdoRes.Close
    Set AdoRes = Nothing
    Set oCom = Nothing
    Set oParam = Nothing
    
    '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CAdjudicaciones", "CargarAdjudicaciones", ERR, Erl)
      Exit Sub
   End If
End Sub

''' <summary>devuelve un recordset con las adjudicaciones del proveedor</summary>
''' <param name="sProve">C�digo del proveedor</param>
''' <returns>recordaset con las adjudicaciones del proveedor</returns>
''' <remarks>Llamada desde:frmAdjProve.CargarAdjudicacionesProveedor</remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>
''' <revision>LTG 29/06/2011</revision>

Public Function DevolverAdjudicacionesProveedor(ByVal sProve As String, ByVal sIdioma As String, ByVal sMoneda As String, ByVal dblCambio As Double) As adodb.Recordset
    Dim oConn As adodb.Connection
    Dim rstAdj As adodb.Recordset
    Dim rs As adodb.Recordset
    Dim oSTM As adodb.Stream
    Dim sConsulta As String
    
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sConsulta = "SHAPE {"
    sConsulta = sConsulta & " SELECT PROVE, AP.GMN1, AP.GMN2, AP.GMN3, AP.GMN4, GMN4.DEN_" & sIdioma
    sConsulta = sConsulta & " FROM PROVE_GMN4 AP WITH (NOLOCK) INNER JOIN"
    sConsulta = sConsulta & "     GMN4 WITH (NOLOCK) ON AP.GMN1=GMN4.GMN1 AND AP.GMN2=GMN4.GMN2 AND AP.GMN3=GMN4.GMN3 AND AP.GMN4=GMN4.COD"
    sConsulta = sConsulta & " WHERE AP.PROVE='" & DblQuote(sProve) & "' AND AP.ADJ>0"
    sConsulta = sConsulta & " }"
    sConsulta = sConsulta & " APPEND ({"
    sConsulta = sConsulta & " SELECT ISNULL(I.GMN1, A.GMN1) AS GMN1, ISNULL(I.GMN2, A.GMN2) AS GMN2, ISNULL(I.GMN3, A.GMN3) AS GMN3, ISNULL(I.GMN4, A.GMN4) AS GMN4,"
    sConsulta = sConsulta & "     I.ART , I.DESCR, I.DEST, I.UNI, "
    sConsulta = sConsulta & "     CASE PO.MON "
    sConsulta = sConsulta & "          WHEN '" & DblQuote(sMoneda) & "' THEN IOF.PREC_VALIDO "
    sConsulta = sConsulta & "          ELSE CASE P.MON "
    sConsulta = sConsulta & "                    WHEN '" & DblQuote(sMoneda) & "' THEN (IOF.PREC_VALIDO/PO.CAMBIO) "
    sConsulta = sConsulta & "                    ELSE CASE '" & DblQuote(gParametrosGenerales.gsMONCEN) & "'"
    sConsulta = sConsulta & "                         WHEN '" & DblQuote(sMoneda) & "' THEN ((IOF.PREC_VALIDO/PO.CAMBIO)/P.CAMBIO)"
    sConsulta = sConsulta & "                         ELSE ((IOF.PREC_VALIDO/PO.CAMBIO)/P.CAMBIO)*" & DblToSQLFloat(dblCambio) & " END END END AS PREC_VALIDO, "
    sConsulta = sConsulta & "     IAJ.CANT_ADJ, IAJ.PORCEN, I.FECINI, I.FECFIN"
    sConsulta = sConsulta & " FROM ITEM_ADJ AS IAJ WITH (NOLOCK) INNER JOIN"
    sConsulta = sConsulta & "     PROCE AS P WITH (NOLOCK) ON IAJ.ANYO = P.ANYO AND IAJ.GMN1 = P.GMN1 AND IAJ.PROCE = P.COD INNER JOIN"
    sConsulta = sConsulta & "     ITEM AS I WITH (NOLOCK) ON IAJ.ANYO = I.ANYO AND IAJ.GMN1 = I.GMN1_PROCE AND IAJ.PROCE = I.PROCE AND"
    sConsulta = sConsulta & "     IAJ.ITEM = I.ID LEFT JOIN"
    sConsulta = sConsulta & "     ART4 AS A WITH (NOLOCK) ON I.ART = A.COD LEFT JOIN"
    sConsulta = sConsulta & "     ITEM_OFE AS IOF WITH (NOLOCK) ON IOF.NUM = IAJ.OFE AND IOF.PROVE = IAJ.PROVE AND IOF.ITEM = IAJ.ITEM AND"
    sConsulta = sConsulta & "     IOF.PROCE = IAJ.PROCE AND IOF.GMN1 = IAJ.GMN1 AND IOF.ANYO = IAJ.ANYO INNER JOIN"
    sConsulta = sConsulta & "     PROCE_OFE PO WITH (NOLOCK) ON PO.ANYO=IOF.ANYO AND PO.GMN1=IOF.GMN1 AND PO.PROCE=IOF.PROCE AND"
    sConsulta = sConsulta & "     PO.PROVE=IOF.PROVE AND PO.OFE=IOF.NUM"
    sConsulta = sConsulta & " WHERE ((P.EST IN (12, 13) AND IAJ.PORCEN > 0) OR"
    sConsulta = sConsulta & "      (P.EST = 11 AND IAJ.PORCEN > 0 AND I.EST = 1)) AND"
    sConsulta = sConsulta & "      IAJ.PROVE='" & DblQuote(sProve) & "'"
    sConsulta = sConsulta & " ORDER BY I.ART ASC, I.FECINI ASC"
    sConsulta = sConsulta & "}"
    sConsulta = sConsulta & " RELATE 'GMN1' TO 'GMN1','GMN2' TO 'GMN2','GMN3' TO 'GMN3','GMN4' TO 'GMN4'"
    sConsulta = sConsulta & ") AS ADJ"
    
    Set oConn = New adodb.Connection
    oConn.Open "Provider=MSDataShape.1;Persist Security Info=True;Data Source=" & basParametros.ServidorGS & ";User ID=" & LICDBLogin & ";Password=" & LICDBContra & ";Initial Catalog=" & basParametros.BaseDatosGS & ";Data Provider=SQLOLEDB.1"
    oConn.CursorLocation = adUseClient
    oConn.CommandTimeout = 120
    
    Set rstAdj = New adodb.Recordset
    rstAdj.CursorLocation = adUseClient
    rstAdj.Open sConsulta, oConn, adOpenForwardOnly, adLockBatchOptimistic
    rstAdj.ActiveConnection = Nothing
    
    oConn.Close
    Set oConn = Nothing
    
    Set oSTM = New adodb.Stream
    rstAdj.Save oSTM, adPersistXML
    Set rs = New adodb.Recordset
    rs.Open oSTM
    rs.ActiveConnection = Nothing
    
    Set DevolverAdjudicacionesProveedor = rs
    
    oSTM.Close
    Set oSTM = Nothing
    Set rstAdj = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CAdjudicaciones", "DevolverAdjudicacionesProveedor", ERR, Erl)
      Exit Function
   End If
End Function


