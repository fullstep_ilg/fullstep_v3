VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CPMParticipantes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Private Enum TipoDeError
'
'    ConexionNoEstablecida = 613
'
'End Enum

Private m_Col As Collection
'Private m_oConexion As CConexion
Private m_bEOF As Boolean

Public Property Get eof() As Boolean
    eof = m_bEOF
End Property
Friend Property Let eof(ByVal b As Boolean)
    m_bEOF = b
End Property

Public Property Get Item(vntIndexKey As Variant) As CPMParticipante
On Error GoTo NoSeEncuentra:

    Set Item = m_Col(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

'Friend Property Set Conexion(ByVal con As CConexion)
'    Set m_oConexion = con
'End Property
'
'Friend Property Get Conexion() As CConexion
'    Set Conexion = m_oConexion
'End Property

Public Property Get Count() As Long
    If m_Col Is Nothing Then
        Count = 0
    Else
         Count = m_Col.Count
    End If
End Property

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
     Set NewEnum = m_Col.[_NewEnum]
End Property

Public Function Add(ByVal lId As Long, ByVal lRol As Long, Optional ByVal sPer As String, Optional ByVal sGMN1 As String, Optional ByVal sGMN2 As String, Optional ByVal sGMN3 As String, Optional ByVal sGMN4 As String, Optional ByVal vIndice As Variant) As CPMParticipante
    
    'create a new object
    Dim objnewmember As CPMParticipante
    
    Set objnewmember = New CPMParticipante
    With objnewmember
        'Set .Conexion = m_oConexion
        .ID = lId
        .Rol = lRol
        .Per = sPer
        .GMN1 = sGMN1
        .GMN2 = sGMN2
        .GMN3 = sGMN3
        .GMN4 = sGMN4
                
        If Not IsMissing(vIndice) And Not IsNull(vIndice) Then
            .Indice = vIndice
            m_Col.Add objnewmember, CStr(vIndice)
        Else
            m_Col.Add objnewmember, CStr(lId)
        End If
    
    End With
    
    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing


End Function

Public Sub AddParticipante(ByVal oParticipante As CPMParticipante, Optional ByVal vIndice As Variant)
    If Not IsMissing(vIndice) And Not IsNull(vIndice) Then
        oParticipante.Indice = vIndice
        m_Col.Add oParticipante, CStr(vIndice)
    Else
        m_Col.Add oParticipante, CStr(oParticipante.ID)
    End If
End Sub

Public Sub Remove(vntIndexKey As Variant)
On Error GoTo Error

    m_Col.Remove vntIndexKey

Error:

End Sub

Public Sub Clear()
    Dim i As Integer
    Dim Count As Integer
    Count = m_Col.Count
    For i = 1 To Count
        m_Col.Remove 1
    Next
End Sub


Private Sub Class_Initialize()
    Set m_Col = New Collection
End Sub


Private Sub Class_Terminate()
    'Proceroys collection when this class is terminated
    On Error Resume Next
    Set m_Col = Nothing
'    Set m_oConexion = Nothing
    
End Sub






