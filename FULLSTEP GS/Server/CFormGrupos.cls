VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CFormGrupos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

Private mCol As Collection

Private m_oConexion As CConexion
Private m_bEOF As Boolean

Public Property Get eof() As Boolean
    eof = m_bEOF
End Property
Friend Property Let eof(ByVal b As Boolean)
    m_bEOF = b
End Property

Public Function Add(ByVal Id As Long, ByVal Den As CMultiidiomas, Optional ByVal FechaActual As Variant, _
                    Optional ByVal oFormulario As CFormulario, Optional ByVal oInstancia As CInstancia, _
                    Optional ByVal vIndice As Variant, Optional ByVal Orden As Variant) As CFormGrupo
        
    Dim objnewmember As CFormGrupo
    
    Set objnewmember = New CFormGrupo
    
    Set objnewmember.Conexion = m_oConexion
    
    Set objnewmember.Formulario = oFormulario
    Set objnewmember.Instancia = oInstancia
    
    objnewmember.Id = Id
    
    Set objnewmember.Denominaciones = Den
    
    If Not IsMissing(Orden) Then
        objnewmember.Orden = Orden
    Else
        objnewmember.Orden = Null
    End If
    
    If IsMissing(FechaActual) Then
        objnewmember.FecAct = Null
    Else
        objnewmember.FecAct = FechaActual
    End If
    
    If Not IsMissing(vIndice) And Not IsNull(vIndice) Then
        objnewmember.Indice = vIndice
        mCol.Add objnewmember, CStr(vIndice)
    Else
        mCol.Add objnewmember, CStr(Id)
    End If

    Set Add = objnewmember
    
    Set objnewmember = Nothing

End Function

Friend Property Set Conexion(ByVal vData As CConexion)
    Set m_oConexion = vData
End Property

Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property

Private Sub InsertarCamposVisiblesNotif(ByVal lCampo As Long, ByVal lForm As Long)
Dim i As Integer
Dim sCampo As String
Dim sConsulta As String
For i = 0 To 2
    Select Case i
        Case 0  '-- Workflow de alta
            sCampo = ""
            sConsulta = "INSERT INTO PM_NOTIFICADO_AVISO_EXPIRACION_CAMPOS (CAMPO, NOTIFICADO, VISIBLE)" & _
                " SELECT " & lCampo & ", PM_NOTIFICADO_AVISO_EXPIRACION.ID, CASE WHEN PM_NOTIFICADO_AVISO_EXPIRACION.CONFIGURACION_ROL=0 THEN 1 ELSE 0 END " & _
                " FROM PM_NOTIFICADO_AVISO_EXPIRACION WITH (NOLOCK) " & _
                " INNER JOIN PM_AVISO_EXPIRACION_CAMPOS WITH (NOLOCK) ON PM_AVISO_EXPIRACION_CAMPOS.ID=PM_NOTIFICADO_AVISO_EXPIRACION.AVISO_EXPIRACION_CAMPOS" & _
                " INNER JOIN SOLICITUD WITH (NOLOCK) ON SOLICITUD.WORKFLOW" & sCampo & "=PM_AVISO_EXPIRACION_CAMPOS.WORKFLOW" & _
                " WHERE SOLICITUD.FORMULARIO=" & lForm
            m_oConexion.ADOCon.Execute sConsulta
            
            'COPIA
            sConsulta = "INSERT INTO PM_COPIA_NOTIFICADO_AVISO_EXPIRACION_CAMPOS (CAMPO, NOTIFICADO, VISIBLE)" & _
                " SELECT " & lCampo & ", PM_COPIA_NOTIFICADO_AVISO_EXPIRACION.ID, CASE WHEN PM_COPIA_NOTIFICADO_AVISO_EXPIRACION.CONFIGURACION_ROL=0 THEN 1 ELSE 0 END " & _
                " FROM PM_COPIA_NOTIFICADO_AVISO_EXPIRACION WITH (NOLOCK) " & _
                " INNER JOIN PM_COPIA_AVISO_EXPIRACION_CAMPOS WITH (NOLOCK) ON PM_COPIA_AVISO_EXPIRACION_CAMPOS.ID=PM_COPIA_NOTIFICADO_AVISO_EXPIRACION.AVISO_EXPIRACION_CAMPOS" & _
                " INNER JOIN PM_COPIA_WORKFLOW WITH (NOLOCK) ON PM_COPIA_WORKFLOW.ID=PM_COPIA_AVISO_EXPIRACION_CAMPOS.WORKFLOW AND PM_COPIA_WORKFLOW.ULTIMO=1" & _
                " INNER JOIN SOLICITUD WITH (NOLOCK) ON SOLICITUD.WORKFLOW" & sCampo & "=PM_COPIA_WORKFLOW.WORKFLOW_ORIGEN " & _
                " WHERE SOLICITUD.FORMULARIO=" & lForm
            m_oConexion.ADOCon.Execute sConsulta
        Case 1  '-- Workflow de baja
            sCampo = "_BAJA"
        Case 2  '-- Workflow de modificaci�n
            sCampo = "_MODIF"
    End Select
    sConsulta = "INSERT INTO PM_NOTIFICADO_ACCION_CAMPOS (CAMPO, NOTIFICADO, VISIBLE)" & _
        " SELECT " & lCampo & ", PM_NOTIFICADO_ACCION.ID, CASE WHEN PM_NOTIFICADO_ACCION.CONFIGURACION_ROL=0 THEN 1 ELSE 0 END " & _
        " FROM PM_NOTIFICADO_ACCION WITH(NOLOCK) " & _
        " INNER JOIN PM_ACCIONES WITH(NOLOCK) ON PM_ACCIONES.ID=PM_NOTIFICADO_ACCION.ACCION" & _
        " INNER JOIN PM_BLOQUE WITH(NOLOCK) ON PM_BLOQUE.ID=PM_ACCIONES.BLOQUE" & _
        " INNER JOIN SOLICITUD WITH(NOLOCK) ON SOLICITUD.WORKFLOW" & sCampo & "=PM_BLOQUE.WORKFLOW" & _
        " WHERE SOLICITUD.FORMULARIO=" & lForm
    m_oConexion.ADOCon.Execute sConsulta
    
    'COPIA
    sConsulta = "INSERT INTO PM_COPIA_NOTIFICADO_ACCION_CAMPOS (CAMPO, NOTIFICADO, VISIBLE)" & _
        " SELECT " & lCampo & ", PM_COPIA_NOTIFICADO_ACCION.ID, CASE WHEN PM_COPIA_NOTIFICADO_ACCION.CONFIGURACION_ROL=0 THEN 1 ELSE 0 END " & _
        " FROM PM_COPIA_NOTIFICADO_ACCION WITH(NOLOCK) " & _
        " INNER JOIN PM_COPIA_ACCIONES WITH(NOLOCK) ON PM_COPIA_ACCIONES.ID=PM_COPIA_NOTIFICADO_ACCION.ACCION" & _
        " INNER JOIN PM_COPIA_BLOQUE WITH(NOLOCK) ON PM_COPIA_BLOQUE.ID=PM_COPIA_ACCIONES.BLOQUE" & _
        " INNER JOIN PM_COPIA_WORKFLOW WITH (NOLOCK) ON PM_COPIA_WORKFLOW.ID=PM_COPIA_BLOQUE.WORKFLOW AND PM_COPIA_WORKFLOW.ULTIMO=1 " & _
        " INNER JOIN SOLICITUD WITH(NOLOCK) ON SOLICITUD.WORKFLOW" & sCampo & "=PM_COPIA_WORKFLOW.WORKFLOW_ORIGEN" & _
        " WHERE SOLICITUD.FORMULARIO=" & lForm
    m_oConexion.ADOCon.Execute sConsulta
    
    sConsulta = "INSERT INTO PM_NOTIFICADO_ENLACE_CAMPOS (CAMPO, NOTIFICADO, VISIBLE)" & _
        " SELECT " & lCampo & ", PM_NOTIFICADO_ENLACE.ID, CASE WHEN PM_NOTIFICADO_ENLACE.CONFIGURACION_ROL=0 THEN 1 ELSE 0 END " & _
        " FROM PM_NOTIFICADO_ENLACE WITH(NOLOCK) " & _
        " INNER JOIN PM_ENLACE WITH(NOLOCK) ON PM_ENLACE.ID=PM_NOTIFICADO_ENLACE.ENLACE" & _
        " INNER JOIN PM_BLOQUE WITH(NOLOCK) ON PM_BLOQUE.ID=PM_ENLACE.BLOQUE_ORIGEN" & _
        " INNER JOIN SOLICITUD WITH(NOLOCK) ON SOLICITUD.WORKFLOW" & sCampo & "=PM_BLOQUE.WORKFLOW" & _
        " WHERE SOLICITUD.FORMULARIO=" & lForm
    m_oConexion.ADOCon.Execute sConsulta
    
    'COPIA
    sConsulta = "INSERT INTO PM_COPIA_NOTIFICADO_ENLACE_CAMPOS (CAMPO, NOTIFICADO, VISIBLE)" & _
        " SELECT " & lCampo & ", PM_COPIA_NOTIFICADO_ENLACE.ID, CASE WHEN PM_COPIA_NOTIFICADO_ENLACE.CONFIGURACION_ROL=0 THEN 1 ELSE 0 END " & _
        " FROM PM_COPIA_NOTIFICADO_ENLACE WITH(NOLOCK) " & _
        " INNER JOIN PM_COPIA_ENLACE WITH(NOLOCK) ON PM_COPIA_ENLACE.ID=PM_COPIA_NOTIFICADO_ENLACE.ENLACE" & _
        " INNER JOIN PM_COPIA_BLOQUE WITH(NOLOCK) ON PM_COPIA_BLOQUE.ID=PM_COPIA_ENLACE.BLOQUE_ORIGEN " & _
        " INNER JOIN PM_COPIA_WORKFLOW WITH (NOLOCK) ON PM_COPIA_WORKFLOW.ID=PM_COPIA_BLOQUE.WORKFLOW AND PM_COPIA_WORKFLOW.ULTIMO=1" & _
        " INNER JOIN SOLICITUD WITH(NOLOCK) ON SOLICITUD.WORKFLOW" & sCampo & "=PM_COPIA_WORKFLOW.WORKFLOW_ORIGEN" & _
        " WHERE SOLICITUD.FORMULARIO=" & lForm
    m_oConexion.ADOCon.Execute sConsulta
Next i
End Sub

Public Property Get Item(vntIndexKey As Variant) As CFormGrupo

    ''' * Objetivo: Recuperar una moneda de la coleccion
    ''' * Recibe: Indice de la moneda a recuperar
    ''' * Devuelve: Moneda correspondiente
        
On Error GoTo NoSeEncuentra:

    ''' Devolvemos el item de la coleccion privada
    
    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:

    Set Item = Nothing
    
End Property

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4

    ''' ! Pendiente de revision, objetivo desconocido
    
    Set NewEnum = mCol.[_NewEnum]

End Property
Public Sub Remove(vntIndexKey As Variant)

    ''' * Objetivo: Eliminar una moneda de la coleccion
    ''' * Recibe: Indice de la moneda a eliminar
    ''' * Devuelve: Nada
   
    mCol.Remove vntIndexKey
    
End Sub
Private Sub Class_Initialize()

    ''' * Objetivo: Crear la coleccion
    Set mCol = New Collection
    
End Sub
Private Sub Class_Terminate()

    ''' * Objetivo: Limpiar memoria
    
    Set mCol = Nothing
    Set m_oConexion = Nothing
End Sub

Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
     Count = mCol.Count
End If

End Property


Public Function GuardarOrdenGrupos() As TipoErrorSummit
'****************************************************************************************
'Descripci�n:   Funci�n para guardar el orden de los grupos
'Valor que devuelve: Error
'****************************************************************************************

Dim sConsulta As String
Dim TESError As TipoErrorSummit
Dim oGrupo As CFormGrupo
Dim btrans As Boolean
Dim rs As adodb.Recordset

'******************* Precondicion *******************
If m_oConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CFormGrupos.GuardarOrdenGrupos", "No se ha establecido la conexion"
End If
'*****************************************************

On Error GoTo ERROR:

    TESError.NumError = TESnoerror
    
    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
    btrans = True
    
    Set rs = New adodb.Recordset
    
    For Each oGrupo In Me
        sConsulta = "UPDATE FORM_GRUPO SET ORDEN=" & StrToSQLNULL(oGrupo.Orden) & " WHERE ID=" & oGrupo.Id & " AND BAJALOG=0"
        m_oConexion.ADOCon.Execute sConsulta
        If m_oConexion.ADOCon.Errors.Count > 0 Then
            GoTo ERROR
        End If
        
        sConsulta = "SELECT FECACT FROM FORM_GRUPO WITH (NOLOCK) WHERE ID=" & oGrupo.Id & " AND BAJALOG=0"
        rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
        If Not rs.eof Then
            oGrupo.FecAct = rs(0).Value
        End If
        rs.Close
    Next
    
    Set rs = Nothing
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    btrans = False
    GuardarOrdenGrupos = TESError
    
    Exit Function

ERROR:
    GuardarOrdenGrupos = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    
    If btrans Then
        m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
    End If
    Set rs = Nothing
End Function


Public Function CopiarGrupo(ByVal lIdGrupo As Long, lOrden As Long, ByVal sCopia As String, ByVal lForm As Long) As TipoErrorSummit
'****************************************************************************************
'Descripci�n:   Funci�n para guardar copiar un grupo de otro,en un determinado orden y
'modificar el orden del resto de grupos del formulario
' lIdGrupo: Id del grupo que se copia
' lOrden: El orden del grupo que se copia
' sCopia: Denominaci�n del grupo que se copia
' lForm: El formulario del grupo
'Valor que devuelve: Error
'Revisado por ILG 05/09/2011
'****************************************************************************************

Dim sConsulta As String
Dim TESError As TipoErrorSummit
Dim oGrupo As CFormGrupo
Dim btrans As Boolean
Dim rs As adodb.Recordset
Dim sDen As String
Dim sCod As String
Dim sAyuda As String
Dim lNuevoGrupo As Long
Dim lNuevoCampo As Long
Dim lSubcampo As Long
Dim sValor As String
Dim rsSub As adodb.Recordset
Dim rsCampo As adodb.Recordset
Dim rsCampoId As adodb.Recordset
Dim lCampoAntNuevo As Dictionary
Dim i As Integer
Dim sCampo As String
Dim sX As String
'******************* Precondicion *******************
If m_oConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CFormGrupos.CopiarGrupo", "No se ha establecido la conexion"
End If
'*****************************************************

On Error GoTo ERROR:

    TESError.NumError = TESnoerror
    
    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
    btrans = True
    
    Set rs = New adodb.Recordset
    
    'A�ade el grupo copi�ndole del que se pasa como par�metro:
    sConsulta = "SELECT COD FROM IDIOMAS WITH(NOLOCK) "
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    While Not rs.eof
        sValor = sValor & "," & "VALOR_TEXT_" & rs.Fields("COD").Value
        sAyuda = sAyuda & "," & "AYUDA_" & rs.Fields("COD").Value
        sCod = sCod & "," & "DEN_" & rs.Fields("COD").Value
        sDen = sDen & ",'" & DblQuote(sCopia) & "' + DEN_" & rs.Fields("COD").Value
        rs.MoveNext
    Wend
    rs.Close
    
    sConsulta = "INSERT INTO FORM_GRUPO (FORMULARIO " & sCod & ",ORDEN) SELECT FORMULARIO" & sDen & "," & lOrden & " FROM FORM_GRUPO WITH(NOLOCK) WHERE ID=" & lIdGrupo & " AND BAJALOG=0"
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR
    'Obtiene el id del GRUPO introducido:
    sConsulta = "SELECT MAX(ID) AS ID FROM FORM_GRUPO WHERE FORMULARIO=" & lForm
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
    lNuevoGrupo = rs("ID").Value
    rs.Close
    
    Set rsCampo = New adodb.Recordset
    sConsulta = "SELECT ID, SUBTIPO FROM FORM_CAMPO WITH(NOLOCK) WHERE GRUPO=" & lIdGrupo & " AND ES_SUBCAMPO=0 AND BAJALOG = 0"
    rsCampo.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    Set lCampoAntNuevo = New Dictionary
    
    sConsulta = "SELECT MAX(ID_CALCULO) AS ID FROM FORM_CAMPO FC WITH(NOLOCK) INNER JOIN FORM_GRUPO FG WITH(NOLOCK) ON FC.GRUPO = FG.ID WHERE FG.FORMULARIO =" & lForm & " AND LEFT(FC.ID_CALCULO,1) = 'X' AND FC.BAJALOG=0"
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
    sX = "X"
    If Not IsNull(rs("ID").Value) Then sX = sX & CInt(Right(rs("ID").Value, Len(rs("ID").Value) - 1)) + 1
    rs.Close
    
    While Not rsCampo.eof
        'Inserta los campos del nuevo grupo copi�ndolos del anterior
        sConsulta = "INSERT INTO FORM_CAMPO (GRUPO " & sCod & sAyuda & ",TIPO,SUBTIPO,INTRO,VALOR_NUM,VALOR_TEXT,VALOR_FEC,VALOR_BOOL,MINNUM,MAXNUM,MINFEC,MAXFEC,"
        sConsulta = sConsulta & "ORDEN,ID_ATRIB_GS,TIPO_CAMPO_GS,ES_SUBCAMPO,GMN1,GMN2,GMN3,GMN4,LINEAS_PRECONF,ID_CALCULO,FORMULA,ORDEN_CALCULO,ORIGEN_CALC_DESGLOSE,POPUP,ANYADIR_ART,IDREFSOLICITUD, AVISOBLOQUEOIMPACUM,CARGAR_ULT_ADJ,MAX_LENGTH,A_PROCESO,A_PEDIDO,TABLA_EXTERNA,PRES5,CENTRO_COSTE,VER_UON,CAMPO_ORIGEN, SAVE_VALUES,DEN_PROCESO,FORMATO,SERVICIO,NIVEL_SELECCION,FECHA_VALOR_NO_ANT_SIS,CARGAR_REL_PROVE_ART4) "
        sConsulta = sConsulta & " SELECT " & lNuevoGrupo & sCod & sAyuda & ",TIPO,SUBTIPO,INTRO,VALOR_NUM,VALOR_TEXT,VALOR_FEC,VALOR_BOOL,MINNUM,MAXNUM,MINFEC,MAXFEC,"
        sConsulta = sConsulta & "ORDEN,ID_ATRIB_GS,TIPO_CAMPO_GS,ES_SUBCAMPO,GMN1,GMN2,GMN3,GMN4,LINEAS_PRECONF,CASE WHEN LEFT(ID_CALCULO,1) = 'X' THEN " & StrToSQLNULL(sX) & " ELSE NULL END,FORMULA,ORDEN_CALCULO,ORIGEN_CALC_DESGLOSE,POPUP,ANYADIR_ART,IDREFSOLICITUD, AVISOBLOQUEOIMPACUM,CARGAR_ULT_ADJ,MAX_LENGTH,A_PROCESO,A_PEDIDO,TABLA_EXTERNA,PRES5,CENTRO_COSTE,VER_UON,ID, SAVE_VALUES,DEN_PROCESO,FORMATO,SERVICIO,NIVEL_SELECCION,FECHA_VALOR_NO_ANT_SIS,CARGAR_REL_PROVE_ART4 "
        sConsulta = sConsulta & " FROM FORM_CAMPO WITH(NOLOCK) WHERE ID=" & rsCampo.Fields("ID").Value
        m_oConexion.ADOCon.Execute sConsulta
        If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR
        'Obtiene el id del Campo introducido:
        sConsulta = "SELECT MAX(ID) AS ID FROM FORM_CAMPO WHERE GRUPO=" & lNuevoGrupo
        rs.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
        lNuevoCampo = rs("ID").Value
        rs.Close
        
        'Meto al diccionario el ID nuevo de cada campo para su ID viejo
        lCampoAntNuevo.Add CStr(rsCampo.Fields("ID").Value), lNuevoCampo
        
        'A�ado a FORM_CAMPO_SERVICIO_PARAMETROS si es de tipo servicio
        sConsulta = "INSERT INTO FORM_CAMPO_SERVICIO_PARAMETRO (FORM_CAMPO,SERVICIO_PARAMETRO,DIRECTO,VALOR_TEXT,"
        sConsulta = sConsulta & "VALOR_NUM,VALOR_FEC,VALOR_BOOL,TIPO_CAMPO,CAMPO)"
        sConsulta = sConsulta & " SELECT " & lNuevoCampo & ",SERVICIO_PARAMETRO,DIRECTO,FCSP.VALOR_TEXT,"
        sConsulta = sConsulta & "FCSP.VALOR_NUM,FCSP.VALOR_FEC,FCSP.VALOR_BOOL,TIPO_CAMPO,CAMPO"
        sConsulta = sConsulta & " FROM FORM_CAMPO_SERVICIO_PARAMETRO FCSP WITH(NOLOCK) "
        sConsulta = sConsulta & "INNER JOIN FORM_CAMPO FC WITH(NOLOCK) ON FC.ID=FCSP.FORM_CAMPO "
        sConsulta = sConsulta & "WHERE FC.TIPO=7 AND FCSP.FORM_CAMPO=" & rsCampo.Fields("ID").Value
        m_oConexion.ADOCon.Execute sConsulta
        If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR
        
        'Se a�adir� el campo en las tablas de filtros (BATCHUpdates)
        sConsulta = "INSERT INTO UPD_VISORES_PM (ACCION,ID_TABLA,ID_CAMPO,SUBTIPO,NOM_TABLA)"
        sConsulta = sConsulta & "SELECT 'I',P.ID, C.ID, CASE WHEN C.TIPO_CAMPO_GS = " & TipoCampoGS.Almacen & " THEN " & TiposDeAtributos.TipoTextoMedio & " ELSE C.SUBTIPO END, P.NOM_TABLA FROM PM_FILTROS P WITH (NOLOCK) "
        sConsulta = sConsulta & "INNER JOIN FORMULARIO F WITH (NOLOCK) ON F.ID=P.FORMULARIO "
        sConsulta = sConsulta & "INNER JOIN FORM_GRUPO G WITH (NOLOCK) ON F.ID=G.FORMULARIO "
        sConsulta = sConsulta & "INNER JOIN FORM_CAMPO C WITH (NOLOCK) ON G.ID=C.GRUPO "
        sConsulta = sConsulta & "WHERE C.ES_SUBCAMPO = 0 AND C.SUBTIPO <> " & TiposDeAtributos.TipoDesglose & " "
        sConsulta = sConsulta & "AND (C.TIPO_CAMPO_GS <> " & TipoCampoGS.Desglose & " OR C.TIPO_CAMPO_GS IS NULL) AND C.ID  = " & lNuevoCampo & " AND G.BAJALOG=0"
        m_oConexion.ADOCon.Execute sConsulta
        If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR
        
        'Se a�aden los registros para este campo en QA_FILTRO_USU_CAMPOS
        sConsulta = "INSERT INTO QA_FILTRO_USU_CAMPOS (ID_FILTRO_USU,CAMPO) "
        sConsulta = sConsulta & "SELECT FU.ID, C.ID FROM QA_FILTRO_USU FU WITH (NOLOCK) "
        sConsulta = sConsulta & "INNER JOIN QA_FILTROS P WITH (NOLOCK) ON P.ID=FU.ID_FILTRO "
        sConsulta = sConsulta & "INNER JOIN FORMULARIO F WITH (NOLOCK) ON F.ID=P.FORMULARIO "
        sConsulta = sConsulta & "INNER JOIN FORM_GRUPO G WITH (NOLOCK) ON F.ID=G.FORMULARIO "
        sConsulta = sConsulta & "INNER JOIN FORM_CAMPO C WITH (NOLOCK) ON G.ID=C.GRUPO "
        sConsulta = sConsulta & "WHERE C.ES_SUBCAMPO = 0 AND C.SUBTIPO <> " & TiposDeAtributos.TipoDesglose & " AND G.BAJALOG=0 "
        sConsulta = sConsulta & "AND (C.TIPO_CAMPO_GS <> " & TipoCampoGS.Desglose & " OR C.TIPO_CAMPO_GS IS NULL) AND C.ID  = " & lNuevoCampo
        m_oConexion.ADOCon.Execute sConsulta
        If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR
        
        'Se a�adir� el campo en las tablas de filtros (BATCHUpdates)
        sConsulta = "INSERT INTO UPD_VISORES_QA (ACCION,ID_TABLA,ID_CAMPO,SUBTIPO,NOM_TABLA)"
        sConsulta = sConsulta & "SELECT 'I',P.ID, C.ID,CASE WHEN C.TIPO_CAMPO_GS = " & TipoCampoGS.Almacen & " THEN " & TiposDeAtributos.TipoTextoMedio & " ELSE C.SUBTIPO END, P.NOM_TABLA FROM QA_FILTROS P WITH (NOLOCK) "
        sConsulta = sConsulta & "INNER JOIN FORMULARIO F WITH (NOLOCK) ON F.ID=P.FORMULARIO "
        sConsulta = sConsulta & "INNER JOIN FORM_GRUPO G WITH (NOLOCK) ON F.ID=G.FORMULARIO "
        sConsulta = sConsulta & "INNER JOIN FORM_CAMPO C WITH (NOLOCK) ON G.ID=C.GRUPO "
        sConsulta = sConsulta & "WHERE C.ES_SUBCAMPO = 0 AND C.SUBTIPO <> " & TiposDeAtributos.TipoDesglose & " "
        sConsulta = sConsulta & "AND (C.TIPO_CAMPO_GS <> " & TipoCampoGS.Desglose & " OR C.TIPO_CAMPO_GS IS NULL) AND C.ID  = " & lNuevoCampo & " AND G.BAJALOG=0"
        m_oConexion.ADOCon.Execute sConsulta
        If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR
        
        'Se a�aden los registros para este campo en PM_CONTR_FILTRO_USU_CAMPOS
        sConsulta = "DECLARE @POS INT" & vbCrLf
        sConsulta = sConsulta & "DECLARE @IDFILTRO INT" & vbCrLf
        sConsulta = sConsulta & "" & vbCrLf
        sConsulta = sConsulta & "DECLARE curFILTROS CURSOR LOCAL FOR " & vbCrLf
        sConsulta = sConsulta & "       SELECT DISTINCT FU.ID FROM PM_CONTR_FILTRO_USU FU WITH (NOLOCK) " & vbCrLf
        sConsulta = sConsulta & "       INNER JOIN PM_CONTR_FILTROS P WITH (NOLOCK) ON P.ID=FU.ID_FILTRO " & vbCrLf
        sConsulta = sConsulta & "       INNER JOIN FORMULARIO F WITH (NOLOCK) ON F.ID=P.FORMULARIO " & vbCrLf
        sConsulta = sConsulta & "       INNER JOIN FORM_GRUPO G WITH (NOLOCK) ON F.ID=G.FORMULARIO " & vbCrLf
        sConsulta = sConsulta & "       INNER JOIN FORM_CAMPO C WITH (NOLOCK) ON G.ID=C.GRUPO " & vbCrLf
        sConsulta = sConsulta & "       WHERE C.ES_SUBCAMPO = 0 AND C.SUBTIPO <> " & TiposDeAtributos.TipoDesglose & vbCrLf
        sConsulta = sConsulta & "           AND (C.TIPO_CAMPO_GS <> " & TipoCampoGS.Desglose & " OR C.TIPO_CAMPO_GS IS NULL) AND C.ID  = " & lNuevoCampo & " AND G.BAJALOG=0 " & vbCrLf
        sConsulta = sConsulta & "" & vbCrLf
        sConsulta = sConsulta & "OPEN curFILTROS" & vbCrLf
        sConsulta = sConsulta & "FETCH NEXT FROM curFILTROS INTO @IDFILTRO" & vbCrLf
        sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
        sConsulta = sConsulta & "BEGIN " & vbCrLf
        sConsulta = sConsulta & "       SET @POS =  (SELECT MAX(A.POS)" & vbCrLf
        sConsulta = sConsulta & "               FROM (SELECT POS FROM PM_CONTR_FILTRO_USU_CAMPOS WITH (NOLOCK) WHERE ID_FILTRO_USU=@IDFILTRO " & vbCrLf
        sConsulta = sConsulta & "                     UNION " & vbCrLf
        sConsulta = sConsulta & "                     SELECT POS FROM PM_CONTR_FILTRO_USU_CAMPOS_CONTR WITH (NOLOCK) WHERE ID_FILTRO_USU=@IDFILTRO) A " & vbCrLf
        sConsulta = sConsulta & "               ) " & vbCrLf
        sConsulta = sConsulta & "       " & vbCrLf
        sConsulta = sConsulta & "       SET @POS= ISNULL(@POS,0) + 1" & vbCrLf
        sConsulta = sConsulta & "       " & vbCrLf
        sConsulta = sConsulta & "       INSERT INTO PM_CONTR_FILTRO_USU_CAMPOS (ID_FILTRO_USU,CAMPO,TAMANYO,POS) " & vbCrLf
        sConsulta = sConsulta & "       SELECT FU.ID, C.ID, 100 TAMANYO, @POS POS FROM PM_CONTR_FILTRO_USU FU WITH (NOLOCK) " & vbCrLf
        sConsulta = sConsulta & "       INNER JOIN PM_CONTR_FILTROS P WITH (NOLOCK) ON P.ID=FU.ID_FILTRO " & vbCrLf
        sConsulta = sConsulta & "       INNER JOIN FORMULARIO F WITH (NOLOCK) ON F.ID=P.FORMULARIO " & vbCrLf
        sConsulta = sConsulta & "       INNER JOIN FORM_GRUPO G WITH (NOLOCK) ON F.ID=G.FORMULARIO " & vbCrLf
        sConsulta = sConsulta & "       INNER JOIN FORM_CAMPO C WITH (NOLOCK) ON G.ID=C.GRUPO " & vbCrLf
        sConsulta = sConsulta & "       WHERE C.ES_SUBCAMPO = 0 AND C.SUBTIPO <> " & TiposDeAtributos.TipoDesglose & vbCrLf
        sConsulta = sConsulta & "           AND (C.TIPO_CAMPO_GS <> " & TipoCampoGS.Desglose & " OR C.TIPO_CAMPO_GS IS NULL) AND C.ID  = " & lNuevoCampo & " AND G.BAJALOG=0 " & vbCrLf
        sConsulta = sConsulta & "           AND FU.ID=@IDFILTRO" & vbCrLf
        sConsulta = sConsulta & "       " & vbCrLf
        sConsulta = sConsulta & "       FETCH NEXT FROM curFILTROS INTO @IDFILTRO" & vbCrLf
        sConsulta = sConsulta & "END" & vbCrLf
        sConsulta = sConsulta & "CLOSE curFILTROS" & vbCrLf
        sConsulta = sConsulta & "DEALLOCATE curFILTROS" & vbCrLf
        
        'Se a�adir� el campo en las tablas de filtros (BATCHUpdates)
        sConsulta = "INSERT INTO UPD_VISORES_CONTR (ACCION,ID_TABLA,ID_CAMPO,SUBTIPO,NOM_TABLA)"
        sConsulta = sConsulta & "SELECT 'I',P.ID, C.ID,CASE WHEN C.TIPO_CAMPO_GS = " & TipoCampoGS.Almacen & " THEN " & TiposDeAtributos.TipoTextoMedio & " ELSE C.SUBTIPO END, P.NOM_TABLA FROM PM_CONTR_FILTROS P WITH (NOLOCK) "
        sConsulta = sConsulta & "INNER JOIN FORMULARIO F WITH (NOLOCK) ON F.ID=P.FORMULARIO "
        sConsulta = sConsulta & "INNER JOIN FORM_GRUPO G WITH (NOLOCK) ON F.ID=G.FORMULARIO "
        sConsulta = sConsulta & "INNER JOIN FORM_CAMPO C WITH (NOLOCK) ON G.ID=C.GRUPO "
        sConsulta = sConsulta & "WHERE C.ES_SUBCAMPO = 0 AND C.SUBTIPO <> " & TiposDeAtributos.TipoDesglose & " "
        sConsulta = sConsulta & "AND (C.TIPO_CAMPO_GS <> " & TipoCampoGS.Desglose & " OR C.TIPO_CAMPO_GS IS NULL) AND C.ID  = " & lNuevoCampo & " AND G.BAJALOG=0 "
        m_oConexion.ADOCon.Execute sConsulta
        If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR
        
        'Inserta los adjuntos del campo:
        sConsulta = "INSERT INTO CAMPO_ADJUN (CAMPO,NOM,RUTA,IDIOMA,DATASIZE,DATA,DGUID,PER,COMENT,FECALTA) "
        sConsulta = sConsulta & " SELECT " & lNuevoCampo & ",NOM,RUTA,IDIOMA,DATASIZE,DATA,NEWID(),PER,COMENT,FECALTA"
        sConsulta = sConsulta & " FROM CAMPO_ADJUN WITH (NOLOCK) WHERE CAMPO=" & rsCampo.Fields("ID").Value
        m_oConexion.ADOCon.Execute sConsulta
        If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR
        'Inserta los valores de las listas
        sConsulta = "INSERT INTO CAMPO_VALOR_LISTA (CAMPO,ORDEN,VALOR_NUM,VALOR_FEC" & sValor & ",ORD) "
        sConsulta = sConsulta & " SELECT " & lNuevoCampo & ",ORDEN,VALOR_NUM,VALOR_FEC" & sValor & ",ORD"
        sConsulta = sConsulta & " FROM CAMPO_VALOR_LISTA WITH (NOLOCK) WHERE CAMPO=" & rsCampo.Fields("ID").Value
        m_oConexion.ADOCon.Execute sConsulta
        If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR
        
        'Insertar los desgloses
        sConsulta = "SELECT D.CAMPO_HIJO "
        sConsulta = sConsulta & "FROM DESGLOSE D WITH(NOLOCK) "
        sConsulta = sConsulta & "INNER JOIN FORM_CAMPO FC WITH(NOLOCK) ON FC.ID=D.CAMPO_HIJO AND FC.BAJALOG=0 "
        sConsulta = sConsulta & "WHERE D.CAMPO_PADRE=" & rsCampo.Fields("ID").Value
        Set rsSub = New adodb.Recordset
        rsSub.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
        
        While Not rsSub.eof
            sConsulta = "INSERT INTO FORM_CAMPO (GRUPO " & sCod & sAyuda & ",TIPO,SUBTIPO,INTRO,VALOR_NUM,VALOR_TEXT,VALOR_FEC,VALOR_BOOL,MINNUM,MAXNUM,MINFEC,MAXFEC,"
            sConsulta = sConsulta & "ORDEN,ID_ATRIB_GS,TIPO_CAMPO_GS,ES_SUBCAMPO,FORMATO,GMN1,GMN2,GMN3,GMN4,LINEAS_PRECONF,ID_CALCULO,FORMULA,ORDEN_CALCULO,ORIGEN_CALC_DESGLOSE,ANYADIR_ART,CARGAR_ULT_ADJ,MAX_LENGTH,A_PROCESO,A_PEDIDO,TABLA_EXTERNA,PRES5,CENTRO_COSTE,VER_UON,CAMPO_ORIGEN, SAVE_VALUES,FECHA_VALOR_NO_ANT_SIS,CARGAR_REL_PROVE_ART4) "
            sConsulta = sConsulta & " SELECT " & lNuevoGrupo & sCod & sAyuda & ",TIPO,SUBTIPO,INTRO,VALOR_NUM,VALOR_TEXT,VALOR_FEC,VALOR_BOOL,MINNUM,MAXNUM,MINFEC,MAXFEC,"
            sConsulta = sConsulta & "ORDEN,ID_ATRIB_GS,TIPO_CAMPO_GS,ES_SUBCAMPO,FORMATO,GMN1,GMN2,GMN3,GMN4,LINEAS_PRECONF,ID_CALCULO,FORMULA,ORDEN_CALCULO,ORIGEN_CALC_DESGLOSE,ANYADIR_ART,CARGAR_ULT_ADJ,MAX_LENGTH,A_PROCESO,A_PEDIDO,TABLA_EXTERNA,PRES5,CENTRO_COSTE,VER_UON,ID, SAVE_VALUES,FECHA_VALOR_NO_ANT_SIS,CARGAR_REL_PROVE_ART4 "
            sConsulta = sConsulta & " FROM FORM_CAMPO WITH(NOLOCK) WHERE ID=" & rsSub.Fields("CAMPO_HIJO").Value
            m_oConexion.ADOCon.Execute sConsulta
            If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR
            'Obtiene el m�ximo de id_calculo
            sConsulta = "SELECT MAX(ID) AS ID FROM FORM_CAMPO WITH(NOLOCK) WHERE GRUPO=" & lNuevoGrupo
            rs.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
            lSubcampo = rs("ID").Value
            rs.Close
            
            'Inserta los adjuntos del campo:
            sConsulta = "INSERT INTO CAMPO_ADJUN (CAMPO,NOM,RUTA,IDIOMA,DATASIZE,DATA,DGUID,PER,COMENT,FECALTA) "
            sConsulta = sConsulta & " SELECT " & lSubcampo & ",NOM,RUTA,IDIOMA,DATASIZE,DATA,NEWID(),PER,COMENT,FECALTA"
            sConsulta = sConsulta & " FROM CAMPO_ADJUN WITH(NOLOCK) WHERE CAMPO=" & rsSub.Fields("CAMPO_HIJO").Value
            m_oConexion.ADOCon.Execute sConsulta
            If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR
            'Desglose:
            sConsulta = "INSERT INTO DESGLOSE (CAMPO_PADRE,CAMPO_HIJO) VALUES (" & lNuevoCampo & "," & lSubcampo & ")"
            m_oConexion.ADOCon.Execute sConsulta
            If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR
            'Insertar las l�neas de desglose
            sConsulta = "INSERT INTO LINEA_DESGLOSE (LINEA,CAMPO_PADRE,CAMPO_HIJO,VALOR_NUM,VALOR_FEC,VALOR_TEXT,VALOR_BOOL) "
            sConsulta = sConsulta & " SELECT LINEA," & lNuevoCampo & "," & lSubcampo & ",VALOR_NUM,VALOR_FEC,VALOR_TEXT,VALOR_BOOL"
            sConsulta = sConsulta & " FROM LINEA_DESGLOSE WITH(NOLOCK) WHERE CAMPO_PADRE=" & rsCampo.Fields("ID").Value & " AND CAMPO_HIJO=" & rsSub.Fields("CAMPO_HIJO").Value
            m_oConexion.ADOCon.Execute sConsulta
            If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR
            'Inserta los adjuntos de las l�neas de desglose:
            sConsulta = "INSERT INTO LINEA_DESGLOSE_ADJUN (CAMPO_PADRE,CAMPO_HIJO,LINEA,NOM,RUTA,IDIOMA,DATASIZE,DATA,PER,COMENT,FECALTA) "
            sConsulta = sConsulta & " SELECT " & lNuevoCampo & "," & lSubcampo & ",LINEA,NOM,RUTA,IDIOMA,DATASIZE,DATA,PER,COMENT,FECALTA"
            sConsulta = sConsulta & " FROM LINEA_DESGLOSE_ADJUN WITH(NOLOCK) WHERE CAMPO_PADRE=" & rsCampo.Fields("ID").Value & " AND CAMPO_HIJO=" & rsSub.Fields("CAMPO_HIJO").Value
            m_oConexion.ADOCon.Execute sConsulta
            If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR
            'Se a�adir� el campo en las tablas de filtros (BATCHUpdates)
            'se aceptan los campos de desglose en los escenarios
            sConsulta = "INSERT INTO UPD_VISORES_PM (ACCION,ID_TABLA,ID_CAMPO,SUBTIPO,NOM_TABLA)"
            sConsulta = sConsulta & "SELECT 'I',P.ID, C.ID, CASE WHEN C.TIPO_CAMPO_GS = " & TipoCampoGS.Almacen & " THEN " & TiposDeAtributos.TipoTextoMedio & " ELSE C.SUBTIPO END, P.NOM_TABLA + '_DESGLOSE' FROM PM_FILTROS P WITH (NOLOCK) "
            sConsulta = sConsulta & "INNER JOIN FORMULARIO F WITH (NOLOCK) ON F.ID=P.FORMULARIO "
            sConsulta = sConsulta & "INNER JOIN FORM_GRUPO G WITH (NOLOCK) ON F.ID=G.FORMULARIO "
            sConsulta = sConsulta & "INNER JOIN FORM_CAMPO C WITH (NOLOCK) ON G.ID=C.GRUPO "
            sConsulta = sConsulta & "WHERE C.SUBTIPO <> " & TiposDeAtributos.TipoDesglose & " "
            sConsulta = sConsulta & "AND (C.TIPO_CAMPO_GS <> " & TipoCampoGS.Desglose & " OR C.TIPO_CAMPO_GS IS NULL) AND C.ID  = " & lSubcampo & " AND G.BAJALOG=0 "
            m_oConexion.ADOCon.Execute sConsulta
            If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR
            ' Inserta los campos como visibles en los notificados
            InsertarCamposVisiblesNotif lSubcampo, lForm
            rsSub.MoveNext
        Wend
        rsSub.Close
        Set rsSub = Nothing
        
        If Not rsCampo.Fields("subtipo").Value = TipoEditor Then
            ' Inserta los campos como visibles en los notificados
            InsertarCamposVisiblesNotif lNuevoCampo, lForm
        End If
        rsCampo.MoveNext
    Wend
    rsCampo.Close
    Set rsCampo = Nothing
       
    'Miro qu� campo necesito actualizar en FORM_CAMPO_SERVICIO_PARAMETRO cuando sea un parametro de salida
    'con destino en un campo de la solicitud
    sConsulta = "SELECT FC.ID, FCSP.CAMPO FROM FORM_CAMPO FC WITH(NOLOCK) INNER JOIN FORM_CAMPO_SERVICIO_PARAMETRO FCSP WITH(NOLOCK) "
    sConsulta = sConsulta & " ON FC.ID=FCSP.FORM_CAMPO WHERE FC.TIPO=7 AND FCSP.CAMPO<>0 AND FC.GRUPO=" & lNuevoGrupo & " AND FC.BAJALOG=0 "
    Set rsCampoId = New adodb.Recordset
    rsCampoId.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    'Actualizo CAMPO en FORM_CAMPO_SERVICIO_PARAMETRO
    While Not rsCampoId.eof
        If lCampoAntNuevo.Exists(CStr(rsCampoId("CAMPO"))) Then
            sConsulta = "UPDATE FORM_CAMPO_SERVICIO_PARAMETRO SET CAMPO = " & lCampoAntNuevo.Item(CStr(rsCampoId("CAMPO")))
            sConsulta = sConsulta & " WHERE CAMPO<>0 AND FORM_CAMPO = " & rsCampoId("ID")
            m_oConexion.ADOCon.Execute sConsulta
            If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR
        End If
        rsCampoId.MoveNext
    Wend
    rsCampoId.Close
    
    'Actualizamos el campo CENTRO_COSTE relacionado con el nuevo ID del campo
    sConsulta = "UPDATE FORM_CAMPO SET CENTRO_COSTE = FC2.ID FROM FORM_CAMPO FD WITH (NOLOCK) "
    sConsulta = sConsulta & "INNER JOIN ( SELECT FC.ID,FC.CAMPO_ORIGEN FROM FORM_CAMPO FC WITH (NOLOCK) "
    sConsulta = sConsulta & "WHERE FC.GRUPO = " & lNuevoGrupo & " AND FC.TIPO_CAMPO_GS=129) FC2 "
    sConsulta = sConsulta & "ON FD.CENTRO_COSTE = FC2.CAMPO_ORIGEN AND FC2.CAMPO_ORIGEN IS NOT NULL "
    sConsulta = sConsulta & "WHERE FD.GRUPO = " & lNuevoGrupo & " AND FD.BAJALOG=0"
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR
    '********************************************'
    'Ahora modifica el orden del resto de grupos:
    For Each oGrupo In Me
        sConsulta = "UPDATE FORM_GRUPO SET ORDEN=" & StrToSQLNULL(oGrupo.Orden) & " WHERE ID=" & oGrupo.Id & " AND BAJALOG=0"
        m_oConexion.ADOCon.Execute sConsulta
        If m_oConexion.ADOCon.Errors.Count > 0 Then
            GoTo ERROR
        End If
        
        sConsulta = "SELECT FECACT FROM FORM_GRUPO WITH(NOLOCK) WHERE ID=" & oGrupo.Id & " AND BAJALOG=0"
        rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
        If Not rs.eof Then
            oGrupo.FecAct = rs(0).Value
        End If
        rs.Close
    Next
    
    Set rs = Nothing
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    btrans = False
    CopiarGrupo = TESError
    
    Exit Function

ERROR:
    CopiarGrupo = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    
    If btrans Then
        m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
    End If
    Set rs = Nothing
End Function


Public Function addGrupo(ByRef oGrupo As CFormGrupo, Optional ByVal vIndice As Variant) As CFormGrupo
    
    If Not IsMissing(vIndice) And Not IsNull(vIndice) Then
        oGrupo.Indice = vIndice
        mCol.Add oGrupo, CStr(vIndice)
    Else
        mCol.Add oGrupo, CStr(oGrupo.Id)
    End If
End Function

