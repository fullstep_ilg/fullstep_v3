VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CPublicacion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

Private mvarIndice As Variant 'Nos indica la posicion secuencial dentro de una Coleccion
Private mvarCodProve As String 'local copy
Private mvarDenProve As String 'local copy
Private mvarFechaActivacion As Variant
Private mvarActiva As Boolean
Private m_vFechaPublicacionObjetivo As Variant
Private m_bPublicacionObjetivoActiva As Boolean
Private m_inumObjetivos As Integer
Private m_btieneObjetivosNuevos As Boolean

Private mvarConexion As CConexion 'local copy

Public Property Get FechaActivacion() As Variant
    FechaActivacion = mvarFechaActivacion
End Property
Public Property Let FechaActivacion(ByVal Dat As Variant)
    mvarFechaActivacion = Dat
End Property

Public Property Get CodProve() As String
    CodProve = mvarCodProve
End Property
Public Property Let CodProve(ByVal Cod As String)
    mvarCodProve = Cod
End Property
Public Property Get DenProve() As String
    DenProve = mvarDenProve
End Property
Public Property Let DenProve(ByVal Den As String)
    mvarDenProve = Den
End Property
Public Property Get Activa() As Boolean
    Activa = mvarActiva
End Property
Public Property Let Activa(ByVal Cod As Boolean)
    mvarActiva = Cod
End Property
Public Property Get Indice() As Variant
    Indice = mvarIndice
End Property
Public Property Let Indice(ByVal varInd As Variant)
    mvarIndice = varInd
End Property
Friend Property Set Conexion(ByVal vData As CConexion)
    Set mvarConexion = vData
End Property
Friend Property Get Conexion() As CConexion
    Set Conexion = mvarConexion
End Property

Private Sub Class_Terminate()
    Set mvarConexion = Nothing
End Sub

Public Property Get FechaPublicacionObjetivo() As Variant
    FechaPublicacionObjetivo = m_vFechaPublicacionObjetivo
End Property

Public Property Let FechaPublicacionObjetivo(ByVal vFechaPublicacionObjetivo As Variant)
    m_vFechaPublicacionObjetivo = vFechaPublicacionObjetivo
End Property

Public Property Get PublicacionObjetivoActiva() As Boolean
    PublicacionObjetivoActiva = m_bPublicacionObjetivoActiva
End Property

Public Property Let PublicacionObjetivoActiva(ByVal bPublicacionObjetivoActiva As Boolean)
    m_bPublicacionObjetivoActiva = bPublicacionObjetivoActiva
End Property

Public Property Get numObjetivos() As Integer

    numObjetivos = m_inumObjetivos

End Property

Public Property Let numObjetivos(ByVal inumObjetivos As Integer)

    m_inumObjetivos = inumObjetivos

End Property

Public Property Get tieneObjetivosNuevos() As Boolean

    tieneObjetivosNuevos = m_btieneObjetivosNuevos

End Property

Public Property Let tieneObjetivosNuevos(ByVal btieneObjetivosNuevos As Boolean)

    m_btieneObjetivosNuevos = btieneObjetivosNuevos

End Property
