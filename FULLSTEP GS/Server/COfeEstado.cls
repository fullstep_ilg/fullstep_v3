VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "COfeEstado"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
''' *** Clase: COfeEstado
''' *** Creacion: 9/09/1998 (Javier Arana)
''' *** Ultima revision: 18/01/1999 (Alfredo Magallon)

Option Explicit

Implements IBaseDatos

''' Variables privadas con la informacion de una OfeEstado

Private mvarCod As String
Private mvarDen As String
Private mvarComp As Boolean
Private mvarAdj As Boolean
Private mvarIdioma As Variant

''' Conexion

Private mvarConexion As CConexion
Private mvarConexionServer As CConexionDeUseServer

''' Control de errores

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

''' Recordset para la OfeEstado a editar

'ado  Private mvarResultset As rdoResultset
Private mvarRecordset As adodb.Recordset

Private mvarRecordsetIdioma As adodb.Recordset


''' Indice de la OfeEstado en la coleccion

Private mvarIndice As Long
Public Property Get Indice() As Integer

    ''' * Objetivo: Devolver el indice de la OfeEstado en la coleccion
    ''' * Recibe: Nada
    ''' * Devuelve: Indice de la OfeEstado en la coleccion

    Indice = mvarIndice
    
End Property
Public Property Let Indice(ByVal iInd As Integer)

    ''' * Objetivo: Dar valor al indice de la OfeEstado en la coleccion
    ''' * Recibe: Indice de la OfeEstado en la coleccion
    ''' * Devuelve: Nada

    mvarIndice = iInd
    
End Property
Public Property Get Adjudicable() As Boolean
    Adjudicable = mvarAdj
End Property
Public Property Let Adjudicable(ByVal b As Boolean)
    mvarAdj = b
End Property
Public Property Get Comparable() As Boolean
    Comparable = mvarComp
End Property
Public Property Let Comparable(ByVal b As Boolean)
    mvarComp = b
End Property

Public Property Get Idioma() As Variant
    Idioma = mvarIdioma
End Property
Public Property Let Idioma(ByVal VarID As Variant)
    mvarIdioma = VarID
End Property

Friend Property Set Conexion(ByVal vData As CConexion)

    ''' * Objetivo: Dar valor a la conexion de la OfeEstado
    ''' * Recibe: Conexion
    ''' * Devuelve: Nada

    Set mvarConexion = vData
    
End Property
Friend Property Get Conexion() As CConexion

    ''' * Objetivo: Devolver la conexion de la OfeEstado
    ''' * Recibe: Nada
    ''' * Devuelve: Conexion

    Set Conexion = mvarConexion
    
End Property
Friend Property Set ConexionServer(ByVal con As CConexionDeUseServer)
Set mvarConexionServer = con
End Property

Friend Property Get ConexionServer() As CConexionDeUseServer
Set ConexionServer = mvarConexionServer
End Property
Public Property Let Den(ByVal vData As String)

    ''' * Objetivo: Dar valor a la variable privada DEN
    ''' * Recibe: Denominacion de la OfeEstado
    ''' * Devuelve: Nada

    mvarDen = vData
    
End Property
Public Property Get Den() As String

    ''' * Objetivo: Devolver la variable privada DEN
    ''' * Recibe: Nada
    ''' * Devuelve: Denominacion de la OfeEstado

    Den = mvarDen
    
End Property
Public Property Let Cod(ByVal vData As String)

    ''' * Objetivo: Dar valor a la variable privada COD
    ''' * Recibe: Codigo de la OfeEstado
    ''' * Devuelve: Nada

    mvarCod = vData
    
End Property
Public Property Get Cod() As String

    ''' * Objetivo: Devolver la variable privada COD
    ''' * Recibe: Nada
    ''' * Devuelve: Codigo de la OfeEstado

    Cod = mvarCod
    
End Property

Private Sub ConectarDeUseServer()
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If mvarConexionServer.ADOSummitCon Is Nothing Then
        Set mvarConexionServer.ADOSummitCon = New adodb.Connection
        mvarConexionServer.ADOSummitCon.Open "Provider=SQLOLEDB.1;Auto Translate=FALSE;Server=" & basParametros.ServidorGS & ";Database=" & basParametros.BaseDatosGS & ";", LICDBLogin, LICDBContra
        mvarConexionServer.ADOSummitCon.CursorLocation = adUseServer
        mvarConexionServer.ADOSummitCon.CommandTimeout = 120
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "COfeEstado", "ConectarDeUseServer", ERR, Erl)
      Exit Sub
   End If
End Sub

Private Sub Class_Terminate()

    ''' * Objetivo: Limpiar la memoria
    
    'ado  Set mvarResultset = Nothing
    Set mvarConexion = Nothing
    Set mvarConexionServer = Nothing
    
End Sub

''' <summary>A�ade un nuevo registro a la BD</summary>
''' <returns>Variable tipo TipoErrorSummit con el error si se ha producido</returns>
''' <remarks>Llamada desde: FSGSClient; Tiempo m�ximo: < 1seg </remarks>
''' <revision>LTG 09/01/2012</revision>

Private Function IBaseDatos_AnyadirABaseDatos() As TipoErrorSummit

    ''' * Objetivo: Anyadir la OfeEstado a la tabla
    ''' * Recibe: Nada
    ''' * Devuelve: TipoErrorSummit con el error si lo hay
    
    Dim sConsulta As String
    Dim TESError As TipoErrorSummit
    Dim mvarResultado As adodb.Recordset
    Dim fldCod As adodb.Field
    Dim bTran As Boolean
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    TESError.NumError = TESnoerror
    
    
If g_oErrores.fg_bProgramando Then On Error GoTo ERROR_cls:
                    
    ''' Ejecutar insercion
    mvarConexion.ADOCon.Execute "BEGIN TRANSACTION"
    bTran = True
    
    'OFEEST
    sConsulta = "INSERT INTO OFEEST (COD,COMP,ADJ) VALUES ('" & DblQuote(mvarCod) & "'," & BooleanToSQLBinary(mvarComp) & "," & BooleanToSQLBinary(mvarAdj) & ")"
    mvarConexion.ADOCon.Execute sConsulta
    If mvarConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR_cls
    'OFEEST_IDIOMA
    'Inserta la denominaci�n para el lenguaje de instalaci�n y para el resto lo inserta como nulo
    Set mvarResultado = New adodb.Recordset
    sConsulta = "SELECT COD FROM IDIOMAS WITH (NOLOCK)"
    If gParametrosGenerales.gbSincronizacionMat = True Then
        sConsulta = sConsulta & " WHERE APLICACION=1"
    End If
    mvarResultado.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    
    If Not mvarResultado.eof Then
        Set fldCod = mvarResultado.Fields("COD")
        While Not mvarResultado.eof
            If mvarResultado("COD") = mvarIdioma Then
                sConsulta = "INSERT INTO OFEEST_IDIOMA (COD,IDIOMA,DEN) VALUES ('" & DblQuote(mvarCod) & "','" & DblQuote(mvarIdioma) & "','" & DblQuote(mvarDen) & "')"
                mvarConexion.ADOCon.Execute sConsulta
            Else
                sConsulta = "INSERT INTO OFEEST_IDIOMA (COD,IDIOMA,DEN) VALUES ('" & DblQuote(mvarCod) & "','" & DblQuote(fldCod.Value) & "',NULL)"
                mvarConexion.ADOCon.Execute sConsulta
            End If
            If mvarConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR_cls
            mvarResultado.MoveNext
        Wend
        Set fldCod = Nothing
    End If
    mvarConexion.ADOCon.Execute "COMMIT TRANSACTION"
    bTran = False
    mvarResultado.Close
    Set mvarResultado = Nothing
    
    IBaseDatos_AnyadirABaseDatos = TESError
    
    Exit Function
    
ERROR_cls:
     
    IBaseDatos_AnyadirABaseDatos = basErrores.TratarError(mvarConexion.ADOCon.Errors)
          
    If bTran Then
        mvarConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
    End If
    If Not mvarResultado Is Nothing Then
        mvarResultado.Close
        Set mvarResultado = Nothing
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      'Call g_oErrores.TratarError("M�dulo de clase", "COfeEstado", "IBaseDatos_AnyadirABaseDatos", ERR, Erl)
      GoTo ERROR_cls
      Exit Function
   End If
End Function
Private Function IBaseDatos_CambiarCodigo(CodigoNuevo As Variant) As TipoErrorSummit

    ''' * Objetivo: Cambiar de codigo el estado
    ''' * Recibe: Codigo nuevo
    ''' * Devuelve: TipoErrorSummit con el error si lo hay
    
    'ado Dim q As rdoQuery, sql As String
    Dim TESError As TipoErrorSummit
    Dim cm As adodb.Command
    Dim par As adodb.Parameter
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    TESError.NumError = TESnoerror

        
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR_cls:
    
    ''' Preparar la SP y sus parametros
    
    'ado  sql = "{ CALL OFEEST_COD (?, ?) }"
     
    'ado  Set q = mvarConexion.rdoSummitCon.CreateQuery("CambiarCodigo", sql)
        
    'ado  q.rdoParameters(0) = Me.Cod
    'ado  q.rdoParameters(1) = CodigoNuevo
    
    Set cm = New adodb.Command
    Set cm.ActiveConnection = mvarConexion.ADOCon
    
    Set par = cm.CreateParameter("PAR1", adVarChar, adParamInput, 50, Me.Cod)
    cm.Parameters.Append par
    Set par = cm.CreateParameter("PAR2", adVarChar, adParamInput, 50, CodigoNuevo)
    cm.Parameters.Append par
    
    cm.CommandType = adCmdStoredProc
    cm.CommandText = "OFEEST_COD"
    
    ''' Ejecutar la SP
    
    'ado  q.Execute
    cm.Execute
    
    'ado  Set q = Nothing
    Set cm = Nothing
    
    mvarCod = CodigoNuevo
        
    IBaseDatos_CambiarCodigo = TESError
    
    Exit Function
    
ERROR_cls:
    
    If Not cm Is Nothing Then
         Set cm = Nothing
    End If
    
    IBaseDatos_CambiarCodigo = basErrores.TratarError(mvarConexion.ADOCon.Errors)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      'Call g_oErrores.TratarError("M�dulo de clase", "COfeEstado", "IBaseDatos_CambiarCodigo", ERR, Erl)
      GoTo ERROR_cls
      Exit Function
   End If
    
End Function
Private Sub IBaseDatos_CancelarEdicion()

    ''' * Objetivo: Cancelar la edicion, cerrando el Resultset
    
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not mvarRecordset Is Nothing Then
        mvarRecordset.Close
        Set mvarRecordset = Nothing
    End If

    If Not mvarRecordsetIdioma Is Nothing Then
        mvarRecordsetIdioma.Close
        Set mvarRecordsetIdioma = Nothing
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "COfeEstado", "IBaseDatos_CancelarEdicion", ERR, Erl)
      Exit Sub
   End If

End Sub
Private Function IBaseDatos_ComprobarExistenciaEnBaseDatos() As Boolean

    ''' ! No usado por ahora
    
End Function
Private Function IBaseDatos_EliminarDeBaseDatos() As TipoErrorSummit

    ''' * Objetivo: Eliminar la OfeEstado de la tabla
    ''' * Recibe: Nada
    ''' * Devuelve: TipoErrorSummit con el error si lo hay
    Dim bTran As Boolean
    Dim TESError As TipoErrorSummit
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    TESError.NumError = TESnoerror
    
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR_cls:
        
    ''' Borrado
    mvarConexion.ADOCon.Execute "BEGIN TRANSACTION"
    bTran = True
    'OFEEST_IDIOMA
    'Cal_idioma
    mvarConexion.ADOCon.Execute "DELETE FROM OFEEST_IDIOMA WHERE COD='" & DblQuote(mvarCod) & "'"
    If mvarConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR_cls
    'OFEEST
    mvarConexion.ADOCon.Execute "DELETE FROM OFEEST WHERE COD='" & DblQuote(mvarCod) & "'"
    If mvarConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR_cls
    
    IBaseDatos_EliminarDeBaseDatos = TESError
    mvarConexion.ADOCon.Execute "COMMIT TRANSACTION"
    bTran = False
    Exit Function
        
ERROR_cls:
        
    IBaseDatos_EliminarDeBaseDatos = basErrores.TratarError(mvarConexion.ADOCon.Errors)
    If bTran Then
        mvarConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      'Call g_oErrores.TratarError("M�dulo de clase", "COfeEstado", "IBaseDatos_EliminarDeBaseDatos", ERR, Erl)
      GoTo ERROR_cls
      Exit Function
   End If
    
End Function

Private Function IBaseDatos_FinalizarEdicionEliminando() As TipoErrorSummit

    ''' ! No usado por ahora
    
End Function
Private Function IBaseDatos_FinalizarEdicionModificando() As TipoErrorSummit

    ''' * Objetivo: Finalizar la edicion del Resultset modificando
    ''' * Recibe: Nada
    ''' * Devuelve: TipoErrorSummit con el error si lo hay

    Dim TESError As TipoErrorSummit
    Dim sConsulta As String
    Dim btrans As Boolean
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    TESError.NumError = TESnoerror

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR_cls:
    
    ''' Actualizar
    mvarConexion.ADOCon.Execute "BEGIN TRANSACTION"
    btrans = True
    'Actualiza la tabla OFEEST

    'mvarResultset("DEN") = mvarDen
    mvarRecordset("ADJ") = Abs(mvarAdj)
    mvarRecordset("COMP") = Abs(mvarComp)
    mvarRecordset.Update
    
    'Actualiza la tabla OFEEST_IDIOMA
    If mvarRecordsetIdioma.eof Then
        sConsulta = "INSERT INTO OFEEST_IDIOMA (COD,IDIOMA,DEN) VALUES ('" & DblQuote(mvarCod) & "','" & DblQuote(mvarIdioma) & "','" & DblQuote(mvarDen) & "')"
        mvarConexion.ADOCon.Execute sConsulta
        If mvarConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR_cls
    Else
        mvarRecordsetIdioma("DEN") = mvarDen
        mvarRecordsetIdioma.Update
    End If
    
    ''' Cierre
    
    mvarRecordset.Close
    Set mvarRecordset = Nothing
    
    mvarRecordsetIdioma.Close
    Set mvarRecordsetIdioma = Nothing
    mvarConexion.ADOCon.Execute "COMMIT TRANSACTION"
    btrans = False
    
    IBaseDatos_FinalizarEdicionModificando = TESError
    
    Exit Function
    
ERROR_cls:
    If mvarConexion.ADOCon.Errors.Count > 0 Then
        IBaseDatos_FinalizarEdicionModificando = basErrores.TratarError(mvarConexion.ADOCon.Errors)
    Else
        IBaseDatos_FinalizarEdicionModificando = basErrores.TratarError(mvarConexionServer.ADOSummitCon.Errors)
    End If
    If btrans Then
        mvarConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
    End If
        
    Resume Salir:

Salir:
    
    On Error Resume Next
    
    ''' Cancelar la actualizacion del recordset
    If mvarRecordset.EditMode > 0 Then
        mvarRecordset.CancelUpdate
    End If
    
    If mvarRecordsetIdioma.EditMode > 0 Then
        mvarRecordsetIdioma.CancelUpdate
    End If
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      'Call g_oErrores.TratarError("M�dulo de clase", "COfeEstado", "IBaseDatos_FinalizarEdicionModificando", ERR, Erl)
      GoTo ERROR_cls
      Exit Function
   End If

End Function

''' <summary>Abrir la edicion en la OfeEstado actual</summary>
''' <param name="Bloquear">No se usa</param>
''' <param name="UsuarioBloqueo">No se usa</param>
''' <returns>Variable tipo TipoErrorSummit con el error si se ha producido</returns>
''' <remarks>Llamada desde: FSGSClient; Tiempo m�ximo: < 1seg </remarks>
''' <revision>LTG 09/01/2012</revision>

Private Function IBaseDatos_IniciarEdicion(Optional ByVal Bloquear As Boolean, Optional ByVal UsuarioBloqueo As String) As TipoErrorSummit
    Dim TESError As TipoErrorSummit
    Dim sConsulta As String
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    TESError.NumError = TESnoerror
    
    
    ConectarDeUseServer 'Si no hay conexion de use server la abre

    ''' Abrir Resultset OFEEST
    'ado  Set mvarResultset = mvarConexion.rdoSummitCon.OpenResultset("SELECT * FROM OfeEst WHERE COD='" & DblQuote(mvarCod) & "'", rdOpenKeyset, rdConcurRowVer)
    'CALIDAD: Sin WITH (NOLOCK) porque se rellena el recordset mvarRecordset que posteriormente se utiliza para las actualizaciones
    'en otros m�todos de la clase. Si se hace la lectura con WITH (NOLOCK) no se permite la llamada al m�todo Update del recordset.
    sConsulta = "SELECT COD,COMP,ADJ,FECACT FROM OFEEST WHERE COD='" & DblQuote(mvarCod) & "'"
    Set mvarRecordset = New adodb.Recordset
    'mvarRecordset.CursorLocation = adUseServer
    mvarRecordset.Open sConsulta, mvarConexionServer.ADOSummitCon, adOpenKeyset, adLockOptimistic
    
    ''' Si no esta la OfeEstado, alguien la ha eliminado
    If mvarRecordset.eof Then
        mvarRecordset.Close
        Set mvarRecordset = Nothing
        
        TESError.NumError = TESDatoEliminado
        TESError.Arg1 = "OfeEstado"
        
        IBaseDatos_IniciarEdicion = TESError
                
        Exit Function
    End If
    
    
    ''' Si hay datos diferentes, devolver la condicion
    If mvarCod <> mvarRecordset("COD") Then
        mvarCod = mvarRecordset("COD")
        TESError.NumError = TESInfActualModificada
        TESError.Arg1 = "OfeEstado"
    End If
    'If mvarDen <> mvarResultset("DEN") Then
    '    mvarDen = mvarResultset("DEN")
    '    TESError.NumError = TESInfActualModificada
    '    TESError.Arg1 = "OfeEstado"
    'End If
    If mvarComp <> CBool(mvarRecordset("COMP")) Then
        mvarComp = mvarRecordset("COMP")
        TESError.NumError = TESInfActualModificada
        TESError.Arg1 = "OfeEstado"
    End If
    If mvarAdj <> CBool(mvarRecordset("ADJ")) Then
        mvarAdj = mvarRecordset("ADJ")
        TESError.NumError = TESInfActualModificada
        TESError.Arg1 = "OfeEstado"
    End If
    
    'OFEEST_IDIOMA

    'ado  Set mvarResulsetIdioma = mvarConexion.rdoSummitCon.OpenResultset("SELECT * FROM OFEEST_IDIOMA WHERE COD='" & DblQuote(mvarCod) & "' AND IDIOMA = '" & mvarIdioma & "'", rdOpenKeyset, rdConcurRowVer)
    'sin NOLOCK por ser un recordset en modo bloqueo
    sConsulta = "SELECT COD,IDIOMA,DEN FROM OFEEST_IDIOMA WHERE COD='" & DblQuote(mvarCod) & "' AND IDIOMA = '" & mvarIdioma & "'"
    Set mvarRecordsetIdioma = New adodb.Recordset
    'mvarRecordsetIdioma.CursorLocation = adUseServer
    mvarRecordsetIdioma.Open sConsulta, mvarConexionServer.ADOSummitCon, adOpenKeyset, adLockOptimistic
    If Not mvarRecordsetIdioma.eof Then
        If mvarDen <> mvarRecordsetIdioma("DEN") Then
            mvarDen = mvarRecordsetIdioma("DEN")
            TESError.NumError = TESInfActualModificada
            TESError.Arg1 = "OfeEstado"
        End If
    End If

    IBaseDatos_IniciarEdicion = TESError
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "COfeEstado", "IBaseDatos_IniciarEdicion", ERR, Erl)
      Exit Function
   End If

End Function
Private Function IBaseDatos_ModificarEnBaseDatos() As Boolean

    ''' ! No usado por ahora
    
End Function





