VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CNotificadosInt"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CNotificadoInt ****************************
'*             Creada : 27/01/06
'****************************************************************


Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum
Private mCol As Collection
Private m_oConexion As CConexion
Public m_bEOF As Boolean

''' <summary>Carga todos los notificados que no tengan alertas de integraci�n</summary>
''' <returns></returns>

Public Sub CargarTodasLosNotificados(ByVal erp As Variant)
Dim AdoRes As adodb.Recordset
Dim fldId As adodb.Field
Dim fldNombre As adodb.Field
Dim fldEmail As adodb.Field
Dim sConsulta As String
Dim lIndice As Long

'********* Precondicion **************************************
If m_oConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CNotificadoInt.CargarTodosLosPagos", "No se ha establecido la conexion"
    Exit Sub
End If
'*************************************************************

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

sConsulta = " SELECT ID,EMAIL,NOMBRE FROM EMAIL_INTEGRACION WITH (NOLOCK) WHERE ERP = " & StrToSQLNULL(erp) & " AND RECIBE_ALERTA_MONITORIZACION = 0 ORDER BY ID "

Set AdoRes = New adodb.Recordset
AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
      
If AdoRes.eof Then
    AdoRes.Close
    Set AdoRes = Nothing
    Exit Sub
Else
    Set mCol = Nothing
    Set mCol = New Collection
    Set fldId = AdoRes.Fields("ID")
    Set fldNombre = AdoRes.Fields("NOMBRE")
    Set fldEmail = AdoRes.Fields("EMAIL")
        
    While Not AdoRes.eof
        Me.Add fldId.Value, fldNombre.Value, fldEmail.Value
        AdoRes.MoveNext
    Wend
    
    AdoRes.Close
    Set AdoRes = Nothing
    Set fldId = Nothing
    Set fldNombre = Nothing
    Set fldEmail = Nothing
    Exit Sub
End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CNotificadosInt", "CargarTodasLosNotificados", ERR, Erl)
      Exit Sub
   End If

End Sub
Public Function Add(ByVal Id As Long, ByVal Nombre As Variant, ByVal Email As Variant) As CNotificadoInt
'create a new object
Dim objnewmember As CNotificadoInt

    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
     Set objnewmember = New CNotificadoInt
   
    objnewmember.Id = Id
    objnewmember.Nombre = Nombre
    objnewmember.Email = Email
       
    Set objnewmember.Conexion = m_oConexion
        
    'set the properties passed into the method
    mCol.Add objnewmember, CStr(Id)

    
    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CNotificadosInt", "Add", ERR, Erl)
      Exit Function
   End If

End Function
Public Property Get Item(vntIndexKey As Variant) As CNotificadoInt
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Friend Property Set Conexion(ByVal con As CConexion)
Set m_oConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = m_oConexion
End Property


Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
     Count = mCol.Count
End If


End Property


Public Sub Remove(vntIndexKey As Variant)

   
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    mCol.Remove vntIndexKey
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CNotificadosInt", "Remove", ERR, Erl)
      Exit Sub
   End If
End Sub


Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
     Set NewEnum = mCol.[_NewEnum]
End Property


Private Sub Class_Initialize()
    Set mCol = New Collection
End Sub


Private Sub Class_Terminate()
    'destroys collection when this class is terminated
    Set mCol = Nothing
    Set m_oConexion = Nothing
End Sub

Public Property Get eof() As Boolean
    eof = m_bEOF
End Property

Public Property Let eof(ByVal vNewValue As Boolean)
    m_bEOF = vNewValue
End Property


