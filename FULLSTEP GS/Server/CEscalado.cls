VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CEscalado"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CEscalado **********************************
'*             Autor : DPD
'*             Creada : 26/07/2011
'***************************************************************

Option Explicit

Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum

Private m_oConexion As CConexion

Private lId As Long
Private m_vInicial As Variant ' Dependiendo del modo contiene la cantidad inicial del rango o la cantidad directa
Private m_vFinal As Variant  ' Cantidad final del rango (s�lo en modo rango)
Private m_vPresupuesto As Variant ' Presupuesto ( s�lo para �tems, no se define en en grupos)
Private m_vObjetivo As Variant
'Variables para ofertas
Private m_vPrecio As Variant
Private m_vPrecioOferta As Variant
'Variables para atributos7
Private m_vValorNum As Variant
Private m_vValorPond As Variant

Private lIndex As Long ' �ndice para saber la posici�n que ocupa un escalado en un grupo y poder relacionarlo con otros grupos

Friend Property Set Conexion(ByVal con As CConexion)
    Set m_oConexion = con
End Property

Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property

Public Property Get Id() As Long
    Id = lId
End Property

Public Property Let Id(ByVal lvar As Long)
    lId = lvar
End Property

Public Property Get inicial() As Variant
    inicial = m_vInicial
End Property

Public Property Let inicial(ByVal vVar As Variant)
    m_vInicial = vVar
End Property

Public Property Get Final() As Variant
    Final = m_vFinal
End Property

Public Property Let Final(ByVal vVar As Variant)
    m_vFinal = vVar
End Property

Public Property Get Presupuesto() As Variant
    Presupuesto = m_vPresupuesto
End Property

Public Property Let Presupuesto(ByVal vVar As Variant)
    m_vPresupuesto = vVar
End Property

Public Property Get Objetivo() As Variant
    Objetivo = m_vObjetivo
End Property

Public Property Let Objetivo(ByVal vVar As Variant)
    m_vObjetivo = vVar
End Property

Public Property Get Precio() As Variant
    Precio = m_vPrecio
End Property

Public Property Let Precio(ByVal vVar As Variant)
    m_vPrecio = vVar
End Property

Public Property Get PrecioOferta() As Variant
    PrecioOferta = m_vPrecioOferta
End Property

Public Property Let PrecioOferta(ByVal vVar As Variant)
    m_vPrecioOferta = vVar
End Property

Public Property Let ValorNum(ByVal vData As Variant)
    m_vValorNum = vData
End Property
Public Property Get ValorNum() As Variant
    ValorNum = m_vValorNum
End Property

Public Property Let ValorPond(ByVal vData As Variant)
    m_vValorPond = vData
End Property
Public Property Get ValorPond() As Variant
    ValorPond = m_vValorPond
End Property

Public Property Get Index() As Long
    Index = lIndex
End Property

Public Property Let Index(ByVal lParamIndex As Long)
    lIndex = lParamIndex
End Property

''' <summary>Guarda los cambios realizados en el precio ofertado para un escalado</summary>
''' <param name="oLineaEnEdicion">L�nea de oferta en edici�n</param>
''' <returns>Variable de tipo TipoErrorSummit con el error si se ha producido</returns>
''' <remarks>Llamada desde: frmOFERec.sdbgPreciosEscalados_AfterColUpdate; Tiempo m�ximo:0,1</remarks>

Public Function GuardarCambiosOferta(ByRef oLineaEnEdicion As CPrecioItem) As TipoErrorSummit
    Dim TESError As TipoErrorSummit
    Dim adoComm As adodb.Command
    Dim adoParam As adodb.Parameter
    Dim rs As adodb.Recordset
    Dim oAtribO As CAtributoOfertado
    Dim oAtribEsc As CEscalado
    Dim bUsar As Boolean
    Dim sCadena As String
    Dim sConsulta As String
    Dim bTransaccionEnCurso As Boolean
    
    '********* Precondicion *******************
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CEscalado.GuardarCambiosOferta", "No se ha establecido la conexion"
    End If
    '******************************************
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

    If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:

    TESError.NumError = TESnoerror
        
    m_oConexion.ADOCon.Execute "BEGIN DISTRIBUTED TRAN"
    bTransaccionEnCurso = True
    m_oConexion.ADOCon.Execute "SET XACT_ABORT ON"
    
    Set adoComm = New adodb.Command
    Set adoComm.ActiveConnection = m_oConexion.ADOCon
    adoComm.CommandType = adCmdText
    With adoComm
        'CALIDAD: Sin WITH (NOLOCK) porque es para obtener los valores actuales
        .CommandText = "SELECT PRECIO FROM ITEM_OFEESC WHERE  ANYO=? AND GMN1=? AND PROCE=? AND ITEM=? AND PROVE=? AND OFE=? AND ESC=?"
        Set adoParam = .CreateParameter("ANYO", adSmallInt, adParamInput, , oLineaEnEdicion.Oferta.Anyo)
        .Parameters.Append adoParam
        Set adoParam = .CreateParameter("GMN1", adVarChar, adParamInput, Len(oLineaEnEdicion.Oferta.GMN1Cod), oLineaEnEdicion.Oferta.GMN1Cod)
        .Parameters.Append adoParam
        Set adoParam = .CreateParameter("PROCE", adInteger, adParamInput, , oLineaEnEdicion.Oferta.Proce)
        .Parameters.Append adoParam
        Set adoParam = .CreateParameter("ITEM", adInteger, adParamInput, , oLineaEnEdicion.Id)
        .Parameters.Append adoParam
        Set adoParam = .CreateParameter("PROVE", adVarChar, adParamInput, Len(oLineaEnEdicion.Oferta.Prove), oLineaEnEdicion.Oferta.Prove)
        .Parameters.Append adoParam
        Set adoParam = .CreateParameter("OFE", adSmallInt, adParamInput, , oLineaEnEdicion.Oferta.Num)
        .Parameters.Append adoParam
        Set adoParam = .CreateParameter("ESC", adInteger, adParamInput, , lId)
        .Parameters.Append adoParam
            
        Set rs = .Execute
    End With
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR
    
    If rs.RecordCount > 0 Then
        If IsNull(m_vPrecio) Or IsEmpty(m_vPrecio) Or Not IsNumeric(m_vPrecio) Then
            'Si no hay precio se borra el registro
            adoComm.CommandText = "DELETE FROM ITEM_OFEESC WHERE ANYO=? AND GMN1=? AND PROCE=? AND ITEM=? AND PROVE=? AND OFE=? AND ESC=?"
            adoComm.Execute
            If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR
            
            'Quitar los costes/descuentos aplicados al escalado
            If Not oLineaEnEdicion.AtribOfertados Is Nothing Then
                If oLineaEnEdicion.AtribOfertados.Count > 0 Then
                    For Each oAtribO In oLineaEnEdicion.AtribOfertados
                        If Not oAtribO.Escalados Is Nothing Then
                            If oAtribO.Escalados.Count > 0 Then
                                If Not oAtribO.Escalados.Item(CStr(lId)) Is Nothing Then
                                    oAtribO.Escalados.Item(CStr(lId)).ValorNum = Null
                                End If
                            End If
                        End If
                    Next
                End If
            End If
        Else
            'Actualizar el registro
            Set adoComm = New adodb.Command
            With adoComm
                .ActiveConnection = m_oConexion.ADOCon
                .CommandText = "UPDATE ITEM_OFEESC SET PRECIO=?,PREC_VALIDO=? WHERE ANYO=? AND GMN1=? AND PROCE=? AND ITEM=? AND PROVE=? AND OFE=? AND ESC=?"
                .CommandType = adCmdText
                Set adoParam = .CreateParameter("PRECIO", adDouble, adParamInput, , m_vPrecio)
                .Parameters.Append adoParam
                Set adoParam = .CreateParameter("PREC_VALIDO", adDouble, adParamInput, , m_vPrecio)
                .Parameters.Append adoParam
                Set adoParam = .CreateParameter("ANYO", adSmallInt, adParamInput, , oLineaEnEdicion.Oferta.Anyo)
                .Parameters.Append adoParam
                Set adoParam = .CreateParameter("GMN1", adVarChar, adParamInput, Len(oLineaEnEdicion.Oferta.GMN1Cod), oLineaEnEdicion.Oferta.GMN1Cod)
                .Parameters.Append adoParam
                Set adoParam = .CreateParameter("PROCE", adInteger, adParamInput, , oLineaEnEdicion.Oferta.Proce)
                .Parameters.Append adoParam
                Set adoParam = .CreateParameter("ITEM", adInteger, adParamInput, , oLineaEnEdicion.Id)
                .Parameters.Append adoParam
                Set adoParam = .CreateParameter("PROVE", adVarChar, adParamInput, Len(oLineaEnEdicion.Oferta.Prove), oLineaEnEdicion.Oferta.Prove)
                .Parameters.Append adoParam
                Set adoParam = .CreateParameter("OFE", adSmallInt, adParamInput, , oLineaEnEdicion.Oferta.Num)
                .Parameters.Append adoParam
                Set adoParam = .CreateParameter("ESC", adInteger, adParamInput, , lId)
                .Parameters.Append adoParam
                    
                .Execute
            End With
            If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR
        End If
    Else
        'Si el registro no existe insertarlo
        Set adoComm = New adodb.Command
        With adoComm
            .ActiveConnection = m_oConexion.ADOCon
            .CommandText = "INSERT INTO ITEM_OFEESC (ANYO,GMN1,PROCE,ITEM,PROVE,OFE,ESC,PRECIO,PREC_VALIDO) VALUES(?,?,?,?,?,?,?,?,?)"
            .CommandType = adCmdText
            Set adoParam = .CreateParameter("ANYO", adSmallInt, adParamInput, , oLineaEnEdicion.Oferta.Anyo)
            .Parameters.Append adoParam
            Set adoParam = .CreateParameter("GMN1", adVarChar, adParamInput, Len(oLineaEnEdicion.Oferta.GMN1Cod), oLineaEnEdicion.Oferta.GMN1Cod)
            .Parameters.Append adoParam
            Set adoParam = .CreateParameter("PROCE", adInteger, adParamInput, , oLineaEnEdicion.Oferta.Proce)
            .Parameters.Append adoParam
            Set adoParam = .CreateParameter("ITEM", adInteger, adParamInput, , oLineaEnEdicion.Id)
            .Parameters.Append adoParam
            Set adoParam = .CreateParameter("PROVE", adVarChar, adParamInput, Len(oLineaEnEdicion.Oferta.Prove), oLineaEnEdicion.Oferta.Prove)
            .Parameters.Append adoParam
            Set adoParam = .CreateParameter("OFE", adSmallInt, adParamInput, , oLineaEnEdicion.Oferta.Num)
            .Parameters.Append adoParam
            Set adoParam = .CreateParameter("ESC", adInteger, adParamInput, , lId)
            .Parameters.Append adoParam
            Set adoParam = .CreateParameter("PRECIO", adDouble, adParamInput, , m_vPrecio)
            .Parameters.Append adoParam
            Set adoParam = .CreateParameter("PREC_VALIDO", adDouble, adParamInput, , m_vPrecio)
            .Parameters.Append adoParam
                
            .Execute
        End With
        If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR
    End If
    
    'Actualizar el Precio de ITEM_OFE si corresponde
    'Actualizar Precio si es necesario
    bUsar = False
    If Not IsNull(oLineaEnEdicion.Cantidad) And Not IsEmpty(oLineaEnEdicion.Cantidad) And IsNumeric(oLineaEnEdicion.Cantidad) Then
        bUsar = oLineaEnEdicion.Escalados.Usar(lId, oLineaEnEdicion.Cantidad)
        If bUsar Then
            oLineaEnEdicion.Precio = m_vPrecio
        End If
    Else
        oLineaEnEdicion.Precio = Null
    End If
    If bUsar Then
        Set adoComm = New adodb.Command
        Set adoComm.ActiveConnection = m_oConexion.ADOCon
        adoComm.CommandType = adCmdText
        With adoComm
            'CALIDAD: Sin WITH (NOLOCK) porque es para obtener los valores actuales
            .CommandText = "SELECT PRECIO,IMPORTE FROM ITEM_OFE WHERE  ANYO=? AND GMN1=? AND PROCE=? AND ITEM=? AND PROVE=? AND NUM=?"
            Set adoParam = .CreateParameter("ANYO", adSmallInt, adParamInput, , oLineaEnEdicion.Oferta.Anyo)
            .Parameters.Append adoParam
            Set adoParam = .CreateParameter("GMN1", adVarChar, adParamInput, Len(oLineaEnEdicion.Oferta.GMN1Cod), oLineaEnEdicion.Oferta.GMN1Cod)
            .Parameters.Append adoParam
            Set adoParam = .CreateParameter("PROCE", adInteger, adParamInput, , oLineaEnEdicion.Oferta.Proce)
            .Parameters.Append adoParam
            Set adoParam = .CreateParameter("ITEM", adInteger, adParamInput, , oLineaEnEdicion.Id)
            .Parameters.Append adoParam
            Set adoParam = .CreateParameter("PROVE", adVarChar, adParamInput, Len(oLineaEnEdicion.Oferta.Prove), oLineaEnEdicion.Oferta.Prove)
            .Parameters.Append adoParam
            Set adoParam = .CreateParameter("NUM", adSmallInt, adParamInput, , oLineaEnEdicion.Oferta.Num)
            .Parameters.Append adoParam
                
            Set rs = .Execute
        End With
        If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR
    
        oLineaEnEdicion.PrecioUniOld = rs("PRECIO")
        oLineaEnEdicion.ImporteItemOld = rs("IMPORTE")
        
        Set adoComm = New adodb.Command
        With adoComm
            .ActiveConnection = m_oConexion.ADOCon
            .CommandText = "UPDATE ITEM_OFE SET PRECIO=? WHERE ANYO=? AND GMN1=? AND PROCE=? AND ITEM=? AND PROVE=? AND NUM=?"
            .CommandType = adCmdText
            Set adoParam = .CreateParameter("PRECIO", adDouble, adParamInput, , m_vPrecio)
            .Parameters.Append adoParam
            Set adoParam = .CreateParameter("ANYO", adSmallInt, adParamInput, , oLineaEnEdicion.Oferta.Anyo)
            .Parameters.Append adoParam
            Set adoParam = .CreateParameter("GMN1", adVarChar, adParamInput, Len(oLineaEnEdicion.Oferta.GMN1Cod), oLineaEnEdicion.Oferta.GMN1Cod)
            .Parameters.Append adoParam
            Set adoParam = .CreateParameter("PROCE", adInteger, adParamInput, , oLineaEnEdicion.Oferta.Proce)
            .Parameters.Append adoParam
            Set adoParam = .CreateParameter("ITEM", adInteger, adParamInput, , oLineaEnEdicion.Id)
            .Parameters.Append adoParam
            Set adoParam = .CreateParameter("PROVE", adVarChar, adParamInput, Len(oLineaEnEdicion.Oferta.Prove), oLineaEnEdicion.Oferta.Prove)
            .Parameters.Append adoParam
            Set adoParam = .CreateParameter("NUM", adSmallInt, adParamInput, , oLineaEnEdicion.Oferta.Num)
            .Parameters.Append adoParam
                
            .Execute
        End With
        If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR
    End If
    
    'Comprobamos si tiene un escenario de adjudicaciones guardado.Si es as� se borra:
    If oLineaEnEdicion.EliminarAdj Then
        Set adoComm = New adodb.Command
    
        With adoComm
            .CommandText = "EXEC SP_ELIMINAR_ADJUDICACIONES @ANYO=?, @GMN1=?, @PROCE=?, @USAR_ESC=? "
            Set .ActiveConnection = m_oConexion.ADOCon
            Set adoParam = .CreateParameter("ANYO", adSmallInt, adParamInput, , oLineaEnEdicion.Oferta.Anyo)
            .Parameters.Append adoParam
            Set adoParam = .CreateParameter("GMN1", adVarChar, adParamInput, 50, oLineaEnEdicion.Oferta.GMN1Cod)
            .Parameters.Append adoParam
            Set adoParam = .CreateParameter("PROCE", adInteger, adParamInput, , oLineaEnEdicion.Oferta.Proce)
            .Parameters.Append adoParam
            Set adoParam = .CreateParameter("USAR_ESC", adTinyInt, adParamInput, , 1)
            .Parameters.Append adoParam
            .CommandType = adCmdText
            
            .Execute
        End With
        If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR
        
        oLineaEnEdicion.EliminarAdj = False
    End If
    
    'Realizamos el recalculo de la oferta si fuera necesario.
    If oLineaEnEdicion.Oferta.Recalcular Then
       TESError = oLineaEnEdicion.Oferta.Proceso.RecalcularOfertas(False, , oLineaEnEdicion.Oferta.Num, oLineaEnEdicion.Oferta.Prove, oLineaEnEdicion)
       If TESError.NumError <> TESnoerror Then GoTo ERROR
       
       oLineaEnEdicion.Oferta.Recalcular = False
    End If
    
    'Registro la acci�n (cambio el precio de un item en la oferta)
    If NullToDbl0(oLineaEnEdicion.PrecioUniOld) <> NullToDbl0(m_vPrecio) Then
        sCadena = "Anyo:" & oLineaEnEdicion.Oferta.Anyo & "GMN1:" & oLineaEnEdicion.Oferta.GMN1Cod & "Cod:" & oLineaEnEdicion.Oferta.Proce
        sCadena = sCadena & "PrecioAnterior:" & NullToStr(oLineaEnEdicion.PrecioUniOld) & "PrecioNuevo:" & NullToStr(m_vPrecio)
        If gParametrosGenerales.gbACTIVLOG Then
            sConsulta = "INSERT INTO LOG (USU,ACC,DAT,FEC) VALUES ('" & DblQuote(oLineaEnEdicion.Oferta.Usuario.Cod) & "'," & ACCCondOfePrecio & ",'" & DblQuote(sCadena) & "'," & DateToSQLTimeDate(Now) & ")"
            m_oConexion.ADOCon.Execute sConsulta
        End If
    End If
        
    m_oConexion.ADOCon.Execute "SET XACT_ABORT OFF"
    m_oConexion.ADOCon.Execute "COMMIT TRAN"
    bTransaccionEnCurso = False
    
    GuardarCambiosOferta = TESError
    
    If oLineaEnEdicion.EliminarAdj Then
            'Actualizar campos pm
            Dim oProceso As cProceso
            Set oProceso = New cProceso
            Set oProceso.Conexion = Me.Conexion
            oProceso.Anyo = oLineaEnEdicion.Oferta.Anyo
            oProceso.GMN1Cod = oLineaEnEdicion.Oferta.GMN1Cod
            oProceso.Cod = oLineaEnEdicion.Oferta.Proce
            oProceso.ActualizarCamposPM
            Set oProceso = Nothing
    End If

Salir:
    Set adoComm = Nothing
    Set adoParam = Nothing
    Set rs = Nothing
    Set oAtribO = Nothing
    Set oAtribEsc = Nothing
    Exit Function
Error_Cls:
    GuardarCambiosOferta = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    If bTransaccionEnCurso Then
        m_oConexion.ADOCon.Execute "ROLLBACK TRAN"
        bTransaccionEnCurso = False
    End If
    m_oConexion.ADOCon.Execute "SET XACT_ABORT OFF"
    Resume Salir
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Or m_oConexion.ADOCon.Errors.Count > 0 Then
      'Call g_oErrores.TratarError("M�dulo de clase", "CEscalado", "GuardarCambiosOferta", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If
End Function

''' <summary>Guarda los cambios realizados en los costes/descuentos aplicados al precio del escalado de una oferta</summary>
''' <param name="oLineaEnEdicion">L�nea de oferta en edici�n</param>
''' <param name="lIdAtrib">Id. atributo</param>
''' <returns>Variable de tipo TipoErrorSummit con el error si se ha producido</returns>
''' <remarks>Llamada desde: frmOFERec.sdbgPreciosEscalados_AfterColUpdate; Tiempo m�ximo:0,1</remarks>

Public Function GuardarCambiosOfertaCosteDescuentoAplicado(ByRef oLineaEnEdicion As CPrecioItem, ByVal lIdAtrib As Long) As TipoErrorSummit
    Dim TESError As TipoErrorSummit
    Dim adoComm As adodb.Command
    Dim adoParam As adodb.Parameter
    Dim rs As adodb.Recordset
    Dim bUsar As Boolean
    Dim sCadena As String
    Dim sCod As String
    Dim sConsulta As String
    Dim vValor As Variant
    Dim vBruto As Variant
    Dim oAtributos As CAtributos
    Dim oAtributo As CAtributo
    
    '********* Precondicion *******************
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CEscalado.GuardarCambiosOferta", "No se ha establecido la conexion"
    End If
    '******************************************
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

    If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:

    TESError.NumError = TESnoerror
        
    m_oConexion.ADOCon.Execute "BEGIN DISTRIBUTED TRAN"
    m_oConexion.ADOCon.Execute "SET XACT_ABORT ON"

    Set adoComm = New adodb.Command
    Set adoComm.ActiveConnection = m_oConexion.ADOCon
    adoComm.CommandType = adCmdText
    With adoComm
        'CALIDAD: Sin WITH (NOLOCK) porque es para obtener los valores actuales
        .CommandText = "SELECT ESC FROM OFE_ITEM_ATRIBESC WHERE  ANYO=? AND GMN1=? AND PROCE=? AND ITEM=? AND PROVE=? AND OFE=? AND ESC=? AND ATRIB_ID=?"
        Set adoParam = .CreateParameter("ANYO", adSmallInt, adParamInput, , oLineaEnEdicion.Oferta.Anyo)
        .Parameters.Append adoParam
        Set adoParam = .CreateParameter("GMN1", adVarChar, adParamInput, Len(oLineaEnEdicion.Oferta.GMN1Cod), oLineaEnEdicion.Oferta.GMN1Cod)
        .Parameters.Append adoParam
        Set adoParam = .CreateParameter("PROCE", adInteger, adParamInput, , oLineaEnEdicion.Oferta.Proce)
        .Parameters.Append adoParam
        Set adoParam = .CreateParameter("ITEM", adInteger, adParamInput, , oLineaEnEdicion.Id)
        .Parameters.Append adoParam
        Set adoParam = .CreateParameter("PROVE", adVarChar, adParamInput, Len(oLineaEnEdicion.Oferta.Prove), oLineaEnEdicion.Oferta.Prove)
        .Parameters.Append adoParam
        Set adoParam = .CreateParameter("OFE", adSmallInt, adParamInput, , oLineaEnEdicion.Oferta.Num)
        .Parameters.Append adoParam
        Set adoParam = .CreateParameter("ESC", adInteger, adParamInput, , lId)
        .Parameters.Append adoParam
        Set adoParam = .CreateParameter("ATRIB_ID", adInteger, adParamInput, , lIdAtrib)
        .Parameters.Append adoParam
            
        Set rs = .Execute
    End With
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR

    If rs.RecordCount > 0 Then
        If IsNull(m_vValorNum) Or IsEmpty(m_vValorNum) Or Not IsNumeric(m_vValorNum) Then
            adoComm.CommandText = "DELETE FROM OFE_ITEM_ATRIBESC WHERE  ANYO=? AND GMN1=? AND PROCE=? AND ITEM=? AND PROVE=? AND OFE=? AND ESC=? AND ATRIB_ID=?"
            adoComm.Execute
            
            If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR
        Else
            Set adoComm = New adodb.Command
            Set adoComm.ActiveConnection = m_oConexion.ADOCon
            adoComm.CommandType = adCmdText
            With adoComm
                .CommandText = "UPDATE OFE_ITEM_ATRIBESC SET VALOR_NUM=? WHERE  ANYO=? AND GMN1=? AND PROCE=? AND ITEM=? AND PROVE=? AND OFE=? AND ESC=? AND ATRIB_ID=?"
                Set adoParam = .CreateParameter("VALOR_NUM", adDouble, adParamInput, , m_vValorNum)
                .Parameters.Append adoParam
                Set adoParam = .CreateParameter("ANYO", adSmallInt, adParamInput, , oLineaEnEdicion.Oferta.Anyo)
                .Parameters.Append adoParam
                Set adoParam = .CreateParameter("GMN1", adVarChar, adParamInput, Len(oLineaEnEdicion.Oferta.GMN1Cod), oLineaEnEdicion.Oferta.GMN1Cod)
                .Parameters.Append adoParam
                Set adoParam = .CreateParameter("PROCE", adInteger, adParamInput, , oLineaEnEdicion.Oferta.Proce)
                .Parameters.Append adoParam
                Set adoParam = .CreateParameter("ITEM", adInteger, adParamInput, , oLineaEnEdicion.Id)
                .Parameters.Append adoParam
                Set adoParam = .CreateParameter("PROVE", adVarChar, adParamInput, Len(oLineaEnEdicion.Oferta.Prove), oLineaEnEdicion.Oferta.Prove)
                .Parameters.Append adoParam
                Set adoParam = .CreateParameter("OFE", adSmallInt, adParamInput, , oLineaEnEdicion.Oferta.Num)
                .Parameters.Append adoParam
                Set adoParam = .CreateParameter("ESC", adInteger, adParamInput, , lId)
                .Parameters.Append adoParam
                Set adoParam = .CreateParameter("ATRIB_ID", adInteger, adParamInput, , lIdAtrib)
                .Parameters.Append adoParam
                    
                Set rs = .Execute
            End With
            If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR
        End If
    Else
        If Not IsNull(m_vValorNum) And Not IsEmpty(m_vValorNum) And Not IsMissing(m_vValorNum) And m_vValorNum <> "" Then
            Set adoComm = New adodb.Command
            Set adoComm.ActiveConnection = m_oConexion.ADOCon
            adoComm.CommandType = adCmdText
            With adoComm
                .CommandText = "INSERT INTO OFE_ITEM_ATRIBESC (ANYO,GMN1,PROCE,ITEM,PROVE,OFE,ATRIB_ID,ESC,VALOR_NUM) VALUES (?,?,?,?,?,?,?,?,?)"
                Set adoParam = .CreateParameter("ANYO", adSmallInt, adParamInput, , oLineaEnEdicion.Oferta.Anyo)
                .Parameters.Append adoParam
                Set adoParam = .CreateParameter("GMN1", adVarChar, adParamInput, Len(oLineaEnEdicion.Oferta.GMN1Cod), oLineaEnEdicion.Oferta.GMN1Cod)
                .Parameters.Append adoParam
                Set adoParam = .CreateParameter("PROCE", adInteger, adParamInput, , oLineaEnEdicion.Oferta.Proce)
                .Parameters.Append adoParam
                Set adoParam = .CreateParameter("ITEM", adInteger, adParamInput, , oLineaEnEdicion.Id)
                .Parameters.Append adoParam
                Set adoParam = .CreateParameter("PROVE", adVarChar, adParamInput, Len(oLineaEnEdicion.Oferta.Prove), oLineaEnEdicion.Oferta.Prove)
                .Parameters.Append adoParam
                Set adoParam = .CreateParameter("OFE", adSmallInt, adParamInput, , oLineaEnEdicion.Oferta.Num)
                .Parameters.Append adoParam
                Set adoParam = .CreateParameter("ATRIB_ID", adInteger, adParamInput, , lIdAtrib)
                .Parameters.Append adoParam
                Set adoParam = .CreateParameter("ESC", adInteger, adParamInput, , lId)
                .Parameters.Append adoParam
                Set adoParam = .CreateParameter("VALOR_NUM", adDouble, adParamInput, , m_vValorNum)
                .Parameters.Append adoParam
                    
                Set rs = .Execute
            End With
            If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR
        End If
    End If
    
    bUsar = False
    If Not IsNull(oLineaEnEdicion.Cantidad) And Not IsEmpty(oLineaEnEdicion.Cantidad) And IsNumeric(oLineaEnEdicion.Cantidad) Then
        sCod = CStr(oLineaEnEdicion.Id) & "$" & CStr(lIdAtrib)
        
        If oLineaEnEdicion.Cantidad > 0 Then
            bUsar = oLineaEnEdicion.AtribOfertados.Item(sCod).Escalados.Usar(lId, oLineaEnEdicion.Cantidad)
            If bUsar Then
                sCod = CStr(oLineaEnEdicion.Id & "$" & CStr(lIdAtrib))
                oLineaEnEdicion.AtribOfertados.Item(sCod).ValorNum = m_vValorNum
                TESError = oLineaEnEdicion.AtribOfertados.Item(sCod).GuardarCambiosOfertaAtributo(oLineaEnEdicion.Id, True)
                
                'Se obtienen los valores antiguos de ITEM_OFE
                Set adoComm = New adodb.Command
                Set adoComm.ActiveConnection = m_oConexion.ADOCon
                adoComm.CommandType = adCmdText
                With adoComm
                    'CALIDAD: Sin WITH (NOLOCK) porque es para obtener los valores actuales
                    .CommandText = "SELECT PRECIO,IMPORTE FROM ITEM_OFE WHERE  ANYO=? AND GMN1=? AND PROCE=? AND ITEM=? AND PROVE=? AND NUM=?"
                    Set adoParam = .CreateParameter("ANYO", adSmallInt, adParamInput, , oLineaEnEdicion.Oferta.Anyo)
                    .Parameters.Append adoParam
                    Set adoParam = .CreateParameter("GMN1", adVarChar, adParamInput, Len(oLineaEnEdicion.Oferta.GMN1Cod), oLineaEnEdicion.Oferta.GMN1Cod)
                    .Parameters.Append adoParam
                    Set adoParam = .CreateParameter("PROCE", adInteger, adParamInput, , oLineaEnEdicion.Oferta.Proce)
                    .Parameters.Append adoParam
                    Set adoParam = .CreateParameter("ITEM", adInteger, adParamInput, , oLineaEnEdicion.Id)
                    .Parameters.Append adoParam
                    Set adoParam = .CreateParameter("PROVE", adVarChar, adParamInput, Len(oLineaEnEdicion.Oferta.Prove), oLineaEnEdicion.Oferta.Prove)
                    .Parameters.Append adoParam
                    Set adoParam = .CreateParameter("NUM", adSmallInt, adParamInput, , oLineaEnEdicion.Oferta.Num)
                    .Parameters.Append adoParam
                        
                    Set rs = .Execute
                End With
                If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR
            
                oLineaEnEdicion.PrecioUniOld = rs("PRECIO")
                oLineaEnEdicion.ImporteItemOld = rs("IMPORTE")
                oLineaEnEdicion.AtribOfertados.Item(sCod).ValorNum = m_vValorNum
            End If
        Else
            oLineaEnEdicion.AtribOfertados.Item(sCod).ValorNum = Null
        End If
    End If
    
    'Comprobamos si tiene un escenario de adjudicaciones guardado.Si es as� se borra:
    If oLineaEnEdicion.EliminarAdj Then
        Set adoComm = New adodb.Command
    
        With adoComm
            .CommandText = "EXEC SP_ELIMINAR_ADJUDICACIONES @ANYO=?, @GMN1=?, @PROCE=?, @USAR_ESC=? "
            Set .ActiveConnection = m_oConexion.ADOCon
            Set adoParam = .CreateParameter("ANYO", adSmallInt, adParamInput, , oLineaEnEdicion.Oferta.Anyo)
            .Parameters.Append adoParam
            Set adoParam = .CreateParameter("GMN1", adVarChar, adParamInput, 50, oLineaEnEdicion.Oferta.GMN1Cod)
            .Parameters.Append adoParam
            Set adoParam = .CreateParameter("PROCE", adInteger, adParamInput, , oLineaEnEdicion.Oferta.Proce)
            .Parameters.Append adoParam
            Set adoParam = .CreateParameter("USAR_ESC", adTinyInt, adParamInput, , 1)
            .Parameters.Append adoParam
            .CommandType = adCmdText
            
            .Execute
        End With
        If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR
        
        oLineaEnEdicion.EliminarAdj = False
    End If
    
    'Realizamos el recalculo de la oferta si fuera necesario
    'Ser� necesario si se ha modificado el valor del atributo que se corresponde con el escalado que se aplica
    If oLineaEnEdicion.Oferta.Recalcular Then
       TESError = oLineaEnEdicion.Oferta.Proceso.RecalcularOfertas(False, , oLineaEnEdicion.Oferta.Num, oLineaEnEdicion.Oferta.Prove, oLineaEnEdicion)
       If TESError.NumError <> TESnoerror Then GoTo ERROR
       
       oLineaEnEdicion.Oferta.Recalcular = False
    Else
        'Puede que, aunque no se corresponda con el escalado que se aplica, sea un atributo que se aplica al precio. En ese caso hay que
        'recalcular el PREC_VALIDO
        'Actualizar los valores de ITEM_OFEESC
                
        'Aplicamos atributo
        If Not IsNull(m_vValorNum) And IsNumeric(m_vValorNum) Then
            'Cargar los atributos que se aplican al precio (USAR_PREC=1). Ya vienen ordenados por orden de aplicaci�n
            Set oAtributos = oLineaEnEdicion.Oferta.Proceso.CargarAtributosOfertaAplicadosOrden
            If Not oAtributos Is Nothing Then
                If oAtributos.Count > 0 Then
                    'Comprobar si el atributo se aplica, si no, no hay que hacer nada
                    If Not oAtributos.Item(CStr(lIdAtrib)) Is Nothing Then
                        vBruto = oLineaEnEdicion.Escalados.Item(CStr(lId)).Precio
                        
                        For Each oAtributo In oAtributos
                            If Not IsNull(vBruto) Then vBruto = oLineaEnEdicion.Oferta.Proceso.AplicamosAtributo(oAtributo.PrecioFormula, vBruto, m_vValorNum)
                        Next
                        Set oAtributo = Nothing
                        
                        oLineaEnEdicion.Escalados.Item(CStr(lId)).PrecioOferta = vBruto
                        TESError = oLineaEnEdicion.Oferta.Proceso.RecalculoActualizarItemEscalados(oLineaEnEdicion.Oferta.Prove, oLineaEnEdicion.Oferta.Num, oLineaEnEdicion.Id, lId, vBruto)
                        If TESError.NumError <> TESnoerror Then GoTo ERROR
                    End If
                    Set oAtributos = Nothing
                End If
            End If
        End If
    End If
    
    'Registro la acci�n
    If NullToDbl0(m_vPrecio) <> NullToDbl0(m_vPrecio) Then
        sCadena = "Anyo:" & oLineaEnEdicion.Oferta.Anyo & "GMN1:" & oLineaEnEdicion.Oferta.GMN1Cod & "Cod:" & oLineaEnEdicion.Oferta.Proce
        sCadena = sCadena & "ValorNuevo:" & NullToStr(m_vValorNum)
        If gParametrosGenerales.gbACTIVLOG Then
            sConsulta = "INSERT INTO LOG (USU,ACC,DAT,FEC) VALUES ('" & DblQuote(oLineaEnEdicion.Oferta.Usuario.Cod) & "'," & ACCRecOfeAtribMod & ",'" & DblQuote(sCadena) & "'," & DateToSQLTimeDate(Now) & ")"
            m_oConexion.ADOCon.Execute sConsulta
        End If
    End If
    
    m_oConexion.ADOCon.Execute "SET XACT_ABORT OFF"
    m_oConexion.ADOCon.Execute "COMMIT TRAN"
    
    GuardarCambiosOfertaCosteDescuentoAplicado = TESError

Salir:
    Set adoComm = Nothing
    Set adoParam = Nothing
    Set rs = Nothing
    Exit Function
Error_Cls:
    GuardarCambiosOfertaCosteDescuentoAplicado = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    m_oConexion.ADOCon.Execute "ROLLBACK TRAN"
    m_oConexion.ADOCon.Execute "SET XACT_ABORT OFF"
    Resume Salir
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Or m_oConexion.ADOCon.Errors.Count > 0 Then
      'Call g_oErrores.TratarError("M�dulo de clase", "CEscalado", "GuardarCambiosOfertaCosteDescuentoAplicado", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If
End Function

