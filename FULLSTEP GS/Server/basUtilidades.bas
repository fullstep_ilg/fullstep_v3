Attribute VB_Name = "basUtilidades"
Private Declare Function RegOpenKeyEx Lib "advapi32.dll" Alias "RegOpenKeyExA" (ByVal hKey As Long, ByVal lpSubKey As String, ByVal ulOptions As Long, ByVal samDesired As Long, phkResult As Long) As Long
Private Declare Function RegQueryValueEx Lib "advapi32.dll" Alias "RegQueryValueExA" (ByVal hKey As Long, ByVal lpValueName As String, ByVal lpReserved As Long, lpType As Long, lpData As Any, lpcbData As Long) As Long
Private Declare Function RegQueryValueExString Lib "advapi32.dll" Alias "RegQueryValueExA" (ByVal hKey As Long, ByVal lpValueName As String, ByVal lpReserved As Long, lpType As Long, ByVal lpData As String, lpcbData As Long) As Long
Private Declare Function RegQueryValueExLong Lib "advapi32.dll" Alias "RegQueryValueExA" (ByVal hKey As Long, ByVal lpValueName As String, ByVal lpReserved As Long, lpType As Long, lpData As Long, lpcbData As Long) As Long
Private Declare Function RegCloseKey Lib "advapi32.dll" (ByVal hKey As Long) As Long
Private Declare Function RegQueryValueExNULL Lib "advapi32.dll" Alias "RegQueryValueExA" (ByVal hKey As Long, ByVal lpValueName As String, ByVal lpReserved As Long, lpType As Long, ByVal lpData As Long, lpcbData As Long) As Long
Public Const KEY_ALL_ACCESS = &H3F
Public Const HKEY_CURRENT_USER = &H80000001
Public Const ERROR_NONE = 0
Public Const REG_SZ As Long = 1
Public Const REG_DWORD As Long = 4

Public Const ADS_SECURE_AUTHENTICATION  As Long = 1

Private oConexion As CConexion

Public Const ClaveSQLtod = "aldj�w"
Public Const ClaveOBJtod = "jldfad"
Public Const ClaveADMpar = "pruiop"
Public Const ClaveADMimp = "ljkdag"
Public Const ClaveUSUpar = "agkag�"
Public Const ClaveUSUimp = "h+hlL_"
Public Property Set Conexion(ByVal vData As CConexion)
    Set oConexion = vData
End Property


''' <summary>
''' Extrae el login del fichero FSDB.LIC y lo desencripta.
''' </summary>
''' <returns>La login desencriptada</returns>
''' <remarks>Llamada desde: 60-veces(En todas las cargas MSDataShape y calses q tengan una funcion "ConectarDeUseServer", pq ejecutan, m_oConexionServer.ADOSummitCon.Open)
''' ; Tiempo m�ximo: 0,1</remarks>
Function LICDBLogin() As String

    Dim fso As New FileSystemObject, fil As File, ts As TextStream
    Dim FechaHora As String
    Dim tmplogin As String
    Dim tmpcontra As String

    Set fil = fso.GetFile(App.Path & "\FSDB.LIC")
    Set ts = fil.OpenAsTextStream(ForReading)
    
    FechaHora = Format(ts.ReadLine, "DD\/MM\/YYYY HH\:NN\:SS")

    tmplogin = ts.ReadLine
    tmpcontra = ts.ReadLine
    ts.Close
        
    ''' Desencriptar login
    LICDBLogin = EncriptarAES("", tmplogin, False, FechaHora, 0)
    
End Function
Function DesactivarConexion(ByVal X As Integer) As Integer

    DesactivarConexion = REG_SZ * X
    'Desactivada
    
End Function

Public Sub ReadByteArray(ByVal strPath As String, ByRef arrData() As Byte)

    Dim lngFile As Long

    ' open the file
    lngFile = FreeFile
    Open strPath For Binary Access Read As lngFile

    ' allocate enough memory to read file in one go
    ReDim arrData(1 To LOF(lngFile)) As Byte

    ' read blob
    Get lngFile, , arrData
    
    ' close file
    Close lngFile
   
End Sub

Public Sub WriteByteArray(ByVal strPath As String, ByRef arrData() As Byte)

        Dim lngFile As Long
       
        ' open the file
        lngFile = FreeFile()
        Open strPath For Binary Access Write As lngFile
       
        ' write blob
        Put lngFile, , arrData
       
        ' close file
        Close lngFile

End Sub

Public Function EncodeBase64(ByRef arrData() As Byte) As String

    Dim objXML As Object
    Dim objNode As Object
    
    Set objXML = CreateObject("MSXML2.DOMDocument")
   
    ' byte array to base64
    Set objNode = objXML.createElement("b64")
    objNode.DataType = "bin.base64"
    objNode.nodeTypedValue = arrData
    EncodeBase64 = objNode.Text

 

    ' thanks, bye
    Set objNode = Nothing
    Set objXML = Nothing


End Function

Public Function DecodeBase64(ByVal strData As String) As Byte()

    Dim objXML As Object
    Dim objNode As Object
   
    ' help from MSXML
    Set objXML = CreateObject("MSXML2.DOMDocument")
    Set objNode = objXML.createElement("b64")
    objNode.DataType = "bin.base64"
    objNode.Text = strData
    DecodeBase64 = objNode.nodeTypedValue
   
    ' thanks, bye
    Set objNode = Nothing
    Set objXML = Nothing

End Function

''' <summary>
''' Extrae la contrase�a del fichero FSDB.LIC y la desencripta.
''' </summary>
''' <returns>La contrase�a desencriptada</returns>
''' <remarks>Llamada desde: 60-veces(En todas las cargas de objetos MSDataShape y clases q tengan una funcion "ConectarDeUseServer", pq ejecutan, m_oConexionServer.ADOSummitCon.Open)
''' ; Tiempo m�ximo: 0,1</remarks>
Function LICDBContra() As String

    Dim fso As New FileSystemObject, fil As File, ts As TextStream
    Dim FechaHora As String
    Dim tmplogin As String
    Dim tmpcontra As String

    Set fil = fso.GetFile(App.Path & "\FSDB.LIC")
    Set ts = fil.OpenAsTextStream(ForReading)
    
    FechaHora = Format(ts.ReadLine, "DD\/MM\/YYYY HH\:NN\:SS")
    
    tmplogin = ts.ReadLine
    tmpcontra = ts.ReadLine
    ts.Close
        
    ''' Desencriptar login
    LICDBContra = EncriptarAES("", tmpcontra, False, FechaHora, 0)

End Function
Private Function QueryValueEx(ByVal lHkey As Long, ByVal szValueName As String, vValue As Variant) As Long
Dim cch As Long
Dim lrc As Long
Dim lType As Long
Dim lValue As Long
Dim sValue As String
On Error GoTo QueryValueExError
          
    ' Determine the size and type of data to be read
    lrc = RegQueryValueExNULL(lHkey, szValueName, 0&, lType, 0&, cch)
    If lrc <> ERROR_NONE Then Error 5
    Select Case lType
              ' For strings
        Case REG_SZ:
                sValue = String(cch, 0)
                lrc = RegQueryValueExString(lHkey, szValueName, 0&, lType, sValue, cch)
                If lrc = ERROR_NONE Then
                    vValue = Left$(sValue, cch - 1)
                Else
                    vValue = Empty
                End If
              ' For DWORDS
        Case REG_DWORD:
                lrc = RegQueryValueExLong(lHkey, szValueName, 0&, lType, lValue, cch)
                If lrc = ERROR_NONE Then vValue = lValue
        Case Else                  'all other data types not supported
                lrc = -1
    End Select
    
QueryValueExExit:
QueryValueEx = lrc
Exit Function
QueryValueExError:
          Resume QueryValueExExit

End Function
Public Function QueryValue(sKeyName As String, sValueName As String) As Variant
Dim lRetVal As Long         'result of the API functions
Dim hKey As Long         'handle of opened key
Dim vValue As Variant      'setting of queried value
    
    lRetVal = RegOpenKeyEx(HKEY_CURRENT_USER, sKeyName, 0, KEY_ALL_ACCESS, hKey)
    lRetVal = QueryValueEx(hKey, sValueName, vValue)
    RegCloseKey (hKey)
    QueryValue = vValue
    
End Function

Function StrToSQLNULL(StrThatCanBeEmpty) As Variant
    
    If IsEmpty(StrThatCanBeEmpty) Then
        StrToSQLNULL = "NULL"
    Else
        If IsNull(StrThatCanBeEmpty) Then
            StrToSQLNULL = "NULL"
        Else
            If StrThatCanBeEmpty = "" Then
                StrToSQLNULL = "NULL"
            Else
                StrToSQLNULL = "'" & DblQuote(CStr(StrThatCanBeEmpty)) & "'"
            End If
        End If
    End If
    
End Function

''' <summary>
''' Devuelve un valor preparado para unicode
''' </summary>
''' <param name="StrThatCanBeEmpty">Valor que puede ser nulo</param>
''' <returns>Devuelve un valor preparado para unicode</returns>
''' <remarks>Llamada desde: Muchos sitios de la aplicacion
'''         Tiempo m�ximo: 0 seg </remarks>
Function StrToSQLNULLNVar(StrThatCanBeEmpty) As Variant
    
    If IsEmpty(StrThatCanBeEmpty) Then
        StrToSQLNULLNVar = "NULL"
    Else
        If IsNull(StrThatCanBeEmpty) Then
            StrToSQLNULLNVar = "NULL"
        Else
            If StrThatCanBeEmpty = "" Then
                StrToSQLNULLNVar = "NULL"
            Else
                StrToSQLNULLNVar = "N'" & DblQuote(CStr(StrThatCanBeEmpty)) & "'"
            End If
        End If
    End If
    
End Function

Function DateToSQLDate(DateThatCanBeEmpty) As Variant


If IsEmpty(DateThatCanBeEmpty) Then
    DateToSQLDate = "NULL"
Else
    If IsNull(DateThatCanBeEmpty) Then
        DateToSQLDate = "NULL"
    Else
        If DateThatCanBeEmpty = "" Then
            DateToSQLDate = "NULL"
        Else
            DateToSQLDate = "Convert(datetime,'" & Format(DateThatCanBeEmpty, "mm-dd-yyyy") & "',110)"
        End If
    End If
End If

End Function
Function DateToSQLTimeDate(DateThatCanBeEmpty) As Variant


If IsEmpty(DateThatCanBeEmpty) Then
    DateToSQLTimeDate = "NULL"
Else
    If IsNull(DateThatCanBeEmpty) Then
        DateToSQLTimeDate = "NULL"
    Else
        If DateThatCanBeEmpty = "" Then
            DateToSQLTimeDate = "NULL"
        Else
            DateToSQLTimeDate = "Convert(datetime,'" & Format(DateThatCanBeEmpty, "mm-dd-yyyy hh:mm:ss") & "',110)"
        End If
    End If
End If

End Function
Function StrToVbNULL(StrThatCanBeEmpty) As Variant
    
    If IsEmpty(StrThatCanBeEmpty) Then
        StrToVbNULL = Null
    Else
        If IsNull(StrThatCanBeEmpty) Then
            StrToVbNULL = Null
        Else
            If StrThatCanBeEmpty = "" Then
                StrToVbNULL = Null
            Else
                StrToVbNULL = StrThatCanBeEmpty
            End If
        End If
    End If
    
End Function
''' <summary>
''' Recibe un string q puede contener o no un double. De contenerlo lo devuelve sino devuelve cero.
''' </summary>
''' <param name="str">string vacio o q representa un double</param>
''' <returns>Un double</returns>
''' <remarks>Llamada desde: CTraslados.TrasladoProvesRelacionados ; Tiempo m�ximo: 0</remarks>
Public Function StrToDbl0(ByVal str As String) As Variant
    If str = "" Then
        StrToDbl0 = 0
    Else
        If IsNull(str) Then
            StrToDbl0 = 0
        Else
            StrToDbl0 = CDbl(str)
        End If
    End If
End Function
Public Function NullToStr(ByVal ValueThatCanBeNull As Variant) As String
  
    If IsNull(ValueThatCanBeNull) Then
        NullToStr = ""
    Else
        NullToStr = ValueThatCanBeNull
    End If

End Function
Public Function NullToTrim(ByVal ValueThatCanBeNull As Variant) As String
  
    If IsNull(ValueThatCanBeNull) Then
        NullToTrim = ""
    Else
        NullToTrim = Trim(ValueThatCanBeNull)
    End If

End Function
Public Function NullToDbl0(ByVal ValueThatCanBeNull As Variant) As Variant
  
    If IsNull(ValueThatCanBeNull) Then
        NullToDbl0 = 0
    Else
        NullToDbl0 = ValueThatCanBeNull
    End If

End Function

Public Function Dbl0ToSQLNull(ByVal ValueThatCanBeZero As Variant) As Variant
  
    If ValueThatCanBeZero = 0 Then
        Dbl0ToSQLNull = "Null"
    Else
        Dbl0ToSQLNull = ValueThatCanBeZero
    End If

End Function

Public Function DblQuote(ByVal StrToDblQuote As String) As String

    Dim Ind As Long
    Dim QPos As Long
    Dim pos As Long
    
    Dim TmpDblQuote As String
    
    TmpDblQuote = ""
    pos = 1
    
    Do While Len(StrToDblQuote) > 0
    
        QPos = InStr(pos, StrToDblQuote, "'")
        
        If QPos > 0 Then
            
            TmpDblQuote = TmpDblQuote & Mid(StrToDblQuote, pos, QPos - 1) & "''"
            StrToDblQuote = Right(StrToDblQuote, Len(StrToDblQuote) - QPos)
            
        Else
        
            TmpDblQuote = TmpDblQuote & StrToDblQuote
            Exit Do
            
        End If
            
    Loop
    
    DblQuote = TmpDblQuote
    
End Function

Public Function DblToSQLFloat(DblToConvert) As String

    Dim Ind As Long
    Dim SeekPos As Long
    Dim pos As Long
    
    Dim TmpFloat As String
    Dim StrToConvert As String
    
    Dim sDecimal As String
    Dim sThousand As String
        
    If IsEmpty(DblToConvert) Then
        DblToSQLFloat = "NULL"
        Exit Function
    End If
    
    If IsNull(DblToConvert) Then
        DblToSQLFloat = "NULL"
        Exit Function
    End If
    
    If DblToConvert = "" Then
        DblToSQLFloat = "NULL"
        Exit Function
    End If
    
    StrToConvert = CStr(DblToConvert)
    
    If basParametros.gsSETTINGSThousandSymbol = "" Then
       sThousand = "."
    Else
       sThousand = basParametros.gsSETTINGSThousandSymbol
    End If
    
    If basParametros.gsSETTINGSDecimalSymbol = "" Then
       sDecimal = ","
    Else
       sDecimal = basParametros.gsSETTINGSDecimalSymbol
    End If
    
    TmpFloat = ""
    pos = 1
    
    Do While Len(StrToConvert) > 0
    
        SeekPos = InStr(pos, StrToConvert, sThousand)
        
        If SeekPos > 0 Then
            
            TmpFloat = TmpFloat & Mid(StrToConvert, pos, SeekPos - 1)
            StrToConvert = Right(StrToConvert, Len(StrToConvert) - SeekPos)
            
        Else
        
            TmpFloat = TmpFloat & StrToConvert
            Exit Do
            
        End If
            
    Loop
    
    DblToSQLFloat = TmpFloat

    StrToConvert = DblToSQLFloat
    TmpFloat = ""
    pos = 1
    
    Do While Len(StrToConvert) > 0
    
        SeekPos = InStr(pos, StrToConvert, sDecimal)
        
        If SeekPos > 0 Then
            
            TmpFloat = TmpFloat & Mid(StrToConvert, pos, SeekPos - 1) & "."
            StrToConvert = Right(StrToConvert, Len(StrToConvert) - SeekPos)
            
        Else
        
            TmpFloat = TmpFloat & StrToConvert
            Exit Do
            
        End If
            
    Loop
    
    DblToSQLFloat = TmpFloat

End Function
Public Function SQLBinaryToBoolean(ByVal var As Variant) As Variant
    
    If IsNull(var) Then
        SQLBinaryToBoolean = False
    Else
        If var = 1 Then
            SQLBinaryToBoolean = True
        Else
            SQLBinaryToBoolean = False
        End If
    End If
    

End Function
Public Function BooleanToSQLBinary(ByVal b As Variant) As Variant
    If IsNull(b) Then
        BooleanToSQLBinary = "NULL"
    Else
        If b = "" Then
            BooleanToSQLBinary = "NULL"
        Else
            If b Then
                BooleanToSQLBinary = 1
            Else
                BooleanToSQLBinary = 0
            End If
        End If
    End If
    
End Function

Function GetDecimalSymbol() As String

    Dim Result As Long, Handle As Long
    
    GetWordVersion = 0
    
    Result = RegOpenKeyEx(HKEY_CURRENT_USER, "ControlPanel", 0, STANDARD_RIGHTS_ALL, Handle)
    
    If Result = 0 Then
        
        Result = RegOpenKeyEx(Handle, "Microsoft", 0, STANDARD_RIGHTS_ALL, Handle)
                
        If Result = 0 Then
                
            Result = RegOpenKeyEx(Handle, "Office", 0, STANDARD_RIGHTS_ALL, Handle)
            
            If Result = 0 Then
            
                Result = RegOpenKeyEx(Handle, "8.0", 0, STANDARD_RIGHTS_ALL, Handle)
                
                If Result = 0 Then
                
                    Result = RegOpenKeyEx(Handle, "Word", 0, STANDARD_RIGHTS_ALL, Handle)
                    
                    If Result = 0 Then GetWordVersion = 97
                        
                End If
                
            End If
            
        End If
        
    End If
    
    If GetWordVersion = 0 Then
        
        Result = RegOpenKeyEx(HKEY_CURRENT_USER, "Software", 0, STANDARD_RIGHTS_ALL, Handle)
        
        If Result = 0 Then
            
            Result = RegOpenKeyEx(Handle, "Microsoft", 0, STANDARD_RIGHTS_ALL, Handle)
                    
            If Result = 0 Then
                    
                Result = RegOpenKeyEx(Handle, "Word", 0, STANDARD_RIGHTS_ALL, Handle)
                
                If Result = 0 Then GetWordVersion = 95
            
            End If
        
        End If
        
    End If

End Function

Public Function NoHayParametro(ByVal vParametro As Variant) As Boolean
    If (IsMissing(vParametro) Or IsEmpty(vParametro) Or IsNull(vParametro)) Then
        NoHayParametro = True
    ElseIf (vParametro = "") Then
        NoHayParametro = True
    Else
        NoHayParametro = False
    End If
End Function
Function DateToSQLDateNull(DateThatCanBeEmpty) As Variant


If IsEmpty(DateThatCanBeEmpty) Then
    DateToSQLDateNull = Null
Else
    If IsNull(DateThatCanBeEmpty) Then
        DateToSQLDateNull = Null
    Else
        If DateThatCanBeEmpty = "" Then
            DateToSQLDateNull = Null
        Else
            DateToSQLDateNull = "Convert(datetime,'" & Format(DateThatCanBeEmpty, "mm-dd-yyyy") & "',110)"
        End If
    End If
End If

End Function

''' <summary>Realiza la encriptaci�n/desencriptaci�n del password del usuario</summary>
''' <param name="Seed">Semilla para encriptar � desencriptar</param>
''' <param name="Dato">Dato a encriptar � desencriptar</param>
''' <param name="Encriptando">Indica si se trata de una encriptaci�n � desencriptaci�n</param>
''' <param name="Fecha">Fecha para la encriptaci�n</param>
''' <param name="Quien">Si estas trabajando con 0 licencias 1 usuario 2 objetos (CRaiz.ValidarAccesoObjetos)</param>
''' <param name="Tipo">Tipo De Usuario</param>
''' <returns>String con el dato encriptado/desencriptado</returns>
''' <remarks>Llamada desde: LICDBLogin   LICDBContra    CGestorSeguridad.ValidarUsuario     CRaiz.ValidarAccesoObjetos
'''     CGestorParametros.CargarDatosDeDominio      CGestorParametros.DevolverParametrosGenerales        CGestorParametros.GuardarParametrosInstalacion
'''     CUsuario.IBaseDatos_IniciarEdicion; Tiempo m�ximo:1msg</remarks>
Public Function EncriptarAES(ByVal Seed As String, ByVal dato As String, ByVal Encriptando As Boolean, ByVal Fecha As Variant _
, ByVal Quien As Integer, Optional ByVal Tipo As TipoDeUsuario) As String
    Dim fechahoracrypt As Date
    Dim diacrypt
    Dim oCrypt2 As cCrypt2
    
    If IsMissing(Fecha) Then
        Fecha = DateSerial(1974, 3, 5)
    Else
        If Fecha = "" Then
            Fecha = DateSerial(1974, 3, 5)
        End If
    End If
        
    fechahoracrypt = Fecha
    
    Set oCrypt2 = New cCrypt2
    
    oCrypt2.InBuffer = dato
    
    If Quien = 0 Then
        oCrypt2.Codigo = ClaveSQLtod
    ElseIf Quien = 1 Then
        diacrypt = Day(Fecha)
        
        If IsMissing(Tipo) Then 'CUsuario.IBaseDatos_CambiarCodigo
            If diacrypt Mod 2 = 0 Then
                oCrypt2.Codigo = Seed & ClaveUSUpar
            Else
                oCrypt2.Codigo = Seed & ClaveUSUimp
            End If
        Else
            If diacrypt Mod 2 = 0 Then
                If Tipo = 1 Then
                    oCrypt2.Codigo = Seed & ClaveADMpar
                Else
                    oCrypt2.Codigo = Seed & ClaveUSUpar
                End If
            Else
                If Tipo = 1 Then
                    oCrypt2.Codigo = Seed & ClaveADMimp
                Else
                    oCrypt2.Codigo = Seed & ClaveUSUimp
                End If
            End If
        End If
    Else
        oCrypt2.Codigo = Seed & ClaveOBJtod
    End If
        
    oCrypt2.Fecha = fechahoracrypt
    
    'Datos
    If Encriptando Then
        oCrypt2.Encrypt
    Else
        oCrypt2.Decrypt
    End If
    
    EncriptarAES = oCrypt2.OutBuffer
                            
    Set oCrypt2 = Nothing
End Function

''' <summary>
''' Realiza la encriptaci�n/desencriptaci�n del Dato suministrado
''' </summary>
''' <param name="sCadena">Semilla para encriptar � desencriptar</param>
''' <param name="Dato">Dato a encriptar � desencriptar</param>
''' <param name="Encriptando">Indica si se trata de una encriptaci�n � desencriptaci�n</param>
''' <returns>String con el dato encriptado/desencriptado</returns>
''' <remarks>Llamada desde: CSobres.add     CSobre.AbrirSobre     CSobre.IBaseDatos_AnyadirABaseDatos     CSobre.IBaseDatos_FinalizarEdicionModificando
''' CProceso.CambiarTipoDeProceso   CProceso.IBaseDatos_AnyadirABaseDatos   CProceso.IBaseDatos_CambiarCodigo       CProceso.CopiarProceso
''' CUsuario.ExistenSobresCerradosDeUsuario ; Tiempo m�ximo: 0</remarks>
Public Function EncriptarAperturaSobre(ByVal sCadena As String, ByVal sDato As String, ByVal Encriptando As Boolean) As String
   
    Dim oCrypt2 As cCrypt2
        
    Set oCrypt2 = New cCrypt2
    
    oCrypt2.InBuffer = sDato
    If Len(sCadena) Mod 2 = 0 Then
        oCrypt2.Codigo = sCadena & ClaveUSUpar
    Else
        oCrypt2.Codigo = sCadena & ClaveUSUimp
    End If
    oCrypt2.Fecha = DateSerial(1974, 3, 5)
    
    If Encriptando Then
        oCrypt2.Encrypt
    Else
        oCrypt2.Decrypt
    End If
            
    EncriptarAperturaSobre = oCrypt2.OutBuffer
    
    Set oCrypt2 = Nothing
End Function





Public Function Fecha(ByVal mFecha As Variant, DateFMT As String) As Variant



Dim datesep As String
Dim returnFecha As Variant
Dim mdia As Integer
Dim mmes As Integer
Dim manyo As Integer
Dim sHora As String
On Error GoTo ERROR
If IsNull(mFecha) Or mFecha = "" Then
    Fecha = Null
    Exit Function
End If
returnFecha = mFecha
datesep = Mid(DateFMT, 3, 1)
        
If InStr(returnFecha, ":") Then
    sHora = Mid(returnFecha, InStr(returnFecha, " "), Len(returnFecha))
    returnFecha = Mid(returnFecha, 1, InStr(returnFecha, " "))
End If
        
If Left(DateFMT, 1) = "d" Then
    mFecha = Split(returnFecha, datesep)
                
    mdia = CLng(mFecha(0))
    mmes = CLng(mFecha(1))
    manyo = CLng(mFecha(2))

Else
    mFecha = Split(returnFecha, datesep)
                
    mdia = CLng(mFecha(1))
    mmes = CLng(mFecha(0))
    manyo = CLng(mFecha(2))
End If


On Error Resume Next
If sHora = "" Then
    returnFecha = DateSerial(manyo, mmes, mdia)
Else
    
    returnFecha = DateSerial(manyo, mmes, mdia)
    returnFecha = DateAdd("h", CInt(Mid(sHora, 1, InStr(sHora, ":") - 1)), returnFecha)
    returnFecha = DateAdd("n", CInt(Mid(sHora, InStr(sHora, ":") + 1, 2)), returnFecha)
    
        
End If

Fecha = returnFecha
Exit Function

ERROR:
Fecha = Null


End Function

Public Function Version(oCon As adodb.Connection) As String
    Dim adoRs As adodb.Recordset

    Set adoRs = New adodb.Recordset
    
    adoRs.Open "SELECT @@VERSION VERSION", oCon
    
    Version = adoRs.Fields("VERSION").Value
    
    adoRs.Close
    
    Set adoRs = Nothing

End Function

Public Function DevolverDenGMN() As String
    If gParametrosGenerales.gbSincronizacionMat = True Then
        DevolverDenGMN = "DEN_" & gParametrosInstalacion.gIdiomaPortal
    Else
        DevolverDenGMN = "DEN_SPA"
    End If
End Function

''' <summary>
''' Realiza la encriptaci�n/desencriptaci�n del Dato suministrado
''' </summary>
''' <param name="sCadena">Semilla para encriptar � desencriptar</param>
''' <param name="Dato">Dato a encriptar � desencriptar</param>
''' <param name="Encriptando">Indica si se trata de una encriptaci�n � desencriptaci�n</param>
''' <remarks>Llamada desde: CGestorParametros.DevolverParametrosGenerales ; Tiempo m�ximo: 0</remarks>
Public Function EncriptarAccesoFSWS(ByVal sCadena As String, ByVal sDato As String, ByVal Encriptando As Boolean) As String
    Dim oCrypt2 As cCrypt2
        
    Set oCrypt2 = New cCrypt2
    
    oCrypt2.InBuffer = sDato
    If Len(sCadena) Mod 2 = 0 Then
        oCrypt2.Codigo = ClaveUSUpar
    Else
        oCrypt2.Codigo = ClaveUSUimp
    End If
    oCrypt2.Fecha = DateSerial(1974, 3, 5)
    
    If Encriptando Then
        oCrypt2.Encrypt
    Else
        oCrypt2.Decrypt
    End If
            
    EncriptarAccesoFSWS = oCrypt2.OutBuffer
    
    Set oCrypt2 = Nothing
End Function

Public Function Numero(ByVal Num As Variant, ByVal sDecimalFmt As String) As Variant

    Dim NumeroFinal As Variant
    Dim Indice As Integer
    Dim dividir As Variant
    Dim decimales As Boolean

    Dim Numerico As Variant
    On Error GoTo ERROR
    If IsNull(Num) Then
        Numero = Null
        Exit Function
    End If
    dividir = 1
    NumeroFinal = ""

    Num = Replace(Num, sDecimalFmt, ".")


    If Num = "" Then
        Numero = Null
        Exit Function
    End If
        
    For Indice = 1 To Len(Num)
        Select Case Mid(Num, Indice, 1)
            Case "."
                NumeroFinal = NumeroFinal
                decimales = True
            Case Else
                NumeroFinal = NumeroFinal & Mid(Num, Indice, 1)
                If decimales = True Then
                    dividir = dividir * 10
                End If
        End Select
    Next
    If decimales = True Then
        Numerico = CDec(NumeroFinal) / CDec(dividir)
    Else
        Numerico = CDec(NumeroFinal)
    End If
    
    Numero = Numerico
fin:
    Exit Function
ERROR:
    Numero = Null
    Resume fin
    Resume 0
                
End Function


''' <summary>
''' Comprueba que el c�digo del idioma est� entre los idiomas v�lidos
''' </summary>
''' <param name="sIdi">C�digo de idioma</param>
''' <returns>sIdi si es un idioma v�lido, "ENG" si no es v�lido
'''     * Si intentan hacer SQL Injection o no es un idioma v�lido devolvemos el idioma "ENG" por defecto
''' </returns>
''' <remarks>Llamada desde: Todas las funciones y m�todos que se vayan adecuando a la normativa en cuanto a SQL Injection
'''          Cada programador deber� anotar aqu� las funciones donde la usa; Tiempo m�ximo:1msg
''' </remarks>
Public Function EsIdiomaValido(ByVal sIdi As String) As String
    Select Case sIdi
    Case "ENG"
        EsIdiomaValido = "ENG"
    Case "SPA"
        EsIdiomaValido = "SPA"
    Case "GER"
        EsIdiomaValido = "GER"
    Case "FRA"
        EsIdiomaValido = "FRA"
    Case Else
        EsIdiomaValido = "ENG"
    End Select
End Function

''' <summary>
''' Si el Dato a traducir contiene algo lo devuelve como un entero 1 true 0 false, eoc devuelve null
''' </summary>
''' <param name="b">Dato a traducir</param>
''' <returns>si b contiene algo un entero 1 true 0 false, null eoc</returns>
''' <remarks>Llamada desde: coferta.AlmacenarOfertaPortal; Tiempo m�ximo:0</remarks>
Public Function BooleanToVbBinary(ByVal b As Variant) As Variant
    If IsEmpty(b) Or IsNull(b) Then
        BooleanToVbBinary = Null
        Exit Function
    End If
    
    If b Then
        BooleanToVbBinary = 1
    Else
        BooleanToVbBinary = 0
    End If
    
End Function

''' <summary>
''' Si el Dato a traducir contiene algo lo devuelve como un double
''' </summary>
''' <param name="DblToConvert">Dato a traducir</param>
''' <returns>si DblToConvert contiene algo double, null eoc</returns>
''' <remarks>Llamada desde: coferta.AlmacenarOfertaPortal; Tiempo m�ximo:0</remarks>
Public Function DblToVbFloat(DblToConvert) As Variant
    If IsEmpty(DblToConvert) Or IsNull(DblToConvert) Or DblToConvert = "" Then
        DblToVbFloat = Null
        Exit Function
    End If

    DblToVbFloat = CDbl(DblToConvert)
End Function
Function NumToVbNULL(StrThatCanBeEmpty) As Variant
    
    If IsEmpty(StrThatCanBeEmpty) Then
        NumToVbNULL = Null
    Else
        If IsNull(StrThatCanBeEmpty) Then
            NumToVbNULL = Null
        Else
            If StrThatCanBeEmpty = "" Or StrThatCanBeEmpty = 0 Then
                NumToVbNULL = Null
            Else
                NumToVbNULL = StrThatCanBeEmpty
            End If
        End If
    End If
    
End Function

''' <summary>
''' Devuelve la denominaci�n de CC tal como la ver� el usuario
''' </summary>
''' <param name="U1">Unidad organizativa nivel 1</param>
''' <param name="U2">Unidad organizativa nivel 2</param>
''' <param name="U3">Unidad organizativa nivel 3</param>
''' <param name="U4">Unidad organizativa nivel 4</param>
''' <param name="U1Den">Denominaci�n Unidad organizativa nivel 1</param>
''' <param name="U2Den">Denominaci�n Unidad organizativa nivel 2</param>
''' <param name="U3Den">Denominaci�n Unidad organizativa nivel 3</param>
''' <param name="U4Den">Denominaci�n Unidad organizativa nivel 4</param>
''' <returns>string</returns>
''' <remarks>Llamada desde:CRecepci�n.CargarLineasRecepcion, CRecepci�n.CargarLineasPedido,cOrdenEntrega.cargarLineasPedido </remarks>
''' <remarks>Tiempo m�ximo: < 2seg </remarks>
Public Function DevolverDenCC(ByVal U1 As Variant, ByVal U2 As Variant, ByVal U3 As Variant, ByVal U4 As Variant, ByVal U1Den As Variant, ByVal U2Den As Variant, ByVal U3Den As Variant, ByVal U4Den As Variant) As String

If Not NoHayParametro(U4) Then
    DevolverDenCC = U4Den & "(" & NullToStr(U1) & "-" & NullToStr(U2) & "-" & NullToStr(U3) & ")"
ElseIf Not NoHayParametro(U3) Then
    DevolverDenCC = U3Den & "(" & NullToStr(U1) & "-" & NullToStr(U2) & ")"
ElseIf Not NoHayParametro(U2) Then
    DevolverDenCC = U2Den & "(" & NullToStr(U1) & ")"
ElseIf Not NoHayParametro(U1) Then
    DevolverDenCC = U1Den
Else
    DevolverDenCC = ""
End If
End Function

''' <summary>
''' Llama al Servicio de Integraci�n.
''' </summary>
''' <param name="iTabla">El ID de la tabla a integrar.</param>
''' <param name="lIEntidad">El ID de la entidad a integrar, pero en caso de que la tabla se corresponda a las
'''    Estructuras de Materiales o a las Recepciones(si biene la empresa este campo ser� el id de la entidad), este campo indicar�a su ID en la tabla de LOG.</param>
''' <param name="strErp">El ERP actual, se usa �nicamente para las estructuras de materiales.</param>
''' <param name="iEmpresa">El id de la empresa.</param>
''' <param name="lIDPedido">El id del pedido.</param>
''' <remarks>Llamada desde: CGrupoMatNivel1.AnyadirABaseDatos, CGrupoMatNivel1.FinalizarEdicionEliminando,
''' CGrupoMatNivel1.FInalizarEdicionModificando, CGrupoMatNivel1.CambiarCodigo, CGrupoMatNivel2.AnyadirABaseDatos, CGrupoMatNivel2.FinalizarEdicionEliminando,
''' CGrupoMatNivel2.FInalizarEdicionModificando, CGrupoMatNivel2.CambiarCodigo, CGrupoMatNivel3.AnyadirABaseDatos, CGrupoMatNivel3.FinalizarEdicionEliminando,
''' CGrupoMatNivel3.FInalizarEdicionModificando, CGrupoMatNivel3.CambiarCodigo, CGrupoMatNivel4.AnyadirABaseDatos, CGrupoMatNivel4.FinalizarEdicionEliminando,
''' CGrupoMatNivel4.FInalizarEdicionModificando, CGrupoMatNivel4.CambiarCodigo, CAtributoOfertado.GuardarAtributoPedido, CAtributoEspecificacion.GuardarAtributoPedido,
''' CLineaPedido.FinalizarEdicionModificando, CPresProyectosNivel4.AlmacenarPresDeLineaPedido,
''' CPresContablesNivel4.AlmacenarPresDeLineaPedido, CPresConceptos3Nivel4.AlmacenarPresDeLineaPedido,
''' CPresConceptos4Nivel4.AlmacenarPresDeLineaPedido, COrdenEntrega.GrabarPedidoEnLog, COrdenesEntrega.EmitirPedidoConLineasTemporales,
''' COrdenesEntrega.BucleEmitirPedidoConLineasTemporales, COrdenEntrega.EliminarDeBaseDatos, COrdenEntrega.GrabarRecepEnLog,
''' CRecepcion.AnyadirABaseDatos, CRecepcion.EliminarDeBaseDatos, CRecepcion.FinalizarRecepcionModificando.</remarks>
''' <remarks>Tiempo m�ximo: < 2seg </remarks>
Public Sub LlamarFSIS(ByVal iTabla As Integer, ByVal lIDEntidad As Long, Optional ByVal strErp As String = "", _
        Optional ByVal iEmpresa As Integer = 0, Optional ByVal lIDPedido As Long = 0, Optional ByVal lIdInstancia As Long = 0)
    Dim ResulGrabaError As TipoErrorSummit
    Dim rs As adodb.Recordset
    Dim iTipoInt As Integer
    Dim cErp As New CERPInt
    Dim iEstado, iOrigen As Integer
    Dim bPedAcepProvActivo As Boolean
    Dim lIdLogEntidad As Long
    Dim sERP As String
    Dim sNombre As String
    Dim sPass  As String
    Dim iServiceBindingType As Integer
    Dim iServiceSecurityMode As Integer
    Dim iClientCredentialType As Integer
    Dim iProxyCredentialType As Integer

    Set cErp.Conexion = oConexion
    Set rs = New adodb.Recordset
   
    Select Case iTabla
    Case EntidadIntegracion.art4:
        If oFSIS Is Nothing Then
            Set oFSIS = New CFSISService
        End If
        cErp.ObtenerParametrosWCF EntidadIntegracion.art4, strErp, iServiceBindingType, iServiceSecurityMode, iClientCredentialType, iProxyCredentialType, sNombre, sPass
        If gParametrosGenerales.gcolRutas("FullstepIS") <> "" Then
            oFSIS.Exportar lIDEntidad, iTabla, gParametrosGenerales.gcolRutas("FullstepIS"), iServiceBindingType, iServiceSecurityMode, iClientCredentialType, iProxyCredentialType, sNombre, sPass
        End If
        
    Case EntidadIntegracion.Materiales:
        If strErp <> "" Then
            iTipoInt = cErp.ObtenerTipoInt(EntidadIntegracion.Materiales, strErp)
            If (iTipoInt = TipoDestinoIntegracion.WCF) Then
                If oFSIS Is Nothing Then
                    Set oFSIS = New CFSISService
                End If
                cErp.ObtenerParametrosWCF EntidadIntegracion.Materiales, strErp, iServiceBindingType, iServiceSecurityMode, iClientCredentialType, iProxyCredentialType, sNombre, sPass
                If gParametrosGenerales.gcolRutas("FullstepIS") <> "" Then
                    oFSIS.Exportar lIDEntidad, iTabla, gParametrosGenerales.gcolRutas("FullstepIS"), iServiceBindingType, iServiceSecurityMode, iClientCredentialType, iProxyCredentialType, sNombre, sPass
                End If
            End If
        End If
    Case EntidadIntegracion.PED_directo, EntidadIntegracion.PED_Aprov:
        ''Obtenemos el ERP :
        '' 1� buscando a partir de ORDEN_ENTREGA.ORGCOMPRAS, si este campo ERP es NULL entonces
        '' 2� a partir de EMP.SOCIEDAD
        sConsulta = "SELECT DISTINCT ERP = CASE WHEN EO.ERP IS NULL THEN ERP_SOCIEDAD.ERP ELSE EO.ERP END "
        sConsulta = sConsulta & " FROM ORDEN_ENTREGA WITH(NOLOCK) "
        sConsulta = sConsulta & " INNER JOIN EMP WITH(NOLOCK) ON ORDEN_ENTREGA.EMPRESA = EMP.ID"
        sConsulta = sConsulta & " LEFT JOIN ERP_SOCIEDAD WITH(NOLOCK) ON ERP_SOCIEDAD.SOCIEDAD = EMP.SOCIEDAD"
        sConsulta = sConsulta & " LEFT JOIN (ERP_ORGCOMPRAS EO WITH(NOLOCK) INNER JOIN SOCIEDAD_ORGCOMPRAS SO WITH(NOLOCK)"
        sConsulta = sConsulta & " ON SO.ORGCOMPRAS=EO.ORGCOMPRAS) ON EO.ORGCOMPRAS = ORDEN_ENTREGA.ORGCOMPRAS AND SO.SOCIEDAD=EMP.SOCIEDAD"
        sConsulta = sConsulta & " WHERE ORDEN_ENTREGA.ID=" & lIDEntidad
        
        rs.Open sConsulta, oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
        If Not rs.eof Then
            sERP = NullToStr(rs.Fields("ERP").Value)
        End If
        rs.Close
        
       rs.Open "SELECT LOG_ORDEN_ENTREGA.ID, TABLA, LOG_ORDEN_ENTREGA.EST, LOG_GRAL.ERP, LOG_ORDEN_ENTREGA.ACCION FROM LOG_ORDEN_ENTREGA WITH(NOLOCK) INNER JOIN LOG_GRAL WITH(NOLOCK) ON ID_TABLA=LOG_ORDEN_ENTREGA.ID AND TABLA IN (" & EntidadIntegracion.PED_directo & "," & EntidadIntegracion.PED_Aprov & ")" & _
        " AND LOG_GRAL.ORIGEN IN (" & OrigenIntegracion.FSGSInt & ", " & OrigenIntegracion.FSGSIntReg & ") WHERE ID_ORDEN_ENTREGA=" & lIDEntidad & " AND LOG_GRAL.ESTADO=" & EstadoIntegracion.PendienteDeTratar & " ORDER BY LOG_ORDEN_ENTREGA.ID DESC", oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
        If Not rs.eof Then
            lIdLogEntidad = rs.Fields("ID").Value
            iTabla = NullToStr(rs.Fields("TABLA").Value)
            iEstado = NullToStr(rs.Fields("EST").Value)
            If rs.Fields("ACCION").Value = Accion_Baja Then
                'El pedido ha sido eliminado de la base de datos, por lo que en la primera consulta no hemos podido obtener el ERP, lo obtenemos de las tablas de log, ya que es el ERP del registro que vamos a enviar, y este registro es el �ltimo que est� con estado a 0 para el pedido con id de orden entrega lIdEntidad.
                sERP = NullToStr(rs.Fields("ERP").Value)
            End If
        End If
        rs.Close
        If sERP <> "" And lIdLogEntidad > 0 Then
            bPedAcepProvActivo = cErp.IntPedTrasAcepActivado(sERP)
            If Not bPedAcepProvActivo Or (bPedAcepProvActivo And iEstado >= AceptadoPorProveedor) Then

                iTipoInt = cErp.ObtenerTipoInt(EntidadIntegracion.PED_directo, sERP)
                If (iTipoInt = TipoDestinoIntegracion.WCF) Then
                    If oFSIS Is Nothing Then
                        Set oFSIS = New CFSISService
                    End If
                    cErp.ObtenerParametrosWCF EntidadIntegracion.PED_directo, sERP, iServiceBindingType, iServiceSecurityMode, iClientCredentialType, iProxyCredentialType, sNombre, sPass
                    If gParametrosGenerales.gcolRutas("FullstepIS") <> "" Then
                        oFSIS.Exportar lIdLogEntidad, iTabla, gParametrosGenerales.gcolRutas("FullstepIS"), iServiceBindingType, iServiceSecurityMode, iClientCredentialType, iProxyCredentialType, sNombre, sPass
                    End If
                End If
                
            End If
        End If
       
    Case EntidadIntegracion.Rec_Directo:
        If iEmpresa <> 0 Then
            sConsulta = "SELECT DISTINCT ERP = CASE WHEN EO.ERP IS NULL THEN ERP_SOCIEDAD.ERP ELSE EO.ERP END FROM EMP WITH (NOLOCK)"
            sConsulta = sConsulta & " LEFT JOIN ERP_SOCIEDAD WITH (NOLOCK) ON ERP_SOCIEDAD.SOCIEDAD = EMP.SOCIEDAD"
            sConsulta = sConsulta & " LEFT JOIN (ERP_ORGCOMPRAS EO WITH(NOLOCK) INNER JOIN SOCIEDAD_ORGCOMPRAS SO WITH(NOLOCK)"
            sConsulta = sConsulta & " ON SO.ORGCOMPRAS=EO.ORGCOMPRAS) ON SO.SOCIEDAD = EMP.SOCIEDAD"
            sConsulta = sConsulta & " WHERE EMP.ID = " & iEmpresa
        Else
            'Aqu� el id de la entidad NO es el id de la entidad, si no que es el id de la orden.
            sConsulta = "SELECT ERP FROM ERP_ORGCOMPRAS EO WITH (NOLOCK) INNER JOIN ORDEN_ENTREGA WITH (NOLOCK) ON EO.ORGCOMPRAS = ORDEN_ENTREGA.ORGCOMPRAS WHERE ORDEN_ENTREGA.ID=" & lIDEntidad
        End If
        rs.Open sConsulta, oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
        If Not rs.eof Then
            sERP = NullToStr(rs.Fields("ERP").Value)
        End If
        rs.Close
        
      'Llamada FSIS.
        If sERP <> "" Then
            iTipoInt = cErp.ObtenerTipoInt(EntidadIntegracion.Rec_Directo, sERP)
            If (iTipoInt = TipoDestinoIntegracion.WCF) Then
                 'Obtenemos el ID que acabamos de meter en LOG_PEDIDO_RECEP
                sConsulta = "SELECT TOP 1 LOG_PEDIDO_RECEP.ID as ID, TABLA FROM LOG_PEDIDO_RECEP WITH(NOLOCK) INNER JOIN LOG_GRAL WITH(NOLOCK) ON LOG_GRAL.ID_TABLA=LOG_PEDIDO_RECEP.ID " & _
                    " AND TABLA IN (" & EntidadIntegracion.Rec_Directo & "," & EntidadIntegracion.Rec_Aprov & ") AND LOG_GRAL.ORIGEN IN (" & OrigenIntegracion.FSGSInt & ", " & OrigenIntegracion.FSGSIntReg & ")  WHERE ID_PEDIDO_RECEP=" & lIDEntidad & " AND LOG_gRAL.ESTADO=" & EstadoIntegracion.PendienteDeTratar & " ORDER BY LOG_GRAL.ID DESC"
                rs.Open sConsulta, oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
                If Not rs.eof Then
                    lIdLogEntidad = rs.Fields("ID").Value
                    iTabla = rs.Fields("TABLA").Value
                End If
                rs.Close
                
                If oFSIS Is Nothing Then
                    Set oFSIS = New CFSISService
                End If
                cErp.ObtenerParametrosWCF EntidadIntegracion.Rec_Directo, sERP, iServiceBindingType, iServiceSecurityMode, iClientCredentialType, iProxyCredentialType, sNombre, sPass
                If gParametrosGenerales.gcolRutas("FullstepIS") <> "" Then
                     oFSIS.Exportar lIdLogEntidad, iTabla, gParametrosGenerales.gcolRutas("FullstepIS"), iServiceBindingType, iServiceSecurityMode, iClientCredentialType, iProxyCredentialType, sNombre, sPass
                End If
            End If
        End If
        
    Case EntidadIntegracion.Prove
         'Llamada FSIS
         If strErp = "" Then 'Si no nos llega ERP puede que estemos en una modificaci�n y necesitamos obtenerlo.
            sConsulta = "SELECT TOP 1 LP.ERP FROM LOG_GRAL AS LG WITH(NOLOCK) "
            sConsulta = sConsulta & "INNER JOIN LOG_PROVE AS LP WITH(NOLOCK) "
            sConsulta = sConsulta & "ON LG.ID_TABLA=LP.ID AND LG.TABLA=" & EntidadIntegracion.Prove
            sConsulta = sConsulta & " WHERE LG.ORIGEN IN (" & OrigenIntegracion.FSGSInt & ", " & OrigenIntegracion.FSGSIntReg & ") AND LP.ID=" & lIDEntidad
            sConsulta = sConsulta & " ORDER BY LG.ID DESC"
            rs.Open sConsulta, oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
            If Not rs.eof Then
                strErp = NullToStr(rs.Fields("ERP").Value)
            End If
            rs.Close
        End If
         
        If strErp <> "" Then
            iTipoInt = cErp.ObtenerTipoInt(EntidadIntegracion.Prove, strErp)
            If (iTipoInt = TipoDestinoIntegracion.WCF) Then
                If oFSIS Is Nothing Then
                    Set oFSIS = New CFSISService
                End If
                cErp.ObtenerParametrosWCF EntidadIntegracion.Prove, strErp, iServiceBindingType, iServiceSecurityMode, iClientCredentialType, iProxyCredentialType, sNombre, sPass
                If gParametrosGenerales.gcolRutas("FullstepIS") <> "" Then
                    oFSIS.Exportar lIDEntidad, iTabla, gParametrosGenerales.gcolRutas("FullstepIS"), iServiceBindingType, iServiceSecurityMode, iClientCredentialType, iProxyCredentialType, sNombre, sPass
                End If
            End If
        End If
    Case EntidadIntegracion.SolicitudPM
        cErp.ObtenerIDsLogInstancia lIdInstancia, lIDEntidad, strErp
          If strErp <> "" Then
            iTipoInt = cErp.ObtenerTipoInt(EntidadIntegracion.SolicitudPM, strErp)
            If (iTipoInt = TipoDestinoIntegracion.WCF) Then
                If oFSIS Is Nothing Then
                    Set oFSIS = New CFSISService
                End If
                cErp.ObtenerParametrosWCF EntidadIntegracion.SolicitudPM, strErp, iServiceBindingType, iServiceSecurityMode, iClientCredentialType, iProxyCredentialType, sNombre, sPass
                If gParametrosGenerales.gcolRutas("FullstepIS") <> "" Then
                    oFSIS.Exportar lIDEntidad, iTabla, gParametrosGenerales.gcolRutas("FullstepIS"), iServiceBindingType, iServiceSecurityMode, iClientCredentialType, iProxyCredentialType, sNombre, sPass
                End If
            End If
        End If
    Case EntidadIntegracion.Adj:
       If strErp <> "" Then
            iTipoInt = cErp.ObtenerTipoInt(EntidadIntegracion.Adj, strErp)
            If (iTipoInt = TipoDestinoIntegracion.WCF) Then
                If oFSIS Is Nothing Then
                    Set oFSIS = New CFSISService
                End If
                cErp.ObtenerParametrosWCF EntidadIntegracion.Adj, strErp, iServiceBindingType, iServiceSecurityMode, iClientCredentialType, iProxyCredentialType, sNombre, sPass
                If gParametrosGenerales.gcolRutas("FullstepIS") <> "" Then
                    oFSIS.Exportar lIDEntidad, iTabla, gParametrosGenerales.gcolRutas("FullstepIS"), iServiceBindingType, iServiceSecurityMode, iClientCredentialType, iProxyCredentialType, sNombre, sPass
                End If
            End If
        End If
   End Select
   
   Set cErp = Nothing
   Set rs = Nothing
End Sub

Public Function ExisteItemEnColeccion(ByVal oCol As Collection, ByVal sCod As String) As Boolean
    On Error GoTo ERROR
    
    If Not oCol.Item(sCod) Is Nothing Then
        ExisteItemEnColeccion = True
        Exit Function
    End If
    
ERROR:
    If ERR.Number = 5 Then
        ExisteItemEnColeccion = False
    End If
End Function


''' <summary>
''' Esta funci�n convierte un VARIANT a un Integer.
''' </summary>
''' <param="vDato">El dato a convertir.</param>
''' <returns>Integer con toda la informaci�n del campo vDato.</returns>
''' <remarks>Llamada desde: Cualquier parte de la aplicaci�n que lo requiera.
''' Tiempo m�ximo: 0,1</remarks>
Public Function VarToInt(ByVal vDato As Variant) As Integer
'''
''' Esta funci�n recoge un valor que representa un dato num�rico
''' y lo convierte a un dato de tipo integer.
'''
    If IsNull(vDato) Then
        VarToInt = 0
    ElseIf IsEmpty(vDato) Then
        VarToInt = 0
    ElseIf vDato = "" Then
        VarToInt = 0
    ElseIf Not IsNumeric(vDato) Then
        VarToInt = 0
    Else
        VarToInt = CInt(vDato)
    End If
End Function

''' <summary>
''' Funci�n que se encarga de dar formato al texto escrito por el usuario en la caja de texto destinada a la busqueda e art�culos, encargada de ignorar acentos, may�sculas y min�sculas...
''' </summary>
''' <param name="Cadena">Texto a buscar</param>
''' <param name="IgnorarAcentos">Valor booleano que indica si se deben ignorar las diferencias de acentos, di�resis, etc.</param>
''' <returns>Cadena para operador LIKE</returns>

Public Function strToSQLLIKE(ByVal Cadena As String, Optional ByVal IgnorarAcentos As Boolean = False) As String
    Cadena = Replace(Cadena, "[", "[[]")
    Cadena = Replace(Cadena, "_", "[_]")
    Cadena = Replace(Cadena, "%", "[%]")
    Cadena = Replace(Cadena, "'", "''''")
    Cadena = UCase(Cadena)
    If IgnorarAcentos Then
        Cadena = Replace(Cadena, "A", "[A����]")
        Cadena = Replace(Cadena, "E", "[E����]")
        Cadena = Replace(Cadena, "I", "[I����]")
        Cadena = Replace(Cadena, "O", "[O����]")
        Cadena = Replace(Cadena, "U", "[U����]")
    End If
    
    strToSQLLIKE = Cadena
End Function

Public Sub CreaRegistroCambioEstado(ByVal DesdeDonde As String, ByVal QueHaces As String, ByVal Persona As String, ByVal oConexion As CConexion _
, ByVal Solicitud As Variant, Optional ByVal Anyo As Integer, Optional ByVal Gmn As String, Optional ByVal Proce As Long _
, Optional ByVal ProceGrupo As Long = 0, Optional ByVal Item As Long = 0)

Dim rs As adodb.Recordset
Dim sConsulta As String

If Not IsNull(Solicitud) Then
    Set rs = New adodb.Recordset

    Select Case DesdeDonde
    Case "PROCE"
        If QueHaces = "INSERT" Then
            sConsulta = "SELECT ID FROM INSTANCIA WITH (NOLOCK) WHERE ESTADO=100 AND ID=" & CStr(Solicitud)
        ElseIf QueHaces = "INSERT_SELECT" Then
            sConsulta = "SELECT ID FROM INSTANCIA WITH (NOLOCK) WHERE ESTADO=100 AND ID= ("
            sConsulta = sConsulta & "SELECT ISNULL(SOLICIT,0) FROM PROCE WITH(NOLOCK) WHERE ANYO=" & CStr(Anyo) & " AND GMN1=" & StrToSQLNULL(Gmn) & " AND COD=" & CStr(Proce) & ")"
        Else 'UPDATE
            sConsulta = "SELECT ID FROM INSTANCIA WITH (NOLOCK) WHERE ESTADO=100 AND ID=" & CStr(Solicitud)
            sConsulta = sConsulta & " AND NOT ID=(SELECT ISNULL(SOLICIT,0) FROM PROCE WITH(NOLOCK) WHERE ANYO=" & CStr(Anyo) & " AND GMN1=" & StrToSQLNULL(Gmn) & " AND COD=" & CStr(Proce) & ")"
        End If
    Case "PROCE_GRUPO"
        If QueHaces = "INSERT" Then
            sConsulta = "SELECT ID FROM INSTANCIA WITH (NOLOCK) WHERE ESTADO=100 AND ID=" & CStr(Solicitud)
        ElseIf QueHaces = "INSERT_SELECT" Then
            sConsulta = "SELECT ID FROM INSTANCIA WITH (NOLOCK) WHERE ESTADO=100 AND ID= ("
            sConsulta = sConsulta & "SELECT ISNULL(SOLICIT,0) FROM PROCE_GRUPO WITH(NOLOCK) WHERE ANYO=" & CStr(Anyo) & " AND GMN1=" & StrToSQLNULL(Gmn) & " AND PROCE=" & CStr(Proce) & " AND ID=" & ProceGrupo & ")"
        Else 'UPDATE
            sConsulta = "SELECT ID FROM INSTANCIA WITH (NOLOCK) WHERE ESTADO=100 AND ID=" & CStr(Solicitud)
            sConsulta = sConsulta & " AND NOT ID=(SELECT ISNULL(SOLICIT,0) FROM PROCE_GRUPO WITH(NOLOCK) WHERE ANYO=" & CStr(Anyo) & " AND GMN1=" & StrToSQLNULL(Gmn) & " AND PROCE=" & CStr(Proce) & " AND ID=" & ProceGrupo & ")"
        End If
    Case "ITEM"
        If QueHaces = "INSERT" Then
            sConsulta = "SELECT ID FROM INSTANCIA WITH (NOLOCK) WHERE ESTADO=100 AND ID=" & CStr(Solicitud)
        ElseIf QueHaces = "INSERT_SELECT" Then
            sConsulta = "SELECT DISTINCT ID FROM INSTANCIA WITH (NOLOCK) WHERE ESTADO=100 AND ID= ("
            sConsulta = sConsulta & "SELECT ISNULL(SOLICIT,0) FROM ITEM WITH(NOLOCK) WHERE ANYO=" & CStr(Anyo) & " AND GMN1_PROCE=" & StrToSQLNULL(Gmn) & " AND PROCE=" & CStr(Proce) & " AND GRUPO=" & ProceGrupo & ")"
        Else 'UPDATE
            sConsulta = "SELECT DISTINCT ID FROM INSTANCIA WITH (NOLOCK) WHERE ESTADO=100 AND ID=" & CStr(Solicitud)
            sConsulta = sConsulta & " AND NOT ID=(SELECT ISNULL(SOLICIT,0) FROM ITEM WITH(NOLOCK) WHERE ANYO=" & CStr(Anyo) & " AND GMN1_PROCE=" & StrToSQLNULL(Gmn) & " AND PROCE=" & CStr(Proce) & " AND ID=" & Item & ")"
        End If
    End Select
    
    rs.Open sConsulta, oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    While Not rs.eof
        sConsulta = "INSERT INTO INSTANCIA_EST (INSTANCIA,FECHA,EST,PER) VALUES(" & CStr(rs.Fields("ID").Value) & ",GETDATE(),101," & StrToSQLNULL(Persona) & ")"
        oConexion.ADOCon.Execute sConsulta
        
        rs.MoveNext
    Wend
    
    rs.Close
    Set rs = Nothing
End If
End Sub

