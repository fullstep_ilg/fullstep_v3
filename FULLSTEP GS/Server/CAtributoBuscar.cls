VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CAtributoBuscar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
Private m_lIdAtrib As Long
Private m_sCod As String
Private m_sDen As String
Private m_udtTipo As TiposDeAtributos
Private m_vValor As Variant
Private m_udtTipoIntroduccion As TAtributoIntroduccion
Private m_vMinimo As Variant
Private m_vMaximo As Variant
Private m_sOperador As String
Private m_iListaExterna As Integer
Private m_udtAmbitoBuscar As TAtributoAmbito
Private m_iPosicion As Integer
Private m_oConexion As CConexion
Private m_oConexionServer As CConexionDeUseServer

Friend Property Set Conexion(ByVal vData As CConexion)
    Set m_oConexion = vData
End Property
Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property

Friend Property Set ConexionServer(ByVal con As CConexionDeUseServer)
Set m_oConexionServer = con
End Property
Friend Property Get ConexionServer() As CConexionDeUseServer
Set ConexionServer = m_oConexionServer
End Property

Public Property Let IdAtrib(ByVal Data As Long)
    m_lIdAtrib = Data
End Property
Public Property Get IdAtrib() As Long
    IdAtrib = m_lIdAtrib
End Property

Public Property Let Cod(ByVal Data As String)
    m_sCod = Data
End Property
Public Property Get Cod() As String
    Cod = m_sCod
End Property

Public Property Let Den(ByVal Data As String)
    m_sDen = Data
End Property
Public Property Get Den() As String
    Den = m_sDen
End Property

Public Property Let Tipo(ByVal Data As TiposDeAtributos)
    m_udtTipo = Data
End Property
Public Property Get Tipo() As TiposDeAtributos
    Tipo = m_udtTipo
End Property

Public Property Get Valor() As Variant
    Valor = m_vValor
End Property
Public Property Let Valor(ByVal vData As Variant)
    m_vValor = vData
End Property

Public Property Get Minimo() As Variant
    Minimo = m_vMinimo
End Property
Public Property Let Minimo(ByVal vData As Variant)
    m_vMinimo = vData
End Property

Public Property Get Maximo() As Variant
    Maximo = m_vMaximo
End Property
Public Property Let Maximo(ByVal vData As Variant)
    m_vMaximo = vData
End Property

Public Property Let TipoIntroduccion(ByVal vData As TAtributoIntroduccion)
    m_udtTipoIntroduccion = vData
End Property
Public Property Get TipoIntroduccion() As TAtributoIntroduccion
    TipoIntroduccion = m_udtTipoIntroduccion
End Property

Public Property Let Operador(ByVal Data As String)
    m_sOperador = Data
End Property
Public Property Get Operador() As String
    Operador = m_sOperador
End Property

Public Property Let AmbitoBuscar(ByVal vData As TAtributoAmbito)
    m_udtAmbitoBuscar = vData
End Property
Public Property Get AmbitoBuscar() As TAtributoAmbito
    AmbitoBuscar = m_udtAmbitoBuscar
End Property

Public Property Let ListaExterna(ByVal vData As Integer)
    m_iListaExterna = vData
End Property
Public Property Get ListaExterna() As Integer
    ListaExterna = m_iListaExterna
End Property

Public Property Let Posicion(ByVal vData As Integer)
    m_iPosicion = vData
End Property
Public Property Get Posicion() As Integer
    Posicion = m_iPosicion
End Property

Private Sub Class_Terminate()
Set m_oConexion = Nothing
Set m_oConexionServer = Nothing
End Sub
