VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CBaseDeDatos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

Private moConexion As CConexion

Private mlID As Long
Private msDenominacion As String
Private mvServidor As Variant
Private mvNombre As Variant
Private mvUsuario As Variant
Private mvPassword As Variant
Private mvImpUsuario As Variant
Private mvImpPassword  As Variant
Private mvImpDominio As Variant
Private mbPrincipal As Boolean
Private mbLogin As Boolean
Private mbActiva As Boolean

''' <summary>
''' Dar valor a la conexion
''' </summary>
''' <param name="con">Dar valor a la conexion</param>
''' <remarks>Llamada desde: frmLogin ; Tiempo m�ximo: 0</remarks>
Friend Property Set Conexion(ByVal con As CConexion)
Set moConexion = con
End Property

''' <summary>
''' Devolver valor a la conexion
''' </summary>
''' <returns>Valor de la variable</returns>
''' <remarks>Llamada desde: frmLogin ; Tiempo m�ximo: 0</remarks>
Friend Property Get Conexion() As CConexion
Set Conexion = moConexion
End Property

''' <summary>
''' Devolver el valor de la variable mlID. El FS_BDS.ID de la bd a q me conecto.
''' </summary>
''' <returns>Valor de la variable</returns>
''' <remarks>Llamada desde: frmLogin ; Tiempo m�ximo: 0</remarks>
Public Property Get Id() As Long
    Id = mlID
End Property

''' <summary>
''' Establacer el valor de la variable mlID. El FS_BDS.ID de la bd a q me conecto.
''' </summary>
''' <param name="Value">Valor de la variable</param>
''' <remarks>Llamada desde: frmLogin ; Tiempo m�ximo: 0</remarks>
Public Property Let Id(ByVal Value As Long)
    mlID = Value
End Property

''' <summary>
''' Devolver el valor de la variable msDenominacion. La Denominacion de la bd a q me conecto.
''' </summary>
''' <returns>Valor de la variable</returns>
''' <remarks>Llamada desde: frmLogin ; Tiempo m�ximo: 0</remarks>
Public Property Get Denominacion() As String
    Denominacion = msDenominacion
End Property

''' <summary>
''' Establacer el valor de la variable msDenominacion. La Denominacion de la bd a q me conecto.
''' </summary>
''' <param name="Value">Valor de la variable</param>
''' <remarks>Llamada desde: frmLogin ; Tiempo m�ximo: 0</remarks>
Public Property Let Denominacion(ByVal Value As String)
    msDenominacion = Value
End Property

''' <summary>
''' Devolver el valor de la variable mvServidor. El Servidor de la bd a q me conecto.
''' </summary>
''' <returns>Valor de la variable</returns>
''' <remarks>Llamada desde: frmLogin ; Tiempo m�ximo: 0</remarks>
Public Property Get Servidor() As Variant
    Servidor = mvServidor
End Property

''' <summary>
''' Establacer el valor de la variable mvServidor. El Servidor de la bd a q me conecto.
''' </summary>
''' <param name="Value">Valor de la variable</param>
''' <remarks>Llamada desde: frmLogin ; Tiempo m�ximo: 0</remarks>
Public Property Let Servidor(ByVal Value As Variant)
    mvServidor = Value
End Property

''' <summary>
''' Devolver el valor de la variable mvNombre. El Nombre de la bd a q me conecto.
''' </summary>
''' <returns>Valor de la variable</returns>
''' <remarks>Llamada desde: frmLogin ; Tiempo m�ximo: 0</remarks>
Public Property Get Nombre() As Variant
    Nombre = mvNombre
End Property

''' <summary>
''' Establacer el valor de la variable mvNombre. El Nombre de la bd a q me conecto.
''' </summary>
''' <param name="Value">Valor de la variable</param>
''' <remarks>Llamada desde: frmLogin ; Tiempo m�ximo: 0</remarks>
Public Property Let Nombre(ByVal Value As Variant)
    mvNombre = Value
End Property

''' <summary>
''' Devolver el valor de la variable mvUsuario. El Usuario de la bd a q me conecto.
''' </summary>
''' <returns>Valor de la variable</returns>
''' <remarks>Llamada desde: frmLogin ; Tiempo m�ximo: 0</remarks>
Public Property Get Usuario() As Variant
    Usuario = mvUsuario
End Property

''' <summary>
''' Establacer el valor de la variable mvUsuario. El Usuario de la bd a q me conecto.
''' </summary>
''' <param name="Value">Valor de la variable</param>
''' <remarks>Llamada desde: frmLogin ; Tiempo m�ximo: 0</remarks>
Public Property Let Usuario(ByVal Value As Variant)
    mvUsuario = Value
End Property

''' <summary>
''' Devolver el valor de la variable mvPassword. El Password de la bd a q me conecto.
''' </summary>
''' <returns>Valor de la variable</returns>
''' <remarks>Llamada desde: frmLogin ; Tiempo m�ximo: 0</remarks>
Public Property Get Password() As Variant
    Password = mvPassword
End Property

''' <summary>
''' Establacer el valor de la variable mvPassword. El Password de la bd a q me conecto.
''' </summary>
''' <param name="Value">Valor de la variable</param>
''' <remarks>Llamada desde: frmLogin ; Tiempo m�ximo: 0</remarks>
Public Property Let Password(ByVal Value As Variant)
    mvPassword = Value
End Property

''' <summary>
''' Devolver el valor de la variable mvImpUsuario. El Impersonate Usuario de la bd a q me conecto.
''' <returns>Valor de la variable</returns>
''' <remarks>Llamada desde: frmLogin ; Tiempo m�ximo: 0</remarks>
Public Property Get ImpUsuario() As Variant
    ImpUsuario = mvImpUsuario
End Property

''' <summary>
''' Establacer el valor de la variable mvImpUsuario. El Impersonate Usuario de la bd a q me conecto.
''' </summary>
''' <param name="Value">Valor de la variable</param>
''' <remarks>Llamada desde: frmLogin ; Tiempo m�ximo: 0</remarks>
Public Property Let ImpUsuario(ByVal Value As Variant)
    mvImpUsuario = Value
End Property

''' <summary>
''' Devolver el valor de la variable mvImpPassword. El Impersonate Password de la bd a q me conecto.
''' </summary>
''' <returns>Valor de la variable</returns>
''' <remarks>Llamada desde: frmLogin ; Tiempo m�ximo: 0</remarks>
Public Property Get ImpPassword() As Variant
    ImpPassword = mvImpPassword
End Property

''' <summary>
''' Establacer el valor de la variable mvImpPassword. El Impersonate Password de la bd a q me conecto.
''' </summary>
''' <param name="Value">Valor de la variable</param>
''' <remarks>Llamada desde: frmLogin ; Tiempo m�ximo: 0</remarks>
Public Property Let ImpPassword(ByVal Value As Variant)
    mvImpPassword = Value
End Property

''' <summary>
''' Devolver el valor de la variable mvImpDominio. El Impersonate Dominio de la bd a q me conecto.
''' </summary>>
''' <returns>Valor de la variable</returns>
''' <remarks>Llamada desde: frmLogin ; Tiempo m�ximo: 0</remarks>
Public Property Get ImpDominio() As Variant
    ImpDominio = mvImpDominio
End Property

''' <summary>
''' Establacer el valor de la variable mvImpDominio. El Impersonate Dominio de la bd a q me conecto.
''' </summary>
''' <param name="Value">Valor de la variable</param>
''' <remarks>Llamada desde: frmLogin ; Tiempo m�ximo: 0</remarks>
Public Property Let ImpDominio(ByVal Value As Variant)
    mvImpDominio = Value
End Property

''' <summary>
''' Devolver el valor de la variable mbPrincipal. Si la bd a q me conecto es la principal
''' </summary>
''' <returns>Valor de la variable</returns>
''' <remarks>Llamada desde: frmLogin ; Tiempo m�ximo: 0</remarks>
Public Property Get Principal() As Boolean
    Principal = mbPrincipal
End Property

''' <summary>
''' Establacer el valor de la variable mbPrincipal. Si la bd a q me conecto es la principal
''' </summary>
''' <param name="Value">Valor de la variable</param>
''' <remarks>Llamada desde: frmLogin ; Tiempo m�ximo: 0</remarks>
Public Property Let Principal(ByVal Value As Boolean)
    mbPrincipal = Value
End Property

''' <summary>
''' Devolver el valor de la variable mbLogin. Si la bd a q me conecto es la bd sobre la q hacer login
''' </summary>
''' <returns>Valor de la variable</returns>
''' <remarks>Llamada desde: frmLogin ; Tiempo m�ximo: 0</remarks>
Public Property Get Login() As Boolean
    Login = mbLogin
End Property

''' <summary>
''' Establacer el valor de la variable mbLogin. Si la bd a q me conecto es la bd sobre la q hacer login
''' </summary>
''' <param name="Value">Valor de la variable</param>
''' <remarks>Llamada desde: frmLogin ; Tiempo m�ximo: 0</remarks>
Public Property Let Login(ByVal Value As Boolean)
    mbLogin = Value
End Property

''' <summary>
''' Devolver el valor de la variable mbActiva. Si la bd a q me conecto esta Activa
''' </summary>
''' <returns>Valor de la variable</returns>
''' <remarks>Llamada desde: frmLogin ; Tiempo m�ximo: 0</remarks>
Public Property Get Activa() As Boolean
    Activa = mbActiva
End Property

''' <summary>
''' Establacer el valor de la variable mbActiva. Si la bd a q me conecto esta Activa
''' </summary>
''' <param name="Value">Valor de la variable</param>
''' <remarks>Llamada desde: frmLogin ; Tiempo m�ximo: 0</remarks>
Public Property Let Activa(ByVal Value As Boolean)
    mbActiva = Value
End Property

''' <summary>
''' Lee de bbdd las posibles BD a las que conectarse.
''' </summary>
''' <returns>Recordset</returns>
''' <remarks>Llamada desde: frmLogin ; Tiempo m�ximo: 0,2</remarks>
Public Function CargarBasesDeDatos() As adodb.Recordset
    Dim AdoRes As New adodb.Recordset
    Dim adoComm As New adodb.Command
    adoComm.CommandType = adCmdStoredProc
    adoComm.CommandText = "FSGS_DEVOLVER_BASES_DATOS_SECUNDARIAS"
    Set adoComm.ActiveConnection = Conexion.ADOCon

    adoComm.Prepared = True
    Set AdoRes = New adodb.Recordset
    Set AdoRes = adoComm.Execute
    Set adoComm = Nothing
    
    If Not AdoRes.eof Then
        Set CargarBasesDeDatos = AdoRes
    Else
        Set CargarBasesDeDatos = Nothing
    End If
End Function
