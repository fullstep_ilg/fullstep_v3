VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CCampo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Private m_lID As Long
'Private m_iNum As Integer
Private m_sCod As String
Private m_sDen As String
'Private m_sIdioma As String
Private m_bObligatorio As String
Private m_bInterno As Boolean
Private m_vValor As Variant
Private m_vValorNum As Variant
Private m_vValorText As Variant
Private m_vValorFec As Variant
Private m_vValorBool As Variant
Private m_Ambito As AmbitoDelCampoDePedido
Private m_Origen As AtributosLineaCatalogo 'Origen del campo de pedido, si es de solicitud, del maestro de atributos...
Private m_udtTipo As TiposDeAtributos
Private m_vMinimo As Variant
Private m_vMaximo As Variant
Private m_udtTipoIntroduccion As TAtributoIntroduccion
Private m_vAtribID As Variant
Private m_bListaExterna As Boolean
Private m_bMostrarEnRecep As Boolean

Public Property Let Id(ByVal Data As Long)
    Let m_lID = Data
End Property

Public Property Get Id() As Long
    Id = m_lID
End Property


Public Property Let Den(ByVal Data As String)
    Let m_sDen = Data
End Property

Public Property Get Den() As String
    Den = m_sDen
End Property


Public Property Let Obligatorio(ByVal Data As Boolean)
    Let m_bObligatorio = Data
End Property

Public Property Get Obligatorio() As Boolean
    Obligatorio = m_bObligatorio
End Property
Public Property Let Interno(ByVal Data As Boolean)
    Let m_bInterno = Data
End Property

Public Property Get Interno() As Boolean
    Interno = m_bInterno
End Property

Public Property Let Ambito(ByVal Data As AmbitoDelCampoDePedido)
    Let m_Ambito = Data
End Property
Public Property Get Ambito() As AmbitoDelCampoDePedido
    Ambito = m_Ambito
End Property

Public Property Let Origen(ByVal Data As AtributosLineaCatalogo)
    Let m_Origen = Data
End Property
Public Property Get Origen() As AtributosLineaCatalogo
    Origen = m_Origen
End Property

Public Property Let Cod(ByVal Data As String)
    Let m_sCod = Data
End Property
Public Property Get Cod() As String
    Cod = m_sCod
End Property

Public Property Let Valor(ByVal Data As Variant)
    Let m_vValor = Data
End Property
Public Property Get Valor() As Variant
    Valor = m_vValor
End Property

Public Property Let ValorNum(ByVal vData As Variant)
    m_vValorNum = vData
End Property
Public Property Get ValorNum() As Variant
    ValorNum = m_vValorNum
End Property

Public Property Let ValorText(ByVal vData As Variant)
    m_vValorText = vData
End Property
Public Property Get ValorText() As Variant
    ValorText = m_vValorText
End Property
    
Public Property Let ValorFec(ByVal vData As Variant)
    m_vValorFec = vData
End Property
Public Property Get ValorFec() As Variant
    ValorFec = m_vValorFec
End Property
    
Public Property Let ValorBool(ByVal vData As Variant)
    m_vValorBool = vData
End Property
Public Property Get ValorBool() As Variant
    ValorBool = m_vValorBool
End Property

Public Property Let Tipo(ByVal vData As TiposDeAtributos)
    m_udtTipo = vData
End Property
Public Property Get Tipo() As TiposDeAtributos
    Tipo = m_udtTipo
End Property

Public Property Get Minimo() As Variant
    Minimo = m_vMinimo
End Property
Public Property Let Minimo(ByVal vData As Variant)
    m_vMinimo = vData
End Property

Public Property Get Maximo() As Variant
    Maximo = m_vMaximo
End Property
Public Property Let Maximo(ByVal vData As Variant)
    m_vMaximo = vData
End Property


Public Property Let TipoIntroduccion(ByVal vData As TAtributoIntroduccion)
    m_udtTipoIntroduccion = vData
End Property
Public Property Get TipoIntroduccion() As TAtributoIntroduccion
    TipoIntroduccion = m_udtTipoIntroduccion
End Property

Public Property Let AtribID(ByVal vData As Variant)
    m_vAtribID = vData
End Property
Public Property Get AtribID() As Variant
    AtribID = m_vAtribID
End Property

Public Property Let Lista_Externa(ByVal vData As Boolean)
    m_bListaExterna = vData
End Property
Public Property Get Lista_Externa() As Boolean
    Lista_Externa = m_bListaExterna
End Property

Public Property Let MostrarEnRecep(ByVal vData As Boolean)
    m_bMostrarEnRecep = vData
End Property
Public Property Get MostrarEnRecep() As Boolean
    MostrarEnRecep = m_bMostrarEnRecep
End Property
