VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CConfVistasCalGlobal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private mCol As Collection
Private m_oConexion As CConexion
Private m_bEOF As Boolean

Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum

''' <summary>
''' A�ade (al objeto CConfVistaCalGlobal) una configuraci�n de vista global de calificaci�n en la comparativa QA
''' </summary>
''' <param name="iVista">ID de la vista</param>
''' <param name="sNombre">Nombre de la vista</param>
''' <param name="iTipoVision">Se ve: 0 - grid VarsProve 1- grid ProveVars</param>
''' <param name="bCalificaciones">Visibilidad de columna Calificaciones</param>
''' <param name="bPuntos">Visibilidad de columna Puntos</param>
''' <param name="bMateriales_Proceso">Solo materiales del proceso o no solo</param>
''' <param name="iDecimales">Decimales de los puntos</param>
''' <param name="dblwidth1_gr_var">En grid VarsProve, ancho de grupo Variables</param>
''' <param name="dblwidth1_cod_var">En grid VarsProve, ancho de columna Cod Variable</param>
''' <param name="dblwidth1_den_var">En grid VarsProve, ancho de columna Den Variable</param>
''' <param name="dblwidth1_gr_prove">En grid VarsProve, ancho de grupo Proveedor</param>
''' <param name="dblwidth1_cal">En grid VarsProve, ancho de columna Calificaciones</param>
''' <param name="dblwidth1_punt">En grid VarsProve, ancho de columna Puntos</param>
''' <param name="dblwidth2_gr_prove">En grid ProveVars, ancho de grupo Proveedor</param>
''' <param name="dblwidth2_cod_prove">En grid ProveVars, ancho de columna Cod Proveedor</param>
''' <param name="dblwidth2_den_prove">En grid ProveVars, ancho de columna Den Proveedor</param>
''' <param name="dblwidth2_gr_var">En grid ProveVars, ancho de grupo Variables</param>
''' <param name="dblwidth2_cal">En grid ProveVars, ancho de columna Calificaciones</param>
''' <param name="dblwidth2_punt">En grid ProveVars, ancho de columna Puntos</param>
''' <param name="dblwidth3_gr_unqa">Ancho del grupo Unidades de negocio</param>
''' <param name="iPyme">ID de la pyme</param>
''' <param name="udtTipoVista">0- vista inicial 5- otra vista</param>
''' <param name="vIndice">Indice donde a�adir</param>
''' <returns>Un objeto CConfVistaCalGlobal</returns>
''' <remarks>Llamada desde: CargarTodasLasVistas    frmComparativaQA/ConfiguracionVistaActualQA; Tiempo m�ximo:0,1</remarks>
Public Function Add(ByVal iVista As Integer, ByVal sNombre As String, ByVal iTipoVision As Integer, _
ByVal bCalificaciones As Boolean, ByVal bPuntos As Boolean, ByVal bMateriales_Proceso As Boolean, _
ByVal iDecimales As Integer, Optional ByVal dblwidth1_gr_var As Double, Optional ByVal dblwidth1_cod_var As Double, _
Optional ByVal dblwidth1_den_var As Double, Optional ByVal dblwidth1_gr_prove As Double, Optional ByVal dblwidth1_cal As Double, _
Optional ByVal dblwidth1_punt As Double, Optional ByVal dblwidth2_gr_prove As Double, Optional ByVal dblwidth2_cod_prove As Double, _
Optional ByVal dblwidth2_den_prove As Double, Optional ByVal dblwidth2_gr_var As Double, Optional ByVal dblwidth2_cal As Double, _
Optional ByVal dblwidth2_punt As Double, Optional ByVal dblwidth3_gr_unqa As Double, _
Optional ByVal iPyme As Integer, Optional ByVal udtTipoVista As TipoDeVistaQA, Optional ByVal vIndice As Variant) As CConfVistaCalGlobal
    'create a new object
    Dim objnewmember As CConfVistaCalGlobal

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New CConfVistaCalGlobal

    objnewmember.Vista = iVista
    objnewmember.Nombre = sNombre
    objnewmember.TipoVista = udtTipoVista
    objnewmember.TipoVision = iTipoVision
    objnewmember.Calificaciones = bCalificaciones
    objnewmember.Puntos = bPuntos
    objnewmember.Materiales_Proceso = bMateriales_Proceso
    objnewmember.decimales = iDecimales
    objnewmember.Width1_grupo_var = dblwidth1_gr_var
    objnewmember.Width1_cod_var = dblwidth1_cod_var
    objnewmember.Width1_den_var = dblwidth1_den_var
    objnewmember.Width1_grupo_prove = dblwidth1_gr_prove
    objnewmember.Width1_calificacion = dblwidth1_cal
    objnewmember.Width1_puntos = dblwidth1_punt
    objnewmember.Width2_grupo_prove = dblwidth2_gr_prove
    objnewmember.Width2_cod_prove = dblwidth2_cod_prove
    objnewmember.Width2_den_prove = dblwidth2_den_prove
    objnewmember.Width2_grupo_var = dblwidth2_gr_var
    objnewmember.Width2_calificacion = dblwidth2_cal
    objnewmember.Width2_puntos = dblwidth2_punt
    objnewmember.Width3_grupo_unqa = dblwidth3_gr_unqa
    objnewmember.Pyme = iPyme
    objnewmember.Indice = vIndice

    Set objnewmember.Conexion = m_oConexion
       
    If Not IsMissing(vIndice) And Not IsNull(vIndice) Then
        objnewmember.Indice = vIndice
        mCol.Add objnewmember, CStr(vIndice)
    Else
        mCol.Add objnewmember, CStr(iVista)
    End If
    
    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CConfVistasCalGlobal", "Add", ERR, Erl)
      Exit Function
   End If

End Function

Public Function AddComboVista(Optional ByVal sNombre As String, Optional ByVal iVista As Integer, Optional ByVal iTipoVision As Integer, Optional ByVal udtTipoVista As TipoDeVistaQA) As CConfVistaCalGlobal

    'create a new object
    Dim objnewmember As CConfVistaCalGlobal

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New CConfVistaCalGlobal

    objnewmember.Vista = iVista
    objnewmember.Nombre = sNombre
    objnewmember.TipoVision = iTipoVision
    objnewmember.TipoVista = udtTipoVista

    Set objnewmember.Conexion = m_oConexion

    mCol.Add objnewmember, CStr(iVista)
    'return the object created
    Set AddComboVista = objnewmember
    Set objnewmember = Nothing

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CConfVistasCalGlobal", "AddComboVista", ERR, Erl)
      Exit Function
   End If

End Function

Public Sub CargarCombosVistas(Optional ByVal Pyme As Variant)
Dim sConsulta As String
Dim AdoRes As New adodb.Recordset
Dim adofldNombre As adodb.Field
Dim adofldVista As adodb.Field
Dim adofldTipoVision As adodb.Field
Dim adofldTipoVista As adodb.Field
 

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
sConsulta = "SELECT VISTA, NOM, TIPOVISION, (case when VISTA = 0 then 0 else 5 end) as TIPOVISTA FROM CONF_VISTAS_CAL_GLOBAL WITH (NOLOCK) "
If Not IsMissing(Pyme) Then
    sConsulta = sConsulta & " WHERE PYME = " & Pyme
End If
sConsulta = sConsulta & " ORDER BY VISTA,NOM "

AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly

Set adofldNombre = AdoRes.Fields("NOM")
Set adofldVista = AdoRes.Fields("VISTA")
Set adofldTipoVision = AdoRes.Fields("TIPOVISION")
Set adofldTipoVista = AdoRes.Fields("TIPOVISTA")

While Not AdoRes.eof
    Me.AddComboVista adofldNombre.Value, adofldVista.Value, adofldTipoVision, adofldTipoVista
    AdoRes.MoveNext
Wend

AdoRes.Close
Set adofldNombre = Nothing
Set adofldVista = Nothing
Set adofldTipoVision = Nothing
Set adofldTipoVista = Nothing
Set AdoRes = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CConfVistasCalGlobal", "CargarCombosVistas", ERR, Erl)
      Exit Sub
   End If

End Sub

Friend Property Set Conexion(ByVal con As CConexion)
    Set m_oConexion = con
End Property
Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property

Public Property Get Count() As Long
    If mCol Is Nothing Then
        Count = 0
    Else
         Count = mCol.Count
    End If
End Property

Public Property Get eof() As Boolean
    eof = m_bEOF
End Property

Friend Property Let eof(ByVal b As Boolean)
    m_bEOF = b
End Property

Public Property Get Item(vntIndexKey As Variant) As CConfVistaCalGlobal
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
     Set NewEnum = mCol.[_NewEnum]
End Property

Public Sub Remove(vntIndexKey As Variant)
On Error GoTo ERROR

    mCol.Remove vntIndexKey

ERROR:

End Sub

Private Sub Class_Initialize()
    Set mCol = New Collection
End Sub

Private Sub Class_Terminate()
    On Error Resume Next
    Set mCol = Nothing
    Set m_oConexion = Nothing
End Sub

''' <summary>
''' Carga las vistas de calidad creadas para la Comparativa QA
''' </summary>
''' <param name="iVista">ID de la vista</param>
''' <param name="UsarIndice">Usar o no indice para la colecci�n</param>
''' <param name="PYME">ID de la pyme</param>
''' <remarks>Llamada desde: frmComparativaQA ; Tiempo m�ximo: 0,1</remarks>
Public Sub CargarTodasLasVistas(Optional ByVal iVista As Integer, Optional ByVal UsarIndice As Boolean, Optional ByVal Pyme As Variant)
    Dim AdoRes As New adodb.Recordset
    Dim sConsulta As String
    Dim lIndice As Long
    Dim adofldVista As adodb.Field
    Dim adofldNombre As adodb.Field
    Dim adofldTipoVision As adodb.Field
    Dim adofldCalifs As adodb.Field
    Dim adofldPuntos As adodb.Field
    Dim adofldMat As adodb.Field
    Dim adofldDec As adodb.Field
    Dim adofldWidth1_gr_var As adodb.Field
    Dim adofldWidth1_cod_var As adodb.Field
    Dim adofldWidth1_den_var As adodb.Field
    Dim adofldWidth1_gr_prove As adodb.Field
    Dim adofldWidth1_cal As adodb.Field
    Dim adofldWidth1_punt As adodb.Field
    Dim adofldWidth2_gr_prove As adodb.Field
    Dim adofldWidth2_cod_prove As adodb.Field
    Dim adofldWidth2_den_prove As adodb.Field
    Dim adofldWidth2_gr_var As adodb.Field
    Dim adofldWidth2_cal As adodb.Field
    Dim adofldWidth2_punt As adodb.Field
    Dim adofldPyme As adodb.Field
    Dim adofldTipoVista As adodb.Field
    Dim adofldWidth3_gr_Unqa As adodb.Field
    Dim sWhere As String
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sConsulta = "SELECT VISTA, NOM, TIPOVISION, CALIFS, PUNTOS, MAT, DEC, ISNULL(WIDTH1_GR_VAR,1000) WIDTH1_GR_VAR, ISNULL(WIDTH1_COD_VAR,1000) WIDTH1_COD_VAR,"
    sConsulta = sConsulta & " ISNULL(WIDTH1_DEN_VAR,1000) WIDTH1_DEN_VAR, ISNULL(WIDTH1_GR_PROVE,1000) WIDTH1_GR_PROVE,"
    sConsulta = sConsulta & " ISNULL(WIDTH1_CAL,1000) WIDTH1_CAL, ISNULL(WIDTH1_PUNT,1000) WIDTH1_PUNT, ISNULL(WIDTH2_GR_PROVE,1000) WIDTH2_GR_PROVE,"
    sConsulta = sConsulta & " ISNULL(WIDTH2_COD_PROVE,1000) WIDTH2_COD_PROVE, ISNULL(WIDTH2_DEN_PROVE,1000) WIDTH2_DEN_PROVE, ISNULL(WIDTH2_GR_VAR,1000) WIDTH2_GR_VAR,"
    sConsulta = sConsulta & " ISNULL(WIDTH2_CAL,1000) WIDTH2_CAL, ISNULL(WIDTH2_PUNT,1000) WIDTH2_PUNT, PYME, (CASE WHEN VISTA = 0 THEN 0 ELSE 5 END) AS TIPOVISTA,"
    sConsulta = sConsulta & " ISNULL(WIDTH3_GR_UNQA,1000) WIDTH3_GR_UNQA FROM CONF_VISTAS_CAL_GLOBAL WITH (NOLOCK) "
    sWhere = ""
    If Not IsMissing(iVista) Then
        sWhere = sWhere & " VISTA=" & iVista
    End If
    If Not IsMissing(Pyme) Then
        If sWhere <> "" Then
            sWhere = sWhere & " AND "
        End If
        sWhere = sWhere & " PYME = " & Pyme
    End If
    If sWhere <> "" Then
        sConsulta = sConsulta & " WHERE " & sWhere
    End If
    sConsulta = sConsulta & " ORDER BY VISTA"
    
    AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
          
    If AdoRes.eof Then
        AdoRes.Close
        Set AdoRes = Nothing
        If mCol Is Nothing Then
            Set mCol = Nothing
            Set mCol = New Collection
        End If
        Exit Sub
    Else
        If mCol Is Nothing Then
            Set mCol = Nothing
            Set mCol = New Collection
        End If
        
        Set adofldVista = AdoRes.Fields("VISTA")
        Set adofldNombre = AdoRes.Fields("NOM")
        Set adofldTipoVision = AdoRes.Fields("TIPOVISION")
        Set adofldCalifs = AdoRes.Fields("CALIFS")
        Set adofldPuntos = AdoRes.Fields("PUNTOS")
        Set adofldMat = AdoRes.Fields("MAT")
        Set adofldDec = AdoRes.Fields("DEC")
        Set adofldWidth1_gr_var = AdoRes.Fields("WIDTH1_GR_VAR")
        Set adofldWidth1_cod_var = AdoRes.Fields("WIDTH1_COD_VAR")
        Set adofldWidth1_den_var = AdoRes.Fields("WIDTH1_DEN_VAR")
        Set adofldWidth1_gr_prove = AdoRes.Fields("WIDTH1_GR_PROVE")
        Set adofldWidth1_cal = AdoRes.Fields("WIDTH1_CAL")
        Set adofldWidth1_punt = AdoRes.Fields("WIDTH1_PUNT")
        Set adofldWidth2_gr_prove = AdoRes.Fields("WIDTH2_GR_PROVE")
        Set adofldWidth2_cod_prove = AdoRes.Fields("WIDTH2_COD_PROVE")
        Set adofldWidth2_den_prove = AdoRes.Fields("WIDTH2_DEN_PROVE")
        Set adofldWidth2_gr_var = AdoRes.Fields("WIDTH2_GR_VAR")
        Set adofldWidth2_cal = AdoRes.Fields("WIDTH2_CAL")
        Set adofldWidth2_punt = AdoRes.Fields("WIDTH2_PUNT")
        Set adofldWidth3_gr_Unqa = AdoRes.Fields("WIDTH3_GR_UNQA")
        Set adofldPyme = AdoRes.Fields("PYME")
        Set adofldTipoVista = AdoRes.Fields("TIPOVISTA")
        
        If UsarIndice Then
            lIndice = 0
            
            While Not AdoRes.eof
                Me.Add adofldVista, adofldNombre, adofldTipoVision, adofldCalifs, adofldPuntos, adofldMat, adofldDec, adofldWidth1_gr_var, _
                    adofldWidth1_cod_var, adofldWidth1_den_var, adofldWidth1_gr_prove, adofldWidth1_cal, adofldWidth1_punt, _
                    adofldWidth2_gr_prove, adofldWidth2_cod_prove, adofldWidth2_den_prove, adofldWidth2_gr_var, _
                    adofldWidth2_cal, adofldWidth2_punt, adofldWidth3_gr_Unqa, adofldPyme, adofldTipoVista, lIndice

                AdoRes.MoveNext
                lIndice = lIndice + 1
            Wend
        Else
            While Not AdoRes.eof
                Me.Add adofldVista, adofldNombre, adofldTipoVision, adofldCalifs, adofldPuntos, adofldMat, adofldDec, adofldWidth1_gr_var, _
                    adofldWidth1_cod_var, adofldWidth1_den_var, adofldWidth1_gr_prove, adofldWidth1_cal, adofldWidth1_punt, _
                    adofldWidth2_gr_prove, adofldWidth2_cod_prove, adofldWidth2_den_prove, adofldWidth2_gr_var, _
                    adofldWidth2_cal, adofldWidth2_punt, adofldWidth3_gr_Unqa, adofldPyme, adofldTipoVista

                AdoRes.MoveNext
            Wend
        End If

        'Libera los objetos
        Set adofldVista = Nothing
        Set adofldNombre = Nothing
        Set adofldTipoVision = Nothing
        Set adofldCalifs = Nothing
        Set adofldPuntos = Nothing
        Set adofldMat = Nothing
        Set adofldDec = Nothing
        Set adofldWidth1_gr_var = Nothing
        Set adofldWidth1_cod_var = Nothing
        Set adofldWidth1_den_var = Nothing
        Set adofldWidth1_gr_prove = Nothing
        Set adofldWidth1_cal = Nothing
        Set adofldWidth1_punt = Nothing
        Set adofldWidth2_gr_prove = Nothing
        Set adofldWidth2_cod_prove = Nothing
        Set adofldWidth2_den_prove = Nothing
        Set adofldWidth2_gr_var = Nothing
        Set adofldWidth2_cal = Nothing
        Set adofldWidth2_punt = Nothing
        Set adofldPyme = Nothing
        Set adofldTipoVista = Nothing
        Set adofldWidth3_gr_Unqa = Nothing
        
        AdoRes.Close
        Set AdoRes = Nothing
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CConfVistasCalGlobal", "CargarTodasLasVistas", ERR, Erl)
      Exit Sub
   End If
End Sub


''' <summary>
''' En la comparativa QA el bot�n de vista por defecto debe deshabiltarse en caso de q este viendo la vista por
''' defecto del usuario. As� q si un usuario no tiene vista por defecto, aparte de q vera la del responsable al
''' entrar o la vista inicial si el responsable no tiene vista por defecto, el boton nunca debe deshabiltarse.
''' </summary>
''' <param name="Anyo">A�o del proceso</param>
''' <param name="GMN1">GMN1 del proceso</param>
''' <param name="Proce">ID del proceso</param>
''' <param name="Usu">Usuario conectado</param>
''' <returns>si un usuario tiene vista por defecto o no</returns>
''' <remarks>Llamada desde: frmComparativaQA/Form_Load; Tiempo m�ximo: 0,1</remarks>
Public Function ExisteVistaDefecto(ByVal Anyo As Integer, ByVal GMN1 As String, ByVal Proce As Long, ByVal USU As String) As Boolean
    Dim AdoRes As New adodb.Recordset
    Dim sConsulta As String
        
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sConsulta = "SELECT 1 FROM CONF_VISTAS_DEFECTO WITH(NOLOCK) "
    sConsulta = sConsulta & " WHERE ANYO=" & Anyo & " AND GMN1=" & StrToSQLNULL(GMN1) & " AND PROCE=" & Proce
    sConsulta = sConsulta & " AND USU=" & StrToSQLNULL(USU) & " AND VISTA_QA IS NOT NULL"

    AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    
    ExisteVistaDefecto = Not AdoRes.eof
    
    AdoRes.Close
    Set AdoRes = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CConfVistasCalGlobal", "ExisteVistaDefecto", ERR, Erl)
      Exit Function
   End If
End Function
