VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CGestorLisPerExternos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private mvarConexion As CConexion

Friend Property Set Conexion(ByVal vData As CConexion)
    Set mvarConexion = vData
End Property
Friend Property Get Conexion() As CConexion
    Set Conexion = mvarConexion
End Property

Private Sub Class_Terminate()
    Set mvarConexion = Nothing
End Sub

''' <summary>Funci�n que devuelve el listado externo que corresponda seg�n la pantalla de la aplicaci�n en la que estemos</summary>
''' <param "Origen">Formulario desde donde se le llama </param>
''' <param "sIdi">Idioma</param>
''' <returns>un recordset con el listado(s)</returns>
''' <remarks>Llamada desde: Los eventos Load de: frmEST, frmADJ, frmPROCE, frmRESREU, frmSELPROVE </remarks>
''' <remarks>Tiempo m�ximo: 0 sec </remarks>
Public Function DevolverLisPerExterno(ByVal Origen As String, ByVal sIdi As String) As adodb.Recordset
Dim sConsulta As String
Dim adoRecordset As adodb.Recordset

    On Error GoTo Error
    
    Set adoRecordset = New adodb.Recordset
    adoRecordset.CursorLocation = adUseClient
    

    sConsulta = "SELECT URL, DEN_" & sIdi & " " & _
                "FROM LISPER_EXTERNOS WITH(NOLOCK) " & _
                "WHERE FRM_ORIGEN = " & StrToSQLNULL(Origen)

    
    adoRecordset.CursorLocation = adUseClient

    adoRecordset.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
            
    Set adoRecordset.ActiveConnection = Nothing
    
    If adoRecordset.RecordCount = 0 Then
        Set DevolverLisPerExterno = Nothing
    Else
        Set DevolverLisPerExterno = adoRecordset
    End If
    
    Exit Function
    
Error:
    Set adoRecordset.ActiveConnection = Nothing
    Set DevolverLisPerExterno = Nothing
End Function

''' <summary>Funci�n que devuelve un booleano indicandonos si hay listados externos configurados para la pantalla donde nos encontremos</summary>
''' <param "Origen">Formulario desde donde se le llama </param>
''' <returns>True si hay listado configurado.</returns>
''' <remarks>Llamada desde: Los eventos Load de: frmEST, frmADJ, frmPROCE, frmRESREU, frmSELPROVE </remarks>
''' <remarks>Tiempo m�ximo: 0 sec </remarks>
Public Function HayLisPerExternos(ByVal Origen As String) As Boolean
Dim sConsulta As String
Dim adoRecordset As adodb.Recordset

    On Error GoTo Error
    
    Set adoRecordset = New adodb.Recordset
    adoRecordset.CursorLocation = adUseClient
    

    sConsulta = "SELECT URL " & _
                "FROM LISPER_EXTERNOS WITH(NOLOCK) " & _
                "WHERE FRM_ORIGEN = " & StrToSQLNULL(Origen)

    
    adoRecordset.CursorLocation = adUseClient

    adoRecordset.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
            
    Set adoRecordset.ActiveConnection = Nothing
    
    If adoRecordset.RecordCount = 0 Then
        HayLisPerExternos = False
    Else
        HayLisPerExternos = True
    End If
    
    Exit Function
    
Error:
    Set adoRecordset.ActiveConnection = Nothing
    HayLisPerExternos = False
End Function



