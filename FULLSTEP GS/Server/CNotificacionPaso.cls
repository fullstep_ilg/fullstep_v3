VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CNotificacionPaso"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum

Implements IBaseDatos

Private moConexion As CConexion

Private m_lID As Long
Private m_lPaso As Long
Private m_vPersona As Variant
Private m_vEmail As Variant
Private mdtFecAct As Variant
Private mvarIndice As Variant

Private m_adores As adodb.Recordset

Friend Property Set Conexion(ByVal con As CConexion)
    Set moConexion = con
End Property

Friend Property Get Conexion() As CConexion
    Set Conexion = moConexion
End Property

Public Property Get Id() As Long
    Id = m_lID
End Property

Public Property Let Id(ByVal Data As Long)
    m_lID = Data
End Property

Public Property Get Paso() As Long
    Paso = m_lPaso
End Property

Public Property Let Paso(ByVal Data As Long)
    m_lPaso = Data
End Property

Public Property Let Indice(ByVal varIndice As Variant)
    mvarIndice = varIndice
End Property
Public Property Get Indice() As Variant
    Indice = mvarIndice
End Property

Public Property Get FecAct() As Variant
    FecAct = mdtFecAct
End Property
Public Property Let FecAct(ByVal Data As Variant)
    mdtFecAct = Data
End Property

Public Property Get Persona() As Variant
    Persona = m_vPersona
End Property

Public Property Let Persona(sPer As Variant)
    m_vPersona = sPer
End Property

Public Property Let Email(ByVal vData As Variant)
    m_vEmail = vData
End Property
Public Property Get Email() As Variant
    Email = m_vEmail
End Property

Private Sub Class_Terminate()
    Set moConexion = Nothing
End Sub


Private Function IBaseDatos_AnyadirABaseDatos() As TipoErrorSummit
Dim sConsulta As String
Dim TESError As TipoErrorSummit
Dim AdoRes As adodb.Recordset
Dim bTransaccionEnCurso As Boolean
Dim sFSP As String
Dim i As Integer
Dim sDen As String


TESError.NumError = TESnoerror

If moConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CNotificacionPaso.AnyadirABAseDatos", "No se ha establecido la conexion"
End If
' *****************************************

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
    
    moConexion.ADOCon.Execute "BEGIN TRAN"
    bTransaccionEnCurso = True
    moConexion.ADOCon.Execute "SET XACT_ABORT OFF"
    
    
    sConsulta = "SELECT MAX(ID) ID FROM PASO_NOTIF"
    Set AdoRes = New adodb.Recordset
    AdoRes.Open sConsulta, moConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    If IsNull(AdoRes("id").Value) Then
        m_lID = 1
    Else
        m_lID = AdoRes("ID").Value + 1
    End If
    AdoRes.Close
    Set AdoRes = Nothing
    
    
    sConsulta = "INSERT INTO PASO_NOTIF (ID, PASO, PER) VALUES (" & m_lID
    sConsulta = sConsulta & "," & m_lPaso & "," & StrToSQLNULL(m_vPersona) & " )"
    moConexion.ADOCon.Execute sConsulta
    If moConexion.ADOCon.Errors.Count > 0 Then
        GoTo ERROR
    End If

    ''' Recuperar la fecha de insercion
    sConsulta = "SELECT FECACT FROM PASO_NOTIF WHERE ID =" & m_lID
    Set AdoRes = New adodb.Recordset
    AdoRes.Open sConsulta, moConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    mdtFecAct = AdoRes("FECACT").Value
    AdoRes.Close
    Set AdoRes = Nothing
    
    moConexion.ADOCon.Execute "COMMIT TRAN"
    bTransaccionEnCurso = False
       
    IBaseDatos_AnyadirABaseDatos = TESError
    Exit Function
Error_Cls:

    If Not AdoRes Is Nothing Then
        AdoRes.Close
        Set AdoRes = Nothing
    End If
                 
    IBaseDatos_AnyadirABaseDatos = basErrores.TratarError(moConexion.ADOCon.Errors)
                 
                 
    If bTransaccionEnCurso Then
        moConexion.ADOCon.Execute "ROLLBACK TRAN"
        bTransaccionEnCurso = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CNotificacionPaso", "IBaseDatos_AnyadirABaseDatos", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If
    
End Function

Private Function IBaseDatos_CambiarCodigo(CodigoNuevo As Variant) As TipoErrorSummit

End Function

Private Sub IBaseDatos_CancelarEdicion()

End Sub

Private Function IBaseDatos_ComprobarExistenciaEnBaseDatos() As Boolean

End Function

Private Function IBaseDatos_EliminarDeBaseDatos() As TipoErrorSummit

End Function

Private Function IBaseDatos_FinalizarEdicionEliminando() As TipoErrorSummit

Dim TESError As TipoErrorSummit
Dim btrans As Boolean
Dim sConsulta As String
Dim AdoRes As adodb.Recordset
Dim i As Integer


TESError.NumError = TESnoerror

'******************* Precondicion *******************
If moConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CNotificacionPaso.FinalizarEdicionEliminando", "No se ha establecido la conexion"
End If
'*****************************************************

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
    
    moConexion.ADOCon.Execute "BEGIN TRAN"
    moConexion.ADOCon.Execute "SET XACT_ABORT OFF"
    btrans = True
    moConexion.ADOCon.Execute "DELETE FROM PASO_NOTIF WHERE ID = " & m_lID
    moConexion.ADOCon.Execute "COMMIT TRAN"
    btrans = False
    Set m_adores = Nothing
    
    IBaseDatos_FinalizarEdicionEliminando = TESError
    
    Exit Function
    
Error_Cls:
    
    If moConexion.ADOCon.Errors.Count > 0 Then
        IBaseDatos_FinalizarEdicionEliminando = basErrores.TratarError(moConexion.ADOCon.Errors)
        If btrans Then
            moConexion.ADOCon.Execute "ROLLBACK TRAN"
            btrans = False
        End If
        Exit Function
    End If
    
    If m_adores.EditMode = 1 Then
        m_adores.CancelUpdate
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CNotificacionPaso", "IBaseDatos_FinalizarEdicionEliminando", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If

End Function

Private Function IBaseDatos_FinalizarEdicionModificando() As TipoErrorSummit
Dim TESError As TipoErrorSummit
Dim sConsulta As String
Dim bTransaccionEnCurso As Boolean
Dim sFSP As String
Dim i As Integer
Dim sTotalStr As String
Dim lIdACT1 As Long
Dim sCod As String
Dim sDen As String
Dim AdoRes As adodb.Recordset
Dim sDenAnterior As String


TESError.NumError = TESnoerror

'********* Precondicion *******************
If moConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CNotificacionPaso.FinalizarEdicionModificando", "No se ha establecido la conexion"
End If
'******************************************

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:

    moConexion.ADOCon.Execute "BEGIN TRAN"
    bTransaccionEnCurso = True
    moConexion.ADOCon.Execute "SET XACT_ABORT OFF"
    
    'Comprobamos si ha habido cambios en otra sesi�n
    Set m_adores = New adodb.Recordset
    sConsulta = "SELECT FECACT FROM PASO_NOTIF WHERE ID=" & m_lID
    m_adores.Open sConsulta, moConexion.ADOCon, adOpenStatic, adLockReadOnly
    
    If m_adores.eof Then
        m_adores.Close
        Set m_adores = Nothing
        TESError.NumError = TESDatoEliminado
        TESError.Arg1 = 178 ''"Notificador"
        IBaseDatos_FinalizarEdicionModificando = TESError
        moConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
        Exit Function
    End If
    
    If mdtFecAct <> m_adores("FECACT").Value Then
        m_adores.Close
        Set m_adores = Nothing
        TESError.NumError = TESInfActualModificada
        moConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
        IBaseDatos_FinalizarEdicionModificando = TESError
        Exit Function
    End If
    
    ''' Actualizar
    sConsulta = "UPDATE PASO_NOTIF SET PASO = " & m_lPaso & ", PER = " & StrToSQLNULL(m_vPersona)
    sConsulta = sConsulta & " WHERE ID = " & m_lID
    moConexion.ADOCon.Execute sConsulta
    If moConexion.ADOCon.Errors.Count > 0 Then
        GoTo ERROR
    End If
 
    'Recogemos el nuevo valor de FECACT
    m_adores.Requery
    mdtFecAct = m_adores("FECACT").Value
    m_adores.Close
    Set m_adores = Nothing
    
    moConexion.ADOCon.Execute "COMMIT TRAN"
    bTransaccionEnCurso = False
    
    IBaseDatos_FinalizarEdicionModificando = TESError
    
    Exit Function
    

Error_Cls:
    
    IBaseDatos_FinalizarEdicionModificando = basErrores.TratarError(moConexion.ADOCon.Errors)
    
    If Not m_adores Is Nothing Then
        m_adores.Close
        Set m_adores = Nothing
    End If
    
    If bTransaccionEnCurso Then
        moConexion.ADOCon.Execute "ROLLBACK"
        moConexion.ADOCon.Execute "SET XACT_ABORT OFF"
        bTransaccionEnCurso = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CNotificacionPaso", "IBaseDatos_FinalizarEdicionModificando", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If

End Function

Private Function IBaseDatos_IniciarEdicion(Optional ByVal Bloquear As Boolean, Optional ByVal UsuarioBloqueo As String) As TipoErrorSummit
Dim TESError As TipoErrorSummit


TESError.NumError = TESnoerror

'******************* Precondicion *******************
If moConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CNotificacionPaso.IniciarEdicion", "No se ha establecido la conexion"
End If
'*****************************************************

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set m_adores = New adodb.Recordset
    m_adores.Open "SELECT PASO_NOTIF.ID,PASO_NOTIF.PASO,PASO_NOTIF.PER,PER.EMAIL, PASO_NOTIF.FECACT FROM PASO_NOTIF INNER JOIN PER ON PASO_NOTIF.PER=PER.COD WHERE ID =" & m_lID, moConexion.ADOCon, adOpenKeyset, adLockOptimistic
    
    If m_adores.eof Then
        m_adores.Close
        Set m_adores = Nothing
        TESError.NumError = TESDatoEliminado
        TESError.Arg1 = 178 '''Notificador
        TESError.Arg2 = 1
        IBaseDatos_IniciarEdicion = TESError
        Exit Function
    End If
    
    m_vPersona = m_adores.Fields("PER").Value
    m_lPaso = m_adores.Fields("PASO").Value
    m_vEmail = m_adores.Fields("EMAIL").Value
    mdtFecAct = m_adores.Fields("FECACT").Value
    
    IBaseDatos_IniciarEdicion = TESError
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CNotificacionPaso", "IBaseDatos_IniciarEdicion", ERR, Erl)
      Exit Function
   End If

End Function



