VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CRelacTipos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CRelacTipos **********************************
'*             Autor : JVS
'*             Creada : 5/8/11
'****************************************************************


Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum
Private mCol As Collection
Private mvarConexion As CConexion
Public mvarEOF As Boolean

''' <summary>
''' Carga los tipos de relaci�n
''' </summary>
''' <param optional name="UsarIndice">Usa �ndice (s/n)</param>
''' <revision>JVS 24/08/2011</revision>
Public Sub CargarTodosLosRelacTipos(Optional ByVal UsarIndice As Boolean, Optional ByVal lId As Long, Optional ByVal sDen As String, Optional ByVal sIdi As String)
'ado  Dim rdores As rdoResultset
Dim rs As adodb.Recordset
Dim rsIdi As adodb.Recordset
Dim fldId As adodb.Field
Dim fldCod As adodb.Field
Dim fldPedido As adodb.Field
Dim fldFecAct As adodb.Field
    
Dim sConsulta As String
Dim sConsultaIdi As String
Dim lIndice As Long

Dim oDen As CMultiidiomas

'********* Precondicion **************************************
If mvarConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CRelacTipos.CargarTodosLosOfeEstados", "No se ha establecido la conexion"
    Exit Sub
End If
'*************************************************************

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

sConsulta = "SELECT TR.ID,TR.COD,TR.PEDIDO,TR.FECACT FROM GS_PROVETREL TR WITH (NOLOCK)"

If sDen <> "" Then
    sConsulta = sConsulta & " INNER JOIN GS_PROVETREL_IDI TRIDI WITH (NOLOCK) ON TRIDI.PROVETREL=TR.ID AND TRIDI.IDI= '" & sIdi & "' AND TRIDI.DEN='" & DblQuote(sDen) & "'"
End If
If lId <> 0 Then
    sConsulta = sConsulta & " WHERE TR.ID = " & lId
End If

sConsulta = sConsulta & " ORDER BY TR.COD"

Set rs = New adodb.Recordset
rs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly

If rs.eof Then
    Set mCol = Nothing
    Set mCol = New Collection
    rs.Close
    Set rs = Nothing
    Exit Sub
      
Else
    Set mCol = Nothing
    Set mCol = New Collection

    Set fldId = rs.Fields("ID")
    Set fldCod = rs.Fields("COD")
    Set fldPedido = rs.Fields("PEDIDO")
    Set fldFecAct = rs.Fields("FECACT")
    
    If UsarIndice Then
        
        lIndice = 0
        Set rsIdi = New adodb.Recordset
        While Not rs.eof
            
            Set oDen = New CMultiidiomas
            
            sConsultaIdi = "SELECT IDI.DEN,IDI.IDI FROM GS_PROVETREL_IDI AS IDI WITH (NOLOCK) WHERE IDI.PROVETREL = " & rs("ID").Value
            rsIdi.Open sConsultaIdi, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
            
            If rsIdi.eof Then
                rs.Close
                rsIdi.Close
                Set rs = Nothing
                Set rsIdi = Nothing
                Exit Sub
            Else
                While Not rsIdi.eof
                    oDen.Add rsIdi.Fields("IDI").Value, rsIdi.Fields("DEN").Value
                    rsIdi.MoveNext
                Wend
            End If
            Me.Add fldId.Value, fldCod.Value, oDen, fldPedido.Value, fldFecAct.Value, lIndice
            rsIdi.Close
        
            rs.MoveNext
            lIndice = lIndice + 1
        Wend
        Set rsIdi = Nothing
    Else
        Set rsIdi = New adodb.Recordset
        While Not rs.eof
            
            Set oDen = New CMultiidiomas
            
            sConsultaIdi = "SELECT GS_PROVETREL_IDI.DEN,GS_PROVETREL_IDI.IDI FROM GS_PROVETREL_IDI WITH (NOLOCK) WHERE GS_PROVETREL_IDI.PROVETREL = " & rs("ID")
        
            rsIdi.Open sConsultaIdi, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
        
            If rsIdi.eof Then
                rs.Close
                rsIdi.Close
                Set rs = Nothing
                Set rsIdi = Nothing
                Exit Sub
            Else
                While Not rsIdi.eof
                    oDen.Add rsIdi.Fields("IDI").Value, rsIdi.Fields("DEN").Value
                    rsIdi.MoveNext
                Wend
            End If
            Me.Add fldId.Value, fldCod.Value, oDen, fldPedido.Value, fldFecAct.Value
            rsIdi.Close

            rs.MoveNext
        Wend
        Set rsIdi = Nothing
    End If
    
    rs.Close
    Set rs = Nothing
    Set fldId = Nothing
    Set fldCod = Nothing
    Set oDen = Nothing
    Set fldPedido = Nothing
    Exit Sub
      
End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CRelacTipos", "CargarTodosLosRelacTipos", ERR, Erl)
      Exit Sub
   End If
End Sub


''' <summary>
''' Carga los tipos de relaci�n
''' </summary>
''' <param name="ID">Identificador</param>
''' <param name="Cod">C�digo</param>
''' <param name="Den">Denominaci�n (multiidioma)</param>
''' <param name="Pedido">Pedido</param>
''' <param name="FecAct">Fecha de actualizaci�n</param>
''' <param optional name="varIndice">Indice</param>
''' <returns>Tipo de relaci�n</returns>
''' <revision>JVS 24/08/2011</revision>
Public Function Add(ByVal Id As Long, ByVal Cod As String, ByVal Den As CMultiidiomas, ByVal Pedido As Boolean, ByVal FecAct As Variant, Optional ByVal varIndice As Variant) As CRelacTipo
    'create a new object
    Dim objnewmember As CRelacTipo
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New CRelacTipo
   
    objnewmember.Id = Id
    objnewmember.Cod = Cod
    Set objnewmember.Descripciones = Den
    objnewmember.PermitePedidos = Pedido
    objnewmember.FecAct = FecAct
    
    Set objnewmember.Conexion = mvarConexion
    
    If Not IsMissing(varIndice) And Not IsNull(varIndice) Then
        objnewmember.Indice = varIndice
        mCol.Add objnewmember, CStr(varIndice)
    Else
    'set the properties passed into the method
       mCol.Add objnewmember, CStr(Id)
    End If
    
    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CRelacTipos", "Add", ERR, Erl)
      Exit Function
   End If

End Function

Public Property Get Item(vntIndexKey As Variant) As CRelacTipo
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Friend Property Set Conexion(ByVal con As CConexion)
Set mvarConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = mvarConexion
End Property

Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
     Count = mCol.Count
End If


End Property


Public Sub Remove(vntIndexKey As Variant)
   
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    mCol.Remove vntIndexKey
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CRelacTipos", "Remove", ERR, Erl)
      Exit Sub
   End If
End Sub


Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
     Set NewEnum = mCol.[_NewEnum]
End Property


Private Sub Class_Initialize()
    Set mCol = New Collection
End Sub


Private Sub Class_Terminate()
    'destroys collection when this class is terminated
    Set mCol = Nothing
    Set mvarConexion = Nothing
End Sub


Public Function EliminarTiposDeRelacionDeBaseDatos(aCodigos As Variant) As TipoErrorSummit
'*********************************************************************
'*** Descripci�n: Elimina el/los estado/s de las ofertas           ***
'*** seleccionados de la base de datos.                            ***
'*** Par�metros:  Codigos ::>  Es un array con los c�digos de las  ***
'***                           estados de las ofertas a eliminar.  ***
'*** Valor que devuelve: Un TipoErrorSummit que es una estructura  ***
'***                     de datos definida en CTiposDeDatos, y, en ***
'***                     caso de ocurrir alg�n error al borrar, el ***
'***                     campo Arg1 de TipoErrorSummit contendr�   ***
'***                     un array bidimensional con los datos      ***
'***                     (c�digo,codigo de datos relacionado,      ***
'***                     bookmark de la linea de denominaci�n del  ***
'***                     OfeEstado actual) de los destinos que no  ***
'***                     pudieron ser eliminados                   ***
'*********************************************************************
    Dim iRes As Integer
    Dim i As Integer
    Dim aNoEliminados() As Variant
    Dim udtTESError As TipoErrorSummit
    Dim btrans As Boolean
    Dim AdoRes As adodb.Recordset
    Dim sConsulta As String
    
    
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    udtTESError.NumError = TESnoerror
    iRes = 0
    If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
 
        For i = 1 To UBound(aCodigos)
            mvarConexion.ADOCon.Execute "BEGIN TRANSACTION"
            btrans = True
            mvarConexion.ADOCon.Execute "DELETE FROM GS_PROVETREL_IDI WHERE PROVETREL=" & aCodigos(i)
            If mvarConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
            mvarConexion.ADOCon.Execute "DELETE FROM GS_PROVETREL WHERE ID=" & aCodigos(i)
            If mvarConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
            If (btrans) Then
                mvarConexion.ADOCon.Execute "COMMIT TRANSACTION"
            End If
            btrans = False
        Next i
        
        If iRes > 0 Then
            udtTESError.NumError = TESImposibleEliminar
            udtTESError.Arg1 = aNoEliminados
        End If
        EliminarTiposDeRelacionDeBaseDatos = udtTESError
        Exit Function
        
        'Zona de tratamiento del error
Error_Cls:
        udtTESError = basErrores.TratarError(mvarConexion.ADOCon.Errors)
        If (mvarConexion.ADOCon.Errors(0).NativeError = 547) Then
            ReDim Preserve aNoEliminados(3, iRes)
            aNoEliminados(0, iRes) = aCodigos(i)
            aNoEliminados(1, iRes) = udtTESError.Arg1
            aNoEliminados(2, iRes) = i
                        
            iRes = iRes + 1
            If (btrans) Then
                mvarConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
                btrans = False
            End If
            Resume Next
        Else
            mvarConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
            EliminarTiposDeRelacionDeBaseDatos = udtTESError
        End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CRelacTipos", "EliminarTiposDeRelacionDeBaseDatos", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If
End Function

''' <summary>
'''  Carga de todos los tipos de relaci�n a partir de uno dado
''' </summary>
''' <param name="iNumMaximo">N�mero m�ximo de tipos a obtener</param>
''' <param name="sCodTRel">C�digo de tipo de relaci�n</param>
''' <param optional name="bCoincidenciaTotal">Coincidencia total (s/n)</param>
''' <returns></returns>
''' <remarks>Llamada desde: ; Tiempo m�ximo: 1 sec</remarks>
''' <revision>JVS 24/08/2011</revision>
Public Sub CargarTodosLosTiposRelacionDesde(ByVal iNumMaximo As Integer, _
                                    Optional ByVal sCodTRel As String, _
                                    Optional ByVal bCoincidenciaTotal As Boolean = False)

Dim rs As adodb.Recordset
Dim rsIdi As adodb.Recordset
Dim fldId As adodb.Field
Dim fldCod As adodb.Field
Dim fldPedido As adodb.Field
Dim fldFecAct As adodb.Field
    
Dim sConsulta As String
Dim sConsultaIdi As String
Dim lIndice As Long

Dim oDen As CMultiidiomas

'********* Precondicion **************************************
If mvarConexion Is Nothing Then
    ERR.Raise vbObjectError + 613, "CRelacTipos.CargarTodosLosTiposRelacionDesde", "No se ha establecido la conexion"
    Exit Sub
End If
'*************************************************************

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

sConsulta = "SELECT DISTINCT TOP " & iNumMaximo & " TR.ID,TR.COD,TR.PEDIDO,TR.FECACT FROM GS_PROVETREL TR WITH (NOLOCK)"

If (bCoincidenciaTotal) Then
    sConsulta = sConsulta & " WHERE TR.ID=" & sCodTRel
Else
    sConsulta = sConsulta & " WHERE TR.ID>=" & sCodTRel
End If

sConsulta = sConsulta & " ORDER BY TR.COD"

Set rs = New adodb.Recordset
rs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly

If rs.eof Then
    Set mCol = Nothing
    Set mCol = New Collection
    rs.Close
    Set rs = Nothing
    Exit Sub
      
Else
    Set mCol = Nothing
    Set mCol = New Collection

    Set fldId = rs.Fields("ID")
    Set fldCod = rs.Fields("COD")
    Set fldPedido = rs.Fields("PEDIDO")
    Set fldFecAct = rs.Fields("FECACT")
    
    Set rsIdi = New adodb.Recordset
    While Not rs.eof
        
        Set oDen = New CMultiidiomas
        
        sConsultaIdi = "SELECT GS_PROVETREL_IDI.DEN,GS_PROVETREL_IDI.IDI FROM GS_PROVETREL_IDI WITH (NOLOCK) WHERE GS_PROVETREL_IDI.PROVETREL = " & rs("ID")
    
        rsIdi.Open sConsultaIdi, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    
        If rsIdi.eof Then
            rs.Close
            rsIdi.Close
            Set rs = Nothing
            Set rsIdi = Nothing
            Exit Sub
        Else
            While Not rsIdi.eof
                oDen.Add rsIdi.Fields("IDI").Value, rsIdi.Fields("DEN").Value
                rsIdi.MoveNext
            Wend
        End If
        Me.Add fldId.Value, fldCod.Value, oDen, fldPedido.Value, fldFecAct.Value
        rsIdi.Close

        rs.MoveNext
    Wend
    Set rsIdi = Nothing

    
    rs.Close
    Set rs = Nothing
    Set fldId = Nothing
    Set fldCod = Nothing
    Set oDen = Nothing
    Set fldPedido = Nothing
    Exit Sub
      
End If

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CRelacTipos", "CargarTodosLosTiposRelacionDesde", ERR, Erl)
      Exit Sub
   End If
    
End Sub

