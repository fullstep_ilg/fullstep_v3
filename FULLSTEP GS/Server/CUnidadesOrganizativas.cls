VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CUnidadesOrganizativas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Collection" ,"IUon"
Attribute VB_Ext_KEY = "Member0" ,"IUon"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'********************* CUons **********************************
'*             Autor : Jim
'*             Creada : 29/05/2014
'*
'* Clase que representa una colecci�n de unidades organizativas
'* Sus elementos deben ser unidades organizativas de cualquier nivel
'* (usando variant para las uons en lugar de una interfaz, para modificar
'* menos c�digo), lo que permite tratar
'* los distintos tipos de Uon de forma general
'* **************************************************************

Option Explicit

'local variable to hold collection
Private mCol As Collection
Private m_oConexion As CConexion
Private m_oConexionServer As CConexionDeUseServer

Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property
Friend Property Set Conexion(ByVal con As CConexion)
    Set m_oConexion = con
End Property
Friend Property Set ConexionServer(ByVal con As CConexionDeUseServer)
    Set m_oConexionServer = con
End Property

Friend Property Get ConexionServer() As CConexionDeUseServer
    Set ConexionServer = m_oConexionServer
End Property

'''<summary>Devuelve el nombre completo de la uon o del grupo<summary>
''' <revision>JIM 19/06/2014</revision>
Public Function titulo() As String
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If mCol.Count = 0 Then
        titulo = ""
    ElseIf mCol.Count = 1 Then
        titulo = mCol.Item(1).titulo
    Else
        titulo = mCol.Item(1).titulo & " ... "
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CUnidadesOrganizativas", "titulo", ERR, Erl)
        Exit Function
    End If
End Function

''' <summary>Inserta una unidad organizativa en la colecci�n</summary>
''' <param name="oUon">Unidad organizativa, de cualquier nivel</param>
''' <param name="sKey">clave para acceder al elemento, opcional. Si no se incluye se toma la key de la Uon</param>
''' <revision>JIM 19/06/2014</revision>
Public Function Add(oUON As IUon, Optional sKey As String) As Variant
    'create a new object
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sKey = "" Then
        mCol.Add oUON, oUON.Key
    Else
        mCol.Add oUON, sKey
    End If
    Set oUON.Conexion = Conexion
    Set Add = oUON
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CUnidadesOrganizativas", "Add", ERR, Erl)
        Exit Function
    End If
End Function

Public Property Get Item(vntIndexKey As Variant) As Variant
Attribute Item.VB_UserMemId = 0
    'used when referencing an element in the collection
    'vntIndexKey contains either the Index or Key to the collection,
    'this is why it is declared as a Variant
    'Syntax: Set foo = x.Item(xyz) or Set foo = x.Item(5)
  Set Item = mCol(vntIndexKey)
End Property



Public Property Get Count() As Long
    'used when retrieving the number of elements in the
    'collection. Syntax: Debug.Print x.Count
    Count = mCol.Count
End Property


Public Sub Remove(vntIndexKey As Variant)
    'used when removing an element from the collection
    'vntIndexKey contains either the Index or Key, which is why
    'it is declared as a Variant
    'Syntax: x.Remove(xyz)


    mCol.Remove vntIndexKey
End Sub


Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
    'this property allows you to enumerate
    'this collection with the For...Each syntax
    Set NewEnum = mCol.[_NewEnum]
End Property


Private Sub Class_Initialize()
    'creates the collection when this class is created
    Set mCol = New Collection
End Sub


Private Sub Class_Terminate()
    'destroys collection when this class is terminated
    Set mCol = Nothing
End Sub

Public Sub Clear()
    Set mCol = Nothing
    Set mCol = New Collection
End Sub
''' <summary>Comprueba si la colecci�n contiene a la uon pasada como par�metro o alguna descendiente</summary>
''' <param name="oUon">Unidad organizativa, de cualquier nivel</param>
''' <returns>Verdadero si la uon pasada como par�metro est� contenida</returns>
''' <revision>JIM 19/06/2014</revision>
Public Function Contiene(ByRef oUON As IUon) As Boolean
    Dim o As IUon
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Contiene = False
    For Each o In mCol
        If o.incluye(oUON) Then
            Contiene = True
            Exit For
        End If
    Next
    Set o = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CUnidadesOrganizativas", "Contiene", ERR, Erl)
        Exit Function
    End If
End Function
Public Function EsDistribuibleUON(ByRef oUON As IUon) As Boolean
    Dim o As IUon
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    EsDistribuibleUON = False
    
    For Each o In mCol
        If oUON.incluye(o) Or o.incluye(oUON) Then
            EsDistribuibleUON = True
            Exit For
        End If
    Next
    Set o = Nothing
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CUnidadesOrganizativas", "EstaContenida", ERR, Erl)
        Exit Function
    End If
End Function

''' <summary>Comprueba si la colecci�n contiene a la uon pasada como par�metro (coincidencia exacta)</summary>
''' <param name="oUon">Unidad organizativa, de cualquier nivel</param>
''' <returns>Verdadero si la uon pasada como par�metro est� contenida</returns>
''' <revision>JIM 19/06/2014</revision>
Public Function ContieneIgual(ByRef oUON As IUon) As Boolean
    Dim o As Variant
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    ContieneIgual = False
    For Each o In mCol
        If o.equals(oUON) Then
            ContieneIgual = True
        End If
    Next
    Set o = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CUnidadesOrganizativas", "ContieneIgual", ERR, Erl)
        Exit Function
    End If
End Function

''' <summary>Comprueba si la colecci�n contiene un elemento con clave uon</summary>
''' <param name="oUon">Unidad organizativa, de cualquier nivel</param>
''' <param name="oLineaPedido">L�nea de pedido</param>
''' <returns>Verdadero si la uon pasada como par�metro est� contenida</returns>
''' <revision>JIM 19/06/2014</revision>
Public Function Existe(ByRef Key As String) As Boolean
    On Error GoTo ERR
    Dim u As Variant
    Set u = mCol.Item(Key)
    Existe = True
    Exit Function
ERR:
    Existe = False
End Function

Public Function toString() As String
    Dim oUON As IUon
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    toString = ""
    For Each oUON In mCol
        toString = toString & oUON.titulo & vbCrLf
    Next
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CUnidadesOrganizativas", "toString", ERR, Erl)
        Exit Function
    End If
End Function


'<summary>Carga todas las uons que tienen integraci�n activa para art�culos</summary>
Public Sub cargarUonsIntegradasArticulo()
    Dim adoComm As adodb.Command
    
    
    Dim rs As New adodb.Recordset
    
    Dim sConsulta As String
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sConsulta = "SELECT UON1.COD UON1,'' UON2,'' UON3, '' UON4,UON1.DEN,SENTIDO,UON1.EMPRESA "
    sConsulta = sConsulta & " FROM UON1 WITH(NOLOCK) "
    sConsulta = sConsulta & " INNER JOIN EMP WITH(NOLOCK) ON UON1.EMPRESA=EMP.ID "
    sConsulta = sConsulta & " INNER JOIN ERP_SOCIEDAD ES WITH(NOLOCK) ON EMP.SOCIEDAD = ES.SOCIEDAD "
    sConsulta = sConsulta & " INNER JOIN TABLAS_INTEGRACION_ERP TI WITH(NOLOCK) ON TI.ERP=ES.ERP "
    sConsulta = sConsulta & " WHERE TABLA=7 AND ACTIVA=1 AND BAJALOG=0 "
    sConsulta = sConsulta & " UNION "
    sConsulta = sConsulta & " SELECT UON2.UON1,UON2.COD UON2,'' UON3, '' UON4,UON2.DEN,SENTIDO, EMPRESA "
    sConsulta = sConsulta & " FROM UON2 WITH(NOLOCK) "
    sConsulta = sConsulta & " INNER JOIN EMP WITH(NOLOCK) ON UON2.EMPRESA=EMP.ID "
    sConsulta = sConsulta & " INNER JOIN ERP_SOCIEDAD ES WITH(NOLOCK) ON EMP.SOCIEDAD = ES.SOCIEDAD "
    sConsulta = sConsulta & " INNER JOIN TABLAS_INTEGRACION_ERP TI WITH(NOLOCK) ON TI.ERP=ES.ERP "
    sConsulta = sConsulta & " WHERE TABLA=7 AND ACTIVA=1 AND BAJALOG=0 "
    sConsulta = sConsulta & " UNION "
    sConsulta = sConsulta & " SELECT UON3.UON1 ,UON3.UON2,UON3.COD UON3, '' UON4,UON3.DEN,SENTIDO, EMPRESA "
    sConsulta = sConsulta & " FROM UON3 WITH(NOLOCK) "
    sConsulta = sConsulta & " INNER JOIN EMP WITH(NOLOCK) ON UON3.EMPRESA=EMP.ID "
    sConsulta = sConsulta & " INNER JOIN ERP_SOCIEDAD ES WITH(NOLOCK) ON EMP.SOCIEDAD = ES.SOCIEDAD "
    sConsulta = sConsulta & " INNER JOIN TABLAS_INTEGRACION_ERP TI WITH(NOLOCK) ON TI.ERP=ES.ERP "
    sConsulta = sConsulta & " WHERE TABLA=7 AND ACTIVA=1 AND BAJALOG=0 "
    sConsulta = sConsulta & " UNION"
    sConsulta = sConsulta & " SELECT UON4.UON1 ,UON4.UON2,UON4.UON3 ,UON4.COD UON4, UON4.DEN, SENTIDO, EMPRESA"
    sConsulta = sConsulta & " FROM UON4 WITH (NOLOCK)"
    sConsulta = sConsulta & " INNER JOIN EMP WITH(NOLOCK) ON UON4.EMPRESA=EMP.ID"
    sConsulta = sConsulta & " INNER JOIN ERP_SOCIEDAD ES WITH(NOLOCK) ON EMP.SOCIEDAD = ES.SOCIEDAD"
    sConsulta = sConsulta & " INNER JOIN TABLAS_INTEGRACION_ERP TI WITH(NOLOCK) ON TI.ERP=ES.ERP"
    sConsulta = sConsulta & " Where TABLA = 7 And Activa = 1 AND BAJALOG=0 "
    
    Set adoComm = New adodb.Command
    Set adoComm.ActiveConnection = m_oConexion.ADOCon
    adoComm.CommandText = sConsulta
    adoComm.CommandType = adCmdText
    Set rs = adoComm.Execute
    
    
    While Not rs.eof
        Dim oUON As IUon
        
        Set oUON = Me.createUon(rs("UON1").Value, rs("UON2").Value, rs("UON3").Value, rs("UON4").Value, rs("DEN"), rs("EMPRESA").Value)
        oUON.SentidoIntegracion = rs("SENTIDO").Value
        'Set oUon.conexion = Me.conexion
        Me.Add oUON, oUON.Key
        
        rs.MoveNext
    Wend
    rs.Close
    Set rs = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CUnidadesOrganizativas", "cargarUonsIntegradasArticulo", ERR, Erl)
        Exit Sub
    End If
End Sub

''' <summary>Carga todas las uons de una empresa</summary>
''' <param name="idEmpresa">Empresa</param>
''' <param name="sOrgCompras">Org. compras</param>
''' <param name="sUON1">UON1</param>
''' <param name="sUON2">UON2</param>
''' <param name="sUON3">UON3</param>
''' <param name="lIdPerfil">Id Perfil</param>
''' <remarks>Llamada desde: frmPROCEBuscarArticulo </remarks>

Public Sub cargarUonsEmpresa(ByVal idEmpresa As Integer, Optional ByVal sOrgCompras As String, Optional ByVal sUON1 As String, Optional ByVal sUON2 As String, _
        Optional ByVal sUON3 As String, Optional ByVal lIdPerfil As Long)
    Dim adoComm As adodb.Command
    Dim rs As New adodb.Recordset
    Dim oParam As adodb.Parameter
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set adoComm = New adodb.Command
    With adoComm
        Set .ActiveConnection = m_oConexion.ADOCon
        .CommandText = "SP_DEVOLVER_UONS_EMPRESA"
        .CommandType = adCmdStoredProc
        
        Set oParam = .CreateParameter("@IDEMPRESA", adInteger, adParamInput, , idEmpresa)
        .Parameters.Append oParam
        Set oParam = .CreateParameter("@ORGCOMPRAS", adVarChar, adParamInput, 50, IIf(sOrgCompras <> "", sOrgCompras, Null))
        .Parameters.Append oParam
        Set oParam = .CreateParameter("@UON1", adVarChar, adParamInput, 50, IIf(sUON1 <> "", sUON1, Null))
        .Parameters.Append oParam
        Set oParam = .CreateParameter("@UON2", adVarChar, adParamInput, 50, IIf(sUON2 <> "", sUON2, Null))
        .Parameters.Append oParam
        Set oParam = .CreateParameter("@UON3", adVarChar, adParamInput, 50, IIf(sUON3 <> "", sUON3, Null))
        .Parameters.Append oParam
        Set oParam = .CreateParameter("@IDPERFIL", adInteger, adParamInput, , lIdPerfil)
        .Parameters.Append oParam
        
        Set rs = .Execute
    End With
        
    Set mCol = New Collection
    While Not rs.eof
        Dim oUON As IUon
        Set oUON = Me.createUon(rs("UON1").Value, rs("UON2").Value, rs("UON3").Value, rs("UON4").Value, rs("DEN"))
        Me.Add oUON, oUON.Key
        
        rs.MoveNext
    Wend
    
    rs.Close
    Set rs = Nothing
    Set oParam = Nothing
    Set adoComm = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CUnidadesOrganizativas", "cargarUonsEmpresa", ERR, Erl)
        Exit Sub
    End If
End Sub

Public Sub cargarTodasLasUnidadesDistribuidasNivelProceso(ByVal sAnyo As Integer, ByVal sGMN1 As String, ByVal sProce As Long)
       
    Dim adoComm As adodb.Command
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set mCol = Nothing
    Set mCol = New Collection
    Dim rs As New adodb.Recordset
    
    Dim sConsulta As String
        
        
    sConsulta = sConsulta & " SELECT U.COD UON1,'' UON2,''UON3,DEN "
    sConsulta = sConsulta & " FROM UON1 U WITH(NOLOCK) "
    sConsulta = sConsulta & " INNER JOIN PROCE_UON1 PU WITH(NOLOCK) ON U.COD=PU.UON1 "
    sConsulta = sConsulta & " WHERE ANYO=" & sAnyo & " AND GMN1='" & sGMN1 & "' AND PROCE=" & sProce
    sConsulta = sConsulta & " Union"
    sConsulta = sConsulta & " SELECT U.UON1,U.COD UON2,'' UON3,DEN "
    sConsulta = sConsulta & " FROM UON2 U WITH(NOLOCK) "
    sConsulta = sConsulta & " INNER JOIN PROCE_UON2 PU WITH(NOLOCK) ON U.UON1=PU.UON1 AND U.COD=PU.UON2"
    sConsulta = sConsulta & " WHERE ANYO=" & sAnyo & " AND GMN1='" & sGMN1 & "' AND PROCE=" & sProce
    sConsulta = sConsulta & " Union"
    sConsulta = sConsulta & " SELECT U.UON1,U.UON2,U.COD UON3,DEN "
    sConsulta = sConsulta & " FROM UON3 U WITH(NOLOCK) "
    sConsulta = sConsulta & " INNER JOIN PROCE_UON3 PU WITH(NOLOCK) ON U.COD=PU.UON3 AND U.UON2=PU.UON2 AND U.UON1=PU.UON1"
    sConsulta = sConsulta & " WHERE ANYO=" & sAnyo & " AND GMN1='" & sGMN1 & "' AND PROCE=" & sProce
    
    Set adoComm = New adodb.Command
    Set adoComm.ActiveConnection = m_oConexion.ADOCon
    adoComm.CommandText = sConsulta
    adoComm.CommandType = adCmdText
    Set rs = adoComm.Execute
    
    
    While Not rs.eof
        Dim oUON As IUon
        
        Set oUON = Me.createUon(rs("UON1").Value, rs("UON2").Value, rs("UON3").Value, "", rs("DEN"))
        
        Me.Add oUON, oUON.Key
        
        rs.MoveNext
    Wend
    rs.Close
    Set rs = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CUnidadesOrganizativas", "cargarTodasLasUnidadesDistribuidasNivelProceso", ERR, Erl)
        Exit Sub
    End If
End Sub

Public Sub cargarTodasLasUnidadesDistribuidasNivelGrupo(ByVal sAnyo As Integer, ByVal sGMN1 As String, ByVal sProce As Long, ByVal iGrupo As Integer)
       
    Dim adoComm As adodb.Command
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set mCol = Nothing
    Set mCol = New Collection
    Dim rs As New adodb.Recordset
    
    Dim sConsulta As String
        
        
    sConsulta = sConsulta & " SELECT U.COD UON1,'' UON2,''UON3,DEN "
    sConsulta = sConsulta & " FROM UON1 U WITH(NOLOCK) "
    sConsulta = sConsulta & " INNER JOIN PROCE_GR_UON1 PU WITH(NOLOCK) ON U.COD=PU.UON1 "
    sConsulta = sConsulta & " WHERE ANYO=" & sAnyo & " AND GMN1='" & sGMN1 & "' AND PROCE=" & sProce & " AND GRUPO = " & iGrupo
    sConsulta = sConsulta & " AND U.BAJALOG=0 "
    sConsulta = sConsulta & " Union"
    sConsulta = sConsulta & " SELECT U.UON1,U.COD UON2,'' UON3,DEN "
    sConsulta = sConsulta & " FROM UON2 U WITH(NOLOCK) "
    sConsulta = sConsulta & " INNER JOIN PROCE_GR_UON2 PU WITH(NOLOCK) ON U.UON1=PU.UON1 AND U.COD=PU.UON2"
    sConsulta = sConsulta & " WHERE ANYO=" & sAnyo & " AND GMN1='" & sGMN1 & "' AND PROCE=" & sProce & " AND GRUPO = " & iGrupo
    sConsulta = sConsulta & " AND U.BAJALOG=0 "
    sConsulta = sConsulta & " Union"
    sConsulta = sConsulta & " SELECT U.UON1,U.UON2,U.COD UON3,DEN "
    sConsulta = sConsulta & " FROM UON3 U WITH(NOLOCK) "
    sConsulta = sConsulta & " INNER JOIN PROCE_GR_UON3 PU WITH(NOLOCK) ON U.COD=PU.UON3 AND U.UON2=PU.UON2 AND U.UON1=PU.UON1"
    sConsulta = sConsulta & " WHERE ANYO=" & sAnyo & " AND GMN1='" & sGMN1 & "' AND PROCE=" & sProce & " AND GRUPO = " & iGrupo
    sConsulta = sConsulta & " AND U.BAJALOG=0 "
    
    Set adoComm = New adodb.Command
    Set adoComm.ActiveConnection = m_oConexion.ADOCon
    adoComm.CommandText = sConsulta
    adoComm.CommandType = adCmdText
    Set rs = adoComm.Execute
    
    
    While Not rs.eof
        Dim oUON As IUon
        
        Set oUON = Me.createUon(rs("UON1").Value, rs("UON2").Value, rs("UON3").Value, "", rs("DEN"))
        
        Me.Add oUON, oUON.Key
        
        rs.MoveNext
    Wend
    rs.Close
    Set rs = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CUnidadesOrganizativas", "cargarTodasLasUnidadesDistribuidasNivelGrupo", ERR, Erl)
        Exit Sub
    End If
End Sub

Public Sub cargarTodasLasUnidadesDistribuidasNivelItem(ByVal sAnyo As Integer, ByVal sGMN1 As String, ByVal sProce As Long, ByVal iItem As Integer)
       
    Dim adoComm As adodb.Command
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set mCol = Nothing
    Set mCol = New Collection
    Dim rs As New adodb.Recordset
    
    Dim sConsulta As String
        
        
    sConsulta = "SELECT U.COD UON1,'' UON2,''UON3, DEN"
    sConsulta = sConsulta & " FROM UON1 U WITH(NOLOCK)"
    sConsulta = sConsulta & " INNER JOIN ITEM_UON1 PU WITH(NOLOCK) ON U.COD=PU.UON1"
    sConsulta = sConsulta & " WHERE ANYO=" & sAnyo & " AND GMN1='" & sGMN1 & "' AND PROCE=" & sProce & " AND ITEM= " & iItem
    sConsulta = sConsulta & " AND U.BAJALOG=0 "
    sConsulta = sConsulta & " Union"
    sConsulta = sConsulta & " SELECT U.UON1,U.COD UON2, '' UON3 ,DEN "
    sConsulta = sConsulta & " FROM UON2 U WITH(NOLOCK)"
    sConsulta = sConsulta & " INNER JOIN ITEM_UON2 PU WITH(NOLOCK) ON U.COD=PU.UON2 AND U.UON1=PU.UON1"
    sConsulta = sConsulta & " WHERE ANYO=" & sAnyo & " AND GMN1='" & sGMN1 & "' AND PROCE=" & sProce & " AND ITEM = " & iItem
    sConsulta = sConsulta & " AND U.BAJALOG=0 "
    sConsulta = sConsulta & " Union"
    sConsulta = sConsulta & " SELECT U.UON1,U.UON2,COD UON3 ,DEN "
    sConsulta = sConsulta & " FROM UON3 U WITH(NOLOCK)"
    sConsulta = sConsulta & " INNER JOIN ITEM_UON3 PU WITH(NOLOCK) ON U.COD=PU.UON3 AND U.UON2=PU.UON2 AND U.UON1=PU.UON1"
    sConsulta = sConsulta & " WHERE ANYO=" & sAnyo & " AND GMN1='" & sGMN1 & "' AND PROCE=" & sProce & " AND ITEM = " & iItem
    sConsulta = sConsulta & " AND U.BAJALOG=0 "
    
    Set adoComm = New adodb.Command
    Set adoComm.ActiveConnection = m_oConexion.ADOCon
    adoComm.CommandText = sConsulta
    adoComm.CommandType = adCmdText
    Set rs = adoComm.Execute
    
    
    While Not rs.eof
        Dim oUON As IUon
        
        Set oUON = Me.createUon(rs("UON1").Value, rs("UON2").Value, rs("UON3").Value, "", rs("DEN"))
        
        Me.Add oUON, oUON.Key
        
        rs.MoveNext
    Wend
    rs.Close
    Set rs = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CUnidadesOrganizativas", "cargarTodasLasUnidadesDistribuidasNivelItem", ERR, Erl)
        Exit Sub
    End If
End Sub

Public Sub cargarTodasLasUnidadesUsuPerf(oUsuario As CUsuario, bRestrUsu As Boolean, bRestrPerf As Boolean, sUON1 As Variant, sUON2 As Variant, sUON3 As Variant)
    Dim sConsulta1, sConsulta2, sConsulta As String
    Dim adoComm As adodb.Command
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set mCol = Nothing
    Set mCol = New Collection
    Dim rs As New adodb.Recordset
    If bRestrUsu Or (bRestrPerf And Not oUsuario.Perfil Is Nothing) Then
        If bRestrUsu Then
            sConsulta1 = " SELECT '" & DblQuote(sUON1) & "' UON1 "
            sConsulta1 = sConsulta1 & ",'" & DblQuote(sUON2) & "' UON2 "
            sConsulta1 = sConsulta1 & ",'" & DblQuote(sUON3) & "' UON3 "
        End If
        If bRestrPerf And Not oUsuario.Perfil Is Nothing Then
            sConsulta2 = " SELECT UON1.COD UON1,'' UON2,'' UON3 "
            sConsulta2 = sConsulta2 & " FROM UON1 WITH (NOLOCK)"
            sConsulta2 = sConsulta2 & " INNER JOIN PERF_UON PU WITH(NOLOCK) ON UON1.COD=PU.UON1 AND PU.UON2 IS NULL"
            sConsulta2 = sConsulta2 & " AND UON1.BAJALOG=0 AND PU.PERF=" & oUsuario.Perfil.Id
            sConsulta2 = sConsulta2 & " Union"
            sConsulta2 = sConsulta2 & " SELECT UON2.UON1 ,UON2.COD UON2 ,'' UON3"
            sConsulta2 = sConsulta2 & " FROM UON2 WITH (NOLOCK)"
            sConsulta2 = sConsulta2 & " INNER JOIN PERF_UON PU WITH(NOLOCK) ON UON2.UON1=PU.UON1 AND UON2.COD=PU.UON2 AND PU.UON3 IS NULL"
            sConsulta2 = sConsulta2 & " AND UON2.BAJALOG=0 AND  PU.PERF=" & oUsuario.Perfil.Id
            sConsulta2 = sConsulta2 & " Union"
            sConsulta2 = sConsulta2 & " SELECT UON3.UON1 ,UON3.UON2 ,UON3.COD UON3"
            sConsulta2 = sConsulta2 & " FROM UON3 WITH (NOLOCK)"
            sConsulta2 = sConsulta2 & " INNER JOIN PERF_UON PU WITH(NOLOCK) ON UON3.UON1=PU.UON1 AND UON3.UON2=PU.UON2 AND UON3.COD=PU.UON3"
            sConsulta2 = sConsulta2 & " AND UON3.BAJALOG=0 AND PU.PERF=" & oUsuario.Perfil.Id
        End If
        
        If bRestrUsu And (bRestrPerf And Not oUsuario.Perfil Is Nothing) Then
            sConsulta = sConsulta1 & " UNION " & sConsulta2
        ElseIf bRestrUsu And (Not bRestrPerf Or oUsuario.Perfil Is Nothing) Then
            sConsulta = sConsulta1
        Else
            sConsulta = sConsulta2
        End If
        Set adoComm = New adodb.Command
        Set adoComm.ActiveConnection = m_oConexion.ADOCon
        adoComm.CommandText = sConsulta
        adoComm.CommandType = adCmdText
        Set rs = adoComm.Execute
        
        
        While Not rs.eof
            Dim oUON As IUon
            
            Set oUON = Me.createUon(rs("UON1").Value, rs("UON2").Value, rs("UON3").Value, "")
            
            Me.Add oUON, oUON.Key
            
            rs.MoveNext
        Wend
        rs.Close
        Set rs = Nothing
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CUnidadesOrganizativas", "cargarTodasLasUnidadesUsuPerf", ERR, Erl)
        Exit Sub
    End If
End Sub

Public Function createUon(sUON1 As String, sUON2 As String, sUON3 As String, sUON4 As String, Optional Den As String, Optional idEmpresa As Variant) As IUon
    Dim oUON As IUon
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sUON1 = "" Then
        Set oUON = New CUnidadOrgNivel0
    ElseIf sUON2 = "" Then
        Set oUON = New CUnidadOrgNivel1
        oUON.CodUnidadOrgNivel1 = sUON1
    ElseIf sUON3 = "" Then
        Set oUON = New CUnidadOrgNivel2
        oUON.CodUnidadOrgNivel1 = sUON1
        oUON.CodUnidadOrgNivel2 = sUON2
    ElseIf sUON4 = "" Then
        Set oUON = New CUnidadOrgNivel3
        oUON.CodUnidadOrgNivel1 = sUON1
        oUON.CodUnidadOrgNivel2 = sUON2
        oUON.CodUnidadOrgNivel3 = sUON3
    Else
        Set oUON = New CUnidadOrgNivel4
        oUON.CodUnidadOrgNivel1 = sUON1
        oUON.CodUnidadOrgNivel2 = sUON2
        oUON.CodUnidadOrgNivel3 = sUON3
        oUON.CodUnidadOrgNivel4 = sUON4
        
    End If
    oUON.Den = Den
    oUON.idEmpresa = idEmpresa
    Set oUON.Conexion = Me.Conexion
    Set createUon = oUON
    Set oUON = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CUnidadesOrganizativas", "createUon", ERR, Erl)
        Exit Function
    End If
End Function

'---------------------------------------------------------------------------------------
' Procedure : clonar
'<revision> jim : 17/10/2014</revision>
'<summary>Crea una copia de la colecci�n'</summary>
'<returns>CUnidadesOrganizativas</returns>
'---------------------------------------------------------------------------------------
'
Public Function clonar() As CUnidadesOrganizativas
    Dim oUON As IUon
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set clonar = New CUnidadesOrganizativas
    Set clonar.Conexion = Me.Conexion
    
    For Each oUON In mCol
       clonar.Add oUON.clonar
    Next
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CUnidadesOrganizativas", "clonar", ERR, Erl)
        Exit Function
    End If
End Function
'<summary>Obtiene la Unidad Organizativa de un Centro u Organizaci�n de Compras </summary>
Public Function ObtenerUONDeCentroOrgCompras(ByVal Centro As Variant, ByVal OrgCompras As Variant) As IUon

Dim adoComm As adodb.Command
Dim rs As New adodb.Recordset
Dim sConsulta As String

'UON3
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
sConsulta = "SELECT U3.UON1, U3.UON2, U3.COD UON3, U3.DEN FROM UON3 U3 WITH(NOLOCK)"
sConsulta = sConsulta & " INNER JOIN UON2 U2 WITH(NOLOCK) ON U3.UON2 = U2.COD AND U3.UON1 = U2.UON1"
sConsulta = sConsulta & " INNER JOIN UON1 U1 WITH(NOLOCK) ON U2.UON1 = U1.COD"

If NoHayParametro(OrgCompras) Then
    sConsulta = sConsulta & " WHERE ISNULL(U3.ORGCOMPRAS, ISNULL(U2.ORGCOMPRAS, U1.ORGCOMPRAS)) IS NULL "
Else
    sConsulta = sConsulta & " WHERE ISNULL(U3.ORGCOMPRAS, ISNULL(U2.ORGCOMPRAS, U1.ORGCOMPRAS)) ='" & DblQuote(OrgCompras) & "'"
End If

If NoHayParametro(Centro) Then
    sConsulta = sConsulta & " AND U3.CENTROS IS NULL"
Else
    sConsulta = sConsulta & " AND U3.CENTROS ='" & DblQuote(Centro) & "'"
End If

sConsulta = sConsulta & " AND U3.BAJALOG = 0"


'UON2
sConsulta = sConsulta & " UNION SELECT U2.UON1, U2.COD UON2, NULL UON3, U2.DEN FROM UON2 U2 WITH(NOLOCK)"
sConsulta = sConsulta & " INNER JOIN UON1 U1 WITH(NOLOCK) ON U2.UON1 = U1.COD"

If NoHayParametro(OrgCompras) Then
    sConsulta = sConsulta & " WHERE ISNULL(U2.ORGCOMPRAS, U1.ORGCOMPRAS) IS NULL"
Else
    sConsulta = sConsulta & " WHERE ISNULL(U2.ORGCOMPRAS, U1.ORGCOMPRAS) = '" & DblQuote(OrgCompras) & "'"
End If

If NoHayParametro(Centro) Then
    sConsulta = sConsulta & " AND U2.CENTROS IS NULL"
Else
    sConsulta = sConsulta & " AND U2.CENTROS = '" & DblQuote(Centro) & "'"
End If

sConsulta = sConsulta & " AND U2.BAJALOG = 0"


'UON1
sConsulta = sConsulta & " UNION SELECT U1.COD UON1, NULL UON2, NULL UON3, U1.DEN FROM UON1 U1 WITH(NOLOCK)"

If NoHayParametro(OrgCompras) Then
    sConsulta = sConsulta & " WHERE U1.ORGCOMPRAS IS NULL"
Else
    sConsulta = sConsulta & " WHERE U1.ORGCOMPRAS = '" & DblQuote(OrgCompras) & "'"
End If

If NoHayParametro(Centro) Then
    sConsulta = sConsulta & " AND U1.CENTROS IS NULL"
Else
    sConsulta = sConsulta & " AND U1.CENTROS = '" & DblQuote(Centro) & "'"
End If

sConsulta = sConsulta & " AND U1.BAJALOG = 0"


Set adoComm = New adodb.Command
Set adoComm.ActiveConnection = m_oConexion.ADOCon
adoComm.CommandText = sConsulta
adoComm.CommandType = adCmdText
Set rs = adoComm.Execute



If Not rs.eof Then
    Dim oUON As IUon
        
    Set oUON = Me.createUon(NullToStr(rs("UON1").Value), NullToStr(rs("UON2").Value), NullToStr(rs("UON3").Value), "", rs("DEN"))
        
    rs.MoveNext
Else

    Set oUON = Nothing
End If
rs.Close
Set rs = Nothing

Set ObtenerUONDeCentroOrgCompras = oUON
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CUnidadesOrganizativas", "ObtenerUONDeCentroOrgCompras", ERR, Erl)
        Exit Function
    End If

End Function
