VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CConfVistaVisorSolPP"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements IBaseDatos

Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum

Private m_sUsu As String
Private m_sPres5_Nivel0 As String
Private m_sPres5_Nivel1 As String
Private m_sPres5_Nivel2 As String
Private m_sPres5_Nivel3 As String
Private m_sPres5_Nivel4 As String

Private m_oConexion As CConexion
Private m_oConexionServer As CConexionDeUseServer
Private m_vIndice As Variant
Private m_adores As adodb.Recordset

Public Property Let Usuario(ByVal sUsuario As String)
    m_sUsu = sUsuario
End Property
Public Property Get Usuario() As String
    Usuario = m_sUsu
End Property

Public Property Let Pres5_Nivel0(ByVal dato As String)
    m_sPres5_Nivel0 = dato
End Property
Public Property Get Pres5_Nivel0() As String
    Pres5_Nivel0 = m_sPres5_Nivel0
End Property

Public Property Let Pres5_Nivel1(ByVal dato As String)
    m_sPres5_Nivel1 = dato
End Property
Public Property Get Pres5_Nivel1() As String
    Pres5_Nivel1 = m_sPres5_Nivel1
End Property

Public Property Let Pres5_Nivel2(ByVal dato As String)
    m_sPres5_Nivel2 = dato
End Property
Public Property Get Pres5_Nivel2() As String
    Pres5_Nivel2 = m_sPres5_Nivel2
End Property

Public Property Let Pres5_Nivel3(ByVal dato As String)
    m_sPres5_Nivel3 = dato
End Property
Public Property Get Pres5_Nivel3() As String
    Pres5_Nivel3 = m_sPres5_Nivel3
End Property

Public Property Let Pres5_Nivel4(ByVal dato As String)
    m_sPres5_Nivel4 = dato
End Property
Public Property Get Pres5_Nivel4() As String
    Pres5_Nivel4 = m_sPres5_Nivel4
End Property

Friend Property Set ConexionServer(ByVal con As CConexionDeUseServer)
    Set m_oConexionServer = con
End Property

Friend Property Get ConexionServer() As CConexionDeUseServer
    Set ConexionServer = m_oConexionServer
End Property

Friend Property Set Conexion(ByVal con As CConexion)
    Set m_oConexion = con
End Property

Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property

Public Property Let Indice(ByVal vIndice As Variant)
    m_vIndice = vIndice
End Property
Public Property Get Indice() As Variant
    Indice = m_vIndice
End Property

''' <summary>
''' A�ade un registro a la tabla CONF_VISOR_SOL_PP por cada partida presupuestaria configurada en el visor de solicitudes
''' </summary>
''' <returns>Un TipoErrorSummit en el caso de que haya habido error</returns>
''' <remarks>Llamada desde:frmSolicitudes.GuardarConfiguracionVisorSol; Tiempo m�ximo:0,1</remarks>
Private Function IBaseDatos_AnyadirABaseDatos() As TipoErrorSummit
Dim sConsulta As String
Dim TESError As TipoErrorSummit

'********* Precondicion *******************
If m_oConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "cConfVistaVisorSol.AnyadirABAseDatos", "No se ha establecido la conexion"
    Exit Function
End If
'******************************************
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
    
    TESError.NumError = TESnoerror
    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
            
    sConsulta = "INSERT INTO CONF_VISOR_SOL_PP (USU,PRES5_NIV0,PRES5_NIV1,PRES5_NIV2,PRES5_NIV3,PRES5_NIV4)"
      
    sConsulta = sConsulta & " VALUES ('" & DblQuote(m_sUsu) & "'"
    sConsulta = sConsulta & "," & StrToSQLNULL(m_sPres5_Nivel0)
    sConsulta = sConsulta & "," & StrToSQLNULL(m_sPres5_Nivel1)
    sConsulta = sConsulta & "," & StrToSQLNULL(m_sPres5_Nivel2)
    sConsulta = sConsulta & "," & StrToSQLNULL(m_sPres5_Nivel3)
    sConsulta = sConsulta & "," & StrToSQLNULL(m_sPres5_Nivel4)
    sConsulta = sConsulta & ")"

    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    IBaseDatos_AnyadirABaseDatos = TESError
    Exit Function
    
Error_Cls:
    
    IBaseDatos_AnyadirABaseDatos = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
    On Error Resume Next
    If Not m_adores Is Nothing Then
        m_adores.Close
        Set m_adores = Nothing
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        'Call g_oErrores.TratarError("Class Module", "CConfVistaVisorSolPP", "IBaseDatos_AnyadirABaseDatos", ERR, Erl)
        GoTo Error_Cls
        Exit Function
    End If

End Function

Private Function IBaseDatos_CambiarCodigo(CodigoNuevo As Variant) As TipoErrorSummit

End Function

Private Sub IBaseDatos_CancelarEdicion()

End Sub

Private Function IBaseDatos_EliminarDeBaseDatos() As TipoErrorSummit
Dim sConsulta As String
Dim TESError As TipoErrorSummit

    '''Precondicion
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "cConfVistaVisorSol.EliminarDeBaseDatos", "No se ha establecido la conexion"
        Exit Function
    End If
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls

    TESError.NumError = TESnoerror
     
    sConsulta = "DELETE FROM CONF_VISOR_SOL_PP WHERE USU='" & DblQuote(m_sUsu) & "'"
        
    m_oConexion.ADOCon.Execute sConsulta

    IBaseDatos_EliminarDeBaseDatos = TESError

    Exit Function

Error_Cls:
    IBaseDatos_EliminarDeBaseDatos = basErrores.TratarError(m_oConexion.ADOCon.Errors)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        'Call g_oErrores.TratarError("Class Module", "CConfVistaVisorSolPP", "IBaseDatos_EliminarDeBaseDatos", ERR, Erl)
        GoTo Error_Cls
        Exit Function
    End If

End Function

Private Function IBaseDatos_FinalizarEdicionEliminando() As TipoErrorSummit

End Function

Private Function IBaseDatos_FinalizarEdicionModificando() As TipoErrorSummit

End Function

Private Function IBaseDatos_IniciarEdicion(Optional ByVal Bloquear As Boolean, Optional ByVal UsuarioBloqueo As String) As TipoErrorSummit

End Function

''' <summary>
''' Comprueba que existe ya una configuraci�n de partidas presupuestarias para ese usuario
''' </summary>
''' <returns>true/false: si existe o no la configuraci�n</returns>
''' <remarks>Llamada desde:frmSolicitudes.GuardarConfiguracionVisorSol; Tiempo m�ximo:0,1</remarks>
Private Function IBaseDatos_ComprobarExistenciaEnBaseDatos() As Boolean
    Dim sConsulta As String
    Dim AdoRes As New adodb.Recordset
    
    '******************* Precondicion *******************
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CConfVisor.ComprobarExistenciaEnBaseDatos", "No se ha establecido la conexion"
        Exit Function
    End If
    '****************************************************
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
    sConsulta = "SELECT COUNT(*) FROM CONF_VISOR_SOL_PP WITH (NOLOCK) WHERE USU='" & DblQuote(m_sUsu) & "'"
        
    Set AdoRes = New adodb.Recordset
    AdoRes.CursorLocation = adUseServer
    AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    If AdoRes.eof Then
        IBaseDatos_ComprobarExistenciaEnBaseDatos = False
    Else
        If AdoRes.Fields(0).Value > 0 Then
            IBaseDatos_ComprobarExistenciaEnBaseDatos = True
        Else
            IBaseDatos_ComprobarExistenciaEnBaseDatos = False
        End If
    End If

    AdoRes.Close
    Set AdoRes = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CConfVistaVisorSolPP", "IBaseDatos_ComprobarExistenciaEnBaseDatos", ERR, Erl)
        Exit Function
    End If
End Function

Private Sub Class_Terminate()
On Error GoTo Error_Cls:
    
    Set m_oConexionServer = Nothing
    Set m_oConexion = Nothing
    Exit Sub
Error_Cls:
    Resume Next
End Sub


