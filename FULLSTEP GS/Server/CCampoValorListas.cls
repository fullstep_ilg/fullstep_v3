VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CCampoValorListas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

Private mCol As Collection

Private m_oConexion As CConexion
Private m_bEOF As Boolean

Public Property Get eof() As Boolean
    eof = m_bEOF
End Property
Friend Property Let eof(ByVal b As Boolean)
    m_bEOF = b
End Property

Public Function Add(ByVal IdOrden As Long, Optional ByVal oCampo As CFormItem, Optional ByVal ValorNum As Variant, Optional ByVal ValorText As CMultiidiomas, Optional ByVal ValorFec As Variant, _
            Optional ByVal FechaActual As Variant, Optional ByVal vIndice As Variant, Optional ByVal oCampoPredef As CCampoPredef, Optional ByVal lidCampoPadre As Long, Optional ByVal iOrdenCampoPadre As Integer, _
            Optional ByVal lOrdenAnterior As Long, Optional ByVal sGMN1 As String, Optional ByVal sGMN2 As String, Optional ByVal sGMN3 As String, Optional ByVal sGMN4 As String, _
            Optional ByVal GMNDen As CMultiidiomas) As CCampoValorLista
        
    Dim objnewmember As CCampoValorLista
    
    Set objnewmember = New CCampoValorLista
    Set objnewmember.Conexion = m_oConexion
    
    objnewmember.Orden = IdOrden
    Set objnewmember.Campo = oCampo
    Set objnewmember.CampoPredef = oCampoPredef
    
    If Not IsMissing(ValorNum) Then
        objnewmember.ValorNum = ValorNum
    Else
        objnewmember.ValorNum = Null
    End If
    
    Set objnewmember.ValorText = ValorText
    
    If Not IsMissing(ValorFec) Then
        objnewmember.ValorFec = ValorFec
    Else
        objnewmember.ValorFec = Null
    End If
    
    If IsMissing(FechaActual) Then
        objnewmember.FecAct = Null
    Else
        objnewmember.FecAct = FechaActual
    End If
    
    objnewmember.IdCampoPadre = lidCampoPadre
    objnewmember.OrdenCampoPadre = iOrdenCampoPadre
    
    If Not IsMissing(lOrdenAnterior) Then
        objnewmember.OrdenAnterior = lOrdenAnterior
    Else
        objnewmember.OrdenAnterior = Null
    End If
    
    objnewmember.GMN1 = sGMN1
    objnewmember.GMN2 = sGMN2
    objnewmember.GMN3 = sGMN3
    objnewmember.GMN4 = sGMN4
    Set objnewmember.GMNDen = GMNDen
    
    If Not IsMissing(vIndice) And Not IsNull(vIndice) Then
        objnewmember.Indice = vIndice
        mCol.Add objnewmember, CStr(vIndice)
    Else
        If Not oCampo Is Nothing Then
            mCol.Add objnewmember, CStr(IdOrden) & "$" & CStr(oCampo.Id)
        Else
            mCol.Add objnewmember, CStr(IdOrden) & "$" & CStr(oCampoPredef.Id)
        End If
    End If

    Set Add = objnewmember
    
    Set objnewmember = Nothing

End Function

Friend Property Set Conexion(ByVal vData As CConexion)
    Set m_oConexion = vData
End Property

Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property

Public Property Get Item(vntIndexKey As Variant) As CCampoValorLista

    ''' * Objetivo: Recuperar una moneda de la coleccion
    ''' * Recibe: Indice de la moneda a recuperar
    ''' * Devuelve: Moneda correspondiente
        
On Error GoTo NoSeEncuentra:

    ''' Devolvemos el item de la coleccion privada
    
    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:

    Set Item = Nothing
    
End Property

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4

    ''' ! Pendiente de revision, objetivo desconocido
    
    Set NewEnum = mCol.[_NewEnum]

End Property
Public Sub Remove(vntIndexKey As Variant)

    ''' * Objetivo: Eliminar una moneda de la coleccion
    ''' * Recibe: Indice de la moneda a eliminar
    ''' * Devuelve: Nada
   
    mCol.Remove vntIndexKey
    
End Sub
Private Sub Class_Initialize()

    ''' * Objetivo: Crear la coleccion
    Set mCol = New Collection
    
End Sub
Private Sub Class_Terminate()

    ''' * Objetivo: Limpiar memoria
    
    Set mCol = Nothing
    Set m_oConexion = Nothing
End Sub

Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
     Count = mCol.Count
End If

End Property





