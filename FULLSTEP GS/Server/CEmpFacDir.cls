VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CEmpFacDir"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private m_oConexion As CConexion

Private m_lEmp As Long
Private m_lFacDir As Long

Public Property Get Empresa() As Long
    Empresa = m_lEmp
End Property
Public Property Let Empresa(ByVal Value As Long)
    m_lEmp = Value
End Property
Public Property Get FacDir() As Long
    FacDir = m_lFacDir
End Property
Public Property Let FacDir(ByVal Value As Long)
    m_lFacDir = Value
End Property

''' <summary>Acciones a ejecutar al liberar un objeto de esta clase</summary>
Private Sub Class_Terminate()
    On Error Resume Next
    
    Set m_oConexion = Nothing
End Sub

Friend Property Set Conexion(ByVal oCon As CConexion)
    Set m_oConexion = oCon
End Property

Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property

''' <summary>
''' A�ade en EMP_FACDIR un nuevo registro.
''' Esta funci�n necesita que se abra una transacci�n en la funci�n llamante
''' </summary>
''' <remarks>Llamada desde CUnidadORGNivel1.AnyadirABaseDatos</remarks>

Public Sub AnyadirABaseDatos()
    Dim sConsulta As String
    Dim adoComm As ADODB.Command
    Dim adoParam As ADODB.Parameter
    
    sConsulta = "INSERT EMP_FACDIR (EMP, FACDIR) VALUES (?,?)"
    
    Set adoComm = New ADODB.Command
    Set adoComm.ActiveConnection = m_oConexion.ADOCon
    With adoComm
        .CommandText = sConsulta
        .CommandType = adCmdText
        .Prepared = True
        
        Set adoParam = .CreateParameter(Type:=adInteger, Direction:=adParamInput, Value:=m_lEmp)
        .Parameters.Append adoParam
        Set adoParam = .CreateParameter(Type:=adInteger, Direction:=adParamInput, Value:=m_lFacDir)
        .Parameters.Append adoParam
        
        .Execute
    End With
    Set adoParam = Nothing
    Set adoComm = Nothing
End Sub

''' <summary>
''' Elimina un registro en EMP_FACDIR.
''' Esta funci�n necesita que se abra una transacci�n en la funci�n llamante
''' </summary>
''' <remarks>Llamada desde CUnidadORGNivel1.AnyadirABaseDatos</remarks>

Public Sub EliminarDeBaseDatos()
    Dim sConsulta As String
    Dim adoComm As ADODB.Command
    Dim adoParam As Parameter
    
    sConsulta = "DELETE FROM EMP_FACDIR WHERE EMP=? AND FACDIR=?"
    
    Set adoComm = New ADODB.Command
    With adoComm
        Set .ActiveConnection = m_oConexion.ADOCon
        .CommandText = sConsulta
        .CommandType = adCmdText
        .Prepared = True
        
        Set adoParam = .CreateParameter(Type:=adInteger, Direction:=adParamInput, Value:=m_lEmp)
        .Parameters.Append adoParam
        Set adoParam = .CreateParameter(Type:=adInteger, Direction:=adParamInput, Value:=m_lFacDir)
        .Parameters.Append adoParam
        
        .Execute
    End With
    
    Set adoParam = Nothing
    Set adoComm = Nothing
End Sub

''' <summary>Elimina un registro en EMP_FACDIR y tambi�n elimina la direcci�n de env�o.</summary>
''' <returns>Error de haberlo</returns>
''' <remarks>Llamada desde CUnidadORGNivel1.AnyadirABaseDatos</remarks>

Public Function EliminarDeBaseDatosTx(Optional ByVal bBorrarDir As Boolean = False) As TipoErrorSummit
    Dim oFacDir As CFacDirEnvio
    Dim sConsulta As String
    Dim adoComm As ADODB.Command
    Dim adoParam As Parameter
    Dim TESError As TipoErrorSummit
    
    On Error GoTo Error
    
    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
    m_oConexion.ADOCon.Execute "SET XACT_ABORT OFF"
    
    sConsulta = "DELETE FROM EMP_FACDIR WHERE EMP=? AND FACDIR=?"
    
    Set adoComm = New ADODB.Command
    With adoComm
        Set .ActiveConnection = m_oConexion.ADOCon
        .CommandText = sConsulta
        .CommandType = adCmdText
        .Prepared = True
        
        Set adoParam = .CreateParameter(Type:=adInteger, Direction:=adParamInput, Value:=m_lEmp)
        .Parameters.Append adoParam
        Set adoParam = .CreateParameter(Type:=adInteger, Direction:=adParamInput, Value:=m_lFacDir)
        .Parameters.Append adoParam
        
        .Execute
        If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error
    End With
    
    If bBorrarDir Then
        Set oFacDir = New CFacDirEnvio
        Set oFacDir.Conexion = m_oConexion
        oFacDir.Id = m_lFacDir
        oFacDir.EliminarDeBaseDatos
        Set oFacDir = Nothing
    End If
    
    m_oConexion.ADOCon.Execute "COMMIT"
    
Salir:
    EliminarDeBaseDatosTx = TESError
    
    Set adoParam = Nothing
    Set adoComm = Nothing
    Set oFacDir = Nothing
    Exit Function
Error:
    TESError = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    m_oConexion.RollbackTransaction
    Resume Salir
End Function

Public Function TieneEmpresasAsocidas() As Boolean
    Dim oComm As ADODB.Command
    Dim oParam As ADODB.Parameter
    Dim rsNumEmp As Recordset
    Dim sConsulta As String
    
    TieneEmpresasAsocidas = False
    
    sConsulta = "SELECT count(EMP) AS NUMEMP FROM EMP_FACDIR WITH (NOLOCK) WHERE FACDIR=?"
    
    Set oComm = New ADODB.Command
    With oComm
        Set .ActiveConnection = m_oConexion.ADOCon
        .CommandText = sConsulta
        .CommandType = adCmdText
        .Prepared = True
        
        Set oParam = .CreateParameter(Type:=adInteger, Direction:=adParamInput, Value:=m_lFacDir)
        .Parameters.Append oParam
        
        Set rsNumEmp = .Execute
        If Not rsNumEmp.eof Then
            If rsNumEmp("NUMEMP").Value > 1 Then
                TieneEmpresasAsocidas = True
            End If
        End If
    End With
    
    Set oParam = Nothing
    Set oComm = Nothing
End Function
