VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CProvincias"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CProvincias **********************************
'*             Autor : Javier Arana
'*             Creada : 3/9/98
'****************************************************************

Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum
Private mCol As Collection
Private m_bEOF As Boolean
Private m_oConexion As CConexion
Private m_bPortal As Boolean
' Variables para el control de cambios del Log e Integraci�n
Private m_udtOrigen As OrigenIntegracion
'

''' <summary>Elimina la/las provincia/s seleccionadas de la base de datos.</summary>
''' <param name="Codigos">Es un array con los c�digos de las Unidades a eliminar</param>
''' <param name="Usuario">Usuario conectado que elimina</param>
''' <returns>Un TipoErrorSummit que es una estructura de datos definida en CTiposDeDatos, y, en
'''                     caso de ocurrir alg�n error al borrar, el
'''                     campo Arg1 de TipoErrorSummit contendr�
'''                     un array bidimensional con los datos
'''                     (c�digo,codigo de datos relacionado,
'''                     bookmark de la linea de denominaci�n del
'''                     destino actual) de las uniadades que no
'''                     pudieron ser eliminadas. </returns>
''' <remarks>Llamada desde: frmPROVI\cmdEliminar_Click
''' Tiempo m�ximo: 1 sec </remarks>
''' <revision>JVS 05/01/2012</revision>
Public Function EliminarProvinciasDeBaseDatos(ByVal Pais As String, ByVal Codigos As Variant, ByVal Usuario As String) As TipoErrorSummit

'*********************************************************************
'***  9/05/2002    Se incluye tratamiento para integraci�n y log   ***
'*********************************************************************
'******************************************
'***** M U Y   I M P O R T A N T E     ****
'*****                                 ****
'***** SI SE REALIZAN CAMBIOS          ****
'***** DESPUES DEL 21/06/2002          ****
'***** MIRAR SI HAY QUE PASARLOS A     ****
'***** FSGSINTEGRATION.DLL             ****
'******************************************

Dim vNoEliminados() As Variant
Dim udtTESError As TipoErrorSummit
Dim i As Integer
Dim iRes As Integer
Dim AdoRes As adodb.Recordset
Dim sConsulta As String
Dim sDen As String
Dim bGrabarEnLog As Boolean
Dim ContRegBorrados As Long
Dim lPais As Long
Dim sFSP As String
Dim adoComm As adodb.Command
Dim adoParam As adodb.Parameter
Dim bGuardar As Boolean
Dim oDen As CMultiidiomas
Dim oMulti As CMultiidioma
Dim lIdLogPROVI As Long

udtTESError.NumError = TESnoerror
iRes = 0

On Error GoTo Error
    m_oConexion.ADOCon.Execute "BEGIN DISTRIBUTED TRAN"
    m_oConexion.ADOCon.Execute "SET XACT_ABORT ON"

    bGrabarEnLog = GrabarEnLog 'Llamamos al m�todo una sola vez
    
    For i = 1 To UBound(Codigos)
        bGuardar = True
        
        'Comprobamos que el pais no se est� usando en el portal,en el caso de tener una sola compradora
        If basParametros.gParametrosGenerales.giINSTWEB = ConPortal Then
            If basParametros.gParametrosGenerales.gbUnaCompradora = True Then
                sFSP = basParametros.gParametrosGenerales.gsFSP_SRV & "." & basParametros.gParametrosGenerales.gsFSP_BD & ".dbo."
                'CALIDAD Sin WITH(NOLOCK) porque se leen tablas de PORTAL
                sConsulta = "SELECT ID FROM " & sFSP & "PAI WITH(NOLOCK) WHERE COD='" & DblQuote(Pais) & "'"
                Set AdoRes = New adodb.Recordset
                AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
                If Not AdoRes.eof Then
                    lPais = AdoRes.Fields("ID").Value
                    Set AdoRes = Nothing
                    
                    'CALIDAD Sin WITH(NOLOCK) porque se leen tablas de PORTAL
                    sConsulta = " SELECT * FROM " & sFSP & "CIAS WITH(NOLOCK) WHERE PAI=" & lPais
                    sConsulta = sConsulta & " AND PROVI=(SELECT ID FROM " & sFSP & "PROVI WITH(NOLOCK) WHERE PAI=" & lPais & " AND COD='" & DblQuote(Codigos(i)) & "')"
                    Set AdoRes = New adodb.Recordset
                    AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
                    If Not AdoRes.eof Then
                        ReDim Preserve vNoEliminados(3, iRes)
                        vNoEliminados(0, iRes) = Codigos(i)
                        vNoEliminados(1, iRes) = "Portal"
                        vNoEliminados(2, iRes) = i
                        vNoEliminados(3, iRes) = "Portal"
                        iRes = iRes + 1
                        bGuardar = False
                    End If
                    AdoRes.Close
                    Set AdoRes = Nothing
                Else
                    AdoRes.Close
                    Set AdoRes = Nothing
                End If
            End If
        End If
        
        'Va comprobando que el pais no se est� usando en las provincias,destinos,etc...Se hace as�
        'en lugar de provocar el error porque sino no funciona la transacci�n distribuida:
            
        ' ESTA RELACIONADA CON LA TABLA DESTINOS
        If bGuardar = True Then
            Set AdoRes = New adodb.Recordset
            sConsulta = "SELECT DEST.COD, DEST.DEN_" & gParametrosInstalacion.gIdioma & " DEN FROM DEST WITH(NOLOCK) WHERE DEST.PROVI='" & DblQuote(Codigos(i)) & "' AND DEST.PAI ='" & DblQuote(Pais) & "'"
            AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
            If Not AdoRes.eof Then
                ReDim Preserve vNoEliminados(3, iRes)
                vNoEliminados(0, iRes) = Codigos(i)
                vNoEliminados(1, iRes) = 36
                vNoEliminados(2, iRes) = i
                vNoEliminados(3, iRes) = AdoRes(0).Value & "-" & AdoRes(1).Value
                iRes = iRes + 1
                bGuardar = False
            End If
            Set AdoRes = Nothing
        End If
        
        ' ESTA RELACIONADA CON LA TABLA PROVE
        If bGuardar = True Then
            Set AdoRes = New adodb.Recordset
            sConsulta = "SELECT PROVE.COD, PROVE.DEN FROM PROVE WITH(NOLOCK) WHERE PROVE.PROVI='" & DblQuote(Codigos(i)) & "' AND PROVE.PAI ='" & DblQuote(Pais) & "'"
            AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
            If Not AdoRes.eof Then
                ReDim Preserve vNoEliminados(3, iRes)
                vNoEliminados(0, iRes) = Codigos(i)
                vNoEliminados(1, iRes) = 90
                vNoEliminados(2, iRes) = i
                vNoEliminados(3, iRes) = AdoRes(0).Value & "-" & AdoRes(1).Value
                iRes = iRes + 1
                bGuardar = False
            End If
            Set AdoRes = Nothing
        End If
        
        ' ESTA RELACIONADA CON LA TABLA PARGEN DEF
        If bGuardar = True Then
            Set AdoRes = New adodb.Recordset
            sConsulta = "SELECT PARGEN_DEF.ID FROM PARGEN_DEF WITH(NOLOCK) WHERE PARGEN_DEF.PROVIDEF='" & DblQuote(Codigos(i)) & "' AND PARGEN_DEF.PAIDEF ='" & DblQuote(Pais) & "'"
            AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
            If Not AdoRes.eof Then
                ReDim Preserve vNoEliminados(3, iRes)
                vNoEliminados(0, iRes) = Codigos(i)
                vNoEliminados(1, iRes) = 66
                vNoEliminados(2, iRes) = i
                vNoEliminados(3, iRes) = AdoRes(0).Value
                iRes = iRes + 1
                bGuardar = False
            End If
            Set AdoRes = Nothing
        End If
        
        'Almacena en la Base de datos:
        If bGuardar = True Then
            If bGrabarEnLog Then ' primero recuperamos los datos del pago que se va a borra
                sConsulta = "SELECT IDIOMA, DEN FROM PROVI_DEN WITH(NOLOCK) WHERE COD =N'" & DblQuote(Codigos(i)) & "' AND PAI =N'" & DblQuote(Pais) & "'"
                Set AdoRes = New adodb.Recordset
                AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
                
                Set oDen = Nothing
                Set oDen = New CMultiidiomas
                Set oDen.Conexion = m_oConexion
                While Not AdoRes.eof
                    oDen.Add AdoRes.Fields("IDIOMA").Value, AdoRes.Fields("DEN").Value
                    AdoRes.MoveNext
                Wend
                AdoRes.Close
                Set AdoRes = Nothing
            End If
            
            
            sConsulta = "UPDATE COPIA_CAMPO SET VALOR_TEXT=NULL WHERE VALOR_TEXT='" & DblQuote(Codigos(i)) & "' AND COPIA_CAMPO_DEF IN (SELECT ID FROM FORM_CAMPO WITH(NOLOCK) WHERE TIPO_CAMPO_GS=" & TipoCampoGS.Provincia & ")"
            m_oConexion.ADOCon.Execute sConsulta
            If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error
            sConsulta = "UPDATE COPIA_LINEA_DESGLOSE SET VALOR_TEXT=NULL WHERE VALOR_TEXT='" & DblQuote(Codigos(i)) & "' AND CAMPO_HIJO IN (SELECT COPIA_CAMPO.ID FROM COPIA_CAMPO WITH(NOLOCK) INNER JOIN FORM_CAMPO WITH(NOLOCK) ON COPIA_CAMPO.COPIA_CAMPO_DEF=FORM_CAMPO.ID WHERE ES_SUBCAMPO=1 AND TIPO_CAMPO_GS=" & TipoCampoGS.Provincia & ")"
            m_oConexion.ADOCon.Execute sConsulta
            If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error
            'elimina de los formularios:
            sConsulta = "UPDATE FORM_CAMPO SET VALOR_TEXT=NULL WHERE VALOR_TEXT='" & DblQuote(Codigos(i)) & "' AND TIPO_CAMPO_GS=" & TipoCampoGS.Provincia
            m_oConexion.ADOCon.Execute sConsulta
            If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error
            sConsulta = "UPDATE LINEA_DESGLOSE SET VALOR_TEXT=NULL WHERE VALOR_TEXT='" & DblQuote(Codigos(i)) & "' AND CAMPO_HIJO IN (SELECT ID FROM FORM_CAMPO WITH(NOLOCK) WHERE ES_SUBCAMPO=1 AND TIPO_CAMPO_GS=" & TipoCampoGS.Provincia & ")"
            m_oConexion.ADOCon.Execute sConsulta
            If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error
            'Elimina de las solicitudes favoritas los campos con la Provincia
            sConsulta = "UPDATE PM_CAMPO_FAV  SET VALOR_TEXT = NULL FROM PM_CAMPO_FAV CF WITH(NOLOCK)"
            sConsulta = sConsulta & " INNER JOIN FORM_CAMPO FC WITH (NOLOCK) ON FC.ID = CF.CAMPO"
            sConsulta = sConsulta & " WHERE CF.VALOR_TEXT='" & DblQuote(Codigos(i)) & "' AND FC.TIPO_CAMPO_GS=" & TipoCampoGS.Provincia
            m_oConexion.ADOCon.Execute sConsulta
            If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error
            'Elimina de las solicitudes favoritas las lineas de desglose con la Provincia
            sConsulta = "UPDATE PM_LINEA_DESGLOSE_FAV SET VALOR_TEXT = NULL FROM PM_LINEA_DESGLOSE_FAV LDF WITH(NOLOCK)"
            sConsulta = sConsulta & " INNER JOIN FORM_CAMPO FC WITH (NOLOCK) ON FC.ID = LDF.CAMPO_HIJO"
            sConsulta = sConsulta & " WHERE LDF.VALOR_TEXT='" & DblQuote(Codigos(i)) & "' AND FC.TIPO_CAMPO_GS=" & TipoCampoGS.Provincia
            m_oConexion.ADOCon.Execute sConsulta
            If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error
            ''MultiERP: Elimina de PROVI_ERP
            m_oConexion.ADOCon.Execute "DELETE FROM PROVI_ERP WHERE COD_GS=N'" & DblQuote(Codigos(i)) & "' AND PAI_GS =N'" & DblQuote(Pais) & "'"
            If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error
            m_oConexion.ADOCon.Execute "DELETE FROM PROVI_DEN WHERE PROVI=N'" & DblQuote(Codigos(i)) & "' AND PAI =N'" & DblQuote(Pais) & "'"
            If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error
            m_oConexion.ADOCon.Execute "DELETE FROM PROVI WHERE COD=N'" & DblQuote(Codigos(i)) & "' AND PAI =N'" & DblQuote(Pais) & "'", ContRegBorrados
            If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error
            
            'Elimina la provincia correspondiente en el portal:
            If basParametros.gParametrosGenerales.giINSTWEB = ConPortal Then
                If basParametros.gParametrosGenerales.gbUnaCompradora = True Then
                    Set adoComm = New adodb.Command
            
                    Set adoParam = adoComm.CreateParameter("PAI_COD", adVarChar, adParamInput, 20, Pais)
                    adoComm.Parameters.Append adoParam
                    Set adoParam = adoComm.CreateParameter("PROVI_COD", adVarChar, adParamInput, 20, Codigos(i))
                    adoComm.Parameters.Append adoParam
                    
                    Set adoComm.ActiveConnection = m_oConexion.ADOCon
                    adoComm.CommandType = adCmdStoredProc
                    adoComm.CommandText = sFSP & "SP_ELIM_PROVI"
                    adoComm.Prepared = True
                    adoComm.Execute
                    If m_oConexion.ADOCon.Errors.Count > 0 Then
                        GoTo Error
                    End If
                    Set adoComm = Nothing
                End If
            End If
                
            If bGrabarEnLog And ContRegBorrados > 0 Then
                ''10/09/2007(ngo):MultiERP
                sConsulta = "SELECT COD FROM ERP WITH(NOLOCK)"
                Set AdoRes = New adodb.Recordset
                AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
                While Not AdoRes.eof
                    sConsulta = "INSERT INTO LOG_PROVI (ACCION,PAI,COD,ORIGEN,USU,ERP) VALUES ('" & Accion_Baja & "',N'" & DblQuote(Pais) & "',N'" & DblQuote(Codigos(i)) & "'," & m_udtOrigen & ",'" & DblQuote(Usuario) & "','" & DblQuote(AdoRes.Fields("COD").Value) & "')"
                    m_oConexion.ADOCon.Execute sConsulta
                    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error
                    
                    Dim ador As adodb.Recordset
                    Set ador = New adodb.Recordset
                    'CALIDAD Sin WITH(NOLOCK) en el FROM para asegurar la recuperaci�n del mayor valor de la tabla
                    ador.Open "SELECT SCOPE_IDENTITY()", m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
                    If Not ador.eof Then
                        lIdLogPROVI = ador.Collect(0)
                    End If
                    ador.Close
                    Set ador = Nothing
                    
                    If Not oDen Is Nothing Then
                        For Each oMulti In oDen
                            sConsulta = "INSERT INTO LOG_PROVI_DEN (ID_LOG_PROVI,DEN,IDI) VALUES (" & lIdLogPROVI & ","
                            sConsulta = sConsulta & StrToSQLNULLNVar(oMulti.DEN) & ",'" & DblQuote(oMulti.COD) & "')"
                            m_oConexion.ADOCon.Execute sConsulta
                            If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error
                        Next
                    End If
                    
                    AdoRes.MoveNext
                Wend
                AdoRes.Close
                Set AdoRes = Nothing
            End If
        End If
    Next
    
    m_oConexion.ADOCon.Execute "SET XACT_ABORT OFF"
    m_oConexion.ADOCon.Execute "COMMIT TRAN"
    
    If iRes > 0 Then
        udtTESError.NumError = TESImposibleEliminar
        udtTESError.Arg1 = vNoEliminados
    End If
    EliminarProvinciasDeBaseDatos = udtTESError
    Exit Function

Error:
        udtTESError = basErrores.TratarError(m_oConexion.ADOCon.Errors)
        
        m_oConexion.ADOCon.Execute "SET XACT_ABORT OFF"
        
        EliminarProvinciasDeBaseDatos = udtTESError
      

End Function

Public Function Add(ByVal COD As String, ByVal DEN As String, Optional ByVal varIndice As Variant, Optional ByVal ID As Integer, Optional ByVal FecAct As Variant, Optional ByVal varEstado As Variant) As CProvincia
    
    Dim objnewmember As CProvincia
    Set objnewmember = New CProvincia
    
    objnewmember.COD = COD
    objnewmember.DEN = DEN
    objnewmember.FecAct = FecAct
    objnewmember.EstadoIntegracion = varEstado 'Estado integracion

    Set objnewmember.Conexion = m_oConexion
    
    
    If Not IsMissing(ID) Then
        objnewmember.ID = ID
    End If
    
    If Not IsMissing(varIndice) And Not IsNull(varIndice) Then
        objnewmember.Indice = varIndice
        mCol.Add objnewmember, CStr(varIndice)
    Else
       mCol.Add objnewmember, CStr(COD)
    End If
    
    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing


End Function
Public Property Let Portal(ByVal Data As Boolean)
    Let m_bPortal = Data
End Property

Public Property Get Item(vntIndexKey As Variant) As CProvincia

On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:

    Set Item = Nothing
    
End Property
Public Property Get eof() As Boolean
    eof = m_bEOF
End Property
Friend Property Let eof(ByVal b As Boolean)
    m_bEOF = b
End Property

Friend Property Set Conexion(ByVal con As CConexion)
Set m_oConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = m_oConexion
End Property



Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
     Count = mCol.Count
End If


End Property


Public Sub Remove(vntIndexKey As Variant)
   
    mCol.Remove vntIndexKey
End Sub


Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
     Set NewEnum = mCol.[_NewEnum]
End Property


Private Sub Class_Initialize()
 
    Set mCol = New Collection
End Sub


Private Sub Class_Terminate()
    'destroys collection when this class is terminated
    Set mCol = Nothing
    Set m_oConexion = Nothing

End Sub

Private Function GrabarEnLog() As Boolean
'***********************************************************************************
'*** Descripci�n: Nos indica si hay que grabar o no el cambio en el log de       ***
'***              monedas (tabla LOG_PROVI) y carga en la variable                 ***
'***              del m�dulo m_udtOrigen el origen del movimiento                ***
'*** Parametros:  Ninguno                                                        ***
'*** Devuelve:    True (hay que grabar) False (No hay que grabar)                ***
'***********************************************************************************

    If gParametrosGenerales.gbACTIVLOG Or gParametrosIntegracion.gaExportar(EntidadIntegracion.Provi) Then
        If gParametrosGenerales.gbACTIVLOG And gParametrosIntegracion.gaExportar(EntidadIntegracion.Provi) Then
            m_udtOrigen = FSGSIntReg
        Else
            If gParametrosGenerales.gbACTIVLOG Then
                m_udtOrigen = FSGSReg
            Else
                m_udtOrigen = FSGSInt
            End If
        End If
        GrabarEnLog = True
    Else
        GrabarEnLog = False
    End If

End Function
