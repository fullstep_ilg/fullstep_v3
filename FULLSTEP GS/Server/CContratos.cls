VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CContratos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

''' *** Clase: CContratos
''' *** Creacion: 02/12/2002 (EPB)

''' Variables que almacenan las propiedades de los objetos
Private m_Col As Collection
Private m_bEOF As Boolean
Private m_oConexion As CConexion

''' Control de errores
Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum


Public Function Add(ByVal lId As Variant, ByVal sNombre As String, ByVal sExtension As String, ByVal iAnyo As Integer, ByVal sGMN1 As String, ByVal iProce As Long, _
                    ByVal sProve As String, Optional ByVal vUsu As Variant, Optional ByVal vSize As Variant, Optional ByVal vFecAct As Variant, _
                    Optional ByVal varIndice As Variant) As CContrato
    Dim sCod As String
    Dim objnewmember As CContrato
    
    Set objnewmember = New CContrato
    
    If Not IsMissing(lId) Then
        objnewmember.Id = lId
    Else
        objnewmember.Id = Null
    End If
    objnewmember.Nombre = sNombre
    objnewmember.Extension = sExtension
    objnewmember.Anyo = iAnyo
    objnewmember.GMN1Cod = sGMN1
    objnewmember.Proce = iProce
    objnewmember.Prove = sProve
    If Not IsMissing(vUsu) Then
        objnewmember.Usuario = vUsu
    Else
        objnewmember.Usuario = Null
    End If
    If Not IsMissing(vSize) Then
        objnewmember.DataSize = vSize
    Else
        objnewmember.DataSize = Null
    End If
    If Not IsMissing(vFecAct) Then
        objnewmember.FecAct = vFecAct
    Else
        objnewmember.FecAct = Null
    End If
    
    Set objnewmember.Conexion = m_oConexion
    
    If Not IsMissing(varIndice) And Not IsNull(varIndice) Then
        objnewmember.Indice = varIndice
        m_Col.Add objnewmember, CStr(varIndice)
    Else
        If Not IsMissing(lId) And Not IsNull(lId) Then
            m_Col.Add objnewmember, CStr(lId)
        Else
            sCod = sGMN1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(sGMN1))
            sCod = CStr(iAnyo) & sCod & CStr(iProce)
            sCod = sCod & sProve & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(sProve))
            sCod = sCod & sNombre
            m_Col.Add objnewmember, sCod
        End If
    End If
            
    Set Add = objnewmember
    
    Set objnewmember = Nothing
    
End Function
                    


Public Property Get Item(vntIndexKey As Variant) As CContrato

On Error GoTo NoSeEncuentra:

    ''' Devolvemos el item de la coleccion privada
    
    Set Item = m_Col(vntIndexKey)
     
    Exit Property

NoSeEncuentra:

    Set Item = Nothing
    
End Property
Public Property Get eof() As Boolean

    ''' * Objetivo: Devolver variable privada EOF
    ''' * Recibe: Nada
    ''' * Devuelve: Variable privada EOF
    
    eof = m_bEOF
    
End Property
Friend Property Set Conexion(ByVal con As CConexion)

    ''' * Objetivo: Dar valor a la variable privada Conexion
    ''' * Recibe: Conexion
    ''' * Devuelve: Nada
    
    Set m_oConexion = con
    
End Property
Friend Property Get Conexion() As CConexion

    ''' * Objetivo: Devolver variable privada Conexion
    ''' * Recibe: Nada
    ''' * Devuelve: Conexion
    
    Set Conexion = m_oConexion
    
End Property
Public Property Get Count() As Long

    ''' * Objetivo: Devolver variable privada Count
    ''' * Recibe: Nada
    ''' * Devuelve: Numero de elementos de la coleccion

    If m_Col Is Nothing Then
        Count = 0
    Else
        Count = m_Col.Count
    End If

End Property
Public Sub Remove(vntIndexKey As Variant)

    ''' * Objetivo: Eliminar de la coleccion
    ''' * Recibe: Indice a eliminar
    ''' * Devuelve: Nada
   
    m_Col.Remove vntIndexKey
    
End Sub
Private Sub Class_Initialize()

    ''' * Objetivo: Crear la coleccion

    Set m_Col = New Collection
    
End Sub
Private Sub Class_Terminate()

    ''' * Objetivo: Limpiar memoria
    Set m_Col = Nothing
    Set m_oConexion = Nothing
End Sub
Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
    
    Set NewEnum = m_Col.[_NewEnum]

End Property

''' <summary>
''' Devuelve contratos proximos a expirar
''' </summary>
''' <param name="sPer">Codigo persona</param>
''' <param name="ParametroEntrada2">Explicaci�n par�metro 2</param>
''' <returns>recordset con los contratos</returns>
''' <remarks>Llamada desde:=frmAvisoDespublicacion.frm; Tiempo m�ximo;0,2seg</remarks>
Public Function DevolverProximosExpirar(ByVal sPer As String) As adodb.Recordset
Dim sConsulta As String
Dim rs As adodb.Recordset

'********* Precondicion **************************************
If m_oConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CContratos.DevolverProximosExpirar", "No se ha establecido la conexion"
    Exit Function
End If
'*************************************************************
    
    sConsulta = "SELECT DISTINCT  I.ID, C.NOMBRE,c.FEC_INI , c.FEC_FIN"
    sConsulta = sConsulta & " , CONVERT(NVARCHAR,CI.ANYO) + '/' + CONVERT(NVARCHAR,CI.GMN1) + '/' + CONVERT(NVARCHAR,CI.PROCE) PROCESO"
    sConsulta = sConsulta & " , C.ID CONTRATO, CASE WHEN PCR.PER IS NULL THEN 0 ELSE 1 END OBSERVADOR"
    sConsulta = sConsulta & " FROM CONTRATO C WITH(NOLOCK)"
    If sPer <> "" Then
        sConsulta = sConsulta & " INNER JOIN CONTRATO_NOTIF CN WITH (NOLOCK) ON CN.CONTRATO = C.ID AND CN.PER=" & StrToSQLNULL(sPer)
    End If
    
    sConsulta = sConsulta & " INNER JOIN INSTANCIA I WITH (NOLOCK) ON I.ID = C.INSTANCIA"
    sConsulta = sConsulta & " LEFT JOIN PM_COPIA_ROL PCR WITH (NOLOCK) ON PCR.WORKFLOW=I.WORKFLOW AND PCR.TIPO=5"
    sConsulta = sConsulta & " LEFT JOIN INSTANCIA_ROL IR WITH (NOLOCK) ON IR.INSTANCIA=I.ID AND IR.ROL=PCR.ROL AND IR.PER=" & StrToSQLNULL(sPer)
    sConsulta = sConsulta & " LEFT JOIN CONTRATO_ITEM CI WITH (NOLOCK) ON CI.CONTRATO = C.ID"
    sConsulta = sConsulta & " Where (DateDiff(Day, GETDATE(), c.FEC_FIN) >= 0"
    sConsulta = sConsulta & " And DateDiff(Day, GETDATE(), C.FEC_FIN) <= ALERTA)"
    
    'PARA SACAR LOS EXPIRADOS NO RENEGOCIADOS
    sConsulta = sConsulta & " OR"
    sConsulta = sConsulta & " ("
    sConsulta = sConsulta & " DateDiff(Day, GETDATE(), c.FEC_FIN) < 0"
    sConsulta = sConsulta & " AND C.ID NOT IN (SELECT CONTRATO_ORIGEN FROM PROCE P WITH (NOLOCK))"
    sConsulta = sConsulta & " )"


    Set rs = New adodb.Recordset
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    
    Set rs.ActiveConnection = Nothing
    
    Set DevolverProximosExpirar = rs

End Function
