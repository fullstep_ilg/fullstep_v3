VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CDiccionario"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* Diccionario **********************************
'*             Autor : Javier Arana
'*             Creada : 16/10/98
'****************************************************************
Option Explicit
Private oConexion As CConexion
Friend Property Get Conexion() As CConexion

    Set Conexion = oConexion
    
End Property

Friend Property Set Conexion(ByVal oCon As CConexion)
    Set oConexion = oCon
End Property

''' <summary>
''' Devuelve el valor del campo LONGITUD de la tabla DIC para los c�digos
''' </summary>
''' <returns>colecci�n de longitudes de c�digos</returns>
''' <remarks>Tiempo m�ximo</remarks>
''' <revision>JVS 22/08/2011</revision>

Public Function DevolverLongitudesDeCodigos() As LongitudesDeCodigos
    
    Dim rs As New adodb.Recordset
    
    rs.Open "SELECT LONGITUD,NOMBRE FROM DIC WITH (NOLOCK) ORDER BY ID", oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    
    While Not rs.eof
            
        Select Case rs(1).Value
        Case "ART"
            gLongitudesDeCodigos.giLongCodART = rs(0).Value
        Case "CAL"
            gLongitudesDeCodigos.giLongCodCAL = rs(0).Value
        Case "COM"
            gLongitudesDeCodigos.giLongCodCOM = rs(0).Value
        'Case "CON"
        '    gLongitudesDeCodigos.giLongCodCON = rs(0).Value
        Case "DEP"
            gLongitudesDeCodigos.giLongCodDEP = rs(0).Value
        Case "DEST"
            gLongitudesDeCodigos.giLongCodDEST = rs(0).Value
        Case "EQP"
            gLongitudesDeCodigos.giLongCodEQP = rs(0).Value
        Case "GMN1"
            gLongitudesDeCodigos.giLongCodGMN1 = rs(0).Value
        Case "GMN2"
            gLongitudesDeCodigos.giLongCodGMN2 = rs(0).Value
        Case "GMN3"
            gLongitudesDeCodigos.giLongCodGMN3 = rs(0).Value
        Case "GMN4"
            gLongitudesDeCodigos.giLongCodGMN4 = rs(0).Value
        Case "MON"
            gLongitudesDeCodigos.giLongCodMON = rs(0).Value
        Case "OFEEST"
            gLongitudesDeCodigos.giLongCodOFEEST = rs(0).Value
        Case "PAG"
            gLongitudesDeCodigos.giLongCodPAG = rs(0).Value
        Case "VIA_PAG"
            gLongitudesDeCodigos.giLongCodVIAPAG = rs(0).Value
        Case "PAI"
            gLongitudesDeCodigos.giLongCodPAI = rs(0).Value
        Case "PER"
            gLongitudesDeCodigos.giLongCodPER = rs(0).Value
        Case "PERF"
            gLongitudesDeCodigos.giLongCodPERF = rs(0).Value
        Case "PRESCON1"
            gLongitudesDeCodigos.giLongCodPRESCON1 = rs(0).Value
        Case "PRESCON2"
            gLongitudesDeCodigos.giLongCodPRESCON2 = rs(0).Value
        Case "PRESCON3"
            gLongitudesDeCodigos.giLongCodPRESCON3 = rs(0).Value
        Case "PRESCON4"
            gLongitudesDeCodigos.giLongCodPRESCON4 = rs(0).Value
        Case "PRESPROY1"
            gLongitudesDeCodigos.giLongCodPRESPROY1 = rs(0).Value
        Case "PRESPROY2"
            gLongitudesDeCodigos.giLongCodPRESPROY2 = rs(0).Value
        Case "PRESPROY3"
            gLongitudesDeCodigos.giLongCodPRESPROY3 = rs(0).Value
        Case "PRESPROY4"
            gLongitudesDeCodigos.giLongCodPRESPROY4 = rs(0).Value
        Case "PROVE"
            gLongitudesDeCodigos.giLongCodPROVE = rs(0).Value
        Case "PROVI"
            gLongitudesDeCodigos.giLongCodPROVI = rs(0).Value
        Case "ROL"
            gLongitudesDeCodigos.giLongCodROL = rs(0).Value
        Case "UNI"
            gLongitudesDeCodigos.giLongCodUNI = rs(0).Value
        Case "UON1"
            gLongitudesDeCodigos.giLongCodUON1 = rs(0).Value
        Case "UON2"
            gLongitudesDeCodigos.giLongCodUON2 = rs(0).Value
        Case "UON3"
            gLongitudesDeCodigos.giLongCodUON3 = rs(0).Value
        Case "UON4"
            gLongitudesDeCodigos.giLongCodUON4 = rs(0).Value
        Case "USU"
            gLongitudesDeCodigos.giLongCodUSU = rs(0).Value
        Case "ACT"
            gLongitudesDeCodigos.giLongCodACT1 = rs(0).Value
        Case "SUBACT1"
            gLongitudesDeCodigos.giLongCodACT2 = rs(0).Value
        Case "SUBACT2"
            gLongitudesDeCodigos.giLongCodACT3 = rs(0).Value
        Case "SUBACT3"
            gLongitudesDeCodigos.giLongCodACT4 = rs(0).Value
        Case "SUBACT4"
            gLongitudesDeCodigos.giLongCodACT5 = rs(0).Value
        Case "PRESCONCEP31"
            gLongitudesDeCodigos.giLongCodPRESCONCEP31 = rs(0).Value
        Case "PRESCONCEP32"
            gLongitudesDeCodigos.giLongCodPRESCONCEP32 = rs(0).Value
        Case "PRESCONCEP33"
            gLongitudesDeCodigos.giLongCodPRESCONCEP33 = rs(0).Value
        Case "PRESCONCEP34"
            gLongitudesDeCodigos.giLongCodPRESCONCEP34 = rs(0).Value
        Case "PRESCONCEP41"
            gLongitudesDeCodigos.giLongCodPRESCONCEP41 = rs(0).Value
        Case "PRESCONCEP42"
            gLongitudesDeCodigos.giLongCodPRESCONCEP42 = rs(0).Value
        Case "PRESCONCEP43"
            gLongitudesDeCodigos.giLongCodPRESCONCEP43 = rs(0).Value
        Case "PRESCONCEP44"
            gLongitudesDeCodigos.giLongCodPRESCONCEP44 = rs(0).Value
        Case "CAT1"
            gLongitudesDeCodigos.giLongCodCAT1 = rs(0).Value
        Case "CAT2"
            gLongitudesDeCodigos.giLongCodCAT2 = rs(0).Value
        Case "CAT3"
            gLongitudesDeCodigos.giLongCodCAT3 = rs(0).Value
        Case "CAT4"
            gLongitudesDeCodigos.giLongCodCAT4 = rs(0).Value
        Case "CAT5"
            gLongitudesDeCodigos.giLongCodCAT5 = rs(0).Value
        Case "GRUPO"
            gLongitudesDeCodigos.giLongCodGRUPOPROCE = rs(0).Value
        Case "DIC"

        Case "CONTR"
            
        Case "VARCAL"
            gLongitudesDeCodigos.giLongCodVarCal = rs(0).Value
        Case "DENART"
            gLongitudesDeCodigos.giLongCodDENART = rs(0).Value
        Case "PRES5_0"
            gLongitudesDeCodigos.giLongCodPres5Niv0 = rs(0).Value
        Case "PRES5_1"
            gLongitudesDeCodigos.giLongCodPres5Niv1 = rs(0).Value
        Case "PRES5_2"
            gLongitudesDeCodigos.giLongCodPres5Niv2 = rs(0).Value
        Case "PRES5_3"
            gLongitudesDeCodigos.giLongCodPres5Niv3 = rs(0).Value
        Case "PRES5_4"
            gLongitudesDeCodigos.giLongCodPres5Niv4 = rs(0).Value
        Case "PROVETREL"
            gLongitudesDeCodigos.giLongCodProveTipoRel = rs(0).Value
        Case "TIPPED"
            gLongitudesDeCodigos.giLongCodTIPPED = rs(0).Value
        Case "DENTIPPED"
            gLongitudesDeCodigos.giLongCodDENTIPPED = rs(0).Value
        End Select
                        
        rs.MoveNext

    Wend
        
    rs.Close
    Set rs = Nothing
        

    DevolverLongitudesDeCodigos = gLongitudesDeCodigos
        
End Function
