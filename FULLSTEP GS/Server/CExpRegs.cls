VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CExpRegs"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private mCol As Collection

Public Function Add(ByVal IdSolicitud As Long, ByVal CampoProve As String, ByVal ExpRegDescr As String, ByVal ExpReg As String, ByVal Operador As Integer) As CExpReg
    Dim oExpReg As CExpReg
    
    On Error GoTo ElementoRepetido
    
    Set oExpReg = New CExpReg
        
    With oExpReg
        .IdSolicitud = IdSolicitud
        .CampoProve = CampoProve
        .ExpRegDescr = ExpRegDescr
        .ExpReg = ExpReg
        .Operador = Operador
    End With
        
    mCol.Add oExpReg
    Set Add = oExpReg
    Set oExpReg = Nothing
    Exit Function
    
ElementoRepetido:
    Set Add = Nothing

End Function

Public Property Get Item(vntIndexKey As Variant) As CAccion
    On Error GoTo vacio:
 
    Set Item = mCol(vntIndexKey)
    Exit Property
    
vacio:
    Set Item = Nothing
End Property

Public Property Get Count() As Long
    If mCol Is Nothing Then
        Count = 0
    Else
        Count = mCol.Count
    End If
End Property

Public Sub Remove(vntIndexKey As Variant)
    On Error GoTo NoExisteLaClave:
    
    mCol.Remove vntIndexKey

NoExisteLaClave:
    Resume Next
End Sub

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
    Set NewEnum = mCol.[_NewEnum]
End Property

Private Sub Class_Initialize()
    Set mCol = New Collection
End Sub

Private Sub Class_Terminate()
    Set mCol = Nothing
End Sub
