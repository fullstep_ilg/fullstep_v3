VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CAdjsDeArt"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CAdjsDeArt **********************************
'*             Autor : Javier Arana
'*             Creada : 24/2/98
'****************************************************************


Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum

Private mCol As Collection
Private mvarConexion As CConexion
Private mvarConexionServer As CConexionDeUseServer
Private mvarEOF As Boolean

Friend Property Set Conexion(ByVal con As CConexion)
Set mvarConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = mvarConexion
End Property
Friend Property Set ConexionServer(ByVal con As CConexionDeUseServer)
Set mvarConexionServer = con
End Property
Friend Property Get ConexionServer() As CConexionDeUseServer
Set ConexionServer = mvarConexionServer
End Property

Public Property Get eof() As Boolean
    eof = mvarEOF
End Property
Friend Property Let eof(ByVal b As Boolean)
    mvarEOF = b
End Property

Public Property Get Item(vntIndexKey As Variant) As CAdjDeArt
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
     Count = mCol.Count
End If


End Property

''' <summary>
''' Carga el historico de adjudicaciones de art�culos
''' en los par�metros.
''' </summary>
''' <param name="CriterioOrdenacion">Por qu� se ordena</param>
''' <param name="oArticulo">De qu� art�culo</param>
''' <param name="AnterioresAlSummit">Que se han metido a mano  no desde la adjudicaci�n de un proceso</param>
''' <param name="UsarIndice">Indica si la colecci�m interna se va a indexar</param>
''' <returns>El n� de registros</returns>
''' <remarks>Tiempo m�ximo: 0 seg </remarks>
''' <revision>LTG 22/12/2011</revision>

Public Function CargarTodasLasAdjsDeArt(ByVal CriterioOrdenacion As TipoOrdenacionAdjs, ByVal oArticulo As CArticulo, Optional ByVal AnterioresAlSummit As Boolean, _
        Optional ByVal UsarIndice As Boolean) As Long
    Dim rs As New adodb.Recordset
    Dim fldDest As adodb.Field
    Dim fldUni As adodb.Field
    Dim fldId As adodb.Field
    Dim fldCodProv As adodb.Field
    Dim fldDescr As adodb.Field
    Dim fldPrec As adodb.Field
    Dim fldPorcen As adodb.Field
    Dim fldFecini As adodb.Field
    Dim fldFecfin As adodb.Field
    Dim fldCant As adodb.Field
    Dim sConsulta As String
    Dim lIndice As Long
    Dim iNumAdjDeArt As Integer
    
    '********* Precondicion **************************************
    If mvarConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CAdjsDeArt.CargarTodosLosAdjsDeArt", "No se ha establecido la conexion"
        Exit Function
    End If
    '*************************************************************
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
    iNumAdjDeArt = 0
    
    Select Case basParametros.gParametrosGenerales.giNEM
        Case 1
        Case 2
        Case 3
        Case 4
            sConsulta = "SELECT DEST,UNI,ID,CODPROVE,DENPROVE AS DESCR, PREC, PORCEN, FECINI, FECFIN,CANT FROM ART4_ADJ WITH (NOLOCK) WHERE ART='" & DblQuote(oArticulo.Cod) & "'"
    End Select
           
    If AnterioresAlSummit Then
        sConsulta = sConsulta & " AND PROCE IS NULL"
    'Else
    '    sConsulta = sConsulta & " AND PROCE IS NOT NULL"
    End If
    
    Select Case CriterioOrdenacion
            
        Case TipoOrdenacionAdjs.OrdAdjPorDescr
            
            sConsulta = sConsulta & " ORDER BY DESCR"
            
        Case TipoOrdenacionAdjs.OrdAdjPorDest
                    
            sConsulta = sConsulta & " ORDER BY DEST"
                    
        Case TipoOrdenacionAdjs.OrdAdjPorUni
                    
            sConsulta = sConsulta & " ORDER BY UNI"
        
        Case TipoOrdenacionAdjs.OrdAdjPorPrec
                    
            sConsulta = sConsulta & " ORDER BY PREC"
            
        Case TipoOrdenacionAdjs.OrdAdjPorCant
        
            sConsulta = sConsulta & " ORDER BY CANT"
                        
        Case TipoOrdenacionAdjs.OrdAdjPorPorc
        
            sConsulta = sConsulta & " ORDER BY PORCEN"
            
        Case TipoOrdenacionAdjs.OrdAdjPorFecIni
                    
            sConsulta = sConsulta & " ORDER BY FECINI"
                    
        Case TipoOrdenacionAdjs.OrdAdjPorFecFin
                    
            sConsulta = sConsulta & " ORDER BY FECFIN"
                    
    End Select
    
    'mvarConexion.rdoSummitCon.Execute "SET CONCAT_NULL_YIELDS_NULL OFF", rdExecDirect
    rs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    'mvarConexion.rdoSummitCon.Execute "SET CONCAT_NULL_YIELDS_NULL ON", rdExecDirect
    
    Set mCol = Nothing
    Set mCol = New Collection
    
    If rs.eof Then
        CargarTodasLasAdjsDeArt = 0
    Else
        
        Set fldDest = rs.Fields(0)
        Set fldUni = rs.Fields(1)
        Set fldId = rs.Fields(2)
        Set fldCodProv = rs.Fields(3)
        Set fldDescr = rs.Fields(4)
        Set fldPrec = rs.Fields(5)
        Set fldPorcen = rs.Fields(6)
        Set fldFecini = rs.Fields(7)
        Set fldFecfin = rs.Fields(8)
        Set fldCant = rs.Fields(9)
            
        iNumAdjDeArt = 0
        
        If UsarIndice Then
            lIndice = 0
                   
            While Not rs.eof
                ' Dejamos en manos del programador cuando asignar la conexion al obejto usuario
                Me.Add oArticulo, fldId.Value, fldDest.Value, fldUni.Value, fldPrec.Value, fldCodProv.Value, fldPorcen.Value, fldFecini.Value, fldFecfin.Value, fldDescr.Value, lIndice, fldCant.Value
                rs.MoveNext
                lIndice = lIndice + 1
            Wend
            
            rs.Close
            Set rs = Nothing
        
        Else
            While Not rs.eof
                ' Dejamos en manos del programador cuando asignar la conexion al obejto usuario
                Me.Add oArticulo, fldId.Value, fldDest.Value, fldUni.Value, fldPrec.Value, fldCodProv.Value, fldPorcen.Value, fldFecini.Value, fldFecfin.Value, fldDescr.Value, , fldCant.Value
                rs.MoveNext
                lIndice = lIndice + 1
            Wend
            
            rs.Close
            
        End If
        sConsulta = "SELECT count(*) FROM ART4_ADJ WITH (NOLOCK) WHERE ART='" & DblQuote(oArticulo.Cod) & "'"
        sConsulta = sConsulta & " AND PROCE IS NOT NULL"
        rs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
        CargarTodasLasAdjsDeArt = rs(0).Value
        
        Set fldDest = Nothing
        Set fldUni = Nothing
        Set fldId = Nothing
        Set fldCodProv = Nothing
        Set fldDescr = Nothing
        Set fldPrec = Nothing
        Set fldPorcen = Nothing
        Set fldFecini = Nothing
        Set fldFecfin = Nothing
        Set fldCant = Nothing
    End If
    
    rs.Close
    Set rs = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CAdjsDeArt", "CargarTodasLasAdjsDeArt", ERR, Erl)
        Exit Function
    End If
End Function
Public Function Add(ByVal oArticulo As CArticulo, ByVal Id As Integer, ByVal Dest As String, ByVal Uni As String, ByVal Precio As Double, Optional ByVal Prove As Variant, _
            Optional ByVal Porcentaje As Variant, Optional ByVal FecIni As Variant, Optional ByVal FecFin As Variant, Optional ByVal Descr As Variant, _
            Optional ByVal varIndice As Variant, Optional ByVal Cantidad As Variant, Optional ByVal proceAnyo As Variant, Optional ByVal proceGmn As Variant, _
            Optional ByVal ProceCod As Variant, Optional ByVal sMon As String, Optional sUsu As String, Optional dFecha_PreAdj As Date, Optional iUltimo As Integer) As CAdjDeArt
    'create a new object
    Dim objnewmember As CAdjDeArt
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New CAdjDeArt
      
    Set objnewmember.Articulo = oArticulo
    objnewmember.Id = Id
    objnewmember.DestCod = Dest
    objnewmember.UniCod = Uni
    objnewmember.Precio = Precio
    objnewmember.Cantidad = Cantidad
    Set objnewmember.Conexion = mvarConexion
    Set objnewmember.ConexionServer = mvarConexionServer
    
    If IsMissing(Prove) Then
        objnewmember.ProveCod = Null
    Else
        objnewmember.ProveCod = Prove
    End If
    
    If IsMissing(Porcentaje) Then
        objnewmember.Porcentaje = 100
    Else
        If IsNull(Porcentaje) Then
            objnewmember.Porcentaje = 100
        Else
            objnewmember.Porcentaje = Porcentaje
        End If
    End If
    
    If IsMissing(FecIni) Then
        objnewmember.FechaInicioSuministro = Null
    Else
        objnewmember.FechaInicioSuministro = FecIni
    End If
    If IsMissing(FecFin) Then
        objnewmember.FechaFinSuministro = Null
    Else
        objnewmember.FechaFinSuministro = FecFin
    End If
    
    If IsMissing(Descr) Then
        objnewmember.Descr = Null
    Else
        objnewmember.Descr = Descr
    End If
    
    objnewmember.proceAnyo = proceAnyo
    objnewmember.proceGmn = proceGmn
    objnewmember.ProceCod = ProceCod
    objnewmember.Moneda = sMon
    
    objnewmember.Usuario = sUsu
    If IsDate(dFecha_PreAdj) Then objnewmember.Fecha_PreAdj = dFecha_PreAdj
    objnewmember.Ultimo_PreAdj = iUltimo
    
    If Not IsMissing(varIndice) And Not IsNull(varIndice) Then
        objnewmember.Indice = varIndice
        mCol.Add objnewmember, CStr(varIndice)
    Else
       mCol.Add objnewmember, CStr(Id)
    End If
    
    If IsMissing(Cantidad) Then
        objnewmember.Cantidad = Null
    Else
        objnewmember.Cantidad = Cantidad
    End If
    
    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CAdjsDeArt", "Add", ERR, Erl)
        Exit Function
    End If

End Function

Public Sub Remove(vntIndexKey As Variant)
    
        mCol.Remove vntIndexKey
    
End Sub


Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
     Set NewEnum = mCol.[_NewEnum]
End Property


Private Sub Class_Initialize()

  
    Set mCol = New Collection
End Sub


Private Sub Class_Terminate()
    'AdjDeArtroys collection when this class is terminated
    Set mCol = Nothing
    Set mvarConexion = Nothing
    Set mvarConexionServer = Nothing
End Sub

Public Function BorrarEnModoIndice(ByVal iIndice As Integer)
Dim IndFor As Integer
Dim oAdjDeArt As CAdjDeArt

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    For IndFor = iIndice To mCol.Count - 2
            
        mCol.Remove (CStr(IndFor))
        Set oAdjDeArt = mCol.Item(CStr(IndFor + 1))
        mCol.Add oAdjDeArt, CStr(IndFor)
        Set oAdjDeArt = Nothing
        
    Next IndFor
    
    mCol.Remove CStr(IndFor)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CAdjsDeArt", "BorrarEnModoIndice", ERR, Erl)
        Exit Function
    End If
    
End Function

Public Sub DevolverProveMaxCantidad(ByVal oArticulo As CArticulo)
Dim rs As New adodb.Recordset
Dim fldId As adodb.Field
Dim fldDest As adodb.Field
Dim fldUni As adodb.Field
Dim fldPrec As adodb.Field
Dim fldCodProve As adodb.Field
Dim fldPorcen As adodb.Field
Dim fldFecini As adodb.Field
Dim fldFecfin As adodb.Field
Dim fldCant As adodb.Field

Dim sConsulta As String

'********* Precondicion **************************************
If mvarConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CAdjsDeArt.CargarTodosLosAdjsDeArt", "No se ha establecido la conexion"
    Exit Sub
End If
'*************************************************************
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

sConsulta = "SELECT TOP 1 ID,DEST,UNI,CODPROVE,PREC,PORCEN,FECINI,FECFIN,CANT FROM ART4_ADJ"

'Select Case basParametros.gParametrosGenerales.giNEM
'    Case 1
'        sConsulta = sConsulta & " WHERE GMN1='" & DblQuote(oArticulo.GMN1Cod) & "' AND ART='" & DblQuote(oArticulo.Cod) & "'"
'
'    Case 2
'        sConsulta = sConsulta & " WHERE GMN1='" & DblQuote(oArticulo.GMN1Cod) & "' AND  GMN2='" & DblQuote(oArticulo.GMN2Cod) & "' AND ART='" & DblQuote(oArticulo.Cod) & "'"
'
'    Case 3
'        sConsulta = sConsulta & " WHERE GMN1='" & DblQuote(oArticulo.GMN1Cod) & "' AND  GMN2='" & DblQuote(oArticulo.GMN2Cod) & "' AND GMN3='" & DblQuote(oArticulo.GMN3Cod) & "' AND ART='" & DblQuote(oArticulo.Cod) & "'"
'
'    Case 4
'        sConsulta = sConsulta & " WHERE GMN1='" & DblQuote(oArticulo.GMN1Cod) & "' AND  GMN2='" & DblQuote(oArticulo.GMN2Cod) & "' AND GMN3='" & DblQuote(oArticulo.GMN3Cod) & "' AND GMN4='" & DblQuote(oArticulo.GMN4Cod) & "' AND ART='" & DblQuote(oArticulo.Cod) & "'"
'End Select
sConsulta = sConsulta & " WHERE ART='" & DblQuote(oArticulo.Cod) & "'"
sConsulta = sConsulta & " ORDER BY CANT DESC,PORCEN DESC"

rs.Open sConsulta, mvarConexion.ADOCon, adOpenStatic, adLockReadOnly
      
If rs.eof Then
    rs.Close
    Set rs = Nothing

    Set mCol = Nothing
    Set mCol = New Collection
    Exit Sub
      
Else
    Set mCol = Nothing
    Set mCol = New Collection
    
    Set fldId = rs.Fields(0)
    Set fldDest = rs.Fields(1)
    Set fldUni = rs.Fields(2)
    Set fldCodProve = rs.Fields(3)
    Set fldPrec = rs.Fields(4)
    Set fldPorcen = rs.Fields(5)
    Set fldFecini = rs.Fields(6)
    Set fldFecfin = rs.Fields(7)
    Set fldCant = rs.Fields(8)
        
    Me.Add oArticulo, fldId.Value, fldDest.Value, fldUni.Value, fldPrec.Value, fldCodProve.Value, fldPorcen.Value, fldFecini.Value, fldFecfin.Value, , 0, fldCant.Value

    rs.Close
    Set rs = Nothing
        
End If

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CAdjsDeArt", "DevolverProveMaxCantidad", ERR, Erl)
        Exit Sub
    End If
        
End Sub

Public Sub CargarAdjsDeArt(ByVal CriterioOrdenacion As TipoOrdenacionAdjs, ByVal oArticulo As CArticulo, Optional ByVal UsarIndice As Boolean, Optional ByVal Prove As String)
Dim rs As New adodb.Recordset
Dim sConsulta As String
Dim lIndice As Long
Dim iNumAdjDeArt As Integer
Dim fldId As adodb.Field
Dim fldDest As adodb.Field
Dim fldUni As adodb.Field
Dim fldPrec As adodb.Field
Dim fldCodProve As adodb.Field
Dim fldPorcen As adodb.Field
Dim fldFecini As adodb.Field
Dim fldFecfin As adodb.Field
Dim fldCant As adodb.Field
Dim fldDenProve As adodb.Field

'********* Precondicion **************************************
If mvarConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CAdjsDeArt.CargarTodosLosAdjsDeArt", "No se ha establecido la conexion"
    Exit Sub
End If
'*************************************************************
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

iNumAdjDeArt = 0

Select Case basParametros.gParametrosGenerales.giNEM
    Case 1
        
    Case 2
        
    Case 3
        
    Case 4
        sConsulta = "SELECT DEST,UNI,ID,CODPROVE,DENPROVE AS DESCR, PREC, PORCEN, FECINI, FECFIN,CANT FROM ART4_ADJ WHERE ART='" & DblQuote(oArticulo.Cod) & "'"
End Select
       
If Not IsMissing(Prove) And Prove <> "" Then
    sConsulta = sConsulta & " AND CODPROVE ='" & Prove & "'"
End If

Select Case CriterioOrdenacion
        
    Case TipoOrdenacionAdjs.OrdAdjPorDescr
        
        sConsulta = sConsulta & " ORDER BY DESCR"
        
    Case TipoOrdenacionAdjs.OrdAdjPorDest
                
        sConsulta = sConsulta & " ORDER BY DEST"
                
    Case TipoOrdenacionAdjs.OrdAdjPorUni
                
        sConsulta = sConsulta & " ORDER BY UNI"
    
    Case TipoOrdenacionAdjs.OrdAdjPorPrec
                
        sConsulta = sConsulta & " ORDER BY PREC"
        
    Case TipoOrdenacionAdjs.OrdAdjPorCant
    
        sConsulta = sConsulta & " ORDER BY CANT"
                    
    Case TipoOrdenacionAdjs.OrdAdjPorPorc
    
        sConsulta = sConsulta & " ORDER BY PORCEN"
        
    Case TipoOrdenacionAdjs.OrdAdjPorFecIni
                
        sConsulta = sConsulta & " ORDER BY FECINI"
                
    Case TipoOrdenacionAdjs.OrdAdjPorFecFin
                
        sConsulta = sConsulta & " ORDER BY FECFIN"
                
End Select

rs.Open sConsulta, mvarConexion.ADOCon, adOpenStatic, adLockReadOnly

If rs.eof Then
    rs.Close
    Set rs = Nothing
    Set mCol = Nothing
    Set mCol = New Collection
    Exit Sub
    
Else
    Set mCol = Nothing
    Set mCol = New Collection
        
    iNumAdjDeArt = 0
    
    Set fldDest = rs.Fields(0)
    Set fldUni = rs.Fields(1)
    Set fldId = rs.Fields(2)
    Set fldCodProve = rs.Fields(3)
    Set fldDenProve = rs.Fields(4)
    Set fldPrec = rs.Fields(5)
    Set fldPorcen = rs.Fields(6)
    Set fldFecini = rs.Fields(7)
    Set fldFecfin = rs.Fields(8)
    Set fldCant = rs.Fields(9)
    
    If UsarIndice Then
        lIndice = 0
               
        While Not rs.eof
            ' Dejamos en manos del programador cuando asignar la conexion al obejto usuario
            Me.Add oArticulo, fldId.Value, fldDest.Value, fldUni.Value, fldPrec.Value, fldCodProve.Value, fldPorcen.Value, fldFecini.Value, fldFecfin.Value, fldDenProve.Value, lIndice, fldCant.Value
            rs.MoveNext
            lIndice = lIndice + 1
        Wend
        
        rs.Close
        Set rs = Nothing
    
    Else
    
        While Not rs.eof
            'Dejamos en manos del programador cuando asignar la conexion al obejto usuario
            Me.Add oArticulo, fldId.Value, fldDest.Value, fldUni.Value, fldPrec.Value, fldCodProve.Value, fldPorcen.Value, fldFecini.Value, fldFecfin.Value, fldDenProve.Value, , fldCant.Value
            rs.MoveNext
            lIndice = lIndice + 1
        Wend
        
        rs.Close
        
    End If
    
    Set fldDest = Nothing
    Set fldUni = Nothing
    Set fldId = Nothing
    Set fldCodProve = Nothing
    Set fldDenProve = Nothing
    Set fldPrec = Nothing
    Set fldPorcen = Nothing
    Set fldFecini = Nothing
    Set fldFecfin = Nothing
    Set fldCant = Nothing

    Set rs = Nothing
      
End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CAdjsDeArt", "CargarAdjsDeArt", ERR, Erl)
        Exit Sub
    End If

End Sub


''' <summary>DEvuelve el precio de la ultima Adjudicacion de un articulo</summary>
''' <param "oArticulo">El �rticulo </param>
''' <remarks>Llamada desde: frmProce.CargarProvActual </remarks>
''' <remarks>Tiempo m�ximo: < 1 sec </remarks>
''' <revision>LTG 23/10/2012</revision>

Public Sub DevolverProveUltimaAdjudicacion(ByVal sArticulo As String, Optional ByVal sUni As String)
    Dim rs As New adodb.Recordset
    Dim fldId As adodb.Field
    Dim fldUni As adodb.Field
    Dim fldPrec As adodb.Field
    Dim fldCodProve As adodb.Field
    Dim fldPorcen As adodb.Field
    Dim fldFecini As adodb.Field
    Dim oArticulo As CArticulo
    Dim sConsulta2 As String
    
    
    '********* Precondicion **************************************
    If mvarConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CAdjsDeArt.CargarTodosLosAdjsDeArt", "No se ha establecido la conexion"
        Exit Sub
    End If
    '*************************************************************
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oArticulo = New CArticulo
    oArticulo.Cod = sArticulo
    sConsulta2 = " SELECT MAX(ART4_ADJ.FECINI) FECINI,MAX(ART4_ADJ.ID) AS ID,ART4_ADJ.PORCEN,ART4_ADJ.CODPROVE PROVE,ART4_ADJ.PREC,ART4_ADJ.UNI,0 AS TIPO"
    sConsulta2 = sConsulta2 & " FROM ART4_ADJ WITH (NOLOCK)"
    sConsulta2 = sConsulta2 & " WHERE ART4_ADJ.ART=" & StrToSQLNULL(sArticulo) & " AND ART4_ADJ.PROCE is null "
    If sUni <> "" Then sConsulta2 = sConsulta2 & " AND ART4_ADJ.UNI=" & StrToSQLNULL(sUni)
    sConsulta2 = sConsulta2 & " GROUP BY ART4_ADJ.PORCEN,ART4_ADJ.CODPROVE ,ART4_ADJ.PREC,ART4_ADJ.UNI"
    sConsulta2 = sConsulta2 & " UNION "
    sConsulta2 = sConsulta2 & "  SELECT MAX(PROCE.FECULTREU) FECINI,MAX(ART4_ADJ.ID) AS ID,ART4_ADJ.PORCEN,ART4_ADJ.CODPROVE PROVE,ART4_ADJ.PREC,ART4_ADJ.UNI,1 AS TIPO "
    sConsulta2 = sConsulta2 & " FROM ART4_ADJ WITH (NOLOCK)"
    sConsulta2 = sConsulta2 & " INNER JOIN PROCE WITH (NOLOCK) ON ART4_ADJ.ANYO=PROCE.ANYO AND ART4_ADJ.GMN1=PROCE.GMN1 AND ART4_ADJ.PROCE=PROCE.COD"
    sConsulta2 = sConsulta2 & " WHERE ART4_ADJ.ART=" & StrToSQLNULL(sArticulo)
    If sUni <> "" Then sConsulta2 = sConsulta2 & " AND ART4_ADJ.UNI=" & StrToSQLNULL(sUni)
    sConsulta2 = sConsulta2 & " GROUP BY ART4_ADJ.PORCEN,ART4_ADJ.CODPROVE ,ART4_ADJ.PREC,ART4_ADJ.UNI"
    sConsulta2 = sConsulta2 & " ORDER BY  FECINI DESC,TIPO DESC,ART4_ADJ.PORCEN DESC,ID DESC"
    
    rs.Open sConsulta2, mvarConexion.ADOCon, adOpenStatic, adLockReadOnly
          
    If rs.eof Then
        rs.Close
        Set rs = Nothing
    
        Set mCol = Nothing
        Set mCol = New Collection
        Exit Sub
          
    Else
        Set mCol = Nothing
        Set mCol = New Collection
        
        Set fldId = rs.Fields("ID")
        Set fldUni = rs.Fields("UNI")
        Set fldCodProve = rs.Fields("PROVE")
        Set fldPrec = rs.Fields("PREC")
        Set fldPorcen = rs.Fields("PORCEN")
        Set fldFecini = rs.Fields("FECINI")
        
        Me.Add oArticulo, fldId.Value, "", fldUni.Value, fldPrec.Value, fldCodProve.Value, fldPorcen.Value, fldFecini.Value
    
        rs.Close
        Set rs = Nothing
            
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CAdjsDeArt", "DevolverProveUltimaAdjudicacion", ERR, Erl)
        Exit Sub
    End If
End Sub



''' <summary>DEvuelve el precio de la ultima Adjudicacion de un articulo dentro de un escalado</summary>
''' <param "oArticulo">El �rticulo </param>
''' <remarks>Llamada desde: frmProce.CargarProvActual </remarks>
''' <remarks>Tiempo m�ximo: < 1 sec </remarks>

Public Sub DevolverProveUltimaAdjudicacionEsc(ByVal sArticulo As String, Optional ByVal sUni As String, Optional oParamEscalado As CEscalado)
Dim rs As New adodb.Recordset
Dim fldId As adodb.Field
Dim fldUni As adodb.Field
Dim fldPrec As adodb.Field
Dim fldCodProve As adodb.Field
Dim fldPorcen As adodb.Field
Dim oArticulo As CArticulo
Dim sConsulta2 As String


'********* Precondicion **************************************
If mvarConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CAdjsDeArt.CargarTodosLosAdjsDeArt", "No se ha establecido la conexion"
    Exit Sub
End If
'*************************************************************
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
Set oArticulo = New CArticulo
oArticulo.Cod = sArticulo


If IsMissing(oParamEscalado) Then
    'Para procesos sin escalado
    sConsulta2 = " SELECT MAX(ART4_ADJ.FECINI) FECINI,MAX(ART4_ADJ.ID) AS ID,ART4_ADJ.PORCEN,ART4_ADJ.CODPROVE PROVE,ART4_ADJ.PREC,ART4_ADJ.UNI,0 AS TIPO"
    sConsulta2 = sConsulta2 & " FROM ART4_ADJ WITH (NOLOCK)"
    sConsulta2 = sConsulta2 & " WHERE ART4_ADJ.ART=" & StrToSQLNULL(sArticulo) & " AND ART4_ADJ.PROCE is null "
    If sUni <> "" Then sConsulta2 = sConsulta2 & " AND ART4_ADJ.UNI=" & StrToSQLNULL(sUni)
    sConsulta2 = sConsulta2 & " GROUP BY ART4_ADJ.PORCEN,ART4_ADJ.CODPROVE ,ART4_ADJ.PREC,ART4_ADJ.UNI"
    sConsulta2 = sConsulta2 & " UNION "
    sConsulta2 = sConsulta2 & "  SELECT MAX(PROCE.FECULTREU) FECINI,MAX(ART4_ADJ.ID) AS ID,ART4_ADJ.PORCEN,ART4_ADJ.CODPROVE PROVE,ART4_ADJ.PREC,ART4_ADJ.UNI,1 AS TIPO "
    sConsulta2 = sConsulta2 & " FROM ART4_ADJ WITH (NOLOCK)"
    sConsulta2 = sConsulta2 & " INNER JOIN PROCE WITH (NOLOCK) ON ART4_ADJ.ANYO=PROCE.ANYO AND ART4_ADJ.GMN1=PROCE.GMN1 AND ART4_ADJ.PROCE=PROCE.COD"
    sConsulta2 = sConsulta2 & " WHERE ART4_ADJ.ART=" & StrToSQLNULL(sArticulo)
    If sUni <> "" Then sConsulta2 = sConsulta2 & " AND ART4_ADJ.UNI=" & StrToSQLNULL(sUni)
    sConsulta2 = sConsulta2 & " GROUP BY ART4_ADJ.PORCEN,ART4_ADJ.CODPROVE ,ART4_ADJ.PREC,ART4_ADJ.UNI"
    sConsulta2 = sConsulta2 & " ORDER BY  FECINI DESC,TIPO DESC,ART4_ADJ.PORCEN DESC,ID DESC"

ElseIf IsNumeric(oParamEscalado.Final) Then
    'Escalado con rangos
    'Hay que encontrar un rango igual al del escalado o si no una cantidad incluida dentro del escalado
    sConsulta2 = " SELECT MAX(ART4_ADJ.FECINI) FECINI,MAX(ART4_ADJ.ID) AS ID,ART4_ADJ.PORCEN,ART4_ADJ.CODPROVE PROVE,ART4_ADJ.PREC,ART4_ADJ.UNI,0 AS TIPO "
    sConsulta2 = sConsulta2 & ",(CASE WHEN(CANT_FIN IS NULL) THEN 0 ELSE 1 END) AS RANGO"
    sConsulta2 = sConsulta2 & " FROM ART4_ADJ WITH (NOLOCK)"
    sConsulta2 = sConsulta2 & " WHERE ART4_ADJ.ART=" & StrToSQLNULL(sArticulo) & " AND ART4_ADJ.PROCE is null "
    If sUni <> "" Then sConsulta2 = sConsulta2 & " AND ART4_ADJ.UNI=" & StrToSQLNULL(sUni)
    sConsulta2 = sConsulta2 & " AND ((CANT_FIN IS NOT NULL AND ART4_ADJ.CANT = " & oParamEscalado.inicial & " AND ART4_ADJ.CANT_FIN = " & oParamEscalado.Final & ") OR (ART4_ADJ.CANT_FIN IS NULL AND ART4_ADJ.CANT>=" & oParamEscalado.inicial & " AND ART4_ADJ.CANT<=" & oParamEscalado.Final & "))"
    sConsulta2 = sConsulta2 & " GROUP BY ART4_ADJ.PORCEN,ART4_ADJ.CODPROVE ,ART4_ADJ.PREC,ART4_ADJ.UNI,ART4_ADJ.CANT_FIN"
    sConsulta2 = sConsulta2 & " UNION "
    sConsulta2 = sConsulta2 & "  SELECT MAX(PROCE.FECULTREU) FECINI,MAX(ART4_ADJ.ID) AS ID,ART4_ADJ.PORCEN,ART4_ADJ.CODPROVE PROVE,ART4_ADJ.PREC,ART4_ADJ.UNI,1 AS TIPO "
    sConsulta2 = sConsulta2 & ",(CASE WHEN(CANT_FIN IS NULL) THEN 0 ELSE 1 END) AS RANGO"
    sConsulta2 = sConsulta2 & " FROM ART4_ADJ WITH (NOLOCK)"
    sConsulta2 = sConsulta2 & " INNER JOIN PROCE WITH (NOLOCK) ON ART4_ADJ.ANYO=PROCE.ANYO AND ART4_ADJ.GMN1=PROCE.GMN1 AND ART4_ADJ.PROCE=PROCE.COD"
    sConsulta2 = sConsulta2 & " WHERE ART4_ADJ.ART=" & StrToSQLNULL(sArticulo)
    If sUni <> "" Then sConsulta2 = sConsulta2 & " AND ART4_ADJ.UNI=" & StrToSQLNULL(sUni)
    sConsulta2 = sConsulta2 & " AND ((CANT_FIN IS NOT NULL AND ART4_ADJ.CANT = " & oParamEscalado.inicial & " AND ART4_ADJ.CANT_FIN = " & oParamEscalado.Final & ") OR (ART4_ADJ.CANT_FIN IS NULL AND ART4_ADJ.CANT>=" & oParamEscalado.inicial & " AND ART4_ADJ.CANT<=" & oParamEscalado.Final & "))"
    sConsulta2 = sConsulta2 & " GROUP BY ART4_ADJ.PORCEN,ART4_ADJ.CODPROVE ,ART4_ADJ.PREC,ART4_ADJ.UNI,ART4_ADJ.CANT_FIN"
    sConsulta2 = sConsulta2 & " ORDER BY RANGO DESC, FECINI DESC,TIPO DESC,ART4_ADJ.PORCEN DESC,ID DESC"

Else
    'Escalado con cantidades directas
    'Hay que encontrar una cantidad igual a la del escalado, o si no un rango que lo incluya
    sConsulta2 = " SELECT MAX(ART4_ADJ.FECINI) FECINI,MAX(ART4_ADJ.ID) AS ID,ART4_ADJ.PORCEN,ART4_ADJ.CODPROVE PROVE,ART4_ADJ.PREC,ART4_ADJ.UNI,0 AS TIPO "
    sConsulta2 = sConsulta2 & ",(CASE WHEN(CANT_FIN IS NULL) THEN 0 ELSE 1 END) AS RANGO"
    sConsulta2 = sConsulta2 & " FROM ART4_ADJ WITH (NOLOCK)"
    sConsulta2 = sConsulta2 & " WHERE ART4_ADJ.ART=" & StrToSQLNULL(sArticulo) & " AND ART4_ADJ.PROCE is null "
    If sUni <> "" Then sConsulta2 = sConsulta2 & " AND ART4_ADJ.UNI=" & StrToSQLNULL(sUni)
    sConsulta2 = sConsulta2 & " AND ((ART4_ADJ.CANT_FIN IS NULL AND ART4_ADJ.CANT=" & oParamEscalado.inicial & ") OR (ART4_ADJ.CANT_FIN IS NOT NULL AND ART4_ADJ.CANT<=" & oParamEscalado.inicial & " AND ART4_ADJ.CANT_FIN>=" & oParamEscalado.inicial & " ))"
    sConsulta2 = sConsulta2 & " GROUP BY ART4_ADJ.PORCEN,ART4_ADJ.CODPROVE ,ART4_ADJ.PREC,ART4_ADJ.UNI,ART4_ADJ.CANT_FIN"
    sConsulta2 = sConsulta2 & " UNION "
    sConsulta2 = sConsulta2 & "  SELECT MAX(PROCE.FECULTREU) FECINI,MAX(ART4_ADJ.ID) AS ID,ART4_ADJ.PORCEN,ART4_ADJ.CODPROVE PROVE,ART4_ADJ.PREC,ART4_ADJ.UNI,1 AS TIPO "
    sConsulta2 = sConsulta2 & ",(CASE WHEN(CANT_FIN IS NULL) THEN 0 ELSE 1 END) AS RANGO"
    sConsulta2 = sConsulta2 & " FROM ART4_ADJ WITH (NOLOCK)"
    sConsulta2 = sConsulta2 & " INNER JOIN PROCE WITH (NOLOCK) ON ART4_ADJ.ANYO=PROCE.ANYO AND ART4_ADJ.GMN1=PROCE.GMN1 AND ART4_ADJ.PROCE=PROCE.COD"
    sConsulta2 = sConsulta2 & " WHERE ART4_ADJ.ART=" & StrToSQLNULL(sArticulo)
    If sUni <> "" Then sConsulta2 = sConsulta2 & " AND ART4_ADJ.UNI=" & StrToSQLNULL(sUni)
    sConsulta2 = sConsulta2 & " AND ((ART4_ADJ.CANT_FIN IS NULL AND ART4_ADJ.CANT=" & oParamEscalado.inicial & ") OR (ART4_ADJ.CANT_FIN IS NOT NULL AND ART4_ADJ.CANT<=" & oParamEscalado.inicial & " AND ART4_ADJ.CANT_FIN>=" & oParamEscalado.inicial & " ))"
    sConsulta2 = sConsulta2 & " GROUP BY ART4_ADJ.PORCEN,ART4_ADJ.CODPROVE ,ART4_ADJ.PREC,ART4_ADJ.UNI,ART4_ADJ.CANT_FIN"
    sConsulta2 = sConsulta2 & " ORDER BY RANGO ASC, FECINI DESC,TIPO DESC,ART4_ADJ.PORCEN DESC,ID DESC"
    
End If


rs.Open sConsulta2, mvarConexion.ADOCon, adOpenStatic, adLockReadOnly
      
If rs.eof Then
    rs.Close
    Set rs = Nothing

    Set mCol = Nothing
    Set mCol = New Collection
    Exit Sub
      
Else
    Set mCol = Nothing
    Set mCol = New Collection
    
    Set fldId = rs.Fields("ID")
    Set fldUni = rs.Fields("UNI")
    Set fldCodProve = rs.Fields("PROVE")
    Set fldPrec = rs.Fields("PREC")
    Set fldPorcen = rs.Fields("PORCEN")
    
    Me.Add oArticulo, fldId.Value, "", fldUni.Value, fldPrec.Value, fldCodProve.Value, fldPorcen.Value

    rs.Close
    Set rs = Nothing
        
End If

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CAdjsDeArt", "DevolverProveUltimaAdjudicacionEsc", ERR, Erl)
        Exit Sub
    End If
        
End Sub

Public Sub DevolverAdjudicacionesLineaInstancia(oArticulo As CArticulo, ByVal lInstancia As Long, ByVal lGrupo As Long, ByVal lLinea As Integer, Optional ByVal bPreadj As Boolean)
    Dim adoComm As adodb.Command
    Dim adoParam As adodb.Parameter
    Dim AdoRes As adodb.Recordset
    Dim dFecha As Date
    Dim i As Integer
   
    '********* Precondicion **************************************
    If mvarConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CAdjsDeArt.DevolverAdjudicacionesLineaInstancia", "No se ha establecido la conexion"
        Exit Sub
    End If
    '*************************************************************
    If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
       
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    i = 0
    Set adoComm = New adodb.Command
    Set adoComm.ActiveConnection = mvarConexion.ADOCon
    Set adoParam = adoComm.CreateParameter("INSTANCIA", adInteger, adParamInput, , lInstancia)
    adoComm.Parameters.Append adoParam
    Set adoParam = adoComm.CreateParameter("CAMPO_SOLICIT", adInteger, adParamInput, , 0) 'Solo se usa desde PM porque no tiene COPIA_CAMPO_DEF informado
    adoComm.Parameters.Append adoParam
    Set adoParam = adoComm.CreateParameter("LINEA_SOLICIT", adInteger, adParamInput, , lLinea)
    adoComm.Parameters.Append adoParam
    Set adoParam = adoComm.CreateParameter("PREADJ", adInteger, adParamInput, , IIf(bPreadj, 1, 0))
    adoComm.Parameters.Append adoParam
    Set adoParam = adoComm.CreateParameter("COPIA_CAMPO_DEF", adInteger, adParamInput, , lGrupo)
    adoComm.Parameters.Append adoParam
    
    adoComm.CommandText = "FSPM_DEVOLVER_ADJUDICACIONES_LINEA_INSTANCIA"
    adoComm.CommandType = adCmdStoredProc
    Set AdoRes = adoComm.Execute

    Set mCol = New Collection
    While Not AdoRes.eof
        If IsDate(AdoRes("F_UTC0").Value) Then
            dFecha = ConvertirUTCaTZ(AdoRes("F_UTC0").Value, GMT1_TZ)
        End If
        Me.Add oArticulo, i, "", "", NullToDbl0(AdoRes("PREC_VALIDO").Value), AdoRes("PROVE").Value, AdoRes("PORCEN").Value, AdoRes("FECINI").Value, _
                AdoRes("FECFIN").Value, AdoRes("DEN").Value, , NullToDbl0(AdoRes("CANT").Value), AdoRes("ANYO").Value, AdoRes("GMN1_PROCE").Value, _
                AdoRes("PROCE").Value, AdoRes("MON").Value, NullToStr(AdoRes("NOMUSU").Value), dFecha, _
                NullToDbl0(AdoRes("ULTIMO").Value)
        i = i + 1
        AdoRes.MoveNext
    Wend
    AdoRes.Close
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CAdjsDeArt", "DevolverAdjudicacionesLineaInstancia", ERR, Erl)
        Exit Sub
    End If
        
End Sub



