VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cPyme"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Implements IBaseDatos

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

Private m_lId As Long
Private m_sCod As String
Private m_sDen As String


Private m_oConexion As CConexion
Private m_oConexionServer As CConexionDeUseServer



Public Property Get ID() As Long
    ID = m_lId
End Property

Public Property Let ID(ByVal sID As Long)
    m_lId = sID
End Property

Public Property Get Cod() As String
    Cod = m_sCod
End Property

Public Property Let Cod(ByVal sCod As String)
    m_sCod = sCod
End Property

Public Property Get Den() As String
    Den = m_sDen
End Property
Public Property Let Den(ByVal sDen As String)
    m_sDen = sDen
End Property

Friend Property Set ConexionServer(ByVal con As CConexionDeUseServer)
    Set m_oConexionServer = con
End Property

Friend Property Get ConexionServer() As CConexionDeUseServer
    Set ConexionServer = m_oConexionServer
End Property
Friend Property Set Conexion(ByVal con As CConexion)
    Set m_oConexion = con
End Property

Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property





