VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CCompradores"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Collection" ,"CComprador"
Attribute VB_Ext_KEY = "Member0" ,"CComprador"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

'local variable to hold collection
Private mCol As Collection
Private mvarConexion As CConexion
Private mvarEOF As Boolean

Friend Property Set Conexion(ByVal con As CConexion)
Set mvarConexion = con
End Property
''' <summary>
''' A�ade elementos a la coleccion
''' </summary>
''' <param name="CodEqp">Codigo de equipo del comprador</param>
''' <param name="DenEqp">Codigo de equipo del comprador</param>
''' <param name="Cod">Codigo del comprador</param>
''' <param name="Nombre">Nombre del comprador</param>
''' <param name="Apel">Apellido del comprador</param>
''' <param name="Tfno">Tfno del comprador</param>
''' <param name="Fax">fax del comprador</param>
''' <param name="varIndice">Indice de la coleccion</param>
''' <param name="Cargo">Cargo del comprador en la organizacion</param>
''' <returns></returns>
''' <remarks>Llamada desde:CargarTodosLosCompradores,CargarTodosLosCompradores,CProceso.IAsignaciones_DevolverCompradoresAsignados </remarks>
''' <remarks>Tiempo m�ximo: 0 seg </remarks>
Public Function Add(ByVal CodEqp As String, ByVal DenEqp As String, ByVal Cod As String, ByVal Nombre As String, ByVal Apel As String, ByVal Tfno As String, ByVal Fax As String, ByVal Mail As String, Optional ByVal varIndice As Variant, Optional ByVal Cargo As String) As CComprador
    'create a new object
    Dim oCom As CComprador
    Dim sCod As String
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oCom = New CComprador

    oCom.CodEqp = CodEqp
    oCom.DenEqp = DenEqp
    oCom.Cod = Cod
    oCom.Nombre = Nombre
    oCom.Apel = Apel
    oCom.Tfno = Tfno
    oCom.Fax = Fax
    oCom.Mail = Mail
    If Not IsMissing(Cargo) Then
        If Not IsNull(Cargo) Then
            oCom.Cargo = Cargo
        Else
            oCom.Cargo = ""
        End If
    End If
    Set oCom.Conexion = mvarConexion
        
    If Not IsMissing(varIndice) And Not IsNull(varIndice) Then
        mCol.Add oCom, CStr(varIndice)
    Else
        sCod = CodEqp & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodEQP - Len(CodEqp))
        mCol.Add oCom, sCod & Cod
    End If


    'return the object created
    Set Add = oCom
    Set oCom = Nothing

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CCompradores", "Add", ERR, Erl)
        Exit Function
    End If

End Function

Public Property Get Item(vntIndexKey As Variant) As CComprador
Attribute Item.VB_UserMemId = 0
    On Error GoTo NoSeEncuentra
     Set Item = mCol(vntIndexKey)
     Exit Property
NoSeEncuentra:
    Set Item = Nothing
    
End Property

Public Property Get eof() As Boolean
    eof = mvarEOF
End Property
Friend Property Let eof(ByVal eof As Boolean)
    mvarEOF = eof
End Property

Public Property Get Count() As Long
     Count = mCol.Count
End Property


Public Sub Remove(vntIndexKey As Variant)

On Error GoTo NoSeEncuentra:
    mCol.Remove vntIndexKey
    Exit Sub
NoSeEncuentra:

End Sub


Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
     Set NewEnum = mCol.[_NewEnum]
End Property


Private Sub Class_Initialize()
   

    'creates the collection when this class is created
    Set mCol = New Collection
End Sub


Private Sub Class_Terminate()
    'destroys collection when this class is terminated
    Set mCol = Nothing
    Set mvarConexion = Nothing
End Sub
Public Sub CargarTodosLosCompradores(Optional ByVal CarIniCod As String, Optional ByVal CarIniNombre As String, Optional ByVal CarIniApel As String, Optional ByVal CoincidenciaTotal As Boolean = False, Optional ByVal OrdenadosPorNombre As Boolean = False, Optional ByVal OrdenadosPorApel As Boolean = False, Optional ByVal OrdenadosPorCodEqp As Boolean = False, Optional ByVal OrdenadosPorDenEqp As Boolean = False, Optional ByVal UsarIndice As Boolean = False, Optional ByVal NoBajaLog As Boolean)

Dim lIndice As Long
Dim rs As New adodb.Recordset
Dim fldEqp As adodb.Field
Dim fldCod As adodb.Field
Dim fldApe As adodb.Field
Dim fldNom As adodb.Field
Dim fldFax As adodb.Field
Dim fldEmail As adodb.Field
Dim fldTfno As adodb.Field
Dim fldDenEqp As adodb.Field
Dim fldCargo As adodb.Field
Dim fldTfno2 As adodb.Field
Dim sConsulta As String
Dim oCom As CComprador
Dim sCad As String

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If CarIniNombre = "" Then
    sCad = "0"
Else
    sCad = "1"
End If
If CarIniApel = "" Then
    sCad = sCad & "0"
Else
    sCad = sCad & "1"
End If
If CarIniCod = "" Then
    sCad = sCad & "0"
Else
    sCad = sCad & "1"
End If

sConsulta = "SELECT COM.EQP,COM.COD,COM.NOM,COM.APE,COM.FAX,COM.EMAIL,COM.TFNO,EQP.DEN AS DENEQP,"
sConsulta = sConsulta & "COM.CAR,COM.TFNO2 "

If NoBajaLog = True Then
    sConsulta = sConsulta & " FROM EQP WITH (NOLOCK),COM WITH (NOLOCK),PER WITH (NOLOCK) WHERE"
Else
    sConsulta = sConsulta & " FROM EQP WITH (NOLOCK),COM WITH (NOLOCK) WHERE"
End If

Select Case sCad

Case "000"
    sConsulta = sConsulta & " EQP.COD=COM.EQP"
Case "001"
        If CoincidenciaTotal Then
            sConsulta = sConsulta & " COM.COD ='" & DblQuote(CarIniCod) & "'"
        Else
            sConsulta = sConsulta & " COM.COD LIKE '" & DblQuote(CarIniCod) & "%'"
        End If
        sConsulta = sConsulta & " AND EQP.COD=COM.EQP"
Case "010"
        If CoincidenciaTotal Then
            sConsulta = sConsulta & " COM.APE ='" & DblQuote(CarIniApel) & "'"
        Else
            sConsulta = sConsulta & " COM.APE LIKE '" & DblQuote(CarIniApel) & "%'"
        End If
        sConsulta = sConsulta & " AND EQP.COD=COM.EQP"
Case "011"
        If CoincidenciaTotal Then
            sConsulta = sConsulta & " COM.COD ='" & DblQuote(CarIniCod) & "'"
            sConsulta = sConsulta & " AND COM.APE ='" & DblQuote(CarIniApel) & "'"
        Else
            sConsulta = sConsulta & " COM.COD LIKE '" & DblQuote(CarIniCod) & "%'"
            sConsulta = sConsulta & " AND COM.APE LIKE '" & DblQuote(CarIniApel) & "%'"
        End If
        sConsulta = sConsulta & " AND EQP.COD=COM.EQP"
Case "100"
        If CoincidenciaTotal Then
            sConsulta = sConsulta & " COM.NOM ='" & DblQuote(CarIniNombre) & "'"
        Else
            sConsulta = sConsulta & " COM.NOM LIKE '" & DblQuote(CarIniNombre) & "%'"
        End If
        sConsulta = sConsulta & " AND EQP.COD=COM.EQP"
Case "101"
         If CoincidenciaTotal Then
            sConsulta = sConsulta & " COM.NOM ='" & DblQuote(CarIniNombre) & "'"
            sConsulta = sConsulta & " AND COM.COD ='" & DblQuote(CarIniCod) & "'"
        Else
            sConsulta = sConsulta & " COM.NOM LIKE '" & DblQuote(CarIniNombre) & "%'"
            sConsulta = sConsulta & " AND COM.COD LIKE '" & DblQuote(CarIniCod) & "%'"
        End If
        sConsulta = sConsulta & " AND EQP.COD=COM.EQP"
Case "110"
        If CoincidenciaTotal Then
            sConsulta = sConsulta & " COM.NOM ='" & DblQuote(CarIniNombre) & "'"
            sConsulta = sConsulta & " AND COM.APE ='" & DblQuote(CarIniApel) & "'"
        Else
            sConsulta = sConsulta & " COM.NOM LIKE '" & DblQuote(CarIniNombre) & "%'"
            sConsulta = sConsulta & " AND COM.APE LIKE '" & DblQuote(CarIniApel) & "%'"
        End If
        sConsulta = sConsulta & " AND EQP.COD=COM.EQP"
Case "111"
        If CoincidenciaTotal Then
             sConsulta = sConsulta & " COM.COD ='" & DblQuote(CarIniCod) & "'"
            sConsulta = sConsulta & " AND COM.NOM ='" & DblQuote(CarIniNombre) & "'"
            sConsulta = sConsulta & " AND COM.APE ='" & DblQuote(CarIniApel) & "'"
        Else
            sConsulta = sConsulta & " COM.COD LIKE '" & DblQuote(CarIniCod) & "%'"
            sConsulta = sConsulta & " AND COM.NOM LIKE '" & DblQuote(CarIniNombre) & "%'"
            sConsulta = sConsulta & " AND COM.APE LIKE '" & DblQuote(CarIniApel) & "%'"
        End If
        sConsulta = sConsulta & " AND EQP.COD=COM.EQP"
End Select

If NoBajaLog = True Then
    sConsulta = sConsulta & " AND COM.COD=PER.COD AND COM.EQP=PER.EQP AND PER.BAJALOG=0"
End If
  
If OrdenadosPorNombre Then
    sConsulta = sConsulta & " ORDER BY COM.NOM,COM.APE,EQP.COD"
Else
    If OrdenadosPorApel Then
        sConsulta = sConsulta & " ORDER BY COM.APE,COM.NOM,EQP.COD"
    Else
        If OrdenadosPorCodEqp Then
            sConsulta = sConsulta & " ORDER BY EQP.COD,EQP.DEN,COM.COD,COM.NOM,COM.APE"
        Else
            If OrdenadosPorDenEqp Then
                sConsulta = sConsulta & " ORDER BY EQP.DEN,EQP.COD,COM.COD,COM.NOM,COM.APE"
            Else
                sConsulta = sConsulta & " ORDER BY COM.COD,COM.NOM,COM.APE,EQP.COD"
            End If
        End If
    End If
End If

rs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
      
If rs.eof Then
        
    rs.Close
    Set rs = Nothing
    Set mCol = Nothing
    Set mCol = New Collection
    Exit Sub
      
Else
    
    Set fldEqp = rs.Fields("EQP")
    Set fldCod = rs.Fields("COD")
    Set fldApe = rs.Fields("APE")
    Set fldNom = rs.Fields("NOM")
    Set fldFax = rs.Fields("FAX")
    Set fldEmail = rs.Fields("EMAIL")
    Set fldTfno = rs.Fields("TFNO")
    Set fldDenEqp = rs.Fields("DENEQP")
    Set fldCargo = rs.Fields("CAR")
    Set fldTfno2 = rs.Fields("TFNO2")
    
    Set mCol = Nothing
    Set mCol = New Collection
    
    If UsarIndice Then
        
        lIndice = 0
        
        While Not rs.eof
            
            Set oCom = Nothing
            Set oCom = New CComprador
            If IsNull(fldEqp.Value) Then
                oCom.CodEqp = ""
            Else
                oCom.CodEqp = fldEqp.Value
            End If
            If IsNull(fldDenEqp.Value) Then
                oCom.DenEqp = ""
            Else
                oCom.DenEqp = fldDenEqp.Value
            End If
            If IsNull(fldCod.Value) Then
                oCom.Cod = ""
            Else
                oCom.Cod = fldCod.Value
            End If
            If IsNull(fldApe.Value) Then
                oCom.Apel = ""
            Else
                oCom.Apel = fldApe.Value
            End If
            If IsNull(fldNom.Value) Then
                oCom.Nombre = ""
            Else
                oCom.Nombre = fldNom.Value
            End If
            If IsNull(fldEmail.Value) Then
                oCom.Mail = ""
            Else
                oCom.Mail = fldEmail.Value
            End If
            If IsNull(fldTfno.Value) Then
                oCom.Tfno = ""
            Else
                oCom.Tfno = fldTfno.Value
            End If
            If IsNull(fldFax.Value) Then
                oCom.Fax = ""
            Else
                oCom.Fax = fldFax.Value
            End If
            If IsNull(fldCargo.Value) Then
                oCom.Cargo = ""
            Else
                oCom.Cargo = fldCargo.Value
            End If
            If IsNull(fldTfno2.Value) Then
                oCom.Tfno2 = ""
            Else
                oCom.Tfno2 = fldTfno2.Value
            End If
            
            Set oCom.Conexion = mvarConexion
            
            mCol.Add oCom, CStr(lIndice)
            rs.MoveNext
            lIndice = lIndice + 1
        Wend
    Else
        While Not rs.eof
            
            Set oCom = Nothing
            Set oCom = New CComprador
           If IsNull(fldEqp.Value) Then
                oCom.CodEqp = ""
            Else
                oCom.CodEqp = fldEqp.Value
            End If
            If IsNull(fldDenEqp.Value) Then
                oCom.DenEqp = ""
            Else
                oCom.DenEqp = fldDenEqp.Value
            End If
            If IsNull(fldCod.Value) Then
                oCom.Cod = ""
            Else
                oCom.Cod = fldCod.Value
            End If
            If IsNull(fldApe.Value) Then
                oCom.Apel = ""
            Else
                oCom.Apel = fldApe.Value
            End If
            If IsNull(fldNom.Value) Then
                oCom.Nombre = ""
            Else
                oCom.Nombre = fldNom.Value
            End If
            If IsNull(fldEmail.Value) Then
                oCom.Mail = ""
            Else
                oCom.Mail = fldEmail.Value
            End If
            If IsNull(fldTfno.Value) Then
                oCom.Tfno = ""
            Else
                oCom.Tfno = fldTfno.Value
            End If
            If IsNull(fldFax.Value) Then
                oCom.Fax = ""
            Else
                oCom.Fax = fldFax.Value
            End If
            If IsNull(fldCargo.Value) Then
                oCom.Cargo = ""
            Else
                oCom.Cargo = fldCargo.Value
            End If
            If IsNull(fldTfno2.Value) Then
                oCom.Tfno2 = ""
            Else
                oCom.Tfno2 = fldTfno2.Value
            End If
            
            Set oCom.Conexion = mvarConexion
            
            mCol.Add oCom, fldEqp.Value & fldCod.Value
            rs.MoveNext
        Wend
     End If
        
    Set fldEqp = Nothing
    Set fldCod = Nothing
    Set fldApe = Nothing
    Set fldNom = Nothing
    Set fldFax = Nothing
    Set fldEmail = Nothing
    Set fldTfno = Nothing
    Set fldDenEqp = Nothing
    Set fldCargo = Nothing
    Set fldTfno2 = Nothing
    
    rs.Close
    Set rs = Nothing
    Exit Sub
      
End If

Set oCom = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CCompradores", "CargarTodosLosCompradores", ERR, Erl)
        Exit Sub
    End If
End Sub

''' <summary>Carga los compradores en funci�n de unos criterios</summary>
''' <param name="NumMaximo">N� max. de registros a cargar</param>
''' <param name="CarIniCod">Caracteres iniciales de c�digo</param>
''' <param name="CarIniNombre">Caracteres iniciales de nombre</param>
''' <param name="OrdenadosPorNombre">Ordenados por nombre</param>
''' <param name="UsarIndice">Usar �ndice</param>
''' <remarks>Llamada desde frmOFEAvisoAnya</remarks>
''' <revision>LTG 03/01/2012</revision>

Public Sub CargarTodosLosCompradoresDesde(ByVal NumMaximo As Integer, Optional ByVal CarIniCod As String, Optional ByVal CarIniNombre As String, _
        Optional ByVal OrdenadosPorNombre As Boolean = False, Optional ByVal UsarIndice As Boolean = False)
    Dim lIndice As Long
    Dim rs As New adodb.Recordset
    Dim fldEqp As adodb.Field
    Dim fldCod As adodb.Field
    Dim fldNom As adodb.Field
    Dim fldApe As adodb.Field
    Dim fldEqpDen As adodb.Field
    Dim sConsulta As String
    Dim oCom As CComprador
        
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sConsulta = "SELECT TOP " & NumMaximo & " COM.EQP,COM.COD,COM.NOM,COM.APE,EQP.DEN AS EQPDEN"
    sConsulta = sConsulta & " FROM COM WITH (NOLOCK),EQP WITH (NOLOCK) WHERE"
    sConsulta = sConsulta & " COM.EQP=EQP.COD"
    
    
    If CarIniCod = "" And CarIniNombre = "" Then
       
    Else
        
        If Not (CarIniCod = "") And Not (CarIniNombre = "") Then
            
                sConsulta = sConsulta & " AND COM.COD >='" & DblQuote(CarIniCod) & "'"
                sConsulta = sConsulta & " AND COM.NOM >='" & DblQuote(CarIniNombre) & "'"
                        
        Else
            
            If Not (CarIniCod = "") Then
                    sConsulta = sConsulta & "AND COM.COD >='" & DblQuote(CarIniCod) & "'"
            Else
                    sConsulta = sConsulta & " COM.NOM >='" & DblQuote(CarIniNombre) & "'"
            End If
        
        End If
               
    End If
    
    If OrdenadosPorNombre Then
        sConsulta = sConsulta & " ORDER BY EQP.COD,COM.NOM,COM.COD"
    Else
        sConsulta = sConsulta & " ORDER BY EQP.COD,COM.COD,COM.NOM"
    End If
          
    rs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
          
    If rs.eof Then
            
        rs.Close
        Set rs = Nothing
        Set mCol = Nothing
        Set mCol = New Collection
        Exit Sub
          
    Else
        
        Set fldEqp = rs.Fields(0)
        Set fldCod = rs.Fields(1)
        Set fldNom = rs.Fields(2)
        Set fldApe = rs.Fields(3)
        Set fldEqpDen = rs.Fields(4)
    
        Set mCol = Nothing
        Set mCol = New Collection
        
            
        If UsarIndice Then
            
            lIndice = 0
            
                    
            While Not rs.eof
                ' Dejamos en manos del programador cuando asignar la conexion al obejto usuario
                Me.Add fldEqp.Value, fldEqpDen.Value, fldCod.Value, fldNom.Value, fldApe.Value, "", "", "", lIndice
                rs.MoveNext
                lIndice = lIndice + 1
                
            Wend
            
            If Not rs.eof Then
                mvarEOF = False
            Else
                mvarEOF = True
            End If
            
            
                
        Else
                 
            While Not rs.eof
                ' Dejamos en manos del programador cuando asignar la conexion al obejto usuario
                Me.Add fldEqp.Value, fldEqpDen.Value, fldCod.Value, fldNom.Value, fldApe.Value, "", "", ""
                rs.MoveNext
            Wend
            
            If Not rs.eof Then
                mvarEOF = False
            Else
                mvarEOF = True
            End If
            
        End If
        
        Set fldEqp = Nothing
        Set fldCod = Nothing
        Set fldNom = Nothing
        Set fldApe = Nothing
        Set fldEqpDen = Nothing
    
        rs.Close
        Set rs = Nothing
        Exit Sub
         
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CCompradores", "CargarTodosLosCompradoresDesde", ERR, Erl)
        Exit Sub
    End If
End Sub

''' <summary>
''' Carga la colecci�n interna de objetos CComprador con los elementos que satisfacen las condiciones establecidas
''' en los par�metros.
''' </summary>
''' <param name="sCod">C�digo del comprador</param>
''' <param name="sNom">Nombre del comprador</param>
''' <param name="sApe">Apellidos del comprador</param>
''' <param name="sUON1">C�digo de la UON1</param>
''' <param name="sUON2">C�digo de la UON2</param>
''' <param name="sUON3">C�digo de la UON3</param>
''' <param name="sDep">C�digo del departamento</param>
''' <param name="CoincidenciaTotal">Indica si los filtros por c�digo, nombre y apellidos de comprador,
''' se van a realizar con un like o con un operador de igualdad</param>
''' <param name="OrdenarPor">Nombre del campo por el que se va a realizar el ORDER BY (COD,NOM,APE, etc)</param>
''' <param name="UsarIndice">Indica si la colecci�m interna se va a indexar</param>
''' <param name="iPYME">C�digo de la empresa PYME a la que pertence el usuario actual</param>
''' <param name="bRPerfUON">Restringir a las UON del perfil del usuario</param>
''' <param name="lIdPerf">Id perfil</param>
''' <remarks>Tiempo m�ximo: 0 seg </remarks>
''' <revision>LTG 03/01/2012</revision>

Public Sub CargarTodosLosCompradoresPorUON(Optional ByVal sCod As String, Optional ByVal sNom As String, Optional ByVal sApe As String, _
        Optional ByVal sUON1 As String, Optional ByVal sUON2 As String, Optional ByVal sUON3 As String, Optional ByVal sDep As String, _
        Optional ByVal CoincidenciaTotal As Boolean, Optional ByVal OrdenarPor As String, Optional ByVal UsarIndice As Boolean, Optional ByVal iPyme As Integer, _
        Optional ByVal bRPerfUON As Boolean, Optional ByVal lIdPerf As Long)
    Dim sConsulta As String
    Dim sWhere As String
    Dim rs As New adodb.Recordset
    Dim adoComm As adodb.Command
    Dim adoParam As adodb.Parameter
    Dim fldEqp As adodb.Field
    Dim fldCod As adodb.Field
    Dim fldApe As adodb.Field
    Dim fldNom As adodb.Field
    Dim fldFax As adodb.Field
    Dim fldEmail As adodb.Field
    Dim fldTfno As adodb.Field
    Dim fldDenEqp As adodb.Field
    Dim fldCargo As adodb.Field
    Dim fldTfno2 As adodb.Field
    Dim lIndice As Long
    Dim oCom As CComprador
    Dim sUON As String
    Dim bRUO As Boolean
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set adoComm = New adodb.Command
    
    sConsulta = "SELECT DISTINCT COM.EQP,COM.COD,COM.APE,COM.NOM,COM.FAX,COM.EMAIL,COM.TFNO,"
    sConsulta = sConsulta & "(SELECT DEN FROM EQP WITH (NOLOCK) WHERE COM.EQP=EQP.COD) AS DENEQP,COM.CAR,COM.TFNO2"
    bRUO = (sUON1 <> "" Or sUON2 <> "" Or sUON3 <> "")
    If bRUO Or bRPerfUON Then
        sUON = ConstruirCriterioUONUsuPerfCom(bRUO, bRPerfUON, sUON1, sUON2, sUON3, lIdPerf, True)
        sConsulta = sConsulta & " FROM (" & sUON & ") COM "
    Else
        sConsulta = sConsulta & " FROM COM WITH(NOLOCK)"
    End If
    sConsulta = sConsulta & " INNER JOIN PER WITH(NOLOCK) ON PER.COD = COM.COD AND PER.BAJALOG=0"
    If iPyme <> 0 Then
        sConsulta = sConsulta & " INNER JOIN UON1 WITH(NOLOCK) ON COM.UON1 = UON1.COD AND UON1.PYME= ?"
        Set adoParam = adoComm.CreateParameter(Type:=adInteger, Direction:=adParamInput, Value:=iPyme)
        adoComm.Parameters.Append adoParam
    End If
    
    sWhere = " WHERE 1=1"
    If sUON1 <> "" And sUON2 <> "" And sUON3 <> "" Then
        Set adoParam = adoComm.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(sUON1), Value:=sUON1)
        adoComm.Parameters.Append adoParam
        Set adoParam = adoComm.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(sUON2), Value:=sUON2)
        adoComm.Parameters.Append adoParam
        Set adoParam = adoComm.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(sUON3), Value:=sUON3)
        adoComm.Parameters.Append adoParam
    ElseIf sUON1 <> "" And sUON2 <> "" And sUON3 = "" Then
        Set adoParam = adoComm.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(sUON1), Value:=sUON1)
        adoComm.Parameters.Append adoParam
        Set adoParam = adoComm.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(sUON2), Value:=sUON2)
        adoComm.Parameters.Append adoParam
    ElseIf sUON1 <> "" And sUON2 = "" And sUON3 = "" Then
        Set adoParam = adoComm.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(sUON1), Value:=sUON1)
        adoComm.Parameters.Append adoParam
    End If
    
    If sDep <> "" Then
        If Len(sWhere) > 0 Then
            sWhere = sWhere & " AND PER.DEP=?"
        Else
            sWhere = " WHERE PER.DEP=?"
        End If
        Set adoParam = adoComm.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(sDep), Value:=sDep)
        adoComm.Parameters.Append adoParam
    End If

    If Len(sCod) > 0 Then
        If Len(sWhere) > 0 Then
            If CoincidenciaTotal Then
                sWhere = " AND COM.COD=?"
            Else
                sWhere = " AND COM.COD LIKE '%" & sCod & "%'"
            End If
        Else
            If CoincidenciaTotal Then
                sWhere = " WHERE COM.COD = ?"
            Else
                sWhere = " WHERE COM.COD LIKE '%" & sCod & "%'"
            End If
        End If
        Set adoParam = adoComm.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(sCod), Value:=sCod)
        adoComm.Parameters.Append adoParam
    End If
    
    If Len(sNom) > 0 Then
        If Len(sWhere) > 0 Then
            If CoincidenciaTotal Then
                sWhere = sWhere & " AND COM.NOM=?"
            Else
                sWhere = sWhere & " AND COM.NOM LIKE '%" & sNom & "%'"
            End If
        Else
            If CoincidenciaTotal Then
                sWhere = " WHERE COM.NOM=?"
            Else
                sWhere = " WHERE COM.NOM LIKE '%" & sNom & "%'"
            End If
        End If
        Set adoParam = adoComm.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(sNom), Value:=sNom)
        adoComm.Parameters.Append adoParam
    End If
    
    If Len(sApe) > 0 Then
        If Len(sWhere) > 0 Then
            If CoincidenciaTotal Then
                sWhere = sWhere & " AND COM.APE= ?"
            Else
                sWhere = sWhere & " AND COM.APE LIKE '%" & sApe & "%'"
            End If
        Else
            If CoincidenciaTotal Then
                sWhere = " WHERE COM.APE=?"
            Else
                sWhere = " WHERE COM.APE LIKE '%" & sApe & "%'"
            End If
        End If
        Set adoParam = adoComm.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(sApe), Value:=sApe)
        adoComm.Parameters.Append adoParam
    End If
    
    If Len(OrdenarPor) > 0 Then
        sConsulta = sConsulta & sWhere & " ORDER BY " & OrdenarPor
    Else
        sConsulta = sConsulta & sWhere & " ORDER BY COD"
    End If
    
    adoComm.CommandText = sConsulta
    adoComm.CommandType = adCmdText
    Set adoComm.ActiveConnection = mvarConexion.ADOCon
    adoComm.Prepared = True
    Set rs = adoComm.Execute

    Set mCol = Nothing
    Set mCol = New Collection
        
    If Not rs.eof Then
        Set fldEqp = rs.Fields("EQP")
        Set fldCod = rs.Fields("COD")
        Set fldApe = rs.Fields("APE")
        Set fldNom = rs.Fields("NOM")
        Set fldFax = rs.Fields("FAX")
        Set fldEmail = rs.Fields("EMAIL")
        Set fldTfno = rs.Fields("TFNO")
        Set fldDenEqp = rs.Fields("DENEQP")
        Set fldCargo = rs.Fields("CAR")
        Set fldTfno2 = rs.Fields("TFNO2")
        
        If UsarIndice Then
            lIndice = 0
            
            While Not rs.eof
                Set oCom = Nothing
                Set oCom = New CComprador
                If IsNull(fldEqp.Value) Then
                    oCom.CodEqp = ""
                Else
                    oCom.CodEqp = fldEqp.Value
                End If
                If IsNull(fldDenEqp.Value) Then
                    oCom.DenEqp = ""
                Else
                    oCom.DenEqp = fldDenEqp.Value
                End If
                If IsNull(fldCod.Value) Then
                    oCom.Cod = ""
                Else
                    oCom.Cod = fldCod.Value
                End If
                If IsNull(fldApe.Value) Then
                    oCom.Apel = ""
                Else
                    oCom.Apel = fldApe.Value
                End If
                If IsNull(fldNom.Value) Then
                    oCom.Nombre = ""
                Else
                    oCom.Nombre = fldNom.Value
                End If
                If IsNull(fldEmail.Value) Then
                    oCom.Mail = ""
                Else
                    oCom.Mail = fldEmail.Value
                End If
                If IsNull(fldTfno.Value) Then
                    oCom.Tfno = ""
                Else
                    oCom.Tfno = fldTfno.Value
                End If
                If IsNull(fldFax.Value) Then
                    oCom.Fax = ""
                Else
                    oCom.Fax = fldFax.Value
                End If
                If IsNull(fldCargo.Value) Then
                    oCom.Cargo = ""
                Else
                    oCom.Cargo = fldCargo.Value
                End If
                If IsNull(fldTfno2.Value) Then
                    oCom.Tfno2 = ""
                Else
                    oCom.Tfno2 = fldTfno2.Value
                End If
                
                Set oCom.Conexion = mvarConexion
                
                mCol.Add oCom, CStr(lIndice)
                rs.MoveNext
                lIndice = lIndice + 1
            Wend
        Else
            While Not rs.eof
                
                Set oCom = Nothing
                Set oCom = New CComprador
                If IsNull(fldEqp.Value) Then
                    oCom.CodEqp = ""
                Else
                    oCom.CodEqp = fldEqp.Value
                End If
                If IsNull(fldDenEqp.Value) Then
                    oCom.DenEqp = ""
                Else
                    oCom.DenEqp = fldDenEqp.Value
                End If
                If IsNull(fldCod.Value) Then
                    oCom.Cod = ""
                Else
                    oCom.Cod = fldCod.Value
                End If
                If IsNull(fldApe.Value) Then
                    oCom.Apel = ""
                Else
                    oCom.Apel = fldApe.Value
                End If
                If IsNull(fldNom.Value) Then
                    oCom.Nombre = ""
                Else
                    oCom.Nombre = fldNom.Value
                End If
                If IsNull(fldEmail.Value) Then
                    oCom.Mail = ""
                Else
                    oCom.Mail = fldEmail.Value
                End If
                If IsNull(fldTfno.Value) Then
                    oCom.Tfno = ""
                Else
                    oCom.Tfno = fldTfno.Value
                End If
                If IsNull(fldFax.Value) Then
                    oCom.Fax = ""
                Else
                    oCom.Fax = fldFax.Value
                End If
                If IsNull(fldCargo.Value) Then
                    oCom.Cargo = ""
                Else
                    oCom.Cargo = fldCargo.Value
                End If
                If IsNull(fldTfno2.Value) Then
                    oCom.Tfno2 = ""
                Else
                    oCom.Tfno2 = fldTfno2.Value
                End If
                
                Set oCom.Conexion = mvarConexion
                
                mCol.Add oCom, fldEqp.Value & fldCod.Value
                rs.MoveNext
            Wend
        End If
        
        Set fldEqp = Nothing
        Set fldCod = Nothing
        Set fldApe = Nothing
        Set fldNom = Nothing
        Set fldFax = Nothing
        Set fldEmail = Nothing
        Set fldTfno = Nothing
        Set fldDenEqp = Nothing
        Set fldCargo = Nothing
        Set fldTfno2 = Nothing
    End If
    
    rs.Close
    Set rs = Nothing
    Set adoParam = Nothing
    Set adoComm = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CCompradores", "CargarTodosLosCompradoresPorUON", ERR, Erl)
        Exit Sub
    End If
End Sub

Public Function DevolverLosCodigos() As TipoDatosCombo
   
   Dim Codigos As TipoDatosCombo
   Dim iCont As Integer
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
   ReDim Codigos.Cod(mCol.Count)
   ReDim Codigos.Den(mCol.Count)
   
    For iCont = 0 To mCol.Count - 1
        
        Codigos.Cod(iCont) = mCol(iCont + 1).Cod
        Codigos.Den(iCont) = mCol(iCont + 1).Apel & ", " & mCol(iCont + 1).Nombre
    
    Next

   DevolverLosCodigos = Codigos
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CCompradores", "DevolverLosCodigos", ERR, Erl)
        Exit Function
    End If
   
End Function

''' <summary>Devuelve los materiales de un comprador</summary>
''' <param name="CodEqpComprador">Cod. equipo comprador</param>
''' <param name="CodComprador">Cod. comprador</param>
''' <returns>Array de materiales</returns>
''' <remarks>Llamada desde GSClient</remarks>
''' <revision>LTG 03/01/2012</revision>

Public Function DevolverMaterialesDeUnComprador(ByVal CodEqpComprador As String, ByVal CodComprador As String) As Variant
    Dim rs As New adodb.Recordset
    Dim sConsulta As String
    Dim arrMatComp() As Variant
    Dim i As Integer

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sConsulta = "SELECT GMN1, GMN2, GMN3, GMN4 FROM COM_GMN4 WITH (NOLOCK) "
    sConsulta = sConsulta & "WHERE EQP='" & DblQuote(CodEqpComprador) & "' AND COM='" & DblQuote(CodComprador) & "' "
    sConsulta = sConsulta & "ORDER BY GMN1,GMN2,GMN3,GMN4"
    
    rs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
      
    ReDim Preserve arrMatComp(4, i)
      
    If rs.eof Then
        rs.Close
        Set rs = Nothing
        DevolverMaterialesDeUnComprador = arrMatComp
        Exit Function
    Else
         i = 0
         While Not rs.eof
            ReDim Preserve arrMatComp(4, i)
            arrMatComp(0, i) = rs.Fields("GMN1")
            arrMatComp(1, i) = rs.Fields("GMN2")
            arrMatComp(2, i) = rs.Fields("GMN3")
            arrMatComp(3, i) = rs.Fields("GMN4")
            i = i + 1
            rs.MoveNext
         Wend
    End If
    
    rs.Close
    Set rs = Nothing
    DevolverMaterialesDeUnComprador = arrMatComp
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CCompradores", "DevolverMaterialesDeUnComprador", ERR, Erl)
        Exit Function
    End If
End Function


