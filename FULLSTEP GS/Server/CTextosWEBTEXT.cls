VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CTextosWEBTEXT"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

''' Control de errores
Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum

Private m_oConexion As CConexion

Friend Property Set Conexion(ByVal con As CConexion)
    Set m_oConexion = con
End Property

Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property

'<summary>Devuelve un texto de la tabla WEBTEXT</summary>
'<param name="Modulo">Modulo</param>
'<param name="ID">ID</param>
'<param name="Idioma">Idioma</param>
'<returns>Un string con el texto pedido</returns>
'<remarks>Llamada desde: GSNotificador, Tiempo m�ximo: 0,3 seg</remarks>

Public Function DevolverTextoWEBTEXT(ByVal Modulo As Long, ByVal Id As Long, ByVal Idioma As String) As String
    Dim rs As adodb.Recordset
    Dim adocom As adodb.Command
    Dim oParam As adodb.Parameter
    
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CTextosWEBTEXT.DevolverTextoWEBTEXT", "No se ha establecido la conexion"
        Exit Function
    End If
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
    Set adocom = New adodb.Command
    Set adocom.ActiveConnection = m_oConexion.ADOCon
    adocom.CommandType = adCmdStoredProc
    adocom.CommandText = "FSGS_DEVOLVER_TEXTO_WEBTEXT"
        
    Set oParam = adocom.CreateParameter("MODULO", adInteger, adParamInput, , Modulo)
    adocom.Parameters.Append oParam
    Set oParam = adocom.CreateParameter("ID", adInteger, adParamInput, , Id)
    adocom.Parameters.Append oParam
    Set oParam = adocom.CreateParameter("IDIOMA", adVarChar, adParamInput, 50, Idioma)
    adocom.Parameters.Append oParam
        
    Set rs = adocom.Execute
       
    If Not rs Is Nothing Then
        If Not rs.eof Then
            DevolverTextoWEBTEXT = rs("TEXT_" & Idioma).Value
        End If
    End If
      
    Set rs = Nothing
    Set adocom = Nothing
    Set oParam = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CTextosWEBTEXT", "DevolverTextoWEBTEXT", ERR, Erl)
        Exit Function
    End If
End Function

'<summary>Devuelve un texto de la tabla WEBFSWSTEXT</summary>
'<param name="Modulo">Modulo</param>
'<param name="ID">ID</param>
'<param name="Idioma">Idioma</param>
'<returns>Un string con el texto pedido</returns>
'<remarks>Llamada desde: frmSELPROVEResp/cmdaceptar_click, Tiempo m�ximo: 0,3 seg</remarks>
''' Revisado por: jbg; Fecha: 01/10/2013
Public Function DevolverTextoWEBFSWSTEXT(ByVal Modulo As Long, ByVal Id As Long, ByVal Idioma As String) As String
    Dim rs As adodb.Recordset
    Dim adocom As adodb.Command
    
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CTextosWEBTEXT.DevolverTextoWEBFSWSTEXT", "No se ha establecido la conexion"
        Exit Function
    End If
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
    Set adocom = New adodb.Command
    Set adocom.ActiveConnection = m_oConexion.ADOCon
    adocom.CommandType = adCmdText
    adocom.CommandText = "SELECT TEXT_" & Idioma & " FROM WEBFSWSTEXT WITH(NOLOCK) WHERE MODULO =" & Modulo & " AND ID =" & Id
        
    Set rs = adocom.Execute
       
    If Not rs Is Nothing Then
        If Not rs.eof Then
            DevolverTextoWEBFSWSTEXT = rs("TEXT_" & Idioma).Value
        End If
    End If
      
    Set rs = Nothing
    Set adocom = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CTextosWEBTEXT", "DevolverTextoWEBFSWSTEXT", ERR, Erl)
        Exit Function
    End If
End Function
