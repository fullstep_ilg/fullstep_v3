VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CPaises"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Collection" ,"CPais"
Attribute VB_Ext_KEY = "Member0" ,"CPais"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit
''' *** Clase: CPaises
''' *** Creacion: 1/09/1998 (Javier Arana)
''' *** Ultima revision: 14/01/1999 (Alfredo Magallon)

''' Variables que almacenan las propiedades de los objetos

Private mCol As Collection
Private m_bEOF As Boolean
Private m_oConexion As CConexion
Private m_bPortal As Boolean

' Variables para el control de cambios del Log e Integraci�n
Private m_udtOrigen As OrigenIntegracion
''' Control de errores

Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum

Friend Property Let Portal(ByVal Data As Boolean)
    
    Let m_bPortal = Data
    
End Property

''' <summary>
''' Carga los paises de la BD en la coleccion
''' </summary>
''' <param name="CaracteresInicialesCod">Cod de busqueda</param>
''' <param name="CaracteresInicialesDen">Den de busqueda</param>
''' <param name="CoincidenciaTotal">Si la busqueda ha de coincidir con la den o el cod</param>
''' <param name="OrdenadosPorDen">Ordenacion por denominacion</param>
''' <param name="OrdenadosPorMoneda">Ordenacion por moneda</param>
''' <param name="UsarIndice">Si se usa indice para la coleccion</param>
''' <param name="MostrarEstadoIntegracion">Si se muestra el estado de la integracion</param>
''' <param name="OrdenadosPorEstadoInt">Si se ordena por el estado de la integracion</param>
''' <returns></returns>
''' <remarks>Llamada desde:Todos los combos de paises y pantallas que tienen combos de pais de la aplicacion </remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>
''' <revision>JVS 26/12/2011</revision>
Public Sub CargarTodosLosPaises(Optional ByVal CaracteresInicialesCod As String, Optional ByVal CaracteresInicialesDen As String, Optional ByVal CoincidenciaTotal As Boolean = False, Optional ByVal OrdenadosPorDen As Boolean = False, Optional ByVal OrdenadosPorMoneda As Boolean = False, Optional ByVal UsarIndice As Boolean = False, Optional ByVal MostrarEstadoIntegracion As Boolean = False, Optional ByVal OrdenadosPorEstadoInt As Boolean = False)

    'ado  Dim rdores As rdoResultset
    Dim rs As adodb.Recordset
    Dim fldPaCod As adodb.Field
    Dim fldPaDen As adodb.Field
    Dim fldPaId As adodb.Field
    Dim fldMonCod As adodb.Field
    Dim fldMonDen As adodb.Field
    Dim fldFecAct As adodb.Field
    Dim fldMonId As adodb.Field
    Dim fldEstadoInt As adodb.Field   'Estado integraci�n
    Dim sConsulta As String
    Dim lIndice As Long
    Dim sFSP As String
    Dim v_Estado As Variant
    
    
    ''' * Objetivo: Cargar la coleccion de paises
    ''' *           Cuando se trabaja con integraci�n, desde el mantenimiento se visualiza el estado de integraci�n
    ''' *           en el sistema receptor de las modificaciones originadas en FSGS.
    ''' *           Buscamos por coincidencia de c�digo (COD) cuando la acci�n es distinta a un cambio de c�digo (C)
    ''' *           y por coincidencia en c�digo nuevo (COD_NEW) cuando la acci�n es un cambio de c�digo
   
    ''' Precondicion
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CDivisiones.CargarTodasLasdivisiones", "No se ha establecido la conexion"
        Exit Sub
    End If
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
    ''' Generacion del SQL a partir de los parametros
    
    sFSP = basParametros.gParametrosGenerales.gsFSP_SRV & "." & basParametros.gParametrosGenerales.gsFSP_BD & ".dbo."
    
    If m_bPortal Then
        'CALIDAD Resulta q no se puede poner WITH (NOLOCK) en esta sentencia pq esta preguntando por tablas de otro servidor
        'De poner WITH(NOLOCK) dar�a el siguiente Error_Cls:
        '   Number : -2147217900
        '   Description: Cannot specify an index or locking hint for a remote data source.
        sConsulta = "DECLARE @IDI INT"
        sConsulta = sConsulta & " SELECT @IDI = ID FROM " & sFSP & "IDI WHERE COD = '" & gParametrosInstalacion.gIdioma & "'  "
        sConsulta = sConsulta & " SELECT TABLA.ID, TABLA.PAICOD, TABLA.PAIDEN, TABLA.PAIMON, TABLA.MONCOD, TABLA.MONDEN, TABLA.MONFECACT, TABLA.MONID  FROM (" ''MultiERP
        sConsulta = sConsulta & " SELECT PAI.ID,PAI.COD AS PAICOD,PAI_DEN.DEN AS PAIDEN,PAI.MON AS PAIMON,MON.COD AS MONCOD, MON_DEN.DEN AS MONDEN,MON.FECACT AS MONFECACT,MON.ID AS MONID FROM " & sFSP & "PAI AS PAI  LEFT JOIN " & sFSP & "PAI_DEN AS PAI_DEN ON PAI_DEN.PAI = PAI.ID AND PAI_DEN.IDIOMA = @IDI LEFT JOIN " & sFSP & "MON AS MON ON PAI.MON=MON.ID LEFT JOIN " & sFSP & " MON_DEN AS MON_DEN ON MON_DEN.MON=MON.ID AND MON_DEN.IDIOMA = @IDI "
    Else
        If MostrarEstadoIntegracion Then
            ''MultiERP
            sConsulta = "SELECT PAICOD, PAIDEN, PAIMON, MAX(PAIFECACT) AS PAIFECACT, MONCOD, MONDEN, MIN(ESTADO) AS ESTADO FROM ( "
            sConsulta = sConsulta & "SELECT PAI.COD AS PAICOD,PAI_DEN.DEN AS PAIDEN,PAI.MON AS PAIMON,PAI.FECACT AS PAIFECACT,MON.COD AS MONCOD, MON_DEN.DEN AS MONDEN,CASE WHEN RIGHT(MAX(convert(varchar,log_gral.fecact,121)+LOG_GRAL.ESTADO),1) is null then 4 WHEN RIGHT(MAX(convert(varchar,log_gral.fecact,121)+LOG_GRAL.ESTADO),1) = 4 then 4 else 3 END AS ESTADO "
            sConsulta = sConsulta & " FROM PAI WITH (NOLOCK) "
            sConsulta = sConsulta & " LEFT JOIN PAI_DEN WITH (NOLOCK) ON PAI.COD = PAI_DEN.PAI AND PAI_DEN.IDIOMA = '" & gParametrosInstalacion.gIdioma & "'"
            sConsulta = sConsulta & " LEFT JOIN MON WITH (NOLOCK) ON PAI.MON=MON.COD "
            sConsulta = sConsulta & " LEFT JOIN MON_DEN WITH (NOLOCK) ON MON.COD=MON_DEN.MON AND MON_DEN.IDIOMA = '" & gParametrosInstalacion.gIdioma & "'"
            sConsulta = sConsulta & " LEFT JOIN (LOG_PAI WITH (NOLOCK) INNER JOIN LOG_GRAL WITH (NOLOCK) ON LOG_PAI.ID = LOG_GRAL.ID_TABLA AND LOG_GRAL.TABLA = " & EntidadIntegracion.Pai & " AND (LOG_GRAL.ORIGEN = " & OrigenIntegracion.FSGSInt & " OR LOG_GRAL.ORIGEN = " & OrigenIntegracion.FSGSIntReg & "))"
            sConsulta = sConsulta & " ON (LOG_PAI.COD = PAI.COD AND LOG_PAI.ACCION <> '" & Accion_CambioCodigo & "') OR (LOG_PAI.COD_NEW = PAI.COD AND LOG_PAI.ACCION = '" & Accion_CambioCodigo & "') "
            

        Else
            sConsulta = "SELECT TABLA.PAICOD, TABLA.PAIDEN, TABLA.PAIMON, TABLA.PAIFECACT, TABLA.MONCOD, TABLA.MONDEN FROM (" ''MultiERP
            sConsulta = sConsulta & " SELECT PAI.COD AS PAICOD,PAI_DEN.DEN AS PAIDEN,PAI.MON AS PAIMON,PAI.FECACT AS PAIFECACT,MON.COD AS MONCOD, MON_DEN.DEN AS MONDEN "
            sConsulta = sConsulta & " FROM PAI WITH (NOLOCK) "
            sConsulta = sConsulta & " LEFT JOIN PAI_DEN WITH (NOLOCK) ON PAI.COD = PAI_DEN.PAI AND PAI_DEN.IDIOMA = '" & gParametrosInstalacion.gIdioma & "'"
            sConsulta = sConsulta & " LEFT JOIN MON WITH (NOLOCK) ON PAI.MON=MON.COD"
            sConsulta = sConsulta & " LEFT JOIN MON_DEN WITH (NOLOCK) ON MON.COD=MON_DEN.MON AND MON_DEN.IDIOMA = '" & gParametrosInstalacion.gIdioma & "'"

        End If
    End If

    
    If CaracteresInicialesCod = "" And CaracteresInicialesDen = "" Then
            
    Else
        If Not (CaracteresInicialesCod = "") And Not (CaracteresInicialesDen = "") Then
            
            If CoincidenciaTotal Then
                
                If m_bPortal Then
                    sConsulta = sConsulta & " WHERE PAI.COD ='" & DblQuote(CaracteresInicialesCod) & "'"
                    sConsulta = sConsulta & " AND PAI_DEN.DEN='" & DblQuote(CaracteresInicialesDen) & "'"
                Else
                    sConsulta = sConsulta & " WHERE PAI.COD ='" & DblQuote(CaracteresInicialesCod) & "'"
                    sConsulta = sConsulta & " AND PAI_DEN.DEN='" & DblQuote(CaracteresInicialesDen) & "'"
                End If
            Else
                If m_bPortal Then
                    sConsulta = sConsulta & " WHERE PAI.COD LIKE '" & DblQuote(CaracteresInicialesCod) & "%'"
                    sConsulta = sConsulta & " PAI_DEN.DEN LIKE '" & DblQuote(CaracteresInicialesDen) & "%'"
                Else
                    sConsulta = sConsulta & " WHERE PAI.COD LIKE '" & DblQuote(CaracteresInicialesCod) & "%'"
                    sConsulta = sConsulta & " PAI_DEN.DEN LIKE '" & DblQuote(CaracteresInicialesDen) & "%'"
                End If
            End If
                        
        Else
            
            If Not (CaracteresInicialesCod = "") Then
                
                If CoincidenciaTotal Then
                
                    If m_bPortal Then
                        sConsulta = sConsulta & " WHERE PAI.COD ='" & DblQuote(CaracteresInicialesCod) & "'"
                    Else
                        sConsulta = sConsulta & " WHERE PAI.COD ='" & DblQuote(CaracteresInicialesCod) & "'"
                    End If
                Else
                    
                    If m_bPortal Then
                        sConsulta = sConsulta & " WHERE PAI.COD LIKE '" & DblQuote(CaracteresInicialesCod) & "%'"
                    Else
                        sConsulta = sConsulta & " WHERE PAI.COD LIKE '" & DblQuote(CaracteresInicialesCod) & "%'"
                    End If
                End If
                
            Else
                
                If CoincidenciaTotal Then
                
                    If m_bPortal Then
                        sConsulta = sConsulta & " WHERE PAI_DEN.DEN='" & DblQuote(CaracteresInicialesDen) & "'"
                    Else
                        sConsulta = sConsulta & " WHERE PAI_DEN.DEN='" & DblQuote(CaracteresInicialesDen) & "'"
                    End If
                    
                Else
                    
                    If m_bPortal Then
                        sConsulta = sConsulta & " WHERE PAI_DEN.DEN LIKE '" & DblQuote(CaracteresInicialesDen) & "%'"
                    
                    Else
                    
                       sConsulta = sConsulta & " WHERE PAI_DEN.DEN LIKE '" & DblQuote(CaracteresInicialesDen) & "%'"
                    
                    End If
                    
                End If
                
            End If
        
        End If
               
    End If
    
    If MostrarEstadoIntegracion Then
        sConsulta = sConsulta & " GROUP BY PAI.COD,PAI_DEN.DEN,PAI.MON,PAI.FECACT,MON.COD,MON_DEN.DEN,LOG_PAI.ERP"
        sConsulta = sConsulta & ") AS TABLA GROUP BY TABLA.PAICOD,TABLA.PAIDEN,TABLA.PAIMON,TABLA.PAIFECACT,TABLA.MONCOD,TABLA.MONDEN "
    Else ''MultiERP
        sConsulta = sConsulta & ") AS TABLA "
    End If
    
    If OrdenadosPorDen Then
        sConsulta = sConsulta & " ORDER BY PAIDEN"
    Else
        If OrdenadosPorMoneda Then
            sConsulta = sConsulta & " ORDER BY MONCOD"
        Else
            If OrdenadosPorEstadoInt Then
                sConsulta = sConsulta & " ORDER BY ESTADO"
            Else
                sConsulta = sConsulta & " ORDER BY PAICOD"
            End If
        End If
    End If
          
    ''' Creacion del Resultset
    
    Set rs = New adodb.Recordset
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    
    If rs.eof Then
            
        rs.Close
        Set rs = Nothing
        Set mCol = Nothing
        Set mCol = New Collection
        Exit Sub
          
    Else
        
        ''' Carga del Resultset en la coleccion
        
        Set mCol = Nothing
        Set mCol = New Collection
        
        If m_bPortal Then
            Set fldPaId = rs.Fields("ID")   '0
            Set fldPaCod = rs.Fields("PAICOD") '1
            Set fldPaDen = rs.Fields("PAIDEN") '2
            Set fldMonCod = rs.Fields("MONCOD")   '4
            Set fldMonDen = rs.Fields("MONDEN")   '5
            Set fldMonId = rs.Fields("MONID")  '7
        
            If UsarIndice Then
                
                lIndice = 0
            
                While Not rs.eof
                
                    Me.Add fldPaCod.Value, fldPaDen.Value, fldMonCod.Value, fldMonDen.Value, lIndice, fldPaId.Value, fldMonId.Value
                    rs.MoveNext
                    lIndice = lIndice + 1
                 
                Wend
                
            Else
            
                While Not rs.eof
                
                    Me.Add fldPaCod.Value, fldPaDen.Value, fldMonCod.Value, fldMonDen.Value, , fldPaId.Value, NullToDbl0(fldMonId.Value)
                    rs.MoveNext
                    
                Wend
                
            End If
            Set fldPaId = Nothing
            Set fldPaCod = Nothing
            Set fldPaDen = Nothing
            Set fldMonCod = Nothing
            Set fldMonDen = Nothing
            Set fldMonId = Nothing
            
        Else
            Set fldPaCod = rs.Fields("PAICOD") '1
            Set fldPaDen = rs.Fields("PAIDEN") '2
            Set fldMonCod = rs.Fields("MONCOD")   '4
            Set fldMonDen = rs.Fields("MONDEN")   '5
            Set fldFecAct = rs.Fields("PAIFECACT")
            If MostrarEstadoIntegracion Then
                Set fldEstadoInt = rs.Fields("ESTADO")
            End If

            If UsarIndice Then
                
                lIndice = 0
            
                While Not rs.eof
                    If MostrarEstadoIntegracion Then
                        v_Estado = fldEstadoInt.Value
                    Else
                        v_Estado = Null
                    End If

                    Me.Add fldPaCod.Value, fldPaDen.Value, fldMonCod.Value, fldMonDen.Value, lIndice, , , fldFecAct.Value, v_Estado
                    
                    rs.MoveNext
                    lIndice = lIndice + 1
                    
                Wend
                
            Else
            
                While Not rs.eof
                    If MostrarEstadoIntegracion Then
                        v_Estado = fldEstadoInt.Value
                    Else
                        v_Estado = Null
                    End If

                    Me.Add fldPaCod.Value, fldPaDen.Value, fldMonCod.Value, fldMonDen.Value, , , , fldFecAct, v_Estado
                    
                    rs.MoveNext
                    
                Wend
                
            End If
            Set fldPaCod = Nothing
            Set fldPaDen = Nothing
            Set fldMonCod = Nothing
            Set fldMonDen = Nothing
            Set fldFecAct = Nothing
            Set fldEstadoInt = Nothing
          
        End If
        
        rs.Close
        Set rs = Nothing
    
        
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CPaises", "CargarTodosLosPaises", ERR, Erl)
        Exit Sub
    End If
    
End Sub
Public Function Add(ByVal Cod As String, ByVal Den As String, Optional ByVal MonCod As Variant, Optional ByVal MonDen As Variant, Optional ByVal varIndice As Variant, Optional ByVal PaiId As Integer, Optional ByVal MonId As Integer, Optional ByVal FecAct As Variant, Optional ByVal varEstado As Variant) As CPais
    
    ''' * Objetivo: Anyadir un pais a la coleccion
    ''' * Recibe: Datos del pais
    ''' * Devuelve: Pais anyadido
        
    Dim oPais As CPais
    
    Dim oGestorParametros As CGestorParametros
    Dim oIdiomas As CIdiomas
    Dim oDen As CMultiidiomas
    Dim oIdi As CIdioma

    ''' Creacion de objeto pais
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oPais = New CPais
    
    ''' Paso de los parametros al nuevo objeto pais
    
    oPais.Cod = Cod
    oPais.Den = Den
    If Not IsMissing(FecAct) Then
        oPais.FecAct = FecAct
    End If
    oPais.EstadoIntegracion = varEstado 'Estado integracion
    
    If Not IsMissing(PaiId) Then
        oPais.Id = PaiId
    End If
    
    
    Set oPais.Conexion = m_oConexion
    oPais.Portal = m_bPortal
    
    If Not IsMissing(MonCod) And Not IsNull(MonCod) Then
    
        Set oPais.Moneda = New CMoneda
        Set oPais.Moneda.Conexion = m_oConexion
        
        oPais.Moneda.Cod = MonCod
        
        If Not IsMissing(MonDen) And Not IsNull(MonDen) Then
            'Carga todos los idiomas de la aplicaci�n:
            Set oGestorParametros = New CGestorParametros
            Set oGestorParametros.Conexion = m_oConexion
            Set oIdiomas = oGestorParametros.DevolverIdiomas(False, False, True)
            
            Set oDen = Nothing
            Set oDen = New CMultiidiomas
            Set oDen.Conexion = m_oConexion
            
            For Each oIdi In oIdiomas
                oDen.Add oIdi.Cod, ""
            Next
            
            oDen.Item(gParametrosInstalacion.gIdioma).Den = MonDen
            
            Set oPais.Moneda.Denominaciones = oDen
            
        End If
            
        If Not IsMissing(MonId) Then
            oPais.Moneda.Id = MonId
        End If
        
    End If
    
    ''' Anyadir el objeto pais a la coleccion
    ''' Si no se especifica indice, se anyade al final
    
    If Not IsMissing(varIndice) And Not IsNull(varIndice) Then
    
        oPais.Indice = varIndice
        mCol.Add oPais, CStr(varIndice)
        
    Else
        mCol.Add oPais, Cod
    End If
    
    Set Add = oPais
    
    Set oPais = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CPaises", "Add", ERR, Erl)
        Exit Function
    End If

End Function
Public Property Get Item(vntIndexKey As Variant) As CPais

    ''' * Objetivo: Recuperar un pais de la coleccion
    ''' * Recibe: Indice del pais a recuperar
    ''' * Devuelve: Pais correspondiente
    
On Error GoTo NoSeEncuentra:

    ''' Devolvemos el item de la coleccion privada
    
    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:

    Set Item = Nothing
    
End Property

Friend Property Set Conexion(ByVal con As CConexion)

    ''' * Objetivo: Dar valor a la variable privada Conexion
    ''' * Recibe: Conexion
    ''' * Devuelve: Nada
    
    Set m_oConexion = con
    
End Property
Friend Property Get Conexion() As CConexion

    ''' * Objetivo: Devolver variable privada Conexion
    ''' * Recibe: Nada
    ''' * Devuelve: Conexion
    
    Set Conexion = m_oConexion
    
End Property


Public Property Get Count() As Long

    ''' * Objetivo: Devolver variable privada Count
    ''' * Recibe: Nada
    ''' * Devuelve: Numero de elementos de la coleccion

    If mCol Is Nothing Then
        Count = 0
    Else
        Count = mCol.Count
    End If

End Property

Public Property Get eof() As Boolean

    ''' * Objetivo: Devolver variable privada EOF
    ''' * Recibe: Nada
    ''' * Devuelve: Variable privada EOF
    
    eof = m_bEOF
    
End Property
Public Sub Remove(vntIndexKey As Variant)
   
    ''' * Objetivo: Eliminar una moneda de la coleccion
    ''' * Recibe: Indice de la moneda a eliminar
    ''' * Devuelve: Nada
    
    mCol.Remove vntIndexKey
    
End Sub
Private Sub Class_Initialize()

    ''' * Objetivo: Crear la coleccion
    
    '*-*'
    
    Set mCol = New Collection
    
End Sub
Private Sub Class_Terminate()

    ''' * Objetivo: Limpiar memoria
    
    Set mCol = Nothing
    Set m_oConexion = Nothing

End Sub
Public Function DevolverPais(ByVal Cod As String) As CPais

    ''' ! Pendiente de revision, segun tema de combos, etc.
    
    Dim oPais As CPais
    Dim iIndice As Integer
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    
    For iIndice = 1 To mCol.Count
        
        Set oPais = mCol.Item(iIndice)
        If oPais.Cod = Cod Then
            Set DevolverPais = oPais
            Set oPais = Nothing
            Exit Function
        End If
    
    Next
    
    Set oPais = Nothing
    Set DevolverPais = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CPaises", "DevolverPais", ERR, Erl)
        Exit Function
    End If

End Function
Public Function DevolverLosCodigos() As TipoDatosCombo
   
    ''' ! Pendiente de revision, segun tema de combos
    
   Dim Codigos As TipoDatosCombo
   Dim iCont As Integer
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
   ReDim Codigos.Cod(mCol.Count)
   ReDim Codigos.Den(mCol.Count)
   ReDim Codigos.Id(mCol.Count)
   
    For iCont = 0 To mCol.Count - 1
        
        Codigos.Cod(iCont) = mCol(iCont + 1).Cod
        Codigos.Den(iCont) = mCol(iCont + 1).Den
        Codigos.Id(iCont) = mCol(iCont + 1).Id
    Next

   DevolverLosCodigos = Codigos
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CPaises", "DevolverLosCodigos", ERR, Erl)
        Exit Function
    End If
   
End Function

''' <summary>
''' Carga los paises de la BD en la coleccion
''' </summary>
''' <param name="NumMaximo">Numero m�ximo de paises</param>
''' <param name="CaracteresInicialesCod">Cod de busqueda</param>
''' <param name="CaracteresInicialesDen">Den de busqueda</param>
''' <param name="OrdenadosPorDen">Ordenacion por denominacion</param>
''' <param name="UsarIndice">Si se usa indice para la coleccion</param>
''' <returns></returns>
''' <remarks>Llamada desde:Todos los combos de paises y pantallas que tienen combos de pais de la aplicacion </remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>
''' <revision>JVS 26/12/2011</revision>

Public Sub CargarTodosLosPaisesDesde(ByVal NumMaximo As Integer, Optional ByVal CaracteresInicialesCod As String, Optional ByVal CaracteresInicialesDen As String, Optional ByVal OrdenadosPorDen As Boolean, Optional ByVal UsarIndice As Boolean)

    
    'ado  Dim rdores As rdoResultset
    Dim rs As adodb.Recordset
    Dim fldPaCod As adodb.Field
    Dim fldPaDen As adodb.Field
    Dim fldPaId As adodb.Field
    Dim fldMonCod As adodb.Field
    Dim fldMonDen As adodb.Field
    Dim fldMonId As adodb.Field
    Dim sConsulta As String
    Dim lIndice As Long
    Dim sFSP As String
    
    ''' Precondicion
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CDestinos.CargarTodosLosDestinos", "No se ha establecido la conexion"
        Exit Sub
    End If
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
     sFSP = basParametros.gParametrosGenerales.gsFSP_SRV & "." & basParametros.gParametrosGenerales.gsFSP_BD & ".dbo."
   
   
    ''' Generacion del SQL a partir de los parametros
           
    If CaracteresInicialesCod = "" And CaracteresInicialesDen = "" Then
        If m_bPortal Then
            'CALIDAD Resulta q no se puede poner WITH (NOLOCK) en esta sentencia pq esta preguntando por tablas de otro servidor
            'De poner WITH(NOLOCK) dar�a el siguiente Error_Cls:
            '   Number : -2147217900
            '   Description: Cannot specify an index or locking hint for a remote data source.
            sConsulta = "DECLARE @IDI INT"
            sConsulta = sConsulta & " SELECT @IDI = ID FROM " & sFSP & "IDI WHERE COD = '" & gParametrosInstalacion.gIdioma & "'  "
            sConsulta = sConsulta & " SELECT TOP " & NumMaximo & " PAI.ID,PAI.COD AS PAICOD,PAI_DEN.DEN AS PAIDEN,PAI.MON AS PAIMON,MON.COD AS MONCOD, MON_DEN.DEN AS MONDEN,MON.FECACT AS MONFECACT,MON.ID AS MONID FROM " & sFSP & "PAI AS PAI LEFT JOIN " & sFSP & "PAI_DEN AS PAI_DEN ON PAI_DEN.PAI = PAI.ID AND PAI_DEN.IDIOMA = @IDI LEFT JOIN " & sFSP & "MON AS MON ON PAI.MON=MON.ID LEFT JOIN " & sFSP & " MON_DEN AS MON_DEN ON MON_DEN.MON=MON.ID AND MON_DEN.IDIOMA = @IDI "
        Else
            sConsulta = "SELECT TOP " & NumMaximo & " PAI.COD AS PAICOD,PAI_DEN.DEN AS PAIDEN,PAI.MON AS PAIMON,PAI.FECACT AS PAIFECACT,MON.COD AS MONCOD, MON_DEN.DEN AS MONDEN,MON.FECACT AS MONFECACT FROM PAI WITH(NOLOCK) "
            sConsulta = sConsulta & " LEFT JOIN PAI_DEN WITH (NOLOCK) ON PAI.COD = PAI_DEN.PAI AND PAI_DEN.IDIOMA = '" & gParametrosInstalacion.gIdioma & "'"
            sConsulta = sConsulta & " LEFT JOIN MON WITH (NOLOCK) ON PAI.MON=MON.COD"
            sConsulta = sConsulta & " LEFT JOIN MON_DEN WITH (NOLOCK) ON MON.COD=MON_DEN.MON AND MON_DEN.IDIOMA = '" & gParametrosInstalacion.gIdioma & "'"
        End If
              
    Else
        
        If m_bPortal Then
            'CALIDAD Resulta q no se puede poner WITH (NOLOCK) en esta sentencia pq esta preguntando por tablas de otro servidor
            'De poner WITH(NOLOCK) dar�a el siguiente Error_Cls:
            '   Number : -2147217900
            '   Description: Cannot specify an index or locking hint for a remote data source.
            sConsulta = "DECLARE @IDI INT"
            sConsulta = sConsulta & " SELECT @IDI = ID FROM " & sFSP & "IDI WHERE COD = '" & gParametrosInstalacion.gIdioma & "'  "
            sConsulta = sConsulta & " SELECT TOP " & NumMaximo & " PAI.ID,PAI.COD AS PAICOD,PAI_DEN.DEN AS PAIDEN,PAI.MON AS PAIMON,MON.COD AS MONCOD, MON_DEN.DEN AS MONDEN,MON.FECACT AS MONFECACT,MON.ID AS MONID FROM " & sFSP & "PAI AS PAI LEFT JOIN " & sFSP & "PAI_DEN AS PAI_DEN ON PAI_DEN.PAI = PAI.ID AND PAI_DEN.IDIOMA = @IDI LEFT JOIN " & sFSP & "MON AS MON ON PAI.MON=MON.ID LEFT JOIN " & sFSP & " MON_DEN AS MON_DEN ON MON_DEN.MON=MON.ID AND MON_DEN.IDIOMA = @IDI WHERE"
        Else
            sConsulta = "SELECT TOP " & NumMaximo & " PAI.COD AS PAICOD,PAI_DEN.DEN AS PAIDEN,PAI.MON AS PAIMON,PAI.FECACT AS PAIFECACT,MON.COD AS MONCOD, MON_DEN.DEN AS MONDEN,MON.FECACT AS MONFECACT FROM PAI WITH(NOLOCK) "
            sConsulta = sConsulta & " LEFT JOIN PAI_DEN WITH (NOLOCK) ON PAI.COD = PAI_DEN.PAI AND PAI_DEN.IDIOMA = '" & gParametrosInstalacion.gIdioma & "'"
            sConsulta = sConsulta & " LEFT JOIN MON WITH (NOLOCK) ON PAI.MON=MON.COD"
            sConsulta = sConsulta & " LEFT JOIN MON_DEN WITH (NOLOCK) ON MON.COD=MON_DEN.MON AND MON_DEN.IDIOMA = '" & gParametrosInstalacion.gIdioma & "' WHERE"
            
        End If
        
        If Not (CaracteresInicialesCod = "") And Not (CaracteresInicialesDen = "") Then
                
            sConsulta = sConsulta & " PAI.COD >='" & DblQuote(CaracteresInicialesCod) & "'"
            sConsulta = sConsulta & " AND PAI_DEN.DEN >='" & DblQuote(CaracteresInicialesDen) & "'"
                    
        Else
            
            If Not (CaracteresInicialesCod = "") Then
                sConsulta = sConsulta & " PAI.COD >='" & DblQuote(CaracteresInicialesCod) & "'"
            Else
                sConsulta = sConsulta & " PAI_DEN.DEN >='" & DblQuote(CaracteresInicialesDen) & "'"
            End If
        
        End If
               
    End If
    
    If OrdenadosPorDen Then
        sConsulta = sConsulta & " ORDER BY PAIDEN"
    Else
        sConsulta = sConsulta & " ORDER BY PAICOD"
    End If
          
    ''' Crear el Recordset
    
    'ado  Set rdores = m_oConexion.rdoSummitCon.OpenResultset(sConsulta, rdOpenForwardOnly, rdConcurReadOnly, rdExecDirect)
    Set rs = New adodb.Recordset
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    
    If rs.eof Then
            
        rs.Close
        Set rs = Nothing
        Set mCol = Nothing
        Set mCol = New Collection
        
        Exit Sub
          
    Else
        
        ''' Llenar la coleccion a partir del Resultset
        ''' teniendo en cuenta el maximo
        
        Set mCol = Nothing
        Set mCol = New Collection
         
        If m_bPortal Then
            Set fldPaId = rs.Fields("ID")   '0
            Set fldPaCod = rs.Fields("PAICOD") '1
            Set fldPaDen = rs.Fields("PAIDEN") '2
            Set fldMonCod = rs.Fields("MONCOD")   '4
            Set fldMonDen = rs.Fields("MONDEN")   '5
            Set fldMonId = rs.Fields("MONID")  '7
            
            If UsarIndice Then
                
                lIndice = 0
                        
                While Not rs.eof
                    'ado  Me.Add rdores("PAICOD"), rdores("PAIDEN"), rdores("MONCOD"), rdores("MONDEN"), lIndice, rdores("ID"), rdores("MONID")
                    Me.Add fldPaCod.Value, fldPaDen.Value, fldMonCod.Value, fldMonDen.Value, lIndice, fldPaId.Value, fldMonId.Value
                    rs.MoveNext
                    lIndice = lIndice + 1
                Wend
                
                If Not rs.eof Then
                    m_bEOF = False
                Else
                    m_bEOF = True
                End If
        
            Else
                                
                While Not rs.eof
                    'ado  Me.Add rdores("PAICOD"), rdores("PAIDEN"), NullToStr(rdores("MONCOD")), NullToStr(rdores("MONDEN")), , rdores("ID"), NullToDbl0(rdores("MONID"))
                    Me.Add fldPaCod.Value, fldPaDen.Value, NullToStr(fldMonCod.Value), NullToStr(fldMonDen.Value), , fldPaId.Value, NullToDbl0(fldMonId.Value)
                    rs.MoveNext
                Wend
                
                If Not rs.eof Then
                    m_bEOF = False
                Else
                    m_bEOF = True
                End If
                
            End If
        Else
            Set fldPaCod = rs.Fields("PAICOD") '1
            Set fldPaDen = rs.Fields("PAIDEN") '2
            Set fldMonCod = rs.Fields("MONCOD")   '4
            Set fldMonDen = rs.Fields("MONDEN")   '5
            If UsarIndice Then
                
                lIndice = 0
                        
                While Not rs.eof
                    'ado  Me.Add rdores("PAICOD"), rdores("PAIDEN"), rdores("MONCOD"), rdores("MONDEN"), lIndice
                    Me.Add fldPaCod.Value, fldPaDen.Value, fldMonCod.Value, fldMonDen.Value, lIndice
                    rs.MoveNext
                    lIndice = lIndice + 1
                Wend
                
                If Not rs.eof Then
                    m_bEOF = False
                Else
                    m_bEOF = True
                End If
        
            Else
                                
                While Not rs.eof
                    'ado  Me.Add rdores("PAICOD"), rdores("PAIDEN"), NullToStr(rdores("MONCOD")), NullToStr(rdores("MONDEN"))
                    Me.Add fldPaCod.Value, fldPaDen.Value, NullToStr(fldMonCod.Value), NullToStr(fldMonDen.Value)
                    rs.MoveNext
                    
                Wend
                
                If Not rs.eof Then
                    m_bEOF = False
                Else
                    m_bEOF = True
                End If
                
            End If
        
        End If
        rs.Close
        Set rs = Nothing
        Set fldPaId = Nothing
        Set fldPaCod = Nothing
        Set fldPaDen = Nothing
        Set fldMonCod = Nothing
        Set fldMonDen = Nothing
        Set fldMonId = Nothing

    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CPaises", "CargarTodosLosPaisesDesde", ERR, Erl)
        Exit Sub
    End If
    
End Sub
Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"

    ''' ! Pendiente de revision, objetivo desconocido
    
    Set NewEnum = mCol.[_NewEnum]

End Property

Private Function GrabarEnLog() As Boolean
'***********************************************************************************
'*** Descripci�n: Nos indica si hay que grabar o no el cambio en el log de       ***
'***              monedas (tabla LOG_PAG) y carga en la variable                 ***
'***              del m�dulo m_udtOrigen el origen del movimiento                ***
'*** Parametros:  Ninguno                                                        ***
'*** Devuelve:    True (hay que grabar) False (No hay que grabar)                ***
'***********************************************************************************

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If gParametrosGenerales.gbACTIVLOG Or gParametrosIntegracion.gaExportar(EntidadIntegracion.Pai) Then
        If gParametrosGenerales.gbACTIVLOG And gParametrosIntegracion.gaExportar(EntidadIntegracion.Pai) Then
            m_udtOrigen = FSGSIntReg
        Else
            If gParametrosGenerales.gbACTIVLOG Then
                m_udtOrigen = FSGSReg
            Else
                m_udtOrigen = FSGSInt
            End If
        End If
        GrabarEnLog = True
    Else
        GrabarEnLog = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CPaises", "GrabarEnLog", ERR, Erl)
        Exit Function
    End If

End Function

