VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CDistItemsNivel2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CDistItemsNivel2 **********************************
'*             Autor : Javier Arana
'*             Creada : 16/6/99
'****************************************************************


Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum

Private mCol As Collection
Private mvarConexion As CConexion
Private mvarEOF As Boolean

Friend Property Set Conexion(ByVal con As CConexion)
Set mvarConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = mvarConexion
End Property

Public Property Get eof() As Boolean
    eof = mvarEOF
End Property
Friend Property Let eof(ByVal b As Boolean)
    mvarEOF = b
End Property

Public Property Get Item(vntIndexKey As Variant) As CDistItemNivel2
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
     Count = mCol.Count
End If


End Property


Public Function CargarTodasLasDistribucionesItem(ByVal oItem As CItem, Optional ByVal OrdPorUON1 As Boolean, Optional ByVal OrdPorDen As Boolean, Optional ByVal OrdPorPorcentaje As Boolean, Optional ByVal UsarIndice As Boolean) As Double
Dim sConsulta As String
Dim lIndice As Long
Dim dSuma As Double
Dim rs As adodb.Recordset
Dim fldUON1 As adodb.Field
Dim fldUON2 As adodb.Field
Dim fldPorcen As adodb.Field

'********* Precondicion **************************************
If mvarConexion Is Nothing Then
    Err.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CDistribucionesItem.CargarTodosLosDistribucionesItem", "No se ha establecido la conexion"
    Exit Function
End If
'*************************************************************
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

    dSuma = 0
            
    sConsulta = "SELECT ITEM_UON2.UON1 AS UON1, ITEM_UON2.UON2 AS UON2,PORCEN FROM ITEM_UON2 WITH (NOLOCK) WHERE ANYO=" & oItem.Proceso.Anyo & " AND GMN1='" & DblQuote(oItem.Proceso.GMN1Cod) & "' AND PROCE=" & oItem.Proceso.Cod
    sConsulta = sConsulta & " AND ITEM=" & oItem.Id
    sConsulta = sConsulta & " AND UON1 > '  '"
    
    If OrdPorPorcentaje Then
        sConsulta = sConsulta & " ORDER BY PORCEN"
    Else
        sConsulta = sConsulta & " ORDER BY ITEM_UON2.UON1,ITEM_UON2.UON2"
    End If
                
    Set rs = New adodb.Recordset
    rs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
 
    If rs.eof Then
            
        rs.Close
        Set rs = Nothing
        Set mCol = Nothing
        Set mCol = New Collection
        CargarTodasLasDistribucionesItem = 0
        Exit Function
          
    Else
 
        Set fldUON1 = rs.Fields(0)
        Set fldUON2 = rs.Fields(1)
        Set fldPorcen = rs.Fields(2)
        
        Set mCol = Nothing
        Set mCol = New Collection
            
        If UsarIndice Then
            
            lIndice = 0
                   
            While Not rs.eof
                ' Dejamos en manos del programador cuando asignar la conexion al obejto usuario
                Me.Add oItem, fldUON1.Value, fldUON2.Value, fldPorcen.Value, , lIndice
                dSuma = dSuma + fldPorcen.Value
                rs.MoveNext
                lIndice = lIndice + 1
            Wend
            
        Else
            
            While Not rs.eof
                ' Dejamos en manos del programador cuando asignar la conexion al obejto usuario
                Me.Add oItem, fldUON1.Value, fldUON2.Value, fldPorcen.Value
                dSuma = dSuma + fldPorcen.Value
                rs.MoveNext
            Wend
            
        End If
        
        CargarTodasLasDistribucionesItem = dSuma
        rs.Close
        Set rs = Nothing
        Set fldUON1 = Nothing
        Set fldUON2 = Nothing
        Set fldPorcen = Nothing
          
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If Err.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CDistItemsNivel2", "CargarTodasLasDistribucionesItem", Err, Erl)
      Exit Function
   End If

End Function

''' <summary>Carga todas las distribuciones de un proceso</summary>
''' <param name="iAnyo">Anyo</param>
''' <param name="sGMN1">GMN1</param>
''' <param name="iProceso">Id proceso</param>
''' <param name="OrdPorUON1">Orden por UON1</param>
''' <param name="OrdPorPorcentaje">Orden por porcentaje</param>
''' <param name="UsarIndice">Usar �ndice</param>
''' <returns>Suma de porcentajes</returns>
''' <remarks>Llamada desde: FSGSClient; Tiempo m�ximo: 1 sec </remarks>
''' <revision>LTG 04/01/2012</revision>

Public Function CargarTodasLasDistribucionesProce(ByVal iAnyo As Integer, ByVal sGMN1 As String, ByVal iProceso As Long, Optional ByVal OrdPorUON1 As Boolean, _
        Optional ByVal OrdPorPorcentaje As Boolean, Optional ByVal UsarIndice As Boolean) As Double
    Dim AdoRes As adodb.Recordset
    Dim sConsulta As String
    Dim lIndice As Long
    Dim dSuma As Double
    Dim adofldUON1 As adodb.Field
    Dim adofldUON2 As adodb.Field
    Dim adofldPorcen As adodb.Field
    Dim oItem As CItem
    
    '********* Precondicion **************************************
    If mvarConexion Is Nothing Then
        Err.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CDistribucionesItem.CargarTodasLasDistribucionesProce", "No se ha establecido la conexion"
        Exit Function
    End If
    '*************************************************************
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

        dSuma = 0

        sConsulta = "SELECT UON1,UON2,PORCEN"
        sConsulta = sConsulta & " FROM PROCE_UON2 WITH (NOLOCK)"
        sConsulta = sConsulta & " WHERE ANYO=" & iAnyo & " AND GMN1='" & DblQuote(sGMN1) & "' AND PROCE=" & iProceso
        If OrdPorPorcentaje Then
            sConsulta = sConsulta & " ORDER BY PORCEN"
        Else
            sConsulta = sConsulta & " ORDER BY UON1,UON2"
        End If
    
        Set AdoRes = New adodb.Recordset
        AdoRes.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly

        If AdoRes.eof Then
                
            AdoRes.Close
            Set AdoRes = Nothing
            Set mCol = Nothing
            Set mCol = New Collection
            CargarTodasLasDistribucionesProce = 0
            Exit Function
              
        Else
                
            Set adofldUON1 = AdoRes.Fields("UON1")
            Set adofldUON2 = AdoRes.Fields("UON2")
            Set adofldPorcen = AdoRes.Fields("PORCEN")
            Set mCol = Nothing
            Set mCol = New Collection
            Set oItem = Nothing
            
            If UsarIndice Then
                
                lIndice = 0
        
                While Not AdoRes.eof
                    ' Dejamos en manos del programador cuando asignar la conexion al obejto usuario
                    Me.Add oItem, adofldUON1.Value, adofldUON2.Value, adofldPorcen.Value, , lIndice
                    dSuma = dSuma + adofldPorcen.Value
                    AdoRes.MoveNext
                    lIndice = lIndice + 1
                Wend
            
            Else
                       
                While Not AdoRes.eof
                    ' Dejamos en manos del programador cuando asignar la conexion al obejto usuario
                    Me.Add oItem, adofldUON1.Value, adofldUON2.Value, adofldPorcen.Value
                    dSuma = dSuma + adofldPorcen.Value
                    AdoRes.MoveNext
                Wend
                
            End If
        
            CargarTodasLasDistribucionesProce = dSuma
            AdoRes.Close
            Set adofldUON1 = Nothing
            Set adofldUON2 = Nothing
            Set adofldPorcen = Nothing
            Set AdoRes = Nothing
          
        End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If Err.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CDistItemsNivel2", "CargarTodasLasDistribucionesProce", Err, Erl)
      Exit Function
   End If

End Function

Public Function CargarTodasLasDistribucionesGrupo(ByVal iAnyo As Integer, ByVal sGMN1 As String, ByVal iProceso As Long, ByVal lGrupo As Long, Optional ByVal OrdPorUON1 As Boolean, Optional ByVal OrdPorPorcentaje As Boolean, Optional ByVal UsarIndice As Boolean) As Double
Dim AdoRes As adodb.Recordset
Dim sConsulta As String
Dim lIndice As Long
Dim dSuma As Double
Dim adofldUON1 As adodb.Field
Dim adofldUON2 As adodb.Field
Dim adofldPorcen As adodb.Field
Dim oItem As CItem

'********* Precondicion **************************************
If mvarConexion Is Nothing Then
    Err.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CDistItemsNivel2.CargarTodasLasDistribucionesGrupo", "No se ha establecido la conexion"
    Exit Function
End If
'*************************************************************
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

        dSuma = 0

        sConsulta = "SELECT UON1,UON2,PORCEN"
        sConsulta = sConsulta & " FROM PROCE_GR_UON2 WITH (NOLOCK) "
        sConsulta = sConsulta & " WHERE ANYO=" & iAnyo & " AND GMN1='" & DblQuote(sGMN1) & "' AND PROCE=" & iProceso & " AND GRUPO=" & lGrupo
        If OrdPorPorcentaje Then
            sConsulta = sConsulta & " ORDER BY PORCEN"
        Else
            sConsulta = sConsulta & " ORDER BY UON1,UON2"
        End If
    
        Set AdoRes = New adodb.Recordset
        AdoRes.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly

        If AdoRes.eof Then
                
            AdoRes.Close
            Set AdoRes = Nothing
            Set mCol = Nothing
            Set mCol = New Collection
            CargarTodasLasDistribucionesGrupo = 0
            Exit Function
              
        Else
                
            Set adofldUON1 = AdoRes.Fields("UON1")
            Set adofldUON2 = AdoRes.Fields("UON2")
            Set adofldPorcen = AdoRes.Fields("PORCEN")
            Set mCol = Nothing
            Set mCol = New Collection
            Set oItem = Nothing
            
            If UsarIndice Then
                
                lIndice = 0
        
                While Not AdoRes.eof
                    ' Dejamos en manos del programador cuando asignar la conexion al obejto usuario
                    Me.Add oItem, adofldUON1.Value, adofldUON2.Value, adofldPorcen.Value, , lIndice
                    dSuma = dSuma + adofldPorcen.Value
                    AdoRes.MoveNext
                    lIndice = lIndice + 1
                Wend
            
            Else
                       
                While Not AdoRes.eof
                    ' Dejamos en manos del programador cuando asignar la conexion al obejto usuario
                    Me.Add oItem, adofldUON1.Value, adofldUON2.Value, adofldPorcen.Value
                    dSuma = dSuma + adofldPorcen.Value
                    AdoRes.MoveNext
                Wend
                
            End If
        
            CargarTodasLasDistribucionesGrupo = dSuma
            AdoRes.Close
            Set adofldUON1 = Nothing
            Set adofldUON2 = Nothing
            Set adofldPorcen = Nothing
            Set AdoRes = Nothing
          
        End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If Err.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CDistItemsNivel2", "CargarTodasLasDistribucionesGrupo", Err, Erl)
      Exit Function
   End If

End Function

Public Function Add(ByVal oItem As CItem, ByVal UON1 As String, ByVal UON2 As String, ByVal Porcentaje As Double, Optional ByVal Den As Variant, Optional ByVal varIndice As Variant) As CDistItemNivel2
    'create a new object
    Dim sCod As String
    Dim objnewmember As CDistItemNivel2
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New CDistItemNivel2
      
    Set objnewmember.Conexion = mvarConexion
    Set objnewmember.Item = oItem
    objnewmember.CodUON1 = UON1
    objnewmember.CodUON2 = UON2
    
    If IsMissing(Den) Then
        objnewmember.Den = Null
    Else
        objnewmember.Den = Den
    End If
    If IsMissing(UON2) Then
        objnewmember.CodUON2 = Null
    Else
        objnewmember.CodUON2 = UON2
    End If
    
    objnewmember.Porcentaje = Porcentaje
    
    If Not IsMissing(varIndice) And Not IsNull(varIndice) Then
        objnewmember.Indice = varIndice
        mCol.Add objnewmember, CStr(varIndice)
    Else
        
        sCod = UON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(UON1))
        sCod = sCod & UON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(UON2))
        mCol.Add objnewmember, CStr(sCod)
    End If
    
    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If Err.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CDistItemsNivel2", "Add", Err, Erl)
      Exit Function
   End If

End Function

Public Sub Remove(vntIndexKey As Variant)
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
        mCol.Remove vntIndexKey
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If Err.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CDistItemsNivel2", "Remove", Err, Erl)
      Exit Sub
   End If
    
End Sub


Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
     Set NewEnum = mCol.[_NewEnum]
End Property


Private Sub Class_Initialize()
   

    Set mCol = New Collection
End Sub


Private Sub Class_Terminate()
    'AdjDeArtroys collection when this class is terminated
    Set mCol = Nothing
    Set mvarConexion = Nothing
End Sub

Public Function BorrarEnModoIndice(ByVal iIndice As Integer)
Dim IndFor As Integer
Dim oDist As CDistItemNivel2

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    For IndFor = iIndice To mCol.Count - 2
            
        mCol.Remove (CStr(IndFor))
        Set oDist = mCol.Item(CStr(IndFor + 1))
        mCol.Add oDist, CStr(IndFor)
        Set oDist = Nothing
        
    Next IndFor
    
    mCol.Remove CStr(IndFor)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If Err.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CDistItemsNivel2", "BorrarEnModoIndice", Err, Erl)
      Exit Function
   End If
    
End Function

''' <summary>Devuelve si la integracion en esa la empresa/s es de solo entrada</summary>
''' <remarks>Llamada desde: frmproce</remarks>
''' <revision>epb 15/04/2015</revision>
Public Function IntegracionArtSoloEntrada() As Boolean
Dim sConsulta As String
Dim oUON2 As CDistItemNivel2
Dim adoRecordset As adodb.Recordset

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

    Set adoRecordset = New adodb.Recordset
    IntegracionArtSoloEntrada = False
    For Each oUON2 In mCol
        sConsulta = "SELECT TE.ACTIVA, TE.SENTIDO FROM UON2 WITH(NOLOCK) "
        sConsulta = sConsulta & "      LEFT JOIN EMP ON UON2.EMPRESA=EMP.ID LEFT JOIN ERP_SOCIEDAD ES ON ES.SOCIEDAD=EMP.SOCIEDAD"
        sConsulta = sConsulta & "      LEFT JOIN TABLAS_INTEGRACION_ERP TE ON TE.ERP=ES.ERP AND TE.TABLA=7"
        sConsulta = sConsulta & "      WHERE UON2.UON1='" & DblQuote(oUON2.CodUON1) & "' AND UON2.COD='" & DblQuote(oUON2.CodUON2) & "'"
        sConsulta = sConsulta & " union SELECT TE.ACTIVA, TE.SENTIDO FROM UON1 WITH(NOLOCK) "
        sConsulta = sConsulta & "      LEFT JOIN EMP ON UON1.EMPRESA=EMP.ID LEFT JOIN ERP_SOCIEDAD ES ON ES.SOCIEDAD=EMP.SOCIEDAD"
        sConsulta = sConsulta & "      LEFT JOIN TABLAS_INTEGRACION_ERP TE ON TE.ERP=ES.ERP AND TE.TABLA=7"
        sConsulta = sConsulta & "      WHERE UON1.COD='" & DblQuote(oUON2.CodUON1) & "'"
        adoRecordset.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    
          While Not adoRecordset.eof
              
              If NullToDbl0(adoRecordset("ACTIVA").Value) = 1 And NullToDbl0(adoRecordset("SENTIDO").Value) = 2 Then 'SOLO ENTRADA
                  IntegracionArtSoloEntrada = True
                  adoRecordset.Close
                  Set adoRecordset = Nothing
                  Exit Function
              End If
              adoRecordset.MoveNext
          Wend
          
        adoRecordset.Close
    Next
    Set adoRecordset = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If Err.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CDistItemsNivel2", "IntegracionArtSoloEntrada", Err, Erl)
      Exit Function
   End If
  
End Function









