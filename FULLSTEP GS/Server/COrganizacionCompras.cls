VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "COrganizacionCompras"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

''' Variables privadas con la informacion de una ORGANIZACION de COMPRAS
Private m_vId As Variant
Private m_vIndice As Variant
Private m_sCod As String
Private m_sDen As String

''' Conexion
Private m_oConexion As CConexion

Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum

Friend Property Set Conexion(ByVal con As CConexion)
Set m_oConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = m_oConexion
End Property

Public Property Get Cod() As String
    Cod = m_sCod
End Property

Public Property Let Cod(ByVal Data As String)
    m_sCod = Data
End Property

Public Property Get Den() As String
    Den = m_sDen
End Property

Public Property Let Den(ByVal Data As String)
    m_sDen = Data
End Property

Public Property Get Id() As Variant
    Id = m_vId
End Property

Public Property Let Id(ByVal Data As Variant)
    m_vId = Data
End Property

Public Property Let Indice(ByVal varIndice As Variant)
    m_vIndice = varIndice
End Property

Public Property Get Indice() As Variant
    Indice = m_vIndice
End Property

''' <summary>Devuelve la denominación de una organización de compras</summary>
''' <param name="sCod">Cod. organización</param>
''' <returns>La denominación</returns>
''' <remarks>Llamada desde: FSGSClient</remarks>
''' <revision>LTG 11/01/2012</revision>

Public Function DevolverOrganizacionCompras(ByVal sCod As String) As String

    Dim sConsulta As String
    Dim rs As adodb.Recordset

    '********* Precondicion **************************************
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "COrdenEntrega.DevolverHistoriaDeLosEstados", "No se ha establecido la conexion"
        Exit Function
    End If
    '*************************************************************
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

    sConsulta = "SELECT COD,DEN FROM ORGCOMPRAS WITH (NOLOCK) WHERE COD ='" & sCod & "' ORDER BY DEN"
    
    Set rs = New adodb.Recordset
              
    rs.CursorLocation = adUseClient

    rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
             
    If Not rs.eof Then
        DevolverOrganizacionCompras = rs.Fields("DEN").Value
    End If
    
    rs.Close
    Set rs = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "COrganizacionCompras", "DevolverOrganizacionCompras", ERR, Erl)
        Exit Function
    End If
End Function


Private Sub Class_Terminate()
    
    Set m_oConexion = Nothing
    
End Sub

