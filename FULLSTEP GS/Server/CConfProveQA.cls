VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CConfProveQA"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
Option Base 0

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

Private m_oConexion As CConexion
Private m_adores As adodb.Recordset
Private m_adoComm As adodb.Command
Private m_oConexionServer As CConexionDeUseServer

Private m_variablesCalChequeadas As String
Private m_unidadesOrgChequeadas As String
Private m_puntuacionCalificacion As Long
Private m_calificacionesProveedores As Long

Public Property Let variablesCalChequeadas(ByVal vData As String)
    m_variablesCalChequeadas = vData
End Property

Public Property Get variablesCalChequeadas() As String
    variablesCalChequeadas = m_variablesCalChequeadas
End Property

Public Property Let unidadesOrgChequeadas(ByVal vData As String)
    m_unidadesOrgChequeadas = vData
End Property

Public Property Get unidadesOrgChequeadas() As String
    unidadesOrgChequeadas = m_unidadesOrgChequeadas
End Property

Public Property Let puntuacionCalificacion(ByVal vData As Long)
    m_puntuacionCalificacion = vData
End Property

Public Property Get puntuacionCalificacion() As Long
    puntuacionCalificacion = m_puntuacionCalificacion
End Property

Public Property Let calificacionesProveedores(ByVal vData As Long)
    m_calificacionesProveedores = vData
End Property

Public Property Get calificacionesProveedores() As Long
    calificacionesProveedores = m_calificacionesProveedores
End Property

Friend Property Set Conexion(ByVal con As CConexion)
Set m_oConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = m_oConexion
End Property

Private Sub Class_Terminate()
    
    Set m_adores = Nothing
    Set m_adoComm = Nothing
    Set m_oConexion = Nothing
    
End Sub

Friend Property Set ConexionServer(ByVal con As CConexionDeUseServer)
    Set m_oConexionServer = con
End Property

Friend Property Get ConexionServer() As CConexionDeUseServer
    Set ConexionServer = m_oConexionServer
End Property

Public Function DevolverUsuCod(ByVal Cod As String) As Boolean
Dim sConsulta As String
Dim bEsNuevo As Boolean
Dim AdoRes As New adodb.Recordset

'********Precondición******************************************************************************************************************
If m_oConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CConfProveQA.DevolverUsuCod", "No se ha establecido la conexion"
End If
'**************************************************************************************************************************************

sConsulta = "SELECT USU FROM CONF_SELPROVE_QA WITH(NOLOCK) WHERE USU='" & DblQuote(Cod) & "'"
AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly

If AdoRes.eof Then
    bEsNuevo = True
Else
    bEsNuevo = False
End If
AdoRes.Close
Set AdoRes = Nothing
            
DevolverUsuCod = bEsNuevo
    
End Function


Public Function AnyadirConfUsuario(ByVal CodUsu As String, ByVal filtroValCal As String, ByVal filtroUNQA As String, ByVal puntCalif As Long, ByVal califProveedores As Long) As TipoErrorSummit
    Dim sConsulta As String
    Dim TESError As TipoErrorSummit
    
    '********Precondición******************************************************************************************************************
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CConfProveQA.AnyadirConfUsuario", "No se ha establecido la conexion"
    End If
    '**************************************************************************************************************************************

    TESError.NumError = TESnoerror

    On Error GoTo ERROR
    
    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"

    sConsulta = "INSERT INTO CONF_SELPROVE_QA (USU, FILTROVARCAL,FILTROUNQA,PUNTCALIF,CALIFICACIONESPROVEEDORES) VALUES ("
    sConsulta = sConsulta & "'" & DblQuote(CodUsu) & "','" & DblQuote(filtroValCal) & "','" & DblQuote(filtroUNQA) & "'," & puntCalif & "," & califProveedores & ")"
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR
    
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"

    AnyadirConfUsuario = TESError

    Exit Function
Salir:
    Set m_adores = Nothing
    AnyadirConfUsuario = TESError
    Exit Function
ERROR:
    If m_oConexion.ADOCon.Errors.Count > 0 Or ERR.Number > 0 Then TESError = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
    Resume Salir
    
End Function


Public Function ModificarConfUsuario(ByVal CodUsu As String, ByVal filtroValCal As String, ByVal filtroUNQA As String, ByVal puntCalif As Long, ByVal califProveedores As Long) As TipoErrorSummit
    Dim sConsulta As String
    Dim TESError As TipoErrorSummit
    
    '********Precondición******************************************************************************************************************
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CConfProveQA.ModificarConfUsuario", "No se ha establecido la conexion"
    End If
    '**************************************************************************************************************************************

    TESError.NumError = TESnoerror

    On Error GoTo ERROR
    
    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"

    sConsulta = "UPDATE CONF_SELPROVE_QA SET FILTROVARCAL='" & DblQuote(filtroValCal) & "',FILTROUNQA='" & DblQuote(filtroUNQA) & "',PUNTCALIF=" & puntCalif & ",CALIFICACIONESPROVEEDORES=" & califProveedores
    sConsulta = sConsulta & " WHERE USU='" & DblQuote(CodUsu) & "'"
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR
    
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"

    ModificarConfUsuario = TESError

    Exit Function
Salir:
    Set m_adores = Nothing
    ModificarConfUsuario = TESError
    Exit Function
ERROR:
    If m_oConexion.ADOCon.Errors.Count > 0 Or ERR.Number > 0 Then TESError = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
    Resume Salir
    
End Function

Public Sub ObtenerConfiguracionUsuario(ByVal CodUsu As String)
    Dim sConsulta As String
    Dim AdoRes As New adodb.Recordset
    
    '********Precondición******************************************************************************************************************
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CConfProveQA.ObtenerConfiguracionUsuario", "No se ha establecido la conexion"
    End If
    '**************************************************************************************************************************************
    
    Set AdoRes = New adodb.Recordset
    
    sConsulta = "SELECT FILTROVARCAL,FILTROUNQA,PUNTCALIF,CALIFICACIONESPROVEEDORES FROM CONF_SELPROVE_QA WITH(NOLOCK) WHERE USU='" & DblQuote(CodUsu) & "'"
    AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly

    If Not AdoRes.eof Then
        m_variablesCalChequeadas = AdoRes("FILTROVARCAL")
        m_unidadesOrgChequeadas = AdoRes("FILTROUNQA")
        m_puntuacionCalificacion = AdoRes("PUNTCALIF")
        m_calificacionesProveedores = AdoRes("CALIFICACIONESPROVEEDORES")
    End If
    AdoRes.Close
    Set AdoRes = Nothing
End Sub
