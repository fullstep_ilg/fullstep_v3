VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CConfVistaGrupoProv"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CConfVistaGrupoProv **********************************
'*             Autor : Mertxe Mart�n
'*             Creada : 28/02/2002
'***************************************************************

'@@@@
Option Explicit

Implements IBaseDatos

Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum

Private m_oConfVistaGr As CConfVistaGrupo
Private m_sUsu As String
Private m_iVista As Integer

Private m_sProv As String
Private m_bVisible As TipoProvVisible
Private m_sDenProve As String

Private m_oConexion As CConexion
Private m_vIndice As Variant
Private m_adores As adodb.Recordset

Public Property Let Vista(ByVal iVista As Integer)
    m_iVista = iVista
End Property

Public Property Get Vista() As Integer
    Vista = m_iVista
End Property

Public Property Set ConfVistaGrupo(ByVal oProce As CConfVistaGrupo)
    Set m_oConfVistaGr = oProce
End Property
Public Property Get ConfVistaGrupo() As CConfVistaGrupo
    Set ConfVistaGrupo = m_oConfVistaGr
End Property

Public Property Let Proveedor(ByVal dato As String)
    m_sProv = dato
End Property
Public Property Get Proveedor() As String
    Proveedor = m_sProv
End Property

Public Property Let Visible(ByVal dato As TipoProvVisible)
    m_bVisible = dato
End Property

Public Property Get Visible() As TipoProvVisible
    Visible = m_bVisible
End Property

Public Property Let DenProve(ByVal dato As String)
    m_sDenProve = dato
End Property

Public Property Get DenProve() As String
    DenProve = m_sDenProve
End Property

Friend Property Set Conexion(ByVal con As CConexion)
    Set m_oConexion = con
End Property

Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property

Public Property Let Indice(ByVal vIndice As Variant)
    m_vIndice = vIndice
End Property
Public Property Get Indice() As Variant
    Indice = m_vIndice
End Property

Private Function IBaseDatos_AnyadirABaseDatos() As TipoErrorSummit
Dim sConsulta As String
Dim TESError As TipoErrorSummit

'********* Precondicion *******************
If m_oConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CConfVistaGrupoProv.AnyadirABAseDatos", "No se ha establecido la conexion"
End If
'******************************************

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------


If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:

    TESError.NumError = TESnoerror
    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"

    sConsulta = "INSERT INTO CONF_VISTAS_GRUPO_PROVE (ANYO,GMN1,PROCE,GRUPO,PROVE,USU,VISTA,VISIBLE)"
    sConsulta = sConsulta & " VALUES (" & m_oConfVistaGr.Grupo.Proceso.Anyo & ",'" & DblQuote(m_oConfVistaGr.Grupo.Proceso.GMN1Cod) & "',"
    sConsulta = sConsulta & m_oConfVistaGr.Grupo.Proceso.Cod & "," & (m_oConfVistaGr.Grupo.Id)
    sConsulta = sConsulta & ",'" & DblQuote(m_sProv) & "','" & DblQuote(m_oConfVistaGr.Usuario) & "'," & m_oConfVistaGr.Vista & "," & m_bVisible & ")"

    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    IBaseDatos_AnyadirABaseDatos = TESError
    Exit Function

Error_Cls:

    IBaseDatos_AnyadirABaseDatos = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CConfVistaGrupoProv", "IBaseDatos_AnyadirABaseDatos", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If

End Function


Private Function IBaseDatos_CambiarCodigo(CodigoNuevo As Variant) As TipoErrorSummit

End Function

Private Sub IBaseDatos_CancelarEdicion()

End Sub


Private Function IBaseDatos_ComprobarExistenciaEnBaseDatos() As Boolean
    Dim sConsulta As String
    Dim AdoRes As New adodb.Recordset

'    ******************* Precondicion ********************
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CConfVistaGrupoProv.ComprobarExistenciaEnBaseDatos", "No se ha establecido la conexion"
    End If
'    *****************************************************

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

    sConsulta = "SELECT * FROM CONF_VISTAS_GRUPO_PROVE WITH (NOLOCK) WHERE ANYO=" & m_oConfVistaGr.Grupo.Proceso.Anyo & " AND GMN1='" & DblQuote(m_oConfVistaGr.Grupo.Proceso.GMN1Cod) & "'"
    sConsulta = sConsulta & " AND PROCE=" & m_oConfVistaGr.Grupo.Proceso.Cod & " AND GRUPO=" & DblQuote(m_oConfVistaGr.Grupo.Id)
    sConsulta = sConsulta & " AND USU='" & DblQuote(m_oConfVistaGr.Usuario) & "' AND VISTA=" & m_oConfVistaGr.Vista
    sConsulta = sConsulta & " AND PROVE='" & DblQuote(m_sProv) & "'"

    Set AdoRes = New adodb.Recordset
    AdoRes.CursorLocation = adUseServer
    AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    If AdoRes.eof Then
        IBaseDatos_ComprobarExistenciaEnBaseDatos = False
    Else
        IBaseDatos_ComprobarExistenciaEnBaseDatos = True
    End If

    AdoRes.Close
    Set AdoRes = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CConfVistaGrupoProv", "IBaseDatos_ComprobarExistenciaEnBaseDatos", ERR, Erl)
      Exit Function
   End If

End Function


Private Function IBaseDatos_EliminarDeBaseDatos() As TipoErrorSummit
Dim sConsulta As String
Dim TESError As TipoErrorSummit

    '''Precondicion
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CConfVistaGrupoProv.EliminarDeBaseDatos", "No se ha establecido la conexion"
    End If
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:

    TESError.NumError = TESnoerror

    sConsulta = "DELETE FROM CONF_VISTAS_GRUPO_PROVE WHERE ANYO=" & m_oConfVistaGr.Grupo.Proceso.Anyo & " AND GMN1='" & DblQuote(m_oConfVistaGr.Grupo.Proceso.GMN1Cod) & "'"
    sConsulta = sConsulta & " AND PROCE=" & m_oConfVistaGr.Grupo.Proceso.Cod & " AND GRUPO='" & m_oConfVistaGr.Grupo.Codigo & "'"
    sConsulta = sConsulta & " AND USU='" & DblQuote(m_oConfVistaGr.Usuario) & "' AND VISTA=" & m_oConfVistaGr.Vista
    sConsulta = sConsulta & " AND PROVE='" & DblQuote(m_sProv) & "'"

    m_oConexion.ADOCon.Execute sConsulta

    IBaseDatos_EliminarDeBaseDatos = TESError

    Exit Function

Error_Cls:
    IBaseDatos_EliminarDeBaseDatos = basErrores.TratarError(m_oConexion.ADOCon.Errors)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CConfVistaGrupoProv", "IBaseDatos_EliminarDeBaseDatos", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If
End Function


Private Function IBaseDatos_FinalizarEdicionEliminando() As TipoErrorSummit

End Function


Private Function IBaseDatos_FinalizarEdicionModificando() As TipoErrorSummit
Dim TESError As TipoErrorSummit
Dim sConsulta As String



If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
    TESError.NumError = TESnoerror

    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"

    sConsulta = "UPDATE CONF_VISTAS_GRUPO_PROVE SET "
    sConsulta = sConsulta & " VISIBLE=" & m_bVisible
    sConsulta = sConsulta & " WHERE ANYO=" & m_oConfVistaGr.Grupo.Proceso.Anyo & " AND GMN1='" & DblQuote(m_oConfVistaGr.Grupo.Proceso.GMN1Cod) & "'"
    sConsulta = sConsulta & " AND PROCE=" & m_oConfVistaGr.Grupo.Proceso.Cod
    sConsulta = sConsulta & " AND GRUPO=" & (m_oConfVistaGr.Grupo.Id) & " AND PROVE='" & m_sProv & "' "
    sConsulta = sConsulta & " AND USU='" & DblQuote(m_oConfVistaGr.Usuario) & "' AND VISTA=" & m_oConfVistaGr.Vista

    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"

    IBaseDatos_FinalizarEdicionModificando = TESError

    Exit Function

Error_Cls:
    IBaseDatos_FinalizarEdicionModificando = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"

    On Error Resume Next
    If Not m_adores Is Nothing Then
        m_adores.Close
        Set m_adores = Nothing
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CConfVistaGrupoProv", "IBaseDatos_FinalizarEdicionModificando", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If

End Function

Private Function IBaseDatos_IniciarEdicion(Optional ByVal Bloquear As Boolean, Optional ByVal UsuarioBloqueo As String) As TipoErrorSummit

End Function

Private Sub Class_Terminate()
On Error GoTo Error_Cls:
    Set m_oConexion = Nothing
    Exit Sub
Error_Cls:
    Resume Next
End Sub

Public Function VistaAnyadirABaseDatos(ByVal iVista As Integer, ByVal sUsuario As String) As TipoErrorSummit
Dim sConsulta As String
Dim TESError As TipoErrorSummit

'********* Precondicion *******************
If m_oConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CConfVistaGrupoProv.AnyadirABAseDatos", "No se ha establecido la conexion"
End If
'******************************************

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:

    TESError.NumError = TESnoerror

    sConsulta = "INSERT INTO CONF_VISTAS_GRUPO_PROVE (ANYO,GMN1,PROCE,GRUPO,PROVE,USU,VISTA,VISIBLE)"
    sConsulta = sConsulta & " VALUES (" & m_oConfVistaGr.Grupo.Proceso.Anyo & ",'" & DblQuote(m_oConfVistaGr.Grupo.Proceso.GMN1Cod) & "',"
    sConsulta = sConsulta & m_oConfVistaGr.Grupo.Proceso.Cod & "," & (m_oConfVistaGr.Grupo.Id)
    sConsulta = sConsulta & ",'" & DblQuote(m_sProv) & "','" & DblQuote(sUsuario) & "'," & iVista & "," & m_bVisible & ")"

    m_oConexion.ADOCon.Execute sConsulta

    VistaAnyadirABaseDatos = TESError
    Exit Function

Error_Cls:

    VistaAnyadirABaseDatos = basErrores.TratarError(m_oConexion.ADOCon.Errors)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CConfVistaGrupoProv", "VistaAnyadirABaseDatos", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If

End Function





