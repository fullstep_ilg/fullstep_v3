VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CCondicionesEnlace"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum

Private m_Col As Collection
Private m_oConexion As CConexion
Private m_bEOF As Boolean

Public Property Get eof() As Boolean
    eof = m_bEOF
End Property
Friend Property Let eof(ByVal b As Boolean)
    m_bEOF = b
End Property

Public Property Get Item(vntIndexKey As Variant) As CCondicionEnlace
On Error GoTo NoSeEncuentra:

    Set Item = m_Col(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Friend Property Set Conexion(ByVal con As CConexion)
Set m_oConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = m_oConexion
End Property

Public Property Get Count() As Long
    If m_Col Is Nothing Then
        Count = 0
    Else
         Count = m_Col.Count
    End If
End Property

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
     Set NewEnum = m_Col.[_NewEnum]
End Property

Public Function Add(ByVal lId As Long, ByVal sCod As String, ByVal lEnlace As Long, Optional ByVal iTipoCampo As Integer, Optional ByVal lCampo As Long, Optional ByVal sOperador As String, Optional ByVal iTipoValor As Integer, Optional ByVal lCampoValor As Long, Optional ByVal vValor As Variant, Optional ByVal dtFecAct As Variant, Optional ByVal vIndice As Variant, Optional ByVal lPrecondicion As Long, Optional ByVal iOrigen As OrigenCondicionEnlace = OrigenCondicionEnlace.Enlace, Optional ByVal sMoneda As String) As CCondicionEnlace
    
    'create a new object
    Dim objnewmember As CCondicionEnlace
    
    Set objnewmember = New CCondicionEnlace
    With objnewmember
        Set .Conexion = m_oConexion
        .Id = lId
        .Cod = sCod
        .Origen = iOrigen
        .Enlace = lEnlace
        .Precondicion = lPrecondicion
        .TipoCampo = iTipoCampo
        .Campo = lCampo
        .Operador = sOperador
        .TipoValor = iTipoValor
        .CampoValor = lCampoValor
        .Valor = vValor
        .FecAct = dtFecAct
        .Moneda = sMoneda
        
        If Not IsMissing(vIndice) And Not IsNull(vIndice) Then
            .Indice = vIndice
            m_Col.Add objnewmember, CStr(vIndice)
        Else
            m_Col.Add objnewmember, CStr(lId)
        End If
    
    End With
    
    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing


End Function

Public Sub AddCondicion(ByVal oCondicion As CCondicionEnlace, Optional ByVal vIndice As Variant)
    If Not IsMissing(vIndice) And Not IsNull(vIndice) Then
        oCondicion.Indice = vIndice
        m_Col.Add oCondicion, CStr(vIndice)
    Else
        m_Col.Add oCondicion, CStr(oCondicion.Id)
    End If
End Sub

Public Sub Remove(vntIndexKey As Variant)
On Error GoTo ERROR

    m_Col.Remove vntIndexKey

ERROR:

End Sub


Private Sub Class_Initialize()
    Set m_Col = New Collection
End Sub


Private Sub Class_Terminate()
    'Proceroys collection when this class is terminated
    On Error Resume Next
    Set m_Col = Nothing
    Set m_oConexion = Nothing
    
End Sub

Public Function DevolverCondiciones(ByVal lIDEnlace As Long) As adodb.Recordset
Dim oadorecordset As adodb.Recordset
Dim sql As String
    
    sql = "SELECT ID, COD, ENLACE, TIPO_CAMPO, CAMPO, OPERADOR, TIPO_VALOR, CAMPO_VALOR, VALOR_TEXT, VALOR_NUM, VALOR_FEC, VALOR_BOOL, FECACT, MON " _
                & "FROM PM_ENLACE_CONDICIONES WITH (NOLOCK) WHERE ENLACE = " & lIDEnlace
        
    Set oadorecordset = New adodb.Recordset
    oadorecordset.CursorLocation = adUseClient
    oadorecordset.Open sql, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    oadorecordset.ActiveConnection = Nothing
        
    Set DevolverCondiciones = oadorecordset
    
End Function

Public Sub CargarCondiciones(ByVal lIDEnlace As Long)
    Dim oRS As adodb.Recordset
    Dim vValor As Variant

    Set oRS = DevolverCondiciones(lIDEnlace)
    While Not oRS.eof
        If Not IsNull(oRS.Fields("VALOR_TEXT").Value) Then
            vValor = NullToStr(oRS.Fields("VALOR_TEXT").Value)
        ElseIf Not IsNull(oRS.Fields("VALOR_NUM").Value) Then
            vValor = NullToDbl0(oRS.Fields("VALOR_NUM").Value)
        ElseIf Not IsNull(oRS.Fields("VALOR_FEC").Value) Then
            vValor = oRS.Fields("VALOR_FEC").Value
        ElseIf Not IsNull(oRS.Fields("VALOR_BOOL").Value) Then
            vValor = oRS.Fields("VALOR_BOOL").Value
        End If
        Add NullToDbl0(oRS("ID").Value), NullToStr(oRS("COD").Value), NullToDbl0(oRS("ENLACE").Value), NullToDbl0(oRS("TIPO_CAMPO").Value), NullToDbl0(oRS("CAMPO").Value), NullToStr(oRS("OPERADOR").Value), NullToDbl0(oRS("TIPO_VALOR").Value), NullToDbl0(oRS("CAMPO_VALOR").Value), vValor, oRS("FECACT").Value, , , , NullToStr(oRS("MON").Value)
        oRS.MoveNext
    Wend
    oRS.Close
    Set oRS = Nothing
End Sub

Public Sub CargarCondicionesPrecondicion(ByVal lIdPrecondicion As Long)
    Dim sql As String
    Dim oRS As adodb.Recordset
    Dim vValor As Variant
        
    sql = "SELECT ID, COD, ACCION_PRECOND, TIPO_CAMPO, CAMPO, OPERADOR, TIPO_VALOR, CAMPO_VALOR, VALOR_TEXT, VALOR_NUM, VALOR_FEC, VALOR_BOOL, FECACT , MON " _
            & "FROM PM_ACCION_CONDICIONES WITH (NOLOCK) WHERE ACCION_PRECOND = " & lIdPrecondicion
    Set oRS = New adodb.Recordset
    oRS.CursorLocation = adUseClient
    oRS.Open sql, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    While Not oRS.eof
        If Not IsNull(oRS.Fields("VALOR_TEXT").Value) Then
            vValor = NullToStr(oRS.Fields("VALOR_TEXT").Value)
        ElseIf Not IsNull(oRS.Fields("VALOR_NUM").Value) Then
            vValor = NullToDbl0(oRS.Fields("VALOR_NUM").Value)
        ElseIf Not IsNull(oRS.Fields("VALOR_FEC").Value) Then
            vValor = oRS.Fields("VALOR_FEC").Value
        ElseIf Not IsNull(oRS.Fields("VALOR_BOOL").Value) Then
            vValor = oRS.Fields("VALOR_BOOL").Value
        End If
        Add NullToDbl0(oRS("ID").Value), NullToStr(oRS("COD").Value), 0, NullToDbl0(oRS("TIPO_CAMPO").Value), NullToDbl0(oRS("CAMPO").Value), NullToStr(oRS("OPERADOR").Value), NullToDbl0(oRS("TIPO_VALOR").Value), NullToDbl0(oRS("CAMPO_VALOR").Value), vValor, oRS("FECACT").Value, , NullToDbl0(oRS("ACCION_PRECOND").Value), OrigenCondicionEnlace.Precondicion, NullToStr(oRS("MON").Value)
        oRS.MoveNext
    Wend
    oRS.Close
    Set oRS = Nothing
End Sub

Public Function EliminarCondicionesEnlace(ByVal lIDEnlace As Long) As TipoErrorSummit
Dim TESError As TipoErrorSummit
Dim btrans As Boolean
Dim sConsulta As String

TESError.NumError = TESnoerror

On Error GoTo ERROR:
    
    m_oConexion.ADOCon.Execute "BEGIN TRAN"
    m_oConexion.ADOCon.Execute "SET XACT_ABORT OFF"
    btrans = True
    
    sConsulta = "DELETE FROM PM_ENLACE_CONDICIONES WHERE ENLACE = " & lIDEnlace
     
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then
        GoTo ERROR
    End If
    m_oConexion.ADOCon.Execute "COMMIT TRAN"
    btrans = False
    
    EliminarCondicionesEnlace = TESError
    
    Exit Function
    
ERROR:
    
    If m_oConexion.ADOCon.Errors.Count > 0 Then
        EliminarCondicionesEnlace = basErrores.TratarError(m_oConexion.ADOCon.Errors)
        If btrans Then
            m_oConexion.ADOCon.Execute "ROLLBACK TRAN"
            btrans = False
        End If
        Exit Function
    End If

End Function

Public Function DevolverCondicionesPrecondicion(ByVal lIdPrecondicion As Long) As adodb.Recordset
Dim oadorecordset As adodb.Recordset
Dim sql As String
    
    sql = "SELECT ID, COD, ACCION_PRECOND, TIPO_CAMPO, CAMPO, OPERADOR, TIPO_VALOR, CAMPO_VALOR, VALOR_TEXT, VALOR_NUM, VALOR_FEC, VALOR_BOOL, FECACT " _
                & "FROM PM_ACCION_CONDICIONES WHERE ACCION_PRECOND = " & lIdPrecondicion
        
    Set oadorecordset = New adodb.Recordset
    oadorecordset.CursorLocation = adUseClient
    oadorecordset.Open sql, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    oadorecordset.ActiveConnection = Nothing
        
    Set DevolverCondicionesPrecondicion = oadorecordset
    
End Function

Public Function ObtenerCod(ByVal lEnlace As Long) As Long
Dim sConsulta As String
Dim AdoRes As adodb.Recordset

    sConsulta = "SELECT max(case ISNUMERIC(RIGHT(COD,LEN(COD)-1)) when 1 then CAST(RIGHT(COD,LEN(COD)-1) AS INTEGER) else 0 end) AS COD FROM PM_ENLACE_CONDICIONES "
    sConsulta = sConsulta & " WHERE ENLACE=" & lEnlace & " AND COD like 'X%'"
    
    Set AdoRes = New adodb.Recordset
    AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
    
    If IsNull(AdoRes.Fields("COD").Value) Then
        ObtenerCod = 1
    Else
        ObtenerCod = AdoRes.Fields("COD").Value + 1
    End If

    AdoRes.Close
    Set AdoRes = Nothing
    
End Function

Public Function ObtenerCodPrecondiciones(ByVal lIdPrecondicion As Long) As Long
Dim sConsulta As String
Dim AdoRes As adodb.Recordset

    sConsulta = "SELECT max(case ISNUMERIC(RIGHT(COD,LEN(COD)-1)) when 1 then CAST(RIGHT(COD,LEN(COD)-1) AS INTEGER) else 0 end) AS COD FROM PM_ACCION_CONDICIONES "
    sConsulta = sConsulta & " WHERE ACCION_PRECOND =" & lIdPrecondicion & " AND COD like 'X%'"
    
    Set AdoRes = New adodb.Recordset
    AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
    
    If IsNull(AdoRes.Fields("COD").Value) Then
        ObtenerCodPrecondiciones = 1
    Else
        ObtenerCodPrecondiciones = AdoRes.Fields("COD").Value + 1
    End If

    AdoRes.Close
    Set AdoRes = Nothing
    
End Function
