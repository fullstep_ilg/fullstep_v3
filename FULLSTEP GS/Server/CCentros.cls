VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CCentros"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
End Enum
'local variable to hold collection
Private mCol As Collection
Private mvarConexion As CConexion
Private mvarEOF As Boolean

Public Sub Add(ByVal sCod As Variant, ByVal sDen As Variant, Optional ByVal varIndice As Variant, Optional ByVal varAlmacen As Variant)
    
    'create a new object
    Dim objnewmember As CCentro
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New CCentro

    If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:

    'set the properties passed into the method
    objnewmember.Cod = sCod
    
    objnewmember.Den = "" & sDen
    
    If IsNull(varIndice) Or IsMissing(varIndice) Then
        objnewmember.Almacen = ""
    Else
        objnewmember.Almacen = varAlmacen
    End If
    
    Set objnewmember.Conexion = mvarConexion
    
    If IsNull(varIndice) Or IsMissing(varIndice) Then
        mCol.Add objnewmember, CStr(sCod)
    Else
        mCol.Add objnewmember, CStr(varIndice)
    End If


    'return the object created
    Set objnewmember = Nothing
    Exit Sub
Error_Cls:
    'CargarCentrosPorArticulo saca informacion tanto de centro como de almacen. Por ello puede
    '   ocurrir que tenga como parte de la coleccion dos registros del mismo centro uno con almacen
    '   el otro sin almacen.
    If IsNull(varIndice) Or IsMissing(varIndice) Then
        mCol.Item(sCod).Almacen = "" & varAlmacen
    End If
    Resume Next
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
ERROR:
    If ERR.Number <> 0 Then
        'Call g_oErrores.TratarError("Class Module", "CCentros", "Add", ERR, Erl)
        GoTo Error_Cls
        Exit Sub
    End If
End Sub

''' <summary>Devuelve los centros de un item</summary>
''' <param name="Empresa">Empresa</param>
''' <param name="OrgCompras">Organización de Compras</param>
''' <param name="Anyo">Anyo</param>
''' <param name="GMN1">GMN1</param>
''' <param name="Proce">Cod. del proceso</param>
''' <param name="Item">Id del item</param>
''' <param name="Empresa">Empresa</param>
''' <param name="OrgCompras">Organización de Compras</param>
''' <returns>Recordset con los centros</returns>
''' <remarks>Llamada desde: GSClient</remarks>
''' <revision>LTG 02/01/2012</revision>

Public Function CargarCentrosPorItem(ByVal Anyo As Integer, ByVal GMN1 As String, ByVal Proce As Long, ByVal Item As Long, Optional ByVal OrgCompras As String = "") As adodb.Recordset
    Dim rs As adodb.Recordset
    Dim oCom As adodb.Command
    Dim oParam As adodb.Parameter
    
    If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
       
    Set oCom = New adodb.Command
    With oCom
        Set .ActiveConnection = mvarConexion.ADOCon
        .CommandType = adCmdStoredProc
        .CommandText = "FSEP_DEVOLVER_CENTROS_ITEM"
        Set oParam = .CreateParameter(Type:=adSmallInt, Direction:=adParamInput, Value:=Anyo)
        .Parameters.Append oParam
        Set oParam = .CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=50, Value:=StrToVbNULL(GMN1))
        .Parameters.Append oParam
        Set oParam = .CreateParameter(Type:=adInteger, Direction:=adParamInput, Value:=StrToVbNULL(Proce))
        .Parameters.Append oParam
        Set oParam = .CreateParameter(Type:=adInteger, Direction:=adParamInput, Value:=StrToVbNULL(Item))
        .Parameters.Append oParam
        If OrgCompras <> "" Then
            Set oParam = .CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=100, Value:=StrToVbNULL(OrgCompras))
            .Parameters.Append oParam
        End If
        
        Set rs = .Execute
    End With
    
    Set rs.ActiveConnection = Nothing
    Set CargarCentrosPorItem = rs

Salir:
    Set rs = Nothing
    Set oCom = Nothing
    Set oParam = Nothing
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CCentros", "CargarCentrosPorItem", ERR, Erl)
        Exit Function
    End If
End Function

''' <summary>Devuelve el centro y almacen en la creación de pedidos directos desde solicitudes para el item indicado</summary>
''' <param name="Anyo">Anyo</param>
''' <param name="GMN1">GMN1</param>
''' <param name="Proce">Cod. del proceso</param>
''' <param name="Item">Id del item</param>
''' <returns>Recordset con los centros</returns>
''' <remarks>Llamada desde: GSClient</remarks>
''' <revision>JBG 19/12/2012</revision>
Public Function CargarCentroYAlmacenPorItemDesdeSolicitud(ByVal Anyo As Integer, ByVal GMN1 As String, ByVal Proce As Long, ByVal Item As Long) As adodb.Recordset
    Dim rs As New adodb.Recordset
    Dim sConsulta As String
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sConsulta = "SELECT CL.VALOR_NUM AS ALMACEN, CL.VALOR_TEXT AS CENTRO, FC.TIPO_CAMPO_GS "
    sConsulta = sConsulta & " FROM ITEM I WITH (NOLOCK) "
    sConsulta = sConsulta & " INNER JOIN COPIA_CAMPO CC WITH (NOLOCK)ON CC.INSTANCIA=I.SOLICIT "
    sConsulta = sConsulta & " INNER JOIN INSTANCIA WITH(NOLOCK) ON CC.INSTANCIA = INSTANCIA.ID AND CC.NUM_VERSION = INSTANCIA.ULT_VERSION "
    sConsulta = sConsulta & " INNER JOIN FORM_CAMPO FC WITH (NOLOCK)ON FC.ID=CC.COPIA_CAMPO_DEF AND (TIPO_CAMPO_GS= " & TipoCampoGS.Centro & " OR TIPO_CAMPO_GS= " & TipoCampoGS.Almacen & ") "
    sConsulta = sConsulta & " INNER JOIN COPIA_LINEA_DESGLOSE CL WITH (NOLOCK) ON CL.CAMPO_HIJO=CC.ID AND CL.LINEA= I.LINEA_SOLICIT "
    sConsulta = sConsulta & " WHERE I.ANYO=" & Anyo & " AND I.GMN1_PROCE='" & DblQuote(GMN1) & "' AND I.PROCE=" & Proce & " AND I.ID=" & Item
    
    rs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
       
    Set rs.ActiveConnection = Nothing
    Set CargarCentroYAlmacenPorItemDesdeSolicitud = rs
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CCentros", "CargarCentroYAlmacenPorItemDesdeSolicitud", ERR, Erl)
        Exit Function
    End If
    
End Function

''' <summary>Devuelve los centros de un item no codificado, desde la UO en la que está distribuido</summary>
''' <param name="Anyo">Anyo</param>
''' <param name="GMN1">GMN1</param>
''' <param name="Proce">Cod. del proceso</param>
''' <param name="Item">Id del item</param>
''' <returns>Recordset con los centros</returns>
''' <remarks>Llamada desde: GSClient.frmPEDIDOS</remarks>
''' <revision>EPB 23/07/2012</revision>

Public Function CargarCentrosPorItemNoCodificado(ByVal Anyo As Integer, ByVal GMN1 As String, ByVal Proce As Long, ByVal Item As Long, Optional ByVal Cod As String) As adodb.Recordset
    Dim rs As adodb.Recordset
    Dim fldCentro As adodb.Field
    Dim fldDen As adodb.Field
    Dim oCom As adodb.Command
    Dim oParam As adodb.Parameter
        
    If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
       
    Set mCol = New Collection
    
    Set oCom = New adodb.Command
    With oCom
        Set .ActiveConnection = mvarConexion.ADOCon
        .CommandText = "FSEP_DEVOLVER_CENTROS_ITEM_NO_CODIFICADO"
        .CommandType = adCmdStoredProc
        
        Set oParam = .CreateParameter("ANYO", adSmallInt, adParamInput, , Anyo)
        .Parameters.Append oParam
        Set oParam = .CreateParameter("GMN1", adVarChar, adParamInput, 50, GMN1)
        .Parameters.Append oParam
        Set oParam = .CreateParameter("PROCE", adInteger, adParamInput, , Proce)
        .Parameters.Append oParam
        Set oParam = .CreateParameter("ITEM", adInteger, adParamInput, , Item)
        .Parameters.Append oParam
        Set oParam = .CreateParameter("CODCENTRO", adVarChar, adParamInput, 50, IIf(Cod = "", Null, Cod))
        .Parameters.Append oParam
        
        Set rs = .Execute
    End With
   
    If Not rs.eof Then
        Set fldCentro = rs.Fields("COD")
        Set fldDen = rs.Fields("DEN")
                        
        While Not rs.eof
            Me.Add fldCentro.Value, fldDen.Value
            rs.MoveNext
        Wend
        
        rs.MoveFirst
        
        Set fldCentro = Nothing
        Set fldDen = Nothing
    End If
    
    Set rs.ActiveConnection = Nothing
    Set CargarCentrosPorItemNoCodificado = rs

Salir:
    Set oCom = Nothing
    Set oParam = Nothing
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CCentros", "CargarCentrosPorItemNoCodificado", ERR, Erl)
        Resume Salir
    End If
End Function

''' <summary>Carga los centros de una organización de compras</summary>
''' <param name="sOrgCompras">Org. compras</param>
''' <param name="sUsu">Cod. usuario</param>
''' <returns>Recordset con los centros</returns>
''' <remarks>Llamada desde: GSClient.frmPEDIDOS</remarks>
''' <revision>EPB 23/07/2012</revision>

Public Function CargarCentrosPorOrgCompras(ByVal sOrgCompras As String, ByVal sUsu As String) As adodb.Recordset
    Dim rs As adodb.Recordset
    Dim fldCentro As adodb.Field
    Dim fldDen As adodb.Field
    Dim oCom As adodb.Command
    Dim oParam As adodb.Parameter
        
    If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
       
    Set mCol = New Collection
    
    Set oCom = New adodb.Command
    With oCom
        Set .ActiveConnection = mvarConexion.ADOCon
        .CommandText = "FSWS_GETCENTROS"
        .CommandType = adCmdStoredProc
        
        Set oParam = .CreateParameter("COD_ORGCOMPRAS", adVarChar, adParamInput, 50, sOrgCompras)
        .Parameters.Append oParam
        Set oParam = .CreateParameter("USU", adVarChar, adParamInput, 50, sUsu)
        .Parameters.Append oParam
        Set oParam = .CreateParameter("RESTRICUONSOLUSUUON", adBoolean, adParamInput, , 0)
        .Parameters.Append oParam
        Set oParam = .CreateParameter("RESTRICUONSOLPERFUON", adBoolean, adParamInput, , 0)
        .Parameters.Append oParam
        
        Set rs = .Execute
    End With
   
    If Not rs.eof Then
        Set fldCentro = rs.Fields("COD")
        Set fldDen = rs.Fields("DEN")
                        
        While Not rs.eof
            Me.Add fldCentro.Value, fldDen.Value
            rs.MoveNext
        Wend
        
        rs.MoveFirst
        
        Set fldCentro = Nothing
        Set fldDen = Nothing
    End If
    
    Set rs.ActiveConnection = Nothing
    Set CargarCentrosPorOrgCompras = rs

Salir:
    Set oCom = Nothing
    Set oParam = Nothing
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CCentros", "CargarCentrosPorOrgCompras", ERR, Erl)
        Resume Salir
    End If
End Function

Public Property Get Item(vntIndexKey As Variant) As CCentro
   On Error GoTo Error_Cls:
   
    Set Item = mCol(vntIndexKey)
    Exit Property
Error_Cls:
    Set Item = Nothing
    
End Property

Public Property Get eof() As Boolean
    eof = mvarEOF
End Property

Public Property Get Count() As Long
    
    If mCol Is Nothing Then
        Count = 0
    Else
        Count = mCol.Count
    End If
    
End Property

Public Property Set Centros(ByVal vData As Collection)
    Set mCol = vData
End Property

Public Property Get Centros() As Collection
    Set Centros = mCol
End Property


Public Sub Remove(vntIndexKey As Variant)
    mCol.Remove vntIndexKey
End Sub


Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
    Set NewEnum = mCol.[_NewEnum]
End Property
Friend Property Set Conexion(ByVal con As CConexion)
Set mvarConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = mvarConexion
End Property

Private Sub Class_Initialize()
    'creates the collection when this class is created
  
    Set mCol = New Collection
End Sub


Private Sub Class_Terminate()
    'destroys collection when this class is terminated
    Set mCol = Nothing
    Set mvarConexion = Nothing
End Sub

Public Sub CargarTodosLosCentros(Optional ByVal sCodCentro As String, Optional ByVal sCodOrganizacion As Variant _
, Optional ByVal bOrdenadoPorDen As Boolean)
Dim rs As New adodb.Recordset
Dim fldCod As adodb.Field
Dim fldDen As adodb.Field
Dim sConsulta As String


If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If IsMissing(sCodOrganizacion) Then
        sConsulta = "SELECT C.COD, C.DEN "
        sConsulta = sConsulta & " FROM CENTROS C WITH (NOLOCK) WHERE 1 = 1"
        
        
    Else
        sConsulta = "SELECT C.COD, C.DEN"
        sConsulta = sConsulta & " FROM ORGCOMPRAS_CENTROS OCC WITH (NOLOCK)"
        sConsulta = sConsulta & " INNER JOIN CENTROS C WITH (NOLOCK) ON C.COD = OCC.CENTROS"
        sConsulta = sConsulta & " INNER JOIN ORGCOMPRAS OC WITH (NOLOCK) ON OC.COD = OCC.ORGCOMPRAS"
        sConsulta = sConsulta & " WHERE OC.COD = '" & sCodOrganizacion & "'"
        
    End If
    
    If sCodCentro <> "" Then
        sConsulta = sConsulta & " AND C.COD = '" & sCodCentro & "'"
    End If
    
    If bOrdenadoPorDen Then
        sConsulta = sConsulta & " ORDER BY C.DEN"
    Else
        sConsulta = sConsulta & " ORDER BY C.COD"
    End If

    rs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
          
    If Not rs.eof Then
        
        Set fldCod = rs.Fields("COD")
        Set fldDen = rs.Fields("DEN")
        
        Set mCol = Nothing
        Set mCol = New Collection
        
        While Not rs.eof
            Me.Add fldCod.Value, fldDen.Value
            rs.MoveNext
                        
        Wend
        
        Set fldCod = Nothing
        Set fldDen = Nothing
        
    End If
    
    rs.Close
    Set rs = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CCentros", "CargarTodosLosCentros", ERR, Erl)
        Exit Sub
    End If
    
End Sub

''' <summary>Devuelve los centros por artículo</summary>
''' <param name="GMN1">GMN1</param>
''' <param name="sCodArt">Cod. del artículo</param>
''' <param name="bAtravesItem">bAtravesItem</param>
''' <param name="Anyo">Anyo</param>
''' <param name="Proce">Cod. del proceso</param>
''' <param name="Item">Id. del item</param>
''' <param name="Cod">Cod. del centro</param>
''' <param name="OrgCompra">OrgCompra</param>
''' <param name="Almacen">Cod. de almacén</param>
''' <returns>Recordset con los centros</returns>
''' <remarks>Llamada desde: GSClient</remarks>
''' <revision>LTG 02/01/2012</revision>

Public Function CargarCentrosPorArticulo(ByVal GMN1 As String, ByVal sCodArt As String, Optional ByVal bAtravesItem As Boolean = False _
        , Optional ByVal Anyo As Integer = 0, Optional ByVal Proce As Long = 0, Optional ByVal Item As Long = 0 _
        , Optional ByVal Cod As String = "", Optional ByVal OrgCompra As String = "", Optional ByVal Almacen As Boolean = False _
        ) As adodb.Recordset
    Dim rs As New adodb.Recordset
    Dim fldCentro As adodb.Field
    Dim fldAlmacen As adodb.Field
    Dim oCom As adodb.Command
    Dim oParam As adodb.Parameter

    If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR

    Set mCol = Nothing
    Set mCol = New Collection

    Set oCom = New adodb.Command
    With oCom
        .ActiveConnection = mvarConexion.ADOCon
        .CommandText = "FSEP_DEVOLVER_CENTROS_ARTICULO"
        .CommandType = adCmdStoredProc

        Set oParam = .CreateParameter("CODART", adVarChar, adParamInput, 50, sCodArt)
        .Parameters.Append oParam
        Set oParam = .CreateParameter("ATRAVESITEM", adTinyInt, adParamInput, , BooleanToVbBinary(bAtravesItem))
        .Parameters.Append oParam
        Set oParam = .CreateParameter("ANYO", adSmallInt, adParamInput, , Anyo)
        .Parameters.Append oParam
        Set oParam = .CreateParameter("GMN1", adVarChar, adParamInput, 50, GMN1)
        .Parameters.Append oParam
        Set oParam = .CreateParameter("PROCE", adInteger, adParamInput, , Proce)
        .Parameters.Append oParam
        Set oParam = .CreateParameter("ITEM", adInteger, adParamInput, , Item)
        .Parameters.Append oParam
        Set oParam = .CreateParameter("CODCENTRO", adVarChar, adParamInput, 50, IIf(Cod = "", Null, Cod))
        .Parameters.Append oParam
        Set oParam = .CreateParameter("ORGCOMPRAS", adVarChar, adParamInput, 50, IIf(OrgCompra = "", Null, OrgCompra))
        .Parameters.Append oParam
        Set oParam = .CreateParameter("ALMACEN", adTinyInt, adParamInput, , BooleanToVbBinary(Almacen))
        .Parameters.Append oParam

        Set rs = .Execute
    End With

    If Not rs Is Nothing Then
        If Not rs.eof Then
            Set fldCentro = rs.Fields("CENTROS")
            Set fldAlmacen = rs.Fields("ALMACEN")

            While Not rs.eof
                Me.Add fldCentro.Value, IIf(Almacen, "", fldAlmacen.Value), , IIf(Almacen, fldAlmacen.Value, "")
                rs.MoveNext
            Wend

            rs.MoveFirst

            Set fldCentro = Nothing
            Set fldAlmacen = Nothing
        End If
    End If

    Set CargarCentrosPorArticulo = rs

Salir:
    Set oParam = Nothing
    Set oCom = Nothing
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CCentros", "CargarCentrosPorArticulo", ERR, Erl)
        Resume Salir
    End If
End Function

''' <summary>Carga los centros de una organización</summary>
''' <param name="sCodOrganizacion">Cod. organización</param>
''' <param name="bOrdenadoPorDen">Orden por denominación</param>
''' <remarks>Llamada desde: GSClient</remarks>
''' <revision>LTG 02/01/2012</revision>

Public Sub CargarCentrosDeLaOrganizacion(ByVal sCodOrganizacion As String, Optional ByVal bOrdenadoPorDen As Boolean)
    Dim rs As New adodb.Recordset
    Dim fldCod As adodb.Field
    Dim fldDen As adodb.Field
    Dim sConsulta As String
    
    '********* Precondicion **************************************
    If mvarConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CCentros", "No se ha establecido la conexion"
        Exit Sub
    End If
    '*************************************************************
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
  
    sConsulta = "SELECT C.COD, C.DEN"
    sConsulta = sConsulta & " FROM ORGCOMPRAS_CENTROS OCC WITH (NOLOCK)"
    sConsulta = sConsulta & " INNER JOIN CENTROS C WITH (NOLOCK) ON C.COD = OCC.CENTROS"
    sConsulta = sConsulta & " INNER JOIN ORGCOMPRAS OC WITH (NOLOCK) ON OC.COD = OCC.ORGCOMPRAS"
    sConsulta = sConsulta & " WHERE OC.COD = '" & sCodOrganizacion & "'"
   

    If bOrdenadoPorDen Then
        sConsulta = sConsulta & " ORDER BY C.DEN"
    Else
        sConsulta = sConsulta & " ORDER BY C.COD"
    End If
          
    rs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
          
    If Not rs.eof Then
        
        Set fldCod = rs.Fields("COD")
        Set fldDen = rs.Fields("DEN")
        
        
        While Not rs.eof
            Me.Add fldCod.Value, fldDen.Value
            rs.MoveNext
                        
        Wend
        
        Set fldCod = Nothing
        Set fldDen = Nothing
        
    End If
    
    rs.Close
    Set rs = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CCentros", "CargarCentrosDeLaOrganizacion", ERR, Erl)
        Exit Sub
    End If
    
End Sub

