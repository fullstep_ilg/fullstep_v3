VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CCamposVinculado"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

Private mCol As Collection

Private m_oConexion As CConexion

    ''' * Objetivo: Dar valor a la conexion de la Destino
    ''' * Recibe: Conexion
    ''' * Devuelve: Nada
Friend Property Set Conexion(ByVal vData As CConexion)
    Set m_oConexion = vData
End Property

    ''' * Objetivo: Devolver la conexion de la Destino
    ''' * Recibe: Nada
    ''' * Devuelve: Conexion
Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property

    ''' * Objetivo: Recuperar un campo de la coleccion
    ''' * Recibe: Indice del campo a recuperar
    ''' * Devuelve: campo correspondiente
Public Property Get Item(vntIndexKey As Variant) As CVinculado
       
On Error GoTo NoSeEncuentra:

    ''' Devolvemos el item de la coleccion privada
    
    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:

    Set Item = Nothing
    
End Property

    ''' * Objetivo: Eliminar un campo de la coleccion
    ''' * Recibe: Indice del campo a eliminar
    ''' * Devuelve: Nada
Public Sub Remove(vntIndexKey As Variant)
    mCol.Remove vntIndexKey
End Sub

Public Property Get NewEnum() As IUnknown

    ''' ! Pendiente de revision, objetivo desconocido
    
    Set NewEnum = mCol.[_NewEnum]

End Property

''' * Objetivo: Crear la coleccion
Private Sub Class_Initialize()
    Set mCol = New Collection
End Sub

Private Sub Class_Terminate()
    Set mCol = Nothing
    Set m_oConexion = Nothing
End Sub

Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
    Count = mCol.Count
End If

End Property
''' <summary>
''' Crear un objeto campos vinculados entre desgloses
''' </summary>
''' <param name="DesglVinculadoId">DESGLOSE_VINCULADO.ID</param>
''' <param name="CampoVinculado">Id de form_campo del campo del Desglose Vinculado</param>
''' <param name="campoOrigen">Id de form_campo del campo del Desglose Vinculado origen, de haberlo<</param>
''' <param name="lSolicitud">Solicitud</param>
''' <param name="lDesgloseOrigen">Id de form_campo de Desglose origen, de haberlo</param>
''' <param name="lDesgloseVinculado">Id de form_campo de Desglose Vinculado</param>
''' <param name="FecAct">Fecha ultima grabaci�n</param>
''' <param name="vIndice">si se usa indice o no</param>
''' <returns>Un objeto CVinculado</returns>
''' <remarks>Llamada desde: frmFormularioVincularCampos; Tiempo m�ximo: 0,1</remarks>
Public Function Add(ByVal DesglVinculadoId As Variant, ByVal CampoVinculado As Long, ByVal campoOrigen As Variant, ByVal lSolicitud As Long _
, ByVal lDesgloseOrigen As Long, ByVal lDesgloseVinculado As Long, Optional ByVal FecAct As Variant, Optional ByVal vIndice As Variant) As CCampoVinculado
    Dim objnewmember As CCampoVinculado
    
    Set objnewmember = New CCampoVinculado
    Set objnewmember.Conexion = m_oConexion
    
    objnewmember.DesglVinculadoId = DesglVinculadoId

    objnewmember.CampoVinculado = CampoVinculado
    
    If IsNumeric(campoOrigen) Then
        objnewmember.campoOrigen = campoOrigen
    Else
        objnewmember.campoOrigen = Null
    End If
    If Not IsMissing(FecAct) Then
        objnewmember.FecAct = FecAct
    Else
        objnewmember.FecAct = Null
    End If
    
    objnewmember.Solicitud = lSolicitud
    objnewmember.DesgloseOrigen = lDesgloseOrigen
    objnewmember.DesgloseVinculado = lDesgloseVinculado
    
    If Not IsMissing(vIndice) And Not IsNull(vIndice) Then
        objnewmember.Indice = vIndice
        mCol.Add objnewmember, CStr(vIndice)
    Else
        mCol.Add objnewmember, CStr(CampoVinculado)
    End If

    Set Add = objnewmember
    
    Set objnewmember = Nothing
End Function
''' <summary>
''' Grabar las vinculaciones de campos del desglose
''' </summary>
''' <param name="bCambioDeOrigen">indica si ha habido cambio de deglose origen en la vinculaci�n</param>
''' <returns>Error de haberlo</returns>
''' <remarks>Llamada desde: frmFormularioVincularDesglose; Tiempo m�ximo: 0,1</remarks>
Public Function GrabarCamposVinculaciones(ByVal bCambioDeOrigen As Boolean) As TipoErrorSummit
    Dim udtTESError As TipoErrorSummit
    Dim oLinea As CCampoVinculado
    Dim sConsulta As String
    
    Dim ador As adodb.Recordset
    Dim bYaInsertado As Boolean
    Dim bSonInserts As Boolean
    Dim lDVID As Long
        
    udtTESError.NumError = TESnoerror
    
    On Error GoTo Error
    
    bYaInsertado = False
    bSonInserts = False
    
    For Each oLinea In mCol
        If oLinea.DesglVinculadoId = "" Then
            If (Not bYaInsertado) Then
                bSonInserts = True
                
                sConsulta = "INSERT INTO DESGLOSE_VINCULADO (SOLICITUD,CAMPO_VINCULADO,CAMPO_ORIGEN) VALUES"
                sConsulta = sConsulta & "(" & oLinea.Solicitud & "," & oLinea.DesgloseVinculado & "," & oLinea.DesgloseOrigen & ")"
                
                m_oConexion.ADOCon.Execute sConsulta
                
                Set ador = New adodb.Recordset
                'Sin with(nolock) pq interesa el recien insertado
                sConsulta = "SELECT MAX(ID) DVID FROM DESGLOSE_VINCULADO"
                ador.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
                
                lDVID = ador.Fields("DVID").Value
            End If
        Else
            lDVID = CLng(oLinea.DesglVinculadoId)
            
            If bCambioDeOrigen Then
                sConsulta = "UPDATE DESGLOSE_VINCULADO SET CAMPO_ORIGEN=" & oLinea.DesgloseOrigen & " WHERE ID=" & lDVID
                
                m_oConexion.ADOCon.Execute sConsulta
            End If
        End If
        
        bYaInsertado = True
        
        If bSonInserts Then
            sConsulta = "INSERT INTO LINEA_DESGLOSE_VINCULADO(DESGLOSE_VINCULADO,CAMPO_HIJO_VINCULADO,CAMPO_HIJO_ORIGEN)"
            sConsulta = sConsulta & " VALUES(" & lDVID & "," & oLinea.CampoVinculado & ","
            sConsulta = sConsulta & DblToSQLFloat(oLinea.campoOrigen) & ")"
        Else
            sConsulta = "UPDATE LINEA_DESGLOSE_VINCULADO SET CAMPO_HIJO_ORIGEN=" & DblToSQLFloat(oLinea.campoOrigen)
            sConsulta = sConsulta & " WHERE DESGLOSE_VINCULADO=" & lDVID & " AND CAMPO_HIJO_VINCULADO=" & oLinea.CampoVinculado
        End If
                
        m_oConexion.ADOCon.Execute sConsulta
    Next
    
    GrabarCamposVinculaciones = udtTESError
    Exit Function

Error:
    udtTESError = basErrores.TratarError(m_oConexion.ADOCon.Errors)
        
    GrabarCamposVinculaciones = udtTESError
End Function
