VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CDistItemsNivel3"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CDistItemsNivel3 **********************************
'*             Autor : Javier Arana
'*             Creada : 16/6/99
'****************************************************************


Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum

Private mCol As Collection
Private mvarConexion As CConexion
Private mvarEOF As Boolean

Friend Property Set Conexion(ByVal con As CConexion)
Set mvarConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = mvarConexion
End Property

Public Property Get eof() As Boolean
    eof = mvarEOF
End Property
Friend Property Let eof(ByVal b As Boolean)
    mvarEOF = b
End Property

Public Property Get Item(vntIndexKey As Variant) As CDistItemNivel3
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
     Count = mCol.Count
End If


End Property

Public Function CargarTodasLasDistribucionesItem(ByVal oItem As CItem, Optional ByVal OrdPorUON1 As Boolean, Optional ByVal OrdPorDen As Boolean, Optional ByVal OrdPorPorcentaje As Boolean, Optional ByVal UsarIndice As Boolean) As Double
    Dim sConsulta As String
    Dim lIndice As Long
    Dim iNumAdjDeArt As Integer
    Dim dSuma As Double
    Dim rs As New adodb.Recordset
    Dim fldUON1 As adodb.Field
    Dim fldUON2 As adodb.Field
    Dim fldUON3 As adodb.Field
    Dim fldPorcen As adodb.Field
    Dim fldDen As adodb.Field
    
    '********* Precondicion **************************************
    If mvarConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CDistribucionesItem.CargarTodosLosDistribucionesItem", "No se ha establecido la conexion"
        Exit Function
    End If
    '*************************************************************
    If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
       
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    dSuma = 0
    iNumAdjDeArt = 0
                
    sConsulta = "SELECT ITEM_UON3.UON1 AS UON1,ITEM_UON3.UON2 AS UON2,ITEM_UON3.UON3 AS UON3,PORCEN,U3.DEN "
    sConsulta = sConsulta & " FROM ITEM_UON3 WITH (NOLOCK) "
    sConsulta = sConsulta & " INNER JOIN UON3 U3 WITH (NOLOCK) ON U3.UON1=ITEM_UON3.UON1 AND U3.UON2=ITEM_UON3.UON2 AND U3.COD=ITEM_UON3.UON3"
    sConsulta = sConsulta & " WHERE ANYO=" & oItem.Proceso.Anyo & " AND GMN1='" & DblQuote(oItem.Proceso.GMN1Cod) & "' AND PROCE=" & oItem.Proceso.Cod
    sConsulta = sConsulta & " AND ITEM=" & oItem.Id
    sConsulta = sConsulta & " AND ITEM_UON3.UON1 > '  '"
    If OrdPorPorcentaje Then
        sConsulta = sConsulta & " ORDER BY PORCEN"
    Else
        sConsulta = sConsulta & " ORDER BY ITEM_UON3.UON1,ITEM_UON3.UON2,ITEM_UON3.UON3"
    End If
    
    'Set rdores = mvarConexion.rdoSummitCon.OpenResultset(sConsulta, rdOpenForwardOnly, rdConcurReadOnly, rdExecDirect)
    rs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly

    Set mCol = Nothing
    Set mCol = New Collection
        
    If rs.eof Then
        CargarTodasLasDistribucionesItem = 0
    Else
        Set fldUON1 = rs.Fields("UON1")
        Set fldUON2 = rs.Fields("UON2")
        Set fldUON3 = rs.Fields("UON3")
        Set fldPorcen = rs.Fields("PORCEN")
        Set fldDen = rs.Fields("DEN")
        
        iNumAdjDeArt = 0
        
        If UsarIndice Then
            lIndice = 0
                   
            While Not rs.eof
                ' Dejamos en manos del programador cuando asignar la conexion al obejto usuario
                Me.Add oItem, fldUON1.Value, fldUON2.Value, fldUON3.Value, fldPorcen.Value, fldDen.Value, lIndice
                dSuma = dSuma + fldPorcen.Value
                rs.MoveNext
                lIndice = lIndice + 1
            Wend
        Else
            While Not rs.eof
                ' Dejamos en manos del programador cuando asignar la conexion al obejto usuario
                Me.Add oItem, fldUON1.Value, fldUON2.Value, fldUON3.Value, fldPorcen.Value, fldDen.Value
                dSuma = dSuma + fldPorcen.Value
                rs.MoveNext
                lIndice = lIndice + 1
            Wend
        End If
        
        Set fldUON1 = Nothing
        Set fldUON2 = Nothing
        Set fldUON3 = Nothing
        Set fldPorcen = Nothing
        CargarTodasLasDistribucionesItem = dSuma
    End If
    
    rs.Close
    Set rs = Nothing
    
    Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CDistItemsNivel3", "CargarTodasLasDistribucionesItem", ERR, Erl)
      Exit Function
   End If
End Function

''' <summary>Carga todas las distribuciones de un proceso</summary>
''' <param name="iAnyo">Anyo</param>
''' <param name="sGMN1">GMN1</param>
''' <param name="iProceso">Id proceso</param>
''' <param name="OrdPorUON1">Orden por UON1</param>
''' <param name="OrdPorPorcentaje">Orden por porcentaje</param>
''' <param name="UsarIndice">Usar �ndice</param>
''' <returns>Suma de porcentajes</returns>
''' <remarks>Llamada desde: FSGSClient; Tiempo m�ximo: 1 sec </remarks>
''' <revision>LTG 04/01/2012</revision>

Public Function CargarTodasLasDistribucionesProce(ByVal iAnyo As Integer, ByVal sGMN1 As String, ByVal iProceso As Long, Optional ByVal OrdPorUON1 As Boolean, _
        Optional ByVal OrdPorPorcentaje As Boolean, Optional ByVal UsarIndice As Boolean) As Double
    Dim sConsulta As String
    Dim lIndice As Long
    Dim dSuma As Double
    Dim AdoRes As adodb.Recordset
    Dim adofldUON1 As adodb.Field
    Dim adofldUON2 As adodb.Field
    Dim adofldUON3 As adodb.Field
    Dim adofldPorcen As adodb.Field
    Dim oItem As CItem
    
    '********* Precondicion **************************************
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If mvarConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CDistribucionesItem.CargarTodosLosDistribucionesProce", "No se ha establecido la conexion"
        Exit Function
    End If
    '*************************************************************

    dSuma = 0

    sConsulta = "SELECT UON1,UON2,UON3,PORCEN"
    sConsulta = sConsulta & " FROM PROCE_UON3 WITH (NOLOCK)"
    sConsulta = sConsulta & " WHERE ANYO=" & iAnyo & " AND GMN1='" & DblQuote(sGMN1) & "' AND PROCE=" & iProceso
    If OrdPorPorcentaje Then
        sConsulta = sConsulta & " ORDER BY PORCEN"
    Else
        sConsulta = sConsulta & " ORDER BY UON1,UON2,UON3"
    End If

    Set AdoRes = New adodb.Recordset
    AdoRes.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly

    If AdoRes.eof Then
            
        AdoRes.Close
        Set AdoRes = Nothing
        Set mCol = Nothing
        Set mCol = New Collection
        CargarTodasLasDistribucionesProce = 0
        Exit Function
          
    Else
        
        Set adofldUON1 = AdoRes.Fields("UON1")
        Set adofldUON2 = AdoRes.Fields("UON2")
        Set adofldUON3 = AdoRes.Fields("UON3")
        Set adofldPorcen = AdoRes.Fields("PORCEN")
        
        Set mCol = Nothing
        Set mCol = New Collection
        Set oItem = Nothing
                    
        If UsarIndice Then
            
            lIndice = 0
                   
            While Not AdoRes.eof
                ' Dejamos en manos del programador cuando asignar la conexion al obejto usuario
                Me.Add oItem, adofldUON1.Value, adofldUON2.Value, adofldUON3.Value, adofldPorcen.Value, , lIndice
                dSuma = dSuma + adofldPorcen.Value
                AdoRes.MoveNext
                lIndice = lIndice + 1
            Wend
        
        Else
                               
            While Not AdoRes.eof
                ' Dejamos en manos del programador cuando asignar la conexion al obejto usuario
                Me.Add oItem, adofldUON1.Value, adofldUON2.Value, adofldUON3.Value, adofldPorcen.Value
                dSuma = dSuma + adofldPorcen.Value
                AdoRes.MoveNext
            Wend
            
        End If
        
        AdoRes.Close
        Set AdoRes = Nothing
        Set adofldUON1 = Nothing
        Set adofldUON2 = Nothing
        Set adofldUON3 = Nothing
        Set adofldPorcen = Nothing
        CargarTodasLasDistribucionesProce = dSuma
          
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CDistItemsNivel3", "CargarTodasLasDistribucionesProce", ERR, Erl)
      Exit Function
   End If

End Function

''' <summary>Carga todas las distribuciones de un grupo</summary>
''' <param name="iAnyo">Anyo</param>
''' <param name="sGMN1">GMN1</param>
''' <param name="iProceso">Id proceso</param>
''' <param name="lGrupo">Id grupo</param>
''' <param name="OrdPorUON1">Orden por UON1</param>
''' <param name="OrdPorPorcentaje">Orden por porcentaje</param>
''' <param name="UsarIndice">Usar �ndice</param>
''' <returns>Suma de porcentajes</returns>
''' <remarks>Llamada desde: FSGSClient; Tiempo m�ximo: 1 sec </remarks>
''' <revision>LTG 04/01/2012</revision>

Public Function CargarTodasLasDistribucionesGrupo(ByVal iAnyo As Integer, ByVal sGMN1 As String, ByVal iProceso As Long, ByVal lGrupo As Long, _
        Optional ByVal OrdPorUON1 As Boolean, Optional ByVal OrdPorPorcentaje As Boolean, Optional ByVal UsarIndice As Boolean) As Double
    Dim sConsulta As String
    Dim lIndice As Long
    Dim dSuma As Double
    Dim AdoRes As adodb.Recordset
    Dim adofldUON1 As adodb.Field
    Dim adofldUON2 As adodb.Field
    Dim adofldUON3 As adodb.Field
    Dim adofldPorcen As adodb.Field
    Dim oItem As CItem
    
    '********* Precondicion **************************************
    If mvarConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CDistribucionesItem.CargarTodasLasDistribucionesGrupo", "No se ha establecido la conexion"
        Exit Function
    End If
    '*************************************************************
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

    dSuma = 0

    sConsulta = "SELECT UON1,UON2,UON3,PORCEN"
    sConsulta = sConsulta & " FROM PROCE_GR_UON3 WITH (NOLOCK)"
    sConsulta = sConsulta & " WHERE ANYO=" & iAnyo & " AND GMN1='" & DblQuote(sGMN1) & "' AND PROCE=" & iProceso & " AND GRUPO=" & lGrupo
    If OrdPorPorcentaje Then
        sConsulta = sConsulta & " ORDER BY PORCEN"
    Else
        sConsulta = sConsulta & " ORDER BY UON1,UON2,UON3"
    End If

    Set AdoRes = New adodb.Recordset
    AdoRes.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly

    If AdoRes.eof Then
            
        AdoRes.Close
        Set AdoRes = Nothing
        Set mCol = Nothing
        Set mCol = New Collection
        CargarTodasLasDistribucionesGrupo = 0
        Exit Function
          
    Else
        
        Set adofldUON1 = AdoRes.Fields("UON1")
        Set adofldUON2 = AdoRes.Fields("UON2")
        Set adofldUON3 = AdoRes.Fields("UON3")
        Set adofldPorcen = AdoRes.Fields("PORCEN")
        
        Set mCol = Nothing
        Set mCol = New Collection
        Set oItem = Nothing
                    
        If UsarIndice Then
            
            lIndice = 0
                   
            While Not AdoRes.eof
                ' Dejamos en manos del programador cuando asignar la conexion al obejto usuario
                Me.Add oItem, adofldUON1.Value, adofldUON2.Value, adofldUON3.Value, adofldPorcen.Value, , lIndice
                dSuma = dSuma + adofldPorcen.Value
                AdoRes.MoveNext
                lIndice = lIndice + 1
            Wend
        
        Else
                               
            While Not AdoRes.eof
                ' Dejamos en manos del programador cuando asignar la conexion al obejto usuario
                Me.Add oItem, adofldUON1.Value, adofldUON2.Value, adofldUON3.Value, adofldPorcen.Value
                dSuma = dSuma + adofldPorcen.Value
                AdoRes.MoveNext
            Wend
            
        End If
        
        AdoRes.Close
        Set AdoRes = Nothing
        Set adofldUON1 = Nothing
        Set adofldUON2 = Nothing
        Set adofldUON3 = Nothing
        Set adofldPorcen = Nothing
        CargarTodasLasDistribucionesGrupo = dSuma
          
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CDistItemsNivel3", "CargarTodasLasDistribucionesGrupo", ERR, Erl)
      Exit Function
   End If

End Function

Public Function Add(ByVal oItem As CItem, ByVal UON1 As String, ByVal UON2 As String, ByVal UON3 As String, ByVal Porcentaje As Double, Optional ByVal Den As Variant, Optional ByVal varIndice As Variant) As CDistItemNivel3
    'create a new object
    Dim sCod As String
    Dim objnewmember As CDistItemNivel3
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New CDistItemNivel3
      
    Set objnewmember.Item = oItem
    objnewmember.CodUON1 = UON1
    objnewmember.CodUON2 = UON2
    objnewmember.CodUON3 = UON3
    objnewmember.Porcentaje = Porcentaje
    Set objnewmember.Conexion = mvarConexion
    
    If IsMissing(Den) Then
        objnewmember.Den = Null
    Else
        objnewmember.Den = Den
    End If
    
    If Not IsMissing(varIndice) And Not IsNull(varIndice) Then
        objnewmember.Indice = varIndice
        mCol.Add objnewmember, CStr(varIndice)
    Else
        
        sCod = UON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(UON1))
        sCod = sCod & UON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(UON2))
        sCod = sCod & UON3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(UON3))
        mCol.Add objnewmember, CStr(sCod)
    End If
    
    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CDistItemsNivel3", "Add", ERR, Erl)
      Exit Function
   End If

End Function

Public Sub Remove(vntIndexKey As Variant)
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
        mCol.Remove vntIndexKey
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CDistItemsNivel3", "Remove", ERR, Erl)
      Exit Sub
   End If
    
End Sub


Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
     Set NewEnum = mCol.[_NewEnum]
End Property


Private Sub Class_Initialize()

   

    Set mCol = New Collection
End Sub


Private Sub Class_Terminate()
    'AdjDeArtroys collection when this class is terminated
    Set mCol = Nothing
    Set mvarConexion = Nothing
End Sub

Public Function BorrarEnModoIndice(ByVal iIndice As Integer)
Dim IndFor As Integer
Dim oDist As CDistItemNivel3

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    For IndFor = iIndice To mCol.Count - 2
            
        mCol.Remove (CStr(IndFor))
        Set oDist = mCol.Item(CStr(IndFor + 1))
        mCol.Add oDist, CStr(IndFor)
        Set oDist = Nothing
        
    Next IndFor
    
    mCol.Remove CStr(IndFor)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CDistItemsNivel3", "BorrarEnModoIndice", ERR, Erl)
      Exit Function
   End If
    
End Function


''' <summary>Devuelve si la integracion en esa la empresa/s es de solo entrada</summary>
''' <remarks>Llamada desde: frmproce</remarks>
''' <revision>epb 15/04/2015</revision>

Public Function IntegracionArtSoloEntrada() As Boolean
Dim sConsulta As String
Dim oUON3 As CDistItemNivel3
Dim adoRecordset As adodb.Recordset

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

    Set adoRecordset = New adodb.Recordset
    IntegracionArtSoloEntrada = False
    For Each oUON3 In mCol
        sConsulta = "SELECT TE.ACTIVA, TE.SENTIDO FROM UON3 WITH(NOLOCK) "
        sConsulta = sConsulta & "      LEFT JOIN EMP ON UON3.EMPRESA=EMP.ID LEFT JOIN ERP_SOCIEDAD ES ON ES.SOCIEDAD=EMP.SOCIEDAD"
        sConsulta = sConsulta & "      LEFT JOIN TABLAS_INTEGRACION_ERP TE ON TE.ERP=ES.ERP AND TE.TABLA=7"
        sConsulta = sConsulta & "      WHERE UON3.UON1='" & DblQuote(oUON3.CodUON1) & "' AND UON3.UON2='" & DblQuote(oUON3.CodUON2) & "' AND UON3.COD='" & DblQuote(oUON3.CodUON3) & "'"
        sConsulta = sConsulta & " union SELECT TE.ACTIVA, TE.SENTIDO FROM UON2 WITH(NOLOCK) "
        sConsulta = sConsulta & "      LEFT JOIN EMP ON UON2.EMPRESA=EMP.ID LEFT JOIN ERP_SOCIEDAD ES ON ES.SOCIEDAD=EMP.SOCIEDAD"
        sConsulta = sConsulta & "      LEFT JOIN TABLAS_INTEGRACION_ERP TE ON TE.ERP=ES.ERP AND TE.TABLA=7"
        sConsulta = sConsulta & "      WHERE UON2.UON1='" & DblQuote(oUON3.CodUON1) & "' AND UON2.COD='" & DblQuote(oUON3.CodUON2) & "'"
        sConsulta = sConsulta & " union SELECT TE.ACTIVA, TE.SENTIDO FROM UON1 WITH(NOLOCK) "
        sConsulta = sConsulta & "      LEFT JOIN EMP ON UON1.EMPRESA=EMP.ID LEFT JOIN ERP_SOCIEDAD ES ON ES.SOCIEDAD=EMP.SOCIEDAD"
        sConsulta = sConsulta & "      LEFT JOIN TABLAS_INTEGRACION_ERP TE ON TE.ERP=ES.ERP AND TE.TABLA=7"
        sConsulta = sConsulta & "      WHERE UON1.COD='" & DblQuote(oUON3.CodUON1) & "'"
        adoRecordset.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    
          While Not adoRecordset.eof
              
              If NullToDbl0(adoRecordset("ACTIVA").Value) = 1 And NullToDbl0(adoRecordset("SENTIDO").Value) = 2 Then 'SOLO ENTRADA
                  IntegracionArtSoloEntrada = True
                  adoRecordset.Close
                  Set adoRecordset = Nothing
                  Exit Function
              End If
              adoRecordset.MoveNext
          Wend
          
        adoRecordset.Close
    Next
    Set adoRecordset = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CDistItemsNivel3", "IntegracionArtSoloEntrada", ERR, Erl)
      Exit Function
   End If
  
End Function




