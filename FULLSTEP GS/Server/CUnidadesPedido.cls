VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CUnidadesPedido"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CUnidadesPedido **********************************
'*             Autor : Elena Mu�oz
'*             Creada : 22/8/2001
'****************************************************************

Option Explicit

Private mCol As Collection
Private mvarConexion As CConexion
Private mvarEOF As Boolean

Friend Property Set Conexion(ByVal con As CConexion)
Set mvarConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = mvarConexion
End Property




Public Function Add(Optional ByVal IDLinea As Variant, Optional ByVal varIndice As Variant, Optional ByVal ProveCod As String, Optional ByVal ProveDen As String, Optional ByVal ArtCod_Interno As String, Optional ByVal ArtCod_Externo As String, Optional ByVal ArtDen As String, Optional ByVal UnidadCompra As String, _
Optional ByVal UCDen As String, Optional ByVal UnidadPedido As String, Optional ByVal UPDen As String, Optional ByVal FactorConversion As Double, Optional ByVal CantidadMinima As Variant, Optional ByVal GMN1 As String, Optional ByVal GMN2 As String, Optional ByVal GMN3 As String, Optional ByVal GMN4 As String, Optional ByVal FecAct As Variant, Optional ByVal CantidadMaximaPed As Variant, Optional ByVal CantidadMaximaTotal As Variant, Optional ByVal CantidadMaximaTotal_UC As Variant, Optional ByVal CantidadConsumida_UC As Variant, Optional ByVal CantidadConsumida As Variant, Optional ByVal Defecto As Boolean) As CUnidadPedido

    'create a new object
    Dim objnewmember As CUnidadPedido
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New CUnidadPedido
    
    If IsMissing(IDLinea) Then
        objnewmember.IDLinea = Null
    Else
        objnewmember.IDLinea = IDLinea
    End If
    
    objnewmember.ProveCod = ProveCod
    objnewmember.ProveDen = ProveDen
    objnewmember.ArtCod_Externo = ArtCod_Externo
    objnewmember.ArtCod_Interno = ArtCod_Interno
    objnewmember.ArtDen = ArtDen
    objnewmember.UnidadCompra = UnidadCompra
    objnewmember.UCDen = UCDen
    objnewmember.UnidadPedido = UnidadPedido
    objnewmember.UPDen = UPDen
    objnewmember.FactorConversion = FactorConversion
 
    
        
    If IsMissing(CantidadMinima) Then
        objnewmember.CantidadMinima = Null
    Else
        objnewmember.CantidadMinima = CantidadMinima
    End If
    If IsMissing(CantidadMaximaPed) Then
        objnewmember.CantidadMaximaPed = Null
    Else
        objnewmember.CantidadMaximaPed = CantidadMaximaPed
    End If
    If IsMissing(CantidadMaximaTotal) Then
        objnewmember.CantidadMaximaTotal = Null
    Else
        objnewmember.CantidadMaximaTotal = CantidadMaximaTotal
    End If
    If IsMissing(CantidadMaximaTotal_UC) Then
        objnewmember.CantidadMaximaTotal_UC = Null
    Else
        objnewmember.CantidadMaximaTotal_UC = CantidadMaximaTotal_UC
    End If
    If IsMissing(CantidadConsumida_UC) Then
        objnewmember.CantidadConsumida_UC = Null
    Else
        objnewmember.CantidadConsumida_UC = CantidadConsumida_UC
    End If
    If IsMissing(CantidadConsumida) Then
        objnewmember.CantidadConsumida = Null
    Else
        objnewmember.CantidadConsumida = CantidadConsumida
    End If
    If IsMissing(Defecto) Then
        objnewmember.Defecto = False
    Else
        objnewmember.Defecto = Defecto
    End If
    
    objnewmember.GMN1Cod = GMN1
    objnewmember.GMN2Cod = GMN2
    objnewmember.GMN3Cod = GMN3
    objnewmember.GMN4Cod = GMN4
    objnewmember.FechaAct = FecAct
    
    Set objnewmember.Conexion = mvarConexion
    
    If Not IsMissing(varIndice) And Not IsNull(varIndice) Then
        objnewmember.Indice = varIndice
        mCol.Add objnewmember, CStr(varIndice)
    Else
        objnewmember.Indice = IDLinea
        mCol.Add objnewmember, CStr(IDLinea)
    End If
            
    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If Err.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CUnidadesPedido", "Add", Err, Erl)
      Exit Function
   End If

End Function

Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
     Count = mCol.Count
End If


End Property

Public Property Get eof() As Boolean
    eof = mvarEOF
End Property

Friend Property Let eof(ByVal b As Boolean)
    mvarEOF = b
End Property

Public Property Get Item(vntIndexKey As Variant) As CUnidadPedido
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
     Set NewEnum = mCol.[_NewEnum]
End Property

Public Sub Remove(vntIndexKey As Variant)
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    mCol.Remove vntIndexKey
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If Err.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CUnidadesPedido", "Remove", Err, Erl)
      Exit Sub
   End If
End Sub

Private Sub Class_Initialize()
    Set mCol = New Collection
End Sub


Private Sub Class_Terminate()
    'destroys collection when this class is terminated
    Set mCol = Nothing
    Set mvarConexion = Nothing
End Sub


