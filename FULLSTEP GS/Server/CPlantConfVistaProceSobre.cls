VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CPlantConfVistaProceSobre"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CPlantConfVistaProceSobre **********************************
'*             Autor : Mertxe Mart�n
'*             Creada : 01/12/2003
'***************************************************************

Option Explicit

Implements IBaseDatos

Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum

Private m_oPlantConfVistaProc As CPlantConfVistaProce
Private m_iVista As Integer

Private m_lSobre As Long
Private m_bVisible As Boolean

Private m_oConexion As CConexion
Private m_oConexionServer As CConexionDeUseServer
Private m_vIndice As Variant
Private m_adores As adodb.Recordset

Public Property Let Sobre(ByVal lSobre As Long)
    m_lSobre = lSobre
End Property

Public Property Get Sobre() As Long
    Sobre = m_lSobre
End Property

Public Property Let Vista(ByVal iVista As Integer)
    m_iVista = iVista
End Property

Public Property Get Vista() As Integer
    Vista = m_iVista
End Property

Public Property Set PlantConfVistaProc(ByVal oSobre As CPlantConfVistaProce)
    Set m_oPlantConfVistaProc = oSobre
End Property
Public Property Get PlantConfVistaProc() As CPlantConfVistaProce
    Set PlantConfVistaProc = m_oPlantConfVistaProc
End Property

Public Property Let Visible(ByVal dato As Boolean)
    m_bVisible = dato
End Property

Public Property Get Visible() As Boolean
    Visible = m_bVisible
End Property

Friend Property Set ConexionServer(ByVal con As CConexionDeUseServer)
    Set m_oConexionServer = con
End Property

Friend Property Get ConexionServer() As CConexionDeUseServer
    Set ConexionServer = m_oConexionServer
End Property

Friend Property Set Conexion(ByVal con As CConexion)
    Set m_oConexion = con
End Property

Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property

Public Property Let Indice(ByVal vIndice As Variant)
    m_vIndice = vIndice
End Property
Public Property Get Indice() As Variant
    Indice = m_vIndice
End Property

Private Function IBaseDatos_AnyadirABaseDatos() As TipoErrorSummit
Dim sConsulta As String
Dim TESError As TipoErrorSummit

'********* Precondicion *******************
If m_oConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CPlantConfVistaProceSobre.AnyadirABAseDatos", "No se ha establecido la conexion"
End If

'******************************************

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
    
    TESError.NumError = TESnoerror
    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
    
    sConsulta = "INSERT INTO PLANTILLA_CONF_VISTAS_PROCE_SOBRE (PLANT,VISTA,SOBRE,VISIBLE)"
    sConsulta = sConsulta & " VALUES (" & m_oPlantConfVistaProc.Plantilla.Id
    sConsulta = sConsulta & "," & m_oPlantConfVistaProc.Vista & "," & m_lSobre
    sConsulta = sConsulta & "," & BooleanToSQLBinary(m_bVisible) & ")"
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    IBaseDatos_AnyadirABaseDatos = TESError
    Exit Function
    
Error_Cls:
    
    IBaseDatos_AnyadirABaseDatos = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
    On Error Resume Next
    If Not m_adores Is Nothing Then
        m_adores.Close
        Set m_adores = Nothing
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CPlantConfVistaProceSobre", "IBaseDatos_AnyadirABaseDatos", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If

End Function

Private Function IBaseDatos_CambiarCodigo(CodigoNuevo As Variant) As TipoErrorSummit

End Function

Private Sub IBaseDatos_CancelarEdicion()

End Sub

''' <summary>
''' Funci�n que comprueba si est�n la plantilla, vista y sobre en la tabla
''' </summary>
''' <returns>Boolean</returns>
''' <remarks>Llamada desde: FSGSClient; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 26/12/2011</revision>
Private Function IBaseDatos_ComprobarExistenciaEnBaseDatos() As Boolean
    Dim sConsulta As String
    Dim AdoRes As New adodb.Recordset
    
    '******************* Precondicion *******************
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CPlantConfVistaProceSobre.ComprobarExistenciaEnBaseDatos", "No se ha establecido la conexion"
    End If
    '*****************************************************
    
    If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
    sConsulta = "SELECT PLANT,VISTA,SOBRE FROM PLANTILLA_CONF_VISTAS_PROCE_SOBRE WITH(NOLOCK) WHERE "
    sConsulta = sConsulta & " PLANT=" & m_oPlantConfVistaProc.Plantilla.Id & " AND VISTA=" & m_oPlantConfVistaProc.Vista
    sConsulta = sConsulta & " AND SOBRE=" & (m_lSobre)

    Set AdoRes = New adodb.Recordset
    AdoRes.CursorLocation = adUseServer
    AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    If AdoRes.eof Then
        IBaseDatos_ComprobarExistenciaEnBaseDatos = False
    Else
        IBaseDatos_ComprobarExistenciaEnBaseDatos = True
    End If

    AdoRes.Close
    Set AdoRes = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CPlantConfVistaProceSobre", "IBaseDatos_ComprobarExistenciaEnBaseDatos", ERR, Erl)
      Exit Function
   End If
End Function


Private Function IBaseDatos_EliminarDeBaseDatos() As TipoErrorSummit
Dim sConsulta As String
Dim TESError As TipoErrorSummit

    '''Precondicion
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CPlantConfVistaProceSobre.EliminarDeBaseDatos", "No se ha establecido la conexion"
    End If
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:

    TESError.NumError = TESnoerror
     
    sConsulta = "DELETE FROM PLANTILLA_CONF_VISTAS_PROCE_SOBRE WHERE "
    sConsulta = sConsulta & " PLANT=" & m_oPlantConfVistaProc.Plantilla.Id
    sConsulta = sConsulta & " AND VISTA=" & m_oPlantConfVistaProc.Vista & " AND SOBRE =" & m_lSobre
    
    m_oConexion.ADOCon.Execute sConsulta

    IBaseDatos_EliminarDeBaseDatos = TESError

    Exit Function

Error_Cls:
    IBaseDatos_EliminarDeBaseDatos = basErrores.TratarError(m_oConexion.ADOCon.Errors)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CPlantConfVistaProceSobre", "IBaseDatos_EliminarDeBaseDatos", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If

End Function


Private Function IBaseDatos_FinalizarEdicionEliminando() As TipoErrorSummit

End Function


Private Function IBaseDatos_FinalizarEdicionModificando() As TipoErrorSummit
Dim TESError As TipoErrorSummit
Dim sConsulta As String


If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
    TESError.NumError = TESnoerror

    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
    sConsulta = "UPDATE PLANTILLA_CONF_VISTAS_PROCE_SOBRE SET VISIBLE=" & BooleanToSQLBinary(m_bVisible)
    sConsulta = sConsulta & " WHERE"
    sConsulta = sConsulta & " PLANT=" & m_oPlantConfVistaProc.Plantilla.Id
    sConsulta = sConsulta & " AND VISTA=" & m_oPlantConfVistaProc.Vista & " AND SOBRE=" & m_lSobre
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls

    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"

    IBaseDatos_FinalizarEdicionModificando = TESError

    Exit Function

Error_Cls:
    IBaseDatos_FinalizarEdicionModificando = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"

    On Error Resume Next
    If Not m_adores Is Nothing Then
        m_adores.Close
        Set m_adores = Nothing
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CPlantConfVistaProceSobre", "IBaseDatos_FinalizarEdicionModificando", ERR, Erl)
      Exit Function
   End If

End Function

Private Function IBaseDatos_IniciarEdicion(Optional ByVal Bloquear As Boolean, Optional ByVal UsuarioBloqueo As String) As TipoErrorSummit

End Function

Private Sub ConectarDeUseServer()

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If m_oConexionServer.ADOSummitCon Is Nothing Then
        Set m_oConexionServer.ADOSummitCon = New adodb.Connection
        m_oConexionServer.ADOSummitCon.Open "Provider=SQLOLEDB.1;Server=" & basParametros.ServidorGS & ";Database=" & basParametros.BaseDatosGS & ";", LICDBLogin, LICDBContra
        m_oConexionServer.ADOSummitCon.CursorLocation = adUseServer
        m_oConexionServer.ADOSummitCon.CommandTimeout = 120
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CPlantConfVistaProceSobre", "ConectarDeUseServer", ERR, Erl)
      Exit Sub
   End If
End Sub


Private Sub Class_Terminate()
On Error GoTo Error_Cls:
    Set m_oConexionServer = Nothing
    Set m_oConexion = Nothing
    Exit Sub
Error_Cls:
    Resume Next
End Sub


Public Function VistaAnyadirABaseDatos(ByVal iVista As Integer) As TipoErrorSummit
Dim sConsulta As String
Dim sConsultaVista As String
Dim TESError As TipoErrorSummit
Dim AdoRes As New adodb.Recordset


'********* Precondicion *******************
If m_oConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CConfVistaProce.AnyadirABAseDatos", "No se ha establecido la conexion"
End If
'******************************************


If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
    
    TESError.NumError = TESnoerror
    
    sConsulta = "INSERT INTO PLANTILLA_CONF_VISTAS_PROCE_SOBRE (PLANT,VISTA,SOBRE,VISIBLE)"
    sConsulta = sConsulta & " VALUES (" & m_oPlantConfVistaProc.Plantilla.Id
    sConsulta = sConsulta & "," & iVista & "," & m_lSobre
    sConsulta = sConsulta & "," & BooleanToSQLBinary(m_bVisible) & ")"

    
    m_oConexion.ADOCon.Execute sConsulta
    
    VistaAnyadirABaseDatos = TESError
    
    Exit Function
    
Error_Cls:
    
    VistaAnyadirABaseDatos = basErrores.TratarError(m_oConexion.ADOCon.Errors)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CPlantConfVistaProceSobre", "VistaAnyadirABaseDatos", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If

End Function









