VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CParametrosSM"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

''' *** Clase: CParametrosSM

''' Variables que almacenan las propiedades de los objetos
Private mCol As Collection
Private m_bEOF As Boolean
Private m_oConexion As CConexion

''' Control de errores
Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum



Public Function CargaAnyosDePartida(ByVal Anyo As Long, ByVal Gmn As String, ByVal Proce As Long, ByVal Item As Long) As Recordset
    Dim sConsulta As String
    Dim rs As adodb.Recordset
    Dim sPres As String

    Dim adocom As adodb.Command
    Dim oParam As adodb.Parameter
    Dim ador As adodb.Recordset
    
    If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR

    sConsulta = "SELECT IP.PRES0, IP.PRES1, IP.PRES2, IP.PRES3, IP.PRES4 FROM INSTANCIA_PRES5_DESGLOSE IP WITH(NOLOCK)"
    sConsulta = sConsulta & " INNER JOIN ITEM IT WITH(NOLOCK) ON IT.SOLICIT=IP.INSTANCIA AND IT.CAMPO_SOLICIT=IP.CAMPO_ORIGEN_PADRE AND IT.LINEA_SOLICIT=IP.LINEA"
    sConsulta = sConsulta & " WHERE IT.ANYO=" & Anyo & " AND IT.GMN1_PROCE=" & StrToSQLNULL(Gmn) & " AND IT.PROCE=" & Proce & " AND IT.ID=" & Item
    
    Set rs = New adodb.Recordset
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly

    Set adocom = New adodb.Command
    Set adocom.ActiveConnection = m_oConexion.ADOCon

    adocom.CommandType = adCmdStoredProc
    adocom.CommandText = "FSSM_BUSCAR_ANYOPARTIDA"
    
    If Not rs.eof Then
        Set oParam = adocom.CreateParameter("PRES0", adLongVarChar, adParamInput, 20, rs("PRES0").Value)
        adocom.Parameters.Append oParam
        
        sPres = "xx#"
        If Not IsNull(rs("PRES1").Value) Then sPres = sPres & rs("PRES1").Value
        If Not IsNull(rs("PRES2").Value) Then sPres = sPres & "|" & rs("PRES2").Value
        If Not IsNull(rs("PRES3").Value) Then sPres = sPres & "|" & rs("PRES3").Value
        If Not IsNull(rs("PRES4").Value) Then sPres = sPres & "|" & rs("PRES4").Value
        
        Set oParam = adocom.CreateParameter("PRES", adLongVarChar, adParamInput, 2000, sPres)
        adocom.Parameters.Append oParam
    Else
        Set oParam = adocom.CreateParameter("PRES0", adLongVarChar, adParamInput, 20, "")
        adocom.Parameters.Append oParam
        Set oParam = adocom.CreateParameter("PRES", adLongVarChar, adParamInput, 2000, "")
        adocom.Parameters.Append oParam
    End If
    rs.Close

    Set ador = adocom.Execute
    Set adocom.ActiveConnection = Nothing
    Set adocom = Nothing
    
    Set CargaAnyosDePartida = ador
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CParametrosSM", "CargaAnyosDePartida", ERR, Erl)
        Exit Function
    End If
End Function

''' <summary>
''' Carga todos los �rboles de la PARGEN_SM, con el nombres del nivel de imputaci�n en el idioma del usuario
''' No es necesario validar el idioma porque se usa el de parametros que se ha leido de BD
''' </summary>
''' <param name></param>
''' <returns></returns>
''' <remarks>Llamada desde:basSubMain.Main</remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>
Public Sub CargarTodosLosArboles()

    Dim sConsulta As String
    Dim rs As adodb.Recordset
    Dim fldPres5 As adodb.Field
    Dim fldImpModo As adodb.Field
    Dim fldImpSC As adodb.Field
    Dim fldImpPedido As adodb.Field
    Dim fldImpRecep As adodb.Field
    Dim fldImpNivel As adodb.Field
    Dim fldVigencia As adodb.Field
    Dim fldDispSC As adodb.Field
    Dim fldComprAcum As adodb.Field
    Dim fldNom1 As adodb.Field
    Dim fldNom2 As adodb.Field
    Dim fldNom3 As adodb.Field
    Dim fldNom4 As adodb.Field
    Dim fldAcumularImpuestos As adodb.Field
    Dim fldControlDispConPres As adodb.Field
    Dim fldPlurianual As adodb.Field
    Dim oParamSM As CParametroSM
        
   
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sConsulta = "SELECT PRES5,IMPMODO,IMP_SC,IMP_PEDIDO,IMP_RECEP,IMP_NIVEL,VIGENCIA,DISP_SC,COMPR_ACUM,NOM_NIVEL1,NOM_NIVEL2,NOM_NIVEL3,NOM_NIVEL4 ,ACUMULARIMP, CONTROL_DISP_SC_CON_PRES, PLURIANUAL  "
    sConsulta = sConsulta & " FROM PARGEN_SM SM WITH (NOLOCK) "
    sConsulta = sConsulta & " INNER JOIN PRES5_IDIOMAS PI WITH (NOLOCK) ON PI.PRES0=SM.PRES5 AND PI.PRES1 IS NULL AND PI.PRES2 IS NULL AND PI.PRES3 IS NULL AND PI.PRES4 IS NULL "
    sConsulta = sConsulta & " WHERE PI.IDIOMA='" & gParametrosInstalacion.gIdioma & "' ORDER BY SM.ID"
    
    Set rs = New adodb.Recordset
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
        
    If rs.eof Then
        rs.Close
        Set rs = Nothing
        Set mCol = Nothing
        Set mCol = New Collection
        Set g_oParametrosSM = Nothing
        Exit Sub
    Else
        Set mCol = Nothing
        Set mCol = New Collection
        Set fldPres5 = rs.Fields("PRES5")
        Set fldImpModo = rs.Fields("IMPMODO")
        Set fldImpSC = rs.Fields("IMP_SC")
        Set fldImpPedido = rs.Fields("IMP_PEDIDO")
        Set fldImpRecep = rs.Fields("IMP_RECEP")
        Set fldImpNivel = rs.Fields("IMP_NIVEL")
        Set fldVigencia = rs.Fields("VIGENCIA")
        Set fldDispSC = rs.Fields("DISP_SC")
        Set fldComprAcum = rs.Fields("COMPR_ACUM")
        Set fldNom1 = rs.Fields("NOM_NIVEL1")
        Set fldNom2 = rs.Fields("NOM_NIVEL2")
        Set fldNom3 = rs.Fields("NOM_NIVEL3")
        Set fldNom4 = rs.Fields("NOM_NIVEL4")
        Set fldAcumularImpuestos = rs.Fields("ACUMULARIMP")
        Set fldControlDispConPres = rs.Fields("CONTROL_DISP_SC_CON_PRES")
        Set fldPlurianual = rs.Fields("PLURIANUAL")
        While Not rs.eof
            'Se a�ade el �rbol a la colecci�n.
            Set oParamSM = Me.Add(fldPres5.Value, fldImpModo.Value, fldImpSC.Value, fldImpPedido.Value, fldImpRecep.Value, fldImpNivel.Value, fldVigencia.Value, fldDispSC.Value, fldComprAcum.Value, fldAcumularImpuestos.Value, fldControlDispConPres.Value, fldPlurianual.Value)
            Select Case oParamSM.ImpNivel
            Case 1
                oParamSM.NomNivelImputacion = fldNom1.Value
            Case 2
                oParamSM.NomNivelImputacion = fldNom2.Value
            Case 3
                oParamSM.NomNivelImputacion = fldNom3.Value
            Case 4
                oParamSM.NomNivelImputacion = fldNom4.Value
            End Select
            rs.MoveNext
        Wend

        rs.Close
        Set rs = Nothing
        Set fldPres5 = Nothing
        Set fldImpModo = Nothing
        Set fldImpSC = Nothing
        Set fldImpPedido = Nothing
        Set fldImpRecep = Nothing
        Set fldImpNivel = Nothing
        Set fldVigencia = Nothing
        Set fldDispSC = Nothing
        Set fldComprAcum = Nothing
        Set fldNom1 = Nothing
        Set fldNom2 = Nothing
        Set fldNom3 = Nothing
        Set fldNom4 = Nothing
        Set fldAcumularImpuestos = Nothing
        Set fldControlDispConPres = Nothing
        Set fldPlurianual = Nothing
    End If
    
    Set g_oParametrosSM = Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CParametrosSM", "CargarTodosLosArboles", ERR, Erl)
      Exit Sub
   End If
        
End Sub


Public Function Add(ByVal sPRES5 As String, ByVal iImpModo As Integer, ByVal iImpSC As Integer, ByVal iImpPedido As Integer, ByVal iImpRecep As Integer, ByVal iImpNivel As Integer, ByVal iVigencia As Integer, ByVal iDispSC As Integer, ByVal iComprAcum As Integer, ByVal bAcumularImpuesto As Boolean, ByVal bControlDispConPres As Boolean, ByVal bPlurianual) As CParametroSM

    ''' * Objetivo: A�adir un arbol a la coleccion
    ''' * Recibe: Datos del arbol
    ''' * Devuelve: Arbol a�adido
    
    Dim objnewmember As CParametroSM
    
    ''' Creacion de objeto �rbol
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New CParametroSM
   
    ''' Paso de los parametros al nuevo objeto �rbol.
    objnewmember.PRES5 = sPRES5
    objnewmember.ImpModo = iImpModo
    objnewmember.ImpSC = iImpSC
    objnewmember.ImpPedido = iImpPedido
    objnewmember.ImpRecep = iImpRecep
    objnewmember.ImpNivel = iImpNivel
    objnewmember.Vigencia = iVigencia
    objnewmember.DispSC = iDispSC
    objnewmember.ComprAcum = iComprAcum
    objnewmember.acumularImpuestos = bAcumularImpuesto
    objnewmember.ControlDispConPres = bControlDispConPres
    objnewmember.PluriAnual = bPlurianual
    Set objnewmember.Conexion = m_oConexion
        
    ''' A�adir el objeto arbol a la coleccion
    ''' Si no se especifica indice, se a�ade al final
    mCol.Add objnewmember, CStr(sPRES5)
            
    Set Add = objnewmember
    
    Set objnewmember = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CParametrosSM", "Add", ERR, Erl)
      Exit Function
   End If

End Function


Friend Property Set Conexion(ByVal con As CConexion)

    ''' * Objetivo: Dar valor a la variable privada Conexion
    ''' * Recibe: Conexion
    ''' * Devuelve: Nada
    
    Set m_oConexion = con
    
End Property
Friend Property Get Conexion() As CConexion

    ''' * Objetivo: Devolver variable privada Conexion
    ''' * Recibe: Nada
    ''' * Devuelve: Conexion
    
    Set Conexion = m_oConexion
    
End Property


Public Property Get Count() As Long

    ''' * Objetivo: Devolver variable privada Count
    ''' * Recibe: Nada
    ''' * Devuelve: Numero de elementos de la coleccion

    If mCol Is Nothing Then
        Count = 0
    Else
        Count = mCol.Count
    End If

End Property
Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
     Set NewEnum = mCol.[_NewEnum]
End Property
Public Sub Remove(vntIndexKey As Variant)
On Error GoTo NoSeEncuentra:

    mCol.Remove vntIndexKey
    Exit Sub

NoSeEncuentra:
    
End Sub

Public Property Get Item(vntIndexKey As Variant) As CParametroSM

    ''' * Objetivo: Recuperar un �rbol de la coleccion
    ''' * Recibe: Indice del �rbol a recuperar
    ''' * Devuelve: �rbol correspondiente
        
On Error GoTo NoSeEncuentra:

    ''' Devolvemos el item de la coleccion privada
    
    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:

    Set Item = Nothing
    
End Property


''' <summary>
''' Funci�n que me dice si un usuario tiene permiso para un centro de coste al que pertenece un contrato en concreto.
''' </summary>
''' <param name="psContrato">Contrato</param>
''' <returns>True : si tiene permiso; False : si no tiene permiso.</returns>
''' <remarks>Llamada desde:frmPedidos.txtContrato_Validate</remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>

Public Function PermisoParaEseCC(ByVal psContrato As String, ByVal oArbol As CParametroSM, ByVal sUsu As String) As Boolean
Dim sConsulta As String
Dim rs As New adodb.Recordset
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sConsulta = "SELECT COUNT(*) from PRES5_UON P WITH (NOLOCK)"
    sConsulta = sConsulta & "INNER JOIN USU_CC_IMPUTACION U WITH (NOLOCK) on ISNULL(P.UON1,0)=ISNULL(U.UON1,0) and ISNULL(P.UON2,0)=ISNULL(U.UON2,0) and ISNULL(P.UON3,0)=ISNULL(U.UON3,0) and ISNULL(P.UON4,0)=ISNULL(U.UON4,0) "
    sConsulta = sConsulta & "WHERE U.USU='" & DblQuote(sUsu) & "' AND P.PRES0='" & DblQuote(oArbol.PRES5) & "' "
    
    Select Case oArbol.ImpNivel
    Case 1
        sConsulta = sConsulta & "AND P.PRES1='" & DblQuote(psContrato) & "' AND P.PRES2 IS NULL AND P.PRES3 IS NULL AND P.PRES4 IS NULL "
    Case 2
        sConsulta = sConsulta & "AND P.PRES2='" & DblQuote(psContrato) & "' AND P.PRES3 IS NULL AND P.PRES4 IS NULL "
    Case 3
        sConsulta = sConsulta & "AND P.PRES3='" & DblQuote(psContrato) & "' AND P.PRES4 IS NULL "
    Case 4
        sConsulta = sConsulta & "AND P.PRES4='" & DblQuote(psContrato) & "' "
    End Select
    
    rs.CursorLocation = adUseClient
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    
    If rs(0).Value > 0 Then
        PermisoParaEseCC = True
    Else
        PermisoParaEseCC = False
    End If
    
    rs.Close
    Set rs.ActiveConnection = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CParametrosSM", "PermisoParaEseCC", ERR, Erl)
      Exit Function
   End If
    
End Function

Private Sub Class_Initialize()
    Set mCol = New Collection
End Sub

Private Sub Class_Terminate()
    'destroys collection when this class is terminated
    Set mCol = Nothing
    Set m_oConexion = Nothing

End Sub

