VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CTipoPedidoCats"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

''' Variables que almacenan las propiedades de los objetos

Private mCol As Collection
Private m_bEOF As Boolean
Private m_oConexion As CConexion

''' Control de errores
Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum


''' <summary>
''' Carga todas las categorias de tipos de pedido.
''' </summary>
''' <param name="sIdi">Es el idioma de la aplicaci�n</param>
''' <returns></returns>
''' <remarks>Llamada desde:frmMantPedidosCat.Form_Load</remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>
Public Sub CargarTodasLasCategoriasTiposPedidos(ByVal sID As String)

    Dim sConsulta As String
    Dim rs As adodb.Recordset
    Dim fldId As adodb.Field
    Dim fldTipopedidoId As adodb.Field
    Dim fldCATN As adodb.Field
    Dim fldNivel As adodb.Field
    Dim fldFecAct As adodb.Field

        
    ''' * Objetivo: Cargar la coleccion de categorias de un tipo de pedido
    
    ''' Precondicion
    
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CTipoPedidoCats.CargarTodasLasCategoriasTiposPedidos", "No se ha establecido la conexion"
        Exit Sub
    End If
    
    If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    

    sConsulta = "SELECT ID, TIPOPEDIDO_ID, CATN, NIVEL, FECACT FROM TIPOPEDIDO_CATN WITH (NOLOCK) "
    sConsulta = sConsulta & "WHERE TIPOPEDIDO_ID = '" & DblQuote(sID) & "' "
    sConsulta = sConsulta & "ORDER BY NIVEL, CATN"
    
    Set rs = New adodb.Recordset
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    
    
    If rs.eof Then
        rs.Close
        Set rs = Nothing
        Set mCol = Nothing
        Set mCol = New Collection
        Exit Sub
    Else
        Set mCol = Nothing
        Set mCol = New Collection
        Set fldId = rs.Fields("ID")
        Set fldTipopedidoId = rs.Fields("TIPOPEDIDO_ID")
        Set fldCATN = rs.Fields("CATN")
        Set fldNivel = rs.Fields("NIVEL")
        Set fldFecAct = rs.Fields("FECACT")
                
        While Not rs.eof
            Me.Add fldId.Value, fldTipopedidoId.Value, fldCATN.Value, fldNivel.Value, fldFecAct.Value
            rs.MoveNext
        Wend

        rs.Close
        Set rs = Nothing
        Set fldId = Nothing
        Set fldTipopedidoId = Nothing
        Set fldCATN = Nothing
        Set fldNivel = Nothing
        Set fldFecAct = Nothing
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CTipoPedidoCats", "CargarTodasLasCategoriasTiposPedidos", ERR, Erl)
      Exit Sub
   End If
        
End Sub




Public Function Add(ByVal lId As Long, ByVal lTipoPedidoId As Long, ByVal lCATN As Long, ByVal iNivel As Integer, ByVal vFecAct As Variant) As CTipoPedidoCat
    
    ''' * Objetivo: Anyadir una categoria de tipo de pedido a la coleccion
    
    Dim objnewmember As CTipoPedidoCat
    Dim sIndice As String
    
    
    
    ''' Creacion de objeto arbol
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New CTipoPedidoCat
   
    ''' Paso de los parametros al nuevo objeto
    objnewmember.Id = lId
    objnewmember.TipoPedidoId = lTipoPedidoId
    objnewmember.CATN = lCATN
    objnewmember.Nivel = iNivel
    objnewmember.FecAct = vFecAct

                
    Set objnewmember.Conexion = m_oConexion
    
    sIndice = CStr(lTipoPedidoId) & "-" & CStr(lCATN) & "-Nivel-" + CStr(iNivel)
    
    mCol.Add objnewmember, sIndice
    ''' Anyadir el objeto a la coleccion
            
    Set Add = objnewmember
        
    Set objnewmember = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CTipoPedidoCats", "Add", ERR, Erl)
      Exit Function
   End If

End Function


Friend Property Set Conexion(ByVal con As CConexion)

    ''' * Objetivo: Dar valor a la variable privada Conexion
    ''' * Recibe: Conexion
    ''' * Devuelve: Nada
    
    Set m_oConexion = con
    
End Property
Friend Property Get Conexion() As CConexion

    ''' * Objetivo: Devolver variable privada Conexion
    ''' * Recibe: Nada
    ''' * Devuelve: Conexion
    
    Set Conexion = m_oConexion
    
End Property


Public Property Get Count() As Long

    ''' * Objetivo: Devolver variable privada Count
    ''' * Recibe: Nada
    ''' * Devuelve: Numero de elementos de la coleccion

    If mCol Is Nothing Then
        Count = 0
    Else
        Count = mCol.Count
    End If

End Property



Public Sub Remove(vntIndexKey As Variant)

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    mCol.Remove vntIndexKey
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CTipoPedidoCats", "Remove", ERR, Erl)
      Exit Sub
   End If
End Sub



Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
     Set NewEnum = mCol.[_NewEnum]
End Property




Public Property Get Item(vntIndexKey As Variant) As CTipoPedidoCat

    ''' * Objetivo: Recuperar un �rbol de la coleccion
    ''' * Recibe: Indice del �rbol a recuperar
    ''' * Devuelve: �rbol correspondiente
        
On Error GoTo NoSeEncuentra:

    ''' Devolvemos el item de la coleccion privada
    
    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:

    Set Item = Nothing
    
End Property


Private Sub Class_Initialize()
    Set mCol = New Collection
End Sub

Private Sub Class_Terminate()
    Set mCol = Nothing
    Set m_oConexion = Nothing
End Sub




'3328
Public Function EliminarCategoriasTipoPedido(ByVal m_lTipoPedidoId As Long) As TipoErrorSummit
Dim sConsulta As String
Dim TESError As TipoErrorSummit
Dim bTransaccionEnCurso As Boolean
Dim rs As adodb.Recordset

    '********* Precondicion **************************************
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CCategoriaN1.DeshacerBajaLogica", "No se ha establecido la conexion"
        Exit Function
    End If
    '*************************************************************
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

If g_oErrores.fg_bProgramando Then On Error GoTo error_cls:

    TESError.NumError = TESnoerror
    
    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
    bTransaccionEnCurso = True
        
    sConsulta = "DELETE FROM TIPOPEDIDO_CATN WHERE TIPOPEDIDO_ID=" & m_lTipoPedidoId & ""
    m_oConexion.ADOCon.Execute sConsulta
    
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo error_cls
    
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
        
    EliminarCategoriasTipoPedido = TESError
    Exit Function
    
    
error_cls:
    EliminarCategoriasTipoPedido = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    
    If bTransaccionEnCurso Then
        m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CTipoPedidoCats", "EliminarCategoriasTipoPedido", ERR, Erl)
      GoTo error_cls
      Exit Function
   End If

End Function



