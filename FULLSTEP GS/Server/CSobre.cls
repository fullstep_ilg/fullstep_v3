VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CSobre"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CSobre **********************************
'*             Autor : epb
'*             Creada : 13/01/2003
'***************************************************************

Option Explicit

Implements IBaseDatos

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

Private m_oProceso As cProceso
Private m_iSobre As Integer
Private m_vFechaPrevistaAper As Variant
Private m_sUsuario As String
Private m_iEstado As Integer  'Puede ser 0 cerrado 1 abierto
Private m_sEstadoCrypt As String
Private m_sFechaCrypt As String
Private m_vObserv As Variant
Private m_vFechaRealAper As Variant
Private m_vFecAct As Variant
Private m_oConexion As CConexion
Private m_vIndice As Variant


Public Property Set Proceso(ByVal oProce As cProceso)
    Set m_oProceso = oProce
End Property

Public Property Get Proceso() As cProceso
    Set Proceso = m_oProceso
End Property

Public Property Get Sobre() As Integer
    Sobre = m_iSobre
End Property

Public Property Let Sobre(ByVal iCod As Integer)
    m_iSobre = iCod
End Property
Public Property Get FechaAperturaPrevista() As Variant
    FechaAperturaPrevista = m_vFechaPrevistaAper
End Property

Public Property Let FechaAperturaPrevista(ByVal dFecha As Variant)
    m_vFechaPrevistaAper = dFecha
End Property


Friend Property Let FechaEncriptada(ByVal sFecha As String)
    m_sFechaCrypt = sFecha
End Property
Friend Property Let EstadoEncriptado(ByVal sFecha As String)
    m_sEstadoCrypt = sFecha
End Property
Friend Property Get FechaEncriptada() As String
    FechaEncriptada = m_sFechaCrypt
End Property
Friend Property Get EstadoEncriptado() As String
    EstadoEncriptado = m_sEstadoCrypt
End Property

Public Property Get FechaAperturaReal() As Variant
    FechaAperturaReal = m_vFechaRealAper
End Property

Public Property Let FechaAperturaReal(ByVal vFecha As Variant)
    m_vFechaRealAper = vFecha
End Property

Public Property Let Usuario(ByVal sDato As String)
    m_sUsuario = sDato
End Property

Public Property Get Usuario() As String
    Usuario = m_sUsuario
End Property
Public Property Let Estado(ByVal dato As Integer)
    m_iEstado = dato
End Property

Public Property Get Estado() As Integer
    Estado = m_iEstado
End Property

Public Property Let Observaciones(ByVal vObs As Variant)
    m_vObserv = vObs
End Property

Public Property Get Observaciones() As Variant
     Observaciones = m_vObserv
End Property

Public Property Let FecAct(ByVal vFec As Variant)
    m_vFecAct = vFec
End Property

Public Property Get FecAct() As Variant
    FecAct = m_vFecAct
End Property

Friend Property Set Conexion(ByVal con As CConexion)
    Set m_oConexion = con
End Property

Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property

Public Property Let Indice(ByVal vIndice As Variant)
    m_vIndice = vIndice
End Property
Public Property Get Indice() As Variant
    Indice = m_vIndice
End Property



Private Function IBaseDatos_CambiarCodigo(CodigoNuevo As Variant) As TipoErrorSummit

End Function

''' <summary>Esta funci�n inserta un sobre en la tabla</summary>
''' <returns>TipoErrorSummit con el error si lo hay</returns>
''' <remarks>Llamada desde: FSGClient
''' Tiempo m�ximo: 1 sec </remarks>
''' <revision>JVS 09/01/2012</revision>
Private Function IBaseDatos_AnyadirABaseDatos() As TipoErrorSummit
Dim sConsulta As String
Dim TESError As TipoErrorSummit
Dim AdoRes As adodb.Recordset

'********* Precondicion *******************
If m_oConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CSobre.AnyadirABAseDatos", "No se ha establecido la conexion"
End If

'******************************************
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------


If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
    
    TESError.NumError = TESnoerror
    'Encriptar la fecha y estado
    m_sFechaCrypt = EncriptarAperturaSobre(CStr(m_oProceso.Anyo) & CStr(m_oProceso.Cod) & CStr(m_iSobre), Format(m_vFechaPrevistaAper, "dd/mm/yyyy hh:mm"), True)
    If m_iEstado = 1 Then
        m_sEstadoCrypt = EncriptarAperturaSobre(CStr(m_oProceso.Anyo) & CStr(m_oProceso.Cod) & CStr(m_iSobre), "Abierto", True)
    Else
        m_sEstadoCrypt = EncriptarAperturaSobre(CStr(m_oProceso.Anyo) & CStr(m_oProceso.Cod) & CStr(m_iSobre), "Cerrado", True)
    End If
        
        m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
        'CALIDAD Se obtiene la Fecha con bloqueo para recuperar la verdadera �ltima fecha de actualizaci�n
        sConsulta = "SELECT EST,FECVAL,USUVAL FROM PROCE"
        sConsulta = sConsulta & " WHERE ANYO=" & m_oProceso.Anyo & " AND COD=" & m_oProceso.Cod & " AND GMN1='" & DblQuote(m_oProceso.GMN1Cod) & "'"
        Set AdoRes = New adodb.Recordset
        AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
        If AdoRes("EST").Value >= TipoEstadoProceso.validado Then
            TESError.NumError = TESValidarYaValidado
            TESError.Arg1 = 83 '"El proceso ya ha sido validado. Fecha y usuario : "
            TESError.Arg2 = Format(AdoRes("FECVAL").Value, "Short Date") & " " & AdoRes("USUVAL").Value
            AdoRes.Close
            Set AdoRes = Nothing
            IBaseDatos_AnyadirABaseDatos = TESError
            m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
            Exit Function
        End If
        AdoRes.Close

        sConsulta = "INSERT INTO SOBRE (ANYO,GMN1,PROCE,SOBRE,FECAPE,USUAPER,EST,OBS)"
        sConsulta = sConsulta & " VALUES (" & m_oProceso.Anyo & "," & StrToSQLNULL(m_oProceso.GMN1Cod) & "," & m_oProceso.Cod
        sConsulta = sConsulta & "," & m_iSobre
        sConsulta = sConsulta & "," & StrToSQLNULL(m_sFechaCrypt)
        sConsulta = sConsulta & "," & StrToSQLNULL(m_sUsuario)
        sConsulta = sConsulta & "," & StrToSQLNULL(m_sEstadoCrypt)
        sConsulta = sConsulta & "," & StrToSQLNULL(m_vObserv) & ")"
        m_oConexion.ADOCon.Execute sConsulta
        If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
        m_iEstado = 0
        
        'CALIDAD Se obtiene la Fecha de Actualizaci�n con bloqueo para recuperar la verdadera �ltima fecha de actualizaci�n
        AdoRes.Open "SELECT FECACT FROM SOBRE WHERE ANYO=" & m_oProceso.Anyo & " AND GMN1 ='" & DblQuote(m_oProceso.GMN1Cod) & "' AND PROCE=" & m_oProceso.Cod & " AND SOBRE=" & m_iSobre, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
        m_vFecAct = AdoRes(0).Value
        AdoRes.Close
        Set AdoRes = Nothing
            
        m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
        IBaseDatos_AnyadirABaseDatos = TESError
        
Finalizar:
    On Error Resume Next
    If Not AdoRes Is Nothing Then
        AdoRes.Close
        Set AdoRes = Nothing
    End If

    Exit Function

Error_Cls:
    
    IBaseDatos_AnyadirABaseDatos = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    
    m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"

    Resume Finalizar
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      'Call g_oErrores.TratarError("Class Module", "CSobre", "IBaseDatos_AnyadirABaseDatos", Err, Erl)
      GoTo Error_Cls
      Exit Function
   End If
End Function

''' <summary>Esta funci�n finaliza la edici�n del resulset modificando</summary>
''' <returns>TipoErrorSummit con el error si lo hay</returns>
''' <remarks>Llamada desde: FSGClient
''' Tiempo m�ximo: <1 seg </remarks>
''' <revision>JVS 09/01/2012</revision>
Private Function IBaseDatos_FinalizarEdicionModificando() As TipoErrorSummit
Dim TESError As TipoErrorSummit
Dim sConsulta As String
Dim AdoRes As adodb.Recordset

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls

    TESError.NumError = TESnoerror
    'Encriptar la fecha y estado
    m_sFechaCrypt = EncriptarAperturaSobre(CStr(m_oProceso.Anyo) & CStr(m_oProceso.Cod) & CStr(m_iSobre), Format(m_vFechaPrevistaAper, "dd/mm/yyyy hh:mm"), True)
    If m_iEstado = 1 Then
        m_sEstadoCrypt = EncriptarAperturaSobre(CStr(m_oProceso.Anyo) & CStr(m_oProceso.Cod) & CStr(m_iSobre), "Abierto", True)
    Else
        m_sEstadoCrypt = EncriptarAperturaSobre(CStr(m_oProceso.Anyo) & CStr(m_oProceso.Cod) & CStr(m_iSobre), "Cerrado", True)
    End If
    
    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
        'CALIDAD Se obtiene la Fecha con bloqueo para recuperar la verdadera �ltima fecha de actualizaci�n
        sConsulta = "SELECT EST,FECVAL,USUVAL FROM PROCE"
        sConsulta = sConsulta & " WHERE ANYO=" & m_oProceso.Anyo & " AND COD=" & m_oProceso.Cod & " AND GMN1='" & DblQuote(m_oProceso.GMN1Cod) & "'"
        Set AdoRes = New adodb.Recordset
        AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
        If AdoRes("EST").Value >= TipoEstadoProceso.validado Then
            TESError.NumError = TESValidarYaValidado
            TESError.Arg1 = 83 '"El proceso ya ha sido validado. Fecha y usuario : "
            TESError.Arg2 = Format(AdoRes("FECVAL").Value, "Short Date") & " " & AdoRes("USUVAL").Value
            AdoRes.Close
            Set AdoRes = Nothing
            IBaseDatos_FinalizarEdicionModificando = TESError
            m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
            Exit Function
        End If
        AdoRes.Close
        
        'CALIDAD Se obtiene la Fecha de Actualizaci�n con bloqueo para recuperar la verdadera �ltima fecha de actualizaci�n
        sConsulta = "SELECT FECACT FROM SOBRE"
        sConsulta = sConsulta & " WHERE ANYO=" & m_oProceso.Anyo & " AND PROCE=" & m_oProceso.Cod & " AND GMN1='" & DblQuote(m_oProceso.GMN1Cod) & "' AND SOBRE=" & m_iSobre
        AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
        
        If AdoRes.eof Then
            AdoRes.Close
            Set AdoRes = Nothing
            TESError.NumError = TESDatoEliminado
            TESError.Arg1 = 126 'SOBRE
            IBaseDatos_FinalizarEdicionModificando = TESError
            m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
            Exit Function
        End If
        
        If m_vFecAct <> AdoRes("FECACT").Value Then
            AdoRes.Close
            Set AdoRes = Nothing
            TESError.NumError = TESInfActualModificada
            IBaseDatos_FinalizarEdicionModificando = TESError
            m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
            Exit Function
        End If
            
        sConsulta = "UPDATE SOBRE SET FECAPE='" & DblQuote(m_sFechaCrypt) & "',USUAPER=" & StrToSQLNULL(m_sUsuario)
        sConsulta = sConsulta & ",EST=" & StrToSQLNULL(m_sEstadoCrypt) & ",OBS=" & StrToSQLNULL(m_vObserv)
        sConsulta = sConsulta & " WHERE ANYO=" & m_oProceso.Anyo & " AND GMN1='" & DblQuote(m_oProceso.GMN1Cod) & "' AND PROCE=" & m_oProceso.Cod & " AND SOBRE=" & m_iSobre
        
        m_oConexion.ADOCon.Execute sConsulta
        If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
        
        If Not AdoRes Is Nothing Then
            AdoRes.Requery
            m_vFecAct = AdoRes("FECACT").Value
            
            'CALIDAD Se obtiene la Fecha de Actualizaci�n con bloqueo para recuperar la verdadera �ltima fecha de actualizaci�n
            sConsulta = "SELECT FECACT FROM PROCE"
            sConsulta = sConsulta & " WHERE ANYO=" & m_oProceso.Anyo & " AND GMN1='" & DblQuote(m_oProceso.GMN1Cod) & "' AND COD='" & DblQuote(m_oProceso.Cod) & "'"
            AdoRes.Close
            AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
            m_oProceso.FecAct = AdoRes("FECACT").Value
            
            AdoRes.Close
            Set AdoRes = Nothing
        End If
                
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    IBaseDatos_FinalizarEdicionModificando = TESError
    

Finalizar:
    On Error Resume Next
    If Not AdoRes Is Nothing Then
        AdoRes.Close
        Set AdoRes = Nothing
    End If
    Exit Function
Error_Cls:

    IBaseDatos_FinalizarEdicionModificando = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    
    m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
    
    Resume Finalizar
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      'Call g_oErrores.TratarError("Class Module", "CSobre", "IBaseDatos_FinalizarEdicionModificando", Err, Erl)
      GoTo Error_Cls
      Exit Function
   End If
End Function
Private Function IBaseDatos_FinalizarEdicionEliminando() As TipoErrorSummit

End Function

''' <summary>Esta funci�n inicia la edici�n del resulset</summary>
'''<param optional name="Bloquear">Si se bloquea o no</param>
'''<param optional name="UsuarioBloqueo">Usuario del bloqueo</param>
''' <returns>TipoErrorSummit con el error si lo hay</returns>
''' <remarks>Llamada desde: FSGClient
''' Tiempo m�ximo: <1 seg </remarks>
''' <revision>JVS 09/01/2012</revision>
Private Function IBaseDatos_IniciarEdicion(Optional ByVal Bloquear As Boolean, Optional ByVal UsuarioBloqueo As String) As TipoErrorSummit
Dim sConsulta As String
Dim TESError As TipoErrorSummit
Dim m_adores As adodb.Recordset

'******************* Precondicion *******************
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If m_oConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CSobre.IniciarEdicion", "No se ha establecido la conexion"
End If
'*****************************************************
         
        TESError.NumError = TESnoerror

        'CALIDAD Se obtiene la Fecha de Actualizaci�n con bloqueo para recuperar la verdadera �ltima fecha de actualizaci�n
        sConsulta = "SELECT FECACT FROM SOBRE"
        sConsulta = sConsulta & " WHERE ANYO=" & m_oProceso.Anyo & " AND PROCE=" & m_oProceso.Cod & " AND GMN1='" & DblQuote(m_oProceso.GMN1Cod) & "' AND SOBRE=" & m_iSobre
      
        Set m_adores = New adodb.Recordset
        m_adores.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
      
        If m_adores.eof Then
            m_adores.Close
            Set m_adores = Nothing
            TESError.NumError = TESDatoEliminado
            TESError.Arg1 = 126 'Sobre
            IBaseDatos_IniciarEdicion = TESError
            Exit Function
        End If
            
        If m_vFecAct <> m_adores("FECACT") Then
            m_adores.Close
            Set m_adores = Nothing
            TESError.NumError = TESInfActualModificada
            IBaseDatos_IniciarEdicion = TESError
            Exit Function
        End If
        
        m_adores.Close
        Set m_adores = Nothing
        
        IBaseDatos_IniciarEdicion = TESError
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CSobre", "IBaseDatos_IniciarEdicion", ERR, Erl)
      Exit Function
   End If

End Function

''' <summary>
''' Elimina un sobre
''' </summary>
''' <returns>Un errorsummit</returns>
''' <remarks>Llamada desde: FSGSClient;
''' Tiempo m�ximo: <1 seg</remarks>
''' <revision>JVS 09/01/2012</revision>
Private Function IBaseDatos_EliminarDeBaseDatos() As TipoErrorSummit
Dim sConsulta As String
Dim TESError As TipoErrorSummit
Dim adoComm As adodb.Command
Dim adoParam As adodb.Parameter
Dim AdoRes As adodb.Recordset
Dim btrans As Boolean

'******************* Precondicion *******************
If m_oConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CProceso.CambiarTipoDeProceso", "No se ha establecido la conexion"
End If
'*****************************************************
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
TESError.NumError = TESnoerror


If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:

m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
btrans = True

        'CALIDAD Se obtiene la Fecha con bloqueo para recuperar la verdadera �ltima fecha de actualizaci�n
        sConsulta = "SELECT EST,FECVAL,USUVAL FROM PROCE"
        sConsulta = sConsulta & " WHERE ANYO=" & m_oProceso.Anyo & " AND COD=" & m_oProceso.Cod & " AND GMN1='" & DblQuote(m_oProceso.GMN1Cod) & "'"
        Set AdoRes = New adodb.Recordset
        AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
        If AdoRes("EST").Value >= TipoEstadoProceso.validado Then
            TESError.NumError = TESValidarYaValidado
            TESError.Arg1 = 83 '"El proceso ya ha sido validado. Fecha y usuario : "
            TESError.Arg2 = Format(AdoRes("FECVAL").Value, "Short Date") & " " & AdoRes("USUVAL").Value
            AdoRes.Close
            Set AdoRes = Nothing
            IBaseDatos_EliminarDeBaseDatos = TESError
            m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
            Exit Function
        End If
        AdoRes.Close
        Set AdoRes = Nothing

        Set adoComm = New adodb.Command
    
        Set adoComm.ActiveConnection = m_oConexion.ADOCon
        sConsulta = "EXEC SP_ELIMINAR_SOBRE @ANYO=?, @GMN1=?, @PROCE=?, @SOBRE=?"
        
        Set adoParam = adoComm.CreateParameter("ANYO", adSmallInt, adParamInput, , m_oProceso.Anyo)
        adoComm.Parameters.Append adoParam
    
        Set adoParam = adoComm.CreateParameter("GMN1", adVarChar, adParamInput, 50, m_oProceso.GMN1Cod)
        adoComm.Parameters.Append adoParam
    
        Set adoParam = adoComm.CreateParameter("PROCE", adInteger, adParamInput, , m_oProceso.Cod)
        adoComm.Parameters.Append adoParam
            
        Set adoParam = adoComm.CreateParameter("SOBRE", adSmallInt, adParamInput, , m_iSobre)
        adoComm.Parameters.Append adoParam
    
        adoComm.CommandType = adCmdText
        adoComm.CommandText = sConsulta
    
        adoComm.Execute
        If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR
        
        Set adoComm = Nothing
        Set adoParam = Nothing


m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
btrans = False

IBaseDatos_EliminarDeBaseDatos = TESError

Exit Function

Error_Cls:
    IBaseDatos_EliminarDeBaseDatos = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    If btrans Then
        m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      'Call g_oErrores.TratarError("Class Module", "CSobre", "IBaseDatos_EliminarDeBaseDatos", Err, Erl)
      GoTo Error_Cls
      Exit Function
   End If

End Function
Private Sub IBaseDatos_CancelarEdicion()

End Sub
Private Function IBaseDatos_ComprobarExistenciaEnBaseDatos() As Boolean

End Function

''' <summary>
''' Abrir sobre
''' </summary>
''' <returns>Un errorsummit</returns>
''' <remarks>Llamada desde: frmAbrirSobre.cmdAceptar_Click;
''' Tiempo m�ximo: <1 seg</remarks>
''' <revision>JVS 09/01/2012</revision>
Public Function AbrirSobre() As TipoErrorSummit
Dim TESError As TipoErrorSummit
Dim adoComm As adodb.Command
Dim adoParam As adodb.Parameter
Dim AdoRes As adodb.Recordset
Dim btrans As Boolean
Dim sConsulta As String

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls

TESError.NumError = TESnoerror


        m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
        
        Set AdoRes = New adodb.Recordset
        
        'CALIDAD Se obtiene la Fecha de Actualizaci�n con bloqueo para recuperar la verdadera �ltima fecha de actualizaci�n
        sConsulta = "SELECT FECACT FROM SOBRE"
        sConsulta = sConsulta & " WHERE ANYO=" & m_oProceso.Anyo & " AND PROCE=" & m_oProceso.Cod & " AND GMN1='" & DblQuote(m_oProceso.GMN1Cod) & "' AND SOBRE=" & m_iSobre
        AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
        
        If AdoRes.eof Then
            AdoRes.Close
            Set AdoRes = Nothing
            TESError.NumError = TESDatoEliminado
            TESError.Arg1 = 126 'SOBRE
            AbrirSobre = TESError
            m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
            Exit Function
        End If
        
        If m_vFecAct <> AdoRes("FECACT").Value Then
            AdoRes.Close
            Set AdoRes = Nothing
            TESError.NumError = TESInfActualModificada
            AbrirSobre = TESError
            m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
            Exit Function
        End If
            
        sConsulta = "UPDATE SOBRE SET FECEST=? ,EST=? "
        sConsulta = sConsulta & " WHERE ANYO=? AND GMN1=? AND PROCE=? AND SOBRE=?"
        
        Set adoComm = New adodb.Command
        
        Set adoComm.ActiveConnection = m_oConexion.ADOCon
        
        adoComm.CommandText = sConsulta
        adoComm.CommandType = adCmdText
        
        Set adoParam = adoComm.CreateParameter("FECEST", adDate, adParamInput, , m_vFechaRealAper)
        adoComm.Parameters.Append adoParam
        
        m_sEstadoCrypt = EncriptarAperturaSobre(CStr(m_oProceso.Anyo) & CStr(m_oProceso.Cod) & CStr(m_iSobre), "Abierto", True)
        Set adoParam = adoComm.CreateParameter("EST", adVarChar, adParamInput, 50, m_sEstadoCrypt)
        adoComm.Parameters.Append adoParam
        
        Set adoParam = adoComm.CreateParameter("ANYO", adSmallInt, adParamInput, , m_oProceso.Anyo)
        adoComm.Parameters.Append adoParam
    
        Set adoParam = adoComm.CreateParameter("GMN1", adVarChar, adParamInput, Len(m_oProceso.GMN1Cod), m_oProceso.GMN1Cod)
        adoComm.Parameters.Append adoParam
    
        Set adoParam = adoComm.CreateParameter("PROCE", adInteger, adParamInput, , m_oProceso.Cod)
        adoComm.Parameters.Append adoParam
            
        Set adoParam = adoComm.CreateParameter("SOBRE", adSmallInt, adParamInput, , m_iSobre)
        adoComm.Parameters.Append adoParam
        
        adoComm.Execute
        If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
        
    
        If Not AdoRes Is Nothing Then
            AdoRes.Requery
            m_vFecAct = AdoRes("FECACT").Value
            AdoRes.Close
            Set AdoRes = Nothing
        End If
                
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    AbrirSobre = TESError
    

Finalizar:
    On Error Resume Next
    If Not AdoRes Is Nothing Then
        AdoRes.Close
        Set AdoRes = Nothing
    End If
    Exit Function
Error_Cls:

    AbrirSobre = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    
    m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
    
    Resume Finalizar
    Resume 0
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      'Call g_oErrores.TratarError("Class Module", "CSobre", "AbrirSobre", Err, Erl)
      GoTo Error_Cls
      Exit Function
   End If

End Function
