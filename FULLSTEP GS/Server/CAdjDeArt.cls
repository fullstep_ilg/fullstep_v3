VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CAdjDeArt"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CAdjDeArticulo **********************************
'*             Autor : Javier Arana
'*             Creada : 23/2/99
'***************************************************************

Option Explicit
Implements IBaseDatos

Private mvarArticulo As CArticulo
Private mvarId As Integer
Private mvarProceAnyo As Variant
Private mvarProceGmn As Variant
Private mvarProceCod As Variant
Private mvarProveCod As Variant
Private mvarDescr As Variant
Private mvarDestCod As String
Private mvarUniCod As String
Private mvarPrecio As Double
Private mvarPorcentaje As Double
Private mvarFecInicio As Variant
Private mvarFecFin As Variant
Private mvarIndice As Variant
Private mvarRecordset As adodb.Recordset
Private mvarConexion As CConexion
Private mvarConexionServer As CConexionDeUseServer
Private mvarCantidad As Variant
Private mvarMoneda As Variant
Private m_sUsuario As String
Private m_dFecha_PreAdj As Date
Private m_iUltimo As Integer
Public Property Set Articulo(ByVal oArt As CArticulo)
    Set mvarArticulo = oArt
End Property
Public Property Get Articulo() As Variant
    Set Articulo = mvarArticulo
End Property
Public Property Let Cantidad(ByVal varCantidad As Variant)
    mvarCantidad = varCantidad
End Property
Public Property Get Cantidad() As Variant
    Cantidad = mvarCantidad
End Property

Public Property Let Indice(ByVal varIndice As Variant)
    mvarIndice = varIndice
End Property
Public Property Get Indice() As Variant
    Indice = mvarIndice
End Property
Public Property Let ProveCod(ByVal var As Variant)
    mvarProveCod = var
End Property
Public Property Get ProveCod() As Variant
    ProveCod = mvarProveCod
End Property
Public Property Let Descr(ByVal var As Variant)
    mvarDescr = var
End Property
Public Property Get Descr() As Variant
    Descr = mvarDescr
End Property

Public Property Let FechaInicioSuministro(ByVal Dat As Variant)
    mvarFecInicio = Dat
End Property
Public Property Get FechaInicioSuministro() As Variant
    FechaInicioSuministro = mvarFecInicio
End Property
Public Property Let FechaFinSuministro(ByVal Dat As Variant)
    mvarFecFin = Dat
End Property
Public Property Get FechaFinSuministro() As Variant
    FechaFinSuministro = mvarFecFin
End Property
Public Property Let DestCod(ByVal var As String)
    mvarDestCod = var
End Property
Public Property Get DestCod() As String
    DestCod = mvarDestCod
End Property
Public Property Let UniCod(ByVal S As String)
    mvarUniCod = S
End Property
Public Property Get UniCod() As String
    UniCod = mvarUniCod
End Property
Public Property Let Id(ByVal i As Integer)
    mvarId = i
End Property
Public Property Get Id() As Integer
    Id = mvarId
End Property

Public Property Let Precio(ByVal v As Double)
    mvarPrecio = v
End Property
Public Property Get Precio() As Double
    Precio = mvarPrecio
End Property
Public Property Let Porcentaje(ByVal v As Double)
    mvarPorcentaje = v
End Property
Public Property Get Porcentaje() As Double
    Porcentaje = mvarPorcentaje
End Property

Friend Property Set Conexion(ByVal con As CConexion)
Set mvarConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = mvarConexion
End Property
Friend Property Set ConexionServer(ByVal con As CConexionDeUseServer)
Set mvarConexionServer = con
End Property
Friend Property Get ConexionServer() As CConexionDeUseServer
Set ConexionServer = mvarConexionServer
End Property
Private Sub ConectarDeUseServer()
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If mvarConexionServer.ADOSummitCon Is Nothing Then
        Set mvarConexionServer.ADOSummitCon = New adodb.Connection
        mvarConexionServer.ADOSummitCon.Open "Provider=SQLOLEDB.1;Auto Translate=FALSE;Server=" & basParametros.ServidorGS & ";Database=" & basParametros.BaseDatosGS & ";", LICDBLogin, LICDBContra
        mvarConexionServer.ADOSummitCon.CursorLocation = adUseServer
        mvarConexionServer.ADOSummitCon.CommandTimeout = 120
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CAdjDeArt", "ConectarDeUseServer", ERR, Erl)
      Exit Sub
   End If
End Sub
Private Sub Class_Terminate()
    Set mvarConexion = Nothing
    Set mvarArticulo = Nothing
    Set mvarConexionServer = Nothing
End Sub

''' <summary>A�ade un nuevo registro</summary>
''' <returns>Objeto tipo TipoErrorSummit con el error si se ha producido</returns>
''' <remarks>Llamada desde: FSGClient Tiempo m�ximo: 1 sec </remarks>
''' <revision>LTG 22/12/2011</revision>

Private Function IBaseDatos_AnyadirABaseDatos() As TipoErrorSummit
    Dim sConsulta As String
    Dim sConsulta1 As String
    Dim sConsulta2 As String
    Dim TESError As TipoErrorSummit
    Dim rs As New adodb.Recordset
    Dim fldNum As adodb.Field
    Dim iNum As Integer
    Dim btrans As Boolean
    
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls
    
    TESError.NumError = TESnoerror
    
    'Si existen adjudicaciones actuales de summit, no podemos anyadir
    'adjudicaciones anteriores a summit
    
    mvarConexion.ADOCon.Execute "BEGIN TRANSACTION"
    btrans = True
    
    sConsulta1 = "SELECT COUNT (*)  AS NUM FROM ART4_ADJ WITH (NOLOCK) WHERE PROCE IS NOT NULL AND ART='" & DblQuote(mvarArticulo.Cod) & "'"
    'CALIDAD: No se usa WITH (NOLOCK) porque es para obtener el nuevo valor de la clave
    sConsulta2 = "SELECT MAX(ID) AS NUM FROM ART4_ADJ WHERE ART='" & DblQuote(mvarArticulo.Cod) & "'"
    
    rs.Open sConsulta1, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
        
    Set fldNum = rs.Fields(0)
        
    If fldNum.Value = 0 Then
        'Recuperamos el valor de ID para la adj  a anyadir
        rs.Close
        Set rs = Nothing
        Set rs = New adodb.Recordset
        rs.Open sConsulta2, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
        Set fldNum = rs.Fields(0)
        If IsNull(fldNum.Value) Then
            iNum = 1
        Else
            iNum = fldNum.Value + 1
        End If
        
        sConsulta = "INSERT INTO ART4_ADJ (ART,ID,CODPROVE,DENPROVE,PREC,PORCEN,FECINI,FECFIN,UNI,DEST,CANT)"
        sConsulta = sConsulta & " VALUES (" & StrToSQLNULL(mvarArticulo.Cod)
               
        sConsulta = sConsulta & "," & iNum
        
        sConsulta = sConsulta & "," & StrToSQLNULL(mvarProveCod)
        
        'If IsNull(mvarProveCod) Or mvarProveCod = "" Then
            sConsulta = sConsulta & "," & StrToSQLNULL(mvarDescr)
        'Else
        '    sConsulta = sConsulta & ",NULL"
        'End If
        
        sConsulta = sConsulta & "," & DblToSQLFloat(mvarPrecio)
        sConsulta = sConsulta & "," & DblToSQLFloat(mvarPorcentaje)
        sConsulta = sConsulta & "," & DateToSQLDate(mvarFecInicio)
        sConsulta = sConsulta & "," & DateToSQLDate(mvarFecFin)
        sConsulta = sConsulta & ",'" & DblQuote(mvarUniCod) & "'"
        sConsulta = sConsulta & ",'" & DblQuote(mvarDestCod) & "'"
        sConsulta = sConsulta & "," & DblToSQLFloat(mvarCantidad) & ")"
                
        mvarConexion.ADOCon.Execute sConsulta
        If mvarConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
        
        mvarConexion.ADOCon.Execute "COMMIT TRANSACTION"
        IBaseDatos_AnyadirABaseDatos = TESError
        btrans = False
        mvarId = iNum
        
    Else
        ' Ha habido adjudicaciones nuevas
        rs.Close
        Set rs = Nothing
        TESError.NumError = TESAdjAntDenegada
        IBaseDatos_AnyadirABaseDatos = TESError
        mvarConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
        btrans = False
    End If
    
    Set fldNum = Nothing
    Exit Function
        
Error_Cls:
    IBaseDatos_AnyadirABaseDatos = basErrores.TratarError(mvarConexion.ADOCon.Errors)
    
    If btrans Then
        mvarConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      'Call g_oErrores.TratarError("M�dulo de clase", "CAdjDeArt", "IBaseDatos_AnyadirABaseDatos", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If
End Function

Private Function IBaseDatos_CambiarCodigo(CodigoNuevo As Variant) As TipoErrorSummit

End Function

Private Sub IBaseDatos_CancelarEdicion()

    ''' * Objetivo: Cancelar la edicion, cerrando el Resultset
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not mvarRecordset Is Nothing Then
        mvarRecordset.Close
        Set mvarRecordset = Nothing
    End If

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CAdjDeArt", "IBaseDatos_CancelarEdicion", ERR, Erl)
      Exit Sub
   End If

End Sub

Private Function IBaseDatos_ComprobarExistenciaEnBaseDatos() As Boolean
' NO USADO POR AHORA
End Function

Private Function IBaseDatos_EliminarDeBaseDatos() As TipoErrorSummit

Dim sConsulta As String
Dim TESError As TipoErrorSummit

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
TESError.NumError = TESnoerror


Select Case basParametros.gParametrosGenerales.giNEM
    
    Case 1
        sConsulta = "DELETE  FROM ART1_ADJ WHERE GMN1='" & DblQuote(mvarArticulo.GMN1Cod) & "' AND ART='" & DblQuote(mvarArticulo.Cod) & "' AND ID=" & mvarId
    Case 2
        sConsulta = "DELETE FROM ART2_ADJ WHERE GMN1='" & DblQuote(mvarArticulo.GMN1Cod) & "' AND GMN2='" & DblQuote(mvarArticulo.GMN2Cod) & "' AND ART='" & DblQuote(mvarArticulo.Cod) & "' AND ID=" & mvarId
    Case 3
        sConsulta = "DELETE FROM ART3_ADJ WHERE GMN1='" & DblQuote(mvarArticulo.GMN1Cod) & "' AND GMN2='" & DblQuote(mvarArticulo.GMN2Cod) & "' AND GMN3='" & DblQuote(mvarArticulo.GMN3Cod) & "' AND ART='" & DblQuote(mvarArticulo.Cod) & "' AND ID=" & mvarId
    Case 4
        sConsulta = "DELETE FROM ART4_ADJ WHERE ART='" & DblQuote(mvarArticulo.Cod) & "' AND ID=" & mvarId
    End Select

    mvarConexion.ADOCon.Execute sConsulta
    IBaseDatos_EliminarDeBaseDatos = TESError

    Exit Function

    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CAdjDeArt", "IBaseDatos_EliminarDeBaseDatos", ERR, Erl)
      Exit Function
   End If
    
End Function

Private Function IBaseDatos_FinalizarEdicionEliminando() As TipoErrorSummit
Dim TESError As TipoErrorSummit

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
TESError.NumError = TESnoerror


If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls
    
    '******************************************************************
    TESError.NumError = TESnoerror
    
    'pendiente especificaciones
    mvarRecordset.Delete
    mvarRecordset.Close
    Set mvarRecordset = Nothing
    
Error_Cls:
    
    IBaseDatos_FinalizarEdicionEliminando = basErrores.TratarError(mvarConexionServer.ADOSummitCon.Errors)
    If mvarRecordset.EditMode <> adEditNone Then
        mvarRecordset.CancelUpdate
    End If
    mvarRecordset.Close
    Set mvarRecordset = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      'Call g_oErrores.TratarError("M�dulo de clase", "CAdjDeArt", "IBaseDatos_FinalizarEdicionEliminando", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If
End Function

Private Function IBaseDatos_FinalizarEdicionModificando() As TipoErrorSummit
Dim TESError As TipoErrorSummit


If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls
    
    mvarRecordset("CODPROVE") = mvarProveCod
    'If IsNull(mvarProveCod) Then
    mvarRecordset("DENPROVE") = mvarDescr
    'Else
    '    mvarResultset("DENPROVE") = Null
    'End If
    mvarRecordset("DEST") = mvarDestCod
    mvarRecordset("UNI") = mvarUniCod
    mvarRecordset("PREC") = mvarPrecio
    mvarRecordset("PORCEN") = mvarPorcentaje
    mvarRecordset("FECINI") = mvarFecInicio
    mvarRecordset("FECFIN") = mvarFecFin
    mvarRecordset("CANT") = mvarCantidad
    
    mvarRecordset.Update
    mvarRecordset.Close
    Set mvarRecordset = Nothing
    TESError.NumError = TESnoerror
    IBaseDatos_FinalizarEdicionModificando = TESError
    
    Exit Function
    

Error_Cls:

    IBaseDatos_FinalizarEdicionModificando = basErrores.TratarError(mvarConexionServer.ADOSummitCon.Errors)
    Resume Salir:
    
Salir:

    On Error Resume Next
    If mvarRecordset.EditMode <> adEditNone Then
        mvarRecordset.CancelUpdate
        
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      'Call g_oErrores.TratarError("M�dulo de clase", "CAdjDeArt", "IBaseDatos_FinalizarEdicionModificando", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If
  
End Function

''' <summary>A�ade un nuevo registro</summary>
''' <param name="Bloquear">No se utiliza</param>
''' <param name="UsuarioBloqueo">No se utiliza</param>
''' <returns>Objeto tipo TipoErrorSummit con el error si se ha producido</returns>
''' <remarks>Llamada desde: FSGClient Tiempo m�ximo: 1 sec </remarks>
''' <revision>LTG 22/12/2011</revision>

Private Function IBaseDatos_IniciarEdicion(Optional ByVal Bloquear As Boolean, Optional ByVal UsuarioBloqueo As String) As TipoErrorSummit
    Dim sConsulta As String
    Dim TESError As TipoErrorSummit
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    TESError.NumError = TESnoerror
    
       
    'CALIDAD: Sin WITH (NOLOCK) porque se rellena el recordset mvarRecordset que posteriormente se utiliza para las actualizaciones
    'en otros m�todos de la clase. Si se hace la lectura con WITH (NOLOCK) no se permite la llamada al m�todo Update del recordset.
    sConsulta = "SELECT ART,ID,DEST,UNI,CODPROVE,DENPROVE,PREC,PORCEN,FECINI,FECFIN,PROCE,GMN1,ANYO,FECACT,CANT"
    sConsulta = sConsulta & " FROM ART4_ADJ WHERE ART='" & DblQuote(mvarArticulo.Cod) & "' AND ID=" & mvarId
    
    ConectarDeUseServer
    Set mvarRecordset = New adodb.Recordset
    'mvarRecordset.CursorLocation = adUseServer
    mvarRecordset.Open sConsulta, mvarConexionServer.ADOSummitCon, adOpenKeyset, adLockOptimistic
    
    If mvarRecordset.eof Then
        mvarRecordset.Close
        Set mvarRecordset = Nothing
        TESError.NumError = TESDatoEliminado
        TESError.Arg1 = 47
        IBaseDatos_IniciarEdicion = TESError
        Exit Function
    End If
    
    If mvarDescr <> mvarRecordset("DENPROVE") Then
        TESError.NumError = TESInfActualModificada
        IBaseDatos_IniciarEdicion = TESError
        mvarDescr = mvarRecordset("DENPROVE")
        Exit Function
    End If
    
    If mvarDestCod <> mvarRecordset("DEST") Then
        TESError.NumError = TESInfActualModificada
        IBaseDatos_IniciarEdicion = TESError
        mvarDestCod = mvarRecordset("DEST")
        Exit Function
    End If
    
    If mvarUniCod <> mvarRecordset("UNI") Then
        TESError.NumError = TESInfActualModificada
        IBaseDatos_IniciarEdicion = TESError
        mvarUniCod = mvarRecordset("UNI")
        Exit Function
    End If
    
    If mvarPrecio <> mvarRecordset("PREC") Then
        TESError.NumError = TESInfActualModificada
        IBaseDatos_IniciarEdicion = TESError
        mvarPrecio = mvarRecordset("PREC")
        Exit Function
    End If
        
    If mvarPorcentaje <> mvarRecordset("PORCEN") Then
        TESError.NumError = TESInfActualModificada
        IBaseDatos_IniciarEdicion = TESError
        mvarPorcentaje = mvarRecordset("PORCEN")
        Exit Function
    End If
    
    If Not IsNull(mvarFecInicio) Then
    
        If CDate(mvarFecInicio) <> mvarRecordset("FECINI") Then
            TESError.NumError = TESInfActualModificada
            IBaseDatos_IniciarEdicion = TESError
            mvarFecInicio = mvarRecordset("FECINI")
            Exit Function
        End If
        
    Else
        If Not IsNull(mvarRecordset("FECINI")) Then
            TESError.NumError = TESInfActualModificada
            IBaseDatos_IniciarEdicion = TESError
            mvarFecInicio = mvarRecordset("FECINI")
            Exit Function
        End If
    End If
    
    If Not IsNull(mvarFecFin) Then
    
        If CDate(mvarFecFin) <> mvarRecordset("FECFIN") Then
            TESError.NumError = TESInfActualModificada
            IBaseDatos_IniciarEdicion = TESError
            mvarFecFin = mvarRecordset("FECFIN")
            Exit Function
        End If
    
    Else
        If Not IsNull(mvarRecordset("FECFIN")) Then
            TESError.NumError = TESInfActualModificada
            IBaseDatos_IniciarEdicion = TESError
            mvarFecFin = mvarRecordset("FECFIN")
            Exit Function
        End If
    End If
    
    If Not IsNull(mvarCantidad) Then
        If CDbl(mvarCantidad) <> mvarRecordset("CANT") Then
            TESError.NumError = TESInfActualModificada
            IBaseDatos_IniciarEdicion = TESError
            mvarCantidad = mvarRecordset("CANT")
            Exit Function
        End If
    Else
        If Not IsNull(mvarRecordset("CANT")) Then
            TESError.NumError = TESInfActualModificada
            IBaseDatos_IniciarEdicion = TESError
            mvarCantidad = mvarRecordset("CANT")
            Exit Function
        End If
    End If
    
    IBaseDatos_IniciarEdicion = TESError
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CAdjDeArt", "IBaseDatos_IniciarEdicion", ERR, Erl)
      Exit Function
   End If
End Function


Public Property Get proceGmn() As Variant
    proceGmn = mvarProceGmn
End Property

Public Property Let proceGmn(ByVal vProceGmn As Variant)
    mvarProceGmn = vProceGmn
End Property


Public Property Get ProceCod() As Variant
    ProceCod = mvarProceCod
End Property

Public Property Let ProceCod(ByVal vProceCod As Variant)
    mvarProceCod = vProceCod
End Property

Public Property Get proceAnyo() As Variant
    proceAnyo = mvarProceAnyo
End Property

Public Property Let proceAnyo(ByVal vProceAnyo As Variant)
    mvarProceAnyo = vProceAnyo
End Property

Public Property Get Moneda() As Variant
    Moneda = mvarMoneda
End Property

Public Property Let Moneda(ByVal vMoneda As Variant)
    mvarMoneda = vMoneda
End Property

Public Property Get Usuario() As String
    Usuario = m_sUsuario
End Property

Public Property Let Usuario(ByVal sUsuario As String)
    m_sUsuario = sUsuario
End Property
Public Property Get Ultimo_PreAdj() As Integer
    Ultimo_PreAdj = m_iUltimo
End Property

Public Property Let Ultimo_PreAdj(ByVal iUltimo_PreAdj As Integer)
    m_iUltimo = iUltimo_PreAdj
End Property
Public Property Get Fecha_PreAdj() As Date
    Fecha_PreAdj = m_dFecha_PreAdj
End Property

Public Property Let Fecha_PreAdj(ByVal dFecha_PreAdj As Date)
    m_dFecha_PreAdj = dFecha_PreAdj
End Property

