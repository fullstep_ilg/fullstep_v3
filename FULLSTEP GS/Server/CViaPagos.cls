VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CViaPagos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True

Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum
Private mCol As Collection
Private m_oConexion As CConexion
Public m_bEOF As Boolean

' Variables para el control de cambios del Log e Integraci�n
Private m_udtOrigen As OrigenIntegracion


Public Function DevolverLosCodigos() As TipoDatosCombo
   
   Dim Codigos As TipoDatosCombo
   Dim iCont As Integer
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
   ReDim Codigos.Cod(mCol.Count)
   ReDim Codigos.Den(mCol.Count)
   
    For iCont = 0 To mCol.Count - 1
        
        Codigos.Cod(iCont) = mCol(iCont + 1).Cod
        Codigos.Den(iCont) = mCol(iCont + 1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
    
    Next

   DevolverLosCodigos = Codigos
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CViaPagos", "DevolverLosCodigos", ERR, Erl)
      Exit Function
   End If
   
End Function
 
''' <summary>
''' Carga las unidades de la BD en la coleccion
''' </summary>
''' <param name="NumMaximo">Numero m�ximo de paises</param>
''' <param name="CaracteresInicialesCod">Cod de busqueda</param>
''' <param name="CaracteresInicialesDen">Den de busqueda</param>
''' <param name="OrdenadosPorDen">Ordenacion por denominacion</param>
''' <param name="UsarIndice">Si se usa indice para la coleccion</param>
''' <returns></returns>
''' <remarks>Llamada desde:Todos los combos de pagos y pantallas que tienen combos de pagos de la aplicacion </remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>
Public Sub CargarTodosLosPagosDesde(ByVal NumMaximo As Integer, Optional ByVal CaracteresInicialesCod As String, Optional ByVal CaracteresInicialesDen As String, Optional ByVal OrdenadosPorDen As Boolean, Optional ByVal UsarIndice As Boolean)

    ''' * Objetivo: Cargar las v�as de Pagos hasta un numero maximo
    ''' * Objetivo: desde un codigo o una denominacion determinadas
    
    'ado  Dim rdores As rdoResultset
    Dim rs As adodb.Recordset
    Dim fldCod As adodb.Field
    
    Dim oDen As CMultiidiomas
    Dim oGestorParametros As CGestorParametros
    Dim oIdiomas As CIdiomas
    Dim oIdi As CIdioma
    Dim sCod As String
    Dim sCod2 As String
    
    Dim sConsulta As String
    
    Dim lIndice As Long
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oGestorParametros = New CGestorParametros
    Set oGestorParametros.Conexion = m_oConexion
    Set oIdiomas = oGestorParametros.DevolverIdiomas(False, False, True)
    
    sCod = ""
    sCod2 = ""
    For Each oIdi In oIdiomas
        sCod = sCod & ", " & oIdi.Cod & ".DEN AS DEN_" & oIdi.Cod
        sCod2 = sCod2 & " LEFT JOIN VIA_PAG_DEN " & oIdi.Cod & " WITH(NOLOCK) ON VIA_PAG.COD = " & oIdi.Cod & ".VIA_PAG AND " & oIdi.Cod & ".IDIOMA = '" & oIdi.Cod & "' "
    Next
    
    ''' Generacion del SQL a partir de los parametros
    
    If CaracteresInicialesCod = "" And CaracteresInicialesDen = "" Then
        
        sConsulta = "SELECT TOP " & NumMaximo & " VIA_PAG.COD,VIA_PAG.FECACT " & sCod & " FROM VIA_PAG WITH (NOLOCK) "
        sConsulta = sConsulta & sCod2
        sConsulta = sConsulta & " WHERE 1 = 1"
    Else
    
        If Not (CaracteresInicialesCod = "") And Not (CaracteresInicialesDen = "") Then
            
                sConsulta = "SELECT TOP " & NumMaximo & " VIA_PAG.COD,VIA_PAG.FECACT " & sCod & " FROM VIA_PAG WITH (NOLOCK) "
                sConsulta = sConsulta & sCod2
                sConsulta = sConsulta & " WHERE COD >='" & DblQuote(CaracteresInicialesCod) & "'"
                sConsulta = sConsulta & " AND DEN_" & gParametrosInstalacion.gIdioma & " >='" & DblQuote(CaracteresInicialesDen) & "'"
                        
        Else
            
            If Not (CaracteresInicialesCod = "") Then
                    sConsulta = "SELECT TOP " & NumMaximo & " VIA_PAG.COD,VIA_PAG.FECACT " & sCod & " FROM VIA_PAG WITH (NOLOCK) "
                    sConsulta = sConsulta & sCod2
                    sConsulta = sConsulta & " WHERE COD >='" & DblQuote(CaracteresInicialesCod) & "'"
            Else
                    sConsulta = "SELECT TOP " & NumMaximo & " VIA_PAG.COD,VIA_PAG.FECACT " & sCod & " FROM VIA_PAG WITH (NOLOCK) "
                    sConsulta = sConsulta & sCod2
                    sConsulta = sConsulta & " WHERE DEN_" & gParametrosInstalacion.gIdioma & " >='" & DblQuote(CaracteresInicialesDen) & "'"
            End If
        
        End If
               
    End If
    
    If OrdenadosPorDen Then
        sConsulta = sConsulta & " ORDER BY DEN_" & gParametrosInstalacion.gIdioma
    Else
        sConsulta = sConsulta & " ORDER BY COD"
    End If
          
    ''' Crear el Recordset
    
    'ado  Set rdores = m_oConexion.rdoSummitCon.OpenResultset(sConsulta, rdOpenForwardOnly, rdConcurReadOnly, rdExecDirect)
    Set rs = New adodb.Recordset
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
          
    If rs.eof Then
            
        rs.Close
        Set rs = Nothing
        Set mCol = Nothing
        Set mCol = New Collection
        Exit Sub
          
    Else
        
        ''' Llenar la coleccion a partir del Resultset
        ''' teniendo en cuenta el maximo
        
        Set mCol = Nothing
        Set mCol = New Collection
        Set fldCod = rs.Fields("COD")
        'Carga todos los idiomas de la aplicaci�n:
        Set oGestorParametros = New CGestorParametros
        Set oGestorParametros.Conexion = m_oConexion
        Set oIdiomas = oGestorParametros.DevolverIdiomas(False, False, True)

            
        If UsarIndice Then
            
            lIndice = 0
       
            While Not rs.eof
                
                Set oDen = Nothing
                Set oDen = New CMultiidiomas
                Set oDen.Conexion = m_oConexion
                For Each oIdi In oIdiomas
                    oDen.Add oIdi.Cod, rs.Fields("DEN_" & oIdi.Cod).Value
                Next
                Me.Add fldCod.Value, oDen, lIndice
                
                rs.MoveNext
                lIndice = lIndice + 1
                
            Wend
            
            If Not rs.eof Then
                m_bEOF = False
            Else
                m_bEOF = True
            End If
                        
        Else
 
            While Not rs.eof
            
                Set oDen = Nothing
                Set oDen = New CMultiidiomas
                Set oDen.Conexion = m_oConexion
                For Each oIdi In oIdiomas
                    oDen.Add oIdi.Cod, rs.Fields("DEN_" & oIdi.Cod).Value
                Next
                Me.Add fldCod.Value, oDen
                
                rs.MoveNext
                
            Wend
            
            If Not rs.eof Then
                m_bEOF = False
            Else
                m_bEOF = True
            End If
            
        End If
        
        rs.Close
        Set rs = Nothing
        Set fldCod = Nothing
        Set oDen = Nothing
          
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CViaPagos", "CargarTodosLosPagosDesde", ERR, Erl)
      Exit Sub
   End If
    
End Sub

''' <summary>
''' Carga las vias de pago de la BD en la coleccion
''' </summary>
''' <param name="CaracteresInicialesCod">Cod de busqueda</param>
''' <param name="CaracteresInicialesDen">Den de busqueda</param>
''' <param name="CoincidenciaTotal">Si la busqueda ha de coincidir con la den o el cod</param>
''' <param name="OrdenadosPorDen">Ordenacion por denominacion</param>
''' <param name="UsarIndice">Si se usa indice para la coleccion</param>
''' <param name="MostrarEstadoIntegracion">Si se muestra el estado de la integracion</param>
''' <param name="OrdenadosPorEstadoInt">Si se ordena por el estado de la integracion</param>
''' <param name="DenAOrdenar">El idioma de la ordenacion por denominacion</param>
''' <returns></returns>
''' <remarks>Llamada desde:Todos los combos de vias de pago y pantallas que tienen combos de vias de pago de la aplicacion </remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>

Public Sub CargarTodosLosPagos(Optional ByVal CaracteresInicialesCod As String, Optional ByVal CaracteresInicialesDen As String, Optional ByVal CoincidenciaTotal As Boolean, Optional ByVal OrdenadosPorDen As Boolean, Optional ByVal UsarIndice As Boolean, Optional ByVal MostrarEstadoIntegracion As Boolean = False, Optional ByVal OrdenadosPorEstadoInt As Boolean = False, Optional DenAOrdenar As String = "")
'ado  Dim rdores As rdoResultset
Dim rs As adodb.Recordset
Dim fldCod As adodb.Field
Dim fldDen As adodb.Field
Dim fldFecAct As adodb.Field
Dim fldEstadoInt As adodb.Field   'Estado integraci�n
Dim sConsulta As String
Dim lIndice As Long
Dim v_Estado As Variant
Dim oDen As CMultiidiomas
Dim oGestorParametros As CGestorParametros
Dim oIdiomas As CIdiomas
Dim oIdi As CIdioma
Dim sCod As String
Dim sCod2 As String ''MultiERP
Dim sCod3 As String ''MultiERP
''' * Objetivo: Cuando se trabaja con integraci�n, desde el mantenimiento se visualiza el estado de integraci�n
''' *            en el sistema receptor de las modificaciones originadas en FSGS.
''' *           Buscamos por coincidencia de c�digo (COD) cuando la acci�n es distinta a un cambio de c�digo (C)
''' *           y por coincidencia en c�digo nuevo (COD_NEW) cuando la acci�n es un cambio de c�digo


'Carga todos los idiomas de la aplicaci�n:
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
Set oGestorParametros = New CGestorParametros
Set oGestorParametros.Conexion = m_oConexion
Set oIdiomas = oGestorParametros.DevolverIdiomas(False, False, True)


If MostrarEstadoIntegracion Then
        
    Set oDen = Nothing
    Set oDen = New CMultiidiomas
    Set oDen.Conexion = m_oConexion
      
    sCod = ""
    sCod2 = ""
    sCod3 = ""
    
    For Each oIdi In oIdiomas
        sCod = sCod & ", " & oIdi.Cod & ".DEN AS DEN_" & oIdi.Cod
        sCod2 = sCod2 & " LEFT JOIN VIA_PAG_DEN " & oIdi.Cod & " WITH(NOLOCK) ON VIA_PAG.COD = " & oIdi.Cod & ".VIA_PAG AND " & oIdi.Cod & ".IDIOMA = '" & oIdi.Cod & "' "
        sCod3 = sCod3 & "," & "TABLA.DEN_" & oIdi.Cod ''MultiERP
    Next
        
    ''MultiERP
    sConsulta = "SELECT TABLA.COD " & sCod3 & ", MAX(TABLA.FECACT) AS FECACT, MIN(TABLA.ESTADO) AS ESTADO FROM ("
    sConsulta = sConsulta & "SELECT VIA_PAG.COD " & sCod & ",VIA_PAG.FECACT,CASE WHEN RIGHT(MAX(convert(varchar,log_gral.fecact,121)+LOG_GRAL.ESTADO),1) is null then 4 WHEN RIGHT(MAX(convert(varchar,log_gral.fecact,121)+LOG_GRAL.ESTADO),1) = 4 then 4 else 3 END AS ESTADO FROM VIA_PAG WITH (NOLOCK) "
    sConsulta = sConsulta & sCod2
    sConsulta = sConsulta & " LEFT JOIN (LOG_VIA_PAG WITH (NOLOCK) INNER JOIN LOG_GRAL WITH (NOLOCK) ON LOG_VIA_PAG.ID = LOG_GRAL.ID_TABLA AND LOG_GRAL.TABLA = " & EntidadIntegracion.ViaPag & " AND (LOG_GRAL.ORIGEN = " & OrigenIntegracion.FSGSInt & " OR LOG_GRAL.ORIGEN = " & OrigenIntegracion.FSGSIntReg & "))"
    sConsulta = sConsulta & " ON (LOG_VIA_PAG.COD = VIA_PAG.COD AND LOG_VIA_PAG.ACCION <> '" & Accion_CambioCodigo & "') OR (LOG_VIA_PAG.COD_NEW = VIA_PAG.COD AND LOG_VIA_PAG.ACCION = '" & Accion_CambioCodigo & "') "

Else

    sCod = ""
    sCod2 = ""
    For Each oIdi In oIdiomas
        sCod = sCod & ", " & oIdi.Cod & ".DEN AS DEN_" & oIdi.Cod
        sCod2 = sCod2 & " LEFT JOIN VIA_PAG_DEN " & oIdi.Cod & " WITH(NOLOCK) ON VIA_PAG.COD = " & oIdi.Cod & ".VIA_PAG AND " & oIdi.Cod & ".IDIOMA = '" & oIdi.Cod & "' "
    Next
            
    sConsulta = "SELECT * FROM (" ''MultiERP
    sConsulta = sConsulta & "SELECT COD,VIA_PAG.FECACT" & sCod & " FROM VIA_PAG WITH (NOLOCK)"
    sConsulta = sConsulta & sCod2
End If

If CaracteresInicialesCod = "" And CaracteresInicialesDen = "" Then
  
Else
    If Not (CaracteresInicialesCod = "") And Not (CaracteresInicialesDen = "") Then
        
        If CoincidenciaTotal Then
            
            sConsulta = sConsulta & " WHERE"
            sConsulta = sConsulta & " VIA_PAG.COD ='" & DblQuote(CaracteresInicialesCod) & "'"
            sConsulta = sConsulta & " AND DEN_" & gParametrosInstalacion.gIdioma & "='" & DblQuote(CaracteresInicialesDen) & "'"
            
        Else
            
            sConsulta = sConsulta & " WHERE"
            sConsulta = sConsulta & " VIA_PAG.COD LIKE '" & DblQuote(CaracteresInicialesCod) & "%'"
            sConsulta = sConsulta & " DEN_" & gParametrosInstalacion.gIdioma & " LIKE '" & DblQuote(CaracteresInicialesDen) & "%'"
                       
        End If
                    
    Else
        
        If Not (CaracteresInicialesCod = "") Then
            
            If CoincidenciaTotal Then
            
                sConsulta = sConsulta & " WHERE"
                sConsulta = sConsulta & " VIA_PAG.COD ='" & DblQuote(CaracteresInicialesCod) & "'"
                
            Else
                    
                sConsulta = sConsulta & " WHERE"
                sConsulta = sConsulta & " VIA_PAG.COD LIKE '" & DblQuote(CaracteresInicialesCod) & "%'"
                
            End If
            
        Else
            
            If CoincidenciaTotal Then
            
                sConsulta = sConsulta & " WHERE"
                sConsulta = sConsulta & " DEN_" & gParametrosInstalacion.gIdioma & "='" & DblQuote(CaracteresInicialesDen) & "'"
                
            Else
           
                sConsulta = sConsulta & " WHERE"
                sConsulta = sConsulta & " DEN_" & gParametrosInstalacion.gIdioma & " LIKE '" & DblQuote(CaracteresInicialesDen) & "%'"
                  
            End If
            
        End If
    
    End If
           
End If
If MostrarEstadoIntegracion Then
   
    sCod = ""
    sCod2 = ""
    For Each oIdi In oIdiomas
        sCod = sCod & "," & oIdi.Cod & ".DEN"
        sCod2 = sCod2 & ", TABLA.DEN_" & oIdi.Cod   ''MultiERP
    Next
    
    sConsulta = sConsulta & " GROUP BY VIA_PAG.COD" & sCod & ",VIA_PAG.FECACT, LOG_VIA_PAG.ERP"
    sConsulta = sConsulta & " ) AS TABLA GROUP BY TABLA.COD" & sCod2 & ",TABLA.FECACT" ''MultiERP
Else ''MultiERP
    sConsulta = sConsulta & " ) AS TABLA "
End If
   
If OrdenadosPorDen Then
    If DenAOrdenar = "" Then
        sConsulta = sConsulta & " ORDER BY TABLA.DEN_" & gParametrosInstalacion.gIdioma & ",TABLA.COD" ''MultiERP
    Else
        sConsulta = sConsulta & " ORDER BY TABLA." & DenAOrdenar & ",TABLA.COD" ''MultiERP
    End If
Else
    If OrdenadosPorEstadoInt Then
        sConsulta = sConsulta & " ORDER BY ESTADO,TABLA.COD,TABLA.DEN_" & gParametrosInstalacion.gIdioma ''MultiERP
    Else
        sConsulta = sConsulta & " ORDER BY TABLA.COD,TABLA.DEN_" & gParametrosInstalacion.gIdioma ''MultiERP
    End If
End If
      
'ado  Set rdores = m_oConexion.rdoSummitCon.OpenResultset(sConsulta, rdOpenForwardOnly, rdConcurReadOnly, rdExecDirect)
Set rs = New adodb.Recordset
rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
      
If rs.eof Then
        
    rs.Close
    Set rs = Nothing
    Exit Sub
      
Else
    
    Set mCol = Nothing
    Set mCol = New Collection
    Set fldCod = rs.Fields("COD")
    Set fldFecAct = rs.Fields("FECACT")
    If MostrarEstadoIntegracion Then
        Set fldEstadoInt = rs.Fields("ESTADO")
    End If
        
    If UsarIndice Then
        
        lIndice = 0
        
        While Not rs.eof
            ' Dejamos en manos del programador cuando asignar la conexion al obejto usuario
            If MostrarEstadoIntegracion Then
                v_Estado = fldEstadoInt.Value
            Else
                v_Estado = Null
            End If

            Set oDen = Nothing
            Set oDen = New CMultiidiomas
            Set oDen.Conexion = m_oConexion
            For Each oIdi In oIdiomas
                oDen.Add oIdi.Cod, rs.Fields("DEN_" & oIdi.Cod).Value
            Next
            Me.Add fldCod.Value, oDen, lIndice, fldFecAct.Value, v_Estado
            
            rs.MoveNext
            lIndice = lIndice + 1
        Wend
        
    Else
        
        While Not rs.eof
            ' Dejamos en manos del programador cuando asignar la conexion al obejto usuario
            If MostrarEstadoIntegracion Then
                v_Estado = fldEstadoInt.Value
            Else
                v_Estado = Null
            End If
            
            Set oDen = Nothing
            Set oDen = New CMultiidiomas
            Set oDen.Conexion = m_oConexion
            For Each oIdi In oIdiomas
                oDen.Add oIdi.Cod, rs.Fields("DEN_" & oIdi.Cod).Value
            Next


            Me.Add fldCod.Value, oDen, , fldFecAct.Value, v_Estado
            
            rs.MoveNext
        Wend
    End If
    
    rs.Close
    Set rs = Nothing
    Set fldCod = Nothing
    Set oDen = Nothing
    Set fldFecAct = Nothing
    Set fldEstadoInt = Nothing
    Exit Sub
      
End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CViaPagos", "CargarTodosLosPagos", ERR, Erl)
      Exit Sub
   End If
End Sub
Public Function Add(ByVal Cod As String, ByVal oDen As CMultiidiomas, Optional ByVal varIndice As Variant, Optional ByVal FecAct As Variant, Optional ByVal varEstado As Variant) As CViaPago
    'create a new object
    Dim objnewmember As CViaPago
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New CViaPago
   
    objnewmember.Cod = Cod
    If Not IsMissing(oDen) Then
        Set objnewmember.Denominaciones = oDen
    Else
        Set objnewmember.Denominaciones = Nothing
    End If
    
    objnewmember.EstadoIntegracion = varEstado 'Estado integracion
    objnewmember.FecAct = FecAct
    
    Set objnewmember.Conexion = m_oConexion
        
    If Not IsMissing(varIndice) And Not IsNull(varIndice) Then
        objnewmember.Indice = varIndice
        mCol.Add objnewmember, CStr(varIndice)
    Else
    'set the properties passed into the method
       mCol.Add objnewmember, CStr(Cod)
    End If
    
    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CViaPagos", "Add", ERR, Erl)
      Exit Function
   End If

End Function

Public Property Get Item(vntIndexKey As Variant) As CViaPago
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Friend Property Set Conexion(ByVal con As CConexion)
Set m_oConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = m_oConexion
End Property


Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
     Count = mCol.Count
End If


End Property


Public Sub Remove(vntIndexKey As Variant)
   
    mCol.Remove vntIndexKey
End Sub


Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
     Set NewEnum = mCol.[_NewEnum]
End Property


Private Sub Class_Initialize()
    Set mCol = New Collection
End Sub


Private Sub Class_Terminate()
    'destroys collection when this class is terminated
    Set mCol = Nothing
    Set m_oConexion = Nothing
End Sub

Public Property Get eof() As Boolean
    eof = m_bEOF
End Property

Public Property Let eof(ByVal vNewValue As Boolean)
    m_bEOF = vNewValue
End Property

''' <summary>Elimina las v�as de pago seleccionadas en el
'''         GS de la tabla VIA_PAG de la base de datos.</summary>
''' <param name="Codigos">Es un array con los c�digos de los pagos a eliminar</param>
''' <param name="Usuario">Usuario conectado que elimina</param>
''' <returns>Una estructura de tipo TipoErrorSummit en la que
'''         se guardan los datos si se produce algun error </returns>
''' <remarks>Llamada desde: frmPARViaPag\cmdEliminar_Click
''' Tiempo m�ximo: 1 sec </remarks>
''' <revision>JVS 13/01/2011</revision>
Public Function EliminarPagosDeBaseDatos(Codigos As Variant, ByVal Usuario As String) As TipoErrorSummit

'******************************************
'***** M U Y   I M P O R T A N T E     ****
'*****                                 ****
'***** SI SE REALIZAN CAMBIOS          ****
'***** DESPUES DEL 21/06/2002          ****
'***** MIRAR SI HAY QUE PASARLOS A     ****
'***** FSGSINTEGRATION.DLL             ****
'******************************************

Dim vNoEliminados() As Variant
Dim udtTESError As TipoErrorSummit
Dim i As Integer
Dim iRes As Integer
Dim rs As adodb.Recordset
Dim sConsulta As String
Dim bGrabarEnLog As Boolean
Dim a As Integer
Dim ContRegBorrados As Long
Dim oDen As CMultiidiomas
Dim oMulti As CMultiidioma
Dim lIdLogVIA_PAG As Long


If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
udtTESError.NumError = TESnoerror

If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls
    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
    
    bGrabarEnLog = GrabarEnLog 'Llamamos al m�todo una sola vez
    
    For i = 1 To UBound(Codigos)
    
        If bGrabarEnLog Then ' primero recuperamos los datos de la via de pago que se va a borrar
            
            Set oDen = Nothing
            Set oDen = New CMultiidiomas
            Set oDen.Conexion = m_oConexion
            sConsulta = "SELECT IDI, DEN FROM VIA_PAG_DEN WITH(NOLOCK) WHERE VIA_PAG =N'" & DblQuote(Codigos(i)) & "'"
            Set rs = New adodb.Recordset
            rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly

            While Not rs.eof
                oDen.Add rs.Fields("IDI").Value, rs.Fields("DEN").Value
            
                rs.MoveNext
            Wend
            rs.Close
            Set rs = Nothing
            
        End If
        
        ''MultiERP: Elimina de PAG_ERP
        m_oConexion.ADOCon.Execute "DELETE FROM VIA_PAG_ERP WHERE COD_GS=N'" & DblQuote(Codigos(i)) & "'"
        If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
        m_oConexion.ADOCon.Execute "DELETE FROM VIA_PAG_DEN WHERE VIA_PAG=N'" & DblQuote(Codigos(i)) & "'"
        If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
        m_oConexion.ADOCon.Execute "DELETE FROM VIA_PAG WHERE COD=N'" & DblQuote(Codigos(i)) & "'", ContRegBorrados
        If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
        If bGrabarEnLog And ContRegBorrados > 0 Then

            'MultiERP
            Set rs = New adodb.Recordset
            sConsulta = "SELECT COD FROM ERP WITH(NOLOCK)"
            rs.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
                While Not rs.eof
                sConsulta = "INSERT INTO LOG_VIA_PAG (ACCION, COD, ORIGEN, USU, ERP) VALUES ('" & Accion_Baja & "',N'" & DblQuote(Codigos(i)) & "'"
                sConsulta = sConsulta & "," & m_udtOrigen & ",'" & DblQuote(Usuario) & "','" & DblQuote(rs.Fields("COD").Value) & "')"
                m_oConexion.ADOCon.Execute sConsulta
                If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
                
                Dim ador As adodb.Recordset
                Set ador = New adodb.Recordset
                'CALIDAD Sin WITH(NOLOCK) en el FROM para asegurar la recuperaci�n del mayor valor de la tabla
                ador.Open "SELECT SCOPE_IDENTITY()", m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
                If Not ador.eof Then
                    lIdLogVIA_PAG = ador.Collect(0)
                End If
                ador.Close
                Set ador = Nothing
                
                If Not oDen Is Nothing Then
                    For Each oMulti In oDen
                        sConsulta = "INSERT INTO LOG_VIA_PAG_DEN (ID_LOG_VIA_PAG,DEN,IDI) VALUES (" & lIdLogVIA_PAG & ","
                        sConsulta = sConsulta & StrToSQLNULLNVar(oMulti.Den) & ",'" & DblQuote(oMulti.Cod) & "')"
                        m_oConexion.ADOCon.Execute sConsulta
                        If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
                    Next
                End If
                
                rs.MoveNext
            Wend
            rs.Close
            Set rs = Nothing
        End If

NoLog:  a = 1
    
    Next
    
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"

    If iRes > 0 Then
        udtTESError.NumError = TESImposibleEliminar
        udtTESError.Arg1 = vNoEliminados
    End If
    EliminarPagosDeBaseDatos = udtTESError
    Exit Function

Error_Cls:
      udtTESError = basErrores.TratarError(m_oConexion.ADOCon.Errors)
      If m_oConexion.ADOCon.Errors(0).NativeError = 547 Then
            
        ReDim Preserve vNoEliminados(3, iRes)
        vNoEliminados(0, iRes) = Codigos(i)
        vNoEliminados(1, iRes) = udtTESError.Arg1
        vNoEliminados(2, iRes) = i
        vNoEliminados(3, iRes) = ""
        iRes = iRes + 1
        Resume NoLog

      Else
        
        m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
        EliminarPagosDeBaseDatos = udtTESError
      
      End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      'Call g_oErrores.TratarError("Class Module", "CViaPagos", "EliminarPagosDeBaseDatos", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If
   
End Function
Private Function GrabarEnLog() As Boolean
'***********************************************************************************
'*** Descripci�n: Nos indica si hay que grabar o no el cambio en el log de       ***
'***              V�as de pago (tabla LOG_VIA_PAG) y carga en la variable        ***
'***              del m�dulo m_udtOrigen el origen del movimiento                ***
'*** Parametros:  Ninguno                                                        ***
'*** Devuelve:    True (hay que grabar) False (No hay que grabar)                ***
'***********************************************************************************

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If gParametrosGenerales.gbACTIVLOG Or gParametrosIntegracion.gaExportar(EntidadIntegracion.ViaPag) Then
        If gParametrosGenerales.gbACTIVLOG And gParametrosIntegracion.gaExportar(EntidadIntegracion.ViaPag) Then
            m_udtOrigen = FSGSIntReg
        Else
            If gParametrosGenerales.gbACTIVLOG Then
                m_udtOrigen = FSGSReg
            Else
                m_udtOrigen = FSGSInt
            End If
        End If
        GrabarEnLog = True
    Else
        GrabarEnLog = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CViaPagos", "GrabarEnLog", ERR, Erl)
      Exit Function
   End If

End Function

''' <summary>Carga todos los c�digos de v�as de pago disponibles.
''' Cargar� los c�digos que existan en la tabla de mapeo para el ERP </summary>
''' <param name="lEmpresa">ID de la empresa</param>
''' <param name="sCodProveERP">C�digo ERP del proveedor para esa empresa</param>
''' <param name="bOrdenadosPorDen">OPCIONAL. TRUE para devolver los datos ordenados por denominaci�n</param>
''' <remarks>Llamada desde: frmPedidos.sdbcViaPagoCod_DropDown(), frmPedidos.sdbcViaPagoDen_DropDown().
''' Tiempo m�ximo: 1 seg</remarks>
''' <revision>ngo 17/01/2012</revision>
Public Sub CargarViasPagosCorrespondenciaCon(ByVal lEmpresa As Long, ByVal sCodProveERP As String, Optional bOrdenadosPorDen As Boolean)
Dim rs As adodb.Recordset
Dim fldCod As adodb.Field
Dim fldDen As adodb.Field
Dim fldFecAct As adodb.Field
Dim fldEstadoInt As adodb.Field   'Estado integraci�n
Dim sConsulta As String
Dim lIndice As Long
Dim v_Estado As Variant
Dim oDen As CMultiidiomas
Dim oGestorParametros As CGestorParametros
Dim oIdiomas As CIdiomas
Dim oIdi As CIdioma
Dim sCod As String
Dim sCod2 As String ''MultiERP
'Carga todos los idiomas de la aplicaci�n:
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
Set oGestorParametros = New CGestorParametros
Set oGestorParametros.Conexion = m_oConexion
Set oIdiomas = oGestorParametros.DevolverIdiomas(False, False, True)

Set rs = New adodb.Recordset

    For Each oIdi In oIdiomas
        sCod = sCod & ", " & oIdi.Cod & ".DEN AS DEN_" & oIdi.Cod
        sCod2 = sCod2 & " LEFT JOIN VIA_PAG_DEN " & oIdi.Cod & " WITH(NOLOCK) ON VIA_PAG.COD = " & oIdi.Cod & ".VIA_PAG AND " & oIdi.Cod & ".IDIOMA = '" & oIdi.Cod & "' "
    Next

'Carga las v�as de pago asociadas al proveedor para el ERP correspondiente
sConsulta = "SELECT DISTINCT VIA_PAG.* "
sConsulta = sConsulta & sCod
sConsulta = sConsulta & " FROM VIA_PAG WITH (NOLOCK) "
sConsulta = sConsulta & sCod2
sConsulta = sConsulta & " INNER JOIN VIA_PAG_ERP WITH (NOLOCK) ON VIA_PAG.COD=VIA_PAG_ERP.COD_GS "
sConsulta = sConsulta & " INNER JOIN ERP_SOCIEDAD WITH (NOLOCK) ON VIA_PAG_ERP.ERP=ERP_SOCIEDAD.ERP"
sConsulta = sConsulta & " INNER JOIN EMP WITH (NOLOCK) ON ERP_SOCIEDAD.SOCIEDAD = EMP.SOCIEDAD"
sConsulta = sConsulta & " WHERE EMP.ID = " & lEmpresa
rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
If rs.eof Then
    'Si no hay datos en las tablas de mapeo se cargan todos los datos que hay en PAG.
    CargarTodosLosPagosDesde gParametrosInstalacion.giCargaMaximaCombos, , , bOrdenadosPorDen, False
    rs.Close
    Set rs = Nothing
    Exit Sub
Else
    Set mCol = Nothing
    Set mCol = New Collection
    Set fldCod = rs.Fields("COD")
    Set fldFecAct = rs.Fields("FECACT")
    While Not rs.eof
        Set oDen = Nothing
        Set oDen = New CMultiidiomas
        Set oDen.Conexion = m_oConexion
        For Each oIdi In oIdiomas
            oDen.Add oIdi.Cod, "" & rs.Fields("DEN_" & oIdi.Cod).Value
        Next
        Me.Add "" & fldCod.Value, oDen, , fldFecAct.Value
        rs.MoveNext
    Wend
    rs.Close
    Set rs = Nothing
    Set fldCod = Nothing
    Set oDen = Nothing
    Set fldFecAct = Nothing
    Set fldEstadoInt = Nothing
    Exit Sub
End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CViaPagos", "CargarViasPagosCorrespondenciaCon", ERR, Erl)
      Exit Sub
   End If
End Sub


''' <summary>
''' Recupera la v�a de pago del proveedor ERP. Si es null, recupera la v�a de pago del proveedor FS
''' </summary>
''' <param name="lEmpresa">Id de la empresa</param>
''' <param name="sCodProveERP">C�digo del proveedor ERP</param>
''' <returns>El c�digo de la v�a de pago recuperada (Null si ninguno de los dos proveedores tiene v�a de pago asignada)</returns>
''' <remarks>Tiempo m�ximo: 0 seg </remarks>

Public Function strViaPagoProvERP(ByVal lEmpresa As Long, ByVal sCodProveERP As String) As String
Dim sConsulta As String
Dim rs As New adodb.Recordset

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sConsulta = "SELECT DISTINCT PROVE_ERP.VIA_PAG,PROVE.VIA_PAG AS VIAPAGPROVE FROM PROVE_ERP WITH(NOLOCK)"
    sConsulta = sConsulta & " LEFT JOIN PROVE_EMPRESA_COD_ERP WITH (NOLOCK) ON PROVE_ERP.COD_ERP=PROVE_EMPRESA_COD_ERP.COD_ERP"
    sConsulta = sConsulta & " AND PROVE_ERP.EMPRESA=PROVE_EMPRESA_COD_ERP.EMPRESA "
    sConsulta = sConsulta & " LEFT JOIN PROVE WITH (NOLOCK) ON PROVE_EMPRESA_COD_ERP.PROVE=PROVE.COD "
    sConsulta = sConsulta & " WHERE PROVE_ERP.EMPRESA = " & lEmpresa
    sConsulta = sConsulta & " AND PROVE_ERP.COD_ERP= " & StrToSQLNULL(sCodProveERP) & " AND PROVE_ERP.BAJALOG=0"
    
    rs.CursorLocation = adUseClient
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    If Not rs.eof Then
        If IsNull(rs.Fields("VIA_PAG").Value) Then
            If IsNull(rs.Fields("VIAPAGPROVE").Value) Then
                strViaPagoProvERP = ""
            Else
                strViaPagoProvERP = NullToStr(rs.Fields("VIAPAGPROVE").Value)
            End If
        Else
            strViaPagoProvERP = NullToStr(rs.Fields("VIA_PAG").Value)
        End If
    End If
    rs.Close
    Set rs.ActiveConnection = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CViaPagos", "strViaPagoProvERP", ERR, Erl)
      Exit Function
   End If
End Function


