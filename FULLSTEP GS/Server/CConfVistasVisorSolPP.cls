VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CConfVistasVisorSolPP"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit


Private mCol As Collection
Private m_oConexion As CConexion
Private m_oConexionServer As CConexionDeUseServer
Private m_bEOF As Boolean
Private m_bHayCambios As Boolean

Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum

Public Property Let HayCambios(ByVal dato As Boolean)
    m_bHayCambios = dato
End Property

Public Property Get HayCambios() As Boolean
    HayCambios = m_bHayCambios
End Property

''' <summary>
''' A�ade la configuraci�n de partidas presupuestarias al objeto CConfVistaVisorSolPP
''' </summary>
''' <param name="sUsuario">Cod del usuario</param>
''' <param name="ConfigVisorSolPP">Configuraci�n de partidas presupuestarias</param>
''' <param name="vIndice">si se usa indice o no</param>
''' <returns>Un objeto CConfVistaVisorSolPP</returns>
''' <remarks>Llamada desde:CargarConfVisorSolPP; Tiempo m�ximo:0,1</remarks>
Public Function Add(ByVal sUsuario As String, ConfigVisorSolPP As TipoConfigVisorSolPP, Optional ByVal vIndice As Variant) As CConfVistaVisorSolPP
    
    'create a new object
    Dim objnewmember As CConfVistaVisorSolPP
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New CConfVistaVisorSolPP
    
    objnewmember.Usuario = sUsuario
    
    objnewmember.Pres5_Nivel0 = ConfigVisorSolPP.sPres5_Nivel0
    objnewmember.Pres5_Nivel1 = ConfigVisorSolPP.sPres5_Nivel1
    objnewmember.Pres5_Nivel2 = ConfigVisorSolPP.sPres5_Nivel2
    objnewmember.Pres5_Nivel3 = ConfigVisorSolPP.sPres5_Nivel3
    objnewmember.Pres5_Nivel4 = ConfigVisorSolPP.sPres5_Nivel4
       
    Set objnewmember.Conexion = m_oConexion
    Set objnewmember.ConexionServer = m_oConexionServer
       
    If Not IsMissing(vIndice) And Not IsNull(vIndice) Then
        objnewmember.Indice = vIndice
        mCol.Add objnewmember, CStr(vIndice)
    Else
        mCol.Add objnewmember
    End If
    
    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CConfVistasVisorSolPP", "Add", ERR, Erl)
        Exit Function
    End If

End Function

Friend Property Set Conexion(ByVal con As CConexion)
    Set m_oConexion = con
End Property

Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property

Friend Property Set ConexionServer(ByVal con As CConexionDeUseServer)
    Set m_oConexionServer = con
End Property

Friend Property Get ConexionServer() As CConexionDeUseServer
    Set ConexionServer = m_oConexionServer
End Property

Public Property Get Count() As Long
    If mCol Is Nothing Then
        Count = 0
    Else
         Count = mCol.Count
    End If
End Property

Public Property Get eof() As Boolean
    eof = m_bEOF
End Property

Friend Property Let eof(ByVal b As Boolean)
    m_bEOF = b
End Property

Public Property Get Item(vntIndexKey As Variant) As CConfVistaVisorSolPP
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Public Property Get NewEnum() As IUnknown
     Set NewEnum = mCol.[_NewEnum]
End Property

Public Sub Remove(vntIndexKey As Variant)
On Error GoTo ERROR

    mCol.Remove vntIndexKey

ERROR:

End Sub

Private Sub Class_Initialize()
    Set mCol = New Collection
End Sub

Private Sub Class_Terminate()
    'Proceroys collection when this class is terminated
    On Error Resume Next
    Set mCol = Nothing
    Set m_oConexion = Nothing
End Sub

''' <summary>
''' Cargar las partidas presupuestarias de configuraci�n del visor de solicitudes de un usuario
''' </summary>
''' <param name="sUsuario">Cod del usuario</param>
''' <param name="vIndice">si se usa indice o no</param>
''' <remarks>Llamada desde:frmSolicitudes.CargarConfiguracionVisorSolPP; Tiempo m�ximo:1</remarks>
Public Sub CargarConfVisorSolPP(ByVal sUsuario As String, Optional ByVal UsarIndice As Boolean)
    Dim AdoRes As New adodb.Recordset
    Dim sConsulta As String
    Dim lIndice As Long
    Dim TpConfVisorSolPP As TipoConfigVisorSolPP
    
    
    '********* Precondicion **************************************
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CConfVistasVisor.CargarConfVisor", "No se ha establecido la conexion"
        Exit Sub
    End If
    '*************************************************************
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

    sConsulta = "SELECT PRES5_NIV0, PRES5_NIV1, PRES5_NIV2, PRES5_NIV3, PRES5_NIV4 " _
    & "FROM CONF_VISOR_SOL_PP WITH (NOLOCK) WHERE USU='" & DblQuote(sUsuario) & "'"
    AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly

    If AdoRes.eof Then
        AdoRes.Close
        Set AdoRes = Nothing
        If mCol Is Nothing Then
            Set mCol = Nothing
            Set mCol = New Collection
        End If
        Exit Sub

    Else
        If mCol Is Nothing Then
            Set mCol = Nothing
            Set mCol = New Collection
        End If
                        
        If UsarIndice Then
            lIndice = 0

            While Not AdoRes.eof
                TpConfVisorSolPP.sPres5_Nivel0 = AdoRes.Fields("PRES5_NIV0").Value
                TpConfVisorSolPP.sPres5_Nivel1 = AdoRes.Fields("PRES5_NIV1").Value
                TpConfVisorSolPP.sPres5_Nivel2 = NullToStr(AdoRes.Fields("PRES5_NIV2").Value)
                TpConfVisorSolPP.sPres5_Nivel3 = NullToStr(AdoRes.Fields("PRES5_NIV3").Value)
                TpConfVisorSolPP.sPres5_Nivel4 = NullToStr(AdoRes.Fields("PRES5_NIV4").Value)
                
                Me.Add sUsuario, TpConfVisorSolPP, lIndice
                AdoRes.MoveNext
                lIndice = lIndice + 1
            Wend
        Else
            While Not AdoRes.eof
                TpConfVisorSolPP.sPres5_Nivel0 = AdoRes.Fields("PRES5_NIV0").Value
                TpConfVisorSolPP.sPres5_Nivel1 = AdoRes.Fields("PRES5_NIV1").Value
                TpConfVisorSolPP.sPres5_Nivel2 = NullToStr(AdoRes.Fields("PRES5_NIV2").Value)
                TpConfVisorSolPP.sPres5_Nivel3 = NullToStr(AdoRes.Fields("PRES5_NIV3").Value)
                TpConfVisorSolPP.sPres5_Nivel4 = NullToStr(AdoRes.Fields("PRES5_NIV4").Value)
            
                Me.Add sUsuario, TpConfVisorSolPP
                AdoRes.MoveNext
            Wend
        End If
        
        AdoRes.Close
        Set AdoRes = Nothing
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CConfVistasVisorSolPP", "CargarConfVisorSolPP", ERR, Erl)
        Exit Sub
    End If
End Sub


Public Function EliminarConfiguracionDeBaseDatos(ByVal sUsuario As String) As TipoErrorSummit
    Dim sConsulta As String
    Dim TESError As TipoErrorSummit

    '''Precondicion
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "cConfVistaVisorSol.EliminarDeBaseDatos", "No se ha establecido la conexion"
        Exit Function
    End If
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error GoTo Error_Cls

    TESError.NumError = TESnoerror
     
    sConsulta = "DELETE FROM CONF_VISOR_SOL_PP WHERE USU='" & DblQuote(sUsuario) & "'"
        
    m_oConexion.ADOCon.Execute sConsulta

    EliminarConfiguracionDeBaseDatos = TESError

    Exit Function

Error_Cls:
    EliminarConfiguracionDeBaseDatos = basErrores.TratarError(m_oConexion.ADOCon.Errors)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        'Call g_oErrores.TratarError("Class Module", "CConfVistasVisorSolPP", "EliminarConfiguracionDeBaseDatos", ERR, Erl)
        GoTo Error_Cls
        Exit Function
    End If

End Function

