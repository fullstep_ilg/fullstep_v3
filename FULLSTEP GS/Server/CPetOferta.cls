VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CPetOferta"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

'Private mvarResultset As rdoResultset
Private mvarIndice As Variant 'Nos indica la posicion secuencial dentro de una Coleccion
Private mvarCodProve As String 'local copy
Private mvarDenProve As String 'local copy
Private mvarId As Integer
Private mvarApellidos As String
Private mvarNombre As Variant
Private mvarEMail As Variant
Private mvarTfno As Variant
Private mvarTfno2 As Variant
Private mvarTfnoMovil As Variant
Private mvarFax As Variant
Private mvarFecha As Date
Private mvarViaWeb As Boolean
Private mvarViaEMail As Boolean
Private mvarViaImpreso As Boolean
Private mvarFechaReunion As Variant
Private mvarTipoPet As Variant
Private mvarAntesDeCierreParcial As Boolean

Private mvarMailCancel As Boolean
Private mvarPortContacto As Boolean

Private mvarConexion As CConexion 'local copy
Private mvarConexionServer As CConexionDeUseServer
Public Property Get Nombre() As Variant
    Nombre = mvarNombre
End Property
Public Property Let Nombre(ByVal Val As Variant)
    mvarNombre = Val
End Property
Public Property Get Email() As Variant
    Email = mvarEMail
End Property
Public Property Let Email(ByVal Val As Variant)
    mvarEMail = Val
End Property
Public Property Get Tfno() As Variant
    Tfno = mvarTfno
End Property
Public Property Let Tfno(ByVal Val As Variant)
    mvarTfno = Val
End Property
Public Property Get Tfno2() As Variant
    Tfno2 = mvarTfno2
End Property
Public Property Let fechareunion(ByVal Val As Variant)
    mvarFechaReunion = Val
End Property
Public Property Get fechareunion() As Variant
    fechareunion = mvarFechaReunion
End Property
Public Property Get TipoComunicacion() As Variant
    TipoComunicacion = mvarTipoPet
End Property
Public Property Let TipoComunicacion(ByVal Val As Variant)
    mvarTipoPet = Val
End Property

Public Property Let Tfno2(ByVal Val As Variant)
    mvarTfno2 = Val
End Property
Public Property Get Fax() As Variant
    Fax = mvarFax
End Property
Public Property Let Fax(ByVal Val As Variant)
    mvarFax = Val
End Property

Public Property Get Id() As Integer
    Id = mvarId
End Property
Public Property Let Id(ByVal Val As Integer)
    mvarId = Val
End Property
Public Property Get Fecha() As Date
    Fecha = mvarFecha
End Property
Public Property Let Fecha(ByVal Val As Date)
    mvarFecha = Val
End Property
Public Property Get CodProve() As String
    CodProve = mvarCodProve
End Property
Public Property Let CodProve(ByVal Cod As String)
    mvarCodProve = Cod
End Property
Public Property Get Apellidos() As String
    Apellidos = mvarApellidos
End Property
Public Property Let Apellidos(ByVal Cod As String)
    mvarApellidos = Cod
End Property
Public Property Get AntesDeCierreParcial() As Boolean
    AntesDeCierreParcial = mvarAntesDeCierreParcial
End Sub
Public Property Let AntesDeCierreParcial(ByVal b As Boolean)
    mvarAntesDeCierreParcial = b
End Property

Public Property Get MailCancel() As Boolean
    MailCancel = mvarMailCancel
End Property
Public Property Let MailCancel(ByVal b As Boolean)
    mvarMailCancel = b
End Property

Public Property Get DenProve() As String
    DenProve = mvarDenProve
End Property
Public Property Let DenProve(ByVal Den As String)
    mvarDenProve = Den
End Property
Public Property Get ViaWeb() As Boolean
    ViaWeb = mvarViaWeb
End Property
Public Property Let ViaWeb(ByVal b As Boolean)
    mvarViaWeb = b
End Property
Public Property Get viaEMail() As Boolean
    viaEMail = mvarViaEMail
End Property
Public Property Let viaEMail(ByVal b As Boolean)
    mvarViaEMail = b
End Property
Public Property Get ViaImp() As Boolean
    ViaImp = mvarViaImpreso
End Property
Public Property Let ViaImp(ByVal b As Boolean)
    mvarViaImpreso = b
End Property

Public Property Get PortContacto() As Boolean
    PortContacto = mvarPortContacto
End Property
Public Property Let PortContacto(ByVal dato As Boolean)
    mvarPortContacto = dato
End Property

Public Property Get Indice() As Variant
    Indice = mvarIndice
End Property
Public Property Let Indice(ByVal varInd As Variant)
    mvarIndice = varInd
End Property
Friend Property Set Conexion(ByVal vData As CConexion)
    Set mvarConexion = vData
End Property
Friend Property Get Conexion() As CConexion
    Set Conexion = mvarConexion
End Property
Friend Property Set ConexionServer(ByVal con As CConexionDeUseServer)
Set mvarConexionServer = con
End Property

Friend Property Get ConexionServer() As CConexionDeUseServer
Set ConexionServer = mvarConexionServer
End Property

Private Sub ConectarDeUseServer()

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If mvarConexionServer.ADOSummitCon Is Nothing Then
        Set mvarConexionServer.ADOSummitCon = New adodb.Connection
        mvarConexionServer.ADOSummitCon.Open "Provider=SQLOLEDB.1;Auto Translate=FALSE;Server=" & basParametros.ServidorGS & ";Database=" & basParametros.BaseDatosGS & ";", LICDBLogin, LICDBContra
        mvarConexionServer.ADOSummitCon.CursorLocation = adUseServer
        mvarConexionServer.ADOSummitCon.CommandTimeout = 120
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CPetOferta", "ConectarDeUseServer", ERR, Erl)
      Exit Sub
   End If
End Sub

Private Sub Class_Terminate()
    Set mvarConexion = Nothing
    Set mvarConexionServer = Nothing
    
    
End Sub

Public Function DevolverMensaje() As String

' PENDIENTE

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    DevolverMensaje = "Le comunicamos que ha sido seleccionado como proveedor potencial para el proceso X." & vbLf & " Rellene el impreso en la wwww.sumitweb.com si le interesa."
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CPetOferta", "DevolverMensaje", ERR, Erl)
      Exit Function
   End If
    
End Function


Public Property Get tfnomovil() As Variant
    tfnomovil = mvarTfnoMovil
End Property

Public Property Let tfnomovil(ByVal Val As Variant)
    mvarTfnoMovil = Val
End Property
