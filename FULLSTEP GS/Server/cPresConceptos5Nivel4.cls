VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cPresConceptos5Nivel4"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum

Private mCol As Collection
Private m_oConexion As CConexion
Private m_oConexionServer As CConexionDeUseServer
Private m_bEOF As Boolean

''' <summary>
''' Funci�n que recibe los datos de una partida presupuestaria de nivel 4 y los a�ade a una instancia de la clase cPresConcep5Nivel4
''' que luego a�ade a una colecci�n a nivel de clase (mCol) que se usa en otros procedimientos. Devuelve el objeto cPresConcep5Nivel4 creado.
''' </summary>
''' <param name="Pres0">String. C�digo de la partida presupuestaria padre (Nivel 0)</param>
''' <param name="Pres1">String. C�digo de la partida presupuestaria padre (Nivel 1)</param>
''' <param name="Pres2">String. C�digo de la partida presupuestaria padre (Nivel 2)</param>
''' <param name="Pres3">String. C�digo de la partida presupuestaria padre (Nivel 3)</param>
''' <param name="Cod">String. C�digo de la partida presupuestaria</param>
''' <param name="Den">String. Descripci�n de la partida presupuestaria</param>
''' <param name="BajaLog">Boolean. Opcional. Indica si la partida presupuestaria es baja l�gica (1) o no (0)</param>
''' <param name="varIndice">Variant. Opcional. �ndice de la partida presupuestaria. En la funci�n se usa al a�adir a la colecci�n la partida presupuestaria</param>
''' <param name="Seleccionado">Integer. Opcional. Informa sobre si la partida presupuestaria est� vinculada a un centro de coste concreto que es el que se ha usado para recuperar esta informaci�n</param>
''' <returns>La funci�n devuelve el objeto cPresConcep5Nivel1 creado, con los datos de la partida presup.</returns>
''' <remarks>Llamada desde CargarPresupuestosConceptos5; Tiempo m�ximo < 1 seg.</remarks>
Public Function Add(ByVal sPRES0 As String, ByVal sPRES1 As String, ByVal sPRES2 As String, ByVal sPRES3 As String, ByVal Cod As String, ByVal Den As String, Optional ByVal bBajaLog As Boolean, Optional ByVal varIndice As Variant, Optional ByVal Seleccionado As Integer) As cPresConcep5Nivel4
    'create a new object
    
    Dim objnewmember As cPresConcep5Nivel4
    Dim sCod As String
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New cPresConcep5Nivel4
   
    objnewmember.PRES0 = sPRES0
    objnewmember.PRES1 = sPRES1
    objnewmember.PRES2 = sPRES2
    objnewmember.Pres3 = sPRES3

    objnewmember.Cod = Cod
    objnewmember.Den = Den
    
    If NoHayParametro(bBajaLog) Then
        objnewmember.BajaLog = False
    Else
        objnewmember.BajaLog = bBajaLog
    End If
    If NoHayParametro(Seleccionado) Then
        objnewmember.Seleccionado = 0
    Else
        objnewmember.Seleccionado = Seleccionado
    End If
    
        
    Set objnewmember.Conexion = m_oConexion
    Set objnewmember.ConexionServer = m_oConexionServer
    If Not IsMissing(varIndice) And Not IsNull(varIndice) Then
        objnewmember.Indice = varIndice
        mCol.Add objnewmember, CStr(varIndice)
    Else
    
        sCod = sPRES1 & Mid$(Space(basParametros.gLongitudesDeCodigos.giLongCodPres5Niv1), 1, basParametros.gLongitudesDeCodigos.giLongCodPres5Niv1 - Len(sPRES1))
        sCod = sCod & sPRES2 & Mid$(Space(basParametros.gLongitudesDeCodigos.giLongCodPres5Niv2), 1, basParametros.gLongitudesDeCodigos.giLongCodPres5Niv2 - Len(sPRES2))
        sCod = sCod & sPRES3 & Mid$(Space(basParametros.gLongitudesDeCodigos.giLongCodPres5Niv3), 1, basParametros.gLongitudesDeCodigos.giLongCodPres5Niv3 - Len(sPRES3))
        sCod = sCod & Cod & Mid$(Space(basParametros.gLongitudesDeCodigos.giLongCodPres5Niv4), 1, basParametros.gLongitudesDeCodigos.giLongCodPres5Niv4 - Len(Cod))
    
        mCol.Add objnewmember, sCod
    End If
    
    Set Add = objnewmember
    Set objnewmember = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "cPresConceptos5Nivel4", "Add", ERR, Erl)
      Exit Function
   End If

End Function

Public Property Get Item(vntIndexKey As Variant) As cPresConcep5Nivel4
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Friend Property Set Conexion(ByVal con As CConexion)
Set m_oConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = m_oConexion
End Property

Friend Property Set ConexionServer(ByVal con As CConexionDeUseServer)
Set m_oConexionServer = con
End Property

Friend Property Get ConexionServer() As CConexionDeUseServer
Set ConexionServer = m_oConexionServer
End Property

Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
     Count = mCol.Count
End If


End Property


Public Sub Remove(vntIndexKey As Variant)
    mCol.Remove vntIndexKey
End Sub


Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
     Set NewEnum = mCol.[_NewEnum]
End Property


Private Sub Class_Initialize()
   
    Set mCol = New Collection
End Sub


Private Sub Class_Terminate()
    'destroys collection when this class is terminated
    Set mCol = Nothing
    Set m_oConexion = Nothing
    Set m_oConexionServer = Nothing
End Sub

Public Function DevolverLosCodigos() As TipoDatosCombo
      
End Function

Public Property Get eof() As Boolean
    eof = m_bEOF
End Property

Friend Property Let eof(ByVal b As Boolean)
    m_bEOF = b
End Property

''' <summary>
''' Devuelve todos las partidas de nivel 4 que cumplen los criterios de busqueda
''' </summary>
''' <param name="sPres0">Arbol de nivel 0</param>
''' <param name="CaracteresinicialesCod">Valor de codigo por el q buscar</param>
''' <param name="CaracteresinicialesDen">Valor de den por el q buscar</param>
''' <param name="CoincidenciaTotal">Indica si se hace la where con like o con = </param>
''' <param name="OrdenadosPorDen">Si se ordena por denominacion el resultado de la busqueda.</param>
''' <param name="bBajaLog">Si se muestran los nodos de baja logica</param>
''' <param name="UsarIndice">Si se usa indice al a�adir a la coleccion</param>
''' <remarks>Llamada desde frmPresCon5Buscar.cmdCargarClick(); Tiempo m�ximo < 1 seg.</remarks>
Public Sub CargarPresupuestosConceptos5(Optional ByVal sPRES0 As String, Optional ByVal sPRES1 As String, Optional ByVal sPRES2 As String, Optional ByVal sPRES3 As String, Optional ByVal CaracteresInicialesCod As String, Optional ByVal CaracteresInicialesDen As String, Optional ByVal CoincidenciaTotal As Boolean, Optional ByVal OrdenadosPorDen As Boolean, Optional ByVal bBajaLog As Variant, Optional ByVal UsarIndice As Boolean)

Dim rs As adodb.Recordset
Dim fldPres0 As adodb.Field 'Pres0
Dim fldPres1 As adodb.Field 'Pres1
Dim fldPres2 As adodb.Field 'Pres2
Dim fldPres3 As adodb.Field 'Pres3
Dim fldCod As adodb.Field   'Cod
Dim fldDen As adodb.Field   'Den
Dim fldBajaLog As adodb.Field  'BAJALOG

Dim sConsulta As String
Dim lIndice As Long

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
    sConsulta = "SELECT PRES5_NIV4.*,PRES5_IDIOMAS.DEN FROM PRES5_NIV4 WITH (NOLOCK)"
    sConsulta = sConsulta & "INNER JOIN PRES5_IDIOMAS WITH (NOLOCK) "
    sConsulta = sConsulta & " ON PRES5_NIV4.PRES0 = PRES5_IDIOMAS.PRES0 AND "
    sConsulta = sConsulta & "  PRES5_NIV4.PRES1 = PRES5_IDIOMAS.PRES1 AND PRES5_NIV4.PRES2=PRES5_IDIOMAS.PRES2 AND PRES5_NIV4.PRES3 = PRES5_IDIOMAS.PRES3 AND PRES5_NIV4.COD=PRES5_IDIOMAS.PRES4"
    sConsulta = sConsulta & " WHERE 1=1 "
    
    
    If CaracteresInicialesCod = "" And CaracteresInicialesDen = "" Then
       
    Else
        If Not (CaracteresInicialesCod = "") And Not (CaracteresInicialesDen = "") Then
            
            If CoincidenciaTotal Then
                sConsulta = sConsulta & " AND PRES5_NIV4.COD ='" & DblQuote(CaracteresInicialesCod) & "'"
                sConsulta = sConsulta & " AND PRES5_IDIOMAS.DEN='" & DblQuote(CaracteresInicialesDen) & "'"
            Else
                sConsulta = sConsulta & " AND PRES5_NIV4.COD LIKE '" & DblQuote(CaracteresInicialesCod) & "%'"
                sConsulta = sConsulta & " AND PRES5_IDIOMAS.DEN LIKE '" & DblQuote(CaracteresInicialesDen) & "%'"
            End If
        Else
            If Not (CaracteresInicialesCod = "") Then
                If CoincidenciaTotal Then
                    sConsulta = sConsulta & " AND PRES5_NIV4.COD ='" & DblQuote(CaracteresInicialesCod) & "'"
                Else
                    sConsulta = sConsulta & " AND PRES5_NIV4.COD LIKE '" & DblQuote(CaracteresInicialesCod) & "%'"
                End If
            Else
                If CoincidenciaTotal Then
                    sConsulta = sConsulta & " AND PRES5_IDIOMAS.DEN='" & DblQuote(CaracteresInicialesDen) & "'"
                Else
                    sConsulta = sConsulta & " AND PRES5_IDIOMAS.DEN LIKE '" & DblQuote(CaracteresInicialesDen) & "%'"
                End If
            End If
        End If
    End If
    
    sConsulta = sConsulta & " AND PRES5_IDIOMAS.IDIOMA='" & DblQuote(gParametrosInstalacion.gIdioma) & "'"
    
    If Not NoHayParametro(bBajaLog) Then
        sConsulta = sConsulta & " AND PRES5_NIV4.BAJALOG=" & BooleanToSQLBinary(bBajaLog)
    End If
    
    If Trim(sPRES0) <> "" Then
        sConsulta = sConsulta & " AND PRES5_NIV4.PRES0='" & DblQuote(sPRES0) & "'"
    End If
    
    If Trim(sPRES1) <> "" Then
        sConsulta = sConsulta & " AND PRES5_NIV4.PRES1='" & DblQuote(sPRES1) & "'"
    End If
        
    If Trim(sPRES2) <> "" Then
        sConsulta = sConsulta & " AND PRES5_NIV4.PRES2='" & DblQuote(sPRES2) & "'"
    End If
    
    If Trim(sPRES3) <> "" Then
        sConsulta = sConsulta & " AND PRES5_NIV4.PRES3='" & DblQuote(sPRES3) & "'"
    End If

    If OrdenadosPorDen Then
        sConsulta = sConsulta & " ORDER BY PRES5_IDIOMAS.DEN,PRES5_NIV4.COD"
    Else
        sConsulta = sConsulta & " ORDER BY PRES5_NIV4.COD,PRES5_IDIOMAS.DEN"
    End If
          
    Set rs = New adodb.Recordset
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    
    If rs.eof Then
            
        rs.Close
        Set rs = Nothing
        Set mCol = Nothing
        Set mCol = New Collection
        Exit Sub
          
    Else
        
        Set mCol = Nothing
        Set mCol = New Collection
        Set fldPres0 = rs.Fields("PRES0")
        Set fldPres1 = rs.Fields("PRES1")
        Set fldPres2 = rs.Fields("PRES2")
        Set fldPres3 = rs.Fields("PRES3")
        Set fldCod = rs.Fields("COD")
        Set fldDen = rs.Fields("DEN")
        Set fldBajaLog = rs.Fields.Item("BAJALOG")
        
        If UsarIndice Then
            lIndice = 0
            While Not rs.eof
                Me.Add fldPres0.Value, fldPres1.Value, fldPres2.Value, fldPres3.Value, fldCod.Value, fldDen.Value, fldBajaLog.Value, lIndice
                rs.MoveNext
                lIndice = lIndice + 1
            Wend
        Else
            While Not rs.eof
                Me.Add fldPres0.Value, fldPres1.Value, fldPres2.Value, fldPres3.Value, fldCod.Value, fldDen.Value, fldBajaLog.Value
                rs.MoveNext
            Wend
        End If
        
        rs.Close
        Set rs = Nothing
        Set fldPres0 = Nothing
        Set fldPres1 = Nothing
        Set fldPres2 = Nothing
        Set fldPres3 = Nothing
        Set fldCod = Nothing
        Set fldDen = Nothing
        Set fldBajaLog = Nothing
    
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "cPresConceptos5Nivel4", "CargarPresupuestosConceptos5", ERR, Erl)
      Exit Sub
   End If
End Sub







