VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CPerfil"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'********************* CPerfil **********************************
'*             Autor : Javier Arana
'*             Creada : 14/7/98
'* **************************************************************
Option Explicit

Implements IBaseDatos

Private Enum TipoDeError
    ConexionNoEstablecida = 613
    DenominacionVacia = 614
End Enum

Private mvarConexion As CConexion
Private mvarConexionServer As CConexionDeUseServer
Private mlID As Long
Private mvarCod As String
Private mvarDen As String 'local copy
Private mvarAcciones As CAcciones 'local copy
Private mvarRecordset As adodb.Recordset
Private mvarPresup5Niv0 As cPresConceptos5Nivel0
Private mbFSGS As Boolean
Private mbFSPM As Boolean
Private mbFSQA As Boolean
Private mbFSEP As Boolean
Private mbFSSM As Boolean
Private mbFSCM As Boolean
Private mbFSIM As Boolean
Private mbFSIS As Boolean
Private mbFSBI As Boolean

' ********************* Metodos publicos de la clase ***************
Friend Property Set Conexion(ByVal con As CConexion)
Set mvarConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = mvarConexion
End Property

Friend Property Set ConexionServer(ByVal con As CConexionDeUseServer)
Set mvarConexionServer = con
End Property

Friend Property Get ConexionServer() As CConexionDeUseServer
Set ConexionServer = mvarConexionServer
End Property

Private Sub ConectarDeUseServer()
    If mvarConexionServer.ADOSummitCon Is Nothing Then
        Set mvarConexionServer.ADOSummitCon = New adodb.Connection
        mvarConexionServer.ADOSummitCon.Open "Provider=SQLOLEDB.1;Auto Translate=FALSE;Server=" & basParametros.ServidorGS & ";Database=" & basParametros.BaseDatosGS & ";", LICDBLogin, LICDBContra
        mvarConexionServer.ADOSummitCon.CursorLocation = adUseServer
        mvarConexionServer.ADOSummitCon.CommandTimeout = 120
    End If
End Sub
Public Property Set Acciones(ByVal vData As CAcciones)
    Set mvarAcciones = vData
End Property


Public Property Get Acciones() As CAcciones
    Set Acciones = mvarAcciones
End Property

Public Property Let Id(ByVal vData As Long)
    mlID = vData
End Property
Public Property Get Id() As Long
    Id = mlID
End Property

Public Property Let Cod(ByVal vData As String)
    mvarCod = vData
End Property
Public Property Get Cod() As String
    Cod = mvarCod
End Property

Public Property Let Den(ByVal vData As String)
    mvarDen = vData
End Property
Public Property Get Den() As String
    Den = mvarDen
End Property

Public Property Set Presups5Niv0(ByVal con As cPresConceptos5Nivel0)
Set mvarPresup5Niv0 = con
End Property

Public Property Get Presups5Niv0() As cPresConceptos5Nivel0
Set Presups5Niv0 = mvarPresup5Niv0
End Property

Public Property Get FSGS() As Boolean
    FSGS = mbFSGS
End Property
Public Property Let FSGS(ByVal vNewValue As Boolean)
    mbFSGS = vNewValue
End Property

Public Property Get FSPM() As Boolean
    FSPM = mbFSPM
End Property
Public Property Let FSPM(ByVal vNewValue As Boolean)
    mbFSPM = vNewValue
End Property

Public Property Get FSQA() As Boolean
    FSQA = mbFSQA
End Property
Public Property Let FSQA(ByVal vNewValue As Boolean)
    mbFSQA = vNewValue
End Property

Public Property Get FSEP() As Boolean
    FSEP = mbFSEP
End Property
Public Property Let FSEP(ByVal vNewValue As Boolean)
    mbFSEP = vNewValue
End Property

Public Property Get FSSM() As Boolean
    FSSM = mbFSSM
End Property
Public Property Let FSSM(ByVal vNewValue As Boolean)
    mbFSSM = vNewValue
End Property

Public Property Get FSCM() As Boolean
    FSCM = mbFSCM
End Property
Public Property Let FSCM(ByVal vNewValue As Boolean)
    mbFSCM = vNewValue
End Property

Public Property Get FSIM() As Boolean
    FSIM = mbFSIM
End Property
Public Property Let FSIM(ByVal vNewValue As Boolean)
    mbFSIM = vNewValue
End Property

Public Property Get FSIS() As Boolean
    FSIS = mbFSIS
End Property
Public Property Let FSIS(ByVal vNewValue As Boolean)
    mbFSIS = vNewValue
End Property

Public Property Get FSBI() As Boolean
    FSBI = mbFSBI
End Property
Public Property Let FSBI(ByVal vNewValue As Boolean)
    mbFSBI = vNewValue
End Property

Public Property Get accesofsint() As Boolean
    accesofsint = mbFSIS
End Property

Public Property Let accesofsint(ByVal vNewValue As Boolean)
    mbFSIS = vNewValue
End Property

''' <summary>
''' Carga de acciones desde la tabla de acciones de perfil
''' </summary>
'''<param optional name="Idioma">Idioma</param>
''' <remarks>Llamada desde: frmPERFIL.cmdRestaurar_Click; frmPERFIL.CargarAccionesPerfil;
''' Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 26/12/2011</revision>
Public Sub CargarAcciones(Optional ByVal Idioma As String)
    Dim AdoRes As adodb.Recordset
    Dim oAcciones As CAcciones
    Dim Id As Long
    Dim Den As String
    Dim fldACC As adodb.Field
    Dim fldDen As adodb.Field
    
    '********* Precondicion *******************
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If mvarConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CPerfil.ExpandirAcciones", "No se ha establecido la conexion"
        Exit Sub
    End If
    '*****************************************
        
    Set AdoRes = New adodb.Recordset
    AdoRes.Open "SELECT PERF_ACC.ACC,ACC_IDIOMA.DEN FROM PERF_ACC WITH(NOLOCK) INNER JOIN ACC WITH(NOLOCK) ON PERF_ACC.ACC=ACC.ID LEFT JOIN ACC_IDIOMA WITH(NOLOCK) ON ACC_IDIOMA.ID = ACC.ID AND ACC_IDIOMA.IDIOMA = '" & DblQuote(Idioma) & "' WHERE PERF_ACC.PERF=" & DblQuote(mlID) & " ORDER BY ACC.ORDEN", mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    
    Set oAcciones = New CAcciones
    If Not AdoRes.eof Then
        Set fldACC = AdoRes.Fields(0)
        Set fldDen = AdoRes.Fields(1)
        
        While Not AdoRes.eof
            Id = fldACC.Value
            Den = NullToStr(fldDen.Value)
            oAcciones.Add Id, Den
            AdoRes.MoveNext
        Wend
        AdoRes.Close
        Set fldACC = Nothing
        Set fldDen = Nothing
        Set mvarAcciones = oAcciones
    End If
    
    Set AdoRes = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CPerfil", "CargarAcciones", ERR, Erl)
        Exit Sub
    End If
End Sub


Private Sub Class_Terminate()

    Set mvarAcciones = Nothing
    Set mvarConexion = Nothing
    Set mvarConexionServer = Nothing

End Sub

Private Function IBaseDatos_AnyadirABaseDatos() As TipoErrorSummit


Dim oAccion As CAccion

Dim bExiste As Boolean
Dim iRespuesta As Integer
Dim sConsulta As String
Dim gNoerror As TipoErrorSummit
Dim bTransaccionEnCurso As Boolean
Dim oPres5Niv0  As cPresConcep5Nivel0

'********* Precondicion *******************
If mvarConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CPerfil.ExpandirAcciones", "No se ha establecido la conexion"
    Exit Function
End If
'******************************************
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
gNoerror.NumError = TESnoerror

    
If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
        mvarConexion.ADOCon.Execute "BEGIN TRANSACTION"
        bTransaccionEnCurso = True
        
        sConsulta = "INSERT INTO PERF (COD,DEN) VALUES ('" & DblQuote(mvarCod) & "','" & DblQuote(mvarDen) & "')"
        mvarConexion.ADOCon.Execute sConsulta
        If mvarConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
        
        For Each oAccion In mvarAcciones
            mvarConexion.ADOCon.Execute "INSERT INTO PERF_ACC (PERF,ACC) VALUES ('" & DblQuote(mvarCod) & "'," & oAccion.Id & ")"
            If mvarConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
        Next
        
        If Not mvarPresup5Niv0 Is Nothing Then
            For Each oPres5Niv0 In mvarPresup5Niv0
                sConsulta = "INSERT INTO PERF_PRES5 (PERF,PRES5) VALUES ('" & DblQuote(mvarCod) & "','" & DblQuote(oPres5Niv0.Cod) & "')"
                mvarConexion.ADOCon.Execute sConsulta
                If mvarConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
            Next
        End If
        
        mvarConexion.ADOCon.Execute "COMMIT TRANSACTION"
        bTransaccionEnCurso = False
        IBaseDatos_AnyadirABaseDatos = gNoerror
    
        Exit Function
    
Error_Cls:

    IBaseDatos_AnyadirABaseDatos = basErrores.TratarError(mvarConexion.ADOCon.Errors)
    
    If bTransaccionEnCurso Then
        mvarConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
        bTransaccionEnCurso = False
    End If
    
    'Resume Salir
    
'Salir:
    

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        'Call g_oErrores.TratarError("Class Module", "CPerfil", "IBaseDatos_AnyadirABaseDatos", ERR, Erl)
        GoTo Error_Cls
        Exit Function
    End If

End Function

Private Function IBaseDatos_CambiarCodigo(CodigoNuevo As Variant) As TipoErrorSummit

    ''' * Objetivo: Cambiar de codigo la moneda
    ''' * Recibe: Codigo nuevo
    ''' * Devuelve: TipoErrorSummit con el error si lo hay
    
    'ado  Dim q As rdoQuery, sql As String
    Dim cm As adodb.Command
    Dim par As adodb.Parameter
    Dim TESError As TipoErrorSummit
    
    ''' Precondicion
    
    If mvarConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CPerfil.CambiarCodigo", "No se ha establecido la conexion"
        Exit Function
    End If
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    TESError.NumError = TESnoerror
    
        
If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
    
    ''' Preparar la SP y sus parametros
    Set cm = New adodb.Command
    Set cm.ActiveConnection = mvarConexion.ADOCon
    
    'ado  sql = "{ CALL PERF_COD (?, ?) }"
     
    'ado  Set q = mvarConexion.rdoSummitCon.CreateQuery("CambiarCodigo", sql)
        
    'ado  q.rdoParameters(0) = Me.Cod
    'ado  q.rdoParameters(1) = CodigoNuevo
    
    Set par = cm.CreateParameter("PAR1", adVarChar, adParamInput, 50, Me.Cod)
    cm.Parameters.Append par
    Set par = cm.CreateParameter("PAR2", adVarChar, adParamInput, 50, CodigoNuevo)
    cm.Parameters.Append par
    
    cm.CommandType = adCmdStoredProc
    cm.CommandText = "PERF_COD"
    
    ''' Ejecutar la SP
    
    cm.Execute
    
    Set cm = Nothing
    
    mvarCod = CodigoNuevo
        
    IBaseDatos_CambiarCodigo = TESError
    
    Exit Function
    
Error_Cls:
    
     If Not cm Is Nothing Then
         Set cm = Nothing
     End If
    
    IBaseDatos_CambiarCodigo = basErrores.TratarError(mvarConexion.ADOCon.Errors)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        'Call g_oErrores.TratarError("Class Module", "CPerfil", "IBaseDatos_CambiarCodigo", ERR, Erl)
        GoTo Error_Cls
        Exit Function
    End If

End Function

Private Sub IBaseDatos_CancelarEdicion()

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If Not mvarRecordset Is Nothing Then
        mvarRecordset.Close
        Set mvarRecordset = Nothing
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CPerfil", "IBaseDatos_CancelarEdicion", ERR, Erl)
        Exit Sub
    End If
End Sub

Private Function IBaseDatos_ComprobarExistenciaEnBaseDatos() As Boolean

End Function

Private Function IBaseDatos_EliminarDeBaseDatos() As TipoErrorSummit
Dim TESError As TipoErrorSummit
Dim bTransaccionEnCurso As Boolean

'******************* Precondicion *******************
If mvarConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CPerfil.EliminarDeBaseDatos", "No se ha establecido la conexion"
    Exit Function
End If
'*****************************************************
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
TESError.NumError = TESnoerror

If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
        
        mvarConexion.ADOCon.Execute "BEGIN TRANSACTION"
        bTransaccionEnCurso = True
        mvarConexion.ADOCon.Execute "DELETE FROM PERF_ACC WHERE PERF='" & DblQuote(mvarCod) & "'"
        If mvarConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
        mvarConexion.ADOCon.Execute "DELETE FROM PERF_LIS WHERE PERF='" & DblQuote(mvarCod) & "'"
        If mvarConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
        mvarConexion.ADOCon.Execute "DELETE FROM PERF WHERE COD='" & DblQuote(mvarCod) & "'"
        If mvarConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
        mvarConexion.ADOCon.Execute "COMMIT TRANSACTION"
        bTransaccionEnCurso = False
        
        IBaseDatos_EliminarDeBaseDatos = TESError

Exit Function

Error_Cls:

    IBaseDatos_EliminarDeBaseDatos = basErrores.TratarError(mvarConexion.ADOCon.Errors)
        
    If bTransaccionEnCurso Then
        mvarConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
        bTransaccionEnCurso = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CPerfil", "IBaseDatos_EliminarDeBaseDatos", ERR, Erl)
        GoTo Error_Cls
        Exit Function
    End If
       
End Function

''' <summary>Esta funci�n finaliza la edici�n del resulset eliminando</summary>
''' <returns>TipoErrorSummit con el error si lo hay</returns>
''' <remarks>Llamada desde: FSGClient
''' Tiempo m�ximo: 1 sec </remarks>
''' <revision>JVS 26/12/2011</revision>
Private Function IBaseDatos_FinalizarEdicionEliminando() As TipoErrorSummit

Dim TESError As TipoErrorSummit
Dim bTransaccionEnCurso As Boolean

Dim AdoRes As adodb.Recordset
    
'********* Precondicion *******************
If mvarConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CPerfil.FinalizarEdicionModificando", "No se ha establecido la conexion"
    Exit Function

End If
'******************************************
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:

        Set AdoRes = New adodb.Recordset
        AdoRes.Open "SELECT 1 FROM USU WITH(NOLOCK) WHERE PERF='" & DblQuote(mvarCod) & "' AND BAJA=0" _
        , mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
        
        If Not AdoRes.eof Then
            TESError.NumError = TESValidarPerfilEliminar
            IBaseDatos_FinalizarEdicionEliminando = TESError
            Exit Function
        End If
        
        mvarConexion.ADOCon.Execute "BEGIN TRANSACTION"
        bTransaccionEnCurso = True
        
        mvarConexion.ADOCon.Execute "UPDATE USU SET PERF=NULL WHERE PERF='" & DblQuote(mvarCod) & "' AND BAJA=1"
        If mvarConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
        mvarConexion.ADOCon.Execute "DELETE FROM PERF_ACC WHERE PERF='" & DblQuote(mvarCod) & "'"
        If mvarConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
        mvarConexion.ADOCon.Execute "DELETE FROM PERF_LIS WHERE PERF='" & DblQuote(mvarCod) & "'"
        If mvarConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
        mvarConexion.ADOCon.Execute "DELETE FROM PERF_PRES5 WHERE PERF='" & DblQuote(mvarCod) & "'"
        If mvarConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
        mvarConexion.ADOCon.Execute "DELETE FROM PERF WHERE COD='" & DblQuote(mvarCod) & "'"
        If mvarConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
        mvarConexion.ADOCon.Execute "COMMIT TRANSACTION"
        bTransaccionEnCurso = False
        
        mvarRecordset.Close
        Set mvarRecordset = Nothing
        IBaseDatos_FinalizarEdicionEliminando = TESError
    
        Exit Function
    
Error_Cls:
    If mvarConexion.ADOCon.Errors.Count > 0 Then
        IBaseDatos_FinalizarEdicionEliminando = basErrores.TratarError(mvarConexion.ADOCon.Errors)
    Else
        IBaseDatos_FinalizarEdicionEliminando = basErrores.TratarError(mvarConexionServer.ADOSummitCon.Errors)
    End If
    
    If bTransaccionEnCurso Then
        mvarConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
        bTransaccionEnCurso = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        'Call g_oErrores.TratarError("Class Module", "CPerfil", "IBaseDatos_FinalizarEdicionEliminando", ERR, Erl)
        GoTo Error_Cls
        Exit Function
    End If
    
End Function

Private Function IBaseDatos_FinalizarEdicionModificando() As TipoErrorSummit
Dim oAccion As CAccion
Dim sCodigoAntiguo As String
Dim sConsulta As String
Dim TESError As TipoErrorSummit
Dim bTransaccionEnCurso As Boolean
Dim oPres5Niv0 As cPresConcep5Nivel0
        
'********* Precondicion *******************
If mvarConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CPerfil.FinalizarEdicionModificando", "No se ha establecido la conexion"
    Exit Function
End If
'******************************************
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
    
    mvarConexion.ADOCon.Execute "BEGIN TRANSACTION"
    bTransaccionEnCurso = True
        
    'ADO  mvarRecordset.Edit
    mvarRecordset("DEN") = mvarDen
    
    mvarConexion.ADOCon.Execute "DELETE FROM PERF_ACC WHERE PERF='" & DblQuote(mvarCod) & "'"
    If mvarConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    For Each oAccion In Acciones
        sConsulta = "INSERT INTO PERF_ACC (PERF,ACC) VALUES ('" & DblQuote(mvarCod) & "'," & oAccion.Id & ")"
        mvarConexion.ADOCon.Execute sConsulta
        If mvarConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    Next
    
    If gParametrosGenerales.gsAccesoFSSM <> TipoAccesoFSSM.SinAcceso Then
        mvarConexion.ADOCon.Execute "DELETE FROM PERF_PRES5 WHERE PERF='" & DblQuote(mvarCod) & "'"
        If mvarConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
        For Each oPres5Niv0 In mvarPresup5Niv0
            sConsulta = "INSERT INTO PERF_PRES5 (PERF,PRES5) VALUES ('" & DblQuote(mvarCod) & "','" & DblQuote(oPres5Niv0.Cod) & "')"
            mvarConexion.ADOCon.Execute sConsulta
            If mvarConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
        Next
    End If
    mvarRecordset.Update
    mvarRecordset.Close
    Set mvarRecordset = Nothing
    mvarConexion.ADOCon.Execute "COMMIT TRANSACTION"
    bTransaccionEnCurso = False

    TESError.NumError = TESnoerror
    IBaseDatos_FinalizarEdicionModificando = TESError
    
    Exit Function
    

Error_Cls:
 
     If mvarConexion.ADOCon.Errors.Count > 0 Then
        IBaseDatos_FinalizarEdicionModificando = basErrores.TratarError(mvarConexion.ADOCon.Errors)
    Else
        IBaseDatos_FinalizarEdicionModificando = basErrores.TratarError(mvarConexionServer.ADOSummitCon.Errors)
    End If
       
    If bTransaccionEnCurso Then
        mvarConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
        bTransaccionEnCurso = False
    End If
    
    Resume Salir:
    
Salir:

    On Error Resume Next
    If mvarRecordset.EditMode > 0 Then
        mvarRecordset.CancelUpdate
        
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        'Call g_oErrores.TratarError("Class Module", "CPerfil", "IBaseDatos_FinalizarEdicionModificando", ERR, Erl)
        GoTo Error_Cls
        Exit Function
    End If
    
End Function

''' <summary>Esta funci�n inicia la edici�n del resulset</summary>
'''<param optional name="Bloquear">Si se bloquea o no</param>
'''<param optional name="UsuarioBloqueo">Usuario del bloqueo</param>
''' <returns>TipoErrorSummit con el error si lo hay</returns>
''' <remarks>Llamada desde: FSGClient
''' Tiempo m�ximo: 1 sec </remarks>
''' <revision>JVS 26/12/2011</revision>
Private Function IBaseDatos_IniciarEdicion(Optional ByVal Bloquear As Boolean, Optional ByVal UsuarioBloqueo As String) As TipoErrorSummit
    Dim bTransaccionEnCurso As Boolean
    'ado  Dim rdores As rdoResultset
    Dim TESError As TipoErrorSummit
    Dim sConsulta As String
        
    '******************* Precondicion *******************
    If mvarConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CUsuario.ExpandirAcciones", "No se ha establecido la conexion"
        Exit Function
    End If
    '*****************************************************
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
    ConectarDeUseServer 'Si no hay conexion de use server la abre
        
    'CALIDAD: Sin WITH (NOLOCK) porque se rellena el recordset mvarRecordset que posteriormente se utiliza para las actualizaciones
    'en otros m�todos de la clase. Si se hace la lectura con WITH (NOLOCK) no se permite la llamada al m�todo Update del recordset.
    sConsulta = "SELECT COD,DEN,FECACT FROM PERF WHERE COD='" & DblQuote(mvarCod) & "'"
    Set mvarRecordset = New adodb.Recordset
    mvarRecordset.Open sConsulta, mvarConexionServer.ADOSummitCon, adOpenKeyset, adLockOptimistic
    
    If mvarRecordset.eof Then
        mvarRecordset.Close
        Set mvarRecordset = Nothing
        TESError.NumError = TESDatoEliminado
        TESError.Arg1 = 72 ''"el perfil"
    Else
        mvarDen = mvarRecordset("DEN")
        TESError.NumError = TESnoerror
    End If

    IBaseDatos_IniciarEdicion = TESError
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CPerfil", "IBaseDatos_IniciarEdicion", ERR, Erl)
        Exit Function
    End If
End Function


''' <summary>Devuelve las UON asociadas al perfil</summary>
''' <returns>recordset con los datos de las UON asociadas</returns>
''' <remarks>Llamada desde: CProceso.CargarTodosLosProcesosDesde</remarks>
''' <revision>LTG 03/01/2013</revision>

Public Function DevolverUONsPerfil() As adodb.Recordset
    Dim sConsulta As String
    Dim oComm As New adodb.Command
    Dim oParam As New adodb.Parameter
        
    '********* Precondicion *******************
    If mvarConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CPerfil.DevolverUONsPerfil", "No se ha establecido la conexion"
        Exit Function
    End If
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
        
    If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls
    
    sConsulta = "SELECT UON1,UON2,UON3 FROM PERF_UON WITH (NOLOCK) WHERE PERF=?"
    
    Set oComm = New adodb.Command
    With oComm
        .ActiveConnection = mvarConexion.ADOCon
        .CommandType = adCmdText
        .CommandText = sConsulta
        
        Set oParam = .CreateParameter("PERF", adInteger, adParamInput, , mlID)
        .Parameters.Append oParam
        
        Set DevolverUONsPerfil = .Execute
    End With

Salir:
    Set oComm = Nothing
    Set oParam = Nothing
    Exit Function
Error_Cls:
    Resume Salir
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CPerfil", "DevolverUONsPerfil", ERR, Erl)
        GoTo Error_Cls
        Exit Function
    End If
End Function
