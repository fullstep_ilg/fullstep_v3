VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CHitosFact"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum

Private mCol As Collection
Private mvarConexion As CConexion

Public Sub CopiarHitos(ByVal oHitosFact As CHitosFact, ByVal Importe As Double, ByVal ImporteMonProce As Double, ByVal bSeguimiento As Boolean)
    Dim oHito As CHitoFact
    Dim Valor As Double
    Dim ValorMonProce As Double
    Dim i As Integer
    
    If bSeguimiento Then
        'Si los hitos ya existen en BD hay que marcarlos para eliminaci�n
        For i = Me.Count To 1 Step -1
            Set oHito = Me.Item(i)
            If oHito.Id < 0 Then
                Me.Remove i
            Else
                oHito.AccionEdicion = TipoAccionEdicion.Eliminacion
            End If
        Next
    Else
        Me.RemoveAll True
    End If
    
    i = 0
    For Each oHito In oHitosFact
        Select Case oHito.TipoPlan
            Case 1, 4
                Valor = Round(Importe * oHito.Porcen / 100, 2)
                ValorMonProce = Round(ImporteMonProce * oHito.Porcen / 100, 2)
            Case Else
                Valor = 0
                ValorMonProce = 0
        End Select
        Me.Add i, oHito.Fecha, oHito.Den, oHito.Clase, oHito.TipoPlan, oHito.Porcen, Valor, TipoAccionEdicion.Alta, ValorMonProce
        i = i - 1
    Next
End Sub

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
''' ! Pendiente de revision, objetivo desconocido
       Set NewEnum = mCol.[_NewEnum]
End Property
''' <summary>
''' Establecer el valor de mvarConexion
''' </summary>
''' <param name="con">Conexion</param>
''' <remarks>Llamada desde: CRaiz/generar_CHitosFact; Tiempo m�ximo: 0</remarks>
Friend Property Set Conexion(ByVal con As CConexion)
Set mvarConexion = con
End Property

''' <summary>
''' Obtener el valor de mvarConexion
''' </summary>
''' <remarks>Llamada desde: </remarks>
Friend Property Get Conexion() As CConexion
    Set Conexion = mvarConexion
End Property

''' <summary>
''' Recuperar un Impuesto de la coleccion
''' </summary>
''' <param name="vntIndexKey">Indice del hito a recuperar</param>
''' <returns>Unidad correspondiente</returns>
''' <remarks>Llamada desde:  Tiempo m�ximo: 0</remarks>
Public Property Get Item(vntIndexKey As Variant) As CHitoFact
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Public Function ValidarFactParcial(ByVal Importe As Double, Optional bMonproce As Boolean = False) As Boolean
Dim dblImporteFactParcial As Double
dblImporteFactParcial = ImporteHitos("ZINV", bMonproce)
ValidarFactParcial = (dblImporteFactParcial = Importe)

End Function

Public Function ValidarAnticipos(Optional bMonproce As Boolean = False) As Boolean
Dim dblImporteFactParcial As Double
Dim dblImporteAnticipos As Double
dblImporteFactParcial = ImporteHitos("ZINV", bMonproce)
dblImporteAnticipos = ImporteHitos("ZANT", bMonproce)

ValidarAnticipos = (dblImporteFactParcial >= dblImporteAnticipos)
End Function

Private Function ImporteHitos(Optional sClase As String = "", Optional bMonproce As Boolean = False) As Double
Dim Importe As Double
Dim i As Integer
For i = 1 To Me.Count
    If (Item(i).Clase = sClase Or sClase = "") And Item(i).AccionEdicion <> TipoAccionEdicion.Eliminacion Then
        If bMonproce Then
            Importe = Importe + Item(i).ValorMonProce
        Else
            Importe = Importe + Item(i).Valor
        End If
    End If
Next i
ImporteHitos = Importe
End Function
''' <summary>
''' creates the collection when this class is created
''' </summary>
''' <remarks>Llamada desde: sistema ; Tiempo m�ximo: 0</remarks>
Private Sub Class_Initialize()
    Set mCol = New Collection
End Sub


''' <summary>
''' destroys collection when this class is terminated
''' </summary>
''' <remarks>Llamada desde: sistema ; Tiempo m�ximo: 0</remarks>
Private Sub Class_Terminate()
    Set mCol = Nothing
    Set mvarConexion = Nothing
End Sub


Public Function Existe(ByVal Id As Variant) As Boolean
    Dim o As CHitoFact
    On Error GoTo ERR
    '
    Set o = mCol(CStr(Id))
    
    Existe = True
    Exit Function
ERR:
    Existe = False
End Function

''' <summary>
''' Eliminar un hito de la coleccion
''' </summary>
''' <param name="vntIndexKey">Indice del hito a eliminar</param>
Public Sub Remove(vntIndexKey As Variant)
    mCol.Remove vntIndexKey
End Sub


''' <summary>
''' Eliminar todos los hitos de la coleccion
''' </summary>
''' <param name="vntIndexKey">Indice del hito a eliminar</param>
Public Sub RemoveAll(Optional bTodo As Boolean = False)
Dim col As CHitoFact
    For Each col In Me
        If bTodo Or col.Id < 1 Then
            Me.Remove 1
        Else
            col.AccionEdicion = Eliminacion
        End If
    Next
End Sub

''' <summary>
''' Devolver Numero de elementos de la coleccion
''' </summary>
''' <returns>Numero de elementos de la coleccion</returns>
Public Property Get Count() As Long
    If mCol Is Nothing Then
        Count = 0
    Else
        Count = mCol.Count
    End If
End Property

Public Function AddHito(ByRef oHito As CHitoFact, Optional ByVal varIndice As Variant) As CHitoFact
    Set AddHito = Add(oHito.Id, oHito.Fecha, oHito.Den, oHito.Clase, oHito.TipoPlan, oHito.Porcen, oHito.Valor, oHito.AccionEdicion, oHito.ValorMonProce, varIndice)
End Function

''' <summary>A�ade un nuevo item a la colecci�n</summary>
''' <param name=""></param>
''' <returns>Objeto de tipo CHitoFact a�adido</returns>
''' <remarks>Llamada desde:  ; Tiempo m�ximo: 0</remarks>
Public Function Add(ByVal Id As Long, ByVal Fecha As Date, ByVal Den As String, ByVal Clase As String, ByVal Tipo As String, ByVal Porcen As Variant, _
                    ByVal Valor As Double, ByVal AccionEdicion As TipoAccionEdicion, Optional ByVal ValorMonProce As Double = 0, Optional ByVal varIndice As Variant) As CHitoFact
    Dim objnewmember As CHitoFact
    
On Error GoTo Error
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New CHitoFact
    Set objnewmember.Conexion = mvarConexion
    objnewmember.Id = Id
    objnewmember.Fecha = Fecha
    objnewmember.Den = Den
    objnewmember.Clase = Clase
    objnewmember.TipoPlan = Tipo
    If Porcen <> 0 Then
        objnewmember.Porcen = Porcen
    Else
        objnewmember.Porcen = Null
    End If
    objnewmember.Valor = Valor
    objnewmember.ValorMonProce = ValorMonProce
    objnewmember.AccionEdicion = AccionEdicion
    
    If Not IsMissing(varIndice) And Not IsNull(varIndice) Then
        mCol.Add objnewmember, CStr(varIndice)
    Else
        mCol.Add objnewmember, CStr(Id)
    End If
    
    Set Add = objnewmember
    
    Set objnewmember = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CHitosFact", "Add", ERR, Erl)
      Exit Function
   End If
End Function

Public Function NumHitosNoEliminados() As Integer
    Dim i As Integer
    Dim oHito As CHitoFact
    
    For Each oHito In Me
        If oHito.AccionEdicion <> TipoAccionEdicion.Eliminacion Then i = i + 1
    Next
    Set oHito = Nothing
    
    NumHitosNoEliminados = i
End Function

