VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CNotificadoInt"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements IBaseDatos

''' Variables privadas con la informacion de un notificado

Private m_lID As Long
Private m_vNombre As Variant
Private m_vEmail As Variant
Private m_vCodERP As Variant

''' Conexion

Private m_oConexion As CConexion


''' Control de errores

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

''' Recordset para el Pago a editar
Private m_adores As adodb.Recordset

''' Indice de la Pago en la coleccion

Private m_lIndice As Long


Public Property Let Nombre(ByVal vData As Variant)

    ''' * Objetivo: Dar valor al destino de integración de la entidad
    ''' * Recibe: el valor
    ''' * Devuelve: Nada

    m_vNombre = vData
    
End Property
Public Property Get Nombre() As Variant

    ''' * Objetivo: Devolver el destino de integración de la entidad
    ''' * Recibe: Nada
    ''' * Devuelve: El destino de integración de la entidad
    
    Nombre = m_vNombre
    
End Property

Public Property Let Email(ByVal vData As Variant)

    ''' * Objetivo: Dar valor al sentido de integración de la entidad
    ''' * Recibe: el valor
    ''' * Devuelve: Nada

    m_vEmail = vData
    
End Property
Public Property Get Email() As Variant

    ''' * Objetivo: Devolver el sentido de integración de la entidad
    ''' * Recibe: Nada
    ''' * Devuelve: El sentido de integración de la entidad
    
    Email = m_vEmail
    
End Property
Public Property Let CodERP(ByVal vData As Variant)

    ''' * Objetivo: Dar valor al sentido de integración de la entidad
    ''' * Recibe: el valor
    ''' * Devuelve: Nada

    m_vCodERP = vData
    
End Property
Public Property Get CodERP() As Variant

    ''' * Objetivo: Devolver el sentido de integración de la entidad
    ''' * Recibe: Nada
    ''' * Devuelve: El sentido de integración de la entidad
    
    CodERP = m_vCodERP
    
End Property
Public Property Get Indice() As Long

    ''' * Objetivo: Devolver el indice de la Pago en la coleccion
    ''' * Recibe: Nada
    ''' * Devuelve: Indice de la Pago en la coleccion

    Indice = m_lIndice
    
End Property
Public Property Let Indice(ByVal iInd As Long)

    ''' * Objetivo: Dar valor al indice de la Pago en la coleccion
    ''' * Recibe: Indice de la Pago en la coleccion
    ''' * Devuelve: Nada

    m_lIndice = iInd
    
End Property
Friend Property Set Conexion(ByVal vData As CConexion)

    ''' * Objetivo: Dar valor a la conexion de la Pago
    ''' * Recibe: Conexion
    ''' * Devuelve: Nada

    Set m_oConexion = vData
    
End Property
Friend Property Get Conexion() As CConexion

    ''' * Objetivo: Devolver la conexion de la Pago
    ''' * Recibe: Nada
    ''' * Devuelve: Conexion

    Set Conexion = m_oConexion
    
End Property






Public Property Let Id(ByVal vData As Long)

    ''' * Objetivo: Dar valor a la variable privada m_iTabla
    ''' * Recibe: El identificador de la entidad
    ''' * Devuelve: Nada

    m_lID = vData
    
End Property

Public Property Get Id() As Long

    ''' * Objetivo: Devolver la variable privada Tabla
    ''' * Recibe: Nada
    ''' * Devuelve: Identificador de la entidad

    Id = m_lID
    
End Property

Private Sub Class_Terminate()

    ''' * Objetivo: Limpiar la memoria
    
    Set m_oConexion = Nothing
    
End Sub
Private Function IBaseDatos_AnyadirABaseDatos() As TipoErrorSummit

    Dim TESError As TipoErrorSummit
    Dim sConsulta As String
    Dim AdoRes As adodb.Recordset


    TESError.NumError = TESnoerror

    ''' Precondicion
    
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CEntidadInt.FinalizarEdicionModificando", "No se ha establecido la conexion"
    End If
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:

    sConsulta = "INSERT INTO EMAIL_INTEGRACION (NOMBRE,EMAIL,ERP) VALUES ("
    sConsulta = sConsulta & StrToSQLNULL(m_vNombre) & "," & StrToSQLNULL(m_vEmail) & "," & StrToSQLNULL(m_vCodERP) & ")"
    
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then
        GoTo ERROR
    End If
 
     sConsulta = "SELECT MAX(ID) FROM EMAIL_INTEGRACION"
    Set AdoRes = New adodb.Recordset
    AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    m_lID = AdoRes(0).Value
    AdoRes.Close
    Set AdoRes = Nothing

    
    
    IBaseDatos_AnyadirABaseDatos = TESError
    
    Exit Function
    
Error_Cls:
        
    IBaseDatos_AnyadirABaseDatos = basErrores.TratarError(m_oConexion.ADOCon.Errors)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Módulo de clase", "CNotificadoInt", "IBaseDatos_AnyadirABaseDatos", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If
           
End Function
Private Function IBaseDatos_CambiarCodigo(CodigoNuevo As Variant) As TipoErrorSummit
    
End Function
Private Sub IBaseDatos_CancelarEdicion()

    ''' * Objetivo: Cancelar la edicion, cerrando el Resultset
    
    If Not m_adores Is Nothing Then
        m_adores.Close
        Set m_adores = Nothing
    End If

End Sub
Private Function IBaseDatos_ComprobarExistenciaEnBaseDatos() As Boolean
    
End Function
Private Function IBaseDatos_EliminarDeBaseDatos() As TipoErrorSummit

    ''' * Objetivo: Eliminar la moneda de la tabla
    ''' * Recibe: Nada
    ''' * Devuelve: TipoErrorSummit con el error si lo hay

    Dim TESError As TipoErrorSummit
    Dim sConsulta As String
   

    TESError.NumError = TESnoerror
    
    ''' Precondicion
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CNotificadoInt.EliminarDeBaseDatos", "No se ha establecido la conexion"
    End If
    
    If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
        
   
    m_oConexion.ADOCon.Execute "DELETE FROM EMAIL_INTEGRACION WHERE ID=" & m_lID
    If m_oConexion.ADOCon.Errors.Count > 0 Then
        GoTo ERROR
    End If

    IBaseDatos_EliminarDeBaseDatos = TESError
    
    Exit Function
        
Error_Cls:
        
    IBaseDatos_EliminarDeBaseDatos = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Módulo de clase", "CNotificadoInt", "IBaseDatos_EliminarDeBaseDatos", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If
    
End Function

Private Function IBaseDatos_FinalizarEdicionEliminando() As TipoErrorSummit

End Function
Private Function IBaseDatos_FinalizarEdicionModificando() As TipoErrorSummit

    Dim TESError As TipoErrorSummit
    Dim sConsulta As String



    TESError.NumError = TESnoerror

    ''' Precondicion
    
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CEntidadInt.FinalizarEdicionModificando", "No se ha establecido la conexion"
    End If
    
    If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:

    sConsulta = "UPDATE EMAIL_INTEGRACION SET NOMBRE=" & StrToSQLNULL(m_vNombre) & ",EMAIL=" & StrToSQLNULL(m_vEmail)
    sConsulta = sConsulta & " WHERE ID=" & m_lID
    
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then
        GoTo ERROR
    End If
 
    
    IBaseDatos_FinalizarEdicionModificando = TESError
    
    Exit Function
    
Error_Cls:
        
    IBaseDatos_FinalizarEdicionModificando = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Módulo de clase", "CNotificadoInt", "IBaseDatos_FinalizarEdicionModificando", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If
   
End Function

Private Function IBaseDatos_IniciarEdicion(Optional ByVal Bloquear As Boolean, Optional ByVal UsuarioBloqueo As String) As TipoErrorSummit

    ''' * Objetivo: Abrir la edicion
    ''' * Recibe: Nada
    ''' * Devuelve: TipoErrorSummit con el error si lo hay

    Dim TESError As TipoErrorSummit
    Dim sConsulta As String
    

    TESError.NumError = TESnoerror
    
    ''' Precondicion
    
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CNotificadoInt.IniciarEdicion", "No se ha establecido la conexion"
    End If
    
    If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
     ''' Abrir Recordset
    sConsulta = "SELECT ID,EMAIL,NOMBRE,FORMATO,ERP FROM EMAIL_INTEGRACION WITH (NOLOCK) WHERE ID=" & m_lID
    Set m_adores = New adodb.Recordset
    m_adores.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly

    ''' Si no esta la Pago, alguien la ha eliminado
    
    If m_adores.eof Then
        m_adores.Close
        Set m_adores = Nothing
        TESError.NumError = TESDatoEliminado
        TESError.Arg1 = 16  '"Pago"
        IBaseDatos_IniciarEdicion = TESError
        Exit Function
    End If
    
    
    m_vNombre = m_adores("NOMBRE").Value
    m_vEmail = m_adores("EMAIL").Value
       
    
    m_adores.Close
    Set m_adores = Nothing
        
    IBaseDatos_IniciarEdicion = TESError
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Módulo de clase", "CNotificadoInt", "IBaseDatos_IniciarEdicion", ERR, Erl)
      Exit Function
   End If

End Function
Private Function IBaseDatos_ModificarEnBaseDatos() As Boolean

    ''' ! No usado por ahora
    
End Function







