VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CFacDirEnvios"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Const ConexionNoEstablecida = 613

Private mCol As Collection
Private m_oConexion As CConexion

Private Sub Class_Initialize()
    Set mCol = New Collection
End Sub

Private Sub Class_Terminate()
    On Error Resume Next
    
    Set mCol = Nothing
    Set m_oConexion = Nothing
End Sub

Friend Property Set Conexion(ByVal oCon As CConexion)
    Set m_oConexion = oCon
End Property

Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
     Set NewEnum = mCol.[_NewEnum]
End Property

''' <summary>Carga todas las direcciones de env�o existentes</summary>
''' <param name="sCaracteresInicialesDir">Caracteres iniciales de las dir. a cargar</param>
''' <param name="bCoincidenciaTotal">Coincidencia total de la dir. con el par�metro CaracteresInicialesDir</param>
''' <param name="bOrdenadosPorDir">Ordenar las dir. por DIR</param>
''' <param name="bOrdenadosPorPoblacion">Ordenar las dir. por POB</param>
''' <param name="bOrdenadosPorProvinciaDen">Ordenar las dir. por la denominaci�n de la provincia</param>
''' <param name="bOrdenadosPorPaisDen">Ordenar las dir. por la denominaci�n del pa�s</param>
''' <param name="bUsarIndice"></param>
''' <returns>Estructura de tipo TipoErrorSummit que contiene el error si lo ha habido</returns>
''' <remarks>Llamada desde FSGSClient.frmESTRORGDetalle</remarks>
''' <revision>LTG 04/01/2012<revision>

Public Function CargarTodosLasDirecciones(Optional ByVal sCaracteresInicialesDir As String, _
                                          Optional ByVal bCoincidenciaTotal As Boolean = False, _
                                          Optional ByVal bOrdenadosPorDir As Boolean = False, _
                                          Optional ByVal bOrdenadosPorPoblacion As Boolean = False, _
                                          Optional ByVal bOrdenadosPorProvinciaDen As Boolean = False, _
                                          Optional ByVal bOrdenadosPorPaisDen As Boolean = False, _
                                          Optional ByVal bUsarIndice As Boolean = False) As TipoErrorSummit
    Dim sConsulta As String
    Dim sOrden As String
    Dim rsDir As ADODB.Recordset
    Dim lIndice As Long
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls
    sConsulta = "SELECT FDE.ID, FDE.DIR, FDE.POB, FDE.CP, FDE.PROVI, PROVI_DEN.DEN AS PROVIDEN, FDE.PAI, PAI_DEN.DEN AS PAIDEN, FDE.FECACT " & _
                "FROM FAC_DIR_ENVIO FDE WITH (NOLOCK) " & _
                "LEFT JOIN PAI WITH (NOLOCK) ON PAI.COD = FDE.PAI " & _
                "LEFT JOIN PAI_DEN WITH (NOLOCK) ON PAI_DEN.PAI=FDE.PAI AND PAI_DEN.IDIOMA='" & gParametrosInstalacion.gIdioma & "' " & _
                "LEFT JOIN PROVI WITH (NOLOCK) ON PROVI.PAI = FDE.PAI AND PROVI.COD = FDE.PROVI " & _
                "LEFT JOIN PROVI_DEN WITH (NOLOCK) ON PROVI_DEN.PAI=FDE.PAI AND PROVI_DEN.PROVI=FDE.PROVI AND PROVI_DEN.IDIOMA='" & gParametrosInstalacion.gIdioma & "'"
            
    If sCaracteresInicialesDir <> vbNullString Then
        If bCoincidenciaTotal Then
            sConsulta = sConsulta & "WHERE FDE.DIR ='" & DblQuote(sCaracteresInicialesDir) & "'"
        Else
            sConsulta = sConsulta & "WHERE FDE.DIR LIKE '" & DblQuote(sCaracteresInicialesDir) & "%'"
        End If
    End If
    
    If bOrdenadosPorPaisDen Then sOrden = "PAIDEN"
    If bOrdenadosPorProvinciaDen Then sOrden = sOrden & ",PROVIDEN"
    If bOrdenadosPorPoblacion Then sOrden = sOrden & ",POB"
    If bOrdenadosPorDir Then sOrden = sOrden & ",DIR"
    
    If sOrden <> vbNullString Then
        If Left(sOrden, 1) = "," Then sOrden = Right(sOrden, Len(sOrden) - 1)
        sConsulta = sConsulta & " ORDER BY " & sOrden
    End If
    
    Set rsDir = New ADODB.Recordset
    rsDir.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    
    Set mCol = Nothing
    Set mCol = New Collection
        
    If Not rsDir.eof Then
        lIndice = 0
                
        Do While Not rsDir.eof
            If bUsarIndice Then
                Me.Add rsDir("ID"), rsDir("DIR"), NullToStr(rsDir("CP")), NullToStr(rsDir("POB")), _
                       NullToStr(rsDir("PROVI")), NullToStr(rsDir("PROVIDEN")), NullToStr(rsDir("PAI")), _
                       NullToStr(rsDir("PAIDEN")), lIndice, rsDir("FECACT")
                       
                lIndice = lIndice + 1
            Else
                Me.Add rsDir("ID"), rsDir("DIR"), NullToStr(rsDir("CP")), NullToStr(rsDir("POB")), _
                       NullToStr(rsDir("PROVI")), NullToStr(rsDir("PROVIDEN")), NullToStr(rsDir("PAI")), _
                       NullToStr(rsDir("PAIDEN")), , rsDir("FECACT")
            End If
            
            rsDir.MoveNext
        Loop
        
        rsDir.Close
    End If
    
    CargarTodosLasDirecciones.NumError = TESnoerror
    
Salir:
    Set rsDir = Nothing
    Exit Function
Error_Cls:
    If ERR.Number = vbObjectError + ConexionNoEstablecida Then
        CargarTodosLasDirecciones.NumError = ERR.Number
        CargarTodosLasDirecciones.Arg1 = "CFacDirEnvios.CargarTodosLasDirecciones"
        CargarTodosLasDirecciones.Arg2 = "No se ha establecido la conexi�n"
    Else
        If m_oConexion.ADOCon.Errors.Count > 0 Then
            CargarTodosLasDirecciones = basErrores.TratarError(m_oConexion.ADOCon.Errors)
        Else
            CargarTodosLasDirecciones.NumError = ERR.Number
            CargarTodosLasDirecciones.Arg1 = ERR.Number
            CargarTodosLasDirecciones.Arg2 = ERR.Description
        End If
    End If
    Resume Salir
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      'Call g_oErrores.TratarError("Class Module", "CFacDirEnvios", "CargarTodosLasDirecciones", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If
End Function

''' <summary>Carga un objeto CFacDirEnvio en la colecci�n</summary>
''' <param name="lID">ID de la dir.</param>
''' <param name="sDir">Direcci�n</param>
''' <param name="sCP">C�digo Postal</param>
''' <param name="sPob">Poblaci�n</param>
''' <param name="sProvi">C�digo de la Provincia</param>
''' <param name="sProviDen">Denominaci�n de la provincia</param>
''' <param name="sPai">C�digo del Pa�s</param>
''' <param name="sPaiDen">Denominaci�n de pa�s</param>
''' <param name="varIndice">Indice de la colecci�n</param>
''' <returns>Objeto de tipo CFacDirEnvio que se ha a�adido a la colecci�n</returns>
''' <remarks>Llamada desde FSGSClient.frmESTRORGDetalle</remarks>

Public Function Add(ByVal lId As Long, ByVal sDir As String, Optional ByVal sCP As String, Optional ByVal sPob As String, _
                    Optional ByVal sProvi As String, Optional ByVal sProviDen As String, Optional ByVal sPai As String, Optional ByVal sPaiDen As String, _
                    Optional ByVal varIndice As Variant, Optional ByVal vFecAct As Variant) As CFacDirEnvio
    Dim objnewmember As CFacDirEnvio
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New CFacDirEnvio
    
    objnewmember.Id = lId
    objnewmember.Direccion = sDir
    objnewmember.Poblacion = sPob
    objnewmember.CP = sCP
    objnewmember.ProviCod = sProvi
    objnewmember.ProviDen = sProviDen
    objnewmember.PaiCod = sPai
    objnewmember.PaiDen = sPaiDen
    objnewmember.PaiDen = sPaiDen
    If IsMissing(vFecAct) Then
         objnewmember.FecAct = Null
    Else
        objnewmember.FecAct = vFecAct
    End If
    
    Set objnewmember.Conexion = m_oConexion
    
    If Not IsMissing(varIndice) And Not IsNull(varIndice) Then
        objnewmember.Indice = varIndice
        mCol.Add objnewmember, CStr(varIndice)
    Else
        mCol.Add objnewmember, CStr(lId)
    End If
    
    Set Add = objnewmember
    
    Set objnewmember = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CFacDirEnvios", "Add", ERR, Erl)
      Exit Function
   End If
End Function

Public Property Get Count() As Long
    If mCol Is Nothing Then
        Count = 0
    Else
        Count = mCol.Count
    End If
End Property

Public Property Get Item(vntIndexKey As Variant) As CFacDirEnvio
    On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property
NoSeEncuentra:
    Set Item = Nothing
End Property

Public Sub Remove(vntIndexKey As Variant)
    mCol.Remove vntIndexKey
End Sub

