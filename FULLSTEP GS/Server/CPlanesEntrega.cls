VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CPlanesEntrega"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum
Private mCol As Collection
Private mvarConexion As CConexion

Private m_sAplicarUnPlanALIneaPedido As String

''' <summary>
''' Establecer el valor de mvarConexion
''' </summary>
''' <remarks>Llamada desde: CRaiz/Generar_CPlanesEntrega; Tiempo m�ximo: 0</remarks>
Friend Property Set Conexion(ByVal con As CConexion)
Set mvarConexion = con
End Property

''' <summary>
''' Obtener el valor de mvarConexion
''' </summary>
''' <remarks>Llamada desde: AplicarPlanEnTransaccion; Tiempo m�ximo: 0</remarks>
Friend Property Get Conexion() As CConexion
Set Conexion = mvarConexion
End Property

''' <summary>
''' Recuperar una Unidad de la coleccion
''' </summary>
''' <param name="vntIndexKey">Indice de la Unidad a recuperar</param>
''' <returns>Unidad correspondiente</returns>
''' <remarks>Llamada desde: frmplanesEntrega/sdbgPlanes_BeforeUpdate         frmplanesEntrega/sdbgPlanes_Change; Tiempo m�ximo: 0</remarks>
Public Property Get Item(vntIndexKey As Variant) As CPlanEntrega
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
''' ! Pendiente de revision, objetivo desconocido
       Set NewEnum = mCol.[_NewEnum]
End Property

''' <summary>
''' Eliminar una Unidad de la coleccion
''' </summary>
''' <param name="vntIndexKey">Indice de la Unidad a eliminar</param>
''' <remarks>Llamada desde: frmPlanesEntrega/cmdEliminar_Click  ; Tiempo m�ximo: 0</remarks>
Public Sub Remove(vntIndexKey As Variant)
   
    mCol.Remove vntIndexKey
End Sub

''' <summary>
''' Devolver Numero de elementos de la coleccion
''' </summary>
''' <returns>Numero de elementos de la coleccion</returns>
''' <remarks>Llamada desde: frmPlanesEntrega/cmdEliminar_Click       frmPlanesEntrega/CargarGrid ; Tiempo m�ximo: 0</remarks>
Public Property Get Count() As Long
    If mCol Is Nothing Then
        Count = 0
    Else
        Count = mCol.Count
    End If
End Property

'''<summary>A�ade un nuevo item a la colecci�n</summary>
''' <param name="ID">ID del plan entrega</param>
''' <param name="idLineaPedido">Id de la linea de pedido del plan entrega</param>
''' <param name="IDOrdenEntrega">OrdenEntrega del plan entrega</param>
''' <param name="Fecha">Fecha del plan entrega</param>
''' <param name="Cantidad">Cantidad del plan entrega</param>
''' <param name="Albaran">Albaran del plan entrega</param>
''' <param name="FecAct">Fecha ultima actualizacion del plan entrega</param>
''' <param name="Importe">Importe del plan entrega (para los de recepcion por importe)</param>
''' <returns>Objeto de tipo CPlanEntrega a�adido</returns>
''' <remarks>Llamada desde: frmPlanesEntrega/actualizarYSalir       frmPlanesEntrega/cmdAceptarFecha_Click
''' frmPlanesEntrega/CargarGrid      frmPlanesEntrega/sdbgPlanes_BeforeUpdate ; Tiempo m�ximo: 0</remarks>
Public Function Add(ByVal Id As Variant, ByVal idLineaPedido As Variant, ByVal IDOrdenEntrega As Variant, ByVal Fecha As Variant _
, Optional ByVal Cantidad As Variant, Optional ByVal Albaran As Variant, Optional ByVal FecAct As Variant, Optional ByVal Importe As Variant) As CPlanEntrega
    Dim objnewmember As CPlanEntrega
    
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New CPlanEntrega
    Set objnewmember.Conexion = mvarConexion
    
    objnewmember.Id = Id
    objnewmember.idLineaPedido = idLineaPedido
    objnewmember.IDOrdenEntrega = IDOrdenEntrega
    objnewmember.Fecha = Fecha
    
    If Not IsMissing(Cantidad) Then
        objnewmember.Cantidad = Cantidad
    Else
        objnewmember.Cantidad = Null
    End If
    
    If Not IsMissing(Albaran) Then
        objnewmember.Albaran = Albaran
    Else
        objnewmember.Albaran = Null
    End If
    
    If IsMissing(FecAct) Then
        objnewmember.FecAct = Null
    Else
        objnewmember.FecAct = FecAct
    End If
    
    If IsMissing(Importe) Then
        objnewmember.Importe = Null
    Else
        objnewmember.Importe = Importe
    End If
    
    mCol.Add objnewmember, CStr(Id)
    
    Set Add = objnewmember
    
    Set objnewmember = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CPlanesEntrega", "Add", ERR, Erl)
      Exit Function
   End If
End Function

''' <summary>
''' creates the collection when this class is created
''' </summary>
''' <remarks>Llamada desde: sistema ; Tiempo m�ximo: 0</remarks>
Private Sub Class_Initialize()
    Set mCol = New Collection
End Sub

''' <summary>
''' destroys collection when this class is terminated
''' </summary>
''' <remarks>Llamada desde: sistema ; Tiempo m�ximo: 0</remarks>
Private Sub Class_Terminate()
    Set mCol = Nothing
    Set mvarConexion = Nothing
End Sub

''' <summary>
''' Recalcular los planes de las diferentes lines de pedido, lo q se calcula es la nueva cantidad
''' </summary>
''' <param name="oLineaPedido">Para q linea de pedido se recalculan los planes</param>
''' <param name="dCantPlanConfig">Cantidad suma de cantidades de todos los planes ya configurados</param>
''' <param name="dPorcentajeAConfig">Esta cantidad suma cuanto porcentaje representa respecto a la linea de pedido, ejemplo dCantPlanConfig:1 Cantidadpedida: 5, luego dPorcentajeAConfig:0,2 </param>
''' <param name="bRecepImporte">Para saber si me pasa importe o cantidad</param>
''' <returns>Objeto cargado con los planes recalculados</returns>
''' <remarks>Llamada desde: frmSeguimiento/AplicarPlanConfigurado ; Tiempo m�ximo: 0</remarks>
Public Function RecalcularCantidadesPlan(ByVal oLineaPedido As CLineaPedido, ByVal dCantPlanConfig As Double, ByVal dPorcentajeAConfig As Double, Optional ByVal bRecepImporte As Boolean = False) As CPlanesEntrega
    Dim oPlan As CPlanEntrega
    Dim oPlanes As CPlanesEntrega
    
    Dim i As Integer
    Dim ColCount As Integer
    Dim CantLlevas As Double
    Dim Cantidad As Double
    Dim dCantLinPedido As Double
    Dim ImporteLlevas As Double
    Dim Importe As Double
    Dim dImporteLinPedido As Double
    Dim dPorcentaje As Double
    Dim iNumDecimales As Integer
    
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oPlanes = New CPlanesEntrega
    Set oPlanes.Conexion = mvarConexion
    
    i = 1
    ColCount = mCol.Count
    CantLlevas = 0
    ImporteLlevas = 0
    
    dCantLinPedido = oLineaPedido.CantidadPedida * dPorcentajeAConfig
    dImporteLinPedido = oLineaPedido.ImporteNeto * dPorcentajeAConfig
    
    For Each oPlan In mCol
        If Not bRecepImporte Then
            If oPlan.Cantidad <> "" Then
                dPorcentaje = oPlan.Cantidad / dCantPlanConfig
            End If
        Else
            If oPlan.Importe <> "" Then
                dPorcentaje = oPlan.Importe / dCantPlanConfig
            End If
        End If
        
        'Si el plan se ha hecho por importe el porcentaje se calcular� a partir de ah� pero habr� que
        'recalcular tanto importes como cantidades para los dos tipos de recepciones posibles
        If oLineaPedido.tipoRecepcion = 0 Then
            If i < ColCount Then
                Cantidad = dCantLinPedido * dPorcentaje
            Else
                Cantidad = dCantLinPedido - CantLlevas
            End If
            iNumDecimales = ValidarNumeroMaximoDecimales(oLineaPedido.UnidadPedido, Cantidad)
            If iNumDecimales <> -1 Then
                Cantidad = Round(Cantidad, iNumDecimales)
            End If
            CantLlevas = CantLlevas + Cantidad
            oPlanes.Add CStr(i), oLineaPedido.Id, oLineaPedido.Orden, oPlan.Fecha, Cantidad, ""
        Else
            If i < ColCount Then
                Importe = dImporteLinPedido * dPorcentaje
            Else
                Importe = dImporteLinPedido - ImporteLlevas
            End If
            iNumDecimales = ValidarNumeroMaximoDecimales(oLineaPedido.UnidadPedido, Importe)
            If iNumDecimales <> -1 Then
                Importe = Round(Importe, iNumDecimales)
            End If
            ImporteLlevas = ImporteLlevas + Importe
            oPlanes.Add CStr(i), oLineaPedido.Id, oLineaPedido.Orden, oPlan.Fecha, , "", , Importe
        End If
        i = i + 1
    Next
    
    Set RecalcularCantidadesPlan = oPlanes
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CPlanesEntrega", "RecalcularCantidadesPlan", ERR, Erl)
      Exit Function
   End If
End Function

''' <summary>
''' Hacer en una unica transaccion todas las modificaciones/inserciones/borrados de un cambio en los planes cuando se "aplica a todas las lineas de pedido"
''' </summary>
''' <returns>Error de haberlo</returns>
''' <remarks>Llamada desde: frmSeguimiento/AplicarPlanConfigurado       frmSeguimiento/AplicarUnPlanConfigurado
''' frmSeguimiento/EliminarPlanesDeLineaPedido; Tiempo m�ximo: 0,1sg para una unica update en AplicarUnPlanConfigurado</remarks>
Public Function AplicarPlanEnTransaccion() As TipoErrorSummit
    
    Dim TESError As TipoErrorSummit
    
    TESError.NumError = TESnoerror
    
    If mvarConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CPLanesEntrega.AplicarPlanEnTransaccion", "No se ha establecido la conexion"
    End If
    
    If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
    
    mvarConexion.ADOCon.Execute "BEGIN TRANSACTION"
    
    mvarConexion.ADOCon.Execute m_sAplicarUnPlanALIneaPedido
    If mvarConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    
    mvarConexion.ADOCon.Execute "COMMIT TRANSACTION"
    AplicarPlanEnTransaccion = TESError
    Exit Function
    
Error_Cls:
     
    AplicarPlanEnTransaccion = basErrores.TratarError(mvarConexion.ADOCon.Errors)
    mvarConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CPlanesEntrega", "AplicarPlanEnTransaccion", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If
End Function

''' <summary>
''' Obtener el valor de m_sAplicarUnPlanALIneaPedido
''' </summary>
''' <remarks>Llamada desde: frmSeguimiento/AplicarPlanConfigurado       frmSeguimiento/AplicarUnPlanConfigurado
''' frmSeguimiento/EliminarPlanesDeLineaPedido; Tiempo m�ximo: 0</remarks>
Public Property Get SQLAplicarUnPlanALIneaPedido() As String
    SQLAplicarUnPlanALIneaPedido = m_sAplicarUnPlanALIneaPedido
End Property

''' <summary>
''' Establecer el valor de m_sAplicarUnPlanALIneaPedido
''' </summary>
''' <remarks>Llamada desde: frmSeguimiento/AplicarPlanConfigurado     frmSeguimiento/AplicarUnPlanConfigurado
''' frmSeguimiento/EliminarPlanesDeLineaPedido; Tiempo m�ximo: 0</remarks>
Public Property Let SQLAplicarUnPlanALIneaPedido(ByVal Data As String)
    m_sAplicarUnPlanALIneaPedido = Data
End Property

''' <summary>
''' Valida el numero maximo de decimales de una cantidad
''' </summary>
''' <param name="UnidadCompra">Unidad de compra</param>
''' <param name="Cantidad">Cantidad</param>
''' <returns>Si la validacion es correcta se envia -1, sino se envia el numero maximo de decimales permitido</returns>
''' <remarks>Llamada desde: CplanesEntrega.RecalcularCantidadesPlan, Tiempo m�ximo: 0</remarks>
Private Function ValidarNumeroMaximoDecimales(ByVal UnidadCompra As String, ByVal Cantidad As Variant) As Integer
    Dim oUnidades As CUnidades
    Dim iLongitudParteEntera As Integer
    Dim iNumDecimales As Integer
    Dim dParteDecimal As Double
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oUnidades = New CUnidades
    Set oUnidades.Conexion = mvarConexion
    oUnidades.CargarTodasLasUnidades UnidadCompra, , True
    If Not oUnidades.Item(UnidadCompra) Is Nothing Then
        iLongitudParteEntera = Len(CStr(Int(Cantidad)))
        iNumDecimales = Len(CStr(Cantidad)) - iLongitudParteEntera - 1
        If iNumDecimales <> -1 Then
            dParteDecimal = Round(Round(Cantidad, iNumDecimales) - Int(Cantidad), iNumDecimales)
            iNumDecimales = Len(Mid(CStr(dParteDecimal), 3, Len(CStr(dParteDecimal))))
            If Not IsNull(oUnidades.Item(UnidadCompra).NumDecimales) Then
                If iNumDecimales > oUnidades.Item(UnidadCompra).NumDecimales Then
                     ValidarNumeroMaximoDecimales = oUnidades.Item(UnidadCompra).NumDecimales
                Else
                     ValidarNumeroMaximoDecimales = -1
                End If
            Else
                ValidarNumeroMaximoDecimales = -1
            End If
        Else
            ValidarNumeroMaximoDecimales = -1
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CPlanesEntrega", "ValidarNumeroMaximoDecimales", ERR, Erl)
      Exit Function
   End If
End Function
