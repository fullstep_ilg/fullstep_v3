VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CNotificacionesPedido"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum
Private mCol As Collection
Private mvarConexion As CConexion

Friend Property Set Conexion(ByVal con As CConexion)
Set mvarConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = mvarConexion
End Property


Public Property Get Item(vntIndexKey As Variant) As CNotificacionPedido
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property
Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
       Set NewEnum = mCol.[_NewEnum]
End Property
Public Sub Remove(vntIndexKey As Variant)
   
    mCol.Remove vntIndexKey
End Sub
Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
     Count = mCol.Count
End If


End Property

''' <summary>
''' Crea y carga un objeto Notificacion de Pedido
''' </summary>
''' <param name="ID">Id de la Notificacion</param>
''' <param name="Pedido">Numero pedido del pedido</param>
''' <param name="Orden">Numero de la orden</param>
''' <param name="CodProve">Proveedor de la orden<</param>
''' <param name="DenProve">Denominaci�n del proveedor</param>
''' <param name="con">contacto del proveedor</param>
''' <param name="Apellidos">Apellidos de contacto</param>
''' <param name="Nombre">Nombre de contacto</param>
''' <param name="Email">Email de contacto</param>
''' <param name="Tipo">Tipo de comunicaci�n (emision, anulacion, ...)</param>
''' <param name="Fecha">Fecha de comunicaci�n</param>
''' <param name="Web">Comunica via web o no</param>
''' <param name="Mail">Comunica via mail o no</param>
''' <param name="Carta">Comunica via carta o no</param>
''' <param name="FormatoPedido">0 Nada ,  1 Word ,  2 Excel</param>
''' <param name="varIndice">Indice</param>
''' <param name="iIDRegistroMail">Id de la tabla REGISTRO_MAIL</param>
''' <param name="bFallido">Envio mail correcto o no</param>
''' <returns>Objeto CNotificacionPedido</returns>
''' <remarks>Llamada desde: frmPedidosEmision1/cmdAceptar_Click     frmNotificaPedido/cmdAceptar_Click; Tiempo m�ximo: 0</remarks>
Public Function Add(Optional ByVal ID As Variant, Optional ByVal Pedido As Variant, Optional ByVal Orden As Variant, Optional ByVal CodProve As String _
, Optional ByVal DenProve As String, Optional ByVal con As Variant, Optional ByVal Apellidos As String, Optional ByVal Nombre As String _
, Optional ByVal Email As String, Optional ByVal Tipo As Integer, Optional ByVal Fecha As Date, Optional ByVal Web As Boolean _
, Optional ByVal Mail As Boolean, Optional ByVal Carta As Boolean, Optional ByVal FormatoPedido As Variant, Optional ByVal varIndice As Variant _
, Optional iIDRegistroMail As Long, Optional bFallido As Boolean) As CNotificacionPedido
   
    Dim objnewmember As CNotificacionPedido
    
    Set objnewmember = New CNotificacionPedido
    Set objnewmember.Conexion = mvarConexion
    
    If Not IsMissing(ID) And Not IsEmpty(ID) Then
        objnewmember.ID = ID
    Else
        objnewmember.ID = Null
    End If
    
    If Not IsMissing(Pedido) Then
        objnewmember.Pedido = Pedido
    Else
        objnewmember.Pedido = Null
    End If
    
    If Not IsMissing(Orden) Then
        objnewmember.Orden = Orden
    Else
        objnewmember.Orden = Null
    End If
    
    objnewmember.CodProve = CodProve
    objnewmember.DenProve = DenProve
    
    If Not IsMissing(con) Then
        objnewmember.con = con
    Else
        objnewmember.con = Null
    End If
    
    objnewmember.Apellidos = Apellidos
    objnewmember.Nombre = Nombre
    objnewmember.Email = Email
    objnewmember.Tipo = Tipo
    objnewmember.Fecha = Fecha
    objnewmember.Web = Web
    objnewmember.Mail = Mail
    objnewmember.Carta = Carta
    
    If Not IsMissing(FormatoPedido) Then
        objnewmember.FormatoPedido = FormatoPedido
    Else
        objnewmember.FormatoPedido = Null
    End If
    
    objnewmember.IDRegistroMail = iIDRegistroMail
    'Por defecto inicializamos la propiedad Fallido a false si no nos viene como par�metro.
    If Not IsMissing(bFallido) Then
        objnewmember.Fallido = bFallido
    Else
        objnewmember.Fallido = False
    End If
    objnewmember.MailCancel = False 'Por defecto inicializamos siempre esta propiedad a false.
    
    If Not IsMissing(varIndice) And Not IsNull(varIndice) Then
        objnewmember.Indice = varIndice
        mCol.Add objnewmember, CStr(varIndice)
    Else
        mCol.Add objnewmember, CStr(ID)
    End If
    
    Set objnewmember.Conexion = mvarConexion
    
    Set Add = objnewmember
    
    Set objnewmember = Nothing

End Function
Private Sub Class_Initialize()
    Set mCol = New Collection
End Sub
Private Sub Class_Terminate()
    'destroys collection when this class is terminated
    Set mCol = Nothing
    Set mvarConexion = Nothing
   
End Sub






