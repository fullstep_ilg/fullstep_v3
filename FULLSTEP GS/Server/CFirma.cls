VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CFirma"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
''' *** Clase: CFirma
''' *** Creacion: 1/7/1999 (Javier Arana)

Option Explicit

Implements IBaseDatos

''' Variables privadas con la informacion de una Unidad

Private mvarDen As String
Private mvarIndice As Integer
Private mvarCod As Integer

''' Conexion

Private mvarConexion As CConexion
Private mvarConexionServer As CConexionDeUseServer

''' Control de errores

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

''' Resultset para la Unidad a editar

Private mvarRecordset As ADODB.Recordset



Public Property Get Indice() As Integer

    ''' * Objetivo: Devolver el indice de la Unidad en la coleccion
    ''' * Recibe: Nada
    ''' * Devuelve: Indice de la Unidad en la coleccion

    Indice = mvarIndice
    
End Property
Public Property Let Indice(ByVal iInd As Integer)

    ''' * Objetivo: Dar valor al indice de la Unidad en la coleccion
    ''' * Recibe: Indice de la Unidad en la coleccion
    ''' * Devuelve: Nada

    mvarIndice = iInd
    
End Property
Friend Property Set ConexionServer(ByVal con As CConexionDeUseServer)
Set mvarConexionServer = con
End Property
Friend Property Get ConexionServer() As CConexionDeUseServer
Set ConexionServer = mvarConexionServer
End Property
Private Sub ConectarDeUseServer()
    If mvarConexionServer.ADOSummitCon Is Nothing Then
        Set mvarConexionServer.ADOSummitCon = New ADODB.Connection
        mvarConexionServer.ADOSummitCon.Open "Provider=SQLOLEDB.1;Auto Translate=FALSE;Server=" & basParametros.ServidorGS & ";Database=" & basParametros.BaseDatosGS & ";", LICDBLogin, LICDBContra
        mvarConexionServer.ADOSummitCon.CursorLocation = adUseServer
        mvarConexionServer.ADOSummitCon.CommandTimeout = 120
    End If
End Sub
Public Property Get Cod() As Integer

    ''' * Objetivo: Devolver el indice de la Unidad en la coleccion
    ''' * Recibe: Nada
    ''' * Devuelve: Indice de la Unidad en la coleccion

    Cod = mvarCod
    
End Property
Public Property Let Cod(ByVal iInd As Integer)

    ''' * Objetivo: Dar valor al indice de la Unidad en la coleccion
    ''' * Recibe: Indice de la Unidad en la coleccion
    ''' * Devuelve: Nada

    mvarCod = iInd
    
End Property

Friend Property Set Conexion(ByVal vData As CConexion)

    ''' * Objetivo: Dar valor a la conexion de la Unidad
    ''' * Recibe: Conexion
    ''' * Devuelve: Nada

    Set mvarConexion = vData
    
End Property
Friend Property Get Conexion() As CConexion

    ''' * Objetivo: Devolver la conexion de la Unidad
    ''' * Recibe: Nada
    ''' * Devuelve: Conexion

    Set Conexion = mvarConexion
    
End Property
Public Property Let Den(ByVal vData As String)

    ''' * Objetivo: Dar valor a la variable privada DEN
    ''' * Recibe: Denominacion de la Unidad
    ''' * Devuelve: Nada

    mvarDen = vData
    
End Property
Public Property Get Den() As String

    ''' * Objetivo: Devolver la variable privada DEN
    ''' * Recibe: Nada
    ''' * Devuelve: Denominacion de la Unidad

    Den = mvarDen
    
End Property

Private Sub Class_Terminate()

    ''' * Objetivo: Limpiar la memoria
    
    Set mvarRecordset = Nothing
    Set mvarConexion = Nothing
    Set mvarConexionServer = Nothing
End Sub
Private Function IBaseDatos_AnyadirABaseDatos() As TipoErrorSummit

    ''' * Objetivo: Anyadir la Unidad a la tabla
    ''' * Recibe: Nada
    ''' * Devuelve: TipoErrorSummit con el error si lo hay
    
    Dim sConsulta As String
    Dim TESError As TipoErrorSummit
    Dim rs As New ADODB.Recordset
    
    TESError.NumError = TESnoerror
    
    ''' Precondicion
    
    If mvarConexion Is Nothing Then
        Err.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CUnidad.AnyadirABAseDatos", "No se ha establecido la conexion"
    End If
    
On Error GoTo Error:
                    
    ''' Ejecutar insercion
    
    sConsulta = "INSERT INTO FIRMAS (COD,DEN) VALUES (" & mvarCod & ",'" & DblQuote(mvarDen) & "')"
    mvarConexion.ADOCon.Execute sConsulta
  
    IBaseDatos_AnyadirABaseDatos = TESError
    
    Exit Function
    
Error:
     
    IBaseDatos_AnyadirABaseDatos = basErrores.TratarError(mvarConexion.ADOCon.Errors)
           
End Function
Private Function IBaseDatos_CambiarCodigo(CodigoNuevo As Variant) As TipoErrorSummit

    ''' * Objetivo: Cambiar de codigo la Unidad
    ''' * Recibe: Codigo nuevo
    ''' * Devuelve: TipoErrorSummit con el error si lo hay
    
    Dim cm As ADODB.Command
    Dim par As ADODB.Parameter

    Dim TESError As TipoErrorSummit
    
    TESError.NumError = TESnoerror
    
    ''' Precondicion
    
    If mvarConexion Is Nothing Then
        Err.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CUnidad.EliminarDeBaseDatos", "No se ha establecido la conexion"
    End If
        
On Error GoTo Error:
    
    ''' Preparar la SP y sus parametros
    
        
    'Set q = mvarConexion.rdoSummitCon.CreateQuery("CambiarCodigo", sql)
    Set cm = New ADODB.Command
    Set cm.ActiveConnection = mvarConexion.ADOCon
    
    Set par = cm.CreateParameter("OLD", adInteger, adParamInput, , Me.Cod)
    cm.Parameters.Append par
    Set par = cm.CreateParameter("NEW", adInteger, adParamInput, , CodigoNuevo)
    cm.Parameters.Append par

   
    ''' Ejecutar la SP
    
    cm.CommandType = adCmdStoredProc
    cm.CommandText = "FIRMAS_COD"
    
    cm.Execute
 
    Set cm = Nothing

    
    mvarCod = CodigoNuevo
        
    IBaseDatos_CambiarCodigo = TESError
    
    Exit Function
    
Error:
    
    If Not cm Is Nothing Then
        Set cm = Nothing
    End If
    
    IBaseDatos_CambiarCodigo = basErrores.TratarError(mvarConexion.ADOCon.Errors)
        
End Function
Private Sub IBaseDatos_CancelarEdicion()

    ''' * Objetivo: Cancelar la edicion, cerrando el Resultset
    
    If Not mvarRecordset Is Nothing Then
        mvarRecordset.Close
        Set mvarRecordset = Nothing
    End If

End Sub
Private Function IBaseDatos_ComprobarExistenciaEnBaseDatos() As Boolean

    ''' ! No usado por ahora
    
End Function
Private Function IBaseDatos_EliminarDeBaseDatos() As TipoErrorSummit

    ''' * Objetivo: Eliminar la Unidad de la tabla
    ''' * Recibe: Nada
    ''' * Devuelve: TipoErrorSummit con el error si lo hay

    Dim TESError As TipoErrorSummit
    
    TESError.NumError = TESnoerror
    
    ''' Precondicion
    
    If mvarConexion Is Nothing Then
        Err.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CUnidad.EliminarDeBaseDatos", "No se ha establecido la conexion"
    End If
    
On Error GoTo Error:
        
    ''' Borrado
    
    mvarConexion.ADOCon.Execute "DELETE FROM FIRMAS WHERE COD=" & mvarCod
    
    IBaseDatos_EliminarDeBaseDatos = TESError
    
    Exit Function
        
Error:
        
    IBaseDatos_EliminarDeBaseDatos = basErrores.TratarError(mvarConexion.ADOCon.Errors)
    
End Function

Private Function IBaseDatos_FinalizarEdicionEliminando() As TipoErrorSummit

    ''' ! No usado por ahora
    
End Function
Private Function IBaseDatos_FinalizarEdicionModificando() As TipoErrorSummit

    ''' * Objetivo: Finalizar la edicion del Resultset modificando
    ''' * Recibe: Nada
    ''' * Devuelve: TipoErrorSummit con el error si lo hay

    Dim TESError As TipoErrorSummit

    TESError.NumError = TESnoerror

    ''' Precondicion
    
    If mvarConexion Is Nothing Then
        Err.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CUnidad.FinalizarEdicionModificando", "No se ha establecido la conexion"
    End If

On Error GoTo Error:
    
    ''' Actualizar
        
    mvarRecordset("DEN") = mvarDen
    mvarRecordset.Update
    
    ''' Cierre
    
    mvarRecordset.Close
    Set mvarRecordset = Nothing
    
    IBaseDatos_FinalizarEdicionModificando = TESError
    
    Exit Function
    
Error:
        
    IBaseDatos_FinalizarEdicionModificando = basErrores.TratarError(mvarConexionServer.ADOSummitCon.Errors)
    Resume Salir:

Salir:
    
    On Error Resume Next
    
    ''' Cancelar la actualizacion del recordset
    
    If mvarRecordset.EditMode > 0 Then
        mvarRecordset.CancelUpdate
    End If
    
    IBaseDatos_FinalizarEdicionModificando = TESError

End Function

''' <summary>Inicia la edici�n</summary>
''' <param name="Bloquear">No se usa</param>
''' <param name="UsuarioBloqueo">No se usa</param>
''' <returns>Variable tipo TipoErrorSummit con el error si se ha producido</returns>
''' <remarks>Llamada desde: FSGSClient; Tiempo m�ximo: 1 sec </remarks>
''' <revision>LTG 04/01/2012</revision>

Private Function IBaseDatos_IniciarEdicion(Optional ByVal Bloquear As Boolean, Optional ByVal UsuarioBloqueo As String) As TipoErrorSummit
    Dim TESError As TipoErrorSummit
    
    TESError.NumError = TESnoerror
    
    ''' Precondicion
    
    If mvarConexion Is Nothing Then
        Err.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CFirma.IniciarEdicion", "No se ha establecido la conexion"
    End If
    
    ''' Abrir Resultset
    ConectarDeUseServer
    
    Set mvarRecordset = New ADODB.Recordset
    'CALIDAD: Sin WITH (NOLOCK) porque se rellena el recordset mvarRecordset que posteriormente se utiliza para las actualizaciones
    'en otros m�todos de la clase. Si se hace la lectura con WITH (NOLOCK) no se permite la llamada al m�todo Update del recordset.
    mvarRecordset.Open "SELECT COD,DEN,FECACT FROM FIRMAS WHERE COD=" & mvarCod, mvarConexionServer.ADOSummitCon, adOpenKeyset, adLockOptimistic

    ''' Si no esta la Unidad, alguien la ha eliminado
    
    If mvarRecordset.eof Then
    
        mvarRecordset.Close
        Set mvarRecordset = Nothing
        
        TESError.NumError = TESDatoEliminado
        TESError.Arg1 = 93 '"Firma"
        
        IBaseDatos_IniciarEdicion = TESError
                
        Exit Function
        
    End If
    
    ''' Si hay datos diferentes, devolver la condicion
    
    If mvarDen <> mvarRecordset("DEN") Then
        mvarDen = mvarRecordset("DEN")
        TESError.NumError = TESInfActualModificada
        'TESError.Arg1 = "Firma"
    End If
    
    IBaseDatos_IniciarEdicion = TESError

End Function
Private Function IBaseDatos_ModificarEnBaseDatos() As Boolean

    ''' ! No usado por ahora
    
End Function

