VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CConfVistaCalGlobalVar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements IBaseDatos

Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum

Private m_iVista As Integer
Private m_iVarcal As Integer
Private m_iNivel As Integer
Private m_bVisible As Boolean
Private m_iPos As Integer
Private m_sDen As String
Private m_sUnQas As String
Private m_oVarCalVista As CConfVistasCalGlobalVar

Private m_oConexion As CConexion
Private m_vIndice As Variant

Friend Property Set Conexion(ByVal con As CConexion)
    Set m_oConexion = con
End Property
Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property

Public Property Set VariblesCalVista(ByVal vData As CConfVistasCalGlobalVar)
    Set m_oVarCalVista = vData
End Property
Public Property Get VariblesCalVista() As CConfVistasCalGlobalVar
    Set VariblesCalVista = m_oVarCalVista
End Property

Public Property Let Vista(ByVal dato As Integer)
    m_iVista = dato
End Property
Public Property Get Vista() As Integer
    Vista = m_iVista
End Property

Public Property Let VarCal(ByVal dato As Integer)
    m_iVarcal = dato
End Property
Public Property Get VarCal() As Integer
    VarCal = m_iVarcal
End Property

Public Property Let Nivel(ByVal dato As Integer)
    m_iNivel = dato
End Property
Public Property Get Nivel() As Integer
    Nivel = m_iNivel
End Property

Public Property Let Visible(ByVal dato As Boolean)
    m_bVisible = dato
End Property
Public Property Get Visible() As Boolean
    Visible = m_bVisible
End Property

Public Property Let Posicion(ByVal dato As Integer)
    m_iPos = dato
End Property
Public Property Get Posicion() As Integer
    Posicion = m_iPos
End Property

Public Property Let Denominacion(ByVal dato As String)
    m_sDen = dato
End Property
Public Property Get Denominacion() As String
    Denominacion = m_sDen
End Property

Public Property Let Indice(ByVal vIndice As Variant)
    m_vIndice = vIndice
End Property
Public Property Get Indice() As Variant
    Indice = m_vIndice
End Property

''' <summary>
''' Inserta en bbdd una configuraci�n.
''' </summary>
''' <returns>Error de haberlo</returns>
''' <remarks>Llamada desde: frmComparativaQA/GuardarVistaGeneralEnBd ; Tiempo m�ximo: 0,1</remarks>
Private Function IBaseDatos_AnyadirABaseDatos() As TipoErrorSummit
Dim sConsulta As String
Dim TESError As TipoErrorSummit


If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:

    TESError.NumError = TESnoerror
    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
    
    sConsulta = "INSERT INTO CONF_VISTAS_CAL_GLOBAL_VAR (VISTA, VARCAL, NIVEL, VISIBLE, POS, UNQAS)"
    sConsulta = sConsulta & " VALUES (" & m_iVista & ", " & m_iVarcal & ", " & m_iNivel
    sConsulta = sConsulta & ", " & BooleanToSQLBinary(m_bVisible) & ", " & m_iPos & ", " & StrToSQLNULL(m_sUnQas) & ") "

    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    IBaseDatos_AnyadirABaseDatos = TESError
    Exit Function

Error_Cls:
    IBaseDatos_AnyadirABaseDatos = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      'Call g_oErrores.TratarError("Class Module", "CConfVistaCalGlobalVar", "IBaseDatos_AnyadirABaseDatos", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If
End Function

Private Function IBaseDatos_ComprobarExistenciaEnBaseDatos() As Boolean
    Dim sConsulta As String
    Dim AdoRes As New adodb.Recordset
    
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sConsulta = "SELECT count(1) EXISTE FROM CONF_VISTAS_CAL_GLOBAL_VAR WITH (NOLOCK) WHERE VISTA = " & m_iVista
    sConsulta = sConsulta & " AND VARCAL = " & m_iVarcal
    sConsulta = sConsulta & " AND NIVEL = " & m_iNivel

    Set AdoRes = New adodb.Recordset
    AdoRes.CursorLocation = adUseServer
    AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    If AdoRes.Fields("EXISTE").Value > 0 Then
        IBaseDatos_ComprobarExistenciaEnBaseDatos = True
    Else
        IBaseDatos_ComprobarExistenciaEnBaseDatos = False
    End If

    AdoRes.Close
    Set AdoRes = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CConfVistaCalGlobalVar", "IBaseDatos_ComprobarExistenciaEnBaseDatos", ERR, Erl)
      Exit Function
   End If
End Function

Private Function IBaseDatos_EliminarDeBaseDatos() As TipoErrorSummit
Dim sConsulta As String
Dim TESError As TipoErrorSummit

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls

    TESError.NumError = TESnoerror
     
    sConsulta = "DELETE FROM CONF_VISTAS_CAL_GLOBAL_VAR WHERE VISTA = " & m_iVista
    sConsulta = sConsulta & " AND VARCAL = " & m_iVarcal
    sConsulta = sConsulta & " AND NIVEL = " & m_iNivel

    m_oConexion.ADOCon.Execute sConsulta

    IBaseDatos_EliminarDeBaseDatos = TESError

    Exit Function

Error_Cls:
    IBaseDatos_EliminarDeBaseDatos = basErrores.TratarError(m_oConexion.ADOCon.Errors)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      'Call g_oErrores.TratarError("Class Module", "CConfVistaCalGlobalVar", "IBaseDatos_EliminarDeBaseDatos", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If

End Function

''' <summary>
''' Actualiza en BD los datos de una vista
''' </summary>
''' <returns>Error de haberlo</returns>
''' <remarks>Llamada desde: frmComparativaQA/GuardarVistaGeneralEnBd ; Tiempo m�ximo: 0,1</remarks>
Private Function IBaseDatos_FinalizarEdicionModificando() As TipoErrorSummit
Dim TESError As TipoErrorSummit
Dim sConsulta As String

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls
    TESError.NumError = TESnoerror
    
    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"

    sConsulta = "UPDATE CONF_VISTAS_CAL_GLOBAL_VAR SET VISIBLE = " & BooleanToSQLBinary(m_bVisible) & ", POS = " & m_iPos & ", UNQAS = " & StrToSQLNULL(m_sUnQas)
    sConsulta = sConsulta & " WHERE VISTA = " & m_iVista
    sConsulta = sConsulta & " AND VARCAL = " & m_iVarcal
    sConsulta = sConsulta & " AND NIVEL = " & m_iNivel

    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"

    IBaseDatos_FinalizarEdicionModificando = TESError

    Exit Function

Error_Cls:
    IBaseDatos_FinalizarEdicionModificando = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"

    On Error Resume Next
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      'Call g_oErrores.TratarError("Class Module", "CConfVistaCalGlobalVar", "IBaseDatos_FinalizarEdicionModificando", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If
End Function

Private Sub Class_Terminate()
On Error GoTo Error_Cls:

    Set m_oConexion = Nothing
    Exit Sub

Error_Cls:
    Resume Next
End Sub

Private Function IBaseDatos_CambiarCodigo(CodigoNuevo As Variant) As TipoErrorSummit

End Function

Private Sub IBaseDatos_CancelarEdicion()

End Sub

Private Function IBaseDatos_FinalizarEdicionEliminando() As TipoErrorSummit

End Function

Private Function IBaseDatos_IniciarEdicion(Optional ByVal Bloquear As Boolean, Optional ByVal UsuarioBloqueo As String) As TipoErrorSummit

End Function

''' <summary>
''' A�ade una nueva vista global en la BD
''' </summary>
''' <param name="iVista">Id de la vista a crear</param>
''' <returns>Devuelve si se ha guardado correctamente en BD</returns>
''' <remarks>Llamada desde: CConfVistaCalGlobal/GuardarVistaNuevaBd; Tiempo m�ximo: 0,1</remarks>
Public Function VistaAnyadirABaseDatos(ByVal iVista As Integer) As TipoErrorSummit
Dim sConsulta As String
Dim TESError As TipoErrorSummit

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:

    TESError.NumError = TESnoerror

    sConsulta = "INSERT INTO CONF_VISTAS_CAL_GLOBAL_VAR (VISTA, VARCAL, NIVEL, VISIBLE, POS, UNQAS)"
    sConsulta = sConsulta & " VALUES (" & iVista & ", " & m_iVarcal & ", " & m_iNivel
    sConsulta = sConsulta & ", " & BooleanToSQLBinary(m_bVisible) & ", " & m_iPos & ", " & StrToSQLNULL(m_sUnQas) & ") "

    m_oConexion.ADOCon.Execute sConsulta

    VistaAnyadirABaseDatos = TESError
    Exit Function

Error_Cls:
    VistaAnyadirABaseDatos = basErrores.TratarError(m_oConexion.ADOCon.Errors)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      'Call g_oErrores.TratarError("Class Module", "CConfVistaCalGlobalVar", "VistaAnyadirABaseDatos", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If
End Function

Public Property Let UnQas(ByVal dato As String)
    m_sUnQas = dato
End Property
Public Property Get UnQas() As String
    UnQas = m_sUnQas
End Property
