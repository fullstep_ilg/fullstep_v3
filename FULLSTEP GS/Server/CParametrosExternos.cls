VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CParametrosExternos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

''' *** Clase: CParametrosExternos

''' Variables que almacenan las propiedades de los objetos
Private mCol As Collection
Private m_bEOF As Boolean
Private m_oConexion As CConexion

''' Control de errores
Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum

Public Property Get eof() As Boolean
    eof = m_bEOF
End Property
Friend Property Let eof(ByVal b As Boolean)
    m_bEOF = b
End Property

Friend Property Set Conexion(ByVal con As CConexion)
    ''' * Objetivo: Dar valor a la variable privada Conexion
    ''' * Recibe: Conexion
    ''' * Devuelve: Nada
    Set m_oConexion = con
End Property
Friend Property Get Conexion() As CConexion
    ''' * Objetivo: Devolver variable privada Conexion
    ''' * Recibe: Nada
    ''' * Devuelve: Conexion
    Set Conexion = m_oConexion
End Property

Public Property Get Count() As Long
    If mCol Is Nothing Then
        Count = 0
    Else
         Count = mCol.Count
    End If
End Property

Public Property Get Item(vntIndexKey As Variant) As CParametroExterno
On Error GoTo NoSeEncuentra:
    ''' Devolvemos el item de la coleccion privada
    Set Item = mCol(vntIndexKey)
    Exit Property
NoSeEncuentra:
    Set Item = Nothing
End Property

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
    Set NewEnum = mCol.[_NewEnum]
End Property
Public Sub Remove(vntIndexKey As Variant)
    mCol.Remove vntIndexKey
End Sub

Public Function Add(ByVal Sentido As Integer, ByVal param As String, ByVal Tipo As Integer, ByVal Grupo As Long, _
                ByVal TipoCampo As Integer, ByVal Campo As Long, Optional ByVal Directo As Boolean, _
                Optional ByVal IndError As Boolean, Optional ByVal Valor As String, Optional Id As Integer) As CParametroExterno
        
    Dim objnewmember As CParametroExterno
    Set objnewmember = New CParametroExterno
    Set objnewmember.Conexion = m_oConexion
    
    objnewmember.Sentido = Sentido
    objnewmember.param = param
    objnewmember.Tipo = Tipo
    objnewmember.Grupo = Grupo
    objnewmember.TipoCampo = TipoCampo
    objnewmember.Campo = Campo
    
    If Not IsMissing(Directo) Then
        objnewmember.Directo = Directo
    Else
        objnewmember.Directo = Null
    End If
    If Not IsMissing(IndError) Then
        objnewmember.IndError = IndError
    Else
        objnewmember.IndError = Null
    End If
    If Not IsMissing(Valor) Then
        objnewmember.Valor = Valor
    Else
        objnewmember.Valor = Null
    End If
    If Not IsMissing(Id) Then
        objnewmember.Id = Id
    Else
        objnewmember.Id = Null
    End If
    
    mCol.Add objnewmember, CStr(param)
    Set Add = objnewmember
    Set objnewmember = Nothing

End Function

Private Sub Class_Initialize()
    ''' * Objetivo: Crear la coleccion
    Set mCol = New Collection
End Sub
Private Sub Class_Terminate()
    ''' * Objetivo: Limpiar memoria
    Set mCol = Nothing
    Set m_oConexion = Nothing
End Sub

