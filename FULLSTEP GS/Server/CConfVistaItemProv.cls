VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CConfVistaItemProv"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CConfVistaItemProv **********************************
'*             Autor : Elena Mu�oz
'*             Creada : 08/07/2002
'***************************************************************

Option Explicit

Implements IBaseDatos

Private m_oConfVista As CConfVistaItem
Private m_iVista As Integer

Private m_sProv As String
Private m_bVisible As TipoProvVisible
Private m_sDenProve As String

Private m_oConexion As CConexion
Private m_vIndice As Variant

Public Property Let Vista(ByVal iVista As Integer)
    m_iVista = iVista
End Property

Public Property Get Vista() As Integer
    Vista = m_iVista
End Property

Public Property Set ConfVistaItem(ByVal oConf As CConfVistaItem)
    Set m_oConfVista = oConf
End Property
Public Property Get ConfVistaItem() As CConfVistaItem
    Set ConfVistaItem = m_oConfVista
End Property

Public Property Let Proveedor(ByVal dato As String)
    m_sProv = dato
End Property
Public Property Get Proveedor() As String
    Proveedor = m_sProv
End Property

Public Property Let Visible(ByVal dato As TipoProvVisible)
    m_bVisible = dato
End Property

Public Property Get Visible() As TipoProvVisible
    Visible = m_bVisible
End Property

Public Property Let DenProve(ByVal dato As String)
    m_sDenProve = dato
End Property

Public Property Get DenProve() As String
    DenProve = m_sDenProve
End Property

Friend Property Set Conexion(ByVal con As CConexion)
    Set m_oConexion = con
End Property

Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property

Public Property Let Indice(ByVal vIndice As Variant)
    m_vIndice = vIndice
End Property
Public Property Get Indice() As Variant
    Indice = m_vIndice
End Property

Private Sub Class_Terminate()
On Error GoTo Error_Cls:
    Set m_oConexion = Nothing
    Exit Sub
Error_Cls:
    Resume Next
End Sub

Private Function IBaseDatos_AnyadirABaseDatos() As TipoErrorSummit
Dim sConsulta As String
Dim TESError As TipoErrorSummit


If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
    
    TESError.NumError = TESnoerror
    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
    
    sConsulta = "INSERT INTO CONF_VISTAS_ITEM_PROVE (ANYO,GMN1,PROCE,GRUPO,PROVE,USU,VISTA,VISIBLE)"
    sConsulta = sConsulta & " VALUES (" & m_oConfVista.Grupo.Proceso.Anyo & ",'" & DblQuote(m_oConfVista.Grupo.Proceso.GMN1Cod) & "'," & m_oConfVista.Grupo.Proceso.Cod
    sConsulta = sConsulta & "," & (m_oConfVista.Grupo.Id)
    sConsulta = sConsulta & ",'" & DblQuote(m_sProv) & "','" & DblQuote(m_oConfVista.Usuario) & "'," & m_oConfVista.Vista & "," & m_bVisible & ")"
        
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    IBaseDatos_AnyadirABaseDatos = TESError
    Exit Function
    
Error_Cls:
    
    IBaseDatos_AnyadirABaseDatos = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      'Call g_oErrores.TratarError("Class Module", "CConfVistaItemProv", "IBaseDatos_AnyadirABaseDatos", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If

End Function

Private Function IBaseDatos_CambiarCodigo(CodigoNuevo As Variant) As TipoErrorSummit

End Function

Private Sub IBaseDatos_CancelarEdicion()

End Sub

Private Function IBaseDatos_ComprobarExistenciaEnBaseDatos() As Boolean
    Dim sConsulta As String
    Dim AdoRes As New adodb.Recordset
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sConsulta = "SELECT * FROM CONF_VISTAS_ITEM_PROVE WITH (NOLOCK) WHERE ANYO=" & m_oConfVista.Grupo.Proceso.Anyo & " AND GMN1='" & DblQuote(m_oConfVista.Grupo.Proceso.GMN1Cod) & "'"
    sConsulta = sConsulta & " AND PROCE=" & m_oConfVista.Grupo.Proceso.Cod & " AND GRUPO=" & DblQuote(m_oConfVista.Grupo.Id)
    sConsulta = sConsulta & " AND USU='" & DblQuote(m_oConfVista.Usuario) & "' AND VISTA=" & m_oConfVista.Vista
    sConsulta = sConsulta & " AND PROVE='" & DblQuote(m_sProv) & "'"

    Set AdoRes = New adodb.Recordset
    AdoRes.CursorLocation = adUseServer
    AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    If AdoRes.eof Then
        IBaseDatos_ComprobarExistenciaEnBaseDatos = False
    Else
        IBaseDatos_ComprobarExistenciaEnBaseDatos = True
    End If

    AdoRes.Close
    Set AdoRes = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CConfVistaItemProv", "IBaseDatos_ComprobarExistenciaEnBaseDatos", ERR, Erl)
      Exit Function
   End If

End Function

Private Function IBaseDatos_EliminarDeBaseDatos() As TipoErrorSummit

End Function

Private Function IBaseDatos_FinalizarEdicionEliminando() As TipoErrorSummit

End Function

Private Function IBaseDatos_FinalizarEdicionModificando() As TipoErrorSummit

End Function

Private Function IBaseDatos_IniciarEdicion(Optional ByVal Bloquear As Boolean, Optional ByVal UsuarioBloqueo As String) As TipoErrorSummit

End Function
Public Function VistaAnyadirABaseDatos(ByVal iVista As Integer, ByVal sUsuario As String) As TipoErrorSummit
Dim sConsulta As String
Dim TESError As TipoErrorSummit

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
    
    TESError.NumError = TESnoerror
    
    sConsulta = "INSERT INTO CONF_VISTAS_ITEM_PROVE (ANYO,GMN1,PROCE,GRUPO,PROVE,USU,VISTA,VISIBLE)"
    sConsulta = sConsulta & " VALUES (" & m_oConfVista.Grupo.Proceso.Anyo & ",'" & DblQuote(m_oConfVista.Grupo.Proceso.GMN1Cod) & "'," & m_oConfVista.Grupo.Proceso.Cod
    sConsulta = sConsulta & "," & (m_oConfVista.Grupo.Id)
    sConsulta = sConsulta & ",'" & DblQuote(m_sProv) & "','" & DblQuote(sUsuario) & "'," & iVista & "," & m_bVisible & ")"
        
    m_oConexion.ADOCon.Execute sConsulta

    VistaAnyadirABaseDatos = TESError
    Exit Function
    
Error_Cls:
    
    VistaAnyadirABaseDatos = basErrores.TratarError(m_oConexion.ADOCon.Errors)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      'Call g_oErrores.TratarError("Class Module", "CConfVistaItemProv", "VistaAnyadirABaseDatos", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If

End Function
