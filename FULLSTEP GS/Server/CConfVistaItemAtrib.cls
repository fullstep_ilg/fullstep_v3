VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CConfVistaItemAtrib"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CConfVistaItemAtrib **********************************
'*             Autor : Elena Mu�oz
'*             Creada : 08/07/2002
'***************************************************************

Option Explicit

Implements IBaseDatos

Private m_oConfVista As CConfVistaItem
Private m_iVista As Integer

Private m_lAtrib As Long
Private m_bVisible As Boolean
Private m_dblWidth As Double
Private m_sCodAtrib As String
Private m_sDenAtrib As String
Private m_iPosicion As Integer

Private m_oConexion As CConexion
Private m_vIndice As Variant


Public Property Let Atributo(ByVal lAtrib As Long)
    m_lAtrib = lAtrib
End Property

Public Property Get Atributo() As Long
    Atributo = m_lAtrib
End Property

Public Property Let Vista(ByVal iVista As Integer)
    m_iVista = iVista
End Property

Public Property Get Vista() As Integer
    Vista = m_iVista
End Property

Public Property Set ConfVistaItem(ByVal oConf As CConfVistaItem)
    Set m_oConfVista = oConf
End Property
Public Property Get ConfVistaItem() As CConfVistaItem
    Set ConfVistaItem = m_oConfVista
End Property

Public Property Let Visible(ByVal dato As Boolean)
    m_bVisible = dato
End Property

Public Property Get Visible() As Boolean
    Visible = m_bVisible
End Property

Public Property Let Width(ByVal dato As Double)
    m_dblWidth = dato
End Property

Public Property Get Width() As Double
    Width = m_dblWidth
End Property

Public Property Let CodAtributo(ByVal dato As String)
    m_sCodAtrib = dato
End Property

Public Property Get CodAtributo() As String
    CodAtributo = m_sCodAtrib
End Property

Public Property Let DenAtributo(ByVal dato As String)
    m_sDenAtrib = dato
End Property

Public Property Get DenAtributo() As String
    DenAtributo = m_sDenAtrib
End Property

Friend Property Set Conexion(ByVal con As CConexion)
    Set m_oConexion = con
End Property

Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property

Public Property Let Posicion(ByVal dato As Integer)
    m_iPosicion = dato
End Property

Public Property Get Posicion() As Integer
    Posicion = m_iPosicion
End Property

Public Property Let Indice(ByVal vIndice As Variant)
    m_vIndice = vIndice
End Property
Public Property Get Indice() As Variant
    Indice = m_vIndice
End Property

Private Sub Class_Terminate()
On Error GoTo Error_Cls:
    Set m_oConexion = Nothing
    Exit Sub
Error_Cls:
    Resume Next
End Sub

Private Function IBaseDatos_AnyadirABaseDatos() As TipoErrorSummit
Dim sConsulta As String
Dim TESError As TipoErrorSummit

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
    
    TESError.NumError = TESnoerror
    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
    
    sConsulta = "INSERT INTO CONF_VISTAS_ITEM_ATRIB (ANYO,GMN1,PROCE,GRUPO,ATRIB,USU,VISTA,VISIBLE,POS,WIDTH)"
    sConsulta = sConsulta & " VALUES (" & m_oConfVista.Grupo.Proceso.Anyo & ",'" & DblQuote(m_oConfVista.Grupo.Proceso.GMN1Cod) & "'," & m_oConfVista.Grupo.Proceso.Cod
    sConsulta = sConsulta & "," & (m_oConfVista.Grupo.Id) & "," & m_lAtrib
    sConsulta = sConsulta & ",'" & DblQuote(m_oConfVista.Usuario) & "'," & m_oConfVista.Vista
    sConsulta = sConsulta & "," & BooleanToSQLBinary(m_bVisible) & "," & NullToDbl0(m_iPosicion) & "," & DblToSQLFloat(m_dblWidth) & ")"
    
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    IBaseDatos_AnyadirABaseDatos = TESError
    Exit Function
    
Error_Cls:
    
    IBaseDatos_AnyadirABaseDatos = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      'Call g_oErrores.TratarError("Class Module", "CConfVistaItemAtrib", "IBaseDatos_AnyadirABaseDatos", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If

End Function

Private Function IBaseDatos_CambiarCodigo(CodigoNuevo As Variant) As TipoErrorSummit

End Function

Private Sub IBaseDatos_CancelarEdicion()

End Sub

Private Function IBaseDatos_ComprobarExistenciaEnBaseDatos() As Boolean
    Dim sConsulta As String
    Dim AdoRes As adodb.Recordset
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sConsulta = "SELECT * FROM CONF_VISTAS_ITEM_ATRIB WITH (NOLOCK) WHERE ANYO=" & m_oConfVista.Grupo.Proceso.Anyo & " AND GMN1='" & DblQuote(m_oConfVista.Grupo.Proceso.GMN1Cod) & "'"
    sConsulta = sConsulta & " AND PROCE=" & m_oConfVista.Grupo.Proceso.Cod & " AND GRUPO=" & (m_oConfVista.Grupo.Id)
    sConsulta = sConsulta & " AND USU='" & DblQuote(m_oConfVista.Usuario) & "' AND VISTA=" & m_oConfVista.Vista
    sConsulta = sConsulta & " AND ATRIB=" & (m_lAtrib)

    Set AdoRes = New adodb.Recordset
    AdoRes.CursorLocation = adUseServer
    AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    If AdoRes.eof Then
        'Si se ha eliminado el atributo en base de datos que no lo inserte (para el caso de que
        'se est� trabajando sin restaurar los datos.)
        AdoRes.Close
        sConsulta = "SELECT * FROM PROCE_ATRIB WITH (NOLOCK) WHERE ID=" & m_lAtrib
        AdoRes.CursorLocation = adUseServer
        AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
        If AdoRes.eof Then
            IBaseDatos_ComprobarExistenciaEnBaseDatos = True
        Else
            IBaseDatos_ComprobarExistenciaEnBaseDatos = False
        End If
    Else
        IBaseDatos_ComprobarExistenciaEnBaseDatos = True
    End If

    AdoRes.Close
    Set AdoRes = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CConfVistaItemAtrib", "IBaseDatos_ComprobarExistenciaEnBaseDatos", ERR, Erl)
      Exit Function
   End If
End Function

Private Function IBaseDatos_EliminarDeBaseDatos() As TipoErrorSummit

End Function

Private Function IBaseDatos_FinalizarEdicionEliminando() As TipoErrorSummit

End Function

Private Function IBaseDatos_FinalizarEdicionModificando() As TipoErrorSummit

End Function

Private Function IBaseDatos_IniciarEdicion(Optional ByVal Bloquear As Boolean, Optional ByVal UsuarioBloqueo As String) As TipoErrorSummit

End Function

Public Function VistaAnyadirABaseDatos(ByVal iVista As Integer, ByVal sUsuario As String) As TipoErrorSummit
Dim sConsulta As String
Dim TESError As TipoErrorSummit

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
    
    TESError.NumError = TESnoerror
    
    sConsulta = "INSERT INTO CONF_VISTAS_ITEM_ATRIB (ANYO,GMN1,PROCE,GRUPO,ATRIB,USU,VISTA,VISIBLE,POS,WIDTH)"
    sConsulta = sConsulta & " VALUES (" & m_oConfVista.Grupo.Proceso.Anyo & ",'" & DblQuote(m_oConfVista.Grupo.Proceso.GMN1Cod) & "'," & m_oConfVista.Grupo.Proceso.Cod
    sConsulta = sConsulta & "," & (m_oConfVista.Grupo.Id) & "," & m_lAtrib
    sConsulta = sConsulta & ",'" & DblQuote(sUsuario) & "'," & iVista
    sConsulta = sConsulta & "," & BooleanToSQLBinary(m_bVisible) & "," & NullToDbl0(m_iPosicion) & "," & DblToSQLFloat(m_dblWidth) & ")"
    
    m_oConexion.ADOCon.Execute sConsulta
    VistaAnyadirABaseDatos = TESError
    Exit Function
    
Error_Cls:
    
    VistaAnyadirABaseDatos = basErrores.TratarError(m_oConexion.ADOCon.Errors)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      'Call g_oErrores.TratarError("Class Module", "CConfVistaItemAtrib", "VistaAnyadirABaseDatos", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If

End Function
