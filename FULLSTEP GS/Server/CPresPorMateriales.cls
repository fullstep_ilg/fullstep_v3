VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CPresPorMateriales"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CPresupuestosPorMaterial **********************************
'*             Autor : Javier Arana
'*             Creada : 3/2/99
'****************************************************************


Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum
Private mCol As Collection
Private mvarConexion As CConexion
Private mvarConexionServer As CConexionDeUseServer

''' <summary>
''' Carga de los datos de presupuestos
''' </summary>
''' <param optional name="Anyo">A�o</param>
''' <param optional name="CodGMN1">Cod GMN1</param>
''' <param optional name="CodGMN2">Cod GMN2</param>
''' <param optional name="CodGMN3">Cod GMN3</param>
''' <param optional name="CodGMN4">Cod GMN4</param>
'''<param optional name="CaracteresInicialesCod">C�digo o primeros caracteres del c�digo</param>
'''<param optional name="CaracteresInicialesDen">Denominaci�n o primeros caracteres de la denominaci�n</param>
'''<param optional name="CoincidenciaTotal">Determina si usamos como criterio de selecci�n el c�digo y la denominaci�n o los primeros caracteres de ambos</param>
'''<param optional name="OrdenadosPorAnyo">Ordena por A�o</param>
'''<param optional name="OrdenadosPorGMN1">Ordena por GMN1</param>
'''<param optional name="OrdenadosPorGMN2">Ordena por GMN2</param>
'''<param optional name="OrdenadosPorGMN3">Ordena por GMN3</param>
'''<param optional name="OrdenadosPorGMN4">Ordena por GMN4</param>
'''<param optional name="OrdenadosPorDen">Ordena por Denomninaci�n</param>
'''<param optional name="OrdenadosPorPres">Ordena por Presupuesto</param>
'''<param optional name="OrdenadosPorObj">Ordena por Obj</param>
'''<param optional name="CodEqpComprador">C�d. eqp. comprador</param>
'''<param optional name="CodComprador">C�d. comprador</param>
'''<param optional name="UsarIndice">Si se usa �ndice</param>
''' <returns>Double</returns>
''' <remarks>Llamada desde: FSGSClient
''' Tiempo m�ximo: 1 sec </remarks>
''' <revision>JVS 03/01/2012</revision>
Public Sub CargarTodosLosPresupuestos(Optional ByVal Anyo As Integer, Optional ByVal CodGMN1 As String, Optional ByVal CodGMN2 As String, Optional ByVal CodGMN3 As String, Optional ByVal CodGMN4 As String, Optional ByVal CaracteresInicialesCod As String, Optional ByVal CaracteresInicialesDen As String, Optional ByVal CoincidenciaTotal As Boolean, Optional ByVal OrdenadosPorAnyo As Boolean, Optional ByVal OrdenadosPorGMN1 As Boolean, Optional ByVal OrdenadosPorGMN2 As Boolean, Optional ByVal OrdenadosPorGMN3 As Boolean, Optional ByVal OrdenadosPorGMN4 As Boolean, Optional ByVal OrdenadosPorDen As Boolean, Optional ByVal OrdenadosPorPres As Boolean, Optional ByVal OrdenadosPorObj As Boolean, Optional ByVal CodEqpComprador As String, Optional ByVal CodComprador As String, Optional ByVal UsarIndice As Boolean)
Dim sConsulta As String
Dim lIndice As Long
Dim rs As New adodb.Recordset
Dim fldAnyo As adodb.Field  'Anyo
Dim fldCod As adodb.Field   'Cod
Dim fldDen As adodb.Field   'Den
Dim fldPres As adodb.Field  'Pres
Dim fldObj As adodb.Field   'Obj
Dim fldGmn1 As adodb.Field  'Gmn1
Dim fldGmn2 As adodb.Field  'Gmn2
Dim fldGmn3 As adodb.Field  'Gmn3

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
Select Case basParametros.gParametrosGenerales.giNEM

    Case 1
    
    Case 2
    
    Case 3
    
    Case 4 'Cuatro niveles de material
            
            'Hay restriccion al material del comprador
                        
            If Trim(CodEqpComprador) <> "" And Trim(CodComprador) <> "" Then
            
                sConsulta = "SELECT PRES_MAT4.ANYO AS ANYO,PRES_MAT4.PRES, PRES_MAT4.OBJ,PRES_MAT4.FECACT, GMN4.GMN1 AS GMN1 ,GMN4.GMN2 AS GMN2 ,GMN4.GMN3 AS GMN3 ,GMN4.COD AS COD ,GMN4." & DevolverDenGMN & " FROM (GMN4 WITH(NOLOCK) INNER JOIN COM_GMN4 WITH(NOLOCK) ON GMN4.GMN1=COM_GMN4.GMN1 AND GMN4.GMN2=COM_GMN4.GMN2 AND GMN4.GMN3=COM_GMN4.GMN3 AND GMN4.COD=COM_GMN4.GMN4 AND COM_GMN4.EQP='" & DblQuote(CodEqpComprador) & "' AND COM_GMN4.COM='" & DblQuote(CodComprador) & "' ) LEFT JOIN PRES_MAT4 WITH(NOLOCK) ON PRES_MAT4.GMN4 = GMN4.COD AND PRES_MAT4.GMN3 = GMN4.GMN3 AND PRES_MAT4.GMN2 = GMN4.GMN2 AND PRES_MAT4.GMN1 = GMN4.GMN1 "
                
                If Not IsEmpty(Anyo) Then
                    sConsulta = sConsulta & " AND ANYO=" & Anyo
                End If
                
                sConsulta = sConsulta & " WHERE 1=1 "
              
            Else
            
                sConsulta = "SELECT PRES_MAT4.ANYO AS ANYO,PRES_MAT4.PRES, PRES_MAT4.OBJ, GMN4.GMN1 AS GMN1 ,GMN4.GMN2 AS GMN2 ,GMN4.GMN3 AS GMN3 ,GMN4.COD AS COD ,GMN4." & DevolverDenGMN & " FROM GMN4 WITH(NOLOCK) LEFT JOIN PRES_MAT4 WITH(NOLOCK) ON PRES_MAT4.GMN4 = GMN4.COD AND PRES_MAT4.GMN3 = GMN4.GMN3 AND PRES_MAT4.GMN2 = GMN4.GMN2 AND PRES_MAT4.GMN1 = GMN4.GMN1 "
                
                If Not IsEmpty(Anyo) Then
                    sConsulta = sConsulta & " AND ANYO=" & Anyo
                End If
                
                sConsulta = sConsulta & " WHERE 1=1 "
                
            End If
            
                If CaracteresInicialesCod = "" And CaracteresInicialesDen = "" Then
                    
                    
                Else
                    If Not (CaracteresInicialesCod = "") And Not (CaracteresInicialesDen = "") Then
                        
                        If CoincidenciaTotal Then
                            
                            sConsulta = sConsulta & " AND COD ='" & DblQuote(CaracteresInicialesCod) & "'"
                            sConsulta = sConsulta & " AND " & DevolverDenGMN & "='" & DblQuote(CaracteresInicialesDen) & "'"
                            
                        Else
                            
                            
                            sConsulta = sConsulta & " AND COD LIKE '" & DblQuote(CaracteresInicialesCod) & "%'"
                            sConsulta = sConsulta & " AND " & DevolverDenGMN & " LIKE '" & DblQuote(CaracteresInicialesDen) & "%'"
                                       
                        End If
                                    
                    Else
                        
                        If Not (CaracteresInicialesCod = "") Then
                            
                            If CoincidenciaTotal Then
                            
                                sConsulta = sConsulta & " AND COD ='" & DblQuote(CaracteresInicialesCod) & "'"
                                
                            Else
                                    
                                sConsulta = sConsulta & " AND COD LIKE '" & DblQuote(CaracteresInicialesCod) & "%'"
                                
                            End If
                            
                        Else
                            
                            If CoincidenciaTotal Then
                            
                                sConsulta = sConsulta & " AND " & DevolverDenGMN & "='" & DblQuote(CaracteresInicialesDen) & "'"
                                
                            Else
                           
                                sConsulta = sConsulta & " AND " & DevolverDenGMN & " LIKE '" & DblQuote(CaracteresInicialesDen) & "%'"
                                  
                            End If
                            
                        End If
                    
                    End If
                           
                End If
                
                
                If Trim(CodGMN1) <> "" Then
                    sConsulta = sConsulta & " AND GMN4.GMN1='" & CodGMN1 & "'"
                End If
                
                If Trim(CodGMN2) <> "" Then
                    sConsulta = sConsulta & " AND GMN4.GMN2='" & CodGMN2 & "'"
                End If
                
                If Trim(CodGMN3) <> "" Then
                    sConsulta = sConsulta & " AND GMN4.GMN3='" & CodGMN3 & "'"
                End If
                
                If Trim(CodGMN4) <> "" Then
                    sConsulta = sConsulta & " AND GMN4.COD='" & CodGMN4 & "'"
                End If
                
                If OrdenadosPorAnyo Then
                    
                    sConsulta = sConsulta & " ORDER BY ANYO,COD"
                    
                Else
                
                    If OrdenadosPorGMN1 Then
                        
                        sConsulta = sConsulta & " ORDER BY GMN1,COD"
                    Else
                        
                        If OrdenadosPorGMN2 Then
                        
                            sConsulta = sConsulta & " ORDER BY GMN2,COD"
                        Else
                            
                            If OrdenadosPorGMN3 Then
                        
                                sConsulta = sConsulta & " ORDER BY GMN3,COD"
                    
                            Else
                                
                                If OrdenadosPorGMN4 Then
                        
                                    sConsulta = sConsulta & " ORDER BY GMN4,COD"
                    
                                Else
                                    
                                    If OrdenadosPorPres Then
                                        
                                        sConsulta = sConsulta & " ORDER BY PRES,COD"
                                    
                                    Else
                                            
                                        If OrdenadosPorObj Then
                                                
                                                sConsulta = sConsulta & " ORDER BY OBJ,COD"
                                        Else
                                            
                                            If OrdenadosPorDen Then
                                                
                                                sConsulta = sConsulta & " ORDER BY " & DevolverDenGMN & ",COD"
                                            Else
                                                sConsulta = sConsulta & " ORDER BY COD," & DevolverDenGMN
                                            
                                            End If
                                        End If
                                    End If
                                End If
                            End If
                        End If
                    End If
                End If
            
            rs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
                  
            If rs.eof Then
                    
                rs.Close
                Set rs = Nothing
                Exit Sub
                  
            Else
                
                Set mCol = Nothing
                Set mCol = New Collection
                    
                Set fldAnyo = rs.Fields("ANYO")
                Set fldCod = rs.Fields("COD")
                Set fldDen = rs.Fields(DevolverDenGMN)
                Set fldPres = rs.Fields("PRES")
                Set fldObj = rs.Fields("OBJ")
                Set fldGmn1 = rs.Fields("GMN1")
                Set fldGmn2 = rs.Fields("GMN2")
                Set fldGmn3 = rs.Fields("GMN3")
                    
                If UsarIndice Then
                    
                    lIndice = 0
                    
                    While Not rs.eof
                        ' Dejamos en manos del programador cuando asignar la conexion al obejto usuario
                        Me.Add fldAnyo.Value, fldCod.Value, fldDen.Value, fldPres.Value, fldObj.Value, fldGmn1.Value, fldGmn2.Value, fldGmn3.Value, fldCod.Value, lIndice
                        rs.MoveNext
                        lIndice = lIndice + 1
                    Wend
                    
                Else
                    
                    While Not rs.eof
                        ' Dejamos en manos del programador cuando asignar la conexion al obejto usuario
                        Me.Add fldAnyo.Value, fldCod.Value, fldDen.Value, fldPres.Value, fldObj.Value, fldGmn1.Value, fldGmn2.Value, fldGmn3.Value, fldCod.Value
                        rs.MoveNext
                    Wend
                End If
                
                rs.Close
                Set rs = Nothing
                Set fldAnyo = Nothing
                Set fldCod = Nothing
                Set fldDen = Nothing
                Set fldPres = Nothing
                Set fldObj = Nothing
                Set fldGmn1 = Nothing
                Set fldGmn2 = Nothing
                Set fldGmn3 = Nothing
                        
                Exit Sub
                  
            End If

    
End Select
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CPresPorMateriales", "CargarTodosLosPresupuestos", ERR, Erl)
      Exit Sub
   End If

End Sub
Public Function Add(ByVal Anyo As Variant, ByVal Cod As String, ByVal Den As String, ByVal Presupuesto As Variant, ByVal Objetivo As Variant, Optional ByVal CodGMN1 As String, Optional ByVal CodGMN2 As String, Optional ByVal CodGMN3 As String, Optional ByVal CodGMN4 As String, Optional ByVal varIndice As Variant) As CPresPorMaterial
    'create a new object
    Dim objnewmember As CPresPorMaterial
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New CPresPorMaterial
   
    objnewmember.Anyo = Anyo
    objnewmember.CodGMN1 = CodGMN1
    objnewmember.CodGMN2 = CodGMN2
    objnewmember.CodGMN3 = CodGMN3
    objnewmember.CodGMN4 = CodGMN4
    objnewmember.Cod = Cod
    objnewmember.Den = Den
    objnewmember.Presupuesto = Presupuesto
    objnewmember.Objetivo = Objetivo
    Set objnewmember.Conexion = mvarConexion
    Set objnewmember.ConexionServer = mvarConexionServer
    
    If Not IsMissing(varIndice) And Not IsNull(varIndice) Then
        objnewmember.Indice = varIndice
        mCol.Add objnewmember, CStr(varIndice)
    Else
    'set the properties passed into the method
       mCol.Add objnewmember, CStr(Cod)
    End If
    
    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CPresPorMateriales", "Add", ERR, Erl)
      Exit Function
   End If

End Function

Public Property Get Item(vntIndexKey As Variant) As CPresPorMaterial
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Friend Property Set Conexion(ByVal con As CConexion)
Set mvarConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = mvarConexion
End Property

Friend Property Get ConexionServer() As CConexionDeUseServer
Set ConexionServer = mvarConexionServer
End Property

Friend Property Set ConexionServer(ByVal con As CConexionDeUseServer)
Set mvarConexionServer = con
End Property

Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
     Count = mCol.Count
End If


End Property


Public Sub Remove(vntIndexKey As Variant)
   
    mCol.Remove vntIndexKey
End Sub


Public Property Get NewEnum() As IUnknown
     Set NewEnum = mCol.[_NewEnum]
End Property


Private Sub Class_Initialize()

    

    Set mCol = New Collection
End Sub


Private Sub Class_Terminate()
    'destroys collection when this class is terminated
    Set mCol = Nothing
    Set mvarConexion = Nothing
    Set mvarConexionServer = Nothing
End Sub


''' <summary>
''' Obtener la estructura de materiales
''' </summary>
''' <param optional name="Anyo">A�o</param>
'''<param optional name="CodEqpComprador">C�d. eqp. comprador</param>
'''<param optional name="CodComprador">C�d. comprador</param>
'''<param optional name="MostrarEstadoIntegracion">Si se muestra el estado de LOG_GRAL</param>
''' <returns>recordset de la estructura de materiales</returns>
''' <remarks>Llamada desde: frmSELMat.MostrarMateriales;
''' Tiempo m�ximo: 1 sec </remarks>
''' <revision>JVS 03/01/2012</revision>
Public Function DevolverEstructuraDeMateriales(ByVal Anyo As Long, Optional ByVal CodEqpComprador As String, Optional ByVal CodComprador As String, Optional ByVal MostrarEstadoIntegracion As Boolean = False) As adodb.Recordset
Dim oadorecordset As adodb.Recordset
Dim oADOConnection As adodb.Connection
Dim sql As String

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oADOConnection = New adodb.Connection
    oADOConnection.Open "Provider=MSDataShape.1;Persist Security Info=True;Data Source=" & basParametros.ServidorGS & ";User ID=" & LICDBLogin & ";Password=" & LICDBContra & ";Initial Catalog=" & basParametros.BaseDatosGS & ";Data Provider=SQLOLEDB.1"
    
    oADOConnection.CursorLocation = adUseClient
    oADOConnection.CommandTimeout = 120
    
    Set oadorecordset = New adodb.Recordset
    
    If Trim(CodEqpComprador) <> "" And Trim(CodComprador) <> "" Then  'Con restricci�n de material:
        'gmn1:
        sql = "SHAPE {SELECT DISTINCT GMN1.COD, GMN1." & DevolverDenGMN & " AS DEN, PRES_MAT1.PRES, PRES_MAT1.OBJ FROM GMN1 WITH(NOLOCK) " & _
            " INNER JOIN COM_GMN4 WITH(NOLOCK) ON GMN1.COD=COM_GMN4.GMN1 AND COM_GMN4.EQP='" & DblQuote(CodEqpComprador) & "' AND COM_GMN4.COM='" & DblQuote(CodComprador) & "' " & _
            " INNER JOIN (SELECT DISTINCT GMN4.GMN1,SUM(PRES_MAT4.PRES) AS PRES,SUM(PRES_MAT4.OBJ)  AS OBJ FROM GMN4 WITH(NOLOCK) " & _
            " INNER JOIN COM_GMN4 WITH(NOLOCK) ON GMN4.COD=COM_GMN4.GMN4 AND GMN4.GMN3=COM_GMN4.GMN3 AND GMN4.GMN2=COM_GMN4.GMN2 AND GMN4.GMN1=COM_GMN4.GMN1 AND COM_GMN4.EQP='" & DblQuote(CodEqpComprador) & "' AND COM_GMN4.COM='" & DblQuote(CodComprador) & "' " & _
            " LEFT OUTER JOIN PRES_MAT4 WITH(NOLOCK) ON GMN4.GMN1 = PRES_MAT4.GMN1 AND GMN4.GMN2 = PRES_MAT4.GMN2 AND GMN4.GMN3 = PRES_MAT4.GMN3 AND GMN4.COD = PRES_MAT4.GMN4 AND ANYO=" & Anyo & _
            " GROUP BY GMN4.GMN1) PRES_MAT1 ON GMN1.COD=PRES_MAT1.GMN1 ORDER BY GMN1.COD}  AS gmn1 "
            
        'gmn2:
        sql = sql & " APPEND (( SHAPE {SELECT DISTINCT GMN2.COD, GMN2." & DevolverDenGMN & " AS DEN, PRES_MAT2.PRES, GMN2.GMN1, PRES_MAT2.OBJ " & _
            " FROM GMN2 WITH(NOLOCK)  " & _
            " INNER JOIN COM_GMN4 WITH(NOLOCK) ON GMN2.COD=COM_GMN4.GMN2 AND GMN2.GMN1=COM_GMN4.GMN1 AND COM_GMN4.EQP='" & DblQuote(CodEqpComprador) & "' AND COM_GMN4.COM='" & DblQuote(CodComprador) & "' " & _
            " INNER JOIN (SELECT DISTINCT GMN4.GMN1,GMN4.GMN2,SUM(PRES_MAT4.PRES) AS PRES,SUM(PRES_MAT4.OBJ)  AS OBJ " & _
            " FROM GMN4 WITH(NOLOCK) INNER JOIN COM_GMN4 WITH(NOLOCK) ON GMN4.COD=COM_GMN4.GMN4 AND GMN4.GMN3=COM_GMN4.GMN3 AND GMN4.GMN2=COM_GMN4.GMN2 AND GMN4.GMN1=COM_GMN4.GMN1 AND COM_GMN4.EQP='" & DblQuote(CodEqpComprador) & "' AND COM_GMN4.COM='" & DblQuote(CodComprador) & "' " & _
            " LEFT OUTER JOIN PRES_MAT4 WITH(NOLOCK) ON GMN4.GMN1 = PRES_MAT4.GMN1 AND GMN4.GMN2 = PRES_MAT4.GMN2 AND GMN4.GMN3 = PRES_MAT4.GMN3 AND GMN4.COD = PRES_MAT4.GMN4 AND ANYO=" & Anyo & _
            " GROUP BY GMN4.GMN1,GMN4.GMN2) PRES_MAT2 ON GMN2.GMN1=PRES_MAT2.GMN1 AND GMN2.COD=PRES_MAT2.GMN2  ORDER BY GMN2.COD}  AS gmn2  "
            
        'gmn3:
        sql = sql & " APPEND (( SHAPE {SELECT DISTINCT GMN3.COD, GMN3." & DevolverDenGMN & " AS DEN, GMN3.GMN2, GMN3.GMN1, PRES_MAT3.PRES, PRES_MAT3.OBJ  " & _
            " FROM GMN3 WITH(NOLOCK)  " & _
            " INNER JOIN COM_GMN4 WITH(NOLOCK) ON GMN3.COD=COM_GMN4.GMN3 AND GMN3.GMN2=COM_GMN4.GMN2 AND GMN3.GMN1=COM_GMN4.GMN1 AND COM_GMN4.EQP='" & DblQuote(CodEqpComprador) & "' AND COM_GMN4.COM='" & DblQuote(CodComprador) & "' " & _
            " INNER JOIN (SELECT DISTINCT GMN4.GMN1,GMN4.GMN2,GMN4.GMN3,SUM(PRES_MAT4.PRES) AS PRES,SUM(PRES_MAT4.OBJ)  AS OBJ " & _
            " FROM GMN4 WITH(NOLOCK) INNER JOIN COM_GMN4 WITH(NOLOCK) ON GMN4.COD=COM_GMN4.GMN4 AND GMN4.GMN3=COM_GMN4.GMN3 AND GMN4.GMN2=COM_GMN4.GMN2 AND GMN4.GMN1=COM_GMN4.GMN1 AND COM_GMN4.EQP='" & DblQuote(CodEqpComprador) & "' AND COM_GMN4.COM='" & DblQuote(CodComprador) & "' " & _
            " LEFT OUTER JOIN PRES_MAT4 WITH(NOLOCK) ON GMN4.GMN1 = PRES_MAT4.GMN1 AND GMN4.GMN2 = PRES_MAT4.GMN2 AND GMN4.GMN3 = PRES_MAT4.GMN3 AND GMN4.COD = PRES_MAT4.GMN4 AND ANYO=" & Anyo & _
            " GROUP BY GMN4.GMN1,GMN4.GMN2,GMN4.GMN3) PRES_MAT3 ON GMN3.GMN1=PRES_MAT3.GMN1 AND GMN3.GMN2=PRES_MAT3.GMN2 AND GMN3.COD=PRES_MAT3.GMN3 ORDER BY GMN3.COD}  AS gmn3  "
            
        'gmn4:
        sql = sql & " APPEND (( SHAPE {SELECT DISTINCT GMN4.COD, GMN4.GMN2, GMN4.GMN1, GMN4." & DevolverDenGMN & " AS DEN, GMN4.GMN3, PRES_MAT4.PRES, PRES_MAT4.OBJ, PRES_MAT4.PRESDIRECTA, PRES_MAT4.ANYO  " & _
            " FROM GMN4 WITH(NOLOCK)  " & _
            " INNER JOIN COM_GMN4 WITH(NOLOCK) ON GMN4.COD=COM_GMN4.GMN4 AND GMN4.GMN3=COM_GMN4.GMN3 AND GMN4.GMN2=COM_GMN4.GMN2 AND GMN4.GMN1=COM_GMN4.GMN1 AND COM_GMN4.EQP='" & DblQuote(CodEqpComprador) & "' AND COM_GMN4.COM='" & DblQuote(CodComprador) & "' " & _
            " LEFT OUTER JOIN PRES_MAT4 WITH(NOLOCK) ON GMN4.GMN1 = PRES_MAT4.GMN1 AND GMN4.GMN2 = PRES_MAT4.GMN2 AND GMN4.GMN3 = PRES_MAT4.GMN3 AND GMN4.COD = PRES_MAT4.GMN4 AND ANYO=" & Anyo & " ORDER BY GMN4.COD}  AS gmn4  "
        
    Else
        'Sin restricciones:
        sql = "SHAPE {SELECT GMN1.COD, GMN1." & DevolverDenGMN & " AS DEN, PRES_MAT1.PRES, PRES_MAT1.OBJ FROM GMN1 WITH(NOLOCK) " & _
            " LEFT OUTER JOIN PRES_MAT1 WITH(NOLOCK) ON GMN1.COD = PRES_MAT1.GMN1 AND ANYO=" & Anyo & " ORDER BY GMN1.COD}  AS gmn1 " & _
            " APPEND (( SHAPE {SELECT GMN2.COD, GMN2." & DevolverDenGMN & " AS DEN, PRES_MAT2.PRES, GMN2.GMN1, PRES_MAT2.OBJ  " & _
            " FROM GMN2 WITH(NOLOCK)  " & _
            " LEFT OUTER JOIN PRES_MAT2 WITH(NOLOCK) ON GMN2.GMN1 = PRES_MAT2.GMN1 AND GMN2.COD = PRES_MAT2.GMN2 AND ANYO=" & Anyo & "  ORDER BY GMN2.COD}  AS gmn2  " & _
            " APPEND (( SHAPE {SELECT GMN3.COD, GMN3." & DevolverDenGMN & " AS DEN, GMN3.GMN2, GMN3.GMN1, PRES_MAT3.PRES, PRES_MAT3.OBJ  " & _
            " FROM GMN3 WITH(NOLOCK)  " & _
            " LEFT OUTER JOIN PRES_MAT3 WITH(NOLOCK) ON GMN3.GMN1 = PRES_MAT3.GMN1 AND GMN3.GMN2 = PRES_MAT3.GMN2 AND GMN3.COD = PRES_MAT3.GMN3 AND ANYO=" & Anyo & "  ORDER BY GMN3.COD}  AS gmn3  " & _
            " APPEND (( SHAPE {SELECT GMN4.COD, GMN4.GMN2, GMN4.GMN1, GMN4." & DevolverDenGMN & " AS DEN, GMN4.GMN3, PRES_MAT4.PRES, PRES_MAT4.OBJ, PRES_MAT4.PRESDIRECTA, PRES_MAT4.ANYO  " & _
            " FROM GMN4 WITH(NOLOCK)  " & _
            " LEFT OUTER JOIN PRES_MAT4 WITH(NOLOCK) ON GMN4.GMN1 = PRES_MAT4.GMN1 AND GMN4.GMN2 = PRES_MAT4.GMN2 AND GMN4.GMN3 = PRES_MAT4.GMN3 AND GMN4.COD = PRES_MAT4.GMN4 AND ANYO=" & Anyo & "  ORDER BY GMN4.COD}  AS gmn4  "
    End If
    
    'art�culos:
    sql = sql & " APPEND (( SHAPE {SELECT ART4.GMN1, ART4.GMN2, ART4.GMN3, ART4.GMN4, ART4.COD, ART4.DEN, ART4.UNI, PRES_ART.CANT, PRES_ART.PRES, PRES_ART.OBJ, PRES_ART.ANYO "
    If MostrarEstadoIntegracion = True Then
        sql = sql & " ,CASE WHEN RIGHT(MAX(convert(varchar,LOG_GRAL.FECACT,121)+LOG_GRAL.ESTADO),1) is null then 4 else RIGHT(MAX(convert(varchar,LOG_GRAL.FECACT,121)+LOG_GRAL.ESTADO),1) END AS ESTADO" & _
        " FROM ART4 WITH(NOLOCK)  " & _
        " LEFT OUTER JOIN PRES_ART WITH(NOLOCK) ON ART4.COD = PRES_ART.COD AND ANYO=" & Anyo & _
        " LEFT JOIN (LOG_PRES_ART WITH(NOLOCK) INNER JOIN LOG_GRAL WITH(NOLOCK) ON LOG_PRES_ART.ID = LOG_GRAL.ID_TABLA AND LOG_GRAL.TABLA =" & EntidadIntegracion.PresArt & _
        " AND (LOG_GRAL.ORIGEN = " & OrigenIntegracion.FSGSInt & " OR LOG_GRAL.ORIGEN = " & OrigenIntegracion.FSGSIntReg & ")" & _
        " )ON LOG_PRES_ART.ANYO=PRES_ART.ANYO  AND LOG_PRES_ART.COD=PRES_ART.COD " & _
        " GROUP BY ART4.GMN1, ART4.GMN2, ART4.GMN3, ART4.GMN4, ART4.COD, ART4.DEN, ART4.UNI, PRES_ART.CANT, PRES_ART.PRES, PRES_ART.OBJ, PRES_ART.ANYO }  AS Articulos  "
    Else
        sql = sql & " FROM ART4 WITH(NOLOCK)  " & _
        " LEFT OUTER JOIN PRES_ART WITH(NOLOCK) ON ART4.COD = PRES_ART.COD AND ANYO=" & Anyo & "} AS Articulos "
    End If
        
    'adjudicaciones:
    sql = sql & " APPEND ({SELECT ART4.GMN1, ART4.GMN2, ART4.GMN3, ART4.GMN4,ART4_ADJ.ART, ART4_ADJ.ID, ART4_ADJ.DEST, ART4_ADJ.UNI, ART4_ADJ.CODPROVE, ART4_ADJ.DENPROVE, ART4_ADJ.PREC, ART4_ADJ.PORCEN, ART4_ADJ.FECINI, ART4_ADJ.FECFIN, ART4_ADJ.CANT FROM ART4_ADJ WITH(NOLOCK) INNER JOIN ART4 WITH(NOLOCK) ON ART4_ADJ.ART=ART4.COD WHERE YEAR(FECINI)<=" & Anyo & " ORDER BY FECINI DESC}  AS adj "
    
    sql = sql & " RELATE 'GMN1' TO 'GMN1','GMN2' TO 'GMN2','GMN3' TO 'GMN3','GMN4' TO 'GMN4','COD' TO 'ART') AS adj) AS Articulos " & _
                " RELATE 'GMN1' TO 'GMN1','GMN2' TO 'GMN2','GMN3' TO 'GMN3','COD' TO 'GMN4') AS Articulos) AS gmn4 " & _
                " RELATE 'GMN1' TO 'GMN1','GMN2' TO 'GMN2','COD' TO 'GMN3') AS gmn4) AS gmn3" & _
                " RELATE 'GMN1' TO 'GMN1','COD' TO 'GMN2') AS gmn3) AS gmn2 RELATE 'COD' TO 'GMN1') AS gmn2"
    
    oadorecordset.Open sql, oADOConnection
    
    oadorecordset.ActiveConnection = Nothing
    oADOConnection.Close
    
    Set DevolverEstructuraDeMateriales = oadorecordset
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CPresPorMateriales", "DevolverEstructuraDeMateriales", ERR, Erl)
      Exit Function
   End If
    
End Function


