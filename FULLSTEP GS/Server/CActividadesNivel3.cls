VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CActividadesNivel3"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CActividadesNivel3 **********************************
'*             Autor : Javier Arana
'*             Creada : 18/11/98
'****************************************************************

Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum

Private mCol As Collection
Private mvarConexion As CConexion
Private mvarEOF As Boolean

Public Function Add(ByVal ACN1 As Long, ByVal ACN2 As Long, ByVal Id As Long, ByVal Cod As String, ByVal Den As String, Optional ByVal varIndice As Variant, Optional ByVal ACT1Cod As String, Optional ByVal ACT1Den As String, Optional ByVal ACT2Cod As String, Optional ByVal ACT2Den As String, Optional ByVal numProve As Integer) As CActividadNivel3
    'create a new object
    Dim objnewmember As CActividadNivel3
    Dim sCod As String
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New CActividadNivel3
   
    With objnewmember
        .Cod = Cod
        .Den = Den
        .Id = Id
        .ACN1 = ACN1
        .ACN2 = ACN2
        .CodACT1 = ACT1Cod
        .DenACT1 = ACT1Den
        .CodACT2 = ACT2Cod
        .DenACT2 = ACT2Den
        .numProve = numProve
    End With
    
    Set objnewmember.Conexion = mvarConexion
    
    If Not IsMissing(varIndice) And Not IsNull(varIndice) Then
        objnewmember.Indice = varIndice
        mCol.Add objnewmember, CStr(varIndice)
    Else
        sCod = CStr(ACN1) & Mid$("                         ", 1, 10 - Len(CStr(ACN1)))
        sCod = sCod & CStr(ACN2) & Mid$("                         ", 1, 10 - Len(CStr(ACN2)))
        sCod = sCod & CStr(Id) & Mid$("                         ", 1, 10 - Len(CStr(Id)))
        mCol.Add objnewmember, sCod
    End If
      
    Set Add = objnewmember
    Set objnewmember = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CActividadesNivel3", "Add", ERR, Erl)
        Exit Function
    End If

End Function

Public Property Get Item(vntIndexKey As Variant) As CActividadNivel3
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Friend Property Set Conexion(ByVal con As CConexion)
Set mvarConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = mvarConexion
End Property


Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
     Count = mCol.Count
End If


End Property


Public Sub Remove(vntIndexKey As Variant)
    mCol.Remove vntIndexKey
End Sub


Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
     Set NewEnum = mCol.[_NewEnum]
End Property


Private Sub Class_Initialize()

        Set mCol = New Collection
End Sub


Private Sub Class_Terminate()
    'destroys collection when this class is terminated
    Set mCol = Nothing
    Set mvarConexion = Nothing
End Sub

Public Function DevolverLosCodigos() As TipoDatosCombo
   
   Dim Codigos As TipoDatosCombo
   Dim iCont As Integer
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
   ReDim Codigos.Cod(mCol.Count)
   ReDim Codigos.Den(mCol.Count)
   
    For iCont = 0 To mCol.Count - 1
        
        Codigos.Cod(iCont) = mCol(iCont + 1).Cod
        Codigos.Den(iCont) = mCol(iCont + 1).Den
    
    Next

   DevolverLosCodigos = Codigos
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CActividadesNivel3", "DevolverLosCodigos", ERR, Erl)
        Exit Function
    End If
   
End Function

''' <summary>Carga en la coleccion las actividades de Nivel3</summary>
''' <param name="Prove">el codigo de un proveedor</param>
''' <param name="Idioma">el Idioma en el que se devolvera la denominacion</param>
''' <param name="OrdPorDen">si es True se ordena por Denominacion sino por Cod</param>
''' <param name="UsarIndice">si es True la coleccion se cargara con un indice correlativo, sino el indice sera el ID de la actividad</param>
''' <remarks>Llamada desde: FSGClient Tiempo m�ximo: 1 sec </remarks>
''' <revision>LTG 22/12/2011</revision>

Public Sub CargarActividadesN3Proveedor(ByVal Prove As Long, Optional ByVal Idioma As String, Optional ByVal OrdPorDen As Boolean, Optional ByVal UsarIndice As Boolean)
    Dim rs As New adodb.Recordset
    Dim fldAct1Id As adodb.Field
    Dim fldAct2Id As adodb.Field
    Dim fldAct3Id As adodb.Field
    Dim fldAct1Cod As adodb.Field
    Dim fldAct2Cod As adodb.Field
    Dim fldAct3Cod As adodb.Field
    Dim fldAct1sDen As adodb.Field
    Dim fldAct2sDen As adodb.Field
    Dim fldAct3sDen As adodb.Field
    Dim lIndice As Long
    Dim sDen As String
    Dim sFSP As String
    Dim sConsulta As String
    
    '********* Precondicion **************************************
    If mvarConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CDestinos.CargarTodosLosDestinos", "No se ha establecido la conexion"
        Exit Sub
    End If
    '*************************************************************
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

    If Idioma = "" Then
        sDen = "DEN_" & basParametros.gParametrosGenerales.gIdiomaPortal
    Else
        sDen = "DEN_" & Idioma
    End If
    
    sFSP = basParametros.gParametrosGenerales.gsFSP_SRV & "." & basParametros.gParametrosGenerales.gsFSP_BD & ".dbo."
 
    sConsulta = "SELECT ACT3.ACT1, ACT1.COD, ACT1." & sDen & ","
    sConsulta = sConsulta & " ACT3.ACT2, ACT2.COD, ACT2." & sDen & ","
    sConsulta = sConsulta & " ACT3.ID, ACT3.COD as Cod3, ACT3." & sDen & " AS sDen3"
    'CALIDAD: No se usa WITH (NOLOCK) porque es una consulta a otra BD
    sConsulta = sConsulta & " FROM " & sFSP & "CIAS_ACT3 AS CIAS_ACT3"
    sConsulta = sConsulta & " INNER JOIN " & sFSP & "ACT3 AS ACT3 ON"
    sConsulta = sConsulta & " ACT3.ACT1 = CIAS_ACT3.ACT1 AND"
    sConsulta = sConsulta & " ACT3.ACT2 = CIAS_ACT3.ACT2 AND"
    sConsulta = sConsulta & " ACT3.ID = CIAS_ACT3.ACT3"
    sConsulta = sConsulta & " INNER JOIN " & sFSP & "ACT2 AS ACT2 ON"
    sConsulta = sConsulta & " ACT3.ACT1 = ACT2.ACT1 AND"
    sConsulta = sConsulta & " ACT3.ACT2 = ACT2.ID"
    sConsulta = sConsulta & " INNER JOIN " & sFSP & "ACT1 AS ACT1 ON"
    sConsulta = sConsulta & " ACT3.ACT1 = ACT1.ID"
    sConsulta = sConsulta & " WHERE CIAS_ACT3.CIA=" & Prove & " "
        
    If OrdPorDen Then
        sConsulta = sConsulta & "ORDER BY sDen3"
    Else
        sConsulta = sConsulta & "ORDER BY Cod3"
    End If
        
    rs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    
    If Not rs.eof Then
        Set fldAct1Id = rs.Fields(0)
        Set fldAct2Id = rs.Fields(3)
        Set fldAct3Id = rs.Fields(6)
        Set fldAct1Cod = rs.Fields(1)
        Set fldAct2Cod = rs.Fields(4)
        Set fldAct3Cod = rs.Fields(7)
        Set fldAct1sDen = rs.Fields(2)
        Set fldAct2sDen = rs.Fields(5)
        Set fldAct3sDen = rs.Fields(8)
     
        Set mCol = Nothing
        Set mCol = New Collection
           
        If UsarIndice Then
            lIndice = 0
            While Not rs.eof
                Me.Add fldAct1Id.Value, fldAct2Id.Value, fldAct3Id.Value, fldAct3Cod.Value, fldAct3sDen.Value, lIndice, fldAct1Cod.Value, fldAct1sDen, fldAct2Cod.Value, fldAct2sDen.Value
                rs.MoveNext
                lIndice = lIndice + 1
            Wend
        Else
            While Not rs.eof
                Me.Add fldAct1Id.Value, fldAct2Id.Value, fldAct3Id.Value, fldAct3Cod.Value, fldAct3sDen.Value, , fldAct1Cod.Value, fldAct1sDen, fldAct2Cod.Value, fldAct2sDen.Value
                rs.MoveNext
            Wend
        End If
           
        Set fldAct1Id = Nothing
        Set fldAct2Id = Nothing
        Set fldAct3Id = Nothing
        Set fldAct1Cod = Nothing
        Set fldAct2Cod = Nothing
        Set fldAct3Cod = Nothing
        Set fldAct1sDen = Nothing
        Set fldAct3sDen = Nothing
        Set fldAct3sDen = Nothing
    End If
            
    rs.Close
    Set rs = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CActividadesNivel3", "CargarActividadesN3Proveedor", ERR, Erl)
        Exit Sub
    End If
End Sub

Public Property Get eof() As Boolean
    eof = mvarEOF
End Property
Friend Property Let eof(ByVal b As Boolean)
    mvarEOF = b
End Property



