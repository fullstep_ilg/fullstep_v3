VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CAccionesBloque"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum

Private m_Col As Collection
Private m_oConexion As CConexion
Private m_bEOF As Boolean

Public Property Get eof() As Boolean
    eof = m_bEOF
End Property
Friend Property Let eof(ByVal b As Boolean)
    m_bEOF = b
End Property

Public Property Get Item(vntIndexKey As Variant) As CAccionBloque
On Error GoTo NoSeEncuentra:

    Set Item = m_Col(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Friend Property Set Conexion(ByVal con As CConexion)
Set m_oConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = m_oConexion
End Property

Public Property Get Count() As Long
    If m_Col Is Nothing Then
        Count = 0
    Else
         Count = m_Col.Count
    End If
End Property

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
     Set NewEnum = m_Col.[_NewEnum]
End Property

Public Function Add(ByVal lId As Long, ByVal oDenominaciones As CMultiidiomas, ByVal lBloque As Long, Optional ByVal iTipo As TipoAccionBloque, Optional ByVal bAsignadoARol As Boolean, Optional bCumpOblRol As Boolean, Optional bGuarda As Boolean, Optional iTipoRechazo As TipoRechazoAccionBloque, Optional bSoloRechazadas As Boolean, Optional ByVal dtFecAct As Variant, Optional ByVal vIndice As Variant, Optional ByVal bAprobar As Boolean = False, Optional ByVal bRechazar As Boolean = False) As CAccionBloque
    
    'create a new object
    Dim sCod As String
    Dim objnewmember As CAccionBloque
    
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New CAccionBloque
    With objnewmember
        Set .Conexion = m_oConexion
    
        .Id = lId
        Set .Denominaciones = oDenominaciones
        .Bloque = lBloque
        .CumpOblRol = bCumpOblRol
        .Guarda = bGuarda
        .Tipo = iTipo
        .TipoRechazo = iTipoRechazo
        .SoloRechazadas = bSoloRechazadas
        .FecAct = dtFecAct
        .AsignadoARol = bAsignadoARol
        .Aprobar = bAprobar
        .Rechazar = bRechazar
            
        If Not IsMissing(vIndice) And Not IsNull(vIndice) Then
            .Indice = vIndice
            m_Col.Add objnewmember, CStr(vIndice)
        Else
            m_Col.Add objnewmember, CStr(lId)
        End If
    
    End With
    
    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CAccionesBloque", "Add", ERR, Erl)
      Exit Function
   End If

End Function

Public Sub AddAccionBloque(ByVal oAccion As CAccionBloque, Optional ByVal vIndice As Variant)

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not IsMissing(vIndice) And Not IsNull(vIndice) Then
        oAccion.Indice = vIndice
        m_Col.Add oAccion, CStr(vIndice)
    Else
        m_Col.Add oAccion, CStr(oAccion.Id)
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CAccionesBloque", "AddAccionBloque", ERR, Erl)
      Exit Sub
   End If
End Sub

Public Sub Remove(vntIndexKey As Variant)

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_Col.Remove vntIndexKey

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CAccionesBloque", "Remove", ERR, Erl)
      Exit Sub
   End If

End Sub


Private Sub Class_Initialize()
    Set m_Col = New Collection
End Sub


Private Sub Class_Terminate()
    'Proceroys collection when this class is terminated
    On Error Resume Next
    Set m_Col = Nothing
    Set m_oConexion = Nothing
    
End Sub

Public Function DevolverAcciones(ByVal sIdi As String, Optional ByVal lIdBloque As Long = 0) As adodb.Recordset
Dim oadorecordset As adodb.Recordset
Dim sql As String

    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sql = "SELECT ID, '" & DblQuote(sIdi) & "' AS IDI, ISNULL(DEN,'') AS DEN, BLOQUE, CUMP_OBL_ROL, GUARDA, TIPO, TIPO_RECHAZO, SOLO_RECHAZADAS, FECACT FROM PM_ACCIONES A WITH (NOLOCK)" & _
        "LEFT JOIN PM_ACCIONES_DEN AD WITH (NOLOCK) ON A.ID = AD.ACCION AND IDI = '" & DblQuote(sIdi) & "'"
    If lIdBloque > 0 Then
        sql = sql & "WHERE A.BLOQUE = " & lIdBloque
    End If
        
    Set oadorecordset = New adodb.Recordset
    oadorecordset.CursorLocation = adUseClient
    oadorecordset.Open sql, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    oadorecordset.ActiveConnection = Nothing
        
    Set DevolverAcciones = oadorecordset
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CAccionesBloque", "DevolverAcciones", ERR, Erl)
      Exit Function
   End If
    
End Function


Public Sub CargarAcciones(ByVal sIdi As String, Optional ByVal lIdBloque As Long = 0)
    Dim oRS As adodb.Recordset
    Dim oDenominaciones As CMultiidiomas
    
     
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oRS = DevolverAcciones(sIdi, lIdBloque)
    While Not oRS.eof
        Set oDenominaciones = New CMultiidiomas
        Set oDenominaciones.Conexion = m_oConexion
        oDenominaciones.Add oRS("IDI").Value, oRS("DEN").Value
        Add NullToDbl0(oRS("ID").Value), oDenominaciones, NullToDbl0(oRS("BLOQUE").Value), NullToDbl0(oRS("TIPO").Value), , SQLBinaryToBoolean(oRS("CUMP_OBL_ROL").Value), SQLBinaryToBoolean(oRS("GUARDA").Value), NullToDbl0(oRS("TIPO_RECHAZO").Value), SQLBinaryToBoolean(oRS("SOLO_RECHAZADAS").Value), oRS("FECACT").Value
        oRS.MoveNext
    Wend
    oRS.Close
    Set oRS = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CAccionesBloque", "CargarAcciones", ERR, Erl)
      Exit Sub
   End If
End Sub

Public Sub CargarAccionesBloqueRol(ByVal sIdi As String, ByVal lIdBloque As Long, ByVal lIdRol As Long)
    Dim oRS As adodb.Recordset
    Dim sql As String
    Dim oDenominaciones As CMultiidiomas
    
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sql = "SELECT A.ID, '" & DblQuote(sIdi) & "' AS IDI, ISNULL(AD.DEN,'') AS DEN, A.BLOQUE, CUMP_OBL_ROL, A.GUARDA, A.TIPO, A.TIPO_RECHAZO, A.SOLO_RECHAZADAS, A.FECACT, CASE WHEN RA.ROL = " & lIdRol & " THEN 1 ELSE 0 END AS ASIGNADO " & _
            ", CASE WHEN RA.ROL = " & lIdRol & " THEN RA.APROBAR ELSE A.APROBAR END AS APROBAR, CASE WHEN RA.ROL = " & lIdRol & " THEN RA.RECHAZAR ELSE A.RECHAZAR END AS RECHAZAR " & _
            "FROM PM_ACCIONES A WITH (NOLOCK)" & _
            "LEFT JOIN PM_ROL_ACCION RA WITH (NOLOCK) ON A.ID = RA.ACCION AND A.BLOQUE = RA.BLOQUE AND (RA.ROL = " & lIdRol & " OR RA.ROL IS NULL) " & _
            "LEFT JOIN PM_ACCIONES_DEN AD WITH (NOLOCK) ON A.ID = AD.ACCION AND AD.IDI = '" & DblQuote(sIdi) & "' " & _
            "WHERE A.BLOQUE = " & lIdBloque

    Set oRS = New adodb.Recordset
    oRS.CursorLocation = adUseClient
    oRS.Open sql, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    oRS.ActiveConnection = Nothing
    While Not oRS.eof
        Set oDenominaciones = New CMultiidiomas
        Set oDenominaciones.Conexion = m_oConexion
        oDenominaciones.Add oRS("IDI").Value, oRS("DEN").Value
        Add NullToDbl0(oRS("ID").Value), oDenominaciones, NullToDbl0(oRS("BLOQUE").Value), NullToDbl0(oRS("TIPO").Value), SQLBinaryToBoolean(oRS("ASIGNADO").Value), SQLBinaryToBoolean(oRS("CUMP_OBL_ROL").Value), SQLBinaryToBoolean(oRS("GUARDA").Value), NullToDbl0(oRS("TIPO_RECHAZO").Value), SQLBinaryToBoolean(oRS("SOLO_RECHAZADAS").Value), oRS("FECACT").Value, bAprobar:=(oRS("APROBAR") = 1), bRechazar:=(oRS("RECHAZAR") = 1)
        oRS.MoveNext
    Wend
    oRS.Close
    Set oRS = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CAccionesBloque", "CargarAccionesBloqueRol", ERR, Erl)
      Exit Sub
   End If
End Sub

Public Function DevolverAccionesExternas(ByVal sIdi As String) As adodb.Recordset
    Dim oadorecordset As adodb.Recordset
    Dim sql As String
    
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sql = "SELECT ID, DEN, NOM_XML FROM PM_ACCIONES_EXTERNAS PMAE WITH (NOLOCK)"
    
        
    Set oadorecordset = New adodb.Recordset
    oadorecordset.CursorLocation = adUseClient
    oadorecordset.Open sql, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    oadorecordset.ActiveConnection = Nothing
        
    Set DevolverAccionesExternas = oadorecordset
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CAccionesBloque", "DevolverAccionesExternas", ERR, Erl)
      Exit Function
   End If
    
End Function

