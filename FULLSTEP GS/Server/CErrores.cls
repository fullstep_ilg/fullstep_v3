VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CErrores"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
Private m_sFrmCargado As String
Private m_oConexion As CConexion
Private Declare Function DestroyWindow Lib "user32" (ByVal hwnd As Long) As Long
Private Declare Function IsWindowEnabled Lib "user32" (ByVal hwnd As Long) As Long
Private Declare Function MoveWindow Lib "user32" (ByVal hwnd As Long, ByVal X As Long, ByVal Y As Long, _
                         ByVal nWidth As Long, ByVal nHeight As Long, ByVal bRepaint As Long) As Long
Public m_sTexto1Errores As String
Public m_sTexto2Errores As String
Public m_sTexto3Errores As String
Public m_sTexto4Errores As String
Public g_iProgramando As Integer

Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum

Friend Property Get Programando() As Integer
    Programando = g_iProgramando
End Property

Friend Property Let Programando(ByVal sData As Integer)
    g_iProgramando = sData
End Property

Friend Property Get FrmCargado() As String
    FrmCargado = m_sFrmCargado
End Property

Friend Property Let FrmCargado(ByVal sData As String)
    m_sFrmCargado = sData
End Property
Public Function fg_bProgramando() As Boolean
Dim oFos As FileSystemObject

If g_iProgramando = 1 Then
    fg_bProgramando = True
Else
    fg_bProgramando = False
End If

End Function

''' <summary>
''' Trata un error para grabar la informacion necesaria en la Tabla Errores y despu�s lanzar un msgbox
''' </summary>
''' <param name="sRutaError">Funci�n , pantalla ,evento q da el error</param>
''' <param name="Error">objeto Error</param>
Public Function TratarError(ByVal sTipoObj As String, ByVal sNombreObj As String, ByVal sEventoObj As String, _
                       ByVal ERROR As ErrObject, ByVal LinErr As Long, Optional ByRef oForm As Object = Nothing, _
                       Optional ByRef b_Activado As Boolean = False, Optional sMsgError As String = "") As Boolean
                       
Dim TESError As TipoErrorSummit
Dim vllError As Long
Dim vllDesError As String
Dim sRutaErr As String
Dim sPagina As String

vllError = ERR.Number
vllDesError = ERR.Description
'Se rellena la variable para recoger el camino desde el que viene el error.
If ERR.Source <> "" Then
    sRutaErr = ERR.Source & " - " & sTipoObj & "." & sNombreObj & "." & sEventoObj
Else
    sRutaErr = sTipoObj & "." & sNombreObj & "." & sEventoObj
End If
'En caso de a�adir el n�mero de las lineas, en la informaci�n no dir� la linea del casque
If Erl > 0 Then
    sRutaErr = sRutaErr & " (" & m_sTexto1Errores & ": " & Erl & ")"
End If
sPagina = sTipoObj & "." & sNombreObj & "." & sEventoObj
'si el error es en BD devolvemos un error definido por nosotros
'If m_oConexion.ADOCon.Errors.Count > 0 Then
'    vllError = vbObjectError + 999
'End If
'Si hay una transacci�n abierta la cierro
m_oConexion.ADOCon.Execute "  IF @@TRANCOUNT>0 ROLLBACK TRANSACTION"
'Solo mostramos el mensaje cuando venga de un evento de formulario, la propiedad FrmCargado ti4ene el formulario desde el que viene
'y la constante todos los formularios tratados
'De momento se grabar� siempre el error hasta que se meta bien el control de errores
TESError = GrabarError(sRutaErr, ERR.Number, ERR.Description, sPagina)
If Not oForm Is Nothing Then
    sMsgError = m_sTexto2Errores & vbCrLf & m_sTexto3Errores & ": " & TESError.Arg1 & vbCrLf & m_sTexto4Errores
    sRutaErr = ""
Else
    'Se almacenar� el camino en las clases, modulos(bas), y funciones/procedimientos de los formularios.
    ERR.Raise vllError, sRutaErr, vllDesError
End If
If UCase$(sEventoObj) <> "FORM_LOAD" And UCase$(sEventoObj) <> "FORM_UNLOAD" Then
    'Solo se descargar� el formulario cuando ya est� cargado
    'Se tratar� en los diferentes formularios la descarga del mismo en caso de que el error d� durante la carga
    If Not oForm Is Nothing And b_Activado Then
        On Error GoTo ERROR:
        oForm.m_bDescargarFrm = True
        Unload oForm
        Set oForm = Nothing
        Screen.MousePointer = vbNormal
    End If
    'Se pone siempre a false porque ya marcamos la variable arriba
    TratarError = False
Else
    TratarError = True
End If

'--------------------------------------------------------------------------------------------
    Exit Function
    
ERROR:
    Select Case ERR.Number
        Case 365
            'No se puede descargar el formulario, hay contextos en los que no se puede descar gar el formularios y da este error (365)
            'Para estos casos usaremos esta API
            DestroyWindow oForm.hwnd
    End Select
End Function


''' <summary>
''' Grabar un error en una comunicaci�n
''' </summary>
''' <param name="Pagina">Funci�n o pantalla q da el error</param>
''' <param name="NumError">Numero de Error VB. El Id de la tabla va en teserror.arg1</param>
''' <param name="Message">Mensaje del error VB</param>
''' <returns>Error de haberlo</returns>
''' <remarks>Llamada desde: frmAdjAnya/cmdAceptar_Click frmObjAnya/cmdAceptar_Click frmOfePetAnya/cmdAceptar_Click
'''     basUtilidades/EnviarMensaje     basUtilidades/GrabarEnviarMensaje ; Tiempo m�ximo: 0,2</remarks>
''' Revisado por: jbg; Fecha: 13/06/2013
Public Function GrabarError(ByVal RutaCompleta As String, ByVal NumError As String, ByVal Message As String, ByVal Pagina As String) As TipoErrorSummit
    Dim TESError As TipoErrorSummit
    Dim rs As adodb.Recordset
    
    '********* Precondicion *******************
    If m_oConexion.ADOCon Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CConexion.GrabarError", "No se ha establecido la conexion"
        Exit Function
    End If
    
    On Error GoTo ERROR:
    
    TESError.NumError = TESnoerror
    'm_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
        
    Pagina = "GS: " & Pagina
        
    m_oConexion.ADOCon.Execute "INSERT INTO ERRORES (FEC_ALTA,PAGINA,USUARIO,EX_FULLNAME,EX_MESSAGE,EX_STACKTRACE)" _
        & " VALUES(GETDATE()," & StrToSQLNULL(Pagina) & "," & StrToSQLNULL(basParametros.g_sCodUsuConectado) _
        & "," & StrToSQLNULL(NumError) & "," & StrToSQLNULL(Message) & "," & StrToSQLNULL(RutaCompleta) & ")"
        
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR
    
    Set rs = New adodb.Recordset
    'Buscamos el numero de id maximo
    'CALIDAD: Sin WITH (NOLOCK) porque es para obtener el nuevo ID
    rs.Open "SELECT SCOPE_IDENTITY()", m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    If rs.eof Then
        TESError.NumError = TESDatoEliminado
        'm_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
        rs.Close
        Set rs = Nothing
        GrabarError = TESError
        Exit Function
    Else
        TESError.Arg1 = NullToDbl0(rs(0).Value)
        TESError.Arg2 = Message
    End If
    
    rs.Close
    Set rs = Nothing
    
    'm_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    
    GrabarError = TESError
    
    Exit Function
    
ERROR:
    GrabarError = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    
    'm_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
    
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
End Function


Friend Property Get Conexion() As CConexion
Set Conexion = m_oConexion
End Property


Friend Property Set Conexion(ByVal con As CConexion)
Set m_oConexion = con
End Property


Private Sub Class_Terminate()
Set m_oConexion = Nothing
End Sub


