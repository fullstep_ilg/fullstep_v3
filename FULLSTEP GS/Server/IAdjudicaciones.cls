VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "IAdjudicaciones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Public Function DevolverAdjudicacionesDeProveedor(ByVal Prove As String, Optional ByVal bCerrados As Boolean, Optional ByVal bNoComunicados As Boolean, Optional ByVal bCargarEscalados As Boolean) As CAdjudicaciones

End Function

Public Function DevolverAdjudicacionesDeItemConFecha(ByVal ItemId As Integer, ByVal Fecha As Date) As CAdjudicaciones

End Function

Public Function DevolverAdjudicacionesDeGrupoOItem(ByVal sProve As String, Optional ByVal sGrupo As String, Optional ByVal lItem As Long, Optional ByVal bCerrados As Boolean) As CAdjudicaciones

End Function
Public Function DevolverAdjudicaciones(Optional ByVal UsarIndice As Boolean, Optional ByVal FecReu As Variant, Optional ByVal Eqp As String = "", Optional ByVal COM As String = "", Optional ByVal Positivas As Boolean = False) As CAdjudicaciones

End Function

Public Function DevolverProveedoresConAdjudicaciones(Optional ByVal UsarIndice As Boolean, Optional ByVal Eqp As String, Optional ByVal Comp As String, Optional ByVal FecReu As Variant, Optional ByVal bNoComunicados As Boolean, Optional ByVal bCerrados As Boolean) As CProveedores

End Function


Public Function IniciarAdjudicacion(ByVal UsuarioBloqueo As String) As TipoErrorSummit

End Function

Public Function FinalizarAdjudicacion() As TipoErrorSummit

End Function

Public Function CancelarAdjudicacion() As TipoErrorSummit

End Function

Public Function RealizarAdjudicaciones(ByVal oAdjs As CAdjudicaciones, Optional ByVal FecReu As Variant, Optional ByVal ActualizarResultados As Boolean, Optional ByVal oAdjsGrupo As CAdjsGrupo, Optional ByVal bCierre As Boolean = False, Optional ByVal dblConsumidoProce As Double, Optional ByVal dblAdjudicadoProce As Double, Optional ByVal bPreadj As Boolean, Optional ByVal oUsu As CUsuario) As TipoErrorSummit

End Function

Public Function RealizarAdjudicacionesParciales(ByVal oAdjs As CAdjudicaciones, Optional ByVal FecReu As Variant, Optional ByVal ActualizarResultados As Boolean, Optional ByVal oAdjsGrupo As CAdjsGrupo, Optional ByVal bCierre As Boolean = False, Optional ByVal dblConsumidoProce As Double, Optional ByVal dblAdjudicadoProce As Double, Optional ByVal bPreadj As Boolean, Optional ByVal oUsu As CUsuario) As TipoErrorSummit

End Function


Public Function DevolverProveedoresSinAdjudicaciones(Optional ByVal UsarIndice As Boolean, Optional ByVal Eqp As String, Optional ByVal Comp As String, Optional ByVal FecReu As Variant) As CProveedores

End Function

Public Function DevolverProveedoresCompConAdjudicaciones(Optional ByVal FecReu As Variant) As CProveedores

End Function

