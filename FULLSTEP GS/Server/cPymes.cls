VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cPymes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

Private mCol As Collection
Private m_oConexion As CConexion
Private m_oConexionServer As CConexionDeUseServer


Public Function Add(ByVal lId As Variant, ByVal sCod As String, ByVal sDen As String, Optional ByVal varIndice As Variant) As cPyme
    
    'create a new object
    Dim objnewmember As cPyme
    Set objnewmember = New cPyme
    
    'set the properties passed into the method
    objnewmember.ID = lId
    objnewmember.Cod = sCod
    objnewmember.Den = sDen

    Set objnewmember.Conexion = m_oConexion
    Set objnewmember.ConexionServer = m_oConexionServer
    
    
    If IsMissing(varIndice) Or IsNull(varIndice) Then
        mCol.Add objnewmember, sCod
    Else
        mCol.Add objnewmember, CStr(varIndice)
    End If
    
    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing


End Function

Public Property Get Item(vntIndexKey As Variant) As cPyme


On Error GoTo NoSeEncuentra:
     
Set Item = mCol(vntIndexKey)
Exit Property

NoSeEncuentra:
    Set Item = Nothing

End Property
Friend Property Set Conexion(ByVal con As CConexion)
Set m_oConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = m_oConexion
End Property


Friend Property Get ConexionServer() As CConexionDeUseServer
    Set ConexionServer = m_oConexionServer
End Property
Friend Property Set ConexionServer(ByVal con As CConexionDeUseServer)
    Set m_oConexionServer = con
End Property

Public Property Get Count() As Long
    If mCol Is Nothing Then
       Count = 0
    Else
        Count = mCol.Count
    End If
End Property


Public Sub Remove(vntIndexKey As Variant)
On Error GoTo NoSeEncuentra:
    mCol.Remove vntIndexKey
    Exit Sub
NoSeEncuentra:
    Resume Next
End Sub


Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
    Set NewEnum = mCol.[_NewEnum]
End Property

Private Sub Class_Initialize()
    Set mCol = New Collection
End Sub

Private Sub Class_Terminate()
    'Proceroys collection when this class is terminated
    On Error Resume Next
    Set mCol = Nothing
    Set m_oConexion = Nothing
End Sub

Public Sub CargarTodasLasPymes(Optional ByVal bUsarIndice As Boolean = False, Optional ByVal ConVariablesCalidad As Boolean = False)
'MPG (17/03/2009)
'************************************************************
'*** Descripci�n:   Carga la combo con todas las pymes q
'*** Par�metros de entrada: bUsarIndice--> SI usa indice para la colecci�n.
'*** Llamada desde: frmVarCalidad.frm
'*** Tiempo m�ximo: 0,1seg
'************************************************************

Dim rs As New adodb.Recordset
Dim sConsulta As String
Dim lIndice As Long
Dim lId As Long
Dim sCod, sDen As String



'********* Precondicion **************************************
If m_oConexion Is Nothing Then
    Err.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "cPymes.CargarTodasLasPymes", "No se ha establecido la conexion"
    Exit Sub
End If
'*************************************************************

    
    sConsulta = "SELECT PYMES.ID,COD,DEN "
    
    sConsulta = sConsulta & " FROM PYMES WITH (NOLOCK) "

    If ConVariablesCalidad Then
        sConsulta = sConsulta & "INNER JOIN VAR_CAL0 ON PYMES.ID=VAR_CAL0.PYME "
    End If
    
    sConsulta = sConsulta & " ORDER BY PYMES.ID"

    
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    
    If rs.eof Then
        rs.Close
        Set rs = Nothing
        'Exit Sub
          
    Else
        
        If bUsarIndice Then
            lIndice = 0
        End If
        
                   
        While Not rs.eof
            lId = rs("ID").Value
            sCod = rs("COD").Value
            sDen = rs("DEN").Value

            If bUsarIndice Then
                Me.Add lId, sCod, sDen, lIndice
                lIndice = lIndice + 1
            Else
            
                Me.Add lId, sCod, sDen
            End If
            
            rs.MoveNext
            
        Wend
        
        rs.Close
        Set rs = Nothing
      
        
    End If
End Sub


