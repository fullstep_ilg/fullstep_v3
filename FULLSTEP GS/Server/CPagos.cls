VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CPagos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CPagos **********************************
'*             Autor : Javier Arana
'*             Creada : 13/8/98
'****************************************************************


Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum
Private mCol As Collection
Private m_oConexion As CConexion
Public m_bEOF As Boolean

' Variables para el control de cambios del Log e Integraci�n
Private m_udtOrigen As OrigenIntegracion

Public Function DevolverLosCodigos() As TipoDatosCombo
   
   Dim Codigos As TipoDatosCombo
   Dim iCont As Integer
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
   ReDim Codigos.Cod(mCol.Count)
   ReDim Codigos.Den(mCol.Count)
   
    For iCont = 0 To mCol.Count - 1
        
        Codigos.Cod(iCont) = mCol(iCont + 1).Cod
        Codigos.Den(iCont) = mCol(iCont + 1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
    
    Next

   DevolverLosCodigos = Codigos
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CPagos", "DevolverLosCodigos", ERR, Erl)
        Exit Function
    End If
   
End Function

''' <summary>
''' Carga las fromas de pago de la BD en la coleccion
''' </summary>
''' <param name="NumMaximo">Numero m�ximo de pagos</param>
''' <param name="CaracteresInicialesCod">Cod de busqueda</param>
''' <param name="CaracteresInicialesDen">Den de busqueda</param>
''' <param name="OrdenadosPorDen">Ordenacion por denominacion</param>
''' <param name="UsarIndice">Si se usa indice para la coleccion</param>
''' <returns></returns>
''' <remarks>Llamada desde:Todos los combos de unidades y pantallas que tienen combos de unidades de la aplicacion </remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>
Public Sub CargarTodosLosPagosDesde(ByVal NumMaximo As Integer, Optional ByVal CaracteresInicialesCod As String, Optional ByVal CaracteresInicialesDen As String, Optional ByVal OrdenadosPorDen As Boolean, Optional ByVal UsarIndice As Boolean)

    ''' * Objetivo: Cargar las Pagos hasta un numero maximo
    ''' * Objetivo: desde un codigo o una denominacion determinadas
    
    'ado  Dim rdores As rdoResultset
    Dim rs As adodb.Recordset
    Dim fldCod As adodb.Field
    
    Dim oDen As CMultiidiomas
    Dim oGestorParametros As CGestorParametros
    Dim oIdiomas As CIdiomas
    Dim oIdi As CIdioma
    Dim sCod As String
    Dim sCod2 As String
    Dim sConsulta As String
    
    Dim lIndice As Long
    
    ''' Precondicion
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CPagos.CargarTodosLosPagos", "No se ha establecido la conexion"
        Exit Sub
    End If
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
    ''' Generacion del SQL a partir de los parametros
    
    Set oGestorParametros = New CGestorParametros
    Set oGestorParametros.Conexion = m_oConexion
    Set oIdiomas = oGestorParametros.DevolverIdiomas(False, False, True)
    

    sCod = ""
    sCod2 = ""
    For Each oIdi In oIdiomas
        sCod = sCod & ", " & oIdi.Cod & ".DEN AS DEN_" & oIdi.Cod
        sCod2 = sCod2 & " LEFT JOIN PAG_DEN " & oIdi.Cod & " WITH(NOLOCK) ON PAG.COD = " & oIdi.Cod & ".PAG AND " & oIdi.Cod & ".IDIOMA = '" & oIdi.Cod & "' "
    Next
    
    
    If CaracteresInicialesCod = "" And CaracteresInicialesDen = "" Then
        
        sConsulta = "SELECT TOP " & NumMaximo & " PAG.COD,PAG.FECACT " & sCod & " FROM PAG WITH (NOLOCK) "
        sConsulta = sConsulta & sCod2
        sConsulta = sConsulta & " WHERE 1 = 1"
        
       
    Else
    
        If Not (CaracteresInicialesCod = "") And Not (CaracteresInicialesDen = "") Then
            
                sConsulta = "SELECT TOP " & NumMaximo & " PAG.COD,PAG.FECACT " & sCod & " FROM PAG WITH (NOLOCK) "
                sConsulta = sConsulta & sCod2
                sConsulta = sConsulta & " WHERE COD >='" & DblQuote(CaracteresInicialesCod) & "'"
                sConsulta = sConsulta & " AND DEN_" & gParametrosInstalacion.gIdioma & " >='" & DblQuote(CaracteresInicialesDen) & "'"
                        
        Else
            
            If Not (CaracteresInicialesCod = "") Then
                    sConsulta = "SELECT TOP " & NumMaximo & " PAG.COD,PAG.FECACT " & sCod & " FROM PAG WITH (NOLOCK) "
                    sConsulta = sConsulta & sCod2
                    sConsulta = sConsulta & " WHERE COD >='" & DblQuote(CaracteresInicialesCod) & "'"
            Else
                    sConsulta = "SELECT TOP " & NumMaximo & " PAG.COD,PAG.FECACT " & sCod & " FROM PAG WITH (NOLOCK) "
                    sConsulta = sConsulta & sCod2
                    sConsulta = sConsulta & " WHERE DEN_" & gParametrosInstalacion.gIdioma & " >='" & DblQuote(CaracteresInicialesDen) & "'"
            End If
        
        End If
               
    End If
    
    If OrdenadosPorDen Then
        sConsulta = sConsulta & " ORDER BY DEN_" & gParametrosInstalacion.gIdioma
    Else
        sConsulta = sConsulta & " ORDER BY COD"
    End If
          
    ''' Crear el Recordset
    
    'ado  Set rdores = m_oConexion.rdoSummitCon.OpenResultset(sConsulta, rdOpenForwardOnly, rdConcurReadOnly, rdExecDirect)
    Set rs = New adodb.Recordset
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
          
    If rs.eof Then
            
        rs.Close
        Set rs = Nothing
        Set mCol = Nothing
        Set mCol = New Collection
        Exit Sub
          
    Else
        
        ''' Llenar la coleccion a partir del Resultset
        ''' teniendo en cuenta el maximo
        
        Set mCol = Nothing
        Set mCol = New Collection
        Set fldCod = rs.Fields("COD")
        'Carga todos los idiomas de la aplicaci�n:
        Set oGestorParametros = New CGestorParametros
        Set oGestorParametros.Conexion = m_oConexion
        Set oIdiomas = oGestorParametros.DevolverIdiomas(False, False, True)

            
        If UsarIndice Then
            
            lIndice = 0
       
            While Not rs.eof
                
                Set oDen = Nothing
                Set oDen = New CMultiidiomas
                Set oDen.Conexion = m_oConexion
                For Each oIdi In oIdiomas
                    oDen.Add oIdi.Cod, rs.Fields("DEN_" & oIdi.Cod).Value
                Next
                Me.Add fldCod.Value, oDen, lIndice
                
                rs.MoveNext
                lIndice = lIndice + 1
                
            Wend
            
            If Not rs.eof Then
                m_bEOF = False
            Else
                m_bEOF = True
            End If
                        
        Else
 
            While Not rs.eof
            
                Set oDen = Nothing
                Set oDen = New CMultiidiomas
                Set oDen.Conexion = m_oConexion
                For Each oIdi In oIdiomas
                    oDen.Add oIdi.Cod, rs.Fields("DEN_" & oIdi.Cod).Value
                Next
                Me.Add fldCod.Value, oDen
                
                rs.MoveNext
                
            Wend
            
            If Not rs.eof Then
                m_bEOF = False
            Else
                m_bEOF = True
            End If
            
        End If
        
        rs.Close
        Set rs = Nothing
        Set fldCod = Nothing
        Set oDen = Nothing
          
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CPagos", "CargarTodosLosPagosDesde", ERR, Erl)
        Exit Sub
    End If
    
End Sub

''' <summary>
''' Carga los pagos de la BD en la coleccion
''' </summary>
''' <param name="CaracteresInicialesCod">Cod de busqueda</param>
''' <param name="CaracteresInicialesDen">Den de busqueda</param>
''' <param name="CoincidenciaTotal">Si la busqueda ha de coincidir con la den o el cod</param>
''' <param name="OrdenadosPorDen">Ordenacion por denominacion</param>
''' <param name="UsarIndice">Si se usa indice para la coleccion</param>
''' <param name="MostrarEstadoIntegracion">Si se muestra el estado de la integracion</param>
''' <param name="OrdenadosPorEstadoInt">Si se ordena por el estado de la integracion</param>
''' <param name="DenAOrdenar">El idioma de la ordenacion por denominacion</param>
''' <returns></returns>
''' <remarks>Llamada desde:Todos los combos de pagos y pantallas que tienen combos de pagos de la aplicacion </remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>
Public Sub CargarTodosLosPagos(Optional ByVal CaracteresInicialesCod As String, Optional ByVal CaracteresInicialesDen As String, Optional ByVal CoincidenciaTotal As Boolean, Optional ByVal OrdenadosPorDen As Boolean, Optional ByVal UsarIndice As Boolean, Optional ByVal MostrarEstadoIntegracion As Boolean = False, Optional ByVal OrdenadosPorEstadoInt As Boolean = False, Optional DenAOrdenar As String = "")
'ado  Dim rdores As rdoResultset
Dim rs As adodb.Recordset
Dim fldCod As adodb.Field
Dim fldFecAct As adodb.Field
Dim fldEstadoInt As adodb.Field   'Estado integraci�n
Dim sConsulta As String
Dim lIndice As Long
Dim v_Estado As Variant
Dim oDen As CMultiidiomas
Dim oGestorParametros As CGestorParametros
Dim oIdiomas As CIdiomas
Dim oIdi As CIdioma
Dim sCod As String
Dim sCod2 As String ''MultiERP
Dim sCod3 As String ''MultiERP

'********* Precondicion **************************************
If m_oConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CPagos.CargarTodosLosPagos", "No se ha establecido la conexion"
    Exit Sub
End If
'*************************************************************
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

''' * Objetivo: Cuando se trabaja con integraci�n, desde el mantenimiento se visualiza el estado de integraci�n
''' *            en el sistema receptor de las modificaciones originadas en FSGS.
''' *           Buscamos por coincidencia de c�digo (COD) cuando la acci�n es distinta a un cambio de c�digo (C)
''' *           y por coincidencia en c�digo nuevo (COD_NEW) cuando la acci�n es un cambio de c�digo


'Carga todos los idiomas de la aplicaci�n:
Set oGestorParametros = New CGestorParametros
Set oGestorParametros.Conexion = m_oConexion
Set oIdiomas = oGestorParametros.DevolverIdiomas(False, False, True)


If MostrarEstadoIntegracion Then
        
    Set oDen = Nothing
    Set oDen = New CMultiidiomas
    Set oDen.Conexion = m_oConexion
      
    sCod = ""
    sCod2 = ""
    sCod3 = ""
    
    For Each oIdi In oIdiomas
        sCod = sCod & ", " & oIdi.Cod & ".DEN AS DEN_" & oIdi.Cod
        sCod2 = sCod2 & " LEFT JOIN PAG_DEN " & oIdi.Cod & " WITH(NOLOCK) ON PAG.COD = " & oIdi.Cod & ".PAG AND " & oIdi.Cod & ".IDIOMA = '" & oIdi.Cod & "' "
        sCod3 = sCod3 & "," & "TABLA.DEN_" & oIdi.Cod ''MultiERP
    Next
        

    sConsulta = "SELECT TABLA.COD " & sCod3 & ", MAX(TABLA.FECACT) AS FECACT, MIN(TABLA.ESTADO) AS ESTADO FROM ("
    sConsulta = sConsulta & "SELECT PAG.COD " & sCod & ",PAG.FECACT,CASE WHEN RIGHT(MAX(convert(varchar,log_gral.fecact,121)+LOG_GRAL.ESTADO),1) is null then 4 WHEN RIGHT(MAX(convert(varchar,log_gral.fecact,121)+LOG_GRAL.ESTADO),1) = 4 then 4 else 3 END AS ESTADO FROM PAG WITH (NOLOCK) "
    sConsulta = sConsulta & sCod2
    sConsulta = sConsulta & " LEFT JOIN (LOG_PAG WITH (NOLOCK) INNER JOIN LOG_GRAL WITH (NOLOCK) ON LOG_PAG.ID = LOG_GRAL.ID_TABLA AND LOG_GRAL.TABLA = " & EntidadIntegracion.Pag & " AND (LOG_GRAL.ORIGEN = " & OrigenIntegracion.FSGSInt & " OR LOG_GRAL.ORIGEN = " & OrigenIntegracion.FSGSIntReg & "))"
    sConsulta = sConsulta & " ON (LOG_PAG.COD = PAG.COD AND LOG_PAG.ACCION <> '" & Accion_CambioCodigo & "') OR (LOG_PAG.COD_NEW = PAG.COD AND LOG_PAG.ACCION = '" & Accion_CambioCodigo & "') "

Else

    sCod = ""
    sCod2 = ""
    For Each oIdi In oIdiomas
        sCod = sCod & ", " & oIdi.Cod & ".DEN AS DEN_" & oIdi.Cod
        sCod2 = sCod2 & " LEFT JOIN PAG_DEN " & oIdi.Cod & " WITH(NOLOCK) ON PAG.COD = " & oIdi.Cod & ".PAG AND " & oIdi.Cod & ".IDIOMA = '" & oIdi.Cod & "' "
    Next

    sConsulta = "SELECT * FROM (" ''MultiERP
    sConsulta = sConsulta & "SELECT COD,PAG.FECACT" & sCod & " FROM PAG WITH (NOLOCK)"
    sConsulta = sConsulta & sCod2
    
End If

If CaracteresInicialesCod = "" And CaracteresInicialesDen = "" Then
  
Else
    If Not (CaracteresInicialesCod = "") And Not (CaracteresInicialesDen = "") Then
        
        If CoincidenciaTotal Then
            
            sConsulta = sConsulta & " WHERE"
            sConsulta = sConsulta & " PAG.COD ='" & DblQuote(CaracteresInicialesCod) & "'"
            sConsulta = sConsulta & " AND DEN_" & gParametrosInstalacion.gIdioma & "='" & DblQuote(CaracteresInicialesDen) & "'"
            
        Else
            
            sConsulta = sConsulta & " WHERE"
            sConsulta = sConsulta & " PAG.COD LIKE '" & DblQuote(CaracteresInicialesCod) & "%'"
            sConsulta = sConsulta & " DEN_" & gParametrosInstalacion.gIdioma & " LIKE '" & DblQuote(CaracteresInicialesDen) & "%'"
                       
        End If
                    
    Else
        
        If Not (CaracteresInicialesCod = "") Then
            
            If CoincidenciaTotal Then
            
                sConsulta = sConsulta & " WHERE"
                sConsulta = sConsulta & " PAG.COD ='" & DblQuote(CaracteresInicialesCod) & "'"
                
            Else
                    
                sConsulta = sConsulta & " WHERE"
                sConsulta = sConsulta & " PAG.COD LIKE '" & DblQuote(CaracteresInicialesCod) & "%'"
                
            End If
            
        Else
            
            If CoincidenciaTotal Then
            
                sConsulta = sConsulta & " WHERE"
                sConsulta = sConsulta & " DEN_" & gParametrosInstalacion.gIdioma & "='" & DblQuote(CaracteresInicialesDen) & "'"
                
            Else
           
                sConsulta = sConsulta & " WHERE"
                sConsulta = sConsulta & " DEN_" & gParametrosInstalacion.gIdioma & " LIKE '" & DblQuote(CaracteresInicialesDen) & "%'"
                  
            End If
            
        End If
    
    End If
           
End If
If MostrarEstadoIntegracion Then
    sCod = ""
    sCod2 = ""
    For Each oIdi In oIdiomas
        sCod = sCod & "," & oIdi.Cod & ".DEN"
        sCod2 = sCod2 & ", TABLA.DEN_" & oIdi.Cod   ''MultiERP
    Next
        
    sConsulta = sConsulta & " GROUP BY PAG.COD" & sCod & ",PAG.FECACT, LOG_PAG.ERP"
    sConsulta = sConsulta & " ) AS TABLA GROUP BY TABLA.COD" & sCod2 & ",TABLA.FECACT" ''MultiERP
Else ''MultiERP
    sConsulta = sConsulta & " ) AS TABLA "
End If
   
If OrdenadosPorDen Then
    If DenAOrdenar = "" Then
        sConsulta = sConsulta & " ORDER BY TABLA.DEN_" & gParametrosInstalacion.gIdioma & ",TABLA.COD" ''MultiERP
    Else
        sConsulta = sConsulta & " ORDER BY TABLA." & DenAOrdenar & ",TABLA.COD" ''MultiERP
    End If
Else
    If OrdenadosPorEstadoInt Then
        sConsulta = sConsulta & " ORDER BY ESTADO,TABLA.COD,TABLA.DEN_" & gParametrosInstalacion.gIdioma ''MultiERP
    Else
        sConsulta = sConsulta & " ORDER BY TABLA.COD,TABLA.DEN_" & gParametrosInstalacion.gIdioma ''MultiERP
    End If
End If
      
Set rs = New adodb.Recordset
rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
      
If rs.eof Then
        
    Set mCol = Nothing
    Set mCol = New Collection
    
    rs.Close
    Set rs = Nothing
    Exit Sub
      
Else
    
    Set mCol = Nothing
    Set mCol = New Collection
    Set fldCod = rs.Fields("COD")
    Set fldFecAct = rs.Fields("FECACT")
    If MostrarEstadoIntegracion Then
        Set fldEstadoInt = rs.Fields("ESTADO")
    End If
        
    If UsarIndice Then
        
        lIndice = 0
        
        While Not rs.eof
            ' Dejamos en manos del programador cuando asignar la conexion al obejto usuario
            If MostrarEstadoIntegracion Then
                v_Estado = fldEstadoInt.Value
            Else
                v_Estado = Null
            End If

            Set oDen = Nothing
            Set oDen = New CMultiidiomas
            Set oDen.Conexion = m_oConexion
            For Each oIdi In oIdiomas
                oDen.Add oIdi.Cod, rs.Fields("DEN_" & oIdi.Cod).Value
            Next
            Me.Add fldCod.Value, oDen, lIndice, fldFecAct.Value, v_Estado
            
            rs.MoveNext
            lIndice = lIndice + 1
        Wend
        
    Else
        
        While Not rs.eof
            ' Dejamos en manos del programador cuando asignar la conexion al obejto usuario
            If MostrarEstadoIntegracion Then
                v_Estado = fldEstadoInt.Value
            Else
                v_Estado = Null
            End If
            
            Set oDen = Nothing
            Set oDen = New CMultiidiomas
            Set oDen.Conexion = m_oConexion
            For Each oIdi In oIdiomas
                oDen.Add oIdi.Cod, rs.Fields("DEN_" & oIdi.Cod).Value
            Next


            Me.Add fldCod.Value, oDen, , fldFecAct.Value, v_Estado
            
            rs.MoveNext
        Wend
    End If
    
    rs.Close
    Set rs = Nothing
    Set fldCod = Nothing
    Set oDen = Nothing
    Set fldFecAct = Nothing
    Set fldEstadoInt = Nothing
    Exit Sub
      
End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CPagos", "CargarTodosLosPagos", ERR, Erl)
        Exit Sub
    End If
End Sub
Public Function Add(ByVal Cod As String, ByVal oDen As CMultiidiomas, Optional ByVal varIndice As Variant, Optional ByVal FecAct As Variant, Optional ByVal varEstado As Variant) As CPago
    'create a new object
    Dim objnewmember As CPago
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New CPago
   
    objnewmember.Cod = Cod
    If Not IsMissing(oDen) Then
        Set objnewmember.Denominaciones = oDen
    Else
        Set objnewmember.Denominaciones = Nothing
    End If
    
    objnewmember.EstadoIntegracion = varEstado 'Estado integracion
    objnewmember.FecAct = FecAct
    
    Set objnewmember.Conexion = m_oConexion
        
    If Not IsMissing(varIndice) And Not IsNull(varIndice) Then
        objnewmember.Indice = varIndice
        mCol.Add objnewmember, CStr(varIndice)
    Else
    'set the properties passed into the method
       mCol.Add objnewmember, CStr(Cod)
    End If
    
    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CPagos", "Add", ERR, Erl)
        Exit Function
    End If

End Function

Public Property Get Item(vntIndexKey As Variant) As CPago
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Friend Property Set Conexion(ByVal con As CConexion)
Set m_oConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = m_oConexion
End Property


Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
     Count = mCol.Count
End If


End Property


Public Sub Remove(vntIndexKey As Variant)
   
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    mCol.Remove vntIndexKey
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CPagos", "Remove", ERR, Erl)
        Exit Sub
    End If
End Sub


Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
     Set NewEnum = mCol.[_NewEnum]
End Property


Private Sub Class_Initialize()
    Set mCol = New Collection
End Sub


Private Sub Class_Terminate()
    'destroys collection when this class is terminated
    Set mCol = Nothing
    Set m_oConexion = Nothing
End Sub

Public Property Get eof() As Boolean
    eof = m_bEOF
End Property

Public Property Let eof(ByVal vNewValue As Boolean)
    m_bEOF = vNewValue
End Property
Private Function GrabarEnLog() As Boolean
'***********************************************************************************
'*** Descripci�n: Nos indica si hay que grabar o no el cambio en el log de       ***
'***              Pagos (tabla LOG_PAG) y carga en la variable                   ***
'***              del m�dulo m_udtOrigen el origen del movimiento                ***
'*** Parametros:  Ninguno                                                        ***
'*** Devuelve:    True (hay que grabar) False (No hay que grabar)                ***
'***********************************************************************************

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If gParametrosGenerales.gbACTIVLOG Or gParametrosIntegracion.gaExportar(EntidadIntegracion.Pag) Then
        If gParametrosGenerales.gbACTIVLOG And gParametrosIntegracion.gaExportar(EntidadIntegracion.Pag) Then
            m_udtOrigen = FSGSIntReg
        Else
            If gParametrosGenerales.gbACTIVLOG Then
                m_udtOrigen = FSGSReg
            Else
                m_udtOrigen = FSGSInt
            End If
        End If
        GrabarEnLog = True
    Else
        GrabarEnLog = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CPagos", "GrabarEnLog", ERR, Erl)
        Exit Function
    End If

End Function

''' <summary>Carga todos los c�digos de formas de pago disponibles.
''' Cargar� los c�digos que existan en la tabla de mapeo para el ERP </summary>
''' <param name="lEmpresa">ID de la empresa</param>
''' <param name="sCodProveERP">C�digo ERP del proveedor para esa empresa</param>
''' <param name="bOrdenadosPorDen">OPCIONAL. TRUE para devolver los datos ordenados por denominaci�n</param>
''' <remarks>Llamada desde: frmPedidos.sdbcPagoCod_DropDown(), frmPedidos.sdbcPagoDen_DropDown().
''' Tiempo m�ximo: 1 seg</remarks>
''' <revision>ngo 17/01/2012</revision>
Public Sub CargarLosPagosCorrespondenciaCon(ByVal lEmpresa As Long, ByVal sCodProveERP As String, Optional ByVal bOrdenadosPorDen As Boolean)
Dim rs As adodb.Recordset
Dim fldCod As adodb.Field
Dim fldFecAct As adodb.Field
Dim fldEstadoInt As adodb.Field   'Estado integraci�n
Dim sConsulta As String
Dim oDen As CMultiidiomas
Dim oGestorParametros As CGestorParametros
Dim oIdiomas As CIdiomas
Dim oIdi As CIdioma
Dim sCod As String
Dim sCod2 As String ''MultiERP

'********* Precondicion **************************************
If m_oConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CPagos.CargarTodosLosPagos", "No se ha establecido la conexion"
    Exit Sub
End If
'*************************************************************
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
      
'Carga todos los idiomas de la aplicaci�n:
Set oGestorParametros = New CGestorParametros
Set oGestorParametros.Conexion = m_oConexion
Set oIdiomas = oGestorParametros.DevolverIdiomas(False, False, True)

    For Each oIdi In oIdiomas
        sCod = sCod & ", " & oIdi.Cod & ".DEN AS DEN_" & oIdi.Cod
        sCod2 = sCod2 & " LEFT JOIN PAG_DEN " & oIdi.Cod & " WITH(NOLOCK) ON PAG.COD = " & oIdi.Cod & ".PAG AND " & oIdi.Cod & ".IDIOMA = '" & oIdi.Cod & "' "
    Next

Set rs = New adodb.Recordset
sConsulta = "SELECT DISTINCT PAG.* "
sConsulta = sConsulta & sCod
sConsulta = sConsulta & " FROM PAG WITH (NOLOCK) "
sConsulta = sConsulta & sCod2
sConsulta = sConsulta & " INNER JOIN PAG_ERP WITH (NOLOCK) ON PAG.COD=PAG_ERP.COD_GS"
sConsulta = sConsulta & " INNER JOIN ERP_SOCIEDAD WITH (NOLOCK) ON PAG_ERP.ERP=ERP_SOCIEDAD.ERP"
sConsulta = sConsulta & " INNER JOIN EMP WITH (NOLOCK) ON ERP_SOCIEDAD.SOCIEDAD = EMP.SOCIEDAD"
sConsulta = sConsulta & " WHERE EMP.ID = " & lEmpresa
rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
If rs.eof Then
    'Si no hay datos en las tablas de mapeo se cargan todos los datos que hay en PAG.
    CargarTodosLosPagosDesde gParametrosInstalacion.giCargaMaximaCombos, , , bOrdenadosPorDen, False
    rs.Close
    Set rs = Nothing
    Exit Sub
Else
    Set mCol = Nothing
    Set mCol = New Collection
    Set fldCod = rs.Fields("COD")
    Set fldFecAct = rs.Fields("FECACT")
    While Not rs.eof
        Set oDen = Nothing
        Set oDen = New CMultiidiomas
        Set oDen.Conexion = m_oConexion
        For Each oIdi In oIdiomas
            oDen.Add oIdi.Cod, "" & rs.Fields("DEN_" & oIdi.Cod).Value
        Next
        Me.Add "" & fldCod.Value, oDen, , fldFecAct.Value
        rs.MoveNext
    Wend
    rs.Close
    Set rs = Nothing
    Set fldCod = Nothing
    Set oDen = Nothing
    Set fldFecAct = Nothing
    Set fldEstadoInt = Nothing
    Exit Sub
End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CPagos", "CargarLosPagosCorrespondenciaCon", ERR, Erl)
        Exit Sub
    End If
End Sub

'-------------------------------------
'22/09/2008 si es nul que devuelva lo del proveedor
Public Function strFormaPagoProvERP(ByVal lEmpresa As Long, ByVal sCodProveERP As String, ByVal sCodProve As String) As String
Dim sConsulta As String
Dim rs As New adodb.Recordset

'********* Precondicion **************************************
If m_oConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CPagos.strFormaPagoProvERP", "No se ha establecido la conexion"
    Exit Function
End If
'*************************************************************
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
   
    sConsulta = "SELECT PROVE_ERP.PAG,PROVE.PAG AS PAGPROVE FROM PROVE_ERP WITH(NOLOCK) "
    sConsulta = sConsulta & " LEFT JOIN PROVE_EMPRESA_COD_ERP WITH (NOLOCK) ON PROVE_ERP.EMPRESA=PROVE_EMPRESA_COD_ERP.EMPRESA "
    sConsulta = sConsulta & " AND PROVE_ERP.COD_ERP=PROVE_EMPRESA_COD_ERP.COD_ERP  "
    sConsulta = sConsulta & " LEFT JOIN PROVE WITH (NOLOCK) ON PROVE_EMPRESA_COD_ERP.PROVE=PROVE.COD "
    sConsulta = sConsulta & " WHERE PROVE_ERP.EMPRESA = " & lEmpresa
    sConsulta = sConsulta & " AND PROVE_ERP.COD_ERP= " & StrToSQLNULL(sCodProveERP)
    sConsulta = sConsulta & " AND PROVE_EMPRESA_COD_ERP.PROVE= " & StrToSQLNULL(sCodProve)
    
    rs.CursorLocation = adUseClient
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    If Not rs.eof Then
        If IsNull(rs.Fields("PAG").Value) Then
            If IsNull(rs.Fields("PAGPROVE").Value) Then
                strFormaPagoProvERP = ""
            Else
                strFormaPagoProvERP = NullToStr(rs.Fields("PAGPROVE").Value)
            End If
        Else
            strFormaPagoProvERP = NullToStr(rs.Fields("PAG").Value)
        End If
    End If
    rs.Close
    Set rs.ActiveConnection = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CPagos", "strFormaPagoProvERP", ERR, Erl)
        Exit Function
    End If
End Function

