VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CConfVistasCalGlobalVar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private mCol As Collection
Private m_oConexion As CConexion
Private m_bEOF As Boolean

Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum

''' <summary>
''' A�ade (al objeto CConfVistaCalGlobalVar) una configuraci�n de vista global de calificaci�n para una variable en la
''' comparativa QA
''' </summary>
''' <param name="iVista">ID de la vista</param>
''' <param name="iVarCal">ID de la variable</param>
''' <param name="iNivel">Nivel de la variable</param>
''' <param name="bVisible">Visibilidad de la variable</param>
''' <param name="iPos">Posici�n de la variable en el grid. No se usa, pero como esta en bbdd ...</param>
''' <param name="sDen">Denominaci�n de la variable</param>
''' <param name="sUnQas">Unidades de negocio visibles en la variable</param>
''' <param name="vIndice">Indice donde a�adir</param>
''' <returns>Un objeto CConfVistaCalGlobalVar</returns>
''' <remarks>Llamada desde: CargarVariablesVista ; Tiempo m�ximo:0,1</remarks>
Public Function Add(ByVal iVista As Integer, ByVal iVarCal As Integer, ByVal iNivel As Integer _
, Optional ByVal bVisible As Variant, Optional ByVal iPos As Variant, Optional ByVal sDen As String _
, Optional ByVal sUnQas As String, Optional ByVal vIndice As Variant) As CConfVistaCalGlobalVar

    'create a new object
    Dim objnewmember As CConfVistaCalGlobalVar
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New CConfVistaCalGlobalVar
   
    objnewmember.Vista = iVista
    objnewmember.VarCal = iVarCal
    objnewmember.Nivel = iNivel
    If Not IsMissing(iPos) Then
        objnewmember.Posicion = iPos
    Else
        objnewmember.Posicion = 0
    End If
    If Not IsMissing(bVisible) Then
        objnewmember.Visible = bVisible
    Else
        objnewmember.Visible = False
    End If
    If Not IsMissing(sDen) Then
        objnewmember.Denominacion = sDen
    Else
        objnewmember.Denominacion = ""
    End If
    If Not IsMissing(sUnQas) Then
        objnewmember.UnQas = sUnQas
    Else
        objnewmember.UnQas = ","
    End If

    Set objnewmember.Conexion = m_oConexion
       
    If Not IsMissing(vIndice) And Not IsNull(vIndice) Then
        objnewmember.Indice = vIndice
        mCol.Add objnewmember, CStr(vIndice)
    Else
       mCol.Add objnewmember, CStr(iVista) & CStr(iNivel) & CStr(iVarCal)
    End If
    
    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CConfVistasCalGlobalVar", "Add", ERR, Erl)
      Exit Function
   End If

End Function

Friend Property Set Conexion(ByVal con As CConexion)
    Set m_oConexion = con
End Property

Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property

Public Property Get Count() As Long
    If mCol Is Nothing Then
        Count = 0
    Else
         Count = mCol.Count
    End If
End Property

Public Property Get eof() As Boolean
    eof = m_bEOF
End Property

Friend Property Let eof(ByVal b As Boolean)
    m_bEOF = b
End Property

Public Property Get Item(vntIndexKey As Variant) As CConfVistaCalGlobalVar
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
End Property

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
     Set NewEnum = mCol.[_NewEnum]
End Property

Public Sub Remove(vntIndexKey As Variant)
On Error GoTo ERROR

    mCol.Remove vntIndexKey

ERROR:
End Sub

Private Sub Class_Initialize()
    Set mCol = New Collection
End Sub

Private Sub Class_Terminate()
    On Error Resume Next
    Set mCol = Nothing
    Set m_oConexion = Nothing
End Sub

''' <summary>
''' Carga las variables de calidad asociada a una vista
''' </summary>
''' <param name="iVista">Id de la vista</param>
''' <param name=" sUsuCod">Codigo de usuario</param>
''' <param name=" vIndice">Para indicar el indice de la coleccion</param>
''' <returns>Coleccion de variables de calidad desde el nivel 0 al nivel5</returns>
''' <remarks>Llamada desde CConfVistaCalGlobal.CargarConfCalGlobalVar; Tiempo m�ximo < 1seg</remarks>
Public Sub CargarVariablesVista(ByVal iVista As Integer, Optional ByVal sUsuCod As Variant, Optional ByVal vIndice As Variant, Optional ByVal Pyme As Variant)
Dim rs1 As adodb.Recordset
Dim rs As adodb.Recordset
Dim adocom As adodb.Command
Dim oParam As adodb.Parameter
Dim oVarCalVista As CConfVistaCalGlobalVar

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
    Set adocom = New adodb.Command
    Set adocom.ActiveConnection = m_oConexion.ADOCon
    
    adocom.CommandType = adCmdStoredProc
    adocom.CommandText = "FSQA_GET_VARIABLES_VISTA"

    Set oParam = adocom.CreateParameter("VISTA", adInteger, adParamInput, , iVista)
    adocom.Parameters.Append oParam
    Set oParam = adocom.CreateParameter("USU", adVarChar, adParamInput, 50, sUsuCod)
    adocom.Parameters.Append oParam
    Set oParam = adocom.CreateParameter("IDI", adVarChar, adParamInput, 20, gParametrosInstalacion.gIdioma)
    adocom.Parameters.Append oParam
    Set oParam = adocom.CreateParameter("PYME", adInteger, adParamInput, , Pyme)
    adocom.Parameters.Append oParam
       
    ''' Ejecutar la SP
    Set rs1 = adocom.Execute

    Dim id0 As Integer
    

    If Not rs1.eof Then
        Set mCol = Nothing
        Set mCol = New Collection
        
        While Not rs1.eof
            id0 = rs1.Fields("ID").Value
            If iVista = TipoDeVistaQA.Vistainicialqa Then
                Me.Add iVista, rs1.Fields("ID").Value, 0, True, , rs1.Fields("DEN").Value, rs1.Fields("UNQAS").Value, vIndice
            Else
                If Not IsNull(rs1.Fields("NIVEL").Value) Then   'Si no est� en la vista, el nivel viene a null
                    Me.Add iVista, rs1.Fields("ID").Value, rs1.Fields("NIVEL").Value, SQLBinaryToBoolean(rs1.Fields("VISIBLE").Value), rs1.Fields("POS").Value, rs1.Fields("DEN").Value, rs1.Fields("UNQAS").Value
                End If
            End If
            rs1.MoveNext
        Wend
    End If
    

    Set rs = rs1.NextRecordset
    
    While Not rs.eof
        Set oVarCalVista = Me.Item(CStr(iVista) & CStr(0) & CStr(id0))
        If oVarCalVista.VariblesCalVista Is Nothing Then
            Set oVarCalVista.VariblesCalVista = New CConfVistasCalGlobalVar
            Set oVarCalVista.Conexion = m_oConexion
        End If
        If iVista = TipoDeVistaQA.Vistainicialqa Then
            oVarCalVista.VariblesCalVista.Add iVista, rs.Fields("ID").Value, 1, True, , rs.Fields("DEN").Value, rs.Fields("UNQAS").Value
        Else
            If Not IsNull(rs.Fields("NIVEL").Value) Then   'Si no est� en la vista, el nivel viene a null
                oVarCalVista.VariblesCalVista.Add iVista, rs.Fields("ID").Value, rs.Fields("NIVEL").Value, SQLBinaryToBoolean(rs.Fields("VISIBLE").Value), rs.Fields("POS").Value, rs.Fields("DEN").Value, rs.Fields("UNQAS").Value, vIndice
            End If
        End If
        rs.MoveNext
    Wend
    rs.Close
    
    Set rs = rs1.NextRecordset
    While Not rs.eof
        Set oVarCalVista = Me.Item(CStr(iVista) & CStr(0) & CStr(id0)).VariblesCalVista.Item(CStr(iVista) & CStr(1) & CStr(rs.Fields("ID_VAR_CAL1").Value))
        If oVarCalVista.VariblesCalVista Is Nothing Then
            Set oVarCalVista.VariblesCalVista = New CConfVistasCalGlobalVar
            Set oVarCalVista.Conexion = m_oConexion
        End If
        If iVista = TipoDeVistaQA.Vistainicialqa Then
            oVarCalVista.VariblesCalVista.Add iVista, rs.Fields("ID").Value, 2, True, , rs.Fields("DEN").Value, rs.Fields("UNQAS").Value
        Else
            If Not IsNull(rs.Fields("NIVEL").Value) Then   'Si no est� en la vista, el nivel viene a null
                oVarCalVista.VariblesCalVista.Add iVista, rs.Fields("ID").Value, rs.Fields("NIVEL").Value, SQLBinaryToBoolean(rs.Fields("VISIBLE").Value), rs.Fields("POS").Value, rs.Fields("DEN").Value, rs.Fields("UNQAS").Value, vIndice
            End If
        End If
        rs.MoveNext
    Wend
    rs.Close
    
    Set rs = rs1.NextRecordset
    While Not rs.eof
        Set oVarCalVista = Me.Item(CStr(iVista) & CStr(0) & CStr(id0)).VariblesCalVista.Item(CStr(iVista) & CStr(1) & CStr(rs.Fields("ID_VAR_CAL1").Value)).VariblesCalVista.Item(CStr(iVista) & CStr(2) & CStr(rs.Fields("ID_VAR_CAL2").Value))
        If oVarCalVista.VariblesCalVista Is Nothing Then
            Set oVarCalVista.VariblesCalVista = New CConfVistasCalGlobalVar
            Set oVarCalVista.VariblesCalVista.Conexion = m_oConexion
        End If
        If iVista = TipoDeVistaQA.Vistainicialqa Then
            oVarCalVista.VariblesCalVista.Add iVista, rs.Fields("ID").Value, 3, True, , rs.Fields("DEN").Value, rs.Fields("UNQAS").Value
        Else
            If Not IsNull(rs.Fields("NIVEL").Value) Then   'Si no est� en la vista, el nivel viene a null
                oVarCalVista.VariblesCalVista.Add iVista, rs.Fields("ID").Value, rs.Fields("NIVEL").Value, SQLBinaryToBoolean(rs.Fields("VISIBLE").Value), rs.Fields("POS").Value, rs.Fields("DEN").Value, rs.Fields("UNQAS").Value, vIndice
            End If
        End If
        rs.MoveNext
    Wend
    
    Set rs = rs1.NextRecordset
    While Not rs.eof
        Set oVarCalVista = Me.Item(CStr(iVista) & CStr(0) & CStr(id0)).VariblesCalVista.Item(CStr(iVista) & CStr(1) & CStr(rs.Fields("ID_VAR_CAL1").Value)).VariblesCalVista.Item(CStr(iVista) & CStr(2) & CStr(rs.Fields("ID_VAR_CAL2").Value)).VariblesCalVista.Item(CStr(iVista) & CStr(3) & CStr(rs.Fields("ID_VAR_CAL3").Value))
        If oVarCalVista.VariblesCalVista Is Nothing Then
            Set oVarCalVista.VariblesCalVista = New CConfVistasCalGlobalVar
            Set oVarCalVista.VariblesCalVista.Conexion = m_oConexion
        End If
        If iVista = TipoDeVistaQA.Vistainicialqa Then
            oVarCalVista.VariblesCalVista.Add iVista, rs.Fields("ID").Value, 4, True, , rs.Fields("DEN").Value, rs.Fields("UNQAS").Value
        Else
            If Not IsNull(rs.Fields("NIVEL").Value) Then   'Si no est� en la vista, el nivel viene a null
                oVarCalVista.VariblesCalVista.Add iVista, rs.Fields("ID").Value, rs.Fields("NIVEL").Value, SQLBinaryToBoolean(rs.Fields("VISIBLE").Value), rs.Fields("POS").Value, rs.Fields("DEN").Value, rs.Fields("UNQAS").Value, vIndice
            End If
        End If
        rs.MoveNext
    Wend

    Set rs = rs1.NextRecordset
    While Not rs.eof
        Set oVarCalVista = Me.Item(CStr(iVista) & CStr(0) & CStr(id0)).VariblesCalVista.Item(CStr(iVista) & CStr(1) & CStr(rs.Fields("ID_VAR_CAL1").Value)).VariblesCalVista.Item(CStr(iVista) & CStr(2) & CStr(rs.Fields("ID_VAR_CAL2").Value)).VariblesCalVista.Item(CStr(iVista) & CStr(3) & CStr(rs.Fields("ID_VAR_CAL3").Value)).VariblesCalVista.Item(CStr(iVista) & CStr(4) & CStr(rs.Fields("ID_VAR_CAL4").Value))
        If oVarCalVista.VariblesCalVista Is Nothing Then
            Set oVarCalVista.VariblesCalVista = New CConfVistasCalGlobalVar
            Set oVarCalVista.VariblesCalVista.Conexion = m_oConexion
        End If
        If iVista = TipoDeVistaQA.Vistainicialqa Then
            oVarCalVista.VariblesCalVista.Add iVista, rs.Fields("ID").Value, 5, True, , rs.Fields("DEN").Value, rs.Fields("UNQAS").Value
        Else
            If Not IsNull(rs.Fields("NIVEL").Value) Then   'Si no est� en la vista, el nivel viene a null
                oVarCalVista.VariblesCalVista.Add iVista, rs.Fields("ID").Value, rs.Fields("NIVEL").Value, SQLBinaryToBoolean(rs.Fields("VISIBLE").Value), rs.Fields("POS").Value, rs.Fields("DEN").Value, rs.Fields("UNQAS").Value, vIndice
            End If
        End If
        rs.MoveNext
    Wend
    
    rs.Close
    Set rs = Nothing
    rs1.Close
    Set rs1 = Nothing
    Set adocom = Nothing
    Set oParam = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CConfVistasCalGlobalVar", "CargarVariablesVista", ERR, Erl)
      Exit Sub
   End If
    
End Sub
