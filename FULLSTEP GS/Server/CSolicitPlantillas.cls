VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CSolicitPlantillas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

''' *** Clase: CSolicitPlantillas

''' Variables que almacenan las propiedades de los objetos
Private m_Col As Collection
Private m_bEOF As Boolean
Private m_oConexion As CConexion

''' Control de errores
Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum

Public Sub CargarTodasLasPlantillas(ByVal lIDSolicitud As Long, Optional ByVal UsarIndice As Boolean = False)

    Dim sConsulta As String
    Dim lIndice As Long
    Dim rs As adodb.Recordset
    Dim fldId As adodb.Field
    Dim fldIdioma As adodb.Field
    Dim fldRuta As adodb.Field
    Dim fldFecAct As adodb.Field
    Dim oPlant As CSolicitPlantilla
            
    ''' Precondicion
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CSolicitPLantillas.CargarTodasLasPlantillas", "No se ha establecido la conexion"
        Exit Sub
    End If
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
        
    ''' Generacion de SQL
    Set rs = New adodb.Recordset
    sConsulta = "SELECT ID,IDIOMA,RUTA,FECACT FROM SOLICITUD_PLANTILLAS WITH (NOLOCK) WHERE SOLICITUD=" & lIDSolicitud
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
             
    If rs.eof Then
    
        rs.Close
        Set rs = Nothing
        Set m_Col = Nothing
        Set m_Col = New Collection
        Exit Sub
        
    Else
        
        ''' Carga del Resultset en la coleccion
        
        Set m_Col = Nothing
        Set m_Col = New Collection
        
        Set fldId = rs.Fields("ID")
        Set fldIdioma = rs.Fields("IDIOMA")
        Set fldRuta = rs.Fields("RUTA")
        Set fldFecAct = rs.Fields("FECACT")
        
        If UsarIndice Then
            
            lIndice = 0
            
            While Not rs.eof
                
                Set oPlant = New CSolicitPlantilla
                With oPlant
                    .Id = fldId
                    .Solicitud = lIDSolicitud
                    .Idioma = fldIdioma
                    .Ruta = fldRuta
                    .FecAct = CDate(fldFecAct.Value)
                    Set .Conexion = m_oConexion
                    'Set .ConexionServer = mvarConexionServer
                    .Indice = lIndice
                End With
                
                m_Col.Add oPlant, CStr(lIndice)
                
                rs.MoveNext
                lIndice = lIndice + 1
            Wend
        Else
        
            While Not rs.eof
        
                Set oPlant = New CSolicitPlantilla
                With oPlant
                    .Id = fldId
                    .Solicitud = lIDSolicitud
                    .Idioma = fldIdioma
                    .Ruta = fldRuta
                    .FecAct = CDate(fldFecAct.Value)
                    Set .Conexion = m_oConexion
                    'Set .ConexionServer = mvarConexionServer
                    
                End With
                
                m_Col.Add oPlant, CStr(fldId)
                rs.MoveNext
            Wend
        End If
    
        Set fldId = Nothing
        Set fldIdioma = Nothing
        Set fldRuta = Nothing
        Set fldFecAct = Nothing
                
        rs.Close
        Set rs = Nothing
        
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CSolicitPlantillas", "CargarTodasLasPlantillas", ERR, Erl)
      Exit Sub
   End If
End Sub


''' <summary>
''' Crear la colecci�n de estados de una No Conformidad
''' </summary>
''' <param name="oSolicitud">Solictud de la q se estan cargando los estados</param>
''' <param name="COD">Codigo de estado</param>
''' <param name="oDen">Opcional. coleccion multidioma de denominaciones</param>
''' <param name="FecAct">Opcional. Fecha de ultima actualizacion</param>
''' <param name="varIndice">Opcional. �ndice del estado</param>
''' <returns>La funci�n devuelve el objeto CSolicitEstado creado, con los datos de los estados</returns>
''' <remarks>Llamada desde: CSolicitud/IBaseDatos_AnyadirABaseDatos     CSolicitud/CargarEstadosNoConformidad
'''         frmSOLConfigEstados/sdbgEstados_BeforeColUpdate         frmSOLConfigEstados/sdbgEstados_BeforeUpdate
'''; Tiempo m�ximo:0</remarks>
Public Function Add(ByVal lId As Long, ByVal lIDSolicitud As Long, Optional ByVal sIdioma As String, Optional ByVal sRuta As String, _
                    Optional ByVal FecAct As Variant, Optional ByVal varIndice As Variant) As CSolicitPlantilla
                    
    Dim objnewmember As CSolicitPlantilla
    
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New CSolicitPlantilla
    
    
    objnewmember.Id = lId
    objnewmember.Solicitud = lIDSolicitud
    
    If Not IsMissing(sIdioma) Then
       objnewmember.Idioma = sIdioma
    Else
       objnewmember.Idioma = Null
    End If
    If Not IsMissing(sRuta) Then
        objnewmember.Ruta = sRuta
    Else
        objnewmember.Ruta = Null
    End If
    If Not IsMissing(FecAct) Then
        objnewmember.FecAct = FecAct
    Else
        objnewmember.FecAct = Null
    End If
    
    Set objnewmember.Conexion = m_oConexion
    
    If Not IsMissing(varIndice) And Not IsNull(varIndice) Then
        objnewmember.Indice = varIndice
        m_Col.Add objnewmember, CStr(varIndice)
    Else
        m_Col.Add objnewmember, CStr(lId)
    End If
            
    Set Add = objnewmember
    
    Set objnewmember = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CSolicitPlantillas", "Add", ERR, Erl)
      Exit Function
   End If
    
End Function
                    


Public Property Get Item(vntIndexKey As Variant) As CSolicitPlantilla

On Error GoTo NoSeEncuentra:

    ''' Devolvemos el item de la coleccion privada
    
    Set Item = m_Col(vntIndexKey)
     
    Exit Property

NoSeEncuentra:

    Set Item = Nothing
    
End Property
Public Property Get eof() As Boolean

    ''' * Objetivo: Devolver variable privada EOF
    ''' * Recibe: Nada
    ''' * Devuelve: Variable privada EOF
    
    eof = m_bEOF
    
End Property
Friend Property Set Conexion(ByVal con As CConexion)

    ''' * Objetivo: Dar valor a la variable privada Conexion
    ''' * Recibe: Conexion
    ''' * Devuelve: Nada
    
    Set m_oConexion = con
    
End Property
Friend Property Get Conexion() As CConexion

    ''' * Objetivo: Devolver variable privada Conexion
    ''' * Recibe: Nada
    ''' * Devuelve: Conexion
    
    Set Conexion = m_oConexion
    
End Property
Public Property Get Count() As Long

    ''' * Objetivo: Devolver variable privada Count
    ''' * Recibe: Nada
    ''' * Devuelve: Numero de elementos de la coleccion

    If m_Col Is Nothing Then
        Count = 0
    Else
        Count = m_Col.Count
    End If

End Property
Public Sub Remove(vntIndexKey As Variant)


    ''' * Objetivo: Eliminar una solicitud de la coleccion
    ''' * Recibe: Indice de la solicitud a eliminar
    ''' * Devuelve: Nada
   
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_Col.Remove vntIndexKey
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CSolicitPlantillas", "Remove", ERR, Erl)
      Exit Sub
   End If
    
End Sub
Private Sub Class_Initialize()

    ''' * Objetivo: Crear la coleccion

    Set m_Col = New Collection
    
End Sub
Private Sub Class_Terminate()

    ''' * Objetivo: Limpiar memoria
    Set m_Col = Nothing
    Set m_oConexion = Nothing
End Sub
Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4

    ''' ! Pendiente de revision, objetivo desconocido
    
    Set NewEnum = m_Col.[_NewEnum]

End Property

Public Function EliminarDeBaseDatos(Codigos As Variant) As TipoErrorSummit

'*********************************************************************
'*** Descripci�n: Elimina plantillas de contrato seleccionadas en  ***
'***               el GS de la tabla SOLICITUD_PLANTILLAS de la    ***
'***                base de datos.                                 ***
'*** Parametros:  Codigos :  Es un array con los c�digos de las    ***
'***                         plantillas que se van a eliminar      ***
'*** Devuelve: Una estructura de tipo TipoErrorSummit en la que    ***
'***                se guardan los datos si se produce algun error ***
'***                                                               ***
'*********************************************************************
Dim udtTESError As TipoErrorSummit
Dim i As Integer

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
udtTESError.NumError = TESnoerror

If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
    m_oConexion.ADOCon.Execute "SET XACT_ABORT OFF"
    
    For i = 1 To UBound(Codigos)
        m_oConexion.ADOCon.Execute "DELETE FROM SOLICITUD_PLANTILLAS WHERE ID=" & Codigos(i)
        If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    Next
    
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"

    EliminarDeBaseDatos = udtTESError
    Exit Function

Error_Cls:
    'No hay posiblidad de que exista en tablas relacionadas porque no las hay
    'si se pusiera este dato en alguna tabla hay que repasar esto
      udtTESError = basErrores.TratarError(m_oConexion.ADOCon.Errors)
      m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
      EliminarDeBaseDatos = udtTESError
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CSolicitPlantillas", "EliminarDeBaseDatos", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If
      
End Function

