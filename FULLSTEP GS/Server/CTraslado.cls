VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CTraslado"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
''' *** Clase: CTraslado
''' *** Creacion: 11/06/2004 (MMV)

Option Explicit

Private m_lID As Long

Private m_sProveERP As String
Private m_dFecCierre As Date
Private m_sProve As String
Private m_oProceso As CProceso
Private m_lEmpresa As Long
Private m_iEstado As Integer
Private m_oProvesRelacionados As CItemProves 'Colecci�n de proveedores relacionados por defecto


Private m_lIndice As Long

''' Conexion
Private m_oConexion As CConexion

''' Control de errores
Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

Friend Property Set Conexion(ByVal vData As CConexion)

    Set m_oConexion = vData
    
End Property

Friend Property Get Conexion() As CConexion

    Set Conexion = m_oConexion
    
End Property

Private Sub Class_Terminate()
    Set m_oProceso = Nothing
    Set m_oConexion = Nothing
End Sub

Public Property Get ID() As Long
    ID = m_lID
End Property

Public Property Let ID(ByVal vVar As Long)
    m_lID = vVar
End Property

Public Property Get Estado() As Integer
    Estado = m_iEstado
End Property

Public Property Let Estado(ByVal vVar As Integer)
    m_iEstado = vVar
End Property

Public Property Get FecCierre() As Date
    FecCierre = m_dFecCierre
End Property

Public Property Let FecCierre(ByVal vVar As Date)
    m_dFecCierre = vVar
End Property

Public Property Get ProveERP() As String
    ProveERP = m_sProveERP
End Property

Public Property Let ProveERP(ByVal vVar As String)
    m_sProveERP = vVar
End Property

Public Property Get Prove() As String
    Prove = m_sProve
End Property

Public Property Let Prove(ByVal vVar As String)
    m_sProve = vVar
End Property

Public Property Set Proceso(ByVal oProce As CProceso)
    Set m_oProceso = oProce
End Property

Public Property Get Proceso() As CProceso
    Set Proceso = m_oProceso
End Property

Public Property Get Empresa() As Long
    Empresa = m_lEmpresa
End Property

Public Property Let Empresa(ByVal vVar As Long)
    m_lEmpresa = vVar
End Property

Public Property Set ProvesRelacionados(ByVal oProvesRelacionados As CItemProves)
    Set m_oProvesRelacionados = oProvesRelacionados
End Property

Public Property Get ProvesRelacionados() As CItemProves
    Set ProvesRelacionados = m_oProvesRelacionados
End Property


Public Property Get Indice() As Long

    Indice = m_lIndice
    
End Property
Public Property Let Indice(ByVal iInd As Long)

    m_lIndice = iInd
    
End Property







