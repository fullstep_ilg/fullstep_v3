VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CConfVistaVisorSol"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements IBaseDatos

Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum

Private m_sUsu As String

'Ancho Columnas
Private m_dblTipoWidth As Double
Private m_dblAltaWidth As Double
Private m_dblNecesidadWidth As Double
Private m_dblIdentWidth As Double
Private m_dblDenominacionWidth As Double
Private m_dblImporteWidth As Double
Private m_dblPeticionarioWidth As Double
Private m_dblUONWidth As Double
Private m_dblEstadoWidth As Double
Private m_dblAsignadoAWidth As Double
Private m_dblDepartamentoWidth As Double
Private m_dblSinProcesoWidth As Double
Private m_dblSinPedidoWidth As Double
Private m_dblSinIntegracionWidth As Double
Private m_dblNumSolErpWidth As Double

'
Private m_bHayCambios As Boolean

'Valores
Private m_vTipo As Variant
Private m_vIdentificador As Variant
Private m_vDescripcion As Variant
Private m_vPeticionario As Variant
Private m_vDesde As Variant
Private m_vHasta As Variant
Private m_vUON1 As Variant
Private m_vUON2 As Variant
Private m_vUON3 As Variant
Private m_vEnCursoAprobacion  As Variant
Private m_vPendientes  As Variant
Private m_vAprobadas  As Variant
Private m_vRechazadas  As Variant
Private m_vAnuladas  As Variant
Private m_vCerradas  As Variant
Private m_sDepartamento As String
Private m_lEmpresa As Long
Private m_dImporteDesde As Double
Private m_dImporteHasta As Double
Private m_sCodArticulo As String
Private m_sDenArticulo As String
Private m_sGMN1 As String
Private m_sGMN2 As String
Private m_sGMN3 As String
Private m_sGMN4 As String
Private m_sComprador As String
Private m_bAlertaSinProceso As Boolean
Private m_bAlertaSinPedido As Boolean
Private m_bAlertaIntegracion As Boolean
Private m_sUON1_CC As String
Private m_sUON2_CC As String
Private m_sUON3_CC As String
Private m_sUON4_CC As String
Private m_sProveedor As String

Private m_oConexion As CConexion
Private m_oConexionServer As CConexionDeUseServer
Private m_vIndice As Variant
Private m_adores As adodb.Recordset

Public Property Let Usuario(ByVal sUsuario As String)
    m_sUsu = sUsuario
End Property

Public Property Get Usuario() As String
    Usuario = m_sUsu
End Property

'Ancho Columna ------------------------------------------------------------
Public Property Let TipoWidth(ByVal dato As Double)
    m_dblTipoWidth = dato
End Property
Public Property Get TipoWidth() As Double
    TipoWidth = m_dblTipoWidth
End Property
'Ancho Columna ------------------------------------------------------------
Public Property Let AltaWidth(ByVal dato As Double)
    m_dblAltaWidth = dato
End Property
Public Property Get AltaWidth() As Double
    AltaWidth = m_dblAltaWidth
End Property
'Ancho Columna ------------------------------------------------------------
Public Property Let NecesidadWidth(ByVal dato As Double)
    m_dblNecesidadWidth = dato
End Property
Public Property Get NecesidadWidth() As Double
    NecesidadWidth = m_dblNecesidadWidth
End Property
'Ancho Columna ------------------------------------------------------------
Public Property Let IdentWidth(ByVal dato As Double)
    m_dblIdentWidth = dato
End Property
Public Property Get IdentWidth() As Double
    IdentWidth = m_dblIdentWidth
End Property
'Ancho Columna ------------------------------------------------------------
Public Property Let DenominacionWidth(ByVal dato As Double)
    m_dblDenominacionWidth = dato
End Property
Public Property Get DenominacionWidth() As Double
    DenominacionWidth = m_dblDenominacionWidth
End Property
'Ancho Columna ------------------------------------------------------------
Public Property Let ImporteWidth(ByVal dato As Double)
    m_dblImporteWidth = dato
End Property
Public Property Get ImporteWidth() As Double
    ImporteWidth = m_dblImporteWidth
End Property
'Ancho Columna ------------------------------------------------------------
Public Property Let PeticionarioWidth(ByVal dato As Double)
    m_dblPeticionarioWidth = dato
End Property
Public Property Get PeticionarioWidth() As Double
    PeticionarioWidth = m_dblPeticionarioWidth
End Property
'Ancho Columna ------------------------------------------------------------
Public Property Let UONWidth(ByVal dato As Double)
    m_dblUONWidth = dato
End Property
Public Property Get UONWidth() As Double
    UONWidth = m_dblUONWidth
End Property
'Ancho Columna ------------------------------------------------------------
Public Property Let EstadoWidth(ByVal dato As Double)
    m_dblEstadoWidth = dato
End Property
Public Property Get EstadoWidth() As Double
    EstadoWidth = m_dblEstadoWidth
End Property
'Ancho Columna ------------------------------------------------------------
Public Property Let AsignadoAWidth(ByVal dato As Double)
    m_dblAsignadoAWidth = dato
End Property
Public Property Get AsignadoAWidth() As Double
    AsignadoAWidth = m_dblAsignadoAWidth
End Property
'Ancho Columna ------------------------------------------------------------
Public Property Let DepartamentoWidth(ByVal dato As Double)
    m_dblDepartamentoWidth = dato
End Property
Public Property Get DepartamentoWidth() As Double
    DepartamentoWidth = m_dblDepartamentoWidth
End Property
'Ancho Columna ------------------------------------------------------------
Public Property Let SinProcesoWidth(ByVal dato As Double)
    m_dblSinProcesoWidth = dato
End Property
Public Property Get SinProcesoWidth() As Double
    SinProcesoWidth = m_dblSinProcesoWidth
End Property
'Ancho Columna ------------------------------------------------------------
Public Property Let SinPedidoWidth(ByVal dato As Double)
    m_dblSinPedidoWidth = dato
End Property
Public Property Get SinPedidoWidth() As Double
    SinPedidoWidth = m_dblSinPedidoWidth
End Property
'Ancho Columna ------------------------------------------------------------
Public Property Let SinIntegracionWidth(ByVal dato As Double)
    m_dblSinIntegracionWidth = dato
End Property
Public Property Get SinIntegracionWidth() As Double
    SinIntegracionWidth = m_dblSinIntegracionWidth
End Property

'Valor ------------------------------------------------------------------
Public Property Let Tipo(ByVal dato As Variant)
    m_vTipo = dato
End Property
Public Property Get Tipo() As Variant
    Tipo = m_vTipo
End Property

'Valor ------------------------------------------------------------------
Public Property Let Identificador(ByVal dato As Variant)
    m_vIdentificador = dato
End Property
Public Property Get Identificador() As Variant
    Identificador = m_vIdentificador
End Property

'Valor ------------------------------------------------------------------
Public Property Let Descripcion(ByVal dato As Variant)
    m_vDescripcion = dato
End Property
Public Property Get Descripcion() As Variant
    Descripcion = m_vDescripcion
End Property

'Valor ------------------------------------------------------------------
Public Property Let Peticionario(ByVal dato As Variant)
    m_vPeticionario = dato
End Property
Public Property Get Peticionario() As Variant
    Peticionario = m_vPeticionario
End Property

'Valor ------------------------------------------------------------------
Public Property Let Desde(ByVal dato As Variant)
    m_vDesde = dato
End Property
Public Property Get Desde() As Variant
    Desde = m_vDesde
End Property

'Valor ------------------------------------------------------------------
Public Property Let Hasta(ByVal dato As Variant)
    m_vHasta = dato
End Property
Public Property Get Hasta() As Variant
    Hasta = m_vHasta
End Property

'Valor ------------------------------------------------------------------
Public Property Let UON1(ByVal dato As Variant)
    m_vUON1 = dato
End Property
Public Property Get UON1() As Variant
    UON1 = m_vUON1
End Property

'Valor ------------------------------------------------------------------
Public Property Let UON2(ByVal dato As Variant)
    m_vUON2 = dato
End Property
Public Property Get UON2() As Variant
    UON2 = m_vUON2
End Property

'Valor ------------------------------------------------------------------
Public Property Let UON3(ByVal dato As Variant)
    m_vUON3 = dato
End Property
Public Property Get UON3() As Variant
    UON3 = m_vUON3
End Property

'Valor ------------------------------------------------------------------
Public Property Let EnCursoAprobacion(ByVal dato As Variant)
    m_vEnCursoAprobacion = dato
End Property
Public Property Get EnCursoAprobacion() As Variant
    EnCursoAprobacion = m_vEnCursoAprobacion
End Property

'Valor ------------------------------------------------------------------
Public Property Let Pendientes(ByVal dato As Variant)
    m_vPendientes = dato
End Property
Public Property Get Pendientes() As Variant
    Pendientes = m_vPendientes
End Property

'Valor ------------------------------------------------------------------
Public Property Let Aprobadas(ByVal dato As Variant)
    m_vAprobadas = dato
End Property
Public Property Get Aprobadas() As Variant
    Aprobadas = m_vAprobadas
End Property

'Valor ------------------------------------------------------------------
Public Property Let Rechazadas(ByVal dato As Variant)
    m_vRechazadas = dato
End Property
Public Property Get Rechazadas() As Variant
    Rechazadas = m_vRechazadas
End Property

'Valor ------------------------------------------------------------------
Public Property Let Anuladas(ByVal dato As Variant)
    m_vAnuladas = dato
End Property
Public Property Get Anuladas() As Variant
    Anuladas = m_vAnuladas
End Property
'Valor ------------------------------------------------------------------
Public Property Let Cerradas(ByVal dato As Variant)
    m_vCerradas = dato
End Property
Public Property Get Cerradas() As Variant
    Cerradas = m_vCerradas
End Property
'Valor ------------------------------------------------------------------
Public Property Let Departamento(ByVal dato As String)
    m_sDepartamento = dato
End Property
Public Property Get Departamento() As String
    Departamento = m_sDepartamento
End Property
'Valor ------------------------------------------------------------------
Public Property Let Empresa(ByVal dato As Long)
    m_lEmpresa = dato
End Property
Public Property Get Empresa() As Long
    Empresa = m_lEmpresa
End Property
'Valor ------------------------------------------------------------------
Public Property Let ImporteDesde(ByVal dato As Double)
    m_dImporteDesde = dato
End Property
Public Property Get ImporteDesde() As Double
    ImporteDesde = m_dImporteDesde
End Property
'Valor ------------------------------------------------------------------
Public Property Let ImporteHasta(ByVal dato As Double)
    m_dImporteHasta = dato
End Property
Public Property Get ImporteHasta() As Double
    ImporteHasta = m_dImporteHasta
End Property
'Valor ------------------------------------------------------------------
Public Property Let CodArticulo(ByVal dato As String)
    m_sCodArticulo = dato
End Property
Public Property Get CodArticulo() As String
    CodArticulo = m_sCodArticulo
End Property
'Valor ------------------------------------------------------------------
Public Property Let DenArticulo(ByVal dato As String)
    m_sDenArticulo = dato
End Property
Public Property Get DenArticulo() As String
    DenArticulo = m_sDenArticulo
End Property
'Valor ------------------------------------------------------------------
Public Property Let GMN1(ByVal dato As String)
    m_sGMN1 = dato
End Property
Public Property Get GMN1() As String
    GMN1 = m_sGMN1
End Property
'Valor ------------------------------------------------------------------
Public Property Let GMN2(ByVal dato As String)
    m_sGMN2 = dato
End Property
Public Property Get GMN2() As String
    GMN2 = m_sGMN2
End Property
'Valor ------------------------------------------------------------------
Public Property Let GMN3(ByVal dato As String)
    m_sGMN3 = dato
End Property
Public Property Get GMN3() As String
    GMN3 = m_sGMN3
End Property
'Valor ------------------------------------------------------------------
Public Property Let GMN4(ByVal dato As String)
    m_sGMN4 = dato
End Property
Public Property Get GMN4() As String
    GMN4 = m_sGMN4
End Property
'Valor ------------------------------------------------------------------
Public Property Let Comprador(ByVal dato As String)
    m_sComprador = dato
End Property
Public Property Get Comprador() As String
    Comprador = m_sComprador
End Property
'Valor ------------------------------------------------------------------
Public Property Let AlertaSinProceso(ByVal dato As Boolean)
    m_bAlertaSinProceso = dato
End Property
Public Property Get AlertaSinProceso() As Boolean
    AlertaSinProceso = m_bAlertaSinProceso
End Property
'Valor ------------------------------------------------------------------
Public Property Let AlertaSinPedido(ByVal dato As Boolean)
    m_bAlertaSinPedido = dato
End Property
Public Property Get AlertaSinPedido() As Boolean
    AlertaSinPedido = m_bAlertaSinPedido
End Property
'Valor ------------------------------------------------------------------
Public Property Let AlertaIntegracion(ByVal dato As Boolean)
    m_bAlertaIntegracion = dato
End Property
Public Property Get AlertaIntegracion() As Boolean
    AlertaIntegracion = m_bAlertaIntegracion
End Property
Public Property Let UON1_CC(ByVal dato As String)
    m_sUON1_CC = dato
End Property
Public Property Get UON1_CC() As String
    UON1_CC = m_sUON1_CC
End Property

Public Property Let UON2_CC(ByVal dato As String)
    m_sUON2_CC = dato
End Property
Public Property Get UON2_CC() As String
    UON2_CC = m_sUON2_CC
End Property

Public Property Let UON3_CC(ByVal dato As String)
    m_sUON3_CC = dato
End Property
Public Property Get UON3_CC() As String
    UON3_CC = m_sUON3_CC
End Property

Public Property Let UON4_CC(ByVal dato As String)
    m_sUON4_CC = dato
End Property
Public Property Get UON4_CC() As String
    UON4_CC = m_sUON4_CC
End Property



Friend Property Set ConexionServer(ByVal con As CConexionDeUseServer)
    Set m_oConexionServer = con
End Property

Friend Property Get ConexionServer() As CConexionDeUseServer
    Set ConexionServer = m_oConexionServer
End Property

Friend Property Set Conexion(ByVal con As CConexion)
    Set m_oConexion = con
End Property

Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property

Public Property Let Indice(ByVal vIndice As Variant)
    m_vIndice = vIndice
End Property
Public Property Get Indice() As Variant
    Indice = m_vIndice
End Property

''' <summary>
''' A�ade un registro a la tabla CONF_VISOR_SOL con todos los campos de configuraci�n del visor de solicitudes
''' </summary>
''' <returns>Un TipoErrorSummit en el caso de que haya habido error</returns>
''' <remarks>Llamada desde:frmSolicitudes.GuardarConfiguracionVisorSol; Tiempo m�ximo:0,1</remarks>
''' Revisado por: jbg; Fecha: 07/02/2013
Private Function IBaseDatos_AnyadirABaseDatos() As TipoErrorSummit
Dim sConsulta As String
Dim TESError As TipoErrorSummit

'********* Precondicion *******************
If m_oConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "cConfVistaVisorSol.AnyadirABAseDatos", "No se ha establecido la conexion"
    Exit Function
End If
'******************************************
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
    
    TESError.NumError = TESnoerror
    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
            

    sConsulta = "INSERT INTO CONF_VISOR_SOL (USU,TIPO_WIDTH, ALTA_WIDTH, NECESIDAD_WIDTH, IDENT_WIDTH, DENOMINACION_WIDTH, IMPORTE_WIDTH," _
    & "PETICIONARIO_WIDTH, UON_WIDTH, ESTADO_WIDTH , ASIGNADOA_WIDTH, DEP_WIDTH, SIN_PROCESO_WIDTH, SIN_PEDIDO_WIDTH, SIN_INTEGRACION_WIDTH," _
    & "TIPO, IDENTIFICADOR, DESCRIPCION, PETICIONARIO, DESDE, HASTA," _
    & "UON1,UON2,UON3,ENCURSOAPROBACION,PENDIENTES,APROBADAS,RECHAZADAS,ANULADAS,CERRADAS,DEPARTAMENTO,EMPRESA,IMPORTE_DESDE,IMPORTE_HASTA,COD_ART," _
    & "DEN_ART,GMN1,GMN2,GMN3,GMN4,COMPRADOR,ALERTA_SINPROC,ALERTA_SINPED,ALERTA_INT,UON1_CC,UON2_CC,UON3_CC,UON4_CC,PROVE,NUM_SOL_ERP_WIDTH)"
        
    sConsulta = sConsulta & " VALUES ('" & DblQuote(m_sUsu) & "'," & DblToSQLFloat(m_dblTipoWidth)
    sConsulta = sConsulta & "," & DblToSQLFloat(m_dblAltaWidth)
    sConsulta = sConsulta & "," & DblToSQLFloat(m_dblNecesidadWidth)
    sConsulta = sConsulta & "," & DblToSQLFloat(m_dblIdentWidth)
    sConsulta = sConsulta & "," & DblToSQLFloat(m_dblDenominacionWidth)
    sConsulta = sConsulta & "," & DblToSQLFloat(m_dblImporteWidth)
    sConsulta = sConsulta & "," & DblToSQLFloat(m_dblPeticionarioWidth)
    sConsulta = sConsulta & "," & DblToSQLFloat(m_dblUONWidth)
    sConsulta = sConsulta & "," & DblToSQLFloat(m_dblEstadoWidth)
    sConsulta = sConsulta & "," & DblToSQLFloat(m_dblAsignadoAWidth)
    sConsulta = sConsulta & "," & DblToSQLFloat(m_dblDepartamentoWidth)
    sConsulta = sConsulta & "," & DblToSQLFloat(m_dblSinProcesoWidth)
    sConsulta = sConsulta & "," & DblToSQLFloat(m_dblSinPedidoWidth)
    sConsulta = sConsulta & "," & DblToSQLFloat(m_dblSinIntegracionWidth)
    
    sConsulta = sConsulta & "," & StrToSQLNULL(m_vTipo)
    sConsulta = sConsulta & "," & StrToSQLNULL(m_vIdentificador)
    sConsulta = sConsulta & "," & StrToSQLNULL(m_vDescripcion)
    sConsulta = sConsulta & "," & StrToSQLNULL(m_vPeticionario)
    sConsulta = sConsulta & "," & DateToSQLDate(m_vDesde)
    sConsulta = sConsulta & "," & DateToSQLDate(m_vHasta)
    sConsulta = sConsulta & "," & StrToSQLNULL(m_vUON1)
    sConsulta = sConsulta & "," & StrToSQLNULL(m_vUON2)
    sConsulta = sConsulta & "," & StrToSQLNULL(m_vUON3)
    sConsulta = sConsulta & "," & BooleanToSQLBinary(m_vEnCursoAprobacion)
    sConsulta = sConsulta & "," & BooleanToSQLBinary(m_vPendientes)
    sConsulta = sConsulta & "," & BooleanToSQLBinary(m_vAprobadas)
    sConsulta = sConsulta & "," & BooleanToSQLBinary(m_vRechazadas)
    sConsulta = sConsulta & "," & BooleanToSQLBinary(m_vAnuladas)
    sConsulta = sConsulta & "," & BooleanToSQLBinary(m_vCerradas)
    sConsulta = sConsulta & "," & StrToSQLNULL(m_sDepartamento)
    sConsulta = sConsulta & "," & DblToSQLFloat(m_lEmpresa)
    sConsulta = sConsulta & "," & DblToSQLFloat(m_dImporteDesde)
    sConsulta = sConsulta & "," & DblToSQLFloat(m_dImporteHasta)
    sConsulta = sConsulta & "," & StrToSQLNULL(m_sCodArticulo)
    sConsulta = sConsulta & "," & StrToSQLNULL(m_sDenArticulo)
    sConsulta = sConsulta & "," & StrToSQLNULL(m_sGMN1)
    sConsulta = sConsulta & "," & StrToSQLNULL(m_sGMN2)
    sConsulta = sConsulta & "," & StrToSQLNULL(m_sGMN3)
    sConsulta = sConsulta & "," & StrToSQLNULL(m_sGMN4)
    sConsulta = sConsulta & "," & StrToSQLNULL(m_sComprador)
    sConsulta = sConsulta & "," & BooleanToSQLBinary(m_bAlertaSinProceso)
    sConsulta = sConsulta & "," & BooleanToSQLBinary(m_bAlertaSinPedido)
    sConsulta = sConsulta & "," & BooleanToSQLBinary(m_bAlertaIntegracion)
    sConsulta = sConsulta & "," & StrToSQLNULL(m_sUON1_CC)
    sConsulta = sConsulta & "," & StrToSQLNULL(m_sUON2_CC)
    sConsulta = sConsulta & "," & StrToSQLNULL(m_sUON3_CC)
    sConsulta = sConsulta & "," & StrToSQLNULL(m_sUON4_CC)
    sConsulta = sConsulta & "," & StrToSQLNULL(m_sProveedor)
    sConsulta = sConsulta & "," & DblToSQLFloat(m_dblNumSolErpWidth)
    
    sConsulta = sConsulta & ")"

    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    IBaseDatos_AnyadirABaseDatos = TESError
    Exit Function
    
Error_Cls:
    
    IBaseDatos_AnyadirABaseDatos = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
    On Error Resume Next
    If Not m_adores Is Nothing Then
        m_adores.Close
        Set m_adores = Nothing
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CConfVistaVisorSol", "IBaseDatos_AnyadirABaseDatos", ERR, Erl)
        GoTo Error_Cls
        Exit Function
    End If

End Function


Private Function IBaseDatos_CambiarCodigo(CodigoNuevo As Variant) As TipoErrorSummit

End Function

Private Sub IBaseDatos_CancelarEdicion()

End Sub

''' <summary>
''' Comprueba que existe ya una configuraci�n para ese usuario
''' </summary>
''' <returns>true/false: si existe o no la configuraci�n</returns>
''' <remarks>Llamada desde:frmSolicitudes.GuardarConfiguracionVisorSol; Tiempo m�ximo:0,1</remarks>
Private Function IBaseDatos_ComprobarExistenciaEnBaseDatos() As Boolean
    Dim sConsulta As String
    Dim AdoRes As New adodb.Recordset
    
    '******************* Precondicion *******************
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CConfVisor.ComprobarExistenciaEnBaseDatos", "No se ha establecido la conexion"
        Exit Function
    End If
    '****************************************************
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
    sConsulta = "SELECT COUNT(*) FROM CONF_VISOR_SOL WITH (NOLOCK) WHERE USU='" & DblQuote(m_sUsu) & "'"
        
    Set AdoRes = New adodb.Recordset
    AdoRes.CursorLocation = adUseServer
    AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    If AdoRes.eof Then
        IBaseDatos_ComprobarExistenciaEnBaseDatos = False
    Else
        If AdoRes.Fields(0).Value > 0 Then
            IBaseDatos_ComprobarExistenciaEnBaseDatos = True
        Else
            IBaseDatos_ComprobarExistenciaEnBaseDatos = False
        End If
    End If

    AdoRes.Close
    Set AdoRes = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CConfVistaVisorSol", "IBaseDatos_ComprobarExistenciaEnBaseDatos", ERR, Erl)
        Exit Function
    End If
End Function


Private Function IBaseDatos_EliminarDeBaseDatos() As TipoErrorSummit
Dim sConsulta As String
Dim TESError As TipoErrorSummit
    '''Precondicion
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "cConfVistaVisorSol.EliminarDeBaseDatos", "No se ha establecido la conexion"
        Exit Function
    End If
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
    
If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls

    TESError.NumError = TESnoerror
     
    sConsulta = "DELETE FROM CONF_VISOR_SOL WHERE USU='" & DblQuote(m_sUsu) & "'"
    
    
    m_oConexion.ADOCon.Execute sConsulta

    IBaseDatos_EliminarDeBaseDatos = TESError

    Exit Function

Error_Cls:
    IBaseDatos_EliminarDeBaseDatos = basErrores.TratarError(m_oConexion.ADOCon.Errors)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CConfVistaVisorSol", "IBaseDatos_EliminarDeBaseDatos", ERR, Erl)
        GoTo Error_Cls
        Exit Function
    End If

End Function


Private Function IBaseDatos_FinalizarEdicionEliminando() As TipoErrorSummit

End Function

''' <summary>
''' Actualiza un registro a la tabla CONF_VISOR_SOL con todos los campos de configuraci�n del visor de solicitudes
''' </summary>
''' <returns>Un TipoErrorSummit en el caso de que haya habido error</returns>
''' <remarks>Llamada desde:frmSolicitudes.GuardarConfiguracionVisorSol; Tiempo m�ximo:0,1</remarks>
''' Revisado por: jbg; Fecha: 07/02/2013
Private Function IBaseDatos_FinalizarEdicionModificando() As TipoErrorSummit
Dim TESError As TipoErrorSummit
Dim sConsulta As String

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls
    TESError.NumError = TESnoerror

    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
    sConsulta = "UPDATE CONF_VISOR_SOL SET "
    sConsulta = sConsulta & " TIPO_WIDTH = " & DblToSQLFloat(m_dblTipoWidth)
    sConsulta = sConsulta & " ,ALTA_WIDTH=" & DblToSQLFloat(m_dblAltaWidth)
    sConsulta = sConsulta & " ,NECESIDAD_WIDTH=" & DblToSQLFloat(m_dblNecesidadWidth)
    sConsulta = sConsulta & " ,IDENT_WIDTH=" & DblToSQLFloat(m_dblIdentWidth)
    sConsulta = sConsulta & " ,DENOMINACION_WIDTH=" & DblToSQLFloat(m_dblDenominacionWidth)
    sConsulta = sConsulta & " ,IMPORTE_WIDTH=" & DblToSQLFloat(m_dblImporteWidth)
    sConsulta = sConsulta & " ,PETICIONARIO_WIDTH=" & DblToSQLFloat(m_dblPeticionarioWidth)
    sConsulta = sConsulta & " ,UON_WIDTH=" & DblToSQLFloat(m_dblUONWidth)
    sConsulta = sConsulta & " ,ESTADO_WIDTH=" & DblToSQLFloat(m_dblEstadoWidth)
    sConsulta = sConsulta & " ,ASIGNADOA_WIDTH=" & DblToSQLFloat(m_dblAsignadoAWidth)
    sConsulta = sConsulta & ",DEP_WIDTH=" & DblToSQLFloat(m_dblDepartamentoWidth)
    sConsulta = sConsulta & ",SIN_PROCESO_WIDTH=" & DblToSQLFloat(m_dblSinProcesoWidth)
    sConsulta = sConsulta & ",SIN_PEDIDO_WIDTH=" & DblToSQLFloat(m_dblSinPedidoWidth)
    sConsulta = sConsulta & ",SIN_INTEGRACION_WIDTH=" & DblToSQLFloat(m_dblSinIntegracionWidth)
    
    sConsulta = sConsulta & " ,TIPO=" & StrToSQLNULL(m_vTipo)
    sConsulta = sConsulta & " ,IDENTIFICADOR=" & StrToSQLNULL(m_vIdentificador)
    sConsulta = sConsulta & " ,DESCRIPCION=" & StrToSQLNULL(m_vDescripcion)
    sConsulta = sConsulta & " ,PETICIONARIO=" & StrToSQLNULL(m_vPeticionario)
    sConsulta = sConsulta & " ,DESDE=" & DateToSQLDate(m_vDesde)
    sConsulta = sConsulta & " ,HASTA=" & DateToSQLDate(m_vHasta)
    sConsulta = sConsulta & " ,UON1=" & StrToSQLNULL(m_vUON1)
    sConsulta = sConsulta & " ,UON2=" & StrToSQLNULL(m_vUON2)
    sConsulta = sConsulta & " ,UON3=" & StrToSQLNULL(m_vUON3)
    sConsulta = sConsulta & " ,ENCURSOAPROBACION =" & BooleanToSQLBinary(m_vEnCursoAprobacion)
    sConsulta = sConsulta & " ,PENDIENTES =" & BooleanToSQLBinary(m_vPendientes)
    sConsulta = sConsulta & " ,APROBADAS =" & BooleanToSQLBinary(m_vAprobadas)
    sConsulta = sConsulta & " ,RECHAZADAS =" & BooleanToSQLBinary(m_vRechazadas)
    sConsulta = sConsulta & " ,ANULADAS =" & BooleanToSQLBinary(m_vAnuladas)
    sConsulta = sConsulta & " ,CERRADAS=" & BooleanToSQLBinary(m_vCerradas)
    sConsulta = sConsulta & " ,DEPARTAMENTO=" & StrToSQLNULL(m_sDepartamento)
    sConsulta = sConsulta & " ,EMPRESA=" & DblToSQLFloat(m_lEmpresa)
    sConsulta = sConsulta & " ,IMPORTE_DESDE=" & DblToSQLFloat(m_dImporteDesde)
    sConsulta = sConsulta & " ,IMPORTE_HASTA=" & DblToSQLFloat(m_dImporteHasta)
    sConsulta = sConsulta & " ,COD_ART=" & StrToSQLNULL(m_sCodArticulo)
    sConsulta = sConsulta & " ,DEN_ART=" & StrToSQLNULL(m_sDenArticulo)
    sConsulta = sConsulta & " ,GMN1=" & StrToSQLNULL(m_sGMN1)
    sConsulta = sConsulta & " ,GMN2=" & StrToSQLNULL(m_sGMN2)
    sConsulta = sConsulta & " ,GMN3=" & StrToSQLNULL(m_sGMN3)
    sConsulta = sConsulta & " ,GMN4=" & StrToSQLNULL(m_sGMN4)
    sConsulta = sConsulta & " ,COMPRADOR=" & StrToSQLNULL(m_sComprador)
    sConsulta = sConsulta & " ,ALERTA_SINPROC=" & BooleanToSQLBinary(m_bAlertaSinProceso)
    sConsulta = sConsulta & " ,ALERTA_SINPED=" & BooleanToSQLBinary(m_bAlertaSinPedido)
    sConsulta = sConsulta & " ,ALERTA_INT=" & BooleanToSQLBinary(m_bAlertaIntegracion)
    sConsulta = sConsulta & " ,UON1_CC=" & StrToSQLNULL(m_sUON1_CC)
    sConsulta = sConsulta & " ,UON2_CC=" & StrToSQLNULL(m_sUON2_CC)
    sConsulta = sConsulta & " ,UON3_CC=" & StrToSQLNULL(m_sUON3_CC)
    sConsulta = sConsulta & " ,UON4_CC=" & StrToSQLNULL(m_sUON4_CC)
    sConsulta = sConsulta & " ,PROVE=" & StrToSQLNULL(m_sProveedor)
    sConsulta = sConsulta & " ,NUM_SOL_ERP_WIDTH=" & DblToSQLFloat(m_dblNumSolErpWidth)
    sConsulta = sConsulta & " WHERE USU='" & DblQuote(m_sUsu) & "'"
    
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"

    IBaseDatos_FinalizarEdicionModificando = TESError

    Exit Function

Error_Cls:
    IBaseDatos_FinalizarEdicionModificando = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"

    On Error Resume Next
    If Not m_adores Is Nothing Then
        m_adores.Close
        Set m_adores = Nothing
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CConfVistaVisorSol", "IBaseDatos_FinalizarEdicionModificando", ERR, Erl)
        GoTo Error_Cls
        Exit Function
    End If

End Function

Private Function IBaseDatos_IniciarEdicion(Optional ByVal Bloquear As Boolean, Optional ByVal UsuarioBloqueo As String) As TipoErrorSummit


End Function

Private Sub Class_Terminate()
On Error GoTo Error_Cls:
    
    Set m_oConexionServer = Nothing
    Set m_oConexion = Nothing
    Exit Sub
Error_Cls:
    Resume Next
End Sub


Public Property Let HayCambios(ByVal dato As Boolean)
    m_bHayCambios = dato
End Property

Public Property Get HayCambios() As Boolean
    HayCambios = m_bHayCambios
End Property

''' <summary>
''' Establece Proveedor de la vista
''' </summary>
''' <param name="dato">Proveedor de la vista</param>
Public Property Let Proveedor(ByVal dato As String)
    m_sProveedor = dato
End Property
''' <summary>
''' Devuelve Proveedor de la vista
''' </summary>
''' <returns>Proveedor de la vista</returns>
Public Property Get Proveedor() As String
    Proveedor = m_sProveedor
End Property

Public Property Get NumSolErpWidth() As Double

    NumSolErpWidth = m_dblNumSolErpWidth

End Property

Public Property Let NumSolErpWidth(ByVal dNumSolErp As Double)

    m_dblNumSolErpWidth = dNumSolErp

End Property
