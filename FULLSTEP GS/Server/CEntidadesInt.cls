VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CEntidadesInt"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CEntidadesInt ****************************
'*             Creada : 27/05/02
'****************************************************************

Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum
Private mCol As Collection
Private m_oConexion As CConexion
Public m_bEOF As Boolean

''' <summary>
''' Carga las entidades de integraci�n de cada erp
''' </summary>
''' <param name="erp">C�digo del erp</param>
''' <remarks>Llamada desde: FSGSClient.frmPedidos.blnTieneERP, FSGSClient.frmINTFTP.cmdAceptar_Clik
'''  FSGSClient.frmINTOrigen.cmdAceptar_Clik,FSGSClient.frmINTOrigen.sdbcERP_CloseUp,
'''  FSGSClient.frmINTDestino.cmdAceptar_Clik,FSGSClient.frmINTDestino.sdbcERP_CloseUp,
'''  FSGSClient.frmConfIntegracion.cmdEdicion_Click, FSGSClient.frmConfIntegracion.Form_Load, FSGSClient.frmConfIntegracion.Form_Unload,FSGSClient.frmConfIntegracion.sdbERP_CloseUp, FSGSClient.frmConfIntegracion.sdbcERP_CloseUp
''' Tiempo m�ximo:0,1</remarks>
''' <revision>LTG 04/04/2012</revision>

Public Sub CargarTodasLasEntidades(ByVal erp As Variant)
    Dim adoComm As adodb.Command
    Dim adoParam As adodb.Parameter
    Dim AdoRes As adodb.Recordset
    Dim fldEntidad As adodb.Field
    Dim fldSentido As adodb.Field
    Dim fldActiva As adodb.Field
    Dim fldTipoTraduccion As adodb.Field
    Dim fldDestino As adodb.Field
    Dim fldDestinoTipo As adodb.Field
    Dim fldOrigenTipo As adodb.Field
    Dim fldOrigenEntidad As adodb.Field
    Dim fldTablaIntermedia As adodb.Field
    Dim fldOrigenIntermedia As adodb.Field
    Dim fldOrigenRecibosERP As adodb.Field
    Dim fldDestinoRecibosFSGS As adodb.Field
    Dim fldCodERP As adodb.Field
    Dim fldFTP_IP As adodb.Field
    Dim fldFTP_Puerto As adodb.Field
    Dim fldFTP_Usu As adodb.Field
    Dim fldFTP_Pwd As adodb.Field
    Dim fldFTP_Origen As adodb.Field
    Dim fldFTP_Destino As adodb.Field
    Dim fldFTP_Intermedia As adodb.Field
    Dim fldFTP_RecibosFSGS As adodb.Field
    Dim fldFTP_RecibosERP As adodb.Field
    Dim fldTabla As adodb.Field
    Dim fldIdTablaExterna As adodb.Field
    Dim fldTablaExterna As adodb.Field
    Dim fldWEB_Orig_Url As adodb.Field
    Dim fldWEB_Orig_Usu As adodb.Field
    Dim fldWEB_Orig_Pwd As adodb.Field
    Dim fldWEB_Dest_Url As adodb.Field
    Dim fldWEB_Dest_Usu As adodb.Field
    Dim fldWEB_Dest_Pwd As adodb.Field
    Dim fldWCF_Orig_Url As adodb.Field
    Dim fldWCF_Orig_Usu As adodb.Field
    Dim fldWCF_Orig_Pwd As adodb.Field
    Dim fldWCF_Dest_Url As adodb.Field
    Dim fldWCF_Dest_Usu As adodb.Field
    Dim fldWCF_Dest_Pwd As adodb.Field
    Dim fldWCF_Dest_FecPwd As adodb.Field
    Dim fldWCF_Orig_FecPwd As adodb.Field
    
    Dim sConsulta As String
    Dim lIndice As Long

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set adoComm = New adodb.Command
    Set adoComm.ActiveConnection = m_oConexion.ADOCon
    adoComm.CommandType = adCmdText

    sConsulta = "SELECT ISNULL(ENTIDAD,0) ENTIDAD, ISNULL(ACTIVA,0) ACTIVA, SENTIDO, TRADUCCION, ISNULL(TABLA_INTERMEDIA,0) TABLA_INTERMEDIA, ? ERP, DESTINO, DESTINO_TIPO, ORIGEN_TIPO, ORIGEN_ENTIDAD, ORIGEN_INTERMEDIA, RECIBOS_ERP, RECIBOS_FSGS, FTP_IP, FTP_PUERTO, FTP_USU, FTP_PWD, FTP_ORIGEN, FTP_DESTINO, FTP_RECIBOS_FSGS, FTP_RECIBOS_ERP, FTP_INTERMEDIA, TABLA, ID_TABLA_EXTERNA, TABLA_EXTERNA, WEBSERVICE_ORIG_URL ,WEBSERVICE_ORIG_USU, WEBSERVICE_ORIG_PWD, WEBSERVICE_DEST_URL ,WEBSERVICE_DEST_USU, WEBSERVICE_DEST_PWD, WCF_ORIG_URL ,WCF_ORIG_USU, WCF_ORIG_PWD, WCF_ORIG_FECPWD, WCF_ORIG_FECPWD, WCF_DEST_URL ,WCF_DEST_USU, WCF_DEST_PWD, WCF_DEST_FECPWD "
    
    Set adoParam = adoComm.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=4, Value:=StrToVbNULL(erp))
    adoComm.Parameters.Append adoParam
    
    sConsulta = sConsulta & " FROM ("
    sConsulta = sConsulta & " SELECT DISTINCT TIE.ENTIDAD, ISNULL(TIE.ACTIVA,0) ACTIVA, TIE.SENTIDO, TIE.TRADUCCION,   ISNULL(TIE.TABLA_INTERMEDIA,0) TABLA_INTERMEDIA, TIE.DESTINO, TIE.DESTINO_TIPO, TIE.ORIGEN_TIPO,TIE.ORIGEN_ENTIDAD, TIE.ORIGEN_INTERMEDIA, TIE.RECIBOS_ERP, TIE.RECIBOS_FSGS, TIE.FTP_IP, TIE.FTP_PUERTO,TIE.FTP_USU, TIE.FTP_PWD, TIE.FTP_ORIGEN, TIE.FTP_DESTINO, TIE.FTP_RECIBOS_FSGS, TIE.FTP_RECIBOS_ERP,TIE.FTP_INTERMEDIA, TIE.TABLA, 0 AS ID_TABLA_EXTERNA, NULL AS TABLA_EXTERNA, TIE.WEBSERVICE_ORIG_URL ,TIE.WEBSERVICE_ORIG_USU,TIE.WEBSERVICE_ORIG_PWD, TIE.WEBSERVICE_DEST_URL ,TIE.WEBSERVICE_DEST_USU,TIE.WEBSERVICE_DEST_PWD, WCF_ORIG_URL ,WCF_ORIG_USU, WCF_ORIG_PWD, WCF_ORIG_FECPWD, WCF_DEST_URL ,WCF_DEST_USU, WCF_DEST_PWD, WCF_DEST_FECPWD "
    sConsulta = sConsulta & " FROM TABLAS_INTEGRACION_ERP TIE WITH (NOLOCK)"
    sConsulta = sConsulta & " WHERE TIE.ERP = ? AND TIE.TABLA <> " & EntidadIntegracion.TablasExternas
    
    Set adoParam = adoComm.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=4, Value:=StrToVbNULL(erp))
    adoComm.Parameters.Append adoParam
    
    sConsulta = sConsulta & " UNION"
    sConsulta = sConsulta & " SELECT TIE.ENTIDAD, ISNULL(TIE.ACTIVA,0) ACTIVA, TIE.SENTIDO, TIE.TRADUCCION, ISNULL(TIE.TABLA_INTERMEDIA,0) TABLA_INTERMEDIA, TIE.DESTINO, TIE.DESTINO_TIPO, TIE.ORIGEN_TIPO, TIE.ORIGEN_ENTIDAD, TIE.ORIGEN_INTERMEDIA, TIE.RECIBOS_ERP, TIE.RECIBOS_FSGS, TIE.FTP_IP, TIE.FTP_PUERTO, TIE.FTP_USU, TIE.FTP_PWD, TIE.FTP_ORIGEN, TIE.FTP_DESTINO, TIE.FTP_RECIBOS_FSGS, TIE.FTP_RECIBOS_ERP, TIE.FTP_INTERMEDIA, 24 TABLA,TE.ID AS ID_TABLA_EXTERNA, TE.DENOMINACION AS TABLA_EXTERNA, TIE.WEBSERVICE_ORIG_URL ,TIE.WEBSERVICE_ORIG_USU,TIE.WEBSERVICE_ORIG_PWD, TIE.WEBSERVICE_DEST_URL ,TIE.WEBSERVICE_DEST_USU,TIE.WEBSERVICE_DEST_PWD, WCF_ORIG_URL ,WCF_ORIG_USU, WCF_ORIG_PWD, WCF_ORIG_FECPWD, WCF_DEST_URL ,WCF_DEST_USU, WCF_DEST_PWD, WCF_DEST_FECPWD"
    sConsulta = sConsulta & " FROM TABLAS_EXTERNAS TE WITH (NOLOCK)"
    sConsulta = sConsulta & " INNER JOIN TABLAS_INTEGRACION_ERP TIE WITH (NOLOCK) ON TIE.TABLA_EXTERNA = TE.ID AND TIE.ERP = ? "
    
    Set adoParam = adoComm.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=4, Value:=StrToVbNULL(erp))
    adoComm.Parameters.Append adoParam
    
    sConsulta = sConsulta & " )X ORDER BY ID_TABLA_EXTERNA"
    
    Set AdoRes = New adodb.Recordset

    adoComm.CommandText = sConsulta
    Set AdoRes = adoComm.Execute
      
    If AdoRes.eof Then
        AdoRes.Close
        Set AdoRes = Nothing
    Else
        Set mCol = Nothing
        Set mCol = New Collection
        Set fldEntidad = AdoRes.Fields("ENTIDAD")
        Set fldActiva = AdoRes.Fields("ACTIVA")
        Set fldSentido = AdoRes.Fields("SENTIDO")
        Set fldDestino = AdoRes.Fields("DESTINO")
        Set fldDestinoTipo = AdoRes.Fields("DESTINO_TIPO")
        Set fldTablaIntermedia = AdoRes.Fields("TABLA_INTERMEDIA")
        Set fldOrigenTipo = AdoRes.Fields("ORIGEN_TIPO")
        Set fldOrigenEntidad = AdoRes.Fields("ORIGEN_ENTIDAD")
        Set fldOrigenIntermedia = AdoRes.Fields("ORIGEN_INTERMEDIA")
        Set fldTipoTraduccion = AdoRes.Fields("TRADUCCION")
        Set fldOrigenRecibosERP = AdoRes.Fields("RECIBOS_ERP")
        Set fldDestinoRecibosFSGS = AdoRes.Fields("RECIBOS_FSGS")
        Set fldCodERP = AdoRes.Fields("ERP")
        Set fldFTP_IP = AdoRes.Fields("FTP_IP")
        Set fldFTP_Puerto = AdoRes.Fields("FTP_PUERTO")
        Set fldFTP_Usu = AdoRes.Fields("FTP_USU")
        Set fldFTP_Pwd = AdoRes.Fields("FTP_PWD")
        Set fldFTP_Origen = AdoRes.Fields("FTP_ORIGEN")
        Set fldFTP_Destino = AdoRes.Fields("FTP_DESTINO")
        Set fldFTP_RecibosERP = AdoRes.Fields("FTP_RECIBOS_ERP")
        Set fldFTP_RecibosFSGS = AdoRes.Fields("FTP_RECIBOS_FSGS")
        Set fldFTP_Intermedia = AdoRes.Fields("FTP_INTERMEDIA")
        Set fldTabla = AdoRes.Fields("TABLA")
        Set fldIdTablaExterna = AdoRes.Fields("ID_TABLA_EXTERNA")
        Set fldTablaExterna = AdoRes.Fields("TABLA_EXTERNA")
        Set fldWEB_Orig_Url = AdoRes.Fields("WEBSERVICE_ORIG_URL")
        Set fldWEB_Orig_Usu = AdoRes.Fields("WEBSERVICE_ORIG_USU")
        Set fldWEB_Orig_Pwd = AdoRes.Fields("WEBSERVICE_ORIG_PWD")
        Set fldWEB_Dest_Url = AdoRes.Fields("WEBSERVICE_DEST_URL")
        Set fldWEB_Dest_Usu = AdoRes.Fields("WEBSERVICE_DEST_USU")
        Set fldWEB_Dest_Pwd = AdoRes.Fields("WEBSERVICE_DEST_PWD")
        Set fldWCF_Orig_Url = AdoRes.Fields("WCF_ORIG_URL")
        Set fldWCF_Orig_Usu = AdoRes.Fields("WCF_ORIG_USU")
        Set fldWCF_Orig_Pwd = AdoRes.Fields("WCF_ORIG_PWD")
        Set fldWCF_Orig_FecPwd = AdoRes.Fields("WCF_ORIG_FECPWD")
        Set fldWCF_Dest_Url = AdoRes.Fields("WCF_DEST_URL")
        Set fldWCF_Dest_Usu = AdoRes.Fields("WCF_DEST_USU")
        Set fldWCF_Dest_Pwd = AdoRes.Fields("WCF_DEST_PWD")
        Set fldWCF_Dest_FecPwd = AdoRes.Fields("WCF_DEST_FECPWD")
       
        
        
        While Not AdoRes.eof
            
            If (Not basParametros.gParametrosGenerales.gbPedidosAprov And (fldTabla.Value = 11 Or fldTabla.Value = 12)) Or (Not basParametros.gParametrosGenerales.gbPedidosDirectos And (fldTabla.Value = 13 Or fldTabla.Value = 14)) _
            Or (gParametrosGenerales.gsAccesoFSQA = TipoAccesoFSQA.SinAcceso And (fldTabla.Value = 17 Or fldTabla.Value = EntidadIntegracion.PPM Or fldTabla.Value = EntidadIntegracion.CargosProveedor Or fldTabla.Value = EntidadIntegracion.TasasServicio)) _
            Or (gParametrosGenerales.gsAccesoFSIM = TipoAccesoFSIM.SinAcceso And (fldTabla.Value = EntidadIntegracion.Facturas Or fldTabla.Value = EntidadIntegracion.Pagos)) Then
            Else
                Me.Add fldEntidad.Value, fldActiva.Value, fldSentido.Value, fldDestinoTipo.Value, fldDestino.Value, fldTablaIntermedia.Value, fldOrigenTipo.Value, fldOrigenEntidad.Value, fldOrigenIntermedia.Value, fldTipoTraduccion.Value, fldOrigenRecibosERP.Value, fldDestinoRecibosFSGS.Value, fldCodERP.Value, fldFTP_IP.Value, fldFTP_Puerto.Value, fldFTP_Usu.Value, fldFTP_Pwd.Value, fldFTP_Origen.Value, fldFTP_Destino.Value, fldFTP_RecibosERP.Value, fldFTP_RecibosFSGS.Value, fldFTP_Intermedia.Value, fldTabla.Value, fldIdTablaExterna.Value, fldTablaExterna.Value, fldWEB_Orig_Url.Value, fldWEB_Orig_Usu.Value, fldWEB_Orig_Pwd.Value, fldWEB_Dest_Url.Value, fldWEB_Dest_Usu.Value, fldWEB_Dest_Pwd.Value, fldWCF_Orig_Url.Value, fldWCF_Orig_Usu.Value, fldWCF_Orig_Pwd, fldWCF_Orig_FecPwd.Value, fldWCF_Dest_Url.Value, fldWCF_Dest_Usu.Value, fldWCF_Dest_Pwd, fldWCF_Dest_FecPwd.Value
            End If
            AdoRes.MoveNext
        Wend
        
        AdoRes.Close
        Set AdoRes = Nothing
        Set fldEntidad = Nothing
        Set fldActiva = Nothing
        Set fldSentido = Nothing
        Set fldDestino = Nothing
        Set fldDestinoTipo = Nothing
        Set fldOrigenTipo = Nothing
        Set fldOrigenEntidad = Nothing
        Set fldOrigenIntermedia = Nothing
        Set fldTablaIntermedia = Nothing
        Set fldTipoTraduccion = Nothing
        Set fldOrigenRecibosERP = Nothing
        Set fldDestinoRecibosFSGS = Nothing
        Set fldCodERP = Nothing
        Set fldFTP_IP = Nothing
        Set fldFTP_Puerto = Nothing
        Set fldFTP_Usu = Nothing
        Set fldFTP_Pwd = Nothing
        Set fldFTP_Origen = Nothing
        Set fldFTP_Destino = Nothing
        Set fldFTP_RecibosERP = Nothing
        Set fldFTP_RecibosFSGS = Nothing
        Set fldFTP_Intermedia = Nothing
        Set fldTabla = Nothing
        Set fldIdTablaExterna = Nothing
        Set fldTablaExterna = Nothing
        Set fldWEB_Orig_Url = Nothing
        Set fldWEB_Orig_Usu = Nothing
        Set fldWEB_Orig_Pwd = Nothing
        Set fldWEB_Dest_Url = Nothing
        Set fldWEB_Dest_Usu = Nothing
        Set fldWEB_Dest_Pwd = Nothing
    End If

    Set adoParam = Nothing
    Set adoComm = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If Err.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CEntidadesInt", "CargarTodasLasEntidades", Err, Erl)
      Exit Sub
   End If
End Sub

''' <summary>
''' create a new object CEntidadInt
''' </summary>
''' <param name="Entidad">Entidad</param>
''' <param name="Activa">Activa la integraci�n</param>
''' <param name="sentido">sentido de la integraci�n</param>
''' <param name="destino_tipo">tipo de destino</param>
''' <param name="destino">Ruta destino local de entidad ntfs o http o msmq</param>
''' <param name="Intermedia">Ruta destino local tabla intermedia de entidad ntfs o http o msmq</param>
''' <param name="OrigenTipo">tipo de origen</param>
''' <param name="OrigenEntidad">Ruta origen local de entidad ntfs o http o msmq</param>
''' <param name="OrigenIntermedia">Ruta origen local tabla intermedia de entidad ntfs o http o msmq</param>
''' <param name="TipoTraduccion">Tipo Traduccion</param>
''' <param name="OrigenRecibosERP">Ruta local recibos Erp de entidad ntfs o http o msmq</param>
''' <param name="DestinoRecibosFSGS">Ruta local recibos FSGS de entidad ntfs o http o msmq</param>
''' <param name="CodERP">ERP</param>
''' <param name="FTP_Ip">Direcci�n Ip</param>
''' <param name="FTP_puerto">Puerto Ip</param>
''' <param name="FTP_usu">Usuario Ftp</param>
''' <param name="FTP_pwd">Password Ftp</param>
''' <param name="FTP_Origen">Ruta origen local de entidad ftp</param>
''' <param name="FTP_Destino">Ruta destino local de entidad ftp</param>
''' <param name="FTP_RecibosERP">Ruta local recibos Erp de entidad ftp</param>
''' <param name="FTP_RecibosFSGS">Ruta local recibos FSGS de entidad ftp</param>
''' <param name="FTP_Intermedia">Ruta local tabla intermedia de entidad ftp</param>
''' <param name="Tabla">Tabla</param>
''' <param name="IdTablaExterna">Id tabla externa</param>
''' <param name="Externa">Tabla externa</param>
''' <param name="WEB_Orig_Url">Direcci�n Url origen</param>
''' <param name="WEB_Orig_usu">Usuario webservice origen</param>
''' <param name="WEB_Orig_pwd">Password webservice origen</param>
''' <param name="WEB_Dest_Url">Direcci�n Url destino</param>
''' <param name="WEB_Dest_usu">Usuario webservice destino</param>
''' <param name="WEB_Dest_pwd">Password webservice destino</param>
''' <returns>objeto CEntidadInt</returns>
''' <remarks>Llamada desde: CargarTodasLasEntidades; Tiempo m�ximo: 0</remarks>
Public Function Add(ByVal Entidad As Integer, ByVal Activa As Boolean, ByVal sentido As Variant _
, ByVal destino_tipo As Variant, ByVal destino As Variant, ByVal Intermedia As Boolean, ByVal OrigenTipo As Variant _
, ByVal OrigenEntidad As Variant, ByVal OrigenIntermedia As Variant, ByVal TipoTraduccion As Variant _
, ByVal OrigenRecibosERP As Variant, ByVal DestinoRecibosFSGS As Variant, ByVal CodERP As Variant _
, ByVal FTP_Ip As Variant, ByVal FTP_puerto As Variant, ByVal FTP_usu As Variant, ByVal FTP_pwd As Variant _
, ByVal FTP_Origen As Variant, ByVal FTP_Destino As Variant, ByVal FTP_RecibosERP As Variant _
, ByVal FTP_RecibosFSGS As Variant, ByVal FTP_Intermedia As Variant, ByVal Tabla As Variant _
, ByVal IdTablaExterna As Variant, ByVal Externa As Variant _
, ByVal WEB_Orig_Url As Variant, ByVal WEB_Orig_usu As Variant, ByVal WEB_Orig_pwd As Variant _
, ByVal WEB_Dest_Url As Variant, ByVal WEB_Dest_usu As Variant, ByVal WEB_Dest_pwd As Variant _
, ByVal WCF_Orig_Url As Variant, ByVal WCF_Orig_usu As Variant, ByVal WCF_Orig_pwd As Variant, ByVal WCF_Orig_FecPwd As Variant _
, ByVal WCF_Dest_Url As Variant, ByVal WCF_Dest_usu As Variant, ByVal WCF_Dest_pwd As Variant, ByVal WCF_Dest_FecPwd As Variant) As CEntidadInt

    Dim objnewmember As CEntidadInt
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New CEntidadInt
   
    objnewmember.Entidad = Entidad
    objnewmember.Activa = Activa
    objnewmember.sentido = sentido
    objnewmember.DestinoTipo = destino_tipo
    objnewmember.destino = destino
    objnewmember.TablaIntermedia = Intermedia
    objnewmember.OrigenTipo = OrigenTipo
    objnewmember.OrigenEntidad = OrigenEntidad
    objnewmember.OrigenIntermedia = OrigenIntermedia
    objnewmember.TipoTraduccion = TipoTraduccion
    objnewmember.OrigenRecibosERP = OrigenRecibosERP
    objnewmember.DestinoRecibosFSGS = DestinoRecibosFSGS
    objnewmember.CodERP = CodERP
    objnewmember.FTP_Ip = FTP_Ip
    objnewmember.FTP_puerto = FTP_puerto
    objnewmember.FTP_usu = FTP_usu
    objnewmember.FTP_pwd = FTP_pwd
    objnewmember.FTP_Origen = FTP_Origen
    objnewmember.FTP_Destino = FTP_Destino
    objnewmember.FTP_RecibosERP = FTP_RecibosERP
    objnewmember.FTP_RecibosFSGS = FTP_RecibosFSGS
    objnewmember.FTP_Intermedia = FTP_Intermedia
    objnewmember.Tabla = Tabla
    objnewmember.IdTablaExterna = IdTablaExterna
    objnewmember.TablaExterna = Externa
    objnewmember.WEB_Orig_Url = WEB_Orig_Url
    objnewmember.WEB_Orig_usu = WEB_Orig_usu
    objnewmember.WEB_Orig_pwd = WEB_Orig_pwd
    objnewmember.WEB_Dest_Url = WEB_Dest_Url
    objnewmember.WEB_Dest_usu = WEB_Dest_usu
    objnewmember.WEB_Dest_pwd = WEB_Dest_pwd
    objnewmember.WCF_Orig_Url = WCF_Orig_Url
    objnewmember.WCF_Orig_usu = WCF_Orig_usu
    objnewmember.WCF_Orig_pwd = WCF_Orig_pwd
    objnewmember.WCF_Orig_FecPwd = WCF_Orig_FecPwd
    objnewmember.WCF_Dest_Url = WCF_Dest_Url
    objnewmember.WCF_Dest_usu = WCF_Dest_usu
    objnewmember.WCF_Dest_pwd = WCF_Dest_pwd
    objnewmember.WCF_Dest_FecPwd = WCF_Dest_FecPwd

    '''desencriptar la contrase�a
    ''desencriptar password
    If Not IsNull(WCF_Orig_pwd) Then
        Dim sPassword As String
        'sPassword = fldWCF_Orig_Pwd.Value
        sPassword = basUtilidades.EncriptarAES(WCF_Orig_usu, WCF_Orig_pwd, False, WCF_Orig_FecPwd, 1)
        objnewmember.WCF_Orig_pwd = sPassword
    End If
    If Not IsNull(WCF_Dest_pwd) Then
        Dim sPassword2 As String
        'sPassword2 = fldWCF_Dest_Pwd.Value
        sPassword2 = basUtilidades.EncriptarAES(WCF_Dest_usu, WCF_Dest_pwd, False, WCF_Dest_FecPwd, 1)
        objnewmember.WCF_Dest_pwd = sPassword2
    End If
    Set objnewmember.Conexion = m_oConexion
        
    'set the properties passed into the method
    mCol.Add objnewmember, CStr(Entidad) & "_" & CStr(Tabla) & "_" & CStr(IdTablaExterna)

    
    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If Err.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CEntidadesInt", "Add", Err, Erl)
      Exit Function
   End If

End Function

Public Property Get Item(vntIndexKey As Variant) As CEntidadInt
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Friend Property Set Conexion(ByVal con As CConexion)
Set m_oConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = m_oConexion
End Property


Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
     Count = mCol.Count
End If


End Property


Public Sub Remove(vntIndexKey As Variant)
   
    mCol.Remove vntIndexKey
End Sub


Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
     Set NewEnum = mCol.[_NewEnum]
End Property


Private Sub Class_Initialize()
    Set mCol = New Collection
End Sub


Private Sub Class_Terminate()
    'destroys collection when this class is terminated
    Set mCol = Nothing
    Set m_oConexion = Nothing
End Sub

Public Property Get eof() As Boolean
    eof = m_bEOF
End Property

Public Property Let eof(ByVal vNewValue As Boolean)
    m_bEOF = vNewValue
End Property
