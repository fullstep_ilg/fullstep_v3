VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CPrecondicion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum


Implements IBaseDatos

Private moConexion As CConexion

Private mlID As Long
Private miOrden As Integer
Private msCod As String
Private mlAccion As Long
Private moDenominaciones As CMultiidiomas
Private miTipo As TipoPrecondicion
Private msFormula As String
Private mdtFecAct As Variant

Private m_adores As adodb.Recordset
'Private moAcciones As CAcciones

Private mvarIndice As Variant


Friend Property Set Conexion(ByVal con As CConexion)
Set moConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = moConexion
End Property

Public Property Get Id() As Long
    Id = mlID
End Property
Public Property Let Id(ByVal Value As Long)
    mlID = Value
End Property

Public Property Get Orden() As Integer
    Orden = miOrden
End Property
Public Property Let Orden(ByVal Value As Integer)
    miOrden = Value
End Property

Public Property Get Codigo() As String
    Codigo = msCod
End Property
Public Property Let Codigo(ByVal Value As String)
    msCod = Value
End Property

Public Property Get Accion() As Long
    Accion = mlAccion
End Property
Public Property Let Accion(ByVal Value As Long)
    mlAccion = Value
End Property

Public Property Get Denominaciones() As CMultiidiomas
    Set Denominaciones = moDenominaciones
End Property
Public Property Set Denominaciones(ByVal dato As CMultiidiomas)
    Set moDenominaciones = dato
End Property

Public Property Get TipoPrecondicion() As TipoPrecondicion
    TipoPrecondicion = miTipo
End Property
Public Property Let TipoPrecondicion(ByVal Value As TipoPrecondicion)
    miTipo = Value
End Property

Public Property Get Formula() As String
    Formula = msFormula
End Property
Public Property Let Formula(ByVal Value As String)
    msFormula = Value
End Property

Public Property Let Indice(ByVal varIndice As Variant)
    mvarIndice = varIndice
End Property
Public Property Get Indice() As Variant
    Indice = mvarIndice
End Property

Public Property Get FecAct() As Variant
    FecAct = mdtFecAct
End Property

Public Property Let FecAct(ByVal Data As Variant)
    mdtFecAct = Data
End Property

Private Sub Class_Terminate()
    Set moDenominaciones = Nothing
    Set moConexion = Nothing
End Sub

''' <summary>Esta funci�n a�ade la precondici�n a la tabla </summary>
''' <returns>TipoErrorSummit con el error si lo hay</returns>
''' <remarks>Llamada desde: FSGClient
''' Tiempo m�ximo: 1 sec </remarks>
''' <revision>JVS 27/12/2011</revision>
Private Function IBaseDatos_AnyadirABaseDatos() As TipoErrorSummit
Dim sConsulta As String
Dim TESError As TipoErrorSummit
Dim AdoRes As adodb.Recordset
Dim bTransaccionEnCurso As Boolean
Dim oDen As CMultiidioma
Dim i As Integer


TESError.NumError = TESnoerror

If moConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CPrecondicion.AnyadirABAseDatos", "No se ha establecido la conexion"
End If
' *****************************************

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:

    moConexion.ADOCon.Execute "BEGIN DISTRIBUTED TRAN"
    bTransaccionEnCurso = True
    moConexion.ADOCon.Execute "SET XACT_ABORT OFF"

    'Insetar la Precondicion
    sConsulta = "INSERT INTO PM_ACCION_PRECOND " & _
        "(ORDEN, COD, ACCION, TIPO, FORMULA) " & _
        "VALUES " & _
        "(" & miOrden & ",'" & DblQuote(msCod) & "'," & mlAccion & "," & miTipo & ","
    If msFormula = "" Then
        sConsulta = sConsulta & "NULL)"
    Else
        sConsulta = sConsulta & "'" & DblQuote(msFormula) & "')"
    End If
    moConexion.ADOCon.Execute sConsulta
    If moConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    
    'CALIDAD Sin WITH(NOLOCK) en el FROM para asegurar la recuperaci�n del mayor valor de la tabla
    sConsulta = "SELECT MAX(ID) ID FROM PM_ACCION_PRECOND"
    Set AdoRes = New adodb.Recordset
    AdoRes.Open sConsulta, moConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    mlID = AdoRes("ID").Value
    AdoRes.Close
    Set AdoRes = Nothing
    
    'Insetar las Denominaciones:
    If Not moDenominaciones Is Nothing Then
        For Each oDen In moDenominaciones
            sConsulta = "INSERT INTO PM_ACCION_PRECOND_DEN (ACCION_PRECOND, IDI, DEN) VALUES (" & mlID & ",'" & DblQuote(oDen.Cod) & "','" & DblQuote(oDen.Den) & "')"
            moConexion.ADOCon.Execute sConsulta
            If moConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
        Next
    End If
    
    ''' Recuperar la fecha de insercion
    'CALIDAD Se obtiene la Fecha de Actualizaci�n con bloqueo para recuperar la verdadera �ltima fecha de actualizaci�n
    sConsulta = "SELECT FECACT FROM PM_ACCION_PRECOND WHERE ID =" & mlID
    Set AdoRes = New adodb.Recordset
    AdoRes.Open sConsulta, moConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    mdtFecAct = AdoRes("FECACT").Value
    AdoRes.Close
    Set AdoRes = Nothing

    moConexion.ADOCon.Execute "COMMIT TRAN"
    bTransaccionEnCurso = False

    IBaseDatos_AnyadirABaseDatos = TESError
    Exit Function
Error_Cls:

    If Not AdoRes Is Nothing Then
        AdoRes.Close
        Set AdoRes = Nothing
    End If

    IBaseDatos_AnyadirABaseDatos = basErrores.TratarError(moConexion.ADOCon.Errors)


    If bTransaccionEnCurso Then
        moConexion.ADOCon.Execute "ROLLBACK TRAN"
        bTransaccionEnCurso = False
    End If

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CPrecondicion", "IBaseDatos_AnyadirABaseDatos", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If

End Function

Private Function IBaseDatos_CambiarCodigo(CodigoNuevo As Variant) As TipoErrorSummit

End Function

Private Sub IBaseDatos_CancelarEdicion()

If Not m_adores Is Nothing Then
    m_adores.Close
    Set m_adores = Nothing
End If

End Sub

Private Function IBaseDatos_ComprobarExistenciaEnBaseDatos() As Boolean

End Function

''' <summary>Esta funci�n borra la precondici�n en la tabla </summary>
''' <returns>TipoErrorSummit con el error si lo hay</returns>
''' <remarks>Llamada desde: FSGClient
''' Tiempo m�ximo: 1 sec </remarks>
''' <revision>JVS 27/12/2011</revision>

Private Function IBaseDatos_EliminarDeBaseDatos() As TipoErrorSummit
Dim TESError As TipoErrorSummit
Dim btrans As Boolean
Dim sConsulta As String
Dim AdoRes As adodb.Recordset


TESError.NumError = TESnoerror

'******************* Precondicion *******************
If moConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CPrecondicion.EliminarDeBaseDatos", "No se ha establecido la conexion"
End If
'*****************************************************

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:

    moConexion.ADOCon.Execute "BEGIN TRAN"
    moConexion.ADOCon.Execute "SET XACT_ABORT OFF"
    btrans = True

    sConsulta = "DELETE FROM PM_ACCION_CONDICIONES WHERE ACCION_PRECOND = " & mlID & vbCrLf
    sConsulta = sConsulta & " DELETE FROM PM_ACCION_PRECOND_DEN WHERE ACCION_PRECOND = " & mlID & vbCrLf
    sConsulta = sConsulta & " DELETE FROM PM_ACCION_PRECOND WHERE ID = " & mlID & vbCrLf
    
    'Actualizar el orden del Resto de Precondiciones:
    sConsulta = sConsulta & " UPDATE PM_ACCION_PRECOND SET ORDEN = ORDEN - 1 WHERE ACCION = " & mlAccion & " AND ORDEN > " & miOrden

    moConexion.ADOCon.Execute sConsulta
    If moConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls

    moConexion.ADOCon.Execute "COMMIT TRAN"
    btrans = False
    Set m_adores = Nothing

    IBaseDatos_EliminarDeBaseDatos = TESError

    Exit Function

Error_Cls:

    If moConexion.ADOCon.Errors.Count > 0 Then
        IBaseDatos_EliminarDeBaseDatos = basErrores.TratarError(moConexion.ADOCon.Errors)
        If btrans Then
            moConexion.ADOCon.Execute "ROLLBACK TRAN"
            btrans = False
        End If
        Exit Function
    End If

    If m_adores.EditMode = 1 Then
        m_adores.CancelUpdate
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CPrecondicion", "IBaseDatos_EliminarDeBaseDatos", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If
End Function

Private Function IBaseDatos_FinalizarEdicionEliminando() As TipoErrorSummit

End Function

Private Function IBaseDatos_FinalizarEdicionModificando() As TipoErrorSummit
Dim TESError As TipoErrorSummit
Dim sConsulta As String
Dim bTransaccionEnCurso As Boolean
Dim AdoRes As adodb.Recordset
Dim oExtraPoint As CEnlaceExtraPoint
Dim oDen As CMultiidioma
Dim i As Integer


TESError.NumError = TESnoerror

'********* Precondicion *******************
If moConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CPrecondicion.FinalizarEdicionModificando", "No se ha establecido la conexion"
End If
'******************************************

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:

    moConexion.ADOCon.Execute "BEGIN TRAN"
    bTransaccionEnCurso = True
    moConexion.ADOCon.Execute "SET XACT_ABORT OFF"

    'Comprobamos si ha habido cambios en otra sesi�n
    Set m_adores = New adodb.Recordset
    
    'CALIDAD Se obtiene la Fecha de Actualizaci�n con bloqueo para recuperar la verdadera �ltima fecha de actualizaci�n
    sConsulta = "SELECT FECACT FROM PM_ACCION_PRECOND WHERE ID=" & mlID
    m_adores.Open sConsulta, moConexion.ADOCon, adOpenStatic, adLockReadOnly

    If m_adores.eof Then
        m_adores.Close
        Set m_adores = Nothing
        TESError.NumError = TESDatoEliminado
        TESError.Arg1 = 186
        IBaseDatos_FinalizarEdicionModificando = TESError
        moConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
        Exit Function
    End If

    If mdtFecAct <> m_adores("FECACT").Value Then
        m_adores.Close
        Set m_adores = Nothing
        TESError.NumError = TESInfActualModificada
        moConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
        IBaseDatos_FinalizarEdicionModificando = TESError
        Exit Function
    End If

    ''' Actualizar Precondicion
    sConsulta = "UPDATE PM_ACCION_PRECOND SET " & _
        "ORDEN = " & miOrden & ", " & _
        "COD = '" & DblQuote(msCod) & "', " & _
        "ACCION = " & mlAccion & ", " & _
        "TIPO = " & miTipo & ", " & _
        "FORMULA = "
    If msFormula = "" Then
        sConsulta = sConsulta & "NULL "
    Else
        sConsulta = sConsulta & "'" & DblQuote(msFormula) & "' "
    End If
    sConsulta = sConsulta & "WHERE ID = " & mlID
    moConexion.ADOCon.Execute sConsulta
    If moConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    
    '''Actualizar las Denominaciones
    If Not moDenominaciones Is Nothing Then
        For Each oDen In moDenominaciones
            sConsulta = "UPDATE PM_ACCION_PRECOND_DEN SET DEN = '" & DblQuote(oDen.Den) & "' WHERE ACCION_PRECOND = " & mlID & " AND IDI = '" & DblQuote(oDen.Cod) & "'"
            moConexion.ADOCon.Execute sConsulta
            If moConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
        Next
    End If

    'Recogemos el nuevo valor de FECACT
    m_adores.Requery
    mdtFecAct = m_adores("FECACT").Value
    m_adores.Close
    Set m_adores = Nothing

    moConexion.ADOCon.Execute "COMMIT TRAN"
    bTransaccionEnCurso = False

    IBaseDatos_FinalizarEdicionModificando = TESError

    Exit Function

Error_Cls:

    IBaseDatos_FinalizarEdicionModificando = basErrores.TratarError(moConexion.ADOCon.Errors)

    If Not m_adores Is Nothing Then
        m_adores.Close
        Set m_adores = Nothing
    End If

    If bTransaccionEnCurso Then
        moConexion.ADOCon.Execute "ROLLBACK"
        moConexion.ADOCon.Execute "SET XACT_ABORT OFF"
        bTransaccionEnCurso = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CPrecondicion", "IBaseDatos_FinalizarEdicionModificando", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If

End Function

Private Function IBaseDatos_IniciarEdicion(Optional ByVal Bloquear As Boolean, Optional ByVal UsuarioBloqueo As String) As TipoErrorSummit
Dim TESError As TipoErrorSummit
Dim oDenominaciones As CMultiidiomas
Dim i As Integer


TESError.NumError = TESnoerror

'******************* Precondicion *******************
If moConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CPrecondicion.IniciarEdicion", "No se ha establecido la conexion"
End If
'*****************************************************

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

'''Cargar Precondicion
Set m_adores = New adodb.Recordset
m_adores.Open "SELECT ID, ORDEN, COD, ACCION, TIPO, FORMULA, FECACT FROM PM_ACCION_PRECOND WITH (NOLOCK) WHERE ID =" & mlID, moConexion.ADOCon, adOpenKeyset, adLockReadOnly

If m_adores.eof Then
        m_adores.Close
        Set m_adores = Nothing
        TESError.NumError = TESDatoEliminado
        TESError.Arg1 = 186
        IBaseDatos_IniciarEdicion = TESError
        Exit Function
End If

miOrden = NullToDbl0(m_adores.Fields("ORDEN").Value)
msCod = NullToStr(m_adores.Fields("COD").Value)
mlAccion = NullToDbl0(m_adores.Fields("ACCION").Value)
miTipo = NullToDbl0(m_adores.Fields("TIPO").Value)
msFormula = NullToStr(m_adores.Fields("FORMULA").Value)
mdtFecAct = m_adores.Fields("FECACT").Value

m_adores.Close

'''Cargar Denominaciones
m_adores.Open "SELECT I.COD AS IDI, ISNULL(D.DEN,'') AS DEN FROM IDIOMAS I WITH (NOLOCK) LEFT JOIN PM_ACCION_PRECOND_DEN D WITH (NOLOCK) ON D.IDI = I.COD AND D.ACCION_PRECOND = " & mlID & " WHERE I.APLICACION = 1", moConexion.ADOCon, adOpenKeyset, adLockReadOnly

If m_adores.eof Then
        m_adores.Close
        Set m_adores = Nothing
        TESError.NumError = TESDatoEliminado
        TESError.Arg1 = 186
        IBaseDatos_IniciarEdicion = TESError
        Exit Function
End If
Set oDenominaciones = New CMultiidiomas
Set oDenominaciones.Conexion = moConexion

m_adores.Close
Set m_adores = Nothing

IBaseDatos_IniciarEdicion = TESError
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CPrecondicion", "IBaseDatos_IniciarEdicion", ERR, Erl)
      Exit Function
   End If

End Function


