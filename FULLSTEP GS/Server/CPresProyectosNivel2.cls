VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CPresProyectosNivel2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CPresProyectosNivel2 **********************************
'*             Autor : Javier Arana
'*             Creada : 10/2/99
'****************************************************************

Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum

Private mCol As Collection
Private mvarConexion As CConexion
Private mvarConexionServer As CConexionDeUseServer
Private mvarEOF As Boolean

''' <summary>
''' Carga de los datos de presupuestos
''' </summary>
''' <param optional name="iAnyo">A�o</param>
'''<param optional name="sPRES1">Presupuesto 1</param>
'''<param optional name="CaracteresInicialesCod">C�digo o primeros caracteres del c�digo</param>
'''<param optional name="CaracteresInicialesDen">Denominaci�n o primeros caracteres de la denominaci�n</param>
'''<param optional name="CoincidenciaTotal">Determina si usamos como criterio de selecci�n el c�digo y la denominaci�n o los primeros caracteres de ambos</param>
'''<param optional name="OrdenadosPorDen">Ordena por Denomninaci�n</param>
'''<param optional name="OrdenadosPorPres">Ordena por Presupuesto</param>
'''<param optional name="OrdenadosPorAnyo">Ordena por A�o</param>
'''<param optional name="OrdenadosPorObj">Ordena por Obj</param>
'''<param optional name="UsarIndice">Si se usa �ndice</param>
'''<param optional name="vUON1">Unidad organizativa 1</param>
'''<param optional name="vUON2">Unidad organizativa 2</param>
'''<param optional name="vUON3">Unidad organizativa 3</param>
'''<param optional name="bBajaLog">Para el campo BAJALOG</param>
''' <returns>Double</returns>
''' <remarks>Llamada desde: frmPresupuestos2
''' Tiempo m�ximo: 1 sec </remarks>
''' <revision>JVS 03/01/2012</revision>
Public Function CargarTodosLosPresupuestos(Optional ByVal iAnyo As Integer, Optional ByVal sPRES1 As String, Optional ByVal CaracteresInicialesCod As String, Optional ByVal CaracteresInicialesDen As String, Optional ByVal CoincidenciaTotal As Boolean, Optional ByVal OrdenadosPorDen As Boolean, Optional ByVal OrdenadosPorPres As Boolean, Optional ByVal OrdenadosPorAnyo As Boolean, Optional ByVal OrdenadosPorObj As Boolean, Optional ByVal UsarIndice As Boolean, Optional ByVal vUON1 As Variant, Optional ByVal vUON2 As Variant, Optional ByVal vUON3 As Variant, Optional ByVal bBajaLog As Variant) As Double
    Dim sConsulta As String
    Dim lIndice As Long
    Dim dSuma As Double
    Dim rs As New adodb.Recordset
    Dim fldAnyo As adodb.Field  'Anyo
    Dim fldCod As adodb.Field   'Cod
    Dim fldDen As adodb.Field   'Den
    Dim fldImp As adodb.Field   'Imp
    Dim fldObj As adodb.Field   'Obj
    Dim fldPres1 As adodb.Field   'Pres1
    Dim fldUON1 As adodb.Field
    Dim fldUON2 As adodb.Field
    Dim fldUON3 As adodb.Field
    Dim fldBajaLog As adodb.Field  'BAJALOG
    Dim fldFecIniVal As adodb.Field  'FEC_INI_VAL
    Dim fldFecFinVal As adodb.Field  'FEC_FIN_VAL
    
    
    
    '********* Precondicion **************************************
    If mvarConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CDestinos.CargarTodosLosDestinos", "No se ha establecido la conexion"
        Exit Function
    End If
    '*************************************************************
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    dSuma = 0
    
    sConsulta = "SELECT ID,ANYO,PRES1,COD,UON1,UON2,UON3,DEN,IMP,OBJ,FECACT,BAJALOG,FEC_INI_VAL,FEC_FIN_VAL FROM PRES1_NIV2 WITH(NOLOCK) WHERE 1=1 "
    
    If CaracteresInicialesCod = "" And CaracteresInicialesDen = "" Then
       
    Else
        If Not (CaracteresInicialesCod = "") And Not (CaracteresInicialesDen = "") Then
            
            If CoincidenciaTotal Then
                
                sConsulta = sConsulta & " AND COD ='" & DblQuote(CaracteresInicialesCod) & "'"
                sConsulta = sConsulta & " AND DEN='" & DblQuote(CaracteresInicialesDen) & "'"
                
            Else
                
                sConsulta = sConsulta & " AND COD LIKE '" & DblQuote(CaracteresInicialesCod) & "%'"
                sConsulta = sConsulta & " AND DEN LIKE '" & DblQuote(CaracteresInicialesDen) & "%'"
                           
            End If
                        
        Else
            
            If Not (CaracteresInicialesCod = "") Then
                
                If CoincidenciaTotal Then
                
                    sConsulta = sConsulta & " AND COD ='" & DblQuote(CaracteresInicialesCod) & "'"
                    
                Else
                        
                    sConsulta = sConsulta & " AND COD LIKE '" & DblQuote(CaracteresInicialesCod) & "%'"
                    
                End If
                
            Else
                
                If CoincidenciaTotal Then
                
                    sConsulta = sConsulta & " AND DEN='" & DblQuote(CaracteresInicialesDen) & "'"
                    
                Else
               
                    sConsulta = sConsulta & " AND DEN LIKE '" & DblQuote(CaracteresInicialesDen) & "%'"
                      
                End If
                
            End If
        
        End If
               
    End If
    
    If Trim(iAnyo) <> "" Then
        sConsulta = sConsulta & " AND ANYO =" & iAnyo
    End If
    
    If Not NoHayParametro(bBajaLog) Then
        sConsulta = sConsulta & " AND BAJALOG=" & BooleanToSQLBinary(bBajaLog)
    End If
    
    If Trim(sPRES1) <> "" Then
        sConsulta = sConsulta & " AND PRES1='" & DblQuote(sPRES1) & "'"
    End If
    
    If (NoHayParametro(vUON1) And NoHayParametro(vUON2) And NoHayParametro(vUON3)) Then
        sConsulta = sConsulta & " AND UON1 IS NULL AND UON2 IS NULL AND UON3 IS NULL"
    ElseIf (Not NoHayParametro(vUON1)) And NoHayParametro(vUON2) And NoHayParametro(vUON3) Then
        sConsulta = sConsulta & " AND UON1='" & DblQuote(vUON1) & "' AND UON2 IS NULL AND UON3 IS NULL"
    ElseIf (Not NoHayParametro(vUON1)) And (Not NoHayParametro(vUON2)) And NoHayParametro(vUON3) Then
        sConsulta = sConsulta & " AND UON1='" & DblQuote(vUON1) & "' AND UON2='" & DblQuote(vUON2) & "' AND UON3 IS NULL"
    ElseIf (Not NoHayParametro(vUON1)) And (Not NoHayParametro(vUON2)) And (Not NoHayParametro(vUON3)) Then
        sConsulta = sConsulta & " AND UON1='" & DblQuote(vUON1) & "' AND UON2='" & DblQuote(vUON2) & "' AND UON3='" & DblQuote(vUON3) & "'"
    End If
    
    If OrdenadosPorDen Then
        sConsulta = sConsulta & " ORDER BY DEN,COD,ANYO"
    Else
        If OrdenadosPorPres Then
            sConsulta = sConsulta & " ORDER BY IMP,COD,ANYO"
        Else
            If OrdenadosPorAnyo Then
                sConsulta = sConsulta & " ORDER BY ANYO,COD,DEN"
            Else
                If OrdenadosPorObj Then
                    sConsulta = sConsulta & " ORDER BY OBJ,COD,DEN"
                Else
                sConsulta = sConsulta & " ORDER BY COD,DEN"
                End If
            End If
        End If
    End If
          
    rs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
          
    If rs.eof Then
            
        rs.Close
        Set rs = Nothing
        Exit Function
          
    Else
        
        Set mCol = Nothing
        Set mCol = New Collection
            
        Set fldAnyo = rs.Fields("ANYO")
        Set fldCod = rs.Fields("COD")
        Set fldDen = rs.Fields("DEN")
        Set fldImp = rs.Fields("IMP")
        Set fldObj = rs.Fields("OBJ")
        Set fldPres1 = rs.Fields("PRES1")
        Set fldUON1 = rs.Fields.Item("UON1")
        Set fldUON2 = rs.Fields.Item("UON2")
        Set fldUON3 = rs.Fields.Item("UON3")
        Set fldBajaLog = rs.Fields.Item("BAJALOG")
        Set fldFecIniVal = rs.Fields.Item("FEC_INI_VAL")
        Set fldFecFinVal = rs.Fields.Item("FEC_FIN_VAL")
        
        
        If UsarIndice Then
            
            lIndice = 0
            
            While Not rs.eof
                ' Dejamos en manos del programador cuando asignar la conexion al obejto usuario
                Me.Add fldAnyo.Value, fldPres1.Value, fldCod.Value, fldDen.Value, fldImp.Value, fldObj.Value, lIndice, fldUON1.Value, fldUON2.Value, fldUON3.Value, , fldBajaLog.Value, fldFecIniVal.Value, fldFecFinVal.Value
                dSuma = dSuma + NullToDbl0(fldImp.Value)
                rs.MoveNext
                lIndice = lIndice + 1
            Wend
            
        Else
            
            While Not rs.eof
                ' Dejamos en manos del programador cuando asignar la conexion al obejto usuario
                Me.Add fldAnyo.Value, fldPres1.Value, fldCod.Value, fldDen.Value, fldImp.Value, fldObj.Value, , fldUON1.Value, fldUON2.Value, fldUON3.Value, , fldBajaLog.Value, fldFecIniVal.Value, fldFecFinVal.Value
                dSuma = dSuma + NullToDbl0(fldImp.Value)
                rs.MoveNext
            Wend
        End If
        
        rs.Close
        Set rs = Nothing
        Set fldAnyo = Nothing
        Set fldCod = Nothing
        Set fldDen = Nothing
        Set fldImp = Nothing
        Set fldObj = Nothing
        Set fldPres1 = Nothing
        Set fldUON1 = Nothing
        Set fldUON2 = Nothing
        Set fldUON3 = Nothing
        Set fldBajaLog = Nothing
        Set fldFecIniVal = Nothing
        Set fldFecFinVal = Nothing

        
        CargarTodosLosPresupuestos = dSuma
        Exit Function
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CPresProyectosNivel2", "CargarTodosLosPresupuestos", ERR, Erl)
        Exit Function
    End If
End Function

Public Function Add(ByVal Anyo As Integer, ByVal PRES1 As String, ByVal Cod As String, ByVal Den As String, Optional ByVal Importe As Variant, Optional ByVal Objetivo As Variant, Optional ByVal varIndice As Variant, Optional ByVal vUON1 As Variant, Optional ByVal vUON2 As Variant, Optional ByVal vUON3 As Variant, Optional ByVal Id As Long, Optional ByVal bBajaLog As Boolean, Optional ByVal vFecIniVal As Variant, Optional ByVal vFecFinVal As Variant) As CPresProyNivel2
    'create a new object
    
    Dim objnewmember As CPresProyNivel2
    Dim sCod As String
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New CPresProyNivel2
   
    objnewmember.Anyo = Anyo
    objnewmember.CodPRES1 = PRES1
    objnewmember.Importe = Importe
    objnewmember.Objetivo = Objetivo
    objnewmember.Cod = Cod
    objnewmember.Den = Den
    
    If NoHayParametro(vUON1) Then
        objnewmember.UON1 = Null
    Else
        objnewmember.UON1 = vUON1
    End If
    
    If NoHayParametro(vUON2) Then
        objnewmember.UON2 = Null
    Else
        objnewmember.UON2 = vUON2
    End If
    
    If NoHayParametro(vUON3) Then
        objnewmember.UON3 = Null
    Else
        objnewmember.UON3 = vUON3
    End If
    
    If NoHayParametro(Id) Then
        objnewmember.Id = 0
    Else
        objnewmember.Id = Id
    End If
    
    If NoHayParametro(bBajaLog) Then
        objnewmember.BajaLog = False
    Else
        objnewmember.BajaLog = bBajaLog
    End If
    
    If NoHayParametro(vFecIniVal) Then
        objnewmember.FecIniVal = Null
    Else
        objnewmember.FecIniVal = vFecIniVal
    End If
    
    If NoHayParametro(vFecFinVal) Then
        objnewmember.FecFinVal = Null
    Else
        objnewmember.FecFinVal = vFecFinVal
    End If
    
    Set objnewmember.Conexion = mvarConexion
    Set objnewmember.ConexionServer = mvarConexionServer
    
    If Not IsMissing(varIndice) And Not IsNull(varIndice) Then
        objnewmember.Indice = varIndice
        mCol.Add objnewmember, CStr(varIndice)
    Else
        sCod = PRES1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY1 - Len(PRES1))
        sCod = sCod & Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY2 - Len(Cod))
        mCol.Add objnewmember, sCod
    End If
        
    Set Add = objnewmember
    Set objnewmember = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CPresProyectosNivel2", "Add", ERR, Erl)
        Exit Function
    End If

End Function

Public Property Get Item(vntIndexKey As Variant) As CPresProyNivel2
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Friend Property Set Conexion(ByVal con As CConexion)
Set mvarConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = mvarConexion
End Property

Friend Property Get ConexionServer() As CConexionDeUseServer
Set ConexionServer = mvarConexionServer
End Property

Friend Property Set ConexionServer(ByVal con As CConexionDeUseServer)
Set mvarConexionServer = con
End Property

Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
     Count = mCol.Count
End If


End Property


Public Sub Remove(vntIndexKey As Variant)
    mCol.Remove vntIndexKey
End Sub


Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
     Set NewEnum = mCol.[_NewEnum]
End Property


Private Sub Class_Initialize()
   

    Set mCol = New Collection
End Sub


Private Sub Class_Terminate()
    'destroys collection when this class is terminated
    Set mCol = Nothing
    Set mvarConexion = Nothing
    Set mvarConexionServer = Nothing
End Sub

Public Function DevolverLosCodigos() As TipoDatosCombo
   
   Dim Codigos As TipoDatosCombo
   Dim iCont As Integer
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
   ReDim Codigos.Cod(mCol.Count)
   ReDim Codigos.Den(mCol.Count)
   
    For iCont = 0 To mCol.Count - 1
        
        Codigos.Cod(iCont) = mCol(iCont + 1).Cod
        Codigos.Den(iCont) = mCol(iCont + 1).Den
    
    Next

   DevolverLosCodigos = Codigos
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CPresProyectosNivel2", "DevolverLosCodigos", ERR, Erl)
        Exit Function
    End If
   
End Function
Public Property Get eof() As Boolean
    eof = mvarEOF
End Property
Friend Property Let eof(ByVal b As Boolean)
    mvarEOF = b
End Property


