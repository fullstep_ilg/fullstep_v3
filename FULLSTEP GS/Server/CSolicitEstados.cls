VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CSolicitEstados"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

''' *** Clase: CSolicitEstados
''' *** Creacion: 30/01/2006 (MMV)

''' Variables que almacenan las propiedades de los objetos
Private m_Col As Collection
Private m_bEOF As Boolean
Private m_oConexion As CConexion

''' Control de errores
Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum

''' <summary>
''' Crear la colecci�n de estados de una No Conformidad
''' </summary>
''' <param name="oSolicitud">Solictud de la q se estan cargando los estados</param>
''' <param name="COD">Codigo de estado</param>
''' <param name="oDen">Opcional. coleccion multidioma de denominaciones</param>
''' <param name="FecAct">Opcional. Fecha de ultima actualizacion</param>
''' <param name="varIndice">Opcional. �ndice del estado</param>
''' <returns>La funci�n devuelve el objeto CSolicitEstado creado, con los datos de los estados</returns>
''' <remarks>Llamada desde: CSolicitud/IBaseDatos_AnyadirABaseDatos     CSolicitud/CargarEstadosNoConformidad
'''         frmSOLConfigEstados/sdbgEstados_BeforeColUpdate         frmSOLConfigEstados/sdbgEstados_BeforeUpdate
'''; Tiempo m�ximo:0</remarks>
Public Function Add(ByVal oSolicitud As CSolicitud, ByVal Cod As String, Optional ByVal oDen As CMultiidiomas, _
                    Optional ByVal FecAct As Variant, Optional ByVal varIndice As Variant) As CSolicitEstado
                    
    Dim objnewmember As CSolicitEstado
    
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New CSolicitEstado
    
    Set objnewmember.Solicitud = oSolicitud
    objnewmember.Codigo = Cod
    If Not IsMissing(oDen) Then
        Set objnewmember.Denominaciones = oDen
    Else
        Set objnewmember.Denominaciones = Nothing
    End If
    
    If Not IsMissing(FecAct) Then
        objnewmember.FecAct = FecAct
    Else
        objnewmember.FecAct = Null
    End If
    
    Set objnewmember.Conexion = m_oConexion
    
    If Not IsMissing(varIndice) And Not IsNull(varIndice) Then
        objnewmember.Indice = varIndice
        m_Col.Add objnewmember, CStr(varIndice)
    Else
        m_Col.Add objnewmember, CStr(Cod)
    End If
            
    Set Add = objnewmember
    
    Set objnewmember = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CSolicitEstados", "Add", ERR, Erl)
      Exit Function
   End If
    
End Function
                    


Public Property Get Item(vntIndexKey As Variant) As CSolicitEstado

On Error GoTo NoSeEncuentra:

    ''' Devolvemos el item de la coleccion privada
    
    Set Item = m_Col(vntIndexKey)
     
    Exit Property

NoSeEncuentra:

    Set Item = Nothing
    
End Property
Public Property Get eof() As Boolean

    ''' * Objetivo: Devolver variable privada EOF
    ''' * Recibe: Nada
    ''' * Devuelve: Variable privada EOF
    
    eof = m_bEOF
    
End Property
Friend Property Set Conexion(ByVal con As CConexion)

    ''' * Objetivo: Dar valor a la variable privada Conexion
    ''' * Recibe: Conexion
    ''' * Devuelve: Nada
    
    Set m_oConexion = con
    
End Property
Friend Property Get Conexion() As CConexion

    ''' * Objetivo: Devolver variable privada Conexion
    ''' * Recibe: Nada
    ''' * Devuelve: Conexion
    
    Set Conexion = m_oConexion
    
End Property
Public Property Get Count() As Long

    ''' * Objetivo: Devolver variable privada Count
    ''' * Recibe: Nada
    ''' * Devuelve: Numero de elementos de la coleccion

    If m_Col Is Nothing Then
        Count = 0
    Else
        Count = m_Col.Count
    End If

End Property
Public Sub Remove(vntIndexKey As Variant)

    ''' * Objetivo: Eliminar una solicitud de la coleccion
    ''' * Recibe: Indice de la solicitud a eliminar
    ''' * Devuelve: Nada
   
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_Col.Remove vntIndexKey
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CSolicitEstados", "Remove", ERR, Erl)
      Exit Sub
   End If
    
End Sub
Private Sub Class_Initialize()

    ''' * Objetivo: Crear la coleccion

    Set m_Col = New Collection
    
End Sub
Private Sub Class_Terminate()

    ''' * Objetivo: Limpiar memoria
    Set m_Col = Nothing
    Set m_oConexion = Nothing
End Sub
Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4

    ''' ! Pendiente de revision, objetivo desconocido
    
    Set NewEnum = m_Col.[_NewEnum]

End Property
