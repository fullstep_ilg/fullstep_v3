VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CProveERPs"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum
Private mCol As Collection
Private moConexion As CConexion

Friend Property Set Conexion(ByVal vData As CConexion)
    Set moConexion = vData
End Property


Friend Property Get Conexion() As CConexion
    Set Conexion = moConexion
End Property


Public Function Add(ByVal sCod As String, ByVal sDen As String, Optional ByVal vNIF As Variant, Optional ByVal vCampo1 As Variant, Optional ByVal vCampo2 As Variant, Optional ByVal vCampo3 As Variant, Optional ByVal vCampo4 As Variant, Optional ByVal bBajaLog As Boolean, Optional ByVal vPag As Variant, Optional ByVal vViaPag As Variant, Optional ByVal vContacto As Variant) As CProveERP
    
    Dim objnewmember As CProveERP
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New CProveERP
    
    
    With objnewmember
        .Cod = sCod
        .Den = sDen
        .Nif = vNIF
        .Campo1 = vCampo1
        .Campo2 = vCampo2
        .Campo3 = vCampo3
        .Campo4 = vCampo4
        .BajaLogica = bBajaLog
        .Pag = vPag
        .ViaPag = vViaPag
        .Contacto = vContacto
    End With
    
    mCol.Add objnewmember, CStr(sCod)
    
    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CProveERPs", "Add", ERR, Erl)
      Exit Function
   End If

End Function

Public Property Get Item(vntIndexKey As Variant) As CProveERP

On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:

    Set Item = Nothing
    
End Property



Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
     Count = mCol.Count
End If


End Property


Public Sub Remove(vntIndexKey As Variant)
   
    mCol.Remove vntIndexKey
End Sub


Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
     Set NewEnum = mCol.[_NewEnum]
End Property


Private Sub Class_Initialize()
 
    Set mCol = New Collection
End Sub


Private Sub Class_Terminate()
    
    Set mCol = Nothing

End Sub

''' <summary>
''' Devuelve una colecci�n con los proveedores vinculados
''' </summary>
''' <param optional name="sNif">NIF</param>
''' <returns>CProveedores</returns>
''' <remarks>Llamada desde: frmProve.cmdAceptar_Click;
''' Tiempo m�ximo: < 1seg </remarks>
''' <revision>JVS 05/01/2012</revision>
Public Function ProveedoresVinculados(ByVal sNif As String) As CProveedores
Dim sConsulta As String
Dim adoRs As adodb.Recordset
Dim oProves As CProveedores

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
Set adoRs = New adodb.Recordset


sConsulta = "SELECT DISTINCT P.COD, P.DEN FROM PROVE P WITH(NOLOCK) INNER JOIN PROVE_EMPRESA_COD_ERP PECE WITH(NOLOCK) ON P.COD = PECE.PROVE INNER JOIN PROVE_ERP PE WITH(NOLOCK) ON PECE.COD_ERP = PE.COD_ERP AND PECE.EMPRESA = PE.EMPRESA WHERE PE.NIF = '" & DblQuote(sNif) & "'"

adoRs.Open sConsulta, moConexion.ADOCon, adOpenForwardOnly

Set oProves = New CProveedores

While Not adoRs.eof
    oProves.Add adoRs.Fields("COD").Value, adoRs.Fields("DEN").Value

    adoRs.MoveNext
Wend
adoRs.Close
Set adoRs = Nothing

Set ProveedoresVinculados = oProves
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CProveERPs", "ProveedoresVinculados", ERR, Erl)
      Exit Function
   End If

End Function

Public Sub CargarProveedoresERP(Optional ByVal lEmpresa As Long, Optional ByVal sProve As String, _
                                Optional ByVal sCodERP As String, Optional ByVal bUltimo As Boolean, Optional ByVal sOrgCompras As String, Optional ByVal CargaCampoGs As Boolean)
    Dim rs As New adodb.Recordset
    Dim fldCod As adodb.Field
    Dim fldDen As adodb.Field
    Dim fldEmpresa As adodb.Field
    Dim fldNIF As adodb.Field
    Dim fldCampo1 As adodb.Field
    Dim fldCampo2 As adodb.Field
    Dim fldCampo3 As adodb.Field
    Dim fldCampo4 As adodb.Field
    Dim fldContacto As adodb.Field
    Dim sConsulta As String
    Dim lIndice As Long
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If CargaCampoGs Then
        'Si CampoGs es true haremos la carga del campo GS Proveedor ERP(143)
        sConsulta = "SELECT DISTINCT PE.COD_ERP,PE.DEN_ERP,NULL AS EMPRESA,NULL AS NIF,NULL AS CAMPO1,NULL AS CAMPO2,NULL AS CAMPO3,NULL AS CAMPO4,NULL AS CONTACTO FROM PROVE_ERP PE WITH(NOLOCK)"
        sConsulta = sConsulta & " INNER JOIN PROVE WITH(NOLOCK) ON PE.NIF=PROVE.NIF"
        sConsulta = sConsulta & " INNER JOIN EMP WITH(NOLOCK) ON PE.EMPRESA=EMP.ID"
        sConsulta = sConsulta & " WHERE 1=1 AND BAJALOG=0 "
        If sProve <> "" Then
            sConsulta = sConsulta & " AND PROVE.COD='" & DblQuote(sProve) & "'"
        End If
        If sOrgCompras <> "" Then
            sConsulta = sConsulta & " AND EMP.ORG_COMPRAS='" & DblQuote(sOrgCompras) & "'"
        End If
        If sCodERP <> "" Then
            sConsulta = sConsulta & " AND PE.COD_ERP='" & DblQuote(sCodERP) & "'"
        End If
    Else
        sConsulta = "SELECT PROVE_ERP.COD_ERP,PROVE_ERP.EMPRESA,DEN_ERP,NIF,CAMPO1,CAMPO2,CAMPO3,CAMPO4,CONTACTO FROM PROVE_ERP WITH (NOLOCK)"
        sConsulta = sConsulta & " INNER JOIN PROVE_EMPRESA_COD_ERP WITH (NOLOCK) ON PROVE_ERP.EMPRESA=PROVE_EMPRESA_COD_ERP.EMPRESA AND PROVE_ERP.COD_ERP=PROVE_EMPRESA_COD_ERP.COD_ERP"
        sConsulta = sConsulta & " WHERE 1=1 AND BAJALOG=0"
        
        If lEmpresa <> 0 Then
            sConsulta = sConsulta & " AND PROVE_ERP.EMPRESA=" & lEmpresa
        End If
        
        If sProve <> "" Then
            sConsulta = sConsulta & " AND PROVE_EMPRESA_COD_ERP.PROVE='" & DblQuote(sProve) & "'"
        End If
        
        If sCodERP <> "" Then
            sConsulta = sConsulta & " AND PROVE_ERP.COD_ERP='" & DblQuote(sCodERP) & "'"
        End If
        
        If bUltimo = True Then
            sConsulta = sConsulta & " AND PROVE_EMPRESA_COD_ERP.ULTIMO=1"
        End If
    End If
            
    rs.Open sConsulta, moConexion.ADOCon, adOpenForwardOnly
    
    Set mCol = Nothing
    Set mCol = New Collection
    If Not rs.eof Then
        Set fldCod = rs.Fields("COD_ERP")
        Set fldDen = rs.Fields("DEN_ERP")
        Set fldEmpresa = rs.Fields("EMPRESA")
        Set fldNIF = rs.Fields("NIF")
        Set fldCampo1 = rs.Fields("CAMPO1")
        Set fldCampo2 = rs.Fields("CAMPO2")
        Set fldCampo3 = rs.Fields("CAMPO3")
        Set fldCampo4 = rs.Fields("CAMPO4")
        Set fldContacto = rs.Fields("CONTACTO")
        
        While Not rs.eof
            Me.Add fldCod.Value, fldDen.Value, fldNIF.Value, fldCampo1.Value, fldCampo2.Value, fldCampo3.Value, fldCampo4.Value, , , , fldContacto.Value
            rs.MoveNext
        Wend
        
        Set fldCod = Nothing
        Set fldDen = Nothing
        Set fldEmpresa = Nothing
        Set fldNIF = Nothing
        Set fldCampo1 = Nothing
        Set fldCampo2 = Nothing
        Set fldCampo3 = Nothing
        Set fldCampo4 = Nothing
        Set fldContacto = Nothing
    End If
    
    rs.Close
    Set rs = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CProveERPs", "CargarProveedoresERP", ERR, Erl)
      Exit Sub
   End If
End Sub
