VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CEtapasBloqueo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True


Private mvarConexion As CConexion
Private mvarEOF As Boolean
Private mvarConexionServer As CConexionDeUseServer


Friend Property Set Conexion(ByVal con As CConexion)
Set mvarConexion = con
End Property
Friend Property Set ConexionServer(ByVal con As CConexionDeUseServer)
Set mvarConexionServer = con
End Property
Friend Property Get ConexionServer() As CConexionDeUseServer
Set ConexionServer = mvarConexionServer
End Property

Private Sub Class_Initialize()
   

    'creates the collection when this class is created
    'Set mCol = New Collection
End Sub


Private Sub Class_Terminate()
    'destroys collection when this class is terminated
    'Set mCol = Nothing
    Set mvarConexion = Nothing
    Set mvarConexionServer = Nothing
End Sub


Public Function CargarEtapas(ByVal idWorkflow As Long, ByVal idIdioma As String) As adodb.Recordset

Dim rs As New adodb.Recordset
Dim sConsulta As String

sConsulta = "select * from PM_BLOQUE_DEN D WITH (NOLOCK) INNER JOIN PM_BLOQUE B WITH (NOLOCK) ON  D.BLOQUE = B.ID AND B.WORKFLOW=" & idWorkflow & " LEFT JOIN PM_BLOQUEO_ETAPA E WITH (NOLOCK) ON E.IDBLOQUE = B.ID where IDI='" & idIdioma & "'"
rs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
      
Set CargarEtapas = rs
    
Set rs = Nothing
      
End Function

Public Function GuardarEtapas(ByVal idbloque As Long, ByVal bloqueoapertura As Integer, ByVal bloqueoadjudicacion As Integer, ByVal bloqueopedidosdirectos As Integer, ByVal bloqueoep As Integer, ByVal Accion As Long) As TipoErrorSummit

Dim sConsultainsert As String
Dim sConsultaupdate As String
Dim TESError As TipoErrorSummit
Dim AdoRes As adodb.Recordset

TESError.NumError = TESnoerror

If mvarConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CWorkflow.AnyadirABAseDatos", "No se ha establecido la conexion"
End If


On Error GoTo Error:
    
    mvarConexion.ADOCon.Execute "BEGIN DISTRIBUTED TRAN"
    bTransaccionEnCurso = True
    mvarConexion.ADOCon.Execute "SET XACT_ABORT OFF"

If Accion = 1 Then
    sConsultainsert = "insert into PM_BLOQUEO_ETAPA (BLOQUEO_APERTURA,BLOQUEO_ADJUDICACION,BLOQUEO_PEDIDOSDIRECTOS,BLOQUEO_EP,IDBLOQUE) VALUES (" & bloqueoapertura & "," & bloqueoadjudicacion & "," & bloqueopedidosdirectos & "," & bloqueoep & "," & idbloque & ")"
    mvarConexion.ADOCon.Execute sConsultainsert
Else

    sConsultaupdate = "update PM_BLOQUEO_ETAPA SET BLOQUEO_APERTURA=" & bloqueoapertura & ",BLOQUEO_ADJUDICACION=" & bloqueoadjudicacion & ",BLOQUEO_PEDIDOSDIRECTOS=" & bloqueopedidosdirectos & ",BLOQUEO_EP=" & bloqueoep & " where IDBLOQUE=" & idbloque
    mvarConexion.ADOCon.Execute sConsultaupdate
End If
   
    If mvarConexion.ADOCon.Errors.Count > 0 Then
        GoTo Error
    End If

    mvarConexion.ADOCon.Execute "COMMIT TRAN"
    bTransaccionEnCurso = False
       
    GuardarEtapas = TESError
        
    Exit Function
    
Error:

    If Not AdoRes Is Nothing Then
        AdoRes.Close
        Set AdoRes = Nothing
    End If
                 
    GuardarEtapas = basErrores.TratarError(mvarConexion.ADOCon.Errors)
                 
                 
    If bTransaccionEnCurso Then
        mvarConexion.ADOCon.Execute "ROLLBACK TRAN"
        bTransaccionEnCurso = False
    End If
    
End Function

