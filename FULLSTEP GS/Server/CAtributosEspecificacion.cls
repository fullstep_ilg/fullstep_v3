VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CAtributosEspecificacion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CAtributosEspecificacion **********************************
'*             Autor : Eduardo Luis
'*             Creada : 19/dic/2007
'*             Tomada de CAtributosOfertados
'****************************************************************

Option Explicit

Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum

Private mCol As Collection
Private m_oConexion As CConexion
Private mvarEOF As Boolean

Private m_sError As Variant

Public Property Get sError() As Variant
    sError = m_sError
End Property

Friend Property Set Conexion(ByVal con As CConexion)
    Set m_oConexion = con
End Property

Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property
Public Property Get Item(vntIndexKey As Variant) As CAtributoEspecificacion
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
     Count = mCol.Count
End If

End Property
Public Property Get eof() As Boolean
    eof = mvarEOF
End Property

Friend Property Let eof(ByVal b As Boolean)
    mvarEOF = b
End Property
Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
    Set NewEnum = mCol.[_NewEnum]
End Property


Public Function AddAtributo(ByVal oAtributo As CAtributo, ByVal oObjeto As Object, Optional ByVal Orden As Variant, Optional ByVal Valor As Variant) As CAtributoEspecificacion
           
    Dim objnewmember As CAtributoEspecificacion
    Dim sCod As String
    Dim mivalor As Variant
    Dim oProceso As cProceso
    Dim Ambito As Variant
    Dim Grupo As Variant
    Dim Item As Variant
    
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New CAtributoEspecificacion
    Set objnewmember.Conexion = m_oConexion
    
    objnewmember.Atrib = oAtributo.Id
    objnewmember.Interno = oAtributo.Interno
    objnewmember.Pedido = False
    
    If IsNull(Orden) = False Then
        objnewmember.Orden = Orden
    Else
        objnewmember.Orden = oAtributo.Orden
    End If
    
    If TypeOf oObjeto Is CGrupo Then
        Set oProceso = oObjeto.Proceso
        Grupo = oObjeto.Codigo
        Ambito = TipoAmbitoProceso.AmbGrupo
    ElseIf TypeOf oObjeto Is cProceso Then
        Set oProceso = oObjeto
        Ambito = TipoAmbitoProceso.AmbProceso
    ElseIf TypeOf oObjeto Is CItem Then
        Set oProceso = oObjeto.Proceso
        'Item = oObjeto.Item
        Ambito = TipoAmbitoProceso.AmbItem
    End If
    
    If oProceso Is Nothing Then
        Exit Function
    End If
    
    objnewmember.Proceso = oProceso
    objnewmember.Anyo = oProceso.Anyo
    objnewmember.GMN1 = oProceso.GMN1Cod
    objnewmember.Proce = oProceso.Cod
    
    Set objnewmember.Objeto = oAtributo
    
    If IsNull(Valor) Then
        mivalor = oAtributo.Valor
    Else
        mivalor = Valor
    End If
    
    objnewmember.AsignarValor (mivalor)

    
    objnewmember.FecAct = oAtributo.FecAct
    objnewmember.Ambito = Ambito
    
    'Dependiendo de si es un atributo de proceso,grupo o item lo a�ade en la colecci�n.
    
    If Ambito = AmbGrupo Then
        objnewmember.Grupo = Grupo
        objnewmember.Item = IdentificadorDe(objnewmember, AmbGrupo)
        mCol.Add objnewmember, CStr(objnewmember.Item)
    ElseIf Ambito = AmbItem Then
        objnewmember.Grupo = oObjeto.GrupoCod
        objnewmember.Item = oObjeto.Id
        mCol.Add objnewmember, IdentificadorDe(oObjeto, AmbItem, objnewmember.Atrib)
    Else ' a nivel de proceso
        objnewmember.Item = IdentificadorDe(objnewmember, AmbProceso)
        mCol.Add objnewmember, CStr(objnewmember.Item)
    End If
    
    Set AddAtributo = objnewmember
    Set objnewmember = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CAtributosEspecificacion", "addAtributo", ERR, Erl)
      Exit Function
   End If

End Function

' construye identificador de atributo en funcion de su tipo para poder identificarlos en el dbgrid

Private Function IdentificadorDe(obj As Object, Optional Ambito As Variant, Optional Id As String = "") As String
    Dim sCod As String
    
    Dim oAtributoEspcificacion As New CAtributoEspecificacion
    
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    IdentificadorDe = oAtributoEspcificacion.IdentificadorDe(obj, Ambito, Id)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CAtributosEspecificacion", "IdentificadorDe", ERR, Erl)
      Exit Function
   End If
     
End Function


''' <summary> A�ade un elemento a la colecci�n con sus propiedades</summary>
''' <param name="oProceso">proceso</param>
''' <param name="Grupo">Grupo</param>
''' <param name="Item">item</param>
''' <param name="Atrib">Id Atrib</param>
''' <param name="Interno">Interno</param>
''' <param name="Pedido">Pedido</param>
''' <param name="Orden">Orden</param>
''' <param name="valor_num">Valor del atributo (en caso de ser num�rico)</param>
''' <param name="valor_text">Valor del atributo (en caso de ser de tipo texto)</param>
''' <param name="valor_fec">Valor del atributo (en caso de ser de tipo fecha)</param>
''' <param name="valor_bool">Valor del atributo (en caso de ser booleano)</param>
''' <param name="FecAct">fecha actualizacion</param>
''' <param name="oObjecto"></param>
''' <param name="Ambito">Ambito</param>
''' <param name="oObjeto"></param>
''' <returns>Atributo con las propiedades actualizadas</returns>
''' <remarks>Llamada desde: ; Tiempo m�ximo < 0,1</remarks>

Public Function Add( _
    ByVal oProceso As Object, _
    Optional ByVal Grupo As Variant, _
    Optional ByVal Item As Variant, _
    Optional ByVal Atrib As Variant, _
    Optional ByVal Interno As Variant, _
    Optional ByVal Pedido As Variant, _
    Optional ByVal Orden As Variant, _
    Optional ByVal ValorNum As Variant, _
    Optional ByVal ValorText As Variant, _
    Optional ByVal ValorFec As Variant, _
    Optional ByVal ValorBool As Variant, _
    Optional ByVal FecAct As Variant, _
    Optional ByVal oObjecto As Variant, _
    Optional ByVal Ambito As Variant, _
    Optional ByVal oObjeto As Object, _
    Optional ByVal Den As Variant _
    ) As CAtributoEspecificacion
           
    Dim objnewmember As CAtributoEspecificacion
    Dim sCod As String
    
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New CAtributoEspecificacion
    Set objnewmember.Conexion = m_oConexion
    
    Set objnewmember.Proceso = oProceso

    objnewmember.Anyo = oProceso.Anyo
    objnewmember.GMN1 = oProceso.GMN1Cod
    objnewmember.Proce = oProceso.Cod
    objnewmember.Atrib = Atrib
    objnewmember.Grupo = Grupo
    objnewmember.Item = Item
    objnewmember.Atrib = Atrib
    objnewmember.Interno = Interno
    objnewmember.Pedido = Pedido
    objnewmember.Orden = Orden
    objnewmember.ValorNum = ValorNum
    objnewmember.ValorText = ValorText
    objnewmember.ValorFec = ValorFec
    objnewmember.ValorBool = ValorBool
    objnewmember.FecAct = FecAct
    objnewmember.Ambito = Ambito
    
    If oObjeto Is Nothing Then
    
        Dim oAtrib As CAtributo
        Set oAtrib = New CAtributo
        Set oAtrib.Conexion = m_oConexion
        Set oObjeto = oAtrib.SeleccionarDefinicionDeAtributo(Atrib)
        Set oAtrib = Nothing
    End If
    
    Set objnewmember.Objeto = oObjeto ' atributo
    'Set objnewmember.UltimaEspecificacion = UltimaEspecificacion

    
    If Not IsMissing(Den) And Not IsNull(Den) Then
        objnewmember.Den = Den
    End If
    
    'Dependiendo de si es un atributo de proceso,grupo o item lo a�ade en la colecci�n.
       
    If Not IsNull(Item) Then  'atributos de nivel de item
        mCol.Add objnewmember, CStr(Item) & "$" & CStr(Atrib)
        
    ElseIf Not IsNull(Grupo) Then   'atributos de nivel de grupo
        sCod = CStr(Grupo) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE - Len(CStr(Grupo)))
        sCod = sCod & CStr(Atrib)
        mCol.Add objnewmember, sCod
    Else  'atributos de nivel de proceso
        mCol.Add objnewmember, CStr(Atrib)
    End If

    Set Add = objnewmember
    Set objnewmember = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CAtributosEspecificacion", "Add", ERR, Erl)
      Exit Function
   End If

End Function

Public Sub Remove(vntIndexKey As Variant)
   Dim i As Integer
   Dim oAtrib As CAtributoEspecificacion
   
   
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
   i = IndiceDe(CStr(vntIndexKey))
   If i > 0 Then
        Set oAtrib = mCol(i)
        oAtrib.Eliminar
   End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CAtributosEspecificacion", "Remove", ERR, Erl)
      Exit Sub
   End If
End Sub
Private Sub Class_Initialize()
   
    '*-*'

    Set mCol = New Collection
End Sub

Private Sub Class_Terminate()
    'destroys collection when this class is terminated
    Set mCol = Nothing
    Set m_oConexion = Nothing
    
End Sub


Function Actualizar(Optional ByVal Ambito As Variant, Optional ByVal Id As Variant, Optional ByVal Atributo As Variant) As TipoErrorSummit
    Dim TESError As TipoErrorSummit
    Dim oAtrib As CAtributoEspecificacion

    Dim escribir As Boolean
    
    '********* Precondicion *******************
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CAtributosEspecificacion.Modificar", "No se ha establecido la conexion"
    End If
    '******************************************
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
    
    TESError.NumError = TESnoerror

    If IsMissing(Ambito) Then
       Ambito = Null
    End If
    
    If IsMissing(Id) Then
        Id = Null
    End If
    If IsMissing(Atributo) Then
        Atributo = Null
    End If
    
    For Each oAtrib In mCol

        If Not IsNull(Ambito) Then
            If oAtrib.Ambito = Ambito Then
                escribir = True
            Else
                escribir = False
            End If
            
        Else
            escribir = True
        End If
        
        If escribir = True Then
            If Not IsNull(Id) Then ' se especifica un codigo de grupo, item o proceso
                If Not PropietarioDe(oAtrib) = Id Then
                    escribir = False
                End If
            End If
        End If
        
        If escribir = True Then
            If Not IsNull(Atributo) Then ' se especifica un codigo de grupo, item o proceso
                If Atributo <> oAtrib.Atrib Then
                    escribir = False
                End If
            End If
        End If
        
        If escribir = True Then
            If oAtrib.Existe() Then
                TESError = oAtrib.Modificar
            Else
                TESError = oAtrib.Anyadir
            End If
        End If
        
    Next
    
    Actualizar = TESError

    Exit Function


Error_Cls:

    Actualizar = basErrores.TratarError(m_oConexion.ADOCon.Errors)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      'Call g_oErrores.TratarError("M�dulo de clase", "CAtributosEspecificacion", "Actualizar", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If

End Function

Function Modificar() As TipoErrorSummit
    Dim TESError As TipoErrorSummit
    Dim oAtrib As CAtributoEspecificacion
    Dim mi_set As String
    
    '********* Precondicion *******************
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CAtributosEspecificacion.Modificar", "No se ha establecido la conexion"
    End If
    '******************************************
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
    TESError.NumError = TESnoerror
    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
    For Each oAtrib In mCol
        oAtrib.Modificar
    Next
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    Modificar = TESError
Exit Function

Error_Cls:
    Modificar = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    m_oConexion.RollbackTransaction
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      'Call g_oErrores.TratarError("M�dulo de clase", "CAtributosEspecificacion", "Modificar", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If

End Function

Public Function Anyadir() As TipoErrorSummit

    Dim TESError As TipoErrorSummit
    Dim oAtrib As CAtributoEspecificacion

    '********Precondici�n******************************************************************************************************************
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CPlantilla.AnyadirAtributo", "No se ha establecido la conexion"
    End If
    '**************************************************************************************************************************************
    
    If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
    TESError.NumError = TESnoerror

If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:

    TESError.NumError = TESnoerror


    For Each oAtrib In mCol
        oAtrib.Anyadir
    Next

    Anyadir = TESError

    Exit Function

Error_Cls:

    Anyadir = basErrores.TratarError(m_oConexion.ADOCon.Errors)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      'Call g_oErrores.TratarError("M�dulo de clase", "CAtributosEspecificacion", "Anyadir", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If

End Function

Public Function Eliminar() As TipoErrorSummit
    Dim TESError As TipoErrorSummit
    Dim oAtrib As CAtributoEspecificacion
    
    '********* Precondicion *******************
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CAtributosEspecificacion.Eliminar", "No se ha establecido la conexion"
    End If
    '******************************************
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
        
    TESError.NumError = TESnoerror


    For Each oAtrib In mCol
        oAtrib.Eliminar
    Next

    Eliminar = TESError

    Exit Function

Error_Cls:
    
    Eliminar = basErrores.TratarError(m_oConexion.ADOCon.Errors)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      'Call g_oErrores.TratarError("M�dulo de clase", "CAtributosEspecificacion", "Eliminar", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If

End Function

Public Function Seleccionar(Proceso As Object, Optional Ambito As Variant, Optional Id As Variant) As adodb.Recordset
    Dim rs As New adodb.Recordset
    Dim dato As Object
    Dim miCol As Collection
    Dim miObjeto As Object
    
    
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If mCol Is Nothing Then
        Set miCol = CargarAtributos(Proceso, Ambito, Id)
    Else
        Set miCol = mCol
    End If
    
    'declarar columnas
    
    With rs
        With .Fields
            .Append "CODIGO", adVariant
            .Append "VALOR", adVariant
            .Append "ORDEN", adVariant
        End With
    End With
    
    rs.Open
    
    For Each dato In mCol
        If Not (IsNull(dato.Valor) Or dato.Valor = "") Then

        If Not IsNull(Ambito) Then
            If Ambito = dato.Ambito Then
                If Not IsNull(Id) Then
                    If dato.Id = Id Then
                        With rs
                            .AddNew
                            !Codigo = dato.Descripcion
                            !Valor = dato.Valor
                            !Orden = dato.Orden
                            .Update
                        End With
                    End If
                Else
                    With rs
                        .AddNew
                        !Codigo = dato.Descripcion
                        !Valor = dato.Valor
                        !Orden = dato.Orden
                        .Update
                    End With
                End If
            End If
        Else
            With rs
                .AddNew
                !Codigo = dato.Descripcion
                !Valor = dato.Valor
                !Orden = dato.Orden
                .Update
            End With
        End If

        End If
    Next
    
    Set Seleccionar = rs
    Set miCol = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CAtributosEspecificacion", "Seleccionar", ERR, Erl)
      Exit Function
   End If
    
End Function



Public Sub Cargar(Proceso As Object, Optional Ambito As Variant, Optional Id As Variant)


If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set mCol = CargarAtributos(Proceso, Ambito, Id)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CAtributosEspecificacion", "Cargar", ERR, Erl)
      Exit Sub
   End If

End Sub

''' <summary>Carga los atributos</summary>
''' <param name="Proceso">Objeto proceso</param>
''' <param name="Ambito">Ambito del atributo</param>
''' <param name="ID">ID</param>
''' <returns>Colecci�n de atributos</returns>
''' <remarks>Llamada desde: GSClient</remarks>
''' <revision>LTG 02/01/2012</revision>

Private Function CargarAtributos(Proceso As Object, Optional Ambito As Variant, Optional Id As Variant) As Collection
    Dim miColeccion As Collection
    Dim miObjeto As Object
    Dim oAtributo As New CAtributoEspecificacion
    Dim Filtro As String
    Dim miCol As Collection
    
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oAtributo.Conexion = Conexion
    If IsMissing(Ambito) Then
         Ambito = Null
    End If
    If IsMissing(Id) Then
         Id = Null
    End If
    
    Set miCol = New Collection
    
    If IsNull(Ambito) Or Ambito = AmbProceso Then
        Set miColeccion = oAtributo.Seleccionar("SELECT ANYO,GMN1,PROCE,ATRIB,INTERNO,PEDIDO,ORDEN,VALOR_NUM,VALOR_TEXT,VALOR_FEC,VALOR_BOOL,FECACT," & _
            "OBLIG,VALIDACION FROM PROCE_ATRIBESP WITH (NOLOCK) WHERE ANYO = " & Proceso.Anyo & " AND GMN1 = '" & DblQuote(Proceso.GMN1Cod) & _
            "' AND PROCE = '" & DblQuote(Proceso.Cod) & "' ORDER BY ORDEN")
    
        If Not miColeccion Is Nothing Then
            For Each miObjeto In miColeccion
                miObjeto.Anyo = Proceso.Anyo
                miObjeto.GMN1 = Proceso.GMN1Cod
                miObjeto.Proce = Proceso.Cod
                miObjeto.Ambito = AmbProceso
                miObjeto.Item = IdentificadorDe(miObjeto, AmbProceso)
                miCol.Add miObjeto
            Next
        End If
    End If
    
    If IsNull(Ambito) Or Ambito = AmbItem Then
    
        If Not IsNull(Id) Then
            Filtro = " AND ITEM = '" & DblQuote(Id) & "'"
        Else
            Filtro = "" ' todos los atributos de item de ese proceso
        End If
        
        Set miColeccion = oAtributo.Seleccionar("SELECT ANYO,GMN1,PROCE,ITEM,ATRIB,VALOR_NUM,VALOR_TEXT,VALOR_FEC,VALOR_BOOL,FECACT,ART4_SINC " & _
            "FROM ITEM_ATRIBESP WITH (NOLOCK) WHERE ANYO = " & Proceso.Anyo & " AND GMN1 = '" & DblQuote(Proceso.GMN1Cod) & "' AND PROCE = '" & _
            DblQuote(Proceso.Cod) & "' " & Filtro & " ORDER BY ORDEN")
    
        If Not miColeccion Is Nothing Then
            For Each miObjeto In miColeccion
                miObjeto.Anyo = Proceso.Anyo
                miObjeto.GMN1 = Proceso.GMN1Cod
                miObjeto.Proce = Proceso.Cod
                miObjeto.Ambito = AmbItem
                'miObjeto.Item = IdentificadorDe(miObjeto, AmbItem) ' ID
                miObjeto.Item = miObjeto.Item
                miCol.Add miObjeto
            Next
        End If
    End If
    
    If IsNull(Ambito) Or Ambito = AmbGrupo Then
        If Not IsNull(Id) Then
            Filtro = " AND GRUPO = '" & DblQuote(Id) & "'"
        Else
            Filtro = "" ' todos los atributos de grupo de ese proceso
        End If
        
        Set miColeccion = oAtributo.Seleccionar("SELECT ANYO,GMN1,PROCE,GRUPO,ATRIB,INTERNO,PEDIDO,ORDEN,VALOR_NUM,VALOR_TEXT,VALOR_FEC," & _
            "VALOR_BOOL,FECACT,OBLIG,VALIDACION FROM PROCE_GRUPO_ATRIBESP WITH (NOLOCK) WHERE ANYO = " & Proceso.Anyo & " AND GMN1 = '" & _
            DblQuote(Proceso.GMN1Cod) & "' AND PROCE = '" & DblQuote(Proceso.Cod) & "' " & Filtro & " ORDER BY ORDEN")
    
        If Not miColeccion Is Nothing Then
            For Each miObjeto In miColeccion
                miObjeto.Anyo = Proceso.Anyo
                miObjeto.GMN1 = Proceso.GMN1Cod
                miObjeto.Proce = Proceso.Cod
                miObjeto.Grupo = Id
                miObjeto.Ambito = AmbGrupo
                miObjeto.Item = IdentificadorDe(miObjeto, AmbGrupo)
                miCol.Add miObjeto
            Next
        End If
    End If

    Set CargarAtributos = miCol
    Set miCol = Nothing
    Set miColeccion = Nothing
    Set miObjeto = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CAtributosEspecificacion", "CargarAtributos", ERR, Erl)
      Exit Function
   End If
    
End Function



Public Function AtributoDe(ByVal clave As String) As CAtributoEspecificacion
    Dim mc As Object
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    For Each mc In mCol
        If mc.Atrib = clave Then
            Set AtributoDe = mc
            Exit Function
        End If
    Next
    
    Set mc = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CAtributosEspecificacion", "AtributoDe", ERR, Erl)
      Exit Function
   End If
End Function

Public Function ItemDe(ByVal clave As String) As CAtributoEspecificacion
    Dim mc As Object
    Dim S As String
    
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    For Each mc In mCol
        Select Case mc.Ambito
        Case AmbItem
            's = mc.Item & "$" & mc.Grupo & "$" & mc.Atrib
            S = mc.Item & "$" & mc.Atrib
        Case AmbGrupo
            S = mc.Grupo & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE - Len(CStr(mc.Grupo))) & mc.Atrib
        Case Else
            S = mc.Atrib
        End Select
        
        If S = clave Then
            Set ItemDe = mc
            Exit For
        End If
    Next
    Set mc = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CAtributosEspecificacion", "ItemDe", ERR, Erl)
      Exit Function
   End If
End Function

Public Function IndiceDe(ByVal clave As String) As Integer

    Dim i As Integer
    Dim S As String
    
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    For i = 1 To mCol.Count
        Select Case mCol(i).Ambito
        Case AmbItem
            's = mCol(i).Item & "$" & mCol(i).Grupo & "$" & mCol(i).Atrib
            S = mCol(i).Item & "$" & mCol(i).Atrib
        Case AmbGrupo
            S = mCol(i).Grupo & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE - Len(CStr(mCol(i).Grupo))) & mCol(i).Atrib
        Case Else
            S = mCol(i).Atrib
        End Select
        If S = clave Then
            IndiceDe = i
            Exit For
        End If
    Next
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CAtributosEspecificacion", "IndiceDe", ERR, Erl)
      Exit Function
   End If

End Function

Public Function PropietarioDe(mc As Object) As String
    Dim S As String
    
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Select Case mc.Ambito
    Case AmbItem
        S = mc.Item
    Case AmbGrupo
        S = mc.Grupo
    Case Else
        S = mc.Proce
    End Select
    
    PropietarioDe = S
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CAtributosEspecificacion", "PropietarioDe", ERR, Erl)
      Exit Function
   End If

End Function

Public Function Existe(ByVal Id As Variant) As Boolean
    Dim o As CAtributoEspecificacion
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error GoTo ERR
    '
    Set o = mCol(CStr(Id))
    
    Existe = True
    Exit Function
ERR:
    Existe = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CAtributosEspecificacion", "Existe", ERR, Erl)
      Exit Function
   End If
End Function

Public Sub addAtributoEsp(oAtributo As CAtributoEspecificacion)

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    mCol.Add oAtributo, CStr(oAtributo.Atrib)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CAtributosEspecificacion", "addAtributoEsp", ERR, Erl)
      Exit Sub
   End If
End Sub
