VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CAprovisionadores"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private mCol As Collection
Private mvarConexion As CConexion

Friend Property Set Conexion(ByVal con As CConexion)
Set mvarConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = mvarConexion
End Property

Public Function Add(ByVal ID As Long, ByVal Segur As Long, ByVal Nivel As Integer, ByVal Per As CPersona, Optional ByVal Importe As Double, Optional ByVal Padre As Variant, Optional ByVal PadreID As Variant, Optional ByVal varIndice As Variant, Optional ByVal oDepartamento As CDepartamento, Optional ByVal UON1 As String, Optional ByVal UON2 As String, Optional ByVal UON3 As String, Optional ByVal DenUON1 As String, Optional ByVal DenUON2 As String, Optional ByVal DenUON3 As String) As CAprovisionador
   Dim objnewmember As CAprovisionador
   
    Set objnewmember = New CAprovisionador
    
    objnewmember.ID = ID
    Set objnewmember.Conexion = mvarConexion
    objnewmember.Seguridad = Segur
    objnewmember.Nivel = Nivel
    Set objnewmember.persona = Per
    
    objnewmember.Importe = Importe
   
    If Not IsMissing(oDepartamento) Then
        Set objnewmember.Departamento = oDepartamento
    Else
        objnewmember.Departamento = Nothing
    End If
    
    objnewmember.UON1 = UON1
    objnewmember.UON2 = UON2
    objnewmember.UON3 = UON3
    
    objnewmember.DenUON1 = DenUON1
    objnewmember.DenUON2 = DenUON2
    objnewmember.DenUON3 = DenUON3
    
    If Not IsMissing(Padre) Then
        objnewmember.Padre = Padre
    Else
        objnewmember.Padre = Null
    End If
    
    If Not IsMissing(PadreID) Then
        objnewmember.PadreID = PadreID
    Else
        objnewmember.PadreID = Null
    End If
    If Not IsMissing(varIndice) And Not IsNull(varIndice) Then
        objnewmember.Indice = varIndice
        mCol.Add objnewmember, CStr(varIndice)
    Else
        mCol.Add objnewmember, CStr(ID)
    End If

    Set Add = objnewmember
    Set objnewmember = Nothing
   
End Function
Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
     Count = mCol.Count
End If


End Property
Public Property Get Item(vntIndexKey As Variant) As CAprovisionador
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property
Public Sub Remove(vntIndexKey As Variant)
    
        mCol.Remove vntIndexKey
    
End Sub
Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
     Set NewEnum = mCol.[_NewEnum]
End Property

Private Sub Class_Initialize()
    
    Set mCol = New Collection

End Sub

Private Sub Class_Terminate()
    Set mCol = Nothing
    Set mvarConexion = Nothing
End Sub

