VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CVarPonderaciones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

Private mCol As Collection

Private m_oConexion As CConexion
Private m_bEOF As Boolean

Public Property Get eof() As Boolean
    eof = m_bEOF
End Property
Friend Property Let eof(ByVal b As Boolean)
    m_bEOF = b
End Property

Public Function Add(ByVal VarID As Long, VarNivel As Integer, ByVal oCampo As CFormItem, ByVal oGrupo As CFormGrupo, _
            Optional ByVal Tipo As TAtributoPonderacion, Optional ByVal Codigo As String, Optional ByVal Orden As Long, Optional ByVal Formula As Variant, Optional ByVal oLista As CValoresPond, _
            Optional ByVal vSi As Variant, Optional ByVal vNo As Variant, _
            Optional ByVal FechaActual As Variant, Optional ByVal vIndice As Variant, Optional ByVal lForm As Long) As CVarPonderacion
        
    Dim objnewmember As CVarPonderacion
    
    Set objnewmember = New CVarPonderacion
    Set objnewmember.Conexion = m_oConexion
    
    objnewmember.VarID = VarID
    objnewmember.VarNivel = VarNivel
    Set objnewmember.Campo = oCampo
    Set objnewmember.Grupo = oGrupo
    objnewmember.PondTipo = Tipo
    objnewmember.PondCod = Codigo
    objnewmember.Orden = Orden
    objnewmember.Formulario = lForm
    
    If Not IsMissing(Formula) Then
        objnewmember.PondFormula = Formula
    Else
        objnewmember.PondFormula = Null
    End If
    Set objnewmember.PondLista = oLista
    If Not IsMissing(vSi) Then
        objnewmember.PondSi = vSi
    Else
        objnewmember.PondSi = Null
    End If
    If Not IsMissing(vNo) Then
        objnewmember.PondNo = vNo
    Else
        objnewmember.PondNo = Null
    End If
    If IsMissing(FechaActual) Then
        objnewmember.FecAct = Null
    Else
        objnewmember.FecAct = FechaActual
    End If
    
    If Not IsMissing(vIndice) And Not IsNull(vIndice) Then
        objnewmember.Indice = vIndice
        mCol.Add objnewmember, CStr(vIndice)
    Else
        mCol.Add objnewmember, CStr(oCampo.ID)
    End If

    Set Add = objnewmember
    
    Set objnewmember = Nothing

End Function

Friend Property Set Conexion(ByVal vData As CConexion)
    Set m_oConexion = vData
End Property

Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property

Public Property Get Item(vntIndexKey As Variant) As CVarPonderacion

    ''' * Objetivo: Recuperar un campo de la coleccion
    ''' * Recibe: Indice del campo a recuperar
    ''' * Devuelve: campo correspondiente
        
On Error GoTo NoSeEncuentra:

    ''' Devolvemos el item de la coleccion privada
    
    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:

    Set Item = Nothing
    
End Property

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4

    ''' ! Pendiente de revision, objetivo desconocido
    
    Set NewEnum = mCol.[_NewEnum]

End Property
Public Sub Remove(vntIndexKey As Variant)

    ''' * Objetivo: Eliminar un campo de la coleccion
    ''' * Recibe: Indice del campo a eliminar
    ''' * Devuelve: Nada
   
    mCol.Remove vntIndexKey
    
End Sub
Private Sub Class_Initialize()

    ''' * Objetivo: Crear la coleccion
    Set mCol = New Collection
    
End Sub
Private Sub Class_Terminate()

    ''' * Objetivo: Limpiar memoria
    
    Set mCol = Nothing
    Set m_oConexion = Nothing
End Sub

Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
     Count = mCol.Count
End If

End Property

