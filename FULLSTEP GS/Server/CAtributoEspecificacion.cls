VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CAtributoEspecificacion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'**********************CAtributoEspecificacion*******************************
'*                 Autor: Eduardo Luis
'*                 Creada: 19/diciembre/2007
'*                 Tomada de CAtributoOfertado
'**********************************************************************

Option Explicit

Implements IBaseDatos

Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum

Private m_oConexion As CConexion

' propiedades de la clase

Private m_oProceso As cProceso
Private m_vProce As Variant
Private m_vAnyo As Variant
Private m_vGMN1 As Variant
Private m_vGrupo As Variant
Private m_vItem As Variant
Private m_vAtrib As Variant
Private m_vInterno As Variant
Private m_vPedido As Variant
Private m_vOrden As Variant
Private m_vValorNum As Variant
Private m_vValorText As Variant
Private m_vValorFec As Variant
Private m_vValorBool As Variant
Private m_vFecAct As Variant
Private m_sError As Variant
Private m_vAmbito As Variant
Private m_vDescripcion As Variant
Private m_vValor As Variant

Private m_oObjeto As Object
Private m_sDen As String

Public Property Get sError() As Variant
    sError = m_sError
End Property

Public Property Set Objeto(ByRef oObj As Object)
    Set m_oObjeto = oObj
End Property
Public Property Get Objeto() As Object
    Set Objeto = m_oObjeto
End Property

Public Property Set Proceso(ByVal oProce As cProceso)
    Set m_oProceso = oProce
End Property
Public Property Get Proceso() As cProceso
    Set Proceso = m_oProceso
End Property

Public Property Let Anyo(ByVal vAnyo As Variant)
    m_vAnyo = vAnyo
End Property
Public Property Get Anyo() As Variant
    Anyo = m_vAnyo
End Property


Public Property Let GMN1(ByVal vGMN1 As Variant)
    m_vGMN1 = vGMN1
End Property
Public Property Get GMN1() As Variant
    GMN1 = m_vGMN1
End Property

Public Property Let Proce(ByVal vProce As Variant)
   m_vProce = vProce
End Property
Public Property Get Proce() As Variant
    Proce = m_vProce
End Property


Public Property Let Grupo(ByVal vGrupo As Variant)
    m_vGrupo = vGrupo
End Property
Public Property Get Grupo() As Variant
    Grupo = m_vGrupo
End Property

Public Property Let Item(ByVal vItem As Variant)
    m_vItem = vItem
End Property
Public Property Get Item() As Variant
    Item = m_vItem
End Property

Public Property Let Atrib(ByVal vAtrib As Variant)
    m_vAtrib = vAtrib
End Property

Public Property Get Atrib() As Variant
    Atrib = m_vAtrib
End Property

Public Property Let Interno(ByVal vInterno As Variant)
    m_vInterno = vInterno
End Property
Public Property Get Interno() As Variant
    Interno = m_vInterno
End Property

Public Property Let Pedido(ByVal vPedido As Variant)
    m_vPedido = vPedido
End Property
Public Property Get Pedido() As Variant
    Pedido = m_vPedido
End Property

Public Property Let Orden(ByVal vOrden As Variant)
    m_vOrden = vOrden
End Property
Public Property Get Orden() As Variant
    Orden = m_vOrden
End Property

Public Property Let ValorNum(ByVal vData As Variant)
    m_vValorNum = vData
End Property
Public Property Get ValorNum() As Variant
    ValorNum = m_vValorNum
End Property

Public Property Let ValorText(ByVal vData As Variant)
    m_vValorText = vData
End Property
Public Property Get ValorText() As Variant
    ValorText = m_vValorText
End Property

Public Property Let ValorFec(ByVal vData As Variant)
    m_vValorFec = vData
End Property
Public Property Get ValorFec() As Variant
    ValorFec = m_vValorFec
End Property

Public Property Let ValorBool(ByVal vData As Variant)
    m_vValorBool = vData
End Property
Public Property Get ValorBool() As Variant
    ValorBool = m_vValorBool
End Property

Public Property Let FecAct(ByVal vData As Variant)
    m_vFecAct = vData
End Property
Public Property Get FecAct() As Variant
    FecAct = m_vFecAct
End Property
Public Property Let Ambito(ByVal vAmbito As Variant)
    m_vAmbito = vAmbito
End Property
Public Property Get Ambito() As Variant
    Ambito = m_vAmbito
End Property
Public Property Let Valor(ByVal vValor As Variant)
    m_vValor = vValor
End Property
Public Property Get Valor() As Variant
    Valor = m_vValor
End Property
Public Property Let Descripcion(ByVal vDescripcion As Variant)
    m_vDescripcion = vDescripcion
End Property
Public Property Get Descripcion() As Variant
    Descripcion = m_vDescripcion
End Property

Friend Property Set Conexion(ByVal vData As CConexion)
    Set m_oConexion = vData
End Property
Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property

Public Property Let Den(ByVal vData As String)
    m_sDen = vData
End Property
Public Property Get Den() As String
    Den = m_sDen
End Property

Private Sub Class_Terminate()
    
    Set m_oConexion = Nothing

End Sub

Public Function Anyadir() As TipoErrorSummit
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Anyadir = IBaseDatos_AnyadirABaseDatos()
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CAtributoEspecificacion", "Anyadir", ERR, Erl)
      Exit Function
   End If
End Function

Public Function Eliminar() As TipoErrorSummit
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Eliminar = IBaseDatos_EliminarDeBaseDatos
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CAtributoEspecificacion", "Eliminar", ERR, Erl)
      Exit Function
   End If
End Function

Function Existe() As Boolean
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Existe = IBaseDatos_ComprobarExistenciaEnBaseDatos
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CAtributoEspecificacion", "Existe", ERR, Erl)
      Exit Function
   End If
End Function


Function SeleccionarValor(ByVal sConsulta As String) As Variant

    Dim rs As adodb.Recordset
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls
    
    'TESError.NumError = TESnoerror
   
    Set rs = New adodb.Recordset

    rs.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
    If rs.RecordCount <= 0 Then
        ' no encontrado
        SeleccionarValor = Null
    Else
        SeleccionarValor = rs(0).Value
    End If

    rs.Close

    Set rs = Nothing
    Exit Function


Error_Cls:

    SeleccionarValor = basErrores.TratarError(m_oConexion.ADOCon.Errors)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CAtributoEspecificacion", "SeleccionarValor", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If
    
End Function

Function SeleccionarRegistro(ByVal sConsulta As String) As adodb.Recordset

    Dim rs As adodb.Recordset
    Dim miError As Variant


If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls

    
    'TESError.NumError = TESnoerror
   
    Set rs = New adodb.Recordset

    rs.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
    If rs.RecordCount <= 0 Then
        ' no encontrado
        Set SeleccionarRegistro = Nothing
    Else
        Set SeleccionarRegistro = rs
    End If

    'rs.Close

    'Set rs = Nothing
    Exit Function


Error_Cls:

    miError = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    Set SeleccionarRegistro = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CAtributoEspecificacion", "SeleccionarRegistro", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If
    
End Function
Public Function FormateoNumerico(ByVal dblNum As Double, Optional ByVal sFormato As String) As String
Dim dblRes As String
'Formato por defecto a 8 decimales
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls

      If sFormato = "" Then
        dblRes = Format(dblNum, "#,##0.########")
      Else
        dblRes = Format(dblNum, sFormato)
      End If
      'Se quita el simbolo decimal si no hay decimales
      If Not IsNumeric(Right(dblRes, 1)) Then
        dblRes = Left(dblRes, Len(dblRes) - 1)
      End If
      FormateoNumerico = dblRes
      Exit Function
Error_Cls:
    If ERR.Number = 13 Then
        dblRes = Format(dblNum, "#,##0.########")
        Resume Next
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CAtributoEspecificacion", "FormateoNumerico", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If
End Function

''' <summary>A�ade un nuevo registro a la BD</summary>
''' <returns>Variable tipo TipoErrorSummit con el error si se ha producido</returns>
''' <remarks>Llamada desde: GSClient</remarks>
''' <revision>LTG 02/01/2012</revision>

Private Function IBaseDatos_AnyadirABaseDatos() As TipoErrorSummit
    Dim sConsulta As String
    Dim TESError As TipoErrorSummit
    Dim rdo As Variant
    Dim Orden As Integer
    Dim AdoRes As adodb.Recordset
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    TESError.NumError = TESnoerror

    If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls

    Select Case Ambito
    
    Case AmbGrupo

        rdo = SeleccionarValor("select ISNULL(max(orden), 0) from PROCE_GRUPO_ATRIBESP WITH (NOLOCK) where ANYO=" & m_oProceso.Anyo & " AND GMN1='" & DblQuote(m_oProceso.GMN1Cod) & "' AND PROCE=" & m_oProceso.Cod & " AND GRUPO='" & DblQuote(Grupo) & "'")
            
    Case AmbItem

        rdo = SeleccionarValor("select ISNULL(max(orden), 0) from ITEM_ATRIBESP_DEF WITH (NOLOCK) where ANYO=" & m_oProceso.Anyo & " AND GMN1='" & DblQuote(m_oProceso.GMN1Cod) & "' AND PROCE=" & m_oProceso.Cod & " AND GRUPO= " & m_vGrupo & " AND ATRIB = " & Atrib)

    Case Else
        
        rdo = SeleccionarValor("select ISNULL(max(orden), 0) from PROCE_ATRIBESP WITH (NOLOCK) where ANYO=" & m_oProceso.Anyo & " AND GMN1='" & DblQuote(m_oProceso.GMN1Cod) & "' AND PROCE=" & m_oProceso.Cod)
    
    End Select

    If TypeOf rdo Is TipoErrorSummit Then
        IBaseDatos_AnyadirABaseDatos = rdo
        Exit Function
    End If
    
    
    If IsNumeric(rdo) Then
        Orden = CInt(rdo)
    Else
        Orden = 0
    End If
    
    Orden = Orden + 1
    
    If IsNull(ValorText) Then
        ValorText = ""
    End If
    
    Select Case Ambito
    
    Case AmbGrupo

        sConsulta = "INSERT INTO PROCE_GRUPO_ATRIBESP (ANYO, GMN1, PROCE, GRUPO, ATRIB, INTERNO, PEDIDO, ORDEN, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL, FECACT) VALUES ("
        sConsulta = sConsulta & m_oProceso.Anyo & ",'" & DblQuote(m_oProceso.GMN1Cod) & "'," & m_oProceso.Cod & ", '" & DblQuote(Grupo) & "', "
        sConsulta = sConsulta & "'" & DblQuote(Atrib) & "', " & BooleanToSQLBinary(Interno) & ", " & BooleanToSQLBinary(Pedido) & ", " & Orden & ", "
        sConsulta = sConsulta & DblToSQLFloat(ValorNum) & ", '" & DblQuote(ValorText) & "', " & DateToSQLDate(ValorFec) & ", " & BooleanToSQLBinary(ValorBool) & ", GETDATE())"
        m_oConexion.ADOCon.Execute sConsulta
            
    Case AmbItem

        'Se inserta en ITEM_ATRIBESP
        sConsulta = "INSERT INTO ITEM_ATRIBESP (ANYO, GMN1, PROCE, ITEM, ATRIB, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL, FECACT) VALUES ("
        sConsulta = sConsulta & m_oProceso.Anyo & ",'" & DblQuote(m_oProceso.GMN1Cod) & "'," & m_oProceso.Cod & ",'" & m_vItem & "', "
        sConsulta = sConsulta & "'" & DblQuote(Atrib) & "', "
        sConsulta = sConsulta & DblToSQLFloat(ValorNum) & ", '" & DblQuote(ValorText) & "', " & DateToSQLDate(ValorFec) & ", " & BooleanToSQLBinary(ValorBool) & ", GETDATE())"
        m_oConexion.ADOCon.Execute sConsulta
        'Se inserta en ITEM_ATRIBESP_DEF si no existe.
        sConsulta = "SELECT ATRIB FROM ITEM_ATRIBESP_DEF WITH(NOLOCK) WHERE ANYO=" & m_oProceso.Anyo & " AND GMN1='" & DblQuote(m_oProceso.GMN1Cod) & "' AND PROCE=" & m_oProceso.Cod & " AND GRUPO=" & m_vGrupo & " AND ATRIB=" & m_vAtrib
        Set AdoRes = New adodb.Recordset
        AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
        If AdoRes.eof Then
        Else
            'Como no existe se inserta.
            sConsulta = "INSERT INTO ITEM_ATRIBESP_DEF (ANYO, GMN1, PROCE, GRUPO, ATRIB, INTERNO, PEDIDO, ORDEN, FECACT) VALUES ("
            sConsulta = sConsulta & m_oProceso.Anyo & ",'" & DblQuote(m_oProceso.GMN1Cod) & "'," & m_oProceso.Cod & ",'" & m_vGrupo & "', "
            sConsulta = sConsulta & "'" & DblQuote(Atrib) & "', " & BooleanToSQLBinary(Interno) & ", " & BooleanToSQLBinary(Pedido) & ", " & Orden & ",  GETDATE())"
            m_oConexion.ADOCon.Execute sConsulta
        End If
        AdoRes.Close


    Case Else
        
        sConsulta = "INSERT INTO PROCE_ATRIBESP (ANYO, GMN1, PROCE, ATRIB, INTERNO, PEDIDO, ORDEN, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL, FECACT) VALUES ("
        sConsulta = sConsulta & m_oProceso.Anyo & ",'" & DblQuote(m_oProceso.GMN1Cod) & "'," & m_oProceso.Cod & ",'"
        sConsulta = sConsulta & DblQuote(Atrib) & "', " & BooleanToSQLBinary(Interno) & ", " & BooleanToSQLBinary(Pedido) & ", " & Orden & ", "
        sConsulta = sConsulta & DblToSQLFloat(ValorNum) & ", '" & DblQuote(ValorText) & "', " & DateToSQLDate(ValorFec) & ", " & BooleanToSQLBinary(ValorBool) & ", GETDATE())"
        m_oConexion.ADOCon.Execute sConsulta
    
    End Select
    
    


    IBaseDatos_AnyadirABaseDatos = TESError

    Exit Function

Error_Cls:

    IBaseDatos_AnyadirABaseDatos = basErrores.TratarError(m_oConexion.ADOCon.Errors)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CAtributoEspecificacion", "IBaseDatos_AnyadirABaseDatos", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If
    
End Function

Function Modificar() As TipoErrorSummit
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Modificar = IBaseDatos_FinalizarEdicionModificando
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CAtributoEspecificacion", "Modificar", ERR, Erl)
      Exit Function
   End If
    
End Function

Public Function Actualizar() As TipoErrorSummit
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Existe() Then
        Actualizar = Modificar()
    Else
        Actualizar = Anyadir()
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CAtributoEspecificacion", "Actualizar", ERR, Erl)
      Exit Function
   End If
End Function

    
Private Function IBaseDatos_CambiarCodigo(CodigoNuevo As Variant) As TipoErrorSummit

End Function

Private Sub IBaseDatos_CancelarEdicion()

End Sub

''' <summary>Comprueba la existencia de un registro en BD</summary>
''' <returns>True si existe</returns>
''' <remarks>Llamada desde: GSClient</remarks>
''' <revision>LTG 02/01/2012</revision>

Private Function IBaseDatos_ComprobarExistenciaEnBaseDatos() As Boolean
    Dim rdo As Variant
    Dim sConsulta As String
    

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Select Case Ambito
    
    Case AmbGrupo

        sConsulta = "SELECT COUNT(*) FROM PROCE_GRUPO_ATRIBESP WITH (NOLOCK) WHERE ANYO=" & m_oProceso.Anyo
        sConsulta = sConsulta & " AND GMN1='" & DblQuote(GMN1) & "' AND PROCE=" & m_oProceso.Cod
        sConsulta = sConsulta & " AND ATRIB='" & DblQuote(Atrib) & "' AND GRUPO='" & DblQuote(Grupo) & "'"

    Case AmbItem

        sConsulta = "SELECT COUNT(*) FROM ITEM_ATRIBESP WITH (NOLOCK) WHERE ANYO=" & m_oProceso.Anyo
        sConsulta = sConsulta & " AND GMN1='" & DblQuote(GMN1) & "' AND PROCE=" & m_oProceso.Cod
        sConsulta = sConsulta & " AND ITEM=" & m_vItem
        sConsulta = sConsulta & " AND ATRIB='" & DblQuote(Atrib) & "'"
    Case Else

        sConsulta = "SELECT COUNT(*) FROM PROCE_ATRIBESP WITH (NOLOCK) WHERE ANYO=" & m_oProceso.Anyo
        sConsulta = sConsulta & " AND GMN1='" & DblQuote(GMN1) & "' AND PROCE=" & m_oProceso.Cod
        sConsulta = sConsulta & " AND ATRIB='" & DblQuote(Atrib) & "'"

    End Select
    
    rdo = SeleccionarValor(sConsulta)
    
    If TypeOf rdo Is TipoErrorSummit Then
        ' error
    Else
        If IsNumeric(rdo) Then
            If rdo > 0 Then
                IBaseDatos_ComprobarExistenciaEnBaseDatos = True
            Else
                IBaseDatos_ComprobarExistenciaEnBaseDatos = False
            End If
        Else
            If IsNull(rdo) Then
                IBaseDatos_ComprobarExistenciaEnBaseDatos = False
            End If
            ' error
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CAtributoEspecificacion", "IBaseDatos_ComprobarExistenciaEnBaseDatos", ERR, Erl)
      Exit Function
   End If
End Function

Private Function IBaseDatos_EliminarDeBaseDatos() As TipoErrorSummit
    Dim TESError As TipoErrorSummit
    Dim sConsulta As String
    
    '********* Precondicion *******************
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CAtributoEspecificacion.Eliminar", "No se ha establecido la conexion"
    End If
    '******************************************
    
If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls
        
    TESError.NumError = TESnoerror
    

    Select Case Ambito
    
    Case AmbGrupo

        sConsulta = "DELETE FROM PROCE_GRUPO_ATRIBESP WHERE ANYO=" & m_oProceso.Anyo
        sConsulta = sConsulta & " AND GMN1='" & DblQuote(GMN1) & "' AND PROCE=" & m_oProceso.Cod
        sConsulta = sConsulta & " AND ATRIB='" & DblQuote(Atrib) & "' AND GRUPO='" & DblQuote(Grupo) & "'"

    Case AmbItem

        sConsulta = "DELETE FROM ITEM_ATRIBESP WHERE ANYO=" & m_oProceso.Anyo
        sConsulta = sConsulta & " AND GMN1='" & DblQuote(GMN1) & "' AND PROCE=" & m_oProceso.Cod
        sConsulta = sConsulta & " AND ITEM=" & m_vItem
        sConsulta = sConsulta & " AND ATRIB='" & DblQuote(Atrib) & "'"

    Case Else

        sConsulta = "DELETE FROM PROCE_ATRIBESP WHERE ANYO=" & m_oProceso.Anyo
        sConsulta = sConsulta & " AND GMN1='" & DblQuote(GMN1) & "' AND PROCE=" & m_oProceso.Cod
        sConsulta = sConsulta & " AND ATRIB='" & DblQuote(Atrib) & "'"

    End Select
    
    m_oConexion.ADOCon.Execute sConsulta
    
    
    IBaseDatos_EliminarDeBaseDatos = TESError

    Exit Function
    
    
Error_Cls:
    
    IBaseDatos_EliminarDeBaseDatos = basErrores.TratarError(m_oConexion.ADOCon.Errors)
End Function

Private Function IBaseDatos_FinalizarEdicionEliminando() As TipoErrorSummit

End Function

Private Function IBaseDatos_FinalizarEdicionModificando() As TipoErrorSummit
    Dim TESError As TipoErrorSummit
    Dim sConsulta As String
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls
    
    TESError.NumError = TESnoerror

    Select Case Ambito

    Case AmbGrupo

        sConsulta = "UPDATE PROCE_GRUPO_ATRIBESP SET INTERNO = " & BooleanToSQLBinary(Interno) & " , PEDIDO = " & BooleanToSQLBinary(Pedido) & ", VALOR_NUM=" & DblToSQLFloat(ValorNum) & ", VALOR_TEXT='" & DblQuote(ValorText) & "', VALOR_FEC=" & DateToSQLDate(ValorFec) & ", VALOR_BOOL=" & BooleanToSQLBinary(ValorBool)
        sConsulta = sConsulta & ", FECACT = GETDATE() WHERE ANYO=" & m_oProceso.Anyo & " AND GMN1='" & DblQuote(GMN1) & "' AND PROCE=" & m_oProceso.Cod
        sConsulta = sConsulta & " AND ATRIB='" & DblQuote(Atrib) & "' AND GRUPO='" & DblQuote(Grupo) & "'"
            
    Case AmbItem

        'Actualizamos la ITEM_ATRIBESP
        sConsulta = "UPDATE ITEM_ATRIBESP SET VALOR_NUM=" & DblToSQLFloat(ValorNum) & ", VALOR_TEXT='" & DblQuote(ValorText) & "', VALOR_FEC=" & DateToSQLDate(ValorFec) & ", VALOR_BOOL=" & BooleanToSQLBinary(ValorBool)
        sConsulta = sConsulta & ", FECACT = GETDATE() WHERE ANYO=" & m_oProceso.Anyo & " AND GMN1='" & DblQuote(GMN1) & "' AND PROCE=" & m_oProceso.Cod
        sConsulta = sConsulta & " AND ATRIB='" & DblQuote(Atrib) & "' AND ITEM='" & m_vItem & "'"
        m_oConexion.ADOCon.Execute sConsulta
        'Actualizamos la ITEM_ATRIBESP_DEF
        sConsulta = "UPDATE ITEM_ATRIBESP_DEF SET INTERNO = " & BooleanToSQLBinary(Interno) & " , PEDIDO = " & BooleanToSQLBinary(Pedido)
        sConsulta = sConsulta & ", FECACT = GETDATE() WHERE ANYO=" & m_oProceso.Anyo & " AND GMN1='" & DblQuote(GMN1) & "' AND PROCE=" & m_oProceso.Cod
        sConsulta = sConsulta & " AND ATRIB='" & DblQuote(Atrib) & "' AND GRUPO='" & DblQuote(Grupo) & "'"
        
        
    Case Else

        sConsulta = "UPDATE PROCE_ATRIBESP SET INTERNO = " & BooleanToSQLBinary(Interno) & " , PEDIDO = " & BooleanToSQLBinary(Pedido) & ", VALOR_NUM=" & DblToSQLFloat(ValorNum) & ", VALOR_TEXT='" & DblQuote(ValorText) & "', VALOR_FEC=" & DateToSQLDate(ValorFec) & ", VALOR_BOOL=" & BooleanToSQLBinary(ValorBool)
        sConsulta = sConsulta & ", FECACT = GETDATE() WHERE ANYO=" & m_oProceso.Anyo & " AND GMN1='" & DblQuote(GMN1) & "' AND PROCE=" & m_oProceso.Cod
        sConsulta = sConsulta & " AND ATRIB='" & DblQuote(Atrib) & "'"
    
    End Select

    m_oConexion.ADOCon.Execute sConsulta

    IBaseDatos_FinalizarEdicionModificando = TESError

    Exit Function


Error_Cls:

    IBaseDatos_FinalizarEdicionModificando = basErrores.TratarError(m_oConexion.ADOCon.Errors)

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CAtributoEspecificacion", "IBaseDatos_FinalizarEdicionModificando", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If

End Function

Public Function Seleccionar(ByVal sConsulta As String) As Collection
    Dim objnewmember As CAtributoEspecificacion
    Dim rs As Recordset
    Dim oAtrib As New CAtributo
    Dim Ambito As Integer
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set rs = SeleccionarRegistro(sConsulta)
    If rs Is Nothing Then
        Set Seleccionar = Nothing
        Exit Function
    End If
    If rs.RecordCount <= 0 Then
        Set Seleccionar = Nothing
        Exit Function
    End If
    'averiguar ambito
    If InStr(sConsulta, "ITEM_ATRIBESP") > 0 Then
        Ambito = AmbItem
    Else
        If InStr(sConsulta, "GRUPO_ATRIBESP") > 0 Then
            Ambito = AmbGrupo
        Else
            Ambito = AmbProceso
        End If
    End If
    
    Set Seleccionar = New Collection
    While Not rs.eof
        Set objnewmember = New CAtributoEspecificacion
        Set objnewmember.Conexion = m_oConexion
    
        objnewmember.Anyo = rs("ANYO").Value
        objnewmember.GMN1 = rs("GMN1").Value
        objnewmember.Proce = rs("PROCE").Value
        objnewmember.Grupo = m_vGrupo
        
        If Ambito = AmbItem Then
            objnewmember.Item = rs("ITEM").Value
        End If
        
        objnewmember.Atrib = rs("ATRIB").Value
        objnewmember.Interno = SQLBinaryToBoolean(rs("Interno").Value)
        objnewmember.Pedido = SQLBinaryToBoolean(rs("Pedido").Value)
        objnewmember.Orden = rs("orden").Value
        objnewmember.ValorNum = rs("VALOR_NUM").Value
        objnewmember.ValorText = rs("VALOR_TEXT").Value
        objnewmember.ValorFec = rs("VALOR_FEC").Value
        objnewmember.ValorBool = rs("VALOR_BOOL").Value
        objnewmember.FecAct = rs("FECACT").Value

        
        Set oAtrib = New CAtributo
        Set oAtrib.Conexion = Conexion
        Set objnewmember.Objeto = oAtrib.SeleccionarDefinicionDeAtributo(objnewmember.Atrib)

        Select Case oAtrib.Tipo
        Case TipoNumerico
            objnewmember.Valor = CStr(objnewmember.ValorNum)
        Case TipoFecha
            objnewmember.Valor = CStr(objnewmember.ValorFec)
        Case TipoBoolean
            objnewmember.Valor = CStr(objnewmember.ValorBool)
        Case Else
            objnewmember.Valor = CStr(objnewmember.ValorText)
        End Select

        objnewmember.Descripcion = objnewmember.Objeto.Den
        
        Seleccionar.Add objnewmember
        
        Set oAtrib = Nothing
        Set objnewmember = Nothing
        rs.MoveNext
    Wend
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CAtributoEspecificacion", "Seleccionar", ERR, Erl)
      Exit Function
   End If
    
End Function



Private Function IBaseDatos_IniciarEdicion(Optional ByVal Bloquear As Boolean, Optional ByVal UsuarioBloqueo As String) As TipoErrorSummit

End Function


Public Function MostrarAtributo(ByVal oValorAtrib As Object, ByVal udtTipo As TiposDeAtributos, ByVal m_sIdiFalse As String, ByVal m_sIdiTrue As String) As Variant

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Select Case udtTipo
        Case TiposDeAtributos.TipoString
            MostrarAtributo = NullToStr(oValorAtrib.ValorText)
        Case TiposDeAtributos.TipoNumerico
            MostrarAtributo = oValorAtrib.ValorNum
        Case TiposDeAtributos.TipoFecha
            MostrarAtributo = oValorAtrib.ValorFec
        Case TiposDeAtributos.TipoBoolean
            Select Case oValorAtrib.ValorBool
            Case 0, False
                MostrarAtributo = m_sIdiFalse
            Case 1, True
                MostrarAtributo = m_sIdiTrue
            Case Else
                MostrarAtributo = ""
            End Select
    End Select
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CAtributoEspecificacion", "MostrarAtributo", ERR, Erl)
      Exit Function
   End If
        
End Function

' devuelve falso si el valor no ha cambiado

Public Function AsignarValor(mivalor As Variant) As Boolean

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    AsignarValor = False
    
    ValorText = ""
            
    If Me.Objeto Is Nothing Then
        Dim oAtrib As Object
        Set oAtrib = New CAtributo
        Set Me.Objeto = oAtrib.SeleccionarDefinicionDeAtributo(Me.Atrib)
    End If
    
    Select Case Me.Objeto.Tipo
    Case TipoNumerico
        If (ValorNum <> mivalor) Then
            ValorNum = mivalor
            AsignarValor = True
        End If
    Case TipoBoolean
        If (ValorBool <> mivalor) Then
            ValorBool = mivalor
            AsignarValor = True
        End If
    Case TipoFecha
        If (ValorFec <> mivalor) Then
            ValorFec = mivalor
            AsignarValor = True
        End If
    Case Else
        If (ValorText <> mivalor) Then
            ValorText = mivalor
            AsignarValor = True
        End If
    End Select
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CAtributoEspecificacion", "AsignarValor", ERR, Erl)
      Exit Function
   End If
    
End Function

' devuelve falso si el valor no ha cambiado

Public Function AsignarMarcas(Optional bInterno As Boolean, Optional bPedido As Boolean) As Boolean

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not IsMissing(bInterno) Then
        Me.Interno = bInterno
    End If
    
    If Not IsMissing(bPedido) Then
        Me.Pedido = bPedido
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CAtributoEspecificacion", "AsignarMarcas", ERR, Erl)
      Exit Function
   End If

End Function

' construye identificador de atributo en funcion de su tipo para poder identificarlos en el dbgrid

Public Function IdentificadorDe(obj As Object, Optional Ambito As Variant, Optional ID As String = "") As String
    Dim sCod As String
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If IsNull(Ambito) Then
        Ambito = obj.Ambito
    End If
    
    If Ambito = AmbGrupo Then
        sCod = CStr(obj.Grupo) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE - Len(CStr(obj.Grupo)))
        sCod = sCod & CStr(obj.Atrib)
        IdentificadorDe = sCod
    ElseIf Ambito = AmbItem Then
        IdentificadorDe = obj.ID & "$" & ID
        'clave compuesta de : item $ grupo  $ atributo
        'IdentificadorDe = obj.id & "$" & obj.GrupoCod & "$" & id
    Else ' a nivel de proceso
        IdentificadorDe = obj.Atrib
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CAtributoEspecificacion", "IdentificadorDe", ERR, Erl)
      Exit Function
   End If
    
End Function
