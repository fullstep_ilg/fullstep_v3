VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CUnidadesOrgNivel4"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum

'local variable to hold collection
Private mCol As Collection
Private m_oConexion As CConexion
Private m_oConexionServer As CConexionDeUseServer

Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property
Friend Property Set Conexion(ByVal con As CConexion)
    Set m_oConexion = con
End Property
Friend Property Set ConexionServer(ByVal con As CConexionDeUseServer)
    Set m_oConexionServer = con
End Property

Friend Property Get ConexionServer() As CConexionDeUseServer
    Set ConexionServer = m_oConexionServer
End Property

Public Property Get Item(vntIndexKey As Variant) As CUnidadOrgNivel4
Attribute Item.VB_UserMemId = 0
  On Error GoTo vacio
  
    Set Item = mCol(vntIndexKey)
    Exit Property
vacio:
    Set Item = Nothing
    
End Property

Public Property Get Count() As Long
    If mCol Is Nothing Then
        Count = 0
    Else
        Count = mCol.Count
    End If
End Property
Public Sub Remove(vntIndexKey As Variant)
    mCol.Remove vntIndexKey
End Sub
Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
    Set NewEnum = mCol.[_NewEnum]
End Property


Private Sub Class_Initialize()
    Set mCol = New Collection
End Sub

Private Sub Class_Terminate()
    Set mCol = Nothing
    Set m_oConexion = Nothing
    Set m_oConexionServer = Nothing
End Sub

'ATENCION he movido ByVal sCod As String una a la dcha!!!!
Public Function DevolverDenominacion(ByVal sCodUON1 As String, sCodUON2 As String, sCodUON3 As String, ByVal sCod As String) As String
'Devuelve la denominaci�n (campo DEN) de la UO de nivel 4 (tabla UON4)
'cuyo c�digo corresponda con el especificado por los argumentos sCod (nivel 4) y sCodUON3 (nivel 3) y sCodUON2 (nivel 2) y sCodUON1 (nivel 1)
Dim sConsulta As String
Dim adoresAdo As adodb.Recordset
Dim sDenominacion As String
    
    
    sDenominacion = ""
    Set adoresAdo = New adodb.Recordset
    sConsulta = "SELECT DEN FROM UON4 WITH (NOLOCK) WHERE UON1='" & DblQuote(sCodUON1) & "' AND "
    sConsulta = sConsulta & " UON2='" & DblQuote(sCodUON2) & "' AND "
    sConsulta = sConsulta & " UON3='" & DblQuote(sCodUON3) & "' AND "
    sConsulta = sConsulta & " COD='" & DblQuote(sCod) & "'"
    adoresAdo.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    If Not adoresAdo.eof Then sDenominacion = adoresAdo.Fields.Item(0).Value
    adoresAdo.Close
    Set adoresAdo = Nothing
    DevolverDenominacion = sDenominacion
End Function
'He a�adido Optional ByVal UON4 As Variant como ultimo parametro
Public Sub CargarTodasLasUnidadesOrgNivel4(Optional ByVal UON1 As Variant, Optional ByVal UON2 As Variant, Optional ByVal UON3 As Variant, Optional ByVal CodDep As Variant, Optional ByVal RUO As Boolean = False, Optional ByVal RDep As Boolean, Optional ByVal CarIniCod As String, Optional ByVal CarIniDen As String, Optional ByVal CoincidenciaTotal As Boolean, Optional ByVal OrdenadasPorDen As Boolean = False, Optional ByVal UsarIndice As Boolean = False, Optional ByVal bCargarEmpresas As Boolean = False, Optional ByVal sCarIniNif As String, Optional ByVal sCarIniRazon As String, Optional ByVal BajaLog As Boolean, Optional ByVal iAnyo As Integer, Optional ByVal sGMN1 As String, Optional ByVal iProceso As Long, Optional ByVal sGrupo As String, Optional ByVal udtTipoDist As TipoDefinicionDatoProceso, Optional ByVal lItem As Long, Optional ByVal UON4 As Variant)

Dim rs As New adodb.Recordset
Dim sConsulta1 As String    'Esta es para
Dim sConsulta2 As String    'Esta es para
Dim sConsulta3 As String    'Esta es para

Dim sCad As String
Dim lIndice As Long
Dim oUON4 As CUnidadOrgNivel4

Dim fldUon1Cod As adodb.Field
Dim fldUon2Cod As adodb.Field
Dim fldUon3Cod As adodb.Field
Dim fldUon4Cod As adodb.Field
Dim fldUON4Den As adodb.Field

Dim fldOrgCompraCod As adodb.Field
Dim fldOrgCompraDen As adodb.Field
Dim fldCentrosCod As adodb.Field
Dim fldCentrosDen As adodb.Field
Dim fldAlmacenCod As adodb.Field
Dim fldAlmacenDen As adodb.Field
Dim fldAlmacenID As adodb.Field
Dim fldBajaLog As adodb.Field
Dim fldEmpresa As adodb.Field
Dim SQLSelectEmpresas As String
Dim SQLFromEmpresas  As String
Dim SQLWhereEmpresas As String
   
    If bCargarEmpresas Then
        SQLSelectEmpresas = ", UON4.EMPRESA, E.NIF, E.DEN, E.DIR, E.CP, E.POB, E.PAI, E.PROVI, E.ERP, E.REMITENTE "
        SQLFromEmpresas = " LEFT JOIN EMP E WITH (NOLOCK) ON  E.ID = UON4.EMPRESA "
        If sCarIniNif <> "" Then
            SQLWhereEmpresas = " AND E.NIF LIKE '" & DblQuote(sCarIniNif) & "%'"
        End If
        If sCarIniRazon <> "" Then
            SQLWhereEmpresas = SQLWhereEmpresas & " AND E.DEN LIKE '" & DblQuote(sCarIniRazon) & "%'"
        End If
        
    Else
        SQLSelectEmpresas = ", UON4.EMPRESA "
        SQLFromEmpresas = ""
    
    End If

    If Not IsMissing(UON1) Then
        If IsEmpty(UON1) Then
            UON1 = Null
        End If
        If Trim(UON1) = "" Then
            UON1 = Null
        End If
    Else
        UON1 = Null
    End If

    If Not IsMissing(UON2) Then
        If IsEmpty(UON2) Then
            UON2 = Null
        End If
        If Trim(UON2) = "" Then
            UON2 = Null
        End If
    Else
        UON2 = Null
    End If

    If Not IsMissing(UON3) Then
        If IsEmpty(UON3) Then
            UON3 = Null
        End If
        If Trim(UON3) = "" Then
            UON3 = Null
        End If
    Else
        UON3 = Null
    End If

    If Not IsMissing(UON4) Then
        If IsEmpty(UON4) Then
            UON4 = Null
        End If
        If Trim(UON4) = "" Then
            UON4 = Null
        End If
    Else
        UON4 = Null
    End If

    If CarIniCod <> "" Then     'viene por argumento
        sCad = "1"
    Else
        sCad = "0"
    End If
    
    If CarIniDen <> "" Then     'viene por argumento
        sCad = sCad & "1"
    Else
        sCad = sCad & "0"
    End If
   If Not RDep Then    'RESTRICCION DE DEPARTAMENTO
        '____________________________________________________________________________________________________________
        '////////////////////////////////////////////////////////////////////////////////////////////////////////////
        If RUO Then  'RESTRICCION DE UNIDAD
            If IsNull(UON1) Then ' La persona pertence al nivel 0
                sConsulta1 = "SELECT DISTINCT UON4.COD AS UON4COD, UON4.DEN AS UON4DEN, UON4.UON1 AS UON1COD ,UON4.UON2 AS UON2COD, UON4.UON3 AS UON3COD, UON4.BAJALOG " & SQLSelectEmpresas
                sConsulta1 = sConsulta1 & " FROM UON4 WITH (NOLOCK) " & SQLFromEmpresas
                sConsulta1 = sConsulta1 & " WHERE 1=1"
            ElseIf IsNull(UON2) Then 'La persona pertenece al nivel 1
                sConsulta1 = "SELECT DISTINCT UON4.COD AS UON4COD, UON4.DEN AS UON4DEN, UON4.UON1 AS UON1COD ,UON4.UON2 AS UON2COD, UON4.UON3 AS UON3COD,UON4.BAJALOG " & SQLSelectEmpresas
                sConsulta1 = sConsulta1 & " FROM UON4 WITH (NOLOCK) " & SQLFromEmpresas
                sConsulta1 = sConsulta1 & " WHERE UON4.UON1='" & DblQuote(CStr(UON1)) & "'"
            ElseIf IsNull(UON3) Then 'La persona pertenece al nivel 2
                sConsulta1 = "SELECT DISTINCT UON4.COD AS UON4COD, UON4.DEN AS UON4DEN, UON4.UON1 AS UON1COD ,UON4.UON2 AS UON2COD, UON4.UON3 AS UON3COD,UON4.BAJALOG " & SQLSelectEmpresas
                sConsulta1 = sConsulta1 & " FROM UON4 WITH (NOLOCK) " & SQLFromEmpresas
                sConsulta1 = sConsulta1 & " WHERE UON4.UON1='" & DblQuote(CStr(UON1)) & "' AND UON4.UON2='" & DblQuote(CStr(UON2)) & "'"
            ElseIf IsNull(UON4) Then 'La persona pertenece al nivel 2
                sConsulta1 = "SELECT DISTINCT UON4.COD AS UON4COD, UON4.DEN AS UON4DEN, UON4.UON1 AS UON1COD ,UON4.UON2 AS UON2COD,UON4.UON3 AS UON3COD ,UON4.BAJALOG " & SQLSelectEmpresas
                sConsulta1 = sConsulta1 & " FROM UON4 WITH (NOLOCK) " & SQLFromEmpresas
                sConsulta1 = sConsulta1 & " WHERE UON4.UON1='" & DblQuote(CStr(UON1)) & "'"
                sConsulta1 = sConsulta1 & " AND UON4.UON2='" & DblQuote(CStr(UON2)) & "'"
                sConsulta1 = sConsulta1 & " AND UON4.UON3='" & DblQuote(CStr(UON3)) & "'"
            Else  ' La persona pertence al nivel 4  ' Solo cargar lasuya
                sConsulta1 = "SELECT DISTINCT UON4.COD AS UON4COD, UON4.DEN AS UON4DEN, UON4.UON1 AS UON1COD ,UON4.UON2 AS UON2COD,UON4.UON3 AS UON3COD ,UON4.BAJALOG " & SQLSelectEmpresas
                sConsulta1 = sConsulta1 & " FROM UON4 WITH (NOLOCK) " & SQLFromEmpresas
                sConsulta1 = sConsulta1 & " WHERE UON4.UON1='" & DblQuote(CStr(UON1)) & "'"
                sConsulta1 = sConsulta1 & " AND UON4.UON2='" & DblQuote(CStr(UON2)) & "'"
                sConsulta1 = sConsulta1 & " AND UON4.UON3='" & DblQuote(CStr(UON3)) & "'"
                sConsulta1 = sConsulta1 & " AND UON4.COD='" & DblQuote(CStr(UON3)) & "'"
            End If
        Else     ' NO HAY RESTRICCION RUO
            sConsulta1 = "SELECT DISTINCT UON4.COD AS UON4COD, UON4.DEN AS UON4DEN, UON4.UON1 AS UON1COD ,UON4.UON2 AS UON2COD,UON4.UON3 AS UON3COD ,UON4.BAJALOG " & SQLSelectEmpresas
            sConsulta1 = sConsulta1 & " FROM UON4 WITH (NOLOCK) " & SQLFromEmpresas
            sConsulta1 = sConsulta1 & " WHERE 1=1"
        End If
        '////////////////////////////////////////////////////////////////////////////////////////////////////////////
        '----------------------------------------------------------------------------------------------------------------
        Select Case sCad
            Case "01"
                    If CoincidenciaTotal Then
                        sConsulta1 = sConsulta1 & " AND UON4.DEN = '" & DblQuote(CarIniDen) & "'"
                    Else
                        sConsulta1 = sConsulta1 & " AND UON4.DEN LIKE '" & DblQuote(CarIniDen) & "%'"
                    End If
                              
            Case "10"
                    If CoincidenciaTotal Then
                        sConsulta1 = sConsulta1 & " AND UON4.COD = '" & DblQuote(CarIniCod) & "'"
                    Else
                        sConsulta1 = sConsulta1 & " AND UON4.COD LIKE '" & DblQuote(CarIniCod) & "%'"
                    End If
                           
            Case "11"
                    If CoincidenciaTotal Then
                        sConsulta1 = sConsulta1 & " AND UON4.COD = '" & DblQuote(CarIniCod) & "'"
                        sConsulta1 = sConsulta1 & " AND UON4.DEN = '" & DblQuote(CarIniDen) & "'"
                    Else
                        sConsulta1 = sConsulta1 & " AND UON4.COD LIKE '" & DblQuote(CarIniCod) & "%'"
                        sConsulta1 = sConsulta1 & " AND UON4.DEN LIKE '" & DblQuote(CarIniDen) & "%'"
                    End If
        End Select
            
        If Not BajaLog Then
            sConsulta1 = sConsulta1 & " AND UON4.BAJALOG =0 "
        End If
        
        sConsulta1 = sConsulta1 & SQLWhereEmpresas
        
        If OrdenadasPorDen Then
            sConsulta1 = sConsulta1 & " ORDER BY UON4DEN"
        Else
            sConsulta1 = sConsulta1 & " ORDER BY UON4COD"
        End If
            
        rs.Open sConsulta1, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
          
        If rs.eof Then
            rs.Close
            Set rs = Nothing
            Set mCol = Nothing
            Set mCol = New Collection
            Exit Sub
              
        Else
            
            Set mCol = Nothing
            Set mCol = New Collection
             
            Set fldUon1Cod = rs.Fields("UON1COD")
            Set fldUon2Cod = rs.Fields("UON2COD")
            Set fldUon3Cod = rs.Fields("UON3COD")
            Set fldUon4Cod = rs.Fields("UON4COD")
            Set fldUON4Den = rs.Fields("UON4DEN")
            Set fldBajaLog = rs.Fields("BAJALOG")
            Set fldEmpresa = rs.Fields("EMPRESA")
            
            If UsarIndice Then
                lIndice = 0
                Set oUON4 = New CUnidadOrgNivel4
                
                While Not rs.eof
                    oUON4.CodUnidadOrgNivel1 = fldUon1Cod.Value
                    oUON4.CodUnidadOrgNivel2 = fldUon2Cod.Value
                    oUON4.CodUnidadOrgNivel3 = fldUon3Cod.Value
                    oUON4.Cod = fldUon4Cod.Value
                    oUON4.Den = fldUON4Den.Value
                    oUON4.BajaLog = fldBajaLog.Value
                    oUON4.idEmpresa = fldEmpresa.Value
                    If bCargarEmpresas And Not IsNull(fldEmpresa.Value) Then
                        Set oUON4.Empresa = New CEmpresa
                        With oUON4.Empresa
                            Set .Conexion = m_oConexion
                            .Id = fldEmpresa.Value
                            .NIF = rs.Fields("NIF").Value
                            .Den = rs.Fields("DEN").Value
                            .CP = rs.Fields("CP").Value
                            .Poblacion = rs.Fields("POB").Value
                            .CodPais = rs.Fields("PAI").Value
                            .CodProvi = rs.Fields("PROVI").Value
                            .Direccion = rs.Fields("DIR").Value
                            .Remitente = NullToStr(rs.Fields("REMITENTE").Value)
                        End With
                    End If
                    
                    mCol.Add oUON4, lIndice
                    rs.MoveNext
                    lIndice = lIndice + 1
                    Set oUON4 = Nothing
                    Set oUON4 = New CUnidadOrgNivel4
                    Set oUON4.Conexion = m_oConexion
                    Set oUON4.ConexionServer = m_oConexionServer
                Wend
            
            Else    '(If UsarIndice)
                
                Set oUON4 = New CUnidadOrgNivel4
                Set oUON4.Conexion = m_oConexion
                Set oUON4.ConexionServer = m_oConexionServer
                
                While Not rs.eof
                    oUON4.CodUnidadOrgNivel1 = fldUon1Cod.Value
                    oUON4.CodUnidadOrgNivel2 = fldUon2Cod.Value
                    oUON4.CodUnidadOrgNivel3 = fldUon3Cod.Value
                    oUON4.Cod = fldUon4Cod.Value
                    oUON4.Den = fldUON4Den.Value
                    oUON4.BajaLog = fldBajaLog.Value
                    oUON4.idEmpresa = fldEmpresa.Value
                    If bCargarEmpresas And Not IsNull(fldEmpresa.Value) Then
                        Set oUON4.Empresa = New CEmpresa
                        With oUON4.Empresa
                            Set .Conexion = m_oConexion
                            .Id = fldEmpresa.Value
                            .NIF = rs.Fields("NIF").Value
                            .Den = rs.Fields("DEN").Value
                            .CP = rs.Fields("CP").Value
                            .Poblacion = rs.Fields("POB").Value
                            .CodPais = rs.Fields("PAI").Value
                            .CodProvi = rs.Fields("PROVI").Value
                            .Direccion = rs.Fields("DIR").Value
                            .Remitente = NullToStr(rs.Fields("REMITENTE").Value)
                        End With
                    End If
                    
                    mCol.Add oUON4, CStr(fldUon1Cod.Value) & CStr(fldUon2Cod.Value) & CStr(fldUon3Cod.Value) & CStr(fldUon4Cod.Value)
                    rs.MoveNext
                    Set oUON4 = Nothing
                    Set oUON4 = New CUnidadOrgNivel4
                    Set oUON4.Conexion = m_oConexion
                    Set oUON4.ConexionServer = m_oConexionServer
                Wend
            End If
            
            rs.Close
            Set rs = Nothing
            Exit Sub
              
        End If '(If rs.eof)
        
    End If 'NOT RDEP

    If Not RUO Then ' No RUO + Restriccion de DEPARTAMENTO
        sConsulta3 = "SELECT DISTINCT UON4.COD AS UON4COD, UON4.DEN AS UON4DEN, UON4.UON1 AS UON1COD ,UON4.UON2 AS UON2COD,UON4.UON3 AS UON3COD, UON4.BAJALOG " & SQLSelectEmpresas
        sConsulta3 = sConsulta3 & " FROM UON4 WITH (NOLOCK) " & SQLFromEmpresas & " INNER JOIN UON3_DEP WITH (NOLOCK) ON UON4.UON1=UON3_DEP.UON1  AND UON4.UON2=UON3_DEP.UON2 AND UON4.UON3=UON3_DEP.UON3 AND UON3_DEP.DEP='" & DblQuote(CodDep) & "'"
        sConsulta3 = sConsulta3 & " WHERE 1=1"
    End If
    
    If RUO Then    '  RUO + Restriccion de DEPARTAMENTO
        If IsNull(UON1) Then
            'Es de nivel 0
            sConsulta3 = "SELECT DISTINCT UON4.COD AS UON4COD, UON4.DEN AS UON4DEN, UON4.UON1 AS UON1COD ,UON4.UON2 AS UON2COD, UON4.UON3 AS UON3COD, UON4.BAJALOG " & SQLSelectEmpresas & " FROM UON4 WITH (NOLOCK) " & SQLFromEmpresas & ",UON3_DEP WITH (NOLOCK) WHERE UON4.UON1=UON3_DEP.UON1  AND UON4.UON2=UON3_DEP.UON2 AND UON4.UON3=UON3_DEP.UON3 AND UON3_DEP.DEP='" & DblQuote(CodDep) & "'"
        Else
            If IsNull(UON2) Then
                'Es de nivel1
                sConsulta3 = "SELECT DISTINCT UON4.COD AS UON4COD, UON4.DEN AS UON4DEN, UON4.UON1 AS UON1COD ,UON4.UON2 AS UON2COD,UON4.UON3 AS UON3COD , UON4.BAJALOG " & SQLSelectEmpresas & " FROM UON4 WITH (NOLOCK) " & SQLFromEmpresas & ",UON3_DEP WITH (NOLOCK) WHERE UON4.UON1=UON3_DEP.UON1  AND UON4.UON2=UON3_DEP.UON2 AND UON4.UON3=UON3_DEP.UON3 AND UON3_DEP.DEP='" & DblQuote(CodDep) & "' AND UON4.UON1='" & DblQuote(UON1) & "'"
    
            Else
                If IsNull(UON3) Then
                    'Es de nivel 2
                    sConsulta3 = "SELECT DISTINCT UON4.COD AS UON4COD, UON4.DEN AS UON4DEN, UON4.UON1 AS UON1COD ,UON4.UON2 AS UON2COD,UON4.UON3 AS UON3COD , UON4.BAJALOG " & SQLSelectEmpresas & " FROM UON4 WITH (NOLOCK) " & SQLFromEmpresas & ",UON3_DEP WITH (NOLOCK) WHERE UON4.UON1=UON3_DEP.UON1  AND UON4.UON2=UON3_DEP.UON2 AND UON4.UON3=UON3_DEP.UON3 AND UON3_DEP.DEP='" & DblQuote(CodDep) & "' AND UON4.UON1='" & DblQuote(UON1) & "' AND UON4.UON2='" & DblQuote(UON2) & "'"
    
                Else
                    'Es de nivel 3
                    sConsulta3 = "SELECT DISTINCT UON4.COD AS UON4COD, UON4.DEN AS UON4DEN, UON4.UON1 AS UON1COD ,UON4.UON2 AS UON2COD,UON4.UON3 AS UON3COD , UON4.BAJALOG " & SQLSelectEmpresas & " FROM UON4 WITH (NOLOCK) " & SQLFromEmpresas & ",UON3_DEP WITH (NOLOCK) WHERE UON4.UON1=UON3_DEP.UON1  AND UON4.UON2=UON3_DEP.UON2 AND UON4.UON3=UON3_DEP.UON3 AND UON3_DEP.DEP='" & DblQuote(CodDep) & "' AND UON4.UON1='" & DblQuote(UON1) & "' AND UON4.UON2='" & DblQuote(UON2) & "' AND UON4.UON3='" & DblQuote(UON3) & "'"
                End If
            End If
        End If
    End If

    Select Case sCad
    Case "01"
            If CoincidenciaTotal Then
                sConsulta3 = sConsulta3 & " AND UON4.DEN = '" & DblQuote(CarIniDen) & "'"
            Else
                sConsulta3 = sConsulta3 & " AND UON4.DEN LIKE '" & DblQuote(CarIniDen) & "%'"
            End If
    Case "10"
            If CoincidenciaTotal Then
                sConsulta3 = sConsulta3 & " AND UON4.COD = '" & DblQuote(CarIniCod) & "'"
            Else
                sConsulta3 = sConsulta3 & " AND UON4.COD LIKE '" & DblQuote(CarIniCod) & "%'"
            End If
    Case "11"
            If CoincidenciaTotal Then
                sConsulta3 = sConsulta3 & " AND UON4.COD = '" & DblQuote(CarIniCod) & "'"
                sConsulta3 = sConsulta3 & " AND UON4.DEN = '" & DblQuote(CarIniDen) & "'"
            Else
                sConsulta3 = sConsulta3 & " AND UON4.COD LIKE '" & DblQuote(CarIniCod) & "%'"
                sConsulta3 = sConsulta3 & " AND UON4.DEN LIKE '" & DblQuote(CarIniDen) & "%'"
            End If
    End Select
    
    sConsulta3 = sConsulta3 & SQLWhereEmpresas
    
    If Not BajaLog Then
        sConsulta3 = sConsulta3 & " AND UON4.BAJALOG =0"
    End If
    
    If OrdenadasPorDen Then
        sConsulta3 = sConsulta3 & " ORDER BY UON4DEN"
    Else
        sConsulta3 = sConsulta3 & " ORDER BY UON4COD"
    End If
    
    rs.Open sConsulta3, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
        If rs.eof Then
            rs.Close
            Set rs = Nothing
            Set mCol = Nothing
            Set mCol = New Collection
            Exit Sub
              
        Else
            
            Set mCol = Nothing
            Set mCol = New Collection
             
            Set fldUon1Cod = rs.Fields("UON1COD")
            Set fldUon2Cod = rs.Fields("UON2COD")
            Set fldUon3Cod = rs.Fields("UON3COD")
            Set fldUon4Cod = rs.Fields("UON4COD")
            Set fldUON4Den = rs.Fields("UON4DEN")
            Set fldBajaLog = rs.Fields("BAJALOG")
            Set fldEmpresa = rs.Fields("EMPRESA")
            
            If UsarIndice Then
                lIndice = 0
                Set oUON4 = New CUnidadOrgNivel4
                
                While Not rs.eof
                    oUON4.CodUnidadOrgNivel1 = fldUon1Cod.Value
                    oUON4.CodUnidadOrgNivel2 = fldUon2Cod.Value
                    oUON4.CodUnidadOrgNivel3 = fldUon3Cod.Value
                    oUON4.Cod = fldUon4Cod.Value
                    oUON4.Den = fldUON4Den.Value
                    oUON4.BajaLog = fldBajaLog.Value
                    oUON4.idEmpresa = fldEmpresa.Value
                    If bCargarEmpresas And Not IsNull(fldEmpresa.Value) Then
                        Set oUON4.Empresa = New CEmpresa
                        With oUON4.Empresa
                            Set .Conexion = m_oConexion
                            .Id = fldEmpresa.Value
                            .NIF = rs.Fields("NIF").Value
                            .Den = rs.Fields("DEN").Value
                            .CP = rs.Fields("CP").Value
                            .Poblacion = rs.Fields("POB").Value
                            .CodPais = rs.Fields("PAI").Value
                            .CodProvi = rs.Fields("PROVI").Value
                            .Direccion = rs.Fields("DIR").Value
                            .Remitente = NullToStr(rs.Fields("REMITENTE").Value)
                        End With
                    End If
                    
                    mCol.Add oUON4, lIndice
                    rs.MoveNext
                    lIndice = lIndice + 1
                    Set oUON4 = Nothing
                    Set oUON4 = New CUnidadOrgNivel4
                    Set oUON4.Conexion = m_oConexion
                    Set oUON4.ConexionServer = m_oConexionServer
                Wend
            
            Else    '(If UsarIndice)
                
                Set oUON4 = New CUnidadOrgNivel4
                Set oUON4.Conexion = m_oConexion
                Set oUON4.ConexionServer = m_oConexionServer
                
                While Not rs.eof
                    oUON4.CodUnidadOrgNivel1 = fldUon1Cod.Value
                    oUON4.CodUnidadOrgNivel2 = fldUon2Cod.Value
                    oUON4.CodUnidadOrgNivel3 = fldUon3Cod.Value
                    oUON4.Cod = fldUon4Cod.Value
                    oUON4.Den = fldUON4Den.Value
                    oUON4.BajaLog = fldBajaLog.Value
                    oUON4.idEmpresa = fldEmpresa.Value
                    If bCargarEmpresas And Not IsNull(fldEmpresa.Value) Then
                        Set oUON4.Empresa = New CEmpresa
                        With oUON4.Empresa
                            Set .Conexion = m_oConexion
                            .Id = fldEmpresa.Value
                            .NIF = rs.Fields("NIF").Value
                            .Den = rs.Fields("DEN").Value
                            .CP = rs.Fields("CP").Value
                            .Poblacion = rs.Fields("POB").Value
                            .CodPais = rs.Fields("PAI").Value
                            .CodProvi = rs.Fields("PROVI").Value
                            .Direccion = rs.Fields("DIR").Value
                            .Remitente = NullToStr(rs.Fields("REMITENTE").Value)
                        End With
                    End If
                    
                    mCol.Add oUON4, CStr(fldUon1Cod.Value) & CStr(fldUon2Cod.Value) & CStr(fldUon3Cod.Value) & CStr(fldUon4Cod.Value)
                    rs.MoveNext
                    Set oUON4 = Nothing
                    Set oUON4 = New CUnidadOrgNivel4
                    Set oUON4.Conexion = m_oConexion
                    Set oUON4.ConexionServer = m_oConexionServer
                Wend
            End If
            
            rs.Close
            Set rs = Nothing
            Exit Sub
              
        End If '(If rs.eof)
        

        Set fldUon1Cod = Nothing
        Set fldUon2Cod = Nothing
        Set fldUon3Cod = Nothing
        Set fldUon4Cod = Nothing
        Set fldUON4Den = Nothing
        Set fldBajaLog = Nothing
        Set fldEmpresa = Nothing
    
End Sub

''' <summary>
''' Nodos CC
''' </summary>
''' <param name="pUsu">Usuario</param>
''' <returns>recordset</returns>
''' <remarks>Llamada desde: FSGClient
''' Tiempo m�ximo: < 1seg </remarks>
''' <revision>JVS 10/01/2012</revision>
Public Function NodosCC(pUsu As String) As adodb.Recordset
Dim sConsulta As String
Dim rs As New adodb.Recordset

    sConsulta = "SELECT UON4 From USU_CC_IMPUTACION WITH(NOLOCK) WHERE USU ='" & pUsu & "'"
    sConsulta = sConsulta & " AND UON1 IS NOT NULL"
    sConsulta = sConsulta & " AND UON2 IS NOT NULL"
    sConsulta = sConsulta & " AND UON3 IS NOT NULL"
    sConsulta = sConsulta & " AND UON4 IS NOT NULL"
                
    rs.CursorLocation = adUseClient
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly

    Set rs.ActiveConnection = Nothing
    Set NodosCC = rs
End Function








''' <summary>
''' Recupera las unidades organizativas de Nivel 4 y una serie de datos de �stas y los guarda
''' en una colecci�n de objetos CUnidadOrgNivel1
''' </summary>
''' <param name="OrdenadasPorDen">Boolean. True -> Ordena los resultados por Descripcion. False -> Los ordena por C�digo</param>
''' <param name=" UsarIndice">Opcional. Boolean. True -> Al agregar las Unidades Organizativas a la colecci�n, la clave de cada registro en un �ndice num�rico. False -> La clave de cada registro es su C�digo</param>
''' <param name="BajaLog">Opcional. Boolean. True -> Incluye en los resultados las UONs que sean baja l�gica. False -> Las excluye de los resultados</param>
''' <param name="pUsu">Opcional. String. Cuando se recibe un usuario, se modifica la consulta para mostrar para cada UON si el usuario tiene permiso para imputar permisos de aprovisionamiento</param>
''' <remarks>Llamada desde frmUsuarios.GenerarEstructuraOrgCC; Tiempo m�ximo < 1 seg.</remarks>
Public Sub CargarTodosLosCCNivel4(ByVal OrdenadasPorDen As Boolean, Optional ByVal UsarIndice As Boolean = False, Optional ByVal BajaLog As Boolean, Optional pUsu As String = "")

Dim rs As New adodb.Recordset

Dim sConsulta1 As String

Dim lIndice As Long
Dim oUON4 As CUnidadOrgNivel4

Dim fldUON1 As adodb.Field
Dim fldUON2 As adodb.Field
Dim fldUON3 As adodb.Field
Dim fldUon4Cod As adodb.Field
Dim fldUON4Den As adodb.Field
Dim fldEmpresa As adodb.Field

Dim fldBajaLog As adodb.Field
Dim fldCC As adodb.Field
Dim fldNodo_Usuario As adodb.Field

Dim sSelect As String

'********* Precondicion **************************************
If m_oConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CGrupoDevolverTodasLasUnidadesOrgNivel1", "No se ha establecido la conexion"
    Exit Sub
End If
'************************************************************
    
    If pUsu <> "" Then
        sConsulta1 = "SELECT UON4.UON1,UON4.UON2,UON4.UON3,UON4.COD , UON4.DEN , UON4.EMPRESA , UON4.BAJALOG,COUNT(CSM.UON4) AS CC, CASE WHEN ISNULL(UCCI.UON4, '') = '' THEN 0 ELSE 1 END NODO_USU"
    Else
        sConsulta1 = "SELECT UON4.UON1,UON4.UON2,UON4.UON3,UON4.COD , UON4.DEN , UON4.EMPRESA , UON4.BAJALOG,COUNT(CSM.UON4) AS CC "
    End If
    
    sConsulta1 = sConsulta1 & " FROM UON4 WITH (NOLOCK) LEFT JOIN CENTRO_SM_UON CSM WITH (NOLOCK) ON UON4.UON1=CSM.UON1 AND UON4.UON2=CSM.UON2 AND UON4.UON3=CSM.UON3 AND UON4.COD=CSM.UON4"
    
    If pUsu <> "" Then
        sConsulta1 = sConsulta1 & " LEFT JOIN (SELECT UON1, UON2, UON3, UON4 From USU_CC_IMPUTACION WITH (NOLOCK) "
        sConsulta1 = sConsulta1 & " WHERE USU ='" & pUsu & "'"
        sConsulta1 = sConsulta1 & " AND UON1 IS NOT NULL"
        sConsulta1 = sConsulta1 & " AND UON2 IS NOT NULL"
        sConsulta1 = sConsulta1 & " AND UON3 IS NOT NULL"
        sConsulta1 = sConsulta1 & " AND UON4 IS NOT NULL) UCCI"
        sConsulta1 = sConsulta1 & " ON UCCI.UON1 = UON4.UON1 AND UCCI.UON2 = UON4.UON2 AND UCCI.UON3 = UON4.UON3 AND UCCI.UON4 = UON4.COD "
    End If
    
    sConsulta1 = sConsulta1 & "  WHERE 1=1"
        
    If Not BajaLog Then
        sConsulta1 = sConsulta1 & " AND UON4.BAJALOG =0 "
    End If
        
    If pUsu <> "" Then
        sConsulta1 = sConsulta1 & " GROUP BY UON4.UON1,UON4.UON2,UON4.UON3,UON4.COD , UON4.DEN , UON4.EMPRESA , UON4.BAJALOG, UCCI.UON4"
    Else
        sConsulta1 = sConsulta1 & " GROUP BY UON4.UON1,UON4.UON2,UON4.UON3,UON4.COD , UON4.DEN , UON4.EMPRESA , UON4.BAJALOG"
    End If
    
    If OrdenadasPorDen Then
        sConsulta1 = sConsulta1 & " ORDER BY DEN"
    Else
        sConsulta1 = sConsulta1 & " ORDER BY COD"
    End If
         
    rs.Open sConsulta1, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    
    If rs.eof Then
            
        rs.Close
        Set rs = Nothing
        Set mCol = Nothing
        Set mCol = New Collection
        Exit Sub
          
    Else
        
        Set mCol = Nothing
        Set mCol = New Collection
         
        Set fldUON1 = rs.Fields("UON1")
        Set fldUON2 = rs.Fields("UON2")
        Set fldUON3 = rs.Fields("UON3")
        Set fldUon4Cod = rs.Fields("COD")
        Set fldUON4Den = rs.Fields("DEN")
        Set fldEmpresa = rs.Fields("EMPRESA")
        Set fldBajaLog = rs.Fields("BAJALOG")
        Set fldCC = rs.Fields("CC")
        If pUsu <> "" Then
            Set fldNodo_Usuario = rs.Fields("NODO_USU")
        End If
        While Not rs.eof
            Set oUON4 = Nothing
            Set oUON4 = New CUnidadOrgNivel4
            Set oUON4.Conexion = m_oConexion
            Set oUON4.ConexionServer = m_oConexionServer

            oUON4.CodUnidadOrgNivel1 = fldUON1.Value
            oUON4.CodUnidadOrgNivel2 = fldUON2.Value
            oUON4.CodUnidadOrgNivel3 = fldUON3.Value
            oUON4.Cod = fldUon4Cod.Value
            oUON4.Den = fldUON4Den.Value
            oUON4.idEmpresa = fldEmpresa.Value
            oUON4.BajaLog = fldBajaLog.Value
            oUON4.CC = IIf(fldCC.Value > 0, True, False)
            If pUsu <> "" Then
                oUON4.Nodo_Usuario = SQLBinaryToBoolean(fldNodo_Usuario.Value)
            End If
            If UsarIndice Then
                lIndice = 0
                
                mCol.Add oUON4, CStr(lIndice)
                rs.MoveNext
                lIndice = lIndice + 1
                        
            Else
                mCol.Add oUON4, CStr(fldUON1.Value) & CStr(fldUON2.Value) & CStr(fldUON3.Value) & CStr(fldUon4Cod.Value)
                rs.MoveNext
            End If
        Wend
        
        rs.Close
        Set rs = Nothing
        Exit Sub
          
    End If

End Sub


Public Function Add(ByVal Cod As String, ByVal Den As String, ByVal CodUnidadOrgNivel1 As Variant, ByVal CodUnidadOrgNivel2 As Variant, ByVal CodUnidadOrgNivel3 As Variant, Optional ByVal varIndice As Variant) As CUnidadOrgNivel4
    'create a new object
    Dim objnewmember As CUnidadOrgNivel4
    Dim sCod As String
    
    Set objnewmember = New CUnidadOrgNivel4

    Set objnewmember.Conexion = m_oConexion
    Set objnewmember.ConexionServer = m_oConexionServer
    
    'set the properties passed into the method
    objnewmember.Cod = Cod
    objnewmember.Den = Den
    
    If Not IsMissing(CodUnidadOrgNivel1) Then
        objnewmember.CodUnidadOrgNivel1 = CodUnidadOrgNivel1
    End If
    If Not IsMissing(CodUnidadOrgNivel2) Then
        objnewmember.CodUnidadOrgNivel2 = CodUnidadOrgNivel2
    End If
    If Not IsMissing(CodUnidadOrgNivel3) Then
        objnewmember.CodUnidadOrgNivel3 = CodUnidadOrgNivel3
    End If
    
    If Not IsMissing(varIndice) And Not IsNull(varIndice) Then
        objnewmember.Indice = varIndice
        mCol.Add objnewmember, CStr(varIndice)
    Else
        sCod = CodUnidadOrgNivel1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(CodUnidadOrgNivel1))
        sCod = sCod & CodUnidadOrgNivel2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(CodUnidadOrgNivel2))
        sCod = sCod & CodUnidadOrgNivel3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(CodUnidadOrgNivel3))
        sCod = sCod & Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON4 - Len(Cod))
        mCol.Add objnewmember, sCod
    End If
    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing

End Function

''' Revisado por: blp. Fecha: 19/06/2012
''' <summary>
''' Recupera las unidades organizativas de Nivel 4 y una serie de datos de �stas y los guarda
''' en una colecci�n de objetos CUnidadOrgNivel1
''' </summary>
''' <param name="OrdenadasPorDen">Boolean. True -> Ordena los resultados por Descripcion. False -> Los ordena por C�digo</param>
''' <param name="pUsu">Usuario</param>
''' <remarks>Llamada desde frmUsuarios.GenerarEstructuraOrgGestor; Tiempo m�ximo < 1 seg.</remarks>
Public Sub CargarTodosLosCCGestorNivel4(ByVal OrdenadasPorDen As Boolean, pUsu As String)

Dim lIndice As Long
Dim oUON4 As CUnidadOrgNivel4
Dim fldUON1 As adodb.Field
Dim fldUON2 As adodb.Field
Dim fldUON3 As adodb.Field
Dim fldUon4Cod As adodb.Field
Dim fldUON4Den As adodb.Field
Dim fldEmpresa As adodb.Field
Dim fldBajaLog As adodb.Field
Dim fldCC As adodb.Field
Dim fldNodo_Usuario As adodb.Field
Dim fldTieneGestor As Boolean

Dim sConsulta1 As String
Dim adoComm As adodb.Command
Set adoComm = New adodb.Command
Set adoComm.ActiveConnection = m_oConexion.ADOCon
Dim rs As New adodb.Recordset
Dim adoParam As adodb.Parameter

'********* Precondicion **************************************
If m_oConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CGrupoDevolverTodasLasUnidadesOrgNivel1", "No se ha establecido la conexion"
    Exit Sub
End If
'************************************************************
    
    sConsulta1 = "SELECT UON4.UON1,UON4.UON2,UON4.UON3,UON4.COD , UON4.DEN , UON4.EMPRESA , UON4.BAJALOG,COUNT(CSM.UON4) AS CC "
    sConsulta1 = sConsulta1 & ", CASE WHEN SUM(CASE WHEN P5I.GESTOR=? THEN 1 ELSE 0 END)>0 THEN 1 ELSE 0 END AS ESGESTOR"
    
    Set adoParam = adoComm.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(pUsu) + 1, Value:=pUsu)
    adoComm.Parameters.Append adoParam
    
    sConsulta1 = sConsulta1 & " FROM UON4 WITH (NOLOCK) "
    sConsulta1 = sConsulta1 & " LEFT JOIN CENTRO_SM_UON CSM WITH (NOLOCK) "
    sConsulta1 = sConsulta1 & "     ON UON4.UON1=CSM.UON1 "
    sConsulta1 = sConsulta1 & "     AND UON4.UON2=CSM.UON2 "
    sConsulta1 = sConsulta1 & "     AND UON4.UON3=CSM.UON3 "
    sConsulta1 = sConsulta1 & "     AND UON4.COD=CSM.UON4"
    sConsulta1 = sConsulta1 & " LEFT JOIN PRES5_UON P5U WITH(NOLOCK)"
    sConsulta1 = sConsulta1 & "     ON P5U.UON1 = CSM.UON1"
    sConsulta1 = sConsulta1 & "     AND P5U.UON2 = CSM.UON2"
    sConsulta1 = sConsulta1 & "     AND P5U.UON3 = CSM.UON3"
    sConsulta1 = sConsulta1 & "     AND P5U.UON4 = CSM.UON4"
    sConsulta1 = sConsulta1 & " LEFT JOIN PRES5_IMPORTES P5I WITH (NOLOCK)"
    sConsulta1 = sConsulta1 & "     ON P5U.PRES0=P5I.PRES0"
    sConsulta1 = sConsulta1 & "     AND ISNULL(P5U.PRES1,'') = ISNULL(P5I.PRES1,'')"
    sConsulta1 = sConsulta1 & "     AND ISNULL(P5U.PRES2,'') = ISNULL(P5I.PRES2,'')"
    sConsulta1 = sConsulta1 & "     AND ISNULL(P5U.PRES3,'') = ISNULL(P5I.PRES3,'')"
    sConsulta1 = sConsulta1 & "     AND ISNULL(P5U.PRES4,'') = ISNULL(P5I.PRES4,'')"
    sConsulta1 = sConsulta1 & "     AND P5I.CERRADO = 0"
    sConsulta1 = sConsulta1 & "     AND P5I.GESTOR = ?"
    sConsulta1 = sConsulta1 & "  WHERE 1=1"
        
    Set adoParam = adoComm.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(pUsu) + 1, Value:=pUsu)
    adoComm.Parameters.Append adoParam
        
    sConsulta1 = sConsulta1 & " AND UON4.BAJALOG =0 "
        
    sConsulta1 = sConsulta1 & " GROUP BY UON4.UON1,UON4.UON2,UON4.UON3,UON4.COD , UON4.DEN , UON4.EMPRESA , UON4.BAJALOG"
    
    If OrdenadasPorDen Then
        sConsulta1 = sConsulta1 & " ORDER BY DEN"
    Else
        sConsulta1 = sConsulta1 & " ORDER BY COD"
    End If
         
    adoComm.CommandText = sConsulta1
    adoComm.CommandType = adCmdText
    adoComm.Prepared = True
    rs.CursorType = adOpenForwardOnly
    rs.LockType = adLockReadOnly
    Set rs = adoComm.Execute
    
    If rs.eof Then
        rs.Close
        Set rs = Nothing
        Set adoComm = Nothing
        Set mCol = Nothing
        Set mCol = New Collection
        Exit Sub
    Else
        Set mCol = Nothing
        Set mCol = New Collection
        Set fldUON1 = rs.Fields("UON1")
        Set fldUON2 = rs.Fields("UON2")
        Set fldUON3 = rs.Fields("UON3")
        Set fldUon4Cod = rs.Fields("COD")
        Set fldUON4Den = rs.Fields("DEN")
        Set fldEmpresa = rs.Fields("EMPRESA")
        Set fldBajaLog = rs.Fields("BAJALOG")
        Set fldCC = rs.Fields("CC")
        fldTieneGestor = rs.Fields("ESGESTOR")
        While Not rs.eof
            Set oUON4 = Nothing
            Set oUON4 = New CUnidadOrgNivel4
            Set oUON4.Conexion = m_oConexion
            Set oUON4.ConexionServer = m_oConexionServer

            oUON4.CodUnidadOrgNivel1 = fldUON1.Value
            oUON4.CodUnidadOrgNivel2 = fldUON2.Value
            oUON4.CodUnidadOrgNivel3 = fldUON3.Value
            oUON4.Cod = fldUon4Cod.Value
            oUON4.Den = fldUON4Den.Value
            oUON4.idEmpresa = fldEmpresa.Value
            oUON4.BajaLog = fldBajaLog.Value
            oUON4.CC = IIf(fldCC.Value > 0, True, False)
            oUON4.TieneGestor = CBool(fldTieneGestor)
            mCol.Add oUON4, CStr(fldUON1.Value) & CStr(fldUON2.Value) & CStr(fldUON3.Value) & CStr(fldUon4Cod.Value)
            rs.MoveNext
        Wend
        
        rs.Close
        Set rs = Nothing
        Exit Sub
          
    End If

End Sub
