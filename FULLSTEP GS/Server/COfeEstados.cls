VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "COfeEstados"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* COfeEstados **********************************
'*             Autor : Javier Arana
'*             Creada : 13/8/98
'****************************************************************


Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum
Private mCol As Collection
Private mvarConexion As CConexion
Private mvarConexionServer As CConexionDeUseServer
Public mvarEOF As Boolean

''' <summary>Carga estados de oferta seg�n los criterios indicados</summary>
''' <param name="NumMaximo">N� max. de registros a cargar</param>
''' <param name="CaracteresInicialesCod">caracteres iniciales del c�digo</param>
''' <param name="CaracteresInicialesDen">caracteres iniciales de la denominaci�n</param>
''' <param name="OrdenadosPorDen">orden por denominaci�n</param>
''' <param name="UsarIndice">usar �ndice</param>
''' <param name="Idi">Idioma</param>
''' <remarks>Llamada desde: FSGSClient; Tiempo m�ximo: < 1seg </remarks>
''' <revision>LTG 09/01/2012</revision>

Public Sub CargarTodosLosOfeEstadosDesde(ByVal NumMaximo As Integer, Optional ByVal CaracteresInicialesCod As String, _
        Optional ByVal CaracteresInicialesDen As String, Optional ByVal OrdenadosPorDen As Boolean, Optional ByVal UsarIndice As Boolean, _
        Optional ByVal Idi As String)
    Dim rs As adodb.Recordset
    Dim fldCod As adodb.Field
    Dim fldDen As adodb.Field
    Dim fldComp As adodb.Field
    Dim fldAdj As adodb.Field
    Dim fldIdioma As adodb.Field
    
    Dim sConsulta As String
    
    Dim lIndice As Long

    
    ''' Generacion del SQL a partir de los parametros
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If CaracteresInicialesCod = "" And CaracteresInicialesDen = "" Then
        
        'sConsulta = "SELECT * FROM OFEEST"
        sConsulta = "SELECT TOP " & NumMaximo & " OFEEST.COD,OFEEST.COMP,OFEEST.ADJ,OFEEST.FECACT,OFEEST_IDIOMA.DEN,OFEEST_IDIOMA.IDIOMA FROM OFEEST WITH (NOLOCK) "
        sConsulta = sConsulta & " INNER JOIN OFEEST_IDIOMA WITH (NOLOCK) ON OFEEST.COD = OFEEST_IDIOMA.COD"
       
    Else
    
        If Not (CaracteresInicialesCod = "") And Not (CaracteresInicialesDen = "") Then
                sConsulta = "SELECT TOP " & NumMaximo & " OFEEST.COD,OFEEST.COMP,OFEEST.ADJ,OFEEST.FECACT,OFEEST_IDIOMA.DEN,OFEEST_IDIOMA.IDIOMA FROM OFEEST WITH (NOLOCK) "
                sConsulta = sConsulta & " INNER JOIN OFEEST_IDIOMA WITH (NOLOCK) ON OFEEST.COD = OFEEST_IDIOMA.COD WHERE "
                
                'sConsulta = "SELECT * FROM OFEEST WHERE"
                sConsulta = sConsulta & " COD >='" & DblQuote(CaracteresInicialesCod) & "'"
                sConsulta = sConsulta & " AND DEN >='" & DblQuote(CaracteresInicialesDen) & "'"
                        
        Else
            
            If Not (CaracteresInicialesCod = "") Then
                    sConsulta = "SELECT TOP " & NumMaximo & " OFEEST.COD,OFEEST.COMP,OFEEST.ADJ,OFEEST.FECACT,OFEEST_IDIOMA.DEN,OFEEST_IDIOMA.IDIOMA FROM OFEEST WITH (NOLOCK) "
                    sConsulta = sConsulta & " INNER JOIN OFEEST_IDIOMA WITH (NOLOCK) ON OFEEST.COD = OFEEST_IDIOMA.COD WHERE "
                
                    'sConsulta = "SELECT * FROM OFEEST WHERE"
                    sConsulta = sConsulta & " COD >='" & DblQuote(CaracteresInicialesCod) & "'"
            Else
                    sConsulta = "SELECT TOP " & NumMaximo & " OFEEST.COD,OFEEST.COMP,OFEEST.ADJ,OFEEST.FECACT,OFEEST_IDIOMA.DEN,OFEEST_IDIOMA.IDIOMA FROM OFEEST WITH (NOLOCK) "
                    sConsulta = sConsulta & " INNER JOIN OFEEST_IDIOMA WITH (NOLOCK) ON OFEEST.COD = OFEEST_IDIOMA.COD WHERE "
                
                    'sConsulta = "SELECT * FROM OFEEST WHERE"
                    sConsulta = sConsulta & " DEN >='" & DblQuote(CaracteresInicialesDen) & "'"
            End If
        
        End If
               
    End If
    
    'Filtra por el idioma
    If Not IsMissing(Idi) And Idi <> "" Then
        sConsulta = sConsulta & " AND IDIOMA = '" & Idi & "'"
    End If

    If OrdenadosPorDen Then
        sConsulta = sConsulta & " ORDER BY OFEEST_IDIOMA.DEN"
    Else
        sConsulta = sConsulta & " ORDER BY OFEEST.COD"
    End If
          
    ''' Crear el Recordset
    
    'ado  Set rdores = mvarConexion.rdoSummitCon.OpenResultset(sConsulta, rdOpenForwardOnly, rdConcurReadOnly, rdExecDirect)
    Set rs = New adodb.Recordset
    rs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
          
    If rs.eof Then
            
        rs.Close
        Set rs = Nothing
        Set mCol = Nothing
        Set mCol = New Collection
        Exit Sub
          
    Else
        
        ''' Llenar la coleccion a partir del Recordset
        ''' teniendo en cuenta el maximo
        
        Set mCol = Nothing
        Set mCol = New Collection
        Set fldCod = rs.Fields("COD")
        Set fldDen = rs.Fields("DEN")
        Set fldComp = rs.Fields("COMP")
        Set fldAdj = rs.Fields("ADJ")
        Set fldIdioma = rs.Fields("IDIOMA")
         
        If UsarIndice Then
            
            lIndice = 0
                    
            While Not rs.eof
      
                If Not IsMissing(Idi) Then
                    Me.Add fldCod.Value, Idi, IIf(IsNull(fldDen.Value), "", fldDen.Value), fldComp.Value, fldAdj.Value, lIndice
                Else
                    Me.Add fldCod.Value, fldIdioma.Value, IIf(IsNull(fldDen.Value), "", fldDen.Value), fldComp.Value, fldAdj.Value, lIndice
                End If
                rs.MoveNext
                lIndice = lIndice + 1
                
            Wend
            
            If Not rs.eof Then
                mvarEOF = False
            Else
                mvarEOF = True
            End If
                        
        Else
            
            
            While Not rs.eof
                
                If Not IsMissing(Idi) Then
                    Me.Add fldCod.Value, Idi, IIf(IsNull(fldDen.Value), "", fldDen.Value), fldComp.Value, fldAdj.Value
                Else
                    Me.Add fldCod.Value, fldIdioma.Value, IIf(IsNull(fldDen.Value), "", fldDen.Value), fldComp.Value, fldAdj.Value
                End If
                rs.MoveNext
    
            Wend
            
            If Not rs.eof Then
                mvarEOF = False
            Else
                mvarEOF = True
            End If
            
        End If
        
        rs.Close
        Set rs = Nothing
        Set fldCod = Nothing
        Set fldDen = Nothing
        Set fldComp = Nothing
        Set fldAdj = Nothing
        Set fldIdioma = Nothing
          
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "COfeEstados", "CargarTodosLosOfeEstadosDesde", ERR, Erl)
      Exit Sub
   End If
    
End Sub

Public Sub CargarTodosLosOfeEstados(Optional ByVal CaracteresInicialesCod As String, Optional ByVal CaracteresInicialesDen As String, Optional ByVal CoincidenciaTotal As Boolean, Optional ByVal OrdenadosPorDen As Boolean, Optional ByVal OrdenadosPorComp As Boolean, Optional ByVal OrdenadosPorAdj As Boolean, Optional ByVal UsarIndice As Boolean, Optional ByVal Idi As String)
'ado  Dim rdores As rdoResultset
Dim rs As adodb.Recordset
Dim fldCod As adodb.Field
Dim fldDen As adodb.Field
Dim fldComp As adodb.Field
Dim fldAdj As adodb.Field
Dim fldIdioma As adodb.Field
    
Dim sConsulta As String
Dim lIndice As Long


If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If CaracteresInicialesCod = "" And CaracteresInicialesDen = "" Then
    
    'sConsulta = "SELECT * FROM OFEEST"
    sConsulta = "SELECT OFEEST.COD,OFEEST.COMP,OFEEST.ADJ,OFEEST.FECACT,OFEEST_IDIOMA.DEN,OFEEST_IDIOMA.IDIOMA FROM OFEEST WITH (NOLOCK) LEFT JOIN OFEEST_IDIOMA WITH (NOLOCK) ON OFEEST.COD= OFEEST_IDIOMA.COD"
    'Filtra por el idioma
    If Not IsMissing(Idi) And Idi <> "" Then
        sConsulta = sConsulta & " AND IDIOMA = '" & Idi & "'"
    End If
            
   
Else
    If Not (CaracteresInicialesCod = "") And Not (CaracteresInicialesDen = "") Then
        
        If CoincidenciaTotal Then
            sConsulta = "SELECT OFEEST.*,OFEEST_IDIOMA.DEN,OFEEST_IDIOMA.IDIOMA FROM OFEEST WITH (NOLOCK) LEFT JOIN OFEEST_IDIOMA WITH (NOLOCK) ON OFEEST.COD= OFEEST_IDIOMA.COD"
            'Filtra por el idioma
            If Not IsMissing(Idi) And Idi <> "" Then
                sConsulta = sConsulta & " AND IDIOMA = '" & Idi & "'"
            End If
            sConsulta = sConsulta & " WHERE"
                
            'sConsulta = "SELECT * FROM OFEEST WHERE"
            sConsulta = sConsulta & " OFEEST.COD ='" & DblQuote(CaracteresInicialesCod) & "'"
            sConsulta = sConsulta & " AND DEN='" & DblQuote(CaracteresInicialesDen) & "'"
            
        Else
            sConsulta = "SELECT OFEEST.*,OFEEST_IDIOMA.DEN,OFEEST_IDIOMA.IDIOMA FROM OFEEST WITH (NOLOCK) LEFT JOIN OFEEST_IDIOMA WITH (NOLOCK) ON OFEEST.COD= OFEEST_IDIOMA.COD"
            'Filtra por el idioma
            If Not IsMissing(Idi) And Idi <> "" Then
                sConsulta = sConsulta & " AND IDIOMA = '" & Idi & "'"
            End If
            sConsulta = sConsulta & " WHERE"
                
            'sConsulta = "SELECT * FROM OFEEST WHERE"
            sConsulta = sConsulta & " OFEEST.COD LIKE '" & DblQuote(CaracteresInicialesCod) & "%'"
            sConsulta = sConsulta & " DEN LIKE '" & DblQuote(CaracteresInicialesDen) & "%'"
                       
        End If
                    
    Else
        
        If Not (CaracteresInicialesCod = "") Then
            
            If CoincidenciaTotal Then
                sConsulta = "SELECT OFEEST.*,OFEEST_IDIOMA.DEN,OFEEST_IDIOMA.IDIOMA FROM OFEEST WITH (NOLOCK) LEFT JOIN OFEEST_IDIOMA WITH (NOLOCK) ON OFEEST.COD= OFEEST_IDIOMA.COD"
                'Filtra por el idioma
                If Not IsMissing(Idi) And Idi <> "" Then
                    sConsulta = sConsulta & " AND IDIOMA = '" & Idi & "'"
                End If
                sConsulta = sConsulta & " WHERE"
                
                'sConsulta = "SELECT * FROM OFEEST WHERE"
                sConsulta = sConsulta & " OFEEST.COD ='" & DblQuote(CaracteresInicialesCod) & "'"
                
            Else
                sConsulta = "SELECT OFEEST.*,OFEEST_IDIOMA.DEN,OFEEST_IDIOMA.IDIOMA FROM OFEEST WITH (NOLOCK) LEFT JOIN OFEEST_IDIOMA WITH (NOLOCK) ON OFEEST.COD= OFEEST_IDIOMA.COD"
                'Filtra por el idioma
                If Not IsMissing(Idi) And Idi <> "" Then
                    sConsulta = sConsulta & " AND IDIOMA = '" & Idi & "'"
                End If
                sConsulta = sConsulta & " WHERE"
                
                'sConsulta = "SELECT * FROM OFEEST WHERE"
                sConsulta = sConsulta & " OFEEST.COD LIKE '" & DblQuote(CaracteresInicialesCod) & "%'"
                
            End If
            
        Else
            
            If CoincidenciaTotal Then
                sConsulta = "SELECT OFEEST.*,OFEEST_IDIOMA.DEN,OFEEST_IDIOMA.IDIOMA FROM OFEEST WITH (NOLOCK) LEFT JOIN OFEEST_IDIOMA WITH (NOLOCK) ON OFEEST.COD= OFEEST_IDIOMA.COD"
                'Filtra por el idioma
                If Not IsMissing(Idi) And Idi <> "" Then
                    sConsulta = sConsulta & " AND IDIOMA = '" & Idi & "'"
                End If
                sConsulta = sConsulta & " WHERE"
                
                'sConsulta = "SELECT * FROM OFEEST WHERE"
                sConsulta = sConsulta & " DEN='" & DblQuote(CaracteresInicialesDen) & "'"
                
            Else
           
                sConsulta = "SELECT OFEEST.*,OFEEST_IDIOMA.DEN,OFEEST_IDIOMA.IDIOMA FROM OFEEST WITH (NOLOCK) LEFT JOIN OFEEST_IDIOMA WITH (NOLOCK) ON OFEEST.COD= OFEEST_IDIOMA.COD"
                'Filtra por el idioma
                If Not IsMissing(Idi) And Idi <> "" Then
                    sConsulta = sConsulta & " AND IDIOMA = '" & Idi & "'"
                End If
                sConsulta = sConsulta & " WHERE"
                
                'sConsulta = "SELECT * FROM OFEEST WHERE"
                sConsulta = sConsulta & " DEN LIKE '" & DblQuote(CaracteresInicialesDen) & "%'"
                  
            End If
            
        End If
    
    End If
           
End If

If OrdenadosPorDen Then
    sConsulta = sConsulta & " ORDER BY OFEEST_IDIOMA.DEN,OFEEST.COD"
Else
    If OrdenadosPorComp Then
        sConsulta = sConsulta & " ORDER BY COMP"
    Else
        If OrdenadosPorAdj Then
            sConsulta = sConsulta & " ORDER BY ADJ"
        Else
            sConsulta = sConsulta & " ORDER BY OFEEST.COD,OFEEST_IDIOMA.DEN"
        End If
    End If
End If

'Set rdores = mvarConexion.rdoSummitCon.OpenResultset(sConsulta, rdOpenForwardOnly, rdConcurReadOnly, rdExecDirect)
Set rs = New adodb.Recordset
rs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly

If rs.eof Then
        
    rs.Close
    Set rs = Nothing
    Exit Sub
      
Else
    
    Set mCol = Nothing
    Set mCol = New Collection
    Set fldCod = rs.Fields("COD")
    Set fldDen = rs.Fields("DEN")
    Set fldComp = rs.Fields("COMP")
    Set fldAdj = rs.Fields("ADJ")
    Set fldIdioma = rs.Fields("IDIOMA")
        
    If UsarIndice Then
        
        lIndice = 0
        
        While Not rs.eof
            
            If Not IsMissing(Idi) Then
                Me.Add fldCod.Value, Idi, IIf(IsNull(fldDen.Value), "", fldDen.Value), fldComp.Value, fldAdj.Value, lIndice
            Else
                
                Me.Add fldCod.Value, fldIdioma.Value, IIf(IsNull(fldDen.Value), "", fldDen.Value), fldComp.Value, fldAdj.Value, lIndice
            End If
            rs.MoveNext
            lIndice = lIndice + 1
        Wend
        
    Else
        
        While Not rs.eof
            
            If Not IsMissing(Idi) Then
                Me.Add fldCod.Value, Idi, IIf(IsNull(fldDen.Value), "", fldDen.Value), fldComp.Value, fldAdj.Value
            Else
                Me.Add fldCod.Value, fldIdioma.Value, IIf(IsNull(fldDen.Value), "", fldDen.Value), fldComp.Value, fldAdj.Value
            End If
            rs.MoveNext
        Wend
    End If
    
    rs.Close
    Set rs = Nothing
    Set fldCod = Nothing
    Set fldDen = Nothing
    Set fldComp = Nothing
    Set fldAdj = Nothing
    Set fldIdioma = Nothing
    Exit Sub
      
End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "COfeEstados", "CargarTodosLosOfeEstados", ERR, Erl)
      Exit Sub
   End If
End Sub
Public Function Add(ByVal Cod As String, ByVal Idi As String, ByVal Den As String, ByVal Comp As Boolean, ByVal Adj As Boolean, Optional ByVal varIndice As Variant) As COfeEstado
    'create a new object
    Dim objnewmember As COfeEstado
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New COfeEstado
   
    objnewmember.Cod = Cod
    objnewmember.Den = Den
    objnewmember.Adjudicable = Adj
    objnewmember.Comparable = Comp
    objnewmember.Idioma = Idi
    
    Set objnewmember.Conexion = mvarConexion
    Set objnewmember.ConexionServer = mvarConexionServer
    
    If Not IsMissing(varIndice) And Not IsNull(varIndice) Then
        objnewmember.Indice = varIndice
        mCol.Add objnewmember, CStr(varIndice)
    Else
    'set the properties passed into the method
       mCol.Add objnewmember, CStr(Cod)
    End If
    
    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "COfeEstados", "Add", ERR, Erl)
      Exit Function
   End If

End Function

Public Property Get Item(vntIndexKey As Variant) As COfeEstado
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Friend Property Set Conexion(ByVal con As CConexion)
Set mvarConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = mvarConexion
End Property

Friend Property Set ConexionServer(ByVal con As CConexionDeUseServer)
Set mvarConexionServer = con
End Property

Friend Property Get ConexionServer() As CConexionDeUseServer
Set ConexionServer = mvarConexionServer
End Property

Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
     Count = mCol.Count
End If


End Property


Public Sub Remove(vntIndexKey As Variant)
   
    mCol.Remove vntIndexKey
End Sub


Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
     Set NewEnum = mCol.[_NewEnum]
End Property


Private Sub Class_Initialize()
    Set mCol = New Collection
End Sub


Private Sub Class_Terminate()
    'destroys collection when this class is terminated
    Set mCol = Nothing
    Set mvarConexion = Nothing
    Set mvarConexionServer = Nothing
End Sub
Public Function DevolverLosCodigos() As TipoDatosCombo
   
   Dim Codigos As TipoDatosCombo
   Dim iCont As Integer
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
   ReDim Codigos.Cod(mCol.Count)
   ReDim Codigos.Den(mCol.Count)
   
    For iCont = 0 To mCol.Count - 1
        
        Codigos.Cod(iCont) = mCol(iCont + 1).Cod
        Codigos.Den(iCont) = mCol(iCont + 1).Den
    
    Next

   DevolverLosCodigos = Codigos
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "COfeEstados", "DevolverLosCodigos", ERR, Erl)
      Exit Function
   End If
   
End Function

Public Function BorrarEnModoIndice(ByVal iIndice As Integer)
Dim IndFor As Integer
Dim oEst As COfeEstado

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    For IndFor = iIndice To mCol.Count - 2
            
        mCol.Remove (CStr(IndFor))
        Set oEst = mCol.Item(CStr(IndFor + 1))
        mCol.Add oEst, CStr(IndFor)
        Set oEst = Nothing
        
    Next IndFor
    
    mCol.Remove CStr(IndFor)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "COfeEstados", "BorrarEnModoIndice", ERR, Erl)
      Exit Function
   End If
    
End Function
Public Property Get eof() As Boolean
    eof = mvarEOF
End Property
Public Property Let eof(ByVal vNewValue As Boolean)
    mvarEOF = vNewValue
End Property

''' <summary>Elimina el/los estado/s de las ofertas seleccionados de la base de datos.</summary>
''' <param name="aCodigos">Es un array con los c�digos de las estados de las ofertas a eliminar.</param>
''' <returns>Variable tipo TipoErrorSummit con el error si se ha producido</returns>
''' <remarks>Llamada desde: FSGSClient; Tiempo m�ximo: < 1seg </remarks>
''' <revision>LTG 09/01/2012</revision>

Public Function EliminarOfeEstadosDeBaseDatos(aCodigos As Variant) As TipoErrorSummit
    Dim iRes As Integer
    Dim i As Integer
    Dim aNoEliminados() As Variant
    Dim udtTESError As TipoErrorSummit
    Dim btrans As Boolean
    Dim AdoRes As adodb.Recordset
    Dim sConsulta As String
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    udtTESError.NumError = TESnoerror
    iRes = 0
    If g_oErrores.fg_bProgramando Then On Error GoTo ERROR_Cls:
 
        For i = 1 To UBound(aCodigos)
            mvarConexion.ADOCon.Execute "BEGIN TRANSACTION"
            btrans = True
            mvarConexion.ADOCon.Execute "DELETE FROM OFEEST_IDIOMA WHERE COD='" & DblQuote(aCodigos(i)) & "'"
            If mvarConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR_Cls
            mvarConexion.ADOCon.Execute "DELETE FROM OFEEST WHERE COD='" & DblQuote(aCodigos(i)) & "'"
            If mvarConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR_Cls
            If (btrans) Then
                mvarConexion.ADOCon.Execute "COMMIT TRANSACTION"
            End If
            btrans = False
        Next i
        
        If iRes > 0 Then
            udtTESError.NumError = TESImposibleEliminar
            udtTESError.Arg1 = aNoEliminados
        End If
        EliminarOfeEstadosDeBaseDatos = udtTESError
        Exit Function
        
        'Zona de tratamiento del error
ERROR_Cls:
        udtTESError = basErrores.TratarError(mvarConexion.ADOCon.Errors)
        If (mvarConexion.ADOCon.Errors(0).NativeError = 547) Then
            ReDim Preserve aNoEliminados(3, iRes)
            aNoEliminados(0, iRes) = aCodigos(i)
            aNoEliminados(1, iRes) = udtTESError.Arg1
            aNoEliminados(2, iRes) = i
            
            Set AdoRes = New adodb.Recordset
            Select Case udtTESError.Arg1
                Case 66  'PARGEN_DEF
                    sConsulta = "SELECT ID, ESTINI FROM PARGEN_DEF WITH (NOLOCK) WHERE ESTINI='" & DblQuote(aCodigos(i)) & "'"
                    AdoRes.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
                    aNoEliminados(3, iRes) = CStr(AdoRes.Fields.Item(0) & " - " & AdoRes.Fields.Item(1))
                Case 84  'PROCE_OFE
                    sConsulta = "SELECT ANYO, GMN1, PROCE, PROVE, OFE FROM PROCE_OFE WITH (NOLOCK) WHERE EST='" & DblQuote(aCodigos(i)) & "'"
                    AdoRes.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
                    aNoEliminados(3, iRes) = CStr(AdoRes.Fields.Item(0) & "/" & AdoRes.Fields.Item(1) & "/" & AdoRes.Fields.Item(2) & " - " & AdoRes.Fields.Item(3) & " - " & AdoRes.Fields.Item(4))
            End Select
            AdoRes.Close
            Set AdoRes = Nothing
            
            iRes = iRes + 1
            If (btrans) Then
                mvarConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
                btrans = False
            End If
            Resume Next
        Else
            mvarConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
            EliminarOfeEstadosDeBaseDatos = udtTESError
        End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      'Call g_oErrores.TratarError("M�dulo de clase", "COfeEstados", "EliminarOfeEstadosDeBaseDatos", ERR, Erl)
      GoTo ERROR_Cls
      Exit Function
   End If
End Function
