VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CGruposMatNivel4"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CGruposMatNivel4 **********************************
'*             Autor : Javier Arana
'*             Creada : 18/11/98
'****************************************************************

Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum

Private mCol As Collection
Private mvarConexion As CConexion
Private mvarEOF As Boolean
Public Property Get eof() As Boolean
    eof = mvarEOF
End Property
Friend Property Let eof(ByVal b As Boolean)
    mvarEOF = b
End Property

''' <summary>Carga los grupos de material</summary>
''' <param name="CaracteresInicialesCod">Caracteres iniciales de c�digo</param>
''' <param name="CaracteresInicialesDen">Caracteres iniciales de la descripci�n</param>
''' <param name="CoincidenciaTotal">Coincidencia total de c�digo y denominaci�n</param>
''' <param name="OrdenadosPorDen">ordenar por denominaci�n</param>
''' <param name="UsarIndice">usar �ndice</param>
''' <param name="sGMN1">GMN1</param>
''' <param name="sGMN2">GMN2</param>
''' <param name="sGMN3">GMN2</param>
''' <param name="oAtributos">Filtro por atributos del GMN4</param>
''' <param name="CodFinalizanPor">Indica si los caracteres pasados son los finales del c�digo</param>
''' <param name="DenFinalizanPor">Indica si los caracteres pasados son los finales de la denominaci�n</param>
''' <param name="bCoincidenciaCod">Indica si los caracteres pasados son los finales de la denominaci�n</param>
''' <param name="bCoincidenciaDen">Indica si debe haber coincidencia total por denominaci�n</param>
''' <remarks>Llamada desde: frmESTRMATBuscar
''' Tiempo m�ximo: 1 sec </remarks>
''' <remarks>Revisado por: LTG  Fecha: 12/08/2011</remarks>

Public Sub CargarTodosLosGruposMat(Optional ByVal CaracteresInicialesCod As String, _
        Optional ByVal CaracteresInicialesDen As String, Optional ByVal CoincidenciaTotal As Boolean, _
        Optional ByVal OrdenadosPorDen As Boolean, Optional ByVal UsarIndice As Boolean, Optional ByVal sGMN1 As String, _
        Optional ByVal sGMN2 As String, Optional ByVal sGMN3 As String, Optional ByVal oAtributos As CAtributos, _
        Optional ByVal CodFinalizanPor As Boolean, Optional ByVal DenFinalizanPor As Boolean, _
        Optional ByVal bCoincidenciaCod As Boolean, Optional ByVal bCoincidenciaDen As Boolean)
    Dim rs As New adodb.Recordset
    Dim sConsulta As String
    Dim lIndice As Long
    Dim fldGmn1 As adodb.Field
    Dim fldGmn2 As adodb.Field
    Dim fldGmn3 As adodb.Field
    Dim fldCod As adodb.Field
    Dim fldDen As adodb.Field
    Dim oAtributo As CAtributo
    
    '********* Precondicion **************************************
    If mvarConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CDestinos.CargarTodosLosDestinos", "No se ha establecido la conexion"
        Exit Sub
    End If
    '*************************************************************
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
    sConsulta = "SELECT GMN1,GMN2,GMN3,COD,FECACT," & DevolverDenGMN & " FROM GMN4 WITH (NOLOCK) "
    sConsulta = sConsulta & "WHERE 1=1"
    
    If CaracteresInicialesCod <> "" Or CaracteresInicialesDen <> "" Then
        If Not (CaracteresInicialesCod = "") And Not (CaracteresInicialesDen = "") Then
            If CoincidenciaTotal Then
                sConsulta = sConsulta & " AND COD ='" & DblQuote(CaracteresInicialesCod) & "'"
                sConsulta = sConsulta & " AND " & DevolverDenGMN & "='" & DblQuote(CaracteresInicialesDen) & "'"
            Else
                If bCoincidenciaCod Then
                    sConsulta = sConsulta & " AND COD ='" & DblQuote(CaracteresInicialesCod) & "'"
                    If bCoincidenciaDen Then
                        sConsulta = sConsulta & " AND " & DevolverDenGMN & "='" & DblQuote(CaracteresInicialesDen) & "'"
                    Else
                        If DenFinalizanPor Then
                            sConsulta = sConsulta & " AND " & DevolverDenGMN & " LIKE '%" & DblQuote(CaracteresInicialesDen) & "'"
                        Else
                            sConsulta = sConsulta & " AND " & DevolverDenGMN & " LIKE '" & DblQuote(CaracteresInicialesDen) & "%'"
                        End If
                    End If
                Else
                    If CodFinalizanPor Then
                        sConsulta = sConsulta & " AND COD LIKE '%" & DblQuote(CaracteresInicialesCod) & "'"
                        If bCoincidenciaDen Then
                            sConsulta = sConsulta & " AND " & DevolverDenGMN & "='" & DblQuote(CaracteresInicialesDen) & "'"
                        Else
                            If DenFinalizanPor Then
                                sConsulta = sConsulta & " AND " & DevolverDenGMN & " LIKE '%" & DblQuote(CaracteresInicialesDen) & "'"
                            Else
                               sConsulta = sConsulta & " AND " & DevolverDenGMN & " LIKE '" & DblQuote(CaracteresInicialesDen) & "%'"
                            End If
                        End If
                    Else
                        sConsulta = sConsulta & " AND COD LIKE '" & DblQuote(CaracteresInicialesCod) & "%'"
                        If bCoincidenciaDen Then
                            sConsulta = sConsulta & " AND " & DevolverDenGMN & "='" & DblQuote(CaracteresInicialesDen) & "'"
                        Else
                            If DenFinalizanPor Then
                                sConsulta = sConsulta & " AND " & DevolverDenGMN & " LIKE '%" & DblQuote(CaracteresInicialesDen) & "'"
                            Else
                                sConsulta = sConsulta & " AND " & DevolverDenGMN & " LIKE '" & DblQuote(CaracteresInicialesDen) & "%'"
                            End If
                        End If
                    End If
                End If
            End If
        Else
            If CaracteresInicialesCod <> "" Then
                If CoincidenciaTotal Then
                    sConsulta = sConsulta & "WHERE COD ='" & DblQuote(CaracteresInicialesCod) & "'"
                Else
                    If bCoincidenciaCod Then
                        sConsulta = sConsulta & " AND COD ='" & DblQuote(CaracteresInicialesCod) & "'"
                    Else
                        If CodFinalizanPor Then
                            sConsulta = sConsulta & " AND COD LIKE '%" & DblQuote(CaracteresInicialesCod) & "'"
                        Else
                            sConsulta = sConsulta & " AND COD LIKE '" & DblQuote(CaracteresInicialesCod) & "%'"
                        End If
                    End If
                End If
            Else
                If CoincidenciaTotal Then
                    sConsulta = sConsulta & "WHERE " & DevolverDenGMN & "='" & DblQuote(CaracteresInicialesDen) & "'"
                Else
                    If bCoincidenciaDen Then
                        sConsulta = sConsulta & " AND " & DevolverDenGMN & "='" & DblQuote(CaracteresInicialesDen) & "'"
                    Else
                        If DenFinalizanPor Then
                            sConsulta = sConsulta & " AND " & DevolverDenGMN & " LIKE '%" & DblQuote(CaracteresInicialesDen) & "'"
                        Else
                            sConsulta = sConsulta & " AND " & DevolverDenGMN & " LIKE '" & DblQuote(CaracteresInicialesDen) & "%'"
                        End If
                    End If
                End If
            End If
        End If
    End If
    
    If sGMN1 <> vbNullString Then
        sConsulta = sConsulta & " AND GMN1='" & DblQuote(sGMN1) & "'"
    End If
    If sGMN2 <> vbNullString Then
        sConsulta = sConsulta & " AND GMN2='" & DblQuote(sGMN2) & "'"
    End If
    If sGMN3 <> vbNullString Then
        sConsulta = sConsulta & " AND GMN3='" & DblQuote(sGMN3) & "'"
    End If
    
    'Filtro por atributos
    If Not oAtributos Is Nothing Then
        If oAtributos.Count > 0 Then
            For Each oAtributo In oAtributos
                sConsulta = sConsulta & " AND EXISTS(SELECT GA.GMN4"
                sConsulta = sConsulta & " FROM GMN4_ATRIB GA WITH (NOLOCK) INNER JOIN "
                sConsulta = sConsulta & " DEF_ATRIB DA WITH (NOLOCK) ON GA.ATRIB=DA.ID "
                sConsulta = sConsulta & " WHERE GA.GMN1=GMN4.GMN1 "
                sConsulta = sConsulta & " AND GA.GMN2=GMN4.GMN2 "
                sConsulta = sConsulta & " AND GA.GMN3=GMN4.GMN3 "
                sConsulta = sConsulta & " AND GA.GMN4=GMN4.COD "
                sConsulta = sConsulta & " AND DA.ID=" & oAtributo.Id
                                
                Select Case oAtributo.Tipo
                    Case TiposDeAtributos.TipoString, TiposDeAtributos.TipoTextoCorto, TiposDeAtributos.TipoTextoLargo, TiposDeAtributos.TipoTextoMedio
                        If Not IsEmpty(oAtributo.ValorText) And Not IsNull(oAtributo.ValorText) Then
                            sConsulta = sConsulta & " AND GA.VALOR_TEXT LIKE '" & DblQuote(oAtributo.ValorText) & "'"
                        End If
                    Case TiposDeAtributos.TipoNumerico
                        If Not IsEmpty(oAtributo.ValorText) And Not IsNull(oAtributo.ValorNum) Then
                            sConsulta = sConsulta & " AND GA.VALOR_NUM" & oAtributo.Formula & oAtributo.ValorNum
                        End If
                    Case TiposDeAtributos.TipoFecha
                        If Not IsEmpty(oAtributo.ValorText) And Not IsNull(oAtributo.ValorFec) Then
                            sConsulta = sConsulta & " AND GA.VALOR_FEC=" & DateToSQLTimeDate(oAtributo.ValorFec)
                        End If
                    Case TiposDeAtributos.TipoBoolean
                        If Not IsEmpty(oAtributo.ValorText) And Not IsNull(oAtributo.ValorBool) Then
                            sConsulta = sConsulta & " AND GA.VALOR_BOOL=" & BooleanToSQLBinary(oAtributo.ValorBool)
                        End If
                End Select
                
                sConsulta = sConsulta & ")"
            Next
        End If
    End If
    
    If OrdenadosPorDen Then
        sConsulta = sConsulta & " ORDER BY " & DevolverDenGMN & ",COD"
    Else
        sConsulta = sConsulta & " ORDER BY COD," & DevolverDenGMN
    End If
          
    rs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    
    Set mCol = Nothing
    Set mCol = New Collection
        
    If rs.eof Then
        mvarEOF = True
    Else
        Set fldGmn1 = rs.Fields("GMN1")
        Set fldGmn2 = rs.Fields("GMN2")
        Set fldGmn3 = rs.Fields("GMN3")
        Set fldCod = rs.Fields("COD")
        Set fldDen = rs.Fields(DevolverDenGMN)
            
        If UsarIndice Then
            lIndice = 0
            
            While Not rs.eof
                ' Dejamos en manos del programador cuando asignar la conexion al obejto usuario
                Me.Add fldGmn1.Value, fldGmn2.Value, fldGmn3.Value, "", "", "", fldCod.Value, fldDen.Value, lIndice
                rs.MoveNext
                lIndice = lIndice + 1
            Wend
        Else
            While Not rs.eof
                ' Dejamos en manos del programador cuando asignar la conexion al obejto usuario
                Me.Add fldGmn1.Value, fldGmn2.Value, fldGmn3.Value, "", "", "", fldCod.Value, fldDen.Value
                rs.MoveNext
            Wend
        End If
                    
        Set fldGmn1 = Nothing
        Set fldGmn2 = Nothing
        Set fldGmn3 = Nothing
        Set fldCod = Nothing
        Set fldDen = Nothing
    End If
    
    rs.Close
    Set rs = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CGruposMatNivel4", "CargarTodosLosGruposMat", ERR, Erl)
      Exit Sub
   End If
End Sub

Public Function Add(ByVal GMN1Cod As String, ByVal GMN2Cod As String, ByVal GMN3Cod As String, ByVal GMN1Den As String, ByVal GMN2Den As String, ByVal GMN3Den As String, ByVal Cod As String, ByVal Den As String, Optional ByVal varIndice As Variant, Optional ByVal tipoRecepcion As Integer) As CGrupoMatNivel4
    'create a new object
    Dim objnewmember As CGrupoMatNivel4
    Dim sCod As String
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New CGrupoMatNivel4
   
    objnewmember.GMN1Cod = GMN1Cod
    objnewmember.GMN2Cod = GMN2Cod
    objnewmember.GMN3Cod = GMN3Cod
    objnewmember.GMN1Den = GMN1Den
    objnewmember.GMN2Den = GMN2Den
    objnewmember.GMN3Den = GMN3Den
    objnewmember.Cod = Cod
    objnewmember.Den = Den
    Set objnewmember.Conexion = mvarConexion
    
    If Not IsMissing(varIndice) And Not IsNull(varIndice) Then
        objnewmember.Indice = varIndice
        mCol.Add objnewmember, CStr(varIndice)
    Else
        sCod = GMN1Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(GMN1Cod))
        sCod = sCod & GMN2Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(GMN2Cod))
        sCod = sCod & GMN3Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(GMN3Cod))
        sCod = sCod & Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN4 - Len(Cod))
        mCol.Add objnewmember, sCod
    End If
    
    Set Add = objnewmember
    Set objnewmember = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CGruposMatNivel4", "Add", ERR, Erl)
      Exit Function
   End If

End Function

Public Property Get Item(vntIndexKey As Variant) As CGrupoMatNivel4
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Friend Property Set Conexion(ByVal con As CConexion)
Set mvarConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = mvarConexion
End Property

Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
     Count = mCol.Count
End If


End Property


Public Sub Remove(vntIndexKey As Variant)
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    mCol.Remove vntIndexKey
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CGruposMatNivel4", "Remove", ERR, Erl)
      Exit Sub
   End If
   
End Sub


Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
     Set NewEnum = mCol.[_NewEnum]
End Property


Private Sub Class_Initialize()
    

    Set mCol = New Collection
End Sub


Private Sub Class_Terminate()
    'destroys collection when this class is terminated
    Set mCol = Nothing
    Set mvarConexion = Nothing
End Sub

Public Function DevolverLosCodigos() As TipoDatosCombo
   
   Dim Codigos As TipoDatosCombo
   Dim iCont As Integer
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
   ReDim Codigos.Cod(mCol.Count)
   ReDim Codigos.Den(mCol.Count)
   
    For iCont = 0 To mCol.Count - 1
        
        Codigos.Cod(iCont) = mCol(iCont + 1).Cod
        Codigos.Den(iCont) = mCol(iCont + 1).Den
    
    Next

   DevolverLosCodigos = Codigos
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CGruposMatNivel4", "DevolverLosCodigos", ERR, Erl)
      Exit Function
   End If
   
End Function
Public Function DevolverLosCodigosDeNivel1() As TipoDatosCombo
   
   Dim Codigos As TipoDatosCombo
   Dim iCont As Integer
   Dim oGMN4 As CGrupoMatNivel4
   Dim oGrups As CGruposMatNivel1
   
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
   Set oGrups = New CGruposMatNivel1
   
   ReDim Codigos.Cod(1)
   ReDim Codigos.Den(1)
   
   iCont = 0
   For Each oGMN4 In Me
   
    If oGrups.Item(oGMN4.GMN1Cod) Is Nothing Then
        Codigos.Cod(iCont) = oGMN4.GMN1Cod
        Codigos.Den(iCont) = oGMN4.GMN1Den
        iCont = iCont + 1
        oGrups.Add oGMN4.GMN1Cod, ""
        ReDim Preserve Codigos.Cod(iCont + 1)
        ReDim Preserve Codigos.Den(iCont + 1)
    End If
    
   Next
   Set oGrups = Nothing
   
   DevolverLosCodigosDeNivel1 = Codigos
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CGruposMatNivel4", "DevolverLosCodigosDeNivel1", ERR, Erl)
      Exit Function
   End If
   
End Function

''' <summary>Devuelve la estructura de materiales</summary>
''' <param name="CodEqpComprador">Cod. equipo comprador</param>
''' <param name="CodComprador">Cod. comprador</param>
''' <param name="bOrdPorCod">orden por c�digo</param>
''' <returns>recordset con los datos</returns>
''' <remarks>Llamada desde: FSGSClient</remarks>
''' <revision>LTG 09/01/2012</revision>

Public Function DevolverEstructuraDeMateriales(Optional ByVal CodEqpComprador As String, Optional ByVal CodComprador As String, Optional ByVal bOrdPorCod As Boolean) As adodb.Recordset
    Dim oadorecordset As adodb.Recordset
    Dim oADOConnection As adodb.Connection
    Dim sql As String
    Dim sOrden As String
    Dim oIdiomas As CIdiomas
    Dim oIdi As CIdioma
    Dim sFrom As String
    Dim oGestorParametros As CGestorParametros
    Dim stm As adodb.Stream
    Dim rs As adodb.Recordset
    Dim sFrom4 As String

    If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
    Set oADOConnection = New adodb.Connection
    oADOConnection.Open "Provider=MSDataShape.1;Persist Security Info=True;Data Source=" & basParametros.ServidorGS & ";User ID=" & LICDBLogin & ";Password=" & LICDBContra & ";Initial Catalog=" & basParametros.BaseDatosGS & ";Data Provider=SQLOLEDB.1"
    oADOConnection.CursorLocation = adUseClient
    oADOConnection.CommandTimeout = 120
    
    'Preparo el orden (c�digo o denominaci�n)
    If bOrdPorCod Then
        sOrden = " ORDER BY COD "
    Else
        If gParametrosGenerales.gbSincronizacionMat Then
            If Trim(CodEqpComprador) <> "" And Trim(CodComprador) <> "" Then
                sOrden = " ORDER BY DEN_" & gParametrosInstalacion.gIdiomaPortal & " "
            Else
                sOrden = " ORDER BY DEN_" & gParametrosInstalacion.gIdiomaPortal
            End If
        Else
            sOrden = " ORDER BY DEN_SPA"
        End If
    End If
    
    If gParametrosGenerales.gbSincronizacionMat Then
        Set oGestorParametros = New CGestorParametros
        Set oGestorParametros.Conexion = mvarConexion
        Set oIdiomas = oGestorParametros.DevolverIdiomas(False, True, False)
        Set oGestorParametros = Nothing
        For Each oIdi In oIdiomas
            sFrom = sFrom & ",DEN_" & oIdi.Cod & " "
            sFrom4 = sFrom4 & ",DEN_" & oIdi.Cod
        Next
        Set oIdiomas = Nothing
        Set oGestorParametros = Nothing
    Else
        sFrom = ",DEN_SPA "
    End If
        
    'Con restricci�n de comprador:
    If Trim(CodEqpComprador) <> "" And Trim(CodComprador) <> "" Then
        sql = "SHAPE {SELECT G1.COD " & sFrom & ",MAX(ASIG) ASIG,TIPORECEPCION,CANTIDAD_MODIFICABLE FROM (SELECT 1 ASIG, GMN1 FROM COM_GMN1 WITH (NOLOCK) WHERE EQP = '" & DblQuote(CodEqpComprador) & "' AND COM='" & DblQuote(CodComprador) & "'" & _
            " UNION SELECT DISTINCT 0 ASIG, GMN1 FROM COM_GMN2 WITH (NOLOCK) WHERE EQP = '" & DblQuote(CodEqpComprador) & "' AND COM='" & DblQuote(CodComprador) & "'" & _
            " UNION SELECT DISTINCT 0 ASIG, GMN1 FROM COM_GMN3 WITH (NOLOCK) WHERE EQP = '" & DblQuote(CodEqpComprador) & "' AND COM='" & DblQuote(CodComprador) & "'" & _
            " UNION SELECT DISTINCT 0 ASIG, GMN1 FROM COM_GMN4 WITH (NOLOCK) WHERE EQP = '" & DblQuote(CodEqpComprador) & "' AND COM='" & DblQuote(CodComprador) & "'" & _
            " ) CG INNER JOIN GMN1 G1 WITH (NOLOCK) ON CG.GMN1=G1.COD GROUP BY G1.COD " & sFrom & ",TIPORECEPCION,CANTIDAD_MODIFICABLE " & sOrden & "}  AS GMN1 "

        sql = sql & " APPEND (( SHAPE {SELECT G2.COD " & sFrom & " ,MAX(ASIG) ASIG, G2.GMN1,TIPORECEPCION,CANTIDAD_MODIFICABLE FROM (SELECT 1 ASIG, GMN1, GMN2 FROM COM_GMN2 WITH (NOLOCK) WHERE EQP = '" & DblQuote(CodEqpComprador) & "' AND COM='" & DblQuote(CodComprador) & "'" & _
             " UNION SELECT DISTINCT 0 ASIG, GMN1, GMN2 FROM COM_GMN3 WITH (NOLOCK) WHERE EQP = '" & DblQuote(CodEqpComprador) & "' AND COM='" & DblQuote(CodComprador) & "'" & _
             " UNION SELECT DISTINCT 0 ASIG, GMN1, GMN2 FROM COM_GMN4 WITH (NOLOCK) WHERE EQP = '" & DblQuote(CodEqpComprador) & "' AND COM='" & DblQuote(CodComprador) & "'" & _
             ") CG INNER JOIN GMN2 G2 WITH (NOLOCK) ON CG.GMN1 = G2.GMN1 AND CG.GMN2 = G2.COD GROUP BY G2.GMN1 , G2.COD " & sFrom & ",TIPORECEPCION,CANTIDAD_MODIFICABLE " & sOrden & "}  AS GMN2 "
        
        sql = sql & " APPEND (( SHAPE {SELECT G3.COD " & sFrom & ",MAX(ASIG) ASIG, G3.GMN1,G3.GMN2,TIPORECEPCION,CANTIDAD_MODIFICABLE FROM (SELECT DISTINCT 1 ASIG, GMN1, GMN2, GMN3 FROM COM_GMN3 WITH (NOLOCK) WHERE EQP = '" & DblQuote(CodEqpComprador) & "' AND COM='" & DblQuote(CodComprador) & "'" & _
            " UNION SELECT DISTINCT 0 ASIG, GMN1, GMN2, GMN3 FROM COM_GMN4 WITH (NOLOCK) WHERE EQP = '" & DblQuote(CodEqpComprador) & "' AND COM='" & DblQuote(CodComprador) & "'" & _
            ") GM INNER JOIN GMN3 G3 WITH (NOLOCK) ON GM.GMN1 = G3.GMN1 AND GM.GMN2 = G3.GMN2 AND GM.GMN3 = G3.COD " & _
            " GROUP BY G3.GMN1,G3.GMN2,G3.COD" & sFrom & ",TIPORECEPCION,CANTIDAD_MODIFICABLE " & sOrden & "} AS GMN3 "

        If gParametrosGenerales.gbSincronizacionMat = True Then
            sql = sql & " APPEND ({SELECT DISTINCT GMN4.GMN1,GMN4.GMN2,GMN4.GMN3,GMN4.COD" & sFrom4 & ",TIPORECEPCION,CANTIDAD_MODIFICABLE, 1 AS ASIG FROM GMN4 WITH (NOLOCK) INNER JOIN COM_GMN4 WITH (NOLOCK) ON GMN4.COD=COM_GMN4.GMN4 AND GMN4.GMN3=COM_GMN4.GMN3 AND GMN4.GMN2=COM_GMN4.GMN2 AND GMN4.GMN1=COM_GMN4.GMN1 AND COM_GMN4.EQP='" & DblQuote(CodEqpComprador) & "' AND COM_GMN4.COM='" & DblQuote(CodComprador) & "' " & sOrden & "} AS GMN4 "
        Else
            sql = sql & " APPEND ({SELECT DISTINCT GMN4.GMN1,GMN4.GMN2,GMN4.GMN3,GMN4.COD,GMN4.DEN_SPA,TIPORECEPCION,CANTIDAD_MODIFICABLE, 1 AS ASIG FROM GMN4 WITH (NOLOCK) INNER JOIN COM_GMN4 WITH (NOLOCK) ON GMN4.COD=COM_GMN4.GMN4 AND GMN4.GMN3=COM_GMN4.GMN3 AND GMN4.GMN2=COM_GMN4.GMN2 AND GMN4.GMN1=COM_GMN4.GMN1 AND COM_GMN4.EQP='" & DblQuote(CodEqpComprador) & "' AND COM_GMN4.COM='" & DblQuote(CodComprador) & "' " & sOrden & "} AS GMN4 "
        End If
        
    Else  'Sin restricci�n de comprador
        If gParametrosGenerales.gbSincronizacionMat = True Then
            sql = "SHAPE {SELECT COD" & sFrom4 & ",TIPORECEPCION,CANTIDAD_MODIFICABLE FROM GMN1 WITH (NOLOCK) " & sOrden & "}  AS GMN1 APPEND (( SHAPE {SELECT GMN1,COD" & sFrom4 & ",TIPORECEPCION,CANTIDAD_MODIFICABLE FROM GMN2 WITH (NOLOCK) " & sOrden & "}  AS GMN2 APPEND (( SHAPE {SELECT GMN1,GMN2,COD " & sFrom4 & ",TIPORECEPCION,CANTIDAD_MODIFICABLE FROM GMN3 WITH (NOLOCK) " & sOrden & "}  AS GMN3 APPEND ({SELECT GMN1,GMN2,GMN3,COD " & sFrom4 & ",TIPORECEPCION,CANTIDAD_MODIFICABLE FROM GMN4 WITH (NOLOCK) " & sOrden & "}  AS GMN4 "
        Else
            sql = "SHAPE {SELECT COD,DEN_SPA,TIPORECEPCION,CANTIDAD_MODIFICABLE FROM GMN1 WITH (NOLOCK) " & sOrden & "}  AS GMN1 APPEND (( SHAPE {SELECT GMN1,COD,DEN_SPA,TIPORECEPCION,CANTIDAD_MODIFICABLE FROM GMN2 WITH (NOLOCK) " & sOrden & "}  AS GMN2 APPEND (( SHAPE {SELECT GMN1,GMN2,COD,DEN_SPA,TIPORECEPCION,CANTIDAD_MODIFICABLE FROM GMN3 WITH (NOLOCK) " & sOrden & "}  AS GMN3 APPEND ({SELECT GMN1,GMN2,GMN3,COD,DEN_SPA, TIPORECEPCION,CANTIDAD_MODIFICABLE FROM GMN4 WITH (NOLOCK) " & sOrden & "}  AS GMN4 "
        End If
    End If
    
    sql = sql & " RELATE 'GMN1' TO 'GMN1','GMN2' TO 'GMN2','COD' TO 'GMN3') AS GMN4) AS GMN3 RELATE 'GMN1' TO 'GMN1','COD' TO 'GMN2') AS GMN3) AS GMN2 RELATE 'COD' TO 'GMN1') AS GMN2"
    
    Set oadorecordset = New adodb.Recordset
    oadorecordset.CursorLocation = adUseClient
    oadorecordset.Open sql, oADOConnection, adOpenForwardOnly, adLockBatchOptimistic
    oadorecordset.ActiveConnection = Nothing
    oADOConnection.Close
        
    'Paso el recordset a stream y lo vuelvo a abrir desde ah�.Se hace as� porque sino no se desconecta:
    Set stm = New adodb.Stream
    oadorecordset.Save stm, adPersistXML
    Set rs = New adodb.Recordset
    rs.Open stm
    rs.ActiveConnection = Nothing
    
    Set DevolverEstructuraDeMateriales = rs
    
    stm.Close
    Set stm = Nothing

    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CGruposMatNivel4", "DevolverEstructuraDeMateriales", ERR, Erl)
        Exit Function
    End If
End Function

''' <summary>Carga las denominaciones de los materiales existentes en la colecci�n</summary>
''' <param name="sIdi">Idioma de la denominaci�n</param>
''' <remarks>Llamada desde: frmEST</remarks>
''' <revision>LTG 19/04/2013</revision>

Public Sub CargarDenGMN4s(ByVal sIdi As String)
    Dim sSQL As String
    Dim adoRs As adodb.Recordset
    Dim oCom As adodb.Command
    Dim oParam As adodb.Parameter
    Dim oGMN4 As CGrupoMatNivel4
    Dim sCod As String
        
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sSQL = " SELECT GMN1,GMN2,GMN3,COD,DEN_" & sIdi & " FROM GMN4 WITH (NOLOCK) WHERE "
    For Each oGMN4 In Me
        sSQL = sSQL & "(GMN1=? AND GMN2=? AND GMN3=? AND COD=?) OR "
    Next
    Set oGMN4 = Nothing
    sSQL = Left(sSQL, Len(sSQL) - 4)
    
    Set oCom = New adodb.Command
    With oCom
        Set .ActiveConnection = mvarConexion.ADOCon
    
        For Each oGMN4 In Me
            Set oParam = .CreateParameter(, adVarChar, adParamInput, 50, oGMN4.GMN1Cod)
            .Parameters.Append oParam
            Set oParam = .CreateParameter(, adVarChar, adParamInput, 50, oGMN4.GMN2Cod)
            .Parameters.Append oParam
            Set oParam = .CreateParameter(, adVarChar, adParamInput, 50, oGMN4.GMN3Cod)
            .Parameters.Append oParam
            Set oParam = .CreateParameter(, adVarChar, adParamInput, 50, oGMN4.Cod)
            .Parameters.Append oParam
        Next
        Set oGMN4 = Nothing
    
        .CommandType = adCmdText
        .CommandText = sSQL
        .Prepared = True
        Set adoRs = .Execute
    End With
    Set oCom = Nothing
    Set oParam = Nothing
    
    If Not adoRs.eof Then
        adoRs.MoveFirst
        While Not adoRs.eof
            sCod = adoRs("GMN1") & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(adoRs("GMN1")))
            sCod = sCod & adoRs("GMN2") & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(adoRs("GMN2")))
            sCod = sCod & adoRs("GMN3") & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(adoRs("GMN3")))
            sCod = sCod & adoRs("COD") & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN4 - Len(adoRs("COD")))
            
            If Not Me.Item(sCod) Is Nothing Then Me.Item(sCod).Den = adoRs("DEN_" & sIdi)
            
            adoRs.MoveNext
        Wend
    End If
    
    Set adoRs = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CGruposMatNivel4", "CargarDenGMN4s", ERR, Erl)
      Exit Sub
   End If
End Sub

''' <summary>Devuelve una cadena de caracteres con los c�digos de los materiales de la colecci�n</summary>
''' <remarks>Llamada desde: CConfVistaVisor.AnyadirABaseDatos</remarks>
''' <revision>LTG 19/04/2013</revision>

Public Function GMNCodToStr() As String
    Dim oGMN4 As CGrupoMatNivel4
    Dim sGMN As String
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    For Each oGMN4 In Me
        sGMN = sGMN & oGMN4.GMNCodToStr
    Next
    Set oGMN4 = Nothing
    
    GMNCodToStr = Left(sGMN, Len(sGMN) - 1)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CGruposMatNivel4", "GMNCodToStr", ERR, Erl)
      Exit Function
   End If
End Function
