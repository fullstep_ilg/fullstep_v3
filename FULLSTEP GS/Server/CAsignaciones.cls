VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CAsignaciones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CAsiganciones **********************************
'*             Autor : Javier Arana
'*             Creada : 14/3/99
'****************************************************************


Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum
Private mCol As Collection
Private mvarConexion As CConexion

Public Function Add(ByVal CodProve As String, ByVal DenProve As String, ByVal Asignado As Boolean, Optional ByVal CodEqp As Variant, _
                    Optional ByVal CodComp As Variant, Optional ByVal Cal1 As Variant, Optional ByVal Cal2 As Variant, _
                    Optional ByVal Cal3 As Variant, Optional ByVal Val1 As Variant, Optional ByVal Val2 As Variant, _
                    Optional ByVal Val3 As Variant, Optional ByVal varIndice As Variant, Optional ByVal CodPortal As Variant, _
                    Optional ByVal Premium As Variant, Optional ByVal PremiumActivo As Variant, Optional ByVal AntesDeCierreParcial As Variant, _
                    Optional ByVal bProve As Boolean, Optional ByVal bHayAdjMatP As Variant, Optional ByVal bHayAdjMatNoP As Variant, _
                    Optional ByVal Calidad As String) As CAsignacion

    'create a new object
    Dim objnewmember As CAsignacion
    Dim sCod As String
    
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New CAsignacion
   
    objnewmember.CodProve = CodProve
    objnewmember.DenProve = DenProve
    objnewmember.Asignado = Asignado
    
    Set objnewmember.Conexion = mvarConexion
    
    If IsMissing(CodEqp) Then
        objnewmember.CodEqp = Null
    Else
        objnewmember.CodEqp = CodEqp
    End If
    
    If IsMissing(CodComp) Then
        objnewmember.CodComp = Null
    Else
        objnewmember.CodComp = CodComp
    End If
    
    If IsMissing(CodPortal) Then
        objnewmember.CodPortal = Null
    Else
        objnewmember.CodPortal = CodPortal
    End If
    
    If IsMissing(Premium) Then
        objnewmember.Premium = False
    Else
        objnewmember.Premium = SQLBinaryToBoolean(Premium)
    End If
    
    If IsMissing(PremiumActivo) Then
        objnewmember.PremActivo = False
    Else
        If objnewmember.Premium Then
            objnewmember.PremActivo = SQLBinaryToBoolean(PremiumActivo)
        Else
            objnewmember.PremActivo = False
        End If
    End If
    
    If IsMissing(Cal1) Then
        objnewmember.Calif1 = Null
    Else
        objnewmember.Calif1 = Cal1
    End If
    If IsMissing(Val1) Then
        objnewmember.Val1 = Null
    Else
        objnewmember.Val1 = Val1
    End If
    If IsMissing(Cal2) Then
        objnewmember.Calif2 = Null
    Else
        objnewmember.Calif2 = Cal2
    End If
    If IsMissing(Val2) Then
        objnewmember.Val2 = Null
    Else
        objnewmember.Val2 = Val2
    End If
    
    If IsMissing(Cal3) Then
        objnewmember.Calif3 = Null
    Else
        objnewmember.Calif3 = Cal3
    End If
    
    If IsMissing(Val3) Then
        objnewmember.Val3 = Null
    Else
        objnewmember.Val3 = Val3
    End If
    
    If IsMissing(bHayAdjMatP) Then
        objnewmember.HayAdjMaterialesProce = Null
    Else
        objnewmember.HayAdjMaterialesProce = bHayAdjMatP
    End If
    
    If IsMissing(bHayAdjMatNoP) Then
        objnewmember.HayAdjMaterialesNoProce = Null
    Else
        objnewmember.HayAdjMaterialesNoProce = bHayAdjMatNoP
    End If
    
    If IsMissing(AntesDeCierreParcial) Then
        objnewmember.AntesDeCierreParcial = False
    Else
        objnewmember.AntesDeCierreParcial = SQLBinaryToBoolean(AntesDeCierreParcial)
    End If
    
    If IsMissing(Calidad) Then
        objnewmember.Calidad = ""
    Else
        objnewmember.Calidad = Calidad
    End If
    
    If Not IsMissing(varIndice) And Not IsNull(varIndice) Then
        objnewmember.Indice = varIndice
        mCol.Add objnewmember, CStr(varIndice)
    ElseIf bProve Then
        sCod = CodProve & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(CodProve))
        mCol.Add objnewmember, sCod
    Else
    'set the properties passed into the method
        sCod = CodProve & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(CodProve))
        sCod = sCod & NullToStr(CodEqp) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodEQP - Len(NullToStr(CodEqp)))
        sCod = sCod & NullToStr(CodComp) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCOM - Len(NullToStr(CodComp)))
        mCol.Add objnewmember, sCod
    End If
    
    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CAsignaciones", "Add", ERR, Erl)
      Exit Function
   End If

End Function

Public Property Get Item(vntIndexKey As Variant) As CAsignacion
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Friend Property Set Conexion(ByVal con As CConexion)
Set mvarConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = mvarConexion
End Property


Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
     Count = mCol.Count
End If


End Property


Public Sub Remove(vntIndexKey As Variant)


If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    mCol.Remove vntIndexKey
    Exit Sub


'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CAsignaciones", "Remove", ERR, Erl)
      Exit Sub
   End If
    
End Sub


Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
     Set NewEnum = mCol.[_NewEnum]
End Property


Private Sub Class_Initialize()

   
    Set mCol = New Collection
End Sub


Private Sub Class_Terminate()
    'destroys collection when this class is terminated
    Set mCol = Nothing
    Set mvarConexion = Nothing
End Sub
Public Function DevolverLosCodigos() As TipoDatosCombo
   
   Dim Codigos As TipoDatosCombo
   Dim iCont As Integer
   
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
   ReDim Codigos.Cod(mCol.Count)
   ReDim Codigos.Den(mCol.Count)
   
    For iCont = 0 To mCol.Count - 1
        
        Codigos.Cod(iCont) = mCol(iCont + 1).CodProve
        Codigos.Den(iCont) = mCol(iCont + 1).DenProve
    
    Next

   DevolverLosCodigos = Codigos
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CAsignaciones", "DevolverLosCodigos", ERR, Erl)
      Exit Function
   End If
   
End Function

