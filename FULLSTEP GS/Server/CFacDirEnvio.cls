VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CFacDirEnvio"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Const m_lLongDir = 255
Private Const m_lLongCP = 20
Private Const m_lLongPob = 100
Private Const m_lLongPaiCod = 50
Private Const m_lLongProviCod = 50

Private m_oConexion As CConexion
Private m_lIndice As Long

Private m_lID As Long
Private m_sDireccion As String
Private m_sCP As String
Private m_sPoblacion As String
Private m_sPaiCod As String
Private m_sPaiDen As String
Private m_sProviCod As String
Private m_sProviDen As String
Private m_vFecAct  As Variant

Public Property Get Direccion() As String
    Direccion = m_sDireccion
End Property
Public Property Let Direccion(ByVal Value As String)
    m_sDireccion = Value
End Property
Public Property Get CP() As String
    CP = m_sCP
End Property
Public Property Let CP(ByVal Value As String)
    m_sCP = Value
End Property
Public Property Get Poblacion() As String
    Poblacion = m_sPoblacion
End Property
Public Property Let Poblacion(ByVal Value As String)
    m_sPoblacion = Value
End Property
Public Property Get PaiCod() As String
    PaiCod = m_sPaiCod
End Property
Public Property Let PaiCod(ByVal Value As String)
    m_sPaiCod = Value
End Property
Public Property Get PaiDen() As String
    PaiDen = m_sPaiDen
End Property
Public Property Let PaiDen(ByVal Value As String)
    m_sPaiDen = Value
End Property
Public Property Get ProviCod() As String
    ProviCod = m_sProviCod
End Property
Public Property Let ProviCod(ByVal Value As String)
    m_sProviCod = Value
End Property
Public Property Get ProviDen() As String
    ProviDen = m_sProviDen
End Property
Public Property Let ProviDen(ByVal Value As String)
    m_sProviDen = Value
End Property
Public Property Get FecAct() As Variant
    FecAct = m_vFecAct
End Property
Public Property Let FecAct(ByVal Value As Variant)
    m_vFecAct = Value
End Property
''' <summary>Acciones a ejecutar al liberar un objeto de esta clase</summary>
Private Sub Class_Terminate()
    On Error Resume Next
    
    Set m_oConexion = Nothing
End Sub

Friend Property Set Conexion(ByVal oCon As CConexion)
    Set m_oConexion = oCon
End Property

Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property

Public Property Get Indice() As Long
    Indice = m_lIndice
End Property

Public Property Let Indice(ByVal lInd As Long)
    m_lIndice = lInd
End Property

Public Property Get ID() As Long
    ID = m_lID
End Property
Public Property Let ID(ByVal Value As Long)
    m_lID = Value
End Property

''' <summary>
''' A�ade en FAC_DIR_ENVIO un nuevo registro.
''' Esta funci�n necesita que se abra una transacci�n en la funci�n llamante
''' </summary>
''' <remarks>Llamada desde CUnidadORGNivel1.AnyadirABaseDatos</remarks>
''' <revision>LTG 04/01/2012<revision>

Public Sub AnyadirABaseDatos()
    Dim sConsulta As String
    Dim adoComm As adodb.Command
    Dim adoParam As adodb.Parameter
    Dim adoRs As adodb.Recordset
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sConsulta = "INSERT FAC_DIR_ENVIO (DIR, CP, POB, PAI, PROVI) VALUES (?,?,?,?,?)"
    
    Set adoComm = New adodb.Command
    Set adoComm.ActiveConnection = m_oConexion.ADOCon
    With adoComm
        .CommandText = sConsulta
        .CommandType = adCmdText
        .Prepared = True
        
        Set adoParam = .CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=m_lLongDir, Value:=m_sDireccion)
        .Parameters.Append adoParam
        Set adoParam = .CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=m_lLongCP, Value:=StrToVbNULL(m_sCP))
        .Parameters.Append adoParam
        Set adoParam = .CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=m_lLongPob, Value:=StrToVbNULL(m_sPoblacion))
        .Parameters.Append adoParam
        Set adoParam = .CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=m_lLongPaiCod, Value:=StrToVbNULL(m_sPaiCod))
        .Parameters.Append adoParam
        Set adoParam = .CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=m_lLongProviCod, Value:=StrToVbNULL(m_sProviCod))
        .Parameters.Append adoParam
        
        .Execute
    End With
    Set adoParam = Nothing
    Set adoComm = Nothing
    
    Set adoRs = New adodb.Recordset
    sConsulta = "SELECT MAX(ID) ID FROM FAC_DIR_ENVIO WITH (NOLOCK)"
    adoRs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly
    m_lID = adoRs.Fields("ID").Value
    adoRs.Close
    Set adoRs = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If Err.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CFacDirEnvio", "AnyadirABaseDatos", Err, Erl)
      Exit Sub
   End If
End Sub

''' <summary>
''' Modifica un registro en FAC_DIR_ENVIO.
''' Esta funci�n necesita que se abra una transacci�n en la funci�n llamante
''' </summary>
''' <remarks>Llamada desde CUnidadORGNivel1.AnyadirABaseDatos</remarks>
''' <revision>LTG 04/01/2012<revision>

Public Function FinalizarEdicionModificando() As TipoErrorSummit
    Dim sConsulta As String
    Dim adoComm As adodb.Command
    Dim adoParam As adodb.Parameter
    Dim adoRs As Recordset
    
    'Comprobar que el registro que se va a modificar no ha sufrido cambios previamente
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sConsulta = "SELECT DIR,CP,POB,PROVI,PAI,FECACT FROM FAC_DIR_ENVIO WITH (NOLOCK) WHERE ID=?"

    Set adoComm = New adodb.Command
    With adoComm
        Set .ActiveConnection = m_oConexion.ADOCon
        .CommandText = sConsulta
        .CommandType = adCmdText
        .Prepared = True
        
        Set adoParam = .CreateParameter(Type:=adInteger, Direction:=adParamInput, Value:=m_lID)
        .Parameters.Append adoParam
        
        Set adoRs = .Execute
    End With
    
    If adoRs.eof Then
        adoRs.Close
        Set adoRs = Nothing
        FinalizarEdicionModificando.NumError = TESDatoEliminado
        FinalizarEdicionModificando.Arg1 = 71
        Exit Function
    End If
    
    If Not IsNull(m_vFecAct) Then
        If DateDiff("s", m_vFecAct, adoRs("FECACT").Value) > 0 Then
            adoRs.Close
            Set adoRs = Nothing
            FinalizarEdicionModificando.NumError = TESInfActualModificada
            Exit Function
        End If
    End If
    
    'Comprobar si se ha modificado algo
    If adoRs("DIR").Value <> m_sDireccion Or _
       NullToStr(adoRs("CP").Value) <> m_sCP Or _
       NullToStr(adoRs("POB").Value) <> m_sPoblacion Or _
       NullToStr(adoRs("PAI").Value) <> m_sPaiCod Or _
       NullToStr(adoRs("PROVI").Value) <> m_sProviCod Then
        'Realizar la actualizaci�n
        sConsulta = "UPDATE FAC_DIR_ENVIO SET DIR = ?, CP=?, POB=?, PAI=?, PROVI=? WHERE ID = ?"
        
        Set adoComm = New adodb.Command
        With adoComm
            Set .ActiveConnection = m_oConexion.ADOCon
            .CommandText = sConsulta
            .CommandType = adCmdText
            .Prepared = True
            
            Set adoParam = .CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=m_lLongDir, Value:=m_sDireccion)
            adoComm.Parameters.Append adoParam
            Set adoParam = adoComm.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=m_lLongCP, Value:=StrToVbNULL(m_sCP))
            adoComm.Parameters.Append adoParam
            Set adoParam = adoComm.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=m_lLongPob, Value:=StrToVbNULL(m_sPoblacion))
            adoComm.Parameters.Append adoParam
            Set adoParam = adoComm.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=m_lLongPaiCod, Value:=StrToVbNULL(m_sPaiCod))
            adoComm.Parameters.Append adoParam
            Set adoParam = adoComm.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=m_lLongProviCod, Value:=StrToVbNULL(m_sProviCod))
            adoComm.Parameters.Append adoParam
            Set adoParam = adoComm.CreateParameter(Type:=adInteger, Direction:=adParamInput, Value:=m_lID)
            adoComm.Parameters.Append adoParam
            
            .Execute
        End With
        Set adoParam = Nothing
        Set adoComm = Nothing
    End If
    
    FinalizarEdicionModificando.NumError = TESnoerror
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If Err.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CFacDirEnvio", "FinalizarEdicionModificando", Err, Erl)
      Exit Function
   End If
End Function

''' <summary>
''' Elimina un registro en FAC_DIR_ENVIO.
''' Esta funci�n necesita que se abra una transacci�n en la funci�n llamante
''' </summary>
''' <remarks>Llamada desde CUnidadORGNivel1.AnyadirABaseDatos</remarks>

Public Sub EliminarDeBaseDatos()
    Dim sConsulta As String
    Dim adoComm As adodb.Command
    Dim adoParam As Parameter
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sConsulta = "DELETE FROM FAC_DIR_ENVIO WHERE ID=?"
    
    Set adoComm = New adodb.Command
    With adoComm
        Set .ActiveConnection = m_oConexion.ADOCon
        .CommandText = sConsulta
        .CommandType = adCmdText
        .Prepared = True
        
        Set adoParam = .CreateParameter(Type:=adInteger, Direction:=adParamInput, Value:=m_lID)
        .Parameters.Append adoParam
        
        .Execute
    End With
    
    Set adoParam = Nothing
    Set adoComm = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If Err.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CFacDirEnvio", "EliminarDeBaseDatos", Err, Erl)
      Exit Sub
   End If
End Sub
