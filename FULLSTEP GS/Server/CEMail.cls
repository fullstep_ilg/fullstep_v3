VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CEMail"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

Private m_lID As Long
Private m_sPara As String       'A quien va el correo
Private m_sSubject As String    'Subject del mail
Private m_sCuerpo As String     'Cuerpo del mail
Private m_iNumAdjun As Integer
Private m_oAdjuntos As CEspecificaciones

Private m_oConexion As CConexion

Public Property Get Id() As Long
    Id = m_lID
End Property
Public Property Let Id(ByVal lId As Long)
    m_lID = lId
End Property

Public Property Get Para() As String
    Para = m_sPara
End Property
Public Property Let Para(ByVal dato As String)
    m_sPara = dato
End Property

Public Property Get Subject() As String
    Subject = m_sSubject
End Property
Public Property Let Subject(ByVal dato As String)
    m_sSubject = dato
End Property

Public Property Get Cuerpo() As String
    Cuerpo = m_sCuerpo
End Property
Public Property Let Cuerpo(ByVal dato As String)
    m_sCuerpo = dato
End Property
Public Property Get NumAdjun() As Integer
    NumAdjun = m_iNumAdjun
End Property
Public Property Let NumAdjun(ByVal dato As Integer)
    m_iNumAdjun = dato
End Property

Public Property Get Adjuntos() As CEspecificaciones
    Set Adjuntos = m_oAdjuntos
End Property
Public Property Set Adjuntos(ByVal dato As CEspecificaciones)
    Set m_oAdjuntos = dato
End Property

Friend Property Set Conexion(ByVal vData As CConexion)
    Set m_oConexion = vData
End Property
Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property

Private Sub Class_Terminate()
    Set m_oConexion = Nothing
End Sub
''' <summary>
''' Carga los datos para la composici�n del mail.
''' </summary>
''' <param name="iIdRegistroEMail">El id del registro de la tabla REGISTRO_EMAIL</param>
''' <returns>Devuelve el n�mero de ficheros adjuntos que tiene.</returns>
''' <remarks>Tiempo m�ximo: 0 seg </remarks>

''' <summary>
''' Carga los datos para la composici�n del mail.
''' </summary>
''' <param name></param>
''' <returns>Devuelve false si no existe ese registro en REGISTRO_MAIL.</returns>
''' <remarks>Tiempo m�ximo: 0 seg </remarks>

Public Function CargarDatosMail() As Boolean

Dim AdoRes As New adodb.Recordset
Dim sConsulta As String

    
    'Obtenemos el n�mero de adjuntos que tiene ese email.
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sConsulta = "SELECT COUNT(ID) AS NUMADJUNTO FROM REGISTRO_EMAIL_ADJUN WITH (NOLOCK) WHERE REG_ID = " & m_lID
    AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenKeyset, adLockReadOnly
    If Not AdoRes.eof Then
        m_iNumAdjun = NullToDbl0(AdoRes.Fields("NUMADJUNTO").Value)
    End If
    AdoRes.Close
    
    'Obtenemos los datos para el mail.
    sConsulta = "SELECT RE.ID,RE.PARA,RE.SUBJECT,RE.CUERPO,RA.REG_ID AS NUMADJUNTO "
    sConsulta = sConsulta & " FROM REGISTRO_EMAIL RE WITH (NOLOCK) "
    sConsulta = sConsulta & " LEFT JOIN REGISTRO_EMAIL_ADJUN RA WITH (NOLOCK) ON RA.REG_ID=RE.ID"
    sConsulta = sConsulta & " WHERE RE.ID = " & m_lID
    AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenKeyset, adLockReadOnly
          
    If Not AdoRes.eof Then
        m_sPara = NullToStr(AdoRes.Fields("PARA").Value)
        m_sSubject = NullToStr(AdoRes.Fields("SUBJECT").Value)
        m_sCuerpo = NullToStr(AdoRes.Fields("CUERPO").Value)
        CargarDatosMail = True
    Else
    'No existe ese registro en REGISTRO_MAIL, se puede deber al proceso de limpieza.
        CargarDatosMail = False
    End If
    
    AdoRes.Close
    Set AdoRes = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CEMail", "CargarDatosMail", ERR, Erl)
      Exit Function
   End If
    
End Function


''' <summary>
''' Carga los datos para la composici�n del mail.
''' </summary>
''' <param name="iIdRegistroEMail">El id del registro de la tabla REGISTRO_EMAIL</param>
''' <returns>Devuelve el n�mero de ficheros adjuntos que tiene.</returns>
''' <remarks>Tiempo m�ximo: 0 seg </remarks>

Public Sub DevolverAdjuntosMail()
Dim AdoRes As New adodb.Recordset
Dim sConsulta As String


    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sConsulta = "SELECT RA.ID,RA.NOM,RA.FECACT,DATALENGTH(DATA) AS SIZE FROM REGISTRO_EMAIL_ADJUN RA WITH (NOLOCK) "
    sConsulta = sConsulta & " WHERE RA.REG_ID=" & m_lID
    AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenKeyset, adLockReadOnly
          
    If AdoRes.eof Then
        Set m_oAdjuntos = Nothing
        Set m_oAdjuntos = New CEspecificaciones
        Set m_oAdjuntos.Conexion = m_oConexion
    Else
        Set m_oAdjuntos = Nothing
        Set m_oAdjuntos = New CEspecificaciones
        Set m_oAdjuntos.Conexion = m_oConexion
        While Not AdoRes.eof
            m_oAdjuntos.Add AdoRes.Fields("NOM").Value, AdoRes.Fields("FECACT").Value, AdoRes.Fields("ID").Value, , , , , , , , , , AdoRes.Fields("SIZE").Value, , m_lID
            AdoRes.MoveNext
        Wend
        
    End If
    
    AdoRes.Close
    Set AdoRes = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CEMail", "DevolverAdjuntosMail", ERR, Erl)
      Exit Sub
   End If
    
End Sub






