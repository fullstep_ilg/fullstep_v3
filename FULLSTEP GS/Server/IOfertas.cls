VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "IOfertas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Public Function DevolverOfertasDelProveedor(ByVal Prove As String, Optional ByVal UsarIndice As Boolean) As COfertas

End Function

Public Function DevolverOfertasWeb(Optional ByVal OrdPorDenProve As Boolean = False, Optional ByVal OrdPorNumOfe As Boolean = False, Optional ByVal OrdPorFecRec As Boolean = False, Optional ByVal OrdPorFecVal As Boolean = False, Optional ByVal OrdPorMon As Boolean = False, Optional ByVal UsarIndice As Boolean, Optional ByVal CodProve As Variant) As COfertas

End Function

Public Function DevolverOfertasDelProceso(Optional ByVal Prove As String, Optional ByVal sGrupo As String, Optional ByVal Item As Integer, Optional ByVal SoloAbiertos As Boolean, Optional ByVal lIdEscalado As Long) As ADODB.Recordset

End Function

Public Function DevolverNumOfertasDelProceso(Optional ByVal Item As Integer, Optional ByVal Prove As String) As Integer

End Function

Public Function DevolverOfertasDelProcesoSinPrecio(Optional ByVal Prove As String, Optional ByVal sGrupo As String, Optional ByVal Item As Integer, Optional ByVal SoloAbiertos As Boolean, Optional ByVal lIdEscalado As Long) As Boolean

End Function

Public Sub DevolverUltimasOfertasDelProceso(Optional ByVal Idi As Variant)

End Sub
Public Function CargarOfertasDelProceso(Optional ByVal sProve As String = "", Optional ByVal iNumOfe As Integer = 0) As COfertas

End Function


Public Function DevolverOfertasDelProcesoEnReunion(ByVal dFecReu As Date, Optional ByVal Idi As Variant)


End Function
