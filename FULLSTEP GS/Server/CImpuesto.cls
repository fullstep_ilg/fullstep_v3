VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CImpuesto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum
    
Implements IBaseDatos

Private m_oConexion As CConexion

Private m_vIDImpuesto As Variant
Private m_vIDMat As Variant
Private m_vIDValor As Variant
Private m_vCod As Variant

Private m_vDescr As Variant
Private m_oDen As CMultiidiomas

Private m_vGMN1 As Variant
Private m_vGMN2 As Variant
Private m_vGMN3 As Variant
Private m_vGMN4 As Variant
Private m_vConcepto As Variant

Private m_vArt As Variant
Private m_vValor As Variant
Private m_vVigencia As Variant
Private m_vRetenido As Variant
Private m_vTipo As Variant
Private m_vAtribID As Variant
Private m_vNivelCategoria As Variant

Private m_vImpuestoPaisProvincia As Variant

Private m_vCodPais As Variant
Private m_vCodProvincia As Variant
Private m_vDenPais As Variant
Private m_vDenProvincia As Variant

Private m_vHeredado As Variant
Private m_vGrupoCompatibilidad As Variant

Private m_vFecAct As Variant

''' <summary>
''' Obtener el valor de m_vIDImpuesto
''' </summary>
''' <remarks>Llamada desde: frmImpuestos/CargarGrid; Tiempo m�ximo: 0</remarks>
Public Property Get IDImpuesto() As Variant
    IDImpuesto = m_vIDImpuesto
End Property

''' <summary>
''' Establecer el valor de m_vIDImpuesto
''' </summary>
''' <param name="Data">Valor de la propiedad</param>
''' <remarks>Llamada desde: CImpuestos/Add; Tiempo m�ximo: 0</remarks>
Public Property Let IDImpuesto(ByVal Data As Variant)
    m_vIDImpuesto = Data
End Property

''' <summary>
''' Obtener el valor de m_vGrupoCompatibilidad
''' </summary>
''' <remarks>Llamada desde: frmImpuestos/CargarGrid; Tiempo m�ximo: 0</remarks>
Public Property Get GrupoCompatibilidad() As Variant
    GrupoCompatibilidad = m_vGrupoCompatibilidad
End Property

''' <summary>
''' Establecer el valor de m_vGrupoCompatibilidad
''' </summary>
''' <param name="Data">Valor de la propiedad</param>
''' <remarks>Llamada desde: CImpuestos/Add; Tiempo m�ximo: 0</remarks>
Public Property Let GrupoCompatibilidad(ByVal Data As Variant)
    m_vGrupoCompatibilidad = Data
End Property

''' <summary>
''' Obtener el valor de m_vIDMat
''' </summary>
''' <remarks>Llamada desde: frmImpuestos/CargarGrid; Tiempo m�ximo: 0</remarks>
Public Property Get IDMat() As Variant
    IDMat = m_vIDMat
End Property

''' <summary>
''' Establecer el valor de m_vIDMat
''' </summary>
''' <param name="Data">Valor de la propiedad</param>
''' <remarks>Llamada desde: CImpuestos/Add; Tiempo m�ximo: 0</remarks>
Public Property Let IDMat(ByVal Data As Variant)
    m_vIDMat = Data
End Property

''' <summary>
''' Obtener el valor de m_vIDValor
''' </summary>
''' <remarks>Llamada desde: frmImpuestos/CargarGrid; Tiempo m�ximo: 0</remarks>
Public Property Get IDValor() As Variant
    IDValor = m_vIDValor
End Property

''' <summary>
''' Establecer el valor de m_vIDValor
''' </summary>
''' <param name="Data">Valor de la propiedad</param>
''' <remarks>Llamada desde: CImpuestos/Add; Tiempo m�ximo: 0</remarks>
Public Property Let IDValor(ByVal Data As Variant)
    m_vIDValor = Data
End Property

''' <summary>
''' Obtener el valor de m_vCOD
''' </summary>
''' <remarks>Llamada desde: frmImpuestos/CargarGrid; Tiempo m�ximo: 0</remarks>
Public Property Get CodImpuesto() As Variant
    CodImpuesto = m_vCod
End Property

''' <summary>
''' Establecer el valor de m_vCOD
''' </summary>
''' <param name="Data">Valor de la propiedad</param>
''' <remarks>Llamada desde: CImpuestos/Add; Tiempo m�ximo: 0</remarks>
Public Property Let CodImpuesto(ByVal Data As Variant)
    m_vCod = Data
End Property

''' <summary>
''' Obtener el valor de m_vDescr
''' </summary>
''' <remarks>Llamada desde: frmImpuestos/CargarGrid; Tiempo m�ximo: 0</remarks>
Public Property Get Descripcion() As Variant
    Descripcion = m_vDescr
End Property

''' <summary>
''' Establecer el valor de m_vDescr
''' </summary>
''' <param name="Data">Valor de la propiedad</param>
''' <remarks>Llamada desde: CImpuestos/Add; Tiempo m�ximo: 0</remarks>
Public Property Let Descripcion(ByVal Data As Variant)
    m_vDescr = Data
End Property
''' <summary>
''' Devolver las denominacions del Impuesto
''' </summary>
''' <remarks>Llamada desde: frmNuevoImpuesto/cmdAceptar_click; Tiempo m�ximo: 0</remarks>
Public Property Get Denominaciones() As CMultiidiomas
    Set Denominaciones = m_oDen
End Property

''' <summary>
''' Dar valor al objeto Denominaciones
''' </summary>
''' <param name="Data">Valor de la propiedad</param>
''' <remarks>Llamada desde: frmNuevoImpuesto/cmdAceptar_click;; Tiempo m�ximo: 0</remarks>
Public Property Set Denominaciones(ByVal dato As CMultiidiomas)
    Set m_oDen = dato
End Property

''' <summary>
''' Obtener el valor de m_vGMN1
''' </summary>
''' <remarks>Llamada desde: frmImpuestos/CargarGrid; Tiempo m�ximo: 0</remarks>
Public Property Get GMN1Cod() As Variant
    GMN1Cod = m_vGMN1
End Property

''' <summary>
''' Establecer el valor de m_vGMN1
''' </summary>
''' <param name="Data">Valor de la propiedad</param>
''' <remarks>Llamada desde: CImpuestos/Add; Tiempo m�ximo: 0</remarks>
Public Property Let GMN1Cod(ByVal Data As Variant)
    m_vGMN1 = Data
End Property

''' <summary>
''' Obtener el valor de m_vGMN2
''' </summary>
''' <remarks>Llamada desde: frmImpuestos/CargarGrid; Tiempo m�ximo: 0</remarks>
Public Property Get GMN2Cod() As Variant
    GMN2Cod = m_vGMN2
End Property

''' <summary>
''' Establecer el valor de m_vGMN2
''' </summary>
''' <param name="Data">Valor de la propiedad</param>
''' <remarks>Llamada desde: CImpuestos/Add; Tiempo m�ximo: 0</remarks>
Public Property Let GMN2Cod(ByVal Data As Variant)
    m_vGMN2 = Data
End Property

''' <summary>
''' Obtener el valor de m_vGMN3
''' </summary>
''' <remarks>Llamada desde: frmImpuestos/CargarGrid; Tiempo m�ximo: 0</remarks>
Public Property Get GMN3Cod() As Variant
    GMN3Cod = m_vGMN3
End Property

''' <summary>
''' Establecer el valor de m_vGMN3
''' </summary>
''' <param name="Data">Valor de la propiedad</param>
''' <remarks>Llamada desde: CImpuestos/Add; Tiempo m�ximo: 0</remarks>
Public Property Let GMN3Cod(ByVal Data As Variant)
    m_vGMN3 = Data
End Property

''' <summary>
''' Obtener el valor de m_vGMN4
''' </summary>
''' <remarks>Llamada desde: frmImpuestos/CargarGrid; Tiempo m�ximo: 0</remarks>
Public Property Get GMN4Cod() As Variant
    GMN4Cod = m_vGMN4
End Property

''' <summary>
''' Establecer el valor de m_vGMN4
''' </summary>
''' <param name="Data">Valor de la propiedad</param>
''' <remarks>Llamada desde: CImpuestos/Add; Tiempo m�ximo: 0</remarks>
Public Property Let GMN4Cod(ByVal Data As Variant)
    m_vGMN4 = Data
End Property

''' <summary>
''' Obtener el valor de m_vArt
''' </summary>
''' <remarks>Llamada desde: frmImpuestos/CargarGrid; Tiempo m�ximo: 0</remarks>
Public Property Get Art() As Variant
    Art = m_vArt
End Property

''' <summary>
''' Establecer el valor de m_vArt
''' </summary>
''' <param name="Data">Valor de la propiedad</param>
''' <remarks>Llamada desde: CImpuestos/Add; Tiempo m�ximo: 0</remarks>
Public Property Let Art(ByVal Data As Variant)
    m_vArt = Data
End Property

''' <summary>
''' Obtener el valor de m_vValor
''' </summary>
''' <remarks>Llamada desde: frmImpuestos/CargarGrid; Tiempo m�ximo: 0</remarks>
Public Property Get Valor() As Variant
    Valor = m_vValor
End Property

''' <summary>
''' Establecer el valor de m_vValor
''' </summary>
''' <param name="Data">Valor de la propiedad</param>
''' <remarks>Llamada desde: CImpuestos/Add; Tiempo m�ximo: 0</remarks>
Public Property Let Valor(ByVal Data As Variant)
    m_vValor = Data
End Property

''' <summary>
''' Obtener el valor de m_vRetenido
''' </summary>
''' <remarks>Llamada desde: frmImpuestos/CargarGrid; Tiempo m�ximo: 0</remarks>
Public Property Get Retenido() As Variant
    Retenido = m_vRetenido
End Property

''' <summary>
''' Establecer el valor de m_vRetenido
''' </summary>
''' <param name="Data">Valor de la propiedad</param>
''' <remarks>Llamada desde: CImpuestos/Add; Tiempo m�ximo: 0</remarks>
Public Property Let Retenido(ByVal Data As Variant)
    m_vRetenido = Data
End Property

''' <summary>
''' Obtener el valor de m_vVigencia
''' </summary>
''' <remarks>Llamada desde: frmImpuestos/CargarGrid; Tiempo m�ximo: 0</remarks>
Public Property Get Vigencia() As Variant
    Vigencia = m_vVigencia
End Property

''' <summary>
''' Establecer el valor de m_vVigencia
''' </summary>
''' <param name="Data">Valor de la propiedad</param>
''' <remarks>Llamada desde: CImpuestos/Add; Tiempo m�ximo: 0</remarks>
Public Property Let Vigencia(ByVal Data As Variant)
    m_vVigencia = Data
End Property

''' <summary>
''' Obtener el valor de m_vImpuestoPaisProvincia
''' </summary>
''' <remarks>Llamada desde: frmImpuestos/CargarGrid; Tiempo m�ximo: 0</remarks>
Public Property Get ImpuestoPaisProvincia() As Variant
    ImpuestoPaisProvincia = m_vImpuestoPaisProvincia
End Property

''' <summary>
''' Establecer el valor de m_vImpuestoPais
''' </summary>
''' <param name="Data">Valor de la propiedad</param>
''' <remarks>Llamada desde: CImpuestos/Add; Tiempo m�ximo: 0</remarks>
Public Property Let ImpuestoPaisProvincia(ByVal Data As Variant)
    m_vImpuestoPaisProvincia = Data
End Property

''' <summary>
''' Obtener el valor de m_vCodPais
''' </summary>
''' <remarks>Llamada desde: frmImpuestos/CargarGrid; Tiempo m�ximo: 0</remarks>
Public Property Get CodPais() As Variant
    CodPais = m_vCodPais
End Property

''' <summary>
''' Establecer el valor de m_vCodPais
''' </summary>
''' <param name="Data">Valor de la propiedad</param>
''' <remarks>Llamada desde: CImpuestos/Add; Tiempo m�ximo: 0</remarks>
Public Property Let CodPais(ByVal Data As Variant)
    m_vCodPais = Data
End Property

''' <summary>
''' Obtener el valor de m_vCodProvincia
''' </summary>
''' <remarks>Llamada desde: frmImpuestos/CargarGrid; Tiempo m�ximo: 0</remarks>
Public Property Get CodProvincia() As Variant
    CodProvincia = m_vCodProvincia
End Property

''' <summary>
''' Establecer el valor de m_vCodProvincia
''' </summary>
''' <param name="Data">Valor de la propiedad</param>
''' <remarks>Llamada desde: CImpuestos/Add; Tiempo m�ximo: 0</remarks>
Public Property Let CodProvincia(ByVal Data As Variant)
    m_vCodProvincia = Data
End Property

''' <summary>
''' Obtener el valor de m_vDenPais
''' </summary>
''' <remarks>Llamada desde: frmImpuestos/CargarGrid; Tiempo m�ximo: 0</remarks>
Public Property Get DenPais() As Variant
    DenPais = m_vDenPais
End Property

''' <summary>
''' Establecer el valor de m_vDenPais
''' </summary>
''' <param name="Data">Valor de la propiedad</param>
''' <remarks>Llamada desde: CImpuestos/Add; Tiempo m�ximo: 0</remarks>
Public Property Let DenPais(ByVal Data As Variant)
    m_vDenPais = Data
End Property

''' <summary>
''' Obtener el valor de m_vDenProvincia
''' </summary>
''' <remarks>Llamada desde: frmImpuestos/CargarGrid; Tiempo m�ximo: 0</remarks>
Public Property Get DenProvincia() As Variant
    DenProvincia = m_vDenProvincia
End Property

''' <summary>
''' Establecer el valor de m_vDenProvincia
''' </summary>
''' <param name="Data">Valor de la propiedad</param>
''' <remarks>Llamada desde: CImpuestos/Add; Tiempo m�ximo: 0</remarks>
Public Property Let DenProvincia(ByVal Data As Variant)
    m_vDenProvincia = Data
End Property

''' <summary>
''' Obtener el valor de m_vHeredado
''' </summary>
''' <remarks>Llamada desde: frmImpuestos/CargarGrid; Tiempo m�ximo: 0</remarks>
Public Property Get Heredado() As Variant
    Heredado = m_vHeredado
End Property

''' <summary>
''' Establecer el valor de m_vHeredado
''' </summary>
''' <param name="Data">Valor de la propiedad</param>
''' <remarks>Llamada desde: CImpuestos/Add; Tiempo m�ximo: 0</remarks>
Public Property Let Heredado(ByVal Data As Variant)
    m_vHeredado = Data
End Property

''' <summary>
''' Obtener el valor de m_vFecAct
''' </summary>
''' <remarks>Llamada desde: Nadie, usan la privada IBaseDatos_AnyadirABaseDatos y IBaseDatos_FinalizarEdicionModificando ; Tiempo m�ximo: 0</remarks>
Public Property Get FecAct() As Variant
    FecAct = m_vFecAct
End Property
''' <summary>
''' Establecer el valor de m_vFecAct
''' </summary>
''' <param name="Data">Valor de la propiedad</param>
''' <remarks>Llamada desde: CImpuestos/Add ; Tiempo m�ximo: 0</remarks>
Public Property Let FecAct(ByVal Data As Variant)
    m_vFecAct = Data
End Property

''' <summary>
''' Obtener el valor de m_iTipo
''' </summary>
''' <remarks>Llamada desde: Nadie, usan la privada IBaseDatos_AnyadirABaseDatos y IBaseDatos_FinalizarEdicionModificando ; Tiempo m�ximo: 0</remarks>
Public Property Get Tipo() As Variant
    Tipo = m_vTipo
End Property
''' <summary>
''' Establecer el valor de m_iTipo
''' </summary>
''' <param name="Data">Valor de la propiedad</param>
''' <remarks>Llamada desde: CImpuestos/Add ; Tiempo m�ximo: 0</remarks>
Public Property Let Tipo(ByVal Data As Variant)
    m_vTipo = Data
End Property

''' <summary>
''' Obtener el valor de m_vAtribId
''' </summary>
''' <remarks>Llamada desde: Nadie, usan la privada IBaseDatos_AnyadirABaseDatos y IBaseDatos_FinalizarEdicionModificando ; Tiempo m�ximo: 0</remarks>
Public Property Get AtribID() As Variant
    AtribID = m_vAtribID
End Property
''' <summary>
''' Establecer el valor de m_vAtribId
''' </summary>
''' <param name="Data">Valor de la propiedad</param>
''' <remarks>Llamada desde: CImpuestos/Add ; Tiempo m�ximo: 0</remarks>
Public Property Let AtribID(ByVal Data As Variant)
    m_vAtribID = Data
End Property

''' <summary>
''' Obtener el valor de m_vNivelCategoria
''' </summary>
''' <remarks>Llamada desde: Nadie, usan la privada IBaseDatos_AnyadirABaseDatos y IBaseDatos_FinalizarEdicionModificando ; Tiempo m�ximo: 0</remarks>
Public Property Get NivelCategoria() As Variant
    NivelCategoria = m_vNivelCategoria
End Property
''' <summary>
''' Establecer el valor de m_vNivelCategoria
''' </summary>
''' <param name="Data">Valor de la propiedad</param>
''' <remarks>Llamada desde: CImpuestos/Add ; Tiempo m�ximo: 0</remarks>
Public Property Let NivelCategoria(ByVal Data As Variant)
    m_vNivelCategoria = Data
End Property

''' <summary>
''' Obtener el valor de m_vConcepto
''' </summary>
''' <remarks>Llamada desde: frmImpuestos/CargarGrid; Tiempo m�ximo: 0</remarks>
Public Property Get Concepto() As Variant
    Concepto = m_vConcepto
End Property

''' <summary>
''' Establecer el valor de m_vConcepto
''' </summary>
''' <param name="Data">Valor de la propiedad</param>
''' <remarks>Llamada desde: CImpuestos/Add; Tiempo m�ximo: 0</remarks>
Public Property Let Concepto(ByVal Data As Variant)
    m_vConcepto = Data
End Property

''' <summary>
''' Establecer el valor de m_oConexion
''' </summary>
''' <param name="Data">Valor de la propiedad</param>
''' <remarks>Llamada desde: CImpuestos/Add ; Tiempo m�ximo: 0</remarks>
Friend Property Set Conexion(ByVal con As CConexion)
Set m_oConexion = con
End Property
''' <summary>
''' Obtener el valor de m_oConexion
''' </summary>
''' <remarks>Llamada desde: Nadie, usan la privada IBaseDatos_AnyadirABaseDatos y IBaseDatos_FinalizarEdicionModificando ; Tiempo m�ximo: 0</remarks>
Friend Property Get Conexion() As CConexion
Set Conexion = m_oConexion
End Property

''' <summary>
''' destroys collection when this class is terminated
''' </summary>
''' <remarks>Llamada desde: sistema ; Tiempo m�ximo: 0</remarks>
Private Sub Class_Terminate()
    Set m_oConexion = Nothing
End Sub

''' <summary>Esta funci�n a�ade el Impuesto al materia/articulo </summary>
''' <returns>TipoErrorSummit con el error si lo hay</returns>
''' <remarks>Llamada desde: frmImpuestos/sdbgImpuestos_BeforeUpdate
''' Tiempo m�ximo: 0,1 sec </remarks>
''' Revisado por: Jbg. Fecha: 14/06/2012
Private Function IBaseDatos_AnyadirABaseDatos() As TipoErrorSummit
    Dim sConsulta As String
    Dim sWhere As String
    Dim TESError As TipoErrorSummit
    Dim rs As adodb.Recordset
    Dim rsIP As adodb.Recordset
    Dim rsIV As adodb.Recordset
    
    Dim bEstaEn_NoMat As Boolean
    Dim bEstaEn_NoMat_PeroSinPadreMat As Boolean
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    TESError.NumError = TESnoerror

    If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:

    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
    
    If NullToStr(m_vImpuestoPaisProvincia) = "" Then
        'comprobar no preexistencia en bd
        Set rsIP = New adodb.Recordset
        sConsulta = "SELECT ID FROM IMPUESTO_PAIPROVI I WITH(NOLOCK) "
        sConsulta = sConsulta & " WHERE I.IMPUESTO=" & m_vIDImpuesto & " AND I.PAI=" & StrToSQLNULL(m_vCodPais)
        If NullToStr(m_vCodProvincia) <> "" Then
            sConsulta = sConsulta & " AND I.PROVI=" & StrToSQLNULL(m_vCodProvincia)
        Else
            sConsulta = sConsulta & " AND I.PROVI IS NULL"
        End If
        rsIP.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
        If rsIP.eof Then
                sConsulta = "INSERT INTO IMPUESTO_PAIPROVI(PAI,PROVI,IMPUESTO,FECACT) VALUES (" _
                    & StrToSQLNULL(m_vCodPais) & "," & StrToSQLNULL(m_vCodProvincia) & "," & m_vIDImpuesto & ",GETDATE())"
            If NullToStr(m_vCodPais) <> "" Then
                m_oConexion.ADOCon.Execute sConsulta
                If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
        
                'Calidad Sin with(nolock)
                Set rsIP = New adodb.Recordset
                rsIP.Open "SELECT @@IDENTITY", m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
                
                m_vImpuestoPaisProvincia = rsIP(0).Value
                
                rsIP.Close
            End If
        End If
        Set rsIP = Nothing
    End If
        
    'comprobar no preexistencia en bd
    'Valor
    Set rsIV = New adodb.Recordset
    sConsulta = "SELECT ID,VIGENTE FROM IMPUESTO_PP_VALOR IV WITH(NOLOCK) "
    sConsulta = sConsulta & " WHERE IV.IMPUESTO_PAIPROVI=" & m_vImpuestoPaisProvincia & " AND IV.VALOR=" & DblToSQLFloat(m_vValor)
    
    If m_vImpuestoPaisProvincia <> "" Then
        rsIV.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly

        If rsIV.eof Then
            sConsulta = "INSERT INTO IMPUESTO_PP_VALOR (IMPUESTO_PAIPROVI,VALOR,VIGENTE,FECACT) VALUES (" _
                & m_vImpuestoPaisProvincia & "," & DblToSQLFloat(m_vValor) & ",1,GETDATE())"
            m_oConexion.ADOCon.Execute sConsulta
            If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
            'Calidad Sin with(nolock)
            Set rsIV = New adodb.Recordset
            rsIV.Open "SELECT @@IDENTITY", m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
            
            m_vIDValor = rsIV(0).Value
            
            m_vVigencia = 1
        
            rsIV.Close
        Else
            m_vVigencia = rsIV("VIGENTE").Value
            
            rsIV.Close
        End If
    End If
    
    Set rsIV = Nothing
    
    
    Select Case Me.Tipo
    Case TipoImpuesto.ArticuloMaterial
        'Mat/Art
        Set rs = New adodb.Recordset
        sConsulta = "SELECT ID FROM IMPUESTO_MAT I WITH(NOLOCK)"
        sConsulta = sConsulta & " WHERE I.IMPUESTO_PP_VALOR=" & m_vIDValor
        
        sWhere = ""
        If NullToStr(m_vArt) = "" Then
            If NullToStr(m_vGMN1) <> "" Then sWhere = sWhere & " AND I.GMN1=" & StrToSQLNULL(m_vGMN1)
            If NullToStr(m_vGMN2) = "" Then
                sWhere = sWhere & " AND I.GMN2 IS NULL"
            Else
                sWhere = sWhere & " AND I.GMN2=" & StrToSQLNULL(m_vGMN2)
                If NullToStr(m_vGMN3) = "" Then
                    sWhere = sWhere & " AND I.GMN3 IS NULL"
                Else
                    sWhere = sWhere & " AND I.GMN3=" & StrToSQLNULL(m_vGMN3)
                    If NullToStr(m_vGMN4) = "" Then
                        sWhere = sWhere & " AND I.GMN4 IS NULL"
                    Else
                        sWhere = sWhere & " AND I.GMN4=" & StrToSQLNULL(m_vGMN4)
                    End If
                End If
            End If
        Else
           sWhere = " AND I.ART=" & StrToSQLNULL(m_vArt)
        End If
        sConsulta = sConsulta & sWhere
                
        If m_vIDValor <> "" Then
            rs.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
            If rs.eof Then
                bEstaEn_NoMat = False
            
                'Q no este entre los padres "eliminados"
                Set rs = New adodb.Recordset
                sConsulta = "SELECT ID FROM IMPUESTO_NO_MAT I WITH(NOLOCK) WHERE I.IMPUESTO_PP_VALOR=" & m_vIDValor & sWhere
        
                rs.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
                If Not rs.eof Then
                    bEstaEn_NoMat = True
                
                    sConsulta = "DELETE IMPUESTO_NO_MAT WHERE IMPUESTO_PP_VALOR= " & m_vIDValor & Replace(sWhere, "I.", "")
                    
                    m_oConexion.ADOCon.Execute sConsulta
                    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
                    Set rs = New adodb.Recordset
                    sConsulta = "SELECT ID FROM IMPUESTO_MAT I WITH(NOLOCK)"
                    sConsulta = sConsulta & " WHERE I.IMPUESTO_PP_VALOR=" & m_vIDValor
                    sConsulta = sConsulta & " AND I.GMN1=" & StrToSQLNULL(m_vGMN1) 'Est� el padre a la altura q est�, colgara de gmn1
                    
                    rs.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
                    
                    bEstaEn_NoMat_PeroSinPadreMat = False
                    If Not rs.eof Then
                        m_vHeredado = "1"
                    Else
                        bEstaEn_NoMat_PeroSinPadreMat = True
                    End If
                End If
                
                If (Not bEstaEn_NoMat) Or (bEstaEn_NoMat And bEstaEn_NoMat_PeroSinPadreMat) Then
                    If m_vArt <> "" Then
                        sConsulta = "INSERT INTO IMPUESTO_MAT (ART,IMPUESTO_PP_VALOR,FECACT) VALUES (" _
                            & StrToSQLNULL(m_vArt) & "," & m_vIDValor & ", GETDATE())"
                    Else
                        sConsulta = "INSERT INTO IMPUESTO_MAT (GMN1,GMN2,GMN3,GMN4,IMPUESTO_PP_VALOR,FECACT) VALUES (" _
                            & StrToSQLNULL(m_vGMN1) & "," & StrToSQLNULL(m_vGMN2) & "," & StrToSQLNULL(m_vGMN3) & "," & StrToSQLNULL(m_vGMN4) _
                            & "," & m_vIDValor & ", GETDATE())"
                    End If
                    
                    m_oConexion.ADOCon.Execute sConsulta
                    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
                    'Cargamos el ID de lo recien insertado por eso no se pone WITH (NOLOCk)
                    Set rs = New adodb.Recordset
                    sConsulta = "SELECT @@IDENTITY"
                    rs.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
                    
                    m_vIDMat = rs(0).Value
                    
                    rs.Close
                    Set rs = Nothing
                    
                    'Cargamos el FECACT de lo recien insertado por eso no se pone WITH (NOLOCk)
                    Set rs = New adodb.Recordset
                    sConsulta = "SELECT FECACT FROM IMPUESTO_MAT WHERE ID=" & m_vIDMat
                    rs.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
                    m_vFecAct = rs(0).Value
                    rs.Close
                End If
            End If
        End If
    
    Set rs = Nothing
    
    
    
    Case TipoImpuesto.CosteCategoria
        Set rs = New adodb.Recordset
        
        sConsulta = "SELECT ID FROM IMPUESTO_CATN" & Me.NivelCategoria & "_ATRIB ICA WITH(NOLOCK)"
        sConsulta = sConsulta & " WHERE ICA.IMPUESTO_PP_VALOR=" & m_vIDValor
        sConsulta = sConsulta & " AND ICA.CATN" & Me.NivelCategoria & "_ATRIB=" & Me.AtribID
        
        rs.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
        If rs.eof Then
            sConsulta = "INSERT INTO IMPUESTO_CATN" & Me.NivelCategoria & "_ATRIB (IMPUESTO_PP_VALOR,CATN" & Me.NivelCategoria & "_ATRIB) VALUES (" _
            & m_vIDValor & ", " & Me.AtribID & ")"
            m_oConexion.ADOCon.Execute sConsulta
            If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
            'Cargamos el ID de lo recien insertado por eso no se pone WITH (NOLOCk)
            Set rs = New adodb.Recordset
            sConsulta = "SELECT @@IDENTITY"
            rs.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
            
            m_vIDMat = rs(0).Value
            
            rs.Close
            Set rs = Nothing
            
            'Cargamos el FECACT de lo recien insertado por eso no se pone WITH (NOLOCk)
            Set rs = New adodb.Recordset
            sConsulta = "SELECT FECACT FROM IMPUESTO_CATN" & Me.NivelCategoria & "_ATRIB WHERE ID=" & m_vIDMat
            rs.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
            m_vFecAct = rs(0).Value
            rs.Close
        End If
    Case TipoImpuesto.CosteLineaCatalogo
        Set rs = New adodb.Recordset
        
        sConsulta = "SELECT ID FROM IMPUESTO_CATALOG_LIN_ATRIB ICA WITH(NOLOCK)"
        sConsulta = sConsulta & " WHERE ICA.IMPUESTO_PP_VALOR=" & m_vIDValor
        sConsulta = sConsulta & " AND ICA.CATALOG_LIN_ATRIB=" & Me.AtribID
        
        rs.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
        If rs.eof Then
            sConsulta = "INSERT INTO IMPUESTO_CATALOG_LIN_ATRIB (IMPUESTO_PP_VALOR,CATALOG_LIN_ATRIB) VALUES (" _
            & m_vIDValor & ", " & Me.AtribID & ")"
            m_oConexion.ADOCon.Execute sConsulta
            If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
            'Cargamos el ID de lo recien insertado por eso no se pone WITH (NOLOCk)
            Set rs = New adodb.Recordset
            sConsulta = "SELECT @@IDENTITY"
            rs.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
            
            m_vIDMat = rs(0).Value
            
            rs.Close
            Set rs = Nothing
            
            'Cargamos el FECACT de lo recien insertado por eso no se pone WITH (NOLOCk)
            Set rs = New adodb.Recordset
            sConsulta = "SELECT FECACT FROM IMPUESTO_CATALOG_LIN_ATRIB WHERE ID=" & m_vIDMat
            rs.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
            m_vFecAct = rs(0).Value
            rs.Close
        End If
    Case TipoImpuesto.LineaCatalogo
        Set rs = New adodb.Recordset
        
        sConsulta = "SELECT ID FROM IMPUESTO_CATALOG_LIN ICA WITH(NOLOCK)"
        sConsulta = sConsulta & " WHERE ICA.IMPUESTO_PP_VALOR=" & m_vIDValor
        sConsulta = sConsulta & " AND ICA.CATALOG_LIN=" & Me.AtribID 'id de la linea del catalogo
        
        rs.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
        If rs.eof Then
            sConsulta = "INSERT INTO IMPUESTO_CATALOG_LIN (IMPUESTO_PP_VALOR,CATALOG_LIN) VALUES (" _
            & m_vIDValor & ", " & Me.AtribID & ")"
            m_oConexion.ADOCon.Execute sConsulta
            If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
            'Cargamos el ID de lo recien insertado por eso no se pone WITH (NOLOCk)
            Set rs = New adodb.Recordset
            sConsulta = "SELECT @@IDENTITY"
            rs.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
            
            m_vIDMat = rs(0).Value
            
            rs.Close
            Set rs = Nothing
            
            'Cargamos el FECACT de lo recien insertado por eso no se pone WITH (NOLOCk)
            Set rs = New adodb.Recordset
            sConsulta = "SELECT FECACT FROM IMPUESTO_CATALOG_LIN WHERE ID=" & m_vIDMat
            rs.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
            m_vFecAct = rs(0).Value
            rs.Close
        End If
    End Select
    
    
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    IBaseDatos_AnyadirABaseDatos = TESError
    Exit Function
    
Error_Cls:
     
    IBaseDatos_AnyadirABaseDatos = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      'Call g_oErrores.TratarError("Class Module", "CImpuesto", "IBaseDatos_AnyadirABaseDatos", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If
    
End Function

Private Function IBaseDatos_CambiarCodigo(CodigoNuevo As Variant) As TipoErrorSummit

End Function

Private Sub IBaseDatos_CancelarEdicion()

End Sub

Private Function IBaseDatos_ComprobarExistenciaEnBaseDatos() As Boolean

End Function

Private Function IBaseDatos_EliminarDeBaseDatos() As TipoErrorSummit

End Function

Private Function IBaseDatos_FinalizarEdicionEliminando() As TipoErrorSummit

End Function

Private Function IBaseDatos_FinalizarEdicionModificando() As TipoErrorSummit

End Function

''' <summary>Esta funci�n inicia la edici�n del resulset</summary>
'''<param optional name="Bloquear">Si se bloquea o no</param>
'''<param optional name="UsuarioBloqueo">Usuario del bloqueo</param>
''' <returns>TipoErrorSummit con el error si lo hay</returns>
''' <remarks>Llamada desde: frmImpuestos/sdbgImpuestos_Change
''' Tiempo m�ximo: 0,1 seg </remarks>
''' Revisado por: Jbg. Fecha: 15/06/2012
Private Function IBaseDatos_IniciarEdicion(Optional ByVal Bloquear As Boolean, Optional ByVal UsuarioBloqueo As String) As TipoErrorSummit
    Dim TESError As TipoErrorSummit
    Dim sSQL As String
    Dim m_adores As adodb.Recordset
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    TESError.NumError = TESnoerror
     
    Set m_adores = New adodb.Recordset
    'Calidad sin With(nolock) para asegurarse de q realmente no se ha modificado
    Select Case Me.Tipo
    Case TipoImpuesto.ArticuloMaterial
        m_adores.Open ("SELECT FECACT FROM IMPUESTO_MAT WHERE ID=" & m_vIDMat), m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
    Case TipoImpuesto.CosteCategoria
        m_adores.Open ("SELECT FECACT FROM IMPUESTO_CATN" & Me.NivelCategoria & "_ATRIB WHERE ID=" & m_vIDMat), m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
    Case TipoImpuesto.CosteLineaCatalogo
        m_adores.Open ("SELECT FECACT FROM IMPUESTO_CATALOG_LIN_ATRIB WHERE ID=" & m_vIDMat), m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
    Case TipoImpuesto.LineaCatalogo
        m_adores.Open ("SELECT FECACT FROM IMPUESTO_CATALOG_LIN WHERE ID=" & m_vIDMat), m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
    End Select
    
    
    If m_adores.eof Then
        m_adores.Close
        Set m_adores = Nothing
        TESError.NumError = TESDatoEliminado
        TESError.Arg1 = 199 'IMPUESTO_MAT
        IBaseDatos_IniciarEdicion = TESError
        Exit Function
    End If
       
    If m_vFecAct <> m_adores("FECACT") Then
        m_adores.Close
        Set m_adores = Nothing
        TESError.NumError = TESInfActualModificada
        IBaseDatos_IniciarEdicion = TESError
        Exit Function
    End If
    
    m_adores.Close
    Set m_adores = Nothing
    
    IBaseDatos_IniciarEdicion = TESError
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CImpuesto", "IBaseDatos_IniciarEdicion", ERR, Erl)
      Exit Function
   End If
End Function

''' <summary>Esta funci�n a�ade el Impuesto</summary>
''' <returns>TipoErrorSummit con el error si lo hay</returns>
''' <remarks>Llamada desde: frmNuevoImpuesto/cmdAceptar_Click ; Tiempo m�ximo: 0</remarks>
''' Revisado por: Jbg. Fecha: 18/06/2012
Public Function CrearNuevoImpuesto() As TipoErrorSummit
    Dim sConsulta As String
    Dim TESError As TipoErrorSummit
    Dim rs As adodb.Recordset
    Dim oMulti As CMultiidioma
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    TESError.NumError = TESnoerror
    
    If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
    
    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
    
    sConsulta = "INSERT INTO IMPUESTO (COD,RETENIDO,CONCEPTO,GRP_COMP,FECACT) VALUES (" & StrToSQLNULL(m_vCod) & "," & BooleanToSQLBinary(m_vRetenido) & "," & DblToSQLFloat(m_vConcepto) & "," & m_vGrupoCompatibilidad & ", GETDATE())"
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    'Cargamos el ID de lo recien insertado por eso no se pone WITH (NOLOCk)
    Set rs = New adodb.Recordset
    sConsulta = "SELECT MAX(ID) AS ID FROM IMPUESTO"
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
    m_vIDImpuesto = rs("ID").Value
    rs.Close
    Set rs = Nothing

    'Inserta las denominaciones en IMPUESTO_DEN
    If Not m_oDen Is Nothing Then
        For Each oMulti In m_oDen
            If Not IsEmpty(oMulti.Den) Then
                sConsulta = "INSERT INTO IMPUESTO_DEN (IMPUESTO,IDIOMA,DEN,FECACT) VALUES (" & m_vIDImpuesto & "," & StrToSQLNULL(oMulti.Cod) & ","
                sConsulta = sConsulta & StrToSQLNULL(oMulti.Den) & ", GETDATE())"
                m_oConexion.ADOCon.Execute sConsulta
                If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
            End If
        Next
    End If
    
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    CrearNuevoImpuesto = TESError
    Exit Function
    
Error_Cls:
     
    CrearNuevoImpuesto = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      'Call g_oErrores.TratarError("Class Module", "CImpuesto", "CrearNuevoImpuesto", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If
End Function
''' <summary>Crea una copia de la colecci�n</summary>
''' <remarks>Llamada desde: frmPEDIDOS.AplicarImpuestosTodasLineas ; Tiempo m�ximo: 0</remarks>
Public Function copiar() As CImpuesto

Dim oImp As CImpuesto

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
Set copiar = New CImpuesto

    copiar.IDMat = IDMat
    copiar.CodImpuesto = CodImpuesto
    copiar.Descripcion = Descripcion
    copiar.IDImpuesto = IDImpuesto
    copiar.Valor = Valor
    copiar.Vigencia = Vigencia
    copiar.Retenido = Retenido
    copiar.CodPais = CodPais
    copiar.DenPais = DenPais
    copiar.CodProvincia = CodProvincia
    copiar.DenProvincia = DenProvincia
    copiar.GrupoCompatibilidad = GrupoCompatibilidad
    copiar.GMN1Cod = GMN1Cod
    copiar.GMN2Cod = GMN2Cod
    copiar.GMN3Cod = GMN3Cod
    copiar.GMN4Cod = GMN4Cod
    copiar.Heredado = Heredado
    copiar.IDValor = IDValor
    copiar.ImpuestoPaisProvincia = ImpuestoPaisProvincia
    copiar.Tipo = Tipo

Set copiar.Conexion = Me.Conexion
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CImpuesto", "copiar", ERR, Erl)
      Exit Function
   End If
End Function



