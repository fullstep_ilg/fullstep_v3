VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CServicio"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum

Private m_oConexion As CConexion
Private mvarConexion As CConexion
Private mvarConexionServer As CConexionDeUseServer
Private miID As Integer
Private miURL As String
Private m_oDenominaciones As CMultiidiomas
Private m_oParametros As CParametrosExternos

Friend Property Set Conexion(ByVal vData As CConexion)
    ''' * Objetivo: Dar valor a la conexion de la Destino
    ''' * Recibe: Conexion
    ''' * Devuelve: Nada
    Set m_oConexion = vData
End Property
Friend Property Get Conexion() As CConexion
    ''' * Objetivo: Devolver la conexion de la Destino
    ''' * Recibe: Nada
    ''' * Devuelve: Conexion
    Set Conexion = m_oConexion
End Property

Public Property Get Id() As Integer
    Id = miID
End Property
Public Property Let Id(ByVal Data As Integer)
    miID = Data
End Property

Public Property Get URL() As String
    URL = miURL
End Property
Public Property Let URL(ByVal Data As String)
    miURL = Data
End Property

Public Property Get Denominaciones() As CMultiidiomas
    Set Denominaciones = m_oDenominaciones
End Property
Public Property Set Denominaciones(ByVal dato As CMultiidiomas)
    Set m_oDenominaciones = dato
End Property

Public Property Get Parametros() As CParametrosExternos
    Set Parametros = m_oParametros
End Property
Public Property Set Parametros(ByVal dato As CParametrosExternos)
    Set m_oParametros = dato
End Property


'****************************************************************************************************
'FUNCIONES
'****************************************************************************************************
Public Function AnyadirServicio(ByVal iServAntiguo As Integer) As TipoErrorSummit
    Dim sConsulta As String
    Dim TESError As TipoErrorSummit
    Dim AdoRes As adodb.Recordset
    
    '********* Precondicion *******************
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CServicio.AnyadirServicio", "No se ha establecido la conexion"
    End If
    '******************************************
    
    On Error GoTo Error:
    TESError.NumError = TESnoerror
    If iServAntiguo = 0 Then
        Set AdoRes = New adodb.Recordset
        'Inserta el campo en BD:
        sConsulta = "INSERT INTO SERVICIO (URL) VALUES ('" & miURL & "')"
        m_oConexion.ADOCon.Execute sConsulta
    
        'Obtiene el id del campo introducido:
        'Calidad: sin lectura sucio por su uso
        sConsulta = "SELECT MAX(ID) AS ID FROM SERVICIO WHERE URL='" & miURL & "'"
        AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
        miID = AdoRes("ID").Value
        AdoRes.Close
        Set AdoRes = Nothing
    Else
        miID = iServAntiguo
    End If
    AnyadirServicio = TESError
    Exit Function
   
Error:
    AnyadirServicio = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    Set AdoRes = Nothing
    
End Function

Public Function AnyadirDenAServicio(ByVal iServAntiguo As Integer, ByVal pID As Integer) As TipoErrorSummit
    Dim sConsulta As String
    Dim TESError As TipoErrorSummit
    Dim oMulti As CMultiidioma
    
    '********* Precondicion *******************
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CServicio.AnyadirDenAServicio", "No se ha establecido la conexion"
    End If
    '******************************************
    
    On Error GoTo Error:
    TESError.NumError = TESnoerror
    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
    If Not m_oDenominaciones Is Nothing Then
        For Each oMulti In m_oDenominaciones
            If Not IsEmpty(oMulti.Den) Then
                If iServAntiguo = 0 Then
                    sConsulta = "INSERT INTO SERVICIO_DEN (SERVICIO,IDIOMA,DEN) VALUES (" & pID & "," & _
                            "'" & DblQuote(oMulti.Cod) & "'," & StrToSQLNULLNVar(oMulti.Den) & ")"
                Else
                    sConsulta = "UPDATE SERVICIO_DEN SET DEN=" & StrToSQLNULLNVar(oMulti.Den) & " WHERE SERVICIO=" & pID & " AND IDIOMA=" & StrToSQLNULLNVar(oMulti.Cod)
                End If
                
                m_oConexion.ADOCon.Execute sConsulta
                If m_oConexion.ADOCon.Errors.Count > 0 Then
                    GoTo Error
                End If
            End If
        Next
    End If
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    AnyadirDenAServicio = TESError
    Exit Function

Error:
    AnyadirDenAServicio = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
    
End Function

''' <summary>
''' Anyadir/Updatar un param de un servicio
''' </summary>
''' <param name="iServAntiguo">Id del servicio usado como modelo</param>
''' <param name="pServicio">Id del servicio usado creado/a modificar</param>
''' <param name="pFormCampo">Id de campo a crear</param>
''' <param name="pCampoAntiguo">Id de campo a modificar</param>
''' <remarks>Llamada desde: frmServicio.cmdAceptar_click ; Tiempo m�ximo: 0</remarks>
''' Revisado por: jbg; Fecha: 22/09/2015
Public Function AnyadirParamAServicio(ByVal iServAntiguo As Integer, ByVal pServicio As Integer, Optional ByVal pFormCampo As Long, Optional ByVal pCampoAntiguo As Long) As TipoErrorSummit
    Dim sConsulta As String
    Dim TESError As TipoErrorSummit
    Dim AdoRes As adodb.Recordset
    Dim oParam As CParametroExterno
    Dim paramID As Long
    
    '********* Precondicion *******************
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CServicio.AnyadirParamAServicio", "No se ha establecido la conexion"
    End If
    '******************************************
    
    On Error GoTo Error:
    TESError.NumError = TESnoerror
    
    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
    
    If Not m_oParametros Is Nothing Then
        For Each oParam In m_oParametros
            If iServAntiguo = 0 Then
                sConsulta = "INSERT INTO SERVICIO_PARAMETRO (SERVICIO,DEN,SENTIDO,TIPO,INDICADOR_ERROR) " & _
                        "VALUES (" & pServicio & "," & StrToSQLNULLNVar(oParam.param) & "," & oParam.Sentido & "," & _
                        oParam.Tipo & "," & BooleanToSQLBinary(oParam.IndError) & ")"
                m_oConexion.ADOCon.Execute sConsulta
                If m_oConexion.ADOCon.Errors.Count > 0 Then
                    GoTo Error
                End If
                'Obtiene el id del campo introducido:
                'Calidad: sin lectura sucio por su uso
                Set AdoRes = New adodb.Recordset
                sConsulta = "SELECT MAX(ID) AS ID FROM SERVICIO_PARAMETRO WHERE DEN=" & StrToSQLNULLNVar(oParam.param)
                AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
                paramID = AdoRes("ID").Value
                AdoRes.Close
                Set AdoRes = Nothing
            Else
                sConsulta = "UPDATE SERVICIO_PARAMETRO SET DEN=" & StrToSQLNULLNVar(oParam.param) & "," & _
                            "INDICADOR_ERROR=" & BooleanToSQLBinary(oParam.IndError) & " WHERE ID=" & oParam.Id
                m_oConexion.ADOCon.Execute sConsulta
                If m_oConexion.ADOCon.Errors.Count > 0 Then
                    GoTo Error
                End If
                paramID = oParam.Id
            End If
            If pCampoAntiguo = 0 Then
                sConsulta = "INSERT INTO FORM_CAMPO_SERVICIO_PARAMETRO (FORM_CAMPO,SERVICIO_PARAMETRO,DIRECTO,VALOR_TEXT," & _
                        "VALOR_NUM,VALOR_FEC,VALOR_BOOL,TIPO_CAMPO,CAMPO) VALUES (" & pFormCampo & "," & paramID & ","
                If IsNull(oParam.Directo) Then
                    sConsulta = sConsulta & ",NULL,"
                Else
                    sConsulta = sConsulta & BooleanToSQLBinary(oParam.Directo) & ","
                End If
                Select Case oParam.Tipo
                    Case 2
                        sConsulta = sConsulta & "NULL," & DblToSQLFloat(oParam.Valor) & ",NULL,NULL,"
                    Case 3
                        sConsulta = sConsulta & "NULL,NULL," & DateToSQLDate(oParam.Valor) & ",NULL,"
                    Case 4
                        sConsulta = sConsulta & "NULL,NULL,NULL," & BooleanToSQLBinary(oParam.Valor) & ","
                    Case 6
                        sConsulta = sConsulta & StrToSQLNULL(oParam.Valor) & ",NULL,NULL,NULL,"
                End Select
                sConsulta = sConsulta & oParam.TipoCampo & "," & oParam.Campo & ")"
            Else
                sConsulta = "UPDATE FORM_CAMPO_SERVICIO_PARAMETRO SET"
                If Not IsNull(oParam.Directo) Then
                    sConsulta = sConsulta & " DIRECTO=" & BooleanToSQLBinary(oParam.Directo)
                End If
                Select Case oParam.Tipo
                    Case 2
                        sConsulta = sConsulta & ", VALOR_NUM=" & DblToSQLFloat(oParam.Valor)
                    Case 3
                        sConsulta = sConsulta & ", VALOR_FEC=" & DateToSQLDate(oParam.Valor)
                    Case 4
                        sConsulta = sConsulta & ", VALOR_BOOL=" & BooleanToSQLBinary(oParam.Valor)
                    Case 6
                        sConsulta = sConsulta & ", VALOR_TEXT=" & StrToSQLNULL(oParam.Valor)
                End Select
                sConsulta = sConsulta & ", TIPO_CAMPO=" & oParam.TipoCampo & ", CAMPO=" & oParam.Campo
                sConsulta = sConsulta & " WHERE FORM_CAMPO=" & pCampoAntiguo & " AND SERVICIO_PARAMETRO=" & paramID
            End If
            
            m_oConexion.ADOCon.Execute sConsulta
            If m_oConexion.ADOCon.Errors.Count > 0 Then
                GoTo Error
            End If
        Next
    End If
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    Exit Function
Error:
    AnyadirParamAServicio = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
    
End Function

Public Function TieneResultados() As Boolean
    Dim sConsulta As String
    Dim rs As New adodb.Recordset

    sConsulta = "SELECT ID FROM SERVICIO WITH (NOLOCK)"
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    If rs.eof Then
        TieneResultados = False
    Else
        TieneResultados = True
    End If
    
    rs.Close
    Set rs = Nothing
    
End Function

Public Function DenominacionesIdioma(ByVal psIdioma As String) As Recordset
    Dim sConsulta As String
    Dim rs As New adodb.Recordset

    '*************************************************************
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CServicio.Denominaciones", "No se ha establecido la conexion"
    End If
    '*************************************************************

    sConsulta = "SELECT S.ID, SD.DEN FROM SERVICIO S WITH (NOLOCK)INNER JOIN SERVICIO_DEN SD ON S.ID=SD.SERVICIO " & _
                "WHERE SD.IDIOMA='" & psIdioma & "'"
    rs.CursorLocation = adUseClient
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    Set rs.ActiveConnection = Nothing
    Set DenominacionesIdioma = rs
    
End Function

Public Function SacarDenIdiomaServicio(ByVal piID As Integer, ByVal psIdioma As String) As String
    Dim sConsulta As String
    Dim rs As New adodb.Recordset

    '*************************************************************
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CServicio.SacarDenIdiomaServicio", "No se ha establecido la conexion"
    End If
    '*************************************************************
    
    If piID = 0 Then
        SacarDenIdiomaServicio = ""
    Else
        sConsulta = "SELECT DEN FROM SERVICIO_DEN WITH (NOLOCK) WHERE SERVICIO= " & piID & " AND IDIOMA='" & psIdioma & "'"
        rs.CursorLocation = adUseClient
        rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
        If Not rs.eof Then
            SacarDenIdiomaServicio = rs.Fields("DEN").Value
        End If
        rs.Close
        Set rs.ActiveConnection = Nothing
    End If
    
End Function

Public Function SacarURLServicio(ByVal piID As Integer) As String
    Dim sConsulta As String
    Dim rs As New adodb.Recordset

    '*************************************************************
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CServicio.SacarURLServicio", "No se ha establecido la conexion"
    End If
    '*************************************************************
    
    If piID = 0 Then
        SacarURLServicio = ""
    Else
        sConsulta = "SELECT URL FROM SERVICIO WITH (NOLOCK) WHERE ID= " & piID
        rs.CursorLocation = adUseClient
        rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
        If Not rs.eof Then
            SacarURLServicio = rs.Fields("URL").Value
        End If
        rs.Close
        Set rs.ActiveConnection = Nothing
    End If
    
End Function

Public Function SacarParamServicio(ByVal piID As Integer) As Recordset
    Dim sConsulta As String
    Dim rs As New adodb.Recordset

    '*************************************************************
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CServicio.SacarParamServicio", "No se ha establecido la conexion"
    End If
    '*************************************************************

    sConsulta = "SELECT ID,DEN,SENTIDO,TIPO,INDICADOR_ERROR FROM SERVICIO_PARAMETRO WITH (NOLOCK) WHERE SERVICIO= " & piID
    rs.CursorLocation = adUseClient
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    If Not rs.eof Then
        Set SacarParamServicio = rs
    End If
    Set rs.ActiveConnection = Nothing
    
End Function

Public Function SacarDatosServicio(ByVal iCampoForm As Long) As Recordset
    Dim sConsulta As String
    Dim rs As New adodb.Recordset

    '*************************************************************
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CServicio.SacarDatosServicio", "No se ha establecido la conexion"
    End If
    '*************************************************************

    sConsulta = "SELECT SP.ID,SP.SERVICIO,SP.DEN,SP.SENTIDO,SP.TIPO,SP.INDICADOR_ERROR,FCSP.DIRECTO," & _
                "FCSP.VALOR_TEXT,FCSP.VALOR_NUM,FCSP.VALOR_FEC,FCSP.VALOR_BOOL,FCSP.TIPO_CAMPO,FCSP.CAMPO " & _
                "FROM SERVICIO_PARAMETRO SP INNER JOIN FORM_CAMPO_SERVICIO_PARAMETRO FCSP " & _
                "ON SP.ID=FCSP.SERVICIO_PARAMETRO WHERE FCSP.FORM_CAMPO=" & iCampoForm
    rs.CursorLocation = adUseClient
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    If Not rs.eof Then
        Set SacarDatosServicio = rs
    End If
    Set rs.ActiveConnection = Nothing
End Function

Public Function DevolverIdFormCampo(ByVal iServAntiguo As Integer, ByVal iGrupoID As Long) As Long
    Dim sConsulta As String
    Dim rs As New adodb.Recordset

    '*************************************************************
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CServicio.DevolverIdFormCampo", "No se ha establecido la conexion"
    End If
    '*************************************************************
    
    sConsulta = "SELECT ID FROM FORM_CAMPO WITH (NOLOCK) WHERE GRUPO= " & iGrupoID & " AND SERVICIO=" & iServAntiguo & " AND BAJALOG=0 "
    rs.CursorLocation = adUseClient
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    If Not rs.eof Then
        DevolverIdFormCampo = rs.Fields("ID").Value
    End If
    rs.Close
    Set rs.ActiveConnection = Nothing

End Function

Public Function ActualizarFormCampo(ByVal iCampoID As Long) As TipoErrorSummit
    Dim sConsulta As String
    Dim TESError As TipoErrorSummit
    Dim oDen As CMultiidioma
    Dim sDen As String
    
    '********* Precondicion *******************
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CServicio.ActualizarFormCampo", "No se ha establecido la conexion"
    End If
    '******************************************
    
    On Error GoTo Error:
    TESError.NumError = TESnoerror
    'Formateo las DEN para la consulta
    If Not m_oDenominaciones Is Nothing Then
        For Each oDen In m_oDenominaciones
            sDen = sDen & "DEN_" & oDen.Cod & " = " & StrToSQLNULL(oDen.Den) & ","
        Next
        sDen = Left(sDen, Len(sDen) - 1) 'Quito la �ltima coma
    End If
    'Actualiza el campo en BD:
    sConsulta = "UPDATE FORM_CAMPO SET " & sDen & " WHERE ID=" & iCampoID
    m_oConexion.ADOCon.Execute sConsulta

    ActualizarFormCampo = TESError
    Exit Function
   
Error:
    ActualizarFormCampo = basErrores.TratarError(m_oConexion.ADOCon.Errors)

End Function

Public Function sacarIDGrupo(ByVal iCampoID As Long) As Long
    Dim sConsulta As String
    Dim rs As New adodb.Recordset

    '*************************************************************
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CServicio.sacarIDGrupo", "No se ha establecido la conexion"
    End If
    '*************************************************************
    
    If iCampoID = 0 Then
        sacarIDGrupo = 0
    Else
        sConsulta = "SELECT GRUPO FROM FORM_CAMPO WITH (NOLOCK) WHERE ID= " & iCampoID
        rs.CursorLocation = adUseClient
        rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
        If Not rs.eof Then
            sacarIDGrupo = rs.Fields("GRUPO").Value
        End If
        rs.Close
        Set rs.ActiveConnection = Nothing
    End If

End Function

Public Function DenCampoIdioma(ByVal iCampo As Long, ByVal sIdioma As String) As String
    Dim sConsulta As String
    Dim rs As New adodb.Recordset
    Dim sDen As String

    '*************************************************************
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CServicio.DenCampoIdioma", "No se ha establecido la conexion"
    End If
    '*************************************************************
   
    If iCampo = 0 Then
        DenCampoIdioma = ""
    Else
        sDen = "DEN_" & sIdioma
        sConsulta = "SELECT " & sDen & " FROM FORM_CAMPO WHERE ID=" & iCampo
        rs.CursorLocation = adUseClient
        rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
        If Not rs.eof Then
            DenCampoIdioma = rs.Fields(sDen).Value
        End If
        rs.Close
        Set rs.ActiveConnection = Nothing
    End If
    
End Function

Public Function DenGrupoIdioma(ByVal iGrupo As Long, ByVal sIdioma As String) As String
    Dim sConsulta As String
    Dim rs As New adodb.Recordset
    Dim sDen As String

    '*************************************************************
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CServicio.DenGrupoIdioma", "No se ha establecido la conexion"
    End If
    '*************************************************************
   
    If iGrupo = 0 Then
        DenGrupoIdioma = ""
    Else
        sDen = "DEN_" & sIdioma
        sConsulta = "SELECT " & sDen & " FROM FORM_GRUPO WHERE ID=" & iGrupo
        rs.CursorLocation = adUseClient
        rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
        If Not rs.eof Then
            DenGrupoIdioma = rs.Fields(sDen).Value
        End If
        rs.Close
        Set rs.ActiveConnection = Nothing
    End If
    
End Function

Public Function SacarServicio(ByVal iCampo As Long) As Integer
    Dim sConsulta As String
    Dim rs As New adodb.Recordset

    '*************************************************************
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CServicio.SacarServicio", "No se ha establecido la conexion"
    End If
    '*************************************************************
   
    sConsulta = "SELECT SERVICIO FROM FORM_CAMPO WHERE ID=" & iCampo
    rs.CursorLocation = adUseClient
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    If Not rs.eof Then
        SacarServicio = rs.Fields("SERVICIO").Value
    End If
    rs.Close
    Set rs.ActiveConnection = Nothing
    
End Function

Private Sub Class_Terminate()
    Set m_oDenominaciones = Nothing
    Set m_oConexion = Nothing
End Sub


