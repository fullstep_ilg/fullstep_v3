VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CUnidadOrgNivel0"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
'********************* CUnidadOrgNivel0 **********************************
'*             Autor : Javier Arana
'*             Creada : 27/7/98
'* **************************************************************


Option Explicit

Implements IUon

' *********************** Variables privadas de la clase *********+
Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

Private mvarConexion As CConexion
Private mvarUnidadesOrgNivel1 As CUnidadesOrgNivel1
Private mvarDepartamentos As CDepartamentos 'local copy
Private mvarConexionServer As CConexionDeUseServer
Private m_oEmpresa As CEmpresa

' *********************** Propiedades de la clase ******************

Friend Property Set Conexion(ByVal vData As CConexion)
    
    Set mvarConexion = vData
    
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = mvarConexion
End Property
Friend Property Get ConexionServer() As CConexionDeUseServer
Set ConexionServer = mvarConexionServer
End Property
Friend Property Set ConexionServer(ByVal con As CConexionDeUseServer)
Set mvarConexionServer = con
End Property
Public Property Set Departamentos(ByVal vData As CDepartamentos)
 Set mvarDepartamentos = vData
End Property

''' <summary>
''' Propiedad que indica el nivel de la UON (uso cuando tengo una colecci�n gen�rica de UONs para y evitar comprobaci�n de tipos)
''' </summary>
Public Property Get Nivel() As Integer
    Nivel = 0
End Property
Public Property Get titulo() As String
    titulo = gParametrosGenerales.gsDEN_UON0
End Property
'''<summary>
'''indica si la uon actual contiene (o es ella misma) a la unidad pasada por par�metro, tipo de uon tiene que ser CUon1,CUon2 o CUon3
'''</summary>
Public Function incluye(ByRef uon As Variant) As Boolean
    incluye = True
End Function
'''<summary>
'''indica si la uon actual est� contenida (o es ella misma) en la unidad pasada por par�metro, tipo de uon tiene que ser CUon1,CUon2 o CUon3
'''</summary>
Public Function perteneceA(ByRef uon As Variant) As Boolean
    perteneceA = (uon.Nivel = 0)
End Function
'<summary>Devuelve clave para utilizar como �ndice en colecciones</summary>
Public Function Key() As String
    Key = "UON0"
End Function

Public Property Get CodUnidadOrgNivel1() As Variant
    CodUnidadOrgNivel1 = ""
End Property

Public Property Get CodUnidadOrgNivel2() As Variant
    CodUnidadOrgNivel2 = ""
End Property

Public Property Get CodUnidadOrgNivel3() As Variant
    CodUnidadOrgNivel3 = ""
End Property

Public Property Get CodUnidadOrgNivel4() As Variant
    CodUnidadOrgNivel4 = ""
End Property

Public Property Get Departamentos() As CDepartamentos
    Set Departamentos = mvarDepartamentos
End Property

Public Property Get idEmpresa() As Variant
    idEmpresa = ""
End Property

Public Property Let idEmpresa(ByVal vEmpr As Variant)
    'No se puede asignar empresa a uon 0
End Property

Public Property Set UnidadesOrgNivel1(ByVal vData As CUnidadesOrgNivel1)
        Set mvarUnidadesOrgNivel1 = vData
End Property


Public Property Get UnidadesOrgNivel1() As CUnidadesOrgNivel1
     Set UnidadesOrgNivel1 = mvarUnidadesOrgNivel1
End Property

' ********************Funciones publicas de la clase ************




'--<summary>
'--Carga las unidades organizativas de nivel 1
'--</summary>
'--<param name="UON1">UON1</param>
'--<param name="UON2">UON2</param>
'--<param name="UON3">UON3</param>
'--<param name="RUO">RUO</param>
'--<param name="RUODep">RUODep</param>
'--<param name="RDepAsoc">RDepAsoc</param>
'--<param name="CarIniCod">CarIniCod</param>
'--<param name="CarIniDen">CarIniDen</param>
'--<param name="CoincidenciaTotal">CoincidenciaTotal</param>
'--<param name="OrdenadasPorDen">OrdenadasPorDen</param>
'--<param name="UsarIndice">UsarIndice</param>
'--<revision>DPD 08/11/2011</revision>

 Public Sub CargarTodasLasUnidadesOrgNivel1(Optional ByVal UON1 As Variant, Optional ByVal UON2 As Variant, Optional ByVal UON3 As Variant, Optional ByVal RUO As Boolean = False, Optional ByVal RUODep As Boolean, Optional ByVal RDepAsoc As Boolean, Optional ByVal CarIniCod As String, Optional ByVal CarIniDen As String, Optional ByVal CoincidenciaTotal As Boolean, Optional ByVal OrdenadasPorDen As Boolean = False, Optional ByVal UsarIndice As Boolean = False)

Dim rs As New adodb.Recordset
Dim sConsulta As String
Dim sCad As String
Dim lIndice As Long
Dim fldCod As adodb.Field
Dim fldDen As adodb.Field

'********* Precondicion **************************************
If mvarConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CGrupoDevolverTodasLasUnidadesOrgNivel1", "No se ha establecido la conexion"
    Exit Sub
End If
'*************************************************************


If Not IsMissing(UON1) Then
    If IsEmpty(UON1) Then
        UON1 = Null
    End If
    If Trim(UON1) = "" Then
        UON1 = Null
    End If
Else
    UON1 = Null
End If

If Not IsMissing(UON2) Then
    If IsEmpty(UON2) Then
        UON2 = Null
    End If
    If Trim(UON2) = "" Then
        UON2 = Null
    End If
Else
    UON2 = Null
End If

If Not IsMissing(UON3) Then
    If IsEmpty(UON3) Then
        UON3 = Null
    End If
    If Trim(UON3) = "" Then
        UON3 = Null
    End If
Else
    UON3 = Null
End If


If Not RUO And Not RUODep And Not RDepAsoc Then
    
    ' No hay restricciones
    sConsulta = "SELECT UON1.COD AS UON1COD, UON1.DEN as UON1DEN FROM UON1 WITH(NOLOCK) "
    
Else
    
    If IsNull(UON1) Then
        'Es del nivel 0 , o sea que como si no hubiese restricciones
        sConsulta = "SELECT UON1.COD AS UON1COD, UON1.DEN as UON1DEN FROM UON1 WITH(NOLOCK) "
    Else ' Es del nivel 1 o 2 o 3, asi que cargamos solo su unidad
        CarIniCod = UON1
        CoincidenciaTotal = True
        sConsulta = "SELECT UON1.COD AS UON1COD, UON1.DEN as UON1DEN FROM UON1 WITH(NOLOCK) "
    End If

End If

If CarIniCod <> "" Then
    sCad = "1"
Else
    sCad = "0"
End If

If CarIniDen <> "" Then
    sCad = sCad & "1"
Else
    sCad = sCad & "0"
End If
    
    Select Case sCad
            
    Case "01"
            
            If CoincidenciaTotal Then
                sConsulta = sConsulta & "WHERE UON1DEN = '" & DblQuote(CarIniDen) & "'"
            Else
                sConsulta = sConsulta & "AND UON1DEN LIKE '" & DblQuote(CarIniDen) & "%'"
            End If
                      
    Case "10"
            If CoincidenciaTotal Then
                sConsulta = sConsulta & "AND UON1COD = '" & DblQuote(CarIniCod) & "'"
            Else
                sConsulta = sConsulta & "AND UON1COD LIKE '" & DblQuote(CarIniCod) & "%'"
            End If
                   
    Case "11"
            If CoincidenciaTotal Then
                sConsulta = sConsulta & "AND UON1COD = '" & DblQuote(CarIniCod) & "'"
                sConsulta = sConsulta & "AND UON1DEN = '" & DblQuote(CarIniDen) & "'"
            Else
                sConsulta = sConsulta & "AND UON1COD LIKE '" & DblQuote(CarIniCod) & "%'"
                sConsulta = sConsulta & "AND UON1DEN LIKE '" & DblQuote(CarIniDen) & "%'"
            End If
    End Select
    
If OrdenadasPorDen Then
    sConsulta = sConsulta & " ORDER BY UON1DEN"
Else
    sConsulta = sConsulta & " ORDER BY UON1COD"
End If

rs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
      
If rs.eof Then
        
    rs.Close
    Set rs = Nothing
    Set mvarUnidadesOrgNivel1 = Nothing
    Set mvarUnidadesOrgNivel1 = New CUnidadesOrgNivel1
    Set mvarUnidadesOrgNivel1.Conexion = mvarConexion
    Set mvarUnidadesOrgNivel1.ConexionServer = mvarConexionServer
    
    Exit Sub
      
Else
    
    Set mvarUnidadesOrgNivel1 = Nothing
    Set mvarUnidadesOrgNivel1 = New CUnidadesOrgNivel1
    Set mvarUnidadesOrgNivel1.Conexion = mvarConexion
    Set mvarUnidadesOrgNivel1.ConexionServer = mvarConexionServer
    
    Set fldCod = rs.Fields("COD")
    Set fldDen = rs.Fields("DEN")
    
    If UsarIndice Then
        lIndice = 0
        
        While Not rs.eof
                
            mvarUnidadesOrgNivel1.Add fldCod.Value, fldDen.Value, lIndice
            rs.MoveNext
            lIndice = lIndice + 1
            
        Wend
    Else
        
        While Not rs.eof
            mvarUnidadesOrgNivel1.Add fldCod.Value, fldDen.Value
            rs.MoveNext
            
        Wend
    End If
    
    rs.Close
    Set rs = Nothing
    Set fldCod = Nothing
    Set fldDen = Nothing
    Exit Sub
      
End If
        
End Sub

''' <summary>
''' Carga el objeto empresa de la instalaci�n
''' </summary>
''' <returns>Objeto empresa de la instalaci�n cargado</returns>
''' <remarks>Llamada desde: Iuon_Empresa ; Tiempo m�ximo: 0,2</remarks>
''' Revisado por: jbg; Fecha: 11/08/2015
Public Function CargarEmpresa2() As CEmpresa
    Dim sConsulta As String
    Dim adoRs As adodb.Recordset
    
    sConsulta = "SELECT TOP 1 E.ID,E.DEN,E.NIF,E.CP,E.PAI,E.PROVI,E.DIR,E.POB,E.ERP ,E.SOCIEDAD,ES.ERP IDERP "
    sConsulta = sConsulta & " FROM EMP E WITH(NOLOCK) INNER JOIN UON1 U1 WITH(NOLOCK) ON E.ID=U1.EMPRESA "
    sConsulta = sConsulta & " LEFT JOIN ERP_SOCIEDAD ES WITH(NOLOCK) ON E.SOCIEDAD=ES.SOCIEDAD"

    Set adoRs = New adodb.Recordset
    adoRs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly
    If adoRs.eof Then
        adoRs.Close
        Set m_oEmpresa = Nothing
    Else
        Set m_oEmpresa = New CEmpresa
        With m_oEmpresa
            Set .Conexion = mvarConexion
            .Id = adoRs.Fields("ID").Value
            .Den = adoRs.Fields("DEN").Value
            .NIF = adoRs.Fields("NIF").Value
            .CP = adoRs.Fields("CP").Value
            .CodPais = adoRs.Fields("PAI").Value
            .CodProvi = adoRs.Fields("PROVI").Value
            .Direccion = adoRs.Fields("DIR").Value
            .Poblacion = adoRs.Fields("POB").Value
            .erp = (adoRs.Fields("ERP").Value = 1)
            .Sociedad = adoRs.Fields("SOCIEDAD").Value
            .IdErp = NullToStr(adoRs.Fields("IDERP").Value)
        End With
        adoRs.Close
        
    End If
    Set adoRs = Nothing
    
    Set CargarEmpresa2 = m_oEmpresa

End Function

Private Sub Class_Terminate()
    Set mvarConexion = Nothing
    Set mvarConexionServer = Nothing
End Sub

Public Sub CargarTodosLosDepartamentos(Optional ByVal CarIniCod As String, Optional ByVal CarIniDen As String, Optional ByVal CoincidenciaTotal As Boolean, Optional ByVal OrdenadosPorDen As Boolean = False, Optional ByVal UsarIndice As Boolean = False)

Dim rs As New adodb.Recordset
Dim sConsulta As String
Dim sCad As String
Dim lIndice As Long
Dim fldCod As adodb.Field
Dim fldDen As adodb.Field

'********* Precondicion **************************************
If mvarConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CUnidadeOrgNivel0CargarTodosLosDep", "No se ha establecido la conexion"
    Exit Sub
End If
'*************************************************************

If CarIniCod <> "" Then
    sCad = "1"
Else
    sCad = "0"
End If

If CarIniDen <> "" Then
    sCad = sCad & "1"
Else
    sCad = sCad & "0"
End If

        sConsulta = "SELECT DEP.COD,DEP.DEN"
        sConsulta = sConsulta & " FROM DEP WITH (NOLOCK)"
        sConsulta = sConsulta & " INNER JOIN UON0_DEP WITH (NOLOCK) ON UON0_DEP.DEP = DEP.COD "
    

    Select Case sCad
            
    Case "01"
            
            If CoincidenciaTotal Then
                sConsulta = sConsulta & " AND DEP.DEN = '" & DblQuote(CarIniDen) & "'"
            Else
                sConsulta = sConsulta & " AND DEP.DEN LIKE '" & DblQuote(CarIniDen) & "%'"
            End If
                      
    Case "10"
            If CoincidenciaTotal Then
                sConsulta = sConsulta & " AND DEP.COD = '" & DblQuote(CarIniCod) & "'"
            Else
                sConsulta = sConsulta & " AND DEP.COD LIKE '" & DblQuote(CarIniCod) & "%'"
            End If
                   
    Case "11"
            If CoincidenciaTotal Then
                sConsulta = sConsulta & " AND DEP.COD = '" & DblQuote(CarIniCod) & "'"
                sConsulta = sConsulta & " AND DEP.DEN = '" & DblQuote(CarIniDen) & "'"
            Else
                sConsulta = sConsulta & " AND DEP.COD LIKE '" & DblQuote(CarIniCod) & "%'"
                sConsulta = sConsulta & " AND DEP.DEN LIKE '" & DblQuote(CarIniDen) & "%'"
            End If
    End Select

If OrdenadosPorDen Then
    sConsulta = sConsulta & " ORDER BY DEP.DEN"
Else
    sConsulta = sConsulta & " ORDER BY DEP.COD"
End If
      
rs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
      
If rs.eof Then
        
    rs.Close
    Set rs = Nothing
    Set mvarDepartamentos = Nothing
    Set mvarDepartamentos = New CDepartamentos
    Set mvarDepartamentos.Conexion = mvarConexion
    Set mvarDepartamentos.ConexionServer = mvarConexionServer
    Exit Sub
      
Else
         
    Set mvarDepartamentos = Nothing
    Set mvarDepartamentos = New CDepartamentos
    Set mvarDepartamentos.Conexion = mvarConexion
    Set mvarDepartamentos.ConexionServer = mvarConexionServer
    
    Set fldCod = rs.Fields("COD")
    Set fldDen = rs.Fields("DEN")
    
    If UsarIndice Then
        
        lIndice = 0
    
        While Not rs.eof
    
            mvarDepartamentos.Add rs.Fields("COD"), rs.Fields("DEN"), lIndice
            rs.MoveNext
            lIndice = lIndice + 1
            
        Wend
    Else
         While Not rs.eof
    
            mvarDepartamentos.Add rs.Fields("COD"), rs.Fields("DEN")
            rs.MoveNext
            
        Wend
    
    End If
    
    rs.Close
    Set rs = Nothing
    Set fldCod = Nothing
    Set fldDen = Nothing
    Exit Sub
      
End If
          
End Sub

'''<summary>Compara dos unidades organizativas</summary>
Public Function equals(ByVal oUON As IUon) As Boolean
    equals = False
    If oUON.Nivel = 0 Then
        equals = True
    End If
End Function

'<summary>Comprueba si la Uon o alguna de sus descendientes tiene valores para un determinado art�culo en alg�n atributo</summary>
Public Function tieneAtributoValoradoArticulo(ByVal idArticulo As String) As Boolean

    Dim sConsulta As String
    Dim adoRs As adodb.Recordset
    Dim sWhere As String
    
        
    '********* Precondicion **************************************
    If mvarConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CUnidadeOrgNivel0.tieneAtributoValoradoArticulo", "No se ha establecido la conexion"
        Exit Function
    End If
    '*************************************************************
        
    sConsulta = " select COUNT(ART) C"
    sConsulta = sConsulta & " FROM ART4_UON_ATRIB AUA"
    sConsulta = sConsulta & " WHERE ART = '" & DblQuote(idArticulo) & "' "

    
    Set adoRs = New adodb.Recordset
    adoRs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly
    tieneAtributoValoradoArticulo = False
    If Not adoRs.eof Then
        tieneAtributoValoradoArticulo = (adoRs("C").Value = 1)
    End If
    adoRs.Close
    Set adoRs = Nothing
End Function



Public Property Let IUon_SentidoIntegracion(ByVal Value As SentidoIntegracion)
     
End Property
Public Property Get IUon_SentidoIntegracion() As SentidoIntegracion
     IUon_SentidoIntegracion = 0
End Property

Public Property Get IUon_Titulo(Optional sSeparadorCod As String = "/", Optional sSeparadorDen As String = "-", Optional bIncluirDen As Boolean = True) As String
    IUon_Titulo = Me.titulo
End Property

Public Property Get IUon_Den() As String
    IUon_Den = Me.titulo
End Property

Public Property Let IUon_Den(Value As String)
    
End Property

Public Property Get IUon_CodUnidadOrgNivel1() As String
    IUon_CodUnidadOrgNivel1 = ""
End Property

Public Property Let IUon_CodUnidadOrgNivel1(Value As String)
    
End Property

Public Property Get IUon_CodUnidadOrgNivel2() As String
    IUon_CodUnidadOrgNivel2 = ""
End Property

Public Property Let IUon_CodUnidadOrgNivel2(Value As String)
    
End Property

Public Property Get IUon_CodUnidadOrgNivel3() As String
    IUon_CodUnidadOrgNivel3 = ""
End Property

Public Property Let IUon_CodUnidadOrgNivel3(Value As String)
    
End Property

Public Property Get IUon_CodUnidadOrgNivel4() As String
    IUon_CodUnidadOrgNivel4 = ""
End Property

Public Property Let IUon_CodUnidadOrgNivel4(Value As String)
    
End Property

''' <summary>
''' Propiedad que indica el nivel de la UON (uso cuando tengo una colecci�n gen�rica de UONs para y evitar comprobaci�n de tipos)
''' </summary>
Public Property Get IUon_Nivel() As Integer
    IUon_Nivel = Nivel
End Property

'''<summary>
'''indica si la uon actual contiene a la unidad pasada por par�metro, tipo de uon tiene que ser CUon1,CUon2 o CUon3
'''</summary>
Public Function IUon_incluye(ByRef uon As Variant) As Boolean
    IUon_incluye = incluye(uon)
End Function

'''<summary>
'''indica si la uon actual est� contenida (o es ella misma) en la unidad pasada por par�metro, tipo de uon tiene que ser CUon1,CUon2 o CUon3
'''</summary>
Public Function IUon_perteneceA(ByRef uon As Variant) As Boolean
    IUon_perteneceA = Me.perteneceA(uon)
End Function

'<summary>Devuelve clave para utilizar como �ndice en colecciones</summary>
Public Function IUon_Key() As String
    IUon_Key = Key
End Function

'<summary>Comprueba si la Uon o alguna de sus descendientes tiene valor para un determinado atributo</summary>
Public Function IUon_tieneAtributoValorado(ByVal IDAtributo As Integer) As Boolean
    IUon_tieneAtributoValorado = False
End Function

'<summary>Comprueba si la Uon o alguna de sus descendientes tiene valores para un determinado art�culo en alg�n atributo</summary>
Public Function IUon_tieneAtributoValoradoArticulo(ByVal idArticulo As String) As Boolean
    IUon_tieneAtributoValoradoArticulo = False
End Function

'''<summary>Compara dos unidades organizativas</summary>
Public Function IUon_equals(ByVal oUON As IUon) As Boolean
    IUon_equals = equals(oUON)
End Function

Public Function IUon_clonar() As IUon
    Set IUon_clonar = New CUnidadOrgNivel0
    IUon_clonar.Den = Me.titulo
End Function


Public Property Get IUon_IdEmpresa() As Variant
    IUon_IdEmpresa = 0
End Property

Public Property Let IUon_IdEmpresa(ByVal vEmpr As Variant)
    
End Property

Public Property Set Iuon_Empresa(ByVal oEmp As CEmpresa)
    Set m_oEmpresa = oEmp
End Property
Public Property Get Iuon_Empresa() As CEmpresa
    If m_oEmpresa Is Nothing Then
        Set m_oEmpresa = CargarEmpresa2
    End If
    Set Iuon_Empresa = m_oEmpresa
End Property

Public Property Get IUon_Conexion() As CConexion
    Set IUon_Conexion = mvarConexion
End Property

Public Property Set IUon_Conexion(ByVal Value As CConexion)
    Set mvarConexion = Value
End Property

Private Sub IUon_CargarTodosLosDepartamentos(Optional ByVal CarIniCod As String, Optional ByVal CarIniDen As String, Optional ByVal CoincidenciaTotal As Boolean, Optional ByVal OrdenadosPorDen As Boolean = False, Optional ByVal UsarIndice As Boolean = False)
    Me.CargarTodosLosDepartamentos CarIniCod, CarIniDen, CoincidenciaTotal, OrdenadosPorDen, UsarIndice
End Sub

Private Property Get IUon_Departamentos() As CDepartamentos
    Set IUon_Departamentos = Me.Departamentos
End Property


