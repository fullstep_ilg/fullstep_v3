VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CConexion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum

Private mvarADOCon  As adodb.Connection

Private Const m_sFrms_TratarError As String = "FRMPROCE"
'Private mvarADOErs As adodb.Errors

Private Declare Function DestroyWindow Lib "user32" (ByVal hwnd As Long) As Long
Private Declare Function IsWindowEnabled Lib "user32" (ByVal hwnd As Long) As Long
Private Declare Function MoveWindow Lib "user32" (ByVal hwnd As Long, ByVal X As Long, ByVal Y As Long, _
                         ByVal nWidth As Long, ByVal nHeight As Long, ByVal bRepaint As Long) As Long

Friend Property Set ADOCon(ByVal vData As adodb.Connection)
    Set mvarADOCon = vData
End Property


Friend Property Get ADOCon() As adodb.Connection
    Set ADOCon = mvarADOCon
End Property



Private Sub Class_Terminate()

Dim i As Integer

Set mvarADOCon = Nothing

For i = 1 To 5
    i = DesactivarConexion(i)
Next i

End Sub

''' <summary>
''' Grabar un error en una comunicaci�n
''' </summary>
''' <param name="Pagina">Funci�n o pantalla q da el error</param>
''' <param name="NumError">Numero de Error VB. El Id de la tabla va en teserror.arg1</param>
''' <param name="Message">Mensaje del error VB</param>
''' <returns>Error de haberlo</returns>
''' <remarks>Llamada desde: frmAdjAnya/cmdAceptar_Click frmObjAnya/cmdAceptar_Click frmOfePetAnya/cmdAceptar_Click
'''     basUtilidades/EnviarMensaje     basUtilidades/GrabarEnviarMensaje ; Tiempo m�ximo: 0,2</remarks>
''' Revisado por: jbg; Fecha: 13/06/2013
Public Function GrabarError(ByVal Pagina As String, ByVal NumError As String, ByVal Message As String) As TipoErrorSummit
    Dim TESError As TipoErrorSummit
    Dim rs As adodb.Recordset
    
    '********* Precondicion *******************
    If ADOCon Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CConexion.GrabarError", "No se ha establecido la conexion"
        Exit Function
    End If
    
    On Error GoTo ERROR:

    TESError.NumError = TESnoerror
    ADOCon.Execute "BEGIN TRANSACTION"
        
    Pagina = "GS: " & Pagina
        
    ADOCon.Execute "INSERT INTO ERRORES (FEC_ALTA,PAGINA,USUARIO,EX_FULLNAME,EX_MESSAGE)" _
        & " VALUES(GETDATE()," & StrToSQLNULL(Pagina) & "," & StrToSQLNULL(basParametros.g_sCodUsuConectado) _
        & "," & StrToSQLNULL(NumError) & "," & StrToSQLNULL(Message) & ")"
        
    If ADOCon.Errors.Count > 0 Then GoTo ERROR
    
    Set rs = New adodb.Recordset
    'Buscamos el numero de id maximo
    'CALIDAD: Sin WITH (NOLOCK) porque es para obtener el nuevo ID
    rs.Open "SELECT MAX(ID) AS NUM FROM ERRORES", ADOCon, adOpenForwardOnly, adLockReadOnly
    If rs.eof Then
        TESError.NumError = TESDatoEliminado
        ADOCon.Execute "ROLLBACK TRANSACTION"
        rs.Close
        Set rs = Nothing
        GrabarError = TESError
        Exit Function
    Else
        TESError.Arg1 = NullToDbl0(rs(0).Value)
        TESError.Arg2 = Message
    End If
    
    rs.Close
    Set rs = Nothing
    
    ADOCon.Execute "COMMIT TRANSACTION"
    
    GrabarError = TESError
    
    Exit Function
    
ERROR:
    GrabarError = basErrores.TratarError(ADOCon.Errors)
    
    ADOCon.Execute "ROLLBACK TRANSACTION"
    
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
End Function

Public Sub BEGINTRANSACTION()
    mvarADOCon.Execute "BEGIN TRANSACTION"
    mvarADOCon.Execute "SET XACT_ABORT ON"
End Sub

Public Sub BEGINDISTRIBUTEDTRANSACTION()
    mvarADOCon.Execute "BEGIN DISTRIBUTED TRAN"
    mvarADOCon.Execute "SET XACT_ABORT ON"
End Sub

Public Sub COMMITTRANSACTION()
    mvarADOCon.Execute "COMMIT TRAN"
    mvarADOCon.Execute "SET XACT_ABORT OFF"
End Sub

Public Sub ROLLBACKTRANSACTION()
    mvarADOCon.Execute "IF @@TRANCOUNT>0 ROLLBACK TRANSACTION"
    mvarADOCon.Execute "SET XACT_ABORT OFF"
End Sub
