VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CPMConfCumplimentacion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements IBaseDatos

Private m_lIndice As Long

Private m_lBloque As Long
Private m_lRol As Long
Private m_lCampo As Long

Private m_oCampo As CFormItem
Private m_bIntro As Boolean

Private m_bVisible As Boolean
Private m_bEscritura As Boolean
Private m_bObl As Boolean
Private m_iOrden As Integer
Private m_vFecAct As Variant
Private m_bEscrituraFormula As Boolean
Private m_bPermitirMover As Boolean

Private m_vCodAtrib As Variant

''' Conexion
Private m_oConexion As CConexion

''' Control de errores
Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum

Public Property Get Bloque() As Long
    Bloque = m_lBloque
End Property
Public Property Let Bloque(ByVal Value As Long)
    m_lBloque = Value
End Property

Public Property Get Rol() As Long
    Rol = m_lRol
End Property
Public Property Let Rol(ByVal Value As Long)
    m_lRol = Value
End Property

Public Property Get IdCampo() As Long
    IdCampo = m_lCampo
End Property
Public Property Let IdCampo(ByVal Value As Long)
    m_lCampo = Value
End Property

Public Property Get Campo() As CFormItem
    Set Campo = m_oCampo
End Property
Public Property Set Campo(ByVal Value As CFormItem)
    Set m_oCampo = Value
End Property

Public Property Get Intro() As Boolean
    Intro = m_bIntro
End Property
Public Property Let Intro(ByVal bIntro As Boolean)
    m_bIntro = bIntro
End Property

Public Property Get Visible() As Boolean
    Visible = m_bVisible
End Property
Public Property Let Visible(ByVal bVisible As Boolean)
    m_bVisible = bVisible
End Property

Public Property Get Escritura() As Boolean
    Escritura = m_bEscritura
End Property
Public Property Let Escritura(ByVal bEscr As Boolean)
    m_bEscritura = bEscr
End Property

Public Property Get Obligatorio() As Boolean
    Obligatorio = m_bObl
End Property
Public Property Let Obligatorio(ByVal bObl As Boolean)
    m_bObl = bObl
End Property

Public Property Get Orden() As Integer
    Orden = m_iOrden
End Property
Public Property Let Orden(ByVal Value As Integer)
    m_iOrden = Value
End Property

Public Property Get Indice() As Long
    Indice = m_lIndice
End Property

Public Property Let Indice(ByVal lInd As Long)
    m_lIndice = lInd
End Property

Public Property Let FecAct(ByVal vData As Variant)
    m_vFecAct = vData
End Property

Public Property Get FecAct() As Variant
    FecAct = m_vFecAct
End Property

Public Property Let CodAtrib(ByVal vData As Variant)
    m_vCodAtrib = vData
End Property
Public Property Get CodAtrib() As Variant
    CodAtrib = m_vCodAtrib
End Property

Public Property Get EscrituraFormula() As Boolean
    EscrituraFormula = m_bEscrituraFormula
End Property
Public Property Let EscrituraFormula(ByVal bEscrituraFormula As Boolean)
    m_bEscrituraFormula = bEscrituraFormula
End Property
Friend Property Set Conexion(ByVal vData As CConexion)
    Set m_oConexion = vData
End Property

Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property

Private Sub Class_Terminate()
    Set m_oCampo = Nothing
    Set m_oConexion = Nothing
End Sub

Private Function IBaseDatos_AnyadirABaseDatos() As TipoErrorSummit
    'No se usa
End Function

Private Function IBaseDatos_CambiarCodigo(CodigoNuevo As Variant) As TipoErrorSummit
    'No se usa
End Function

Private Sub IBaseDatos_CancelarEdicion()
    'No se usa
End Sub

Private Function IBaseDatos_ComprobarExistenciaEnBaseDatos() As Boolean
    'No se usa
End Function

Private Function IBaseDatos_EliminarDeBaseDatos() As TipoErrorSummit
    'No se usa
End Function

Private Function IBaseDatos_FinalizarEdicionEliminando() As TipoErrorSummit
    'No se usa
End Function

'Funci�n que graba un 1 en PREVIO_A_VINCULADO en el campo que este antes en el orden al campo "Desglose Vinculado",
'asi se sabra cuando hay que crear este campo al abrir la pantalla frmCumplDesglose si el desglose esta vinculado
Public Function GrabarPrevioDesgloseVinculado(ByVal iOrdenModif As Integer, ByVal lCampoPadre As Long) As TipoErrorSummit
Dim TESError As TipoErrorSummit
Dim btrans As Boolean
Dim sConsulta As String
Dim rs As ADODB.Recordset
Dim Orden As Integer

    TESError.NumError = TESnoerror

    ''' Precondicion
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CPMConfCumplimentacion.GrabarPrevioDesgloseVinculado", "No se ha establecido la conexion"
    End If
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo Error
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:

    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
    btrans = True
    Select Case iOrdenModif
    Case Is <> 0
        sConsulta = "SELECT FECACT,ORDEN FROM PM_CONF_CUMP_BLOQUE WHERE BLOQUE = " & m_lBloque & " AND ROL = " & m_lRol & " AND CAMPO = " & m_lCampo
    
        Set rs = New ADODB.Recordset
        rs.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
    
        If rs.eof Then
            rs.Close
            Set rs = Nothing
            TESError.NumError = TESDatoEliminado
            TESError.Arg1 = 183
            GrabarPrevioDesgloseVinculado = TESError
            m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
            Exit Function
        End If
        Orden = rs.Fields("ORDEN").Value
        
        sConsulta = "UPDATE PM_CONF_CUMP_BLOQUE SET PREVIO_A_VINCULADO=0 "
        sConsulta = sConsulta & " FROM PM_CONF_CUMP_BLOQUE C INNER JOIN DESGLOSE WITH (NOLOCK) ON DESGLOSE.CAMPO_PADRE=" & lCampoPadre & " AND DESGLOSE.CAMPO_HIJO=C.CAMPO"
        sConsulta = sConsulta & " AND C.BLOQUE = " & m_lBloque & " AND C.ROL = " & m_lRol & " AND PREVIO_A_VINCULADO=1"
        m_oConexion.ADOCon.Execute sConsulta
        If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
        
        sConsulta = "UPDATE PM_CONF_CUMP_BLOQUE SET PREVIO_A_VINCULADO=1 "
        sConsulta = sConsulta & " FROM PM_CONF_CUMP_BLOQUE C INNER JOIN DESGLOSE WITH (NOLOCK) ON DESGLOSE.CAMPO_PADRE=" & lCampoPadre & " AND DESGLOSE.CAMPO_HIJO=C.CAMPO"
        sConsulta = sConsulta & " AND C.BLOQUE = " & m_lBloque & " AND C.ROL = " & m_lRol & " AND ORDEN = " & Orden + (iOrdenModif)
        m_oConexion.ADOCon.Execute sConsulta
        If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
        
        m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
        btrans = False
    Case Else
        sConsulta = "SELECT FECACT FROM PM_CONF_CUMP_BLOQUE WHERE BLOQUE = " & m_lBloque & " AND ROL = " & m_lRol & " AND CAMPO = " & m_lCampo
    
        Set rs = New ADODB.Recordset
        rs.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
    
        If rs.eof Then
            rs.Close
            Set rs = Nothing
            TESError.NumError = TESDatoEliminado
            TESError.Arg1 = 183
            GrabarPrevioDesgloseVinculado = TESError
            m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
            Exit Function
        End If
        
        sConsulta = "UPDATE PM_CONF_CUMP_BLOQUE SET PREVIO_A_VINCULADO=0 "
        sConsulta = sConsulta & " FROM PM_CONF_CUMP_BLOQUE C INNER JOIN DESGLOSE WITH (NOLOCK) ON DESGLOSE.CAMPO_PADRE=" & lCampoPadre & " AND DESGLOSE.CAMPO_HIJO=C.CAMPO"
        sConsulta = sConsulta & " AND C.BLOQUE = " & m_lBloque & " AND C.ROL = " & m_lRol & " AND PREVIO_A_VINCULADO=1"
        m_oConexion.ADOCon.Execute sConsulta
        If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
        
        sConsulta = "UPDATE PM_CONF_CUMP_BLOQUE SET PREVIO_A_VINCULADO=1 "
        sConsulta = sConsulta & " FROM PM_CONF_CUMP_BLOQUE C INNER JOIN DESGLOSE WITH (NOLOCK) ON DESGLOSE.CAMPO_PADRE=" & lCampoPadre & " AND DESGLOSE.CAMPO_HIJO=C.CAMPO"
        sConsulta = sConsulta & " AND C.BLOQUE = " & m_lBloque & " AND C.ROL = " & m_lRol & " AND CAMPO = " & m_lCampo
        m_oConexion.ADOCon.Execute sConsulta
        If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
        
        m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
        btrans = False
    
    End Select
    
    rs.Requery
    m_vFecAct = rs(0).Value
    rs.Close
    Set rs = Nothing
    TESError.NumError = TESnoerror
    GrabarPrevioDesgloseVinculado = TESError

    Exit Function


Error_Cls:
    TESError = basErrores.TratarError(m_oConexion.ADOCon.Errors)

    If btrans Then
        m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
    End If

    Resume Salir:

Salir:

    On Error Resume Next
    If Not rs Is Nothing Then
        rs.Close
    End If

    GrabarPrevioDesgloseVinculado = TESError
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CPMConfCumplimentacion", "GrabarPrevioDesgloseVinculado", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If

End Function


Private Function IBaseDatos_FinalizarEdicionModificando() As TipoErrorSummit
Dim TESError As TipoErrorSummit
Dim btrans As Boolean
Dim sConsulta As String
Dim rs As ADODB.Recordset

    TESError.NumError = TESnoerror

    ''' Precondicion
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CPMConfCumplimentacion.FinalizarEdicionModificando", "No se ha establecido la conexion"
    End If
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo Error
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:

    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
    btrans = True

    sConsulta = "SELECT FECACT FROM PM_CONF_CUMP_BLOQUE WHERE BLOQUE = " & m_lBloque & " AND ROL = " & m_lRol & " AND CAMPO = " & m_lCampo
    
    Set rs = New ADODB.Recordset
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly

    If rs.eof Then
        rs.Close
        Set rs = Nothing
        TESError.NumError = TESDatoEliminado
        TESError.Arg1 = 183
        IBaseDatos_FinalizarEdicionModificando = TESError
        m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
        Exit Function
    End If

    sConsulta = "UPDATE PM_CONF_CUMP_BLOQUE SET VISIBLE=" & BooleanToSQLBinary(m_bVisible)
    sConsulta = sConsulta & ",ESCRITURA=" & BooleanToSQLBinary(m_bEscritura)
    sConsulta = sConsulta & ",OBLIGATORIO=" & BooleanToSQLBinary(m_bObl)
    'sConsulta = sConsulta & ",ORDEN=" & m_iOrden

    sConsulta = sConsulta & " WHERE BLOQUE = " & m_lBloque & " AND ROL = " & m_lRol & " AND CAMPO = " & m_lCampo

    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    
    sConsulta = "UPDATE PM_NOTIFICADO_ENLACE_CAMPOS SET VISIBLE=" & BooleanToSQLBinary(m_bVisible)
    sConsulta = sConsulta & " FROM PM_NOTIFICADO_ENLACE_CAMPOS PNEC "
    sConsulta = sConsulta & " INNER JOIN PM_NOTIFICADO_ENLACE PME WITH(NOLOCK) ON PME.ID=PNEC.NOTIFICADO AND PME.CONFIGURACION_ROL=1 AND PNEC.CAMPO = " & m_lCampo
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    
    sConsulta = "UPDATE PM_NOTIFICADO_ACCION_CAMPOS SET VISIBLE=" & BooleanToSQLBinary(m_bVisible)
    sConsulta = sConsulta & " FROM PM_NOTIFICADO_ACCION_CAMPOS PNEC "
    sConsulta = sConsulta & " INNER JOIN PM_NOTIFICADO_ACCION PME WITH(NOLOCK) ON PME.ID=PNEC.NOTIFICADO AND PME.CONFIGURACION_ROL=1 AND PNEC.CAMPO = " & m_lCampo
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    btrans = False
    
    rs.Requery
    m_vFecAct = rs(0).Value
    rs.Close
    Set rs = Nothing
    TESError.NumError = TESnoerror
    IBaseDatos_FinalizarEdicionModificando = TESError

    Exit Function


Error_Cls:
    TESError = basErrores.TratarError(m_oConexion.ADOCon.Errors)

    If btrans Then
        m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
    End If

    Resume Salir:

Salir:

    On Error Resume Next
    If Not rs Is Nothing Then
        rs.Close
    End If

    IBaseDatos_FinalizarEdicionModificando = TESError





'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CPMConfCumplimentacion", "IBaseDatos_FinalizarEdicionModificando", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If

End Function

Private Function IBaseDatos_IniciarEdicion(Optional ByVal Bloquear As Boolean, Optional ByVal UsuarioBloqueo As String) As TipoErrorSummit
    
End Function


Public Function ModificarCumplimentacionDef() As TipoErrorSummit
Dim TESError As TipoErrorSummit
Dim btrans As Boolean
Dim sConsulta As String

    TESError.NumError = TESnoerror

    ''' Precondicion
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CPMConfCumplimentacion.ModificarCumplimentacionDef", "No se ha establecido la conexion"
    End If
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo Error
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:

    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
    btrans = True

    sConsulta = "UPDATE PM_CONF_CUMP_ROL_DEF SET VISIBLE=" & BooleanToSQLBinary(m_bVisible)
    sConsulta = sConsulta & ",ESCRITURA=" & BooleanToSQLBinary(m_bEscritura)
    sConsulta = sConsulta & ",OBLIGATORIO=" & BooleanToSQLBinary(m_bObl)
    sConsulta = sConsulta & ",ORDEN=" & m_iOrden
    sConsulta = sConsulta & " WHERE ROL = " & m_lRol & " AND CAMPO = " & m_lCampo

    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    btrans = False
   
    TESError.NumError = TESnoerror
    ModificarCumplimentacionDef = TESError

    Exit Function


Error_Cls:
    TESError = basErrores.TratarError(m_oConexion.ADOCon.Errors)

    If btrans Then
        m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
    End If

    ModificarCumplimentacionDef = TESError

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CPMConfCumplimentacion", "ModificarCumplimentacionDef", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If

End Function

Public Function ModificarOrdenCumplimentacionDef() As TipoErrorSummit
Dim TESError As TipoErrorSummit
Dim btrans As Boolean
Dim sConsulta As String


    TESError.NumError = TESnoerror

    ''' Precondicion
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CPMConfCumplimentacion.ModificarOrdenCumplimentacionDef", "No se ha establecido la conexion"
    End If
    
    If Not g_oErrores.fg_bProgramando Then On Error GoTo Error
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:

    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
    btrans = True

    sConsulta = "UPDATE PM_CONF_CUMP_ROL_DEF SET ORDEN=" & m_iOrden
    sConsulta = sConsulta & " WHERE ROL = " & m_lRol & " AND CAMPO = " & m_lCampo

    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    btrans = False
   
    TESError.NumError = TESnoerror
    ModificarOrdenCumplimentacionDef = TESError

    Exit Function


Error_Cls:
    TESError = basErrores.TratarError(m_oConexion.ADOCon.Errors)

    If btrans Then
        m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
    End If

    ModificarOrdenCumplimentacionDef = TESError

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CPMConfCumplimentacion", "ModificarOrdenCumplimentacionDef", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If

End Function

''' <summary>
''' Indicar si se permiter mover lineas vinculadas
''' </summary>
''' <remarks>Llamada desde:frmFlujosCumplDesglose y frmFlujosConfVisibilidadDefDesglose; Tiempo m�ximo: 0</remarks>
Public Property Get PermitirMover() As Boolean
    PermitirMover = m_bPermitirMover
End Property
''' <summary>
''' Indicar si se permiter mover lineas vinculadas
''' </summary>
''' <remarks>Llamada desde:frmFlujosCumplDesglose y frmFlujosConfVisibilidadDefDesglose; Tiempo m�ximo: 0</remarks>
Public Property Let PermitirMover(ByVal bPermitirMover As Boolean)
    m_bPermitirMover = bPermitirMover
End Property
''' <summary>
''' Actualizar si se permiter mover lineas vinculadas o no.
''' </summary>
''' <returns>Error de haberlo</returns>
''' <remarks>Llamada desde: frmFlujosConfVisibilidadDefDesglose; Tiempo m�ximo:0,1</remarks>
Public Function ModificarMoverCumplimentacionDef() As TipoErrorSummit
Dim TESError As TipoErrorSummit
Dim btrans As Boolean
Dim sConsulta As String


    TESError.NumError = TESnoerror

    ''' Precondicion
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CPMConfCumplimentacion.ModificarMoverCumplimentacionDef", "No se ha establecido la conexion"
    End If
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo Error
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:

    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
    btrans = True

    sConsulta = "UPDATE PM_CONF_CUMP_ROL_DEF SET MOVER_LINEAS =" & BooleanToSQLBinary(m_bPermitirMover)
    sConsulta = sConsulta & " WHERE ROL = " & m_lRol & " AND CAMPO = " & m_lCampo

    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    btrans = False
   
    TESError.NumError = TESnoerror
    ModificarMoverCumplimentacionDef = TESError

    Exit Function
Error_Cls:
    TESError = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    If btrans Then
        m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
    End If
    ModificarMoverCumplimentacionDef = TESError
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CPMConfCumplimentacion", "ModificarMoverCumplimentacionDef", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If
End Function
''' <summary>
''' Actualizar si se permiter mover lineas vinculadas o no.
''' </summary>
''' <returns>Error de haberlo</returns>
''' <remarks>Llamada desde: frmFlujosCumplDesglose; Tiempo m�ximo:0,1</remarks>
Public Function ModificarMoverCumplimentacion() As TipoErrorSummit
Dim TESError As TipoErrorSummit
Dim btrans As Boolean
Dim sConsulta As String


    TESError.NumError = TESnoerror

    ''' Precondicion
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CPMConfCumplimentacion.ModificarMoverCumplimentacion", "No se ha establecido la conexion"
    End If
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo Error
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:

    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
    btrans = True

    sConsulta = "UPDATE PM_CONF_CUMP_BLOQUE SET MOVER_LINEAS =" & BooleanToSQLBinary(m_bPermitirMover)
    sConsulta = sConsulta & " WHERE BLOQUE = " & m_lBloque & " AND ROL = " & m_lRol & " AND CAMPO = " & m_lCampo
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    btrans = False
   
    TESError.NumError = TESnoerror
    ModificarMoverCumplimentacion = TESError

    Exit Function
Error_Cls:
    TESError = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    If btrans Then
        m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
    End If
    ModificarMoverCumplimentacion = TESError
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CPMConfCumplimentacion", "ModificarMoverCumplimentacion", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If
End Function
