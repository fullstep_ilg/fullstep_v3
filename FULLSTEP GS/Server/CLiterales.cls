VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CLiterales"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Private mCol As Collection

Public Function Add(ByVal Idioma As String, ByVal Den As String, ByVal ID As Integer) As CLiteral
    
    ''' * Objetivo: Anyadir una moneda a la coleccion
    ''' * Recibe: Datos de la moneda
    ''' * Devuelve: Moneda a�adida
    
    Dim objnewmember As CLiteral
    
    ''' Creacion de objeto moneda
    
    Set objnewmember = New CLiteral
   
    ''' Paso de los parametros al nuevo objeto moneda
    
    objnewmember.Idioma = Idioma
    objnewmember.Den = Den
    objnewmember.ID = ID
    
    
    mCol.Add objnewmember, Idioma & CStr(ID)
    
            
    Set Add = objnewmember
    
    Set objnewmember = Nothing

End Function
Public Property Get Item(vntIndexKey As Variant) As CLiteral

    ''' * Objetivo: Recuperar una moneda de la coleccion
    ''' * Recibe: Indice de la moneda a recuperar
    ''' * Devuelve: Moneda correspondiente
        
On Error GoTo NoSeEncuentra:

    ''' Devolvemos el item de la coleccion privada
    
    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:

    Set Item = Nothing
    
End Property

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"

    ''' ! Pendiente de revision, objetivo desconocido
    
    Set NewEnum = mCol.[_NewEnum]

End Property
Public Sub Remove(vntIndexKey As Variant)

    ''' * Objetivo: Eliminar una moneda de la coleccion
    ''' * Recibe: Indice de la moneda a eliminar
    ''' * Devuelve: Nada
   
    mCol.Remove vntIndexKey
    
End Sub
Private Sub Class_Initialize()

    ''' * Objetivo: Crear la coleccion
    
   
    Set mCol = New Collection
    
End Sub
Private Sub Class_Terminate()

    ''' * Objetivo: Limpiar memoria
    
    Set mCol = Nothing
        
End Sub

Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
     Count = mCol.Count
End If


End Property
