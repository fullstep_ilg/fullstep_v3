VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "ICompProveAsignados"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
Public Function DevolverCompradoresAsignados(Optional ByVal CodEqp As String, Optional ByVal bSoloNoBaja As Boolean) As CCompradores

End Function

Public Function NivelAsignadoComprador(ByVal CodEqp As String, ByVal CodCom As String)

End Function

Public Function NivelAsignadoProveedor(ByVal CodProve As String)

End Function
Public Function AsignarCompradores() As TipoErrorSummit

End Function
Public Function DesAsignarCompradores() As TipoErrorSummit

End Function
Public Property Set Compradores(ByVal oComps As CCompradores)

End Property
Public Property Get Compradores() As CCompradores

End Property

Public Function BuscarProveedoresDesde(ByVal NumMaximo As Integer, Optional ByVal CodEqp As String, Optional ByVal CarIniCod As String, Optional ByVal CarIniDen As String, Optional ByVal CodPais As String, Optional ByVal CodMoneda As String, Optional ByVal CodProvi As String, Optional ByVal Calif1 As String, Optional ByVal Calif2 As String, Optional ByVal Calif3 As String, Optional ByVal OrdenadosPorDen As Boolean = False, Optional ByVal OrdenadosPorCalif1 As Boolean = False, Optional ByVal OrdenadosPorCalif2 As Boolean = False, Optional ByVal OrdenadosPorCalif3 As Boolean = False, Optional ByVal UsarIndice As Boolean = False) As CProveedores

End Function

Public Function AsignarProveedores() As TipoErrorSummit

End Function
Public Function DesAsignarProveedores() As TipoErrorSummit

End Function
Public Property Set Proveedores(ByVal oComps As CProveedores)

End Property
Public Property Get Proveedores() As CProveedores

End Property


