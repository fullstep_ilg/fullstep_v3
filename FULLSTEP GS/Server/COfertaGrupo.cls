VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "COfertaGrupo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* COfertaGrupo ******************************
'*             Autor : Hilario Barrenkua
'*             Creada : 15/05/2002
'****************************************************************

Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

Private m_oOferta As COferta
Private m_oGrupo As CGrupo



' Lineas de oferta
Private m_oLineas As CPrecioItems

' Ficheros Adjuntos
Private m_oAdjuntos As CAdjuntos
Private m_vObsAdjun As Variant

Private m_vIndice As Variant
Private m_oConexion As CConexion
Private m_lNumAtribsOfer As Long
Private m_lNumAdjuntos As Long
Private m_lNumPrecios As Long
Private m_oAtribOfertados As CAtributosOfertados

Private m_vImporteBruto As Variant
Private m_vImporteNeto As Variant


Public Property Get Oferta() As COferta
    Set Oferta = m_oOferta
End Property

Public Property Set Oferta(ByRef oOferta As COferta)
    Set m_oOferta = oOferta
End Property

Public Property Get Grupo() As CGrupo
    Set Grupo = m_oGrupo
End Property

Public Property Set Grupo(ByRef oGrupo As CGrupo)
    Set m_oGrupo = oGrupo
End Property
Public Property Let NumAtribOfer(ByVal lSiz As Long)
    m_lNumAtribsOfer = lSiz
End Property
Public Property Get NumAtribOfer() As Long
    NumAtribOfer = m_lNumAtribsOfer
End Property
Public Property Let NumAdjuntos(ByVal lSiz As Long)
    m_lNumAdjuntos = lSiz
End Property
Public Property Get NumAdjuntos() As Long
    NumAdjuntos = m_lNumAdjuntos
End Property
Public Property Let NumPrecios(ByVal lSiz As Long)
    m_lNumPrecios = lSiz
End Property
Public Property Get NumPrecios() As Long
    NumPrecios = m_lNumPrecios
End Property

Public Property Get AtribOfertados() As CAtributosOfertados
    Set AtribOfertados = m_oAtribOfertados
End Property
Public Property Set AtribOfertados(ByVal oAtribs As CAtributosOfertados)
    Set m_oAtribOfertados = oAtribs
End Property

Public Property Let Indice(ByVal varIndice As Variant)
    m_vIndice = varIndice
End Property
Public Property Get Indice() As Variant
    Indice = m_vIndice
End Property

Public Property Let ObsAdjun(ByVal var As Variant)
    m_vObsAdjun = var
End Property
Public Property Get ObsAdjun() As Variant
    ObsAdjun = m_vObsAdjun
End Property

Public Property Get Lineas() As CPrecioItems
    Set Lineas = m_oLineas
End Property
Public Property Set Lineas(ByVal oLineas As CPrecioItems)
    Set m_oLineas = oLineas
End Property
Public Property Get Adjuntos() As CAdjuntos
    Set Adjuntos = m_oAdjuntos
End Property
Public Property Set Adjuntos(ByVal oAdjuntos As CAdjuntos)
    Set m_oAdjuntos = oAdjuntos
End Property

Public Property Let ImporteBruto(ByVal d As Variant)
    m_vImporteBruto = d
End Property
Public Property Get ImporteBruto() As Variant
    ImporteBruto = m_vImporteBruto
End Property

Public Property Let ImporteNeto(ByVal d As Variant)
    m_vImporteNeto = d
End Property
Public Property Get ImporteNeto() As Variant
    ImporteNeto = m_vImporteNeto
End Property

Friend Property Set Conexion(ByVal con As CConexion)
Set m_oConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = m_oConexion
End Property

Private Sub Class_Terminate()
    Set m_oConexion = Nothing
End Sub


Public Sub CargarAtributosOfertados()
Dim AdoRes As adodb.Recordset
Dim adoComm As adodb.Command
Dim adoParam As adodb.Parameter
Dim sConsulta As String

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:


sConsulta = "SELECT * " _
          & "  FROM OFE_GR_ATRIB OA WITH (NOLOCK) " _
          & " WHERE OA.ANYO= ? " _
          & "   AND OA.GMN1 = ? " _
          & "   AND OA.PROCE = ? " _
          & "   AND OA.GRUPO = ? " _
          & "   AND OA.PROVE = ? " _
          & "   AND OA.OFE = ? "
        
    
Set adoComm = New adodb.Command

Set adoComm.ActiveConnection = m_oConexion.ADOCon

adoComm.CommandText = sConsulta
adoComm.CommandType = adCmdText

With m_oOferta
    Set adoParam = adoComm.CreateParameter(Type:=adInteger, Direction:=adParamInput, Value:=.Anyo)
    adoComm.Parameters.Append adoParam
    
    Set adoParam = adoComm.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(.GMN1Cod), Value:=.GMN1Cod)
    adoComm.Parameters.Append adoParam
    
    Set adoParam = adoComm.CreateParameter(Type:=adInteger, Direction:=adParamInput, Value:=.Proce)
    adoComm.Parameters.Append adoParam
    
    Set adoParam = adoComm.CreateParameter(Type:=adSmallInt, Direction:=adParamInput, Value:=m_oGrupo.Id)
    adoComm.Parameters.Append adoParam
    
    Set adoParam = adoComm.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(.Prove), Value:=.Prove)
    adoComm.Parameters.Append adoParam
    
    Set adoParam = adoComm.CreateParameter(Type:=adInteger, Direction:=adParamInput, Value:=.Num)
    adoComm.Parameters.Append adoParam
End With
Set AdoRes = adoComm.Execute

Set m_oAtribOfertados = New CAtributosOfertados
Set m_oAtribOfertados.Conexion = m_oConexion

If Not AdoRes.eof Then
    While Not AdoRes.eof
        m_oAtribOfertados.Add IdAtribProce:=AdoRes("ATRIB_ID").Value, UltimaOferta:=m_oOferta, _
                              ValorBool:=AdoRes("VALOR_BOOL").Value, ValorFec:=AdoRes("VALOR_FEC").Value, _
                              ValorNum:=AdoRes("VALOR_NUM").Value, ValorText:=AdoRes("VALOR_TEXT").Value, _
                              vFecAct:=AdoRes("FECACT").Value
        AdoRes.MoveNext
    Wend

End If
fin:
On Error Resume Next
AdoRes.Close
Set AdoRes = Nothing

Set adoComm = Nothing
Error_Cls:

Resume fin
'Resume 0

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "COfertaGrupo", "CargarAtributosOfertados", ERR, Erl)
      GoTo Error_Cls
      Exit Sub
   End If

End Sub




Public Sub CargarAdjuntos()
Dim AdoRes As adodb.Recordset
Dim adoComm As adodb.Command
Dim adoParam As adodb.Parameter
Dim sConsulta As String

Dim lIndice As Long

'on error goto error



If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
Set adoComm = New adodb.Command

Set adoComm.ActiveConnection = m_oConexion.ADOCon

With m_oOferta
    Set adoParam = adoComm.CreateParameter(Type:=adInteger, Direction:=adParamInput, Value:=.Anyo)
    adoComm.Parameters.Append adoParam
    
    Set adoParam = adoComm.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(.GMN1Cod), Value:=.GMN1Cod)
    adoComm.Parameters.Append adoParam
    
    Set adoParam = adoComm.CreateParameter(Type:=adInteger, Direction:=adParamInput, Value:=.Proce)
    adoComm.Parameters.Append adoParam
    
    Set adoParam = adoComm.CreateParameter(Type:=adSmallInt, Direction:=adParamInput, Value:=m_oGrupo.Id)
    adoComm.Parameters.Append adoParam
    
    Set adoParam = adoComm.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(.Prove), Value:=.Prove)
    adoComm.Parameters.Append adoParam
    
    Set adoParam = adoComm.CreateParameter(Type:=adInteger, Direction:=adParamInput, Value:=.Num)
    adoComm.Parameters.Append adoParam
End With

Set adoComm.ActiveConnection = m_oConexion.ADOCon
adoComm.CommandText = "EXEC SP_DEVOLVER_ADJUNTOS @ANYO = ?, @GMN1 =?, @PROCE =?, @GRUPO =?, @PROVE =?, @OFE =?"
adoComm.CommandType = adCmdText

Set AdoRes = adoComm.Execute

Set m_oAdjuntos = New CAdjuntos
Set m_oAdjuntos.Conexion = m_oConexion
If Not AdoRes.eof Then

    lIndice = 0
    While Not AdoRes.eof
        m_oAdjuntos.Add Id:=AdoRes.Fields("ID").Value, Nombre:=AdoRes.Fields("NOM").Value, DataSize:=NullToDbl0(AdoRes("DATASIZE").Value), Comentario:=AdoRes.Fields("COM").Value, IDPortal:=AdoRes.Fields("IDPORTAL")
        lIndice = lIndice + 1

        AdoRes.MoveNext
    Wend


End If

AdoRes.Close


adoComm.CommandText = "EXEC SP_DEVOLVER_OBS_ADJUNTOS @ANYO=?, @GMN1=?, @PROCE=?,@GRUPO =?, @PROVE=?, @OFE=?"
Set AdoRes = adoComm.Execute
If AdoRes.Fields(0).ActualSize > 0 Then
    m_vObsAdjun = AdoRes.Fields(0).GetChunk(AdoRes.Fields(0).ActualSize)
End If
AdoRes.Close
Set AdoRes = Nothing
Set adoComm = Nothing
Set adoParam = Nothing

Exit Sub

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "COfertaGrupo", "CargarAdjuntos", ERR, Erl)
      Exit Sub
   End If

End Sub


''' <summary>
'''  Carga de los precios del grupo con/sin escalados
''' </summary>
''' <param optional name="bTodos">Si se sacan todos los �tems con independencia de su estado</param>
''' <param optional name="bUsaEscalados">
''' S�lo tiene sentido si el item tiene escalados. Indica si se sacan los precios y c/d por escalados o no
''' Si tiene escalados: True se cargan las estructuras de precios y c/d por escalados (por defecto). False se cargan las estructuras de precios y c/d sin escalados
''' Si no tiene escalados: se ignora el par�metro
''' </param>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: frmOFERec.cmdAceptar_Click; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 24/10/2011</revision>

Public Sub CargarPrecios(Optional ByVal bTodos As Boolean = True, Optional ByVal bUsaEscalados As Boolean = True)
Dim AdoRes As adodb.Recordset

Dim adoComm As adodb.Command
Dim adoParam As adodb.Parameter
Dim sConsulta As String
Dim oAtrib As CAtributo
Dim vValor As Variant
Dim oEscalado As CEscalado
Dim sProced As String
Dim bUsarEsc As Boolean
Dim vEscPresup As Variant

'on error goto error


If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
Set adoComm = New adodb.Command

Set adoComm.ActiveConnection = m_oConexion.ADOCon

bUsarEsc = False

If m_oGrupo.UsarEscalados And bUsaEscalados Then
    bUsarEsc = True
End If

If bUsarEsc Then
    sProced = "SP_ITEMS_ESC"
Else
    sProced = "SP_ITEMS"
End If
sConsulta = "EXEC " & sProced & " @ANYO=?, @GMN1=?, @PROCE=?, @GRUPO=?, @PROVE=?, @OFE=?, @TODOS=?, @IDIOMA=?"
With m_oOferta
    Set adoParam = adoComm.CreateParameter("ANYO", adSmallInt, adParamInput, , .Anyo)
    adoComm.Parameters.Append adoParam
    
    Set adoParam = adoComm.CreateParameter("GMN1", adVarChar, adParamInput, 50, .GMN1Cod)
    adoComm.Parameters.Append adoParam
    
    Set adoParam = adoComm.CreateParameter("PROCE", adInteger, adParamInput, , .Proce)
    adoComm.Parameters.Append adoParam
    
    Set adoParam = adoComm.CreateParameter("GRUPO", adSmallInt, adParamInput, , m_oGrupo.Id)
    adoComm.Parameters.Append adoParam
    
    Set adoParam = adoComm.CreateParameter("PROVE", adVarChar, adParamInput, 50, .Prove)
    adoComm.Parameters.Append adoParam
    
    Set adoParam = adoComm.CreateParameter("OFE", adSmallInt, adParamInput, , .Num)
    adoComm.Parameters.Append adoParam
    
    Set adoParam = adoComm.CreateParameter("TODOS", adInteger, adParamInput, , IIf(bTodos, 1, 0))
    adoComm.Parameters.Append adoParam
    
    Set adoParam = adoComm.CreateParameter("IDIOMA", adVarChar, adParamInput, 20, gParametrosInstalacion.gIdioma)
    adoComm.Parameters.Append adoParam
End With

adoComm.CommandType = adCmdText
adoComm.CommandText = sConsulta

Set AdoRes = adoComm.Execute

Set m_oLineas = New CPrecioItems
Set m_oLineas.Conexion = m_oConexion
Dim iAnyoImputacion As Integer
iAnyoImputacion = 0
If Not bUsaEscalados Then
    iAnyoImputacion = NullToDbl0(AdoRes("AnyoImputacion"))
End If
While Not AdoRes.eof
    m_oLineas.Add m_oOferta, AdoRes("ID").Value, AdoRes("DEST").Value, AdoRes("UNI").Value, AdoRes("PAG").Value, AdoRes("CANT").Value, AdoRes("FECINI").Value, _
                            AdoRes("FECFIN").Value, , AdoRes("CANTMAX").Value, AdoRes("ART").Value, AdoRes("DESCR").Value, AdoRes("PREC").Value, AdoRes("PRES").Value, , , _
                            AdoRes("EST").Value, AdoRes("PRECIO").Value, AdoRes("PRECIO2").Value, AdoRes("PRECIO3").Value, AdoRes("USAR").Value, m_oGrupo.Id, _
                            AdoRes("FECACT").Value, AdoRes("COMENT1").Value, AdoRes("COMENT2").Value, AdoRes("COMENT3").Value, NullToDbl0(AdoRes("NUMADJUN").Value), _
                            AdoRes("OBSADJUN").Value, m_oGrupo.Codigo, iAnyoImputacion
    
    Dim oPrecio As CPrecioItem
    Set oPrecio = m_oLineas.Item(CStr(AdoRes("ID").Value))
    If bUsarEsc Then
        Set oPrecio.Escalados = New CEscalados
        Set oPrecio.Escalados.Conexion = m_oConexion
        m_oGrupo.CargarEscalados
        oPrecio.Escalados.Modo = m_oGrupo.Escalados.Modo
        For Each oEscalado In m_oGrupo.Escalados
            If IsNull(AdoRes("PRES_" & oEscalado.Id).Value) Then
                vEscPresup = Null
            Else
                vEscPresup = CDec(AdoRes("PRES_" & oEscalado.Id).Value * m_oOferta.Cambio)
            End If
            oPrecio.Escalados.Add oEscalado.Id, oEscalado.inicial, oEscalado.Final, vEscPresup, 0, AdoRes("PREC_" & oEscalado.Id).Value, AdoRes("PREC_VALIDO_" & oEscalado.Id).Value, , , AdoRes("OBJ_" & oEscalado.Id).Value
        Next
    End If
    If Not m_oGrupo.AtributosItem Is Nothing Then
        Set oPrecio.AtribOfertados = New CAtributosOfertados
        Set oPrecio.AtribOfertados.Conexion = m_oConexion
        Dim oAtribOfe As CAtributoOfertado
        For Each oAtrib In m_oGrupo.AtributosItem
            Select Case oAtrib.Tipo
            Case TipoNumerico
                If bUsarEsc Then
                    If IsNull(oAtrib.PrecioAplicarA) Or (m_oGrupo.UsarEscalados = 0) Then
                        If IsNull(AdoRes.Fields("AT_" & oAtrib.Cod).Value) Then
                            oPrecio.AtribOfertados.Add oAtrib.IdAtribProce, m_oOferta, AdoRes("ID").Value, m_oGrupo.Codigo, , , , , , , , oAtrib
                        Else
                            oPrecio.AtribOfertados.Add oAtrib.IdAtribProce, m_oOferta, AdoRes("ID").Value, m_oGrupo.Codigo, , , CDbl(AdoRes.Fields("AT_" & oAtrib.Cod).Value), , , , , oAtrib
                        End If
                    Else
                        Set oAtribOfe = oPrecio.AtribOfertados.Add(oAtrib.IdAtribProce, m_oOferta, AdoRes("ID").Value, m_oGrupo.Codigo, , , , , , , , oAtrib)
                        Set oAtribOfe.Escalados = New CEscalados
                        Set oAtribOfe.Escalados.Conexion = m_oConexion
                        oAtribOfe.Escalados.Modo = oPrecio.Escalados.Modo
                        For Each oEscalado In m_oGrupo.Escalados
                            oAtribOfe.Escalados.Add oEscalado.Id, oEscalado.inicial, oEscalado.Final, AdoRes("PRES_" & oEscalado.Id).Value, 0, AdoRes("PREC_" & oEscalado.Id).Value, AdoRes("PREC_VALIDO_" & oEscalado.Id).Value, AdoRes("VALOR_" & oEscalado.Id & "_" & oAtrib.IdAtribProce)
                        Next
                    End If
                Else
                    If IsNull(AdoRes.Fields("AT_" & oAtrib.Cod).Value) Then
                        oPrecio.AtribOfertados.Add oAtrib.IdAtribProce, m_oOferta, AdoRes("ID").Value, m_oGrupo.Codigo, , , , , , , , oAtrib
                    Else
                        oPrecio.AtribOfertados.Add oAtrib.IdAtribProce, m_oOferta, AdoRes("ID").Value, m_oGrupo.Codigo, , , CDbl(AdoRes.Fields("AT_" & oAtrib.Cod).Value), , , , , oAtrib
                    End If
                End If
            Case TipoFecha
                If IsNull(AdoRes.Fields("AT_" & oAtrib.Cod).Value) Then
                    oPrecio.AtribOfertados.Add oAtrib.IdAtribProce, m_oOferta, AdoRes("ID").Value, m_oGrupo.Codigo, , , , , , , , oAtrib
                Else
                    oPrecio.AtribOfertados.Add oAtrib.IdAtribProce, m_oOferta, AdoRes("ID").Value, m_oGrupo.Codigo, , CDate(AdoRes.Fields("AT_" & oAtrib.Cod).Value), , , , , , oAtrib
                End If
            Case TipoString
                oPrecio.AtribOfertados.Add oAtrib.IdAtribProce, m_oOferta, AdoRes("ID").Value, m_oGrupo.Codigo, , , , , AdoRes.Fields("AT_" & oAtrib.Cod).Value, , , oAtrib
            Case TipoBoolean
                oPrecio.AtribOfertados.Add oAtrib.IdAtribProce, m_oOferta, AdoRes("ID").Value, m_oGrupo.Codigo, AdoRes.Fields("AT_" & oAtrib.Cod).Value, , , , , , , oAtrib
            End Select
        Next
    End If
    
    AdoRes.MoveNext
Wend
AdoRes.Close
Set adoComm = Nothing
Set AdoRes = Nothing

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "COfertaGrupo", "CargarPrecios", ERR, Erl)
      Exit Sub
   End If

End Sub



Public Function ModificarObsAdjunto() As TipoErrorSummit
Dim sConsulta As String
Dim TESError As TipoErrorSummit
Dim AdoRes As adodb.Recordset
Dim btrans As Boolean

'********* Precondicion *******************
If m_oConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CItem.ModificarEspecificacion", "No se ha establecido la conexion"
End If

'******************************************

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
    
    TESError.NumError = TESnoerror
    
    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
    btrans = True
    
    If IsMissing(m_vObsAdjun) Then
        sConsulta = "UPDATE GRUPO_OFE SET OBSADJUN=NULL WHERE ANYO=" & m_oOferta.Anyo & " AND GMN1='" & DblQuote(m_oOferta.GMN1Cod) & "' AND PROCE=" & m_oOferta.Proce & " AND GRUPO =" & DblQuote(m_oGrupo.Id)
        sConsulta = sConsulta & " AND PROVE ='" & DblQuote(m_oOferta.Prove) & "' AND OFE=" & m_oOferta.Num
    Else
        If IsEmpty(m_vObsAdjun) Then
            sConsulta = "UPDATE GRUPO_OFE SET OBSADJUN=NULL WHERE ANYO=" & m_oOferta.Anyo & " AND GMN1='" & DblQuote(m_oOferta.GMN1Cod) & "' AND PROCE=" & m_oOferta.Proce & " AND GRUPO =" & DblQuote(m_oGrupo.Id)
            sConsulta = sConsulta & " AND PROVE ='" & DblQuote(m_oOferta.Prove) & "' AND OFE=" & m_oOferta.Num
        Else
            If IsNull(m_vObsAdjun) Then
                sConsulta = "UPDATE GRUPO_OFE SET OBSADJUN=NULL WHERE ANYO=" & m_oOferta.Anyo & " AND GMN1='" & DblQuote(m_oOferta.GMN1Cod) & "' AND PROCE=" & m_oOferta.Proce & " AND GRUPO =" & DblQuote(m_oGrupo.Id)
                sConsulta = sConsulta & " AND PROVE ='" & DblQuote(m_oOferta.Prove) & "' AND OFE=" & m_oOferta.Num
            Else
                If m_vObsAdjun = "" Then
                    sConsulta = "UPDATE GRUPO_OFE SET OBSADJUN=NULL WHERE ANYO=" & m_oOferta.Anyo & " AND GMN1='" & DblQuote(m_oOferta.GMN1Cod) & "' AND PROCE=" & m_oOferta.Proce & " AND GRUPO =" & DblQuote(m_oGrupo.Id)
                    sConsulta = sConsulta & " AND PROVE ='" & DblQuote(m_oOferta.Prove) & "' AND OFE=" & m_oOferta.Num
                Else
                    sConsulta = "UPDATE GRUPO_OFE SET OBSADJUN='" & DblQuote(m_vObsAdjun) & "' WHERE ANYO=" & m_oOferta.Anyo & " AND GMN1='" & DblQuote(m_oOferta.GMN1Cod) & "' AND PROCE=" & m_oOferta.Proce & " AND GRUPO =" & DblQuote(m_oGrupo.Id)
                    sConsulta = sConsulta & " AND PROVE ='" & DblQuote(m_oOferta.Prove) & "' AND OFE=" & m_oOferta.Num
                End If
            End If
        End If
    End If
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    btrans = False
    ModificarObsAdjunto = TESError
    Exit Function

Error_Cls:
    
    ModificarObsAdjunto = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    
    If btrans Then
        m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "COfertaGrupo", "ModificarObsAdjunto", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If
    
End Function

''' <summary>Actualiza los acumulados por grupo oferta cuando se realizan adjudicaciones</summary>
''' <param name="iAnyo">Anyo</param>
''' <param name="sGmn1">GMN1</param>
''' <param name="lProce">Cod. proceso</param>
''' <param name="oAdjsGrupo">Adjudicaciones</param>
''' <returns>Error si se ha producido</returns>
''' <remarks>Llamada desde: CProceso.RealizarAdjudicaciones, CProceso.RealizarAdjudicacionesParciales</remarks>

Public Function ActualizarAcumuladosAdjudicaciones(ByVal iAnyo As Integer, ByVal sGMN1 As String, ByVal lProce As Long, ByRef oAdjGrupo As CAdjGrupo) As TipoErrorSummit
    Dim TESError As TipoErrorSummit
    Dim sConsulta As String
    Dim adoComm As adodb.Command
    Dim adoParam As adodb.Parameter
    Dim dblVar As Double
    
    '********* Precondicion **************************************
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "COfertaGrupo.ActualizarAcumuladosAdjudicaciones", "No se ha establecido la conexion"
        Exit Function
    End If
    '********************************************************
    
    On Error GoTo TratarError

    TESError.NumError = TESnoerror
        
    sConsulta = "UPDATE GRUPO_OFE SET "
    sConsulta = sConsulta & " CONSUMIDO= ?"
    sConsulta = sConsulta & " ,ADJUDICADO=?"
    sConsulta = sConsulta & " ,AHORRADO=?"
    sConsulta = sConsulta & " ,AHORRADO_PORCEN=?"
    sConsulta = sConsulta & " ,ABIERTO=?"
    sConsulta = sConsulta & " ,IMPORTE=?"
    sConsulta = sConsulta & " ,AHORRO_OFE=?"
    sConsulta = sConsulta & " ,AHORRO_OFE_PORCEN=?"
    sConsulta = sConsulta & " WHERE ANYO=" & iAnyo & " AND GMN1='" & DblQuote(sGMN1) & "' AND PROCE=" & lProce
    sConsulta = sConsulta & " AND GRUPO=" & (oAdjGrupo.GrupoID) & " AND PROVE='" & DblQuote(oAdjGrupo.Prove) & "'"
    sConsulta = sConsulta & " AND OFE=" & oAdjGrupo.NumOfe
    
    Set adoComm = New adodb.Command
    With adoComm
        Set .ActiveConnection = m_oConexion.ADOCon
        .CommandText = sConsulta
        Set adoParam = .CreateParameter("CONSUMIDO", adDouble, adParamInput, , oAdjGrupo.ConsumidoReal)
        .Parameters.Append adoParam
        If Not IsNull(oAdjGrupo.AdjudicadoReal) Then
            Set adoParam = .CreateParameter("ADJUDICADO", adDouble, adParamInput, , oAdjGrupo.AdjudicadoReal)
            .Parameters.Append adoParam
            dblVar = oAdjGrupo.ConsumidoReal - oAdjGrupo.AdjudicadoReal
            Set adoParam = .CreateParameter("AHORRADO", adDouble, adParamInput, , dblVar)
            .Parameters.Append adoParam
            
            If oAdjGrupo.ConsumidoReal <> 0 Then
                dblVar = CDec(CDec((oAdjGrupo.ConsumidoReal - oAdjGrupo.AdjudicadoReal) / oAdjGrupo.ConsumidoReal) * 100)
            Else
                dblVar = 0
            End If
            Set adoParam = .CreateParameter("AHORRADO_PORCEN", adDouble, adParamInput, , dblVar)
            .Parameters.Append adoParam
        Else
            Set adoParam = .CreateParameter("ADJUDICADO", adDouble, adParamInput, , Null)
            .Parameters.Append adoParam
            Set adoParam = .CreateParameter("AHORRADO", adDouble, adParamInput, , Null)
            .Parameters.Append adoParam
            Set adoParam = .CreateParameter("AHORRADO_PORCEN", adDouble, adParamInput, , Null)
            .Parameters.Append adoParam
        End If
        Set adoParam = .CreateParameter("ABIERTO", adDouble, adParamInput, , oAdjGrupo.Abierto)
        .Parameters.Append adoParam
        
        If Not IsNull(oAdjGrupo.Importe) Then
            Set adoParam = .CreateParameter("IMPORTE", adDouble, adParamInput, , oAdjGrupo.Importe)
            .Parameters.Append adoParam
            dblVar = oAdjGrupo.AbiertoOfe - oAdjGrupo.Importe
            Set adoParam = .CreateParameter("AHORRO_OFE", adDouble, adParamInput, , dblVar)
            .Parameters.Append adoParam
            If oAdjGrupo.AbiertoOfe <> 0 Then
                dblVar = CDec(CDec(oAdjGrupo.AbiertoOfe - oAdjGrupo.Importe) / oAdjGrupo.AbiertoOfe) * 100
            Else
                dblVar = 0
            End If
            Set adoParam = .CreateParameter("AHORRO_OFE_PORCEN", adDouble, adParamInput, , dblVar)
            .Parameters.Append adoParam
        Else
            Set adoParam = .CreateParameter("IMPORTE", adDouble, adParamInput, , Null)
            .Parameters.Append adoParam
            Set adoParam = .CreateParameter("AHORRO_OFE", adDouble, adParamInput, , Null)
            .Parameters.Append adoParam
            Set adoParam = .CreateParameter("AHORRO_OFE_PORCEN", adDouble, adParamInput, , Null)
            .Parameters.Append adoParam
        End If
    
        .Execute
    End With
       
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo TratarError
    
Salir:
    ActualizarAcumuladosAdjudicaciones = TESError
    Set adoComm = Nothing
    Set adoParam = Nothing
    Exit Function
TratarError:
    TESError = basErrores.TratarError(m_oConexion.ADOCon.Errors, ERR)
    Resume Salir
End Function

''' <summary>Inserta registro en GRUPO_OFE_REU cuando se realizan adjudicaciones</summary>
''' <param name="iAnyo">Anyo</param>
''' <param name="sGmn1">GMN1</param>
''' <param name="lProce">Cod. proceso</param>
''' <param name="FecReu">Fecha reuni�n</param>
''' <param name="oAdjsGrupo">Adjudicaciones</param>
''' <returns>Error si se ha producido</returns>
''' <remarks>Llamada desde: CProceso.RealizarAdjudicaciones, CProceso.RealizarAdjudicacionesParciales</remarks>

Public Function InsertarRegistroReunion(ByVal iAnyo As Integer, ByVal sGMN1 As String, ByVal lProce As Long, ByVal FecReu As Variant, ByRef oAdjGrupo As CAdjGrupo) As TipoErrorSummit
    Dim TESError As TipoErrorSummit
    Dim sConsulta As String
    Dim adoComm As adodb.Command
    Dim adoParam As adodb.Parameter
    Dim dblVar As Double
    
    '********* Precondicion **************************************
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "COfertaGrupo.InsertarRegistroReunion", "No se ha establecido la conexion"
        Exit Function
    End If
    '********************************************************
    
    On Error GoTo TratarError

    TESError.NumError = TESnoerror
        
    sConsulta = "INSERT INTO GRUPO_OFE_REU (ANYO,GMN1,PROCE,GRUPO,PROVE,OFE,FECREU,CONSUMIDO,ADJUDICADO"
    sConsulta = sConsulta & " ,AHORRADO,AHORRADO_PORCEN,ABIERTO,IMPORTE,AHORRO_OFE,AHORRO_OFE_PORCEN) VALUES"
    sConsulta = sConsulta & " (" & iAnyo & ", '" & DblQuote(sGMN1) & "' ," & lProce
    sConsulta = sConsulta & " ," & (oAdjGrupo.GrupoID) & " ,'" & DblQuote(oAdjGrupo.Prove) & "'"
    sConsulta = sConsulta & " ," & oAdjGrupo.NumOfe & " ," & DateToSQLTimeDate(FecReu)
    sConsulta = sConsulta & " ,?,?,?,?,?,?,?,?)"
    
    Set adoComm = New adodb.Command
    With adoComm
        Set .ActiveConnection = m_oConexion.ADOCon
        .CommandText = sConsulta
        Set adoParam = .CreateParameter("CONSUMIDO", adDouble, adParamInput, , oAdjGrupo.Consumido)
        .Parameters.Append adoParam
        If Not IsNull(oAdjGrupo.Adjudicado) Then
            Set adoParam = .CreateParameter("ADJUDICADO", adDouble, adParamInput, , oAdjGrupo.Adjudicado)
            .Parameters.Append adoParam
            dblVar = oAdjGrupo.Consumido - oAdjGrupo.Adjudicado
            Set adoParam = .CreateParameter("AHORRADO", adDouble, adParamInput, , dblVar)
            .Parameters.Append adoParam
            
            If oAdjGrupo.Consumido <> 0 Then
                dblVar = CDec(CDec((oAdjGrupo.Consumido - oAdjGrupo.Adjudicado) / oAdjGrupo.Consumido) * 100)
            Else
                dblVar = 0
            End If
            Set adoParam = .CreateParameter("AHORRADO_PORCEN", adDouble, adParamInput, , dblVar)
            .Parameters.Append adoParam
        Else
            Set adoParam = .CreateParameter("ADJUDICADO", adDouble, adParamInput, , Null)
            .Parameters.Append adoParam
            Set adoParam = .CreateParameter("AHORRADO", adDouble, adParamInput, , Null)
            .Parameters.Append adoParam
            Set adoParam = .CreateParameter("AHORRADO_PORCEN", adDouble, adParamInput, , Null)
            .Parameters.Append adoParam
        End If
        Set adoParam = .CreateParameter("ABIERTO", adDouble, adParamInput, , oAdjGrupo.Abierto)
        .Parameters.Append adoParam
        
        If Not IsNull(oAdjGrupo.Importe) Then
            Set adoParam = .CreateParameter("IMPORTE", adDouble, adParamInput, , oAdjGrupo.Importe)
            .Parameters.Append adoParam
            dblVar = oAdjGrupo.AbiertoOfe - oAdjGrupo.Importe
            Set adoParam = .CreateParameter("AHORRO_OFE", adDouble, adParamInput, , dblVar)
            .Parameters.Append adoParam
            If oAdjGrupo.AbiertoOfe <> 0 Then
                dblVar = CDec(CDec(oAdjGrupo.AbiertoOfe - oAdjGrupo.Importe) / oAdjGrupo.AbiertoOfe) * 100
            Else
                dblVar = 0
            End If
            Set adoParam = .CreateParameter("AHORRO_OFE_PORCEN", adDouble, adParamInput, , dblVar)
            .Parameters.Append adoParam
        Else
            Set adoParam = .CreateParameter("IMPORTE", adDouble, adParamInput, , Null)
            .Parameters.Append adoParam
            Set adoParam = .CreateParameter("AHORRO_OFE", adDouble, adParamInput, , Null)
            .Parameters.Append adoParam
            Set adoParam = .CreateParameter("AHORRO_OFE_PORCEN", adDouble, adParamInput, , Null)
            .Parameters.Append adoParam
        End If
        
        .Execute
    End With
   
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo TratarError
    
Salir:
    InsertarRegistroReunion = TESError
    Set adoComm = Nothing
    Set adoParam = Nothing
    Exit Function
TratarError:
    TESError = basErrores.TratarError(m_oConexion.ADOCon.Errors, ERR)
    Resume Salir
End Function
