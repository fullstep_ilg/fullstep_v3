VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CAtributoOfertado"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'**********************CAtributoOfertado*******************************
'*                 Autor: Mertxe Mart�n
'*                 Creada: 16/04/2002
'**********************************************************************

Option Explicit

Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum

Private m_AdoRecordset As adodb.Recordset

Private m_oConexion As CConexion

Private m_vIDAtribProce As Long 'corresponde a PROCE_ATRIB.ID en atributos de proceso o grupo
                                'en atributos de pedido corresponde a DEF_ATRIB.ID

Private m_vItem As Variant
Private m_vGrupo As Variant

Private m_vIndice As Variant
Private m_vFecAct As Variant

Private m_vValorNum As Variant
Private m_vValorText As Variant
Private m_vValorFec As Variant
Private m_vValorBool As Variant

Private m_vValorPond As Variant

Private m_oUltimaOferta As COferta

Private m_udtTipo As TiposDeAtributos

Private m_oObjeto As Object
Private m_bAtribIntEnAtributosProceso As Boolean
Private m_bValidacion_ERP As Boolean
Private m_bInterno As Boolean
Private m_bPedido As Boolean
Private m_bObligatorio As Boolean
Private m_bModificado As Boolean
Private m_oEscalados As CEscalados ' Tabla de precios escalados

Private m_bOrdenarIntegracion As Boolean

Public Property Let AtribIntEnAtributosProceso(ByVal vData As Boolean)
    m_bAtribIntEnAtributosProceso = vData
End Property
Public Property Get AtribIntEnAtributosProceso() As Boolean
    AtribIntEnAtributosProceso = m_bAtribIntEnAtributosProceso
End Property

Public Property Set Objeto(ByRef oObj As Object)
    Set m_oObjeto = oObj
End Property
Public Property Get Objeto() As Object
    Set Objeto = m_oObjeto
End Property

Public Property Set UltimaOferta(ByVal oOfe As COferta)
    Set m_oUltimaOferta = oOfe
End Property
Public Property Get UltimaOferta() As COferta
    Set UltimaOferta = m_oUltimaOferta
End Property

Public Property Let Item(ByVal vData As Variant)
    m_vItem = vData
End Property
Public Property Get Item() As Variant
    Item = m_vItem
End Property

Public Property Let Grupo(ByVal vData As Variant)
    m_vGrupo = vData
End Property
Public Property Get Grupo() As Variant
    Grupo = m_vGrupo
End Property

Public Property Let ValorNum(ByVal vData As Variant)
    m_vValorNum = vData
End Property
Public Property Get ValorNum() As Variant
    ValorNum = m_vValorNum
End Property

Public Property Let ValorText(ByVal vData As Variant)
    m_vValorText = vData
End Property
Public Property Get ValorText() As Variant
    ValorText = m_vValorText
End Property

Public Property Let ValorFec(ByVal vData As Variant)
    m_vValorFec = vData
End Property
Public Property Get ValorFec() As Variant
    ValorFec = m_vValorFec
End Property

Public Property Let ValorBool(ByVal vData As Variant)
    m_vValorBool = vData
End Property
Public Property Get ValorBool() As Variant
    ValorBool = m_vValorBool
End Property

Public Property Let ValorPond(ByVal vData As Variant)
    m_vValorPond = vData
End Property
Public Property Get ValorPond() As Variant
    ValorPond = m_vValorPond
End Property

Public Property Get IdAtribProce() As Long
    IdAtribProce = m_vIDAtribProce
End Property
Public Property Let IdAtribProce(ByVal vData As Long)
    m_vIDAtribProce = vData
End Property

Public Property Let Indice(ByVal vData As Variant)
    m_vIndice = vData
End Property
Public Property Get Indice() As Variant
    Indice = m_vIndice
End Property
Public Property Let FecAct(ByVal vData As Variant)
    m_vFecAct = vData
End Property
Public Property Get FecAct() As Variant
    FecAct = m_vFecAct
End Property

Friend Property Set Conexion(ByVal vData As CConexion)
    Set m_oConexion = vData
End Property
Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property

Public Property Let Validacion_ERP(ByVal vData As Variant)
    m_bValidacion_ERP = vData
End Property
Public Property Get Validacion_ERP() As Variant
    Validacion_ERP = m_bValidacion_ERP
End Property

Public Property Let Interno(ByVal vData As Variant)
    Interno = vData
End Property
Public Property Get Interno() As Variant
    Interno = m_bInterno
End Property

Public Property Let Pedido(ByVal vData As Variant)
    Pedido = vData
End Property
Public Property Get Pedido() As Variant
    Pedido = m_bPedido
End Property


Public Property Get Obligatorio() As Boolean
    Obligatorio = m_bObligatorio
End Property
Public Property Let Obligatorio(ByVal vData As Boolean)
    m_bObligatorio = vData
End Property
Public Property Get Modificado() As Boolean
    Modificado = m_bModificado
End Property
Public Property Let Modificado(ByVal vData As Boolean)
    m_bModificado = vData
End Property

Public Property Get Escalados() As CEscalados
    Set Escalados = m_oEscalados
End Property

Public Property Set Escalados(ByVal oEscalados As CEscalados)
    Set m_oEscalados = oEscalados
End Property
Public Property Let Tipo(ByVal vData As TiposDeAtributos)
    m_udtTipo = vData
End Property
Public Property Get Tipo() As TiposDeAtributos
    Tipo = m_udtTipo
End Property

Private Sub Class_Terminate()
    
    Set m_oConexion = Nothing

End Sub



''' <summary>Almacena los atributos del pedido.</summary>
''' <param name="lCodCampo">El ID de ORDEN_ENTREGA</param>
''' <param name="vProveERP">C�digo del proveedor en el ERP</param>
''' <param name="vPer">Codigo de la persona</param>
''' <param name="vUsu">Codigo del usuario</param>
''' <returns>TipoErrorSummit</returns>
''' <remarks>Llamada desde: FSGSClient.frmSeguimiento.sdbgAtributos_BeforeUpdate
''' Tiempo m�ximo: < 1seg </remarks>
''' <revision>aam 03/02/2014</revision>
Public Function GuardarAtributoPedido(ByVal lCodCampo As Long, ByVal vProveERP As Variant, ByVal vPer As Variant, _
                                      ByVal vUsu As Variant, ByVal oOrden As COrdenEntrega, Optional ByVal oLinea As CLineaPedido = Nothing) As TipoErrorSummit
    Dim TESError As TipoErrorSummit
    Dim sConsulta As String
    Dim rs As adodb.Recordset
    Dim oAtrib As CAtributo
    Dim vMinNum As Variant
    Dim vMaxNum As Variant
    Dim vMinFec As Variant
    Dim vMaxFec As Variant
    Dim vValorLista As Variant
    Dim bGuardar As Boolean
    Dim oValorLista As CValorPond
    Dim adocom As adodb.Command
    Dim oParam As adodb.Parameter
    Dim lIdLogOrden As Long
    Dim iTipoInt As Integer
    Dim cErp As CERPInt
    Dim bPedAcepProvActivo As Boolean
    Dim udtEstado As Integer
    Dim bHayTrans As Boolean
    Dim bNuevoLog As Boolean
    Dim sTabla As String
    Dim sCampo As String
    Dim idLogOrden As Long
    Dim lIdLogLineasPedido As Long
    Dim sERP As String
    '***************************************** INTEGRACION*************************************
    '************************ se integran los atributos de pedido *****************************
    '******************************************************************************************
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    TESError.NumError = TESnoerror
    
    If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
    
    If oLinea Is Nothing Then
        sTabla = "ORDEN_ENTREGA"
        sCampo = "ORDEN"
    Else
        sTabla = "LINEAS_PEDIDO"
        sCampo = "LINEA"
    End If
    
    
    Set oAtrib = m_oObjeto
    If oAtrib.Tipo = TipoNumerico Then
        vMinNum = DblToSQLFloat(oAtrib.Minimo)
        vMaxNum = DblToSQLFloat(oAtrib.Maximo)
        vMinFec = "NULL"
        vMaxFec = "NULL"
    ElseIf oAtrib.Tipo = TipoFecha Then
        vMinFec = DateToSQLTimeDate(oAtrib.Minimo)
        vMaxFec = DateToSQLTimeDate(oAtrib.Maximo)
        vMinNum = "NULL"
        vMaxNum = "NULL"
    Else
        vMinNum = "NULL"
        vMaxNum = "NULL"
        vMinFec = "NULL"
        vMaxFec = "NULL"
    End If

    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
    bHayTrans = True
    '
    sConsulta = "SELECT OA." & sCampo & " FROM " & sTabla & "_ATRIB OA WITH(NOLOCK) WHERE OA." & sCampo & "=" & lCodCampo & " AND OA.ATRIB=" & m_vIDAtribProce
    Set rs = New adodb.Recordset
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
    If rs.eof Then
        sConsulta = "INSERT INTO " & sTabla & "_ATRIB (" & sCampo & ",ATRIB,VALOR_NUM,VALOR_TEXT,VALOR_FEC,VALOR_BOOL,INTERNO,INTRO,MINNUM,MAXNUM,MINFEC,MAXFEC) VALUES"
        sConsulta = sConsulta & " (" & lCodCampo & "," & m_vIDAtribProce & "," & DblToSQLFloat(m_vValorNum) & "," & StrToSQLNULL(m_vValorText) & "," & DateToSQLTimeDate(m_vValorFec) & "," & IIf(IsNull(m_vValorBool), "NULL", BooleanToSQLBinary(m_vValorBool))
        sConsulta = sConsulta & "," & BooleanToSQLBinary(oAtrib.Interno) & "," & oAtrib.TipoIntroduccion & "," & vMinNum & "," & vMaxNum & "," & vMinFec & "," & vMaxFec & ")"
        m_oConexion.ADOCon.Execute sConsulta
        If oAtrib.TipoIntroduccion = Introselec And Not oAtrib.ListaPonderacion Is Nothing Then
            For Each oValorLista In oAtrib.ListaPonderacion
                ''' Preparar la SP de inserci�n de los nuevos valores de la lista y sus parametros
                Set adocom = New adodb.Command
                Set adocom.ActiveConnection = m_oConexion.ADOCon

                adocom.CommandType = adCmdText

                Set oParam = adocom.CreateParameter("TIPO", adSmallInt, adParamInput, , 3) 'Tipo 3 = orden entrega
                adocom.Parameters.Append oParam
                Set oParam = adocom.CreateParameter("ATRIB", adInteger, adParamInput, , m_vIDAtribProce)
                adocom.Parameters.Append oParam
                Set oParam = adocom.CreateParameter(sCampo, adSmallInt, adParamInput, , oValorLista.IdOrden)
                adocom.Parameters.Append oParam
                Set oParam = adocom.CreateParameter("ENDEFINICION", adInteger, adParamInput, , 0)
                adocom.Parameters.Append oParam
                Set oParam = adocom.CreateParameter("ID", adInteger, adParamInput, , lCodCampo)
                adocom.Parameters.Append oParam
                vValorLista = Null
                vValorLista = oValorLista.ValorLista
                Select Case oAtrib.Tipo
                Case TipoString
                    Set oParam = adocom.CreateParameter("VALORTEXT", adVarChar, adParamInput, 100, vValorLista)
                    adocom.Parameters.Append oParam
                    sConsulta = "EXEC ATR_INSERTAR_LISTA @TIPO=?,@ATRIB=?,@ORDEN=?,@ENDEFINICION=?,@ID=?,@VALORTEXT=?"
                Case TipoNumerico
                    Set oParam = adocom.CreateParameter("VALORNUM", adDouble, adParamInput, , vValorLista)
                    adocom.Parameters.Append oParam
                    sConsulta = "EXEC ATR_INSERTAR_LISTA @TIPO=?,@ATRIB=?,@ORDEN=?,@ENDEFINICION=?,@ID=?,@VALORNUM=?"
                Case TipoFecha
                    Set oParam = adocom.CreateParameter("VALORFEC", adDate, adParamInput, , CVDate(vValorLista))
                    adocom.Parameters.Append oParam
                    sConsulta = "EXEC ATR_INSERTAR_LISTA @TIPO=?,@ATRIB=?,@" & sCampo & "=?,@ENDEFINICION=?,@ID=?,@VALORFEC=?"
                End Select

                ''' Ejecutar la SP
                adocom.CommandText = sConsulta
                adocom.Execute
                If m_oConexion.ADOCon.Errors.Count > 0 Then
                    GoTo Error_Cls
                End If
                Set adocom.ActiveConnection = Nothing
                Set adocom = Nothing

            Next
        End If
    Else
        sConsulta = "UPDATE " & sTabla & "_ATRIB SET VALOR_NUM=" & DblToSQLFloat(m_vValorNum) & ",VALOR_TEXT=" & StrToSQLNULL(m_vValorText) & ",VALOR_FEC=" & DateToSQLTimeDate(m_vValorFec) & ",VALOR_BOOL= " & IIf(IsNull(m_vValorBool), "NULL", BooleanToSQLBinary(m_vValorBool))
        sConsulta = sConsulta & " WHERE " & sCampo & "= " & lCodCampo & " AND ATRIB=" & m_vIDAtribProce
        m_oConexion.ADOCon.Execute sConsulta
    End If
    
    If oLinea Is Nothing Then
        'Atributos de la orden de entrega
        If oOrden.GrabarEnLogERP(vProveERP) Then
            rs.Close
            oOrden.TipoLogPedido = Atrib_Cabecera
            TESError = oOrden.GrabarPedidoEnLog(Accion_Modificacion, , , CLng(m_vIDAtribProce))
            If TESError.NumError <> 0 Then
                GoTo ERROR
            End If
        
            sConsulta = "SELECT LOG_ORDEN_ENTREGA.ID,LOG_GRAL.ESTADO,LOG_ORDEN_ENTREGA.ACCION FROM LOG_ORDEN_ENTREGA WITH(NOLOCK) INNER JOIN LOG_GRAL WITH(NOLOCK) ON LOG_ORDEN_ENTREGA.ID = LOG_GRAL.ID_TABLA AND LOG_GRAL.TABLA = " & PED_directo & " AND LOG_GRAL.ORIGEN <> " & OrigenIntegracion.erp
            sConsulta = sConsulta & " WHERE LOG_ORDEN_ENTREGA.ID_ORDEN_ENTREGA = " & lCodCampo & " AND (LOG_ORDEN_ENTREGA.ORIGEN=0 OR LOG_ORDEN_ENTREGA.ORIGEN=2)"
            rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
            If Not rs.eof Then
                While Not rs.eof
                    If rs("ESTADO").Value = EstadoIntegracion.PendienteDeTratar Then
                        bNuevoLog = False
                        lIdLogOrden = rs("ID").Value
                    Else
                        If rs("ACCION").Value = "I" Then
                            bNuevoLog = True
                        End If
                    End If
                    rs.MoveNext
                Wend
            Else
                bNuevoLog = True
            End If
            rs.Close
            If bNuevoLog Then
                'Llamada al servicio de integraci�n WCF.
                Set basUtilidades.Conexion = m_oConexion
                basUtilidades.LlamarFSIS EntidadIntegracion.PED_directo, lCodCampo
            End If
        End If
    Else
        'Atribudo de la Linea de Pedido
        If oLinea.GrabarEnLog Then
            Set oLinea.ObjetoOrden = oOrden
            oLinea.ObjetoOrden.TipoLogPedido = Atrib_LineaPedido
            TESError = oLinea.ObjetoOrden.GrabarLogOrdenEntrega(idLogOrden, sERP, Accion_Reenvio)
            If TESError.NumError <> TESnoerror Then
                GoTo Error_Cls
            End If
        
            TESError = oLinea.GrabarLogLineaPedido(idLogOrden, lIdLogLineasPedido, Accion_Modificacion, sERP)
            If TESError.NumError <> TESnoerror Then
                GoTo Error_Cls
            End If
            oLinea.IdCod_Atrib = m_vIDAtribProce
            TESError = oLinea.GrabarLogResto(idLogOrden, lIdLogLineasPedido)
            If TESError.NumError <> TESnoerror Then
                GoTo Error_Cls
            End If
        End If
    End If
    
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    bHayTrans = False
    
    GuardarAtributoPedido = TESError

Terminar:
    On Error Resume Next
    rs.Close
    Set rs = Nothing
    Exit Function

Error_Cls:
    GuardarAtributoPedido = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    If bHayTrans Then m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
    Resume Terminar
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      'Call g_oErrores.TratarError("Class Module", "CAtributoOfertado", "GuardarAtributoPedido", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If
End Function

''' <summary>Guarda los cambios realizados en un atributo de oferta</summary>
''' <param name="lIdItem">Id. item</param>
''' <returns>Variable de tipo TipoErrorSummit con el error si se ha producido</returns>
''' <remarks>Llamada desde: frmOFERec.sdbgPreciosEscalados_AfterColUpdate; Tiempo m�ximo:0,1</remarks>

Public Function GuardarCambiosOfertaAtributo(ByVal lIdItem As Long, Optional ByVal bHayTran As Boolean) As TipoErrorSummit
    Dim TESError As TipoErrorSummit
    Dim adoComm As adodb.Command
    Dim adoParam As adodb.Parameter
    Dim rs As adodb.Recordset
    Dim sConsulta As String
    Dim bHayValor As Boolean

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:

    TESError.NumError = TESnoerror
    
    If Not bHayTran Then
        m_oConexion.ADOCon.Execute "BEGIN DISTRIBUTED TRAN"
        m_oConexion.ADOCon.Execute "SET XACT_ABORT ON"
    End If
    
    Set adoComm = New adodb.Command
    Set adoComm.ActiveConnection = m_oConexion.ADOCon
    adoComm.CommandType = adCmdText
    With adoComm
        .CommandText = "DELETE FROM OFE_ITEM_ATRIB WHERE ANYO=? AND GMN1=? AND PROCE=? AND ITEM=? AND PROVE=? AND OFE=? AND ATRIB_ID=?"
        Set adoParam = .CreateParameter("ANYO", adSmallInt, adParamInput, , m_oUltimaOferta.Anyo)
        .Parameters.Append adoParam
        Set adoParam = .CreateParameter("GMN1", adVarChar, adParamInput, Len(m_oUltimaOferta.GMN1Cod), m_oUltimaOferta.GMN1Cod)
        .Parameters.Append adoParam
        Set adoParam = .CreateParameter("PROCE", adInteger, adParamInput, , m_oUltimaOferta.Proce)
        .Parameters.Append adoParam
        Set adoParam = .CreateParameter("ITEM", adInteger, adParamInput, , lIdItem)
        .Parameters.Append adoParam
        Set adoParam = .CreateParameter("PROVE", adVarChar, adParamInput, Len(m_oUltimaOferta.Prove), m_oUltimaOferta.Prove)
        .Parameters.Append adoParam
        Set adoParam = .CreateParameter("OFE", adSmallInt, adParamInput, , m_oUltimaOferta.Num)
        .Parameters.Append adoParam
        Set adoParam = .CreateParameter("ATRIB_ID", adInteger, adParamInput, , m_vIDAtribProce)
        .Parameters.Append adoParam
            
        .Execute
    End With
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    
    sConsulta = "INSERT INTO OFE_ITEM_ATRIB (ANYO,GMN1,PROCE,ITEM,PROVE,OFE,ATRIB_ID"
    bHayValor = False
    Select Case m_oObjeto.Tipo
        Case TipoNumerico
            If Not IsNull(m_vValorNum) And Not IsEmpty(m_vValorNum) And Not IsMissing(m_vValorNum) And m_vValorNum <> "" Then
                sConsulta = sConsulta & ",VALOR_NUM"
                bHayValor = True
            End If
        Case TipoFecha
            If Not IsNull(m_vValorFec) And Not IsEmpty(m_vValorFec) And Not IsMissing(m_vValorFec) And m_vValorFec <> "" Then
                sConsulta = sConsulta & ",VALOR_FEC)"
                bHayValor = True
            End If
        Case TipoString
            If Not IsNull(m_vValorText) And Not IsEmpty(m_vValorText) And Not IsMissing(m_vValorText) And m_vValorText <> "" Then
                sConsulta = sConsulta & ",VALOR_TEXT"
                bHayValor = True
            End If
        Case TipoBoolean
            If Not IsNull(m_vValorBool) And Not IsEmpty(m_vValorBool) And Not IsMissing(m_vValorBool) And m_vValorBool <> "" Then
                sConsulta = sConsulta & ",VALOR_BOOL"
                bHayValor = True
            End If
    End Select
    sConsulta = sConsulta & ") VALUES (?,?,?,?,?,?,?"
    If bHayValor Then sConsulta = sConsulta & ",?"
    sConsulta = sConsulta & ")"
    
    Set adoComm = New adodb.Command
    Set adoComm.ActiveConnection = m_oConexion.ADOCon
    adoComm.CommandType = adCmdText
    With adoComm
        .CommandText = sConsulta
        Set adoParam = .CreateParameter("ANYO", adSmallInt, adParamInput, , m_oUltimaOferta.Anyo)
        .Parameters.Append adoParam
        Set adoParam = .CreateParameter("GMN1", adVarChar, adParamInput, Len(m_oUltimaOferta.GMN1Cod), m_oUltimaOferta.GMN1Cod)
        .Parameters.Append adoParam
        Set adoParam = .CreateParameter("PROCE", adInteger, adParamInput, , m_oUltimaOferta.Proce)
        .Parameters.Append adoParam
        Set adoParam = .CreateParameter("ITEM", adInteger, adParamInput, , lIdItem)
        .Parameters.Append adoParam
        Set adoParam = .CreateParameter("PROVE", adVarChar, adParamInput, Len(m_oUltimaOferta.Prove), m_oUltimaOferta.Prove)
        .Parameters.Append adoParam
        Set adoParam = .CreateParameter("OFE", adSmallInt, adParamInput, , m_oUltimaOferta.Num)
        .Parameters.Append adoParam
        Set adoParam = .CreateParameter("ATRIB_ID", adInteger, adParamInput, , m_vIDAtribProce)
        .Parameters.Append adoParam
        Select Case m_oObjeto.Tipo
            Case TipoNumerico
                If bHayValor Then
                    Set adoParam = .CreateParameter("VALOR_NUM", adDouble, adParamInput, , m_vValorNum)
                    .Parameters.Append adoParam
                End If
            Case TipoFecha
                If bHayValor Then
                    Set adoParam = .CreateParameter("VALOR_FEC", adDBDate, adParamInput, , m_vValorFec)
                    .Parameters.Append adoParam
                End If
            Case TipoString
                If bHayValor Then
                    Set adoParam = .CreateParameter("VALOR_TEXT", adVarChar, adParamInput, Len(m_vValorText), m_vValorText)
                    .Parameters.Append adoParam
                End If
            Case TipoBoolean
                If bHayValor Then
                    Set adoParam = .CreateParameter("VALOR_BOOL", adTinyInt, adParamInput, , m_vValorBool)
                    .Parameters.Append adoParam
                End If
        End Select
            
        .Execute
    End With
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    
    If Not bHayTran Then
        m_oConexion.ADOCon.Execute "SET XACT_ABORT OFF"
        m_oConexion.ADOCon.Execute "COMMIT TRAN"
    End If
    
    GuardarCambiosOfertaAtributo = TESError

Salir:
    Set adoComm = Nothing
    Set adoParam = Nothing
    Set rs = Nothing
    Exit Function
Error_Cls:
    GuardarCambiosOfertaAtributo = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    If Not bHayTran Then
        m_oConexion.ADOCon.Execute "ROLLBACK TRAN"
        m_oConexion.ADOCon.Execute "SET XACT_ABORT OFF"
    End If
    Resume Salir
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      'Call g_oErrores.TratarError("Class Module", "CAtributoOfertado", "GuardarCambiosOfertaAtributo", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If
End Function

'''<summary>Establece el valor del atributo , sea cual sea el tipo</summary>
Public Function setValor(ByVal Valor As Variant) As Integer

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    setValor = 0
    Select Case m_udtTipo
        
        Case TiposDeAtributos.TipoBoolean
            If Valor = "0" Or Valor = "1" Then
                m_vValorBool = Valor
            Else
                setValor = 1
            End If
        Case TiposDeAtributos.TipoFecha
            If IsDate(Valor) Then
                m_vValorFec = Valor
            Else
                setValor = 2
            End If
        Case TiposDeAtributos.TipoNumerico
            If IsNumeric(Valor) Then
                m_vValorNum = Valor
            Else
                setValor = 3
            End If
        Case TiposDeAtributos.TipoString
            If TypeName(Valor) = "String" Then
                m_vValorText = Valor
            Else
                setValor = 4
            End If
    End Select
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CAtributo", "setValor", ERR, Erl)
      Exit Function
   End If
    
End Function

Public Property Get OrdenarIntegracion() As Boolean
    OrdenarIntegracion = m_bOrdenarIntegracion
End Property

Public Property Let OrdenarIntegracion(ByVal vData As Boolean)
    m_bOrdenarIntegracion = vData
End Property
