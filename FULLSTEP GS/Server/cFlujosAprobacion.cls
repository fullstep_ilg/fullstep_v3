VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "cFlujosAprobacion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum
Private mCol As Collection
Private moConexion As CConexion

Friend Property Set Conexion(ByVal vData As CConexion)
    Set moConexion = vData
End Property


Friend Property Get Conexion() As CConexion
    Set Conexion = moConexion
End Property

''' <summary>
''' Crea y carga el objeto
''' </summary>
''' <param name="Categoria">Categoria</param>
''' <param name="NivelCat">Nivel Categoria</param>
''' <param name="TipoPedido">Tipo Pedido</param>
''' <param name="TipoPedidoDen">Den Tipo Pedido</param>
''' <param name="Emp">Empresa</param>
''' <param name="EmpDen">Den Empresa</param>
''' <param name="Solicitud">Solicitud</param>
''' <param name="SolicitudDen">Den Solicitud</param>
''' <param name="TipoSolicitud">Tipo Solicitud</param>
''' <param name="Formulario">Formulario Solicitud</param>
''' <param name="Workflow">Workflow Solicitud</param>
''' <param name="CodCategoria">C�digo Categoria</param>
''' <param name="CodCompletoCategoria">C�digo Categoria</param>
''' <param name="DenCategoria">Den Categoria</param>
''' <returns>objeto cFlujoAprobacion</returns>
''' <remarks>Llamada desde: frmCatalogoconfiguracion    frmCAtSeguridad; Tiempo m�ximo: 0,2</remarks>
Public Function Add(ByVal Categoria As Long, ByVal NivelCat As Long, ByVal TipoPedido As Long, ByVal TipoPedidoDen As String, ByVal Emp As Long, ByVal EmpDen As String, ByVal Solicitud As Long, ByVal SolicitudDen As String _
, ByVal TipoSolicitud As Long, ByVal Formulario As Long, ByVal Workflow As Long, Optional ByVal CodCategoria As String, Optional ByVal CodCompletoCategoria As String, Optional ByVal DenCategoria As String) As cFlujoAprobacion
    
    Dim objnewmember As cFlujoAprobacion
    Set objnewmember = New cFlujoAprobacion
    
    
    With objnewmember
        .Categoria = Categoria
        .NivelCat = NivelCat
        .TipoPedido = TipoPedido
        .TipoPedidoDen = TipoPedidoDen
        .Emp = Emp
        .EmpDen = EmpDen
        .Solicitud = Solicitud
        .SolicitudDen = SolicitudDen
        .TipoSolicitud = TipoSolicitud
        .Formulario = Formulario
        .Workflow = Workflow
        If Not IsEmpty(CodCategoria) Then
            .CodCategoria = CodCategoria
        End If
        If Not IsEmpty(CodCompletoCategoria) Then
            .CodCompletoCategoria = CodCompletoCategoria
        End If
        If Not IsEmpty(DenCategoria) Then
            .DenCategoria = DenCategoria
        End If
        Set .Conexion = Conexion
    End With
    
    mCol.Add objnewmember, CStr(Categoria) & "_" & CStr(NivelCat) & "_" & CStr(TipoPedido) & "_" & CStr(Emp) & "_" & CStr(Solicitud)
    
    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing


End Function

Public Property Get Item(vntIndexKey As Variant) As cFlujoAprobacion

On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:

    Set Item = Nothing
    
End Property



Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
     Count = mCol.Count
End If


End Property


Public Sub Remove(vntIndexKey As Variant)
   
    mCol.Remove vntIndexKey
End Sub


Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
     Set NewEnum = mCol.[_NewEnum]
End Property


Private Sub Class_Initialize()
 
    Set mCol = New Collection
End Sub


Private Sub Class_Terminate()
    
    Set mCol = Nothing

End Sub

''' <summary>Devuelvo un recordset con todas las categorias de los flujos de aprobacion</summary>
Public Function CargarCategoriasFlujosAprobacion() As ADODB.Recordset
    Dim sConsulta As String
    Dim rs As New ADODB.Recordset
    
    sConsulta = "SELECT DISTINCT CTES.CAT,CTES.COD COD_CAT_COMPLETO,CTES.COD_CAT,CTES.DEN,CTES.NIVEL FROM ("
    sConsulta = sConsulta & " SELECT C1.COD,C1.COD COD_CAT ,C1.COD AS CATN1, NULL AS CATN2, NULL AS CATN3, NULL AS CATN4, NULL AS CATN5, DEN,"
    sConsulta = sConsulta & " CTES.Cat , CTES.Nivel, CTES.TipoPedido, CTES.Solicitud, CTES.Emp"
    sConsulta = sConsulta & " FROM CATN_TIPOPEDIDO_EMP_SOLICITUD CTES WITH(NOLOCK)"
    sConsulta = sConsulta & " INNER JOIN CATN1 C1 WITH(NOLOCK) ON CTES.CAT = C1.ID AND CTES.NIVEL=1"
    sConsulta = sConsulta & " Union"
    sConsulta = sConsulta & " SELECT C1.COD + ' ' + C2.COD COD,C2.COD COD_CAT ,C1.COD AS CATN1, C2.COD AS CATN2, NULL AS CATN3, NULL AS CATN4, NULL AS CATN5, C2.DEN,"
    sConsulta = sConsulta & " CTES.Cat , CTES.Nivel, CTES.TipoPedido, CTES.Solicitud, CTES.Emp"
    sConsulta = sConsulta & " FROM CATN_TIPOPEDIDO_EMP_SOLICITUD CTES WITH(NOLOCK)"
    sConsulta = sConsulta & " INNER JOIN CATN2 C2 WITH(NOLOCK) ON CTES.CAT = C2.ID AND CTES.NIVEL=2"
    sConsulta = sConsulta & " INNER JOIN CATN1 C1 WITH(NOLOCK) ON C1.ID = C2.CAT1"
    sConsulta = sConsulta & " Union"
    sConsulta = sConsulta & " SELECT C1.COD + ' ' + C2.COD + ' ' + C3.COD COD,C3.COD COD_CAT, C1.COD AS CATN1, C2.COD AS CATN2, C3.COD AS CATN3, NULL AS CATN4, NULL AS CATN5, C3.DEN,"
    sConsulta = sConsulta & " CTES.Cat , CTES.Nivel, CTES.TipoPedido, CTES.Solicitud, CTES.Emp"
    sConsulta = sConsulta & " FROM CATN_TIPOPEDIDO_EMP_SOLICITUD CTES WITH(NOLOCK)"
    sConsulta = sConsulta & " INNER JOIN CATN3 C3 WITH(NOLOCK) ON CTES.CAT = C3.ID AND CTES.NIVEL=3"
    sConsulta = sConsulta & " INNER JOIN CATN2 C2 WITH(NOLOCK) ON C2.ID = C3.CAT2 AND C2.CAT1 = C3.CAT1"
    sConsulta = sConsulta & " INNER JOIN CATN1 C1 WITH(NOLOCK) ON C1.ID = C3.CAT1"
    sConsulta = sConsulta & " Union"
    sConsulta = sConsulta & " SELECT C1.COD + ' ' + C2.COD + ' ' + C3.COD + ' ' + C4.COD COD,C4.COD COD_CAT ,C1.COD AS CATN1, C2.COD AS CATN2, C3.COD AS CATN3, NULL AS CATN4, NULL AS CATN5, C4.DEN,"
    sConsulta = sConsulta & " CTES.Cat , CTES.Nivel, CTES.TipoPedido, CTES.Solicitud, CTES.Emp"
    sConsulta = sConsulta & " FROM CATN_TIPOPEDIDO_EMP_SOLICITUD CTES WITH(NOLOCK)"
    sConsulta = sConsulta & " INNER JOIN CATN4 C4 WITH(NOLOCK) ON CTES.CAT = C4.ID AND CTES.NIVEL=4"
    sConsulta = sConsulta & " INNER JOIN CATN3 C3 WITH(NOLOCK) ON C4.CAT3 = C3.ID AND C4.CAT2=C3.CAT2 AND C4.CAT1=C3.CAT1"
    sConsulta = sConsulta & " INNER JOIN CATN2 C2 WITH(NOLOCK) ON C2.ID = C3.CAT2 AND C2.CAT1 = C3.CAT1"
    sConsulta = sConsulta & " INNER JOIN CATN1 C1 WITH(NOLOCK) ON C1.ID = C3.CAT1"
    sConsulta = sConsulta & " Union"
    sConsulta = sConsulta & " SELECT C1.COD + ' ' + C2.COD + ' ' + C3.COD + ' ' + C4.COD + ' ' + C5.COD COD ,C5.COD COD_CAT, C1.COD AS CATN1, C2.COD AS CATN2, C3.COD AS CATN3, NULL AS CATN4, NULL AS CATN5, C5.DEN,"
    sConsulta = sConsulta & " CTES.Cat , CTES.Nivel, CTES.TipoPedido, CTES.Solicitud, CTES.Emp"
    sConsulta = sConsulta & " FROM CATN_TIPOPEDIDO_EMP_SOLICITUD CTES WITH(NOLOCK)"
    sConsulta = sConsulta & " INNER JOIN CATN5 C5 WITH(NOLOCK) ON CTES.CAT = C5.ID AND CTES.NIVEL=5"
    sConsulta = sConsulta & " INNER JOIN CATN4 C4 WITH(NOLOCK) ON C5.CAT4 = C4.ID AND C5.CAT3=C4.CAT3 AND C5.CAT2=C4.CAT2 AND C5.CAT1=C4.CAT1"
    sConsulta = sConsulta & " INNER JOIN CATN3 C3 WITH(NOLOCK) ON C4.CAT3 = C3.ID AND C4.CAT2=C3.CAT2 AND C4.CAT1=C3.CAT1"
    sConsulta = sConsulta & " INNER JOIN CATN2 C2 WITH(NOLOCK) ON C2.ID = C3.CAT2 AND C2.CAT1 = C3.CAT1"
    sConsulta = sConsulta & " INNER JOIN CATN1 C1 WITH(NOLOCK) ON C1.ID = C3.CAT1 ) CTES"
    
    
    rs.Open sConsulta, moConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    Set CargarCategoriasFlujosAprobacion = rs
End Function

''' <summary>Creo la coleccion con todos los flujos de aprobacion para todas las categorias</summary>
Public Sub CargarTodosFlujosAprobacion(Optional ByVal TipoPedido As String, Optional ByVal Empresa As String, Optional ByVal Solicitud As String, Optional ByVal Categoria As String, Optional ByVal NivelCategoria As String)
    Dim sConsulta As String
    Dim rs As New ADODB.Recordset
    
    sConsulta = "SELECT S.TIPO,S.FORMULARIO,S.WORKFLOW,CTES.CAT,CTES.COD COD_CAT_COMPLETO,CTES.COD_CAT,CTES.DEN,CTES.NIVEL,CTES.TIPOPEDIDO,CTES.SOLICITUD,CTES.EMP,E.DEN AS EMP_DEN,S.DEN_SPA AS SOLICITUD_DEN ,TPD.DEN AS TIPOPEDIDO_DEN FROM ("
    sConsulta = sConsulta & " SELECT C1.COD,C1.COD COD_CAT ,C1.COD AS CATN1, NULL AS CATN2, NULL AS CATN3, NULL AS CATN4, NULL AS CATN5, DEN,"
    sConsulta = sConsulta & " CTES.Cat , CTES.Nivel, CTES.TipoPedido, CTES.Solicitud, CTES.Emp"
    sConsulta = sConsulta & " FROM CATN_TIPOPEDIDO_EMP_SOLICITUD CTES WITH(NOLOCK)"
    sConsulta = sConsulta & " INNER JOIN CATN1 C1 WITH(NOLOCK) ON CTES.CAT = C1.ID AND CTES.NIVEL=1"
    sConsulta = sConsulta & " Union"
    sConsulta = sConsulta & " SELECT C1.COD + ' ' + C2.COD COD,C2.COD COD_CAT ,C1.COD AS CATN1, C2.COD AS CATN2, NULL AS CATN3, NULL AS CATN4, NULL AS CATN5, C2.DEN,"
    sConsulta = sConsulta & " CTES.Cat , CTES.Nivel, CTES.TipoPedido, CTES.Solicitud, CTES.Emp"
    sConsulta = sConsulta & " FROM CATN_TIPOPEDIDO_EMP_SOLICITUD CTES WITH(NOLOCK)"
    sConsulta = sConsulta & " INNER JOIN CATN2 C2 WITH(NOLOCK) ON CTES.CAT = C2.ID AND CTES.NIVEL=2"
    sConsulta = sConsulta & " INNER JOIN CATN1 C1 WITH(NOLOCK) ON C1.ID = C2.CAT1"
    sConsulta = sConsulta & " Union"
    sConsulta = sConsulta & " SELECT C1.COD + ' ' + C2.COD + ' ' + C3.COD COD,C3.COD COD_CAT, C1.COD AS CATN1, C2.COD AS CATN2, C3.COD AS CATN3, NULL AS CATN4, NULL AS CATN5, C3.DEN,"
    sConsulta = sConsulta & " CTES.Cat , CTES.Nivel, CTES.TipoPedido, CTES.Solicitud, CTES.Emp"
    sConsulta = sConsulta & " FROM CATN_TIPOPEDIDO_EMP_SOLICITUD CTES WITH(NOLOCK)"
    sConsulta = sConsulta & " INNER JOIN CATN3 C3 WITH(NOLOCK) ON CTES.CAT = C3.ID AND CTES.NIVEL=3"
    sConsulta = sConsulta & " INNER JOIN CATN2 C2 WITH(NOLOCK) ON C2.ID = C3.CAT2 AND C2.CAT1 = C3.CAT1"
    sConsulta = sConsulta & " INNER JOIN CATN1 C1 WITH(NOLOCK) ON C1.ID = C3.CAT1"
    sConsulta = sConsulta & " Union"
    sConsulta = sConsulta & " SELECT C1.COD + ' ' + C2.COD + ' ' + C3.COD + ' ' + C4.COD COD,C4.COD COD_CAT ,C1.COD AS CATN1, C2.COD AS CATN2, C3.COD AS CATN3, NULL AS CATN4, NULL AS CATN5, C4.DEN,"
    sConsulta = sConsulta & " CTES.Cat , CTES.Nivel, CTES.TipoPedido, CTES.Solicitud, CTES.Emp"
    sConsulta = sConsulta & " FROM CATN_TIPOPEDIDO_EMP_SOLICITUD CTES WITH(NOLOCK)"
    sConsulta = sConsulta & " INNER JOIN CATN4 C4 WITH(NOLOCK) ON CTES.CAT = C4.ID AND CTES.NIVEL=4"
    sConsulta = sConsulta & " INNER JOIN CATN3 C3 WITH(NOLOCK) ON C4.CAT3 = C3.ID AND C4.CAT2=C3.CAT2 AND C4.CAT1=C3.CAT1"
    sConsulta = sConsulta & " INNER JOIN CATN2 C2 WITH(NOLOCK) ON C2.ID = C3.CAT2 AND C2.CAT1 = C3.CAT1"
    sConsulta = sConsulta & " INNER JOIN CATN1 C1 WITH(NOLOCK) ON C1.ID = C3.CAT1"
    sConsulta = sConsulta & " Union"
    sConsulta = sConsulta & " SELECT C1.COD + ' ' + C2.COD + ' ' + C3.COD + ' ' + C4.COD + ' ' + C5.COD COD ,C5.COD COD_CAT, C1.COD AS CATN1, C2.COD AS CATN2, C3.COD AS CATN3, NULL AS CATN4, NULL AS CATN5, C5.DEN,"
    sConsulta = sConsulta & " CTES.Cat , CTES.Nivel, CTES.TipoPedido, CTES.Solicitud, CTES.Emp"
    sConsulta = sConsulta & " FROM CATN_TIPOPEDIDO_EMP_SOLICITUD CTES WITH(NOLOCK)"
    sConsulta = sConsulta & " INNER JOIN CATN5 C5 WITH(NOLOCK) ON CTES.CAT = C5.ID AND CTES.NIVEL=5"
    sConsulta = sConsulta & " INNER JOIN CATN4 C4 WITH(NOLOCK) ON C5.CAT4 = C4.ID AND C5.CAT3=C4.CAT3 AND C5.CAT2=C4.CAT2 AND C5.CAT1=C4.CAT1"
    sConsulta = sConsulta & " INNER JOIN CATN3 C3 WITH(NOLOCK) ON C4.CAT3 = C3.ID AND C4.CAT2=C3.CAT2 AND C4.CAT1=C3.CAT1"
    sConsulta = sConsulta & " INNER JOIN CATN2 C2 WITH(NOLOCK) ON C2.ID = C3.CAT2 AND C2.CAT1 = C3.CAT1"
    sConsulta = sConsulta & " INNER JOIN CATN1 C1 WITH(NOLOCK) ON C1.ID = C3.CAT1 ) CTES"
    sConsulta = sConsulta & " INNER JOIN EMP E WITH (NOLOCK) ON E.ID=CTES.EMP"
    sConsulta = sConsulta & " INNER JOIN TIPOPEDIDO TP WITH (NOLOCK) ON TP.ID=CTES.TIPOPEDIDO"
    sConsulta = sConsulta & " INNER JOIN TIPOPEDIDO_DEN TPD WITH (NOLOCK) ON TPD.TIPOPEDIDO=TP.ID AND TPD.IDI='" & gParametrosGenerales.gIdioma & "'"
    sConsulta = sConsulta & " INNER JOIN SOLICITUD S WITH (NOLOCK) ON S.ID=CTES.SOLICITUD"
    sConsulta = sConsulta & " WHERE 1=1 "
    If TipoPedido <> "" Then
        sConsulta = sConsulta & " AND TP.ID=" & TipoPedido
    End If
    If Empresa <> "" Then
        sConsulta = sConsulta & " AND E.ID=" & Empresa
    End If
    If Solicitud <> "" Then
        sConsulta = sConsulta & " AND S.ID=" & Solicitud
    End If
    If Categoria <> "" Then
        sConsulta = sConsulta & " AND CTES.CAT=" & Categoria & " AND CTES.NIVEL=" & NivelCategoria
    End If
    rs.Open sConsulta, moConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    While Not rs.eof
        Me.Add rs("CAT").Value, rs("NIVEL").Value, rs("TIPOPEDIDO").Value, rs("TIPOPEDIDO_DEN").Value, rs("EMP").Value, rs("EMP_DEN").Value, rs("SOLICITUD").Value, rs("SOLICITUD_DEN").Value, rs("TIPO").Value, rs("FORMULARIO").Value, NullToDbl0(rs("WORKFLOW").Value), rs("COD_CAT").Value, rs("COD_CAT_COMPLETO").Value, rs("DEN").Value
        rs.MoveNext
    Wend
    Set rs = Nothing
End Sub
