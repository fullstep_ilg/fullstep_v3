VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CImagen"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private mvarArticulo As CArticulo
Private mvarProveedor As String
Private mvarData As Variant
Private mvarDataSize As Long
Private mvarConexion As CConexion
Private mvarRecordset As adodb.Recordset
Private mvarAdoCom As adodb.Command

Public Property Get Articulo() As CArticulo
    Set Articulo = mvarArticulo
End Property

Public Property Set Articulo(ByVal Data As CArticulo)
    Set mvarArticulo = Data
End Property
Public Property Let Proveedor(ByVal Data As String)
    Let mvarProveedor = Data
End Property
Public Property Get Proveedor() As String
    Proveedor = mvarProveedor
End Property
Public Property Let DataImagen(ByVal Data As Variant)
    Let mvarData = Data
End Property
Public Property Get DataImagen() As Variant
    DataImagen = mvarData
End Property

Public Property Let dataSize(ByVal Data As Long)
    Let mvarDataSize = Data
End Property
Public Property Get dataSize() As Long
    dataSize = mvarDataSize
End Property
Friend Property Set Conexion(ByVal con As CConexion)
Set mvarConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = mvarConexion
End Property

Private Sub Class_Initialize()
    Set mvarArticulo = New CArticulo
        mvarDataSize = 0
End Sub

Private Sub Class_Terminate()
     Set mvarArticulo = Nothing
     Set mvarConexion = Nothing
     Set mvarRecordset = Nothing
End Sub

''' <summary>Lectura de datos</summary>
''' <returns>Variable tipo TipoErrorSummit con el error si se ha producido</returns>
''' <remarks>Llamada desde: FSGSClient</remarks>
''' <revision>LTG 09/01/2012</revision>

Public Function ComenzarLecturaData() As TipoErrorSummit
    Dim TESError As TipoErrorSummit
    Dim sConsulta As String
    '********* Precondicion **************************************
    If mvarConexion Is Nothing Then
        ERR.Raise vbObjectError + 613, "CImagen.ComenzarLecturaData", "No se ha establecido la conexion"
        Exit Function
    End If
    '*************************************************************
    
    On Error GoTo Error
    
    TESError.NumError = TESnoerror
    
    sConsulta = "Select IMAGEN from PROVE_ART4 WITH (NOLOCK) where ART='" & DblQuote(mvarArticulo.Cod) & "' AND PROVE='" & DblQuote(mvarProveedor) & "'"
    
    Set mvarRecordset = New adodb.Recordset
    mvarRecordset.Open sConsulta, mvarConexion.ADOCon, adOpenStatic, adLockReadOnly
        
    If mvarRecordset.eof Then
        TESError.NumError = TESDatoEliminado
        TESError.Arg1 = 136 'Art�culo asignado al proveedor
        mvarDataSize = 0
        mvarRecordset.Close
        Set mvarRecordset = Nothing
        ComenzarLecturaData = TESError
        Exit Function
    End If
    
    mvarDataSize = mvarRecordset("IMAGEN").ActualSize
    ComenzarLecturaData = TESError
    Exit Function
    
Error:
    ComenzarLecturaData = basErrores.TratarError(mvarConexion.ADOCon.Errors)
    If Not mvarRecordset Is Nothing Then
        mvarRecordset.Close
        Set mvarRecordset = Nothing
    End If


End Function

Public Function ReadData(ByVal lBytes As Long) As Variant
Dim Chunk As Variant

On Error GoTo Error:
    'chunk = mvarRecordset("IMAGEN").GetChunk(lBytes)
    Chunk = mvarRecordset(0).GetChunk(&HFFFFFF)
    ReadData = Chunk
    Exit Function
    
Error:
    
    If Not mvarRecordset Is Nothing Then
        mvarRecordset.Close
    End If
    Set mvarRecordset = Nothing
    ReadData = Null
    
End Function

Public Sub FinalizarLecturaData()
    'Cierra el recordset
    If Not mvarRecordset Is Nothing Then
        mvarRecordset.Close
    End If
    
    Set mvarRecordset = Nothing
    
End Sub
''' <summary>
''' Inserta o modifica la imagen o thumbnail.
''' </summary>
''' <param name="iTipo">Si 0 la imagen, si 1 thumbnail.</param>
''' <returns>Un errorsummit</returns>
''' <remarks>Llamada desde: frmCATDatExtImagen.cmdSustutuirThumb_Click,frmCATDatExtImagen.cmdSustuturIma_Click; Tiempo m�ximo: 1 seg</remarks>
''' Revisada EPB 29/08/2011
Public Function ModificarImagen(ByVal iTipo As Integer) As TipoErrorSummit
Dim TESError As TipoErrorSummit
Dim sConsulta As String
Dim sCampo As String

On Error GoTo Error

    If iTipo = 1 Then
        sCampo = "THUMBNAIL"
    Else
        sCampo = "IMAGEN"
    End If
    'Sin with nolock pq es para ver si existe
    sConsulta = "SELECT " & sCampo & " FROM PROVE_ART4 WHERE ART='" & DblQuote(mvarArticulo.Cod) & "' AND PROVE='" & DblQuote(mvarProveedor) & "'"
    
    Set mvarRecordset = New adodb.Recordset
    mvarRecordset.Open sConsulta, mvarConexion.ADOCon, adOpenKeyset, adLockOptimistic
    
    If mvarRecordset.eof Then 'Si no existe el registro se inserta
        'TESError.NumError = TESDatoEliminado
        'TESError.Arg1 = 136 'Art�culo asignado al provedor
        mvarConexion.ADOCon.Execute "INSERT INTO PROVE_ART4 (PROVE, ART,HOM) VALUES ('" & DblQuote(mvarProveedor) & "', '" & DblQuote(mvarArticulo.Cod) & "',0)"

        mvarRecordset.Close
        mvarRecordset.Open sConsulta, mvarConexion.ADOCon, adOpenKeyset, adLockOptimistic
    End If

    'Modifica la imagen
    If Not IsEmpty(mvarData) Then _
        mvarRecordset(0).AppendChunk mvarData

    mvarRecordset.Update
    mvarRecordset.Close
    Set mvarRecordset = Nothing
    ModificarImagen = TESError
    
Salir:
    ''' Cierre
    On Error Resume Next
    If Not mvarRecordset Is Nothing Then
        If mvarRecordset.EditMode > 0 Then
            mvarRecordset.CancelUpdate
        End If
        mvarRecordset.Close
        Set mvarRecordset = Nothing
    End If
    

    Exit Function

Error:
    ModificarImagen = basErrores.TratarError(mvarConexion.ADOCon.Errors)
    
End Function

''' <summary>
''' Abre la lectura de la imagen thimbnail
''' </summary>
''' <returns>Un errorsummit</returns>
''' <remarks>Llamada desde: frmCATDatExtImagen.cmdSustutuirThumb_Click,frmCATDatExtImagen.cmdSustuturIma_Click; Tiempo m�ximo: 1 seg</remarks>
''' Revisada EPB 29/08/2011
Public Function ComenzarLecturaThumbnail() As TipoErrorSummit
    Dim TESError As TipoErrorSummit
    Dim sConsulta As String
    
    '********* Precondicion **************************************
    If mvarConexion Is Nothing Then
        ERR.Raise vbObjectError + 613, "CImagen.ComenzarLecturaThumbnail", "No se ha establecido la conexion"
        Exit Function
    End If
    '*************************************************************
    
    On Error GoTo Error
    
    TESError.NumError = TESnoerror
    'Sin nolock pq es para ver que existe
    sConsulta = "SELECT THUMBNAIL FROM PROVE_ART4 WHERE ART='" & DblQuote(mvarArticulo.Cod) & "' AND PROVE='" & DblQuote(mvarProveedor) & "'"
    
    Set mvarRecordset = New adodb.Recordset
    mvarRecordset.Open sConsulta, mvarConexion.ADOCon, adOpenStatic, adLockReadOnly
    
    If mvarRecordset.eof Then
        'TESError.NumError = TESDatoEliminado
        'TESError.Arg1 = 136 'Art�culo asignado al proveedor
        mvarDataSize = 0
        mvarRecordset.Close
        Set mvarRecordset = Nothing
        ComenzarLecturaThumbnail = TESError
        Exit Function
    End If
    
    mvarDataSize = mvarRecordset("THUMBNAIL").ActualSize
    ComenzarLecturaThumbnail = TESError
    Exit Function
    
Error:
    ComenzarLecturaThumbnail = basErrores.TratarError(mvarConexion.ADOCon.Errors)
    If Not mvarRecordset Is Nothing Then
        mvarRecordset.Close
        Set mvarRecordset = Nothing
    End If


End Function

''' <summary>
''' Elimina la imagen o thumbnail.
''' </summary>
''' <param name="iTipo">Si 0 eliminar la imagen, si 1 eliminar thumbnail.</param>
''' <returns>Un errorsummit</returns>
''' <remarks>Llamada desde: frmCATDatExtImagen.cmdEliminarThumb_Click,frmCATDatExtImagen.cmdEliminarIma_Click; Tiempo m�ximo: 1 seg</remarks>

Public Function EliminarImagen(ByVal iTipo As Integer) As TipoErrorSummit
'------------------------------------------------------------------
'iTipo, 0 --> Eliminar la imagen       1 --> Eliminar el thumbnail
'------------------------------------------------------------------
Dim TESError As TipoErrorSummit
Dim sConsulta As String
Dim sCampo As String
Dim sCampo2 As String
Dim bEliminarRegistro As Boolean

    '********* Precondicion **************************************
    If mvarConexion Is Nothing Then
        ERR.Raise vbObjectError + 613, "CImagen.EliminarImagen", "No se ha establecido la conexion"
        Exit Function
    End If
    '*************************************************************
    
    On Error GoTo Error
    
    TESError.NumError = TESnoerror
    If iTipo = 1 Then
        sCampo = "THUMBNAIL"
        sCampo2 = "IMAGEN"
    Else
        sCampo = "IMAGEN"
        sCampo2 = "THUMBNAIL"
    End If
    mvarConexion.ADOCon.Execute "BEGIN TRANSACTION"
    
    sConsulta = "SELECT HOM,COD_EXT,IMAGEN,THUMBNAIL,OBSADJUN FROM PROVE_ART4 WITH (NOLOCK) WHERE ART='" & DblQuote(mvarArticulo.Cod) & "' AND PROVE='" & DblQuote(mvarProveedor) & "'"
    
    Set mvarRecordset = New adodb.Recordset
    mvarRecordset.Open sConsulta, mvarConexion.ADOCon, adOpenStatic, adLockReadOnly
    mvarDataSize = 0
    
    If mvarRecordset.eof Then
        TESError.NumError = TESDatoEliminado
        TESError.Arg1 = 136 'Art�culo asignado al provedor
        mvarRecordset.Close
        Set mvarRecordset = Nothing
        EliminarImagen = TESError
        Exit Function
    End If
    
    'Si el resto de campos de PROVE_ART4 est� vacio (HOM=0, COD_EXT=NULL,IMAGEN=NULL o THUMBNAIL=NULL,OBSADJUN=NULL)
    'y no hay registros en PROVE_ART4_ADJUN ni en PROVE_ART4_ATRIB para el prove-articulo, se elimina el registro de PROVE_ART4.
    bEliminarRegistro = False
    If sCampo = "THUMBNAIL" Then
        If mvarRecordset.Fields("HOM").Value = 0 And IsNull(mvarRecordset.Fields("COD_EXT").Value) And IsNull(mvarRecordset.Fields("IMAGEN").Value) And IsNull(mvarRecordset.Fields("OBSADJUN").Value) Then
            mvarRecordset.Close
            sConsulta = "SELECT PROVE FROM PROVE_ART4_ADJUN WITH (NOLOCK) WHERE ART='" & DblQuote(mvarArticulo.Cod) & "' AND PROVE='" & DblQuote(mvarProveedor) & "'"
            mvarRecordset.Open sConsulta, mvarConexion.ADOCon, adOpenStatic, adLockReadOnly
            If mvarRecordset.eof Then 'No hay registros en PROVE_ART4_ADJUN
                mvarRecordset.Close
                sConsulta = "SELECT PROVE FROM PROVE_ART4_ATRIB WITH (NOLOCK) WHERE ART='" & DblQuote(mvarArticulo.Cod) & "' AND PROVE='" & DblQuote(mvarProveedor) & "'"
                mvarRecordset.Open sConsulta, mvarConexion.ADOCon, adOpenStatic, adLockReadOnly
                If mvarRecordset.eof Then 'No hay registros en PROVE_ART4_ATRIB
                    bEliminarRegistro = True
                Else
                    mvarRecordset.Close
                End If
            Else
                mvarRecordset.Close
            End If
        End If
    Else
        If mvarRecordset.Fields("HOM").Value = 0 And IsNull(mvarRecordset.Fields("COD_EXT").Value) And IsNull(mvarRecordset.Fields("THUMBNAIL").Value) And IsNull(mvarRecordset.Fields("OBSADJUN").Value) Then
            mvarRecordset.Close
            sConsulta = "SELECT PROVE FROM PROVE_ART4_ADJUN WITH (NOLOCK) WHERE ART='" & DblQuote(mvarArticulo.Cod) & "' AND PROVE='" & DblQuote(mvarProveedor) & "'"
            mvarRecordset.Open sConsulta, mvarConexion.ADOCon, adOpenStatic, adLockReadOnly
            If mvarRecordset.eof Then 'No hay registros en PROVE_ART4_ADJUN
                mvarRecordset.Close
                sConsulta = "SELECT PROVE FROM PROVE_ART4_ATRIB WITH (NOLOCK) WHERE ART='" & DblQuote(mvarArticulo.Cod) & "' AND PROVE='" & DblQuote(mvarProveedor) & "'"
                mvarRecordset.Open sConsulta, mvarConexion.ADOCon, adOpenStatic, adLockReadOnly
                If mvarRecordset.eof Then 'No hay registros en PROVE_ART4_ATRIB
                    bEliminarRegistro = True
                Else
                    mvarRecordset.Close
                End If
            Else
                mvarRecordset.Close
            End If
        End If
    End If
    Set mvarRecordset = Nothing

    If bEliminarRegistro Then
        sConsulta = "DELETE FROM PROVE_ART4 WHERE ART='" & DblQuote(mvarArticulo.Cod) & "' AND PROVE='" & DblQuote(mvarProveedor) & "'"
    Else
        sConsulta = "UPDATE PROVE_ART4 SET " & sCampo & " = NULL WHERE ART='" & DblQuote(mvarArticulo.Cod) & "' AND PROVE='" & DblQuote(mvarProveedor) & "'"
    End If
    
    mvarConexion.ADOCon.Execute sConsulta
    If mvarConexion.ADOCon.Errors.Count > 0 Then GoTo Error
    mvarDataSize = 0
    
    
    mvarConexion.ADOCon.Execute "COMMIT TRANSACTION"
    EliminarImagen = TESError
    
FinalizarFunct:
    On Error Resume Next
    If Not mvarRecordset Is Nothing Then
        mvarRecordset.Close
        Set mvarRecordset = Nothing
    End If
    
    Exit Function
    
Error:
    EliminarImagen = basErrores.TratarError(mvarConexion.ADOCon.Errors)
    mvarConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
    Resume FinalizarFunct


End Function

