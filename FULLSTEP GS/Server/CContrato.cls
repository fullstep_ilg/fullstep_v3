VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CContrato"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
''' *** Clase: CContrato
''' *** Creacion: 02/12/2002 (EPB)

Option Explicit
Option Base 0

Implements IBaseDatos

''' Variables privadas con la informacion de un Tipo contrato

Private m_lID As Variant
Private m_iAnyo As Integer
Private m_sGMN1Cod As String
Private m_iProceCod As Long
Private m_sProveCod As String
Private m_sNombre As String
Private m_sExtension As String
Private m_lDataSize As Long
Private m_sUsuario As String
Private m_vFecAct As Variant

Private m_lIndice As Long

''' Conexion
Private m_oConexion As CConexion

''' Control de errores
Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum


Public Property Get Indice() As Long

    Indice = m_lIndice
    
End Property
Public Property Let Indice(ByVal lInd As Long)

    m_lIndice = lInd
    
End Property

Friend Property Set Conexion(ByVal vData As CConexion)

    Set m_oConexion = vData
    
End Property

Friend Property Get Conexion() As CConexion

    Set Conexion = m_oConexion
    
End Property
Public Property Let Nombre(ByVal vData As String)

    m_sNombre = vData
    
End Property
Public Property Get Nombre() As String

    Nombre = m_sNombre
    
End Property
Public Property Let Extension(ByVal vData As String)

    m_sExtension = vData
    
End Property
Public Property Get Extension() As String

    Extension = m_sExtension
    
End Property

Public Property Get DataSize() As Long

    DataSize = m_lDataSize
    
End Property
Public Property Let DataSize(ByVal lInd As Long)

    m_lDataSize = lInd
    
End Property

Public Property Let Id(ByVal vData As Variant)

    m_lID = vData
    
End Property
Public Property Get Id() As Variant

    Id = m_lID
    
End Property

Public Property Let Anyo(ByVal vData As Integer)

    m_iAnyo = vData
    
End Property
Public Property Get Anyo() As Integer
    Anyo = m_iAnyo
    
End Property

Public Property Let GMN1Cod(ByVal vData As String)

    m_sGMN1Cod = vData
    
End Property
Public Property Get GMN1Cod() As String
    GMN1Cod = m_sGMN1Cod
    
End Property
Public Property Let Proce(ByVal vData As Long)

    m_iProceCod = vData
    
End Property
Public Property Get Proce() As Long
    Proce = m_iProceCod
    
End Property

Public Property Let Prove(ByVal vData As String)

    m_sProveCod = vData
    
End Property
Public Property Get Prove() As String
    Prove = m_sProveCod
    
End Property

Public Property Let Usuario(ByVal vData As String)

    m_sUsuario = vData
    
End Property
Public Property Get Usuario() As String
    Usuario = m_sUsuario
    
End Property

Public Property Let FecAct(ByVal vData As Variant)

    m_vFecAct = vData
    
End Property
Public Property Get FecAct() As Variant
    FecAct = m_vFecAct
    
End Property

Private Sub Class_Initialize()
    m_lDataSize = 0
End Sub
Private Sub Class_Terminate()
    
    Set m_oConexion = Nothing
    
End Sub

Private Function IBaseDatos_AnyadirABaseDatos() As TipoErrorSummit
End Function
Private Function IBaseDatos_CambiarCodigo(CodigoNuevo As Variant) As TipoErrorSummit
End Function
Private Sub IBaseDatos_CancelarEdicion()
End Sub
Private Function IBaseDatos_ComprobarExistenciaEnBaseDatos() As Boolean
End Function
Private Function IBaseDatos_EliminarDeBaseDatos() As TipoErrorSummit
    
End Function


Private Function IBaseDatos_FinalizarEdicionEliminando() As TipoErrorSummit

    ''' ! No usado por ahora
    
End Function
Private Function IBaseDatos_FinalizarEdicionModificando() As TipoErrorSummit
End Function

''' <summary>Inicia la edici�n de un registro</summary>
''' <param name="Bloquear">No se usa</param>
''' <param name="UsuarioBloqueo">No se usa</param>
''' <returns>Variable tipo TipoErrorSummit con el error si lo hay</returns>
''' <remarks>Llamada desde: FSGClient; Tiempo m�ximo: 1 sec </remarks>
''' <revision>LTG 04/01/2012</revision>

Private Function IBaseDatos_IniciarEdicion(Optional ByVal Bloquear As Boolean, Optional ByVal UsuarioBloqueo As String) As TipoErrorSummit
End Function

'Descripcion:Devuelve la ruta donde se almacenara temporalmete, el fichero de contrato
'Llamada desde:frmCONTRWIZAD.frm
'Tiempo ejecucion:0seg.
Public Function DevolverRutaContrato() As String

Dim rs As adodb.Recordset

Dim sConsulta As String
Dim sResultado As String

'******************* Precondicion *******************
If m_oConexion Is Nothing Then
    Err.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CContrato.LoadContrato", "No se ha establecido la conexion"
End If
'*****************************************************
    sResultado = ""
    
    sConsulta = "SELECT RUTA_CONTRATO FROM PARGEN_PM WITH (NOLOCK)"
    
    Set rs = New adodb.Recordset
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenKeyset, adLockReadOnly
    
    If Not rs.eof Then
        sResultado = NullToStr(rs.Fields(0).Value)
    End If
    
    DevolverRutaContrato = sResultado
End Function

'Descripcion:comprueba si el usuario se encuentra en el flujo de la solicitud
'Parametro entrada:
    'sCod: Codigo Solicitud
'Devuelve: True si el usuario se encuentra en el flujo de la solicitud
'Llamada desde:frmCONTRWIZAD.frm
'Tiempo ejecucion:0,2seg.
Public Function ComprobarPermisoSolicitud(ByVal sCod As String) As Boolean
Dim rs As adodb.Recordset

Dim sConsulta As String
Dim bResultado As Boolean

'******************* Precondicion *******************
If m_oConexion Is Nothing Then
    Err.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CContrato.ComprobarPermisoSolicitud", "No se ha establecido la conexion"
End If
'*****************************************************
    
    sConsulta = "SELECT TOP 1 R.PER FROM SOLICITUD S WITH (NOLOCK) "
    sConsulta = sConsulta & " INNER JOIN WORKFLOW W WITH (NOLOCK) ON W.ID = S.WORKFLOW"
    sConsulta = sConsulta & " INNER JOIN PM_ROL R WITH (NOLOCK) ON R.WORKFLOW = W.ID"
    sConsulta = sConsulta & " WHERE COD = " & StrToSQLNULL(sCod) & " AND R.PER IN (SELECT ADM_GS FROM PARGEN_PM WITH (NOLOCK))"

    
    Set rs = New adodb.Recordset
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenKeyset, adLockReadOnly
    
    bResultado = False
    If Not rs.eof Then
        bResultado = True
    End If
    
    ComprobarPermisoSolicitud = bResultado
End Function

