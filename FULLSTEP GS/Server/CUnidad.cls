VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CUnidad"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
''' *** Clase: CUnidad
''' *** Creacion: 9/09/1998 (Javier Arana)
''' *** Ultima revision: 18/01/1999 (Alfredo Magallon)

Option Explicit

Implements IBaseDatos

''' Variables privadas con la informacion de una Unidad

Private m_sCod As String
Private m_oDen As CMultiidiomas
Private m_vEstIntegracion As Variant 'Estado integraci�n
Private m_vFecAct As Variant

''' Variables para el control de cambios del Log e Integraci�n
Private m_sUsuario As String             'Usuario que realiza los cambios.
Private m_udtOrigen As OrigenIntegracion 'Origen del mvto en la tabla de LOG
Private m_vNumDecimales As Variant


''' Conexion

Private m_oConexion As CConexion

''' Control de errores

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

''' Recordset para la Unidad
Private m_adores As adodb.Recordset

''' Indice de la Unidad en la coleccion

Private m_lIndice As Long
Private Function GrabarEnLog() As Boolean
'***********************************************************************************
'*** Descripci�n: Nos indica si hay que grabar o no el cambio en el log de       ***
'***              UNIDADES (tabla LOG_UNI) y carga en la variable                ***
'***              del m�dulo m_udtOrigen el origen del movimiento                ***
'*** Parametros:  Ninguno                                                        ***
'*** Devuelve:    True (hay que grabar) False (No hay que grabar)                ***
'***********************************************************************************

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If gParametrosGenerales.gbACTIVLOG Or gParametrosIntegracion.gaExportar(EntidadIntegracion.Uni) Then
        If gParametrosGenerales.gbACTIVLOG And gParametrosIntegracion.gaExportar(EntidadIntegracion.Uni) Then
            m_udtOrigen = FSGSIntReg
        Else
            If gParametrosGenerales.gbACTIVLOG Then
                m_udtOrigen = FSGSReg
            Else
                m_udtOrigen = FSGSInt
            End If
        End If
        GrabarEnLog = True
    Else
        GrabarEnLog = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CUnidad", "GrabarEnLog", ERR, Erl)
        Exit Function
    End If

End Function

Public Property Let FecAct(ByVal vData As Variant)

    ''' * Objetivo: Dar valor a la variable privada FECACT
    ''' * Recibe: Fecha de ultima actualizacion de la UNIDAD
    ''' * Devuelve: Nada

    m_vFecAct = vData
    
End Property
Public Property Get FecAct() As Variant

    ''' * Objetivo: Devolver la variable privada FECACT
    ''' * Recibe: Nada
    ''' * Devuelve: Fecha de ultima actualizacion de la UNIDAD

    FecAct = m_vFecAct
    
End Property
Public Property Let EstadoIntegracion(ByVal vData As Variant)

    ''' * Objetivo: Dar valor al estado de integraci�n de la  unidad
    ''' * Recibe: el valor
    ''' * Devuelve: Nada

    m_vEstIntegracion = vData
    
End Property
Public Property Get EstadoIntegracion() As Variant

    ''' * Objetivo: Devolver el estado de integraci�n de la unidad
    ''' * Recibe: Nada
    ''' * Devuelve: El estado de integraci�n de la  unidad

    EstadoIntegracion = m_vEstIntegracion
    
End Property
Public Property Let Usuario(ByVal vData As String)

    ''' * Objetivo: Dar valor a la variable privada Usuario
    ''' * Recibe: Codigo del usuario conectado
    ''' * Devuelve: Nada

    m_sUsuario = vData
    
End Property
Public Property Get Indice() As Integer

    ''' * Objetivo: Devolver el indice de la Unidad en la coleccion
    ''' * Recibe: Nada
    ''' * Devuelve: Indice de la Unidad en la coleccion

    Indice = m_lIndice
    
End Property
Public Property Let Indice(ByVal iInd As Integer)

    ''' * Objetivo: Dar valor al indice de la Unidad en la coleccion
    ''' * Recibe: Indice de la Unidad en la coleccion
    ''' * Devuelve: Nada

    m_lIndice = iInd
    
End Property
Friend Property Set Conexion(ByVal vData As CConexion)

    ''' * Objetivo: Dar valor a la conexion de la Unidad
    ''' * Recibe: Conexion
    ''' * Devuelve: Nada

    Set m_oConexion = vData
    
End Property
Friend Property Get Conexion() As CConexion

    ''' * Objetivo: Devolver la conexion de la Unidad
    ''' * Recibe: Nada
    ''' * Devuelve: Conexion

    Set Conexion = m_oConexion
    
End Property
Public Property Get Denominaciones() As CMultiidiomas

    ''' * Objetivo: Devolver la denominacion de la Unidad
    ''' * Recibe: Nada
    ''' * Devuelve: Conexion
    Set Denominaciones = m_oDen
    
End Property

Public Property Set Denominaciones(ByVal dato As CMultiidiomas)

    ''' * Objetivo: Dar valor al objeto Denominaciones
    ''' * Recibe: Codigo de la Unidad
    ''' * Devuelve: Nada
    Set m_oDen = dato
    
End Property
Public Property Let Cod(ByVal vData As String)

    ''' * Objetivo: Dar valor a la variable privada COD
    ''' * Recibe: Codigo de la Unidad
    ''' * Devuelve: Nada

    m_sCod = vData
    
End Property
Public Property Get Cod() As String

    ''' * Objetivo: Devolver la variable privada COD
    ''' * Recibe: Nada
    ''' * Devuelve: Codigo de la Unidad

    Cod = m_sCod
    
End Property

Public Property Let NumDecimales(ByVal vData As Variant)

 

    ''' * Objetivo: Dar valor a la variable privada NumDecimales

    ''' * Recibe: Numero de decimales

    ''' * Devuelve: Nada

 

    m_vNumDecimales = vData

    

End Property

Public Property Get NumDecimales() As Variant

 

    ''' * Objetivo: Devolver la variable privada NumDecimales

    ''' * Recibe: Nada

    ''' * Devuelve: Numero de decimales


    NumDecimales = m_vNumDecimales
End Property



Private Sub Class_Terminate()

    ''' * Objetivo: Limpiar la memoria
    
    Set m_adores = Nothing
    Set m_oConexion = Nothing
        
End Sub

''' <summary>Esta funci�n a�ade la Unidad de la tabla </summary>
''' <returns>TipoErrorSummit con el error si lo hay</returns>
''' <remarks>Llamada desde: FSGClient
''' Tiempo m�ximo: 1 sec </remarks>

Private Function IBaseDatos_AnyadirABaseDatos() As TipoErrorSummit
    
    Dim sConsulta As String
    Dim TESError As TipoErrorSummit
    Dim bTransaccionEnCurso As Boolean
    Dim AdoRes As adodb.Recordset
    Dim oMulti As CMultiidioma
    Dim sCod As String
    Dim lIdLogUNI As Long
 '******************************************
'***** M U Y   I M P O R T A N T E     ****
'*****                                 ****
'***** SI SE REALIZAN CAMBIOS          ****
'***** DESPUES DEL 21/06/2002          ****
'***** MIRAR SI HAY QUE PASARLOS A     ****
'***** FSGSINTEGRATION.DLL             ****
'******************************************
   
    ''' Precondicion
    
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CUnidad.AnyadirABAseDatos", "No se ha establecido la conexion"
        Exit Function
    End If
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    TESError.NumError = TESnoerror
    
    
If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:

    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
    bTransaccionEnCurso = True
    sConsulta = "INSERT INTO UNI (COD) VALUES (N'" & DblQuote(m_sCod) & "')"
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    
    'Inserta las denominaciones en UNI_DEN
    If Not m_oDen Is Nothing Then
        For Each oMulti In m_oDen
            If Not IsEmpty(oMulti.Den) Then
                sConsulta = "INSERT INTO UNI_DEN (UNI,DEN,IDIOMA) VALUES (N'" & DblQuote(m_sCod) & "',"
                sConsulta = sConsulta & StrToSQLNULLNVar(oMulti.Den) & ",'" & DblQuote(oMulti.Cod) & "')"
                m_oConexion.ADOCon.Execute sConsulta
                If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
            End If
        Next
    End If

     ''' Recuperar la fecha de insercion
    'CALIDAD Se obtiene la Fecha de Actualizaci�n con bloqueo para recuperar la verdadera �ltima fecha de actualizaci�n
    sConsulta = "SELECT FECACT FROM UNI WHERE COD =N'" & DblQuote(m_sCod) & "'"
    Set AdoRes = New adodb.Recordset
    AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    m_vFecAct = AdoRes("FECACT").Value
    AdoRes.Close
    
   
    ' Tratamiento para el LOG de cambios y la Integraci�n
    If GrabarEnLog Then
        ''10/09/2007(ngo): MultiERP
        sConsulta = "SELECT COD FROM ERP WITH(NOLOCK)"
        AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
        While Not AdoRes.eof
            sConsulta = "INSERT INTO LOG_UNI (ACCION, COD, ORIGEN, USU, ERP) VALUES ('" & Accion_Alta & "',N'" & DblQuote(m_sCod) & "'"
            sConsulta = sConsulta & "," & m_udtOrigen & ",'" & DblQuote(m_sUsuario) & "','" & DblQuote(AdoRes.Fields("COD").Value) & "')"
            m_oConexion.ADOCon.Execute sConsulta
            If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
            
            Dim ador As adodb.Recordset
            Set ador = New adodb.Recordset
            'CALIDAD Sin WITH(NOLOCK) en el FROM para asegurar la recuperaci�n del mayor valor de la tabla
            ador.Open "SELECT SCOPE_IDENTITY()", m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
            If Not ador.eof Then
                lIdLogUNI = ador.Collect(0)
            End If
            ador.Close
            Set ador = Nothing
            
            If Not m_oDen Is Nothing Then
                For Each oMulti In m_oDen
                    sConsulta = "INSERT INTO LOG_UNI_DEN (ID_LOG_UNI,DEN,IDI) VALUES (" & lIdLogUNI & ","
                    sConsulta = sConsulta & StrToSQLNULLNVar(oMulti.Den) & ",'" & DblQuote(oMulti.Cod) & "')"
                    m_oConexion.ADOCon.Execute sConsulta
                    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
                Next
            End If
            
            m_vEstIntegracion = 0 'EstadoIntegracion.PendienteDeTratar
            AdoRes.MoveNext
        Wend
        AdoRes.Close
    End If
    Set AdoRes = Nothing
    
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    
    IBaseDatos_AnyadirABaseDatos = TESError
    
    Exit Function
    
Error_Cls:
     
    IBaseDatos_AnyadirABaseDatos = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    If Not AdoRes Is Nothing Then
        AdoRes.Close
        Set AdoRes = Nothing
    End If
    
     If bTransaccionEnCurso Then
        m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
        bTransaccionEnCurso = False
    End If

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        'Call g_oErrores.TratarError("Class Module", "CUnidad", "IBaseDatos_AnyadirABaseDatos", ERR, Erl)
        GoTo Error_Cls
        Exit Function
    End If
           
End Function

''' <summary>Esta funci�n cambia de codigo la Unidad </summary>
''' <param name="CodigoNuevo">Codigo nuevo</param>
''' <returns>TipoErrorSummit con el error si lo hay</returns>
''' <remarks>Llamada desde: FSGClient
''' Tiempo m�ximo: 1 sec </remarks>
''' <revision>JVS 09/01/2012</revision>
Private Function IBaseDatos_CambiarCodigo(CodigoNuevo As Variant) As TipoErrorSummit

    Dim TESError As TipoErrorSummit
    Dim adocom As adodb.Command
    Dim oParam As adodb.Parameter
    Dim ContRegActualizados As Long
    Dim bLog As Boolean
    Dim AdoRes As adodb.Recordset
    Dim sConsulta As String
    Dim oMulti As CMultiidioma
    Dim lIdLogUNI As Long
    
    ''' Precondicion
    
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CUnidad.EliminarDeBaseDatos", "No se ha establecido la conexion"
        Exit Function
    End If
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    TESError.NumError = TESnoerror
 '******************************************
'***** M U Y   I M P O R T A N T E     ****
'*****                                 ****
'***** SI SE REALIZAN CAMBIOS          ****
'***** DESPUES DEL 21/06/2002          ****
'***** MIRAR SI HAY QUE PASARLOS A     ****
'***** FSGSINTEGRATION.DLL             ****
'******************************************
   
        
If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
    
    ''' Preparar la SP y sus parametros
    Set adocom = New adodb.Command
    Set adocom.ActiveConnection = m_oConexion.ADOCon
    
    Set oParam = adocom.CreateParameter("OLD", adVarChar, adParamInput, 50, m_sCod)
    adocom.Parameters.Append oParam
    Set oParam = adocom.CreateParameter("NEW", adVarChar, adParamInput, 50, CodigoNuevo)
    adocom.Parameters.Append oParam
    
    adocom.CommandType = adCmdStoredProc
    adocom.CommandText = "UNI_COD"
        
    ''' Ejecutar la SP
    
    adocom.Execute ContRegActualizados
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    
    Set adocom = Nothing
    
    Set AdoRes = New adodb.Recordset
       
    ''' Tratamiento para el LOG de cambios y la Integraci�n. Solo grabamos si se han actualizado de verdad alg�n registro
    bLog = True
    If GrabarEnLog And ContRegActualizados <> -1 Then
        
        ''10/09/2007(ngo): MultiERP
        sConsulta = "SELECT COD FROM ERP WITH(NOLOCK)"
        AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
        
        While Not AdoRes.eof
            sConsulta = "INSERT INTO LOG_UNI (ACCION, COD, COD_NEW, ORIGEN, USU, ERP) VALUES ('" & Accion_CambioCodigo & "', N'" & DblQuote(m_sCod) & "'"
            sConsulta = sConsulta & ", N'" & DblQuote(CodigoNuevo) & "'," & m_udtOrigen & ",'" & DblQuote(m_sUsuario) & "','" & DblQuote(AdoRes.Fields("COD").Value) & "')"
            m_oConexion.ADOCon.Execute sConsulta
            If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
            
            Dim ador As adodb.Recordset
            Set ador = New adodb.Recordset
            'CALIDAD Sin WITH(NOLOCK) en el FROM para asegurar la recuperaci�n del mayor valor de la tabla
            ador.Open "SELECT SCOPE_IDENTITY()", m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
            If Not ador.eof Then
                lIdLogUNI = ador.Collect(0)
            End If
            ador.Close
            Set ador = Nothing
            
            If Not m_oDen Is Nothing Then
                For Each oMulti In m_oDen
                    sConsulta = "INSERT INTO LOG_UNI_DEN (ID_LOG_UNI,DEN,IDI) VALUES (" & lIdLogUNI & ","
                    sConsulta = sConsulta & StrToSQLNULLNVar(oMulti.Den) & ",'" & DblQuote(oMulti.Cod) & "')"
                    m_oConexion.ADOCon.Execute sConsulta
                    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
                Next
            End If
            
            
            m_vEstIntegracion = 0 'EstadoIntegracion.PendienteDeTratar
            AdoRes.MoveNext
        Wend
        AdoRes.Close
    End If
    bLog = False
    
    m_sCod = CodigoNuevo
    
    'Actualizamos FecAct para no bloquearnos a nosotros mismos
    'CALIDAD Se obtiene la Fecha de Actualizaci�n con bloqueo para recuperar la verdadera �ltima fecha de actualizaci�n
    sConsulta = "SELECT FECACT FROM UNI WHERE COD='" & DblQuote(m_sCod) & "'"
    AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
    If AdoRes.eof Then
        AdoRes.Close
        Set AdoRes = Nothing
        TESError.NumError = TESDatoEliminado
        TESError.Arg1 = 24  ''"Unidad"
        IBaseDatos_CambiarCodigo = TESError
        m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
        Exit Function
    End If
    m_vFecAct = AdoRes.Collect(0)
    AdoRes.Close
    Set AdoRes = Nothing
    
    IBaseDatos_CambiarCodigo = TESError
    
    Exit Function
    
Error_Cls:
    If bLog Then 'Si el error se ha producido al grabar en la tabla de LOG, deshacemos los cambios a mano
        Set adocom = Nothing
        Set adocom = New adodb.Command
        Set adocom.ActiveConnection = m_oConexion.ADOCon
        Set oParam = adocom.CreateParameter("PAR1", adVarChar, adParamInput, 50, CodigoNuevo)
        adocom.Parameters.Append oParam
        Set oParam = adocom.CreateParameter("PAR2", adVarChar, adParamInput, 50, m_sCod)
        adocom.Parameters.Append oParam
        adocom.CommandType = adCmdStoredProc
        adocom.CommandText = "UNI_COD"
        adocom.Execute
        Set AdoRes = New adodb.Recordset
        'CALIDAD Se obtiene la Fecha de Actualizaci�n con bloqueo para recuperar la verdadera �ltima fecha de actualizaci�n
        sConsulta = "SELECT FECACT FROM UNI WHERE COD='" & DblQuote(m_sCod) & "'"
        AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
        m_vFecAct = AdoRes("FECACT").Value
        AdoRes.Close
        Set AdoRes = Nothing
    End If
     
    If Not adocom Is Nothing Then
        Set adocom = Nothing
    End If
    
    IBaseDatos_CambiarCodigo = basErrores.TratarError(m_oConexion.ADOCon.Errors)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        'Call g_oErrores.TratarError("Class Module", "CUnidad", "IBaseDatos_CambiarCodigo", ERR, Erl)
        GoTo Error_Cls
        Exit Function
    End If
    
End Function
Private Sub IBaseDatos_CancelarEdicion()

    ''' * Objetivo: Cancelar la edicion, cerrando el Resultset
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not m_adores Is Nothing Then
        m_adores.Close
        Set m_adores = Nothing
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CUnidad", "IBaseDatos_CancelarEdicion", ERR, Erl)
        Exit Sub
    End If

End Sub
Private Function IBaseDatos_ComprobarExistenciaEnBaseDatos() As Boolean

    ''' ! No usado por ahora
    
End Function

''' <summary>Esta funci�n Eliminar la Unidad de la tabla </summary>
''' <returns>TipoErrorSummit con el error si lo hay</returns>
''' <remarks>Llamada desde: FSGClient
''' Tiempo m�ximo: 1 sec </remarks>
''' <revision>JVS 09/01/2012</revision>
Private Function IBaseDatos_EliminarDeBaseDatos() As TipoErrorSummit

    Dim TESError As TipoErrorSummit
    Dim bTransaccionEnCurso As Boolean
    Dim ContRegBorrados As Long
    Dim sConsulta As String
    Dim oMulti As CMultiidioma
    Dim lIdLogUNI As Long
    
    ''' Precondicion
    
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CUnidad.EliminarDeBaseDatos", "No se ha establecido la conexion"
        Exit Function
    End If
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    TESError.NumError = TESnoerror
    '******************************************
'***** M U Y   I M P O R T A N T E     ****
'*****                                 ****
'***** SI SE REALIZAN CAMBIOS          ****
'***** DESPUES DEL 21/06/2002          ****
'***** MIRAR SI HAY QUE PASARLOS A     ****
'***** FSGSINTEGRATION.DLL             ****
'******************************************

    
If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
       
    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
    bTransaccionEnCurso = True
  
    ''' Borrado
    m_oConexion.ADOCon.Execute "DELETE FROM PROVE_ART_UP WHERE UP='" & DblQuote(m_sCod) & "'"
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    m_oConexion.ADOCon.Execute "DELETE FROM PROVE_ART_UP WHERE UC='" & DblQuote(m_sCod) & "'"
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    sConsulta = "UPDATE COPIA_CAMPO SET VALOR_TEXT=NULL WHERE VALOR_TEXT='" & DblQuote(m_sCod) & "' AND COPIA_CAMPO_DEF IN (SELECT ID FROM FORM_CAMPO WITH(NOLOCK) WHERE TIPO_CAMPO_GS=" & TipoCampoGS.Unidad & ")"
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    sConsulta = "UPDATE COPIA_LINEA_DESGLOSE SET VALOR_TEXT=NULL WHERE VALOR_TEXT='" & DblQuote(m_sCod) & "' AND CAMPO_HIJO IN (SELECT COPIA_CAMPO.ID FROM COPIA_CAMPO WITH(NOLOCK) INNER JOIN FORM_CAMPO WITH(NOLOCK) ON COPIA_CAMPO.COPIA_CAMPO_DEF=FORM_CAMPO.ID WHERE ES_SUBCAMPO=1 AND TIPO_CAMPO_GS=" & TipoCampoGS.Unidad & ")"
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    sConsulta = "UPDATE FORM_CAMPO SET VALOR_TEXT=NULL WHERE VALOR_TEXT='" & DblQuote(m_sCod) & "' AND TIPO_CAMPO_GS=" & TipoCampoGS.Unidad
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    sConsulta = "UPDATE LINEA_DESGLOSE SET VALOR_TEXT=NULL WHERE VALOR_TEXT='" & DblQuote(m_sCod) & "' AND CAMPO_HIJO IN (SELECT ID FROM FORM_CAMPO WITH(NOLOCK) WHERE ES_SUBCAMPO=1 AND TIPO_CAMPO_GS=" & TipoCampoGS.Unidad & ")"
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    'Elimina de las solicitudes favoritas los campos con la Unidad
    sConsulta = "UPDATE PM_CAMPO_FAV  SET VALOR_TEXT = NULL FROM PM_CAMPO_FAV CF WITH(NOLOCK)"
    sConsulta = sConsulta & " INNER JOIN FORM_CAMPO FC WITH (NOLOCK) ON FC.ID = CF.CAMPO"
    sConsulta = sConsulta & " WHERE CF.VALOR_TEXT='" & DblQuote(m_sCod) & "' AND FC.TIPO_CAMPO_GS=" & TipoCampoGS.Unidad
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    'Elimina de las solicitudes favoritas las lineas de desglose con la Unidad
    sConsulta = "UPDATE PM_LINEA_DESGLOSE_FAV SET VALOR_TEXT = NULL FROM PM_LINEA_DESGLOSE_FAV LDF WITH(NOLOCK)"
    sConsulta = sConsulta & " INNER JOIN FORM_CAMPO FC WITH (NOLOCK) ON FC.ID = LDF.CAMPO_HIJO"
    sConsulta = sConsulta & " WHERE LDF.VALOR_TEXT='" & DblQuote(m_sCod) & "' AND FC.TIPO_CAMPO_GS=" & TipoCampoGS.Unidad
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    ''MultiERP: Elimina de UNI_ERP
    m_oConexion.ADOCon.Execute "DELETE FROM UNI_ERP WHERE COD_GS=N'" & DblQuote(m_sCod) & "'"
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    m_oConexion.ADOCon.Execute "DELETE FROM UNI_DEN WHERE UNI=N'" & DblQuote(m_sCod) & "'"
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    m_oConexion.ADOCon.Execute "DELETE FROM UNI WHERE COD=N'" & DblQuote(m_sCod) & "'", ContRegBorrados
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls

    ''' Tratamiento para el LOG de cambios y la Integraci�n
    If GrabarEnLog And ContRegBorrados > 0 Then
        
        ''10/09/2007(ngo): MultiERP
        sConsulta = "SELECT COD FROM ERP WITH(NOLOCK)"
        Dim AdoRes As adodb.Recordset
        Set AdoRes = New adodb.Recordset
        AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
        While Not AdoRes.eof
            sConsulta = "INSERT INTO LOG_UNI (ACCION, COD, ORIGEN, USU, ERP) VALUES ('" & Accion_Baja & "','" & DblQuote(m_sCod) & "'"

            sConsulta = sConsulta & "," & m_udtOrigen & ",'" & DblQuote(m_sUsuario) & "',N'" & DblQuote(AdoRes.Fields("COD").Value) & "')"
            m_oConexion.ADOCon.Execute sConsulta
            If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
            
            Dim ador As adodb.Recordset
            Set ador = New adodb.Recordset
            'CALIDAD Sin WITH(NOLOCK) en el FROM para asegurar la recuperaci�n del mayor valor de la tabla
            ador.Open "SELECT SCOPE_IDENTITY()", m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
            If Not ador.eof Then
                lIdLogUNI = ador.Collect(0)
            End If
            ador.Close
            Set ador = Nothing
            
            If Not m_oDen Is Nothing Then
                For Each oMulti In m_oDen
                    sConsulta = "INSERT INTO LOG_UNI_DEN (ID_LOG_UNI,DEN,IDI) VALUES (" & lIdLogUNI & ","
                    sConsulta = sConsulta & StrToSQLNULLNVar(oMulti.Den) & ",'" & DblQuote(oMulti.Cod) & "')"
                    m_oConexion.ADOCon.Execute sConsulta
                    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
                Next
            End If
            
            AdoRes.MoveNext
        Wend
        AdoRes.Close
        Set AdoRes = Nothing
    End If
        
     ''' Terminar transaccion
    
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    IBaseDatos_EliminarDeBaseDatos = TESError
    
    Exit Function
        
Error_Cls:
        
    IBaseDatos_EliminarDeBaseDatos = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    
    If bTransaccionEnCurso Then
        m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
        bTransaccionEnCurso = False
    End If

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        'Call g_oErrores.TratarError("Class Module", "CUnidad", "IBaseDatos_EliminarDeBaseDatos", ERR, Erl)
        GoTo Error_Cls
        Exit Function
    End If
    
End Function

Private Function IBaseDatos_FinalizarEdicionEliminando() As TipoErrorSummit

    ''' ! No usado por ahora
    
End Function

''' <summary>Esta funci�n Finaliza la edicion del Resultset modificando </summary>
''' <returns>TipoErrorSummit con el error si lo hay</returns>
''' <remarks>Llamada desde: FSGClient
''' Tiempo m�ximo: 1 sec </remarks>
''' <revision>JVS 09/01/2012</revision>
Private Function IBaseDatos_FinalizarEdicionModificando() As TipoErrorSummit

    Dim TESError As TipoErrorSummit
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim oMulti As CMultiidioma
    Dim sCod As String
    Dim lIdLogUNI As Long
'******************************************
'***** M U Y   I M P O R T A N T E     ****
'*****                                 ****
'***** SI SE REALIZAN CAMBIOS          ****
'***** DESPUES DEL 21/06/2002          ****
'***** MIRAR SI HAY QUE PASARLOS A     ****
'***** FSGSINTEGRATION.DLL             ****
'******************************************
    ''' Precondicion
    
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CUnidad.FinalizarEdicionModificando", "No se ha establecido la conexion"
        Exit Function
    End If

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    TESError.NumError = TESnoerror


If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
    
    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
    bTransaccionEnCurso = True

    'Comprobamos si ha habido cambios en otra sesi�n
    Set m_adores = New adodb.Recordset
    'CALIDAD Se obtiene la Fecha de Actualizaci�n con bloqueo para recuperar la verdadera �ltima fecha de actualizaci�n
    sConsulta = "SELECT FECACT FROM UNI WITH(NOLOCK) WHERE COD=N'" & DblQuote(m_sCod) & "'"
    m_adores.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
    
    If m_adores.eof Then
        m_adores.Close
        Set m_adores = Nothing
        TESError.NumError = TESDatoEliminado
        TESError.Arg1 = 24  ''"Unidad"
        IBaseDatos_FinalizarEdicionModificando = TESError
        m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
        Exit Function
    End If
    
    If m_vFecAct <> m_adores("FECACT") Then
        m_adores.Close
        Set m_adores = Nothing
        TESError.NumError = TESInfActualModificada
        m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
        IBaseDatos_FinalizarEdicionModificando = TESError
        Exit Function
    End If

   
    For Each oMulti In m_oDen
        sConsulta = "UPDATE UNI_DEN SET DEN= " & StrToSQLNULLNVar(oMulti.Den) & _
            " WHERE UNI=N'" & DblQuote(m_sCod) & "'" & _
            " AND IDIOMA = '" & DblQuote(oMulti.Cod) & "'"
        
        m_oConexion.ADOCon.Execute sConsulta
        If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    Next
    
    'Recogemos el nuevo valor de FECACT
    m_adores.Requery
    m_vFecAct = m_adores("FECACT").Value
    m_adores.Close

    ''' Tratamiento para el LOG de cambios y la Integraci�n
    If GrabarEnLog Then
        If Not m_oDen Is Nothing Then
            For Each oMulti In m_oDen
                sCod = sCod & "," & "DEN_" & oMulti.Cod
            Next
        End If
        ''10/09/2007(ngo): MultiERP
        sConsulta = "SELECT COD FROM ERP WITH(NOLOCK)"
        m_adores.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
        While Not m_adores.eof
            sConsulta = "INSERT INTO LOG_UNI (ACCION, COD, ORIGEN, USU, ERP) VALUES ('" & Accion_Modificacion & "',N'" & DblQuote(m_sCod) & "'"
            sConsulta = sConsulta & " ," & m_udtOrigen & ",'" & DblQuote(m_sUsuario) & "','" & DblQuote(m_adores.Fields("COD").Value) & "')"
            m_oConexion.ADOCon.Execute sConsulta
            If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
            
            Dim ador As adodb.Recordset
            Set ador = New adodb.Recordset
            'CALIDAD Sin WITH(NOLOCK) en el FROM para asegurar la recuperaci�n del mayor valor de la tabla
            ador.Open "SELECT SCOPE_IDENTITY()", m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
            If Not ador.eof Then
                lIdLogUNI = ador.Collect(0)
            End If
            ador.Close
            Set ador = Nothing
            
            If Not m_oDen Is Nothing Then
                For Each oMulti In m_oDen
                    sConsulta = "INSERT INTO LOG_UNI_DEN (ID_LOG_UNI,DEN,IDI) VALUES (" & lIdLogUNI & ","
                    sConsulta = sConsulta & StrToSQLNULLNVar(oMulti.Den) & ",'" & DblQuote(oMulti.Cod) & "')"
                    m_oConexion.ADOCon.Execute sConsulta
                    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
                Next
            End If
            
            m_vEstIntegracion = 0 'EstadoIntegracion.PendienteDeTratar
            m_adores.MoveNext
        Wend
        m_adores.Close
    End If
    
    Set m_adores = Nothing
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
     
    IBaseDatos_FinalizarEdicionModificando = TESError
    
    Exit Function
    
Error_Cls:
        
    IBaseDatos_FinalizarEdicionModificando = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    
    If Not m_adores Is Nothing Then
        m_adores.Close
        Set m_adores = Nothing
    End If
    
    If bTransaccionEnCurso Then
        m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
        bTransaccionEnCurso = False
    End If

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        'Call g_oErrores.TratarError("Class Module", "CUnidad", "IBaseDatos_FinalizarEdicionModificando", ERR, Erl)
        GoTo Error_Cls
        Exit Function
    End If

End Function

''' <summary>Esta funci�n inicia la edici�n del resulset</summary>
'''<param optional name="Bloquear">Si se bloquea o no</param>
'''<param optional name="UsuarioBloqueo">Usuario del bloqueo</param>
''' <returns>TipoErrorSummit con el error si lo hay</returns>
''' <remarks>Llamada desde: FSGClient
''' Tiempo m�ximo: <1 seg </remarks>
''' <revision>JVS 09/01/2012</revision>
Private Function IBaseDatos_IniciarEdicion(Optional ByVal Bloquear As Boolean, Optional ByVal UsuarioBloqueo As String) As TipoErrorSummit

    ''' * Objetivo: Abrir la edicion en la Unidad actual
    ''' * Objetivo: creando un Resultset para ello
    ''' * Recibe: Nada
    ''' * Devuelve: TipoErrorSummit con el error si lo hay

    Dim TESError As TipoErrorSummit
    Dim i As Integer
    ''' Precondicion
    
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CUnidad.IniciarEdicion", "No se ha establecido la conexion"
        Exit Function
    End If
    
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    TESError.NumError = TESnoerror
    
    ''' Abrir Resultset
    
    Set m_adores = New adodb.Recordset
    m_adores.Open ("SELECT COD,FECACT,NUMDEC FROM Uni WITH(NOLOCK) WHERE COD='" & DblQuote(m_sCod) & "'"), m_oConexion.ADOCon, adOpenStatic, adLockReadOnly

    ''' Si no esta la Unidad, alguien la ha eliminado
    
    If m_adores.eof Then
        m_adores.Close
        Set m_adores = Nothing
        TESError.NumError = TESDatoEliminado
        TESError.Arg1 = 24 ''"Unidad"
        IBaseDatos_IniciarEdicion = TESError
        Exit Function
    End If
    
    ''' Si la fecha de actualizaci�n ha variado es que la han modificado en otra sesi�n
    If Not IsMissing(m_vFecAct) Then
        If m_vFecAct <> m_adores("FECACT").Value Then
            m_adores.Close
            Set m_adores = Nothing
            TESError.NumError = TESInfActualModificada
            IBaseDatos_IniciarEdicion = TESError
            Exit Function
        End If
    End If
    
    m_sCod = m_adores("COD").Value
    Set m_oDen = New CMultiidiomas
    For i = 0 To m_adores.Fields.Count - 1
        If Left(m_adores.Fields(i).Name, 4) = "DEN_" Then
            m_oDen.Add Right(m_adores.Fields(i).Name, Len(m_adores.Fields(i).Name) - 4), m_adores.Fields(i).Value
        End If
    Next i
        
    IBaseDatos_IniciarEdicion = TESError
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CUnidad", "IBaseDatos_IniciarEdicion", ERR, Erl)
        Exit Function
    End If

End Function
Private Function IBaseDatos_ModificarEnBaseDatos() As Boolean

    ''' ! No usado por ahora
    
End Function


