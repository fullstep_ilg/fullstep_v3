VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CPlantConfVistasGrupo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CPlantConfVistasGrupo **********************************
'*             Autor : Mertxe Martin
'*             Creada : 01/12/2003
'****************************************************************

Option Explicit

Private mCol As Collection
Private m_oConexion As CConexion
Private m_oConexionServer As CConexionDeUseServer
Private m_bEOF As Boolean

Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum

''' <summary>A�ade una nueva vista a la colecci�n</summary>
''' <param name="oPlantilla">Plantilla</param>
''' <param name="oGrupo">Grupo</param>
''' <param name="iVista">ID de la vista</param>
''' <param name="ConfigVistasGr">tipo de config. de la vista</param>
''' <param name="vIndice">Indice</param>
''' <remarks>Llamada desde:frmADJ/frmRESREU; Tiempo m�ximo<1 seg</remarks>
''' <returns>Objeto de tipo CPlantConfVistaGrupo con los datos de la vista</returns>
Public Function Add(ByVal oPlantilla As CPlantilla, ByVal oGrupo As CGrupo, ByVal iVista As Integer, ConfigVistasGr As TipoConfigVista, _
Optional ByVal vIndice As Variant) As CPlantConfVistaGrupo
    
    'create a new object
    Dim objnewmember As CPlantConfVistaGrupo
        
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New CPlantConfVistaGrupo
   
    Set objnewmember.Plantilla = oPlantilla
    Set objnewmember.Grupo = oGrupo
    
    objnewmember.DescrWidth = ConfigVistasGr.dblDescrWidth
    
    objnewmember.AnyoPos = ConfigVistasGr.iAnyoPos
    objnewmember.AnyoVisible = ConfigVistasGr.bAnyoVisible
    objnewmember.AnyoWidth = ConfigVistasGr.dblAnyoWidth
    objnewmember.AnyoLevel = ConfigVistasGr.iAnyoLevel
    
    objnewmember.ImportePos = ConfigVistasGr.iImportePos
    objnewmember.ImporteVisible = ConfigVistasGr.bImporteVisible
    objnewmember.ImporteWidth = ConfigVistasGr.dblImporteWidth
    objnewmember.ImporteLevel = ConfigVistasGr.iImporteLevel
    
    objnewmember.AdjPos = ConfigVistasGr.iAdjPos
    objnewmember.AdjVisible = ConfigVistasGr.bAdjVisible
    objnewmember.AdjWidth = ConfigVistasGr.dblAdjWidth
    objnewmember.AdjLevel = ConfigVistasGr.iAdjLevel
    
    objnewmember.ProveedorPos = ConfigVistasGr.iProvPos
    objnewmember.ProveedorVisible = ConfigVistasGr.bProvVisible
    objnewmember.ProveedorWidth = ConfigVistasGr.dblProvWidth
    objnewmember.ProveedorLevel = ConfigVistasGr.iProvLevel
    
    objnewmember.AhorroPos = ConfigVistasGr.iAhorroImpPos
    objnewmember.AhorroVisible = ConfigVistasGr.bAhorroImpVisible
    objnewmember.AhorroWidth = ConfigVistasGr.dblAhorroImpWidth
    objnewmember.AhorroLevel = ConfigVistasGr.iAhorroImpLevel
    
    objnewmember.AhorroPorcenPos = ConfigVistasGr.iAhorroPorcenPos
    objnewmember.AhorroPorcenVisible = ConfigVistasGr.bAhorroPorcenVisible
    objnewmember.AhorroPorcenWidth = ConfigVistasGr.dblAhorroPorcenWidth
    objnewmember.AhorroPorcenLevel = ConfigVistasGr.iAhorroPorcenLevel
    
    objnewmember.PresUniPos = ConfigVistasGr.iPresUniPos
    objnewmember.PresUniVisible = ConfigVistasGr.bPresUniVisible
    objnewmember.PresUniWidth = ConfigVistasGr.dblPresUniWidth
    objnewmember.PresUniLevel = ConfigVistasGr.iPresUniLevel
    
    objnewmember.OBJLevel = ConfigVistasGr.iObjLevel
    objnewmember.OBJPos = ConfigVistasGr.iObjPos
    objnewmember.OBJVisible = ConfigVistasGr.bObjVisible
    objnewmember.OBJWidth = ConfigVistasGr.dblObjWidth
    
    objnewmember.CantidadLevel = ConfigVistasGr.iCantLevel
    objnewmember.CantidadPos = ConfigVistasGr.iCantPos
    objnewmember.CantidadVisible = ConfigVistasGr.bCantVisible
    objnewmember.CantidadWidth = ConfigVistasGr.dblCantWidth

    objnewmember.OcultarFila2 = ConfigVistasGr.bOcultarFila2
    
    objnewmember.Grupo0Width = ConfigVistasGr.dblGrupo0Width
    objnewmember.GruposWidth = ConfigVistasGr.dblGruposWidth
    
    objnewmember.ImpAdjVisible = ConfigVistasGr.bImpAdjVisible
    objnewmember.ImpAdjWidth = ConfigVistasGr.dblImpAdjWidth
    objnewmember.ImpAdjLevel = ConfigVistasGr.iImpAdjLevel
    objnewmember.ImpAdjPos = ConfigVistasGr.iImpAdjPos
    
    objnewmember.AnchoFila = ConfigVistasGr.dblAnchoFila
    
    objnewmember.PrecioProvLevel = ConfigVistasGr.iPrecioProvLevel
    objnewmember.PrecioProvPos = ConfigVistasGr.iPrecioProvPos
    objnewmember.PrecioProvVisible = ConfigVistasGr.bPrecioProvVisible
    objnewmember.PrecioProvWidth = ConfigVistasGr.dblPrecioProvWidth
    
    objnewmember.CantProvLevel = ConfigVistasGr.iCantProvLevel
    objnewmember.CantProvPos = ConfigVistasGr.iCantProvPos
    objnewmember.CantProvVisible = ConfigVistasGr.bCantProvVisible
    objnewmember.CantProvWidth = ConfigVistasGr.dblCantProvWidth
    
    objnewmember.AdjProvLevel = ConfigVistasGr.iAdjProvLevel
    objnewmember.AdjProvPos = ConfigVistasGr.iAdjProvPos
    objnewmember.AdjProvVisible = ConfigVistasGr.bAdjProvVisible
    objnewmember.AdjProvWidth = ConfigVistasGr.dblAdjProvWidth
    
    objnewmember.Vista = iVista
    
    objnewmember.OcultarItCerrados = ConfigVistasGr.bOcultarItCerrados
    objnewmember.ExcluirItCerradosResult = ConfigVistasGr.bExcluirItCerradosResult
    objnewmember.OcultarNoAdj = ConfigVistasGr.bOcultarNoAdj
    objnewmember.OcultarProvSinOfe = ConfigVistasGr.bOcultarProvSinOfe
    objnewmember.DecResult = ConfigVistasGr.iDecResult
    objnewmember.DecPorcen = ConfigVistasGr.iDecPorcen
    objnewmember.DecPrecios = ConfigVistasGr.iDecPrecios
    objnewmember.DecCant = ConfigVistasGr.iDecCant
    
    objnewmember.SolicVinculadaPos = ConfigVistasGr.iSolicVinculadaPos
    objnewmember.SolicVinculadaVisible = ConfigVistasGr.bSolicVinculadaVisible
    objnewmember.SolicVinculadaWidth = ConfigVistasGr.dblSolicVinculadaWidth
    objnewmember.SolicVinculadaLevel = ConfigVistasGr.iSolicVinculadaLevel
    
    objnewmember.FecIniSumPos = ConfigVistasGr.iFecIniSumPos
    objnewmember.FecIniSumVisible = ConfigVistasGr.bFecIniSumVisible
    objnewmember.FecIniSumWidth = ConfigVistasGr.dblFecIniSumWidth
    objnewmember.FecIniSumLevel = ConfigVistasGr.iFecIniSumLevel
    
    objnewmember.FecFinSumPos = ConfigVistasGr.iFecFinSumPos
    objnewmember.FecFinSumVisible = ConfigVistasGr.bFecFinSumVisible
    objnewmember.FecFinSumWidth = ConfigVistasGr.dblFecFinSumWidth
    objnewmember.FecFinSumLevel = ConfigVistasGr.iFecFinSumLevel
    
    objnewmember.EstrMatPos = ConfigVistasGr.iEstrMatPos
    objnewmember.EstrMatVisible = ConfigVistasGr.bEstrMatVisible
    objnewmember.EstrMatWidth = ConfigVistasGr.dblEstrMatWidth
    objnewmember.EstrMatLevel = ConfigVistasGr.iEstrMatLevel
    
    objnewmember.AnyoPartidaPos = ConfigVistasGr.iAnyoPartidaPos
    objnewmember.AnyoPartidaVisible = ConfigVistasGr.bAnyoPartidaVisible
    objnewmember.AnyoPartidaWidth = ConfigVistasGr.dblAnyoPartidaWidth
    objnewmember.AnyoPartidaLevel = ConfigVistasGr.iAnyoPartidaLevel
    
    Set objnewmember.Conexion = m_oConexion
    Set objnewmember.ConexionServer = m_oConexionServer
       
    If Not IsMissing(vIndice) And Not IsNull(vIndice) Then
        objnewmember.Indice = vIndice
        mCol.Add objnewmember, CStr(vIndice)
    Else
       mCol.Add objnewmember, CStr(iVista)
    End If
    
    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CPlantConfVistasGrupo", "Add", ERR, Erl)
      Exit Function
   End If
End Function

Friend Property Set Conexion(ByVal con As CConexion)
    Set m_oConexion = con
End Property

Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property

Friend Property Set ConexionServer(ByVal con As CConexionDeUseServer)
    Set m_oConexionServer = con
End Property

Friend Property Get ConexionServer() As CConexionDeUseServer
    Set ConexionServer = m_oConexionServer
End Property

Public Property Get Count() As Long
    If mCol Is Nothing Then
        Count = 0
    Else
         Count = mCol.Count
    End If
End Property

Public Property Get eof() As Boolean
    eof = m_bEOF
End Property

Friend Property Let eof(ByVal b As Boolean)
    m_bEOF = b
End Property

Public Property Get Item(vntIndexKey As Variant) As CPlantConfVistaGrupo
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
     Set NewEnum = mCol.[_NewEnum]
End Property

Public Sub Remove(vntIndexKey As Variant)

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    mCol.Remove vntIndexKey
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CPlantConfVistasGrupo", "Remove", ERR, Erl)
      Exit Sub
   End If

End Sub

Private Sub Class_Initialize()
    Set mCol = New Collection
End Sub

Private Sub Class_Terminate()
    'Proceroys collection when this class is terminated
    On Error Resume Next
    Set mCol = Nothing
    Set m_oConexion = Nothing
End Sub


''' <summary>
''' Carga las vistas del grupo
''' </summary>
'''<param name="oPlantilla">objeto que contiene la plantilla</param>
'''<param name="oGrupo">objeto que contiene el grupo</param>
'''<param optional name="iVista">Id de la vista (si se quiere una concreta)</param>
'''<param optional name="UsarIndice">Si insertamos el �ndice en la colecci�n</param>
'''<param optional name="Defecto">Boolean</param>
''' <remarks>Llamada desde: frmADJ
''' Tiempo m�ximo: 1 sec </remarks>
''' <revision>JVS 26/12/2011</revision>
Public Sub CargarTodasLasVistas(ByVal oPlantilla As CPlantilla, ByVal oGrupo As CGrupo, Optional ByVal iVista As Long, Optional ByVal UsarIndice As Boolean, Optional ByVal Defecto As Boolean)
    Dim AdoRes As New adodb.Recordset
    Dim sConsulta As String
    Dim lIndice As Long
    Dim adofldVista As adodb.Field
    Dim adofldDescrWidth As adodb.Field
    Dim adofldAnyoPos As adodb.Field
    Dim adofldAnyoVisible As adodb.Field
    Dim adofldAnyoWidth As adodb.Field
    Dim adofldAnyoLevel As adodb.Field
    Dim adofldImportePos As adodb.Field
    Dim adofldImporteVisible As adodb.Field
    Dim adofldImporteWidth As adodb.Field
    Dim adofldImporteLevel As adodb.Field
    Dim adofldAdjPos As adodb.Field
    Dim adofldAdjVisible As adodb.Field
    Dim adofldAdjWidth As adodb.Field
    Dim adofldAdjLevel As adodb.Field
    Dim adofldProvPos As adodb.Field
    Dim adofldProvVisible As adodb.Field
    Dim adofldProvWidth As adodb.Field
    Dim adofldProvLevel As adodb.Field
    Dim adofldPresUniPos As adodb.Field
    Dim adofldPresUniVisible As adodb.Field
    Dim adofldPresUniWidth As adodb.Field
    Dim adofldPresUniLevel As adodb.Field
    Dim adofldAhorroImpPos As adodb.Field
    Dim adofldAhorroImpVisible As adodb.Field
    Dim adofldAhorroImpWidth As adodb.Field
    Dim adofldAhorroImpLevel As adodb.Field
    Dim adofldAhorroPorcenPos As adodb.Field
    Dim adofldAhorroPorcenVisible As adodb.Field
    Dim adofldAhorroPorcenWidth As adodb.Field
    Dim adofldAhorroPorcenLevel As adodb.Field
    Dim adofldObjPos As adodb.Field
    Dim adofldObjVisible As adodb.Field
    Dim adofldObjWidth As adodb.Field
    Dim adofldObjLevel As adodb.Field
    Dim adofldCantPos As adodb.Field
    Dim adofldCantVisible As adodb.Field
    Dim adofldCantWidth As adodb.Field
    Dim adofldCantLevel As adodb.Field
    Dim adofldOcultarFila2 As adodb.Field
    Dim adofldGrupo0Width As adodb.Field
    Dim adofldGruposWidth As adodb.Field
    Dim adofldImpAdjWidth As adodb.Field
    Dim adofldImpAdjVisible As adodb.Field
    Dim adofldAnchoFila As adodb.Field
    Dim adofldPrecioProvPos As adodb.Field
    Dim adofldPrecioProvVisible As adodb.Field
    Dim adofldPrecioProvWidth As adodb.Field
    Dim adofldPrecioProvLevel As adodb.Field
    Dim adofldCantProvPos As adodb.Field
    Dim adofldCantProvVisible As adodb.Field
    Dim adofldCantProvWidth As adodb.Field
    Dim adofldCantProvLevel As adodb.Field
    Dim adofldAdjProvPos As adodb.Field
    Dim adofldAdjProvVisible As adodb.Field
    Dim adofldAdjProvWidth As adodb.Field
    Dim adofldAdjProvLevel As adodb.Field
    Dim TpConfVistas As TipoConfigVista
    Dim adofldImpAdjPos As adodb.Field
    Dim adofldImpAdjLevel As adodb.Field
    Dim adofldOcultarItCerrados As adodb.Field
    Dim adofldExcluirItCerradosResult As adodb.Field
    Dim adofldOcultarNoAdj As adodb.Field
    Dim adofldOcultarProvSinOfe As adodb.Field
    Dim adofldDecResult As adodb.Field
    Dim adofldDecPorcen As adodb.Field
    Dim adofldDecPrecios As adodb.Field
    Dim adofldDecCant As adodb.Field
    Dim adofldSolicVinculadaPos As adodb.Field
    Dim adofldSolicVinculadaVisible As adodb.Field
    Dim adofldSolicVinculadaWidth As adodb.Field
    Dim adofldSolicVinculadaLevel As adodb.Field
    Dim adofldFecIniSumPos As adodb.Field
    Dim adofldFecIniSumVisible As adodb.Field
    Dim adofldFecIniSumWidth As adodb.Field
    Dim adofldFecIniSumLevel As adodb.Field
    Dim adofldFecFinSumPos As adodb.Field
    Dim adofldFecFinSumVisible As adodb.Field
    Dim adofldFecFinSumWidth As adodb.Field
    Dim adofldFecFinSumLevel As adodb.Field
    Dim adofldEstrMatPos As adodb.Field
    Dim adofldEstrMatVisible As adodb.Field
    Dim adofldEstrMatWidth As adodb.Field
    Dim adofldEstrMatLevel As adodb.Field
    Dim adofldAnyoPartidaPos As adodb.Field
    Dim adofldAnyoPartidaVisible As adodb.Field
    Dim adofldAnyoPartidaWidth As adodb.Field
    Dim adofldAnyoPartidaLevel As adodb.Field
    
    '********* Precondicion **************************************
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CConfVistasGrupo.CargarTodasLasVistas", "No se ha establecido la conexion"
        Exit Sub
    End If
    '*************************************************************
    
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

    sConsulta = "SELECT PLANT,GRUPO,VISTA,DESCR_WIDTH,ANYO_POS,ANYO_VISIBLE,ANYO_WIDTH,ANYO_LEVEL,PROV_POS,PROV_VISIBLE,PROV_WIDTH,PROV_LEVEL,PRESUNI_POS,PRESUNI_VISIBLE"
    sConsulta = sConsulta & ",PRESUNI_WIDTH,PRESUNI_LEVEL,OBJ_POS,OBJ_VISIBLE,OBJ_WIDTH,OBJ_LEVEL,ADJ_POS,ADJ_VISIBLE,ADJ_WIDTH,ADJ_LEVEL,CANT_POS,CANT_VISIBLE,CANT_WIDTH,CANT_LEVEL"
    sConsulta = sConsulta & ",IMP_POS,IMP_VISIBLE,IMP_WIDTH,IMP_LEVEL,AHORRO_IMP_POS,AHORRO_IMP_VISIBLE,AHORRO_IMP_WIDTH,AHORRO_IMP_LEVEL,AHORRO_PORCEN_POS,AHORRO_PORCEN_VISIBLE"
    sConsulta = sConsulta & ",AHORRO_PORCEN_WIDTH,AHORRO_PORCEN_LEVEL,OCULTAR_FILA2,GRUPO0_WIDTH,GRUPOS_WIDTH,IMP_ADJ_VISIBLE,IMP_ADJ_WIDTH,ANCHO_FILA,PRECIO_PROV_POS,PRECIO_PROV_VISIBLE"
    sConsulta = sConsulta & ",PRECIO_PROV_WIDTH,PRECIO_PROV_LEVEL,CANT_PROV_POS,CANT_PROV_VISIBLE,CANT_PROV_WIDTH,CANT_PROV_LEVEL,ADJ_PROV_POS,ADJ_PROV_VISIBLE,ADJ_PROV_WIDTH"
    sConsulta = sConsulta & ",ADJ_PROV_LEVEL,IMP_ADJ_POS,IMP_ADJ_LEVEL,OCULTAR_IT_CERRADOS,EXCLUIR_IT_CERRADOS_RESULT,OCULTAR_NOADJ,OCULTAR_PROV_SIN_OFE,DEC_RESULT,DEC_PORCEN"
    sConsulta = sConsulta & ",DEC_PRECIOS,DEC_CANT,SOLIC_VINCULADA_POS,SOLIC_VINCULADA_VISIBLE,SOLIC_VINCULADA_WIDTH,SOLIC_VINCULADA_LEVEL"
    sConsulta = sConsulta & ",FECINI_POS,FECINI_VISIBLE,FECINI_WIDTH,FECINI_LEVEL,FECFIN_POS,FECFIN_VISIBLE,FECFIN_WIDTH,FECFIN_LEVEL"
    sConsulta = sConsulta & ",GMN_POS,GMN_VISIBLE,GMN_WIDTH,GMN_LEVEL,ANYOIMPUTACION_POS,ANYOIMPUTACION_VISIBLE,ANYOIMPUTACION_WIDTH,ANYOIMPUTACION_LEVEL"
    sConsulta = sConsulta & " FROM PLANTILLA_CONF_VISTAS_GRUPO WITH(NOLOCK) WHERE "
    sConsulta = sConsulta & " PLANT =" & oPlantilla.Id
    sConsulta = sConsulta & " AND ='" & DblQuote(oGrupo.Codigo) & "'"
    
    If Not IsMissing(iVista) Then
        sConsulta = sConsulta & " AND VISTA=" & iVista
    End If
    
    sConsulta = sConsulta & " ORDER BY VISTA"

    AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly

    If mCol Is Nothing Then
        Set mCol = Nothing
        Set mCol = New Collection
    End If
    
    If Not AdoRes.eof Then
        Set adofldVista = AdoRes.Fields("VISTA")
        Set adofldDescrWidth = AdoRes.Fields("DESCR_WIDTH")
        Set adofldAnyoPos = AdoRes.Fields("ANYO_POS")
        Set adofldAnyoVisible = AdoRes.Fields("ANYO_VISIBLE")
        Set adofldAnyoWidth = AdoRes.Fields("ANYO_WIDTH")
        Set adofldAnyoLevel = AdoRes.Fields("ANYO_LEVEL")
        Set adofldImportePos = AdoRes.Fields("IMP_POS")
        Set adofldImporteVisible = AdoRes.Fields("IMP_VISIBLE")
        Set adofldImporteWidth = AdoRes.Fields("IMP_WIDTH")
        Set adofldImporteLevel = AdoRes.Fields("IMP_LEVEL")
        Set adofldAdjPos = AdoRes.Fields("ADJ_POS")
        Set adofldAdjVisible = AdoRes.Fields("ADJ_VISIBLE")
        Set adofldAdjWidth = AdoRes.Fields("ADJ_WIDTH")
        Set adofldAdjLevel = AdoRes.Fields("ADJ_LEVEL")
        Set adofldProvPos = AdoRes.Fields("PROV_POS")
        Set adofldProvVisible = AdoRes.Fields("PROV_VISIBLE")
        Set adofldProvWidth = AdoRes.Fields("PROV_WIDTH")
        Set adofldProvLevel = AdoRes.Fields("PROV_LEVEL")
        Set adofldPresUniPos = AdoRes.Fields("PRESUNI_POS")
        Set adofldPresUniVisible = AdoRes.Fields("PRESUNI_VISIBLE")
        Set adofldPresUniWidth = AdoRes.Fields("PRESUNI_WIDTH")
        Set adofldPresUniLevel = AdoRes.Fields("PRESUNI_LEVEL")
        Set adofldAhorroImpPos = AdoRes.Fields("AHORRO_IMP_POS")
        Set adofldAhorroImpVisible = AdoRes.Fields("AHORRO_IMP_VISIBLE")
        Set adofldAhorroImpWidth = AdoRes.Fields("AHORRO_IMP_WIDTH")
        Set adofldAhorroImpLevel = AdoRes.Fields("AHORRO_IMP_LEVEL")
        Set adofldAhorroPorcenPos = AdoRes.Fields("AHORRO_PORCEN_POS")
        Set adofldAhorroPorcenVisible = AdoRes.Fields("AHORRO_PORCEN_VISIBLE")
        Set adofldAhorroPorcenWidth = AdoRes.Fields("AHORRO_PORCEN_WIDTH")
        Set adofldAhorroPorcenLevel = AdoRes.Fields("AHORRO_PORCEN_LEVEL")
        Set adofldObjPos = AdoRes.Fields("OBJ_POS")
        Set adofldObjVisible = AdoRes.Fields("OBJ_VISIBLE")
        Set adofldObjWidth = AdoRes.Fields("OBJ_WIDTH")
        Set adofldObjLevel = AdoRes.Fields("OBJ_LEVEL")
        Set adofldCantPos = AdoRes.Fields("CANT_POS")
        Set adofldCantVisible = AdoRes.Fields("CANT_VISIBLE")
        Set adofldCantWidth = AdoRes.Fields("CANT_WIDTH")
        Set adofldCantLevel = AdoRes.Fields("CANT_LEVEL")
        Set adofldOcultarFila2 = AdoRes.Fields("OCULTAR_FILA2")
        Set adofldGrupo0Width = AdoRes.Fields("GRUPO0_WIDTH")
        Set adofldGruposWidth = AdoRes.Fields("GRUPOS_WIDTH")
        Set adofldImpAdjWidth = AdoRes.Fields("IMP_ADJ_WIDTH")
        Set adofldImpAdjVisible = AdoRes.Fields("IMP_ADJ_VISIBLE")
        Set adofldAnchoFila = AdoRes.Fields("ANCHO_FILA")
        Set adofldPrecioProvPos = AdoRes.Fields("PRECIO_PROV_POS")
        Set adofldPrecioProvVisible = AdoRes.Fields("PRECIO_PROV_VISIBLE")
        Set adofldPrecioProvWidth = AdoRes.Fields("PRECIO_PROV_WIDTH")
        Set adofldPrecioProvLevel = AdoRes.Fields("PRECIO_PROV_LEVEL")
        Set adofldCantProvPos = AdoRes.Fields("CANT_PROV_POS")
        Set adofldCantProvVisible = AdoRes.Fields("CANT_PROV_VISIBLE")
        Set adofldCantProvWidth = AdoRes.Fields("CANT_PROV_WIDTH")
        Set adofldCantProvLevel = AdoRes.Fields("CANT_PROV_LEVEL")
        Set adofldAdjProvPos = AdoRes.Fields("ADJ_PROV_POS")
        Set adofldAdjProvVisible = AdoRes.Fields("ADJ_PROV_VISIBLE")
        Set adofldAdjProvWidth = AdoRes.Fields("ADJ_PROV_WIDTH")
        Set adofldAdjProvLevel = AdoRes.Fields("ADJ_PROV_LEVEL")
        Set adofldImpAdjPos = AdoRes.Fields("IMP_ADJ_POS")
        Set adofldImpAdjLevel = AdoRes.Fields("IMP_ADJ_LEVEL")
        Set adofldOcultarItCerrados = AdoRes.Fields("OCULTAR_IT_CERRADOS")
        Set adofldExcluirItCerradosResult = AdoRes.Fields("EXCLUIR_IT_CERRADOS_RESULT")
        Set adofldOcultarNoAdj = AdoRes.Fields("OCULTAR_NOADJ")
        Set adofldOcultarProvSinOfe = AdoRes.Fields("OCULTAR_PROV_SIN_OFE")
        Set adofldDecResult = AdoRes.Fields("DEC_RESULT")
        Set adofldDecPorcen = AdoRes.Fields("DEC_PORCEN")
        Set adofldDecPrecios = AdoRes.Fields("DEC_PRECIOS")
        Set adofldDecCant = AdoRes.Fields("DEC_CANT")
        Set adofldSolicVinculadaPos = AdoRes.Fields("SOLIC_VINCULADA_POS")
        Set adofldSolicVinculadaVisible = AdoRes.Fields("SOLIC_VINCULADA_VISIBLE")
        Set adofldSolicVinculadaWidth = AdoRes.Fields("SOLIC_VINCULADA_WIDTH")
        Set adofldSolicVinculadaLevel = AdoRes.Fields("SOLIC_VINCULADA_LEVEL")
        Set adofldFecIniSumPos = AdoRes.Fields("FECINI_POS")
        Set adofldFecIniSumVisible = AdoRes.Fields("FECINI_VISIBLE")
        Set adofldFecIniSumWidth = AdoRes.Fields("FECINI_WIDTH")
        Set adofldFecIniSumLevel = AdoRes.Fields("FECINI_LEVEL")
        Set adofldFecFinSumPos = AdoRes.Fields("FECFIN_POS")
        Set adofldFecFinSumVisible = AdoRes.Fields("FECFIN_VISIBLE")
        Set adofldFecFinSumWidth = AdoRes.Fields("FECFIN_WIDTH")
        Set adofldFecFinSumLevel = AdoRes.Fields("FECFIN_LEVEL")
        Set adofldEstrMatPos = AdoRes.Fields("GMN_POS")
        Set adofldEstrMatVisible = AdoRes.Fields("GMN_VISIBLE")
        Set adofldEstrMatWidth = AdoRes.Fields("GMN_WIDTH")
        Set adofldEstrMatLevel = AdoRes.Fields("GMN_LEVEL")
        Set adofldAnyoPartidaPos = AdoRes.Fields("ANYOIMPUTACION_POS")
        Set adofldAnyoPartidaVisible = AdoRes.Fields("ANYOIMPUTACION_VISIBLE")
        Set adofldAnyoPartidaWidth = AdoRes.Fields("ANYOIMPUTACION_WIDTH")
        Set adofldAnyoPartidaLevel = AdoRes.Fields("ANYOIMPUTACION_LEVEL")
        
        'Rellena el tipo de usuario
        With TpConfVistas
        .bOcultarFila2 = adofldOcultarFila2.Value
        .dblAnchoFila = adofldAnchoFila.Value
        .dblDescrWidth = adofldDescrWidth.Value
        .dblGrupo0Width = adofldGrupo0Width.Value
        .dblGruposWidth = adofldGruposWidth.Value
        
        .bImpAdjVisible = adofldImpAdjVisible.Value
        .dblImpAdjWidth = adofldImpAdjWidth.Value
        .iImpAdjLevel = adofldImpAdjLevel.Value
        .iImpAdjPos = adofldImpAdjPos.Value
        
        .bAdjVisible = adofldAdjVisible.Value
        .iAdjLevel = adofldAdjLevel.Value
        .iAdjPos = adofldAdjPos.Value
        .dblAdjWidth = adofldAdjWidth.Value
        
        .bAdjProvVisible = adofldAdjProvVisible.Value
        .iAdjProvLevel = adofldAdjProvLevel.Value
        .iAdjProvPos = adofldAdjProvPos.Value
        .dblAdjProvWidth = adofldAdjProvWidth.Value
        
        .bAhorroImpVisible = adofldAhorroImpVisible.Value
        .iAhorroImpLevel = adofldAhorroImpLevel.Value
        .iAhorroImpPos = adofldAhorroImpPos.Value
        .dblAhorroImpWidth = adofldAhorroImpWidth.Value
        
        .bAhorroPorcenVisible = adofldAhorroPorcenVisible.Value
        .iAhorroPorcenLevel = adofldAhorroPorcenLevel.Value
        .iAhorroPorcenPos = adofldAhorroPorcenPos.Value
        .dblAhorroPorcenWidth = adofldAhorroPorcenWidth.Value
        
        .bAnyoVisible = adofldAnyoVisible.Value
        .iAnyoLevel = adofldAnyoLevel.Value
        .iAnyoPos = adofldAnyoPos.Value
        .dblAnyoWidth = adofldAnyoWidth.Value
        
        .bCantVisible = adofldCantVisible.Value
        .iCantLevel = adofldCantLevel.Value
        .iCantPos = adofldCantPos.Value
        .dblCantWidth = adofldCantWidth.Value
        
        .bCantProvVisible = adofldCantProvVisible.Value
        .iCantProvLevel = adofldCantProvLevel.Value
        .iCantProvPos = adofldCantProvPos.Value
        .dblCantProvWidth = adofldCantProvWidth.Value
        
        .bImporteVisible = adofldImporteVisible.Value
        .iImporteLevel = adofldImporteLevel.Value
        .iImportePos = adofldImportePos.Value
        .dblImporteWidth = adofldImporteWidth.Value
        
        .bObjVisible = adofldObjVisible.Value
        .iObjLevel = adofldObjLevel.Value
        .iObjPos = adofldObjPos.Value
        .dblObjWidth = adofldObjWidth.Value
        
        .bPrecioProvVisible = adofldPrecioProvVisible.Value
        .iPrecioProvLevel = adofldPrecioProvLevel.Value
        .iPrecioProvPos = adofldPrecioProvPos.Value
        .dblPrecioProvWidth = adofldPrecioProvWidth.Value
        
        .bPresUniVisible = adofldPresUniVisible.Value
        .iPresUniLevel = adofldPresUniLevel.Value
        .iPresUniPos = adofldPresUniPos.Value
        .dblPresUniWidth = adofldPresUniWidth.Value
        
        .bProvVisible = adofldProvVisible.Value
        .iProvLevel = adofldProvLevel.Value
        .iProvPos = adofldProvPos.Value
        .dblProvWidth = adofldProvWidth.Value
        
        .bOcultarItCerrados = adofldOcultarItCerrados.Value
        .bExcluirItCerradosResult = adofldExcluirItCerradosResult.Value
        .bOcultarNoAdj = adofldOcultarNoAdj.Value
        .bOcultarProvSinOfe = adofldOcultarProvSinOfe.Value
        .iDecResult = adofldDecResult.Value
        .iDecPorcen = adofldDecPorcen.Value
        .iDecPrecios = adofldDecPrecios.Value
        .iDecCant = adofldDecCant.Value
        
        .iSolicVinculadaPos = NullToDbl0(adofldSolicVinculadaPos.Value)
        .bSolicVinculadaVisible = adofldSolicVinculadaVisible.Value
        .dblSolicVinculadaWidth = NullToDbl0(adofldSolicVinculadaWidth.Value)
        .iSolicVinculadaLevel = adofldSolicVinculadaLevel.Value
        
        .iFecIniSumPos = NullToDbl0(adofldFecIniSumPos.Value)
        .bFecIniSumVisible = adofldFecIniSumVisible.Value
        .dblFecIniSumWidth = NullToDbl0(adofldFecIniSumWidth.Value)
        .iFecIniSumLevel = adofldFecIniSumLevel.Value
        
        .iFecFinSumPos = NullToDbl0(adofldFecFinSumPos.Value)
        .bFecFinSumVisible = adofldFecFinSumVisible.Value
        .dblFecFinSumWidth = NullToDbl0(adofldFecFinSumWidth.Value)
        .iFecFinSumLevel = adofldFecFinSumLevel.Value
        
        .iEstrMatPos = NullToDbl0(adofldEstrMatPos.Value)
        .bEstrMatVisible = adofldEstrMatVisible.Value
        .dblEstrMatWidth = NullToDbl0(adofldEstrMatWidth.Value)
        .iEstrMatLevel = adofldEstrMatLevel.Value
        
        .bEscSolicVinculada = False
        
        .iAnyoPartidaPos = NullToDbl0(adofldAnyoPartidaPos.Value)
        .bAnyoPartidaVisible = adofldAnyoPartidaVisible.Value
        .dblAnyoPartidaWidth = NullToDbl0(adofldAnyoPartidaWidth.Value)
        .iAnyoPartidaLevel = adofldAnyoPartidaLevel.Value
        End With
        
        If UsarIndice Then
            lIndice = 0

            While Not AdoRes.eof
                ' Dejamos en manos del programador cuando asignar la conexion al obejto usuario
                Me.Add oPlantilla, oGrupo, iVista, TpConfVistas, lIndice
                AdoRes.MoveNext
                lIndice = lIndice + 1
            Wend
        Else
            While Not AdoRes.eof
                ' Dejamos en manos del programador cuando asignar la conexion al obejto usuario
                Me.Add oPlantilla, oGrupo, iVista, TpConfVistas
                AdoRes.MoveNext
            Wend
        End If

        'Libera los objetos
        Set adofldVista = Nothing
        Set adofldDescrWidth = Nothing
        Set adofldAnyoPos = Nothing
        Set adofldAnyoVisible = Nothing
        Set adofldAnyoWidth = Nothing
        Set adofldAnyoLevel = Nothing
        Set adofldImportePos = Nothing
        Set adofldImporteVisible = Nothing
        Set adofldImporteWidth = Nothing
        Set adofldImporteLevel = Nothing
        Set adofldAdjPos = Nothing
        Set adofldAdjVisible = Nothing
        Set adofldAdjWidth = Nothing
        Set adofldAdjLevel = Nothing
        Set adofldProvPos = Nothing
        Set adofldProvVisible = Nothing
        Set adofldProvWidth = Nothing
        Set adofldProvLevel = Nothing
        Set adofldPresUniPos = Nothing
        Set adofldPresUniVisible = Nothing
        Set adofldPresUniWidth = Nothing
        Set adofldPresUniLevel = Nothing
        Set adofldAhorroImpPos = Nothing
        Set adofldAhorroImpVisible = Nothing
        Set adofldAhorroImpWidth = Nothing
        Set adofldAhorroImpLevel = Nothing
        Set adofldAhorroPorcenPos = Nothing
        Set adofldAhorroPorcenVisible = Nothing
        Set adofldAhorroPorcenWidth = Nothing
        Set adofldAhorroPorcenLevel = Nothing
        Set adofldOcultarFila2 = Nothing
        Set adofldObjPos = Nothing
        Set adofldObjVisible = Nothing
        Set adofldObjWidth = Nothing
        Set adofldObjLevel = Nothing
        Set adofldCantPos = Nothing
        Set adofldCantVisible = Nothing
        Set adofldCantWidth = Nothing
        Set adofldCantLevel = Nothing
        Set adofldGrupo0Width = Nothing
        Set adofldGruposWidth = Nothing
        Set adofldImpAdjWidth = Nothing
        Set adofldImpAdjVisible = Nothing
        Set adofldAnchoFila = Nothing
        Set adofldPrecioProvPos = Nothing
        Set adofldPrecioProvVisible = Nothing
        Set adofldPrecioProvWidth = Nothing
        Set adofldPrecioProvLevel = Nothing
        Set adofldCantProvPos = Nothing
        Set adofldCantProvVisible = Nothing
        Set adofldCantProvWidth = Nothing
        Set adofldCantProvLevel = Nothing
        Set adofldAdjProvPos = Nothing
        Set adofldAdjProvVisible = Nothing
        Set adofldAdjProvWidth = Nothing
        Set adofldAdjProvLevel = Nothing
        Set adofldImpAdjPos = Nothing
        Set adofldImpAdjLevel = Nothing
        Set adofldOcultarItCerrados = Nothing
        Set adofldExcluirItCerradosResult = Nothing
        Set adofldOcultarNoAdj = Nothing
        Set adofldOcultarProvSinOfe = Nothing
        Set adofldDecResult = Nothing
        Set adofldDecPorcen = Nothing
        Set adofldDecPrecios = Nothing
        Set adofldDecCant = Nothing
        Set adofldSolicVinculadaPos = Nothing
        Set adofldSolicVinculadaVisible = Nothing
        Set adofldSolicVinculadaWidth = Nothing
        Set adofldSolicVinculadaLevel = Nothing
        Set adofldFecIniSumPos = Nothing
        Set adofldFecIniSumVisible = Nothing
        Set adofldFecIniSumWidth = Nothing
        Set adofldFecIniSumLevel = Nothing
        Set adofldFecFinSumPos = Nothing
        Set adofldFecFinSumVisible = Nothing
        Set adofldFecFinSumWidth = Nothing
        Set adofldFecFinSumLevel = Nothing
        Set adofldEstrMatPos = Nothing
        Set adofldEstrMatVisible = Nothing
        Set adofldEstrMatWidth = Nothing
        Set adofldEstrMatLevel = Nothing
        Set adofldAnyoPartidaPos = Nothing
        Set adofldAnyoPartidaVisible = Nothing
        Set adofldAnyoPartidaWidth = Nothing
        Set adofldAnyoPartidaLevel = Nothing
    End If
    
    AdoRes.Close
    Set AdoRes = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CPlantConfVistasGrupo", "CargarTodasLasVistas", ERR, Erl)
      Exit Sub
   End If
End Sub
