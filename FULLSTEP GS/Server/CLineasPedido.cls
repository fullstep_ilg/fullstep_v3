VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CLineasPedido"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum
Private mCol As Collection
Private mvarConexion As CConexion

Friend Property Set Conexion(ByVal con As CConexion)
Set mvarConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = mvarConexion
End Property

Public Property Get Item(vntIndexKey As Variant) As CLineaPedido
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property
Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
       Set NewEnum = mCol.[_NewEnum]
End Property
Public Sub Remove(vntIndexKey As Variant)
   
    mCol.Remove vntIndexKey
End Sub
Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
     Count = mCol.Count
End If


End Property

Public Function Add(Optional ByVal Id As Variant, Optional ByVal AnyoPedido As Integer, Optional ByVal Pedido As Variant, Optional ByVal Orden As Variant, _
Optional ByVal Anyo As Variant, Optional ByVal GMN1 As Variant, Optional ByVal GMN2 As Variant, Optional ByVal GMN3 As Variant, Optional ByVal GMN4 As Variant, _
Optional ByVal ProceCod As Variant, Optional ByVal Item As Variant, Optional ByVal ProveCod As String, Optional ByVal ArtCod_Interno As String, Optional ByVal ArtCod_Externo As String, _
Optional ByVal ArtDen As String, Optional ByVal Hom As Boolean, Optional ByVal UnidadCompra As String, Optional ByVal UnidadPedido As String, Optional ByVal FC As Double, Optional ByVal PrecioUC As Double, Optional ByVal PrecioUP As Double, _
Optional ByVal CantidadAdj As Variant, Optional ByVal CantidadPedida As Double, Optional ByVal CantidadPedidaTotal As Double, Optional ByVal CodDestino As String, Optional ByVal FechaEntrega As Variant, Optional ByVal Obligat As Boolean, _
Optional ByVal FechaEmision As Date, Optional ByVal Cat1 As Variant, Optional ByVal Cat2 As Variant, Optional ByVal Cat3 As Variant, Optional ByVal Cat4 As Variant, Optional ByVal Cat5 As Variant, Optional ByVal Seguridad As Variant, _
Optional ByVal Estado As Variant, Optional ByVal NivelAprobacion As Variant, Optional ByVal Aprobacion As TipoEstadoAprobacionLineaPedido, Optional ByVal Observaciones As String, Optional ByVal oUnidadesPedido As CUnidadesPedido, _
Optional ByVal varIndice As Variant, Optional ByVal FecEntProve As Variant, Optional ByVal FecAct As Date, Optional ByVal PrecioAdj As Double, Optional ByVal ProveDen As String, Optional ByVal bConRecep As Boolean, Optional ByVal vSolicitud As Variant, _
Optional ByVal Dato1 As Variant, Optional ByVal Valor1 As Variant, Optional ByVal Dato2 As Variant, Optional ByVal Valor2 As Variant, Optional ByVal DenCampo1 As Variant, Optional ByVal DenCampo2 As Variant, _
Optional ByVal Adjuntos As Long, Optional ByVal ObsAdjun As Variant, Optional ByVal bConModificaciones As Boolean, _
Optional ByVal Centro As String = "", Optional ByVal Almacen As Variant, Optional ByVal PedidoAbierto As Variant, Optional ByVal GMN1Proce As Variant) As CLineaPedido
   
    Dim objnewmember As CLineaPedido
    Dim sCod As String
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New CLineaPedido
    Set objnewmember.Conexion = mvarConexion
    
    If Not IsMissing(Id) Then
        objnewmember.Id = Id
    Else
        objnewmember.Id = Null
    End If
    
    objnewmember.AnyoPedido = AnyoPedido
    
    If Not IsMissing(Pedido) Then
        objnewmember.Pedido = Pedido
    Else
        objnewmember.Pedido = Null
    End If
    
    objnewmember.Anyo = Anyo
    
    If Not IsMissing(Orden) Then
        objnewmember.Orden = Orden
    Else
        objnewmember.Orden = Null
    End If
    
    objnewmember.GMN1Cod = GMN1
    objnewmember.GMN2Cod = GMN2
    objnewmember.GMN3Cod = GMN3
    objnewmember.GMN4Cod = GMN4

    objnewmember.ProceCod = ProceCod
    objnewmember.Item = Item
    objnewmember.ProveCod = ProveCod
    objnewmember.ProveDen = ProveDen
    objnewmember.ArtCod_Interno = ArtCod_Interno
    objnewmember.ArtCod_Externo = ArtCod_Externo
    objnewmember.ArtDen = ArtDen
    objnewmember.Hom = Hom
    objnewmember.UnidadCompra = UnidadCompra
    objnewmember.UnidadPedido = UnidadPedido
    objnewmember.FC = FC
    objnewmember.PrecioUC = PrecioUC
    objnewmember.PrecioUP = PrecioUP
    If IsMissing(FechaEntrega) Then
        objnewmember.CantidadAdj = Null
    Else
        objnewmember.CantidadAdj = CantidadAdj
    End If
    objnewmember.CantidadPedida = CantidadPedida
    objnewmember.CantidadPedidaTotal = CantidadPedidaTotal
    objnewmember.CodDestino = CodDestino
    If IsMissing(FechaEntrega) Then
        objnewmember.FechaEntrega = Null
    Else
        objnewmember.FechaEntrega = FechaEntrega
    End If
    
    objnewmember.Obligat = Obligat
    objnewmember.FechaEmision = FechaEmision
    objnewmember.Cat1 = Cat1
    objnewmember.Cat2 = Cat2
    objnewmember.Cat3 = Cat3
    objnewmember.Cat4 = Cat4
    objnewmember.Cat5 = Cat5
    
    If IsMissing(Seguridad) Then
        objnewmember.Seguridad = Null
    Else
        objnewmember.Seguridad = Seguridad
    End If
    
    objnewmember.Estado = Estado
    objnewmember.NivelAprobacion = NivelAprobacion
    objnewmember.Observaciones = Observaciones
    
    If oUnidadesPedido Is Nothing Then
        Set objnewmember.UnidadesPedido = New CUnidadesPedido
    Else
        Set objnewmember.UnidadesPedido = oUnidadesPedido
    End If
    
    If IsMissing(FecEntProve) Then
        objnewmember.FecEntProve = Null
    Else
        objnewmember.FecEntProve = FecEntProve
    End If
    
    objnewmember.FecAct = FecAct
    objnewmember.ConRecepciones = bConRecep
    objnewmember.PrecioAdj = PrecioAdj
    
    objnewmember.Dato1 = Dato1
    objnewmember.Dato2 = Dato2
    objnewmember.Valor1 = Valor1
    objnewmember.Valor2 = Valor2
    objnewmember.DenDato1 = DenCampo1
    objnewmember.DenDato2 = DenCampo2
    
    objnewmember.ConModificaciones = bConModificaciones
    
    objnewmember.Centro = Centro
    If IsMissing(Almacen) Then
        objnewmember.Almacen = Null
    Else
        objnewmember.Almacen = Almacen
    End If
    
    If gParametrosGenerales.gsAccesoFSSM = AccesoFSSM Then
        Set objnewmember.Imputaciones = New CImputaciones
        Set objnewmember.Imputaciones.Conexion = mvarConexion
    End If
    
    Set objnewmember.Activo = Nothing
    
    Set objnewmember.Conexion = mvarConexion
    
    If Not IsMissing(varIndice) And Not IsNull(varIndice) Then
        objnewmember.Indice = varIndice
        mCol.Add objnewmember, CStr(varIndice)
    Else
        If Not IsMissing(Id) Then
            mCol.Add objnewmember, CStr(Id)
        Else
            sCod = GMN1Proce & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(GMN1Proce))
            mCol.Add objnewmember, CStr(Anyo) & sCod & CStr(ProceCod) & CStr(Item) & CStr(ArtCod_Interno)
        End If
    End If
    
    If Not IsMissing(vSolicitud) Then
        objnewmember.Solicitud = vSolicitud
    Else
        objnewmember.Solicitud = Null
    End If
    
    objnewmember.NumAdjuntos = Adjuntos
    If Not IsMissing(ObsAdjun) Then
        objnewmember.ObsAdjun = ObsAdjun
    Else
        objnewmember.ObsAdjun = Null
    End If
    
    If Not IsMissing(PedidoAbierto) Then
        objnewmember.PedidoAbierto = PedidoAbierto
    Else
        objnewmember.PedidoAbierto = 0
    End If
    
    If Not IsMissing(GMN1Proce) Then
        objnewmember.GMN1Proce = StrToVbNULL(GMN1Proce)
    Else
        objnewmember.GMN1Proce = Null
    End If
    
    Set Add = objnewmember
    
    Set objnewmember = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CLineasPedido", "Add", ERR, Erl)
      Exit Function
   End If

End Function
Private Sub Class_Initialize()
    Set mCol = New Collection
End Sub
Private Sub Class_Terminate()
    'destroys collection when this class is terminated
    Set mCol = Nothing
    Set mvarConexion = Nothing
End Sub
