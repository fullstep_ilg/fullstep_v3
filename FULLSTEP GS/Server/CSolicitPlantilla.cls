VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CSolicitPlantilla"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
''' *** Clase: CSolicitPlantilla

Option Explicit

Implements IBaseDatos

Private m_sIdioma As String
Private m_sRuta As String
Private m_vFecAct As Variant
Private m_lIndice As Long
Private m_lID As Long
Private m_lSolicitud As Long

''' Conexion
Private m_oConexion As CConexion

''' Control de errores
Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum

Public Property Get Indice() As Long

    Indice = m_lIndice
    
End Property
Public Property Let Indice(ByVal lInd As Long)

    m_lIndice = lInd
    
End Property

Friend Property Set Conexion(ByVal vData As CConexion)

    Set m_oConexion = vData
    
End Property

Friend Property Get Conexion() As CConexion

    Set Conexion = m_oConexion
    
End Property

Public Property Get Id() As Long
    Id = m_lID
End Property
Public Property Let Id(ByVal lId As Long)
    m_lID = lId
End Property

Public Property Let Idioma(ByVal vData As String)

    m_sIdioma = vData
    
End Property
Public Property Get Idioma() As String

    Idioma = m_sIdioma
    
End Property

Public Property Let Ruta(ByVal vData As String)

    m_sRuta = vData
    
End Property
Public Property Get Ruta() As String

    Ruta = m_sRuta
    
End Property

Public Property Let FecAct(ByVal vData As Variant)

    m_vFecAct = vData
    
End Property
Public Property Get FecAct() As Variant
    FecAct = m_vFecAct
    
End Property

Public Property Let Solicitud(ByVal lSol As Long)
    m_lSolicitud = lSol
End Property

Public Property Get Solicitud() As Long
    Solicitud = m_lSolicitud
End Property

Private Sub Class_Terminate()
    Set m_oConexion = Nothing
End Sub

''' <summary>
''' Inserta en bbdd un registro de SOLICITUD_PLANTILLAS
''' </summary>
''' <returns>Error de haberlo</returns>
''' <remarks>Llamada desde:frmParContr; Tiempo m�ximo:0,1</remarks>
Private Function IBaseDatos_AnyadirABaseDatos() As TipoErrorSummit
Dim rs As adodb.Recordset
Dim sConsulta As String
Dim TESError As TipoErrorSummit
'Dim btrans As Boolean
Dim sCod As String
Dim oMulti As CMultiidioma
Dim NoExistianEstados As Boolean

'********Precondici�n******************************************************************************************************************
If m_oConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CSolicitPlantilla.AnyadirABAseDatos", "No se ha establecido la conexion"
End If
'**************************************************************************************************************************************

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

TESError.NumError = TESnoerror
If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:

    'm_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
    'btrans = True

    'If Not m_oDen Is Nothing Then
    '    For Each oMulti In m_oDen
    '        sCod = sCod & "," & "DEN_" & oMulti.Cod
    '    Next
    'End If
    
    'Set rs = New adodb.Recordset
    'sConsulta = "SELECT COUNT(1) FROM SOLICITUD_ESTADOS WITH (NOLOCK) WHERE SOLICITUD=" & m_oSolicitud.ID
    'rs.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
    'NoExistianEstados = (rs(0).Value = 0)
    'rs.Close
    'Set rs = Nothing
    
    sConsulta = "INSERT INTO SOLICITUD_PLANTILLAS (SOLICITUD,IDIOMA,RUTA) "
    sConsulta = sConsulta & " VALUES (" & m_lSolicitud & ",'" & DblQuote(m_sIdioma) & "'," & StrToSQLNULL(m_sRuta) & ")"
    m_oConexion.ADOCon.Execute sConsulta
    
    Set rs = New adodb.Recordset
    'Calidad Sin with(nolock)
    rs.Open "SELECT @@IDENTITY", m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
    m_lID = rs(0).Value
    rs.Close
    Set rs = Nothing
    
    TESError.NumError = TESnoerror
    IBaseDatos_AnyadirABaseDatos = TESError
    
    Exit Function
    
Error_Cls:
    IBaseDatos_AnyadirABaseDatos = basErrores.TratarError(m_oConexion.ADOCon.Errors)
        
    Set rs = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CSolicitPlantilla", "IBaseDatos_AnyadirABaseDatos", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If
    
End Function
Private Function IBaseDatos_CambiarCodigo(CodigoNuevo As Variant) As TipoErrorSummit
            
End Function

Private Sub IBaseDatos_CancelarEdicion()

End Sub
Private Function IBaseDatos_ComprobarExistenciaEnBaseDatos() As Boolean

End Function
Private Function IBaseDatos_EliminarDeBaseDatos() As TipoErrorSummit
    Dim TESError As TipoErrorSummit
    Dim sConsulta As String
    
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
        
    TESError.NumError = TESnoerror

    sConsulta = "DELETE FROM SOLICITUD_PLANTILLAS WHERE SOLICITUD =" & m_lSolicitud & " AND IDIOMA='" & DblQuote(m_sIdioma) & "'"
    m_oConexion.ADOCon.Execute sConsulta
    
    IBaseDatos_EliminarDeBaseDatos = TESError
    
    Exit Function
    
Error_Cls:
    IBaseDatos_EliminarDeBaseDatos = basErrores.TratarError(m_oConexion.ADOCon.Errors)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CSolicitPlantilla", "IBaseDatos_EliminarDeBaseDatos", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If

End Function

Private Function IBaseDatos_FinalizarEdicionEliminando() As TipoErrorSummit

    ''' ! No usado por ahora
    
End Function

''' <summary>
''' Modifica en bbdd un registro de SOLICITUD_PLANTILLAS
''' </summary>
''' <returns>Error de haberlo</returns>
''' <remarks>Llamada desde:frmPARContr; Tiempo m�ximo:0,1</remarks>
Private Function IBaseDatos_FinalizarEdicionModificando() As TipoErrorSummit
Dim TESError As TipoErrorSummit
Dim btrans As Boolean
Dim sConsulta As String
Dim rs As adodb.Recordset
Dim i As Integer
Dim oMulti As CMultiidioma


    TESError.NumError = TESnoerror

    ''' Precondicion
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CSolicitPlantilla.FinalizarEdicionModificando", "No se ha establecido la conexion"
    End If
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
    
    sConsulta = "UPDATE SOLICITUD_PLANTILLAS SET"
    sConsulta = sConsulta & " IDIOMA = " & StrToSQLNULL(m_sIdioma) & ","
    sConsulta = sConsulta & " RUTA = " & StrToSQLNULL(m_sRuta)
    sConsulta = sConsulta & " WHERE ID=" & m_lID
    m_oConexion.ADOCon.Execute sConsulta

    TESError.NumError = TESnoerror
    IBaseDatos_FinalizarEdicionModificando = TESError
    
    Exit Function
    

Error_Cls:
    TESError = basErrores.TratarError(m_oConexion.ADOCon.Errors)
   
    Resume Salir:
    
Salir:

    On Error Resume Next
    If Not rs Is Nothing Then
        rs.Close
    End If
    
    IBaseDatos_FinalizarEdicionModificando = TESError
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CSolicitPlantilla", "IBaseDatos_FinalizarEdicionModificando", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If

End Function

''' <summary>
''' Carga las propertys de la clase
''' </summary>
''' <param name="Bloquear">bloquear si/no</param>
''' <param name="UsuarioBloqueo">usuario q bloquea</param>
''' <returns>Error de haberlo</returns>
''' <remarks>Llamada desde: frmSOLConfigEstados/sdbgEstados_Change; Tiempo m�ximo: 0,1</remarks>
Private Function IBaseDatos_IniciarEdicion(Optional ByVal Bloquear As Boolean, Optional ByVal UsuarioBloqueo As String) As TipoErrorSummit
      
    
End Function

