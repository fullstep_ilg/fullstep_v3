VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CParametroExterno"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum

Private m_oConexion As CConexion

Private m_iSentido As Integer
Private m_iID As Integer
Private m_sParam As String
Private m_iTipo As Integer
Private m_iDirecto As Boolean
Private m_sValor As String
Private m_iGrupo As Long
Private m_iTipoCampo As Integer
Private m_iCampo As Long
Private m_iIndError As Boolean

Public Property Let Sentido(ByVal dato As Integer)
    m_iSentido = dato
End Property
Public Property Get Sentido() As Integer
    Sentido = m_iSentido
End Property

Public Property Let Id(ByVal dato As Integer)
    m_iID = dato
End Property
Public Property Get Id() As Integer
    Id = m_iID
End Property

Public Property Let param(ByVal dato As String)
    m_sParam = dato
End Property
Public Property Get param() As String
    param = m_sParam
End Property

Public Property Let Tipo(ByVal dato As Integer)
    m_iTipo = dato
End Property
Public Property Get Tipo() As Integer
    Tipo = m_iTipo
End Property

Public Property Let Directo(ByVal dato As Boolean)
    m_iDirecto = dato
End Property
Public Property Get Directo() As Boolean
    Directo = m_iDirecto
End Property

Public Property Let Valor(ByVal dato As String)
    m_sValor = dato
End Property
Public Property Get Valor() As String
    Valor = m_sValor
End Property

Public Property Let Grupo(ByVal dato As Long)
    m_iGrupo = dato
End Property
Public Property Get Grupo() As Long
    Grupo = m_iGrupo
End Property

Public Property Let TipoCampo(ByVal dato As Integer)
    m_iTipoCampo = dato
End Property
Public Property Get TipoCampo() As Integer
    TipoCampo = m_iTipoCampo
End Property

Public Property Let Campo(ByVal dato As Long)
    m_iCampo = dato
End Property
Public Property Get Campo() As Long
    Campo = m_iCampo
End Property

Public Property Let IndError(ByVal dato As Boolean)
    m_iIndError = dato
End Property
Public Property Get IndError() As Boolean
    IndError = m_iIndError
End Property

Friend Property Set Conexion(ByVal vData As CConexion)
    ''' * Objetivo: Dar valor a la conexion de la Destino
    ''' * Recibe: Conexion
    ''' * Devuelve: Nada
    Set m_oConexion = vData
End Property

Friend Property Get Conexion() As CConexion
    ''' * Objetivo: Devolver la conexion de la Destino
    ''' * Recibe: Nada
    ''' * Devuelve: Conexion
    Set Conexion = m_oConexion
End Property

Private Sub Class_Terminate()
    On Error GoTo Error:
    
    Set m_oConexion = Nothing
    Exit Sub
Error:
    Resume Next
    
End Sub

