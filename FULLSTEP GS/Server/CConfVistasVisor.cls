VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CConfVistasVisor"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CConfVistasVisor **********************************
'*             Autor : Mertxe Martin
'*             Creada : 24/09/2004
'****************************************************************

Option Explicit

Private mCol As Collection
Private m_oConexion As CConexion
Private m_oConexionServer As CConexionDeUseServer
Private m_bEOF As Boolean

Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum

''' <summary>
''' A�ade una configuraci�n de campos del visor procesos a la colecci�n
''' </summary>
''' <param name="sUsuario">Usuario</param>
''' <param name="ConfigVisor">configuraci�n de campos</param>
''' <param name="vIndice">Indice</param>
''' <returns>configuraci�n de campos del visor procesos a�adido</returns>
''' <remarks>Llamada desde: CargarConfVisor ; Tiempo m�ximo: 0,2</remarks>
Public Function Add(ByVal sUsuario As String, ConfigVisor As TipoConfigVisor, Optional ByVal vIndice As Variant) As CConfVistaVisor
    'create a new object
    Dim sCodGmn1 As String
    Dim objnewmember As CConfVistaVisor
        
    If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
       
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New CConfVistaVisor
    
    With objnewmember
        .Usuario = sUsuario
        .AnyoVisible = ConfigVisor.bAnyoVisible
        .AnyoPos = ConfigVisor.iAnyoPos
        .AnyoWidth = ConfigVisor.dblAnyoWidth
        .GMN1Visible = ConfigVisor.bGMN1Visible
        .GMN1Pos = ConfigVisor.iGMN1Pos
        .GMN1Width = ConfigVisor.dblGMN1Width
        .ProceVisible = ConfigVisor.bProceVisible
        .ProcePos = ConfigVisor.iProcePos
        .ProceWidth = ConfigVisor.dblProceWidth
        .DescrVisible = ConfigVisor.bDescrVisible
        .DescrPos = ConfigVisor.iDescrPos
        .DescrWidth = ConfigVisor.dblDescrWidth
        .EstadoVisible = ConfigVisor.bEstadoVisible
        .EstadoPos = ConfigVisor.iEstadoPos
        .EstadoWidth = ConfigVisor.dblEstadoWidth
        .ResponsableVisible = ConfigVisor.bRespVisible
        .ResponsablePos = ConfigVisor.iRespPos
        .ResponsableWidth = ConfigVisor.dblRespWidth
        .AdjDirVisible = ConfigVisor.bAdjDirVisible
        .AdjDirPos = ConfigVisor.iAdjDirPos
        .AdjDirWidth = ConfigVisor.dblAdjDirWidth
        .PVisible = ConfigVisor.bPVisible
        .PPos = ConfigVisor.iPPos
        .PWidth = ConfigVisor.dblPWidth
        .OVisible = ConfigVisor.bOVisible
        .OPos = ConfigVisor.iOPos
        .OWidth = ConfigVisor.dblOWidth
        .WVisible = ConfigVisor.bWVisible
        .WPos = ConfigVisor.iWPos
        .WWidth = ConfigVisor.dblWWidth
        .RVisible = ConfigVisor.bRVisible
        .RPos = ConfigVisor.iRPos
        .RWidth = ConfigVisor.dblRWidth
        .FecApeVisible = ConfigVisor.bFecApeVisible
        .FecApePos = ConfigVisor.iFecApePos
        .FecApeWidth = ConfigVisor.dblFecApeWidth
        .FecLimOfeVisible = ConfigVisor.bFecLimOfeVisible
        .FecLimOfePos = ConfigVisor.iFecLimOfePos
        .FecLimOfeWidth = ConfigVisor.dblFecLimOfeWidth
        .FecPresVisible = ConfigVisor.bFecPresVisible
        .FecPresPos = ConfigVisor.iFecPresPos
        .FecPresWidth = ConfigVisor.dblFecPresWidth
        .FecCierreVisible = ConfigVisor.bFecCierreVisible
        .FecCierrePos = ConfigVisor.iFecCierrePos
        .FecCierreWidth = ConfigVisor.dblFecCierreWidth
        .FecNecVisible = ConfigVisor.bFecNecVisible
        .FecNecPos = ConfigVisor.iFecNecPos
        .FecNecWidth = ConfigVisor.dblFecNecWidth
        .FecIniVisible = ConfigVisor.bFecIniVisible
        .FecIniPos = ConfigVisor.iFecIniPos
        .FecIniWidth = ConfigVisor.dblFecIniWidth
        .FecFinVisible = ConfigVisor.bFecFinVisible
        .FecFinPos = ConfigVisor.iFecFinPos
        .FecFinWidth = ConfigVisor.dblFecFinWidth
        .PresVisible = ConfigVisor.bPresVisible
        .PresPos = ConfigVisor.iPresPos
        .PresWidth = ConfigVisor.dblPresWidth
        .ConsumidoVisible = ConfigVisor.bConsumidoVisible
        .ConsumidoPos = ConfigVisor.iConsumidoPos
        .ConsumidoWidth = ConfigVisor.dblConsumidoWidth
        .ImporteVisible = ConfigVisor.bImporteVisible
        .ImportePos = ConfigVisor.iImportePos
        .ImporteWidth = ConfigVisor.dblImporteWidth
        .AhorroVisible = ConfigVisor.bAhorroVisible
        .AhorroPos = ConfigVisor.iAhorroPos
        .AhorroWidth = ConfigVisor.dblAhorroWidth
        .MonedaVisible = ConfigVisor.bMonedaVisible
        .MonedaPos = ConfigVisor.iMonedaPos
        .MonedaWidth = ConfigVisor.dblMonedaWidth
        .DestinoVisible = ConfigVisor.bDestVisible
        .DestinoPos = ConfigVisor.iDestPos
        .DestinoWidth = ConfigVisor.dblDestWidth
        .DestinoDenVisible = ConfigVisor.bDestDenVisible
        .DestinoDenPos = ConfigVisor.iDestDenPos
        .DestinoDenWidth = ConfigVisor.dblDestDenWidth
        .PagoVisible = ConfigVisor.bPagoVisible
        .PagoPos = ConfigVisor.iPagoPos
        .PagoWidth = ConfigVisor.dblPagoWidth
        .ProveedorVisible = ConfigVisor.bProvVisible
        .ProveedorPos = ConfigVisor.iProvPos
        .ProveedorWidth = ConfigVisor.dblProvWidth
        .ProveedorDenVisible = ConfigVisor.bProvDenVisible
        .ProveedorDenPos = ConfigVisor.iProvDenPos
        .ProveedorDenWidth = ConfigVisor.dblProvDenWidth
        .gsToErpVisible = ConfigVisor.bgsToErpVisible
        .gsToErpPos = ConfigVisor.igsToErpPos
        .gsToErpWidth = ConfigVisor.dblgsToErpWidth
        .FecValAperturaVisible = ConfigVisor.bFecValAperturaVisible
        .FecValAperturaPos = ConfigVisor.iFecValAperturaPos
        .FecValAperturaWidth = ConfigVisor.dblFecValAperturaWidth
        .PorcentajeAhorroVisible = ConfigVisor.bPorcentajeAhorroVisible
        .PorcentajeAhorroPos = ConfigVisor.iPorcentajeAhorroPos
        .PorcentajeAhorroWidth = ConfigVisor.dblPorcentajeAhorroWidth
        .CambioVisible = ConfigVisor.bCambioVisible
        .CambioPos = ConfigVisor.iCambioPos
        .CambioWidth = ConfigVisor.dblCambioWidth
        .SolicVinculadaVisible = ConfigVisor.bSolicVinculadaVisible
        .SolicVinculadaPos = ConfigVisor.iSolicVinculadaPos
        .SolicVinculadaWidth = ConfigVisor.dblSolicVinculadaWidth
        .OcultarFiltro = ConfigVisor.bOcultarFiltro
        .OcultarMenu = ConfigVisor.bOcultarMenu
        .AnyoDesde = ConfigVisor.vAnyoDesde
        .MesDesde = ConfigVisor.vMesDesde
        Set .GMN1Cod = ConfigVisor.oGMN1
        Set .GMN2Cod = ConfigVisor.oGMN2
        Set .GMN3Cod = ConfigVisor.oGMN3
        Set .GMN4Cod = ConfigVisor.oGMN4
        .CodEqp = ConfigVisor.vCodEqp
        .CodComprador = ConfigVisor.vCodCom
        .Responsable = ConfigVisor.bResponsable
        .UON1 = ConfigVisor.vUON1
        .UON2 = ConfigVisor.vUON2
        .UON3 = ConfigVisor.vUON3
        .Pres1Niv1 = ConfigVisor.vPres1Niv1
        .Pres1Niv2 = ConfigVisor.vPres1Niv2
        .Pres1Niv3 = ConfigVisor.vPres1Niv3
        .Pres1Niv4 = ConfigVisor.vPres1Niv4
        .Pres2Niv1 = ConfigVisor.vPres2Niv1
        .Pres2Niv2 = ConfigVisor.vPres2Niv2
        .Pres2Niv3 = ConfigVisor.vPres2Niv3
        .Pres2Niv4 = ConfigVisor.vPres2Niv4
        .Pres3Niv1 = ConfigVisor.vPres3Niv1
        .Pres3Niv2 = ConfigVisor.vPres3Niv2
        .Pres3Niv3 = ConfigVisor.vPres3Niv3
        .Pres3Niv4 = ConfigVisor.vPres3Niv4
        .Pres4Niv1 = ConfigVisor.vPres4Niv1
        .Pres4Niv2 = ConfigVisor.vPres4Niv2
        .Pres4Niv3 = ConfigVisor.vPres4Niv3
        .Pres4Niv4 = ConfigVisor.vPres4Niv4
        .Denominacion = ConfigVisor.vDenominacion
        .AtrBuscarVisible = ConfigVisor.bAtrBuscarVisible
        Set .Conexion = m_oConexion
        Set .ConexionServer = m_oConexionServer
    End With
       
    If Not IsMissing(vIndice) And Not IsNull(vIndice) Then
        objnewmember.Indice = vIndice
        mCol.Add objnewmember, CStr(vIndice)
    Else
        mCol.Add objnewmember, CStr(sUsuario)
    End If
    
    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing

    '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CConfVistasVisor", "Add", ERR, Erl)
      Exit Function
   End If

End Function

Friend Property Set Conexion(ByVal con As CConexion)
    Set m_oConexion = con
End Property

Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property

Friend Property Set ConexionServer(ByVal con As CConexionDeUseServer)
    Set m_oConexionServer = con
End Property

Friend Property Get ConexionServer() As CConexionDeUseServer
    Set ConexionServer = m_oConexionServer
End Property

Public Property Get Count() As Long
    If mCol Is Nothing Then
        Count = 0
    Else
         Count = mCol.Count
    End If
End Property

Public Property Get eof() As Boolean
    eof = m_bEOF
End Property

Friend Property Let eof(ByVal b As Boolean)
    m_bEOF = b
End Property

Public Property Get Item(vntIndexKey As Variant) As CConfVistaVisor
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
     Set NewEnum = mCol.[_NewEnum]
End Property

Public Sub Remove(vntIndexKey As Variant)

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    mCol.Remove vntIndexKey
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CConfVistasVisor", "Remove", ERR, Erl)
      Exit Sub
   End If

End Sub

Private Sub Class_Initialize()
    Set mCol = New Collection
End Sub

Private Sub Class_Terminate()
    'Proceroys collection when this class is terminated
    On Error Resume Next
    Set mCol = Nothing
    Set m_oConexion = Nothing
End Sub

''' <summary>
''' Carga la configuraci�n del visor de procesos para un usuario
''' </summary>
''' <param name="sUsuario">Usuario</param>
''' <param name="UsarIndice">Indica si la colecci�n interna se va a indexar</param>
''' <remarks>Llamada desde: frmEST/CargarConfiguracionVisor ; Tiempo m�ximo: 0,2</remarks>
''' Revisado por: jbg; Fecha: 08/01/2013
Public Sub CargarConfVisor(ByVal sUsuario As String, Optional ByVal UsarIndice As Boolean)
    Dim AdoRes As New adodb.Recordset
    Dim lIndice As Long
    Dim TpConfVisor As TipoConfigVisor
    Dim sGMN As String
    Dim arGMN() As String
    Dim arCod() As String
    Dim i As Integer
    Dim oCom As adodb.Command
    Dim oParam As adodb.Parameter
    
    '********* Precondicion **************************************
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CConfVistasVisor.CargarConfVisor", "No se ha establecido la conexion"
        Exit Sub
    End If
    '*************************************************************
    
    If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
    Set oCom = New adodb.Command
    With oCom
        Set .ActiveConnection = m_oConexion.ADOCon
        .CommandType = adCmdStoredProc
        .CommandText = "FSGS_DEVOLVER_CONF_VISOR"
        
        Set oParam = .CreateParameter("@USU", adVarChar, adParamInput, Len(sUsuario), sUsuario)
        .Parameters.Append oParam
        
        Set AdoRes = .Execute
    End With
        
    If mCol Is Nothing Then
        Set mCol = Nothing
        Set mCol = New Collection
    End If
        
    If Not AdoRes.eof Then
        'jpa Tarea 178
        With TpConfVisor
            .bAnyoVisible = SQLBinaryToBoolean(AdoRes.Fields("ANYO_VISIBLE").Value)
            .iAnyoPos = AdoRes.Fields("ANYO_POS").Value
            .dblAnyoWidth = AdoRes.Fields("ANYO_WIDTH").Value
            .bGMN1Visible = SQLBinaryToBoolean(AdoRes.Fields("GMN1_VISIBLE").Value)
            .iGMN1Pos = AdoRes.Fields("GMN1_POS").Value
            .dblGMN1Width = AdoRes.Fields("GMN1_WIDTH").Value
            .bProceVisible = SQLBinaryToBoolean(AdoRes.Fields("PROCE_VISIBLE").Value)
            .iProcePos = AdoRes.Fields("PROCE_POS").Value
            .dblProceWidth = AdoRes.Fields("PROCE_WIDTH").Value
            .bDescrVisible = SQLBinaryToBoolean(AdoRes.Fields("DESCR_VISIBLE").Value)
            .iDescrPos = AdoRes.Fields("DESCR_POS").Value
            .dblDescrWidth = AdoRes.Fields("DESCR_WIDTH").Value
            .bEstadoVisible = SQLBinaryToBoolean(AdoRes.Fields("EST_VISIBLE").Value)
            .iEstadoPos = AdoRes.Fields("EST_POS").Value
            .dblEstadoWidth = AdoRes.Fields("EST_WIDTH").Value
            .bRespVisible = SQLBinaryToBoolean(AdoRes.Fields("RESP_VISIBLE").Value)
            .iRespPos = AdoRes.Fields("RESP_POS").Value
            .dblRespWidth = AdoRes.Fields("RESP_WIDTH").Value
            .bAdjDirVisible = SQLBinaryToBoolean(AdoRes.Fields("DIR_VISIBLE").Value)
            .iAdjDirPos = AdoRes.Fields("DIR_POS").Value
            .dblAdjDirWidth = AdoRes.Fields("DIR_WIDTH").Value
            .bOVisible = SQLBinaryToBoolean(AdoRes.Fields("O_VISIBLE").Value)
            .iOPos = AdoRes.Fields("O_POS").Value
            .dblOWidth = AdoRes.Fields("O_WIDTH").Value
            .bPVisible = SQLBinaryToBoolean(AdoRes.Fields("P_VISIBLE").Value)
            .iPPos = AdoRes.Fields("P_POS").Value
            .dblPWidth = AdoRes.Fields("P_WIDTH").Value
            .bWVisible = SQLBinaryToBoolean(AdoRes.Fields("W_VISIBLE").Value)
            .iWPos = AdoRes.Fields("W_POS").Value
            .dblWWidth = AdoRes.Fields("W_WIDTH").Value
            .bRVisible = SQLBinaryToBoolean(AdoRes.Fields("R_VISIBLE").Value)
            .iRPos = AdoRes.Fields("R_POS").Value
            .dblRWidth = AdoRes.Fields("R_WIDTH").Value
            .bFecApeVisible = SQLBinaryToBoolean(AdoRes.Fields("FECAPE_VISIBLE").Value)
            .iFecApePos = AdoRes.Fields("FECAPE_POS").Value
            .dblFecApeWidth = AdoRes.Fields("FECAPE_WIDTH").Value
            .bFecLimOfeVisible = SQLBinaryToBoolean(AdoRes.Fields("FECLIMOFE_VISIBLE").Value)
            .iFecLimOfePos = AdoRes.Fields("FECLIMOFE_POS").Value
            .dblFecLimOfeWidth = AdoRes.Fields("FECLIMOFE_WIDTH").Value
            .bFecPresVisible = SQLBinaryToBoolean(AdoRes.Fields("FECPRES_VISIBLE").Value)
            .iFecPresPos = AdoRes.Fields("FECPRES_POS").Value
            .dblFecPresWidth = AdoRes.Fields("FECPRES_WIDTH").Value
            .bFecCierreVisible = SQLBinaryToBoolean(AdoRes.Fields("FECCIERRE_VISIBLE").Value)
            .iFecCierrePos = AdoRes.Fields("FECCIERRE_POS").Value
            .dblFecCierreWidth = AdoRes.Fields("FECCIERRE_WIDTH").Value
            .bFecNecVisible = SQLBinaryToBoolean(AdoRes.Fields("FECNEC_VISIBLE").Value)
            .iFecNecPos = AdoRes.Fields("FECNEC_POS").Value
            .dblFecNecWidth = AdoRes.Fields("FECNEC_WIDTH").Value
            .bFecIniVisible = SQLBinaryToBoolean(AdoRes.Fields("FECINI_VISIBLE").Value)
            .iFecIniPos = AdoRes.Fields("FECINI_POS").Value
            .dblFecIniWidth = AdoRes.Fields("FECINI_WIDTH").Value
            .bFecFinVisible = SQLBinaryToBoolean(AdoRes.Fields("FECFIN_VISIBLE").Value)
            .iFecFinPos = AdoRes.Fields("FECFIN_POS").Value
            .dblFecFinWidth = AdoRes.Fields("FECFIN_WIDTH").Value
            .bPresVisible = SQLBinaryToBoolean(AdoRes.Fields("PRES_VISIBLE").Value)
            .iPresPos = AdoRes.Fields("PRES_POS").Value
            .dblPresWidth = AdoRes.Fields("PRES_WIDTH").Value
            .bConsumidoVisible = SQLBinaryToBoolean(AdoRes.Fields("CONS_VISIBLE").Value)
            .iConsumidoPos = AdoRes.Fields("CONS_POS").Value
            .dblConsumidoWidth = AdoRes.Fields("CONS_WIDTH").Value
            .bImporteVisible = SQLBinaryToBoolean(AdoRes.Fields("IMP_VISIBLE").Value)
            .iImportePos = AdoRes.Fields("IMP_POS").Value
            .dblImporteWidth = AdoRes.Fields("IMP_WIDTH").Value
            .bAhorroVisible = SQLBinaryToBoolean(AdoRes.Fields("AHORRO_VISIBLE").Value)
            .iAhorroPos = AdoRes.Fields("AHORRO_POS").Value
            .dblAhorroWidth = AdoRes.Fields("AHORRO_WIDTH").Value
            .bMonedaVisible = SQLBinaryToBoolean(AdoRes.Fields("MON_VISIBLE").Value)
            .iMonedaPos = AdoRes.Fields("MON_POS").Value
            .dblMonedaWidth = AdoRes.Fields("MON_WIDTH").Value
            .bDestVisible = SQLBinaryToBoolean(AdoRes.Fields("DEST_VISIBLE").Value)
            .iDestPos = AdoRes.Fields("DEST_POS").Value
            .dblDestWidth = AdoRes.Fields("DEST_WIDTH").Value
            .bDestDenVisible = SQLBinaryToBoolean(AdoRes.Fields("DESTDEN_VISIBLE").Value)
            .iDestDenPos = AdoRes.Fields("DESTDEN_POS").Value
            .dblDestDenWidth = AdoRes.Fields("DESTDEN_WIDTH").Value
            .bPagoVisible = SQLBinaryToBoolean(AdoRes.Fields("PAGO_VISIBLE").Value)
            .iPagoPos = AdoRes.Fields("PAGO_POS").Value
            .dblPagoWidth = AdoRes.Fields("PAGO_WIDTH").Value
            .bProvVisible = SQLBinaryToBoolean(AdoRes.Fields("PROVE_VISIBLE").Value)
            .iProvPos = AdoRes.Fields("PROVE_POS").Value
            .dblProvWidth = AdoRes.Fields("PROVE_WIDTH").Value
            .bProvDenVisible = SQLBinaryToBoolean(AdoRes.Fields("PROVEDEN_VISIBLE").Value)
            .iProvDenPos = AdoRes.Fields("PROVEDEN_POS").Value
            .dblProvDenWidth = AdoRes.Fields("PROVEDEN_WIDTH").Value
            'jpa Tarea 178
            .bgsToErpVisible = SQLBinaryToBoolean(AdoRes.Fields("GSTOERP_VISIBLE").Value)
            .igsToErpPos = AdoRes.Fields("GSTOERP_POS").Value
            .dblgsToErpWidth = AdoRes.Fields("GSTOERP_WIDTH").Value
            
            .bFecValAperturaVisible = SQLBinaryToBoolean(AdoRes.Fields("FECVALAPERTURA_VISIBLE").Value)
            .iFecValAperturaPos = AdoRes.Fields("FECVALAPERTURA_POS").Value
            .dblFecValAperturaWidth = AdoRes.Fields("FECVALAPERTURA_WIDTH").Value
            
            .bPorcentajeAhorroVisible = SQLBinaryToBoolean(AdoRes.Fields("PORCENTAJEAHORRO_VISIBLE").Value)
            .iPorcentajeAhorroPos = AdoRes.Fields("PORCENTAJEAHORRO_POS").Value
            .dblPorcentajeAhorroWidth = AdoRes.Fields("PORCENTAJEAHORRO_WIDTH").Value
            
            .bCambioVisible = SQLBinaryToBoolean(AdoRes.Fields("CAMBIO_VISIBLE").Value)
            .iCambioPos = AdoRes.Fields("CAMBIO_POS").Value
            .dblCambioWidth = AdoRes.Fields("CAMBIO_WIDTH").Value
            
            .bSolicVinculadaVisible = SQLBinaryToBoolean(AdoRes.Fields("VINCULADA_VISIBLE").Value)
            .iSolicVinculadaPos = AdoRes.Fields("VINCULADA_POS").Value
            .dblSolicVinculadaWidth = AdoRes.Fields("VINCULADA_WIDTH").Value
            
            'fin jpa Tarea 178
            .bOcultarFiltro = SQLBinaryToBoolean(AdoRes.Fields("OCULTAR_FILTRO").Value)
            .bOcultarMenu = SQLBinaryToBoolean(AdoRes.Fields("OCULTAR_MENU").Value)
            .vAnyoDesde = AdoRes.Fields("ANYO_DESDE").Value
            .vMesDesde = AdoRes.Fields("MES_DESDE").Value
            
            'Los c�digos de todos los materiales del filtro est�n en una cadena separados por caracteres.
            'El car�cter cuyo c�digo ascii es 188 separa los c�digos de un mismo GMN y el 189 los distintos GMN
            sGMN = NullToStr(AdoRes.Fields("GMN").Value)
            If sGMN <> "" Then
                Set .oGMN1 = New CGruposMatNivel1
                Set .oGMN1.Conexion = m_oConexion
                Set .oGMN2 = New CGruposMatNivel2
                Set .oGMN2.Conexion = m_oConexion
                Set .oGMN3 = New CGruposMatNivel3
                Set .oGMN3.Conexion = m_oConexion
                Set .oGMN4 = New CGruposMatNivel4
                Set .oGMN4.Conexion = m_oConexion
                
                arGMN = Split(sGMN, Chr(189))
                For i = 0 To UBound(arGMN)
                    arCod = Split(arGMN(i), Chr(188))
                    Select Case UBound(arCod)
                        Case 0
                            .oGMN1.Add Mid(arCod(0), 2, Len(arCod(0)) - 2), ""
                        Case 1
                            .oGMN2.Add Mid(arCod(0), 2, Len(arCod(0)) - 2), "", Mid(arCod(1), 2, Len(arCod(1)) - 2), ""
                        Case 2
                            .oGMN3.Add Mid(arCod(0), 2, Len(arCod(0)) - 2), Mid(arCod(1), 2, Len(arCod(1)) - 2), Mid(arCod(2), 2, Len(arCod(2)) - 2), "", "", ""
                        Case 3
                            .oGMN4.Add Mid(arCod(0), 2, Len(arCod(0)) - 2), Mid(arCod(1), 2, Len(arCod(1)) - 2), Mid(arCod(2), 2, Len(arCod(2)) - 2), _
                                        "", "", "", Mid(arCod(3), 2, Len(arCod(3)) - 2), ""
                    End Select
                Next
            End If
            
            .vCodEqp = AdoRes.Fields("EQP").Value
            .vCodCom = AdoRes.Fields("COM").Value
            .bResponsable = SQLBinaryToBoolean(AdoRes.Fields("RESPONSABLE").Value)
            .vUON1 = AdoRes.Fields("UON1").Value
            .vUON2 = AdoRes.Fields("UON2").Value
            .vUON3 = AdoRes.Fields("UON3").Value
            
            .vPres1Niv1 = AdoRes.Fields("PRES1NIV1").Value
            .vPres1Niv2 = AdoRes.Fields("PRES1NIV2").Value
            .vPres1Niv3 = AdoRes.Fields("PRES1NIV3").Value
            .vPres1Niv4 = AdoRes.Fields("PRES1NIV4").Value
            
            .vPres2Niv1 = AdoRes.Fields("PRES2NIV1").Value
            .vPres2Niv2 = AdoRes.Fields("PRES2NIV2").Value
            .vPres2Niv3 = AdoRes.Fields("PRES2NIV3").Value
            .vPres2Niv4 = AdoRes.Fields("PRES2NIV4").Value
            
            .vPres3Niv1 = AdoRes.Fields("PRES3NIV1").Value
            .vPres3Niv2 = AdoRes.Fields("PRES3NIV2").Value
            .vPres3Niv3 = AdoRes.Fields("PRES3NIV3").Value
            .vPres3Niv4 = AdoRes.Fields("PRES3NIV4").Value
            
            .vPres4Niv1 = AdoRes.Fields("PRES4NIV1").Value
            .vPres4Niv2 = AdoRes.Fields("PRES4NIV2").Value
            .vPres4Niv3 = AdoRes.Fields("PRES4NIV3").Value
            .vPres4Niv4 = AdoRes.Fields("PRES4NIV4").Value
            
            .vDenominacion = AdoRes.Fields("DEN").Value
            .bAtrBuscarVisible = SQLBinaryToBoolean(AdoRes.Fields("ATRBUSCAR_VISIBLE").Value)
        End With
        'fin jpa Tarea 178
        
        If UsarIndice Then
            lIndice = 0

            While Not AdoRes.eof
                ' Dejamos en manos del programador cuando asignar la conexion al obejto usuario
                Me.Add sUsuario, TpConfVisor, lIndice
                AdoRes.MoveNext
                lIndice = lIndice + 1
            Wend

        Else
            While Not AdoRes.eof
                ' Dejamos en manos del programador cuando asignar la conexion al obejto usuario
                Me.Add sUsuario, TpConfVisor
                AdoRes.MoveNext
            Wend
        End If
    End If
    
    AdoRes.Close
    '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    
Salir:
    Set AdoRes = Nothing
    Set oParam = Nothing
    Set oCom = Nothing
    Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CConfVistasVisor", "CargarConfVisor", ERR, Erl)
      Resume Salir
   End If
End Sub


