VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CERPsInt"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CERPsInt ****************************
'*             Creada : 19/06/07
'****************************************************************


Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum
Private mCol As Collection
Private m_oConexion As CConexion
Public m_bEOF As Boolean

Public Sub CargarTodosLosERPs()

Dim AdoRes As adodb.Recordset
Dim fldCod As adodb.Field
Dim fldDen As adodb.Field
Dim fldTipo As adodb.Field
Dim fldMapper As adodb.Field
Dim sConsulta As String
Dim lIndice As Long

'********* Precondicion **************************************
If m_oConexion Is Nothing Then
    Err.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "cERPsInt.CargarTodosLosPagos", "No se ha establecido la conexion"
    Exit Sub
End If
'*************************************************************

sConsulta = " SELECT COD, DEN, TIPO, MAPPER FROM ERP WITH (NOLOCK) ORDER BY COD "

Set AdoRes = New adodb.Recordset
AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
      
If AdoRes.eof Then
    AdoRes.Close
    Set AdoRes = Nothing
    Exit Sub
Else
    Set mCol = Nothing
    Set mCol = New Collection
    Set fldCod = AdoRes.Fields("COD")
    Set fldDen = AdoRes.Fields("DEN")
    Set fldTipo = AdoRes.Fields("TIPO")
    Set fldMapper = AdoRes.Fields("MAPPER")
        
    While Not AdoRes.eof
        Me.Add fldCod.value, fldDen.value, fldTipo.value, fldMapper.value
        AdoRes.MoveNext
    Wend
    
    AdoRes.Close
    Set AdoRes = Nothing
    Set fldCod = Nothing
    Set fldDen = Nothing
    Set fldTipo = Nothing
    Set fldMapper = Nothing
    Exit Sub
End If

End Sub
Public Function Add(ByVal Cod As Variant, ByVal Den As Variant, ByVal Tipo As Variant, ByVal Mapper As Variant) As CERPInt
'create a new object
Dim objnewmember As CERPInt
    
     Set objnewmember = New CERPInt
   
    objnewmember.Cod = Cod
    objnewmember.Den = Den
    objnewmember.Tipo = Tipo
    objnewmember.Mapper = Mapper
       
    Set objnewmember.Conexion = m_oConexion
        
    'set the properties passed into the method
    mCol.Add objnewmember, CStr(Cod)

    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing

End Function


Public Property Get Item(vntIndexKey As Variant) As CERPInt
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Friend Property Set Conexion(ByVal con As CConexion)
Set m_oConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = m_oConexion
End Property


Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
     Count = mCol.Count
End If


End Property


Public Sub Remove(vntIndexKey As Variant)
   
    mCol.Remove vntIndexKey
End Sub


Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
     Set NewEnum = mCol.[_NewEnum]
End Property


Private Sub Class_Initialize()
    Set mCol = New Collection
End Sub


Private Sub Class_Terminate()
    'destroys collection when this class is terminated
    Set mCol = Nothing
    Set m_oConexion = Nothing
End Sub

Public Property Get eof() As Boolean
    eof = m_bEOF
End Property

Public Property Let eof(ByVal vNewValue As Boolean)
    m_bEOF = vNewValue
End Property

'''
'''<summary>Obtiene el c�digo del erp asociado a una determinada organizaci�n de compras de una empresa</summary>
'''<param name="iOrgCompras">Identificador de la empresa</param>
'''<returns>C�digo del erp</returns>
Public Function obtenerErpOrgCompras(ByVal iEmpresa As Integer) As String
    Dim rst As New adodb.Recordset
    Dim strSql As String
    
    strSql = "FSGS_OBTENER_ERP_ORGCOMPRAS " & iEmpresa
    
    rst.Open strSql, m_oConexion.ADOCon, adOpenStatic, adLockOptimistic
    
    If Not rst.eof Then
        obtenerErpOrgCompras = rst(0)
    Else
        obtenerErpOrgCompras = -1
    End If
    
    rst.Close
    
    
End Function
'''
'''<summary>Obtiene el c�digo del erp asociado a una determinada empresa</summary>
'''<param name="iEmpresa">Identificador de la empresa</param>
'''<returns>C�digo del erp</returns>
Public Function obtenerErpEmpresa(ByVal iEmpresa As Integer) As String
    Dim rst As New adodb.Recordset
    Dim strSql As String
    
    strSql = "FSGS_OBTENER_ERP_EMPRESA " & iEmpresa
    
    rst.Open strSql, m_oConexion.ADOCon, adOpenStatic, adLockOptimistic
    
    If Not rst.eof Then
        obtenerErpEmpresa = rst(0)
    Else
        obtenerErpEmpresa = -1
    End If
    
    rst.Close
    
    
End Function

'---------------------------------------------------------------------------------------
' Procedure : getErps
'<revision> jim : 05/11/2014</revision>
'<summary>Obtiene los ERP's correspondientes a las empresas de las unidades organizativas en las
'que podemos utilizar un art�culo
'</summary>
'<returns>CErps</returns>
'---------------------------------------------------------------------------------------
'
Public Function getErpsArticulo(ByVal sCodArticulo As String)
    Dim adoComm As adodb.Command
    Dim adoParam As adodb.Parameter
    Dim sConsulta As String
    Dim rs As New adodb.Recordset
    
    '********* Precondicion **************************************
    If m_oConexion Is Nothing Then
        Err.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "Carticulo.getErps", "No se ha establecido la conexion"
        Exit Function
    End If
    
    'preparar borrado
          
    Set adoComm = New adodb.Command
    Set adoComm.ActiveConnection = m_oConexion.ADOCon
    
    
    sConsulta = "SELECT DISTINCT E.ERP "
    sConsulta = sConsulta & " FROM TABLAS_INTEGRACION_ERP T WITH(NOLOCK)"
    sConsulta = sConsulta & " INNER JOIN ERP_SOCIEDAD E WITH(NOLOCK) ON E.ERP=T.ERP"
    sConsulta = sConsulta & " INNER JOIN EMP WITH(NOLOCK) ON EMP.SOCIEDAD=E.SOCIEDAD"
    sConsulta = sConsulta & " INNER JOIN ("

    sConsulta = sConsulta & " SELECT DISTINCT A.UON1, A.UON2, A.UON3 ,"
    sConsulta = sConsulta & " CASE WHEN UON1.EMPRESA IS NULL THEN"
    sConsulta = sConsulta & " CASE WHEN UON2.EMPRESA IS NULL THEN"
    sConsulta = sConsulta & " CASE WHEN UON3.EMPRESA IS NULL THEN ISNULL(UON4.EMPRESA,0) ELSE UON3.EMPRESA END"
    sConsulta = sConsulta & " ELSE UON2.EMPRESA END"
    sConsulta = sConsulta & " ELSE UON1.EMPRESA END AS EMPRESA"

    sConsulta = sConsulta & " FROM ART4_UON A WITH(NOLOCK)"
    sConsulta = sConsulta & " INNER JOIN UON1 WITH(NOLOCK) ON A.UON1=UON1.COD"
    '--como m�nimo existe la relaci�n a nivel UON1"
    sConsulta = sConsulta & " LEFT JOIN UON2 WITH(NOLOCK) ON A.UON1=UON2.UON1 AND ISNULL(A.UON2,UON2.COD) = UON2.COD"
    sConsulta = sConsulta & " LEFT JOIN UON3 WITH(NOLOCK) ON A.UON1=UON3.UON1 AND ISNULL(A.UON2,UON3.UON2)=UON3.UON2 AND ISNULL(A.UON3,UON3.COD)=UON3.COD"
    sConsulta = sConsulta & " LEFT JOIN UON4 WITH(NOLOCK) ON A.UON1=UON4.UON1 AND ISNULL(A.UON2,UON4.UON2)=UON4.UON2 AND ISNULL(A.UON3,UON4.UON3)=UON4.UON3"
    sConsulta = sConsulta & " WHERE ART4=?"
    sConsulta = sConsulta & " and UON1.BAJALOG=0 and ISNULL(UON2.BAJALOG,0)=0 and ISNULL(UON3.BAJALOG,0)=0 and ISNULL(UON4.BAJALOG,0)=0"

    sConsulta = sConsulta & " )"
    sConsulta = sConsulta & " ART_EMP ON ART_EMP.EMPRESA = EMP.ID"
    sConsulta = sConsulta & " WHERE EMP.ERP=1 AND T.ACTIVA=1 AND T.TABLA=7 AND T.SENTIDO IN (1,3)"
    
    Set adoParam = adoComm.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(sCodArticulo) + 1, value:=sCodArticulo)
    adoComm.Parameters.Append adoParam
    adoComm.CommandText = sConsulta
    adoComm.CommandType = adCmdText
    adoComm.Prepared = True
    Set rs = adoComm.Execute
    
    While Not rs.eof
        Me.Add rs("ERP").value, "", 0, Null
        rs.MoveNext
    Wend

    Set adoComm = Nothing
End Function


