VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CPago"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
''' *** Clase: CPago
''' *** Creacion: 9/09/1998 (Javier Arana)
''' *** Ultima revision: 18/01/1999 (Alfredo Magallon)

Option Explicit

Implements IBaseDatos

''' Variables privadas con la informacion de una forma de Pago

Private m_sCod As String
Private m_oDen As CMultiidiomas
Private m_vEstIntegracion As Variant
Private m_vFecAct As Variant

''' Conexion

Private m_oConexion As CConexion

' Variables para el control de cambios del Log e Integraci�n
Private m_sUsuario As String             'Usuario que realiza los cambios.
Private m_udtOrigen As OrigenIntegracion 'Origen del mvto en la tabla de LOG


''' Control de errores

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

''' Recordset para el Pago a editar
Private m_adores As adodb.Recordset

''' Indice de la Pago en la coleccion

Private m_lIndice As Long
Public Property Let FecAct(ByVal vData As Variant)

    ''' * Objetivo: Dar valor a la variable privada FECACT
    ''' * Recibe: Fecha de ultima actualizacion de la forma de pago
    ''' * Devuelve: Nada

    m_vFecAct = vData
    
End Property
Public Property Get FecAct() As Variant

    ''' * Objetivo: Devolver la variable privada FECACT
    ''' * Recibe: Nada
    ''' * Devuelve: Fecha de ultima actualizacion de la forma de pago

    FecAct = m_vFecAct
    
End Property
Public Property Let Usuario(ByVal vData As String)

    ''' * Objetivo: Dar valor a la variable privada Usuario
    ''' * Recibe: Codigo del usuario conectado
    ''' * Devuelve: Nada

    m_sUsuario = vData
    
End Property
Public Property Let EstadoIntegracion(ByVal vData As Variant)

    ''' * Objetivo: Dar valor al estado de integraci�n de la moneda
    ''' * Recibe: el valor
    ''' * Devuelve: Nada

    m_vEstIntegracion = vData
    
End Property
Public Property Get EstadoIntegracion() As Variant

    ''' * Objetivo: Devolver el estado de integraci�n de la moneda
    ''' * Recibe: Nada
    ''' * Devuelve: El estado de integraci�n de la moneda

    EstadoIntegracion = m_vEstIntegracion
    
End Property
Public Property Get Indice() As Integer

    ''' * Objetivo: Devolver el indice de la Pago en la coleccion
    ''' * Recibe: Nada
    ''' * Devuelve: Indice de la Pago en la coleccion

    Indice = m_lIndice
    
End Property
Public Property Let Indice(ByVal iInd As Integer)

    ''' * Objetivo: Dar valor al indice de la Pago en la coleccion
    ''' * Recibe: Indice de la Pago en la coleccion
    ''' * Devuelve: Nada

    m_lIndice = iInd
    
End Property
Friend Property Set Conexion(ByVal vData As CConexion)

    ''' * Objetivo: Dar valor a la conexion de la Pago
    ''' * Recibe: Conexion
    ''' * Devuelve: Nada

    Set m_oConexion = vData
    
End Property
Friend Property Get Conexion() As CConexion

    ''' * Objetivo: Devolver la conexion de la Pago
    ''' * Recibe: Nada
    ''' * Devuelve: Conexion

    Set Conexion = m_oConexion
    
End Property

Public Property Get Denominaciones() As CMultiidiomas

    ''' * Objetivo: Devolver la denominacion
    ''' * Recibe: Nada
    ''' * Devuelve: Conexion
    Set Denominaciones = m_oDen
    
End Property

Public Property Set Denominaciones(ByVal dato As CMultiidiomas)

    ''' * Objetivo: Dar valor al objeto Denominaciones
    ''' * Recibe: Codigo de la Unidad
    ''' * Devuelve: Nada
    Set m_oDen = dato
    
End Property

Public Property Let Cod(ByVal vData As String)

    ''' * Objetivo: Dar valor a la variable privada COD
    ''' * Recibe: Codigo de la Pago
    ''' * Devuelve: Nada

    m_sCod = vData
    
End Property
Public Property Get Cod() As String

    ''' * Objetivo: Devolver la variable privada COD
    ''' * Recibe: Nada
    ''' * Devuelve: Codigo de la Pago

    Cod = m_sCod
    
End Property

Private Sub Class_Terminate()

    ''' * Objetivo: Limpiar la memoria
    
    Set m_oDen = Nothing
    
    Set m_oConexion = Nothing
            
End Sub
''' <summary>Esta funci�n a�ade la forma de pago de la tabla </summary>
''' <returns>TipoErrorSummit con el error si lo hay</returns>
''' <remarks>Llamada desde: FSGClient
''' Tiempo m�ximo: 1 sec </remarks>
''' <revision>JVS 23/12/2011</revision>

Private Function IBaseDatos_AnyadirABaseDatos() As TipoErrorSummit

 '******************************************
'***** M U Y   I M P O R T A N T E     ****
'*****                                 ****
'***** SI SE REALIZAN CAMBIOS          ****
'***** DESPUES DEL 21/06/2002          ****
'***** MIRAR SI HAY QUE PASARLOS A     ****
'***** FSGSINTEGRATION.DLL             ****
'******************************************
   
    Dim sConsulta As String
    Dim TESError As TipoErrorSummit
    Dim bTransaccionEnCurso As Boolean
    Dim AdoRes As adodb.Recordset
    Dim oMulti As CMultiidioma
    Dim lIdLogPAG As Long
    
    ''' Precondicion
    
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CPago.AnyadirABAseDatos", "No se ha establecido la conexion"
        Exit Function
    End If
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    TESError.NumError = TESnoerror
    
On Error GoTo Error_Cls:
                   
    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
    bTransaccionEnCurso = True
    
    ''' Ejecutar insercion
    sConsulta = "INSERT INTO PAG (COD) VALUES (N'" & DblQuote(m_sCod) & "')"
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then
        GoTo Error_Cls
    End If
    
    'Inserta las denominaciones en PAG_DEN
    If Not m_oDen Is Nothing Then
        For Each oMulti In m_oDen
            If Not IsEmpty(oMulti.Den) Then
                sConsulta = "INSERT INTO PAG_DEN (PAG,DEN,IDIOMA) VALUES (N'" & DblQuote(m_sCod) & "',"
                sConsulta = sConsulta & StrToSQLNULLNVar(oMulti.Den) & ",'" & DblQuote(oMulti.Cod) & "')"
                m_oConexion.ADOCon.Execute sConsulta
                If m_oConexion.ADOCon.Errors.Count > 0 Then
                    GoTo Error_Cls
                End If
            End If
        Next
    End If
    
     ''' Recuperar la fecha de insercion
    'CALIDAD Se obtiene la Fecha de Actualizaci�n con bloqueo para recuperar la verdadera �ltima fecha de actualizaci�n
    sConsulta = "SELECT FECACT FROM PAG WHERE COD ='" & DblQuote(m_sCod) & "'"
    Set AdoRes = New adodb.Recordset
    AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    m_vFecAct = AdoRes("FECACT").Value
    AdoRes.Close

   
    ' Tratamiento para el LOG de cambios y la Integraci�n
    If GrabarEnLog Then
        ''10/09/2007(ngo):MultiERP
        m_vEstIntegracion = 0 'EstadoIntegracion.PendienteDeTratar
        sConsulta = "SELECT COD FROM ERP WITH(NOLOCK)"
        AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
        While Not AdoRes.eof
            sConsulta = "INSERT INTO LOG_PAG (ACCION, COD, ORIGEN, USU, ERP) VALUES ('" & Accion_Alta & "',N'" & DblQuote(m_sCod) & "'"
            sConsulta = sConsulta & "," & m_udtOrigen & ",'" & DblQuote(m_sUsuario) & "','" & DblQuote(AdoRes.Fields("COD").Value) & "')"
            m_oConexion.ADOCon.Execute sConsulta
            If m_oConexion.ADOCon.Errors.Count > 0 Then
                GoTo Error_Cls
            End If
            
            Dim ador As adodb.Recordset
            Set ador = New adodb.Recordset
            'CALIDAD Sin WITH(NOLOCK) en el FROM para asegurar la recuperaci�n del mayor valor de la tabla
            ador.Open "SELECT SCOPE_IDENTITY()", m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
            If Not ador.eof Then
                lIdLogPAG = ador.Collect(0)
            End If
            ador.Close
            Set ador = Nothing
            
            If Not m_oDen Is Nothing Then
                For Each oMulti In m_oDen
                    sConsulta = "INSERT INTO LOG_PAG_DEN (ID_LOG_PAG,DEN,IDI) VALUES (" & lIdLogPAG & ","
                    sConsulta = sConsulta & StrToSQLNULLNVar(oMulti.Den) & ",'" & DblQuote(oMulti.Cod) & "')"
                    m_oConexion.ADOCon.Execute sConsulta
                    If m_oConexion.ADOCon.Errors.Count > 0 Then
                        GoTo Error_Cls
                    End If
                Next
            End If
            
            AdoRes.MoveNext
        Wend
    End If
    
    Set AdoRes = Nothing
    
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    IBaseDatos_AnyadirABaseDatos = TESError
    
    Exit Function
    
Error_Cls:
     
    IBaseDatos_AnyadirABaseDatos = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    
     If bTransaccionEnCurso Then
        m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
        bTransaccionEnCurso = False
    End If
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        'Call g_oErrores.TratarError("Class Module", "CPago", "IBaseDatos_AnyadirABaseDatos", ERR, Erl)
        GoTo Error_Cls
        Exit Function
    End If
           
End Function
''' <summary>Esta funci�n cambia de codigo la FORMA DE PAGO </summary>
''' <param name="CodigoNuevo">Codigo nuevo</param>
''' <returns>TipoErrorSummit con el error si lo hay</returns>
''' <remarks>Llamada desde: FSGClient
''' Tiempo m�ximo: 1 sec </remarks>
''' <revision>JVS 23/12/2011</revision>
Private Function IBaseDatos_CambiarCodigo(CodigoNuevo As Variant) As TipoErrorSummit

'******************************************
'***** M U Y   I M P O R T A N T E     ****
'*****                                 ****
'***** SI SE REALIZAN CAMBIOS          ****
'***** DESPUES DEL 21/06/2002          ****
'***** MIRAR SI HAY QUE PASARLOS A     ****
'***** FSGSINTEGRATION.DLL             ****
'******************************************
  
    'ado  Dim q As rdoQuery, sql As String
    Dim cm As adodb.Command
    Dim par As adodb.Parameter
    Dim sConsulta As String
    Dim bLog As Boolean
    Dim ContRegActualizados As Long
    Dim oMulti As CMultiidioma
    Dim lIdLogPAG As Long
    Dim TESError As TipoErrorSummit
    
    ''' Precondicion
    
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CPago.CambiarCodigo", "No se ha establecido la conexion"
        Exit Function
    End If
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    TESError.NumError = TESnoerror
    
        
If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
    
    ''' Preparar la SP y sus parametros
    Set cm = New adodb.Command
    Set cm.ActiveConnection = m_oConexion.ADOCon
   
    Set par = cm.CreateParameter("PAR1", adVarChar, adParamInput, 50, Me.Cod)
    cm.Parameters.Append par
    Set par = cm.CreateParameter("PAR2", adVarChar, adParamInput, 50, CodigoNuevo)
    cm.Parameters.Append par
    
    cm.CommandType = adCmdStoredProc
    cm.CommandText = "PAG_COD"
    
    ''' Ejecutar la SP

    cm.Execute ContRegActualizados
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    ''' Tratamiento para el LOG de cambios y la Integraci�n
    bLog = True
    If GrabarEnLog And ContRegActualizados <> -1 Then

        ''10/09/2007(ngo):MultiERP
        Dim AdoRes As adodb.Recordset
        Set AdoRes = New adodb.Recordset
        sConsulta = "SELECT COD FROM ERP WITH(NOLOCK)"
        AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
        While Not AdoRes.eof
            sConsulta = "INSERT INTO LOG_PAG (ACCION, COD, COD_NEW, ORIGEN, USU, ERP) VALUES ('" & Accion_CambioCodigo & "',N'" & DblQuote(m_sCod) & "'"
            sConsulta = sConsulta & ",N'" & DblQuote(CodigoNuevo) & "'," & m_udtOrigen & ",'" & DblQuote(m_sUsuario) & "','" & DblQuote(AdoRes.Fields("COD").Value) & "')"
            m_oConexion.ADOCon.Execute sConsulta
            If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
            
            Dim ador As adodb.Recordset
            Set ador = New adodb.Recordset
            'CALIDAD Sin WITH(NOLOCK) en el FROM para asegurar la recuperaci�n del mayor valor de la tabla
            ador.Open "SELECT SCOPE_IDENTITY()", m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
            If Not ador.eof Then
                lIdLogPAG = ador.Collect(0)
            End If
            ador.Close
            Set ador = Nothing
            
            If Not m_oDen Is Nothing Then
                For Each oMulti In m_oDen
                    sConsulta = "INSERT INTO LOG_PAG_DEN (ID_LOG_PAG,DEN,IDI) VALUES (" & lIdLogPAG & ","
                    sConsulta = sConsulta & StrToSQLNULLNVar(oMulti.Den) & "," & StrToSQLNULLNVar(oMulti.Cod) & ")"
                    m_oConexion.ADOCon.Execute sConsulta
                    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
                Next
            End If
            
            m_vEstIntegracion = 0 'EstadoIntegracion.PendienteDeTratar
            AdoRes.MoveNext
        Wend
        AdoRes.Close
        Set AdoRes = Nothing
    End If
    bLog = False

    'ado  Set q = Nothing
    Set cm = Nothing
    
    m_sCod = CodigoNuevo
    
    'Recuperamos el valor de FecAct
    Set m_adores = New adodb.Recordset
    'CALIDAD Se obtiene la Fecha de Actualizaci�n con bloqueo para recuperar la verdadera �ltima fecha de actualizaci�n
    sConsulta = "SELECT FECACT FROM PAG WHERE COD='" & DblQuote(CodigoNuevo) & "'"
    m_adores.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
    m_vFecAct = m_adores("FECACT").Value
    m_adores.Close
    Set m_adores = Nothing
        
    IBaseDatos_CambiarCodigo = TESError
    
    Exit Function
    
Error_Cls:
    If bLog Then 'Si el error se ha producido al grabar en la tabla de LOG, deshacemos los cambios a mano
        Set cm = Nothing
        Set cm = New adodb.Command
        Set cm.ActiveConnection = m_oConexion.ADOCon
        Set par = cm.CreateParameter("PAR1", adVarChar, adParamInput, 50, CodigoNuevo)
        cm.Parameters.Append par
        Set par = cm.CreateParameter("PAR2", adVarChar, adParamInput, 50, m_sCod)
        cm.Parameters.Append par
        cm.CommandType = adCmdStoredProc
        cm.CommandText = "PAG_COD"
        cm.Execute
        Set m_adores = New adodb.Recordset
        'CALIDAD Se obtiene la Fecha de Actualizaci�n con bloqueo para recuperar la verdadera �ltima fecha de actualizaci�n
        sConsulta = "SELECT FECACT FROM PAG WHERE COD='" & DblQuote(m_sCod) & "'"
        m_adores.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
        m_vFecAct = m_adores("FECACT").Value
        m_adores.Close
        Set m_adores = Nothing
    End If
     
    If Not cm Is Nothing Then
          Set cm = Nothing
    End If
    
    IBaseDatos_CambiarCodigo = basErrores.TratarError(m_oConexion.ADOCon.Errors)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        'Call g_oErrores.TratarError("Class Module", "CPago", "IBaseDatos_CambiarCodigo", ERR, Erl)
        GoTo Error_Cls
        Exit Function
    End If
    
End Function
Private Sub IBaseDatos_CancelarEdicion()

    ''' * Objetivo: Cancelar la edicion, cerrando el Resultset
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not m_adores Is Nothing Then
        m_adores.Close
        Set m_adores = Nothing
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CPago", "IBaseDatos_CancelarEdicion", ERR, Erl)
        Exit Sub
    End If

End Sub
Private Function IBaseDatos_ComprobarExistenciaEnBaseDatos() As Boolean

    ''' ! No usado por ahora
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CPago", "IBaseDatos_ComprobarExistenciaEnBaseDatos", ERR, Erl)
        Exit Function
    End If
End Function

''' <summary>Esta funci�n Eliminar el Pago de la tabla </summary>
''' <returns>TipoErrorSummit con el error si lo hay</returns>
''' <remarks>Llamada desde: FSGClient
''' Tiempo m�ximo: 1 sec </remarks>
''' <revision>JVS 23/12/2011</revision>
Private Function IBaseDatos_EliminarDeBaseDatos() As TipoErrorSummit

    Dim TESError As TipoErrorSummit
    Dim sConsulta As String
    Dim bTransaccionEnCurso As Boolean
    Dim ContRegBorrados As Long
    Dim oMulti As CMultiidioma
    Dim lIdLogPAG As Long
    
  '******************************************
'***** M U Y   I M P O R T A N T E     ****
'*****                                 ****
'***** SI SE REALIZAN CAMBIOS          ****
'***** DESPUES DEL 21/06/2002          ****
'***** MIRAR SI HAY QUE PASARLOS A     ****
'***** FSGSINTEGRATION.DLL             ****
'******************************************
  
    
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CPago.EliminarDeBaseDatos", "No se ha establecido la conexion"
        Exit Function
    End If
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    TESError.NumError = TESnoerror
    
    ''' Precondicion
If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
        
    ''' Borrado
    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
    bTransaccionEnCurso = True

    sConsulta = "UPDATE FORM_CAMPO SET VALOR_TEXT=NULL WHERE VALOR_TEXT='" & DblQuote(m_sCod) & "' AND TIPO_CAMPO_GS=" & TipoCampoGS.FormaPago
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    sConsulta = "UPDATE LINEA_DESGLOSE SET VALOR_TEXT=NULL WHERE VALOR_TEXT='" & DblQuote(m_sCod) & "' AND CAMPO_HIJO IN (SELECT ID FROM FORM_CAMPO WITH(NOLOCK) WHERE ES_SUBCAMPO=1 AND TIPO_CAMPO_GS=" & TipoCampoGS.FormaPago & ")"
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    ''MultiERP: Elimina de PAG_ERP
    m_oConexion.ADOCon.Execute "DELETE FROM PAG_ERP WHERE COD_GS=N'" & DblQuote(m_sCod) & "'"
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    m_oConexion.ADOCon.Execute "DELETE FROM PAG_DEN WHERE PAG=N'" & DblQuote(m_sCod) & "'"
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    m_oConexion.ADOCon.Execute "DELETE FROM PAG WHERE COD=N'" & DblQuote(m_sCod) & "'", ContRegBorrados
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls

       ''' Tratamiento para el LOG de cambios y la Integraci�n
    If GrabarEnLog And ContRegBorrados > 0 Then

        ''10/09/2007(ngo):MultiERP
        Dim AdoRes As adodb.Recordset
        Set AdoRes = New adodb.Recordset
        sConsulta = "SELECT COD FROM ERP WITH(NOLOCK)"
        AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
        While Not AdoRes.eof
            sConsulta = "INSERT INTO LOG_PAG (ACCION, COD, ORIGEN, USU, ERP) VALUES ('" & Accion_Baja & "',N'" & DblQuote(m_sCod) & "'"
            sConsulta = sConsulta & "," & m_udtOrigen & ",'" & DblQuote(m_sUsuario) & "','" & DblQuote(AdoRes.Fields("COD").Value) & "')"
            m_oConexion.ADOCon.Execute sConsulta
            If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
            
            Dim ador As adodb.Recordset
            Set ador = New adodb.Recordset
            'CALIDAD Sin WITH(NOLOCK) en el FROM para asegurar la recuperaci�n del mayor valor de la tabla
            ador.Open "SELECT SCOPE_IDENTITY()", m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
            If Not ador.eof Then
                lIdLogPAG = ador.Collect(0)
            End If
            ador.Close
            Set ador = Nothing
            
            If Not m_oDen Is Nothing Then
                For Each oMulti In m_oDen
                    sConsulta = "INSERT INTO LOG_PAG_DEN (ID_LOG_PAG,DEN,IDI) VALUES (" & lIdLogPAG & ","
                    sConsulta = sConsulta & StrToSQLNULLNVar(oMulti.Den) & "," & StrToSQLNULLNVar(oMulti.Cod) & ")"
                    m_oConexion.ADOCon.Execute sConsulta
                    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
                Next
            End If
            
            AdoRes.MoveNext
        Wend
        AdoRes.Close
        Set AdoRes = Nothing
    End If
        
     ''' Terminar transaccion
    
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
     
    IBaseDatos_EliminarDeBaseDatos = TESError
    
    Exit Function
        
Error_Cls:
        
    IBaseDatos_EliminarDeBaseDatos = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    
    If bTransaccionEnCurso Then
        m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
        bTransaccionEnCurso = False
    End If
   
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        'Call g_oErrores.TratarError("Class Module", "CPago", "IBaseDatos_EliminarDeBaseDatos", ERR, Erl)
        GoTo Error_Cls
        Exit Function
    End If
    
End Function

Private Function IBaseDatos_FinalizarEdicionEliminando() As TipoErrorSummit

    ''' ! No usado por ahora
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CPago", "IBaseDatos_FinalizarEdicionEliminando", ERR, Erl)
        Exit Function
    End If
End Function
''' <summary>Esta funci�n finaliza la edici�n del resulset modificando</summary>
''' <returns>TipoErrorSummit con el error si lo hay</returns>
''' <remarks>Llamada desde: FSGClient
''' Tiempo m�ximo: 1 sec </remarks>
''' <revision>JVS 23/12/2011</revision>
Private Function IBaseDatos_FinalizarEdicionModificando() As TipoErrorSummit

'******************************************
'***** M U Y   I M P O R T A N T E     ****
'*****                                 ****
'***** SI SE REALIZAN CAMBIOS          ****
'***** DESPUES DEL 21/06/2002          ****
'***** MIRAR SI HAY QUE PASARLOS A     ****
'***** FSGSINTEGRATION.DLL             ****
'******************************************

    Dim TESError As TipoErrorSummit
    Dim sConsulta As String
    Dim bTransaccionEnCurso As Boolean
    Dim oMulti As CMultiidioma
    Dim sCod As String
    Dim lIdLogPAG As Long
    
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CPago.FinalizarEdicionModificando", "No se ha establecido la conexion"
        Exit Function
    End If


If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    TESError.NumError = TESnoerror

    ''' Precondicion
If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:

    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
    bTransaccionEnCurso = True
    
    'Comprobamos si ha habido cambios en otra sesi�n
    Set m_adores = New adodb.Recordset
    'CALIDAD Se obtiene la Fecha de Actualizaci�n con bloqueo para recuperar la verdadera �ltima fecha de actualizaci�n
    sConsulta = "SELECT FECACT FROM PAG WHERE COD=N'" & DblQuote(m_sCod) & "'"
    m_adores.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
    
    If m_adores.eof Then
        m_adores.Close
        Set m_adores = Nothing
        TESError.NumError = TESDatoEliminado
        TESError.Arg1 = 16  '"Pago"
        IBaseDatos_FinalizarEdicionModificando = TESError
        m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
        Exit Function
    End If
    
    If m_vFecAct <> m_adores("FECACT").Value Then
        m_adores.Close
        Set m_adores = Nothing
        TESError.NumError = TESInfActualModificada
        m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
        IBaseDatos_FinalizarEdicionModificando = TESError
        Exit Function
    End If
    
    ''' Actualizar
    For Each oMulti In m_oDen
        sConsulta = "UPDATE PAG_DEN SET DEN= " & StrToSQLNULLNVar(oMulti.Den) & _
            " WHERE PAG=N'" & DblQuote(m_sCod) & "'" & _
            " AND IDIOMA = '" & DblQuote(oMulti.Cod) & "'"
        
        m_oConexion.ADOCon.Execute sConsulta
        If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    Next

 
    'Recogemos el nuevo valor de FECACT
    m_adores.Requery
    m_vFecAct = m_adores("FECACT").Value
    m_adores.Close
 
      ''' Tratamiento para el LOG de cambios y la Integraci�n
    If GrabarEnLog Then
        If Not m_oDen Is Nothing Then
            For Each oMulti In m_oDen
                sCod = sCod & "," & "DEN_" & oMulti.Cod
            Next
        End If
        ''10/09/2007(ngo):MultiERP
        sConsulta = "SELECT COD FROM ERP WITH(NOLOCK)"
        m_adores.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
        While Not m_adores.eof
            sConsulta = "INSERT INTO LOG_PAG (ACCION, COD, ORIGEN, USU, ERP) VALUES ('" & Accion_Modificacion & "',N'" & DblQuote(m_sCod) & "'"
            sConsulta = sConsulta & " ," & m_udtOrigen & ",'" & DblQuote(m_sUsuario) & "','" & DblQuote(m_adores.Fields("COD").Value) & "')"
            m_oConexion.ADOCon.Execute sConsulta
            If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
            
            Dim ador As adodb.Recordset
            Set ador = New adodb.Recordset
            'CALIDAD Sin WITH(NOLOCK) en el FROM para asegurar la recuperaci�n del mayor valor de la tabla
            ador.Open "SELECT SCOPE_IDENTITY()", m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
            If Not ador.eof Then
                lIdLogPAG = ador.Collect(0)
            End If
            ador.Close
            Set ador = Nothing
            
            If Not m_oDen Is Nothing Then
                For Each oMulti In m_oDen
                    sConsulta = "INSERT INTO LOG_PAG_DEN (ID_LOG_PAG,DEN,IDI) VALUES (" & lIdLogPAG & ","
                    sConsulta = sConsulta & StrToSQLNULLNVar(oMulti.Den) & "," & StrToSQLNULLNVar(oMulti.Cod) & ")"
                    m_oConexion.ADOCon.Execute sConsulta
                    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
                Next
            End If
            m_vEstIntegracion = 0 'EstadoIntegracion.PendienteDeTratar
            m_adores.MoveNext
        Wend
        m_adores.Close
    End If
    
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    Set m_adores = Nothing
    IBaseDatos_FinalizarEdicionModificando = TESError
    
    Exit Function
    
Error_Cls:
        
    IBaseDatos_FinalizarEdicionModificando = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    
   If Not m_adores Is Nothing Then
        m_adores.Close
        Set m_adores = Nothing
    End If
    
    If bTransaccionEnCurso Then
        m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
        bTransaccionEnCurso = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        'Call g_oErrores.TratarError("Class Module", "CPago", "IBaseDatos_FinalizarEdicionModificando", ERR, Erl)
        GoTo Error_Cls
        Exit Function
    End If
   
End Function
''' <summary>Esta funci�n finaliza la edici�n del resulset modificando</summary>
'''<param optional name="Bloquear"></param>
'''<param optional name="UsuarioBloqueo"></param>
''' <returns>TipoErrorSummit con el error si lo hay</returns>
''' <remarks>Llamada desde: FSGClient
''' Tiempo m�ximo: 1 sec </remarks>
''' <revision>JVS 23/12/2011</revision>
Private Function IBaseDatos_IniciarEdicion(Optional ByVal Bloquear As Boolean, Optional ByVal UsuarioBloqueo As String) As TipoErrorSummit

    ''' * Objetivo: Abrir la edicion en la Pago actual
    ''' * Objetivo: creando un Recordset para ello
    ''' * Recibe: Nada
    ''' * Devuelve: TipoErrorSummit con el error si lo hay

    Dim TESError As TipoErrorSummit
    Dim sConsulta As String
    Dim i As Long
    ''' Precondicion
    
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CPago.IniciarEdicion", "No se ha establecido la conexion"
        Exit Function
    End If
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    TESError.NumError = TESnoerror
    
    
     ''' Abrir Recordset
     'CALIDAD Se obtiene la Fecha de Actualizaci�n con bloqueo para recuperar la verdadera �ltima fecha de actualizaci�n
    sConsulta = "SELECT COD,FECACT FROM PAG WHERE COD='" & DblQuote(m_sCod) & "'"
    Set m_adores = New adodb.Recordset
    m_adores.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly

    ''' Si no esta la Pago, alguien la ha eliminado
    
    If m_adores.eof Then
        m_adores.Close
        Set m_adores = Nothing
        TESError.NumError = TESDatoEliminado
        TESError.Arg1 = 16  '"Pago"
        IBaseDatos_IniciarEdicion = TESError
        Exit Function
    End If
    
    If m_vFecAct <> m_adores("FECACT").Value Then
        m_adores.Close
        Set m_adores = Nothing
        TESError.NumError = TESInfActualModificada
        IBaseDatos_IniciarEdicion = TESError
        Exit Function
    End If
    

    Set m_oDen = New CMultiidiomas
    For i = 0 To m_adores.Fields.Count - 1
        If Left(m_adores.Fields(i).Name, 4) = "DEN_" Then
            m_oDen.Add Right(m_adores.Fields(i).Name, Len(m_adores.Fields(i).Name) - 4), m_adores.Fields(i).Value
        End If
    Next i

        
    IBaseDatos_IniciarEdicion = TESError
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CPago", "IBaseDatos_IniciarEdicion", ERR, Erl)
        Exit Function
    End If

End Function
Private Function IBaseDatos_ModificarEnBaseDatos() As Boolean

    ''' ! No usado por ahora
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CPago", "IBaseDatos_ModificarEnBaseDatos", ERR, Erl)
        Exit Function
    End If
End Function
Private Function GrabarEnLog() As Boolean
'***********************************************************************************
'*** Descripci�n: Nos indica si hay que grabar o no el cambio en el log de       ***
'***              monedas (tabla LOG_PAG) y carga en la variable                 ***
'***              del m�dulo m_udtOrigen el origen del movimiento                ***
'*** Parametros:  Ninguno                                                        ***
'*** Devuelve:    True (hay que grabar) False (No hay que grabar)                ***
'***********************************************************************************

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If gParametrosGenerales.gbACTIVLOG Or gParametrosIntegracion.gaExportar(EntidadIntegracion.Pag) Then
        If gParametrosGenerales.gbACTIVLOG And gParametrosIntegracion.gaExportar(EntidadIntegracion.Pag) Then
            m_udtOrigen = FSGSIntReg
        Else
            If gParametrosGenerales.gbACTIVLOG Then
                m_udtOrigen = FSGSReg
            Else
                m_udtOrigen = FSGSInt
            End If
        End If
        GrabarEnLog = True
    Else
        GrabarEnLog = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CPago", "GrabarEnLog", ERR, Erl)
        Exit Function
    End If

End Function





