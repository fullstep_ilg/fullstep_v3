VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CReuniones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
    Option Explicit

''' *** Clase: CReuniones
''' *** Creacion: 11/05/1999 (Javier Arana)
''' *** Ultima revision:

''' Variables que almacenan las propiedades de los objetos

Private mCol As Collection
Private mvarEOF As Boolean
Private mvarConexion As CConexion
Private mvarConexionServer As CConexionDeUseServer
Private mvarNumProce As Long
''' Control de errores
Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum
Public Function Add(ByVal Fecha As Date, Optional ByVal oProcesos As CProcesos, Optional ByVal Ref As Variant, Optional varIndice As Variant, Optional ByVal NumProce As Long) As CReunion
    
    Dim objnewmember As CReunion
    
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New CReunion
   
    ''' Paso de los parametros al nuevo objeto Unidad
    
    objnewmember.Fecha = Fecha
    Set objnewmember.Conexion = mvarConexion
    Set objnewmember.ConexionServer = mvarConexionServer
    
    If Not IsMissing(oProcesos) Then
        Set objnewmember.Procesos = oProcesos
    End If
    
    If IsMissing(Ref) Then
        objnewmember.Referencia = Null
    Else
        objnewmember.Referencia = Ref
    End If
    
    If Not IsMissing(varIndice) And Not IsNull(varIndice) Then
        objnewmember.Indice = varIndice
        mCol.Add objnewmember, CStr(varIndice)
    Else
        mCol.Add objnewmember, CStr(Fecha)
    End If
    
    objnewmember.NumProce = NumProce
    Set Add = objnewmember
    
    Set objnewmember = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CReuniones", "Add", ERR, Erl)
      Exit Function
   End If

End Function
Public Property Get Item(vntIndexKey As Variant) As CReunion

    ''' * Objetivo: Recuperar una Unidad de la coleccion
    ''' * Recibe: Indice de la Unidad a recuperar
    ''' * Devuelve: Unidad correspondiente
        
On Error GoTo NoSeEncuentra:

    ''' Devolvemos el item de la coleccion privada
    
    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:

    Set Item = Nothing
    
End Property
Public Property Get eof() As Boolean

    ''' * Objetivo: Devolver variable privada EOF
    ''' * Recibe: Nada
    ''' * Devuelve: Variable privada EOF
    
    eof = mvarEOF
    
End Property
Friend Property Set Conexion(ByVal con As CConexion)

    ''' * Objetivo: Dar valor a la variable privada Conexion
    ''' * Recibe: Conexion
    ''' * Devuelve: Nada
    
    Set mvarConexion = con
    
End Property

Friend Property Get Conexion() As CConexion

    ''' * Objetivo: Devolver variable privada Conexion
    ''' * Recibe: Nada
    ''' * Devuelve: Conexion
    
    Set Conexion = mvarConexion
    
End Property
Friend Property Get ConexionServer() As CConexionDeUseServer
Set ConexionServer = mvarConexionServer
End Property

Friend Property Set ConexionServer(ByVal con As CConexionDeUseServer)
Set mvarConexionServer = con
End Property
Private Sub ConectarDeUseServer()

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If mvarConexionServer.ADOSummitCon Is Nothing Then
        Set mvarConexionServer.ADOSummitCon = New adodb.Connection
        mvarConexionServer.ADOSummitCon.Open "Provider=SQLOLEDB.1;Auto Translate=FALSE;Server=" & basParametros.ServidorGS & ";Database=" & basParametros.BaseDatosGS & ";", LICDBLogin, LICDBContra
        mvarConexionServer.ADOSummitCon.CursorLocation = adUseServer
        mvarConexionServer.ADOSummitCon.CommandTimeout = 120
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CReuniones", "ConectarDeUseServer", ERR, Erl)
      Exit Sub
   End If
End Sub

Public Property Get Count() As Long

    ''' * Objetivo: Devolver variable privada Count
    ''' * Recibe: Nada
    ''' * Devuelve: Numero de elementos de la coleccion

    If mCol Is Nothing Then
        Count = 0
    Else
        Count = mCol.Count
    End If

End Property
Public Sub Remove(vntIndexKey As Variant)

    ''' * Objetivo: Eliminar una Unidad de la coleccion
    ''' * Recibe: Indice de la Unidad a eliminar
    ''' * Devuelve: Nada
   
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    mCol.Remove vntIndexKey
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CReuniones", "Remove", ERR, Erl)
      Exit Sub
   End If
    
End Sub
Private Sub Class_Initialize()

    ''' * Objetivo: Crear la coleccion
    
   

    Set mCol = New Collection
    
End Sub
Private Sub Class_Terminate()

    ''' * Objetivo: Limpiar memoria
On Error Resume Next

    Set mCol = Nothing
    Set mvarConexion = Nothing
    Set mvarConexionServer = Nothing
        
    
End Sub
Public Function DevolverLosCodigos() As TipoDatosCombo
   
    ''' ! Pendiente de revision, segun tema de combos
    
   Dim Codigos As TipoDatosCombo
   Dim iCont As Integer
   
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
   ReDim Codigos.Cod(mCol.Count)
   ReDim Codigos.Den(mCol.Count)
   
    For iCont = 0 To mCol.Count - 1
        
        Codigos.Cod(iCont) = mCol(iCont + 1).Cod
        Codigos.Den(iCont) = mCol(iCont + 1).Den
    
    Next

   DevolverLosCodigos = Codigos
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CReuniones", "DevolverLosCodigos", ERR, Erl)
      Exit Function
   End If
   
End Function
Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"

    ''' ! Pendiente de revision, objetivo desconocido
    
    Set NewEnum = mCol.[_NewEnum]

End Property

'Public Sub EstablecerTiempoDeBloqueo(Optional ByVal varMilisegundos As Variant)
'
'    If IsMissing(varMilisegundos) Then
'        mvarConexionServer.rdoSummitCon.Execute "SET LOCK_TIMEOUT  " & gParametrosGenerales.giLockTimeOut, rdExecDirect
'    Else
'        mvarConexionServer.rdoSummitCon.Execute "SET LOCK_TIMEOUT  " & varMilisegundos, rdExecDirect
'    End If
    
'End Sub

Public Sub EstablecerTiempoDeBloqueo(Optional ByVal varMilisegundos As Variant)
       
       
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error GoTo ErrorDeConexion
    
    ConectarDeUseServer
    If IsMissing(varMilisegundos) Then
        mvarConexionServer.ADOSummitCon.Execute "SET LOCK_TIMEOUT  " & gParametrosGenerales.giLockTimeOut
    Else
        mvarConexionServer.ADOSummitCon.Execute "SET LOCK_TIMEOUT  " & varMilisegundos
    End If
    
    Exit Sub
    
ErrorDeConexion:
    
    Resume Salir
    
Salir:
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CReuniones", "EstablecerTiempoDeBloqueo", ERR, Erl)
      Exit Sub
   End If

End Sub

''' <summary>
''' Carga de reuniones
''' </summary>
''' <param optional name="DesdeFecha">Desde la fecha</param>
''' <param optional name="HastaFecha">Hasta la fecha</param>
''' <param optional name="UsarIndice">par�metro que indica si las clases se crean con �ndice o no</param>
''' <param optional name="SoloFechasDeResultados">Incluir solo fechas de resultado</param>
''' <param optional name="bAscendente">Orden</param>
''' <remarks>Llamada desde: frmREU.sdbcFecReu_DropDown
''' Tiempo m�ximo: <1 seg </remarks>
''' <revision>JVS 05/01/2012</revision>
Public Sub CargarReuniones(Optional ByVal DesdeFecha As Variant, Optional ByVal HastaFecha As Variant, Optional ByVal UsarIndice As Boolean = False, Optional ByVal SoloFechasDeResultados As Boolean, Optional ByVal bAscendente As Boolean)
Dim rs As New adodb.Recordset
Dim sConsulta As String
Dim lIndice As Long
Dim oReu As CReunion
Dim fldFecha As adodb.Field
Dim fldRef As adodb.Field


'CALIDAD Sin WITH(NOLOCK) para obtener las fechas actualizadas
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If SoloFechasDeResultados Then
    sConsulta = "SELECT DISTINCT AR_PROCE.FECULTREU AS FECHA,REU.REF FROM AR_PROCE LEFT JOIN REU ON AR_PROCE.ADJDIR <1 AND AR_PROCE.FECULTREU=REU.FECHA WHERE AR_PROCE.ADJDIR <1"
Else
    sConsulta = "SELECT FECHA,REF FROM REU WHERE 1=1"
End If

If Not IsMissing(DesdeFecha) Then
    sConsulta = sConsulta & " AND FECHA >= " & DateToSQLTimeDate(DesdeFecha)
End If

If Not IsMissing(HastaFecha) Then
    sConsulta = sConsulta & " AND FECHA <= " & DateToSQLTimeDate(HastaFecha)
End If

If bAscendente Then
    sConsulta = sConsulta & " ORDER BY FECHA ASC"
Else
    sConsulta = sConsulta & " ORDER BY FECHA DESC"
End If

rs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    
    If rs.eof Then
    
        rs.Close
        Set rs = Nothing
        Set mCol = New Collection
        Exit Sub
        
    Else
        
        ''' Carga del Resultset en la coleccion
        Set mCol = Nothing
        Set mCol = New Collection
        
        Set fldFecha = rs.Fields("FECHA")
        Set fldRef = rs.Fields("REF")
        
        If UsarIndice Then
            
            lIndice = 0
            
            While Not rs.eof
                
                Set oReu = New CReunion
                With oReu
                    .Fecha = CDate(fldFecha.Value)
                    .Referencia = fldRef.Value
                    Set .Conexion = mvarConexion
                    Set .ConexionServer = mvarConexionServer
                    .Indice = lIndice
                End With
                
                mCol.Add oReu, CStr(lIndice)
                
                rs.MoveNext
                lIndice = lIndice + 1
                
            Wend
            
        Else
        
            While Not rs.eof
        
                Set oReu = New CReunion
                With oReu
                    
                    .Fecha = CDate(fldFecha.Value)
                    .Referencia = fldRef.Value
                    Set .Conexion = mvarConexion
                    Set .ConexionServer = mvarConexionServer
                    
                End With
                
                mCol.Add oReu, CStr(fldFecha.Value)
                rs.MoveNext
                
            Wend
            
        End If
        
        rs.Close
        Set rs = Nothing
        Set fldFecha = Nothing
        Set fldRef = Nothing
        
    End If

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CReuniones", "CargarReuniones", ERR, Erl)
      Exit Sub
   End If

End Sub

