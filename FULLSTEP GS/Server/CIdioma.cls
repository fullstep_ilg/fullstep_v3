VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CIdioma"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Private mvarCOD As String
Private mvarDen As String
Private mvarOffset As Variant

Public Property Get Cod() As String
Cod = mvarCOD
End Property

Public Property Let Cod(ByVal Data As String)
Let mvarCOD = Data
End Property

Public Property Get Den() As String
Den = mvarDen
End Property

Public Property Let Den(ByVal Data As String)
  Let mvarDen = Data
End Property

Public Property Get Offset() As Variant
Offset = mvarOffset
End Property

Public Property Let Offset(ByVal Data As Variant)
Let mvarOffset = Data
End Property

