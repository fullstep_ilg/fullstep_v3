VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CMaterialQA"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True

Option Explicit

Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum

Private mvarId As Integer 'local copy
Private mvarDen As String 'local copy
Private mvarBaja As Integer 'local copy

Private mCol_GMN1 As CGruposMatNivel1
Private mCol_GMN2 As CGruposMatNivel2
Private mCol_GMN3 As CGruposMatNivel3
Private mCol_GMN4 As CGruposMatNivel4

Private mvarConexion As CConexion 'local copy
Private mvarConexionServer As CConexionDeUseServer  'local copy

Public Property Get ID() As Integer
    ID = mvarId
End Property

Public Property Let ID(ByVal Data As Integer)
    mvarId = Data
End Property

Public Property Get Den() As String
    Den = mvarDen
End Property

Public Property Let Den(ByVal Data As String)
    mvarDen = Data
End Property

Public Property Get Baja() As Integer
    Baja = mvarBaja
End Property

Public Property Let Baja(ByVal Data As Integer)
    mvarBaja = Data
End Property

Friend Property Set Conexion(ByVal vData As CConexion)
    Set mvarConexion = vData
End Property
Friend Property Get Conexion() As CConexion
    Set Conexion = mvarConexion
End Property

Friend Property Set ConexionServer(ByVal con As CConexionDeUseServer)
Set mvarConexionServer = con
End Property
Friend Property Get ConexionServer() As CConexionDeUseServer
Set ConexionServer = mvarConexionServer
End Property

Private Sub ConectarDeUseServer()
    If mvarConexionServer.ADOSummitCon Is Nothing Then
        Set mvarConexionServer.ADOSummitCon = New adodb.Connection
        mvarConexionServer.ADOSummitCon.Open "Provider=SQLOLEDB.1;Auto Translate=FALSE;Server=" & basParametros.ServidorGS & ";Database=" & basParametros.BaseDatosGS & ";", LICDBLogin, LICDBContra
        mvarConexionServer.ADOSummitCon.CursorLocation = adUseServer
        mvarConexionServer.ADOSummitCon.CommandTimeout = 120
    End If
End Sub

Private Sub Class_Terminate()

    Set mCol_GMN1 = Nothing
    Set mCol_GMN2 = Nothing
    Set mCol_GMN3 = Nothing
    Set mCol_GMN4 = Nothing
    Set mvarConexion = Nothing
    Set mvarConexionServer = Nothing
End Sub

Public Property Get Col_GMN1() As CGruposMatNivel1
    Set Col_GMN1 = mCol_GMN1
End Property

Public Property Get Col_GMN2() As CGruposMatNivel2
    Set Col_GMN2 = mCol_GMN2
End Property

Public Property Get Col_GMN3() As CGruposMatNivel3
    Set Col_GMN3 = mCol_GMN3
End Property

Public Property Get Col_GMN4() As CGruposMatNivel4
    Set Col_GMN4 = mCol_GMN4
End Property

''' Revisado por: Jbg. Fecha: 13/12/2011
''' <summary>
''' Carga en las colecciones "estructura" GMN de uno o todos los materiales qa
''' </summary>
''' <param name="Todos">1-> informaci�n de todos los materiales 0->eoc</param>
''' <param name="Idioma">Idioma</param>
''' <remarks>Llamada desde: cMaterialesQa.Add ; Tiempo m�ximo: 0,2</remarks>
Public Sub DameEstructuraMaterial(ByVal Todos As Boolean, ByVal Idioma As String)
    Dim rs As adodb.Recordset
    Dim sConsulta As String
    
    '********* Precondicion **************************************
    If mvarConexion Is Nothing Then
        Err.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CDestinos.CargarTodosLosMateriales", "No se ha establecido la conexion"
        Exit Sub
    End If
    '*************************************************************
    
    Set mCol_GMN1 = New CGruposMatNivel1
    Set mCol_GMN2 = New CGruposMatNivel2
    Set mCol_GMN3 = New CGruposMatNivel3
    Set mCol_GMN4 = New CGruposMatNivel4
    
    Set rs = New adodb.Recordset
    If Todos Then
        sConsulta = "SELECT COD AS GMN1,DEN_" & Idioma & " AS G1DEN FROM GMN1 G WITH(NOLOCK) ORDER BY GMN1"
    Else
        sConsulta = "SELECT MQ.GMN1,DEN_" & Idioma & " AS G1DEN FROM MATERIAL_QA_GMN1 MQ WITH(NOLOCK) INNER JOIN GMN1 G WITH(NOLOCK) ON MQ.GMN1= G.COD WHERE MATERIAL_QA=" & mvarId & " ORDER BY MQ.GMN1"
    End If
    rs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    While Not rs.eof
        mCol_GMN1.Add rs("GMN1").Value, rs("G1DEN").Value
        rs.MoveNext
    Wend
    
    Set rs = New adodb.Recordset
    If Todos Then
        sConsulta = "SELECT G2.GMN1,G2.COD AS GMN2,G1.DEN_" & Idioma & " AS G1DEN,G2.DEN_" & Idioma & " AS G2DEN "
        sConsulta = sConsulta & " FROM GMN1 G1 WITH(NOLOCK) INNER JOIN GMN2 G2 WITH(NOLOCK) ON G1.COD= G2.GMN1 "
        sConsulta = sConsulta & " ORDER BY G2.GMN1,G2.COD"
    Else
        sConsulta = "SELECT MQ.GMN1,MQ.GMN2,G1.DEN_" & Idioma & " AS G1DEN,G2.DEN_" & Idioma & " AS G2DEN FROM MATERIAL_QA_GMN2 MQ WITH(NOLOCK) "
        sConsulta = sConsulta & " INNER JOIN GMN1 G1 WITH(NOLOCK) ON MQ.GMN1= G1.COD"
        sConsulta = sConsulta & " INNER JOIN GMN2 G2 WITH(NOLOCK) ON MQ.GMN1= G2.GMN1 AND MQ.GMN2= G2.COD"
        sConsulta = sConsulta & " WHERE MATERIAL_QA=" & mvarId & " ORDER BY MQ.GMN1,MQ.GMN2"
    End If
    rs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    While Not rs.eof
        mCol_GMN2.Add rs("GMN1").Value, rs("G1DEN").Value, rs("GMN2").Value, rs("G2DEN").Value
        rs.MoveNext
    Wend
    
    Set rs = New adodb.Recordset
    If Todos Then
        sConsulta = "SELECT G3.GMN1,G3.GMN2,G3.COD AS GMN3,G1.DEN_" & Idioma & " AS G1DEN,G2.DEN_" & Idioma & " AS G2DEN,G3.DEN_" & Idioma & " AS G3DEN "
        sConsulta = sConsulta & " FROM GMN1 G1 WITH(NOLOCK) INNER JOIN GMN2 G2 WITH(NOLOCK) ON G1.COD= G2.GMN1 "
        sConsulta = sConsulta & " INNER JOIN GMN3 G3 WITH(NOLOCK) ON G1.COD= G3.GMN1 AND G2.COD= G3.GMN2 "
        sConsulta = sConsulta & " ORDER BY G3.GMN1,G3.GMN2,G3.COD"
    Else
        sConsulta = "SELECT MQ.GMN1,MQ.GMN2,MQ.GMN3,G1.DEN_" & Idioma & " AS G1DEN,G2.DEN_" & Idioma & " AS G2DEN,G3.DEN_" & Idioma & " AS G3DEN FROM MATERIAL_QA_GMN3 MQ WITH(NOLOCK) "
        sConsulta = sConsulta & " INNER JOIN GMN1 G1 WITH(NOLOCK) ON MQ.GMN1= G1.COD"
        sConsulta = sConsulta & " INNER JOIN GMN2 G2 WITH(NOLOCK) ON MQ.GMN1= G2.GMN1 AND MQ.GMN2= G2.COD"
        sConsulta = sConsulta & " INNER JOIN GMN3 G3 WITH(NOLOCK) ON MQ.GMN1= G3.GMN1 AND MQ.GMN2= G3.GMN2 AND MQ.GMN3= G3.COD"
        sConsulta = sConsulta & " WHERE MATERIAL_QA=" & mvarId & " ORDER BY MQ.GMN1,MQ.GMN2,MQ.GMN3"
    End If
    rs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    While Not rs.eof
        mCol_GMN3.Add rs("GMN1").Value, rs("GMN2").Value, rs("GMN3").Value, rs("G1DEN").Value, rs("G2DEN").Value, rs("G3DEN").Value
        rs.MoveNext
    Wend
    
    Set rs = New adodb.Recordset
    If Todos Then
        sConsulta = "SELECT G4.GMN1,G4.GMN2,G4.GMN3,G4.COD AS GMN4,G1.DEN_" & Idioma & " AS G1DEN,G2.DEN_" & Idioma & " AS G2DEN,G3.DEN_" & Idioma & " AS G3DEN ,G4.DEN_" & Idioma & " AS G4DEN "
        sConsulta = sConsulta & " FROM GMN1 G1 WITH(NOLOCK) INNER JOIN GMN2 G2 WITH(NOLOCK) ON G1.COD= G2.GMN1 "
        sConsulta = sConsulta & " INNER JOIN GMN3 G3 WITH(NOLOCK) ON G1.COD= G3.GMN1 AND G2.COD= G3.GMN2 "
        sConsulta = sConsulta & " INNER JOIN GMN4 G4 WITH(NOLOCK) ON G1.COD= G4.GMN1 AND G2.COD= G4.GMN2 AND G3.COD= G4.GMN3 "
        sConsulta = sConsulta & " ORDER BY G4.GMN1,G4.GMN2,G4.GMN3,G4.COD"
    Else
        sConsulta = "SELECT MQ.GMN1,MQ.GMN2,MQ.GMN3,MQ.GMN4,G1.DEN_" & Idioma & " AS G1DEN,G2.DEN_" & Idioma & " AS G2DEN,G3.DEN_" & Idioma & " AS G3DEN,G4.DEN_" & Idioma & " AS G4DEN FROM MATERIAL_QA_GMN4 MQ WITH(NOLOCK) "
        sConsulta = sConsulta & " INNER JOIN GMN1 G1 WITH(NOLOCK) ON MQ.GMN1= G1.COD"
        sConsulta = sConsulta & " INNER JOIN GMN2 G2 WITH(NOLOCK) ON MQ.GMN1= G2.GMN1 AND MQ.GMN2= G2.COD"
        sConsulta = sConsulta & " INNER JOIN GMN3 G3 WITH(NOLOCK) ON MQ.GMN1= G3.GMN1 AND MQ.GMN2= G3.GMN2 AND MQ.GMN3= G3.COD"
        sConsulta = sConsulta & " INNER JOIN GMN4 G4 WITH(NOLOCK) ON MQ.GMN1= G4.GMN1 AND MQ.GMN2= G4.GMN2 AND MQ.GMN3= G4.GMN3 AND MQ.GMN4= G4.COD"
        sConsulta = sConsulta & " WHERE MATERIAL_QA=" & mvarId & " ORDER BY MQ.GMN1,MQ.GMN2,MQ.GMN3,MQ.GMN4"
    End If
    rs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    While Not rs.eof
        mCol_GMN4.Add rs("GMN1").Value, rs("GMN2").Value, rs("GMN3").Value, rs("G1DEN").Value, rs("G2DEN").Value, rs("G3DEN").Value, rs("GMN4").Value, rs("G4DEN").Value
        rs.MoveNext
    Wend
    
    rs.Close
    Set rs = Nothing
End Sub
