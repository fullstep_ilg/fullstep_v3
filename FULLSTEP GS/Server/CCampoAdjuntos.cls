VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CCampoAdjuntos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CCampoAdjuntos **********************************
'*             Autor : Mertxe Martin
'*             Creada : 21/02/2005
'****************************************************************


Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum

Private mCol As Collection
Private m_oConexion As CConexion

Public Property Get Item(vntIndexKey As Variant) As CCampoAdjunto
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Friend Property Set Conexion(ByVal con As CConexion)
    Set m_oConexion = con
End Property

Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property


Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
     Count = mCol.Count
End If


End Property

Public Function Add(ByVal Id As Long, ByVal Fecha As Date, ByVal Nombre As String, Optional ByVal dataSize As Variant, _
            Optional ByVal Comentario As Variant, Optional ByVal Ruta As Variant, Optional ByVal Idioma As Variant, _
            Optional ByVal oPersona As CPersona, Optional ByVal oCampo As CFormItem, Optional ByVal oLinea As CLineaDesglose, _
            Optional ByVal FecAct As Variant, Optional ByVal varIndice As Variant, Optional ByVal oSolicitud As CSolicitud, _
            Optional ByVal oInstancia As CInstancia) As CCampoAdjunto
            
    'create a new object
  
    Dim objnewmember As CCampoAdjunto
    Set objnewmember = New CCampoAdjunto
   
    Set objnewmember.Solicitud = oSolicitud
    Set objnewmember.Campo = oCampo
    Set objnewmember.Linea = oLinea
    Set objnewmember.Instancia = oInstancia
    
    objnewmember.Id = Id
    objnewmember.Fecha = Fecha
    objnewmember.Nombre = Nombre
    
    If IsMissing(dataSize) Or IsNull(dataSize) Then
        objnewmember.dataSize = 0
    Else
        objnewmember.dataSize = dataSize
    End If
    
    If IsMissing(Comentario) Then
        objnewmember.Comentario = Null
    Else
        objnewmember.Comentario = Comentario
    End If
    
    If IsMissing(Ruta) Then
        objnewmember.Ruta = Null
    Else
        objnewmember.Ruta = Ruta
    End If
    
    If IsMissing(Idioma) Then
        objnewmember.Idioma = Null
    Else
        objnewmember.Idioma = Idioma
    End If
    
    If IsMissing(FecAct) Then
        objnewmember.FecAct = Null
    Else
        objnewmember.FecAct = FecAct
    End If
    
    Set objnewmember.Persona = oPersona
    
    Set objnewmember.Conexion = m_oConexion
    
    If Not IsMissing(varIndice) And Not IsNull(varIndice) Then
        objnewmember.Indice = varIndice
        mCol.Add objnewmember, CStr(varIndice)
    Else
       mCol.Add objnewmember, CStr(Id)
    End If
    
    
    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing

End Function

Public Sub Remove(vntIndexKey As Variant)
    mCol.Remove vntIndexKey
End Sub


Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
     Set NewEnum = mCol.[_NewEnum]
End Property


Private Sub Class_Initialize()
    Set mCol = New Collection
End Sub


Private Sub Class_Terminate()
    Set mCol = Nothing
    Set m_oConexion = Nothing
End Sub



