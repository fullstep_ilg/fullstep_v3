VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CHistorico"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Private mvarItemId As Integer
Private mvarPrecioInicial As Variant
Private mvarObjetivoInicial As Variant
Private mvarPostObjetivoInicial As Variant
Private mvarObjetivoUltimo As Variant
Private mvarPostObjetivoUltimo As Variant
Private mvarAhorroPorcentual As Variant
Private mvarAhorroImporte As Variant
Private mvarIndice As Variant
Public Property Get Indice() As Variant
    Indice = mvarIndice
End Property
Public Property Let Indice(ByVal varInd As Variant)
    mvarIndice = varInd
End Property
Public Property Get ItemId() As Integer
    ItemId = mvarItemId
End Property
Public Property Let ItemId(ByVal I As Integer)
    mvarItemId = I
End Property

Public Property Get PrecioInicial() As Variant
    PrecioInicial = mvarPrecioInicial
End Property
Public Property Let PrecioInicial(ByVal v As Variant)
    mvarPrecioInicial = v
End Property
Public Property Get AhorroPorcentual() As Variant
    AhorroPorcentual = mvarAhorroPorcentual
End Property
Public Property Let AhorroPorcentual(ByVal v As Variant)
    mvarAhorroPorcentual = v
End Property
Public Property Get AhorroImporte() As Variant
    AhorroImporte = mvarAhorroImporte
End Property
Public Property Let AhorroImporte(ByVal v As Variant)
    mvarAhorroImporte = v
End Property

Public Property Get ObjetivoInicial() As Variant
    ObjetivoInicial = mvarObjetivoInicial
End Property
Public Property Let ObjetivoInicial(ByVal v As Variant)
    mvarObjetivoInicial = v
End Property
Public Property Get ObjetivoUltimo() As Variant
    ObjetivoUltimo = mvarObjetivoUltimo
End Property
Public Property Let ObjetivoUltimo(ByVal v As Variant)
    mvarObjetivoUltimo = v
End Property
Public Property Get PostObjetivoInicial() As Variant
    PostObjetivoInicial = mvarPostObjetivoInicial
End Property
Public Property Let PostObjetivoInicial(ByVal v As Variant)
    mvarPostObjetivoInicial = v
End Property

Public Property Get PostObjetivoUltimo() As Variant
    PostObjetivoUltimo = mvarPostObjetivoUltimo
End Property
Public Property Let PostObjetivoUltimo(ByVal v As Variant)
    mvarPostObjetivoUltimo = v
End Property

