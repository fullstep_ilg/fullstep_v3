VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CRelacionAtributo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True

'********************* CRelacionAtributo*************************
'*             Autor : NSA
'*             Creada : 29/01/2013
'****************************************************************


Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum


Implements IBaseDatos

Private m_oConexion As CConexion
'Variables de la tabla INT_ATRIB_RELACION
Private m_vId As Variant
Private m_vCod As Variant 'COD de la relacion X1,X2,.. etc operandos de la formula
Private m_vAtribID As Variant
Private m_vAtribIdRel As Variant
Private m_vValorNum As Variant
Private m_vValorBool As Variant
Private m_vValorText As Variant
Private m_vValorFec As Variant
Private m_vFecAct As Variant
Private m_vMinimo As Variant
Private m_vMaximo As Variant
'Variables necesarias para el formulario de relacion AtribRelacion
Private m_vAtribRelCod As Variant
Private m_vAtribRelDen As Variant
Private m_vAtribRelDescr As Variant
Private m_vAtribRelTipo As Variant
Private m_vAtribSelecion As Variant

Friend Property Set Conexion(ByVal vData As CConexion)
    Set m_oConexion = vData
End Property
Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property

Public Property Let Id(ByVal vData As Variant)
    m_vId = vData
End Property
Public Property Get Id() As Variant
    Id = m_vId
End Property

Public Property Let Minimo(ByVal vData As Variant)
    m_vMinimo = vData
End Property
Public Property Get Minimo() As Variant
    Minimo = m_vMinimo
End Property

Public Property Let Maximo(ByVal vData As Variant)
    m_vMaximo = vData
End Property
Public Property Get Maximo() As Variant
    Maximo = m_vMaximo
End Property

Public Property Let Cod(ByVal vData As Variant)
    m_vCod = vData
End Property
Public Property Get Cod() As Variant
    Cod = m_vCod
End Property

Public Property Let AtribID(ByVal vData As Variant)
    m_vAtribID = vData
End Property
Public Property Get AtribID() As Variant
    Id = m_vAtribID
End Property

Public Property Let AtribIdRel(ByVal vData As Variant)
    m_vAtribIdRel = vData
End Property
Public Property Get AtribIdRel() As Variant
    AtribIdRel = m_vAtribIdRel
End Property

Public Property Let ValorNum(ByVal vData As Variant)
    m_vValorNum = vData
End Property
Public Property Get ValorNum() As Variant
    ValorNum = m_vValorNum
End Property

Public Property Let ValorBool(ByVal vData As Variant)
    m_vValorBool = vData
End Property
Public Property Get ValorBool() As Variant
    ValorBool = m_vValorBool
End Property

Public Property Let ValorText(ByVal vData As Variant)
    m_vValorText = vData
End Property
Public Property Get ValorText() As Variant
    ValorText = m_vValorText
End Property

Public Property Let ValorFec(ByVal vData As Variant)
    m_vValorFec = vData
End Property
''' <summary>
''' Da acceso a la propiedad Valor Fecha
''' </summary>
''' <returns>propiedad Valor Fecha</returns>
''' <remarks>Llamada desde: CRelacionAtributos/CargarRelacionesAtributo ; Tiempo m�ximo: 0,2</remarks>
Public Property Get ValorFec() As Variant
    ValorFec = m_vValorFec
End Property

Public Property Let FecAct(ByVal vData As Variant)
    m_vFecAct = vData
End Property
Public Property Get FecAct() As Variant
    ValorNum = m_vFecAct
End Property

Public Property Let AtribRelCod(ByVal vData As Variant)
    m_vAtribRelCod = vData
End Property
Public Property Get AtribRelCod() As Variant
    AtribRelCod = m_vAtribRelCod
End Property

Public Property Let AtribRelDen(ByVal vData As Variant)
    m_vAtribRelDen = vData
End Property
Public Property Get AtribRelDen() As Variant
    AtribRelDen = m_vAtribRelDen
End Property

Public Property Let AtribRelDescr(ByVal vData As Variant)
    m_vAtribRelDescr = vData
End Property
Public Property Get AtribRelDescr() As Variant
    AtribRelDescr = m_vAtribRelDescr
End Property

Public Property Let AtribRelTipo(ByVal vData As Variant)
    m_vAtribRelTipo = vData
End Property
Public Property Get AtribRelTipo() As Variant
    AtribRelTipo = m_vAtribRelTipo
End Property

Public Property Let AtribRelSeleccion(ByVal vData As Variant)
    m_vAtribSelecion = vData
End Property
Public Property Get AtribRelSeleccion() As Variant
    AtribRelSeleccion = m_vAtribSelecion
End Property

Private Function IBaseDatos_AnyadirABaseDatos() As TipoErrorSummit

    Dim rs As adodb.Recordset
    Dim fldNum As adodb.Field
    Dim sConsulta As String
    Dim TESError As TipoErrorSummit
    Dim btrans As Boolean
    Dim adocom As adodb.Command
    Dim ador As adodb.Recordset
    Dim oParam As adodb.Parameter


TESError.NumError = TESnoerror

If m_oConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CRelacionAtributo.AnyadirABAseDatos", "No se ha establecido la conexion"
End If
' *****************************************

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
    
    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
    btrans = True

''' Preparar la SP de inserci�n de los nuevos valores de la lista y sus parametros


        Set adocom = New adodb.Command
        Set adocom.ActiveConnection = m_oConexion.ADOCon
        
        adocom.CommandType = adCmdStoredProc
        adocom.CommandText = "ATR_INSERTAR_RELACION"
        
        Set oParam = adocom.CreateParameter("COD", adVarChar, adParamInput, 5, m_vCod)
        adocom.Parameters.Append oParam
        Set oParam = adocom.CreateParameter("ATRIBID", adInteger, adParamInput, , m_vAtribID)
        adocom.Parameters.Append oParam
        Set oParam = adocom.CreateParameter("ATRIBREL", adInteger, adParamInput, , m_vAtribIdRel)
        adocom.Parameters.Append oParam
        Set oParam = adocom.CreateParameter("VALORTEXT", adVarChar, adParamInput, -1, m_vValorText)
        adocom.Parameters.Append oParam
        Set oParam = adocom.CreateParameter("VALORNUM", adDouble, adParamInput, , m_vValorNum)
        adocom.Parameters.Append oParam
        Set oParam = adocom.CreateParameter("VALORBOOL", adTinyInt, adParamInput, , m_vValorBool)
        adocom.Parameters.Append oParam
         If Not IsNull(m_vValorFec) And m_vValorFec <> "" And Not IsEmpty(m_vValorFec) Then
            Set oParam = adocom.CreateParameter("VALORFEC", adDate, adParamInput, , CVDate(m_vValorFec))
        Else
            Set oParam = adocom.CreateParameter("VALORFEC", adDate, adParamInput, , m_vValorFec)
        End If
        adocom.Parameters.Append oParam
        
        'Parametros de que devuelve el stored
        Set oParam = adocom.CreateParameter("FECACT", adDate, adParamOutput)
        adocom.Parameters.Append oParam
        Set oParam = adocom.CreateParameter("INS", adInteger, adParamOutput)
        adocom.Parameters.Append oParam
        
        
         ''' Ejecutar la SP
        adocom.Prepared = True

        adocom.Execute
        
        If m_oConexion.ADOCon.Errors.Count > 0 Then
            GoTo ERROR
        End If
        
        'Recogemos el nuevo valor de FECACT y ID del atributo
        m_vFecAct = adocom.Parameters("FECACT").Value
        m_vId = adocom.Parameters("INS").Value
        
         Set adocom.ActiveConnection = Nothing
        Set adocom = Nothing
        
        m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
        btrans = False
        
        IBaseDatos_AnyadirABaseDatos = TESError
        Exit Function
    
Error_Cls:

    IBaseDatos_AnyadirABaseDatos = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    If btrans Then
        m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
    End If
    
    If Not adocom Is Nothing Then
        Set adocom = Nothing
    End If
    

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CRelacionAtributo", "IBaseDatos_AnyadirABaseDatos", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If

End Function


Private Function IBaseDatos_CambiarCodigo(CodigoNuevo As Variant) As TipoErrorSummit

End Function

Private Sub IBaseDatos_CancelarEdicion()

End Sub

Private Function IBaseDatos_ComprobarExistenciaEnBaseDatos() As Boolean

End Function

Private Function IBaseDatos_EliminarDeBaseDatos() As TipoErrorSummit

Dim TESError As TipoErrorSummit
    Dim btrans As Boolean
    Dim sConsulta As String
    Dim rs As adodb.Recordset
    Dim adocom As adodb.Command
    Dim ador As adodb.Recordset
    Dim oParam As adodb.Parameter
    Dim iTipo As Integer
    

    TESError.NumError = TESnoerror
    
    '******************* Precondicion *******************
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CRelacionAtributo.EliminarDeBaseDatos", "No se ha establecido la conexion"
    End If
    '*****************************************************
    
    If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
    If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
    
    
    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
    btrans = True
    

        
        ''' Preparar la SP de inserci�n de los nuevos valores de la lista y sus parametros

        Set adocom = New adodb.Command
        Set adocom.ActiveConnection = m_oConexion.ADOCon
        
        adocom.CommandType = adCmdStoredProc
        adocom.CommandText = "ATR_ELIMINAR_RELACION"
        
        Set oParam = adocom.CreateParameter("IDRELACION", adInteger, adParamInput, , m_vId)
        adocom.Parameters.Append oParam
        
       
        ''' Ejecutar la SP
        adocom.Prepared = True
        Set ador = adocom.Execute
        
        If m_oConexion.ADOCon.Errors.Count > 0 Then
            GoTo ERROR
        End If
        
        Set adocom.ActiveConnection = Nothing
        Set adocom = Nothing
        
        Set ador = Nothing

    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
  
    btrans = False
    
    IBaseDatos_EliminarDeBaseDatos = TESError
    Exit Function
    
Error_Cls:
    
    IBaseDatos_EliminarDeBaseDatos = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    
    If btrans Then
        m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
    End If
    
    If Not adocom Is Nothing Then
        Set adocom = Nothing
    End If

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CRelacionAtributo", "IBaseDatos_EliminarDeBaseDatos", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If

End Function

Private Function IBaseDatos_FinalizarEdicionEliminando() As TipoErrorSummit

End Function

''' <summary>
''' Grabar en bbdd un cambio
''' </summary>
''' <returns>Error de haberlo</returns>
''' <remarks>Llamada desde: frmATRIBRelacion/sdbgRelaciones_BeforeUpdate ; Tiempo m�ximo: 0,2</remarks>
''' Revisado por: jbg; Fecha: 25/09/2013
Private Function IBaseDatos_FinalizarEdicionModificando() As TipoErrorSummit

Dim AdoRes As adodb.Recordset
Dim sConsulta As String
Dim TESError As TipoErrorSummit
Dim btrans As Boolean

'********Precondici�n******************************************************************************************************************
If m_oConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CRelacionAtributo.FinalizarEdicionModificando", "No se ha establecido la conexion"
End If
'**************************************************************************************************************************************

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

TESError.NumError = TESnoerror

If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:

    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
    btrans = True
    Set AdoRes = New adodb.Recordset

        'CALIDAD Se obtiene la Fecha de Actualizaci�n con bloqueo para recuperar la verdadera �ltima fecha de actualizaci�n
        sConsulta = "SELECT FECACT FROM INT_ATRIB_RELACION"
        sConsulta = sConsulta & " WHERE ID = " & m_vId
        AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
        If AdoRes.eof Then
            AdoRes.Close
            Set AdoRes = Nothing
            TESError.NumError = TESDatoEliminado
            TESError.Arg1 = 141 ' Tabla PROCE_ATRIB
            IBaseDatos_FinalizarEdicionModificando = TESError
            m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
            Exit Function
        End If

    
        If m_vFecAct <> AdoRes("FECACT") Then
            AdoRes.Close
            Set AdoRes = Nothing
            TESError.NumError = TESInfActualModificada
            m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
            Exit Function
        End If
    
    
        sConsulta = "UPDATE INT_ATRIB_RELACION "
        Select Case m_vAtribRelTipo
        Case 1:
            sConsulta = sConsulta & " SET VALOR_TEXT=" & StrToSQLNULL(m_vValorText) & ",VALOR_NUM=NULL,VALOR_FEC=NULL,VALOR_BOOL=NULL"
        Case 2
            sConsulta = sConsulta & " SET VALOR_NUM=" & DblToSQLFloat(m_vValorNum) & ",VALOR_TEXT=NULL,VALOR_FEC=NULL,VALOR_BOOL=NULL"
        Case 3:
            sConsulta = sConsulta & " SET VALOR_FEC=" & DateToSQLDate(m_vValorFec) & ",VALOR_NUM=NULL,VALOR_TEXT=NULL,VALOR_BOOL=NULL"
        Case 4:
            sConsulta = sConsulta & " SET VALOR_BOOL=" & BooleanToSQLBinary(m_vValorBool) & ",VALOR_NUM=NULL,VALOR_FEC=NULL,VALOR_TEXT=NULL"
        End Select
    
        sConsulta = sConsulta & " ,COD = " & StrToSQLNULL(m_vCod)
        sConsulta = sConsulta & " ,ATRIB_REL = " & CStr(m_vAtribIdRel)
        
        sConsulta = sConsulta & " WHERE ID =  " & m_vId
        
        'CALIDAD Se obtiene la Fecha de Actualizaci�n con bloqueo para recuperar la verdadera �ltima fecha de actualizaci�n
        sConsulta = sConsulta & " SELECT FECACT FROM INT_ATRIB "
        sConsulta = sConsulta & " WHERE ID =  " & m_vId
        
 
        m_oConexion.ADOCon.Execute sConsulta

    If m_oConexion.ADOCon.Errors.Count > 0 Then
        GoTo ERROR
    End If
     
     'Recogemos el nuevo valor de FECACT
     AdoRes.Requery
     m_vFecAct = AdoRes("FECACT").Value
   
     m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
     btrans = False
     
     IBaseDatos_FinalizarEdicionModificando = TESError
     
Finalizar:
On Error Resume Next
    If Not AdoRes Is Nothing Then
        AdoRes.Close
        Set AdoRes = Nothing
    End If
    Exit Function
    
Error_Cls:
  
    IBaseDatos_FinalizarEdicionModificando = basErrores.TratarError(m_oConexion.ADOCon.Errors)

    If btrans Then
        m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
    End If
    Resume Finalizar

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CRelacionAtributo", "IBaseDatos_FinalizarEdicionModificando", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If

End Function

Private Function IBaseDatos_IniciarEdicion(Optional ByVal Bloquear As Boolean, Optional ByVal UsuarioBloqueo As String) As TipoErrorSummit

End Function

