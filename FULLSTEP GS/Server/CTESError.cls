VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CTESError"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Private mvarNumError As Integer
Private mvarArg1 As Variant
Private mvarArg2 As Variant

Public Property Get NumError() As Integer
    NumError = mvarNumError
End Property

Public Property Let NumError(ByVal Num As Integer)
    mvarNumError = Num
End Property


Public Property Let Arg1(ByVal v As Variant)
    mvarArg1 = v
End Property

Public Property Get Arg1() As Variant
    Arg1 = mvarArg1
End Property

Public Property Let Arg2(ByVal v As Variant)
    mvarArg2 = v
End Property

Public Property Get Arg2() As Variant
    Arg2 = mvarArg2
End Property

