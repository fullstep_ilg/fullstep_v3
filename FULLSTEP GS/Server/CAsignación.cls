VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CAsignacion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

Private mvarRecordset As adodb.Recordset
Private mvarIndice As Variant 'Nos indica la posicion secuencial dentro de una Coleccion
Private mvarCodProve As String 'local copy
Private mvarDenProve As String 'local copy
Private mvarVal(3) As Variant
Private mvarCalif(3) As Variant
Private mvarCodEqp As Variant
Private mvarCodComp As Variant
Private mvarAsignado As Boolean
Private mvarCodPortal As Variant
Private mvarPremium As Boolean
Private mvarAntesDeCierreParcial As Boolean
Private mvarPremActivo As Boolean
Private mvarHayAdjMaterialesProce As Variant
Private mvarHayAdjMaterialesNoProce As Variant
Private m_oGrupos As CGrupos
Private mvarConexion As CConexion 'local copy
Private mvarCalidad As String
Private m_oPuntuaciones As Collection
Private m_oClaves As Collection

Public Property Get puntuaciones() As Collection
    Set puntuaciones = m_oPuntuaciones
End Property
Public Property Let puntuaciones(ByVal Valor As Collection)
    Set m_oPuntuaciones = Valor
End Property

Public Property Get claves() As Collection
    Set claves = m_oClaves
End Property
Public Property Let claves(ByVal Valor As Collection)
    Set m_oClaves = Valor
End Property

Public Property Get CodPortal() As Variant
    CodPortal = mvarCodPortal
End Property
Public Property Let CodPortal(ByVal Valor As Variant)
    mvarCodPortal = Valor
End Property
Public Property Get Grupos() As CGrupos
    Set Grupos = m_oGrupos
End Property
Public Property Set Grupos(ByVal Valor As CGrupos)
    Set m_oGrupos = Valor
End Property

Public Property Get Premium() As Boolean
    Premium = mvarPremium
End Property
Public Property Let Premium(ByVal Valor As Boolean)
    mvarPremium = Valor
End Property
Public Property Get AntesDeCierreParcial() As Boolean
    AntesDeCierreParcial = mvarAntesDeCierreParcial
End Property
Public Property Let AntesDeCierreParcial(ByVal Valor As Boolean)
    mvarAntesDeCierreParcial = Valor
End Property
Public Property Get PremActivo() As Boolean
    PremActivo = mvarPremActivo
End Property

Public Property Let PremActivo(ByVal Valor As Boolean)
    mvarPremActivo = Valor
End Property

Public Property Get Val1() As Variant
    Val1 = mvarVal(0)
End Property
Public Property Let Val1(ByVal Valor As Variant)
    mvarVal(0) = Valor
End Property
Public Property Get Val2() As Variant
    Val2 = mvarVal(1)
End Property
Public Property Let Val2(ByVal Valor As Variant)
    mvarVal(1) = Valor
End Property
Public Property Get Val3() As Variant
    Val3 = mvarVal(2)
End Property
Public Property Let Val3(ByVal Valor As Variant)
    mvarVal(2) = Valor
End Property
Public Property Get Calif1() As Variant
    Calif1 = mvarCalif(0)
End Property
Public Property Let Calif1(ByVal Val As Variant)
    mvarCalif(0) = Val
End Property
Public Property Get Calif2() As Variant
    Calif2 = mvarCalif(1)
End Property
Public Property Let Calif2(ByVal Val As Variant)
    mvarCalif(1) = Val
End Property
Public Property Get Calif3() As Variant
    Calif3 = mvarCalif(2)
End Property
Public Property Let Calif3(ByVal Val As Variant)
    mvarCalif(2) = Val
End Property
Public Property Get CodProve() As String
    CodProve = mvarCodProve
End Property
Public Property Let CodProve(ByVal Cod As String)
    mvarCodProve = Cod
End Property
Public Property Get DenProve() As String
    DenProve = mvarDenProve
End Property
Public Property Let DenProve(ByVal Den As String)
    mvarDenProve = Den
End Property
Public Property Get CodEqp() As Variant
    CodEqp = mvarCodEqp
End Property
Public Property Let CodEqp(ByVal Cod As Variant)
    mvarCodEqp = Cod
End Property
Public Property Get Asignado() As Boolean
    Asignado = mvarAsignado
End Property
Public Property Let Asignado(ByVal Cod As Boolean)
    mvarAsignado = Cod
End Property
Public Property Get CodComp() As Variant
    CodComp = mvarCodComp
End Property
Public Property Let CodComp(ByVal Cod As Variant)
    mvarCodComp = Cod
End Property
Public Property Get Indice() As Variant
    Indice = mvarIndice
End Property
Public Property Let Indice(ByVal varInd As Variant)
    mvarIndice = varInd
End Property

Public Property Get HayAdjMaterialesProce() As Variant
    HayAdjMaterialesProce = mvarHayAdjMaterialesProce
End Property
Public Property Let HayAdjMaterialesProce(ByVal varInd As Variant)
    mvarHayAdjMaterialesProce = varInd
End Property

Public Property Get HayAdjMaterialesNoProce() As Variant
    HayAdjMaterialesNoProce = mvarHayAdjMaterialesNoProce
End Property
Public Property Let HayAdjMaterialesNoProce(ByVal varInd As Variant)
    mvarHayAdjMaterialesNoProce = varInd
End Property

Friend Property Set Conexion(ByVal vData As CConexion)
    Set mvarConexion = vData
End Property
Friend Property Get Conexion() As CConexion
    Set Conexion = mvarConexion
End Property

Private Sub Class_Initialize()
Set m_oPuntuaciones = New Collection
Set m_oClaves = New Collection
End Sub

Private Sub Class_Terminate()
    Set mvarConexion = Nothing
    Set m_oPuntuaciones = Nothing
    Set m_oClaves = Nothing
End Sub

Public Property Get Calidad() As String
    Calidad = mvarCalidad
End Property
Public Property Let Calidad(ByVal Cal As String)
    mvarCalidad = Cal
End Property

''' <summary></summary>
''' <param name="Anyo">Anyo del proceso</param>
''' <param name="GMN1">GMN1 del proceso</param>
''' <param name="Proce">Codigo del proceso</param>
''' <param name="OrdPor"></param>
''' <param name="SoloConfirmados"></param>
''' <param name="UsarIndice"></param>
''' <returns>Objeto de tipo CHistoricos</returns>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>
''' <remarks>Revisado por: LTG  Fecha: 09/05/2011</remarks>

Public Function DevolverHistoricos(ByVal Anyo As String, ByVal GMN1 As String, ByVal Proce As Long, Optional ByVal OrdPor As TipoOrdenacionItems, Optional ByVal SoloConfirmados As Boolean, Optional ByVal UsarIndice As Boolean) As CHistoricos
    Dim rs As New adodb.Recordset
    Dim fldFecRec As adodb.Field
    Dim fldOfe As adodb.Field
    Dim fldCambio As adodb.Field
    Dim fldId As adodb.Field
    Dim fldPrecio As adodb.Field
    Dim fldFecReu As adodb.Field
    Dim fldObj As adodb.Field
    Dim fldItem As adodb.Field
    Dim sConsulta As String
    Dim varObs As Variant
    Dim oOferta As COferta
    Dim datFechaPrimeraOferta As Date
    Dim datFechaObjetivoInicial As Date
    Dim datFechaUltimaOferta As Date
    Dim iNumOfe As Integer
    Dim oHists As CHistoricos
    Dim dCambio As Double
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
    '1) Obtener precios de la primera oferta
        '1.1) Obtener fecha primera oferta, y numero de oferta y cambio respecto a la moneda de apertura
        
    sConsulta = "SELECT FECREC,OFE,CAMBIO FROM PROCE_OFE WITH (NOLOCK) WHERE ANYO=" & Anyo & " AND GMN1='" & DblQuote(GMN1) & "' AND PROCE=" & Proce & " AND PROVE='" & DblQuote(mvarCodProve) & "' AND FECREC=( SELECT MIN(FECREC) FROM PROCE_OFE WITH (NOLOCK) INNER JOIN OFEEST WITH (NOLOCK) ON PROCE_OFE.EST=OFEEST.COD WHERE ANYO=" & Anyo & " AND GMN1='" & DblQuote(GMN1) & "' AND PROCE=" & Proce & " AND PROVE='" & DblQuote(mvarCodProve) & "' AND OFEEST.COMP=1)"
    rs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    
    If rs.eof Then
        rs.Close
        Set rs = Nothing
        Set DevolverHistoricos = Nothing
        Exit Function
    Else
        
        Set fldFecRec = rs.Fields(0)
        Set fldOfe = rs.Fields(1)
        Set fldCambio = rs.Fields(2)

        datFechaPrimeraOferta = fldFecRec.Value
        iNumOfe = fldOfe.Value
        dCambio = fldCambio.Value
        rs.Close
        Set rs = Nothing
        
        '1.2) Obtener precios
        sConsulta = "SELECT ITEM.ID,ITEM_OFE.PRECIO FROM ITEM_OFE WITH (NOLOCK) INNER JOIN ITEM WITH (NOLOCK) ON ITEM.ANYO=ITEM_OFE.ANYO AND ITEM_OFE.GMN1 = ITEM.GMN1_PROCE AND ITEM_OFE.PROCE=ITEM.PROCE AND ITEM_OFE.ITEM=ITEM.ID AND ITEM_OFE.ANYO=" & Anyo & " AND ITEM_OFE.GMN1='" & DblQuote(GMN1) & "' AND ITEM_OFE.PROCE=" & Proce & " AND ITEM_OFE.PROVE='" & DblQuote(mvarCodProve) & "' AND ITEM_OFE.NUM=" & iNumOfe
        Select Case OrdPor
            Case TipoOrdenacionItems.OrdItemPorCodArt
                    sConsulta = sConsulta & " ORDER BY ITEM.ART"
            Case TipoOrdenacionItems.OrdItemPorDen
                    sConsulta = sConsulta & " ORDER BY ITEM.DESCR"
            Case TipoOrdenacionItems.OrdItemPorDest
                    sConsulta = sConsulta & " ORDER BY ITEM.DEST"
            Case TipoOrdenacionItems.OrdItemPorUni
                    sConsulta = sConsulta & " ORDER BY ITEM.UNI"
            Case TipoOrdenacionItems.OrdItemPorCant
                    sConsulta = sConsulta & " ORDER BY ITEM.CAN"
            Case TipoOrdenacionItems.OrdItemPorPrec
                    sConsulta = sConsulta & " ORDER BY ITEM.PRECIO"
            Case TipoOrdenacionItems.OrdItemPorPres
                    sConsulta = sConsulta & " ORDER BY ITEM.PRES"
            Case TipoOrdenacionItems.OrdPorPago
                    sConsulta = sConsulta & " ORDER BY ITEM.PAG"
            Case TipoOrdenacionItems.OrdItemPorFecIni
                    sConsulta = sConsulta & " ORDER BY FECINI ITEM.DESC"
            Case TipoOrdenacionItems.OrdItemPorFecFin
                    sConsulta = sConsulta & " ORDER BY FECFIN ITEM.DESC"
        End Select
        
        Set rs = New adodb.Recordset
        rs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
               
        Set fldId = rs.Fields(0)
        Set fldPrecio = rs.Fields(1)
               
        Set oHists = New CHistoricos
        
        While Not rs.eof
            ' Lo devolvemos en moneda de apertura.
            oHists.Add fldId.Value, fldPrecio.Value / dCambio
            rs.MoveNext
        Wend
        
    '2.) Obtener objetivos con fecha inmediatamente superior a la fecha de la primera oferta
        'Estamos suponiendo que se han fijado todos los objetivos de los items en la misma reuni�n
        sConsulta = "SELECT FECREU,OBJ,ITEM FROM ITEM_OBJ WITH (NOLOCK) WHERE ANYO =" & Anyo & " AND GMN1='" & DblQuote(GMN1) & "' AND PROCE=" & Proce & " AND FECREU=( SELECT MIN(FECREU) FROM ITEM_OBJ WITH (NOLOCK) WHERE ANYO =" & Anyo & " AND GMN1='" & DblQuote(GMN1) & "' AND PROCE=" & Proce & " AND  FECREU > " & DateToSQLTimeDate(ConvertirUTCaTZ(datFechaPrimeraOferta, GMT1_TZ)) & ")"
        rs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
        
        If rs.eof Then
            rs.Close
            Set rs = Nothing
        Else
            
            Set fldFecReu = rs.Fields(0)
            Set fldObj = rs.Fields(1)
            Set fldItem = rs.Fields(2)
            datFechaObjetivoInicial = fldFecReu
            While Not rs.eof
                oHists.Item(CStr(fldItem.Value)).ObjetivoInicial = fldObj.Value
                rs.MoveNext
            Wend
            rs.Close
            Set rs = Nothing
            
        End If
        
        Set fldFecRec = Nothing
        Set fldOfe = Nothing
        Set fldCambio = Nothing
        Set fldId = Nothing
        Set fldPrecio = Nothing
        Set fldObj = Nothing
        Set fldItem = Nothing
    '3.) Obtener precios con fecha inmediatamente superior a la m�nima fecha del objetivo inicial
        '3.1) Obtener numero de oferta y cambio
        
        'La fecha objetivo inicial se pasa a UTC para compararla con la FECREC (suponemos que est� en GMT+1)
        sConsulta = "SELECT FECREC,OFE,CAMBIO FROM PROCE_OFE WITH (NOLOCK) WHERE ANYO=" & Anyo & " AND GMN1='" & DblQuote(GMN1) & "' AND PROCE=" & Proce & " AND PROVE='" & DblQuote(mvarCodProve) & "' AND FECREC=( SELECT MIN(FECREC) AS FECREC FROM PROCE_OFE WITH (NOLOCK) INNER JOIN OFEEST WITH (NOLOCK) ON PROCE_OFE.EST=OFEEST.COD AND ANYO=" & Anyo & " AND GMN1='" & DblQuote(GMN1) & "' AND PROCE=" & Proce & " AND PROVE='" & DblQuote(mvarCodProve) & "' AND FECREC >" & DateToSQLDate(ConvertirTZaUTC(GMT1_TZ, datFechaObjetivoInicial)) & " AND OFEEST.COMP=1)"
        Set rs = New adodb.Recordset
        rs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
        
        If rs.eof Then
            rs.Close
            Set rs = Nothing
        Else
            
            Set fldFecRec = rs.Fields(0)
            Set fldOfe = rs.Fields(1)
            Set fldCambio = rs.Fields(2)

            iNumOfe = fldOfe.Value
            dCambio = fldCambio.Value
            rs.Close
            Set rs = Nothing
        End If
        '3.2) Obtener precios
        sConsulta = "SELECT ITEM.ID,ITEM_OFE.PRECIO FROM ITEM_OFE WITH (NOLOCK) INNER JOIN ITEM WITH (NOLOCK) ON ITEM.ANYO=ITEM_OFE.ANYO AND ITEM_OFE.GMN1 = ITEM.GMN1_PROCE AND ITEM_OFE.PROCE= ITEM.PROCE AND ITEM_OFE.ITEM=ITEM.ID AND ITEM_OFE.ANYO=" & Anyo & " AND ITEM_OFE.GMN1='" & DblQuote(GMN1) & "' AND ITEM_OFE.PROCE=" & Proce & " AND ITEM_OFE.PROVE='" & DblQuote(mvarCodProve) & "' AND ITEM_OFE.NUM=" & iNumOfe
        Set rs = New adodb.Recordset
        rs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
        
        Set fldId = rs.Fields(0)
        Set fldPrecio = rs.Fields(1)
                
        While Not rs.eof
            oHists.Item(CStr(fldId.Value)).PostObjetivoInicial = fldPrecio.Value / dCambio
            rs.MoveNext
        Wend
        
        rs.Close
        Set rs = Nothing
        Set fldFecRec = Nothing
        Set fldOfe = Nothing
        Set fldCambio = Nothing
        Set fldId = Nothing
        Set fldPrecio = Nothing
        Set fldObj = Nothing
        Set fldItem = Nothing

        
        '4.) Objetivo �ltimo. Obtener objetivos con fecha inmediatamente anterior a la �ltima oferta
         '4.1) Obtener fecha de objetivo inmediatamente anterior a la �ltima oferta
            '4.1.1) Obtener fecha y numero de oferta de �ltima oferta
            sConsulta = "SELECT FECREC,OFE,CAMBIO FROM PROCE_OFE WITH (NOLOCK) WHERE ANYO=" & Anyo & " AND GMN1='" & DblQuote(GMN1) & "' AND PROCE=" & Proce & " AND PROVE='" & DblQuote(mvarCodProve) & "' AND FECREC=(SELECT MAX(FECREC) FROM PROCE_OFE WITH (NOLOCK) INNER JOIN OFEEST WITH (NOLOCK) ON PROCE_OFE.EST=OFEEST.COD WHERE ANYO=" & Anyo & " AND GMN1='" & DblQuote(GMN1) & "' AND PROCE=" & Proce & " AND PROVE='" & DblQuote(mvarCodProve) & "' AND OFEEST.COMP=1 )"
            Set rs = New adodb.Recordset
            rs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    
            If rs.eof Then
                rs.Close
                Set rs = Nothing
            Else
                
                Set fldFecRec = rs.Fields(0)
                Set fldOfe = rs.Fields(1)
                Set fldCambio = rs.Fields(2)
                
                datFechaUltimaOferta = fldFecRec.Value
                iNumOfe = fldOfe.Value
                dCambio = fldCambio.Value
                rs.Close
                Set rs = Nothing
            '4.1.2) Obtener precios de �ltima oferta
                sConsulta = "SELECT ITEM.ID,ITEM_OFE.PRECIO FROM ITEM_OFE WITH (NOLOCK) INNER JOIN ITEM WITH (NOLOCK) ON ITEM.ANYO=ITEM_OFE.ANYO AND ITEM_OFE.GMN1 = ITEM.GMN1_PROCE AND ITEM_OFE.PROCE= ITEM.PROCE AND ITEM_OFE.ITEM=ITEM.ID AND ITEM_OFE.ANYO=" & Anyo & " AND ITEM_OFE.GMN1='" & DblQuote(GMN1) & "' AND ITEM_OFE.PROCE=" & Proce & " AND ITEM_OFE.PROVE='" & DblQuote(mvarCodProve) & "' AND ITEM_OFE.NUM=" & iNumOfe
                Set rs = New adodb.Recordset
                rs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
            
                Set fldId = rs.Fields(0)
                Set fldPrecio = rs.Fields(1)
            
                While Not rs.eof
                    oHists.Item(CStr(fldId.Value)).PostObjetivoUltimo = fldPrecio.Value / dCambio
                    rs.MoveNext
                Wend
            
                rs.Close
                Set rs = Nothing
            '4.1.3)Obtener objetivos inmediatamente anteriores a esa fecha
                sConsulta = "SELECT OBJ,ITEM FROM ITEM_OBJ WITH (NOLOCK) WHERE ANYO=" & Anyo & " AND GMN1='" & DblQuote(GMN1) & "' AND PROCE=" & Proce & " AND FECREU =(SELECT MAX(FECREU) FROM ITEM_OBJ WITH (NOLOCK) WHERE ANYO=" & Anyo & " AND GMN1='" & DblQuote(GMN1) & "' AND PROCE=" & Proce & " AND FECREU <" & DateToSQLTimeDate(datFechaUltimaOferta) & " )"
                Set rs = New adodb.Recordset
                rs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
            
                Set fldObj = rs.Fields(0)
                Set fldItem = rs.Fields(1)
                
                While Not rs.eof
                    oHists.Item(CStr(fldItem.Value)).ObjetivoUltimo = fldObj.Value
                    rs.MoveNext
                Wend
                        
            End If
               
            Set DevolverHistoricos = oHists
            Set oHists = Nothing
    End If
    
    rs.Close
    Set rs = Nothing
    Set fldFecRec = Nothing
    Set fldOfe = Nothing
    Set fldCambio = Nothing
    Set fldId = Nothing
    Set fldPrecio = Nothing
    Set fldObj = Nothing
    Set fldItem = Nothing
    
        Exit Function
    
Error_Cls:
    
       Set DevolverHistoricos = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CAsignacion", "DevolverHistoricos", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If
        
End Function

''' <summary></summary>
''' <param name="Anyo">Anyo del proceso</param>
''' <param name="GMN1">GMN1 del proceso</param>
''' <param name="Proce">Codigo del proceso</param>
''' <param name="SoloConfirmados"></param>
''' <param name="UsarIndice"></param>
''' <returns>Objeto de tipo CHistorico</returns>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>
''' <remarks>Revisado por: LTG  Fecha: 09/05/2011</remarks>

Public Function DevolverHistoricoGeneral(ByVal Anyo As String, ByVal GMN1 As String, ByVal Proce As Long, Optional ByVal SoloConfirmados As Boolean, Optional ByVal UsarIndice As Boolean) As CHistorico
    Dim rs As New adodb.Recordset
    Dim fldFecRec As adodb.Field
    Dim fldOfe As adodb.Field
    Dim fldCambio As adodb.Field
    Dim fldPrecio As adodb.Field
    Dim fldFecReu As adodb.Field
    Dim fldGlobPres As adodb.Field
    Dim fldPres As adodb.Field
    Dim sConsulta As String
    Dim varObs As Variant
    Dim oOferta As COferta
    Dim datFechaPrimeraOferta As Date
    Dim datFechaObjetivoInicial As Date
    Dim datFechaUltimaOferta As Date
    Dim iNumOfe As Integer
    Dim dCambio As Double
    Dim oHist As CHistorico
    Dim bCondicion As Boolean
    Dim dImporte As Double
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
    '1) Obtener sumatorio de precios*cantidades de la primera oferta
        '1.1) Obtener fecha primera oferta, y numero de oferta y cambio respecto a la moneda de apertura
            
    sConsulta = "SELECT FECREC,OFE,CAMBIO FROM PROCE_OFE WITH (NOLOCK) WHERE ANYO=" & Anyo & " AND GMN1='" & DblQuote(GMN1) & "' AND PROCE=" & Proce & " AND PROVE='" & DblQuote(mvarCodProve) & "' AND FECREC=( SELECT MIN(FECREC) FROM PROCE_OFE WITH (NOLOCK) INNER JOIN OFEEST WITH (NOLOCK) ON PROCE_OFE.EST=OFEEST.COD WHERE ANYO=" & Anyo & " AND GMN1='" & DblQuote(GMN1) & "' AND PROCE=" & Proce & " AND PROVE='" & DblQuote(mvarCodProve) & "' AND OFEEST.COMP=1)"
    rs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    
    If rs.eof Then
        rs.Close
        Set rs = Nothing
        Set DevolverHistoricoGeneral = Nothing
        Exit Function
    Else
        
        Set fldFecRec = rs.Fields(0)
        Set fldOfe = rs.Fields(1)
        Set fldCambio = rs.Fields(2)

        datFechaPrimeraOferta = fldFecRec.Value
        iNumOfe = fldOfe.Value
        dCambio = fldCambio.Value
        rs.Close
        Set rs = Nothing
        
        '1.2) Obtener precios
        sConsulta = "SELECT ITEM.ID,(ITEM_OFE.PRECIO*ITEM.CANT) AS PRECIO FROM ITEM_OFE WITH (NOLOCK) INNER JOIN ITEM WITH (NOLOCK) ON ITEM.ANYO=ITEM_OFE.ANYO AND ITEM_OFE.GMN1 = ITEM.GMN1_PROCE AND ITEM_OFE.PROCE=ITEM.PROCE AND ITEM_OFE.ITEM=ITEM.ID AND ITEM_OFE.ANYO=" & Anyo & " AND ITEM_OFE.GMN1='" & DblQuote(GMN1) & "' AND ITEM_OFE.PROCE=" & Proce & " AND ITEM_OFE.PROVE='" & DblQuote(mvarCodProve) & "' AND ITEM_OFE.NUM=" & iNumOfe
        Set rs = New adodb.Recordset
        rs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
        
        Set fldPrecio = rs.Fields(1)
        
        Set oHist = New CHistorico
        oHist.AhorroImporte = Null
        oHist.PrecioInicial = 0
        bCondicion = True
        While Not rs.eof And bCondicion
            ' Lo multiplicamos por el cambio ya.
            If IsNull(fldPrecio.Value) Then
                oHist.PrecioInicial = Null
                bCondicion = False
            Else
                oHist.PrecioInicial = oHist.PrecioInicial + fldPrecio.Value / dCambio
            End If
            rs.MoveNext
        Wend
        
        rs.Close
        Set rs = Nothing
    '2.) Obtener objetivos con fecha inmediatamente superior a la fecha de la primera oferta
        'Estamos suponiendo que se han fijado todos los objetivos de los items en la misma reuni�n
        sConsulta = "SELECT MIN(FECREU) AS FECREU FROM ITEM_OBJ WITH (NOLOCK) WHERE ANYO =" & Anyo & " AND GMN1='" & DblQuote(GMN1) & "' AND PROCE=" & Proce & " AND  FECREU > " & DateToSQLTimeDate(ConvertirUTCaTZ(datFechaPrimeraOferta, GMT1_TZ))
        Set rs = New adodb.Recordset
        rs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
        
        If rs.eof Then
            Set rs = Nothing
        Else
            
            Set fldFecReu = rs.Fields(0)
            datFechaObjetivoInicial = fldFecReu.Value
            bCondicion = True
            rs.Close
            Set rs = Nothing
        End If
        
        Set fldFecRec = Nothing
        Set fldOfe = Nothing
        Set fldCambio = Nothing
        Set fldPrecio = Nothing
        Set fldFecReu = Nothing
        
    '3.) Obtener precios con fecha inmediatamente superior a la m�nima fecha del objetivo inicial
        '3.1) Obtener numero de oferta y cambio
                
        sConsulta = "SELECT FECREC,OFE,CAMBIO FROM PROCE_OFE WITH (NOLOCK) WHERE ANYO=" & Anyo & " AND GMN1='" & DblQuote(GMN1) & "' AND PROCE=" & Proce & " AND PROVE='" & DblQuote(mvarCodProve) & "' AND FECREC=( SELECT MIN(FECREC) AS FECREC FROM PROCE_OFE WITH (NOLOCK) INNER JOIN OFEEST WITH (NOLOCK) ON PROCE_OFE.EST=OFEEST.COD AND ANYO=" & Anyo & " AND GMN1='" & DblQuote(GMN1) & "' AND PROCE=" & Proce & " AND PROVE='" & DblQuote(mvarCodProve) & "' AND FECREC >" & DateToSQLDate(ConvertirTZaUTC(GMT1_TZ, datFechaObjetivoInicial)) & " AND OFEEST.COMP=1)"
        Set rs = New adodb.Recordset
        rs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
        
        If rs.eof Then
            rs.Close
            Set rs = Nothing
        Else
            
            Set fldFecRec = rs.Fields(0)
            Set fldOfe = rs.Fields(1)
            Set fldCambio = rs.Fields(2)

            iNumOfe = fldOfe.Value
            dCambio = fldCambio.Value
            rs.Close
            Set rs = Nothing
        End If
        '3.2) Obtener precios
        sConsulta = "SELECT ITEM.ID,(ITEM_OFE.PRECIO*ITEM.CANT) AS PRECIO FROM ITEM_OFE WITH (NOLOCK) INNER JOIN ITEM WITH (NOLOCK) ON ITEM.ANYO=ITEM_OFE.ANYO AND ITEM_OFE.GMN1 = ITEM.GMN1_PROCE AND ITEM_OFE.PROCE= ITEM.PROCE AND ITEM_OFE.ITEM=ITEM.ID AND ITEM_OFE.ANYO=" & Anyo & " AND ITEM_OFE.GMN1='" & DblQuote(GMN1) & "' AND ITEM_OFE.PROCE=" & Proce & " AND ITEM_OFE.PROVE='" & DblQuote(mvarCodProve) & "' AND ITEM_OFE.NUM=" & iNumOfe
        Set rs = New adodb.Recordset
        rs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
        
        Set fldPrecio = rs.Fields(1)
        
        bCondicion = True
        
        oHist.PostObjetivoInicial = 0
        While Not rs.eof And bCondicion
            ' Lo multiplicamos por el cambio ya.
            If IsNull(fldPrecio.Value) Then
                oHist.PostObjetivoInicial = Null
                bCondicion = False
            Else
                oHist.PostObjetivoInicial = oHist.PostObjetivoInicial + fldPrecio.Value / dCambio
            End If
            rs.MoveNext
        Wend
        
        rs.Close
        Set rs = Nothing
        Set fldFecRec = Nothing
        Set fldOfe = Nothing
        Set fldCambio = Nothing
        Set fldPrecio = Nothing
        Set fldFecReu = Nothing

        
        '4.) Objetivo �ltimo. Obtener objetivos con fecha inmediatamente anterior a la �ltima oferta
         '4.1) Obtener fecha de objetivo inmediatamente anterior a la �ltima oferta
            '4.1.1) Obtener fecha y numero de oferta de �ltima oferta
            sConsulta = "SELECT FECREC,OFE,CAMBIO FROM PROCE_OFE WITH (NOLOCK) WHERE ANYO=" & Anyo & " AND GMN1='" & DblQuote(GMN1) & "' AND PROCE=" & Proce & " AND PROVE='" & DblQuote(mvarCodProve) & "' AND FECREC=( SELECT MAX(FECREC) FROM PROCE_OFE WITH (NOLOCK) INNER JOIN OFEEST WITH (NOLOCK) ON PROCE_OFE.EST=OFEEST.COD WHERE ANYO=" & Anyo & " AND GMN1='" & DblQuote(GMN1) & "' AND PROCE=" & Proce & " AND PROVE='" & DblQuote(mvarCodProve) & "' AND OFEEST.COMP=1)"
            Set rs = New adodb.Recordset
            rs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
            
                        
            If rs.eof Then
                rs.Close
                Set rs = Nothing
            Else
                
                Set fldFecRec = rs.Fields(0)
                Set fldOfe = rs.Fields(1)
                Set fldCambio = rs.Fields(2)

                datFechaUltimaOferta = fldFecRec.Value
                iNumOfe = fldOfe.Value
                dCambio = fldCambio.Value
                rs.Close
                Set rs = Nothing
            End If
            
            '4.1.2) Obtener precios de �ltima oferta
                sConsulta = "SELECT ITEM.ID,(ITEM_OFE.PRECIO*ITEM.CANT) AS PRECIO FROM ITEM_OFE WITH (NOLOCK) INNER JOIN ITEM WITH (NOLOCK) ON ITEM.ANYO=ITEM_OFE.ANYO AND ITEM_OFE.GMN1 = ITEM.GMN1_PROCE AND ITEM_OFE.PROCE= ITEM.PROCE AND ITEM_OFE.ITEM=ITEM.ID AND ITEM_OFE.ANYO=" & Anyo & " AND ITEM_OFE.GMN1='" & DblQuote(GMN1) & "' AND ITEM_OFE.PROCE=" & Proce & " AND ITEM_OFE.PROVE='" & DblQuote(mvarCodProve) & "' AND ITEM_OFE.NUM=" & iNumOfe
                Set rs = New adodb.Recordset
                rs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
                
                Set fldPrecio = rs.Fields(1)
                
                oHist.PostObjetivoUltimo = 0
                While Not rs.eof And bCondicion
                    If IsNull(fldPrecio.Value) Then
                        oHist.PostObjetivoUltimo = Null
                        bCondicion = False
                    Else
                        oHist.PostObjetivoUltimo = oHist.PostObjetivoUltimo + fldPrecio.Value / dCambio
                    End If
                    rs.MoveNext
                Wend
            
                rs.Close
                Set rs = Nothing
                Set fldPrecio = Nothing
            ' Ahora tenemos que calcular el ahorro
            sConsulta = "SELECT GLOBPRES,PRES FROM PROCE WITH (NOLOCK) WHERE ANYO=" & Anyo & " AND GMN1='" & DblQuote(GMN1) & "' AND COD=" & Proce
            Set rs = New adodb.Recordset
            rs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
            
            Set fldGlobPres = rs.Fields(0)
            Set fldPres = rs.Fields(1)
            
            If rs.eof Then
                Set DevolverHistoricoGeneral = Nothing
            Else
                If fldGlobPres.Value Then
                    oHist.AhorroImporte = fldPres.Value - oHist.PostObjetivoUltimo
                    oHist.AhorroPorcentual = (oHist.AhorroImporte / fldPres.Value) * 100
                    rs.Close
                    Set rs = Nothing
                Else
                    rs.Close
                    Set rs = Nothing
                    sConsulta = "SELECT ITEM.CANT*ITEM.PRECIO AS PRECIO FROM ITEM WITH (NOLOCK) WHERE ANYO=" & Anyo & " AND GMN1_PROCE='" & DblQuote(GMN1) & "' AND PROCE=" & Proce
                    Set rs = New adodb.Recordset
                    rs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
                    
                    Set fldPrecio = rs.Fields(0)
                    
                    dImporte = 0
                    While Not rs.eof
                        dImporte = fldPrecio.Value + dImporte
                        rs.MoveNext
                    Wend
                
                    rs.Close
                    Set rs = Nothing
                    
                    oHist.AhorroImporte = dImporte - oHist.PostObjetivoUltimo
                    oHist.AhorroPorcentual = (oHist.AhorroImporte / dImporte) * 100
                End If
                
                Set DevolverHistoricoGeneral = oHist
            
            End If
            
            
            Set oHist = Nothing
    End If
    
    Set fldFecRec = Nothing
    Set fldOfe = Nothing
    Set fldCambio = Nothing
    Set fldPrecio = Nothing
    Set fldFecReu = Nothing
    Set fldGlobPres = Nothing
    Set fldPres = Nothing

        Exit Function
 

Error_Cls:
    
       Set DevolverHistoricoGeneral = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CAsignacion", "DevolverHistoricoGeneral", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If
        
End Function

Public Sub CargarGrupos(ByVal iAnyo As Integer, ByVal sGMN1 As String, ByVal iCod As Long, Optional bBloquear As Boolean = False)
Dim sConsulta As String
Dim oRS As adodb.Recordset
Dim oProce As cProceso
Dim iBloqueo As Integer
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
Set m_oGrupos = Nothing
Set m_oGrupos = New CGrupos
Set m_oGrupos.Conexion = mvarConexion

Set oProce = New cProceso
Set oProce.Conexion = mvarConexion
oProce.Anyo = iAnyo
oProce.GMN1Cod = sGMN1
oProce.Cod = iCod

sConsulta = "SELECT PG.ID,PG.COD GRUPO, PG.DEN, PPG.BLOQUEO FROM PROCE_PROVE_GRUPOS PPG WITH (NOLOCK)" & _
             " INNER JOIN PROCE_GRUPO PG WITH (NOLOCK) ON PPG.ANYO=PG.ANYO AND PPG.GMN1=PG.GMN1 AND PPG.PROCE=PG.PROCE AND PPG.GRUPO=PG.ID WHERE PPG.ANYO=" & iAnyo & _
             " AND PPG.GMN1='" & DblQuote(sGMN1) & "' AND PPG.PROCE=" & iCod & " AND PPG.PROVE='" & DblQuote(mvarCodProve) & "'" & _
             " ORDER BY GRUPO"
Set oRS = New adodb.Recordset
oRS.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
While Not oRS.eof
    If bBloquear Then
        iBloqueo = Val(oRS("BLOQUEO") & "")
    End If
    m_oGrupos.Add oProce, oRS("GRUPO").Value, oRS("DEN").Value, , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , _
                  oRS("ID").Value, , , , , , , , , iBloqueo
    oRS.MoveNext
Wend
oRS.Close
Set oRS = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CAsignacion", "CargarGrupos", ERR, Erl)
      Exit Sub
   End If
End Sub

Public Sub BloquearGrupo(ByVal iAnyo As Integer, ByVal sGMN1 As String, ByVal iProce As Long, ByVal iBloqueo As Integer, ByVal lIdGrupoID As Long)
''' * Objetivo: Bloquear el grupo al proveedor asignado
''' * Recibe: lGrupoID      - ID del grupo
'''           sGMN1         - Grupo de materiales
'''           iProce        - Proceso
'''           iBloqueo      - 1= bloquear, 0 - desbloquear
'''           lIdGrupoID    - C�digo del grupo

Dim sConsulta As String
Dim bTransaccionEnCurso As Boolean
Dim oProce As cProceso
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
mvarConexion.ADOCon.Execute "BEGIN TRANSACTION"
 bTransaccionEnCurso = True

 ''' Ejecutar modificaci�n
 sConsulta = "UPDATE PROCE_PROVE_GRUPOS SET BLOQUEO = " & Abs(iBloqueo) & " WHERE ANYO = " & iAnyo & " AND GMN1 = '" & sGMN1 & "' AND PROCE = " & iProce & _
             " AND PROVE = '" & DblQuote(mvarCodProve) & "' AND GRUPO = " & lIdGrupoID
 mvarConexion.ADOCon.Execute sConsulta
If mvarConexion.ADOCon.Errors.Count > 0 Then GoTo ERROR
 ''' Terminar transaccion
 mvarConexion.ADOCon.Execute "COMMIT TRANSACTION"
 
 bTransaccionEnCurso = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If bTransaccionEnCurso Then
        mvarConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
        bTransaccionEnCurso = False
    End If
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CAsignacion", "BloquearGrupo", ERR, Erl)
      Exit Sub
   End If
End Sub

Public Function AsignarGrupo(ByVal iAnyo As Integer, ByVal sGMN1 As String, ByVal iProce As Long, ByVal sGrupo As String, Optional ByVal sDen As String, Optional ByVal lIdGrupoID As Long) As TipoErrorSummit

    ''' * Objetivo: Anyadir el grupo al proveedor asignado
    ''' * Recibe: lGrupoID - ID del grupo
    '''           sGrupo   - C�digo del grupo
    ''' * Devuelve: TipoErrorSummit con el error si lo hay
    Dim sConsulta As String
    Dim TESError As TipoErrorSummit
    Dim bTransaccionEnCurso As Boolean
    Dim oProce As cProceso
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    TESError.NumError = TESnoerror
    
If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
    
    mvarConexion.ADOCon.Execute "BEGIN TRANSACTION"
    bTransaccionEnCurso = True
   
    ''' Ejecutar insercion
    sConsulta = "INSERT INTO PROCE_PROVE_GRUPOS(ANYO,GMN1,PROCE,PROVE,GRUPO) VALUES (" & iAnyo & ",'" & DblQuote(sGMN1) & "'," & iProce & ",'" & DblQuote(mvarCodProve) & "'," & lIdGrupoID & ")"
    mvarConexion.ADOCon.Execute sConsulta
    If mvarConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    Set oProce = New cProceso
    Set oProce.Conexion = mvarConexion
    oProce.Anyo = iAnyo
    oProce.GMN1Cod = sGMN1
    oProce.Cod = iProce
    m_oGrupos.Add oProce, sGrupo, sDen, , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , lIdGrupoID

    ''' Terminar transaccion
    mvarConexion.ADOCon.Execute "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
   
    AsignarGrupo = TESError
    
    Exit Function
    
Error_Cls:
    AsignarGrupo = basErrores.TratarError(mvarConexion.ADOCon.Errors)
    If bTransaccionEnCurso Then
        mvarConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
        bTransaccionEnCurso = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CAsignacion", "AsignarGrupo", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If
        
End Function

Public Function DesasignarGrupo(ByVal iAnyo As Integer, ByVal sGMN1 As String, ByVal iProce As Long, ByVal sGrupoCod As String, Optional ByVal lGrupoID As Long) As TipoErrorSummit

    ''' * Objetivo: Anyadir el grupo al proveedor asignado
    ''' * Recibe: lGrupoID - ID del grupo
    ''' * Devuelve: TipoErrorSummit con el error si lo hay
    Dim sConsulta As String
    Dim TESError As TipoErrorSummit
    Dim bTransaccionEnCurso As Boolean
    Dim adoComm As adodb.Command
    Dim adoParam As adodb.Parameter
    Dim oProceso As cProceso
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    TESError.NumError = TESnoerror

If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
    
    mvarConexion.ADOCon.Execute "BEGIN DISTRIBUTED TRAN"
    bTransaccionEnCurso = True
    mvarConexion.ADOCon.Execute "SET XACT_ABORT ON"
   
    Set adoComm = New adodb.Command

    Set adoComm.ActiveConnection = mvarConexion.ADOCon
    sConsulta = "EXEC SP_ELIMINAR_ASIGNACION_GRUPO @ANYO=?, @GMN1=?, @PROCE=?, @PROVE=?, @GRUPO=?,@CODGRUPO=?"
    
    Set adoParam = adoComm.CreateParameter("ANYO", adSmallInt, adParamInput, , iAnyo)
    adoComm.Parameters.Append adoParam

    Set adoParam = adoComm.CreateParameter("GMN1", adVarChar, adParamInput, 50, sGMN1)
    adoComm.Parameters.Append adoParam

    Set adoParam = adoComm.CreateParameter("PROCE", adInteger, adParamInput, , iProce)
    adoComm.Parameters.Append adoParam

    Set adoParam = adoComm.CreateParameter("PROVE", adVarChar, adParamInput, 50, mvarCodProve)
    adoComm.Parameters.Append adoParam

    Set adoParam = adoComm.CreateParameter("GRUPO", adSmallInt, adParamInput, , lGrupoID)
    adoComm.Parameters.Append adoParam
    
    Set adoParam = adoComm.CreateParameter("CODGRUPO", adVarChar, adParamInput, 50, sGrupoCod)
    adoComm.Parameters.Append adoParam

    adoComm.CommandType = adCmdText
    adoComm.CommandText = sConsulta
    adoComm.Execute
    If mvarConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    
    Set adoComm = Nothing
    Set adoParam = Nothing
    
    Set oProceso = New cProceso
    Set oProceso.Conexion = mvarConexion
    oProceso.Anyo = iAnyo
    oProceso.GMN1Cod = sGMN1
    oProceso.Cod = iProce
    oProceso.CargarGrupos
    If oProceso.TieneOfertas > 0 Then
        TESError = oProceso.RecalcularOfertas(, , , mvarCodProve, , True)
        If TESError.NumError <> TESnoerror Then
            GoTo Error_Cls
        End If
    End If

    'm_oGrupos.Remove sGrupoCod
    m_oGrupos.Remove sGrupoCod
    
    ''' Terminar transaccion
    mvarConexion.ADOCon.Execute "COMMIT TRAN"
    mvarConexion.ADOCon.Execute "SET XACT_ABORT OFF"
    bTransaccionEnCurso = False
   
    DesasignarGrupo = TESError
    
    Exit Function
    
Error_Cls:
    DesasignarGrupo = basErrores.TratarError(mvarConexion.ADOCon.Errors)
    If bTransaccionEnCurso Then
        mvarConexion.ADOCon.Execute "SET XACT_ABORT OFF"
        bTransaccionEnCurso = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CAsignacion", "DesasignarGrupo", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If
        
End Function

Public Sub addPuntuacion(ByVal clave As String, ByVal Puntuacion As String)

m_oPuntuaciones.Add Puntuacion, clave
m_oClaves.Add clave, clave

End Sub


