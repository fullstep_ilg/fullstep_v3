VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CAccion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Private m_lID As Long
Private m_lOrden As Long
Private m_sDen As String

Public Property Get id() As Long
    id = m_lID
End Property

Public Property Let id(ByVal p_lID As Long)
    m_lID = p_lID
End Property

Public Property Get Orden() As Long
    Orden = m_lOrden
End Property

Public Property Let Orden(ByVal p_lOrden As Long)
    m_lOrden = p_lOrden
End Property

Public Property Get Den() As String
    Den = m_sDen
End Property

Public Property Let Den(ByVal p_sDen As String)
    m_sDen = p_sDen
End Property


