VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cPagosFactura"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

''' *** Clase: cPagosFacturas

''' Variables que almacenan las propiedades de los objetos
Private mCol As Collection
Private m_bEOF As Boolean
Private m_oConexion As CConexion


''' Control de errores
Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum


Public Function Add(ByVal lId As Long, ByVal sNumero As String, ByVal lIDFactura As Long, ByVal dFecha As Date, ByVal sEstadoDen As String) As cPagoFactura
    
    Dim objnewmember As cPagoFactura
    Dim Indice As String
    Indice = CStr(lId)
    Set objnewmember = New cPagoFactura
    
    objnewmember.ID = lId
    objnewmember.IdFactura = lIDFactura
    objnewmember.Numero = sNumero
    objnewmember.Fecha = dFecha
    objnewmember.EstadoDen = sEstadoDen
    
        
    Set objnewmember.Conexion = m_oConexion
        
    ''' A�adir el objeto arbol a la coleccion
    ''' Si no se especifica indice, se a�ade al final
    mCol.Add objnewmember, Indice
            
    Set Add = objnewmember
    
    Set objnewmember = Nothing

End Function


Friend Property Set Conexion(ByVal con As CConexion)

    ''' * Objetivo: Dar valor a la variable privada Conexion
    ''' * Recibe: Conexion
    ''' * Devuelve: Nada
    
    Set m_oConexion = con
    
End Property
Friend Property Get Conexion() As CConexion

    ''' * Objetivo: Devolver variable privada Conexion
    ''' * Recibe: Nada
    ''' * Devuelve: Conexion
    
    Set Conexion = m_oConexion
    
End Property


Public Property Get Count() As Long

    ''' * Objetivo: Devolver variable privada Count
    ''' * Recibe: Nada
    ''' * Devuelve: Numero de elementos de la coleccion

    If mCol Is Nothing Then
        Count = 0
    Else
        Count = mCol.Count
    End If

End Property

Public Property Get Item(vntIndexKey As Variant) As cPagoFactura

    ''' * Objetivo: Recuperar un �rbol de la coleccion
    ''' * Recibe: Indice del �rbol a recuperar
    ''' * Devuelve: �rbol correspondiente
        
On Error GoTo NoSeEncuentra:

    ''' Devolvemos el item de la coleccion privada
    
    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:

    Set Item = Nothing
    
End Property


Private Sub Class_Initialize()

    Set mCol = New Collection

End Sub

Private Sub Class_Terminate()
    Set mCol = Nothing
    Set m_oConexion = Nothing
End Sub

Public Sub Remove(vntIndexKey As Variant)
   
    mCol.Remove vntIndexKey
End Sub


Public Property Get NewEnum() As IUnknown
     Set NewEnum = mCol.[_NewEnum]
End Property

''' <summary>
''' Carga los distintos estados que tiene un pago.
''' </summary>
''' <param name="sIdi">Codigo Idioma</param>
''' <returns>RecordSet con los estados que tiene una factura</returns>
''' <remarks>Llamada desde=frmSeguimiento --> sdbcEstadoPago_DropDown; Tiempo m�ximo=0,1seg.</remarks>
Public Function devolverTipoEstados(sIdi As String) As adodb.Recordset
Dim sConsulta As String
Dim rs As adodb.Recordset
Dim adoComm As adodb.Command
Dim adoParam As adodb.Parameter
        
    sConsulta = "SELECT EP.ID, EPD.DEN"
    sConsulta = sConsulta & " FROM EST_PAGO EP WITH (NOLOCK)"
    sConsulta = sConsulta & " INNER JOIN EST_PAGO_DEN EPD WITH (NOLOCK) ON EPD.EST_PAGO = EP.ID AND EPD.IDI='" & sIdi & "'"
    
    Set adoComm = New adodb.Command
    Set adoComm.ActiveConnection = m_oConexion.ADOCon
    Set adoParam = adoComm.CreateParameter("IDI", adVarChar, adParamInput, 20, sIdi)
    adoComm.Parameters.Append adoParam
    adoComm.CommandText = sConsulta
    Set rs = adoComm.Execute
    
    Set devolverTipoEstados = rs
    Set rs.ActiveConnection = Nothing
End Function


