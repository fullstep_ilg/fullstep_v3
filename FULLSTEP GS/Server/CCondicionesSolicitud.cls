VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CCondicionesSolicitud"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum

Private m_Col As Collection
Private m_oConexion As CConexion
Private m_bEOF As Boolean

Public Property Get eof() As Boolean
    eof = m_bEOF
End Property
Friend Property Let eof(ByVal b As Boolean)
    m_bEOF = b
End Property

Public Property Get Item(vntIndexKey As Variant) As CCondicionSolicitud
    On Error GoTo NoSeEncuentra:

    Set Item = m_Col(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
End Property

Friend Property Set Conexion(ByVal con As CConexion)
    Set m_oConexion = con
End Property
Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property

Public Property Get Count() As Long
    If m_Col Is Nothing Then
        Count = 0
    Else
         Count = m_Col.Count
    End If
End Property

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
     Set NewEnum = m_Col.[_NewEnum]
End Property

''' <summary>A�ade un nuevo elemento a la colecci�n</summary>
''' <param name="lId"></param>
''' <param name="sCod"></param>
''' <param name="lCampo"></param>
''' <param name="sOperador"></param>
''' <param name="iTipoValor"></param>
''' <param name="lCampoValor"></param>
''' <param name="vValor"></param>
''' <param name="dtFecAct"></param>
''' <param name="vIndice"></param>
''' <param name="iTipoCampo"></param>
''' <remarks>Llamada desde: CSolicitud.CargarCondicionesSolicitud</remarks>

Public Function Add(ByVal lId As Long, ByVal sCod As String, Optional ByVal lCampo As Long, Optional ByVal sOperador As String, Optional ByVal iTipoValor As Integer, Optional ByVal vCampoValor As Variant, _
        Optional ByVal vValor As Variant, Optional ByVal dtFecAct As Variant, Optional ByVal vIndice As Variant, Optional ByVal iTipoCampo As TipoCampoCondicionEnlace) As CCondicionSolicitud
    'create a new object
    Dim objnewmember As CCondicionSolicitud
    
    Set objnewmember = New CCondicionSolicitud
    With objnewmember
        Set .Conexion = m_oConexion
        .Id = lId
        .Cod = sCod
        .Campo = lCampo
        .Operador = sOperador
        .TipoValor = iTipoValor
        .CampoValor = vCampoValor
        .Valor = vValor
        .FecAct = dtFecAct
        If iTipoCampo <> 0 Then .TipoCampo = iTipoCampo
        
        If Not IsMissing(vIndice) And Not IsNull(vIndice) Then
            .Indice = vIndice
            m_Col.Add objnewmember, CStr(vIndice)
        Else
            m_Col.Add objnewmember, CStr(sCod)
        End If
    
    End With
    
    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing
End Function

Public Sub AddCondicion(ByVal oCondicion As CCondicionSolicitud, Optional ByVal vIndice As Variant)
    If Not IsMissing(vIndice) And Not IsNull(vIndice) Then
        oCondicion.Indice = vIndice
        m_Col.Add oCondicion, CStr(vIndice)
    Else
        m_Col.Add oCondicion, CStr(oCondicion.Cod)
    End If
End Sub

Public Sub Remove(vntIndexKey As Variant)
    On Error GoTo ERROR

    m_Col.Remove vntIndexKey

ERROR:
End Sub

Private Sub Class_Initialize()
    Set m_Col = New Collection
End Sub

Private Sub Class_Terminate()
    'Proceroys collection when this class is terminated
    On Error Resume Next
    Set m_Col = Nothing
    Set m_oConexion = Nothing
End Sub

''' <summary>Devuelve el c�digo para una nueva condici�n de enlace</summary>
''' <remarks>Llamada desde: frmCertifCondicionOtroCertif</remarks>

Public Function NuevoCod() As Integer
    Dim oCondicion As CCondicionSolicitud
    Dim iMaxCod As Integer
    
    If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR

    If m_Col.Count = 0 Then
        iMaxCod = 1
    Else
        iMaxCod = 0
        For Each oCondicion In m_Col
            If CLng(Right(oCondicion.Cod, Len(oCondicion.Cod) - 1)) > iMaxCod Then iMaxCod = CLng(Right(oCondicion.Cod, Len(oCondicion.Cod) - 1))
        Next
        iMaxCod = iMaxCod + 1
    End If
    
Salir:
    NuevoCod = iMaxCod
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CCondicionesSolicitud", "NuevoCod", ERR, Erl)
        Resume Salir
    End If
End Function

