VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CRoles"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CRoles **********************************
'*             Autor : Javier Arana
'*             Creada : 13/8/98
'****************************************************************


Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum
Private mCol As Collection
Private mvarConexion As CConexion
Public mvarEOF As Boolean
Private mvarConexionServer As CConexionDeUseServer

Public Function EliminarRolesDeBaseDatos(ByVal Codigos As Variant) As TipoErrorSummit

'*********************************************************************
'*** Descripci�n: Elimina el/los rol/es seleccionados de la     ***
'***              base de datos.                                   ***
'*** Par�metros:  Codigos ::>  Es un array con los c�digos de las  ***
'***                           Unidades a eliminar                 ***
'*** Valor que devuelve: Un TipoErrorSummit que es una estructura  ***
'***                     de datos definida en CTiposDeDatos, y, en ***
'***                     caso de ocurrir alg�n error al borrar, el ***
'***                     campo Arg1 de TipoErrorSummit contendr�   ***
'***                     un array bidimensional con los datos      ***
'***                     (c�digo,codigo de datos relacionado,      ***
'***                     bookmark de la linea de denominaci�n del  ***
'***                     destino actual) de los roles que no   ***
'***                     pudieron ser eliminadas                   ***
'*********************************************************************

  Dim vNoEliminados() As Variant
  Dim udtTESError As TipoErrorSummit
  Dim i As Integer
  Dim iRes As Integer
  Dim btrans As Boolean
  Dim rs As adodb.Recordset
  Dim sConsulta As String


If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
  udtTESError.NumError = TESnoerror
  iRes = 0

  If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls
    
      For i = 1 To UBound(Codigos)
        
          mvarConexion.ADOCon.Execute "BEGIN TRANSACTION"
          btrans = True
          mvarConexion.ADOCon.Execute "DELETE FROM ROL_IDIOMA WHERE COD='" & DblQuote(Codigos(i)) & "'"
          If mvarConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
          mvarConexion.ADOCon.Execute "DELETE FROM ROL WHERE COD='" & DblQuote(Codigos(i)) & "'"
          If mvarConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
          If btrans Then
              mvarConexion.ADOCon.Execute "COMMIT TRANSACTION"
          End If
Seguir:
      Next
        
      If iRes > 0 Then
          udtTESError.NumError = TESImposibleEliminar
          udtTESError.Arg1 = vNoEliminados
      End If
      EliminarRolesDeBaseDatos = udtTESError
  Exit Function

Error_Cls:
      udtTESError = basErrores.TratarError(mvarConexion.ADOCon.Errors)
      If mvarConexion.ADOCon.Errors(0).NativeError = 547 Then
        Set rs = New adodb.Recordset
        Select Case udtTESError.Arg1
        
        Case 86
            ' ESTA RELACIONADA CON LA TABLA PROCE_PER
            sConsulta = "SELECT PROCE_PER.ANYO, PROCE_PER.GMN1, PROCE_PER.PROCE FROM PROCE_PER WITH (NOLOCK) WHERE PROCE_PER.ROL='" & DblQuote(Codigos(i)) & "'"
            rs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
            ReDim Preserve vNoEliminados(3, iRes)
            vNoEliminados(0, iRes) = Codigos(i)
            vNoEliminados(1, iRes) = udtTESError.Arg1
            vNoEliminados(2, iRes) = i
            vNoEliminados(3, iRes) = rs(0).Value & "-" & rs(1).Value
            iRes = iRes + 1
            If btrans Then
                mvarConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
            End If
            Resume Seguir
        Case 66
            ' ESTA RELACIONADA CON LA TABLA PARGEN DEF
            sConsulta = "SELECT PARGEN_DEF.ID FROM PARGEN_DEF WITH (NOLOCK) WHERE PARGEN_DEF.ROLDEF='" & DblQuote(Codigos(i)) & "'"
                rs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
            ReDim Preserve vNoEliminados(3, iRes)
            vNoEliminados(0, iRes) = Codigos(i)
            vNoEliminados(1, iRes) = udtTESError.Arg1
            vNoEliminados(2, iRes) = i
            vNoEliminados(3, iRes) = rs(0).Value
            iRes = iRes + 1
            If btrans Then
                mvarConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
            End If
            Resume Seguir
        End Select
        
        rs.Close
        Set rs = Nothing
      Else
      
        mvarConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
        EliminarRolesDeBaseDatos = udtTESError
      
      End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
        'Call g_oErrores.TratarError("Class Module", "CRoles", "EliminarRolesDeBaseDatos", Err, Erl)
      GoTo Error_Cls
      Exit Function
   End If
End Function

Public Sub CargarTodosLosRolesDesde(ByVal NumMaximo As Integer, Optional ByVal CaracteresInicialesCod As String, Optional ByVal CaracteresInicialesDen As String, Optional ByVal OrdenadosPorDen As Boolean, Optional ByVal OrdenadosPorConv As Boolean, Optional ByVal UsarIndice As Boolean, Optional ByVal Idi As String)

    ''' * Objetivo: Cargar las Roles hasta un numero maximo
    ''' * Objetivo: desde un codigo o una denominacion determinadas
    
    Dim rs As New adodb.Recordset
    
    Dim sConsulta As String
    
    Dim lIndice As Long
    '''''''''''''''''''''Dim iNumMon As Integer
    Dim fldCod As adodb.Field
    Dim fldDen As adodb.Field
    Dim fldConv As adodb.Field
    Dim fldIdioma As adodb.Field
    Dim fldInvi As adodb.Field ' a�ado el invitado Kepa Mu�oz 20/01/2006
    
    ''' Precondicion
    If mvarConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CRoles.CargarTodosLosRoles", "No se ha establecido la conexion"
        Exit Sub
    End If
   
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
    ''' Generacion del SQL a partir de los parametros
    sConsulta = "SELECT TOP " & NumMaximo & " ROL.*,ROL_IDIOMA.DEN,ROL_IDIOMA.IDIOMA FROM ROL WITH (NOLOCK) INNER JOIN ROL_IDIOMA WITH (NOLOCK)"
    sConsulta = sConsulta & " ON ROL.COD= ROL_IDIOMA.COD"
    
    If CaracteresInicialesCod = "" And CaracteresInicialesDen = "" Then
        'sConsulta = "SELECT * FROM ROL"
       
    Else
    
        If Not (CaracteresInicialesCod = "") And Not (CaracteresInicialesDen = "") Then
                'sConsulta = "SELECT * FROM ROL WHERE"
                'sConsulta = sConsulta & " COD >='" & DblQuote(CaracteresInicialesCod) & "'"
                'sConsulta = sConsulta & " AND DEN >='" & DblQuote(CaracteresInicialesDen) & "'"
                        
            sConsulta = sConsulta & " AND ROL.COD >='" & DblQuote(CaracteresInicialesCod) & "'"
            sConsulta = sConsulta & " AND DEN >='" & DblQuote(CaracteresInicialesDen) & "'"
                        
        Else
            
            If Not (CaracteresInicialesCod = "") Then
                    'sConsulta = "SELECT * FROM ROL WHERE"
                    'sConsulta = sConsulta & " COD >='" & DblQuote(CaracteresInicialesCod) & "'"
                
                sConsulta = sConsulta & " AND ROL.COD >='" & DblQuote(CaracteresInicialesCod) & "'"
            
            Else
                    'sConsulta = "SELECT * FROM ROL WHERE"
                    'sConsulta = sConsulta & " DEN >='" & DblQuote(CaracteresInicialesDen) & "'"
                sConsulta = sConsulta & " AND DEN >='" & DblQuote(CaracteresInicialesDen) & "'"
            
            End If
        
        End If
               
    End If
    
    'Filtra por el idioma
    If Not IsMissing(Idi) And Idi <> "" Then
        sConsulta = sConsulta & " AND IDIOMA = '" & Idi & "'"
    End If
    
    If OrdenadosPorDen Then
        'sConsulta = sConsulta & " ORDER BY DEN"
        sConsulta = sConsulta & " ORDER BY DEN,ROL.COD"
    Else
        If OrdenadosPorConv Then
            sConsulta = sConsulta & " ORDER BY CONV,ROL.COD"
        Else
            sConsulta = sConsulta & " ORDER BY ROL.COD"
        End If
    End If
          
    ''' Crear el Resultset
    
    rs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    
    If rs.eof Then
            
        rs.Close
        Set rs = Nothing
        Set mCol = Nothing
        Set mCol = New Collection
        Exit Sub
          
    Else
        
        ''' Llenar la coleccion a partir del Resultset
        ''' teniendo en cuenta el maximo
        
        Set mCol = Nothing
        Set mCol = New Collection
            
        Set fldCod = rs.Fields("COD")
        Set fldDen = rs.Fields("DEN")
        Set fldConv = rs.Fields("CONV")
        Set fldIdioma = rs.Fields("IDIOMA")
        Set fldInvi = rs.Fields("INVITADO") ' recojo el check de invitado Kepa Mu�oz 20/01/2006
        
        If UsarIndice Then
            
            lIndice = 0
            ''''''''''''''''iNumMon = 0
                    
            While Not rs.eof ''''''''''''''''And iNumMon < NumMaximo
                'Me.Add rdores("COD"), rdores("DEN"), rdores("CONV"), lIndice
                'rdores.MoveNext
                'lIndice = lIndice + 1
                'iNumMon = iNumMon + 1
                
                'a�ado el fldInvi al Me.Add Kepa Mu�oz 20/01/2006
                If Not IsMissing(Idi) Then
                    Me.Add fldCod.Value, IIf(IsNull(fldDen.Value), "", fldDen.Value), SQLBinaryToBoolean(fldConv.Value), SQLBinaryToBoolean(fldInvi.Value), Idi, lIndice
                Else
                    ' Dejamos en manos del programador cuando asignar la conexion al obejto usuario
                    Me.Add fldCod.Value, IIf(IsNull(fldDen.Value), "", fldDen.Value), SQLBinaryToBoolean(fldConv.Value), SQLBinaryToBoolean(fldInvi.Value), fldIdioma.Value, lIndice
                End If
                rs.MoveNext
                lIndice = lIndice + 1
                '''''''''''''''''''''''iNumMon = iNumMon + 1
                
            Wend
            
            If Not rs.eof Then
                mvarEOF = False
            Else
                mvarEOF = True
            End If
                        
        Else
            
            ''''''''''''''''''iNumMon = 0
            
            While Not rs.eof ''''''''''''''''''And iNumMon < NumMaximo
                'Me.Add rdores("COD"), rdores("DEN"), rdores("CONV")
                'rdores.MoveNext
                'iNumMon = iNumMon + 1
                                
                ' Dejamos en manos del programador cuando asignar la conexion al obejto usuario
                 'a�ado el fldInvi al Me.Add Kepa Mu�oz 20/01/2006
                If Not IsMissing(Idi) Then
                    Me.Add fldCod.Value, IIf(IsNull(fldDen.Value), "", fldDen.Value), SQLBinaryToBoolean(fldConv.Value), SQLBinaryToBoolean(fldInvi.Value), Idi
                Else
                    Me.Add fldCod.Value, IIf(IsNull(fldDen.Value), "", fldDen.Value), SQLBinaryToBoolean(fldConv.Value), SQLBinaryToBoolean(fldInvi.Value), fldIdioma.Value
                End If
                rs.MoveNext
                ''''''''''''''''''iNumMon = iNumMon + 1
             Wend
            
            If Not rs.eof Then
                mvarEOF = False
            Else
                mvarEOF = True
            End If
            
        End If
        
        rs.Close
        Set rs = Nothing
        Set fldCod = Nothing
        Set fldDen = Nothing
        Set fldConv = Nothing
        Set fldIdioma = Nothing
        Set fldInvi = Nothing ' limpio la variable de invitado Kepa Mu�oz 20/01/2006
                        
          
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CRoles", "CargarTodosLosRolesDesde", ERR, Erl)
      Exit Sub
   End If
    
End Sub

Public Sub CargarTodosLosRoles(Optional ByVal CaracteresInicialesCod As String, Optional ByVal CaracteresInicialesDen As String, Optional ByVal CoincidenciaTotal As Boolean, Optional ByVal OrdenadosPorDen As Boolean, Optional ByVal OrdenadosPorConv As Boolean, Optional ByVal UsarIndice As Boolean, Optional ByVal Idi As String)
Dim rs As New adodb.Recordset
Dim sConsulta As String
Dim lIndice As Long
Dim fldCod As adodb.Field
Dim fldDen As adodb.Field
Dim fldConv As adodb.Field
Dim fldIdioma As adodb.Field
Dim fldInvi As adodb.Field ' a�ado fldInvi Kepa Mu�oz 20/01/2006

'********* Precondicion **************************************
If mvarConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CRoles.CargarTodosLosRoles", "No se ha establecido la conexion"
    Exit Sub
End If
'*************************************************************
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

sConsulta = "SELECT ROL.COD,ROL.FECACT,ROL.INVITADO,ROL.CONV,ROL_IDIOMA.DEN,ROL_IDIOMA.IDIOMA FROM ROL WITH (NOLOCK) "
sConsulta = sConsulta & "LEFT JOIN ROL_IDIOMA WITH (NOLOCK) ON ROL.COD = ROL_IDIOMA.COD"

'Filtramos por el idioma
If Not IsMissing(Idi) And Idi <> "" Then
    sConsulta = sConsulta & " AND IDIOMA = '" & Idi & "'"
End If

If CaracteresInicialesCod = "" And CaracteresInicialesDen = "" Then
    
    'sConsulta = "SELECT * FROM ROL"
   
Else
    If Not (CaracteresInicialesCod = "") And Not (CaracteresInicialesDen = "") Then
        
        If CoincidenciaTotal Then
            'sConsulta = "SELECT * FROM ROL WHERE"
            'sConsulta = sConsulta & " COD ='" & DblQuote(CaracteresInicialesCod) & "'"
            'sConsulta = sConsulta & " AND DEN='" & DblQuote(CaracteresInicialesDen) & "'"
            
            sConsulta = sConsulta & " WHERE ROL.COD ='" & DblQuote(CaracteresInicialesCod) & "'"
            sConsulta = sConsulta & " AND DEN='" & DblQuote(CaracteresInicialesDen) & "'"
        
        Else
            'sConsulta = "SELECT * FROM ROL WHERE"
            'sConsulta = sConsulta & " COD LIKE '" & DblQuote(CaracteresInicialesCod) & "%'"
            'sConsulta = sConsulta & " DEN LIKE '" & DblQuote(CaracteresInicialesDen) & "%'"
            
            sConsulta = sConsulta & " WHERE ROL.COD LIKE '" & DblQuote(CaracteresInicialesCod) & "%'"
            sConsulta = sConsulta & " DEN LIKE '" & DblQuote(CaracteresInicialesDen) & "%'"
        
        End If
                    
    Else
        
        If Not (CaracteresInicialesCod = "") Then
            
            If CoincidenciaTotal Then
                'sConsulta = "SELECT * FROM ROL WHERE"
                'sConsulta = sConsulta & " COD ='" & DblQuote(CaracteresInicialesCod) & "'"
                
                sConsulta = sConsulta & " WHERE ROL.COD ='" & DblQuote(CaracteresInicialesCod) & "'"
            
            Else
                'sConsulta = "SELECT * FROM ROL WHERE"
                'sConsulta = sConsulta & " COD LIKE '" & DblQuote(CaracteresInicialesCod) & "%'"
                
                sConsulta = sConsulta & " WHERE ROL.COD LIKE '" & DblQuote(CaracteresInicialesCod) & "%'"
            
            End If
            
        Else
            
            If CoincidenciaTotal Then
                'sConsulta = "SELECT * FROM ROL WHERE"
                'sConsulta = sConsulta & " DEN='" & DblQuote(CaracteresInicialesDen) & "'"
                
                sConsulta = sConsulta & " WHERE DEN ='" & DblQuote(CaracteresInicialesDen) & "'"
            
            Else
                'sConsulta = "SELECT * FROM ROL WHERE"
                'sConsulta = sConsulta & " DEN LIKE '" & DblQuote(CaracteresInicialesDen) & "%'"
                  
                sConsulta = sConsulta & " WHERE DEN LIKE '" & DblQuote(CaracteresInicialesDen) & "%'"
            
            End If
            
        End If
    
    End If
           
End If

If OrdenadosPorDen Then
    'sConsulta = sConsulta & " ORDER BY DEN,COD"
    sConsulta = sConsulta & " ORDER BY DEN,ROL.COD"

Else
    If OrdenadosPorConv Then
        sConsulta = sConsulta & " ORDER BY CONV,ROL.COD"
    Else
        sConsulta = sConsulta & " ORDER BY ROL.COD,DEN"
    End If
End If
      
rs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly

If rs.eof Then
        
    rs.Close
    Set rs = Nothing
    Set mCol = New Collection
    Exit Sub
      
Else
    
    Set mCol = Nothing
    Set mCol = New Collection
        
    Set fldCod = rs.Fields("COD")
    Set fldDen = rs.Fields("DEN")
    Set fldConv = rs.Fields("CONV")
    Set fldIdioma = rs.Fields("IDIOMA")
    Set fldInvi = rs.Fields("INVITADO") 'a�ado fldInvi Kepa Mu�oz 20/01/2006
    
    If UsarIndice Then
        
        lIndice = 0
        
        While Not rs.eof
            'Dejamos en manos del programador cuando asignar la conexion al obejto usuario
            'Me.Add rdores("COD"), rdores("DEN"), rdores("CONV"), lIndice
            'rdores.MoveNext
            'lIndice = lIndice + 1
           
            ' a�ado el fldInvi al Me.Add Kepa Mu�oz 20/01/2006
            If Not IsMissing(Idi) Then
                Me.Add fldCod.Value, IIf(IsNull(fldDen.Value), "", fldDen.Value), SQLBinaryToBoolean(fldConv.Value), SQLBinaryToBoolean(fldInvi.Value), Idi, lIndice
            Else
                ' Dejamos en manos del programador cuando asignar la conexion al obejto usuario
                Me.Add fldCod.Value, IIf(IsNull(fldDen.Value), "", fldDen.Value), SQLBinaryToBoolean(fldConv.Value), SQLBinaryToBoolean(fldInvi.Value), fldIdioma.Value, lIndice
            End If
            rs.MoveNext
            lIndice = lIndice + 1
            
        Wend
        
    Else
        
        While Not rs.eof
            'Me.Add rdores("COD"), rdores("DEN"), rdores("CONV")
            'rdores.MoveNext
            ' a�ado el fldInvi al Me.Add Kepa Mu�oz 20/01/2006
            If Not IsMissing(Idi) Then
                Me.Add fldCod.Value, IIf(IsNull(fldDen.Value), "", fldDen.Value), SQLBinaryToBoolean(fldConv.Value), SQLBinaryToBoolean(fldInvi.Value), Idi
            Else
                ' Dejamos en manos del programador cuando asignar la conexion al obejto usuario
                Me.Add fldCod.Value, IIf(IsNull(fldDen.Value), "", fldDen.Value), SQLBinaryToBoolean(fldConv.Value), SQLBinaryToBoolean(fldInvi.Value), fldIdioma.Value
            End If
            rs.MoveNext

        Wend
    End If
    
    rs.Close
    Set rs = Nothing
    Set fldCod = Nothing
    Set fldDen = Nothing
    Set fldConv = Nothing
    Set fldIdioma = Nothing
    
    Exit Sub
      
End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CRoles", "CargarTodosLosRoles", ERR, Erl)
      Exit Sub
   End If
End Sub
Public Function Add(ByVal Cod As String, ByVal Den As String, ByVal Conv As Boolean, ByVal Invi As Boolean, ByVal Idi As String, Optional ByVal varIndice As Variant) As CRol
    ' A�ado la funci�n ADD el parametro Invi para a�adir invitado Kepa Mu�oz 20/01/2006
    'create a new object
    Dim objnewmember As CRol
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New CRol
   
    objnewmember.Cod = Cod
    objnewmember.Den = Den
    objnewmember.Conv = Conv
    objnewmember.Invi = Invi ' recojo el valor de Invi y lo dejo en el objeto Kepa Mu�oz 20/01/2006
    objnewmember.Idioma = Idi
    
    
    Set objnewmember.Conexion = mvarConexion
    Set objnewmember.ConexionServer = mvarConexionServer
        
    If Not IsMissing(varIndice) And Not IsNull(varIndice) Then
        objnewmember.Indice = varIndice
        mCol.Add objnewmember, CStr(varIndice)
    Else
        'set the properties passed into the method
       mCol.Add objnewmember, CStr(Cod)
    End If
    
    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CRoles", "Add", ERR, Erl)
      Exit Function
   End If

End Function

Public Property Get Item(vntIndexKey As Variant) As CRol
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Friend Property Set Conexion(ByVal con As CConexion)
Set mvarConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = mvarConexion
End Property
Friend Property Get ConexionServer() As CConexionDeUseServer
Set ConexionServer = mvarConexionServer
End Property

Friend Property Set ConexionServer(ByVal con As CConexionDeUseServer)
Set mvarConexionServer = con
End Property
Public Property Get Count() As Long
    If mCol Is Nothing Then
        Count = 0
    Else
         Count = mCol.Count
    End If
End Property

Public Sub Remove(vntIndexKey As Variant)
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    mCol.Remove vntIndexKey
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CRoles", "Remove", ERR, Erl)
      Exit Sub
   End If
End Sub

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
     Set NewEnum = mCol.[_NewEnum]
End Property

Private Sub Class_Initialize()
    Set mCol = New Collection
End Sub

Private Sub Class_Terminate()
    'destroys collection when this class is terminated
    Set mCol = Nothing
    Set mvarConexion = Nothing
    Set mvarConexionServer = Nothing
End Sub
Public Function DevolverLosCodigos() As TipoDatosCombo
   
   Dim Codigos As TipoDatosCombo
   Dim iCont As Integer
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
   ReDim Codigos.Cod(mCol.Count)
   ReDim Codigos.Den(mCol.Count)
   
    For iCont = 0 To mCol.Count - 1
        
        Codigos.Cod(iCont) = mCol(iCont + 1).Cod
        Codigos.Den(iCont) = mCol(iCont + 1).Den
    
    Next

   DevolverLosCodigos = Codigos
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CRoles", "DevolverLosCodigos", ERR, Erl)
      Exit Function
   End If
   
End Function
Public Property Get eof() As Boolean
    eof = mvarEOF
End Property

Public Property Let eof(ByVal vNewValue As Boolean)
    mvarEOF = vNewValue
End Property
