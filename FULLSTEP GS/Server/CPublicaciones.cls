VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CPublicaciones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CPublicaciones **********************************
'*             Autor : Javier Arana
'*             Creada : 29/3/99
'****************************************************************


Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum
Private mCol As Collection
Private mvarConexion As CConexion
Public Function Add(ByVal CodProve As String, ByVal DenProve As String, ByVal Activa As Boolean, Optional ByVal FechaActivacion As Variant, Optional ByVal varIndice As Variant, _
                        Optional ByVal PublicacionObjetivoActiva As Boolean, Optional ByVal FechaPublicacionObjetivo As Variant, Optional ByVal numObjetivos As Integer, _
                        Optional ByVal tieneObjetivosNuevos As Boolean) As CPublicacion

    'create a new object
    Dim objnewmember As CPublicacion
    Dim sCod As String
    
    Set objnewmember = New CPublicacion
   
    objnewmember.CodProve = CodProve
    objnewmember.DenProve = DenProve
    objnewmember.Activa = Activa
    Set objnewmember.Conexion = mvarConexion
    
    If IsMissing(FechaActivacion) Then
        objnewmember.FechaActivacion = Null
    Else
        If IsNull(FechaActivacion) Then
            objnewmember.FechaActivacion = Null
        Else
            objnewmember.FechaActivacion = Format(CDate(FechaActivacion), "Short Date")
        End If
    End If
    
    objnewmember.PublicacionObjetivoActiva = PublicacionObjetivoActiva
    
    If IsNull(FechaPublicacionObjetivo) Then
        objnewmember.FechaPublicacionObjetivo = Null
    Else
        If IsNull(FechaActivacion) Then
            objnewmember.FechaPublicacionObjetivo = Null
        Else
            objnewmember.FechaPublicacionObjetivo = Format(CDate(FechaPublicacionObjetivo), "Short Date")
        End If
    End If
    
    objnewmember.numObjetivos = numObjetivos
    objnewmember.tieneObjetivosNuevos = tieneObjetivosNuevos
    
    If Not IsMissing(varIndice) And Not IsNull(varIndice) Then
        objnewmember.Indice = varIndice
        mCol.Add objnewmember, CStr(varIndice)
    Else
        mCol.Add objnewmember, CodProve
    End If
    
    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing


End Function

Public Property Get Item(vntIndexKey As Variant) As CPublicacion
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Friend Property Set Conexion(ByVal con As CConexion)
Set mvarConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = mvarConexion
End Property


Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
     Count = mCol.Count
End If


End Property


Public Sub Remove(vntIndexKey As Variant)
On Error GoTo NoSeEncuentra:

    mCol.Remove vntIndexKey
    Exit Sub

NoSeEncuentra:
    
End Sub


Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
     Set NewEnum = mCol.[_NewEnum]
End Property


Private Sub Class_Initialize()
   

    Set mCol = New Collection
End Sub


Private Sub Class_Terminate()
    'destroys collection when this class is terminated
    Set mCol = Nothing
    Set mvarConexion = Nothing
End Sub



