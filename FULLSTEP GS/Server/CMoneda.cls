VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CMoneda"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True

''' *** Clase: CMoneda
''' *** Creacion: 9/09/1998 (Javier Arana)
''' *** Ultima revision: 31/05/1999 (Alfredo Magallon)


Option Explicit

Implements IBaseDatos

''' Variables privadas con la informacion de una moneda
Private m_oConexion As CConexion
Private m_sCod As String
Private m_oDen As CMultiidiomas
Private m_dblEQUIV As Double
Private m_bEQ_ACT As Boolean
Private m_vFecAct As Variant
Private m_iID As Integer
Private m_vEstIntegracion As Variant

' Variables para el control de cambios del Log e Integración
Private m_sUsuario As String 'Usuario que realiza los cambios.
Private m_udtOrigen As OrigenIntegracion 'Origen del mvto en la tabla de LOG

''' Control de errores

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

Private m_adores As adodb.Recordset

''' Indice de la moneda en la coleccion

Private m_lIndice As Long
Private Function GrabarEnLog() As Boolean
'***********************************************************************************
'*** Descripción: Nos indica si hay que grabar o no el cambio en el log de       ***
'***              monedas (tabla LOG_MON) y carga en la variable                 ***
'***              del módulo m_udtOrigen el origen del movimiento                ***
'*** Parametros:  Ninguno                                                        ***
'*** Devuelve:    True (hay que grabar) False (No hay que grabar)                ***
'***********************************************************************************

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If gParametrosGenerales.gbACTIVLOG Or gParametrosIntegracion.gaExportar(EntidadIntegracion.Mon) Then
        If gParametrosGenerales.gbACTIVLOG And gParametrosIntegracion.gaExportar(EntidadIntegracion.Mon) Then
            m_udtOrigen = FSGSIntReg
        Else
            If gParametrosGenerales.gbACTIVLOG Then
                m_udtOrigen = FSGSReg
            Else
                m_udtOrigen = FSGSInt
            End If
        End If
        GrabarEnLog = True
    Else
        GrabarEnLog = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CMoneda", "GrabarEnLog", ERR, Erl)
        Exit Function
    End If

End Function

Friend Property Let Id(ByVal Data As Integer)
    Let m_iID = Data
End Property

Public Property Let equiv(ByVal vData As Double)

    ''' * Objetivo: Dar valor a la variable privada EQUIV
    ''' * Recibe: Equivalencia de la moneda respecto a la central
    ''' * Devuelve: Nada
    
    m_dblEQUIV = vData
    
End Property
Public Property Get equiv() As Double

    ''' * Objetivo: Devolver la variable privada EQUIV
    ''' * Recibe: Nada
    ''' * Devuelve: Equivalencia de la moneda respecto a la central

    equiv = m_dblEQUIV
    
End Property
Public Property Let eq_Act(ByVal vData As Boolean)

    ''' * Objetivo: Dar valor a la variable privada EQ_ACT
    ''' * Recibe: Equivalencia actualizable S/N
    ''' * Devuelve: Nada
    
    m_bEQ_ACT = vData
    
End Property
Public Property Get eq_Act() As Boolean

    ''' * Objetivo: Devolver la variable privada EQUIV
    ''' * Recibe: Nada
    ''' * Devuelve: Equivalencia actualizable S/N

    eq_Act = m_bEQ_ACT
    
End Property
Public Property Let FecAct(ByVal vData As Variant)

    ''' * Objetivo: Dar valor a la variable privada FECACT
    ''' * Recibe: Fecha de ultima actualizacion de la moneda
    ''' * Devuelve: Nada

    m_vFecAct = vData
    
End Property
Public Property Get FecAct() As Variant

    ''' * Objetivo: Devolver la variable privada FECACT
    ''' * Recibe: Nada
    ''' * Devuelve: Fecha de ultima actualizacion de la moneda

    FecAct = m_vFecAct
    
End Property
Public Property Get Indice() As Integer

    ''' * Objetivo: Devolver el indice de la moneda en la coleccion
    ''' * Recibe: Nada
    ''' * Devuelve: Indice de la moneda en la coleccion

    Indice = m_lIndice
    
End Property
Public Property Let EstadoIntegracion(ByVal vData As Variant)

    ''' * Objetivo: Dar valor al estado de integración de la moneda
    ''' * Recibe: el valor
    ''' * Devuelve: Nada

    m_vEstIntegracion = vData
    
End Property
Public Property Get EstadoIntegracion() As Variant

    ''' * Objetivo: Devolver el estado de integración de la moneda
    ''' * Recibe: Nada
    ''' * Devuelve: El estado de integración de la moneda

    EstadoIntegracion = m_vEstIntegracion
    
End Property
Public Property Let Indice(ByVal iInd As Integer)

    ''' * Objetivo: Dar valor al indice de la moneda en la coleccion
    ''' * Recibe: Indice de la moneda en la coleccion
    ''' * Devuelve: Nada

    m_lIndice = iInd
    
End Property

Friend Property Set Conexion(ByVal vData As CConexion)

    ''' * Objetivo: Dar valor a la conexion de la moneda
    ''' * Recibe: Conexion
    ''' * Devuelve: Nada

    Set m_oConexion = vData
    
End Property
 Friend Property Get Conexion() As CConexion

       ''' * Objetivo: Devolver la conexion de la moneda
       ''' * Recibe: Nada
       ''' * Devuelve: Conexion

       Set Conexion = m_oConexion
    
   End Property
   
Public Property Get Denominaciones() As CMultiidiomas

    ''' * Objetivo: Devolver la denominacion
    ''' * Recibe: Nada
    ''' * Devuelve: Conexion
    Set Denominaciones = m_oDen
    
End Property

Public Property Set Denominaciones(ByVal dato As CMultiidiomas)

    ''' * Objetivo: Dar valor al objeto Denominaciones
    ''' * Recibe: Codigo de la Unidad
    ''' * Devuelve: Nada
    Set m_oDen = dato
    
End Property

Public Property Let Cod(ByVal vData As String)

    ''' * Objetivo: Dar valor a la variable privada COD
    ''' * Recibe: Codigo de la moneda
    ''' * Devuelve: Nada

    m_sCod = vData
    
End Property
Public Property Let Usuario(ByVal vData As String)

    ''' * Objetivo: Dar valor a la variable privada Usuario
    ''' * Recibe: Codigo del usuario conectado
    ''' * Devuelve: Nada

    m_sUsuario = vData
    
End Property

Public Property Get Cod() As String

    ''' * Objetivo: Devolver la variable privada COD
    ''' * Recibe: Nada
    ''' * Devuelve: Codigo de la moneda

    Cod = m_sCod
    
End Property


 Private Sub Class_Terminate()
''' * Objetivo: Limpiar la memoria
    
     Set m_oConexion = Nothing
    
 End Sub

Private Function IBaseDatos_AnyadirABaseDatos() As TipoErrorSummit

End Function

Private Function IBaseDatos_CambiarCodigo(CodigoNuevo As Variant) As TipoErrorSummit

End Function
Private Sub IBaseDatos_CancelarEdicion()

End Sub
Private Function IBaseDatos_ComprobarExistenciaEnBaseDatos() As Boolean

End Function

Private Function IBaseDatos_EliminarDeBaseDatos() As TipoErrorSummit

End Function

Private Function IBaseDatos_FinalizarEdicionEliminando() As TipoErrorSummit

End Function

Private Function IBaseDatos_FinalizarEdicionModificando() As TipoErrorSummit

End Function

Private Function IBaseDatos_IniciarEdicion(Optional ByVal Bloquear As Boolean, Optional ByVal UsuarioBloqueo As String) As TipoErrorSummit

End Function
Private Function IBaseDatos_ModificarEnBaseDatos() As Boolean

    ''' ! No usado por ahora
    
End Function
