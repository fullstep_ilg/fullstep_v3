VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CJob"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CJob **********************************
'*             Autor : Encarni Prieto
'*             Creada : 3/11/2003
'****************************************************************

Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

Private m_adores As ADODB.Recordset
Private m_bEnabled As Boolean
Private m_bEnabledOrig As Boolean
Private m_sNombre As String 'local copy
Private m_vFechaAct As Variant
Private m_sScheduleName As String
Private m_udtTipo As TipoSchedule
Private m_udtTipoOrig As TipoSchedule
Private m_lIntervalo As Long 'Diario: cada x dias, Semanal: cada x semanas, Mensual: cada x meses
Private m_udtInterSemanal As Long 'Dias de la semana en que se ejecuta, combinando los valores de IntervaloSemanal
Private m_lIntervaloMensual As Integer 'Dia del mes en que se ejcuta 1-31 cada m_lIntervalo meses
Private m_udtIntervaloMensRelativo As IntervaloMensualRel 'Dias de la semana en que se ejecuta
Private m_udtRelativo As RelativoMensual 'Realitivo a 1�, 2�, ...
Private m_udtFrecuenciaDia As FrecuenciaDiaria 'Horas, minutos o una vez
Private m_oConexion As CConexion 'local copy
Private m_iIntervaloDia As Integer
Private m_dDiaStartTime As Date
Private m_dDiaEndTime As Date
Private m_dStart As Date
Private m_vEnd As Variant

Friend Property Set Conexion(ByVal vData As CConexion)
    Set m_oConexion = vData
End Property

Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property

Public Property Let FechaUltimaAct(ByVal vData As Variant)

    m_vFechaAct = vData
    
End Property
Public Property Get FechaUltimaAct() As Variant

    FechaUltimaAct = m_vFechaAct
    
End Property

Public Property Let Enabled(ByVal vData As Boolean)

    m_bEnabled = vData
    
End Property
Public Property Get Enabled() As Boolean

    Enabled = m_bEnabled
    
End Property

Public Property Let Nombre(ByVal vData As String)
    m_sNombre = vData
    
End Property
Public Property Get Nombre() As String

    Nombre = m_sNombre
    
End Property
Public Property Let Tipo(ByVal vData As TipoSchedule)

    m_udtTipo = vData
    
End Property
Public Property Get Tipo() As TipoSchedule
    Tipo = m_udtTipo
End Property


Public Property Get Intervalo() As Long
    Intervalo = m_lIntervalo
End Property
Public Property Let Intervalo(ByVal Cant As Long)
    m_lIntervalo = Cant
End Property
Public Property Get IntervSemanal() As Long
    IntervSemanal = m_udtInterSemanal
End Property
Public Property Let IntervSemanal(ByVal Dat As Long)
    m_udtInterSemanal = Dat
End Property

Public Property Get IntervMensual() As Integer
    IntervMensual = m_lIntervaloMensual
End Property
Public Property Let IntervMensual(ByVal Dat As Integer)
    m_lIntervaloMensual = Dat
End Property
Public Property Get IntervMensualRelativo() As IntervaloMensualRel
    IntervMensualRelativo = m_udtIntervaloMensRelativo
End Property
Public Property Let IntervMensualRelativo(ByVal Dat As IntervaloMensualRel)
    m_udtIntervaloMensRelativo = Dat
End Property
Public Property Get RelativoMens() As RelativoMensual
    RelativoMens = m_udtRelativo
End Property
Public Property Let RelativoMens(ByVal Dat As RelativoMensual)
    m_udtRelativo = Dat
End Property

Public Property Get Frecuencia() As FrecuenciaDiaria
    Frecuencia = m_udtFrecuenciaDia
End Property
Public Property Let Frecuencia(ByVal Dat As FrecuenciaDiaria)
    m_udtFrecuenciaDia = Dat
End Property

Public Property Get IntervDia() As Integer
    IntervDia = m_iIntervaloDia
End Property
Public Property Let IntervDia(ByVal Dat As Integer)
    m_iIntervaloDia = Dat
End Property

Public Property Get DiaHoraComienzo() As Date
    DiaHoraComienzo = m_dDiaStartTime
End Property
Public Property Let DiaHoraComienzo(ByVal Dat As Date)
    m_dDiaStartTime = Dat
End Property
Public Property Get DiaHoraFin() As Date
    DiaHoraFin = m_dDiaEndTime
End Property
Public Property Let DiaHoraFin(ByVal Dat As Date)
    m_dDiaEndTime = Dat
End Property

Public Property Get FechaComienzo() As Date
    FechaComienzo = m_dStart
End Property
Public Property Let FechaComienzo(ByVal Dat As Date)
    m_dStart = Dat
End Property
Public Property Get FechaFin() As Variant
    FechaFin = m_vEnd
End Property
Public Property Let FechaFin(ByVal Dat As Variant)
    m_vEnd = Dat
End Property


Public Function ModificarJob() As TipoErrorSummit
Dim sConsulta As String
Dim TESError As TipoErrorSummit
Dim AdoRes As ADODB.Recordset
Dim adocommAdo As ADODB.Command
Dim adoparamPar As ADODB.Parameter
Dim bTransaccion As Boolean
Dim sFecha As String
Dim lFecha As Long
Dim lReturnStatus As Long
'Dim sOwner As String
'
'********* Precondicion *******************
If m_oConexion Is Nothing Then
    Err.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CJob.ModificarJob", "No se ha establecido la conexion"
End If

'******************************************
    TESError.NumError = TESnoerror
    
On Error GoTo Error:
    
    If m_udtTipo = NoExiste Then
        ModificarJob = TESError
        Exit Function
    End If
    
    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
    
    'sOwner = LICDBLogin
    
    Set adocommAdo = New ADODB.Command
    Set adocommAdo.ActiveConnection = m_oConexion.ADOCon
    adocommAdo.CommandType = adCmdStoredProc
    adocommAdo.CommandText = "SP_AR_JOB_MODIF_DATOS"
    
    If m_udtTipoOrig = NoExiste Then
        Set adoparamPar = adocommAdo.CreateParameter("MODIF", adTinyInt, adParamInput, , 0)
        adocommAdo.Parameters.Append adoparamPar
        Set adoparamPar = adocommAdo.CreateParameter("ENAB", adTinyInt, adParamInput, , BooleanToSQLBinary(m_bEnabled))
        adocommAdo.Parameters.Append adoparamPar
        m_sNombre = "ACT_AHORROS_" & UCase(BaseDatosGS)
    Else
        Set adoparamPar = adocommAdo.CreateParameter("MODIF", adTinyInt, adParamInput, , 1)
        adocommAdo.Parameters.Append adoparamPar
        If m_bEnabled = m_bEnabledOrig Then
            Set adoparamPar = adocommAdo.CreateParameter("ENAB", adTinyInt, adParamInput, , 2)
            adocommAdo.Parameters.Append adoparamPar
        Else
            Set adoparamPar = adocommAdo.CreateParameter("ENAB", adTinyInt, adParamInput, , BooleanToSQLBinary(m_bEnabled))
            adocommAdo.Parameters.Append adoparamPar
        End If
        
    End If
    Set adoparamPar = adocommAdo.CreateParameter("NOMBRE", adVarChar, adParamInput, 50, m_sNombre)
    adocommAdo.Parameters.Append adoparamPar
'    Set adoparamPar = adocommAdo.CreateParameter("PROPI", adVarChar, adParamInput, 50, sOwner)
'    adocommAdo.Parameters.Append adoparamPar
    Set adoparamPar = adocommAdo.CreateParameter("TIPO", adInteger, adParamInput, , m_udtTipo)
    adocommAdo.Parameters.Append adoparamPar
    Select Case m_udtTipo
    Case Diario
        Set adoparamPar = adocommAdo.CreateParameter("INTERVALO", adInteger, adParamInput, , m_lIntervalo)
        adocommAdo.Parameters.Append adoparamPar
        Set adoparamPar = adocommAdo.CreateParameter("RECFACTOR", adInteger, adParamInput, , 0)
        adocommAdo.Parameters.Append adoparamPar
        
    Case Semanal
        Set adoparamPar = adocommAdo.CreateParameter("INTERVALO", adInteger, adParamInput, , m_udtInterSemanal)
        adocommAdo.Parameters.Append adoparamPar
        Set adoparamPar = adocommAdo.CreateParameter("RECFACTOR", adInteger, adParamInput, , m_lIntervalo)
        adocommAdo.Parameters.Append adoparamPar
    
    Case Mensual
        Set adoparamPar = adocommAdo.CreateParameter("INTERVALO", adInteger, adParamInput, , m_lIntervaloMensual)
        adocommAdo.Parameters.Append adoparamPar
        Set adoparamPar = adocommAdo.CreateParameter("RECFACTOR", adInteger, adParamInput, , m_lIntervalo)
        adocommAdo.Parameters.Append adoparamPar
    
    Case MensualRelativo
        Set adoparamPar = adocommAdo.CreateParameter("INTERVALO", adInteger, adParamInput, , m_udtIntervaloMensRelativo)
        adocommAdo.Parameters.Append adoparamPar
        Set adoparamPar = adocommAdo.CreateParameter("RECFACTOR", adInteger, adParamInput, , m_lIntervalo)
        adocommAdo.Parameters.Append adoparamPar
    
    End Select
    Set adoparamPar = adocommAdo.CreateParameter("SUBDIA", adInteger, adParamInput, , m_udtFrecuenciaDia)
    adocommAdo.Parameters.Append adoparamPar
    Set adoparamPar = adocommAdo.CreateParameter("FRECSUBDIA", adInteger, adParamInput, , m_iIntervaloDia)
    adocommAdo.Parameters.Append adoparamPar
    Set adoparamPar = adocommAdo.CreateParameter("RELATIVO", adInteger, adParamInput, , m_udtRelativo)
    adocommAdo.Parameters.Append adoparamPar
    sFecha = Format(m_dStart, "YYYYMMDD")
    lFecha = CLng(sFecha)
    Set adoparamPar = adocommAdo.CreateParameter("FECHAINI", adInteger, adParamInput, , lFecha)
    adocommAdo.Parameters.Append adoparamPar
    sFecha = Format(m_dDiaStartTime, "HHMMSS")
    lFecha = CLng(sFecha)
    Set adoparamPar = adocommAdo.CreateParameter("TIMEINI", adInteger, adParamInput, , lFecha)
    adocommAdo.Parameters.Append adoparamPar
                    
    Set adoparamPar = adocommAdo.CreateParameter("RET_STAT", adInteger, adParamOutput)
    adocommAdo.Parameters.Append adoparamPar
                    
    ''' Ejecutar la SP
    adocommAdo.Execute
    
    If m_oConexion.ADOCon.Errors.Count > 0 Then
        GoTo Error
    End If
    lReturnStatus = adocommAdo.Parameters("RET_STAT").Value
        
    Set adocommAdo.ActiveConnection = Nothing
    Set adocommAdo = Nothing
    If lReturnStatus <> 0 Then
        m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
        TESError.NumError = TESOtroerror
    Else
        m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
        m_udtTipoOrig = m_udtTipo
        m_bEnabledOrig = m_bEnabled
    End If
    ModificarJob = TESError

    Exit Function

Error:

    ModificarJob = basErrores.TratarError(m_oConexion.ADOCon.Errors)

    m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"


End Function

''' <summary>Lectura de datos</summary>
''' <remarks>Llamada desde: FSGSClient</remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>
''' <revision>LTG 09/01/2012</revision>

Public Sub LeerDatos()
    Dim rs As ADODB.Recordset
    Dim adocom As ADODB.Command
    Dim oParam As ADODB.Parameter
    Dim sConsulta As String
    Dim sFecha As String
    Dim sVersion As String
        
    '********* Precondicion **************************************
    If m_oConexion Is Nothing Then
        Err.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CJob.LeerDatos", "No se ha establecido la conexion"
        Exit Sub
    End If
    '*************************************************************
    
    On Error GoTo Error
    
        Set rs = New ADODB.Recordset
        rs.Open "SELECT FEC_ACT_AHORROS FROM PARGEN_DEF WITH (NOLOCK)", m_oConexion.ADOCon, adOpenKeyset, adLockReadOnly
        If Not rs.eof Then
            m_vFechaAct = rs(0).Value
        End If
        rs.Close
        Set rs = Nothing
        
        
        Set rs = New ADODB.Recordset
        rs.Open "SELECT @@version ", m_oConexion.ADOCon, adOpenKeyset, adLockReadOnly
        If Not rs.eof Then
            sVersion = rs(0).Value
        End If
        rs.Close
        Set rs = Nothing
        sVersion = Replace(Left(sVersion, 26), " ", "")
        m_sNombre = "ACT_AHORROS_" & UCase(BaseDatosGS)
        
        'Leo los datos del Schedule
        Set adocom = New ADODB.Command
        Set adocom.ActiveConnection = m_oConexion.ADOCon
        
        adocom.CommandType = adCmdStoredProc
        adocom.CommandText = "SP_AR_JOB_LEER_DATOS"
        Set oParam = adocom.CreateParameter("BD", adVarChar, adParamInput, 50, UCase(BaseDatosGS))
        adocom.Parameters.Append oParam
                
        ''' Ejecutar la SP
        Set rs = adocom.Execute
        If m_oConexion.ADOCon.Errors.Count > 0 Then
            GoTo Error
        End If

        If Not rs Is Nothing Then
            If sVersion = "MicrosoftSQLServer2000" Then
                Set rs = rs.NextRecordset
            End If
            m_bEnabled = SQLBinaryToBoolean(rs("enabled").Value)
            Set rs = rs.NextRecordset
            Set rs = rs.NextRecordset
            If Not rs.eof Then
                m_sScheduleName = rs("schedule_name").Value
                m_udtTipo = rs("freq_type").Value
                Select Case m_udtTipo
                Case Diario
                    m_lIntervalo = rs("freq_interval").Value
                Case Semanal
                    m_udtInterSemanal = rs("freq_interval").Value
                    m_lIntervalo = rs("freq_recurrence_factor").Value
                Case Mensual
                    m_lIntervaloMensual = rs("freq_interval").Value
                    m_lIntervalo = rs("freq_recurrence_factor").Value
                Case MensualRelativo
                    m_udtIntervaloMensRelativo = rs("freq_interval").Value
                    m_udtRelativo = rs("freq_relative_interval").Value
                    m_lIntervalo = rs("freq_recurrence_factor").Value
                End Select
                m_udtFrecuenciaDia = rs("freq_subday_type").Value
                m_iIntervaloDia = rs("freq_subday_interval").Value
                sFecha = rs("active_start_time").Value
                While Len(sFecha) < 6
                    sFecha = "0" & sFecha
                Wend
                m_dDiaStartTime = TimeSerial(Left(sFecha, 2), Mid(sFecha, 3, 2), Right(sFecha, 2))
                sFecha = rs("active_end_time").Value
                While Len(sFecha) < 6
                    sFecha = "0" & sFecha
                Wend
                m_dDiaEndTime = TimeSerial(Left(sFecha, 2), Mid(sFecha, 3, 2), Right(sFecha, 2))
                sFecha = rs("active_start_date").Value
                m_dStart = DateSerial(Left(sFecha, 4), Mid(sFecha, 5, 2), Right(sFecha, 2))
                If rs("active_end_date").Value >= 99991231 Then
                    m_vEnd = Null
                Else
                    sFecha = rs("active_end_date").Value
                    m_vEnd = DateSerial(Left(sFecha, 4), Mid(sFecha, 5, 2), Right(sFecha, 2))
                End If
            End If
            rs.Close
            Set rs = Nothing
        End If
        Set adocom.ActiveConnection = Nothing
        Set adocom = Nothing
        m_udtTipoOrig = m_udtTipo
        m_bEnabledOrig = m_bEnabled
        Exit Sub
Error:
    
    If m_oConexion.ADOCon.Errors(0).NativeError = 14262 Then
            m_bEnabled = False
            m_udtTipo = NoExiste
            m_lIntervalo = 1
            m_udtInterSemanal = IntervaloMensualRel.MDomingo
            m_udtFrecuenciaDia = FUnaVez
            m_iIntervaloDia = 1
            m_dDiaStartTime = TimeSerial(0, 0, 0)
            m_dDiaEndTime = TimeSerial(23, 59, 59)
            m_dStart = Date
            m_vEnd = Null
            Set adocom.ActiveConnection = Nothing
            Set adocom = Nothing
            m_udtTipoOrig = m_udtTipo
            m_bEnabledOrig = m_bEnabled
    Else
        TratarError m_oConexion.ADOCon.Errors
    End If
    
End Sub



Private Sub Class_Terminate()
    Set m_oConexion = Nothing
End Sub



