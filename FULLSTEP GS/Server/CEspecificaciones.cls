VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CEspecificaciones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CEspecificaciones **********************************
'*             Autor : Javier Arana
'*             Creada : 2/3/98
'****************************************************************



Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum

Private mCol As Collection 'Contendra las especificaciones de un proceso o item
Private m_oConexion As CConexion

Public Property Get Item(vntIndexKey As Variant) As CEspecificacion
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Friend Property Set Conexion(ByVal con As CConexion)
    Set m_oConexion = con
End Property

Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property

Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
     Count = mCol.Count
End If


End Property

Public Function Add(ByVal Nombre As String, ByVal Fecha As Date, ByVal Id As Long, Optional ByVal oProceso As cProceso, _
                    Optional ByVal oItem As CItem, Optional ByVal Comentario As Variant, Optional ByVal varIndice As Variant, _
                    Optional ByVal oArticulo As CArticulo, Optional ByVal oProveedor As CProveedor, Optional ByVal oGrupo As CGrupo, _
                    Optional ByVal varRuta As Variant, Optional ByVal oPlantilla As CPlantilla, Optional ByVal DataSize As Variant, Optional ByVal udtTipo As Variant, Optional ByVal IdProve As Long) As CEspecificacion
    'create a new object
    Dim sCod As String
    Dim objnewmember As CEspecificacion
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New CEspecificacion
   
    If Not IsMissing(oProceso) Then
        Set objnewmember.Proceso = oProceso
    End If
    
    If Not IsMissing(oItem) Then
        Set objnewmember.Item = oItem
    End If
    
    If Not IsMissing(oArticulo) Then
        Set objnewmember.Articulo = oArticulo
    End If
    
    If Not IsMissing(oProveedor) Then
        Set objnewmember.Proveedor = oProveedor
    End If
    
    If Not IsMissing(oGrupo) Then
        Set objnewmember.Grupo = oGrupo
    End If
    
    If Not IsMissing(oPlantilla) Then
        Set objnewmember.Plantilla = oPlantilla
    End If
    
    objnewmember.Id = Id
    objnewmember.Nombre = Nombre
    If IsMissing(Comentario) Then
        objnewmember.Comentario = Null
    Else
        objnewmember.Comentario = Comentario
    End If
    If Not IsMissing(varRuta) Then
        objnewmember.Ruta = varRuta
    Else
        objnewmember.Ruta = Null
    End If

    objnewmember.Fecha = Fecha
    
    If IsMissing(DataSize) Then
        objnewmember.DataSize = 0
    Else
        objnewmember.DataSize = NullToDbl0(DataSize)
    End If
    
    If IsMissing(udtTipo) Then
        objnewmember.TipoEsp = Null
    Else
        objnewmember.TipoEsp = udtTipo
    End If
    
    objnewmember.Id_ProveEsp = IdProve
    Set objnewmember.Conexion = m_oConexion
    
    If Not IsMissing(varIndice) And Not IsNull(varIndice) Then
        objnewmember.Indice = varIndice
        mCol.Add objnewmember, CStr(varIndice)
    Else
    'set the properties passed into the method
       mCol.Add objnewmember, CStr(Id)
    End If
    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CEspecificaciones", "Add", ERR, Erl)
        Exit Function
    End If

End Function

Public Sub addEspecificacion(ByRef Esp As CEspecificacion)
    If Not Existe(Esp.Id) Then
        mCol.Add Esp, CStr(Esp.Id)
    End If
End Sub

Public Function Existe(ByVal Index As Variant) As Boolean
    Dim oEspecificacion As CEspecificacion    '
    On Error Resume Next
    '
    Set oEspecificacion = mCol(Index)
    ' si se produce un error es que no existe ese elemento
    If ERR.Number <> 0 Then
        Existe = False
    Else
        Existe = True
    End If
End Function

Public Sub Remove(vntIndexKey As Variant)
    mCol.Remove CStr(vntIndexKey)
End Sub


Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
     Set NewEnum = mCol.[_NewEnum]
End Property


Private Sub Class_Initialize()
 

    Set mCol = New Collection
End Sub


Private Sub Class_Terminate()
    'EspProceroys collection when this class is terminated
    Set mCol = Nothing
    Set m_oConexion = Nothing
End Sub

Public Function DevolverLosCodigos() As TipoDatosCombo
   
   Dim udtCodigos As TipoDatosCombo
   Dim iCont As Integer
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
   ReDim udtCodigos.Cod(mCol.Count)
   ReDim udtCodigos.Den(mCol.Count)
   
    For iCont = 0 To mCol.Count - 1
        
        udtCodigos.Cod(iCont) = mCol(iCont + 1).Cod
        udtCodigos.Den(iCont) = mCol(iCont + 1).Den
    
    Next

   DevolverLosCodigos = udtCodigos
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CEspecificaciones", "DevolverLosCodigos", ERR, Erl)
        Exit Function
    End If
   
End Function


