VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CPresPorMaterial"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
''' *** Clase: CPresPorMaterial
''' *** Creacion: 3/2/1999 (Javier Arana)

Option Explicit

Implements IBaseDatos

''' Variables privadas con la informacion de una PresPorMaterial
Private m_vAnyo As Variant ' Si es null significa que no hay presupuesto establecido para el material correspondiente
Private m_vCod As String ' Contendr� el anyo + la ruta del material al que esta asociado
Private m_vDen As String ' Contendr� la denominacion de material al que esta asociado
Private m_vPresupuesto As Variant
Private m_vObjetivo As Variant
Private m_sCodGMN1 As String
Private m_sCodGMN2 As String
Private m_sCodGMN3 As String
Private m_sCodGMN4 As String
Private m_vCantidad As Variant
Private m_vArt As Variant
Private m_iPresDirecta As Integer

''' Conexion

Private m_oConexion As CConexion
Private m_oConexionServer As CConexionDeUseServer

''' Recordset para el PresPorMaterial a editar
Private m_adores As adodb.Recordset


''' Indice de el PresPorMaterial en la coleccion

Private m_vIndice As Long

''' Variables para el control de cambios del Log e Integraci�n
Private m_sUsuario As String             'Usuario que realiza los cambios.
Private m_udtOrigen As OrigenIntegracion 'Origen del mvto en la tabla de LOG


Public Property Get Indice() As Integer

    ''' * Objetivo: Devolver el indice de la PresPorMaterial en la coleccion
    ''' * Recibe: Nada
    ''' * Devuelve: Indice de la PresPorMaterial en la coleccion

    Indice = m_vIndice
    
End Property
Public Property Let Indice(ByVal iInd As Integer)

    ''' * Objetivo: Dar valor al indice de la PresPorMaterial en la coleccion
    ''' * Recibe: Indice de la PresPorMaterial en la coleccion
    ''' * Devuelve: Nada

    m_vIndice = iInd
    
End Property
Public Property Get Presupuesto() As Variant

    ''' * Objetivo: Devolver el indice de la PresPorMaterial en la coleccion
    ''' * Recibe: Nada
    ''' * Devuelve: Indice de la PresPorMaterial en la coleccion

    Presupuesto = m_vPresupuesto
    
End Property
Public Property Let Presupuesto(ByVal vVal As Variant)

    ''' * Objetivo: Dar valor al indice de la PresPorMaterial en la coleccion
    ''' * Recibe: Indice de la PresPorMaterial en la coleccion
    ''' * Devuelve: Nada

    m_vPresupuesto = vVal
    
End Property
Public Property Get Objetivo() As Variant

    ''' * Objetivo: Devolver el indice de la PresPorMaterial en la coleccion
    ''' * Recibe: Nada
    ''' * Devuelve: Indice de la PresPorMaterial en la coleccion

    Objetivo = m_vObjetivo
    
End Property
Public Property Let Objetivo(ByVal vVal As Variant)

    ''' * Objetivo: Dar valor al indice de la PresPorMaterial en la coleccion
    ''' * Recibe: Indice de la PresPorMaterial en la coleccion
    ''' * Devuelve: Nada

    m_vObjetivo = vVal
    
End Property
Public Property Get Anyo() As Variant

    ''' * Objetivo: Devolver el indice de la PresPorMaterial en la coleccion
    ''' * Recibe: Nada
    ''' * Devuelve: Indice de la PresPorMaterial en la coleccion

    Anyo = m_vAnyo
    
End Property
Public Property Let Anyo(ByVal vVal As Variant)

    ''' * Objetivo: Dar valor al indice de la PresPorMaterial en la coleccion
    ''' * Recibe: Indice de la PresPorMaterial en la coleccion
    ''' * Devuelve: Nada

    m_vAnyo = vVal
    
End Property

Friend Property Set Conexion(ByVal vData As CConexion)

    ''' * Objetivo: Dar valor a la conexion de la PresPorMaterial
    ''' * Recibe: Conexion
    ''' * Devuelve: Nada

    Set m_oConexion = vData
    
End Property
Friend Property Get Conexion() As CConexion

    ''' * Objetivo: Devolver la conexion de la PresPorMaterial
    ''' * Recibe: Nada
    ''' * Devuelve: Conexion

    Set Conexion = m_oConexion
    
End Property
Private Sub ConectarDeUseServer()
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If m_oConexionServer.ADOSummitCon Is Nothing Then
        Set m_oConexionServer.ADOSummitCon = New adodb.Connection
        m_oConexionServer.ADOSummitCon.Open "Provider=SQLOLEDB.1;Auto Translate=FALSE;Server=" & basParametros.ServidorGS & ";Database=" & basParametros.BaseDatosGS & ";", LICDBLogin, LICDBContra
        m_oConexionServer.ADOSummitCon.CursorLocation = adUseServer
        m_oConexionServer.ADOSummitCon.CommandTimeout = 120
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CPresPorMaterial", "ConectarDeUseServer", ERR, Erl)
      Exit Sub
   End If
End Sub

Friend Property Get ConexionServer() As CConexionDeUseServer
Set ConexionServer = m_oConexionServer
End Property

Friend Property Set ConexionServer(ByVal con As CConexionDeUseServer)
Set m_oConexionServer = con
End Property
Public Property Let Den(ByVal vData As String)

    ''' * Objetivo: Dar valor a la variable privada DEN
    ''' * Recibe: Denominacion de la PresPorMaterial
    ''' * Devuelve: Nada

    m_vDen = vData
    
End Property
Public Property Get Den() As String

    ''' * Objetivo: Devolver la variable privada DEN
    ''' * Recibe: Nada
    ''' * Devuelve: Denominacion de la PresPorMaterial

    Den = m_vDen
    
End Property
Public Property Let Cod(ByVal vData As String)

    ''' * Objetivo: Dar valor a la variable privada COD
    ''' * Recibe: Codigo de la PresPorMaterial
    ''' * Devuelve: Nada

    m_vCod = vData
    
End Property
Public Property Get Cod() As String

    ''' * Objetivo: Devolver la variable privada COD
    ''' * Recibe: Nada
    ''' * Devuelve: Codigo de la PresPorMaterial

    Cod = m_vCod
    
End Property
Public Property Let CodGMN1(ByVal vData As String)

    ''' * Objetivo: Dar valor a la variable privada COD
    ''' * Recibe: Codigo de la PresPorMaterial
    ''' * Devuelve: Nada

    m_sCodGMN1 = vData
    
End Property
Public Property Get CodGMN1() As String

    ''' * Objetivo: Devolver la variable privada COD
    ''' * Recibe: Nada
    ''' * Devuelve: Codigo de la PresPorMaterial

    CodGMN1 = m_sCodGMN1
    
End Property
Public Property Let CodGMN2(ByVal vData As String)

    ''' * Objetivo: Dar valor a la variable privada COD
    ''' * Recibe: Codigo de la PresPorMaterial
    ''' * Devuelve: Nada

    m_sCodGMN2 = vData
    
End Property
Public Property Get CodGMN2() As String

    ''' * Objetivo: Devolver la variable privada COD
    ''' * Recibe: Nada
    ''' * Devuelve: Codigo de la PresPorMaterial

    CodGMN2 = m_sCodGMN2
    
End Property
Public Property Let CodGMN3(ByVal vData As String)

    ''' * Objetivo: Dar valor a la variable privada COD
    ''' * Recibe: Codigo de la PresPorMaterial
    ''' * Devuelve: Nada

    m_sCodGMN3 = vData
    
End Property
Public Property Get CodGMN3() As String

    ''' * Objetivo: Devolver la variable privada COD
    ''' * Recibe: Nada
    ''' * Devuelve: Codigo de la PresPorMaterial

    CodGMN3 = m_sCodGMN3
    
End Property
Public Property Let CodGMN4(ByVal vData As String)

    ''' * Objetivo: Dar valor a la variable privada COD
    ''' * Recibe: Codigo de la PresPorMaterial
    ''' * Devuelve: Nada

    m_sCodGMN4 = vData
    
End Property
Public Property Get CodGMN4() As String

    ''' * Objetivo: Devolver la variable privada COD
    ''' * Recibe: Nada
    ''' * Devuelve: Codigo de la PresPorMaterial

    CodGMN4 = m_sCodGMN4
    
End Property

Public Property Let Cantidad(ByVal vData As Variant)
    
    m_vCantidad = vData
    
End Property
Public Property Get Cantidad() As Variant
    
    Cantidad = m_vCantidad
    
End Property

Public Property Let Articulo(ByVal vData As Variant)
    
    m_vArt = vData
    
End Property
Public Property Get Articulo() As Variant
    
    Articulo = m_vArt
    
End Property

Public Property Let PresDirecta(ByVal vData As Integer)
    
    m_iPresDirecta = vData
    
End Property
Public Property Get PresDirecta() As Integer
    
    PresDirecta = m_iPresDirecta
    
End Property

Public Property Let Usuario(ByVal vData As String)

    ''' * Objetivo: Dar valor a la variable privada Usuario
    ''' * Recibe: Codigo del usuario conectado
    ''' * Devuelve: Nada

    m_sUsuario = vData
    
End Property


Private Sub Class_Terminate()

    ''' * Objetivo: Limpiar la memoria
    
    Set m_adores = Nothing
    Set m_oConexion = Nothing
    Set m_oConexionServer = Nothing
    
End Sub

Private Function IBaseDatos_AnyadirABaseDatos() As TipoErrorSummit

    ''' * Objetivo: Anyadir el presupuesto a la tabla
    ''' * Recibe: Nada
    ''' * Devuelve: TipoErrorSummit con el error si lo hay
    
    Dim sConsulta As String
    Dim TESError As TipoErrorSummit
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    TESError.NumError = TESnoerror
If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
                    
    ''' Ejecutar insercion
    Select Case basParametros.gParametrosGenerales.giNEM
        
        Case 1
        
        Case 2
        
        Case 3
        
        Case 4 'Cuatro niveles de materiales, usamos tabla PRES_MAT4 o PRES_ART
            If IsNull(m_vArt) Then
                'Presupuestaci�n directa:insertamos en pres_mat4
                sConsulta = "INSERT INTO PRES_MAT4 (ANYO,GMN1,GMN2,GMN3,GMN4,PRES,OBJ,PRESDIRECTA) VALUES (" & m_vAnyo & ",'" & DblQuote(m_sCodGMN1) & "','" & DblQuote(m_sCodGMN2) & "','" & DblQuote(m_sCodGMN3) & "','" & DblQuote(m_sCodGMN4) & "'," & DblToSQLFloat(m_vPresupuesto) & "," & DblToSQLFloat(m_vObjetivo) & ",1)"
                m_oConexion.ADOCon.Execute sConsulta
            Else
                'Presupuestaci�n a nivel de art�culo:insertamos en pres_art
                sConsulta = "INSERT INTO PRES_ART (ANYO,COD,CANT,PRES,OBJ) VALUES (" & m_vAnyo & ",'" & DblQuote(m_vArt) & "'," & DblToSQLFloat(m_vCantidad) & "," & DblToSQLFloat(m_vPresupuesto) & "," & DblToSQLFloat(m_vObjetivo) & ")"
                m_oConexion.ADOCon.Execute sConsulta
                
                'Inserta en la tabla LOG_PRES_ART
                If GrabarEnLog Then
                    sConsulta = "INSERT INTO LOG_PRES_ART (ACCION,ANYO,COD,CANT,PRES,OBJ,ORIGEN,USU) VALUES ('" & Accion_Modificacion & "',"
                    sConsulta = sConsulta & m_vAnyo & ",'" & DblQuote(m_vArt) & "'," & DblToSQLFloat(m_vCantidad) & "," & DblToSQLFloat(m_vPresupuesto) & "," & DblToSQLFloat(m_vObjetivo)
                    sConsulta = sConsulta & "," & m_udtOrigen & ",'" & DblQuote(m_sUsuario) & "')"
                    m_oConexion.ADOCon.Execute sConsulta
                    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
                End If
            End If
    End Select
    
    IBaseDatos_AnyadirABaseDatos = TESError
    
    Exit Function
    
Error_Cls:
     
    IBaseDatos_AnyadirABaseDatos = basErrores.TratarError(m_oConexion.ADOCon.Errors)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      'Call g_oErrores.TratarError("Class Module", "CPresPorMaterial", "IBaseDatos_AnyadirABaseDatos", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If
           
End Function
Private Function IBaseDatos_CambiarCodigo(CodigoNuevo As Variant) As TipoErrorSummit
    
End Function
Private Sub IBaseDatos_CancelarEdicion()

    ''' * Objetivo: Cancelar la edicion, cerrando el Resultset

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not m_adores Is Nothing Then
        m_adores.Close
        Set m_adores = Nothing
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CPresPorMaterial", "IBaseDatos_CancelarEdicion", ERR, Erl)
      Exit Sub
   End If

End Sub

''' <summary>
''' Comprueba la existencia del presupuesto en la tabla
''' </summary>
''' <returns>Boolean</returns>
''' <remarks>Llamada desde: FSGClient
''' Tiempo m�ximo: 1 sec </remarks>
''' <revision>JVS 03/01/2012</revision>
Private Function IBaseDatos_ComprobarExistenciaEnBaseDatos() As Boolean
    Dim sConsulta As String
    Dim AdoRes As New adodb.Recordset
   
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If IsNull(m_vArt) Then
        sConsulta = "SELECT ANYO,GMN1,GMN2,GMN3,GMN4 FROM PRES_MAT4 WITH(NOLOCK) WHERE ANYO=" & m_vAnyo & " AND GMN1='" & DblQuote(m_sCodGMN1) & "' AND GMN2='" & DblQuote(m_sCodGMN2) & "'"
        sConsulta = sConsulta & " AND GMN3='" & DblQuote(m_sCodGMN3) & "' AND GMN4='" & DblQuote(m_sCodGMN4) & "'"
    Else
        sConsulta = "SELECT ANYO,COD FROM PRES_ART WITH(NOLOCK) WHERE ANYO=" & m_vAnyo & " AND COD='" & DblQuote(m_vArt) & "'"
    End If

    Set AdoRes = New adodb.Recordset
    AdoRes.CursorLocation = adUseServer
    AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    If AdoRes.eof Then
        IBaseDatos_ComprobarExistenciaEnBaseDatos = False
    Else
        IBaseDatos_ComprobarExistenciaEnBaseDatos = True
    End If

    AdoRes.Close
    Set AdoRes = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CPresPorMaterial", "IBaseDatos_ComprobarExistenciaEnBaseDatos", ERR, Erl)
      Exit Function
   End If

End Function
Private Function IBaseDatos_EliminarDeBaseDatos() As TipoErrorSummit

    ''' * Objetivo: Eliminar la PresPorMaterial de la tabla
    ''' * Recibe: Nada
    ''' * Devuelve: TipoErrorSummit con el error si lo hay

    Dim TESError As TipoErrorSummit
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    TESError.NumError = TESnoerror
If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
        
    ''' Borrado
    
    Select Case basParametros.gParametrosGenerales.giNEM
        
        Case 1
        
        Case 2
        
        Case 3
        
        Case 4 'Cuatro niveles de materiales, usamos tabla PRES_MAT4
                    
                m_oConexion.ADOCon.Execute "DELETE FROM PRES_MAT4 WHERE ANYO='" & m_vAnyo & "AND GMN1='" & DblQuote(m_sCodGMN1) & "' AND GMN2='" & DblQuote(m_sCodGMN2) & "' AND GMN3='" & DblQuote(m_sCodGMN3) & "' AND GMN4='" & DblQuote(m_sCodGMN4) & "'"
        
    End Select
    
    
    IBaseDatos_EliminarDeBaseDatos = TESError
    
    Exit Function
        
Error_Cls:
        
    IBaseDatos_EliminarDeBaseDatos = basErrores.TratarError(m_oConexion.ADOCon.Errors)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      'Call g_oErrores.TratarError("Class Module", "CPresPorMaterial", "IBaseDatos_EliminarDeBaseDatos", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If
    
End Function

Private Function IBaseDatos_FinalizarEdicionEliminando() As TipoErrorSummit

    ''' ! No usado por ahora
    
End Function
Private Function IBaseDatos_FinalizarEdicionModificando() As TipoErrorSummit

    Dim TESError As TipoErrorSummit
    Dim sConsulta As String

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    TESError.NumError = TESnoerror

If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
    
    Select Case basParametros.gParametrosGenerales.giNEM
        
        Case 1
        
        Case 2
        
        Case 3
        
        Case 4 'Cuatro niveles de materiales, usamos tabla PRES_MAT4 o PRES_ART
            If IsNull(m_vArt) Then
                'Presupuestaci�n directa:insertamos en pres_mat4
                sConsulta = "UPDATE PRES_MAT4 SET PRES=" & DblToSQLFloat(m_vPresupuesto) & ",OBJ= " & DblToSQLFloat(m_vObjetivo)
                sConsulta = sConsulta & " ,PRESDIRECTA=1"
                sConsulta = sConsulta & " WHERE ANYO=" & m_vAnyo & " AND GMN1='" & DblQuote(m_sCodGMN1) & "' AND GMN2='" & DblQuote(m_sCodGMN2)
                sConsulta = sConsulta & "' AND GMN3='" & DblQuote(m_sCodGMN3) & "' AND GMN4='" & DblQuote(m_sCodGMN4) & "'"
                m_oConexion.ADOCon.Execute sConsulta
                
            Else
                'Presupuestaci�n a nivel de art�culo:insertamos en pres_art
                sConsulta = "UPDATE PRES_ART SET CANT=" & DblToSQLFloat(m_vCantidad) & ",PRES=" & DblToSQLFloat(m_vPresupuesto) & ",OBJ= " & DblToSQLFloat(m_vObjetivo)
                sConsulta = sConsulta & " WHERE ANYO=" & m_vAnyo & " AND COD='" & DblQuote(m_vArt) & "'"
                m_oConexion.ADOCon.Execute sConsulta
                
                'Inserta en la tabla LOG_PRES_ART
                If GrabarEnLog Then
                    sConsulta = "INSERT INTO LOG_PRES_ART (ACCION,ANYO,COD,CANT,PRES,OBJ,ORIGEN,USU) VALUES ('" & Accion_Modificacion & "',"
                    sConsulta = sConsulta & m_vAnyo & ",'" & DblQuote(m_vArt) & "'," & DblToSQLFloat(m_vCantidad) & "," & DblToSQLFloat(m_vPresupuesto) & "," & DblToSQLFloat(m_vObjetivo)
                    sConsulta = sConsulta & "," & m_udtOrigen & ",'" & DblQuote(m_sUsuario) & "')"
                    m_oConexion.ADOCon.Execute sConsulta
                    If m_oConexion.ADOCon.Errors.Count > 0 Then
                        GoTo Error_Cls
                    End If
                End If
    
            End If
            
    End Select
    
    
    IBaseDatos_FinalizarEdicionModificando = TESError
    Exit Function
        
Error_Cls:
        
    IBaseDatos_FinalizarEdicionModificando = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    
    If Not m_adores Is Nothing Then
        m_adores.Close
        Set m_adores = Nothing
    End If
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      'Call g_oErrores.TratarError("Class Module", "CPresPorMaterial", "IBaseDatos_FinalizarEdicionModificando", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If
    
End Function

''' <summary>
''' Esta funci�n inicia la edici�n del resulset
''' </summary>
'''<param optional name="Bloquear">Si se bloquea o no</param>
'''<param optional name="UsuarioBloqueo">Usuario del bloqueo</param>
''' <returns>TipoErrorSummit con el error si lo hay</returns>
''' <remarks>Llamada desde: FSGClient
''' Tiempo m�ximo: 1 sec </remarks>
''' <revision>JVS 03/01/2012</revision>
Private Function IBaseDatos_IniciarEdicion(Optional ByVal Bloquear As Boolean, Optional ByVal UsuarioBloqueo As String) As TipoErrorSummit

    ''' * Objetivo: Abrir la edicion en la PresPorMaterial actual
    ''' * Objetivo: creando un Recordset para ello
    ''' * Recibe: Nada
    ''' * Devuelve: TipoErrorSummit con el error si lo hay

    Dim TESError As TipoErrorSummit
    Dim sConsulta As String
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    TESError.NumError = TESnoerror
  
    
    ''' Abrir Resultset
    Select Case basParametros.gParametrosGenerales.giNEM
    
        Case 1
        
        Case 2
        
        Case 3
        
        Case 4
                'sin NOLOCK por ser un recordset en modo bloqueo
                sConsulta = "SELECT ANYO,GMN1,GMN2,GMN3,GMN4,PRES,OBJ,FECACT FROM PRES_MAT4 WHERE ANYO="
                
                sConsulta = sConsulta & CStr(m_vAnyo) & " AND GMN1='" & DblQuote(m_sCodGMN1) & "'" & " AND GMN2='" & DblQuote(m_sCodGMN2) & "' AND GMN3='" & DblQuote(m_sCodGMN3) & "' AND GMN4='" & DblQuote(m_sCodGMN4) & "'"
    
    End Select
    
    ConectarDeUseServer
    Set m_adores = New adodb.Recordset
    m_adores.Open sConsulta, m_oConexionServer.ADOSummitCon, adOpenKeyset, adLockOptimistic
    
    ''' Si no esta la PresPorMaterial, alguien la ha eliminado
    If m_adores.eof Then
        m_adores.Close
        Set m_adores = Nothing
        TESError.NumError = TESDatoEliminado
        TESError.Arg1 = 80
        IBaseDatos_IniciarEdicion = TESError
        Exit Function
    End If
    ''' Si hay datos diferentes, devolver la condicion
    If CDbl(basUtilidades.NullToDbl0(m_vPresupuesto)) <> CDbl(basUtilidades.NullToDbl0(m_adores("PRES"))) Then
        m_vPresupuesto = m_adores("PRES")
        TESError.NumError = TESInfActualModificada
        TESError.Arg1 = 80
    End If
    
    If CDbl(basUtilidades.NullToDbl0(m_vObjetivo)) <> CDbl(basUtilidades.NullToDbl0(m_adores("OBJ"))) Then
        m_vObjetivo = m_adores("OBJ")
        TESError.NumError = TESInfActualModificada
        TESError.Arg1 = "Objetivo"
    End If
        
    IBaseDatos_IniciarEdicion = TESError
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CPresPorMaterial", "IBaseDatos_IniciarEdicion", ERR, Erl)
      
      Exit Function
   End If

End Function
Private Function IBaseDatos_ModificarEnBaseDatos() As Boolean

    ''' ! No usado por ahora
    
End Function

''' <summary>
''' Obtiene el consumo para ese art�culo/unidad en el a�o anterior al seleccionado
''' </summary>
''' <returns>CANT</returns>
''' <remarks>Llamada desde: frmPRESArt.CargarConsumo;
''' Tiempo m�ximo: 1 sec </remarks>
''' <revision>JVS 03/01/2012</revision>
Public Function DevolverConsumoAnyoAnterior() As Variant
Dim rs As New adodb.Recordset
Dim sConsulta As String

'''    'Suma las cantidades adjudicadas para ese art�culo/unidad en el a�o anterior al seleccionado en frmPresArt
'''    sConsulta = "SELECT SUM(CANT) AS CANT FROM ART4_ADJ "
'''    sConsulta = sConsulta & " INNER JOIN PROCE ON ART4_ADJ.ANYO=PROCE.ANYO AND ART4_ADJ.GMN1=PROCE.GMN1 AND ART4_ADJ.PROCE=PROCE.COD"
'''    sConsulta = sConsulta & " WHERE YEAR(PROCE.FECULTREU)=" & m_vAnyo
'''    sConsulta = sConsulta & " AND ART4_ADJ.GMN1='" & DblQuote(m_sCodGMN1) & "' AND ART4_ADJ.GMN2='" & DblQuote(m_sCodGMN2) & "'"
'''    sConsulta = sConsulta & " AND ART4_ADJ.GMN3='" & DblQuote(m_sCodGMN3) & "' AND ART4_ADJ.GMN4='" & DblQuote(m_sCodGMN4) & "'"
'''    sConsulta = sConsulta & " AND ART4_ADJ.ART='" & DblQuote(m_vArt) & "' AND ART4_ADJ.UNI='" & DblQuote(sUni) & "'"
    
    'Obtiene el consumo para ese art�culo/unidad en el a�o anterior al seleccionado en frmPresArt
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sConsulta = "SELECT CANT FROM PRES_ART WITH(NOLOCK) "
    sConsulta = sConsulta & " WHERE ANYO=" & m_vAnyo & " AND COD='" & DblQuote(m_vArt) & "'"
    
    Set rs = New adodb.Recordset
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
                  
    If rs.eof Then
        DevolverConsumoAnyoAnterior = Null
    Else
        DevolverConsumoAnyoAnterior = rs.Fields("CANT").Value
    End If

    rs.Close
    Set rs = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CPresPorMaterial", "DevolverConsumoAnyoAnterior", ERR, Erl)
      Exit Function
   End If

End Function

''' <summary>
''' Obtiene la adjudicaci�n para ese art�culo/unidad en el a�o anterior al seleccionado
''' </summary>
''' <returns>PREC</returns>
''' <remarks>Llamada desde: frmPRESArt.CargarPresupuestos;
''' Tiempo m�ximo: 1 sec </remarks>
''' <revision>JVS 03/01/2012</revision>
Public Function DevolverUltimaAdjAnyoAnterior(ByVal sUni As String) As Variant
Dim rs As New adodb.Recordset
Dim sConsulta As String
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sConsulta = "SELECT TOP 1 PREC FROM ART4_ADJ WITH(NOLOCK) "
    sConsulta = sConsulta & " INNER JOIN PROCE WITH(NOLOCK) ON ART4_ADJ.ANYO=PROCE.ANYO AND ART4_ADJ.GMN1=PROCE.GMN1 AND ART4_ADJ.PROCE=PROCE.COD"
    sConsulta = sConsulta & " WHERE YEAR(PROCE.FECULTREU)=" & m_vAnyo
    sConsulta = sConsulta & " AND ART4_ADJ.ART='" & DblQuote(m_vArt) & "' AND ART4_ADJ.UNI='" & DblQuote(sUni) & "'"
    sConsulta = sConsulta & " ORDER BY PROCE.FECULTREU DESC,ART4_ADJ.ID DESC"
    
    Set rs = New adodb.Recordset
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
                  
    If rs.eof Then
        DevolverUltimaAdjAnyoAnterior = Null
    Else
        DevolverUltimaAdjAnyoAnterior = rs.Fields("PREC").Value
    End If

    rs.Close
    Set rs = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CPresPorMaterial", "DevolverUltimaAdjAnyoAnterior", ERR, Erl)
      Exit Function
   End If
            
End Function

''' <summary>
''' Obtiene suma de precio * cantidad para ese art�culo/unidad en el a�o anterior al seleccionado
''' </summary>
''' <returns>SUM(PREC*CANT)</returns>
''' <remarks>Llamada desde: frmPRESArt.CargarPresupuestos;
''' Tiempo m�ximo: 1 sec </remarks>
''' <revision>JVS 03/01/2012</revision>
Public Function DevolverMediaAdjAnyoAnterior(ByVal sUni As String) As Variant
Dim rs As New adodb.Recordset
Dim sConsulta As String

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sConsulta = "SELECT SUM(PREC*CANT) AS PRES,SUM(CANT) AS SUMCANT FROM ART4_ADJ WITH(NOLOCK) "
    sConsulta = sConsulta & " INNER JOIN PROCE WITH(NOLOCK) ON ART4_ADJ.ANYO=PROCE.ANYO AND ART4_ADJ.GMN1=PROCE.GMN1 AND ART4_ADJ.PROCE=PROCE.COD"
    sConsulta = sConsulta & " WHERE YEAR(PROCE.FECULTREU)=" & m_vAnyo
    sConsulta = sConsulta & " AND ART4_ADJ.ART='" & DblQuote(m_vArt) & "' AND ART4_ADJ.UNI='" & DblQuote(sUni) & "'"
        
    Set rs = New adodb.Recordset
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
                  
    If rs.eof Then
        DevolverMediaAdjAnyoAnterior = Null
    Else
        If IsNull(rs.Fields("SUMCANT").Value) Then
            DevolverMediaAdjAnyoAnterior = Null
        ElseIf rs.Fields("SUMCANT").Value = 0 Then
            DevolverMediaAdjAnyoAnterior = 0
        Else
            DevolverMediaAdjAnyoAnterior = NullToDbl0(rs.Fields("PRES").Value) / rs.Fields("SUMCANT").Value
        End If
    End If

    rs.Close
    Set rs = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CPresPorMaterial", "DevolverMediaAdjAnyoAnterior", ERR, Erl)
      Exit Function
   End If

End Function

Public Function DevolverPresupuestoAnyoActual() As Variant
Dim rs As New adodb.Recordset
Dim sConsulta As String

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sConsulta = "SELECT PRES FROM PRES_ART WITH (NOLOCK) "
    sConsulta = sConsulta & " WHERE ANYO=" & m_vAnyo
    sConsulta = sConsulta & " AND COD='" & DblQuote(m_vArt) & "'"
    
    Set rs = New adodb.Recordset
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
                  
    If rs.eof Then
        DevolverPresupuestoAnyoActual = Null
    Else
        DevolverPresupuestoAnyoActual = rs.Fields("PRES").Value
    End If

    rs.Close
    Set rs = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CPresPorMaterial", "DevolverPresupuestoAnyoActual", ERR, Erl)
      Exit Function
   End If

End Function

''' <summary>
''' Comprueba si hay registros en la tabla de PRES_ART para el a�o actual
''' </summary>
''' <returns>Boolean</returns>
''' <remarks>Llamada desde: frmPRESArt.sdbgUltraPres_BeforeCellUpdate;
''' Tiempo m�ximo: 1 sec </remarks>
''' <revision>JVS 03/01/2012</revision>
Public Function ExistenPresupuestosArticulos() As Boolean
Dim rs As New adodb.Recordset
Dim sConsulta As String

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sConsulta = "SELECT ANYO,COD FROM PRES_ART WITH(NOLOCK) "
    sConsulta = sConsulta & " WHERE ANYO=" & m_vAnyo
    sConsulta = sConsulta & " AND (PRES IS NOT NULL OR OBJ IS NOT NULL)"
    
    Set rs = New adodb.Recordset
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
                  
    If rs.eof Then
        ExistenPresupuestosArticulos = False
    Else
        ExistenPresupuestosArticulos = True
    End If

    rs.Close
    Set rs = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CPresPorMaterial", "ExistenPresupuestosArticulos", ERR, Erl)
      Exit Function
   End If

End Function


Private Function GrabarEnLog() As Boolean
'***********************************************************************************
'*** Descripci�n: Nos indica si hay que grabar o no el cambio en el log de       ***
'***              PRES_ART (tabla LOG_PRES_ART) y carga en la variable           ***
'***              del m�dulo m_udtOrigen el origen del movimiento                ***
'*** Parametros:  Ninguno                                                        ***
'*** Devuelve:    True (hay que grabar) False (No hay que grabar)                ***
'***********************************************************************************

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If gParametrosGenerales.gbACTIVLOG Or gParametrosIntegracion.gaExportar(EntidadIntegracion.PresArt) Then
        If gParametrosGenerales.gbACTIVLOG And gParametrosIntegracion.gaExportar(EntidadIntegracion.PresArt) Then
            m_udtOrigen = FSGSIntReg
        Else
            If gParametrosGenerales.gbACTIVLOG Then
                m_udtOrigen = FSGSReg
            Else
                m_udtOrigen = FSGSInt
            End If
        End If
        GrabarEnLog = True
    Else
        GrabarEnLog = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CPresPorMaterial", "GrabarEnLog", ERR, Erl)
      Exit Function
   End If

End Function

