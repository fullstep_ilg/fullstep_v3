VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CSesion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit
Private mvarConexion As CConexion
Private mvarId As Long 'local copy

Friend Property Get Conexion() As CConexion
    Set Conexion = mvarConexion
End Property
Friend Property Set Conexion(ByVal con As CConexion)

    Set mvarConexion = con
    
End Property
Public Property Let ID(ByVal vData As Long)
    mvarId = vData
End Property


Public Property Get ID() As Long
    ID = mvarId
End Property


''' <summary>
''' Registra la entrada en la session
''' </summary>
''' <param name="sCodUsu">Codigo Usuario</param>
''' <remarks>Llamada desde basSubMain.Main  Tiempo m�ximo:0,2</remarks>
''' <revision>JVS 09/01/2012</revision>
Public Function RegistrarNuevaSesion(ByVal sCodUsu As String) As TipoErrorSummit
    
Dim adoSessionRecordset As ADODB.Recordset
Dim TESError As TipoErrorSummit

On Error GoTo Error
    
    TESError.NumError = TESnoerror
    
    mvarConexion.ADOCon.Execute "INSERT INTO SES (USU,FIS) VALUES ('" & DblQuote(sCodUsu) & "', GETUTCDATE())"
    
    Set adoSessionRecordset = New ADODB.Recordset
    'CALIDAD Sin WITH(NOLOCK) en el FROM para asegurar la recuperaci�n del mayor valor de la tabla
    adoSessionRecordset.Open "SELECT MAX(ID) AS ID FROM SES WHERE USU='" & DblQuote(sCodUsu) & "'", mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    mvarId = adoSessionRecordset.Collect(0)
    
    adoSessionRecordset.Close
    Set adoSessionRecordset = Nothing
    RegistrarNuevaSesion = TESError
    
    Exit Function

Error:
        
    RegistrarNuevaSesion = basErrores.TratarError(mvarConexion.ADOCon.Errors)
    
End Function

''' <summary>
''' Registra el fin de la session
''' </summary>
''' <param name="sCodUsu">Codigo Usuario</param>
''' <remarks>Llamada desde MDI.Form_UNLOAD  Tiempo m�ximo:0</remarks>
Public Function DesRegistrarSesion() As TipoErrorSummit

Dim TESError As TipoErrorSummit

On Error GoTo Error
    
    TESError.NumError = TESnoerror
    
    'mvarConexion.ADOCon.Execute "DELETE FROM SES WHERE ID=" & mvarId
    mvarConexion.ADOCon.Execute "UPDATE SES SET FFS=GETUTCDATE() WHERE ID=" & mvarId
    DesRegistrarSesion = TESError
    
    Exit Function
    
Error:
    
    DesRegistrarSesion = basErrores.TratarError(mvarConexion.ADOCon.Errors)

End Function




