VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CUsuarios"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Collection" ,"CUsuario"
Attribute VB_Ext_KEY = "Member0" ,"CUsuario"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 513
    FalloDeIntegridadEnBD = 514
    UsuarioExternoNoIndicado = 515
    PersonaNoIndicada = 516
    CompradorNoindicado = 517
    TipoIncorrecto = 518
End Enum

Private mvarConexion As CConexion
Private mvarConexionServer As CConexionDeUseServer
Private mvarRecordset As adodb.Recordset

Private mCol As Collection

''' Revisado por: blp. Fecha: 30/10/2012
''' Crea una instancia CUsuario cuyas propiedades rellena con los par�metros recibidos y la a�ade al control CUSUARIOS
''' Cod: C�digo del usuario
''' pwd: pwd del usuario
''' Tipo: Tipo del usuario
''' Bloq: Bloq
''' varCodPerfil: C�digo del perfil
''' varDenPerfil: Descripci�n del perfil
''' DepDen: DepDEn
''' ComApe: Sin uso
''' ComNom: Sin uso
''' PerApe: Apellidos
''' PerNom: Nombre
''' UsuNom: Nombre
''' Eqpcod: CodEqp
''' EqpDen: DenEqp
''' bAccesoFSEP: Acceso a FSEP
''' vFecha: FecAct
''' bOtrosDest: Sin uso
''' DestCod: C�digo de Destino
''' DestDen: Descripci�n de destino
''' bAccesoFSWS: Acceso a FSWS
''' bPedidoLibre: Permiso para hacer pedido libre
''' vTSRutaLocal: TSRutaLocal
''' vTSRutaUnidad: TSRutaUnidad
''' vTSUnidad: TSUnidad
''' bAccesoFSGS: Acceso a FSGS
''' bFSWSPresUO: FSWSPresUO
''' bFSWSTrasladosUO: FSWSTrasladosUO
''' vFSWSSustitucion: FSWSSustitucion
''' vFSWSPres1: FSWSPres1
''' vFSWSPres2: FSWSPres2
''' vFSWSPres3: FSWSPres3
''' vFSWSPres4: FSWSPres4
''' bFSWSTrasladosDEP: FSWSTrasladosDEP
''' bFSWSRestrProvMat: FSWSRestrProvMat
''' bFSWSRestrMatUsu: FSWSRestrMatUsu
''' bFSWSRestrDestUO: FSWSRestrDestUO
''' bModifPrecios: ModifPrecios
''' bAccesoFSQA: Acceso a FSQA
''' bFSQARestProvDepCalidad: FSQARestProvDepCalidad
''' bFSQARestProvMaterial: FSQARestProvMaterial
''' bFSQARestProvEquipo: FSQARestProvEquipo
''' bFSQARestCertifUsu: FSQARestCertifUsu
''' bFSQARestCertifUO: FSQARestCertifUO
''' bFSQARestCertifDep: FSQARestCertifDep
''' bFSQARestNoConformUsu: FSQARestNoConformUsu
''' bFSQARestNoConformUO: FSQARestNoConformUO
''' bFSQARestNoConformDep: FSQARestNoConformDep
''' bFSQAMantenimientoMat: FSQAMantenimientoMat
''' bAccesoContratos: Acceso a Contratos
''' bAccesoFSIM: Acceso a FSIM
''' bAccesoFSBI: Acceso a FSBI
''' bAccesoFSBI: Acceso a FSSM
''' bAccesoFSBI: Acceso a FSINT
''' Devuelve el objeto CUsuario creado
''' Llamada desde CGestorSeguridad.cls

Public Function Add(ByVal Cod As String, ByVal pwd As String, ByVal Tipo As Integer, ByVal Bloq As Integer, _
            Optional ByVal varCodPerfil As Variant, Optional ByVal varDenPerfil As Variant, _
            Optional ByVal DepDen As Variant, Optional ByVal ComApe As Variant, _
            Optional ByVal ComNom As Variant, Optional ByVal PerApe As Variant, _
            Optional ByVal PerNom As Variant, Optional ByVal UsuNom As Variant, _
            Optional ByVal Eqpcod As Variant, Optional ByVal EqpDen As Variant, _
            Optional ByVal bAccesoFSEP As Boolean, Optional ByVal vFecha As Variant, _
            Optional ByVal bOtrosDest As Boolean, Optional ByVal DestCod As Variant, _
            Optional ByVal DestDen As Variant, Optional ByVal bAccesoFSWS As Boolean, _
            Optional ByVal bPedidoLibre As Boolean, Optional ByVal vTSRutaLocal As Variant, _
            Optional ByVal vTSRutaUnidad As Variant, Optional ByVal vTSUnidad As Variant, _
            Optional ByVal bAccesoFSGS As Boolean, Optional ByVal bFSWSPresUO As Boolean, _
            Optional ByVal bFSWSTrasladosUO As Boolean, Optional ByVal vFSWSSustitucion As Variant, _
            Optional ByVal vFSWSPres1 As Variant, Optional ByVal vFSWSPres2 As Variant, Optional ByVal vFSWSPres3 As Variant, Optional ByVal vFSWSPres4 As Variant, _
            Optional ByVal bFSWSTrasladosDEP As Boolean, Optional ByVal bFSWSRestrProvMat As Boolean, Optional ByVal bFSWSRestrMatUsu As Boolean, Optional ByVal bFSWSRestrDestUO As Boolean, _
            Optional ByVal bModifPrecios As Boolean, Optional ByVal bAccesoFSQA As Boolean, _
            Optional ByVal bFSQARestProvDepCalidad As Boolean, Optional ByVal bFSQARestProvMaterial As Boolean, Optional ByVal bFSQARestProvEquipo As Boolean, _
            Optional ByVal bFSQARestCertifUsu As Boolean, Optional ByVal bFSQARestCertifUO As Boolean, Optional ByVal bFSQARestCertifDep As Boolean, _
            Optional ByVal bFSQARestNoConformUsu As Boolean, Optional ByVal bFSQARestNoConformUO As Boolean, Optional ByVal bFSQARestNoConformDep As Boolean, _
            Optional ByVal bFSQAMantenimientoMat As Boolean, Optional ByVal bAccesoContratos As Boolean, Optional ByVal bAccesoFSIM As Boolean, _
            Optional ByVal bAccesoFSBI As Boolean, Optional ByVal bAccesoFSINT As Boolean, Optional ByVal bAccesoFSSM As Boolean) As CUsuario
            
    'create a new object
    Dim oUsuario As CUsuario
    Dim sClave As String
    Dim oPerfil As CPerfil
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oUsuario = New CUsuario
    
    oUsuario.Bloq = Bloq
    oUsuario.Cod = Cod
    oUsuario.pwd = pwd
    oUsuario.Tipo = Tipo
    oUsuario.AccesoFSEP = bAccesoFSEP
    oUsuario.AccesoFSWS = bAccesoFSWS
    oUsuario.AccesoFSGS = bAccesoFSGS
    oUsuario.AccesoFSQA = bAccesoFSQA
    oUsuario.AccesoFSContratos = bAccesoContratos
    oUsuario.AccesoFSIM = bAccesoFSIM
    oUsuario.AccesoFSBI = bAccesoFSBI
    oUsuario.accesofsint = bAccesoFSINT
    oUsuario.AccesoFSSM = bAccesoFSSM
    
    If Not IsMissing(EqpDen) And Not IsMissing(PerApe) And Not IsMissing(PerNom) Then
        Set oUsuario.Comprador = New CComprador
        oUsuario.Comprador.Apel = PerApe
        oUsuario.Comprador.Nombre = NullToStr(PerNom)
        oUsuario.Comprador.DenEqp = NullToStr(EqpDen)
    End If
    
    If Not IsMissing(DepDen) And Not IsMissing(PerApe) And Not IsMissing(PerNom) Then
        Set oUsuario.Persona = New CPersona
        oUsuario.Persona.Apellidos = PerApe
        oUsuario.Persona.Nombre = PerNom
        oUsuario.Persona.DenDep = DepDen
        If Not IsMissing(Eqpcod) Then
            oUsuario.Persona.CodEqp = Eqpcod
        End If
    End If
    
    If Not IsMissing(varCodPerfil) And Not IsNull(varCodPerfil) Then
        Set oUsuario.Perfil = New CPerfil
        oUsuario.Perfil.Cod = varCodPerfil
        oUsuario.Perfil.Den = varDenPerfil
    End If
    
    If Not IsMissing(vFecha) Then
        oUsuario.FecAct = vFecha
    End If
        
    If Not IsMissing(DestCod) And Not IsNull(DestCod) Then
        Set oUsuario.destino = New CDestino
        Set oUsuario.destino.Denominaciones = New CMultiidiomas
        oUsuario.destino.Cod = DestCod
        oUsuario.destino.Denominaciones.Add gParametrosInstalacion.gIdioma, ""
        oUsuario.destino.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den = DestDen
    End If

'    oUsuario.OtrosDestinos = bOtrosDest
    oUsuario.PedidoLibre = bPedidoLibre
    oUsuario.ModificarPrecios = bModifPrecios
    If Not IsMissing(vTSRutaLocal) Then
        oUsuario.TSRutaLocal = vTSRutaLocal
    Else
        oUsuario.TSRutaLocal = Null
    End If
    If Not IsMissing(vTSRutaUnidad) Then
        oUsuario.TSRutaUnidad = vTSRutaUnidad
    Else
        oUsuario.TSRutaUnidad = Null
    End If
    If Not IsMissing(vTSUnidad) Then
        oUsuario.TSUnidad = vTSUnidad
    Else
        oUsuario.TSUnidad = Null
    End If
    
    oUsuario.FSWSPresUO = bFSWSPresUO
    oUsuario.FSWSTrasladosUO = bFSWSTrasladosUO
    oUsuario.FSWSTrasladosDEP = bFSWSTrasladosDEP
    oUsuario.FSWSRestrProvMat = bFSWSRestrProvMat
    oUsuario.FSWSRestrMatUsu = bFSWSRestrMatUsu
    oUsuario.FSWSRestrDestUO = bFSWSRestrDestUO
    
    If Not IsMissing(vFSWSSustitucion) Then
        oUsuario.FSWSSustitucion = vFSWSSustitucion
    Else
        oUsuario.FSWSSustitucion = Null
    End If
    
    If Not IsMissing(vFSWSPres1) Then
        oUsuario.FSWSPres1 = vFSWSPres1
    Else
        oUsuario.FSWSPres1 = Null
    End If

    If Not IsMissing(vFSWSPres2) Then
        oUsuario.FSWSPres2 = vFSWSPres2
    Else
        oUsuario.FSWSPres2 = Null
    End If
    
    If Not IsMissing(vFSWSPres3) Then
        oUsuario.FSWSPres3 = vFSWSPres3
    Else
        oUsuario.FSWSPres3 = Null
    End If
    
    If Not IsMissing(vFSWSPres4) Then
        oUsuario.FSWSPres4 = vFSWSPres4
    Else
        oUsuario.FSWSPres4 = Null
    End If
    
    oUsuario.QARestCertifDep = bFSQARestCertifDep
    oUsuario.QARestCertifUO = bFSQARestCertifUO
    oUsuario.QARestCertifUsu = bFSQARestCertifUsu
    oUsuario.QARestNoConformDep = bFSQARestNoConformDep
    oUsuario.QARestNoConformUO = bFSQARestNoConformUO
    oUsuario.QARestNoConformUsu = bFSQARestNoConformUsu
    oUsuario.QARestProvDepCalidad = bFSQARestProvDepCalidad
    oUsuario.QARestProvEquipo = bFSQARestProvEquipo
    oUsuario.QARestProvMaterial = bFSQARestProvMaterial
    oUsuario.QAMantenimientoMat = bFSQAMantenimientoMat
    
    mCol.Add oUsuario, Cod
        
    'return the object created
    Set Add = oUsuario
    Set oUsuario = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CUsuarios", "Add", ERR, Erl)
        Exit Function
    End If

End Function

''' <summary>
''' Devolver los emails de usuarios de gestion de materiales qa
''' </summary>
''' <param name="ador">ByRef. Recordset con los emails de usuarios de gestion de materiales qa</param>
''' <param name="GMNCod">nivel GMN1 si @GMN1 y @GMN2 nulos   nivel GMN2 si @GMN1 no nulo y @GMN2 nulo    nivel GMN3 si @GMN1 no nulo y @GMN2 no nulo</param>
''' <param name="GMN1">nulo o nivel GMN1 si @COD no nulo y @GMN2 nulo</param>
''' <param name="GMN2">nulo o nivel GMN2</param>
''' <returns>Error de haberlo</returns>
''' <remarks>Llamada desde: basUtilidades/NotificarAsigMatQA ; Tiempo m�ximo: 0</remarks>
Public Function getEmailsUsuariosGestMatQA(ByRef ador As adodb.Recordset, Optional ByVal GMNCod As String = "", Optional ByVal GMN1 As String = "", Optional ByVal GMN2 As String = "") As TipoErrorSummit

    Dim TESError As TipoErrorSummit
    Dim adocom As adodb.Command
    Dim adoParam As adodb.Parameter
        
    ''' Precondicion
    If mvarConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CUsuario.CambiarLogin", "No se ha establecido la conexion"
        Exit Function
    End If
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
        
If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
        
    ''' Preparar la SP y sus parametros
    
    Set adocom = New adodb.Command
    Set adocom.ActiveConnection = mvarConexion.ADOCon
    
    Set adoParam = adocom.CreateParameter("@FSQA_MANTENIMIENTO_MAT", adInteger, adParamInput, , AccionesDeSeguridad.FSQAMantenimientoMat)
    adocom.Parameters.Append adoParam
    
    If GMNCod <> "" Then
        Set adoParam = adocom.CreateParameter("@COD", adVarChar, adParamInput, 50, GMNCod)
        adocom.Parameters.Append adoParam
        If GMN1 <> "" Then
            Set adoParam = adocom.CreateParameter("@GMN1", adVarChar, adParamInput, 50, GMN1)
            adocom.Parameters.Append adoParam
            If GMN2 <> "" Then
                Set adoParam = adocom.CreateParameter("@GMN2", adVarChar, adParamInput, 50, GMN2)
                adocom.Parameters.Append adoParam
            End If
        End If
    End If
        
    adocom.CommandType = adCmdStoredProc
    adocom.CommandText = "FSQA_GET_EMAILS_USU_MATQA"
    

    ''' Ejecutar la SP
    
    Set ador = adocom.Execute
    Set adocom = Nothing
    Set ador.ActiveConnection = Nothing
    
    getEmailsUsuariosGestMatQA = TESError
    
    Exit Function
    
Error_Cls:
    
    If Not adocom Is Nothing Then
    
        Set adocom = Nothing
    End If

    getEmailsUsuariosGestMatQA = basErrores.TratarError(mvarConexion.ADOCon.Errors)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        'Call g_oErrores.TratarError("Class Module", "CUsuarios", "getEmailsUsuariosGestMatQA", ERR, Erl)
        GoTo Error_Cls
        Exit Function
    End If
End Function


Friend Property Set Conexion(ByVal con As CConexion)
Set mvarConexion = con
End Property

Friend Property Get ConexionServer() As CConexionDeUseServer
Set ConexionServer = mvarConexionServer
End Property

Friend Property Set ConexionServer(ByVal con As CConexionDeUseServer)
Set mvarConexionServer = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = mvarConexion
End Property

Public Property Get Item(vntIndexKey As String) As CUsuario
Attribute Item.VB_UserMemId = 0

On Error GoTo vacio:
 
  Set Item = mCol(vntIndexKey)
  Exit Property

vacio:
  
  Set Item = Nothing

End Property



Public Property Get Count() As Long
    
    If mCol Is Nothing Then
        Count = 0
    Else
        Count = mCol.Count
    End If
    
End Property


Public Sub Remove(vntIndexKey As String)
        mCol.Remove vntIndexKey
End Sub


Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
    'this property allows you to enumerate
    'this collection with the For...Each syntax
    Set NewEnum = mCol.[_NewEnum]
End Property


Private Sub Class_Initialize()
    
   
    Set mCol = New Collection
End Sub


Private Sub Class_Terminate()
    
    Set mCol = Nothing
End Sub


