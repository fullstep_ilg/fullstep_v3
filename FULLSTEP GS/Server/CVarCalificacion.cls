VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CVarCalificacion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CCalificacion **********************************
'*             Autor : Javier Arana
'*             Creada : 23/12/98
'****************************************************************

Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

Private m_vIndice As Variant 'Nos indica la posicion secuencial dentro de una Coleccion
Private m_lVarID As Long
Private m_iVarNivel As Integer
Private m_lID As Long
Private m_dblValorSup As Double
Private m_dblValorInf As Double
Private m_oConexion As CConexion 'local copy
Private m_oDenominaciones As CMultiidiomas

Public Property Get Indice() As Variant
    Indice = m_vIndice
End Property
Public Property Let Indice(ByVal varInd As Variant)
    m_vIndice = varInd
End Property
Public Property Get VarID() As Long
    VarID = m_lVarID
End Property
Public Property Let VarID(ByVal varInd As Long)
    m_lVarID = varInd
End Property

Public Property Get Nivel() As Integer
    Nivel = m_iVarNivel
End Property
Public Property Let Nivel(ByVal varInd As Integer)
    m_iVarNivel = varInd
End Property
Public Property Get Id() As Long
    Id = m_lID
End Property
Public Property Let Id(ByVal varInd As Long)
    m_lID = varInd
End Property

Public Property Get Denominaciones() As CMultiidiomas
    Set Denominaciones = m_oDenominaciones
End Property
Public Property Set Denominaciones(ByVal dato As CMultiidiomas)
    Set m_oDenominaciones = dato
End Property

Public Property Get ValorSup() As Double
    ValorSup = m_dblValorSup
End Property
Public Property Let ValorSup(ByVal varInd As Double)
    m_dblValorSup = varInd
End Property
Public Property Get ValorInf() As Double
    ValorInf = m_dblValorInf
End Property
Public Property Let ValorInf(ByVal varInd As Double)
    m_dblValorInf = varInd
End Property
Friend Property Set Conexion(ByVal vData As CConexion)
    Set m_oConexion = vData
End Property

Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property

Private Sub Class_Terminate()
    Set m_oConexion = Nothing
End Sub

''' <summary>
''' Obtener la calificacion de la variable de calidad
''' </summary>
''' <param name="iVarCal">Id de la variable de calidad</param>
''' <param name="iNivel">Nivel de la variable de calidad</param>
''' <param name="dblValue">Valor actual de la puntuacion de la variable de calidad</param>
''' <returns>Devuelve la calificacion de la variable de calidad</returns>
''' <remarks>Llamada desde frmComparativaQA.CargarProveVarsQA, frmComparativaQA.CargarVarsQAProve; Tiempo m�ximo < 0,1</remarks>

Public Function CargarCalificacion(ByVal iVarCal As Integer, ByVal iNivel As Integer, ByVal dblValue As Double) As String
Dim rs As adodb.Recordset
Dim adocom As adodb.Command
Dim oParam As adodb.Parameter

    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set adocom = New adodb.Command
    Set adocom.ActiveConnection = m_oConexion.ADOCon
    
    adocom.CommandType = adCmdStoredProc
    adocom.CommandText = "FSQA_GET_CALIFICATION"
    
    Set oParam = adocom.CreateParameter("VARCAL", adInteger, adParamInput, , iVarCal)
    adocom.Parameters.Append oParam
    Set oParam = adocom.CreateParameter("NIVEL", adSmallInt, adParamInput, , iNivel)
    adocom.Parameters.Append oParam
    Set oParam = adocom.CreateParameter("VALUE", adDouble, adParamInput, , dblValue)
    adocom.Parameters.Append oParam
    Set oParam = adocom.CreateParameter("IDI", adVarChar, adParamInput, 20, gParametrosInstalacion.gIdioma)
    adocom.Parameters.Append oParam
    
    Set rs = adocom.Execute
    
    If rs.eof Then
        CargarCalificacion = ""
    Else
        CargarCalificacion = rs("CAL").Value
    End If
    rs.Close
    Set rs = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If Err.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CVarCalificacion", "CargarCalificacion", Err, Erl)
      Exit Function
   End If
End Function
