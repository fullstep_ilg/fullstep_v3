VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CSolicitEstado"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
''' *** Clase: CSolicitEstado
''' *** Creacion: 30/01/2006 (MMV)

Option Explicit

Implements IBaseDatos

Private m_sCod As String
Private m_oDen As CMultiidiomas
Private m_vFecAct As Variant
Private m_lIndice As Long

Private m_oSolicitud As CSolicitud

''' Conexion
Private m_oConexion As CConexion

''' Control de errores
Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum

Public Property Get Indice() As Long

    Indice = m_lIndice
    
End Property
Public Property Let Indice(ByVal lInd As Long)

    m_lIndice = lInd
    
End Property

Friend Property Set Conexion(ByVal vData As CConexion)

    Set m_oConexion = vData
    
End Property

Friend Property Get Conexion() As CConexion

    Set Conexion = m_oConexion
    
End Property

Public Property Let Codigo(ByVal vData As String)

    m_sCod = vData
    
End Property
Public Property Get Codigo() As String

    Codigo = m_sCod
    
End Property

Public Property Get Denominaciones() As CMultiidiomas
    Set Denominaciones = m_oDen
End Property
Public Property Set Denominaciones(ByVal dato As CMultiidiomas)
    Set m_oDen = dato
End Property

Public Property Let FecAct(ByVal vData As Variant)

    m_vFecAct = vData
    
End Property
Public Property Get FecAct() As Variant
    FecAct = m_vFecAct
    
End Property

Public Property Set Solicitud(ByVal oSolicit As CSolicitud)
    Set m_oSolicitud = oSolicit
End Property

Public Property Get Solicitud() As CSolicitud
    Set Solicitud = m_oSolicitud
End Property

Private Sub Class_Terminate()
    
    Set m_oDen = Nothing
    Set m_oConexion = Nothing
    Set m_oSolicitud = Nothing
End Sub

''' <summary>
''' Inserta en bbdd un registro de SOLICITUD_ESTADOS
''' </summary>
''' <returns>Error de haberlo</returns>
''' <remarks>Llamada desde:frmSOLConfigEstados/sdbgEstados_BeforeUpdate; Tiempo m�ximo:0,1</remarks>
Private Function IBaseDatos_AnyadirABaseDatos() As TipoErrorSummit
Dim rs As adodb.Recordset
Dim sConsulta As String
Dim TESError As TipoErrorSummit
Dim btrans As Boolean
Dim sCod As String
Dim oMulti As CMultiidioma
Dim NoExistianEstados As Boolean

'********Precondici�n******************************************************************************************************************
If m_oConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CSolicitEstado.AnyadirABAseDatos", "No se ha establecido la conexion"
End If
'**************************************************************************************************************************************

If Not g_oErrores.fg_bProgramando Then On Error GoTo Error
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

TESError.NumError = TESnoerror
If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:

    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
    btrans = True

    If Not m_oDen Is Nothing Then
        For Each oMulti In m_oDen
            sCod = sCod & "," & "DEN_" & oMulti.Cod
        Next
    End If
    
    Set rs = New adodb.Recordset
    sConsulta = "SELECT COUNT(1) FROM SOLICITUD_ESTADOS WITH (NOLOCK) WHERE SOLICITUD=" & m_oSolicitud.Id
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
    NoExistianEstados = (rs(0).Value = 0)
    rs.Close
    Set rs = Nothing
    
    sConsulta = "INSERT INTO SOLICITUD_ESTADOS (SOLICITUD,COD" & sCod & ") "
    sConsulta = sConsulta & " VALUES (" & m_oSolicitud.Id & ",'" & DblQuote(m_sCod) & "'"
    For Each oMulti In m_oDen
        sConsulta = sConsulta & "," & StrToSQLNULL(oMulti.Den)
    Next
    sConsulta = sConsulta & ")"
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    
    Set rs = New adodb.Recordset
    'Calidad Sin with(nolock) pq FECACT se usa en control de errores para modificaciones por otros usuarios
    rs.Open "SELECT FECACT FROM SOLICITUD_ESTADOS WITH (NOLOCK) WHERE SOLICITUD=" & m_oSolicitud.Id & " AND COD='" & DblQuote(m_sCod) & "'", m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
    m_vFecAct = rs(0).Value
    rs.Close
    Set rs = Nothing
    
    If NoExistianEstados Then
        'Inserto en CONF_CUMPLIMENTACION un registro para el campo Estado, aunque no es un campo del form
        'va con CAMPO=id de campo del campo acciones al que pertenece
        'Insertamos uno por cada campo Acciones (y por solicitante y proveedor) y con orden el �ltimo de su desglose
        'Todo con WITH (NOLOCK) sin mayor problema
        sConsulta = "INSERT INTO CONF_CUMPLIMENTACION (SOLICITUD,CAMPO,PASO,SOLIC_PROVE,VISIBLE,ESCRITURA,OBLIGATORIO,ORDEN,ES_ESTADO)"
        sConsulta = sConsulta & " SELECT " & m_oSolicitud.Id & " AS SOLICITUD ,FC.ID , NULL AS PASO ,2 AS SOLIC_PROVE, 1 AS VISIBLE"
        sConsulta = sConsulta & " ,1 AS ESCRITURA ,0 AS OBLIGATORIO, Z.ORDENC, 1 AS ES_ESTADO "
        sConsulta = sConsulta & " FROM SOLICITUD S WITH (NOLOCK) INNER JOIN FORM_GRUPO FG WITH (NOLOCK) ON FG.FORMULARIO=S.FORMULARIO "
        sConsulta = sConsulta & " INNER JOIN FORM_CAMPO FC WITH (NOLOCK) ON FC.GRUPO=FG.ID AND FC.TIPO_CAMPO_GS=34 "
        sConsulta = sConsulta & " INNER JOIN (SELECT MAX(CF.ORDEN)+1 AS ORDENC,D.CAMPO_PADRE AS CAMPO FROM CONF_CUMPLIMENTACION CF WITH (NOLOCK) "
        sConsulta = sConsulta & " INNER JOIN DESGLOSE D WITH (NOLOCK) ON D.CAMPO_HIJO=CF.CAMPO AND CF.SOLICITUD=" & m_oSolicitud.Id & " GROUP BY D.CAMPO_PADRE) AS Z ON Z.CAMPO=FC.ID"
        sConsulta = sConsulta & " WHERE S.ID = " & m_oSolicitud.Id
        m_oConexion.ADOCon.Execute sConsulta
        If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
        'para el solicitante:
        sConsulta = "INSERT INTO CONF_CUMPLIMENTACION (SOLICITUD,CAMPO,PASO,SOLIC_PROVE,VISIBLE,ESCRITURA,OBLIGATORIO,ORDEN,ES_ESTADO)"
        sConsulta = sConsulta & " SELECT " & m_oSolicitud.Id & " AS SOLICITUD ,FC.ID , NULL AS PASO ,1 AS SOLIC_PROVE, 1 AS VISIBLE"
        sConsulta = sConsulta & " ,1 AS ESCRITURA ,0 AS OBLIGATORIO, Z.ORDENC, 1 AS ES_ESTADO "
        sConsulta = sConsulta & " FROM SOLICITUD S WITH (NOLOCK) INNER JOIN FORM_GRUPO FG WITH (NOLOCK) ON FG.FORMULARIO=S.FORMULARIO "
        sConsulta = sConsulta & " INNER JOIN FORM_CAMPO FC WITH (NOLOCK) ON FC.GRUPO=FG.ID AND FC.TIPO_CAMPO_GS=34 "
        sConsulta = sConsulta & " INNER JOIN (SELECT MAX(CF.ORDEN)+1 AS ORDENC,D.CAMPO_PADRE AS CAMPO FROM CONF_CUMPLIMENTACION CF WITH (NOLOCK) "
        sConsulta = sConsulta & " INNER JOIN DESGLOSE D WITH (NOLOCK) ON D.CAMPO_HIJO=CF.CAMPO AND CF.SOLICITUD=" & m_oSolicitud.Id & " GROUP BY D.CAMPO_PADRE) AS Z ON Z.CAMPO=FC.ID"
        sConsulta = sConsulta & " WHERE S.ID = " & m_oSolicitud.Id
        m_oConexion.ADOCon.Execute sConsulta
        If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    End If
    
    
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    btrans = False
    TESError.NumError = TESnoerror
    IBaseDatos_AnyadirABaseDatos = TESError
    
    Exit Function
    
Error_Cls:
    IBaseDatos_AnyadirABaseDatos = basErrores.TratarError(m_oConexion.ADOCon.Errors)
     
    If btrans Then
        m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
    End If

    Set rs = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CSolicitEstado", "IBaseDatos_AnyadirABaseDatos", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If
    
End Function
Private Function IBaseDatos_CambiarCodigo(CodigoNuevo As Variant) As TipoErrorSummit
Dim TESError As TipoErrorSummit
Dim rs As adodb.Recordset
Dim sConsulta As String


    TESError.NumError = TESnoerror
    
    ''' Precondicion
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CSolicitEstado.CambiarCodigo", "No se ha establecido la conexion"
    End If
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo Error
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
        
If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
    
    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
    
    sConsulta = "SELECT FECACT FROM SOLICITUD_ESTADOS WITH (NOLOCK) WHERE COD='" & DblQuote(m_sCod) & "' AND SOLICITUD=" & m_oSolicitud.Id
    Set rs = New adodb.Recordset
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
    
    If rs.eof Then
        rs.Close
        Set rs = Nothing
        TESError.NumError = TESDatoEliminado
        TESError.Arg1 = 190 ' Tabla SOLICITUD_ESTADOS
        IBaseDatos_CambiarCodigo = TESError
        m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
        Exit Function
    End If
    
    'Comprobamos si ha habido cambios en otra sesi�n
    If m_vFecAct <> rs("FECACT") Then
        rs.Close
        Set rs = Nothing
        TESError.NumError = TESInfActualModificada
        IBaseDatos_CambiarCodigo = TESError
        m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
        Exit Function
    End If
    
    m_oConexion.ADOCon.Execute "ALTER TABLE SOLICITUD_ESTADOS NOCHECK CONSTRAINT ALL"
    
    sConsulta = "UPDATE SOLICITUD_ESTADOS SET COD= '" & DblQuote(CodigoNuevo) & "' WHERE COD='" & DblQuote(m_sCod) & "' AND SOLICITUD=" & m_oSolicitud.Id
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    m_oConexion.ADOCon.Execute "ALTER TABLE SOLICITUD_ESTADOS CHECK CONSTRAINT ALL"
    
    rs.Close
    sConsulta = "SELECT FECACT FROM SOLICITUD_ESTADOS WITH (NOLOCK) WHERE COD='" & DblQuote(CodigoNuevo) & "'  AND SOLICITUD=" & m_oSolicitud.Id
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
    m_vFecAct = rs(0).Value
    rs.Close
    Set rs = Nothing
    m_sCod = CodigoNuevo
    
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    IBaseDatos_CambiarCodigo = TESError
    
    Exit Function
    
Error_Cls:
     
    IBaseDatos_CambiarCodigo = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
            
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CSolicitEstado", "IBaseDatos_CambiarCodigo", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If
            
End Function
Private Sub IBaseDatos_CancelarEdicion()

End Sub
Private Function IBaseDatos_ComprobarExistenciaEnBaseDatos() As Boolean

End Function
Private Function IBaseDatos_EliminarDeBaseDatos() As TipoErrorSummit
    Dim TESError As TipoErrorSummit
    Dim sConsulta As String
    Dim btrans As Boolean
    
    
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo Error
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
        
    TESError.NumError = TESnoerror

    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
    btrans = True
        
    sConsulta = "DELETE FROM SOLICITUD_ESTADOS WHERE SOLICITUD =" & m_oSolicitud.Id & " AND COD='" & DblQuote(m_sCod) & "'"
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    
    IBaseDatos_EliminarDeBaseDatos = TESError
    btrans = False
    
    Exit Function
    
Error_Cls:
    IBaseDatos_EliminarDeBaseDatos = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    
    If btrans Then
        m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CSolicitEstado", "IBaseDatos_EliminarDeBaseDatos", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If

End Function

Private Function IBaseDatos_FinalizarEdicionEliminando() As TipoErrorSummit

    ''' ! No usado por ahora
    
End Function

''' <summary>
''' Modifica en bbdd un registro de SOLICITUD_ESTADOS
''' </summary>
''' <returns>Error de haberlo</returns>
''' <remarks>Llamada desde:frmSOLConfigEstados/sdbgEstados_BeforeUpdate; Tiempo m�ximo:0,1</remarks>
Private Function IBaseDatos_FinalizarEdicionModificando() As TipoErrorSummit
Dim TESError As TipoErrorSummit
Dim btrans As Boolean
Dim sConsulta As String
Dim rs As adodb.Recordset
Dim i As Integer
Dim oMulti As CMultiidioma


    TESError.NumError = TESnoerror

    ''' Precondicion
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CSolicitEstado.FinalizarEdicionModificando", "No se ha establecido la conexion"
    End If
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo Error
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
    
    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
    btrans = True
    
    'Calidad Sin with(nolock) pq FECACT se usa en control de errores para modificaciones por otros usuarios
    sConsulta = "SELECT FECACT FROM SOLICITUD_ESTADOS WHERE SOLICITUD=" & m_oSolicitud.Id & " AND COD='" & DblQuote(m_sCod) & "'"
    Set rs = New adodb.Recordset
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly

    If rs.eof Then
        rs.Close
        Set rs = Nothing
        TESError.NumError = TESDatoEliminado
        TESError.Arg1 = 190
        IBaseDatos_FinalizarEdicionModificando = TESError
        m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
        Exit Function
    End If


    sConsulta = "UPDATE SOLICITUD_ESTADOS SET"
    For Each oMulti In m_oDen
        sConsulta = sConsulta & " DEN_" & oMulti.Cod & "= " & StrToSQLNULL(oMulti.Den) & ","
    Next
    sConsulta = Left(sConsulta, Len(sConsulta) - 1)
    sConsulta = sConsulta & " WHERE SOLICITUD=" & m_oSolicitud.Id & " AND COD='" & DblQuote(m_sCod) & "'"
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls

    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    btrans = False
    rs.Requery
    m_vFecAct = rs(0).Value
    rs.Close
    Set rs = Nothing
    TESError.NumError = TESnoerror
    IBaseDatos_FinalizarEdicionModificando = TESError
    
    Exit Function
    

Error_Cls:
    TESError = basErrores.TratarError(m_oConexion.ADOCon.Errors)
     
    If btrans Then
        m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
    End If

    Resume Salir:
    
Salir:

    On Error Resume Next
    If Not rs Is Nothing Then
        rs.Close
    End If
    
    IBaseDatos_FinalizarEdicionModificando = TESError
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CSolicitEstado", "IBaseDatos_FinalizarEdicionModificando", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If

End Function

''' <summary>
''' Carga las propertys de la clase
''' </summary>
''' <param name="Bloquear">bloquear si/no</param>
''' <param name="UsuarioBloqueo">usuario q bloquea</param>
''' <returns>Error de haberlo</returns>
''' <remarks>Llamada desde: frmSOLConfigEstados/sdbgEstados_Change; Tiempo m�ximo: 0,1</remarks>
Private Function IBaseDatos_IniciarEdicion(Optional ByVal Bloquear As Boolean, Optional ByVal UsuarioBloqueo As String) As TipoErrorSummit
Dim TESError As TipoErrorSummit
Dim sSQL As String
Dim rs As adodb.Recordset
Dim i As Long
Dim oGestorParametros As CGestorParametros
Dim oIdiomas As CIdiomas
Dim oIdi As CIdioma
    

    TESError.NumError = TESnoerror
    
    ''' Precondici�n
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CSolicitEstado.IniciarEdicion", "No se ha establecido la conexion"
    End If
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo Error
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
    
    'Carga todos los idiomas de la aplicaci�n:
    Set oGestorParametros = New CGestorParametros
    Set oGestorParametros.Conexion = m_oConexion
    Set oIdiomas = oGestorParametros.DevolverIdiomas(False, False, True)
    
    Dim den_general As String
    den_general = ""
    
    For Each oIdi In oIdiomas
        If (den_general <> "") Then
           den_general = den_general & ","
        End If
        den_general = den_general & "DEN_" & oIdi.Cod
    Next
    ' den_general = DEN_SPA,DEN_ENG,DEN_GER,DEN_FRA
    
    
    Set rs = New adodb.Recordset
    rs.Open ("SELECT SOLICITUD,COD,FECACT," & den_general & " FROM SOLICITUD_ESTADOS WITH (NOLOCK) WHERE SOLICITUD=" & m_oSolicitud.Id & " AND COD='" & DblQuote(m_sCod) & "'"), m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
    
    If rs.eof Then  'El estado ha sido eliminado
        rs.Close
        Set rs = Nothing
        TESError.NumError = TESDatoEliminado
        TESError.Arg1 = 190 '''SOLICITUD_ESTADOS
        IBaseDatos_IniciarEdicion = TESError
        Exit Function
    End If
     
    ''' Si hay datos diferentes, devolver la condicion
    If CStr(m_vFecAct) <> CStr(rs("FECACT").Value) Then
        TESError.NumError = TESInfActualModificada
        rs.Close
        Set rs = Nothing
        IBaseDatos_IniciarEdicion = TESError
        Exit Function
    End If
    
    m_sCod = rs("COD").Value
    Set m_oDen = New CMultiidiomas
    For i = 0 To rs.Fields.Count - 1
        If Left(rs.Fields(i).Name, 4) = "DEN_" Or Left(rs.Fields(i).Name, 6) = "DESCR_" Then
            m_oDen.Add Right(rs.Fields(i).Name, Len(rs.Fields(i).Name) - 4), rs.Fields(i).Value
        End If
    Next i
    
    m_vFecAct = rs("FECACT").Value
    
    rs.Close
    Set rs = Nothing
    TESError.NumError = TESnoerror
    IBaseDatos_IniciarEdicion = TESError
        
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CSolicitEstado", "IBaseDatos_IniciarEdicion", ERR, Erl)
      Exit Function
   End If
    
End Function


