VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "IUon"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Member0" ,"uon3"
Attribute VB_Ext_KEY = "Member1" ,"uon2"
Attribute VB_Ext_KEY = "Member2" ,"uon1"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"

Option Explicit

Public Property Get titulo(Optional sSeparadorCod As String = "/", Optional sSeparadorDen As String = "-", Optional bIncluirDen As Boolean = True) As String

End Property

Public Property Get Den() As String

End Property

Public Property Let Den(Value As String)

End Property

Public Property Get CodUnidadOrgNivel1() As String
    
End Property

Public Property Let CodUnidadOrgNivel1(Value As String)
    
End Property

Public Property Get CodUnidadOrgNivel2() As String
    
End Property

Public Property Let CodUnidadOrgNivel2(Value As String)
    
End Property

Public Property Get CodUnidadOrgNivel3() As String
    
End Property

Public Property Let CodUnidadOrgNivel3(Value As String)
    
End Property

Public Property Get CodUnidadOrgNivel4() As String
    
End Property

Public Property Let CodUnidadOrgNivel4(Value As String)
    
End Property

''' <summary>
''' Propiedad que indica el nivel de la UON (uso cuando tengo una colecci�n gen�rica de UONs para y evitar comprobaci�n de tipos)
''' </summary>
Public Property Get Nivel() As Integer
    
End Property

'''<summary>
'''indica si la uon actual contiene a la unidad pasada por par�metro, tipo de uon tiene que ser CUon1,CUon2 o CUon3
'''</summary>
Public Function incluye(ByRef uon As Variant) As Boolean

End Function

'''<summary>
'''indica si la uon actual est� contenida (o es ella misma) en la unidad pasada por par�metro, tipo de uon tiene que ser CUon1,CUon2 o CUon3
'''</summary>
Public Function perteneceA(ByRef uon As Variant) As Boolean

End Function

'<summary>Devuelve clave para utilizar como �ndice en colecciones</summary>
Public Function Key() As String
    
End Function

'<summary>Comprueba si la Uon o alguna de sus descendientes tiene valor para un determinado atributo</summary>
Public Function tieneAtributoValorado(ByVal IDAtributo As Integer) As Boolean

End Function

'<summary>Comprueba si la Uon o alguna de sus descendientes tiene valores para un determinado art�culo en alg�n atributo</summary>
Public Function tieneAtributoValoradoArticulo(ByVal idArticulo As String) As Boolean

End Function

'''<summary>Compara dos unidades organizativas</summary>
Public Function equals(ByVal oUON As IUon) As Boolean

End Function
'''<summary>Hace una copia de la unidad</summary>
Public Function clonar() As IUon

End Function

Public Property Get SentidoIntegracion() As SentidoIntegracion

End Property

Public Property Let SentidoIntegracion(ByVal Value As SentidoIntegracion)

End Property

Public Property Get idEmpresa() As Variant
    
End Property

Public Property Let idEmpresa(ByVal vEmpr As Variant)
    
End Property

Public Property Set Empresa(ByVal oEmp As CEmpresa)

End Property
Public Property Get Empresa() As CEmpresa

End Property

Public Property Get Conexion() As CConexion

End Property

Public Property Set Conexion(ByVal Value As CConexion)

End Property

Public Property Get Departamentos() As CDepartamentos

End Property

Public Sub CargarTodosLosDepartamentos(Optional ByVal CarIniCod As String, Optional ByVal CarIniDen As String, Optional ByVal CoincidenciaTotal As Boolean, Optional ByVal OrdenadosPorDen As Boolean = False, Optional ByVal UsarIndice As Boolean = False)

End Sub

