VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CPresConceptos4Nivel3"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CPresConceptos4Nivel3 **********************************
'*
'*             Creada : 13/2/2000
'****************************************************************

Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum

Private mCol As Collection
Private m_oConexion As CConexion
Private m_oConexionServer As CConexionDeUseServer
Private m_bEOF As Boolean

''' <summary>
''' Carga de los presupuestos
''' </summary>
'''<param optional name="sPRES1">Presupuesto 1</param>
'''<param optional name="sPRES2">Presupuesto 2</param>
'''<param optional name="sPRES3">Presupuesto 3</param>
'''<param optional name="CaracteresInicialesCod">C�digo o primeros caracteres del c�digo</param>
'''<param optional name="CaracteresInicialesDen">Denominaci�n o primeros caracteres de la denominaci�n</param>
'''<param optional name="CoincidenciaTotal">Determina si usamos como criterio de selecci�n el c�digo y la denominaci�n o los primeros caracteres de ambos</param>
'''<param optional name="OrdenadosPorDen">Ordena por Denomninaci�n</param>
'''<param optional name="OrdenadosPorPres">Ordena por Presupuesto</param>
'''<param optional name="OrdenadosPorObj">Ordena por Obj</param>
'''<param optional name="UsarIndice">Si se usa �ndice</param>
'''<param optional name="vUON1">Unidad organizativa 1</param>
'''<param optional name="vUON2">Unidad organizativa 2</param>
'''<param optional name="vUON3">Unidad organizativa 3</param>
'''<param optional name="bBajaLog">Para el campo BAJALOG</param>
''' <returns>Double</returns>
''' <remarks>Llamada desde: FSGClient
''' Tiempo m�ximo: 1 sec </remarks>
''' <revision>JVS 28/12/2011</revision>
Public Function CargarPresupuestosConceptos4(Optional ByVal sPRES1 As String, Optional ByVal sPRES2 As String, Optional ByVal CaracteresInicialesCod As String, Optional ByVal CaracteresInicialesDen As String, Optional ByVal CoincidenciaTotal As Boolean, Optional ByVal OrdenadosPorDen As Boolean, Optional ByVal OrdenadosPorPres As Boolean, Optional ByVal OrdenadosPorObj As Boolean, Optional ByVal UsarIndice As Boolean, Optional ByVal vUON1 As Variant, Optional ByVal vUON2 As Variant, Optional ByVal vUON3 As Variant, Optional ByVal bBajaLog As Variant) As Double
'ado  Dim rdores As rdoResultset
Dim rs As adodb.Recordset
Dim fldCod As adodb.Field   'Cod
Dim fldDen As adodb.Field   'Den
Dim fldImp As adodb.Field   'Imp
Dim fldObj As adodb.Field   'Obj
Dim fldPres1 As adodb.Field  'Pres1
Dim fldPres2 As adodb.Field  'Pres2
Dim fldUON1 As adodb.Field  'UON1
Dim fldUON2 As adodb.Field  'UON2
Dim fldUON3 As adodb.Field  'UON3
Dim fldBajaLog As adodb.Field  'BAJALOG
Dim fldFecIniVal As adodb.Field  'FEC_INI_VAL
Dim fldFecFinVal As adodb.Field  'FEC_FIN_VAL
Dim sConsulta As String
Dim lIndice As Long
Dim dSuma As Double

'********* Precondicion **************************************
If m_oConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CDestinos.CargarTodosLosDestinos", "No se ha establecido la conexion"
    Exit Function
End If
'*************************************************************
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
dSuma = 0

sConsulta = "SELECT ID,PRES1,PRES2,COD,DEN,UON1,UON2,UON3,IMP,OBJ,FECACT,BAJALOG,FEC_INI_VAL,FEC_FIN_VAL FROM PRES4_NIV3 WITH(NOLOCK) WHERE 1=1 "

If CaracteresInicialesCod = "" And CaracteresInicialesDen = "" Then
   
Else
    If Not (CaracteresInicialesCod = "") And Not (CaracteresInicialesDen = "") Then
        
        If CoincidenciaTotal Then
            
            sConsulta = sConsulta & " AND COD ='" & DblQuote(CaracteresInicialesCod) & "'"
            sConsulta = sConsulta & " AND DEN='" & DblQuote(CaracteresInicialesDen) & "'"
            
        Else
            
            sConsulta = sConsulta & " AND COD LIKE '" & DblQuote(CaracteresInicialesCod) & "%'"
            sConsulta = sConsulta & " AND DEN LIKE '" & DblQuote(CaracteresInicialesDen) & "%'"
                       
        End If
                    
    Else
        
        If Not (CaracteresInicialesCod = "") Then
            
            If CoincidenciaTotal Then
            
                sConsulta = sConsulta & " AND COD ='" & DblQuote(CaracteresInicialesCod) & "'"
                
            Else
                    
                sConsulta = sConsulta & " AND COD LIKE '" & DblQuote(CaracteresInicialesCod) & "%'"
                
            End If
            
        Else
            
            If CoincidenciaTotal Then
            
                sConsulta = sConsulta & " AND DEN='" & DblQuote(CaracteresInicialesDen) & "'"
                
            Else
           
                sConsulta = sConsulta & " AND DEN LIKE '" & DblQuote(CaracteresInicialesDen) & "%'"
                  
            End If
            
        End If
    
    End If
           
End If

If Not NoHayParametro(bBajaLog) Then
    sConsulta = sConsulta & " AND BAJALOG=" & BooleanToSQLBinary(bBajaLog)
End If

If Trim(sPRES1) <> "" Then
    sConsulta = sConsulta & " AND PRES1='" & DblQuote(sPRES1) & "'"
End If

If Trim(sPRES2) <> "" Then
    sConsulta = sConsulta & " AND PRES2='" & DblQuote(sPRES2) & "'"
End If

sConsulta = sConsulta & HacerParteWhereUON(vUON1, vUON2, vUON3)

If OrdenadosPorDen Then
    sConsulta = sConsulta & " ORDER BY DEN,COD"
Else
    If OrdenadosPorPres Then
        sConsulta = sConsulta & " ORDER BY IMP,COD"
    Else
        If OrdenadosPorObj Then
            sConsulta = sConsulta & " ORDER BY OBJ,COD,DEN"
        Else
            sConsulta = sConsulta & " ORDER BY COD,DEN"
        End If
    End If
End If
      
'ado  Set rdores = m_oConexion.rdoSummitCon.OpenResultset(sConsulta, rdOpenForwardOnly, rdConcurReadOnly, rdExecDirect)
Set rs = New adodb.Recordset
rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly

If rs.eof Then
        
    rs.Close
    Set rs = Nothing
    Set mCol = Nothing
    Set mCol = New Collection
    CargarPresupuestosConceptos4 = dSuma
    Exit Function
      
Else
    
    Set mCol = Nothing
    Set mCol = New Collection
    Set fldCod = rs.Fields("COD")
    Set fldDen = rs.Fields("DEN")
    Set fldImp = rs.Fields("IMP")
    Set fldObj = rs.Fields("OBJ")
    Set fldPres1 = rs.Fields("PRES1")
    Set fldPres2 = rs.Fields("PRES2")
    Set fldUON1 = rs.Fields.Item("UON1")
    Set fldUON2 = rs.Fields.Item("UON2")
    Set fldUON3 = rs.Fields.Item("UON3")
    Set fldBajaLog = rs.Fields.Item("BAJALOG")
    Set fldFecIniVal = rs.Fields.Item("FEC_INI_VAL")
    Set fldFecFinVal = rs.Fields.Item("FEC_FIN_VAL")
    
    If UsarIndice Then
        
        lIndice = 0
        
        While Not rs.eof
            ' Dejamos en manos del programador cuando asignar la conexion al obejto usuario
            'ado  Me.Add rdores("PRES1"), rdores("PRES2"), rdores("COD"), rdores("DEN"), rdores("IMP"), rdores("OBJ"), lIndice
            'ado  dSuma = dSuma + NullToDbl0(rdores("IMP"))
            Me.Add fldPres1.Value, fldPres2.Value, fldCod.Value, fldDen.Value, fldImp.Value, fldObj.Value, lIndice, fldUON1.Value, fldUON2.Value, fldUON3.Value, , fldBajaLog.Value, fldFecIniVal.Value, fldFecFinVal.Value
            dSuma = dSuma + NullToDbl0(fldImp.Value)
            rs.MoveNext
            lIndice = lIndice + 1
        Wend
        
    Else
        
        While Not rs.eof
            ' Dejamos en manos del programador cuando asignar la conexion al obejto usuario
            'ado  Me.Add rdores("PRES1"), rdores("PRES2"), rdores("COD"), rdores("DEN"), rdores("IMP"), rdores("OBJ")
            'ado  dSuma = dSuma + NullToDbl0(rdores("IMP"))
            Me.Add fldPres1.Value, fldPres2.Value, fldCod.Value, fldDen.Value, fldImp.Value, fldObj.Value, , fldUON1.Value, fldUON2.Value, fldUON3.Value, , fldBajaLog.Value, fldFecIniVal.Value, fldFecFinVal.Value
            dSuma = dSuma + NullToDbl0(fldImp.Value)
            rs.MoveNext
        Wend
    End If
    
    rs.Close
    Set rs = Nothing
    Set fldCod = Nothing
    Set fldDen = Nothing
    Set fldImp = Nothing
    Set fldObj = Nothing
    Set fldPres1 = Nothing
    Set fldPres2 = Nothing
    Set fldUON1 = Nothing
    Set fldUON2 = Nothing
    Set fldUON3 = Nothing
    Set fldBajaLog = Nothing
    Set fldFecIniVal = Nothing
    Set fldFecFinVal = Nothing
    
    CargarPresupuestosConceptos4 = dSuma
    Exit Function
      
End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CPresConceptos4Nivel3", "CargarPresupuestosConceptos4", ERR, Erl)
        Exit Function
    End If
End Function

Public Function Add(ByVal sPRES1 As String, ByVal sPRES2 As String, ByVal Cod As String, ByVal Den As String, Optional ByVal Importe As Variant, Optional ByVal Objetivo As Variant, Optional ByVal varIndice As Variant, Optional ByVal vUON1 As Variant, Optional ByVal vUON2 As Variant, Optional ByVal vUON3 As Variant, Optional ByVal Id As Long, Optional ByVal bBajaLog As Boolean, Optional ByVal vFecIniVal As Variant, Optional ByVal vFecFinVal As Variant) As CPresConcep4Nivel3
    'create a new object
    
    Dim objnewmember As CPresConcep4Nivel3
    Dim sCod As String
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New CPresConcep4Nivel3
   
    objnewmember.CodPRES1 = sPRES1
    objnewmember.CodPRES2 = sPRES2
    objnewmember.Importe = Importe
    objnewmember.Objetivo = Objetivo
    objnewmember.Cod = Cod
    objnewmember.Den = Den
    
    If NoHayParametro(vUON1) Then
        objnewmember.UON1 = Null
    Else
        objnewmember.UON1 = vUON1
    End If
    
    If NoHayParametro(vUON2) Then
        objnewmember.UON2 = Null
    Else
        objnewmember.UON2 = vUON2
    End If
    
    If NoHayParametro(vUON3) Then
        objnewmember.UON3 = Null
    Else
        objnewmember.UON3 = vUON3
    End If
    
    If NoHayParametro(Id) Then
        objnewmember.Id = 0
    Else
        objnewmember.Id = Id
    End If
    
    If NoHayParametro(bBajaLog) Then
        objnewmember.BajaLog = False
    Else
        objnewmember.BajaLog = bBajaLog
    End If
    
    If NoHayParametro(vFecIniVal) Then
        objnewmember.FecIniVal = Null
    Else
        objnewmember.FecIniVal = vFecIniVal
    End If
    
    If NoHayParametro(vFecFinVal) Then
        objnewmember.FecFinVal = Null
    Else
        objnewmember.FecFinVal = vFecFinVal
    End If
    
    Set objnewmember.Conexion = m_oConexion
    Set objnewmember.ConexionServer = m_oConexionServer
    If Not IsMissing(varIndice) And Not IsNull(varIndice) Then
        objnewmember.Indice = varIndice
        mCol.Add objnewmember, CStr(varIndice)
    Else
        sCod = sPRES1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(sPRES1))
        sCod = sCod & sPRES2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON2 - Len(sPRES2))
        sCod = sCod & Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON3 - Len(Cod))
        mCol.Add objnewmember, sCod
    End If
    
    Set Add = objnewmember
    Set objnewmember = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CPresConceptos4Nivel3", "Add", ERR, Erl)
        Exit Function
    End If

End Function

Public Property Get Item(vntIndexKey As Variant) As CPresConcep4Nivel3
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Friend Property Set Conexion(ByVal con As CConexion)
Set m_oConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = m_oConexion
End Property

Friend Property Set ConexionServer(ByVal con As CConexionDeUseServer)
Set m_oConexionServer = con
End Property

Friend Property Get ConexionServer() As CConexionDeUseServer
Set ConexionServer = m_oConexionServer
End Property

Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
     Count = mCol.Count
End If


End Property


Public Sub Remove(vntIndexKey As Variant)
    mCol.Remove vntIndexKey
End Sub


Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
     Set NewEnum = mCol.[_NewEnum]
End Property


Private Sub Class_Initialize()
   

    Set mCol = New Collection
End Sub


Private Sub Class_Terminate()
    'destroys collection when this class is terminated
    Set mCol = Nothing
    Set m_oConexion = Nothing
    Set m_oConexionServer = Nothing
End Sub

Public Function DevolverLosCodigos() As TipoDatosCombo
   
   Dim Codigos As TipoDatosCombo
   Dim iCont As Integer
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
   ReDim Codigos.Cod(mCol.Count)
   ReDim Codigos.Den(mCol.Count)
   
    For iCont = 0 To mCol.Count - 1
        
        Codigos.Cod(iCont) = mCol(iCont + 1).Cod
        Codigos.Den(iCont) = mCol(iCont + 1).Den
    
    Next

   DevolverLosCodigos = Codigos
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CPresConceptos4Nivel3", "DevolverLosCodigos", ERR, Erl)
        Exit Function
    End If
   
End Function
Public Property Get eof() As Boolean
    eof = m_bEOF
End Property
Friend Property Let eof(ByVal b As Boolean)
    m_bEOF = b
End Property

Private Function HacerParteWhereUON(ByVal vUON1 As Variant, ByVal vUON2 As Variant, ByVal vUON3 As Variant) As String
'************************************************************************************************************
'*** Descripci�n: Compone un string que ser� parte de la clausula Where de una consulta. Ser�n las        ***
'***              condiciones referentes a las unidades organizativas.                                    ***
'***                                                                                                      ***
'*** Par�metros:  vUON1, vUON2, vUON3 ::>> De tipo variant contienen informaci�n sobre cada nivel de una  ***
'***                                       unidad organizativa.                                           ***
'***                                                                                                      ***
'*** Valor que devuelve: Un string con lo ya explicado en la descripci�n de la funci�n.                   ***
'************************************************************************************************************

    Dim sSalida As String

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sSalida = ""
    If NoHayParametro(vUON1) Then
        sSalida = sSalida & " AND UON1 IS NULL"
    Else
        sSalida = sSalida & " AND UON1='" & DblQuote(vUON1) & "'"
    End If
    If NoHayParametro(vUON2) Then
        sSalida = sSalida & " AND UON2 IS NULL"
    Else
        sSalida = sSalida & " AND UON2='" & DblQuote(vUON2) & "'"
    End If
    If NoHayParametro(vUON3) Then
        sSalida = sSalida & " AND UON3 IS NULL"
    Else
        sSalida = sSalida & " AND UON3='" & DblQuote(vUON3) & "'"
    End If
    HacerParteWhereUON = sSalida
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CPresConceptos4Nivel3", "HacerParteWhereUON", ERR, Erl)
        Exit Function
    End If
End Function

