VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CUnidadOrgCC"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'********************************************************
'clase creada para guardar en la tabla USU_CC_IMPUTACION
'********************************************************

Implements IBaseDatos

Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum

Private m_oConexion As CConexion

Private m_sUsu As String
Private m_vUON1 As Variant
Private m_vUON2 As Variant
Private m_vUON3 As Variant
Private m_vUON4 As Variant

Friend Property Set Conexion(ByVal con As CConexion)
Set m_oConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = m_oConexion
End Property

Public Property Let USU(ByVal vData As Variant)
    m_sUsu = vData
End Property
Public Property Get USU() As Variant
    USU = m_sUsu
End Property

Public Property Let UON1(ByVal vData As Variant)
    m_vUON1 = vData
End Property
Public Property Get UON1() As Variant
    UON1 = m_vUON1
End Property
Public Property Let UON2(ByVal vData As Variant)
    m_vUON2 = vData
End Property
Public Property Get UON2() As Variant
    UON2 = m_vUON2
End Property
Public Property Let UON3(ByVal vData As Variant)
    m_vUON3 = vData
End Property
Public Property Get UON3() As Variant
    UON3 = m_vUON3
End Property
Public Property Let UON4(ByVal vData As Variant)
    m_vUON4 = vData
End Property
Public Property Get UON4() As Variant
    UON4 = m_vUON4
End Property
Private Sub Class_Terminate()
Set m_oConexion = Nothing
End Sub

Private Function IBaseDatos_AnyadirABaseDatos() As TipoErrorSummit
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim TESError As TipoErrorSummit

TESError.NumError = TESnoerror

'********* Precondicion *******************
If m_oConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CUnidadOrgNivel2.AnyadirABAseDatos", "No se ha establecido la conexion"
End If
'******************************************

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
    
   m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
   bTransaccionEnCurso = True
   m_oConexion.ADOCon.Execute "SET XACT_ABORT OFF"

    sConsulta = "INSERT INTO USU_CC_IMPUTACION (USU, UON1,UON2,UON3,UON4) VALUES ("
    sConsulta = sConsulta & "'" & DblQuote(m_sUsu) & "',"
    sConsulta = sConsulta & "'" & DblQuote(m_vUON1) & "',"
    If Trim(m_vUON2) <> "" Then
        sConsulta = sConsulta & "'" & DblQuote(m_vUON2) & "',"
    Else
        sConsulta = sConsulta & "NULL,"
    End If
    If Trim(m_vUON3) <> "" Then
        sConsulta = sConsulta & "'" & DblQuote(m_vUON3) & "',"
    Else
        sConsulta = sConsulta & "NULL,"
    End If
    If Trim(m_vUON4) <> "" Then
        sConsulta = sConsulta & "'" & DblQuote(m_vUON4) & "')"
    Else
        sConsulta = sConsulta & "NULL)"
    End If
    
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    
    m_oConexion.ADOCon.Execute "COMMIT TRAN"
    bTransaccionEnCurso = False
       
    IBaseDatos_AnyadirABaseDatos = TESError
    Exit Function
Error_Cls:
   
    IBaseDatos_AnyadirABaseDatos = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    If bTransaccionEnCurso Then
        m_oConexion.ADOCon.Execute "ROLLBACK TRAN"
        bTransaccionEnCurso = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CUnidadOrgCC", "IBaseDatos_AnyadirABaseDatos", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If

End Function

Private Function IBaseDatos_CambiarCodigo(CodigoNuevo As Variant) As TipoErrorSummit

End Function

Private Sub IBaseDatos_CancelarEdicion()

End Sub

''' <summary>
''' Comprobar que el registro est� en la BD
''' </summary>
''' <returns>Boolean</returns>
''' <remarks>Llamada desde: FSGSClient;
''' Tiempo m�ximo: <1 seg</remarks>
''' <revision>JVS 10/01/2012</revision>
Private Function IBaseDatos_ComprobarExistenciaEnBaseDatos() As Boolean
Dim rs As New adodb.Recordset
Dim sConsulta As String

'******************* Precondicion *******************
If m_oConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CUnidadOrgNivel2.ComprobarExistenciaEnBaseDatos", "No se ha establecido la conexion"
End If
'*****************************************************

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

'    sConsulta = "SELECT USU FROM USU_CC_IMPUTACION WHERE UON1='" & DblQuote(m_vUON1)
'    sConsulta = sConsulta & "' AND UON2='" & DblQuote(m_vUON2)
'    sConsulta = sConsulta & "' AND UON3='" & DblQuote(m_vUON3)
'    sConsulta = sConsulta & "' AND UON4='" & DblQuote(m_vUON4)
'    sConsulta = sConsulta & "' AND USU='" & DblQuote(m_sUsu) & "'"
    
    sConsulta = "SELECT USU FROM USU_CC_IMPUTACION WITH(NOLOCK) WHERE "
    If Trim(m_vUON1) <> "" Then
        sConsulta = sConsulta & " UON1='" & m_vUON1 & "'"
    Else
        sConsulta = sConsulta & " UON1 IS NULL"
    End If
    If Trim(m_vUON2) <> "" Then
        sConsulta = sConsulta & " AND UON2='" & m_vUON2 & "'"
    Else
        sConsulta = sConsulta & " AND UON2 IS NULL"
    End If
    If Trim(m_vUON3) <> "" Then
        sConsulta = sConsulta & " AND UON3='" & m_vUON3 & "'"
    Else
        sConsulta = sConsulta & " AND UON3 IS NULL"
    End If
    If Trim(m_vUON4) <> "" Then
        sConsulta = sConsulta & " AND UON4='" & m_vUON4 & "'"
    Else
        sConsulta = sConsulta & " AND UON4 IS NULL"
    End If

    sConsulta = sConsulta & " AND USU='" & DblQuote(m_sUsu) & "'"
    
    
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    
    If rs.eof Then
        
        IBaseDatos_ComprobarExistenciaEnBaseDatos = False
    Else
        IBaseDatos_ComprobarExistenciaEnBaseDatos = True
    End If
        
    rs.Close
    Set rs = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CUnidadOrgCC", "IBaseDatos_ComprobarExistenciaEnBaseDatos", ERR, Erl)
      Exit Function
   End If

End Function

Private Function IBaseDatos_EliminarDeBaseDatos() As TipoErrorSummit

Dim TESError As TipoErrorSummit
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
   
    TESError.NumError = TESnoerror
    
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CUnidad.EliminarDeBaseDatos", "No se ha establecido la conexion"
    End If
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
       
    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
    bTransaccionEnCurso = True
  
    ''' Borrado
    sConsulta = "DELETE FROM USU_CC_IMPUTACION WHERE "
    sConsulta = sConsulta & " UON1 ='" & DblQuote(m_vUON1) & "'"
    If Trim(m_vUON2) <> "" Then
        sConsulta = sConsulta & " AND UON2='" & DblQuote(m_vUON2) & "'"
    Else
        sConsulta = sConsulta & " AND UON2 IS NULL"
    End If
    If Trim(m_vUON3) <> "" Then
        sConsulta = sConsulta & " AND UON3='" & DblQuote(m_vUON3) & "'"
    Else
        sConsulta = sConsulta & " AND UON3 IS NULL"
    End If
    If Trim(m_vUON4) <> "" Then
        sConsulta = sConsulta & " AND UON4='" & DblQuote(m_vUON4) & "'"
    Else
        sConsulta = sConsulta & " AND UON4 IS NULL"
    End If
    sConsulta = sConsulta & " AND USU='" & DblQuote(m_sUsu) & "'"
    
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
     ''' Terminar transaccion
    
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    IBaseDatos_EliminarDeBaseDatos = TESError
    
    Exit Function
        
Error_Cls:
        
    IBaseDatos_EliminarDeBaseDatos = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    
    If bTransaccionEnCurso Then
        m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
        bTransaccionEnCurso = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CUnidadOrgCC", "IBaseDatos_EliminarDeBaseDatos", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If
End Function
Private Function IBaseDatos_FinalizarEdicionEliminando() As TipoErrorSummit
End Function

Private Function IBaseDatos_FinalizarEdicionModificando() As TipoErrorSummit
End Function

Private Function IBaseDatos_IniciarEdicion(Optional ByVal Bloquear As Boolean, Optional ByVal UsuarioBloqueo As String) As TipoErrorSummit
End Function

'*****************************************************
'*****************************************************
'*****************************************************
'*****************************************************
'*****************************************************
'FSSM
'*****************************************************
'*****************************************************
'*****************************************************
'*****************************************************
'*****************************************************

''' <summary>
''' Comprobar que el registro est� en la BD
''' </summary>
''' <returns>Boolean</returns>
''' <remarks>Llamada desde: FSGSClient;
''' Tiempo m�ximo: <1 seg</remarks>
''' <revision>JVS 10/01/2012</revision>
Public Function ComprobarExistenciaEnBaseDatosFSSM() As Boolean
Dim rs As New adodb.Recordset
Dim sConsulta As String

'******************* Precondicion *******************
If m_oConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CUnidadOrgCC.ComprobarExistenciaEnBaseDatosFSSM", "No se ha establecido la conexion"
End If
'*****************************************************

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

    'SELECT USU, UON1, UON2, UON3, UON4, CONSULTA, MODIF, FECACT From USU_CC_CONTROL

    sConsulta = "SELECT USU FROM USU_CC_CONTROL WITH(NOLOCK) WHERE "
    sConsulta = sConsulta & " USU='" & DblQuote(m_sUsu) & "'"
    sConsulta = sConsulta & " AND UON1='" & DblQuote(m_vUON1) & "'"
    
    If IsNull(m_vUON2) Then
        sConsulta = sConsulta & " AND UON2 IS NULL"
    Else
        sConsulta = sConsulta & " AND UON2='" & DblQuote(m_vUON2) & "'"
    End If
    
    If IsNull(m_vUON3) Then
        sConsulta = sConsulta & " AND UON3 IS NULL"
    Else
        sConsulta = sConsulta & " AND UON3='" & DblQuote(m_vUON3) & "'"
    End If
    
    If IsNull(m_vUON4) Then
        sConsulta = sConsulta & " AND UON4 IS NULL"
    Else
        sConsulta = sConsulta & " AND UON4='" & DblQuote(m_vUON4) & "'"
    End If
    
    
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    
    If rs.eof Then
        
        ComprobarExistenciaEnBaseDatosFSSM = False
    Else
        ComprobarExistenciaEnBaseDatosFSSM = True
    End If
        
    rs.Close
    Set rs = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CUnidadOrgCC", "ComprobarExistenciaEnBaseDatosFSSM", ERR, Erl)
      Exit Function
   End If

End Function

Public Function EliminarDeBaseDatosFSSM() As TipoErrorSummit

Dim TESError As TipoErrorSummit
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
   
    TESError.NumError = TESnoerror
    
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CUnidadOrgCC.EliminarDeBaseDatosFSSM", "No se ha establecido la conexion"
    End If
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
       
    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
    bTransaccionEnCurso = True
  
    'SELECT USU, UON1, UON2, UON3, UON4, CONSULTA, MODIF, FECACT From USU_CC_CONTROL
  
    ''' Borrado
    sConsulta = "DELETE FROM USU_CC_CONTROL WHERE "
    sConsulta = sConsulta & " USU='" & DblQuote(m_sUsu) & "'"
    sConsulta = sConsulta & " AND UON1 ='" & DblQuote(m_vUON1) & "'"
    
    If IsNull(m_vUON2) Then
        sConsulta = sConsulta & " AND UON2 IS NULL"
    Else
        sConsulta = sConsulta & " AND UON2='" & DblQuote(m_vUON2) & "'"
    End If
    
    If IsNull(m_vUON3) Then
        sConsulta = sConsulta & " AND UON3 IS NULL"
    Else
        sConsulta = sConsulta & " AND UON3='" & DblQuote(m_vUON3) & "'"
    End If
    
    If IsNull(m_vUON4) Then
        sConsulta = sConsulta & " AND UON4 IS NULL"
    Else
        sConsulta = sConsulta & " AND UON4='" & DblQuote(m_vUON4) & "'"
    End If
    
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
     ''' Terminar transaccion
    
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    EliminarDeBaseDatosFSSM = TESError
    
    Exit Function
        
Error_Cls:
        
    EliminarDeBaseDatosFSSM = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    
    If bTransaccionEnCurso Then
        m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
        bTransaccionEnCurso = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CUnidadOrgCC", "EliminarDeBaseDatosFSSM", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If
End Function

'Descripcion: A�ade los permisos sobre las Partidas presupuestarias del usuario
'Parametros entrada:
'   pConsulta:Permiso consulta
'   pModif: Permiso Modificacion
'   pAlta: Permiso ALta
'   pPresup: Permiso Presupuestacion
'Llamada desde: frmUsuarios.frm --> GrabarUON
'Tiempo ejecucion:0,2seg.
Public Function AnyadirABaseDatosFSSM(pConsulta As Variant, pModif As Variant, pAlta As Variant, pPresup As Variant) As TipoErrorSummit
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim TESError As TipoErrorSummit

TESError.NumError = TESnoerror

'********* Precondicion ****************************************************************************
If m_oConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CUnidadOrgCC.AnyadirABaseDatosFSSM", "No se ha establecido la conexion"
End If
'***************************************************************************************************

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
    
   m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
   bTransaccionEnCurso = True
   m_oConexion.ADOCon.Execute "SET XACT_ABORT OFF"

    sConsulta = "INSERT INTO USU_CC_CONTROL (USU, UON1,UON2,UON3,UON4, CONSULTA, MODIF,ALTA, PRESUP) VALUES ('"
    sConsulta = sConsulta & m_sUsu & "','"
    sConsulta = sConsulta & m_vUON1
    sConsulta = sConsulta & "'," & NullToQuote(m_vUON2)
    sConsulta = sConsulta & "," & NullToQuote(m_vUON3)
    sConsulta = sConsulta & "," & NullToQuote(m_vUON4)
    If IsNull(pConsulta) Then
        sConsulta = sConsulta & ",0"
    Else
        sConsulta = sConsulta & ",1"
    End If
    
    If IsNull(pModif) Then
        sConsulta = sConsulta & ",0"
    Else
        sConsulta = sConsulta & ",1"
    End If
    
    If IsNull(pAlta) Then
        sConsulta = sConsulta & ",0"
    Else
        sConsulta = sConsulta & ",1"
    End If
    
    If IsNull(pPresup) Then
        sConsulta = sConsulta & ",0"
    Else
        sConsulta = sConsulta & ",1"
    End If
    
    sConsulta = sConsulta & ")"
    
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    
    m_oConexion.ADOCon.Execute "COMMIT TRAN"
    bTransaccionEnCurso = False
       
    AnyadirABaseDatosFSSM = TESError
    Exit Function
Error_Cls:
   
    AnyadirABaseDatosFSSM = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    If bTransaccionEnCurso Then
        m_oConexion.ADOCon.Execute "ROLLBACK TRAN"
        bTransaccionEnCurso = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CUnidadOrgCC", "AnyadirABaseDatosFSSM", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If

End Function
Private Function NullToQuote(ByVal ValueThatCanBeNull As Variant) As Variant
    If Not IsNull(ValueThatCanBeNull) Then
        NullToQuote = "'" & CStr(ValueThatCanBeNull) & "'"
    Else
        NullToQuote = "NULL"
    End If
End Function
