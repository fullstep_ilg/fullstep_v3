VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CSolCumplimentacionGs"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements IBaseDatos

Private m_lId As Long
Private m_lSolicitud As Long
Private m_lCampo As Long

Private m_bEscritura As Boolean
Private m_bObligatorio As Boolean

Private m_vFecAct As Variant

'Para mostrar cual es el campo en frmSolCumpli...GS
Private m_oCampo As CFormItem
Private m_vCodAtrib As Variant

''' Conexion
Private m_oConexion As CConexion

''' Control de errores
Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum

Public Property Get Id() As Long
    Id = m_lId
End Property
Public Property Let Id(ByVal Value As Long)
    m_lId = Value
End Property

Public Property Get Solicitud() As Long
    Solicitud = m_lSolicitud
End Property
Public Property Let Solicitud(ByVal Value As Long)
    m_lSolicitud = Value
End Property

Public Property Get IdCampo() As Long
    IdCampo = m_lCampo
End Property
Public Property Let IdCampo(ByVal Value As Long)
    m_lCampo = Value
End Property

Public Property Get Escritura() As Boolean
    Escritura = m_bEscritura
End Property
Public Property Let Escritura(ByVal bEscr As Boolean)
    m_bEscritura = bEscr
End Property

Public Property Get Obligatorio() As Boolean
    Obligatorio = m_bObligatorio
End Property
Public Property Let Obligatorio(ByVal bObl As Boolean)
    m_bObligatorio = bObl
End Property

Public Property Let FecAct(ByVal vData As Variant)
    m_vFecAct = vData
End Property

Public Property Get FecAct() As Variant
    FecAct = m_vFecAct
End Property

Public Property Let CodAtrib(ByVal vData As Variant)
    m_vCodAtrib = vData
End Property
Public Property Get CodAtrib() As Variant
    CodAtrib = m_vCodAtrib
End Property

Public Property Get Campo() As CFormItem
    Set Campo = m_oCampo
End Property
Public Property Set Campo(ByVal Value As CFormItem)
    Set m_oCampo = Value
End Property

Friend Property Set Conexion(ByVal vData As CConexion)
    Set m_oConexion = vData
End Property

Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property

Private Sub Class_Terminate()
    Set m_oConexion = Nothing
End Sub


Private Function IBaseDatos_AnyadirABaseDatos() As TipoErrorSummit

End Function

Private Function IBaseDatos_CambiarCodigo(CodigoNuevo As Variant) As TipoErrorSummit

End Function

Private Sub IBaseDatos_CancelarEdicion()

End Sub

Private Function IBaseDatos_ComprobarExistenciaEnBaseDatos() As Boolean

End Function

Private Function IBaseDatos_EliminarDeBaseDatos() As TipoErrorSummit

End Function

Private Function IBaseDatos_FinalizarEdicionEliminando() As TipoErrorSummit

End Function

Private Function IBaseDatos_FinalizarEdicionModificando() As TipoErrorSummit
    Dim TESError As TipoErrorSummit
    Dim btrans As Boolean
    Dim sConsulta As String
    Dim rs As ADODB.Recordset

    TESError.NumError = TESnoerror

    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not g_oErrores.fg_bProgramando Then On Error GoTo Error
    If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:

    ''' Precondicion
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CSolicitudCumplimentacionGs.FinalizarEdicionModificando", "No se ha establecido la conexion"
    End If
        
    m_oConexion.ADOCon.Execute "BEGIN TRANSACTION"
    btrans = True

    sConsulta = "SELECT FECACT FROM SOLICITUD_CUMPLIMENTACION_GS WHERE SOLICITUD = " & m_lSolicitud & " AND CAMPO = " & m_lCampo
    
    Set rs = New ADODB.Recordset
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly

    If rs.eof Then
        rs.Close
        Set rs = Nothing
        TESError.NumError = TESDatoEliminado
        IBaseDatos_FinalizarEdicionModificando = TESError
        m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
        Exit Function
    Else
        If rs.Fields("FECACT").Value <> m_vFecAct Then
            TESError.NumError = TESInfModificadaResum
            IBaseDatos_FinalizarEdicionModificando = TESError
            rs.Close
            Set rs = Nothing
            m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
            Exit Function
        End If
    End If
    
    sConsulta = "UPDATE SOLICITUD_CUMPLIMENTACION_GS SET ESCRITURA=" & BooleanToSQLBinary(m_bEscritura)
    sConsulta = sConsulta & ",OBLIGATORIO=" & BooleanToSQLBinary(m_bObligatorio)
    sConsulta = sConsulta & " WHERE SOLICITUD = " & m_lSolicitud & " AND CAMPO = " & m_lCampo
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    
    m_oConexion.ADOCon.Execute "COMMIT TRANSACTION"
    btrans = False
    
    rs.Requery
    m_vFecAct = rs(0).Value
    rs.Close
    Set rs = Nothing
    TESError.NumError = TESnoerror
    IBaseDatos_FinalizarEdicionModificando = TESError

    Exit Function
    
Error_Cls:
    TESError = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    If btrans Then
        m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
    End If
    Resume Salir:

Salir:
    On Error Resume Next
    If Not rs Is Nothing Then
        rs.Close
    End If
    IBaseDatos_FinalizarEdicionModificando = TESError
    '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
Error:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CSolicitudCumplimentacionGs", "IBaseDatos_FinalizarEdicionModificando", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If
End Function

Private Function IBaseDatos_IniciarEdicion(Optional ByVal Bloquear As Boolean, Optional ByVal UsuarioBloqueo As String) As TipoErrorSummit

End Function
