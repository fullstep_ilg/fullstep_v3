VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CUnidadNegQA"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum

Private mvarId As Integer 'local copy
Private munQA1 As Integer 'local copy
Private munQA2 As Integer 'local copy
Private mvarCod As String 'local copy
Private mvarDen As String 'local copy
Private mPyme As Integer 'local copy
Private mbaja As Boolean 'local copy
Private mNivel As Boolean 'local copy
Private m_oUnidadesNegQA As CUnidadesNegQA
Private mbSeleccionada As Boolean 'local copy

Private mPermiso As Integer  'local copy

Private mvarConexion As CConexion 'local copy
Private mvarConexionServer As CConexionDeUseServer  'local copy

Public Property Get Id() As Integer
    Id = mvarId
End Property
Public Property Let Id(ByVal Data As Integer)
    mvarId = Data
End Property

Public Property Get Cod() As String
    Cod = mvarCod
End Property
Public Property Let Cod(ByVal Data As String)
    mvarCod = Data
End Property

Public Property Get unQA1() As Integer
    unQA1 = munQA1
End Property
Public Property Let unQA1(ByVal Data As Integer)
    munQA1 = Data
End Property

Public Property Get unQA2() As Integer
    unQA2 = munQA2
End Property
Public Property Let unQA2(ByVal Data As Integer)
    munQA2 = Data
End Property

Friend Property Set Conexion(ByVal vData As CConexion)
    Set mvarConexion = vData
End Property
Friend Property Get Conexion() As CConexion
    Set Conexion = mvarConexion
End Property

Friend Property Set ConexionServer(ByVal con As CConexionDeUseServer)
    Set mvarConexionServer = con
End Property
Friend Property Get ConexionServer() As CConexionDeUseServer
    Set ConexionServer = mvarConexionServer
End Property

Private Sub Class_Terminate()
    Set mvarConexion = Nothing
    Set mvarConexionServer = Nothing
End Sub


Public Property Set UnidadesNegQA(ByVal vData As CUnidadesNegQA)
    Set m_oUnidadesNegQA = vData
End Property


Public Property Get UnidadesNegQA() As CUnidadesNegQA
    Set UnidadesNegQA = m_oUnidadesNegQA
End Property


Public Property Get Den() As String
    Den = mvarDen
End Property
Public Property Let Den(ByVal Data As String)
    mvarDen = Data
End Property


Public Property Get Seleccionada() As Boolean
    Seleccionada = mbSeleccionada
End Property
Public Property Let Seleccionada(ByVal Data As Boolean)
    mbSeleccionada = Data
End Property

Public Property Get Permiso() As Integer
    Permiso = mPermiso
End Property

Public Property Let Permiso(ByVal Data As Integer)
    mPermiso = Data
End Property

Public Function tieneAlgunaUNQASeleccionada() As Boolean
Dim oUnidad As CUnidadNegQA
    If Me.Seleccionada Then
        tieneAlgunaUNQASeleccionada = True
    Else
        If Not Me.UnidadesNegQA.Unidades Is Nothing Then
        For Each oUnidad In Me.UnidadesNegQA.Unidades
            If oUnidad.tieneAlgunaUNQASeleccionada() Then
                tieneAlgunaUNQASeleccionada = True
                Me.Seleccionada = True
                Exit For
            End If
        Next
        End If
    End If
End Function
