﻿Imports System.Configuration
Imports System.IO


Module GeneracionFacturas
    Private Enum TiempoProcesamiento
        InicioDisco = 1
        InicioBD = 2
        Fin = 3
    End Enum

    Private Const cnTipopProcesamientoXMLFSNWeb As Short = 1
    Private Const cnTipoSolicitudFactura As Short = 7

    Dim oFacturas As New Facturas
    Dim usuario As String
    Dim gSolicitud As Integer
    Private gFormulario As Integer
    Private gWorkflow As Integer
    Dim instancia As Integer
    Dim idbloque As Integer
    Dim idrol As Integer
    Dim workflow As Integer
    Dim sCodUsuario As String
    Dim sPwd As String
    Dim dateFecPwd As Date
    Dim numeroMax As Integer
    Dim FicheroError As System.IO.StreamWriter
    Dim dia As Integer
    Dim mes As Integer
    Dim anyo As Integer
    Dim empresa As Integer
    Dim NombresTags As DataTable
    Dim dFechaUltimaRecepcion As DateTime


    Public Sub Main()
        Dim gbAccesoFSSM As Boolean
        Dim dtEmpresas As DataSet

        dia = Date.Now.Day
        mes = Date.Now.Month
        anyo = Date.Now.Year

        Try
            dtEmpresas = oFacturas.EmpresasAAutoFacturar
            Dim data As DataSet
            Dim dtUsuario As DataTable

            If dtEmpresas IsNot Nothing Then
                'Si tengo empresas
                If dtEmpresas.Tables.Count >= 1 Then
                    data = oFacturas.Parametros_GetData
                    NombresTags = New DataTable 'tabla con los tags para remplazar los nombre de las tablas con valores numericos detras

                    gbAccesoFSSM = Encrypter.EncriptarAcceso("SM", DBNullToStr(data.Tables(0).Rows(0).Item("ACCESO_FSSM")), False) = "FULLSTEP SM"

                    For x As Integer = 0 To dtEmpresas.Tables(0).Rows.Count - 1
                        numeroMax = DBNullToInteger(dtEmpresas.Tables(0).Rows(x).Item("NUM_LIN_FACTURA"))
                        If numeroMax = 0 Then
                            numeroMax = 999999999 'Numero maximo cuando numeroMax es igual a 0
                        End If
                        gSolicitud = DBNullToInteger(dtEmpresas.Tables(0).Rows(x).Item("SOLICITUD"))
                        gFormulario = DBNullToInteger(dtEmpresas.Tables(0).Rows(x).Item("FORMULARIO"))
                        gWorkflow = DBNullToInteger(dtEmpresas.Tables(0).Rows(x).Item("WORKFLOW"))
                        empresa = DBNullToInteger(dtEmpresas.Tables(0).Rows(x).Item("ID"))
                        dFechaUltimaRecepcion = dtEmpresas.Tables(0).Rows(x).Item("FECHA")

                        dtUsuario = oFacturas.ObtenerDatosUsuario(gSolicitud)
                        If dtUsuario IsNot Nothing Then
                            If dtUsuario.Rows.Count >= 1 Then
                                usuario = DBNullToStr(dtUsuario.Rows(0).Item("PER"))
                                idrol = DBNullToInteger(dtUsuario.Rows(0).Item("IDROL"))
                                workflow = DBNullToInteger(dtUsuario.Rows(0).Item("WORKFLOW"))
                                sCodUsuario = DBNullToStr(dtUsuario.Rows(0).Item("COD"))
                                sPwd = DBNullToStr(dtUsuario.Rows(0).Item("PWD"))
                                dateFecPwd = DBNullToStr(dtUsuario.Rows(0).Item("FECPWD"))
                            End If
                        End If

                        If gbAccesoFSSM = True Then
                            GenerarDSAutoFactConCentroCoste(empresa)
                        Else
                            GenerarDSAutoFactSinCentroCoste(empresa)
                        End If
                    Next
                End If
            End If

        Catch ex As Exception

            If dtEmpresas IsNot Nothing Then
                GrabarError("En el main.", ex.Message)
            End If

        Finally
            If FicheroError IsNot Nothing Then
                FicheroError.Close()
            End If
        End Try
    End Sub
    '''<summary>
    ''' Obtener los datos para las auto facturas de una empresa determinada
    ''' </summary>  
    '''  <param name="empresa">int que contiene el id de la empresa</param>
    ''' <remarks>Llamada desde:GeneracionFacturas.vb main; Tiempo máximo: segundos</remarks>
    Private Sub GenerarDSAutoFactConCentroCoste(ByVal empresa As Integer)

        Dim dsCargarDatos As DataSet ' obtener todos los datos
        dsCargarDatos = oFacturas.Facturas_CargarDatos(empresa, dFechaUltimaRecepcion)

        'PARA GENERAR EL XML ME HACE FALTA PONER MONEDA Y PROVEDOR

        'INICIO LOG CON TODOS LOS DATOS QUE VOY A FACTURAR
        Try
            Dim Fichero As System.IO.StreamWriter

            Fichero = New System.IO.StreamWriter(ConfigurationManager.AppSettings("rutaLogError") & empresa & "-" & dia & "-" & mes & "-" & anyo & ".log")
            Fichero.WriteLine(empresa)
            'AÑADO LAS LINEAS DE FACTURACION LIBRE O SIN RECEPCIONES
            If dsCargarDatos.Tables("LINEAS") IsNot Nothing Then
                If dsCargarDatos.Tables(0).Rows.Count >= 1 Then
                    Fichero.WriteLine("Pedidos con lineas de facturacion libre o sin recepciones")
                    Fichero.WriteLine("---------------------------------------------------------")
                    For w As Integer = 0 To dsCargarDatos.Tables("LINEAS").Rows.Count - 1
                        Fichero.WriteLine(dsCargarDatos.Tables("LINEAS").Rows(w).Item("ID") & "        " & dsCargarDatos.Tables("LINEAS").Rows(w).Item("ID1"))
                    Next
                End If
            End If

            'AÑADO LAS LINEAS CON FACTURACION TRAS RECEPCIONES
            If dsCargarDatos.Tables("LINEAS_CON_RECEPCIONES") IsNot Nothing Then
                If dsCargarDatos.Tables(1).Rows.Count >= 1 Then
                    Fichero.WriteLine("Pedidos con lineas de facturacion tras recepciones")
                    Fichero.WriteLine("--------------------------------------------------")
                    For w As Integer = 0 To dsCargarDatos.Tables("LINEAS_CON_RECEPCIONES").Rows.Count - 1
                        Fichero.WriteLine(dsCargarDatos.Tables("LINEAS_CON_RECEPCIONES").Rows(w).Item("PEDIDO") & "        " & dsCargarDatos.Tables("LINEAS_CON_RECEPCIONES").Rows(w).Item("ID1") & "        " & dsCargarDatos.Tables("LINEAS_CON_RECEPCIONES").Rows(w).Item("ALBARAN"))
                    Next
                End If
            End If

            Fichero.Close()

            'FIN LOG CON TODOS LOS DATOS QUE VOY A FACTURAR

        Catch ex As Exception
            GrabarError("No se ha podido generar el fichero Log de la empresa: " & DBNullToInteger(empresa) & ".", ex.Message)
        End Try

        GenerarXMLPorGastos_Inversiones_PorCentroCoste(dsCargarDatos, empresa, 0)
        GenerarXMLPorGastos_Inversiones_PorCentroCoste(dsCargarDatos, empresa, 1)

    End Sub

    '''<summary>
    ''' Crear un dataset que contenga la  estructura del dataSet para autofacturas)
    ''' </summary>
    ''' <returns>dataset con la estructura a pasar al dataset</returns>
    ''' <remarks>Llamada desde:GeneracionFacturas.vb GenerarXMLPorCentroCosteIMP y GenerarXMLPorImpuestos; Tiempo máximo: segundos</remarks>
    Private Function CargarEstructuraDataSetSimple(ByVal empresa As Integer) As DataSet

        Dim dt As DataTable
        Dim ds As DataSet
        Dim orden_entrega As DataColumn
        Dim ret_garantia As DataColumn
        Dim grp_comp As DataColumn 'GRP_COMP

        'INICIO CARGAR ESTRUCTURA DE FACTURA 
        ds = New DataSet
        dt = ds.Tables.Add("FACTURA")

        Try
            dt.Columns.Add("ID", System.Type.GetType("System.Int32"))
            dt.Columns.Add("NUM", System.Type.GetType("System.Int32"))
            dt.Columns.Add("PROVE", System.Type.GetType("System.String"))
            dt.Columns.Add("ESTADO", System.Type.GetType("System.Int32"))
            dt.Columns.Add("FECHA", System.Type.GetType("System.DateTime"))
            dt.Columns.Add("IMPORTE", System.Type.GetType("System.Double"))
            dt.Columns.Add("FEC_CONT", System.Type.GetType("System.DateTime"))
            dt.Columns.Add("EMPRESA", System.Type.GetType("System.Int32"))
            dt.Columns.Add("INSTANCIA", System.Type.GetType("System.Int32"))
            dt.Columns.Add("RET_GARANTIA", System.Type.GetType("System.Double"))
            dt.Columns.Add("PAG", System.Type.GetType("System.String"))
            dt.Columns.Add("VIA_PAG", System.Type.GetType("System.String"))
            dt.Columns.Add("OBS", System.Type.GetType("System.String"))
            dt.Columns.Add("TIPO", System.Type.GetType("System.Int32"))
            dt.Columns.Add("NUM_ORIGINAL", System.Type.GetType("System.Int32"))
            dt.Columns.Add("FEC_ORIGINAL_INI", System.Type.GetType("System.DateTime"))
            dt.Columns.Add("FEC_ORIGINAL_FIN", System.Type.GetType("System.DateTime"))
            dt.Columns.Add("TOT_COSTES", System.Type.GetType("System.Double"))
            dt.Columns.Add("TOT_DESCUENTOS", System.Type.GetType("System.Double"))
            dt.Columns.Add("MON", System.Type.GetType("System.String"))
            dt.Columns.Add("CONCEPTO", System.Type.GetType("System.Int32"))
            orden_entrega = New DataColumn("ORDEN_ENTREGA", System.Type.GetType("System.Int32"))
            orden_entrega.ColumnMapping = MappingType.Hidden
            dt.Columns.Add(orden_entrega)

            dt = ds.Tables.Add("LINEAS") 'CARGO ESTRUCTURA DE LINEAS

            dt.Columns.Add("LINEA", System.Type.GetType("System.Int32"))
            dt.Columns.Add("NUM", System.Type.GetType("System.Int32"))
            dt.Columns.Add("TOT_COSTES", System.Type.GetType("System.Double"))
            dt.Columns.Add("TOT_DESCUENTOS", System.Type.GetType("System.Double"))
            dt.Columns.Add("IMPUESTOS", System.Type.GetType("System.Double"))
            dt.Columns.Add("IMP_BRUTO", System.Type.GetType("System.Double"))
            dt.Columns.Add("IMP_NETO", System.Type.GetType("System.Double"))
            dt.Columns.Add("OBS", System.Type.GetType("System.String"))
            dt.Columns.Add("LIN_PEDIDO", System.Type.GetType("System.Int32"))
            dt.Columns.Add("ALBARAN", System.Type.GetType("System.String"))
            dt.Columns.Add("COSTES", System.Type.GetType("System.Double"))
            dt.Columns.Add("COD", System.Type.GetType("System.Int32"))
            dt.Columns.Add("DEN", System.Type.GetType("System.Int32"))
            dt.Columns.Add("VALOR", System.Type.GetType("System.Double"))
            dt.Columns.Add("IMPORTE", System.Type.GetType("System.Double"))
            dt.Columns.Add("OPERACION", System.Type.GetType("System.Int32"))
            'añado retencion de garantia por cada linea porque ya que la calculo guardo el dato y no lo tengo que volver a calcular
            ret_garantia = New DataColumn("RET_GARANTIA", System.Type.GetType("System.Double"))
            ret_garantia.ColumnMapping = MappingType.Hidden 'MPF EXPEDIENTE RET_GARANTIA
            dt.Columns.Add(ret_garantia)

            dt = ds.Tables.Add("COSTES")

            dt.Columns.Add("LINEA", System.Type.GetType("System.Int32"))
            dt.Columns.Add("ID", System.Type.GetType("System.Int32"))
            dt.Columns.Add("DEN", System.Type.GetType("System.String"))
            dt.Columns.Add("OPERACION", System.Type.GetType("System.String"))
            dt.Columns.Add("VALOR", System.Type.GetType("System.Double"))
            dt.Columns.Add("IMPORTE", System.Type.GetType("System.Double"))
            dt.Columns.Add("PLANIFICADO", System.Type.GetType("System.Int32"))

            dt = ds.Tables.Add("DESCUENTOS")

            dt.Columns.Add("LINEA", System.Type.GetType("System.Int32"))
            dt.Columns.Add("ID", System.Type.GetType("System.Int32"))
            dt.Columns.Add("DEN", System.Type.GetType("System.String"))
            dt.Columns.Add("OPERACION", System.Type.GetType("System.String"))
            dt.Columns.Add("VALOR", System.Type.GetType("System.Double"))
            dt.Columns.Add("IMPORTE", System.Type.GetType("System.Double"))
            dt.Columns.Add("PLANIFICADO", System.Type.GetType("System.Int32"))

            dt = ds.Tables.Add("LINEAS_CON_RECEPCIONES") 'CARGO ESTRUCTURA DE LINEAS CON RECEPCIONES

            dt.Columns.Add("LINEA", System.Type.GetType("System.Int32"))
            dt.Columns.Add("NUM", System.Type.GetType("System.Int32"))
            dt.Columns.Add("TOT_COSTES", System.Type.GetType("System.Double"))
            dt.Columns.Add("TOT_DESCUENTOS", System.Type.GetType("System.Double"))
            dt.Columns.Add("IMPUESTOS", System.Type.GetType("System.Double"))
            dt.Columns.Add("IMP_BRUTO", System.Type.GetType("System.Double"))
            dt.Columns.Add("IMP_NETO", System.Type.GetType("System.Double"))
            dt.Columns.Add("OBS", System.Type.GetType("System.String"))
            dt.Columns.Add("LIN_PEDIDO", System.Type.GetType("System.Int32"))
            dt.Columns.Add("ALBARAN", System.Type.GetType("System.String"))
            dt.Columns.Add("COSTES", System.Type.GetType("System.Double"))
            dt.Columns.Add("COD", System.Type.GetType("System.Int32"))
            dt.Columns.Add("DEN", System.Type.GetType("System.Int32"))
            dt.Columns.Add("VALOR", System.Type.GetType("System.Double"))
            dt.Columns.Add("IMPORTE", System.Type.GetType("System.Double"))
            dt.Columns.Add("OPERACION", System.Type.GetType("System.Int32"))
            'añado retencion de garantia por cada linea porque ya que la calculo guardo el dato y no lo tengo que volver a calcular
            ret_garantia = New DataColumn("RET_GARANTIA", System.Type.GetType("System.Double"))
            ret_garantia.ColumnMapping = MappingType.Hidden
            dt.Columns.Add(ret_garantia)

            dt = ds.Tables.Add("IMPUESTOS") 'CARGO ESTRUCTURA DE LINEAS CON RECEPCIONES

            dt.Columns.Add("LINEA", System.Type.GetType("System.Int32"))
            dt.Columns.Add("COD", System.Type.GetType("System.String")) 'impuesto.cod es un string 
            dt.Columns.Add("DEN", System.Type.GetType("System.String"))
            dt.Columns.Add("VALOR", System.Type.GetType("System.Double")) 'NO DEJA FLOAT ASI QUE USO DOUBLE 
            dt.Columns.Add("IMPORTE", System.Type.GetType("System.Double"))
            dt.Columns.Add("RETENIDO", System.Type.GetType("System.Double"))
            dt.Columns.Add("COMENT", System.Type.GetType("System.String"))
            'añado la columna grupo de compatibilidad pero no la tengo que mostrar
            grp_comp = New DataColumn("GRP_COMP", System.Type.GetType("System.Int32"))
            grp_comp.ColumnMapping = MappingType.Hidden
            dt.Columns.Add(grp_comp) 'ANEXO

            dt = ds.Tables.Add("LINEAS_CENTROS_COSTES")

            dt.Columns.Add("linea", System.Type.GetType("System.Int32"))
            dt.Columns.Add("UON1", System.Type.GetType("System.String"))
            dt.Columns.Add("UON2", System.Type.GetType("System.String"))
            dt.Columns.Add("UON3", System.Type.GetType("System.String"))
            dt.Columns.Add("UON4", System.Type.GetType("System.String"))

        Catch ex As Exception
            GrabarError("No se pudo cargar la estructura del dataset.", ex.Message)
        End Try
        ''FIN CARGAR ESTRUCTURA DATASET

        Return ds
    End Function
    '''<summary>
    ''' Crear un dataset que contenga la  estructura del dataSet para autofacturas)
    ''' </summary>
    ''' <returns>dataset con la estructura a pasar al dataset</returns>
    ''' <remarks>Llamada desde:GeneracionFacturas.vb GenerarXMLPorCentroCosteIMP y GenerarXMLPorImpuestos; Tiempo máximo: segundos</remarks>
    Public Function CargarEstructuraDataSetParaSchema() As DataSet
        Dim dt As DataTable
        Dim ds As DataSet
        Dim orden_entrega As DataColumn
        Dim ret_garantia As DataColumn

        'INICIO CARGAR ESTRUCTURA DE FACTURA 
        ds = New DataSet
        dt = ds.Tables.Add("FACTURA")

        Try
            dt.Columns.Add("ID", System.Type.GetType("System.Int32"))
            dt.Columns.Add("NUM", System.Type.GetType("System.Int32"))
            dt.Columns.Add("PROVE", System.Type.GetType("System.String"))
            dt.Columns.Add("ESTADO", System.Type.GetType("System.Int32"))
            dt.Columns.Add("FECHA", System.Type.GetType("System.DateTime"))
            dt.Columns.Add("IMPORTE", System.Type.GetType("System.Double"))
            dt.Columns.Add("FEC_CONT", System.Type.GetType("System.DateTime"))
            dt.Columns.Add("EMPRESA", System.Type.GetType("System.Int32"))
            dt.Columns.Add("INSTANCIA", System.Type.GetType("System.Int32"))
            dt.Columns.Add("RET_GARANTIA", System.Type.GetType("System.Double"))
            dt.Columns.Add("PAG", System.Type.GetType("System.String"))
            dt.Columns.Add("VIA_PAG", System.Type.GetType("System.String"))
            dt.Columns.Add("OBS", System.Type.GetType("System.String"))
            dt.Columns.Add("TIPO", System.Type.GetType("System.Int32"))
            dt.Columns.Add("NUM_ORIGINAL", System.Type.GetType("System.Int32"))
            dt.Columns.Add("FEC_ORIGINAL_INI", System.Type.GetType("System.DateTime"))
            dt.Columns.Add("FEC_ORIGINAL_FIN", System.Type.GetType("System.DateTime"))
            dt.Columns.Add("TOT_COSTES", System.Type.GetType("System.Double"))
            dt.Columns.Add("TOT_DESCUENTOS", System.Type.GetType("System.Double"))
            dt.Columns.Add("MON", System.Type.GetType("System.String"))
            dt.Columns.Add("CONCEPTO", System.Type.GetType("System.Int32"))
            orden_entrega = New DataColumn("ORDEN_ENTREGA", System.Type.GetType("System.Int32"))
            orden_entrega.ColumnMapping = MappingType.Hidden
            dt.Columns.Add(orden_entrega)

            dt = ds.Tables.Add("LINEAS") 'CARGO ESTRUCTURA DE LINEAS

            dt.Columns.Add("LINEA", System.Type.GetType("System.Int32"))
            dt.Columns.Add("NUM", System.Type.GetType("System.Int32"))
            dt.Columns.Add("TOT_COSTES", System.Type.GetType("System.Double"))
            dt.Columns.Add("TOT_DESCUENTOS", System.Type.GetType("System.Double"))
            dt.Columns.Add("IMPUESTOS", System.Type.GetType("System.Double"))
            dt.Columns.Add("IMP_BRUTO", System.Type.GetType("System.Double"))
            dt.Columns.Add("IMP_NETO", System.Type.GetType("System.Double"))
            dt.Columns.Add("OBS", System.Type.GetType("System.String"))
            dt.Columns.Add("LIN_PEDIDO", System.Type.GetType("System.Int32"))
            dt.Columns.Add("ALBARAN", System.Type.GetType("System.String"))
            dt.Columns.Add("COSTES", System.Type.GetType("System.Double"))
            dt.Columns.Add("COD", System.Type.GetType("System.Int32"))
            dt.Columns.Add("DEN", System.Type.GetType("System.Int32"))
            dt.Columns.Add("VALOR", System.Type.GetType("System.Double"))
            dt.Columns.Add("IMPORTE", System.Type.GetType("System.Double"))
            dt.Columns.Add("OPERACION", System.Type.GetType("System.Int32"))
            'añado retencion de garantia por cada linea porque ya que la calculo guardo el dato y no lo tengo que volver a calcular
            ret_garantia = New DataColumn("RET_GARANTIA", System.Type.GetType("System.Double"))
            ret_garantia.ColumnMapping = MappingType.Hidden
            dt.Columns.Add(ret_garantia)

            dt = ds.Tables.Add("COSTE")

            dt.Columns.Add("LINEA", System.Type.GetType("System.Int32"))
            dt.Columns.Add("ID", System.Type.GetType("System.Int32"))
            dt.Columns.Add("DEN", System.Type.GetType("System.String"))
            dt.Columns.Add("OPERACION", System.Type.GetType("System.String"))
            dt.Columns.Add("VALOR", System.Type.GetType("System.Double"))
            dt.Columns.Add("IMPORTE", System.Type.GetType("System.Double"))
            dt.Columns.Add("PLANIFICADO", System.Type.GetType("System.Int32"))

            dt = ds.Tables.Add("DESCUENTO")

            dt.Columns.Add("LINEA", System.Type.GetType("System.Int32"))
            dt.Columns.Add("ID", System.Type.GetType("System.Int32"))
            dt.Columns.Add("DEN", System.Type.GetType("System.String"))
            dt.Columns.Add("OPERACION", System.Type.GetType("System.String"))
            dt.Columns.Add("VALOR", System.Type.GetType("System.Double"))
            dt.Columns.Add("IMPORTE", System.Type.GetType("System.Double"))
            dt.Columns.Add("PLANIFICADO", System.Type.GetType("System.Int32"))

            dt = ds.Tables.Add("LINEAS_CON_RECEPCIONES") 'CARGO ESTRUCTURA DE LINEAS CON RECEPCIONES

            dt.Columns.Add("LINEA", System.Type.GetType("System.Int32"))
            dt.Columns.Add("NUM", System.Type.GetType("System.Int32"))
            dt.Columns.Add("TOT_COSTES", System.Type.GetType("System.Double"))
            dt.Columns.Add("TOT_DESCUENTOS", System.Type.GetType("System.Double"))
            dt.Columns.Add("IMPUESTOS", System.Type.GetType("System.Double"))
            dt.Columns.Add("IMP_BRUTO", System.Type.GetType("System.Double"))
            dt.Columns.Add("IMP_NETO", System.Type.GetType("System.Double"))
            dt.Columns.Add("OBS", System.Type.GetType("System.String"))
            dt.Columns.Add("LIN_PEDIDO", System.Type.GetType("System.Int32"))
            dt.Columns.Add("ALBARAN", System.Type.GetType("System.String"))
            dt.Columns.Add("COSTES", System.Type.GetType("System.Double"))
            dt.Columns.Add("COD", System.Type.GetType("System.Int32"))
            dt.Columns.Add("DEN", System.Type.GetType("System.Int32"))
            dt.Columns.Add("VALOR", System.Type.GetType("System.Double"))
            dt.Columns.Add("IMPORTE", System.Type.GetType("System.Double"))
            dt.Columns.Add("OPERACION", System.Type.GetType("System.Int32"))
            'añado retencion de garantia por cada linea porque ya que la calculo guardo el dato y no lo tengo que volver a calcular
            ret_garantia = New DataColumn("RET_GARANTIA", System.Type.GetType("System.Double"))
            ret_garantia.ColumnMapping = MappingType.Hidden
            dt.Columns.Add(ret_garantia)

            dt = ds.Tables.Add("IMPUESTO") 'CARGO ESTRUCTURA DE LINEAS CON RECEPCIONES

            dt.Columns.Add("LINEA", System.Type.GetType("System.Int32"))
            dt.Columns.Add("COD", System.Type.GetType("System.String")) 'impuesto.cod es un string 
            dt.Columns.Add("DEN", System.Type.GetType("System.String"))
            dt.Columns.Add("VALOR", System.Type.GetType("System.Double")) 'NO DEJA FLOAT ASI QUE USO DOUBLE 
            dt.Columns.Add("IMPORTE", System.Type.GetType("System.Double"))
            dt.Columns.Add("RETENIDO", System.Type.GetType("System.Double"))
            dt.Columns.Add("COMENT", System.Type.GetType("System.String"))

            dt = ds.Tables.Add("SOLICITUD")

            With dt.Columns
                .Add("TIPO_PROCESAMIENTO_XML")
                .Add("TIPO_DE_SOLICITUD")
                .Add("COMPLETO")
                .Add("CODIGOUSUARIO")
                .Add("INSTANCIA")
                .Add("SOLICITUD")
                .Add("USUARIO")
                .Add("USUARIO_EMAIL")
                .Add("USUARIO_IDIOMA")
                .Add("PEDIDO_DIRECTO")
                .Add("FORMULARIO")
                .Add("WORKFLOW")
                .Add("IMPORTE", System.Type.GetType("System.Double"))
                .Add("COMENTALTANOCONF")
                .Add("ACCION")
                .Add("IDACCION")
                .Add("IDACCIONFORM")
                .Add("GUARDAR")
                .Add("NOTIFICAR")
                .Add("TRASLADO_USUARIO")
                .Add("TRASLADO_PROVEEDOR")
                .Add("TRASLADO_FECHA")
                .Add("TRASLADO_COMENTARIO")
                .Add("TRASLADO_PROVEEDOR_CONTACTO")
                .Add("DEVOLUCION_COMENTARIO")
                .Add("COMENTARIO")
                .Add("BLOQUE_ORIGEN")
                .Add("NUEVO_ID_INSTANCIA")
                .Add("BLOQUES_DESTINO")
                .Add("ROL_ACTUAL")
                .Add("NOCONFORMIDAD")       '27
                .Add("CERTIFICADO")         '28
                .Add("CONTRATO")            '29
                .Add("FACTURA")             '30
                .Add("PEDIDO")          '31
                .Add("IDTIEMPOPROC")
            End With

        Catch ex As Exception
            GrabarError("No se pudo cargar la estructura del dataset para generar el Schema.", ex.Message)
        End Try
        ''FIN CARGAR ESTRUCTURA DATASET

        Return ds
    End Function
    '''<summary>
    ''' ''GENERO LOS XML VALIDANDO QUE NO SE SUPERE EL NUMERO MAXIMO DE LINEAS Y QUE NO SE DEJE UNAS LINEAS ALBARAN EN DISTINTAS FACTURAS"
    ''' </summary>
    ''' <remarks>Llamada desde:GenerarFacturas.vb; Tiempo máximo: segundos</remarks>
    Private Sub GenerarXMLConLineaMaxima(
       ByVal dsTablas As DataSet, numMax As Integer)

        Dim dtSolicitudAux As DataTable
        Dim posicion As Integer = 0

        Dim dtaux As DataTable
        dtaux = New DataTable
        If (dsTablas Is Nothing) Then _
            Throw New ArgumentNullException("Dataset")

        Dim proveedor As String
        Dim tipoMoneda As String
        Dim numfacturas As Integer
        Dim albaranTotal_ult As Double
        Dim ret_garantia_ult As Double
        Dim importe_noAlbaran As Double
        Dim ret_garantia_noalbaran As Double

        numfacturas = 0
        proveedor = dsTablas.Tables(0).Rows(0).Item("PROVE")
        tipoMoneda = dsTablas.Tables(0).Rows(0).Item("MON")
        albaranTotal_ult = dsTablas.Tables(0).Rows(0).Item("IMPORTE") 'Cargo importes totales 
        ret_garantia_ult = dsTablas.Tables(0).Rows(0).Item("RET_GARANTIA") ' Cargo retenciones totales
        importe_noAlbaran = 0
        ret_garantia_noalbaran = 0

        Dim AlbImporte As Double
        Dim AlbRetGarantia As Double

        'INICIO RECORRER LIMITE XXX LINEAS 
        Dim contador As Integer = 0
        Dim numlineas As Integer = 0
        Dim nombreTabla As String

        Dim dsAuxMaximoLineas As DataSet
        dsAuxMaximoLineas = New DataSet

        Dim dtlinea As DataTable
        Dim codigoFacturas As Integer = 0 ' codigo para diferenciar el nombre de la factura 1 a la 2
        Dim LineaPed_LineaRecep As Integer = 0 '0 Linea Pedido 1 Linea Recep Si es linea Pedido la ultima linea no tengo que comparar mas 
        Dim AlbaranUltLinea As String
        Dim Pos_LineaPed_LineaRecep As Integer = 0 ' posicion de la linea
        Dim LineaPedido As Integer = 0

        AlbaranUltLinea = 0

        Dim dsMismoAlbaran As DataSet = Nothing

        Try

            While contador < dsTablas.Tables.Count

                nombreTabla = dsTablas.Tables(contador).TableName
                dtlinea = dsTablas.Tables(contador).Copy

                dsAuxMaximoLineas.Tables.Add(dtlinea)
                'inicio añadiendo importes y retencion para cuando no tenmos albaran
                If dtlinea.TableName.StartsWith("LINEA") Then
                    importe_noAlbaran = importe_noAlbaran + dtlinea.Rows(0).Item("IMPORTE")
                    ret_garantia_noalbaran = ret_garantia_noalbaran + dtlinea.Rows(0).Item("RET_GARANTIA")
                ElseIf dtlinea.TableName.StartsWith("COSTE") Or dtlinea.TableName.StartsWith("IMPUESTOS") Then
                    importe_noAlbaran = importe_noAlbaran + dtlinea.Rows(0).Item("IMPORTE")
                ElseIf dtlinea.TableName.StartsWith("DESCUENTO") Then
                    importe_noAlbaran = importe_noAlbaran - dtlinea.Rows(0).Item("IMPORTE")
                End If
                'fin añadiendo importes para cuando no tenenos albaran

                If nombreTabla.StartsWith("LINEA") Then
                    numlineas = numlineas + 1

                    'INCIO CAMBIAR EL NUM DE CADA LINEA DE CADA FACTURA FACTURA
                    dtlinea.Rows(0).Item("NUM") = numlineas  ' CAMBIANDO EL DTLINEA ME CAMBIA TAMBIEN EL DEL DATASET
                    dtlinea.Rows(0).Item("LINEA") = numlineas
                    'FIN CAMBIAR EL NUM DE CADA LINEA DE CADA FACTURA FACTURA

                    'COMPROBAR ULTIMO ALBARAN 'NO ME VALE CON IR COMPROBANDO SOLO EL ULTIMO ALBARAN TENGO QUE HACERLO DESDE EL PRINCIPIO POR SI SE REPITE
                    If nombreTabla.StartsWith("LINEAS_CON_RECEPCIONES") Then
                        LineaPed_LineaRecep = 1 ' ES LINEA RECEP
                        If Pos_LineaPed_LineaRecep = 0 Then
                            AlbaranUltLinea = DBNullToStr(dtlinea.Rows(0).Item("ALBARAN"))
                            Pos_LineaPed_LineaRecep = contador 'guardo la posicion del albaran
                        ElseIf Not (AlbaranUltLinea.Equals(DBNullToStr(dtlinea.Rows(0).Item("ALBARAN")))) Then
                            'Si no es tienen el mismo albaran cambio el albaran y la poscion si no la dejo como estaba
                            AlbaranUltLinea = DBNullToStr(dtlinea.Rows(0).Item("ALBARAN"))
                            Pos_LineaPed_LineaRecep = contador
                        End If
                    Else
                        LineaPed_LineaRecep = 0 ' ES LINEA PEDIDO
                    End If

                    'ejemplo If numlineas > 99 Then para 100 lineas
                    If numlineas > numMax - 1 Then
                        contador = contador + 1

                        While Not (numlineas = 0 AndAlso dsAuxMaximoLineas.Tables(0).TableName = "FACTURA") AndAlso contador < dsTablas.Tables.Count
                            If dsTablas.Tables(contador) IsNot Nothing Then

                                nombreTabla = dsTablas.Tables(contador).TableName
                                If nombreTabla.StartsWith("LINEA") Then
                                    'AQUI TENGO EL DATATABLE DE LA PRIMERA LINEA DE LA SIQUIENTE FACTURA

                                    If LineaPed_LineaRecep = 1 AndAlso nombreTabla.StartsWith("LINEAS_CON_RECEPCIONES") AndAlso dsTablas.Tables(contador).Rows(0).Item("ALBARAN").Equals(AlbaranUltLinea) Then
                                        'QUE LA ULTIMA LINEA SEA DE RECEPCION QUE LA PRIMERA (DEL SIGUIENTE) TAMBIEN QUE TENGAN EL MISMO ALBARAN
                                        'RECORRO EL DATASET HASTA ENCONTRAR ESTA LINEA

                                        AlbImporte = 0
                                        AlbRetGarantia = 0

                                        dsMismoAlbaran = New DataSet
                                        For z As Integer = 0 To Pos_LineaPed_LineaRecep - 1
                                            'calculo el importe y la retencion de garantia CON -1 PARA QUE NO ME SAQUE LINEA_RECEP

                                            dsMismoAlbaran.Tables.Add(dsAuxMaximoLineas.Tables(z).Copy)
                                            If dsAuxMaximoLineas.Tables(z).TableName.StartsWith("LINEA") Then
                                                AlbImporte = AlbImporte + dsAuxMaximoLineas.Tables(z).Rows(0).Item("IMPORTE")
                                                AlbRetGarantia = AlbRetGarantia + dsAuxMaximoLineas.Tables(z).Rows(0).Item("RET_GARANTIA")
                                            ElseIf dsAuxMaximoLineas.Tables(z).TableName.StartsWith("COSTE") Or dsAuxMaximoLineas.Tables(z).TableName.StartsWith("IMPUESTOS") Then
                                                AlbImporte = AlbImporte + dsAuxMaximoLineas.Tables(z).Rows(0).Item("IMPORTE")
                                            ElseIf dsAuxMaximoLineas.Tables(z).TableName.StartsWith("DESCUENTO") Then
                                                AlbImporte = AlbImporte - dsAuxMaximoLineas.Tables(z).Rows(0).Item("IMPORTE")
                                            End If
                                        Next

                                        dsMismoAlbaran.Tables(0).Rows(0).Item("IMPORTE") = AlbImporte
                                        dsMismoAlbaran.Tables(0).Rows(0).Item("RET_GARANTIA") = AlbRetGarantia
                                        'voy restanto al importe y la ret_total lo que ya he ido sacando
                                        albaranTotal_ult = albaranTotal_ult - AlbImporte
                                        ret_garantia_ult = ret_garantia_ult - AlbRetGarantia
                                    End If

                                    If dsMismoAlbaran IsNot Nothing Then
                                        dsMismoAlbaran.DataSetName = dsAuxMaximoLineas.DataSetName

                                        If dsMismoAlbaran.Tables.Count <= 1 Then
                                            GrabarError("El albarán tiene mayor número de líneas que el nº máximo de líneas permitidas en una factura (" & numMax & " líneas). El albarán que nos da este problema es:  " & AlbaranUltLinea & ", de la empresa " & DBNullToInteger(empresa), "")
                                            Exit Sub 'INC 265 error de configuracion mayor numero de lineas de albaran que numero maximo de lineas
                                        End If

                                        'INICO ESCRITURA XML
                                        dtSolicitudAux = GenerarDTSolicitud(tipoMoneda)
                                        dsMismoAlbaran.Tables.Add(dtSolicitudAux)
                                        dsMismoAlbaran.Tables(0).Rows(0).Item("INSTANCIA") = instancia 'añado la instancia a la factura
                                        dsMismoAlbaran = ComprobarValorLinea(dsMismoAlbaran) 'comprobando valores de linea
                                        dsMismoAlbaran.DataSetName = "NewDataSet" 'por formato para su tratamiento este debe ser el nombre del dataSet
                                        dsMismoAlbaran.WriteXml(ConfigurationManager.AppSettings("rutaXML") & "\" & usuario & "#" & instancia & "#" & idbloque & "#" & "F" & ".xml")
                                        NombresTags = NombreTablas(dsMismoAlbaran)
                                        RemplazarNumerosTags(NombresTags, ConfigurationManager.AppSettings("rutaXML") & "\" & usuario & "#" & instancia & "#" & idbloque & "#" & "F" & ".xml") 'quitando tags
                                        'FIN ESCRITURA XML

                                        'Cambiar el importe y la ret para el siguiente la mando el importe que me queda

                                        dsAuxMaximoLineas.Tables(0).Rows(0).Item("IMPORTE") = albaranTotal_ult
                                        dsAuxMaximoLineas.Tables(0).Rows(0).Item("RET_GARANTIA") = ret_garantia_ult

                                        dsMismoAlbaran.Dispose()
                                        dsMismoAlbaran = New DataSet

                                        'SI TENEMOS IGUAL ALBARAN Y PEDIDO

                                        numlineas = 0

                                        'INICIO  con misma llamada con ds mas pequeño
                                        dsMismoAlbaran.DataSetName = dsTablas.DataSetName
                                        dsMismoAlbaran.Tables.Add(dsAuxMaximoLineas.Tables(0).Copy)
                                        'FIN  con misma llamada con ds mas pequeño

                                        For w As Integer = Pos_LineaPed_LineaRecep To dsTablas.Tables.Count - 1
                                            dsMismoAlbaran.Tables.Add(dsTablas.Tables(w).Copy)
                                            If dsTablas.Tables(w).TableName.StartsWith("LINEA") Then
                                                numlineas = numlineas + 1
                                                dsMismoAlbaran.Tables(dsMismoAlbaran.Tables.Count - 1).Rows(0).Item("NUM") = numlineas ' Tengo que cambiar el numero
                                            End If
                                        Next

                                        If numlineas <= numMax Then GenerarXMLConLineaMaxima(dsMismoAlbaran, numMax)

                                        Exit Sub

                                        'SI TENEMOS IGUAL ALBARAN Y PEDIDO
                                    Else
                                        dsAuxMaximoLineas.DataSetName = dsTablas.DataSetName

                                        dsAuxMaximoLineas.Tables(0).Rows(0).Item("IMPORTE") = importe_noAlbaran + AlbImporte
                                        dsAuxMaximoLineas.Tables(0).Rows(0).Item("RET_GARANTIA") = ret_garantia_noalbaran + AlbRetGarantia

                                        'INICO ESCRITURA XML
                                        dtSolicitudAux = GenerarDTSolicitud(tipoMoneda)
                                        dsAuxMaximoLineas.Tables.Add(dtSolicitudAux)
                                        dsAuxMaximoLineas.Tables(0).Rows(0).Item("INSTANCIA") = instancia 'añado la instancia a la factura
                                        dsAuxMaximoLineas = ComprobarValorLinea(dsAuxMaximoLineas) 'comprobar valores de linea
                                        dsAuxMaximoLineas.DataSetName = "NewDataSet" 'por formato para su tratamiento este debe ser el nombre del dataSet
                                        dsAuxMaximoLineas.WriteXml(ConfigurationManager.AppSettings("rutaXML") & "\" & usuario & "#" & instancia & "#" & idbloque & "#" & "F" & ".xml")
                                        NombresTags = NombreTablas(dsAuxMaximoLineas)
                                        RemplazarNumerosTags(NombresTags, ConfigurationManager.AppSettings("rutaXML") & "\" & usuario & "#" & instancia & "#" & idbloque & "#" & "F" & ".xml") 'quitando tags
                                        'FIN ESCRITURA XML

                                        codigoFacturas = codigoFacturas + 1
                                        dsAuxMaximoLineas.Clear()
                                        dsAuxMaximoLineas.Dispose()
                                        dsAuxMaximoLineas = New DataSet 'Si Solo elimino me dice que ya existe
                                        dtlinea = dsTablas.Tables(0).Copy 'tengo que meter la factura
                                        dsAuxMaximoLineas.Tables.Add(dtlinea)

                                        numlineas = 0
                                        importe_noAlbaran = 0 'inicalizo a 0 el importe y la retencion de garantia por cada factura
                                        ret_garantia_noalbaran = 0
                                        AlbImporte = 0 'comprobacion de importes
                                        AlbRetGarantia = 0

                                        Exit While
                                    End If

                                Else
                                    dtlinea = dsTablas.Tables(contador).Copy
                                    dsAuxMaximoLineas.Tables.Add(dtlinea) ' aqui tengo que añadir importes y retenciones

                                    If dtlinea.TableName.StartsWith("LINEA") Then
                                        importe_noAlbaran = importe_noAlbaran + dtlinea.Rows(0).Item("IMPORTE")
                                        ret_garantia_noalbaran = ret_garantia_noalbaran + dtlinea.Rows(0).Item("RET_GARANTIA")
                                    ElseIf dtlinea.TableName.StartsWith("COSTE") Or dtlinea.TableName.StartsWith("IMPUESTOS") Then
                                        importe_noAlbaran = importe_noAlbaran + dtlinea.Rows(0).Item("IMPORTE")
                                    ElseIf dtlinea.TableName.StartsWith("DESCUENTO") Then
                                        importe_noAlbaran = importe_noAlbaran - dtlinea.Rows(0).Item("IMPORTE")
                                    End If

                                    'añado los importes para que en cada grupo de facturas de el importe y ret segun sus lineas-costes-descuentos e impuestos

                                    contador = contador + 1
                                End If
                            Else
                                Exit While
                            End If
                        End While
                    Else
                        contador = contador + 1
                    End If
                Else
                    contador = contador + 1
                End If
            End While

            'para que me saque el ultimo 

            If dsAuxMaximoLineas IsNot Nothing Then

                dsAuxMaximoLineas.Tables(0).Rows(0).Item("IMPORTE") = importe_noAlbaran + AlbImporte
                dsAuxMaximoLineas.Tables(0).Rows(0).Item("RET_GARANTIA") = ret_garantia_noalbaran + AlbRetGarantia

                'INICO ESCRITURA XML
                dtSolicitudAux = GenerarDTSolicitud(tipoMoneda)
                dsAuxMaximoLineas.Tables.Add(dtSolicitudAux)
                dsAuxMaximoLineas.Tables(0).Rows(0).Item("INSTANCIA") = instancia 'añado la instancia a la factura
                dsAuxMaximoLineas = ComprobarValorLinea(dsAuxMaximoLineas) 'caso que me cambien el valor de linea
                dsAuxMaximoLineas.DataSetName = "NewDataSet" 'por formato para su tratamiento este debe ser el nombre del dataSet
                dsAuxMaximoLineas.WriteXml(ConfigurationManager.AppSettings("rutaXML") & "\" & usuario & "#" & instancia & "#" & idbloque & "#" & "F" & ".xml")
                NombresTags = NombreTablas(dsAuxMaximoLineas)
                RemplazarNumerosTags(NombresTags, ConfigurationManager.AppSettings("rutaXML") & "\" & usuario & "#" & instancia & "#" & idbloque & "#" & "F" & ".xml") 'quitando tags
                'FIN ESCRITURA XML
            End If

        Catch ex As Exception
            GrabarError("Generar XML con línea máxima.", ex.Message)
        End Try

        'FIN RECORRER LIMITE XXX LINEAS 
    End Sub


    '''<summary>
    ''' agrupa todos los datos segun Proveedor ,Moneda y Abono en caso de que sea sin centro de coste
    ''' </summary>  
    '''  <param name="empresa">int que contiene el id de la empresa</param>
    ''' <remarks>Llamada desde:GeneracionFacturas.vb main; Tiempo máximo: segundos</remarks>
    Private Sub GenerarDSAutoFactSinCentroCoste(ByVal empresa As Integer)

        Dim dsCargarDatos As DataSet '' obtener todos los datos

        dsCargarDatos = oFacturas.Facturas_CargarDatos(empresa, dFechaUltimaRecepcion)

        'PARA GENERAR EL XML ME HACE FALTA PONER MONEDA Y PROVEDOR

        'INICIO LOG CON TODOS LOS DATOS QUE VOY A FACTURAR

        Dim Fichero As System.IO.StreamWriter
        Try
            Fichero = New System.IO.StreamWriter(ConfigurationManager.AppSettings("rutaLogError") & empresa & "-" & dia & "-" & mes & "-" & anyo & ".log")
            Fichero.WriteLine(empresa)
            'AÑADO LAS LINEAS DE FACTURACION LIBRE O SIN RECEPCIONES
            If dsCargarDatos.Tables("LINEAS") IsNot Nothing Then
                If dsCargarDatos.Tables(0).Rows.Count >= 1 Then
                    Fichero.WriteLine("Pedidos con líneas de facturación libre o sin recepciones")
                    Fichero.WriteLine("---------------------------------------------------------")
                    For w As Integer = 0 To dsCargarDatos.Tables("LINEAS").Rows.Count - 1
                        Fichero.WriteLine(dsCargarDatos.Tables("LINEAS").Rows(w).Item("ID") & "        " & dsCargarDatos.Tables("LINEAS").Rows(w).Item("ID1"))
                    Next
                End If
            End If
            'AÑADO LAS LINEAS CON FACTURACION TRAS RECEPCIONES
            If dsCargarDatos.Tables("LINEAS_CON_RECEPCIONES") IsNot Nothing Then
                If dsCargarDatos.Tables(1).Rows.Count >= 1 Then
                    Fichero.WriteLine("Pedidos con líneas de facturación tras recepciones")
                    Fichero.WriteLine("--------------------------------------------------")
                    For w As Integer = 0 To dsCargarDatos.Tables("LINEAS_CON_RECEPCIONES").Rows.Count - 1
                        Fichero.WriteLine(dsCargarDatos.Tables("LINEAS_CON_RECEPCIONES").Rows(w).Item("PEDIDO") & "        " & dsCargarDatos.Tables("LINEAS_CON_RECEPCIONES").Rows(w).Item("ID1") & "        " & dsCargarDatos.Tables("LINEAS_CON_RECEPCIONES").Rows(w).Item("ALBARAN"))
                    Next
                End If
            End If

            Fichero.Close()

            'FIN LOG CON TODOS LOS DATOS QUE VOY A FACTURAR
        Catch ex As Exception
            GrabarError("No se ha podido generar Log de la empresa " & DBNullToInteger(empresa) & ".", ex.Message)
        End Try

        ' agrupar impuestos
        GenerarXMLPorGastos_Inversiones(dsCargarDatos, empresa, 0) 'Gasto 0
        GenerarXMLPorGastos_Inversiones(dsCargarDatos, empresa, 1) 'Inversion 1

    End Sub


    Private Sub GrabarError(ByVal sTextoFuncion As String, ByVal sTextoDescripcion As String)
        If FicheroError Is Nothing Then
            FicheroError = New System.IO.StreamWriter(ConfigurationManager.AppSettings("rutaLogError") & "Error-" & DBNullToInteger(empresa) & "-" & dia & "-" & mes & "-" & anyo & ".log")
        End If
        FicheroError.WriteLine("Hora del error: " & Date.Now)
        FicheroError.WriteLine("Error: " & sTextoFuncion)
        FicheroError.WriteLine("Descripción: " & sTextoDescripcion)
    End Sub

    ''' <summary>
    ''' Funcion que nos devuelve el datatable de solicitud 
    ''' </summary>
    ''' <param name="tipoMoneda">el tipo de moneda (string)</param>
    ''' <returns>Nos devuelve el datatable despues de crear la instancia y obtener el id_bloque</returns>
    ''' <remarks></remarks>
    Private Function GenerarDTSolicitud(ByVal tipoMoneda As String) As DataTable
        Dim dtbloque As DataTable
        Dim idaccion As Integer

        'INICO ESCRITURA XML
        Try
            instancia = oFacturas.CrearInstancia(gSolicitud, gFormulario, gWorkflow, tipoMoneda, usuario)
        Catch ex As Exception
            GrabarError("Al crear crear la instancia.", ex.Message)
        End Try

        Try
            dtbloque = oFacturas.ObtenerBloque_Accion_Auto(gSolicitud)
            If dtbloque IsNot Nothing Then
                If dtbloque.Rows.Count >= 1 Then
                    idbloque = DBNullToStr(dtbloque.Rows(0).Item("ID"))
                    idaccion = DBNullToInteger(dtbloque.Rows(0).Item("IDACCION"))
                End If
            End If
        Catch ex As Exception
            GrabarError("Al obtener el bloque y la acción.", ex.Message)
        End Try

        Dim lIDTiempoProc As Long
        oFacturas.ActualizarTiempoProcesamiento(lIDTiempoProc, instancia, sCodUsuario, 0, 0, False, Nothing, Nothing, Nothing)

        'AÑADIR TABLA SOLICITUD A DATASET
        Dim dtSolicitud As DataTable
        Dim dtSolicitudAux As DataTable
        Dim dnewSolicitud As DataRow

        dtSolicitud = New DataTable
        With dtSolicitud.Columns
            .Add("TIPO_PROCESAMIENTO_XML")
            .Add("TIPO_DE_SOLICITUD")
            .Add("COMPLETO")
            .Add("CODIGOUSUARIO")
            .Add("INSTANCIA")
            .Add("SOLICITUD")
            .Add("USUARIO")
            .Add("USUARIO_EMAIL")
            .Add("USUARIO_IDIOMA")
            .Add("PEDIDO_DIRECTO")
            .Add("FORMULARIO")
            .Add("WORKFLOW")
            .Add("IMPORTE", System.Type.GetType("System.Double"))
            .Add("COMENTALTANOCONF")
            .Add("ACCION")
            .Add("IDACCION")
            .Add("IDACCIONFORM")
            .Add("GUARDAR")
            .Add("NOTIFICAR")
            .Add("TRASLADO_USUARIO")
            .Add("TRASLADO_PROVEEDOR")
            .Add("TRASLADO_FECHA")
            .Add("TRASLADO_COMENTARIO")
            .Add("TRASLADO_PROVEEDOR_CONTACTO")
            .Add("DEVOLUCION_COMENTARIO")
            .Add("COMENTARIO")
            .Add("BLOQUE_ORIGEN")
            .Add("NUEVO_ID_INSTANCIA")
            .Add("BLOQUES_DESTINO")
            .Add("ROL_ACTUAL")
            .Add("NOCONFORMIDAD")       '27
            .Add("CERTIFICADO")         '28
            .Add("CONTRATO")            '29
            .Add("FACTURA")             '30
            .Add("PEDIDO")          '31
            .Add("IDTIEMPOPROC")
        End With

        Try
            dtSolicitudAux = dtSolicitud.Clone

            dnewSolicitud = dtSolicitudAux.NewRow
            With dnewSolicitud
                .Item("TIPO_PROCESAMIENTO_XML") = cnTipopProcesamientoXMLFSNWeb
                .Item("TIPO_DE_SOLICITUD") = cnTipoSolicitudFactura
                .Item("COMPLETO") = 1
                .Item("CODIGOUSUARIO") = sCodUsuario
                .Item("INSTANCIA") = instancia
                .Item("SOLICITUD") = gSolicitud
                .Item("USUARIO") = usuario
                .Item("USUARIO_EMAIL") = ""
                .Item("USUARIO_IDIOMA") = ""
                .Item("PEDIDO_DIRECTO") = ""
                .Item("FORMULARIO") = ""
                .Item("WORKFLOW") = workflow
                .Item("IMPORTE") = 0
                .Item("COMENTALTANOCONF") = ""
                .Item("ACCION") = "guardarsolicitud"
                .Item("IDACCION") = idaccion
                .Item("IDACCIONFORM") = idaccion
                .Item("GUARDAR") = 1
                .Item("NOTIFICAR") = 1
                .Item("TRASLADO_USUARIO") = ""
                .Item("TRASLADO_PROVEEDOR") = ""
                .Item("TRASLADO_FECHA") = ""
                .Item("TRASLADO_COMENTARIO") = ""
                .Item("TRASLADO_PROVEEDOR_CONTACTO") = ""
                .Item("DEVOLUCION_COMENTARIO") = ""
                .Item("COMENTARIO") = ""
                .Item("BLOQUE_ORIGEN") = idbloque
                .Item("NUEVO_ID_INSTANCIA") = ""
                .Item("BLOQUES_DESTINO") = ""
                .Item("ROL_ACTUAL") = idrol 'id pm_rol
                .Item("NOCONFORMIDAD") = 0
                .Item("CERTIFICADO") = 0
                .Item("CONTRATO") = 0
                .Item("FACTURA") = 1
                .Item("IDTIEMPOPROC") = lIDTiempoProc
            End With

            dtSolicitudAux.Rows.Add(dnewSolicitud)
            dtSolicitudAux.TableName = "SOLICITUD"
        Catch ex As Exception
            GrabarError("Al cargar la tabla solicitud.", ex.Message)
        End Try

        Return dtSolicitudAux
    End Function

    Public Function DBNullToStr(Optional ByVal value As Object = Nothing) As String
        If IsDBNull(value) Then
            Return ""
        Else
            Return value
        End If
    End Function

    Public Function NothingToDBNull(Optional ByVal value As Object = Nothing) As Object
        If IsNothing(value) Then
            Return System.DBNull.Value
        Else
            Return value
        End If
    End Function
    ''' <summary>
    ''' Tratamos de convertir el objeto recibido a Integer
    ''' </summary>
    ''' <param name="value">Valor a convertir a Integer</param>
    ''' <returns>Valor convertido a Integer. Si no es posible o el value es null o nothing, devuelve 0</returns>
    ''' <remarks>Llamado desde toda la aplicación, donde se requiera su uso. Tiempo máx inferior a 0,1 seg</remarks>
    Public Function DBNullToInteger(Optional ByVal value As Object = Nothing) As Integer
        If IsDBNull(value) Then
            Return 0
        ElseIf value Is Nothing Then
            Return 0
        Else
            Return CInt(value)
        End If
    End Function
    ''' <summary>
    ''' Tratamos de convertir el objeto recibido a Double
    ''' </summary>
    ''' <param name="value">Valor a convertir a Double</param>
    ''' <returns>Valor convertido a double. Si no es posible o el value es null o nothing, devuelve 0</returns>
    ''' <remarks>Llamado desde toda la aplicación, donde se requiera su uso. Tiempo máx inferior a 0,1 seg</remarks>
    Public Function DBNullToDbl(Optional ByVal value As Object = Nothing) As Double
        If IsDBNull(value) Then
            Return 0
        ElseIf value Is Nothing Then
            Return 0
        Else
            Try
                Dim valueDbl As Double = CDbl(value)
                Return valueDbl
            Catch ex As Exception
                Return 0
            End Try
        End If
    End Function

    ''' <summary>
    ''' Compruebo que el valor de linea sea el mismo en costes descuentos e impuesto. 
    ''' Si no lo es me pone el mismo valor linea en el campo linea de Costes,Descuentos e impuestos
    ''' </summary>
    ''' <param name="ds">dataset a escribir</param>
    ''' <returns>Dataset con los mismos valores en linea para costes, descuentos e impuestos</returns>
    ''' <remarks>Llamado desde toda la aplicación, donde se requiera su uso. Tiempo máx inferior a 0,1 seg</remarks>
    Public Function ComprobarValorLinea(ByVal ds As DataSet) As DataSet
        Dim nombreTabla As String
        Dim numlinea As Integer

        Dim PosLinea As Integer
        PosLinea = 0
        Dim totCostes As Double
        Dim totDescuentos As Double
        Dim totImpuestos As Double

        Try
            For i As Integer = 1 To ds.Tables.Count - 1
                nombreTabla = ds.Tables(i).TableName
                If nombreTabla.StartsWith("LINEA") Then
                    numlinea = DBNullToInteger(ds.Tables(i).Rows(0).Item("LINEA"))
                    If PosLinea = 0 Then
                        PosLinea = i
                    Else
                        ds.Tables(PosLinea).Rows(0).Item("TOT_COSTES") = totCostes
                        ds.Tables(PosLinea).Rows(0).Item("TOT_DESCUENTOS") = totDescuentos
                        ds.Tables(PosLinea).Rows(0).Item("IMPUESTOS") = totImpuestos
                        ds.Tables(PosLinea).Rows(0).Item("IMP_BRUTO") = ds.Tables(PosLinea).Rows(0).Item("IMPORTE") 'en las lineas al importe bruto le llamo importe 
                        ds.Tables(PosLinea).Rows(0).Item("IMPORTE") = totCostes + totImpuestos - totDescuentos + ds.Tables(PosLinea).Rows(0).Item("IMP_BRUTO")
                        PosLinea = i
                        totCostes = 0
                        totDescuentos = 0
                        totImpuestos = 0
                    End If
                ElseIf nombreTabla.StartsWith("COSTE") Then
                    If numlinea <> DBNullToInteger(ds.Tables(i).Rows(0).Item("LINEA")) Then
                        ds.Tables(i).Rows(0).Item("LINEA") = numlinea 'cambio el numero de linea
                        totCostes = totCostes + ds.Tables(i).Rows(0).Item("IMPORTE")
                    End If
                ElseIf nombreTabla.StartsWith("DESCUENTO") Then
                    If numlinea <> DBNullToInteger(ds.Tables(i).Rows(0).Item("LINEA")) Then
                        ds.Tables(i).Rows(0).Item("LINEA") = numlinea 'cambio el numero de linea
                        totDescuentos = totDescuentos + ds.Tables(i).Rows(0).Item("IMPORTE")
                    End If
                ElseIf nombreTabla.StartsWith("IMPUESTO") Then
                    If numlinea <> DBNullToInteger(ds.Tables(i).Rows(0).Item("LINEA")) Then
                        ds.Tables(i).Rows(0).Item("LINEA") = numlinea 'cambio el numero de linea
                        totImpuestos = totImpuestos + ds.Tables(i).Rows(0).Item("IMPORTE")
                    End If
                End If

            Next
            'sumar el ultimo Valido para que sea una linea 
            nombreTabla = ds.Tables(PosLinea).TableName
            If nombreTabla.StartsWith("LINEA") Then

                ds.Tables(PosLinea).Rows(0).Item("TOT_COSTES") = totCostes
                ds.Tables(PosLinea).Rows(0).Item("TOT_DESCUENTOS") = totDescuentos
                ds.Tables(PosLinea).Rows(0).Item("IMPUESTOS") = totImpuestos
                ds.Tables(PosLinea).Rows(0).Item("IMP_BRUTO") = ds.Tables(PosLinea).Rows(0).Item("IMPORTE") 'en las lineas al importe bruto le llamo importe 
                ds.Tables(PosLinea).Rows(0).Item("IMPORTE") = totCostes + totImpuestos - totDescuentos + ds.Tables(PosLinea).Rows(0).Item("IMP_BRUTO")

            End If

        Catch ex As Exception
            GrabarError("Al tratar de cambiar el valor de línea de costes, descuentos e impuestos", ex.Message)
        End Try

        Return ds

    End Function
    ''' <summary>
    ''' Nos devuelve un datatable con todos los nombres de las tablas del dataset  
    ''' esto nos hace falta para poder solventar le problema del tag numerico del xml
    ''' </summary>
    ''' <param name="ds">Dataset que se ha de escribir</param>
    ''' <returns>devulve una tabla con todos los nombres de las tablas</returns>
    ''' <remarks>Llamado desde todos los lugares donde se utiliza writexml. Tiempo máx inferior a </remarks>
    Public Function NombreTablas(ByVal ds As DataSet) As DataTable
        Dim nombreTabla As String
        Dim dt As DataTable
        dt = New DataTable

        dt.Columns.Add("NOMBRE", System.Type.GetType("System.String"))

        Try
            For i As Integer = 1 To ds.Tables.Count - 1
                nombreTabla = ds.Tables(i).TableName
                dt.Rows.Add(nombreTabla)
            Next
        Catch ex As Exception
            GrabarError("Al cargar la tabla con el nombre de las tablas.", ex.Message)
        End Try

        Return dt

    End Function
    ''' <summary>
    ''' Remplaza los tags numericos de de los nombres de las tablas por el tag sin el numero
    ''' Si no lo es me pone el mismo valor linea en el campo linea de Costes,Descuentos e impuestos
    ''' </summary>
    ''' <param name="dt">datatable con los nombres de las tablas</param>
    ''' <param name="ruta">string con la ruta y el numbre del xml a modificar</param>
    ''' <remarks>Llamado desde toda la aplicación, donde se requiera su uso. Tiempo máx inferior a 0,1 seg</remarks>
    Public Sub RemplazarNumerosTags(ByVal dt As DataTable, ByVal ruta As String)

        Dim objStreamReader As StreamReader
        Dim strTotal As String

        objStreamReader = New StreamReader(ruta)

        strTotal = objStreamReader.ReadToEnd

        For x As Integer = 0 To dt.Rows.Count - 1

            If dt.Rows(x).Item(0).StartsWith("COSTE") Then
                strTotal = Replace(strTotal, dt.Rows(x).Item(0), "COSTE", 1, 2) 'remplazo desde la primera posicion y solo las 2 primeras veces que aparece para evitar problema coste3 y coste33 
            End If
            If dt.Rows(x).Item(0).StartsWith("DESCUENTO") Then
                strTotal = Replace(strTotal, dt.Rows(x).Item(0), "DESCUENTO", 1, 2)
            End If
            If dt.Rows(x).Item(0).StartsWith("IMPUESTO") Then
                strTotal = Replace(strTotal, dt.Rows(x).Item(0), "IMPUESTO", 1, 2)
            End If
            If dt.Rows(x).Item(0).StartsWith("LINEAS") Then
                strTotal = Replace(strTotal, dt.Rows(x).Item(0), "LINEAS", 1, 2) 'tratamiento Linea por lineas
            End If
        Next
        Try

            'Inicio Escribir Schema 
            Dim DsEscribir As DataSet
            DsEscribir = New DataSet

            If Not System.IO.File.Exists("cabeceraSchema" & ".xml") Then
                DsEscribir = GeneracionFacturas.CargarEstructuraDataSetParaSchema() 'obtengo el schema que debe llevar el xml
                DsEscribir.WriteXml("cabeceraSchema" & ".xml", XmlWriteMode.WriteSchema)
            End If

            Dim objStreamReaderSchema As StreamReader
            Dim strSchema As String

            objStreamReaderSchema = New StreamReader("cabeceraSchema" & ".xml")

            strSchema = objStreamReaderSchema.ReadToEnd
            strSchema = Replace(strSchema, "<NewDataSet>", "<NewDataSet xmlns="""">", 1, 1)
            strSchema = Replace(strSchema, "</NewDataSet>", "", 1, 1)

            Replace(strTotal, "<?xml version=""1.0"" standalone=""yes""?>", "")

            strTotal = Replace(strTotal, "<NewDataSet>", strSchema)
            strTotal = Replace(strTotal, "<?xml version=""1.0"" standalone=""yes""?>", "")
            strTotal = Replace(strTotal, "<?xml version=""1.0"" standalone=""yes""?>", strSchema)

            'Fin Escribir Schema

            objStreamReader.Close() 'tengo que cerrar el fichero para poderlo escribir despues
            Dim Fichero As System.IO.StreamWriter

            Fichero = New System.IO.StreamWriter(ruta)

            Fichero.Write(strTotal)
            Fichero.Close()
            dt.Dispose()

            Dim oXMLDoc As New System.Xml.XmlDocument

            Dim sUsername As String
            Dim sPassword As String

            sUsername = sCodUsuario
            sPassword = Encrypter.Encrypt(sPwd, sCodUsuario, False, Encrypter.TipoDeUsuario.Persona, dateFecPwd)

            Try
                oXMLDoc.Load(ruta)
            Catch ex As Exception
                GrabarError("Al cargar el schema en el documento.", ex.Message)
            End Try

            Dim sXMLName As String
            Dim sFile As String = Path.GetFileName(ruta) 'obtengo el nombre del fichero
            sXMLName = Replace(sFile, ".xml", "", 1, 1) ' quito la extension al nombre del fichero

            oFacturas.Actualizar_En_Proceso(instancia, 1) 'Se queda en proceso si falla            

            Try
                Dim xmlName As String = sCodUsuario & "#" & instancia & "#0"
                Dim lIDTiempoProc As Long
                oFacturas.ActualizarTiempoProcesamiento(lIDTiempoProc, instancia, Nothing, 0, TiempoProcesamiento.InicioDisco, False, Nothing, Nothing, Nothing)

                oXMLDoc.Save(ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & xmlName & "#FSNWEB.xml")
                If File.Exists(ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & xmlName & ".xml") Then _
                    File.Delete(ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & xmlName & ".xml")
                FileSystem.Rename(ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & xmlName & "#FSNWEB.xml",
                                  ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & xmlName & ".xml")
            Catch ex As Exception
                GrabarError("Al llamar al webService.", ex.Message)
            End Try

        Catch ex As Exception
            GrabarError("Al tratar se sustituir los tags numéricos en el xml.", ex.Message)
        End Try
    End Sub

    '''<summary>
    ''' Crear un dataset que contenga la  estructura y los datos que debe llevar el xml
    ''' </summary>
    ''' <remarks>Llamada desde:GeneracionFacturas.vb GenerarDSAutoFactSinCentroCoste; Tiempo máximo: segundos</remarks>
    Private Sub GenerarXMLPorGastos_Inversiones(ByVal DsTrasCarga As DataSet, ByVal empresa As Integer, ByVal concepto As Integer)

        Dim ds As DataSet
        Dim dNewLineaRow As DataRow
        Dim numeroLinea As Integer = 1
        Dim dsEscribir As DataSet
        Dim dtLineas As DataTable
        Dim dtCostes As DataTable
        Dim dtDescuentos As DataTable
        Dim dtImpuestos As DataTable
        Dim dNewCosteRow As DataRow
        Dim sArt_Init As String ' para el articulo de cada linea
        Dim dNewDescuentoRow As DataRow
        Dim numDataset As Integer = 1
        Dim numPosLinea As Integer = 0 'Guardar la poscion de inicio de la linea
        Dim numLinea_Ped_Rec As Integer = 0 'Guarda el numero de la linea como metodo de validacion del dataset para saber que la linea de la posicion del dataset debe ser esta
        Dim dnewPosTipoImpuesto As DataRow
        Dim dtPosTipoImpuesto As DataTable
        Dim dsImpuestos As DataSet
        dsImpuestos = New DataSet

        ds = CargarEstructuraDataSetSimple(empresa)
        Dim dsAux(2) As DataSet
        Dim dsPosTipoImpuesto As DataSet
        dsPosTipoImpuesto = New DataSet

        Dim dsImpuesto As DataSet 'para guarda el tipo de impuestos que tengo
        dsImpuesto = New DataSet

        Dim tipoMoneda As String
        Dim dNewFacturaRow As DataRow
        Dim dFacturasRow As DataRow
        Dim dNewImpuestoRow As DataRow

        Dim dtSolicitudAux As DataTable

        'Inicio TRATO EL IMPUESTO CONCEPTO ESTO ES PORQUE EN EL CONCEPTO EN EL IMPUESTO PUEDE LLEVAR 0 GASTO/1 iNVERSION / 2 AMBOS (EN ESTE CASO LO AÑADO A GASTO) 
        Dim iConceptoImpuesto As Integer
        If concepto <> 1 Then
            iConceptoImpuesto = 2
        Else
            iConceptoImpuesto = 1
        End If
        'FIN TRATO EL IMPUESTO CONCEPTO

        'TENGO QUE SACAR TODOS LOS PROVEEDORES CON SU CORRESPONDIENTE MONEDA Y SU ABONO 0 /1
        Dim dtProveMon As DataTable = Nothing
        Dim existe As Integer = 0
        Dim dnewProveMon As DataRow

        For z As Integer = 0 To DsTrasCarga.Tables("LINEAS").Rows.Count - 1

            If dtProveMon IsNot Nothing Then
                For w As Integer = 0 To dtProveMon.Rows.Count - 1
                    If DBNullToStr(DsTrasCarga.Tables("LINEAS").Rows(z).Item("PROVE")).Equals(DBNullToStr(dtProveMon.Rows(w).Item("PROVE"))) AndAlso DBNullToStr(DsTrasCarga.Tables("LINEAS").Rows(z).Item("MON")).Equals(DBNullToStr(dtProveMon.Rows(w).Item("MON"))) AndAlso DBNullToInteger(DsTrasCarga.Tables("LINEAS").Rows(z).Item("ABONO")) = (DBNullToInteger(dtProveMon.Rows(w).Item("ABONO"))) AndAlso (DBNullToInteger(DsTrasCarga.Tables("LINEAS").Rows(z).Item("CONCEPTO")) = concepto) Then
                        existe = 1
                        Exit For
                    End If
                Next
            End If

            If existe = 0 Then

                If dtProveMon Is Nothing Then
                    Try
                        dtProveMon = DsTrasCarga.Tables("LINEAS").Clone
                    Catch ex As Exception
                        GrabarError("Error al clonar la tabla líneas " & DBNullToInteger(empresa), ex.Message)
                    End Try
                End If

                If DBNullToInteger(DsTrasCarga.Tables("LINEAS").Rows(z).Item("CONCEPTO")) = concepto Then

                    dnewProveMon = dtProveMon.NewRow
                    dnewProveMon.Item("ID") = DsTrasCarga.Tables("LINEAS").Rows(z).Item("ID")
                    dnewProveMon.Item("PROVE") = DsTrasCarga.Tables("LINEAS").Rows(z).Item("PROVE")
                    dnewProveMon.Item("EMPRESA") = DsTrasCarga.Tables("LINEAS").Rows(z).Item("EMPRESA")
                    dnewProveMon.Item("PAG") = DsTrasCarga.Tables("LINEAS").Rows(z).Item("PAG")
                    dnewProveMon.Item("VIA_PAG") = DsTrasCarga.Tables("LINEAS").Rows(z).Item("VIA_PAG")
                    dnewProveMon.Item("OBS") = DsTrasCarga.Tables("LINEAS").Rows(z).Item("OBS")
                    dnewProveMon.Item("ABONO") = DsTrasCarga.Tables("LINEAS").Rows(z).Item("ABONO")
                    dnewProveMon.Item("ABONO_DESDE") = DsTrasCarga.Tables("LINEAS").Rows(z).Item("ABONO_DESDE")
                    dnewProveMon.Item("ABONO_HASTA") = DsTrasCarga.Tables("LINEAS").Rows(z).Item("ABONO_HASTA")
                    dnewProveMon.Item("MON") = DsTrasCarga.Tables("LINEAS").Rows(z).Item("MON")
                    dnewProveMon.Item("FACTURA") = DsTrasCarga.Tables("LINEAS").Rows(z).Item("FACTURA")
                    dnewProveMon.Item("ID1") = DsTrasCarga.Tables("LINEAS").Rows(z).Item("ID1")
                    dnewProveMon.Item("IM_FACTRASRECEP") = DsTrasCarga.Tables("LINEAS").Rows(z).Item("IM_FACTRASRECEP")
                    dnewProveMon.Item("PREC_UP") = DsTrasCarga.Tables("LINEAS").Rows(z).Item("PREC_UP")
                    dnewProveMon.Item("CANT_PED") = DsTrasCarga.Tables("LINEAS").Rows(z).Item("CANT_PED")
                    dnewProveMon.Item("IM_RETGARANTIA") = DsTrasCarga.Tables("LINEAS").Rows(z).Item("IM_RETGARANTIA")
                    dnewProveMon.Item("ART_INT") = DsTrasCarga.Tables("LINEAS").Rows(z).Item("ART_INT")

                    dtProveMon.Rows.Add(dnewProveMon)
                End If
            Else
                existe = 0
            End If

        Next

        'INICIO LINEAS CON RECEPCIONES
        existe = 0

        For z As Integer = 0 To DsTrasCarga.Tables("LINEAS_CON_RECEPCIONES").Rows.Count - 1

            If dtProveMon IsNot Nothing Then
                For w As Integer = 0 To dtProveMon.Rows.Count - 1
                    If DBNullToStr(DsTrasCarga.Tables("LINEAS_CON_RECEPCIONES").Rows(z).Item("PROVE")).Equals(DBNullToStr(dtProveMon.Rows(w).Item("PROVE"))) AndAlso DBNullToStr(DsTrasCarga.Tables("LINEAS_CON_RECEPCIONES").Rows(z).Item("MON")).Equals(DBNullToStr(dtProveMon.Rows(w).Item("MON"))) AndAlso DBNullToInteger(DsTrasCarga.Tables("LINEAS_CON_RECEPCIONES").Rows(z).Item("ABONO")) = (DBNullToInteger(dtProveMon.Rows(w).Item("ABONO"))) AndAlso (DBNullToInteger(DsTrasCarga.Tables("LINEAS_CON_RECEPCIONES").Rows(z).Item("CONCEPTO")) = concepto) Then
                        existe = 1
                        Exit For
                    End If
                Next
            End If

            If existe = 0 Then
                If dtProveMon Is Nothing Then
                    Try
                        dtProveMon = DsTrasCarga.Tables("LINEAS_CON_RECEPCIONES").Clone
                    Catch ex As Exception
                        GrabarError("Error al clonar la tabla recepciones " & DBNullToInteger(empresa), ex.Message)
                    End Try
                End If

                If DBNullToInteger(DsTrasCarga.Tables("LINEAS_CON_RECEPCIONES").Rows(z).Item("CONCEPTO")) = concepto Then
                    dnewProveMon = dtProveMon.NewRow
                    dnewProveMon.Item("ID") = DsTrasCarga.Tables("LINEAS_CON_RECEPCIONES").Rows(z).Item("ID")
                    dnewProveMon.Item("PROVE") = DsTrasCarga.Tables("LINEAS_CON_RECEPCIONES").Rows(z).Item("PROVE")
                    dnewProveMon.Item("EMPRESA") = DsTrasCarga.Tables("LINEAS_CON_RECEPCIONES").Rows(z).Item("EMPRESA")
                    dnewProveMon.Item("PAG") = DsTrasCarga.Tables("LINEAS_CON_RECEPCIONES").Rows(z).Item("PAG")
                    dnewProveMon.Item("VIA_PAG") = DsTrasCarga.Tables("LINEAS_CON_RECEPCIONES").Rows(z).Item("VIA_PAG")
                    dnewProveMon.Item("OBS") = DsTrasCarga.Tables("LINEAS_CON_RECEPCIONES").Rows(z).Item("OBS")
                    dnewProveMon.Item("ABONO") = DsTrasCarga.Tables("LINEAS_CON_RECEPCIONES").Rows(z).Item("ABONO")
                    dnewProveMon.Item("ABONO_DESDE") = DsTrasCarga.Tables("LINEAS_CON_RECEPCIONES").Rows(z).Item("ABONO_DESDE")
                    dnewProveMon.Item("ABONO_HASTA") = DsTrasCarga.Tables("LINEAS_CON_RECEPCIONES").Rows(z).Item("ABONO_HASTA")
                    dnewProveMon.Item("MON") = DsTrasCarga.Tables("LINEAS_CON_RECEPCIONES").Rows(z).Item("MON")
                    dnewProveMon.Item("FACTURA") = DsTrasCarga.Tables("LINEAS_CON_RECEPCIONES").Rows(z).Item("FACTURA")
                    dnewProveMon.Item("ID1") = DsTrasCarga.Tables("LINEAS_CON_RECEPCIONES").Rows(z).Item("ID1")
                    dnewProveMon.Item("IM_FACTRASRECEP") = DsTrasCarga.Tables("LINEAS_CON_RECEPCIONES").Rows(z).Item("IM_FACTRASRECEP")
                    dnewProveMon.Item("PREC_UP") = DsTrasCarga.Tables("LINEAS_CON_RECEPCIONES").Rows(z).Item("PREC_UP")
                    dnewProveMon.Item("CANT_PED") = DsTrasCarga.Tables("LINEAS_CON_RECEPCIONES").Rows(z).Item("CANT_PED")
                    dnewProveMon.Item("IM_RETGARANTIA") = DsTrasCarga.Tables("LINEAS_CON_RECEPCIONES").Rows(z).Item("IM_RETGARANTIA")
                    dnewProveMon.Item("ART_INT") = DsTrasCarga.Tables("LINEAS_CON_RECEPCIONES").Rows(z).Item("ART_INT")

                    dtProveMon.Rows.Add(dnewProveMon)
                End If

            Else
                existe = 0
            End If

        Next
        'FIN LINEAS CON RECEPCIONES

        'FIN CARGA PROVEMON ABONO 0/1

        'AHORA SOLO SACO LA PRIMER FACTURA

        'CARGO LA PRIMERA FACTURA
        If dtProveMon IsNot Nothing Then

            For z As Integer = 0 To dtProveMon.Rows.Count - 1

                numeroLinea = 1 'que siempre empiece por 1 el numero de linea
                dsEscribir = New DataSet
                dFacturasRow = dtProveMon.Rows(z)

                dNewFacturaRow = ds.Tables("FACTURA").NewRow
                dNewFacturaRow.Item("ID") = vbEmpty
                dNewFacturaRow.Item("NUM") = vbEmpty
                dNewFacturaRow.Item("PROVE") = dFacturasRow.Item("PROVE")
                dNewFacturaRow.Item("ESTADO") = vbEmpty   'LA FACTURA GENERADA TENDRA ESTADO= 0
                dNewFacturaRow.Item("FECHA") = Date.Now
                dNewFacturaRow.Item("IMPORTE") = vbEmpty
                dNewFacturaRow.Item("FEC_CONT") = DBNull.Value
                dNewFacturaRow.Item("EMPRESA") = dFacturasRow.Item("EMPRESA")
                dNewFacturaRow.Item("INSTANCIA") = vbEmpty
                dNewFacturaRow.Item("RET_GARANTIA") = vbEmpty
                dNewFacturaRow.Item("PAG") = dFacturasRow.Item("PAG")
                dNewFacturaRow.Item("VIA_PAG") = dFacturasRow.Item("VIA_PAG")
                dNewFacturaRow.Item("CONCEPTO") = concepto 'Concepto 0 Gasto/ Concepto 1 Inversion

                'ABONADO
                '<TIPO></TIPO>	//ORDEN_ENTREGA.ABONO   0= ORIGINAL F ,1 = RECTIFICATIVA A
                '<NUM_ORIGINAL></NUM_ORIGINAL>//CUANDO ES RECTIFICA VA LO SACO DE 			ORDEN_ENTREGA.FACTURA
                '<FEC_ORIGINAL_INI></FEC_ORIGINAL_INI>										ORDEN_ENTREGA.ABONO_DESDE
                '<FEC_ORIGINAL_FIN></FEC_ORIGINAL_FIN>										ORDEN_ENTREGA.ABONO_FIN
                '
                If dFacturasRow.Item("ABONO") = 1 Then
                    dNewFacturaRow.Item("TIPO") = dFacturasRow.Item("ABONO")
                    dNewFacturaRow.Item("NUM_ORIGINAL") = dFacturasRow.Item("FACTURA")
                    dNewFacturaRow.Item("TOT_COSTES") = vbEmpty
                    dNewFacturaRow.Item("FEC_ORIGINAL_INI") = dFacturasRow.Item("ABONO_DESDE")
                    dNewFacturaRow.Item("FEC_ORIGINAL_FIN") = dFacturasRow.Item("ABONO_HASTA")
                    dNewFacturaRow.Item("TOT_DESCUENTOS") = vbEmpty
                    dNewFacturaRow.Item("MON") = dFacturasRow.Item("MON")
                Else
                    dNewFacturaRow.Item("TIPO") = dFacturasRow.Item("ABONO")
                    dNewFacturaRow.Item("NUM_ORIGINAL") = vbEmpty
                    dNewFacturaRow.Item("TOT_COSTES") = vbEmpty
                    dNewFacturaRow.Item("FEC_ORIGINAL_INI") = DBNull.Value
                    dNewFacturaRow.Item("FEC_ORIGINAL_FIN") = DBNull.Value
                    dNewFacturaRow.Item("TOT_DESCUENTOS") = vbEmpty
                    dNewFacturaRow.Item("MON") = dFacturasRow.Item("MON")
                End If

                Try
                    ds.Tables("FACTURA").Rows.Add(dNewFacturaRow)
                    dtLineas = ds.Tables("FACTURA").Copy
                    dtLineas.TableName = "FACTURA"
                    dsEscribir.Tables.Add(dtLineas)
                    ds.Tables("FACTURA").Clear()
                    numDataset = numDataset + 1

                Catch ex As Exception
                    GrabarError("Error al añadir tabla factura", ex.Message)
                End Try

                Try
                    ''CARGAR LAS LINEAS SIN RECEPCION
                    For Each dRow In DsTrasCarga.Tables("LINEAS").Rows

                        If dFacturasRow.Item("PROVE") = dRow.Item("PROVE") AndAlso dFacturasRow.Item("MON") = dRow.Item("MON") AndAlso dFacturasRow.Item("ABONO") = dRow.Item("ABONO") AndAlso DBNullToInteger(dRow.Item("CONCEPTO")) = concepto Then
                            'EN CADA FACTURA TIENEN QUE ESTAR EL MISMO PROVEEDOR CON LA MISMA MONEDA EN LA LINEA DE FACTURA

                            dNewLineaRow = ds.Tables("LINEAS").NewRow
                            dNewLineaRow.Item("LINEA") = numeroLinea ' vbEmpty
                            dNewLineaRow.Item("NUM") = numeroLinea
                            dNewLineaRow.Item("TOT_COSTES") = vbEmpty
                            dNewLineaRow.Item("TOT_DESCUENTOS") = vbEmpty
                            dNewLineaRow.Item("IMPUESTOS") = vbEmpty
                            dNewLineaRow.Item("IMP_BRUTO") = vbEmpty
                            dNewLineaRow.Item("OBS") = DBNull.Value
                            dNewLineaRow.Item("LIN_PEDIDO") = dRow.Item("ID1")
                            dNewLineaRow.Item("ALBARAN") = DBNull.Value
                            sArt_Init = dRow.Item("ART_INT")
                            numeroLinea = numeroLinea + 1
                            'INICIO AÑADIR IMPORTE POR LINEA
                            dNewLineaRow.Item("IMPORTE") = dRow.Item("PREC_UP") * dRow.Item("CANT_PED")
                            dNewLineaRow.Item("RET_GARANTIA") = dRow.Item("PREC_UP") * dRow.Item("CANT_PED") * dRow.Item("IM_RETGARANTIA") / 100

                            'FIN AÑADIR IMPORTE POR LINEA

                            ds.Tables("LINEAS").Rows.Add(dNewLineaRow)
                            dtLineas = ds.Tables("LINEAS").Copy
                            dtLineas.TableName = "LINEAS" & numDataset 'mpf modifiacion para tratamiento de xml
                            dsEscribir.Tables.Add(dtLineas)
                            ds.Tables("LINEAS").Clear()
                            numPosLinea = numDataset ' este dato es para saber la posicion donde empieza la linea
                            numDataset = numDataset + 1

                            '' CARGAR COSTES LINEAS SIN RECEPCION
                            'tengo que hacer uno por Coste y otro por Descuento porque sino se mezclan
                            ''bucle para optimizar Costes no tengo que cargar todos solo los de un numero
                            Try
                                Dim dCostesRow As DataRow
                                If DsTrasCarga.Tables("COSTES_Y_DESCUENTOS") IsNot Nothing Then
                                    If DsTrasCarga.Tables("COSTES_Y_DESCUENTOS").Rows.Count >= 1 Then
                                        dCostesRow = DsTrasCarga.Tables("COSTES_Y_DESCUENTOS").Rows.Item(0)
                                        If dCostesRow IsNot Nothing Then
                                            Dim counter As Integer = 0

                                            While dCostesRow.Item("LINEA") <= dRow.item("ID1")
                                                If DsTrasCarga.Tables("COSTES_Y_DESCUENTOS") IsNot Nothing AndAlso DsTrasCarga.Tables("COSTES_Y_DESCUENTOS").Rows.Count >= counter Then

                                                    If counter <> DsTrasCarga.Tables("COSTES_Y_DESCUENTOS").Rows.Count Then
                                                        dCostesRow = DsTrasCarga.Tables("COSTES_Y_DESCUENTOS").Rows.Item(counter)
                                                    Else
                                                        Exit While 'porque se me va de rango 
                                                    End If

                                                    If dCostesRow.Item("LINEA") = dRow.item("ID1") Then

                                                        If dCostesRow.Item("TIPO") = 0 Then

                                                            dNewCosteRow = ds.Tables("COSTES").NewRow
                                                            dNewCosteRow.Item("LINEA") = numeroLinea 'añadiendo linea en xml dara problemas
                                                            dNewCosteRow.Item("ID") = dCostesRow.Item("ID") 'cambio cod por id
                                                            dNewCosteRow.Item("DEN") = dCostesRow.Item("DEN")
                                                            dNewCosteRow.Item("VALOR") = dCostesRow.Item("VALOR")
                                                            dNewCosteRow.Item("PLANIFICADO") = dCostesRow.Item("PLANIFICADO")
                                                            dNewCosteRow.Item("IMPORTE") = vbEmpty

                                                            'SACANDO EL IMPORTE
                                                            If dCostesRow.Item("OPERACION") = 0 Then
                                                                'SUMO
                                                                dNewCosteRow.Item("IMPORTE") = dCostesRow.Item("VALOR")
                                                                dNewCosteRow.Item("OPERACION") = "+" 'queremos que en xml salga + o +%

                                                            ElseIf dCostesRow.Item("OPERACION") = 1 Then
                                                                '% DEL IMPORTE
                                                                dNewCosteRow.Item("IMPORTE") = dCostesRow.Item("VALOR") / 100 * dRow.Item("PREC_UP") * dRow.Item("CANT_PED")
                                                                dNewCosteRow.Item("OPERACION") = "+%" 'queremos que en xml salga + o +%
                                                            End If

                                                            'FIN SACAR IMPORTE
                                                            'dNewCosteRow.Item("OPERACION") = dCostesRow.Item("OPERACION")

                                                            Try
                                                                ds.Tables("COSTES").Rows.Add(dNewCosteRow)
                                                                dtCostes = ds.Tables("COSTES").Copy
                                                                dtCostes.TableName = "COSTE" & numDataset
                                                                dsEscribir.Tables.Add(dtCostes)
                                                                ds.Tables("COSTES").Clear()
                                                                numDataset = numDataset + 1
                                                            Catch ex As Exception
                                                                GrabarError("Error al añadir un coste en el dataset a escribir.", ex.Message)
                                                            End Try
                                                        End If
                                                    End If
                                                    counter = counter + 1
                                                Else
                                                    Exit While
                                                End If

                                            End While
                                        End If
                                    End If 'validar es vacia costes y descuentos
                                End If

                                If DsTrasCarga.Tables("COSTES_Y_DESCUENTOS") IsNot Nothing Then
                                    If DsTrasCarga.Tables("COSTES_Y_DESCUENTOS").Rows.Count >= 1 Then
                                        dCostesRow = DsTrasCarga.Tables("COSTES_Y_DESCUENTOS").Rows.Item(0)
                                        If dCostesRow IsNot Nothing Then
                                            Dim counter As Integer = 0
                                            While dCostesRow.Item("LINEA") <= dRow.item("ID1")
                                                If DsTrasCarga.Tables("COSTES_Y_DESCUENTOS") IsNot Nothing AndAlso DsTrasCarga.Tables("COSTES_Y_DESCUENTOS").Rows.Count >= counter Then
                                                    If counter >= DsTrasCarga.Tables("COSTES_Y_DESCUENTOS").Rows.Count Then
                                                        Exit While
                                                    End If
                                                    dCostesRow = DsTrasCarga.Tables("COSTES_Y_DESCUENTOS").Rows.Item(counter)
                                                    'por que se me va de rango

                                                    If dCostesRow.Item("LINEA") = dRow.item("ID1") Then

                                                        If dCostesRow.Item("TIPO") = 1 Then

                                                            dNewDescuentoRow = ds.Tables("DESCUENTOS").NewRow
                                                            dNewDescuentoRow.Item("LINEA") = numeroLinea 'añadido para xml
                                                            dNewDescuentoRow.Item("ID") = dCostesRow.Item("ID")
                                                            dNewDescuentoRow.Item("DEN") = dCostesRow.Item("DEN")
                                                            dNewDescuentoRow.Item("VALOR") = dCostesRow.Item("VALOR")
                                                            dNewDescuentoRow.Item("PLANIFICADO") = dCostesRow.Item("PLANIFICADO")

                                                            'SACANDO EL IMPORTE
                                                            If dCostesRow.Item("OPERACION") = 0 Then
                                                                'SUMO
                                                                dNewDescuentoRow.Item("IMPORTE") = dCostesRow.Item("VALOR")
                                                                dNewDescuentoRow.Item("OPERACION") = "-" 'quermos que salga - o -% porque es un descuento
                                                            ElseIf dCostesRow.Item("OPERACION") = 1 Then
                                                                '% DEL IMPORTE
                                                                dNewDescuentoRow.Item("IMPORTE") = dCostesRow.Item("VALOR") / 100 * dRow.Item("PREC_UP") * dRow.Item("CANT_PED")
                                                                dNewDescuentoRow.Item("OPERACION") = "-%" 'quermos que salga - o -% porque es un descuento
                                                            End If
                                                            'dNewDescuentoRow.Item("OPERACION") = dCostesRow.Item("OPERACION")

                                                            Try

                                                                ds.Tables("DESCUENTOS").Rows.Add(dNewDescuentoRow)
                                                                dtDescuentos = ds.Tables("DESCUENTOS").Copy
                                                                dtDescuentos.TableName = numDataset
                                                                dtDescuentos.TableName = "DESCUENTO" & numDataset
                                                                dsEscribir.Tables.Add(dtDescuentos)
                                                                ds.Tables("DESCUENTOS").Clear()
                                                                numDataset = numDataset + 1

                                                            Catch ex As Exception
                                                                GrabarError("Error al añadir un descuento en el dataset a escribir.", ex.Message)
                                                            End Try
                                                        End If
                                                    End If
                                                    counter = counter + 1
                                                Else
                                                    Exit While
                                                End If
                                            End While
                                        End If
                                    End If 'Validar costes y descuentos vacios
                                End If

                                'FIN CARGAR DESCUENTOS LINEAS SIN RECEPCION

                                'CARGAR IMPUESTOS LINEAS SIN RECEPCION
                                'DEPENDEN DEL PROVEEDOR Y DEL ARTICULO   Prove y ART

                                For Each dImpuestoRow In DsTrasCarga.Tables("IMPUESTOS_PROV_ART").Rows

                                    If dImpuestoRow.item("PROVE") = dFacturasRow.Item("PROVE") AndAlso sArt_Init = dImpuestoRow.item("ART") AndAlso ((dImpuestoRow.item("CONCEPTO") = concepto) Or (dImpuestoRow.item("CONCEPTO") = iConceptoImpuesto)) Then

                                        dNewImpuestoRow = ds.Tables("IMPUESTOS").NewRow
                                        dNewImpuestoRow.Item("LINEA") = numeroLinea 'cambios xml
                                        dNewImpuestoRow.Item("COD") = dImpuestoRow.Item("COD")
                                        dNewImpuestoRow.Item("DEN") = dImpuestoRow.Item("DEN")
                                        dNewImpuestoRow.Item("VALOR") = dImpuestoRow.Item("VALOR")
                                        dNewImpuestoRow.Item("IMPORTE") = dRow.Item("PREC_UP") * dRow.Item("CANT_PED") * dImpuestoRow.Item("VALOR") / 100 '' TENGO QUE CALCULARLO CREO QUE ES dRow.Item("PREC_UP") * dRow.Item("CANT_PED")*VALOR /100
                                        dNewImpuestoRow.Item("RETENIDO") = dImpuestoRow.Item("RETENIDO")
                                        dNewImpuestoRow.Item("COMENT") = "" ' vbEmpty
                                        dNewImpuestoRow.Item("GRP_COMP") = dImpuestoRow.Item("GRP_COMP") 'ANEXO

                                        Try
                                            ds.Tables("IMPUESTOS").Rows.Add(dNewImpuestoRow)
                                            dtImpuestos = ds.Tables("IMPUESTOS").Copy
                                            dtImpuestos.TableName = numDataset
                                            dtImpuestos.TableName = "IMPUESTOS" & numDataset
                                            dsEscribir.Tables.Add(dtImpuestos)

                                            If dsImpuesto.Tables.Count = 0 Then
                                                dsImpuesto.Tables.Add(dtImpuestos.Copy)
                                                dtImpuestos.Dispose()
                                            Else
                                                Dim existeImpuesto As Integer
                                                existeImpuesto = 0
                                                For w As Integer = 0 To dsImpuesto.Tables(0).Rows.Count - 1
                                                    If dsImpuesto.Tables(0).Rows(w).Item("COD").Equals(DBNullToStr(dtImpuestos.Rows(0).Item("COD"))) Then
                                                        existeImpuesto = 1
                                                        Exit For
                                                    End If

                                                Next
                                                If existeImpuesto = 0 Then
                                                    dsImpuesto.Tables(0).ImportRow(dtImpuestos.Rows(0))

                                                Else
                                                    existeImpuesto = 0
                                                End If
                                            End If

                                            'INICIO AÑADIR EN LA TABLA POSICON COD IMPUESTO Y LINEA

                                            If dtPosTipoImpuesto Is Nothing Then
                                                dtPosTipoImpuesto = dsPosTipoImpuesto.Tables.Add("IMPUESTOS")

                                                Try
                                                    dtPosTipoImpuesto.Columns.Add("POSICION_LINEA", System.Type.GetType("System.Int32"))
                                                    dtPosTipoImpuesto.Columns.Add("COD", System.Type.GetType("System.String"))
                                                    dtPosTipoImpuesto.Columns.Add("LINEA", System.Type.GetType("System.Int32"))
                                                Catch ex As Exception
                                                    GrabarError("Al crear la tabla de impuestos.", ex.Message)
                                                End Try
                                            End If
                                            dnewPosTipoImpuesto = dsPosTipoImpuesto.Tables("IMPUESTOS").NewRow
                                            dnewPosTipoImpuesto.Item("POSICION_LINEA") = numPosLinea
                                            dnewPosTipoImpuesto.Item("COD") = dtImpuestos.Rows(0).Item("COD")
                                            dnewPosTipoImpuesto.Item("LINEA") = dRow.Item("ID1")

                                            dsPosTipoImpuesto.Tables(0).Rows.Add(dnewPosTipoImpuesto)

                                            'FIN DE AÑADIR EN LA TABLA POSICON COD IMPUESTO Y LINEA

                                            dtImpuestos.Dispose() 'me esta dejando basurilla
                                            ds.Tables("IMPUESTOS").Clear()
                                            numDataset = numDataset + 1

                                        Catch ex As Exception
                                            GrabarError("Al añadir el impuesto en el dataset a escribir.", ex.Message)
                                        End Try
                                    End If
                                Next

                                'FIN CARGAR IMPUESTOS LINEAS SIN RECEPCION

                            Catch ex As Exception
                                GrabarError("Al añadir las líneas sin recepción.", ex.Message)
                            End Try
                        End If 'FIN END IF CUANDO FACTURA PROVEEDOR NO ES IGUAL QUE LINEA_PEDIDO.PROVE
                    Next
                Catch ex As Exception
                    GrabarError("Al añadir las líneas sin recepción.", ex.Message)
                End Try

                ''INICIO CARGA DE LINEAS CON RECEPCIONES

                For Each dRow In DsTrasCarga.Tables("LINEAS_CON_RECEPCIONES").Rows
                    If dFacturasRow.Item("PROVE") = dRow.Item("PROVE") AndAlso dFacturasRow.Item("MON") = dRow.Item("MON") AndAlso dFacturasRow.Item("ABONO") = dRow.Item("ABONO") AndAlso DBNullToInteger(dRow.Item("CONCEPTO")) = concepto Then

                        dNewLineaRow = ds.Tables("LINEAS_CON_RECEPCIONES").NewRow
                        dNewLineaRow.Item("LINEA") = numeroLinea ' vbEmpty
                        dNewLineaRow.Item("NUM") = numeroLinea
                        dNewLineaRow.Item("TOT_COSTES") = vbEmpty
                        dNewLineaRow.Item("TOT_DESCUENTOS") = vbEmpty
                        dNewLineaRow.Item("IMPUESTOS") = vbEmpty
                        dNewLineaRow.Item("IMP_BRUTO") = vbEmpty
                        dNewLineaRow.Item("OBS") = DBNull.Value
                        dNewLineaRow.Item("LIN_PEDIDO") = dRow.Item("ID1")  '"ID"
                        dNewLineaRow.Item("ALBARAN") = dRow.Item("ALBARAN")
                        numeroLinea = numeroLinea + 1
                        sArt_Init = dRow.Item("ART_INT") 'articulo para impuestos_prov_art

                        Try
                            'INICIO AÑADIR IMPORTE POR LINEA
                            dNewLineaRow.Item("IMPORTE") = dRow.Item("PREC_UP") * dRow.Item("CANT")
                            dNewLineaRow.Item("RET_GARANTIA") = dRow.Item("PREC_UP") * dRow.Item("CANT") * dRow.Item("IM_RETGARANTIA") / 100

                            'FIN AÑADIR IMPORTE POR LINEA

                            ds.Tables("LINEAS_CON_RECEPCIONES").Rows.Add(dNewLineaRow)
                            dtLineas = ds.Tables("LINEAS_CON_RECEPCIONES").Copy

                            dtLineas.TableName = "LINEAS_CON_RECEPCIONES" & numDataset
                            dsEscribir.Tables.Add(dtLineas)
                            ds.Tables("LINEAS_CON_RECEPCIONES").Clear()
                            numPosLinea = numDataset 'Este dato es para saber la posicion de la tabla linea_recep en el dataset
                            numDataset = numDataset + 1

                            'CARGAR COSTES DE CADA LINEA CON RECEPCIONES

                            Dim dCostesRow As DataRow
                            If DsTrasCarga.Tables("COSTES_Y_DESCUENTOS") IsNot Nothing Then
                                If DsTrasCarga.Tables("COSTES_Y_DESCUENTOS").Rows.Count >= 1 Then
                                    dCostesRow = DsTrasCarga.Tables("COSTES_Y_DESCUENTOS").Rows.Item(0)
                                    If dCostesRow IsNot Nothing Then
                                        Dim counter As Integer = 0

                                        While dCostesRow.Item("LINEA") <= dRow.item("ID1")
                                            If DsTrasCarga.Tables("COSTES_Y_DESCUENTOS") IsNot Nothing AndAlso DsTrasCarga.Tables("COSTES_Y_DESCUENTOS").Rows.Count >= counter Then
                                                Try
                                                    If counter >= DsTrasCarga.Tables("COSTES_Y_DESCUENTOS").Rows.Count Then
                                                        Exit While
                                                    Else
                                                        dCostesRow = DsTrasCarga.Tables("COSTES_Y_DESCUENTOS").Rows.Item(counter)
                                                    End If
                                                Catch ex As Exception
                                                    GrabarError("Al obtener el datarow", ex.Message)
                                                End Try

                                                If dCostesRow.Item("LINEA") = dRow.Item("ID1") Then

                                                    If dCostesRow.Item("TIPO") = 0 Then

                                                        dNewCosteRow = ds.Tables("COSTES").NewRow
                                                        dNewCosteRow.Item("LINEA") = numeroLinea 'añadido para xml
                                                        dNewCosteRow.Item("ID") = dCostesRow.Item("ID") 'cambio cod por id
                                                        dNewCosteRow.Item("DEN") = dCostesRow.Item("DEN")
                                                        dNewCosteRow.Item("VALOR") = dCostesRow.Item("VALOR")
                                                        dNewCosteRow.Item("PLANIFICADO") = dCostesRow.Item("PLANIFICADO")
                                                        dNewCosteRow.Item("IMPORTE") = vbEmpty

                                                        'SACANDO EL IMPORTE
                                                        If dCostesRow.Item("OPERACION") = 0 Then
                                                            'SUMO
                                                            dNewCosteRow.Item("IMPORTE") = dCostesRow.Item("VALOR")
                                                            dNewCosteRow.Item("OPERACION") = "+" 'queremos que salga + ,+%

                                                        ElseIf dCostesRow.Item("OPERACION") = 1 Then
                                                            '% DEL IMPORTE
                                                            dNewCosteRow.Item("IMPORTE") = dCostesRow.Item("VALOR") / 100 * dRow.Item("PREC_UP") * dRow.Item("CANT")
                                                            dNewCosteRow.Item("OPERACION") = "+%" 'queremos que salga + ,+%
                                                        End If

                                                        'FIN SACAR IMPORTE

                                                        Try

                                                            ds.Tables("COSTES").Rows.Add(dNewCosteRow)
                                                            dtCostes = ds.Tables("COSTES").Copy
                                                            dtCostes.TableName = "COSTE" & numDataset
                                                            dsEscribir.Tables.Add(dtCostes)
                                                            ds.Tables("COSTES").Clear()
                                                            numDataset = numDataset + 1

                                                        Catch ex As Exception
                                                            GrabarError("Al añadir la tabla coste en el dataset a escribir.", ex.Message)
                                                        End Try
                                                    End If
                                                End If
                                                counter = counter + 1
                                            Else
                                                Exit While
                                            End If
                                        End While
                                    End If
                                End If 'validar costes y descuentos vacio
                            End If

                            If DsTrasCarga.Tables("COSTES_Y_DESCUENTOS") IsNot Nothing Then
                                If DsTrasCarga.Tables("COSTES_Y_DESCUENTOS").Rows.Count >= 1 Then
                                    dCostesRow = DsTrasCarga.Tables("COSTES_Y_DESCUENTOS").Rows.Item(0)
                                    If dCostesRow IsNot Nothing Then
                                        Dim counter As Integer = 0

                                        While dCostesRow.Item("LINEA") <= dRow.item("ID1")
                                            If DsTrasCarga.Tables("COSTES_Y_DESCUENTOS") IsNot Nothing AndAlso DsTrasCarga.Tables("COSTES_Y_DESCUENTOS").Rows.Count >= counter Then
                                                Try
                                                    If counter >= DsTrasCarga.Tables("COSTES_Y_DESCUENTOS").Rows.Count Then
                                                        Exit While
                                                    Else
                                                        dCostesRow = DsTrasCarga.Tables("COSTES_Y_DESCUENTOS").Rows.Item(counter)
                                                    End If
                                                Catch ex As Exception
                                                    GrabarError("Al obtener datarow.", ex.Message)
                                                End Try
                                                If dCostesRow.Item("LINEA") = dRow.item("ID1") Then

                                                    If dCostesRow.Item("TIPO") = 1 Then

                                                        dNewCosteRow = ds.Tables("DESCUENTOS").NewRow
                                                        dNewCosteRow.Item("LINEA") = numeroLinea 'añadido para xml
                                                        dNewCosteRow.Item("ID") = dCostesRow.Item("ID")
                                                        dNewCosteRow.Item("DEN") = dCostesRow.Item("DEN")
                                                        dNewCosteRow.Item("VALOR") = dCostesRow.Item("VALOR")
                                                        dNewCosteRow.Item("PLANIFICADO") = dCostesRow.Item("PLANIFICADO")

                                                        'SACANDO EL IMPORTE
                                                        If dCostesRow.Item("OPERACION") = 0 Then
                                                            'SUMO
                                                            dNewCosteRow.Item("IMPORTE") = dCostesRow.Item("VALOR")
                                                            dNewCosteRow.Item("OPERACION") = "-" 'queremos que salga - ,-%

                                                        ElseIf dCostesRow.Item("OPERACION") = 1 Then
                                                            '% DEL IMPORTE
                                                            dNewCosteRow.Item("IMPORTE") = dCostesRow.Item("VALOR") / 100 * dRow.Item("PREC_UP") * dRow.Item("CANT")
                                                            dNewCosteRow.Item("OPERACION") = "-%" 'queremos que salga - ,-%
                                                        End If

                                                        Try

                                                            ds.Tables("DESCUENTOS").Rows.Add(dNewCosteRow)
                                                            dtDescuentos = ds.Tables("DESCUENTOS").Copy
                                                            dtDescuentos.TableName = numDataset
                                                            dtDescuentos.TableName = "DESCUENTO" & numDataset
                                                            dsEscribir.Tables.Add(dtDescuentos)
                                                            ds.Tables("DESCUENTOS").Clear()
                                                            numDataset = numDataset + 1

                                                        Catch ex As Exception
                                                            GrabarError("Al añadir la tabla descuentos en el dataset a escribir.", ex.Message)
                                                        End Try
                                                    End If
                                                End If
                                                counter = counter + 1

                                            Else
                                                Exit While
                                            End If

                                        End While
                                    End If
                                End If 'validar costes y descuentos es vacia
                            End If

                            'FIN CARGAR DESCUENTOS DE CADA LINEA CON RECEPCIONES
                            For Each dImpuestoRow In DsTrasCarga.Tables("IMPUESTOS_PROV_ART").Rows
                                If dImpuestoRow.item("PROVE") = dFacturasRow.Item("PROVE") AndAlso sArt_Init = dImpuestoRow.item("ART") AndAlso ((dImpuestoRow.item("CONCEPTO") = concepto) Or (dImpuestoRow.item("CONCEPTO") = iConceptoImpuesto)) Then

                                    dNewImpuestoRow = ds.Tables("IMPUESTOS").NewRow
                                    dNewImpuestoRow.Item("LINEA") = numeroLinea 'cambios xml
                                    dNewImpuestoRow.Item("COD") = dImpuestoRow.Item("COD")
                                    dNewImpuestoRow.Item("DEN") = dImpuestoRow.Item("DEN")
                                    dNewImpuestoRow.Item("VALOR") = dImpuestoRow.Item("VALOR")
                                    dNewImpuestoRow.Item("IMPORTE") = dRow.Item("PREC_UP") * dRow.Item("CANT") * dImpuestoRow.Item("VALOR") / 100 '' TENGO QUE CALCULARLO CREO QUE ES dRow.Item("PREC_UP") * dRow.Item("CANT_PED")*VALOR /100
                                    dNewImpuestoRow.Item("RETENIDO") = dImpuestoRow.Item("RETENIDO")
                                    dNewImpuestoRow.Item("COMENT") = "" 'vbEmpty
                                    dNewImpuestoRow.Item("GRP_COMP") = dImpuestoRow.Item("GRP_COMP") 'ANEXO2 EL CONCEPTO NO HACE FALTA PORQUE SOLO SE CARGA EL IMPUESTO CUANDO COINCIDE

                                    Try
                                        ds.Tables("IMPUESTOS").Rows.Add(dNewImpuestoRow)
                                        dtImpuestos = ds.Tables("IMPUESTOS").Copy
                                        dtImpuestos.TableName = numDataset
                                        dtImpuestos.TableName = "IMPUESTOS" & numDataset
                                        dsEscribir.Tables.Add(dtImpuestos)
                                        If dsImpuesto.Tables.Count = 0 Then
                                            dsImpuesto.Tables.Add(dtImpuestos.Copy)
                                            dtImpuestos.Dispose()
                                        Else
                                            'Miro si añado el impuesto
                                            Dim existeImpuesto As Integer
                                            existeImpuesto = 0
                                            For w As Integer = 0 To dsImpuesto.Tables(0).Rows.Count - 1
                                                If dsImpuesto.Tables(0).Rows(w).Item("COD").Equals(DBNullToStr(dtImpuestos.Rows(0).Item("COD"))) Then
                                                    existeImpuesto = 1
                                                    Exit For
                                                End If
                                            Next
                                            If existeImpuesto = 0 Then
                                                dsImpuesto.Tables(0).ImportRow(dtImpuestos.Rows(0))
                                            Else
                                                existeImpuesto = 0
                                            End If
                                        End If

                                        If dtPosTipoImpuesto Is Nothing Then

                                            dtPosTipoImpuesto = dsPosTipoImpuesto.Tables.Add("IMPUESTOS")

                                            Try
                                                dtPosTipoImpuesto.Columns.Add("POSICION_LINEA", System.Type.GetType("System.Int32"))
                                                dtPosTipoImpuesto.Columns.Add("COD", System.Type.GetType("System.String"))
                                                dtPosTipoImpuesto.Columns.Add("LINEA", System.Type.GetType("System.Int32"))
                                            Catch ex As Exception
                                                GrabarError("Al crear la tabla impuestos.", ex.Message)
                                            End Try
                                        End If
                                        dnewPosTipoImpuesto = dsPosTipoImpuesto.Tables("IMPUESTOS").NewRow
                                        dnewPosTipoImpuesto.Item("POSICION_LINEA") = numPosLinea
                                        dnewPosTipoImpuesto.Item("COD") = dtImpuestos.Rows(0).Item("COD")
                                        dnewPosTipoImpuesto.Item("LINEA") = dRow.Item("ID1")

                                        dsPosTipoImpuesto.Tables(0).Rows.Add(dnewPosTipoImpuesto)

                                        '            'FIN DE AÑADIR EN LA TABLA POSICON COD IMPUESTO Y LINEA

                                        dtImpuestos.Dispose() 'me esta dejando basurilla
                                        ds.Tables("IMPUESTOS").Clear()
                                        numDataset = numDataset + 1

                                    Catch ex As Exception
                                        GrabarError("Al añadir la tabla impuestos en el dataset a escribir.", ex.Message)
                                    End Try
                                End If
                            Next

                        Catch ex As Exception
                            GrabarError("Al añadir línea con recepción.", ex.Message)
                        End Try
                    End If
                Next

                ''FIN CARGA DE LINEAS CON RECEPCIONES
                ''Fin de Factura- linea- Costes -Descuento

                dsAux(0) = dsEscribir
                dsAux(1) = dsImpuesto ' No me interesa el valor y el coment
                dsAux(2) = dsPosTipoImpuesto

                'Agrupar impuestos
                If dsAux(0) IsNot Nothing AndAlso dsAux(1) IsNot Nothing AndAlso dsAux(2) IsNot Nothing Then
                    If dsAux(1).Tables.Count >= 1 Then
                        If dsAux(1).Tables(0).Rows.Count >= 1 Then
                            tipoMoneda = dsAux(0).Tables(0).Rows(0).Item("MON")

                            'Saco el numero de Lineas de la tabla impuestos 
                            If dsAux(1).Tables(0).Rows.Count = 1 Then
                                'si es solo uno solo tengo un impuesto y tendria que compararlo con el maximo

                                'Inicio Escribir cuando Tengo un Solo Impuesto
                                If dsAux(0).Tables(0).Rows(0).Item("NUM") < numeroMax Then
                                    dsAux(0).Tables(0).Rows(0).Item("NUM") = vbEmpty

                                    If dsAux(0).Tables.Count >= 2 Then
                                        'pongo mayor igual porque me salian xml con solo la tabla factura
                                        'INICO ESCRITURA XML

                                        dtSolicitudAux = GenerarDTSolicitud(tipoMoneda)
                                        dsAux(0).Tables.Add(dtSolicitudAux)
                                        dsAux(0).Tables(0).Rows(0).Item("INSTANCIA") = instancia 'añado la instancia a la factura
                                        dsAux(0) = ComprobarValorLinea(dsAux(0)) 'caso que me cambie le numero de linea
                                        dsAux(0).DataSetName = "NewDataSet" 'por formato para su tratamiento este debe ser el nombre del dataSet
                                        dsAux(0).WriteXml(ConfigurationManager.AppSettings("rutaXML") & "\" & usuario & "#" & instancia & "#" & idbloque & "#" & "F" & ".xml")
                                        NombresTags = NombreTablas(dsAux(0))
                                        RemplazarNumerosTags(NombresTags, ConfigurationManager.AppSettings("rutaXML") & "\" & usuario & "#" & instancia & "#" & idbloque & "#" & "F" & ".xml") 'quitando tags
                                        'FIN ESCRITURA XML

                                    End If
                                Else
                                    dsAux(0).Tables(0).Rows(0).Item("NUM") = vbEmpty
                                    GenerarXMLConLineaMaxima(dsAux(0), numeroMax)
                                End If
                                'Fin Escribir cuando Tengo un Solo Impuesto

                            Else
                                'Anexo 2 aqui lo trato si tengo impuestos de distintos grupos
                                Dim dtImpuestosAgrupados As DataTable
                                dtImpuestosAgrupados = ObtenerGruposCompatibilidad(dsAux(1).Tables(0)) 'COMPRUEBO SI TENGO VARIOS GRUPOS DE COMPATIBILIDAD

                                If dtImpuestosAgrupados.Rows.Count <= 1 Then
                                    'Inicio Escribir cuando No tiene impuestos o Solo tiene un Grupo
                                    'solo tengo que tratar dsAux(0)
                                    If dsAux(0).Tables(0) IsNot Nothing Then

                                        tipoMoneda = dsAux(0).Tables(0).Rows(0).Item("MON")

                                        If dsAux(0).Tables(0).Rows(0).Item("NUM") < numeroMax Then
                                            dsAux(0).Tables(0).Rows(0).Item("NUM") = vbEmpty

                                            If dsAux(0).Tables.Count >= 2 Then
                                                'pongo mayor igual porque me salian xml con solo la tabla factura
                                                'INICO ESCRITURA XML

                                                dtSolicitudAux = GenerarDTSolicitud(tipoMoneda)
                                                dsAux(0).Tables.Add(dtSolicitudAux)
                                                dsAux(0).Tables(0).Rows(0).Item("INSTANCIA") = instancia 'añado la instancia a la factura
                                                dsAux(0) = ComprobarValorLinea(dsAux(0)) 'caso que me cambie le numero de linea
                                                dsAux(0).DataSetName = "NewDataSet" 'por formato para su tratamiento este debe ser el nombre del dataSet
                                                dsAux(0).WriteXml(ConfigurationManager.AppSettings("rutaXML") & "\" & usuario & "#" & instancia & "#" & idbloque & "#" & "F" & ".xml")
                                                NombresTags = NombreTablas(dsAux(0))
                                                RemplazarNumerosTags(NombresTags, ConfigurationManager.AppSettings("rutaXML") & "\" & usuario & "#" & instancia & "#" & idbloque & "#" & "F" & ".xml") 'quitando tags
                                                'FIN ESCRITURA XML

                                            End If
                                        Else
                                            dsAux(0).Tables(0).Rows(0).Item("NUM") = vbEmpty
                                            GenerarXMLConLineaMaxima(dsAux(0), numeroMax)
                                        End If
                                    End If
                                    'Fin Escribir cuando No tiene impuestos

                                Else
                                    'cuando tiene mas de un grupo de compatibilidad el impuesto ira vacio si no estamos en ese grupo
                                    'dtImpuestosAgrupados.Rows(0).Item(0) es el grupo de compatibilidad que el primer impuesto seleccionado
                                    dsAux(0) = DataSetGrupodeCompatibilidad(dsAux(0), dtImpuestosAgrupados.Rows(0).Item(0)) ' dataset solo con los impuestos Grupo Compatibilidad

                                    'INICIO COMPROBAR TAMAÑO MAXIMO

                                    If dsAux(0) IsNot Nothing Then
                                        If dsAux(0).Tables(0) IsNot Nothing Then

                                            If dsAux(0).Tables(0).Rows(0).Item("NUM") < numeroMax Then
                                                dsAux(0).Tables(0).Rows(0).Item("NUM") = vbEmpty

                                                If dsAux(0).Tables.Count >= 2 Then
                                                    'pongo mayor igual porque me salian xml con solo la tabla factura
                                                    'INICO ESCRITURA XML

                                                    dtSolicitudAux = GenerarDTSolicitud(tipoMoneda)
                                                    dsAux(0).Tables.Add(dtSolicitudAux)
                                                    dsAux(0).Tables(0).Rows(0).Item("INSTANCIA") = instancia 'añado la instancia a la factura
                                                    dsAux(0) = ComprobarValorLinea(dsAux(0)) 'caso que me cambie le numero de linea
                                                    dsAux(0).DataSetName = "NewDataSet" 'por formato para su tratamiento este debe ser el nombre del dataSet
                                                    dsAux(0).WriteXml(ConfigurationManager.AppSettings("rutaXML") & "\" & usuario & "#" & instancia & "#" & idbloque & "#" & "F" & ".xml")
                                                    NombresTags = NombreTablas(dsAux(0))
                                                    RemplazarNumerosTags(NombresTags, ConfigurationManager.AppSettings("rutaXML") & "\" & usuario & "#" & instancia & "#" & idbloque & "#" & "F" & ".xml") 'quitando tags
                                                    'FIN ESCRITURA XML

                                                End If
                                            Else
                                                dsAux(0).Tables(0).Rows(0).Item("NUM") = vbEmpty
                                                GenerarXMLConLineaMaxima(dsAux(0), numeroMax)
                                            End If
                                        End If
                                    End If

                                End If
                            End If 'anexo
                            'vacio los datasets
                        Else
                            'Inicio Escribir cuando No tiene impuestos
                            'solo tengo que tratar dsAux(0)
                            If dsAux(0).Tables(0) IsNot Nothing Then

                                tipoMoneda = dsAux(0).Tables(0).Rows(0).Item("MON")

                                If dsAux(0).Tables(0).Rows(0).Item("NUM") < numeroMax Then
                                    dsAux(0).Tables(0).Rows(0).Item("NUM") = vbEmpty


                                    If dsAux(0).Tables.Count >= 2 Then
                                        'pongo mayor igual porque me salian xml con solo la tabla factura
                                        'INICO ESCRITURA XML

                                        dtSolicitudAux = GenerarDTSolicitud(tipoMoneda)
                                        dsAux(0).Tables.Add(dtSolicitudAux)
                                        dsAux(0).Tables(0).Rows(0).Item("INSTANCIA") = instancia 'añado la instancia a la factura
                                        dsAux(0) = ComprobarValorLinea(dsAux(0)) 'caso que me cambie le numero de linea
                                        dsAux(0).DataSetName = "NewDataSet" 'por formato para su tratamiento este debe ser el nombre del dataSet
                                        dsAux(0).WriteXml(ConfigurationManager.AppSettings("rutaXML") & "\" & usuario & "#" & instancia & "#" & idbloque & "#" & "F" & ".xml")
                                        NombresTags = NombreTablas(dsAux(0))
                                        RemplazarNumerosTags(NombresTags, ConfigurationManager.AppSettings("rutaXML") & "\" & usuario & "#" & instancia & "#" & idbloque & "#" & "F" & ".xml") 'quitando tags
                                        'FIN ESCRITURA XML

                                    End If
                                Else
                                    dsAux(0).Tables(0).Rows(0).Item("NUM") = vbEmpty
                                    GenerarXMLConLineaMaxima(dsAux(0), numeroMax)
                                End If
                            End If
                            'Fin Escribir cuando No tiene impuestos
                        End If
                    Else

                        'Inicio Escribir cuando No tiene impuestos
                        'solo tengo que tratar dsAux(0)
                        If dsAux(0).Tables(0) IsNot Nothing Then

                            tipoMoneda = dsAux(0).Tables(0).Rows(0).Item("MON")

                            If dsAux(0).Tables(0).Rows(0).Item("NUM") < numeroMax Then
                                dsAux(0).Tables(0).Rows(0).Item("NUM") = vbEmpty


                                If dsAux(0).Tables.Count >= 2 Then
                                    'pongo mayor igual porque me salian xml con solo la tabla factura
                                    'INICO ESCRITURA XML

                                    dtSolicitudAux = GenerarDTSolicitud(tipoMoneda)
                                    dsAux(0).Tables.Add(dtSolicitudAux)
                                    dsAux(0).Tables(0).Rows(0).Item("INSTANCIA") = instancia 'añado la instancia a la factura
                                    dsAux(0) = ComprobarValorLinea(dsAux(0)) 'caso que me cambie le numero de linea
                                    dsAux(0).DataSetName = "NewDataSet" 'por formato para su tratamiento este debe ser el nombre del dataSet
                                    dsAux(0).WriteXml(ConfigurationManager.AppSettings("rutaXML") & "\" & usuario & "#" & instancia & "#" & idbloque & "#" & "F" & ".xml")
                                    NombresTags = NombreTablas(dsAux(0))
                                    RemplazarNumerosTags(NombresTags, ConfigurationManager.AppSettings("rutaXML") & "\" & usuario & "#" & instancia & "#" & idbloque & "#" & "F" & ".xml") 'quitando tags
                                    'FIN ESCRITURA XML

                                End If
                            Else
                                dsAux(0).Tables(0).Rows(0).Item("NUM") = vbEmpty
                                GenerarXMLConLineaMaxima(dsAux(0), numeroMax)
                            End If
                        End If
                        'Fin Escribir cuando No tiene impuestos

                    End If

                    dsEscribir.Clear()
                    dsEscribir.Dispose()

                    If dsImpuesto IsNot Nothing Then
                        If dsImpuesto.Tables.Count >= 1 Then
                            If dsImpuesto.Tables(0).Rows.Count > 0 Then
                                dsImpuesto.Tables(0).Clear()
                            End If
                        End If
                    End If

                    If dsPosTipoImpuesto IsNot Nothing Then
                        If dsPosTipoImpuesto.Tables.Count >= 1 Then
                            If dsPosTipoImpuesto.Tables(0).Rows.Count > 0 Then
                                dsPosTipoImpuesto.Tables(0).Clear()
                            End If
                        End If
                    End If

                End If

            Next
        End If

    End Sub

    '''<summary>
    ''' agrupa todos los datos segun Proveedor ,Moneda , Abono y Gastos / Inversiones 
    ''' </summary>
    ''' <param name="DsTrasCarga">dataset que contiene todos los datos que vamos a utilizar en AutoFacturas</param>
    ''' <remarks>Llamada desde:GeneracionFacturas.vb GenerarDSAutoFactConCentroCoste; Tiempo máximo: segundos</remarks>
    Private Sub GenerarXMLPorGastos_Inversiones_PorCentroCoste(ByVal DsTrasCarga As DataSet, ByVal empresa As Integer, ByVal concepto As Integer)

        Dim ds As DataSet
        Dim dNewLineaRow As DataRow
        Dim numeroLinea As Integer = 1
        Dim dsEscribir As DataSet
        Dim dtLineas As DataTable
        Dim dtCostes As DataTable
        Dim dtDescuentos As DataTable
        Dim dtImpuestos As DataTable
        Dim dNewCosteRow As DataRow
        Dim sArt_Init As String ' para el articulo de cada linea
        Dim dNewDescuentoRow As DataRow
        Dim numDataset As Integer = 1
        Dim numPosLinea As Integer = 0 'Guardar la poscion de inicio de la linea
        Dim numLinea_Ped_Rec As Integer = 0 'Guarda el numero de la linea como metodo de validacion del dataset para saber que la linea de la posicion del dataset debe ser esta
        Dim dnewPosTipoImpuesto As DataRow
        Dim dtPosTipoImpuesto As DataTable
        Dim dsImpuestos As DataSet
        dsImpuestos = New DataSet

        ds = CargarEstructuraDataSetSimple(empresa)
        Dim dsAux(4) As DataSet
        Dim dsPosTipoImpuesto As DataSet
        dsPosTipoImpuesto = New DataSet

        Dim dsImpuesto As DataSet 'para guarda el tipo de impuestos que tengo
        dsImpuesto = New DataSet

        dsEscribir = New DataSet
        Dim dsTipoCentros As DataSet
        dsTipoCentros = New DataSet 'para guarda el tipo de centros que tengo
        Dim dtTipoCentro As DataTable
        Dim dnewTipoCentro As DataRow

        Dim existeCentro As Integer = 0

        Dim dsPosCentros As DataSet
        dsPosCentros = New DataSet
        Dim dnewPosTipoCentro As DataRow
        Dim dtPosTipoCentro As DataTable

        Dim dNewFacturaRow As DataRow
        Dim dFacturasRow As DataRow
        Dim dNewImpuestoRow As DataRow
        Dim TotalImporte As Double = 0
        Dim TotalCoste As Double = 0
        Dim totalDescuento As Double = 0
        Dim TotalRetencionGarantia As Double = 0

        'Inicio TRATO EL IMPUESTO CONCEPTO ESTO ES PORQUE EN EL CONCEPTO EN EL IMPUESTO PUEDE LLEVAR 0 GASTO/1 iNVERSION / 2 AMBOS (EN ESTE CASO LO AÑADO A GASTO) 
        Dim iConceptoImpuesto As Integer
        If concepto <> 1 Then
            iConceptoImpuesto = 2
        Else
            iConceptoImpuesto = 1
        End If
        'FIN TRATO EL IMPUESTO CONCEPTO

        'TENGO QUE SACAR TODOS LOS PROVEEDORES CON SU CORRESPONDIENTE MONEDA
        Dim dtProveMon As DataTable
        Dim existe As Integer = 0
        Dim dnewProveMon As DataRow
        Dim dtAuxProveMon As DataTable
        dtAuxProveMon = New DataTable

        'INICIO RECORRO LINEAS PARA PODEER SACAR TODOS LOS PROVEEDORES CON SU CORRESPONDIENTE MONEDA , ABONO (0 o 1) Y CONCEPTO (GASTO o INVERSION)

        For z As Integer = 0 To DsTrasCarga.Tables("LINEAS").Rows.Count - 1

            If dtProveMon IsNot Nothing Then
                For w As Integer = 0 To dtProveMon.Rows.Count - 1
                    If DBNullToStr(DsTrasCarga.Tables("LINEAS").Rows(z).Item("PROVE")).Equals(DBNullToStr(dtProveMon.Rows(w).Item("PROVE"))) AndAlso DBNullToStr(DsTrasCarga.Tables("LINEAS").Rows(z).Item("MON")).Equals(DBNullToStr(dtProveMon.Rows(w).Item("MON"))) AndAlso DBNullToInteger(DsTrasCarga.Tables("LINEAS").Rows(z).Item("ABONO")) = (DBNullToInteger(dtProveMon.Rows(w).Item("ABONO"))) AndAlso DBNullToInteger(DsTrasCarga.Tables("LINEAS").Rows(z).Item("CONCEPTO")) = concepto Then
                        existe = 1
                        Exit For
                    End If
                Next
            End If

            If existe = 0 Then

                If dtProveMon Is Nothing Then
                    Try
                        dtProveMon = DsTrasCarga.Tables("LINEAS").Clone
                    Catch ex As Exception
                        GrabarError("Al tratar de clonar datatable lineas.", ex.Message)
                    End Try
                End If

                If DBNullToInteger(DsTrasCarga.Tables("LINEAS").Rows(z).Item("CONCEPTO")) = concepto Then

                    dnewProveMon = dtProveMon.NewRow
                    dnewProveMon.Item("ID") = DsTrasCarga.Tables("LINEAS").Rows(z).Item("ID")
                    dnewProveMon.Item("PROVE") = DsTrasCarga.Tables("LINEAS").Rows(z).Item("PROVE")
                    dnewProveMon.Item("EMPRESA") = DsTrasCarga.Tables("LINEAS").Rows(z).Item("EMPRESA")
                    dnewProveMon.Item("PAG") = DsTrasCarga.Tables("LINEAS").Rows(z).Item("PAG")
                    dnewProveMon.Item("VIA_PAG") = DsTrasCarga.Tables("LINEAS").Rows(z).Item("VIA_PAG")
                    dnewProveMon.Item("OBS") = DsTrasCarga.Tables("LINEAS").Rows(z).Item("OBS")
                    dnewProveMon.Item("ABONO") = DsTrasCarga.Tables("LINEAS").Rows(z).Item("ABONO")
                    dnewProveMon.Item("ABONO_DESDE") = DsTrasCarga.Tables("LINEAS").Rows(z).Item("ABONO_DESDE")
                    dnewProveMon.Item("ABONO_HASTA") = DsTrasCarga.Tables("LINEAS").Rows(z).Item("ABONO_HASTA")
                    dnewProveMon.Item("MON") = DsTrasCarga.Tables("LINEAS").Rows(z).Item("MON")
                    dnewProveMon.Item("FACTURA") = DsTrasCarga.Tables("LINEAS").Rows(z).Item("FACTURA")
                    dnewProveMon.Item("ID1") = DsTrasCarga.Tables("LINEAS").Rows(z).Item("ID1")
                    dnewProveMon.Item("IM_FACTRASRECEP") = DsTrasCarga.Tables("LINEAS").Rows(z).Item("IM_FACTRASRECEP")
                    dnewProveMon.Item("PREC_UP") = DsTrasCarga.Tables("LINEAS").Rows(z).Item("PREC_UP")
                    dnewProveMon.Item("CANT_PED") = DsTrasCarga.Tables("LINEAS").Rows(z).Item("CANT_PED")
                    dnewProveMon.Item("IM_RETGARANTIA") = DsTrasCarga.Tables("LINEAS").Rows(z).Item("IM_RETGARANTIA")
                    dnewProveMon.Item("ART_INT") = DsTrasCarga.Tables("LINEAS").Rows(z).Item("ART_INT")

                    dtProveMon.Rows.Add(dnewProveMon)
                End If

            Else
                existe = 0
            End If

        Next
        'FIN RECORRO LINEAS PARA PODEER SACAR TODOS LOS PROVEEDORES CON SU CORRESPONDIENTE MONEDA , ABONO (0 o 1) Y CONCEPTO (GASTO o INVERSION)

        existe = 0

        'INICIO RECORRO LINEAS_CON_RECEPCIONES PARA PODEER SACAR TODOS LOS PROVEEDORES CON SU CORRESPONDIENTE MONEDA , ABONO (0 o 1) Y CONCEPTO (GASTO o INVERSION)

        For z As Integer = 0 To DsTrasCarga.Tables("LINEAS_CON_RECEPCIONES").Rows.Count - 1

            If dtProveMon IsNot Nothing Then
                For w As Integer = 0 To dtProveMon.Rows.Count - 1
                    If DBNullToStr(DsTrasCarga.Tables("LINEAS_CON_RECEPCIONES").Rows(z).Item("PROVE")).Equals(DBNullToStr(dtProveMon.Rows(w).Item("PROVE"))) AndAlso DBNullToStr(DsTrasCarga.Tables("LINEAS_CON_RECEPCIONES").Rows(z).Item("MON")).Equals(DBNullToStr(dtProveMon.Rows(w).Item("MON"))) AndAlso DBNullToInteger(DsTrasCarga.Tables("LINEAS_CON_RECEPCIONES").Rows(z).Item("ABONO")) = (DBNullToInteger(dtProveMon.Rows(w).Item("ABONO"))) AndAlso DBNullToInteger(DsTrasCarga.Tables("LINEAS_CON_RECEPCIONES").Rows(z).Item("CONCEPTO")) = concepto Then
                        existe = 1
                        Exit For
                    End If

                Next
            End If

            If existe = 0 Then

                If dtProveMon Is Nothing Then
                    Try
                        dtProveMon = DsTrasCarga.Tables("LINEAS_CON_RECEPCIONES").Clone
                    Catch ex As Exception
                        GrabarError("Al tratar de clonar la tabla líneas_con_recepciones.", ex.Message)
                    End Try
                End If

                If DBNullToInteger(DsTrasCarga.Tables("LINEAS_CON_RECEPCIONES").Rows(z).Item("CONCEPTO")) = concepto Then
                    dnewProveMon = dtProveMon.NewRow
                    dnewProveMon.Item("ID") = DsTrasCarga.Tables("LINEAS_CON_RECEPCIONES").Rows(z).Item("ID")
                    dnewProveMon.Item("PROVE") = DsTrasCarga.Tables("LINEAS_CON_RECEPCIONES").Rows(z).Item("PROVE")
                    dnewProveMon.Item("EMPRESA") = DsTrasCarga.Tables("LINEAS_CON_RECEPCIONES").Rows(z).Item("EMPRESA")
                    dnewProveMon.Item("PAG") = DsTrasCarga.Tables("LINEAS_CON_RECEPCIONES").Rows(z).Item("PAG")
                    dnewProveMon.Item("VIA_PAG") = DsTrasCarga.Tables("LINEAS_CON_RECEPCIONES").Rows(z).Item("VIA_PAG")
                    dnewProveMon.Item("OBS") = DsTrasCarga.Tables("LINEAS_CON_RECEPCIONES").Rows(z).Item("OBS")
                    dnewProveMon.Item("ABONO") = DsTrasCarga.Tables("LINEAS_CON_RECEPCIONES").Rows(z).Item("ABONO")
                    dnewProveMon.Item("ABONO_DESDE") = DsTrasCarga.Tables("LINEAS_CON_RECEPCIONES").Rows(z).Item("ABONO_DESDE")
                    dnewProveMon.Item("ABONO_HASTA") = DsTrasCarga.Tables("LINEAS_CON_RECEPCIONES").Rows(z).Item("ABONO_HASTA")
                    dnewProveMon.Item("MON") = DsTrasCarga.Tables("LINEAS_CON_RECEPCIONES").Rows(z).Item("MON")
                    dnewProveMon.Item("FACTURA") = DsTrasCarga.Tables("LINEAS_CON_RECEPCIONES").Rows(z).Item("FACTURA")
                    dnewProveMon.Item("ID1") = DsTrasCarga.Tables("LINEAS_CON_RECEPCIONES").Rows(z).Item("ID1")
                    dnewProveMon.Item("IM_FACTRASRECEP") = DsTrasCarga.Tables("LINEAS_CON_RECEPCIONES").Rows(z).Item("IM_FACTRASRECEP")
                    dnewProveMon.Item("PREC_UP") = DsTrasCarga.Tables("LINEAS_CON_RECEPCIONES").Rows(z).Item("PREC_UP")
                    dnewProveMon.Item("CANT_PED") = DsTrasCarga.Tables("LINEAS_CON_RECEPCIONES").Rows(z).Item("CANT_PED")
                    dnewProveMon.Item("IM_RETGARANTIA") = DsTrasCarga.Tables("LINEAS_CON_RECEPCIONES").Rows(z).Item("IM_RETGARANTIA")
                    dnewProveMon.Item("ART_INT") = DsTrasCarga.Tables("LINEAS_CON_RECEPCIONES").Rows(z).Item("ART_INT")

                    dtProveMon.Rows.Add(dnewProveMon)
                End If

            Else
                existe = 0
            End If

        Next
        'FIN RECORRO LINEAS_CON_RECEPCIONES PARA PODEER SACAR TODOS LOS PROVEEDORES CON SU CORRESPONDIENTE MONEDA , ABONO (0 o 1) Y CONCEPTO (GASTO o INVERSION)

        'FIN CARGA PROVEMON

        'CARGO LAS FACTURAS AGRUPADAS POR PROVEEDOR , MONEDA , ABONO Y CONCEPTO (GASTO o INVERSION)
        Try
            If dtProveMon IsNot Nothing Then

                For z As Integer = 0 To dtProveMon.Rows.Count - 1

                    numDataset = 1 ' lo inicialido en cada vuelta

                    dFacturasRow = dtProveMon.Rows(z)
                    dNewFacturaRow = ds.Tables("FACTURA").NewRow
                    dNewFacturaRow.Item("ID") = vbEmpty
                    dNewFacturaRow.Item("NUM") = vbEmpty
                    dNewFacturaRow.Item("PROVE") = dFacturasRow.Item("PROVE")
                    dNewFacturaRow.Item("ESTADO") = vbEmpty   'LA FACTURA GENERADA TENDRA ESTADO= 0
                    dNewFacturaRow.Item("FECHA") = Date.Now
                    dNewFacturaRow.Item("IMPORTE") = vbEmpty
                    dNewFacturaRow.Item("FEC_CONT") = DBNull.Value
                    dNewFacturaRow.Item("EMPRESA") = dFacturasRow.Item("EMPRESA")
                    dNewFacturaRow.Item("INSTANCIA") = vbEmpty
                    dNewFacturaRow.Item("RET_GARANTIA") = vbEmpty
                    dNewFacturaRow.Item("PAG") = dFacturasRow.Item("PAG")
                    dNewFacturaRow.Item("VIA_PAG") = dFacturasRow.Item("VIA_PAG")
                    dNewFacturaRow.Item("CONCEPTO") = concepto 'Concepto 0 Gasto/ Concepto 1 Inversion

                    If dFacturasRow.Item("ABONO") = 1 Then
                        dNewFacturaRow.Item("TIPO") = 2 'Rectifiactiva
                        dNewFacturaRow.Item("NUM_ORIGINAL") = dFacturasRow.Item("FACTURA")
                        dNewFacturaRow.Item("FEC_ORIGINAL_INI") = dFacturasRow.Item("ABONO_DESDE")
                        dNewFacturaRow.Item("FEC_ORIGINAL_FIN") = dFacturasRow.Item("ABONO_HASTA")
                    Else
                        dNewFacturaRow.Item("TIPO") = 1 'Original
                        dNewFacturaRow.Item("NUM_ORIGINAL") = vbEmpty
                        dNewFacturaRow.Item("FEC_ORIGINAL_INI") = DBNull.Value
                        dNewFacturaRow.Item("FEC_ORIGINAL_FIN") = DBNull.Value
                    End If

                    dNewFacturaRow.Item("TOT_COSTES") = vbEmpty
                    dNewFacturaRow.Item("TOT_DESCUENTOS") = vbEmpty
                    dNewFacturaRow.Item("MON") = dFacturasRow.Item("MON")

                    Try

                        ds.Tables("FACTURA").Rows.Add(dNewFacturaRow)
                        dtLineas = ds.Tables("FACTURA").Copy
                        dtLineas.TableName = "FACTURA"
                        dsEscribir.Tables.Add(dtLineas)
                        ds.Tables("FACTURA").Clear()
                        numDataset = numDataset + 1
                    Catch ex As Exception
                        GrabarError("Error al añadir la tabla factura en dataset a escribir " & DBNullToInteger(empresa), ex.Message)
                    End Try

                    ''CARGAR LAS LINEAS SIN RECEPCION
                    For Each dRow In DsTrasCarga.Tables("LINEAS").Rows

                        If dFacturasRow.Item("PROVE") = dRow.Item("PROVE") AndAlso dFacturasRow.Item("MON") = dRow.Item("MON") AndAlso dFacturasRow.Item("ABONO") = dRow.Item("ABONO") AndAlso dRow.Item("CONCEPTO") = concepto Then
                            'EN CADA FACTURA TIENEN QUE ESTAR EL MISMO PROVEEDOR CON LA MISMA MONEDA EN LA LINEA DE FACTURA

                            dNewLineaRow = ds.Tables("LINEAS").NewRow
                            dNewLineaRow.Item("LINEA") = numeroLinea ' vbEmpty
                            dNewLineaRow.Item("NUM") = numeroLinea
                            dNewLineaRow.Item("TOT_COSTES") = vbEmpty
                            dNewLineaRow.Item("TOT_DESCUENTOS") = vbEmpty
                            dNewLineaRow.Item("IMPUESTOS") = vbEmpty
                            dNewLineaRow.Item("IMP_BRUTO") = vbEmpty
                            dNewLineaRow.Item("OBS") = DBNull.Value
                            dNewLineaRow.Item("LIN_PEDIDO") = dRow.Item("ID1")
                            dNewLineaRow.Item("ALBARAN") = DBNull.Value
                            sArt_Init = dRow.Item("ART_INT")
                            numeroLinea = numeroLinea + 1
                            'INICIO AÑADIR IMPORTE POR LINEA
                            dNewLineaRow.Item("IMPORTE") = dRow.Item("PREC_UP") * dRow.Item("CANT_PED")
                            dNewLineaRow.Item("RET_GARANTIA") = dRow.Item("PREC_UP") * dRow.Item("CANT_PED") * dRow.Item("IM_RETGARANTIA") / 100

                            'FIN AÑADIR IMPORTE POR LINEA

                            ds.Tables("LINEAS").Rows.Add(dNewLineaRow)
                            dtLineas = ds.Tables("LINEAS").Copy
                            dtLineas.TableName = "LINEAS" & numDataset 'mpf añadiendo lineas tema de tratamiento xml 
                            dsEscribir.Tables.Add(dtLineas)
                            'añadiendo centros de coste 
                            For v As Integer = 0 To DsTrasCarga.Tables("LINEAS_CENTROS_COSTE").Rows.Count - 1
                                If ds.Tables("LINEAS").Rows(0).Item("LIN_PEDIDO") = DsTrasCarga.Tables("LINEAS_CENTROS_COSTE").Rows(v).Item("LINEA") Then

                                    If dtPosTipoCentro Is Nothing Then
                                        dtPosTipoCentro = dsPosCentros.Tables.Add("CENTROS")

                                        Try
                                            dtPosTipoCentro.Columns.Add("LINEA", System.Type.GetType("System.Int32"))
                                            dtPosTipoCentro.Columns.Add("UON1", System.Type.GetType("System.String"))
                                            dtPosTipoCentro.Columns.Add("UON2", System.Type.GetType("System.String"))
                                            dtPosTipoCentro.Columns.Add("UON3", System.Type.GetType("System.String"))
                                            dtPosTipoCentro.Columns.Add("UON4", System.Type.GetType("System.String"))
                                        Catch ex As Exception
                                            GrabarError("Al generar la tabla centros.", ex.Message)
                                        End Try
                                    End If

                                    dnewPosTipoCentro = dsPosCentros.Tables("CENTROS").NewRow
                                    dnewPosTipoCentro.Item("LINEA") = DsTrasCarga.Tables("LINEAS_CENTROS_COSTE").Rows(v).Item("LINEA")
                                    dnewPosTipoCentro.Item("UON1") = DBNullToStr(DsTrasCarga.Tables("LINEAS_CENTROS_COSTE").Rows(v).Item("UON1"))
                                    dnewPosTipoCentro.Item("UON2") = DBNullToStr(DsTrasCarga.Tables("LINEAS_CENTROS_COSTE").Rows(v).Item("UON2"))
                                    dnewPosTipoCentro.Item("UON3") = DBNullToStr(DsTrasCarga.Tables("LINEAS_CENTROS_COSTE").Rows(v).Item("UON3"))
                                    dnewPosTipoCentro.Item("UON4") = DBNullToStr(DsTrasCarga.Tables("LINEAS_CENTROS_COSTE").Rows(v).Item("UON4"))

                                    dsPosCentros.Tables(0).Rows.Add(dnewPosTipoCentro)
                                    'añado el Tipo de centros
                                    If dtTipoCentro Is Nothing Then

                                        dtTipoCentro = dsTipoCentros.Tables.Add("TIPO_CENTROS")

                                        Try
                                            dtTipoCentro.Columns.Add("UON1", System.Type.GetType("System.String"))
                                            dtTipoCentro.Columns.Add("UON2", System.Type.GetType("System.String"))
                                            dtTipoCentro.Columns.Add("UON3", System.Type.GetType("System.String"))
                                            dtTipoCentro.Columns.Add("UON4", System.Type.GetType("System.String"))
                                        Catch ex As Exception
                                            GrabarError("Al tratar de generar la tabla Tipo Centros.", ex.Message)
                                        End Try
                                    End If

                                    dnewTipoCentro = dsTipoCentros.Tables("TIPO_CENTROS").NewRow
                                    dnewTipoCentro.Item("UON1") = DBNullToStr(DsTrasCarga.Tables("LINEAS_CENTROS_COSTE").Rows(v).Item("UON1"))
                                    dnewTipoCentro.Item("UON2") = DBNullToStr(DsTrasCarga.Tables("LINEAS_CENTROS_COSTE").Rows(v).Item("UON2"))
                                    dnewTipoCentro.Item("UON3") = DBNullToStr(DsTrasCarga.Tables("LINEAS_CENTROS_COSTE").Rows(v).Item("UON3"))
                                    dnewTipoCentro.Item("UON4") = DBNullToStr(DsTrasCarga.Tables("LINEAS_CENTROS_COSTE").Rows(v).Item("UON4"))

                                    If dsTipoCentros.Tables(0).Rows.Count = 0 Then
                                        dsTipoCentros.Tables(0).Rows.Add(dnewTipoCentro) 'aqui añado todos solo debo añadir los que no estan

                                    Else
                                        For w As Integer = 0 To dsTipoCentros.Tables(0).Rows.Count - 1
                                            If dnewTipoCentro.Item("UON1").Equals(dsTipoCentros.Tables(0).Rows(w).Item("UON1")) AndAlso dnewTipoCentro.Item("UON2").Equals(dsTipoCentros.Tables(0).Rows(w).Item("UON2")) AndAlso dnewTipoCentro.Item("UON3").Equals(dsTipoCentros.Tables(0).Rows(w).Item("UON3")) AndAlso dnewTipoCentro.Item("UON4").Equals(dsTipoCentros.Tables(0).Rows(w).Item("UON4")) Then
                                                existeCentro = 1
                                                Exit For
                                            End If
                                        Next
                                        If existeCentro = 0 Then
                                            dsTipoCentros.Tables(0).Rows.Add(dnewTipoCentro)
                                        Else
                                            existeCentro = 0
                                        End If

                                    End If

                                End If

                            Next
                            'añadiendo centros de coste 
                            ds.Tables("LINEAS").Clear()
                            numPosLinea = numDataset ' este dato es para saber la posicion donde empieza la linea
                            numDataset = numDataset + 1

                            '' CARGAR COSTES LINEAS SIN RECEPCION
                            'tengo que hacer uno por Coste y otro por Descuento porque sino se mezclan
                            ''bucle para optimizar Costes no tengo que cargar todos solo los de un numero
                            Try
                                Dim dCostesRow As DataRow
                                If DsTrasCarga.Tables("COSTES_Y_DESCUENTOS") IsNot Nothing Then
                                    If DsTrasCarga.Tables("COSTES_Y_DESCUENTOS").Rows.Count >= 1 Then
                                        dCostesRow = DsTrasCarga.Tables("COSTES_Y_DESCUENTOS").Rows.Item(0)

                                        If dCostesRow IsNot Nothing Then
                                            Dim counter As Integer = 0

                                            While dCostesRow.Item("LINEA") <= dRow.item("ID1")
                                                If DsTrasCarga.Tables("COSTES_Y_DESCUENTOS") IsNot Nothing AndAlso DsTrasCarga.Tables("COSTES_Y_DESCUENTOS").Rows.Count >= counter Then
                                                    Try
                                                        If counter >= DsTrasCarga.Tables("COSTES_Y_DESCUENTOS").Rows.Count Then
                                                            Exit While
                                                        Else
                                                            dCostesRow = DsTrasCarga.Tables("COSTES_Y_DESCUENTOS").Rows.Item(counter)
                                                        End If
                                                    Catch ex As Exception
                                                        GrabarError("Al obtener datarow", ex.Message)
                                                    End Try
                                                    If dCostesRow.Item("LINEA") = dRow.item("ID1") Then

                                                        If dCostesRow.Item("TIPO") = 0 Then

                                                            dNewCosteRow = ds.Tables("COSTES").NewRow
                                                            dNewCosteRow.Item("LINEA") = numeroLinea 'cambios xml
                                                            dNewCosteRow.Item("ID") = dCostesRow.Item("ID") 'cambio cod por id
                                                            dNewCosteRow.Item("DEN") = dCostesRow.Item("DEN")
                                                            dNewCosteRow.Item("VALOR") = dCostesRow.Item("VALOR")
                                                            dNewCosteRow.Item("PLANIFICADO") = dCostesRow.Item("PLANIFICADO")
                                                            dNewCosteRow.Item("IMPORTE") = vbEmpty

                                                            'SACANDO EL IMPORTE
                                                            If dCostesRow.Item("OPERACION") = 0 Then
                                                                'SUMO
                                                                dNewCosteRow.Item("IMPORTE") = dCostesRow.Item("VALOR")
                                                                dNewCosteRow.Item("OPERACION") = "+" 'queremos este dato en el xml y no 1 o 0

                                                            ElseIf dCostesRow.Item("OPERACION") = 1 Then
                                                                '% DEL IMPORTE
                                                                dNewCosteRow.Item("IMPORTE") = dCostesRow.Item("VALOR") / 100 * dRow.Item("PREC_UP") * dRow.Item("CANT_PED")
                                                                dNewCosteRow.Item("OPERACION") = "+%" 'queremos este dato en el xml y no 1 o 0
                                                            End If

                                                            'FIN SACAR IMPORTE

                                                            Try

                                                                ds.Tables("COSTES").Rows.Add(dNewCosteRow)
                                                                dtCostes = ds.Tables("COSTES").Copy
                                                                dtCostes.TableName = "COSTE" & numDataset
                                                                dsEscribir.Tables.Add(dtCostes)
                                                                ds.Tables("COSTES").Clear()
                                                                numDataset = numDataset + 1

                                                            Catch ex As Exception
                                                                GrabarError("Al añadir la tabla coste al dataset donde escribimos.", ex.Message)
                                                            End Try
                                                        End If
                                                    End If
                                                    counter = counter + 1
                                                Else
                                                    Exit While
                                                End If

                                            End While
                                        End If
                                    End If 'costes vacio
                                End If

                                If DsTrasCarga.Tables("COSTES_Y_DESCUENTOS") IsNot Nothing Then
                                    If DsTrasCarga.Tables("COSTES_Y_DESCUENTOS").Rows.Count >= 1 Then
                                        dCostesRow = DsTrasCarga.Tables("COSTES_Y_DESCUENTOS").Rows.Item(0)
                                        If dCostesRow IsNot Nothing Then
                                            Dim counter As Integer = 0
                                            While dCostesRow.Item("LINEA") <= dRow.item("ID1")
                                                If DsTrasCarga.Tables("COSTES_Y_DESCUENTOS") IsNot Nothing AndAlso DsTrasCarga.Tables("COSTES_Y_DESCUENTOS").Rows.Count >= counter Then
                                                    Try
                                                        If counter >= DsTrasCarga.Tables("COSTES_Y_DESCUENTOS").Rows.Count Then
                                                            Exit While
                                                        Else
                                                            dCostesRow = DsTrasCarga.Tables("COSTES_Y_DESCUENTOS").Rows.Item(counter)
                                                        End If

                                                    Catch ex As Exception
                                                        GrabarError("Al obtener un datarow inexistente " & DBNullToInteger(empresa), ex.Message)
                                                    End Try
                                                    If dCostesRow.Item("LINEA") = dRow.item("ID1") Then

                                                        If dCostesRow.Item("TIPO") = 1 Then

                                                            dNewDescuentoRow = ds.Tables("DESCUENTOS").NewRow
                                                            dNewDescuentoRow.Item("LINEA") = numeroLinea 'CAMBIOS XML
                                                            dNewDescuentoRow.Item("ID") = dCostesRow.Item("ID")
                                                            dNewDescuentoRow.Item("DEN") = dCostesRow.Item("DEN")
                                                            dNewDescuentoRow.Item("VALOR") = dCostesRow.Item("VALOR")
                                                            dNewDescuentoRow.Item("PLANIFICADO") = dCostesRow.Item("PLANIFICADO")

                                                            'SACANDO EL IMPORTE
                                                            If dCostesRow.Item("OPERACION") = 0 Then
                                                                'SUMO
                                                                dNewDescuentoRow.Item("IMPORTE") = dCostesRow.Item("VALOR")
                                                                dNewDescuentoRow.Item("OPERACION") = "-" 'queremos este dato en el xml y no 1 o 0

                                                            ElseIf dCostesRow.Item("OPERACION") = 1 Then
                                                                '% DEL IMPORTE
                                                                dNewDescuentoRow.Item("IMPORTE") = dCostesRow.Item("VALOR") / 100 * dRow.Item("PREC_UP") * dRow.Item("CANT_PED")
                                                                dNewDescuentoRow.Item("OPERACION") = "-%" 'queremos este dato en el xml y no 1 o 0
                                                            End If

                                                            Try

                                                                ds.Tables("DESCUENTOS").Rows.Add(dNewDescuentoRow)
                                                                dtDescuentos = ds.Tables("DESCUENTOS").Copy
                                                                dtDescuentos.TableName = numDataset
                                                                dtDescuentos.TableName = "DESCUENTO" & numDataset
                                                                dsEscribir.Tables.Add(dtDescuentos)
                                                                ds.Tables("DESCUENTOS").Clear()
                                                                numDataset = numDataset + 1

                                                            Catch ex As Exception
                                                                GrabarError("Al añadir la tabla descuentos al dataset donde escribimos.", ex.Message)
                                                            End Try
                                                        End If
                                                    End If
                                                    counter = counter + 1
                                                Else
                                                    Exit While

                                                End If

                                            End While
                                        End If
                                    End If 'validar vacio en costes y descuentos
                                End If

                                'FIN CARGAR DESCUENTOS LINEAS SIN RECEPCION

                                'CARGAR IMPUESTOS LINEAS SIN RECEPCION
                                'DEPENDEN DEL PROVEEDOR Y DEL ARTICULO   Prove y ART
                                For Each dImpuestoRow In DsTrasCarga.Tables("IMPUESTOS_PROV_ART").Rows
                                    If dImpuestoRow.item("PROVE") = dFacturasRow.Item("PROVE") AndAlso sArt_Init = dImpuestoRow.item("ART") AndAlso ((dImpuestoRow.item("CONCEPTO") = concepto) Or (dImpuestoRow.item("CONCEPTO") = iConceptoImpuesto)) Then

                                        dNewImpuestoRow = ds.Tables("IMPUESTOS").NewRow
                                        dNewImpuestoRow.Item("LINEA") = numeroLinea 'cambios xml
                                        dNewImpuestoRow.Item("COD") = dImpuestoRow.Item("COD")
                                        dNewImpuestoRow.Item("DEN") = dImpuestoRow.Item("DEN")
                                        dNewImpuestoRow.Item("VALOR") = dImpuestoRow.Item("VALOR")
                                        dNewImpuestoRow.Item("IMPORTE") = dRow.Item("PREC_UP") * dRow.Item("CANT_PED") * dImpuestoRow.Item("VALOR") / 100 '' TENGO QUE CALCULARLO CREO QUE ES dRow.Item("PREC_UP") * dRow.Item("CANT_PED")*VALOR /100
                                        dNewImpuestoRow.Item("RETENIDO") = dImpuestoRow.Item("RETENIDO")
                                        dNewImpuestoRow.Item("COMENT") = "" ' vbEmpty
                                        dNewImpuestoRow.Item("GRP_COMP") = dImpuestoRow.Item("GRP_COMP") 'ANEXO

                                        Try
                                            ds.Tables("IMPUESTOS").Rows.Add(dNewImpuestoRow)
                                            dtImpuestos = ds.Tables("IMPUESTOS").Copy
                                            dtImpuestos.TableName = numDataset
                                            dtImpuestos.TableName = "IMPUESTOS" & numDataset
                                            dsEscribir.Tables.Add(dtImpuestos)

                                            If dsImpuesto.Tables.Count = 0 Then
                                                dsImpuesto.Tables.Add(dtImpuestos.Copy)
                                                dtImpuestos.Dispose()
                                            Else

                                                Dim existeImpuesto As Integer
                                                existeImpuesto = 0
                                                For w As Integer = 0 To dsImpuesto.Tables(0).Rows.Count - 1
                                                    Try
                                                        If dsImpuesto.Tables(0).Rows(w).Item("COD").Equals(DBNullToStr(dtImpuestos.Rows(0).Item("COD"))) Then
                                                            existeImpuesto = 1
                                                            Exit For
                                                        End If
                                                    Catch ex As Exception
                                                        GrabarError("Al obtener un código de la tabla impuestos " & DBNullToInteger(empresa), ex.Message)
                                                    End Try
                                                Next
                                                If existeImpuesto = 0 Then
                                                    dsImpuesto.Tables(0).ImportRow(dtImpuestos.Rows(0))
                                                Else
                                                    existeImpuesto = 0
                                                End If
                                            End If

                                            'INICIO AÑADIR EN LA TABLA POSICON COD IMPUESTO Y LINEA

                                            If dtPosTipoImpuesto Is Nothing Then
                                                dtPosTipoImpuesto = dsPosTipoImpuesto.Tables.Add("IMPUESTOS")

                                                Try
                                                    dtPosTipoImpuesto.Columns.Add("POSICION_LINEA", System.Type.GetType("System.Int32"))
                                                    dtPosTipoImpuesto.Columns.Add("COD", System.Type.GetType("System.String"))
                                                    dtPosTipoImpuesto.Columns.Add("LINEA", System.Type.GetType("System.Int32"))
                                                Catch ex As Exception
                                                    GrabarError("Al crear la tabla impuestos " & DBNullToInteger(empresa), ex.Message)
                                                End Try
                                            End If

                                            dnewPosTipoImpuesto = dsPosTipoImpuesto.Tables("IMPUESTOS").NewRow
                                            dnewPosTipoImpuesto.Item("POSICION_LINEA") = numPosLinea
                                            dnewPosTipoImpuesto.Item("COD") = dtImpuestos.Rows(0).Item("COD")
                                            dnewPosTipoImpuesto.Item("LINEA") = dRow.Item("ID1")

                                            dsPosTipoImpuesto.Tables(0).Rows.Add(dnewPosTipoImpuesto)

                                            'FIN DE AÑADIR EN LA TABLA POSICON COD IMPUESTO Y LINEA

                                            dtImpuestos.Dispose() 'me esta dejando basurilla
                                            ds.Tables("IMPUESTOS").Clear()
                                            numDataset = numDataset + 1

                                        Catch ex As Exception
                                            GrabarError("Al añadir la tabla impuestos al dataset donde escribimos", ex.Message)
                                        End Try
                                    End If
                                Next

                                'FIN CARGAR IMPUESTOS LINEAS SIN RECEPCION

                            Catch ex As Exception
                                GrabarError("Al tratar de cargar líneas sin recepción", ex.Message)
                            End Try
                        End If 'FIN END IF CUANDO FACTURA PROVEEDOR NO ES IGUAL QUE LINEA_PEDIDO.PROVE
                    Next

                    ''INICIO CARGA DE LINEAS CON RECEPCIONES

                    For Each dRow In DsTrasCarga.Tables("LINEAS_CON_RECEPCIONES").Rows

                        If dFacturasRow.Item("PROVE") = dRow.Item("PROVE") AndAlso dFacturasRow.Item("MON") = dRow.Item("MON") AndAlso dFacturasRow.Item("ABONO") = dRow.Item("ABONO") AndAlso dRow.Item("CONCEPTO") = concepto Then

                            dNewLineaRow = ds.Tables("LINEAS_CON_RECEPCIONES").NewRow
                            dNewLineaRow.Item("LINEA") = numeroLinea 'vbEmpty
                            dNewLineaRow.Item("NUM") = numeroLinea
                            dNewLineaRow.Item("TOT_COSTES") = vbEmpty
                            dNewLineaRow.Item("TOT_DESCUENTOS") = vbEmpty
                            dNewLineaRow.Item("IMPUESTOS") = vbEmpty
                            dNewLineaRow.Item("IMP_BRUTO") = vbEmpty
                            dNewLineaRow.Item("OBS") = DBNull.Value
                            dNewLineaRow.Item("LIN_PEDIDO") = dRow.Item("ID1")  '"ID"
                            dNewLineaRow.Item("ALBARAN") = dRow.Item("ALBARAN")
                            numeroLinea = numeroLinea + 1
                            sArt_Init = dRow.Item("ART_INT") 'articulo para impuestos_prov_art

                            Try
                                'INICIO AÑADIR IMPORTE POR LINEA
                                dNewLineaRow.Item("IMPORTE") = dRow.Item("PREC_UP") * dRow.Item("CANT")
                                dNewLineaRow.Item("RET_GARANTIA") = dRow.Item("PREC_UP") * dRow.Item("CANT") * dRow.Item("IM_RETGARANTIA") / 100

                                'FIN AÑADIR IMPORTE POR LINEA

                                ds.Tables("LINEAS_CON_RECEPCIONES").Rows.Add(dNewLineaRow)
                                dtLineas = ds.Tables("LINEAS_CON_RECEPCIONES").Copy

                                dtLineas.TableName = "LINEAS_CON_RECEPCIONES" & numDataset
                                dsEscribir.Tables.Add(dtLineas)

                                'añadiendo centros de coste 
                                For v As Integer = 0 To DsTrasCarga.Tables("LINEAS_CENTROS_COSTE").Rows.Count - 1
                                    If dtLineas.Rows(0).Item("LIN_PEDIDO") = DsTrasCarga.Tables("LINEAS_CENTROS_COSTE").Rows(v).Item("LINEA") Then

                                        If dtPosTipoCentro Is Nothing Then
                                            dtPosTipoCentro = dsPosCentros.Tables.Add("CENTROS")

                                            Try
                                                dtPosTipoCentro.Columns.Add("LINEA", System.Type.GetType("System.Int32"))
                                                dtPosTipoCentro.Columns.Add("UON1", System.Type.GetType("System.String"))
                                                dtPosTipoCentro.Columns.Add("UON2", System.Type.GetType("System.String"))
                                                dtPosTipoCentro.Columns.Add("UON3", System.Type.GetType("System.String"))
                                                dtPosTipoCentro.Columns.Add("UON4", System.Type.GetType("System.String"))
                                            Catch ex As Exception
                                                GrabarError("Al crear la tabla centros.", ex.Message)
                                            End Try
                                        End If

                                        dnewPosTipoCentro = dsPosCentros.Tables("CENTROS").NewRow
                                        dnewPosTipoCentro.Item("LINEA") = DsTrasCarga.Tables("LINEAS_CENTROS_COSTE").Rows(v).Item("LINEA")
                                        dnewPosTipoCentro.Item("UON1") = DBNullToStr(DsTrasCarga.Tables("LINEAS_CENTROS_COSTE").Rows(v).Item("UON1"))
                                        dnewPosTipoCentro.Item("UON2") = DBNullToStr(DsTrasCarga.Tables("LINEAS_CENTROS_COSTE").Rows(v).Item("UON2"))
                                        dnewPosTipoCentro.Item("UON3") = DBNullToStr(DsTrasCarga.Tables("LINEAS_CENTROS_COSTE").Rows(v).Item("UON3"))
                                        dnewPosTipoCentro.Item("UON4") = DBNullToStr(DsTrasCarga.Tables("LINEAS_CENTROS_COSTE").Rows(v).Item("UON4"))

                                        dsPosCentros.Tables(0).Rows.Add(dnewPosTipoCentro)
                                        'añado el Tipo de centros
                                        If dtTipoCentro Is Nothing Then

                                            dtTipoCentro = dsTipoCentros.Tables.Add("TIPO_CENTROS")

                                            Try
                                                dtTipoCentro.Columns.Add("UON1", System.Type.GetType("System.String"))
                                                dtTipoCentro.Columns.Add("UON2", System.Type.GetType("System.String"))
                                                dtTipoCentro.Columns.Add("UON3", System.Type.GetType("System.String"))
                                                dtTipoCentro.Columns.Add("UON4", System.Type.GetType("System.String"))
                                            Catch ex As Exception
                                                GrabarError("Al crear la tabla tipo de centros.", ex.Message)
                                            End Try
                                        End If

                                        dnewTipoCentro = dsTipoCentros.Tables("TIPO_CENTROS").NewRow
                                        dnewTipoCentro.Item("UON1") = DBNullToStr(DsTrasCarga.Tables("LINEAS_CENTROS_COSTE").Rows(v).Item("UON1"))
                                        dnewTipoCentro.Item("UON2") = DBNullToStr(DsTrasCarga.Tables("LINEAS_CENTROS_COSTE").Rows(v).Item("UON2"))
                                        dnewTipoCentro.Item("UON3") = DBNullToStr(DsTrasCarga.Tables("LINEAS_CENTROS_COSTE").Rows(v).Item("UON3"))
                                        dnewTipoCentro.Item("UON4") = DBNullToStr(DsTrasCarga.Tables("LINEAS_CENTROS_COSTE").Rows(v).Item("UON4"))

                                        If dsTipoCentros.Tables(0).Rows.Count = 0 Then
                                            dsTipoCentros.Tables(0).Rows.Add(dnewTipoCentro) 'aqui añado todos solo debo añadir los que no estan

                                        Else
                                            For w As Integer = 0 To dsTipoCentros.Tables(0).Rows.Count - 1
                                                If dnewTipoCentro.Item("UON1").Equals(dsTipoCentros.Tables(0).Rows(w).Item("UON1")) AndAlso dnewTipoCentro.Item("UON2").Equals(dsTipoCentros.Tables(0).Rows(w).Item("UON2")) AndAlso dnewTipoCentro.Item("UON3").Equals(dsTipoCentros.Tables(0).Rows(w).Item("UON3")) AndAlso dnewTipoCentro.Item("UON4").Equals(dsTipoCentros.Tables(0).Rows(w).Item("UON4")) Then
                                                    existeCentro = 1
                                                    Exit For
                                                End If
                                            Next
                                            If existeCentro = 0 Then
                                                dsTipoCentros.Tables(0).Rows.Add(dnewTipoCentro)
                                            Else
                                                existeCentro = 0
                                            End If
                                        End If
                                    End If
                                Next
                                'añadiendo centros de coste 

                                ds.Tables("LINEAS_CON_RECEPCIONES").Clear()
                                numPosLinea = numDataset 'Este dato es para saber la posicion de la tabla linea_recep en el dataset
                                numDataset = numDataset + 1
                                ''CADA linea sumo el importe bruto sin costes y descuentos
                                TotalImporte = TotalImporte + dRow.Item("PREC_UP") * dRow.Item("CANT")
                                TotalRetencionGarantia = TotalRetencionGarantia + dRow.Item("PREC_UP") * dRow.Item("CANT") * dRow.Item("IM_RETGARANTIA") / 100

                                'CARGAR COSTES DE CADA LINEA CON RECEPCIONES

                                Dim dCostesRow As DataRow
                                If DsTrasCarga.Tables("COSTES_Y_DESCUENTOS") IsNot Nothing Then
                                    If DsTrasCarga.Tables("COSTES_Y_DESCUENTOS").Rows.Count >= 1 Then
                                        dCostesRow = DsTrasCarga.Tables("COSTES_Y_DESCUENTOS").Rows.Item(0)
                                        If dCostesRow IsNot Nothing Then
                                            Dim counter As Integer = 0
                                            While dCostesRow.Item("LINEA") <= dRow.item("ID1")
                                                If DsTrasCarga.Tables("COSTES_Y_DESCUENTOS") IsNot Nothing AndAlso DsTrasCarga.Tables("COSTES_Y_DESCUENTOS").Rows.Count >= counter Then

                                                    Try
                                                        If counter >= DsTrasCarga.Tables("COSTES_Y_DESCUENTOS").Rows.Count Then
                                                            Exit While
                                                        Else
                                                            dCostesRow = DsTrasCarga.Tables("COSTES_Y_DESCUENTOS").Rows.Item(counter)
                                                        End If
                                                    Catch ex As Exception
                                                        GrabarError("Error al obtener datarow.", ex.Message)
                                                    End Try

                                                    If dCostesRow.Item("LINEA") = dRow.Item("ID1") Then

                                                        If dCostesRow.Item("TIPO") = 0 Then

                                                            dNewCosteRow = ds.Tables("COSTES").NewRow
                                                            dNewCosteRow.Item("LINEA") = numeroLinea 'cambios xml
                                                            dNewCosteRow.Item("ID") = dCostesRow.Item("ID") 'cambio cod por id
                                                            dNewCosteRow.Item("DEN") = dCostesRow.Item("DEN")
                                                            dNewCosteRow.Item("VALOR") = dCostesRow.Item("VALOR")
                                                            dNewCosteRow.Item("PLANIFICADO") = dCostesRow.Item("PLANIFICADO")
                                                            dNewCosteRow.Item("IMPORTE") = vbEmpty

                                                            'SACANDO EL IMPORTE
                                                            If dCostesRow.Item("OPERACION") = 0 Then
                                                                'SUMO
                                                                dNewCosteRow.Item("IMPORTE") = dCostesRow.Item("VALOR")
                                                                dNewCosteRow.Item("OPERACION") = "+" 'para el xml

                                                            ElseIf dCostesRow.Item("OPERACION") = 1 Then
                                                                '% DEL IMPORTE
                                                                dNewCosteRow.Item("IMPORTE") = dCostesRow.Item("VALOR") / 100 * dRow.Item("PREC_UP") * dRow.Item("CANT")
                                                                dNewCosteRow.Item("OPERACION") = "+%" 'para el xml
                                                            End If

                                                            'FIN SACAR IMPORTE

                                                            Try

                                                                ds.Tables("COSTES").Rows.Add(dNewCosteRow)
                                                                dtCostes = ds.Tables("COSTES").Copy
                                                                dtCostes.TableName = "COSTE" & numDataset
                                                                dsEscribir.Tables.Add(dtCostes)
                                                                ds.Tables("COSTES").Clear()
                                                                numDataset = numDataset + 1

                                                            Catch ex As Exception
                                                                GrabarError("Al añadir la tabla costes en el dataset a escribir.", ex.Message)
                                                            End Try
                                                        End If
                                                    End If
                                                    counter = counter + 1
                                                Else
                                                    Exit While
                                                End If
                                            End While
                                        End If
                                    End If 'validar costes y descuentos vacios
                                End If

                                If DsTrasCarga.Tables("COSTES_Y_DESCUENTOS") IsNot Nothing Then
                                    If DsTrasCarga.Tables("COSTES_Y_DESCUENTOS").Rows.Count >= 1 Then
                                        dCostesRow = DsTrasCarga.Tables("COSTES_Y_DESCUENTOS").Rows.Item(0)
                                        If dCostesRow IsNot Nothing Then
                                            Dim counter As Integer = 0

                                            While dCostesRow.Item("LINEA") <= dRow.item("ID1")
                                                If DsTrasCarga.Tables("COSTES_Y_DESCUENTOS") IsNot Nothing AndAlso DsTrasCarga.Tables("COSTES_Y_DESCUENTOS").Rows.Count >= counter Then
                                                    Try
                                                        If counter >= DsTrasCarga.Tables("COSTES_Y_DESCUENTOS").Rows.Count Then
                                                            Exit While
                                                        Else
                                                            dCostesRow = DsTrasCarga.Tables("COSTES_Y_DESCUENTOS").Rows.Item(counter)
                                                        End If
                                                    Catch ex As Exception
                                                        GrabarError("Al obtener datarow.", ex.Message)
                                                    End Try

                                                    If dCostesRow.Item("LINEA") = dRow.item("ID1") Then

                                                        If dCostesRow.Item("TIPO") = 1 Then

                                                            dNewCosteRow = ds.Tables("DESCUENTOS").NewRow
                                                            dNewCosteRow.Item("LINEA") = numeroLinea
                                                            dNewCosteRow.Item("ID") = dCostesRow.Item("ID")
                                                            dNewCosteRow.Item("DEN") = dCostesRow.Item("DEN")
                                                            dNewCosteRow.Item("VALOR") = dCostesRow.Item("VALOR")
                                                            dNewCosteRow.Item("PLANIFICADO") = dCostesRow.Item("PLANIFICADO")

                                                            'SACANDO EL IMPORTE
                                                            If dCostesRow.Item("OPERACION") = 0 Then
                                                                'SUMO
                                                                dNewCosteRow.Item("IMPORTE") = dCostesRow.Item("VALOR")
                                                                dNewCosteRow.Item("OPERACION") = "-" 'para el xml

                                                            ElseIf dCostesRow.Item("OPERACION") = 1 Then
                                                                '% DEL IMPORTE
                                                                dNewCosteRow.Item("IMPORTE") = dCostesRow.Item("VALOR") / 100 * dRow.Item("PREC_UP") * dRow.Item("CANT_PED")
                                                                dNewCosteRow.Item("OPERACION") = "-%" 'para el xml
                                                            End If
                                                            'dNewCosteRow.Item("OPERACION") = dCostesRow.Item("OPERACION")

                                                            Try

                                                                ds.Tables("DESCUENTOS").Rows.Add(dNewCosteRow)
                                                                dtDescuentos = ds.Tables("DESCUENTOS").Copy
                                                                dtDescuentos.TableName = numDataset
                                                                dtDescuentos.TableName = "DESCUENTO" & numDataset
                                                                dsEscribir.Tables.Add(dtDescuentos)
                                                                ds.Tables("DESCUENTOS").Clear()
                                                                numDataset = numDataset + 1

                                                            Catch ex As Exception
                                                                GrabarError("Al añadir la tabla descuentos en el dataset a escribir", ex.Message)
                                                            End Try
                                                        End If
                                                    End If
                                                    counter = counter + 1
                                                Else
                                                    Exit While
                                                End If

                                            End While
                                        End If
                                    End If 'validar costes y descuentos vacio
                                End If


                                'FIN CARGAR DESCUENTOS DE CADA LINEA CON RECEPCIONES
                                For Each dImpuestoRow In DsTrasCarga.Tables("IMPUESTOS_PROV_ART").Rows
                                    If dImpuestoRow.item("PROVE") = dFacturasRow.Item("PROVE") And sArt_Init = dImpuestoRow.item("ART") AndAlso ((dImpuestoRow.item("CONCEPTO") = concepto) Or (dImpuestoRow.item("CONCEPTO") = iConceptoImpuesto)) Then

                                        dNewImpuestoRow = ds.Tables("IMPUESTOS").NewRow
                                        dNewImpuestoRow.Item("LINEA") = numeroLinea 'cambios xml
                                        dNewImpuestoRow.Item("COD") = dImpuestoRow.Item("COD")
                                        dNewImpuestoRow.Item("DEN") = dImpuestoRow.Item("DEN")
                                        dNewImpuestoRow.Item("VALOR") = dImpuestoRow.Item("VALOR")
                                        dNewImpuestoRow.Item("IMPORTE") = dRow.Item("PREC_UP") * dRow.Item("CANT") * dImpuestoRow.Item("VALOR") / 100 '' TENGO QUE CALCULARLO CREO QUE ES dRow.Item("PREC_UP") * dRow.Item("CANT_PED")*VALOR /100
                                        dNewImpuestoRow.Item("RETENIDO") = dImpuestoRow.Item("RETENIDO")
                                        dNewImpuestoRow.Item("COMENT") = DBNull.Value
                                        dNewImpuestoRow.Item("GRP_COMP") = dImpuestoRow.Item("GRP_COMP") 'ANEXO

                                        Try
                                            ds.Tables("IMPUESTOS").Rows.Add(dNewImpuestoRow)
                                            dtImpuestos = ds.Tables("IMPUESTOS").Copy
                                            dtImpuestos.TableName = numDataset
                                            dtImpuestos.TableName = "IMPUESTOS" & numDataset
                                            dsEscribir.Tables.Add(dtImpuestos)
                                            If dsImpuesto.Tables.Count = 0 Then
                                                dsImpuesto.Tables.Add(dtImpuestos.Copy)
                                                dtImpuestos.Dispose()
                                            Else

                                                Dim existeImpuesto As Integer
                                                existeImpuesto = 0
                                                For w As Integer = 0 To dsImpuesto.Tables(0).Rows.Count - 1
                                                    If dsImpuesto.Tables(0).Rows(w).Item("COD").Equals(DBNullToStr(dtImpuestos.Rows(0).Item("COD"))) Then
                                                        existeImpuesto = 1
                                                        Exit For
                                                    End If
                                                Next
                                                If existeImpuesto = 0 Then
                                                    dsImpuesto.Tables(0).ImportRow(dtImpuestos.Rows(0))
                                                Else
                                                    existeImpuesto = 0
                                                End If
                                            End If

                                            If dtPosTipoImpuesto Is Nothing Then

                                                dtPosTipoImpuesto = dsPosTipoImpuesto.Tables.Add("IMPUESTOS")

                                                Try
                                                    dtPosTipoImpuesto.Columns.Add("POSICION_LINEA", System.Type.GetType("System.Int32"))
                                                    dtPosTipoImpuesto.Columns.Add("COD", System.Type.GetType("System.String"))
                                                    dtPosTipoImpuesto.Columns.Add("LINEA", System.Type.GetType("System.Int32"))
                                                Catch ex As Exception
                                                    GrabarError("Al crear la tabla impuestos " & DBNullToInteger(empresa), ex.Message)
                                                End Try
                                            End If

                                            dnewPosTipoImpuesto = dsPosTipoImpuesto.Tables("IMPUESTOS").NewRow
                                            dnewPosTipoImpuesto.Item("POSICION_LINEA") = numPosLinea
                                            dnewPosTipoImpuesto.Item("COD") = dtImpuestos.Rows(0).Item("COD")
                                            dnewPosTipoImpuesto.Item("LINEA") = dRow.Item("ID1")

                                            dsPosTipoImpuesto.Tables(0).Rows.Add(dnewPosTipoImpuesto)

                                            'FIN DE AÑADIR EN LA TABLA POSICON COD IMPUESTO Y LINEA

                                            dtImpuestos.Dispose() 'me esta dejando basurilla
                                            ds.Tables("IMPUESTOS").Clear()
                                            numDataset = numDataset + 1

                                        Catch ex As Exception
                                            GrabarError("Al añadir impuestos en el dataset a escribir.", ex.Message)
                                        End Try
                                    End If
                                Next

                            Catch ex As Exception
                                GrabarError("Al añadir líneas con recepción.", ex.Message)
                            End Try
                        End If
                    Next

                    ''FIN CARGA DE LINEAS CON RECEPCIONES
                    ''Fin de Factura- linea- Costes -Descuento

                    ''Cargo el importe total en la factura y la retencion de Garantia   'HAY QUE CAMBIARLO
                    'dsEscribir.Tables(0).Rows(0).Item("IMPORTE") = TotalImporte + TotalCoste - totalDescuento
                    'dsEscribir.Tables(0).Rows(0).Item("RET_GARANTIA") = TotalRetencionGarantia

                    dsAux(0) = dsEscribir
                    dsAux(1) = dsImpuesto ' No me interesa el valor y el coment
                    dsAux(2) = dsPosTipoImpuesto
                    dsAux(3) = dsTipoCentros
                    dsAux(4) = dsPosCentros

                    EscribirXMLPorGastos_InversionesConCentros(dsAux)

                    dsEscribir.Clear()
                    dsEscribir.Dispose()
                    dsEscribir = New DataSet

                    If dsImpuesto IsNot Nothing Then
                        If dsImpuesto.Tables.Count >= 1 Then
                            If dsImpuesto.Tables(0).Rows.Count > 0 Then
                                dsImpuesto.Tables(0).Clear()
                            End If
                        End If
                    End If

                    If dsPosTipoImpuesto IsNot Nothing Then
                        If dsPosTipoImpuesto.Tables.Count >= 1 Then
                            If dsPosTipoImpuesto.Tables(0).Rows.Count > 0 Then
                                dsPosTipoImpuesto.Tables(0).Clear()
                            End If
                        End If
                    End If

                    If dsTipoCentros IsNot Nothing Then
                        If dsTipoCentros.Tables.Count >= 1 Then
                            If dsTipoCentros.Tables(0).Rows.Count > 0 Then
                                dsTipoCentros.Tables(0).Clear()
                            End If
                        End If
                    End If

                    If dsPosCentros IsNot Nothing Then
                        If dsPosCentros.Tables.Count >= 1 Then
                            If dsPosCentros.Tables(0).Rows.Count > 0 Then
                                dsPosCentros.Tables(0).Clear()
                            End If
                        End If
                    End If

                Next
            End If

        Catch ex As Exception
            GrabarError("Al cargar el dataset para la posterior generacion de xml.", ex.Message)
        End Try

    End Sub

    '''<summary>
    ''' Escribe los xml segun los datos pasados y realiza validaciones
    ''' </summary>
    ''' <param name="ds">array dataset que lleva la estructura que tenemos que escribir</param>
    ''' <remarks>Llamada desde:GeneracionFacturas.vb GenerarXMLPorGastos_Inversiones_PorCentroCoste; Tiempo máximo: segundos</remarks>
    Private Sub EscribirXMLPorGastos_InversionesConCentros(ByVal ds() As DataSet)
        Dim dtSolicitudAux As DataTable
        Dim proveedor As String
        Dim tipoMoneda As String
        Dim contador As Integer
        Dim nombreTabla As String
        Dim dtlinea As DataTable
        Dim retGarantia As Double
        Dim lineaAux As Integer

        Dim dtImpuestosAgrupados As DataTable

        If ds(0) IsNot Nothing AndAlso ds(1) IsNot Nothing AndAlso ds(2) IsNot Nothing AndAlso ds(3) IsNot Nothing AndAlso ds(4) IsNot Nothing Then

            'Saco el numero de Lineas de la tabla impuestos 
            If ds(1).Tables.Count >= 1 AndAlso ds(1).Tables(0).Rows.Count >= 1 Then

                If ds(1).Tables(0).Rows.Count = 1 Then
                    '    ' solo tengo un impuesto y tendria que comapararlo con el maximo
                    'Inicio solo Tengo un impuesto pero nose los centros de coste que tengo

                    If ds(3).Tables.Count >= 1 Then
                        dtImpuestosAgrupados = ObtenerGruposCompatibilidad(ds(1).Tables(0))
                        ds(0) = DataSetGrupodeCompatibilidad(ds(0), dtImpuestosAgrupados.Rows(0).Item(0))
                        ' Tiene impuestos y centros
                        If ds(0) IsNot Nothing Then
                            If ds(0).Tables(0) IsNot Nothing Then
                                'INICIO AGRUPAR POR CENTRO DE COSTE
                                proveedor = ds(0).Tables(0).Rows(0).Item("PROVE")
                                tipoMoneda = ds(0).Tables(0).Rows(0).Item("MON")

                                Dim dsCentrosAgrupados(ds(3).Tables(0).Rows.Count - 1) As DataSet
                                Dim dsInicial As DataSet
                                dsInicial = New DataSet

                                For x As Integer = 0 To ds(3).Tables(0).Rows.Count - 1

                                    dsCentrosAgrupados(x) = dsInicial.Clone
                                    dsCentrosAgrupados(x).Tables.Add(ds(0).Tables(0).Copy) 'añado la tabla factura
                                    dsCentrosAgrupados(x).DataSetName = ds(0).DataSetName + "-" + DBNullToStr(ds(3).Tables(0).Rows(x).Item("UON1")) + DBNullToStr(ds(3).Tables(0).Rows(x).Item("UON2")) + DBNullToStr(ds(3).Tables(0).Rows(x).Item("UON3")) + DBNullToStr(ds(3).Tables(0).Rows(x).Item("UON4"))
                                    dsCentrosAgrupados(x).Tables(0).Rows(0).Item("NUM") = vbEmpty
                                    dsCentrosAgrupados(x).Tables(0).Rows(0).Item("IMPORTE") = vbEmpty
                                    dsCentrosAgrupados(x).Tables(0).Rows(0).Item("RET_GARANTIA") = vbEmpty

                                    'recorro todo el dataset y compruebo linea tiene el mismo centro de coste
                                    contador = 0
                                    While contador < ds(0).Tables.Count
                                        Try
                                            nombreTabla = ds(0).Tables(contador).TableName
                                            dtlinea = ds(0).Tables(contador).Copy
                                        Catch ex As Exception
                                            GrabarError("Al cargar datatable con una tabla de un dataset.", ex.Message)
                                        End Try

                                        If nombreTabla.StartsWith("LINEA") Then
                                            'aqui es donde tengo que comprobar que la linea esta en el centro de coste
                                            For m As Integer = 0 To ds(4).Tables(0).Rows.Count - 1
                                                Try
                                                    If contador < ds(0).Tables.Count Then
                                                        nombreTabla = ds(0).Tables(contador).TableName
                                                        dtlinea = ds(0).Tables(contador).Copy
                                                    End If
                                                Catch ex As Exception
                                                    GrabarError("Al querer acceder a una tabla que no existe.", ex.Message)
                                                End Try

                                                If nombreTabla.StartsWith("LINEA") Then
                                                    If dtlinea.Rows(0).Item("LIN_PEDIDO") = ds(4).Tables(0).Rows(m).Item("LINEA") Then

                                                        If ds(3).Tables(0).Rows(x).Item("UON1").Equals(ds(4).Tables(0).Rows(m).Item("UON1")) AndAlso ds(3).Tables(0).Rows(x).Item("UON2").Equals(ds(4).Tables(0).Rows(m).Item("UON2")) AndAlso ds(3).Tables(0).Rows(x).Item("UON3").Equals(ds(4).Tables(0).Rows(m).Item("UON3")) AndAlso ds(3).Tables(0).Rows(x).Item("UON4").Equals(ds(4).Tables(0).Rows(m).Item("UON4")) Then
                                                            'aqui lo añado EN SU DATASET ESPECIFICO Y AÑADO LINEAS HASTA ENCONTRA UNA LINEA
                                                            dtlinea.Rows.Item(0).Item("NUM") = dsCentrosAgrupados(x).Tables(0).Rows(0).Item("NUM") + 1 'añado el num de linea
                                                            dtlinea.Rows.Item(0).Item("LINEA") = dtlinea.Rows.Item(0).Item("NUM") 'añado el num a la  linea
                                                            dsCentrosAgrupados(x).Tables(0).Rows(0).Item("NUM") = dtlinea.Rows.Item(0).Item("NUM")
                                                            dsCentrosAgrupados(x).Tables(0).Rows(0).Item("IMPORTE") = dsCentrosAgrupados(x).Tables(0).Rows(0).Item("IMPORTE") + dtlinea.Rows.Item(0).Item("IMPORTE")
                                                            dsCentrosAgrupados(x).Tables(0).Rows(0).Item("RET_GARANTIA") = dsCentrosAgrupados(x).Tables(0).Rows(0).Item("RET_GARANTIA") + dtlinea.Rows.Item(0).Item("RET_GARANTIA")

                                                            dsCentrosAgrupados(x).Tables.Add(dtlinea)

                                                            'compara hasta llegar a otra linea añadiendo Costes,Descuentos e impuestos a la linea

                                                            Try
                                                                contador = contador + 1
                                                                If contador >= ds(0).Tables.Count Then
                                                                    Exit While 'Evitar que busque un rango superior
                                                                End If
                                                                nombreTabla = ds(0).Tables(contador).TableName

                                                                dtlinea = ds(0).Tables(contador).Copy
                                                            Catch ex As Exception
                                                                GrabarError("Al querer obtener una tabla que no existe.", ex.Message)
                                                            End Try

                                                            While contador < ds(0).Tables.Count
                                                                If nombreTabla.StartsWith("LINEA") Then
                                                                    Exit While
                                                                ElseIf nombreTabla.StartsWith("COSTE") Then
                                                                    dtlinea = ds(0).Tables(contador).Copy
                                                                    dsCentrosAgrupados(x).Tables.Add(dtlinea.Copy)
                                                                    dsCentrosAgrupados(x).Tables(0).Rows(0).Item("IMPORTE") = dsCentrosAgrupados(x).Tables(0).Rows(0).Item("IMPORTE") + dtlinea.Rows.Item(0).Item("IMPORTE")
                                                                    'AÑADIENDO COSTES E IMPUESTOS AL IMPORTE
                                                                ElseIf nombreTabla.StartsWith("DESCUENTO") Then
                                                                    dtlinea = ds(0).Tables(contador).Copy
                                                                    dsCentrosAgrupados(x).Tables.Add(dtlinea.Copy)
                                                                    dsCentrosAgrupados(x).Tables(0).Rows(0).Item("IMPORTE") = dsCentrosAgrupados(x).Tables(0).Rows(0).Item("IMPORTE") - dtlinea.Rows.Item(0).Item("IMPORTE")
                                                                    'RESTO LOS DESCUENTOS AL IMPORTE
                                                                ElseIf nombreTabla.StartsWith("IMPUESTOS") AndAlso ds(0).Tables(contador).Rows(0).Item("GRP_COMP") = dtImpuestosAgrupados.Rows(0).Item(0) Then
                                                                    dtlinea = ds(0).Tables(contador).Copy
                                                                    dsCentrosAgrupados(x).Tables.Add(dtlinea.Copy)
                                                                    dsCentrosAgrupados(x).Tables(0).Rows(0).Item("IMPORTE") = dsCentrosAgrupados(x).Tables(0).Rows(0).Item("IMPORTE") + dtlinea.Rows.Item(0).Item("IMPORTE")
                                                                    'AÑADIENDO COSTES E IMPUESTOS 

                                                                End If

                                                                dtlinea = ds(0).Tables(contador).Copy
                                                                contador = contador + 1
                                                                If contador >= ds(0).Tables.Count Then
                                                                    Exit While 'Evitar que busque un rango superior
                                                                End If
                                                                nombreTabla = ds(0).Tables(contador).TableName

                                                            End While

                                                        End If

                                                    End If

                                                End If

                                            Next
                                            'COMO LA LINEA NO ES no añado costes,descuentos e impuestos
                                            If contador + 1 >= ds(0).Tables.Count Then
                                                'TABLA (0) = COUNT 1 POR ESO EL +1
                                                Exit While

                                            Else
                                                Try
                                                    contador = contador + 1
                                                    nombreTabla = ds(0).Tables(contador).TableName
                                                Catch ex As Exception
                                                    GrabarError("Al obtener el nombre de una tabla que no existe. Contador: " & contador, ex.Message)
                                                End Try

                                                While contador + 1 < ds(0).Tables.Count
                                                    If nombreTabla.StartsWith("LINEA") Then
                                                        Exit While
                                                    End If
                                                    Try
                                                        contador = contador + 1
                                                        nombreTabla = ds(0).Tables(contador).TableName
                                                    Catch ex As Exception
                                                        GrabarError("Al obtener el nombre de una tabla que no existe. Contador: " & contador, ex.Message)
                                                    End Try
                                                End While
                                            End If
                                        End If

                                        If Not (nombreTabla.StartsWith("LINEA")) Then
                                            contador = contador + 1
                                        End If
                                    End While
                                Next
                                'Recorro los centros de coste y escribo los datasets

                                For Z As Integer = 0 To dsCentrosAgrupados.Length - 1

                                    If dsCentrosAgrupados(Z).Tables(0).Rows(0).Item("NUM") < numeroMax Then
                                        dsCentrosAgrupados(Z).Tables(0).Rows(0).Item("NUM") = vbEmpty
                                        If dsCentrosAgrupados(Z).Tables.Count >= 2 Then
                                            'La tabla 1 tiene La Factura
                                            'INICO ESCRITURA XML

                                            dtSolicitudAux = GenerarDTSolicitud(tipoMoneda)
                                            dsCentrosAgrupados(Z).Tables.Add(dtSolicitudAux)
                                            dsCentrosAgrupados(Z).Tables(0).Rows(0).Item("INSTANCIA") = instancia 'añado la instancia a la factura
                                            dsCentrosAgrupados(Z) = ComprobarValorLinea(dsCentrosAgrupados(Z))
                                            dsCentrosAgrupados(Z).DataSetName = "NewDataSet" 'por formato para su tratamiento este debe ser el nombre del dataSet
                                            dsCentrosAgrupados(Z).WriteXml(ConfigurationManager.AppSettings("rutaXML") & "\" & usuario & "#" & instancia & "#" & idbloque & "#" & "F" & ".xml") ', XmlWriteMode.WriteSchema no me cambia todos los tags
                                            NombresTags = NombreTablas(dsCentrosAgrupados(Z))
                                            RemplazarNumerosTags(NombresTags, ConfigurationManager.AppSettings("rutaXML") & "\" & usuario & "#" & instancia & "#" & idbloque & "#" & "F" & ".xml") 'quitando tags
                                            'FIN ESCRITURA XML

                                        End If
                                    Else
                                        dsCentrosAgrupados(Z).Tables(0).Rows(0).Item("NUM") = vbEmpty
                                        GenerarXMLConLineaMaxima(dsCentrosAgrupados(Z), numeroMax)
                                    End If
                                Next

                                'FIN DE ESCRITURA DE LOS DATASETS SIN IMPUESTOS

                            End If

                        End If

                    End If 'GRP_COMP

                    'FIN solo Tengo un impuesto pero nose los centros de coste que tengo
                Else

                    '    Tengo mas de un impuesto
                    '    'INICIO CARGA DEL RESTO DE LINEAS

                    Dim numlineas As Integer = 0

                    Try

                        'Tengo mas de un impuesto recorro para saber los centros de coste

                        If ds(3).Tables.Count >= 1 Then
                            'Dim dtImpuestosAgrupados As DataTable
                            dtImpuestosAgrupados = ObtenerGruposCompatibilidad(ds(1).Tables(0))
                            ds(0) = DataSetGrupodeCompatibilidad(ds(0), dtImpuestosAgrupados.Rows(0).Item(0))
                            'No tiene impuestos pero si centros
                            If ds(0) IsNot Nothing Then
                                If ds(0).Tables(0) IsNot Nothing Then
                                    'INICIO AGRUPAR POR CENTRO DE COSTE
                                    proveedor = ds(0).Tables(0).Rows(0).Item("PROVE")
                                    tipoMoneda = ds(0).Tables(0).Rows(0).Item("MON")

                                    Dim dsCentrosAgrupados(ds(3).Tables(0).Rows.Count - 1) As DataSet
                                    Dim dsInicial As DataSet
                                    dsInicial = New DataSet

                                    For x As Integer = 0 To ds(3).Tables(0).Rows.Count - 1

                                        dsCentrosAgrupados(x) = dsInicial.Clone
                                        dsCentrosAgrupados(x).Tables.Add(ds(0).Tables(0).Copy) 'añado la tabla factura
                                        dsCentrosAgrupados(x).DataSetName = ds(0).DataSetName + "-" + DBNullToStr(ds(3).Tables(0).Rows(x).Item("UON1")) + DBNullToStr(ds(3).Tables(0).Rows(x).Item("UON2")) + DBNullToStr(ds(3).Tables(0).Rows(x).Item("UON3")) + DBNullToStr(ds(3).Tables(0).Rows(x).Item("UON4"))
                                        dsCentrosAgrupados(x).Tables(0).Rows(0).Item("NUM") = vbEmpty
                                        dsCentrosAgrupados(x).Tables(0).Rows(0).Item("IMPORTE") = vbEmpty
                                        dsCentrosAgrupados(x).Tables(0).Rows(0).Item("RET_GARANTIA") = vbEmpty

                                        'recorro todo el dataset y compruebo linea tiene el mismo centro de coste
                                        contador = 0
                                        While contador < ds(0).Tables.Count
                                            Try
                                                nombreTabla = ds(0).Tables(contador).TableName
                                                dtlinea = ds(0).Tables(contador).Copy
                                            Catch ex As Exception
                                                GrabarError("Al cargar datatable con una tabla de un dataset. Contador: " & contador, ex.Message)
                                            End Try

                                            If nombreTabla.StartsWith("LINEA") Then
                                                'aqui es donde tengo que comprobar que la linea esta en el centro de coste
                                                For m As Integer = 0 To ds(4).Tables(0).Rows.Count - 1
                                                    Try
                                                        If contador < ds(0).Tables.Count Then
                                                            nombreTabla = ds(0).Tables(contador).TableName
                                                            dtlinea = ds(0).Tables(contador).Copy
                                                        End If
                                                    Catch ex As Exception
                                                        GrabarError("Al acceder a una tabla que no existe. Contador: " & contador, ex.Message)
                                                    End Try

                                                    If nombreTabla.StartsWith("LINEA") Then
                                                        If dtlinea.Rows(0).Item("LIN_PEDIDO") = ds(4).Tables(0).Rows(m).Item("LINEA") Then

                                                            If ds(3).Tables(0).Rows(x).Item("UON1").Equals(ds(4).Tables(0).Rows(m).Item("UON1")) AndAlso ds(3).Tables(0).Rows(x).Item("UON2").Equals(ds(4).Tables(0).Rows(m).Item("UON2")) AndAlso ds(3).Tables(0).Rows(x).Item("UON3").Equals(ds(4).Tables(0).Rows(m).Item("UON3")) AndAlso ds(3).Tables(0).Rows(x).Item("UON4").Equals(ds(4).Tables(0).Rows(m).Item("UON4")) Then
                                                                'aqui lo añado EN SU DATASET ESPECIFICO Y AÑADO LINEAS HASTA ENCONTRA UNA LINEA
                                                                dtlinea.Rows.Item(0).Item("NUM") = dsCentrosAgrupados(x).Tables(0).Rows(0).Item("NUM") + 1 'añado el num de linea
                                                                dtlinea.Rows.Item(0).Item("LINEA") = dtlinea.Rows.Item(0).Item("NUM") 'añado el num a la  linea
                                                                dsCentrosAgrupados(x).Tables(0).Rows(0).Item("NUM") = dtlinea.Rows.Item(0).Item("NUM")
                                                                dsCentrosAgrupados(x).Tables(0).Rows(0).Item("IMPORTE") = dsCentrosAgrupados(x).Tables(0).Rows(0).Item("IMPORTE") + dtlinea.Rows.Item(0).Item("IMPORTE")
                                                                dsCentrosAgrupados(x).Tables(0).Rows(0).Item("RET_GARANTIA") = dsCentrosAgrupados(x).Tables(0).Rows(0).Item("RET_GARANTIA") + dtlinea.Rows.Item(0).Item("RET_GARANTIA")

                                                                dsCentrosAgrupados(x).Tables.Add(dtlinea)

                                                                'compara hasta llegar a otra linea añadiendo Costes,Descuentos e impuestos a la linea

                                                                Try
                                                                    contador = contador + 1
                                                                    If contador >= ds(0).Tables.Count Then
                                                                        Exit While 'Evitar que busque un rango superior
                                                                    End If
                                                                    nombreTabla = ds(0).Tables(contador).TableName

                                                                    dtlinea = ds(0).Tables(contador).Copy
                                                                Catch ex As Exception
                                                                    GrabarError("Al obtener una tabla que no existe. Contador: " & contador, ex.Message)
                                                                End Try

                                                                While contador < ds(0).Tables.Count
                                                                    If nombreTabla.StartsWith("LINEA") Then
                                                                        Exit While
                                                                    ElseIf nombreTabla.StartsWith("COSTE") Then
                                                                        dtlinea = ds(0).Tables(contador).Copy
                                                                        dsCentrosAgrupados(x).Tables.Add(dtlinea.Copy)
                                                                        dsCentrosAgrupados(x).Tables(0).Rows(0).Item("IMPORTE") = dsCentrosAgrupados(x).Tables(0).Rows(0).Item("IMPORTE") + dtlinea.Rows.Item(0).Item("IMPORTE")
                                                                        'AÑADIENDO COSTES E IMPUESTOS AL IMPORTE
                                                                    ElseIf nombreTabla.StartsWith("DESCUENTO") Then
                                                                        dtlinea = ds(0).Tables(contador).Copy
                                                                        dsCentrosAgrupados(x).Tables.Add(dtlinea.Copy)
                                                                        dsCentrosAgrupados(x).Tables(0).Rows(0).Item("IMPORTE") = dsCentrosAgrupados(x).Tables(0).Rows(0).Item("IMPORTE") - dtlinea.Rows.Item(0).Item("IMPORTE")
                                                                        'RESTO LOS DESCUENTOS AL IMPORTE
                                                                    ElseIf nombreTabla.StartsWith("IMPUESTOS") AndAlso dtImpuestosAgrupados.Rows(0).Item(0) = ds(0).Tables(contador).Rows(0).Item("GRP_COMP") Then
                                                                        dtlinea = ds(0).Tables(contador).Copy
                                                                        dsCentrosAgrupados(x).Tables.Add(dtlinea.Copy)
                                                                        dsCentrosAgrupados(x).Tables(0).Rows(0).Item("IMPORTE") = dsCentrosAgrupados(x).Tables(0).Rows(0).Item("IMPORTE") + dtlinea.Rows.Item(0).Item("IMPORTE")
                                                                        'AÑADIENDO COSTES E IMPUESTOS 
                                                                    End If

                                                                    dtlinea = ds(0).Tables(contador).Copy
                                                                    contador = contador + 1
                                                                    If contador >= ds(0).Tables.Count Then
                                                                        Exit While 'Evitar que busque un rango superior
                                                                    End If
                                                                    nombreTabla = ds(0).Tables(contador).TableName

                                                                End While

                                                            End If

                                                        End If

                                                    End If

                                                Next
                                                'COMO LA LINEA NO ES no añado costes,descuentos e impuestos
                                                If contador + 1 >= ds(0).Tables.Count Then
                                                    'TABLA (0) = COUNT 1 POR ESO EL +1
                                                    Exit While

                                                Else
                                                    Try
                                                        contador = contador + 1
                                                        nombreTabla = ds(0).Tables(contador).TableName
                                                    Catch ex As Exception
                                                        GrabarError("Al obtener el nombre de una tabla que no existe. Contador: " & contador, ex.Message)
                                                    End Try

                                                    While contador + 1 < ds(0).Tables.Count
                                                        If nombreTabla.StartsWith("LINEA") Then
                                                            Exit While
                                                        End If
                                                        Try
                                                            contador = contador + 1
                                                            nombreTabla = ds(0).Tables(contador).TableName
                                                        Catch ex As Exception
                                                            GrabarError("Al obtener el nombre de una tabla que no existe. Contador: " & contador, ex.Message)
                                                        End Try
                                                    End While
                                                End If
                                            End If

                                            If Not (nombreTabla.StartsWith("LINEA")) Then
                                                contador = contador + 1
                                            End If
                                        End While
                                    Next
                                    'Recorro los centros de coste y escribo los datasets

                                    For Z As Integer = 0 To dsCentrosAgrupados.Length - 1

                                        If dsCentrosAgrupados(Z).Tables(0).Rows(0).Item("NUM") < numeroMax Then
                                            dsCentrosAgrupados(Z).Tables(0).Rows(0).Item("NUM") = vbEmpty
                                            If dsCentrosAgrupados(Z).Tables.Count >= 2 Then
                                                'La tabla 1 tiene La Factura
                                                'INICO ESCRITURA XML

                                                dtSolicitudAux = GenerarDTSolicitud(tipoMoneda)
                                                dsCentrosAgrupados(Z).Tables.Add(dtSolicitudAux)
                                                dsCentrosAgrupados(Z).Tables(0).Rows(0).Item("INSTANCIA") = instancia 'añado la instancia a la factura
                                                dsCentrosAgrupados(Z) = ComprobarValorLinea(dsCentrosAgrupados(Z))
                                                dsCentrosAgrupados(Z).DataSetName = "NewDataSet" 'por formato para su tratamiento este debe ser el nombre del dataSet
                                                dsCentrosAgrupados(Z).WriteXml(ConfigurationManager.AppSettings("rutaXML") & "\" & usuario & "#" & instancia & "#" & idbloque & "#" & "F" & ".xml") ', XmlWriteMode.WriteSchema no me cambia todos los tags
                                                NombresTags = NombreTablas(dsCentrosAgrupados(Z))
                                                RemplazarNumerosTags(NombresTags, ConfigurationManager.AppSettings("rutaXML") & "\" & usuario & "#" & instancia & "#" & idbloque & "#" & "F" & ".xml") 'quitando tags
                                                'FIN ESCRITURA XML
                                            End If
                                        Else
                                            dsCentrosAgrupados(Z).Tables(0).Rows(0).Item("NUM") = vbEmpty
                                            GenerarXMLConLineaMaxima(dsCentrosAgrupados(Z), numeroMax)
                                        End If
                                    Next
                                    'FIN DE ESCRITURA DE LOS DATASETS SIN IMPUESTOS
                                End If
                            End If
                        End If 'GRP_COMP

                        If ds(3).Tables.Count >= 1 Then
                            dtImpuestosAgrupados = ObtenerGruposCompatibilidad(ds(1).Tables(0))
                            ds(0) = DataSetGrupodeCompatibilidad(ds(0), dtImpuestosAgrupados.Rows(0).Item(0))
                            dtImpuestosAgrupados = ObtenerGruposCompatibilidad(ds(1).Tables(0)) 'COMPRUEBO SI TENGO VARIOS GRUPOS DE COMPATIBILIDAD

                            If dtImpuestosAgrupados.Rows.Count <= 1 Then

                                While contador < ds(0).Tables.Count
                                    nombreTabla = ds(0).Tables(contador).TableName
                                    dtlinea = ds(0).Tables(contador).Copy

                                    If nombreTabla.StartsWith("LINEA") Then

                                        dtlinea.Rows.Item(0).Item("NUM") = ds(0).Tables(0).Rows(0).Item("NUM") + 1
                                        dtlinea.Rows.Item(0).Item("LINEA") = dtlinea.Rows.Item(0).Item("NUM") 'AÑADIENDO NUM A LINEA
                                        ds(0).Tables(0).Rows(0).Item("NUM") = dtlinea.Rows.Item(0).Item("NUM")
                                        retGarantia = retGarantia + dtlinea.Rows.Item(0).Item("RET_GARANTIA")
                                        lineaAux = dtlinea.Rows.Item(0).Item("LIN_PEDIDO")
                                        'añadiendo al importe y la retencion de garantia a la factura
                                        ds(0).Tables(0).Rows(0).Item("IMPORTE") = ds(0).Tables(0).Rows(0).Item("IMPORTE") + dtlinea.Rows.Item(0).Item("IMPORTE")
                                        ds(0).Tables(0).Rows(0).Item("RET_GARANTIA") = ds(0).Tables(0).Rows(0).Item("RET_GARANTIA") + dtlinea.Rows.Item(0).Item("RET_GARANTIA")
                                        ds(0).Tables.Add(dtlinea.Copy)

                                        Try
                                            contador = contador + 1
                                            If contador >= ds(0).Tables.Count Then
                                                Exit While 'Evitar que busque un rango superior
                                            End If
                                            nombreTabla = ds(0).Tables(contador).TableName
                                        Catch ex As Exception
                                            GrabarError("Al obtener el nombre de una tabla que no existe. Contador: " & contador, ex.Message)
                                        End Try
                                        dtlinea = ds(0).Tables(contador).Copy

                                        While contador < ds(0).Tables.Count
                                            If nombreTabla.StartsWith("LINEA") Then
                                                Exit While
                                            ElseIf nombreTabla.StartsWith("COSTE") Then
                                                dtlinea = ds(0).Tables(contador).Copy
                                                ds(0).Tables.Add(dtlinea.Copy)
                                                ds(0).Tables(0).Rows(0).Item("IMPORTE") = ds(0).Tables(0).Rows(0).Item("IMPORTE") + dtlinea.Rows.Item(0).Item("IMPORTE")
                                                'AÑADIENDO COSTES E IMPUESTOS AL IMPORTE
                                            ElseIf nombreTabla.StartsWith("DESCUENTO") Then
                                                dtlinea = ds(0).Tables(contador).Copy
                                                ds(0).Tables.Add(dtlinea.Copy)
                                                ds(0).Tables(0).Rows(0).Item("IMPORTE") = ds(0).Tables(0).Rows(0).Item("IMPORTE") - dtlinea.Rows.Item(0).Item("IMPORTE")
                                                'RESTO LOS DESCUENTOS AL IMPORTE
                                            ElseIf nombreTabla.StartsWith("IMPUESTOS") AndAlso dtImpuestosAgrupados.Rows(0).Item(0) = ds(0).Tables(contador).Rows(0).Item("GPR_COMP") Then
                                                dtlinea = ds(0).Tables(contador).Copy
                                                ds(0).Tables.Add(dtlinea.Copy)
                                                ds(0).Tables(0).Rows(0).Item("IMPORTE") = ds(0).Tables(0).Rows(0).Item("IMPORTE") + dtlinea.Rows.Item(0).Item("IMPORTE")
                                                'AÑADIENDO COSTES E IMPUESTOS 
                                            End If

                                            dtlinea = ds(0).Tables(contador).Copy
                                            contador = contador + 1
                                            If contador >= ds(0).Tables.Count Then
                                                Exit While 'Evitar que busque un rango superior
                                            End If
                                            nombreTabla = ds(0).Tables(contador).TableName
                                        End While
                                    Else
                                        If contador + 1 >= ds(0).Tables.Count Then
                                            'TABLA (0) = COUNT 1 POR ESO EL +1
                                            Exit While
                                        Else
                                            contador = contador + 1
                                        End If
                                    End If

                                    'Else
                                    'COMO LA LINEA NO ES no añado costes,descuentos e impuestos
                                    If contador + 1 >= ds(0).Tables.Count Then
                                        'TABLA (0) = COUNT 1 POR ESO EL +1
                                        Exit While
                                    Else
                                        Try
                                            contador = contador + 1
                                            nombreTabla = ds(0).Tables(contador).TableName
                                        Catch ex As Exception
                                            GrabarError("Al obtener el nombre de una tabla que no existe. Contador: " & contador, ex.Message)
                                        End Try

                                        While contador + 1 < ds(0).Tables.Count
                                            If nombreTabla.StartsWith("LINEA") Then
                                                Exit While
                                            End If
                                            Try
                                                contador = contador + 1
                                                nombreTabla = ds(0).Tables(contador).TableName
                                            Catch ex As Exception
                                                GrabarError("Al obtener el nombre de una tabla que no existe. Contador: " & contador, ex.Message)
                                            End Try
                                        End While
                                    End If
                                End While
                            End If
                        End If

                    Catch ex As Exception
                        GrabarError("Al cargar el resto de lineas.", ex.Message)
                    End Try

                    'FIN CARGA RESTO DE LINEAS

                    'INICIO COMPROBAR TAMAÑO MAXIMO

                    Try
                        If ds(0) IsNot Nothing Then
                            If ds(0).Tables(0) IsNot Nothing Then
                                'INICIO AGRUPAR POR CENTRO DE COSTE
                                proveedor = ds(0).Tables(0).Rows(0).Item("PROVE")
                                tipoMoneda = ds(0).Tables(0).Rows(0).Item("MON")

                                Dim dsCentrosAgrupados(ds(3).Tables(0).Rows.Count - 1) As DataSet
                                Dim dsInicial As DataSet
                                dsInicial = New DataSet

                                For x As Integer = 0 To ds(3).Tables(0).Rows.Count - 1

                                    dsCentrosAgrupados(x) = dsInicial.Clone
                                    dsCentrosAgrupados(x).Tables.Add(ds(0).Tables(0).Copy) 'añado la tabla factura
                                    dsCentrosAgrupados(x).DataSetName = ds(0).DataSetName + "-" + DBNullToStr(ds(3).Tables(0).Rows(x).Item("UON1")) + DBNullToStr(ds(3).Tables(0).Rows(x).Item("UON2")) + DBNullToStr(ds(3).Tables(0).Rows(x).Item("UON3")) + DBNullToStr(ds(3).Tables(0).Rows(x).Item("UON4"))
                                    dsCentrosAgrupados(x).Tables(0).Rows(0).Item("NUM") = vbEmpty
                                    dsCentrosAgrupados(x).Tables(0).Rows(0).Item("IMPORTE") = vbEmpty
                                    dsCentrosAgrupados(x).Tables(0).Rows(0).Item("RET_GARANTIA") = vbEmpty

                                    'recorro todo el dataset y compruebo linea tiene el mismo centro de coste
                                    contador = 0
                                    While contador < ds(0).Tables.Count
                                        Try
                                            nombreTabla = ds(0).Tables(contador).TableName
                                            dtlinea = ds(0).Tables(contador).Copy
                                        Catch ex As Exception
                                            GrabarError("Al cargar datatable con una tabla de un dataset. Contador: " & contador, ex.Message)
                                        End Try

                                        If nombreTabla.StartsWith("LINEA") Then
                                            'aqui es donde tengo que comprobar que la linea esta en el centro de coste
                                            For m As Integer = 0 To ds(4).Tables(0).Rows.Count - 1
                                                Try
                                                    If contador < ds(0).Tables.Count Then
                                                        nombreTabla = ds(0).Tables(contador).TableName
                                                        dtlinea = ds(0).Tables(contador).Copy
                                                    End If
                                                Catch ex As Exception
                                                    GrabarError("Al acceder a una tabla que no existe. Contador: " & contador, ex.Message)
                                                End Try

                                                If nombreTabla.StartsWith("LINEA") Then
                                                    If dtlinea.Rows(0).Item("LIN_PEDIDO") = ds(4).Tables(0).Rows(m).Item("LINEA") Then

                                                        If ds(3).Tables(0).Rows(x).Item("UON1").Equals(ds(4).Tables(0).Rows(m).Item("UON1")) AndAlso ds(3).Tables(0).Rows(x).Item("UON2").Equals(ds(4).Tables(0).Rows(m).Item("UON2")) AndAlso ds(3).Tables(0).Rows(x).Item("UON3").Equals(ds(4).Tables(0).Rows(m).Item("UON3")) AndAlso ds(3).Tables(0).Rows(x).Item("UON4").Equals(ds(4).Tables(0).Rows(m).Item("UON4")) Then
                                                            'aqui lo añado EN SU DATASET ESPECIFICO Y AÑADO LINEAS HASTA ENCONTRA UNA LINEA
                                                            dtlinea.Rows.Item(0).Item("NUM") = dsCentrosAgrupados(x).Tables(0).Rows(0).Item("NUM") + 1 'añado el num de linea
                                                            dtlinea.Rows.Item(0).Item("LINEA") = dtlinea.Rows.Item(0).Item("NUM") 'añado el num a la  linea
                                                            dsCentrosAgrupados(x).Tables(0).Rows(0).Item("NUM") = dtlinea.Rows.Item(0).Item("NUM")
                                                            dsCentrosAgrupados(x).Tables(0).Rows(0).Item("IMPORTE") = dsCentrosAgrupados(x).Tables(0).Rows(0).Item("IMPORTE") + dtlinea.Rows.Item(0).Item("IMPORTE")
                                                            dsCentrosAgrupados(x).Tables(0).Rows(0).Item("RET_GARANTIA") = dsCentrosAgrupados(x).Tables(0).Rows(0).Item("RET_GARANTIA") + dtlinea.Rows.Item(0).Item("RET_GARANTIA")

                                                            dsCentrosAgrupados(x).Tables.Add(dtlinea)

                                                            'compara hasta llegar a otra linea añadiendo Costes,Descuentos e impuestos a la linea

                                                            Try
                                                                contador = contador + 1
                                                                If contador >= ds(0).Tables.Count Then
                                                                    Exit While 'Evitar que busque un rango superior
                                                                End If
                                                                nombreTabla = ds(0).Tables(contador).TableName

                                                                dtlinea = ds(0).Tables(contador).Copy
                                                            Catch ex As Exception
                                                                GrabarError("Al acceder a una tabla que no existe. Contador: " & contador, ex.Message)
                                                            End Try

                                                            While contador < ds(0).Tables.Count
                                                                If nombreTabla.StartsWith("LINEA") Then
                                                                    Exit While
                                                                ElseIf nombreTabla.StartsWith("COSTE") Then
                                                                    dtlinea = ds(0).Tables(contador).Copy
                                                                    dsCentrosAgrupados(x).Tables.Add(dtlinea.Copy)
                                                                    dsCentrosAgrupados(x).Tables(0).Rows(0).Item("IMPORTE") = dsCentrosAgrupados(x).Tables(0).Rows(0).Item("IMPORTE") + dtlinea.Rows.Item(0).Item("IMPORTE")
                                                                    'AÑADIENDO COSTES E IMPUESTOS AL IMPORTE
                                                                ElseIf nombreTabla.StartsWith("DESCUENTO") Then
                                                                    dtlinea = ds(0).Tables(contador).Copy
                                                                    dsCentrosAgrupados(x).Tables.Add(dtlinea.Copy)
                                                                    dsCentrosAgrupados(x).Tables(0).Rows(0).Item("IMPORTE") = dsCentrosAgrupados(x).Tables(0).Rows(0).Item("IMPORTE") - dtlinea.Rows.Item(0).Item("IMPORTE")
                                                                    'RESTO LOS DESCUENTOS AL IMPORTE
                                                                ElseIf nombreTabla.StartsWith("IMPUESTOS") AndAlso dtImpuestosAgrupados.Rows(0).Item(0) = ds(0).Tables(contador).Rows(0).Item("GPR_COMP") Then
                                                                    dtlinea = ds(0).Tables(contador).Copy
                                                                    dsCentrosAgrupados(x).Tables.Add(dtlinea.Copy)
                                                                    dsCentrosAgrupados(x).Tables(0).Rows(0).Item("IMPORTE") = dsCentrosAgrupados(x).Tables(0).Rows(0).Item("IMPORTE") + dtlinea.Rows.Item(0).Item("IMPORTE")
                                                                    'AÑADIENDO COSTES E IMPUESTOS 
                                                                End If

                                                                dtlinea = ds(0).Tables(contador).Copy
                                                                contador = contador + 1
                                                                If contador >= ds(0).Tables.Count Then
                                                                    Exit While 'Evitar que busque un rango superior
                                                                End If
                                                                nombreTabla = ds(0).Tables(contador).TableName
                                                            End While
                                                        End If
                                                    End If
                                                End If
                                            Next
                                            'COMO LA LINEA NO ES no añado costes,descuentos e impuestos
                                            If contador + 1 >= ds(0).Tables.Count Then
                                                'TABLA (0) = COUNT 1 POR ESO EL +1
                                                Exit While
                                            Else
                                                Try
                                                    contador = contador + 1
                                                    nombreTabla = ds(0).Tables(contador).TableName
                                                Catch ex As Exception
                                                    GrabarError("Al obtener el nombre de una tabla que no existe. Contador: " & contador, ex.Message)
                                                End Try

                                                While contador + 1 < ds(0).Tables.Count
                                                    If nombreTabla.StartsWith("LINEA") Then
                                                        Exit While
                                                    End If
                                                    Try
                                                        contador = contador + 1
                                                        nombreTabla = ds(0).Tables(contador).TableName
                                                    Catch ex As Exception
                                                        GrabarError("Al obtener el nombre de una tabla que no existe. Contador: " & contador, ex.Message)
                                                    End Try
                                                End While
                                            End If
                                        End If

                                        If Not (nombreTabla.StartsWith("LINEA")) Then
                                            contador = contador + 1
                                        End If
                                    End While
                                Next

                                'FIN AGRUPAR POR CENTRO DE COSTE
                                'LOS RECORRO POR IMPUESTOS

                                For Z As Integer = 0 To dsCentrosAgrupados.Length - 1

                                    If dsCentrosAgrupados(Z).Tables(0).Rows(0).Item("NUM") < numeroMax Then
                                        dsCentrosAgrupados(Z).Tables(0).Rows(0).Item("NUM") = vbEmpty
                                        If dsCentrosAgrupados(Z).Tables.Count >= 2 Then
                                            'La tabla 1 tiene La Factura
                                            'INICO ESCRITURA XML

                                            dtSolicitudAux = GenerarDTSolicitud(tipoMoneda)
                                            dsCentrosAgrupados(Z).Tables.Add(dtSolicitudAux)
                                            dsCentrosAgrupados(Z).Tables(0).Rows(0).Item("INSTANCIA") = instancia 'añado la instancia a la factura
                                            dsCentrosAgrupados(Z) = ComprobarValorLinea(dsCentrosAgrupados(Z))
                                            dsCentrosAgrupados(Z).DataSetName = "NewDataSet" 'por formato para su tratamiento este debe ser el nombre del dataSet
                                            dsCentrosAgrupados(Z).WriteXml(ConfigurationManager.AppSettings("rutaXML") & "\" & usuario & "#" & instancia & "#" & idbloque & "#" & "F" & ".xml")
                                            NombresTags = NombreTablas(dsCentrosAgrupados(Z))
                                            RemplazarNumerosTags(NombresTags, ConfigurationManager.AppSettings("rutaXML") & "\" & usuario & "#" & instancia & "#" & idbloque & "#" & "F" & ".xml") 'quitando tags
                                            'FIN ESCRITURA XML
                                        End If
                                    Else
                                        dsCentrosAgrupados(Z).Tables(0).Rows(0).Item("NUM") = vbEmpty
                                        GenerarXMLConLineaMaxima(dsCentrosAgrupados(Z), numeroMax)
                                    End If
                                Next
                            End If
                        End If

                    Catch ex As Exception
                        GrabarError("Al agrupar los impuestos.", ex.Message)
                    End Try
                End If

            Else 'CUANDO NO TIENE IMPUESTOS

                If ds(3).Tables.Count >= 1 Then
                    'No tiene impuestos pero si centros
                    If ds(0) IsNot Nothing Then
                        If ds(0).Tables(0) IsNot Nothing Then
                            'INICIO AGRUPAR POR CENTRO DE COSTE
                            proveedor = ds(0).Tables(0).Rows(0).Item("PROVE")
                            tipoMoneda = ds(0).Tables(0).Rows(0).Item("MON")

                            Dim dsCentrosAgrupados(ds(3).Tables(0).Rows.Count - 1) As DataSet
                            Dim dsInicial As DataSet
                            dsInicial = New DataSet

                            For x As Integer = 0 To ds(3).Tables(0).Rows.Count - 1

                                dsCentrosAgrupados(x) = dsInicial.Clone
                                dsCentrosAgrupados(x).Tables.Add(ds(0).Tables(0).Copy) 'añado la tabla factura
                                dsCentrosAgrupados(x).DataSetName = ds(0).DataSetName + "-" + DBNullToStr(ds(3).Tables(0).Rows(x).Item("UON1")) + DBNullToStr(ds(3).Tables(0).Rows(x).Item("UON2")) + DBNullToStr(ds(3).Tables(0).Rows(x).Item("UON3")) + DBNullToStr(ds(3).Tables(0).Rows(x).Item("UON4"))
                                dsCentrosAgrupados(x).Tables(0).Rows(0).Item("NUM") = vbEmpty
                                dsCentrosAgrupados(x).Tables(0).Rows(0).Item("IMPORTE") = vbEmpty
                                dsCentrosAgrupados(x).Tables(0).Rows(0).Item("RET_GARANTIA") = vbEmpty

                                'recorro todo el dataset y compruebo linea tiene el mismo centro de coste
                                contador = 0
                                While contador < ds(0).Tables.Count
                                    Try
                                        nombreTabla = ds(0).Tables(contador).TableName
                                        dtlinea = ds(0).Tables(contador).Copy
                                    Catch ex As Exception
                                        GrabarError("Al cargar datatable con una tabla de un dataset. Contador: " & contador, ex.Message)
                                    End Try

                                    If nombreTabla.StartsWith("LINEA") Then
                                        'aqui es donde tengo que comprobar que la linea esta en el centro de coste
                                        For m As Integer = 0 To ds(4).Tables(0).Rows.Count - 1
                                            Try
                                                If contador < ds(0).Tables.Count Then
                                                    nombreTabla = ds(0).Tables(contador).TableName
                                                    dtlinea = ds(0).Tables(contador).Copy
                                                End If
                                            Catch ex As Exception
                                                GrabarError("Al acceder a una tabla que no existe. Contador: " & contador, ex.Message)
                                            End Try

                                            If nombreTabla.StartsWith("LINEA") Then
                                                If dtlinea.Rows(0).Item("LIN_PEDIDO") = ds(4).Tables(0).Rows(m).Item("LINEA") Then

                                                    If ds(3).Tables(0).Rows(x).Item("UON1").Equals(ds(4).Tables(0).Rows(m).Item("UON1")) AndAlso ds(3).Tables(0).Rows(x).Item("UON2").Equals(ds(4).Tables(0).Rows(m).Item("UON2")) AndAlso ds(3).Tables(0).Rows(x).Item("UON3").Equals(ds(4).Tables(0).Rows(m).Item("UON3")) AndAlso ds(3).Tables(0).Rows(x).Item("UON4").Equals(ds(4).Tables(0).Rows(m).Item("UON4")) Then
                                                        'aqui lo añado EN SU DATASET ESPECIFICO Y AÑADO LINEAS HASTA ENCONTRA UNA LINEA
                                                        dtlinea.Rows.Item(0).Item("NUM") = dsCentrosAgrupados(x).Tables(0).Rows(0).Item("NUM") + 1 'añado el num de linea
                                                        dtlinea.Rows.Item(0).Item("LINEA") = dtlinea.Rows.Item(0).Item("NUM") 'añado el num a la  linea
                                                        dsCentrosAgrupados(x).Tables(0).Rows(0).Item("NUM") = dtlinea.Rows.Item(0).Item("NUM")
                                                        dsCentrosAgrupados(x).Tables(0).Rows(0).Item("IMPORTE") = dsCentrosAgrupados(x).Tables(0).Rows(0).Item("IMPORTE") + dtlinea.Rows.Item(0).Item("IMPORTE")
                                                        dsCentrosAgrupados(x).Tables(0).Rows(0).Item("RET_GARANTIA") = dsCentrosAgrupados(x).Tables(0).Rows(0).Item("RET_GARANTIA") + dtlinea.Rows.Item(0).Item("RET_GARANTIA")

                                                        dsCentrosAgrupados(x).Tables.Add(dtlinea)

                                                        'compara hasta llegar a otra linea añadiendo Costes,Descuentos e impuestos a la linea

                                                        Try
                                                            contador = contador + 1
                                                            If contador >= ds(0).Tables.Count Then
                                                                Exit While 'Evitar que busque un rango superior
                                                            End If
                                                            nombreTabla = ds(0).Tables(contador).TableName

                                                            dtlinea = ds(0).Tables(contador).Copy
                                                        Catch ex As Exception
                                                            GrabarError("Al obtener una tabla que no existe. Contador: " & contador, ex.Message)
                                                        End Try

                                                        While contador < ds(0).Tables.Count
                                                            If nombreTabla.StartsWith("LINEA") Then
                                                                Exit While
                                                            ElseIf nombreTabla.StartsWith("COSTE") Then
                                                                dtlinea = ds(0).Tables(contador).Copy
                                                                dsCentrosAgrupados(x).Tables.Add(dtlinea.Copy)
                                                                dsCentrosAgrupados(x).Tables(0).Rows(0).Item("IMPORTE") = dsCentrosAgrupados(x).Tables(0).Rows(0).Item("IMPORTE") + dtlinea.Rows.Item(0).Item("IMPORTE")
                                                                'AÑADIENDO COSTES E IMPUESTOS AL IMPORTE
                                                            ElseIf nombreTabla.StartsWith("DESCUENTO") Then
                                                                dtlinea = ds(0).Tables(contador).Copy
                                                                dsCentrosAgrupados(x).Tables.Add(dtlinea.Copy)
                                                                dsCentrosAgrupados(x).Tables(0).Rows(0).Item("IMPORTE") = dsCentrosAgrupados(x).Tables(0).Rows(0).Item("IMPORTE") - dtlinea.Rows.Item(0).Item("IMPORTE")
                                                                'RESTO LOS DESCUENTOS AL IMPORTE
                                                            ElseIf nombreTabla.StartsWith("IMPUESTOS") Then
                                                                dtlinea = ds(0).Tables(contador).Copy
                                                                dsCentrosAgrupados(x).Tables.Add(dtlinea.Copy)
                                                                dsCentrosAgrupados(x).Tables(0).Rows(0).Item("IMPORTE") = dsCentrosAgrupados(x).Tables(0).Rows(0).Item("IMPORTE") + dtlinea.Rows.Item(0).Item("IMPORTE")
                                                                'AÑADIENDO COSTES E IMPUESTOS 
                                                            End If

                                                            dtlinea = ds(0).Tables(contador).Copy
                                                            contador = contador + 1
                                                            If contador >= ds(0).Tables.Count Then
                                                                Exit While 'Evitar que busque un rango superior
                                                            End If
                                                            nombreTabla = ds(0).Tables(contador).TableName
                                                        End While
                                                    End If
                                                End If
                                            End If
                                        Next
                                        'COMO LA LINEA NO ES no añado costes,descuentos e impuestos
                                        If contador + 1 >= ds(0).Tables.Count Then
                                            'TABLA (0) = COUNT 1 POR ESO EL +1
                                            Exit While
                                        Else
                                            Try
                                                contador = contador + 1
                                                nombreTabla = ds(0).Tables(contador).TableName
                                            Catch ex As Exception
                                                GrabarError("Al obtener el nombre de una tabla que no existe. Contador: " & contador, ex.Message)
                                            End Try

                                            While contador + 1 < ds(0).Tables.Count
                                                If nombreTabla.StartsWith("LINEA") Then
                                                    Exit While
                                                End If
                                                Try
                                                    contador = contador + 1
                                                    nombreTabla = ds(0).Tables(contador).TableName
                                                Catch ex As Exception
                                                    GrabarError("Al obtener el nombre de una tabla que no existe. Contador: " & contador, ex.Message)
                                                End Try
                                            End While
                                        End If
                                    End If

                                    If Not (nombreTabla.StartsWith("LINEA")) Then
                                        contador = contador + 1
                                    End If
                                End While
                            Next
                            'Recorro los centros de coste y escribo los datasets

                            For Z As Integer = 0 To dsCentrosAgrupados.Length - 1

                                If dsCentrosAgrupados(Z).Tables(0).Rows(0).Item("NUM") < numeroMax Then
                                    dsCentrosAgrupados(Z).Tables(0).Rows(0).Item("NUM") = vbEmpty
                                    If dsCentrosAgrupados(Z).Tables.Count >= 2 Then
                                        'La tabla 1 tiene La Factura
                                        'INICO ESCRITURA XML

                                        dtSolicitudAux = GenerarDTSolicitud(tipoMoneda)
                                        dsCentrosAgrupados(Z).Tables.Add(dtSolicitudAux)
                                        dsCentrosAgrupados(Z).Tables(0).Rows(0).Item("INSTANCIA") = instancia 'añado la instancia a la factura
                                        dsCentrosAgrupados(Z) = ComprobarValorLinea(dsCentrosAgrupados(Z))
                                        dsCentrosAgrupados(Z).DataSetName = "NewDataSet" 'por formato para su tratamiento este debe ser el nombre del dataSet
                                        dsCentrosAgrupados(Z).WriteXml(ConfigurationManager.AppSettings("rutaXML") & "\" & usuario & "#" & instancia & "#" & idbloque & "#" & "F" & ".xml") ', XmlWriteMode.WriteSchema no me cambia todos los tags
                                        NombresTags = NombreTablas(dsCentrosAgrupados(Z))
                                        RemplazarNumerosTags(NombresTags, ConfigurationManager.AppSettings("rutaXML") & "\" & usuario & "#" & instancia & "#" & idbloque & "#" & "F" & ".xml") 'quitando tags
                                        'FIN ESCRITURA XML
                                    End If
                                Else
                                    dsCentrosAgrupados(Z).Tables(0).Rows(0).Item("NUM") = vbEmpty
                                    GenerarXMLConLineaMaxima(dsCentrosAgrupados(Z), numeroMax)
                                End If
                            Next

                            'FIN DE ESCRITURA DE LOS DATASETS SIN IMPUESTOS

                        End If
                    End If

                Else ' escritura cuando no tenemos ni impuestos ni centros de coste

                    If ds(0) IsNot Nothing Then
                        If ds(0).Tables(0) IsNot Nothing Then
                            proveedor = ds(0).Tables(0).Rows(0).Item("PROVE")
                            tipoMoneda = ds(0).Tables(0).Rows(0).Item("MON")

                            If ds(0).Tables(0).Rows(0).Item("NUM") < numeroMax Then
                                ds(0).Tables(0).Rows(0).Item("NUM") = vbEmpty

                                'INICO ESCRITURA XML

                                dtSolicitudAux = GenerarDTSolicitud(tipoMoneda)
                                ds(0).Tables.Add(dtSolicitudAux)
                                ds(0).Tables(0).Rows(0).Item("INSTANCIA") = instancia 'añado la instancia a la factura
                                ds(0) = ComprobarValorLinea(ds(0)) 'para los casos que me ha cambiado el numero linea 
                                ds(0).DataSetName = "NewDataSet" 'por formato para su tratamiento este debe ser el nombre del dataSet
                                ds(0).WriteXml(ConfigurationManager.AppSettings("rutaXML") & "\" & usuario & "#" & instancia & "#" & idbloque & "#" & "F" & ".xml")
                                NombresTags = NombreTablas(ds(0))
                                RemplazarNumerosTags(NombresTags, ConfigurationManager.AppSettings("rutaXML") & "\" & usuario & "#" & instancia & "#" & idbloque & "#" & "F" & ".xml") 'quitando tags
                                'FIN ESCRITURA XML

                            Else
                                ds(0).Tables(0).Rows(0).Item("NUM") = vbEmpty
                                GenerarXMLConLineaMaxima(ds(0), numeroMax)
                            End If
                        End If
                    End If
                End If 'validar que ds(1) no sea vacio
            End If
        End If
    End Sub
    '''<summary>
    ''' Devuelve un datatable  con los 2 primeros grupos de compatibilidad que tenemos (solo me interesa el primero)
    ''' </summary>
    ''' <param name="dt">datatable con todos sus datos</param>
    ''' <remarks>Llamada desde:GeneracionFacturas.vb GenerarXMLPorGastos_Inversiones_PorCentroCoste; Tiempo máximo: segundos</remarks>
    Function ObtenerGruposCompatibilidad(ByVal dt As DataTable) As DataTable
        Dim dtGrupoCompatibilidad As DataTable = Nothing
        Dim dtAux As DataTable
        Dim newrowGRP As DataRow
        Dim existe As Boolean

        dtAux = New DataTable
        dtAux.Columns.Add("GRP_COMP", System.Type.GetType("System.Int32"))
        Try
            dtGrupoCompatibilidad = dtAux.Clone
            For i As Integer = 0 To dt.Rows.Count - 1
                If dtGrupoCompatibilidad Is Nothing Then
                    newrowGRP = dtGrupoCompatibilidad.NewRow
                    newrowGRP.Item("GRP_COMP") = dt.Rows(i).Item("GRP_COMP")
                    dtGrupoCompatibilidad.Rows.Add(newrowGRP)
                Else
                    For x As Integer = 0 To dtGrupoCompatibilidad.Rows.Count - 1
                        If dt.Rows(i).Item("GRP_COMP") = dtGrupoCompatibilidad.Rows(x).Item("GRP_COMP") Then
                            existe = True
                            Exit For
                        End If
                    Next
                    If existe = False Then
                        newrowGRP = dtGrupoCompatibilidad.NewRow
                        newrowGRP.Item("GRP_COMP") = dt.Rows(i).Item("GRP_COMP")
                        dtGrupoCompatibilidad.Rows.Add(newrowGRP)
                        If dtGrupoCompatibilidad.Rows.Count >= 2 Then
                            Exit For 'Solo me interesa saber que tengo mas de 1 los demas me dan igual.
                        End If
                    Else
                        existe = False
                    End If
                End If
            Next

        Catch ex As Exception
            GrabarError("Al tratar de obtener los grupos de compatibildad.", ex.Message)
        End Try

        Return dtGrupoCompatibilidad
    End Function
    '''<summary>
    ''' Escribe un dataset solo con los impuesto del grupo de compatibilidad que le pasamos
    ''' </summary>
    ''' <param name="ds1">dataset con todos sus datos</param>
    ''' <param name="iGpr_Comp">integer con el codigo de compatibilidad se impuesto que puede llevar la factura</param>
    ''' <remarks>Llamada desde:GeneracionFacturas.vb GenerarXMLPorGastos_Inversiones_PorCentroCoste; Tiempo máximo: segundos</remarks>
    Function DataSetGrupodeCompatibilidad(ByVal ds1 As DataSet, ByVal iGpr_Comp As Integer) As DataSet

        Dim dsImpuestoGRP_COMP As DataSet

        dsImpuestoGRP_COMP = New DataSet
        dsImpuestoGRP_COMP.Tables.Add(ds1.Tables(0).Copy)

        'INICIO CARGA DEL RESTO DE LINEAS
        Dim contador As Integer
        Dim numlineas As Integer = 0
        Dim nombreTabla As String
        Dim numeroLinea As Integer

        Dim dsAuxMaximoLineas As DataSet
        dsAuxMaximoLineas = New DataSet

        Dim dtlinea As DataTable

        Dim importe As Double = 0
        Dim retGarantia As Double = 0

        numeroLinea = 1
        contador = 0

        Dim lineaAux As Integer = 0 'para guardar el codigo de la linea 

        Try

            While contador < ds1.Tables.Count
                nombreTabla = ds1.Tables(contador).TableName
                dtlinea = ds1.Tables(contador).Copy

                If nombreTabla.StartsWith("LINEA") Then

                    dtlinea.Rows.Item(0).Item("NUM") = dsImpuestoGRP_COMP.Tables(0).Rows(0).Item("NUM") + 1
                    dtlinea.Rows.Item(0).Item("LINEA") = dtlinea.Rows.Item(0).Item("NUM") 'AÑADIENDO A LINEA EL NUM
                    dsImpuestoGRP_COMP.Tables(0).Rows(0).Item("NUM") = dtlinea.Rows.Item(0).Item("NUM")
                    retGarantia = retGarantia + dtlinea.Rows.Item(0).Item("RET_GARANTIA")
                    lineaAux = dtlinea.Rows.Item(0).Item("LIN_PEDIDO")
                    'añadiendo al importe y la retencion de garantia a la factura
                    dsImpuestoGRP_COMP.Tables(0).Rows(0).Item("IMPORTE") = dsImpuestoGRP_COMP.Tables(0).Rows(0).Item("IMPORTE") + dtlinea.Rows.Item(0).Item("IMPORTE")
                    dsImpuestoGRP_COMP.Tables(0).Rows(0).Item("RET_GARANTIA") = dsImpuestoGRP_COMP.Tables(0).Rows(0).Item("RET_GARANTIA") + dtlinea.Rows.Item(0).Item("RET_GARANTIA")
                    dsImpuestoGRP_COMP.Tables.Add(dtlinea.Copy)

                    Try
                        contador = contador + 1
                        If contador >= ds1.Tables.Count Then
                            Exit While 'Evitar que busque un rango superior
                        End If
                        nombreTabla = ds1.Tables(contador).TableName
                    Catch ex As Exception
                        GrabarError("La tabla no existe al obtener su nombre. Contador: " & contador, ex.Message)
                    End Try
                    dtlinea = ds1.Tables(contador).Copy

                    While contador < ds1.Tables.Count
                        If nombreTabla.StartsWith("LINEA") Then
                            Exit While
                        ElseIf nombreTabla.StartsWith("COSTE") Then
                            dtlinea = ds1.Tables(contador).Copy
                            dsImpuestoGRP_COMP.Tables.Add(dtlinea.Copy)
                            dsImpuestoGRP_COMP.Tables(0).Rows(0).Item("IMPORTE") = dsImpuestoGRP_COMP.Tables(0).Rows(0).Item("IMPORTE") + dtlinea.Rows.Item(0).Item("IMPORTE")
                            'AÑADIENDO COSTES E IMPUESTOS AL IMPORTE
                        ElseIf nombreTabla.StartsWith("DESCUENTO") Then
                            dtlinea = ds1.Tables(contador).Copy
                            dsImpuestoGRP_COMP.Tables.Add(dtlinea.Copy)
                            dsImpuestoGRP_COMP.Tables(0).Rows(0).Item("IMPORTE") = dsImpuestoGRP_COMP.Tables(0).Rows(0).Item("IMPORTE") - dtlinea.Rows.Item(0).Item("IMPORTE")
                            'RESTO LOS DESCUENTOS AL IMPORTE
                        ElseIf nombreTabla.StartsWith("IMPUESTOS") AndAlso ds1.Tables(contador).Rows.Item(0).Item("GRP_COMP") = iGpr_Comp Then
                            dtlinea = ds1.Tables(contador).Copy
                            dsImpuestoGRP_COMP.Tables.Add(dtlinea.Copy)
                            dsImpuestoGRP_COMP.Tables(0).Rows(0).Item("IMPORTE") = dsImpuestoGRP_COMP.Tables(0).Rows(0).Item("IMPORTE") + dtlinea.Rows.Item(0).Item("IMPORTE")
                            'AÑADIENDO COSTES E IMPUESTOS 
                        End If

                        dtlinea = ds1.Tables(contador).Copy
                        contador = contador + 1
                        If contador >= ds1.Tables.Count Then
                            Exit While 'Evitar que busque un rango superior
                        End If
                        nombreTabla = ds1.Tables(contador).TableName
                    End While
                Else
                    'COMO LA LINEA NO ES no añado costes,descuentos e impuestos
                    If contador + 1 >= ds1.Tables.Count Then
                        'TABLA (0) = COUNT 1 POR ESO EL +1
                        Exit While
                    Else
                        Try
                            contador = contador + 1
                            nombreTabla = ds1.Tables(contador).TableName
                        Catch ex As Exception
                            GrabarError("Al obtener el nombre de una tabla que no existe. Contador: " & contador, ex.Message)
                        End Try

                        While contador + 1 < ds1.Tables.Count
                            If nombreTabla.StartsWith("LINEA") Then
                                Exit While
                            End If
                            Try
                                contador = contador + 1
                                nombreTabla = ds1.Tables(contador).TableName
                            Catch ex As Exception
                                GrabarError("Al obtener el nombre de una tabla que no existe. Contador: " & contador, ex.Message)
                            End Try
                        End While
                    End If
                End If
            End While

        Catch ex As Exception
            GrabarError("Al cargar el resto de las líneas.", ex.Message)
        End Try

        'FIN CARGA RESTO DE LINEAS
        Return dsImpuestoGRP_COMP

    End Function

End Module
