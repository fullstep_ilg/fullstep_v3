﻿Imports System.Configuration
Imports System.Xml
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
Imports System.Threading
Imports PMGenerarFacturas.Encrypter
Imports System.IO
Imports System.Web.Services

<Serializable()> _
Public Class Facturas
    'Inherits Security

    Private m_lNumeroFacturasPendientes As Long

    Private mDBLogin As String = ""
    Private mDBPassword As String = ""
    Private mDBName As String = ""
    Private mDBServer As String = ""
    Private mDBConnection As String = ""
    Private mIsAuthenticated As Boolean = False
    Public Property NumeroFacturasPendientes() As Long
        Get
            Return m_lNumeroFacturasPendientes
        End Get

        Set(ByVal Value As Long)
            m_lNumeroFacturasPendientes = Value
        End Set
    End Property

    Private Enum Tipo
        Coste = 0
        Descuento = 1
    End Enum
    'La he tenido que añadir porque en el EP se necesita para acceder a BD desde la CAPA DE negocio corregir ep!!
    ReadOnly Property DBConnection() As String
        Get
            Return mDBConnection
        End Get
    End Property
  '''<summary>
    ''' Obtener array de dataset con todos los datos que necesitamos para generar autofactura
    ''' </summary>  
    '''  <param name="empresa">int que contiene el id de la empresa</param>
    ''' <remarks>Llamada desde:GeneracionFacturas.vb GenerarDSAutoFactConCentroCoste y GenerarDSAutoFactSinCentroCoste; Tiempo máximo: segundos</remarks>
    Public Function Facturas_CargarDatos(ByVal empresa As Integer, ByVal fecha As DateTime) As DataSet
        'Cargar DataSet con todos los datos de linea de Pedido

        mDBConnection = DBConnection()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSPM_GENERAR_AUTO_FACTURAS"
            cm.Parameters.AddWithValue("@EMPRESA", empresa) '
            cm.Parameters.AddWithValue("@FECHALIMITERECEPCION", fecha)
            cm.CommandType = CommandType.StoredProcedure
            da.SelectCommand = cm
            da.Fill(ds)

            ds.Tables(0).TableName = "LINEAS"
            ds.Tables(1).TableName = "LINEAS_CON_RECEPCIONES"
            ds.Tables(2).TableName = "COSTES_Y_DESCUENTOS"
            ds.Tables(3).TableName = "IMPUESTOS_PROV_ART"
            ds.Tables(4).TableName = "LINEAS_CENTROS_COSTE"
            'Todos los costes y descuentos que vienen del pedido son planificados (por definición lo son), de modo que añadimos una columna "PLANIFICADO" con valor 1 para todos. Al generar la factura se grabará ese valor.
            Dim dcPlanificado As New DataColumn
            dcPlanificado.ColumnName = "PLANIFICADO"
            dcPlanificado.DataType = System.Type.GetType("System.Int32")
            dcPlanificado.DefaultValue = 1
            ds.Tables("COSTES_Y_DESCUENTOS").Columns.Add(dcPlanificado)
            Return ds
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try
    End Function
    '''<summary>
    ''' Obtener dataset con el id de las empresas que se deben AutoFacturar
    ''' </summary>  
    ''' <remarks>Llamada desde:GeneracionFacturas.vb ; Tiempo máximo: segundos</remarks>
    Public Function EmpresasAAutoFacturar() As DataSet
        'Cargar DataSet con todos los datos de linea de Pedido
        'Prueba MPF para cargar dataSet con datos que necesitaria el .xml
        mDBConnection = DBConnection()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter

        Try
            cn.Open()
            cm = New SqlCommand
            cm.Connection = cn
            cm.CommandText = "FSPM_DEVOLVER_EMPRESAS_A_AUTOFACTURAR"
            cm.CommandType = CommandType.StoredProcedure
            da.SelectCommand = cm
            da.Fill(ds)

            ds.Tables(0).TableName = "EMPRESAS"

            Return ds
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()

        End Try

    End Function

    Public Sub New()
        'Comprobar tema de licencias y cargar datos de conexión
        Dim sDB As String = ConfigurationManager.AppSettings("DB:FS_WS")
        If sDB Is Nothing Then
            Throw New Exception("Configuration file corrupted. Missing FS_WS entry !")
        End If
        'Tenemos que extraer el login y la contraseña de conexión.
        Dim doc As New XmlDocument
        Dim sdblogin As String = ""
        Dim sdbPassword As String = ""
        Try
            doc.Load(Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory(), "License.xml"))

        Catch e As Exception
            Throw New Exception("LICENSE file missing!")
        End Try

        Dim child As XmlNode
        child = doc.SelectSingleNode("/LICENSE/DB_LOGIN")
        If Not (child Is Nothing) Then
            mDBLogin = Encrypter.Encrypt(child.InnerText, , False, Encrypter.TipoDeUsuario.Administrador)
            'mDBLogin = child.InnerText
        End If
        child = Nothing
        child = doc.SelectSingleNode("/LICENSE/DB_PASSWORD")
        If Not (child Is Nothing) Then
            mDBPassword = Encrypter.Encrypt(child.InnerText, , False, Encrypter.TipoDeUsuario.Administrador)
            'mDBPassword = child.InnerText
        Else
            mDBPassword = ""
        End If

        child = doc.SelectSingleNode("/LICENSE/DB_NAME")
        If Not (child Is Nothing) Then
            mDBName = Encrypter.Encrypt(child.InnerText, , False, Encrypter.TipoDeUsuario.Administrador)
        End If
        child = Nothing
        child = doc.SelectSingleNode("/LICENSE/DB_SERVER")
        If Not (child Is Nothing) Then
            mDBServer = Encrypter.Encrypt(child.InnerText, , False, Encrypter.TipoDeUsuario.Administrador)
        End If

        If mDBServer = "" Or mDBName = "" Or mDBLogin = "" Then
            Throw New Exception("Corrupted LICENSE file !")
        End If

        'PENDIENTE METER ENCRIPTACIÓN

        sDB = sDB.Replace("DB_LOGIN", mDBLogin)
        sDB = sDB.Replace("DB_PASSWORD", mDBPassword)
        sDB = sDB.Replace("DB_NAME", mDBName)
        sDB = sDB.Replace("DB_SERVER", mDBServer)
        doc = Nothing
        mDBConnection = sDB

    End Sub
    ''' <summary>
    ''' Funcion que devuelve los parámetros generales de la instalación
    ''' </summary>
    ''' <returns>Un objeto del tipo ParametrosGenerales con los que correspondan de la aplicación</returns>
    ''' <remarks>
    ''' Llamada desdE: PmServer/root/GetAccesType
    ''' Tiempo máximo: 0,15 seg</remarks>
    Public Function Parametros_GetData() As Data.DataSet

        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Try
            cn.Open()
            cm = New SqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED", cn)
            cm.ExecuteNonQuery()
            cm = New SqlCommand
            cm.Connection = cn
            cm.CommandText = "FSPM_GETPARAMETROS"
            cm.CommandType = CommandType.StoredProcedure

            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try


    End Function
    '''<summary>
    ''' Obtener tabla con datos del usuario codigo del usuario y workflow peticionario(datos[ peticionario,solicitud,idrol,workflow])
    ''' </summary>  
    ''' <remarks>Llamada desde:GeneracionFacturas.vb ; Tiempo máximo: segundos</remarks>
    Public Function ObtenerDatosUsuario(Solicitud As Integer) As DataTable

        mDBConnection = DBConnection()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter

        Try
            cn.Open()
            cm = New SqlCommand
            cm.Connection = cn
            cm.CommandText = "FSPM_DEVOLVER_DATOS_USUARIO"
            cm.Parameters.AddWithValue("@SOLICITUD", Solicitud)
            cm.CommandType = CommandType.StoredProcedure
            da.SelectCommand = cm
            da.Fill(dt)

            dt.TableName = "USUARIO"

            Return dt
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()

        End Try

    End Function
    '''<summary>
    ''' Creo una instancia y obtengo el id de la misma
    ''' </summary>  
    ''' <remarks>Llamada desde:GeneracionFacturas.vb ; Tiempo máximo: segundos</remarks>
    Public Function CrearInstancia(ByVal solicitud As Integer, ByVal IdFormulario As Long, ByVal IdWorkflow As Long, ByVal moneda As String, ByVal per As String) As Integer
        mDBConnection = DBConnection()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand
        Dim id As Integer
        Dim errorvar As Long

        Try
            cn.Open()
            cm = New SqlCommand
            cm.Connection = cn

            cm.CommandText = "FSPM_CREATE_NEW_INSTANCE"
            cm.CommandType = CommandType.StoredProcedure

            cm.Parameters.AddWithValue("@SOLICITUD", solicitud)
            cm.Parameters.AddWithValue("@FORMULARIO", IdFormulario)
            cm.Parameters.AddWithValue("@WORKFLOW", IdWorkflow)
            cm.Parameters.AddWithValue("@IMPORTE_PEDIDO_DIRECTO", 0)
            cm.Parameters.AddWithValue("@PETICIONARIO", per)
            cm.Parameters.AddWithValue("@MON", moneda)
            cm.Parameters.Add("@ID", SqlDbType.Int).Direction = ParameterDirection.InputOutput
            cm.Parameters("@ID").Value = id

            cm.ExecuteNonQuery()

            id = cm.Parameters("@ID").Value

            Return id
        Catch ex As Exception
            Throw ex
        Finally
            cn.Dispose()
            cm.Dispose()

        End Try
    End Function

    ''' <summary>
    ''' Actualiza el tiempo de procesamiento de las solicitudes
    ''' </summary>
    ''' <param name="lID">Id (campo identity). Si no tiene nada hay que hacer una insert, si no, una update.</param>
    ''' <param name="lInstancia">Id de la instancia que se está procesando</param>
    ''' <param name="sCodUsuario">Cod. del usuario</param>
    ''' <param name="lBloque">Id del bloque en el que estamos</param>
    ''' <param name="iFecha">Fecha que hay que actualizar (inicio, fin,..)</param>
    ''' <param name="HayError">Si se ha producido un error</param>
    ''' <param name="EsPortal">Si es una solicitud de portal</param>
    ''' <param name="TipoGuardado">No guardado, insert, update</param>
    ''' <param name="TipoAccion">no accion, cambio etapa, cambio etapa y validaciones, traslado, devolucion</param>
    ''' <remarks>Llamada desde: PMServer --> Instancia.vb -->ActualizarTiempoProcesamiento; Tiempo máximo: 0sg.</remarks>
    Public Sub ActualizarTiempoProcesamiento(ByRef lID As Long, ByVal lInstancia As Long, ByVal sCodUsuario As String, ByVal lBloque As Long, ByVal iFecha As Integer, ByVal HayError As Boolean, ByVal EsPortal As Nullable(Of Boolean),
                                            ByVal TipoGuardado As Nullable(Of Integer), ByVal TipoAccion As Nullable(Of Integer))
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Try
            cn.Open()
            With cm
                .Connection = cn
                .CommandType = CommandType.StoredProcedure
                .CommandText = "FSWS_ACTUALIZAR_TIEMPO_PROC"
                If Not lInstancia = 0 Then .Parameters.AddWithValue("@INSTANCIA", lInstancia)
                If Not String.IsNullOrEmpty(sCodUsuario) Then .Parameters.AddWithValue("@USUARIO", sCodUsuario)
                .Parameters.Add("@ID", SqlDbType.Int).Direction = ParameterDirection.InputOutput
                If Not lID = 0 Then
                    .Parameters("@ID").Value = lID
                    .Parameters.AddWithValue("@CAMPOFECHA", iFecha)
                End If
                If Not lBloque = 0 Then .Parameters.AddWithValue("@BLOQUE", lBloque)
                If HayError Then .Parameters.AddWithValue("@ERROR", HayError)
                If EsPortal.HasValue Then .Parameters.AddWithValue("@ESPORTAL", EsPortal)
                If TipoGuardado.HasValue Then .Parameters.AddWithValue("@TIPOGUARDADO", TipoGuardado)
                If TipoAccion.HasValue Then .Parameters.AddWithValue("@TIPOACCION", TipoAccion)
                .ExecuteNonQuery()

                If lID = Nothing Then lID = .Parameters("@ID").Value
            End With
        Catch e As Exception
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Sub

    '''<summary>
    ''' obtengo una tabla con el id deL bloque de la instancia y el id accion
    ''' </summary>  
    ''' <remarks>Llamada desde:GeneracionFacturas.vb ; Tiempo máximo: segundos</remarks>
    Public Function ObtenerBloque_Accion_Auto(ByVal idinstancia As Integer) As DataTable
        mDBConnection = DBConnection()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand
        Dim id As Integer
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter

        Try

            cn.Open()
            cm = New SqlCommand
            cm.Connection = cn
            cm.CommandText = "FSPM_DEVOLVER_BLOQUE_ACCION_AUTOFACTURA"
            cm.Parameters.AddWithValue("@ID", idinstancia)
            cm.CommandType = CommandType.StoredProcedure
            da.SelectCommand = cm
            da.Fill(dt)

            dt.TableName = "BLOQUE"

            Return dt
        Finally
            cn.Dispose()
            cm.Dispose()

        End Try

    End Function
    ''' <summary>
    ''' Actualiza el campo EN_PROCESO de la tabla INSTANCIA
    ''' </summary>
    ''' <param name="lInstancia">Identificador de instancia</param>
    ''' <param name="iEnProceso">0,1,2 según la acción a realizar</param>
    ''' <param name="iBloque">Identificador de bloque</param>
    ''' <remarks></remarks>
    Public Sub Actualizar_En_Proceso(ByVal lInstancia As Long, ByVal iEnProceso As Integer, Optional ByVal iBloque As Integer = 0)

        'Actualiza el campo EN_PROCESO de la tabla INSTANCIA
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter

        Try
            cn.Open()
            cm = New SqlCommand

            With cm
                .Connection = cn
                .CommandType = CommandType.StoredProcedure
                .CommandText = "FSPM_ACTUALIZAR_EN_PROCESO"
                .Parameters.AddWithValue("@ID", lInstancia)
                .Parameters.AddWithValue("@EN_PROCESO", iEnProceso)
                .Parameters.AddWithValue("@BLOQUE", iBloque)

                .ExecuteNonQuery()
            End With

        Catch e As Exception
            cm = New SqlCommand("IF @@TRANCOUNT>0 ROLLBACK TRANSACTION", cn)
            cm.ExecuteNonQuery()
            Throw e
        Finally
            cn.Dispose()
            cm.Dispose()
        End Try
    End Sub
End Class
