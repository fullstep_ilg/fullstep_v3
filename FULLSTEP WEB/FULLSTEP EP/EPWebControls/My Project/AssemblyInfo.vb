﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices
Imports System.Web.UI

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("EPWebControls")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyProduct("EPWebControls")> 

<Assembly: TagPrefix("EPWebControls", "fsn")> 

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("39dbd480-62c3-44ea-8cc8-c13df87a2db5")> 

<Assembly: WebResource("Fullstep.EPWebControls.cesta.gif", "image/gif")> 
<Assembly: WebResource("Fullstep.EPWebControls.cargando.gif", "image/gif")> 
<Assembly: WebResource("Fullstep.EPWebControls.lupa.gif", "image/gif")> 
<Assembly: WebResource("Fullstep.EPWebControls.trans.gif", "image/gif")> 
<Assembly: WebResource("Fullstep.EPWebControls.camion.jpg", "image/jpeg")> 
<Assembly: WebResource("Fullstep.EPWebControls.FSEPWebPartCesta.js", "application/x-javascript")> 