﻿Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.HtmlControls
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.ComponentModel
Imports System.Configuration

Public Class FSEPWebPartPedidosFav
    Inherits WebPart

    Private lblTitulo As Label
    Private lblFavoritos As Label
    Private lblNumPedidos As Label
    Private lnkVerTodos As FSNWebControls.FSNHyperlink
    Private iNumPedidos As Integer
    Private lstPedidos As DataList
    Private mNumLineas As Integer
    Private sTitulo As String
    Private _Textos(5) As String
    Private _sIdioma As String

    Private Class DataListTemplate
        Implements ITemplate

        ''' <summary>
        ''' Define el objeto Control al que pertenecen los controles secundarios y las plantillas. Estos controles secundarios están a su vez definidos en una plantilla en línea.
        ''' </summary>
        ''' <param name="container">Objeto Control que contiene las instancias de los controles de la plantilla en línea.</param>
        Private Sub InstantiateIn(ByVal container As Control) _
           Implements ITemplate.InstantiateIn
            Dim hr = New HtmlGenericControl("hr")
            container.Controls.Add(hr)
            Dim tabla As New HtmlTable()
            tabla.Width = "100%"
            tabla.Attributes.Add("class", "ListItemLink")
            AddHandler tabla.DataBinding, AddressOf EmitirPedido_DataBinding
            Dim fila As New HtmlTableRow()
            Dim celda As New HtmlTableCell()
            celda.ColSpan = 2
            celda.Style.Add(HtmlTextWriterStyle.FontWeight, "bold")
            Dim lc As New Literal()
            AddHandler lc.DataBinding, AddressOf Denominacion_DataBinding
            celda.Controls.Add(lc)
            fila.Cells.Add(celda)
            tabla.Rows.Add(fila)
            fila = New HtmlTableRow()
            celda = New HtmlTableCell()
            celda.ColSpan = 2
            lc = New Literal()
            AddHandler lc.DataBinding, AddressOf Empresa_DataBinding
            celda.Controls.Add(lc)
            fila.Cells.Add(celda)
            tabla.Rows.Add(fila)
            fila = New HtmlTableRow()
            celda = New HtmlTableCell()
            lc = New Literal()
            AddHandler lc.DataBinding, AddressOf Receptor_DataBinding
            celda.Controls.Add(lc)
            fila.Cells.Add(celda)
            celda = New HtmlTableCell()
            celda.RowSpan = 2
            celda.VAlign = "bottom"
            celda.Align = "right"
            celda.Style.Add(HtmlTextWriterStyle.FontWeight, "bold")
            lc = New Literal()
            AddHandler lc.DataBinding, AddressOf Importe_DataBinding
            celda.Controls.Add(lc)
            fila.Cells.Add(celda)
            tabla.Rows.Add(fila)
            fila = New HtmlTableRow()
            celda = New HtmlTableCell()
            lc = New Literal()
            AddHandler lc.DataBinding, AddressOf Proveedor_DataBinding
            celda.Controls.Add(lc)
            fila.Cells.Add(celda)
            tabla.Rows.Add(fila)
            container.Controls.Add(tabla)
        End Sub

        Private Sub Denominacion_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim lc As Literal = CType(sender, Literal)
            Dim container As DataListItem = CType(lc.NamingContainer, DataListItem)
            lc.Text = DataBinder.Eval(container.DataItem, "Denominacion")
        End Sub

        ''' <summary>
        ''' Se produce al enlazar la etiqueta de la plantilla en que se muestra la empresa con el origen de datos.
        ''' </summary>
        Private Sub Empresa_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim lc As Literal = CType(sender, Literal)
            Dim container As DataListItem = CType(lc.NamingContainer, DataListItem)
            lc.Text = DataBinder.Eval(container.DataItem, "Empresa")
        End Sub

        Private Sub Receptor_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim lc As Literal = CType(sender, Literal)
            Dim container As DataListItem = CType(lc.NamingContainer, DataListItem)
            lc.Text = DataBinder.Eval(container.DataItem, "Receptor")
        End Sub

        ''' <summary>
        ''' Se produce al enlazar la etiqueta de la plantilla en que se muestra el proveedor con el origen de datos.
        ''' </summary>
        Private Sub Proveedor_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim lc As Literal = CType(sender, Literal)
            Dim container As DataListItem = CType(lc.NamingContainer, DataListItem)
            lc.Text = DataBinder.Eval(container.DataItem, "Proveedor")
        End Sub

        ''' <summary>
        ''' Se produce al enlazar la etiqueta de la plantilla en que se muestra el importe con el origen de datos.
        ''' </summary>
        Private Sub Importe_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim lc As Literal = CType(sender, Literal)
            Dim container As DataListItem = CType(lc.NamingContainer, DataListItem)
            lc.Text = DataBinder.Eval(container.DataItem, "Importe")
        End Sub

        ''' <summary>
        ''' Se produce al enlazar la tabla de la plantilla que muestra los datos del pedido con el origen de datos.
        ''' </summary>
        Private Sub EmitirPedido_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim tab As HtmlTable = CType(sender, HtmlTable)
            Dim container As DataListItem = CType(tab.NamingContainer, DataListItem)
            tab.Attributes.Add("onclick", "window.location.href='" & ConfigurationManager.AppSettings("rutaEP") & "EmisionPedido.aspx?DesdeFavorito=True&favNuevo=False&ID=" & DataBinder.Eval(container.DataItem, "Id") & "';")
        End Sub

    End Class

    <Serializable()> _
    Public Class mOrden
        Private miId As Integer
        Private mDenominacion As String
        Private mEmpresa As String
        Private mReceptor As String
        Private mProveedor As String
        Private mImporte As String

        Public Property Id() As Integer
            Get
                Return miId
            End Get
            Set(ByVal value As Integer)
                miId = value
            End Set
        End Property

        Public Property Denominacion() As String
            Get
                Return mDenominacion
            End Get
            Set(ByVal value As String)
                mDenominacion = value
            End Set
        End Property

        Public Property Empresa() As String
            Get
                Return mEmpresa
            End Get
            Set(ByVal value As String)
                mEmpresa = value
            End Set
        End Property

        Public Property Receptor() As String
            Get
                Return mReceptor
            End Get
            Set(ByVal value As String)
                mReceptor = value
            End Set
        End Property

        Public Property Proveedor() As String
            Get
                Return mProveedor
            End Get
            Set(ByVal value As String)
                mProveedor = value
            End Set
        End Property

        Public Property Importe() As String
            Get
                Return mImporte
            End Get
            Set(ByVal value As String)
                mImporte = value
            End Set
        End Property

    End Class

    Private Class ControlEditor
        Inherits EditorPart

        Private _txtTitulo As TextBox
        Private _txtAncho As TextBox
        Private _reqAncho As RequiredFieldValidator
        Private _ranAncho As RangeValidator
        Private _txtNumPedidos As TextBox
        Private _reqNumPedidos As RequiredFieldValidator
        Private _ranNumPedidos As RangeValidator

        ''' <summary>
        ''' Lo llama el marco de trabajo de las páginas ASP.NET para indicar a los controles de servidor que utilizan la implementación basada en la composición que creen los controles secundarios que contengan como forma de preparar la devolución o representación de los datos.
        ''' </summary>
        Protected Overrides Sub CreateChildControls()
            Dim part As FSEPWebPartPedidosFav = CType(WebPartToEdit, FSEPWebPartPedidosFav)
            Controls.Clear()
            _txtTitulo = New TextBox()
            _txtTitulo.Width = Unit.Pixel(250)
            _txtTitulo.Text = part.Title
            Controls.Add(_txtTitulo)
            _txtAncho = New TextBox()
            _txtAncho.MaxLength = 3
            _txtAncho.Width = Unit.Pixel(30)
            _txtAncho.Text = part.Width.Value.ToString()
            _txtAncho.ID = Me.ID & "_txtAncho"
            Controls.Add(_txtAncho)
            _reqAncho = New RequiredFieldValidator()
            _reqAncho.ID = _txtAncho.ID & "_Req"
            _reqAncho.ErrorMessage = "*"
            _reqAncho.ControlToValidate = _txtAncho.ID
            Controls.Add(_reqAncho)
            _ranAncho = New RangeValidator()
            _ranAncho.ID = _txtAncho.ID & "_Ran"
            _ranAncho.Type = ValidationDataType.Integer
            _ranAncho.MaximumValue = 500
            _ranAncho.MinimumValue = 200
            _ranAncho.ErrorMessage = "(200-500)"
            _ranAncho.ControlToValidate = _txtAncho.ID
            Controls.Add(_ranAncho)
            _txtNumPedidos = New TextBox()
            _txtNumPedidos.MaxLength = 2
            _txtNumPedidos.Width = Unit.Pixel(20)
            _txtNumPedidos.Text = part.mNumLineas
            _txtNumPedidos.ID = Me.ID & "_txtNumPedidos"
            Controls.Add(_txtNumPedidos)
            _reqNumPedidos = New RequiredFieldValidator()
            _reqNumPedidos.ID = _txtNumPedidos.ID & "_Req"
            _reqNumPedidos.ErrorMessage = "*"
            _reqNumPedidos.ControlToValidate = _txtNumPedidos.ID
            Controls.Add(_reqNumPedidos)
            _ranNumPedidos = New RangeValidator()
            _ranNumPedidos.ID = _txtNumPedidos.ID & "_Ran"
            _ranNumPedidos.Type = ValidationDataType.Integer
            _ranNumPedidos.MaximumValue = 99
            _ranNumPedidos.MinimumValue = 0
            _ranNumPedidos.ErrorMessage = "(0-99)"
            _ranNumPedidos.ControlToValidate = _txtNumPedidos.ID
            Controls.Add(_ranNumPedidos)
        End Sub

        ''' <summary>
        ''' Presenta el contenido del control en el sistema de escritura especificado. Este método lo usan principalmente los programadores de controles. 
        ''' </summary>
        ''' <param name="writer">HtmlTextWriter que representa la secuencia de salida para presentar contenido HTML en el cliente.</param>
        Protected Overrides Sub RenderContents(ByVal writer As System.Web.UI.HtmlTextWriter)
            Dim part As FSEPWebPartPedidosFav = CType(WebPartToEdit, FSEPWebPartPedidosFav)
            writer.Write("<b>" & part.Textos(2) & ":</b> ")
            _txtTitulo.RenderControl(writer)
            writer.WriteBreak()
            writer.Write("<b>" & part.Textos(3) & ":</b> ")
            _txtAncho.RenderControl(writer)
            _reqAncho.RenderControl(writer)
            _ranAncho.RenderControl(writer)
            writer.WriteBreak()
            writer.Write("<b>" & part.Textos(4) & ":</b> ")
            _txtNumPedidos.RenderControl(writer)
            _reqNumPedidos.RenderControl(writer)
            _ranNumPedidos.RenderControl(writer)
            writer.WriteBreak()
        End Sub

        ''' <summary>
        ''' Guarda los valores de un control EditorPart en las propiedades correspondientes del control WebPart asociado.
        ''' </summary>
        ''' <returns>Es true si la acción de guardar los valores del control EditorPart en el control WebPart se realiza correctamente; en caso contrario (si se produce un error), es false.</returns>
        Public Overrides Function ApplyChanges() As Boolean
            _reqAncho.Validate()
            _ranAncho.Validate()
            _reqNumPedidos.Validate()
            _ranNumPedidos.Validate()
            If _reqAncho.IsValid() And _ranAncho.IsValid() And _reqNumPedidos.IsValid() And _ranNumPedidos.IsValid() Then
                Dim part As FSEPWebPartPedidosFav = CType(WebPartToEdit, FSEPWebPartPedidosFav)
                part.Title = _txtTitulo.Text
                part.Width = Unit.Pixel(CType(_txtAncho.Text, Integer))
                part.mNumLineas = CType(_txtNumPedidos.Text, Integer)
                part.CargarPedidos()
                Return True
            Else
                Return False
            End If
        End Function

        ''' <summary>
        ''' Obtiene los valores de propiedad de un control WebPart correspondientes a su control EditorPart asociado.
        ''' </summary>
        Public Overrides Sub SyncChanges()
            Dim part As FSEPWebPartPedidosFav = CType(WebPartToEdit, FSEPWebPartPedidosFav)
            txtTitulo.Text = part.Title
            txtAncho.Text = part.Width.Value.ToString()
            txtNumPedidos.Text = part.mNumLineas
        End Sub

        Private ReadOnly Property txtTitulo() As TextBox
            Get
                EnsureChildControls()
                Return _txtTitulo
            End Get
        End Property

        Private ReadOnly Property txtAncho() As TextBox
            Get
                EnsureChildControls()
                Return _txtAncho
            End Get
        End Property

        Private ReadOnly Property txtNumPedidos() As TextBox
            Get
                EnsureChildControls()
                Return _txtNumPedidos
            End Get
        End Property

    End Class

    <Personalizable(), WebBrowsable(), DefaultValue(5)> _
    Public Property NumLineas() As Integer
        Get
            Return mNumLineas
        End Get
        Set(ByVal value As Integer)
            mNumLineas = value
        End Set
    End Property

    ''' <summary>
    ''' Devuelve el idioma de la página que alberga el control.
    ''' </summary>
    ''' <returns>El código de idioma.</returns>
    Private Function Idioma() As String
        If Not DesignMode Then
            If Me.Page Is Nothing AndAlso TypeOf Me.Page Is FSNServer.IFSNPage Then
                Return CType(Me.Page, FSNServer.IFSNPage).Idioma
            Else
                For Each x As String In HttpContext.Current.Session.Keys
                    If TypeOf HttpContext.Current.Session(x) Is FSNServer.User Then
                        Return CType(HttpContext.Current.Session(x), FSNServer.User).Idioma
                    End If
                Next
                Return String.Empty
            End If
        Else
            Return String.Empty
        End If
    End Function

    Public ReadOnly Property Textos(ByVal Index As Short) As String
        Get
            If String.IsNullOrEmpty(_Textos(Index)) Or Idioma() <> _sIdioma Then
                If Not DesignMode Then
                    Dim FSNDict As FSNServer.Dictionary = New FSNServer.Dictionary()
                    _sIdioma = Idioma()
                    FSNDict.LoadData(TiposDeDatos.ModulosIdiomas.Webparts, _sIdioma)
                    With FSNDict.Data.Tables(0)
                        _Textos(0) = .Rows(8).Item(1)
                        _Textos(1) = .Rows(7).Item(1)
                        _Textos(2) = .Rows(19).Item(1)
                        _Textos(3) = .Rows(21).Item(1)
                        _Textos(4) = .Rows(22).Item(1)
                        _Textos(5) = .Rows(26).Item(1)
                    End With
                    Return _Textos(Index)
                Else
                    Return String.Empty
                End If
            Else
                Return _Textos(Index)
            End If
        End Get
    End Property

    Public Overrides Property Title() As String
        Get
            If String.IsNullOrEmpty(MyBase.Title) Then
                If String.IsNullOrEmpty(sTitulo) Then
                    If Me.Page Is Nothing And String.IsNullOrEmpty(_Textos(0)) Then
                        MyBase.Title = Textos(0)
                        sTitulo = MyBase.Title
                    Else
                        sTitulo = _Textos(0)
                    End If
                End If
                Return sTitulo
            Else
                sTitulo = MyBase.Title
                Return MyBase.Title
            End If
        End Get
        Set(ByVal value As String)
            MyBase.Title = value
        End Set
    End Property

    Public Sub New()
        mNumLineas = 5
        Me.Width = Unit.Pixel(335)
    End Sub

    ''' <summary>
    ''' Lo llama el marco de trabajo de las páginas ASP.NET para indicar a los controles de servidor que utilizan la implementación basada en la composición que creen los controles secundarios que contengan como forma de preparar la devolución o representación de los datos.
    ''' </summary>
    Protected Overrides Sub CreateChildControls()
        Controls.Clear()
        Dim img As HtmlImage = New HtmlImage()
        If DesignMode Then img.Src = "images/trans.gif" Else img.Src = VirtualPathUtility.ToAbsolute("~/images/trans.gif")
        img.Width = 335
        img.Height = 1
        Me.Controls.Add(img)
        Dim tabla As HtmlTable = New HtmlTable()
        tabla.Width = "100%"
        tabla.Height = "55px"
        Dim fila As HtmlTableRow = New HtmlTableRow()
        fila.Height = "55px"
        Dim celda As HtmlTableCell = New HtmlTableCell()
        celda.Width = "90%"
        lblTitulo = New Label()
        lblTitulo.CssClass = "Rotulo"
        celda.Controls.Add(lblTitulo)
        celda.Controls.Add(New LiteralControl("<br/>"))
        lblNumPedidos = New Label()
        lblNumPedidos.CssClass = "EtiquetaGrande"
        celda.Controls.Add(lblNumPedidos)
        lblFavoritos = New Label()
        lblFavoritos.CssClass = "EtiquetaGrande"
        celda.Controls.Add(lblFavoritos)
        fila.Cells.Add(celda)
        celda = New HtmlTableCell
        celda.Align = "right"
        celda.VAlign = "bottom"
        img.Width = 105
        celda.Controls.Add(img)
        If DesignMode Then
            celda.Style.Add("background-image", "images/corazon.gif")
        Else
            celda.Style.Add("background-image", VirtualPathUtility.ToAbsolute("~/images/corazon.gif"))
        End If
        celda.Style.Add("background-position", "top left")
        celda.Style.Add("background-repeat", "no-repeat")
        lnkVerTodos = New FSNWebControls.FSNHyperlink()
        lnkVerTodos.NavigateUrl = ConfigurationManager.AppSettings("rutaEP") & "Favoritos.aspx"
        celda.Controls.Add(lnkVerTodos)
        fila.Cells.Add(celda)
        tabla.Rows.Add(fila)
        fila = New HtmlTableRow
        celda = New HtmlTableCell
        celda.ColSpan = 2
        lstPedidos = New DataList()
        lstPedidos.Width = Unit.Percentage(100)
        lstPedidos.ItemTemplate = New DataListTemplate()
        celda.Controls.Add(lstPedidos)
        fila.Cells.Add(celda)
        tabla.Rows.Add(fila)
        Me.Controls.Add(tabla)

        lblTitulo.Text = Textos(0)
        lblFavoritos.Text = " " & Textos(5)
        lnkVerTodos.Text = Textos(1)

        If Not DesignMode Then _
            CargarPedidos()
    End Sub

    ''' <summary>
    ''' Envía el contenido del control de servidor a un objeto HtmlTextWriter que se proporciona y almacena la información de seguimiento sobre el control si dicho seguimiento está habilitado.
    ''' </summary>
    ''' <param name="writer">Objeto HTmlTextWriter que recibe el contenido del control.</param>
    Public Overrides Sub RenderControl(ByVal writer As System.Web.UI.HtmlTextWriter)
        If DesignMode Then
            lblTitulo.Text = "Pedidos Favoritos"
            lblNumPedidos.Text = "4 pedidos"
            lnkVerTodos.Text = "Ver Todos"
            Dim lista As New List(Of mOrden)
            For i As Integer = 1 To 5
                Dim mo As New mOrden()
                mo.Denominacion = "Pedido Favorito " & i.ToString()
                mo.Empresa = "Empresa " & i.ToString()
                mo.Receptor = "Receptor " & i.ToString()
                mo.Proveedor = "Proveedor " & i.ToString()
                mo.Importe = CDbl(325.47).ToString("0.00") & " &euro;"
                lista.Add(mo)
            Next
            lstPedidos.DataSource = lista
            lstPedidos.DataBind()
        End If
        MyBase.RenderControl(writer)
    End Sub

    ''' <summary>
    ''' Carga los pedidos en el DataList del control.
    ''' </summary>
    ''' <remarks>Llamadas desde: FSEPWebPartpedidos_PreRender</remarks>
    Private Sub CargarPedidos()
        If Not Me.Page Is Nothing AndAlso TypeOf Me.Page Is FSNServer.IFSNPage Then
            Dim pag As FSNServer.IFSNPage = CType(Me.Page, FSNServer.IFSNPage)
            Dim srv As New FSNServer.Root
            Dim ds As DataSet
            Dim iLineas As Integer = mNumLineas
            Dim lista As New List(Of mOrden)
            Dim _FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim oCOrdenesFavoritos As FSNServer.COrdenesFavoritos = _FSNServer.Get_Object(GetType(FSNServer.COrdenesFavoritos))
            ds = oCOrdenesFavoritos.BuscarPorUsuario(pag.Usuario.Cod, iLineas)
            If ds.Tables.Count > 0 Then
                For Each fila As DataRow In ds.Tables(0).Rows
                    Dim mo As New mOrden()
                    mo.Id = fila.Item("ID")
                    mo.Denominacion = fila.Item("DENOMINACION")
                    mo.Empresa = fila.Item("DENEMPRESA")
                    mo.Receptor = DBNullToStr(fila.Item("NOMRECEPTOR"))
                    mo.Proveedor = fila.Item("PROVEDEN")
                    mo.Importe = FSNLibrary.FormatNumber(CType(fila.Item("IMPORTE") * fila.Item("CAMBIO"), Double), pag.Usuario.NumberFormat) & " " & fila.Item("MON")
                    lista.Add(mo)
                Next
            End If
            lstPedidos.DataSource = lista
            lstPedidos.DataBind()
            iNumPedidos = iLineas
            lblNumPedidos.Text = iNumPedidos.ToString()
            lnkVerTodos.Visible = iNumPedidos > 0
        End If
    End Sub

    ''' <summary>
    ''' Devuelve una colección de controles EditorPart personalizados que pueden utilizarse para editar un control WebPart cuando se encuentra en modo de edición.
    ''' </summary>
    ''' <returns>Una colección EditorPartCollection que contiene controles EditorPart personalizados asociados a un control WebPart.</returns>
    Public Overrides Function CreateEditorParts() As System.Web.UI.WebControls.WebParts.EditorPartCollection
        Dim editorArray As New ArrayList()
        Dim edPart As New ControlEditor()
        edPart.ID = Me.ID & "_editor1"
        editorArray.Add(edPart)
        Dim editorParts As New EditorPartCollection(editorArray)
        Return editorParts
    End Function

    Public Overrides ReadOnly Property WebBrowsableObject() As Object
        Get
            Return Me
        End Get
    End Property

    ''' <summary>
    ''' Registra el control como un control cuyo estado se debe conservar.
    ''' </summary>
    ''' <param name="e">Objeto EventArgs que contiene los datos del evento.</param>
    Protected Overrides Sub OnInit(ByVal e As System.EventArgs)
        MyBase.OnInit(e)
        Page.RegisterRequiresControlState(Me)
    End Sub

    ''' <summary>
    ''' Guarda los cambios de estado del control.
    ''' </summary>
    ''' <returns>Devuelve el estado actual del control de servidor.</returns>
    Protected Overrides Function SaveControlState() As Object
        Dim obj As Object = MyBase.SaveControlState()
        If obj IsNot Nothing Then
            obj = New Pair(obj, _Textos)
        Else
            obj = _Textos
        End If
        obj = New Pair(obj, _sIdioma)
        Return obj
    End Function

    ''' <summary>
    ''' Restaura información de estado del control.
    ''' </summary>
    ''' <param name="state">Object que representa el estado del control que se va a restaurar.</param>
    Protected Overrides Sub LoadControlState(ByVal state As Object)
        If (state IsNot Nothing) Then
            Dim p As Pair = TryCast(state, Pair)
            _sIdioma = p.Second
            state = p.First
            p = TryCast(state, Pair)
            If p IsNot Nothing Then
                MyBase.LoadControlState(p.First)
                _Textos = CType(p.Second, String())
            Else
                If (TypeOf (state) Is String()) Then
                    _Textos = CType(state, String())
                Else
                    MyBase.LoadControlState(state)
                End If
            End If
        End If
    End Sub
End Class
