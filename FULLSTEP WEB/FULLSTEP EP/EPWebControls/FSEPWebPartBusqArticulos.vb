﻿Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Configuration
Imports System.Text
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.HtmlControls
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts

Public Class FSEPWebPartBusqArticulos
    Inherits WebPart

    Private Class ControlEditor
        Inherits EditorPart

        Private _txtTitulo As TextBox

        ''' <summary>
        ''' Lo llama el marco de trabajo de las páginas ASP.NET para indicar a los controles de servidor que utilizan la implementación basada en la composición que creen los controles secundarios que contengan como forma de preparar la devolución o representación de los datos.
        ''' </summary>
        Protected Overrides Sub CreateChildControls()
            Dim part As FSEPWebPartBusqArticulos = CType(WebPartToEdit, FSEPWebPartBusqArticulos)
            Controls.Clear()
            _txtTitulo = New TextBox()
            _txtTitulo.Width = Unit.Pixel(250)
            _txtTitulo.Text = part.Title
            Controls.Add(_txtTitulo)
        End Sub

        ''' <summary>
        ''' Presenta el contenido del control en el sistema de escritura especificado. Este método lo usan principalmente los programadores de controles. 
        ''' </summary>
        ''' <param name="writer">HtmlTextWriter que representa la secuencia de salida para presentar contenido HTML en el cliente.</param>
        Protected Overrides Sub RenderContents(ByVal writer As System.Web.UI.HtmlTextWriter)
            Dim part As FSEPWebPartBusqArticulos = CType(WebPartToEdit, FSEPWebPartBusqArticulos)
            writer.Write("<b>" & part.Textos(3) & ":</b> ")
            _txtTitulo.RenderControl(writer)
            writer.WriteBreak()
        End Sub

        ''' <summary>
        ''' Guarda los valores de un control EditorPart en las propiedades correspondientes del control WebPart asociado.
        ''' </summary>
        ''' <returns>Es true si la acción de guardar los valores del control EditorPart en el control WebPart se realiza correctamente; en caso contrario (si se produce un error), es false.</returns>
        Public Overrides Function ApplyChanges() As Boolean
            Dim part As FSEPWebPartBusqArticulos = CType(WebPartToEdit, FSEPWebPartBusqArticulos)
            part.Title = _txtTitulo.Text
            Return True
        End Function

        ''' <summary>
        ''' Obtiene los valores de propiedad de un control WebPart correspondientes a su control EditorPart asociado.
        ''' </summary>
        Public Overrides Sub SyncChanges()
            Dim part As FSEPWebPartBusqArticulos = CType(WebPartToEdit, FSEPWebPartBusqArticulos)
            txtTitulo.Text = part.Title
        End Sub

        Private ReadOnly Property txtTitulo() As TextBox
            Get
                EnsureChildControls()
                Return _txtTitulo
            End Get
        End Property

    End Class

    Private lblBusqArt As Label
    Private txtBusqArt As TextBox
    Private wtmBusqArt As AjaxControlToolkit.TextBoxWatermarkExtender
    Private WithEvents btnBuscarArt As FSNWebControls.FSNButton
    Private sTitulo As String
    Private _Textos(3) As String
    Private _sIdioma As String

    ''' <summary>
    ''' Devuelve el idioma de la página que alberga el control.
    ''' </summary>
    ''' <returns>El código de idioma.</returns>
    Private Function Idioma() As String
        If Not DesignMode Then
            If Me.Page Is Nothing AndAlso TypeOf Me.Page Is FSNServer.IFSNPage Then
                Return CType(Me.Page, FSNServer.IFSNPage).Idioma
            Else
                For Each x As String In HttpContext.Current.Session.Keys
                    If TypeOf HttpContext.Current.Session(x) Is FSNServer.User Then
                        Return CType(HttpContext.Current.Session(x), FSNServer.User).Idioma
                    End If
                Next
                Return String.Empty
            End If
        Else
            Return String.Empty
        End If
    End Function

    Private ReadOnly Property Textos(ByVal Index As Short) As String
        Get
            If String.IsNullOrEmpty(_Textos(Index)) Or Idioma() <> _sIdioma Then
                If Not DesignMode Then
                    Dim FSNDict As FSNServer.Dictionary = New FSNServer.Dictionary()
                    _sIdioma = Idioma()
                    FSNDict.LoadData(TiposDeDatos.ModulosIdiomas.Webparts, _sIdioma)
                    For i As Short = 0 To 2
                        _Textos(i) = FSNDict.Data.Tables(0).Rows(i + 48).Item(1)
                    Next
                    _Textos(3) = FSNDict.Data.Tables(0).Rows(19).Item(1)
                    Return _Textos(Index)
                Else
                    Select Case Index
                        Case 1
                            Return "Nombre, Descripción..."
                        Case 2
                            Return "Buscar"
                        Case 3
                            Return "Título"
                        Case Else
                            Return "Buscar Artículos"
                    End Select
                End If
            Else
                Return _Textos(Index)
            End If
        End Get
    End Property

    Public Overrides Property Title() As String
        Get
            If String.IsNullOrEmpty(MyBase.Title) Then
                If String.IsNullOrEmpty(sTitulo) Then
                    If Me.Page Is Nothing And String.IsNullOrEmpty(_Textos(0)) Then
                        MyBase.Title = Textos(0)
                        sTitulo = MyBase.Title
                    Else
                        sTitulo = _Textos(0)
                    End If
                End If
                Return sTitulo
            Else
                sTitulo = MyBase.Title
                Return MyBase.Title
            End If
        End Get
        Set(ByVal value As String)
            MyBase.Title = value
        End Set
    End Property

    ''' Revisado por:blp. Fecha: 29/05/2012
    ''' <summary>
    ''' Lo llama el marco de trabajo de las páginas ASP.NET para indicar a los controles de servidor que utilizan la implementación basada en la composición que creen los controles secundarios que contengan como forma de preparar la devolución o representación de los datos.
    ''' </summary>
    '''<remarks>Llamada desde el momento de creación de los controles en el ciclo de vida de la página. Max: 0,3 seg.</remarks>
    Protected Overrides Sub CreateChildControls()
        Controls.Clear()
        Dim tabla As HtmlTable = New HtmlTable()
        tabla.Width = "100%"
        Dim fila As HtmlTableRow = New HtmlTableRow()
        Dim celda As HtmlTableCell = New HtmlTableCell()
        celda.Align = "left"
        Dim img As HtmlImage = New HtmlImage()
        img.Src = Me.Page.ClientScript.GetWebResourceUrl(GetType(FSEPWebPartCesta), "Fullstep.EPWebControls.lupa.gif")
        celda.Controls.Add(img)
        Dim espacio As New Literal()
        espacio.Text = "&nbsp;"
        celda.Controls.Add(espacio)
        lblBusqArt = New Label()
        lblBusqArt.CssClass = "Etiqueta"
        celda.Controls.Add(lblBusqArt)
        fila.Cells.Add(celda)
        tabla.Rows.Add(fila)
        fila = New HtmlTableRow()
        celda = New HtmlTableCell()
        celda.Align = "left"
        txtBusqArt = New TextBox()
        txtBusqArt.ID = "txtBusqArt"
        txtBusqArt.Width = New Unit(200, UnitType.Pixel)
        celda.Controls.Add(txtBusqArt)
        Dim AutoCompl As AjaxControlToolkit.AutoCompleteExtender = New AjaxControlToolkit.AutoCompleteExtender()
        AutoCompl.CompletionInterval = 500
        AutoCompl.MinimumPrefixLength = 3
        AutoCompl.ServiceMethod = "GetAppNos"
        AutoCompl.ServicePath = ConfigurationManager.AppSettings("rutaEP") & "App_Services/EPConsultas.asmx"
        AutoCompl.TargetControlID = "txtBusqArt"
        AutoCompl.ContextKey = HttpContext.Current.Session("sSession")
        AutoCompl.UseContextKey = True
        AutoCompl.ID = "AutoCompl"
        'Añadimos una función de javascript al evento de mostrar el panel de autocomplete para corregir un bug del control AjaxControlToolkit.AutoCompleteExtender, 
        'por el que en FF y Chrome se añade un margin al control por el tamaño de letra definido para el control contenedor, en este caso el body, de modo que el desplegable no se encuentra pegado al textbox del que depende.
        'Esta función se asigna al evento OnClientShowing del control en EPWebControls\FSEPWebPartBusqArticulos.vb
        AutoCompl.OnClientShowing = "quitarMarginTop"
        Dim sScript As String = "function quitarMarginTop() { $get('" & Me.ClientID & "_" & AutoCompl.ClientID & "_completionListElem').style.marginTop = '0px';};"
        Me.Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "quitarMarginTop", sScript, True)

        'Funciones para codificar caracteres especiales (<, >, &) y que la validación del formulario que se lanza antes del evento onclick del botón de buscar no de errores.
        sScript = "function htmlEncode(str) {" & vbCrLf & _
        "str = this.replaceAll(str, '&', '&amp;');" & vbCrLf & _
        "str = this.replaceAll(str, '<', '&lt;');" & vbCrLf & _
        "str = this.replaceAll(str, '>', '&gt;');" & vbCrLf & _
        "return str;" & vbCrLf & "}" & vbCrLf & _
        "function replaceAll(str, subStr, newStr) {" & vbCrLf & _
            "var offset = 0;" & vbCrLf & _
            "var index = str.indexOf(subStr);" & vbCrLf & _
            "while (index != -1) {" & vbCrLf & _
                "str = str.substr(0, index) + newStr + str.substr(index + subStr.length);" & vbCrLf & _
                "offset = index + newStr.length;" & vbCrLf & _
                "index = str.indexOf(subStr, offset);" & vbCrLf & "}" & vbCrLf & _
            "return str;" & vbCrLf & "}" & vbCrLf & _
        "function encodeComment() {" & vbCrLf & "document.getElementById('" & Me.ClientID & "_" & txtBusqArt.ClientID & "').value = htmlEncode(document.getElementById('" & Me.ClientID & "_" & txtBusqArt.ClientID & "').value);" & vbCrLf & "}" & vbCrLf
        Me.Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "htmlEncode", sScript, True)

        celda.Controls.Add(AutoCompl)
        wtmBusqArt = New AjaxControlToolkit.TextBoxWatermarkExtender()
        wtmBusqArt.TargetControlID = "txtBusqArt"
        celda.Controls.Add(wtmBusqArt)
        fila.Cells.Add(celda)
        tabla.Rows.Add(fila)
        fila = New HtmlTableRow()
        celda = New HtmlTableCell()
        celda.Align = "right"
        btnBuscarArt = New FSNWebControls.FSNButton()
        btnBuscarArt.ID = "btnBuscarArt"
        'Codificamos los caracteres &, >, < para que no de problemas al validar la página
        btnBuscarArt.OnClientClick = "encodeComment();TempCargando();"
        celda.Controls.Add(btnBuscarArt)
        fila.Cells.Add(celda)
        tabla.Rows.Add(fila)
        Dim panel As Panel = New Panel()
        panel.DefaultButton = "btnBuscarArt"
        panel.Controls.Add(tabla)
        Me.Controls.Add(panel)

        lblBusqArt.Text = Textos(0)
        wtmBusqArt.WatermarkText = Textos(1)
        btnBuscarArt.Text = Textos(2)
        sTitulo = Textos(0)
    End Sub


#Region "btnBuscarArt"

    '''' <summary>
    '''' Evento creado para controlar desde la página contenedora el evento btnBuscarArt_Click
    '''' </summary>
    'Public Event Search_Submit As EventHandler(Of eventoPersonalizado)

    Public Event manejadorDeEvento As CommandEventHandler


    ''' <summary>
    ''' Establece el contenido de la caja de texto como valor de la propiedad TextoBusqueda del control.
    ''' </summary>
    ''' <param name="sender">Origen del evento.</param>
    ''' <param name="e">Clase EventArgs que no contiene datos de evento.</param>
    ''' <remarks>Se produce cuando se hace clic en el control FSNButton [nombre del botón].</remarks>
    Protected Sub btnBuscarArt_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBuscarArt.Click
        'Cuando el click se hace desde la página EmisionPedido, el control del evento se va a hacer desde la propia página, no aquí
        If System.IO.Path.GetFileName(Me.Page.Request.PhysicalPath) = "EmisionPedido.aspx" Then
            'Dim even As New eventoPersonalizado
            'even.Texto = txtBusqArt.Text
            'RaiseEvent Search_Submit(Me, even)
            RaiseEvent manejadorDeEvento(Me, New CommandEventArgs("textoBusqueda", txtBusqArt.Text))
        Else
            HttpContext.Current.Response.Redirect(ConfigurationManager.AppSettings("rutaEP") & "CatalogoWeb.aspx?TextoBusquedaCatalogo=" & HttpContext.Current.Server.UrlEncode(txtBusqArt.Text))
        End If
    End Sub

#End Region

    ''' <summary>
    ''' Devuelve una colección de controles EditorPart personalizados que pueden utilizarse para editar un control WebPart cuando se encuentra en modo de edición.
    ''' </summary>
    ''' <returns>Una colección EditorPartCollection que contiene controles EditorPart personalizados asociados a un control WebPart.</returns>
    Public Overrides Function CreateEditorParts() As System.Web.UI.WebControls.WebParts.EditorPartCollection
        Dim editorArray As New ArrayList()
        Dim edPart As New ControlEditor()
        edPart.ID = Me.ID & "_editor1"
        editorArray.Add(edPart)
        Dim editorParts As New EditorPartCollection(editorArray)
        Return editorParts
    End Function

    ''' <summary>
    ''' Registra el control como un control cuyo estado se debe conservar.
    ''' </summary>
    ''' <param name="e">Objeto EventArgs que contiene los datos del evento.</param>
    Protected Overrides Sub OnInit(ByVal e As System.EventArgs)
        MyBase.OnInit(e)
        Page.RegisterRequiresControlState(Me)
    End Sub

    ''' <summary>
    ''' Guarda los cambios de estado del control.
    ''' </summary>
    ''' <returns>Devuelve el estado actual del control de servidor.</returns>
    Protected Overrides Function SaveControlState() As Object
        Dim obj As Object = MyBase.SaveControlState()
        If obj IsNot Nothing Then
            obj = New Pair(obj, _Textos)
        Else
            obj = _Textos
        End If
        obj = New Pair(obj, _sIdioma)
        Return obj
    End Function

    ''' <summary>
    ''' Restaura información de estado del control.
    ''' </summary>
    ''' <param name="state">Object que representa el estado del control que se va a restaurar.</param>
    Protected Overrides Sub LoadControlState(ByVal state As Object)
        If (state IsNot Nothing) Then
            Dim p As Pair = TryCast(state, Pair)
            _sIdioma = p.Second
            state = p.First
            p = TryCast(state, Pair)
            If p IsNot Nothing Then
                MyBase.LoadControlState(p.First)
                _Textos = CType(p.Second, String())
            Else
                If (TypeOf (state) Is String()) Then
                    _Textos = CType(state, String())
                Else
                    MyBase.LoadControlState(state)
                End If
            End If
        End If
    End Sub

End Class
