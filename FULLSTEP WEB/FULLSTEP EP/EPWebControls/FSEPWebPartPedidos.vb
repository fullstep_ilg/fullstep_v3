﻿Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.HtmlControls
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.ComponentModel
Imports System.Configuration

Public Class FSEPWebPartPedidos
    Inherits WebPart

    Private lblTitulo As Label
    Private lblPedidos As Label
    Private lblNumPedidos As Label
    Private lnkVerTodos As FSNWebControls.FSNHyperlink
    Private iNumPedidos As Integer
    Private lstPedidos As DataList
    Private mFiltroEstado As FiltroEstadoPedido
    Private mNumLineas As Integer
    Private sTitulo As String
    Private _Textos As DataSet
    Private _sIdioma As String
    Private _ModuloIdioma As FSNLibrary.TiposDeDatos.ModulosIdiomas
    Private _bPendientesAprobar As Boolean
    Private _bPendientesRecepcionar As Boolean

    Private Class DataListTemplate
        Implements ITemplate

        ''' <summary>
        ''' Define el objeto Control al que pertenecen los controles secundarios y las plantillas. Estos controles secundarios están a su vez definidos en una plantilla en línea.
        ''' </summary>
        ''' <param name="container">Objeto Control que contiene las instancias de los controles de la plantilla en línea.</param>
        Private Sub InstantiateIn(ByVal container As Control) _
           Implements ITemplate.InstantiateIn
            Dim hr = New HtmlGenericControl("hr")
            container.Controls.Add(hr)
            Dim tabla As New HtmlTable()
            tabla.Width = "100%"
            tabla.Attributes.Add("class", "ListItemLink")
            AddHandler tabla.DataBinding, AddressOf VerPedido_DataBinding
            Dim fila As New HtmlTableRow()
            Dim celda As New HtmlTableCell()
            celda.ColSpan = 2
            celda.Style.Add(HtmlTextWriterStyle.FontWeight, "bold")
            Dim lc As New Literal()
            AddHandler lc.DataBinding, AddressOf Empresa_DataBinding
            celda.Controls.Add(lc)
            fila.Cells.Add(celda)
            tabla.Rows.Add(fila)
            fila = New HtmlTableRow()
            celda = New HtmlTableCell()
            lc = New Literal()
            AddHandler lc.DataBinding, AddressOf Proveedor_DataBinding
            celda.Controls.Add(lc)
            fila.Cells.Add(celda)
            celda = New HtmlTableCell()
            celda.RowSpan = 2
            celda.Align = "right"
            celda.VAlign = "bottom"
            celda.Style.Add(HtmlTextWriterStyle.FontWeight, "bold")
            lc = New Literal()
            AddHandler lc.DataBinding, AddressOf Importe_DataBinding
            celda.Controls.Add(lc)
            fila.Cells.Add(celda)
            tabla.Rows.Add(fila)
            fila = New HtmlTableRow()
            celda = New HtmlTableCell()
            Dim lab = New Label()
            AddHandler lab.DataBinding, AddressOf Estado_DataBinding
            celda.Controls.Add(lab)
            fila.Cells.Add(celda)
            tabla.Rows.Add(fila)
            container.Controls.Add(tabla)
        End Sub

        ''' <summary>
        ''' Se produce al enlazar la etiqueta de la plantilla en que se muestra la empresa con el origen de datos.
        ''' </summary>
        Private Sub Empresa_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim lc As Literal = CType(sender, Literal)
            Dim container As DataListItem = CType(lc.NamingContainer, DataListItem)
            lc.Text = DataBinder.Eval(container.DataItem, "Empresa") & " " & DataBinder.Eval(container.DataItem, "Pedido")
        End Sub

        ''' <summary>
        ''' Se produce al enlazar la etiqueta de la plantilla en que se muestra el proveedor con el origen de datos.
        ''' </summary>
        Private Sub Proveedor_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim lc As Literal = CType(sender, Literal)
            Dim container As DataListItem = CType(lc.NamingContainer, DataListItem)
            lc.Text = DataBinder.Eval(container.DataItem, "Proveedor")
        End Sub

        ''' <summary>
        ''' Se produce al enlazar la etiqueta de la plantilla en que se muestra el estado del pedido con el origen de datos.
        ''' </summary>
        Private Sub Estado_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim lab As Label = CType(sender, Label)
            Dim container As DataListItem = CType(lab.NamingContainer, DataListItem)
            lab.Text = DataBinder.Eval(container.DataItem, "Estado")
            Select Case DataBinder.Eval(container.DataItem, "CodEstado")
                Case 1, 20, 21, 22
                    lab.ForeColor = Drawing.Color.Red
            End Select
        End Sub

        ''' <summary>
        ''' Se produce al enlazar la etiqueta de la plantilla en que se muestra el importe con el origen de datos.
        ''' </summary>
        Private Sub Importe_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim lc As Literal = CType(sender, Literal)
            Dim container As DataListItem = CType(lc.NamingContainer, DataListItem)
            lc.Text = DataBinder.Eval(container.DataItem, "Importe")
        End Sub

        ''' Revisado por: blp. Fecha: 23/04/2012
        ''' <summary>
        ''' Se produce al enlazar la tabla de la plantilla que muestra los datos del pedido con el origen de datos.
        ''' </summary>
        ''' <param name="sender">Control que lanza el evento</param>
        ''' <param name="e">parámetros del evento</param>
        ''' <remarks>Llamada desde evento. Max 0,1 seg.</remarks>
        Private Sub VerPedido_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim tab As HtmlTable = CType(sender, HtmlTable)
            Dim container As DataListItem = CType(tab.NamingContainer, DataListItem)
            tab.Attributes.Add("onclick", "TempCargando(); window.location.href='" & ConfigurationManager.AppSettings("rutaEP") & "seguimiento.aspx?orden=" & DataBinder.Eval(container.DataItem, "Pedido") & "';")
        End Sub

    End Class

    <Serializable()> _
    Public Class mOrden
        Private miId As Integer
        Private mEmpresa As String
        Private mPedido As String
        Private mProveedor As String
        Private mCodEstado As Integer
        Private mEstado As String
        Private mImporte As String

        Public Property Id() As Integer
            Get
                Return miId
            End Get
            Set(ByVal value As Integer)
                miId = value
            End Set
        End Property

        Public Property Empresa() As String
            Get
                Return mEmpresa
            End Get
            Set(ByVal value As String)
                mEmpresa = value
            End Set
        End Property

        Public Property Pedido() As String
            Get
                Return mPedido
            End Get
            Set(ByVal value As String)
                mPedido = value
            End Set
        End Property

        Public Property Proveedor() As String
            Get
                Return mProveedor
            End Get
            Set(ByVal value As String)
                mProveedor = value
            End Set
        End Property

        Public Property CodEstado() As Integer
            Get
                Return mCodEstado
            End Get
            Set(ByVal value As Integer)
                mCodEstado = value
            End Set
        End Property

        Public Property Estado() As String
            Get
                Return mEstado
            End Get
            Set(ByVal value As String)
                mEstado = value
            End Set
        End Property

        Public Property Importe() As String
            Get
                Return mImporte
            End Get
            Set(ByVal value As String)
                mImporte = value
            End Set
        End Property

    End Class

    <Flags()> _
    Public Enum FiltroEstadoPedido As Integer
        Ninguno = 0
        PendienteAprobar = 1
        DenegadoParcialmente = 2
        EmitidoProveedor = 4
        AceptadoProveedor = 8
        EnCamino = 16
        RecibidoParcialmente = 32
        RecibidoTotalmente = 64
        Anulado = 128
        Rechazado = 256
        Denegado = 512
    End Enum

    Private Class ControlEditor
        Inherits EditorPart

        Private _txtTitulo As TextBox
        Private _txtAncho As TextBox
        Private _reqAncho As RequiredFieldValidator
        Private _ranAncho As RangeValidator
        Private _txtNumPedidos As TextBox
        Private _reqNumPedidos As RequiredFieldValidator
        Private _ranNumPedidos As RangeValidator
        Private _chkFiltroEstado As CheckBoxList
        Private _chkPendientesAprobar As CheckBox
        Private _chkPendientesRecepcionar As CheckBox

        ''' Revisado por: blp. Fecha: 01/03/2012
        ''' <summary>
        ''' Lo llama el marco de trabajo de las páginas ASP.NET para indicar a los controles de servidor que utilizan la implementación basada en la composición que creen los controles secundarios que contengan como forma de preparar la devolución o representación de los datos.
        ''' </summary>
        ''' <remarks>Llamada desde Carga del webpart. Max. 0,3 seg.</remarks>
        Protected Overrides Sub CreateChildControls()
            Dim part As FSEPWebPartPedidos = CType(WebPartToEdit, FSEPWebPartPedidos)
            Controls.Clear()
            _txtTitulo = New TextBox()
            _txtTitulo.Width = Unit.Pixel(250)
            _txtTitulo.Text = part.Title
            Controls.Add(_txtTitulo)
            _txtAncho = New TextBox()
            _txtAncho.MaxLength = 3
            _txtAncho.Width = Unit.Pixel(30)
            _txtAncho.Text = part.Width.Value.ToString()
            _txtAncho.ID = Me.ID & "_txtAncho"
            Controls.Add(_txtAncho)
            _reqAncho = New RequiredFieldValidator()
            _reqAncho.ID = _txtAncho.ID & "_Req"
            _reqAncho.ErrorMessage = "*"
            _reqAncho.ControlToValidate = _txtAncho.ID
            Controls.Add(_reqAncho)
            _ranAncho = New RangeValidator()
            _ranAncho.ID = _txtAncho.ID & "_Ran"
            _ranAncho.Type = ValidationDataType.Integer
            _ranAncho.MaximumValue = 500
            _ranAncho.MinimumValue = 200
            _ranAncho.ErrorMessage = "(200-500)"
            _ranAncho.ControlToValidate = _txtAncho.ID
            Controls.Add(_ranAncho)
            _txtNumPedidos = New TextBox()
            _txtNumPedidos.MaxLength = 2
            _txtNumPedidos.Width = Unit.Pixel(20)
            _txtNumPedidos.Text = part.mNumLineas
            _txtNumPedidos.ID = Me.ID & "_txtNumPedidos"
            Controls.Add(_txtNumPedidos)
            _reqNumPedidos = New RequiredFieldValidator()
            _reqNumPedidos.ID = _txtNumPedidos.ID & "_Req"
            _reqNumPedidos.ErrorMessage = "*"
            _reqNumPedidos.ControlToValidate = _txtNumPedidos.ID
            Controls.Add(_reqNumPedidos)
            _ranNumPedidos = New RangeValidator()
            _ranNumPedidos.ID = _txtNumPedidos.ID & "_Ran"
            _ranNumPedidos.Type = ValidationDataType.Integer
            _ranNumPedidos.MaximumValue = 99
            _ranNumPedidos.MinimumValue = 0
            _ranNumPedidos.ErrorMessage = "(0-99)"
            _ranNumPedidos.ControlToValidate = _txtNumPedidos.ID
            Controls.Add(_ranNumPedidos)
            _chkFiltroEstado = New CheckBoxList
            _chkFiltroEstado.Items.Clear()
            _chkFiltroEstado.Items.Add(New ListItem(part.Textos(3), 1))
            _chkFiltroEstado.Items.Add(New ListItem(part.Textos(4), 2))
            _chkFiltroEstado.Items.Add(New ListItem(part.Textos(5), 4))
            _chkFiltroEstado.Items.Add(New ListItem(part.Textos(6), 8))
            _chkFiltroEstado.Items.Add(New ListItem(part.Textos(7), 16))
            _chkFiltroEstado.Items.Add(New ListItem(part.Textos(8), 32))
            _chkFiltroEstado.Items.Add(New ListItem(part.Textos(9), 64))
            _chkFiltroEstado.Items.Add(New ListItem(part.Textos(10), 128))
            _chkFiltroEstado.Items.Add(New ListItem(part.Textos(11), 256))
            _chkFiltroEstado.Items.Add(New ListItem(part.Textos(12), 512))
            _chkFiltroEstado.ID = "_chkFiltroEstado"
            Controls.Add(_chkFiltroEstado)
            _chkPendientesAprobar = New CheckBox
            _chkPendientesAprobar.ID = "_chkPendientesAprobar"
            _chkPendientesAprobar.Text = part.Textos(17) 'Pendiente de aprobar por mí
            _chkPendientesAprobar.Checked = part._bPendientesAprobar
            Controls.Add(_chkPendientesAprobar)
            _chkPendientesRecepcionar = New CheckBox
            _chkPendientesRecepcionar.ID = "_chkPendientesRecepcionar"
            _chkPendientesRecepcionar.Text = part.Textos(18) 'Pendientes de recepcionar por mí­
            _chkPendientesRecepcionar.Checked = part._bPendientesRecepcionar
            Controls.Add(_chkPendientesRecepcionar)
            Dim sJavascript As String = ""
            For Each oItem As ListItem In _chkFiltroEstado.Items
                sJavascript = "" & _
                        "if(this.checked)" & _
                            "{ " & _
                                "var chk = document.getElementById('" & _chkPendientesRecepcionar.ClientID & "');" & _
                                "if(chk){chk.checked=false;}" & _
                                "chk = document.getElementById('" & _chkPendientesAprobar.ClientID & "');" & _
                                "if(chk){chk.checked=false;}" & _
                            "}"
                oItem.Attributes.Add("onclick", sJavascript)
            Next
            sJavascript = "" & _
                    "if(document.getElementById('" & _chkPendientesAprobar.ClientID & "').checked)" & _
                        "{ " & _
                            "var chk = document.getElementById('" & _chkPendientesRecepcionar.ClientID & "');" & _
                            "if(chk){chk.checked=false;}" & _
                            "var chklst = document.getElementById('" & _chkFiltroEstado.ClientID & "');" & _
                            "var chklstCount= chklst.getElementsByTagName('input');" & _
                            "if(chklst){for(var i=0;i<chklstCount.length;i++){chklstCount[i].checked = false;}}" & _
                        "}"
            _chkPendientesAprobar.Attributes.Add("onclick", sJavascript)
            sJavascript = "" & _
                    "if(document.getElementById('" & _chkPendientesRecepcionar.ClientID & "').checked)" & _
                        "{ " & _
                            "var chk = document.getElementById('" & _chkPendientesAprobar.ClientID & "');" & _
                            "if(chk){chk.checked=false;}" & _
                            "var chklst = document.getElementById('" & _chkFiltroEstado.ClientID & "');" & _
                            "var chklstCount= chklst.getElementsByTagName('input');" & _
                            "if(chklst){for(var i=0;i<chklstCount.length;i++){chklstCount[i].checked = false;}}" & _
                        "}"
            _chkPendientesRecepcionar.Attributes.Add("onclick", sJavascript)
        End Sub

        ''' Revisado por: blp. Fecha:08/03/2012
        ''' <summary>
        ''' Presenta el contenido del control en el sistema de escritura especificado. Este método lo usan principalmente los programadores de controles. 
        ''' </summary>
        ''' <param name="writer">HtmlTextWriter que representa la secuencia de salida para presentar contenido HTML en el cliente.</param>
        ''' <remarks>Llamada desde la carga del control. Máx. 0,1 seg.</remarks>
        Protected Overrides Sub RenderContents(ByVal writer As System.Web.UI.HtmlTextWriter)
            Dim part As FSEPWebPartPedidos = CType(WebPartToEdit, FSEPWebPartPedidos)
            writer.Write("<div width=""100%"" style=""clear:both;""> ")
            writer.Write("<b>" & part.Textos(13) & ":</b> ")
            _txtTitulo.RenderControl(writer)
            writer.WriteBreak()
            writer.Write("<b>" & part.Textos(15) & ":</b> ")
            _txtAncho.RenderControl(writer)
            _reqAncho.RenderControl(writer)
            _ranAncho.RenderControl(writer)
            writer.WriteBreak()
            writer.Write("<b>" & part.Textos(16) & ":</b> ")
            _txtNumPedidos.RenderControl(writer)
            _reqNumPedidos.RenderControl(writer)
            _ranNumPedidos.RenderControl(writer)
            writer.Write("</div>")
            writer.Write("<div width=""50%"" style=""float:left;""> ")
            writer.Write("<b>" & part.Textos(14) & "</b>")
            writer.Write("</div>")
            writer.WriteBreak()
            writer.Write("<div width=""100%""> ")
            writer.Write("<fieldset width=""50%"" style=""float:left;""> ")
            _chkFiltroEstado.RenderControl(writer)
            writer.Write("</fieldset>")
            writer.Write("<fieldset width=""50%"" style=""text-align:left; margin-bottom:2px;""> ")
            _chkPendientesAprobar.RenderControl(writer)
            writer.Write("</fieldset>")
            writer.Write("<fieldset width=""50%"" style=""text-align:left; height:100%;""> ")
            _chkPendientesRecepcionar.RenderControl(writer)
            writer.Write("</fieldset>")
            writer.Write("</div>")
            writer.WriteBreak()
        End Sub

        ''' Revisado por: blp. Fecha: 08/03/2012
        ''' <summary>
        ''' Guarda los valores de un control EditorPart en las propiedades correspondientes del control WebPart asociado.
        ''' </summary>
        ''' <returns>Es true si la acción de guardar los valores del control EditorPart en el control WebPart se realiza correctamente; en caso contrario (si se produce un error), es false.</returns>
        ''' <remarks>Llamada desde el webpart. Máx. 0,3 seg.</remarks>
        Public Overrides Function ApplyChanges() As Boolean
            _reqAncho.Validate()
            _ranAncho.Validate()
            _reqNumPedidos.Validate()
            _ranNumPedidos.Validate()
            If _reqAncho.IsValid() And _ranAncho.IsValid() And _reqNumPedidos.IsValid() And _ranNumPedidos.IsValid() Then
                Dim part As FSEPWebPartPedidos = CType(WebPartToEdit, FSEPWebPartPedidos)
                part.Title = _txtTitulo.Text
                part.Width = Unit.Pixel(CType(_txtAncho.Text, Integer))
                part.mNumLineas = CType(_txtNumPedidos.Text, Integer)
                Dim filtro As FiltroEstadoPedido = FiltroEstadoPedido.Ninguno
                For Each x As ListItem In _chkFiltroEstado.Items
                    If x.Selected Then
                        filtro = filtro Or CType(x.Value, Integer)
                    End If
                Next
                part.mFiltroEstado = filtro
                part._bPendientesAprobar = _chkPendientesAprobar.Checked
                part._bPendientesRecepcionar = _chkPendientesRecepcionar.Checked
                part.CargarPedidos()
                Return True
            Else
                Return False
            End If
        End Function

        ''' Revisado por: blp. Fecha: 08/03/2012
        ''' <summary>
        ''' Obtiene los valores de propiedad de un control WebPart correspondientes a su control EditorPart asociado.
        ''' </summary>
        ''' <remarks>Llamada desde el webpart. MÃ¡x. 0.3 seg.</remarks>
        Public Overrides Sub SyncChanges()
            Dim part As FSEPWebPartPedidos = CType(WebPartToEdit, FSEPWebPartPedidos)
            txtTitulo.Text = part.Title
            txtAncho.Text = part.Width.Value.ToString()
            txtNumPedidos.Text = part.mNumLineas
            For Each x As ListItem In chkFiltroEstado.Items
                x.Selected = (CType(x.Value, Integer) And part.mFiltroEstado) <> FiltroEstadoPedido.Ninguno
            Next
            chkPendientesAprobar.Checked = part._bPendientesAprobar
            chkPendientesRecepcionar.Checked = part._bPendientesRecepcionar
        End Sub

        Private ReadOnly Property txtTitulo() As TextBox
            Get
                EnsureChildControls()
                Return _txtTitulo
            End Get
        End Property

        Private ReadOnly Property txtAncho() As TextBox
            Get
                EnsureChildControls()
                Return _txtAncho
            End Get
        End Property

        Private ReadOnly Property txtNumPedidos() As TextBox
            Get
                EnsureChildControls()
                Return _txtNumPedidos
            End Get
        End Property

        Private ReadOnly Property chkFiltroEstado() As CheckBoxList
            Get
                EnsureChildControls()
                Return _chkFiltroEstado
            End Get
        End Property

        Private ReadOnly Property chkPendientesAprobar() As CheckBox
            Get
                EnsureChildControls()
                Return _chkPendientesAprobar
            End Get
        End Property

        Private ReadOnly Property chkPendientesRecepcionar() As CheckBox
            Get
                EnsureChildControls()
                Return _chkPendientesRecepcionar
            End Get
        End Property

    End Class

    <Personalizable(), WebBrowsable(), DefaultValue(1023)> _
    Public Property FiltroEstado() As FiltroEstadoPedido
        Get
            Return mFiltroEstado
        End Get
        Set(ByVal value As FiltroEstadoPedido)
            mFiltroEstado = value
        End Set
    End Property

    <Personalizable(), WebBrowsable(), DefaultValue(5)> _
    Public Property NumLineas() As Integer
        Get
            Return mNumLineas
        End Get
        Set(ByVal value As Integer)
            mNumLineas = value
        End Set
    End Property

    ''' <summary>
    ''' Devuelve el idioma de la página que alberga el control.
    ''' </summary>
    ''' <returns>El código de idioma.</returns>
    Private Function Idioma() As String
        If Not DesignMode Then
            If Me.Page Is Nothing AndAlso TypeOf Me.Page Is FSNServer.IFSNPage Then
                Return CType(Me.Page, FSNServer.IFSNPage).Idioma
            Else
                For Each x As String In HttpContext.Current.Session.Keys
                    If TypeOf HttpContext.Current.Session(x) Is FSNServer.User Then
                        Return CType(HttpContext.Current.Session(x), FSNServer.User).Idioma
                    End If
                Next
                Return String.Empty
            End If
        Else
            Return String.Empty
        End If
    End Function

    ''' Revisado por: blp. Fecha: 08/03/2012
    ''' <summary>
    ''' Carga el texto cuyo Ã­ndice se pasa como parÃ¡metro
    ''' </summary>
    ''' <param name="Index">Ãndice del texto que queremos recuperar</param>
    ''' <returns>El texto solicitado</returns>
    ''' <remarks>Llamada desde el CreateChildControls y RenderContent. MÃ¡x. 0,2 seg.</remarks>
    Public ReadOnly Property Textos(ByVal Index As Short) As String
        Get
            ModuloIdioma = TiposDeDatos.ModulosIdiomas.WebpartPedidos
            If Idioma() <> _sIdioma Then
                HttpContext.Current.Cache.Remove("Textos_" & _sIdioma & "_" & Me.ModuloIdioma)
                _Textos = Nothing
            End If
            If _Textos Is Nothing Then
                If Not CType(HttpContext.Current.Cache("Textos_" & _sIdioma & "_" & Me.ModuloIdioma), DataSet) Is Nothing Then _
                    _Textos = CType(HttpContext.Current.Cache("Textos_" & _sIdioma & "_" & Me.ModuloIdioma), DataSet)
            End If
            If _Textos IsNot Nothing AndAlso _Textos.Tables(0).Rows(Index).Item(1) IsNot Nothing Then
                Return _Textos.Tables(0).Rows(Index).Item(1)
            Else
                If Not DesignMode Then
                    Dim FSNDict As FSNServer.Dictionary = New FSNServer.Dictionary()
                    _sIdioma = Idioma()
                    FSNDict.LoadData(TiposDeDatos.ModulosIdiomas.WebpartPedidos, _sIdioma)
                    _Textos = FSNDict.Data
                    HttpContext.Current.Cache.Insert("Textos_" & _sIdioma.ToString() & "_" & Me.ModuloIdioma, _Textos, Nothing, Caching.Cache.NoAbsoluteExpiration, New TimeSpan(0, ConfigurationManager.AppSettings("TiempoExpiracionCacheMin"), 0))
                    Return _Textos.Tables(0).Rows(Index).Item(1)
                Else
                    Return String.Empty
                End If
            End If
        End Get
    End Property

    Public Property ModuloIdioma() As FSNLibrary.TiposDeDatos.ModulosIdiomas
        Get
            Return _ModuloIdioma
        End Get
        Set(ByVal Value As FSNLibrary.TiposDeDatos.ModulosIdiomas)
            If _ModuloIdioma <> Value Then _Textos = Nothing
            _ModuloIdioma = Value
        End Set
    End Property

    Public Overrides Property Title() As String
        Get
            If String.IsNullOrEmpty(MyBase.Title) Then
                If String.IsNullOrEmpty(sTitulo) Then
                    If Me.Page Is Nothing Then
                        MyBase.Title = Textos(0)
                        sTitulo = MyBase.Title
                    Else
                        sTitulo = Textos(0)
                    End If
                End If
                Return sTitulo
            Else
                sTitulo = MyBase.Title
                Return MyBase.Title
            End If
        End Get
        Set(ByVal value As String)
            MyBase.Title = value
        End Set
    End Property

    ''' <summary>
    ''' Valor del checkBox que se muestra en la pantalla de edicion del webpart, "Pedidos Pendientes de Aprobar por mí
    ''' El único motivo por el que se crea esta propiedad es para que el valor de _bPendientesAprobar se guarde en bdd 
    ''' (a través de personalization, que se activa mediante el atributo Personalizable de abajo y la etiqueta personalization en el web.config)
    ''' </summary>
    <Personalizable(), WebBrowsable(), DefaultValue(1023)> _
    Public Property PendientesAprobar() As Boolean
        Get
            Return _bPendientesAprobar
        End Get
        Set(ByVal value As Boolean)
            _bPendientesAprobar = value
        End Set
    End Property

    ''' <summary>
    ''' Valor del checkBox que se muestra en la pantalla de edicion del webpart, "Pedidos Pendientes de Recepcionar por mí
    ''' El único motivo por el que se crea esta propiedad es para que el valor de _bPendientesrecepcionar se guarde en bdd 
    ''' (a través de personalization, que se activa mediante el atributo Personalizable de abajo y la etiqueta personalization en el web.config)
    ''' </summary>
    <Personalizable(), WebBrowsable(), DefaultValue(1023)> _
    Public Property PendientesRecepcionar() As Boolean
        Get
            Return _bPendientesRecepcionar
        End Get
        Set(ByVal value As Boolean)
            _bPendientesRecepcionar = value
        End Set
    End Property

    ''' <summary>
    ''' Carga los datos por defecto para el control
    ''' </summary>
    Public Sub New()
        mNumLineas = 5
        mFiltroEstado = 1023
        Me.Width = Unit.Pixel(335)
    End Sub

    ''' Revisado por: blp. Fecha: 08/03/2012
    ''' <summary>
    ''' Lo llama el marco de trabajo de las páginas ASP.NET para indicar a los controles de servidor que utilizan la implementación basada en la composición que creen los controles secundarios que contengan como forma de preparar la devolución o representación de los datos.
    ''' </summary>
    ''' <remarks>Llamada desde el webpart. Máx. 0,3 seg.</remarks>
    Protected Overrides Sub CreateChildControls()
        Controls.Clear()
        Dim img As HtmlImage = New HtmlImage()
        img.Src = Me.Page.ClientScript.GetWebResourceUrl(GetType(FSEPWebPartPedidos), "Fullstep.EPWebControls.trans.gif")
        img.Width = 335
        img.Height = 1
        Me.Controls.Add(img)
        Dim tabla As HtmlTable = New HtmlTable()
        tabla.Width = "100%"
        tabla.Height = "55px"
        Dim fila As HtmlTableRow = New HtmlTableRow()
        fila.Height = "55px"
        Dim celda As HtmlTableCell = New HtmlTableCell()
        celda.Width = "90%"
        lblTitulo = New Label()
        lblTitulo.CssClass = "Rotulo"
        celda.Controls.Add(lblTitulo)
        celda.Controls.Add(New LiteralControl("<br/>"))
        lblNumPedidos = New Label()
        lblNumPedidos.CssClass = "EtiquetaGrande"
        celda.Controls.Add(lblNumPedidos)
        lblPedidos = New Label()
        lblPedidos.CssClass = "EtiquetaGrande"
        celda.Controls.Add(lblPedidos)
        fila.Cells.Add(celda)
        celda = New HtmlTableCell
        celda.Align = "right"
        celda.VAlign = "bottom"
        img.Width = 125
        celda.Controls.Add(img)
        celda.Style.Add("background-image", Me.Page.ClientScript.GetWebResourceUrl(GetType(FSEPWebPartPedidos), "Fullstep.EPWebControls.camion.jpg"))
        celda.Style.Add("background-position", "top left")
        celda.Style.Add("background-repeat", "no-repeat")
        lnkVerTodos = New FSNWebControls.FSNHyperlink()
        celda.Controls.Add(lnkVerTodos)
        fila.Cells.Add(celda)
        tabla.Rows.Add(fila)
        fila = New HtmlTableRow
        celda = New HtmlTableCell
        celda.ColSpan = 2
        lstPedidos = New DataList()
        lstPedidos.Width = Unit.Percentage(100)
        lstPedidos.ItemTemplate = New DataListTemplate()
        celda.Controls.Add(lstPedidos)
        fila.Cells.Add(celda)
        tabla.Rows.Add(fila)
        Me.Controls.Add(tabla)

        lblTitulo.Text = Textos(0)
        lblPedidos.Text = " " & Textos(1)
        lnkVerTodos.Text = Textos(2)
        lnkVerTodos.Text = Textos(19)

    End Sub

    ''' <summary>
    ''' Envía el contenido del control de servidor a un objeto HtmlTextWriter que se proporciona y almacena la información de seguimiento sobre el control si dicho seguimiento está habilitado.
    ''' </summary>
    ''' <param name="writer">Objeto HTmlTextWriter que recibe el contenido del control.</param>
    Public Overrides Sub RenderControl(ByVal writer As System.Web.UI.HtmlTextWriter)
        If DesignMode Then
            lblTitulo.Text = "Pedidos en trámite"
            lblNumPedidos.Text = "4 pedidos"
            lnkVerTodos.Text = "Ver Todos"
            Dim lista As New List(Of mOrden)
            For i As Integer = 1 To 5
                Dim mo As New mOrden()
                mo.Empresa = "Empresa " & i.ToString()
                mo.Pedido = "1/" & i.ToString() & "/" & Year(Now)
                mo.Proveedor = "Proveedor " & i.ToString()
                mo.Estado = "Pedido emitido al proveedor"
                mo.Importe = CDbl(325.47).ToString("0.00") & " &euro;"
                lista.Add(mo)
            Next
            lstPedidos.DataSource = lista
            lstPedidos.DataBind()
        End If
        MyBase.RenderControl(writer)
    End Sub

    ''' Revisado por: blp. Fecha: 05/03/2012
    ''' <summary>
    ''' Carga los pedidos en el DataList del control.
    ''' </summary>
    ''' <remarks>Llamadas desde: FSEPWebPartpedidos_PreRender. Máx. 0,2 seg.</remarks>
    Private Sub CargarPedidos()
        If Not Me.Page Is Nothing AndAlso TypeOf Me.Page Is FSNServer.IFSNPage Then
            Dim iLineas As Integer = mNumLineas
            Dim lista As New List(Of mOrden)
            Dim pag As FSNServer.IFSNPage = CType(Me.Page, FSNServer.IFSNPage)
            Dim srv As New FSNServer.Root
            Dim ds As DataSet
            Dim _FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim oCOrdenes As FSNServer.COrdenes = _FSNServer.Get_Object(GetType(FSNServer.COrdenes))
            ds = oCOrdenes.BuscarPorAprovisionador(mFiltroEstado, pag.Usuario.CodPersona, _
                    pag.Usuario.PermisoRecepcionarPedidosCCImputables, pag.Usuario.RecepcionVerPedidosCCImputables, _
                    iLineas, _bPendientesAprobar, _bPendientesRecepcionar)
            If ds.Tables.Count > 0 Then
                For Each fila As DataRow In ds.Tables(0).Rows
                    Dim mo As New mOrden()
                    mo.Id = fila.Item("ID")
                    mo.Empresa = fila.Item("DENEMPRESA")
                    mo.Pedido = fila.Item("ANYO") & "/" & fila.Item("NUMPEDIDO") & "/" & fila.Item("NUMORDEN")
                    mo.Proveedor = fila.Item("PROVEDEN")
                    mo.CodEstado = fila.Item("EST")
                    Select Case fila.Item("EST")
                        Case 0, 1, 2, 3, 4, 5, 6
                            mo.Estado = Textos(3 + CType(fila.Item("EST"), Integer))
                        Case 20, 21, 22
                            mo.Estado = Textos(CType(fila.Item("EST"), Integer) - 10)
                    End Select
                    mo.Importe = FSNLibrary.FormatNumber(CType(fila.Item("IMPORTE") * fila.Item("CAMBIO"), Double), pag.Usuario.NumberFormat) & " " & fila.Item("MON")
                    lista.Add(mo)
                Next
            End If
            lstPedidos.DataSource = lista
            lstPedidos.DataBind()
            iNumPedidos = iLineas
            lblNumPedidos.Text = iNumPedidos.ToString()
            If Not DesignMode() Then
                lnkVerTodos.NavigateUrl = ConfigurationManager.AppSettings("rutaEP") & "seguimiento.aspx?filtro=" & mFiltroEstado
                lnkVerTodos.Attributes.Add("onclick", "TempCargando();")
            End If
        End If
    End Sub

    ''' <summary>
    ''' Devuelve una colección de controles EditorPart personalizados que pueden utilizarse para editar un control WebPart cuando se encuentra en modo de edición.
    ''' </summary>
    ''' <returns>Una colección EditorPartCollection que contiene controles EditorPart personalizados asociados a un control WebPart.</returns>
    Public Overrides Function CreateEditorParts() As System.Web.UI.WebControls.WebParts.EditorPartCollection
        Dim editorArray As New ArrayList()
        Dim edPart As New ControlEditor()
        edPart.ID = Me.ID & "_editor1"
        editorArray.Add(edPart)
        Dim editorParts As New EditorPartCollection(editorArray)
        Return editorParts
    End Function

    Public Overrides ReadOnly Property WebBrowsableObject() As Object
        Get
            Return Me
        End Get
    End Property

    ''' <summary>
    ''' Registra el control como un control cuyo estado se debe conservar.
    ''' </summary>
    ''' <param name="e">Objeto EventArgs que contiene los datos del evento.</param>
    Protected Overrides Sub OnInit(ByVal e As System.EventArgs)
        MyBase.OnInit(e)
        Page.RegisterRequiresControlState(Me)
    End Sub

    ''' Revisado por: blp. Fecha:01/03/2012
    ''' <summary>
    ''' Guarda los cambios de estado del control.
    ''' </summary>
    ''' <returns>Devuelve el estado actual del control de servidor. Máx. 0,1 seg.</returns>
    Protected Overrides Function SaveControlState() As Object
        Dim obj As Object = MyBase.SaveControlState()
        obj = _sIdioma
        Return obj
    End Function

    ''' <summary>
    ''' Restaura información de estado del control.
    ''' </summary>
    ''' <param name="state">Object que representa el estado del control que se va a restaurar.</param>
    Protected Overrides Sub LoadControlState(ByVal state As Object)
        If (state IsNot Nothing) Then
            _sIdioma = TryCast(state, String)
        End If
    End Sub

    Private Sub FSEPWebPartPedidos_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not DesignMode() AndAlso Not CType(Page, FSNServer.IFSNPage).ScriptMgr.IsInAsyncPostBack Then
            EnsureChildControls()
            CargarPedidos()
        End If
    End Sub
End Class
