﻿Imports System.Configuration
Imports System.Drawing.Design
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.Design
Imports System.Web.UI.HtmlControls
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.ComponentModel

Public Class FSEPWebPartCesta
    Inherits WebPart

    Private Class ControlEditor
        Inherits EditorPart

        Private _txtTitulo As TextBox

        ''' <summary>
        ''' Lo llama el marco de trabajo de las páginas ASP.NET para indicar a los controles de servidor que utilizan la implementación basada en la composición que creen los controles secundarios que contengan como forma de preparar la devolución o representación de los datos.
        ''' </summary>
        Protected Overrides Sub CreateChildControls()
            Dim part As FSEPWebPartCesta = CType(WebPartToEdit, FSEPWebPartCesta)
            Controls.Clear()
            _txtTitulo = New TextBox()
            _txtTitulo.Width = Unit.Pixel(250)
            _txtTitulo.Text = part.Title
            Controls.Add(_txtTitulo)
        End Sub

        ''' <summary>
        ''' Presenta el contenido del control en el sistema de escritura especificado. Este método lo usan principalmente los programadores de controles. 
        ''' </summary>
        ''' <param name="writer">HtmlTextWriter que representa la secuencia de salida para presentar contenido HTML en el cliente.</param>
        Protected Overrides Sub RenderContents(ByVal writer As System.Web.UI.HtmlTextWriter)
            Dim part As FSEPWebPartCesta = CType(WebPartToEdit, FSEPWebPartCesta)
            writer.Write("<b>" & part.Textos(3) & ":</b> ")
            _txtTitulo.RenderControl(writer)
            writer.WriteBreak()
        End Sub

        ''' <summary>
        ''' Guarda los valores de un control EditorPart en las propiedades correspondientes del control WebPart asociado.
        ''' </summary>
        ''' <returns>Es true si la acción de guardar los valores del control EditorPart en el control WebPart se realiza correctamente; en caso contrario (si se produce un error), es false.</returns>
        Public Overrides Function ApplyChanges() As Boolean
            Dim part As FSEPWebPartCesta = CType(WebPartToEdit, FSEPWebPartCesta)
            part.Title = _txtTitulo.Text
            Return True
        End Function

        ''' <summary>
        ''' Obtiene los valores de propiedad de un control WebPart correspondientes a su control EditorPart asociado.
        ''' </summary>
        Public Overrides Sub SyncChanges()
            Dim part As FSEPWebPartCesta = CType(WebPartToEdit, FSEPWebPartCesta)
            txtTitulo.Text = part.Title
        End Sub

        Private ReadOnly Property txtTitulo() As TextBox
            Get
                EnsureChildControls()
                Return _txtTitulo
            End Get
        End Property

    End Class

    Private lblTitulo As Label
    Private lblImporte As Label
    Private lblNumArticulos As Label
    Private lblArticulos As Label
    Private lnkVerCesta As FSNWebControls.FSNHyperlink
    Private celdafondo As HtmlTableCell
    Private updPanel As UpdatePanel
    Private sTitulo As String
    Private _Textos(3) As String
    Private _sIdioma As String

    ''' <summary>
    ''' Devuelve el idioma de la página que alberga el control.
    ''' </summary>
    ''' <returns>El código de idioma.</returns>
    Private Function Idioma() As String
        If Not DesignMode Then
            If Me.Page Is Nothing AndAlso TypeOf Me.Page Is FSNServer.IFSNPage Then
                Return CType(Me.Page, FSNServer.IFSNPage).Idioma
            Else
                For Each x As String In HttpContext.Current.Session.Keys
                    If TypeOf HttpContext.Current.Session(x) Is FSNServer.User Then
                        Return CType(HttpContext.Current.Session(x), FSNServer.User).Idioma
                    End If
                Next
                Return String.Empty
            End If
        Else
            Return String.Empty
        End If
    End Function

    Private ReadOnly Property Textos(ByVal Index As Short) As String
        Get
            If String.IsNullOrEmpty(_Textos(Index)) Or Idioma() <> _sIdioma Then
                If Not DesignMode Then
                    Dim FSNDict As FSNServer.Dictionary = New FSNServer.Dictionary()
                    _sIdioma = Idioma()
                    FSNDict.LoadData(TiposDeDatos.ModulosIdiomas.Webparts, _sIdioma)
                    For i As Short = 0 To 2
                        _Textos(i) = FSNDict.Data.Tables(0).Rows(i + 23).Item(1)
                    Next
                    _Textos(3) = FSNDict.Data.Tables(0).Rows(19).Item(1)
                    Return _Textos(Index)
                Else
                    Select Case Index
                        Case 1
                            Return "Artículos"
                        Case 2
                            Return "Ver Cesta"
                        Case 3
                            Return "Título"
                        Case Else
                            Return "Cesta Virtual"
                    End Select
                End If
            Else
                Return _Textos(Index)
            End If
        End Get
    End Property

    Public Overrides Property Title() As String
        Get
            If String.IsNullOrEmpty(MyBase.Title) Then
                If String.IsNullOrEmpty(sTitulo) Then
                    If Me.Page Is Nothing And String.IsNullOrEmpty(_Textos(0)) Then
                        MyBase.Title = Textos(0)
                        sTitulo = MyBase.Title
                    Else
                        sTitulo = _Textos(0)
                    End If
                End If
                Return sTitulo
            Else
                sTitulo = MyBase.Title
                Return MyBase.Title
            End If
        End Get
        Set(ByVal value As String)
            MyBase.Title = value
        End Set
    End Property

    <Browsable(True), Bindable(True), _
    Category("Appearance"), Description("Imagen para la cesta de la compra."), _
    Editor(GetType(ImageUrlEditor), GetType(UITypeEditor)), UrlProperty(), _
    Themeable(True)> _
    Public Property ImagenCesta() As String
        Get
            Dim s As String = CStr(ViewState("ImagenCesta"))
            If s Is Nothing Then s = String.Empty
            Return s
        End Get
        Set(ByVal value As String)
            ViewState("ImagenCesta") = value
        End Set
    End Property

    <Browsable(True), Bindable(True), _
    Category("Appearance"), Description("Imagen para mostrar durante la actualización."), _
    Editor(GetType(ImageUrlEditor), GetType(UITypeEditor)), UrlProperty(), _
    Themeable(True)> _
    Public Property ImagenCargando() As String
        Get
            Dim s As String = CStr(ViewState("ImagenCargando"))
            If s Is Nothing Then s = String.Empty
            Return s
        End Get
        Set(ByVal value As String)
            ViewState("ImagenCargando") = value
        End Set
    End Property

    ''' <summary>
    ''' Lo llama el marco de trabajo de las páginas ASP.NET para indicar a los controles de servidor que utilizan la implementación basada en la composición que creen los controles secundarios que contengan como forma de preparar la devolución o representación de los datos.
    ''' </summary>
    Protected Overrides Sub CreateChildControls()
        Controls.Clear()
        Dim tabla As HtmlTable = New HtmlTable()
        tabla.Width = "100%"
        tabla.Height = "55px"
        Dim fila As HtmlTableRow = New HtmlTableRow()
        fila.Height = "55px"
        Dim celda As HtmlTableCell = New HtmlTableCell()
        celda.Align = "left"
        lblTitulo = New Label()
        lblTitulo.CssClass = "Rotulo"
        celda.Controls.Add(lblTitulo)
        celda.Controls.Add(New LiteralControl("<br/>"))
        lblImporte = New Label()
        lblImporte.CssClass = "EtiquetaGrande"
        celda.Controls.Add(lblImporte)
        Dim espacio As New Literal()
        espacio.Text = "&nbsp;"
        celda.Controls.Add(espacio)
        lblNumArticulos = New Label()
        celda.Controls.Add(lblNumArticulos)
        espacio = New Literal()
        espacio.Text = "&nbsp;"
        celda.Controls.Add(espacio)
        lblArticulos = New Label()
        celda.Controls.Add(lblArticulos)
        fila.Cells.Add(celda)
        celdafondo = New HtmlTableCell
        celdafondo.Align = "right"
        celdafondo.VAlign = "bottom"
        celdafondo.Width = "100px"
        If DesignMode Then
            celdafondo.Style.Add("background-image", "images/cesta.gif")
        Else
            If CStr(ViewState("ImagenCesta")) <> String.Empty Then
                celdafondo.Style.Add("background-image", Replace(CStr(ViewState("ImagenCesta")), "~/", HttpContext.Current.Request.ApplicationPath & "/"))
            Else
                celdafondo.Style.Add("background-image", Me.Page.ClientScript.GetWebResourceUrl(GetType(FSEPWebPartCesta), "Fullstep.EPWebControls.cesta.gif"))
            End If
        End If
        celdafondo.Style.Add("background-position", "top left")
        celdafondo.Style.Add("background-repeat", "no-repeat")
        lnkVerCesta = New FSNWebControls.FSNHyperlink()
        lnkVerCesta.NavigateUrl = ConfigurationManager.AppSettings("rutaEP") & "Cesta.aspx"
        celdafondo.Controls.Add(lnkVerCesta)
        fila.Cells.Add(celdafondo)
        tabla.Rows.Add(fila)
        updPanel = New UpdatePanel
        updPanel.UpdateMode = UpdatePanelUpdateMode.Conditional
        updPanel.ContentTemplateContainer.Controls.Add(tabla)
        Me.Controls.Add(updPanel)

        lblTitulo.Text = Textos(0)
        lblArticulos.Text = Textos(1)
        lnkVerCesta.Text = Textos(2)
        sTitulo = Textos(0)

    End Sub

    ''' <summary>
    ''' Carga el importe y nº de artículos que existen en la cesta del usuario actualmente
    ''' </summary>
    ''' <remarks>
    ''' Llamadas desde: ActualizarDatos, Actualizar
    ''' </remarks>
    Private Sub CargarCesta()
        If Not Me.Page Is Nothing AndAlso TypeOf Me.Page Is FSNServer.IFSNPage Then
            Dim pag As FSNServer.IFSNPage = CType(Me.Page, FSNServer.IFSNPage)
            Dim _FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim oUser As FSNServer.User = _FSNServer.Get_Object(GetType(FSNServer.User))
            Dim dr As DataRow = oUser.InfoCesta(pag.Usuario.Cod)
            Dim dImp As Double
            If dr.Item("IMPORTE") > 0 Then
                dImp = CDbl(dr.Item("IMPORTE") * dr.Item("EQUIV"))
            Else
                dImp = 0
            End If
            lblImporte.Text = FSNLibrary.FormatNumber(dImp, pag.Usuario.NumberFormat) & " " & dr.Item("MON").ToString()
            lblNumArticulos.Text = dr.Item("NUMARTICULOS").ToString()
        ElseIf DesignMode Then
            lblImporte.Text = "666 &euro;"
            lblNumArticulos.Text = "2"
        End If
    End Sub

    ''' <summary>
    ''' Refresca los datos de la cesta.
    ''' </summary>
    Public Sub Actualizar()
        CargarCesta()
        updPanel.Update()
    End Sub

    ''' <summary>
    ''' Devuelve una colección de controles EditorPart personalizados que pueden utilizarse para editar un control WebPart cuando se encuentra en modo de edición.
    ''' </summary>
    ''' <returns>Una colección EditorPartCollection que contiene controles EditorPart personalizados asociados a un control WebPart.</returns>
    Public Overrides Function CreateEditorParts() As System.Web.UI.WebControls.WebParts.EditorPartCollection
        Dim editorArray As New ArrayList()
        Dim edPart As New ControlEditor()
        edPart.ID = Me.ID & "_editor1"
        editorArray.Add(edPart)
        Dim editorParts As New EditorPartCollection(editorArray)
        Return editorParts
    End Function

    ''' <summary>
    ''' Registra el control como un control cuyo estado se debe conservar.
    ''' </summary>
    ''' <param name="e">Objeto EventArgs que contiene los datos del evento.</param>
    Protected Overrides Sub OnInit(ByVal e As System.EventArgs)
        MyBase.OnInit(e)
        Page.RegisterRequiresControlState(Me)
    End Sub

    ''' <summary>
    ''' Guarda los cambios de estado del control.
    ''' </summary>
    ''' <returns>Devuelve el estado actual del control de servidor.</returns>
    Protected Overrides Function SaveControlState() As Object
        Dim obj As Object = MyBase.SaveControlState()
        If obj IsNot Nothing Then
            obj = New Pair(obj, _Textos)
        Else
            obj = _Textos
        End If
        obj = New Pair(obj, _sIdioma)
        Return obj
    End Function

    ''' <summary>
    ''' Restaura información de estado del control.
    ''' </summary>
    ''' <param name="state">Object que representa el estado del control que se va a restaurar.</param>
    Protected Overrides Sub LoadControlState(ByVal state As Object)
        If (state IsNot Nothing) Then
            Dim p As Pair = TryCast(state, Pair)
            _sIdioma = p.Second
            state = p.First
            p = TryCast(state, Pair)
            If p IsNot Nothing Then
                MyBase.LoadControlState(p.First)
                _Textos = CType(p.Second, String())
            Else
                If (TypeOf (state) Is String()) Then
                    _Textos = CType(state, String())
                Else
                    MyBase.LoadControlState(state)
                End If
            End If
        End If
    End Sub

    ''' <summary>
    ''' Carga en cliente el recurso FSEPWebPartCesta.js si no está previamente cargado
    ''' </summary>
    ''' <param name="sender">Origen del evento</param>
    ''' <param name="e">EventArgs que contiene datos de eventos.</param>
    ''' <remarks>Se produce cuando el control de servidor se carga en el objeto Page.</remarks>
    Private Sub FSEPWebPartCesta_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.DesignMode AndAlso Not CType(Me.Page, FSNServer.IFSNPage).ScriptMgr.IsInAsyncPostBack Then
            Me.Page.ClientScript.RegisterClientScriptResource(GetType(EPWebControlsClientScript), "Fullstep.EPWebControls.FSEPWebPartCesta.js")
            EnsureChildControls()
            CargarCesta()
        End If
    End Sub


    ''' <summary>
    ''' [Descripción de la funcionalidad]
    ''' </summary>
    ''' <param name="sender">Origen del evento</param>
    ''' <param name="e">EventArgs que contiene datos de eventos.</param>
    ''' <remarks>Se produce una vez que se carga el objeto Control, pero antes de su representación.</remarks>
    Private Sub FSEPWebPartCesta_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If Not Me.DesignMode Then
            Dim sImagenCesta As String
            Dim sImagenCargando As String
            If CStr(ViewState("ImagenCesta")) <> String.Empty Then
                sImagenCesta = Replace(CStr(ViewState("ImagenCesta")), "~/", HttpContext.Current.Request.ApplicationPath & "/")
                sImagenCargando = Replace(CStr(ViewState("ImagenCargando")), "~/", HttpContext.Current.Request.ApplicationPath & "/")
            Else
                sImagenCesta = Me.Page.ClientScript.GetWebResourceUrl(GetType(FSEPWebPartCesta), "Fullstep.EPWebControls.cesta.gif")
                sImagenCargando = Me.Page.ClientScript.GetWebResourceUrl(GetType(FSEPWebPartCesta), "Fullstep.EPWebControls.cargando.gif")
            End If
            Me.Page.ClientScript.RegisterStartupScript(Me.GetType, "RegistrarCesta" & Me.ClientID, _
                "FSNWebPartsCesta.add('" & Me.ClientID & "', $get('" & lblImporte.ClientID & "'), " & _
                    "$get('" & lblNumArticulos.ClientID & "'), $get('" & celdafondo.ClientID & "'), '" & sImagenCesta & "', '" & sImagenCargando & "');", True)
        End If
    End Sub
End Class
