﻿FSNCesta = function(id, spanimporte, spanarticulos, celdafondo, imagencesta, imagencargando) {
    this.id = id;
    this.spanimporte = spanimporte;
    this.spanarticulos = spanarticulos;
    this.celdafondo = celdafondo;
    this.imagencesta = imagencesta;
    this.imagencargando = imagencargando;
}

FSNCesta.prototype = {
    cambiarmodo: function(modo) {
        if (modo == 'actualizando')
            this.celdafondo.style.backgroundImage = 'url(' + this.imagencargando + ')'
        else
            this.celdafondo.style.backgroundImage = 'url(' + this.imagencesta + ')';
    },

    actualizarvalores: function(importe, articulos) {
        this.spanimporte.innerHTML = importe;
        this.spanarticulos.innerHTML = articulos;
    }
}

FSNArrayCestas = function() {
    this.cestas = new Array()
}

FSNArrayCestas.prototype = {
    find: function(id) {
        for (var i = 0; i < this.cestas.length; i++) {
            if (this.cestas[i].id == id) return this.cestas[i];
        }
        return null;
    },

    add: function(id, spanimporte, spanarticulos, celdafondo, imagencesta, imagencargando) {
        var c = this.find(id);
        if (c == null) {
            var cesta = new FSNCesta(id, spanimporte, spanarticulos, celdafondo, imagencesta, imagencargando);
            this.cestas.push(cesta);
        }
        else {
            c.spanimporte = spanimporte;
            c.spanarticulos = spanarticulos;
            c.celdafondo = celdafondo;
            c.imagencesta = imagencesta;
            c.imagencargando = imagencargando;
        }
    },

    remove: function(id) {
        for (var i = this.cestas.length - 1; i >= 0; i--) {
            if (this.cestas[i].id == id) this.cestas.splice(i, 1);
        }
    },

    cambiarmodo: function(modo) {
        for (var i = 0; i < this.cestas.length; i++)
            this.cestas[i].cambiarmodo(modo);
    },

    actualizarvalores: function(importe, articulos) {
        for (var i = 0; i < this.cestas.length; i++)
            this.cestas[i].actualizarvalores(importe, articulos);
    }
}

var FSNWebPartsCesta = new FSNArrayCestas();
