﻿Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Fullstep.FSNServer
Imports Fullstep.FSNWebControls
Imports Fullstep.FSNLibrary
Imports System.Drawing
Imports System.Configuration

Public Class SMPartidasPresupuestarias
    Inherits Panel
    Implements IPostBackEventHandler

#Region "Variables privadas"

    ' Variables privadas para controles
    Private _imgCabecera As HtmlControls.HtmlImage
    Private _lblTitulo As Label
    Private _upTitulo As UpdatePanel
    Private WithEvents _imgCerrar As ImageButton
    Private _lblBuscar As Label
    Private _txtBuscar As TextBox
    Private _imgSiguiente As ImageButton
    Private _lblGestor As Label
    Private _lblGestorDen As Label
    Private _cboGestor As DropDownList
    Private _lblCentro As Label
    Private _txtCentro As TextBox
    Private _imgBuscarCentro As ImageButton
    Private _lblCentroCosteDen As Label
    Private WithEvents _btnBuscar As FSNButton
    Private _lblFechaInicioDesde As Label
    Private _dteFechaInicioDesde As Infragistics.Web.UI.EditorControls.WebDatePicker
    Private _lblFechaHastaFin As Label
    Private _dteFechaHastaFin As Infragistics.Web.UI.EditorControls.WebDatePicker
    Private _chkPartidasNoVigentes As CheckBox
    Private _upParametros As UpdatePanel
    Private _upTreeview As UpdatePanel
    Private _uwtCentrosPartidas As Global.Infragistics.Web.UI.NavigationControls.WebDataTree
    Private _urlImgRoot As String
    Private _urlImgFolder As String
    Private _urlImgCentroCoste As String
    Private _urlImgCandado As String
    Private WithEvents _btnSeleccionar As FSNButton
    Private WithEvents _btnCancelar As FSNButton
    Private WithEvents _pnlCentrosCoste As SMCentrosCoste
    Private _modalCentrosCoste As AjaxControlToolkit.ModalPopupExtender
    Private _empresaOculta As HiddenField

    ' Variables privadas
    Private _Idioma As FSNLibrary.Idioma
    Private _bHayCambiosEnPropiedades As Boolean = False

    ' Variables de propiedades públicas
    Private _sValor As String
    Private _sPRES5 As String
    Private _mFiltroCentroSM As Centro_SM
    Private _mFiltroGestor As String
    Private _sDenPartidas As String
    Private _mCentroSM As Centro_SM
    Private _mPartida As PartidaPRES5
    Private _mEmpresa As Nullable(Of Integer)
    Private _mGestorCod As String

    Private Const _ModuloIdioma As TiposDeDatos.ModulosIdiomas = TiposDeDatos.ModulosIdiomas.Partidas

    Private ReadOnly Property Textos(ByVal iTexto As Integer) As String
        Get
            Dim pag As IFSNPage = CType(Me.Page, IFSNPage)
            If HttpContext.Current.Cache("Textos_" & _Idioma.ToString() & "_" & _ModuloIdioma) Is Nothing Then
                Dim FSNDict As Dictionary
                If pag.FSNServer Is Nothing Then
                    FSNDict = New Dictionary()
                    FSNDict.LoadData(_ModuloIdioma, _Idioma.ToString())
                Else
                    FSNDict = pag.FSNServer.Get_Object(GetType(Dictionary))
                    FSNDict.LoadData(_ModuloIdioma, _Idioma.ToString())
                End If
                HttpContext.Current.Cache.Insert("Textos_" & _Idioma.ToString() & "_" & _ModuloIdioma, FSNDict.Data, Nothing, Caching.Cache.NoAbsoluteExpiration, New TimeSpan(0, ConfigurationManager.AppSettings("TiempoExpiracionCacheMin"), 0))
            End If
            Return HttpContext.Current.Cache("Textos_" & _Idioma.ToString() & "_" & _ModuloIdioma).Tables(0).Rows(iTexto).Item(1)
        End Get
    End Property

    Private ReadOnly Property FSNServer() As FSNServer.Root
        Get
            Return CType(HttpContext.Current.Session("FSN_Server"), FSNServer.Root)
        End Get
    End Property

    Private ReadOnly Property ArbolImputacion() As UONs
        Get
            If HttpContext.Current.Cache("ArbolImputacion_" & Empresa & "_" & CType(Me.Page, FSNServer.IFSNPage).Usuario.Cod & "_" & _Idioma.ToString()) Is Nothing Then
                Dim uons As UONs = Me.FSNServer.Get_Object(GetType(UONs))
                uons.ArbolPartidasImputacion(CType(Me.Page, FSNServer.IFSNPage).Usuario.Cod, _Idioma, Nothing,
                                             CType(Me.Page, FSNServer.IFSNPage).Usuario.PedidosOtrasEmp, Empresa)
                HttpContext.Current.Cache.Insert("ArbolImputacion_" & Empresa & "_" & CType(Me.Page, FSNServer.IFSNPage).Usuario.Cod & "_" & _Idioma.ToString(), uons, Nothing, Caching.Cache.NoAbsoluteExpiration,
                                                 New TimeSpan(0, ConfigurationManager.AppSettings("TiempoExpiracionCacheMin"), 0))
                Return uons
            Else
                Return HttpContext.Current.Cache("ArbolImputacion_" & Empresa & "_" & CType(Me.Page, FSNServer.IFSNPage).Usuario.Cod & "_" & _Idioma.ToString())
            End If
        End Get
    End Property

#End Region

#Region "Gestor"
    ''' <summary>
    ''' Listado de los Gestores para cada una de las partidas a las que el usuario podrá imputar
    ''' EN VEZ DE USAR ESTA PROPIEDAD, PODRÍA DEVOLVERSE EN ArbolImputacion TODA LA INFORMACION DE LAS PARTIDAS PRESUPUESTARIAS, INLCLUIDO EL GESTOR.
    ''' NO LO HACEMOS ASÍ DADO QUE EL GESTOR ES UN DATO NO SIEMPRE NECESARIO Y PORQUE EL STORED QUE SE USA EN ArbolImputacion (FSSM_PARTIDAS_USU)
    ''' ES YA CONSIDERABLEMENTE LENTO. PARECE MÁS EFICIENTE ESTE MODO.
    ''' CASO DE MEJORAR SUSTANCIALMENTE EL RENDIMIENTO DE FSSM_PARTIDAS_USU, CONVIENE CARGAR ALLÍ LOS DATOS DE GESTOR Y USARLOS DIRECTAMENTE,
    ''' SIN CRUZARLOS CON ESTE OBJETO.
    ''' </summary>
    ''' <param name="PRES0">Árbol de Partida Prespuestaria del que queremos recuperar los gestores</param>
    Private ReadOnly Property GestoresPartidasUsu(ByVal PRES0 As String) As PartidasPRES5
        Get
            If PRES0 IsNot Nothing Then
                If HttpContext.Current.Cache("GestoresPartidasUsu_" & CType(Me.Page, FSNServer.IFSNPage).Usuario.Cod & "_" & PRES0) Is Nothing Then
                    Dim oPartidas As PartidasPRES5 = FSNServer.Get_Object(GetType(PartidasPRES5))
                    oPartidas.ObtGestoresPartidasUsu(CType(Me.Page, FSNServer.IFSNPage).Usuario.Cod, PRES0)
                    HttpContext.Current.Cache.Insert("GestoresPartidasUsu_" & CType(Me.Page, FSNServer.IFSNPage).Usuario.Cod & "_" & PRES0, oPartidas, Nothing, Caching.Cache.NoAbsoluteExpiration,
                                                     New TimeSpan(0, ConfigurationManager.AppSettings("TiempoExpiracionCacheMin"), 0))
                End If
                Return CType(HttpContext.Current.Cache("GestoresPartidasUsu_" & CType(Me.Page, FSNServer.IFSNPage).Usuario.Cod & "_" & PRES0), PartidasPRES5)
            Else
                Dim oPartidasVacio As PartidasPRES5 = FSNServer.Get_Object(GetType(PartidasPRES5))
                Return oPartidasVacio
            End If
        End Get
    End Property

    ''' Revisado por: blp. Fecha: 29/06/2012
    ''' <summary>
    ''' Para un objeto partidaPRES5 pasado como parámetro, determina el gestor que tiene asociado
    ''' </summary>
    ''' <param name="Partida">Objeto PartidaPRES5 con los datos de la partida</param>
    ''' <returns>String con el código del gestor</returns>
    ''' <remarks>Llamada desde SMPartidasPresupuestarias.vb. Max. 0,1 seg.</remarks>
    Private Function seleccionaGestor(ByVal Partida As PartidaPRES5) As String
        Return GestoresPartidasUsu(Partida.NIV0).Find(Function(oParPres As FSNServer.PartidaPRES5) oParPres.NIV0 = Partida.NIV0 AndAlso oParPres.NIV1 = Partida.NIV1 AndAlso oParPres.NIV2 = Partida.NIV2 AndAlso oParPres.NIV3 = Partida.NIV3 AndAlso oParPres.NIV4 = Partida.NIV4).GestorCod
    End Function

    ''' Revisado por: blp. Fecha: 29/06/2012
    ''' <summary>
    ''' Para un objeto partidaPRES5 pasado como parámetro, determina el gestor que tiene asociado
    ''' </summary>
    ''' <param name="Partida">Objeto PartidaPRES5 con los datos de la partida</param>
    ''' <returns>String con el nombre y apellido del gestor</returns>
    ''' <remarks>Llamada desde SMPartidasPresupuestarias.vb. Max. 0,1 seg.</remarks>
    Private Function seleccionaGestorDen(ByVal Partida As PartidaPRES5) As String
        Return GestoresPartidasUsu(Partida.NIV0).Find(Function(oParPres As FSNServer.PartidaPRES5) oParPres.NIV0 = Partida.NIV0 AndAlso oParPres.NIV1 = Partida.NIV1 AndAlso oParPres.NIV2 = Partida.NIV2 AndAlso oParPres.NIV3 = Partida.NIV3 AndAlso oParPres.NIV4 = Partida.NIV4).GestorDen
    End Function


    ''' Revisado por: blp. Fecha: 29/06/2012
    ''' <summary>
    ''' Para un objeto partidaPRES5 sin gestor pasado como parámetro, devuelve el mismo objeto con gestor
    ''' </summary>
    ''' <param name="Partida">Objeto PartidaPRES5 con los datos de la partida</param>
    ''' <returns>Objeto partidaPRES5 con los datos de partida y gestor</returns>
    ''' <remarks>Llamada desde SMPartidasPresupuestarias.vb. Max. 0,1 seg.</remarks>
    Private Function seleccionaPartidaGestor(ByVal Partida As PartidaPRES5) As PartidaPRES5
        Dim oPartida As PartidaPRES5 = GestoresPartidasUsu(Partida.NIV0).Find(Function(oParPres As FSNServer.PartidaPRES5) oParPres.NIV0 = Partida.NIV0 AndAlso oParPres.NIV1 = Partida.NIV1 AndAlso oParPres.NIV2 = Partida.NIV2 AndAlso oParPres.NIV3 = Partida.NIV3 AndAlso oParPres.NIV4 = Partida.NIV4)
        If oPartida IsNot Nothing Then
            Partida.GestorCod = oPartida.GestorCod
            Partida.GestorDen = oPartida.GestorDen
        End If
        Return Partida
    End Function

    ''' Revisado por: blp. Fecha: 09/07/2012
    ''' <summary>
    ''' cargar el combo de gestores
    ''' </summary>
    ''' <remarks>Llamada desde SMPartidasPresupuestarias.vb. Max. 0,3 seg.</remarks>
    Private Sub cargarGestoresenCombo()
        If _cboGestor.Visible Then
            Dim query = (From gestores In GestoresPartidasUsu(ArbolPresupuestario)
                         Where gestores.GestorCod IsNot DBNull.Value AndAlso Not String.IsNullOrEmpty(gestores.GestorCod)
                         Group By gestores.GestorCod, gestores.GestorDen Into g = Group
                         Select New With {.Den = GestorDen, .Cod = GestorCod}).ToList
            _cboGestor.DataTextField = "Den"
            _cboGestor.DataValueField = "Cod"
            _cboGestor.DataSource = query
            _cboGestor.DataBind()
            Dim oItemVacio As New ListItem
            oItemVacio.Text = String.Empty
            oItemVacio.Value = String.Empty
            _cboGestor.Items.Insert(0, oItemVacio)
            _upParametros.Update()
        End If
    End Sub
#End Region

#Region "Propiedades públicas"

    Public Property ArbolPresupuestario() As String
        Get
            Return _sPRES5
        End Get
        Set(ByVal value As String)
            _sPRES5 = value
            _bHayCambiosEnPropiedades = True
        End Set
    End Property

    Public Property Valor() As String
        Get
            Return _sValor
        End Get
        Set(ByVal value As String)
            _sValor = value
        End Set
    End Property

    ''' <summary>
    ''' Propiedad que guarda el filtro por centro de coste de las partidas y que formatea los campos de centros de coste del panel
    ''' </summary>
    Public Property FiltroCentroCoste() As Centro_SM
        Get
            Return _mFiltroCentroSM
        End Get
        Set(ByVal value As Centro_SM)
            _mFiltroCentroSM = value
            If _txtCentro IsNot Nothing Then
                If value Is Nothing Then
                    _txtCentro.Visible = True
                    _imgBuscarCentro.Visible = True
                    _txtCentro.Text = String.Empty
                    _lblCentroCosteDen.Visible = False
                Else
                    _txtCentro.Visible = False
                    _imgBuscarCentro.Visible = False
                    _lblCentroCosteDen.Text = If(_mFiltroCentroSM.UONS.UON4 <> String.Empty, _mFiltroCentroSM.UONS.UON4, If(_mFiltroCentroSM.UONS.UON3 <> String.Empty, _mFiltroCentroSM.UONS.UON3, If(_mFiltroCentroSM.UONS.UON2 <> String.Empty, _mFiltroCentroSM.UONS.UON2, If(_mFiltroCentroSM.UONS.UON1 <> String.Empty, _mFiltroCentroSM.UONS.UON1, String.Empty)))) & " - " & _mFiltroCentroSM.Denominacion
                    _lblCentroCosteDen.Visible = True
                End If
                If _txtCentro.Attributes("CENTROSM") IsNot Nothing Then _txtCentro.Attributes("CENTROSM") = String.Empty
                _bHayCambiosEnPropiedades = True
            End If
        End Set
    End Property

    ''' <summary>
    ''' Propiedad que guarda el filtro por gestor de las partidas que se recibe desde la página que use este control (SMPartidasPresupuestarias)
    ''' y que formatea los campos de gestor del panel
    ''' </summary>
    Public Property FiltroGestor() As String
        Get
            Return _mFiltroGestor
        End Get
        Set(ByVal value As String)
            _mFiltroGestor = value
            If _cboGestor IsNot Nothing Then
                If value = String.Empty Then
                    _cboGestor.Visible = True
                    _lblGestorDen.Visible = False
                Else
                    _cboGestor.Visible = False
                    _lblGestorDen.Text = GestoresPartidasUsu(ArbolPresupuestario).Find(Function(oParPres As PartidaPRES5) oParPres.GestorCod = value).GestorDen
                    _lblGestorDen.Attributes.Add("gestorCod", value)
                    _lblGestorDen.Visible = True
                End If
                _bHayCambiosEnPropiedades = True
            End If
        End Set
    End Property

    Public Property DenominacionPartidas() As String
        Get
            Return _sDenPartidas
        End Get
        Set(ByVal value As String)
            _sDenPartidas = value
            If _lblTitulo IsNot Nothing Then
                _lblTitulo.Text = Me.Textos(8) & " " & _sDenPartidas
                _upTitulo.Update()
            End If
        End Set
    End Property

    Public Property OnAceptarClientClick() As String
        Get
            If _btnSeleccionar IsNot Nothing Then
                Return _btnSeleccionar.OnClientClick
            Else
                Return Nothing
            End If
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            _btnSeleccionar.OnClientClick = value
        End Set
    End Property

    Public Property OnCancelarClientClick() As String
        Get
            If _btnCancelar IsNot Nothing Then
                Return _btnCancelar.OnClientClick
            Else
                Return Nothing
            End If
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            _btnCancelar.OnClientClick = value
            _imgCerrar.OnClientClick = value
        End Set
    End Property

    Public ReadOnly Property CentroSM() As Centro_SM
        Get
            Return _mCentroSM
        End Get
    End Property

    Public ReadOnly Property PartidaPresupuestaria() As PartidaPRES5
        Get
            Return _mPartida
        End Get
    End Property

    Public Property Empresa() As Nullable(Of Integer)
        Get
            'EmpresaOculta nos permite conservar el valor de la empresa cuando se usa el buscador en el panel de contratos
            If _mEmpresa Is Nothing Then
                If _empresaOculta.Value <> String.Empty Then
                    _mEmpresa = strToInt(_empresaOculta.Value)
                End If
            End If
            Return _mEmpresa
        End Get
        Set(ByVal value As Nullable(Of Integer))
            _mEmpresa = value
            'Pasamos el valor de la empresa al campo oculto para no perderlo cuando usemos el buscador del panel
            _empresaOculta.Value = IIf(value Is Nothing, String.Empty, value)
            _upParametros.Update()
        End Set
    End Property

    ''' <summary>
    ''' Código del Gestor de la partida que seleccionemos en el panel
    ''' </summary>
    Public ReadOnly Property GestorCod() As String
        Get
            Return _mGestorCod
        End Get
    End Property

#End Region

#Region "Eventos públicos"

    Public Event Aceptar(ByVal sender As Object, ByVal e As EventArgs)
    Public Event Cancelar(ByVal sender As Object, ByVal e As EventArgs)

#End Region

#Region "Carga inicial"

    ''' Revisado por: blp. Fecha:17/07/2012
    ''' <summary>
    ''' Lo llama el marco de trabajo de las páginas ASP.NET para indicar a los controles de servidor que utilizan la implementación basada en la composición que creen los controles secundarios que contengan como forma de preparar la devolución o representación de los datos.
    ''' </summary>
    ''' <remarks>Llamada desde marco de trabajo la página asp.net. Máx. 0,3 seg.</remarks>
    Protected Overrides Sub CreateChildControls()
        Me.Style.Add(HtmlTextWriterStyle.Padding, "6px")
        Me.Style.Add("min-width", "750px")
        Me.Style.Add("max-width", "90%")
        Me.Style.Add("min-height", "250px")

        ' Panel de búsqueda de centros de coste
        _pnlCentrosCoste = New SMCentrosCoste()
        _pnlCentrosCoste.ID = Me.ID & "_pnlCentrosCoste"
        _pnlCentrosCoste.BackColor = Drawing.Color.White
        _pnlCentrosCoste.Style.Add("min-width", "500px")
        _pnlCentrosCoste.Style.Add("max-width", "80%")
        If Not Me.Width.IsEmpty Then _
            _pnlCentrosCoste.Width = New Unit(CInt(Me.Width.Value * 0.8), Me.Width.Type)
        _pnlCentrosCoste.BorderColor = Drawing.Color.DimGray
        _pnlCentrosCoste.BorderWidth = New Unit(1, UnitType.Pixel)
        _pnlCentrosCoste.BorderStyle = WebControls.BorderStyle.Solid
        _pnlCentrosCoste.Style.Add(HtmlTextWriterStyle.Display, "none")
        _pnlCentrosCoste.VerUON = False
        Me.Controls.Add(_pnlCentrosCoste)

        Dim tabla As New HtmlControls.HtmlTable()
        tabla.CellPadding = 4
        tabla.CellSpacing = 0
        tabla.Width = "100%"
        tabla.Border = 0
        Dim fila As New HtmlControls.HtmlTableRow()
        Dim celda As New HtmlControls.HtmlTableCell()
        celda.Width = "65"
        _imgCabecera = New HtmlControls.HtmlImage()
        _imgCabecera.ID = Me.ID & "_imgCabecera"
        _imgCabecera.Border = 0
        celda.Controls.Add(_imgCabecera)
        fila.Cells.Add(celda)
        celda = New HtmlControls.HtmlTableCell()
        _upTitulo = New UpdatePanel()
        _upTitulo.ID = Me.ID & "_upTitulo"
        _upTitulo.UpdateMode = UpdatePanelUpdateMode.Conditional
        _lblTitulo = New Label()
        _lblTitulo.ID = Me.ID & "_lblTitulo"
        _lblTitulo.CssClass = "RotuloGrande"
        _upTitulo.ContentTemplateContainer.Controls.Add(_lblTitulo)
        celda.Controls.Add(_upTitulo)
        fila.Cells.Add(celda)
        celda = New HtmlControls.HtmlTableCell()
        celda.Align = "right"
        celda.VAlign = "top"
        _imgCerrar = New ImageButton()
        _imgCerrar.ID = Me.ID & "_imgCerrar"
        _imgCerrar.SkinID = "Cerrar"
        celda.Controls.Add(_imgCerrar)
        fila.Cells.Add(celda)
        tabla.Rows.Add(fila)
        Me.Controls.Add(tabla)
        Me.Controls.Add(New LiteralControl("<br/>"))

        _upParametros = New UpdatePanel()
        _upParametros.ID = Me.ID & "_upParametros"
        _upParametros.UpdateMode = UpdatePanelUpdateMode.Conditional
        tabla = New HtmlControls.HtmlTable()
        tabla.CellPadding = 4
        tabla.CellSpacing = 0
        tabla.Width = "100%"
        tabla.Border = 0
        fila = New HtmlControls.HtmlTableRow()
        celda = New HtmlControls.HtmlTableCell()
        _lblBuscar = New Label()
        _lblBuscar.ID = Me.ID & "_lblBuscar"
        _lblBuscar.CssClass = "Etiqueta"
        celda.Controls.Add(_lblBuscar)
        fila.Cells.Add(celda)
        celda = New HtmlControls.HtmlTableCell()
        celda.ColSpan = 4
        _txtBuscar = New TextBox()
        _txtBuscar.ID = Me.ID & "_txtBuscar"
        _txtBuscar.Width = New Unit(350, UnitType.Pixel)
        celda.Controls.Add(_txtBuscar)
        celda.Controls.Add(New LiteralControl("&nbsp;&nbsp;"))
        _imgSiguiente = New ImageButton()
        _imgSiguiente.ID = Me.ID & "_imgSiguiente"
        _imgSiguiente.Style.Add(HtmlTextWriterStyle.Visibility, "hidden")
        celda.Controls.Add(_imgSiguiente)
        fila.Cells.Add(celda)
        tabla.Rows.Add(fila)
        'GESTOR
        fila = New HtmlControls.HtmlTableRow()
        celda = New HtmlControls.HtmlTableCell()
        _lblGestor = New Label()
        _lblGestor.ID = Me.ID & "_lblGestor"
        _lblGestor.CssClass = "Etiqueta"
        celda.Controls.Add(_lblGestor)
        fila.Cells.Add(celda)
        celda = New HtmlControls.HtmlTableCell()
        celda.ColSpan = 3
        _cboGestor = New DropDownList()
        _cboGestor.ID = Me.ID & "_cboGestor"
        _cboGestor.Width = New Unit(350, UnitType.Pixel)
        celda.Controls.Add(_cboGestor)
        _lblGestorDen = New Label()
        _lblGestorDen.ID = Me.ID & "_lblGestorDen"
        _lblGestorDen.CssClass = "Etiqueta"
        celda.Controls.Add(_lblGestorDen)
        fila.Cells.Add(celda)
        tabla.Rows.Add(fila)

        fila = New HtmlControls.HtmlTableRow()
        celda = New HtmlControls.HtmlTableCell()
        _lblCentro = New Label()
        _lblCentro.ID = Me.ID & "_lblCentro"
        _lblCentro.CssClass = "Etiqueta"
        celda.Controls.Add(_lblCentro)
        fila.Cells.Add(celda)
        celda = New HtmlControls.HtmlTableCell()
        celda.ColSpan = 3
        _txtCentro = New TextBox()
        _txtCentro.ID = Me.ID & "_txtCentro"
        _txtCentro.Width = New Unit(350, UnitType.Pixel)
        _txtCentro.ReadOnly = True
        celda.Controls.Add(_txtCentro)
        celda.Controls.Add(New LiteralControl("&nbsp;&nbsp;"))
        _imgBuscarCentro = New ImageButton()
        _imgBuscarCentro.ID = Me.ID & "_imgBuscarCentro"
        celda.Controls.Add(_imgBuscarCentro)
        _lblCentroCosteDen = New Label()
        _lblCentroCosteDen.ID = Me.ID & "_lblCentroCosteDen"
        _lblCentroCosteDen.CssClass = "Etiqueta"
        celda.Controls.Add(_lblCentroCosteDen)
        fila.Cells.Add(celda)
        celda = New HtmlControls.HtmlTableCell()
        _btnBuscar = New FSNButton()
        _btnBuscar.ID = Me.ID & "_btnBuscar"
        _btnBuscar.Alineacion = FSNTipos.Alineacion.Left
        celda.Controls.Add(_btnBuscar)
        fila.Cells.Add(celda)
        tabla.Rows.Add(fila)
        fila = New HtmlControls.HtmlTableRow()
        celda = New HtmlControls.HtmlTableCell()
        _lblFechaInicioDesde = New Label()
        _lblFechaInicioDesde.ID = Me.ID & "_lblFechaInicioDesde"
        _lblFechaInicioDesde.CssClass = "Etiqueta"
        celda.Controls.Add(_lblFechaInicioDesde)
        fila.Cells.Add(celda)
        celda = New HtmlControls.HtmlTableCell()
        _dteFechaInicioDesde = New Infragistics.Web.UI.EditorControls.WebDatePicker()
        '_dteFechaInicioDesde.DropDownStyle.CssClass = "WebDateChooserAbsoluto"

        _dteFechaInicioDesde.ID = Me.ID & "_dteFechaInicioDesde"
        _dteFechaInicioDesde.EditMode = Infragistics.Web.UI.EditorControls.DatePickerEditMode.KeyboardAndCalendar
        '_dteFechaInicioDesde.edit= True
        'Si la llamada viene de EmisionPedido, vamos a asociar al evento AfterDrop del WebDateChooser
        'una función javascript. Esta función está en EPWeb\js\jsCalendario.js.
        'Para ponerla en todos los lugares donde se use este control, habría que comentar este 'if' dejando sólo la asociación del evento a la función
        If Me.Page.ToString.IndexOf("emisionpedido") <> -1 Then
            _dteFechaInicioDesde.ClientSideEvents.CalendarOpened = "cboStartDate_AfterDrop"
        End If
        _dteFechaInicioDesde.SkinID = "calendario"
        celda.Controls.Add(_dteFechaInicioDesde)
        fila.Cells.Add(celda)
        celda = New HtmlControls.HtmlTableCell()
        celda.NoWrap = True
        _lblFechaHastaFin = New Label()
        _lblFechaHastaFin.ID = Me.ID & "_lblFechaHastaFin"
        _lblFechaHastaFin.CssClass = "Etiqueta"
        celda.Controls.Add(_lblFechaHastaFin)
        fila.Cells.Add(celda)
        celda = New HtmlControls.HtmlTableCell()
        _dteFechaHastaFin = New Infragistics.Web.UI.EditorControls.WebDatePicker()
        _dteFechaHastaFin.ID = Me.ID & "_dteFechaHastaFin"
        _dteFechaHastaFin.EditMode = Infragistics.Web.UI.EditorControls.DatePickerEditMode.KeyboardAndCalendar
        '_dteFechaHastaFin.Editable = True
        'Si la llamada viene de EmisionPedido, vamos a asociar al evento AfterDrop del WebDateChooser
        'una función javascript. Esta función está en EPWeb\js\jsCalendario.js.
        'Para ponerla en todos los lugares donde se use este control, habría que comentar este 'if' dejando sólo la asociación del evento a la función
        If Me.Page.ToString.IndexOf("emisionpedido") <> -1 Then
            _dteFechaHastaFin.ClientSideEvents.CalendarOpened = "cboStartDate_AfterDrop"
        End If
        _dteFechaHastaFin.SkinID = "calendario"
        celda.Controls.Add(_dteFechaHastaFin)
        fila.Cells.Add(celda)
        celda = New HtmlControls.HtmlTableCell()
        celda.NoWrap = True
        _chkPartidasNoVigentes = New CheckBox()
        _chkPartidasNoVigentes.ID = Me.ID & "_chkPartidasNoVigentes"
        celda.Controls.Add(_chkPartidasNoVigentes)
        _empresaOculta = New HiddenField
        _empresaOculta.ID = "EmpresaID"
        celda.Controls.Add(_empresaOculta)
        fila.Cells.Add(celda)
        tabla.Rows.Add(fila)
        _upParametros.ContentTemplateContainer.Controls.Add(tabla)

        ' Panel de búsqueda de centros de coste
        _modalCentrosCoste = New AjaxControlToolkit.ModalPopupExtender()
        _modalCentrosCoste.ID = Me.ID & "_modalCentrosCoste"
        _modalCentrosCoste.TargetControlID = _imgBuscarCentro.ID
        _modalCentrosCoste.PopupControlID = _pnlCentrosCoste.ID
        _modalCentrosCoste.DropShadow = False
        _upParametros.ContentTemplateContainer.Controls.Add(_modalCentrosCoste)

        Me.Controls.Add(_upParametros)

        _upTreeview = New UpdatePanel()
        _upTreeview.ID = Me.ID & "_upTreeview"
        _upTreeview.UpdateMode = UpdatePanelUpdateMode.Conditional
        Dim capa = New HtmlControls.HtmlGenericControl("div")
        capa.ID = Me.ID & "_dCamposFiltros"
        capa.Attributes.Add("class", "dContenedor")
        capa.Style.Add(HtmlTextWriterStyle.Height, "250px")
        capa.Style.Add(HtmlTextWriterStyle.Width, "95%")
        capa.Style.Add(HtmlTextWriterStyle.Overflow, "auto")
        _uwtCentrosPartidas = New Global.Infragistics.Web.UI.NavigationControls.WebDataTree()
        _uwtCentrosPartidas.ID = Me.ID & "_uwtCentrosPartidas"
        _uwtCentrosPartidas.NodeSettings.SelectedCssClass = "NodeSelected"

        _uwtCentrosPartidas.ClientEvents.Initialize = Me.ClientID & "_InitTree"
        _uwtCentrosPartidas.Style.Add(HtmlTextWriterStyle.Overflow, "visible")
        'Si la llamada viene de EmisionPedido, vamos a asociar al evento InitializeTree del _uwtCentrosPartidas
        'una función javascript. Esta función está en jsUtilities.js.
        'Para ponerla en todos los lugares donde se use este control, habría que comentar este 'if' dejando sólo la asociación del evento a la función
        If Me.Page.ToString.IndexOf("emisionpedido") <> -1 Then
            _uwtCentrosPartidas.ClientEvents.Initialize = "setOverflow"
        End If

        capa.Controls.Add(_uwtCentrosPartidas)
        _upTreeview.ContentTemplateContainer.Controls.Add(capa)
        Me.Controls.Add(_upTreeview)

        tabla = New HtmlControls.HtmlTable()
        tabla.Width = "100%"
        tabla.Border = 0
        fila = New HtmlControls.HtmlTableRow()
        celda = New HtmlControls.HtmlTableCell()
        celda.Align = "center"
        Dim tablain As New HtmlControls.HtmlTable()
        tablain.CellSpacing = 20
        tablain.Border = 0
        Dim filain As New HtmlControls.HtmlTableRow()
        Dim celdain As New HtmlControls.HtmlTableCell()
        celdain.Align = "left"
        _btnSeleccionar = New FSNButton()
        _btnSeleccionar.ID = Me.ID & "_btnSeleccionar"
        _btnSeleccionar.Alineacion = FSNTipos.Alineacion.Left
        celdain.Controls.Add(_btnSeleccionar)
        filain.Cells.Add(celdain)
        celdain = New HtmlControls.HtmlTableCell()
        celdain.Align = "right"
        _btnCancelar = New FSNButton()
        _btnCancelar.ID = Me.ID & "_btnCancelar"
        _btnCancelar.Alineacion = FSNTipos.Alineacion.Right
        celdain.Controls.Add(_btnCancelar)
        filain.Cells.Add(celdain)
        tablain.Rows.Add(filain)
        celda.Controls.Add(tablain)
        fila.Cells.Add(celda)
        tabla.Rows.Add(fila)
        Me.Controls.Add(tabla)
    End Sub

    Private Sub SMPartidasPresupuestarias_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        _Idioma = CType(Me.Page, FSNServer.IFSNPage).Usuario.Idioma
        If String.IsNullOrEmpty(_urlImgRoot) And Me.Page IsNot Nothing Then
            _urlImgRoot = Me.Page.ClientScript.GetWebResourceUrl(GetType(SMPartidasPresupuestarias), "Fullstep.SMWebControls.World.gif")
        End If
        If String.IsNullOrEmpty(_urlImgFolder) And Me.Page IsNot Nothing Then
            _urlImgFolder = Me.Page.ClientScript.GetWebResourceUrl(GetType(SMPartidasPresupuestarias), "Fullstep.SMWebControls.carpetaNaranja.gif")
        End If
        If String.IsNullOrEmpty(_urlImgCentroCoste) And Me.Page IsNot Nothing Then
            _urlImgCentroCoste = Me.Page.ClientScript.GetWebResourceUrl(GetType(SMPartidasPresupuestarias), "Fullstep.SMWebControls.centrocoste_small.jpg")
        End If
        If String.IsNullOrEmpty(_urlImgCandado) And Me.Page IsNot Nothing Then
            _urlImgCandado = Me.Page.ClientScript.GetWebResourceUrl(GetType(SMPartidasPresupuestarias), "Fullstep.SMWebControls.candado.gif")
        End If
    End Sub

    ''' Revisado por: blp. Fecha: 09/07/2012
    ''' <summary>
    ''' Evento que carga los elementos del panel de partidas presupuestarias
    ''' </summary>
    ''' <param name="sender">sender</param>
    ''' <param name="e">argumentos del evento</param>
    ''' <remarks>Llamada desde el evento. Max. 0,3 seg.</remarks>
    Private Sub SMPartidasPresupuestarias_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        EnsureChildControls()

        _lblTitulo.Text = Me.Textos(8) & " " & _sDenPartidas
        _lblBuscar.Text = Me.Textos(0)
        _lblCentro.Text = Me.Textos(1)
        _lblFechaInicioDesde.Text = Me.Textos(2)
        _lblFechaHastaFin.Text = Me.Textos(3)
        _btnBuscar.Text = Me.Textos(4)
        _chkPartidasNoVigentes.Text = Me.Textos(5)
        _btnSeleccionar.Text = Me.Textos(6)
        _btnCancelar.Text = Me.Textos(7)
        _imgSiguiente.ToolTip = Me.Textos(13)
        _lblGestor.Text = Me.Textos(14) 'Gestor

        If Not Page.IsPostBack() Then
            If _mFiltroCentroSM Is Nothing Then
                _txtCentro.Visible = True
                _imgBuscarCentro.Visible = True
                _lblCentroCosteDen.Visible = False
            Else
                _txtCentro.Visible = False
                _imgBuscarCentro.Visible = False
                _lblCentroCosteDen.Visible = True
                _lblCentroCosteDen.Text = _mFiltroCentroSM.Codigo & " - " & _mFiltroCentroSM.Denominacion
            End If

            If _mFiltroGestor Is Nothing Then
                _cboGestor.Visible = True
                _lblGestorDen.Visible = False
            Else
                _cboGestor.Visible = True
                _lblGestorDen.Visible = False
                _lblGestorDen.Text = GestoresPartidasUsu(ArbolPresupuestario).Find(Function(oParPres As PartidaPRES5) oParPres.GestorCod = _mFiltroGestor).GestorDen
            End If

            _dteFechaInicioDesde.NullText = String.Empty
            _dteFechaHastaFin.NullText = String.Empty

            If String.IsNullOrEmpty(_imgCabecera.Src) And Me.Page IsNot Nothing Then _
                _imgCabecera.Src = Me.Page.ClientScript.GetWebResourceUrl(GetType(SMPartidasPresupuestarias), "Fullstep.SMWebControls.centrocoste.jpg")
            If String.IsNullOrEmpty(_imgCerrar.ImageUrl) And Me.Page IsNot Nothing Then _
                _imgCerrar.ImageUrl = Me.Page.ClientScript.GetWebResourceUrl(GetType(SMPartidasPresupuestarias), "Fullstep.SMWebControls.Bt_Cerrar.png")
            If String.IsNullOrEmpty(_imgBuscarCentro.ImageUrl) And Me.Page IsNot Nothing Then _
                _imgBuscarCentro.ImageUrl = Me.Page.ClientScript.GetWebResourceUrl(GetType(SMPartidasPresupuestarias), "Fullstep.SMWebControls.lupa.gif")
            If String.IsNullOrEmpty(_imgSiguiente.ImageUrl) And Me.Page IsNot Nothing Then _
                _imgSiguiente.ImageUrl = Me.Page.ClientScript.GetWebResourceUrl(GetType(SMPartidasPresupuestarias), "Fullstep.SMWebControls.flecha_siguiente.gif")

            _txtBuscar.Attributes.Add("onchange", "sm_FindClicked(this," & Me.ClientID & "_tree, '" & _imgSiguiente.ClientID & "')")
            _txtBuscar.Attributes.Add("onkeyup", "sm_FindClicked(this," & Me.ClientID & "_tree, '" & _imgSiguiente.ClientID & "')")
            _imgSiguiente.Attributes.Add("onclick", "sm_FindNextClicked('" & _txtBuscar.ClientID & "', " & Me.ClientID & "_tree, this); return false")

            _pnlCentrosCoste.OnCancelarClientClick = "$find('" & _modalCentrosCoste.ClientID & "').hide(); return false"
        End If

        Me.Page.ClientScript.RegisterClientScriptResource(GetType(SMWebControlClientScript), "Fullstep.SMWebControls.SMControls.js")

        Dim sScript As String = "//Esta función recoloca el calendario del control WedDateChooser para que se mueva si se hace scroll"
        sScript += vbCrLf & "   function cboStartDate_AfterDrop(oDateChooser, dropDownPanel, oEvent) {"
        sScript += vbCrLf & "       var div = oDateChooser.container;"
        sScript += vbCrLf & "        var iframe = oDateChooser._ieBug ? oDateChooser._ieBug.Element : null;"
        sScript += vbCrLf & "        var panelPartidasTop = parseFloat($get(oDateChooser.Id.slice(0,oDateChooser.Id.indexOf('SMPartidasPresupuestarias')+('SMPartidasPresupuestarias').length)).style.top.replace('px', ''));"
        sScript += vbCrLf & "        var myLeft = izquierda(div.id);"
        sScript += vbCrLf & "        var myTop = panelPartidasTop + 172 + 'px';"
        sScript += vbCrLf & ""
        sScript += vbCrLf & "        div.style.position = 'fixed';"
        sScript += vbCrLf & "        div.style.top = myTop;"
        sScript += vbCrLf & "        div.style.left = myLeft;"
        sScript += vbCrLf & "        if (iframe) {"
        sScript += vbCrLf & "            iframe.style.position = 'fixed';"
        sScript += vbCrLf & "            iframe.style.left = myLeft;"
        sScript += vbCrLf & "            iframe.style.top = myTop;"
        sScript += vbCrLf & "        }"
        sScript += vbCrLf & "    }"

        Me.Page.ClientScript.RegisterClientScriptBlock(GetType(SMWebControlClientScript), "cboStartDate_AfterDrop", sScript, True)

        sScript = "   /* Funcion para establecer el overflow de un treeview de infragistics (que se establece a hidden por defecto)"
        sScript += vbCrLf & "   parametro(entrada)"
        sScript += vbCrLf & "   oTree:= id del control"
        sScript += vbCrLf & "   oEvent:= argumentos del evento"
        sScript += vbCrLf & "   tiempo ejecucion:0seg.*/"
        sScript += vbCrLf & "   function setOverflow(oTree, oEvent) {"
        sScript += vbCrLf & "       var tree = $find(oTree); "
        sScript += vbCrLf & "       tree.Element.style.overflow = 'visible'; "
        sScript += vbCrLf & "   }"

        Me.Page.ClientScript.RegisterClientScriptBlock(GetType(SMWebControlClientScript), "setOverflow", sScript, True)

        Dim sClientScript As String = "var " & Me.ClientID & "_tree;" & _
            "function " & Me.ClientID & "_InitTree(tree,e){" & _
                Me.ClientID & "_tree = $find(tree.get_id());" & _
            "}"
        Me.Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), Me.ClientID & "_InicializarArbol", sClientScript, True)
    End Sub

#End Region

#Region "Enlace a datos"

    ''' Revisado por: blp. Fecha: 29/11/2011
    ''' <summary>
    ''' Método que actualiza el arbol presupuestario. 
    ''' sólo puede lanzarse desde CargarDatos, de este modo, podremos controlar que no se actualice el arbol de manera incontrolada
    ''' </summary>
    ''' <remarks>Llamado desde CargarDatos. Para actualizar desde otras páginas, recurriremos a CargarDatos que es pública. Máx 0,3 seg</remarks>
    Private Sub ActualizarArbol()
        If _uwtCentrosPartidas IsNot Nothing Then

            Dim pag As IFSNPage = CType(Me.Page, IFSNPage)
            Dim arbol As UONs = ArbolConFiltros()

            _uwtCentrosPartidas.Nodes.Clear()
            For Each node As Infragistics.Web.UI.NavigationControls.DataTreeNode In _uwtCentrosPartidas.AllNodes
                Select Case node.Level
                    Case 0
                        node.ImageUrl = _urlImgRoot
                    Case 1, 2, 3, 4
                        node.ImageUrl = _urlImgCentroCoste
                    Case 5
                        node.ImageUrl = _urlImgCandado
                End Select
            Next

            '_uwtCentrosPartidas.Levels(0).LevelImage = _urlImgRoot
            '_uwtCentrosPartidas.Levels(1).LevelImage = _urlImgFolder
            '_uwtCentrosPartidas.Levels(2).LevelImage = _urlImgFolder
            '_uwtCentrosPartidas.Levels(3).LevelImage = _urlImgFolder
            '_uwtCentrosPartidas.Levels(4).LevelImage = _urlImgFolder
            '_uwtCentrosPartidas.Levels(5).LevelImage = _urlImgCandado
            Dim nodoroot As New Infragistics.Web.UI.NavigationControls.DataTreeNode()
            nodoroot.Expanded = True
            nodoroot.Text = pag.Acceso.UON0_Denominacion(_Idioma.ToString)
            _uwtCentrosPartidas.Nodes.Add(nodoroot)

            Dim Fechas1, Fechas2, Fechas3, Fechas4 As String
            Dim sGestor As String = String.Empty
            For Each u1 As UON In arbol
                Dim node1 As New Infragistics.Web.UI.NavigationControls.DataTreeNode()
                node1.Expanded = True
                node1.Target = "1"
                If u1.CentroSM IsNot Nothing AndAlso u1.CentroSM.PartidasPresupuestarias IsNot Nothing _
                    AndAlso u1.CentroSM.PartidasPresupuestarias.Count > 0 Then
                    node1.ImageUrl = _urlImgCentroCoste
                    node1.Text = If(u1.UON4 <> String.Empty, u1.UON4, If(u1.UON3 <> String.Empty, u1.UON3, If(u1.UON2 <> String.Empty, u1.UON2, If(u1.UON1 <> String.Empty, u1.UON1, String.Empty)))) & " - " & u1.CentroSM.Denominacion
                    node1.Key = u1.CentroSM.Codigo
                Else
                    node1.Text = u1.UON1 & " - " & u1.Denominacion
                End If
                If u1.ChildUONs IsNot Nothing AndAlso u1.ChildUONs.Count > 0 Then
                    For Each u2 As UON In u1.ChildUONs
                        Dim node2 As New Infragistics.Web.UI.NavigationControls.DataTreeNode()
                        node2.Expanded = True
                        node2.Target = "1"
                        If u2.CentroSM IsNot Nothing AndAlso u2.CentroSM.PartidasPresupuestarias IsNot Nothing _
                            AndAlso u2.CentroSM.PartidasPresupuestarias.Count > 0 Then
                            node2.ImageUrl = _urlImgCentroCoste
                            node2.Text = If(u2.UON4 <> String.Empty, u2.UON4, If(u2.UON3 <> String.Empty, u2.UON3, If(u2.UON2 <> String.Empty, u2.UON2, If(u2.UON1 <> String.Empty, u2.UON1, String.Empty)))) & " - " & u2.CentroSM.Denominacion
                            node2.Key = u2.CentroSM.Codigo
                        Else
                            node2.Text = u2.UON2 & " - " & u2.Denominacion
                        End If
                        If u2.ChildUONs IsNot Nothing AndAlso u2.ChildUONs.Count > 0 Then
                            For Each u3 As UON In u2.ChildUONs
                                Dim node3 As New Infragistics.Web.UI.NavigationControls.DataTreeNode()
                                node3.Expanded = True
                                node3.Target = "1"
                                If u3.CentroSM IsNot Nothing AndAlso u3.CentroSM.PartidasPresupuestarias IsNot Nothing _
                                    AndAlso u3.CentroSM.PartidasPresupuestarias.Count > 0 Then
                                    node3.ImageUrl = _urlImgCentroCoste
                                    node3.Text = u3.CentroSM.Codigo & " - " & u3.CentroSM.Denominacion
                                    node3.Text = If(u3.UON4 <> String.Empty, u3.UON4, If(u3.UON3 <> String.Empty, u3.UON3, If(u3.UON2 <> String.Empty, u3.UON2, If(u3.UON1 <> String.Empty, u3.UON1, String.Empty)))) & " - " & u3.CentroSM.Denominacion
                                    node3.Key = u3.CentroSM.Codigo
                                Else
                                    node3.Text = u3.UON3 & " - " & u3.Denominacion
                                End If
                                If u3.ChildUONs IsNot Nothing AndAlso u3.ChildUONs.Count > 0 Then
                                    For Each u4 As UON In u3.ChildUONs
                                        Dim node4 As New Infragistics.Web.UI.NavigationControls.DataTreeNode()
                                        node4.Expanded = True
                                        node4.Target = "1"
                                        If u4.CentroSM IsNot Nothing AndAlso u4.CentroSM.PartidasPresupuestarias IsNot Nothing _
                                            AndAlso u4.CentroSM.PartidasPresupuestarias.Count > 0 Then
                                            node4.ImageUrl = _urlImgCentroCoste
                                            node4.Text = If(u4.UON4 <> String.Empty, u4.UON4, If(u4.UON3 <> String.Empty, u4.UON3, If(u4.UON2 <> String.Empty, u4.UON2, If(u4.UON1 <> String.Empty, u4.UON1, String.Empty)))) & " - " & u4.CentroSM.Denominacion
                                            node4.Key = u4.CentroSM.Codigo
                                            For Each p As PartidaPRES5 In u4.CentroSM.PartidasPresupuestarias
                                                Dim nodep As New Infragistics.Web.UI.NavigationControls.DataTreeNode()
                                                nodep.Target = "1"
                                                nodep.ImageUrl = _urlImgCandado
                                                Fechas4 = String.Empty
                                                If p.FechaInicioPresupuesto.HasValue Then
                                                    Fechas4 = " (" & FormatDate(p.FechaInicioPresupuesto, pag.Usuario.DateFormat) & " - "
                                                End If
                                                If p.FechaFinPresupuesto.HasValue Then
                                                    Fechas4 = IIf(Fechas4 = String.Empty, " ( - " & FormatDate(p.FechaFinPresupuesto, pag.Usuario.DateFormat) & ")", Fechas4 & FormatDate(p.FechaFinPresupuesto, pag.Usuario.DateFormat) & ")")
                                                Else
                                                    Fechas4 = IIf(Fechas4 = String.Empty, Fechas4, Fechas4 & ")")
                                                End If
                                                nodep.Text = p.ToString().Substring(p.ToString().LastIndexOf("#") + 1) & _
                                                    " - " & p.Denominacion & Fechas4
                                                sGestor = seleccionaGestorDen(p)
                                                If Not String.IsNullOrEmpty(sGestor) Then
                                                    nodep.Text += " " & Textos(14) & ": " & sGestor
                                                End If
                                                nodep.Key = p.ToString()
                                                If Not p.TienePresupuestoVigente Then
                                                    nodep.CssClass = "NodePresupuestoVigente"
                                                    'Styles.ForeColor = Color.Red
                                                End If
                                                node4.Nodes.Add(nodep)
                                            Next
                                            node3.Nodes.Add(node4)
                                        End If
                                    Next
                                End If
                                If node3.ImageUrl = _urlImgCentroCoste Then
                                    For Each p As PartidaPRES5 In u3.CentroSM.PartidasPresupuestarias
                                        Dim nodep As New Infragistics.Web.UI.NavigationControls.DataTreeNode()
                                        nodep.Target = "1"
                                        nodep.ImageUrl = _urlImgCandado
                                        Fechas3 = String.Empty
                                        If p.FechaInicioPresupuesto.HasValue Then
                                            Fechas3 = " (" & FormatDate(p.FechaInicioPresupuesto, pag.Usuario.DateFormat) & " - "
                                        End If
                                        If p.FechaFinPresupuesto.HasValue Then
                                            Fechas3 = IIf(Fechas3 = String.Empty, " ( - " & FormatDate(p.FechaFinPresupuesto, pag.Usuario.DateFormat) & ")", Fechas3 & FormatDate(p.FechaFinPresupuesto, pag.Usuario.DateFormat) & ")")
                                        Else
                                            Fechas3 = IIf(Fechas3 = String.Empty, Fechas3, Fechas3 & ")")
                                        End If
                                        nodep.Text = p.ToString().Substring(p.ToString().LastIndexOf("#") + 1) & _
                                                    " - " & p.Denominacion & Fechas3
                                        sGestor = seleccionaGestorDen(p)
                                        If Not String.IsNullOrEmpty(sGestor) Then
                                            nodep.Text += " " & Textos(14) & ": " & sGestor
                                        End If
                                        If Not p.TienePresupuestoVigente Then
                                            nodep.CssClass = "NodePresupuestoVigente"
                                            'nodep.Styles.ForeColor = Color.Red
                                        End If
                                        nodep.Key = p.ToString()
                                        node3.Nodes.Add(nodep)
                                    Next
                                End If
                                node2.Nodes.Add(node3)
                            Next
                        End If
                        If node2.ImageUrl = _urlImgCentroCoste Then
                            For Each p As PartidaPRES5 In u2.CentroSM.PartidasPresupuestarias
                                Dim nodep As New Infragistics.Web.UI.NavigationControls.DataTreeNode()
                                nodep.Target = "1"
                                nodep.ImageUrl = _urlImgCandado
                                Fechas2 = String.Empty
                                If p.FechaInicioPresupuesto.HasValue Then
                                    Fechas2 = " (" & FormatDate(p.FechaInicioPresupuesto, pag.Usuario.DateFormat) & " - "
                                End If
                                If p.FechaFinPresupuesto.HasValue Then
                                    Fechas2 = IIf(Fechas2 = String.Empty, " ( - " & FormatDate(p.FechaFinPresupuesto, pag.Usuario.DateFormat) & ")", Fechas2 & FormatDate(p.FechaFinPresupuesto, pag.Usuario.DateFormat) & ")")
                                Else
                                    Fechas2 = IIf(Fechas2 = String.Empty, Fechas2, Fechas2 & ")")
                                End If
                                nodep.Text = p.ToString().Substring(p.ToString().LastIndexOf("#") + 1) & _
                                                    " - " & p.Denominacion & Fechas2
                                sGestor = seleccionaGestorDen(p)
                                If Not String.IsNullOrEmpty(sGestor) Then
                                    nodep.Text += " " & Textos(14) & ": " & sGestor
                                End If
                                If Not p.TienePresupuestoVigente Then
                                    nodep.CssClass = "NodePresupuestoVigente"
                                    'nodep.Styles.ForeColor = Color.Red
                                End If
                                nodep.Key = p.ToString()
                                node2.Nodes.Add(nodep)
                            Next
                        End If
                        node1.Nodes.Add(node2)
                    Next
                End If
                If node1.ImageUrl = _urlImgCentroCoste Then
                    For Each p As PartidaPRES5 In u1.CentroSM.PartidasPresupuestarias
                        Dim nodep As New Infragistics.Web.UI.NavigationControls.DataTreeNode()
                        nodep.Target = "1"
                        nodep.ImageUrl = _urlImgCandado
                        Fechas1 = String.Empty
                        If p.FechaInicioPresupuesto.HasValue Then
                            Fechas1 = " (" & FormatDate(p.FechaInicioPresupuesto, pag.Usuario.DateFormat) & " - "
                        End If
                        If p.FechaFinPresupuesto.HasValue Then
                            Fechas1 = IIf(Fechas1 = String.Empty, " ( - " & FormatDate(p.FechaFinPresupuesto, pag.Usuario.DateFormat) & ")", Fechas1 & FormatDate(p.FechaFinPresupuesto, pag.Usuario.DateFormat) & ")")
                        Else
                            Fechas1 = IIf(Fechas1 = String.Empty, Fechas1, Fechas1 & ")")
                        End If
                        nodep.Text = p.ToString().Substring(p.ToString().LastIndexOf("#") + 1) & _
                                                    " - " & p.Denominacion & Fechas1
                        sGestor = seleccionaGestorDen(p)
                        If Not String.IsNullOrEmpty(sGestor) Then
                            nodep.Text += " " & Textos(14) & ": " & sGestor
                        End If
                        If Not p.TienePresupuestoVigente Then
                            nodep.CssClass = "NodePresupuestoVigente"
                            'nodep.Styles.ForeColor = Color.Red
                        End If
                        nodep.Key = p.ToString()
                        node1.Nodes.Add(nodep)
                    Next
                End If
                nodoroot.Nodes.Add(node1)
            Next

            _pnlCentrosCoste.ListaCentros = From centros In ArbolImputacion.FiltrarPorArbolPresupuestario(_sPRES5).DevolverCentrosdeCoste() _
                                            Where IIf(Empresa IsNot Nothing AndAlso Empresa <> 0, centros.Empresas.Empresa1 = Empresa OrElse centros.Empresas.Empresa2 = Empresa OrElse centros.Empresas.Empresa3 = Empresa OrElse centros.Empresas.Empresa4 = Empresa, 1 = 1) _
                                            Order By centros.Codigo _
                                            Select New ListItem(If(centros.UONS.UON4 <> String.Empty, centros.UONS.UON4, If(centros.UONS.UON3 <> String.Empty, centros.UONS.UON3, If(centros.UONS.UON2 <> String.Empty, centros.UONS.UON2, If(centros.UONS.UON1 <> String.Empty, centros.UONS.UON1, String.Empty)))) & " - " & centros.Denominacion, centros.Codigo) Distinct.ToList()
            _txtBuscar.Text = String.Empty
            _upParametros.Update()
            _upTreeview.Update()

        End If
    End Sub

    ''' Revisado por: blp. Fecha:29/06/2012
    ''' <summary>
    ''' Filtra el árbol de partidas por los filtros de búsqueda.
    ''' </summary>
    ''' <returns>Árbol de partidas filtrado</returns>
    ''' <remarks>Llamada desde SMPartidasPresupuestarias.vb. Max. 0,3 seg.</remarks>
    Private Function ArbolConFiltros() As UONs
        Dim arbol As UONs = ArbolImputacion.FiltrarPorArbolPresupuestario(_sPRES5, Not _chkPartidasNoVigentes.Checked)
        If Empresa IsNot Nothing Then
            FiltrarArbolPorEmpresa(arbol, Empresa)
        End If
        If Not String.IsNullOrEmpty(_txtCentro.Attributes("CENTROSM")) Then
            arbol = FiltrarArbolPorCentro(arbol, _txtCentro.Attributes("CENTROSM"))
        ElseIf _mFiltroCentroSM IsNot Nothing Then
            arbol = FiltrarArbolPorCentro(arbol, _mFiltroCentroSM.Codigo)
        End If
        If _cboGestor.Visible Then
            If _cboGestor.SelectedValue IsNot Nothing AndAlso _cboGestor.SelectedValue <> String.Empty Then
                arbol = FiltrarArbolPorGestor(arbol, _cboGestor.SelectedValue)
            End If
        Else
            If Not String.IsNullOrEmpty(_lblGestorDen.Attributes("gestorCod")) Then
                arbol = FiltrarArbolPorGestor(arbol, _lblGestorDen.Attributes("gestorCod"))
            ElseIf _mFiltroGestor <> String.Empty Then
                arbol = FiltrarArbolPorGestor(arbol, _mFiltroGestor)
            End If
        End If
        If _dteFechaInicioDesde.Value IsNot Nothing Then
            FiltrarArbolPorFechaDesde(arbol, _dteFechaInicioDesde.Value)
        End If
        If _dteFechaHastaFin.Value IsNot Nothing Then
            FiltrarArbolPorFechaHasta(arbol, _dteFechaHastaFin.Value)
        End If
        Return arbol
    End Function

    ''' Revisado por: blp. Fecha: 28/11/2011
    ''' <summary>
    ''' Función que filtra por empresa el árbol de partidas presupuestarias.
    ''' Cada partida tiene un conjunto de UONs que define su posición en la estructura organizativa.
    ''' del mismo modo, la posición de la empresa y de los centros de coste se define por un nivel de UONs.
    ''' Para cada centro de coste hay un conjunto de empresas que corresponden a cada nivel de UON (de 1 a 4)
    ''' Sólo dejaremos aquellas partidas en las para el centro de coste correspondiente, la empresa se encuentre en algún punto de ese conjunto de UONs,
    ''' sea el nivel 1, 2, 3, 4 o más de uno de ellos.
    ''' Es decir, no tienen que coincidir pero sí estar en la misma línea.
    ''' </summary>
    ''' <param name="arbol">Estructura de UONs anidadas que constituye el árbol de partidas presupuestarias con todas las propiedades de centros de coste, empresas, etc</param>
    ''' <param name="empresa">Id de la Empresa por la que queremos filtrar</param>
    ''' <returns>el árbol filtrado</returns>
    ''' <remarks>Llamada desde ArbolConFiltros(). Máx inferior a 0,5 seg</remarks>
    Private Function FiltrarArbolPorEmpresa(ByVal arbol As UONs, ByVal empresa As Integer) As UONs
        arbol.RemoveAll(Function(el As UON) el.CentroSM IsNot Nothing AndAlso (el.CentroSM.Empresas.Empresa1 <> empresa AndAlso el.CentroSM.Empresas.Empresa2 <> empresa AndAlso el.CentroSM.Empresas.Empresa3 <> empresa AndAlso el.CentroSM.Empresas.Empresa4 <> empresa))
        For Each u1 As UON In arbol
            If u1.ChildUONs IsNot Nothing Then
                u1.ChildUONs.RemoveAll(Function(el As UON) el.CentroSM IsNot Nothing AndAlso (el.CentroSM.Empresas.Empresa1 <> empresa AndAlso el.CentroSM.Empresas.Empresa2 <> empresa AndAlso el.CentroSM.Empresas.Empresa3 <> empresa AndAlso el.CentroSM.Empresas.Empresa4 <> empresa))
                If u1.ChildUONs.Count = 0 Then
                    u1.ChildUONs = Nothing
                Else
                    For Each u2 As UON In u1.ChildUONs
                        If u2.ChildUONs IsNot Nothing Then
                            u2.ChildUONs.RemoveAll(Function(el As UON) el.CentroSM IsNot Nothing AndAlso (el.CentroSM.Empresas.Empresa1 <> empresa AndAlso el.CentroSM.Empresas.Empresa2 <> empresa AndAlso el.CentroSM.Empresas.Empresa3 <> empresa AndAlso el.CentroSM.Empresas.Empresa4 <> empresa))
                            If u2.ChildUONs.Count = 0 Then
                                u2.ChildUONs = Nothing
                            Else
                                For Each u3 As UON In u2.ChildUONs
                                    If u3.ChildUONs IsNot Nothing Then
                                        u3.ChildUONs.RemoveAll(Function(el As UON) el.CentroSM IsNot Nothing AndAlso (el.CentroSM.Empresas.Empresa1 <> empresa AndAlso el.CentroSM.Empresas.Empresa2 <> empresa AndAlso el.CentroSM.Empresas.Empresa3 <> empresa AndAlso el.CentroSM.Empresas.Empresa4 <> empresa))
                                        If u3.ChildUONs.Count = 0 Then
                                            u3.ChildUONs = Nothing
                                        End If
                                    End If
                                Next
                            End If
                        End If
                    Next
                End If
            End If
        Next
        Return arbol
    End Function

    ''' Revisado por: blp. Fecha: 29/06/2012
    ''' <summary>
    ''' Filtra el árbol presupuestario pasado como parámetro por el Centro de la búsqueda
    ''' </summary>
    ''' <param name="arbol">árbol de partidas a filtrar</param>
    ''' <param name="CodCentroSM">Código del Centro</param>
    ''' <returns>árbol filtrado</returns>
    ''' <remarks>Llamada desde SMPartidasPresupuestarias.vb. Max. 0,3 seg.</remarks>
    Private Function FiltrarArbolPorCentro(ByVal arbol As UONs, ByVal CodCentroSM As String) As UONs
        For Each u1 As UON In arbol
            If u1.CentroSM IsNot Nothing AndAlso u1.CentroSM.Codigo <> CodCentroSM Then u1.CentroSM = Nothing
            If u1.ChildUONs IsNot Nothing Then
                For Each u2 In u1.ChildUONs
                    If u2.CentroSM IsNot Nothing AndAlso u2.CentroSM.Codigo <> CodCentroSM Then u2.CentroSM = Nothing
                    If u2.ChildUONs IsNot Nothing Then
                        For Each u3 As UON In u2.ChildUONs
                            If u3.CentroSM IsNot Nothing AndAlso u3.CentroSM.Codigo <> CodCentroSM Then u3.CentroSM = Nothing
                            If u3.ChildUONs IsNot Nothing Then
                                For Each u4 As UON In u3.ChildUONs
                                    If u4.CentroSM IsNot Nothing AndAlso u4.CentroSM.Codigo <> CodCentroSM Then u4.CentroSM = Nothing
                                Next
                            End If
                        Next
                    End If
                Next
            End If
        Next

        'Limpiamos el árbol de ramas presupuestarias vacías de contenido
        arbol = LimpiarArbol(arbol)

        Return arbol
    End Function

    ''' Revisado por: blp. Fecha: 29/06/2012
    ''' <summary>
    ''' Filtra el árbol presupuestario pasado como parámetro por el gestor de la búsqueda
    ''' </summary>
    ''' <param name="arbol">árbol de partidas a filtrar</param>
    ''' <param name="gestorCod">Código persona del gestor</param>
    ''' <returns>árbol filtrado</returns>
    ''' <remarks>Llamada desde SMPartidasPresupuestarias.vb. Max. 0,3 seg.</remarks>
    Private Function FiltrarArbolPorGestor(ByVal arbol As UONs, ByVal gestorCod As String) As UONs
        For Each u1 As UON In arbol
            If u1.CentroSM IsNot Nothing Then
                If u1.CentroSM.PartidasPresupuestarias Is Nothing Then
                    u1.CentroSM = Nothing
                Else
                    u1.CentroSM.PartidasPresupuestarias.RemoveAll(Function(par As PartidaPRES5) _
                                                                      seleccionaGestor(par) <> gestorCod)
                    If u1.CentroSM.PartidasPresupuestarias.Count = 0 Then u1.CentroSM = Nothing
                End If
            End If
            If u1.ChildUONs IsNot Nothing Then
                For Each u2 In u1.ChildUONs
                    If u2.CentroSM IsNot Nothing Then
                        If u2.CentroSM.PartidasPresupuestarias Is Nothing Then
                            u2.CentroSM = Nothing
                        Else
                            u2.CentroSM.PartidasPresupuestarias.RemoveAll(Function(par As PartidaPRES5) _
                                                                              seleccionaGestor(par) <> gestorCod)
                            If u2.CentroSM.PartidasPresupuestarias.Count = 0 Then u2.CentroSM = Nothing
                        End If
                    End If
                    If u2.ChildUONs IsNot Nothing Then
                        For Each u3 In u2.ChildUONs
                            If u3.CentroSM IsNot Nothing Then
                                If u3.CentroSM.PartidasPresupuestarias Is Nothing Then
                                    u3.CentroSM = Nothing
                                Else
                                    u3.CentroSM.PartidasPresupuestarias.RemoveAll(Function(par As PartidaPRES5) _
                                                                                      seleccionaGestor(par) <> gestorCod)
                                    If u3.CentroSM.PartidasPresupuestarias.Count = 0 Then u3.CentroSM = Nothing
                                End If
                            End If
                            If u3.ChildUONs IsNot Nothing Then
                                For Each u4 In u3.ChildUONs
                                    If u4.CentroSM IsNot Nothing Then
                                        If u4.CentroSM.PartidasPresupuestarias Is Nothing Then
                                            u4.CentroSM = Nothing
                                        Else
                                            u4.CentroSM.PartidasPresupuestarias.RemoveAll(Function(par As PartidaPRES5) _
                                                                                              seleccionaGestor(par) <> gestorCod)
                                            If u4.CentroSM.PartidasPresupuestarias.Count = 0 Then u4.CentroSM = Nothing
                                        End If
                                    End If
                                Next
                            End If
                        Next
                    End If
                Next
            End If
        Next

        'Limpiamos el árbol de ramas presupuestarias vacías de contenido
        arbol = LimpiarArbol(arbol)

        Return arbol
    End Function

    ''' Revisado por: blp. Fecha: 29/06/2012
    ''' <summary>
    ''' Filtra el árbol presupuestario pasado como parámetro por la Fecha Desde de la búsqueda
    ''' </summary>
    ''' <param name="arbol">árbol de partidas a filtrar</param>
    ''' <param name="Fecha">Fecha Desde</param>
    ''' <returns>árbol filtrado</returns>
    ''' <remarks>Llamada desde SMPartidasPresupuestarias.vb. Max. 0,3 seg.</remarks>
    Private Function FiltrarArbolPorFechaDesde(ByVal arbol As UONs, ByVal Fecha As DateTime) As UONs
        For Each u1 As UON In arbol
            If u1.CentroSM IsNot Nothing Then
                If u1.CentroSM.PartidasPresupuestarias Is Nothing Then
                    u1.CentroSM = Nothing
                Else
                    u1.CentroSM.PartidasPresupuestarias.RemoveAll(Function(par As PartidaPRES5) _
                                                                      par.FechaInicioPresupuesto < Fecha)
                    If u1.CentroSM.PartidasPresupuestarias.Count = 0 Then u1.CentroSM = Nothing
                End If
            End If
            If u1.ChildUONs IsNot Nothing Then
                For Each u2 In u1.ChildUONs
                    If u2.CentroSM IsNot Nothing Then
                        If u2.CentroSM.PartidasPresupuestarias Is Nothing Then
                            u2.CentroSM = Nothing
                        Else
                            u2.CentroSM.PartidasPresupuestarias.RemoveAll(Function(par As PartidaPRES5) _
                                                                              par.FechaInicioPresupuesto < Fecha)
                            If u2.CentroSM.PartidasPresupuestarias.Count = 0 Then u2.CentroSM = Nothing
                        End If
                    End If
                    If u2.ChildUONs IsNot Nothing Then
                        For Each u3 In u2.ChildUONs
                            If u3.CentroSM IsNot Nothing Then
                                If u3.CentroSM.PartidasPresupuestarias Is Nothing Then
                                    u3.CentroSM = Nothing
                                Else
                                    u3.CentroSM.PartidasPresupuestarias.RemoveAll(Function(par As PartidaPRES5) _
                                                                                      par.FechaInicioPresupuesto < Fecha)
                                    If u3.CentroSM.PartidasPresupuestarias.Count = 0 Then u3.CentroSM = Nothing
                                End If
                            End If
                            If u3.ChildUONs IsNot Nothing Then
                                For Each u4 In u3.ChildUONs
                                    If u4.CentroSM IsNot Nothing Then
                                        If u4.CentroSM.PartidasPresupuestarias Is Nothing Then
                                            u4.CentroSM = Nothing
                                        Else
                                            u4.CentroSM.PartidasPresupuestarias.RemoveAll(Function(par As PartidaPRES5) _
                                                                                              par.FechaInicioPresupuesto < Fecha)
                                            If u4.CentroSM.PartidasPresupuestarias.Count = 0 Then u4.CentroSM = Nothing
                                        End If
                                    End If
                                Next
                            End If
                        Next
                    End If
                Next
            End If
        Next

        'Limpiamos el árbol de ramas presupuestarias vacías de contenido
        arbol = LimpiarArbol(arbol)

        Return arbol
    End Function

    ''' Revisado por: blp. Fecha: 29/06/2012
    ''' <summary>
    ''' Filtra el árbol presupuestario pasado como parámetro por la Fecha Hasta de la búsqueda
    ''' </summary>
    ''' <param name="arbol">árbol de partidas a filtrar</param>
    ''' <param name="Fecha">Fecha Hasta</param>
    ''' <returns>árbol filtrado</returns>
    ''' <remarks>Llamada desde SMPartidasPresupuestarias.vb. Max. 0,3 seg.</remarks>
    Private Function FiltrarArbolPorFechaHasta(ByVal arbol As UONs, ByVal Fecha As DateTime) As UONs
        Fecha = DateAdd(DateInterval.Day, 1, Fecha)
        For Each u1 As UON In arbol
            If u1.CentroSM IsNot Nothing Then
                If u1.CentroSM.PartidasPresupuestarias Is Nothing Then
                    u1.CentroSM = Nothing
                Else
                    u1.CentroSM.PartidasPresupuestarias.RemoveAll(Function(par As PartidaPRES5) _
                                                                      par.FechaFinPresupuesto >= Fecha)
                    If u1.CentroSM.PartidasPresupuestarias.Count = 0 Then u1.CentroSM = Nothing
                End If
            End If
            If u1.ChildUONs IsNot Nothing Then
                For Each u2 In u1.ChildUONs
                    If u2.CentroSM IsNot Nothing Then
                        If u2.CentroSM.PartidasPresupuestarias Is Nothing Then
                            u2.CentroSM = Nothing
                        Else
                            u2.CentroSM.PartidasPresupuestarias.RemoveAll(Function(par As PartidaPRES5) _
                                                                              par.FechaFinPresupuesto >= Fecha)
                            If u2.CentroSM.PartidasPresupuestarias.Count = 0 Then u2.CentroSM = Nothing
                        End If
                    End If
                    If u2.ChildUONs IsNot Nothing Then
                        For Each u3 In u2.ChildUONs
                            If u3.CentroSM IsNot Nothing Then
                                If u3.CentroSM.PartidasPresupuestarias Is Nothing Then
                                    u3.CentroSM = Nothing
                                Else
                                    u3.CentroSM.PartidasPresupuestarias.RemoveAll(Function(par As PartidaPRES5) _
                                                                                      par.FechaFinPresupuesto >= Fecha)
                                    If u3.CentroSM.PartidasPresupuestarias.Count = 0 Then u3.CentroSM = Nothing
                                End If
                            End If
                            If u3.ChildUONs IsNot Nothing Then
                                For Each u4 In u3.ChildUONs
                                    If u4.CentroSM IsNot Nothing Then
                                        If u4.CentroSM.PartidasPresupuestarias Is Nothing Then
                                            u4.CentroSM = Nothing
                                        Else
                                            u4.CentroSM.PartidasPresupuestarias.RemoveAll(Function(par As PartidaPRES5) _
                                                                                              par.FechaFinPresupuesto >= Fecha)
                                            If u4.CentroSM.PartidasPresupuestarias.Count = 0 Then u4.CentroSM = Nothing
                                        End If
                                    End If
                                Next
                            End If
                        Next
                    End If
                Next
            End If
        Next

        'Limpiamos el árbol de ramas presupuestarias vacías de contenido
        arbol = LimpiarArbol(arbol)

        Return arbol
    End Function

    ''' Revisado por: blp. Fecha: 29/06/2012
    ''' <summary>
    ''' Limpiamos el árbol de ramas presupuestarias vacías (después de haber aplicado los filtros)
    ''' </summary>
    ''' <param name="arbol">árbol presupuestario</param>
    ''' <returns>árbol presupuestario limpio</returns>
    ''' <remarks>Llamada desde SMPartidasPresupuestarias.vb. Máx 0,1 seg.</remarks>
    Private Function LimpiarArbol(ByVal arbol As UONs) As UONs
        arbol.RemoveAll(Function(el As UON) _
                            el.CentroSM Is Nothing And _
                            (el.ChildUONs Is Nothing OrElse el.ChildUONs.FindInTree(Function(e As UON) e.CentroSM IsNot Nothing) Is Nothing))
        For Each u1 As UON In arbol
            If u1.ChildUONs IsNot Nothing Then
                u1.ChildUONs.RemoveAll(Function(el As UON) _
                                        el.CentroSM Is Nothing And _
                                        (el.ChildUONs Is Nothing OrElse el.ChildUONs.FindInTree(Function(e As UON) e.CentroSM IsNot Nothing) Is Nothing))
                If u1.ChildUONs.Count = 0 Then
                    u1.ChildUONs = Nothing
                Else
                    For Each u2 As UON In u1.ChildUONs
                        If u2.ChildUONs IsNot Nothing Then
                            u2.ChildUONs.RemoveAll(Function(el As UON) _
                                                    el.CentroSM Is Nothing And _
                                                    (el.ChildUONs Is Nothing OrElse el.ChildUONs.FindInTree(Function(e As UON) e.CentroSM IsNot Nothing) Is Nothing))
                            If u2.ChildUONs.Count = 0 Then
                                u2.ChildUONs = Nothing
                            Else
                                For Each u3 As UON In u2.ChildUONs
                                    If u3.ChildUONs IsNot Nothing Then
                                        u3.ChildUONs.RemoveAll(Function(el As UON) _
                                                                el.CentroSM Is Nothing And _
                                                                (el.ChildUONs Is Nothing OrElse el.ChildUONs.FindInTree(Function(e As UON) e.CentroSM IsNot Nothing) Is Nothing))
                                        If u3.ChildUONs.Count = 0 Then
                                            u3.ChildUONs = Nothing
                                        End If
                                    End If
                                Next
                            End If
                        End If
                    Next
                End If
            End If
        Next
        Return arbol
    End Function

#End Region

#Region "Búsqueda Centros de coste"

    ''' Revisado por: blp. Fecha: 01/12/2011
    ''' <summary>
    ''' Método que recoge el valor de centro de coste seleccionado en el panel de Centros de coste, lo copia al campo de centro de coste correspondiente de la ventana
    ''' y crea o actualiza el atributo de centro por el que se filtrarán las partidas
    ''' </summary>
    ''' <param name="sender">Panel de centros que lanza el evento</param>
    ''' <param name="e">argumentos del evento</param>
    ''' <remarks>Llamada desde el evento aceptar del panel. Máx. 0,2 seg.</remarks>
    Private Sub _pnlCentrosCoste_Aceptar(ByVal sender As Object, ByVal e As System.EventArgs) Handles _pnlCentrosCoste.Aceptar
        _modalCentrosCoste.Hide()
        If _pnlCentrosCoste.CentroSM IsNot Nothing Then
            'En este caso, en el texto del centro ponemos la denominación del UONS porque en SMCentrosCoste._btnSeleccionar_Click hemos definido que en ese campo
            'Vaya el UON de nivel más bajo y no vacío/nulo al que corresponde el centro de coste. En realidad habría que usar los UON1, 2, etc. de UONS y coger el de nivel más bajo no nulo/vacío pero en SMCentrosCoste._btnSeleccionar_Click no tenemos esa información
            _txtCentro.Text = _pnlCentrosCoste.CentroSM.UONS.Denominacion & " - " & _pnlCentrosCoste.CentroSM.Denominacion
            If _txtCentro.Attributes("CENTROSM") Is Nothing Then
                _txtCentro.Attributes.Add("CENTROSM", _pnlCentrosCoste.CentroSM.Codigo)
            Else
                _txtCentro.Attributes("CENTROSM") = _pnlCentrosCoste.CentroSM.Codigo
            End If
            _upParametros.Update()
        End If
    End Sub

#End Region

#Region "Búsqueda"

    Private Sub _btnBuscar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _btnBuscar.Click
        ActualizarArbol()
    End Sub

#End Region

#Region "Aceptar/Cancelar"

    ''' Revisado por: blp. Fecha: 01/12/2011
    ''' <summary>
    ''' Lanza el evento Aceptar si hay una partida seleccionada
    ''' </summary>
    ''' <param name="sender">Origen del evento.</param>
    ''' <param name="e">Clase EventArgs que no contiene datos de evento.</param>
    ''' <remarks>Se produce cuando se hace clic en el control FSNButton btnSeleccionar. Máx. 0,2 seg.</remarks>
    Private Sub _btnSeleccionar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _btnSeleccionar.Click
        If _uwtCentrosPartidas.SelectedNodes IsNot Nothing AndAlso _uwtCentrosPartidas.SelectedNodes(0).ImageUrl = _urlImgCandado Then
            Dim sCodCentro = _uwtCentrosPartidas.SelectedNodes(0).ParentNode.Key
            Dim sCodPartida = _uwtCentrosPartidas.SelectedNodes(0).Key
            Dim arbol As UONs = ArbolConFiltros()
            Dim u As UON = arbol.FindInTree(Function(el As UON) el.CentroSM IsNot Nothing AndAlso el.CentroSM.Codigo = sCodCentro _
                                              AndAlso el.CentroSM.PartidasPresupuestarias IsNot Nothing _
                                              AndAlso el.CentroSM.PartidasPresupuestarias.Find(Function(par As PartidaPRES5) par.ToString() = sCodPartida) IsNot Nothing)
            If u IsNot Nothing Then
                _mCentroSM = u.CentroSM
                Dim oUON As FSNServer.UON = FSNServer.Get_Object(GetType(FSNServer.UON))
                'El objeto UON no se ha creado con la finalidad de albergar en la denominación el UON de nivel más bajo al que corresponde el centro de coste
                'Pero de momento sirve para corregir la manera de mostrar la información
                'En este caso, en vez de pasarse el valor a la propiedad "Denominación", podrían pasarse las UON1, 2, etc al objeto oUON y seleccionar luego en la página de destino el UON adecuado pero 
                'lo hacemos así para que funcione igual que en SMCentrosCoste->_btnSeleccionar_Click y minimizar las confusiones.
                oUON.Denominacion = If(u.UON4 <> String.Empty, u.UON4, If(u.UON3 <> String.Empty, u.UON3, If(u.UON2 <> String.Empty, u.UON2, If(u.UON1 <> String.Empty, u.UON1, String.Empty))))
                _mCentroSM.UONS = oUON
                _mPartida = u.CentroSM.PartidasPresupuestarias.Find(Function(par As PartidaPRES5) par.ToString() = sCodPartida)
                _mGestorCod = seleccionaGestor(_mPartida)
                OnAceptar(e)
            End If
        End If
    End Sub

    ''' <summary>
    ''' Lanza el evento Cancelar
    ''' </summary>
    ''' <param name="sender">Origen del evento.</param>
    ''' <param name="e">Clase EventArgs que no contiene datos de evento.</param>
    ''' <remarks>Se produce cuando se hace clic en el control FSNButton btnCancelar.</remarks>
    Private Sub _btnCancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _btnCancelar.Click
        _mCentroSM = Nothing
        _mPartida = Nothing
        _mGestorCod = String.Empty
        OnCancelar(e)
    End Sub

    ''' <summary>
    ''' Lanza el evento Cancelar
    ''' </summary>
    ''' <param name="sender">Origen del evento.</param>
    ''' <param name="e">ImageClickEventArgs que contiene datos de eventos.</param>
    ''' <remarks>Se produce cuando se hace clic en el control ImageButton.</remarks>
    Private Sub _imgCerrar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles _imgCerrar.Click
        _mCentroSM = Nothing
        _mPartida = Nothing
        _mGestorCod = String.Empty
        OnCancelar(e)
    End Sub

    ''' <summary>
    ''' Procedimiento que vacía de contenido los elementos del panel y lanza el evento Aceptar que se propagará hasta la página que contiene el panel
    ''' </summary>
    ''' <param name="e">Argumentos del evento Aceptar</param>
    ''' <remarks>llamado desde _btnSeleccionar_Click. Tiempo máximo: inferior a 0,1 seg</remarks>
    Protected Overridable Sub OnAceptar(ByVal e As EventArgs)
        _txtBuscar.Text = String.Empty
        _txtCentro.Text = String.Empty
        _cboGestor.SelectedIndex = -1
        If _txtCentro.Attributes("CENTROSM") IsNot Nothing Then _
            _txtCentro.Attributes.Remove("CENTROSM")
        _dteFechaInicioDesde.Value = Nothing
        _dteFechaHastaFin.Value = Nothing
        _chkPartidasNoVigentes.Checked = False
        RaiseEvent Aceptar(Me, e)
    End Sub

    ''' <summary>
    ''' Procedimiento que vacía de contenido los elementos del panel y lanza el evento Cancelar que se propagará hasta la página que contiene el panel
    ''' </summary>
    ''' <param name="e">Argumentos del evento Aceptar</param>
    ''' <remarks>llamado desde _btnSeleccionar_Click. Tiempo máximo: inferior a 0,1 seg</remarks>
    Protected Overridable Sub OnCancelar(ByVal e As EventArgs)
        _txtBuscar.Text = String.Empty
        _txtCentro.Text = String.Empty
        _cboGestor.SelectedIndex = -1
        If _txtCentro.Attributes("CENTROSM") IsNot Nothing Then _
            _txtCentro.Attributes.Remove("CENTROSM")
        _dteFechaInicioDesde.Value = Nothing
        _dteFechaHastaFin.Value = Nothing
        _chkPartidasNoVigentes.Checked = False
        RaiseEvent Cancelar(Me, e)
    End Sub

    Public Sub RaisePostBackEvent(ByVal eventArgument As String) Implements System.Web.UI.IPostBackEventHandler.RaisePostBackEvent
        _btnSeleccionar_Click(Me, New EventArgs())
        _btnCancelar_Click(Me, New EventArgs())
        _imgCerrar_Click(Me, New EventArgs())
    End Sub

#End Region

#Region "Control State"

    ''' <summary>
    ''' Registra el control como un control cuyo estado se debe conservar.
    ''' </summary>
    ''' <param name="e">Objeto EventArgs que contiene los datos del evento.</param>
    Protected Overrides Sub OnInit(ByVal e As System.EventArgs)
        MyBase.OnInit(e)
        Page.RegisterRequiresControlState(Me)
    End Sub

    ''' <summary>
    ''' Guarda los cambios de estado del control.
    ''' </summary>
    ''' <returns>Devuelve el estado actual del control de servidor.</returns>
    Protected Overrides Function SaveControlState() As Object
        Dim obj As Object = MyBase.SaveControlState()
        If obj IsNot Nothing Then
            obj = New Pair(obj, _sValor)
        Else
            obj = _sValor
        End If
        obj = New Pair(obj, _sPRES5)
        obj = New Pair(obj, _mFiltroCentroSM)
        obj = New Pair(obj, _sDenPartidas)
        Return obj
    End Function

    ''' <summary>
    ''' Restaura información de estado del control.
    ''' </summary>
    ''' <param name="state">Object que representa el estado del control que se va a restaurar.</param>
    Protected Overrides Sub LoadControlState(ByVal state As Object)
        If (state IsNot Nothing) Then
            Dim p As Pair = TryCast(state, Pair)
            _sDenPartidas = CType(p.Second, String)
            state = p.First
            p = TryCast(state, Pair)
            _mFiltroCentroSM = CType(p.Second, Centro_SM)
            state = p.First
            p = TryCast(state, Pair)
            _sPRES5 = CType(p.Second, String)
            state = p.First
            p = TryCast(state, Pair)
            If p IsNot Nothing Then
                MyBase.LoadControlState(p.First)
                _sValor = CType(p.Second, String)
            Else
                If (TypeOf (state) Is String) Then
                    _sValor = CType(state, String)
                Else
                    MyBase.LoadControlState(state)
                End If
            End If
        End If
    End Sub

#End Region

#Region "Métodos públicos"

    ''' <summary>
    ''' Evento que actualiza el arbol presupuestario. 
    ''' Debe lanzarse siempre que se vaya a mostrar el panel de Partidas presupuestarias
    ''' a fin de actualizar el árbol con los nuevos valores de las propiedades
    ''' </summary>
    ''' <remarks>Llamado desde todo lugar donde se desee actualizar el árbol presupuestario para mostrar el panel</remarks>
    Public Sub CargarDatos()
        If _bHayCambiosEnPropiedades Then
            ActualizarArbol()
            _bHayCambiosEnPropiedades = False
        End If
        cargarGestoresenCombo()
    End Sub

#End Region

End Class
