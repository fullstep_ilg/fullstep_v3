﻿Imports System.Web
Imports System.ComponentModel
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Fullstep.FSNServer
Imports Fullstep.FSNWebControls
Imports Fullstep.FSNLibrary
Imports System.Drawing
Imports System.Configuration

Public Class SMCentrosCoste
    Inherits Panel
    Implements IPostBackEventHandler

#Region "Variables privadas"

    ' Variables privadas para controles
    Private _lblTitulo As Label
    Private _lblCentro As Label
    Private WithEvents _btnSeleccionar As FSNButton
    Private WithEvents _btnCancelar As FSNButton
    Private _txtCentro As TextBox
    Private _uwtCentros As Global.Infragistics.Web.UI.NavigationControls.WebDataTree
    Private _imgCabecera As HtmlControls.HtmlImage
    Private WithEvents _imgCerrar As ImageButton
    Private _urlImgRoot As String
    Private _urlImgFolder As String
    Private _urlImgCentroCoste As String
    Private _uppnlCentros As UpdatePanel

    ' Variables privadas
    Private _sValor As String
    Private _bVerUON As Boolean
    Private _ListaCentros As List(Of ListItem)
    Private _mCentro As FSNServer.Centro_SM

    Private Const _ModuloIdioma As TiposDeDatos.ModulosIdiomas = TiposDeDatos.ModulosIdiomas.CentrosCoste

    Private ReadOnly Property Textos(ByVal iTexto As Integer) As String
        Get
            Dim pag As IFSNPage = CType(Me.Page, IFSNPage)
            If HttpContext.Current.Cache("Textos_" & pag.Idioma & "_" & _ModuloIdioma) Is Nothing Then
                Dim FSNDict As Dictionary
                If pag.FSNServer Is Nothing Then
                    FSNDict = New Dictionary()
                    FSNDict.LoadData(_ModuloIdioma, pag.Idioma)
                Else
                    FSNDict = pag.FSNServer.Get_Object(GetType(Dictionary))
                    FSNDict.LoadData(_ModuloIdioma, pag.Idioma)
                End If
                HttpContext.Current.Cache.Insert("Textos_" & pag.Idioma & "_" & _ModuloIdioma, FSNDict.Data, Nothing, Caching.Cache.NoAbsoluteExpiration, New TimeSpan(0, ConfigurationManager.AppSettings("TiempoExpiracionCacheMin"), 0))
            End If
            Return HttpContext.Current.Cache("Textos_" & pag.Idioma & "_" & _ModuloIdioma).Tables(0).Rows(iTexto).Item(1)
        End Get
    End Property

    Private ReadOnly Property FSNServer() As FSNServer.Root
        Get
            Return CType(HttpContext.Current.Session("FSN_Server"), FSNServer.Root)
        End Get
    End Property

#End Region

#Region "Propiedades públicas"

    Public Property Valor() As String
        Get
            Valor = _sValor
        End Get
        Set(ByVal Value As String)
            _sValor = Value
        End Set
    End Property

    Public Property VerUON() As Boolean
        Get
            VerUON = _bVerUON
        End Get
        Set(ByVal Value As Boolean)
            _bVerUON = Value
            ActualizarArbol()
        End Set
    End Property

    Public Property ListaCentros() As List(Of ListItem)
        Get
            Return _ListaCentros
        End Get
        Set(ByVal value As List(Of ListItem))
            _ListaCentros = value
            If _uwtCentros IsNot Nothing Then _
                ActualizarArbol()
        End Set
    End Property

    Public Property OnAceptarClientClick() As String
        Get
            If _btnSeleccionar IsNot Nothing Then
                Return _btnSeleccionar.OnClientClick
            Else
                Return Nothing
            End If
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            _btnSeleccionar.OnClientClick = value
        End Set
    End Property

    Public Property OnCancelarClientClick() As String
        Get
            If _btnCancelar IsNot Nothing Then
                Return _btnCancelar.OnClientClick
            Else
                Return Nothing
            End If
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            _btnCancelar.OnClientClick = value
            _imgCerrar.OnClientClick = value
        End Set
    End Property

    Public ReadOnly Property CentroSM() As FSNServer.Centro_SM
        Get
            Return _mCentro
        End Get
    End Property

#End Region

#Region "Eventos públicos"

    Public Event Aceptar(ByVal sender As Object, ByVal e As EventArgs)
    Public Event Cancelar(ByVal sender As Object, ByVal e As EventArgs)

#End Region

#Region "Carga inicial"

    ''' <summary>
    ''' Lo llama el marco de trabajo de las páginas ASP.NET para indicar a los controles de servidor que utilizan la implementación basada en la composición que creen los controles secundarios que contengan como forma de preparar la devolución o representación de los datos.
    ''' </summary>
    Protected Overrides Sub CreateChildControls()
        Me.Style.Add(HtmlTextWriterStyle.Padding, "6px")
        Me.Style.Add("min-width", "400px")
        Me.Style.Add("max-width", "90%")
        Me.Style.Add("min-height", "250px")
        Me.ScrollBars = WebControls.ScrollBars.Auto

        Dim tabla As New HtmlControls.HtmlTable()
        tabla.CellPadding = 4
        tabla.CellSpacing = 0
        tabla.Width = "100%"
        tabla.Border = 0
        Dim fila As New HtmlControls.HtmlTableRow()
        Dim celda As New HtmlControls.HtmlTableCell()
        celda.Width = "65"
        _imgCabecera = New HtmlControls.HtmlImage()
        _imgCabecera.ID = Me.ID & "_imgCabecera"
        _imgCabecera.Border = 0
        celda.Controls.Add(_imgCabecera)
        fila.Cells.Add(celda)
        celda = New HtmlControls.HtmlTableCell()
        _lblTitulo = New Label()
        _lblTitulo.ID = Me.ID & "_lblTitulo"
        _lblTitulo.CssClass = "RotuloGrande"
        celda.Controls.Add(_lblTitulo)
        fila.Cells.Add(celda)
        celda = New HtmlControls.HtmlTableCell()
        celda.Align = "right"
        celda.VAlign = "top"
        _imgCerrar = New ImageButton()
        _imgCerrar.ID = Me.ID & "_imgCerrar"
        _imgCerrar.SkinID = "Cerrar"
        celda.Controls.Add(_imgCerrar)
        fila.Cells.Add(celda)
        tabla.Rows.Add(fila)
        Me.Controls.Add(tabla)
        Me.Controls.Add(New LiteralControl("<br/>"))
        _uppnlCentros = New UpdatePanel()
        _uppnlCentros.ID = Me.ID & "_uppnlCentros"
        _uppnlCentros.UpdateMode = UpdatePanelUpdateMode.Conditional
        _lblCentro = New Label()
        _lblCentro.ID = Me.ID & "_lblCentro"
        _lblCentro.CssClass = "Etiqueta"
        _uppnlCentros.ContentTemplateContainer.Controls.Add(_lblCentro)
        _txtCentro = New TextBox()
        _txtCentro.ID = Me.ID & "_txtCentroSM"
        _txtCentro.Width = New Unit(400, UnitType.Pixel)
        _uppnlCentros.ContentTemplateContainer.Controls.Add(_txtCentro)
        _uppnlCentros.ContentTemplateContainer.Controls.Add(New LiteralControl("<br/><br/>"))
        Dim capa As New HtmlControls.HtmlGenericControl("div")
        capa.ID = Me.ID & "_dCamposFiltro"
        capa.Attributes.Add("class", "dContenedor")
        capa.Style.Add(HtmlTextWriterStyle.Height, "250px")
        capa.Style.Add(HtmlTextWriterStyle.Width, "95%")
        _uwtCentros = New Infragistics.Web.UI.NavigationControls.WebDataTree
        _uwtCentros.ID = Me.ID & "_uwtCentros"
        _uwtCentros.Width = New Unit(90, UnitType.Percentage)
        _uwtCentros.NodeSettings.SelectedCssClass = "NodeSelected"

        AddHandler _uwtCentros.NodeBound, AddressOf uwtCentros_NodeBound
        _uwtCentros.ClientEvents.Initialize = Me.ClientID & "_InitTree"
        capa.Controls.Add(_uwtCentros)
        _uppnlCentros.ContentTemplateContainer.Controls.Add(capa)
        Me.Controls.Add(_uppnlCentros)
        tabla = New HtmlControls.HtmlTable()
        tabla.Width = "100%"
        tabla.Border = 0
        fila = New HtmlControls.HtmlTableRow()
        celda = New HtmlControls.HtmlTableCell()
        celda.Align = "center"
        Dim tablain As New HtmlControls.HtmlTable()
        tablain.CellSpacing = 20
        tablain.Border = 0
        Dim filain As New HtmlControls.HtmlTableRow()
        Dim celdain As New HtmlControls.HtmlTableCell()
        celdain.Align = "left"
        _btnSeleccionar = New FSNButton()
        _btnSeleccionar.ID = Me.ID & "_btnSeleccionar"
        _btnSeleccionar.Alineacion = FSNTipos.Alineacion.Left
        celdain.Controls.Add(_btnSeleccionar)
        filain.Cells.Add(celdain)
        celdain = New HtmlControls.HtmlTableCell()
        celdain.Align = "right"
        _btnCancelar = New FSNButton()
        _btnCancelar.ID = Me.ID & "_btnCancelar"
        _btnCancelar.Alineacion = FSNTipos.Alineacion.Right
        celdain.Controls.Add(_btnCancelar)
        filain.Cells.Add(celdain)
        tablain.Rows.Add(filain)
        celda.Controls.Add(tablain)
        fila.Cells.Add(celda)
        tabla.Rows.Add(fila)
        Me.Controls.Add(tabla)
    End Sub

    ''' <summary>
    ''' Inicializa los controles
    ''' </summary>
    ''' <param name="sender">Origen del evento</param>
    ''' <param name="e">EventArgs que contiene datos de eventos.</param>
    ''' <remarks>Se produce cuando el control de servidor se carga en el objeto Page.</remarks>
    Private Sub SMCentrosCoste_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        EnsureChildControls()

        _lblTitulo.Text = Me.Textos(0)
        _lblCentro.Text = Me.Textos(1) & " "
        _btnSeleccionar.Text = Me.Textos(2)
        _btnCancelar.Text = Me.Textos(3)

        If String.IsNullOrEmpty(_urlImgFolder) And Me.Page IsNot Nothing Then
            _urlImgFolder = Me.Page.ClientScript.GetWebResourceUrl(GetType(SMCentrosCoste), "Fullstep.SMWebControls.carpetaNaranja.gif")
        End If
        If String.IsNullOrEmpty(_urlImgRoot) And Me.Page IsNot Nothing Then
            _urlImgRoot = Me.Page.ClientScript.GetWebResourceUrl(GetType(SMCentrosCoste), "Fullstep.SMWebControls.World.gif")
        End If
        If String.IsNullOrEmpty(_urlImgCentroCoste) And Me.Page IsNot Nothing Then
            _urlImgCentroCoste = Me.Page.ClientScript.GetWebResourceUrl(GetType(SMCentrosCoste), "Fullstep.SMWebControls.centrocoste_small.jpg")
        End If

        If Not Page.IsPostBack() Then
            If String.IsNullOrEmpty(_imgCabecera.Src) And Me.Page IsNot Nothing Then
                _imgCabecera.Src = Me.Page.ClientScript.GetWebResourceUrl(GetType(SMCentrosCoste), "Fullstep.SMWebControls.centrocoste.jpg")
            End If
            If String.IsNullOrEmpty(_imgCerrar.ImageUrl) And Me.Page IsNot Nothing Then
                _imgCerrar.ImageUrl = Me.Page.ClientScript.GetWebResourceUrl(GetType(SMCentrosCoste), "Fullstep.SMWebControls.Bt_Cerrar.png")
            End If

            'ActualizarArbol()

            _txtCentro.Attributes.Add("onkeyup", "sm_FindClicked(this, " & Me.ClientID & "_tree)")
        End If

        Me.Page.ClientScript.RegisterClientScriptResource(GetType(SMWebControlClientScript), "Fullstep.SMWebControls.SMControls.js")

        Dim sClientScript As String = "var " & Me.ClientID & "_tree;" &
            "function " & Me.ClientID & "_InitTree(tree,e){" &
                Me.ClientID & "_tree = $find(tree.get_id());" &
            "}"
        Me.Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), Me.ClientID & "_InicializarArbol", sClientScript, True)
        _btnSeleccionar.Attributes.Add("onclick", "guardarCentroActivoEP('" & Me.ClientID & "_uwtCentros', event)")
    End Sub

    ''' <summary>
    ''' Actualiza la información del árbol
    ''' </summary>
    Public Sub ActualizarArbol()
        If _uwtCentros IsNot Nothing Then
            If _ListaCentros Is Nothing Then
                CargarArbolCentros()
            Else
                CargarArbolConLista()
            End If
        End If
    End Sub

#End Region

#Region "Enlace a datos"

    ''' <summary>
    ''' Carga el árbol con consulta a base de datos
    ''' </summary>
    Private Sub CargarArbolCentros()
        Dim pag As IFSNPage = CType(Me.Page, IFSNPage)
        Dim DsCentrosDeCoste As DataSet

        If HttpContext.Current.Cache("DsCentrosDeCoste_" & pag.Usuario.CodPersona) Is Nothing Then
            Dim cCentros As Fullstep.FSNServer.Centros_SM
            cCentros = HttpContext.Current.Session("FSN_Server").Get_Object(GetType(FSNServer.Centros_SM))
            DsCentrosDeCoste = cCentros.LoadData(pag.Usuario.Cod, pag.Idioma, _bVerUON)
            HttpContext.Current.Cache.Insert("DsCentrosDeCoste_" & pag.Usuario.CodPersona, DsCentrosDeCoste, Nothing, Caching.Cache.NoAbsoluteExpiration, New TimeSpan(0, ConfigurationManager.AppSettings("TiempoExpiracionCacheMin"), 0))
        Else
            DsCentrosDeCoste = HttpContext.Current.Cache("DsCentrosDeCoste_" & pag.Usuario.CodPersona)
        End If

        Dim sCampoClave As String = IIf(_bVerUON, "COD", "CENTRO_SM")
        Dim sImagen As String = IIf(_bVerUON, _urlImgFolder, _urlImgCentroCoste)

        _uwtCentros.DataSource = DsCentrosDeCoste


        _uwtCentros.DataBind()

        For Each node As Infragistics.Web.UI.NavigationControls.DataTreeNode In _uwtCentros.AllNodes
            If node.Nodes.Count > 0 Then
                node.Expanded = True
            End If
        Next

        If _uwtCentros.Nodes.Count = 1 Then
            If _uwtCentros.Nodes(0).Nodes.Count = 1 Then
                _uwtCentros.Nodes(0).Nodes(0).Selected = True
            End If
        End If
        _txtCentro.Text = String.Empty
        _uppnlCentros.Update()
        _mCentro = Nothing
    End Sub

    ''' <summary>
    ''' Carga el árbol con los elementos de _ListaCentros
    ''' </summary>
    Private Sub CargarArbolConLista()
        _uwtCentros.Nodes.Clear()

        For Each node As Infragistics.Web.UI.NavigationControls.DataTreeNode In _uwtCentros.AllNodes
            Select Case node.Level
                Case 0
                    node.ImageUrl = _urlImgRoot
                Case 1
                    node.ImageUrl = _urlImgCentroCoste
            End Select
        Next
        Dim nodoroot As New Infragistics.Web.UI.NavigationControls.DataTreeNode()
        nodoroot.Expanded = True
        nodoroot.Text = Textos(5)
        _uwtCentros.Nodes.Add(nodoroot)
        For Each l As ListItem In _ListaCentros
            Dim nodo As New Infragistics.Web.UI.NavigationControls.DataTreeNode()
            nodo.Text = l.Text
            nodo.Key = l.Value
            nodo.Target = "1"
            nodoroot.Nodes.Add(nodo)
        Next
        _txtCentro.Text = String.Empty
        _uppnlCentros.Update()
        _mCentro = Nothing
    End Sub

    ''' <summary>
    ''' Establece las imágenes y el tag de los nodos Centros de Coste y selecciona el correspondiente según _sValor
    ''' </summary>
    ''' <param name="sender">Origen del evento.</param>
    ''' <param name="e">Clase WebTreeNodeEventArgs que no contiene datos de evento.</param>
    Private Sub uwtCentros_NodeBound(ByVal sender As Object, ByVal e As Infragistics.Web.UI.NavigationControls.DataTreeNodeEventArgs)
        If _bVerUON Then
            Dim ds As DataSet
            Dim oRows() As DataRow

            ds = sender.datasource.dataviewmanager.dataset

            Select Case e.Node.Level
                Case 1
                    oRows = ds.Tables(e.Node.Level).Select("COD= '" & e.Node.Key & "'")
                Case 2
                    oRows = ds.Tables(e.Node.Level).Select("UON1= '" & e.Node.ParentNode.Key & "' AND COD= '" & e.Node.Key & "'")
                Case 3
                    oRows = ds.Tables(e.Node.Level).Select("UON1= '" & e.Node.ParentNode.ParentNode.Key & "' AND UON2= '" & e.Node.ParentNode.Key & "' AND COD= '" & e.Node.Key & "'")
                Case 4
                    oRows = ds.Tables(e.Node.Level).Select("UON1= '" & e.Node.ParentNode.ParentNode.ParentNode.Key & "' AND UON2= '" & e.Node.ParentNode.ParentNode.Key & "' AND UON3= '" & e.Node.ParentNode.Key & "' AND COD='" & e.Node.Key & "'")
                Case Else
                    oRows = Nothing
            End Select

            If e.Node.Level > 0 AndAlso oRows.Length > 0 Then
                If Not IsDBNull(oRows(0).Item("CENTRO_SM")) Then
                    e.Node.ImageUrl = _urlImgCentroCoste
                    e.Node.Target = 1
                End If
            End If

            If e.Node.Level = 0 AndAlso Not _bVerUON Then
                e.Node.Text = Me.Textos(5)
            End If

            If _sValor <> Nothing Then
                Dim sUnidadOrg As Object = Split(_sValor, "#")
                Dim sUON1 As String = ""
                Dim sUON2 As String = ""
                Dim sUON3 As String = ""
                Dim sUON4 As String = ""
                Dim iNivel As Integer = UBound(sUnidadOrg) + 1

                Select Case iNivel
                    Case 1
                        sUON1 = sUnidadOrg(0)
                    Case 2
                        sUON1 = sUnidadOrg(0)
                        sUON2 = sUnidadOrg(1)
                    Case 3
                        sUON1 = sUnidadOrg(0)
                        sUON2 = sUnidadOrg(1)
                        sUON3 = sUnidadOrg(2)
                    Case 4
                        sUON1 = sUnidadOrg(0)
                        sUON2 = sUnidadOrg(1)
                        sUON3 = sUnidadOrg(2)
                        sUON4 = sUnidadOrg(3)
                End Select

                If e.Node.Level = iNivel Then
                    If iNivel = 4 Then
                        If e.Node.Key = sUON4 And e.Node.ParentNode.Key = sUON3 And e.Node.ParentNode.ParentNode.Key = sUON2 And e.Node.ParentNode.ParentNode.ParentNode.Key = sUON1 Then
                            e.Node.ParentNode.Expanded = True
                            e.Node.ParentNode.ParentNode.Expanded = True
                            e.Node.Selected = True
                        End If
                    ElseIf iNivel = 3 Then
                        If e.Node.Key = sUON3 And e.Node.ParentNode.Key = sUON2 And e.Node.ParentNode.ParentNode.Key = sUON1 Then
                            e.Node.ParentNode.Expanded = True
                            e.Node.ParentNode.ParentNode.Expanded = True
                            e.Node.Selected = True
                        End If
                    ElseIf iNivel = 2 Then
                        If e.Node.Key = sUON2 And e.Node.ParentNode.Key = sUON1 Then
                            e.Node.ParentNode.Expanded = True
                            e.Node.Selected = True
                        End If
                    ElseIf iNivel = 1 Then
                        If e.Node.Key = sUON1 Then
                            e.Node.Selected = True
                        End If
                    End If
                End If
            End If
        Else
            If e.Node.Level > 0 Then
                e.Node.Target = 1
            End If
        End If
    End Sub

#End Region

#Region "Aceptar/Cancelar"

    ''' Revisado por: blp. fecha: 29/11/2011
    ''' <summary>
    ''' Lanza el evento Aceptar si hay un centro seleccionado
    ''' </summary>
    ''' <param name="sender">Origen del evento.</param>
    ''' <param name="e">Clase EventArgs que no contiene datos de evento.</param>
    ''' <remarks>Se produce cuando se hace clic en el control FSNButton btnSeleccionar. MÃ¡x 0,1 seg</remarks>
    Private Sub _btnSeleccionar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _btnSeleccionar.Click
        If _uwtCentros.SelectedNodes IsNot Nothing AndAlso _uwtCentros.SelectedNodes(0).Target = 1 Then
            _mCentro = FSNServer.Get_Object(GetType(FSNServer.Centro_SM))
            _mCentro.Codigo = _uwtCentros.SelectedNodes(0).Key
            _mCentro.Denominacion = Right(_uwtCentros.SelectedNodes(0).Text, Len(_uwtCentros.SelectedNodes(0).Text) - _uwtCentros.SelectedNodes(0).Text.IndexOf(" - ") - 3)
            Dim oUON As FSNServer.UON = FSNServer.Get_Object(GetType(FSNServer.UON))
            'El objeto UON no se ha creado con la finalidad de albergar en la denominación el UON de nivel más bajo al que corresponde el centro de coste
            'Pero de momento sirve para corregir la manera de mostrar la información
            oUON.Denominacion = Left(_uwtCentros.SelectedNodes(0).Text, _uwtCentros.SelectedNodes(0).Text.IndexOf(" - "))
            _mCentro.UONS = oUON
            OnAceptar(e)
        End If
    End Sub

    Protected Overridable Sub OnAceptar(ByVal e As EventArgs)
        _txtCentro.Text = String.Empty
        _uppnlCentros.Update()
        RaiseEvent Aceptar(Me, e)
    End Sub

    ''' <summary>
    ''' Lanza el evento Cancelar
    ''' </summary>
    ''' <param name="sender">Origen del evento.</param>
    ''' <param name="e">Clase EventArgs que no contiene datos de evento.</param>
    ''' <remarks>Se produce cuando se hace clic en el control FSNButton btnCancelar.</remarks>
    Private Sub _btnCancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _btnCancelar.Click
        _mCentro = Nothing
        OnCancelar(e)
    End Sub

    ''' <summary>
    ''' Lanza el evento Cancelar
    ''' </summary>
    ''' <param name="sender">Origen del evento.</param>
    ''' <param name="e">ImageClickEventArgs que contiene datos de eventos.</param>
    ''' <remarks>Se produce cuando se hace clic en el control ImageButton.</remarks>
    Private Sub _imgCerrar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _imgCerrar.Click
        _mCentro = Nothing
        OnCancelar(e)
    End Sub

    Protected Overridable Sub OnCancelar(ByVal e As EventArgs)
        _txtCentro.Text = String.Empty
        _uppnlCentros.Update()
        RaiseEvent Cancelar(Me, e)
    End Sub

    Public Sub RaisePostBackEvent(ByVal eventArgument As String) Implements System.Web.UI.IPostBackEventHandler.RaisePostBackEvent
        _btnSeleccionar_Click(Me, New EventArgs())
        _btnCancelar_Click(Me, New EventArgs())
        _imgCerrar_Click(Me, New EventArgs())
    End Sub

#End Region

End Class
