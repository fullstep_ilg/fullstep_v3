﻿Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Text
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Fullstep.FSNLibrary
Imports Fullstep.FSNWebControls
Imports Fullstep.FSNServer
Imports System.Drawing
Imports System.Configuration

Public Class SMActivos
    Inherits Panel
    Implements IPostBackEventHandler


#Region "Templates del grid"

    Private Class gridActivosEmptyTemplate
        Implements ITemplate

        ''' <summary>
        ''' Define el objeto Control al que pertenecen los controles secundarios y las plantillas. Estos controles secundarios están a su vez definidos en una plantilla en línea.
        ''' </summary>
        ''' <param name="container">Objeto Control que contiene las instancias de los controles de la plantilla en línea.</param>
        Public Sub InstantiateIn(ByVal container As System.Web.UI.Control) Implements System.Web.UI.ITemplate.InstantiateIn
            Dim lblText As New Label()
            lblText.ID = "_lblEmptyDataText"
            container.Controls.Add(lblText)
        End Sub

    End Class

    Private Class gridActivosPagerTemplate
        Implements ITemplate

        ''' <summary>
        ''' Define el objeto Control al que pertenecen los controles secundarios y las plantillas. Estos controles secundarios están a su vez definidos en una plantilla en línea.
        ''' </summary>
        ''' <param name="container">Objeto Control que contiene las instancias de los controles de la plantilla en línea.</param>
        Public Sub InstantiateIn(ByVal container As System.Web.UI.Control) Implements System.Web.UI.ITemplate.InstantiateIn
            Dim pnlPaginador As New Panel()
            pnlPaginador.ID = "_pnlPaginador"
            pnlPaginador.Height = New Unit(29, UnitType.Pixel)
            pnlPaginador.Width = New Unit(100, UnitType.Percentage)
            Dim tabla As New HtmlControls.HtmlTable()
            tabla.Border = 0
            tabla.Width = "100%"
            tabla.CellPadding = 0
            Dim fila As New HtmlControls.HtmlTableRow()
            Dim celda As New HtmlControls.HtmlTableCell()
            Dim tablain As New HtmlControls.HtmlTable()
            tablain.CellPadding = 2
            Dim filain As New HtmlControls.HtmlTableRow()
            Dim celdain As New HtmlControls.HtmlTableCell()
            Dim imgbtn As New ImageButton()
            imgbtn.ID = "_imgbtnFirst"
            imgbtn.CommandArgument = "first"
            imgbtn.CommandName = "page"
            celdain.Controls.Add(imgbtn)
            filain.Cells.Add(celdain)
            celdain = New HtmlControls.HtmlTableCell()
            imgbtn = New ImageButton()
            imgbtn.ID = "_imgbtnPrev"
            imgbtn.CommandArgument = "prev"
            imgbtn.CommandName = "page"
            celdain.Controls.Add(imgbtn)
            filain.Cells.Add(celdain)
            celdain = New HtmlControls.HtmlTableCell()
            Dim lbl As New Label()
            lbl.ID = "_lblPagina"
            lbl.Font.Size = New FontUnit(FontSize.Small)
            lbl.ForeColor = Drawing.ColorTranslator.FromHtml("#535353")
            lbl.Width = New Unit(42, UnitType.Pixel)
            celdain.Controls.Add(lbl)
            filain.Cells.Add(celdain)
            celdain = New HtmlControls.HtmlTableCell()
            Dim ddl As New DropDownList()
            ddl.ID = "_ddlNumPags"
            ddl.AutoPostBack = True
            ddl.Width = New Unit(48, UnitType.Pixel)
            celdain.Controls.Add(ddl)
            filain.Cells.Add(celdain)
            celdain = New HtmlControls.HtmlTableCell()
            lbl = New Label()
            lbl.ID = "_lblDe"
            lbl.Font.Size = New FontUnit(FontSize.Small)
            lbl.ForeColor = Drawing.ColorTranslator.FromHtml("#535353")
            celdain.Controls.Add(lbl)
            celdain.Controls.Add(New LiteralControl("&nbsp;"))
            lbl = New Label()
            lbl.ID = "_lblTotal"
            lbl.Font.Size = New FontUnit(FontSize.Small)
            lbl.ForeColor = Drawing.ColorTranslator.FromHtml("#535353")
            celdain.Controls.Add(lbl)
            filain.Cells.Add(celdain)
            celdain = New HtmlControls.HtmlTableCell()
            imgbtn = New ImageButton()
            imgbtn.ID = "_imgbtnNext"
            imgbtn.CommandArgument = "next"
            imgbtn.CommandName = "page"
            celdain.Controls.Add(imgbtn)
            filain.Cells.Add(celdain)
            celdain = New HtmlControls.HtmlTableCell()
            imgbtn = New ImageButton()
            imgbtn.ID = "_imgbtnLast"
            imgbtn.CommandArgument = "last"
            imgbtn.CommandName = "page"
            celdain.Controls.Add(imgbtn)
            filain.Cells.Add(celdain)
            tablain.Rows.Add(filain)
            celda.Controls.Add(tablain)
            fila.Cells.Add(celda)
            celda = New HtmlControls.HtmlTableCell()
            celda.NoWrap = True
            celda.Align = "right"
            Dim pnl As New Panel()
            pnl.ID = "_pnlOrdenacion"
            pnl.Visible = True
            pnl.Width = New Unit(100, UnitType.Percentage)
            tablain = New HtmlControls.HtmlTable()
            tablain.Border = 0
            tablain.CellPadding = 2
            tablain.CellSpacing = 0
            filain = New HtmlControls.HtmlTableRow()
            celdain = New HtmlControls.HtmlTableCell()
            celdain.NoWrap = True
            lbl = New Label()
            lbl.ID = "_lblOrdenarPor"
            lbl.CssClass = "Etiqueta"
            celdain.Controls.Add(lbl)
            celdain.Controls.Add(New LiteralControl("&nbsp;"))
            filain.Cells.Add(celdain)
            celdain = New HtmlControls.HtmlTableCell()
            ddl = New DropDownList()
            ddl.ID = "_ddlOrdenarPor"
            ddl.AutoPostBack = True
            ddl.Width = New Unit(150, UnitType.Pixel)
            AddHandler ddl.SelectedIndexChanged, AddressOf ddlOrdenarPor_SelectedIndexChanged
            celdain.Controls.Add(ddl)
            filain.Cells.Add(celdain)
            celdain = New HtmlControls.HtmlTableCell()
            imgbtn = New ImageButton()
            imgbtn.ID = "_imgbtnOrdenacion"
            imgbtn.CommandName = "sort"
            celdain.Controls.Add(imgbtn)
            filain.Cells.Add(celdain)
            tablain.Rows.Add(filain)
            pnl.Controls.Add(tablain)
            celda.Controls.Add(pnl)
            fila.Cells.Add(celda)
            tabla.Rows.Add(fila)
            pnlPaginador.Controls.Add(tabla)
            container.Controls.Add(pnlPaginador)
        End Sub

        ''' <summary>
        ''' Reordena el datagrid según el campo seleccionado
        ''' </summary>
        ''' <param name="sender">Origen del evento.</param>
        ''' <param name="e">Objeto System.EventArgs que contiene los datos de eventos. </param>
        ''' <remarks>Se produce cuando la selección del control de lista cambia entre cada envío al servidor.</remarks>
        Protected Sub ddlOrdenarPor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim ddl As DropDownList = CType(sender, DropDownList)
            Dim container As GridView = CType(ddl.NamingContainer.NamingContainer, GridView)
            container.Sort(ddl.SelectedValue, container.SortDirection)
        End Sub

    End Class

#End Region

#Region "Variables privadas"

    ' Variables privadas para controles
    Private _lblTitulo As Label
    Private _lblBusquedaAvanzada As Label
    Private _lblCodigo As Label
    Private _lblDenominacion As Label
    Private _lblEmpresa As Label
    Private _lblEmpresaPredef As Label
    Private _lblCentro As Label
    Private _lblCentroPredef As Label
    Private _lblNumeroERP As Label
    Private _lblProcesando As Label
    Private _txtCodigo As TextBox
    Private _txtDenominacion As TextBox
    Private _txtCentro As TextBox
    Private _txtNumeroERP As TextBox
    Private _imgCabecera As HtmlControls.HtmlImage
    Private _imgExpandir As WebControls.Image
    Private _imgProgress As WebControls.Image
    Private WithEvents _imgCerrar As ImageButton
    Private _imgbtnBuscarCentro As ImageButton
    Private WithEvents _btnBuscar As FSNButton
    Private WithEvents _btnAceptar As FSNButton
    Private WithEvents _btnCancelar As FSNButton
    Private _ddlEmpresa As DropDownList
    Private WithEvents _gridActivos As GridView
    Private _pnlBusquedaAvanzada As Panel
    Private _pnlParametros As Panel
    Private _upnlParametros As UpdatePanel
    Private _pnlUpdateProgress As Panel
    Private _upnlActivos As UpdatePanel
    Private _autoCompletarCodigo As AjaxControlToolkit.AutoCompleteExtender
    Private _autoCompletarDenominacion As AjaxControlToolkit.AutoCompleteExtender
    Private _autoCompletarCentro As AjaxControlToolkit.AutoCompleteExtender
    Private _autoCompletarNumeroERP As AjaxControlToolkit.AutoCompleteExtender
    Private _collapseParametros As AjaxControlToolkit.CollapsiblePanelExtender
    Private _modalProgress As AjaxControlToolkit.ModalPopupExtender
    Private WithEvents _pnlCentrosCoste As SMCentrosCoste
    Private _modalCentrosCoste As AjaxControlToolkit.ModalPopupExtender

    ' Variables privadas
    Private _bHayIntegracionSalida As Boolean
    Private IdentificadorActivosEnCache As String
    Private IdentificadorUltimaBusquedaEnCache As String
    Private _Idioma As FSNLibrary.Idioma
    Private OrderByFieldCookieName As String
    Private OrderByDirectionCookieName As String
    Private bResetearControlAlCargarDatos As Boolean = False

    ' Variables de propiedades públicas
    Private _mCentroSM As FSNServer.Centro_SM
    Private _mUsuarioPM As String
    Private _mActivo As FSNServer.Activo
    Private _mUON As FSNServer.UON
    Private _mEmpresa As FSNServer.CEmpresa
    Private _mAllowSelection As Boolean

    Private Const _ModuloIdioma As TiposDeDatos.ModulosIdiomas = TiposDeDatos.ModulosIdiomas.Activos

    Private ReadOnly Property FSNServer() As FSNServer.Root
        Get
            Return CType(HttpContext.Current.Session("FSN_Server"), FSNServer.Root)
        End Get
    End Property

    Private ReadOnly Property Textos(ByVal iTexto As Integer) As String
        Get
            Dim pag As IFSNPage = CType(Me.Page, IFSNPage)
            If HttpContext.Current.Cache("Textos_" & pag.Idioma & "_" & _ModuloIdioma) Is Nothing Then
                Dim FSNDict As Dictionary
                If pag.FSNServer Is Nothing Then
                    FSNDict = New Dictionary()
                    FSNDict.LoadData(_ModuloIdioma, pag.Idioma)
                Else
                    FSNDict = pag.FSNServer.Get_Object(GetType(Dictionary))
                    FSNDict.LoadData(_ModuloIdioma, pag.Idioma)
                End If
                HttpContext.Current.Cache.Insert("Textos_" & pag.Idioma & "_" & _ModuloIdioma, FSNDict.Data, Nothing, Caching.Cache.NoAbsoluteExpiration,
                                                 New TimeSpan(0, ConfigurationManager.AppSettings("TiempoExpiracionCacheMin"), 0))
            End If
            Return HttpContext.Current.Cache("Textos_" & pag.Idioma & "_" & _ModuloIdioma).Tables(0).Rows(iTexto).Item(1)
        End Get
    End Property

    ''' <summary>
    ''' Devuelve todos los activos. El resultado se almacena en la caché.
    ''' </summary>
    ''' <returns>Un DataTable con los activos</returns>
    ''' <remarks>Tiempo máximo: 3 sec</remarks>
    Private ReadOnly Property ActivosUsu() As DataTable
        Get
            If HttpContext.Current.Cache(IdentificadorActivosEnCache) Is Nothing Then
                Dim oActivos As FSNServer.Activos = FSNServer.Get_Object(GetType(FSNServer.Activos))

                Dim dt As DataTable
                'De momento se presupone que si hay usuario de PM se esta utilizando el buscador (m_bAllowSelection=true) (propiedad)
                If Not String.IsNullOrEmpty(_mUsuarioPM) Then
                    If Not String.IsNullOrEmpty(_mUON.UON1) Then
                        If _lblEmpresaPredef.Visible Then
                            dt = oActivos.ObtenerActivos_CC_Imputacion(_Idioma.ToString(), _mUsuarioPM, _mUON.UON1, _mUON.UON2, _mUON.UON3, _mUON.UON4, _mEmpresa.ID)
                        Else
                            dt = oActivos.ObtenerActivos_CC_Imputacion(_Idioma.ToString(), _mUsuarioPM, _mUON.UON1, _mUON.UON2, _mUON.UON3, _mUON.UON4)
                        End If
                    Else
                        If _lblEmpresaPredef.Visible Then
                            dt = oActivos.ObtenerActivos_CC_Imputacion(_Idioma.ToString(), _mUsuarioPM, , , , , _mEmpresa.ID)
                        Else
                            dt = oActivos.ObtenerActivos_CC_Imputacion(_Idioma.ToString(), _mUsuarioPM)
                        End If
                    End If

                    If dt IsNot Nothing _
                        AndAlso dt.Rows.Count > 0 _
                        AndAlso _mCentroSM IsNot Nothing _
                        AndAlso Not String.IsNullOrEmpty(_mCentroSM.Codigo) Then

                        Dim query = From activos As DataRow In dt.Rows
                                    Where activos.Item("CENTRO_SM") = _mCentroSM.Codigo
                                    Select activos
                        If query.Any Then
                            Dim dtFiltrado As New DataTable
                            dtFiltrado = dt.Clone()
                            For Each act In query
                                dtFiltrado.ImportRow(act)
                            Next
                            dt.Clear()
                            dt = dtFiltrado.Copy()
                        Else
                            dt.Clear()
                        End If

                    End If
                Else
                    dt = oActivos.ObtenerActivos(_Idioma.ToString(),
                                               IIf(_lblEmpresaPredef.Visible, _mEmpresa.ID, Nothing),
                                               IIf(_lblCentroPredef.Visible, _mCentroSM.Codigo, Nothing))
                End If
                Dim key() As DataColumn = {dt.Columns("ID")}
                dt.PrimaryKey = key
                HttpContext.Current.Cache.Insert(IdentificadorActivosEnCache, dt, Nothing, Caching.Cache.NoAbsoluteExpiration, New TimeSpan(0, ConfigurationManager.AppSettings("TiempoExpiracionCacheMin"), 0))
            End If
            Return HttpContext.Current.Cache(IdentificadorActivosEnCache)
        End Get
    End Property

#Region "Ordenación"

    Private Const DefaultOrderByField As String = "COD"

    ''' <summary>
    ''' Devuelve o establece el campo por el cual se ordena el grid de Activos
    ''' </summary>
    ''' <returns>Un string con el nombre del campo</returns>
    Private Property CampoOrdenacion() As String
        Get
            If String.IsNullOrEmpty(ViewState.Item("CampoOrdenacion")) Then
                Return DefaultOrderByField
            Else
                Return ViewState.Item("CampoOrdenacion")
            End If
        End Get
        Set(ByVal value As String)
            ViewState.Item("CampoOrdenacion") = value
        End Set
    End Property

    ''' <summary>
    ''' Devuelve o establece el sentido de ordenación del grid de Activos
    ''' </summary>
    ''' <value></value>
    ''' <returns>Un valor SortDirection con el sentido de ordenación</returns>
    Private Property SentidoOrdenacion() As SortDirection
        Get
            If ViewState.Item("SentidoOrdenacion") Is Nothing Then
                Return SortDirection.Ascending
            Else
                Return CType(ViewState.Item("SentidoOrdenacion"), SortDirection)
            End If
        End Get
        Set(ByVal value As SortDirection)
            ViewState.Item("SentidoOrdenacion") = value
        End Set
    End Property

#End Region

#End Region

#Region "Propiedades públicas"

    Public WriteOnly Property CentroSM() As FSNServer.Centro_SM
        Set(ByVal value As FSNServer.Centro_SM)
            _mCentroSM = value
            bResetearControlAlCargarDatos = True
        End Set
    End Property

    Public Property UsuarioPM() As String
        Get
            Return _mUsuarioPM
        End Get
        Set(ByVal value As String)
            _mUsuarioPM = value
            bResetearControlAlCargarDatos = True
        End Set
    End Property

    Public ReadOnly Property Activo() As FSNServer.Activo
        Get
            Return _mActivo
        End Get
    End Property

    Public WriteOnly Property UON() As FSNServer.UON
        Set(ByVal value As FSNServer.UON)
            _mUON = value
            bResetearControlAlCargarDatos = True
        End Set
    End Property

    Public WriteOnly Property Empresa() As FSNServer.CEmpresa
        Set(ByVal value As FSNServer.CEmpresa)
            _mEmpresa = value
            bResetearControlAlCargarDatos = True
        End Set
    End Property

    Public Property AllowSelection() As Boolean
        Get
            Return _mAllowSelection
        End Get
        Set(ByVal value As Boolean)
            _mAllowSelection = value
        End Set
    End Property

    Public Property OnAceptarClientClick() As String
        Get
            If _btnAceptar IsNot Nothing Then
                Return _btnAceptar.OnClientClick
            Else
                Return Nothing
            End If
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            _btnAceptar.OnClientClick = value
        End Set
    End Property

    Public Property OnCancelarClientClick() As String
        Get
            If _btnCancelar IsNot Nothing Then
                Return _btnCancelar.OnClientClick
            Else
                Return Nothing
            End If
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            _btnCancelar.OnClientClick = value
            _imgCerrar.OnClientClick = value
        End Set
    End Property

#End Region

#Region "Métodos públicos"

    ''' <summary>
    ''' Evento que actualiza el arbol presupuestario. 
    ''' Debe lanzarse siempre que se vaya a mostrar el panel de Partidas presupuestarias
    ''' a fin de actualizar el árbol con los nuevos valores de las propiedades
    ''' </summary>
    ''' <remarks>Llamado desde todo lugar donde se desee actualizar el árbol presupuestario para mostrar el panel</remarks>
    Public Sub CargarDatos()
        If bResetearControlAlCargarDatos Then
            ResetControl()
            bResetearControlAlCargarDatos = False
        End If
        _mActivo.ID = 0
        _gridActivos.DataSource = GetGridDataSource()
        _gridActivos.DataBind()
        _pnlCentrosCoste.ListaCentros = GetListaCentrosCoste()
        _upnlActivos.Update()
    End Sub

#End Region

#Region "Eventos públicos"

    Public Event Aceptar(ByVal sender As Object, ByVal e As EventArgs)
    Public Event Cancelar(ByVal sender As Object, ByVal e As EventArgs)

#End Region

#Region "Carga inicial"

    ''' <summary>
    ''' Lo llama el marco de trabajo de las páginas ASP.NET para indicar a los controles de servidor que utilizan la implementación basada en la composición que creen los controles secundarios que contengan como forma de preparar la devolución o representación de los datos.
    ''' </summary>
    Protected Overrides Sub CreateChildControls()
        Me.Style.Add(HtmlTextWriterStyle.Padding, "6px")
        Me.Style.Add("min-width", "700px")
        Me.Style.Add("max-width", "90%")
        Me.Style.Add("min-height", "540px")
        Me.ScrollBars = WebControls.ScrollBars.Auto

        ' Panel de búsqueda de centros de coste
        _pnlCentrosCoste = New SMCentrosCoste()
        _pnlCentrosCoste.ID = Me.ID & "_pnlCentrosCoste"
        _pnlCentrosCoste.BackColor = Drawing.Color.White
        _pnlCentrosCoste.Style.Add("min-width", "500px")
        _pnlCentrosCoste.Style.Add("max-width", "80%")
        If Not Me.Width.IsEmpty Then _
            _pnlCentrosCoste.Width = New Unit(CInt(Me.Width.Value * 0.8), Me.Width.Type)
        _pnlCentrosCoste.BorderColor = Drawing.Color.DimGray
        _pnlCentrosCoste.BorderWidth = New Unit(1, UnitType.Pixel)
        _pnlCentrosCoste.BorderStyle = WebControls.BorderStyle.Solid
        _pnlCentrosCoste.Style.Add(HtmlTextWriterStyle.Display, "none")
        _pnlCentrosCoste.VerUON = False
        Me.Controls.Add(_pnlCentrosCoste)

        Dim tabla As New HtmlControls.HtmlTable()
        tabla.CellPadding = 4
        tabla.CellSpacing = 0
        tabla.Width = "100%"
        tabla.Border = 0
        Dim fila As New HtmlControls.HtmlTableRow()
        Dim celda As New HtmlControls.HtmlTableCell()
        celda.Width = "65"
        _imgCabecera = New HtmlControls.HtmlImage()
        _imgCabecera.ID = Me.ID & "_imgCabecera"
        _imgCabecera.Border = 0
        celda.Controls.Add(_imgCabecera)
        fila.Cells.Add(celda)
        celda = New HtmlControls.HtmlTableCell()
        _lblTitulo = New Label()
        _lblTitulo.ID = Me.ID & "_lblTitulo"
        _lblTitulo.CssClass = "RotuloGrande"
        celda.Controls.Add(_lblTitulo)
        fila.Cells.Add(celda)
        celda = New HtmlControls.HtmlTableCell()
        celda.Align = "right"
        celda.VAlign = "top"
        _imgCerrar = New ImageButton()
        _imgCerrar.ID = Me.ID & "_imgCerrar"
        _imgCerrar.SkinID = "Cerrar"
        celda.Controls.Add(_imgCerrar)
        fila.Cells.Add(celda)
        tabla.Rows.Add(fila)
        Me.Controls.Add(tabla)
        _pnlBusquedaAvanzada = New Panel()
        _pnlBusquedaAvanzada.ID = Me.ID & "_pnlBusquedaAvanzada"
        _pnlBusquedaAvanzada.Style.Add(HtmlTextWriterStyle.Padding, "8px")
        _pnlBusquedaAvanzada.Style.Add(HtmlTextWriterStyle.Cursor, "pointer")
        _imgExpandir = New WebControls.Image()
        _imgExpandir.ID = Me.ID & "_imgExpandir"
        _imgExpandir.SkinID = "Expandir"
        _pnlBusquedaAvanzada.Controls.Add(_imgExpandir)
        _pnlBusquedaAvanzada.Controls.Add(New LiteralControl("&nbsp;&nbsp;"))
        _lblBusquedaAvanzada = New Label()
        _lblBusquedaAvanzada.ID = Me.ID & "_lblBusquedaAvanzada"
        _lblBusquedaAvanzada.CssClass = "Rotulo"
        _pnlBusquedaAvanzada.Controls.Add(_lblBusquedaAvanzada)
        Me.Controls.Add(_pnlBusquedaAvanzada)
        _pnlParametros = New Panel()
        _pnlParametros.ID = Me.ID & "_pnlParametros"
        _pnlParametros.CssClass = "Rectangulo"
        _upnlParametros = New UpdatePanel()
        _upnlParametros.ID = Me.ID & "_upnlParametros"
        _upnlParametros.UpdateMode = UpdatePanelUpdateMode.Conditional
        _upnlParametros.ChildrenAsTriggers = True
        tabla = New HtmlControls.HtmlTable()
        tabla.Width = "100%"
        fila = New HtmlControls.HtmlTableRow()
        celda = New HtmlControls.HtmlTableCell()
        celda.Width = "153px"
        _lblCodigo = New Label()
        _lblCodigo.ID = Me.ID & "_lblCodigo"
        _lblCodigo.CssClass = "Etiqueta"
        celda.Controls.Add(_lblCodigo)
        fila.Cells.Add(celda)
        celda = New HtmlControls.HtmlTableCell()
        celda.Width = "237px"
        _txtCodigo = New TextBox()
        _txtCodigo.ID = Me.ID & "_txtCodigo"
        _txtCodigo.Width = New Unit(235, UnitType.Pixel)
        celda.Controls.Add(_txtCodigo)
        _autoCompletarCodigo = New AjaxControlToolkit.AutoCompleteExtender()
        _autoCompletarCodigo.ID = Me.ID & "_autoCompletarCodigo"
        _autoCompletarCodigo.TargetControlID = _txtCodigo.ID
        _autoCompletarCodigo.CompletionInterval = 500
        _autoCompletarCodigo.CompletionSetCount = 10
        _autoCompletarCodigo.MinimumPrefixLength = 3
        _autoCompletarCodigo.EnableCaching = False
        _autoCompletarCodigo.UseContextKey = True
        _autoCompletarCodigo.ServicePath = "WSSMControls.asmx"
        _autoCompletarCodigo.ServiceMethod = "Autocompletar_Codigo"
        _autoCompletarCodigo.OnClientShown = "sm_SetZIndex"
        celda.Controls.Add(_autoCompletarCodigo)
        fila.Cells.Add(celda)
        celda = New HtmlControls.HtmlTableCell()
        celda.Width = "200px"
        _lblDenominacion = New Label()
        _lblDenominacion.ID = Me.ID & "_lblDenominacion"
        _lblDenominacion.CssClass = "Etiqueta"
        celda.Controls.Add(_lblDenominacion)
        fila.Cells.Add(celda)
        celda = New HtmlControls.HtmlTableCell()
        celda.Width = "237px"
        _txtDenominacion = New TextBox()
        _txtDenominacion.ID = Me.ID & "_txtDenominacion"
        _txtDenominacion.Width = New Unit(235, UnitType.Pixel)
        _txtDenominacion.MaxLength = 200
        celda.Controls.Add(_txtDenominacion)
        _autoCompletarDenominacion = New AjaxControlToolkit.AutoCompleteExtender()
        _autoCompletarDenominacion.ID = Me.ID & "_autoCompletarDenominacion"
        _autoCompletarDenominacion.TargetControlID = _txtDenominacion.ID
        _autoCompletarDenominacion.CompletionInterval = 500
        _autoCompletarDenominacion.CompletionSetCount = 10
        _autoCompletarDenominacion.MinimumPrefixLength = 3
        _autoCompletarDenominacion.EnableCaching = False
        _autoCompletarDenominacion.UseContextKey = True
        _autoCompletarDenominacion.ServicePath = "WSSMControls.asmx"
        _autoCompletarDenominacion.ServiceMethod = "Autocompletar_Denominacion"
        _autoCompletarDenominacion.OnClientShown = "sm_SetZIndex"
        celda.Controls.Add(_autoCompletarDenominacion)
        fila.Cells.Add(celda)
        celda = New HtmlControls.HtmlTableCell()
        celda.RowSpan = 3
        celda.Align = "right"
        celda.VAlign = "bottom"
        _btnBuscar = New FSNButton()
        _btnBuscar.ID = Me.ID & "_btnBuscar"
        celda.Controls.Add(_btnBuscar)
        fila.Cells.Add(celda)
        tabla.Rows.Add(fila)
        fila = New HtmlControls.HtmlTableRow()
        celda = New HtmlControls.HtmlTableCell()
        celda.Width = "153px"
        _lblEmpresa = New Label()
        _lblEmpresa.ID = Me.ID & "_lblEmpresa"
        _lblEmpresa.CssClass = "Etiqueta"
        celda.Controls.Add(_lblEmpresa)
        fila.Cells.Add(celda)
        celda = New HtmlControls.HtmlTableCell()
        celda.Width = "237px"
        _ddlEmpresa = New DropDownList()
        _ddlEmpresa.ID = Me.ID & "_ddlEmpresa"
        _ddlEmpresa.Width = New Unit(239, UnitType.Pixel)
        celda.Controls.Add(_ddlEmpresa)
        _lblEmpresaPredef = New Label()
        _lblEmpresaPredef.ID = Me.ID & "_lblEmpresaPredef"
        _lblEmpresaPredef.Visible = False
        celda.Controls.Add(_lblEmpresaPredef)
        fila.Cells.Add(celda)
        celda = New HtmlControls.HtmlTableCell()
        celda.Width = "200px"
        _lblCentro = New Label()
        _lblCentro.ID = Me.ID & "_lblCentro"
        _lblCentro.CssClass = "Etiqueta"
        celda.Controls.Add(_lblCentro)
        fila.Cells.Add(celda)
        celda = New HtmlControls.HtmlTableCell()
        celda.Width = "237px"
        _txtCentro = New TextBox()
        _txtCentro.ID = Me.ClientID & "_txtCentro"
        celda.Controls.Add(_txtCentro)
        _autoCompletarCentro = New AjaxControlToolkit.AutoCompleteExtender()
        _autoCompletarCentro.ID = Me.ID & "_autoCompletarCentro"
        _autoCompletarCentro.TargetControlID = _txtCentro.ID
        _autoCompletarCentro.CompletionInterval = 500
        _autoCompletarCentro.CompletionSetCount = 10
        _autoCompletarCentro.MinimumPrefixLength = 3
        _autoCompletarCentro.EnableCaching = False
        _autoCompletarCentro.UseContextKey = True
        _autoCompletarCentro.ServicePath = "WSSMControls.asmx"
        _autoCompletarCentro.ServiceMethod = "Autocompletar_Centro"
        _autoCompletarCentro.OnClientShown = "sm_SetZIndex"
        celda.Controls.Add(_autoCompletarCentro)
        _lblCentroPredef = New Label()
        _lblCentroPredef.ID = Me.ID & "_lblCentroPredef"
        _lblCentroPredef.Visible = False
        celda.Controls.Add(_lblCentroPredef)
        celda.Controls.Add(New LiteralControl("&nbsp;"))
        _imgbtnBuscarCentro = New ImageButton()
        _imgbtnBuscarCentro.ID = Me.ID & "_imgbtnBuscarCentro"
        celda.Controls.Add(_imgbtnBuscarCentro)
        fila.Cells.Add(celda)
        tabla.Rows.Add(fila)
        fila = New HtmlControls.HtmlTableRow()
        celda = New HtmlControls.HtmlTableCell()
        celda.Width = "153px"
        _lblNumeroERP = New Label()
        _lblNumeroERP.ID = Me.ID & "_lblNumeroERP"
        _lblNumeroERP.CssClass = "Etiqueta"
        celda.Controls.Add(_lblNumeroERP)
        fila.Cells.Add(celda)
        celda = New HtmlControls.HtmlTableCell()
        celda.Width = "237px"
        _txtNumeroERP = New TextBox()
        _txtNumeroERP.ID = Me.ID & "_txtNumeroERP"
        _txtNumeroERP.Width = New Unit(235, UnitType.Pixel)
        _txtNumeroERP.MaxLength = 100
        celda.Controls.Add(_txtNumeroERP)
        _autoCompletarNumeroERP = New AjaxControlToolkit.AutoCompleteExtender()
        _autoCompletarNumeroERP.ID = Me.ID & "_autoCompletarNumeroERP"
        _autoCompletarNumeroERP.TargetControlID = _txtNumeroERP.ID
        _autoCompletarNumeroERP.CompletionInterval = 500
        _autoCompletarNumeroERP.CompletionSetCount = 10
        _autoCompletarNumeroERP.MinimumPrefixLength = 3
        _autoCompletarNumeroERP.EnableCaching = False
        _autoCompletarNumeroERP.UseContextKey = True
        _autoCompletarNumeroERP.ServicePath = "WSSMControls.asmx"
        _autoCompletarNumeroERP.ServiceMethod = "Autocompletar_NumeroERP"
        _autoCompletarNumeroERP.OnClientShown = "sm_SetZIndex"
        celda.Controls.Add(_autoCompletarNumeroERP)
        fila.Cells.Add(celda)
        celda = New HtmlControls.HtmlTableCell()
        celda.Width = "235px"
        fila.Cells.Add(celda)
        celda = New HtmlControls.HtmlTableCell()
        celda.Width = "237px"
        fila.Cells.Add(celda)
        tabla.Rows.Add(fila)
        _upnlParametros.ContentTemplateContainer.Controls.Add(tabla)

        ' Panel de búsqueda de centros de coste
        _modalCentrosCoste = New AjaxControlToolkit.ModalPopupExtender()
        _modalCentrosCoste.ID = Me.ID & "_modalCentrosCoste"
        _modalCentrosCoste.TargetControlID = _imgbtnBuscarCentro.ID
        _modalCentrosCoste.PopupControlID = _pnlCentrosCoste.ID
        _modalCentrosCoste.DropShadow = False
        _upnlParametros.ContentTemplateContainer.Controls.Add(_modalCentrosCoste)

        _pnlParametros.Controls.Add(_upnlParametros)
        _pnlParametros.DefaultButton = _btnBuscar.ID
        Me.Controls.Add(_pnlParametros)
        _collapseParametros = New AjaxControlToolkit.CollapsiblePanelExtender()
        _collapseParametros.ID = Me.ID & "_collapseParametros"
        _collapseParametros.CollapseControlID = _pnlBusquedaAvanzada.ID
        _collapseParametros.ExpandControlID = _pnlBusquedaAvanzada.ID
        _collapseParametros.ImageControlID = _imgExpandir.ID
        _collapseParametros.SuppressPostBack = True
        _collapseParametros.TargetControlID = _pnlParametros.ID
        _collapseParametros.SkinID = "FondoTrans"
        Me.Controls.Add(_collapseParametros)
        Me.Controls.Add(New LiteralControl("<br/>"))
        _upnlActivos = New UpdatePanel()
        _upnlActivos.ID = Me.ID & "_upnlActivos"
        _upnlActivos.UpdateMode = UpdatePanelUpdateMode.Conditional
        _upnlActivos.ChildrenAsTriggers = True
        Dim _div As New HtmlControls.HtmlGenericControl
        _div.TagName = "div"
        _div.Style.Add("width", "100%")
        _div.Style.Add("height", "300px")
        _div.Style.Add("overflow-y", "auto")
        _gridActivos = New GridView()
        _gridActivos.ID = Me.ID & "_gridActivos"
        _gridActivos.Width = New Unit(90, UnitType.Percentage)
        _gridActivos.AutoGenerateColumns = False
        _gridActivos.CellPadding = 4
        _gridActivos.AllowPaging = True
        _gridActivos.Width = New Unit(100, UnitType.Percentage)
        _gridActivos.BackColor = Drawing.ColorTranslator.FromHtml("#CCCCCC")
        _gridActivos.PagerSettings.Position = PagerPosition.Top
        _gridActivos.HeaderStyle.CssClass = "PanelCabecera"
        _gridActivos.SelectedRowStyle.ForeColor = Drawing.Color.White
        _gridActivos.SelectedRowStyle.BackColor = Drawing.ColorTranslator.FromHtml("#3366CC")
        _gridActivos.EmptyDataTemplate = New gridActivosEmptyTemplate()
        _gridActivos.PagerStyle.CssClass = "Rectangulo"
        _gridActivos.PagerTemplate = New gridActivosPagerTemplate()
        Dim column As New BoundField()
        column.DataField = "ID"
        column.SortExpression = "ID"
        column.ReadOnly = True
        column.ItemStyle.Width = New Unit(10, UnitType.Percentage)
        column.Visible = False
        column.ItemStyle.HorizontalAlign = WebControls.HorizontalAlign.Left
        _gridActivos.Columns.Add(column)
        column = New BoundField()
        column.DataField = "COD"
        column.SortExpression = "COD"
        column.ReadOnly = True
        column.ItemStyle.Width = New Unit(10, UnitType.Percentage)
        column.ItemStyle.HorizontalAlign = WebControls.HorizontalAlign.Left
        column.ItemStyle.Wrap = False
        _gridActivos.Columns.Add(column)
        column = New BoundField()
        column.DataField = "DEN_ACTIVO"
        column.SortExpression = "DEN_ACTIVO"
        column.ReadOnly = True
        column.ItemStyle.Width = New Unit(25, UnitType.Percentage)
        column.ItemStyle.HorizontalAlign = WebControls.HorizontalAlign.Left
        column.ItemStyle.Wrap = False
        _gridActivos.Columns.Add(column)
        column = New BoundField()
        column.DataField = "COD_ERP"
        column.SortExpression = "COD_ERP"
        column.ReadOnly = True
        column.ItemStyle.Width = New Unit(20, UnitType.Percentage)
        column.ItemStyle.HorizontalAlign = WebControls.HorizontalAlign.Left
        column.ItemStyle.Wrap = False
        _gridActivos.Columns.Add(column)
        column = New BoundField()
        column.DataField = "DEN_EMPRESA"
        column.SortExpression = "DEN_EMPRESA"
        column.ReadOnly = True
        column.ItemStyle.Width = New Unit(20, UnitType.Percentage)
        column.ItemStyle.HorizontalAlign = WebControls.HorizontalAlign.Left
        column.ItemStyle.Wrap = False
        _gridActivos.Columns.Add(column)
        column = New BoundField()
        column.DataField = "ID_DEN_CENTRO"
        column.SortExpression = "CENTRO_SM"
        column.ReadOnly = True
        column.ItemStyle.Width = New Unit(25, UnitType.Percentage)
        column.ItemStyle.HorizontalAlign = WebControls.HorizontalAlign.Left
        column.ItemStyle.Wrap = False
        _gridActivos.Columns.Add(column)
        column = New BoundField()
        column.DataField = "UONS"
        column.SortExpression = "UONS"
        column.ReadOnly = True
        column.Visible = False
        column.ItemStyle.Width = New Unit(10, UnitType.Percentage)
        column.ItemStyle.HorizontalAlign = WebControls.HorizontalAlign.Left
        _gridActivos.Columns.Add(column)
        column = New BoundField()
        column.DataField = "CENTRO_SM"
        column.SortExpression = "CENTRO_SM"
        column.ReadOnly = True
        column.Visible = False
        column.ItemStyle.Width = New Unit(10, UnitType.Percentage)
        column.ItemStyle.HorizontalAlign = WebControls.HorizontalAlign.Left
        _gridActivos.Columns.Add(column)
        _div.Controls.Add(_gridActivos)
        _upnlActivos.ContentTemplateContainer.Controls.Add(_div)
        Dim trig As New AsyncPostBackTrigger()
        trig.ControlID = _btnBuscar.ID
        trig.EventName = "Click"
        _upnlActivos.Triggers.Add(trig)
        trig = New AsyncPostBackTrigger()
        trig.ControlID = _imgbtnBuscarCentro.ID
        trig.EventName = "Click"
        _upnlActivos.Triggers.Add(trig)
        Me.Controls.Add(_upnlActivos)
        _pnlUpdateProgress = New Panel()
        _pnlUpdateProgress.ID = Me.ID & "_pnlUpdateProgress"
        _pnlUpdateProgress.CssClass = "updateProgress"
        _pnlUpdateProgress.Style.Add(HtmlTextWriterStyle.Display, "none")
        Dim div As New HtmlControls.HtmlGenericControl("div")
        div.Style.Add(HtmlTextWriterStyle.Position, "relative")
        div.Style.Add(HtmlTextWriterStyle.Top, "30%")
        div.Style.Add(HtmlTextWriterStyle.TextAlign, "center")
        _imgProgress = New WebControls.Image()
        _imgProgress.ID = Me.ID & "_imgProgress"
        _imgProgress.SkinID = "ImgProgress"
        div.Controls.Add(_imgProgress)
        _lblProcesando = New Label()
        _lblProcesando.ID = Me.ID & "_lblProcesando"
        _lblProcesando.ForeColor = Drawing.Color.Black
        div.Controls.Add(_lblProcesando)
        _pnlUpdateProgress.Controls.Add(div)
        Me.Controls.Add(_pnlUpdateProgress)
        _modalProgress = New AjaxControlToolkit.ModalPopupExtender()
        _modalProgress.ID = Me.ID & "_modalProgress"
        _modalProgress.TargetControlID = _pnlUpdateProgress.ID
        _modalProgress.BehaviorID = "myModalProgress"
        _modalProgress.BackgroundCssClass = "modalBackground"
        _modalProgress.PopupControlID = _pnlUpdateProgress.ID
        Me.Controls.Add(_modalProgress)
        tabla = New HtmlControls.HtmlTable()
        tabla.Width = "100%"
        tabla.Border = 0
        fila = New HtmlControls.HtmlTableRow()
        celda = New HtmlControls.HtmlTableCell()
        celda.Align = "center"
        Dim tablain As New HtmlControls.HtmlTable()
        tablain.CellSpacing = 20
        tablain.Border = 0
        Dim filain As New HtmlControls.HtmlTableRow()
        Dim celdain As New HtmlControls.HtmlTableCell()
        celdain.Align = "left"
        _btnAceptar = New FSNButton()
        _btnAceptar.ID = Me.ID & "_btnAceptar"
        _btnAceptar.Attributes.Add("btnAceptar", "btnAceptar")
        _btnAceptar.Alineacion = FSNTipos.Alineacion.Left
        celdain.Controls.Add(_btnAceptar)
        filain.Cells.Add(celdain)
        celdain = New HtmlControls.HtmlTableCell()
        celdain.Align = "right"
        _btnCancelar = New FSNButton()
        _btnCancelar.ID = Me.ID & "_btnCancelar"
        _btnCancelar.Alineacion = FSNTipos.Alineacion.Right
        celdain.Controls.Add(_btnCancelar)
        filain.Cells.Add(celdain)
        tablain.Rows.Add(filain)
        celda.Controls.Add(tablain)
        fila.Cells.Add(celda)
        tabla.Rows.Add(fila)
        Me.Controls.Add(tabla)
    End Sub

    ''' <summary>
    ''' Inicializa las variables privadas del control
    ''' </summary>
    ''' <param name="sender">Origen del evento</param>
    ''' <param name="e">EventArgs que contiene datos de eventos.</param>
    Private Sub SMActivos_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        _mCentroSM = FSNServer.Get_Object(GetType(FSNServer.Centro_SM))
        _mActivo = FSNServer.Get_Object(GetType(FSNServer.Activo))
        _mUON = FSNServer.Get_Object(GetType(FSNServer.UON))
        _mEmpresa = FSNServer.Get_Object(GetType(FSNServer.CEmpresa))
        _Idioma = (CType(Me.Page, IFSNPage).Usuario.Idioma)
        IdentificadorActivosEnCache = "Activos_" & CType(Me.Page, IFSNPage).Usuario.CodPersona & "_" & _Idioma.ToString()
        IdentificadorUltimaBusquedaEnCache = "ActivosLastSearch_" & CType(Me.Page, IFSNPage).Usuario.CodPersona & "_" & _Idioma.ToString()
        OrderByFieldCookieName = Me.GetType.FullName & "_ORDERBY_FIELD"
        OrderByDirectionCookieName = Me.GetType.FullName & "_ORDERBY_DIR"
    End Sub

    ''' <summary>
    ''' Inicializa el control según los valores de las propiedades
    ''' </summary>
    Private Sub ResetControl()
        If HttpContext.Current.Cache(IdentificadorActivosEnCache) IsNot Nothing Then _
            HttpContext.Current.Cache.Remove(IdentificadorActivosEnCache)
        If HttpContext.Current.Cache(IdentificadorUltimaBusquedaEnCache) IsNot Nothing Then _
            HttpContext.Current.Cache.Remove(IdentificadorUltimaBusquedaEnCache)
        _txtCodigo.Text = ""
        _txtDenominacion.Text = ""
        _txtNumeroERP.Text = ""
        _txtCentro.Text = ""
        If _mCentroSM.Empresa.HasValue Then _mEmpresa.ID = _mCentroSM.Empresa
        If _mCentroSM.UONS IsNot Nothing Then _mUON = _mCentroSM.UONS
        _txtCentro.Visible = String.IsNullOrEmpty(_mCentroSM.Codigo)
        _imgbtnBuscarCentro.Visible = _txtCentro.Visible
        _lblCentroPredef.Visible = Not _txtCentro.Visible
        If _lblCentroPredef.Visible Then
            If Not String.IsNullOrEmpty(_mCentroSM.Denominacion) Then
                _lblCentroPredef.Text = String.Format("{0} - {1}", _mCentroSM.Codigo, _mCentroSM.Denominacion)
            Else
                _lblCentroPredef.Text = _mCentroSM.Codigo
            End If
        End If
        _lblEmpresaPredef.Visible = (_mEmpresa.ID > 0)
        _ddlEmpresa.Visible = Not _lblEmpresaPredef.Visible
        If _lblEmpresaPredef.Visible Then
            Dim resemp = From Datos In ActivosUsu
                         Where Datos("EMPRESA") = _mEmpresa.ID
                         Select Datos("DEN_EMPRESA") Distinct
            If resemp.Count > 0 Then _lblEmpresaPredef.Text = resemp(0).ToString()
            If String.IsNullOrEmpty(_lblEmpresaPredef.Text) Then _lblEmpresaPredef.Text = _mEmpresa.Den
        Else
            Dim resemp = From Datos In ActivosUsu
                         Select New ListItem(Datos("DEN_EMPRESA"), Datos("EMPRESA")) Distinct.ToArray()
            _ddlEmpresa.Items.Clear()
            _ddlEmpresa.Items.Add("")
            _ddlEmpresa.Items.AddRange(resemp)
        End If
        _upnlParametros.Update()
        _gridActivos.DataSource = Nothing
        _gridActivos.PageIndex = 0
        _gridActivos.DataBind()
    End Sub

    ''' <summary>
    ''' Inicializa los controles
    ''' </summary>
    ''' <param name="sender">Origen del evento</param>
    ''' <param name="e">EventArgs que contiene datos de eventos.</param>
    ''' <remarks>Se produce cuando el control de servidor se carga en el objeto Page.</remarks>
    Private Sub SMActivos_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        EnsureChildControls()
        Dim pag As IFSNPage = CType(Me.Page, IFSNPage)
        If Not Page.IsPostBack() Then
            CargarTextos()
            Dim oActivos As FSNServer.Activos = FSNServer.Get_Object(GetType(FSNServer.Activos))
            _bHayIntegracionSalida = oActivos.HayIntegracionSentidoSalida()
            _lblNumeroERP.Visible = _bHayIntegracionSalida
            _txtNumeroERP.Visible = _bHayIntegracionSalida
            _gridActivos.Columns(3).Visible = _bHayIntegracionSalida
            If IsNumeric(System.Configuration.ConfigurationManager.AppSettings("registrosPaginacion")) Then _
                _gridActivos.PageSize = CInt(System.Configuration.ConfigurationManager.AppSettings("registrosPaginacion"))
            ResetControl()

            'todo 1550 queda pendiente para la tarea 1550: modificar FSNPage para cargar FSNServer.User en Session.
            'Las propiedades ya estan creadas en la clase User.
            '<FSNServer.User>.SMAltaActivos
            '<FSNServer.User>.SMModificarActivos
            '<FSNServer.User>.SMEliminarActivos

            ' Ordenación por cookies
            Dim cookie As HttpCookie
            cookie = HttpContext.Current.Request.Cookies(OrderByFieldCookieName)
            If cookie Is Nothing OrElse String.IsNullOrEmpty(cookie.Value) Then
                CampoOrdenacion = DefaultOrderByField
            Else
                CampoOrdenacion = cookie.Value
                cookie.Expires = DateTime.Now.AddDays(30)
                HttpContext.Current.Response.AppendCookie(cookie)
            End If
            cookie = HttpContext.Current.Request.Cookies(OrderByDirectionCookieName)
            If cookie Is Nothing OrElse String.IsNullOrEmpty(cookie.Value) Then
                SentidoOrdenacion = SortDirection.Ascending
            Else
                SentidoOrdenacion = cookie.Value
                cookie.Expires = DateTime.Now.AddDays(30)
                HttpContext.Current.Response.AppendCookie(cookie)
            End If

            _autoCompletarCodigo.ContextKey = CType(Me.Page, IFSNPage).Usuario.CodPersona & "_" & CType(Me.Page, IFSNPage).Usuario.Idioma.ToString()
            _autoCompletarDenominacion.ContextKey = _autoCompletarCodigo.ContextKey
            _autoCompletarNumeroERP.ContextKey = _autoCompletarCodigo.ContextKey
            _autoCompletarCentro.ContextKey = _autoCompletarCodigo.ContextKey

            _imgCabecera.Src = Me.Page.ClientScript.GetWebResourceUrl(GetType(SMActivos), "Fullstep.SMWebControls.ActivosBuscar.jpg")
            _imgbtnBuscarCentro.ImageUrl = Me.Page.ClientScript.GetWebResourceUrl(GetType(SMActivos), "Fullstep.SMWebControls.lupa.gif")
            _txtCodigo.MaxLength = pag.FSNServer.LongitudesDeCodigos.giLongCodACTIVO
            _txtCentro.MaxLength = pag.FSNServer.LongitudesDeCodigos.giLongCodCENTROCOSTE + 100

            _pnlCentrosCoste.OnCancelarClientClick = "$find('" & _modalCentrosCoste.ClientID & "').hide(); return false"
        End If
        Me.Page.ClientScript.RegisterClientScriptResource(GetType(SMWebControlClientScript), "Fullstep.SMWebControls.SMControls.js")
        _btnAceptar.Attributes.Add("onclick", "guardarActivoSeleccionado('" & Me.ClientID & "_gridActivos');return false;")
    End Sub

    Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)
        If _mAllowSelection Then
            For Each row As GridViewRow In _gridActivos.Rows
                If row.RowType = DataControlRowType.DataRow Then
                    Page.ClientScript.RegisterForEventValidation(_gridActivos.UniqueID, "Select$" & row.RowIndex)
                End If
            Next
        End If
        Page.ClientScript.RegisterForEventValidation(Me.UniqueID)
        MyBase.Render(writer)
    End Sub

    ''' <summary>
    ''' Carga los textos de los controles
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CargarTextos()
        _lblTitulo.Text = Textos(15)
        _lblBusquedaAvanzada.Text = Textos(5)
        _lblCodigo.Text = Textos(1) & ":"
        _lblDenominacion.Text = Textos(2) & ":"
        _lblNumeroERP.Text = Textos(3) & ":"
        _lblCentro.Text = Textos(10) & ":"
        _lblEmpresa.Text = Textos(4) & ":"
        _btnBuscar.Text = Textos(6)
        _gridActivos.Columns(1).HeaderText = Me.Textos(1)
        _gridActivos.Columns(2).HeaderText = Me.Textos(2)
        _gridActivos.Columns(3).HeaderText = Me.Textos(3)
        _gridActivos.Columns(4).HeaderText = Me.Textos(4)
        _gridActivos.Columns(5).HeaderText = Me.Textos(10)
        _lblProcesando.Text = Textos(11)
        _btnAceptar.Text = Textos(17)
        _btnCancelar.Text = Textos(18)
    End Sub

#End Region

#Region "Enlace a datos"

    ''' <summary>
    ''' Devuelve el origen de datos del control GridView
    ''' </summary>
    ''' <returns>Un DataView con el origen de datos</returns>
    ''' <remarks>El origen de datos del grid tiene en cuenta los filtros y la ordenación</remarks>
    Private Function GetGridDataSource() As DataView
        Dim result As DataView = HttpContext.Current.Cache(IdentificadorUltimaBusquedaEnCache)
        If result Is Nothing Then
            Dim dtActivos As DataTable = ActivosUsu
            If dtActivos IsNot Nothing Then result = New DataView(dtActivos)
        End If
        Sort(result)
        Return result
    End Function

    Private Sub _gridActivos_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles _gridActivos.DataBound
        _gridActivos.SelectedIndex = -1
    End Sub

    Private Sub gridActivos_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles _gridActivos.RowDataBound
        With e.Row
            Select Case .RowType
                Case DataControlRowType.Pager
                    AddHandler .FindControl("_imgbtnFirst").PreRender, AddressOf ImgBtnFirst_PreRender
                    AddHandler .FindControl("_imgbtnPrev").PreRender, AddressOf ImgBtnPrev_PreRender
                    AddHandler .FindControl("_imgbtnNext").PreRender, AddressOf ImgBtnNext_PreRender
                    AddHandler .FindControl("_imgbtnLast").PreRender, AddressOf ImgBtnLast_PreRender
                    CType(.FindControl("_lblPagina"), Label).Text = Textos(7)
                    AddHandler .FindControl("_ddlNumPags").PreRender, AddressOf ddlNumPags_PreRender
                    AddHandler CType(.FindControl("_ddlNumPags"), DropDownList).SelectedIndexChanged, AddressOf ddlNumPags_SelectedIndexChanged
                    CType(.FindControl("_lblDe"), Label).Text = Textos(8)
                    CType(.FindControl("_lblTotal"), Label).Text = _gridActivos.PageCount
                    CType(.FindControl("_lblOrdenarPor"), Label).Text = Textos(9) & ":"
                    CType(.FindControl("_imgbtnOrdenacion"), ImageButton).ImageUrl =
                        String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme,
                                      IIf(SentidoOrdenacion = SortDirection.Ascending, "ascendente.gif", "descendente.gif"))
                    Dim ddl As DropDownList = CType(.FindControl("_ddlOrdenarPor"), DropDownList)
                    ddl.Items.Clear()
                    ddl.Items.Add(New ListItem(Textos(1), "COD"))
                    ddl.Items.Add(New ListItem(Textos(2), "DEN_ACTIVO"))
                    If _bHayIntegracionSalida Then ddl.Items.Add(New ListItem(Textos(3), "COD_ERP"))
                    ddl.Items.Add(New ListItem(Textos(4), "DEN_EMPRESA"))
                    ddl.Items.Add(New ListItem(Textos(10), "CENTRO_SM"))
                    ddl.SelectedValue = CampoOrdenacion
                Case DataControlRowType.EmptyDataRow
                    CType(.FindControl("_lblEmptyDataText"), Label).Text = Textos(12)
                Case DataControlRowType.DataRow
                    Dim dataItem As DataRowView = CType(e.Row.DataItem, DataRowView)

                    Dim iMaximoCaracteresCodigo As Integer = 10
                    Dim iMaximoCaracteresDen As Integer = 20
                    Dim iMaximoCaracteresERP As Integer = 10
                    Dim iMaximoCaracteresEmpresa As Integer = 18
                    Dim iMaximoCaracteresCentro As Integer = 20
                    If Not _bHayIntegracionSalida Then
                        iMaximoCaracteresCodigo = 12
                        iMaximoCaracteresDen = 23
                        iMaximoCaracteresERP = 0
                        iMaximoCaracteresEmpresa = 21
                        iMaximoCaracteresCentro = 22
                    End If

                    Dim sTextoLinea As String = ""
                    If _bHayIntegracionSalida Then
                        sTextoLinea = dataItem("COD") & dataItem("DEN_ACTIVO") & dataItem("COD_ERP") & dataItem("DEN_EMPRESA") & dataItem("ID_DEN_CENTRO")
                    Else
                        sTextoLinea = dataItem("COD") & dataItem("DEN_ACTIVO") & dataItem("DEN_EMPRESA") & dataItem("ID_DEN_CENTRO")
                    End If
                    Dim iLenDEN_ACTIVO As Integer = Len(DBNullToStr(dataItem("DEN_ACTIVO")))
                    Dim iLenCODIGO As Integer = Len(DBNullToStr(dataItem("COD")))
                    Dim iLenERP As Integer = Len(DBNullToStr(dataItem("COD_ERP")))
                    Dim iLenDEN_EMPRESA As Integer = Len(DBNullToStr(dataItem("DEN_EMPRESA")))
                    Dim iLenID_DEN_CENTRO As Integer = Len(DBNullToStr(dataItem("ID_DEN_CENTRO")))
                    Dim iTotalCaracteresDondeReducir As Integer = (iLenDEN_ACTIVO + iLenDEN_EMPRESA + iLenID_DEN_CENTRO)
                    Dim oGridActivos As GridView = CType(sender, GridView)
                    Dim iIndexCeldaDEN_ACTIVO As Integer = ColumnIndexBySortExpression(CType(sender, GridView).Columns, "DEN_ACTIVO")
                    Dim iIndexCeldaCODIGO As Integer = ColumnIndexBySortExpression(CType(sender, GridView).Columns, "COD")
                    Dim iIndexCeldaERP As Integer = ColumnIndexBySortExpression(CType(sender, GridView).Columns, "COD_ERP")
                    Dim iIndexCeldaDEN_EMPRESA As Integer = ColumnIndexBySortExpression(CType(sender, GridView).Columns, "DEN_EMPRESA")
                    Dim iIndexCeldaID_DEN_CENTRO As Integer = ColumnIndexBySortExpression(CType(sender, GridView).Columns, "CENTRO_SM")
                    Dim bAcortadoDEN_ACTIVO As Boolean = False
                    Dim bAcortadoCODIGO As Boolean = False
                    Dim bAcortadoERP As Boolean = False
                    Dim bAcortadoDEN_EMPRESA As Boolean = False
                    Dim bAcortadoID_DEN_CENTRO As Boolean = False
                    Dim iLongitudes() As Double = {iLenDEN_ACTIVO, iLenDEN_EMPRESA, iLenID_DEN_CENTRO}
                    With e.Row
                        'Acortamos el texto de CODIGO
                        If iIndexCeldaCODIGO > 0 AndAlso iLenCODIGO > iMaximoCaracteresCodigo Then
                            .Cells(iIndexCeldaCODIGO).Text = Left(dataItem("COD"), iMaximoCaracteresCodigo)
                            bAcortadoCODIGO = True
                        End If
                        'Acortamos el texto de DEN_ACTIVO
                        If iIndexCeldaDEN_ACTIVO > 0 AndAlso iLenDEN_ACTIVO > iMaximoCaracteresDen Then
                            .Cells(iIndexCeldaDEN_ACTIVO).Text = Left(dataItem("DEN_ACTIVO"), iMaximoCaracteresDen)
                            bAcortadoDEN_ACTIVO = True
                        End If
                        'Acortamos el texto de ERP
                        If _bHayIntegracionSalida AndAlso iIndexCeldaERP > 0 AndAlso iLenERP > iMaximoCaracteresERP Then
                            .Cells(iIndexCeldaERP).Text = Left(dataItem("COD_ERP"), iMaximoCaracteresERP)
                            bAcortadoERP = True
                        End If
                        'Acortamos el texto de DEN_EMPRESA. 
                        If iIndexCeldaDEN_EMPRESA > 0 AndAlso iLenDEN_EMPRESA > iMaximoCaracteresEmpresa Then
                            .Cells(iIndexCeldaDEN_EMPRESA).Text = Left(dataItem("DEN_EMPRESA"), iMaximoCaracteresEmpresa)
                            bAcortadoDEN_EMPRESA = True
                        End If
                        'Acortamos el texto de ID_DEN_CENTRO. 
                        If iIndexCeldaID_DEN_CENTRO > 0 AndAlso iLenID_DEN_CENTRO > iMaximoCaracteresCentro Then
                            .Cells(iIndexCeldaID_DEN_CENTRO).Text = Left(dataItem("ID_DEN_CENTRO"), iMaximoCaracteresCentro)
                            bAcortadoID_DEN_CENTRO = True
                        End If

                        If bAcortadoCODIGO Then
                            .Cells(iIndexCeldaCODIGO).ToolTip = dataItem("COD")
                            .Cells(iIndexCeldaCODIGO).Text = .Cells(iIndexCeldaCODIGO).Text & "..."
                        End If
                        If bAcortadoDEN_ACTIVO Then
                            .Cells(iIndexCeldaDEN_ACTIVO).ToolTip = dataItem("DEN_ACTIVO")
                            .Cells(iIndexCeldaDEN_ACTIVO).Text = .Cells(iIndexCeldaDEN_ACTIVO).Text & "..."
                        End If
                        If bAcortadoERP Then
                            .Cells(iIndexCeldaERP).ToolTip = dataItem("COD_ERP")
                            .Cells(iIndexCeldaERP).Text = .Cells(iIndexCeldaDEN_ACTIVO).Text & "..."
                        End If
                        If bAcortadoDEN_EMPRESA Then
                            .Cells(iIndexCeldaDEN_EMPRESA).ToolTip = dataItem("DEN_EMPRESA")
                            .Cells(iIndexCeldaDEN_EMPRESA).Text = .Cells(iIndexCeldaDEN_EMPRESA).Text & "..."
                        End If
                        If bAcortadoID_DEN_CENTRO Then
                            .Cells(iIndexCeldaID_DEN_CENTRO).ToolTip = dataItem("ID_DEN_CENTRO")
                            .Cells(iIndexCeldaID_DEN_CENTRO).Text = .Cells(iIndexCeldaID_DEN_CENTRO).Text & "..."
                        End If
                    End With

                    If _mAllowSelection Then
                        e.Row.Attributes.Add("onMouseOver", "this.style.cursor='hand';")
                        e.Row.Attributes.Add("OnClick", "selectRow(this);")
                        e.Row.Attributes.Add("esFilaActivo", "Verdadero")
                        e.Row.Attributes.Add("ID", e.Row.DataItem("ID"))
                        e.Row.Attributes.Add("DEN_ACTIVO", e.Row.DataItem("COD") + " - " + e.Row.DataItem("DEN_ACTIVO"))
                        e.Row.Attributes.Add("ID_DEN_CENTRO", e.Row.DataItem("ID_DEN_CENTRO"))
                        e.Row.Attributes.Add("CENTRO_SM", e.Row.DataItem("CENTRO_SM"))
                        e.Row.Attributes.Add("UONS", e.Row.DataItem("CENTRO_SM").ToString & "@@" & Replace(e.Row.DataItem("UONS").ToString, "#", "@@"))
                    End If
            End Select
        End With
    End Sub

#End Region

#Region "Acciones Grid"

    Private Sub _gridActivos_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles _gridActivos.RowCommand
        Select Case e.CommandName
            Case "sort"
                SentidoOrdenacion = IIf(SentidoOrdenacion = SortDirection.Ascending, SortDirection.Descending, SortDirection.Ascending)
                _gridActivos.Sort(CampoOrdenacion, SentidoOrdenacion)
        End Select
    End Sub

#End Region

#Region "Paginacion"

    ''' <summary>
    ''' Evento que ocurre cuando se está presentando el control
    ''' </summary>
    ''' <param name="sender">Objeto que lanza el evento</param>
    ''' <param name="e">Argumento de evento</param>
    ''' <remarks>Se utiliza para configurar el control ImageButton de la paginación</remarks>
    Protected Sub ImgBtnFirst_PreRender(ByVal sender As Object, ByVal e As EventArgs)
        If Not _gridActivos.PageIndex = 0 Then
            CType(sender, ImageButton).ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "primero.gif")
            CType(sender, ImageButton).Enabled = True
        Else
            CType(sender, ImageButton).ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "primero_desactivado.gif")
            CType(sender, ImageButton).Enabled = False
        End If
    End Sub

    ''' <summary>
    ''' Evento que ocurre cuando se está presentando el control
    ''' </summary>
    ''' <param name="sender">Objeto que lanza el evento</param>
    ''' <param name="e">Argumento de evento</param>
    ''' <remarks>Se utiliza para configurar el control ImageButton de la paginación</remarks>
    Protected Sub ImgBtnPrev_PreRender(ByVal sender As Object, ByVal e As EventArgs)
        If Not _gridActivos.PageIndex = 0 Then
            CType(sender, ImageButton).ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "anterior.gif")
            CType(sender, ImageButton).Enabled = True
        Else
            CType(sender, ImageButton).ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "anterior_desactivado.gif")
            CType(sender, ImageButton).Enabled = False
        End If
    End Sub

    ''' <summary>
    ''' Evento que ocurre cuando se está presentando el control
    ''' </summary>
    ''' <param name="sender">Objeto que lanza el evento</param>
    ''' <param name="e">Argumento de evento</param>
    ''' <remarks>Se utiliza para configurar el control ImageButton de la paginación</remarks>
    Protected Sub ImgBtnNext_PreRender(ByVal sender As Object, ByVal e As EventArgs)
        If _gridActivos.PageIndex = _gridActivos.PageCount - 1 Then
            CType(sender, ImageButton).ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "siguiente_desactivado.gif")
            CType(sender, ImageButton).Enabled = False
        Else
            CType(sender, ImageButton).ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "siguiente.gif")
            CType(sender, ImageButton).Enabled = True
        End If
    End Sub

    ''' <summary>
    ''' Evento que ocurre cuando se está presentando el control
    ''' </summary>
    ''' <param name="sender">Objeto que lanza el evento</param>
    ''' <param name="e">Argumento de evento</param>
    ''' <remarks>Se utiliza para configurar el control ImageButton de la paginación</remarks>
    Protected Sub ImgBtnLast_PreRender(ByVal sender As Object, ByVal e As EventArgs)
        If _gridActivos.PageIndex = _gridActivos.PageCount - 1 Then
            CType(sender, ImageButton).ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "ultimo_desactivado.gif")
            CType(sender, ImageButton).Enabled = False
        Else
            CType(sender, ImageButton).ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "ultimo.gif")
            CType(sender, ImageButton).Enabled = True
        End If
    End Sub

    ''' <summary>
    ''' Evento que ocurre cuando cambia la selección en el control
    ''' </summary>
    ''' <param name="sender">Objeto que lanza el evento</param>
    ''' <param name="e">Argumento de evento</param>
    ''' <remarks>Se utiliza para establecer la página actual en el GridView</remarks>
    Protected Sub ddlNumPags_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim pageIndex As Integer = CType(sender, DropDownList).SelectedItem.Value - 1
        _gridActivos.PageIndex = pageIndex
        CargarDatos()
    End Sub

    ''' <summary>
    ''' Evento que ocurre cuando se está presentando el control
    ''' </summary>
    ''' <param name="sender">Objeto que lanza el evento</param>
    ''' <param name="e">Argumento de evento</param>
    ''' <remarks>Se utiliza para establecer el número de la página en el combo</remarks>
    Protected Sub ddlNumPags_PreRender(ByVal sender As Object, ByVal e As EventArgs)
        CType(sender, DropDownList).Items.Clear()
        For i As Integer = 1 To _gridActivos.PageCount
            CType(sender, DropDownList).Items.Add(i)
        Next
        CType(sender, DropDownList).SelectedIndex = _gridActivos.PageIndex
    End Sub

    Private Sub _gridActivos_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles _gridActivos.PageIndexChanging
        _gridActivos.PageIndex = e.NewPageIndex
        CargarDatos()
    End Sub

#End Region

#Region "Ordenación"

    ''' <summary>
    ''' Ordena el DataView pasado como argumento. La ordenación se hace
    ''' sobre el campo establecido por la variable _CampoOrdenacion
    ''' </summary>
    ''' <param name="data"></param>
    ''' <remarks></remarks>
    Private Sub Sort(ByRef data As DataView)
        If Not data Is Nothing Then
            Dim sortExpression As String = CampoOrdenacion
            sortExpression += IIf(SentidoOrdenacion = SortDirection.Ascending, " ASC", " DESC")
            data.Sort = sortExpression
        End If
    End Sub

    ''' <summary>
    ''' Evento que ocurre cuando se invoca el método Sort del control GridView
    ''' </summary>
    ''' <param name="sender">Objeto que lanza el evento</param>
    ''' <param name="e">Argumento del evento</param>
    ''' <remarks>Se utiliza para aplicar los criterios de ordenación</remarks>
    Private Sub _gridActivos_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles _gridActivos.Sorting
        If CampoOrdenacion <> e.SortExpression Then
            CampoOrdenacion = e.SortExpression
            Dim cookie As HttpCookie = Page.Request.Cookies(OrderByFieldCookieName)
            If cookie Is Nothing Then
                cookie = New HttpCookie(OrderByFieldCookieName, CampoOrdenacion)
            Else
                cookie.Value = CampoOrdenacion
            End If
            cookie.Expires = Now.AddDays(30)
            Me.Page.Response.AppendCookie(cookie)
        End If
        If SentidoOrdenacion <> e.SortDirection Then
            SentidoOrdenacion = e.SortDirection
            Dim cookie As HttpCookie = Page.Request.Cookies(OrderByDirectionCookieName)
            If cookie Is Nothing Then
                cookie = New HttpCookie(OrderByDirectionCookieName, SentidoOrdenacion)
            Else
                cookie.Value = SentidoOrdenacion
            End If
            cookie.Expires = Now.AddDays(30)
            Me.Page.Response.AppendCookie(cookie)
        End If
        _gridActivos.PageIndex = 0
        CargarDatos()
    End Sub

#End Region

#Region "Búsqueda"

    ''' <summary>
    ''' Realiza la búsqueda según los valores introducidos
    ''' </summary>
    ''' <param name="sender">Origen del evento.</param>
    ''' <param name="e">Clase EventArgs que no contiene datos de evento.</param>
    ''' <remarks>Se produce cuando se hace clic en el control FSNButton [nombre del botón].</remarks>
    Private Sub _btnBuscar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _btnBuscar.Click
        HttpContext.Current.Cache.Remove(IdentificadorUltimaBusquedaEnCache)
        Dim result As DataView = Nothing
        Dim dtActivos As DataTable = ActivosUsu
        If dtActivos IsNot Nothing AndAlso dtActivos.Rows.Count > 0 Then
            result = dtActivos.Clone.DefaultView
            Dim patternCodigo As String = "*"
            Dim patternDenominacion As String = "*"
            Dim patternCodigoERP As String = "*"
            Dim patternEmpresa As String = "*"
            Dim patternCentro As String = "*"

            If Not String.IsNullOrEmpty(_txtCodigo.Text) Then patternCodigo = String.Concat("*", strToSQLLIKE(UCase(_txtCodigo.Text), True), "*")
            If Not String.IsNullOrEmpty(_txtDenominacion.Text) Then patternDenominacion = String.Concat("*", strToSQLLIKE(UCase(_txtDenominacion.Text), True), "*")
            If Not String.IsNullOrEmpty(_txtNumeroERP.Text) Then patternCodigoERP = String.Concat("*", strToSQLLIKE(UCase(_txtNumeroERP.Text), True), "*")
            If Not String.IsNullOrEmpty(_txtCentro.Text) Then patternCentro = String.Concat("*", strToSQLLIKE(UCase(_txtCentro.Text), True), "*")
            If _ddlEmpresa.SelectedIndex > 0 Then patternEmpresa = _ddlEmpresa.SelectedValue
            Dim match As EnumerableRowCollection(Of DataRow) = From Datos In dtActivos
                                                               Where UCase(Datos("COD")) Like patternCodigo _
                                                                  And UCase(Datos("DEN_ACTIVO")) Like patternDenominacion _
                                                                  And UCase(DBNullToStr(Datos("COD_ERP"))) Like patternCodigoERP _
                                                                  And UCase(Datos("EMPRESA")) Like patternEmpresa _
                                                                  And UCase(Datos("ID_DEN_CENTRO")) Like patternCentro
                                                               Select Datos
            If match.Count > 0 Then result = match.AsDataView
            Sort(result)
            HttpContext.Current.Cache.Insert(IdentificadorUltimaBusquedaEnCache, result, Nothing, Caching.Cache.NoAbsoluteExpiration, New TimeSpan(0, ConfigurationManager.AppSettings("TiempoExpiracionCacheMin"), 0))
        End If
        If (sender.ID = "SMActivosEP_btnBuscar") Then
            _ddlEmpresa.Enabled = False
        End If
        _gridActivos.DataSource = result
        _gridActivos.PageIndex = 0
        _gridActivos.DataBind()
        _pnlCentrosCoste.ListaCentros = GetListaCentrosCoste()
    End Sub

#End Region

#Region "Búsqueda Centros de coste"

    Private Sub _pnlCentrosCoste_Aceptar(ByVal sender As Object, ByVal e As System.EventArgs) Handles _pnlCentrosCoste.Aceptar
        _modalCentrosCoste.Hide()
        If _pnlCentrosCoste.CentroSM IsNot Nothing Then
            _txtCentro.Text = _pnlCentrosCoste.CentroSM.Codigo & " - " & _pnlCentrosCoste.CentroSM.Denominacion
            _upnlParametros.Update()
        End If
    End Sub

    ''' <summary>
    ''' Obtiene los centros de coste del resultado de la búsqueda
    ''' </summary>
    ''' <returns>Una lista de ListItem con los centros de coste</returns>
    Private Function GetListaCentrosCoste() As List(Of ListItem)
        Dim lista As New List(Of ListItem)
        Dim tab As DataTable = GetGridDataSource().Table
        lista = From Datos In tab _
                Order By Datos.Item("ID_DEN_CENTRO") _
                Select New ListItem(Datos.Item("ID_DEN_CENTRO"), Datos.Item("CENTRO_SM")) Distinct.ToList

        Return lista
    End Function

#End Region

#Region "Control State"

    ''' <summary>
    ''' Registra el control como un control cuyo estado se debe conservar.
    ''' </summary>
    ''' <param name="e">Objeto EventArgs que contiene los datos del evento.</param>
    Protected Overrides Sub OnInit(ByVal e As System.EventArgs)
        MyBase.OnInit(e)
        Page.RegisterRequiresControlState(Me)
    End Sub

    ''' <summary>
    ''' Guarda los cambios de estado del control.
    ''' </summary>
    ''' <returns>Devuelve el estado actual del control de servidor.</returns>
    Protected Overrides Function SaveControlState() As Object
        Dim obj As Object = MyBase.SaveControlState()
        If obj IsNot Nothing Then
            obj = New Pair(obj, _mCentroSM)
        Else
            obj = _mCentroSM
        End If
        obj = New Pair(obj, _mUsuarioPM)
        obj = New Pair(obj, _mActivo)
        obj = New Pair(obj, _mUON)
        obj = New Pair(obj, _mEmpresa)
        Return obj
    End Function

    ''' <summary>
    ''' Restaura información de estado del control.
    ''' </summary>
    ''' <param name="state">Object que representa el estado del control que se va a restaurar.</param>
    Protected Overrides Sub LoadControlState(ByVal state As Object)
        If (state IsNot Nothing) Then
            Dim p As Pair = TryCast(state, Pair)
            _mEmpresa = CType(p.Second, FSNServer.CEmpresa)
            state = p.First
            p = TryCast(state, Pair)
            _mUON = CType(p.Second, FSNServer.UON)
            state = p.First
            p = TryCast(state, Pair)
            _mActivo = CType(p.Second, FSNServer.Activo)
            state = p.First
            p = TryCast(state, Pair)
            _mUsuarioPM = CType(p.Second, String)
            state = p.First
            p = TryCast(state, Pair)
            If p IsNot Nothing Then
                MyBase.LoadControlState(p.First)
                _mCentroSM = CType(p.Second, FSNServer.Centro_SM)
            Else
                If (TypeOf (state) Is FSNServer.Centro_SM) Then
                    _mCentroSM = CType(state, FSNServer.Centro_SM)
                Else
                    MyBase.LoadControlState(state)
                End If
            End If
        End If
    End Sub

#End Region

#Region "Aceptar/Cancelar"

    ''' <summary>
    ''' Lanza el evento Aceptar si hay un activo seleccionado
    ''' </summary>
    ''' <param name="sender">Origen del evento.</param>
    ''' <param name="e">Clase EventArgs que no contiene datos de evento.</param>
    ''' <remarks>Se produce cuando se hace clic en el control FSNButton btnAceptar.</remarks>
    Private Sub _btnAceptar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _btnAceptar.Click

        Dim idActivo As String = ""
        If Not HttpContext.Current.Session("idActivo") Is Nothing Then
            idActivo = HttpContext.Current.Session("idActivo")
            HttpContext.Current.Session("idActivo") = Nothing
            Dim dr As DataRow = GetGridDataSource().Table.Rows.Find(idActivo)
            If dr IsNot Nothing Then
                _mActivo.ID = dr.Item("ID")
                _mActivo.Codigo = dr.Item("COD")
                _mActivo.Cod_Erp = DBNullToSomething(dr.Item("COD_ERP"))
                _mActivo.Centro_SM = dr.Item("CENTRO_SM")
                _mActivo.Empresa = dr.Item("EMPRESA")
                _mActivo.Denominacion = dr.Item("DEN_ACTIVO")
            Else
                _mActivo.ID = 0
            End If
        End If

        If _mActivo IsNot Nothing Then _
            OnAceptar(e)
    End Sub

    Protected Overridable Sub OnAceptar(ByVal e As EventArgs)
        RaiseEvent Aceptar(Me, e)
    End Sub

    ''' <summary>
    ''' Lanza el evento Cancelar
    ''' </summary>
    ''' <param name="sender">Origen del evento.</param>
    ''' <param name="e">Clase EventArgs que no contiene datos de evento.</param>
    ''' <remarks>Se produce cuando se hace clic en el control FSNButton btnCancelar.</remarks>
    Private Sub _btnCancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _btnCancelar.Click
        _mActivo.ID = 0
        OnCancelar(e)
    End Sub

    ''' <summary>
    ''' Lanza el evento Cancelar
    ''' </summary>
    ''' <param name="sender">Origen del evento.</param>
    ''' <param name="e">ImageClickEventArgs que contiene datos de eventos.</param>
    ''' <remarks>Se produce cuando se hace clic en el control ImageButton.</remarks>
    Private Sub _imgCerrar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _imgCerrar.Click
        _mActivo.ID = 0
        OnCancelar(e)
    End Sub

    Protected Overridable Sub OnCancelar(ByVal e As EventArgs)
        RaiseEvent Cancelar(Me, e)
    End Sub

    Public Sub RaisePostBackEvent(ByVal eventArgument As String) Implements System.Web.UI.IPostBackEventHandler.RaisePostBackEvent
        '_btnAceptar_Click(Me, New EventArgs())
        _btnCancelar_Click(Me, New EventArgs())
        _imgCerrar_Click(Me, New EventArgs())
    End Sub

#End Region

#Region "GridActivos helpers"

    ''' <summary>
    ''' Funcion que nos devuelve el índice de una columna concreta buscando por su SortExpression
    ''' </summary>
    ''' <param name="Columns">Colección de columnas (ejemplo:columnas de un gridview)</param>
    ''' <param name="SortExpression">Valor que buscamos como SortExpression de la columna</param>
    ''' <returns>El índice de la columna buscada</returns>
    ''' <remarks>Llamada desde gridActivos_RowDataBound. Tiempo maz inferior a 1 seg</remarks>
    Private Function ColumnIndexBySortExpression(ByVal Columns As DataControlFieldCollection, ByVal SortExpression As String) As Integer
        For i As Integer = 0 To Columns.Count - 1
            If Columns(i).SortExpression = SortExpression Then
                Return i
            End If
        Next
        Return -1
    End Function

#End Region

End Class
