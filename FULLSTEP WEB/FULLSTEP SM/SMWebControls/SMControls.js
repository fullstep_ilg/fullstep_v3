﻿function sm_SetZIndex(control, args) {

    // Set auto complete extender control's z-index to a high value

    // so it will appear on top of, not under, the ModalPopUp extended panel.

    control._completionListElement.style.zIndex = 99999999;

}

function sm_FindClicked(txtbx, tree, imgNextID) {

    // Set Regular Expression variable
    var regex = txtbx.value;
    regex = regex.toLowerCase();

    // Call the Find function
    var node = sm_find(tree, regex);

    // Set the selected node on the tree to the node that was returned by the function
    
    if (node) {
        node.getElement().scrollIntoView(true);
        node_setSelected(true);
    }

    if (imgNextID) {
        node = sm_findNext(tree, regex);
        var imgNext = document.getElementById(imgNextID)
        if (node) {
            imgNext.style.visibility = 'visible';
        }
        else {
            imgNext.style.visibility = 'hidden';
        }
    }

    // Reset objects
    node = null;
    regex = null;
}

function sm_find(tree, regex) {

    // Call the sm_findInCollection method on the tree's nodes
    var node = sm_findInCollection(tree.getNodes(), regex);

    // Reset objects
    tree = null;

    // Return the node that was found
    return node;
}

function sm_findNext(tree, regex) {

    // Get the currently selected node
    var selectedNode = tree.get_selectedNodes()[0];

    // Create a node object, and initialize it to null
    var node = null;

    // If the selected node is null it means that there were no previously selected nodes on this tree,
    // so run the find function on the tree
    if (selectedNode == null) {
        node = sm_findInCollection(tree.getNodes(), regex);

        // Reset objects
        tree = null;
        selectedNode = null;

        // Return the node that was found
        return node;
    }

    // If the selected node has children, run the findInCollection function in its collection
    if (selectedNode.hasChildren()) {
        node = sm_findInCollection(selectedNode.getItems(), regex);

        // If the node we're looking for is found, wrap up
        if (node != null) {
            // Reset objects
            tree = null;
            selectedNode = null;

            // Return the node that was found
            return node;
        }
    }

    // If the selected node has "NextSiblings" create a collection of nextsiblings nodes,
    // and run the findInCollection method on that collection
    if (selectedNode.getNextSibling() != null) {
        node = sm_findInNextSiblings(selectedNode, regex);

        // If the node we're looking for is found, wrap up
        if (node != null) {
            // Reset objects
            tree = null;
            selectedNode = null;

            // Return the node that was found
            return node;
        }
    }

    // If the selected node has a 'parent' node, go through its parent, and find within that parent's NextSiblings collection
    if (selectedNode.get_parentNode() != null) {
        //Go through the parent's collection
        var parentNode = selectedNode.get_parentNode();

        while (parentNode != null) {
            if (parentNode.getNextSibling() != null) {
                node = sm_findInNextSiblings(parentNode, regex);

                // If the node we're looking for is found, wrap up
                if (node != null) {
                    // Reset objects
                    parentNode = null;
                    tree = null;
                    selectedNode = null;

                    // Return the node that was found
                    return node;
                }
            }

            // Continue searching with the next parent node
            parentNode = parentNode.get_parentNode();
        }
    }
}

function sm_findInNextSiblings(node, regex) {
    // Create an empty array object of next siblings
    var nextSibCollection = new Array();

    // Create a sibling count variable, initialized to 0
    var sibCount = 0;

    // While the node has a next sibling, add the sibling to the sibling array
    nextSibCollection[sibCount++] = node.getNextSibling();

    while (nextSibCollection[sibCount - 1].getNextSibling() != null)
        nextSibCollection[sibCount++] = nextSibCollection[sibCount - 2].getNextSibling();

    // Run the findInCollection method on the array of collection of next siblings
    node = sm_findInCollection(nextSibCollection, regex);

    // Reset objects
    nextSibCollection = null;
    sibCount = null;

    // Return the node that was found
    return node;
}

function sm_findInCollection(NodesCollection, regex) {
    // Loop through the Nodes collection
    for (var i = 0; i < NodesCollection.get_length(); i++) {
        // Get the next Node in collection
        var thisNode = NodesCollection.getNode(i);

        // Convert the Node's text to JavaScript String object
        var nodeStr = new String(thisNode.get_text());
        nodeStr = nodeStr.toLowerCase();

        // Check if the string matches the Regular Expression - if so, this is the Node we are looking for
        if ((thisNode.get_target() == "1") && (nodeStr.match(regex)) && (regex != '')) {
            // Reset objects
            NodesCollection = null;
            regex = null;
            nodeStr = null;

            // Return the node that was found
            return thisNode;
        }
        // If this Node has child Nodes, search recursively through its children as well
        else if (thisNode.hasChildren()) {
            // Clear the nodeStr variable
            nodeStr = null;

            // Get the value that is returned after the recursion has finished
            var node = sm_findInCollection(thisNode.getItems(), regex);

            // If a Node was returned, pass it up stack as this is the node we are looking for
            if (node != null) {
                // Reset objects
                NodesCollection = null;
                regex = null;

                // Return the node that was found
                return node;
            }
        }

        // Clear the nodeStr variable
        nodeStr = null;
    }

    // Reset objects
    NodesCollection = null;
    regex = null;
}

function sm_FindNextClicked(txtbxID, tree, imgNext) {
    // Get the TextBox that has the Regular Expression
    var txtbx = document.getElementById(txtbxID);

    // Set Regular Expression variable
    var regex = txtbx.value;

    // Call FindNext function
    var node = sm_findNext(tree, regex);
    if (node) {
        node.get_element().scrollIntoView()
        // Set the selected node on the tree to the node that was returned by the function
        node_setSelected(true);
    }
    node = sm_findNext(tree, regex);
    if (node) {
        imgNext.style.visibility = 'visible';
    } else {
        imgNext.style.visibility = 'hidden';
    }

    // Reset objects
    node = null;
    regex = null;
    txtbx = null;
}

function selectRow(GridViewRow) {
    if (GridViewRow != null) {
        for (fila = 0; fila < GridViewRow.parentNode.childNodes.length; fila++) {
            if (GridViewRow.parentNode.childNodes.item(fila).attributes != null) {
                if (GridViewRow.parentNode.childNodes.item(fila).attributes.getNamedItem('esFilaActivo') != null) {
                    if (GridViewRow.parentNode.childNodes.item(fila).attributes.getNamedItem('esFilaActivo').value == 'Verdadero') {
                        GridViewRow.parentNode.childNodes.item(fila).style.backgroundColor = '#CCCCCC';
                        GridViewRow.parentNode.childNodes.item(fila).style.color = '#000000';
                        GridViewRow.parentNode.childNodes.item(fila).setAttribute('Seleccionado', 'False');
                    }
                }
            }
        }
        GridViewRow.style.backgroundColor = '#3366CC';
        GridViewRow.style.color = '#FFFFFF';
        GridViewRow.setAttribute('Seleccionado', 'True');
    }
}
function guardarActivoSeleccionado(gridActivos) {
    var codActivo;
    var GridView = document.getElementById(gridActivos).childNodes.item(0);
    for (fila = 0; fila < GridView.childNodes.length; fila++) {
        if (GridView.childNodes.item(fila).attributes != null) {
            if (GridView.childNodes.item(fila).attributes.getNamedItem('Seleccionado') != null) {
                if (GridView.childNodes.item(fila).attributes.getNamedItem('Seleccionado').value == 'True') {
                    idActivo = GridView.childNodes.item(fila).attributes.getNamedItem('ID').value
                    PageMethods.guardarActivoSeleccionado(idActivo)
                }
            }
        }
    }
}
function GuardarDatosFila(usuarioIdioma, Activo) {
    PageMethods.GuardarDatosFila(usuarioIdioma, Activo);
}
