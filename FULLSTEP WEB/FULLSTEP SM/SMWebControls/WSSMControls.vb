﻿Imports System.ComponentModel
Imports System.Web
Imports System.Web.Services
Imports Fullstep.FSNLibrary

<System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
Public Class WSSMControls
    Inherits WebServiceBase

    ''' <summary>
    ''' Devuelve la lista para autocompletar el texto introducido en las búsquedas por código de activo.
    ''' </summary>
    ''' <param name="prefixText">String con el texto actual</param>
    ''' <param name="count">Integer con el número de elementos que hay que retornar</param>
    ''' <param name="contextKey">String con el contexto específico del usuario o de la página.</param>
    ''' <returns>Un array de String con la lista de elementos</returns>
    ''' <remarks>Método vinculado a un control AutoCompleteExtender: VisorActivos.ascx</remarks>
    <System.Web.Services.WebMethod(True)> _
    Public Function Autocompletar_Codigo(ByVal prefixText As String, ByVal count As Integer, ByVal contextKey As String) As String()
        Return Autocompletar("COD", prefixText, count, contextKey)
    End Function

    ''' <summary>
    ''' Devuelve la lista para autocompletar el texto introducido en las búsquedas por denominación de activo.
    ''' </summary>
    ''' <param name="prefixText">String con el texto actual</param>
    ''' <param name="count">Integer con el número de elementos que hay que retornar</param>
    ''' <param name="contextKey">String con el contexto específico del usuario o de la página.</param>
    ''' <returns>Un array de String con la lista de elementos</returns>
    ''' <remarks>Método vinculado a un control AutoCompleteExtender: VisorActivos.ascx</remarks>
    <System.Web.Services.WebMethod(True)> _
    Public Function Autocompletar_Denominacion(ByVal prefixText As String, ByVal count As Integer, ByVal contextKey As String) As String()
        Return Autocompletar("DEN_ACTIVO", prefixText, count, contextKey)
    End Function

    ''' <summary>
    ''' Devuelve la lista para autocompletar el texto introducido en las búsquedas por número de ERP de activo.
    ''' </summary>
    ''' <param name="prefixText">String con el texto actual</param>
    ''' <param name="count">Integer con el número de elementos que hay que retornar</param>
    ''' <param name="contextKey">String con el contexto específico del usuario o de la página.</param>
    ''' <returns>Un array de String con la lista de elementos</returns>
    ''' <remarks>Método vinculado a un control AutoCompleteExtender: VisorActivos.ascx</remarks>
    <System.Web.Services.WebMethod(True)> _
    Public Function Autocompletar_NumeroERP(ByVal prefixText As String, ByVal count As Integer, ByVal contextKey As String) As String()
        Return Autocompletar("COD_ERP", prefixText, count, contextKey)
    End Function

    ''' <summary>
    ''' Devuelve la lista para autocompletar el texto introducido en las búsquedas por centro de activo.
    ''' </summary>
    ''' <param name="prefixText">String con el texto actual</param>
    ''' <param name="count">Integer con el número de elementos que hay que retornar</param>
    ''' <param name="contextKey">String con el contexto específico del usuario o de la página.</param>
    ''' <returns>Un array de String con la lista de elementos</returns>
    ''' <remarks>Método vinculado a un control AutoCompleteExtender: VisorActivos.ascx</remarks>
    <System.Web.Services.WebMethod(True)> _
    Public Function Autocompletar_Centro(ByVal prefixText As String, ByVal count As Integer, ByVal contextKey As String) As String()
        Return Autocompletar("ID_DEN_CENTRO", prefixText, count, contextKey)
    End Function

    ''' <summary>
    ''' Devuelve los elementos que cumplen con los criterios de autocompletar para el campo del origen de datos seleccionado.
    ''' </summary>
    ''' <param name="targetField">String con nombre del campo del origen de datos del GridView</param>
    ''' <param name="prefixText">String con el texto actual</param>
    ''' <param name="count">Integer con el número de elementos que hay que retornar</param>
    ''' <param name="contextKey">String con el contexto específico del usuario o de la página.</param>
    ''' <returns>Un array de String con la lista de elementos</returns>
    ''' <remarks>Esta función se invoca desde todos los métodos de este servicio web</remarks>
    Private Function Autocompletar(ByVal targetField As String, ByVal prefixText As String, ByVal count As Integer, ByVal contextKey As String) As String()
        Dim result As New List(Of String)
        If HttpContext.Current.Cache("Activos_" & contextKey) Is Nothing Then
            Return Nothing
        Else
            Dim dtActivos As DataTable
            If HttpContext.Current.Cache("ActivosLastSearch_" & contextKey) IsNot Nothing Then
                dtActivos = CType(HttpContext.Current.Cache("ActivosLastSearch_" & contextKey), DataView).Table
            Else
                dtActivos = CType(HttpContext.Current.Cache("Activos_" & contextKey), DataTable)
            End If
            Dim pattern As String() = Split(prefixText)
            Dim matches As IEnumerable(Of String) = From Datos In dtActivos _
                        Where pattern.All(Function(palabra) UCase(DBNullToSomething(Datos(targetField)).ToString()) Like String.Concat("*", strToSQLLIKE(palabra, True), "*")) _
                        Select CType(Datos.Item(targetField), String) Distinct.Take(10)

            For Each match As String In matches
                result.Add(match)
            Next
            Return result.ToArray
        End If
    End Function

End Class
