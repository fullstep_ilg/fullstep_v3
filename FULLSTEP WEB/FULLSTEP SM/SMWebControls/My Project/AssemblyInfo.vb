﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices
Imports System.Web.UI

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("SMWebControls")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyProduct("SMWebControls")> 

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("b9f81160-f85c-486b-b362-b83ccd4a8872")> 

<Assembly: WebResource("Fullstep.SMWebControls.SMControls.js", "application/x-javascript")> 
<Assembly: WebResource("Fullstep.SMWebControls.centrocoste_small.jpg", "image/jpeg")> 
<Assembly: WebResource("Fullstep.SMWebControls.centrocoste.jpg", "image/jpeg")> 
<Assembly: WebResource("Fullstep.SMWebControls.Bt_Cerrar.png", "image/png")> 
<Assembly: WebResource("Fullstep.SMWebControls.carpetaNaranja.gif", "image/gif")> 
<Assembly: WebResource("Fullstep.SMWebControls.World.gif", "image/gif")> 
<Assembly: WebResource("Fullstep.SMWebControls.ActivosBuscar.jpg", "image/jpeg")> 
<Assembly: WebResource("Fullstep.SMWebControls.lupa.gif", "image/gif")> 
<Assembly: WebResource("Fullstep.SMWebControls.flecha_siguiente.gif", "image/gif")> 
<Assembly: WebResource("Fullstep.SMWebControls.candado.gif", "image/gif")> 
