﻿Imports System.Security.Permissions
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.HtmlControls
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.ComponentModel
Imports System.Configuration
Imports System
Imports System.Drawing.Design


Public Class FSQAWebPartCertificados
    Inherits WebPart

    Private lblTitulo As Label
    Private lblTotal As Label
    Private lblNumTotal As Label
    Private lblMensaje As Label
    Private lstSolicitudes As DataList

    Private lblError As Label

    Private WithEvents btnVerSiguientes As FSNWebControls.FSNButton
    Private WithEvents btnVerAnteriores As FSNWebControls.FSNButton

    Private sTitulo As String
    Private Shared _Textos(77) As String
    Private _sIdioma As String
    Private mNumLineas As String
    Private mFechaLimiteCumplimentacion As Integer 'Indice de la combo fecha limite de cumplimentación
    Private mVerFechaActu As Boolean 'Indica si esta o no seleccionado el check de "Mostrar solo las modificaciones...
    Private mFechaActualizacion As Integer 'Indica el indice de la fecha de la ultima actualizacion
    Private mTipoEstados As String
    Private mTipoCertificados As String
    Private mTipoPeticionario As Integer
    Private mbMostrarMensaje As Boolean
    Private Shared Dateformat As System.Globalization.DateTimeFormatInfo

    Private miIndiceTipoProveedor As Integer 'Indica la seleccion de los option buttons del tipo de proveedore selecccionado
    Private msCodProves As String ' Lista de codigo de proveedores separados por #
    Private msDenProves As String
    Private mbMostrarProveedoresBaja As Boolean 'Almacena si se muestra o no los proveedores dados de baja

    Private Class DataListTemplate
        Implements ITemplate

        ''' <summary>
        ''' Define el objeto Control al que pertenecen los controles secundarios y las plantillas. Estos controles secundarios están a su vez definidos en una plantilla en línea.
        ''' </summary>
        ''' <param name="container">Objeto Control que contiene las instancias de los controles de la plantilla en línea.</param>
        ''' <remarks>Llamada desde; Tiempo máximo=0seg.</remarks>
        Private Sub InstantiateIn(ByVal container As Control) _
           Implements ITemplate.InstantiateIn
            Dim hr = New HtmlGenericControl("hr")
            container.Controls.Add(hr)
            Dim tabla As New HtmlTable()
            tabla.Border = 0
            tabla.Width = "100%"


            Dim fila As New HtmlTableRow()
            Dim celda As New HtmlTableCell()

            fila = New HtmlTableRow
            Dim lab = New Label()
            AddHandler lab.DataBinding, AddressOf Titulo_DataBinding
            celda.Attributes.Add("class", "ListItemLink")
            celda.Controls.Add(lab)
            fila.Cells.Add(celda)
            celda = New HtmlTableCell()
            celda.Align = "left"
            celda.VAlign = "middle"
            fila.Cells.Add(celda)
            tabla.Rows.Add(fila)

            fila = New HtmlTableRow()
            celda = New HtmlTableCell()
            Dim fsnlinkinfo As New FSNWebControls.FSNLinkInfo
            fsnlinkinfo.PanelInfo = "FSNPanelDatosProveedorQA"
            fsnlinkinfo.CssClass = "Rotulo"
            AddHandler fsnlinkinfo.DataBinding, AddressOf Proveedor_DataBinding
            celda.Controls.Add(fsnlinkinfo)
            fila.Cells.Add(celda)
            tabla.Rows.Add(fila)



            fila = New HtmlTableRow()
            celda = New HtmlTableCell()
            celda.ColSpan = 2
            lab = New Label()
            AddHandler lab.DataBinding, AddressOf Estado_DataBinding
            celda.Controls.Add(lab)
            fila.Cells.Add(celda)
            tabla.Rows.Add(fila)

            fila = New HtmlTableRow()
            celda = New HtmlTableCell()
            celda.ColSpan = 2
            lab = New Label()
            AddHandler lab.DataBinding, AddressOf FechaExpiracion_DataBinding
            celda.Controls.Add(lab)
            fila.Cells.Add(celda)
            tabla.Rows.Add(fila)

            fila = New HtmlTableRow()
            celda = New HtmlTableCell()
            celda.ColSpan = 2
            lab = New Label()
            AddHandler lab.DataBinding, AddressOf FechaLimiteCumplimentacion_DataBinding
            celda.Controls.Add(lab)
            fila.Cells.Add(celda)
            tabla.Rows.Add(fila)

            fila = New HtmlTableRow()
            celda = New HtmlTableCell()
            celda.ColSpan = 2
            lab = New Label()
            AddHandler lab.DataBinding, AddressOf FechaActualizacion_DataBinding
            celda.Controls.Add(lab)
            fila.Cells.Add(celda)
            tabla.Rows.Add(fila)

            fila = New HtmlTableRow()
            celda = New HtmlTableCell()
            fsnlinkinfo = New FSNWebControls.FSNLinkInfo
            fsnlinkinfo.PanelInfo = "FSNPanelDatosPersonaQA"
            fsnlinkinfo.CssClass = "Rotulo"
            AddHandler fsnlinkinfo.DataBinding, AddressOf Peticionario_DataBinding
            celda.Controls.Add(fsnlinkinfo)

            fila.Cells.Add(celda)
            tabla.Rows.Add(fila)
            container.Controls.Add(tabla)

        End Sub

        ''' <summary>
        ''' Se produce al enlazar la etiqueta de la plantilla en que se muestra la empresa con el origen de datos.
        ''' </summary>
        ''' <remarks>Llamada desde:=InstantiateIn;TiempoEjecucion=0seg</remarks>
        Private Sub Titulo_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim lab As Label = CType(sender, Label)
            Dim container As DataListItem = CType(lab.NamingContainer, DataListItem)
            lab.Text = "<b>" & DataBinder.Eval(container.DataItem, "TipoCertificado") & "</b>"

            lab.CssClass = "EtiquetaMediano"

            Dim iInstancia As Long
            iInstancia = DataBinder.Eval(container.DataItem, "Instancia")

            Dim URL As String
            URL = ConfigurationManager.AppSettings("rutaQA") & "frames.aspx?pagina=" & HttpContext.Current.Server.UrlEncode("certificados/DetalleCertificado.aspx?Certificado=" & DataBinder.Eval(container.DataItem, "CERTIFICADO"))
            lab.Attributes.Add("onclick", "return ComprobarInstanciaEnProcesoWebPart(" & iInstancia & ",'" & URL & "','" & ConfigurationManager.AppSettings("rutaFS") & "'," & FSNLibrary.TiposDeDatos.TipoDeSolicitud.Certificado & ");")
        End Sub


        ''' <summary>
        ''' Se produce al enlazar la etiqueta de la plantilla en que se muestra el proveedor con el origen de datos.
        ''' </summary>
        ''' <remarks>Llamada desde:=InstantiateIn;TiempoEjecucion=0seg</remarks>
        Private Sub Proveedor_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim fsn As FSNWebControls.FSNLinkInfo = CType(sender, FSNWebControls.FSNLinkInfo)
            Dim container As DataListItem = CType(fsn.NamingContainer, DataListItem)
            fsn.Text = DataBinder.Eval(container.DataItem, "CodProveedor") & "-" & DataBinder.Eval(container.DataItem, "Proveedor")
            fsn.ContextKey = DataBinder.Eval(container.DataItem, "CodProveedor")

        End Sub

        ''' <summary>
        ''' Se produce al enlazar la etiqueta de la plantilla en que se muestra el estado del pedido con el origen de datos.
        ''' </summary>
        ''' <remarks>Llamada desde:=InstantiateIn;TiempoEjecucion=0seg</remarks>
        Private Sub Estado_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim lab As Label = CType(sender, Label)
            Dim container As DataListItem = CType(lab.NamingContainer, DataListItem)
            Dim sEstado As String = ""


            If DataBinder.Eval(container.DataItem, "EstadoSinRespuestaProveedor") = True Then
                'sEstado = "Sin respuesta por parte del proveedor"
                sEstado = _Textos(19)
            End If

            If DataBinder.Eval(container.DataItem, "EstadoConDatosGuardadosProveedor") = True Then
                If sEstado <> "" Then sEstado = sEstado & ", "
                'sEstado = sEstado & " con datos guardados por el proveedor sin enviar"
                sEstado = sEstado & _Textos(20)
            End If
            If DataBinder.Eval(container.DataItem, "EstadoConCertificado") = True Then
                If sEstado <> "" Then sEstado = sEstado & ", "
                'sEstado = sEstado & " con certificado"
                sEstado = sEstado & _Textos(21)
            End If
            If DataBinder.Eval(container.DataItem, "EstadoExpirado") = True Then
                If sEstado <> "" Then sEstado = sEstado & ", "
                'sEstado = sEstado & " expirado"
                sEstado = sEstado & _Textos(22)
            End If
            If DataBinder.Eval(container.DataItem, "EstadoProximoAExpirar") = True Then
                If sEstado <> "" Then sEstado = sEstado & ", "
                'sEstado = sEstado & " próximo a expirar"
                sEstado = sEstado & _Textos(23)
            End If
            If DataBinder.Eval(container.DataItem, "EstadoPublicado") = True Then
                If sEstado <> "" Then sEstado = sEstado & ", "
                'sEstado = sEstado & " publicado"
                sEstado = sEstado & _Textos(24)
            End If
            If DataBinder.Eval(container.DataItem, "EstadoDesPublicado") = True Then
                If sEstado <> "" Then sEstado = sEstado & ", "
                'sEstado = sEstado & " despublicado"
                sEstado = sEstado & _Textos(25)
            End If
            If DataBinder.Eval(container.DataItem, "EstadoSolicitado") = True Then
                If sEstado <> "" Then sEstado = sEstado & ", "
                'sEstado = sEstado & " solicitado"
                sEstado = sEstado & _Textos(26)
            End If
            If DataBinder.Eval(container.DataItem, "EstadoRenovado") = True Then
                If sEstado <> "" Then sEstado = sEstado & ", "
                'sEstado = sEstado & " renovado"
                sEstado = sEstado & _Textos(27)
            End If
            If DataBinder.Eval(container.DataItem, "EstadoEnviado") = True Then
                If sEstado <> "" Then sEstado = sEstado & ", "
                'sEstado = sEstado & " enviado"
                sEstado = sEstado & _Textos(28)
            End If
            If DataBinder.Eval(container.DataItem, "EstadoPendienteValidar") = True Then
                If sEstado <> "" Then sEstado = sEstado & ", "
                sEstado = sEstado & _Textos(76)
            End If
            If DataBinder.Eval(container.DataItem, "EstadoNoValido") = True Then
                If sEstado <> "" Then sEstado = sEstado & ", "
                sEstado = sEstado & _Textos(77)
            End If

            lab.Text = sEstado
            'lab.CssClass = "Rotulo"
            'lab.Font.Bold = False
        End Sub

        ''' <summary>
        ''' Se produce al enlazar la etiqueta de la plantilla en que se muestra el importe con el origen de datos.
        ''' </summary>
        ''' <remarks>Llamada desde:=InstantiateIn;TiempoEjecucion=0seg</remarks>
        Private Sub FechaExpiracion_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim lab As Label = CType(sender, Label)
            Dim container As DataListItem = CType(lab.NamingContainer, DataListItem)

            If DataBinder.Eval(container.DataItem, "FechaExpiracion") <> "#12:00:00 AM#" Then
                'lab.Text = "Fecha Expiración: " & DataBinder.Eval(container.DataItem, "FechaExpiracion")
                lab.Text = "<b>" & _Textos(29) & ": </b>" & FormatDate(DataBinder.Eval(container.DataItem, "FechaExpiracion"), Dateformat)
                'lab.Font.Bold = True
            End If

        End Sub
        ''' <summary>
        ''' Se produce al enlazar la etiqueta de la plantilla en que se muestra la fecha límite de cumplimentación con el origen de datos.
        ''' </summary>
        ''' <remarks>Llamada desde:=InstantiateIn;TiempoEjecucion=0seg</remarks>
        Private Sub FechaLimiteCumplimentacion_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim lab As Label = CType(sender, Label)
            Dim container As DataListItem = CType(lab.NamingContainer, DataListItem)

            If DataBinder.Eval(container.DataItem, "FechaLimiteCumplimentacionVisible") = "1" Then
                If DataBinder.Eval(container.DataItem, "FechaLimiteCumplimentacion") <> "#12:00:00 AM#" Then
                    lab.Text = "<b>" & _Textos(30) & ": </b>" & FormatDate(DataBinder.Eval(container.DataItem, "FechaLimiteCumplimentacion"), Dateformat)
                ElseIf DataBinder.Eval(container.DataItem, "EstadoSinSolicitar") = True Then
                    lab.Text = ""
                Else
                    lab.Text = "<b>" & _Textos(30) & ": </b>"
                End If
            End If

        End Sub

        ''' <summary>
        ''' Se produce al enlazar la etiqueta de la plantilla en que se muestra el Peticionario, solo si es distinto al usuario de entrada.
        ''' </summary>
        ''' <remarks>Llamada desde:=InstantiateIn;TiempoEjecucion=0seg</remarks>
        Private Sub Peticionario_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs)
            'Dim lc As Literal = CType(sender, Literal)
            'Dim container As DataListItem = CType(lc.NamingContainer, DataListItem)
            'Dim sPeticionario As String
            'sPeticionario = DataBinder.Eval(container.DataItem, "Peticionario")
            'If sPeticionario <> "" Then ' "" = -> El peticionario es el mismo al de la entrada y no tiene que mostrarlo
            '    lc.Text = "<b>" & _Textos(31) & ": </b>" & sPeticionario
            'End If

            Dim fsn As FSNWebControls.FSNLinkInfo = CType(sender, FSNWebControls.FSNLinkInfo)
            Dim container As DataListItem = CType(fsn.NamingContainer, DataListItem)
            Dim sPeticionario As String = DataBinder.Eval(container.DataItem, "Peticionario")

            If sPeticionario <> "" Then ' "" = -> El peticionario es el mismo al de la entrada y no tiene que mostrarlo
                'lc.Text = "<b>Peticionario: </b>" & sPeticionario
                fsn.Text = "<b>" & _Textos(31) & ": </b>" & sPeticionario
                fsn.ContextKey = DataBinder.Eval(container.DataItem, "CodPeticionario")

            End If
        End Sub

        ''' <summary>
        ''' Se produce al enlazar la etiqueta de la plantilla en que se muestra la fecha de Actualizacion con el origen de datos.
        ''' </summary>
        ''' <remarks>Llamada desde:=InstantiateIn;TiempoEjecucion=0seg</remarks>
        Private Sub FechaActualizacion_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim lab As Label = CType(sender, Label)
            Dim container As DataListItem = CType(lab.NamingContainer, DataListItem)
            If DataBinder.Eval(container.DataItem, "FechaUltAct") <> "#12:00:00 AM#" Then
                'lab.Text = "Fecha última actualizacion: " & DataBinder.Eval(container.DataItem, "FechaUltAct")
                lab.Text = "<b>" & _Textos(59) & ": </b>" & FormatDate(DataBinder.Eval(container.DataItem, "FechaUltAct"), Dateformat)
            End If

        End Sub

        ''' <summary>
        ''' Devuelve un string con la fecha pasada en dDate formateada según oDateFormat, por defecto aplica formato d:01/01/1900
        ''' </summary>
        ''' <param name="dDate">fecha a formatear</param>
        ''' <param name="oDateFormat">IFormatProvider</param>
        ''' <param name="sFormat">Format: d :08/17/2000; D :Thursday, August 17, 2000; f :Thursday, August 17, 2000 16:32; F :Thursday, August 17, 2000 16:32:32; g :08/17/2000 16:32;G :08/17/2000 16:32:32...
        '''</param>
        ''' <returns>String con la fecha</returns>
        ''' <remarks>Llamada desde=:FechaResolucion_DataBinding;Tiempo ejecucion = 0</remarks>
        Public Function FormatDate(ByVal dDate As Date, ByVal oDateFormat As System.Globalization.DateTimeFormatInfo, Optional ByVal sFormat As String = "d") As String
            Return dDate.ToString(sFormat, oDateFormat)
        End Function
    End Class

    <Flags()> _
    Public Enum FechaLimiteCumplimentacionValores As Integer
        Vacio = 0
        Sobrepasada = 1
        Dias1 = 2
        Dias2 = 3
        Dias3 = 4
        Dias4 = 5
        Dias5 = 6
        Dias6 = 7
        Semana1 = 8
        Semana2 = 9
        Semana3 = 10
        Mes1 = 11
    End Enum

    <Flags()> _
Public Enum FechaActualizacionValores As Integer
        UltimoDia = 0
        Dias2 = 1
        Dias3 = 2
        Dias4 = 3
        Dias5 = 4
        Dias6 = 5
        Semana1 = 6
        Semana2 = 7
        Semana3 = 8
        Mes1 = 9
    End Enum

    <Flags()> _
Public Enum TipoPeticionarioValores As Integer
        Todos = 0
        DelUsuario = 1
    End Enum

   

    '' *****************************
    '' A continuacion se establecen las propiedades de la lista que se muestran en el webPart
    <Serializable()> _
    Public Class mOrden
        Private mlInstancia As Long
        Private mlCertificado As Long
        Private msTipoCertificado As String
        Private msCodProveedor As String
        Private msProveedor As String

        Private mbEstadoSinRespuestaProveedor As Boolean
        Private mbEstadoConDatosGuardadosProveedor As Boolean
        Private mbEstadoConCertificado As Boolean
        Private mbEstadoExpirado As Boolean
        Private mbEstadoProximoAExpirar As Boolean
        Private mbEstadoPublicado As Boolean
        Private mbEstadoDespublicado As Boolean
        Private mbEstadoSolicitado As Boolean
        Private mbEstadoRenovado As Boolean
        Private mbEstadoEnviado As Boolean
        Private mbEstadoSinSolicitar As Boolean
        Private mbEstadoPendienteValidar As Boolean
        Private mbEstadoNoValido As Boolean

        Private mbFechaLimiteCumplimentacionVisible As Boolean 'Para indicar si se muestra o no las fechas límite de cumplimentación
        Private mdFechaExpiracion As Date
        Private mdFechaLimiteCumplimentacion As Date
        Private mdFechaUltAct As Date
        Private msCodPeticionario As String
        Private msPeticionario As String

        Public Property Instancia() As Long
            Get
                Return mlInstancia
            End Get
            Set(ByVal value As Long)
                mlInstancia = value
            End Set
        End Property

        Public Property Certificado() As Long
            Get
                Return mlCertificado
            End Get
            Set(ByVal value As Long)
                mlCertificado = value
            End Set
        End Property

        Public Property TipoCertificado() As String
            Get
                Return msTipoCertificado
            End Get
            Set(ByVal value As String)
                msTipoCertificado = value
            End Set
        End Property

        Public Property CodProveedor() As String
            Get
                Return msCodProveedor
            End Get
            Set(ByVal value As String)
                msCodProveedor = value
            End Set
        End Property

        Public Property Proveedor() As String
            Get
                Return msProveedor
            End Get
            Set(ByVal value As String)
                msProveedor = value
            End Set
        End Property

        Public Property EstadoSinRespuestaProveedor() As Boolean
            Get
                Return mbEstadoSinRespuestaProveedor
            End Get
            Set(ByVal value As Boolean)
                mbEstadoSinRespuestaProveedor = value
            End Set
        End Property

        Public Property EstadoConDatosGuardadosProveedor() As Boolean
            Get
                Return mbEstadoConDatosGuardadosProveedor
            End Get
            Set(ByVal value As Boolean)
                mbEstadoConDatosGuardadosProveedor = value
            End Set
        End Property

        Public Property EstadoConCertificado() As Boolean
            Get
                Return mbEstadoConCertificado
            End Get
            Set(ByVal value As Boolean)
                mbEstadoConCertificado = value
            End Set
        End Property

        Public Property EstadoExpirado() As Boolean
            Get
                Return mbEstadoExpirado
            End Get
            Set(ByVal value As Boolean)
                mbEstadoExpirado = value
            End Set
        End Property

        Public Property EstadoProximoAExpirar() As Boolean
            Get
                Return mbEstadoProximoAExpirar
            End Get
            Set(ByVal value As Boolean)
                mbEstadoProximoAExpirar = value
            End Set
        End Property

        Public Property EstadoPublicado() As Boolean
            Get
                Return mbEstadoPublicado
            End Get
            Set(ByVal value As Boolean)
                mbEstadoPublicado = value
            End Set
        End Property

        Public Property EstadoDesPublicado() As Boolean
            Get
                Return mbEstadoDespublicado
            End Get
            Set(ByVal value As Boolean)
                mbEstadoDespublicado = value
            End Set
        End Property

        Public Property EstadoSolicitado() As Boolean
            Get
                Return mbEstadoSolicitado
            End Get
            Set(ByVal value As Boolean)
                mbEstadoSolicitado = value
            End Set
        End Property

        Public Property EstadoRenovado() As Boolean
            Get
                Return mbEstadoRenovado
            End Get
            Set(ByVal value As Boolean)
                mbEstadoRenovado = value
            End Set
        End Property

        Public Property EstadoEnviado() As Boolean
            Get
                Return mbEstadoEnviado
            End Get
            Set(ByVal value As Boolean)
                mbEstadoEnviado = value
            End Set
        End Property

        Public Property CodPeticionario() As String
            Get
                Return msCodPeticionario
            End Get
            Set(ByVal value As String)
                msCodPeticionario = value
            End Set
        End Property

        Public Property Peticionario() As String
            Get
                Return msPeticionario
            End Get
            Set(ByVal value As String)
                msPeticionario = value
            End Set
        End Property

        Public Property FechaExpiracion() As Date
            Get
                Return mdFechaExpiracion
            End Get
            Set(ByVal value As Date)
                mdFechaExpiracion = value
            End Set
        End Property

        Public Property FechaLimiteCumplimentacion() As Date
            Get
                Return mdFechaLimiteCumplimentacion
            End Get
            Set(ByVal value As Date)
                mdFechaLimiteCumplimentacion = value
            End Set
        End Property

        Public Property FechaUltAct() As Date
            Get
                Return mdFechaUltAct
            End Get
            Set(ByVal value As Date)
                mdFechaUltAct = value
            End Set
        End Property

        Public Property FechaLimiteCumplimentacionVisible() As Boolean
            Get
                Return mbFechaLimiteCumplimentacionVisible
            End Get
            Set(ByVal value As Boolean)
                mbFechaLimiteCumplimentacionVisible = value
            End Set
        End Property

        ''' <summary>
        ''' Da acceso a la propiedad q indica si el certificado esta sin solicitar
        ''' </summary>
        ''' <remarks>Llamada desde: CargarCertificados   FechaLimiteCumplimentacion_DataBinding; Tiempo maximo:0</remarks>
        Public Property EstadoSinSolicitar() As Boolean
            Get
                Return mbEstadoSinSolicitar
            End Get
            Set(ByVal value As Boolean)
                mbEstadoSinSolicitar = value
            End Set
        End Property

        ''' <summary>
        ''' Dar acceso a la propiedad q indi a si el estado del certificado es "Pendiente de validar" 
        ''' </summary>
        ''' <remarks>Llamada desde: CargarCertificados; Tiempo máximo: 0</remarks>
        Public Property EstadoPendienteValidar() As Boolean
            Get
                Return mbEstadoPendienteValidar
            End Get
            Set(ByVal value As Boolean)
                mbEstadoPendienteValidar = value
            End Set
        End Property

        ''' <summary>
        ''' Dar acceso a la propiedad q indi a si el estado del certificado es "No válido" 
        ''' </summary>
        ''' <remarks>Llamada desde: CargarCertificados; Tiempo máximo: 0</remarks>
        Public Property EstadoNoValido() As Boolean
            Get
                Return mbEstadoNoValido
            End Get
            Set(ByVal value As Boolean)
                mbEstadoNoValido = value
            End Set
        End Property
    End Class
    '' *****************************
    '' Finalizacion de las propiedades de las propiedades de la lista que se muestran en el webPart


    ''******* Elementos del part!!!!

    Private Class ControlEditor
        Inherits EditorPart

        Private _txtTitulo As TextBox
        Private _txtAncho As TextBox
        Private _reqAncho As RequiredFieldValidator
        Private _ranAncho As RangeValidator
        Private _wneNumCertificados As Infragistics.Web.UI.EditorControls.WebNumericEditor
        Private _reqNumCertificados As RequiredFieldValidator
        Private _chkTipoEstados1 As CheckBoxList
        Private _chkTipoEstados2 As CheckBoxList
        Private _chkTipoEstados3 As CheckBoxList
        Private _chkTipoCertificados As CheckBoxList
        Private _lstFechaLimiteCumplimentacion As ListBox
        Private _chkActualizacion As CheckBox
        Private _lstFechaActualizacion As ListBox
        Private _lstPeticionario As ListBox
        Private _tvUNQA As Global.System.Web.UI.WebControls.TreeView

        Private _rblTipoProveedores As RadioButtonList
        Private _lstProveedores As ListBox
        Private _hidCodProves As HiddenField
        Private _hidDenProves As HiddenField
        Private _imgMas As HtmlImage
        Private _imgMenos As HtmlImage
        Private _chkBajaProveedor As CheckBox


        ''' <summary>
        ''' Lo llama el marco de trabajo de las páginas ASP.NET para indicar a los controles de servidor que utilizan la implementación basada en la composición que creen los controles secundarios que contengan como forma de preparar la devolución o representación de los datos.
        ''' </summary>
        ''' <remarks>Llamada desde:sistema; Tiempo máximo:0</remarks>
        Protected Overrides Sub CreateChildControls()
            Dim part As FSQAWebPartCertificados = CType(WebPartToEdit, FSQAWebPartCertificados)
            Controls.Clear()
            _txtTitulo = New TextBox()
            _txtTitulo.Width = Unit.Pixel(250)
            _txtTitulo.Text = part.Title
            Controls.Add(_txtTitulo)
            _txtAncho = New TextBox()
            _txtAncho.MaxLength = 3
            _txtAncho.Width = Unit.Pixel(30)
            _txtAncho.Text = part.Width.Value.ToString()
            _txtAncho.ID = Me.ID & "_txtAncho"
            Controls.Add(_txtAncho)
            _reqAncho = New RequiredFieldValidator()
            _reqAncho.ID = _txtAncho.ID & "_Req"
            _reqAncho.ErrorMessage = "*"
            _reqAncho.ControlToValidate = _txtAncho.ID
            Controls.Add(_reqAncho)
            _ranAncho = New RangeValidator()
            _ranAncho.ID = _txtAncho.ID & "_Ran"
            _ranAncho.Type = ValidationDataType.Integer
            _ranAncho.MaximumValue = 500
            _ranAncho.MinimumValue = 200
            _ranAncho.ErrorMessage = "(200-500)"
            _ranAncho.ControlToValidate = _txtAncho.ID
            Controls.Add(_ranAncho)
            'nº de Certificados
            _wneNumCertificados = New Infragistics.Web.UI.EditorControls.WebNumericEditor
            _wneNumCertificados.ID = Me.ID & "_wneNumCertificados"
            _wneNumCertificados.Width = Unit.Pixel(30)
            _wneNumCertificados.MaxLength = 2
            _wneNumCertificados.DataMode = Infragistics.Web.UI.EditorControls.NumericDataMode.Int
            Controls.Add(_wneNumCertificados)
            _reqNumCertificados = New RequiredFieldValidator()
            _reqNumCertificados.ID = _reqNumCertificados.ID & "_Req"
            _reqNumCertificados.ErrorMessage = "*"
            _reqNumCertificados.ControlToValidate = _wneNumCertificados.ID
            Controls.Add(_reqNumCertificados)
            _chkTipoEstados1 = New CheckBoxList
            _chkTipoEstados1.Items.Clear()
            _chkTipoEstados1.Items.Add(New ListItem(part.Textos(73), 11))
            _chkTipoEstados1.Items.Add(New ListItem(part.Textos(19), 1))
            _chkTipoEstados1.Items.Add(New ListItem(part.Textos(20), 2))
            _chkTipoEstados1.Items.Add(New ListItem(part.Textos(76), 12))
            _chkTipoEstados1.Items.Add(New ListItem(part.Textos(77), 13))
            _chkTipoEstados1.Items.Add(New ListItem(part.Textos(21), 3))
            _chkTipoEstados1.Items.Add(New ListItem(part.Textos(32), 4))
            _chkTipoEstados1.Items.Add(New ListItem(part.Textos(33), 5))
            Controls.Add(_chkTipoEstados1)
            _chkTipoEstados2 = New CheckBoxList
            _chkTipoEstados2.Items.Clear()
            _chkTipoEstados2.Items.Add(New ListItem(part.Textos(34), 6))
            _chkTipoEstados2.Items.Add(New ListItem(part.Textos(35), 7))
            Controls.Add(_chkTipoEstados2)
            _chkTipoEstados3 = New CheckBoxList
            _chkTipoEstados3.Items.Clear()
            _chkTipoEstados3.Items.Add(New ListItem(part.Textos(36), 8))
            _chkTipoEstados3.Items.Add(New ListItem(part.Textos(37), 9))
            _chkTipoEstados3.Items.Add(New ListItem(part.Textos(38), 10))
            Controls.Add(_chkTipoEstados3)

            Try
                Dim pag As FSNServer.IFSNPage = CType(Me.Page, FSNServer.IFSNPage)
                Dim cSolicitudes As FSNServer.Solicitudes
                Dim FSWSServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
                cSolicitudes = FSWSServer.Get_Object(GetType(FSNServer.Solicitudes))

                _chkTipoCertificados = New CheckBoxList
                _chkTipoCertificados.Items.Clear()
                _chkTipoCertificados.DataTextField = "DEN"
                _chkTipoCertificados.DataValueField = "ID"
                cSolicitudes.LoadData(pag.Usuario.Cod, pag.Idioma, 1, 2)
                cSolicitudes.Data.Tables(0).Rows.RemoveAt(0)
                _chkTipoCertificados.DataSource = cSolicitudes.Data
                _chkTipoCertificados.DataBind()
                _chkTipoCertificados.ID = Me.ID & "_chkTipoCertificados"
                _chkTipoCertificados.RepeatColumns = 2
                Controls.Add(_chkTipoCertificados)
            Catch ex As Exception
                'Unicamente buscamos que la carga no casque.No lanzamos la excepción ni la tratamos.
                'Lo que si hacemos es añadir los controles para que luego no casque
                Controls.Add(_chkTipoCertificados)
            End Try

            _lstFechaLimiteCumplimentacion = New ListBox
            _lstFechaLimiteCumplimentacion.Items.Clear()
            _lstFechaLimiteCumplimentacion.SelectionMode = ListSelectionMode.Single
            _lstFechaLimiteCumplimentacion.Rows = 1
            _lstFechaLimiteCumplimentacion.Items.Add("")
            _lstFechaLimiteCumplimentacion.Items.Add(part.Textos(39)) 'sobrepasada
            _lstFechaLimiteCumplimentacion.Items.Add("1 " & part.Textos(40)) 'dia
            For i As Short = 2 To 6
                _lstFechaLimiteCumplimentacion.Items.Add(i & " " & part.Textos(41)) 'dias
            Next
            _lstFechaLimiteCumplimentacion.Items.Add("1 " & part.Textos(42)) ' Semana
            _lstFechaLimiteCumplimentacion.Items.Add("2 " & part.Textos(43)) ' Semanas
            _lstFechaLimiteCumplimentacion.Items.Add("3 " & part.Textos(43)) ' Semanas
            _lstFechaLimiteCumplimentacion.Items.Add("1  " & part.Textos(44)) ' mes
            Controls.Add(_lstFechaLimiteCumplimentacion)

            _chkActualizacion = New CheckBox
            Controls.Add(_chkActualizacion)

            _lstFechaActualizacion = New ListBox
            _lstFechaActualizacion.Items.Clear()
            _lstFechaActualizacion.SelectionMode = ListSelectionMode.Single
            _lstFechaActualizacion.Rows = 1
            _lstFechaActualizacion.Items.Add(part.Textos(45)) 'el último día
            For i As Short = 2 To 6
                _lstFechaActualizacion.Items.Add(Replace(part.Textos(46), "X", i)) '"hace 2 días"
            Next
            _lstFechaActualizacion.Items.Add(part.Textos(47)) '"hace 1 semana"
            _lstFechaActualizacion.Items.Add(Replace(part.Textos(48), "X", 2)) '"hace 2 semanas"
            _lstFechaActualizacion.Items.Add(Replace(part.Textos(48), "X", 3)) '"hace 3 semanas"
            _lstFechaActualizacion.Items.Add(part.Textos(49)) '"hace 1 mes"
            Controls.Add(_lstFechaActualizacion)


            _lstPeticionario = New ListBox
            _lstPeticionario.Items.Clear()
            _lstPeticionario.SelectionMode = ListSelectionMode.Single
            _lstPeticionario.Rows = 1
            _lstPeticionario.Items.Add(New ListItem(part.Textos(50), 0)) 'Todas
            _lstPeticionario.Items.Add(New ListItem(part.Textos(51), 1)) 'del usuario
            Controls.Add(_lstPeticionario)

            'Tipo proveedores
            Dim sTipoProveedores As String
            _rblTipoProveedores = New RadioButtonList
            _rblTipoProveedores.ID = Me.ID & "_rblTipoProveedores"
            sTipoProveedores = Me.ClientID & "_" & Me.ID & "_rblTipoProveedores_3"
            For i = 0 To 3
                _rblTipoProveedores.Items.Add(New ListItem(part.Textos(i + 63), i))
            Next
            _rblTipoProveedores.SelectedIndex = 0
            Controls.Add(_rblTipoProveedores)

            Dim sProveedorID As String
            _lstProveedores = New ListBox
            _lstProveedores.Rows = 5
            _lstProveedores.SelectionMode = ListSelectionMode.Multiple
            _lstProveedores.BorderStyle = WebControls.BorderStyle.Inset
            _lstProveedores.Width = Unit.Percentage(100)
            _lstProveedores.ID = Me.ID & "_lstProveedores"
            sProveedorID = Me.ClientID & "_" & Me.ID & "_lstProveedores"
            Controls.Add(_lstProveedores)

            Dim sCodProvesID As String
            _hidCodProves = New HiddenField
            _hidCodProves.ID = Me.ID & "_CodProves"
            sCodProvesID = Me.ClientID & "_" & Me.ID & "_CodProves"
            Controls.Add(_hidCodProves)

            Dim sDenProvesID As String
            _hidDenProves = New HiddenField
            _hidDenProves.ID = Me.ID & "_DenProves"
            sDenProvesID = Me.ClientID & "_" & Me.ID & "_DenProves"
            Controls.Add(_hidDenProves)

            _imgMas = New HtmlImage()
            _imgMas.Alt = part.Textos(67) '"Añadir proveedor a la selección"
            _imgMas.Src = Me.Page.ClientScript.GetWebResourceUrl(GetType(FSQAWebPartPanelCalidad), "Fullstep.PMWebControls.Mas.gif")
            _imgMas.Attributes.Add("onClick", "javascript:window.open('" & ConfigurationManager.AppSettings("RutaFS") & "_common/BuscadorProveedores.aspx?PM=false&desde=WebPart&IdControl=" & sProveedorID & "&codProves=" & sCodProvesID & "&denProves=" & sDenProvesID & "&IdControlTipoProveedor=" & sTipoProveedores & "', '_blank', 'width=835,height=635,status=yes,resizable=no,top=0,left=150,scrollbars=yes');")
            Controls.Add(_imgMas)

            Dim sMensaje As String = part.Textos(68) '"Seleccione el proveedor que desea eliminar"
            _imgMenos = New HtmlImage()
            _imgMenos.Alt = part.Textos(69) '"Eliminar proveedor de la selección"
            _imgMenos.Src = Me.Page.ClientScript.GetWebResourceUrl(GetType(FSQAWebPartPanelCalidad), "Fullstep.PMWebControls.Menos.gif")
            _imgMenos.Attributes.Add("onClick", "javascript:eliminarElementoLista('" & sProveedorID & "','" & sCodProvesID & "','" & sDenProvesID & "','" & sMensaje & "');")
            Controls.Add(_imgMenos)

            _chkBajaProveedor = New CheckBox
            _chkBajaProveedor.Text = part.Textos(70) '"Incluir proveedores dados de baja"
            Controls.Add(_chkBajaProveedor)
        End Sub

        ''' <summary>
        ''' Presenta el contenido del control en el sistema de escritura especificado. Este método lo usan principalmente los programadores de controles. 
        ''' </summary>
        ''' <param name="writer">HtmlTextWriter que representa la secuencia de salida para presentar contenido HTML en el cliente.</param>
        ''' <remarks>Llamada desde:sistema; Tiempo máximo:0</remarks>
        Protected Overrides Sub RenderContents(ByVal writer As System.Web.UI.HtmlTextWriter)
            Dim part As FSQAWebPartCertificados = CType(WebPartToEdit, FSQAWebPartCertificados)

            writer.Write("<b>" & part.Textos(13) & ":</b> ")
            _txtTitulo.RenderControl(writer)
            writer.WriteBreak()
            writer.Write("<b>" & part.Textos(15) & ":</b> ")
            _txtAncho.RenderControl(writer)
            _reqAncho.RenderControl(writer)
            _ranAncho.RenderControl(writer)
            writer.WriteBreak()
            writer.Write("<b>" & part.Textos(52) & ":</b> ") '"Nº de no certificados a mostrar"
            _wneNumCertificados.RenderControl(writer)
            _reqNumCertificados.RenderControl(writer)

            writer.WriteBreak()

            writer.Write("<div style='border: 1px solid #CCCCCC;height=35;vertical-align:middle'>")
            writer.Write("<table border=0><tr><td height=35>")

            writer.Write(" " & part.Textos(53)) 'Días para la <b> fecha de despublicación:   </b>
            _lstFechaLimiteCumplimentacion.RenderControl(writer)
            writer.WriteBreak()
            writer.Write("</td></tr></table>")
            writer.Write("</div>")

            writer.WriteBreak()
            writer.Write("<div style='border: 1px solid #CCCCCC;'><b>&nbsp;&nbsp;" & part.Textos(54) & "</b>") ' Fecha de actualización
            writer.WriteBreak()
            _chkActualizacion.RenderControl(writer)
            writer.Write("   " & part.Textos(55) & "   ") 'Mostrar solo las modificadas durante
            _lstFechaActualizacion.RenderControl(writer)
            writer.WriteBreak()
            writer.Write("</div>")


            writer.WriteBreak()
            writer.Write("<div style='border: 1px solid #CCCCCC;'><b>&nbsp;&nbsp;" & part.Textos(56) & " </b>") '" Filtrar por estado de los certificados "
            writer.WriteBreak()
            _chkTipoEstados1.RenderControl(writer)
            writer.Write("<hr><b>&nbsp;&nbsp;" & part.Textos(74) & " </b>")
            writer.WriteBreak()
            _chkTipoEstados2.RenderControl(writer)
            writer.Write("<hr><b>&nbsp;&nbsp;" & part.Textos(75) & " </b>")
            writer.WriteBreak()
            _chkTipoEstados3.RenderControl(writer)
            writer.WriteBreak()
            writer.Write("</div>")

            writer.WriteBreak()
            writer.Write("<div style='border: 1px solid #CCCCCC;'><b>&nbsp;&nbsp;" & part.Textos(57) & "</b>") 'Filtrar por tipo de Certificado 
            writer.WriteBreak()
            _chkTipoCertificados.RenderControl(writer)
            writer.WriteBreak()
            writer.Write("</div>")
            writer.WriteBreak()

            writer.Write("<div style='border: 1px solid #CCCCCC;'><b>&nbsp;&nbsp;" & part.Textos(31) & " </b>") 'Peticionario

            writer.WriteBreak()
            writer.Write("&nbsp;&nbsp;" & part.Textos(58) & "  ") 'Ver Certificados
            _lstPeticionario.RenderControl(writer)
            writer.WriteBreak()
            writer.Write("</div>")
            writer.WriteBreak()

            'Proveedor
            writer.Write("<div style='border: 1px solid #CCCCCC;'>")
            writer.Write("<table border=0 cellpadding=2 width=80%><tr><td colspan=2><b>" & part.Textos(71) & " </b></td></tr>") '"Proveedores"
            writer.Write("<tr><td colspan=2>")
            _hidCodProves.RenderControl(writer)
            _hidDenProves.RenderControl(writer)
            _rblTipoProveedores.RenderControl(writer)
            writer.Write("</td></tr>")
            writer.Write("<tr>")
            writer.Write("<td rowspan=2 width=85%>")
            _lstProveedores.RenderControl(writer)
            writer.Write("</td>")
            writer.Write("<td  valign='bottom'>")
            _imgMas.RenderControl(writer)
            writer.Write("</td>")
            writer.Write("</tr>")
            writer.Write("<tr>")
            writer.Write("<td valign='top'>")
            _imgMenos.RenderControl(writer)
            writer.Write("</td>")
            writer.Write("</tr>")
            writer.Write("</table>")
            _chkBajaProveedor.RenderControl(writer)
            writer.Write("</div>")
            writer.WriteBreak()
        End Sub

        ''' <summary>
        ''' Guarda los valores de un control EditorPart en las propiedades correspondientes del control WebPart asociado.
        ''' </summary>
        ''' <returns>Es true si la acción de guardar los valores del control EditorPart en el control WebPart se realiza correctamente; en caso contrario (si se produce un error), es false.</returns>
        Public Overrides Function ApplyChanges() As Boolean
            Dim iNumCertificadosAMostrar As Integer
            Dim part As FSQAWebPartCertificados = CType(WebPartToEdit, FSQAWebPartCertificados)
            Dim bMostrarError As Boolean = False
            _reqAncho.Validate()
            _ranAncho.Validate()
            _reqNumCertificados.Validate()
            If wneNumCertificados.Text <> "" Then
                iNumCertificadosAMostrar = CType(wneNumCertificados.Value, Integer)
            Else
                iNumCertificadosAMostrar = -1

            End If
            If _reqAncho.IsValid() And _ranAncho.IsValid() And _reqNumCertificados.IsValid Then
                part.Title = _txtTitulo.Text
                part.Width = Unit.Pixel(CType(_txtAncho.Text, Integer))
                part.mNumLineas = iNumCertificadosAMostrar
                Dim tipoEstados As String = ""
                For Each x As ListItem In _chkTipoEstados1.Items
                    If x.Selected Then
                        tipoEstados = tipoEstados & "#" & CType(x.Value, Integer) & "#,"
                    End If
                Next
                For Each x As ListItem In _chkTipoEstados2.Items
                    If x.Selected Then
                        tipoEstados = tipoEstados & "#" & CType(x.Value, Integer) & "#,"
                    End If
                Next
                For Each x As ListItem In _chkTipoEstados3.Items
                    If x.Selected Then
                        tipoEstados = tipoEstados & "#" & CType(x.Value, Integer) & "#,"
                    End If
                Next
                If Len(tipoEstados) > 0 Then
                    tipoEstados = Left(tipoEstados, Len(tipoEstados) - 1)
                End If
                part.TipoEstados = tipoEstados

                Dim tipoCertificados As String = ""
                For Each x As ListItem In _chkTipoCertificados.Items
                    If x.Selected Then
                        tipoCertificados = tipoCertificados & CType(x.Value, Integer) & ","
                    End If
                Next
                If Len(tipoCertificados) > 0 Then
                    tipoCertificados = Left(tipoCertificados, Len(tipoCertificados) - 1)
                End If
                part.TipoCertificados = tipoCertificados


                part.indiceFechaActualizacion = _lstFechaActualizacion.SelectedIndex
                part.optVerFechaActu = (_chkActualizacion.Checked = True)
                part.IndiceFechaLimiteCumplimentacion = _lstFechaLimiteCumplimentacion.SelectedIndex
                part.TipoPeticionario = _lstPeticionario.SelectedIndex

                'Proveedores
                part.indiceTipoProveedor = TipoProveedor
                part.CodProves = ListaCodigoProveedores
                part.DenProves = ListaNombreProveedores
                part.MostrarProveedoresBaja = _chkBajaProveedor.Checked

                If ((tipoEstados = "") And (tipoCertificados = "") And (_chkActualizacion.Checked = False) And (part.IndiceFechaLimiteCumplimentacion = 0)) Then
                    part.MonstrarMensaje = True
                Else
                    part.MonstrarMensaje = False
                End If

                part.CargarCertificados(False)
                Return True
            Else
                Return False
            End If
        End Function

        ''' <summary>
        ''' Obtiene los valores de propiedad de un control WebPart correspondientes a su control EditorPart asociado.
        ''' </summary>
        Public Overrides Sub SyncChanges()
            Dim part As FSQAWebPartCertificados = CType(WebPartToEdit, FSQAWebPartCertificados)
            txtTitulo.Text = part.Title
            txtAncho.Text = part.Width.Value.ToString()
            wneNumCertificados.Text = IIf(part.mNumLineas = -1, "", part.NumLineas)
            For Each x As ListItem In chkEstados1.Items
                x.Selected = part.buscarElemento(part.mTipoEstados, "#" & x.Value & "#")
            Next

            For Each x As ListItem In chkEstados2.Items
                x.Selected = part.buscarElemento(part.mTipoEstados, "#" & x.Value & "#")
            Next

            For Each x As ListItem In chkEstados3.Items
                x.Selected = part.buscarElemento(part.mTipoEstados, "#" & x.Value & "#")
            Next

            For Each x As ListItem In chkTipoCertificados.Items
                x.Selected = part.buscarElemento(part.mTipoCertificados, x.Value)

            Next
            Me.lstFechaLimiteCumplimentacion.SelectedIndex = part.IndiceFechaLimiteCumplimentacion
            Me.optVerFechaAct.Checked = part.optVerFechaActu
            Me.lstFechaActualizacion.SelectedIndex = part.indiceFechaActualizacion
            Me.lstPeticionario.SelectedIndex = part.TipoPeticionario

            'Tipo Proveedores
            _rblTipoProveedores.SelectedIndex = part.indiceTipoProveedor
            '(cambiar los hidden por una listbox)?????? ****** !!!!!!!
            _hidCodProves.Value = part.CodProves
            _hidDenProves.Value = part.DenProves

            Dim aux() As String
            Dim aux2() As String
            aux = Split(part.CodProves, "#")
            aux2 = Split(part.DenProves, "#")
            lstProveedores.Items.Clear()
            For i = 0 To UBound(aux)
                If aux(i) <> "" Then lstProveedores.Items.Add(New ListItem(aux2(i), aux(i)))
            Next
            _chkBajaProveedor.Checked = part.MostrarProveedoresBaja

        End Sub


        '' *****************************
        '' A continuacion se establecen las propiedades del webpart
        Private ReadOnly Property txtTitulo() As TextBox
            Get
                EnsureChildControls()
                Return _txtTitulo
            End Get
        End Property

        Private ReadOnly Property txtAncho() As TextBox
            Get
                EnsureChildControls()
                Return _txtAncho
            End Get
        End Property


        Private ReadOnly Property wneNumCertificados() As Infragistics.Web.UI.EditorControls.WebNumericEditor
            Get
                EnsureChildControls()
                Return _wneNumCertificados
            End Get
        End Property

        ''' <summary>
        ''' Dar acceso a los checks "Filtrar por estado de los certificados"
        ''' </summary>
        ''' <remarks>Llamada desde: SyncChanges ; Tiempo máximo: 0</remarks>
        Private ReadOnly Property chkEstados1() As CheckBoxList
            Get
                EnsureChildControls()
                Return _chkTipoEstados1
            End Get
        End Property

        ''' <summary>
        ''' Dar acceso a los checks "Publicado en portal" 
        ''' </summary>
        ''' <remarks>Llamada desde: SyncChanges ; Tiempo máximo: 0</remarks>
        Private ReadOnly Property chkEstados2() As CheckBoxList
            Get
                EnsureChildControls()
                Return _chkTipoEstados2
            End Get
        End Property

        ''' <summary>
        ''' Dar acceso a los checks "Modo de solicitud" 
        ''' </summary>
        ''' <remarks>Llamada desde: SyncChanges ; Tiempo máximo: 0</remarks>
        Private ReadOnly Property chkEstados3() As CheckBoxList
            Get
                EnsureChildControls()
                Return _chkTipoEstados3
            End Get
        End Property


        Private ReadOnly Property chkTipoCertificados() As CheckBoxList
            Get
                EnsureChildControls()
                Return _chkTipoCertificados
            End Get
        End Property

        Private ReadOnly Property lstFechaLimiteCumplimentacion() As ListBox
            Get
                EnsureChildControls()
                Return _lstFechaLimiteCumplimentacion
            End Get
        End Property

        Private ReadOnly Property optVerFechaAct() As CheckBox
            Get
                EnsureChildControls()
                Return _chkActualizacion
            End Get
        End Property

        Private ReadOnly Property lstFechaActualizacion() As ListBox
            Get
                EnsureChildControls()
                Return _lstFechaActualizacion
            End Get
        End Property

        Private ReadOnly Property lstPeticionario() As ListBox
            Get
                EnsureChildControls()
                Return _lstPeticionario
            End Get
        End Property

        Private ReadOnly Property TipoProveedor() As Integer
            Get
                EnsureChildControls()
                Return _rblTipoProveedores.SelectedIndex
            End Get
        End Property

        Private ReadOnly Property lstProveedores() As ListBox
            Get
                EnsureChildControls()
                Return _lstProveedores
            End Get
        End Property

        Private ReadOnly Property ListaCodigoProveedores() As String
            Get
                EnsureChildControls()
                Return _hidCodProves.Value
            End Get
        End Property

        Private ReadOnly Property ListaNombreProveedores() As String
            Get
                EnsureChildControls()
                Return _hidDenProves.Value
            End Get
        End Property

    End Class

    <Personalizable(), WebBrowsable(), DefaultValue("")> _
    Public Property TipoEstados() As String
        Get
            Return mTipoEstados
        End Get
        Set(ByVal value As String)
            mTipoEstados = value
        End Set
    End Property
    <Personalizable(), WebBrowsable(), DefaultValue("")> _
    Public Property TipoCertificados() As String
        Get
            Return mTipoCertificados
        End Get
        Set(ByVal value As String)
            mTipoCertificados = value
        End Set
    End Property

    <Personalizable(), WebBrowsable(), DefaultValue(5)> _
    Public Property NumLineas() As Integer
        Get
            Return mNumLineas
        End Get
        Set(ByVal value As Integer)
            mNumLineas = value
        End Set
    End Property

    <Personalizable(), WebBrowsable(), DefaultValue(0)> _
    Public Property IndiceFechaLimiteCumplimentacion() As Integer
        Get
            Return mFechaLimiteCumplimentacion
        End Get
        Set(ByVal value As Integer)
            mFechaLimiteCumplimentacion = value
        End Set
    End Property

    <Personalizable(), WebBrowsable(), DefaultValue(0)> _
    Public Property optVerFechaActu() As Boolean
        Get
            Return mVerFechaActu
        End Get
        Set(ByVal value As Boolean)
            mVerFechaActu = value
        End Set
    End Property

    <Personalizable(), WebBrowsable(), DefaultValue(0)> _
    Public Property indiceFechaActualizacion() As Integer
        Get
            Return mFechaActualizacion
        End Get
        Set(ByVal value As Integer)
            mFechaActualizacion = value
        End Set
    End Property

    <Personalizable(), WebBrowsable(), DefaultValue(0)> _
    Public Property TipoPeticionario() As Integer
        Get
            Return mTipoPeticionario
        End Get
        Set(ByVal value As Integer)
            mTipoPeticionario = value
        End Set
    End Property

    <Personalizable(), WebBrowsable(), DefaultValue(1)> _
    Public Property MonstrarMensaje() As Boolean
        Get
            Return mbMostrarMensaje
        End Get
        Set(ByVal value As Boolean)
            mbMostrarMensaje = value
        End Set
    End Property

    <Personalizable(), WebBrowsable(), DefaultValue(1)> _
    Public Property indiceTipoProveedor() As Integer
        Get
            Return miIndiceTipoProveedor
        End Get
        Set(ByVal value As Integer)
            miIndiceTipoProveedor = value
        End Set
    End Property

    <Personalizable(), WebBrowsable(), DefaultValue("")> _
    Public Property CodProves() As String
        Get
            Return msCodProves
        End Get
        Set(ByVal value As String)
            msCodProves = value
        End Set
    End Property

    <Personalizable(), WebBrowsable(), DefaultValue("")> _
    Public Property DenProves() As String
        Get
            Return msDenProves
        End Get
        Set(ByVal value As String)
            msDenProves = value
        End Set
    End Property

    <Personalizable(), WebBrowsable(), DefaultValue(0)> _
    Public Property MostrarProveedoresBaja() As Boolean
        Get
            Return mbMostrarProveedoresBaja
        End Get
        Set(ByVal value As Boolean)
            mbMostrarProveedoresBaja = value
        End Set
    End Property

    Public Property ImagenCertificado() As String
        Get
            Dim s As String = CStr(ViewState("ImagenCertificado"))
            If s Is Nothing Then s = String.Empty
            Return s
        End Get
        Set(ByVal value As String)
            ViewState("ImagenCertificado") = value
        End Set
    End Property

    ''' <summary>
    ''' Devuelve el idioma de la página que alberga el control.
    ''' </summary>
    ''' <returns>El código de idioma.</returns>
    Private Function Idioma() As String
        If Not DesignMode Then
            If Me.Page Is Nothing AndAlso TypeOf Me.Page Is FSNServer.IFSNPage Then
                Return CType(Me.Page, FSNServer.IFSNPage).Idioma
            Else
                For Each x As String In HttpContext.Current.Session.Keys
                    If TypeOf HttpContext.Current.Session(x) Is FSNServer.User Then
                        Return CType(HttpContext.Current.Session(x), FSNServer.User).Idioma
                    End If
                Next
                Return String.Empty
            End If
        Else
            Return String.Empty
        End If
    End Function

    ''' <summary>
    ''' Carga los idiomas de la pagina
    ''' </summary>
    ''' <remarks>Llamada:Desde la propia pagina; Tiempo Ejecucion=0seg.</remarks>
    Public ReadOnly Property Textos(ByVal Index As Short) As String
        Get
            If String.IsNullOrEmpty(_Textos(Index)) Or Idioma() <> _sIdioma Then
                If Not DesignMode Then
                    Dim FSNDict As FSNServer.Dictionary = New FSNServer.Dictionary()
                    _sIdioma = Idioma()
                    FSNDict.LoadData(FSNLibrary.TiposDeDatos.ModulosIdiomas.Webparts, _sIdioma)
                    For i As Short = 0 To 2
                        _Textos(i) = FSNDict.Data.Tables(0).Rows(i + 5).Item(1)
                    Next
                    For i As Short = 3 To 16
                        _Textos(i) = FSNDict.Data.Tables(0).Rows(i + 6).Item(1)
                    Next
                    For i As Short = 17 To 59
                        _Textos(i) = FSNDict.Data.Tables(0).Rows(i + 82).Item(1)
                    Next
                    _Textos(60) = FSNDict.Data.Tables(0).Rows(159).Item(1) 'Edite el webpart para poder configurar su contenido.
                    _Textos(61) = FSNDict.Data.Tables(0).Rows(235).Item(1) 'El nº de certificados a mostrar debe de ser númerico.
                    _Textos(62) = FSNDict.Data.Tables(0).Rows(238).Item(1) 'Ver anteriores.
                    For i As Short = 63 To 66
                        _Textos(i) = FSNDict.Data.Tables(0).Rows(i + 116).Item(1)
                    Next
                    _Textos(67) = FSNDict.Data.Tables(0).Rows(183).Item(1)
                    _Textos(68) = FSNDict.Data.Tables(0).Rows(184).Item(1)
                    _Textos(69) = FSNDict.Data.Tables(0).Rows(186).Item(1)
                    _Textos(70) = FSNDict.Data.Tables(0).Rows(185).Item(1)
                    _Textos(71) = FSNDict.Data.Tables(0).Rows(212).Item(1)

                    _Textos(72) = FSNDict.Data.Tables(0).Rows(239).Item(1)

                    _Textos(73) = FSNDict.Data.Tables(0).Rows(249).Item(1)
                    _Textos(74) = FSNDict.Data.Tables(0).Rows(250).Item(1)
                    _Textos(75) = FSNDict.Data.Tables(0).Rows(251).Item(1)
                    _Textos(76) = FSNDict.Data.Tables(0).Rows(252).Item(1)
                    _Textos(77) = FSNDict.Data.Tables(0).Rows(253).Item(1)

                    Return _Textos(Index)
                Else
                    'Si falla la carga de textos devolvemos la cadena vacia para que la carga de la página siga su curso
                    Return String.Empty
                End If
            Else
                Return _Textos(Index)
            End If
        End Get
    End Property

    Public Overrides Property Title() As String
        Get
            If String.IsNullOrEmpty(MyBase.Title) Then
                If String.IsNullOrEmpty(sTitulo) Then
                    If Me.Page Is Nothing And String.IsNullOrEmpty(Textos(17)) Then
                        MyBase.Title = Textos(17)
                        sTitulo = MyBase.Title
                    Else
                        sTitulo = Textos(17)
                    End If
                End If
                Return sTitulo
            Else
                sTitulo = MyBase.Title
                Return MyBase.Title
            End If
        End Get
        Set(ByVal value As String)
            MyBase.Title = value
        End Set
    End Property

    ''' <summary>
    ''' Carga los datos por defecto para el control
    ''' </summary>
    Public Sub New()
        Me.Width = Unit.Pixel(300)
        NumLineas = 5
        mTipoEstados = ""

        Me.indiceTipoProveedor = 1
        Me.CodProves = ""

        MonstrarMensaje = True
    End Sub

    ''' <summary>
    ''' Lo llama el marco de trabajo de las páginas ASP.NET para indicar a los controles de servidor que utilizan la implementación basada en la composición que creen los controles secundarios que contengan como forma de preparar la devolución o representación de los datos.
    ''' Muestra los datos del webPart.
    ''' </summary>
    ''' <remarks>Llamada: Cuando se crea un objeto; Tiempo Ejecucion:=0seg.</remarks>
    Protected Overrides Sub CreateChildControls()
        Controls.Clear()
        Dim img As HtmlImage = New HtmlImage()
        img.Src = Me.Page.ClientScript.GetWebResourceUrl(GetType(FSQAWebPartCertificados), "Fullstep.PMWebControls.trans.gif")
        img.Width = 300
        img.Height = 1
        Me.Controls.Add(img)
        Dim tabla As HtmlTable = New HtmlTable()
        tabla.Width = "100%"
        tabla.Height = "55px"
        Dim fila As HtmlTableRow = New HtmlTableRow()
        fila.Height = "55px"
        Dim celda As HtmlTableCell = New HtmlTableCell()
        celda.Width = "90%"
        lblTitulo = New Label()
        lblTitulo.CssClass = "Rotulo"

        lblMensaje = New Label
        lblMensaje.CssClass = "Rotulo"


        celda.Controls.Add(lblTitulo)
        celda.Controls.Add(lblMensaje)

        celda.Controls.Add(New LiteralControl("<br/>"))
        lblNumTotal = New Label()
        lblNumTotal.CssClass = "EtiquetaGrande"
        celda.Controls.Add(lblNumTotal)
        lblTotal = New Label()
        lblTotal.CssClass = "EtiquetaGrande"
        celda.Controls.Add(lblTotal)
        fila.Cells.Add(celda)
        celda = New HtmlTableCell
        celda.Align = "right"
        celda.VAlign = "bottom"
        img.Width = 125
        celda.Controls.Add(img)

        If DesignMode Then
            celda.Style.Add("background-image", "images/Certificados.gif")
        Else
            If CStr(ViewState("ImagenCertificados")) <> String.Empty Then
                celda.Style.Add("background-image", Replace(CStr(ViewState("ImagenCertificados")), "~/", HttpContext.Current.Request.ApplicationPath & "/"))
            Else
                celda.Style.Add("background-image", Me.Page.ClientScript.GetWebResourceUrl(GetType(FSQAWebPartCertificados), "Fullstep.PMWebControls.Certificados.gif"))
            End If
        End If
        celda.Style.Add("background-position", "top right")
        celda.Style.Add("background-repeat", "no-repeat")
        fila.Cells.Add(celda)
        tabla.Rows.Add(fila)

        fila = New HtmlTableRow
        celda = New HtmlTableCell
        celda.ColSpan = 2
        lstSolicitudes = New DataList()
        lstSolicitudes.Width = Unit.Percentage(100)
        lstSolicitudes.ItemTemplate = New DataListTemplate()
        celda.Controls.Add(lstSolicitudes)
        fila.Cells.Add(celda)
        tabla.Rows.Add(fila)

        fila = New HtmlTableRow
        celda = New HtmlTableCell
        celda.ColSpan = 2
        btnVerSiguientes = New FSNWebControls.FSNButton()
        btnVerSiguientes.ID = "btnVerSiguientes"
        celda.Controls.Add(btnVerSiguientes)
        btnVerAnteriores = New FSNWebControls.FSNButton()
        btnVerSiguientes.ID = "btnVerAnteriores"
        celda.Controls.Add(btnVerAnteriores)
        fila.Cells.Add(celda)
        tabla.Rows.Add(fila)

        lblError = New System.Web.UI.WebControls.Label
        lblError.Visible = False
        celda.Controls.Add(lblError)
        fila.Cells.Add(celda)
        tabla.Rows.Add(fila)

        Me.Controls.Add(tabla)

        lblTitulo.Text = Me.Title 'Titulo dentro del webPart
        lblTotal.Text = " " & Textos(17)  ' " " & Certificados
        btnVerSiguientes.Text = Textos(18) ' Ver siguientes
        btnVerAnteriores.Text = Textos(62) ' Ver anteriores

        If Not Page.IsPostBack Then _
            CargarCertificados(False)
    End Sub

    ''' <summary>
    ''' Refresca los datos del webpart
    ''' </summary>
    Public Sub Actualizar()
        CreateChildControls()
        CargarCertificados(False)
    End Sub

    ''' <summary>
    ''' Envía el contenido del control de servidor a un objeto HtmlTextWriter que se proporciona y almacena la información de seguimiento sobre el control si dicho seguimiento está habilitado.
    ''' </summary>
    ''' <param name="writer">Objeto HTmlTextWriter que recibe el contenido del control.</param>
    Public Overrides Sub RenderControl(ByVal writer As System.Web.UI.HtmlTextWriter)
        MyBase.RenderControl(writer)
    End Sub

    ''' <summary>
    ''' Carga en la lista del webPart los datos de los certificados
    ''' </summary>   
    ''' <param name="bVerSiguientes">Si carga o no en el webPart con las siguientes noConformidades</param>
    ''' <remarks>Llamada desde=btnVerSiguientes_Click,ApplyChanges,CreateChildControls; Tiempo máximo=1,3seg.</remarks>
    Private Sub CargarCertificados(ByVal bVerSiguientes As Boolean)
        If Not Me.Page Is Nothing AndAlso TypeOf Me.Page Is FSNServer.IFSNPage Then
            Dim lista As New List(Of mOrden)
            Dim pag As FSNServer.IFSNPage = CType(Me.Page, FSNServer.IFSNPage)
            Dim FSWSServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim oCertificados As FSNServer.Certificados
            Dim i As Integer
            Dim numTotal As Integer
            Dim sPeticionario As String
            Dim FechaLimiteCumplimentacion As Date = Now
            Dim dFechaActualizacion As Date = Now
            Dim iSobrepasada As Integer
            Dateformat = pag.Usuario.DateFormat

            If optVerFechaActu = True Then
                Select Case indiceFechaActualizacion
                    Case FechaActualizacionValores.UltimoDia
                        dFechaActualizacion = DateAdd(DateInterval.Day, -1, Now)
                    Case FechaActualizacionValores.Dias2
                        dFechaActualizacion = DateAdd(DateInterval.Day, -2, Now)
                    Case FechaActualizacionValores.Dias3
                        dFechaActualizacion = DateAdd(DateInterval.Day, -3, Now)
                    Case FechaActualizacionValores.Dias4
                        dFechaActualizacion = DateAdd(DateInterval.Day, -4, Now)
                    Case FechaActualizacionValores.Dias5
                        dFechaActualizacion = DateAdd(DateInterval.Day, -5, Now)
                    Case FechaActualizacionValores.Dias6
                        dFechaActualizacion = DateAdd(DateInterval.Day, -6, Now)
                    Case FechaActualizacionValores.Semana1
                        dFechaActualizacion = DateAdd(DateInterval.Day, -7, Now)
                    Case FechaActualizacionValores.Semana2
                        dFechaActualizacion = DateAdd(DateInterval.Day, -14, Now)
                    Case FechaActualizacionValores.Semana3
                        dFechaActualizacion = DateAdd(DateInterval.Day, -21, Now)
                    Case FechaActualizacionValores.Mes1
                        dFechaActualizacion = DateAdd(DateInterval.Month, -1, Now)
                End Select

            End If

            If MonstrarMensaje = True Then
                lblTitulo.Visible = False
                lblTotal.Visible = False
                lblNumTotal.Visible = False
                btnVerSiguientes.Visible = False
                btnVerAnteriores.Visible = False
                lblMensaje.Text = Textos(60)
                lblMensaje.Visible = True
                lstSolicitudes.Visible = False
            Else
                lblTitulo.Visible = True
                lblTotal.Visible = True
                lblNumTotal.Visible = True
                lblMensaje.Visible = False

                oCertificados = FSWSServer.Get_Object(GetType(FSNServer.Certificados))

                Dim bPorUsuario As Boolean
                sPeticionario = pag.Usuario.Cod
                If TipoPeticionarioValores.Todos = TipoPeticionario Then
                    bPorUsuario = False
                Else
                    bPorUsuario = True
                End If
                iSobrepasada = 2
                Select Case IndiceFechaLimiteCumplimentacion
                    Case FechaLimiteCumplimentacionValores.Vacio
                        iSobrepasada = 0
                    Case FechaLimiteCumplimentacionValores.Sobrepasada
                        iSobrepasada = 1
                    Case FechaLimiteCumplimentacionValores.Dias1
                        FechaLimiteCumplimentacion = DateAdd(DateInterval.Day, 1, Now)
                    Case FechaLimiteCumplimentacionValores.Dias2
                        FechaLimiteCumplimentacion = DateAdd(DateInterval.Day, 2, Now)
                    Case FechaLimiteCumplimentacionValores.Dias3
                        FechaLimiteCumplimentacion = DateAdd(DateInterval.Day, 3, Now)
                    Case FechaLimiteCumplimentacionValores.Dias4
                        FechaLimiteCumplimentacion = DateAdd(DateInterval.Day, 4, Now)
                    Case FechaLimiteCumplimentacionValores.Dias5
                        FechaLimiteCumplimentacion = DateAdd(DateInterval.Day, 5, Now)
                    Case FechaLimiteCumplimentacionValores.Dias6
                        FechaLimiteCumplimentacion = DateAdd(DateInterval.Day, 6, Now)
                    Case FechaLimiteCumplimentacionValores.Semana1
                        FechaLimiteCumplimentacion = DateAdd(DateInterval.Day, 7, Now)
                    Case FechaLimiteCumplimentacionValores.Semana2
                        FechaLimiteCumplimentacion = DateAdd(DateInterval.Day, 14, Now)
                    Case FechaLimiteCumplimentacionValores.Semana3
                        FechaLimiteCumplimentacion = DateAdd(DateInterval.Day, 21, Now)
                    Case FechaLimiteCumplimentacionValores.Mes1
                        FechaLimiteCumplimentacion = DateAdd(DateInterval.Month, 1, Now)
                End Select

                Dim bEstado1, bEstado2, bEstado3, bEstado4, bEstado5, bEstado6, bEstado7, bEstado8, bEstado9, bEstado10, bEstado11, bEstado12, bEstado13 As Boolean
                bEstado1 = buscarElemento(mTipoEstados, "#1#")
                bEstado2 = buscarElemento(mTipoEstados, "#2#")
                bEstado3 = buscarElemento(mTipoEstados, "#3#")
                bEstado4 = buscarElemento(mTipoEstados, "#4#")
                bEstado5 = buscarElemento(mTipoEstados, "#5#")
                bEstado6 = buscarElemento(mTipoEstados, "#6#")
                bEstado7 = buscarElemento(mTipoEstados, "#7#")
                bEstado8 = buscarElemento(mTipoEstados, "#8#")
                bEstado9 = buscarElemento(mTipoEstados, "#9#")
                bEstado10 = buscarElemento(mTipoEstados, "#10#")
                bEstado11 = buscarElemento(mTipoEstados, "#11#")
                bEstado12 = buscarElemento(mTipoEstados, "#12#")
                bEstado13 = buscarElemento(mTipoEstados, "#13#")

                Try
                    oCertificados.LoadDataWebPartCertificados(iSobrepasada, FechaLimiteCumplimentacion, _
                        optVerFechaActu, dFechaActualizacion, bEstado1, bEstado2, bEstado3, bEstado4, bEstado5, bEstado6, _
                        bEstado7, bEstado8, bEstado9, bEstado10, TipoCertificados, bPorUsuario, sPeticionario, _
                        pag.Idioma, indiceTipoProveedor, CodProves, MostrarProveedoresBaja, _
                        pag.Usuario.QARestCertifUsu, pag.Usuario.QARestCertifUO, pag.Usuario.QARestCertifDep, _
                        pag.Usuario.QARestProvMaterial, pag.Usuario.QARestProvEquipo, pag.Usuario.QARestProvContacto, bEstado11, bEstado12, bEstado13)
                    i = 0
                    If oCertificados.Data.Tables.Count > 0 Then
                        numTotal = oCertificados.Data.Tables(0).Rows.Count
                        lstSolicitudes.Visible = True
                        For Each fila As DataRow In oCertificados.Data.Tables(0).Rows
                            Dim mo As New mOrden()
                            i = i + 1
                            If ((bVerSiguientes And i > NumLineas) Or (Not bVerSiguientes)) Then
                                If Not IsDBNull(fila.Item("CERTIFICADO")) Then
                                    mo.Instancia = fila.Item("INSTANCIA")
                                    mo.Certificado = fila.Item("CERTIFICADO")
                                End If

                                mo.TipoCertificado = fila.Item("TIPOCERTIFICADO")
                                mo.CodProveedor = fila.Item("PROVE")
                                mo.Proveedor = fila.Item("PROVEEDOR")

                                mo.EstadoSinRespuestaProveedor = fila.Item("ESTADO_WEBPART1")
                                mo.EstadoConDatosGuardadosProveedor = fila.Item("ESTADO_WEBPART2")
                                mo.EstadoConCertificado = fila.Item("ESTADO_WEBPART3")
                                mo.EstadoExpirado = fila.Item("ESTADO_WEBPART4")
                                mo.EstadoProximoAExpirar = fila.Item("ESTADO_WEBPART5")
                                mo.EstadoPublicado = fila.Item("ESTADO_WEBPART6")
                                mo.EstadoDesPublicado = fila.Item("ESTADO_WEBPART7")
                                mo.EstadoSolicitado = fila.Item("ESTADO_WEBPART8")
                                mo.EstadoRenovado = fila.Item("ESTADO_WEBPART9")
                                mo.EstadoEnviado = fila.Item("ESTADO_WEBPART10")
                                mo.EstadoSinSolicitar = fila.Item("ESTADO_WEBPART11")
                                mo.EstadoPendienteValidar = fila.Item("ESTADO_WEBPART12")
                                mo.EstadoNoValido = fila.Item("ESTADO_WEBPART13")

                                If Not IsDBNull(fila.Item("FEC_EXPIRACION")) Then
                                    mo.FechaExpiracion = fila.Item("FEC_EXPIRACION")
                                End If
                                If Not IsDBNull(fila.Item("FEC_LIM_CUMPLIM")) Then
                                    mo.FechaLimiteCumplimentacion = fila.Item("FEC_LIM_CUMPLIM")
                                End If

                                If Not IsDBNull(fila.Item("FEC_ACT")) Then
                                    mo.FechaUltAct = fila.Item("FEC_ACT")
                                End If

                                If Not IsDBNull(fila.Item("PET")) Then
                                    If pag.Usuario.CodPersona <> fila.Item("PET") Then
                                        mo.CodPeticionario = fila.Item("PET")
                                        mo.Peticionario = fila.Item("PETICIONARIO")
                                    Else
                                        mo.Peticionario = ""
                                    End If
                                Else
                                    mo.Peticionario = ""
                                End If

                                mo.FechaLimiteCumplimentacionVisible = True

                                lista.Add(mo)
                                If Not bVerSiguientes Then
                                    If i = NumLineas Then
                                        Exit For
                                    End If
                                End If

                            End If
                        Next
                    End If

                    lstSolicitudes.DataSource = lista
                    lstSolicitudes.DataBind()
                    lblNumTotal.Text = numTotal.ToString
                Catch ex As Exception
                    lblError.Text = Textos(72)
                    lblError.Visible = True

                    lblTotal.Visible = False
                End Try

                Dim tabla As HtmlTable = New HtmlTable()

                If Not bVerSiguientes Then
                    If (numTotal > NumLineas) And (NumLineas > 0) Then
                        btnVerSiguientes.Visible = True
                        btnVerAnteriores.Visible = False
                    Else
                        btnVerSiguientes.Visible = False
                        btnVerAnteriores.Visible = IIf(numTotal > NumLineas, True, False)
                    End If
                Else
                    tabla = Me.Controls.Item(0)
                    tabla.Width = "100%"
                    Me.Height = Unit.Percentage(100)
                End If
            End If
        End If
    End Sub

    ''' <summary>
    ''' Devuelve una colección de controles EditorPart personalizados que pueden utilizarse para editar un control WebPart cuando se encuentra en modo de edición.
    ''' </summary>
    ''' <returns>Una colección EditorPartCollection que contiene controles EditorPart personalizados asociados a un control WebPart.</returns>
    Public Overrides Function CreateEditorParts() As System.Web.UI.WebControls.WebParts.EditorPartCollection
        Dim editorArray As New ArrayList()
        Dim edPart As New ControlEditor()
        edPart.ID = Me.ID & "_editor1"
        editorArray.Add(edPart)
        Dim editorParts As New EditorPartCollection(editorArray)
        Return editorParts
    End Function

    Public Overrides ReadOnly Property WebBrowsableObject() As Object
        Get
            Return Me
        End Get
    End Property

    ''' <summary>
    ''' Registra el control como un control cuyo estado se debe conservar.
    ''' </summary>
    ''' <param name="e">Objeto EventArgs que contiene los datos del evento.</param>
    Protected Overrides Sub OnInit(ByVal e As System.EventArgs)
        MyBase.OnInit(e)
        Page.RegisterRequiresControlState(Me)
    End Sub

    ''' <summary>
    ''' Guarda los cambios de estado del control.
    ''' </summary>
    ''' <returns>Devuelve el estado actual del control de servidor.</returns>
    Protected Overrides Function SaveControlState() As Object
        Dim obj As Object = MyBase.SaveControlState()
        If obj IsNot Nothing Then
            obj = New Pair(obj, _Textos)
        Else
            obj = _Textos
        End If
        obj = New Pair(obj, _sIdioma)
        Return obj
    End Function

    ''' <summary>
    ''' Restaura información de estado del control.
    ''' </summary>
    ''' <param name="state">Object que representa el estado del control que se va a restaurar.</param>
    Protected Overrides Sub LoadControlState(ByVal state As Object)
        If (state IsNot Nothing) Then
            Dim p As Pair = TryCast(state, Pair)
            _sIdioma = p.Second
            state = p.First
            p = TryCast(state, Pair)
            If p IsNot Nothing Then
                MyBase.LoadControlState(p.First)
                _Textos = CType(p.Second, String())
            Else
                If (TypeOf (state) Is String()) Then
                    _Textos = CType(state, String())
                Else
                    MyBase.LoadControlState(state)
                End If
            End If
        End If
    End Sub

    ''' <summary>
    ''' Nos indica si un nodo esta dentro de una lista o no
    ''' </summary>
    ''' <param name="listaNodos">Cadena de nodos seleccionada</param>
    ''' <param name="nodo">Valor del nodo a buscar en la lista</param>        
    ''' <returns>True o False si a enontrado o no el nodo</returns>
    ''' <remarks>Llamada desde=SyncChanges//CargarCertificados; Tiempo máximo=0,2seg.</remarks>
    Private Function buscarElemento(ByVal listaNodos As String, ByVal nodo As String) As Boolean
        Dim pos As Integer
        Dim elementosSeleccionados() As String
        Dim bEncontrado As Boolean = False
        pos = InStr(listaNodos, nodo)
        If pos > 0 Then
            If Len(listaNodos) = Len(nodo) Then 'El nodo coincide con la seleccion
                Return True
            Else
                If pos - 1 > 0 Then
                    bEncontrado = False
                    elementosSeleccionados = Split(listaNodos, ",")
                    For i = 0 To UBound(elementosSeleccionados)
                        If elementosSeleccionados(i) = nodo Then
                            bEncontrado = True
                            Exit For
                        End If
                    Next
                    Return bEncontrado
                Else
                    Return True

                End If

            End If
        Else
            Return False
        End If
    End Function

    ''' <summary>
    ''' Muestra todas los Certificados en el webPart
    ''' </summary>
    ''' <param name="sender">las del evento onClick</param>
    ''' <param name="e">las del evento onClick</param>        
    ''' <remarks>Llamada desde; Tiempo máximo=1,3seg.</remarks>
    Private Sub btnVerSiguientes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnVerSiguientes.Click
        Dim part As New FSQAWebPartCertificados
        part = WebBrowsableObject

        Me.CargarCertificados(True)

        btnVerSiguientes.Visible = False
        btnVerAnteriores.Visible = True
    End Sub

    ''' <summary>
    ''' Muestra de nuevo los primeros certificados cargados
    ''' </summary>
    ''' <param name="sender">las del evento onClick</param>
    ''' <param name="e">las del evento onClick</param>        
    ''' <remarks>Llamada desde; Tiempo máximo=1,3seg.</remarks>
    Private Sub btnVerAnteriores_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnVerAnteriores.Click
        Dim part As New FSQAWebPartCertificados
        part = WebBrowsableObject

        Me.CargarCertificados(False)

        btnVerSiguientes.Visible = True
        btnVerAnteriores.Visible = False
    End Sub

    ''' <summary>
    ''' En el webpart se usan funciones javascript q estan en FSQAWebPartPanelCalidad.js, esto es el include.
    ''' </summary>
    ''' <param name="sender">las del evento </param>
    ''' <param name="e">las del evento </param>   
    ''' <remarks>Llamada desde: sistema; Tiempo máximo=0seg.</remarks>
    Private Sub FSQAWebPartCertificados_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.DesignMode Then _
            ScriptManager.RegisterClientScriptResource(Me, GetType(QAWebPartClientScript), "Fullstep.PMWebControls.FSQAWebPartsQA.js")
    End Sub
    'Añado esto para que en Chrome y Safari se pueda seleccionar un proveedor para elimnincar.
    Private Sub FSQAWebPartCertificados_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
        Dim sBrowser = HttpContext.Current.Request.Browser.Browser
        If sBrowser <> "InternetExplorer" And sBrowser <> "IE" Then
             If Not Page.ClientScript.IsStartupScriptRegistered(Me.GetType(), "ScriptCargando") Then
			   Dim sScript = "$('[id$=lstProveedores] option').live('click', function () {" & vbCrLf & _
							"if ($(this).attr('selected') == '' || typeof ($(this).attr('selected')) == 'undefined') {" & vbCrLf & _
								"$(this).attr('selected', 'selected');" & vbCrLf & _
							"} else { $(this).removeAttr('selected') }" & vbCrLf & _
						"});"
				ScriptManager.RegisterStartupScript(Me, Me.GetType(), "ScriptCargando", sScript, True)
			end if
        End If
    End Sub
End Class
