﻿Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.HtmlControls
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.ComponentModel
Imports System.Configuration
Imports System.Drawing
Imports Dundas.Charting.WebControl

Public Class FSQAWebPartPanelCalidad
    Inherits WebPart

    Private lblTitulo As System.Web.UI.WebControls.Label
    Private lblTotal As System.Web.UI.WebControls.Label
    Private lblNumTotal As System.Web.UI.WebControls.Label
    Private lblMensaje As System.Web.UI.WebControls.Label
    Private sTitulo As String
    Private mNumLineas As Integer

    Private lblError As System.Web.UI.WebControls.Label

    Private msOperador As String
    Private msPuntuacionDesde As Double
    Private msPuntuacionHasta As Double
    Private msPuntuacionDesdeEsNothing As Boolean = True
    Private msPuntuacionHastaEsNothing As Boolean = True
    Private miIndiceOrdenPuntuaciones As Integer

    Private mlIDMaterialQA As Long
    Private msMaterialGSDen As String
    Private msMaterialGS As String
    Private msCodArt As String
    Private sVarCal As String   'Id de la variable de calidad seleccionada
    Private sUNQA As String     'Id de la Unidad de negocio seleccionada de la que se va a obtener la PUNTUACION
    Private sUNQA_PROVE As String     'Ids de las unidades de negocio de las que hay que obtener proveedores. La que se chequee más su padre

    Private miIndiceTipoProveedor As Integer 'Indica la seleccion de los option buttons del tipo de proveedore selecccionado
    Private msCodProves As String ' Lista de codigo de proveedores separados por #
    Private msDenProves As String
    Private mbMostrarProveedoresBaja As Boolean 'Almacena si se muestra o no los proveedores dados de baja

    Private miIndiceTipoGrafico As Integer '//0-->Barras;//1-->Evolucion
    Private miDesdeMesGrafico As Integer
    Private miDesdeAnyoGrafico As Integer
    Private msHastaMesGrafico As Integer
    Private msHastaAnyoGrafico As Integer

    Private chartPanelCalidad As Dundas.Charting.WebControl.Chart
    Private mbMostrarMensaje As Boolean


    Private _Textos(53) As String
    Private _sIdioma As String
    Private Shared ModoEdicion As Boolean = False


    Private Class ControlEditor
        Inherits EditorPart

        Private _txtTitulo As TextBox
        Private _txtAncho As TextBox
        Private _reqAncho As RequiredFieldValidator
        Private _ranAncho As RangeValidator
        Private _wneNumProveedores As Infragistics.Web.UI.EditorControls.WebNumericEditor
        Private _txtPuntuacionDesde As Infragistics.Web.UI.EditorControls.WebNumericEditor
        Private _txtPuntuacionHasta As Infragistics.Web.UI.EditorControls.WebNumericEditor

        Private _lstOperadorPuntuaciones As ListBox
        Private _lstOrdenPuntuaciones As ListBox
        Private _lstMaterialQA As ListBox

        Private _tvVarCal As Global.System.Web.UI.WebControls.TreeView
        Private _tvUNQA As Global.System.Web.UI.WebControls.TreeView
        Private _hUNQA As Global.System.Web.UI.WebControls.HiddenField
        Private _hidnumVariablesCalidad As HiddenField
        Private _hidnumUNQA As HiddenField
        Private _rblTipoProveedores As RadioButtonList
        Private _lstProveedores As ListBox
        '*********************+
        Private _hidCodProves As HiddenField
        Private _hidDenProves As HiddenField
        '***********************
        Private _imgMas As HtmlImage
        Private _imgMenos As HtmlImage
        Private _chkBajaProveedor As CheckBox
        Private _txtMaterialGS As TextBox
        Private _hidMaterialGS As HiddenField

        Private _imgLupa As HtmlImage
        Private _txtCodArticulo As TextBox
        Private _txtDenArticulo As TextBox

        Private WithEvents _imgLupa2_1 As LinkButton
        Private _imgLupa2_2 As HtmlImage

        Private _lstTipoGrafico As ListBox
        Private _lstMesDesdeGrafico As ListBox
        Private _lstMesHastaGrafico As ListBox

        Private UNQASProveedores As New Dictionary(Of Integer, String)
        Private _DUNQASProve As Dictionary(Of Integer, String)
        Protected Property DUNQASProve() As Dictionary(Of Integer, String)
            Get
                If _DUNQASProve Is Nothing Then
                    _DUNQASProve = ViewState("DUNQASProve")
                End If
                Return _DUNQASProve
            End Get
            Set(ByVal value As Dictionary(Of Integer, String))
                ViewState("DUNQASProve") = value
            End Set
        End Property

        ''' <summary>
        ''' Lo llama el marco de trabajo de las páginas ASP.NET para indicar a los controles de servidor que utilizan la implementación basada en la composición que creen los controles secundarios que contengan como forma de preparar la devolución o representación de los datos.
        ''' </summary>
        Protected Overrides Sub CreateChildControls()
            Dim pag As FSNServer.IFSNPage = CType(Me.Page, FSNServer.IFSNPage)
            Dim i As Integer = 0
            Dim FSWSServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim part As FSQAWebPartPanelCalidad = CType(WebPartToEdit, FSQAWebPartPanelCalidad)
            Controls.Clear()
            _txtTitulo = New TextBox()
            _txtTitulo.Width = Unit.Pixel(250)
            _txtTitulo.Text = part.Title
            Controls.Add(_txtTitulo)
            _txtAncho = New TextBox()
            _txtAncho.MaxLength = 3
            _txtAncho.Width = Unit.Pixel(30)
            _txtAncho.Text = part.Width.Value.ToString()
            _txtAncho.ID = Me.ID & "_txtAncho"
            Controls.Add(_txtAncho)
            _reqAncho = New RequiredFieldValidator()
            _reqAncho.ID = _txtAncho.ID & "_Req"
            _reqAncho.ErrorMessage = "*"
            _reqAncho.ControlToValidate = _txtAncho.ID
            Controls.Add(_reqAncho)
            _ranAncho = New RangeValidator()
            _ranAncho.ID = _txtAncho.ID & "_Ran"
            _ranAncho.Type = ValidationDataType.Integer
            _ranAncho.MaximumValue = 500
            _ranAncho.MinimumValue = 200
            _ranAncho.ErrorMessage = "(200-500)"
            _ranAncho.ControlToValidate = _txtAncho.ID
            Controls.Add(_ranAncho)

            'nº de Proveedores
            _wneNumProveedores = New Infragistics.Web.UI.EditorControls.WebNumericEditor
            _wneNumProveedores.ID = Me.ID & "_wneNumNoConformidades"
            _wneNumProveedores.Width = Unit.Pixel(30)
            _wneNumProveedores.MaxLength = 2
            _wneNumProveedores.DataMode = Infragistics.Web.UI.EditorControls.NumericDataMode.Int
            Controls.Add(_wneNumProveedores)

            _lstOperadorPuntuaciones = New ListBox
            _lstOperadorPuntuaciones.Items.Clear()
            _lstOperadorPuntuaciones.SelectionMode = ListSelectionMode.Single
            _lstOperadorPuntuaciones.Rows = 1
            _lstOperadorPuntuaciones.Items.Add("")
            _lstOperadorPuntuaciones.Items.Add("=")
            _lstOperadorPuntuaciones.Items.Add("<")
            _lstOperadorPuntuaciones.Items.Add("<=")
            _lstOperadorPuntuaciones.Items.Add(">")
            _lstOperadorPuntuaciones.Items.Add(">=")
            _lstOperadorPuntuaciones.SelectedValue = part.Operador
            Dim sIDPuntuacionHasta As String
            sIDPuntuacionHasta = Me.ClientID & "_" & Me.ID & "_txtPuntuacionHasta"
            _lstOperadorPuntuaciones.Attributes.Add("onChange", "javascript:cambioOperador(this,'" & sIDPuntuacionHasta & "');")
            Controls.Add(_lstOperadorPuntuaciones)


            'Puntuacion desde !!! Falta validacion!!!!!
            _txtPuntuacionDesde = New Infragistics.Web.UI.EditorControls.WebNumericEditor
            _txtPuntuacionDesde.Width = Unit.Pixel(50)
            '_txtPuntuacionDesde.Text = IIf(part.PuntuacionDesdeEsNothing, Nothing, part.PuntuacionDesde)
            _txtPuntuacionDesde.Value = IIf(part.PuntuacionDesdeEsNothing, Nothing, part.PuntuacionDesde)
            _txtPuntuacionDesde.ID = Me.ID & "_txtPuntuacionDesde"
            _txtPuntuacionDesde.DataMode = Infragistics.Web.UI.EditorControls.NumericDataMode.Double
            Controls.Add(_txtPuntuacionDesde)

            'Puntuacion Hasta
            _txtPuntuacionHasta = New Infragistics.Web.UI.EditorControls.WebNumericEditor
            _txtPuntuacionHasta.Width = Unit.Pixel(50)
            _txtPuntuacionHasta.Value = IIf(part.PuntuacionHastaEsNothing, Nothing, part.PuntuacionHasta)
            _txtPuntuacionHasta.ID = Me.ID & "_txtPuntuacionHasta"
            _txtPuntuacionHasta.DataMode = Infragistics.Web.UI.EditorControls.NumericDataMode.Double
            Controls.Add(_txtPuntuacionHasta)


            _lstOrdenPuntuaciones = New ListBox
            _lstOrdenPuntuaciones.Items.Clear()
            _lstOrdenPuntuaciones.SelectionMode = ListSelectionMode.Single
            _lstOrdenPuntuaciones.Rows = 1
            _lstOrdenPuntuaciones.Items.Add(New ListItem(part.Textos(48), 0)) '"ascendente"
            _lstOrdenPuntuaciones.Items.Add(New ListItem(part.Textos(49), 1)) '"descendente"
            _lstOrdenPuntuaciones.SelectedIndex = part.IndiceOrdenPuntuaciones
            Controls.Add(_lstOrdenPuntuaciones)

            Try
                _lstMaterialQA = New ListBox
                Dim PMMaterialesQA As FSNServer.MaterialesQA = FSWSServer.Get_Object(GetType(FSNServer.MaterialesQA))
                Dim dsMaterialesQA As DataSet = PMMaterialesQA.GetData(pag.Idioma, , True, pag.Usuario.Pyme, pag.Usuario.QARestProvMaterial, pag.Usuario.CodPersona)
                _lstMaterialQA.Items.Clear()
                _lstMaterialQA.SelectionMode = ListSelectionMode.Single
                _lstMaterialQA.Rows = 1
                _lstMaterialQA.CausesValidation = True
                _lstMaterialQA.Items.Add(New ListItem("", 0))
                For i = 0 To dsMaterialesQA.Tables(0).Rows.Count - 1
                    _lstMaterialQA.Items.Add(New ListItem(IIf(IsDBNull(dsMaterialesQA.Tables(0).Rows.Item(i)("DEN_" & pag.Idioma)), "", dsMaterialesQA.Tables(0).Rows.Item(i)("DEN_" & pag.Idioma)), dsMaterialesQA.Tables(0).Rows.Item(i)("ID")))
                Next
                _lstMaterialQA.SelectedValue = part.MaterialQA
                Controls.Add(_lstMaterialQA)
            Catch ex As Exception
                'Unicamente buscamos que la carga no casque.No lanzamos la excepción ni la tratamos.
                'Lo que si hacemos es añadir los controles para que luego no casque
                Controls.Add(_lstMaterialQA)
            End Try

            'Material GS
            Dim sMaterialID As String = ""
            Dim sMaterialBDID As String = ""
            Dim sCodArticuloID As String = ""
            Dim sDenArticuloID As String = ""

            sMaterialBDID = Me.ClientID & "_" & Me.ID & "_txtMaterialGS_BBDD"
            sMaterialID = Me.ClientID & "_" & Me.ID & "_txtMaterialGS_Den"
            sCodArticuloID = Me.ClientID & "_" & Me.ID & "_txtCodArticulo"
            sDenArticuloID = Me.ClientID & "_" & Me.ID & "_txtDenArticulo"

            _txtMaterialGS = New TextBox
            _txtMaterialGS.Width = Unit.Pixel(250)
            _txtMaterialGS.Enabled = True
            _txtMaterialGS.Attributes("onkeydown") = "return eliminarMaterial(event,this,'" & sMaterialBDID & "','" & sCodArticuloID & "','" & sDenArticuloID & "')"
            _txtMaterialGS.ID = Me.ID & "_txtMaterialGS_Den"
            Controls.Add(_txtMaterialGS)


            _hidMaterialGS = New HiddenField
            _hidMaterialGS.ID = Me.ID & "_txtMaterialGS_BBDD"
            Controls.Add(_hidMaterialGS)

            _imgLupa = New HtmlImage()
            '_imgLupa.Alt = "Buscador material"
            _imgLupa.Src = Me.Page.ClientScript.GetWebResourceUrl(GetType(FSQAWebPartPanelCalidad), "Fullstep.PMWebControls.Img_Ico_Lupa.gif")
            _imgLupa.Attributes.Add("onClick", "javascript:window.open('" & ConfigurationManager.AppSettings("rutaQA") & "_common/materiales.aspx?desde=WebPart&IdControl=" & sMaterialID & "&MaterialGS_BBDD=" & sMaterialBDID & "','_blank', 'width=550,height=550,status=yes,resizable=no,top=100,left=200');")
            Controls.Add(_imgLupa)

            'Articulo
            _txtCodArticulo = New TextBox
            _txtCodArticulo.Width = Unit.Pixel(100)
            _txtCodArticulo.ID = Me.ID & "_txtCodArticulo"
            _txtCodArticulo.Attributes("onkeydown") = "return eliminarArticulo(event,'" & sCodArticuloID & "','" & sDenArticuloID & "')"
            Controls.Add(_txtCodArticulo)

            _txtDenArticulo = New TextBox
            _txtDenArticulo.Width = Unit.Pixel(200)
            _txtDenArticulo.ID = Me.ID & "_txtDenArticulo"
            _txtDenArticulo.Attributes("onkeydown") = "return eliminarArticulo(event,'" & sCodArticuloID & "','" & sDenArticuloID & "')"
            Controls.Add(_txtDenArticulo)

            _imgLupa2_2 = New HtmlImage()
            _imgLupa2_2.Src = Me.Page.ClientScript.GetWebResourceUrl(GetType(FSQAWebPartPanelCalidad), "Fullstep.PMWebControls.Img_Ico_Lupa.gif")
            _imgLupa2_2.Style.Add("border", "0px")
            _imgLupa2_1 = New LinkButton
            _imgLupa2_1.Controls.Add(_imgLupa2_2)
            Controls.Add(_imgLupa2_1)

            _hidnumVariablesCalidad = New HiddenField
            _hidnumVariablesCalidad.ID = Me.ID & "_numVarCalidad"
            If part.VarCal <> "" Then ' Por defecto se inicializa a "Total del proveedor"
                _hidnumVariablesCalidad.Value = "1"
            End If
            Controls.Add(_hidnumVariablesCalidad)

            Try
                'Cargar treeView Variables de Calidad
                _tvVarCal = New TreeView
                Dim PMVariablesCalidad As FSNServer.VariablesCalidad = FSWSServer.Get_Object(GetType(FSNServer.VariablesCalidad))
                Dim dsVarCal As DataSet = PMVariablesCalidad.GetData_hds(pag.Usuario.Cod, pag.Idioma, pag.Usuario.Pyme, 1)

                Dim a As New System.Web.UI.WebControls.TreeNodeBinding
                a.SelectAction = TreeNodeSelectAction.None
                _tvVarCal.ShowCheckBoxes = TreeNodeTypes.All
                _tvVarCal.DataBindings.Add(a)
                _tvVarCal.DataBindings.Item(0).TextField = "DEN"
                _tvVarCal.DataBindings.Item(0).ValueField = "ID"
                _tvVarCal.DataSource = New HierarchicalDataSet(dsVarCal.Tables(0).DefaultView, "ID", "PADRE")
                _tvVarCal.DataBind()
                _tvVarCal.CollapseAll()
                '_tvVarCal.ExpandAll()
                If part.VarCal = "0-0" Then ' Por defecto se inicializa a "Total del proveedor"
                    _tvVarCal.Nodes(0).Checked = True
                    part.VarCal = _tvVarCal.Nodes(0).Value
                Else
                    For Each Node0 As TreeNode In _tvVarCal.Nodes
                        If Node0.Value = part.VarCal Then
                            Node0.Checked = True
                        End If

                        For Each Node1 As TreeNode In Node0.ChildNodes
                            If Node1.Value = part.VarCal Then
                                Node1.Checked = True
                            End If

                            For Each Node2 As TreeNode In Node1.ChildNodes
                                If Node2.Value = part.VarCal Then
                                    Node2.Checked = True
                                End If

                                For Each Node3 As TreeNode In Node2.ChildNodes
                                    If Node3.Value = part.VarCal Then
                                        Node3.Checked = True
                                    End If

                                    For Each Node4 As TreeNode In Node3.ChildNodes
                                        If Node4.Value = part.VarCal Then
                                            Node4.Checked = True
                                        End If

                                        For Each Node5 As TreeNode In Node4.ChildNodes
                                            If Node5.Value = part.VarCal Then
                                                Node5.Checked = True
                                            End If
                                        Next
                                    Next
                                Next
                            Next
                        Next
                    Next
                End If
                _tvVarCal.Attributes.Add("onClick", "VariablesCalidad_NodeChecked(event, '" & Me.ClientID & "_" & Me.ID & "_numVarCalidad" & "')")

                PMVariablesCalidad = Nothing
                Controls.Add(_tvVarCal)
            Catch ex As Exception
                'Unicamente buscamos que la carga no casque.No lanzamos la excepción ni la tratamos.
                'Lo que si hacemos es añadir los controles para que luego no casque
                Controls.Add(_tvVarCal)
            End Try

            Try
                'Cargar treeView Unidad de negocio
                _tvUNQA = New TreeView
                _hUNQA = New HiddenField
                _hidnumUNQA = New HiddenField
                Dim PMUnidadesQA As FSNServer.UnidadesNeg = FSWSServer.Get_Object(GetType(FSNServer.UnidadesNeg))
                Dim dsUNQA As DataSet = PMUnidadesQA.UsuGetData_hds(pag.Usuario.Cod, FSNLibrary.TipoAccesoUNQAS.ConsultarPuntuacionesPanelCalidad, sIdi:=pag.Idioma, iPyme:=pag.Usuario.Pyme)

                _hidnumUNQA = New HiddenField
                _hidnumUNQA.ID = Me.ID & "_numUNQA"
                If part.UNQA <> "" AndAlso part.UNQA <> "-1" Then
                    If dsUNQA.Tables(1).Rows.Count > 0 Then
                        If part.UNQA <> dsUNQA.Tables(1).Rows(0).Item(0) Then
                            _hidnumUNQA.Value = "1"
                        End If
                    Else
                        _hidnumUNQA.Value = "1"
                    End If
                End If
                Controls.Add(_hidnumUNQA)

                Dim b As New System.Web.UI.WebControls.TreeNodeBinding
                b.SelectAction = TreeNodeSelectAction.None
                _tvUNQA.ShowCheckBoxes = TreeNodeTypes.All
                _tvUNQA.DataBindings.Add(b)
                _tvUNQA.DataBindings.Item(0).TextField = "DEN"
                _tvUNQA.DataBindings.Item(0).ValueField = "ID"
                _tvUNQA.DataBindings.Item(0).NavigateUrlField = "ACCESO"
                _tvUNQA.DataSource = New HierarchicalDataSet(dsUNQA.Tables(0).DefaultView, "ID", "PADRE")
                _tvUNQA.DataBind()
                ConfigurarTreeViewUNQA(_tvUNQA.Nodes)
                DUNQASProve = UNQASProveedores
                Dim HijosConAcceso As Boolean = False
                EliminarUNQASinAcceso(_tvUNQA.Nodes, _tvUNQA.Nodes.Count, HijosConAcceso)
                If dsUNQA.Tables(1).Rows.Count > 0 Then
                    _hUNQA.Value = dsUNQA.Tables(1).Rows(0).Item(0)
                End If

                _tvUNQA.Attributes.Add("onClick", "UNQA_NodeChecked(event, '" & Me.ClientID & "_" & Me.ID & "_numUNQA" & "')")

                PMUnidadesQA = Nothing
                Controls.Add(_tvUNQA)
                Controls.Add(_hUNQA)
            Catch ex As Exception
                'Unicamente buscamos que la carga no casque.No lanzamos la excepción ni la tratamos.
                'Lo que si hacemos es añadir los controles para que luego no casque
                Controls.Add(_hidnumUNQA)
                Controls.Add(_tvUNQA)
                Controls.Add(_hUNQA)
            End Try

            'Tipo proveedores
            Dim sTipoProveedores As String
            _rblTipoProveedores = New RadioButtonList
            _rblTipoProveedores.ID = Me.ID & "_rblTipoProveedores"
            sTipoProveedores = Me.ClientID & "_" & Me.ID & "_rblTipoProveedores_3"
            For i = 0 To 3
                _rblTipoProveedores.Items.Add(New ListItem(part.Textos(i + 4), i))
            Next
            _rblTipoProveedores.SelectedIndex = 0
            Controls.Add(_rblTipoProveedores)

            Dim sProveedorID As String
            _lstProveedores = New ListBox
            _lstProveedores.Rows = 5
            _lstProveedores.SelectionMode = ListSelectionMode.Multiple
            _lstProveedores.BorderStyle = WebControls.BorderStyle.Inset
            _lstProveedores.Width = Unit.Percentage(100)
            _lstProveedores.ID = Me.ID & "_lstProveedores"
            sProveedorID = Me.ClientID & "_" & Me.ID & "_lstProveedores"
            Controls.Add(_lstProveedores)

            Dim sCodProvesID As String
            _hidCodProves = New HiddenField
            _hidCodProves.ID = Me.ID & "_CodProves"
            sCodProvesID = Me.ClientID & "_" & Me.ID & "_CodProves"
            Controls.Add(_hidCodProves)

            Dim sDenProvesID As String
            _hidDenProves = New HiddenField
            _hidDenProves.ID = Me.ID & "_DenProves"
            sDenProvesID = Me.ClientID & "_" & Me.ID & "_DenProves"
            Controls.Add(_hidDenProves)


            _imgMas = New HtmlImage()
            _imgMas.Alt = part.Textos(8) '"Añadir proveedor a la selección"
            _imgMas.Src = Me.Page.ClientScript.GetWebResourceUrl(GetType(FSQAWebPartPanelCalidad), "Fullstep.PMWebControls.Mas.gif")
            _imgMas.Attributes.Add("onClick", "javascript:window.open('" & ConfigurationManager.AppSettings("RutaFS") & "_common/BuscadorProveedores.aspx?PM=false&desde=WebPart&IdControl=" & sProveedorID & "&codProves=" & sCodProvesID & "&denProves=" & sDenProvesID & "&IdControlTipoProveedor=" & sTipoProveedores & "', '_blank', 'width=835,height=635,status=yes,resizable=no,top=0,left=150,scrollbars=yes');")
            Controls.Add(_imgMas)


            Dim sMensaje As String = part.Textos(9) '"Seleccione el proveedor que desea eliminar"
            _imgMenos = New HtmlImage()
            _imgMenos.Alt = part.Textos(11) '"Eliminar proveedor de la selección"
            _imgMenos.Src = Me.Page.ClientScript.GetWebResourceUrl(GetType(FSQAWebPartPanelCalidad), "Fullstep.PMWebControls.Menos.gif")
            _imgMenos.Attributes.Add("onClick", "javascript:eliminarElementoLista('" & sProveedorID & "','" & sCodProvesID & "','" & sDenProvesID & "','" & sMensaje & "');")
            Controls.Add(_imgMenos)

            _chkBajaProveedor = New CheckBox
            _chkBajaProveedor.Text = part.Textos(10) '"Incluir proveedores dados de baja"
            Controls.Add(_chkBajaProveedor)

            'Tipo grafico Barras // Evolucion
            _lstTipoGrafico = New ListBox
            _lstTipoGrafico.Items.Clear()
            _lstTipoGrafico.SelectionMode = ListSelectionMode.Single
            _lstTipoGrafico.Rows = 1
            _lstTipoGrafico.Items.Add(New ListItem(part.Textos(12), 0))  'Barras
            _lstTipoGrafico.Items.Add(New ListItem(part.Textos(13), 1)) '"Evolución  DE MOMENTO SE QUITA EL DE EVOLUCION
            _lstTipoGrafico.Attributes.Add("onChange", "javascript:tipoGrafico()")
            Controls.Add(_lstTipoGrafico)
            'Denominaciones de los meses
            Dim meses(11) As String
            For i = 0 To 11
                meses(i) = part.Textos(14 + i)
            Next
            Try
                _lstMesDesdeGrafico = New ListBox
                _lstMesHastaGrafico = New ListBox

                Dim PMProveedores As FSNServer.Proveedores = FSWSServer.Get_Object(GetType(FSNServer.Proveedores))
                PMProveedores.ObtenerFechasHistPunt()
                Dim dsFechas As DataSet = PMProveedores.Data

                _lstMesDesdeGrafico.Items.Clear()
                _lstMesDesdeGrafico.SelectionMode = ListSelectionMode.Single
                _lstMesDesdeGrafico.Rows = 1
                For i = 0 To dsFechas.Tables(0).Rows.Count - 1
                    _lstMesDesdeGrafico.Items.Add(New ListItem(meses(dsFechas.Tables(0).Rows.Item(i)("MES") - 1) & " " & dsFechas.Tables(0).Rows.Item(i)("ANO"), dsFechas.Tables(0).Rows.Item(i)("MES") & "#" & dsFechas.Tables(0).Rows.Item(i)("ANO")))
                Next
                Dim MesPuntuacionEnCurso As Integer = dsFechas.Tables(0).Rows.Item(i - 1)("MES") + 1
                Dim AnyoPuntuacionEnCurso As Integer = dsFechas.Tables(0).Rows.Item(i - 1)("ANO")
                If dsFechas.Tables(0).Rows.Item(i - 1)("MES") = 12 Then
                    AnyoPuntuacionEnCurso = AnyoPuntuacionEnCurso + 1
                    MesPuntuacionEnCurso = 1
                End If

                _lstMesHastaGrafico.Items.Clear()
                _lstMesHastaGrafico.SelectionMode = ListSelectionMode.Single
                _lstMesHastaGrafico.Rows = 1
                For i = 0 To dsFechas.Tables(0).Rows.Count - 1
                    _lstMesHastaGrafico.Items.Add(New ListItem(meses(dsFechas.Tables(0).Rows.Item(i)("MES") - 1) & " " & dsFechas.Tables(0).Rows.Item(i)("ANO"), dsFechas.Tables(0).Rows.Item(i)("MES") & "#" & dsFechas.Tables(0).Rows.Item(i)("ANO")))
                Next
                _lstMesHastaGrafico.Items.Add(New ListItem(meses(MesPuntuacionEnCurso - 1) & " " & AnyoPuntuacionEnCurso, MesPuntuacionEnCurso & "#" & AnyoPuntuacionEnCurso))

                _lstMesHastaGrafico.SelectedValue = MesPuntuacionEnCurso & "#" & AnyoPuntuacionEnCurso

                Controls.Add(_lstMesDesdeGrafico)
                Controls.Add(_lstMesHastaGrafico)
            Catch ex As Exception
                'Unicamente buscamos que la carga no casque.No lanzamos la excepción ni la tratamos.
                'Lo que si hacemos es añadir los controles para que luego no casque
                Controls.Add(_lstMesDesdeGrafico)
                Controls.Add(_lstMesHastaGrafico)
            End Try

        End Sub
        ''' <summary>
        ''' Procedimiento que recorre los nodos del treeview de unqas
        ''' Conf del treeview por defecto. Todos los nodos tienen check/Todos están contraidos
        ''' Si no tiene acceso a la unqa y a la unqa padre tampoco no mostramos check (Hemos cargado el campo ACCESO en la propiedad NavigateUrl)
        ''' Expandimos los nodos cunado tiene acceso al menos a una unqa.
        ''' </summary>
        ''' <param name="Nodos">Coleccion de Nodos del treeview de UNQAs</param>
        ''' <param name="sChequarNodos">strng con una lista de nodods separados por coma a chequear</param>
        ''' <remarks></remarks>
        Sub ConfigurarTreeViewUNQA(ByRef Nodos As TreeNodeCollection, Optional ByVal sChequarNodos As String = "")
            For Each Nodo As TreeNode In Nodos
                Dim bAcceso As Boolean = IIf(Nodo.NavigateUrl = "0", False, True)

                Dim bHijos As Boolean = IIf(Nodo.ChildNodes.Count = 0, False, True)

                If Not bAcceso Then
                    Nodo.ShowCheckBox = False
                Else
                    Dim UNQAProves As String = Nodo.Value
                    For Each NodoHijo As TreeNode In Nodo.ChildNodes
                        UNQAProves = UNQAProves & "," & NodoHijo.Value

                        For Each NodoNieto As TreeNode In NodoHijo.ChildNodes
                            UNQAProves = UNQAProves & "," & NodoNieto.Value
                        Next
                    Next

                    If Not Nodo.Parent Is Nothing Then
                        UNQAProves = UNQAProves & "," & Nodo.Parent.Value

                        If Not Nodo.Parent.Parent Is Nothing Then
                            UNQAProves = UNQAProves & "," & Nodo.Parent.Parent.Value
                        End If
                    End If

                    UNQASProveedores.Add(Nodo.Value, UNQAProves)
                End If
                'chequeamos las unidades de negoico de la ultima busqueda
                If sChequarNodos <> "" Then
                    If sChequarNodos.IndexOf("," & Nodo.Value & ",") >= 0 Then
                        Nodo.Checked = True
                    End If
                End If

                'Recorremos los hijos
                If bHijos Then
                    ConfigurarTreeViewUNQA(Nodo.ChildNodes, sChequarNodos)
                End If

            Next

        End Sub

        ''' <summary>
        ''' Función recursiva a la que se le pasa el arbol de nodos y elimina las ramas en las cuales
        ''' el usuario no tiene permiso de consulta/modificación de puntuaciones para ninguno de sus nodos
        ''' </summary>
        ''' <param name="Nodos">nodos del arbol</param>
        ''' <param name="CountNodes">número de nodos</param>
        ''' <param name="HijosConAcceso">si el nodo tiene algún hijo con permiso del usuario</param>
        ''' <remarks></remarks>
        ''' ilg 271009
        Sub EliminarUNQASinAcceso(ByRef Nodos As TreeNodeCollection, ByRef CountNodes As Integer, Optional ByRef HijosConAcceso As Boolean = False)
            Dim i As Integer = 0
            Do While i < CountNodes
                If Nodos(i).NavigateUrl = "0" AndAlso Nodos(i).ChildNodes.Count = 0 Then
                    Nodos.Remove(Nodos(i))
                    CountNodes = CountNodes - 1
                ElseIf Nodos(i).NavigateUrl = "0" AndAlso Nodos(i).ChildNodes.Count > 0 Then
                    'Nodos(i).NavigateUrl = ""
                    EliminarUNQASinAcceso(Nodos(i).ChildNodes, Nodos(i).ChildNodes.Count, HijosConAcceso)

                    If Not HijosConAcceso Or Nodos(i).ChildNodes.Count = 0 Then
                        Nodos.Remove(Nodos(i))
                        CountNodes = CountNodes - 1
                    Else
                        If Not Nodos(i).Parent Is Nothing Then
                            HijosConAcceso = True
                        Else
                            HijosConAcceso = False
                        End If
                        i = i + 1
                    End If
                Else
                    'Nodos(i).NavigateUrl = ""
                    If Nodos(i).ChildNodes.Count > 0 Then
                        EliminarUNQASinAcceso(Nodos(i).ChildNodes, Nodos(i).ChildNodes.Count, HijosConAcceso)

                        If Not HijosConAcceso Then
                            Nodos(i).Collapse()
                        End If
                    End If

                    If Not Nodos(i).Parent Is Nothing Then
                        HijosConAcceso = True
                    End If

                    i = i + 1
                End If

            Loop

        End Sub

        ''' <summary>
        ''' Presenta el contenido del control en el sistema de escritura especificado. Este método lo usan principalmente los programadores de controles. 
        ''' </summary>
        ''' <param name="writer">HtmlTextWriter que representa la secuencia de salida para presentar contenido HTML en el cliente.</param>
        Protected Overrides Sub RenderContents(ByVal writer As System.Web.UI.HtmlTextWriter)
            Dim part As FSQAWebPartPanelCalidad = CType(WebPartToEdit, FSQAWebPartPanelCalidad)
            writer.Write("<b>&nbsp;&nbsp;" & part.Textos(1) & ":</b> ")
            _txtTitulo.RenderControl(writer)
            writer.WriteBreak()
            writer.Write("<b>&nbsp;&nbsp;" & part.Textos(2) & ":</b> ")
            _txtAncho.RenderControl(writer)
            _reqAncho.RenderControl(writer)
            _ranAncho.RenderControl(writer)

            writer.WriteBreak()
            writer.Write("<b>&nbsp;&nbsp;" & part.Textos(26) & ":</b> ") '"Nº de no proveedores a mostrar"
            _wneNumProveedores.RenderControl(writer)
            writer.WriteBreak()

            writer.WriteBreak()
            writer.Write("<div style='border: 1px solid #CCCCCC;'>")
            writer.Write("<table border=0 cellpadding=2 width=80%>")
            writer.Write("<tr><td colspan=4><b>" & part.Textos(27) & "</b></td></tr>") '"Puntuaciones"
            writer.Write("<tr><td colspan=2>&nbsp;</td>")
            writer.Write("<td>" & part.Textos(28) & "</td>") '"desde"
            writer.Write("<td>" & part.Textos(29) & "</td>") '"hasta"
            writer.Write("</tr>")
            writer.Write("<tr>")
            writer.Write("<td>" & part.Textos(30) & "</td>") '"Rango de puntuaciones"
            writer.Write("<td>")
            _lstOperadorPuntuaciones.RenderControl(writer)
            writer.Write("</td>")
            writer.Write("<td>")
            _txtPuntuacionDesde.RenderControl(writer)
            writer.Write("</td>")
            writer.Write("<td>")
            _txtPuntuacionHasta.RenderControl(writer)
            writer.Write("</td>")
            writer.Write("</tr>")
            writer.Write("<tr>")
            writer.Write("<td>" & part.Textos(31) & "</td>") '"Orden"
            writer.Write("<td colspan=3>")
            _lstOrdenPuntuaciones.RenderControl(writer)
            writer.Write("</td></tr></table>")
            writer.Write("</div>")
            writer.WriteBreak()
            'Material QA
            writer.Write("<div style='border: 1px solid #CCCCCC;'>")
            writer.Write("<table border=0 cellpadding=2 width=""80%""><tr><td><b>" & part.Textos(32) & " </b></td></tr>")
            writer.Write("<tr><td>")
            _lstMaterialQA.RenderControl(writer)
            writer.Write("</td></tr></table></div>")
            writer.WriteBreak()

            'Material GS
            writer.Write("<div style='border: 1px solid #CCCCCC;'>")
            writer.Write("<table border=0 cellpadding=2 width=80%><tr><td><b>" & part.Textos(33) & " </b></td></tr>") '"Material de GS"
            writer.Write("<tr><td><table><tr><td>")
            _hidMaterialGS.RenderControl(writer)
            _txtMaterialGS.RenderControl(writer)
            writer.Write("</td>")
            writer.Write("<td>")
            _imgLupa.RenderControl(writer)
            writer.Write("</td></tr></table></td></tr>")
            writer.Write("<tr><td><b>" & part.Textos(34) & " </b></td></tr>") '"Artículo"
            writer.Write("<tr><td><table><tr><td>")
            _txtCodArticulo.RenderControl(writer)
            writer.Write("</td>")
            writer.Write("<td>")
            _txtDenArticulo.RenderControl(writer)
            writer.Write("</td>")
            writer.Write("<td>")
            _imgLupa2_1.RenderControl(writer)
            writer.Write("</td>")
            writer.Write("</tr>")
            writer.Write("</table></td></tr></table>")
            writer.Write("</div>")
            writer.WriteBreak()

            'Variable calidad
            writer.Write("<div style='border: 1px solid #CCCCCC;max-height:300px;overflow:auto;'><b>" & "&nbsp;&nbsp;" & part.Textos(35) & "</b>") '"Variable de calidad a considerar"
            writer.WriteBreak()
            _tvVarCal.RenderControl(writer)
            _hidnumVariablesCalidad.RenderControl(writer)
            writer.WriteBreak()
            writer.Write("</div>")
            writer.WriteBreak()

            'Unidad de negocio
            writer.Write("<div style='border: 1px solid #CCCCCC;;max-height:300px;overflow:auto;'><b>" & "&nbsp;&nbsp;" & part.Textos(36) & "</b>") '"Unidad de negocio"
            writer.WriteBreak()
            _tvUNQA.RenderControl(writer)
            _hUNQA.RenderControl(writer)
            _hidnumUNQA.RenderControl(writer)
            writer.WriteBreak()
            writer.Write("</div>")
            writer.WriteBreak()

            'Proveedor
            writer.Write("<div style='border: 1px solid #CCCCCC;'>")
            writer.Write("<table border=0 cellpadding=2 width=80%><tr><td colspan=2><b>" & part.Textos(37) & " </b></td></tr>") '"Proveedores"
            writer.Write("<tr><td colspan=2>")
            _hidCodProves.RenderControl(writer)
            _hidDenProves.RenderControl(writer)
            _rblTipoProveedores.RenderControl(writer)
            writer.Write("</td></tr>")
            writer.Write("<tr>")
            writer.Write("<td rowspan=2 width=85%>")
            _lstProveedores.RenderControl(writer)
            writer.Write("</td>")
            writer.Write("<td  valign='bottom'>")
            _imgMas.RenderControl(writer)
            writer.Write("</td>")
            writer.Write("</tr>")
            writer.Write("<tr>")
            writer.Write("<td valign='top'>")
            _imgMenos.RenderControl(writer)
            writer.Write("</td>")
            writer.Write("</tr>")
            writer.Write("</table>")
            _chkBajaProveedor.RenderControl(writer)
            writer.Write("</div>")
            writer.WriteBreak()
            'Tipo Grafico
            writer.Write("<div style='border: 1px solid #CCCCCC;'><b>&nbsp;&nbsp;" & part.Textos(38) & "</b>") ' "Tipo de gráfico"
            writer.WriteBreak()
            writer.Write("&nbsp;&nbsp;")
            _lstTipoGrafico.RenderControl(writer)
            Dim visible As String = "hidden"
            If _lstTipoGrafico.SelectedIndex > 0 Then
                visible = "visible"
            End If
            writer.Write("<div id='div_GraficoEvolucion' style='border: 0px;visibility:" & visible & "'>")
            writer.Write("<table cellpadding=2 width=80%>")
            writer.Write("<tr>")
            writer.Write("<td valign='middle'>&nbsp;&nbsp;")
            writer.Write(part.Textos(28) & " ") 'DESDE
            writer.Write("&nbsp;&nbsp;")
            writer.Write("</td>")

            writer.Write("<td>")
            _lstMesDesdeGrafico.RenderControl(writer)
            writer.Write("&nbsp;&nbsp;")
            writer.Write("</td>")

            writer.Write("<td valign='middle'>")
            writer.Write(part.Textos(29) & "  ")
            writer.Write("&nbsp;&nbsp;")
            writer.Write("</td>")

            writer.Write("<td>")
            _lstMesHastaGrafico.RenderControl(writer)
            writer.Write("&nbsp;&nbsp;")
            writer.Write("</td>")


            writer.Write("</tr>")
            writer.Write("</table>")
            writer.Write("</div>")
            writer.Write("</div>")
            writer.WriteBreak()


        End Sub

        ''' <summary>
        ''' Guarda los valores de un control EditorPart en las propiedades correspondientes del control WebPart asociado.
        ''' </summary>
        ''' <returns>Es true si la acción de guardar los valores del control EditorPart en el control WebPart se realiza correctamente; en caso contrario (si se produce un error), es false.</returns>
        Public Overrides Function ApplyChanges() As Boolean
            Dim part As FSQAWebPartPanelCalidad = CType(WebPartToEdit, FSQAWebPartPanelCalidad)

            Dim bAuxPuntuacionDesdeEsNothing, bAuxPuntuacionHastaEsNothing As Boolean
            Dim sScript As String = ""
            _reqAncho.Validate()
            _ranAncho.Validate()
            If txtPuntuacionDesde.Text <> Nothing Then
                bAuxPuntuacionDesdeEsNothing = False
            Else
                bAuxPuntuacionDesdeEsNothing = True
            End If
            If txtPuntuacionHasta.Text <> Nothing Then
                bAuxPuntuacionHastaEsNothing = False
            Else
                bAuxPuntuacionHastaEsNothing = True
            End If

            If _reqAncho.IsValid() And _ranAncho.IsValid() Then
                If _lstOperadorPuntuaciones.SelectedIndex = 0 And Not bAuxPuntuacionDesdeEsNothing And bAuxPuntuacionHastaEsNothing Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "Mensaje1_RangoPuntuaciones", "alert('" & part.Textos(39) & "');", True) 'Establezca el operador de comparación o la puntuación hasta.
                    Return False
                ElseIf _lstOperadorPuntuaciones.SelectedIndex > 0 And bAuxPuntuacionDesdeEsNothing And bAuxPuntuacionHastaEsNothing Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "Mensaje2_RangoPuntuaciones", "alert('" & part.Textos(40) & "');", True) 'No ha introducido la puntuación de referencia del operador.
                    Return False
                ElseIf Not bAuxPuntuacionHastaEsNothing And bAuxPuntuacionDesdeEsNothing Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "Mensaje3_RangoPuntuaciones", "alert('" & part.Textos(41) & "');", True) 'No ha introducido la puntuación desde.
                    Return False
                Else

                    'Variable de calidad
                    Dim sVarCal As String = ""
                    Dim cont As Integer = 0
                    'Nivel 0
                    For Each Nodo As TreeNode In _tvVarCal.Nodes
                        If Nodo.Checked = True Then
                            sVarCal = Nodo.Value
                            cont = cont + 1
                            If cont > 1 Then Exit For
                        End If
                        'Nivel 1
                        For Each NodoHijos As TreeNode In Nodo.ChildNodes
                            If NodoHijos.Checked = True Then
                                sVarCal = NodoHijos.Value
                                cont = cont + 1
                                If cont > 1 Then Exit For
                            End If

                            'Nivel 2
                            For Each NodoNietos As TreeNode In NodoHijos.ChildNodes
                                If NodoNietos.Checked = True Then
                                    sVarCal = NodoNietos.Value
                                    cont = cont + 1
                                    If cont > 1 Then Exit For
                                End If

                                'Nivel 3
                                For Each NodoBisNietos As TreeNode In NodoNietos.ChildNodes
                                    If NodoBisNietos.Checked = True Then
                                        sVarCal = NodoBisNietos.Value
                                        cont = cont + 1
                                        If cont > 1 Then Exit For
                                    End If

                                    'Nivel 4
                                    For Each NodoTataraNietos As TreeNode In NodoBisNietos.ChildNodes
                                        If NodoTataraNietos.Checked = True Then
                                            sVarCal = NodoTataraNietos.Value
                                            cont = cont + 1
                                            If cont > 1 Then Exit For
                                        End If

                                        'Nivel 5
                                        For Each NodoUltimo As TreeNode In NodoTataraNietos.ChildNodes
                                            If NodoUltimo.Checked = True Then
                                                sVarCal = NodoUltimo.Value
                                                cont = cont + 1
                                                If cont > 1 Then Exit For
                                            End If
                                        Next
                                    Next
                                Next
                            Next
                        Next
                    Next

                    If sVarCal = "" Then
                        'Alert mensaje "Seleccione la variable de calidad a considerar
                        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "Mensaje1", "alert('" & part.Textos(43) & "');", True) 'Seleccione la variable de calidad a considerar


                        Return False
                    Else
                        If cont > 1 Then
                            'Alert mensaje "Seleccione una única variable de calidad"
                            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "Mensaje2", "alert('" & part.Textos(44) & "');", True) 'Seleccione una única variable de calidad
                            Return False
                        End If
                        'Unidad de Negocio
                        Dim sUNQA As String = ""
                        Dim sUNQA_PROVE As String = ""
                        cont = 0
                        For Each Nodo As TreeNode In _tvUNQA.Nodes
                            If Nodo.Checked = True Then
                                sUNQA = Nodo.Value
                                sUNQA_PROVE = DUNQASProve(Nodo.Value)
                                cont = cont + 1
                                If cont > 1 Then Exit For
                            End If
                            For Each NodoHijos As TreeNode In Nodo.ChildNodes
                                If NodoHijos.Checked = True Then
                                    sUNQA = NodoHijos.Value
                                    sUNQA_PROVE = DUNQASProve(NodoHijos.Value)
                                    cont = cont + 1
                                    If cont > 1 Then Exit For
                                End If
                                For Each NodoNietos As TreeNode In NodoHijos.ChildNodes
                                    If NodoNietos.Checked = True Then
                                        sUNQA = NodoNietos.Value
                                        sUNQA_PROVE = DUNQASProve(NodoNietos.Value)
                                        cont = cont + 1
                                        If cont > 1 Then Exit For
                                    End If
                                Next
                            Next
                        Next

                        If cont > 1 Then
                            'alert mensaje (Seleccione una unica Unidad de Negocio)
                            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "Mensaje3", "alert('" & part.Textos(45) & "');", True) 'Seleccione una única Unidad de Negocio
                            Return False
                        ElseIf cont = 0 Then
                            'Si no tiene acceso al grupo que seleccione una unqa
                            If _hUNQA.Value = "" Then
                                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "Mensaje4", "alert('" & part.Textos(46) & "');", True) 'Seleccione la Unidad de Negocio
                                Return False
                            Else
                                'seleccionamos el grupo
                                sUNQA = _hUNQA.Value
                                sUNQA_PROVE = ""
                            End If
                        End If
                        part.Title = _txtTitulo.Text
                        part.Width = Unit.Pixel(CType(_txtAncho.Text, Integer))
                        If _wneNumProveedores.Text <> "" Then
                            part.NumLineas = CType(_wneNumProveedores.Value, Integer)
                        Else
                            part.NumLineas = 0
                        End If

                        part.Operador = lstOperadorPuntuaciones.SelectedValue
                        If _txtPuntuacionDesde.Text <> Nothing Then
                            part.PuntuacionDesdeEsNothing = False
                        ElseIf LstTipoGrafico.SelectedIndex = 1 Then
                            'Alert mensaje "En grafico evolución el número de proveedores a mostrar debe estar entre 1 y 99."
                            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "Mensaje2", "alert('" & part.Textos(53) & "');", True)
                            Return False
                        Else
                            part.PuntuacionDesdeEsNothing = True
                        End If
                        If _txtPuntuacionHasta.Text <> Nothing Then
                            part.PuntuacionHastaEsNothing = False
                        Else
                            part.PuntuacionHastaEsNothing = True
                        End If
                        part.PuntuacionDesde = IIf(part.PuntuacionDesdeEsNothing, Nothing, _txtPuntuacionDesde.Value)
                        part.PuntuacionHasta = IIf(part.PuntuacionHastaEsNothing, Nothing, _txtPuntuacionHasta.Value)
                        part.IndiceOrdenPuntuaciones = lstOrdenPuntuaciones.SelectedIndex
                        part.MaterialQA = lstMaterialQA.SelectedValue
                        part.MaterialGS = _hidMaterialGS.Value
                        part.MaterialGSDen = txtMaterialGS_Den.Text
                        part.VarCal = sVarCal
                        part.UNQA = sUNQA
                        part.UNQA_PROVE = sUNQA_PROVE
                        DUNQASProve = Nothing
                        part.indiceTipoProveedor = TipoProveedor
                        part.CodProves = ListaCodigoProveedores
                        part.DenProves = ListaNombreProveedores
                        part.MostrarProveedoresBaja = _chkBajaProveedor.Checked
                        part.indiceTipoGrafico = LstTipoGrafico.SelectedIndex
                        If LstTipoGrafico.SelectedIndex = 1 Then
                            Dim fechasPuntuaciones() As String
                            fechasPuntuaciones = Split(_lstMesDesdeGrafico.SelectedValue, "#")
                            part.desdeMesGrafico = fechasPuntuaciones(0)
                            part.desdeAnyoGrafico = fechasPuntuaciones(1)
                            fechasPuntuaciones = Split(_lstMesHastaGrafico.SelectedValue, "#")
                            part.HastaMesGrafico = fechasPuntuaciones(0)
                            part.HastaAnyoGrafico = fechasPuntuaciones(1)
                        End If

                        If ((part.Operador = "") And (lstMaterialQA.SelectedValue = 0) And (_hidMaterialGS.Value = "") And (sVarCal = "") And (sUNQA = "") And (ListaCodigoProveedores = "")) Then
                            part.MonstrarMensaje = True
                        Else
                            part.MonstrarMensaje = False
                        End If

                        ''Comentado para despues de la demo
                        ''************************************
                        'ModoEdicion = False
                        part.CreateChildControls()
                        part.CargarPuntuaciones()

                        Return True

                    End If ' Fin del la validacion de la variable de calidad

                End If
            Else
                Return False
            End If
        End Function
        ''' <summary>
        ''' Obtiene los valores de propiedad de un control WebPart correspondientes a su control EditorPart asociado.
        ''' </summary>
        Public Overrides Sub SyncChanges()
            Dim part As FSQAWebPartPanelCalidad = CType(WebPartToEdit, FSQAWebPartPanelCalidad)
            'part.Controls.Clear()
            Dim i As Integer
            txtTitulo.Text = part.Title
            'part.lblTitulo.Text = part.Title
            txtAncho.Text = part.Width.Value.ToString()
            _wneNumProveedores.Text = IIf(part.NumLineas = 0, "", part.NumLineas)
            lstOperadorPuntuaciones.SelectedValue = part.Operador
            txtPuntuacionDesde.Value = IIf(part.PuntuacionDesdeEsNothing, Nothing, part.PuntuacionDesde)
            txtPuntuacionHasta.Value = IIf(part.PuntuacionHastaEsNothing, Nothing, part.PuntuacionHasta)
            lstOrdenPuntuaciones.SelectedIndex = part.IndiceOrdenPuntuaciones

            lstMaterialQA.SelectedValue = part.MaterialQA
            txtMaterialGS_Den.Text = part.MaterialGSDen
            _hidMaterialGS.Value = part.MaterialGS
            '*************************************************

            'Variables de Calidad
            Dim bEncontrado As Boolean = False
            For Each Nodo As TreeNode In _tvVarCal.Nodes
                If part.VarCal = CType(Nodo.Value, String) Then
                    Nodo.Checked = True
                    Exit For
                End If

                For Each NodoHijos As TreeNode In Nodo.ChildNodes
                    If part.VarCal = CType(NodoHijos.Value, String) Then
                        NodoHijos.Checked = True
                        bEncontrado = True
                        Exit For
                    End If

                    'NodoHijos.Checked = part.buscarElemento(part.ListaUnqas, CType(NodoHijos.Value, String))
                    For Each NodoNietos As TreeNode In NodoHijos.ChildNodes
                        If part.VarCal = CType(NodoNietos.Value, String) Then
                            NodoNietos.Checked = True

                            bEncontrado = True
                            Exit For
                        End If
                    Next
                    If bEncontrado Then Exit For
                Next
                If bEncontrado Then Exit For
            Next

            '*******************************************

            'Unidades de Negocio
            bEncontrado = False
            For Each Nodo As TreeNode In _tvUNQA.Nodes
                If part.UNQA = CType(Nodo.Value, String) Then
                    Nodo.Checked = True
                    Exit For
                End If

                For Each NodoHijos As TreeNode In Nodo.ChildNodes
                    If part.UNQA = CType(NodoHijos.Value, String) Then
                        NodoHijos.Checked = True
                        bEncontrado = True
                        Exit For
                    End If

                    'NodoHijos.Checked = part.buscarElemento(part.ListaUnqas, CType(NodoHijos.Value, String))
                    For Each NodoNietos As TreeNode In NodoHijos.ChildNodes
                        If part.UNQA = CType(NodoNietos.Value, String) Then
                            NodoNietos.Checked = True
                            bEncontrado = True
                            Exit For
                        End If
                    Next
                    If bEncontrado Then Exit For
                Next
                If bEncontrado Then Exit For
            Next
            '***********************************************************

            'Tipo Proveedores
            _rblTipoProveedores.SelectedIndex = part.indiceTipoProveedor
            '(cambiar los hidden por una listbox)?????? ****** !!!!!!!
            _hidCodProves.Value = part.CodProves
            _hidDenProves.Value = part.DenProves

            Dim aux() As String
            Dim aux2() As String
            aux = Split(part.CodProves, "#")
            aux2 = Split(part.DenProves, "#")
            lstProveedores.Items.Clear()
            For i = 0 To UBound(aux)
                If aux(i) <> "" Then lstProveedores.Items.Add(New ListItem(aux2(i), aux(i)))
            Next
            _chkBajaProveedor.Checked = part.MostrarProveedoresBaja
            '******************************

            LstTipoGrafico.SelectedIndex = part.indiceTipoGrafico
            If LstTipoGrafico.SelectedIndex = 1 Then
                If part.desdeMesGrafico <> 0 AndAlso part.desdeAnyoGrafico <> 0 Then
                    _lstMesDesdeGrafico.SelectedValue = part.desdeMesGrafico & "#" & part.desdeAnyoGrafico
                End If
                If part.HastaMesGrafico <> 0 AndAlso part.HastaAnyoGrafico <> 0 Then
                    _lstMesDesdeGrafico.SelectedValue = part.HastaMesGrafico & "#" & part.HastaAnyoGrafico
                End If
            End If

        End Sub


        <Personalizable(), WebBrowsable(), DefaultValue("Panel Calidad")> _
        Private ReadOnly Property txtTitulo() As TextBox
            Get
                EnsureChildControls()
                Return _txtTitulo
            End Get
        End Property

        Private ReadOnly Property txtAncho() As TextBox
            Get
                EnsureChildControls()
                Return _txtAncho
            End Get
        End Property


        Private ReadOnly Property lstOperadorPuntuaciones() As ListBox
            Get
                EnsureChildControls()
                Return _lstOperadorPuntuaciones
            End Get
        End Property

        <Personalizable(), WebBrowsable(), DefaultValue("")> _
        Private ReadOnly Property txtPuntuacionDesde() As Infragistics.Web.UI.EditorControls.WebNumericEditor
            Get
                EnsureChildControls()
                Return _txtPuntuacionDesde
            End Get
        End Property
        <Personalizable(), WebBrowsable(), DefaultValue("")> _
        Private ReadOnly Property txtPuntuacionHasta() As Infragistics.Web.UI.EditorControls.WebNumericEditor
            Get
                EnsureChildControls()
                Return _txtPuntuacionHasta
            End Get
        End Property


        Private ReadOnly Property lstOrdenPuntuaciones() As ListBox
            Get
                EnsureChildControls()
                Return _lstOrdenPuntuaciones
            End Get
        End Property


        Private ReadOnly Property lstMaterialQA() As ListBox
            Get
                EnsureChildControls()
                Return _lstMaterialQA
            End Get
        End Property

        Private ReadOnly Property MaterialGS() As String
            Get
                EnsureChildControls()
                Return _hidMaterialGS.Value
            End Get
        End Property

        Private ReadOnly Property txtMaterialGS_Den() As TextBox
            Get
                EnsureChildControls()
                Return _txtMaterialGS
            End Get
        End Property

        Private ReadOnly Property TipoProveedor() As Integer
            Get
                EnsureChildControls()
                Return _rblTipoProveedores.SelectedIndex
            End Get
        End Property

        Private ReadOnly Property lstProveedores() As ListBox
            Get
                EnsureChildControls()
                Return _lstProveedores
            End Get
        End Property

        Private ReadOnly Property ListaCodigoProveedores() As String
            Get
                EnsureChildControls()
                Return _hidCodProves.Value
            End Get
        End Property

        Private ReadOnly Property ListaNombreProveedores() As String
            Get
                EnsureChildControls()
                Return _hidDenProves.Value
            End Get
        End Property

        Private ReadOnly Property LstTipoGrafico() As ListBox
            Get
                EnsureChildControls()
                Return _lstTipoGrafico
            End Get
        End Property


        ''' <summary>
        ''' Muestra el buscador de articulos
        ''' </summary>
        ''' <param name="sender">objeto q lanza el evento</param>
        ''' <param name="e">argumentos del evento</param>
        ''' <remarks>Llamada desde: Sistema; Tiempomaximo:0</remarks>
        Private Sub _imgLupa2_1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _imgLupa2_1.Click
            Dim pag As FSNServer.IFSNPage = CType(Me.Page, FSNServer.IFSNPage)

            Dim sCodArticuloID As String = ""
            Dim sDenArticuloID As String = ""
            Dim sMaterialBD, sMaterialDen As String

            Dim material() As String
            Dim sMat As String = ""
            If _hidMaterialGS.Value <> "" Then
                material = Split(_hidMaterialGS.Value, "-")
                Dim i As Byte
                Dim lLongCod As Long
                i = 1
                For i = 1 To 4
                    Select Case i
                        Case 1
                            lLongCod = pag.FSNServer.LongitudesDeCodigos.giLongCodGMN1
                        Case 2
                            lLongCod = pag.FSNServer.LongitudesDeCodigos.giLongCodGMN2
                        Case 3
                            lLongCod = pag.FSNServer.LongitudesDeCodigos.giLongCodGMN3
                        Case 4
                            lLongCod = pag.FSNServer.LongitudesDeCodigos.giLongCodGMN4
                    End Select
                    sMat = sMat + Space(lLongCod - Len(material(i - 1))) + material(i - 1)
                Next
            End If

            sCodArticuloID = Me.ClientID & "_" & _txtCodArticulo.ID
            sDenArticuloID = Me.ClientID & "_" & _txtDenArticulo.ID
            sMaterialBD = Me.ClientID & "_" & _hidMaterialGS.ID
            sMaterialDen = Me.ClientID & "_" & _txtMaterialGS.ID
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "Buscador_articulos", "window.open('" & ConfigurationManager.AppSettings("RutaFS") & "_common/BuscadorArticulos.aspx?desde=WebPartPanelCalidad&IdMaterialBD=" & sMaterialBD & "&IdMaterialDen=" & sMaterialDen & "&IdControlCodArt=" & sCodArticuloID & "&IdControlDenArt=" & sDenArticuloID & "&TabContainer=" & Me.ID & "&mat=" & sMat & "', '_blank', 'width=850,height=630,status=yes,resizable=no,top=200,left=200');", True)

        End Sub

        Private Sub ControlEditor_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
            ModoEdicion = True
        End Sub
    End Class

    ''' <summary>
    ''' Devuelve el idioma de la página que alberga el control.
    ''' </summary>
    ''' <returns>El código de idioma.</returns>
    Private Function Idioma() As String
        If Not DesignMode Then
            If Me.Page Is Nothing AndAlso TypeOf Me.Page Is FSNServer.IFSNPage Then
                Return CType(Me.Page, FSNServer.IFSNPage).Idioma
            Else
                For Each x As String In HttpContext.Current.Session.Keys
                    If TypeOf HttpContext.Current.Session(x) Is FSNServer.User Then
                        Return CType(HttpContext.Current.Session(x), FSNServer.User).Idioma
                    End If
                Next
                Return String.Empty
            End If
        Else
            Return String.Empty
        End If
    End Function

    Public ReadOnly Property Textos(ByVal Index As Short) As String
        Get
            If String.IsNullOrEmpty(_Textos(Index)) Or Idioma() <> _sIdioma Then
                If Not DesignMode Then
                    Try
                        Dim FSNDict As FSNServer.Dictionary = New FSNServer.Dictionary()
                        _sIdioma = Idioma()
                        FSNDict.LoadData(FSNLibrary.TiposDeDatos.ModulosIdiomas.Webparts, _sIdioma)

                        _Textos(1) = FSNDict.Data.Tables(0).Rows(19).Item(1) 'Titulo
                        _Textos(2) = FSNDict.Data.Tables(0).Rows(21).Item(1) 'Ancho del elemento
                        _Textos(3) = FSNDict.Data.Tables(0).Rows(159).Item(1) 'Edite el webpart para poder configurar su contenido.

                        For i As Short = 4 To 49
                            _Textos(i) = FSNDict.Data.Tables(0).Rows(i + 175).Item(1)
                        Next
                        _Textos(50) = FSNDict.Data.Tables(0).Rows(236).Item(1)
                        _Textos(51) = FSNDict.Data.Tables(0).Rows(237).Item(1)

                        _Textos(52) = FSNDict.Data.Tables(0).Rows(239).Item(1)
                        _Textos(53) = FSNDict.Data.Tables(0).Rows(254).Item(1)
                        Return _Textos(Index)
                    Catch ex As Exception
                        'Si falla la carga de textos devolvemos la cadena vacia para que la carga de la página siga su curso
                        Return String.Empty
                    End Try
                Else
                    Return String.Empty
                End If
            Else
                Return _Textos(Index)
            End If
        End Get
    End Property

    'Titulo pagina principal
    Public Overrides Property Title() As String
        Get
            If String.IsNullOrEmpty(MyBase.Title) Then
                If String.IsNullOrEmpty(sTitulo) Then
                    If Me.Page Is Nothing And String.IsNullOrEmpty(Textos(47)) Then '"Panel Calidad"
                        MyBase.Title = Textos(47)
                        sTitulo = MyBase.Title
                    Else
                        sTitulo = Textos(47)
                    End If
                End If
                Return sTitulo
            Else
                sTitulo = MyBase.Title
                Return MyBase.Title
            End If
        End Get
        Set(ByVal value As String)
            MyBase.Title = value
        End Set
    End Property

    <Personalizable(), WebBrowsable(), DefaultValue(5)> _
    Public Property NumLineas() As Integer
        Get
            Return mNumLineas
        End Get
        Set(ByVal value As Integer)
            mNumLineas = value
        End Set
    End Property



    <Personalizable(), WebBrowsable(), DefaultValue("")> _
    Public Property Operador() As String
        Get
            Return msOperador
        End Get
        Set(ByVal value As String)
            msOperador = value
        End Set
    End Property


    <Personalizable(), WebBrowsable(), DefaultValue("")> _
    Public Property PuntuacionDesde() As Double
        Get
            Return msPuntuacionDesde
        End Get
        Set(ByVal value As Double)
            msPuntuacionDesde = value
        End Set
    End Property

    <Personalizable(), WebBrowsable(), DefaultValue("")> _
    Public Property PuntuacionHasta() As Double
        Get
            Return msPuntuacionHasta
        End Get
        Set(ByVal value As Double)
            msPuntuacionHasta = value
        End Set
    End Property

    <Personalizable(), WebBrowsable(), DefaultValue("")> _
    Public Property PuntuacionDesdeEsNothing() As Boolean
        Get
            Return msPuntuacionDesdeEsNothing
        End Get
        Set(ByVal value As Boolean)
            msPuntuacionDesdeEsNothing = value
        End Set
    End Property

    <Personalizable(), WebBrowsable(), DefaultValue("")> _
    Public Property PuntuacionHastaEsNothing() As Boolean
        Get
            Return msPuntuacionHastaEsNothing
        End Get
        Set(ByVal value As Boolean)
            msPuntuacionHastaEsNothing = value
        End Set
    End Property

    <Personalizable(), WebBrowsable(), DefaultValue(0)> _
    Public Property IndiceOrdenPuntuaciones() As Integer
        Get
            Return miIndiceOrdenPuntuaciones
        End Get
        Set(ByVal value As Integer)
            miIndiceOrdenPuntuaciones = value
        End Set
    End Property

    <Personalizable(), WebBrowsable(), DefaultValue(0)> _
    Public Property MaterialQA() As Long
        Get
            Return mlIDMaterialQA
        End Get
        Set(ByVal value As Long)
            mlIDMaterialQA = value
        End Set
    End Property

    <Personalizable(), WebBrowsable(), DefaultValue("")> _
    Public Property MaterialGSDen() As String
        Get
            Return msMaterialGSDen
        End Get
        Set(ByVal value As String)
            msMaterialGSDen = value
        End Set
    End Property

    <Personalizable(), WebBrowsable(), DefaultValue("")> _
    Public Property MaterialGS() As String
        Get
            Return msMaterialGS
        End Get
        Set(ByVal value As String)
            msMaterialGS = value
        End Set
    End Property


    <Personalizable(), WebBrowsable(), DefaultValue(0)> _
    Public Property VarCal() As String
        Get
            Return sVarCal
        End Get
        Set(ByVal value As String)
            sVarCal = value
        End Set
    End Property
    <Personalizable(), WebBrowsable(), DefaultValue(0)> _
    Public Property UNQA() As String
        Get
            Return sUNQA
        End Get
        Set(ByVal value As String)
            sUNQA = value
        End Set
    End Property
    <Personalizable(), WebBrowsable(), DefaultValue(0)> _
    Public Property UNQA_PROVE() As String
        Get
            Return sUNQA_PROVE
        End Get
        Set(ByVal value As String)
            sUNQA_PROVE = value
        End Set
    End Property

    <Personalizable(), WebBrowsable(), DefaultValue(1)> _
    Public Property indiceTipoProveedor() As Integer
        Get
            Return miIndiceTipoProveedor
        End Get
        Set(ByVal value As Integer)
            miIndiceTipoProveedor = value
        End Set
    End Property

    <Personalizable(), WebBrowsable(), DefaultValue("")> _
    Public Property CodProves() As String
        Get
            Return msCodProves
        End Get
        Set(ByVal value As String)
            msCodProves = value
        End Set
    End Property

    <Personalizable(), WebBrowsable(), DefaultValue("")> _
    Public Property DenProves() As String
        Get
            Return msDenProves
        End Get
        Set(ByVal value As String)
            msDenProves = value
        End Set
    End Property

    <Personalizable(), WebBrowsable(), DefaultValue(0)> _
    Public Property MostrarProveedoresBaja() As Boolean
        Get
            Return mbMostrarProveedoresBaja
        End Get
        Set(ByVal value As Boolean)
            mbMostrarProveedoresBaja = value
        End Set
    End Property

    <Personalizable(), WebBrowsable(), DefaultValue(0)> _
    Public Property indiceTipoGrafico() As Integer
        Get
            Return miIndiceTipoGrafico
        End Get
        Set(ByVal value As Integer)
            miIndiceTipoGrafico = value
        End Set
    End Property

    <Personalizable(), WebBrowsable(), DefaultValue(0)> _
    Public Property desdeMesGrafico() As Integer
        Get
            Return miDesdeMesGrafico
        End Get
        Set(ByVal value As Integer)
            miDesdeMesGrafico = value
        End Set
    End Property

    <Personalizable(), WebBrowsable(), DefaultValue(0)> _
    Public Property desdeAnyoGrafico() As Integer
        Get
            Return miDesdeAnyoGrafico
        End Get
        Set(ByVal value As Integer)
            miDesdeAnyoGrafico = value
        End Set
    End Property

    <Personalizable(), WebBrowsable(), DefaultValue("")> _
    Public Property HastaMesGrafico() As Integer
        Get
            Return msHastaMesGrafico
        End Get
        Set(ByVal value As Integer)
            msHastaMesGrafico = value
        End Set
    End Property

    <Personalizable(), WebBrowsable(), DefaultValue("")> _
    Public Property HastaAnyoGrafico() As Integer
        Get
            Return msHastaAnyoGrafico
        End Get
        Set(ByVal value As Integer)
            msHastaAnyoGrafico = value
        End Set
    End Property

    <Personalizable(), WebBrowsable(), DefaultValue(1)> _
    Public Property MonstrarMensaje() As Boolean
        Get
            Return mbMostrarMensaje
        End Get
        Set(ByVal value As Boolean)
            mbMostrarMensaje = value
        End Set
    End Property
#Region "Propiedades Estilo Grafico"
    Private _sColorBordeGrafico As String = "#000000"
    Public Property ColorBordeGrafico() As String
        Get
            Return _sColorBordeGrafico
        End Get
        Set(ByVal value As String)
            _sColorBordeGrafico = value
        End Set
    End Property
    Private _sColorGenericoGrafico As String = "#404040"
    Public Property ColorGenericoGrafico() As String
        Get
            Return _sColorGenericoGrafico
        End Get
        Set(ByVal value As String)
            _sColorGenericoGrafico = value
        End Set
    End Property
    Private _sNombreFuenteGrafico As String = "Arial"
    Public Property NombreFuenteGrafico() As String
        Get
            Return _sNombreFuenteGrafico
        End Get
        Set(ByVal value As String)
            _sNombreFuenteGrafico = value
        End Set
    End Property
    Private _iSizeFuenteGrafico As Integer = 8
    Public Property SizeFuenteGrafico() As Integer
        Get
            Return _iSizeFuenteGrafico
        End Get
        Set(ByVal value As Integer)
            _iSizeFuenteGrafico = value
        End Set
    End Property
    Private _sColorCalificacion1 As String = "Gray"
    Public Property ColorCalificacion1() As String
        Get
            Return _sColorCalificacion1
        End Get
        Set(ByVal value As String)
            _sColorCalificacion1 = value
        End Set
    End Property
    Private _sColorCalificacion2 As String = "Red"
    Public Property ColorCalificacion2() As String
        Get
            Return _sColorCalificacion2
        End Get
        Set(ByVal value As String)
            _sColorCalificacion2 = value
        End Set
    End Property
    Private _sColorCalificacion3 As String = "Goldenrod"
    Public Property ColorCalificacion3() As String
        Get
            Return _sColorCalificacion3
        End Get
        Set(ByVal value As String)
            _sColorCalificacion3 = value
        End Set
    End Property
    Private _sColorCalificacion4 As String = "Blue"
    Public Property ColorCalificacion4() As String
        Get
            Return _sColorCalificacion4
        End Get
        Set(ByVal value As String)
            _sColorCalificacion4 = value
        End Set
    End Property
    Private _sColorCalificacion5 As String = "Green"
    Public Property ColorCalificacion5() As String
        Get
            Return _sColorCalificacion5
        End Get
        Set(ByVal value As String)
            _sColorCalificacion5 = value
        End Set
    End Property
    Private _sColorCalificacion6 As String = "Magenta"
    Public Property ColorCalificacion6() As String
        Get
            Return _sColorCalificacion6
        End Get
        Set(ByVal value As String)
            _sColorCalificacion6 = value
        End Set
    End Property
#End Region



    ''' <summary>
    ''' Carga los datos por defecto para el control
    ''' </summary>
    Public Sub New()
        Me.Width = Unit.Pixel(300)
        Me.Title = Textos(47)
        Me.NumLineas = 5
        Me.indiceTipoProveedor = 1
        Me.VarCal = "0-0"
        Me.MaterialGS = ""
        Me.CodProves = ""
        Me.UNQA = -1
        MonstrarMensaje = True
    End Sub

    ''' <summary>
    ''' Lo llama el marco de trabajo de las páginas ASP.NET para indicar a los controles de servidor que utilizan la implementación basada en la composición que creen los controles secundarios que contengan como forma de preparar la devolución o representación de los datos.
    ''' </summary>
    Protected Overrides Sub CreateChildControls()
        'If Not ModoEdicion Then
        Controls.Clear()

        'Tabla para  contener los elementos del webpart
        Dim tabla As HtmlTable = New HtmlTable()
        tabla.Width = Me.Width.Value
        'fila 1
        Dim fila As HtmlTableRow = New HtmlTableRow()
        Dim celda As HtmlTableCell

        fila = New HtmlTableRow
        fila.Height = "55px"
        'Celda 11
        celda = New HtmlTableCell()
        celda.Width = Me.Width.Value - 60
        lblTitulo = New System.Web.UI.WebControls.Label()
        lblTitulo.CssClass = "Rotulo"
        lblTitulo.Text = Me.Title
        lblTitulo.Text = Textos(47)
        celda.Controls.Add(lblTitulo)

        lblMensaje = New System.Web.UI.WebControls.Label
        lblMensaje.CssClass = "Rotulo"
        celda.Controls.Add(lblMensaje)

        celda.Controls.Add(New LiteralControl("<br/>"))

        'Numero de proveedores
        lblNumTotal = New System.Web.UI.WebControls.Label()
        lblNumTotal.CssClass = "EtiquetaGrande"
        celda.Controls.Add(lblNumTotal)

        lblTotal = New System.Web.UI.WebControls.Label()
        lblTotal.CssClass = "EtiquetaGrande"
        celda.Controls.Add(lblTotal)
        fila.Cells.Add(celda)

        'Celda 12
        celda = New HtmlTableCell
        celda.Align = "right"
        celda.VAlign = "bottom"
        celda.Width = 60
        celda.Style.Add("background-image", Me.Page.ClientScript.GetWebResourceUrl(GetType(FSQAWebPartPanelCalidad), "Fullstep.PMWebControls.PanelCalidad.gif"))
        celda.Style.Add("background-position", "top left")
        celda.Style.Add("background-repeat", "no-repeat")
        fila.Cells.Add(celda)
        tabla.Rows.Add(fila)

        fila = New HtmlTableRow
        celda = New HtmlTableCell

        lblError = New System.Web.UI.WebControls.Label
        lblError.Visible = False
        celda.Controls.Add(lblError)
        fila.Cells.Add(celda)
        tabla.Rows.Add(fila)

        If MonstrarMensaje = True Then
            lblTitulo.Visible = False
            lblTotal.Visible = False
            lblNumTotal.Visible = False
            lblMensaje.Text = Textos(3)
            lblMensaje.Visible = True

            Me.Controls.Add(tabla)
        Else
            lblTitulo.Visible = True
            lblTotal.Visible = True
            lblNumTotal.Visible = True
            lblMensaje.Visible = False
            'fila 2
            fila = New HtmlTableRow
            celda = New HtmlTableCell
            celda.ColSpan = 2
            celda.Width = Me.Width.Value
            chartPanelCalidad = New Dundas.Charting.WebControl.Chart
            'Cuidado si no configuramos el grafico antes de añadirlo al webpart, el zoom/scroll via ajax no funcionan, hacen postback de toda la pagina
            ConfigurarChartPanelCalidad()

            celda.Controls.Add(chartPanelCalidad)
            fila.Cells.Add(celda)
            tabla.Rows.Add(fila)

            Me.Controls.Add(tabla)

            If Not Page.IsPostBack Then
                CargarPuntuaciones()
            End If
        End If
    End Sub
    ''' <summary>
    ''' Establece la configuracion incial del grafico previo a saber el tipo del grafico. Es decir las propiedades comunes a mabos tipos de graficos
    ''' </summary>
    ''' <remarks>
    ''' Este metodo antes de añadir el control del chart al webpart, ya que  si el zoom scroll ajax no lo activamos antes de aÃ±adir el chart al webpart no funciona
    ''' </remarks>
    Sub ConfigurarChartPanelCalidad()
        'Estilo del grafico
        chartPanelCalidad.ID = "ChartPC"
        chartPanelCalidad.Width = Me.Width.Value
        chartPanelCalidad.Palette = ChartColorPalette.Dundas
        chartPanelCalidad.BackColor = System.Drawing.Color.LightGray
        chartPanelCalidad.BorderLineStyle = ChartDashStyle.Solid
        chartPanelCalidad.BackGradientType = GradientType.TopBottom
        chartPanelCalidad.BorderLineWidth = 2
        chartPanelCalidad.BorderLineColor = System.Drawing.ColorTranslator.FromHtml(ColorBordeGrafico)
        chartPanelCalidad.BorderSkin.SkinStyle = BorderSkinStyle.Emboss
        'Forma de hacer el render
        'chartPanelCalidad.ImageUrl = "~/TempImages/ChartPic_#SEQ(300,3)"
        'chartPanelCalidad.RenderType = RenderType.ImageTag
        chartPanelCalidad.CallbackStateContent = CallbackStateContent.All
        'Creamos la chartArea, solo va a tener 1 y establecemos su estilo
        Dim chartAreaD As New Dundas.Charting.WebControl.ChartArea
        chartAreaD.Name = "Default"
        'chartAreaD.BorderColor = Color.FromArgb(64, 64, 64, 64)
        chartAreaD.BorderColor = System.Drawing.ColorTranslator.FromHtml(ColorGenericoGrafico)
        chartAreaD.BorderStyle = ChartDashStyle.Solid
        chartAreaD.BackColor = Color.OldLace
        chartAreaD.BackGradientEndColor = Color.White
        chartAreaD.ShadowColor = Color.Transparent

        'Estilo Eje Y
        chartAreaD.AxisY.LineColor = System.Drawing.ColorTranslator.FromHtml(ColorGenericoGrafico)
        chartAreaD.AxisY.MajorGrid.LineColor = System.Drawing.ColorTranslator.FromHtml(ColorGenericoGrafico)
        chartAreaD.AxisY.LabelStyle.Font = New Font(Me.NombreFuenteGrafico, Me.SizeFuenteGrafico, FontStyle.Regular)
        chartAreaD.AxisY.TitleFont = New Font(NombreFuenteGrafico, SizeFuenteGrafico, FontStyle.Bold)
        Dim pag As FSNServer.IFSNPage = CType(Me.Page, FSNServer.IFSNPage)
        'chartAreaD.AxisY.LabelStyle.Format
        'Estilo Eje X
        chartAreaD.AxisX.LineColor = System.Drawing.ColorTranslator.FromHtml(ColorGenericoGrafico)
        chartAreaD.AxisX.MajorGrid.LineColor = System.Drawing.ColorTranslator.FromHtml(ColorGenericoGrafico)
        chartAreaD.AxisX.LabelStyle.Font = New Font(Me.NombreFuenteGrafico, Me.SizeFuenteGrafico, FontStyle.Regular)
        chartAreaD.AxisX.TitleFont = New Font(NombreFuenteGrafico, SizeFuenteGrafico, FontStyle.Bold)
        'chartAreaD.AxisX.LabelStyle.TruncatedLabels = True

        'Añadimos la charArea al grafico
        chartPanelCalidad.ChartAreas.Add(chartAreaD)

        'scrolling and zooming will force keeping of series data between callbacks.
        chartPanelCalidad.ChartAreas("Default").CursorX.UserEnabled = True
        chartPanelCalidad.ChartAreas("Default").AxisX.View.Position = 0
        '¡¡SIZE EN FUNCION DEL WIDTH DEL WEBPART
        chartPanelCalidad.ChartAreas("Default").AxisX.View.Size = 20
        chartPanelCalidad.ChartAreas("Default").AxisY.Title = Textos(27)
        'Set restriction on how far the user can zoom in
        'chartPanelCalidad.ChartAreas("Default").AxisX.View.MinSize = 5
        chartPanelCalidad.AJAXZoomEnabled = True

        chartPanelCalidad.AntiAliasing = AntiAliasing.All
        chartPanelCalidad.TextAntiAliasingQuality = TextAntiAliasingQuality.High


    End Sub

    ''' <summary>
    ''' Refresca los datos del webpart
    ''' </summary>
    Public Sub Actualizar()
        CreateChildControls()
        If Not MonstrarMensaje Then
            CargarPuntuaciones()
        End If
    End Sub

    ''' <summary>
    ''' Envía el contenido del control de servidor a un objeto HtmlTextWriter que se proporciona y almacena la información de seguimiento sobre el control si dicho seguimiento está habilitado.
    ''' </summary>
    ''' <param name="writer">Objeto HTmlTextWriter que recibe el contenido del control.</param>
    Public Overrides Sub RenderControl(ByVal writer As System.Web.UI.HtmlTextWriter)
        MyBase.RenderControl(writer)
    End Sub

    ''' <summary>
    ''' Obtiene el dataset con los proveedores/puntuaciones que resultan de aplicar la configuracion del webpart
    ''' En funcion del tipo de grafico llama a CargarPuntuacionesChartPanelCalidad/CargarPuntuacionesChartPanelCalidadEvolucion
    ''' para que dibujen el grafico
    ''' </summary>
    ''' <remarks>Llamadas desde: ApplyChanges // CreateChildControls;Tiempo Ejecucion: </remarks>
    Private Sub CargarPuntuaciones()
        If Not Me.Page Is Nothing AndAlso TypeOf Me.Page Is FSNServer.IFSNPage Then
            Dim pag As FSNServer.IFSNPage = CType(Me.Page, FSNServer.IFSNPage)
            Dim FSWSServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim NumTotal As Long = 0
            Dim oProveedores As FSNServer.Proveedores
            Dim dsPuntuaciones As DataSet
            Dim sAux() As String
            sAux = Split(Me.VarCal, "-")
            Dim articulo As String = ""

            Try
                oProveedores = FSWSServer.Get_Object(GetType(FSNServer.Proveedores))
                oProveedores.CargarPuntuaciones(sAux(0), sAux(1), MaterialQA, MaterialGS, articulo, UNQA, UNQA_PROVE, _
                    indiceTipoProveedor, CodProves, MostrarProveedoresBaja, indiceTipoGrafico, _
                    IIf(desdeMesGrafico = 12, 1, desdeMesGrafico + 1), IIf(desdeMesGrafico = 12, desdeAnyoGrafico + 1, desdeAnyoGrafico), _
                    IIf(HastaMesGrafico = 12, 1, HastaMesGrafico + 1), IIf(HastaMesGrafico = 12, HastaAnyoGrafico + 1, HastaAnyoGrafico), _
                    IndiceOrdenPuntuaciones, pag.Usuario.Cod, pag.Usuario.QARestProvMaterial, pag.Usuario.QARestProvEquipo, _
                    pag.Usuario.QARestProvContacto, PuntuacionDesde, Me.PuntuacionDesdeEsNothing, PuntuacionHasta, _
                    Me.PuntuacionHastaEsNothing, Operador, pag.Idioma, NumLineas)
                dsPuntuaciones = oProveedores.Data
                oProveedores = Nothing
                If dsPuntuaciones.Tables.Count > 0 Then
                    If Me.indiceTipoGrafico = 0 Then
                        NumTotal = dsPuntuaciones.Tables(0).Rows.Count
                        CargarPuntuacionesChartPanelCalidad(dsPuntuaciones)
                    Else
                        NumTotal = CargarPuntuacionesChartPanelCalidadEvolucion(dsPuntuaciones)
                    End If
                Else
                    NumTotal = 0
                    chartPanelCalidad.Visible = False
                End If
                lblNumTotal.Text = NumTotal & " " & _Textos(37) '"proveedores"

            Catch ex As Exception
                lblError.Text = Textos(52)
                lblError.Visible = True
            End Try
        End If
    End Sub
    ''' <summary>
    ''' Diibuja el grafico de evolucion depuntuacion en el tiempo de proveedores los proveedores.
    ''' </summary>
    ''' <param name="dsPuntuaciones">Dataset con la tabla de proveedores, con las puntauciones obtenidas en diferentes fechas</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function CargarPuntuacionesChartPanelCalidadEvolucion(ByVal dsPuntuaciones As DataSet) As Integer
        chartPanelCalidad.ChartAreas("Default").AxisX.View.Size = 8
        chartPanelCalidad.Height = 400

        chartPanelCalidad.Legends(0).CustomItems.Clear()

        chartPanelCalidad.Series.Clear()
        chartPanelCalidad.DataBindCrossTab(dsPuntuaciones.Tables(0).DefaultView, "PROVE_DEN", "FECHA", "PUNT", "ToolTip=PROVE_DEN")

        For Each Serie As Series In chartPanelCalidad.Series
            Serie.ShadowOffset = 2
            Serie.BorderWidth = 2
            Serie.Type = SeriesChartType.Line
            Serie.ShowInLegend = True
        Next

        chartPanelCalidad.Legends(0).AutoFitText = False
        chartPanelCalidad.Legends(0).Name = "Default"
        chartPanelCalidad.Legends(0).BackColor = Color.Transparent
        chartPanelCalidad.Legends(0).LegendStyle = LegendStyle.Table
        chartPanelCalidad.Legends(0).TableStyle = LegendTableStyle.Auto
        chartPanelCalidad.Legends(0).Docking = LegendDocking.Bottom
        chartPanelCalidad.Legends(0).Alignment = StringAlignment.Center
        chartPanelCalidad.Legends(0).BorderColor = Color.FromArgb(64, 64, 64, 64)
        chartPanelCalidad.Legends(0).BorderWidth = 1
        chartPanelCalidad.ChartAreas("Default").AxisX.Title = Textos(50)

        Return chartPanelCalidad.Series.Count

    End Function
    ''' <summary>
    ''' Dibuja un grafico de barras con una barra por cada proveedor, el valor de la barra ira en funcion de la puntuacion, y el color en funciÃ³n de la calificacion
    ''' </summary>
    ''' <param name="dsPuntuaciones">Dataset con 2 tablas, una con losproveedores/puntuaciones/calificaciones y otra con las posibles calificaciones</param>
    ''' <remarks></remarks>
    Private Sub CargarPuntuacionesChartPanelCalidad(ByVal dsPuntuaciones As DataSet)
        chartPanelCalidad.ChartAreas("Default").AxisX.View.Size = 20
        chartPanelCalidad.Height = 400
        'SERIE---------------------------------------------------------------------------------------------------------------------------
        chartPanelCalidad.Series.Clear()

        Dim seriePC As New Dundas.Charting.WebControl.Series
        seriePC.Name = "PuntuacionesProveedores"
        seriePC.Type = SeriesChartType.Column
        seriePC.BorderColor = Color.FromArgb(180, 26, 59, 105)
        seriePC.BackGradientType = GradientType.DiagonalLeft
        seriePC.ShowInLegend = False


        chartPanelCalidad.Series.Add(seriePC)
        chartPanelCalidad.Series("PuntuacionesProveedores")("PointWidth") = "1"
        chartPanelCalidad.Series("PuntuacionesProveedores").Points.DataBind(dsPuntuaciones.Tables(0).DefaultView, "PROVE_COD", "PUNT", "NombreProve=PROVE_DEN,Calificacion=CALIFICACION,CodProve=PROVE_COD")

        'LEYENDA POR CALIFICACIONES---------------------------------------------------------------------------------------------------------------------------
        Dim BackGradientEndColors() As Color = {Color.FromName("Dark" & ColorCalificacion1), Color.FromName("Dark" & ColorCalificacion2), Color.FromName("Dark" & ColorCalificacion3), Color.FromName("Dark" & ColorCalificacion4), Color.FromName("Dark" & ColorCalificacion5), Color.FromName("Dark" & ColorCalificacion6)}
        Dim Colores() As Color = {Color.FromName(ColorCalificacion1), Color.FromName(ColorCalificacion2), Color.FromName(ColorCalificacion3), Color.FromName(ColorCalificacion4), Color.FromName(ColorCalificacion5), Color.FromName(ColorCalificacion6)}
        chartPanelCalidad.Legends.Clear()
        Dim LegendD As New Dundas.Charting.WebControl.Legend
        LegendD.Enabled = True
        LegendD.AutoFitText = False
        LegendD.Name = "Default"
        LegendD.BackColor = Color.Transparent
        LegendD.LegendStyle = LegendStyle.Table
        LegendD.TableStyle = LegendTableStyle.Auto
        LegendD.Docking = LegendDocking.Bottom
        LegendD.Alignment = StringAlignment.Center
        LegendD.BorderColor = Color.FromArgb(64, 64, 64, 64)
        LegendD.BorderWidth = 1
        Dim pag As FSNServer.IFSNPage = CType(Me.Page, FSNServer.IFSNPage)
        If dsPuntuaciones.Tables.Count > 1 AndAlso dsPuntuaciones.Tables(1).Rows.Count > 0 Then
            Dim iColor As Integer = 0
            Dim totalProveCal As Integer
            Dim totalProve As Integer = dsPuntuaciones.Tables(0).Rows.Count
            For Each RowCal As DataRow In dsPuntuaciones.Tables(1).Rows
                totalProveCal = 0
                For Each Point As DataPoint In chartPanelCalidad.Series("PuntuacionesProveedores").Points
                    If Point("Calificacion") = RowCal("DEN") Then
                        Point.Color = Colores(iColor)
                        If Point("NombreProve").Length > 10 Then
                            Point.AxisLabel = Left(Point("NombreProve"), 10) & "..."
                        Else
                            Point.AxisLabel = Point("NombreProve")
                        End If
                        Point.ToolTip = Point("CodProve") & " - " & Point("NombreProve") & ControlChars.Lf & "Puntuación: " & FSNLibrary.FormatNumber(Point.YValues(0), pag.Usuario.NumberFormat)
                        Point.BackGradientEndColor = BackGradientEndColors(iColor)
                        totalProveCal = totalProveCal + 1
                    End If
                Next
                LegendD.CustomItems.Add(Colores(iColor), RowCal("DEN") & "(" & IIf(totalProveCal = 0, "0", FSNLibrary.FormatNumber(Math.Round(totalProveCal * 100 / totalProve, 2), pag.Usuario.NumberFormat)) & "%)")
                iColor = iColor + 1
            Next
            Dim bSinCalificacion As Boolean = False
            totalProveCal = 0
            For Each Point As DataPoint In chartPanelCalidad.Series("PuntuacionesProveedores").Points
                If Point.ToolTip = "" Then
                    bSinCalificacion = True
                    Point.Color = Color.White
                    If Point("NombreProve").Length > 10 Then
                        Point.AxisLabel = Left(Point("NombreProve"), 10) & "..."
                    Else
                        Point.AxisLabel = Point("NombreProve")
                    End If
                    Point.ToolTip = Point("CodProve") & " - " & Point("NombreProve") & ControlChars.Lf & "Puntuación: " & FSNLibrary.FormatNumber(Point.YValues(0), pag.Usuario.NumberFormat)
                    Point.BackGradientEndColor = Color.WhiteSmoke
                    totalProveCal = totalProveCal + 1
                End If
            Next
            If bSinCalificacion Then
                LegendD.CustomItems.Add(Color.White, Textos(51) & "(" & FSNLibrary.FormatNumber(Math.Round(totalProveCal * 100 / totalProve, 2), pag.Usuario.NumberFormat) & "%)")
            End If
        End If

        chartPanelCalidad.Legends.Add(LegendD)

        chartPanelCalidad.ChartAreas("Default").AxisX.Title = Textos(37)
        chartPanelCalidad.ChartAreas("Default").AxisX.TitleFont = New Font("Arial", 8, FontStyle.Bold)
        '----Etqiuetas Eje X
        '¡¡FORMATEO DE LABELS DE LOS VALORES DEL EJE X
        chartPanelCalidad.ChartAreas("Default").AxisX.LabelStyle.Enabled = True
        chartPanelCalidad.ChartAreas("Default").AxisX.LabelsAutoFit = False
        'chartPanelCalidad.ChartAreas("Default").AxisX.LabelStyle.FontAngle = -45
        chartPanelCalidad.ChartAreas("Default").AxisX.LabelStyle.FontAngle = 90
        chartPanelCalidad.ChartAreas("Default").AxisX.Interval = 1
        chartPanelCalidad.ChartAreas("Default").AxisX.LabelStyle.OffsetLabels = False

    End Sub

    ''' <summary>
    ''' Devuelve una colección de controles EditorPart personalizados que pueden utilizarse para editar un control WebPart cuando se encuentra en modo de edición.
    ''' </summary>
    ''' <returns>Una colección EditorPartCollection que contiene controles EditorPart personalizados asociados a un control WebPart.</returns>
    Public Overrides Function CreateEditorParts() As System.Web.UI.WebControls.WebParts.EditorPartCollection
        Dim editorArray As New ArrayList()
        Dim edPart As New ControlEditor()
        edPart.ID = Me.ID & "_editor1"
        'edPart.Width = 600
        editorArray.Add(edPart)
        Dim editorParts As New EditorPartCollection(editorArray)
        Return editorParts
    End Function

    Public Overrides ReadOnly Property WebBrowsableObject() As Object
        Get
            Return Me
        End Get
    End Property

    ''' <summary>
    ''' Registra el control como un control cuyo estado se debe conservar.
    ''' </summary>
    ''' <param name="e">Objeto EventArgs que contiene los datos del evento.</param>
    Protected Overrides Sub OnInit(ByVal e As System.EventArgs)
        MyBase.OnInit(e)
        Page.RegisterRequiresControlState(Me)
    End Sub

    ''' <summary>
    ''' Guarda los cambios de estado del control.
    ''' </summary>
    ''' <returns>Devuelve el estado actual del control de servidor.</returns>
    Protected Overrides Function SaveControlState() As Object
        Dim obj As Object = MyBase.SaveControlState()
        If obj IsNot Nothing Then
            obj = New Pair(obj, _Textos)
        Else
            obj = _Textos
        End If
        obj = New Pair(obj, _sIdioma)
        Return obj
    End Function

    ''' <summary>
    ''' Restaura información de estado del control.
    ''' </summary>
    ''' <param name="state">Object que representa el estado del control que se va a restaurar.</param>
    Protected Overrides Sub LoadControlState(ByVal state As Object)
        If (state IsNot Nothing) Then
            Dim p As Pair = TryCast(state, Pair)
            _sIdioma = p.Second
            state = p.First
            p = TryCast(state, Pair)
            If p IsNot Nothing Then
                MyBase.LoadControlState(p.First)
                _Textos = CType(p.Second, String())
            Else
                If (TypeOf (state) Is String()) Then
                    _Textos = CType(state, String())
                Else
                    MyBase.LoadControlState(state)
                End If
            End If
        End If
    End Sub

    Private Sub FSQAWebPartPanelCalidad_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.DesignMode Then
            ScriptManager.RegisterClientScriptResource(Me.Page, GetType(QAWebPartClientScript), "Fullstep.PMWebControls.FSQAWebPartPanelCalidad.js")
            ScriptManager.RegisterClientScriptResource(Me.Page, GetType(QAWebPartClientScript), "Fullstep.PMWebControls.FSQAWebPartsQA.js")
        End If

    End Sub

    'Añado esto para que en Chrome y Safari se pueda seleccionar un proveedor para elimnincar.
    Private Sub FSQAWebPartPanelCalidad_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
        Dim sBrowser = HttpContext.Current.Request.Browser.Browser
        If sBrowser <> "InternetExplorer" And sBrowser <> "IE" Then
            If Not Page.ClientScript.IsStartupScriptRegistered(Me.GetType(), "ScriptCargando") Then
			   Dim sScript = "$('[id$=lstProveedores] option').live('click', function () {" & vbCrLf & _
							"if ($(this).attr('selected') == '' || typeof ($(this).attr('selected')) == 'undefined') {" & vbCrLf & _
								"$(this).attr('selected', 'selected');" & vbCrLf & _
							"} else { $(this).removeAttr('selected') }" & vbCrLf & _
						"});"
				ScriptManager.RegisterStartupScript(Me, Me.GetType(), "ScriptCargando", sScript, True)
			end if
        End If
    End Sub
End Class
