﻿Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.HtmlControls
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.ComponentModel
Imports System.Configuration
Public Class FSPMWebPartOtrasURLs
    Inherits WebPart

    Private lblTitulo As Label
    Private lblMensaje As Label
    Private lblError As Label
    Private mURLsSeleccionadas As String
    Private mURLsSeleccionadasArray() As String
    Private mDatosURLsSeleccionadasArray() As String
    Private sTitulo As String
    Private _Textos As DataSet
    Private _sIdioma As String

#Region "Propiedades"

    Public Overrides ReadOnly Property WebBrowsableObject() As Object
        Get
            Return Me
        End Get
    End Property

    <Personalizable(), WebBrowsable(), DefaultValue("")> _
    Public Property URLsSeleccionadas() As String
        Get
            Return mURLsSeleccionadas
        End Get
        Set(ByVal value As String)
            mURLsSeleccionadas = value
        End Set
    End Property


    Public ReadOnly Property Textos(ByVal Index As Short) As String
        Get
            If _Textos Is Nothing Or Idioma() <> _sIdioma Then
                If Not DesignMode Then
                    Dim FSPMDict As New FSNServer.Dictionary
                    _sIdioma = Idioma()
                    If HttpContext.Current.Cache("Textos_" & _sIdioma & "_" & FSNLibrary.TiposDeDatos.ModulosIdiomas.Webparts) Is Nothing Then
                        FSPMDict.LoadData(FSNLibrary.TiposDeDatos.ModulosIdiomas.Webparts, _sIdioma)
                        HttpContext.Current.Cache.Insert("Textos_" & _sIdioma & "_" & FSNLibrary.TiposDeDatos.ModulosIdiomas.Webparts, FSPMDict.Data, Nothing, Caching.Cache.NoAbsoluteExpiration,
                                                         New TimeSpan(0, ConfigurationManager.AppSettings("TiempoExpiracionCacheMin"), 0))
                    End If
                    _Textos = HttpContext.Current.Cache("Textos_" & _sIdioma & "_" & FSNLibrary.TiposDeDatos.ModulosIdiomas.Webparts)
                    Return _Textos.Tables(0).Rows(Index).Item(1)
                Else
                    Return String.Empty
                End If
            Else
                Return _Textos.Tables(0).Rows(Index).Item(1)
            End If
        End Get
    End Property

    Public Overrides Property Title() As String
        Get
            If String.IsNullOrEmpty(MyBase.Title) Then
                If String.IsNullOrEmpty(sTitulo) Then
                    If Me.Page Is Nothing And String.IsNullOrEmpty(Textos(241)) Then
                        MyBase.Title = Textos(241)
                        sTitulo = MyBase.Title
                    Else
                        sTitulo = Textos(241)
                    End If
                End If
                Return sTitulo
            Else
                sTitulo = MyBase.Title
                Return MyBase.Title
            End If
        End Get
        Set(ByVal value As String)
            MyBase.Title = value
        End Set
    End Property
#End Region

    ''' <summary>
    ''' Lo llama el marco de trabajo de las páginas ASP.NET para indicar a los controles de servidor que utilizan la implementación basada en la composición que creen los controles secundarios que contengan como forma de preparar la devolución o representación de los datos.
    ''' </summary>
    Protected Overrides Sub CreateChildControls()
        Controls.Clear()
        Dim img As HtmlImage = New HtmlImage()
        img.Src = Me.Page.ClientScript.GetWebResourceUrl(GetType(FSPMWebPartOtrasURLs), "Fullstep.PMWebControls.trans.gif")
        img.Width = 300
        img.Height = 1
        Me.Controls.Add(img)
        Dim tabla As HtmlTable = New HtmlTable()
        tabla.Width = "100%"
        tabla.Height = "55px"
        Dim fila As HtmlTableRow
        Dim celda As HtmlTableCell

        fila = New HtmlTableRow
        celda = New HtmlTableCell
        celda.ColSpan = 2

        Dim Lista As New HtmlControls.HtmlGenericControl("UL")
        Lista.Style.Add("list-style", "none")

        Dim colAppSettings As System.Collections.Specialized.NameValueCollection

        colAppSettings = System.Configuration.ConfigurationManager.AppSettings

        mURLsSeleccionadasArray = Split(mURLsSeleccionadas, "#")

        If mURLsSeleccionadasArray(0) Is Nothing OrElse mURLsSeleccionadasArray(0) = "" Then
            Dim CargarEnlace As Boolean = False
            Dim sPatronTexto As String = "webpartURL_nom*"

            For Each sKey As String In colAppSettings
                Dim ElementoLista As HtmlControls.HtmlGenericControl
                Dim Enlace As HyperLink = Nothing

                If CargarEnlace Then
                    Enlace.NavigateUrl = colAppSettings.Get(sKey)
                    Enlace.Target = "_blank"
					Enlace.Style.Add("text-decoration", "none")
                    ElementoLista = New HtmlControls.HtmlGenericControl("LI")
                    ElementoLista.Style.Add("margin-bottom", "4px")
                    ElementoLista.Controls.Add(Enlace)
                    Lista.Controls.Add(ElementoLista)
                    CargarEnlace = False
                End If
                If sKey Like sPatronTexto Then
                    Enlace = New HyperLink
                    Enlace.Text = colAppSettings.Get(sKey)
                    CargarEnlace = True
                End If
            Next
        Else
            'Si ya se han seleccionado las aplicaciones que se quieren mostrar o se han añadido nuevas
            For i = 0 To mURLsSeleccionadasArray.GetLength(0) - 1
                Dim ElementoLista As HtmlControls.HtmlGenericControl
                Dim Enlace As HyperLink
                mDatosURLsSeleccionadasArray = Split(mURLsSeleccionadasArray(i), ",")
                If mDatosURLsSeleccionadasArray(0) = "1" Then 'Si el check esta marcado indicando que se debe mostrar
                    Enlace = New HyperLink
					Enlace.Target = "_blank"
                    Enlace.NavigateUrl = mDatosURLsSeleccionadasArray(2) 'Url de la aplicacion
                    Enlace.Text = mDatosURLsSeleccionadasArray(1)
                    Enlace.Style.Add("text-decoration", "none")
                    ElementoLista = New HtmlControls.HtmlGenericControl("LI")
                    ElementoLista.Style.Add("margin-bottom", "4px")
                    ElementoLista.Controls.Add(Enlace)
                    Lista.Controls.Add(ElementoLista)
                End If
            Next
        End If


        celda.Controls.Add(Lista)

        fila.Cells.Add(celda)
        tabla.Rows.Add(fila)
        Me.Controls.Add(tabla)
    End Sub

    ''' <summary>
    ''' Guarda los cambios de estado del control.
    ''' </summary>
    ''' <returns>Devuelve el estado actual del control de servidor.</returns>
    Protected Overrides Function SaveControlState() As Object
        Dim obj As Object = MyBase.SaveControlState()
        If obj IsNot Nothing Then
            obj = New Pair(obj, _Textos)
        Else
            obj = _Textos
        End If
        obj = New Pair(obj, _sIdioma)
        Return obj
    End Function

    ''' <summary>
    ''' Envía el contenido del control de servidor a un objeto HtmlTextWriter que se proporciona y almacena la información de seguimiento sobre el control si dicho seguimiento está habilitado.
    ''' </summary>
    ''' <param name="writer">Objeto HTmlTextWriter que recibe el contenido del control.</param>
    Public Overrides Sub RenderControl(ByVal writer As System.Web.UI.HtmlTextWriter)
        MyBase.RenderControl(writer)
    End Sub



    ''' <summary>
    ''' Devuelve el idioma de la página que alberga el control.
    ''' </summary>
    ''' <returns>El código de idioma.</returns>
    Private Function Idioma() As String
        If Not DesignMode Then
            If Me.Page Is Nothing AndAlso TypeOf Me.Page Is FSNServer.IFSNPage Then
                Return CType(Me.Page, FSNServer.IFSNPage).Idioma
            Else
                For Each x As String In HttpContext.Current.Session.Keys
                    If TypeOf HttpContext.Current.Session(x) Is FSNServer.User Then
                        Return CType(HttpContext.Current.Session(x), FSNServer.User).Idioma
                    End If
                Next
                Return String.Empty
            End If
        Else
            Return String.Empty
        End If
    End Function

    Private Class ControlEditor
        Inherits EditorPart

        Private _txtTitulo As TextBox
        Private _txtAncho As TextBox
        Private _reqAncho As RequiredFieldValidator
        Private _ranAncho As RangeValidator
		Private _regValidator1 As RegularExpressionValidator
        Private _regValidator2 As RegularExpressionValidator
        Private _regValidator3 As RegularExpressionValidator
        Private _regValidator4 As RegularExpressionValidator
        Private _regValidator5 As RegularExpressionValidator
        Private _regValidator6 As RegularExpressionValidator
        Private _regValidator7 As RegularExpressionValidator
        Private _regValidator8 As RegularExpressionValidator
        Private _regValidator9 As RegularExpressionValidator
        Private _regValidator10 As RegularExpressionValidator
        Private _chkAplicacion1 As CheckBox
        Private _chkAplicacion2 As CheckBox
        Private _chkAplicacion3 As CheckBox
        Private _chkAplicacion4 As CheckBox
        Private _chkAplicacion5 As CheckBox
        Private _chkAplicacion6 As CheckBox
        Private _chkAplicacion7 As CheckBox
        Private _chkAplicacion8 As CheckBox
        Private _chkAplicacion9 As CheckBox
        Private _chkAplicacion10 As CheckBox
        Private _txtNomAplicacion1 As TextBox
        Private _txtNomAplicacion2 As TextBox
        Private _txtNomAplicacion3 As TextBox
        Private _txtNomAplicacion4 As TextBox
        Private _txtNomAplicacion5 As TextBox
        Private _txtNomAplicacion6 As TextBox
        Private _txtNomAplicacion7 As TextBox
        Private _txtNomAplicacion8 As TextBox
        Private _txtNomAplicacion9 As TextBox
        Private _txtNomAplicacion10 As TextBox
        Private _txtURLAplicacion1 As TextBox
        Private _txtURLAplicacion2 As TextBox
        Private _txtURLAplicacion3 As TextBox
        Private _txtURLAplicacion4 As TextBox
        Private _txtURLAplicacion5 As TextBox
        Private _txtURLAplicacion6 As TextBox
        Private _txtURLAplicacion7 As TextBox
        Private _txtURLAplicacion8 As TextBox
        Private _txtURLAplicacion9 As TextBox
        Private _txtURLAplicacion10 As TextBox

#Region "Propiedades"
        Private ReadOnly Property chkAplicacion1() As CheckBox
            Get
                EnsureChildControls()
                Return _chkAplicacion1
            End Get
        End Property

        Private ReadOnly Property chkAplicacion2() As CheckBox
            Get
                EnsureChildControls()
                Return _chkAplicacion2
            End Get
        End Property

        Private ReadOnly Property chkAplicacion3() As CheckBox
            Get
                EnsureChildControls()
                Return _chkAplicacion3
            End Get
        End Property

        Private ReadOnly Property chkAplicacion4() As CheckBox
            Get
                EnsureChildControls()
                Return _chkAplicacion4
            End Get
        End Property

        Private ReadOnly Property chkAplicacion5() As CheckBox
            Get
                EnsureChildControls()
                Return _chkAplicacion5
            End Get
        End Property

        Private ReadOnly Property chkAplicacion6() As CheckBox
            Get
                EnsureChildControls()
                Return _chkAplicacion6
            End Get
        End Property

        Private ReadOnly Property chkAplicacion7() As CheckBox
            Get
                EnsureChildControls()
                Return _chkAplicacion7
            End Get
        End Property

        Private ReadOnly Property chkAplicacion8() As CheckBox
            Get
                EnsureChildControls()
                Return _chkAplicacion8
            End Get
        End Property

        Private ReadOnly Property chkAplicacion9() As CheckBox
            Get
                EnsureChildControls()
                Return _chkAplicacion9
            End Get
        End Property

        Private ReadOnly Property chkAplicacion10() As CheckBox
            Get
                EnsureChildControls()
                Return _chkAplicacion10
            End Get
        End Property

        Private ReadOnly Property txtNomAplicacion1() As TextBox
            Get
                EnsureChildControls()
                Return _txtNomAplicacion1
            End Get
        End Property

        Private ReadOnly Property txtNomAplicacion2() As TextBox
            Get
                EnsureChildControls()
                Return _txtNomAplicacion2
            End Get
        End Property

        Private ReadOnly Property txtNomAplicacion3() As TextBox
            Get
                EnsureChildControls()
                Return _txtNomAplicacion3
            End Get
        End Property

        Private ReadOnly Property txtNomAplicacion4() As TextBox
            Get
                EnsureChildControls()
                Return _txtNomAplicacion4
            End Get
        End Property

        Private ReadOnly Property txtNomAplicacion5() As TextBox
            Get
                EnsureChildControls()
                Return _txtNomAplicacion5
            End Get
        End Property

        Private ReadOnly Property txtNomAplicacion6() As TextBox
            Get
                EnsureChildControls()
                Return _txtNomAplicacion6
            End Get
        End Property

        Private ReadOnly Property txtNomAplicacion7() As TextBox
            Get
                EnsureChildControls()
                Return _txtNomAplicacion7
            End Get
        End Property

        Private ReadOnly Property txtNomAplicacion8() As TextBox
            Get
                EnsureChildControls()
                Return _txtNomAplicacion8
            End Get
        End Property

        Private ReadOnly Property txtNomAplicacion9() As TextBox
            Get
                EnsureChildControls()
                Return _txtNomAplicacion9
            End Get
        End Property

        Private ReadOnly Property txtNomAplicacion10() As TextBox
            Get
                EnsureChildControls()
                Return _txtNomAplicacion10
            End Get
        End Property

        Private ReadOnly Property txtUrlAplicacion1() As TextBox
            Get
                EnsureChildControls()
                Return _txtURLAplicacion1
            End Get
        End Property

        Private ReadOnly Property txtUrlAplicacion2() As TextBox
            Get
                EnsureChildControls()
                Return _txtURLAplicacion2
            End Get
        End Property

        Private ReadOnly Property txtUrlAplicacion3() As TextBox
            Get
                EnsureChildControls()
                Return _txtURLAplicacion3
            End Get
        End Property

        Private ReadOnly Property txtUrlAplicacion4() As TextBox
            Get
                EnsureChildControls()
                Return _txtURLAplicacion4
            End Get
        End Property

        Private ReadOnly Property txtUrlAplicacion5() As TextBox
            Get
                EnsureChildControls()
                Return _txtURLAplicacion5
            End Get
        End Property

        Private ReadOnly Property txtUrlAplicacion6() As TextBox
            Get
                EnsureChildControls()
                Return _txtURLAplicacion6
            End Get
        End Property

        Private ReadOnly Property txtUrlAplicacion7() As TextBox
            Get
                EnsureChildControls()
                Return _txtURLAplicacion7
            End Get
        End Property

        Private ReadOnly Property txtUrlAplicacion8() As TextBox
            Get
                EnsureChildControls()
                Return _txtURLAplicacion8
            End Get
        End Property

        Private ReadOnly Property txtUrlAplicacion9() As TextBox
            Get
                EnsureChildControls()
                Return _txtURLAplicacion9
            End Get
        End Property

        Private ReadOnly Property txtUrlAplicacion10() As TextBox
            Get
                EnsureChildControls()
                Return _txtURLAplicacion10
            End Get
        End Property

        Private ReadOnly Property txtTitulo() As TextBox
            Get
                EnsureChildControls()
                Return _txtTitulo
            End Get
        End Property

        Private ReadOnly Property txtAncho() As TextBox
            Get
                EnsureChildControls()
                Return _txtAncho
            End Get
        End Property
#End Region



        ''' <summary>
        ''' Lo llama el marco de trabajo de las páginas ASP.NET para indicar a los controles de servidor que utilizan la implementación basada en la composición que creen los controles secundarios que contengan como forma de preparar la devolución o representación de los datos.
        ''' </summary>
        Protected Overrides Sub CreateChildControls()
            Dim part As FSPMWebPartOtrasURLs = CType(WebPartToEdit, FSPMWebPartOtrasURLs)
            Controls.Clear()

            _txtTitulo = New TextBox()
            _txtTitulo.Width = Unit.Pixel(250)
            _txtTitulo.Text = part.Title
            Controls.Add(_txtTitulo)
            _txtAncho = New TextBox()
            _txtAncho.MaxLength = 3
            _txtAncho.Width = Unit.Pixel(30)
            _txtAncho.Text = part.Width.Value.ToString()
            _txtAncho.ID = Me.ID & "_txtAncho"
            Controls.Add(_txtAncho)
            _reqAncho = New RequiredFieldValidator()
            _reqAncho.ID = _txtAncho.ID & "_Req"
            _reqAncho.ErrorMessage = "*"
            _reqAncho.ControlToValidate = _txtAncho.ID
            Controls.Add(_reqAncho)
            _ranAncho = New RangeValidator()
            _ranAncho.ID = _txtAncho.ID & "_Ran"
            _ranAncho.Type = ValidationDataType.Integer
            _ranAncho.MaximumValue = 500
            _ranAncho.MinimumValue = 200
            _ranAncho.ErrorMessage = "(200-500)"
            _ranAncho.ControlToValidate = _txtAncho.ID
            Controls.Add(_ranAncho)


            'Añadimos los controles
            _chkAplicacion1 = New CheckBox
            Controls.Add(_chkAplicacion1)

            _txtNomAplicacion1 = New TextBox
            Controls.Add(_txtNomAplicacion1)

            _txtURLAplicacion1 = New TextBox
            _txtURLAplicacion1.ID = "txtUrl1"
            _txtURLAplicacion1.Width = Unit.Pixel(350)
            Controls.Add(_txtURLAplicacion1)
            _regValidator1 = New RegularExpressionValidator
            _regValidator1.ID = _txtURLAplicacion1.ID & "_Reg"
            _regValidator1.ControlToValidate = _txtURLAplicacion1.ID
            _regValidator1.ValidationExpression = "^([Hh][Tt][Tt][Pp]|[Hh][Tt][Tt][Pp][Ss]):\/\/.*"
            _regValidator1.ErrorMessage = "*"
            Controls.Add(_regValidator1)

            _chkAplicacion2 = New CheckBox
            Controls.Add(_chkAplicacion2)

            _txtNomAplicacion2 = New TextBox
            Controls.Add(_txtNomAplicacion2)

             _txtURLAplicacion2 = New TextBox
            _txtURLAplicacion2.ID = "txtUrl2"
            _txtURLAplicacion2.Width = Unit.Pixel(350)
            Controls.Add(_txtURLAplicacion2)
            _regValidator2 = New RegularExpressionValidator
            _regValidator2.ID = _txtURLAplicacion2.ID & "_Reg"
            _regValidator2.ControlToValidate = _txtURLAplicacion2.ID
            _regValidator2.ValidationExpression = "^([Hh][Tt][Tt][Pp]|[Hh][Tt][Tt][Pp][Ss]):\/\/.*"
            _regValidator2.ErrorMessage = "*"
            Controls.Add(_regValidator2)

            _chkAplicacion3 = New CheckBox
            Controls.Add(_chkAplicacion3)

            _txtNomAplicacion3 = New TextBox
            Controls.Add(_txtNomAplicacion3)

            _txtURLAplicacion3 = New TextBox
            _txtURLAplicacion3.ID = "txtUrl3"
            _txtURLAplicacion3.Width = Unit.Pixel(350)
            Controls.Add(_txtURLAplicacion3)
            _regValidator3 = New RegularExpressionValidator
            _regValidator3.ID = _txtURLAplicacion3.ID & "_Reg"
            _regValidator3.ControlToValidate = _txtURLAplicacion3.ID
            _regValidator3.ValidationExpression = "^([Hh][Tt][Tt][Pp]|[Hh][Tt][Tt][Pp][Ss]):\/\/.*"
            _regValidator3.ErrorMessage = "*"
            Controls.Add(_regValidator3)

            _chkAplicacion4 = New CheckBox
            Controls.Add(_chkAplicacion4)

            _txtNomAplicacion4 = New TextBox
            Controls.Add(_txtNomAplicacion4)

            _txtURLAplicacion4 = New TextBox
            _txtURLAplicacion4.ID = "txtUrl4"
            _txtURLAplicacion4.Width = Unit.Pixel(350)
            Controls.Add(_txtURLAplicacion4)
            _regValidator4 = New RegularExpressionValidator
            _regValidator4.ID = _txtURLAplicacion4.ID & "_Reg"
            _regValidator4.ControlToValidate = _txtURLAplicacion4.ID
            _regValidator4.ValidationExpression = "^([Hh][Tt][Tt][Pp]|[Hh][Tt][Tt][Pp][Ss]):\/\/.*"
            _regValidator4.ErrorMessage = "*"
            Controls.Add(_regValidator4)

            _chkAplicacion5 = New CheckBox
            Controls.Add(_chkAplicacion5)

            _txtNomAplicacion5 = New TextBox
            Controls.Add(_txtNomAplicacion5)

            _txtURLAplicacion5 = New TextBox
            _txtURLAplicacion5.ID = "txtUrl5"
            _txtURLAplicacion5.Width = Unit.Pixel(350)
            Controls.Add(_txtURLAplicacion5)
            _regValidator5 = New RegularExpressionValidator
            _regValidator5.ID = _txtURLAplicacion5.ID & "_Reg"
            _regValidator5.ControlToValidate = _txtURLAplicacion5.ID
            _regValidator5.ValidationExpression = "^([Hh][Tt][Tt][Pp]|[Hh][Tt][Tt][Pp][Ss]):\/\/.*"
            _regValidator5.ErrorMessage = "*"
            Controls.Add(_regValidator5)

            _chkAplicacion6 = New CheckBox
            Controls.Add(_chkAplicacion6)

            _txtNomAplicacion6 = New TextBox
            Controls.Add(_txtNomAplicacion6)

            _txtURLAplicacion6 = New TextBox
            _txtURLAplicacion6.ID = "txtUrl6"
            _txtURLAplicacion6.Width = Unit.Pixel(350)
            Controls.Add(_txtURLAplicacion6)
            _regValidator6 = New RegularExpressionValidator
            _regValidator6.ID = _txtURLAplicacion6.ID & "_Reg"
            _regValidator6.ControlToValidate = _txtURLAplicacion6.ID
            _regValidator6.ValidationExpression = "^([Hh][Tt][Tt][Pp]|[Hh][Tt][Tt][Pp][Ss]):\/\/.*"
            _regValidator6.ErrorMessage = "*"
            Controls.Add(_regValidator6)

            _chkAplicacion7 = New CheckBox
            Controls.Add(_chkAplicacion7)

            _txtNomAplicacion7 = New TextBox
            Controls.Add(_txtNomAplicacion7)

            _txtURLAplicacion7 = New TextBox
            _txtURLAplicacion7.ID = "txtUrl7"
            _txtURLAplicacion7.Width = Unit.Pixel(350)
            Controls.Add(_txtURLAplicacion7)
            _regValidator7 = New RegularExpressionValidator
            _regValidator7.ID = _txtURLAplicacion7.ID & "_Reg"
            _regValidator7.ControlToValidate = _txtURLAplicacion7.ID
            _regValidator7.ValidationExpression = "^([Hh][Tt][Tt][Pp]|[Hh][Tt][Tt][Pp][Ss]):\/\/.*"
            _regValidator7.ErrorMessage = "*"
            Controls.Add(_regValidator7)

            _chkAplicacion8 = New CheckBox
            Controls.Add(_chkAplicacion8)

            _txtNomAplicacion8 = New TextBox
            Controls.Add(_txtNomAplicacion8)

            _txtURLAplicacion8 = New TextBox
            _txtURLAplicacion8.ID = "txtUrl8"
            _txtURLAplicacion8.Width = Unit.Pixel(350)
            Controls.Add(_txtURLAplicacion8)
            _regValidator8 = New RegularExpressionValidator
            _regValidator8.ID = _txtURLAplicacion8.ID & "_Reg"
            _regValidator8.ControlToValidate = _txtURLAplicacion8.ID
            _regValidator8.ValidationExpression = "^([Hh][Tt][Tt][Pp]|[Hh][Tt][Tt][Pp][Ss]):\/\/.*"
            _regValidator8.ErrorMessage = "*"
            Controls.Add(_regValidator8)

            _chkAplicacion9 = New CheckBox
            Controls.Add(_chkAplicacion9)

            _txtNomAplicacion9 = New TextBox
            Controls.Add(_txtNomAplicacion9)

            _txtURLAplicacion9 = New TextBox
            _txtURLAplicacion9.ID = "txtUrl9"
            _txtURLAplicacion9.Width = Unit.Pixel(350)
            Controls.Add(_txtURLAplicacion9)
            _regValidator9 = New RegularExpressionValidator
            _regValidator9.ID = _txtURLAplicacion9.ID & "_Reg"
            _regValidator9.ControlToValidate = _txtURLAplicacion9.ID
            _regValidator9.ValidationExpression = "^([Hh][Tt][Tt][Pp]|[Hh][Tt][Tt][Pp][Ss]):\/\/.*"
            _regValidator9.ErrorMessage = "*"
            Controls.Add(_regValidator9)

            _chkAplicacion10 = New CheckBox
            Controls.Add(_chkAplicacion10)

            _txtNomAplicacion10 = New TextBox
            Controls.Add(_txtNomAplicacion10)

            _txtURLAplicacion10 = New TextBox
            _txtURLAplicacion10.ID = "txtUrl10"
            _txtURLAplicacion10.Width = Unit.Pixel(350)
            Controls.Add(_txtURLAplicacion10)
            _regValidator10 = New RegularExpressionValidator
            _regValidator10.ID = _txtURLAplicacion10.ID & "_Reg"
            _regValidator10.ControlToValidate = _txtURLAplicacion10.ID
            _regValidator10.ValidationExpression = "^([Hh][Tt][Tt][Pp]|[Hh][Tt][Tt][Pp][Ss]):\/\/.*"
            _regValidator10.ErrorMessage = "*"
            Controls.Add(_regValidator10)

        End Sub

        ''' <summary>
        ''' Presenta el contenido del control en el sistema de escritura especificado. Este método lo usan principalmente los programadores de controles. 
        ''' </summary>
        ''' <param name="writer">HtmlTextWriter que representa la secuencia de salida para presentar contenido HTML en el cliente.</param>
        Protected Overrides Sub RenderContents(ByVal writer As System.Web.UI.HtmlTextWriter)
            Dim part As FSPMWebPartOtrasURLs = CType(WebPartToEdit, FSPMWebPartOtrasURLs)
            writer.Write("<b>" & part.Textos(19) & ":</b> ")
            _txtTitulo.RenderControl(writer)
            writer.WriteBreak()
            writer.Write("<b>" & part.Textos(21) & ":</b> ")
            _txtAncho.RenderControl(writer)
            _reqAncho.RenderControl(writer)
            _ranAncho.RenderControl(writer)
            writer.WriteBreak()
            writer.Write("<div style='border: 1px solid #CCCCCC;'>")
            writer.WriteBreak()
            writer.Write("<table>")
            writer.Write("<tr><td></td><td><b>&nbsp;&nbsp;&nbsp;" & part.Textos(247) & "</b></td><td><b>&nbsp;&nbsp;&nbsp;" & part.Textos(248) & "</b></td>")
            writer.Write("<tr><td>")
            _chkAplicacion1.RenderControl(writer)
            writer.Write("</td><td>")
            _txtNomAplicacion1.RenderControl(writer)
            writer.Write("</td><td>")
            _txtURLAplicacion1.RenderControl(writer)
            writer.Write("</td></tr>")
            writer.Write("<tr><td>")
            _chkAplicacion2.RenderControl(writer)
            writer.Write("</td><td>")
            _txtNomAplicacion2.RenderControl(writer)
            writer.Write("</td><td>")
            _txtURLAplicacion2.RenderControl(writer)
            writer.Write("</td></tr>")
            writer.Write("<tr><td>")
            _chkAplicacion3.RenderControl(writer)
            writer.Write("</td><td>")
            _txtNomAplicacion3.RenderControl(writer)
            writer.Write("</td><td>")
            _txtURLAplicacion3.RenderControl(writer)
            writer.Write("</td></tr>")
            writer.Write("<tr><td>")
            _chkAplicacion4.RenderControl(writer)
            writer.Write("</td><td>")
            _txtNomAplicacion4.RenderControl(writer)
            writer.Write("</td><td>")
            _txtURLAplicacion4.RenderControl(writer)
            writer.Write("</td></tr>")
            writer.Write("<tr><td>")
            _chkAplicacion5.RenderControl(writer)
            writer.Write("</td><td>")
            _txtNomAplicacion5.RenderControl(writer)
            writer.Write("</td><td>")
            _txtURLAplicacion5.RenderControl(writer)
            writer.Write("</td></tr>")
            writer.Write("<tr><td>")
            _chkAplicacion6.RenderControl(writer)
            writer.Write("</td><td>")
            _txtNomAplicacion6.RenderControl(writer)
            writer.Write("</td><td>")
            _txtURLAplicacion6.RenderControl(writer)
            writer.Write("</td></tr>")
            writer.Write("<tr><td>")
            _chkAplicacion7.RenderControl(writer)
            writer.Write("</td><td>")
            _txtNomAplicacion7.RenderControl(writer)
            writer.Write("</td><td>")
            _txtURLAplicacion7.RenderControl(writer)
            writer.Write("</td></tr>")
            writer.Write("<tr><td>")
            _chkAplicacion8.RenderControl(writer)
            writer.Write("</td><td>")
            _txtNomAplicacion8.RenderControl(writer)
            writer.Write("</td><td>")
            _txtURLAplicacion8.RenderControl(writer)
            writer.Write("</td></tr>")
            writer.Write("<tr><td>")
            _chkAplicacion9.RenderControl(writer)
            writer.Write("</td><td>")
            _txtNomAplicacion9.RenderControl(writer)
            writer.Write("</td><td>")
            _txtURLAplicacion9.RenderControl(writer)
            writer.Write("</td></tr>")
            writer.Write("<tr><td>")
            _chkAplicacion10.RenderControl(writer)
            writer.Write("</td><td>")
            _txtNomAplicacion10.RenderControl(writer)
            writer.Write("</td><td>")
            _txtURLAplicacion10.RenderControl(writer)
            writer.Write("</td></tr>")
            writer.Write("</table>")
            writer.WriteBreak()
            writer.Write("</div>")
            writer.WriteBreak()
        End Sub
        ''' <summary>
        ''' Funcion que comprueba que si una url esta chekeada, todos sus datos esten completos
        ''' </summary>
        ''' <returns>retorna true si todo es correcto y false si falta algun dato</returns>
        ''' <remarks></remarks>
        Private Function ValidarUrls() As Boolean
            If _chkAplicacion1.Checked AndAlso (_txtNomAplicacion1.Text.Trim = String.Empty OrElse _txtURLAplicacion1.Text.Trim = String.Empty) Then
                Return False
            End If
            If _chkAplicacion2.Checked AndAlso (_txtNomAplicacion2.Text.Trim = String.Empty OrElse _txtURLAplicacion2.Text.Trim = String.Empty) Then
                Return False
            End If
            If _chkAplicacion3.Checked AndAlso (_txtNomAplicacion3.Text.Trim = String.Empty OrElse _txtURLAplicacion3.Text.Trim = String.Empty) Then
                Return False
            End If
            If _chkAplicacion4.Checked AndAlso (_txtNomAplicacion4.Text.Trim = String.Empty OrElse _txtURLAplicacion4.Text.Trim = String.Empty) Then
                Return False
            End If
            If _chkAplicacion5.Checked AndAlso (_txtNomAplicacion5.Text.Trim = String.Empty OrElse _txtURLAplicacion5.Text.Trim = String.Empty) Then
                Return False
            End If
            If _chkAplicacion6.Checked AndAlso (_txtNomAplicacion6.Text.Trim = String.Empty OrElse _txtURLAplicacion6.Text.Trim = String.Empty) Then
                Return False
            End If
            If _chkAplicacion7.Checked AndAlso (_txtNomAplicacion7.Text.Trim = String.Empty OrElse _txtURLAplicacion7.Text.Trim = String.Empty) Then
                Return False
            End If
            If _chkAplicacion8.Checked AndAlso (_txtNomAplicacion8.Text.Trim = String.Empty OrElse _txtURLAplicacion8.Text.Trim = String.Empty) Then
                Return False
            End If
            If _chkAplicacion9.Checked AndAlso (_txtNomAplicacion9.Text.Trim = String.Empty OrElse _txtURLAplicacion9.Text.Trim = String.Empty) Then
                Return False
            End If
            If _chkAplicacion10.Checked AndAlso (_txtNomAplicacion10.Text.Trim = String.Empty OrElse _txtURLAplicacion10.Text.Trim = String.Empty) Then
                Return False
            End If
			_regValidator1.Validate()
            If Not _regValidator1.IsValid Then
                Return False
            End If
            _regValidator2.Validate()
            If Not _regValidator2.IsValid Then
                Return False
            End If
            _regValidator3.Validate()
            If Not _regValidator3.IsValid Then
                Return False
            End If
            _regValidator4.Validate()
            If Not _regValidator4.IsValid Then
                Return False
            End If
            _regValidator5.Validate()
            If Not _regValidator5.IsValid Then
                Return False
            End If
            _regValidator6.Validate()
            If Not _regValidator6.IsValid Then
                Return False
            End If
            _regValidator7.Validate()
            If Not _regValidator7.IsValid Then
                Return False
            End If
            _regValidator8.Validate()
            If Not _regValidator8.IsValid Then
                Return False
            End If
            _regValidator9.Validate()
            If Not _regValidator9.IsValid Then
                Return False
            End If
            _regValidator10.Validate()
            If Not _regValidator10.IsValid Then
                Return False
            End If
            Return True
        End Function

        ''' <summary>
        ''' Funcion que aplica los cambios realizados al Webpart
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Overrides Function ApplyChanges() As Boolean
            Dim part As FSPMWebPartOtrasURLs = CType(WebPartToEdit, FSPMWebPartOtrasURLs)

            _reqAncho.Validate()
            _ranAncho.Validate()
            If _reqAncho.IsValid() And _ranAncho.IsValid And ValidarUrls() Then
                part.Title = _txtTitulo.Text
                part.Width = Unit.Pixel(CType(_txtAncho.Text, Integer))
                part.mURLsSeleccionadas = String.Empty
                If chkAplicacion1.Checked Then
                    part.mURLsSeleccionadas = part.mURLsSeleccionadas & "1"
                    part.mURLsSeleccionadas = part.mURLsSeleccionadas & "," & _txtNomAplicacion1.Text
                    part.mURLsSeleccionadas = part.mURLsSeleccionadas & "," & _txtURLAplicacion1.Text & "#"
                Else
                    part.mURLsSeleccionadas = part.mURLsSeleccionadas & "0"
                    part.mURLsSeleccionadas = part.mURLsSeleccionadas & "," & _txtNomAplicacion1.Text
                    part.mURLsSeleccionadas = part.mURLsSeleccionadas & "," & _txtURLAplicacion1.Text & "#"

                End If
                If chkAplicacion2.Checked Then
                    part.mURLsSeleccionadas = part.mURLsSeleccionadas & "1"
                    part.mURLsSeleccionadas = part.mURLsSeleccionadas & "," & _txtNomAplicacion2.Text
                    part.mURLsSeleccionadas = part.mURLsSeleccionadas & "," & _txtURLAplicacion2.Text & "#"
                Else
                    part.mURLsSeleccionadas = part.mURLsSeleccionadas & "0"
                    part.mURLsSeleccionadas = part.mURLsSeleccionadas & "," & _txtNomAplicacion2.Text
                    part.mURLsSeleccionadas = part.mURLsSeleccionadas & "," & _txtURLAplicacion2.Text & "#"
                End If
                If chkAplicacion3.Checked Then
                    part.mURLsSeleccionadas = part.mURLsSeleccionadas & "1"
                    part.mURLsSeleccionadas = part.mURLsSeleccionadas & "," & _txtNomAplicacion3.Text
                    part.mURLsSeleccionadas = part.mURLsSeleccionadas & "," & _txtURLAplicacion3.Text & "#"
                Else
                    part.mURLsSeleccionadas = part.mURLsSeleccionadas & "0"
                    part.mURLsSeleccionadas = part.mURLsSeleccionadas & "," & _txtNomAplicacion3.Text
                    part.mURLsSeleccionadas = part.mURLsSeleccionadas & "," & _txtURLAplicacion3.Text & "#"
                End If
                If chkAplicacion4.Checked Then
                    part.mURLsSeleccionadas = part.mURLsSeleccionadas & "1"
                    part.mURLsSeleccionadas = part.mURLsSeleccionadas & "," & _txtNomAplicacion4.Text
                    part.mURLsSeleccionadas = part.mURLsSeleccionadas & "," & _txtURLAplicacion4.Text & "#"
                Else
                    part.mURLsSeleccionadas = part.mURLsSeleccionadas & "0"
                    part.mURLsSeleccionadas = part.mURLsSeleccionadas & "," & _txtNomAplicacion4.Text
                    part.mURLsSeleccionadas = part.mURLsSeleccionadas & "," & _txtURLAplicacion4.Text & "#"
                End If
                If chkAplicacion5.Checked Then
                    part.mURLsSeleccionadas = part.mURLsSeleccionadas & "1"
                    part.mURLsSeleccionadas = part.mURLsSeleccionadas & "," & _txtNomAplicacion5.Text
                    part.mURLsSeleccionadas = part.mURLsSeleccionadas & "," & _txtURLAplicacion5.Text & "#"
                Else
                    part.mURLsSeleccionadas = part.mURLsSeleccionadas & "0"
                    part.mURLsSeleccionadas = part.mURLsSeleccionadas & "," & _txtNomAplicacion5.Text
                    part.mURLsSeleccionadas = part.mURLsSeleccionadas & "," & _txtURLAplicacion5.Text & "#"
                End If
                If chkAplicacion6.Checked Then
                    part.mURLsSeleccionadas = part.mURLsSeleccionadas & "1"
                    part.mURLsSeleccionadas = part.mURLsSeleccionadas & "," & _txtNomAplicacion6.Text
                    part.mURLsSeleccionadas = part.mURLsSeleccionadas & "," & _txtURLAplicacion6.Text & "#"
                Else
                    part.mURLsSeleccionadas = part.mURLsSeleccionadas & "0"
                    part.mURLsSeleccionadas = part.mURLsSeleccionadas & "," & _txtNomAplicacion6.Text
                    part.mURLsSeleccionadas = part.mURLsSeleccionadas & "," & _txtURLAplicacion6.Text & "#"
                End If
                If chkAplicacion7.Checked Then
                    part.mURLsSeleccionadas = part.mURLsSeleccionadas & "1"
                    part.mURLsSeleccionadas = part.mURLsSeleccionadas & "," & _txtNomAplicacion7.Text
                    part.mURLsSeleccionadas = part.mURLsSeleccionadas & "," & _txtURLAplicacion7.Text & "#"
                Else
                    part.mURLsSeleccionadas = part.mURLsSeleccionadas & "0"
                    part.mURLsSeleccionadas = part.mURLsSeleccionadas & "," & _txtNomAplicacion7.Text
                    part.mURLsSeleccionadas = part.mURLsSeleccionadas & "," & _txtURLAplicacion7.Text & "#"
                End If
                If chkAplicacion8.Checked Then
                    part.mURLsSeleccionadas = part.mURLsSeleccionadas & "1"
                    part.mURLsSeleccionadas = part.mURLsSeleccionadas & "," & _txtNomAplicacion8.Text
                    part.mURLsSeleccionadas = part.mURLsSeleccionadas & "," & _txtURLAplicacion8.Text & "#"
                Else
                    part.mURLsSeleccionadas = part.mURLsSeleccionadas & "0"
                    part.mURLsSeleccionadas = part.mURLsSeleccionadas & "," & _txtNomAplicacion8.Text
                    part.mURLsSeleccionadas = part.mURLsSeleccionadas & "," & _txtURLAplicacion8.Text & "#"
                End If
                If chkAplicacion9.Checked Then
                    part.mURLsSeleccionadas = part.mURLsSeleccionadas & "1"
                    part.mURLsSeleccionadas = part.mURLsSeleccionadas & "," & _txtNomAplicacion9.Text
                    part.mURLsSeleccionadas = part.mURLsSeleccionadas & "," & _txtURLAplicacion9.Text & "#"
                Else
                    part.mURLsSeleccionadas = part.mURLsSeleccionadas & "0"
                    part.mURLsSeleccionadas = part.mURLsSeleccionadas & "," & _txtNomAplicacion9.Text
                    part.mURLsSeleccionadas = part.mURLsSeleccionadas & "," & _txtURLAplicacion9.Text & "#"
                End If
                If chkAplicacion10.Checked Then
                    part.mURLsSeleccionadas = part.mURLsSeleccionadas & "1"
                    part.mURLsSeleccionadas = part.mURLsSeleccionadas & "," & _txtNomAplicacion10.Text
                    part.mURLsSeleccionadas = part.mURLsSeleccionadas & "," & _txtURLAplicacion10.Text
                Else
                    part.mURLsSeleccionadas = part.mURLsSeleccionadas & "0"
                    part.mURLsSeleccionadas = part.mURLsSeleccionadas & "," & _txtNomAplicacion10.Text
                    part.mURLsSeleccionadas = part.mURLsSeleccionadas & "," & _txtURLAplicacion10.Text
                End If
                part.CreateChildControls()
                Return True
            Else
                Return False
            End If
        End Function

        ''' <summary>
        ''' Procedimiento que muestra el control Editor con los datos que tiene el Webpart en el momento en el que se le da al boton "Edicion"
        ''' </summary>
        ''' <remarks></remarks>
        Public Overrides Sub SyncChanges()
            Dim part As FSPMWebPartOtrasURLs = CType(WebPartToEdit, FSPMWebPartOtrasURLs)

            txtTitulo.Text = part.Title
            txtAncho.Text = part.Width.Value.ToString()

            part.mURLsSeleccionadasArray = Split(part.mURLsSeleccionadas, "#")
            If part.mURLsSeleccionadasArray(0) Is Nothing OrElse part.mURLsSeleccionadasArray(0) = "" Then
                Dim sPatronTexto As String = "webpartURL_nom*"
                Dim i As Byte = 1
                Dim CargarUrl As Boolean = False
                Dim colAppSettings As System.Collections.Specialized.NameValueCollection
                colAppSettings = System.Configuration.ConfigurationManager.AppSettings
                For Each sKey As String In colAppSettings
                    If CargarUrl Then
                        Select Case i
                            Case 1
                                txtUrlAplicacion1.Text = colAppSettings.Get(sKey)
                            Case 2
                                txtUrlAplicacion2.Text = colAppSettings.Get(sKey)
                            Case 3
                                txtUrlAplicacion3.Text = colAppSettings.Get(sKey)
                            Case 4
                                txtUrlAplicacion4.Text = colAppSettings.Get(sKey)
                            Case 5
                                txtUrlAplicacion5.Text = colAppSettings.Get(sKey)
                            Case 6
                                txtUrlAplicacion6.Text = colAppSettings.Get(sKey)
                            Case 7
                                txtUrlAplicacion7.Text = colAppSettings.Get(sKey)
                            Case 8
                                txtUrlAplicacion8.Text = colAppSettings.Get(sKey)
                            Case 9
                                txtUrlAplicacion9.Text = colAppSettings.Get(sKey)
                            Case 10
                                txtUrlAplicacion10.Text = colAppSettings.Get(sKey)
                        End Select
                        CargarUrl = False
                        i += 1
                    End If
                    If sKey Like sPatronTexto Then
                        Select Case i
                            Case 1
                                chkAplicacion1.Checked = True
                                txtNomAplicacion1.Text = colAppSettings.Get(sKey)
                            Case 2
                                chkAplicacion2.Checked = True
                                txtNomAplicacion2.Text = colAppSettings.Get(sKey)
                            Case 3
                                chkAplicacion3.Checked = True
                                txtNomAplicacion3.Text = colAppSettings.Get(sKey)
                            Case 4
                                chkAplicacion4.Checked = True
                                txtNomAplicacion4.Text = colAppSettings.Get(sKey)
                            Case 5
                                chkAplicacion5.Checked = True
                                txtNomAplicacion5.Text = colAppSettings.Get(sKey)
                            Case 6
                                chkAplicacion6.Checked = True
                                txtNomAplicacion6.Text = colAppSettings.Get(sKey)
                            Case 7
                                chkAplicacion7.Checked = True
                                txtNomAplicacion7.Text = colAppSettings.Get(sKey)
                            Case 8
                                chkAplicacion8.Checked = True
                                txtNomAplicacion8.Text = colAppSettings.Get(sKey)
                            Case 9
                                chkAplicacion9.Checked = True
                                txtNomAplicacion9.Text = colAppSettings.Get(sKey)
                            Case 10
                                chkAplicacion10.Checked = True
                                txtNomAplicacion10.Text = colAppSettings.Get(sKey)
                        End Select
                        CargarUrl = True
                    End If
                Next
            Else
                For i = 0 To part.mURLsSeleccionadasArray.GetLength(0) - 1
                    part.mDatosURLsSeleccionadasArray = Split(part.mURLsSeleccionadasArray(i), ",")
                    Select Case i + 1
                        Case 1
                            If part.mDatosURLsSeleccionadasArray(0) = "1" Then
                                chkAplicacion1.Checked = True
                            End If
                            txtNomAplicacion1.Text = part.mDatosURLsSeleccionadasArray(1)
                            txtUrlAplicacion1.Text = part.mDatosURLsSeleccionadasArray(2)
                        Case 2
                            If part.mDatosURLsSeleccionadasArray(0) = "1" Then
                                chkAplicacion2.Checked = True
                            End If
                            txtNomAplicacion2.Text = part.mDatosURLsSeleccionadasArray(1)
                            txtUrlAplicacion2.Text = part.mDatosURLsSeleccionadasArray(2)
                        Case 3
                            If part.mDatosURLsSeleccionadasArray(0) = "1" Then
                                chkAplicacion3.Checked = True
                            End If
                            txtNomAplicacion3.Text = part.mDatosURLsSeleccionadasArray(1)
                            txtUrlAplicacion3.Text = part.mDatosURLsSeleccionadasArray(2)
                        Case 4
                            If part.mDatosURLsSeleccionadasArray(0) = "1" Then
                                chkAplicacion4.Checked = True
                            End If
                            txtNomAplicacion4.Text = part.mDatosURLsSeleccionadasArray(1)
                            txtUrlAplicacion4.Text = part.mDatosURLsSeleccionadasArray(2)
                        Case 5
                            If part.mDatosURLsSeleccionadasArray(0) = "1" Then
                                chkAplicacion5.Checked = True
                            End If
                            txtNomAplicacion5.Text = part.mDatosURLsSeleccionadasArray(1)
                            txtUrlAplicacion5.Text = part.mDatosURLsSeleccionadasArray(2)
                        Case 6
                            If part.mDatosURLsSeleccionadasArray(0) = "1" Then
                                chkAplicacion6.Checked = True
                            End If
                            txtNomAplicacion6.Text = part.mDatosURLsSeleccionadasArray(1)
                            txtUrlAplicacion6.Text = part.mDatosURLsSeleccionadasArray(2)
                        Case 7
                            If part.mDatosURLsSeleccionadasArray(0) = "1" Then
                                chkAplicacion7.Checked = True
                            End If
                            txtNomAplicacion7.Text = part.mDatosURLsSeleccionadasArray(1)
                            txtUrlAplicacion7.Text = part.mDatosURLsSeleccionadasArray(2)
                        Case 8
                            If part.mDatosURLsSeleccionadasArray(0) = "1" Then
                                chkAplicacion8.Checked = True
                            End If
                            txtNomAplicacion8.Text = part.mDatosURLsSeleccionadasArray(1)
                            txtUrlAplicacion8.Text = part.mDatosURLsSeleccionadasArray(2)
                        Case 9
                            If part.mDatosURLsSeleccionadasArray(0) = "1" Then
                                chkAplicacion9.Checked = True
                            End If
                            txtNomAplicacion9.Text = part.mDatosURLsSeleccionadasArray(1)
                            txtUrlAplicacion9.Text = part.mDatosURLsSeleccionadasArray(2)
                        Case 10
                            If part.mDatosURLsSeleccionadasArray(0) = "1" Then
                                chkAplicacion10.Checked = True
                            End If
                            txtNomAplicacion10.Text = part.mDatosURLsSeleccionadasArray(1)
                            txtUrlAplicacion10.Text = part.mDatosURLsSeleccionadasArray(2)
                    End Select

                Next
            End If
        End Sub
    End Class

    ''' <summary>
    ''' Carga los datos por defecto para el control
    ''' </summary>
    Public Sub New()
        Me.Width = Unit.Pixel(300)
    End Sub

    ''' <summary>
    ''' Refresca los datos del webpart
    ''' </summary>
    Public Sub Actualizar()
        CreateChildControls()
    End Sub

    ''' <summary>
    ''' Devuelve una colección de controles EditorPart personalizados que pueden utilizarse para editar un control WebPart cuando se encuentra en modo de edición.
    ''' </summary>
    ''' <returns>Una colección EditorPartCollection que contiene controles EditorPart personalizados asociados a un control WebPart.</returns>
    Public Overrides Function CreateEditorParts() As System.Web.UI.WebControls.WebParts.EditorPartCollection
        Dim editorArray As New ArrayList()
        Dim edPart As New ControlEditor()
        edPart.ID = Me.ID & "_editor1"
        editorArray.Add(edPart)
        Dim editorParts As New EditorPartCollection(editorArray)
        Return editorParts
    End Function

    ''' <summary>
    ''' Restaura información de estado del control.
    ''' </summary>
    ''' <param name="state">Object que representa el estado del control que se va a restaurar.</param>
    Protected Overrides Sub LoadControlState(ByVal state As Object)
        If (state IsNot Nothing) Then
            Dim p As Pair = TryCast(state, Pair)
            _sIdioma = p.Second
            state = p.First
            p = TryCast(state, Pair)
            If p IsNot Nothing Then
                MyBase.LoadControlState(p.First)
                _Textos = CType(p.Second, DataSet)
            Else
                If (TypeOf (state) Is String()) Then
                    _Textos = CType(state, DataSet)
                Else
                    MyBase.LoadControlState(state)
                End If
            End If
        End If
    End Sub

    ''' <summary>
    ''' Registra el control como un control cuyo estado se debe conservar.
    ''' </summary>
    ''' <param name="e">Objeto EventArgs que contiene los datos del evento.</param>
    Protected Overrides Sub OnInit(ByVal e As System.EventArgs)
        MyBase.OnInit(e)
        Page.RegisterRequiresControlState(Me)
    End Sub
End Class



