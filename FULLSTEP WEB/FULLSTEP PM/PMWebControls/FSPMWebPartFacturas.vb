﻿Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.HtmlControls
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.ComponentModel
Imports System.Configuration
Imports Fullstep.FSNServer

Public Class FSPMWebPartFacturas
    Inherits WebPart

    Private lblTitulo As Label
    Private lblMensaje As Label
    Private lblError As Label
    Private lstSolicitudes As DataList
    Private lnkVerTodos As FSNWebControls.FSNHyperlink
    Private celdaImagen As HtmlTableCell
    Private mTiposSolicitudes As String
    Private mTablasFiltros As String
    Private iModo As Integer
    Private iNumFacturasAMostrar As Integer
    Private dImporteDesde As Double
    Private dImporteHasta As Double
    Private dFechaDesde As Date
    Private dFechaHasta As Date
    Private mEstados As String
    Private bAbiertasPorUsted As Boolean
    Private bParticipo As Boolean
    Private bTrasladadas As Boolean
    Private bOtras As Boolean
    Private bPendientesTratar As Boolean
    Private bPendientesDevolucion As Boolean
    Private sTitulo As String
    Private _Textos As DataSet
    Private _sIdioma As String

    Private Class DataListTemplate
        Implements ITemplate

        ''' <summary>
        ''' Define el objeto Control al que pertenecen los controles secundarios y las plantillas. Estos controles secundarios están a su vez definidos en una plantilla en línea.
        ''' </summary>
        ''' <param name="container">Objeto Control que contiene las instancias de los controles de la plantilla en línea.</param>
        Private Sub InstantiateIn(ByVal container As Control) _
           Implements ITemplate.InstantiateIn
            Dim hr = New HtmlGenericControl("hr")
            container.Controls.Add(hr)
            Dim tabla As New HtmlTable()
            tabla.Width = "100%"
            Dim fila As New HtmlTableRow()
            Dim celda As New HtmlTableCell()
            Dim lc As New Literal()
            AddHandler lc.DataBinding, AddressOf Factura_DataBinding
            celda.Controls.Add(lc)
            celda.Style.Add(HtmlTextWriterStyle.FontWeight, "bold")
            celda.Attributes.Add("class", "ListItemLink")
            AddHandler celda.DataBinding, AddressOf IrADetalleFactura_DataBinding
            fila.Cells.Add(celda)
            celda = New HtmlTableCell()
            lc = New Literal()
            AddHandler lc.DataBinding, AddressOf Importe_DataBinding
            celda.Controls.Add(lc)
            celda.Align = "right"
            celda.Style.Add(HtmlTextWriterStyle.FontWeight, "bold")
            celda.Attributes.Add("class", "ListItemLink")
            AddHandler celda.DataBinding, AddressOf IrADetalleFactura_DataBinding
            fila.Cells.Add(celda)
            tabla.Rows.Add(fila)
            'Proveedor
            fila = New HtmlTableRow()
            celda = New HtmlTableCell()
            lc = New Literal()
            AddHandler lc.DataBinding, AddressOf Proveedor_DataBinding
            celda.ColSpan = 2
            celda.Controls.Add(lc)
            AddHandler celda.DataBinding, AddressOf IrADetalleFactura_DataBinding
            fila.Cells.Add(celda)
            tabla.Rows.Add(fila)
            'Empresa
            fila = New HtmlTableRow()
            celda = New HtmlTableCell()
            lc = New Literal()
            AddHandler lc.DataBinding, AddressOf Empresa_DataBinding
            celda.ColSpan = 2
            celda.Controls.Add(lc)
            AddHandler celda.DataBinding, AddressOf IrADetalleFactura_DataBinding
            fila.Cells.Add(celda)
            tabla.Rows.Add(fila)
            container.Controls.Add(tabla)
        End Sub

        ''' <summary>
        ''' Se produce al enlazar la etiqueta de la plantilla en que se muestra la instancia y el titulo con el origen de datos.
        ''' </summary>
        Private Sub Factura_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim lc As Literal = CType(sender, Literal)
            Dim container As DataListItem = CType(lc.NamingContainer, DataListItem)
            lc.Text = DataBinder.Eval(container.DataItem, "Factura")
        End Sub

        ''' <summary>
        ''' Se produce al enlazar la etiqueta de la plantilla en que se muestra la instancia y el titulo con el origen de datos.
        ''' </summary>
        Private Sub Importe_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim lc As Literal = CType(sender, Literal)
            Dim container As DataListItem = CType(lc.NamingContainer, DataListItem)
            lc.Text = DataBinder.Eval(container.DataItem, "Importe")
        End Sub

        ''' <summary>
        ''' Se produce al enlazar la tabla de la plantilla que muestra los datos con el origen de datos.
        ''' </summary>
        Private Sub IrADetalleFactura_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim tab As HtmlTableCell = CType(sender, HtmlTableCell)
            Dim container As DataListItem = CType(tab.NamingContainer, DataListItem)
            Dim URL As String

            URL = ConfigurationManager.AppSettings("rutaFS") & "PM/Facturas/DetalleFactura.aspx?ID=" & DataBinder.Eval(container.DataItem, "FacturaId") & "&gestor=" & DataBinder.Eval(container.DataItem, "Gestor") & "&receptor=" & DataBinder.Eval(container.DataItem, "Receptor") & "&volver=" & HttpContext.Current.Request("url")
            tab.Attributes.Add("onclick", "return ComprobarInstanciaEnProcesoWebPart(" & DataBinder.Eval(container.DataItem, "Instancia") & ",'" & URL & "','" & ConfigurationManager.AppSettings("rutaFS") & "', " & FSNLibrary.TiposDeDatos.TipoDeSolicitud.Autofactura & ");")
        End Sub


        ''' <summary>
        ''' Se produce al enlazar la etiqueta de la plantilla en que se muestra la fecha de alta con el origen de datos.
        ''' </summary>
        Private Sub Proveedor_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim lc As Literal = CType(sender, Literal)
            Dim container As DataListItem = CType(lc.NamingContainer, DataListItem)
            lc.Text = DataBinder.Eval(container.DataItem, "Proveedor")
        End Sub

        ''' <summary>
        ''' Se produce al enlazar la etiqueta de la plantilla en que se muestra el peticionario con el origen de datos.
        ''' </summary>
        Private Sub Empresa_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim lc As Literal = CType(sender, Literal)
            Dim container As DataListItem = CType(lc.NamingContainer, DataListItem)
            lc.Text = DataBinder.Eval(container.DataItem, "Empresa")
        End Sub
    End Class

    <Serializable()> _
    Public Class mOrden
        Private mlInstancia As Long
        Private mlFacturaId As Long
        Private msFactura As String
        Private msProveedor As String
        Private msEmpresa As String
        Private msImporte As String
        Private miGestor As Short
        Private miReceptor As Short

        Public Property Instancia() As Long
            Get
                Return mlInstancia
            End Get
            Set(ByVal value As Long)
                mlInstancia = value
            End Set
        End Property

        Public Property FacturaId() As Long
            Get
                Return mlFacturaId
            End Get
            Set(ByVal value As Long)
                mlFacturaId = value
            End Set
        End Property

        Public Property Factura() As String
            Get
                Return msFactura
            End Get
            Set(ByVal value As String)
                msFactura = value
            End Set
        End Property

        Public Property Proveedor() As String
            Get
                Return msProveedor
            End Get
            Set(ByVal value As String)
                msProveedor = value
            End Set
        End Property

        Public Property Empresa() As String
            Get
                Return msEmpresa
            End Get
            Set(ByVal value As String)
                msEmpresa = value
            End Set
        End Property

        Public Property Importe() As String
            Get
                Return msImporte
            End Get
            Set(ByVal value As String)
                msImporte = value
            End Set
        End Property

        Public Property Gestor() As Short
            Get
                Return miGestor
            End Get
            Set(ByVal value As Short)
                miGestor = value
            End Set
        End Property

        Public Property Receptor() As Short
            Get
                Return miReceptor
            End Get
            Set(ByVal value As Short)
                miReceptor = value
            End Set
        End Property
    End Class

    Private Class ControlEditor
        Inherits EditorPart

        Private _txtTitulo As TextBox
        Private _txtAncho As TextBox
        Private _reqAncho As RequiredFieldValidator
        Private _ranAncho As RangeValidator
        Private _wneNumFacturas As Infragistics.Web.UI.EditorControls.WebNumericEditor
        Private _reqNumFacturas As RequiredFieldValidator
        Private _chkTipoFactura As CheckBoxList
        Private _chkFiltrosDisponibles As CheckBoxList
        Private _wneImporteDesde As Infragistics.Web.UI.EditorControls.WebNumericEditor
        Private _wneImporteHasta As Infragistics.Web.UI.EditorControls.WebNumericEditor
        Private _wdcFechaDesde As Infragistics.Web.UI.EditorControls.WebDatePicker
        Private _wdcFechaHasta As Infragistics.Web.UI.EditorControls.WebDatePicker
        Private _chkEstados As CheckBoxList
        Private _reqEstados As RequiredFieldValidatorForCheckBoxLists
        Private _reqFiltroSolicitud As RequiredFieldValidatorForCheckBoxLists
        Private _chkFiltroSolicitud As CheckBoxList

        ''' <summary>
        ''' Lo llama el marco de trabajo de las páginas ASP.NET para indicar a los controles de servidor que utilizan la implementación basada en la composición que creen los controles secundarios que contengan como forma de preparar la devolución o representación de los datos.
        ''' </summary>
        Protected Overrides Sub CreateChildControls()
            Dim part As FSPMWebPartFacturas = CType(WebPartToEdit, FSPMWebPartFacturas)
            Controls.Clear()
            _txtTitulo = New TextBox()
            _txtTitulo.Width = Unit.Pixel(250)
            _txtTitulo.Text = part.Title
            Controls.Add(_txtTitulo)
            _txtAncho = New TextBox()
            _txtAncho.MaxLength = 3
            _txtAncho.Width = Unit.Pixel(30)
            _txtAncho.Text = part.Width.Value.ToString()
            _txtAncho.ID = Me.ID & "_txtAncho"
            Controls.Add(_txtAncho)
            _reqAncho = New RequiredFieldValidator()
            _reqAncho.ID = _txtAncho.ID & "_Req"
            _reqAncho.ErrorMessage = "*"
            _reqAncho.ControlToValidate = _txtAncho.ID
            Controls.Add(_reqAncho)
            _ranAncho = New RangeValidator()
            _ranAncho.ID = _txtAncho.ID & "_Ran"
            _ranAncho.Type = ValidationDataType.Integer
            _ranAncho.MaximumValue = 500
            _ranAncho.MinimumValue = 200
            _ranAncho.ErrorMessage = "(200-500)"
            _ranAncho.ControlToValidate = _txtAncho.ID
            Controls.Add(_ranAncho)
            'Nº de solicitudes a mostrar
            _wneNumFacturas = New Infragistics.Web.UI.EditorControls.WebNumericEditor
            _wneNumFacturas.ID = Me.ID & "_wneNumFacturas"
            _wneNumFacturas.Width = Unit.Pixel(30)
            _wneNumFacturas.MaxLength = 2
            _wneNumFacturas.DataMode = Infragistics.Web.UI.EditorControls.NumericDataMode.Int
            Controls.Add(_wneNumFacturas)
            _reqNumFacturas = New RequiredFieldValidator()
            _reqNumFacturas.ID = _wneNumFacturas.ID & "_Req"
            _reqNumFacturas.ErrorMessage = "*"
            _reqNumFacturas.ControlToValidate = _wneNumFacturas.ID
            Controls.Add(_reqNumFacturas)
            'Tipos de factura
            Dim pag As FSNServer.IFSNPage = CType(Me.Page, FSNServer.IFSNPage)
            Dim FSWSServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")

            _chkTipoFactura = New CheckBoxList
            _chkTipoFactura.Items.Clear()
            Try
                Dim cSolicitudes As FSNServer.Solicitudes
                cSolicitudes = FSWSServer.Get_Object(GetType(FSNServer.Solicitudes))
                cSolicitudes.LoadTiposSolicitudesRolPeticionario(pag.Usuario.CodPersona, pag.Idioma, , FSNLibrary.TiposDeDatos.TipoDeSolicitud.Autofactura)

                _chkTipoFactura.DataTextField = "DEN"
                _chkTipoFactura.DataValueField = "ID"
                _chkTipoFactura.DataSource = cSolicitudes.Data
                _chkTipoFactura.DataBind()
            Catch ex As Exception

            End Try
            _chkTipoFactura.ID = Me.ID & "_chkTipoFactura"
            _chkTipoFactura.RepeatColumns = 2
            _chkTipoFactura.Width = Unit.Percentage(100)
            Controls.Add(_chkTipoFactura) 'Hay que añadir el checkboxlist aunque no tenga elementos

            'Filtros disponibles
            _chkFiltrosDisponibles = New CheckBoxList
            _chkFiltrosDisponibles.Items.Clear()
            Try
                Dim cFiltros As FSNServer.Filtros
                cFiltros = FSWSServer.Get_Object(GetType(FSNServer.Filtros))
                _chkFiltrosDisponibles.DataTextField = "NOM_FILTRO"
                _chkFiltrosDisponibles.DataValueField = "NOM_TABLA"
                _chkFiltrosDisponibles.DataSource = cFiltros.LoadDisponiblesYaConfigurados(pag.Usuario.CodPersona, FSNLibrary.TiposDeDatos.TipoDeSolicitud.Autofactura)
                _chkFiltrosDisponibles.DataBind()
            Catch ex As Exception

            End Try
            _chkFiltrosDisponibles.ID = Me.ID & "chkFiltrosDisponibles"
            Controls.Add(_chkFiltrosDisponibles)
            'Importe desde
            _wneImporteDesde = New Infragistics.Web.UI.EditorControls.WebNumericEditor
            _wneImporteDesde.Width = Unit.Pixel(100)
            _wneImporteDesde.DataMode = Infragistics.Web.UI.EditorControls.NumericDataMode.Double
            Controls.Add(_wneImporteDesde)
            'Importe hasta
            _wneImporteHasta = New Infragistics.Web.UI.EditorControls.WebNumericEditor
            _wneImporteHasta.Width = Unit.Pixel(100)
            _wneImporteHasta.DataMode = Infragistics.Web.UI.EditorControls.NumericDataMode.Double
            Controls.Add(_wneImporteHasta)
            'Fecha desde
            _wdcFechaDesde = New Infragistics.Web.UI.EditorControls.WebDatePicker
            _wdcFechaDesde.Width = Unit.Pixel(100)
            _wdcFechaDesde.NullText = ""
            _wdcFechaDesde.Value = part.dFechaDesde
            Controls.Add(_wdcFechaDesde)
            'Fecha hasta
            _wdcFechaHasta = New Infragistics.Web.UI.EditorControls.WebDatePicker
            _wdcFechaHasta.Width = Unit.Pixel(100)
            _wdcFechaHasta.NullText = ""
            _wdcFechaHasta.Value = part.dFechaHasta
            Controls.Add(_wdcFechaHasta)
            'Filtrar por estados
            _chkEstados = New CheckBoxList
            _chkEstados.ID = Me.ID & "chkEstados"
            _chkEstados.Items.Clear()
            _chkEstados.RepeatColumns = 2
            _chkEstados.Width = Unit.Percentage(100)
            Try
                Dim cEstadosFactura As FSNServer.EstadosFactura
                cEstadosFactura = FSWSServer.Get_Object(GetType(FSNServer.EstadosFactura))
                cEstadosFactura.LoadData(pag.Idioma)
                _chkEstados.DataTextField = "DEN"
                _chkEstados.DataValueField = "ID"
                _chkEstados.DataSource = cEstadosFactura.Data
                _chkEstados.DataBind()
            Catch ex As Exception

            End Try
            Controls.Add(_chkEstados)

            _reqEstados = New RequiredFieldValidatorForCheckBoxLists
            _reqEstados.ID = _chkEstados.ID & "_Req"
            _reqEstados.ErrorMessage = " * " & part.Textos(86)
            _reqEstados.ControlToValidate = Me.ID & "chkEstados"
            Controls.Add(_reqEstados)

            'Filtrar solicitudes
            _chkFiltroSolicitud = New CheckBoxList
            _chkFiltroSolicitud.ID = Me.ID & "chkFiltroSolicitud"
            _chkFiltroSolicitud.Items.Clear()
            _chkFiltroSolicitud.RepeatColumns = 2
            _chkFiltroSolicitud.Items.Add(New ListItem("Facturas abiertas por usted", 1))
            _chkFiltroSolicitud.Items.Add(New ListItem("Facturas pendientes de tratar", 2))
            _chkFiltroSolicitud.Items.Add(New ListItem("Facturas en las que participó", 2))
            _chkFiltroSolicitud.Items.Add(New ListItem("Otras facturas", 2))
            Controls.Add(_chkFiltroSolicitud)

            _reqFiltroSolicitud = New RequiredFieldValidatorForCheckBoxLists
            _reqFiltroSolicitud.ID = _chkFiltroSolicitud.ID & "_Req"
            _reqFiltroSolicitud.ErrorMessage = " * " & part.Textos(86)
            _reqFiltroSolicitud.ControlToValidate = Me.ID & "chkFiltroSolicitud"
            Controls.Add(_reqFiltroSolicitud)

        End Sub

        ''' <summary>
        ''' Presenta el contenido del control en el sistema de escritura especificado. Este método lo usan principalmente los programadores de controles. 
        ''' </summary>
        ''' <param name="writer">HtmlTextWriter que representa la secuencia de salida para presentar contenido HTML en el cliente.</param>
        Protected Overrides Sub RenderContents(ByVal writer As System.Web.UI.HtmlTextWriter)
            Dim part As FSPMWebPartFacturas = CType(WebPartToEdit, FSPMWebPartFacturas)
            writer.Write("<b>" & part.Textos(19) & ":</b> ")
            _txtTitulo.RenderControl(writer)
            writer.WriteBreak()
            writer.Write("<b>" & part.Textos(21) & ":</b> ")
            _txtAncho.RenderControl(writer)
            _reqAncho.RenderControl(writer)
            _ranAncho.RenderControl(writer)
            writer.WriteBreak()
            writer.Write("<b>" & part.Textos(58) & ":</b> ")
            _wneNumFacturas.RenderControl(writer)
            _reqNumFacturas.RenderControl(writer)
            writer.WriteBreak()
            writer.WriteBreak()
            writer.Write("<div style='border: 1px solid #CCCCCC;'><b>&nbsp;&nbsp;&nbsp;" & part.Textos(62) & "</b>")
            writer.WriteBreak()
            _chkTipoFactura.RenderControl(writer)
            writer.WriteBreak()
            writer.Write("</div>")
            writer.WriteBreak()
            If _chkFiltrosDisponibles.Items.Count > 0 Then
                writer.Write("<div style='border: 1px solid #CCCCCC;'><b>&nbsp;&nbsp;&nbsp;" & part.Textos(63) & "</b>")
                writer.WriteBreak()
                _chkFiltrosDisponibles.RenderControl(writer)
                writer.WriteBreak()
                writer.Write("</div>")
                writer.WriteBreak()
            End If
            writer.Write("<div style='border: 1px solid #CCCCCC;'>")
            writer.Write("<table cellpadding=2 border=0 width=95%>")
            writer.Write("<tr><td colspan=2><b>" & part.Textos(64) & "</b></td></tr>")
            writer.Write("<tr><td>" & part.Textos(65) & ":</td><td>")
            _wneImporteDesde.RenderControl(writer)
            writer.Write("</td>")
            writer.Write("<td>" & part.Textos(66) & ":</td><td>")
            _wneImporteHasta.RenderControl(writer)
            writer.Write("</td></tr></table>")
            writer.Write("</div>")
            writer.WriteBreak()
            writer.Write("<div style='border: 1px solid #CCCCCC;'>")
            writer.Write("<table cellpadding=2 border=0 width=95%>")
            writer.Write("<tr><td colspan=2><b>" & part.Textos(67) & "</b></td></tr>")
            writer.Write("<tr><td>" & part.Textos(68) & ":</td><td>")
            _wdcFechaDesde.RenderControl(writer)
            writer.Write("</td>")
            writer.Write("<td>" & part.Textos(69) & ":</td><td>")
            _wdcFechaHasta.RenderControl(writer)
            writer.Write("</td></tr></table>")
            writer.Write("</div>")
            writer.WriteBreak()
            writer.Write("<div style='border: 1px solid #CCCCCC;'>")
            writer.Write("<table cellpadding=2 border=0 width=95%>")
            writer.Write("<tr><td colspan=2><b>" & part.Textos(89) & "</b>&nbsp;&nbsp;&nbsp;")
            _reqEstados.RenderControl(writer)
            writer.Write("</td></tr>")
            writer.Write("<tr><td>")
            _chkEstados.RenderControl(writer)
            writer.Write("</td></tr></table></div>")
            writer.WriteBreak()
            writer.Write("<div style='border: 1px solid #CCCCCC;'>")
            _reqFiltroSolicitud.RenderControl(writer)
            _chkFiltroSolicitud.RenderControl(writer)
            writer.WriteBreak()
        End Sub

        ''' <summary>
        ''' Guarda los valores de un control EditorPart en las propiedades correspondientes del control WebPart asociado.
        ''' </summary>
        ''' <returns>Es true si la acción de guardar los valores del control EditorPart en el control WebPart se realiza correctamente; en caso contrario (si se produce un error), es false.</returns>
        Public Overrides Function ApplyChanges() As Boolean
            _reqAncho.Validate()
            _ranAncho.Validate()
            _reqEstados.Validate()
            _reqFiltroSolicitud.Validate()
            _reqNumFacturas.Validate()
            If _reqAncho.IsValid() And _ranAncho.IsValid() And _reqFiltroSolicitud.IsValid And _reqEstados.IsValid And _reqNumFacturas.IsValid Then
                Dim part As FSPMWebPartFacturas = CType(WebPartToEdit, FSPMWebPartFacturas)
                part.Title = _txtTitulo.Text
                part.Width = Unit.Pixel(CType(_txtAncho.Text, Integer))
                'Nº de solicitudes a mostrar
                If wneNumFacturas.Text <> "" Then
                    part.iNumFacturasAMostrar = CType(wneNumFacturas.Value, Integer)
                Else
                    part.iNumFacturasAMostrar = 0
                End If
                'Tipos de solicitud
                Dim tipoSolicitud As String = ""
                For Each x As ListItem In _chkTipoFactura.Items
                    If x.Selected Then
                        tipoSolicitud = tipoSolicitud & CType(x.Value, Integer) & ","
                    End If
                Next
                If Len(tipoSolicitud) > 0 Then
                    tipoSolicitud = Left(tipoSolicitud, Len(tipoSolicitud) - 1)
                End If
                part.mTiposSolicitudes = tipoSolicitud
                'Filtros disponibles
                Dim filtros As String = ""
                For Each x As ListItem In _chkFiltrosDisponibles.Items
                    If x.Selected Then
                        filtros = filtros & x.Value & "#"
                    End If
                Next
                part.mTablasFiltros = filtros 'tiene que acabar con el separador #
                'Importes y fechas
                part.dImporteDesde = IIf(_wneImporteDesde.Text <> Nothing, CType(_wneImporteDesde.Value, Double), Nothing)
                part.dImporteHasta = IIf(_wneImporteHasta.Text <> Nothing, CType(_wneImporteHasta.Value, Double), Nothing)
                part.dFechaDesde = CType(_wdcFechaDesde.Value, Date)
                part.dFechaHasta = CType(_wdcFechaHasta.Value, Date)
                'Estados
                Dim estados As String = ""
                For Each x As ListItem In _chkEstados.Items
                    If x.Selected Then
                        estados = estados & CType(x.Value, String) & ","
                    End If
                Next
                If Len(estados) > 0 Then
                    estados = Left(estados, Len(estados) - 1)
                End If
                part.Estados = estados
                part.mEstados = estados
                'Filtrar solicitudes
                part.bAbiertasPorUsted = _chkFiltroSolicitud.Items(0).Selected
                part.bPendientesTratar = _chkFiltroSolicitud.Items(1).Selected
                part.bParticipo = _chkFiltroSolicitud.Items(2).Selected
                part.bOtras = _chkFiltroSolicitud.Items(3).Selected
                'part.bTrasladadas = _chkFiltroSolicitud.Items(4).Selected
                'part.bPendientesDevolucion = _chkFiltroSolicitud.Items(5).Selected

                part.CargarSolicitudes()
                Return True
            Else
                Return False
            End If
        End Function
        ''' <summary>
        ''' Obtiene los valores de propiedad de un control WebPart correspondientes a su control EditorPart asociado.
        ''' </summary>
        Public Overrides Sub SyncChanges()
            Dim part As FSPMWebPartFacturas = CType(WebPartToEdit, FSPMWebPartFacturas)
            txtTitulo.Text = part.Title
            txtAncho.Text = part.Width.Value.ToString()
            wneNumFacturas.Value = IIf(part.iNumFacturasAMostrar = 0, "", part.iNumFacturasAMostrar)
            wneImporteDesde.Value = IIf(part.dImporteDesde = 0, "", part.dImporteDesde)
            wneImporteHasta.Value = IIf(part.dImporteHasta = 0, "", part.dImporteHasta)
            wdcFechaDesde.Value = part.dFechaDesde
            wdcFechaHasta.Value = part.dFechaHasta
            If part.mTiposSolicitudes = Nothing AndAlso part.mTablasFiltros = Nothing Then
                For Each x As ListItem In chkTipoSolicitud.Items
                    x.Selected = True
                Next
            Else
                If part.mTiposSolicitudes <> Nothing Then
                    For Each x As ListItem In chkTipoSolicitud.Items
                        x.Selected = IIf(InStr(part.mTiposSolicitudes, CType(x.Value, String), ) > 0, True, False)
                    Next
                End If
                If part.mTablasFiltros <> Nothing Then
                    For Each x As ListItem In chkFiltrosDisponibles.Items
                        x.Selected = IIf(InStr(part.mTablasFiltros, CType(x.Value, String), ) > 0, True, False)
                    Next
                End If
            End If

            If part.mEstados <> Nothing Then
                For Each x As ListItem In chkEstados.Items
                    x.Selected = IIf(InStr("," & part.mEstados & ",", "," & CType(x.Value, String) & ",", ) > 0, True, False)
                Next
            Else
                For Each x As ListItem In chkEstados.Items
                    x.Selected = True
                Next
            End If

            chkFiltroSolicitud.Items(0).Selected = part.bAbiertasPorUsted
            chkFiltroSolicitud.Items(1).Selected = part.bPendientesTratar
            chkFiltroSolicitud.Items(2).Selected = part.bParticipo
            chkFiltroSolicitud.Items(3).Selected = part.bOtras
        End Sub

        Private ReadOnly Property txtTitulo() As TextBox
            Get
                EnsureChildControls()
                Return _txtTitulo
            End Get
        End Property

        Private ReadOnly Property txtAncho() As TextBox
            Get
                EnsureChildControls()
                Return _txtAncho
            End Get
        End Property

        Private ReadOnly Property wneNumFacturas() As Infragistics.Web.UI.EditorControls.WebNumericEditor
            Get
                EnsureChildControls()
                Return _wneNumFacturas
            End Get
        End Property

        Private ReadOnly Property chkTipoSolicitud() As CheckBoxList
            Get
                EnsureChildControls()
                Return _chkTipoFactura
            End Get
        End Property

        Private ReadOnly Property chkFiltrosDisponibles() As CheckBoxList
            Get
                EnsureChildControls()
                Return _chkFiltrosDisponibles
            End Get
        End Property

        Private ReadOnly Property wneImporteDesde() As Infragistics.Web.UI.EditorControls.WebNumericEditor
            Get
                EnsureChildControls()
                Return _wneImporteDesde
            End Get
        End Property

        Private ReadOnly Property wneImporteHasta() As Infragistics.Web.UI.EditorControls.WebNumericEditor
            Get
                EnsureChildControls()
                Return _wneImporteHasta
            End Get
        End Property

        Private ReadOnly Property wdcFechaDesde() As Infragistics.Web.UI.EditorControls.WebDatePicker
            Get
                EnsureChildControls()
                Return _wdcFechaDesde
            End Get
        End Property

        Private ReadOnly Property wdcFechaHasta() As Infragistics.Web.UI.EditorControls.WebDatePicker
            Get
                EnsureChildControls()
                Return _wdcFechaHasta
            End Get
        End Property

        Private ReadOnly Property chkEstados() As CheckBoxList
            Get
                EnsureChildControls()
                Return _chkEstados
            End Get
        End Property

        Private ReadOnly Property chkFiltroSolicitud() As CheckBoxList
            Get
                EnsureChildControls()
                Return _chkFiltroSolicitud
            End Get
        End Property
    End Class

    <Personalizable(), WebBrowsable(), DefaultValue(5)> _
    Public Property NumSolicitudesAMostrar() As Integer
        Get
            Return iNumFacturasAMostrar
        End Get
        Set(ByVal value As Integer)
            iNumFacturasAMostrar = value
        End Set
    End Property

    <Personalizable(), WebBrowsable(), DefaultValue("")> _
    Public Property TiposSolicitudes() As String
        Get
            Return mTiposSolicitudes
        End Get
        Set(ByVal value As String)
            mTiposSolicitudes = value
        End Set
    End Property

    <Personalizable(), WebBrowsable(), DefaultValue("")> _
    Public Property TablasFiltros() As String
        Get
            Return mTablasFiltros
        End Get
        Set(ByVal value As String)
            mTablasFiltros = value
        End Set
    End Property

    <Personalizable(), WebBrowsable()> _
    Public Property ImporteDesde() As Double
        Get
            Return dImporteDesde
        End Get
        Set(ByVal value As Double)
            dImporteDesde = value
        End Set
    End Property

    <Personalizable(), WebBrowsable()> _
    Public Property ImporteHasta() As Double
        Get
            Return dImporteHasta
        End Get
        Set(ByVal value As Double)
            dImporteHasta = value
        End Set
    End Property

    <Personalizable(), WebBrowsable()> _
    Public Property FechaDesde() As Date
        Get
            Return dFechaDesde
        End Get
        Set(ByVal value As Date)
            dFechaDesde = value
        End Set
    End Property

    <Personalizable(), WebBrowsable()> _
    Public Property FechaHasta() As Date
        Get
            Return dFechaHasta
        End Get
        Set(ByVal value As Date)
            dFechaHasta = value
        End Set
    End Property

    <Personalizable(), WebBrowsable(), DefaultValue("")> _
    Public Property Estados() As String
        Get
            Return mEstados
        End Get
        Set(ByVal value As String)
            mEstados = value
        End Set
    End Property

    <Personalizable(), WebBrowsable(), DefaultValue(True)> _
    Public Property AbiertasPorUsted() As Boolean
        Get
            Return bAbiertasPorUsted
        End Get
        Set(ByVal value As Boolean)
            bAbiertasPorUsted = value
        End Set
    End Property

    <Personalizable(), WebBrowsable(), DefaultValue(True)> _
    Public Property Participo() As Boolean
        Get
            Return bParticipo
        End Get
        Set(ByVal value As Boolean)
            bParticipo = value
        End Set
    End Property

    <Personalizable(), WebBrowsable(), DefaultValue(True)> _
    Public Property PendientesTratar() As Boolean
        Get
            Return bPendientesTratar
        End Get
        Set(ByVal value As Boolean)
            bPendientesTratar = value
        End Set
    End Property

    <Personalizable(), WebBrowsable(), DefaultValue(True)> _
    Public Property Trasladadas() As Boolean
        Get
            Return bTrasladadas
        End Get
        Set(ByVal value As Boolean)
            bTrasladadas = value
        End Set
    End Property

    <Personalizable(), WebBrowsable(), DefaultValue(True)> _
    Public Property PendientesDevolucion() As Boolean
        Get
            Return bPendientesDevolucion
        End Get
        Set(ByVal value As Boolean)
            bPendientesDevolucion = value
        End Set
    End Property

    <Personalizable(), WebBrowsable(), DefaultValue(True)> _
    Public Property Otras() As Boolean
        Get
            Return bOtras
        End Get
        Set(ByVal value As Boolean)
            bOtras = value
        End Set
    End Property

    ''' <summary>
    ''' Devuelve el idioma de la página que alberga el control.
    ''' </summary>
    ''' <returns>El código de idioma.</returns>
    Private Function Idioma() As String
        If Not DesignMode Then
            If Me.Page Is Nothing AndAlso TypeOf Me.Page Is FSNServer.IFSNPage Then
                Return CType(Me.Page, FSNServer.IFSNPage).Idioma
            Else
                For Each x As String In HttpContext.Current.Session.Keys
                    If TypeOf HttpContext.Current.Session(x) Is FSNServer.User Then
                        Return CType(HttpContext.Current.Session(x), FSNServer.User).Idioma
                    End If
                Next
                Return String.Empty
            End If
        Else
            Return String.Empty
        End If
    End Function

    Public ReadOnly Property Textos(ByVal Index As Short) As String
        Get
            If _Textos Is Nothing Or Idioma() <> _sIdioma Then
                If Not DesignMode Then
                    Dim FSPMDict As New FSNServer.Dictionary
                    _sIdioma = Idioma()
                    If HttpContext.Current.Cache("Textos_" & _sIdioma & "_" & FSNLibrary.TiposDeDatos.ModulosIdiomas.Webparts) Is Nothing Then
                        FSPMDict.LoadData(FSNLibrary.TiposDeDatos.ModulosIdiomas.Webparts, _sIdioma)
                        HttpContext.Current.Cache.Insert("Textos_" & _sIdioma & "_" & FSNLibrary.TiposDeDatos.ModulosIdiomas.Webparts, FSPMDict.Data, Nothing, Caching.Cache.NoAbsoluteExpiration,
                                                         New TimeSpan(0, ConfigurationManager.AppSettings("TiempoExpiracionCacheMin"), 0))
                    End If
                    _Textos = HttpContext.Current.Cache("Textos_" & _sIdioma & "_" & FSNLibrary.TiposDeDatos.ModulosIdiomas.Webparts)
                    Return _Textos.Tables(0).Rows(Index).Item(1)
                Else
                    Return String.Empty
                End If
            Else
                Return _Textos.Tables(0).Rows(Index).Item(1)
            End If
        End Get
    End Property

    Public Overrides Property Title() As String
        Get
            If String.IsNullOrEmpty(MyBase.Title) Then
                If String.IsNullOrEmpty(sTitulo) Then
                    If Me.Page Is Nothing And String.IsNullOrEmpty(Textos(255)) Then
                        MyBase.Title = Textos(255)
                        sTitulo = MyBase.Title
                    Else
                        sTitulo = Textos(255)
                    End If
                End If
                Return sTitulo
            Else
                sTitulo = MyBase.Title
                Return MyBase.Title
            End If
        End Get
        Set(ByVal value As String)
            MyBase.Title = value
        End Set
    End Property

    ''' <summary>
    ''' Carga los datos por defecto para el control
    ''' </summary>
    Public Sub New()
        Me.Width = Unit.Pixel(300)
        iModo = 0
        iNumFacturasAMostrar = 5
        bAbiertasPorUsted = True
    End Sub

    ''' <summary>
    ''' Lo llama el marco de trabajo de las páginas ASP.NET para indicar a los controles de servidor que utilizan la implementación basada en la composición que creen los controles secundarios que contengan como forma de preparar la devolución o representación de los datos.
    ''' </summary>
    Protected Overrides Sub CreateChildControls()
        Controls.Clear()
        Dim img As HtmlImage = New HtmlImage()
        img.Src = Me.Page.ClientScript.GetWebResourceUrl(GetType(FSPMWebPartFacturas), "Fullstep.PMWebControls.trans.gif")
        img.Width = 300
        img.Height = 1
        Me.Controls.Add(img)
        Dim tabla As HtmlTable = New HtmlTable()
        tabla.Width = "100%"
        tabla.Height = "55px"
        Dim fila As HtmlTableRow = New HtmlTableRow()
        fila.Height = "55px"
        Dim celda As HtmlTableCell = New HtmlTableCell()
        celda.Width = "90%"
        lblTitulo = New Label()
        lblTitulo.CssClass = "Rotulo"
        celda.Controls.Add(lblTitulo)
        celda.Controls.Add(New LiteralControl("<br/>"))
        lblMensaje = New Label
        lblMensaje.CssClass = "EtiquetaGrande"
        celda.Controls.Add(lblMensaje)
        celda.Controls.Add(New LiteralControl("<br/>"))
        fila.Cells.Add(celda)
        celdaImagen = New HtmlTableCell
        celdaImagen.Align = "right"
        celdaImagen.VAlign = "bottom"
        img.Width = 125
        celdaImagen.Controls.Add(img)
        celdaImagen.Style.Add("background-image", Me.Page.ClientScript.GetWebResourceUrl(GetType(FSPMWebPartFacturas), "Fullstep.PMWebControls.BusquedaSolicitudes.gif"))
        celdaImagen.Style.Add("background-position", "top left")
        celdaImagen.Style.Add("background-repeat", "no-repeat")
        lnkVerTodos = New FSNWebControls.FSNHyperlink()
        celdaImagen.Controls.Add(lnkVerTodos)
        fila.Cells.Add(celdaImagen)
        tabla.Rows.Add(fila)
        fila = New HtmlTableRow
        celda = New HtmlTableCell
        celda.ColSpan = 2
        lstSolicitudes = New DataList()
        lstSolicitudes.Width = Unit.Percentage(100)
        lstSolicitudes.ItemTemplate = New DataListTemplate()
        celda.Controls.Add(lstSolicitudes)
        fila.Cells.Add(celda)
        tabla.Rows.Add(fila)
        lblError = New Label
        lblError.CssClass = "Rotulo"
        lblError.Visible = False
        celda.Controls.Add(lblError)
        celda.Controls.Add(New LiteralControl("<br/>"))
        fila.Cells.Add(celda)
        tabla.Rows.Add(fila)
        Me.Controls.Add(tabla)

        lblTitulo.Text = Textos(242)
        sTitulo = Textos(242)

        If Not Page.IsPostBack Then
            CargarSolicitudes()
        End If

    End Sub

    ''' <summary>
    ''' Refresca los datos del webpart
    ''' </summary>
    Public Sub Actualizar()
        CreateChildControls()
        CargarSolicitudes()
    End Sub

    ''' <summary>
    ''' Envía el contenido del control de servidor a un objeto HtmlTextWriter que se proporciona y almacena la información de seguimiento sobre el control si dicho seguimiento está habilitado.
    ''' </summary>
    ''' <param name="writer">Objeto HTmlTextWriter que recibe el contenido del control.</param>
    Public Overrides Sub RenderControl(ByVal writer As System.Web.UI.HtmlTextWriter)
        MyBase.RenderControl(writer)
    End Sub

    ''' <summary>
    ''' Carga los pedidos en el DataList del control.
    ''' </summary>
    Private Sub CargarSolicitudes()
        If Not Me.Page Is Nothing AndAlso TypeOf Me.Page Is FSNServer.IFSNPage Then
            Dim lista As New List(Of mOrden)
            Dim iNumFacturasObtenidas As Integer
            Dim pag As FSNServer.IFSNPage = CType(Me.Page, FSNServer.IFSNPage)
            Dim FSWSServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim cFacturas As FSNServer.Facturas

            Try
                Dim ds As DataSet
                cFacturas = FSWSServer.Get_Object(GetType(FSNServer.Facturas))
                'el idioma sobra
                ds = cFacturas.LoadWebPart(pag.Usuario.CodPersona, pag.Idioma, iNumFacturasAMostrar, mTiposSolicitudes, mTablasFiltros, dImporteDesde, dImporteHasta, dFechaDesde, dFechaHasta, mEstados, bAbiertasPorUsted, bParticipo, bPendientesTratar, bPendientesDevolucion, bTrasladadas, bOtras)
                If ds.Tables.Count > 0 Then
                    iNumFacturasObtenidas = ds.Tables(0).Rows.Count
                    lblMensaje.Text = iNumFacturasObtenidas & " " & Textos(243)
                    celdaImagen.Visible = (iNumFacturasObtenidas > 0)
                    lnkVerTodos.Visible = (iNumFacturasObtenidas > iNumFacturasAMostrar) 'se muestra el enlace a ver todas.
                
                    If ds.Tables(0).Rows.Count > 0 Then
                        Dim num As Integer = 0
                        For Each fila As DataRow In ds.Tables(0).Rows
                            Dim mo As New mOrden()
                            mo.Instancia = fila.Item("INSTANCIA")
                            mo.FacturaId = fila.Item("FACTURA")
                            mo.Factura = Textos(244) & ": " & fila.Item("NUM_FACTURA") '& " " & Textos(245) & ":" & fila.Item("INSTANCIA")
                            mo.Importe = FSNLibrary.FormatNumber(fila.Item("IMPORTE"), pag.Usuario.NumberFormat) & " " & fila.Item("MON")
                            mo.Proveedor = fila.Item("COD_PROVEEDOR") & " - " & fila.Item("DEN_PROVEEDOR")
                            mo.Empresa = fila.Item("COD_EMPRESA") & " - " & fila.Item("DEN_EMPRESA")
                            mo.Gestor = fila.Item("GESTOR")
                            mo.Receptor = fila.Item("RECEPTOR")
                            lista.Add(mo)
                            num = num + 1
                            If num >= iNumFacturasAMostrar Then Exit For
                        Next
                    End If
                End If

                lstSolicitudes.DataSource = lista
                lstSolicitudes.DataBind()

            Catch ex As Exception
                If iNumFacturasObtenidas = 0 Then
                    celdaImagen.Visible = False
                End If
                lblError.Text = Textos(239)
                lblError.Visible = True
            End Try

            If Not DesignMode() Then
                lnkVerTodos.Text = IIf(iModo = 2, Textos(76), Textos(87))
                lnkVerTodos.NavigateUrl = ConfigurationManager.AppSettings("rutaPM2008") & "Facturas/VisorFacturas.aspx"
                lnkVerTodos.Attributes.Add("onclick", "TempCargando();")
            End If
        End If
    End Sub

    ''' <summary>
    ''' Devuelve una colección de controles EditorPart personalizados que pueden utilizarse para editar un control WebPart cuando se encuentra en modo de edición.
    ''' </summary>
    ''' <returns>Una colección EditorPartCollection que contiene controles EditorPart personalizados asociados a un control WebPart.</returns>
    Public Overrides Function CreateEditorParts() As System.Web.UI.WebControls.WebParts.EditorPartCollection
        Dim editorArray As New ArrayList()
        Dim edPart As New ControlEditor()
        edPart.ID = Me.ID & "_editor1"
        editorArray.Add(edPart)
        Dim editorParts As New EditorPartCollection(editorArray)
        Return editorParts
    End Function

    Public Overrides ReadOnly Property WebBrowsableObject() As Object
        Get
            Return Me
        End Get
    End Property

    ''' <summary>
    ''' Registra el control como un control cuyo estado se debe conservar.
    ''' </summary>
    ''' <param name="e">Objeto EventArgs que contiene los datos del evento.</param>
    Protected Overrides Sub OnInit(ByVal e As System.EventArgs)
        MyBase.OnInit(e)
        Page.RegisterRequiresControlState(Me)
    End Sub

    ''' <summary>
    ''' Guarda los cambios de estado del control.
    ''' </summary>
    ''' <returns>Devuelve el estado actual del control de servidor.</returns>
    Protected Overrides Function SaveControlState() As Object
        Dim obj As Object = MyBase.SaveControlState()
        If obj IsNot Nothing Then
            obj = New Pair(obj, _Textos)
        Else
            obj = _Textos
        End If
        obj = New Pair(obj, _sIdioma)
        Return obj
    End Function

    ''' <summary>
    ''' Restaura información de estado del control.
    ''' </summary>
    ''' <param name="state">Object que representa el estado del control que se va a restaurar.</param>
    Protected Overrides Sub LoadControlState(ByVal state As Object)
        If (state IsNot Nothing) Then
            Dim p As Pair = TryCast(state, Pair)
            _sIdioma = p.Second
            state = p.First
            p = TryCast(state, Pair)
            If p IsNot Nothing Then
                MyBase.LoadControlState(p.First)
                _Textos = CType(p.Second, DataSet)
            Else
                If (TypeOf (state) Is String()) Then
                    _Textos = CType(state, DataSet)
                Else
                    MyBase.LoadControlState(state)
                End If
            End If
        End If
    End Sub

    Private Sub FSPMWebPartFacturas_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.DesignMode Then
            ScriptManager.RegisterClientScriptResource(Me.Page, GetType(QAWebPartClientScript), "Fullstep.PMWebControls.FSQAWebPartsQA.js")
        End If
    End Sub
End Class
