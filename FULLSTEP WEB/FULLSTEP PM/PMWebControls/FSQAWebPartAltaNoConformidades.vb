﻿Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.HtmlControls
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.ComponentModel
Imports System.Configuration

Public Class FSQAWebPartAltaNoConformidades
    Inherits WebPart

    Private lblTitulo As Label

    Private lblError As Label
    Private lstSolicitudes As DataList
    Private sTitulo As String
    Private _Textos(18) As String
    Private _sIdioma As String

    Private Class DataListTemplate
        Implements ITemplate

        ''' <summary>
        ''' Define el objeto Control al que pertenecen los controles secundarios y las plantillas. Estos controles secundarios están a su vez definidos en una plantilla en línea.
        ''' </summary>
        ''' <param name="container">Objeto Control que contiene las instancias de los controles de la plantilla en línea.</param>
        Private Sub InstantiateIn(ByVal container As Control) _
           Implements ITemplate.InstantiateIn


            Dim tabla As New HtmlTable()
            tabla.Width = "100%"
            tabla.Attributes.Add("class", "ListItemLink")
            AddHandler tabla.DataBinding, AddressOf IrAAltaNoConformidad_DataBinding
            Dim fila As New HtmlTableRow()
            Dim celda As New HtmlTableCell()

            celda.Style.Add(HtmlTextWriterStyle.FontWeight, "bold")
            Dim lc As New Literal()
            AddHandler lc.DataBinding, AddressOf NoConformidadCOD_DataBinding
            celda.Width = "30%"
            celda.Controls.Add(lc)
            fila.Cells.Add(celda)


            celda = New HtmlTableCell()
            lc = New Literal()
            AddHandler lc.DataBinding, AddressOf NoConformidadDEN_DataBinding
            celda.Align = "left"
            celda.Width = "70%"
            celda.Controls.Add(lc)
            fila.Cells.Add(celda)



            tabla.Rows.Add(fila)
            container.Controls.Add(tabla)
        End Sub

        ''' <summary>
        ''' Se produce al enlazar la etiqueta de la plantilla en que se muestra la empresa con el origen de datos.
        ''' </summary>
        Private Sub NoConformidadCOD_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim lc As Literal = CType(sender, Literal)
            Dim container As DataListItem = CType(lc.NamingContainer, DataListItem)
            lc.Text = DataBinder.Eval(container.DataItem, "COD")
        End Sub

        ''' <summary>
        ''' Se produce al enlazar la etiqueta de la plantilla en que se muestra la empresa con el origen de datos.
        ''' </summary>
        Private Sub NoConformidadDEN_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim lc As Literal = CType(sender, Literal)
            Dim container As DataListItem = CType(lc.NamingContainer, DataListItem)
            lc.Text = DataBinder.Eval(container.DataItem, "DEN")
        End Sub
        ''' <summary>
        ''' Se produce al enlazar la tabla de la plantilla que muestra la pagina de alta de noconformidad.
        ''' </summary>
        ''' <param name="sender">propios del evento</param>
        ''' <param name="e">propios del evento</param>        
        ''' <remarks>Llamada desde=misma pagina; Tiempo máximo=0</remarks>
        Private Sub IrAAltaNoConformidad_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim tab As HtmlTable = CType(sender, HtmlTable)
            Dim container As DataListItem = CType(tab.NamingContainer, DataListItem)
            tab.Attributes.Add("onclick", "TempCargando(); window.location.href='" & ConfigurationManager.AppSettings("rutaQA") & "frames.aspx?pagina=" & HttpContext.Current.Server.UrlEncode("NoConformidad/altaNoConformidad.aspx?TipoSolicit=" & DataBinder.Eval(container.DataItem, "ID") & "&DesdeInicio=1") & "';")
        End Sub

    End Class

    <Serializable()> _
    Public Class mOrden
        Private mlID As Long
        Private msCod As String
        Private msDen As String


        Public Property ID() As Long
            Get
                Return mlID
            End Get
            Set(ByVal value As Long)
                mlID = value
            End Set
        End Property

        Public Property Cod() As String
            Get
                Return msCod
            End Get
            Set(ByVal value As String)
                msCod = value
            End Set
        End Property

        Public Property Den() As String
            Get
                Return msDen
            End Get
            Set(ByVal value As String)
                msDen = value
            End Set
        End Property
    End Class


    Private Class ControlEditor
        Inherits EditorPart

        Private _txtTitulo As TextBox
        Private _txtAncho As TextBox
        Private _reqAncho As RequiredFieldValidator
        Private _ranAncho As RangeValidator
        Private _lstTipoSolicitud As ListBox

        ''' <summary>
        ''' Lo llama el marco de trabajo de las páginas ASP.NET para indicar a los controles de servidor que utilizan la implementación basada en la composición que creen los controles secundarios que contengan como forma de preparar la devolución o representación de los datos.
        ''' </summary>
        ''' <remarks>Llamada desde=misma pagina; Tiempo máximo=0</remarks>
        Protected Overrides Sub CreateChildControls()
            Dim part As FSQAWebPartAltaNoConformidades = CType(WebPartToEdit, FSQAWebPartAltaNoConformidades)
            Controls.Clear()
            _txtTitulo = New TextBox()
            _txtTitulo.Width = Unit.Pixel(250)
            _txtTitulo.Text = part.Title
            Controls.Add(_txtTitulo)
            _txtAncho = New TextBox()
            _txtAncho.MaxLength = 3
            _txtAncho.Width = Unit.Pixel(30)
            _txtAncho.Text = part.Width.Value.ToString()
            _txtAncho.ID = Me.ID & "_txtAncho"
            Controls.Add(_txtAncho)
            _reqAncho = New RequiredFieldValidator()
            _reqAncho.ID = _txtAncho.ID & "_Req"
            _reqAncho.ErrorMessage = "*"
            _reqAncho.ControlToValidate = _txtAncho.ID
            Controls.Add(_reqAncho)
            _ranAncho = New RangeValidator()
            _ranAncho.ID = _txtAncho.ID & "_Ran"
            _ranAncho.Type = ValidationDataType.Integer
            _ranAncho.MaximumValue = 500
            _ranAncho.MinimumValue = 200
            _ranAncho.ErrorMessage = "(200-500)"
            _ranAncho.ControlToValidate = _txtAncho.ID
            Controls.Add(_ranAncho)

            _lstTipoSolicitud = New ListBox
            _lstTipoSolicitud.Items.Clear()
            _lstTipoSolicitud.ID = Me.ID & "_lstTipoSolicitud"

            Controls.Add(_lstTipoSolicitud)
        End Sub

        ''' <summary>
        ''' Presenta el contenido del control en el sistema de escritura especificado. Este método lo usan principalmente los programadores de controles. 
        ''' </summary>
        ''' <param name="writer">HtmlTextWriter que representa la secuencia de salida para presentar contenido HTML en el cliente.</param>
        ''' <remarks>Llamada desde=misma pagina; Tiempo máximo=0</remarks>
        Protected Overrides Sub RenderContents(ByVal writer As System.Web.UI.HtmlTextWriter)
            Dim part As FSQAWebPartAltaNoConformidades = CType(WebPartToEdit, FSQAWebPartAltaNoConformidades)
            writer.Write("<b>" & part.Textos(13) & ":</b> ")
            _txtTitulo.RenderControl(writer)
            writer.WriteBreak()
            writer.Write("<b>" & part.Textos(15) & ":</b> ")
            _txtAncho.RenderControl(writer)
            _reqAncho.RenderControl(writer)
            _ranAncho.RenderControl(writer)

            writer.Write("<div style='border: 0px solid #CCCCCC;'>")
            writer.Write("<table border=0 cellpadding=2 width=1%>")
            writer.Write("<tr><td>&nbsp;</td></tr>")
            writer.Write("</table>")
            writer.Write("</div>")
            writer.WriteBreak()

        End Sub

        ''' <summary>
        ''' Guarda los valores de un control EditorPart en las propiedades correspondientes del control WebPart asociado.
        ''' </summary>
        ''' <returns>Es true si la acción de guardar los valores del control EditorPart en el control WebPart se realiza correctamente; en caso contrario (si se produce un error), es false.</returns>
        ''' <remarks>Llamada desde=misma pagina; Tiempo máximo=0</remarks>
        Public Overrides Function ApplyChanges() As Boolean
            _reqAncho.Validate()
            _ranAncho.Validate()
            If _reqAncho.IsValid() And _ranAncho.IsValid() Then
                Dim part As FSQAWebPartAltaNoConformidades = CType(WebPartToEdit, FSQAWebPartAltaNoConformidades)
                part.Title = _txtTitulo.Text
                'part.lblTitulo.Text = _txtTitulo.Text
                part.Width = Unit.Pixel(CType(_txtAncho.Text, Integer))

                part.CargarSolicitudes()
                Return True
            Else
                Return False
            End If
        End Function
        ''' <summary>
        ''' Obtiene los valores de propiedad de un control WebPart correspondientes a su control EditorPart asociado.
        ''' </summary>
        ''' <remarks>Llamada desde=misma pagina; Tiempo máximo=0</remarks>
        Public Overrides Sub SyncChanges()
            Dim part As FSQAWebPartAltaNoConformidades = CType(WebPartToEdit, FSQAWebPartAltaNoConformidades)
            txtTitulo.Text = part.Title
            txtAncho.Text = part.Width.Value.ToString()

        End Sub

        Private ReadOnly Property txtTitulo() As TextBox
            Get
                EnsureChildControls()
                Return _txtTitulo
            End Get
        End Property

        Private ReadOnly Property txtAncho() As TextBox
            Get
                EnsureChildControls()
                Return _txtAncho
            End Get
        End Property

        Private ReadOnly Property lstTipoSolicitud() As ListBox
            Get
                EnsureChildControls()
                Return _lstTipoSolicitud
            End Get
        End Property
    End Class

    ''' <summary>
    ''' Devuelve el idioma de la página que alberga el control.
    ''' </summary>
    ''' <returns>El código de idioma.</returns>
    Private Function Idioma() As String
        If Not DesignMode Then
            If Me.Page Is Nothing AndAlso TypeOf Me.Page Is FSNServer.IFSNPage Then
                Return CType(Me.Page, FSNServer.IFSNPage).Idioma
            Else
                For Each x As String In HttpContext.Current.Session.Keys
                    If TypeOf HttpContext.Current.Session(x) Is FSNServer.User Then
                        Return CType(HttpContext.Current.Session(x), FSNServer.User).Idioma
                    End If
                Next
                Return String.Empty
            End If
        Else
            Return String.Empty
        End If
    End Function

    Public ReadOnly Property Textos(ByVal Index As Short) As String
        Get
            If String.IsNullOrEmpty(_Textos(Index)) Or Idioma() <> _sIdioma Then
                If Not DesignMode Then
                    Dim FSNDict As FSNServer.Dictionary = New FSNServer.Dictionary()
                    _sIdioma = Idioma()

                    Try
                        FSNDict.LoadData(FSNLibrary.TiposDeDatos.ModulosIdiomas.Webparts, _sIdioma)

                        For i As Short = 0 To 2
                            _Textos(i) = FSNDict.Data.Tables(0).Rows(i + 5).Item(1)
                        Next
                        For i As Short = 3 To 16
                            _Textos(i) = FSNDict.Data.Tables(0).Rows(i + 6).Item(1)
                        Next
                        _Textos(17) = FSNDict.Data.Tables(0).Rows(98).Item(1)
                        _Textos(18) = FSNDict.Data.Tables(0).Rows(239).Item(1)
                        Return _Textos(Index)
                    Catch ex As Exception
                        'Si falla la carga de textos devolvemos la cadena vacia para que la carga de la página siga su curso
                        Return String.Empty
                    End Try
                Else
                    Return String.Empty
                End If
            Else
                Return _Textos(Index)
            End If
        End Get
    End Property

    'Titulo pagina principal
    Public Overrides Property Title() As String
        Get
            If String.IsNullOrEmpty(MyBase.Title) Then
                If String.IsNullOrEmpty(sTitulo) Then
                    If Me.Page Is Nothing And String.IsNullOrEmpty(Textos(17)) Then
                        MyBase.Title = Textos(17)
                        sTitulo = MyBase.Title
                    Else
                        sTitulo = Textos(17)
                    End If
                End If
                Return sTitulo
            Else
                sTitulo = MyBase.Title
                Return MyBase.Title
            End If
        End Get
        Set(ByVal value As String)
            MyBase.Title = value
        End Set
    End Property

    ''' <summary>
    ''' Carga los datos por defecto para el control
    ''' </summary>
    Public Sub New()
        Me.Width = Unit.Pixel(300)
    End Sub

    ''' <summary>
    ''' Lo llama el marco de trabajo de las páginas ASP.NET para indicar a los controles de servidor que utilizan la implementación basada en la composición que creen los controles secundarios que contengan como forma de preparar la devolución o representación de los datos.
    ''' Muestra los datos del webPart.
    ''' </summary>
    ''' <remarks>Llamada: Cuando se crea un objeto; Tiempo Ejecucion:=0seg.</remarks>
    Protected Overrides Sub CreateChildControls()
        Controls.Clear()
        Dim img As HtmlImage = New HtmlImage()
        img.Src = Me.Page.ClientScript.GetWebResourceUrl(GetType(FSQAWebPartAltaNoConformidades), "Fullstep.PMWebControls.trans.gif")
        img.Width = 300
        img.Height = 1
        Me.Controls.Add(img)
        Dim tabla As HtmlTable = New HtmlTable()
        tabla.Width = "100%"
        tabla.Height = "55px"
        Dim fila As HtmlTableRow = New HtmlTableRow()
        fila.Height = "30px"
        Dim celda As HtmlTableCell = New HtmlTableCell()
        celda.Width = "90%"
        lblTitulo = New Label()
        lblTitulo.CssClass = "Rotulo"
        celda.Controls.Add(lblTitulo)
        celda.Controls.Add(New LiteralControl("<br/>"))
        fila.Cells.Add(celda)
        celda = New HtmlTableCell
        celda.Align = "right"
        celda.VAlign = "bottom"
        img.Width = 125
        celda.Controls.Add(img)
        If DesignMode Then
            celda.Style.Add("background-image", "images/NoConformidades.gif")
        Else
            If CStr(ViewState("ImagenNoConformidad")) <> String.Empty Then
                celda.Style.Add("background-image", Replace(CStr(ViewState("ImagenNoConformidad")), "~/", HttpContext.Current.Request.ApplicationPath & "/"))
            Else
                celda.Style.Add("background-image", Me.Page.ClientScript.GetWebResourceUrl(GetType(FSQAWebPartNoConfAbiertas), "Fullstep.PMWebControls.NoConformidades.gif"))
            End If
        End If
        celda.Style.Add("background-position", "bottom right")
        celda.Style.Add("background-repeat", "no-repeat")
        fila.Cells.Add(celda)
        tabla.Rows.Add(fila)
        fila = New HtmlTableRow
        celda = New HtmlTableCell
        celda.ColSpan = 2

        lstSolicitudes = New DataList()
        lstSolicitudes.Width = Unit.Percentage(100)
        lstSolicitudes.ItemTemplate = New DataListTemplate()
        celda.Controls.Add(lstSolicitudes)
        fila.Cells.Add(celda)
        tabla.Rows.Add(fila)

        lblError = New Label
        lblError.Visible = False
        celda.Controls.Add(lblError)
        fila.Cells.Add(celda)
        tabla.Rows.Add(fila)

        Me.Controls.Add(tabla)


        'Texto webPart pequeño
        lblTitulo.Text = _Textos(17) 'Alta de no Conformidades

        If Not Page.IsPostBack Then _
            CargarSolicitudes()
    End Sub

    ''' <summary>
    ''' Refresca los datos del webpart
    ''' </summary>
    Public Sub Actualizar()
        CreateChildControls()
        CargarSolicitudes()
    End Sub


    ''' <summary>
    ''' Envía el contenido del control de servidor a un objeto HtmlTextWriter que se proporciona y almacena la información de seguimiento sobre el control si dicho seguimiento está habilitado.
    ''' </summary>
    ''' <param name="writer">Objeto HTmlTextWriter que recibe el contenido del control.</param>
    Public Overrides Sub RenderControl(ByVal writer As System.Web.UI.HtmlTextWriter)
        MyBase.RenderControl(writer)
    End Sub

    ''' <summary>
    ''' Carga las NoConformidades del usuario en el DataList del control.
    ''' </summary>
    ''' <remarks>Llamadas desde: ApplyChanges//CreateChildControls;TiempoEjecucion: 0,3seg.</remarks>
    Private Sub CargarSolicitudes()
        If Not Me.Page Is Nothing AndAlso TypeOf Me.Page Is FSNServer.IFSNPage Then
            Dim pag As FSNServer.IFSNPage = CType(Me.Page, FSNServer.IFSNPage)
            Dim FSWSServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim cSolicitudes As FSNServer.Solicitudes
            Dim lista As New List(Of mOrden)

            Try
                cSolicitudes = FSWSServer.Get_Object(GetType(FSNServer.Solicitudes))

                cSolicitudes.LoadData(pag.Usuario.Cod, pag.Idioma, bEsCombo:=1, iTipoSolicitud:=3, bComboAltaNC:=True)
                If cSolicitudes.Data.Tables.Count > 0 Then
                    For Each fila As DataRow In cSolicitudes.Data.Tables(0).Rows

                        Dim mo As New mOrden()
                        If Not IsDBNull(fila.Item("ID")) Then
                            mo.ID = fila.Item("ID")
                            mo.Cod = fila.Item("COD")
                            mo.Den = fila.Item("DEN")
                            lista.Add(mo)
                        End If
                    Next
                End If

                lstSolicitudes.DataSource = lista
                lstSolicitudes.DataBind()
            Catch ex As Exception
                lblError.Text = Textos(18)
                lblError.Visible = True
            End Try

        End If
    End Sub

    ''' <summary>
    ''' Devuelve una colección de controles EditorPart personalizados que pueden utilizarse para editar un control WebPart cuando se encuentra en modo de edición.
    ''' </summary>
    ''' <returns>Una colección EditorPartCollection que contiene controles EditorPart personalizados asociados a un control WebPart.</returns>
    Public Overrides Function CreateEditorParts() As System.Web.UI.WebControls.WebParts.EditorPartCollection
        Dim editorArray As New ArrayList()
        Dim edPart As New ControlEditor()
        edPart.ID = Me.ID & "_editor1"
        editorArray.Add(edPart)
        Dim editorParts As New EditorPartCollection(editorArray)
        Return editorParts
    End Function

    Public Overrides ReadOnly Property WebBrowsableObject() As Object
        Get
            Return Me
        End Get
    End Property

    ''' <summary>
    ''' Registra el control como un control cuyo estado se debe conservar.
    ''' </summary>
    ''' <param name="e">Objeto EventArgs que contiene los datos del evento.</param>
    Protected Overrides Sub OnInit(ByVal e As System.EventArgs)
        MyBase.OnInit(e)
        Page.RegisterRequiresControlState(Me)
    End Sub

    ''' <summary>
    ''' Guarda los cambios de estado del control.
    ''' </summary>
    ''' <returns>Devuelve el estado actual del control de servidor.</returns>
    Protected Overrides Function SaveControlState() As Object
        Dim obj As Object = MyBase.SaveControlState()
        If obj IsNot Nothing Then
            obj = New Pair(obj, _Textos)
        Else
            obj = _Textos
        End If
        obj = New Pair(obj, _sIdioma)
        Return obj
    End Function

    ''' <summary>
    ''' Restaura información de estado del control.
    ''' </summary>
    ''' <param name="state">Object que representa el estado del control que se va a restaurar.</param>
    Protected Overrides Sub LoadControlState(ByVal state As Object)
        If (state IsNot Nothing) Then
            Dim p As Pair = TryCast(state, Pair)
            _sIdioma = p.Second
            state = p.First
            p = TryCast(state, Pair)
            If p IsNot Nothing Then
                MyBase.LoadControlState(p.First)
                _Textos = CType(p.Second, String())
            Else
                If (TypeOf (state) Is String()) Then
                    _Textos = CType(state, String())
                Else
                    MyBase.LoadControlState(state)
                End If
            End If
        End If
    End Sub
End Class
