﻿Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.HtmlControls
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.ComponentModel
Imports System.Configuration
Imports Fullstep.FSNLibrary.TiposDeDatos
Imports Fullstep.FSNLibrary

Public Class FSPMWebPartBusquedaSolicitudes
    Inherits WebPart

    Private lblTitulo As Label
    Private lblMensaje As Label
    Private lblError As Label
    Private lstSolicitudes As DataList
    Private lnkVerTodos As FSNWebControls.FSNHyperlink
    Private celdaImagen As HtmlTableCell
    Private iModo As Integer
    Private iNumSolicitudesAMostrar As Integer
    Private dImporteDesde As Double
    Private dImporteHasta As Double
    Private dFechaDesde As Date
    Private dFechaHasta As Date
    Private mEstados As String
    Private bAbiertasPorUsted As Boolean
    Private bParticipo As Boolean
    Private bTrasladadas As Boolean
    Private bOtras As Boolean
    Private sTitulo As String
    Private _Textos As DataSet
    Private _sIdioma As String

    Private Class DataListTemplate
        Implements ITemplate
        ''' <summary>
        ''' Define el objeto Control al que pertenecen los controles secundarios y las plantillas. Estos controles secundarios están a su vez definidos en una plantilla en línea.
        ''' </summary>
        ''' <param name="container">Objeto Control que contiene las instancias de los controles de la plantilla en línea.</param>
        Private Sub InstantiateIn(ByVal container As Control) _
           Implements ITemplate.InstantiateIn
            Dim hr = New HtmlGenericControl("hr")
            container.Controls.Add(hr)
            Dim tabla As New HtmlTable()
            tabla.Width = "100%"
            Dim fila As New HtmlTableRow()
            Dim celda As New HtmlTableCell()
            celda.Style.Add(HtmlTextWriterStyle.FontWeight, "bold")
            Dim lc As New Literal()
            AddHandler lc.DataBinding, AddressOf Solicitud_DataBinding
            celda.Controls.Add(lc)
            celda.Attributes.Add("class", "ListItemLink")
            AddHandler celda.DataBinding, AddressOf IrADetalleSolicitud_DataBinding
            fila.Cells.Add(celda)
            tabla.Rows.Add(fila)
            'Peticionario
            fila = New HtmlTableRow()
            celda = New HtmlTableCell()
            lc = New Literal()
            AddHandler lc.DataBinding, AddressOf EtiquetaPeticionario_DataBinding
            celda.Controls.Add(lc)
            Dim fsnlinkinfo As New FSNWebControls.FSNLinkInfo
            fsnlinkinfo.PanelInfo = "FSNPanelDatosPersona"
            fsnlinkinfo.CssClass = "Rotulo"
            AddHandler fsnlinkinfo.DataBinding, AddressOf Peticionario_DataBinding
            celda.Controls.Add(fsnlinkinfo)
            fila.Cells.Add(celda)
            tabla.Rows.Add(fila)
            'Fecha Alta
            fila = New HtmlTableRow()
            celda = New HtmlTableCell()
            lc = New Literal()
            AddHandler lc.DataBinding, AddressOf FechaAlta_DataBinding
            celda.Controls.Add(lc)
            fila.Cells.Add(celda)
            tabla.Rows.Add(fila)
            'Etapa actual 
            fila = New HtmlTableRow()
            celda = New HtmlTableCell()
            lc = New Literal()
            AddHandler lc.DataBinding, AddressOf Etapa_DataBinding
            celda.Controls.Add(lc)
            fila.Cells.Add(celda)
            tabla.Rows.Add(fila)
            container.Controls.Add(tabla)
        End Sub
        ''' <summary>
        ''' Se produce al enlazar la etiqueta de la plantilla en que se muestra la instancia y el titulo con el origen de datos.
        ''' </summary>
        Private Sub Solicitud_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim lc As Literal = CType(sender, Literal)
            Dim container As DataListItem = CType(lc.NamingContainer, DataListItem)
            Dim iTipo As Short = DataBinder.Eval(container.DataItem, "Tipo")

            If iTipo = FSNLibrary.TiposDeDatos.TipoDeSolicitud.Contrato Then
                lc.Text = DataBinder.Eval(container.DataItem, "CodigoContrato") & " " & DataBinder.Eval(container.DataItem, "Titulo")
            Else
                lc.Text = DataBinder.Eval(container.DataItem, "Instancia") & " " & DataBinder.Eval(container.DataItem, "Titulo")
            End If
        End Sub
        ''' <summary>
        ''' Se produce al enlazar la tabla de la plantilla que muestra los datos con el origen de datos.
        ''' </summary>
        Private Sub IrADetalleSolicitud_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim tab As HtmlTableCell = CType(sender, HtmlTableCell)
            Dim container As DataListItem = CType(tab.NamingContainer, DataListItem)
            'si se trata de un aprobador normal (NWgestioninstancia.aspx o GestionContrato.aspx), 
            'una persona a la que se le ha trasladado la solicitud (gestiontrasladada.aspx o GestionContratoTrasladada.aspx), 
            'un aprobador autoasignable (aprobacion.aspx), etc
            'un observador o solicitud guardada y es el peticionario (NWDetalleSolicitud.aspx o DetalleContrato.aspx)
            Dim sPagina As String
            Dim iInstancia As Long
            Dim URL As String
            Dim iTipo As Short
            Dim sCodigo As String

            iTipo = DataBinder.Eval(container.DataItem, "Tipo")
            iInstancia = DataBinder.Eval(container.DataItem, "Instancia")
            sCodigo = DataBinder.Eval(container.DataItem, "CodigoContrato")

            If iTipo = FSNLibrary.TiposDeDatos.TipoDeSolicitud.Contrato Then
                If DataBinder.Eval(container.DataItem, "Estado") = FSNLibrary.TipoEstadoSolic.Guardada Then
                    sPagina = HttpContext.Current.Server.UrlEncode("contratos/DetalleContrato.aspx?desde=Inicio&Instancia=" & iInstancia)
                ElseIf DataBinder.Eval(container.DataItem, "Observador") Then
                    sPagina = HttpContext.Current.Server.UrlEncode("contratos/DetalleContrato.aspx?desde=Inicio&Observador=1&Instancia=" & iInstancia)
                Else
                    sPagina = HttpContext.Current.Server.UrlEncode("contratos/comprobaraprobContratos.aspx?desde=Inicio&Instancia=" & iInstancia & "&Codigo=" & sCodigo)
                End If
            Else
                If DataBinder.Eval(container.DataItem, "Estado") = FSNLibrary.TipoEstadoSolic.Guardada Then
                    sPagina = HttpContext.Current.Server.UrlEncode("seguimiento/NWDetalleSolicitud.aspx?Instancia=" & iInstancia & "&volver=" & HttpContext.Current.Request("url"))
                ElseIf DataBinder.Eval(container.DataItem, "Observador") Then
                    sPagina = HttpContext.Current.Server.UrlEncode("seguimiento/NWDetalleSolicitud.aspx?Instancia=" & iInstancia & "&ComboParticipante=3&volver=" & HttpContext.Current.Request("url"))
                Else
                    sPagina = HttpContext.Current.Server.UrlEncode("workflow/comprobaraprob.aspx?Instancia=" & iInstancia & "&volver=" & HttpContext.Current.Request("url"))
                End If
            End If

            URL = ConfigurationManager.AppSettings("rutaPM") & "frames.aspx?pagina=" & sPagina
            tab.Attributes.Add("onclick", "return ComprobarInstanciaEnProcesoWebPart(" & iInstancia & ",'" & URL & "','" & ConfigurationManager.AppSettings("rutaFS") & "', " & iTipo & ");")
        End Sub
        ''' <summary>
        ''' Se produce al enlazar la etiqueta de la plantilla en que se muestra el peticionario con el origen de datos.
        ''' </summary>
        Private Sub EtiquetaPeticionario_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim lc As Literal = CType(sender, Literal)
            Dim container As DataListItem = CType(lc.NamingContainer, DataListItem)
            If DataBinder.Eval(container.DataItem, "VerDetallePersona") Then
                lc.Text = DataBinder.Eval(container.DataItem, "EtiquetaPeticionario")
            End If
        End Sub
        ''' <summary>
        ''' Se produce al enlazar la etiqueta de la plantilla en que se muestra el peticionario con el origen de datos.
        ''' </summary>
        Private Sub Peticionario_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim fsn As FSNWebControls.FSNLinkInfo = CType(sender, FSNWebControls.FSNLinkInfo)
            Dim container As DataListItem = CType(fsn.NamingContainer, DataListItem)
            If DataBinder.Eval(container.DataItem, "VerDetallePersona") Then
                fsn.Text = DataBinder.Eval(container.DataItem, "Peticionario")
                fsn.ContextKey = DataBinder.Eval(container.DataItem, "CodigoPeticionario")
            End If
        End Sub
        ''' <summary>
        ''' Se produce al enlazar la etiqueta de la plantilla en que se muestra la fecha de alta con el origen de datos.
        ''' </summary>
        Private Sub FechaAlta_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim lc As Literal = CType(sender, Literal)
            Dim container As DataListItem = CType(lc.NamingContainer, DataListItem)
            lc.Text = DataBinder.Eval(container.DataItem, "FechaAlta")
        End Sub
        Private Sub Etapa_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim lc As Literal = CType(sender, Literal)
            Dim container As DataListItem = CType(lc.NamingContainer, DataListItem)
            lc.Text = DataBinder.Eval(container.DataItem, "EtapaActual") & " "
        End Sub
    End Class
    <Serializable()>
    Public Class mOrden
        Private mlInstancia As Long
        Private msCodigoContrato As String
        Private msTitulo As String
        Private msCodigoPeticionario As String
        Private msEtiquetaPeticionario As String
        Private msPeticionario As String
        Private msFechaAlta As String
        Private mbObservador As Boolean
        Private mbVerDetallePersona As Boolean
        Private miTipo As Short
        Private miEstado As Short
        Private msEtapaActual As String
        Public Property Instancia() As Long
            Get
                Return mlInstancia
            End Get
            Set(ByVal value As Long)
                mlInstancia = value
            End Set
        End Property
        Public Property CodigoContrato() As String
            Get
                Return msCodigoContrato
            End Get
            Set(ByVal value As String)
                msCodigoContrato = value
            End Set
        End Property
        Public Property Titulo() As String
            Get
                Return msTitulo
            End Get
            Set(ByVal value As String)
                msTitulo = value
            End Set
        End Property
        Public Property CodigoPeticionario() As String
            Get
                Return msCodigoPeticionario
            End Get
            Set(ByVal value As String)
                msCodigoPeticionario = value
            End Set
        End Property
        Public Property EtiquetaPeticionario() As String
            Get
                Return msEtiquetaPeticionario
            End Get
            Set(ByVal value As String)
                msEtiquetaPeticionario = value
            End Set
        End Property
        Public Property Peticionario() As String
            Get
                Return msPeticionario
            End Get
            Set(ByVal value As String)
                msPeticionario = value
            End Set
        End Property
        Public Property FechaAlta() As String
            Get
                Return msFechaAlta
            End Get
            Set(ByVal value As String)
                msFechaAlta = value
            End Set
        End Property
        Public Property Observador() As Boolean
            Get
                Return mbObservador
            End Get
            Set(ByVal value As Boolean)
                mbObservador = value
            End Set
        End Property
        Public Property VerDetallePersona() As Boolean
            Get
                Return mbVerDetallePersona
            End Get
            Set(ByVal value As Boolean)
                mbVerDetallePersona = value
            End Set
        End Property
        Public Property Tipo() As Short
            Get
                Return miTipo
            End Get
            Set(ByVal value As Short)
                miTipo = value
            End Set
        End Property
        Public Property Estado() As Short
            Get
                Return miEstado
            End Get
            Set(ByVal value As Short)
                miEstado = value
            End Set
        End Property
        Public Property EtapaActual() As String
            Get
                Return msEtapaActual
            End Get
            Set(ByVal value As String)
                msEtapaActual = value
            End Set
        End Property
    End Class
    Private Class ControlEditor
        Inherits EditorPart
        Private _txtTitulo As TextBox
        Private _txtAncho As TextBox
        Private _reqAncho As RequiredFieldValidator
        Private _ranAncho As RangeValidator
        Private _wneNumSolicitudes As Infragistics.Web.UI.EditorControls.WebNumericEditor
        Private _reqNumSolicitudes As RequiredFieldValidator
        Private _chkTipoSolicitud As CheckBoxList
        Private _chkFiltrosDisponibles As CheckBoxList
        Private _wneImporteDesde As Infragistics.Web.UI.EditorControls.WebNumericEditor
        Private _wneImporteHasta As Infragistics.Web.UI.EditorControls.WebNumericEditor
        Private _wdpFechaDesde As Infragistics.Web.UI.EditorControls.WebDatePicker
        Private _wdpFechaHasta As Infragistics.Web.UI.EditorControls.WebDatePicker
        Private _wmonthCal As Infragistics.Web.UI.EditorControls.WebMonthCalendar
        Private _chkEstados As CheckBoxList
        Private _reqEstados As RequiredFieldValidatorForCheckBoxLists
        Private _reqFiltroSolicitud As RequiredFieldValidatorForCheckBoxLists
        Private _chkFiltroSolicitud As CheckBoxList
        ''' <summary>
        ''' Lo llama el marco de trabajo de las páginas ASP.NET para indicar a los controles de servidor que utilizan la implementación basada en la composición que creen los controles secundarios que contengan como forma de preparar la devolución o representación de los datos.
        ''' </summary>
        Protected Overrides Sub CreateChildControls()
            Dim part As FSPMWebPartBusquedaSolicitudes = CType(WebPartToEdit, FSPMWebPartBusquedaSolicitudes)
            Controls.Clear()
            _txtTitulo = New TextBox()
            _txtTitulo.Width = Unit.Pixel(250)
            _txtTitulo.Text = part.Title
            Controls.Add(_txtTitulo)
            _txtAncho = New TextBox()
            _txtAncho.MaxLength = 3
            _txtAncho.Width = Unit.Pixel(30)
            _txtAncho.Text = part.Width.Value.ToString()
            _txtAncho.ID = Me.ID & "_txtAncho"
            Controls.Add(_txtAncho)
            _reqAncho = New RequiredFieldValidator()
            _reqAncho.ID = _txtAncho.ID & "_Req"
            _reqAncho.ErrorMessage = "*"
            _reqAncho.ControlToValidate = _txtAncho.ID
            Controls.Add(_reqAncho)
            _ranAncho = New RangeValidator()
            _ranAncho.ID = _txtAncho.ID & "_Ran"
            _ranAncho.Type = ValidationDataType.Integer
            _ranAncho.MaximumValue = 500
            _ranAncho.MinimumValue = 200
            _ranAncho.ErrorMessage = "(200-500)"
            _ranAncho.ControlToValidate = _txtAncho.ID
            Controls.Add(_ranAncho)
            'Nº de solicitudes a mostrar
            _wneNumSolicitudes = New Infragistics.Web.UI.EditorControls.WebNumericEditor
            _wneNumSolicitudes.ID = Me.ID & "_wneNumSolicitudes"
            _wneNumSolicitudes.Width = Unit.Pixel(30)
            _wneNumSolicitudes.MaxLength = 2
            _wneNumSolicitudes.DataMode = Infragistics.Web.UI.EditorControls.NumericDataMode.Int
            Controls.Add(_wneNumSolicitudes)
            _reqNumSolicitudes = New RequiredFieldValidator()
            _reqNumSolicitudes.ID = _wneNumSolicitudes.ID & "_Req"
            _reqNumSolicitudes.ErrorMessage = "*"
            _reqNumSolicitudes.ControlToValidate = _wneNumSolicitudes.ID
            Controls.Add(_reqNumSolicitudes)
            'Tipos de solicitud
            Dim pag As FSNServer.IFSNPage = CType(Me.Page, FSNServer.IFSNPage)
            Dim FSWSServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")

            _chkTipoSolicitud = New CheckBoxList
            _chkTipoSolicitud.Items.Clear()
            Try
                Dim cSolicitudes As FSNServer.Solicitudes
                cSolicitudes = FSWSServer.Get_Object(GetType(FSNServer.Solicitudes))
                cSolicitudes.LoadTiposSolicitudesRolPeticionario(pag.Usuario.CodPersona, pag.Idioma,
                                                                 iTipoSolicitud:=If(TipoSolicitud = TipoDeSolicitud.Otros, TipoDeSolicitud.Otros, TipoSolicitud))

                _chkTipoSolicitud.DataTextField = "DEN"
                _chkTipoSolicitud.DataValueField = "ID"
                _chkTipoSolicitud.DataSource = cSolicitudes.Data
                _chkTipoSolicitud.DataBind()
            Catch ex As Exception

            End Try
            _chkTipoSolicitud.ID = Me.ID & "_chkTipoSolicitud"
            _chkTipoSolicitud.RepeatColumns = 2
            _chkTipoSolicitud.Width = Unit.Percentage(100)
            Controls.Add(_chkTipoSolicitud) 'Hay que añadir el checkboxlist aunque no tenga elementos
            'Importe desde
            _wneImporteDesde = New Infragistics.Web.UI.EditorControls.WebNumericEditor
            _wneImporteDesde.Width = Unit.Pixel(100)
            '_wneImporteDesde.numberFormat = pag.Usuario.NumberFormat
            _wneImporteDesde.DataMode = Infragistics.Web.UI.EditorControls.NumericDataMode.Double
            Controls.Add(_wneImporteDesde)
            'Importe hasta
            _wneImporteHasta = New Infragistics.Web.UI.EditorControls.WebNumericEditor
            _wneImporteHasta.Width = Unit.Pixel(100)
            '_wneImporteHasta.NumberFormat = pag.Usuario.NumberFormat
            _wneImporteHasta.DataMode = Infragistics.Web.UI.EditorControls.NumericDataMode.Double
            Controls.Add(_wneImporteHasta)
            _wmonthCal = New Infragistics.Web.UI.EditorControls.WebMonthCalendar
            _wmonthCal.Enabled = True
            _wmonthCal.Style.Add("display", "none")
            Controls.Add(_wmonthCal)
            'Fecha desde
            _wdpFechaDesde = New Infragistics.Web.UI.EditorControls.WebDatePicker
            _wdpFechaDesde.DropDownCalendarID = _wmonthCal.ClientID
            _wdpFechaDesde.Width = Unit.Pixel(100)
            _wdpFechaDesde.NullText = ""
            _wdpFechaDesde.Value = part.dFechaDesde
            _wdpFechaDesde.SkinID = "Calendario"
            Controls.Add(_wdpFechaDesde)
            'Fecha hasta
            _wdpFechaHasta = New Infragistics.Web.UI.EditorControls.WebDatePicker
            _wdpFechaHasta.DropDownCalendarID = _wmonthCal.ClientID
            _wdpFechaHasta.Width = Unit.Pixel(100)
            _wdpFechaHasta.NullText = ""
            _wdpFechaHasta.Value = part.dFechaHasta
            _wdpFechaHasta.SkinID = "Calendario"
            Controls.Add(_wdpFechaHasta)
            'Filtrar por estados
            _chkEstados = New CheckBoxList
            _chkEstados.ID = Me.ID & "chkEstados"
            _chkEstados.Items.Clear()
            _chkEstados.RepeatColumns = 2
            _chkEstados.Width = Unit.Percentage(100)
            _chkEstados.Items.Add(New ListItem(part.Textos(77), "0"))
            _chkEstados.Items.Add(New ListItem(part.Textos(78), "8"))
            _chkEstados.Items.Add(New ListItem(part.Textos(79), "2"))
            _chkEstados.Items.Add(New ListItem(part.Textos(80), "100,101,102,103"))
            _chkEstados.Items.Add(New ListItem(part.Textos(81), "6"))
            Controls.Add(_chkEstados)

            _reqEstados = New RequiredFieldValidatorForCheckBoxLists
            _reqEstados.ID = _chkEstados.ID & "_Req"
            _reqEstados.ErrorMessage = " * " & part.Textos(86)
            _reqEstados.ControlToValidate = Me.ID & "chkEstados"
            Controls.Add(_reqEstados)

            'Filtrar solicitudes
            _chkFiltroSolicitud = New CheckBoxList
            _chkFiltroSolicitud.ID = Me.ID & "chkFiltroSolicitud"
            _chkFiltroSolicitud.Items.Clear()
            _chkFiltroSolicitud.Items.Add(New ListItem(If(TipoSolicitud = TipoDeSolicitud.Otros, part.Textos(82), part.Textos(262)), 1))
            _chkFiltroSolicitud.Items.Add(New ListItem(If(TipoSolicitud = TipoDeSolicitud.Otros, part.Textos(83), part.Textos(263)), 2))
            _chkFiltroSolicitud.Items.Add(New ListItem(If(TipoSolicitud = TipoDeSolicitud.Otros, part.Textos(84), part.Textos(264)), 2))
            _chkFiltroSolicitud.Items.Add(New ListItem(If(TipoSolicitud = TipoDeSolicitud.Otros, part.Textos(85), part.Textos(265)), 2))
            Controls.Add(_chkFiltroSolicitud)

            _reqFiltroSolicitud = New RequiredFieldValidatorForCheckBoxLists
            _reqFiltroSolicitud.ID = _chkFiltroSolicitud.ID & "_Req"
            _reqFiltroSolicitud.ErrorMessage = " * " & part.Textos(86)
            _reqFiltroSolicitud.ControlToValidate = Me.ID & "chkFiltroSolicitud"
            Controls.Add(_reqFiltroSolicitud)
        End Sub
        ''' <summary>
        ''' Presenta el contenido del control en el sistema de escritura especificado. Este método lo usan principalmente los programadores de controles. 
        ''' </summary>
        ''' <param name="writer">HtmlTextWriter que representa la secuencia de salida para presentar contenido HTML en el cliente.</param>
        Protected Overrides Sub RenderContents(ByVal writer As System.Web.UI.HtmlTextWriter)
            Dim part As FSPMWebPartBusquedaSolicitudes = CType(WebPartToEdit, FSPMWebPartBusquedaSolicitudes)
            writer.Write("<b>" & part.Textos(19) & ":</b> ")
            _txtTitulo.RenderControl(writer)
            writer.WriteBreak()
            writer.Write("<b>" & part.Textos(21) & ":</b> ")
            _txtAncho.RenderControl(writer)
            _reqAncho.RenderControl(writer)
            _ranAncho.RenderControl(writer)
            writer.WriteBreak()
            writer.Write("<b>" & If(TipoSolicitud = TipoDeSolicitud.Otros, part.Textos(58), part.Textos(260)) & ":</b> ")
            _wneNumSolicitudes.RenderControl(writer)
            _reqNumSolicitudes.RenderControl(writer)
            writer.WriteBreak()
            writer.WriteBreak()
            writer.Write("<div style='border: 1px solid #CCCCCC;'><b>&nbsp;&nbsp;&nbsp;" & If(TipoSolicitud = TipoDeSolicitud.Otros, part.Textos(62), part.Textos(261)) & "</b>")
            writer.WriteBreak()
            _chkTipoSolicitud.RenderControl(writer)
            writer.WriteBreak()
            writer.Write("</div>")
            writer.WriteBreak()
            writer.Write("<div style='border: 1px solid #CCCCCC;'>")
            writer.Write("<table cellpadding=2 border=0 width=95%>")
            writer.Write("<tr><td colspan=2><b>" & part.Textos(64) & "</b></td></tr>")
            writer.Write("<tr><td>" & part.Textos(65) & ":</td><td>")
            _wneImporteDesde.RenderControl(writer)
            writer.Write("</td>")
            writer.Write("<td>" & part.Textos(66) & ":</td><td>")
            _wneImporteHasta.RenderControl(writer)
            writer.Write("</td></tr></table>")
            writer.Write("</div>")
            writer.WriteBreak()
            writer.Write("<div style='border: 1px solid #CCCCCC;'>")
            writer.Write("<table cellpadding=2 border=0 width=95%>")
            writer.Write("<tr><td colspan=2><b>" & part.Textos(67) & "</b></td></tr>")
            writer.Write("<tr><td>" & part.Textos(68) & ":</td><td>")
            _wdpFechaDesde.RenderControl(writer)
            _wmonthCal.RenderControl(writer)
            writer.Write("</td>")
            writer.Write("<td>" & part.Textos(69) & ":</td><td>")
            _wdpFechaHasta.RenderControl(writer)
            writer.Write("</td></tr></table>")
            writer.Write("</div>")
            writer.WriteBreak()
            writer.Write("<div style='border: 1px solid #CCCCCC;'>")
            writer.Write("<table cellpadding=2 border=0 width=95%>")
            writer.Write("<tr><td colspan=2><b>" & part.Textos(89) & "</b>&nbsp;&nbsp;&nbsp;")
            _reqEstados.RenderControl(writer)
            writer.Write("</td></tr>")
            writer.Write("<tr><td>")
            _chkEstados.RenderControl(writer)
            writer.Write("</td></tr></table></div>")
            writer.WriteBreak()
            writer.Write("<div style='border: 1px solid #CCCCCC;'>")
            _reqFiltroSolicitud.RenderControl(writer)
            _chkFiltroSolicitud.RenderControl(writer)
            writer.WriteBreak()
        End Sub
        ''' <summary>
        ''' Guarda los valores de un control EditorPart en las propiedades correspondientes del control WebPart asociado.
        ''' </summary>
        ''' <returns>Es true si la acción de guardar los valores del control EditorPart en el control WebPart se realiza correctamente; en caso contrario (si se produce un error), es false.</returns>
        Public Overrides Function ApplyChanges() As Boolean
            _reqAncho.Validate()
            _ranAncho.Validate()
            _reqEstados.Validate()
            _reqFiltroSolicitud.Validate()
            _reqNumSolicitudes.Validate()
            If _reqAncho.IsValid() And _ranAncho.IsValid() And _reqFiltroSolicitud.IsValid And _reqEstados.IsValid And _reqNumSolicitudes.IsValid Then
                Dim part As FSPMWebPartBusquedaSolicitudes = CType(WebPartToEdit, FSPMWebPartBusquedaSolicitudes)
                part.Title = _txtTitulo.Text
                part.Width = Unit.Pixel(CType(_txtAncho.Text, Integer))
                'Nº de solicitudes a mostrar
                If wneNumSolicitudes.Text <> "" Then
                    part.iNumSolicitudesAMostrar = CType(wneNumSolicitudes.Value, Integer)
                Else
                    part.iNumSolicitudesAMostrar = 0
                End If
                'Tipos de solicitud
                Dim tipoSolicitud As String = ""
                For Each x As ListItem In _chkTipoSolicitud.Items
                    If x.Selected Then
                        tipoSolicitud = tipoSolicitud & CType(x.Value, Integer) & ","
                    End If
                Next
                If Len(tipoSolicitud) > 0 Then
                    tipoSolicitud = Left(tipoSolicitud, Len(tipoSolicitud) - 1)
                End If
                part.mSolicitudes = tipoSolicitud

                'Importes y fechas
                part.dImporteDesde = IIf(_wneImporteDesde.Text <> Nothing, CType(_wneImporteDesde.Value, Double), Nothing)
                part.dImporteHasta = IIf(_wneImporteHasta.Text <> Nothing, CType(_wneImporteHasta.Value, Double), Nothing)
                part.dFechaDesde = CType(_wdpFechaDesde.Value, Date)
                part.dFechaHasta = CType(_wdpFechaHasta.Value, Date)
                'Estados
                Dim estados As String = ""
                For Each x As ListItem In _chkEstados.Items
                    If x.Selected Then
                        estados = estados & CType(x.Value, String) & ","
                    End If
                Next
                If Len(estados) > 0 Then
                    estados = Left(estados, Len(estados) - 1)
                End If
                part.Estados = estados
                'Filtrar solicitudes
                part.bAbiertasPorUsted = _chkFiltroSolicitud.Items(0).Selected
                part.bParticipo = _chkFiltroSolicitud.Items(1).Selected
                part.bTrasladadas = _chkFiltroSolicitud.Items(2).Selected
                part.bOtras = _chkFiltroSolicitud.Items(3).Selected

                part.CargarSolicitudes()
                Return True
            Else
                Return False
            End If
        End Function
        ''' <summary>
        ''' Obtiene los valores de propiedad de un control WebPart correspondientes a su control EditorPart asociado.
        ''' </summary>
        Public Overrides Sub SyncChanges()
            Dim part As FSPMWebPartBusquedaSolicitudes = CType(WebPartToEdit, FSPMWebPartBusquedaSolicitudes)
            txtTitulo.Text = part.Title
            txtAncho.Text = part.Width.Value.ToString()
            wneNumSolicitudes.Value = IIf(part.iNumSolicitudesAMostrar = 0, "", part.iNumSolicitudesAMostrar)
            wneImporteDesde.Value = IIf(part.dImporteDesde = 0, "", part.dImporteDesde)
            wneImporteHasta.Value = IIf(part.dImporteHasta = 0, "", part.dImporteHasta)
            wdpFechaDesde.Value = part.dFechaDesde
            wdpFechaHasta.Value = part.dFechaHasta
            If part.mSolicitudes = Nothing Then
                For Each x As ListItem In chkTipoSolicitud.Items
                    x.Selected = True
                Next
            Else
                If part.mSolicitudes <> Nothing Then
                    For Each x As ListItem In chkTipoSolicitud.Items
                        x.Selected = IIf(InStr(part.mSolicitudes, CType(x.Value, String), ) > 0, True, False)
                    Next
                End If
            End If

            If part.mEstados <> Nothing Then
                For Each x As ListItem In chkEstados.Items
                    x.Selected = IIf(InStr("," & part.mEstados & ",", "," & CType(x.Value, String) & ",", ) > 0, True, False)
                Next
            End If

            chkFiltroSolicitud.Items(0).Selected = part.bAbiertasPorUsted
            chkFiltroSolicitud.Items(1).Selected = part.bParticipo
            chkFiltroSolicitud.Items(2).Selected = part.bTrasladadas
            chkFiltroSolicitud.Items(3).Selected = part.bOtras
        End Sub
        Private ReadOnly Property txtTitulo() As TextBox
            Get
                EnsureChildControls()
                Return _txtTitulo
            End Get
        End Property
        Private ReadOnly Property txtAncho() As TextBox
            Get
                EnsureChildControls()
                Return _txtAncho
            End Get
        End Property
        Private ReadOnly Property wneNumSolicitudes() As Infragistics.Web.UI.EditorControls.WebNumericEditor
            Get
                EnsureChildControls()
                Return _wneNumSolicitudes
            End Get
        End Property
        Private ReadOnly Property chkTipoSolicitud() As CheckBoxList
            Get
                EnsureChildControls()
                Return _chkTipoSolicitud
            End Get
        End Property
        Private ReadOnly Property chkFiltrosDisponibles() As CheckBoxList
            Get
                EnsureChildControls()
                Return _chkFiltrosDisponibles
            End Get
        End Property
        Private ReadOnly Property wneImporteDesde() As Infragistics.Web.UI.EditorControls.WebNumericEditor
            Get
                EnsureChildControls()
                Return _wneImporteDesde
            End Get
        End Property
        Private ReadOnly Property wneImporteHasta() As Infragistics.Web.UI.EditorControls.WebNumericEditor
            Get
                EnsureChildControls()
                Return _wneImporteHasta
            End Get
        End Property
        Private ReadOnly Property wdpFechaDesde() As Infragistics.Web.UI.EditorControls.WebDatePicker
            Get
                EnsureChildControls()
                Return _wdpFechaDesde
            End Get
        End Property
        Private ReadOnly Property wdpFechaHasta() As Infragistics.Web.UI.EditorControls.WebDatePicker
            Get
                EnsureChildControls()
                Return _wdpFechaHasta
            End Get
        End Property
        Private ReadOnly Property chkEstados() As CheckBoxList
            Get
                EnsureChildControls()
                Return _chkEstados
            End Get
        End Property
        Private ReadOnly Property chkFiltroSolicitud() As CheckBoxList
            Get
                EnsureChildControls()
                Return _chkFiltroSolicitud
            End Get
        End Property
        Private _tipoSolicitud As TiposDeDatos.TipoDeSolicitud
        Public Property TipoSolicitud() As TiposDeDatos.TipoDeSolicitud
            Get
                Return _tipoSolicitud
            End Get
            Set(ByVal value As TiposDeDatos.TipoDeSolicitud)
                _tipoSolicitud = value
            End Set
        End Property
    End Class
    <Personalizable(), WebBrowsable(), DefaultValue(5)>
    Public Property NumSolicitudesAMostrar() As Integer
        Get
            Return iNumSolicitudesAMostrar
        End Get
        Set(ByVal value As Integer)
            iNumSolicitudesAMostrar = value
        End Set
    End Property
    Private mTipoSolicitud As TipoDeSolicitud
    <Personalizable(), WebBrowsable(), DefaultValue(TipoDeSolicitud.Otros)>
    Public Property TipoSolicitud() As TipoDeSolicitud
        Get
            Return mTipoSolicitud
        End Get
        Set(ByVal value As TipoDeSolicitud)
            mTipoSolicitud = value
        End Set
    End Property
    Private mSolicitudes As String
    <Personalizable(), WebBrowsable(), DefaultValue("")>
    Public Property Solicitudes() As String
        Get
            Return mSolicitudes
        End Get
        Set(ByVal value As String)
            mSolicitudes = value
        End Set
    End Property
    <Personalizable(), WebBrowsable()>
    Public Property ImporteDesde() As Double
        Get
            Return dImporteDesde
        End Get
        Set(ByVal value As Double)
            dImporteDesde = value
        End Set
    End Property
    <Personalizable(), WebBrowsable()>
    Public Property ImporteHasta() As Double
        Get
            Return dImporteHasta
        End Get
        Set(ByVal value As Double)
            dImporteHasta = value
        End Set
    End Property
    <Personalizable(), WebBrowsable()>
    Public Property FechaDesde() As Date
        Get
            Return dFechaDesde
        End Get
        Set(ByVal value As Date)
            dFechaDesde = value
        End Set
    End Property
    <Personalizable(), WebBrowsable()>
    Public Property FechaHasta() As Date
        Get
            Return dFechaHasta
        End Get
        Set(ByVal value As Date)
            dFechaHasta = value
        End Set
    End Property
    <Personalizable(), WebBrowsable(), DefaultValue("")>
    Public Property Estados() As String
        Get
            Return mEstados
        End Get
        Set(ByVal value As String)
            mEstados = value
        End Set
    End Property
    <Personalizable(), WebBrowsable(), DefaultValue(True)>
    Public Property AbiertasPorUsted() As Boolean
        Get
            Return bAbiertasPorUsted
        End Get
        Set(ByVal value As Boolean)
            bAbiertasPorUsted = value
        End Set
    End Property
    <Personalizable(), WebBrowsable(), DefaultValue(True)>
    Public Property Participo() As Boolean
        Get
            Return bParticipo
        End Get
        Set(ByVal value As Boolean)
            bParticipo = value
        End Set
    End Property
    <Personalizable(), WebBrowsable(), DefaultValue(True)>
    Public Property Trasladadas() As Boolean
        Get
            Return bTrasladadas
        End Get
        Set(ByVal value As Boolean)
            bTrasladadas = value
        End Set
    End Property
    <Personalizable(), WebBrowsable(), DefaultValue(True)>
    Public Property Otras() As Boolean
        Get
            Return bOtras
        End Get
        Set(ByVal value As Boolean)
            bOtras = value
        End Set
    End Property
    ''' <summary>
    ''' Devuelve el idioma de la página que alberga el control.
    ''' </summary>
    ''' <returns>El código de idioma.</returns>
    Private Function Idioma() As String
        If Not DesignMode Then
            If Page Is Nothing AndAlso TypeOf Page Is FSNServer.IFSNPage Then
                Return CType(Page, FSNServer.IFSNPage).Idioma
            Else
                For Each x As String In HttpContext.Current.Session.Keys
                    If TypeOf HttpContext.Current.Session(x) Is FSNServer.User Then
                        Return CType(HttpContext.Current.Session(x), FSNServer.User).Idioma
                    End If
                Next
                Return String.Empty
            End If
        Else
            Return String.Empty
        End If
    End Function
    Public ReadOnly Property Textos(ByVal Index As Short) As String
        Get
            If _Textos Is Nothing Or Idioma() <> _sIdioma Then
                If Not DesignMode Then
                    Dim FSPMDict As New FSNServer.Dictionary
                    _sIdioma = Idioma()
                    If HttpContext.Current.Cache("Textos_" & _sIdioma & "_" & FSNLibrary.TiposDeDatos.ModulosIdiomas.Webparts) Is Nothing Then
                        FSPMDict.LoadData(FSNLibrary.TiposDeDatos.ModulosIdiomas.Webparts, _sIdioma)
                        HttpContext.Current.Cache.Insert("Textos_" & _sIdioma & "_" & FSNLibrary.TiposDeDatos.ModulosIdiomas.Webparts, FSPMDict.Data, Nothing, Caching.Cache.NoAbsoluteExpiration,
                                                         New TimeSpan(0, ConfigurationManager.AppSettings("TiempoExpiracionCacheMin"), 0))
                    End If
                    _Textos = HttpContext.Current.Cache("Textos_" & _sIdioma & "_" & FSNLibrary.TiposDeDatos.ModulosIdiomas.Webparts)
                    Return _Textos.Tables(0).Rows(Index).Item(1)
                Else
                    Return String.Empty
                End If
            Else
                Return _Textos.Tables(0).Rows(Index).Item(1)
            End If
        End Get
    End Property
    Public Overrides Property Title() As String
        Get
            If String.IsNullOrEmpty(MyBase.Title) Then
                If String.IsNullOrEmpty(sTitulo) Then
                    If Page Is Nothing And String.IsNullOrEmpty(Textos(88)) Then
                        MyBase.Title = If(TipoSolicitud = TipoDeSolicitud.Otros, Textos(88), Textos(259))
                        sTitulo = MyBase.Title
                    Else
                        sTitulo = If(TipoSolicitud = TipoDeSolicitud.Otros, Textos(88), Textos(259))
                    End If
                End If
                Return sTitulo
            Else
                sTitulo = MyBase.Title
                Return MyBase.Title
            End If
        End Get
        Set(ByVal value As String)
            MyBase.Title = value
        End Set
    End Property
    ''' <summary>
    ''' Carga los datos por defecto para el control
    ''' </summary>
    Public Sub New()
        Width = Unit.Pixel(300)
        iModo = 0
        iNumSolicitudesAMostrar = 5
        bAbiertasPorUsted = True
        mEstados = "0,2,6,8,100,101,102,103"
    End Sub
    ''' <summary>
    ''' Lo llama el marco de trabajo de las páginas ASP.NET para indicar a los controles de servidor que utilizan la implementación basada en la composición que creen los controles secundarios que contengan como forma de preparar la devolución o representación de los datos.
    ''' </summary>
    Protected Overrides Sub CreateChildControls()
        Controls.Clear()
        Dim img As HtmlImage = New HtmlImage()
        img.Src = Page.ClientScript.GetWebResourceUrl(GetType(FSPMWebPartBusquedaSolicitudes), "Fullstep.PMWebControls.trans.gif")
        img.Width = 300
        img.Height = 1
        Controls.Add(img)
        Dim tabla As HtmlTable = New HtmlTable()
        tabla.Width = "100%"
        tabla.Height = "55px"
        Dim fila As HtmlTableRow = New HtmlTableRow()
        fila.Height = "55px"
        Dim celda As HtmlTableCell = New HtmlTableCell()
        celda.Width = "90%"
        lblTitulo = New Label()
        lblTitulo.CssClass = "Rotulo"
        celda.Controls.Add(lblTitulo)
        celda.Controls.Add(New LiteralControl("<br/>"))
        lblMensaje = New Label
        lblMensaje.CssClass = "EtiquetaGrande"
        celda.Controls.Add(lblMensaje)
        celda.Controls.Add(New LiteralControl("<br/>"))
        fila.Cells.Add(celda)
        celdaImagen = New HtmlTableCell
        celdaImagen.Align = "right"
        celdaImagen.VAlign = "bottom"
        img.Width = 125
        celdaImagen.Controls.Add(img)
        celdaImagen.Style.Add("background-image", Page.ClientScript.GetWebResourceUrl(GetType(FSPMWebPartBusquedaSolicitudes), "Fullstep.PMWebControls.BusquedaSolicitudes.gif"))
        celdaImagen.Style.Add("background-position", "top left")
        celdaImagen.Style.Add("background-repeat", "no-repeat")
        lnkVerTodos = New FSNWebControls.FSNHyperlink()
        celdaImagen.Controls.Add(lnkVerTodos)
        fila.Cells.Add(celdaImagen)
        tabla.Rows.Add(fila)
        fila = New HtmlTableRow
        celda = New HtmlTableCell
        celda.ColSpan = 2
        lstSolicitudes = New DataList()
        lstSolicitudes.Width = Unit.Percentage(100)
        lstSolicitudes.ItemTemplate = New DataListTemplate()
        celda.Controls.Add(lstSolicitudes)
        fila.Cells.Add(celda)
        tabla.Rows.Add(fila)
        lblError = New Label
        lblError.CssClass = "Rotulo"
        lblError.Visible = False
        celda.Controls.Add(lblError)
        celda.Controls.Add(New LiteralControl("<br/>"))
        fila.Cells.Add(celda)
        tabla.Rows.Add(fila)
        Controls.Add(tabla)

        lblTitulo.Text = If(TipoSolicitud = TipoDeSolicitud.Otros, Textos(88), Textos(259))
        sTitulo = If(TipoSolicitud = TipoDeSolicitud.Otros, Textos(88), Textos(259))

        If Not Page.IsPostBack Then
            CargarSolicitudes()
        End If
    End Sub
    ''' <summary>
    ''' Refresca los datos del webpart
    ''' </summary>
    Public Sub Actualizar()
        CreateChildControls()
        CargarSolicitudes()
    End Sub
    ''' <summary>
    ''' Envía el contenido del control de servidor a un objeto HtmlTextWriter que se proporciona y almacena la información de seguimiento sobre el control si dicho seguimiento está habilitado.
    ''' </summary>
    ''' <param name="writer">Objeto HTmlTextWriter que recibe el contenido del control.</param>
    Public Overrides Sub RenderControl(ByVal writer As System.Web.UI.HtmlTextWriter)
        MyBase.RenderControl(writer)
    End Sub
    ''' <summary>
    ''' Carga los pedidos en el DataList del control.
    ''' </summary>
    Private Sub CargarSolicitudes()
        If Not Page Is Nothing AndAlso TypeOf Page Is FSNServer.IFSNPage Then
            Dim lista As New List(Of mOrden)
            Dim iNumSolicitudesObtenidas As Integer
            Dim pag As FSNServer.IFSNPage = CType(Me.Page, FSNServer.IFSNPage)
            Dim FSWSServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim cInstancias As FSNServer.Instancias

            Try
                cInstancias = FSWSServer.Get_Object(GetType(FSNServer.Instancias))
                iNumSolicitudesObtenidas = cInstancias.DevolverNumInstanciasBusqueda(pag.Usuario.CodPersona, mSolicitudes, dImporteDesde, dImporteHasta,
                                                                                     dFechaDesde, dFechaHasta, pag.Usuario.DateFmt, sEstado:=mEstados,
                                                                                     bAbiertasPorUsted:=bAbiertasPorUsted, bParticipo:=bParticipo,
                                                                                     bTrasladadas:=bTrasladadas, bOtras:=bOtras, TipoSolicitud:=TipoSolicitud)
                lblMensaje.Text = iNumSolicitudesObtenidas & " " & Textos(75)
                If iNumSolicitudesObtenidas = 0 Then
                    celdaImagen.Visible = False 'no se muestra el icono de solicitudes y el enlace a ver todas
                Else
                    celdaImagen.Visible = True
                    If iNumSolicitudesObtenidas > iNumSolicitudesAMostrar Then
                        lnkVerTodos.Visible = True 'se muestra el enlace a ver todas.
                    Else
                        lnkVerTodos.Visible = False
                    End If

                    cInstancias = FSWSServer.Get_Object(GetType(FSNServer.Instancias))
                    cInstancias.LoadResultadoBusqueda(pag.Usuario.CodPersona, pag.Idioma, nTop:=iNumSolicitudesAMostrar, sTipos:=mSolicitudes,
                                                      dImporteDesde:=dImporteDesde, dImporteHasta:=dImporteHasta, dFechaDesde:=dFechaDesde, dFechaHasta:=dFechaHasta,
                                                      sFormatoFecha:=pag.Usuario.DateFmt, sEstado:=mEstados, bAbiertasPorUsted:=bAbiertasPorUsted, bParticipo:=bParticipo,
                                                      bTrasladadas:=bTrasladadas, bOtras:=bOtras, TipoSolicitud:=TipoSolicitud)
                    If cInstancias.Data.Tables.Count > 0 Then
                        For Each fila As DataRow In cInstancias.Data.Tables(0).Rows
                            Dim mo As New mOrden()
                            mo.Instancia = fila.Item("ID")
                            mo.CodigoContrato = IIf(IsDBNull(fila.Item("CODIGO")), "", fila.Item("CODIGO"))
                            mo.Titulo = fila.Item("DEN")
                            mo.EtiquetaPeticionario = Textos(55) & ": "
                            mo.CodigoPeticionario = fila.Item("COD_PET")
                            mo.Peticionario = fila.Item("PETICIONARIO")
                            mo.FechaAlta = Textos(56) & ": " & FSNLibrary.FormatDate(fila.Item("FECHA"), pag.Usuario.DateFormat)
                            mo.Observador = fila.Item("OBV")
                            mo.VerDetallePersona = IIf(IsDBNull(fila.Item("VER_DETALLE_PER")), 0, fila.Item("VER_DETALLE_PER"))
                            mo.Tipo = fila.Item("TIPO")
                            mo.Estado = fila.Item("ESTADO")
                            mo.EtapaActual = Textos(95) & ": " & IIf(fila.Item("ETAPA_ACTUAL") = "##", Textos(97), fila.Item("ETAPA_ACTUAL"))
                            lista.Add(mo)
                        Next
                    End If
                End If
                lstSolicitudes.DataSource = lista
                lstSolicitudes.DataBind()
            Catch ex As Exception
                If iNumSolicitudesObtenidas = 0 Then
                    celdaImagen.Visible = False
                End If
                lblError.Text = Textos(239)
                lblError.Visible = True
            End Try

            If Not DesignMode() Then
                lnkVerTodos.Text = IIf(iModo = 2, Textos(76), Textos(274))
                Dim TipoVisor As TipoVisor
                Select Case TipoSolicitud
                    Case TipoDeSolicitud.Factura
                        TipoVisor = TipoVisor.Facturas
                    Case Else
                        TipoVisor = TipoVisor.Solicitudes
                End Select
                lnkVerTodos.NavigateUrl = ConfigurationManager.AppSettings("rutaPM2008") & "Tareas/VisorSolicitudes.aspx" & If(TipoSolicitud = TipoDeSolicitud.Otros, "", "?TipoVisor=" & TipoVisor)
                lnkVerTodos.Attributes.Add("onclick", "TempCargando();")
            End If
        End If
    End Sub
    ''' <summary>
    ''' Devuelve una colección de controles EditorPart personalizados que pueden utilizarse para editar un control WebPart cuando se encuentra en modo de edición.
    ''' </summary>
    ''' <returns>Una colección EditorPartCollection que contiene controles EditorPart personalizados asociados a un control WebPart.</returns>
    Public Overrides Function CreateEditorParts() As System.Web.UI.WebControls.WebParts.EditorPartCollection
        Dim editorArray As New ArrayList()
        Dim edPart As New ControlEditor()
        edPart.ID = ID & "_editor1"
        edPart.TipoSolicitud = TipoSolicitud
        editorArray.Add(edPart)
        Dim editorParts As New EditorPartCollection(editorArray)
        Return editorParts
    End Function
    Public Overrides ReadOnly Property WebBrowsableObject() As Object
        Get
            Return Me
        End Get
    End Property
    ''' <summary>
    ''' Registra el control como un control cuyo estado se debe conservar.
    ''' </summary>
    ''' <param name="e">Objeto EventArgs que contiene los datos del evento.</param>
    Protected Overrides Sub OnInit(ByVal e As System.EventArgs)
        MyBase.OnInit(e)
        Page.RegisterRequiresControlState(Me)
    End Sub
    ''' <summary>
    ''' Guarda los cambios de estado del control.
    ''' </summary>
    ''' <returns>Devuelve el estado actual del control de servidor.</returns>
    Protected Overrides Function SaveControlState() As Object
        Dim obj As Object = MyBase.SaveControlState()
        If obj IsNot Nothing Then
            obj = New Pair(obj, _Textos)
        Else
            obj = _Textos
        End If
        obj = New Pair(obj, _sIdioma)
        Return obj
    End Function
    ''' <summary>
    ''' Restaura información de estado del control.
    ''' </summary>
    ''' <param name="state">Object que representa el estado del control que se va a restaurar.</param>
    Protected Overrides Sub LoadControlState(ByVal state As Object)
        If (state IsNot Nothing) Then
            Dim p As Pair = TryCast(state, Pair)
            _sIdioma = p.Second
            state = p.First
            p = TryCast(state, Pair)
            If p IsNot Nothing Then
                MyBase.LoadControlState(p.First)
                _Textos = CType(p.Second, DataSet)
            Else
                If (TypeOf (state) Is String()) Then
                    _Textos = CType(state, DataSet)
                Else
                    MyBase.LoadControlState(state)
                End If
            End If
        End If
    End Sub
    Private Sub FSPMWebPartBusquedaSolicitudes_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.DesignMode Then
            ScriptManager.RegisterClientScriptResource(Me.Page, GetType(QAWebPartClientScript), "Fullstep.PMWebControls.FSQAWebPartsQA.js")
        End If
    End Sub
End Class
