﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices
Imports System.Web.UI

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("PMWebControls")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyProduct("PMWebControls")> 
<Assembly: TagPrefix("PMWebControls", "fsn")> 

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("5d03f27a-4cfe-4946-ba45-3adc8ab2fbfb")> 

<Assembly: WebResource("Fullstep.PMWebControls.trans.gif", "image/gif")> 
<Assembly: WebResource("Fullstep.PMWebControls.SolicitudesPMSmall.jpg", "image/jpeg")> 
<Assembly: WebResource("Fullstep.PMWebControls.Img_Ico_Alerta.gif", "image/gif")> 
<Assembly: WebResource("Fullstep.PMWebControls.BusquedaSolicitudes.gif", "image/gif")> 
<Assembly: WebResource("Fullstep.PMWebControls.FiltrosDisponibles.gif", "image/gif")> 
<Assembly: WebResource("Fullstep.PMWebControls.MonitorizacionSolicitudes.gif", "image/gif")> 
<Assembly: WebResource("Fullstep.PMWebControls.NoConformidades.gif", "image/gif")> 
<Assembly: WebResource("Fullstep.PMWebControls.Certificados.gif", "image/gif")> 
<Assembly: WebResource("Fullstep.PMWebControls.Mas.gif", "image/gif")> 
<Assembly: WebResource("Fullstep.PMWebControls.Menos.gif", "image/gif")> 
<Assembly: WebResource("Fullstep.PMWebControls.Img_Ico_Lupa.gif", "image/gif")> 
<Assembly: WebResource("Fullstep.PMWebControls.PanelCalidad.gif", "image/gif")> 
<Assembly: WebResource("Fullstep.PMWebControls.FSQAWebPartPanelCalidad.js", "application/x-javascript")> 
<Assembly: WebResource("Fullstep.PMWebControls.FSQAWebPartsQA.js", "application/x-javascript")> 
<Assembly: WebResource("Fullstep.PMWebControls.eliminar.gif", "image/gif")> 

