﻿Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.HtmlControls
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.ComponentModel
Imports System.Configuration
Imports Fullstep.FSNLibrary

Public Class FSPMWebPartMonitorizacionSolicitudes
    Inherits WebPart

    Private lblTitulo As Label
    Private lblError As Label
    Private Shared lblTotalSolicitudes As Label
    Private lblMensaje As Label
    Private lstSolicitudes As DataList
    Private celdaImagen As HtmlTableCell
    Private mTiposSolicitudes As String
    Private dImporteDesde As Double
    Private dImporteHasta As Double
    Private dFechaDesde As Date
    Private dFechaHasta As Date
    Private Shared sUrlEliminar As String
    
    Private sTitulo As String
    Private _Textos As DataSet
    Private _sIdioma As String

    Private Class DataListTemplate
        Implements ITemplate

        ''' <summary>
        ''' Define el objeto Control al que pertenecen los controles secundarios y las plantillas. Estos controles secundarios están a su vez definidos en una plantilla en línea.
        ''' </summary>
        ''' <param name="container">Objeto Control que contiene las instancias de los controles de la plantilla en línea.</param>
        Private Sub InstantiateIn(ByVal container As Control) _
           Implements ITemplate.InstantiateIn
            Dim hr = New HtmlGenericControl("hr")
            hr.Visible = False
            AddHandler hr.DataBinding, AddressOf MostrarHr_DataBinding
            container.Controls.Add(hr)
            Dim tabla As New HtmlTable()
            tabla.Width = "100%"
            tabla.Visible = False
            AddHandler tabla.DataBinding, AddressOf MostrarTabla_DataBinding
            Dim fila As New HtmlTableRow()
            Dim celda As New HtmlTableCell()
            celda.Style.Add(HtmlTextWriterStyle.FontWeight, "bold")
            Dim lc As New Literal()
            AddHandler lc.DataBinding, AddressOf Solicitud_DataBinding
            celda.Controls.Add(lc)
            celda.Attributes.Add("class", "ListItemLink")
            AddHandler celda.DataBinding, AddressOf IrADetalleSolicitud_DataBinding
            fila.Cells.Add(celda)
            celda = New HtmlTableCell()
            Dim img As New ImageButton
            img.ImageUrl = sUrlEliminar
            img.Visible = False
            AddHandler img.DataBinding, AddressOf EliminarMonitorizacion_DataBinding
            AddHandler img.Click, AddressOf Button_Click
            celda.Controls.Add(img)
            fila.Cells.Add(celda)
            tabla.Rows.Add(fila)
            'Estado y fecha
            fila = New HtmlTableRow()
            celda = New HtmlTableCell()
            lc = New Literal()
            AddHandler lc.DataBinding, AddressOf EstadoyFecha_DataBinding
            celda.Controls.Add(lc)
            fila.Cells.Add(celda)
            tabla.Rows.Add(fila)
            'Etapa actual y usuario
            fila = New HtmlTableRow()
            celda = New HtmlTableCell()
            lc = New Literal()
            AddHandler lc.DataBinding, AddressOf Etapa_DataBinding
            celda.Controls.Add(lc)
            Dim fsnlinkinfo As New FSNWebControls.FSNLinkInfo
            fsnlinkinfo.CssClass = "Rotulo"
            AddHandler fsnlinkinfo.DataBinding, AddressOf Usuario_DataBinding
            celda.Controls.Add(fsnlinkinfo)
            fila.Cells.Add(celda)
            tabla.Rows.Add(fila)
            container.Controls.Add(tabla)
        End Sub

        ''' <summary>
        ''' Procedimiento que se dispara cuando se pincha el botÃ³n de actualizar las monitorizaciones de las solicitudes
        ''' </summary>
        ''' <param name="sender">Objeto que lanza el evento</param>
        ''' <param name="e">Los argumentos del evento</param>
        ''' <remarks>
        ''' Llamada desde: AutomÃ¡tico, cuando se pincha el botÃ³n
        ''' Tiempo mÃ¡ximo: 0,1 seg</remarks>
        Protected Sub Button_Click(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim FSWSServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim cInstancia As FSNServer.Instancia

            Dim arg As Long
            arg = CType(sender, ImageButton).CommandArgument

            cInstancia = FSWSServer.Get_Object(GetType(FSNServer.Instancia))
            cInstancia.ID = arg
            cInstancia.ActualizarMonitorizacion(0, HttpContext.Current.Session("FSN_User").CodPersona)

            Dim img As ImageButton = CType(sender, ImageButton)
            Dim container As DataListItem = CType(img.NamingContainer, DataListItem)
            container.Parent.Controls(container.ItemIndex).Controls.Clear()
            lblTotalSolicitudes.Text = CStr(CLng(lblTotalSolicitudes.Text) - 1)
        End Sub

        ''' <summary>
        ''' Se produce al enlazar la etiqueta de la plantilla en que se muestra la instancia y el titulo con el origen de datos.
        ''' </summary>
        Private Sub MostrarHr_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim lc As HtmlGenericControl = CType(sender, HtmlGenericControl)
            lc.Visible = True
        End Sub

        ''' <summary>
        ''' Se produce al enlazar la etiqueta de la plantilla en que se muestra la instancia y el titulo con el origen de datos.
        ''' </summary>
        Private Sub MostrarTabla_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim lc As HtmlTable = CType(sender, HtmlTable)
            lc.Visible = True
        End Sub

        ''' <summary>
        ''' Se produce al enlazar la etiqueta de la plantilla en que se muestra la instancia y el titulo con el origen de datos.
        ''' </summary>
        Private Sub Solicitud_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim lc As Literal = CType(sender, Literal)
            Dim container As DataListItem = CType(lc.NamingContainer, DataListItem)
            Dim iTipo As Short = DataBinder.Eval(container.DataItem, "Tipo")

            If iTipo = FSNLibrary.TiposDeDatos.TipoDeSolicitud.Contrato Then
                lc.Text = DataBinder.Eval(container.DataItem, "CodigoContrato") & " " & DataBinder.Eval(container.DataItem, "Titulo")
            Else
                lc.Text = DataBinder.Eval(container.DataItem, "Instancia") & " " & DataBinder.Eval(container.DataItem, "Titulo")
            End If
        End Sub

        ''' <summary>
        ''' Se produce al enlazar la tabla de la plantilla que muestra los datos con el origen de datos.
        ''' </summary>
        Private Sub IrADetalleSolicitud_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim tab As HtmlTableCell = CType(sender, HtmlTableCell)
            Dim container As DataListItem = CType(tab.NamingContainer, DataListItem)
            Dim sPagina As String
            Dim URL As String

            If DataBinder.Eval(container.DataItem, "EstadoId") = TipoEstadoSolic.Guardada Then
                sPagina = HttpContext.Current.Server.UrlEncode("seguimiento/NWDetalleSolicitud.aspx?Instancia=" & DataBinder.Eval(container.DataItem, "Instancia") & "&volver=" & HttpContext.Current.Request("url"))
            Else
                If DataBinder.Eval(container.DataItem, "Tipo") = FSNLibrary.TiposDeDatos.TipoDeSolicitud.Contrato Then
                    sPagina = HttpContext.Current.Server.UrlEncode("contratos/comprobaraprobContratos.aspx?desde=Inicio&Instancia=" & DataBinder.Eval(container.DataItem, "Instancia") & "&Codigo=" & DataBinder.Eval(container.DataItem, "CodigoContrato"))
                Else
                    sPagina = HttpContext.Current.Server.UrlEncode("workflow/comprobaraprob.aspx?Instancia=" & DataBinder.Eval(container.DataItem, "Instancia") & "&volver=" & HttpContext.Current.Request("url"))
                End If
            End If

            URL = ConfigurationManager.AppSettings("rutaPM") & "frames.aspx?pagina=" & sPagina
            tab.Attributes.Add("onclick", "return ComprobarInstanciaEnProcesoWebPart(" & DataBinder.Eval(container.DataItem, "Instancia") & ",'" & URL & "','" & ConfigurationManager.AppSettings("rutaFS") & "', " & TiposDeDatos.TipoDeSolicitud.SolicitudDeCompras & ");")
        End Sub

        ''' <summary>
        ''' Se produce al enlazar la tabla de la plantilla que muestra los datos con el origen de datos.
        ''' </summary>
        Private Sub EliminarMonitorizacion_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim tab As ImageButton = CType(sender, ImageButton)
            Dim container As DataListItem = CType(tab.NamingContainer, DataListItem)
            tab.CommandArgument = DataBinder.Eval(container.DataItem, "Instancia")
            tab.Visible = True
        End Sub

        ''' <summary>
        ''' Se produce al enlazar la etiqueta de la plantilla en que se muestra el estado y la fecha con el origen de datos.
        ''' </summary>
        Private Sub EstadoyFecha_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim lc As Literal = CType(sender, Literal)
            Dim container As DataListItem = CType(lc.NamingContainer, DataListItem)
            lc.Text = DataBinder.Eval(container.DataItem, "Estado") & " " & DataBinder.Eval(container.DataItem, "FechaUltimaAccion")
        End Sub


        ''' <summary>
        ''' Se produce al enlazar la etiqueta de la plantilla en que se muestra la etapa con el origen de datos.
        ''' </summary>
        Private Sub Etapa_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim lc As Literal = CType(sender, Literal)
            Dim container As DataListItem = CType(lc.NamingContainer, DataListItem)
            lc.Text = DataBinder.Eval(container.DataItem, "EtapaActual") & " "
        End Sub

        ''' <summary>
        ''' Se produce al enlazar la etiqueta de la plantilla en que se muestra el usuario con el origen de datos.
        ''' </summary>
        Private Sub Usuario_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim fsn As FSNWebControls.FSNLinkInfo = CType(sender, FSNWebControls.FSNLinkInfo)
            Dim container As DataListItem = CType(fsn.NamingContainer, DataListItem)
            If DataBinder.Eval(container.DataItem, "VerDetallePersona") Then
                fsn.Text = DataBinder.Eval(container.DataItem, "Usuario")
                If DataBinder.Eval(container.DataItem, "CodigoProveedor") <> Nothing Then
                    fsn.PanelInfo = "FSNPanelDatosProveedor"
                    fsn.ContextKey = DataBinder.Eval(container.DataItem, "CodigoProveedor")
                Else
                    fsn.PanelInfo = "FSNPanelDatosUsuario"
                    fsn.ContextKey = DataBinder.Eval(container.DataItem, "CodigoUsuario")
                End If
            End If
        End Sub
    End Class

    <Serializable()> _
    Public Class mOrden
        Private mlInstancia As Long
        Private msCodigoContrato As String
        Private msTitulo As String
        Private miEstadoId As Short
        Private msEstado As String
        Private miTipo As Short
        Private msFechaUltimaAccion As String
        Private msEtapaActual As String
        Private msUsuario As String
        Private msCodigoUsuario As String
        Private msCodigoProveedor As String
        Private mbVerDetallePersona As Boolean

        Public Property Instancia() As Long
            Get
                Return mlInstancia
            End Get
            Set(ByVal value As Long)
                mlInstancia = value
            End Set
        End Property

        Public Property CodigoContrato() As String
            Get
                Return msCodigoContrato
            End Get
            Set(ByVal value As String)
                msCodigoContrato = value
            End Set
        End Property

        Public Property Titulo() As String
            Get
                Return msTitulo
            End Get
            Set(ByVal value As String)
                msTitulo = value
            End Set
        End Property

        Public Property EstadoId() As Short
            Get
                Return miEstadoId
            End Get
            Set(ByVal value As Short)
                miEstadoId = value
            End Set
        End Property

        Public Property Estado() As String
            Get
                Return msEstado
            End Get
            Set(ByVal value As String)
                msEstado = value
            End Set
        End Property

        Public Property Tipo() As Short
            Get
                Return miTipo
            End Get
            Set(ByVal value As Short)
                miTipo = value
            End Set
        End Property

        Public Property FechaUltimaAccion() As String
            Get
                Return msFechaUltimaAccion
            End Get
            Set(ByVal value As String)
                msFechaUltimaAccion = value
            End Set
        End Property

        Public Property EtapaActual() As String
            Get
                Return msEtapaActual
            End Get
            Set(ByVal value As String)
                msEtapaActual = value
            End Set
        End Property

        Public Property Usuario() As String
            Get
                Return msUsuario
            End Get
            Set(ByVal value As String)
                msUsuario = value
            End Set
        End Property

        Public Property CodigoUsuario() As String
            Get
                Return msCodigoUsuario
            End Get
            Set(ByVal value As String)
                msCodigoUsuario = value
            End Set
        End Property

        Public Property CodigoProveedor() As String
            Get
                Return msCodigoProveedor
            End Get
            Set(ByVal value As String)
                msCodigoProveedor = value
            End Set
        End Property

        Public Property VerDetallePersona() As Boolean
            Get
                Return mbVerDetallePersona
            End Get
            Set(ByVal value As Boolean)
                mbVerDetallePersona = value
            End Set
        End Property
    End Class

    Private Class ControlEditor
        Inherits EditorPart

        Private _txtTitulo As TextBox
        Private _txtAncho As TextBox
        Private _reqAncho As RequiredFieldValidator
        Private _ranAncho As RangeValidator
        Private _chkTipoSolicitud As CheckBoxList
        Private _wneImporteDesde As Infragistics.Web.UI.EditorControls.WebNumericEditor
        Private _wneImporteHasta As Infragistics.Web.UI.EditorControls.WebNumericEditor
        Private _wdcFechaDesde As Infragistics.Web.UI.EditorControls.WebDatePicker
        Private _wdcFechaHasta As Infragistics.Web.UI.EditorControls.WebDatePicker
        Private _wmonthCal As Infragistics.Web.UI.EditorControls.WebMonthCalendar

        ''' <summary>
        ''' Lo llama el marco de trabajo de las páginas ASP.NET para indicar a los controles de servidor que utilizan la implementación basada en la composición que creen los controles secundarios que contengan como forma de preparar la devolución o representación de los datos.
        ''' </summary>
        Protected Overrides Sub CreateChildControls()
            Dim part As FSPMWebPartMonitorizacionSolicitudes = CType(WebPartToEdit, FSPMWebPartMonitorizacionSolicitudes)
            Controls.Clear()
            _txtTitulo = New TextBox()
            _txtTitulo.Width = Unit.Pixel(250)
            _txtTitulo.Text = part.Title
            Controls.Add(_txtTitulo)
            _txtAncho = New TextBox()
            _txtAncho.MaxLength = 3
            _txtAncho.Width = Unit.Pixel(30)
            _txtAncho.Text = part.Width.Value.ToString()
            _txtAncho.ID = Me.ID & "_txtAncho"
            Controls.Add(_txtAncho)
            _reqAncho = New RequiredFieldValidator()
            _reqAncho.ID = _txtAncho.ID & "_Req"
            _reqAncho.ErrorMessage = "*"
            _reqAncho.ControlToValidate = _txtAncho.ID
            Controls.Add(_reqAncho)
            _ranAncho = New RangeValidator()
            _ranAncho.ID = _txtAncho.ID & "_Ran"
            _ranAncho.Type = ValidationDataType.Integer
            _ranAncho.MaximumValue = 500
            _ranAncho.MinimumValue = 200
            _ranAncho.ErrorMessage = "(200-500)"
            _ranAncho.ControlToValidate = _txtAncho.ID
            Controls.Add(_ranAncho)
            'Tipos de solicitud
            _chkTipoSolicitud = New CheckBoxList
            _chkTipoSolicitud.Items.Clear()
            Dim pag As FSNServer.IFSNPage = CType(Me.Page, FSNServer.IFSNPage)
            Dim FSWSServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Try
                Dim cSolicitudes As FSNServer.Solicitudes
                cSolicitudes = FSWSServer.Get_Object(GetType(FSNServer.Solicitudes))
                cSolicitudes.LoadTiposSolicitudesRolPeticionario(pag.Usuario.CodPersona, pag.Idioma)
                _chkTipoSolicitud.DataTextField = "DEN"
                _chkTipoSolicitud.DataValueField = "ID"
                _chkTipoSolicitud.DataSource = cSolicitudes.Data
                _chkTipoSolicitud.DataBind()
            Catch ex As Exception

            End Try
            _chkTipoSolicitud.ID = Me.ID & "_chkTipoSolicitud"
            _chkTipoSolicitud.RepeatColumns = 2
            _chkTipoSolicitud.Width = Unit.Percentage(100)
            Controls.Add(_chkTipoSolicitud)
            'Importe desde
            _wneImporteDesde = New Infragistics.Web.UI.EditorControls.WebNumericEditor
            _wneImporteDesde.Width = Unit.Pixel(100)
            _wneImporteDesde.DataMode = Infragistics.Web.UI.EditorControls.NumericDataMode.Double
            Controls.Add(_wneImporteDesde)
            'Importe hasta
            _wneImporteHasta = New Infragistics.Web.UI.EditorControls.WebNumericEditor
            _wneImporteHasta.Width = Unit.Pixel(100)
            _wneImporteHasta.DataMode = Infragistics.Web.UI.EditorControls.NumericDataMode.Double
            Controls.Add(_wneImporteHasta)

            _wmonthCal = New Infragistics.Web.UI.EditorControls.WebMonthCalendar
            _wmonthCal.Enabled = True
            _wmonthCal.Style.Add("display", "none")
            Controls.Add(_wmonthCal)

            'Fecha desde
            _wdcFechaDesde = New Infragistics.Web.UI.EditorControls.WebDatePicker
            _wdcFechaDesde.DropDownCalendarID = _wmonthCal.ID
            _wdcFechaDesde.Width = Unit.Pixel(100)
            _wdcFechaDesde.NullText = ""
            _wdcFechaDesde.Value = part.dFechaDesde
            _wdcFechaDesde.SkinID = "Calendario"
            Controls.Add(_wdcFechaDesde)

            'Fecha hasta
            _wdcFechaHasta = New Infragistics.Web.UI.EditorControls.WebDatePicker
            _wdcFechaHasta.DropDownCalendarID = _wmonthCal.ID
            _wdcFechaHasta.Width = Unit.Pixel(100)
            _wdcFechaHasta.NullText = ""
            _wdcFechaHasta.Value = part.dFechaHasta
            _wdcFechaHasta.SkinID = "Calendario"
            Controls.Add(_wdcFechaHasta)
        End Sub

        ''' <summary>
        ''' Presenta el contenido del control en el sistema de escritura especificado. Este método lo usan principalmente los programadores de controles. 
        ''' </summary>
        ''' <param name="writer">HtmlTextWriter que representa la secuencia de salida para presentar contenido HTML en el cliente.</param>
        Protected Overrides Sub RenderContents(ByVal writer As System.Web.UI.HtmlTextWriter)
            Dim part As FSPMWebPartMonitorizacionSolicitudes = CType(WebPartToEdit, FSPMWebPartMonitorizacionSolicitudes)
            writer.Write("<b>" & part.Textos(19) & ":</b> ")
            _txtTitulo.RenderControl(writer)
            writer.WriteBreak()
            writer.Write("<b>" & part.Textos(21) & ":</b> ")
            _txtAncho.RenderControl(writer)
            _reqAncho.RenderControl(writer)
            _ranAncho.RenderControl(writer)
            writer.WriteBreak()
            writer.WriteBreak()
            writer.Write("<div style='border: 1px solid #CCCCCC;'>")
            writer.Write("<table cellpadding=2 border=0 width=100%>")
            writer.Write("<tr><td colspan=2><b>" & part.Textos(64) & "</b></td></tr>")
            writer.Write("<tr><td style='width:10em;'>" & part.Textos(65) & ":</td><td style='width:10em;'>")
            _wneImporteDesde.RenderControl(writer)
            writer.Write("</td>")
            writer.Write("<td style='width:10em;'>" & part.Textos(66) & ":</td><td>")
            _wneImporteHasta.RenderControl(writer)
            writer.Write("</td></tr></table>")
            writer.Write("</div>")
            writer.WriteBreak()
            writer.Write("<div style='border: 1px solid #CCCCCC;'>")
            writer.Write("<table cellpadding=2 border=0 width=100%>")
            writer.Write("<tr><td colspan=2><b>" & part.Textos(67) & "</b></td></tr>")
            writer.Write("<tr><td style='width:10em;'>" & part.Textos(68) & ":</td><td style='width:10em;'>")
            _wdcFechaDesde.RenderControl(writer)
            writer.Write("</td>")
            writer.Write("<td style='width:10em;'>" & part.Textos(69) & ":</td><td>")
            _wdcFechaHasta.RenderControl(writer)
            writer.Write("</td></tr></table>")
            writer.Write("</div>")
            writer.WriteBreak()
            writer.Write("<div style='border: 1px solid #CCCCCC;'><b>&nbsp;&nbsp;&nbsp;" & part.Textos(62) & "</b>")
            writer.WriteBreak()
            _chkTipoSolicitud.RenderControl(writer)
            writer.WriteBreak()
            writer.Write("</div>")
            writer.WriteBreak()
            
            _wmonthCal.RenderControl(writer)
        End Sub

        ''' <summary>
        ''' Guarda los valores de un control EditorPart en las propiedades correspondientes del control WebPart asociado.
        ''' </summary>
        ''' <returns>Es true si la acción de guardar los valores del control EditorPart en el control WebPart se realiza correctamente; en caso contrario (si se produce un error), es false.</returns>
        Public Overrides Function ApplyChanges() As Boolean
            _reqAncho.Validate()
            _ranAncho.Validate()
            If _reqAncho.IsValid() And _ranAncho.IsValid() Then
                Dim part As FSPMWebPartMonitorizacionSolicitudes = CType(WebPartToEdit, FSPMWebPartMonitorizacionSolicitudes)
                part.Title = _txtTitulo.Text
                part.Width = Unit.Pixel(CType(_txtAncho.Text, Integer))
                'Tipos de solicitud
                Dim tipoSolicitud As String = ""
                For Each x As ListItem In _chkTipoSolicitud.Items
                    If x.Selected Then
                        tipoSolicitud = tipoSolicitud & CType(x.Value, Integer) & ","
                    End If
                Next
                If Len(tipoSolicitud) > 0 Then
                    tipoSolicitud = Left(tipoSolicitud, Len(tipoSolicitud) - 1)
                End If
                part.mTiposSolicitudes = tipoSolicitud
                'Importes y fechas
                part.dImporteDesde = IIf(_wneImporteDesde.Text <> Nothing, CType(_wneImporteDesde.Value, Double), Nothing)
                part.dImporteHasta = IIf(_wneImporteHasta.Text <> Nothing, CType(_wneImporteHasta.Value, Double), Nothing)
                part.dFechaDesde = CType(_wdcFechaDesde.Value, Date)
                part.dFechaHasta = CType(_wdcFechaHasta.Value, Date)

                part.CargarSolicitudes()
                Return True
            Else
                Return False
            End If
        End Function
        ''' <summary>
        ''' Obtiene los valores de propiedad de un control WebPart correspondientes a su control EditorPart asociado.
        ''' </summary>
        Public Overrides Sub SyncChanges()
            Dim part As FSPMWebPartMonitorizacionSolicitudes = CType(WebPartToEdit, FSPMWebPartMonitorizacionSolicitudes)
            txtTitulo.Text = part.Title
            txtAncho.Text = part.Width.Value.ToString()
            wneImporteDesde.Value = IIf(part.dImporteDesde = 0, "", part.dImporteDesde)
            wneImporteHasta.Value = IIf(part.dImporteHasta = 0, "", part.dImporteHasta)
            wdcFechaDesde.Value = part.dFechaDesde
            wdcFechaHasta.Value = part.dFechaHasta
            If part.mTiposSolicitudes = Nothing Then
                For Each x As ListItem In chkTipoSolicitud.Items
                    x.Selected = True
                Next
            Else
                If part.mTiposSolicitudes <> Nothing Then
                    For Each x As ListItem In chkTipoSolicitud.Items
                        x.Selected = IIf(InStr(part.mTiposSolicitudes, CType(x.Value, String), ) > 0, True, False)
                    Next
                End If
            End If
        End Sub

        Private ReadOnly Property txtTitulo() As TextBox
            Get
                EnsureChildControls()
                Return _txtTitulo
            End Get
        End Property

        Private ReadOnly Property txtAncho() As TextBox
            Get
                EnsureChildControls()
                Return _txtAncho
            End Get
        End Property

        Private ReadOnly Property chkTipoSolicitud() As CheckBoxList
            Get
                EnsureChildControls()
                Return _chkTipoSolicitud
            End Get
        End Property

        Private ReadOnly Property wneImporteDesde() As Infragistics.Web.UI.EditorControls.WebNumericEditor
            Get
                EnsureChildControls()
                Return _wneImporteDesde
            End Get
        End Property

        Private ReadOnly Property wneImporteHasta() As Infragistics.Web.UI.EditorControls.WebNumericEditor
            Get
                EnsureChildControls()
                Return _wneImporteHasta
            End Get
        End Property

        Private ReadOnly Property wdcFechaDesde() As Infragistics.Web.UI.EditorControls.WebDatePicker
            Get
                EnsureChildControls()
                Return _wdcFechaDesde
            End Get
        End Property

        Private ReadOnly Property wdcFechaHasta() As Infragistics.Web.UI.EditorControls.WebDatePicker
            Get
                EnsureChildControls()
                Return _wdcFechaHasta
            End Get
        End Property

    End Class

    <Personalizable(), WebBrowsable(), DefaultValue("")> _
    Public Property TiposSolicitudes() As String
        Get
            Return mTiposSolicitudes
        End Get
        Set(ByVal value As String)
            mTiposSolicitudes = value
        End Set
    End Property

    <Personalizable(), WebBrowsable()> _
    Public Property ImporteDesde() As Double
        Get
            Return dImporteDesde
        End Get
        Set(ByVal value As Double)
            dImporteDesde = value
        End Set
    End Property

    <Personalizable(), WebBrowsable()> _
    Public Property ImporteHasta() As Double
        Get
            Return dImporteHasta
        End Get
        Set(ByVal value As Double)
            dImporteHasta = value
        End Set
    End Property

    <Personalizable(), WebBrowsable()> _
    Public Property FechaDesde() As Date
        Get
            Return dFechaDesde
        End Get
        Set(ByVal value As Date)
            dFechaDesde = value
        End Set
    End Property

    <Personalizable(), WebBrowsable()> _
    Public Property FechaHasta() As Date
        Get
            Return dFechaHasta
        End Get
        Set(ByVal value As Date)
            dFechaHasta = value
        End Set
    End Property

    ''' <summary>
    ''' Devuelve el idioma de la página que alberga el control.
    ''' </summary>
    ''' <returns>El código de idioma.</returns>
    Private Function Idioma() As String
        If Not DesignMode Then
            If Me.Page Is Nothing AndAlso TypeOf Me.Page Is FSNServer.IFSNPage Then
                Return CType(Me.Page, FSNServer.IFSNPage).Idioma
            Else
                For Each x As String In HttpContext.Current.Session.Keys
                    If TypeOf HttpContext.Current.Session(x) Is FSNServer.User Then
                        Return CType(HttpContext.Current.Session(x), FSNServer.User).Idioma
                    End If
                Next
                Return String.Empty
            End If
        Else
            Return String.Empty
        End If
    End Function

    Public ReadOnly Property Textos(ByVal Index As Short) As String
        Get
            If _Textos Is Nothing Or Idioma() <> _sIdioma Then
                If Not DesignMode Then
                    Dim FSPMDict As New FSNServer.Dictionary
                    _sIdioma = Idioma()
                    If HttpContext.Current.Cache("Textos_" & _sIdioma & "_" & FSNLibrary.TiposDeDatos.ModulosIdiomas.Webparts) Is Nothing Then
                        FSPMDict.LoadData(FSNLibrary.TiposDeDatos.ModulosIdiomas.Webparts, _sIdioma)
                        HttpContext.Current.Cache.Insert("Textos_" & _sIdioma & "_" & FSNLibrary.TiposDeDatos.ModulosIdiomas.Webparts, FSPMDict.Data, Nothing, Caching.Cache.NoAbsoluteExpiration,
                                                         New TimeSpan(0, ConfigurationManager.AppSettings("TiempoExpiracionCacheMin"), 0))
                    End If
                    _Textos = HttpContext.Current.Cache("Textos_" & _sIdioma & "_" & FSNLibrary.TiposDeDatos.ModulosIdiomas.Webparts)
                    Return _Textos.Tables(0).Rows(Index).Item(1)
                Else
                    Return String.Empty
                End If
            Else
                Return _Textos.Tables(0).Rows(Index).Item(1)
            End If
        End Get
    End Property

    Public Overrides Property Title() As String
        Get
            If String.IsNullOrEmpty(MyBase.Title) Then
                If String.IsNullOrEmpty(sTitulo) Then
                    If Me.Page Is Nothing And String.IsNullOrEmpty(Textos(93)) Then
                        MyBase.Title = Textos(93)
                        sTitulo = MyBase.Title
                    Else
                        sTitulo = Textos(93)
                    End If
                End If
                Return sTitulo
            Else
                sTitulo = MyBase.Title
                Return MyBase.Title
            End If
        End Get
        Set(ByVal value As String)
            MyBase.Title = value
        End Set
    End Property

    ''' <summary>
    ''' Carga los datos por defecto para el control
    ''' </summary>
    Public Sub New()
        Me.Width = Unit.Pixel(300)
    End Sub

    ''' <summary>
    ''' Lo llama el marco de trabajo de las páginas ASP.NET para indicar a los controles de servidor que utilizan la implementación basada en la composición que creen los controles secundarios que contengan como forma de preparar la devolución o representación de los datos.
    ''' </summary>
    Protected Overrides Sub CreateChildControls()
        Controls.Clear()
        Dim img As HtmlImage = New HtmlImage()
        img.Src = Me.Page.ClientScript.GetWebResourceUrl(GetType(FSPMWebPartMonitorizacionSolicitudes), "Fullstep.PMWebControls.trans.gif")
        img.Width = 300
        img.Height = 1
        Me.Controls.Add(img)
        Dim tabla As HtmlTable = New HtmlTable()
        tabla.Width = "100%"
        tabla.Height = "55px"
        Dim fila As HtmlTableRow = New HtmlTableRow()
        fila.Height = "55px"
        Dim celda As HtmlTableCell = New HtmlTableCell()
        celda.Width = "90%"
        lblTitulo = New Label()
        lblTitulo.CssClass = "Rotulo"
        celda.Controls.Add(lblTitulo)
        celda.Controls.Add(New LiteralControl("<br/>"))
        lblMensaje = New Label
        lblMensaje.CssClass = "EtiquetaGrande"
        lblTotalSolicitudes = New Label
        lblTotalSolicitudes.CssClass = "EtiquetaGrande"
        celda.Controls.Add(lblTotalSolicitudes)
        celda.Controls.Add(lblMensaje)
        celda.Controls.Add(New LiteralControl("<br/>"))
        fila.Cells.Add(celda)
        celdaImagen = New HtmlTableCell
        celdaImagen.Align = "right"
        celdaImagen.VAlign = "bottom"
        img.Width = 125
        celdaImagen.Controls.Add(img)
        celdaImagen.Style.Add("background-image", Me.Page.ClientScript.GetWebResourceUrl(GetType(FSPMWebPartMonitorizacionSolicitudes), "Fullstep.PMWebControls.MonitorizacionSolicitudes.gif"))
        celdaImagen.Style.Add("background-position", "top center")
        celdaImagen.Style.Add("background-repeat", "no-repeat")
        fila.Cells.Add(celdaImagen)
        tabla.Rows.Add(fila)
        fila = New HtmlTableRow
        celda = New HtmlTableCell
        celda.ColSpan = 2
        lstSolicitudes = New DataList()
        lstSolicitudes.Width = Unit.Percentage(100)
        lstSolicitudes.ItemTemplate = New DataListTemplate()
        celda.Controls.Add(lstSolicitudes)
        fila.Cells.Add(celda)
        tabla.Rows.Add(fila)
        lblError = New Label
        lblError.CssClass = "Rotulo"
        lblError.Visible = False
        celda.Controls.Add(lblError)
        fila.Cells.Add(celda)
        tabla.Rows.Add(fila)
        Me.Controls.Add(tabla)

        lblTitulo.Text = Textos(93)
        sTitulo = Textos(93)

        FSPMWebPartMonitorizacionSolicitudes.sUrlEliminar = Me.Page.ClientScript.GetWebResourceUrl(GetType(FSPMWebPartMonitorizacionSolicitudes), "Fullstep.PMWebControls.eliminar.gif")

        If Not Page.IsPostBack Then _
        CargarSolicitudes()
    End Sub

    ''' <summary>
    ''' Refresca los datos del webpart
    ''' </summary>
    Public Sub Actualizar()
        CreateChildControls()
        CargarSolicitudes()
    End Sub

    ''' <summary>
    ''' Envía el contenido del control de servidor a un objeto Hninit
    ''' tmlTextWriter que se proporciona y almacena la información de seguimiento sobre el control si dicho seguimiento está habilitado.
    ''' </summary>
    ''' <param name="writer">Objeto HTmlTextWriter que recibe el contenido del control.</param>
    Public Overrides Sub RenderControl(ByVal writer As System.Web.UI.HtmlTextWriter)
        MyBase.RenderControl(writer)
    End Sub

    ''' <summary>
    ''' Carga los pedidos en el DataList del control.
    ''' </summary>
    Private Sub CargarSolicitudes()
        If Not Me.Page Is Nothing AndAlso TypeOf Me.Page Is FSNServer.IFSNPage Then
            Dim lista As New List(Of mOrden)
            Dim iNumTotal As Integer = 0
            Dim pag As FSNServer.IFSNPage = CType(Me.Page, FSNServer.IFSNPage)
            Dim FSWSServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim cInstancias As FSNServer.Instancias

            Try
                cInstancias = FSWSServer.Get_Object(GetType(FSNServer.Instancias))
                cInstancias.LoadMonitorizacion(pag.Usuario.CodPersona, pag.Idioma, mTiposSolicitudes, dImporteDesde, dImporteHasta, dFechaDesde, dFechaHasta, pag.Usuario.DateFmt)

                If cInstancias.Data.Tables.Count > 0 Then
                    iNumTotal = cInstancias.Data.Tables(0).Rows.Count
                    lblTotalSolicitudes.Text = iNumTotal
                    lblMensaje.Text = " " & Textos(75)

                    For Each fila As DataRow In cInstancias.Data.Tables(0).Rows
                        Dim mo As New mOrden()
                        mo.Instancia = fila.Item("ID")
                        mo.CodigoContrato = IIf(IsDBNull(fila.Item("CODIGO")), "", fila.Item("CODIGO"))
                        mo.Titulo = fila.Item("DEN")
                        mo.EstadoId = fila.Item("ESTADO")
                        mo.Estado = Textos(94) & ": "

                        Select Case fila.Item("ESTADO")
                            Case TipoEstadoSolic.SCRechazada, TipoEstadoSolic.SCAnulada, TipoEstadoSolic.SCPendiente, TipoEstadoSolic.SCAprobada
                                mo.Estado = mo.Estado & Textos(80)
                            Case TipoEstadoSolic.Rechazada
                                mo.Estado = mo.Estado & Textos(81)
                            Case TipoEstadoSolic.Anulada
                                mo.Estado = mo.Estado & Textos(78)
                            Case TipoEstadoSolic.SCCerrada
                                mo.Estado = mo.Estado & Textos(96)
                            Case TipoEstadoSolic.Guardada
                                mo.Estado = mo.Estado & Textos(77)
                            Case TipoEstadoSolic.EnCurso
                                mo.Estado = mo.Estado & Textos(79)
                        End Select

                        mo.Tipo = fila.Item("TIPO")
                        mo.FechaUltimaAccion = "(" & Textos(92) & ": " & FormatDate(fila.Item("FECHA"), pag.Usuario.DateFormat) & ")"
                        mo.EtapaActual = Textos(95) & ": " & IIf(fila.Item("ETAPA_ACTUAL") = "##", Textos(97), fila.Item("ETAPA_ACTUAL"))
                        mo.Usuario = "(" & fila.Item("USUARIO") & ")"
                        mo.CodigoUsuario = fila.Item("COD_USU")
                        mo.CodigoProveedor = IIf(IsDBNull(fila.Item("COD_PROVE")), Nothing, fila.Item("COD_PROVE"))
                        mo.VerDetallePersona = IIf(IsDBNull(fila.Item("VER_DETALLE_PER")), 0, fila.Item("VER_DETALLE_PER"))
                        lista.Add(mo)
                    Next
                End If

                lstSolicitudes.DataSource = lista
                lstSolicitudes.DataBind()
            Catch ex As Exception
                lblError.Text = Textos(239)
                lblError.Visible = True
            End Try
        End If
    End Sub

    ''' <summary>
    ''' Devuelve una colección de controles EditorPart personalizados que pueden utilizarse para editar un control WebPart cuando se encuentra en modo de edición.
    ''' </summary>
    ''' <returns>Una colección EditorPartCollection que contiene controles EditorPart personalizados asociados a un control WebPart.</returns>
    Public Overrides Function CreateEditorParts() As System.Web.UI.WebControls.WebParts.EditorPartCollection
        Dim editorArray As New ArrayList()
        Dim edPart As New ControlEditor()
        edPart.ID = Me.ID & "_editor1"
        editorArray.Add(edPart)
        Dim editorParts As New EditorPartCollection(editorArray)
        Return editorParts
    End Function

    Public Overrides ReadOnly Property WebBrowsableObject() As Object
        Get
            Return Me
        End Get
    End Property

    ''' <summary>
    ''' Registra el control como un control cuyo estado se debe conservar.
    ''' </summary>
    ''' <param name="e">Objeto EventArgs que contiene los datos del evento.</param>
    Protected Overrides Sub OnInit(ByVal e As System.EventArgs)
        MyBase.OnInit(e)
        Page.RegisterRequiresControlState(Me)
    End Sub

    ''' <summary>
    ''' Guarda los cambios de estado del control.
    ''' </summary>
    ''' <returns>Devuelve el estado actual del control de servidor.</returns>
    Protected Overrides Function SaveControlState() As Object
        Dim obj As Object = MyBase.SaveControlState()
        If obj IsNot Nothing Then
            obj = New Pair(obj, _Textos)
        Else
            obj = _Textos
        End If
        obj = New Pair(obj, _sIdioma)
        Return obj
    End Function

    ''' <summary>
    ''' Restaura información de estado del control.
    ''' </summary>
    ''' <param name="state">Object que representa el estado del control que se va a restaurar.</param>
    Protected Overrides Sub LoadControlState(ByVal state As Object)
        If (state IsNot Nothing) Then
            Dim p As Pair = TryCast(state, Pair)
            _sIdioma = p.Second
            state = p.First
            p = TryCast(state, Pair)
            If p IsNot Nothing Then
                MyBase.LoadControlState(p.First)
                _Textos = CType(p.Second, DataSet)
            Else
                If (TypeOf (state) Is String()) Then
                    _Textos = CType(state, DataSet)
                Else
                    MyBase.LoadControlState(state)
                End If
            End If
        End If
    End Sub

    Private Sub FSPMWebPartMonitorizacionSolicitudes_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.DesignMode Then
            ScriptManager.RegisterClientScriptResource(Me.Page, GetType(QAWebPartClientScript), "Fullstep.PMWebControls.FSQAWebPartsQA.js")
        End If
    End Sub
End Class
