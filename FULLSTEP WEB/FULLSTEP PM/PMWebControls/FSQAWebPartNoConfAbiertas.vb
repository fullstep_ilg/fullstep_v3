﻿Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.HtmlControls
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.ComponentModel
Imports System.Configuration
Imports System.Drawing.Design

Public Class FSQAWebPartNoConfAbiertas
    Inherits WebPart

    Private lblTitulo As Label
    Private lblTotal As Label
    Private lblNumTotal As Label
    Private lblMensaje As Label
    Private lstSolicitudes As DataList
    Private WithEvents btnVerSiguientes As FSNWebControls.FSNButton
    Private WithEvents btnVerAnteriores As FSNWebControls.FSNButton

    Private lblError As Label

    Private sTitulo As String

    Private _sIdioma As String
    Private mNumLineas As Integer
    Private mFechaLimite As Integer 'Indice de la combo fecha limite de resolucion
    Private mVerFechaActu As Boolean 'Indica si esta o no seleccionado el check de "Mostrar solo las modificaciones...
    Private mFechaActualizacion As Integer 'Indica el indice de la fecha de la ultima actualizacion
    Private mTipoEstados As String
    Private mTipoNoConformidades As String
    Private mTipoPeticionario As Integer
    Private msListaUnqas As String

    Private mbMonstrarMensaje As Boolean  'Muestra en mensaje de que tiene que configurar inicialmente el webpart con sus filtros...

    Public dsNoConformdidades As DataSet

    Private Shared _Textos(55) As String
    Private Shared Dateformat As System.Globalization.DateTimeFormatInfo

    Private Class DataListTemplate
        Implements ITemplate

        ''' <summary>
        ''' Define el objeto Control al que pertenecen los controles secundarios y las plantillas. Estos controles secundarios están a su vez definidos en una plantilla en línea.
        ''' </summary>
        ''' <param name="container">Objeto Control que contiene las instancias de los controles de la plantilla en línea.</param>
        Private Sub InstantiateIn(ByVal container As Control) _
           Implements ITemplate.InstantiateIn
            Dim hr = New HtmlGenericControl("hr")
            container.Controls.Add(hr)
            Dim tabla As New HtmlTable()
            tabla.Width = "100%"

            Dim fila As New HtmlTableRow()
            Dim celda As New HtmlTableCell()
            celda.ColSpan = 2
            celda.Style.Add(HtmlTextWriterStyle.FontWeight, "bold")

            fila = New HtmlTableRow
            Dim lab As New Label()
            celda.Attributes.Add("class", "ListItemLink")
            AddHandler lab.DataBinding, AddressOf Titulo_DataBinding
            celda.Controls.Add(lab)
            fila.Cells.Add(celda)
            celda = New HtmlTableCell()
            celda.Align = "right"
            celda.VAlign = "bottom"
            celda.Style.Add(HtmlTextWriterStyle.FontWeight, "bold")
            fila.Cells.Add(celda)
            tabla.Rows.Add(fila)

            fila = New HtmlTableRow()
            celda = New HtmlTableCell()
            Dim fsnlinkinfo As New FSNWebControls.FSNLinkInfo
            fsnlinkinfo.PanelInfo = "FSNPanelDatosProveedorQA"
            fsnlinkinfo.CssClass = "Rotulo"
            AddHandler fsnlinkinfo.DataBinding, AddressOf Proveedor_DataBinding
            celda.Controls.Add(fsnlinkinfo)
            fila.Cells.Add(celda)
            tabla.Rows.Add(fila)

            fila = New HtmlTableRow()
            celda = New HtmlTableCell()
            lab = New Label()
            AddHandler lab.DataBinding, AddressOf Estado_DataBinding
            celda.Controls.Add(lab)
            fila.Cells.Add(celda)
            tabla.Rows.Add(fila)

            fila = New HtmlTableRow()
            celda = New HtmlTableCell()
            lab = New Label()
            AddHandler lab.DataBinding, AddressOf FechaResolucion_DataBinding
            celda.Controls.Add(lab)
            fila.Cells.Add(celda)
            tabla.Rows.Add(fila)

            fila = New HtmlTableRow()
            celda = New HtmlTableCell()
            celda.ColSpan = 2
            lab = New Label()
            AddHandler lab.DataBinding, AddressOf FechaActualizacion_DataBinding
            celda.Controls.Add(lab)
            fila.Cells.Add(celda)
            tabla.Rows.Add(fila)


            fila = New HtmlTableRow()
            celda = New HtmlTableCell()
            fsnlinkinfo = New FSNWebControls.FSNLinkInfo
            fsnlinkinfo.PanelInfo = "FSNPanelDatosPersonaQA"
            fsnlinkinfo.CssClass = "Rotulo"
            AddHandler fsnlinkinfo.DataBinding, AddressOf Peticionario_DataBinding
            celda.Controls.Add(fsnlinkinfo)

            fila.Cells.Add(celda)
            tabla.Rows.Add(fila)
            container.Controls.Add(tabla)

        End Sub

        ''' <summary>
        ''' Se produce al enlazar la etiqueta de la plantilla en que se muestra la empresa con el origen de datos.
        ''' </summary>
        ''' <remarks>Llamada desde:=InstantiateIn;TiempoEjecucion=0seg</remarks>
        Private Sub Titulo_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim lab As Label = CType(sender, Label)
            Dim container As DataListItem = CType(lab.NamingContainer, DataListItem)
            lab.Text = "Id." & DataBinder.Eval(container.DataItem, "Instancia") & " NC-" & DataBinder.Eval(container.DataItem, "TipoNoConformidad")

            lab.CssClass = "EtiquetaMediano"

            Dim iInstancia As Long
            iInstancia = DataBinder.Eval(container.DataItem, "Instancia")

            Dim URL As String
            URL = ConfigurationManager.AppSettings("rutaQA") & "frames.aspx?pagina=" & HttpContext.Current.Server.UrlEncode("noconformidad/DetalleNoConformidad.aspx?NoConformidad=" & DataBinder.Eval(container.DataItem, "NOCONFORMIDAD"))
            lab.Attributes.Add("onclick", "return ComprobarInstanciaEnProcesoWebPart(" & iInstancia & ",'" & URL & "','" & ConfigurationManager.AppSettings("rutaFS") & "'," & FSNLibrary.TiposDeDatos.TipoDeSolicitud.NoConformidad & ");")
        End Sub


        ''' <summary>
        ''' Se produce al enlazar la etiqueta de la plantilla en que se muestra el proveedor con el origen de datos.
        ''' </summary>
        ''' <param name="sender">propios del evento</param>
        ''' <param name="e">propios del evento</param>
        ''' <remarks>Llamada desde:=InstantiateIn;TiempoEjecucion=0seg</remarks>
        Private Sub Proveedor_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim fsn As FSNWebControls.FSNLinkInfo = CType(sender, FSNWebControls.FSNLinkInfo)
            Dim container As DataListItem = CType(fsn.NamingContainer, DataListItem)
            fsn.Text = DataBinder.Eval(container.DataItem, "CodProveedor") & "-" & DataBinder.Eval(container.DataItem, "Proveedor")
            fsn.ContextKey = DataBinder.Eval(container.DataItem, "CodProveedor")
        End Sub

        ''' <summary>
        ''' Se produce al enlazar la etiqueta de la plantilla en que se muestra el estado del pedido con el origen de datos.
        ''' </summary>
        ''' <param name="sender">propios del evento</param>
        ''' <param name="e">propios del evento</param>
        ''' <remarks>Llamada desde:=InstantiateIn;TiempoEjecucion=0seg</remarks>
        Private Sub Estado_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim lab As Label = CType(sender, Label)
            Dim container As DataListItem = CType(lab.NamingContainer, DataListItem)
            Dim sEstado As String = ""
            Dim sEstadosNoConformidad As String() = Split(DataBinder.Eval(container.DataItem, "Estado"), ";")

            'sEstado = "Pendiente de emitir"
            If sEstadosNoConformidad(FiltroEstadoNoConformidad.PendienteEmitir - 1) Then
                sEstado = _Textos(37) & ", "
            End If

            'sEstado = "Sin respuesta por parte del proveedor"
            If sEstadosNoConformidad(FiltroEstadoNoConformidad.SinRespuestaProveedor - 1) Then
                sEstado = sEstado & _Textos(17) & ", "
            End If

            'sEstado = "Con Acciones pendientes de revisar por el peticionario"
            If sEstadosNoConformidad(FiltroEstadoNoConformidad.AccPendienteRevPeticionario - 1) Then
                sEstado = sEstado & _Textos(38) & ", "
            End If

            'sEstado = "Con acciones pendientes de finalizar por el proveedor"
            If sEstadosNoConformidad(FiltroEstadoNoConformidad.AccPendienteFinProveedor - 1) Then
                sEstado = sEstado & _Textos(39) & ", "
            End If

            'sEstado = "Con acciones pendientes de finalizar por el peticionario"
            If sEstadosNoConformidad(FiltroEstadoNoConformidad.AccPendienteFinPeticionario - 1) Then
                sEstado = sEstado & _Textos(40) & ", "
            End If

            'sEstado = "Pendientes de cerrar"
            If sEstadosNoConformidad(FiltroEstadoNoConformidad.PendienteCerrar - 1) Then
                sEstado = sEstado & _Textos(41) & ", "
            End If

            'sEstado = "Pendientes de revisar cierre"
            If sEstadosNoConformidad(FiltroEstadoNoConformidad.PendienteRevCierre - 1) Then
                sEstado = sEstado & _Textos(42) & ", "
            End If

            If sEstado <> "" Then
                sEstado = Left(sEstado, sEstado.Length - 2)
                sEstado = Left(sEstado, 1).ToUpper & Right(sEstado, sEstado.Length - 1).ToLower
            End If

            lab.Text = sEstado
            lab.Font.Bold = False
        End Sub

        ''' <summary>
        ''' Se produce al enlazar la etiqueta de la plantilla en que se muestra la fecha de resolucion de la noConformidad con el origen de datos.
        ''' </summary>
        ''' <param name="sender">propios del evento</param>
        ''' <param name="e">propios del evento</param>
        ''' <remarks>Llamada desde:=InstantiateIn;TiempoEjecucion=0seg</remarks>
        Private Sub FechaResolucion_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim lab As Label = CType(sender, Label)
            Dim container As DataListItem = CType(lab.NamingContainer, DataListItem)
            If DataBinder.Eval(container.DataItem, "FechaResolucion") <> "#12:00:00 AM#" Then
                'lab.Text = "Fecha resolucion: " & FormatDate(DataBinder.Eval(container.DataItem, "FechaResolucion"), DateFormat)
                lab.Text = "<b>" & _Textos(19) & ": </b>" & FormatDate(DataBinder.Eval(container.DataItem, "FechaResolucion"), Dateformat)
            End If

        End Sub

        ''' <summary>
        ''' Se produce al enlazar la etiqueta de la plantilla en que se muestra la fecha de Actualizacion con el origen de datos.
        ''' </summary>
        ''' <param name="sender">propios del evento</param>
        ''' <param name="e">propios del evento</param>
        ''' <remarks>Llamada desde:=InstantiateIn;TiempoEjecucion=0seg</remarks>
        Private Sub FechaActualizacion_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim lab As Label = CType(sender, Label)
            Dim container As DataListItem = CType(lab.NamingContainer, DataListItem)
            If DataBinder.Eval(container.DataItem, "FechaUltAct") <> "#12:00:00 AM#" Then
                'lab.Text = "Fecha última actualizacion: " & FormatDate(DataBinder.Eval(container.DataItem, "FechaUltAct"), DateFormat)
                lab.Text = "<b>" & _Textos(18) & ": </b>" & FormatDate(DataBinder.Eval(container.DataItem, "FechaUltAct"), Dateformat)
            End If

        End Sub

        ''' <summary>
        ''' Se produce al enlazar la etiqueta de la plantilla en que se muestra el Peticionario, solo si es distinto al usuario de entrada.
        ''' </summary>
        ''' <param name="sender">propios del evento</param>
        ''' <param name="e">propios del evento</param>
        ''' <remarks>Llamada desde:=InstantiateIn;TiempoEjecucion=0seg</remarks>
        Private Sub Peticionario_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim fsn As FSNWebControls.FSNLinkInfo = CType(sender, FSNWebControls.FSNLinkInfo)
            Dim container As DataListItem = CType(fsn.NamingContainer, DataListItem)
            Dim sPeticionario As String = DataBinder.Eval(container.DataItem, "Peticionario")

            If sPeticionario <> "" Then ' "" = -> El peticionario es el mismo al de la entrada y no tiene que mostrarlo
                'lc.Text = "<b>Peticionario: </b>" & sPeticionario
                fsn.Text = "<b>" & _Textos(20) & ": </b>" & sPeticionario
                fsn.ContextKey = DataBinder.Eval(container.DataItem, "CodPeticionario")

            End If



        End Sub


        ''' <summary>
        ''' Devuelve un string con la fecha pasada en dDate formateada según oDateFormat, por defecto aplica formato d:01/01/1900
        ''' </summary>
        ''' <param name="dDate">fecha a formatear</param>
        ''' <param name="oDateFormat">IFormatProvider</param>
        ''' <param name="sFormat">Format: d :08/17/2000; D :Thursday, August 17, 2000; f :Thursday, August 17, 2000 16:32; F :Thursday, August 17, 2000 16:32:32; g :08/17/2000 16:32;G :08/17/2000 16:32:32...
        '''</param>
        ''' <returns>String con la fecha</returns>
        ''' <remarks>Llamada desde=:FechaResolucion_DataBinding;Tiempo ejecucion = 0</remarks>
        Public Function FormatDate(ByVal dDate As Date, ByVal oDateFormat As System.Globalization.DateTimeFormatInfo, Optional ByVal sFormat As String = "d") As String
            Return dDate.ToString(sFormat, oDateFormat)
        End Function

    End Class

    <Flags()> _
    Public Enum FechaLimiteValores As Integer
        Vacio = 0
        Sobrepasada = 1
        Dias1 = 2
        Dias2 = 3
        Dias3 = 4
        Dias4 = 5
        Dias5 = 6
        Dias6 = 7
        Semana1 = 8
        Semana2 = 9
        Semana3 = 10
        Mes1 = 11
    End Enum

    <Flags()> _
Public Enum FechaActualizacionValores As Integer
        UltimoDia = 0
        Dias2 = 1
        Dias3 = 2
        Dias4 = 3
        Dias5 = 4
        Dias6 = 5
        Semana1 = 6
        Semana2 = 7
        Semana3 = 8
        Mes1 = 9
    End Enum

    <Flags()> _
Public Enum TipoPeticionarioValores As Integer
        Todos = 0
        DelUsuario = 1
    End Enum

    <Flags()> _
    Public Enum FiltroEstadoNoConformidad As Integer
        Ninguno = 0
        PendienteEmitir = 1
        SinRespuestaProveedor = 2
        AccPendienteRevPeticionario = 3
        AccPendienteFinProveedor = 4
        AccPendienteFinPeticionario = 5
        PendienteCerrar = 6
        PendienteRevCierre = 7
    End Enum


    <Serializable()> _
    Public Class mOrden
        Private mlInstancia As Long
        Private mlNoConformidad As Long
        Private msTipoNoConformidad As String
        Private msCodProveedor As String
        Private msProveedor As String
        Private msEstado As String
        Private mdFechaResolucion As Date
        Private mdFechaUltAct As Date
        Private msCodPeticionario As String
        Private msPeticionario As String


        Public Property Instancia() As Long
            Get
                Return mlInstancia
            End Get
            Set(ByVal value As Long)
                mlInstancia = value
            End Set
        End Property

        Public Property NoConformidad() As Long
            Get
                Return mlNoConformidad
            End Get
            Set(ByVal value As Long)
                mlNoConformidad = value
            End Set
        End Property

        Public Property TipoNoConformidad() As String
            Get
                Return msTipoNoConformidad
            End Get
            Set(ByVal value As String)
                msTipoNoConformidad = value
            End Set
        End Property

        Public Property CodProveedor() As String
            Get
                Return msCodProveedor
            End Get
            Set(ByVal value As String)
                msCodProveedor = value
            End Set
        End Property

        Public Property Proveedor() As String
            Get
                Return msProveedor
            End Get
            Set(ByVal value As String)
                msProveedor = value
            End Set
        End Property

        Public Property Estado() As String
            Get
                Return msEstado
            End Get
            Set(ByVal value As String)
                msEstado = value
            End Set
        End Property

        Public Property Peticionario() As String
            Get
                Return msPeticionario
            End Get
            Set(ByVal value As String)
                msPeticionario = value
            End Set
        End Property

        Public Property CodPeticionario() As String
            Get
                Return msCodPeticionario
            End Get
            Set(ByVal value As String)
                msCodPeticionario = value
            End Set
        End Property



        Public Property FechaResolucion() As Date
            Get
                Return mdFechaResolucion
            End Get
            Set(ByVal value As Date)
                mdFechaResolucion = value
            End Set
        End Property

        Public Property FechaUltAct() As Date
            Get
                Return mdFechaUltAct
            End Get
            Set(ByVal value As Date)
                mdFechaUltAct = value
            End Set
        End Property



    End Class


    ''******* Elementos del part!!!!

    Private Class ControlEditor
        Inherits EditorPart

        Private _txtTitulo As TextBox
        Private _txtAncho As TextBox
        Private _reqAncho As RequiredFieldValidator
        Private _ranAncho As RangeValidator
        Private _wneNumNoConformidades As Infragistics.Web.UI.EditorControls.WebNumericEditor
        Private _reqNumNoConformidades As RequiredFieldValidator
        Private _chkTipoEstados As CheckBoxList
        Private _chkTipoNoConformidades As CheckBoxList
        Private _lstFechaLimite As ListBox
        Private _chkActualizacion As CheckBox
        Private _lstFechaActualizacion As ListBox
        Private _lstPeticionario As ListBox
        Private _tvUNQA As Global.System.Web.UI.WebControls.TreeView

        ''' <summary>
        ''' Lo llama el marco de trabajo de las páginas ASP.NET para indicar a los controles de servidor que utilizan la implementación basada en la composición que creen los controles secundarios que contengan como forma de preparar la devolución o representación de los datos.
        ''' </summary>
        Protected Overrides Sub CreateChildControls()
            Dim part As FSQAWebPartNoConfAbiertas = CType(WebPartToEdit, FSQAWebPartNoConfAbiertas)
            Controls.Clear()

            _txtTitulo = New TextBox()
            _txtTitulo.Width = Unit.Pixel(250)
            _txtTitulo.Text = part.Title
            Controls.Add(_txtTitulo)
            _txtAncho = New TextBox()
            _txtAncho.MaxLength = 3
            _txtAncho.Width = Unit.Pixel(30)
            _txtAncho.Text = part.Width.Value.ToString()
            _txtAncho.ID = Me.ID & "_txtAncho"
            Controls.Add(_txtAncho)
            _reqAncho = New RequiredFieldValidator()
            _reqAncho.ID = _txtAncho.ID & "_Req"
            _reqAncho.ErrorMessage = "*"
            _reqAncho.ControlToValidate = _txtAncho.ID
            Controls.Add(_reqAncho)
            _ranAncho = New RangeValidator()
            _ranAncho.ID = _txtAncho.ID & "_Ran"
            _ranAncho.Type = ValidationDataType.Integer
            _ranAncho.MaximumValue = 500
            _ranAncho.MinimumValue = 200
            _ranAncho.ErrorMessage = "(200-500)"
            _ranAncho.ControlToValidate = _txtAncho.ID
            Controls.Add(_ranAncho)
            'nº de no Conformidades
            _wneNumNoConformidades = New Infragistics.Web.UI.EditorControls.WebNumericEditor
            _wneNumNoConformidades.ID = Me.ID & "_wneNumNoConformidades"
            _wneNumNoConformidades.Width = Unit.Pixel(30)
            _wneNumNoConformidades.MaxLength = 2
            _wneNumNoConformidades.DataMode = Infragistics.Web.UI.EditorControls.NumericDataMode.Int
            Controls.Add(_wneNumNoConformidades)
            _reqNumNoConformidades = New RequiredFieldValidator()
            _reqNumNoConformidades.ID = _reqNumNoConformidades.ID & "_Req"
            _reqNumNoConformidades.ErrorMessage = "*"
            _reqNumNoConformidades.ControlToValidate = _wneNumNoConformidades.ID
            Controls.Add(_reqNumNoConformidades)
            _chkTipoEstados = New CheckBoxList
            _chkTipoEstados.Items.Clear()
            _chkTipoEstados.Items.Add(New ListItem(part.Textos(37), 1)) 'Pendiente de emitir
            _chkTipoEstados.Items.Add(New ListItem(part.Textos(17), 2)) 'Sin respuesta por parte del proveedor
            _chkTipoEstados.Items.Add(New ListItem(part.Textos(38), 3)) 'Con acciones pendientes de revisar por el peticionario
            _chkTipoEstados.Items.Add(New ListItem(part.Textos(39), 4)) 'Con acciones pendientes de finalizar por el proveedor
            _chkTipoEstados.Items.Add(New ListItem(part.Textos(40), 5)) 'Con acciones pendientes de finalizar por el peticionario
            _chkTipoEstados.Items.Add(New ListItem(part.Textos(41), 6)) 'Pendientes de cerrar
            _chkTipoEstados.Items.Add(New ListItem(part.Textos(42), 7)) 'Pendientes de revisar cierre
            Controls.Add(_chkTipoEstados)

            Dim pag As FSNServer.IFSNPage = CType(Me.Page, FSNServer.IFSNPage)
            Dim FSWSServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")

            Try
                _chkTipoNoConformidades = New CheckBoxList

                Dim cSolicitudes As FSNServer.Solicitudes
                cSolicitudes = FSWSServer.Get_Object(GetType(FSNServer.Solicitudes))

                _chkTipoNoConformidades.Items.Clear()
                _chkTipoNoConformidades.DataTextField = "DEN"
                _chkTipoNoConformidades.DataValueField = "ID"
                cSolicitudes.LoadData(pag.Usuario.Cod, pag.Idioma, 1, 3)
                cSolicitudes.Data.Tables(0).Rows.RemoveAt(0)
                _chkTipoNoConformidades.DataSource = cSolicitudes.Data
                _chkTipoNoConformidades.DataBind()
                _chkTipoNoConformidades.ID = Me.ID & "_chkTipoNoConformidades"
                _chkTipoNoConformidades.RepeatColumns = 2
                Controls.Add(_chkTipoNoConformidades)
            Catch ex As Exception
                'Unicamente buscamos que la carga no casque.No lanzamos la excepción ni la tratamos.
                'Lo que si hacemos es añadir los controles para que luego no casque
                Controls.Add(_chkTipoNoConformidades)
            End Try


            _lstFechaLimite = New ListBox
            _lstFechaLimite.Items.Clear()
            _lstFechaLimite.SelectionMode = ListSelectionMode.Single
            _lstFechaLimite.Rows = 1
            _lstFechaLimite.Items.Add("")
            _lstFechaLimite.Items.Add(part.Textos(23)) 'sobrepasada
            _lstFechaLimite.Items.Add("1 " & part.Textos(24)) 'dia
            For i As Short = 2 To 6
                _lstFechaLimite.Items.Add(i & " " & part.Textos(25)) 'dias
            Next
            _lstFechaLimite.Items.Add("1 " & part.Textos(26)) ' Semana
            _lstFechaLimite.Items.Add("2 " & part.Textos(27)) ' Semanas
            _lstFechaLimite.Items.Add("3 " & part.Textos(27)) ' Semanas
            _lstFechaLimite.Items.Add("1  " & part.Textos(28)) ' mes

            Controls.Add(_lstFechaLimite)

            _chkActualizacion = New CheckBox
            Controls.Add(_chkActualizacion)

            _lstFechaActualizacion = New ListBox
            _lstFechaActualizacion.Items.Clear()
            _lstFechaActualizacion.SelectionMode = ListSelectionMode.Single
            _lstFechaActualizacion.Rows = 1
            _lstFechaActualizacion.Items.Add(part.Textos(29)) 'el último día
            For i As Short = 2 To 6
                _lstFechaActualizacion.Items.Add(Replace(part.Textos(30), "X", i)) '"hace 2 días"
            Next
            _lstFechaActualizacion.Items.Add(part.Textos(31)) '"hace 1 semana"
            _lstFechaActualizacion.Items.Add(Replace(part.Textos(32), "X", 2)) '"hace 2 semanas"
            _lstFechaActualizacion.Items.Add(Replace(part.Textos(32), "X", 3)) '"hace 3 semanas"
            _lstFechaActualizacion.Items.Add(part.Textos(33)) '"hace 1 mes"
            Controls.Add(_lstFechaActualizacion)


            _lstPeticionario = New ListBox
            _lstPeticionario.Items.Clear()
            _lstPeticionario.SelectionMode = ListSelectionMode.Single
            _lstPeticionario.Rows = 1
            _lstPeticionario.Items.Add(New ListItem(part.Textos(34), 0))
            _lstPeticionario.Items.Add(New ListItem(part.Textos(35), 1))
            Controls.Add(_lstPeticionario)

            _tvUNQA = New TreeView
            'Cargar treeView
            Try
                Dim PMUnidadesQA As FSNServer.UnidadesNeg = FSWSServer.Get_Object(GetType(FSNServer.UnidadesNeg))
                Dim dsUNQA As DataSet = PMUnidadesQA.UsuGetData_hds(pag.Usuario.Cod, FSNLibrary.TipoAccesoUNQAS.ConsultarNoConformidades, sIdi:=pag.Idioma, iPyme:=pag.Usuario.Pyme, bNoConf:=True)
                Dim b As New System.Web.UI.WebControls.TreeNodeBinding
                _tvUNQA.ShowCheckBoxes = TreeNodeTypes.All
                _tvUNQA.DataBindings.Add(b)
                _tvUNQA.DataBindings.Item(0).TextField = "DEN"
                _tvUNQA.DataBindings.Item(0).ValueField = "ID"
                _tvUNQA.DataBindings.Item(0).ToolTipField = "ACCESO"
                _tvUNQA.DataSource = New HierarchicalDataSet(dsUNQA.Tables(0).DefaultView, "ID", "PADRE")
                _tvUNQA.DataBind()
                ConfigurarAccesoUNQAS(_tvUNQA.Nodes)
                Dim HijosConAcceso As Boolean = False
                EliminarUNQASinAcceso(_tvUNQA.Nodes, _tvUNQA.Nodes.Count, HijosConAcceso)
                '_tvUNQA.CollapseAll()
                _tvUNQA.ExpandAll()
                PMUnidadesQA = Nothing
                PMUnidadesQA = Nothing

                Controls.Add(_tvUNQA)
            Catch ex As Exception
                'Unicamente buscamos que la carga no casque.No lanzamos la excepción ni la tratamos.
                'Lo que si hacemos es añadir los controles para que luego no casque
                Controls.Add(_tvUNQA)
            End Try
        End Sub

        ''' <summary>
        ''' Procedimiento que recorre los nodos del treeview de unqas
        ''' Conf del treeview por defecto. Todos los nodos tienen check/Todos están contraidos
        ''' Si no tiene acceso a la unqa y a la unqa padre tampoco no mostramos check (Hemos cargado el campo ACCESO en la propiedad ToolTip)
        ''' Expandimos los nodos cunado tiene acceso al menos a una unqa.
        ''' </summary>
        ''' <param name="Nodos">Coleccion de Nodos del treeview de UNQAs</param>
        ''' <param name="sChequarNodos">strng con una lista de nodods separados por coma a chequear</param>
        ''' <remarks></remarks>
        Sub ConfigurarAccesoUNQAS(ByRef Nodos As TreeNodeCollection, Optional ByVal sChequarNodos As String = "")
            For Each Nodo As TreeNode In Nodos
                Dim bAcceso As Boolean = IIf(Nodo.ToolTip = "0", False, True)

                Dim bHijos As Boolean = IIf(Nodo.ChildNodes.Count = 0, False, True)

                If Not bAcceso Then
                    Nodo.ShowCheckBox = False

                End If
                'chequeamos las unidades de negoico de la ultima busqueda
                If sChequarNodos <> "" Then
                    If sChequarNodos.IndexOf("," & Nodo.Value & ",") >= 0 Then
                        Nodo.Checked = True
                    End If
                End If

                'Recorremos los hijos
                If bHijos Then
                    ConfigurarAccesoUNQAS(Nodo.ChildNodes, sChequarNodos)
                End If

            Next

        End Sub

        ''' <summary>
        ''' Procedimiento que Elimina aquellos nodos que no tiene acceso
        ''' </summary>
        ''' <param name="Nodos">Coleccion de Nodos del treeview de UNQAs</param>
        ''' <param name="CountNodes">nº nodos hijos del nodo</param>
        ''' <param name="HijosConAcceso">Si los hijos tienen acceso</param>
        ''' <remarks>Llamada desde=CreateChildControls; Tiempo ejecucion=0,3seg.</remarks>
        Sub EliminarUNQASinAcceso(ByRef Nodos As TreeNodeCollection, ByRef CountNodes As Integer, Optional ByRef HijosConAcceso As Boolean = False)
            Dim i As Integer = 0
            Do While i < CountNodes
                If Nodos(i).ToolTip = "0" AndAlso Nodos(i).ChildNodes.Count = 0 Then
                    If Nodos(i).Depth > 0 Then
                        If Nodos(i).Parent.ToolTip = "0" Then
                            If Nodos(i).Parent.Depth > 0 Then
                                If Nodos(i).Parent.Parent.ToolTip = "0" Then
                                    Nodos.Remove(Nodos(i))
                                    CountNodes = CountNodes - 1
                                Else
                                    i += 1
                                End If
                            Else
                                Nodos.Remove(Nodos(i))
                                CountNodes = CountNodes - 1
                            End If
                        Else
                            i += 1
                        End If
                    Else
                        Nodos.Remove(Nodos(i))
                        CountNodes = CountNodes - 1
                    End If
                ElseIf Nodos(i).ToolTip = "0" AndAlso Nodos(i).ChildNodes.Count > 0 Then
                    EliminarUNQASinAcceso(Nodos(i).ChildNodes, Nodos(i).ChildNodes.Count, HijosConAcceso)

                    If Not HijosConAcceso Or Nodos(i).ChildNodes.Count = 0 Then
                        Nodos.Remove(Nodos(i))
                        CountNodes = CountNodes - 1
                    Else
                        If Not Nodos(i).Parent Is Nothing Then
                            HijosConAcceso = True
                        Else
                            HijosConAcceso = False
                        End If
                        i = i + 1
                    End If
                Else
                    If Nodos(i).ChildNodes.Count > 0 Then
                        EliminarUNQASinAcceso(Nodos(i).ChildNodes, Nodos(i).ChildNodes.Count, HijosConAcceso)

                        If Not HijosConAcceso Then
                            Nodos(i).Collapse()
                        End If
                    End If

                    If Not Nodos(i).Parent Is Nothing Then
                        HijosConAcceso = True
                    End If

                    i = i + 1
                End If

            Loop

        End Sub

        ''' <summary>
        ''' Presenta el contenido del control en el sistema de escritura especificado. Este método lo usan principalmente los programadores de controles. 
        ''' </summary>
        ''' <param name="writer">HtmlTextWriter que representa la secuencia de salida para presentar contenido HTML en el cliente.</param>
        Protected Overrides Sub RenderContents(ByVal writer As System.Web.UI.HtmlTextWriter)
            Dim part As FSQAWebPartNoConfAbiertas = CType(WebPartToEdit, FSQAWebPartNoConfAbiertas)
            writer.Write("<b>" & part.Textos(13) & ":</b> ")
            _txtTitulo.RenderControl(writer)
            writer.WriteBreak()
            writer.Write("<b>" & part.Textos(15) & ":</b> ")
            _txtAncho.RenderControl(writer)
            _reqAncho.RenderControl(writer)
            _ranAncho.RenderControl(writer)
            writer.WriteBreak()
            writer.Write("<b>" & part.Textos(43) & ":</b> ") '"Nº de no conformidades a mostrar"
            _wneNumNoConformidades.RenderControl(writer)
            _reqNumNoConformidades.RenderControl(writer)
            writer.WriteBreak()

            writer.Write("<div style='border: 1px solid #CCCCCC;height=45;vertical-align:middle'>")
            writer.Write("<table border=0><tr><td height=45>")
            writer.Write(part.Textos(44)) '"Días para la <b>fecha límite de resolución:   </b>"
            _lstFechaLimite.RenderControl(writer)
            writer.Write("</td></tr></table>")

            writer.Write("</div>")

            writer.WriteBreak()
            writer.Write("<div style='border: 1px solid #CCCCCC;'>")
            writer.Write("<b>&nbsp;&nbsp;" & part.Textos(21) & "</b>") '"Fecha de actualización"
            writer.WriteBreak()
            _chkActualizacion.RenderControl(writer)
            writer.Write("&nbsp;&nbsp;" & part.Textos(22) & "&nbsp;&nbsp;") 'Mostrar solo las modificadas durante
            _lstFechaActualizacion.RenderControl(writer)
            writer.WriteBreak()
            writer.Write("</div>")


            writer.WriteBreak()
            writer.Write("<div style='border: 1px solid #CCCCCC;'><b>&nbsp;&nbsp;" & part.Textos(45) & " </b>") 'Filtrar por estado de las no Conformidades
            writer.WriteBreak()
            _chkTipoEstados.RenderControl(writer)
            writer.WriteBreak()
            writer.Write("</div>")

            writer.WriteBreak()
            writer.Write("<div style='border: 1px solid #CCCCCC;'><b>&nbsp;&nbsp;" & part.Textos(46) & " </b>") 'Filtrar por tipo de no conformidades
            writer.WriteBreak()
            _chkTipoNoConformidades.RenderControl(writer)
            writer.WriteBreak()
            writer.Write("</div>")
            writer.WriteBreak()

            writer.Write("<div style='border: 1px solid #CCCCCC;'><b>&nbsp;&nbsp;" & part.Textos(20) & " </b>") '"Peticionario "

            writer.WriteBreak()
            writer.Write("&nbsp;&nbsp;" & part.Textos(47) & "  ") 'Ver no conformidades
            _lstPeticionario.RenderControl(writer)
            writer.WriteBreak()
            writer.Write("</div>")
            writer.WriteBreak()

            writer.Write("<div style='border: 1px solid #CCCCCC;overflow: scroll;'><b>&nbsp;&nbsp;" & part.Textos(48) & "</b>") 'Unidad de negocio

            writer.WriteBreak()
            _tvUNQA.RenderControl(writer)
            writer.WriteBreak()
            writer.Write("</div>")
            writer.WriteBreak()


        End Sub

        ''' <summary>
        ''' Guarda los valores de un control EditorPart en las propiedades correspondientes del control WebPart asociado.
        ''' </summary>
        ''' <returns>Es true si la acción de guardar los valores del control EditorPart en el control WebPart se realiza correctamente; en caso contrario (si se produce un error), es false.</returns>
        ''' <remarks>llamada desde:=propia pagina;Tiempo Ejecucion:=0,5seg.</remarks>
        Public Overrides Function ApplyChanges() As Boolean
            Dim iNumConformidadesMostrar As Integer
            Dim part As FSQAWebPartNoConfAbiertas = CType(WebPartToEdit, FSQAWebPartNoConfAbiertas)
            Dim bMostrarError As Boolean = False
            _reqAncho.Validate()
            _ranAncho.Validate()

            _reqNumNoConformidades.Validate()
            If wneNumNoConformidades.Text <> "" Then
                iNumConformidadesMostrar = CType(wneNumNoConformidades.Value, Integer)
            Else
                iNumConformidadesMostrar = -1
            End If

            If _reqAncho.IsValid() And _ranAncho.IsValid() And _reqNumNoConformidades.IsValid Then
                Dim tipoEstados As String = ""
                For Each x As ListItem In _chkTipoEstados.Items
                    If x.Selected Then
                        tipoEstados = tipoEstados & CType(x.Value, Integer) & ","
                    End If
                Next
                If Len(tipoEstados) > 0 Then
                    tipoEstados = Left(tipoEstados, Len(tipoEstados) - 1)
                End If

                Dim tipoNoConformidad As String = ""
                For Each x As ListItem In _chkTipoNoConformidades.Items
                    If x.Selected Then
                        tipoNoConformidad = tipoNoConformidad & CType(x.Value, Integer) & ","
                    End If
                Next
                If Len(tipoNoConformidad) > 0 Then
                    tipoNoConformidad = Left(tipoNoConformidad, Len(tipoNoConformidad) - 1)
                End If

                Dim listaUNQAs As String = ""
                For Each Nodo As TreeNode In _tvUNQA.Nodes 'nivel 0
                    If Nodo.Checked = True Then
                        listaUNQAs = listaUNQAs & Nodo.Value & ","
                    End If
                    For Each NodoHijos As TreeNode In Nodo.ChildNodes 'nivel 1
                        If NodoHijos.Checked = True Then
                            listaUNQAs = listaUNQAs & NodoHijos.Value & ","
                        End If
                        For Each NodoNietos As TreeNode In NodoHijos.ChildNodes 'nivel 2
                            If NodoNietos.Checked = True Then
                                listaUNQAs = listaUNQAs & NodoNietos.Value & ","
                            End If
                            For Each NodoBiznietos As TreeNode In NodoNietos.ChildNodes 'nivel 3
                                If NodoBiznietos.Checked = True Then
                                    listaUNQAs = listaUNQAs & NodoBiznietos.Value & ","
                                End If
                            Next
                        Next
                    Next
                Next
                If Len(listaUNQAs) > 0 Then
                    listaUNQAs = Left(listaUNQAs, Len(listaUNQAs) - 1)
                End If


                If ((listaUNQAs = "") And (tipoEstados = "") And (tipoNoConformidad = "") And (_chkActualizacion.Checked = False) And (FechaLimiteValores.Vacio = _lstFechaLimite.SelectedIndex)) Then
                    part.MonstrarMensaje = True
                Else
                    part.MonstrarMensaje = False
                End If
                part.Title = _txtTitulo.Text
                part.Width = Unit.Pixel(CType(_txtAncho.Text, Integer))
                part.mNumLineas = iNumConformidadesMostrar
                part.mTipoEstados = tipoEstados
                part.mTipoNoConformidades = tipoNoConformidad
                part.indiceFechaActualizacion = _lstFechaActualizacion.SelectedIndex
                part.optVerFechaActu = (_chkActualizacion.Checked = True)
                part.IndiceFechaLimite = _lstFechaLimite.SelectedIndex
                part.TipoPeticionario = _lstPeticionario.SelectedIndex
                part.ListaUnqas = listaUNQAs


                part.CargarSolicitudes(False)

                Return True


            Else
                Return False
            End If
        End Function
        ''' <summary>
        ''' Obtiene los valores de propiedad de un control WebPart correspondientes a su control EditorPart asociado.
        ''' </summary>
        Public Overrides Sub SyncChanges()
            Dim part As FSQAWebPartNoConfAbiertas = CType(WebPartToEdit, FSQAWebPartNoConfAbiertas)
            txtTitulo.Text = part.Title
            txtAncho.Text = part.Width.Value.ToString()
            wneNumNoConformidades.Text = IIf(part.NumLineas = -1, "", part.NumLineas)
            For Each x As ListItem In chkEstados.Items
                x.Selected = part.buscarElemento(part.mTipoEstados, x.Value)
            Next

            For Each x As ListItem In chkTipoNoConformidades.Items
                x.Selected = part.buscarElemento(part.mTipoNoConformidades, x.Value)

            Next

            For Each Nodo As TreeNode In _tvUNQA.Nodes
                Nodo.Checked = part.buscarElemento(part.ListaUnqas, CType(Nodo.Value, String))
                For Each NodoHijos As TreeNode In Nodo.ChildNodes
                    NodoHijos.Checked = part.buscarElemento(part.ListaUnqas, CType(NodoHijos.Value, String))
                    For Each NodoNietos As TreeNode In NodoHijos.ChildNodes
                        NodoNietos.Checked = part.buscarElemento(part.ListaUnqas, CType(NodoNietos.Value, String))
                    Next
                Next
            Next


            Me.lstFechaLimite.SelectedIndex = part.IndiceFechaLimite
            Me.optVerFechaAct.Checked = part.optVerFechaActu
            Me.lstFechaActualizacion.SelectedIndex = part.indiceFechaActualizacion
            Me.lstPeticionario.SelectedIndex = part.TipoPeticionario

        End Sub

        Private ReadOnly Property txtTitulo() As TextBox
            Get
                EnsureChildControls()
                Return _txtTitulo
            End Get
        End Property

        Private ReadOnly Property txtAncho() As TextBox
            Get
                EnsureChildControls()
                Return _txtAncho
            End Get
        End Property


        Private ReadOnly Property wneNumNoConformidades() As Infragistics.Web.UI.EditorControls.WebNumericEditor
            Get
                EnsureChildControls()
                Return _wneNumNoConformidades
            End Get
        End Property

        Private ReadOnly Property chkEstados() As CheckBoxList
            Get
                EnsureChildControls()
                Return _chkTipoEstados
            End Get
        End Property


        Private ReadOnly Property chkTipoNoConformidades() As CheckBoxList
            Get
                EnsureChildControls()
                Return _chkTipoNoConformidades
            End Get
        End Property

        Private ReadOnly Property lstFechaLimite() As ListBox
            Get
                EnsureChildControls()
                Return _lstFechaLimite
            End Get
        End Property

        Private ReadOnly Property optVerFechaAct() As CheckBox
            Get
                EnsureChildControls()
                Return _chkActualizacion
            End Get
        End Property

        Private ReadOnly Property lstFechaActualizacion() As ListBox
            Get
                EnsureChildControls()
                Return _lstFechaActualizacion
            End Get
        End Property

        Private ReadOnly Property lstPeticionario() As ListBox
            Get
                EnsureChildControls()
                Return _lstPeticionario
            End Get
        End Property
    End Class

    <Personalizable(), WebBrowsable(), DefaultValue("")> _
    Public Property TipoEstados() As String
        Get
            Return mTipoEstados
        End Get
        Set(ByVal value As String)
            mTipoEstados = value
        End Set
    End Property

    <Personalizable(), WebBrowsable(), DefaultValue("")> _
    Public Property TipoNoConformidades() As String
        Get
            Return mTipoNoConformidades
        End Get
        Set(ByVal value As String)
            mTipoNoConformidades = value
        End Set
    End Property

    <Personalizable(), WebBrowsable(), DefaultValue(5)> _
    Public Property NumLineas() As Integer
        Get
            Return mNumLineas
        End Get
        Set(ByVal value As Integer)
            mNumLineas = value
        End Set
    End Property

    <Personalizable(), WebBrowsable(), DefaultValue(1)> _
    Public Property MonstrarMensaje() As Boolean
        Get
            Return mbMonstrarMensaje
        End Get
        Set(ByVal value As Boolean)
            mbMonstrarMensaje = value
        End Set
    End Property



    <Personalizable(), WebBrowsable(), DefaultValue(0)> _
    Public Property IndiceFechaLimite() As Integer
        Get
            Return mFechaLimite
        End Get
        Set(ByVal value As Integer)
            mFechaLimite = value
        End Set
    End Property

    <Personalizable(), WebBrowsable(), DefaultValue(0)> _
    Public Property optVerFechaActu() As Boolean
        Get
            Return mVerFechaActu
        End Get
        Set(ByVal value As Boolean)
            mVerFechaActu = value
        End Set
    End Property

    <Personalizable(), WebBrowsable(), DefaultValue(0)> _
    Public Property indiceFechaActualizacion() As Integer
        Get
            Return mFechaActualizacion
        End Get
        Set(ByVal value As Integer)
            mFechaActualizacion = value
        End Set
    End Property

    <Personalizable(), WebBrowsable(), DefaultValue(0)> _
    Public Property TipoPeticionario() As Integer
        Get
            Return mTipoPeticionario
        End Get
        Set(ByVal value As Integer)
            mTipoPeticionario = value
        End Set
    End Property

    <Personalizable(), WebBrowsable(), DefaultValue("")> _
    Public Property ListaUnqas() As String
        Get
            Return msListaUnqas
        End Get
        Set(ByVal value As String)
            msListaUnqas = value
        End Set
    End Property

    Public Property ImagenNoConformidad() As String
        Get
            Dim s As String = CStr(ViewState("ImagenNoConformidad"))
            If s Is Nothing Then s = String.Empty
            Return s
        End Get
        Set(ByVal value As String)
            ViewState("ImagenNoConformidad") = value
        End Set
    End Property


    ''' <summary>
    ''' Devuelve el idioma de la página que alberga el control.
    ''' </summary>
    ''' <returns>El código de idioma.</returns>
    Private Function Idioma() As String
        If Not DesignMode Then
            If Me.Page Is Nothing AndAlso TypeOf Me.Page Is FSNServer.IFSNPage Then
                Return CType(Me.Page, FSNServer.IFSNPage).Idioma
            Else
                For Each x As String In HttpContext.Current.Session.Keys
                    If TypeOf HttpContext.Current.Session(x) Is FSNServer.User Then
                        Return CType(HttpContext.Current.Session(x), FSNServer.User).Idioma
                    End If
                Next
                Return String.Empty
            End If
        Else
            Return String.Empty
        End If
    End Function

    Public ReadOnly Property Textos(ByVal Index As Short) As String
        Get
            If String.IsNullOrEmpty(_Textos(Index)) Or Idioma() <> _sIdioma Then
                If Not DesignMode Then
                    Dim FSNDict As FSNServer.Dictionary = New FSNServer.Dictionary()
                    _sIdioma = Idioma()
                    FSNDict.LoadData(FSNLibrary.TiposDeDatos.ModulosIdiomas.Webparts, _sIdioma)
                    For i As Short = 0 To 2
                        _Textos(i) = FSNDict.Data.Tables(0).Rows(i + 5).Item(1)
                    Next
                    For i As Short = 3 To 16
                        _Textos(i) = FSNDict.Data.Tables(0).Rows(i + 6).Item(1)
                    Next
                    'Textos existentes
                    _Textos(17) = FSNDict.Data.Tables(0).Rows(101).Item(1) '"Sin Respuesta por parte del proveedor"
                    _Textos(18) = FSNDict.Data.Tables(0).Rows(141).Item(1) '"Fecha última actualizacion
                    _Textos(19) = FSNDict.Data.Tables(0).Rows(142).Item(1) '"Fecha resolución"
                    _Textos(20) = FSNDict.Data.Tables(0).Rows(113).Item(1) '"Peticionario"
                    _Textos(21) = FSNDict.Data.Tables(0).Rows(136).Item(1) '"Fecha de actualización"
                    _Textos(22) = FSNDict.Data.Tables(0).Rows(137).Item(1) '"Mostrar solo las modificadas durante"

                    'textos a partir de 122 (121 indicador i) --> 
                    For i As Short = 23 To 35
                        _Textos(i) = FSNDict.Data.Tables(0).Rows(i + 98).Item(1)
                    Next
                    'textos nuevos
                    'textos a partir de 145 --> 
                    For i As Short = 36 To 50
                        _Textos(i) = FSNDict.Data.Tables(0).Rows(i + 108).Item(1)
                    Next
                    _Textos(51) = FSNDict.Data.Tables(0).Rows(100).Item(1) 'Ver siguientes
                    _Textos(52) = FSNDict.Data.Tables(0).Rows(159).Item(1) 'Edite el webpart para poder configurar su contenido.
                    _Textos(53) = FSNDict.Data.Tables(0).Rows(234).Item(1) 'El nº de no conformidades a mostrar debe de ser númerico.
                    _Textos(54) = FSNDict.Data.Tables(0).Rows(238).Item(1) 'Ver anteriores

                    _Textos(55) = FSNDict.Data.Tables(0).Rows(239).Item(1)
                    Return _Textos(Index)
                Else
                    'Si falla la carga de textos devolvemos la cadena vacia para que la carga de la página siga su curso
                    Return String.Empty
                End If
            Else
                Return _Textos(Index)
            End If
        End Get
    End Property

    Public Overrides Property Title() As String
        Get
            If String.IsNullOrEmpty(MyBase.Title) Then
                If String.IsNullOrEmpty(sTitulo) Then
                    If Me.Page Is Nothing And String.IsNullOrEmpty(Textos(49)) Then '"No Conformidades abiertas"
                        MyBase.Title = Textos(49)
                        sTitulo = MyBase.Title
                    Else
                        sTitulo = Textos(49)
                    End If
                End If
                Return sTitulo
            Else
                sTitulo = MyBase.Title
                Return MyBase.Title
            End If
        End Get
        Set(ByVal value As String)
            MyBase.Title = value
        End Set
    End Property

    ''' <summary>
    ''' Carga los datos por defecto para el control
    ''' </summary>
    ''' <remarks>Llamada desde; Tiempo máximo=0seg.</remarks>
    Public Sub New()
        Me.Width = Unit.Pixel(300)
        NumLineas = 5
        mTipoEstados = ""
        MonstrarMensaje = True
    End Sub

    ''' <summary>
    ''' Lo llama el marco de trabajo de las páginas ASP.NET para indicar a los controles de servidor que utilizan la implementación basada en la composición que creen los controles secundarios que contengan como forma de preparar la devolución o representación de los datos.
    ''' </summary>
    ''' <remarks>Llamada desde; Tiempo máximo=0,4seg.</remarks>
    Protected Overrides Sub CreateChildControls()
        Controls.Clear()
        Dim img As HtmlImage = New HtmlImage()
        img.Src = Me.Page.ClientScript.GetWebResourceUrl(GetType(FSQAWebPartNoConfAbiertas), "Fullstep.PMWebControls.trans.gif")
        img.Width = 300
        img.Height = 1
        Me.Controls.Add(img)
        Dim tabla As HtmlTable = New HtmlTable()
        tabla.ID = "tabla_NoConf"
        tabla.Width = "100%"
        tabla.Height = "55px"
        Dim fila As HtmlTableRow = New HtmlTableRow()
        fila.Height = "55px"
        Dim celda As HtmlTableCell = New HtmlTableCell()
        celda.Width = "90%"
        lblTitulo = New Label()
        lblTitulo.CssClass = "Rotulo"

        lblMensaje = New Label
        lblMensaje.CssClass = "Rotulo"


        celda.Controls.Add(lblTitulo)
        celda.Controls.Add(lblMensaje)
        celda.Controls.Add(New LiteralControl("<br/>"))
        lblNumTotal = New Label()
        lblNumTotal.CssClass = "EtiquetaGrande"
        celda.Controls.Add(lblNumTotal)
        lblTotal = New Label()
        lblTotal.CssClass = "EtiquetaGrande"
        celda.Controls.Add(lblTotal)
        fila.Cells.Add(celda)
        celda = New HtmlTableCell
        celda.Align = "right"
        celda.VAlign = "bottom"
        img.Width = 125
        celda.Controls.Add(img)

        If DesignMode Then
            celda.Style.Add("background-image", "images/NoConformidades.gif")
        Else
            If CStr(ViewState("ImagenNoConformidad")) <> String.Empty Then
                celda.Style.Add("background-image", Replace(CStr(ViewState("ImagenNoConformidad")), "~/", HttpContext.Current.Request.ApplicationPath & "/"))
            Else
                celda.Style.Add("background-image", Me.Page.ClientScript.GetWebResourceUrl(GetType(FSQAWebPartNoConfAbiertas), "Fullstep.PMWebControls.NoConformidades.gif"))
            End If
        End If
        celda.Style.Add("background-position", "top right")
        celda.Style.Add("background-repeat", "no-repeat")
        fila.Cells.Add(celda)
        tabla.Rows.Add(fila)

        fila = New HtmlTableRow
        celda = New HtmlTableCell
        celda.ColSpan = 2
        lstSolicitudes = New DataList()
        lstSolicitudes.Width = Unit.Percentage(100)
        lstSolicitudes.ItemTemplate = New DataListTemplate()
        celda.Controls.Add(lstSolicitudes)
        fila.Cells.Add(celda)
        tabla.Rows.Add(fila)

        fila = New HtmlTableRow
        celda = New HtmlTableCell
        celda.ColSpan = 2
        btnVerSiguientes = New FSNWebControls.FSNButton()
        btnVerSiguientes.ID = "btnVerSiguientes"
        celda.Controls.Add(btnVerSiguientes)
        btnVerAnteriores = New FSNWebControls.FSNButton()
        btnVerSiguientes.ID = "btnVerAnteriores"
        celda.Controls.Add(btnVerAnteriores)
        fila.Cells.Add(celda)
        tabla.Rows.Add(fila)

        lblError = New System.Web.UI.WebControls.Label
        lblError.Visible = False
        celda.Controls.Add(lblError)
        fila.Cells.Add(celda)
        tabla.Rows.Add(fila)

        Me.Controls.Add(tabla)

        lblTitulo.Text = Textos(49)
        lblTotal.Text = " " & Textos(50)  '"No Conformidades" 'Poner texto
        btnVerSiguientes.Text = Textos(51)
        btnVerAnteriores.Text = Textos(54)

        If Not Page.IsPostBack Then _
            CargarSolicitudes(False)
    End Sub

    ''' <summary>
    ''' Refresca los datos del webpart
    ''' </summary>
    Public Sub Actualizar()
        CreateChildControls()
        CargarSolicitudes(False)
    End Sub

    ''' <summary>
    ''' Envía el contenido del control de servidor a un objeto HtmlTextWriter que se proporciona y almacena la información de seguimiento sobre el control si dicho seguimiento está habilitado.
    ''' </summary>
    ''' <param name="writer">Objeto HTmlTextWriter que recibe el contenido del control.</param>
    Public Overrides Sub RenderControl(ByVal writer As System.Web.UI.HtmlTextWriter)
        MyBase.RenderControl(writer)
    End Sub

    ''' <summary>
    ''' Carga las NoConformidades en el DataList del control.
    ''' </summary>
    ''' <param name="bVerSiguientes">Si carga o no en el webPart con las siguientes noConformidades</param>
    ''' <remarks>Llamadas desde: ApplyChanges,CreateChildControls,btnVerTodas_Click ;Tiempo Ejecucion:=0,4seg.</remarks>
    Private Sub CargarSolicitudes(ByVal bVerSiguientes As Boolean)
        If Not Me.Page Is Nothing AndAlso TypeOf Me.Page Is FSNServer.IFSNPage Then
            Dim lista As New List(Of mOrden)
            Dim pag As FSNServer.IFSNPage = CType(Me.Page, FSNServer.IFSNPage)
            Dim FSWSServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dateformat = pag.Usuario.DateFormat
            Dim i As Integer
            Dim numTotal As Integer
            Dim sPeticionario As String
            Dim FechaLimite As Date = Now
            Dim dFechaActualizacion As Date = Now
            Dim iSobrepasada As Integer

            If optVerFechaActu = True Then
                Select Case indiceFechaActualizacion
                    Case FechaActualizacionValores.UltimoDia
                        dFechaActualizacion = DateAdd(DateInterval.Day, -1, Now)
                    Case FechaActualizacionValores.Dias2
                        dFechaActualizacion = DateAdd(DateInterval.Day, -2, Now)
                    Case FechaActualizacionValores.Dias3
                        dFechaActualizacion = DateAdd(DateInterval.Day, -3, Now)
                    Case FechaActualizacionValores.Dias4
                        dFechaActualizacion = DateAdd(DateInterval.Day, -4, Now)
                    Case FechaActualizacionValores.Dias5
                        dFechaActualizacion = DateAdd(DateInterval.Day, -5, Now)
                    Case FechaActualizacionValores.Dias6
                        dFechaActualizacion = DateAdd(DateInterval.Day, -6, Now)
                    Case FechaActualizacionValores.Semana1
                        dFechaActualizacion = DateAdd(DateInterval.Day, -7, Now)
                    Case FechaActualizacionValores.Semana2
                        dFechaActualizacion = DateAdd(DateInterval.Day, -14, Now)
                    Case FechaActualizacionValores.Semana3
                        dFechaActualizacion = DateAdd(DateInterval.Day, -21, Now)
                    Case FechaActualizacionValores.Mes1
                        dFechaActualizacion = DateAdd(DateInterval.Month, -1, Now)
                End Select

            End If

            Dim bPorUsuario As Boolean
            If TipoPeticionarioValores.Todos = TipoPeticionario Then
                bPorUsuario = False
            Else
                bPorUsuario = True
            End If
            sPeticionario = pag.Usuario.Cod
            iSobrepasada = 2
            Select Case IndiceFechaLimite
                Case FechaLimiteValores.Vacio
                    iSobrepasada = 0
                Case FechaLimiteValores.Sobrepasada
                    iSobrepasada = 1
                Case FechaLimiteValores.Dias1
                    FechaLimite = DateAdd(DateInterval.Day, 1, Now)
                Case FechaLimiteValores.Dias2
                    FechaLimite = DateAdd(DateInterval.Day, 2, Now)
                Case FechaLimiteValores.Dias3
                    FechaLimite = DateAdd(DateInterval.Day, 3, Now)
                Case FechaLimiteValores.Dias4
                    FechaLimite = DateAdd(DateInterval.Day, 4, Now)
                Case FechaLimiteValores.Dias5
                    FechaLimite = DateAdd(DateInterval.Day, 5, Now)
                Case FechaLimiteValores.Dias6
                    FechaLimite = DateAdd(DateInterval.Day, 6, Now)
                Case FechaLimiteValores.Semana1
                    FechaLimite = DateAdd(DateInterval.Day, 7, Now)
                Case FechaLimiteValores.Semana2
                    FechaLimite = DateAdd(DateInterval.Day, 14, Now)
                Case FechaLimiteValores.Semana3
                    FechaLimite = DateAdd(DateInterval.Day, 21, Now)
                Case FechaLimiteValores.Mes1
                    FechaLimite = DateAdd(DateInterval.Month, 1, Now)
            End Select
            If MonstrarMensaje = True Then
                lblTitulo.Visible = False
                lblTotal.Visible = False
                lblNumTotal.Visible = False
                btnVerSiguientes.Visible = False
                btnVerAnteriores.Visible = False
                lblMensaje.Text = Textos(52)
                lblMensaje.Visible = True
                lstSolicitudes.Visible = False
            Else
                lblTitulo.Visible = True
                lblTotal.Visible = True
                lblNumTotal.Visible = True
                lblMensaje.Visible = False


                Dim oNoConformidades As FSNServer.NoConformidades
                oNoConformidades = FSWSServer.Get_Object(GetType(FSNServer.NoConformidades))

                Try
                    oNoConformidades.LoadDataWebPartNoConformidades(iSobrepasada, FechaLimite, optVerFechaActu, _
                        dFechaActualizacion, TipoEstados, TipoNoConformidades, bPorUsuario, sPeticionario, _
                        Me.ListaUnqas, pag.Idioma, pag.Usuario.QARestNoConformUsu, pag.Usuario.QARestNoConformUO, _
                        pag.Usuario.QARestNoConformDep, pag.Usuario.QARestProvMaterial, pag.Usuario.QARestProvEquipo, _
                        pag.Usuario.QARestProvContacto)
                    dsNoConformdidades = oNoConformidades.Data
                    oNoConformidades = Nothing

                    i = 0
                    If dsNoConformdidades.Tables.Count > 0 Then
                        numTotal = dsNoConformdidades.Tables(0).Rows.Count
                        lstSolicitudes.Visible = True
                        For Each fila As DataRow In dsNoConformdidades.Tables(0).Rows
                            Dim mo As New mOrden()
                            i = i + 1
                            If ((bVerSiguientes And i > NumLineas) Or (Not bVerSiguientes)) Then
                                mo.Instancia = fila.Item("INSTANCIA")
                                mo.NoConformidad = fila.Item("NOCONFORMIDAD")
                                mo.TipoNoConformidad = fila.Item("TIPONOCONFORMIDAD")
                                mo.CodProveedor = fila.Item("PROVE")
                                mo.Proveedor = fila.Item("PROVEEDOR")
                                mo.Estado = fila.Item("ESTADO_WEBPART1") & ";" & fila.Item("ESTADO_WEBPART2") & ";" & _
                                            fila.Item("ESTADO_WEBPART3") & ";" & fila.Item("ESTADO_WEBPART4") & ";" & _
                                            fila.Item("ESTADO_WEBPART5") & ";" & fila.Item("ESTADO_WEBPART6") & ";" & _
                                            fila.Item("ESTADO_WEBPART7")
                                If Not IsDBNull(fila.Item("FEC_LIM_RESOL")) Then
                                    mo.FechaResolucion = fila.Item("FEC_LIM_RESOL")
                                End If
                                mo.CodPeticionario = fila.Item("PET")
                                If pag.Usuario.CodPersona <> fila.Item("PET") Then
                                    mo.Peticionario = fila.Item("PETICIONARIO")
                                Else
                                    mo.Peticionario = ""
                                End If
                                mo.FechaUltAct = fila.Item("FEC_ACT")
                                lista.Add(mo)
                                If Not bVerSiguientes Then
                                    If i = NumLineas Then
                                        Exit For
                                    End If
                                End If

                            End If
                        Next
                    End If


                    lstSolicitudes.DataSource = lista
                    lstSolicitudes.DataBind()
                    lblNumTotal.Text = numTotal.ToString
                Catch ex As Exception
                    lblError.Text = Textos(55)
                    lblError.Visible = True

                    lblTotal.Visible = False
                End Try

                Dim tabla As HtmlTable = New HtmlTable()

                If Not bVerSiguientes Then
                    If (numTotal > NumLineas) And (NumLineas > 0) Then
                        btnVerSiguientes.Visible = True
                        btnVerAnteriores.Visible = False
                    Else
                        btnVerSiguientes.Visible = False
                        btnVerAnteriores.Visible = IIf(numTotal > NumLineas, True, False)
                    End If
                End If

            End If
        End If
    End Sub

    ''' <summary>
    ''' Devuelve una colección de controles EditorPart personalizados que pueden utilizarse para editar un control WebPart cuando se encuentra en modo de edición.
    ''' </summary>
    ''' <returns>Una colección EditorPartCollection que contiene controles EditorPart personalizados asociados a un control WebPart.</returns>
    Public Overrides Function CreateEditorParts() As System.Web.UI.WebControls.WebParts.EditorPartCollection
        Dim editorArray As New ArrayList()
        Dim edPart As New ControlEditor()
        edPart.ID = Me.ID & "_editor1"
        editorArray.Add(edPart)
        Dim editorParts As New EditorPartCollection(editorArray)
        Return editorParts
    End Function

    Public Overrides ReadOnly Property WebBrowsableObject() As Object
        Get
            Return Me
        End Get
    End Property

    ''' <summary>
    ''' Registra el control como un control cuyo estado se debe conservar.
    ''' </summary>
    ''' <param name="e">Objeto EventArgs que contiene los datos del evento.</param>
    Protected Overrides Sub OnInit(ByVal e As System.EventArgs)
        MyBase.OnInit(e)
        Page.RegisterRequiresControlState(Me)
    End Sub

    ''' <summary>
    ''' Guarda los cambios de estado del control.
    ''' </summary>
    ''' <returns>Devuelve el estado actual del control de servidor.</returns>
    Protected Overrides Function SaveControlState() As Object
        Dim obj As Object = MyBase.SaveControlState()
        If obj IsNot Nothing Then
            obj = New Pair(obj, _Textos)
        Else
            obj = _Textos
        End If
        obj = New Pair(obj, _sIdioma)
        Return obj
    End Function

    ''' <summary>
    ''' Restaura información de estado del control.
    ''' </summary>
    ''' <param name="state">Object que representa el estado del control que se va a restaurar.</param>
    Protected Overrides Sub LoadControlState(ByVal state As Object)
        If (state IsNot Nothing) Then
            Dim p As Pair = TryCast(state, Pair)
            _sIdioma = p.Second
            state = p.First
            p = TryCast(state, Pair)
            If p IsNot Nothing Then
                MyBase.LoadControlState(p.First)
                _Textos = CType(p.Second, String())
            Else
                If (TypeOf (state) Is String()) Then
                    _Textos = CType(state, String())
                Else
                    MyBase.LoadControlState(state)
                End If
            End If
        End If
    End Sub

    ''' <summary>
    ''' Nos indica si un nodo esta dentro de una lista o no
    ''' </summary>
    ''' <param name="listaNodos">Cadena de nodos seleccionada</param>
    ''' <param name="nodo">Valor del nodo a buscar en la lista</param>        
    ''' <returns>True o False si a enontrado o no el nodo</returns>
    ''' <remarks>Llamada desde=SyncChanges//CargarCertificados; Tiempo máximo=0,2seg.</remarks>
    Private Function buscarElemento(ByVal listaNodos As String, ByVal nodo As String) As Boolean
        Dim pos As Integer
        Dim elementosSeleccionados() As String
        Dim bEncontrado As Boolean = False
        pos = InStr(listaNodos, nodo)
        If pos > 0 Then
            If Len(listaNodos) = Len(nodo) Then 'El nodo coincide con la seleccion
                Return True
            Else
                If pos - 1 > 0 Then
                    bEncontrado = False
                    elementosSeleccionados = Split(listaNodos, ",")
                    For i = 0 To UBound(elementosSeleccionados)
                        If elementosSeleccionados(i) = nodo Then
                            bEncontrado = True
                            Exit For
                        End If
                    Next
                    Return bEncontrado
                Else
                    Return True

                End If

            End If
        Else
            Return False
        End If
    End Function
    ''' <summary>
    ''' Muestra todas las No Conformidades en el webPart
    ''' </summary>
    ''' <param name="sender">las del evento onClick</param>
    ''' <param name="e">las del evento onClick</param>        
    ''' <remarks>Llamada desde; Tiempo máximo=1,3seg.</remarks>
    Private Sub btnVerTodas_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnVerSiguientes.Click
        Me.CargarSolicitudes(True)
        btnVerSiguientes.Visible = False
        btnVerAnteriores.Visible = True
    End Sub
    ''' <summary>
    ''' Muestra todas las No Conformidades cargadas inicialmente
    ''' </summary>
    ''' <param name="sender">las del evento onClick</param>
    ''' <param name="e">las del evento onClick</param>        
    ''' <remarks>Llamada desde; Tiempo máximo=1,3seg.</remarks>
    Private Sub btnVerAnteriores_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnVerAnteriores.Click
        Me.CargarSolicitudes(False)
        btnVerSiguientes.Visible = True
        btnVerAnteriores.Visible = False
    End Sub

    Private Sub FSQAWebPartNoConfAbiertas_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.DesignMode Then
            ScriptManager.RegisterClientScriptResource(Me, GetType(QAWebPartClientScript), "Fullstep.PMWebControls.FSQAWebPartsQA.js")
        End If
    End Sub
End Class
