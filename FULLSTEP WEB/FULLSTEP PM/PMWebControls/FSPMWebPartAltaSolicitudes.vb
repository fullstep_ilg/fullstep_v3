﻿Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.HtmlControls
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.ComponentModel
Imports System.Configuration
Imports Fullstep.FSNLibrary
Imports Fullstep.FSNLibrary.TiposDeDatos

Public Class FSPMWebPartAltaSolicitudes
    Inherits WebPart

    Private lblTitulo As Label
    Private lblError As Label
    Private lstSolicitudes As DataList
    Private lnkVerTodos As FSNWebControls.FSNHyperlink
    Private mTiposSolicitudes As String
    Private mSolicitudesFavoritas As String
    Private sTitulo As String
    Private _Textos As DataSet
    Private _sIdioma As String

    Private Class DataListTemplate
        Implements ITemplate

        ''' <summary>
        ''' Define el objeto Control al que pertenecen los controles secundarios y las plantillas. Estos controles secundarios están a su vez definidos en una plantilla en línea.
        ''' </summary>
        ''' <param name="container">Objeto Control que contiene las instancias de los controles de la plantilla en línea.</param>
        Private Sub InstantiateIn(ByVal container As Control) _
           Implements ITemplate.InstantiateIn
            Dim tabla As New HtmlTable()
            tabla.Width = "100%"
            tabla.Attributes.Add("class", "ListItemLink")
            AddHandler tabla.DataBinding, AddressOf IrAAltaSolicitud_DataBinding
            Dim fila As New HtmlTableRow()
            Dim celda As New HtmlTableCell()
            Dim img As New HtmlImage
            AddHandler img.DataBinding, AddressOf Favorito_DataBinding
            celda.Controls.Add(img)
            celda.Width = "10%"
            fila.Cells.Add(celda)
            celda = New HtmlTableCell()
            Dim lc As New Literal()
            AddHandler lc.DataBinding, AddressOf Solicitud_DataBinding
            celda.Controls.Add(lc)
            fila.Cells.Add(celda)

            Dim lbl As New Label()
            lbl.CssClass = "EtiquetaPeque"
            AddHandler lbl.DataBinding, AddressOf FechaUltimaEmision_DataBinding
            celda.Controls.Add(lbl)
            fila.Cells.Add(celda)

            tabla.Rows.Add(fila)
            container.Controls.Add(tabla)
        End Sub

        ''' <summary>
        ''' Se produce al enlazar la etiqueta de la plantilla en que se muestra la solicitud con el origen de datos.
        ''' </summary>
        Private Sub Solicitud_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim lc As Literal = CType(sender, Literal)
            Dim container As DataListItem = CType(lc.NamingContainer, DataListItem)
            lc.Text = DataBinder.Eval(container.DataItem, "Solicitud") & " " '& DataBinder.Eval(container.DataItem, "FechaUltimaEmision")
        End Sub

        ''' <summary>
        ''' Se produce al enlazar la etiqueta de la plantilla en que se muestra la solicitud con el origen de datos.
        ''' </summary>
        Private Sub FechaUltimaEmision_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim lbl As Label = CType(sender, Label)
            Dim container As DataListItem = CType(lbl.NamingContainer, DataListItem)
            lbl.Text = DataBinder.Eval(container.DataItem, "FechaUltimaEmision")
        End Sub

        ''' <summary>
        ''' Se produce al enlazar la tabla de la plantilla que muestra los datos del pedido con el origen de datos.
        ''' </summary>
        Private Sub IrAAltaSolicitud_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs)

            Dim tab As HtmlTable = CType(sender, HtmlTable)
            Dim container As DataListItem = CType(tab.NamingContainer, DataListItem)
            If DataBinder.Eval(container.DataItem, "Tipo") = "5" Then 'Tipo Contrato
                tab.Attributes.Add("onclick", "TempCargando(); window.location.href='" & ConfigurationManager.AppSettings("rutaPM") & "Frames.aspx?pagina=" & HttpContext.Current.Server.UrlEncode("contratos/altaContrato.aspx?DesdeInicio=1&Solicitud=" & DataBinder.Eval(container.DataItem, "Id") & IIf(DataBinder.Eval(container.DataItem, "Favorito"), "&Favorito=1&IdFavorito=" & DataBinder.Eval(container.DataItem, "FavoritoId"), "") & "&NomContrato=" & DataBinder.Eval(container.DataItem, "Solicitud")) & "';")
            Else
                tab.Attributes.Add("onclick", "TempCargando(); window.location.href='" & ConfigurationManager.AppSettings("rutaPM") & "Frames.aspx?pagina=" & HttpContext.Current.Server.UrlEncode("alta/NWAlta.aspx?DesdeInicio=1&Solicitud=" & DataBinder.Eval(container.DataItem, "Id") & IIf(DataBinder.Eval(container.DataItem, "Favorito"), "&Favorito=1&IdFavorito=" & DataBinder.Eval(container.DataItem, "FavoritoId"), "")) & "';")
            End If
        End Sub

        ''' <summary>
        ''' Muestra o no la imagen de solicitud favorita dependiendo del valor de "Favorito"
        ''' </summary>
        Private Sub Favorito_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim img As HtmlImage = CType(sender, HtmlImage)
            Dim container As DataListItem = CType(img.NamingContainer, DataListItem)
            If DataBinder.Eval(container.DataItem, "Favorito") Then
                img.Src = ConfigurationManager.AppSettings("rutaPM") & "alta/images/favoritos_small_17.gif"
            Else
                img.Src = ConfigurationManager.AppSettings("ruta") & "images/trans.gif"
            End If
        End Sub
    End Class
    <Serializable()>
    Public Class mOrden
        Private miId As Integer
        Private mSolicitud As String
        Private mbFavorito As Boolean
        Private miFavId As Integer
        Private mFechaUltimaEmision As String
        Private miTipo As Short

        Public Property Id() As Integer
            Get
                Return miId
            End Get
            Set(ByVal value As Integer)
                miId = value
            End Set
        End Property
        Public Property Solicitud() As String
            Get
                Return mSolicitud
            End Get
            Set(ByVal value As String)
                mSolicitud = value
            End Set
        End Property
        Public Property Favorito() As Boolean
            Get
                Return mbFavorito
            End Get
            Set(ByVal value As Boolean)
                mbFavorito = value
            End Set
        End Property
        Public Property FavoritoId() As Integer
            Get
                Return miFavId
            End Get
            Set(ByVal value As Integer)
                miFavId = value
            End Set
        End Property
        Public Property FechaUltimaEmision() As String
            Get
                Return mFechaUltimaEmision
            End Get
            Set(ByVal value As String)
                mFechaUltimaEmision = value
            End Set
        End Property
        Public Property Tipo() As Short
            Get
                Return miTipo
            End Get
            Set(ByVal value As Short)
                miTipo = value
            End Set
        End Property
    End Class
    Private Class ControlEditor
        Inherits EditorPart

        Private _txtTitulo As TextBox
        Private _txtAncho As TextBox
        Private _reqAncho As RequiredFieldValidator
        Private _ranAncho As RangeValidator
        Private _chkTipoSolicitud As CheckBoxList
        Private _tvSolicitudesFavoritas As TreeView

        ''' <summary>
        ''' Lo llama el marco de trabajo de las páginas ASP.NET para indicar a los controles de servidor que utilizan la implementación basada en la composición que creen los controles secundarios que contengan como forma de preparar la devolución o representación de los datos.
        ''' </summary>
        Protected Overrides Sub CreateChildControls()
            Dim part As FSPMWebPartAltaSolicitudes = CType(WebPartToEdit, FSPMWebPartAltaSolicitudes)
            Controls.Clear()
            _txtTitulo = New TextBox()
            _txtTitulo.Width = Unit.Pixel(250)
            _txtTitulo.Text = part.Title
            Controls.Add(_txtTitulo)
            _txtAncho = New TextBox()
            _txtAncho.MaxLength = 3
            _txtAncho.Width = Unit.Pixel(30)
            _txtAncho.Text = part.Width.Value.ToString()
            _txtAncho.ID = Me.ID & "_txtAncho"
            Controls.Add(_txtAncho)
            _reqAncho = New RequiredFieldValidator()
            _reqAncho.ID = _txtAncho.ID & "_Req"
            _reqAncho.ErrorMessage = "*"
            _reqAncho.ControlToValidate = _txtAncho.ID
            Controls.Add(_reqAncho)
            _ranAncho = New RangeValidator()
            _ranAncho.ID = _txtAncho.ID & "_Ran"
            _ranAncho.Type = ValidationDataType.Integer
            _ranAncho.MaximumValue = 500
            _ranAncho.MinimumValue = 200
            _ranAncho.ErrorMessage = "(200-500)"
            _ranAncho.ControlToValidate = _txtAncho.ID
            Controls.Add(_ranAncho)

            Dim pag As FSNServer.IFSNPage = CType(Me.Page, FSNServer.IFSNPage)
            Dim cSolicitudes As FSNServer.Solicitudes
            Dim FSWSServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")

            cSolicitudes = FSWSServer.Get_Object(GetType(FSNServer.Solicitudes))

            _chkTipoSolicitud = New CheckBoxList
            _chkTipoSolicitud.Items.Clear()
            Try
                cSolicitudes.LoadTiposSolicitudesRolPeticionario(pag.Usuario.CodPersona, pag.Idioma, True, If(TipoSolicitud = TipoDeSolicitud.Otros, TipoDeSolicitud.Otros, TipoSolicitud))
                _chkTipoSolicitud.DataTextField = "DEN"
                _chkTipoSolicitud.DataValueField = "ID"
                _chkTipoSolicitud.DataSource = cSolicitudes.Data
                _chkTipoSolicitud.DataBind()
            Catch ex As Exception

            End Try
            _chkTipoSolicitud.ID = Me.ID & "_chkTipoSolicitud"
            _chkTipoSolicitud.RepeatColumns = 2
            _chkTipoSolicitud.Width = Unit.Percentage(100)
            Controls.Add(_chkTipoSolicitud)

            _tvSolicitudesFavoritas = New TreeView
            _tvSolicitudesFavoritas.ShowCheckBoxes = TreeNodeTypes.Leaf
            _tvSolicitudesFavoritas.ExpandAll()
            _tvSolicitudesFavoritas.ShowExpandCollapse = False
            Try
                cSolicitudes.LoadSolicitudesFavoritas(pag.Usuario.Cod, pag.Idioma, If(TipoSolicitud = TipoDeSolicitud.Otros, TipoDeSolicitud.Otros, TipoSolicitud))
                Dim tnb As New TreeNodeBinding
                _tvSolicitudesFavoritas.DataBindings.Add(tnb)
                _tvSolicitudesFavoritas.DataBindings.Item(0).TextField = "DEN"
                _tvSolicitudesFavoritas.DataBindings.Item(0).ValueField = "ID"
                _tvSolicitudesFavoritas.DataSource = New HierarchicalDataSet(cSolicitudes.Data, "ID_TEMP", "PADRE_TEMP")
                _tvSolicitudesFavoritas.DataBind()
            Catch ex As Exception

            End Try
            _tvSolicitudesFavoritas.ID = Me.ID & "_tvSolicitudesFavoritas"
            Controls.Add(_tvSolicitudesFavoritas)
        End Sub

        ''' <summary>
        ''' Presenta el contenido del control en el sistema de escritura especificado. Este método lo usan principalmente los programadores de controles. 
        ''' </summary>
        ''' <param name="writer">HtmlTextWriter que representa la secuencia de salida para presentar contenido HTML en el cliente.</param>
        Protected Overrides Sub RenderContents(ByVal writer As System.Web.UI.HtmlTextWriter)
            Dim part As FSPMWebPartAltaSolicitudes = CType(WebPartToEdit, FSPMWebPartAltaSolicitudes)
            writer.Write("<b>" & part.Textos(19) & ":</b> ")
            _txtTitulo.RenderControl(writer)
            writer.WriteBreak()
            writer.Write("<b>" & part.Textos(21) & ":</b> ")
            _txtAncho.RenderControl(writer)
            _reqAncho.RenderControl(writer)
            _ranAncho.RenderControl(writer)
            writer.WriteBreak()
            writer.WriteBreak()
            If _chkTipoSolicitud.Items.Count > 0 Then
                writer.Write("<div style='border: 1px solid #CCCCCC;padding-left: 10px'><b>" & part.Textos(62) & "</b>")
                writer.WriteBreak()
                _chkTipoSolicitud.RenderControl(writer)
                writer.WriteBreak()
                writer.Write("</div>")
                writer.WriteBreak()
            End If
            If _tvSolicitudesFavoritas.Nodes.Count > 0 Then
                writer.Write("<div style='border: 1px solid #CCCCCC;padding-left: 10px'><b>" & part.Textos(90) & "</b>")
                writer.WriteBreak()
                _tvSolicitudesFavoritas.RenderControl(writer)
                writer.WriteBreak()
                writer.Write("</div>")
                writer.WriteBreak()
            End If
        End Sub

        ''' <summary>
        ''' Guarda los valores de un control EditorPart en las propiedades correspondientes del control WebPart asociado.
        ''' </summary>
        ''' <returns>Es true si la acción de guardar los valores del control EditorPart en el control WebPart se realiza correctamente; en caso contrario (si se produce un error), es false.</returns>
        Public Overrides Function ApplyChanges() As Boolean
            _reqAncho.Validate()
            _ranAncho.Validate()
            If _reqAncho.IsValid() And _ranAncho.IsValid() Then
                Dim part As FSPMWebPartAltaSolicitudes = CType(WebPartToEdit, FSPMWebPartAltaSolicitudes)
                part.Title = _txtTitulo.Text
                part.Width = Unit.Pixel(CType(_txtAncho.Text, Integer))
                Dim tipoSolicitud As String = ""
                For Each x As ListItem In _chkTipoSolicitud.Items
                    If x.Selected Then
                        tipoSolicitud = tipoSolicitud & CType(x.Value, Integer) & ","
                    End If
                Next
                If Len(tipoSolicitud) > 0 Then
                    tipoSolicitud = Left(tipoSolicitud, Len(tipoSolicitud) - 1)
                End If
                part.mSolicitudes = tipoSolicitud
                Dim favoritas As String = ""
                For Each node As TreeNode In _tvSolicitudesFavoritas.Nodes
                    For Each nodehijo As TreeNode In node.ChildNodes
                        If nodehijo.Checked Then
                            favoritas = favoritas & nodehijo.Value & ","
                        End If
                    Next
                Next
                If Len(favoritas) > 0 Then
                    favoritas = Left(favoritas, Len(favoritas) - 1)
                End If
                part.mSolicitudesFavoritas = favoritas
                part.CargarSolicitudes()

                Return True
            Else
                Return False
            End If
        End Function
        ''' <summary>
        ''' Obtiene los valores de propiedad de un control WebPart correspondientes a su control EditorPart asociado.
        ''' </summary>
        Public Overrides Sub SyncChanges()
            Dim part As FSPMWebPartAltaSolicitudes = CType(WebPartToEdit, FSPMWebPartAltaSolicitudes)
            txtTitulo.Text = part.Title
            txtAncho.Text = part.Width.Value.ToString()

            If part.mSolicitudesFavoritas = "Todas" AndAlso part.mSolicitudes = "Todas" Then
                For Each x As ListItem In _chkTipoSolicitud.Items
                    x.Selected = True
                Next

                For Each node As TreeNode In _tvSolicitudesFavoritas.Nodes
                    For Each nodehijo As TreeNode In node.ChildNodes
                        nodehijo.Checked = True
                    Next
                Next
            Else
                If part.mSolicitudes <> Nothing Then
                    For Each x As ListItem In chkTipoSolicitud.Items
                        x.Selected = IIf(InStr(part.mSolicitudes, CType(x.Value, String), ) > 0, True, False)
                    Next
                End If

                If part.mSolicitudesFavoritas <> Nothing Then
                    For Each node As TreeNode In _tvSolicitudesFavoritas.Nodes
                        For Each nodehijo As TreeNode In node.ChildNodes
                            nodehijo.Checked = IIf(InStr(part.mSolicitudesFavoritas, nodehijo.Value, ) > 0, True, False)
                        Next
                    Next
                End If
            End If
        End Sub

        Private ReadOnly Property txtTitulo() As TextBox
            Get
                EnsureChildControls()
                Return _txtTitulo
            End Get
        End Property
        Private ReadOnly Property txtAncho() As TextBox
            Get
                EnsureChildControls()
                Return _txtAncho
            End Get
        End Property
        Private ReadOnly Property chkTipoSolicitud() As CheckBoxList
            Get
                EnsureChildControls()
                Return _chkTipoSolicitud
            End Get
        End Property
        Private ReadOnly Property tvSolicitudesFavoritas() As TreeView
            Get
                EnsureChildControls()
                Return _tvSolicitudesFavoritas
            End Get
        End Property
        Private _tipoSolicitud As TiposDeDatos.TipoDeSolicitud
        Public Property TipoSolicitud() As TiposDeDatos.TipoDeSolicitud
            Get
                Return _tipoSolicitud
            End Get
            Set(ByVal value As TiposDeDatos.TipoDeSolicitud)
                _tipoSolicitud = value
            End Set
        End Property
    End Class
    Private mTipoSolicitud As TipoDeSolicitud
    <Personalizable(), WebBrowsable(), DefaultValue(TipoDeSolicitud.Otros)>
    Public Property TipoSolicitud() As TipoDeSolicitud
        Get
            Return mTipoSolicitud
        End Get
        Set(ByVal value As TipoDeSolicitud)
            mTipoSolicitud = value
        End Set
    End Property
    Private mSolicitudes As String
    <Personalizable(), WebBrowsable(), DefaultValue("Todas")>
    Public Property Solicitudes() As String
        Get
            Return mSolicitudes
        End Get
        Set(ByVal value As String)
            mSolicitudes = value
        End Set
    End Property
    <Personalizable(), WebBrowsable(), DefaultValue("Todas")>
    Public Property SolicitudesFavoritas() As String
        Get
            Return mSolicitudesFavoritas
        End Get
        Set(ByVal value As String)
            mSolicitudesFavoritas = value
        End Set
    End Property
    ''' <summary>
    ''' Devuelve el idioma de la página que alberga el control.
    ''' </summary>
    ''' <returns>El código de idioma.</returns>
    Private Function Idioma() As String
        If Not DesignMode Then
            If Me.Page Is Nothing AndAlso TypeOf Me.Page Is FSNServer.IFSNPage Then
                Return CType(Me.Page, FSNServer.IFSNPage).Idioma
            Else
                For Each x As String In HttpContext.Current.Session.Keys
                    If TypeOf HttpContext.Current.Session(x) Is FSNServer.User Then
                        Return CType(HttpContext.Current.Session(x), FSNServer.User).Idioma
                    End If
                Next
                Return String.Empty
            End If
        Else
            Return String.Empty
        End If
    End Function
    Public ReadOnly Property Textos(ByVal Index As Short) As String
        Get
            If _Textos Is Nothing Or Idioma() <> _sIdioma Then
                If Not DesignMode Then
                    Dim FSPMDict As New FSNServer.Dictionary
                    _sIdioma = Idioma()
                    If HttpContext.Current.Cache("Textos_" & _sIdioma & "_" & FSNLibrary.TiposDeDatos.ModulosIdiomas.Webparts) Is Nothing Then
                        FSPMDict.LoadData(FSNLibrary.TiposDeDatos.ModulosIdiomas.Webparts, _sIdioma)
                        HttpContext.Current.Cache.Insert("Textos_" & _sIdioma & "_" & FSNLibrary.TiposDeDatos.ModulosIdiomas.Webparts, FSPMDict.Data, Nothing, Caching.Cache.NoAbsoluteExpiration,
                                                         New TimeSpan(0, ConfigurationManager.AppSettings("TiempoExpiracionCacheMin"), 0))
                    End If
                    _Textos = HttpContext.Current.Cache("Textos_" & _sIdioma & "_" & FSNLibrary.TiposDeDatos.ModulosIdiomas.Webparts)
                    Return _Textos.Tables(0).Rows(Index).Item(1)
                Else
                    Return String.Empty
                End If
            Else
                Return _Textos.Tables(0).Rows(Index).Item(1)
            End If
        End Get
    End Property
    Public Overrides Property Title() As String
        Get
            If String.IsNullOrEmpty(MyBase.Title) Then
                If String.IsNullOrEmpty(sTitulo) Then
                    If Page Is Nothing And String.IsNullOrEmpty(Textos(91)) Then
                        MyBase.Title = If(TipoSolicitud = TipoDeSolicitud.Otros, Textos(91), Textos(257))
                        sTitulo = MyBase.Title
                    Else
                        sTitulo = If(TipoSolicitud = TipoDeSolicitud.Otros, Textos(91), Textos(257))
                    End If
                End If
                Return sTitulo
            Else
                sTitulo = MyBase.Title
                Return MyBase.Title
            End If
        End Get
        Set(ByVal value As String)
            MyBase.Title = value
        End Set
    End Property
    ''' <summary>
    ''' Carga los datos por defecto para el control
    ''' </summary>
    Public Sub New()
        Width = Unit.Pixel(300)
        mSolicitudes = "Todas"
        mSolicitudesFavoritas = "Todas"
    End Sub
    ''' <summary>
    ''' Lo llama el marco de trabajo de las páginas ASP.NET para indicar a los controles de servidor que utilizan la implementación basada en la composición que creen los controles secundarios que contengan como forma de preparar la devolución o representación de los datos.
    ''' </summary>
    Protected Overrides Sub CreateChildControls()
        Controls.Clear()
        Dim img As HtmlImage = New HtmlImage()
        img.Src = Page.ClientScript.GetWebResourceUrl(GetType(FSPMWebPartAltaSolicitudes), "Fullstep.PMWebControls.trans.gif")
        img.Width = 300
        img.Height = 1
        Controls.Add(img)
        Dim tabla As HtmlTable = New HtmlTable()
        tabla.Width = "100%"
        tabla.Height = "55px"
        Dim fila As HtmlTableRow = New HtmlTableRow()
        fila.Height = "40px"
        Dim celda As HtmlTableCell = New HtmlTableCell()
        celda.Width = "90%"
        lblTitulo = New Label()
        lblTitulo.CssClass = "Rotulo"
        celda.Controls.Add(lblTitulo)
        celda.Controls.Add(New LiteralControl("<br/>"))
        fila.Cells.Add(celda)
        celda = New HtmlTableCell
        celda.Align = "right"
        celda.VAlign = "bottom"
        img.Width = 125
        celda.Controls.Add(img)
        celda.Style.Add("background-image", Me.Page.ClientScript.GetWebResourceUrl(GetType(FSPMWebPartAltaSolicitudes), "Fullstep.PMWebControls.SolicitudesPMSmall.jpg"))
        celda.Style.Add("background-position", "top center")
        celda.Style.Add("background-repeat", "no-repeat")
        lnkVerTodos = New FSNWebControls.FSNHyperlink()
        celda.Controls.Add(lnkVerTodos)
        fila.Cells.Add(celda)
        tabla.Rows.Add(fila)

        fila = New HtmlTableRow
        celda = New HtmlTableCell
        celda.ColSpan = 2
        celda.Controls.Add(New HtmlGenericControl("hr"))
        fila.Cells.Add(celda)
        tabla.Rows.Add(fila)

        fila = New HtmlTableRow
        celda = New HtmlTableCell
        celda.ColSpan = 2
        lstSolicitudes = New DataList()
        lstSolicitudes.Width = Unit.Percentage(100)
        lstSolicitudes.ItemTemplate = New DataListTemplate()
        celda.Controls.Add(lstSolicitudes)
        fila.Cells.Add(celda)
        tabla.Rows.Add(fila)

        lblError = New Label
        lblError.CssClass = "Rotulo"
        lblError.Visible = False
        celda.Controls.Add(lblError)
        fila.Cells.Add(celda)
        tabla.Rows.Add(fila)

        Controls.Add(tabla)

        lblTitulo.Text = If(TipoSolicitud = TipoDeSolicitud.Otros, Textos(91), Textos(257))
        lnkVerTodos.Text = Textos(87)

        If Not Page.IsPostBack Then _
            CargarSolicitudes()
    End Sub
    ''' <summary>
    ''' Refresca los datos del webpart
    ''' </summary>
    Public Sub Actualizar()
        CreateChildControls()
        CargarSolicitudes()
    End Sub
    ''' <summary>
    ''' Envía el contenido del control de servidor a un objeto HtmlTextWriter que se proporciona y almacena la información de seguimiento sobre el control si dicho seguimiento está habilitado.
    ''' </summary>
    ''' <param name="writer">Objeto HTmlTextWriter que recibe el contenido del control.</param>
    Public Overrides Sub RenderControl(ByVal writer As System.Web.UI.HtmlTextWriter)
        MyBase.RenderControl(writer)
    End Sub
    ''' <summary>
    ''' Carga los pedidos en el DataList del control.
    ''' </summary>
    ''' <remarks>Llamadas desde: FSEPWebPartpedidos_PreRender</remarks>
    Private Sub CargarSolicitudes()
        If Not Page Is Nothing AndAlso TypeOf Page Is FSNServer.IFSNPage Then
            Dim lista As New List(Of mOrden)
            Dim pag As FSNServer.IFSNPage = CType(Page, FSNServer.IFSNPage)
            Dim FSWSServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim cSolicitudes As FSNServer.Solicitudes
            Dim iNumeroSolicitudes As Integer = 0
            Dim iNumeroSolicitudesTotales As Integer

            Try
                cSolicitudes = FSWSServer.Get_Object(GetType(FSNServer.Solicitudes))
                cSolicitudes.LoadAltaSolicitudes(pag.Usuario.Cod, pag.Idioma, mSolicitudes, mSolicitudesFavoritas, TipoSolicitud)
                If cSolicitudes.Data.Tables.Count > 0 Then
                    For Each fila As DataRow In cSolicitudes.Data.Tables(0).Rows
                        Dim mo As New mOrden()
                        mo.Id = fila.Item("ID")
                        mo.Solicitud = fila.Item("DEN")
                        mo.Favorito = fila.Item("FAVORITO")
                        mo.FavoritoId = fila.Item("FAV_ID")
                        mo.Tipo = fila.Item("TIPO")
                        If Not IsDBNull(fila.Item("FEC_ULTIMAEMISION")) Then
                            mo.FechaUltimaEmision = "(" & Textos(92) & ": " & FSNLibrary.FormatDate(fila.Item("FEC_ULTIMAEMISION"), pag.Usuario.DateFormat) & ")"
                        End If
                        lista.Add(mo)
                        'para saber el nº de solicitudes marcadas (no contamos las favoritas)
                        If fila.Item("FAVORITO") = 0 Then
                            iNumeroSolicitudes = iNumeroSolicitudes + 1
                        End If
                    Next
                    iNumeroSolicitudesTotales = cSolicitudes.Data.Tables(1).Rows.Item(0).Item(0)
                End If

                lstSolicitudes.DataSource = lista
                lstSolicitudes.DataBind()
            Catch ex As Exception
                lblError.Text = Textos(239)
                lblError.Visible = True
            End Try

            'si el nº de solicitudes que puede dar de alta es mayor que las que ve en el webpart, ve el botón "Ver Todas"
            If iNumeroSolicitudes < iNumeroSolicitudesTotales Then
                lnkVerTodos.Visible = True
                lnkVerTodos.NavigateUrl = ConfigurationManager.AppSettings("rutaPM2008") & "Tareas/AltaSolicitudes.aspx" & If(TipoSolicitud = TipoDeSolicitud.Otros, "", "?TipoSolicitud=" & TipoSolicitud)
                lnkVerTodos.Attributes.Add("onclick", "TempCargando();")
            Else
                lnkVerTodos.Visible = False
            End If
        End If
    End Sub
    ''' <summary>
    ''' Devuelve una colección de controles EditorPart personalizados que pueden utilizarse para editar un control WebPart cuando se encuentra en modo de edición.
    ''' </summary>
    ''' <returns>Una colección EditorPartCollection que contiene controles EditorPart personalizados asociados a un control WebPart.</returns>
    Public Overrides Function CreateEditorParts() As System.Web.UI.WebControls.WebParts.EditorPartCollection
        Dim editorArray As New ArrayList()
        Dim edPart As New ControlEditor()
        edPart.ID = ID & "_editor1"
        edPart.TipoSolicitud = TipoSolicitud
        editorArray.Add(edPart)
        Dim editorParts As New EditorPartCollection(editorArray)
        Return editorParts
    End Function
    Public Overrides ReadOnly Property WebBrowsableObject() As Object
        Get
            Return Me
        End Get
    End Property
    ''' <summary>
    ''' Registra el control como un control cuyo estado se debe conservar.
    ''' </summary>
    ''' <param name="e">Objeto EventArgs que contiene los datos del evento.</param>
    Protected Overrides Sub OnInit(ByVal e As System.EventArgs)
        MyBase.OnInit(e)
        Page.RegisterRequiresControlState(Me)
    End Sub
    ''' <summary>
    ''' Guarda los cambios de estado del control.
    ''' </summary>
    ''' <returns>Devuelve el estado actual del control de servidor.</returns>
    Protected Overrides Function SaveControlState() As Object
        Dim obj As Object = MyBase.SaveControlState()
        If obj IsNot Nothing Then
            obj = New Pair(obj, _Textos)
        Else
            obj = _Textos
        End If
        obj = New Pair(obj, _sIdioma)
        Return obj
    End Function
    ''' <summary>
    ''' Restaura información de estado del control.
    ''' </summary>
    ''' <param name="state">Object que representa el estado del control que se va a restaurar.</param>
    Protected Overrides Sub LoadControlState(ByVal state As Object)
        If (state IsNot Nothing) Then
            Dim p As Pair = TryCast(state, Pair)
            _sIdioma = p.Second
            state = p.First
            p = TryCast(state, Pair)
            If p IsNot Nothing Then
                MyBase.LoadControlState(p.First)
                _Textos = CType(p.Second, DataSet)
            Else
                If (TypeOf (state) Is String()) Then
                    _Textos = CType(state, DataSet)
                Else
                    MyBase.LoadControlState(state)
                End If
            End If
        End If
    End Sub
End Class