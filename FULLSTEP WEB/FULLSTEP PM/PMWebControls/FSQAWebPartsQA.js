﻿//Funciones javascript para que funcionen en el webPart

CreateXmlHttp();

/*''' <summary>
''' Crear el objeto para llamar con ajax a ComprobarVersionQa y EliminarVersionProveedor
''' </summary>
''' <remarks>Llamada desde: javascript ; Tiempo máximo: 0</remarks>*/
function CreateXmlHttp() {
    // Probamos con IE
    try {
        // Funcionará para JavaScript 5.0
        xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
    }
    catch (e) {
        try {
            xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        catch (oc) {
            xmlHttp = null;
        }
    }
    // Si no se trataba de un IE, probamos con esto
    if (!xmlHttp && typeof XMLHttpRequest != "undefined") {
        xmlHttp = new XMLHttpRequest();
    }
    return xmlHttp;
}

/*
''' <summary>
''' Llama a una página que compruebe si la noconformidad esta o no en proceso. Si esta en proceso muestra el PanelInfo,
''' si no esta en proceso redirigir a la pantalla del detalle de la noconformidad
''' </summary>
''' <param name="nombre">nombre de la lista</param>
''' <param name="listaCodProves">Codigo proveedor</param> 
''' <param name="listaDenProves">Denominacion proveedor</param> 
''' <param name="sMensaje">Mensaje a mostrar</param> 
''' <remarks>Llamada desde: FSQAWebPartPanelCalidad.vb   FSQAWebPartCertificados.vb;Tiempo máximo=0seg.</remarks>
*/
function ComprobarInstanciaEnProcesoWebPart(IdInstancia, URL, rutaFS, TipoInstancia) {
    CreateXmlHttp();
    if (xmlHttp) {        
        var params = "Instancia=" + IdInstancia;
        xmlHttp.open("POST", rutaFS + "_common/ComprobarEnProcesoQA.aspx?Instancia=" + IdInstancia , false);
        xmlHttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        xmlHttp.send(params);

        var retorno;
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {            
		    retorno = xmlHttp.responseText;
		    switch (parseInt(retorno)) {
		        case 0:
		            TempCargando();

		            if ((URL.search("DetalleNoConformidad.aspx") != -1) || (URL.search("DetalleCertificado.aspx") != -1))
		            {
		                var FechaLocal;
		                FechaLocal = new Date();
		                var otz = FechaLocal.getTimezoneOffset();
		                
		                URL = URL + '%26Otz%3d' + otz.toString()
                    }

		            window.location.href = URL;
		            return false;
		            break;
		        case 1:
		            FSNMostrarPanel('ctl00_ctl00_CPH1_CPH4_FSNPanelEnProceso_pnlInfo_AnimationExtender', event, 'ctl00_ctl00_CPH1_CPH4_FSNPanelEnProceso_lblInfo_DynamicPopulateExtender', TipoInstancia); 
		            return false;
		            break;
		        default:		           
		            return false;
		    }
		}
    }
}

/*
''' <summary>
''' Elimina el proveedor seleccionado de la lista de proveedores
''' </summary>
''' <param name="nombre">nombre de la lista</param>
''' <param name="listaCodProves">Codigo proveedor</param> 
''' <param name="listaDenProves">Denominacion proveedor</param> 
''' <param name="sMensaje">Mensaje a mostrar</param> 
''' <remarks>Llamada desde: FSQAWebPartPanelCalidad.vb   FSQAWebPartCertificados.vb;Tiempo máximo=0seg.</remarks>
*/
function eliminarElementoLista(nombre, listaCodProves, listaDenProves, sMensaje) {
    var oLista = document.getElementById(nombre);
    bEncontrado = false;
    if (oLista) {
        for (i = oLista.options.length - 1; i >= 0; i--) {
            if (oLista.options[i].selected == true) {
                codProves = document.getElementById(listaCodProves);
                denProves = document.getElementById(listaDenProves);
                if (codProves)
                    codProves.value = codProves.value.replace(oLista.options[i].value, '')
                if (denProves)
                    denProves.value = denProves.value.replace(oLista.options[i].text, '')

                oLista.options[i] = null;
                bEncontrado = true;
            }


        }
    }
    if (!bEncontrado)
        alert(sMensaje);
}