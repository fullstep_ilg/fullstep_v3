﻿Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.HtmlControls
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.ComponentModel
Imports System.Configuration

Public Class FSQAWebPartNoConformidadesPend
    Inherits WebPart

    Private lblNumTotal As Label
    Private lblTitulo As Label
    Private lnkVerTodos As FSNWebControls.FSNHyperlink
    Private sTitulo As String
    Private mNumLineas As Integer
    Private lstSolicitudes As DataList
    Private Shared Dateformat As System.Globalization.DateTimeFormatInfo
    Private Shared _Textos(23) As String
    Private _sIdioma As String

    Private lblError As Label



    Private Class DataListTemplate
        Implements ITemplate

        ''' <summary>
        ''' Define el objeto Control al que pertenecen los controles secundarios y las plantillas. Estos controles secundarios están a su vez definidos en una plantilla en línea.
        ''' </summary>
        ''' <param name="container">Objeto Control que contiene las instancias de los controles de la plantilla en línea.</param>
        Private Sub InstantiateIn(ByVal container As Control) _
           Implements ITemplate.InstantiateIn
            Dim hr = New HtmlGenericControl("hr")
            container.Controls.Add(hr)
            Dim tabla As New HtmlTable()
            tabla.Width = "100%"

            Dim fila As New HtmlTableRow()
            Dim celda As New HtmlTableCell()
            celda.ColSpan = 2
            celda.Style.Add(HtmlTextWriterStyle.FontWeight, "bold")

            fila = New HtmlTableRow
            Dim lab As New Label()
            celda.Attributes.Add("class", "ListItemLink")
            AddHandler lab.DataBinding, AddressOf Titulo_DataBinding
            celda.Controls.Add(lab)
            fila.Cells.Add(celda)
            celda = New HtmlTableCell()
            celda.Align = "right"
            celda.VAlign = "bottom"
            celda.Style.Add(HtmlTextWriterStyle.FontWeight, "bold")
            fila.Cells.Add(celda)
            tabla.Rows.Add(fila)

            fila = New HtmlTableRow()
            celda = New HtmlTableCell()
            Dim fsnlinkinfo As New FSNWebControls.FSNLinkInfo
            fsnlinkinfo.PanelInfo = "FSNPanelDatosProveedorQA"
            fsnlinkinfo.CssClass = "Rotulo"
            AddHandler fsnlinkinfo.DataBinding, AddressOf Proveedor_DataBinding
            celda.Controls.Add(fsnlinkinfo)
            fila.Cells.Add(celda)
            tabla.Rows.Add(fila)

            fila = New HtmlTableRow()
            celda = New HtmlTableCell()
            lab = New Label()
            AddHandler lab.DataBinding, AddressOf FechaResolucion_DataBinding
            celda.Controls.Add(lab)
            fila.Cells.Add(celda)
            tabla.Rows.Add(fila)

            fila = New HtmlTableRow()
            celda = New HtmlTableCell()
            lab = New Label()
            AddHandler lab.DataBinding, AddressOf FechaActualizacion_DataBinding
            celda.Controls.Add(lab)
            fila.Cells.Add(celda)
            tabla.Rows.Add(fila)


            fila = New HtmlTableRow()
            celda = New HtmlTableCell()
            fsnlinkinfo = New FSNWebControls.FSNLinkInfo
            fsnlinkinfo.PanelInfo = "FSNPanelDatosPersonaQA"
            fsnlinkinfo.CssClass = "Rotulo"
            AddHandler fsnlinkinfo.DataBinding, AddressOf Peticionario_DataBinding
            celda.Controls.Add(fsnlinkinfo)
            fila.Cells.Add(celda)
            tabla.Rows.Add(fila)

            container.Controls.Add(tabla)

        End Sub

        ''' <summary>
        ''' Se produce al enlazar la etiqueta de la plantilla en que se muestra la Instancia y el nº noConformidad con el origen de datos.
        ''' </summary>
        ''' <remarks>Llamada desde:=InstantiateIn;TiempoEjecucion=0seg</remarks>
        Private Sub Titulo_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim lab As Label = CType(sender, Label)
            Dim container As DataListItem = CType(lab.NamingContainer, DataListItem)
            lab.Text = "Id." & DataBinder.Eval(container.DataItem, "Instancia") & " NC-" & DataBinder.Eval(container.DataItem, "TIPONOCONFORMIDAD")

            lab.CssClass = "EtiquetaMediano"

            Dim iInstancia As Long
            iInstancia = DataBinder.Eval(container.DataItem, "Instancia")

            Dim URL As String
            URL = ConfigurationManager.AppSettings("rutaQA") & "frames.aspx?pagina=" & HttpContext.Current.Server.UrlEncode("noconformidad/detalleNoConformidad.aspx?NoConformidad=" & DataBinder.Eval(container.DataItem, "NOCONFORMIDAD"))
            lab.Attributes.Add("onclick", "return ComprobarInstanciaEnProcesoWebPart(" & iInstancia & ",'" & URL & "','" & ConfigurationManager.AppSettings("rutaFS") & "'," & FSNLibrary.TiposDeDatos.TipoDeSolicitud.NoConformidad & ");")
        End Sub


        ''' <summary>
        ''' Se produce al enlazar la etiqueta de la plantilla en que se muestra el proveedor con el origen de datos.
        ''' </summary>
        ''' <remarks>Llamada desde:=InstantiateIn;TiempoEjecucion=0seg</remarks>
        Private Sub Proveedor_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim fsn As FSNWebControls.FSNLinkInfo = CType(sender, FSNWebControls.FSNLinkInfo)
            Dim container As DataListItem = CType(fsn.NamingContainer, DataListItem)
            fsn.Text = DataBinder.Eval(container.DataItem, "CodProveedor") & "-" & DataBinder.Eval(container.DataItem, "Proveedor")
            fsn.ContextKey = DataBinder.Eval(container.DataItem, "CodProveedor")

        End Sub

        ''' <summary>
        ''' Se produce al enlazar la etiqueta de la plantilla en que se muestra el fecha de resolucion con el origen de datos.
        ''' </summary>
        ''' <remarks>Llamada desde:=InstantiateIn;TiempoEjecucion=0seg</remarks>
        Private Sub FechaResolucion_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim lab As Label = CType(sender, Label)
            Dim container As DataListItem = CType(lab.NamingContainer, DataListItem)

            If DataBinder.Eval(container.DataItem, "FechaResolucion") <> "#12:00:00 AM#" Then
                lab.Text = "<b>" & _Textos(18) & ": </b>" & FormatDate(DataBinder.Eval(container.DataItem, "FechaResolucion"), Dateformat)
            End If

        End Sub

        ''' <summary>
        ''' Se produce al enlazar la etiqueta de la plantilla en que se muestra la fecha de actualizacion con el origen de datos.
        ''' </summary>
        ''' <remarks>Llamada desde:=InstantiateIn;TiempoEjecucion=0seg</remarks>
        Private Sub FechaActualizacion_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim lab As Label = CType(sender, Label)
            Dim container As DataListItem = CType(lab.NamingContainer, DataListItem)

            If DataBinder.Eval(container.DataItem, "FechaUltAct") <> "#12:00:00 AM#" Then
                lab.Text = "<b>" & _Textos(21) & ": </b>" & FormatDate(DataBinder.Eval(container.DataItem, "FechaUltAct"), Dateformat)
            End If

        End Sub

        ''' <summary>
        ''' Se produce al enlazar la etiqueta de la plantilla en que se muestra el Peticionario, solo si es distinto al usuario de entrada.
        ''' </summary>
        ''' <remarks>Llamada desde:=InstantiateIn;TiempoEjecucion=0seg</remarks>
        Private Sub Peticionario_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim fsn As FSNWebControls.FSNLinkInfo = CType(sender, FSNWebControls.FSNLinkInfo)
            Dim container As DataListItem = CType(fsn.NamingContainer, DataListItem)
            Dim sPeticionario As String = DataBinder.Eval(container.DataItem, "Peticionario")

            If sPeticionario <> "" Then ' "" = -> El peticionario es el mismo al de la entrada y no tiene que mostrarlo
                'lc.Text = "<b>Peticionario: </b>" & sPeticionario
                fsn.Text = "<b>" & _Textos(17) & ": </b>" & sPeticionario
                fsn.ContextKey = DataBinder.Eval(container.DataItem, "CodPeticionario")

            End If

        End Sub


        ''' <summary>
        ''' Devuelve un string con la fecha pasada en dDate formateada según oDateFormat, por defecto aplica formato d:01/01/1900
        ''' </summary>
        ''' <param name="dDate">fecha a formatear</param>
        ''' <param name="oDateFormat">IFormatProvider</param>
        ''' <param name="sFormat">Format: d :08/17/2000; D :Thursday, August 17, 2000; f :Thursday, August 17, 2000 16:32; F :Thursday, August 17, 2000 16:32:32; g :08/17/2000 16:32;G :08/17/2000 16:32:32...
        '''</param>
        ''' <returns>String con la fecha</returns>
        ''' <remarks>Llamada desde=:FechaResolucion_DataBinding;Tiempo ejecucion = 0</remarks>
        Public Function FormatDate(ByVal dDate As Date, ByVal oDateFormat As System.Globalization.DateTimeFormatInfo, Optional ByVal sFormat As String = "d") As String
            Return dDate.ToString(sFormat, oDateFormat)
        End Function
    End Class

    <Serializable()> _
    Public Class mOrden
        Private mlInstancia As Long
        Private mlNoConformidad As Long
        Private msTipoNoConformidad As String
        Private msCodProveedor As String
        Private msProveedor As String
        Private mdFechaResolucion As Date
        Private mdFechaUltAct As Date
        Private msCodPeticionario As String
        Private msPeticionario As String

        Public Property Instancia() As Long
            Get
                Return mlInstancia
            End Get
            Set(ByVal value As Long)
                mlInstancia = value
            End Set
        End Property
        Public Property NoConformidad() As Long
            Get
                Return mlNoConformidad
            End Get
            Set(ByVal value As Long)
                mlNoConformidad = value
            End Set
        End Property

        Public Property TipoNoConformidad() As String
            Get
                Return msTipoNoConformidad
            End Get
            Set(ByVal value As String)
                msTipoNoConformidad = value
            End Set
        End Property

        Public Property CodProveedor() As String
            Get
                Return msCodProveedor
            End Get
            Set(ByVal value As String)
                msCodProveedor = value
            End Set
        End Property

        Public Property Proveedor() As String
            Get
                Return msProveedor
            End Get
            Set(ByVal value As String)
                msProveedor = value
            End Set
        End Property

        Public Property CodPeticionario() As String
            Get
                Return msCodPeticionario
            End Get
            Set(ByVal value As String)
                msCodPeticionario = value
            End Set
        End Property

        Public Property Peticionario() As String
            Get
                Return msPeticionario
            End Get
            Set(ByVal value As String)
                msPeticionario = value
            End Set
        End Property

        Public Property FechaResolucion() As Date
            Get
                Return mdFechaResolucion
            End Get
            Set(ByVal value As Date)
                mdFechaResolucion = value
            End Set
        End Property

        Public Property FechaUltAct() As Date
            Get
                Return mdFechaUltAct
            End Get
            Set(ByVal value As Date)
                mdFechaUltAct = value
            End Set
        End Property



    End Class


    Private Class ControlEditor
        Inherits EditorPart

        Private _txtTitulo As TextBox
        Private _txtAncho As TextBox
        Private _reqAncho As RequiredFieldValidator
        Private _ranAncho As RangeValidator
        Private _wneNumNoConformidades As Infragistics.Web.UI.EditorControls.WebNumericEditor
        Private _reqNumNoConformidades As RequiredFieldValidator
        Private _lstTipoSolicitud As ListBox

        ''' <summary>
        ''' Lo llama el marco de trabajo de las páginas ASP.NET para indicar a los controles de servidor que utilizan la implementación basada en la composición que creen los controles secundarios que contengan como forma de preparar la devolución o representación de los datos.
        ''' </summary>
        Protected Overrides Sub CreateChildControls()
            Dim part As FSQAWebPartNoConformidadesPend = CType(WebPartToEdit, FSQAWebPartNoConformidadesPend)
            Controls.Clear()
            _txtTitulo = New TextBox()
            _txtTitulo.Width = Unit.Pixel(250)
            _txtTitulo.Text = part.Title
            Controls.Add(_txtTitulo)
            _txtAncho = New TextBox()
            _txtAncho.MaxLength = 3
            _txtAncho.Width = Unit.Pixel(30)
            _txtAncho.Text = part.Width.Value.ToString()
            _txtAncho.ID = Me.ID & "_txtAncho"
            Controls.Add(_txtAncho)
            _reqAncho = New RequiredFieldValidator()
            _reqAncho.ID = _txtAncho.ID & "_Req"
            _reqAncho.ErrorMessage = "*"
            _reqAncho.ControlToValidate = _txtAncho.ID
            Controls.Add(_reqAncho)
            _ranAncho = New RangeValidator()
            _ranAncho.ID = _txtAncho.ID & "_Ran"
            _ranAncho.Type = ValidationDataType.Integer
            _ranAncho.MaximumValue = 500
            _ranAncho.MinimumValue = 200
            _ranAncho.ErrorMessage = "(200-500)"
            _ranAncho.ControlToValidate = _txtAncho.ID
            Controls.Add(_ranAncho)

            'nº de no Conformidades
            _wneNumNoConformidades = New Infragistics.Web.UI.EditorControls.WebNumericEditor
            _wneNumNoConformidades.ID = Me.ID & "_wneNumNoConformidades"
            _wneNumNoConformidades.Width = Unit.Pixel(30)
            _wneNumNoConformidades.MaxLength = 2
            _wneNumNoConformidades.DataMode = Infragistics.Web.UI.EditorControls.NumericDataMode.Int
            Controls.Add(_wneNumNoConformidades)
            _reqNumNoConformidades = New RequiredFieldValidator()
            _reqNumNoConformidades.ID = _reqNumNoConformidades.ID & "_Req"
            _reqNumNoConformidades.ErrorMessage = "*"
            _reqNumNoConformidades.ControlToValidate = _wneNumNoConformidades.ID
            Controls.Add(_reqNumNoConformidades)

        End Sub


        ''' <summary>
        ''' Presenta el contenido del control en el sistema de escritura especificado. Este método lo usan principalmente los programadores de controles. 
        ''' </summary>
        ''' <param name="writer">HtmlTextWriter que representa la secuencia de salida para presentar contenido HTML en el cliente.</param>
        Protected Overrides Sub RenderContents(ByVal writer As System.Web.UI.HtmlTextWriter)
            Dim part As FSQAWebPartNoConformidadesPend = CType(WebPartToEdit, FSQAWebPartNoConformidadesPend)
            writer.Write("<b>" & part.Textos(13) & ":</b> ")
            _txtTitulo.RenderControl(writer)
            writer.WriteBreak()
            writer.Write("<b>" & part.Textos(15) & ":</b> ")
            _txtAncho.RenderControl(writer)
            _reqAncho.RenderControl(writer)
            _ranAncho.RenderControl(writer)

            writer.WriteBreak()
            writer.Write("<b>" & part.Textos(22) & ":</b> ")
            _wneNumNoConformidades.RenderControl(writer)
            _reqNumNoConformidades.RenderControl(writer)
            writer.WriteBreak()
        End Sub

        ''' <summary>
        ''' Guarda los valores de un control EditorPart en las propiedades correspondientes del control WebPart asociado.
        ''' </summary>
        ''' <returns>Es true si la acción de guardar los valores del control EditorPart en el control WebPart se realiza correctamente; en caso contrario (si se produce un error), es false.</returns>
        Public Overrides Function ApplyChanges() As Boolean
            _reqAncho.Validate()
            _ranAncho.Validate()
            _reqNumNoConformidades.Validate()
            If _reqAncho.IsValid() And _ranAncho.IsValid() And _reqNumNoConformidades.IsValid() Then
                Dim part As FSQAWebPartNoConformidadesPend = CType(WebPartToEdit, FSQAWebPartNoConformidadesPend)
                part.Title = _txtTitulo.Text
                part.Width = Unit.Pixel(CType(_txtAncho.Text, Integer))
                part.mNumLineas = _wneNumNoConformidades.Text

                part.CargarNoConformidadesPendientes()
                Return True
            Else
                Return False
            End If
        End Function
        ''' <summary>
        ''' Obtiene los valores de propiedad de un control WebPart correspondientes a su control EditorPart asociado.
        ''' </summary>
        Public Overrides Sub SyncChanges()
            Dim part As FSQAWebPartNoConformidadesPend = CType(WebPartToEdit, FSQAWebPartNoConformidadesPend)
            txtTitulo.Text = part.Title
            txtAncho.Text = part.Width.Value.ToString()
            wneNumNoConformidades.Text = part.NumLineas
        End Sub


        <Personalizable(), WebBrowsable(), DefaultValue("No Conformidades pendientes de revisar cierre")> _
        Private ReadOnly Property txtTitulo() As TextBox
            Get
                EnsureChildControls()
                Return _txtTitulo
            End Get
        End Property

        Private ReadOnly Property txtAncho() As TextBox
            Get
                EnsureChildControls()
                Return _txtAncho
            End Get
        End Property

        Private ReadOnly Property wneNumNoConformidades() As Infragistics.Web.UI.EditorControls.WebNumericEditor
            Get
                EnsureChildControls()
                Return _wneNumNoConformidades
            End Get
        End Property


        Private ReadOnly Property lstTipoSolicitud() As ListBox
            Get
                EnsureChildControls()
                Return _lstTipoSolicitud
            End Get
        End Property
    End Class

    ''' <summary>
    ''' Devuelve el idioma de la página que alberga el control.
    ''' </summary>
    ''' <returns>El código de idioma.</returns>
    Private Function Idioma() As String
        If Not DesignMode Then
            If Me.Page Is Nothing AndAlso TypeOf Me.Page Is FSNServer.IFSNPage Then
                Return CType(Me.Page, FSNServer.IFSNPage).Idioma
            Else
                For Each x As String In HttpContext.Current.Session.Keys
                    If TypeOf HttpContext.Current.Session(x) Is FSNServer.User Then
                        Return CType(HttpContext.Current.Session(x), FSNServer.User).Idioma
                    End If
                Next
                Return String.Empty
            End If
        Else
            Return String.Empty
        End If
    End Function

    Public ReadOnly Property Textos(ByVal Index As Short) As String
        Get
            If String.IsNullOrEmpty(_Textos(Index)) Or Idioma() <> _sIdioma Then
                If Not DesignMode Then
                    Dim FSNDict As FSNServer.Dictionary = New FSNServer.Dictionary()
                    _sIdioma = Idioma()
                    FSNDict.LoadData(FSNLibrary.TiposDeDatos.ModulosIdiomas.Webparts, _sIdioma)
                    For i As Short = 0 To 2
                        _Textos(i) = FSNDict.Data.Tables(0).Rows(i + 5).Item(1)
                    Next
                    For i As Short = 3 To 16
                        _Textos(i) = FSNDict.Data.Tables(0).Rows(i + 6).Item(1)
                    Next
                    _Textos(17) = FSNDict.Data.Tables(0).Rows(55).Item(1)  'Peticionario
                    _Textos(18) = FSNDict.Data.Tables(0).Rows(142).Item(1) 'Fecha de resolución
                    _Textos(19) = FSNDict.Data.Tables(0).Rows(143).Item(1) '¡Tiene #ELEM# no conformidades pendientes de revisar cierre!
                    _Textos(20) = FSNDict.Data.Tables(0).Rows(144).Item(1) 'No Conformidades pendientes de revisar cierre
                    _Textos(21) = FSNDict.Data.Tables(0).Rows(136).Item(1) 'Fecha de actualización
                    _Textos(22) = FSNDict.Data.Tables(0).Rows(151).Item(1) 'Nº de NoConformidades a mostrar

                    _Textos(23) = FSNDict.Data.Tables(0).Rows(239).Item(1)
                    Return _Textos(Index)
                Else
                    'Si falla la carga de textos devolvemos la cadena vacia para que la carga de la página siga su curso
                    Return String.Empty
                End If
            Else
                Return _Textos(Index)
            End If
        End Get
    End Property

    'Titulo pagina principal
    Public Overrides Property Title() As String
        Get
            If String.IsNullOrEmpty(MyBase.Title) Then
                If String.IsNullOrEmpty(sTitulo) Then
                    If Me.Page Is Nothing And String.IsNullOrEmpty(Textos(20)) Then
                        MyBase.Title = Textos(20) '"No Conformidades pendientes de revisar cierre"
                        sTitulo = MyBase.Title
                    Else
                        sTitulo = Textos(20)
                    End If
                End If
                Return sTitulo
            Else
                sTitulo = MyBase.Title
                Return MyBase.Title
            End If
        End Get
        Set(ByVal value As String)
            MyBase.Title = value
        End Set
    End Property

    <Personalizable(), WebBrowsable(), DefaultValue(5)> _
Public Property NumLineas() As Integer
        Get
            Return mNumLineas
        End Get
        Set(ByVal value As Integer)
            mNumLineas = value
        End Set
    End Property


    ''' <summary>
    ''' Carga los datos por defecto para el control
    ''' </summary>
    Public Sub New()
        Me.Width = Unit.Pixel(300)
        Me.Title = Textos(20)
        Me.NumLineas = 5
    End Sub

    ''' <summary>
    ''' Lo llama el marco de trabajo de las páginas ASP.NET para indicar a los controles de servidor que utilizan la implementación basada en la composición que creen los controles secundarios que contengan como forma de preparar la devolución o representación de los datos.
    ''' </summary>
    Protected Overrides Sub CreateChildControls()
        Controls.Clear()
        Dim img As HtmlImage = New HtmlImage()
        img.Src = Me.Page.ClientScript.GetWebResourceUrl(GetType(FSQAWebPartNoConformidadesPend), "Fullstep.PMWebControls.trans.gif")
        img.Width = 300
        img.Height = 1
        Me.Controls.Add(img)
        Dim tabla As HtmlTable = New HtmlTable()
        tabla.Width = "100%"
        tabla.Height = "50px"
        Dim fila As HtmlTableRow = New HtmlTableRow()
        fila.Height = "50px"
        Dim celda As HtmlTableCell = New HtmlTableCell()
        celda.Width = "90%"
        lblTitulo = New Label()
        lblTitulo.CssClass = "Rotulo"
        celda.Controls.Add(lblTitulo)

        celda.Controls.Add(New LiteralControl("<br/>"))
        lblNumTotal = New Label()
        lblNumTotal.CssClass = "Etiqueta"
        celda.Controls.Add(lblNumTotal)

        fila.Cells.Add(celda)
        celda = New HtmlTableCell
        celda.Align = "right"
        celda.VAlign = "bottom"
        img.Width = 125
        celda.Controls.Add(img)
        If DesignMode Then
            celda.Style.Add("background-image", "images/Img_Ico_Alerta.gif")
        Else
            If CStr(ViewState("ImagenNoConformidad")) <> String.Empty Then
                celda.Style.Add("background-image", Replace(CStr(ViewState("ImagenNoConformidad")), "~/", HttpContext.Current.Request.ApplicationPath & "/"))
            Else
                celda.Style.Add("background-image", Me.Page.ClientScript.GetWebResourceUrl(GetType(FSQAWebPartNoConfAbiertas), "Fullstep.PMWebControls.Img_Ico_Alerta.gif"))
            End If
        End If
        celda.Style.Add("background-position", "top right")
        celda.Style.Add("background-repeat", "no-repeat")
        fila.Cells.Add(celda)
        tabla.Rows.Add(fila)
        fila = New HtmlTableRow
        celda = New HtmlTableCell
        celda.ColSpan = 2


        fila = New HtmlTableRow
        celda = New HtmlTableCell
        celda.Align = "left"
        celda.ColSpan = 2
        lnkVerTodos = New FSNWebControls.FSNHyperlink()
        celda.Controls.Add(lnkVerTodos)
        fila.Cells.Add(celda)
        tabla.Rows.Add(fila)

        fila = New HtmlTableRow
        celda = New HtmlTableCell
        celda.ColSpan = 2
        lstSolicitudes = New DataList()
        lstSolicitudes.Width = Unit.Percentage(100)
        lstSolicitudes.ItemTemplate = New DataListTemplate()
        celda.Controls.Add(lstSolicitudes)
        fila.Cells.Add(celda)
        tabla.Rows.Add(fila)

        lblError = New System.Web.UI.WebControls.Label
        lblError.Visible = False
        celda.Controls.Add(lblError)
        fila.Cells.Add(celda)
        tabla.Rows.Add(fila)

        Me.Controls.Add(tabla)


        lnkVerTodos.Text = Textos(2)
        If Not Me.Page Is Nothing AndAlso TypeOf Me.Page Is FSNServer.IFSNPage Then
            Dim pag As FSNServer.IFSNPage = CType(Me.Page, FSNServer.IFSNPage)
            Dateformat = pag.Usuario.DateFormat
        End If

        lblTitulo.Text = Textos(20)
        If Not Page.IsPostBack Then _
            CargarNoConformidadesPendientes()
    End Sub


    ''' <summary>
    ''' Refresca los datos del webpart
    ''' </summary>
    Public Sub Actualizar()
        CreateChildControls()
        CargarNoConformidadesPendientes()
    End Sub

    ''' <summary>
    ''' Envía el contenido del control de servidor a un objeto HtmlTextWriter que se proporciona y almacena la información de seguimiento sobre el control si dicho seguimiento está habilitado.
    ''' </summary>
    ''' <param name="writer">Objeto HTmlTextWriter que recibe el contenido del control.</param>
    Public Overrides Sub RenderControl(ByVal writer As System.Web.UI.HtmlTextWriter)
        MyBase.RenderControl(writer)
    End Sub

    ''' <summary>
    ''' Carga las noConformidades pendientes de revisar en el DataList del control.
    ''' </summary>
    ''' <remarks>Llamadas desde: ApplyChanges,CreateChildControls;Tiempo ejecucion = 0,5seg.</remarks>
    Private Sub CargarNoConformidadesPendientes()
        If Not Me.Page Is Nothing AndAlso TypeOf Me.Page Is FSNServer.IFSNPage Then
            Dim lista As New List(Of mOrden)
            Dim pag As FSNServer.IFSNPage = CType(Me.Page, FSNServer.IFSNPage)
            Dim FSWSServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim NumTotal As Long = 5
            Dim sTexto As String
            Dim i As Integer
            sTexto = Textos(19) '"¡Tiene #ELEM# no conformidades pendientes de revisar cierre!"
            Dim oNoConformidades As FSNServer.NoConformidades

            Try
                oNoConformidades = FSWSServer.Get_Object(GetType(FSNServer.NoConformidades))
                oNoConformidades.LoadDataWebPartNoConformidadesPendientes(pag.Usuario.Cod, pag.Idioma)
                i = 0
                If oNoConformidades.Data.Tables.Count > 0 Then
                    NumTotal = oNoConformidades.Data.Tables(0).Rows.Count
                    For Each fila As DataRow In oNoConformidades.Data.Tables(0).Rows
                        Dim mo As New mOrden()
                        i = i + 1
                        mo.Instancia = fila.Item("ID")
                        mo.NoConformidad = fila.Item("NOCONFORMIDAD")
                        mo.TipoNoConformidad = fila.Item("TIPONOCONFORMIDAD")
                        mo.CodProveedor = fila.Item("PROVE")
                        mo.Proveedor = fila.Item("PROVEEDOR")
                        If Not IsDBNull(fila.Item("FEC_LIM_RESOL")) Then
                            mo.FechaResolucion = fila.Item("FEC_LIM_RESOL")
                        End If
                        mo.FechaUltAct = fila.Item("FEC_ACT")
                        mo.CodPeticionario = fila.Item("PET")
                        mo.Peticionario = fila.Item("PETICIONARIO")

                        lista.Add(mo)

                        If i = NumLineas Then
                            Exit For
                        End If
                    Next
                End If

                lstSolicitudes.DataSource = lista
                lstSolicitudes.DataBind()
            
                lblNumTotal.Text = Replace(sTexto, "#ELEM#", NumTotal.ToString)

            Catch ex As Exception
                lblError.Text = Textos(23)
                lblError.Visible = True

            End Try
            If (NumTotal > NumLineas) And (NumLineas > 0) Then
                If Not DesignMode() Then
                    Page.Session("FiltroNoConformidadEstado") = 5
                    lnkVerTodos.Visible = True
                    lnkVerTodos.NavigateUrl = ConfigurationManager.AppSettings("rutaQA2008") & "Noconformidades/NoConformidades.aspx?Estado=5"
                    lnkVerTodos.Attributes.Add("onclick", "TempCargando();")
                End If
            Else
                lnkVerTodos.Visible = False
            End If
        End If
    End Sub

    ''' <summary>
    ''' Devuelve una colección de controles EditorPart personalizados que pueden utilizarse para editar un control WebPart cuando se encuentra en modo de edición.
    ''' </summary>
    ''' <returns>Una colección EditorPartCollection que contiene controles EditorPart personalizados asociados a un control WebPart.</returns>
    Public Overrides Function CreateEditorParts() As System.Web.UI.WebControls.WebParts.EditorPartCollection
        Dim editorArray As New ArrayList()
        Dim edPart As New ControlEditor()
        edPart.ID = Me.ID & "_editor1"
        editorArray.Add(edPart)
        Dim editorParts As New EditorPartCollection(editorArray)
        Return editorParts
    End Function

    Public Overrides ReadOnly Property WebBrowsableObject() As Object
        Get
            Return Me
        End Get
    End Property

    ''' <summary>
    ''' Registra el control como un control cuyo estado se debe conservar.
    ''' </summary>
    ''' <param name="e">Objeto EventArgs que contiene los datos del evento.</param>
    Protected Overrides Sub OnInit(ByVal e As System.EventArgs)
        MyBase.OnInit(e)
        Page.RegisterRequiresControlState(Me)
    End Sub

    ''' <summary>
    ''' Guarda los cambios de estado del control.
    ''' </summary>
    ''' <returns>Devuelve el estado actual del control de servidor.</returns>
    Protected Overrides Function SaveControlState() As Object
        Dim obj As Object = MyBase.SaveControlState()
        If obj IsNot Nothing Then
            obj = New Pair(obj, _Textos)
        Else
            obj = _Textos
        End If
        obj = New Pair(obj, _sIdioma)
        Return obj
    End Function

    ''' <summary>
    ''' Restaura información de estado del control.
    ''' </summary>
    ''' <param name="state">Object que representa el estado del control que se va a restaurar.</param>
    Protected Overrides Sub LoadControlState(ByVal state As Object)
        If (state IsNot Nothing) Then
            Dim p As Pair = TryCast(state, Pair)
            _sIdioma = p.Second
            state = p.First
            p = TryCast(state, Pair)
            If p IsNot Nothing Then
                MyBase.LoadControlState(p.First)
                _Textos = CType(p.Second, String())
            Else
                If (TypeOf (state) Is String()) Then
                    _Textos = CType(state, String())
                Else
                    MyBase.LoadControlState(state)
                End If
            End If
        End If
    End Sub

    Private Sub FSQAWebPartNoConformidadesPend_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.DesignMode Then
            ScriptManager.RegisterClientScriptResource(Me.Page, GetType(QAWebPartClientScript), "Fullstep.PMWebControls.FSQAWebPartsQA.js")
        End If
    End Sub
End Class
