﻿Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.HtmlControls
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.ComponentModel
Imports System.Configuration

Public Class FSPMWebPartFiltrosDisponibles
    Inherits WebPart

    Private lblTitulo As Label
    Private lblError As Label
    Private lstFiltros As DataList
    Private mFiltrosSeleccionados As String
    Private sTitulo As String
    Private _Textos As DataSet
    Private _sIdioma As String

    Private Class DataListTemplate
        Implements ITemplate

        ''' <summary>
        ''' Define el objeto Control al que pertenecen los controles secundarios y las plantillas. Estos controles secundarios están a su vez definidos en una plantilla en línea.
        ''' </summary>
        ''' <param name="container">Objeto Control que contiene las instancias de los controles de la plantilla en línea.</param>
        Private Sub InstantiateIn(ByVal container As Control) _
           Implements ITemplate.InstantiateIn
            Dim tabla As New HtmlTable()
            tabla.Width = "100%"
            tabla.Attributes.Add("class", "ListItemLink")
            AddHandler tabla.DataBinding, AddressOf IrAVisorFiltro_DataBinding
            Dim fila As New HtmlTableRow()
            Dim celda As New HtmlTableCell()
            fila.Cells.Add(celda)
            celda.Style.Add(HtmlTextWriterStyle.FontWeight, "bold")
            Dim lc As New Literal()
            AddHandler lc.DataBinding, AddressOf NombreFiltro_DataBinding
            celda.Controls.Add(lc)
            fila.Cells.Add(celda)
            tabla.Rows.Add(fila)
            container.Controls.Add(tabla)
        End Sub

        ''' <summary>
        ''' Se produce al enlazar la etiqueta de la plantilla en que se muestra el nombre del filtro con el origen de datos.
        ''' </summary>
        Private Sub NombreFiltro_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim lc As Literal = CType(sender, Literal)
            Dim container As DataListItem = CType(lc.NamingContainer, DataListItem)
            lc.Text = DataBinder.Eval(container.DataItem, "NombreEscenario")
        End Sub

        ''' <summary>
        ''' Se produce al enlazar la tabla de la plantilla que muestra el visor de solicitudes de ese filtro con el origen de datos.
        ''' </summary>
        Private Sub IrAVisorFiltro_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim tab As HtmlTable = CType(sender, HtmlTable)
            Dim container As DataListItem = CType(tab.NamingContainer, DataListItem)
            tab.Attributes.Add("onclick", "TempCargando(); window.location.href='" & ConfigurationManager.AppSettings("rutaPM2008") & "Tareas/VisorSolicitudes.aspx?IdEscenario=" & DataBinder.Eval(container.DataItem, "IdEscenario") & "';")
        End Sub
    End Class

    <Serializable()> _
    Public Class mOrden
        Private msNombreFiltro As String
        Private miFiltroUsuarioId As Integer
        Private msNombreTabla As String

        Public Property FiltroUsuarioId() As Integer
            Get
                Return miFiltroUsuarioId
            End Get
            Set(ByVal value As Integer)
                miFiltroUsuarioId = value
            End Set
        End Property

        Public Property NombreFiltro() As String
            Get
                Return msNombreFiltro
            End Get
            Set(ByVal value As String)
                msNombreFiltro = value
            End Set
        End Property

        Public Property NombreTabla() As String
            Get
                Return msNombreTabla
            End Get
            Set(ByVal value As String)
                msNombreTabla = value
            End Set
        End Property

        Private _idEscenario As Integer
        Public Property IdEscenario() As Integer
            Get
                Return _idEscenario
            End Get
            Set(ByVal value As Integer)
                _idEscenario = value
            End Set
        End Property

        Private _nombreEscenario As String
        Public Property NombreEscenario() As String
            Get
                Return _nombreEscenario
            End Get
            Set(ByVal value As String)
                _nombreEscenario = value
            End Set
        End Property

    End Class

    Private Class ControlEditor
        Inherits EditorPart

        Private _txtTitulo As TextBox
        Private _txtAncho As TextBox
        Private _reqAncho As RequiredFieldValidator
        Private _ranAncho As RangeValidator
        Private _chkFiltrosDisponibles As CheckBoxList
        Private _reqFiltrosDisponibles As RequiredFieldValidatorForCheckBoxLists

        ''' <summary>
        ''' Lo llama el marco de trabajo de las páginas ASP.NET para indicar a los controles de servidor que utilizan la implementación basada en la composición que creen los controles secundarios que contengan como forma de preparar la devolución o representación de los datos.
        ''' </summary>
        Protected Overrides Sub CreateChildControls()
            Dim part As FSPMWebPartFiltrosDisponibles = CType(WebPartToEdit, FSPMWebPartFiltrosDisponibles)
            Controls.Clear()
            _txtTitulo = New TextBox()
            _txtTitulo.Width = Unit.Pixel(250)
            _txtTitulo.Text = part.Title
            Controls.Add(_txtTitulo)
            _txtAncho = New TextBox()
            _txtAncho.MaxLength = 3
            _txtAncho.Width = Unit.Pixel(30)
            _txtAncho.Text = part.Width.Value.ToString()
            _txtAncho.ID = Me.ID & "_txtAncho"
            Controls.Add(_txtAncho)
            _reqAncho = New RequiredFieldValidator()
            _reqAncho.ID = _txtAncho.ID & "_Req"
            _reqAncho.ErrorMessage = "*"
            _reqAncho.ControlToValidate = _txtAncho.ID
            Controls.Add(_reqAncho)
            _ranAncho = New RangeValidator()
            _ranAncho.ID = _txtAncho.ID & "_Ran"
            _ranAncho.Type = ValidationDataType.Integer
            _ranAncho.MaximumValue = 500
            _ranAncho.MinimumValue = 200
            _ranAncho.ErrorMessage = "(200-500)"
            _ranAncho.ControlToValidate = _txtAncho.ID
            Controls.Add(_ranAncho)

            Dim pag As FSNServer.IFSNPage = CType(Me.Page, FSNServer.IFSNPage)
            Dim cFiltros As FSNServer.Filtros
            Dim FSWSServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            cFiltros = FSWSServer.Get_Object(GetType(FSNServer.Filtros))

            _chkFiltrosDisponibles = New CheckBoxList
            _chkFiltrosDisponibles.Items.Clear()
            Try
                _chkFiltrosDisponibles.DataTextField = "NOMBRE"
                _chkFiltrosDisponibles.DataValueField = "ID"
                _chkFiltrosDisponibles.DataSource = cFiltros.LoadDisponiblesYaConfigurados(pag.Usuario.CodPersona)
                _chkFiltrosDisponibles.DataBind()
            Catch ex As Exception

            End Try
            _chkFiltrosDisponibles.ID = Me.ID & "chkFiltrosDisponibles"
            Controls.Add(_chkFiltrosDisponibles)

            _reqFiltrosDisponibles = New RequiredFieldValidatorForCheckBoxLists
            _reqFiltrosDisponibles.ID = _chkFiltrosDisponibles.ID & "_Req"
            _reqFiltrosDisponibles.ErrorMessage = " * " & part.Textos(86)
            _reqFiltrosDisponibles.ControlToValidate = Me.ID & "chkFiltrosDisponibles"
            Controls.Add(_reqFiltrosDisponibles)
        End Sub

        ''' <summary>
        ''' Presenta el contenido del control en el sistema de escritura especificado. Este método lo usan principalmente los programadores de controles. 
        ''' </summary>
        ''' <param name="writer">HtmlTextWriter que representa la secuencia de salida para presentar contenido HTML en el cliente.</param>
        Protected Overrides Sub RenderContents(ByVal writer As System.Web.UI.HtmlTextWriter)
            Dim part As FSPMWebPartFiltrosDisponibles = CType(WebPartToEdit, FSPMWebPartFiltrosDisponibles)
            writer.Write("<b>" & part.Textos(19) & ":</b> ")
            _txtTitulo.RenderControl(writer)
            writer.WriteBreak()
            writer.Write("<b>" & part.Textos(21) & ":</b> ")
            _txtAncho.RenderControl(writer)
            _reqAncho.RenderControl(writer)
            _ranAncho.RenderControl(writer)
            writer.WriteBreak()
            writer.WriteBreak()
            writer.Write("<div style='border: 1px solid #CCCCCC;'><b>&nbsp;&nbsp;&nbsp;" & part.Textos(63) & "</b>")
            writer.WriteBreak()
            _reqFiltrosDisponibles.RenderControl(writer)
            _chkFiltrosDisponibles.RenderControl(writer)
            writer.WriteBreak()
            writer.Write("</div>")
            writer.WriteBreak()
            writer.WriteBreak()
        End Sub

        ''' <summary>
        ''' Guarda los valores de un control EditorPart en las propiedades correspondientes del control WebPart asociado.
        ''' </summary>
        ''' <returns>Es true si la acción de guardar los valores del control EditorPart en el control WebPart se realiza correctamente; en caso contrario (si se produce un error), es false.</returns>
        Public Overrides Function ApplyChanges() As Boolean
            _reqAncho.Validate()
            _ranAncho.Validate()
            _reqFiltrosDisponibles.Validate()
            If _reqAncho.IsValid() And _ranAncho.IsValid() And _reqFiltrosDisponibles.IsValid() Then
                Dim part As FSPMWebPartFiltrosDisponibles = CType(WebPartToEdit, FSPMWebPartFiltrosDisponibles)
                part.Title = _txtTitulo.Text
                part.Width = Unit.Pixel(CType(_txtAncho.Text, Integer))
                Dim filtros As String = ""
                For Each x As ListItem In _chkFiltrosDisponibles.Items
                    If x.Selected Then
                        filtros = filtros & CType(x.Value, Integer) & ","
                    End If
                Next
                If Len(filtros) > 0 Then
                    filtros = Left(filtros, Len(filtros) - 1)
                End If
                part.mFiltrosSeleccionados = filtros
                part.CargarFiltros()
                Return True
            Else
                Return False
            End If
        End Function
        ''' <summary>
        ''' Obtiene los valores de propiedad de un control WebPart correspondientes a su control EditorPart asociado.
        ''' </summary>
        Public Overrides Sub SyncChanges()
            Dim part As FSPMWebPartFiltrosDisponibles = CType(WebPartToEdit, FSPMWebPartFiltrosDisponibles)
            txtTitulo.Text = part.Title
            txtAncho.Text = part.Width.Value.ToString()

            If part.mFiltrosSeleccionados <> Nothing Then
                For Each x As ListItem In chkFiltrosDisponibles.Items
                    x.Selected = IIf(InStr(part.mFiltrosSeleccionados, CType(x.Value, String), ) > 0, True, False)
                Next
            Else
                For Each x As ListItem In chkFiltrosDisponibles.Items
                    x.Selected = True
                Next
            End If
        End Sub

        Private ReadOnly Property txtTitulo() As TextBox
            Get
                EnsureChildControls()
                Return _txtTitulo
            End Get
        End Property

        Private ReadOnly Property txtAncho() As TextBox
            Get
                EnsureChildControls()
                Return _txtAncho
            End Get
        End Property

        Private ReadOnly Property chkFiltrosDisponibles() As CheckBoxList
            Get
                EnsureChildControls()
                Return _chkFiltrosDisponibles
            End Get
        End Property
    End Class

    <Personalizable(), WebBrowsable(), DefaultValue("")> _
    Public Property FiltrosSeleccionados() As String
        Get
            Return mFiltrosSeleccionados
        End Get
        Set(ByVal value As String)
            mFiltrosSeleccionados = value
        End Set
    End Property

    ''' <summary>
    ''' Devuelve el idioma de la página que alberga el control.
    ''' </summary>
    ''' <returns>El código de idioma.</returns>
    Private Function Idioma() As String
        If Not DesignMode Then
            If Me.Page Is Nothing AndAlso TypeOf Me.Page Is FSNServer.IFSNPage Then
                Return CType(Me.Page, FSNServer.IFSNPage).Idioma
            Else
                For Each x As String In HttpContext.Current.Session.Keys
                    If TypeOf HttpContext.Current.Session(x) Is FSNServer.User Then
                        Return CType(HttpContext.Current.Session(x), FSNServer.User).Idioma
                    End If
                Next
                Return String.Empty
            End If
        Else
            Return String.Empty
        End If
    End Function

    Public ReadOnly Property Textos(ByVal Index As Short) As String
        Get
            If _Textos Is Nothing Or Idioma() <> _sIdioma Then
                If Not DesignMode Then
                    Dim FSPMDict As New FSNServer.Dictionary
                    _sIdioma = Idioma()
                    If HttpContext.Current.Cache("Textos_" & _sIdioma & "_" & FSNLibrary.TiposDeDatos.ModulosIdiomas.Webparts) Is Nothing Then
                        FSPMDict.LoadData(FSNLibrary.TiposDeDatos.ModulosIdiomas.Webparts, _sIdioma)
                        HttpContext.Current.Cache.Insert("Textos_" & _sIdioma & "_" & FSNLibrary.TiposDeDatos.ModulosIdiomas.Webparts, FSPMDict.Data, Nothing, Caching.Cache.NoAbsoluteExpiration,
                                                         New TimeSpan(0, ConfigurationManager.AppSettings("TiempoExpiracionCacheMin"), 0))
                    End If
                    _Textos = HttpContext.Current.Cache("Textos_" & _sIdioma & "_" & FSNLibrary.TiposDeDatos.ModulosIdiomas.Webparts)
                    Return _Textos.Tables(0).Rows(Index).Item(1)
                Else
                    Return String.Empty
                End If
            Else
                Return _Textos.Tables(0).Rows(Index).Item(1)
            End If
        End Get
    End Property

    Public Overrides Property Title() As String
        Get
            If String.IsNullOrEmpty(MyBase.Title) Then
                If String.IsNullOrEmpty(sTitulo) Then
                    If Me.Page Is Nothing And String.IsNullOrEmpty(Textos(63)) Then
                        MyBase.Title = Textos(63)
                        sTitulo = MyBase.Title
                    Else
                        sTitulo = Textos(63)
                    End If
                End If
                Return sTitulo
            Else
                sTitulo = MyBase.Title
                Return MyBase.Title
            End If
        End Get
        Set(ByVal value As String)
            MyBase.Title = value
        End Set
    End Property

    ''' <summary>
    ''' Carga los datos por defecto para el control
    ''' </summary>
    Public Sub New()
        Me.Width = Unit.Pixel(300)
    End Sub

    ''' <summary>
    ''' Lo llama el marco de trabajo de las páginas ASP.NET para indicar a los controles de servidor que utilizan la implementación basada en la composición que creen los controles secundarios que contengan como forma de preparar la devolución o representación de los datos.
    ''' </summary>
    Protected Overrides Sub CreateChildControls()
        Controls.Clear()
        Dim img As HtmlImage = New HtmlImage()
        img.Src = Me.Page.ClientScript.GetWebResourceUrl(GetType(FSPMWebPartFiltrosDisponibles), "Fullstep.PMWebControls.trans.gif")
        img.Width = 300
        img.Height = 1
        Me.Controls.Add(img)
        Dim tabla As HtmlTable = New HtmlTable()
        tabla.Width = "100%"
        tabla.Height = "55px"
        Dim fila As HtmlTableRow = New HtmlTableRow()
        fila.Height = "40px"
        Dim celda As HtmlTableCell = New HtmlTableCell()
        celda.Width = "90%"
        lblTitulo = New Label()
        lblTitulo.CssClass = "Rotulo"
        celda.Controls.Add(lblTitulo)
        celda.Controls.Add(New LiteralControl("<br/>"))
        fila.Cells.Add(celda)
        celda = New HtmlTableCell
        celda.Align = "right"
        celda.VAlign = "bottom"
        img.Width = 125
        celda.Controls.Add(img)
        celda.Style.Add("background-image", Me.Page.ClientScript.GetWebResourceUrl(GetType(FSPMWebPartFiltrosDisponibles), "Fullstep.PMWebControls.FiltrosDisponibles.gif"))
        celda.Style.Add("background-position", "top center")
        celda.Style.Add("background-repeat", "no-repeat")
        fila.Cells.Add(celda)
        tabla.Rows.Add(fila)

        fila = New HtmlTableRow
        celda = New HtmlTableCell
        celda.ColSpan = 2
        celda.Controls.Add(New HtmlGenericControl("hr"))
        fila.Cells.Add(celda)
        tabla.Rows.Add(fila)

        fila = New HtmlTableRow
        celda = New HtmlTableCell
        celda.ColSpan = 2
        lstFiltros = New DataList()
        lstFiltros.Width = Unit.Percentage(100)
        lstFiltros.ItemTemplate = New DataListTemplate()
        celda.Controls.Add(lstFiltros)
        fila.Cells.Add(celda)
        tabla.Rows.Add(fila)
        Me.Controls.Add(tabla)

        lblError = New Label
        lblError.CssClass = "Rotulo"
        lblError.Visible = False
        celda.Controls.Add(lblError)
        fila.Cells.Add(celda)
        tabla.Rows.Add(fila)

        lblTitulo.Text = Textos(63)

        If Not Page.IsPostBack Then _
        CargarFiltros()
    End Sub

    ''' <summary>
    ''' Refresca los datos del webpart
    ''' </summary>
    Public Sub Actualizar()
        CreateChildControls()
        CargarFiltros()
    End Sub

    ''' <summary>
    ''' Envía el contenido del control de servidor a un objeto HtmlTextWriter que se proporciona y almacena la información de seguimiento sobre el control si dicho seguimiento está habilitado.
    ''' </summary>
    ''' <param name="writer">Objeto HTmlTextWriter que recibe el contenido del control.</param>
    Public Overrides Sub RenderControl(ByVal writer As System.Web.UI.HtmlTextWriter)
        MyBase.RenderControl(writer)
    End Sub

    ''' <summary>
    ''' Carga los pedidos en el DataList del control.
    ''' </summary>
    ''' <remarks>Llamadas desde: FSPMWebPartFiltrosDisponibles_PreRender</remarks>
    Private Sub CargarFiltros()
        If Not Me.Page Is Nothing AndAlso TypeOf Me.Page Is FSNServer.IFSNPage Then
            Dim lista As New List(Of mOrden)
            Dim pag As FSNServer.IFSNPage = CType(Me.Page, FSNServer.IFSNPage)
            Dim FSWSServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim ds As DataSet
            Dim cFiltros As FSNServer.Filtros

            Try
                cFiltros = FSWSServer.Get_Object(GetType(FSNServer.Filtros))
                ds = cFiltros.LoadDisponiblesSeleccionados(pag.Usuario.Cod, mFiltrosSeleccionados)
                If ds.Tables(0).Rows.Count > 0 Then
                    For Each fila As DataRow In ds.Tables(0).Rows
                        Dim mo As New mOrden()
                        mo.IdEscenario = fila.Item("ID")
                        mo.NombreEscenario = fila.Item("NOMBRE")
                        lista.Add(mo)
                    Next
                End If

                lstFiltros.DataSource = lista
                lstFiltros.DataBind()
            Catch ex As Exception
                lblError.Text = Textos(239)
                lblError.Visible = True
            End Try
        End If
    End Sub

    ''' <summary>
    ''' Devuelve una colección de controles EditorPart personalizados que pueden utilizarse para editar un control WebPart cuando se encuentra en modo de edición.
    ''' </summary>
    ''' <returns>Una colección EditorPartCollection que contiene controles EditorPart personalizados asociados a un control WebPart.</returns>
    Public Overrides Function CreateEditorParts() As System.Web.UI.WebControls.WebParts.EditorPartCollection
        Dim editorArray As New ArrayList()
        Dim edPart As New ControlEditor()
        edPart.ID = Me.ID & "_editor1"
        editorArray.Add(edPart)
        Dim editorParts As New EditorPartCollection(editorArray)
        Return editorParts
    End Function

    Public Overrides ReadOnly Property WebBrowsableObject() As Object
        Get
            Return Me
        End Get
    End Property

    ''' <summary>
    ''' Registra el control como un control cuyo estado se debe conservar.
    ''' </summary>
    ''' <param name="e">Objeto EventArgs que contiene los datos del evento.</param>
    Protected Overrides Sub OnInit(ByVal e As System.EventArgs)
        MyBase.OnInit(e)
        Page.RegisterRequiresControlState(Me)
    End Sub

    ''' <summary>
    ''' Guarda los cambios de estado del control.
    ''' </summary>
    ''' <returns>Devuelve el estado actual del control de servidor.</returns>
    Protected Overrides Function SaveControlState() As Object
        Dim obj As Object = MyBase.SaveControlState()
        If obj IsNot Nothing Then
            obj = New Pair(obj, _Textos)
        Else
            obj = _Textos
        End If
        obj = New Pair(obj, _sIdioma)
        Return obj
    End Function

    ''' <summary>
    ''' Restaura información de estado del control.
    ''' </summary>
    ''' <param name="state">Object que representa el estado del control que se va a restaurar.</param>
    Protected Overrides Sub LoadControlState(ByVal state As Object)
        If (state IsNot Nothing) Then
            Dim p As Pair = TryCast(state, Pair)
            _sIdioma = p.Second
            state = p.First
            p = TryCast(state, Pair)
            If p IsNot Nothing Then
                MyBase.LoadControlState(p.First)
                _Textos = CType(p.Second, DataSet)
            Else
                If (TypeOf (state) Is String()) Then
                    _Textos = CType(state, DataSet)
                Else
                    MyBase.LoadControlState(state)
                End If
            End If
        End If
    End Sub
End Class
