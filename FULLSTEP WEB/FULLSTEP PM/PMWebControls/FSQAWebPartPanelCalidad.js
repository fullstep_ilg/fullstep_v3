﻿//Funciones javascript para que funcionen en el webPart

/*
''' <summary>
''' Visualiza los elementos del grafico de evolucion o los oculta si se trata un grafico de tipo de barras
''' </summary>
''' <remarks>Tiempo máximo=0seg.</remarks>
*/
function tipoGrafico() {
    var div = document.getElementById("div_GraficoEvolucion");
    if (div) {
        if (div.style.visibility == 'visible')
            div.style.visibility = 'hidden'
        else
            div.style.visibility = 'visible'
    }

}
/*
''' <summary>
''' Si se ha elegido un operador en el webPart de panel de calidad muestra la caja de texto Hasta
''' </summary>
''' <param name="lista">nombre de la lista de operador</param>
''' <param name="IdCampoHasta">ID de la caja de texto Hasta</param> 
''' <remarks>Tiempo máximo=0seg.</remarks>
*/
function cambioOperador(lista, IdCampoHasta) {
    if (lista) {
        campoHasta = document.getElementById(IdCampoHasta);
        if (lista.value != '') {
            if (campoHasta) {
                campoHasta.value = ""
                campoHasta.disabled = true;
            }
        } else
            if (campoHasta)
            campoHasta.disabled = false;
    }
}

/*
''' <summary>
''' Eliminar el material, el codigo de articulo y denominacion del webpart
''' </summary>
''' <param name="campo">caja de texto del material</param>
''' <param name="idcampoOculto">hidden del material oculto</param> 
''' <param name="idCampoCodArt">id articulo</param> 
''' <param name="idCampoDenArt">id caja texto denominacion</param> 
''' <remarks>Tiempo máximo=0seg.</remarks>
*/
function eliminarMaterial(event,campo, idcampoOculto, idCampoCodArt, idCampoDenArt) {
    if ((event.keyCode >= 35) && (event.keyCode <= 40)) {
        return true;
    } else {
        if (event.keyCode == 8) {
            campo.value = "";
            campoOculto = document.getElementById(idcampoOculto);
            campoCodArt = document.getElementById(idCampoCodArt);
            campoDenArt = document.getElementById(idCampoDenArt);
            if (campoOculto)
                campoOculto.value = ""
            if (campoCodArt)
                campoCodArt.value = ""
            if (campoDenArt)
                campoDenArt.value = ""
        }
        return false;
    }


}

/*
''' <summary>
''' Eliminar el codigo de articulo y denominacion del webpart
''' </summary>
''' <param name="idCampoCodArt">id articulo</param> 
''' <param name="idCampoDenArt">id caja texto denominacion</param> 
''' <remarks>Tiempo máximo=0seg.</remarks>
*/

function eliminarArticulo(event,idCampoCodArt, idCampoDenArt) {
    if ((event.keyCode >= 35) && (event.keyCode <= 40)) {
        return true;
    } else {
        if (event.keyCode == 8) {
            campoCodArt = document.getElementById(idCampoCodArt);
            campoDenArt = document.getElementById(idCampoDenArt);
            if (campoCodArt)
                campoCodArt.value = "";
            if (campoDenArt)
                campoDenArt.value = "";
        }
        return false;
    }
}
/*
''' <summary>
''' Controla a traves del hidden que solo haya una unica variable de calidad seleccionada
''' </summary>
''' <param name="event">Evento pulsado</param>
''' <param name="hiddenID">id del hidden que controla si hay o no algun checkbox seleccionado</param>        
''' <remarks>Llamada desde=FSQAWebPartPanelCalidad.vb; Tiempo máximo=0seg.</remarks>
*/
function VariablesCalidad_NodeChecked(event, hiddenID) {
    var o = event.srcElement ? event.srcElement : event.target;
        
    if (o.tagName == "INPUT" && o.type == "checkbox") {
        oCheck = document.getElementById(o.id)
        oHidden = document.getElementById(hiddenID) //Obtener el hidden que lleva el control de nº de check seleccionados en el tree Variables de Calidad

        if (oHidden) {
            if (oCheck.checked) {
                if (oHidden.value != "") { //Si ya hay alguna variable de calidad seleccionada no seleccionar ninguna
                    document.getElementById(o.id).cheked = false;
                    oCheck.checked = false;
                } else
                    oHidden.value = o.id;
            } //fin del if (ochecked)
            else {
                oHidden.value = "";
            }  
        }//fin oHidden
                
    }
}

/*
''' <summary>
''' Controla a traves del hidden que solo haya un unico UNQA seleccionada
''' </summary>
''' <param name="event">Evento pulsado</param>
''' <param name="hiddenID">id del hidden que controla si hay o no algun checkbox seleccionado</param>        
''' <remarks>Llamada desde=FSQAWebPartPanelCalidad.vb; Tiempo máximo=0seg.</remarks>
*/
function UNQA_NodeChecked(event, hiddenID) {
    var o = event.srcElement ? event.srcElement : event.target;
    
    if (o.tagName == "INPUT" && o.type == "checkbox") {
        oCheck = document.getElementById(o.id)
        oHidden = document.getElementById(hiddenID) //Obtener el hidden que lleva el control de nº de check seleccionados en el tree Unidades de Negocio
        
        if (oHidden) {
            if (oCheck.checked) {
                if (oHidden.value != "") { //Si ya hay algun UNQA seleccionada no seleccionar ninguna
                    document.getElementById(o.id).cheked = false;
                    oCheck.checked = false;
                } else
                    oHidden.value = o.id;
            } //fin del if (ochecked)
            else {
                oHidden.value = "";
            }
        } //fin oHidden

    }
}