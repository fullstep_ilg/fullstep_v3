Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.Unit

Imports System.ComponentModel
Imports System.ComponentModel.Design
Imports System.ComponentModel.Design.Serialization

Imports Infragistics.WebUI.Shared
Imports Fullstep.FSNLibrary
Imports AjaxControlToolkit
Imports System.Configuration
Imports System.Web.UI.HtmlControls
Imports Fullstep.FSNServer

<Designer("Fullstep.DataEntry.DataEntryDesigner"), _
DefaultProperty("Text"),
ToolboxData("<{0}:GeneralEntry runat=server></{0}:GeneralEntry>")>
Public Class GeneralEntry
    Inherits System.Web.UI.WebControls.WebControl
    Implements INamingContainer
    Implements IProvidesEmbeddableEditor

#Region " Private variables "
    Private _DenGrupo As String
    Private _DenDesgloseMat As String
    Private _text As String
    Private _toolTip As String
    Private _title As String
    Private _windowTitle As String
    Private _tipo As FSNLibrary.TiposDeDatos.TipoGeneral
    Private _tipoGS As FSNLibrary.TiposDeDatos.TipoCampoGS
    Private _grupo As Long
    Private _intro As Integer
    Private _maximo As Object
    Private _minimo As Object
    Private _maxLength As Integer
    Private _lista As DataSet
    Shared _listaTiposPedido As DataSet
    Private _readOnly As Boolean
    Private _valor As Object
    Private _idatrib As Long
    Private _valerp As Short
    Private _DependentValue As Object
    Private _oTag As String
    Private _columnKey As String
    Private _IdContenedor As String
    Private _TabContainer As String
    Private _Name As String
    Private _Idi As String
    Private _errMsg As String
    Private _errMsgFormato As String
    Private _nullMsg As String
    Private _PUCaptionBoton As String
    Private _InputStyle As System.Web.UI.WebControls.Style
    Private _ButtonStyle As System.Web.UI.WebControls.Style
    Private _LabelStyle As System.Web.UI.WebControls.Style
    Private _PopUpStyle As String
    Private _IsGridEditor As Boolean
    Private _DependentField As String
    Private _sMasterField As String
    Private _ValidationControl As String
    Private _Independiente As String
    Private _IdCampoPadre As Long
    Private _IdCamposHijos As String
    Private _InitScript As String
    Private _DropDownGridID As String
    Private _sRestric As String
    Private _Calculado As Boolean
    Private _Desglose As Boolean
    Private _FechaNoAnteriorAlSistema As Boolean
    Private _CargarProveSumiArt As Boolean
    Private _idEntryProveSumiArt As String
    Private _NumberFormat As System.Globalization.NumberFormatInfo
    Private _DateFormat As System.Globalization.DateTimeFormatInfo
    Private _Obligatorio As Boolean
    Private _BaseDesglose As Boolean
    Private _articuloCodificado As Boolean
    Private _anyadirArt As Boolean
    Private _orden As Short
    Private _articuloGenerico As Boolean
    Private _codigoArticulo As String
    Private _DenArticuloModificado As Boolean 'Variable que nos indica si la denominacion del articulo ha sido o no modificada.
    Private _nColumna As Integer
    Private _nLinea As Integer
    Private _nTipoRecepcion As Integer
    Private _idRefSol As Integer
    Private _avisoBloqueoRefSOL As Integer
    Private _InstanciaMoneda As String 'Variable que almacena la Moneda de la instancia (Para visualizar los importes de las solicitudes padre en la Moneda de la solicitud Hija)
    Private _CargarUltADJ As Boolean
    Private _idEntryPREC As String
    Private _idEntryPROV As String
    Private _idEntryART As String
    Private _idEntryUNI As String       'Variables que almacena la UNIDAD DE UN ARTICULO   
    Private _idEntryCANT As String
    'Las siguientes dos Variables sirven para cuando la UNIDAD esta oculta
    Private _sUnidadOculta As String
    Private _idEntryCC As String
    Private _bVerUON As Boolean
    Private _sPRES5 As String
    Private _nNumeroDeDecimales As Integer
    Private _DesgloseCambiaWidth As Boolean = False
    Private _bPM As Boolean
    Private _bPortal As Boolean
    Private _lTablaExterna As Long
    Private _lServicio As Long
    Private _sFormato As String
    'Variables origen= Sirven para obtener el campo del dise�o cuando se le da al boton enviar a favoritos en la etapa peticionario, cuando se ha guardado una solicitud
    Private _CampoOrigen As Long    'Campo que tiene el valor de copia_campo en form_campo
    Private _IdCampoPadreOrigen As Long
    Private _IdCamposHijosOrigen As String
    Private _sIdiomaBotonTextoMedio As String
    Private _idContrato As Long
    Private _idArchivoContrato As Long
    Private _TamanyoContrato As String
    Private Const GeneralEntryKeyCall As String = "GeneralEntryCallScript"
    Private Const IncludeScriptFormat As String = ControlChars.CrLf &
    "<script type=""{0}"" src=""{1}{2}""></script>"
    Private Const IncludeScriptKeyFormat As String = ControlChars.CrLf &
    "<script language=""{0}"">{1}</script>"
    Private Const CamposCalculadosScriptKey As String = "CamposCalculadosIncludeScript"
    Private Const ArticulosScriptKey As String = "ArticulosIncludeScript"
    Private Const CamposObligatoriosScriptKey As String = "CamposObligatoriosScriptKey "
    Private Const CamposObligatoriosDenScriptKey As String = "CamposObligatoriosDenScriptKey "
    Private Const CamposScriptKey As String = "CamposIncludeScript"
    Private Const ObligatoriosScriptKey As String = "CamposObligatoriosIncludeScript"
    'Valores defecto
    Private _PoseeDefecto As Boolean
    Private _valor_Defecto As Object
    Private _texto_Defecto As Object
    Private _Denominacion As String
    Private _MonImporteRepercutido As String
    Private _CambioImporteRepercutido As Double
    Private _MonCentral As Object
    Private _CambioCentral As Object
    Private _bSaltaCambioRechazo As Boolean = False
    Private _NCSinRevisar As String = ""
    Private _iConcepto As Integer
    Private _iAlmacenamiento As Integer
    Private _iRecepcion As Integer
    Private _recordarvalores As Boolean
    'Listas enlazadas
    Private _idCampoHijaListaEnlazada As Long 'Necesario para obtener el combo (id FORM_CAMPO//COPIA_CAMPO)
    Private _idCampoDefHijaListaEnlazada As Long 'Necesario para obtener el combo (id COPIA_CAMPO_DEF)
    Private _idGrupoHijaListaEnlazada As Long
    Private _idCampoPadreListaEnlazada As Long 'Necesario para obtener el combo (id FORM_CAMPO//COPIA_CAMPO)
    Private _idCampoDefPadreListaEnlazada As Long 'Necesario para obtener el combo (id COPIA_CAMPO_DEF)
    Private _idGrupoPadreListaEnlazada As Long
    Private _idCampoListaEnlazada As Long 'Necesario para obtener el combo (id FORM_CAMPO//COPIA_CAMPO)
    Private _ListaOpcionesCargada As Boolean
    Private _lInstancia As Long
    Private _CampoDef_CampoODesgHijo As Long
    Private _CampoDef_DesgPadre As Long
    Private _IdCamposHijosCampoDef As String
    Private _CampoOrigenVinculado As String
    Private _Movible As Boolean
    Private _DescrComboPopUp As String
    Private _bEsProveedorParticipante As Boolean
    Private _sContactos As String
    Private _TipoSolicit As Integer
    Private _idEntryUNIPedido As String
    Private _sUnidadPedidoOculta As String
    Private _MonSolicit As String
    Private _FPagoOculta As String
    Private _UsarOrgCompras As Boolean
    Private _nivelSeleccion As Integer
    Private _idDataEntryFORMMaterial As String
    Private _ActivoSM As Boolean
    Private _IdMaterialOculta As String
    Private _IdCodArticuloOculta As String
    Private _IdDenArticuloOculta As String
    Private _bSelCualquierMatPortal As Boolean
    '9060- A�o de la partida plurianual
    Private _idEntryPartidaPlurianual As String
    Private _EntryPartidaPlurianualCampo As Integer
#End Region
#Region " Properties "
    <Bindable(True), Category("Data"), DefaultValue("")>
    Property DenGrupo() As String
        Get
            If _DenGrupo IsNot Nothing Then
                Return _DenGrupo
            Else
                Return String.Empty
            End If
        End Get
        Set(ByVal Value As String)
            _DenGrupo = Value
        End Set
    End Property
    <Bindable(True), Category("Data"), DefaultValue("")>
    Property DenDesgloseMat() As String
        Get
            If _DenDesgloseMat IsNot Nothing Then
                Return _DenDesgloseMat
            Else
                Return String.Empty
            End If
        End Get
        Set(ByVal Value As String)
            _DenDesgloseMat = Value
        End Set
    End Property
    <Bindable(True), Category("Data"), DefaultValue(0)>
    Property [SelCualquierMatPortal]() As Boolean
        Get
            Return _bSelCualquierMatPortal
        End Get
        Set(ByVal Value As Boolean)
            _bSelCualquierMatPortal = Value
        End Set
    End Property
    <Bindable(True), Category("Data"), DefaultValue(0)>
    Property [FechaNoAnteriorAlSistema]() As Boolean
        Get
            Return _FechaNoAnteriorAlSistema
        End Get

        Set(ByVal Value As Boolean)
            _FechaNoAnteriorAlSistema = Value
        End Set
    End Property
    <Bindable(True), Category("Data"), DefaultValue(0)>
    Property [CargarProveSumiArt]() As Boolean
        Get
            Return _CargarProveSumiArt
        End Get

        Set(ByVal Value As Boolean)
            _CargarProveSumiArt = Value
        End Set
    End Property
    <Bindable(True), Category("Data"), DefaultValue(0)>
    Property idEntryProveSumiArt() As String
        Get
            Return _idEntryProveSumiArt
        End Get

        Set(ByVal Value As String)
            _idEntryProveSumiArt = Value
        End Set
    End Property
    <Bindable(True), Category("PopUp"), Description("El estilo a aplicar a popup."), DefaultValue("")>
    Public Overridable Property PopUpStyle() As String

        Get
            Return _PopUpStyle
        End Get

        Set(ByVal Value As String)
            _PopUpStyle = Value
        End Set

    End Property
    <Category("Style"),
       Description("El estilo a aplicar al cuadro de texto."),
       DesignerSerializationVisibility(DesignerSerializationVisibility.Content),
       NotifyParentProperty(True),
       PersistenceMode(PersistenceMode.InnerProperty)>
    Public Overridable ReadOnly Property InputStyle() As System.Web.UI.WebControls.Style
        Get
            If _InputStyle Is Nothing Then
                _InputStyle = New System.Web.UI.WebControls.Style
                If IsTrackingViewState Then
                    CType(_InputStyle, IStateManager).TrackViewState()
                End If
            End If
            Return _InputStyle
        End Get
    End Property
    <Category("Style"),
       Description("El estilo a aplicar al bot�n si es que el control implementa uno."),
       DesignerSerializationVisibility(DesignerSerializationVisibility.Content),
       NotifyParentProperty(True),
       PersistenceMode(PersistenceMode.InnerProperty)>
    Public Overridable ReadOnly Property LabelStyle() As System.Web.UI.WebControls.Style
        Get
            If _LabelStyle Is Nothing Then
                _LabelStyle = New System.Web.UI.WebControls.Style
                If IsTrackingViewState Then
                    CType(_LabelStyle, IStateManager).TrackViewState()
                End If
            End If
            Return _LabelStyle
        End Get
    End Property
    <Category("Style"),
       Description("El estilo a aplicar al bot�n si es que el control implementa uno."),
       DesignerSerializationVisibility(DesignerSerializationVisibility.Content),
       NotifyParentProperty(True),
       PersistenceMode(PersistenceMode.InnerProperty)>
    Public Overridable ReadOnly Property ButtonStyle() As System.Web.UI.WebControls.Style
        Get
            If _ButtonStyle Is Nothing Then
                _ButtonStyle = New System.Web.UI.WebControls.Style
                If IsTrackingViewState Then
                    CType(_ButtonStyle, IStateManager).TrackViewState()
                End If
            End If
            Return _ButtonStyle
        End Get
    End Property
    ''' <summary>
    ''' El nombre del input que va a contener el valor al hacer un submit.
    ''' </summary>
    ''' <returns>El nombre del input</returns>
    ''' <remarks>Llamada desde: alta/campos.ascx          alta/participantes.ascx; Tiempo maximo:0</remarks>
    <Category("Data"),
    Description("El nombre del input que va a contener el valor al hacer un submit.")>
    Public Overridable ReadOnly Property SubmitKey() As String
        Get
            If Controls.Count = 0 Then Return DBNullToSomething(_valor)

            Dim oInput As Object = Nothing
            Select Case _tipoGS
                Case TiposDeDatos.TipoCampoGS.Desglose, TiposDeDatos.TipoCampoGS.Material, TiposDeDatos.TipoCampoGS.ArchivoEspecific
                    oInput = CType(Controls(0).Controls(0).Controls(0).Controls(0), System.Web.UI.WebControls.TextBox)
                Case TiposDeDatos.TipoCampoGS.PRES1, TiposDeDatos.TipoCampoGS.Pres2, TiposDeDatos.TipoCampoGS.Pres3, TiposDeDatos.TipoCampoGS.Pres4
                    oInput = CType(Controls(0).Controls(0).Controls(0).Controls(1), System.Web.UI.HtmlControls.HtmlInputHidden)
                Case TiposDeDatos.TipoCampoGS.Proveedor, TiposDeDatos.TipoCampoGS.ProveContacto, TiposDeDatos.TipoCampoGS.Persona,
                TiposDeDatos.TipoCampoGS.ProveedorAdj, TiposDeDatos.TipoCampoGS.ImporteRepercutido, TiposDeDatos.TipoCampoGS.CentroCoste,
                TiposDeDatos.TipoCampoGS.Activo, TiposDeDatos.IdsFicticios.DesgloseVinculado, TiposDeDatos.TipoCampoGS.Factura
                    oInput = CType(Controls(0).Controls(0).Controls(0).Controls(1), System.Web.UI.HtmlControls.HtmlInputHidden)
                Case Else
                    If _intro = 0 Then
                        Select Case _tipo
                            Case TiposDeDatos.TipoGeneral.TipoTextoMedio, TiposDeDatos.TipoGeneral.TipoString, TiposDeDatos.TipoGeneral.TipoTextoCorto
                                oInput = CType(Controls(0).Controls(0).Controls(0).Controls(0), Infragistics.WebUI.WebDataInput.WebTextEdit)
                            Case TiposDeDatos.TipoGeneral.TipoNumerico
                                oInput = CType(Controls(0).Controls(0).Controls(0).Controls(0), Infragistics.WebUI.WebDataInput.WebNumericEdit)
                            Case TiposDeDatos.TipoGeneral.TipoFecha
                                oInput = CType(Controls(0).Controls(0).Controls(0).Controls(0), Infragistics.WebUI.WebDataInput.WebDateTimeEdit)
                            Case TiposDeDatos.TipoGeneral.TipoBoolean
                                oInput = CType(Controls(0).Controls(0).Controls(0).Controls(0), Infragistics.WebUI.WebDataInput.WebTextEdit)
                            Case TiposDeDatos.TipoGeneral.TipoTextoLargo
                                oInput = CType(Controls(0).Controls(0).Controls(0).Controls(0), Infragistics.WebUI.WebDataInput.WebTextEdit)
                            Case TiposDeDatos.TipoGeneral.TipoArchivo
                                oInput = CType(Controls(0).Controls(0).Controls(0).Controls(0), System.Web.UI.WebControls.TextBox)
                            Case TiposDeDatos.TipoGeneral.TipoCheckBox
                                oInput = CType(Controls(0).Controls(0).Controls(0).Controls(0), System.Web.UI.WebControls.CheckBox)
                        End Select
                    Else
                        Select Case _tipo
                            Case TiposDeDatos.TipoGeneral.TipoTextoMedio, TiposDeDatos.TipoGeneral.TipoString, TiposDeDatos.TipoGeneral.TipoTextoCorto, TiposDeDatos.TipoGeneral.TipoTextoLargo
                                oInput = CType(Controls(0).Controls(0).Controls(0).Controls(0), Infragistics.WebUI.WebDataInput.WebTextEdit)
                            Case TiposDeDatos.TipoGeneral.TipoNumerico
                                oInput = CType(Controls(0).Controls(0).Controls(0).Controls(0), Infragistics.WebUI.WebDataInput.WebNumericEdit)
                            Case TiposDeDatos.TipoGeneral.TipoFecha
                                oInput = CType(Controls(0).Controls(0).Controls(0).Controls(0), Infragistics.WebUI.WebDataInput.WebDateTimeEdit)
                        End Select

                    End If
            End Select

            Return oInput.ClientId
        End Get
    End Property
    <Bindable(True), Category("Data"), DefaultValue("")>
    Property [Text]() As String
        Get
            Return _text
        End Get
        Set(ByVal Value As String)
            _text = Value
        End Set
    End Property
    <Bindable(True), Category("Data"), DefaultValue("")>
    Overrides Property [ToolTip]() As String
        Get
            Return _toolTip
        End Get
        Set(ByVal Value As String)
            _toolTip = Value
        End Set
    End Property
    <Bindable(True), Category("Data"), DefaultValue(0)>
    Property [Obligatorio]() As Boolean
        Get
            Return _Obligatorio
        End Get
        Set(ByVal Value As Boolean)
            _Obligatorio = Value
        End Set
    End Property
    <Bindable(True), Category("Data"), DefaultValue(0)>
    Property [BaseDesglose]() As Boolean
        Get
            Return _BaseDesglose
        End Get
        Set(ByVal Value As Boolean)
            _BaseDesglose = Value
        End Set
    End Property
    <Bindable(True), Category("Data"), DefaultValue("")>
    Property [Title]() As String
        Get
            Return _title
        End Get
        Set(ByVal Value As String)
            _title = Value
        End Set
    End Property
    <Bindable(True), Category("Data"), DefaultValue("")>
    Property [WindowTitle]() As String
        Get
            Return _windowTitle
        End Get
        Set(ByVal Value As String)
            _windowTitle = Value
        End Set
    End Property
    <Bindable(True), Category("Data"), DefaultValue("")>
    Property [Calculado]() As Boolean
        Get
            Return _Calculado
        End Get
        Set(ByVal Value As Boolean)
            _Calculado = Value
        End Set
    End Property
    <Bindable(True), Category("Data"), DefaultValue("")>
    Property [PM]() As Boolean
        Get
            Return _bPM
        End Get
        Set(ByVal Value As Boolean)
            _bPM = Value
        End Set
    End Property
    ''' <summary>
    ''' En el Entry hay acceso a bbdd. Si se esta en Portal el acceso lo da PMPortalServer y en PM/Qa lo da FSNServer.
    ''' </summary>
    ''' <returns>Si nos encontramos en portal o no</returns>
    ''' <remarks>Llamada desde: GeneralEntry.vb y toda pantalla de portal q use Entry; Tiempo m�ximo:0</remarks>
    <Bindable(True), Category("Data"), DefaultValue(False)>
    Property Portal() As Boolean
        Get
            Return _bPortal
        End Get
        Set(ByVal Value As Boolean)
            _bPortal = Value
        End Set
    End Property
    <Bindable(True), Category("Data"), DefaultValue("")>
    Property [Desglose]() As Boolean
        Get
            Return _Desglose
        End Get
        Set(ByVal Value As Boolean)
            _Desglose = Value
        End Set
    End Property
    <Bindable(True), Category("Data"), DefaultValue("")>
    Property [Restric]() As String
        Get
            Return _sRestric
        End Get
        Set(ByVal Value As String)
            _sRestric = Value
        End Set
    End Property
    <Bindable(True), Category("Data"), DefaultValue("")>
    Property [Idi]() As String
        Get
            Return _Idi
        End Get
        Set(ByVal Value As String)
            _Idi = Value
        End Set
    End Property
    <Bindable(True), Category("Data"), DefaultValue("")>
    Property Valor_Defecto() As Object
        Get
            Return _valor_Defecto
        End Get
        Set(ByVal Value As Object)
            _valor_Defecto = Value
        End Set
    End Property
    <Bindable(True), Category("Data"), DefaultValue("")>
    Property Texto_Defecto() As Object
        Get
            Return _texto_Defecto
        End Get

        Set(ByVal Value As Object)
            _texto_Defecto = Value
        End Set
    End Property
    <Bindable(True), Category("Data"), DefaultValue("")>
    Property MonRepercutido() As String
        Get
            Return _MonImporteRepercutido
        End Get
        Set(ByVal Value As String)
            _MonImporteRepercutido = Value
        End Set
    End Property
    <Bindable(True), Category("Data"), DefaultValue("")>
    Property CambioRepercutido() As Double
        Get
            Return _CambioImporteRepercutido
        End Get
        Set(ByVal Value As Double)
            _CambioImporteRepercutido = Value
        End Set
    End Property
    <Bindable(True), Category("Data"), DefaultValue("")>
    Property MonCentral() As Object
        Get
            Return _MonCentral
        End Get
        Set(ByVal Value As Object)
            _MonCentral = Value
        End Set
    End Property
    <Bindable(True), Category("Data"), DefaultValue("")>
    Property CambioCentral() As Object
        Get
            Return _CambioCentral
        End Get
        Set(ByVal Value As Object)
            _CambioCentral = Value
        End Set
    End Property
    <Bindable(True), Category("Data"), DefaultValue("")>
    Property Posee_Defecto() As Boolean
        Get
            Return _PoseeDefecto
        End Get
        Set(ByVal Value As Boolean)
            _PoseeDefecto = Value
        End Set
    End Property
    <Bindable(True), Category("Data"), DefaultValue("")>
    Property [ColumnKey]() As String
        Get
            Return _columnKey
        End Get
        Set(ByVal Value As String)
            _columnKey = Value
        End Set
    End Property
    ''' <summary>
    ''' Dar acceso a la propiedad _valor
    ''' </summary>
    ''' <returns>El valor del entry</returns>
    ''' <remarks>Llamada desde: campos.ascx    desglose.ascx     jsAlta.js       fsGeneralEntry.js; Tiempo m�ximo: 0</remarks>
    <Bindable(True), Category("Data"), DefaultValue("")>
    Property [Valor]() As Object
        Get
            If Controls.Count = 0 Then Return _valor

            Dim oInput As Object = Nothing
            Select Case _tipoGS
                Case TiposDeDatos.TipoCampoGS.Desglose, TiposDeDatos.TipoCampoGS.Material, TiposDeDatos.TipoCampoGS.ArchivoEspecific
                    oInput = CType(Controls(0).Controls(0).Controls(0).Controls(0), System.Web.UI.WebControls.TextBox)
                    Return oInput.text
                Case TiposDeDatos.TipoCampoGS.PRES1, TiposDeDatos.TipoCampoGS.Pres2, TiposDeDatos.TipoCampoGS.Pres3, TiposDeDatos.TipoCampoGS.Pres4
                    oInput = CType(Controls(0).Controls(0).Controls(0).Controls(1), System.Web.UI.HtmlControls.HtmlInputHidden)
                    Return oInput.value
                Case TiposDeDatos.TipoCampoGS.Proveedor, TiposDeDatos.TipoCampoGS.ProveContacto, TiposDeDatos.TipoCampoGS.Persona,
                TiposDeDatos.TipoCampoGS.Rol, TiposDeDatos.TipoCampoGS.ProveedorAdj, TiposDeDatos.TipoCampoGS.ImporteRepercutido,
                TiposDeDatos.TipoCampoGS.CentroCoste, TiposDeDatos.TipoCampoGS.Activo, TiposDeDatos.TipoCampoGS.Factura
                    oInput = CType(Controls(0).Controls(0).Controls(0).Controls(1), System.Web.UI.HtmlControls.HtmlInputHidden)
                    Return oInput.value
                Case Else

                    If _intro = 0 Then
                        Select Case _tipo
                            Case TiposDeDatos.TipoGeneral.TipoTextoMedio, TiposDeDatos.TipoGeneral.TipoString, TiposDeDatos.TipoGeneral.TipoTextoCorto
                                oInput = CType(Controls(0).Controls(0).Controls(0).Controls(0), Infragistics.WebUI.WebDataInput.WebTextEdit)
                            Case TiposDeDatos.TipoGeneral.TipoNumerico
                                oInput = CType(Controls(0).Controls(0).Controls(0).Controls(0), Infragistics.WebUI.WebDataInput.WebNumericEdit)
                            Case TiposDeDatos.TipoGeneral.TipoFecha
                                oInput = CType(Controls(0).Controls(0).Controls(0).Controls(0), Infragistics.WebUI.WebDataInput.WebDateTimeEdit)
                            Case TiposDeDatos.TipoGeneral.TipoBoolean
                                oInput = CType(Controls(0).Controls(0).Controls(0).Controls(0), Infragistics.WebUI.WebDataInput.WebTextEdit)
                            Case TiposDeDatos.TipoGeneral.TipoTextoLargo
                                oInput = CType(Controls(0).Controls(0).Controls(0).Controls(0), Infragistics.WebUI.WebDataInput.WebTextEdit)
                            Case TiposDeDatos.TipoGeneral.TipoArchivo
                                oInput = CType(Controls(0).Controls(0).Controls(0).Controls(0), System.Web.UI.WebControls.TextBox)
                            Case TiposDeDatos.TipoGeneral.TipoCheckBox
                                oInput = CType(Controls(0).Controls(0).Controls(0).Controls(0), System.Web.UI.WebControls.CheckBox)
                        End Select
                    Else
                        Select Case _tipo
                            Case TiposDeDatos.TipoGeneral.TipoTextoMedio, TiposDeDatos.TipoGeneral.TipoString, TiposDeDatos.TipoGeneral.TipoTextoCorto, TiposDeDatos.TipoGeneral.TipoTextoLargo
                                oInput = CType(Controls(0).Controls(0).Controls(0).Controls(0), Infragistics.WebUI.WebDataInput.WebTextEdit)
                            Case TiposDeDatos.TipoGeneral.TipoNumerico
                                oInput = CType(Controls(0).Controls(0).Controls(0).Controls(0), Infragistics.WebUI.WebDataInput.WebNumericEdit)
                            Case TiposDeDatos.TipoGeneral.TipoFecha
                                oInput = CType(Controls(0).Controls(0).Controls(0).Controls(0), Infragistics.WebUI.WebDataInput.WebDateTimeEdit)
                        End Select
                    End If
            End Select

            Return oInput.value
        End Get
        Set(ByVal Value As Object)
            _valor = Value
        End Set
    End Property
    <Bindable(True), Category("Data"), DefaultValue("")>
    Property [Tag]() As String
        Get
            Return _oTag
        End Get

        Set(ByVal Value As String)
            _oTag = Value

        End Set
    End Property
    <Bindable(True), Category("Data"), DefaultValue("")>
    Property [Name]() As String
        Get

            Return _Name
        End Get

        Set(ByVal Value As String)
            _Name = Value

        End Set
    End Property
    <Bindable(True), Category("Data"), DefaultValue("")>
    Property [IdContenedor]() As Object
        Get

            Return _IdContenedor
        End Get

        Set(ByVal Value As Object)
            _IdContenedor = Value

        End Set
    End Property
    <Bindable(True), Category("Data"), DefaultValue("")>
    Property [TabContainer]() As Object
        Get

            Return _TabContainer
        End Get

        Set(ByVal Value As Object)
            _TabContainer = Value

        End Set
    End Property
    <Bindable(True), Category("PopUp"), DefaultValue("")>
    Property [ErrMsg]() As Object
        Get

            Return _errMsg
        End Get

        Set(ByVal Value As Object)
            _errMsg = Value

        End Set
    End Property
    <Bindable(True), Category("PopUp"), DefaultValue("")>
    Property [ErrMsgFormato]() As Object
        Get

            Return _errMsgFormato
        End Get

        Set(ByVal Value As Object)
            _errMsgFormato = Value

        End Set
    End Property
    <Bindable(True), Category("PopUp"), DefaultValue("")>
    Property [NullMsg]() As Object
        Get

            Return _nullMsg
        End Get

        Set(ByVal Value As Object)
            _nullMsg = Value

        End Set
    End Property
    <Bindable(True), Category("PopUp"), DefaultValue("")>
    Property [PUCaptionBoton]() As Object
        Get

            Return _PUCaptionBoton
        End Get

        Set(ByVal Value As Object)
            _PUCaptionBoton = Value

        End Set
    End Property
    <Bindable(True), Category("Behavour"), DefaultValue(1)>
    Property [NumberFormat]() As System.Globalization.NumberFormatInfo
        Get

            Return _NumberFormat
        End Get

        Set(ByVal Value As System.Globalization.NumberFormatInfo)
            _NumberFormat = Value

        End Set
    End Property
    <Bindable(True), Category("Behavour"), DefaultValue(1)>
    Property [DateFormat]() As System.Globalization.DateTimeFormatInfo
        Get

            Return _DateFormat
        End Get

        Set(ByVal Value As System.Globalization.DateTimeFormatInfo)
            _DateFormat = Value

        End Set
    End Property
    <Bindable(True), Category("Behavour"), DefaultValue(1)>
    <CLSCompliant(False)> Property [Tipo]() As FSNLibrary.TiposDeDatos.TipoGeneral
        Get
            Return _tipo
        End Get
        Set(ByVal Value As FSNLibrary.TiposDeDatos.TipoGeneral)
            _tipo = Value
        End Set
    End Property
    <Bindable(True), Category("Behavour"), DefaultValue(0)>
    Property [IdAtrib]() As Long
        Get

            Return _idatrib
        End Get
        Set(ByVal Value As Long)
            _idatrib = Value

        End Set
    End Property
    <Bindable(True), Category("Behavour"), DefaultValue(0)>
    Property [ValErp]() As Short
        Get

            Return _valerp
        End Get
        Set(ByVal Value As Short)
            _valerp = Value

        End Set
    End Property
    <Bindable(True), Category("Behavour"), DefaultValue(0)>
    Property [Grupo]() As Long
        Get

            Return _grupo
        End Get
        Set(ByVal Value As Long)
            _grupo = Value

        End Set
    End Property
    <Bindable(True), Category("Behavour"), DefaultValue(1)>
    <CLSCompliant(False)> Property [TipoGS]() As FSNLibrary.TiposDeDatos.TipoCampoGS
        Get

            Return _tipoGS
        End Get
        Set(ByVal Value As FSNLibrary.TiposDeDatos.TipoCampoGS)
            _tipoGS = Value

        End Set
    End Property
    <Bindable(True), Category("Behavour"), DefaultValue(0)>
    Property [Intro]() As Integer
        Get

            Return _intro
        End Get
        Set(ByVal Value As Integer)
            _intro = Value

        End Set
    End Property
    <Bindable(True), Category("Behavour"), DefaultValue("")>
    Property [Maximo]() As String
        Get

            Return _maximo
        End Get

        Set(ByVal Value As String)
            _maximo = Value

        End Set
    End Property
    <Bindable(True), Category("Behavour"), DefaultValue("")>
    Property [Minimo]() As String
        Get

            Return _minimo
        End Get

        Set(ByVal Value As String)
            _minimo = Value

        End Set
    End Property
    <Bindable(True), Category("Behavour"), DefaultValue("")>
    Property [MaxLength]() As Integer
        Get

            Return _maxLength
        End Get

        Set(ByVal Value As Integer)
            _maxLength = Value

        End Set
    End Property
    <Bindable(True), Category("Behavour"), DefaultValue("")>
    Property [ReadOnly]() As Boolean
        Get

            Return _readOnly
        End Get

        Set(ByVal Value As Boolean)
            _readOnly = Value

        End Set
    End Property
    <Bindable(True), Category("Data"), DefaultValue("")>
    Property [Lista]() As DataSet
        Get

            Return _lista
        End Get

        Set(ByVal Value As DataSet)
            _lista = Value

        End Set
    End Property
    <Bindable(True), Category("Data"), DefaultValue("")>
    Property [ListaTiposPedido]() As DataSet
        Get

            Return _listaTiposPedido
        End Get

        Set(ByVal Value As DataSet)
            _listaTiposPedido = Value

        End Set
    End Property
    <Bindable(True), Category("Behavour"), DefaultValue("")>
    Property [IsGridEditor]() As Boolean
        Get
            Return _IsGridEditor
        End Get
        Set(ByVal Value As Boolean)
            _IsGridEditor = Value
        End Set
    End Property
    <Bindable(True), Category("Behavour"), DefaultValue("")>
    Property [DropDownGridID]() As String
        Get
            Return _DropDownGridID
        End Get
        Set(ByVal Value As String)
            _DropDownGridID = Value
        End Set
    End Property
    <Bindable(True), Category("Data"), DefaultValue("")>
    Property [DependentValue]() As String
        Get
            Return _DependentValue
        End Get
        Set(ByVal Value As String)
            _DependentValue = Value
        End Set
    End Property
    ''' <summary>
    ''' Id del Control dependiente del control cuyas propiedades son estas.
    ''' </summary>
    <Bindable(True), Category("Data"), DefaultValue("")>
    Property [DependentField]() As String
        Get
            Return _DependentField
        End Get
        Set(ByVal Value As String)
            _DependentField = Value
        End Set
    End Property
    ''' <summary>
    ''' Id del Control del cual depende el control cuyas propiedades son estas.
    ''' </summary>
    <Bindable(True), Category("Data"), DefaultValue("")>
    Property [MasterField]() As String
        Get
            If _sMasterField IsNot Nothing Then
                Return _sMasterField
            Else
                Return String.Empty
            End If
        End Get
        Set(ByVal Value As String)
            _sMasterField = Value
        End Set
    End Property
    <Bindable(True), Category("Data"), DefaultValue("")>
    Property [ValidationControl]() As String
        Get
            Return _ValidationControl
        End Get
        Set(ByVal Value As String)
            _ValidationControl = Value
        End Set
    End Property
    <Bindable(True), Category("Data"), DefaultValue("")>
    Property [Independiente]() As String
        Get
            Return _Independiente
        End Get
        Set(ByVal Value As String)
            _Independiente = Value
        End Set
    End Property
    <Bindable(True), Category("Data"), DefaultValue("")>
    Property [IdCampoPadre]() As Long
        Get
            Return _IdCampoPadre
        End Get
        Set(ByVal Value As Long)
            _IdCampoPadre = Value
        End Set
    End Property
    <Bindable(True), Category("Data"), DefaultValue("")>
    Property [IdCampoPadreOrigen]() As Long
        Get
            Return _IdCampoPadreOrigen
        End Get
        Set(ByVal Value As Long)
            _IdCampoPadreOrigen = Value
        End Set
    End Property
    <Bindable(True), Category("Data"), DefaultValue("")>
    Property [Orden]() As Short
        Get
            Return _orden
        End Get
        Set(ByVal Value As Short)
            _orden = Value
        End Set
    End Property
    <Bindable(True), Category("Data"), DefaultValue("")>
    Property [idRefSolicitud]() As Integer
        Get
            Return _idRefSol
        End Get
        Set(ByVal Value As Integer)
            _idRefSol = Value
        End Set
    End Property
    <Bindable(True), Category("Data"), DefaultValue("")>
    Property [AvisoBloqueoRefSOL]() As Integer
        Get
            Return _avisoBloqueoRefSOL
        End Get
        Set(ByVal Value As Integer)
            _avisoBloqueoRefSOL = Value
        End Set
    End Property
    <Bindable(True), Category("Data"), DefaultValue("")>
    Property [InstanciaMoneda]() As String
        Get
            Return _InstanciaMoneda
        End Get
        Set(ByVal Value As String)
            _InstanciaMoneda = Value
        End Set
    End Property
    <Bindable(True), Category("Data"), DefaultValue(0)>
    Property [CargarUltADJ]() As Boolean
        Get

            Return _CargarUltADJ
        End Get

        Set(ByVal Value As Boolean)
            _CargarUltADJ = Value

        End Set
    End Property
    <Bindable(True), Category("Data"), DefaultValue(0)>
    Property [VerUON]() As Boolean
        Get
            Return _bVerUON
        End Get

        Set(ByVal Value As Boolean)
            _bVerUON = Value
        End Set
    End Property
    <Bindable(True), Category("Data"), DefaultValue("")>
    Property [PRES5]() As String
        Get
            Return _sPRES5
        End Get

        Set(ByVal Value As String)
            _sPRES5 = Value
        End Set
    End Property
    <Bindable(True), Category("Data"), DefaultValue("")>
    Property [Formato]() As String
        Get
            Return _sFormato
        End Get

        Set(ByVal Value As String)
            _sFormato = Value
        End Set
    End Property
    <Bindable(True), Category("Data")>
    Property [NumeroDeDecimales]() As Nullable(Of Integer)
        Get
            Return _nNumeroDeDecimales
        End Get
        Set(ByVal Value As Nullable(Of Integer))
            _nNumeroDeDecimales = Value
        End Set
    End Property
    <Bindable(True), Category("Data"), DefaultValue("")>
    Property [idEntryPREC]() As String
        Get
            Return _idEntryPREC
        End Get
        Set(ByVal Value As String)
            _idEntryPREC = Value
        End Set
    End Property
    <Bindable(True), Category("Data"), DefaultValue("")>
    Property [idEntryCC]() As String
        Get
            Return _idEntryCC
        End Get
        Set(ByVal Value As String)
            _idEntryCC = Value
        End Set
    End Property
    <Bindable(True), Category("Data"), DefaultValue("")>
    Property [idEntryPROV]() As String
        Get
            Return _idEntryPROV
        End Get
        Set(ByVal Value As String)
            _idEntryPROV = Value
        End Set
    End Property
    <Bindable(True), Category("Data"), DefaultValue("")>
    Property [idEntryART]() As String
        Get
            Return _idEntryART
        End Get
        Set(ByVal Value As String)
            _idEntryART = Value
        End Set
    End Property
    <Bindable(True), Category("Data"), DefaultValue("")>
    Property [idEntryUNI]() As String
        Get
            Return _idEntryUNI
        End Get
        Set(ByVal Value As String)
            _idEntryUNI = Value
        End Set
    End Property
    <Bindable(True), Category("Data"), DefaultValue("")>
    Property [idEntryCANT]() As String
        Get
            Return _idEntryCANT
        End Get
        Set(ByVal Value As String)
            _idEntryCANT = Value
        End Set
    End Property
    <Bindable(True), Category("Data"), DefaultValue("")>
    Property [UnidadOculta]() As String
        Get
            Return _sUnidadOculta
        End Get
        Set(ByVal Value As String)
            _sUnidadOculta = Value
        End Set
    End Property
    Private _idDataEntryDependent As String
    <Bindable(True), Category("Data"), DefaultValue("")>
    Property [idDataEntryDependent]() As String
        Get
            Return _idDataEntryDependent
        End Get
        Set(ByVal Value As String)
            _idDataEntryDependent = Value
        End Set
    End Property
    Private _DependentValueDataEntry As Object
    <Bindable(True), Category("Data"), DefaultValue("")>
    Property [DependentValueDataEntry]() As String
        Get
            Return _DependentValueDataEntry
        End Get
        Set(ByVal Value As String)
            _DependentValueDataEntry = Value
        End Set
    End Property
    Private _idDataEntryDependent2 As String
    <Bindable(True), Category("Data"), DefaultValue("")>
    Property [idDataEntryDependent2]() As String
        Get
            Return _idDataEntryDependent2
        End Get
        Set(ByVal Value As String)
            _idDataEntryDependent2 = Value
        End Set
    End Property
    Private _DependentValueDataEntry2 As Object
    <Bindable(True), Category("Data"), DefaultValue("")>
    Property [DependentValueDataEntry2]() As String
        Get
            Return _DependentValueDataEntry2
        End Get
        Set(ByVal Value As String)
            _DependentValueDataEntry2 = Value
        End Set
    End Property
    Private _idDataEntryDependent3 As String
    ''' <summary>
    ''' Dar acceso a la propiedad _idDataEntryDependent3. Relaci�n entre combos Centro y almacen
    ''' </summary>
    ''' <returns>El valor de la propiedad</returns>
    ''' <remarks>Llamada desde: campos.ascx    desglose.ascx     jsAlta.js       fsGeneralEntry.js; Tiempo m�ximo: 0</remarks>
    <Bindable(True), Category("Data"), DefaultValue("")>
    Property [idDataEntryDependent3]() As String
        Get
            Return _idDataEntryDependent3
        End Get
        Set(ByVal Value As String)
            _idDataEntryDependent3 = Value
        End Set
    End Property
    Private _DependentValueDataEntry3 As Object
    <Bindable(True), Category("Data"), DefaultValue("")>
    Property [DependentValueDataEntry3]() As String
        Get
            Return _DependentValueDataEntry3
        End Get
        Set(ByVal Value As String)
            _DependentValueDataEntry3 = Value
        End Set
    End Property
    Private _idDataEntryDependentCentroCoste As String
    ''' <summary>
    ''' Dar acceso a la propiedad _idDataEntryDependent3. Relaci�n entre combos Centro y almacen
    ''' </summary>
    ''' <returns>El valor de la propiedad</returns>
    ''' <remarks>Llamada desde: campos.ascx    desglose.ascx     jsAlta.js       fsGeneralEntry.js; Tiempo m�ximo: 0</remarks>
    <Bindable(True), Category("Data"), DefaultValue("")>
    Property [idDataEntryDependentCentroCoste]() As String
        Get
            Return _idDataEntryDependentCentroCoste
        End Get
        Set(ByVal Value As String)
            _idDataEntryDependentCentroCoste = Value
        End Set
    End Property
    Private _DependentValueCentroCoste As Object
    <Bindable(True), Category("Data"), DefaultValue("")>
    Property [DependentValueCentroCoste]() As String
        Get
            Return _DependentValueCentroCoste
        End Get
        Set(ByVal Value As String)
            _DependentValueCentroCoste = Value
        End Set
    End Property
    ''' <summary>
    ''' Dar acceso a la propiedad _DesgloseCambiaWidth. Si es true se establece el DropDownContainerWidth de los webdropdown.
    ''' Los entrys webdropdown correspondientes a campos no tienen problema con su DropDownContainerWidth es siempre suficientemente
    ''' grande como para ser bien visibles. En cambio, los entrys webdropdown correspondientes a desgloses no se ven bien resultan 
    ''' estrechos pq el drop.width de una columna no es mucho.
    ''' </summary>
    ''' <returns>El valor de la propiedad</returns>
    ''' <remarks>Llamada desde: desglose.ascx; Tiempo m�ximo: 0</remarks>
    <Bindable(True), Category("Data"), DefaultValue(0)>
    Property DesgloseCambiaWidth() As Boolean
        Get
            Return _DesgloseCambiaWidth
        End Get
        Set(ByVal Value As Boolean)
            _DesgloseCambiaWidth = Value
        End Set
    End Property
    <Bindable(True), Category("Behavour"), DefaultValue(0)>
    Property [Campo_Origen]() As Long
        Get

            Return _CampoOrigen
        End Get

        Set(ByVal Value As Long)
            _CampoOrigen = Value

        End Set
    End Property
    <Bindable(True), Category("Behavour"), DefaultValue(0)>
    Property [Tabla_Externa]() As Long
        Get
            Return _lTablaExterna
        End Get
        Set(ByVal Value As Long)
            _lTablaExterna = Value
        End Set
    End Property
    <Bindable(True), Category("Behavour"), DefaultValue(0)>
    Property [Servicio]() As Long
        Get
            Return _lServicio
        End Get
        Set(ByVal Value As Long)
            _lServicio = Value
        End Set
    End Property
    <Bindable(True), Category("Behavour"), DefaultValue(0)>
    Property [TipoRecepcion]() As Integer
        Get
            Return _nTipoRecepcion
        End Get
        Set(ByVal Value As Integer)
            _nTipoRecepcion = Value
        End Set
    End Property
    ''' <summary>
    ''' Dar acceso a la propiedad _recordarvalores
    ''' De momento no esta en Portal esta utilidad
    ''' </summary>
    ''' <remarks>Llamada desde: campos.ascx    desglose.ascx     jsAlta.js       fsGeneralEntry.js; Tiempo m�ximo: 0</remarks>
    <Bindable(True), Category("Behavour"), DefaultValue(1)>
    Property [RecordarValores]() As Boolean
        Get
            If Me.Portal Then _recordarvalores = False
            Return _recordarvalores
        End Get

        Set(ByVal Value As Boolean)
            If Me.Portal Then _recordarvalores = False
            _recordarvalores = Value
        End Set
    End Property
    ''' <summary>
    ''' Dar acceso a la propiedad _bSaltaCambioRechazo
    ''' </summary>
    ''' <remarks>Llamada desde: _common/desglose.ascx; Tiempo m�ximo: 0</remarks>
    <Bindable(True), Category("Data"), DefaultValue("")>
    Property bSaltaCambioRechazo() As Boolean
        Get
            Return _bSaltaCambioRechazo
        End Get
        Set(ByVal Value As Boolean)
            _bSaltaCambioRechazo = Value
        End Set
    End Property
    ''' <summary>
    ''' Dar acceso a la propiedad _NCSinRevisar
    ''' </summary>
    ''' <remarks>Llamada desde: _common/atachedfiles.ascx   _common/desglose.ascx; Tiempo m�ximo: 0</remarks>
    <Bindable(True), Category("Data"), DefaultValue("")>
    Property NCSinRevisar() As String
        Get
            Return _NCSinRevisar
        End Get
        Set(ByVal Value As String)
            _NCSinRevisar = Value
        End Set
    End Property
    ''' <summary>
    ''' Indica el ID del contrato
    ''' </summary>
    ''' <remarks>Llamada desde: _common/atachedfiles.ascx   _common/desglose.ascx; Tiempo m�ximo: 0</remarks>
    <Bindable(True), Category("Data"), DefaultValue(0)>
    Property IdContrato() As Long
        Get
            Return _idContrato
        End Get
        Set(ByVal Value As Long)
            _idContrato = Value
        End Set
    End Property
    ''' <summary>
    ''' Indica el ID del archivo del contrato
    ''' </summary>
    ''' <remarks>Llamada desde: _common/atachedfiles.ascx   _common/desglose.ascx; Tiempo m�ximo: 0</remarks>
    <Bindable(True), Category("Data"), DefaultValue(0)>
    Property IdArchivoContrato() As Long
        Get
            Return _idArchivoContrato
        End Get
        Set(ByVal Value As Long)
            _idArchivoContrato = Value
        End Set
    End Property
    ''' <summary>
    ''' Indica el tama�o del adjunto del contrato
    ''' </summary>
    ''' <remarks>Llamada desde: _common/atachedfiles.ascx   _common/desglose.ascx; Tiempo m�ximo: 0</remarks>
    <Bindable(True), Category("Data"), DefaultValue(0)>
    Property TamanyoContrato() As String
        Get
            Return _TamanyoContrato
        End Get
        Set(ByVal Value As String)
            _TamanyoContrato = Value
        End Set
    End Property
    ''' <summary>
    ''' 9060- A�o de la partida plurianual 
    ''' </summary>
    ''' <returns></returns>
    <Bindable(True), Category("Behavour"), DefaultValue(0)>
    Property idEntryPartidaPlurianual() As String
        Get
            Return _idEntryPartidaPlurianual
        End Get
        Set(ByVal Value As String)
            _idEntryPartidaPlurianual = Value
        End Set
    End Property

    ''' <summary>
    ''' 9060- A�o de la partida plurianual en campo o en desglose
    ''' </summary>
    ''' <returns></returns>
    <Bindable(True), Category("Behavour"), DefaultValue(0)>
    Property EntryPartidaPlurianualCampo() As Integer
        Get
            Return _EntryPartidaPlurianualCampo
        End Get
        Set(ByVal Value As Integer)
            _EntryPartidaPlurianualCampo = Value
        End Set
    End Property

    ''' Revisado por: blp. Fecha: 04/10/2011
    ''' <summary>
    ''' Crea el objeto entry propiamente dicho (en otros sitios es entry parte grafica). Aqui se le 
    ''' pasan las propiedades y se llama al javascript 
    ''' </summary>
    ''' <returns>String con la llamada de creaci�n</returns>
    ''' <remarks>Llamada desde:GeneralEntry.vb/RegisterValidatorCommonScript   script/alta/desglose.aspx 
    ''' script/certificados/certifproveedores.aspx; Tiempo m�ximo:0</remarks>
    <Bindable(True), Category("Behavour"), DefaultValue("")>
    Public ReadOnly Property [InitScript]() As String
        Get
            Dim sValor As String
            Dim sTexto As String
            Dim sValorDefecto As String
            Dim sTextoDefecto As String
            Dim sMonRepercutido As String
            Dim sCambioRepercutido As String
            Dim sMonCentral As String
            Dim sCambioCentral As String

            If _valor Is Nothing Then
                sValor = "null"
            Else
                sValor = """" + JSText(_valor.ToString()) + """"
            End If
            If _text Is Nothing Then
                sTexto = "null"
            Else
                sTexto = """" + JSText(_text.ToString()) + """"
            End If
            If _valor_Defecto Is Nothing Then
                sValorDefecto = "null"
            Else
                If _tipo = TiposDeDatos.TipoGeneral.TipoFecha Then
                    If Not IsDBNull(_valor_Defecto) Then
                        sValorDefecto = """" + CDate(_valor_Defecto).ToString("d", _DateFormat) + """"
                    Else
                        sValorDefecto = "null"
                    End If
                Else
                    sValorDefecto = """" + JSText(_valor_Defecto.ToString()) + """"
                End If
            End If
            If _texto_Defecto Is Nothing Then
                sTextoDefecto = "null"
            Else
                sTextoDefecto = """" + JSText(_texto_Defecto.ToString()) + """"
            End If
            If _MonImporteRepercutido Is Nothing Then
                sMonRepercutido = "null"
            Else
                sMonRepercutido = """" + JSText(_MonImporteRepercutido.ToString()) + """"
            End If
            If _MonCentral Is Nothing Then
                sMonCentral = "null"
            Else
                sMonCentral = """" + JSText(_MonCentral.ToString()) + """"
            End If
            If _CambioCentral Is Nothing Then
                sCambioCentral = "null"
            Else
                sCambioCentral = """" + JSNum(_CambioCentral.ToString()) + """"
            End If

            sCambioRepercutido = """" + JSNum(_CambioImporteRepercutido.ToString()) + """"

            If _DropDownGridID = "" Or Me.TipoGS = TiposDeDatos.TipoCampoGS.CodArticulo Or Me.TipoGS = TiposDeDatos.TipoCampoGS.Provincia Or Me.TipoGS = TiposDeDatos.TipoCampoGS.Contacto Or (Me.TipoGS = TiposDeDatos.TipoCampoGS.Persona And Me.Intro = 1) Then
                _DropDownGridID = Replace(Me.ID, "_", "x")
            End If
            _InitScript = "try{"
            Dim sIdContenedor As String = String.Empty

            If Not Me.IdContenedor = Nothing Then
                sIdContenedor = Me.IdContenedor + "_"
            End If


            If _Calculado Then
                _InitScript += "arrCalculados[arrCalculados.length]=""" + sIdContenedor + Me.ID + """;"
            End If
            If _Obligatorio AndAlso (Not _tipoGS = TiposDeDatos.IdsFicticios.DesgloseVinculado) Then
                _InitScript += "arrObligatorios[arrObligatorios.length]=""" + sIdContenedor + Me.ID + """;"
            End If
            If _Obligatorio AndAlso (Not _tipoGS = TiposDeDatos.IdsFicticios.DesgloseVinculado) Then
                _InitScript += "arrObligatoriosDen[arrObligatoriosDen.length]=""" + Me.Title.ToString + """;"
            End If
            If _tipoGS = TiposDeDatos.TipoCampoGS.CodArticulo Then
                _InitScript += "iNumArts=arrArticulos.length;arrArticulos[iNumArts]=new Array();arrArticulos[iNumArts][1]=""" + Me.ClientID + """;arrArticulos[iNumArts][0]=""" + Me.DependentField + """;"
            End If
            If _tipoGS = TiposDeDatos.TipoCampoGS.PRES1 Then
                _InitScript += "arrPres1[arrPres1.length]=""" + sIdContenedor + Me.ID + """;"
            End If
            If _tipoGS = TiposDeDatos.TipoCampoGS.Pres2 Then
                _InitScript += "arrPres2[arrPres2.length]=""" + sIdContenedor + Me.ID + """;"
            End If
            If _tipoGS = TiposDeDatos.TipoCampoGS.Pres3 Then
                _InitScript += "arrPres3[arrPres3.length]=""" + sIdContenedor + Me.ID + """;"
            End If
            If _tipoGS = TiposDeDatos.TipoCampoGS.Pres4 Then
                _InitScript += "arrPres4[arrPres4.length]=""" + sIdContenedor + Me.ID + """;"
            End If

            Dim sDepend As String
            If Not _DependentField = Nothing Then
                If _tipoGS <> TiposDeDatos.TipoCampoGS.Centro And _tipoGS <> TiposDeDatos.TipoCampoGS.Proveedor And _tipoGS <> TiposDeDatos.TipoCampoGS.OrganizacionCompras Then 'Cuando sea el centro _DependentField tendra el Campo del OrgCompras relacionado (bCentroRelacionadoFORM+campo)
                    sDepend = """" + sIdContenedor + _DependentField + """"
                Else
                    sDepend = """" + _DependentField + """"
                End If
            Else
                sDepend = "null"
            End If

            Dim sValidationControl As String
            If Not _ValidationControl = Nothing Then
                sValidationControl = """" + _ValidationControl + """"
            Else
                sValidationControl = "null"
            End If

            Dim sIndependiente As String
            If Not _Independiente = Nothing Then
                sIndependiente = """" + _Independiente.ToString() + """"
            Else
                sIndependiente = "null"
            End If

            Dim sIdCampoPadre As String
            If Not _IdCampoPadre = Nothing Then
                sIdCampoPadre = """" + _IdCampoPadre.ToString + """"

            Else
                sIdCampoPadre = "null"
            End If

            Dim sRestric As String
            If _sRestric <> Nothing Then
                sRestric = """" & _sRestric & """"
            Else
                sRestric = "null"
            End If



            Dim sIdCamposHijos As String
            If _IdCamposHijos <> Nothing Then
                sIdCamposHijos = _IdCamposHijos
            Else
                sIdCamposHijos = "null"
            End If

            Dim sIdCamposHijosOrigen As String
            If _IdCamposHijosOrigen <> Nothing Then
                sIdCamposHijosOrigen = _IdCamposHijosOrigen
            Else
                sIdCamposHijosOrigen = "null"
            End If

            Dim sIdCamposHijosCampoDef As String
            If _IdCamposHijosCampoDef <> Nothing Then
                sIdCamposHijosCampoDef = _IdCamposHijosCampoDef
            Else
                sIdCamposHijosCampoDef = "null"
            End If

            Dim sInstanciaMoneda As String
            If (_tipoGS = TiposDeDatos.TipoCampoGS.RefSolicitud) Or (_tipoGS = TiposDeDatos.TipoCampoGS.NuevoCodArticulo) Or (_tipoGS = TiposDeDatos.TipoCampoGS.DenArticulo) Or (_tipoGS = TiposDeDatos.TipoCampoGS.CodArticulo) Then
                If _InstanciaMoneda <> Nothing Then
                    sInstanciaMoneda = """" & _InstanciaMoneda & """"
                Else
                    sInstanciaMoneda = """" & "EUR" & """"
                End If
            Else
                sInstanciaMoneda = "null"
            End If

            Dim bCargarUltADJ As Boolean
            If _CargarUltADJ <> Nothing Then
                bCargarUltADJ = _CargarUltADJ
            Else
                bCargarUltADJ = False
            End If

            Dim bVerUON As Boolean
            If _bVerUON <> Nothing Then
                bVerUON = _bVerUON
            Else
                bVerUON = False
            End If

            Dim sPRES5 As String
            If _sPRES5 <> Nothing Then
                sPRES5 = _sPRES5
            Else
                sPRES5 = Nothing
            End If

            Dim sFormato As String
            If _sFormato <> Nothing Then
                sFormato = """" & JSText(_sFormato) & """"
            Else
                sFormato = "null"
            End If

            Dim sIdEntryPrec As String
            If _idEntryPREC <> Nothing Then
                sIdEntryPrec = """" & _idEntryPREC & """"
            Else
                sIdEntryPrec = "null"
            End If
            Dim sIdEntryProv As String
            If _idEntryPROV <> Nothing Then
                sIdEntryProv = """" & _idEntryPROV & """"
            Else
                sIdEntryProv = "null"
            End If
            Dim sIdEntryART As String
            If _idEntryART <> Nothing Then
                sIdEntryART = """" & _idEntryART & """"
            Else
                sIdEntryART = "null"
            End If

            Dim sIdEntryUni As String
            If _idEntryUNI <> Nothing Then
                sIdEntryUni = """" & _idEntryUNI & """"
            Else
                sIdEntryUni = "null"
            End If
            Dim sIdEntryUniPedido As String
            If _idEntryUNIPedido <> Nothing Then
                sIdEntryUniPedido = """" & _idEntryUNIPedido & """"
            Else
                sIdEntryUniPedido = "null"
            End If
            Dim sIdEntryCANT As String
            If _idEntryCANT <> Nothing Then
                sIdEntryCANT = """" & _idEntryCANT & """"
            Else
                sIdEntryCANT = "null"
            End If
            Dim sIdEntryCC As String
            If _idEntryCC <> Nothing Then
                sIdEntryCC = """" & _idEntryCC & """"
            Else
                sIdEntryCC = "null"
            End If
            Dim sUnidadOculta As String
            If _sUnidadOculta <> Nothing Then
                sUnidadOculta = """" + JSText(_sUnidadOculta.ToString()) + """"
            Else
                sUnidadOculta = "null"
            End If
            Dim sIdDataEntryDependent As String
            If _idDataEntryDependent <> Nothing Then
                sIdDataEntryDependent = """" & _idDataEntryDependent & """"
            Else
                sIdDataEntryDependent = "null"
            End If
            Dim sIdDataEntryDependent2 As String
            If _idDataEntryDependent2 <> Nothing Then
                sIdDataEntryDependent2 = """" & _idDataEntryDependent2 & """"
            Else
                sIdDataEntryDependent2 = "null"
            End If
            Dim sIdDataEntryDependent3 As String
            If _idDataEntryDependent3 <> Nothing Then
                sIdDataEntryDependent3 = """" & _idDataEntryDependent3 & """"
            Else
                sIdDataEntryDependent3 = "null"
            End If
            Dim sIdDataEntryDependentCentroCoste As String
            If _idDataEntryDependentCentroCoste <> Nothing Then
                sIdDataEntryDependentCentroCoste = """" & _idDataEntryDependentCentroCoste & """"
            Else
                sIdDataEntryDependentCentroCoste = "null"
            End If
            Dim sIdiomaBotonTextoMedio As String
            sIdiomaBotonTextoMedio = "SPA"
            Select Case _tipo
                Case TiposDeDatos.TipoGeneral.TipoTextoMedio, TiposDeDatos.TipoGeneral.TipoString
                    sIdiomaBotonTextoMedio = _Idi
            End Select

            Dim sNCSinRevisar As String
            If _NCSinRevisar <> Nothing Then
                sNCSinRevisar = """" & _NCSinRevisar & """"
            Else
                sNCSinRevisar = "null"
            End If

            Dim sConcepto As String
            If _iConcepto <> Nothing Then
                sConcepto = """" & _iConcepto & """"
            Else
                sConcepto = "0"
            End If

            Dim sAlmacenamiento As String
            If _iAlmacenamiento <> Nothing Then
                sAlmacenamiento = """" & _iAlmacenamiento & """"
            Else
                sAlmacenamiento = "0"
            End If

            Dim sRecepcion As String
            If _iRecepcion <> Nothing Then
                sRecepcion = """" & _iRecepcion & """"
            Else
                sRecepcion = "0"
            End If

            Dim sNumeroDeDecimales As String
            If _nNumeroDeDecimales <> Nothing Then
                sNumeroDeDecimales = """" & _nNumeroDeDecimales & """"
            Else
                sNumeroDeDecimales = "null"
            End If

            Dim sCampoOrigenVinculado As String
            If Not _CampoOrigenVinculado = Nothing Then
                sCampoOrigenVinculado = """" + _CampoOrigenVinculado + """"
            Else
                sCampoOrigenVinculado = "null"
            End If

            Dim sDescrComboPopUp As String
            If _DescrComboPopUp Is Nothing Then
                sDescrComboPopUp = "null"
            Else
                sDescrComboPopUp = """" + JSText(_DescrComboPopUp.ToString()) + """"
            End If

            Dim bEsProveedorParticipante As Boolean
            If _bEsProveedorParticipante <> Nothing Then
                bEsProveedorParticipante = _bEsProveedorParticipante
            Else
                bEsProveedorParticipante = False
            End If

            Dim bFechaNoAnteriorAlSistema As Boolean
            If _FechaNoAnteriorAlSistema <> Nothing Then
                bFechaNoAnteriorAlSistema = _FechaNoAnteriorAlSistema
            Else
                bFechaNoAnteriorAlSistema = False
            End If
            Dim bCargarProveSumiArt As Boolean
            If _CargarProveSumiArt <> Nothing Then
                bCargarProveSumiArt = _CargarProveSumiArt
            Else
                bCargarProveSumiArt = False
            End If
            Dim sidEntryProveSumiArt As String
            If _idEntryProveSumiArt <> Nothing Then
                sidEntryProveSumiArt = """" + _idEntryProveSumiArt + """"
            Else
                sidEntryProveSumiArt = "null"
            End If


            Dim sContactos As String
            If _sContactos <> Nothing Then
                sContactos = _sContactos
            Else
                sContactos = "null"
            End If

            Dim sTooltip As String
            If _toolTip Is Nothing Then
                sTooltip = "null"
            Else
                sTooltip = """" + JSText(_toolTip.ToString()) + """"
            End If

            Dim sUnidadOcultaPedido As String
            If _sUnidadPedidoOculta <> Nothing Then
                sUnidadOcultaPedido = """" + JSText(_sUnidadPedidoOculta.ToString()) + """"
            Else
                sUnidadOcultaPedido = "null"
            End If

            Dim sMonSolicit As String
            If _MonSolicit <> Nothing Then
                sMonSolicit = """" + JSText(_MonSolicit.ToString()) + """"
            Else
                sMonSolicit = "null"
            End If

            Dim sFPagoOculta As String
            If _FPagoOculta <> Nothing Then
                sFPagoOculta = """" + JSText(_FPagoOculta.ToString()) + """"
            Else
                sFPagoOculta = "null"
            End If

            Me.Name = Me.Name & "$"

            Dim usarOrgCompras As String = "false"
            If Me.UsarOrgCompras Then
                usarOrgCompras = "true"
            End If

            Dim sActivoSM As String = "false"
            If Me.ActivoSM Then sActivoSM = "true"

            Dim sServicio As String
            If _lServicio <> Nothing Then
                sServicio = """" & _lServicio & """"
            Else
                sServicio = "0"
            End If

            Dim sTipoRecepcion As String
            If _nTipoRecepcion <> Nothing Then
                sTipoRecepcion = """" & _nTipoRecepcion & """"
            Else
                sTipoRecepcion = "0"
            End If
            Dim sNivelSeleccion As String = IIf(_nivelSeleccion = Nothing, 0, _nivelSeleccion)
            Dim sidDataEntryFORMMaterial As String
            If idDataEntryFORMMaterial <> "" Then
                sidDataEntryFORMMaterial = """" + JSText(idDataEntryFORMMaterial) + """"
            Else
                sidDataEntryFORMMaterial = "null"
            End If

            Dim sIdMaterialOculta As String
            If _IdMaterialOculta <> Nothing Then
                sIdMaterialOculta = """" & _IdMaterialOculta & """"
            Else
                sIdMaterialOculta = "null"
            End If
            Dim sIdCodArticuloOculta As String
            If _IdCodArticuloOculta <> Nothing Then
                sIdCodArticuloOculta = """" & _IdCodArticuloOculta & """"
            Else
                sIdCodArticuloOculta = "null"
            End If
            Dim sIdDenArticuloOculta As String
            If _IdDenArticuloOculta <> Nothing Then
                sIdDenArticuloOculta = """" & _IdDenArticuloOculta & """"
            Else
                sIdDenArticuloOculta = "null"
            End If

            Dim sSelCualquierMatPortal As String = "false"
            If Me.SelCualquierMatPortal Then sSelCualquierMatPortal = "true"

            Dim sFilasAutoCompleteExtender As String = ConfigurationManager.AppSettings("FilasAutoCompleteExtender")

            Dim sIdEntryPartidaPlurianual As String
            Dim sEntryPartidaPlurianualCampo As String
            If _idEntryPartidaPlurianual <> Nothing Then
                sIdEntryPartidaPlurianual = """" & _idEntryPartidaPlurianual & """"
                sEntryPartidaPlurianualCampo = """" & EntryPartidaPlurianualCampo & """"
            Else
                sIdEntryPartidaPlurianual = "null"
                sEntryPartidaPlurianualCampo = "0"
            End If

            _InitScript += ";fsGeneralEntry_Setup(""" + Me.Name + Me.ID + """,""" + sIdContenedor + Me.ID + """, " + CInt(_tipo).ToString("#")
            _InitScript += "," + IIf(_tipoGS = TiposDeDatos.TipoCampoGS.SinTipo, "0", CInt(_tipoGS).ToString("#")) + ", " + sValor
            _InitScript += "," + IIf(_IsGridEditor, "true", "false") + ", """ + _oTag.ToString() + """,""" + _nullMsg + """," + IIf(_readOnly, "true", "false")
            _InitScript += ",""" + Replace(Replace(Me.IdContenedor, "__", "x"), "_", "") + "x" + _DropDownGridID + "xdd" + """, """ + JSText(_title) + """, """ + _errMsg + """, """ + _errMsgFormato + """, """ + _maxLength.ToString + """, " + sRestric + ", " + IIf(_Obligatorio, "true", "false")
            _InitScript += "," + IIf(_BaseDesglose, "true", "false") + ", " + sDepend + ", " + sValidationControl + ", " + sIndependiente + ", " + sIdCampoPadre + ", " + sIdCamposHijos + ", " + (IIf(_intro = 1, "1", IIf(_intro = 2, "2", "0"))) + ", " + (IIf(_articuloCodificado, "true", "false")) + ", " + _orden.ToString + ", " + _grupo.ToString
            _InitScript += "," + _idatrib.ToString + ", " + (IIf(_articuloGenerico, "true", "false")) + ",false," + (IIf(_anyadirArt, "true", "false")) + ", " + _valerp.ToString + "," + IIf(_codigoArticulo Is Nothing, "null", """" & _codigoArticulo & """") + "," + _nColumna.ToString + "," + _nLinea.ToString + "," + _idRefSol.ToString + "," + _avisoBloqueoRefSOL.ToString + "," + sInstanciaMoneda + "," + IIf(bCargarUltADJ, "true", "false")
            _InitScript += "," + sIdEntryPrec + "," + sIdEntryProv + "," + sIdEntryUni + "," + sIdEntryCC
            _InitScript += "," + sIdDataEntryDependent + "," + sIdDataEntryDependent2
            _InitScript += "," + (IIf(Me.PM, "true", "false"))
            _InitScript += "," + IIf(_Calculado, "true", "false") & "," & _lTablaExterna.ToString() & "," & Campo_Origen & "," & IdCampoPadreOrigen & "," & sIdCamposHijosOrigen
            _InitScript += ",""" + sIdiomaBotonTextoMedio + """"
            _InitScript += "," + IIf(_PoseeDefecto, "true", "false")
            _InitScript += "," + sValorDefecto
            _InitScript += "," + sTextoDefecto
            _InitScript += "," + sMonRepercutido
            _InitScript += "," + sCambioRepercutido
            _InitScript += "," + sMonCentral
            _InitScript += "," + sCambioCentral
            _InitScript += "," + IIf(_bSaltaCambioRechazo, "true", "false")
            _InitScript += "," + sNCSinRevisar
            _InitScript += "," + IIf(bVerUON, "true", "false")
            _InitScript += ",""" + sPRES5 + """"
            _InitScript += "," + sConcepto
            _InitScript += "," + sAlmacenamiento
            _InitScript += "," + sRecepcion
            _InitScript += "," + sTexto
            _InitScript += "," + IIf(RecordarValores, "true", "false")
            _InitScript += "," + sIdEntryCANT + "," + sNumeroDeDecimales + "," + sUnidadOculta

            _InitScript += "," + _idCampoHijaListaEnlazada.ToString
            _InitScript += "," + _idCampoPadreListaEnlazada.ToString
            _InitScript += "," + _idCampoListaEnlazada.ToString
            _InitScript += ",false"

            _InitScript += "," + (IIf(Me.Portal, "true", "false"))
            _InitScript += "," + _CampoDef_CampoODesgHijo.ToString
            _InitScript += "," + _CampoDef_DesgPadre.ToString
            _InitScript += "," + sIdCamposHijosCampoDef
            _InitScript += "," + sCampoOrigenVinculado
            _InitScript += "," + sDescrComboPopUp
            _InitScript += "," + IIf(bEsProveedorParticipante, "true", "false")
            _InitScript += "," + sContactos
            _InitScript += "," + sIdDataEntryDependent3
            _InitScript += ",""" + Me.MasterField + """" 'Se pone la propiedad y no el par�metro privado local para que si es nothing pase un string vac�o
            _InitScript += ",""" + JSText(Me.DenGrupo) + """"
            _InitScript += ",""" + Me.DenDesgloseMat
            _InitScript += """," + sTooltip
            _InitScript += "," + Me.TipoSolicit.ToString
            _InitScript += "," + sIdEntryUniPedido
            _InitScript += "," + sUnidadOcultaPedido
            _InitScript += "," + sMonSolicit
            _InitScript += "," + sFPagoOculta
            _InitScript += "," + usarOrgCompras
            _InitScript += "," + sServicio
            _InitScript += "," + sFormato
            _InitScript += "," + sTipoRecepcion
            _InitScript += "," + sNivelSeleccion
            _InitScript += "," + sidDataEntryFORMMaterial
            _InitScript += "," + sActivoSM
            _InitScript += "," + IIf(Not String.IsNullOrEmpty(sFilasAutoCompleteExtender), sFilasAutoCompleteExtender, "10")
            _InitScript += "," + IIf(bFechaNoAnteriorAlSistema, "true", "false")
            _InitScript += "," + IIf(bCargarProveSumiArt, "true", "false")
            _InitScript += "," + sIdEntryART
            _InitScript += "," + sidEntryProveSumiArt
            _InitScript += "," + sIdDataEntryDependentCentroCoste

            _InitScript += "," + sIdMaterialOculta
            _InitScript += "," + sIdCodArticuloOculta
            _InitScript += "," + sIdDenArticuloOculta
            _InitScript += "," + sSelCualquierMatPortal
            _InitScript += "," + sIdEntryPartidaPlurianual
            _InitScript += "," + sEntryPartidaPlurianualCampo
            _InitScript += ")}catch(e){status=""" + sIdContenedor + Me.ID + "  Can\'t init editor"";}"

            Return _InitScript
        End Get
    End Property
    <Bindable(True), Category("Data"), DefaultValue(0)>
    Property [articuloCodificado]() As Boolean
        Get

            Return _articuloCodificado
        End Get

        Set(ByVal Value As Boolean)
            _articuloCodificado = Value

        End Set
    End Property
    <Bindable(True), Category("Data"), DefaultValue(0)>
    Property [AnyadirArt]() As Boolean
        Get

            Return _anyadirArt
        End Get

        Set(ByVal Value As Boolean)
            _anyadirArt = Value

        End Set
    End Property
    <Bindable(True), Category("Data"), DefaultValue(0)>
    Property [articuloGenerico]() As Boolean
        Get

            Return _articuloGenerico
        End Get

        Set(ByVal Value As Boolean)
            _articuloGenerico = Value

        End Set
    End Property
    <Bindable(True), Category("Data"), DefaultValue(0)>
    Property [DenArticuloModificado]() As Boolean
        Get

            Return _DenArticuloModificado
        End Get

        Set(ByVal Value As Boolean)
            _DenArticuloModificado = Value

        End Set
    End Property
    <Bindable(True), Category("Data"), DefaultValue(0)>
    Property [codigoArticulo]() As String
        Get

            Return _codigoArticulo
        End Get

        Set(ByVal Value As String)
            _codigoArticulo = Value

        End Set
    End Property
    <Bindable(True), Category("Data"), DefaultValue(0)>
    Property [nColumna]() As Byte
        Get

            Return _nColumna
        End Get

        Set(ByVal Value As Byte)
            _nColumna = Value

        End Set
    End Property
    <Bindable(True), Category("Data"), DefaultValue(0)>
    Property [nLinea]() As Byte
        Get

            Return _nLinea
        End Get

        Set(ByVal Value As Byte)
            _nLinea = Value

        End Set
    End Property
    <Bindable(True), Category("Data"), DefaultValue("")>
    Property [IdiomaBotonTextoMedio]() As String
        Get

            Return _sIdiomaBotonTextoMedio
        End Get

        Set(ByVal Value As String)
            _sIdiomaBotonTextoMedio = Value

        End Set
    End Property
    <Bindable(True), Category("Data"), DefaultValue("")>
    Property [Denominacion]() As String
        Get
            Return _Denominacion
        End Get

        Set(ByVal Value As String)
            _Denominacion = Value
        End Set
    End Property
    <Bindable(True), Category("Data"), DefaultValue(0)>
    Property [Concepto]() As Integer
        Get

            Return _iConcepto
        End Get

        Set(ByVal Value As Integer)
            _iConcepto = Value
        End Set
    End Property
    <Bindable(True), Category("Data"), DefaultValue(0)>
    Property [Almacenamiento]() As Integer
        Get

            Return _iAlmacenamiento
        End Get

        Set(ByVal Value As Integer)
            _iAlmacenamiento = Value
        End Set
    End Property
    <Bindable(True), Category("Data"), DefaultValue(0)>
    Property [Recepcion]() As Integer
        Get

            Return _iRecepcion
        End Get

        Set(ByVal Value As Integer)
            _iRecepcion = Value
        End Set
    End Property
    <Bindable(True), Category("Data"), DefaultValue(0)>
    Property [idCampoHijaListaEnlazada]() As Long
        Get

            Return _idCampoHijaListaEnlazada
        End Get

        Set(ByVal Value As Long)
            _idCampoHijaListaEnlazada = Value

        End Set
    End Property
    <Bindable(True), Category("Data"), DefaultValue(0)>
    Property [idCampoDefHijaListaEnlazada]() As Long
        Get

            Return _idCampoDefHijaListaEnlazada
        End Get

        Set(ByVal Value As Long)
            _idCampoDefHijaListaEnlazada = Value

        End Set
    End Property
    <Bindable(True), Category("Data"), DefaultValue(0)>
    Property [idGrupoHijaListaEnlazada]() As Long
        Get

            Return _idGrupoHijaListaEnlazada
        End Get

        Set(ByVal Value As Long)
            _idGrupoHijaListaEnlazada = Value

        End Set
    End Property
    <Bindable(True), Category("Data"), DefaultValue(0)>
    Property [idCampoPadreListaEnlazada]() As Long
        Get

            Return _idCampoPadreListaEnlazada
        End Get

        Set(ByVal Value As Long)
            _idCampoPadreListaEnlazada = Value

        End Set
    End Property
    <Bindable(True), Category("Data"), DefaultValue(0)>
    Property [idCampoListaEnlazada]() As Long
        Get

            Return _idCampoListaEnlazada
        End Get

        Set(ByVal Value As Long)
            _idCampoListaEnlazada = Value

        End Set
    End Property
    <Bindable(True), Category("Data"), DefaultValue(0)>
    Property [idCampoDefPadreListaEnlazada]() As Long
        Get

            Return _idCampoDefPadreListaEnlazada
        End Get

        Set(ByVal Value As Long)
            _idCampoDefPadreListaEnlazada = Value

        End Set
    End Property
    <Bindable(True), Category("Data"), DefaultValue(0)>
    Property [idGrupoPadreListaEnlazada]() As Long
        Get

            Return _idGrupoPadreListaEnlazada
        End Get

        Set(ByVal Value As Long)
            _idGrupoPadreListaEnlazada = Value

        End Set
    End Property
    <Bindable(True), Category("Data"), DefaultValue(False)>
    Property [ListaOpcionesCargada]() As Boolean
        Get

            Return _ListaOpcionesCargada
        End Get

        Set(ByVal Value As Boolean)
            _ListaOpcionesCargada = Value

        End Set
    End Property
    <Bindable(True), Category("Data"), DefaultValue(0)>
    Property [Instancia]() As Long
        Get

            Return _lInstancia
        End Get

        Set(ByVal Value As Long)
            _lInstancia = Value

        End Set
    End Property
    ''' <summary>
    ''' Guarda el COPIA_CAMPO_DEF.ID de un campo no desglose o un campo desglose hijo
    ''' </summary>
    <Bindable(True), Category("Behavour"), DefaultValue(0)>
    Property CampoDef_CampoODesgHijo() As Long
        Get
            Return _CampoDef_CampoODesgHijo
        End Get
        Set(ByVal Value As Long)
            _CampoDef_CampoODesgHijo = Value
        End Set
    End Property
    ''' <summary>
    ''' Guarda el COPIA_CAMPO_DEF.ID de un campo desglose padre
    ''' </summary>
    <Bindable(True), Category("Behavour"), DefaultValue(0)>
    Property CampoDef_DesgPadre() As Long
        Get
            Return _CampoDef_DesgPadre
        End Get
        Set(ByVal Value As Long)
            _CampoDef_DesgPadre = Value
        End Set
    End Property
    ''' <summary>
    ''' Tarea Vinculaciones (1959) de un desglose con otro: Aqui se guarda el FORM_CAMPO.ID del campo origen vinculado al campo q es el entry.
    ''' </summary>
    <Bindable(True), Category("Behavour"), DefaultValue(0)>
    Property CampoOrigenVinculado() As String
        Get
            Return _CampoOrigenVinculado
        End Get
        Set(ByVal Value As String)
            _CampoOrigenVinculado = Value
        End Set
    End Property
    ''' <summary>
    ''' Tarea Vinculaciones (1959) de un desglose con otro: Aqui se guarda si se puede mover lineas de un desglose.
    ''' </summary>
    <Bindable(True), Category("Behavour"), DefaultValue(0)>
    Property Movible() As Boolean
        Get
            Return _Movible
        End Get
        Set(ByVal Value As Boolean)
            _Movible = Value
        End Set
    End Property
    ''' <summary>
    ''' Guarda el texto de un combo en un desglose popup. 
    ''' El valor del combo (ejemplo: valor->1 texto-> Hola Mundo) va en la propiedad Valor.
    ''' Esta propiedad se mete pq a MontarFormularioSubmit llegaba el valor pero no el texto, luego a la hora de grabar en
    ''' copia_linea_desglose no se rellenaba a diferencia de los no popup el valor_text. Y el valor_text es lo q, a fin de cuentas
    ''' , se muestra si es readonly --> muestra vacio si no se rellena.
    ''' </summary>
    <Bindable(True), Category("Behavour"), DefaultValue(0)>
    Property DescrComboPopUp() As String
        Get
            Return _DescrComboPopUp
        End Get
        Set(ByVal Value As String)
            _DescrComboPopUp = Value
        End Set
    End Property
    <Bindable(True), Category("Data"), DefaultValue(0)>
    Property EsProveedorParticipante() As Boolean
        Get
            Return _bEsProveedorParticipante
        End Get

        Set(ByVal Value As Boolean)
            _bEsProveedorParticipante = Value
        End Set
    End Property
    <Bindable(True), Category("Data"), DefaultValue("")>
    Property Contactos() As String
        Get
            Return _sContactos
        End Get

        Set(ByVal Value As String)
            _sContactos = Value
        End Set
    End Property
    ''' <summary>
    ''' Tipo de solicitud  (PM, QA, Pedido Express, ...)
    ''' </summary>
    ''' <remarks>Llamada desde: Todos los entrys; Tiempo m�ximo: 0</remarks>
    <Bindable(True), Category("Data"), DefaultValue("")>
    Property [TipoSolicit]() As Integer
        Get
            Return _TipoSolicit
        End Get

        Set(ByVal Value As Integer)
            _TipoSolicit = Value
        End Set
    End Property
    ''' <summary>
    ''' Id del entry q contiene el campo Unidad de Pedido
    ''' </summary>
    ''' <remarks>Llamada desde: Todos los entrys; Tiempo m�ximo: 0</remarks>
    <Bindable(True), Category("Data"), DefaultValue("")>
    Property [idEntryUNIPedido]() As String
        Get
            Return _idEntryUNIPedido
        End Get
        Set(ByVal Value As String)
            _idEntryUNIPedido = Value
        End Set
    End Property
    ''' <summary>
    ''' Valor del campo Unidad de Pedido oculto
    ''' </summary>
    ''' <remarks>Llamada desde: Todos los entrys; Tiempo m�ximo: 0</remarks>
    <Bindable(True), Category("Data"), DefaultValue("")>
    Property [UnidadPedidoOculta]() As String
        Get
            Return _sUnidadPedidoOculta
        End Get
        Set(ByVal Value As String)
            _sUnidadPedidoOculta = Value
        End Set
    End Property
    ''' <summary>
    ''' Moneda de la solicitud 
    ''' </summary>
    ''' <remarks>Llamada desde: Todos los entrys; Tiempo m�ximo: 0</remarks>
    <Bindable(True), Category("Data"), DefaultValue("")>
    Property [MonSolicit]() As String
        Get
            Return _MonSolicit
        End Get

        Set(ByVal Value As String)
            _MonSolicit = Value
        End Set
    End Property
    ''' <summary>
    ''' Valor del campo Forma de pago oculto
    ''' </summary>
    ''' <remarks>Llamada desde: Todos los entrys; Tiempo m�ximo: 0</remarks>
    <Bindable(True), Category("Data"), DefaultValue("")>
    Property [FPagoOculta]() As String
        Get
            Return _FPagoOculta
        End Get

        Set(ByVal Value As String)
            _FPagoOculta = Value
        End Set
    End Property
    <Bindable(True), Category("Data"), DefaultValue("")>
    Property [UsarOrgCompras]() As Boolean
        Get
            Return _UsarOrgCompras
        End Get

        Set(ByVal Value As Boolean)
            _UsarOrgCompras = Value
        End Set
    End Property
    <Bindable(True), Category("Data"), DefaultValue("")>
    Property [ActivoSM]() As Boolean
        Get
            Return _ActivoSM
        End Get

        Set(ByVal Value As Boolean)
            _ActivoSM = Value
        End Set
    End Property
    <Bindable(True), Category("Data"), DefaultValue("")>
    Property [NivelSeleccion]() As Integer
        Get
            Return _nivelSeleccion
        End Get

        Set(ByVal Value As Integer)
            _nivelSeleccion = Value
        End Set
    End Property
    <Bindable(True), Category("Data"), DefaultValue("")>
    Property idDataEntryFORMMaterial As String
        Get
            Return _idDataEntryFORMMaterial
        End Get
        Set(value As String)
            _idDataEntryFORMMaterial = value
        End Set
    End Property

    <Bindable(True), Category("Data"), DefaultValue("")>
    Property [IdMaterialOculta]() As String
        Get
            Return _IdMaterialOculta
        End Get
        Set(ByVal Value As String)
            _IdMaterialOculta = Value
        End Set
    End Property

    <Bindable(True), Category("Data"), DefaultValue("")>
    Property [IdCodArticuloOculta]() As String
        Get
            Return _IdCodArticuloOculta
        End Get
        Set(ByVal Value As String)
            _IdCodArticuloOculta = Value
        End Set
    End Property

    <Bindable(True), Category("Data"), DefaultValue("")>
    Property [IdDenArticuloOculta]() As String
        Get
            Return _IdDenArticuloOculta
        End Get
        Set(ByVal Value As String)
            _IdDenArticuloOculta = Value
        End Set
    End Property
#End Region
#Region " Control Generator Methods "
    Private Sub CtrlGSTipoChekBox()
        Dim oTbl As System.Web.UI.HtmlControls.HtmlTable
        Dim oTblRow As System.Web.UI.HtmlControls.HtmlTableRow
        Dim oTblCell As System.Web.UI.HtmlControls.HtmlTableCell
        Dim oCheck As System.Web.UI.WebControls.CheckBox
        Dim oWidth As System.Web.UI.WebControls.Unit

        oWidth = Me.Width

        oTbl = New System.Web.UI.HtmlControls.HtmlTable
        oTbl.Width = oWidth.ToString

        oTbl.ID = "_tbl"
        oTbl.CellPadding = 0
        oTbl.CellSpacing = 0
        oTbl.Style.Add("border-width", "0px")

        oCheck = New System.Web.UI.WebControls.CheckBox
        oCheck.ID = "_t"

        If _valor = True Then
            oCheck.Checked = True
        Else
            oCheck.Checked = False
        End If

        oCheck.Text = _text

        oTblRow = New System.Web.UI.HtmlControls.HtmlTableRow
        oTblCell = New System.Web.UI.HtmlControls.HtmlTableCell
        oTblCell.Width = "100%"
        oTblCell.Controls.Add(oCheck)
        oTblRow.Cells.Add(oTblCell)
        oTbl.Rows.Add(oTblRow)
        oTbl.ID = "_tbl"
        Me.Controls.Add(oTbl)
    End Sub
    ''' Revisado por: blp. Fecha: 04/10/2011
    ''' <summary>
    ''' Generar un dataentry de tipo string
    ''' En principio es para los tipo string largo, pero los de 800 caractere tb lo usan para mostrar una textarea
    ''' en luger de un text edit para que puedan ver los textos con salto de linea
    ''' </summary>
    ''' <remarks>Llamada desde: La generacion de cualquier dataentry de tipo String largo o texto medio; Tiempo m�ximo:0 </remarks>
    Private Sub CtrlTipoStringLargo()
        Dim oTblInput As System.Web.UI.HtmlControls.HtmlTable
        Dim oTblCellInput As System.Web.UI.HtmlControls.HtmlTableCell
        Dim oTblRowInput As System.Web.UI.HtmlControls.HtmlTableRow
        Dim oTbl As System.Web.UI.HtmlControls.HtmlTable
        Dim oTblRow As System.Web.UI.HtmlControls.HtmlTableRow
        Dim oTblCell As System.Web.UI.HtmlControls.HtmlTableCell
        Dim oHid As System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oBut As New System.Web.UI.HtmlControls.HtmlInputButton

        Dim oWidth As System.Web.UI.WebControls.Unit

        'por "corregir" 
        _intro = 0

        oTbl = New System.Web.UI.HtmlControls.HtmlTable

        oTblInput = New System.Web.UI.HtmlControls.HtmlTable

        oWidth = Me.Width

        oTblInput.Width = oWidth.ToString
        oTblInput.CellPadding = 0
        oTblInput.CellSpacing = 0
        oTblInput.Border = 0

        If Me.Width.ToString = "0px" Then
            oTblInput.Style.Item("border") = "0 solid white"
        End If
        oTblInput.Style.Item("width") = oWidth.ToString
        oTblRowInput = New System.Web.UI.HtmlControls.HtmlTableRow
        oTblCellInput = New System.Web.UI.HtmlControls.HtmlTableCell
        oTblInput.Rows.Add(oTblRowInput)
        oTblCellInput.Width = "100%"
        oTblRowInput.Cells.Add(oTblCellInput)

        oTbl.Width = oWidth.ToString

        oTbl.ID = "_tbl"
        oTbl.CellPadding = 0
        oTbl.CellSpacing = 0
        oTbl.Border = 0
        oTbl.Style.Add("border-width", "0px")
        oHid = New System.Web.UI.HtmlControls.HtmlInputHidden

        If RecordarValores = True Then
            Dim oTxt As New WebControls.TextBox
            oTxt = New WebControls.TextBox

            oTxt.ID = "_t"
            oHid.ID = "_h"
            oTxt.Rows = 1
            oTxt.Style.Item("height") = "16px"
            If Me.Width.Type = UnitType.Pixel Then
                oTxt.Style.Item("width") = (Me.Width.Value - 22).ToString + "px"
            Else
                oTxt.Style.Item("width") = Me.Width.ToString()
            End If

            If Not _valor Is Nothing Then
                oHid.Value = _valor.ToString
                oTxt.Text = _valor.ToString
            End If

            If Not _text Is Nothing Then
                oTxt.Text = _text.ToString
            End If

            oTxt.Style.Item("overflow-y") = "hidden"
            oTxt.Style.Item("border-style") = "none"
            oTxt.Style.Item("border-width") = "0px"
            oTxt.Style.Item("width") = "100%"
            oTxt.Style.Item("float") = "left"
            oTxt.Style.Add("resize", "none")
            If _readOnly Then
                oTxt.Attributes("READONLY") = _readOnly
                oTxt.Style.Item("background-color") = "transparent"
            End If

            If _maxLength > 0 Then
                oTxt.Attributes("onkeypress") = "return ControlLongitudStringLargo(this," & _maxLength & ")"
                oTxt.Attributes("onchange") = "return validarLengthStringLargo(this," & _maxLength & ",'" & _errMsg & "')"
            End If

            oTblRow = New System.Web.UI.HtmlControls.HtmlTableRow
            oTblCell = New System.Web.UI.HtmlControls.HtmlTableCell
            oTblCell.Width = "100%"
            oTblCellInput.Controls.Add(oTxt)

            'Si el campo tiene activo el campo SAVE_VALUES(FORM_CAMPO) se le a�ade el AutoCompleteExtender
            Dim Extender As New AutoCompleteExtender()
            With Extender
                .ID = Me.UniqueID & "$" & "autocompleteextender"
                .CompletionSetCount = 10
                .DelimiterCharacters = ""
                .Enabled = True
                .MinimumPrefixLength = 0
                .CompletionListCssClass = "autoCompleteList"
                .CompletionListHighlightedItemCssClass = "autoCompleteSelectedListItem"
                .CompletionListItemCssClass = "autoCompleteListItem"
                .ServiceMethod = "DevolverValores"
                .ServicePath = ConfigurationManager.AppSettings("rutaPM") & "AutoCompletePMWEB.asmx"
                .TargetControlID = oTxt.ID
                .EnableCaching = False
                .UseContextKey = True
                If Me._CampoOrigen = 0 Then
                    .ContextKey = Me.Tag
                Else
                    .ContextKey = Me._CampoOrigen
                End If
                .OnClientShown = "resetPosition"
                .CompletionInterval = 500
            End With

            oTblCellInput.Controls.Add(Extender)

        Else
            Dim oTxt As New System.Web.UI.HtmlControls.HtmlTextArea
            oTxt = New System.Web.UI.HtmlControls.HtmlTextArea

            oTxt.ID = "_t"
            oHid.ID = "_h"
            oTxt.Rows = 1
            oTxt.Style.Item("height") = "16px"
            If Me.Width.Type = UnitType.Pixel Then
                oTxt.Style.Item("width") = (Me.Width.Value - 22).ToString + "px"
            Else
                oTxt.Style.Item("width") = Me.Width.ToString()
            End If

            If Not _valor Is Nothing Then
                oHid.Value = _valor.ToString
                oTxt.InnerText = _valor.ToString
            End If

            If Not _text Is Nothing Then
                oTxt.InnerText = _text.ToString
            End If

            oTxt.Style.Item("overflow-y") = "hidden"
            oTxt.Style.Item("border-style") = "none"
            oTxt.Style.Item("border-width") = "0px"
            oTxt.Style.Item("float") = "left"
            oTxt.Style.Add("resize", "none")
            If _readOnly Then
                oTxt.Attributes("READONLY") = _readOnly
                oTxt.Style.Item("background-color") = "transparent"
            End If

            If _maxLength > 0 Then
                oTxt.Attributes("onkeypress") = "return ControlLongitudStringLargo(this," & _maxLength & ")"
                oTxt.Attributes("onchange") = "return validarLengthStringLargo(this," & _maxLength & ",'" & _errMsg & "')"
            End If

            oTblRow = New System.Web.UI.HtmlControls.HtmlTableRow
            oTblCell = New System.Web.UI.HtmlControls.HtmlTableCell
            oTblCell.Width = "100%"
            oTblCellInput.Controls.Add(oTxt)
        End If

        oTblCell.Controls.Add(oHid)
        oTblRow.Cells.Add(oTblCell)

        oBut.Visible = True
        oBut.Attributes("width") = "15px"
        oBut.Style.Item("border-style") = "none"
        oBut.Style.Item("border-width") = "0px"
        oBut.Style.Item("background-image") = "url(" & Me.Page.ClientScript.GetWebResourceUrl(GetType(GeneralEntry), "Fullstep.DataEntry.trespuntos.gif") & ")"
        oBut.Style.Item("background-position") = "center center"
        oBut.Style.Item("background-repeat") = "no-repeat"
        oBut.Style.Item("height") = "6px"
        oBut.Style.Item("width") = "14px"

        oTblCellInput = New System.Web.UI.HtmlControls.HtmlTableCell
        oTblCellInput.Controls.Add(oBut)
        oTblCellInput.Attributes("class") = "fsstyentrydd"

        oTblRowInput.Cells.Add(oTblCellInput)
        oTblCell.Controls.Add(oTblInput)
        oTblRow.Cells.Add(oTblCell)
        oTbl.Rows.Add(oTblRow)
        Me.Controls.Add(oTbl)

    End Sub
    ''' Revisado por: blp. Fecha: 04/10/2011
    ''' <summary>
    ''' Generar un dataentry de tipo string
    ''' </summary>
    ''' <param name="iTipo">tipo del string: corto o medio</param>
    ''' <remarks>Llamada desde: La generacion de cualquier dataentry de tipo String ; Tiempo m�ximo:0 </remarks>
    Private Sub CtrlTipoString(ByVal iTipo As FSNLibrary.TiposDeDatos.TipoGeneral)

        If RecordarValores Then

            Dim oTblInput As System.Web.UI.HtmlControls.HtmlTable
            Dim oTblCellInput As System.Web.UI.HtmlControls.HtmlTableCell
            Dim oTblRowInput As System.Web.UI.HtmlControls.HtmlTableRow
            Dim oTbl As System.Web.UI.HtmlControls.HtmlTable
            Dim oTblRow As System.Web.UI.HtmlControls.HtmlTableRow
            Dim oTblCell As System.Web.UI.HtmlControls.HtmlTableCell
            Dim oHid As System.Web.UI.HtmlControls.HtmlInputHidden
            Dim oBut As New System.Web.UI.HtmlControls.HtmlInputButton
            Dim oTxt As New WebControls.TextBox
            Dim oWidth As System.Web.UI.WebControls.Unit

            'por "corregir" 
            _intro = 0

            oTbl = New System.Web.UI.HtmlControls.HtmlTable
            oTblInput = New System.Web.UI.HtmlControls.HtmlTable

            _toolTip = Nothing

            oWidth = Me.Width

            oTblInput.Width = oWidth.ToString
            oTblInput.CellPadding = 0
            oTblInput.CellSpacing = 0
            oTblInput.Border = 0

            If Me.Width.ToString = "0px" Then
                oTblInput.Style.Item("border") = "0 solid white"
            End If

            oTblInput.Style.Item("width") = oWidth.ToString
            oTblRowInput = New System.Web.UI.HtmlControls.HtmlTableRow
            oTblCellInput = New System.Web.UI.HtmlControls.HtmlTableCell
            oTblInput.Rows.Add(oTblRowInput)
            oTblCellInput.Width = "100%"
            oTblRowInput.Cells.Add(oTblCellInput)

            oTbl.ID = "_tbl"
            oTbl.Width = oWidth.ToString
            oTbl.CellPadding = 0
            oTbl.CellSpacing = 0
            oTbl.Border = 0
            oTbl.Style.Add("border-width", "0px")

            oHid = New System.Web.UI.HtmlControls.HtmlInputHidden
            oTxt = New WebControls.TextBox

            Select Case iTipo
                Case TiposDeDatos.TipoGeneral.TipoTextoCorto, TiposDeDatos.TipoGeneral.TipoTextoMedio, TiposDeDatos.TipoGeneral.TipoString, TiposDeDatos.TipoGeneral.TipoTextoLargo
                    If _maxLength > 0 Then
                        oTxt.MaxLength = _maxLength
                    Else
                        If iTipo = TiposDeDatos.TipoGeneral.TipoTextoCorto Then
                            oTxt.MaxLength = 100
                        ElseIf iTipo = TiposDeDatos.TipoGeneral.TipoTextoMedio Or iTipo = TiposDeDatos.TipoGeneral.TipoString Then
                            oTxt.MaxLength = 800
                        End If
                    End If
            End Select

            oTxt.ID = "_t"
            oHid.ID = "_h"
            oTxt.Rows = 1
            oTxt.Style.Item("height") = "16px"
            If Me.Width.Type = UnitType.Pixel Then
                oTxt.Style.Item("width") = (Me.Width.Value - 22).ToString + "px"
            Else
                oTxt.Style.Item("width") = Me.Width.ToString()
            End If

            If Not _valor Is Nothing Then
                oHid.Value = _valor.ToString
                oTxt.Text = _valor.ToString
            End If

            If Not _text Is Nothing Then
                oTxt.Text = _text.ToString
            End If

            oTxt.Style.Item("overflow-y") = "hidden"
            oTxt.Style.Item("border-style") = "none"
            oTxt.Style.Item("border-width") = "0px"
            oTxt.Style.Item("width") = "100%"
            oTxt.Style.Item("float") = "left"
            oTxt.Style.Add("resize", "none")
            If _readOnly Then
                oTxt.Attributes("READONLY") = _readOnly
                oTxt.Style.Item("background-color") = "transparent"
            End If

            Dim txtEventosChange As String = ""

            If Not _readOnly AndAlso Not _sFormato Is Nothing Then
                txtEventosChange += "validarFormato(this,'" & JSText(_sFormato) & "','" & _title & ". " & _errMsgFormato & "'); "
            End If

            If oTxt.MaxLength > 0 Then 'Si no hay maxlength en los tipo largo no hace falta llamar a estas funciones.
                oTxt.Attributes("onkeypress") = "return ControlLongitudStringLargo(this," & _maxLength & ")"
                txtEventosChange += "return validarLengthStringLargo(this," & _maxLength & ",'" & _errMsg & "')"
            End If

            oTxt.Attributes("onchange") = txtEventosChange

            oTblRow = New System.Web.UI.HtmlControls.HtmlTableRow
            oTblCell = New System.Web.UI.HtmlControls.HtmlTableCell
            oTblCell.Width = "100%"
            oTblCellInput.Controls.Add(oTxt)

            Dim Extender As New AutoCompleteExtender()
            With Extender
                .ID = Me.UniqueID & "$" & "autocompleteextender"
                .CompletionSetCount = 10
                .DelimiterCharacters = ""
                .Enabled = True
                .MinimumPrefixLength = 0
                .CompletionListCssClass = "autoCompleteList"
                .CompletionListHighlightedItemCssClass = "autoCompleteSelectedListItem"
                .CompletionListItemCssClass = "autoCompleteListItem"
                .ServiceMethod = "DevolverValores"
                .ServicePath = ConfigurationManager.AppSettings("rutaPM") & "AutoCompletePMWEB.asmx"
                .TargetControlID = oTxt.ID
                .EnableCaching = False
                .UseContextKey = True
                If Me._CampoOrigen = 0 Then
                    .ContextKey = Me.Tag
                Else
                    .ContextKey = Me._CampoOrigen
                End If
                .OnClientShown = "resetPosition"
                .CompletionInterval = 500
            End With

            oTblCellInput.Controls.Add(Extender)

            oTblCell.Controls.Add(oHid)
            oTblRow.Cells.Add(oTblCell)
            If _readOnly = False Then
                oBut.Visible = True
                oBut.Attributes("width") = "15px"
                oBut.Style.Item("border-style") = "none"
                oBut.Style.Item("border-width") = "0px"
                oBut.Style.Item("background-image") = "url(" & Me.Page.ClientScript.GetWebResourceUrl(GetType(GeneralEntry), "Fullstep.DataEntry.trespuntos.gif") & ")"
                oBut.Style.Item("background-position") = "center center"
                oBut.Style.Item("background-repeat") = "no-repeat"
                oBut.Style.Item("height") = "6px"
                oBut.Style.Item("width") = "14px"

                oTblCellInput = New System.Web.UI.HtmlControls.HtmlTableCell
                oTblCellInput.Controls.Add(oBut)
                oTblCellInput.Attributes("class") = "fsstyentrydd"
            End If

            oTblRowInput.Cells.Add(oTblCellInput)
            oTblCell.Controls.Add(oTblInput)
            oTblRow.Cells.Add(oTblCell)
            oTbl.Rows.Add(oTblRow)
            Me.Controls.Add(oTbl)

        Else
            Dim oTbl As System.Web.UI.HtmlControls.HtmlTable
            Dim oTblRow As System.Web.UI.HtmlControls.HtmlTableRow
            Dim oTblCell As System.Web.UI.HtmlControls.HtmlTableCell
            Dim oHid As System.Web.UI.HtmlControls.HtmlInputHidden
            Dim oWidth As System.Web.UI.WebControls.Unit
            Dim oTxt As Infragistics.WebUI.WebDataInput.WebTextEdit

            oTbl = New System.Web.UI.HtmlControls.HtmlTable
            oHid = New System.Web.UI.HtmlControls.HtmlInputHidden
            oTxt = New Infragistics.WebUI.WebDataInput.WebTextEdit

            oWidth = Me.Width

            oTbl.ID = "_tbl"
            oTbl.Width = oWidth.ToString
            oTbl.CellPadding = 0
            oTbl.CellSpacing = 0
            oTbl.Style.Add("border-width", "0px")

            oTxt.ID = "_t"
            oHid.ID = "_h"

            If _maxLength > 0 Then
                oTxt.MaxLength = _maxLength
            Else
                oTxt.MaxLength = If(iTipo = TiposDeDatos.TipoGeneral.TipoTextoCorto, 100, 800)
            End If

            If Not _valor Is Nothing Then
                oHid.Value = _valor.ToString
                oTxt.Text = _valor.ToString
            End If

            If Not _text Is Nothing Then
                oTxt.Text = _text.ToString
            End If

            oTxt.ReadOnly = _readOnly
            oTxt.Width = Me.Width
            oTxt.Style.Add("resize", "none")
            oTxt.Style.Add("display", "")

            If _readOnly Then
                oTxt.BorderStyle = BorderStyle.None
                oTxt.BorderWidth = Unit.Pixel(0)
                oTxt.BackColor = System.Drawing.Color.Transparent
            End If

            If oTxt.MaxLength > 0 Then
                oTxt.Attributes("onkeypress") = "return ControlLongitudStringLargo(this," & _maxLength & ")"
                oTxt.Attributes("onchange") = "return validarLengthStringLargo(this," & _maxLength & ",'" & _errMsg & "')"
            End If

            If Strings.Len(oTxt.Text) > 80 Then
                oTxt.ButtonsAppearance.CustomButtonDisplay = Infragistics.WebUI.WebDataInput.ButtonDisplay.OnRight
                oTxt.ButtonsAppearance.CustomButtonImageUrl = Me.Page.ClientScript.GetWebResourceUrl(GetType(GeneralEntry), "Fullstep.DataEntry.trespuntos.gif")
            End If

            oTblRow = New System.Web.UI.HtmlControls.HtmlTableRow
            oTblCell = New System.Web.UI.HtmlControls.HtmlTableCell

            oTblCell.Width = "100%"
            oTblCell.Controls.Add(oTxt)
            oTblCell.Controls.Add(oHid)

            oTblRow.Cells.Add(oTblCell)
            oTbl.Rows.Add(oTblRow)
            Me.Controls.Add(oTbl)
        End If
    End Sub
    ''' <summary>
    ''' Generar un dataentry de tipo num�rico
    '''	</summary>
    ''' <remarks>Llamada desde: La generacion de cualquier dataentry de tipo num�rico que no sea lista; Tiempo m�ximo:0 </remarks>
    Private Sub CtrlTipoNumerico(Optional ByVal bControlarNumeroDecimales As Boolean = False)
        Dim oNumber As Infragistics.WebUI.WebDataInput.WebNumericEdit
        Dim oTbl As System.Web.UI.HtmlControls.HtmlTable
        Dim oTblRow As System.Web.UI.HtmlControls.HtmlTableRow
        Dim oTblCell As System.Web.UI.HtmlControls.HtmlTableCell

        Dim oWidth As System.Web.UI.WebControls.Unit

        oWidth = Me.Width

        oTbl = New System.Web.UI.HtmlControls.HtmlTable
        oTbl.ID = "_tbl"
        oTbl.Width = oWidth.ToString
        oTbl.CellPadding = 0
        oTbl.CellSpacing = 0
        oTbl.Style.Add("border-width", "0px")

        oNumber = New Infragistics.WebUI.WebDataInput.WebNumericEdit
        oNumber.ID = "_t"
        oNumber.ReadOnly = _readOnly

        If Not _valor Is Nothing Then oNumber.Value = _valor
        If Not _readOnly Then
            If Not _maximo Is Nothing Then oNumber.MaxValue = _maximo
            If Not _minimo Is Nothing Then oNumber.MinValue = _minimo
        End If

        If Not _readOnly AndAlso _valor IsNot Nothing AndAlso _valor IsNot DBNull.Value AndAlso _valor.ToString <> String.Empty AndAlso (_minimo IsNot Nothing OrElse _maximo IsNot Nothing) _
            AndAlso (CType(_minimo, Double) > _valor OrElse CType(_maximo, Double) < _valor) Then
            oNumber.Value = DBNull.Value
            Dim FSWSServer As Object
            FSWSServer = If(Me.Portal = True, HttpContext.Current.Session("FS_Portal_Server"), HttpContext.Current.Session("FSN_Server"))
            Dim oDict As Object = FSWSServer.Get_Dictionary()
            oDict.LoadData(TiposDeDatos.ModulosIdiomas.Menu, _Idi)
            Dim oTextos As DataTable = oDict.Data.Tables(0)
            Dim iRow As Integer
            If Me.Portal Then
                iRow = 15
            Else
                iRow = 77
            End If
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "numerosIncorrectos" & IdContenedor & "_" & ID & "_" & oNumber.ID, "alert('" & Replace(oTextos.Rows(iRow).Item(1), "###", DenGrupo & " - " & Title) & "')", True)
        End If
        oNumber.Width = Me.Width
        oNumber.NumberFormat = _NumberFormat
        oNumber.Style("display") = ""

        If bControlarNumeroDecimales Then
            oNumber.ClientSideEvents.TextChanged = "limiteDecimalesTextChanged"
        End If

        oTblRow = New System.Web.UI.HtmlControls.HtmlTableRow
        oTblCell = New System.Web.UI.HtmlControls.HtmlTableCell
        oTblCell.Width = "100%"
        oTblCell.Controls.Add(oNumber)
        oTblRow.Cells.Add(oTblCell)
        oTbl.Rows.Add(oTblRow)
        Me.Controls.Add(oTbl)
    End Sub
    ''' <summary>
    ''' Generar un dataentry de tipo fecha
    '''	</summary>
    ''' <remarks>Llamada desde: La generacion de cualquier dataentry de tipo Fecha que no sea lista; Tiempo m�ximo:0 </remarks>
    Private Sub CtrlTipoFecha()
        Dim oTbl As System.Web.UI.WebControls.Table
        Dim oTblRow As System.Web.UI.WebControls.TableRow
        Dim oTblCell As System.Web.UI.WebControls.TableCell
        Dim oDate As Infragistics.WebUI.WebDataInput.WebDateTimeEdit

        oTbl = New System.Web.UI.WebControls.Table
        oTbl.ID = "_tbl"
        oTbl.BorderWidth = Unit.Pixel(0)
        oTbl.CellPadding = 0
        oTbl.CellSpacing = 0
        oTbl.Style.Add("border-width", "0px")

        oDate = New Infragistics.WebUI.WebDataInput.WebDateTimeEdit
        If Not _DateFormat Is Nothing Then
            oDate.EditModeFormat = _DateFormat.ShortDatePattern
            oDate.DisplayModeFormat = _DateFormat.ShortDatePattern
        Else
            oDate.EditModeFormat = "dd/MM/yyyy"
            oDate.DisplayModeFormat = "dd/MM/yyyy"
        End If
        oDate.DataMode = Infragistics.WebUI.WebDataInput.DateDataMode.Date
        oDate.ID = "_t"
        oDate.ReadOnly = _readOnly
        If Not _readOnly Then
            oDate.MaxValue = _maximo
            oDate.MinValue = _minimo
        End If
        If Not _valor Is Nothing Then oDate.Value = _valor
        If Not _readOnly AndAlso _valor IsNot Nothing AndAlso _valor IsNot DBNull.Value AndAlso (_minimo IsNot Nothing OrElse _maximo IsNot Nothing) _
            AndAlso (CType(_minimo, Date) > _valor OrElse CType(_maximo, Date) < _valor) Then
            oDate.Value = DBNull.Value
            Dim FSWSServer As Object
            FSWSServer = If(Me.Portal = True, HttpContext.Current.Session("FS_Portal_Server"), HttpContext.Current.Session("FSN_Server"))
            Dim oDict As Object = FSWSServer.Get_Dictionary()
            oDict.LoadData(TiposDeDatos.ModulosIdiomas.Menu, _Idi)
            Dim oTextos As DataTable = oDict.Data.Tables(0)
            Dim iRow As Integer
            If Me.Portal Then
                iRow = 15
            Else
                iRow = 77
            End If
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "fechasIncorrectas" & IdContenedor & "_" & ID & "_" & oDate.ID, "alert('" & Replace(oTextos.Rows(iRow).Item(1), "###", DenGrupo & " - " & Title) & "')", True)
        End If
        If _readOnly Then
            oDate.ButtonsAppearance.CustomButtonDisplay = Infragistics.WebUI.WebDataInput.ButtonDisplay.None
        Else
            oDate.ButtonsAppearance.CustomButtonDisplay = Infragistics.WebUI.WebDataInput.ButtonDisplay.OnRight
            oDate.ButtonsAppearance.CustomButtonImageUrl = Me.Page.ClientScript.GetWebResourceUrl(GetType(GeneralEntry), "Fullstep.DataEntry.calendar-color.png")
        End If

        If _tipoGS = TiposDeDatos.TipoCampoGS.IniSuministro Or _tipoGS = TiposDeDatos.TipoCampoGS.FinSuministro Then
            oDate.ClientSideEvents.ValueChange = "ddFechasSuministro_ValueChange"
            If _FechaNoAnteriorAlSistema Then
                oDate.ClientSideEvents.Blur = "ddFechaNoAnteriorAlSistema_ValueChange"
            End If
        ElseIf _tipoGS = TiposDeDatos.TipoCampoGS.FecDespublicacion Then
            oDate.ClientSideEvents.ValueChange = "ddFecDespublicacion_ValueChange"
        ElseIf _tipoGS = TiposDeDatos.TipoCampoGS.FechaLimCumplim Then
            oDate.ClientSideEvents.ValueChange = "ddFecLimCumplimentacion_ValueChange"
        ElseIf _FechaNoAnteriorAlSistema Then
            oDate.ClientSideEvents.ValueChange = "ddFechaNoAnteriorAlSistema_ValueChange"
            oDate.ClientSideEvents.Blur = "ddFechaNoAnteriorAlSistema_ValueChange"
        ElseIf ID = "GeneralFecha" Then
            oDate.ClientSideEvents.ValueChange = "validarFechaTraslado"
        Else
            oDate.ClientSideEvents.ValueChange = "ddFec_ValueChange"
            oDate.ClientSideEvents.Blur = "ddFec_ValueChange"
        End If
        oDate.Style("display") = ""

        oTblRow = New System.Web.UI.WebControls.TableRow
        oTblCell = New System.Web.UI.WebControls.TableCell
        oDate.Height = 12 'Este valor impide que con doctype XHTML haya problemas con cambios de altura del control webdatetimeedit en cualquier p�gina que use campos.ascx.vb (e igual tb desglose.ascx.vb)
        oTblCell.Controls.Add(oDate)
        oTblRow.Cells.Add(oTblCell)
        oTbl.Rows.Add(oTblRow)
        Me.Controls.Add(oTbl)
    End Sub
    ''' <summary>
    ''' Generar un dataentry de tipo archivo dentro de un desglose
    ''' </summary>
    ''' <remarks>Llamada desde: La generacion de cualquier dataentry de tipo Archivo de desglose; Tiempo m�ximo:0 </remarks>
    Private Sub CtrlTipoArchivoDesglose()
        Dim DivContenedor As New HtmlControls.HtmlGenericControl("DIV")
        Dim oTbl As System.Web.UI.WebControls.Table
        Dim oTblRow As System.Web.UI.WebControls.TableRow
        Dim oTblCell As System.Web.UI.WebControls.TableCell
        Dim oTblInput As System.Web.UI.HtmlControls.HtmlTable
        Dim oTblCellInput As System.Web.UI.HtmlControls.HtmlTableCell
        Dim oTblRowInput As System.Web.UI.HtmlControls.HtmlTableRow
        Dim oHidAct As System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oHidNew As System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oHidNombre As System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oHidTipo As New System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oHidFavorito As New System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oHidCampo As New System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oHidInstancia As New System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oHidPortalInstancia As New System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oHidID As New System.Web.UI.HtmlControls.HtmlInputHidden


        Dim oBut As New System.Web.UI.HtmlControls.HtmlInputButton
        Dim bSoloUnArchivo As Boolean = False
        Dim oWidth As System.Web.UI.WebControls.Unit

        Dim FSWSServer As Object
        Dim oAdjuntos As Object

        Dim lCiaComp As Long

        If Me.Portal = True Then
            lCiaComp = HttpContext.Current.Session("FS_Portal_CiaComp")

            FSWSServer = HttpContext.Current.Session("FS_Portal_Server")
        Else
            FSWSServer = HttpContext.Current.Session("FSN_Server")
        End If

        oAdjuntos = FSWSServer.Get_Adjuntos()

        Dim idCampo As Integer
        Dim idAdjuntos As String = String.Empty
        Dim idAdjuntosNew As String = String.Empty
        Dim NombreAdjunto As String = String.Empty
        Dim Instancia As Long
        Dim PortalInstancia As Long = 0
        Dim EsAltafactura As Boolean = False
        Dim bFavorito As Boolean
        Dim iTipo As Integer
        Dim IdInstanciaImportar As Integer

        Dim oUser As Object

        If Me.Portal = True Then
            oUser = HttpContext.Current.Session("FS_Portal_User")
        Else
            oUser = HttpContext.Current.Session("FSN_User")
        End If

        Dim sIdi As String = oUser.Idioma.ToString()
        If sIdi = Nothing Then
            sIdi = ConfigurationManager.AppSettings("idioma")
        End If

        Dim oDict As Object = FSWSServer.Get_Dictionary()
        oDict.LoadData(TiposDeDatos.ModulosIdiomas.Adjuntos, sIdi)

        Dim oTextos As DataTable = oDict.Data.Tables(0)

        If Me._CampoOrigen = 0 Then
            idCampo = CInt(Me.Tag)
        Else
            idCampo = CInt(Me._CampoOrigen)
        End If

        'Hidden de campo
        oHidCampo.ID = "_hCampo"
        oHidCampo.Value = idCampo

        If _valor <> "" Then
            If InStr(_valor, "####") Then
                idAdjuntos = Split(_valor, "####")(0)
                idAdjuntosNew = Split(_valor, "####")(1)
            Else
                idAdjuntos = _valor
            End If
            NombreAdjunto = _text.ToString
            If _valor <> "" AndAlso _valor <> "####" AndAlso Not (InStr(_valor, "xx") > 0) Then
                bSoloUnArchivo = True
            End If
        End If



        'Hidden de adjunto
        oHidAct = New System.Web.UI.HtmlControls.HtmlInputHidden
        oHidAct.ID = "_hAct"


        'Hidden de nuevo adjunto
        oHidNew = New System.Web.UI.HtmlControls.HtmlInputHidden
        oHidNew.ID = "_hNew"


        'Hidden de nombre de adjuntos
        oHidNombre = New System.Web.UI.HtmlControls.HtmlInputHidden
        oHidNombre.ID = "_hNombre"

        If Not TryCast(Page.FindControl("Instancia"), HtmlControls.HtmlInputHidden) Is Nothing Then
            Try
                Instancia = CLng(TryCast(Page.FindControl("Instancia"), HtmlControls.HtmlInputHidden).Value)
            Catch ex As Exception
                Instancia = 0
            End Try
            Try
                'Anteriormente: certif automatico tiene Hidden Instancia pero lo pone a ""
                'Tras tarea Solic Autom, no haya copia_campo_adjun pero si instancia -> no llega "" -> intenta ir a copia_campo_adjun

                If Me.Portal AndAlso Instancia = 0 AndAlso (Not TryCast(Page.FindControl("instanciaPort"), HtmlControls.HtmlInputHidden) Is Nothing) Then
                    PortalInstancia = CLng(TryCast(Page.FindControl("instanciaPort"), HtmlControls.HtmlInputHidden).Value)
                End If

                If Me.Portal AndAlso Instancia = 0 AndAlso (Not TryCast(Page.FindControl("instanciaPortF"), HtmlControls.HtmlInputHidden) Is Nothing) Then
                    PortalInstancia = CLng(TryCast(Page.FindControl("instanciaPortF"), HtmlControls.HtmlInputHidden).Value)
                    'Sabes q estas en alta facturas
                    EsAltafactura = True
                    PortalInstancia = PortalInstancia * (-1)
                End If

                If CStr(TryCast(Page.FindControl("Version"), HtmlControls.HtmlInputHidden).Value) = "" Then
                    If Me.Portal Then
                        PortalInstancia = Instancia
                    End If
                    Instancia = 0
                End If
            Catch ex As Exception
            End Try
        Else 'Portal: Alta factura
            If Me.Portal AndAlso (Not TryCast(Page.FindControl("instanciaPortF"), HtmlControls.HtmlInputHidden) Is Nothing) Then
                PortalInstancia = CLng(TryCast(Page.FindControl("instanciaPortF"), HtmlControls.HtmlInputHidden).Value)
                'Sabes q estas en alta facturas
                EsAltafactura = True
                PortalInstancia = PortalInstancia * (-1)
            End If

            Instancia = 0
        End If


        'Si es una linea del desglose
        oHidAct.Value = idAdjuntos
        oHidNew.Value = idAdjuntosNew
        oHidNombre.Value = _text


        If Me.Posee_Defecto = False And Me._BaseDesglose = True And Instancia > 0 Then
            oHidAct.Value = ""
            oHidNew.Value = ""
            oHidNombre.Value = ""
            idAdjuntos = ""
            idAdjuntosNew = ""
            _text = ""
        End If


        If idAdjuntos <> "" Then
            idAdjuntos = Replace(idAdjuntos, "xx", ",")
        End If
        If idAdjuntosNew <> "" Then
            idAdjuntosNew = Replace(idAdjuntosNew, "xx", ",")
        End If


        'Hidden de la instancia
        oHidInstancia = New System.Web.UI.HtmlControls.HtmlInputHidden
        oHidInstancia.ID = "_hInstancia"
        oHidInstancia.Value = Instancia

        If Me.Portal = True Then
            oHidPortalInstancia = New System.Web.UI.HtmlControls.HtmlInputHidden
            oHidPortalInstancia.ID = IIf(EsAltafactura, "_hInstanciaPortF", "_hInstanciaPort")
            oHidPortalInstancia.Value = (-1) * PortalInstancia
        End If

        If Not TryCast(Page.FindControl("Favorito"), HtmlControls.HtmlInputHidden) Is Nothing Then
            bFavorito = IIf(TryCast(Page.FindControl("Favorito"), HtmlControls.HtmlInputHidden).Value = "1", True, False)
        Else
            bFavorito = False
        End If

        If Not TryCast(Page.FindControl("IdInstanciaImportar"), HtmlControls.HtmlInputHidden) Is Nothing Then
            IdInstanciaImportar = IIf(TryCast(Page.FindControl("IdInstanciaImportar"), HtmlControls.HtmlInputHidden).Value <> "", TryCast(Page.FindControl("IdInstanciaImportar"), HtmlControls.HtmlInputHidden).Value, 0)
        Else
            IdInstanciaImportar = 0
        End If

        'Campo desglose
        iTipo = 3


        'Hidden del tipo
        oHidTipo = New System.Web.UI.HtmlControls.HtmlInputHidden
        oHidTipo.ID = "_hTipo"
        oHidTipo.Value = iTipo

        If Not (idAdjuntos = "" AndAlso idAdjuntosNew = "") Then
            'Obtenemos lo adjuntos
            If Me.Portal = True Then
                If Instancia = 0 Then
                    oAdjuntos.Load(lCiaComp, iTipo, idAdjuntos, idAdjuntosNew, sIdi)
                Else
                    oAdjuntos.LoadInst(lCiaComp, iTipo, idAdjuntos, idAdjuntosNew)
                    'Si es una adjunto metido desde A�adir por valor por defecto. La informaci�n esta en LINEA_DESGLOSE_ADJUN
                    If oAdjuntos.Data.Tables(0).Rows.Count = 0 Then
                        oAdjuntos.Load(lCiaComp, _tipo, idAdjuntos, idAdjuntosNew)
                    End If
                End If
            Else
                If Instancia = 0 Then
                    oAdjuntos.Load(iTipo, idAdjuntos, idAdjuntosNew, bFavorito Or IdInstanciaImportar > 0)
                Else
                    oAdjuntos.LoadInst(iTipo, idAdjuntos, idAdjuntosNew)
                    'Si es una adjunto metido desde A�adir por valor por defecto. La informaci�n esta en LINEA_DESGLOSE_ADJUN
                    If oAdjuntos.Data.Tables(0).Rows.Count = 0 Then
                        oAdjuntos.Load(_tipo, idAdjuntos, idAdjuntosNew)
                    End If
                End If
            End If
        End If

        oTbl = New System.Web.UI.WebControls.Table
        oWidth = IIf(Me.Width.IsEmpty, Unit.Percentage(100), Me.Width)
        oTbl.Width = oWidth
        oTbl.BorderWidth = Unit.Pixel(0)
        oTbl.CellPadding = 0
        oTbl.CellSpacing = 0


        oTblInput = New System.Web.UI.HtmlControls.HtmlTable
        oTblRowInput = New System.Web.UI.HtmlControls.HtmlTableRow
        oTblCellInput = New System.Web.UI.HtmlControls.HtmlTableCell

        oTblInput.Width = oWidth.ToString
        oTblInput.CellPadding = 0
        oTblInput.CellSpacing = 0
        oTblInput.Border = 0

        If oAdjuntos.Data Is Nothing OrElse (oAdjuntos.Data.Tables.Count = 0 AndAlso oAdjuntos.Data.Tables(0).Rows.Count = 0) Then
            'SI NO TIENE ADJUNTOS
            oTblCellInput = New System.Web.UI.HtmlControls.HtmlTableCell

            If Not _readOnly Then
                Dim Enlace As New HtmlControls.HtmlAnchor
                Enlace.InnerText = oTextos.Rows(1).Item(1)
                Enlace.Style("cursor") = "pointer"
                Enlace.Attributes("onclick") = "show_atach_file('" + Instancia.ToString() + "', '" + idAdjuntos.ToString() + "','" + iTipo.ToString() + "', '" + Me.ClientID.ToString() + "','" + Page.AppRelativeVirtualPath + "','0','')"
                Enlace.Style("text-decoration") = "underline"
                Enlace.Attributes.Add("class", "enlaceAdj")
                Enlace.Style("color") = "#0000FF"
                oTblCellInput.Controls.Add(Enlace)
            End If

            Dim oLink As System.Web.UI.HtmlControls.HtmlAnchor
            oLink = New System.Web.UI.HtmlControls.HtmlAnchor
            oLink.ID = "_t"
            oLink.Style.Item("display") = "none"
            oLink.Style.Item("width") = "100%"
            'oLink.Attributes.Item("CLASS") = "TipoArchivo"
            oLink.Style("cursor") = "pointer"
            oTblCellInput.Align = "right"
            oTblCellInput.Controls.Add(oLink)

            oTblRowInput.Cells.Add(oTblCellInput)
            oTblInput.Rows.Add(oTblRowInput)

            oTblRow = New System.Web.UI.WebControls.TableRow
            oTblCell = New System.Web.UI.WebControls.TableCell

            DivContenedor.ID = "divcontenedor"
            DivContenedor.Controls.Add(oTblInput)
            oTblCell.Controls.Add(DivContenedor)

            oTblCell.Controls.Add(oHidAct)
            oTblCell.Controls.Add(oHidNew)
            oTblCell.Controls.Add(oHidNombre)
            oTblCell.Controls.Add(oHidCampo)
            oTblCell.Controls.Add(oHidFavorito)
            oTblCell.Controls.Add(oHidInstancia)
            If Me.Portal = True Then oTblCell.Controls.Add(oHidPortalInstancia)
            oTblCell.Controls.Add(oHidTipo)
            oTblRow.Cells.Add(oTblCell)

            oTbl.Rows.Add(oTblRow)
            oTbl.ID = "_tbl"

            Me.Controls.Add(oTbl)
        Else
            'SI TIENE ADJUNTOS

            oTblCellInput = New System.Web.UI.HtmlControls.HtmlTableCell

            Dim oLink As System.Web.UI.HtmlControls.HtmlAnchor
            oLink = New System.Web.UI.HtmlControls.HtmlAnchor
            oLink.ID = "_t"
            oLink.Attributes("text-align") = "left"

            Dim tipos As String = ""
            Dim Nombres As String = ""
            Dim Fechas As String = ""
            For i = 0 To oAdjuntos.Data.Tables(0).Rows.Count - 1
                If tipos = "" Then
                    tipos = oAdjuntos.Data.Tables(0).Rows(i)("id").ToString & "," & oAdjuntos.Data.Tables(0).Rows(i)("tipo").ToString
                Else
                    tipos = tipos & "xx" & oAdjuntos.Data.Tables(0).Rows(i)("id").ToString & "," & oAdjuntos.Data.Tables(0).Rows(i)("tipo").ToString
                End If
                If Me.Portal = True Then
                    If (Not Nombres = "") Then Nombres = Nombres & "@xx@"
                    If (Not Fechas = "") Then Fechas = Fechas & "@xx@"
                    Nombres = Nombres & oAdjuntos.Data.Tables(0).Rows(i)("id").ToString & "@yy@" & Replace(HttpUtility.UrlEncode(oAdjuntos.Data.Tables(0).Rows(i)("NOM").ToString), "'", "\'")
                    Fechas = Fechas & oAdjuntos.Data.Tables(0).Rows(i)("id").ToString & "@yy@" & oAdjuntos.Data.Tables(0).Rows(i)("FECALTA").ToString
                End If
            Next

            Dim sTextoAdjunto As String

            If oAdjuntos.Data.Tables(0).Rows.Count = 1 Then
                sTextoAdjunto = oAdjuntos.Data.Tables(0).Rows(0)("NOM") + " (" + FSNLibrary.FormatNumber(oAdjuntos.Data.Tables(0).Rows(0)("DATASIZE"), oUser.NumberFormat) + " Kb)"
                oLink.InnerText = AjustarAnchoTextoPixels(sTextoAdjunto, 150, "Verdana", 12, False)
                If Me.Portal = True Then
                    If PortalInstancia > 0 Then
                        PortalInstancia = (-1) * PortalInstancia
                        oLink.Attributes("onclick") = "descargarAdjunto('" & oAdjuntos.Data.Tables(0).Rows(0)("id") & "', '" & oAdjuntos.Data.Tables(0).Rows(0)("tipo") & "', '" & PortalInstancia.ToString & "','" + Me.ClientID.ToString _
                            + "','" + CLng(Me.Tag).ToString + "','" + Replace(HttpUtility.UrlEncode(oAdjuntos.Data.Tables(0).Rows(0)("NOM").ToString), "'", "\'") + "','" + oAdjuntos.Data.Tables(0).Rows(0)("FECALTA").ToString() + "','" + IIf(EsAltafactura, "1", "0") + "')"
                    Else
                        oLink.Attributes("onclick") = "descargarAdjunto('" & oAdjuntos.Data.Tables(0).Rows(0)("id") & "', '" & oAdjuntos.Data.Tables(0).Rows(0)("tipo") & "', '" & Instancia.ToString & "','" + Me.ClientID.ToString _
                            + "','" + CLng(Me.Tag).ToString + "','" + Replace(HttpUtility.UrlEncode(oAdjuntos.Data.Tables(0).Rows(0)("NOM").ToString), "'", "\'") + "','" + oAdjuntos.Data.Tables(0).Rows(0)("FECALTA").ToString() + "','" + IIf(EsAltafactura, "1", "0") + "')"
                    End If
                Else
                    oLink.Attributes("onclick") = "descargarAdjunto('" & oAdjuntos.Data.Tables(0).Rows(0)("id") & "', '" & oAdjuntos.Data.Tables(0).Rows(0)("tipo") & "', '" & Instancia.ToString & "','" + Me.ClientID.ToString + "')"
                End If
            Else
                'Si tiene mas de 1 adjunto
                sTextoAdjunto = oAdjuntos.Data.Tables(0).Rows(0)("NOM") + " (" + FSNLibrary.FormatNumber(oAdjuntos.Data.Tables(0).Rows(0)("DATASIZE"), oUser.NumberFormat) + " Kb); " & oAdjuntos.Data.Tables(0).Rows(1)("NOM") + " (" + FSNLibrary.FormatNumber(oAdjuntos.Data.Tables(0).Rows(1)("DATASIZE"), oUser.NumberFormat) + " Kb)"
                oLink.InnerText = AjustarAnchoTextoPixels(sTextoAdjunto, 150, "Verdana", 12, False)
                If Me.Portal = True Then
                    If PortalInstancia > 0 Then
                        PortalInstancia = (-1) * PortalInstancia
                        oLink.Attributes("onclick") = "descargarAdjuntos('" + idAdjuntos.ToString + "', '" + idAdjuntosNew.ToString + "', '" & PortalInstancia.ToString & "', '" & tipos.ToString & "','" + Me.ClientID.ToString _
                            + "','" + CLng(Me.Tag).ToString + "','" + Nombres + "','" + Fechas.ToString + "','" + IIf(EsAltafactura, "1", "0") + "')"
                    Else
                        oLink.Attributes("onclick") = "descargarAdjuntos('" + idAdjuntos.ToString + "', '" + idAdjuntosNew.ToString + "', '" & Instancia.ToString & "', '" & tipos.ToString & "','" + Me.ClientID.ToString _
                            + "','" + CLng(Me.Tag).ToString + "','" + Nombres + "','" + Fechas.ToString + "','" + IIf(EsAltafactura, "1", "0") + "')"
                    End If
                Else
                    oLink.Attributes("onclick") = "descargarAdjuntos('" + idAdjuntos.ToString + "', '" + idAdjuntosNew.ToString + "', '" & Instancia.ToString & "', '" & tipos.ToString & "','" + Me.ClientID.ToString + "')"
                End If
            End If
            'oLink.Style("with") = "150px"
            oLink.Style("cursor") = "pointer"
            oLink.Attributes.Item("CLASS") = "enlaceAdj"
            oTblCellInput.Align = "right"
            oTblCellInput.Width = "150px"
            oTblCellInput.Controls.Add(oLink)
            oTblRowInput.Cells.Add(oTblCellInput)

            If Not _readOnly Then
                'Adjuntar
                Dim Enlace As New HtmlControls.HtmlAnchor
                Enlace.InnerText = oTextos.Rows(1).Item(1)
                Enlace.Style("cursor") = "pointer"
                Enlace.Attributes("onclick") = "show_atach_file('" + Instancia.ToString() + "', '0', '" + iTipo.ToString + "', '" + Me.ClientID.ToString + "', '" + Page.AppRelativeVirtualPath + "','0','')"
                Enlace.Style("text-decoration") = "underline"
                Enlace.Attributes.Add("class", "enlaceAdj")
                Enlace.Style("padding-left") = "7px"
                Enlace.Style("color") = "#0000FF"
                oTblCellInput = New System.Web.UI.HtmlControls.HtmlTableCell
                oTblCellInput.Controls.Add(Enlace)
                oTblCellInput.Align = "right"
                oTblCellInput.Width = "70px"
                oTblRowInput.Cells.Add(oTblCellInput)
            End If

            'Detalle

            oTblCellInput = New System.Web.UI.HtmlControls.HtmlTableCell
            Dim EnlaceDescargarTodos As New HtmlControls.HtmlAnchor
            EnlaceDescargarTodos.InnerText = oTextos.Rows(3).Item(1)
            EnlaceDescargarTodos.Style("text-decoration") = "underline"
            EnlaceDescargarTodos.Style("padding-left") = "7px"
            EnlaceDescargarTodos.Style("color") = "#0000FF"
            EnlaceDescargarTodos.Attributes.Add("class", "enlaceAdj")
            EnlaceDescargarTodos.Style("cursor") = "pointer"
            EnlaceDescargarTodos.Attributes("onclick") = "show_atach_fileDesglose('" + Instancia.ToString() + "', '" + idAdjuntos.ToString + "','" + idAdjuntosNew + "', '" + iTipo.ToString + "', '" + Me.ClientID.ToString + "')"
            oTblCellInput.Controls.Add(EnlaceDescargarTodos)
            oTblCellInput.Width = "70px"
            oTblCellInput.Align = "right"
            oTblRowInput.Cells.Add(oTblCellInput)
            oTblInput.Style.Add("table-layout", "fixed")


            oTblInput.Rows.Add(oTblRowInput)

            oTblRow = New System.Web.UI.WebControls.TableRow
            oTblCell = New System.Web.UI.WebControls.TableCell

            DivContenedor.ID = "divcontenedor"
            DivContenedor.Controls.Add(oTblInput)
            oTblCell.Controls.Add(DivContenedor)

            oTblCell.Controls.Add(oHidAct)
            oTblCell.Controls.Add(oHidNew)
            oTblCell.Controls.Add(oHidNombre)
            oTblCell.Controls.Add(oHidCampo)
            oTblCell.Controls.Add(oHidFavorito)
            oTblCell.Controls.Add(oHidInstancia)
            If Me.Portal = True Then oTblCell.Controls.Add(oHidPortalInstancia)
            oTblCell.Controls.Add(oHidTipo)
            oTblRow.Cells.Add(oTblCell)

            oTbl.Rows.Add(oTblRow)
            oTbl.ID = "_tbl"

            Me.Controls.Add(oTbl)

        End If
    End Sub
    ''' <summary>
    ''' Generar un dataentry de tipo Texto medio o texto largo 
    ''' </summary>
    ''' <param name="iTipo">Tipo de dataentry a crear: medio o largo</param>
    ''' <remarks></remarks>
    Private Sub CtrlTipoStringMedioYLargo(ByVal iTipo As FSNLibrary.TiposDeDatos.TipoGeneral)
        Dim oTbl As System.Web.UI.WebControls.Table
        Dim oTblRow As System.Web.UI.WebControls.TableRow
        Dim oTblCell As System.Web.UI.WebControls.TableCell
        Dim oHid As System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oWidth As System.Web.UI.WebControls.Unit
        Dim sClientId As String = String.Empty

        Dim FSWSServer As Object
        FSWSServer = If(Me.Portal = True, HttpContext.Current.Session("FS_Portal_Server"), HttpContext.Current.Session("FSN_Server"))

        Dim oDict As Object = FSWSServer.Get_Dictionary()
        oDict.LoadData(TiposDeDatos.ModulosIdiomas.PopUpTextosMedioYLargo, _Idi)
        Dim oTextos As DataTable = oDict.Data.Tables(0)

        oTbl = New System.Web.UI.WebControls.Table
        oHid = New System.Web.UI.HtmlControls.HtmlInputHidden

        oHid.ID = "_h"

        oWidth = Me.Width
        oTbl.Width = Unit.Percentage(100)
        If Not _Desglose Then
            oTbl.Style.Add("table-layout", "fixed")
        End If
        oTbl.BorderWidth = Unit.Pixel(0)
        oTbl.CellPadding = 0
        oTbl.CellSpacing = 0


        oTblRow = New System.Web.UI.WebControls.TableRow
        oTblCell = New System.Web.UI.WebControls.TableCell
        oTblCell.Width = Unit.Percentage(100)
        oTblCell.Attributes("nowrap") = True
        If Not Me.IdContenedor = Nothing Then
            sClientId = Me.IdContenedor + "_"
        End If
        sClientId += Me.ID

        If Not _readOnly Then
            Dim oTblInput As New System.Web.UI.WebControls.Table
            Dim oTblRowInput As System.Web.UI.WebControls.TableRow
            Dim oTblCellInput As System.Web.UI.WebControls.TableCell

            Dim oImgEditor As New System.Web.UI.WebControls.HyperLink

            oTblInput.Width = Unit.Percentage(100)
            oTblInput.CellPadding = 0
            oTblInput.CellSpacing = 0
            If Not _Desglose Then
                oTblInput.Style.Add("table-layout", "fixed")
            End If

            oTblRowInput = New System.Web.UI.WebControls.TableRow
            oTblCellInput = New System.Web.UI.WebControls.TableCell

            Dim txtCampo As New TextBox
            txtCampo.ID = "_t"
            txtCampo.TextMode = TextBoxMode.MultiLine

            If _maxLength > 0 Then
                txtCampo.MaxLength = _maxLength
            Else
                If iTipo = TiposDeDatos.TipoGeneral.TipoTextoMedio Then
                    txtCampo.MaxLength = 800
                End If
            End If

            Dim txtEventosChange As String = ""

            If Not _readOnly AndAlso Not _sFormato Is Nothing Then
                txtEventosChange += "validarFormato(this,'" & JSText(_sFormato) & "','" & _title & ". " & _errMsgFormato & "'); "
            End If

            Dim Persona As String
            If Me.Portal = True Then
                Persona = ""
            Else
                Dim oUser As Object = HttpContext.Current.Session("FSN_User")
                Persona = oUser.CodPersona.ToString
            End If
            Dim Instancia As Long
            If Not TryCast(Page.FindControl("Instancia"), HtmlControls.HtmlInputHidden) Is Nothing Then
                Try
                    Instancia = CLng(TryCast(Page.FindControl("Instancia"), HtmlControls.HtmlInputHidden).Value)
                Catch ex As Exception
                    'certif automatico tiene Hidden Instancia pero lo pone a ""
                    Instancia = 0
                End Try
            Else
                Instancia = 0
            End If

            txtCampo.Height = Unit.Pixel(15)
            txtCampo.Style("overflow") = "hidden"
            txtCampo.Attributes("onfocus") = "aumentarTamanoCampo('" & Me.UniqueID.ToString & "', " & iTipo & "," & If(_Desglose, "true", "false") & ")"
            txtCampo.Attributes("onblur") = "reducirTamanoCampo('" & Me.UniqueID.ToString & "', " & If(_Desglose, "true", "false") & ")"

            Dim sDatePattern As String = "dd/MM/yyyy"
            If Not Me.DateFormat Is Nothing Then sDatePattern = Me.DateFormat.ShortDatePattern

            If txtCampo.MaxLength > 0 Then 'Si no hay maxlength en los tipo largo no hace falta llamar a estas funciones.
                txtCampo.Attributes("onkeypress") = "return ControlLongitudStringLargo(this," & txtCampo.MaxLength & ")"
                If _lServicio > 0 Then
                    txtEventosChange += "llamarServicioExternoPorId('" + Me.ClientID.ToString + "'," + CStr(Instancia) + ",'" + Persona + "','" + sDatePattern + "'); return validarLengthyCortarStringLargo(this," & txtCampo.MaxLength & ",'" & _errMsg & "'); "
                Else
                    txtEventosChange += "return validarLengthyCortarStringLargo(this," & txtCampo.MaxLength & ",'" & _errMsg & "'); "
                End If
            ElseIf _lServicio > 0 Then
                txtEventosChange += "llamarServicioExternoPorId('" + Me.ClientID.ToString + "'," + CStr(Instancia) + ",'" + Persona + "','" + sDatePattern + "')"
            End If
            txtCampo.Width = If(_Desglose, Unit.Pixel(240), Unit.Percentage(100))
            txtCampo.Attributes("onchange") = txtEventosChange

            If Not _valor Is Nothing Then
                oHid.Value = _valor.ToString
                txtCampo.Text = _valor.ToString
            End If

            If Not _text Is Nothing Then
                txtCampo.Text = _text.ToString
            End If

            oTblCellInput.Width = Unit.Percentage(95)

            If _Desglose Then
                txtCampo.Attributes("onmouseout") = "reducirTamanoCampo('" & Me.UniqueID.ToString & "', true)"

                Dim oDivTexto As New HtmlControls.HtmlGenericControl("DIV")
                oDivTexto.Style.Add("width", "250px")
                oDivTexto.Style.Add("height", "1px")
                oDivTexto.Style.Add("float", "left")
                oDivTexto.Controls.Add(txtCampo)
                oTblCellInput.VerticalAlign = VerticalAlign.Top
                oTblCellInput.Controls.Add(oDivTexto)

                txtCampo.Style("position") = "relative"
                txtCampo.Style("top") = "-5px"
            Else
                oTblCellInput.Controls.Add(txtCampo)
            End If

            oTblInput.Rows.Add(oTblRowInput)
            oTblCellInput.Style.Add("cursor", "pointer")
            oTblRowInput.Cells.Add(oTblCellInput)

            oTblCellInput = New System.Web.UI.WebControls.TableCell
            oTblCellInput.Width = Unit.Percentage(5)
            oTblCellInput.HorizontalAlign = HorizontalAlign.Right
            oTblCellInput.VerticalAlign = VerticalAlign.Top
            oTblCellInput.Style.Add("cursor", "pointer")

            oImgEditor.ImageUrl = Me.Page.ClientScript.GetWebResourceUrl(GetType(GeneralEntry), "Fullstep.DataEntry.edit2.gif")
            oImgEditor.NavigateUrl = "javascript:void(null)"

            If _lServicio > 0 Then
                oImgEditor.Attributes("onclick") = "llamarServicioExternoPorId('" + Me.ClientID.ToString + "'," + CStr(Instancia) + ",'" + Persona + "','" + sDatePattern + "')"
            Else
                If iTipo = TiposDeDatos.TipoGeneral.TipoTextoLargo Then
                    oImgEditor.Attributes("onclick") = "show_popup(document.getElementById('" + sClientId + "__t'),document.getElementById('" + sClientId + "__t').value,'" & oTextos.Rows(0).Item(1) & "', '" & oTextos.Rows(1).Item(1) & "', '" & oTextos.Rows(2).Item(1) & "','','" & _windowTitle & "')"
                Else
                    oImgEditor.Attributes("onclick") = "show_popup_texto_medio(document.getElementById('" + sClientId + "__t'),document.getElementById('" + sClientId + "__t').value,'" & oTextos.Rows(0).Item(1) & "', '" & oTextos.Rows(1).Item(1) & "', '" & oTextos.Rows(2).Item(1) & "','','" & _windowTitle & "')"
                End If
            End If

            If RecordarValores Then
                Dim Extender As New AutoCompleteExtender()
                With Extender
                    .ID = Me.UniqueID & "$" & "autocompleteextender"
                    .CompletionSetCount = 10
                    .DelimiterCharacters = ""
                    .Enabled = True
                    .MinimumPrefixLength = 0
                    .CompletionListCssClass = "autoCompleteList"
                    .CompletionListHighlightedItemCssClass = "autoCompleteSelectedListItem"
                    .CompletionListItemCssClass = "autoCompleteListItem"
                    .ServiceMethod = "DevolverValores"
                    .ServicePath = ConfigurationManager.AppSettings("rutaPM") & "AutoCompletePMWEB.asmx"
                    .TargetControlID = txtCampo.ID
                    .EnableCaching = False
                    .UseContextKey = True
                    If Me._CampoOrigen = 0 Then
                        .ContextKey = Me.Tag
                    Else
                        .ContextKey = Me._CampoOrigen
                    End If
                    .OnClientShown = "resetPosition"
                    .CompletionInterval = 500
                End With

                oTblCellInput.Controls.Add(Extender)
            End If

            oTblCellInput.Controls.Add(oImgEditor)
            oTblRowInput.Cells.Add(oTblCellInput)
            oTblCell.Controls.Add(oTblInput)
        Else
            Dim oTblInput As New System.Web.UI.WebControls.Table
            Dim oTblRowInput As System.Web.UI.WebControls.TableRow
            Dim oTblCellInput As System.Web.UI.WebControls.TableCell
            oTblInput.Width = Unit.Percentage(100)
            If Not _Desglose Then
                oTblInput.Style.Add("table-layout", "fixed")
            End If


            oTblRowInput = New System.Web.UI.WebControls.TableRow
            oTblCellInput = New System.Web.UI.WebControls.TableCell
            Dim oDivTexto As New HtmlControls.HtmlGenericControl("DIV")
            oDivTexto.ID = "_t"
            If _Desglose Then
                oDivTexto.Style.Add("width", "375px")
            Else
                oDivTexto.Style.Add("width", "100%")
            End If
            oDivTexto.Style.Add("display", "block")
            oDivTexto.Style.Add("overflow", "hidden")
            oDivTexto.Style.Add("text-overflow", "ellipsis")
            oDivTexto.Style.Add("white-space", "nowrap")

            If Not _valor Is Nothing Then
                oHid.Value = _valor.ToString
                oDivTexto.InnerText = _valor.ToString
            End If

            If Not _text Is Nothing Then
                oDivTexto.InnerText = _text.ToString
            End If
            If DBNullToStr(_text) <> String.Empty Then
                oTblCellInput.Style.Add("cursor", "pointer")
                If iTipo = TiposDeDatos.TipoGeneral.TipoTextoLargo Then
                    oDivTexto.Attributes("onclick") = "show_popup(document.getElementById('" + sClientId + "__t'),document.getElementById('" + sClientId + "__h').value,'" & oTextos.Rows(0).Item(1) & "', '" & oTextos.Rows(1).Item(1) & "', '" & oTextos.Rows(2).Item(1) & "','','" & _windowTitle & "')"
                Else
                    oDivTexto.Attributes("onclick") = "show_popup_texto_medio(document.getElementById('" + sClientId + "__t'),document.getElementById('" + sClientId + "__h').value,'" & oTextos.Rows(0).Item(1) & "', '" & oTextos.Rows(1).Item(1) & "', '" & oTextos.Rows(2).Item(1) & "','','" & _windowTitle & "')"
                End If
            End If
            oTblCellInput.Width = Unit.Percentage(100)
            oTblCellInput.Controls.Add(oDivTexto)
            oTblInput.Rows.Add(oTblRowInput)
            oTblCellInput.Width = Unit.Percentage(100)
            oTblRowInput.Cells.Add(oTblCellInput)
            oTblCell.Controls.Add(oTblInput)
        End If

        oTblCell.Controls.Add(oHid)
        oTblRow.Cells.Add(oTblCell)

        oTbl.Rows.Add(oTblRow)
        oTbl.ID = "_tbl"
        Me.Controls.Add(oTbl)
    End Sub
    ''' <summary>
    ''' Generar un dataentry de tipo editor
    ''' </summary>
    ''' <remarks>Llamada desde: La generacion de cualquier dataentry de tipo editor; Tiempo m�ximo:0 </remarks>
    Private Sub CtrlTipoEditor()
        Dim oTbl As System.Web.UI.WebControls.Table
        Dim oTblRow As System.Web.UI.WebControls.TableRow
        Dim oTblCell As System.Web.UI.WebControls.TableCell
        Dim oAnchor As System.Web.UI.HtmlControls.HtmlAnchor
        Dim oImgEditor As System.Web.UI.HtmlControls.HtmlImage
        Dim oHid As System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oWidth As System.Web.UI.WebControls.Unit

        oTbl = New System.Web.UI.WebControls.Table
        oWidth = Me.Width
        oTbl.Width = oWidth
        oTbl.BorderWidth = Unit.Pixel(0)
        oTbl.CellPadding = 0
        oTbl.CellSpacing = 0

        oTblRow = New System.Web.UI.WebControls.TableRow
        oTblCell = New System.Web.UI.WebControls.TableCell
        oTblCell.Width = Unit.Percentage(100)
        oTblCell.Attributes("nowrap") = True

        If Not _readOnly OrElse _valor <> Nothing Then
            oAnchor = New System.Web.UI.HtmlControls.HtmlAnchor
            oAnchor.InnerText = _text
            oAnchor.ID = "_t"
            oAnchor.Attributes.Add("class", "enlaceAdj")
            oAnchor.Attributes("onclick") = "show_editor('" & _title & "','" & Me.ClientID.ToString & "'," & IIf(_readOnly, 1, 0) & ")"
            oTblCell.Controls.Add(oAnchor)

            If Not _readOnly Then
                oImgEditor = New System.Web.UI.HtmlControls.HtmlImage
                oImgEditor.Src = Me.Page.ClientScript.GetWebResourceUrl(GetType(GeneralEntry), "Fullstep.DataEntry.edit2.gif")
                oImgEditor.Attributes("hspace") = "10"
                oImgEditor.Attributes("onclick") = "show_editor('" & _title & "','" & Me.ClientID.ToString & "'," & IIf(_readOnly, 1, 0) & ")"
                oTblCell.Controls.Add(oImgEditor)
            End If
        End If

        oHid = New System.Web.UI.HtmlControls.HtmlInputHidden
        oHid.ID = "_h"
        If _valor <> "" Then
            oHid.Value = _valor
        End If

        oTblCell.Controls.Add(oHid)
        oTblRow.Cells.Add(oTblCell)

        oTbl.Rows.Add(oTblRow)
        oTbl.ID = "_tbl"
        Me.Controls.Add(oTbl)
    End Sub

    Private Sub CtrlTipoEnlace()


        Dim DivGeneral As New HtmlControls.HtmlGenericControl("DIV")

        Dim oTblInput As System.Web.UI.HtmlControls.HtmlTable
        Dim oTblCellInput As System.Web.UI.HtmlControls.HtmlTableCell
        Dim oTblRowInput As System.Web.UI.HtmlControls.HtmlTableRow
        Dim oHidTipo As New System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oHidCampo As New System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oHidInstancia As New System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oHidPortalInstancia As New System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oWidth As System.Web.UI.WebControls.Unit

        oTblInput = New System.Web.UI.HtmlControls.HtmlTable
        oTblRowInput = New System.Web.UI.HtmlControls.HtmlTableRow
        oTblCellInput = New System.Web.UI.HtmlControls.HtmlTableCell

        oTblInput.ID = "_tbl"
        oTblInput.Width = oWidth.ToString
        oTblInput.CellPadding = 0
        oTblInput.CellSpacing = 0
        oTblInput.Border = 0

        'Definimos el campo ENLACE
        oTblCellInput = New System.Web.UI.HtmlControls.HtmlTableCell
        Dim Enlace As New HtmlControls.HtmlAnchor
        Enlace.ID = "_t"
        Enlace.InnerText = _text
        If Not Me.Portal = True Then
            Enlace.Attributes("onclick") = "show_link('" & _valor & "')"
            Enlace.Attributes.Add("class", "enlaceAdj")
            Enlace.Style("color") = "#0000FF"
            Enlace.Style("cursor") = "pointer"
        End If

        oTblCellInput.Controls.Add(Enlace)

        oTblRowInput.Cells.Add(oTblCellInput)

        oTblInput.Rows.Add(oTblRowInput)
        Me.Controls.Add(oTblInput)

    End Sub

    ''' <summary>
    ''' Generar un dataentry de tipo archivo de contrato(Se guarda con FILESTREAM)
    ''' </summary>
    ''' <remarks>Llamada desde: La generacion de cualquier dataentry de tipo editor; Tiempo m�ximo:0 </remarks>
    Private Sub CtrlTipoArchivoContrato()
        Dim DivGeneral As New HtmlControls.HtmlGenericControl("DIV")
        Dim oTbl As System.Web.UI.WebControls.Table
        Dim oTblRow As System.Web.UI.WebControls.TableRow
        Dim oTblCell As System.Web.UI.WebControls.TableCell
        Dim oTblInput As System.Web.UI.HtmlControls.HtmlTable
        Dim oTblCellInput As System.Web.UI.HtmlControls.HtmlTableCell
        Dim oTblRowInput As System.Web.UI.HtmlControls.HtmlTableRow
        Dim oTblOpciones As System.Web.UI.HtmlControls.HtmlTable
        Dim oTblCellOpciones As System.Web.UI.HtmlControls.HtmlTableCell
        Dim oTblRowOpciones As System.Web.UI.HtmlControls.HtmlTableRow
        Dim oHidAct As System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oHidNew As System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oHidNombre As System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oHidTipo As New System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oHidFavorito As New System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oHidCampo As New System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oHidInstancia As New System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oHidID As New System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oBut As New System.Web.UI.HtmlControls.HtmlInputButton
        Dim bSoloUnArchivo As Boolean = False
        Dim oWidth As System.Web.UI.WebControls.Unit
        Dim FSWSServer As Object
        Dim oAdjuntos As Object
        Dim lCiaComp As Long

        If Me.Portal = True Then
            lCiaComp = HttpContext.Current.Session("FS_Portal_CiaComp")

            FSWSServer = HttpContext.Current.Session("FS_Portal_Server")
        Else
            FSWSServer = HttpContext.Current.Session("FSN_Server")
        End If

        oAdjuntos = FSWSServer.Get_Adjuntos()

        Dim idCampo As Integer
        Dim idAdjuntos As String = String.Empty
        Dim idAdjuntosNew As String = String.Empty
        Dim NombreAdjunto As String = String.Empty
        Dim Instancia As Long
        Dim bFavorito As Boolean
        Dim iTipo As Integer

        Dim oUser As Object

        If Me.Portal = True Then
            oUser = HttpContext.Current.Session("FS_Portal_User")
        Else
            oUser = HttpContext.Current.Session("FSN_User")
        End If

        Dim sIdi As String = oUser.Idioma.ToString()
        If sIdi = Nothing Then
            sIdi = ConfigurationManager.AppSettings("idioma")
        End If

        Dim oDict As Object = FSWSServer.Get_Dictionary()

        oDict.LoadData(TiposDeDatos.ModulosIdiomas.Adjuntos, sIdi)
        Dim oTextos As DataTable = oDict.Data.Tables(0)

        If Me._CampoOrigen = 0 Then
            idCampo = CInt(Me.Tag)
        Else
            idCampo = CInt(Me._CampoOrigen)
        End If

        'Hidden de campo
        oHidCampo.ID = "_hCampo"
        oHidCampo.Value = idCampo

        If _valor <> "" Then
            If InStr(_valor, "####") Then
                idAdjuntos = Split(_valor, "####")(0)
                idAdjuntosNew = Split(_valor, "####")(1)
            Else
                idAdjuntos = _valor
            End If
            If Not _text Is Nothing Then
                NombreAdjunto = _text.ToString
            End If
            If _valor <> "" AndAlso _valor <> "####" AndAlso Not (InStr(_valor, "xx") > 0) Then
                bSoloUnArchivo = True
            End If
        End If

        'Obtenemos lo adjuntos
        If Me.Portal = True Then
            oAdjuntos.LoadInst_Adj_Contrato(lCiaComp, Me.IdContrato, Me.IdArchivoContrato, CLng(Me.Tag))
        Else
            oAdjuntos.LoadInst_Adj_Contrato(Me.IdContrato, Me.IdArchivoContrato, CLng(Me.Tag))
        End If

        'Hidden de adjunto
        oHidAct = New System.Web.UI.HtmlControls.HtmlInputHidden
        oHidAct.ID = "_hAct"
        oHidAct.Value = IIf(Me.IdArchivoContrato = 0, "", Me.IdArchivoContrato)

        'Hidden de nuevo adjunto
        oHidNew = New System.Web.UI.HtmlControls.HtmlInputHidden
        oHidNew.ID = "_hNew"
        oHidNew.Value = ""

        'Hidden de nombre de adjuntos
        oHidNombre = New System.Web.UI.HtmlControls.HtmlInputHidden
        oHidNombre.ID = "_hNombre"
        oHidNombre.Value = _text

        If idAdjuntos <> "" Then
            idAdjuntos = Replace(idAdjuntos, "xx", ",")
        End If
        If idAdjuntosNew <> "" Then
            idAdjuntosNew = Replace(idAdjuntosNew, "xx", ",")
        End If

        If Not TryCast(Page.FindControl("Instancia"), HtmlControls.HtmlInputHidden) Is Nothing Then
            Try
                Instancia = CLng(TryCast(Page.FindControl("Instancia"), HtmlControls.HtmlInputHidden).Value)
            Catch ex As Exception
                'certif automatico tiene Hidden Instancia pero lo pone a ""
                Instancia = 0
            End Try
        Else
            Instancia = 0
        End If
        'Hidden de la instancia
        oHidInstancia = New System.Web.UI.HtmlControls.HtmlInputHidden
        oHidInstancia.ID = "_hInstancia"
        oHidInstancia.Value = Instancia

        If Not TryCast(Page.FindControl("Favorito"), HtmlControls.HtmlInputHidden) Is Nothing Then
            bFavorito = IIf(TryCast(Page.FindControl("Favorito"), HtmlControls.HtmlInputHidden).Value = "1", True, False)
        Else
            bFavorito = False
        End If

        'el archivo de contrato se adjunta en un campo simple
        iTipo = 5

        'Hidden del tipo
        oHidTipo = New System.Web.UI.HtmlControls.HtmlInputHidden
        oHidTipo.ID = "_hTipo"
        oHidTipo.Value = iTipo

        oTbl = New System.Web.UI.WebControls.Table
        oWidth = IIf(Me.Width.IsEmpty, Unit.Percentage(100), Me.Width)
        oTbl.Width = oWidth
        oTbl.CellPadding = 0
        oTbl.CellSpacing = 0

        oTblInput = New System.Web.UI.HtmlControls.HtmlTable
        oTblRowInput = New System.Web.UI.HtmlControls.HtmlTableRow
        oTblCellInput = New System.Web.UI.HtmlControls.HtmlTableCell

        oTblInput.Width = oWidth.ToString
        oTblInput.CellPadding = 0
        oTblInput.CellSpacing = 0
        oTblInput.Border = 0

        If oAdjuntos.Data.Tables.Count = 0 OrElse oAdjuntos.Data.Tables(0).Rows.Count = 0 Then
            'SI NO TIENE ADJUNTOS
            oTblCellInput = New System.Web.UI.HtmlControls.HtmlTableCell
            If Not _readOnly Then
                Dim Enlace As New HtmlControls.HtmlAnchor
                Enlace.InnerText = oTextos.Rows(1).Item(1)
                Enlace.Attributes("onclick") = "show_atach_file_Contrato('" + Instancia.ToString() + "', 0 , '" + Me.ClientID.ToString + "','" + Page.AppRelativeVirtualPath + "','" & IdContrato.ToString() & "','')"
                Enlace.Style("text-decoration") = "underline"
                Enlace.Attributes.Add("class", "enlaceAdj")
                Enlace.Style("color") = "#0000FF"
                Enlace.Style("cursor") = "pointer"
                oTblCellInput.Controls.Add(Enlace)
            End If

            Dim oLink As System.Web.UI.HtmlControls.HtmlAnchor
            oLink = New System.Web.UI.HtmlControls.HtmlAnchor
            oLink.ID = "_t"
            oLink.Style.Item("display") = "none"
            oLink.Style.Item("width") = "100%"
            'oLink.Attributes.Item("CLASS") = "TipoArchivo"
            oLink.HRef = "#"
            oTblCellInput.Align = "right"

            oTblCellInput.Controls.Add(oLink)

            oTblRowInput.Cells.Add(oTblCellInput)

            oTblInput.Rows.Add(oTblRowInput)

            oTblRow = New System.Web.UI.WebControls.TableRow
            oTblCell = New System.Web.UI.WebControls.TableCell

            DivGeneral.ID = "divcontenedor"
            DivGeneral.Controls.Add(oTblInput)
            oTblCell.Controls.Add(DivGeneral)

            oTblCell.Controls.Add(oHidAct)
            oTblCell.Controls.Add(oHidNew)
            oTblCell.Controls.Add(oHidNombre)
            oTblCell.Controls.Add(oHidCampo)
            oTblCell.Controls.Add(oHidFavorito)
            oTblCell.Controls.Add(oHidInstancia)
            oTblCell.Controls.Add(oHidTipo)
            oTblRow.Cells.Add(oTblCell)

            oTbl.Rows.Add(oTblRow)
            oTbl.ID = "_tbl"

            Me.Controls.Add(oTbl)
        Else
            'SI TIENE ADJUNTOS
            oTblOpciones = New System.Web.UI.HtmlControls.HtmlTable
            oTblRowOpciones = New System.Web.UI.HtmlControls.HtmlTableRow
            oTblCellOpciones = New System.Web.UI.HtmlControls.HtmlTableCell

            oTblOpciones.CellPadding = 3
            oTblOpciones.CellSpacing = 0
            oTblOpciones.Border = 0
            oTblOpciones.Align = "right"
            oTblOpciones.Style.Item("display") = "inline"

            oTblCellInput = New System.Web.UI.HtmlControls.HtmlTableCell

            Dim oLink As System.Web.UI.HtmlControls.HtmlAnchor
            oLink = New System.Web.UI.HtmlControls.HtmlAnchor
            oLink.ID = "_t"
            oLink.Style.Item("width") = "100%"
            'oLink.Attributes.Item("CLASS") = "TipoArchivo"
            oLink.Style.Item("display") = "none"
            oLink.HRef = "#"
            oTblCellOpciones.Controls.Add(oLink)


            'oTblCellInput.Align = "right"
            oTblRowOpciones.Cells.Add(oTblCellOpciones)

            If oAdjuntos.Data.Tables(0).Rows.Count > 1 Then
                'Si tiene mas de un adjunto
                'Descargar Todos
                Dim tipos As String = ""
                Dim Nombres As String = ""
                Dim Fechas As String = ""
                For i = 0 To oAdjuntos.Data.Tables(0).Rows.Count - 1
                    If tipos = "" Then
                        tipos = oAdjuntos.Data.Tables(0).Rows(i)("id").ToString & ",1"
                    Else
                        tipos = tipos & "xx" & oAdjuntos.Data.Tables(0).Rows(i)("id").ToString & ",1"
                    End If
                    If Me.Portal = True Then
                        If (Not Nombres = "") Then Nombres = Nombres & "@xx@"
                        If (Not Fechas = "") Then Fechas = Fechas & "@xx@"
                        Nombres = Nombres & oAdjuntos.Data.Tables(0).Rows(i)("id").ToString & "@yy@" & Replace(HttpUtility.UrlEncode(oAdjuntos.Data.Tables(0).Rows(i)("NOM").ToString), "'", "\'")
                        Fechas = Fechas & oAdjuntos.Data.Tables(0).Rows(i)("id").ToString & "@yy@" & oAdjuntos.Data.Tables(0).Rows(i)("FECALTA").ToString
                    End If
                Next
                oTblCellOpciones = New System.Web.UI.HtmlControls.HtmlTableCell
                Dim EnlaceDescargarTodos As New HtmlControls.HtmlAnchor
                EnlaceDescargarTodos.InnerText = oTextos.Rows(4).Item(1)
                EnlaceDescargarTodos.Style("text-decoration") = "underline"
                EnlaceDescargarTodos.Attributes.Add("class", "enlaceAdj")
                EnlaceDescargarTodos.Style("color") = "#0000FF"
                EnlaceDescargarTodos.Style("cursor") = "pointer"
                If Me.Portal = True Then
                    EnlaceDescargarTodos.Attributes("onclick") = "descargarAdjuntos('" + idAdjuntos.ToString + "', '" + idAdjuntosNew.ToString + "', '" & Instancia.ToString & "', '" & tipos.ToString & "','" + Me.ClientID.ToString _
                        + "','" + CLng(Me.Tag).ToString + "','" + Nombres + "','" + Fechas.ToString + "','0')"
                Else
                    EnlaceDescargarTodos.Attributes("onclick") = "descargarAdjuntos('" + idAdjuntos.ToString + "', '" + idAdjuntosNew.ToString + "', '" & Instancia.ToString & "', '" & tipos.ToString & "','" + Me.ClientID.ToString + "')"
                End If
                oTblCellOpciones.Controls.Add(EnlaceDescargarTodos)
                'oTblCellInput.Align = "right"
                oTblRowOpciones.Cells.Add(oTblCellOpciones)

                'Eliminar Todos
                If Not _readOnly Then
                    oTblCellOpciones = New System.Web.UI.HtmlControls.HtmlTableCell
                    Dim EnlaceEliminarTodos As New HtmlControls.HtmlAnchor
                    EnlaceEliminarTodos.InnerText = oTextos.Rows(6).Item(1)
                    EnlaceEliminarTodos.Style("text-decoration") = "underline"
                    EnlaceEliminarTodos.Attributes.Add("class", "enlaceAdj")
                    EnlaceEliminarTodos.Style("color") = "#0000FF"
                        EnlaceEliminarTodos.Style("cursor") = "pointer"
                        EnlaceEliminarTodos.Attributes("onclick") = "eliminarAdjuntos('" + Me.ClientID.ToString + "')"
                        oTblCellOpciones.Controls.Add(EnlaceEliminarTodos)
                        oTblCellOpciones.Align = "right"
                        oTblRowOpciones.Cells.Add(oTblCellOpciones)
                    End If
                End If

                oTblOpciones.Rows.Add(oTblRowOpciones)

            oTblCellInput.Controls.Add(oTblOpciones)
            oTblRowInput.Cells.Add(oTblCellInput)
            oTblInput.Rows.Add(oTblRowInput)

            oTblRow = New System.Web.UI.WebControls.TableRow
            oTblCell = New System.Web.UI.WebControls.TableCell


            DivGeneral.ID = "divcontenedor"
            DivGeneral.Controls.Add(oTblInput)
            oTblCell.Controls.Add(DivGeneral)

            oTblCell.Controls.Add(oHidAct)
            oTblCell.Controls.Add(oHidNew)
            oTblCell.Controls.Add(oHidNombre)
            oTblCell.Controls.Add(oHidCampo)
            oTblCell.Controls.Add(oHidFavorito)
            oTblCell.Controls.Add(oHidInstancia)
            oTblCell.Controls.Add(oHidTipo)
            oTblRow.Cells.Add(oTblCell)

            oTbl.Rows.Add(oTblRow)
            oTbl.ID = "_tbl"

            Me.Controls.Add(oTbl)

            'Cargo los adjuntos
            For i = 0 To oAdjuntos.Data.Tables(0).Rows.Count - 1
                A�adirAdjuntoContrato(oAdjuntos.Data.Tables(0).Rows(i), Instancia, iTipo, CLng(Me.Tag))
            Next
        End If
    End Sub
    ''' <summary>
    ''' Generar un dataentry de tipo archivo
    ''' </summary>
    ''' <remarks>Llamada desde: La generacion de cualquier dataentry de tipo Archivo; Tiempo m�ximo:0 </remarks>
    Private Sub CtrlTipoArchivo()

        If Me.TipoGS = TiposDeDatos.TipoCampoGS.ArchivoContrato Then
            CtrlTipoArchivoContrato()
            Exit Sub
        End If

        Dim DivGeneral As New HtmlControls.HtmlGenericControl("DIV")
        Dim oTbl As System.Web.UI.WebControls.Table
        Dim oTblRow As System.Web.UI.WebControls.TableRow
        Dim oTblCell As System.Web.UI.WebControls.TableCell
        Dim oTblInput As System.Web.UI.HtmlControls.HtmlTable
        Dim oTblCellInput As System.Web.UI.HtmlControls.HtmlTableCell
        Dim oTblRowInput As System.Web.UI.HtmlControls.HtmlTableRow
        Dim oTblOpciones As System.Web.UI.HtmlControls.HtmlTable
        Dim oTblCellOpciones As System.Web.UI.HtmlControls.HtmlTableCell
        Dim oTblRowOpciones As System.Web.UI.HtmlControls.HtmlTableRow
        Dim oHidAct As System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oHidNew As System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oHidNombre As System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oHidTipo As New System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oHidFavorito As New System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oHidCampo As New System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oHidInstancia As New System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oHidPortalInstancia As New System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oHidID As New System.Web.UI.HtmlControls.HtmlInputHidden


        Dim oBut As New System.Web.UI.HtmlControls.HtmlInputButton
        Dim bSoloUnArchivo As Boolean = False
        Dim oWidth As System.Web.UI.WebControls.Unit

        Dim FSWSServer As Object
        Dim oAdjuntos As Object

        Dim lCiaComp As Long

        If Me.Portal = True Then
            lCiaComp = HttpContext.Current.Session("FS_Portal_CiaComp")

            FSWSServer = HttpContext.Current.Session("FS_Portal_Server")
        Else
            FSWSServer = HttpContext.Current.Session("FSN_Server")
        End If

        oAdjuntos = FSWSServer.Get_Adjuntos()

        Dim idCampo As Integer
        Dim idAdjuntos As String = String.Empty
        Dim idAdjuntosNew As String = String.Empty
        Dim NombreAdjunto As String = String.Empty
        Dim Instancia As Long
        Dim PortalInstancia As Long = 0
        Dim EsAltafactura As Boolean = False
        Dim bFavorito As Boolean
        Dim iTipo As Integer

        Dim oUser As Object

        If Me.Portal = True Then
            oUser = HttpContext.Current.Session("FS_Portal_User")
        Else
            oUser = HttpContext.Current.Session("FSN_User")
        End If

        Dim sIdi As String = oUser.Idioma.ToString()
        If sIdi = Nothing Then
            sIdi = ConfigurationManager.AppSettings("idioma")
        End If

        Dim oDict As Object = FSWSServer.Get_Dictionary()

        oDict.LoadData(TiposDeDatos.ModulosIdiomas.Adjuntos, sIdi)
        Dim oTextos As DataTable = oDict.Data.Tables(0)

        If Me._CampoOrigen = 0 Then
            idCampo = CInt(Me.Tag)
        Else
            idCampo = CInt(Me._CampoOrigen)
        End If

        'Hidden de campo
        oHidCampo.ID = "_hCampo"
        oHidCampo.Value = idCampo

        If _valor <> "" Then
            If InStr(_valor, "####") Then
                idAdjuntos = Split(_valor, "####")(0)
                idAdjuntosNew = Split(_valor, "####")(1)
            Else
                idAdjuntos = _valor
            End If
            If Not _text Is Nothing Then
                NombreAdjunto = _text.ToString
            End If
            If _valor <> "" AndAlso _valor <> "####" AndAlso Not (InStr(_valor, "xx") > 0) Then
                bSoloUnArchivo = True
            End If
        End If



        'Hidden de adjunto
        oHidAct = New System.Web.UI.HtmlControls.HtmlInputHidden
        oHidAct.ID = "_hAct"
        oHidAct.Value = idAdjuntos

        'Hidden de nuevo adjunto
        oHidNew = New System.Web.UI.HtmlControls.HtmlInputHidden
        oHidNew.ID = "_hNew"
        oHidNew.Value = idAdjuntosNew

        'Hidden de nombre de adjuntos
        oHidNombre = New System.Web.UI.HtmlControls.HtmlInputHidden
        oHidNombre.ID = "_hNombre"
        oHidNombre.Value = _text

        If idAdjuntos <> "" Then
            idAdjuntos = Replace(idAdjuntos, "xx", ",")
        End If
        If idAdjuntosNew <> "" Then
            idAdjuntosNew = Replace(idAdjuntosNew, "xx", ",")
        End If


        If Not TryCast(Page.FindControl("Instancia"), HtmlControls.HtmlInputHidden) Is Nothing Then
            Try
                Instancia = CLng(TryCast(Page.FindControl("Instancia"), HtmlControls.HtmlInputHidden).Value)
            Catch ex As Exception
                Instancia = 0
            End Try
            Try
                'Anteriormente: certif automatico tiene Hidden Instancia pero lo pone a ""
                'Tras tarea Solic Autom, no haya copia_campo_adjun pero si instancia -> no llega "" -> intenta ir a copia_campo_adjun
                If CStr(TryCast(Page.FindControl("Version"), HtmlControls.HtmlInputHidden).Value) = "" Then
                    If Me.Portal Then PortalInstancia = Instancia
                    Instancia = 0
                End If
            Catch ex As Exception
            End Try
        Else 'Portal: Alta factura
            If Me.Portal AndAlso (Not TryCast(Page.FindControl("instanciaPortF"), HtmlControls.HtmlInputHidden) Is Nothing) Then
                PortalInstancia = CLng(TryCast(Page.FindControl("instanciaPortF"), HtmlControls.HtmlInputHidden).Value)
                'Sabes q estas en alta facturas
                EsAltafactura = True
                PortalInstancia = PortalInstancia * (-1)
            End If

            Instancia = 0
        End If
        'Hidden de la instancia
        oHidInstancia = New System.Web.UI.HtmlControls.HtmlInputHidden
        oHidInstancia.ID = "_hInstancia"
        oHidInstancia.Value = Instancia

        If Me.Portal = True Then
            oHidPortalInstancia = New System.Web.UI.HtmlControls.HtmlInputHidden
            oHidPortalInstancia.ID = IIf(EsAltafactura, "_hInstanciaPortF", "_hInstanciaPort")
            oHidPortalInstancia.Value = (-1) * PortalInstancia
        End If

        If Not TryCast(Page.FindControl("Favorito"), HtmlControls.HtmlInputHidden) Is Nothing Then
            bFavorito = IIf(TryCast(Page.FindControl("Favorito"), HtmlControls.HtmlInputHidden).Value = "1", True, False)
        Else
            bFavorito = False
        End If

        'Definimos el tipo de campo
        If _Desglose Then
            'Campo desglose
            iTipo = 3
        Else
            'Campo simple
            iTipo = 1
        End If

        'Hidden del tipo
        oHidTipo = New System.Web.UI.HtmlControls.HtmlInputHidden
        oHidTipo.ID = "_hTipo"
        oHidTipo.Value = iTipo

        'Obtenemos lo adjuntos
        If Not (idAdjuntos = "" AndAlso idAdjuntosNew = "") Then
            If Me.Portal = True Then
                If Instancia = 0 Then
                    oAdjuntos.Load(lCiaComp, iTipo, idAdjuntos, idAdjuntosNew, sIdi)
                Else
                    oAdjuntos.LoadInst(lCiaComp, iTipo, idAdjuntos, idAdjuntosNew)
                    'Si es una adjunto metido desde A�adir por valor por defecto. La informaci�n esta en LINEA_DESGLOSE_ADJUN
                    If oAdjuntos.Data.Tables(0).Rows.Count = 0 Then
                        oAdjuntos.Load(lCiaComp, _tipo, idAdjuntos, idAdjuntosNew)
                    End If
                End If
            Else
                If Instancia = 0 Then
                    If idAdjuntos = "-1" Then 'Archivo adjunto Contrato --> Desde el wizard del GS
                    Else
                        oAdjuntos.Load(iTipo, idAdjuntos, idAdjuntosNew, bFavorito)
                    End If
                Else
                    oAdjuntos.LoadInst(iTipo, idAdjuntos, idAdjuntosNew)
                    'Si es una adjunto metido desde A�adir por valor por defecto. La informaci�n esta en LINEA_DESGLOSE_ADJUN
                    If oAdjuntos.Data.Tables(0).Rows.Count = 0 Then
                        oAdjuntos.Load(_tipo, idAdjuntos, idAdjuntosNew)
                    End If
                End If
            End If
        End If

        oTbl = New System.Web.UI.WebControls.Table
        oWidth = IIf(Me.Width.IsEmpty, Unit.Percentage(100), Me.Width)
        oTbl.Width = oWidth
        oTbl.CellPadding = 0
        oTbl.CellSpacing = 0


        oTblInput = New System.Web.UI.HtmlControls.HtmlTable
        oTblRowInput = New System.Web.UI.HtmlControls.HtmlTableRow
        oTblCellInput = New System.Web.UI.HtmlControls.HtmlTableCell

        oTblInput.Width = oWidth.ToString
        oTblInput.CellPadding = 0
        oTblInput.CellSpacing = 0
        oTblInput.Border = 0

        If idAdjuntos = "-1" Then
            oTblCellInput = New System.Web.UI.HtmlControls.HtmlTableCell

            'A�adir el archivo contrato

            If _text <> "" Then
                '--Si tiene algun contrato adjunto
                Dim EnlaceDescargarTodos As New HtmlControls.HtmlAnchor
                EnlaceDescargarTodos.InnerText = _text & " " & _TamanyoContrato
                EnlaceDescargarTodos.Style("text-decoration") = "underline"
                EnlaceDescargarTodos.Attributes.Add("class", "enlaceAdj")
                EnlaceDescargarTodos.Style("color") = "#0000FF"
                EnlaceDescargarTodos.Style("cursor") = "pointer"
                EnlaceDescargarTodos.ID = -1
                'Aqui no llega. Ver al principio funci�n If Me.TipoGS = TiposDeDatos.TipoCampoGS.ArchivoContrato Then
                EnlaceDescargarTodos.Attributes("onclick") = "descargarAdjuntoContrato('" + Instancia.ToString() + "','" & IdContrato.ToString & "',0,'" & _text & "')"
                oTblCellInput.Controls.Add(EnlaceDescargarTodos)
            Else
                Dim lblFichero As New Label
                'Enlace fichero
                lblFichero.Text = _text
                lblFichero.ID = -1
                oTblCellInput.Controls.Add(lblFichero)
            End If



            Dim inputID As New HtmlControls.HtmlInputHidden
            inputID.ID = "Adj_" & "-1"
            inputID.Value = "-1" & "###" & "1" & "###" & Instancia.ToString
            oTblCellInput.Align = "left"


            oTblCellInput.Controls.Add(inputID)
            oTblRowInput.Cells.Add(oTblCellInput)

            oTblCellInput = New System.Web.UI.HtmlControls.HtmlTableCell
            If Not _readOnly Then
                Dim Enlace As New HtmlControls.HtmlAnchor
                Enlace.InnerText = oTextos.Rows(1).Item(1)
                Enlace.Attributes("onclick") = "show_atach_file('" + Instancia.ToString() + "', '" + idAdjuntos.ToString + "', '" + iTipo.ToString + "', '" + Me.ClientID.ToString + "','" + Page.AppRelativeVirtualPath + "','" & IdContrato.ToString() & "')"
                Enlace.Style("text-decoration") = "underline"
                Enlace.Attributes.Add("class", "enlaceAdj")
                Enlace.Style("color") = "#0000FF"
                Enlace.Style("cursor") = "pointer"
                oTblCellInput.Controls.Add(Enlace)
            End If

            Dim oLink As System.Web.UI.HtmlControls.HtmlAnchor
            oLink = New System.Web.UI.HtmlControls.HtmlAnchor
            oLink.ID = "_t"
            oLink.Style.Item("display") = "none"
            oLink.Style.Item("width") = "100%"
            'oLink.Attributes.Item("CLASS") = "TipoArchivo"
            oLink.HRef = "#"
            oTblCellInput.Align = "right"



            oTblCellInput.Controls.Add(oLink)


            oTblRowInput.Cells.Add(oTblCellInput)


            oTblInput.Rows.Add(oTblRowInput)

            oTblRow = New System.Web.UI.WebControls.TableRow
            oTblCell = New System.Web.UI.WebControls.TableCell

            DivGeneral.ID = "divcontenedor"
            DivGeneral.Controls.Add(oTblInput)
            oTblCell.Controls.Add(DivGeneral)

            oTblCell.Controls.Add(oHidAct)
            oTblCell.Controls.Add(oHidNew)
            oTblCell.Controls.Add(oHidNombre)
            oTblCell.Controls.Add(oHidCampo)
            oTblCell.Controls.Add(oHidFavorito)
            oTblCell.Controls.Add(oHidInstancia)
            If Me.Portal = True Then oTblCell.Controls.Add(oHidPortalInstancia)
            oTblCell.Controls.Add(oHidTipo)
            oTblRow.Cells.Add(oTblCell)

            oTbl.Rows.Add(oTblRow)
            oTbl.ID = "_tbl"

            Me.Controls.Add(oTbl)

        Else
            If oAdjuntos.Data Is Nothing OrElse (oAdjuntos.Data.Tables.Count = 0 AndAlso oAdjuntos.Data.Tables(0).Rows.Count = 0) Then
                'SI NO TIENE ADJUNTOS
                oTblCellInput = New System.Web.UI.HtmlControls.HtmlTableCell
                If Not _readOnly Then
                    Dim Enlace As New HtmlControls.HtmlAnchor
                    Enlace.InnerText = oTextos.Rows(1).Item(1)
                    Enlace.Attributes("onclick") = "show_atach_file('" + Instancia.ToString() + "', '" + idAdjuntos.ToString + "', '" + iTipo.ToString + "', '" + Me.ClientID.ToString + "','" + Page.AppRelativeVirtualPath + "','0')"
                    Enlace.Style("text-decoration") = "underline"
                    Enlace.Attributes.Add("class", "enlaceAdj")
                    Enlace.Style("color") = "#0000FF"
                    Enlace.Style("cursor") = "pointer"
                    oTblCellInput.Controls.Add(Enlace)
                End If

                Dim oLink As System.Web.UI.HtmlControls.HtmlAnchor
                oLink = New System.Web.UI.HtmlControls.HtmlAnchor
                oLink.ID = "_t"
                oLink.Style.Item("display") = "none"
                oLink.Style.Item("width") = "100%"
                'oLink.Attributes.Item("CLASS") = "TipoArchivo"
                oLink.HRef = "#"
                oTblCellInput.Align = "right"



                oTblCellInput.Controls.Add(oLink)


                oTblRowInput.Cells.Add(oTblCellInput)

                oTblInput.Rows.Add(oTblRowInput)

                oTblRow = New System.Web.UI.WebControls.TableRow
                oTblCell = New System.Web.UI.WebControls.TableCell

                DivGeneral.ID = "divcontenedor"
                DivGeneral.Controls.Add(oTblInput)
                oTblCell.Controls.Add(DivGeneral)

                oTblCell.Controls.Add(oHidAct)
                oTblCell.Controls.Add(oHidNew)
                oTblCell.Controls.Add(oHidNombre)
                oTblCell.Controls.Add(oHidCampo)
                oTblCell.Controls.Add(oHidFavorito)
                oTblCell.Controls.Add(oHidInstancia)
                If Me.Portal = True Then oTblCell.Controls.Add(oHidPortalInstancia)
                oTblCell.Controls.Add(oHidTipo)
                oTblRow.Cells.Add(oTblCell)

                oTbl.Rows.Add(oTblRow)
                oTbl.ID = "_tbl"

                Me.Controls.Add(oTbl)
            Else
                'SI TIENE ADJUNTOS
                oTblOpciones = New System.Web.UI.HtmlControls.HtmlTable
                oTblRowOpciones = New System.Web.UI.HtmlControls.HtmlTableRow
                oTblCellOpciones = New System.Web.UI.HtmlControls.HtmlTableCell

                oTblOpciones.CellPadding = 3
                oTblOpciones.CellSpacing = 0
                oTblOpciones.Border = 0
                oTblOpciones.Align = "right"
                oTblOpciones.Style.Item("display") = "inline"

                oTblCellInput = New System.Web.UI.HtmlControls.HtmlTableCell
                If Not _readOnly Then
                    Dim Enlace As New HtmlControls.HtmlAnchor
                    Enlace.InnerText = oTextos.Rows(1).Item(1)
                    Enlace.Attributes("onclick") = "show_atach_file('" + Instancia.ToString() + "', '0', '" + iTipo.ToString + "', '" + Me.ClientID.ToString + "','" + Page.AppRelativeVirtualPath + "','0')"
                    Enlace.Style("text-decoration") = "underline"
                    Enlace.Attributes.Add("class", "enlaceAdj")
                    Enlace.Style("color") = "#0000FF"
                    Enlace.Style("cursor") = "pointer"
                    oTblCellOpciones.Controls.Add(Enlace)
                End If

                Dim oLink As System.Web.UI.HtmlControls.HtmlAnchor
                oLink = New System.Web.UI.HtmlControls.HtmlAnchor
                oLink.ID = "_t"
                oLink.Style.Item("width") = "100%"
                oLink.Style.Item("display") = "none"
                oLink.HRef = "#"
                oTblCellOpciones.Controls.Add(oLink)

                oTblRowOpciones.Cells.Add(oTblCellOpciones)

                If oAdjuntos.Data.Tables(0).Rows.Count > 1 Then
                    'Si tiene mas de un adjunto

                    'Descargar Todos
                    Dim tipos As String = ""
                    Dim Nombres As String = ""
                    Dim Fechas As String = ""
                    For i = 0 To oAdjuntos.Data.Tables(0).Rows.Count - 1
                        If tipos = "" Then
                            tipos = oAdjuntos.Data.Tables(0).Rows(i)("id").ToString & "," & oAdjuntos.Data.Tables(0).Rows(i)("tipo").ToString
                        Else
                            tipos = tipos & "xx" & oAdjuntos.Data.Tables(0).Rows(i)("id").ToString & "," & oAdjuntos.Data.Tables(0).Rows(i)("tipo").ToString
                        End If
                        If Me.Portal = True Then
                            If (Not Nombres = "") Then Nombres = Nombres & "@xx@"
                            If (Not Fechas = "") Then Fechas = Fechas & "@xx@"
                            Nombres = Nombres & oAdjuntos.Data.Tables(0).Rows(i)("id").ToString & "@yy@" & Replace(HttpUtility.UrlEncode(oAdjuntos.Data.Tables(0).Rows(i)("NOM").ToString), "'", "\'")
                            Fechas = Fechas & oAdjuntos.Data.Tables(0).Rows(i)("id").ToString & "@yy@" & oAdjuntos.Data.Tables(0).Rows(i)("FECALTA").ToString
                        End If
                    Next
                    oTblCellOpciones = New System.Web.UI.HtmlControls.HtmlTableCell
                    Dim EnlaceDescargarTodos As New HtmlControls.HtmlAnchor
                    EnlaceDescargarTodos.InnerText = oTextos.Rows(4).Item(1)
                    EnlaceDescargarTodos.Style("text-decoration") = "underline"
                    EnlaceDescargarTodos.Attributes.Add("class", "enlaceAdj")
                    EnlaceDescargarTodos.Style("color") = "#0000FF"
                    EnlaceDescargarTodos.Style("cursor") = "pointer"
                    If Me.Portal = True Then
                        If PortalInstancia > 0 Then
                            PortalInstancia = (-1) * PortalInstancia
                            EnlaceDescargarTodos.Attributes("onclick") = "descargarAdjuntos('" + idAdjuntos.ToString + "', '" + idAdjuntosNew.ToString + "', '" & PortalInstancia.ToString & "', '" & tipos.ToString & "','" + Me.ClientID.ToString _
                                + "','" + CLng(Me.Tag).ToString + "','" + Nombres + "','" + Fechas.ToString + "','" + IIf(EsAltafactura, "1", "0") + "')"
                            PortalInstancia = (-1) * PortalInstancia
                        Else
                            EnlaceDescargarTodos.Attributes("onclick") = "descargarAdjuntos('" + idAdjuntos.ToString + "', '" + idAdjuntosNew.ToString + "', '" & Instancia.ToString & "', '" & tipos.ToString & "','" + Me.ClientID.ToString _
                                + "','" + CLng(Me.Tag).ToString + "','" + Nombres + "','" + Fechas.ToString + "','" + IIf(EsAltafactura, "1", "0") + "')"
                        End If
                    Else
                        EnlaceDescargarTodos.Attributes("onclick") = "descargarAdjuntos('" + idAdjuntos.ToString + "', '" + idAdjuntosNew.ToString + "', '" & Instancia.ToString & "', '" & tipos.ToString & "','" + Me.ClientID.ToString + "')"
                    End If
                    oTblCellOpciones.Controls.Add(EnlaceDescargarTodos)
                    oTblRowOpciones.Cells.Add(oTblCellOpciones)

                    'Eliminar Todos
                    If Not _readOnly Then
                        oTblCellOpciones = New System.Web.UI.HtmlControls.HtmlTableCell
                        Dim EnlaceEliminarTodos As New HtmlControls.HtmlAnchor
                        EnlaceEliminarTodos.InnerText = oTextos.Rows(6).Item(1)
                        EnlaceEliminarTodos.Style("text-decoration") = "underline"
                        EnlaceEliminarTodos.Attributes.Add("class", "enlaceAdj")
                        EnlaceEliminarTodos.Style("color") = "#0000FF"
                        EnlaceEliminarTodos.Style("cursor") = "pointer"
                        EnlaceEliminarTodos.Attributes("onclick") = "eliminarAdjuntos('" + Me.ClientID.ToString + "')"
                        oTblCellOpciones.Controls.Add(EnlaceEliminarTodos)
                        oTblCellOpciones.Align = "right"
                        oTblRowOpciones.Cells.Add(oTblCellOpciones)
                    End If
                End If

                oTblOpciones.Rows.Add(oTblRowOpciones)

                oTblCellInput.Controls.Add(oTblOpciones)
                oTblRowInput.Cells.Add(oTblCellInput)
                oTblInput.Rows.Add(oTblRowInput)

                oTblRow = New System.Web.UI.WebControls.TableRow
                oTblCell = New System.Web.UI.WebControls.TableCell

                DivGeneral.ID = "divcontenedor"
                DivGeneral.Controls.Add(oTblInput)
                oTblCell.Controls.Add(DivGeneral)

                oTblCell.Controls.Add(oHidAct)
                oTblCell.Controls.Add(oHidNew)
                oTblCell.Controls.Add(oHidNombre)
                oTblCell.Controls.Add(oHidCampo)
                oTblCell.Controls.Add(oHidFavorito)
                oTblCell.Controls.Add(oHidInstancia)
                If Me.Portal = True Then oTblCell.Controls.Add(oHidPortalInstancia)
                oTblCell.Controls.Add(oHidTipo)
                oTblRow.Cells.Add(oTblCell)

                oTbl.Rows.Add(oTblRow)
                oTbl.ID = "_tbl"

                Me.Controls.Add(oTbl)

                'Cargo los adjuntos
                If PortalInstancia > 0 Then Instancia = (-1) * PortalInstancia

                For i = 0 To oAdjuntos.Data.Tables(0).Rows.Count - 1
                    A�adirAdjunto(oAdjuntos.Data.Tables(0).Rows(i), Instancia, iTipo, CLng(Me.Tag), EsAltafactura)
                Next
            End If
        End If
    End Sub
    ''' <summary>
    ''' Generar cada uno de los adjuntos de DataEntry tipo archivoContrato
    ''' </summary>
    ''' <remarks>Llamada desde: CtrlTipoArchivo; Tiempo m�ximo:0 </remarks>
    Public Sub A�adirAdjuntoContrato(ByVal fila As System.Data.DataRow, ByVal Instancia As Long, ByVal Tipo As Short, ByVal idCampo As Long)
        Dim tablaContenedora As HtmlControls.HtmlTable
        Dim filaContenedora As New HtmlControls.HtmlTableRow
        Dim celdaContenedora As New HtmlControls.HtmlTableCell
        Dim tablaContenido As New HtmlControls.HtmlTable
        Dim filaContenido As HtmlControls.HtmlTableRow
        Dim celdaContenido As HtmlControls.HtmlTableCell

        Dim EnlaceFichero As New HtmlControls.HtmlAnchor
        Dim lblPersona As New Label
        Dim imgEliminar As New HtmlControls.HtmlImage
        Dim imgSustituir As New HtmlControls.HtmlImage
        Dim txtComentarios As New TextBox
        Dim txtComentariosReadOnly As New HtmlControls.HtmlTextArea
        Dim DivComentarios As New HtmlControls.HtmlGenericControl("DIV")
        Dim imgPreviaAdjun As New Image
        Dim hidComentario As New HtmlControls.HtmlInputHidden
        Dim idAdjuntoContrato As Long

        tablaContenedora = Me.Controls(0).Controls(0).Controls(0).Controls(0).Controls(0)
        tablaContenido.Style.Add("table-layout", "fixed")
        tablaContenido.Width = "100%"

        filaContenido = New HtmlControls.HtmlTableRow
        celdaContenido = New HtmlControls.HtmlTableCell
        celdaContenido.Width = "30%"
        celdaContenido.Attributes.Add("max-width", "30%")
        filaContenido.Cells.Add(celdaContenido)
        celdaContenido = New HtmlControls.HtmlTableCell
        celdaContenido.Width = Unit.Percentage(49).ToString
        filaContenido.Cells.Add(celdaContenido)
        If Not _readOnly Then
            celdaContenido = New HtmlControls.HtmlTableCell
            celdaContenido.Width = Unit.Percentage(4).ToString
            filaContenido.Cells.Add(celdaContenido)
            celdaContenido = New HtmlControls.HtmlTableCell
            celdaContenido.Width = Unit.Percentage(4).ToString
            filaContenido.Cells.Add(celdaContenido)
        End If
        tablaContenido.Rows.Add(filaContenido)


        filaContenido = New HtmlControls.HtmlTableRow
        'Enlace fichero
        EnlaceFichero.InnerText = fila("NOM") '& " (" & fila("DATASIZE") & " kb)"
        EnlaceFichero.ID = fila("ID")
        idAdjuntoContrato = DBNullToInteger(fila("ID"))
        If Me.Portal = True Then
            EnlaceFichero.Attributes("onclick") = "descargarAdjuntoContrato(" + Instancia.ToString + ", " + Me.IdContrato.ToString + ", " + IIf(idAdjuntoContrato <> 0, idAdjuntoContrato.ToString, Me.IdArchivoContrato.ToString) + ", '" + Replace(HttpUtility.UrlEncode(fila("NOM").ToString), "'", "\'") + "'," + IIf(Me.TamanyoContrato <> "", Me.TamanyoContrato, DBNullToInteger(fila("DATASIZE")).ToString) _
                + ",'" + idCampo.ToString + "','" + fila("FECALTA").ToString + "',1)"
        Else
            EnlaceFichero.Attributes("onclick") = "descargarAdjuntoContrato(" + Instancia.ToString + ", " + Me.IdContrato.ToString + ", " + IIf(idAdjuntoContrato <> 0, idAdjuntoContrato.ToString, Me.IdArchivoContrato.ToString) + ", '" + Replace(HttpUtility.UrlEncode(fila("NOM").ToString), "'", "\'") + "'," + IIf(Me.TamanyoContrato <> "", Me.TamanyoContrato, DBNullToInteger(fila("DATASIZE")).ToString) + ")"
        End If
        EnlaceFichero.Style("text-decoration") = "underline"
        EnlaceFichero.Attributes.Add("class", "enlaceAdj")
        EnlaceFichero.Style("color") = "#0000FF"
        EnlaceFichero.Style("cursor") = "pointer"
        celdaContenido = New HtmlControls.HtmlTableCell
        celdaContenido.Width = "30%"
        celdaContenido.Attributes.Add("word-wrap", "break-word")
        'Input Hidden con el ID y tipo del adjunto y la instancia
        Dim inputID As New HtmlControls.HtmlInputHidden
        inputID.ID = "Adj_" & fila("ID")
        inputID.Value = fila("ID") & "###1###" & Instancia.ToString
        celdaContenido.Controls.Add(EnlaceFichero)
        celdaContenido.Controls.Add(inputID)
        filaContenido.Cells.Add(celdaContenido)

        'Persona que adjunto el documento
        celdaContenido = New HtmlControls.HtmlTableCell
        celdaContenido.Width = Unit.Percentage(49).ToString
        lblPersona.Text = fila("NOMBRE") & "( " & fila("FECALTA") & " )"
        lblPersona.CssClass = "fntLogin"
        celdaContenido.Controls.Add(lblPersona)
        filaContenido.Cells.Add(celdaContenido)


        'Eliminar adjunto
        If Not _readOnly Then
            celdaContenido = New HtmlControls.HtmlTableCell
            celdaContenido.Width = Unit.Percentage(4).ToString
            imgEliminar.Alt = "Eliminar"
            imgEliminar.ID = "Eliminar_" & fila("ID")
            If _bPortal Then
                imgEliminar.Src = ConfigurationManager.AppSettings("rutanormal") & "/script/_common/images/eliminar.gif"
            Else
                imgEliminar.Src = ConfigurationManager.AppSettings("ruta") & "images/eliminar.gif"
            End If
            imgEliminar.Attributes("onclick") = "borrarAdjuntoContrato('" + fila("ID").ToString + "', '1', '" & Instancia.ToString & "','" & Me.ClientID.ToString & "','" & Page.AppRelativeVirtualPath & "'," & Me.IdContrato & ")"
            celdaContenido.Controls.Add(imgEliminar)
            celdaContenido.Align = "right"
            filaContenido.Cells.Add(celdaContenido)
        End If

        'Sustituir adjunto
        If Not _readOnly Then
            celdaContenido = New HtmlControls.HtmlTableCell
            celdaContenido.Width = Unit.Percentage(4).ToString
            imgEliminar.ID = "Sustituir_" & fila("ID")
            imgSustituir.Alt = "Sustituir"
            imgSustituir.Attributes("onclick") = "sustituirAdjuntoContrato('" + fila("ID").ToString + "', '1', '" & Instancia.ToString & "','" & Me.ClientID.ToString & "','" & Page.AppRelativeVirtualPath & "','" & Replace(HttpUtility.UrlEncode(fila("NOM").ToString), "'", "\'") & "')"
            If _bPortal Then
                imgSustituir.Src = ConfigurationManager.AppSettings("rutanormal") & "/script/_common/images/sustituir.gif"
            Else
                imgSustituir.Src = ConfigurationManager.AppSettings("rutaPM") & "_common/images/sustituir.gif"
            End If

            celdaContenido.Controls.Add(imgSustituir)
            celdaContenido.Align = "right"
            filaContenido.Cells.Add(celdaContenido)
        End If

        tablaContenido.Rows.Add(filaContenido)

        'A�ado el textArea para los comentarios
        filaContenido = New HtmlControls.HtmlTableRow

        hidComentario.ID = "_hidComent" & fila("ID")
        hidComentario.Value = DBNullToStr(fila("COMENT"))
        If Not _readOnly Then
            txtComentarios.ID = "_text" & fila("ID")
            txtComentarios.TextMode = TextBoxMode.MultiLine
            txtComentarios.MaxLength = 500
            txtComentarios.Height = Unit.Pixel(33)
            txtComentarios.Text = DBNullToStr(fila("COMENT"))
            txtComentarios.Style("overflow") = "hidden"
            txtComentarios.Attributes("onfocus") = "aumentarTamanoCajaTexto('" + Me.UniqueID.ToString + "', '" + fila("ID").ToString + "',0)"
            txtComentarios.Attributes("onblur") = "reducirTamanoCajaTextoContrato('" + Me.UniqueID.ToString + "', '" + fila("ID").ToString + "', '5','" + Instancia.ToString + "',1,0," + _idArchivoContrato.ToString + ")"
            txtComentarios.Attributes("onchange") = "return validarLengthyCortarStringLargo(this,500,'" & _errMsg & "'); "
            txtComentarios.Width = Unit.Percentage(90)
            celdaContenido = New HtmlControls.HtmlTableCell
            celdaContenido.Attributes("colspan") = "4"
            celdaContenido.Width = "100%"
            celdaContenido.Align = "left"
            celdaContenido.Controls.Add(txtComentarios)
            celdaContenido.Controls.Add(hidComentario)
            filaContenido.Cells.Add(celdaContenido)
            tablaContenido.Rows.Add(filaContenido)
        Else
            If fila("COMENT").ToString.Trim <> "" Then
                Dim bAjustarTamanoCajaTexto As Boolean
                Dim posicion As Integer

                txtComentariosReadOnly.ID = "_text" & fila("ID")
                txtComentariosReadOnly.Cols = 110
                txtComentariosReadOnly.Rows = 2
                posicion = InStr(fila("COMENT"), Chr(13) & Chr(10))
                If posicion > 0 Then
                    posicion = InStr(posicion + 2, fila("COMENT"), Chr(13) & Chr(10))
                    If posicion > 0 Then
                        If posicion >= 140 Then
                            posicion = 140
                        End If
                        txtComentariosReadOnly.InnerText = DBNullToStr(fila("COMENT")).Substring(0, posicion - 1) & " ..."
                        bAjustarTamanoCajaTexto = True
                    Else
                        If Len(fila("COMENT")) >= 140 Then
                            bAjustarTamanoCajaTexto = True
                            posicion = InStr(fila("COMENT"), Chr(13) & Chr(10))
                            If Len(fila("COMENT")) > (posicion + 70) Then
                                posicion = posicion + 70
                            Else
                                posicion = 140
                            End If
                            txtComentariosReadOnly.InnerText = DBNullToStr(fila("COMENT")).Substring(0, posicion - 1) & " ..."
                        Else
                            txtComentariosReadOnly.InnerText = DBNullToStr(fila("COMENT"))
                        End If
                    End If
                Else
                    If Len(fila("COMENT")) >= 140 Then
                        bAjustarTamanoCajaTexto = True
                        txtComentariosReadOnly.InnerText = DevolverTextoAjustado(DBNullToStr(fila("COMENT")), 70, 140, 2) & " ..."
                        posicion = txtComentariosReadOnly.InnerText.Length - 4
                    Else
                        txtComentariosReadOnly.InnerText = DBNullToStr(fila("COMENT"))
                    End If

                End If

                txtComentariosReadOnly.Style("border") = "none"
                txtComentariosReadOnly.Style("overflow") = "hidden"
                txtComentariosReadOnly.Style("background-color") = "transparent"
                If bAjustarTamanoCajaTexto Then
                    txtComentariosReadOnly.Attributes("onmouseover") = "aumentarTamanoCajaTexto('" + Me.UniqueID.ToString + "', '" + fila("ID").ToString + "',1)"
                    txtComentariosReadOnly.Attributes("onmouseout") = "reducirTamanoCajaTextoContrato('" + Me.UniqueID.ToString + "', '" + fila("ID").ToString + "','5','" + Instancia.ToString + "',0,'" + (posicion - 1).ToString + "')"
                End If
                txtComentariosReadOnly.Attributes("readonly") = "true"
                txtComentariosReadOnly.Attributes("background-color") = "transparent"
                celdaContenido = New HtmlControls.HtmlTableCell
                celdaContenido.Attributes("colspan") = "4"
                celdaContenido.Width = "100%"
                celdaContenido.Align = "center"
                celdaContenido.Controls.Add(txtComentariosReadOnly)
                celdaContenido.Controls.Add(hidComentario)
                filaContenido.Cells.Add(celdaContenido)
                tablaContenido.Rows.Add(filaContenido)
            End If
        End If

        'A�ado la tabla con el adjunto a la tabla global
        celdaContenedora.ColSpan = 3
        celdaContenedora.Controls.Add(tablaContenido)
        filaContenedora.Cells.Add(celdaContenedora)
        tablaContenedora.Rows.Add(filaContenedora)
    End Sub
    ''' <summary>
    ''' Generar cada uno de los adjuntos de DataEntry tipo archivo
    ''' </summary>
    ''' <remarks>Llamada desde: CtrlTipoArchivo; Tiempo m�ximo:0 </remarks>
    Public Sub A�adirAdjunto(ByVal fila As System.Data.DataRow, ByVal Instancia As Long, ByVal Tipo As Short, ByVal idCampo As Long, ByVal EsAltafactura As Boolean)
        Dim tablaContenedora As HtmlControls.HtmlTable
        Dim filaContenedora As New HtmlControls.HtmlTableRow
        Dim celdaContenedora As New HtmlControls.HtmlTableCell
        Dim tablaContenido As New HtmlControls.HtmlTable
        Dim filaContenido As HtmlControls.HtmlTableRow
        Dim celdaContenido As HtmlControls.HtmlTableCell

        Dim EnlaceFichero As New HtmlControls.HtmlAnchor
        Dim lblPersona As New Label
        Dim imgEliminar As New HtmlControls.HtmlImage
        Dim imgSustituir As New HtmlControls.HtmlImage
        Dim txtComentarios As New TextBox
        Dim txtComentariosReadOnly As New HtmlControls.HtmlTextArea
        Dim DivComentarios As New HtmlControls.HtmlGenericControl("DIV")
        Dim imgPreviaAdjun As New Image
        Dim hidComentario As New HtmlControls.HtmlInputHidden

        tablaContenedora = Me.Controls(0).Controls(0).Controls(0).Controls(0).Controls(0)
        tablaContenido.Style.Add("table-layout", "fixed")
        tablaContenido.Width = "100%"

        filaContenido = New HtmlControls.HtmlTableRow
        celdaContenido = New HtmlControls.HtmlTableCell
        celdaContenido.Width = "30%"
        celdaContenido.Attributes.Add("max-width", "30%")
        filaContenido.Cells.Add(celdaContenido)
        celdaContenido = New HtmlControls.HtmlTableCell
        celdaContenido.Width = Unit.Percentage(49).ToString
        filaContenido.Cells.Add(celdaContenido)
        If Not _readOnly Then
            celdaContenido = New HtmlControls.HtmlTableCell
            celdaContenido.Width = Unit.Percentage(4).ToString
            filaContenido.Cells.Add(celdaContenido)
            celdaContenido = New HtmlControls.HtmlTableCell
            celdaContenido.Width = Unit.Percentage(4).ToString
            filaContenido.Cells.Add(celdaContenido)
        End If
        tablaContenido.Rows.Add(filaContenido)


        filaContenido = New HtmlControls.HtmlTableRow
        'Enlace fichero
        EnlaceFichero.InnerText = fila("NOM") & " (" & fila("DATASIZE") & " kb)"
        EnlaceFichero.ID = fila("ID")
        If Me.Portal = True Then
            EnlaceFichero.Attributes("onclick") = "descargarAdjunto('" & fila("id") & "', '" & fila("tipo") & "', '" & Instancia.ToString & "','" + Me.ClientID.ToString _
                + "','" + idCampo.ToString + "','" + Replace(HttpUtility.UrlEncode(fila("NOM").ToString), "'", "\'") + "','" + fila("FECALTA").ToString() + "','" + IIf(EsAltafactura, "1", "0") + "')"
        Else
            EnlaceFichero.Attributes("onclick") = "descargarAdjunto('" & fila("id") & "', '" & fila("tipo") & "', '" & Instancia.ToString & "','" + Me.ClientID.ToString + "')"
        End If
        If Instancia < 0 Then Instancia = 0
        EnlaceFichero.Style("text-decoration") = "underline"
        EnlaceFichero.Attributes.Add("class", "enlaceAdj")
        EnlaceFichero.Style("color") = "#0000FF"
        EnlaceFichero.Style("cursor") = "pointer"
        celdaContenido = New HtmlControls.HtmlTableCell
        celdaContenido.Width = "30%"
        celdaContenido.Attributes.Add("word-wrap", "break-word")
        'Input Hidden con el ID y tipo del adjunto y la instancia
        Dim inputID As New HtmlControls.HtmlInputHidden
        inputID.ID = "Adj_" & fila("ID")
        inputID.Value = fila("ID") & "###" & fila("tipo") & "###" & Instancia.ToString
        celdaContenido.Controls.Add(EnlaceFichero)
        celdaContenido.Controls.Add(inputID)
        filaContenido.Cells.Add(celdaContenido)

        'Persona que adjunto el documento
        celdaContenido = New HtmlControls.HtmlTableCell
        celdaContenido.Width = Unit.Percentage(49).ToString
        lblPersona.Text = fila("NOMBRE") & "( " & fila("FECALTA") & " )"
        lblPersona.CssClass = "fntLogin"
        celdaContenido.Controls.Add(lblPersona)
        filaContenido.Cells.Add(celdaContenido)


        'Eliminar adjunto
        If Not _readOnly Then
            celdaContenido = New HtmlControls.HtmlTableCell
            celdaContenido.Width = Unit.Percentage(4).ToString
            imgEliminar.Alt = "Eliminar"
            imgEliminar.ID = "Eliminar_" & fila("ID")
            If _bPortal Then
                imgEliminar.Src = ConfigurationManager.AppSettings("rutanormal") & "/script/_common/images/eliminar.gif"
            Else
                imgEliminar.Src = ConfigurationManager.AppSettings("ruta") & "images/eliminar.gif"
            End If
            imgEliminar.Attributes("onclick") = "borrarAdjunto('" + fila("ID").ToString + "', '" & fila("tipo").ToString & "', '" & Instancia.ToString & "','" & Me.ClientID.ToString & "','" & Page.AppRelativeVirtualPath & "')"
            celdaContenido.Controls.Add(imgEliminar)
            celdaContenido.Align = "right"
            filaContenido.Cells.Add(celdaContenido)
        End If

        'Sustituir adjunto
        If Not _readOnly Then
            celdaContenido = New HtmlControls.HtmlTableCell
            celdaContenido.Width = Unit.Percentage(4).ToString
            imgEliminar.ID = "Sustituir_" & fila("ID")
            imgSustituir.Alt = "Sustituir"
            imgSustituir.Attributes("onclick") = "sustituirAdjunto('" + fila("ID").ToString + "', '" & fila("tipo").ToString & "', '" & Instancia.ToString & "','" & Me.ClientID.ToString & "','" & Page.AppRelativeVirtualPath & "','" & Replace(HttpUtility.UrlEncode(fila("NOM").ToString), "'", "\'") & "')"
            If _bPortal Then
                imgSustituir.Src = ConfigurationManager.AppSettings("rutanormal") & "/script/_common/images/sustituir.gif"
            Else
                imgSustituir.Src = ConfigurationManager.AppSettings("rutaPM") & "_common/images/sustituir.gif"
            End If
            celdaContenido.Controls.Add(imgSustituir)
            celdaContenido.Align = "right"
            filaContenido.Cells.Add(celdaContenido)
        End If

        tablaContenido.Rows.Add(filaContenido)

        'A�ado el textArea para los comentarios
        filaContenido = New HtmlControls.HtmlTableRow

        hidComentario.ID = "_hidComent" & fila("ID")
        hidComentario.Value = DBNullToStr(fila("COMENT"))
        If Not _readOnly Then
            txtComentarios.ID = "_text" & fila("ID")
            txtComentarios.TextMode = TextBoxMode.MultiLine
            txtComentarios.MaxLength = 500
            txtComentarios.Height = Unit.Pixel(33)
            txtComentarios.Text = DBNullToStr(fila("COMENT"))
            txtComentarios.Style("overflow") = "hidden"
            txtComentarios.Attributes("onfocus") = "aumentarTamanoCajaTexto('" + Me.UniqueID.ToString + "', '" + fila("ID").ToString + "',0)"
            txtComentarios.Attributes("onblur") = "reducirTamanoCajaTexto('" + Me.UniqueID.ToString + "', '" + fila("ID").ToString + "', '" + fila("TIPO").ToString + "','" + Instancia.ToString + "',1,0)"
            txtComentarios.Attributes("onchange") = "return validarLengthyCortarStringLargo(this,500,'" & _errMsg & "'); "
            txtComentarios.Width = Unit.Percentage(90)
            celdaContenido = New HtmlControls.HtmlTableCell
            celdaContenido.Attributes("colspan") = "4"
            celdaContenido.Width = "100%"
            celdaContenido.Align = "left"
            celdaContenido.Controls.Add(txtComentarios)
            celdaContenido.Controls.Add(hidComentario)
            filaContenido.Cells.Add(celdaContenido)
            tablaContenido.Rows.Add(filaContenido)
        Else
            If fila("COMENT").ToString.Trim <> "" Then
                Dim bAjustarTamanoCajaTexto As Boolean
                Dim posicion As Integer

                txtComentariosReadOnly.ID = "_text" & fila("ID")
                txtComentariosReadOnly.Cols = 110
                txtComentariosReadOnly.Rows = 2
                posicion = InStr(fila("COMENT"), Chr(13) & Chr(10))
                If posicion > 0 Then
                    posicion = InStr(posicion + 2, fila("COMENT"), Chr(13) & Chr(10))
                    If posicion > 0 Then
                        If posicion >= 140 Then
                            posicion = 140
                        End If
                        txtComentariosReadOnly.InnerText = DBNullToStr(fila("COMENT")).Substring(0, posicion - 1) & " ..."
                        bAjustarTamanoCajaTexto = True
                    Else
                        If Len(fila("COMENT")) >= 140 Then
                            bAjustarTamanoCajaTexto = True
                            posicion = InStr(fila("COMENT"), Chr(13) & Chr(10))
                            If Len(fila("COMENT")) > (posicion + 70) Then
                                posicion = posicion + 70
                            Else
                                posicion = 140
                            End If
                            txtComentariosReadOnly.InnerText = DBNullToStr(fila("COMENT")).Substring(0, posicion - 1) & " ..."
                        Else
                            txtComentariosReadOnly.InnerText = DBNullToStr(fila("COMENT"))
                        End If
                    End If
                Else
                    If Len(fila("COMENT")) >= 140 Then
                        bAjustarTamanoCajaTexto = True
                        txtComentariosReadOnly.InnerText = DevolverTextoAjustado(DBNullToStr(fila("COMENT")), 70, 140, 2) & " ..."
                        posicion = txtComentariosReadOnly.InnerText.Length - 4
                    Else
                        txtComentariosReadOnly.InnerText = DBNullToStr(fila("COMENT"))
                    End If

                End If

                txtComentariosReadOnly.Style("border") = "none"
                txtComentariosReadOnly.Style("overflow") = "hidden"
                txtComentariosReadOnly.Style("background-color") = "transparent"
                If bAjustarTamanoCajaTexto Then
                    txtComentariosReadOnly.Attributes("onmouseover") = "aumentarTamanoCajaTexto('" + Me.UniqueID.ToString + "', '" + fila("ID").ToString + "',1)"
                    txtComentariosReadOnly.Attributes("onblur") = "reducirTamanoCajaTexto('" + Me.UniqueID.ToString + "', '" + fila("ID").ToString + "', '" + fila("TIPO").ToString + "','" + Instancia.ToString + "',0,'" + (posicion - 1).ToString + "')"
                End If
                txtComentariosReadOnly.Attributes("readonly") = "true"
                txtComentariosReadOnly.Attributes("background-color") = "transparent"
                celdaContenido = New HtmlControls.HtmlTableCell
                celdaContenido.Attributes("colspan") = "4"
                celdaContenido.Width = "100%"
                celdaContenido.Align = "center"
                celdaContenido.Controls.Add(txtComentariosReadOnly)
                celdaContenido.Controls.Add(hidComentario)
                filaContenido.Cells.Add(celdaContenido)
                tablaContenido.Rows.Add(filaContenido)
            End If
        End If

        'A�ado la tabla con el adjunto a la tabla global
        celdaContenedora.ColSpan = 3
        celdaContenedora.Controls.Add(tablaContenido)
        filaContenedora.Cells.Add(celdaContenedora)
        tablaContenedora.Rows.Add(filaContenedora)
    End Sub
    ''' <summary>
    ''' Generar un dataentry de tipo boolean
    ''' </summary>
    ''' <remarks>Llamada desde: La generacion de cualquier dataentry de tipo boolean; Tiempo m�ximo:0 </remarks>
    Private Sub CtrlTipoBoolean(Optional ByVal bReadOnly As Boolean = False)
        Dim oTbl As System.Web.UI.HtmlControls.HtmlTable
        Dim oTblRow As System.Web.UI.HtmlControls.HtmlTableRow
        Dim oTblCell As System.Web.UI.HtmlControls.HtmlTableCell
        Dim oWidth As System.Web.UI.WebControls.Unit
        Dim oTxt As Infragistics.WebUI.WebDataInput.WebTextEdit
        Dim oHid As System.Web.UI.HtmlControls.HtmlInputHidden

        oTbl = New System.Web.UI.HtmlControls.HtmlTable

        oTbl.CellPadding = 0
        oTbl.CellSpacing = 0
        oTbl.Style.Add("border-width", "0px")

        oWidth = Me.Width

        oTbl.Width = oWidth.ToString



        oHid = New System.Web.UI.HtmlControls.HtmlInputHidden
        oTxt = New Infragistics.WebUI.WebDataInput.WebTextEdit
        oTxt.ID = "_t"
        oHid.ID = "_h"

        If Not _valor Is Nothing Then
            oHid.Value = _valor.ToString
            Dim oRow As DataRow

            For Each oRow In _lista.Tables(0).Rows
                If oRow.Item("value") = (_valor = "1") Then
                    oTxt.Value = oRow.Item("TEXT_" & _Idi)
                End If
            Next
        End If

        oTxt.ReadOnly = _readOnly
        oTxt.Width = oWidth
        oTxt.Style("display") = ""

        If Not _readOnly Then
            oTxt.ButtonsAppearance.CustomButtonImageUrl = Me.Page.ClientScript.GetWebResourceUrl(GetType(GeneralEntry), "Fullstep.DataEntry.ig_cmboDownXP1.bmp")
            oTxt.ButtonsAppearance.CustomButtonDisplay = Infragistics.WebUI.WebDataInput.ButtonDisplay.OnRight
            oTxt.ReadOnly = True
        End If
        oTblRow = New System.Web.UI.HtmlControls.HtmlTableRow
        oTblCell = New System.Web.UI.HtmlControls.HtmlTableCell
        oTblCell.Width = "100%"
        oTblCell.Controls.Add(oTxt)
        oTblCell.Controls.Add(oHid)
        oTblRow.Cells.Add(oTblCell)

        oTbl.Rows.Add(oTblRow)
        oTbl.ID = "_tbl"
        Me.Controls.Add(oTbl)
    End Sub
    ''' Revisado por: blp. Fecha: 04/10/2011
    ''' <summary>
    ''' Generar un dataentry de tipo lista
    ''' </summary>
    ''' <param name="iTipo">tipo de los valores de la lista</param>
    ''' <remarks>Llamada desde: La generacion de cualquier dataentry de tipo lista; Tiempo m�ximo:0 </remarks>
    Private Sub CtrlTipoLista(ByVal iTipo As FSNLibrary.TiposDeDatos.TipoGeneral)
        Dim oTbl As System.Web.UI.HtmlControls.HtmlTable
        Dim oTblRow As System.Web.UI.HtmlControls.HtmlTableRow
        Dim oTblCell As System.Web.UI.HtmlControls.HtmlTableCell
        Dim oTblCell2 As System.Web.UI.HtmlControls.HtmlTableCell
        Dim oDrpDwn As Infragistics.Web.UI.ListControls.WebDropDown
        Dim oLbl As Infragistics.WebUI.WebDataInput.WebTextEdit = Nothing
        Dim oTxt As Infragistics.WebUI.WebDataInput.WebTextEdit
        Dim oHid As System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oWidth As System.Web.UI.WebControls.Unit

        oTbl = New System.Web.UI.HtmlControls.HtmlTable
        oHid = New System.Web.UI.HtmlControls.HtmlInputHidden

        oWidth = Me.Width

        oTbl.Width = oWidth.ToString
        oTbl.ID = "_tbl"
        oTbl.CellPadding = 0
        oTbl.CellSpacing = 0
        oTbl.Style.Add("border-width", "0px")

        oHid.ID = "_h"

        oTblRow = New System.Web.UI.HtmlControls.HtmlTableRow
        oTblCell = New System.Web.UI.HtmlControls.HtmlTableCell
        oTblCell2 = New System.Web.UI.HtmlControls.HtmlTableCell
        oTblCell2.Width = "0%"
        oTblCell.Width = "100%"

        Dim oHidOrgCompras As System.Web.UI.HtmlControls.HtmlInputHidden = Nothing
        If _tipoGS = TiposDeDatos.TipoCampoGS.Centro Then
            oHidOrgCompras = New System.Web.UI.HtmlControls.HtmlInputHidden
            oHidOrgCompras.ID = "_hOrgCompras" 'Contiene el valor del dataEntryDependent

            If _DependentValue <> Nothing Then
                oHidOrgCompras.Value = _DependentValue
            End If
        End If

        Dim oHidCentro As System.Web.UI.HtmlControls.HtmlInputHidden = Nothing
        If _tipoGS = TiposDeDatos.TipoCampoGS.OrganizacionCompras Then
            oHidCentro = New System.Web.UI.HtmlControls.HtmlInputHidden
            oHidCentro.ID = "_hCentro" 'Contiene el valor del dataEntryDependent

            If _DependentValueDataEntry2 <> Nothing Then
                oHidCentro.Value = _DependentValueDataEntry2
            End If
        End If

        If _readOnly Then
            oTxt = New Infragistics.WebUI.WebDataInput.WebTextEdit
            oTxt.ID = "_t"

            Select Case iTipo
                Case TiposDeDatos.TipoGeneral.TipoTextoCorto, TiposDeDatos.TipoGeneral.TipoTextoMedio, TiposDeDatos.TipoGeneral.TipoString, TiposDeDatos.TipoGeneral.TipoTextoLargo
                    If _maxLength > 0 Then
                        oTxt.MaxLength = _maxLength
                    Else
                        If iTipo = TiposDeDatos.TipoGeneral.TipoTextoCorto Then
                            oTxt.MaxLength = 100
                        ElseIf iTipo = TiposDeDatos.TipoGeneral.TipoTextoMedio Or iTipo = TiposDeDatos.TipoGeneral.TipoString Then
                            oTxt.MaxLength = 800
                        End If
                    End If
                Case TiposDeDatos.TipoGeneral.TipoBoolean
                    If Not _valor Is Nothing AndAlso IsNumeric(_valor) Then
                        Dim oUser As Object
                        If Me.Portal = True Then
                            oUser = HttpContext.Current.Session("FS_Portal_User")
                        Else
                            oUser = HttpContext.Current.Session("FSN_User")
                        End If
                        If _valor = 0 Then
                            _text = _lista.Tables("BOOL").Rows(1).Item("TEXT_" & oUser.Idioma.ToString())
                        Else
                            _text = _lista.Tables("BOOL").Rows(0).Item("TEXT_" & oUser.Idioma.ToString())
                        End If
                    End If
            End Select

            If Not _valor Is Nothing Then
                oHid.Value = _valor.ToString
                oTxt.Text = _valor.ToString
                If Not _text Is Nothing Then
                    oTxt.Text = _text.ToString
                    If _toolTip = Nothing Then
                        _toolTip = _text.ToString
                    End If
                End If
            End If

            oTxt.ReadOnly = _readOnly

            oTxt.Width = Me.Width
            oTxt.BorderStyle = BorderStyle.None
            oTxt.BorderWidth = Unit.Pixel(0)
            oTxt.BackColor = System.Drawing.Color.Transparent
            oTxt.Style("display") = ""

            oTblCell.Controls.Add(oTxt)
            oTblCell.Controls.Add(oHid)
        Else
            _toolTip = Nothing

            oDrpDwn = New Infragistics.Web.UI.ListControls.WebDropDown
            'Activamos un icono que indica que el webdropdown se est� cargando
            oDrpDwn.AjaxIndicator.Enabled = Infragistics.Web.UI.DefaultableBoolean.True
            oDrpDwn.AjaxIndicator.Location = Infragistics.Web.UI.RelativeLocation.MiddleCenter
            oDrpDwn.AjaxIndicator.OffsetTop = 80 'La altura es de 120 pixels y con 80 se ve centrado, si se cambia la altura habr� que cambiar este valor para centrarlo. 
            oDrpDwn.AjaxIndicator.RelativeToControl = Infragistics.Web.UI.DefaultableBoolean.[True]
            oDrpDwn.ID = "_t"
            oDrpDwn.AutoFilterQueryType = Infragistics.Web.UI.ListControls.AutoFilterQueryTypes.StartsWith
            oDrpDwn.AutoSelectOnMatch = False 'En principio se deseaba q fuese True 
            'pero fastidia los combos relacionados. Ejemplo: Escribes "es" y te selecciona como pais "Estonia" 
            'y te carga las provincia pero lo q querias era "Espa�a"
            oDrpDwn.DisplayMode = Infragistics.Web.UI.ListControls.DropDownDisplayMode.DropDown
            oDrpDwn.DropDownContainerHeight = Unit.Pixel(120)
            oDrpDwn.DropDownContainerMaxHeight = Unit.Pixel(120)
            oDrpDwn.DropDownOrientation = Infragistics.Web.UI.ListControls.DropDownOrientation.BottomLeft
            oDrpDwn.EnableAutoCompleteFirstMatch = False
            oDrpDwn.EnableAutoFiltering = Infragistics.Web.UI.ListControls.AutoFiltering.Off
            oDrpDwn.EnableCustomValues = True
            oDrpDwn.EnableCustomValueSelection = True
            oDrpDwn.EnableClosingDropDownOnSelect = True
            oDrpDwn.EnableClosingDropDownOnBlur = True
            oDrpDwn.EnableViewState = False
            oDrpDwn.EnableAnimations = False
            If _Desglose Then
                oDrpDwn.EnableDropDownAsChild = True
            Else
                oDrpDwn.EnableDropDownAsChild = False
            End If
            oDrpDwn.Width = oWidth

            Dim bCasoConValueField As Boolean = True

            If (_tipoGS = TiposDeDatos.TipoCampoGS.SinTipo AndAlso (_tipo = TiposDeDatos.TipoGeneral.TipoNumerico OrElse _tipo = TiposDeDatos.TipoGeneral.TipoFecha OrElse _tipo = TiposDeDatos.TipoGeneral.TipoBoolean)) _
            OrElse (_tipoGS = TiposDeDatos.TipoCampoGS.Subtipo) _
            OrElse (_tipoGS = TiposDeDatos.TipoCampoGS.Contacto) _
            OrElse (_tipoGS = TiposDeDatos.TipoCampoGS.Persona) _
            OrElse (_tipoGS = TiposDeDatos.TipoCampoGS.ListadosPersonalizados) _
            OrElse (_tipo = TiposDeDatos.TipoGeneral.TipoBoolean) _
            OrElse (_tipoGS = TiposDeDatos.TipoCampoGS.PeriodoRenovacion) _
            OrElse (_tipoGS = TiposDeDatos.TipoCampoGS.EstadoHomologacion) Then
                If _tipo = TiposDeDatos.TipoGeneral.TipoFecha Then
                    If _idCampoPadreListaEnlazada <> 0 Then
                        oDrpDwn.ValueField = "Valor"
                        oDrpDwn.TextField = "Texto"
                    Else
                        oDrpDwn.TextField = "VALOR_FEC"
                        oDrpDwn.ValueField = "VALOR_FEC"
                    End If
                ElseIf _tipo = TiposDeDatos.TipoGeneral.TipoNumerico Then
                    If _idCampoPadreListaEnlazada <> 0 Then
                        oDrpDwn.ValueField = "Texto"
                        oDrpDwn.TextField = "Texto"
                    Else
                        oDrpDwn.TextField = "VALOR_NUM"
                        oDrpDwn.ValueField = "VALOR_NUM"
                    End If
                ElseIf _tipo = TiposDeDatos.TipoGeneral.TipoBoolean Then
                    Dim oUser As Object
                    If Me.Portal = True Then
                        oUser = HttpContext.Current.Session("FS_Portal_User")
                    Else
                        oUser = HttpContext.Current.Session("FSN_User")
                    End If

                    oDrpDwn.TextField = "TEXT_" & oUser.Idioma.ToString()
                    oDrpDwn.ValueField = "VALUE"
                    oDrpDwn.DropDownContainerHeight = Unit.Pixel(60)
                ElseIf (_tipoGS = TiposDeDatos.TipoCampoGS.Contacto) Then
                    oDrpDwn.TextField = "CONTACTO"
                    oDrpDwn.ValueField = "ID"
                ElseIf (_tipoGS = TiposDeDatos.TipoCampoGS.Persona) Then
                    oDrpDwn.TextField = "PARTNOM"
                    oDrpDwn.ValueField = "PARTCOD"
                ElseIf (_tipoGS = TiposDeDatos.TipoCampoGS.ListadosPersonalizados) Then
                    If _lista.Tables(0).Columns.Count > 1 Then
                        oDrpDwn.TextField = _lista.Tables(0).Columns(1).ColumnName
                    Else
                        oDrpDwn.TextField = _lista.Tables(0).Columns(0).ColumnName
                    End If
                    oDrpDwn.ValueField = _lista.Tables(0).Columns(0).ColumnName
                Else
                    oDrpDwn.TextField = "VALOR_TEXT_" & _Idi
                    oDrpDwn.ValueField = "ORDEN"
                End If

                If Not (_lista Is Nothing) Then
                    'Desglose mete entry sin lista para cabecera
                    'Desglose mete entry con lista para celda
                    If _lista.Tables.Count > 0 Then
                        oDrpDwn.DataSource = _lista.Tables(0)
                    End If
                End If
            Else
				If _tipoGS <> TiposDeDatos.TipoCampoGS.Departamento _
					AndAlso _tipoGS <> TiposDeDatos.TipoCampoGS.Centro _
					AndAlso _tipoGS <> TiposDeDatos.TipoCampoGS.Almacen _
					AndAlso _tipoGS <> TiposDeDatos.TipoCampoGS.Provincia _
					AndAlso _tipoGS <> TiposDeDatos.TipoCampoGS.Unidad _
					AndAlso _tipoGS <> TiposDeDatos.TipoCampoGS.UnidadPedido _
					AndAlso _tipoGS <> TiposDeDatos.TipoCampoGS.FormaPago _
					AndAlso _tipoGS <> TiposDeDatos.TipoCampoGS.OrganizacionCompras _
					AndAlso _tipoGS <> TiposDeDatos.TipoCampoGS.Moneda _
					AndAlso _tipoGS <> TiposDeDatos.TipoCampoGS.Pais _
					AndAlso _tipoGS <> TiposDeDatos.TipoCampoGS.Dest _
					AndAlso _tipoGS <> TiposDeDatos.TipoCampoGS.SinTipo _
					AndAlso _tipoGS <> TiposDeDatos.TipoCampoGS.ProveedorERP _
					AndAlso _tipoGS <> TiposDeDatos.TipoCampoGS.AnyoPartida Then

					oDrpDwn.ItemTemplate = New GridWebDrownTemplate(_tipoGS)
					oDrpDwn.TextField = "DEN"
					oDrpDwn.ValueField = "COD"
					If Not (_lista Is Nothing) Then
						oDrpDwn.DataSource = _lista.Tables(0)
					End If
				End If

				bCasoConValueField = False

                If _DesgloseCambiaWidth = True Then
                    If _tipo = TiposDeDatos.TipoGeneral.TipoBoolean Then
                        oDrpDwn.DropDownContainerWidth = Unit.Pixel(80)
                    Else
                        'cuando el ancho del dropdown est� definido (que es cuando es distinto de 0), 
                        'igualamos el ancho del contenedor (que es el desplegable) a ese ancho.
                        If oDrpDwn.Width <> Unit.Pixel(0) Then
                            oDrpDwn.DropDownContainerWidth = oDrpDwn.Width
                        Else
                            oDrpDwn.DropDownContainerWidth = Unit.Pixel(200)
                        End If
                    End If
                End If

                Select Case _tipoGS
					Case TiposDeDatos.TipoCampoGS.Pais, TiposDeDatos.TipoCampoGS.Provincia, TiposDeDatos.TipoCampoGS.Departamento,
							TiposDeDatos.TipoCampoGS.Centro, TiposDeDatos.TipoCampoGS.Almacen, TiposDeDatos.TipoCampoGS.Moneda,
							TiposDeDatos.TipoCampoGS.Unidad, TiposDeDatos.TipoCampoGS.FormaPago, TiposDeDatos.TipoCampoGS.OrganizacionCompras,
							TiposDeDatos.TipoCampoGS.Dest, TiposDeDatos.TipoCampoGS.SinTipo, TiposDeDatos.TipoCampoGS.UnidadPedido,
							TiposDeDatos.TipoCampoGS.ProveedorERP, TiposDeDatos.TipoCampoGS.PeriodoRenovacion, TiposDeDatos.TipoCampoGS.AnyoPartida
						oDrpDwn.ValueField = "Valor"
                        oDrpDwn.ValueField = "Valor"
                        oDrpDwn.TextField = "Texto"
                        oDrpDwn.EnableClientRendering = True
                        oDrpDwn.ClientEvents.DropDownOpening = "WebDropDown_DataCheck"
                        oDrpDwn.ClientEvents.InputKeyDown = "WebDropDown_KeyDown"
                End Select
            End If

            oDrpDwn.CurrentValue = String.Empty 'Inicializamos el valor que se muestra. A partir de la version 11.2 sale "null" en la caja de texto del webdropdown sin valores.
            If Not _valor Is Nothing AndAlso DBNullToSomething(_valor) IsNot Nothing Then
                oHid.Value = _valor.ToString
                oDrpDwn.SelectedValue = _valor.ToString
                oDrpDwn.DataBind()

                If Not _text Is Nothing AndAlso DBNullToSomething(_valor) IsNot Nothing Then
                    oDrpDwn.CurrentValue = _text.ToString
                End If

                Dim OrdenLista As Integer = 0
                For Each Item As Infragistics.Web.UI.ListControls.DropDownItem In oDrpDwn.Items
                    If _idCampoPadreListaEnlazada <> 0 AndAlso _tipo = TiposDeDatos.TipoGeneral.TipoNumerico Then
                        If oDrpDwn.DataSource.rows(OrdenLista).item("VALOR_NUM").ToString = _valor.ToString Then
                            oDrpDwn.SelectedItemIndex = Item.Index
                            oDrpDwn.CurrentValue = oDrpDwn.DataSource.rows(OrdenLista).item("VALOR_NUM").ToString
                            Exit For
                        End If
                        OrdenLista = OrdenLista + 1
                    ElseIf _idCampoPadreListaEnlazada <> 0 AndAlso _tipo = TiposDeDatos.TipoGeneral.TipoFecha Then
                        If oDrpDwn.DataSource.rows(OrdenLista).item("VALOR_FEC").ToString = _valor.ToString Then
                            oDrpDwn.SelectedItemIndex = Item.Index
                            oDrpDwn.CurrentValue = _valor.ToString
                            Exit For
                        End If
                        OrdenLista = OrdenLista + 1
                    ElseIf bCasoConValueField Then
                        If Item.Value = _valor.ToString Then
                            oDrpDwn.SelectedItemIndex = Item.Index
                            Exit For
                        End If
                    Else
                        If _tipoGS = TiposDeDatos.TipoCampoGS.SinTipo Then
                            If Item.DataItem.row.item("ORD") = _valor.ToString Then
                                oDrpDwn.SelectedItemIndex = Item.Index
                                Exit For
                            End If
                        ElseIf Item.DataItem.row.item(IIf(_tipoGS = TiposDeDatos.TipoCampoGS.Almacen, "ID", "COD")) = _valor.ToString Then
                            oDrpDwn.SelectedItemIndex = Item.Index
                            Exit For
                        End If
                    End If
                Next


            Else
                AddHandler oDrpDwn.DataBound, AddressOf DropDown_DataBound
            End If

            'Si el combo a crear tiene lista enlazada y tb tiene combo padre, su carga va por ajax
            If _idCampoListaEnlazada > 0 AndAlso _idCampoPadreListaEnlazada > 0 Then
                oDrpDwn.EnableClientRendering = True
                oDrpDwn.ClientEvents.DropDownOpening = "WebDropDown_DataCheck"
                oDrpDwn.ClientEvents.InputKeyDown = "WebDropDown_KeyDown"
            End If

            oDrpDwn.ClientEvents.SelectionChanged = "WebDropDown_SelectionChanging"
            oDrpDwn.ClientEvents.ValueChanged = "WebDropDown_valueChanged"
            oDrpDwn.ClientEvents.Blur = "WebDropDown_Focus"

            'En el c�digo javascript de Infragistics que se crea para los controles webdropdown
            'la propiedad enableMovingTargetWithSource es TRUE por defecto
            'Eso hace que a partir de la creaci�n de la p�gina, se llame de forma recurrente a la funci�n _onCheckPosition 
            'que consume muchos recursos y sobrecarga el javascript ejecutado en la p�gina cuando hay muchos webdropdown
            'Para evitarlo, deshabilitamos el enableMovingTargetWithSource
            If Not Page.ClientScript.IsStartupScriptRegistered(Page.GetType(), "enableMovingTargetWithSource_" + Me.IdContenedor + "_" + Me.ID + "_" + oDrpDwn.ID) Then
                Dim sScript As String = String.Format(IncludeScriptKeyFormat, "javascript", "Sys.Application.add_load(function(){$find('" + Me.IdContenedor + "_" + Me.ID + "_" + oDrpDwn.ID + "').behavior.set_enableMovingTargetWithSource(false)})")
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "enableMovingTargetWithSource_" + Me.IdContenedor + "_" + Me.ID + "_" + oDrpDwn.ID, sScript)
            End If

            oTblCell.Controls.Add(oDrpDwn)
            oTblCell.Controls.Add(oHid)

            If _lTablaExterna <= 0 Then
                If _intro <> 0 Then
                    Select Case _tipoGS
						Case TiposDeDatos.TipoCampoGS.Pais, TiposDeDatos.TipoCampoGS.Provincia, TiposDeDatos.TipoCampoGS.Departamento,
							TiposDeDatos.TipoCampoGS.Centro, TiposDeDatos.TipoCampoGS.Almacen, TiposDeDatos.TipoCampoGS.Moneda,
							TiposDeDatos.TipoCampoGS.Unidad, TiposDeDatos.TipoCampoGS.FormaPago, TiposDeDatos.TipoCampoGS.OrganizacionCompras,
							TiposDeDatos.TipoCampoGS.Dest, TiposDeDatos.TipoCampoGS.SinTipo, TiposDeDatos.TipoCampoGS.UnidadPedido,
							TiposDeDatos.TipoCampoGS.PeriodoRenovacion, TiposDeDatos.TipoCampoGS.EstadoHomologacion, TiposDeDatos.TipoCampoGS.AnyoPartida
							Dim oHidDataCheck As New HiddenField
                            'Estructura: IdEntryMaterial@@IDContenedor
                            oHidDataCheck.Value = Me.MasterField & TextosSeparadores.dosArrobas & _IdContenedor
                            oHidDataCheck.ID = oDrpDwn.ID & "_hidDataCheck"
                            oTblCell.Controls.Add(oHidDataCheck)
                    End Select
                End If
            End If

            If _tipoGS = TiposDeDatos.TipoCampoGS.Centro Then oTblCell.Controls.Add(oHidOrgCompras)
            If _tipoGS = TiposDeDatos.TipoCampoGS.OrganizacionCompras Then oTblCell.Controls.Add(oHidCentro)
        End If

        If _tipoGS = TiposDeDatos.TipoCampoGS.Dest Then
            oLbl = New Infragistics.WebUI.WebDataInput.WebTextEdit
            oLbl.ID = "_t2"
            oLbl.BorderWidth = Unit.Pixel(0)
            oLbl.BorderStyle = BorderStyle.None
            oLbl.Width = Unit.Percentage(0)
            If _valor Is Nothing Then
                oLbl.Style.Add("visibility", "hidden")
            End If
            oLbl.ButtonsAppearance.CustomButtonDisplay = Infragistics.WebUI.WebDataInput.ButtonDisplay.OnRight
            oLbl.ButtonsAppearance.CustomButtonImageUrl = Me.Page.ClientScript.GetWebResourceUrl(GetType(GeneralEntry), "Fullstep.DataEntry.info.gif")
        End If
        oTblRow.Cells.Add(oTblCell)
        If _tipoGS = TiposDeDatos.TipoCampoGS.Dest Then
            oTblCell2.Controls.Add(oLbl)
            oTblRow.Cells.Add(oTblCell2)
        End If

        oTbl.Rows.Add(oTblRow)
        Me.Controls.Add(oTbl)
    End Sub
    ''' <summary>Generar un dataentry de tipo lista externa</summary>
    ''' <remarks>Llamada desde: La generacion de cualquier dataentry de tipo lista externa</remarks>
    Private Sub CtrlTipoListaExterna()
        Dim sClientId As String = String.Empty

        ' ______________________
        '| _tbl                 |
        '| ____________________ |
        '|| _t                 ||
        '|| ____  _____  _____ || 
        '|||_t  ||_ext ||_img |||
        '|||____||_____||_____|||
        '||____________________||
        '|______________________|

        Dim oTbl As New System.Web.UI.WebControls.Table
        oTbl.Width = Me.Width
        oTbl.ID = "_tbl"
        oTbl.CellPadding = 0
        oTbl.CellSpacing = 0
        oTbl.BorderWidth = Unit.Pixel(0)
        Me.Controls.Add(oTbl)

        Dim oTblRow As New System.Web.UI.WebControls.TableRow
        oTbl.Rows.Add(oTblRow)

        Dim oTblCell As New System.Web.UI.WebControls.TableCell
        oTblCell.Width = Unit.Percentage(100)
        oTblRow.Cells.Add(oTblCell)

        Dim oTblInput As New System.Web.UI.WebControls.Table
        oTblInput.ID = "_t"
        oTblInput.Width = Me.Width
        oTblInput.CellPadding = 0
        oTblInput.CellSpacing = 0
        oTblInput.BorderWidth = Unit.Pixel(0)
        oTblCell.Controls.Add(oTblInput)

        Dim oTblRowInput As New System.Web.UI.WebControls.TableRow
        oTblInput.Rows.Add(oTblRowInput)

        Dim oTblCellInput As New System.Web.UI.WebControls.TableCell
        oTblCellInput.Width = Unit.Percentage(95)
        oTblCellInput.BorderWidth = Unit.Pixel(0)
        oTblRowInput.Cells.Add(oTblCellInput)

        Dim oTxt As New System.Web.UI.WebControls.TextBox
        oTxt.ID = "_t_t"
        Dim oHid As New System.Web.UI.HtmlControls.HtmlInputHidden
        oHid.ID = "_h"
        oTxt.Rows = 1
        oTxt.Height = Unit.Pixel(16)
        oTxt.Width = Unit.Percentage(100)
        If Not _valor Is Nothing Then oTxt.Text = _valor.ToString
        If Not _text Is Nothing Then oTxt.Text = _text.ToString
        oTxt.Style.Item("overflow") = "hidden"
        oTxt.Style.Add("resize", "none")
        If _readOnly Then
            oTxt.Attributes("READONLY") = _readOnly
            oTxt.Style.Item("background-color") = "transparent"
            oTxt.BorderStyle = BorderStyle.None
            oTxt.BorderWidth = Unit.Pixel(0)
            If Not _valor Is Nothing Then
                oHid.Value = _valor.ToString
            End If
        End If
        oTblCellInput.Controls.Add(oTxt)
        oTblCellInput.Controls.Add(oHid)

        If Not _bPortal AndAlso Not _readOnly Then
            Dim sFilasAutoCompleteExtender As String = ConfigurationManager.AppSettings("FilasAutoCompleteExtender")

            'Extender para sugerir valores
            Dim oExtender As New AutoCompleteExtender()
            With oExtender
                .ID = "_t_ext"
                .CompletionSetCount = IIf(Not String.IsNullOrEmpty(sFilasAutoCompleteExtender), CType(sFilasAutoCompleteExtender, Integer), 10)
                .DelimiterCharacters = ""
                .Enabled = True
                .MinimumPrefixLength = 2
                .CompletionListCssClass = "autoCompleteList"
                .CompletionListHighlightedItemCssClass = "autoCompleteSelectedListItem"
                .CompletionListItemCssClass = "autoCompleteListItem"
                .ServiceMethod = "Obtener_Datos_ListaExterna_Extender"
                .ServicePath = ConfigurationManager.AppSettings("rutaPM") & "AutoCompletePMWEB.asmx"
                .TargetControlID = oTxt.ID
                .EnableCaching = False
                .UseContextKey = True
                .OnClientShown = "resetPosition"
                .OnClientPopulated = "onCodListPopulated"
                .OnClientItemSelected = "onListaExternaExtenderItemSelected"
                .CompletionInterval = 500
            End With
            oTblCellInput.Controls.Add(oExtender)

            Dim oTblCellBut As New System.Web.UI.WebControls.TableCell
            oTblCellBut.Width = Unit.Percentage(5)
            oTblCellBut.HorizontalAlign = HorizontalAlign.Right
            oTblCellBut.VerticalAlign = VerticalAlign.Top
            oTblCellBut.Style.Add("cursor", "pointer")
            oTblCellBut.Style.Add("padding-left", "10px")
            oTblRowInput.Cells.Add(oTblCellBut)

            Dim oImgEditor As New System.Web.UI.WebControls.HyperLink
            oImgEditor.ID = "_t_img"
            oImgEditor.ImageUrl = Me.Page.ClientScript.GetWebResourceUrl(GetType(GeneralEntry), "Fullstep.DataEntry.Buscador.png")
            oImgEditor.NavigateUrl = "javascript:void(null)"
            oTblCellBut.Controls.Add(oImgEditor)
        End If
    End Sub
    ''' Revisado por: blp. Fecha: 04/10/2011
    ''' <summary>
    ''' M�todo que vac�a de contenido el dropdown.
    ''' Se lanza en el evento databound
    ''' </summary>
    ''' <param name="sender">El control webdropdown</param>
    ''' <param name="e">argumentos del evento</param>
    ''' <remarks>Llamada desde: CtrlGSTipoTipoPedido() y CtrlTipoLista. M�x 0,1 seg</remarks>
    Private Sub DropDown_DataBound(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim oDropDown As Infragistics.Web.UI.ListControls.WebDropDown = CType(sender, Infragistics.Web.UI.ListControls.WebDropDown)
        oDropDown.CurrentValue = ""
        oDropDown.SelectedValue = Nothing
    End Sub
    ''' Revisado por: blp. Fecha: 04/10/2011
    ''' <summary>
    ''' Esta funci�n hace lo mismo q las demas, crear la "estructura" necesario para el entry Desglose
    ''' Para Importe Repercutido(TipoGS = 45)
    ''' Se debe guardar no solo el valor del entry, tambien debe ir informaci�n de la moneda y del cambio.
    ''' </summary>
    ''' <remarks>Llamada desde: CreateChildControls. M�x: 0,1 seg</remarks>
    Private Sub CtrlGSTipoDesglose()
        Dim oTbl As System.Web.UI.WebControls.Table
        Dim oTblRow As System.Web.UI.WebControls.TableRow
        Dim oTblCell As System.Web.UI.WebControls.TableCell
        Dim oLbl As Infragistics.WebUI.WebDataInput.WebTextEdit
        Dim oHid As System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oHidLineasGrid As System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oHidLineasVinc As System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oWidth As System.Web.UI.WebControls.Unit
        Dim oRow As DataRow
        Dim sTblLineas As String = "LINEAS"
        Dim sTblEstados As String = ""

        Try
            If Not (Me.Lista.Tables("LINEAS_SIN") Is Nothing) Then
                sTblLineas = "LINEAS_SIN"
            End If
            If Not (Me.Lista.Tables("ESTADO_SIN") Is Nothing) Then
                sTblEstados = "ESTADO_SIN"
            ElseIf Not (Me.Lista.Tables("ESTADO") Is Nothing) Then
                sTblEstados = "ESTADO"
            End If
        Catch ex As Exception
            Exit Sub
        End Try

        oTbl = New System.Web.UI.WebControls.Table
        oWidth = Me.Width
        oTbl.Width = oWidth
        oTbl.BorderWidth = Unit.Pixel(0)
        oTbl.CellPadding = 0
        oTbl.CellSpacing = 0

        oLbl = New Infragistics.WebUI.WebDataInput.WebTextEdit
        oLbl.Width = oWidth
        oLbl.BorderWidth = Unit.Pixel(0)
        oLbl.BorderStyle = BorderStyle.None
        If _valor <> "" Then
            oLbl.Text = _valor
        End If
        oLbl.ReadOnly = True
        oLbl.ID = "_t"
        oLbl.Style("display") = ""

        oLbl.ButtonsAppearance.CustomButtonDisplay = Infragistics.WebUI.WebDataInput.ButtonDisplay.OnRight
        oLbl.ButtonsAppearance.CustomButtonImageUrl = Me.Page.ClientScript.GetWebResourceUrl(GetType(GeneralEntry), "Fullstep.DataEntry.trespuntos.gif")

        oTblRow = New System.Web.UI.WebControls.TableRow
        oTblCell = New System.Web.UI.WebControls.TableCell
        oTblCell.Width = Unit.Percentage(100)
        oTblCell.Controls.Add(oLbl)
        oTblRow.Cells.Add(oTblCell)

        oTbl.Rows.Add(oTblRow)

        oTbl.ID = "_tbl"

        Dim iLineas As Integer
        Dim iMaxIndex As Integer = 0
        Dim bHiddenDeleted As Boolean = False
        Dim i As Integer
        Dim distinctCounts As IEnumerable(Of Int32) = Nothing
        If Not Me.Lista Is Nothing Then
            oHid = New System.Web.UI.HtmlControls.HtmlInputHidden
            oHidLineasGrid = New System.Web.UI.HtmlControls.HtmlInputHidden
            oHidLineasVinc = New System.Web.UI.HtmlControls.HtmlInputHidden

            oHid.ID = "_numRows"
            oHidLineasGrid.ID = "_numRowsGrid"
            oHidLineasVinc.ID = "_numRowsVinc"

            Dim numInstancia As Long
            If Not TryCast(Page.FindControl("Instancia"), HtmlControls.HtmlInputHidden) Is Nothing Then
                Try
                    numInstancia = CLng(TryCast(Page.FindControl("Instancia"), HtmlControls.HtmlInputHidden).Value)
                Catch ex As Exception
                    'certif automatico tiene Hidden Instancia pero lo pone a ""
                    numInstancia = 0
                End Try
            Else
                numInstancia = 0
            End If
            If Me.Lista.Tables(0).Rows.Count = 0 Then
                iLineas = 0
                oHidLineasGrid.Value = 0
                oHidLineasVinc.Value = 0
                iMaxIndex = 0
            Else
                If numInstancia <> 0 Then
                    If Me.Lista.Tables(sTblLineas).Rows.Count = 0 Then
                        iLineas = 0
                        iMaxIndex = iLineas
                    Else
                        iMaxIndex = Me.Lista.Tables(sTblLineas).Select("", "LINEA DESC")(0).Item("LINEA")
                        distinctCounts = From row In Me.Lista.Tables(sTblLineas)
                                         Select row.Field(Of Int32)("LINEA")
                                         Distinct
                        iLineas = distinctCounts.Count
                    End If
                Else
                    iLineas = Me.Lista.Tables(sTblLineas).Rows.Count / Me.Lista.Tables(0).Rows.Count
                    For Each oRow In Me.Lista.Tables(sTblLineas).Rows
                        If iLineas < oRow.Item("LINEA") Then
                            iLineas = oRow.Item("LINEA")
                        End If
                    Next
                    iMaxIndex = iLineas
                End If


                oHidLineasGrid.Value = Me.Lista.Tables("LINEAS").Rows.Count
                oHidLineasVinc.Value = iMaxIndex
            End If
            oHid.Value = iMaxIndex
            Dim oDiv As New System.Web.UI.HtmlControls.HtmlGenericControl("div")
            Dim oDivLineas As New System.Web.UI.HtmlControls.HtmlGenericControl("div")
            oDiv.ID = "_divsource"
            oDiv.Style("visibility") = "hidden;"
            oDiv.Style("position") = "absolute;"
            oDiv.Style("clip") = "rect(0 0 0 0);"

            oTblCell.Controls.Add(oDiv)


            oDivLineas = New System.Web.UI.HtmlControls.HtmlGenericControl("div")
            oDivLineas.ID = "_div"
            oDivLineas.Style("visibility") = "hidden;"
            oDivLineas.Style("position") = "absolute;"
            oDivLineas.Style("clip") = "rect(0 0 0 0);"



            Dim oHidDesglose As System.Web.UI.HtmlControls.HtmlInputHidden
            Dim oHidDesgloseReper As System.Web.UI.HtmlControls.HtmlInputHidden
            Dim oHidDesgloseCombo As System.Web.UI.HtmlControls.HtmlInputHidden
            Dim oHidDesgloseIdSolicVinc As System.Web.UI.HtmlControls.HtmlInputHidden

            oHidDesglose = New System.Web.UI.HtmlControls.HtmlInputHidden
            oHidDesglose.ID = "_Deleted"
            oHidDesglose.Value = "no"
            oDiv.Controls.Add(oHidDesglose)
            _IdCamposHijos = "["
            _IdCamposHijosOrigen = "["
            _IdCamposHijosCampoDef = "["
            Dim lAux As Long
            For Each oRow In Me.Lista.Tables(0).Rows
                _IdCamposHijos += """" + oRow.Item("ID").ToString + ""","

                If DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.IdsFicticios.DesgloseVinculado Then
                    _IdCamposHijosCampoDef += """" + oRow.Item("ID").ToString + "xx-1" + ""","
                Else
                    Try
                        _IdCamposHijosCampoDef += """" + oRow.Item("ID").ToString + "xx" + oRow.Item("COPIA_CAMPO_DEF").ToString + ""","
                    Catch ex As Exception
                        'No hay instancia luego no hay copia_campo_def q es lo q recoge esta variable
                        _IdCamposHijosCampoDef += """" + oRow.Item("ID").ToString + "xx0" + ""","
                    End Try
                End If

                lAux = DBNullToDbl(oRow.Item("CAMPO_ORIGEN"))
                If lAux > 0 Then
                    _IdCamposHijosOrigen += """" + lAux.ToString + ""","
                End If
                oHidDesglose = New System.Web.UI.HtmlControls.HtmlInputHidden
                oHidDesglose.ID = "_" + oRow.Item("ID").ToString
                oHidDesglose.Attributes.Add("obligatorio", DBNullToSomething(oRow.Item("OBLIGATORIO")))
                oHidDesglose.Attributes.Add("tipo", DBNullToSomething(oRow.Item("SUBTIPO")))
                oHidDesglose.Attributes.Add("intro", DBNullToSomething(oRow.Item("INTRO")))
                oHidDesglose.Attributes.Add("tipoGS", IIf(DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) = Nothing, "0", oRow.Item("TIPO_CAMPO_GS")))
                oHidDesglose.Attributes.Add("articuloGenerico", False)
                oHidDesglose.Attributes.Add("DenArticuloModificado", True)
                oHidDesglose.Attributes.Add("codigoArticulo", "")
                oHidDesglose.Attributes.Add("IdAtrib", IIf(DBNullToSomething(oRow.Item("ID_ATRIB_GS")) = Nothing, "0", oRow.Item("ID_ATRIB_GS")))
                oHidDesglose.Attributes.Add("ValErp", IIf(DBNullToSomething(oRow.Item("VALIDACION_ERP")) = Nothing, "0", oRow.Item("VALIDACION_ERP")))
                oHidDesglose.Attributes.Add("escritura", DBNullToSomething(oRow.Item("ESCRITURA")))
                oHidDesglose.Attributes.Add("calculado", IIf((oRow.Item("TIPO") = 3 Or oRow.Item("SUBTIPO") = FSNLibrary.TiposDeDatos.TipoGeneral.TipoNumerico) And Not IsDBNull(oRow.Item("ID_CALCULO")), 1, 0))
                oHidDesglose.Attributes.Add("TablaExterna", IIf(IsDBNull(oRow.Item("TABLA_EXTERNA")), 0, oRow.Item("TABLA_EXTERNA")))
                oHidDesglose.Attributes.Add("title", DBNullToSomething(oRow.Item("DEN_" & Idi)))
                oHidDesglose.Attributes.Add("cargarUltADJ", DBNullToSomething(oRow.Item("CARGAR_ULT_ADJ")))

                oDiv.Controls.Add(oHidDesglose)

                If oRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoArchivo Then
                    oHidDesglose = New System.Web.UI.HtmlControls.HtmlInputHidden
                    oHidDesglose.ID = "_" + oRow.Item("ID").ToString + "_hNew"
                    oDiv.Controls.Add(oHidDesglose)
                    oHidDesglose = New System.Web.UI.HtmlControls.HtmlInputHidden
                    oHidDesglose.ID = "_" + oRow.Item("ID").ToString + "_hNombre"
                    oDiv.Controls.Add(oHidDesglose)

                End If

                If (oRow.Item("TIPO") = 3 Or oRow.Item("SUBTIPO") = FSNLibrary.TiposDeDatos.TipoGeneral.TipoNumerico) And Not IsDBNull(oRow.Item("ID_CALCULO")) Then
                    If Not Page.ClientScript.IsStartupScriptRegistered(Page.GetType(), CamposCalculadosScriptKey + Me.IdContenedor + "_" + oRow.Item("ID").ToString()) Then _
                        Page.ClientScript.RegisterStartupScript(Page.GetType(), CamposCalculadosScriptKey + Me.IdContenedor + "_" + oRow.Item("ID").ToString(), String.Format(IncludeScriptKeyFormat, "javascript", "arrCalculados[arrCalculados.length]='" + Me.IdContenedor + "_" + Me.ID + "__" + oRow.Item("ID").ToString + "'"))
                End If

                If DBNullToSomething(oRow.Item("OBLIGATORIO")) = 1 Then
                    If Not Page.ClientScript.IsStartupScriptRegistered(Page.GetType(), CamposObligatoriosScriptKey + Me.IdContenedor + "_" + Me.ID + "__" + oRow.Item("ID").ToString) Then _
                        Page.ClientScript.RegisterStartupScript(Page.GetType(), CamposObligatoriosScriptKey + Me.IdContenedor + "_" + Me.ID + "__" + oRow.Item("ID").ToString, String.Format(IncludeScriptKeyFormat, "javascript", "arrObligatorios[arrObligatorios.length]='" + Me.IdContenedor + "_" + Me.ID + "__" + oRow.Item("ID").ToString + "'"))
                    If Not Page.ClientScript.IsStartupScriptRegistered(Page.GetType(), CamposObligatoriosDenScriptKey + Me.IdContenedor + "_" + Me.ID + "__" + oRow.Item("ID").ToString) Then _
                        Page.ClientScript.RegisterStartupScript(Page.GetType(), CamposObligatoriosDenScriptKey + Me.IdContenedor + "_" + Me.ID + "__" + oRow.Item("ID").ToString, String.Format(IncludeScriptKeyFormat, "javascript", "arrObligatoriosDen[arrObligatoriosDen.length]='" + oRow.Item("DEN_" + Me.Idi.ToString).ToString + "'"))
                End If


                Dim oDataRows As DataRowCollection = Me.Lista.Tables(sTblLineas).Rows
                Dim oDataTable As DataTable = Me.Lista.Tables(sTblLineas)
                Dim keys(2) As DataColumn
                keys(0) = oDataTable.Columns("LINEA")
                keys(1) = oDataTable.Columns("CAMPO_PADRE")
                keys(2) = oDataTable.Columns("CAMPO_HIJO")

                oDataTable.PrimaryKey = keys
                Dim findTheseVals(2) As Object

                'Portal. Por si resulta que hay estado actual
                Dim oDataRowsEstado As DataRowCollection = Nothing
                Dim oDataTableEstado As DataTable
                Dim keysEstado(1) As DataColumn
                Dim findEstadoVals(1) As Object

                If sTblEstados <> "" Then
                    oDataRowsEstado = Me.Lista.Tables(sTblEstados).Rows
                    oDataTableEstado = Me.Lista.Tables(sTblEstados)

                    keysEstado(0) = oDataTableEstado.Columns("LINEA")
                    keysEstado(1) = oDataTableEstado.Columns("CAMPO_PADRE")
                    oDataTableEstado.PrimaryKey = keysEstado
                End If

                For i = 1 To iMaxIndex
                    If numInstancia = 0 OrElse distinctCounts.Contains(i) Then
                        If Not bHiddenDeleted Then
                            oHidDesglose = New System.Web.UI.HtmlControls.HtmlInputHidden
                            oHidDesglose.ID = "_" + i.ToString() + "__Deleted"
                            oHidDesglose.Value = "no"
                            oDivLineas.Controls.Add(oHidDesglose)
                            oHidDesglose = New System.Web.UI.HtmlControls.HtmlInputHidden
                            oHidDesglose.ID = "_" + i.ToString() + "__Linea"
                            oHidDesglose.Value = i
                            oDivLineas.Controls.Add(oHidDesglose)
                            oHidDesglose = New System.Web.UI.HtmlControls.HtmlInputHidden
                            oHidDesglose.ID = "_" + i.ToString() + "__LineaVinc"
                            oHidDesglose.Value = i
                            oDivLineas.Controls.Add(oHidDesglose)
                            oHidDesglose = New System.Web.UI.HtmlControls.HtmlInputHidden
                            oHidDesglose.ID = "_" + i.ToString() + "__LineaOldVinc"
                            oHidDesglose.Value = i
                            oDivLineas.Controls.Add(oHidDesglose)
                            oHidDesglose = New System.Web.UI.HtmlControls.HtmlInputHidden
                            oHidDesglose.ID = "_" + i.ToString() + "__LineaVincMovible"
                            oHidDesglose.Value = IIf(_Movible, "##Si##", "##No##")
                            oDivLineas.Controls.Add(oHidDesglose)
                        End If

                        oHidDesglose = New System.Web.UI.HtmlControls.HtmlInputHidden
                        oHidDesglose.ID = "_" + i.ToString() + "__" + oRow.Item("ID").ToString

                        findTheseVals(0) = i
                        findTheseVals(1) = Me.Tag
                        findTheseVals(2) = oRow.Item("ID")

                        Dim oDataRow As DataRow

                        oDataRow = oDataRows.Find(findTheseVals)

                        'Siempre hay que crear el hidden aunque oDataRow sea Nothing
                        Select Case oRow.Item("SUBTIPO")
                            Case FSNLibrary.TiposDeDatos.TipoGeneral.TipoString, FSNLibrary.TiposDeDatos.TipoGeneral.TipoTextoCorto, FSNLibrary.TiposDeDatos.TipoGeneral.TipoTextoLargo, FSNLibrary.TiposDeDatos.TipoGeneral.TipoTextoMedio
                                If oRow.Item("INTRO") = 1 AndAlso DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) = 0 Then
                                    oHidDesgloseCombo = New System.Web.UI.HtmlControls.HtmlInputHidden
                                    oHidDesgloseCombo.ID = "_" + i.ToString() + "__" + oRow.Item("ID").ToString + "_DescrComboPopUp"
                                    oHidDesgloseCombo.Value = oHidDesglose.Value
                                    oDivLineas.Controls.Add(oHidDesgloseCombo)
                                End If
                        End Select

                        If Not oDataRow Is Nothing Then
                            Select Case oRow.Item("SUBTIPO")
                                Case FSNLibrary.TiposDeDatos.TipoGeneral.TipoString, FSNLibrary.TiposDeDatos.TipoGeneral.TipoTextoCorto, FSNLibrary.TiposDeDatos.TipoGeneral.TipoTextoLargo, FSNLibrary.TiposDeDatos.TipoGeneral.TipoTextoMedio
                                    oHidDesglose.Value = DBNullToSomething(oDataRow.Item("VALOR_TEXT"))
                                    If oRow.Item("INTRO") = 1 Then
                                        If DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) = 0 Then
                                            oHidDesglose.Value = DBNullToSomething(oDataRow.Item("VALOR_NUM"))
                                        End If
                                    End If
                                    If oHidDesglose.Value = Nothing And DBNullToSomething(oDataRow.Item("VALOR_NUM")) <> Nothing Then
                                        oHidDesglose.Value = DBNullToSomething(oDataRow.Item("VALOR_NUM"))
                                    End If
                                Case FSNLibrary.TiposDeDatos.TipoGeneral.TipoNumerico
                                    'Problema de la aplicacion que compara por subtipo y los presupuestos son numericos
                                    If DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) = FSNLibrary.TiposDeDatos.TipoCampoGS.PRES1 Or DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) = FSNLibrary.TiposDeDatos.TipoCampoGS.Pres2 Or DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) = FSNLibrary.TiposDeDatos.TipoCampoGS.Pres3 Or DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) = FSNLibrary.TiposDeDatos.TipoCampoGS.Pres4 Then
                                        oHidDesglose.Value = DBNullToSomething(oDataRow.Item("VALOR_TEXT"))
                                    Else
                                        oHidDesglose.Value = DBNullToSomething(oDataRow.Item("VALOR_NUM"))
                                        'Para solucionar el problema de los decimales en el desglose.
                                        'Cuando el control oHidDesglose (HtmlInputHidden) carga el n�mero double de BBDD y lo pasa a string
                                        'los decimales los pone por defecto con el formato del sistema.
                                        'Aqu� sustituimos el caracter decimal del sistema por el nuestro, que es "."
                                        oHidDesglose.Value = oHidDesglose.Value.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator, ".")
                                    End If
                                Case FSNLibrary.TiposDeDatos.TipoGeneral.TipoFecha
                                    Dim iTipoFecha As FSNLibrary.TiposDeDatos.TipoFecha
                                    iTipoFecha = DBNullToSomething(oDataRow.Item("VALOR_NUM"))
                                    If iTipoFecha = 0 And IsDBNull(oDataRow.Item("VALOR_FEC")) Then

                                    Else
                                        oHidDesglose.Value = modUtilidades.DateToigDate(DevolverFechaRelativaA(iTipoFecha, DBNullToSomething(oDataRow.Item("VALOR_FEC"))))

                                    End If
                                Case FSNLibrary.TiposDeDatos.TipoGeneral.TipoArchivo
                                    Dim idAdjun As String
                                    Dim sNombreAdjun As String
                                    Dim oHidDesgloseAdjunNombre As New System.Web.UI.HtmlControls.HtmlInputHidden
                                    Dim oDSRowAdjun As DataRow
                                    idAdjun = ""
                                    sNombreAdjun = ""
                                    For Each oDSRowAdjun In oDataRow.GetChildRows("REL_LINEA_ADJUNTO")
                                        idAdjun += oDSRowAdjun.Item("ID").ToString() + "xx"
                                        sNombreAdjun += oDSRowAdjun.Item("NOM").ToString() + ", "
                                    Next
                                    If idAdjun <> "" Then
                                        idAdjun = idAdjun.Substring(0, idAdjun.Length - 2)
                                        sNombreAdjun = sNombreAdjun.Substring(0, sNombreAdjun.Length - 2)
                                    End If
                                    oHidDesglose.Value = idAdjun
                                    oHidDesgloseAdjunNombre.ID = "_" + i.ToString() + "__" + oRow.Item("ID").ToString + "_hNombre"
                                    oHidDesgloseAdjunNombre.Value = sNombreAdjun
                                    oDivLineas.Controls.Add(oHidDesgloseAdjunNombre)
                                Case FSNLibrary.TiposDeDatos.TipoGeneral.TipoBoolean
                                    oHidDesglose.Value = DBNullToSomething(oDataRow.Item("VALOR_BOOL"))
                            End Select

                            If DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.ImporteRepercutido Then
                                oHidDesgloseReper = New System.Web.UI.HtmlControls.HtmlInputHidden
                                oHidDesgloseReper.ID = "_" + i.ToString() + "__" + oRow.Item("ID").ToString + "_MonRepercutido"
                                If IsDBNull(oDataRow.Item("VALOR_TEXT")) Then 'pm
                                    oHidDesgloseReper.Value = DBNullToSomething(oDataRow.Item("MONCEN"))
                                Else 'portal
                                    oHidDesgloseReper.Value = DBNullToSomething(oDataRow.Item("VALOR_TEXT"))
                                End If
                                oDivLineas.Controls.Add(oHidDesgloseReper)
                                oHidDesgloseReper = New System.Web.UI.HtmlControls.HtmlInputHidden
                                oHidDesgloseReper.ID = "_" + i.ToString() + "__" + oRow.Item("ID").ToString + "_CambioRepercutido"
                                oHidDesgloseReper.Value = DBNullToSomething(oDataRow.Item("CAMBIO"))
                                'Para solucionar el problema de los decimales en el desglose.
                                'Cuando el control oHidDesglose (HtmlInputHidden) carga el n�mero double de BBDD y lo pasa a string
                                'los decimales los pone por defecto con el formato del sistema.
                                'Aqu� sustituimos el caracter decimal del sistema por el nuestro, que es "."
                                oHidDesgloseReper.Value = oHidDesgloseReper.Value.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator, ".")
                                oDivLineas.Controls.Add(oHidDesgloseReper)

                                oHidDesglose.Attributes.Add("tipoGS", IIf(DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) = Nothing, "0", oRow.Item("TIPO_CAMPO_GS")))
                            End If

                            If DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.IdsFicticios.DesgloseVinculado Then
                                oHidDesgloseIdSolicVinc = New System.Web.UI.HtmlControls.HtmlInputHidden
                                oHidDesgloseIdSolicVinc.ID = "_" + i.ToString() + "__" + oRow.Item("ID").ToString + "_IdSolicVinculada"
                                oHidDesgloseIdSolicVinc.Value = DBNullToSomething(oDataRow.Item("ORIGEN"))
                                oDivLineas.Controls.Add(oHidDesgloseIdSolicVinc)

                                oHidDesglose.Attributes.Add("tipoGS", TiposDeDatos.IdsFicticios.DesgloseVinculado)
                            End If

                        Else
                            If sTblEstados <> "" Then
                                'Portal. Por si resulta que es un estado actual. Resulta que en estos
                                'popup como no despliege y guarde los cambios, me desaparece como grabe.
                                'Y esto es tema de no tenerlo en ningun oHidDesglose inicialmente.
                                Dim oDataRowEstado As DataRow

                                findEstadoVals(0) = i
                                findEstadoVals(1) = Me.Tag 'oRow.Item("ID")

                                oDataRowEstado = oDataRowsEstado.Find(findEstadoVals)
                                If Not oDataRowEstado Is Nothing Then
                                    oHidDesglose.Value = DBNullToSomething(oDataRowEstado.Item("ESTADO"))
                                End If
                            End If

                            If DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.ImporteRepercutido Then
                                oHidDesgloseReper = New System.Web.UI.HtmlControls.HtmlInputHidden
                                oHidDesgloseReper.ID = "_" + i.ToString() + "__" + oRow.Item("ID").ToString + "_MonRepercutido"
                                oHidDesgloseReper.Value = DBNullToSomething(oRow.Item("MONCEN"))
                                oDivLineas.Controls.Add(oHidDesgloseReper)
                                oHidDesgloseReper = New System.Web.UI.HtmlControls.HtmlInputHidden
                                oHidDesgloseReper.ID = "_" + i.ToString() + "__" + oRow.Item("ID").ToString + "_CambioRepercutido"
                                oHidDesgloseReper.Value = DBNullToSomething(oRow.Item("CAMBIOCEN"))
                                'Para solucionar el problema de los decimales en el desglose.
                                'Cuando el control oHidDesglose (HtmlInputHidden) carga el n�mero double de BBDD y lo pasa a string
                                'los decimales los pone por defecto con el formato del sistema.
                                'Aqu� sustituimos el caracter decimal del sistema por el nuestro, que es "."
                                oHidDesgloseReper.Value = oHidDesgloseReper.Value.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator, ".")
                                oDivLineas.Controls.Add(oHidDesgloseReper)

                                oHidDesglose.Attributes.Add("tipoGS", IIf(DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) = Nothing, "0", oRow.Item("TIPO_CAMPO_GS")))
                            End If

                            If DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.IdsFicticios.DesgloseVinculado Then
                                oHidDesgloseIdSolicVinc = New System.Web.UI.HtmlControls.HtmlInputHidden
                                oHidDesgloseIdSolicVinc.ID = "_" + i.ToString() + "__" + oRow.Item("ID").ToString + "_IdSolicVinculada"
                                oHidDesgloseIdSolicVinc.Value = ""
                                oDivLineas.Controls.Add(oHidDesgloseIdSolicVinc)

                                oHidDesglose.Attributes.Add("tipoGS", TiposDeDatos.IdsFicticios.DesgloseVinculado)
                            End If
                        End If

                        oDivLineas.Controls.Add(oHidDesglose)

                        If oRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoArchivo Then
                            oHidDesglose = New System.Web.UI.HtmlControls.HtmlInputHidden
                            oHidDesglose.ID = "_" + i.ToString() + "__" + oRow.Item("ID").ToString + "_hNew"
                            oDivLineas.Controls.Add(oHidDesglose)
                        End If
                    End If
                Next
                bHiddenDeleted = True

            Next

            If _IdCamposHijos.Length > 1 Then
                _IdCamposHijos = Left(_IdCamposHijos, _IdCamposHijos.Length - 1) + "]"
            Else
                _IdCamposHijos = Nothing
            End If

            If _IdCamposHijosCampoDef.Length > 1 Then
                _IdCamposHijosCampoDef = Left(_IdCamposHijosCampoDef, _IdCamposHijosCampoDef.Length - 1) + "]"
            Else
                _IdCamposHijosCampoDef = Nothing
            End If

            If _IdCamposHijosOrigen.Length > 1 Then
                _IdCamposHijosOrigen = Left(_IdCamposHijosOrigen, _IdCamposHijosOrigen.Length - 1) + "]"
            Else
                _IdCamposHijosOrigen = Nothing
            End If

            oDivLineas.Controls.Add(oHid)
            oDivLineas.Controls.Add(oHidLineasGrid)
            oDivLineas.Controls.Add(oHidLineasVinc)

            oHid = New System.Web.UI.HtmlControls.HtmlInputHidden

            oHid.ID = "_numTotRows"
            oHid.Value = iMaxIndex
            oDivLineas.Controls.Add(oHid)

            oTblCell.Controls.Add(oDivLineas)
        End If

        Controls.Add(oTbl)
    End Sub
    ''' <summary>
    ''' Dataentry de tipo Desglose de Actividad
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CtrlGSDesgloseActividad()
        Dim oHid As New System.Web.UI.HtmlControls.HtmlInputHidden

        oHid.ID = "_tbl"
        oHid.Value = _valor
        Me.Controls.Add(oHid)
    End Sub
    ''' <summary>
    '''   Esta funci�n hace lo mismo q las demas, crear la "estructura" necesario para el entry Presupuesto (tipoGs=110,111,112,113)
    ''' </summary>
    ''' <remarks>Llamada desde: CreateChildControls; Tiempo m�ximo: 0</remarks>
    Private Sub CtrlGSTipoPresupuesto(ByVal iNivel As Integer)
        Dim oTbl As System.Web.UI.WebControls.Table
        Dim oTblRow As System.Web.UI.WebControls.TableRow
        Dim oTblCell As System.Web.UI.WebControls.TableCell
        Dim oTblCell2 As System.Web.UI.WebControls.TableCell
        Dim oLbl As Infragistics.WebUI.WebDataInput.WebTextEdit
        Dim oLbl2 As Infragistics.WebUI.WebDataInput.WebTextEdit
        Dim oHid As System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oWidth As System.Web.UI.WebControls.Unit

        oWidth = Me.Width

        oHid = New System.Web.UI.HtmlControls.HtmlInputHidden

        oLbl = New Infragistics.WebUI.WebDataInput.WebTextEdit
        oLbl.BorderWidth = Unit.Pixel(0)
        oLbl.BorderStyle = BorderStyle.None
        oLbl2 = New Infragistics.WebUI.WebDataInput.WebTextEdit
        oLbl2.BorderWidth = Unit.Pixel(0)
        oLbl2.BorderStyle = BorderStyle.None
        If _toolTip = Nothing Then
            _toolTip = _text
        End If
        If _text <> Nothing Then
            oLbl.Text = _text.ToString

            oHid.Value = _valor

        End If

        oLbl.ReadOnly = True
        oLbl.ID = "_t"
        oHid.ID = "_h"
        oLbl.Width = oWidth
        oLbl2.Width = Unit.Percentage(0)
        oLbl2.ID = "_t2"
        oTbl = New System.Web.UI.WebControls.Table
        oTbl.Width = oWidth
        oTbl.BorderWidth = Unit.Pixel(0)
        oTbl.CellPadding = 0
        oTbl.CellSpacing = 0

        oLbl.BorderStyle = BorderStyle.None
        oLbl.BackColor = System.Drawing.Color.Transparent
        oLbl.Style("display") = ""
        oLbl.ReadOnly = _readOnly

        oTblRow = New System.Web.UI.WebControls.TableRow
        oTblCell = New System.Web.UI.WebControls.TableCell
        oTblCell2 = New System.Web.UI.WebControls.TableCell
        oTblCell.Width = Unit.Percentage(100)
        oTblCell2.Width = Unit.Percentage(0)
        oTblCell.Controls.Add(oLbl)
        oTblCell.Controls.Add(oHid)
        oTblCell2.Controls.Add(oLbl2)
        oTblRow.Cells.Add(oTblCell)
        If Me.PM = True Then
            oTblRow.Cells.Add(oTblCell2)
        End If

        If Not _readOnly Or (_readOnly And Not _valor Is Nothing And _valor <> "") Then
            oLbl.ButtonsAppearance.CustomButtonDisplay = Infragistics.WebUI.WebDataInput.ButtonDisplay.OnRight
            oLbl.ButtonsAppearance.CustomButtonImageUrl = Me.Page.ClientScript.GetWebResourceUrl(GetType(GeneralEntry), "Fullstep.DataEntry.trespuntos.gif")
            If Not _readOnly Then
                oLbl2.ButtonsAppearance.CustomButtonDisplay = Infragistics.WebUI.WebDataInput.ButtonDisplay.OnRight
                oLbl2.ButtonsAppearance.CustomButtonImageUrl = Me.Page.ClientScript.GetWebResourceUrl(GetType(GeneralEntry), "Fullstep.DataEntry.favoritos.gif")
            End If
        End If
        oTbl.Rows.Add(oTblRow)
        oTbl.ID = "_tbl"


        Me.Controls.Add(oTbl)

    End Sub
    ''' <summary>
    '''   Esta funci�n hace lo mismo q las demas, crear la "estructura" necesario para el entry Proveedor (tipoGs=100)
    ''' </summary>
    ''' <remarks>Llamada desde: CreateChildControls; Tiempo m�ximo: 0</remarks>
    Private Sub CtrlGSTipoProveedor()
        Dim oTbl As System.Web.UI.WebControls.Table
        Dim oTblRow As System.Web.UI.WebControls.TableRow
        Dim oTblCell, oTblCell2 As System.Web.UI.WebControls.TableCell
        Dim oLbl, oLbl2 As Infragistics.WebUI.WebDataInput.WebTextEdit
        Dim oHid As System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oWidth As System.Web.UI.WebControls.Unit

        oWidth = Width

        oHid = New System.Web.UI.HtmlControls.HtmlInputHidden

        oLbl = New Infragistics.WebUI.WebDataInput.WebTextEdit
        oLbl.BorderWidth = Unit.Pixel(0)
        oLbl.BorderStyle = BorderStyle.None
        oLbl2 = New Infragistics.WebUI.WebDataInput.WebTextEdit
        oLbl2.BorderWidth = Unit.Pixel(0)
        oLbl2.BorderStyle = BorderStyle.None
        If _toolTip = Nothing AndAlso _text <> Nothing Then
            _toolTip = _text
        End If
        If _text <> Nothing Then
            oLbl.Text = _text.ToString

            oHid.Value = _valor

        End If

        oLbl.ReadOnly = True
        oLbl.ID = "_t"
        oHid.ID = "_h"
        oLbl.Width = oWidth
        oLbl.Style("display") = ""
        oLbl2.Width = Unit.Percentage(0)
        oLbl2.ID = "_t2"

        oTbl = New System.Web.UI.WebControls.Table
        oTbl.Width = oWidth
        oTbl.BorderWidth = Unit.Pixel(0)
        oTbl.CellPadding = 0
        oTbl.CellSpacing = 0

        oTblRow = New System.Web.UI.WebControls.TableRow
        oTblCell = New System.Web.UI.WebControls.TableCell
        oTblCell.Width = Unit.Percentage(100)
        oTblCell.Controls.Add(oLbl)
        oTblCell.Controls.Add(oHid)
        oTblRow.Cells.Add(oTblCell)

        If Me.PM = True AndAlso Not _readOnly Then
            oTblCell2 = New System.Web.UI.WebControls.TableCell
            oTblCell2.Width = Unit.Percentage(0)
            oTblCell2.Controls.Add(oLbl2)
            oTblRow.Cells.Add(oTblCell2)
        End If

        If Not _readOnly Then
            oLbl.ButtonsAppearance.CustomButtonDisplay = Infragistics.WebUI.WebDataInput.ButtonDisplay.OnRight
            oLbl.ButtonsAppearance.CustomButtonImageUrl = Me.Page.ClientScript.GetWebResourceUrl(GetType(GeneralEntry), "Fullstep.DataEntry.trespuntos.gif")
            If Not _bEsProveedorParticipante Then
                oLbl2.ButtonsAppearance.CustomButtonDisplay = Infragistics.WebUI.WebDataInput.ButtonDisplay.OnRight
                oLbl2.ButtonsAppearance.CustomButtonImageUrl = Me.Page.ClientScript.GetWebResourceUrl(GetType(GeneralEntry), "Fullstep.DataEntry.favoritos.gif")
            End If
        End If

        oTbl.Rows.Add(oTblRow)
        oTbl.ID = "_tbl"

        Controls.Add(oTbl)
    End Sub
    ''' <summary>
    '''   Esta funci�n hace lo mismo q las demas, crear la "estructura" necesario para el entry Articulo (tipoGs=104)
    ''' </summary>
    ''' <remarks>Llamada desde: CreateChildControls; Tiempo m�ximo: 0</remarks>
    Private Sub CtrlGSTipoArticulo()
        Dim oTbl As System.Web.UI.WebControls.Table
        Dim oTblRow As System.Web.UI.WebControls.TableRow
        Dim oTblCell As System.Web.UI.WebControls.TableCell
        Dim oTxt As Infragistics.WebUI.WebDataInput.WebTextEdit
        Dim oWidth As System.Web.UI.WebControls.Unit
        Dim oHid As System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oHidOrgCompras As System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oHidCentro As System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oHidUnidadOrganizativa As System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oHidUnidad As System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oHidUnidadPedido As System.Web.UI.HtmlControls.HtmlInputHidden

        oHid = New System.Web.UI.HtmlControls.HtmlInputHidden
        oHid.ID = "_hmat"
        oHidOrgCompras = New System.Web.UI.HtmlControls.HtmlInputHidden
        oHidOrgCompras.ID = "_hOrgCompras" 'Contiene el valor del dataEntryDependent
        oHidCentro = New System.Web.UI.HtmlControls.HtmlInputHidden
        oHidCentro.ID = "_hCentro" 'Contiene el valor del dataEntryDependent2
        oHidUnidadOrganizativa = New System.Web.UI.HtmlControls.HtmlInputHidden
        oHidUnidadOrganizativa.ID = "_hUON" 'Contiene el valor del dataEntryDependent3
        oHidUnidad = New System.Web.UI.HtmlControls.HtmlInputHidden
        oHidUnidad.ID = "_hUni" 'Contiene el valor de la Unidad
        oHidUnidadPedido = New System.Web.UI.HtmlControls.HtmlInputHidden
        oHidUnidadPedido.ID = "_hUniPed" 'Contiene el valor de la Unidad pedido

        If _DependentValue <> Nothing Then
            oHid.Value = _DependentValue
        End If
        If _DependentValueDataEntry <> Nothing Then
            oHidOrgCompras.Value = _DependentValueDataEntry
        End If
        If _DependentValueDataEntry2 <> Nothing Then
            oHidCentro.Value = _DependentValueDataEntry2
        End If
        If _DependentValueDataEntry3 <> Nothing Then
            oHidUnidadOrganizativa.Value = _DependentValueDataEntry3
        End If
        If _sUnidadOculta <> Nothing Then
            oHidUnidad.Value = _sUnidadOculta
        End If
        If _sUnidadPedidoOculta <> Nothing Then
            oHidUnidadPedido.Value = _sUnidadPedidoOculta
        End If

        oWidth = Me.Width
        oTbl = New System.Web.UI.WebControls.Table
        oTbl.Width = oWidth
        oTbl.BorderWidth = Unit.Pixel(0)
        oTbl.CellPadding = 0
        oTbl.CellSpacing = 0

        oTblRow = New System.Web.UI.WebControls.TableRow
        oTblCell = New System.Web.UI.WebControls.TableCell

        oTblCell.Width = Unit.Percentage(25)

        oTxt = New Infragistics.WebUI.WebDataInput.WebTextEdit
        oTxt.ID = "_t"
        oTxt.MaxLength = _maxLength

        If _valor <> Nothing Then
            oTxt.Text = _valor.ToString
        End If

        oTxt.ReadOnly = _readOnly
        If Not _readOnly Then
            oTxt.ButtonsAppearance.CustomButtonDisplay = Infragistics.WebUI.WebDataInput.ButtonDisplay.OnRight
        Else
            oTxt.Style.Add("visibility", "hidden")
            oTxt.Style.Add("position", "absolute")
            oTxt.Style.Add("top", "0")
            oTxt.Style.Add("clip", "rect (0 0 0 0)")
            oTxt.Height = Unit.Pixel(0)
            oTxt.Width = Unit.Pixel(0)
            oTxt.BorderStyle = BorderStyle.None
            oTxt.BorderWidth = Unit.Pixel(0)
            oTxt.BackColor = System.Drawing.Color.Transparent
        End If

        oTxt.Width = Unit.Percentage(100)

        oTblCell.Controls.Add(oTxt)
        oTblRow.Cells.Add(oTblCell)

        If _readOnly Then
            oTblCell.Width = Unit.Percentage(100)
        Else
            oTblCell = New System.Web.UI.WebControls.TableCell

            oTblCell.Width = Unit.Percentage(75)
        End If

        oTxt = New Infragistics.WebUI.WebDataInput.WebTextEdit
        oTxt.ID = "_tden"
        oTxt.MaxLength = 200
        oTxt.Width = Unit.Percentage(100)
        oTxt.Style("display") = ""
        If _text <> Nothing Then
            oTxt.Text = _text
        End If

        oTxt.ReadOnly = _readOnly
        If Not _readOnly Then
            oTxt.ButtonsAppearance.CustomButtonDisplay = Infragistics.WebUI.WebDataInput.ButtonDisplay.OnRight
        Else
            If _valor <> Nothing Then
                oTxt.Text = _valor.ToString + " - " + _text.ToString
            End If
            oTxt.BorderStyle = BorderStyle.None
            oTxt.BorderWidth = Unit.Pixel(0)
            oTxt.BackColor = System.Drawing.Color.Transparent
        End If
        oTblCell.Controls.Add(oTxt)
        oTblCell.Controls.Add(oHid)
        oTblCell.Controls.Add(oHidOrgCompras)
        oTblCell.Controls.Add(oHidCentro)
        oTblCell.Controls.Add(oHidUnidadOrganizativa)
        oTblCell.Controls.Add(oHidUnidad)
        oTblCell.Controls.Add(oHidUnidadPedido)
        oTblRow.Cells.Add(oTblCell)

        oTbl.Rows.Add(oTblRow)
        oTbl.ID = "_tbl"

        Controls.Add(oTbl)
    End Sub
    ''' <summary>
    '''   Esta funci�n hace lo mismo q las demas, crear la "estructura" necesario para el entry Articulo (tipoGs=119)
    ''' </summary>
    ''' <remarks>Llamada desde: CreateChildControls; Tiempo m�ximo: 0</remarks>
    Private Sub CtrlGSTipoEmpresa()
        Dim oTbl As System.Web.UI.WebControls.Table
        Dim oTblInput As System.Web.UI.WebControls.Table
        Dim oTblRow As System.Web.UI.WebControls.TableRow
        Dim oTblRowInput As System.Web.UI.WebControls.TableRow
        Dim oTblCell As System.Web.UI.WebControls.TableCell
        Dim oTblCellInput As System.Web.UI.WebControls.TableCell
        Dim oWidth As System.Web.UI.WebControls.Unit
        Dim oHid As System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oLbl As System.Web.UI.WebControls.TextBox

        oHid = New System.Web.UI.HtmlControls.HtmlInputHidden
        oHid.ID = "_h"

        oWidth = Me.Width
        oTbl = New System.Web.UI.WebControls.Table
        oTbl.Width = oWidth
        If Not _readOnly Then
            oTbl.CssClass = "TipoTextoMedio"
        End If

        oTbl.CellPadding = 0
        oTbl.CellSpacing = 0
        oTbl.ID = "_tbl"
        Controls.Add(oTbl)

        oTblRow = New System.Web.UI.WebControls.TableRow
        oTbl.Rows.Add(oTblRow)

        oTblCellInput = New System.Web.UI.WebControls.TableCell
        If Desglose Then
            oTblCellInput.Width = Unit.Pixel(200)
        Else
            oTblCellInput.Width = Unit.Percentage(100)
        End If
        oTblRow.Cells.Add(oTblCellInput)
        oTblInput = New System.Web.UI.WebControls.Table
        oTblInput.Width = Unit.Percentage(100)
        oTblInput.CellPadding = 0
        oTblInput.CellSpacing = 0
        oTblInput.ID = "_t"
        oTblCellInput.Controls.Add(oTblInput)
        oTblRowInput = New System.Web.UI.WebControls.TableRow
        oTblInput.Rows.Add(oTblRowInput)

        oLbl = New System.Web.UI.WebControls.TextBox
        oLbl.ID = "_t_t"
        oLbl.MaxLength = _maxLength
        If _readOnly Then
            oLbl.Attributes("READONLY") = _readOnly
            oLbl.Style.Item("background-color") = "transparent"
        End If

        If _toolTip = Nothing AndAlso _text <> Nothing Then
            _toolTip = _text
        End If
        If _text <> Nothing Then
            oLbl.Text = _text.ToString
            oHid.Value = _valor
        End If

        oLbl.Width = Unit.Percentage(99)
        oLbl.BorderWidth = Unit.Pixel(0)
        oLbl.BorderStyle = BorderStyle.None
        oLbl.Style("display") = ""
        oLbl.ReadOnly = _readOnly

        oTblCell = New System.Web.UI.WebControls.TableCell
        oTblCell.Controls.Add(oLbl)
        oTblCell.Controls.Add(oHid)

        If Not _readOnly Then
            Dim sFilasAutoCompleteExtender As String = ConfigurationManager.AppSettings("FilasAutoCompleteExtender")
            Dim oExtender As New AutoCompleteExtender()
            With oExtender
                .ID = "_t_ext"
                .CompletionSetCount = IIf(Not String.IsNullOrEmpty(sFilasAutoCompleteExtender), CType(sFilasAutoCompleteExtender, Integer), 10)
                .DelimiterCharacters = ""
                .Enabled = True
                .MinimumPrefixLength = 3
                .CompletionListCssClass = "autoCompleteList"
                .CompletionListHighlightedItemCssClass = "autoCompleteSelectedListItem"
                .CompletionListItemCssClass = "autoCompleteListItem"
                .ServiceMethod = "DevolverEmpresas"
                .ServicePath = IIf(_bPortal, ConfigurationManager.AppSettings("rutanormal") & "Consultas.asmx", ConfigurationManager.AppSettings("rutaPM") & "AutoCompletePMWEB.asmx")
                .TargetControlID = oLbl.ID
                .EnableCaching = False
                .UseContextKey = True
                .OnClientShown = "resetPosition"
                .OnClientItemSelected = "onEmpresaItemSelected"
                .CompletionInterval = 500
            End With

            oTblCell.Controls.Add(oExtender)
        End If
        oTblRowInput.Cells.Add(oTblCell)

        'Bot�n b�squeda avanzada           
        If Not _readOnly Then
            Dim oTblCell2 As New System.Web.UI.WebControls.TableCell
            oTblCell2.Width = Unit.Pixel(14)
            oTblCell2.Height = Unit.Pixel(14)
            oTblCell2.HorizontalAlign = HorizontalAlign.Center
            oTblCell2.VerticalAlign = VerticalAlign.Middle
            oTblCell2.Style.Add("cursor", "pointer")
            oTblCell2.CssClass = "fsstyentrydd"

            Dim oBut As New System.Web.UI.HtmlControls.HtmlInputButton
            With oBut
                .Attributes("width") = Unit.Percentage(100).ToString
                .Style.Item("border-style") = "none"
                .Style.Item("border-width") = "0px"
                .Style.Item("background-image") = "url(" & Me.Page.ClientScript.GetWebResourceUrl(GetType(GeneralEntry), "Fullstep.DataEntry.trespuntos.gif") & ")"
                .Style.Item("background-position") = "center center"
                .Style.Item("background-repeat") = "no-repeat"
                .Style.Item("height") = Unit.Percentage(100).ToString
                .Style.Item("width") = Unit.Percentage(100).ToString
            End With
            oTblCell2.Controls.Add(oBut)

            oTblRowInput.Cells.Add(oTblCell2)
        End If
    End Sub
    ''' <summary>
    '''   Esta funci�n hace lo mismo q las demas, crear la "estructura" necesario para el entry Articulo (tipoGs=119)
    ''' </summary>
    ''' <remarks>Llamada desde: CreateChildControls; Tiempo m�ximo: 0</remarks>
    Private Sub CtrlGSTipoNuevoArticulo()
        Dim oTbl As System.Web.UI.WebControls.Table
        Dim oTblInput As System.Web.UI.WebControls.Table
        Dim oTblInfo As System.Web.UI.WebControls.Table
        Dim oTblRow As System.Web.UI.WebControls.TableRow
        Dim oTblRowInput As System.Web.UI.WebControls.TableRow
        Dim oTblRowInfo As System.Web.UI.WebControls.TableRow
        Dim oTblCell As System.Web.UI.WebControls.TableCell
        Dim oTblCell3 As System.Web.UI.WebControls.TableCell
        Dim oTblCellInput As System.Web.UI.WebControls.TableCell
        Dim oTblCellInfo As System.Web.UI.WebControls.TableCell
        Dim oWidth As System.Web.UI.WebControls.Unit
        Dim oHid As System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oLbl As System.Web.UI.WebControls.TextBox
        Dim oLbl2 As Infragistics.WebUI.WebDataInput.WebTextEdit
        Dim oHidOrgCompras As System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oHidCentro As System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oHidUnidadOrganizativa As System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oHidCentroCoste As System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oHidUnidad As System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oHidUnidadPedido As System.Web.UI.HtmlControls.HtmlInputHidden

        oHid = New System.Web.UI.HtmlControls.HtmlInputHidden
        oHid.ID = "_hmat"
        oHidOrgCompras = New System.Web.UI.HtmlControls.HtmlInputHidden
        oHidOrgCompras.ID = "_hOrgCompras" 'Contiene el valor del dataEntryDependent
        oHidCentro = New System.Web.UI.HtmlControls.HtmlInputHidden
        oHidCentro.ID = "_hCentro" 'Contiene el valor del dataEntryDependent2
        oHidUnidadOrganizativa = New System.Web.UI.HtmlControls.HtmlInputHidden
        oHidUnidadOrganizativa.ID = "_hUON" 'Contiene el valor del dataEntryDependent3
        If Me.ActivoSM Then
            oHidCentroCoste = New System.Web.UI.HtmlControls.HtmlInputHidden
            oHidCentroCoste.ID = "_hCentroCoste" 'Contiene el valor del DependentValueCentroCoste
        End If
        oHidUnidad = New System.Web.UI.HtmlControls.HtmlInputHidden
        oHidUnidad.ID = "_hUni" 'Contiene el valor del DataEntry Unidad
        oHidUnidadPedido = New System.Web.UI.HtmlControls.HtmlInputHidden
        oHidUnidadPedido.ID = "_hUniPed" 'Contiene el valor de la Unidad pedido

        If _DependentValue <> Nothing Then oHid.Value = _DependentValue
        If _DependentValueDataEntry <> Nothing Then oHidOrgCompras.Value = _DependentValueDataEntry
        If _DependentValueDataEntry2 <> Nothing Then oHidCentro.Value = _DependentValueDataEntry2
        If _DependentValueDataEntry3 <> Nothing Then oHidUnidadOrganizativa.Value = _DependentValueDataEntry3
        If Me.ActivoSM AndAlso Not _DependentValueCentroCoste Is Nothing Then oHidCentroCoste.Value = _DependentValueCentroCoste
        If _sUnidadOculta <> Nothing Then oHidUnidad.Value = _sUnidadOculta
        If _sUnidadPedidoOculta <> Nothing Then oHidUnidadPedido.Value = _sUnidadPedidoOculta

        oWidth = Me.Width
        oTbl = New System.Web.UI.WebControls.Table
        oTbl.Width = oWidth

        oTbl.CellPadding = 0
        oTbl.CellSpacing = 0
        oTbl.ID = "_tbl"
        Me.Controls.Add(oTbl)

        oTblRow = New System.Web.UI.WebControls.TableRow
        oTbl.Rows.Add(oTblRow)

        oTblCellInput = New System.Web.UI.WebControls.TableCell
        If Me.Desglose Then
            oTblCellInput.Width = Unit.Pixel(200)
        Else
            oTblCellInput.Width = Unit.Percentage(100)
        End If
        oTblRow.Cells.Add(oTblCellInput)
        oTblInput = New System.Web.UI.WebControls.Table
        oTblInput.Width = Unit.Percentage(100)
        oTblInput.CellPadding = 0
        oTblInput.CellSpacing = 0
        oTblInput.ID = "_t"
        oTblCellInput.Controls.Add(oTblInput)
        oTblRowInput = New System.Web.UI.WebControls.TableRow
        oTblInput.Rows.Add(oTblRowInput)

        oLbl = New System.Web.UI.WebControls.TextBox
        oLbl.ID = "_t_t"
        oLbl.MaxLength = _maxLength

        If _valor <> Nothing Then oLbl.Text = _valor
        If _toolTip = Nothing Then _toolTip = _valor

        oLbl.Width = Unit.Percentage(99)
        oLbl.BorderWidth = Unit.Pixel(0)
        oLbl.BorderStyle = BorderStyle.None
        oLbl.Style("display") = ""
        oLbl.ReadOnly = _readOnly

        oTblCell = New System.Web.UI.WebControls.TableCell
        oTblCell.Controls.Add(oLbl)
        oTblCell.Controls.Add(oHid)
        oTblCell.Controls.Add(oHidOrgCompras)
        oTblCell.Controls.Add(oHidCentro)
        oTblCell.Controls.Add(oHidUnidadOrganizativa)
        If Me.ActivoSM Then oTblCell.Controls.Add(oHidCentroCoste)
        oTblCell.Controls.Add(oHidUnidad)
        oTblCell.Controls.Add(oHidUnidadPedido)

        If Not _readOnly AndAlso Not _bPortal Then
            Dim sFilasAutoCompleteExtender As String = ConfigurationManager.AppSettings("FilasAutoCompleteExtender")

            Dim oExtender As New AutoCompleteExtender()
            With oExtender
                .ID = "_t_ext"
                .CompletionSetCount = IIf(Not String.IsNullOrEmpty(sFilasAutoCompleteExtender), CType(sFilasAutoCompleteExtender, Integer), 10)
                .DelimiterCharacters = ""
                .Enabled = True
                .MinimumPrefixLength = 3
                .CompletionListCssClass = "autoCompleteList"
                .CompletionListHighlightedItemCssClass = "autoCompleteSelectedListItem"
                .CompletionListItemCssClass = "autoCompleteListItem"
                .ServiceMethod = "DevolverArticulos"
                .ServicePath = ConfigurationManager.AppSettings("rutaPM") & "AutoCompletePMWEB.asmx"
                .TargetControlID = oLbl.ID
                .EnableCaching = False
                .UseContextKey = True
                .OnClientShown = "resetPosition"
                .OnClientPopulated = "onCodListPopulated"
                .CompletionInterval = 500
            End With

            oTblCell.Controls.Add(oExtender)
        End If

        oTblRowInput.Cells.Add(oTblCell)

        'Bot�n b�squeda avanzada           
        If Not _readOnly Then
            Dim oTblCell2 As New System.Web.UI.WebControls.TableCell
            oTblCell2.Width = Unit.Pixel(14)
            oTblCell2.Height = Unit.Pixel(14)
            oTblCell2.HorizontalAlign = HorizontalAlign.Center
            oTblCell2.VerticalAlign = VerticalAlign.Middle
            oTblCell2.Style.Add("cursor", "pointer")
            oTblCell2.CssClass = "fsstyentrydd"

            Dim oBut As New System.Web.UI.HtmlControls.HtmlInputButton
            With oBut
                .Attributes("width") = Unit.Percentage(100).ToString
                .Style.Item("border-style") = "none"
                .Style.Item("border-width") = "0px"
                .Style.Item("background-image") = "url(" & Me.Page.ClientScript.GetWebResourceUrl(GetType(GeneralEntry), "Fullstep.DataEntry.trespuntos.gif") & ")"
                .Style.Item("background-position") = "center center"
                .Style.Item("background-repeat") = "no-repeat"
                .Style.Item("height") = Unit.Percentage(100).ToString
                .Style.Item("width") = Unit.Percentage(100).ToString
            End With
            oTblCell2.Controls.Add(oBut)

            oTblRowInput.Cells.Add(oTblCell2)
        End If

        If Me.PM Then
            oTblCellInfo = New System.Web.UI.WebControls.TableCell
            oTblCellInfo.Width = Unit.Percentage(0)
            oTblRow.Cells.Add(oTblCellInfo)
            oTblInfo = New System.Web.UI.WebControls.Table
            oTblInfo.Width = Unit.Percentage(100)
            oTblInfo.CellPadding = 0
            oTblInfo.CellSpacing = 0
            oTblInfo.ID = "_info"
            oTblCellInfo.Controls.Add(oTblInfo)
            oTblRowInfo = New System.Web.UI.WebControls.TableRow
            oTblInfo.Rows.Add(oTblRowInfo)

            oLbl2 = New Infragistics.WebUI.WebDataInput.WebTextEdit
            oLbl2.ID = "_t2"
            oLbl2.BorderWidth = Unit.Pixel(0)
            oLbl2.BorderStyle = BorderStyle.None
            oLbl2.Width = Unit.Percentage(0)
            If Not _valor Is Nothing AndAlso _valor <> "" Then
                oLbl2.ButtonsAppearance.CustomButtonDisplay = Infragistics.WebUI.WebDataInput.ButtonDisplay.OnRight
                oLbl2.ButtonsAppearance.CustomButtonImageUrl = Me.Page.ClientScript.GetWebResourceUrl(GetType(GeneralEntry), "Fullstep.DataEntry.info.gif")
            End If

            oTblCell3 = New System.Web.UI.WebControls.TableCell
            oTblCell3.HorizontalAlign = HorizontalAlign.Right
            oTblCell3.VerticalAlign = VerticalAlign.Top
            oTblCell3.Style.Add("cursor", "pointer")
            oTblCell3.Width = Unit.Percentage(0)
            oTblCell3.Controls.Add(oLbl2)
            oTblRowInfo.Cells.Add(oTblCell3)
            If Not _readOnly And Not Me.Portal _
                And Not (Me.TipoSolicit = TiposDeDatos.TipoDeSolicitud.Certificado) _
                And Not (Me.TipoSolicit = TiposDeDatos.TipoDeSolicitud.NoConformidad) _
                And Not (Me.TipoSolicit = TiposDeDatos.TipoDeSolicitud.Contrato) _
                And Not (Me.TipoSolicit = TiposDeDatos.TipoDeSolicitud.Autofactura) _
                And Not (Me.TipoSolicit = TiposDeDatos.TipoDeSolicitud.SolicitudDePedidoCatalogo) _
                And Not (Me.TipoSolicit = TiposDeDatos.TipoDeSolicitud.SolicitudDePedidoContraAbierto) Then
                Dim oTblCell4 As System.Web.UI.WebControls.TableCell
                Dim oLbl3 As Infragistics.WebUI.WebDataInput.WebTextEdit
                oLbl3 = New Infragistics.WebUI.WebDataInput.WebTextEdit
                oLbl3.BorderWidth = Unit.Pixel(0)
                oLbl3.BorderStyle = BorderStyle.None
                oLbl3.Width = Unit.Percentage(0)
                oLbl3.ID = "_t3"
                oLbl3.ButtonsAppearance.CustomButtonDisplay = Infragistics.WebUI.WebDataInput.ButtonDisplay.OnRight
                oLbl3.ButtonsAppearance.CustomButtonImageUrl = Me.Page.ClientScript.GetWebResourceUrl(GetType(GeneralEntry), "Fullstep.DataEntry.favoritos.gif")
                oTblCell4 = New System.Web.UI.WebControls.TableCell
                oTblCell4.Width = Unit.Percentage(0)
                oTblCell4.Controls.Add(oLbl3)
                oTblRowInfo.Cells.Add(oTblCell4)
            End If
        End If
    End Sub
    ''' <summary>
    '''   Esta funci�n hace lo mismo q las demas, crear la "estructura" necesario para el entry Material (tipoGs=104)
    ''' </summary>
    ''' <remarks>Llamada desde: CreateChildControls; Tiempo m�ximo: 0</remarks>
    Private Sub CtrlGSTipoMaterial()
        Dim oTbl As System.Web.UI.WebControls.Table
        Dim oTblRow As System.Web.UI.WebControls.TableRow
        Dim oTblCell As System.Web.UI.WebControls.TableCell
        Dim oLbl As Infragistics.WebUI.WebDataInput.WebTextEdit
        Dim oHid As System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oWidth As System.Web.UI.WebControls.Unit

        oWidth = Me.Width
        oHid = New System.Web.UI.HtmlControls.HtmlInputHidden

        oLbl = New Infragistics.WebUI.WebDataInput.WebTextEdit
        oLbl.BorderWidth = Unit.Pixel(0)
        oLbl.BorderStyle = BorderStyle.None
        If _Desglose Then
            oLbl.Width = Unit.Pixel(190)
        Else

            If Me.Width.Type = UnitType.Pixel Then
                oLbl.Width = Unit.Pixel(oWidth.Value)
            Else
                If Me.Width.Type = UnitType.Point Then
                    oLbl.Width = Unit.Point(oWidth.Value)
                Else
                    If Me.Width.Type = UnitType.Percentage Then
                        oLbl.Width = Unit.Percentage(100)
                    Else
                        oLbl.Width = Unit.Percentage(100)
                    End If
                End If
            End If
        End If
        oLbl.Style("display") = ""
        If _toolTip = Nothing AndAlso _text <> Nothing Then
            _toolTip = _text
        End If
        If _text <> Nothing Then
            oLbl.Text = _text.ToString
            oHid.Value = _valor

        End If

        oLbl.ButtonsAppearance.CustomButtonDisplay = Infragistics.WebUI.WebDataInput.ButtonDisplay.OnRight
        oLbl.ButtonsAppearance.CustomButtonImageUrl = Me.Page.ClientScript.GetWebResourceUrl(GetType(GeneralEntry), "Fullstep.DataEntry.trespuntos.gif")

        oLbl.ReadOnly = True
        oLbl.ID = "_t"
        oHid.ID = "_h"

        oTbl = New System.Web.UI.WebControls.Table
        oTbl.Width = oWidth
        oTbl.BorderWidth = Unit.Pixel(0)
        oTbl.CellPadding = 0
        oTbl.CellSpacing = 0



        oTblRow = New System.Web.UI.WebControls.TableRow
        oTblCell = New System.Web.UI.WebControls.TableCell
        oTblCell.Width = Unit.Percentage(100)
        oTblCell.Controls.Add(oLbl)
        oTblCell.Controls.Add(oHid)
        oTblRow.Cells.Add(oTblCell)

        oTbl.Rows.Add(oTblRow)
        oTbl.ID = "_tbl"


        Me.Controls.Add(oTbl)


    End Sub
    ''' <summary>
    '''   Esta funci�n hace lo mismo q las demas, crear la "estructura" necesario para el entry denom. Articulo (tipoGs=118)
    ''' </summary>
    ''' <remarks>Llamada desde: CreateChildControls; Tiempo m�ximo: 0</remarks>
    Private Sub CtrlGSTipoDenArticulo()
        Dim oTbl As System.Web.UI.WebControls.Table
        Dim oTblInput As System.Web.UI.WebControls.Table
        Dim oTblInfo As System.Web.UI.WebControls.Table
        Dim oTblRow As System.Web.UI.WebControls.TableRow
        Dim oTblRowInput As System.Web.UI.WebControls.TableRow
        Dim oTblRowInfo As System.Web.UI.WebControls.TableRow
        Dim oTblCell As System.Web.UI.WebControls.TableCell
        Dim oTblCell3 As System.Web.UI.WebControls.TableCell
        Dim oTblCellInput As System.Web.UI.WebControls.TableCell
        Dim oTblCellInfo As System.Web.UI.WebControls.TableCell
        Dim oWidth As System.Web.UI.WebControls.Unit
        Dim oHid As System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oHidMat As System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oHidCod As System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oHidOrgCompras As System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oHidCentro As System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oHidUnidadOrganizativa As System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oHidCentroCoste As System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oHidUnidad As System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oHidUnidadPedido As System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oLbl As System.Web.UI.WebControls.TextBox

        Dim oLbl2 As Infragistics.WebUI.WebDataInput.WebTextEdit

        oHid = New System.Web.UI.HtmlControls.HtmlInputHidden
        oHidMat = New System.Web.UI.HtmlControls.HtmlInputHidden
        oHidCod = New System.Web.UI.HtmlControls.HtmlInputHidden

        oHidMat.ID = "_hmat"
        oHidCod.ID = "_codArt"
        oHidOrgCompras = New System.Web.UI.HtmlControls.HtmlInputHidden
        oHidOrgCompras.ID = "_hOrgCompras" 'Contiene el valor del dataEntryDependent
        oHidCentro = New System.Web.UI.HtmlControls.HtmlInputHidden
        oHidCentro.ID = "_hCentro" 'Contiene el valor del dataEntryDependent2
        oHidUnidadOrganizativa = New System.Web.UI.HtmlControls.HtmlInputHidden
        oHidUnidadOrganizativa.ID = "_hUON" 'Contiene el valor del dataEntryDependent3        
        If Me.ActivoSM Then
            oHidCentroCoste = New System.Web.UI.HtmlControls.HtmlInputHidden
            oHidCentroCoste.ID = "_hCentroCoste" 'Contiene el valor del DependentValueCentroCoste
        End If
        oHidUnidad = New System.Web.UI.HtmlControls.HtmlInputHidden
        oHidUnidad.ID = "_hUni" 'Contiene el valor del dataEntryDependent2
        oHidUnidadPedido = New System.Web.UI.HtmlControls.HtmlInputHidden
        oHidUnidadPedido.ID = "_hUniPed" 'Contiene el valor de la Unidad pedido

        oHid.ID = "_h"
        If _DependentValue <> Nothing Then oHidMat.Value = _DependentValue
        If _DependentValueDataEntry <> Nothing Then oHidOrgCompras.Value = _DependentValueDataEntry
        If _DependentValueDataEntry2 <> Nothing Then oHidCentro.Value = _DependentValueDataEntry2
        If _DependentValueDataEntry3 <> Nothing Then oHidUnidadOrganizativa.Value = _DependentValueDataEntry3
        If Me.ActivoSM AndAlso Not _DependentValueCentroCoste Is Nothing Then oHidCentroCoste.Value = _DependentValueCentroCoste
        If _sUnidadOculta <> Nothing Then
            oHidUnidad.Value = _sUnidadOculta
        End If
        If _sUnidadPedidoOculta <> Nothing Then
            oHidUnidadPedido.Value = _sUnidadPedidoOculta
        End If

        oWidth = Me.Width
        oTbl = New System.Web.UI.WebControls.Table
        oTbl.Width = oWidth
        oTbl.BorderWidth = Unit.Pixel(0)
        oTbl.CellPadding = 0
        oTbl.CellSpacing = 0
        oTbl.ID = "_tbl"
        Me.Controls.Add(oTbl)

        oTblRow = New System.Web.UI.WebControls.TableRow
        oTbl.Rows.Add(oTblRow)

        oTblCellInput = New System.Web.UI.WebControls.TableCell
        If Me.Desglose Then
            oTblCellInput.Width = Unit.Pixel(400)
        Else
            oTblCellInput.Width = Unit.Percentage(100)
        End If
        oTblRow.Cells.Add(oTblCellInput)
        oTblInput = New System.Web.UI.WebControls.Table
        oTblInput.Width = Unit.Percentage(100)
        oTblInput.Attributes.Add("class", "TipoTextoDen")
        oTblInput.CellPadding = 0
        oTblInput.CellSpacing = 0
        oTblInput.ID = "_tden"
        oTblCellInput.Controls.Add(oTblInput)
        oTblRowInput = New System.Web.UI.WebControls.TableRow
        oTblInput.Rows.Add(oTblRowInput)

        oLbl = New System.Web.UI.WebControls.TextBox
        oLbl.ID = "_tden_t"
        oLbl.MaxLength = _maxLength
        oLbl.BorderWidth = Unit.Pixel(0)
        oLbl.BorderStyle = BorderStyle.None
        oLbl.Width = Unit.Percentage(99)
        oLbl.Style("display") = ""

        If _text <> Nothing Then
            oLbl.Text = _text.ToString
            oHid.Value = _valor
        End If

        If _toolTip = Nothing Then _toolTip = _valor
        oLbl.ReadOnly = _readOnly

        oTblCell = New System.Web.UI.WebControls.TableCell
        oTblCell.Controls.Add(oLbl)
        oTblCell.Controls.Add(oHid)
        oTblCell.Controls.Add(oHidMat)
        oTblCell.Controls.Add(oHidOrgCompras)
        oTblCell.Controls.Add(oHidCentro)
        oTblCell.Controls.Add(oHidUnidadOrganizativa)
        If Me.ActivoSM Then oTblCell.Controls.Add(oHidCentroCoste)
        oTblCell.Controls.Add(oHidUnidad)
        oTblCell.Controls.Add(oHidUnidadPedido)

        If Not _readOnly AndAlso Not _bPortal Then
            Dim sFilasAutoCompleteExtender As String = ConfigurationManager.AppSettings("FilasAutoCompleteExtender")

            Dim oExtender As New AutoCompleteExtender()
            With oExtender
                .ID = "_tden_ext"
                .CompletionSetCount = IIf(Not String.IsNullOrEmpty(sFilasAutoCompleteExtender), CType(sFilasAutoCompleteExtender, Integer), 10)
                .DelimiterCharacters = ""
                .Enabled = True
                .MinimumPrefixLength = 3
                .CompletionListCssClass = "autoCompleteList"
                .CompletionListHighlightedItemCssClass = "autoCompleteSelectedListItem"
                .CompletionListItemCssClass = "autoCompleteListItem"
                .ServiceMethod = "DevolverArticulos"
                .ServicePath = ConfigurationManager.AppSettings("rutaPM") & "AutoCompletePMWEB.asmx"
                .TargetControlID = oLbl.ID
                .EnableCaching = False
                .UseContextKey = True
                .OnClientShown = "resetPosition"
                .OnClientPopulated = "onCodListPopulated"
                .OnClientItemSelected = "onDenExtenderItemSelected"
                .CompletionInterval = 500
            End With

            oTblCell.Controls.Add(oExtender)
        End If

        oTblRowInput.Cells.Add(oTblCell)

        If Not _readOnly Then
            Dim oTblCell2 As New System.Web.UI.WebControls.TableCell
            oTblCell2.Width = Unit.Pixel(14)
            oTblCell2.Height = Unit.Pixel(14)
            oTblCell2.HorizontalAlign = HorizontalAlign.Center
            oTblCell2.VerticalAlign = VerticalAlign.Middle
            oTblCell2.Style.Add("cursor", "pointer")
            oTblCell2.CssClass = "fsstyentrydd"

            Dim oBut As New System.Web.UI.HtmlControls.HtmlInputButton
            With oBut
                .Attributes("width") = Unit.Percentage(100).ToString
                .Style.Item("border-style") = "none"
                .Style.Item("border-width") = "0px"
                .Style.Item("background-image") = "url(" & Me.Page.ClientScript.GetWebResourceUrl(GetType(GeneralEntry), "Fullstep.DataEntry.trespuntos.gif") & ")"
                .Style.Item("background-position") = "center center"
                .Style.Item("background-repeat") = "no-repeat"
                .Style.Item("height") = Unit.Percentage(100).ToString
                .Style.Item("width") = Unit.Percentage(100).ToString
            End With
            oTblCell2.Controls.Add(oBut)

            oTblRowInput.Cells.Add(oTblCell2)
        End If

        If Me.PM Then
            oTblCellInfo = New System.Web.UI.WebControls.TableCell
            oTblCellInfo.Width = Unit.Percentage(0)
            oTblRow.Cells.Add(oTblCellInfo)
            oTblInfo = New System.Web.UI.WebControls.Table
            oTblInfo.Width = Unit.Percentage(100)
            oTblInfo.CellPadding = 0
            oTblInfo.CellSpacing = 0
            oTblInfo.ID = "_info"
            oTblCellInfo.Controls.Add(oTblInfo)
            oTblRowInfo = New System.Web.UI.WebControls.TableRow
            oTblInfo.Rows.Add(oTblRowInfo)

            oLbl2 = New Infragistics.WebUI.WebDataInput.WebTextEdit
            oLbl2.ID = "_t2"
            oLbl2.BorderWidth = Unit.Pixel(0)
            oLbl2.BorderStyle = BorderStyle.None
            oLbl2.Width = Unit.Percentage(0)
            If Not _valor Is Nothing AndAlso _valor <> "" Then
                oLbl2.ButtonsAppearance.CustomButtonDisplay = Infragistics.WebUI.WebDataInput.ButtonDisplay.OnRight
                oLbl2.ButtonsAppearance.CustomButtonImageUrl = Me.Page.ClientScript.GetWebResourceUrl(GetType(GeneralEntry), "Fullstep.DataEntry.info.gif")
            End If

            oTblCell3 = New System.Web.UI.WebControls.TableCell
            oTblCell3.HorizontalAlign = HorizontalAlign.Right
            oTblCell3.VerticalAlign = VerticalAlign.Top
            oTblCell3.Style.Add("cursor", "pointer")
            oTblCell3.Width = Unit.Percentage(0)
            oTblCell3.Controls.Add(oLbl2)
            oTblRowInfo.Cells.Add(oTblCell3)
        End If
    End Sub
    ''' Revisado por: blp. Fecha: 04/10/2011
    ''' <summary>
    '''   Esta funci�n hace lo mismo q las demas, crear la "estructura" necesario para el entry Persona (tipoGs=115) o Comprador (tipoGs=144)
    ''' </summary>
    ''' <remarks>Llamada desde: CreateChildControls; Tiempo m�ximo: 0</remarks>
    Private Sub CtrlGSTipoPersona()
        Dim oTbl As System.Web.UI.WebControls.Table
        Dim oTblRow As System.Web.UI.WebControls.TableRow
        Dim oTblCell As System.Web.UI.WebControls.TableCell
        Dim oLbl As Infragistics.WebUI.WebDataInput.WebTextEdit
        Dim oHid As System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oWidth As System.Web.UI.WebControls.Unit

        If _intro = 1 Then
            CtrlTipoLista(TiposDeDatos.TipoGeneral.TipoTextoMedio)
        Else
            oWidth = Me.Width
            oHid = New System.Web.UI.HtmlControls.HtmlInputHidden

            oLbl = New Infragistics.WebUI.WebDataInput.WebTextEdit
            oLbl.BorderWidth = Unit.Pixel(0)
            oLbl.BorderStyle = BorderStyle.None
            If _toolTip = Nothing AndAlso _text <> Nothing Then
                _toolTip = _text
            End If
            If _text <> Nothing Then
                oLbl.Text = _text.ToString
                oHid.Value = _valor
            End If

            oLbl.ReadOnly = True
            oLbl.ID = "_t"
            oHid.ID = "_h"
            oLbl.Width = oWidth
            oLbl.Style("display") = ""

            oTbl = New System.Web.UI.WebControls.Table
            oTbl.Width = oWidth
            oTbl.BorderWidth = Unit.Pixel(0)
            oTbl.CellPadding = 0
            oTbl.CellSpacing = 0

            oTblRow = New System.Web.UI.WebControls.TableRow
            oTblCell = New System.Web.UI.WebControls.TableCell
            oTblCell.Width = Unit.Percentage(100)
            oTblCell.Controls.Add(oLbl)
            oTblCell.Controls.Add(oHid)
            oTblRow.Cells.Add(oTblCell)
            If Not _readOnly Then
                oLbl.ButtonsAppearance.CustomButtonDisplay = Infragistics.WebUI.WebDataInput.ButtonDisplay.OnRight
                oLbl.ButtonsAppearance.CustomButtonImageUrl = Me.Page.ClientScript.GetWebResourceUrl(GetType(GeneralEntry), "Fullstep.DataEntry.trespuntos.gif")
            End If

            oTbl.Rows.Add(oTblRow)
            oTbl.ID = "_tbl"

            Me.Controls.Add(oTbl)
        End If
    End Sub
    ''' <summary>
    '''   Esta funci�n hace lo mismo q las demas, crear la "estructura" necesario para el entry Unidad Organizativa (tipoGs=121)
    ''' </summary>
    ''' <remarks>Llamada desde: CreateChildControls; Tiempo m�ximo: 0</remarks>
    Private Sub CtrlGSTipoUnidadOrganizativa()
        Dim oTbl As System.Web.UI.WebControls.Table
        Dim oTblRow As System.Web.UI.WebControls.TableRow
        Dim oTblCell As System.Web.UI.WebControls.TableCell
        Dim oLbl As Infragistics.WebUI.WebDataInput.WebTextEdit
        Dim oHid As System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oWidth As System.Web.UI.WebControls.Unit

        oWidth = Me.Width
        oHid = New System.Web.UI.HtmlControls.HtmlInputHidden

        oLbl = New Infragistics.WebUI.WebDataInput.WebTextEdit
        oLbl.BorderWidth = Unit.Pixel(0)
        oLbl.BorderStyle = BorderStyle.None
        oLbl.Width = Unit.Percentage(100)
        oLbl.Style("display") = ""
        If _toolTip = Nothing AndAlso _text <> Nothing Then
            _toolTip = _text
        End If
        If _text <> Nothing Then
            oLbl.Text = _text.ToString
            oHid.Value = _valor
        End If

        If Not _readOnly Then
            oLbl.ButtonsAppearance.CustomButtonDisplay = Infragistics.WebUI.WebDataInput.ButtonDisplay.OnRight
            oLbl.ButtonsAppearance.CustomButtonImageUrl = Me.Page.ClientScript.GetWebResourceUrl(GetType(GeneralEntry), "Fullstep.DataEntry.trespuntos.gif")
        End If

        oLbl.ReadOnly = True
        oLbl.ID = "_t"
        oHid.ID = "_h"

        oTbl = New System.Web.UI.WebControls.Table
        oTbl.Width = oWidth
        oTbl.BorderWidth = Unit.Pixel(0)
        oTbl.CellPadding = 0
        oTbl.CellSpacing = 0

        oTblRow = New System.Web.UI.WebControls.TableRow
        oTblCell = New System.Web.UI.WebControls.TableCell
        oTblCell.Width = Unit.Percentage(100)
        oTblCell.Controls.Add(oLbl)
        oTblCell.Controls.Add(oHid)
        oTblRow.Cells.Add(oTblCell)

        oTbl.Rows.Add(oTblRow)
        oTbl.ID = "_tbl"

        Me.Controls.Add(oTbl)
    End Sub
    ''' <summary>
    '''   Esta funci�n hace lo mismo q las demas, crear la "estructura" necesario para el entry Tipo Referencia a Solicitud (tipoGs=128)
    ''' </summary>
    ''' <remarks>Llamada desde: CreateChildControls; Tiempo m�ximo: 0</remarks>
    Private Sub CtrlGSTipoRefSolicitud()
        Dim oTbl As System.Web.UI.WebControls.Table
        Dim oTblRow As System.Web.UI.WebControls.TableRow
        Dim oTblCell As System.Web.UI.WebControls.TableCell
        Dim oLbl As Infragistics.WebUI.WebDataInput.WebTextEdit
        Dim oHid As System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oWidth As System.Web.UI.WebControls.Unit
        oWidth = Me.Width

        oHid = New System.Web.UI.HtmlControls.HtmlInputHidden

        oLbl = New Infragistics.WebUI.WebDataInput.WebTextEdit
        oLbl.BorderWidth = Unit.Pixel(0)
        oLbl.BorderStyle = BorderStyle.None
        If _toolTip = Nothing AndAlso _text <> Nothing Then
            _toolTip = _text
        End If
        If _text <> Nothing Then
            oLbl.Text = _text.ToString
            oHid.Value = _valor

            oLbl.ButtonsAppearance.CustomButtonDisplay = Infragistics.WebUI.WebDataInput.ButtonDisplay.OnRight
            oLbl.ButtonsAppearance.CustomButtonImageUrl = Me.Page.ClientScript.GetWebResourceUrl(GetType(GeneralEntry), "Fullstep.DataEntry.trespuntos.gif")
        End If

        oLbl.ReadOnly = True
        oLbl.ID = "_t"
        oHid.ID = "_h"
        oLbl.Width = oWidth
        oLbl.Style("display") = ""

        oTbl = New System.Web.UI.WebControls.Table
        oTbl.Width = oWidth
        oTbl.BorderWidth = Unit.Pixel(0)
        oTbl.CellPadding = 0
        oTbl.CellSpacing = 0

        oTblRow = New System.Web.UI.WebControls.TableRow
        oTblCell = New System.Web.UI.WebControls.TableCell
        oTblCell.Width = Unit.Percentage(100)
        oTblCell.Controls.Add(oLbl)
        oTblCell.Controls.Add(oHid)
        oTblRow.Cells.Add(oTblCell)

        oTbl.Rows.Add(oTblRow)
        oTbl.ID = "_tbl"

        Controls.Add(oTbl)
    End Sub
    ''' <summary>
    '''   Esta funci�n hace lo mismo q las demas, crear la "estructura" necesario para el entry Importe Solicitud Vinculada(tipoGs=127)
    ''' </summary>
    ''' <remarks>Llamada desde: CreateChildControls; Tiempo m�ximo: 0</remarks>
    Private Sub CtrlGSTipoImporteSolicitudVinculada()
        Dim oTbl As System.Web.UI.WebControls.Table
        Dim oTblRow As System.Web.UI.WebControls.TableRow
        Dim oTblCell As System.Web.UI.WebControls.TableCell
        Dim oLbl As Infragistics.WebUI.WebDataInput.WebTextEdit
        Dim oHid As System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oWidth As System.Web.UI.WebControls.Unit
        oWidth = Me.Width

        oHid = New System.Web.UI.HtmlControls.HtmlInputHidden

        oLbl = New Infragistics.WebUI.WebDataInput.WebTextEdit
        oLbl.BorderWidth = Unit.Pixel(0)
        oLbl.BorderStyle = BorderStyle.None
        If _toolTip = Nothing AndAlso _text <> Nothing Then
            _toolTip = _text
        End If
        If _text <> Nothing Then
            oLbl.Text = _text.ToString
            oHid.Value = _valor

        End If

        oLbl.ReadOnly = _readOnly

        If _intro = 1 Then
            oLbl.Width = Unit.Percentage(100)
            oLbl.ButtonsAppearance.CustomButtonDisplay = Infragistics.WebUI.WebDataInput.ButtonDisplay.OnRight
            oLbl.ButtonsAppearance.CustomButtonImageUrl = Me.Page.ClientScript.GetWebResourceUrl(GetType(GeneralEntry), "Fullstep.DataEntry.trespuntos.gif")
        End If

        oLbl.ID = "_t"
        oHid.ID = "_h"
        'oLbl.Width = oWidth
        oLbl.Width = Unit.Percentage(100)
        oLbl.Style("display") = ""

        oTbl = New System.Web.UI.WebControls.Table
        'oTbl.Width = oWidth
        oTbl.Width = Unit.Percentage(100)
        oTbl.BorderWidth = Unit.Pixel(0)
        oTbl.CellPadding = 0
        oTbl.CellSpacing = 0

        oTblRow = New System.Web.UI.WebControls.TableRow
        oTblCell = New System.Web.UI.WebControls.TableCell
        oTblCell.Width = Unit.Percentage(100)
        'oTblCell.Width = oTbl.Width
        oTblCell.Controls.Add(oLbl)
        oTblCell.Controls.Add(oHid)
        oTblRow.Cells.Add(oTblCell)


        oTbl.Rows.Add(oTblRow)
        oTbl.ID = "_tbl"

        Me.Controls.Add(oTbl)

    End Sub
    ''' <summary>
    '''   Esta funci�n hace lo mismo q las demas, crear la "estructura" necesario para un entry Tabla Externa
    ''' </summary>
    ''' <remarks>Llamada desde: CreateChildControls; Tiempo m�ximo: 0</remarks>
    Private Sub CtrlTablaExterna()
        Dim oTbl As System.Web.UI.WebControls.Table
        Dim oTblRow As System.Web.UI.WebControls.TableRow
        Dim oTblCell As System.Web.UI.WebControls.TableCell
        Dim oCtrl As Object
        Dim oWidth As System.Web.UI.WebControls.Unit
        Dim oHid As System.Web.UI.HtmlControls.HtmlInputHidden
        oWidth = Me.Width
        oHid = New System.Web.UI.HtmlControls.HtmlInputHidden
        oCtrl = New Infragistics.WebUI.WebDataInput.WebTextEdit

        If _toolTip = Nothing AndAlso _text <> Nothing Then _toolTip = _text
        If _tipo = TiposDeDatos.TipoGeneral.TipoFecha Then
            If _valor = New Date Then _valor = Nothing
        End If
        If Not _valor Is Nothing Then
            oCtrl.Value = _valor
        End If
        If Not _text Is Nothing Then
            oCtrl.Text = _text
        ElseIf Not _valor Is Nothing Then
            oCtrl.Text = _valor.ToString()
        End If
        oHid.Value = oCtrl.Text

        oCtrl.ID = "_t"
        oHid.ID = "_h"
        oCtrl.Width = oWidth
        oCtrl.Style("display") = ""

        oTbl = New System.Web.UI.WebControls.Table
        oTbl.Width = oWidth
        oTbl.BorderWidth = Unit.Pixel(0)
        oTbl.CellPadding = 0
        oTbl.CellSpacing = 0

        oCtrl.BorderStyle = BorderStyle.None
        oCtrl.BackColor = System.Drawing.Color.Transparent

        oCtrl.ReadOnly = _readOnly

        oTblRow = New System.Web.UI.WebControls.TableRow
        oTblCell = New System.Web.UI.WebControls.TableCell
        oTblCell.Width = Unit.Percentage(100)
        oTblCell.Controls.Add(oCtrl)
        oTblCell.Controls.Add(oHid)
        oTblRow.Cells.Add(oTblCell)

        If Not (_readOnly And _valor Is Nothing) Then
            oCtrl.ButtonsAppearance.CustomButtonDisplay = Infragistics.WebUI.WebDataInput.ButtonDisplay.OnRight
            oCtrl.ButtonsAppearance.CustomButtonImageUrl = Me.Page.ClientScript.GetWebResourceUrl(GetType(GeneralEntry), "Fullstep.DataEntry.trespuntos.gif")
        End If

        oTbl.Rows.Add(oTblRow)
        oTbl.ID = "_tbl"

        Me.Controls.Add(oTbl)
    End Sub
    ''' <summary>
    '''   Esta funci�n hace lo mismo q las demas, crear la "estructura" necesario para el entry 
    '''   Importe Repercutido (tipoGs=45)
    '''       Caja de texto Numerica + link
    '''       Si es readonly solo Caja de texto con moneda contenada
    ''' </summary>
    ''' <remarks>Llamada desde: CreateChildControls; Tiempo m�ximo: 0</remarks>
    Private Sub CtrlGSTipoImporteRepercutido()

        Dim oTbl As System.Web.UI.WebControls.Table
        Dim oTblRow As System.Web.UI.WebControls.TableRow
        Dim oTblCell As System.Web.UI.WebControls.TableCell
        Dim oTblCell2 As System.Web.UI.WebControls.TableCell

        Dim oNumber As Infragistics.WebUI.WebDataInput.WebNumericEdit = Nothing
        Dim oTxt As Infragistics.WebUI.WebDataInput.WebTextEdit = Nothing

        Dim oHid As System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oWidth As System.Web.UI.WebControls.Unit

        Dim oLink As System.Web.UI.HtmlControls.HtmlAnchor = Nothing

        Dim oHidMon As System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oHidCambio As System.Web.UI.HtmlControls.HtmlInputHidden

        oWidth = Me.Width

        oHid = New System.Web.UI.HtmlControls.HtmlInputHidden
        oHidMon = New System.Web.UI.HtmlControls.HtmlInputHidden
        oHidCambio = New System.Web.UI.HtmlControls.HtmlInputHidden

        If _toolTip = Nothing AndAlso _text <> Nothing Then
            _toolTip = _text
        End If

        oHid.ID = "_h"

        If Not _readOnly Then
            oNumber = New Infragistics.WebUI.WebDataInput.WebNumericEdit
            oNumber.BorderWidth = Unit.Pixel(0)
            oNumber.BorderStyle = BorderStyle.None

            oLink = New System.Web.UI.HtmlControls.HtmlAnchor
            oLink.Attributes.Item("BORDER-WIDTH") = "0"
            oLink.Attributes.Item("BORDER") = BorderStyle.None
            oLink.Attributes.Item("CLASS") = "TablaLink"
            oLink.HRef = "#"

            If Not (_MonImporteRepercutido Is Nothing) Then oLink.InnerText = _MonImporteRepercutido

            If _valor <> Nothing Then
                oNumber.Value = _valor
                oHid.Value = _valor
            Else
                Try
                    If _valor = 0 Then
                        oNumber.Value = _valor
                        oHid.Value = _valor
                    End If
                Catch ex As Exception
                End Try
            End If

            oNumber.ReadOnly = _readOnly
            oNumber.Width = oWidth
            oNumber.Style("display") = ""
            oNumber.NumberFormat = _NumberFormat

            oNumber.ID = "_t"

            oLink.Attributes.Item("WIDTH") = "10px"
            oLink.ID = "_t2"
        Else
            _toolTip = Nothing

            oTxt = New Infragistics.WebUI.WebDataInput.WebTextEdit
            oTxt.BorderWidth = Unit.Pixel(0)
            oTxt.BorderStyle = BorderStyle.None
            'oTxt.BackColor = System.Drawing.Color.Transparent

            If _valor <> Nothing Then
                oHid.Value = _valor

                oTxt.Text = CDbl(_valor).ToString("N", _NumberFormat) & " " & _MonImporteRepercutido
            End If

            oTxt.ReadOnly = True
            oTxt.Style("display") = ""
            oTxt.ID = "_t"
        End If

        oTbl = New System.Web.UI.WebControls.Table
        oTbl.Width = oWidth
        oTbl.BorderWidth = Unit.Pixel(0)
        oTbl.CellPadding = 0
        oTbl.CellSpacing = 0

        oTblRow = New System.Web.UI.WebControls.TableRow
        oTblCell = New System.Web.UI.WebControls.TableCell
        oTblCell2 = New System.Web.UI.WebControls.TableCell
        oTblCell.Width = Unit.Percentage(80)
        oTblCell2.Width = Unit.Percentage(20)

        If Not _readOnly Then
            oTblCell.Controls.Add(oNumber)
            oTblCell2.Controls.Add(oLink)
        Else
            oTblCell.Controls.Add(oTxt)
        End If
        oTblCell.Controls.Add(oHid)

        oTblRow.Cells.Add(oTblCell)
        oTblRow.Cells.Add(oTblCell2)

        oTbl.Rows.Add(oTblRow)
        oTbl.ID = "_tbl"

        Me.Controls.Add(oTbl)
    End Sub

    Private Sub CtrlGSTipoAnyoPartidaSM()
        CtrlTipoLista(TiposDeDatos.TipoGeneral.TipoNumerico)
    End Sub

    'sra 091109
    'Descripci�n:
    '   Esta funci�n hace crea la "estructura" necesaria para el entry Centro de coste (tipoGs=129) y Partida (tipoGs=130)
    '       Caja de texto + link
    'Param.Entrada:
    '   Nada
    'Param. Salida: 
    '   Nada
    'Llamada desde: 
    '   CreateChildControls
    'Tiempo: 0
    Private Sub CtrlGSTipoSM()
        Dim oTbl As System.Web.UI.WebControls.Table
        Dim oTblRow As System.Web.UI.WebControls.TableRow
        Dim oTblCell As System.Web.UI.WebControls.TableCell
        Dim oLbl As Infragistics.WebUI.WebDataInput.WebTextEdit
        Dim oHid As System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oWidth As System.Web.UI.WebControls.Unit

        oWidth = Me.Width

        oHid = New System.Web.UI.HtmlControls.HtmlInputHidden

        oLbl = New Infragistics.WebUI.WebDataInput.WebTextEdit
        oLbl.BorderWidth = Unit.Pixel(0)
        oLbl.BorderStyle = BorderStyle.None

        If _toolTip = Nothing AndAlso _text <> Nothing Then
            _toolTip = _text
        End If
        If _text <> Nothing Then
            oLbl.Text = _text.ToString

            oHid.Value = _valor

        End If

        oLbl.ReadOnly = _readOnly
        oLbl.ID = "_t"
        oHid.ID = "_h"
        oLbl.Width = oWidth
        oLbl.Style("display") = ""
        oTbl = New System.Web.UI.WebControls.Table
        oTbl.Width = oWidth
        oTbl.BorderWidth = Unit.Pixel(0)
        oTbl.CellPadding = 0
        oTbl.CellSpacing = 0

        oTblRow = New System.Web.UI.WebControls.TableRow
        oTblCell = New System.Web.UI.WebControls.TableCell
        oTblCell.Width = Unit.Percentage(100)
        oTblCell.Controls.Add(oLbl)
        oTblCell.Controls.Add(oHid)
        oTblRow.Cells.Add(oTblCell)
        If _readOnly Then
            If Not String.IsNullOrEmpty(oHid.Value) AndAlso Not String.IsNullOrEmpty(oLbl.Text) Then
                oLbl.ButtonsAppearance.CustomButtonDisplay = Infragistics.WebUI.WebDataInput.ButtonDisplay.OnRight
                oLbl.ButtonsAppearance.CustomButtonImageUrl = Me.Page.ClientScript.GetWebResourceUrl(GetType(GeneralEntry), "Fullstep.DataEntry.trespuntos.gif")
            End If
        Else
            oLbl.ButtonsAppearance.CustomButtonDisplay = Infragistics.WebUI.WebDataInput.ButtonDisplay.OnRight
            oLbl.ButtonsAppearance.CustomButtonImageUrl = Me.Page.ClientScript.GetWebResourceUrl(GetType(GeneralEntry), "Fullstep.DataEntry.trespuntos.gif")
        End If

        oTbl.Rows.Add(oTblRow)
        oTbl.ID = "_tbl"

        Me.Controls.Add(oTbl)
    End Sub
    'sra 290812
    'Descripci�n:
    '   Esta funci�n hace crea la "estructura" necesaria para el entry Factura (tipoGS=138)
    '       Caja de texto + link + enlace al detalle
    'Param.Entrada:
    '   Nada
    'Param. Salida: 
    '   Nada
    'Llamada desde: 
    '   CreateChildControls
    'Tiempo: 0
    Private Sub CtrlGSTipoFactura()
        Dim oTbl As System.Web.UI.WebControls.Table
        Dim oTblRow As System.Web.UI.WebControls.TableRow
        Dim oTblCell As System.Web.UI.WebControls.TableCell
        Dim oLbl As Infragistics.WebUI.WebDataInput.WebTextEdit
        Dim oHid As System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oHidIdFactura As System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oWidth As System.Web.UI.WebControls.Unit
        Dim oTblCell2 As System.Web.UI.WebControls.TableCell


        oWidth = Me.Width

        oHid = New System.Web.UI.HtmlControls.HtmlInputHidden
        oHidIdFactura = New System.Web.UI.HtmlControls.HtmlInputHidden

        oLbl = New Infragistics.WebUI.WebDataInput.WebTextEdit
        oLbl.BorderWidth = Unit.Pixel(0)
        oLbl.BorderStyle = BorderStyle.None
        oLbl.Style.Add("display", "")


        If _toolTip = Nothing AndAlso _text <> Nothing Then
            _toolTip = _text
        End If
        If _text <> Nothing Then
            oLbl.Text = _text.ToString
            oHid.Value = _valor
        End If

        oLbl.ReadOnly = _readOnly
        oLbl.ID = "_t"
        oHid.ID = "_h"
        oHidIdFactura.ID = "_hIdFactura"
        oLbl.Width = oWidth

        If _DependentValue <> Nothing Then
            oHidIdFactura.Value = _DependentValue
        Else
            oHidIdFactura.Value = -1
        End If

        oTbl = New System.Web.UI.WebControls.Table
        oTbl.Width = oWidth
        oTbl.BorderWidth = Unit.Pixel(1)
        oTbl.CellPadding = 0
        oTbl.CellSpacing = 0

        oTblRow = New System.Web.UI.WebControls.TableRow
        oTblCell = New System.Web.UI.WebControls.TableCell
        oTblCell.Width = Unit.Percentage(97)
        oTblCell.Controls.Add(oLbl)
        oTblCell.Controls.Add(oHid)
        oTblCell.Controls.Add(oHidIdFactura)
        oTblRow.Cells.Add(oTblCell)

        oTblCell2 = New System.Web.UI.WebControls.TableCell
        oTblCell2.Width = Unit.Percentage(3)

        If _bPortal Then
            oLbl.ButtonsAppearance.CustomButtonDisplay = Infragistics.WebUI.WebDataInput.ButtonDisplay.None
            Dim oImgFactura As System.Web.UI.HtmlControls.HtmlImage
            oImgFactura = New System.Web.UI.HtmlControls.HtmlImage
            oImgFactura.Src = Me.Page.ClientScript.GetWebResourceUrl(GetType(GeneralEntry), "Fullstep.DataEntry.factura.png")
            oImgFactura.ID = "imgFactura"

            oImgFactura.Attributes("onclick") = "show_detalle_factura('" & Me.ClientID & "')"

            oTblCell2.Controls.Add(oImgFactura)
            oTblRow.Cells.Add(oTblCell2)
        Else
            If _readOnly Then
                If Not String.IsNullOrEmpty(oHid.Value) AndAlso Not String.IsNullOrEmpty(oLbl.Text) AndAlso Not String.IsNullOrEmpty(_DependentValue) Then
                    oLbl.ButtonsAppearance.CustomButtonDisplay = Infragistics.WebUI.WebDataInput.ButtonDisplay.OnRight
                    oLbl.ButtonsAppearance.CustomButtonImageUrl = Me.Page.ClientScript.GetWebResourceUrl(GetType(GeneralEntry), "Fullstep.DataEntry.trespuntos.gif")
                End If
            Else
                oLbl.ButtonsAppearance.CustomButtonDisplay = Infragistics.WebUI.WebDataInput.ButtonDisplay.OnRight
                oLbl.ButtonsAppearance.CustomButtonImageUrl = Me.Page.ClientScript.GetWebResourceUrl(GetType(GeneralEntry), "Fullstep.DataEntry.trespuntos.gif")


                Dim oImgFactura As System.Web.UI.HtmlControls.HtmlImage
                oImgFactura = New System.Web.UI.HtmlControls.HtmlImage
                oImgFactura.Src = Me.Page.ClientScript.GetWebResourceUrl(GetType(GeneralEntry), "Fullstep.DataEntry.factura.png")
                oImgFactura.ID = "imgFactura"

                oImgFactura.Attributes("onclick") = "show_detalle_factura('" & Me.ClientID & "')"

                oTblCell2.Controls.Add(oImgFactura)
                oTblRow.Cells.Add(oTblCell2)
            End If
        End If

        oTbl.Rows.Add(oTblRow)
        oTbl.ID = "_tbl"

        Me.Controls.Add(oTbl)
    End Sub
    ''' Revisado por: blp. Fecha: 04/10/2011
    ''' <summary>
    ''' Esta funci�n crea la "estructura" necesaria para el entry Tipo de Pedido (tipoGS=132)
    ''' </summary>
    ''' <remarks>Llamada desde: CreateChildControls; Tiempo m�ximo: 0 sg</remarks>
    Private Sub CtrlGSTipoTipoPedido()
        Dim oTbl As System.Web.UI.WebControls.Table
        Dim oTblRow As System.Web.UI.WebControls.TableRow
        Dim oTblCell As System.Web.UI.WebControls.TableCell
        Dim oDrpDwn As Infragistics.Web.UI.ListControls.WebDropDown = Nothing
        Dim oLbl As Infragistics.WebUI.WebDataInput.WebTextEdit = Nothing
        Dim oHid As System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oWidth As System.Web.UI.WebControls.Unit

        oWidth = Me.Width
        If _toolTip = Nothing AndAlso _text <> Nothing Then
            _toolTip = _text
        End If

        oHid = New System.Web.UI.HtmlControls.HtmlInputHidden

        If _readOnly Then
            oLbl = New Infragistics.WebUI.WebDataInput.WebTextEdit
            oLbl.BorderWidth = Unit.Pixel(0)
            oLbl.BorderStyle = BorderStyle.None
            If _text <> Nothing Then
                oLbl.Text = _text.ToString
                oHid.Value = _valor
            End If

            oLbl.ReadOnly = _readOnly
            oLbl.ID = "_t"
            oLbl.Width = Unit.Percentage(100)
            oLbl.Style("display") = ""

            If Not String.IsNullOrEmpty(oHid.Value) AndAlso Not String.IsNullOrEmpty(oLbl.Text) Then
                oLbl.ButtonsAppearance.CustomButtonDisplay = Infragistics.WebUI.WebDataInput.ButtonDisplay.OnRight
                oLbl.ButtonsAppearance.CustomButtonImageUrl = Me.Page.ClientScript.GetWebResourceUrl(GetType(GeneralEntry), "Fullstep.DataEntry.trespuntos.gif")
            End If
        Else
            _toolTip = Nothing

            oDrpDwn = New Infragistics.Web.UI.ListControls.WebDropDown
            oDrpDwn.ID = "_t"
            oDrpDwn.Width = oWidth

            oDrpDwn.AutoFilterQueryType = Infragistics.Web.UI.ListControls.AutoFilterQueryTypes.StartsWith
            oDrpDwn.AutoSelectOnMatch = False
            oDrpDwn.DisplayMode = Infragistics.Web.UI.ListControls.DropDownDisplayMode.DropDown
            oDrpDwn.DropDownContainerHeight = Unit.Pixel(120)
            oDrpDwn.DropDownContainerMaxHeight = Unit.Pixel(120)
            oDrpDwn.DropDownOrientation = Infragistics.Web.UI.ListControls.DropDownOrientation.BottomLeft
            oDrpDwn.EnableAutoCompleteFirstMatch = True
            oDrpDwn.EnableAutoFiltering = Infragistics.Web.UI.ListControls.AutoFiltering.Off
            oDrpDwn.EnableCustomValues = False
            oDrpDwn.EnableCustomValueSelection = True
            oDrpDwn.EnableClosingDropDownOnSelect = True
            oDrpDwn.EnableClosingDropDownOnBlur = True
            oDrpDwn.EnableViewState = False
            oDrpDwn.EnableAnimations = False
            oDrpDwn.EnableDropDownAsChild = False

            'blp
            oDrpDwn.CssClass = "igdd_FullstepPMWebControl"

            oDrpDwn.TextField = "COD_DEN"
            oDrpDwn.ValueField = "COD"
            oDrpDwn.DataSource = _listaTiposPedido.Tables(0)

            'blp
            oDrpDwn.DataBind()
            For Each oItem As Infragistics.Web.UI.ListControls.DropDownItem In oDrpDwn.Items
                oItem.CssClass = "igdd_FullstepPMWebNoPadding"
            Next

            oDrpDwn.ItemTemplate = New GridWebDrownTemplate(TiposDeDatos.TipoCampoGS.TipoPedido)

            If Not _valor Is Nothing Then
                oHid.Value = _valor.ToString
                oDrpDwn.SelectedValue = _valor.ToString

                For Each Item As Infragistics.Web.UI.ListControls.DropDownItem In oDrpDwn.Items
                    If Item.DataItem.row.item(IIf(_tipoGS = TiposDeDatos.TipoCampoGS.Almacen, "ID", "COD")) = _valor.ToString Then
                        oDrpDwn.SelectedItemIndex = Item.Index
                        Exit For
                    End If
                Next

                If Not _text Is Nothing AndAlso DBNullToSomething(_valor) IsNot Nothing Then
                    oDrpDwn.CurrentValue = _text.ToString
                Else
                    oDrpDwn.CurrentValue = String.Empty
                End If
            Else
                AddHandler oDrpDwn.DataBound, AddressOf DropDown_DataBound

                oDrpDwn.CurrentValue = ""
                oDrpDwn.SelectedValue = Nothing
            End If

            oDrpDwn.ClientEvents.DropDownOpening = "WebDropDown_DropDownOpening"
            oDrpDwn.ClientEvents.SelectionChanged = "WebDropDown_SelectionChanging"
            oDrpDwn.ClientEvents.ValueChanged = "WebDropDown_valueChanged"
            oDrpDwn.ClientEvents.Blur = "WebDropDown_Focus"

            'En el c�digo javascript de Infragistics que se crea para los controles webdropdown
            'la propiedad enableMovingTargetWithSource es TRUE por defecto
            'Eso hace que a partir de la creaci�n de la p�gina, se llame de forma recurrente a la funci�n _onCheckPosition 
            'que consume muchos recursos y sobrecarga el javascript ejecutado en la p�gina cuando hay muchos webdropdown
            'Para evitarlo, deshabilitamos el enableMovingTargetWithSource
            If Not Page.ClientScript.IsStartupScriptRegistered(Page.GetType(), "enableMovingTargetWithSource_" + Me.IdContenedor + "_" + Me.ID + "_" + oDrpDwn.ID) Then
                Dim sScript As String = String.Format(IncludeScriptKeyFormat, "javascript", "Sys.Application.add_load(function(){$find('" + Me.IdContenedor + "_" + Me.ID + "_" + oDrpDwn.ID + "').behavior.set_enableMovingTargetWithSource(false)})")
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "enableMovingTargetWithSource_" + Me.IdContenedor + "_" + Me.ID + "_" + oDrpDwn.ID, sScript)
            End If
        End If

        If _text <> Nothing Then
            oHid.Value = _valor
        End If
        oHid.ID = "_h"

        oTbl = New System.Web.UI.WebControls.Table
        oTbl.ID = "_tbl"
        If _readOnly Then
            oTbl.Width = Unit.Percentage(100)
        Else
            oTbl.Height = Unit.Pixel(21)
            oTbl.Width = oWidth
        End If
        oTbl.BorderWidth = Unit.Pixel(0)
        oTbl.CellPadding = 0
        oTbl.CellSpacing = 0

        oTblRow = New System.Web.UI.WebControls.TableRow
        oTblCell = New System.Web.UI.WebControls.TableCell
        oTblCell.Width = Unit.Percentage(100)
        If _readOnly Then
            oTblCell.Controls.Add(oLbl)
        Else
            oTblCell.Controls.Add(oDrpDwn)
            oTblCell.VerticalAlign = VerticalAlign.Top
        End If

        oTblCell.Controls.Add(oHid)
        oTblRow.Cells.Add(oTblCell)

        oTbl.Rows.Add(oTblRow)

        Me.Controls.Add(oTbl)
    End Sub
    ''' <summary>
    ''' Esta funci�n crea la "estructura" necesaria para el entry Desglose Vinculado (tipoGS=2000)
    ''' </summary>
    ''' <remarks>Llamada desde: CreateChildControls; Tiempo m�ximo: 0 sg</remarks>
    Private Sub CtrlTipoDesgloseVinculado()
        Dim oTbl As System.Web.UI.WebControls.Table
        Dim oTblRow As System.Web.UI.WebControls.TableRow
        Dim oTblCell As System.Web.UI.WebControls.TableCell
        Dim oLbl As Infragistics.WebUI.WebDataInput.WebTextEdit
        Dim oHid As System.Web.UI.HtmlControls.HtmlInputHidden
        Dim oWidth As System.Web.UI.WebControls.Unit
        Dim sClientId As String = String.Empty

        If Not Me.IdContenedor = Nothing Then
            sClientId = Me.IdContenedor + "_"
        End If
        sClientId += Me.ID

        oWidth = Me.Width

        oHid = New System.Web.UI.HtmlControls.HtmlInputHidden

        oLbl = New Infragistics.WebUI.WebDataInput.WebTextEdit
        oLbl.BorderWidth = Unit.Pixel(0)
        oLbl.BorderStyle = BorderStyle.None

        If _toolTip = Nothing AndAlso _text <> Nothing Then
            _toolTip = _text
        End If
        If _text <> Nothing Then
            oLbl.Text = _text.ToString

            oHid.Value = _valor
        End If

        If Not TryCast(Page.FindControl("Instancia"), HtmlControls.HtmlInputHidden) Is Nothing Then
            Try
                Instancia = CLng(TryCast(Page.FindControl("Instancia"), HtmlControls.HtmlInputHidden).Value)
            Catch ex As Exception
                'certif automatico tiene Hidden Instancia pero lo pone a ""
                Instancia = 0
            End Try
        Else
            Instancia = 0
        End If


        oLbl.ReadOnly = True
        oLbl.ID = "_t"
        oHid.ID = "_h"
        oLbl.Width = oWidth
        oLbl.Style("display") = ""
        oTbl = New System.Web.UI.WebControls.Table
        oTbl.Width = oWidth
        oTbl.BorderWidth = Unit.Pixel(0)
        oTbl.CellPadding = 0
        oTbl.CellSpacing = 0
        oTbl.Attributes.Add("onclick", "AbrirInstanciaVinculada(document.getElementById('" + sClientId + "__t'), " & Me.Instancia.ToString & " )")

        oLbl.BorderStyle = BorderStyle.None
        oLbl.BackColor = System.Drawing.Color.Transparent
        oLbl.ForeColor = System.Drawing.Color.Blue
        oLbl.Font.Underline = True


        oTblRow = New System.Web.UI.WebControls.TableRow
        oTblCell = New System.Web.UI.WebControls.TableCell
        oTblCell.Width = Unit.Percentage(100)
        oTblCell.Controls.Add(oLbl)
        oTblCell.Controls.Add(oHid)
        oTblRow.Cells.Add(oTblCell)

        oLbl.ButtonsAppearance.CustomButtonDisplay = Infragistics.WebUI.WebDataInput.ButtonDisplay.OnRight
        oLbl.ButtonsAppearance.CustomButtonImageUrl = Me.Page.ClientScript.GetWebResourceUrl(GetType(GeneralEntry), "Fullstep.DataEntry.trespuntos.gif")

        oTbl.Rows.Add(oTblRow)
        oTbl.ID = "_tbl"

        Me.Controls.Add(oTbl)
    End Sub
#End Region
#Region " Web control "
    ''' Revisado por: blp. Fecha: 04/10/2011
    ''' <summary>
    ''' Encapsula la generacion de dataentrys dependiendo del tipo
    ''' 
    ''' Nota: Toda la utilidad de tipopedido, partida, centros coste, ... no esta en Portal. Vamos lo q a 19/08/2010
    ''' estaba en el dataentry de producto y no el de portal sigue sin estar muy probablemente. 
    ''' Lista (infragistics.webdrop.v9.2) y Lista enlazada s� ha ido.
    ''' </summary>
    ''' <remarks>Llamada desde: La generacion de cualquier dataentry ;Tiempo m�ximo:0 </remarks>
    Protected Overrides Sub CreateChildControls()
        'DescrBreve = 1
        'DescrDetallada = 2
        'Importe = 3
        'Cantidad = 4
        'FecNecesidad = 5
        'IniSuministro = 6
        'FinSuministro = 7
        'Desglose = 8
        'Dest = 9
        'FormaPago = 10
        'Proveedor = 11
        'PRES1 = 12
        'Pres2 = 13
        'Pres3 = 14
        'Pres4 = 15
        'ArchivoEspecific = 16
        'Material = 17
        'CodArticulo = 18
        'Unidad = 19
        'PrecioUnitario = 20
        Select Case _tipoGS
            Case TiposDeDatos.TipoCampoGS.Desglose
                CtrlGSTipoDesglose()
            Case TiposDeDatos.TipoCampoGS.DesgloseActividad
                CtrlGSDesgloseActividad()
            Case TiposDeDatos.TipoCampoGS.DescrBreve
                CtrlTipoStringMedioYLargo(_tipo)
            Case TiposDeDatos.TipoCampoGS.DescrDetallada
                CtrlTipoStringMedioYLargo(_tipo)
            Case TiposDeDatos.TipoCampoGS.Cantidad
                CtrlTipoNumerico(True)
            Case TiposDeDatos.TipoCampoGS.Importe, TiposDeDatos.TipoCampoGS.PrecioUnitario, TiposDeDatos.TipoCampoGS.PrecioUnitarioAdj, TiposDeDatos.TipoCampoGS.CantidadAdj,
                    TiposDeDatos.TipoCampoGS.TotalLineaAdj, TiposDeDatos.TipoCampoGS.TotalLineaPreadj
                CtrlTipoNumerico()
            Case TiposDeDatos.TipoCampoGS.FecNecesidad, TiposDeDatos.TipoCampoGS.FinSuministro, TiposDeDatos.TipoCampoGS.IniSuministro, TiposDeDatos.TipoCampoGS.FecDespublicacion,
                TiposDeDatos.TipoCampoGS.FechaLimCumplim, TiposDeDatos.TipoCampoGS.FechaImputacionDefectos
                CtrlTipoFecha()
            Case TiposDeDatos.TipoCampoGS.Dest, TiposDeDatos.TipoCampoGS.FormaPago, TiposDeDatos.TipoCampoGS.Departamento, TiposDeDatos.TipoCampoGS.Unidad, TiposDeDatos.TipoCampoGS.UnidadPedido,
                 TiposDeDatos.TipoCampoGS.Moneda, TiposDeDatos.TipoCampoGS.Pais, TiposDeDatos.TipoCampoGS.Provincia, TiposDeDatos.TipoCampoGS.Contacto, TiposDeDatos.TipoCampoGS.EstadoNoConf,
                 TiposDeDatos.TipoCampoGS.Rol, TiposDeDatos.TipoCampoGS.OrganizacionCompras, TiposDeDatos.TipoCampoGS.Centro
                CtrlTipoLista(TiposDeDatos.TipoGeneral.TipoTextoMedio)
            Case TiposDeDatos.TipoCampoGS.Proveedor, TiposDeDatos.TipoCampoGS.ProveContacto, TiposDeDatos.TipoCampoGS.ProveedorAdj
                CtrlGSTipoProveedor()
            Case TiposDeDatos.TipoCampoGS.PRES1
                CtrlGSTipoPresupuesto(1)
            Case TiposDeDatos.TipoCampoGS.Pres2
                CtrlGSTipoPresupuesto(2)
            Case TiposDeDatos.TipoCampoGS.Pres3
                CtrlGSTipoPresupuesto(3)
            Case TiposDeDatos.TipoCampoGS.Pres4
                CtrlGSTipoPresupuesto(4)
            Case TiposDeDatos.TipoCampoGS.ArchivoEspecific
                If IdCampoPadre <> 0 Then
                    CtrlTipoArchivoDesglose()
                Else
                    CtrlTipoArchivo()
                End If
            Case TiposDeDatos.TipoCampoGS.Material
                CtrlGSTipoMaterial()
            Case TiposDeDatos.TipoCampoGS.CodArticulo
                CtrlGSTipoArticulo()
            Case TiposDeDatos.TipoCampoGS.NuevoCodArticulo
                CtrlGSTipoNuevoArticulo()
            Case TiposDeDatos.TipoCampoGS.DenArticulo
                CtrlGSTipoDenArticulo()
            Case TiposDeDatos.TipoCampoGS.Persona, TiposDeDatos.TipoCampoGS.Comprador
                CtrlGSTipoPersona()
            Case TiposDeDatos.TipoCampoGS.UnidadOrganizativa
                CtrlGSTipoUnidadOrganizativa()
            Case TiposDeDatos.TipoCampoGS.Almacen
                CtrlTipoLista(TiposDeDatos.TipoGeneral.TipoNumerico)
            Case TiposDeDatos.TipoCampoGS.Empresa
                CtrlGSTipoEmpresa()
            Case TiposDeDatos.TipoCampoGS.ListadosPersonalizados
                If _lista Is Nothing Then
                    CtrlTipoStringMedioYLargo(TiposDeDatos.TipoGeneral.TipoTextoLargo)
                Else
                    CtrlTipoLista(TiposDeDatos.TipoGeneral.TipoTextoLargo)
                End If
            Case TiposDeDatos.TipoCampoGS.RefSolicitud
                CtrlGSTipoRefSolicitud()
            Case TiposDeDatos.TipoCampoGS.ImporteSolicitudesVinculadas
                CtrlGSTipoImporteSolicitudVinculada()
            Case TiposDeDatos.TipoCampoGS.ImporteRepercutido
                CtrlGSTipoImporteRepercutido()
            Case TiposDeDatos.TipoCampoGS.CentroCoste
                CtrlGSTipoSM()
            Case TiposDeDatos.TipoCampoGS.Partida
                CtrlGSTipoSM()
            Case TiposDeDatos.TipoCampoGS.Activo
                CtrlGSTipoSM()
			Case TiposDeDatos.TipoCampoGS.AnyoPartida
				CtrlGSTipoAnyoPartidaSM()
            Case TiposDeDatos.TipoCampoGS.TipoPedido
                CtrlGSTipoTipoPedido()
            Case TiposDeDatos.TipoCampoGS.Factura
                CtrlGSTipoFactura()
            Case TiposDeDatos.TipoCampoGS.Subtipo
                CtrlTipoLista(_tipo)
            Case TiposDeDatos.IdsFicticios.DesgloseVinculado
                CtrlTipoDesgloseVinculado()
            Case TiposDeDatos.TipoCampoGS.ProveedorERP
                CtrlTipoLista(_tipo)
            Case Else
                If _lTablaExterna > 0 Then
                    CtrlTablaExterna()
                Else
                    If _intro = 1 Then
                        Select Case _tipo
                            Case TiposDeDatos.TipoGeneral.TipoNumerico, TiposDeDatos.TipoGeneral.TipoFecha, TiposDeDatos.TipoGeneral.TipoTextoMedio, TiposDeDatos.TipoGeneral.TipoString, TiposDeDatos.TipoGeneral.TipoTextoCorto, FSNLibrary.TiposDeDatos.TipoGeneral.TipoDepBajaLog
                                CtrlTipoLista(_tipo)
                            Case TiposDeDatos.TipoGeneral.TipoTextoLargo
                                CtrlTipoStringLargo()
                            Case TiposDeDatos.TipoGeneral.TipoBoolean
                                CtrlTipoLista(_tipo)
                        End Select
                    ElseIf _intro = 2 Then
                        CtrlTipoListaExterna()
                    Else
                        Select Case _tipo
                            Case TiposDeDatos.TipoGeneral.TipoTextoMedio
                                CtrlTipoStringMedioYLargo(TiposDeDatos.TipoGeneral.TipoTextoMedio)
                            Case TiposDeDatos.TipoGeneral.TipoTextoLargo
                                CtrlTipoStringMedioYLargo(TiposDeDatos.TipoGeneral.TipoTextoLargo)
                            Case TiposDeDatos.TipoGeneral.TipoString, TiposDeDatos.TipoGeneral.TipoTextoCorto
                                CtrlTipoString(_tipo)
                            Case TiposDeDatos.TipoGeneral.TipoNumerico
                                CtrlTipoNumerico()
                            Case TiposDeDatos.TipoGeneral.TipoFecha
                                CtrlTipoFecha()
                            Case TiposDeDatos.TipoGeneral.TipoArchivo
                                If _IdCampoPadre <> 0 Then
                                    CtrlTipoArchivoDesglose()
                                Else
                                    CtrlTipoArchivo()
                                End If
                            Case TiposDeDatos.TipoGeneral.TipoDesglose
                                CtrlGSTipoDesglose()
                            Case TiposDeDatos.TipoGeneral.TipoCheckBox
                                CtrlGSTipoChekBox()
                            Case TiposDeDatos.TipoGeneral.TipoEditor
                                CtrlTipoEditor()
                            Case TiposDeDatos.TipoGeneral.TipoEnlace
                                CtrlTipoEnlace()
                        End Select
                    End If
                End If

        End Select
    End Sub
    ''' Revisado por: blp. Fecha: 04/10/2011
    ''' <summary>
    ''' Esta funci�n establece el estilo del entry y los onclick de los diferentes botones/links asociados
    ''' 
    ''' Nota: Toda la utilidad de tipopedido, partida, centros coste, ... no esta en Portal. Vamos lo q a 19/08/2010
    ''' estaba en el dataentry de producto y no el de portal sigue sin estar muy probablemente. 
    ''' Lista (infragistics.webdrop.v9.2) y Lista enlazada s� ha ido.
    ''' </summary>
    ''' <remarks>Llamada desde: Render; Tiempo m�ximo: 0</remarks>
    Private Sub PrepareControlHierarchy()
        Dim altItemStyle As System.Web.UI.WebControls.Style = Nothing
        If HasControls() = False Then
            Return
        End If

        Dim oInput As Infragistics.WebUI.WebDataInput.WebTextEdit = Nothing
        Dim oInputTextBox As System.Web.UI.WebControls.TextBox = Nothing
        Dim oInputDropDown As Infragistics.Web.UI.ListControls.WebDropDown = Nothing
        Dim oCheckBox As System.Web.UI.WebControls.CheckBox = Nothing
        Dim oButton As System.Web.UI.HtmlControls.HtmlInputButton = Nothing

        Select Case _tipoGS
            Case TiposDeDatos.TipoCampoGS.Desglose
                oInput = CType(Controls(0).Controls(0).Controls(0).Controls(0), Infragistics.WebUI.WebDataInput.WebTextEdit)
                oInput.ClientSideEvents.CustomButtonPress = "show_desglose"
                altItemStyle = New System.Web.UI.WebControls.Style
                altItemStyle.CopyFrom(_InputStyle)
                oInput.MergeStyle(altItemStyle)
                oInput.ButtonsAppearance.ButtonStyle.CssClass = "fsstyentrydd"

            Case TiposDeDatos.TipoCampoGS.PRES1
                oInput = CType(Controls(0).Controls(0).Controls(0).Controls(0), Infragistics.WebUI.WebDataInput.WebTextEdit)
                If _readOnly Then
                    oInput.ClientSideEvents.CustomButtonPress = "show_pres_ReadOnly"
                Else
                    oInput.ClientSideEvents.CustomButtonPress = "show_pres"
                    oInput.ClientSideEvents.KeyDown = "fsVaciaCombo"
                End If
                If Me.PM = True Then
                    Dim oInput2 As Infragistics.WebUI.WebDataInput.WebTextEdit
                    oInput2 = CType(Controls(0).Controls(0).Controls(1).Controls(0), Infragistics.WebUI.WebDataInput.WebTextEdit)

                    If Not _readOnly Then
                        oInput2.ClientSideEvents.CustomButtonPress = "ddPresupFavopenDropDownEvent"
                    End If
                End If
                altItemStyle = New System.Web.UI.WebControls.Style
                altItemStyle.CopyFrom(_InputStyle)
                oInput.MergeStyle(altItemStyle)
                oInput.ButtonsAppearance.ButtonStyle.CssClass = "fsstyentrydd"

            Case TiposDeDatos.TipoCampoGS.Pres2
                oInput = CType(Controls(0).Controls(0).Controls(0).Controls(0), Infragistics.WebUI.WebDataInput.WebTextEdit)
                If _readOnly Then
                    oInput.ClientSideEvents.CustomButtonPress = "show_pres_ReadOnly"
                Else
                    oInput.ClientSideEvents.CustomButtonPress = "show_pres"
                    oInput.ClientSideEvents.KeyDown = "fsVaciaCombo"
                End If
                If Me.PM = True Then
                    Dim oInput2 As Infragistics.WebUI.WebDataInput.WebTextEdit
                    oInput2 = CType(Controls(0).Controls(0).Controls(1).Controls(0), Infragistics.WebUI.WebDataInput.WebTextEdit)

                    If Not _readOnly Then
                        oInput2.ClientSideEvents.CustomButtonPress = "ddPresupFavopenDropDownEvent"
                    End If
                End If
                altItemStyle = New System.Web.UI.WebControls.Style
                altItemStyle.CopyFrom(_InputStyle)
                oInput.MergeStyle(altItemStyle)
                oInput.ButtonsAppearance.ButtonStyle.CssClass = "fsstyentrydd"

            Case TiposDeDatos.TipoCampoGS.Pres3
                oInput = CType(Controls(0).Controls(0).Controls(0).Controls(0), Infragistics.WebUI.WebDataInput.WebTextEdit)
                If _readOnly Then
                    oInput.ClientSideEvents.CustomButtonPress = "show_pres_ReadOnly"
                Else
                    oInput.ClientSideEvents.CustomButtonPress = "show_pres"
                    oInput.ClientSideEvents.KeyDown = "fsVaciaCombo"
                End If
                If Me.PM = True Then
                    Dim oInput2 As Infragistics.WebUI.WebDataInput.WebTextEdit
                    oInput2 = CType(Controls(0).Controls(0).Controls(1).Controls(0), Infragistics.WebUI.WebDataInput.WebTextEdit)

                    If Not _readOnly Then
                        oInput2.ClientSideEvents.CustomButtonPress = "ddPresupFavopenDropDownEvent"
                    End If
                End If
                altItemStyle = New System.Web.UI.WebControls.Style
                altItemStyle.CopyFrom(_InputStyle)
                oInput.MergeStyle(altItemStyle)
                oInput.ButtonsAppearance.ButtonStyle.CssClass = "fsstyentrydd"

            Case TiposDeDatos.TipoCampoGS.Pres4
                oInput = CType(Controls(0).Controls(0).Controls(0).Controls(0), Infragistics.WebUI.WebDataInput.WebTextEdit)
                If _readOnly Then
                    oInput.ClientSideEvents.CustomButtonPress = "show_pres_ReadOnly"
                Else
                    oInput.ClientSideEvents.CustomButtonPress = "show_pres"
                    oInput.ClientSideEvents.KeyDown = "fsVaciaCombo"
                End If
                If Me.PM = True Then
                    Dim oInput2 As Infragistics.WebUI.WebDataInput.WebTextEdit
                    oInput2 = CType(Controls(0).Controls(0).Controls(1).Controls(0), Infragistics.WebUI.WebDataInput.WebTextEdit)

                    If Not _readOnly Then
                        oInput2.ClientSideEvents.CustomButtonPress = "ddPresupFavopenDropDownEvent"
                    End If
                End If
                altItemStyle = New System.Web.UI.WebControls.Style
                altItemStyle.CopyFrom(_InputStyle)
                oInput.MergeStyle(altItemStyle)
                oInput.ButtonsAppearance.ButtonStyle.CssClass = "fsstyentrydd"

            Case TiposDeDatos.TipoCampoGS.Proveedor
                oInput = CType(Controls(0).Controls(0).Controls(0).Controls(0), Infragistics.WebUI.WebDataInput.WebTextEdit)
                If Not _readOnly Then
                    oInput.ClientSideEvents.CustomButtonPress = "show_proves"
                    oInput.ClientSideEvents.KeyDown = "fsVaciaCombo"
                End If
                If Me.PM = True Then
                    If Not _readOnly Then
                        Dim oInput2 As Infragistics.WebUI.WebDataInput.WebTextEdit
                        oInput2 = CType(Controls(0).Controls(0).Controls(1).Controls(0), Infragistics.WebUI.WebDataInput.WebTextEdit)
                        oInput2.ClientSideEvents.CustomButtonPress = "ddProvFavopenDropDownEvent"
                    End If
                End If

                altItemStyle = New System.Web.UI.WebControls.Style
                altItemStyle.CopyFrom(_InputStyle)
                oInput.MergeStyle(altItemStyle)
                oInput.ButtonsAppearance.ButtonStyle.CssClass = "fsstyentrydd"
            Case TiposDeDatos.TipoCampoGS.Material
                oInput = CType(Controls(0).Controls(0).Controls(0).Controls(0), Infragistics.WebUI.WebDataInput.WebTextEdit)
                oInput.ClientSideEvents.CustomButtonPress = "show_mat"
                If Not _readOnly Then
                    oInput.ClientSideEvents.KeyDown = "VaciarMat_NuevoCodArt_Den"
                End If
                altItemStyle = New System.Web.UI.WebControls.Style
                altItemStyle.CopyFrom(_InputStyle)
                oInput.MergeStyle(altItemStyle)
                oInput.ButtonsAppearance.ButtonStyle.CssClass = "fsstyentrydd"

            Case TiposDeDatos.TipoCampoGS.ArchivoEspecific
                altItemStyle = New System.Web.UI.WebControls.Style
                altItemStyle.CopyFrom(_InputStyle)

                Dim sClientId As String = String.Empty
                Dim sCampoId As String

                If Not Me.IdContenedor = Nothing Then
                    sClientId = Me.IdContenedor + "_"
                End If
                sClientId += Me.ID

                sCampoId = Me.ID
                sCampoId = Replace(sCampoId, "fsentry", "")
                sCampoId = Replace(sCampoId, "fsdsentry", "")
                sCampoId = Mid(sCampoId, InStrRev(sCampoId, "_") + 1)

            Case TiposDeDatos.TipoCampoGS.ProveContacto
                oInput = CType(Controls(0).Controls(0).Controls(0).Controls(0), Infragistics.WebUI.WebDataInput.WebTextEdit)
                If Not _readOnly Then
                    oInput.ClientSideEvents.CustomButtonPress = "ddPConPreInitDropDown('" + _DependentField + "###" + _IdContenedor + "###" + Me.ClientID + "');ddPConopenDropDownEvent"
                    oInput.ClientSideEvents.KeyDown = "fsVaciaCombo"
                End If
                altItemStyle = New System.Web.UI.WebControls.Style
                altItemStyle.CopyFrom(_InputStyle)
                oInput.MergeStyle(altItemStyle)
                oInput.ButtonsAppearance.ButtonStyle.CssClass = "fsstyentrydd"

            Case TiposDeDatos.TipoCampoGS.Persona
                If _intro = 0 Then
                    oInput = CType(Controls(0).Controls(0).Controls(0).Controls(0), Infragistics.WebUI.WebDataInput.WebTextEdit)
                    If Not _readOnly Then
                        oInput.ClientSideEvents.CustomButtonPress = "show_personas"
                        oInput.ClientSideEvents.KeyDown = "fsVaciaCombo"
                    End If
                    altItemStyle = New System.Web.UI.WebControls.Style
                    altItemStyle.CopyFrom(_InputStyle)
                    oInput.MergeStyle(altItemStyle)
                    oInput.ButtonsAppearance.ButtonStyle.CssClass = "fsstyentrydd"
                End If

            Case TiposDeDatos.TipoCampoGS.Comprador
                If _intro = 0 Then
                    oInput = CType(Controls(0).Controls(0).Controls(0).Controls(0), Infragistics.WebUI.WebDataInput.WebTextEdit)
                    If Not _readOnly Then
                        oInput.ClientSideEvents.CustomButtonPress = "show_compradores"
                        oInput.ClientSideEvents.KeyDown = "fsVaciaCombo"
                    End If
                    altItemStyle = New System.Web.UI.WebControls.Style
                    altItemStyle.CopyFrom(_InputStyle)
                    oInput.MergeStyle(altItemStyle)
                    oInput.ButtonsAppearance.ButtonStyle.CssClass = "fsstyentrydd"
                End If

            Case TiposDeDatos.TipoCampoGS.UnidadOrganizativa
                oInput = CType(Controls(0).Controls(0).Controls(0).Controls(0), Infragistics.WebUI.WebDataInput.WebTextEdit)
                oInput.ClientSideEvents.CustomButtonPress = "show_UnidadesOrganizativa"
                If Not _readOnly Then
                    oInput.ClientSideEvents.KeyDown = "fsVaciaCombo"
                End If
                altItemStyle = New System.Web.UI.WebControls.Style
                altItemStyle.CopyFrom(_InputStyle)
                oInput.MergeStyle(altItemStyle)
                oInput.ButtonsAppearance.ButtonStyle.CssClass = "fsstyentrydd"

            Case TiposDeDatos.TipoCampoGS.RefSolicitud
                oInput = CType(Controls(0).Controls(0).Controls(0).Controls(0), Infragistics.WebUI.WebDataInput.WebTextEdit)
                If Not _readOnly Then
                    oInput.ClientSideEvents.CustomButtonPress = "show_Solicitudes"
                    oInput.ClientSideEvents.KeyDown = "fsVaciaCombo"
                Else
                    oInput.ClientSideEvents.CustomButtonPress = "show_DetalleSolicitudPadre"
                End If
                altItemStyle = New System.Web.UI.WebControls.Style
                altItemStyle.CopyFrom(_InputStyle)
                oInput.MergeStyle(altItemStyle)
                oInput.ButtonsAppearance.ButtonStyle.CssClass = "fsstyentrydd"

            Case TiposDeDatos.TipoCampoGS.ImporteSolicitudesVinculadas
                oInput = CType(Controls(0).Controls(0).Controls(0).Controls(0), Infragistics.WebUI.WebDataInput.WebTextEdit)
                oInput.ClientSideEvents.CustomButtonPress = "show_Detalles"
                altItemStyle = New System.Web.UI.WebControls.Style
                altItemStyle.CopyFrom(_InputStyle)
                oInput.MergeStyle(altItemStyle)
                oInput.ButtonsAppearance.ButtonStyle.CssClass = "fsstyentrydd"

            Case TiposDeDatos.TipoCampoGS.ImporteRepercutido
                Try
                    oInput = CType(Controls(0).Controls(0).Controls(0).Controls(0), Infragistics.WebUI.WebDataInput.WebNumericEdit)

                    Dim oInput2 As System.Web.UI.HtmlControls.HtmlAnchor
                    oInput2 = CType(Controls(0).Controls(0).Controls(1).Controls(0), System.Web.UI.HtmlControls.HtmlAnchor)

                    oInput2.Attributes.Item("ONCLICK") = "ddMonedaopenDropDownEvent(this, '" & oInput.ClientID & "','" & oInput2.ClientID & "'); return false"
                Catch ex As Exception
                    oInput = CType(Controls(0).Controls(0).Controls(0).Controls(0), Infragistics.WebUI.WebDataInput.WebTextEdit)
                End Try

                altItemStyle = New System.Web.UI.WebControls.Style
                altItemStyle.CopyFrom(_InputStyle)
                oInput.MergeStyle(altItemStyle)
            Case TiposDeDatos.TipoCampoGS.CentroCoste
                oInput = CType(Controls(0).Controls(0).Controls(0).Controls(0), Infragistics.WebUI.WebDataInput.WebTextEdit)
                If Me.Portal = False Then
                    oInput.ClientSideEvents.CustomButtonPress = "show_centros_coste"
                    If Not _readOnly Then oInput.ClientSideEvents.ValueChange = "ddCentroCoste_ValueChange"
                End If
                altItemStyle = New System.Web.UI.WebControls.Style
                altItemStyle.CopyFrom(_InputStyle)
                oInput.MergeStyle(altItemStyle)
                oInput.ButtonsAppearance.ButtonStyle.CssClass = "fsstyentrydd"
            Case TiposDeDatos.TipoCampoGS.Partida
                oInput = CType(Controls(0).Controls(0).Controls(0).Controls(0), Infragistics.WebUI.WebDataInput.WebTextEdit)
                If Not _readOnly Then
                    If Me.Portal = False Then
                        oInput.ClientSideEvents.CustomButtonPress = "show_partidas"
                        oInput.ClientSideEvents.ValueChange = "ddPartida_ValueChange"
                    End If
                Else
                    oInput.ClientSideEvents.CustomButtonPress = "show_detalle_partida"
                End If
                altItemStyle = New System.Web.UI.WebControls.Style
                altItemStyle.CopyFrom(_InputStyle)
                oInput.MergeStyle(altItemStyle)
                oInput.ButtonsAppearance.ButtonStyle.CssClass = "fsstyentrydd"
            Case TiposDeDatos.TipoCampoGS.Activo
                oInput = CType(Controls(0).Controls(0).Controls(0).Controls(0), Infragistics.WebUI.WebDataInput.WebTextEdit)
                If Not _readOnly Then
                    If Me.Portal = False Then
                        oInput.ClientSideEvents.CustomButtonPress = "show_activos"
                        oInput.ClientSideEvents.ValueChange = "ddActivo_ValueChange"
                    End If
                Else
                    oInput.ClientSideEvents.CustomButtonPress = "show_detalle_activo"
                End If
                altItemStyle = New System.Web.UI.WebControls.Style
                altItemStyle.CopyFrom(_InputStyle)
                oInput.MergeStyle(altItemStyle)
                oInput.ButtonsAppearance.ButtonStyle.CssClass = "fsstyentrydd"
            Case TiposDeDatos.TipoCampoGS.TipoPedido
                If Me.Portal = False Then
                    altItemStyle = New System.Web.UI.WebControls.Style
                    altItemStyle.CopyFrom(_InputStyle)
                    If _readOnly Then
                        oInput = CType(Controls(0).Controls(0).Controls(0).Controls(0), Infragistics.WebUI.WebDataInput.WebTextEdit)
                        oInput.MergeStyle(altItemStyle)
                        oInput.ClientSideEvents.CustomButtonPress = "show_detalle_tipo_pedido"
                        oInput.ButtonsAppearance.ButtonStyle.CssClass = "fsstyentrydd"
                    Else
                        oInputDropDown = CType(Controls(0).Controls(0).Controls(0).Controls(0), Infragistics.Web.UI.ListControls.WebDropDown)
                        oInputDropDown.MergeStyle(altItemStyle)
                    End If
                End If
            Case TiposDeDatos.TipoCampoGS.Factura
                oInput = CType(Controls(0).Controls(0).Controls(0).Controls(0), Infragistics.WebUI.WebDataInput.WebTextEdit)
                If Not _readOnly AndAlso Me.Portal = False Then
                    oInput.ClientSideEvents.CustomButtonPress = "show_facturas"
                    oInput.ClientSideEvents.ValueChange = "ddFactura_ValueChange"
                    oInput.ClientSideEvents.KeyDown = "factura_keydown"
                End If

                altItemStyle = New System.Web.UI.WebControls.Style
                altItemStyle.CopyFrom(_InputStyle)
                oInput.MergeStyle(altItemStyle)
                oInput.ButtonsAppearance.ButtonStyle.CssClass = "fsstyentrydd"
            Case TiposDeDatos.IdsFicticios.DesgloseVinculado
                oInput = CType(Controls(0).Controls(0).Controls(0).Controls(0), Infragistics.WebUI.WebDataInput.WebTextEdit)
                oInput.ClientSideEvents.CustomButtonPress = "show_detalle_DesgloseVinculado"
                altItemStyle = New System.Web.UI.WebControls.Style
                altItemStyle.CopyFrom(_InputStyle)
                altItemStyle.CssClass = altItemStyle.CssClass & " CursorMano"
                oInput.MergeStyle(altItemStyle)
                oInput.ButtonsAppearance.ButtonStyle.CssClass = "fsstyentrydd"

            Case TiposDeDatos.TipoCampoGS.Rol
                oInput = CType(Controls(0).Controls(0).Controls(0).Controls(0), Infragistics.WebUI.WebDataInput.WebTextEdit)
                altItemStyle = New System.Web.UI.WebControls.Style
                altItemStyle.CopyFrom(_InputStyle)
                oInput.MergeStyle(altItemStyle)

            Case TiposDeDatos.TipoCampoGS.DesgloseActividad

            Case TiposDeDatos.TipoCampoGS.Dest
                If _readOnly Then
                    oInput = CType(Controls(0).Controls(0).Controls(0).Controls(0), Infragistics.WebUI.WebDataInput.WebTextEdit)
                    altItemStyle = New System.Web.UI.WebControls.Style
                    altItemStyle.CopyFrom(_InputStyle)
                    oInput.MergeStyle(altItemStyle)
                    oInput.ButtonsAppearance.ButtonStyle.CssClass = "fsstyentrydd"
                End If

                oInput = CType(Controls(0).Controls(0).Controls(1).Controls(0), Infragistics.WebUI.WebDataInput.WebTextEdit)
                oInput.ClientSideEvents.CustomButtonPress = "showDetalleDestino"
                altItemStyle = New System.Web.UI.WebControls.Style
                altItemStyle.CopyFrom(_InputStyle)
                altItemStyle.CssClass = altItemStyle.CssClass & " CursorMano"
                oInput.MergeStyle(altItemStyle)
                oInput.ButtonsAppearance.ButtonStyle.CssClass = "fsstyentrydd"
            Case TiposDeDatos.TipoCampoGS.Empresa
                If Not _readOnly Then
                    Dim oBut As System.Web.UI.HtmlControls.HtmlInputButton = Me.Controls(0).Controls(0).Controls(0).Controls(0).Controls(0).Controls(1).Controls(0)
                    oBut.Attributes("onclick") = "show_companies($('#" & Me.ClientID & "__t'),$('#" & Me.ClientID & "__t_t').val(),event)"
                End If
            Case Else
                If _lTablaExterna > 0 Then
                    oInput = CType(Controls(0).Controls(0).Controls(0).Controls(0), Infragistics.WebUI.WebDataInput.WebTextEdit)
                    If _readOnly Then
                        oInput.ClientSideEvents.CustomButtonPress = "show_externa_ReadOnly"
                    Else
                        oInput.ClientSideEvents.CustomButtonPress = "show_externa"
                        oInput.ClientSideEvents.KeyDown = "CerrarDesplegables"
                        oInput.ClientSideEvents.Blur = "validar_externa"
                    End If
                    altItemStyle = New System.Web.UI.WebControls.Style
                    altItemStyle.CopyFrom(_InputStyle)
                    oInput.MergeStyle(altItemStyle)
                    oInput.ButtonsAppearance.ButtonStyle.CssClass = "fsstyentrydd"
                Else
                    If _intro = 0 Then
                        Select Case _tipo
                            Case TiposDeDatos.TipoGeneral.TipoTextoCorto
                                If RecordarValores Then
                                    Dim oTbl As System.Web.UI.HtmlControls.HtmlTable = CType(Controls(0).Controls(0).Controls(0).Controls(1), System.Web.UI.HtmlControls.HtmlTable)
                                    If Not _InputStyle Is Nothing Then
                                        oTbl.Attributes("Class") = _InputStyle.CssClass
                                    End If

                                    Try
                                        If Not _readOnly Then
                                            oButton = CType(Controls(0).Controls(0).Controls(0).Controls(1).Controls(0).Controls(1).Controls(0), System.Web.UI.HtmlControls.HtmlInputButton)
                                            oButton.Attributes("onclick") = "AbrirListaValoresAutoCompletado('" + IIf(Me._CampoOrigen = 0, Me.Tag, Me._CampoOrigen.ToString) + "','" + HttpUtility.UrlEncode(Me._title) + "')"
                                        End If

                                    Catch ex As Exception

                                    End Try
                                Else
                                    oInput = CType(Controls(0).Controls(0).Controls(0).Controls(0), Infragistics.WebUI.WebDataInput.WebTextEdit)
                                    If Not _readOnly AndAlso Not _sFormato Is Nothing Then
                                        oInput.ClientSideEvents.ValueChange = "validarFormatoTextoCorto"
                                    End If
                                End If
                            Case TiposDeDatos.TipoGeneral.TipoNumerico
                                oInput = CType(Controls(0).Controls(0).Controls(0).Controls(0), Infragistics.WebUI.WebDataInput.WebNumericEdit)
                                If _tipoGS = TiposDeDatos.TipoCampoGS.Cantidad Then
                                    oInput.ClientSideEvents.ValueChange = "Cantidad_ValueChange"
                                    oInput.ClientSideEvents.Blur = "Cantidad_ValueChange"
                                End If
                            Case TiposDeDatos.TipoGeneral.TipoFecha
                                oInput = CType(Controls(0).Controls(0).Controls(0).Controls(0), Infragistics.WebUI.WebDataInput.WebDateTimeEdit)
                                If Not _maximo Is Nothing Then
                                    oInput.ClientSideEvents.InvalidValue = "fecha_invalida"
                                End If
                                If Not _readOnly Then
                                    Dim oDt As Infragistics.WebUI.WebDataInput.WebDateTimeEdit
                                    oDt = oInput
                                    oDt.ButtonsAppearance.CustomButtonDisplay = Infragistics.WebUI.WebDataInput.ButtonDisplay.OnRight
                                    oDt.ClientSideEvents.CustomButtonPress = "openDropDownEvent"
                                    oDt.ClientSideEvents.KeyDown = "closeDropDownEvent"
                                    oDt.ClientSideEvents.Spin = "closeDropDownEvent"
                                    oDt.ClientSideEvents.Focus = "closeDropDownEvent"
                                    oDt.SpinButtons.Display = Infragistics.WebUI.WebDataInput.ButtonDisplay.OnLeft
                                    oDt.SpinButtons.SpinOnReadOnly = True
                                    oDt.ButtonsAppearance.ButtonStyle.CssClass = "fsstyentryddFecha"
                                    oDt = Nothing
                                End If

                            Case TiposDeDatos.TipoGeneral.TipoArchivo
                                Dim sClientId As String = String.Empty
                                Dim sCampoId As String

                                If Not Me.IdContenedor = Nothing Then
                                    sClientId = Me.IdContenedor + "_"
                                End If
                                sClientId += Me.ID

                                sCampoId = Me.ID
                                sCampoId = Replace(sCampoId, "fsentry", "")
                                sCampoId = Replace(sCampoId, "fsdsentry", "")
                                sCampoId = Mid(sCampoId, InStrRev(sCampoId, "_") + 1)

                            Case TiposDeDatos.TipoGeneral.TipoDesglose
                                oInput = CType(Controls(0).Controls(0).Controls(0).Controls(0), Infragistics.WebUI.WebDataInput.WebTextEdit)
                                oInput.ClientSideEvents.CustomButtonPress = "show_desglose"
                                altItemStyle = New System.Web.UI.WebControls.Style
                                altItemStyle.CopyFrom(_InputStyle)
                                oInput.MergeStyle(altItemStyle)
                            Case TiposDeDatos.TipoGeneral.TipoCheckBox
                                oCheckBox = CType(Controls(0).Controls(0).Controls(0).Controls(0), System.Web.UI.WebControls.CheckBox)
                        End Select

                        If Not oCheckBox Is Nothing Then
                            If Not (_InputStyle Is Nothing) Then
                                altItemStyle = New System.Web.UI.WebControls.Style
                                altItemStyle.CopyFrom(_InputStyle)
                                oCheckBox.MergeStyle(altItemStyle)
                            End If
                        End If

                        If Not oInput Is Nothing Then
                            If Not (_InputStyle Is Nothing) Then
                                altItemStyle = New System.Web.UI.WebControls.Style
                                altItemStyle.CopyFrom(_InputStyle)
                                oInput.MergeStyle(altItemStyle)
                                Select Case _tipo
                                    Case TiposDeDatos.TipoGeneral.TipoBoolean
                                        oInput.ButtonsAppearance.ButtonStyle.CssClass = "fsstyentrylista"
                                    Case TiposDeDatos.TipoGeneral.TipoFecha
                                        oInput.ButtonsAppearance.ButtonStyle.CssClass = "fsstyentryddFecha"
                                    Case Else
                                        oInput.ButtonsAppearance.ButtonStyle.CssClass = "fsstyentrydd"
                                End Select
                            End If
                        End If

                        If Not oButton Is Nothing Then
                            If Not (_ButtonStyle Is Nothing) Then
                                oButton.Attributes.Item("Class") = _ButtonStyle.CssClass
                            End If
                        End If
                    ElseIf _intro = 1 Then
                        If _tipoGS = TiposDeDatos.TipoCampoGS.CodArticulo OrElse _tipoGS = TiposDeDatos.TipoCampoGS.ImporteSolicitudesVinculadas _
                        OrElse _tipoGS = TiposDeDatos.TipoCampoGS.UnidadOrganizativa OrElse _tipoGS = TiposDeDatos.TipoCampoGS.ProveContacto Then
                            Select Case _tipo
                                Case TiposDeDatos.TipoGeneral.TipoTextoMedio, TiposDeDatos.TipoGeneral.TipoString, TiposDeDatos.TipoGeneral.TipoTextoCorto
                                    oInput = CType(Controls(0).Controls(0).Controls(0).Controls(0), Infragistics.WebUI.WebDataInput.WebTextEdit)
                                Case TiposDeDatos.TipoGeneral.TipoNumerico
                                    oInput = CType(Controls(0).Controls(0).Controls(0).Controls(0), Infragistics.WebUI.WebDataInput.WebNumericEdit)
                                Case TiposDeDatos.TipoGeneral.TipoFecha
                                    oInput = CType(Controls(0).Controls(0).Controls(0).Controls(0), Infragistics.WebUI.WebDataInput.WebDateTimeEdit)
                            End Select
                        ElseIf _tipoGS = TiposDeDatos.TipoCampoGS.NuevoCodArticulo OrElse _tipoGS = TiposDeDatos.TipoCampoGS.DenArticulo Then
                            oInputTextBox = CType(Controls(0).Controls(0).Controls(0).Controls(0).Controls(0).Controls(0).Controls(0), System.Web.UI.WebControls.TextBox)
                        Else
                            If _readOnly Then
                                oInput = CType(Controls(0).Controls(0).Controls(0).Controls(0), Infragistics.WebUI.WebDataInput.WebTextEdit)
                            End If
                        End If

                        If oInput IsNot Nothing OrElse oInputDropDown IsNot Nothing Or oInputTextBox IsNot Nothing Then 'Tipo de pedido??
                            Dim bEsLista As Boolean
                            If oInputDropDown IsNot Nothing AndAlso Not _readOnly Then
                                oInputDropDown.ClientEvents.DropDownOpening = "WebDropDown_DataCheck"
                            End If
                            If oInputTextBox IsNot Nothing AndAlso Not _readOnly Then
                                Select Case _tipoGS
                                    Case TiposDeDatos.TipoCampoGS.NuevoCodArticulo
                                        'table-tablerow-tablecell-table-tablerow-tablecell-textbox
                                        oInputTextBox = Me.Controls(0).Controls(0).Controls(0).Controls(0).Controls(0).Controls(0).Controls(0)
                                        oInputTextBox.Attributes("onchange") = "ddNuevoCodArticulo_ValueChange($('#" & Me.ClientID & "__t'),$('#" & Me.ClientID & "__t_t').val(),event)"
                                        oInputTextBox.Attributes("onReadyStateChange") = "ddNuevoCodArticulo_ValueChange($('#" & Me.ClientID & "__t'),$('#" & Me.ClientID & "__t_t').val(),event)"

                                        oInputTextBox.Attributes("onkeyup") = "ddArtsPreInitDropDown('" + _DependentField + "###" + _IdContenedor + "###" + Me.ClientID + "')"
                                        If Not _bPortal Then oInputTextBox.Attributes("onkeyup") &= ";SetContextKey($('#" & Me.ClientID & "__t'))"
                                        If Not _bPortal Then oInputTextBox.Attributes("onblur") = "ResetContextKey()"

                                        'table-tablerow-tablecell-table-tablerow-tablecell-button                                        
                                        Dim oBut As System.Web.UI.HtmlControls.HtmlInputButton = Me.Controls(0).Controls(0).Controls(0).Controls(0).Controls(0).Controls(1).Controls(0)
                                        oBut.Attributes("onclick") = "ddArtsPreInitDropDown('" + _DependentField + "###" + _IdContenedor + "###" + Me.ClientID + "');ddArtsopenDropDownEvent($('#" & Me.ClientID & "__t'),$('#" & Me.ClientID & "__t_t').val(),event)"

                                        If Me.PM Then
                                            'table-tablerow-tablecell-table-tablerow-tablecell-WebTextEdit
                                            Dim oInput2 As Infragistics.WebUI.WebDataInput.WebTextEdit
                                            oInput2 = CType(Controls(0).Controls(0).Controls(1).Controls(0).Controls(0).Controls(0).Controls(0), Infragistics.WebUI.WebDataInput.WebTextEdit)
                                            If oInputTextBox.Text <> "" Then oInput2.ClientSideEvents.CustomButtonPress = "showDetalleArticulo"
                                            If Me.PM And Not (Me.TipoSolicit = TiposDeDatos.TipoDeSolicitud.Certificado) _
                                                 And Not (Me.TipoSolicit = TiposDeDatos.TipoDeSolicitud.NoConformidad) _
                                                 And Not (Me.TipoSolicit = TiposDeDatos.TipoDeSolicitud.Contrato) _
                                                 And Not (Me.TipoSolicit = TiposDeDatos.TipoDeSolicitud.Autofactura) _
                                                 And Not (Me.TipoSolicit = TiposDeDatos.TipoDeSolicitud.SolicitudDePedidoCatalogo) _
                                                 And Not (Me.TipoSolicit = TiposDeDatos.TipoDeSolicitud.SolicitudDePedidoContraAbierto) Then
                                                Dim oInput3 As Infragistics.WebUI.WebDataInput.WebTextEdit
                                                oInput3 = CType(Controls(0).Controls(0).Controls(1).Controls(0).Controls(0).Controls(1).Controls(0), Infragistics.WebUI.WebDataInput.WebTextEdit)
                                                oInput3.ClientSideEvents.CustomButtonPress = "ddArticuloFavopenDropDownEvent"
                                            End If
                                        End If

                                        altItemStyle = New System.Web.UI.WebControls.Style
                                        altItemStyle.CopyFrom(_InputStyle)
                                        CType(Me.Controls(0).Controls(0).Controls(0).Controls(0), System.Web.UI.WebControls.Table).MergeStyle(altItemStyle)

                                    Case TiposDeDatos.TipoCampoGS.DenArticulo
                                        'table-tablerow-tablecell-table-tablerow-tablecell-textbox
                                        oInputTextBox = Me.Controls(0).Controls(0).Controls(0).Controls(0).Controls(0).Controls(0).Controls(0)
                                        oInputTextBox.Attributes("onkeydown") = "fslimitar_den_art($('#" & Me.ClientID & "__tden'),$('#" & Me.ClientID & "__tden_t').val(),event)"
                                        oInputTextBox.Attributes("onkeyup") = "ddArtsPreInitDropDown('" + _DependentField + "###" + _IdContenedor + "###" + Me.ClientID + "')"
                                        If Not _bPortal Then oInputTextBox.Attributes("onkeyup") &= ";SetContextKey($('#" & Me.ClientID & "__tden'))"
                                        If Not _bPortal Then oInputTextBox.Attributes("onblur") = "ResetContextKey()"
                                        oInputTextBox.Attributes("onchange") = "DenArticulo_ValueChange($('#" & Me.ClientID & "__tden'),$('#" & Me.ClientID & "__tden_t').val())"

                                        'table-tablerow-tablecell-table-tablerow-tablecell-button                                        
                                        Dim oBut As System.Web.UI.HtmlControls.HtmlInputButton = Me.Controls(0).Controls(0).Controls(0).Controls(0).Controls(0).Controls(1).Controls(0)
                                        oBut.Attributes("onclick") = "ddArtsPreInitDropDown('" + _DependentField + "###" + _IdContenedor + "###" + Me.ClientID + "');ddArtsopenDropDownEvent($('#" & Me.ClientID & "__tden'),$('#" & Me.ClientID & "__tden_t').val(),event)"

                                        If PM Then
                                            'table-tablerow-tablecell-table-tablerow-tablecell-WebTextEdit
                                            Dim oInput2 As Infragistics.WebUI.WebDataInput.WebTextEdit
                                            oInput2 = CType(Controls(0).Controls(0).Controls(1).Controls(0).Controls(0).Controls(0).Controls(0), Infragistics.WebUI.WebDataInput.WebTextEdit)
                                            If oInputTextBox.Text <> "" Then oInput2.ClientSideEvents.CustomButtonPress = "showDetalleArticulo"
                                        End If

                                        altItemStyle = New System.Web.UI.WebControls.Style
                                        altItemStyle.CopyFrom(_InputStyle)
                                        CType(Me.Controls(0).Controls(0).Controls(0).Controls(0), System.Web.UI.WebControls.Table).MergeStyle(altItemStyle)
                                End Select
                            End If
                            If oInput IsNot Nothing Then
                                oInput.ButtonsAppearance.ButtonStyle.CssClass = "fsstyentrydd"

                                oInput.ClientSideEvents.KeyDown = "fsVaciaCombo"
                                oInput.ClientSideEvents.CustomButtonPress = "ddopenDropDownEvent"

                                Select Case _tipoGS
                                    Case TiposDeDatos.TipoCampoGS.CodArticulo
                                        oInput.ClientSideEvents.KeyDown = Nothing
                                        If _readOnly Then
                                            oInput = Me.Controls(0).Controls(0).Controls(0).Controls(1)
                                        Else
                                            oInput = Me.Controls(0).Controls(0).Controls(0).Controls(0)
                                            oInput.ClientSideEvents.CustomButtonPress = "ddArtsPreInitDropDown('" + _DependentField + "###" + _IdContenedor + "###" + Me.ClientID + "');ddArtsopenDropDownEvent"
                                            oInput.ClientSideEvents.ValueChange = "ddArtsCodUpdated"

                                            altItemStyle = New System.Web.UI.WebControls.Style
                                            altItemStyle.CopyFrom(_InputStyle)
                                            oInput.MergeStyle(altItemStyle)
                                            oInput.ButtonsAppearance.ButtonStyle.CssClass = "fsstyentrydd"
                                            oInput = Me.Controls(0).Controls(0).Controls(1).Controls(0)
                                        End If

                                        altItemStyle = New System.Web.UI.WebControls.Style
                                        altItemStyle.CopyFrom(_InputStyle)
                                        oInput.MergeStyle(altItemStyle)

                                        oInput.ButtonsAppearance.ButtonStyle.CssClass = "fsstyentrydd"

                                        Dim oEditor As Infragistics.WebUI.WebDataInput.WebTextEdit
                                        If _readOnly Then
                                            oEditor = Me.Controls(0).Controls(0).Controls(0).Controls(1)
                                        Else
                                            oEditor = Me.Controls(0).Controls(0).Controls(1).Controls(0)
                                        End If
                                        oEditor.ClientSideEvents.CustomButtonPress = "ddArtsPreInitDropDown('" + _DependentField + "###" + _IdContenedor + "###" + Me.ClientID + "');ddArtsopenDropDownEvent"
                                        oEditor.ClientSideEvents.TextChanged = "ddArtsTextChanged"
                                        oEditor.ButtonsAppearance.ButtonStyle.CssClass = "fsstyentrydd"
                                End Select

                                If Not (_InputStyle Is Nothing) Then
                                    altItemStyle = New System.Web.UI.WebControls.Style
                                    altItemStyle.CopyFrom(_InputStyle)
                                    oInput.MergeStyle(altItemStyle)
                                    If bEsLista Then
                                        oInput.ButtonsAppearance.ButtonStyle.CssClass = "fsstyentrylista"
                                    Else
                                        oInput.ButtonsAppearance.ButtonStyle.CssClass = "fsstyentrydd"
                                    End If
                                End If
                            End If
                        End If
                    Else
                        'Atributos de lista externa
                        Dim FSWSServer As Object = If(Me.Portal, HttpContext.Current.Session("FS_Portal_Server"), HttpContext.Current.Session("FSN_Server"))
                        Dim oDict As Object = FSWSServer.Get_Dictionary()
                        oDict.LoadData(TiposDeDatos.ModulosIdiomas.BuscadorAtribsListaExterna, _Idi)
                        Dim oTextos As DataTable = oDict.Data.Tables(0)

                        oInputTextBox = Me.FindControl("_t_t")
                        If Not _bPortal Then oInputTextBox.Attributes("onkeyup") = "SetAtribListaExternaContextKey($('#" & Me.ClientID & "__t'))"
                        If Not _bPortal Then oInputTextBox.Attributes("onblur") = "ComprobarAtribListaExterna($('#" & Me.ClientID & "__t'),'" & oTextos.Rows(6).Item(1) & "');ResetAtribListaExternaContextKey()"

                        altItemStyle = New System.Web.UI.WebControls.Style
                        altItemStyle.CopyFrom(_InputStyle)
                        CType(Me.FindControl("_t"), System.Web.UI.WebControls.Table).MergeStyle(altItemStyle)

                        If Not Me.ReadOnly Then
                            Dim oImgEditor As System.Web.UI.WebControls.HyperLink = Me.FindControl("_t_img")
                            oImgEditor.Attributes("onclick") = "MostrarBuscadorAtributosListaExterna($('#" & Me.ClientID & "__t'),$('#" & Me.ClientID & "__t_t').text(),'" & Me.Title & "','" & oTextos.Rows(0).Item(1) & "','" & oTextos.Rows(1).Item(1) & "','" &
                                                            oTextos.Rows(2).Item(1) & "','" & oTextos.Rows(3).Item(1) & "','" & oTextos.Rows(4).Item(1) & "','" & oTextos.Rows(5).Item(1) & "')"
                        End If
                    End If
                End If
                If Not oInput Is Nothing Then
                    oInput.ClientSideEvents.Spin = "CerrarDesplegables"
                    oInput.ClientSideEvents.Focus = "CerrarDesplegables"
                    oInput.ClientSideEvents.MouseDown = "CerrarDesplegables"
                End If
        End Select

        If Not oInput Is Nothing Then
            If Not _readOnly AndAlso _bSaltaCambioRechazo = True Then
                oInput.ClientSideEvents.ValueChange = "fCambioNc"
            End If
        End If

    End Sub
    Protected Overrides Sub Render(ByVal writer As HtmlTextWriter)
        PrepareControlHierarchy()

        RenderContents(writer)
    End Sub
    Protected Overrides Sub OnPreRender(ByVal e As System.EventArgs)
        MyBase.OnPreRender(e)
        RegisterValidatorCommonScript()
    End Sub
    ''' Revisado por: blp. Fecha: 04/10/2011
    ''' <summary>
    ''' M�todo que registra los scripts de cliente que usa la p�gina
    ''' </summary>
    ''' <remarks>Llamada desde OnPreRender(). M�x. 0,3 seg</remarks>
    Protected Sub RegisterValidatorCommonScript()
        Me.Page.ClientScript.RegisterClientScriptResource(Me.GetType(), "Fullstep.DataEntry.formatos.js")
        Me.Page.ClientScript.RegisterClientScriptResource(Me.GetType(), "Fullstep.DataEntry.popupDescrLarga.js")
        Me.Page.ClientScript.RegisterClientScriptResource(Me.GetType(), "Fullstep.DataEntry.popupTextoMedio.js")
        Me.Page.ClientScript.RegisterClientScriptResource(Me.GetType(), "Fullstep.DataEntry.dateCalendar.js")
        Me.Page.ClientScript.RegisterClientScriptResource(Me.GetType(), "Fullstep.DataEntry.dropdown.js")
        Me.Page.ClientScript.RegisterClientScriptResource(Me.GetType(), "Fullstep.DataEntry.fsGeneralEntry.js")

        If Not Page.ClientScript.IsStartupScriptRegistered(Page.GetType(), GeneralEntryKeyCall + Me.ClientID) Then _
            Page.ClientScript.RegisterStartupScript(Page.GetType(), GeneralEntryKeyCall + Me.ClientID, String.Format(IncludeScriptKeyFormat, "javascript", Me.InitScript))

        If Not Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), CamposCalculadosScriptKey) Then
            Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), CamposCalculadosScriptKey, String.Format(IncludeScriptKeyFormat, "javascript", "var arrCalculados = new Array()"))

        End If
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), ArticulosScriptKey) Then
            Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), ArticulosScriptKey, String.Format(IncludeScriptKeyFormat, "javascript", "var arrArticulos = new Array()"))

        End If
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), CamposObligatoriosScriptKey) Then
            Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), CamposObligatoriosScriptKey, String.Format(IncludeScriptKeyFormat, "javascript", "var arrObligatorios = new Array()"))
        End If

        If Not Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), CamposObligatoriosDenScriptKey) Then
            Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), CamposObligatoriosDenScriptKey, String.Format(IncludeScriptKeyFormat, "javascript", "var arrObligatoriosDen = new Array()"))
        End If

        If Not Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), CamposScriptKey) Then
            Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), CamposScriptKey, String.Format(IncludeScriptKeyFormat, "javascript", "var arrCampos = new Array()"))

        End If

        If Not Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), CamposScriptKey + "Desglose") Then
            Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), CamposScriptKey + "Desglose", String.Format(IncludeScriptKeyFormat, "javascript", "var arrCamposDesglose = new Array()"))

        End If

        If _Desglose Then
            'Anteriormente s�lo se registraban en el array arrCampos los campos del fsentry
            'actualmente, por el modo en que carga la aplicaci�n, tb se cargan los fsdsentry
            'Vamos a impedir que se carguen los fsdsentry haciendo que s�lo coja los fsentry
            If Me.ClientID.IndexOf("fsentry") > -1 Then
                If Not Page.ClientScript.IsStartupScriptRegistered(Page.GetType(), CamposScriptKey + Me.ClientID) Then _
                    Page.ClientScript.RegisterStartupScript(Page.GetType(), CamposScriptKey + Me.ClientID, String.Format(IncludeScriptKeyFormat, "javascript", "arrCampos[arrCampos.length]='" + Me.ClientID + "'"))
            End If
        End If
        If _tipo = TiposDeDatos.TipoGeneral.TipoDesglose Then
            If Not Page.ClientScript.IsStartupScriptRegistered(Page.GetType(), CamposScriptKey + "Desglose") Then _
                Page.ClientScript.RegisterStartupScript(Page.GetType(), CamposScriptKey + "Desglose" + Me.ClientID, String.Format(IncludeScriptKeyFormat, "javascript", "arrCamposDesglose[arrCamposDesglose.length]='" + Me.ClientID + "'"))
        End If

        If Not Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), "Pres1") Then
            Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "Pres1", String.Format(IncludeScriptKeyFormat, "javascript", "var arrPres1 = new Array()"))
        End If

        If Not Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), "Pres2") Then
            Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "Pres2", String.Format(IncludeScriptKeyFormat, "javascript", "var arrPres2 = new Array()"))
        End If

        If Not Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), "Pres3") Then
            Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "Pres3", String.Format(IncludeScriptKeyFormat, "javascript", "var arrPres3 = new Array()"))
        End If

        If Not Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), "Pres4") Then
            Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "Pres4", String.Format(IncludeScriptKeyFormat, "javascript", "var arrPres4 = new Array()"))
        End If

    End Sub
#End Region
#Region " Implementaci�n de IProvidesEmbeddableEditor "
    Public Function CanEditType(ByVal type As System.Type) As Boolean Implements Infragistics.WebUI.Shared.IProvidesEmbeddableEditor.CanEditType
        Return True
    End Function
    Public Function CanRenderType(ByVal type As System.Type) As Boolean Implements Infragistics.WebUI.Shared.IProvidesEmbeddableEditor.CanRenderType
        Return True
    End Function
    Public Function RenderValue(ByVal value As Object) As String Implements Infragistics.WebUI.Shared.IProvidesEmbeddableEditor.RenderValue
        If DBNullToSomething(value) Is Nothing Then
            Return Nothing
        Else
            Return value
        End If

    End Function
    Public ReadOnly Property EditorClientID() As String Implements Infragistics.WebUI.Shared.IProvidesEmbeddableEditor.EditorClientID
        Get
            If Me.Controls.Count > 0 Then
                Return (Me.Controls(0).ClientID)
            Else
                Return Me.ClientID
            End If
        End Get
    End Property
    Public ReadOnly Property EditorID() As String Implements Infragistics.WebUI.Shared.IProvidesEmbeddableEditor.EditorID
        Get
            If Me.Controls.Count > 0 Then
                Return Me.Controls(0).Controls(0).Controls(0).Controls(0).ID
            Else
                Return Me.ID
            End If
        End Get
    End Property
#End Region
    Private Class GridWebDrownTemplate
        Implements ITemplate

        Private _TipoGs As Integer
        Private _WidthTable As String

        ''' Revisado por: blp. Fecha: 04/10/2011
        ''' <summary>
        ''' Crea el interfaz para el ItemTemplate
        ''' </summary>
        ''' <param name="TipoGS">tipo de campo</param>
        ''' <remarks>Llamada desde:CtrlTipoLista; Tiempo maximo:0 </remarks>
        Public Sub New(ByVal TipoGS As Integer)
            MyBase.New()

            _TipoGs = TipoGS
            _WidthTable = "99%"
        End Sub
        ''' Revisado por: blp. Fecha: 04/10/2011
        ''' <summary>
        ''' Rellena un Item del dropdown 
        ''' </summary>
        ''' <param name="container">Item del dropdown a rellenar</param>
        ''' <remarks>Llamada desde:sistema; Tiempo maximo:0 </remarks>
        Public Sub InstantiateIn(ByVal container As System.Web.UI.Control) Implements System.Web.UI.ITemplate.InstantiateIn
            Select Case _TipoGs
                Case TiposDeDatos.TipoCampoGS.Dest
                    InstantiateInDestino(container)
                Case TiposDeDatos.TipoCampoGS.TipoPedido
                    InstantiateInTipoPedido(container)
            End Select
        End Sub
        ''' Revisado por: blp. Fecha: 04/10/2011
        ''' <summary>
        ''' Se produce al enlazar la etiqueta de la plantilla en que se muestra el codigo con el origen de datos.
        ''' </summary>
        ''' <param name="sender">Origen del evento</param>
        ''' <param name="e">la informaci�n del evento</param>
        ''' <remarks>Llamada desde:sistema; Tiempo maximo:0 </remarks>
        Private Sub Codigo_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim lc As Literal = CType(sender, Literal)
            Dim container As Infragistics.Web.UI.ListControls.DropDownItem = CType(lc.NamingContainer, Object).Item
            lc.Text = DBNullToEspacio(DataBinder.Eval(container.DataItem, "COD"))
        End Sub
        ''' Revisado por: blp. Fecha: 04/10/2011
        ''' <summary>
        ''' Se produce al enlazar la etiqueta de la plantilla en que se muestra la denominaci�n con el origen de datos.
        ''' </summary>
        ''' <param name="sender">Origen del evento</param>
        ''' <param name="e">la informaci�n del evento</param>
        ''' <remarks>Llamada desde:sistema; Tiempo maximo:0 </remarks>
        Private Sub Denom_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim lc As Literal = CType(sender, Literal)
            Dim container As Infragistics.Web.UI.ListControls.DropDownItem = CType(lc.NamingContainer, Object).Item
            lc.Text = DBNullToEspacio(DataBinder.Eval(container.DataItem, "DEN"))
        End Sub
        ''' Revisado por: blp. Fecha: 04/10/2011
        ''' <summary>
        ''' Se produce al enlazar la etiqueta de la plantilla en que se muestra la Direccion con el origen de datos.
        ''' </summary>
        ''' <param name="sender">Origen del evento</param>
        ''' <param name="e">la informaci�n del evento</param>
        ''' <remarks>Llamada desde:sistema; Tiempo maximo:0 </remarks>
        Private Sub Direccion_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim lc As Literal = CType(sender, Literal)
            Dim container As Infragistics.Web.UI.ListControls.DropDownItem = CType(lc.NamingContainer, Object).Item
            lc.Text = DBNullToStr(DataBinder.Eval(container.DataItem, "DIR"))
        End Sub
        ''' Revisado por: blp. Fecha: 04/10/2011
        ''' <summary>
        ''' Se produce al enlazar la etiqueta de la plantilla en que se muestra la Poblacion con el origen de datos.
        ''' </summary>
        ''' <param name="sender">Origen del evento</param>
        ''' <param name="e">la informaci�n del evento</param>
        ''' <remarks>Llamada desde:sistema; Tiempo maximo:0 </remarks>
        Private Sub Poblacion_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim lc As Literal = CType(sender, Literal)
            Dim container As Infragistics.Web.UI.ListControls.DropDownItem = CType(lc.NamingContainer, Object).Item
            lc.Text = DBNullToEspacio(DataBinder.Eval(container.DataItem, "POB"))
        End Sub
        ''' Revisado por: blp. Fecha: 04/10/2011
        ''' <summary>
        ''' Se produce al enlazar la etiqueta de la plantilla en que se muestra el Codigo Postal con el origen de datos.
        ''' </summary>
        ''' <param name="sender">Origen del evento</param>
        ''' <param name="e">la informaci�n del evento</param>
        ''' <remarks>Llamada desde:sistema; Tiempo maximo:0 </remarks>
        Private Sub CodPostal_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim lc As Literal = CType(sender, Literal)
            Dim container As Infragistics.Web.UI.ListControls.DropDownItem = CType(lc.NamingContainer, Object).Item
            lc.Text = DBNullToEspacio(DataBinder.Eval(container.DataItem, "CP"))
        End Sub
        ''' Revisado por: blp. Fecha: 04/10/2011
        ''' <summary>
        ''' Se produce al enlazar la etiqueta de la plantilla en que se muestra el Pais con el origen de datos.
        ''' </summary>
        ''' <param name="sender">Origen del evento</param>
        ''' <param name="e">la informaci�n del evento</param>
        ''' <remarks>Llamada desde:sistema; Tiempo maximo:0 </remarks>
        Private Sub Pais_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim lc As Literal = CType(sender, Literal)
            Dim container As Infragistics.Web.UI.ListControls.DropDownItem = CType(lc.NamingContainer, Object).Item
            lc.Text = DBNullToEspacio(DataBinder.Eval(container.DataItem, "PAIDEN"))
        End Sub
        ''' Revisado por: blp. Fecha: 04/10/2011
        ''' <summary>
        ''' Se produce al enlazar la etiqueta de la plantilla en que se muestra la Provincia con el origen de datos.
        ''' </summary>
        ''' <param name="sender">Origen del evento</param>
        ''' <param name="e">la informaci�n del evento</param>
        ''' <remarks>Llamada desde:sistema; Tiempo maximo:0 </remarks>
        Private Sub Provincia_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim lc As Literal = CType(sender, Literal)
            Dim container As Infragistics.Web.UI.ListControls.DropDownItem = CType(lc.NamingContainer, Object).Item
            lc.Text = DBNullToEspacio(DataBinder.Eval(container.DataItem, "PROVIDEN"))
        End Sub
        ''' Revisado por: blp. Fecha: 04/10/2011
        ''' <summary>
        ''' Se produce al enlazar la etiqueta de la plantilla en que se muestra la moneda con el origen de datos.
        ''' </summary>
        ''' <param name="sender">Origen del evento</param>
        ''' <param name="e">la informaci�n del evento</param>
        ''' <remarks>Llamada desde:sistema; Tiempo maximo:0 </remarks>
        Private Sub Id_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim lc As Literal = CType(sender, Literal)
            Dim container As Infragistics.Web.UI.ListControls.DropDownItem = CType(lc.NamingContainer, Object).Item
            lc.Text = DBNullToEspacio(DataBinder.Eval(container.DataItem, "ID"))
        End Sub
        ''' Revisado por: blp. Fecha: 04/10/2011
        ''' <summary>
        ''' Se produce al enlazar la etiqueta de la plantilla en que se muestra la moneda con el origen de datos.
        ''' </summary>
        ''' <param name="sender">Origen del evento</param>
        ''' <param name="e">la informaci�n del evento</param>
        ''' <remarks>Llamada desde:sistema; Tiempo maximo:0 </remarks>
        Private Sub CodDen_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim lc As Literal = CType(sender, Literal)
            Dim container As Infragistics.Web.UI.ListControls.DropDownItem = CType(lc.NamingContainer, Object).Item
            lc.Text = DBNullToEspacio(DataBinder.Eval(container.DataItem, "COD_DEN"))
        End Sub
        ''' Revisado por: blp. Fecha: 04/10/2011
        ''' <summary>
        ''' Se produce al enlazar la etiqueta de la plantilla en que se muestra la moneda con el origen de datos.
        ''' </summary>
        ''' <param name="sender">Origen del evento</param>
        ''' <param name="e">la informaci�n del evento</param>
        ''' <remarks>Llamada desde:sistema; Tiempo maximo:0 </remarks>
        Private Sub Concepto_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim lc As Literal = CType(sender, Literal)
            Dim container As Infragistics.Web.UI.ListControls.DropDownItem = CType(lc.NamingContainer, Object).Item
            lc.Text = DBNullToEspacio(DataBinder.Eval(container.DataItem, "CONCEPTO"))
        End Sub
        ''' Revisado por: blp. Fecha: 04/10/2011
        ''' <summary>
        ''' Se produce al enlazar la etiqueta de la plantilla en que se muestra la moneda con el origen de datos.
        ''' </summary>
        ''' <param name="sender">Origen del evento</param>
        ''' <param name="e">la informaci�n del evento</param>
        ''' <remarks>Llamada desde:sistema; Tiempo maximo:0 </remarks>
        Private Sub Recepcionar_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim lc As Literal = CType(sender, Literal)
            Dim container As Infragistics.Web.UI.ListControls.DropDownItem = CType(lc.NamingContainer, Object).Item
            lc.Text = DBNullToEspacio(DataBinder.Eval(container.DataItem, "RECEPCIONAR"))
        End Sub
        ''' Revisado por: blp. Fecha: 04/10/2011
        ''' <summary>
        ''' Se produce al enlazar la etiqueta de la plantilla en que se muestra la moneda con el origen de datos.
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e">la informaci�n del evento</param>
        ''' <remarks>Llamada desde:sistema; Tiempo maximo:0 </remarks>
        Private Sub Almacenar_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim lc As Literal = CType(sender, Literal)
            Dim container As Infragistics.Web.UI.ListControls.DropDownItem = CType(lc.NamingContainer, Object).Item
            lc.Text = DBNullToEspacio(DataBinder.Eval(container.DataItem, "ALMACENAR"))
        End Sub
        ''' Revisado por: blp. Fecha: 04/10/2011
        ''' <summary>
        ''' Se produce al enlazar la etiqueta de la plantilla en que se muestra la moneda con el origen de datos.
        ''' </summary>
        ''' <param name="sender">Origen del evento</param>
        ''' <param name="e">la informaci�n del evento</param>
        ''' <remarks>Llamada desde:sistema; Tiempo maximo:0 </remarks>
        Private Sub ConceptoDen_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim lc As Literal = CType(sender, Literal)
            Dim container As Infragistics.Web.UI.ListControls.DropDownItem = CType(lc.NamingContainer, Object).Item
            lc.Text = DBNullToEspacio(DataBinder.Eval(container.DataItem, "CONCEPTO_DEN"))
        End Sub
        ''' Revisado por: blp. Fecha: 04/10/2011
        ''' <summary>
        ''' Se produce al enlazar la etiqueta de la plantilla en que se muestra la moneda con el origen de datos.
        ''' </summary>
        ''' <param name="sender">Origen del evento</param>
        ''' <param name="e">la informaci�n del evento</param>
        ''' <remarks>Llamada desde:sistema; Tiempo maximo:0 </remarks>
        Private Sub RecepcionarDen_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim lc As Literal = CType(sender, Literal)
            Dim container As Infragistics.Web.UI.ListControls.DropDownItem = CType(lc.NamingContainer, Object).Item
            lc.Text = DBNullToEspacio(DataBinder.Eval(container.DataItem, "RECEPCIONAR_DEN"))
        End Sub
        ''' Revisado por: blp. Fecha: 04/10/2011
        ''' <summary>
        ''' Se produce al enlazar la etiqueta de la plantilla en que se muestra la moneda con el origen de datos.
        ''' </summary>
        ''' <param name="sender">Origen del evento</param>
        ''' <param name="e">la informaci�n del evento</param>
        ''' <remarks>Llamada desde:sistema; Tiempo maximo:0 </remarks>
        Private Sub AlmacenarDen_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim lc As Literal = CType(sender, Literal)
            Dim container As Infragistics.Web.UI.ListControls.DropDownItem = CType(lc.NamingContainer, Object).Item
            lc.Text = DBNullToEspacio(DataBinder.Eval(container.DataItem, "ALMACENAR_DEN"))
        End Sub
        ''' Revisado por: blp. Fecha: 04/10/2011
        ''' <summary>
        ''' Para q no se meta en el html " " q sea el codigo html nbsp;
        ''' </summary>
        ''' <param name="Texto">texto a formatear</param>
        ''' <returns>texto formateado</returns>
        ''' <remarks>Llamada desde: los diferentes datbinding del esta clase ITemplate; Tiempo maximo:0 </remarks>
        Private Function DBNullToEspacio(ByVal Texto As Object) As String
            If DBNullToStr(Texto) = "" Then
                Return "&nbsp;"
            Else
                Return Texto
            End If
        End Function
        ''' Revisado por: blp. Fecha: 04/10/2011
        ''' <summary>
        ''' Rellena un Item del dropdown 
        ''' </summary>
        ''' <param name="container">Item del dropdown a rellenar</param>
        ''' <remarks>Llamada desde:InstantiateIn; Tiempo maximo:0 </remarks>
        Private Sub InstantiateInDestino(ByVal container As System.Web.UI.Control)
            Dim tabla As New HtmlTable()
            tabla.Width = _WidthTable '"630px"

            tabla.CellSpacing = 0
            tabla.CellPadding = 4
            '---------------
            Dim fila As New HtmlTableRow()
            fila.Attributes.Add("class", "igdd_FullstepPMWebListItem")
            fila.Height = "20px"
            '--------------
            Dim celda As New HtmlTableCell()
            celda.Width = "10%" '_WidthPeq
            celda.Align = "left"
            celda.VAlign = "bottom"
            Dim lc As New Literal()
            AddHandler lc.DataBinding, AddressOf Codigo_DataBinding
            celda.Controls.Add(lc)
            fila.Cells.Add(celda)
            '------------
            celda = New HtmlTableCell()
            celda.Width = "25%" '_WidthGrande
            celda.Align = "left"
            celda.VAlign = "bottom"
            lc = New Literal()
            AddHandler lc.DataBinding, AddressOf Denom_DataBinding
            celda.Controls.Add(lc)
            fila.Cells.Add(celda)
            '--------------
            celda = New HtmlTableCell()
            celda.Width = "25%" ' _WidthGrande
            celda.Align = "left"
            celda.VAlign = "bottom"
            lc = New Literal()
            AddHandler lc.DataBinding, AddressOf Direccion_DataBinding
            celda.Controls.Add(lc)
            fila.Cells.Add(celda)
            '--------------
            celda = New HtmlTableCell()
            celda.Width = "10%" '_WidthMediano
            celda.Align = "left"
            celda.VAlign = "bottom"
            lc = New Literal()
            AddHandler lc.DataBinding, AddressOf Poblacion_DataBinding
            celda.Controls.Add(lc)
            fila.Cells.Add(celda)
            '--------------
            celda = New HtmlTableCell()
            celda.Width = "10%" '_WidthPeqMediano
            celda.Align = "left"
            celda.VAlign = "bottom"
            lc = New Literal()
            AddHandler lc.DataBinding, AddressOf CodPostal_DataBinding
            celda.Controls.Add(lc)
            fila.Cells.Add(celda)
            '--------------
            celda = New HtmlTableCell()
            celda.Width = "10%" '_WidthMediano
            celda.Align = "left"
            celda.VAlign = "bottom"
            lc = New Literal()
            AddHandler lc.DataBinding, AddressOf Pais_DataBinding
            celda.Controls.Add(lc)
            fila.Cells.Add(celda)
            '--------------
            celda = New HtmlTableCell()
            celda.Width = "10%" '_WidthMediano
            celda.Align = "left"
            celda.VAlign = "bottom"
            lc = New Literal()
            AddHandler lc.DataBinding, AddressOf Provincia_DataBinding
            celda.Controls.Add(lc)
            fila.Cells.Add(celda)
            '--------------
            tabla.Rows.Add(fila)
            container.Controls.Add(tabla)
        End Sub
        ''' Revisado por: blp. Fecha: 04/10/2011
        ''' <summary>
        ''' Rellena un Item del dropdown 
        ''' </summary>
        ''' <param name="container">Item del dropdown a rellenar</param>
        ''' <remarks>Llamada desde:InstantiateIn; Tiempo maximo:0 </remarks>
        Private Sub InstantiateInTipoPedido(ByVal container As System.Web.UI.Control)
            Dim tabla As New HtmlTable()
            tabla.Width = _WidthTable

            tabla.CellSpacing = 0
            tabla.CellPadding = 0
            '---------------
            Dim fila As New HtmlTableRow()
            fila.Attributes.Add("class", "igdd_FullstepPMWebListItemTable")
            fila.Height = "20px"
            '--------------
            Dim celda As New HtmlTableCell()
            celda.Width = "0%"
            celda.Align = "left"
            celda.VAlign = "bottom"
            celda.Style("display") = "none"
            Dim lc As New Literal()
            AddHandler lc.DataBinding, AddressOf Id_DataBinding
            celda.Controls.Add(lc)
            fila.Cells.Add(celda)
            '------------
            celda = New HtmlTableCell()
            celda.Width = "0%"
            celda.Align = "left"
            celda.VAlign = "bottom"
            celda.Style("display") = "none"
            lc = New Literal()
            AddHandler lc.DataBinding, AddressOf CodDen_DataBinding
            celda.Controls.Add(lc)
            fila.Cells.Add(celda)
            '------------
            celda = New HtmlTableCell()
            celda.Width = "0%"
            celda.Align = "left"
            celda.VAlign = "bottom"
            celda.Style("display") = "none"
            lc = New Literal()
            AddHandler lc.DataBinding, AddressOf Concepto_DataBinding
            celda.Controls.Add(lc)
            fila.Cells.Add(celda)
            '------------
            celda = New HtmlTableCell()
            celda.Width = "0%"
            celda.Align = "left"
            celda.VAlign = "bottom"
            celda.Style("display") = "none"
            lc = New Literal()
            AddHandler lc.DataBinding, AddressOf Recepcionar_DataBinding
            celda.Controls.Add(lc)
            fila.Cells.Add(celda)
            '------------
            celda = New HtmlTableCell()
            celda.Width = "0%"
            celda.Align = "left"
            celda.VAlign = "bottom"
            celda.Style("display") = "none"
            lc = New Literal()
            AddHandler lc.DataBinding, AddressOf Almacenar_DataBinding
            celda.Controls.Add(lc)
            fila.Cells.Add(celda)
            '------------
            celda = New HtmlTableCell()
            celda.Width = "29%"
            celda.Align = "left"
            celda.VAlign = "bottom"
            celda.Attributes.Add("class", "igdd_FullstepPMWebBorders")
            lc = New Literal()
            AddHandler lc.DataBinding, AddressOf Codigo_DataBinding
            celda.Controls.Add(lc)
            fila.Cells.Add(celda)
            '--------------
            celda = New HtmlTableCell()
            celda.Width = "29%"
            celda.Align = "left"
            celda.VAlign = "bottom"
            celda.Attributes.Add("class", "igdd_FullstepPMWebBorders")
            lc = New Literal()
            AddHandler lc.DataBinding, AddressOf Denom_DataBinding
            celda.Controls.Add(lc)
            fila.Cells.Add(celda)
            '--------------
            celda = New HtmlTableCell()
            celda.Width = "14%"
            celda.Align = "left"
            celda.VAlign = "bottom"
            celda.Attributes.Add("class", "igdd_FullstepPMWebBorders")
            lc = New Literal()
            AddHandler lc.DataBinding, AddressOf ConceptoDen_DataBinding
            celda.Controls.Add(lc)
            fila.Cells.Add(celda)
            '--------------
            celda = New HtmlTableCell()
            celda.Width = "14%"
            celda.Align = "left"
            celda.VAlign = "bottom"
            celda.Attributes.Add("class", "igdd_FullstepPMWebBorders")
            lc = New Literal()
            AddHandler lc.DataBinding, AddressOf RecepcionarDen_DataBinding
            celda.Controls.Add(lc)
            fila.Cells.Add(celda)
            '--------------
            celda = New HtmlTableCell()
            celda.Width = "14%"
            celda.Align = "left"
            celda.VAlign = "bottom"
            celda.Attributes.Add("class", "igdd_FullstepPMWebBorders")
            lc = New Literal()
            AddHandler lc.DataBinding, AddressOf AlmacenarDen_DataBinding
            celda.Controls.Add(lc)
            fila.Cells.Add(celda)
            '--------------
            tabla.Rows.Add(fila)
            container.Controls.Add(tabla)
        End Sub
    End Class
End Class

