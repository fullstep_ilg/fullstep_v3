﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices
Imports System.Web.UI

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("DataEntry")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyProduct("DataEntry")> 
<Assembly: CLSCompliant(True)> 

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("900A8D00-CA2C-4B79-9BD8-4D46BE4531DF")> 

<Assembly: WebResource("Fullstep.DataEntry.Buscador.png", "image/png")> 
<Assembly: WebResource("Fullstep.DataEntry.trespuntos.gif", "image/gif")> 
<Assembly: WebResource("Fullstep.DataEntry.calendar-color.png", "image/png")> 
<Assembly: WebResource("Fullstep.DataEntry.edit2.gif", "image/gif")> 
<Assembly: WebResource("Fullstep.DataEntry.factura.png", "image/png")> 
<Assembly: WebResource("Fullstep.DataEntry.ig_cmboDownXP1.bmp", "image/bmp")> 
<Assembly: WebResource("Fullstep.DataEntry.favoritos.gif", "image/gif")> 
<Assembly: WebResource("Fullstep.DataEntry.info.gif", "image/gif")> 

<Assembly: WebResource("Fullstep.DataEntry.formatos.js", "application/x-javascript")> 
<Assembly: WebResource("Fullstep.DataEntry.popupDescrLarga.js", "application/x-javascript")> 
<Assembly: WebResource("Fullstep.DataEntry.popupTextoMedio.js", "application/x-javascript")> 
<Assembly: WebResource("Fullstep.DataEntry.dateCalendar.js", "application/x-javascript")> 
<Assembly: WebResource("Fullstep.DataEntry.dropdown.js", "application/x-javascript")> 
<Assembly: WebResource("Fullstep.DataEntry.fsGeneralEntry.js", "application/x-javascript")> 

<Assembly: TagPrefix("Fullstep.DataEntry", "fsde")> 
