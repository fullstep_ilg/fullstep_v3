function redondea(num,dec)
{
var nTmp = num

var sNum=nTmp.toString()
var rd=/\./
var pDec = sNum.search(rd)

if (pDec==-1)
	return(num)
sDec = sNum.substr(pDec + 1, sNum.length)

dec = (dec<sDec.length)?dec:sDec.length


for (var i=0;i<dec;i++)
	{
	nTmp = nTmp * 10
	}

nTmp = Math.round(nTmp)


for (var i=0;i<dec;i++)
	nTmp = nTmp / 10
	
return(nTmp)

}

//convierte un numero a una cadena aplicandole el formato correspondiente
function num2str(num,vdecimalfmt,vthousanfmt,vprecisionfmt)
{
var j
if (num==0)
	{
	return ("0")
	}
if (num == null)
	{
	return ("")
	}
	
var sNum
var sDec
var sInt
var bNegativo
var i


bNegativo=false
var vNum = redondea(num,vprecisionfmt)
sNum=vNum.toString()
if (sNum.charAt(0)=="-")
	{
	bNegativo=true
	sNum=sNum.substr(1,sNum.length)
	}
rd=/\./
pDec = sNum.search(rd)

if (pDec==-1)
	{
	sInt = sNum
	sDec = ""
	}
else
	{
	sInt = sNum.substr(0, pDec)
	sDec= sNum.substr(pDec+1,20)
	if (sDec.substr(sDec.length-4,4)=="999" + sDec.substr(sDec.length-1,1))
		{
		var lDigito = 10 - parseFloat(sDec.substr(sDec.length-1,1))
		var sSum = "0."
		for (j=1;j<sDec.length;j++)
			sSum += "0"
		sSum += lDigito.toString()
		
		
		sNum = (parseFloat(sInt + "." + sDec) + parseFloat(sSum)).toString()
		pDec = sNum.search(rd)

		if (pDec==-1)
			{
			sInt = sNum
			sDec = ""
			}
		else
			{
			sInt = sNum.substr(0, pDec)
			sDec = sNum.substr(pDec+1,vprecisionfmt)
			}
		}
	}

if (sInt.length>3)
	{
	newValor=""
	s=0
	//los posicionamos correctamente
	for (i=sInt.length-1;i>=0;i--)
		{
		if (s==3)
			{
			newValor=vthousanfmt + newValor
			s=0
			}
		newValor=sInt.charAt(i) + newValor				
		s++
		}
	sInt=newValor
	}

var redondeo = sDec

sDec=sDec.substr(0,vprecisionfmt>=12?10:vprecisionfmt)

while(sDec.charAt(sDec.length-1)=="0")
	sDec=sDec.substr(0,sDec.length-1)

if (sDec!="" && sDec!="000000000000".substr(0,vprecisionfmt))
	sNum= sInt + vdecimalfmt + sDec
else
	sNum = sInt
if (bNegativo==true)
	sNum = "-" + sNum

var vprec = vprecisionfmt
while (sNum=="0" && num!=0)
	{
	vprec++
	sNum = num2str(num,vdecimalfmt,vthousanfmt,vprec)
	}
	

return (sNum)
}

//convierte una cadena en un n�mero
function str2num(str,vdecimalfmt,vthousanfmt)
{
if (str=="")
	{
	return (null)
	}
if (vthousanfmt=="") 
	{
	rm = /\X/i
	}
else
	{
	rm= eval("/\\" + vthousanfmt + "/i")
	}

while (str.search(rm)!=-1)
	{
	str=str.replace(rm,"")
	}

rd=eval("/\\" + vdecimalfmt + "/")

str=str.replace(rd,".")

num = parseFloat(str)

return (num)

}


//Esta funcion convierte un objeto javascript Date en una cadena con una fecha
//en el formato especificado en vdatefmt
function date2str(fec,vdatefmt,bConHora)
{
var vDia
var vMes
var str

if (fec==null)
	{
	return ("")
	}
re=/dd/i
vDia = "00".substr(0,2-fec.getDate().toString().length) + fec.getDate().toString()
str = vdatefmt.replace(re,vDia)

re=/mm/i

vMes = "00".substr(0,2-(fec.getMonth() + 1).toString().length) + (fec.getMonth() + 1).toString()
str = str.replace(re,vMes)

re=/yyyy/i

str = str.replace(re,fec.getFullYear())
if (bConHora)
	str += (" " + "00".substr(0,2-(fec.getHours() + 1).toString().length) + fec.getHours() + ":" + "00".substr(0,2-(fec.getMinutes() + 1).toString().length) + fec.getMinutes())
return(str)
}

function trim(sValor)
{
var re = / /

var i

var sTmp = sValor

while (sTmp.search(re)==0)
	sTmp = sTmp.replace(re,"")

var sRev = ""
for (i=0;i<sTmp.length;i++)
	sRev=sTmp.charAt(i) + sRev


while (sRev.search(re)==0)
	sRev = sRev.replace(re,"")
	
sTmp=""
for (i=0;i<sRev.length;i++)
	sTmp=sRev.charAt(i) + sTmp

return(sTmp)
}

/*''' <summary>
''' Los query string pueden tener un problema con ' , con " , con chr(10) y con chr(13). Los reemplazados
''' por sus codigos unicode en formato decimal.
''' </summary>
''' <param name="Entrada">Texto donde reemplazar</param>
''' <returns>Texto reemplazado</returns>
''' <remarks>Llamada desde: noconformidad.aspx/uwgNoConform_CellClickHandler; Tiempo m�ximo:0</remarks>*/
function JSText(Entrada) {
    Entrada = replaceAll(Entrada, "'", "/X27;")//%39
    Entrada = replaceAll(Entrada, '"', "/X22;")//%34

    return Entrada
}

function replaceAll(sOriginal, sBuscar, sSustituir)
{
if (sOriginal==null)
	return null

var re = eval("/" + sBuscar + "/")
var sTmp = ""

while (sOriginal.search(re)>=0)
	{
	sTmp += sOriginal.substr(0,sOriginal.search(re)) + sSustituir
	sOriginal = sOriginal.substr(sOriginal.search(re) + sBuscar.length,sOriginal.length)
	}
sTmp += sOriginal
return (sTmp)

}

function JS2HTML(s)
{

var sTmp = s
var re = /\<input/
if (sTmp.search(re)!=-1)
	return (s)
var re = /\<textarea/
if (sTmp.search(re)!=-1)
	return (s)
var re = /\<INPUT/
if (sTmp.search(re)!=-1)
	return (s)
var re = /\<TEXTAREA/
if (sTmp.search(re)!=-1)
	return (s)


var re = /\n/
var i = 0 
while (sTmp.search(re)!=-1)
	{
	sTmp = sTmp.replace(re,"<br>")
	}
return(sTmp)
}

function Var2Param(v)
{
var re = /%/
var sTmp = v


while (sTmp.search(re)!=-1)
	{
	
	sTmp = sTmp.replace(re,"###")
	}
var re = /###/


while (sTmp.search(re)!=-1)
	{
	sTmp = sTmp.replace(re,"%25")
	}

sTmp = replaceAll(sTmp,"#","%23")
return(sTmp)
}