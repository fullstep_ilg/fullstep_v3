var arrInputs = new Array();
var arrAcciones = new Array();
var htControlFilas = new Array();
var htControlFilasVinc = new Array();
var htControlArrVinc = new Array();
var htControlArrVincMovible = new Array();

function fsGeneralEntry_getById(id) {
    var o, e
    var sRoot = id.replace("__t", "")
    sRoot = sRoot.replace("__hAct", "")
    e = document.getElementById(sRoot + "__tbl")
    if (!e)
        return null
    o = e.Object
    return o;

}
/* Revisado por: blp. Fecha: 04/10/2011
''' <summary>
''' Crea el objeto entry propiamente dicho
''' </summary>
''' <param name="name">uniqueid del entry</param>
''' <param name="id">id del entry</param>
''' <param name="tipo">tipo del entry (textocorto, fecha, etc)</param>
''' <param name="tipoGS">tipoGS del entry (copia_campo_def.tipo_campo_gs)</param>
''' <param name="valor">valor q almacena el entry</param>
''' <param name="isGridEditor">si es el entry en campos.ascx q es el campo padre del desglose</param>
''' <param name="idCampo">id en bbdd del entry (copia_campo.id)</param>
''' <param name="nullMsg">mensaje de error si no acepta nulos</param>
''' <param name="readOnly">solo lectura/escritura del entry</param>
''' <param name="idGrid">id del entry desglose relacionado</param>
''' <param name="title">titulo del entry</param>
''' <param name="errMsg">mensaje de error si supera el maxlength</param>
''' <param name="maxLength">maxlength del entry</param>
''' <param name="sRestric">si es campo relacionado con otro aqui van las restricciones</param>
''' <param name="obligatorio">obligatoriedad del entry</param>
''' <param name="basedesglose">si es un entry en desglose.ascx q es campo hijo de desglose,es decir, una columna de desglose</param>
''' <param name="dependentfield">id del entry dependiente de este (ejemplo pais dependentfield provincia)</param>
''' <param name="validationcontrol">id del entry para validar este</param>
''' <param name="independiente">si no depende de nadie</param>
''' <param name="idCampoPadre">id en bbdd del desglose del q depende (copia_campo.id)</param>
''' <param name="arrCamposHijos">lista de ids de los entrys hijos</param>
''' <param name="intro">intro del entry (combo, etc)</param>
''' <param name="articuloCodificado">el entry si es un articulo codificado o no</param>
''' <param name="orden">intro del entry dentro de campos o de desglose (form_campo.orden)</param>
''' <param name="grupo">id del grupo del entry (copia_grupo.id)</param>
''' <param name="idatrib">id de atributo del entry (def_atrib.id)</param>
''' <param name="articuloGenerico">el entry si es un articulo generico o no</param>
''' <param name="DenArticuloModificado">el entry es articulo, si le han tocado la denominaci�n</param>
''' <param name="anyadir_art">	check Permitir a�adir nuevos articulos</param>
''' <param name="valerp">	Para los datos q maneja la Mapper, tipo orden coste (Copia_Campo.VALIDACION_ERP/Copia_linea_desglose.VALIDACION_ERP)</param>
''' <param name="codigoArticulo">el entry es articulo o no</param>
''' <param name="x">posici�n x a mostrar el popup cuando das al link/boton del entry</param>
''' <param name="y">posici�n y a mostrar el popup cuando das al link/boton del entry</param>
''' <param name="idRefSol">	Comprobaci�n del bloqueo por importe de la solicitud del padre, id de la solicitud del padre</param>
''' <param name="avisoBloqueo">	Comprobaci�n del bloqueo por importe de la solicitud del padre, mensaje</param>
''' <param name="instanciaMoneda">	moneda en q esta la instancia. el entry sera articulo/denominaci�n art/ ref. solicitud</param>
''' <param name="cargarUltADJ">cuando se carge el precio sacarlo de la ultima adjudicaci�n o no</param>
''' <param name="idEntryPREC">id del entry relacionado precio. este sera entry articulo</param>
''' <param name="idEntryPROV">id del entry relacionado proveedor. este sera entry articulo</param>
''' <param name="idEntryUNI">id del entry relacionado unidad. este sera entry articulo</param>
''' <param name="idEntryCC">id del entry relacionado centro coste. este sera entry articulo</param>
''' <param name="idDataEntryDependent">id del entry relacionado para validaci�n (ejemplo entry articulo idDataEntryDependent2 organizaci�n compras)</param>
''' <param name="idDataEntryDependent2">id del entry relacionado para validaci�n (ejemplo entry articulo idDataEntryDependent2 centro de organizaci�n)</param>
''' <param name="PM">el entry es de un control en pantalla pm o qa/portal</param>
''' <param name="calculado">el entry es un campo calculado o no</param>
''' <param name="TablaExterna">el entry es una tabla externa o no</param>
''' <param name="campo_origen">el entry es un campo calculado este contiene el ID_CAMPO_ORIGEN_CALC_DESGLOSE</param>
''' <param name="idCampoPadre_origen">id en bbdd del desglose del q depende (copia_campo.id) se usa al crear favoritos desde detalle solicitud</param>
''' <param name="arrCamposHijos_origen">lista de ids de los entrys hijos se usa al crear favoritos desde detalle solicitud</param>
''' <param name="IdiomaBotonTextoMedio">idioma del popup q sale para textos medio</param>
''' <param name="PoseeDefecto">si el entry posee un valor por defecto o no</param>
''' <param name="DataValorDefecto">valor por defecto. Si archivo string con id de los archivos</param>
''' <param name="TextoValorDefecto">valor por defecto. Si archivo string con nombre de los archivos. Si texto TextoValorDefecto=DataValorDefecto</param>
''' <param name="MonRepercutido">	si es un importe repercutido, la moneda a grabar</param>
''' <param name="CambioRepercutido">si es un importe repercutido, el cambio a grabar</param>
''' <param name="MonCentral">si es un importe repercutido, moneda del sistema. </param>
''' <param name="CambioCentral">si es un importe repercutido, cambio del sistema.</param>
''' <param name="CtrlCambioRechazo">Si se modifica un dato de las acciones desde el portal, la linea si esta rechazada pasa a sin revisar . Aqui el ctrol para esto.</param>
''' <param name="NCSinRevisar">Texto multiidioma 'sin revisar' para lo de CtrlCambioRechazo</param>        
''' <param name="VerUON">Si se va a ver o no la estructura organizativa en el buscador de centros de coste</param>
''' <param name="PRES5">El c�digo de la partida presupuestaria</param>
''' <param name="Concepto">Caracter�stica del tipo de pedido y el art�culo</param>
''' <param name="Almacenamiento">Caracter�stica del tipo de pedido y el art�culo</param>
''' <param name="Recepcion">Caracter�stica del tipo de pedido y el art�culo</param>
'''<param name="RecordarValores">Indica si el valor que se introduzca en el campo se recordara para mostrarlo en un autocompletado</param>
''' <param name="idEntryCANT">id del entry relacionado cantidad. este sera entry articulo</param>
''' <param name="NumeroDeDecimales">N�mero de decimales a guardar en el dataentry de la cantidad</param>
''' <param name="UnidadOculta">Valor de la unidad</param>
''' <param name="idCampoHijaListaEnlazada">ID del campo hija relacionado al campo</param>
''' <param name="idCampoPadreListaEnlazada">ID del campo padre relacionado al campo</param>
''' <param name="idCampoListaEnlazada">ID del campo en el que se basa la carga de las opciones</param>
''' <param name="valoresCargados">Si ya se han cargado los valores de la lista</param>
''' <param name="Portal">En el Entry hay acceso a bbdd. Si se esta en Portal el acceso lo da PMPortalServer y en PM/Qa lo da PmServer</param>
''' <param name="CampoDef_CampoODesgHijo">Id de copia_campo_def de un campo no desglose o un campo desglose hijo</param>
''' <param name="CampoDef_DesgPadre">Id de copia_campo_def de un campo desglose padre</param>
''' <param name="arrCamposHijos_CampoDef">lista de ids de copia_campo_def de los entrys hijos</param>
''' <param name="CampoOrigenVinculado">Tarea Vinculaciones (1959) de un desglose con otro: Aqui se guarda el FORM_CAMPO.ID del campo origen vinculado al campo q es el entry.</param>
''' <param name="sDescrComboPopUp">Guarda el texto de un combo en un desglose popup. El valor del combo (ejemplo: valor->1 texto-> Hola Mundo) va en la propiedad Valor.</param>
''' <param name="esProveedorParticipante">Si es no un campo asociado al proveedor participante</param>
''' <param name="Contactos">Contactos del proveedor participante</param>
''' <param name="idDataEntryDependent3">id del entry relacionado para carga combo relacionado centro y almacen</param>
''' <param name="idMasterField">id del entry maestro, es decir, del que depende el control, si es que existe</param>
''' <param name="DenGrupo">Denominacion del grupo donde esta el campo</param>
''' <param name="DenDesgloseMat">Denominacion del campo de desglose de Material(si es popup no vendra nada)</param>
''' <param name="tooltip">tooltip del control</param>
''' <param name="TipoSolicit">Tipo de Solicitud (1- solic compra, 2- Certif, ...,8-pedido expres,...)</param>
''' <param name="IdEntryUniPedido">Id del campo Unidad de pedido</param>
''' <param name="UnidadOcultaPedido">Valor de la unidad de pedido</param>
''' <param name="MonSolicit">Moneda de Solicitud/formulario</param>
''' <param name="FPagoOculta">Valor de la forma de pago</param>
''' <param name="UsarOrgCompras">Indica si debemos usar Org Compras para filtrar determinados valores</param>
''' <param name="Servicio">Id del servicio si es un campo externo</param>
''' <param name="Formato">Formato del campo si es un campo de texto</param>
''' <param name="TipoRecepcion">Si es un articulo de recepcion por cantidad (0) o por importe (1)</param>
''' <param name="NivelSeleccion">Nivel del �rbol de materiales que es obligatorio seleccionar : 0(ninguno), 1 a 4</param>
''' <param name="IdEntryPartidaPlurianual">id del entry relacionado con a�o partida. este sera entry partida</param>
''' <param name="EntryPartidaPlurianualCampo">el entry relacionado con a�o partida. este sera entry partida. Esta en campo o desglose</param>
''' <remarks>Llamada desde:GeneralEntry.vb/InitScript; Tiempo m�ximo:0</remarks>*/
function fsGeneralEntry_Setup(name, id, tipo, tipoGS, valor, isGridEditor, idCampo, nullMsg, readOnly, idGrid, title, errMsg, errMsgFormato, maxLength, sRestric,
    obligatorio, basedesglose, dependentfield, validationcontrol, independiente, idCampoPadre, arrCamposHijos, intro, articuloCodificado, orden, grupo, idatrib,
    articuloGenerico, DenArticuloModificado, anyadir_art, valerp, codigoArticulo, x, y, idRefSol, avisoBloqueo, instanciaMoneda, cargarUltADJ, idEntryPREC, idEntryPROV,
    idEntryUNI, idEntryCC, idDataEntryDependent, idDataEntryDependent2, PM, calculado, TablaExterna, campo_origen, idCampoPadre_origen, arrCamposHijos_origen, IdiomaBotonTextoMedio,
    PoseeDefecto, DataValorDefecto, TextoValorDefecto, MonRepercutido, CambioRepercutido, MonCentral, CambioCentral, CtrlCambioRechazo, NCSinRevisar, VerUON, PRES5, Concepto,
    Almacenamiento, Recepcion, Texto, RecordarValores, idEntryCANT, NumeroDeDecimales, UnidadOculta, idCampoHijaListaEnlazada, idCampoPadreListaEnlazada, idCampoListaEnlazada,
    valoresCargados, Portal, CampoDef_CampoODesgHijo, CampoDef_DesgPadre, arrCamposHijos_CampoDef, CampoOrigenVinculado, sDescrComboPopUp, esProveedorParticipante, Contactos,
    idDataEntryDependent3, idMasterField, DenGrupo, DenDesgloseMat, tooltip, TipoSolicit, IdEntryUniPedido, UnidadOcultaPedido, MonSolicit, FPagoOculta, UsarOrgCompras, Servicio,
    Formato, TipoRecepcion, NivelSeleccion, idDataEntryFORMMaterial, ActivoSM, filasAutoCompleteExtender, FechaNoAnteriorAlSistema, CargarProveSumiArt, idEntryART, idEntryProveSumiArt,
    idDataEntryDependentCentroCoste, IdMaterialOculta, IdCodArticuloOculta, IdDenArticuloOculta, bSelCualquierMatPortal, IdEntryPartidaPlurianual, EntryPartidaPlurianualCampo) {
    var o;
    o = new Object()
    o.name = name
    o.id = id
    //calcular la linea a la que corresponde el desglose a partir de su id
    o.idLinea = getIdLineaDesglose(o);
    o.isDesglose = isDesglose(o);
    o.ActivoSM = ActivoSM;
    o.FilasAutoCompleteExtender = filasAutoCompleteExtender;
    o.FechaNoAnteriorAlSistema = FechaNoAnteriorAlSistema;
    o.CargarProveSumiArt = CargarProveSumiArt;
    o.idEntryProveSumiArt = idEntryProveSumiArt;
    o.idEntryART = idEntryART;
    if (document.getElementById(id + "__tbl")) {
        o.Element = document.getElementById(id + "__tbl")
        o.Element.Object = o
        o.Dependent = null

        switch (tipoGS) {
            case 100:
                o.Editor = igedit_getById(id + "__t")
                o.Editor.fsEntry = o
                o.Editor.idGrid = idGrid
                if ((PM == true) && (!readOnly)) {
                    o.Editor2 = igedit_getById(id + "__t2")
                    o.Editor2.fsEntry = o
                    o.Editor2.idGrid = idGrid
                }
                break;
            case 136: //Desglose de actividad
                o.Editor = document.getElementById(id + "__tbl")
                break;
            case 117: //ProveContacto
            case 149: //Empresa
                o.Editor = igedit_getById(id + "__t");
                break;
            case 103:
                o.Editor = igedit_getById(id + "__t")
                if (sRestric)
                    o.Restric = sRestric
                else
                    o.Restric = ""
                //Tarea 3495
                if (idDataEntryFORMMaterial) {
                    o.idDataEntryFORMMaterial = idDataEntryFORMMaterial;
                }
                //Si tiene un campo de lista enlazada como hijo
                o.idCampoHijaListaEnlazada = idCampoHijaListaEnlazada;
                o.SelCualquierMatPortal = bSelCualquierMatPortal;
                break;
            case 106:
                o.Editor = igedit_getById(id + "__t")
                break;
            case 110:
            case 111:
            case 112:
            case 113:
                o.Editor = igedit_getById(id + "__t");
                o.Editor.fsEntry = o;
                o.Editor.idGrid = idGrid;
                if (PM == true) {
                    o.Editor2 = igedit_getById(id + "__t2");
                    o.Editor2.fsEntry = o;
                    o.Editor2.idGrid = idGrid;
                }
                break;
            case 104: //CodArticulo Solicitudes anteriores
                o.UnidadOrganizativaDependent = document.getElementById(id + "__hUON");
                o.Dependent = document.getElementById(id + "__hmat");
                o.OrgComprasDependent = document.getElementById(id + "__hOrgCompras");
                o.CentroDependent = document.getElementById(id + "__hCentro");
                o.UnidadDependent = document.getElementById(id + "__hUni");
                o.UnidadPedidoDependent = document.getElementById(id + "__hUniPed");
                o.Editor = igedit_getById(id + "__t");
                o.EditorDen = igedit_getById(id + "__tden");
                o.EditorDen.idGrid = idGrid;
                o.EditorDen.fsEntry = o;
                if (sRestric)
                    o.Restric = sRestric
                else
                    o.Restric = ""
                break;
                //Tarea 3495 
                if (idDataEntryFORMMaterial) {
                    o.idDataEntryFORMMaterial = idDataEntryFORMMaterial;
                }            
            case 119: //CodArticulo Solicitudes nuevas
                o.UnidadOrganizativaDependent = document.getElementById(id + "__hUON");
                o.Dependent = document.getElementById(id + "__hmat");
                o.OrgComprasDependent = document.getElementById(id + "__hOrgCompras");
                o.CentroDependent = document.getElementById(id + "__hCentro");
                o.UnidadDependent = document.getElementById(id + "__hUni");
                o.UnidadPedidoDependent = document.getElementById(id + "__hUniPed");
                o.Editor = igedit_getById(id + "__t");
                if (sRestric)
                    o.Restric = sRestric
                else
                    o.Restric = ""
                if ((PM == true) && (!readOnly)) {
                    o.Editor2 = igedit_getById(id + "__t2");
                    o.Editor2.fsEntry = o;
                    o.Editor3 = igedit_getById(id + "__t3");
                    //Favorito. Solo Pm. Ni Contratos ni pedido ni qa ni portal.
                    if (o.Editor3) {
                        o.Editor3.fsEntry = o;
                    }
                }
                //Tarea 3495
                if (idDataEntryFORMMaterial) {
                    o.idDataEntryFORMMaterial = idDataEntryFORMMaterial;
                }
                break;
            case 118:
                o.UnidadOrganizativaDependent = document.getElementById(id + "__hUON");
                o.Dependent = document.getElementById(id + "__hmat")
                o.OrgComprasDependent = document.getElementById(id + "__hOrgCompras")
                o.CentroDependent = document.getElementById(id + "__hCentro")
                o.UnidadDependent = document.getElementById(id + "__hUni")
                o.UnidadPedidoDependent = document.getElementById(id + "__hUniPed")
                o.EditorDen = igedit_getById(id + "__tden")

                if (sRestric)
                    o.Restric = sRestric
                else
                    o.Restric = ""
                if ((PM == true) && (!readOnly)) {
                    o.Editor2 = igedit_getById(id + "__t2");
                    o.Editor2.fsEntry = o;
                }
                break;

            case 45: //ImporteRepercutido
                o.Editor = igedit_getById(id + "__t")
                o.Editor.fsEntry = o
                o.Editor.idGrid = idGrid

                dependentfield = id + "__t2"

                o.MonRepercutido = MonRepercutido
                o.CambioRepercutido = CambioRepercutido

                o.MonCentral = MonCentral
                o.CambioCentral = CambioCentral

                break;
            case 2000: //Desglose Vinculado
                o.Editor = igedit_getById(id + "__t")
                break;
            case 138: //Factura
                o.Dependent = document.getElementById(id + "__hIdFactura")
                o.Editor = igedit_getById(id + "__t")
                o.Editor.hid = document.getElementById(id + "__h")
                o.Editor.fsEntry = o
                break;
            default:
                switch (tipo) {
                    //case 6:   
                    case 7:
                        //126 ListadosPersonalizados
                        if ((tipoGS == 126) && (intro == 1))
                            o.Editor = document.getElementById(id + "__t") //WebDropDown
                        else {
                            o.Editor = document.getElementById(id + "__t")
                            o.Editor.elem = document.getElementById(id + "__t")
                            o.Editor.hid = document.getElementById(id + "__h")
                        }
                        if (o.Editor.style.width == "")
                            o.Editor.style.width = "230px"

                        break;
                    case 15:
                    case 8:
                        try {
                            if (idCampoPadre == 'null' || idCampoPadre == null || idCampoPadre == 0)
                                o.Editor = igedit_getById(id + "__hNombre")
                            else
                                o.Editor = igedit_getById(id + "__t")
                        }
                        catch (e) {
                        }
                        if (o.Editor == null)
                            if (idCampoPadre == 'null' || idCampoPadre == null || idCampoPadre == 0)
                                o.Editor = document.getElementById(id + "__hNombre")
                            else
                                o.Editor = document.getElementById(id + "__t")
                        break;
                    case 9:
                        o.DivSource = document.getElementById(id + "__divsource")
                        o.Editor = document.getElementById(id + "__div")
                        igedit_getById(id + "__t").fsEntry = o
                        break;
                    case 10:
                        o.Editor = document.getElementById(id + "__t")
                        break;
                    default:
                        if ((tipoGS != 119) && (tipoGS != 118) && (tipoGS != 104) && (tipoGS != 127) && (tipoGS != 121) && (tipoGS != 116) && (intro == 1)) {
                            if (!readOnly)
                                o.Editor = document.getElementById(id + "__t") //WebDropDown
                            else
                                o.Editor = igedit_getById(id + "__t")
                            o.Editor.value = valor

                            if (Texto != null) {
                                o.Editor.value = Texto
                            }
                            if (tipoGS == 124)
                                o.Dependent = document.getElementById(id + "__hOrgCompras")
                            if (tipoGS == 123)
                                o.CentroDependent = document.getElementById(id + "__hCentro")


                            //Meter datos de la lista enlazada si tiene alguno
                            o.idCampoHijaListaEnlazada = idCampoHijaListaEnlazada
                            o.idCampoPadreListaEnlazada = idCampoPadreListaEnlazada
                            o.idCampoListaEnlazada = idCampoListaEnlazada
                            o.valoresCargados = valoresCargados

                            if (o.idCampoHijaListaEnlazada != 0 || o.idCampoPadreListaEnlazada != 0)
                                o.ListaEnlazada = true
                            else
                                o.ListaEnlazada = false
                        } else {
                            if ((RecordarValores == true) || (((tipoGS == 0) || (tipoGS == 1) || (tipoGS == 2) || (tipoGS == 126) || (tipoGS == 133) || (tipoGS == 134)
                            || (tipoGS == 20) || (tipoGS == 23) || (tipoGS == 24) || (tipoGS == 25) || (tipoGS == 26) || (tipoGS == 30) || (tipoGS == 31)
                            || (tipoGS == 32) || (tipoGS == 35) || (tipoGS == 38) || (tipoGS == 39) || (tipoGS == 1020)) && (tipo == 6) && (TablaExterna == 0))) {
                                o.Editor = document.getElementById(id + "__t")
                                o.Editor.elem = document.getElementById(id + "__t")
                                o.Editor.hid = document.getElementById(id + "__h")
                            } else
                                o.Editor = igedit_getById(id + "__t")
                        }
                }
        }

        if (o.tipoGS == 102)
            o.TipoSolicit = TipoSolicit

        if (tipoGS == 109) { //Destino
            o.Editor2 = igedit_getById(id + "__t2");
            o.Editor2.fsEntry = o;
        }
        if (tipoGS == 118) {
            if (o.EditorDen == null)
                o.EditorDen = document.getElementById(id + "__tden")
            o.EditorDen.fsEntry = o
        }
        else {
            if (!((tipoGS == 132) && (!readOnly)) && (tipo != 15)) {
                if (o.Editor == null)
                    o.Editor = document.getElementById(id + "__t")
                if (document.getElementById(id + "__h")) //hidden donde se guarda el texto de los campos de tipo texto medio y largo.
                    o.Editor.hid = document.getElementById(id + "__h")
                o.Editor.fsEntry = o
            }
        }
        o.Hidden = document.getElementById(id + "__h")
        o.nullMsg = nullMsg
        o.errMsg = errMsg
        o.errMsgFormato = errMsgFormato
        o.maxLength = maxLength
        o.title = title
        o.tipo = tipo
        o.tipoGS = tipoGS
        o.idCampo = idCampo
        o.readOnly = readOnly
        o.isGridEditor = isGridEditor
        o.obligatorio = obligatorio
        o.basedesglose = basedesglose
        o.independiente = independiente
        o.getVisible = fsGeneralEntry_GetVisible;
        o.setVisible = fsGeneralEntry_SetVisible;
        o.setValue = fsGeneralEntry_SetValue;
        o.getValue = fsGeneralEntry_GetValue;
        o.setToolTip = fsGeneralEntry_SetToolTip;
        o.getDataValue = fsGeneralEntry_GetDataValue;
        o.setDataValue = fsGeneralEntry_SetDataValue;
        o.addEventListener = fsGeneralEntry_addEventListener;
        o.removeEventListener = fsGeneralEntry_removeEventListener;
        o.getRenderedValue = fsGeneralEntry_getRenderedValue;
        o.dataValue = valor;
        if (tipoGS == 118)
            o.EditorDen.idGrid = idGrid;
        else {
            if ((tipoGS != 132) && (tipo != 15))
                o.Editor.idGrid = idGrid;
        }
        o.addFile = fsGeneralEntry_AddFile;
        o.removeFile = fsGeneralEntry_RemoveFile;
        o.addDesgloseLine = fsGeneralEntry_AddDesgloseLine;
        o.addDesgloseLineVinc = fsGeneralEntry_AddDesgloseLineVinc;
        o.removeDesgloseLine = fsGeneralEntry_RemoveDesgloseLine;
        o.dependentfield = dependentfield;
        o.validationcontrol = validationcontrol;
        o.resize = fsGeneralEntry_Resize;
        o.intro = intro;
        o.articuloCodificado = articuloCodificado;
        o.articuloGenerico = articuloGenerico;
        o.DenArticuloModificado = DenArticuloModificado;
        o.codigoArticulo = codigoArticulo;
        o.orden = orden;
        o.Grupo = grupo;
        o.DenGrupo = DenGrupo;
        o.DenDesgloseMat = DenDesgloseMat;
        o.IdAtrib = idatrib;
        o.ValErp = valerp;
        o.IdiomaBotonTextoMedio = IdiomaBotonTextoMedio;
        if (document.getElementById(id + "__b") && !o.readOnly)
            o.Button = document.getElementById(id + "__b");

        if (isGridEditor) {
            o.setVisible(false);
        }
        o.idCampoPadre = idCampoPadre;
        if (independiente == "Accion") //Para las acciones de las no conformidades
        {
            arrAcciones[arrAcciones.length] = id
        }
        else {
            if (!independiente) {
                arrInputs[arrInputs.length] = id
            }
        }

        o.arrCamposHijos = arrCamposHijos
        o.anyadir_art = anyadir_art;
        o.nColumna = x  //Variables que sirven para localizar los codigos de
        o.nLinea = y	// almacen, centro, departamento...en una grid.
        o.idRefSol = idRefSol;
        o.avisoBloqueoRefSOL = avisoBloqueo;
        o.PM = PM;
        o.calculado = calculado;
        o.TablaExterna = TablaExterna;
        o.Servicio = Servicio;
        o.Formato = Formato;

        //Variables origen= Sirven para obtener el campo del dise�o cuando se le da al boton enviar a favoritos en la etapa peticionario, cuando se ha guardado una solicitud
        o.campo_origen = campo_origen; //
        o.idCampoPadre_origen = idCampoPadre_origen  //
        o.arrCamposHijos_origen = arrCamposHijos_origen  //

        //Valores Defecto
        o.PoseeDefecto = PoseeDefecto;
        o.TextoValorDefecto = TextoValorDefecto;
        o.DataValorDefecto = DataValorDefecto;
        o.RecordarValores = RecordarValores;

        //Variables exclusivas de Portal
        o.CtrlCambioRechazo = CtrlCambioRechazo
        o.NCSinRevisar = NCSinRevisar
        o.Portal = Portal

        //
        o.CampoDef_CampoODesgHijo = CampoDef_CampoODesgHijo
        o.CampoDef_DesgPadre = CampoDef_DesgPadre
        o.arrCamposHijos_CampoDef = arrCamposHijos_CampoDef
        //
        o.CampoOrigenVinculado = CampoOrigenVinculado
        //
        o.DescrComboPopUp = sDescrComboPopUp
        //
        if ((o.tipoGS == 119) || (o.tipoGS == 118) || (o.tipoGS == 128) || (o.tipoGS == 104))  //NuevoCodArticulo o DenominacionArticulo
            o.instanciaMoneda = instanciaMoneda;

        if ((o.tipoGS == 9) || (o.tipoGS == 100)) // Precio Unitario o Proveedor
            o.cargarUltADJ = cargarUltADJ //Variable que indica si carga o no el ultimo PRECIO / PROVEEDOR adjudicado

        if (o.tipoGS == 129) //centro de coste
            o.VerUON = VerUON

        if (o.tipoGS == 130) { //partida presupuestaria
            o.idEntryCC = idEntryCC;
            o.PRES5 = PRES5
        }

        if (o.tipoGS == 151){ //a�o partida presupuestaria plurianual
            o.IdEntryPartidaPlurianual = IdEntryPartidaPlurianual;
            o.EntryPartidaPlurianualCampo = EntryPartidaPlurianualCampo
        }

        if (o.tipoGS == 131) //activo
            o.idEntryCC = idEntryCC;

        if ((o.tipoGS == 132) || (o.tipoGS == 118) || (o.tipoGS == 119)) { //tipo de pedido, cod y den articulo
            o.Concepto = Concepto
            o.Almacenamiento = Almacenamiento
            o.Recepcion = Recepcion
        }

        if (o.tipoGS == 4) { //cantidad
            o.NumeroDeDecimales = NumeroDeDecimales
            o.UnidadOculta = UnidadOculta
            o.TipoRecepcion = TipoRecepcion
        }

        if ((o.tipoGS == 119) || (o.tipoGS == 118) || (o.tipoGS == 104)) { //NuevoCodArticulo o DenominacionArticulo o CodArticulo Antiguo

            o.idEntryPREC = idEntryPREC;
            o.idEntryPROV = idEntryPROV;
            o.idEntryUNI = idEntryUNI;
            o.idEntryCANT = idEntryCANT;
            o.idEntryCC = idEntryCC;
            o.idDataEntryDependent = idDataEntryDependent //Organizacion de Compras
            o.idDataEntryDependent2 = idDataEntryDependent2 //Centro
            o.idDataEntryDependent3 = idDataEntryDependent3 //Unidad Organizativa
            o.idDataEntryDependentCentroCoste = idDataEntryDependentCentroCoste //Centro de Coste
            //Tarea 2904
            o.TipoSolicit = TipoSolicit
            o.IdEntryUniPedido = IdEntryUniPedido
            o.UnidadPedidoOculta = UnidadOcultaPedido
            o.MonSolicit = MonSolicit
            //Tarea 3495
            if (idDataEntryFORMMaterial) {
                o.idDataEntryFORMMaterial = idDataEntryFORMMaterial;
            }
            //Gestamp Procesos / 2017 / 1146
            o.IdMaterialOculta = IdMaterialOculta
            o.IdCodArticuloOculta = IdCodArticuloOculta
            o.IdDenArticuloOculta = IdDenArticuloOculta
        }

        if (o.tipoGS == 105)
            o.idEntryCANT = idEntryCANT;
        //proveedor ERP
        if (o.tipoGS == 143) {
            o.idEntryPROV = idEntryPROV; //Campo proveedor del que depende el campo proveedor ERP
            o.idDataEntryDependent = idDataEntryDependent //Campo organizacion de compras del que depende el campo proveedor ERP
        }
        if (o.tipoGS == 123) {
            o.idDataEntryDependent = idDataEntryDependent // Relacion entre Organizacion de Compras y articulo
            o.idDataEntryDependent2 = idDataEntryDependent2 //Organizacion de Compras y Centro
        }
        if (o.tipoGS == 124) {
            o.idDataEntryDependent = idDataEntryDependent // Si es Centro se relaciona con el articulo
            o.idDataEntryDependent2 = idDataEntryDependent2 //Relacion entre el Centro y la organizacion de compras
            o.idDataEntryDependent3 = idDataEntryDependent3 //Relacion entre el Centro y almacen
        }

        if (o.tipoGS == 100) {
            o.idDataEntryDependent = idDataEntryDependent // Si es Proveedor se relaciona con Org Compras
            o.idDataEntryDependent2 = idDataEntryDependent2 // y con el Material
            o.esProveedorParticipante = esProveedorParticipante
            o.Contactos = Contactos
            o.TipoSolicit = TipoSolicit
            o.idDataEntryDependent3 = idDataEntryDependent3 // y con la forma pago
            o.FPagoOculta = FPagoOculta;
        }
        if ((o.tipoGS == 6) || (o.tipoGS == 7)) //Fecha Inicio Suministro, Fin suministro
            o.idDataEntryDependent = idDataEntryDependent

        if (o.tipoGS == 103) { //Material
            o.idDataEntryDependent2 = idDataEntryDependent2 // Si es material se relaciona con el proveedor	
            o.TipoSolicit = TipoSolicit
            o.NivelSeleccion = NivelSeleccion;
            if (idDataEntryFORMMaterial) {
                o.idDataEntryFORMMaterial = idDataEntryFORMMaterial;
            }
            //Gestamp Procesos / 2017 / 1146
            o.IdCodArticuloOculta = IdCodArticuloOculta
            o.IdDenArticuloOculta = IdDenArticuloOculta
        }
        if (o.tipoGS == 0 && o.intro == 2) {    //Atributos de lista externa
            o.idDataEntryDependent = idDataEntryDependent;      //Org. compras
            o.idDataEntryDependent2 = idDataEntryDependent2;    //Centro SM
            o.idDataEntryDependent3 = idDataEntryDependent3;    //UON
        }
        if (o.tipo == 9)
            o.idDataEntryDependent = idDataEntryDependent //Relaciona un desglose con campo OrgCompras anterior

        if (o.tipoGS == 121) o.idDataEntryDependent = idDataEntryDependent //Relaciona la UON con Articulo
        if (o.tipoGS == 109) o.idDataEntryDependent3 = idDataEntryDependent3;  //Relacion entre el Centro y almacen

        o.masterField = idMasterField
        o.oldDataValue = '';

        if (o.tipoGS == 125 || (o.tipoGS == 0 && o.intro == 2))//Almacen o atributos de lista externa
            o.UsarOrgCompras = UsarOrgCompras;

        //132   TipoPedido    Readonly->webtext   No ReadOnly->webdrop
        //      Lista         Readonly->webtext   No ReadOnly->webdrop
        //      String        Recordar->No tooltip  No Recordar->tooltip 
        //45    Importe Repercutido     Readonly-> No tooltip   No ReadOnly->webnumeric y tooltip
        //Desde generalentry.vb, se hace _tooltip a nothing y as� seguro q no entra
        if (tooltip != null) {
            if (document.getElementById(id + "__t_t")) {
                document.getElementById(id + "__t_t").title = tooltip;
            }
            else {
                if (document.getElementById(id + "__tden_t")) {
                    //Descr Articulo: Esta en CtrlGSTipoArticulo y CtrlGSTipoDenArticulo
                    document.getElementById(id + "__tden_t").title = tooltip;
                }
            }
        }
    }
}
/* Revisado por: blp. Fecha: 04/10/2011
''' <summary>
''' Funci�n que redimensiona el elemento
''' </summary>
''' <remarks>Llamada desde: fsGeneralEntry Se asigna al resize del objeto creado. Cada vez que se llame al resize se llamar� a la funci�n ; Tiempo m�ximo: 0</remarks>*/
function fsGeneralEntry_Resize() {
    var bVisible = true;
    var width;
    //CALIDAD
    if (this.Element.parentElement.style.width) {
        //CALIDAD
        width = parseFloat(this.Element.parentElement.style.width);
        //CALIDAD
        height = parseFloat(this.Element.parentElement.style.height);
    }
    return
    if (width) {
        this.Element.style.width = width
        switch (this.tipo) {
            case 8:
                this.Element.style.width = width - 1
                this.Element.style.top = top
                this.Element.style.left = left + 1
                if (this.Button) {
                    this.Button.style.width = "18px"
                    this.Button.style.height = height - 2
                    this.Editor.style.width = width - 19
                }
                else
                    this.Editor.style.width = width

                this.Editor.style.height = height - 2
                break;

            case 7:
                if (this.Button) {
                    this.Editor.setVisible(bVisible, 0, 0, width - parseFloat(this.Button.style.width), height)
                    this.Button.style.width = "18px"
                    this.Button.style.position = "absolute"
                    this.Button.style.height = this.Editor.Element.style.height
                    this.Button.style.top = this.Editor.Element.style.top
                    this.Button.style.left = parseFloat(this.Editor.Element.style.left) + parseFloat(this.Editor.Element.style.width)
                }
                else
                    this.Editor.setVisible(bVisible, 0, 0, width, height)

                break;
            default:
                switch (this.tipoGS) {
                    case 18: //articulo
                        var wB = 0
                        if (this.Button) {
                            this.Button.style.width = "18px"
                            this.Button.style.height = height - 2
                            wB = 18
                        }
                        this.Editor.setVisible(bVisible, 0, 0, (width - wB) * 0.40, height)
                        this.EditorDen.setVisible(bVisible, (width - wB) * 0.40, 0, (width - 18) * 0.60, height)
                        break;
                    case 110:
                    case 111:
                    case 112:
                    case 113:
                        this.Element.style.width = width - 1
                        this.Element.style.top = top - 2
                        this.Element.style.left = left
                        var wB = 0
                        if (this.Button) {
                            this.Button.style.width = "18px"
                            this.Button.style.height = height - 1
                            wB = 19
                        }
                        this.Editor.style.width = width - wB
                        this.Editor.style.height = height
                        break;
                    case 100:
                    case 103:
                        this.Element.style.width = width - 1
                        this.Element.style.top = top
                        this.Element.style.left = left + 1
                        var wB = 0
                        if (this.Button) {
                            this.Button.style.width = "18px"
                            this.Button.style.height = height - 2
                            wB = 19
                        }
                        this.Editor.style.width = width - wB
                        this.Editor.style.height = height - 2
                        break;
                    default:
                        this.Editor.setVisible(true, 0, 0, width, height)
                        break;
                }
        }

    }
}
/* Revisado por: blp. Fecha: 04/10/2011
''' <summary>
''' Eliminar una linea de un desglose popoup
''' </summary>
''' <param name="linea">Array con los campos y valores respectivos, de la linea</param>    
''' <remarks>Llamada desde: todos los desglose.aspx de pmweb; Tiempo m�ximo:0</remarks>*/
function fsGeneralEntry_RemoveDesgloseLine(linea) {
    var bValor

    bValor = false
    var sId = this.id

    var arrID = sId.split("_")
    sId = arrID[arrID.length - 1]

    var sPreCampo = sId.replace(this.idCampo.toString(), "")


    var sPre = replaceAll(this.id, sId, "")

    for (oPar in linea) {
        var arrcampo = linea[oPar].split("_")
        var campo = arrcampo[arrcampo.length - 1]
        var index = arrcampo[arrcampo.length - 2]
        if (document.getElementById(this.id + "__" + index.toString() + "__" + campo)) {
            document.getElementById(this.id + "__" + index.toString() + "__" + campo).outerHTML = ""
        }
        if (document.getElementById(this.id + "__" + index.toString() + "__" + campo + "_hNew")) {
            document.getElementById(this.id + "__" + index.toString() + "__" + campo + "_hNew").outerHTML = ""
        }

    }

    if (document.getElementById(this.id + "__" + index.toString() + "__Deleted"))
        document.getElementById(this.id + "__" + index.toString() + "__Deleted").outerHTML = ""
    if (document.getElementById(this.id + "__" + index.toString() + "__Linea"))
        document.getElementById(this.id + "__" + index.toString() + "__Linea").outerHTML = ""

    var i = 0;
    for (k = 1; k <= document.getElementById(this.id + "__numRowsVinc").value; k++) {
        var oLineaV = document.getElementById(this.id + "__" + k.toString() + "__LineaVinc");
        if (oLineaV) {
            if ((oLineaV.value != "0") && (oLineaV.value != "-2") && (oLineaV.value != "-4")) {
                i++;
                if (i == index) {
                    if (document.getElementById(this.id + "__" + k.toString() + "__LineaVinc"))
                        document.getElementById(this.id + "__" + k.toString() + "__LineaVinc").value = "-1"
                    break;
                }
            }
        }
    }
}
/* Revisado por: blp. Fecha: 04/10/2011
''' <summary>
''' Desde un desglose popup cuando das a guardar cambios y cerrar salta javascript actualizarCampoDesglose este 
''' javascript llama a esta funci�n que trae toda la informaci�n de los entrys y la mete en los html de la pantalla 
''' q contiene al desglose. Creando inputs ocultos si es necesario.
''' </summary>
''' <param name="linea">n� de linea creada</param>        
''' <remarks>Llamada desde:script/alta/desglose.aspx javascript actualizarCampoDesglose
'''	script/certificados/desglose.aspx javascript actualizarCampoDesglose
'''	script/nocoformidad/desglose.aspx javascript actualizarCampoDesglose
'''	script/workflow/desglose.aspx javascript actualizarCampoDesglose; Tiempo m�ximo:0</remarks>
*/
function fsGeneralEntry_AddDesgloseLine(linea) {
    var bValor
    var bControlGenerico = false;

    bValor = false
    var sId = this.id

    var arrID = sId.split("_")
    sId = arrID[arrID.length - 1]

    var sPreCampo = sId.replace(this.idCampo.toString(), "")
    var sPre = replaceAll(this.id, sId, "")

    for (oPar in linea) {
        if (!bValor) {
            //ucDesglose_fsdsentry_1_274250
            //ucDesglose_fsdsentry_1_274250_*
            var arrcampo = linea[oPar].split("_")
            var bControlGenerico;
            var campo;
            var index;
            if (arrcampo[arrcampo.length - 1] == '*') {
                bControlGenerico = true;
                campo = arrcampo[arrcampo.length - 2];
                index = arrcampo[arrcampo.length - 3];

            } else {
                bControlGenerico = false;
                campo = arrcampo[arrcampo.length - 1];
                index = arrcampo[arrcampo.length - 2];
            }
            if (!document.getElementById(this.id + "__" + index.toString() + "__" + campo)) {
                var s = '';
                s = this.DivSource.innerHTML
                s = ReplaceDolar(s)
                s = replaceAll(s, sPreCampo + this.idCampo, sPreCampo + "_" + index.toString() + "_" + this.idCampo + "__" + index.toString())
                s = replaceAll(s, sPreCampo + "_" + index.toString() + "_" + this.idCampo, sPreCampo + this.idCampo)
                s = replaceAll(s, "__" + index.toString() + "ACambiar", "$_" + index.toString() + "_")
                s = replaceAll(s, "ACambiar", "$")
                //No repitas el input _delete si ya existe porque si no no se cargar�n bien los datos de desglose al volver a abrir
                //El input delete es el primero en this.DivSource.innerHTML
                //si eso cambia esta funcion deja de funcionar
                //Cuidado con Firefox vs IE por lo de las comillas
                var iniPosicionIdDeleted = s.indexOf('id="', 0) + 4; //sumamos la posicion de 'id="', que es 4
                var finPosicionIdDeleted = s.indexOf('_Deleted"', 0) - 1; //quitamos la posicion de la comilla ", o sea, 1
                if (s.indexOf('_Deleted"', 0) == -1) { //FF
                    iniPosicionIdDeleted = s.indexOf('id=', 0) + 3;
                    finPosicionIdDeleted = s.indexOf('_Deleted', 0);
                }
                var idDeleted = s.substring(iniPosicionIdDeleted, finPosicionIdDeleted + 8);
                iniPosicionIdDeleted = null;
                finPosicionIdDeleted = null;
                if (document.getElementById(idDeleted)) {
                    var finPosicionDeleteTag = s.indexOf('_Deleted">', 0)
                    if (s.indexOf('_Deleted">', 0) == -1) { //FF
                        finPosicionDeleteTag = s.indexOf('_Deleted>', 0)
                        finPosicionDeleteTag += 9// _deleted> -->10 caracteres
                    } else
                        finPosicionDeleteTag += 10 // _deleted'> -->9 caracteres
                    s = s.slice(finPosicionDeleteTag)
                    finPosicionDeleteTag = null;
                }
                //FIN No repitas el input _delete si ya existe
                this.Editor.insertAdjacentHTML("beforeEnd", s)
            }
            bValor = true
        }
        else {
            var vValor = linea[oPar]
            if (document.getElementById(this.id + "__" + index.toString() + "__" + campo + "_hNew")) {
                document.getElementById(this.id + "__" + index.toString() + "__" + campo).value = vValor[0]
                document.getElementById(this.id + "__" + index.toString() + "__" + campo + "_hNew").value = vValor[1]
                if (document.getElementById(this.id + "__" + index.toString() + "__" + campo + "_hNombre"))
                    document.getElementById(this.id + "__" + index.toString() + "__" + campo + "_hNombre").value = vValor[2]
            }
            else
                if (bControlGenerico == true) {
                    document.getElementById(this.id + "__" + index.toString() + "__" + campo).value = vValor[0]
                    document.getElementById(this.id + "__" + index.toString() + "__" + campo).articuloGenerico = vValor[1]
                    document.getElementById(this.id + "__" + index.toString() + "__" + campo).DenArticuloModificado = vValor[2]
                    if ((vValor[3] != null) && (vValor[3] != "") || ((vValor[3] == "") && (vValor[2] == true))) {
                        document.getElementById(this.id + "__" + index.toString() + "__" + campo).codigoArticulo = vValor[3]
                        if (document.getElementById(this.id + "__" + index.toString() + "__" + (campo - 1)))
                            document.getElementById(this.id + "__" + index.toString() + "__" + (campo - 1)).value = vValor[3]
                    }
                }
                else {
                    if (document.getElementById(this.id + "__" + index.toString() + "__" + campo)) {
                        if (document.getElementById(this.id + "__" + index.toString() + "__" + campo).tipoGS == 45) {
                            if (!document.getElementById(this.id + "__" + index.toString() + "__" + campo + "_MonRepercutido")) {
                                var s = '';
                                s = "<INPUT id=" + this.id + "__" + index.toString() + "__" + campo + "_MonRepercutido type=hidden name=" + this.name + "$_" + index.toString() + "__" + campo + "_MonRepercutido runat=server>"
                                this.Editor.insertAdjacentHTML("beforeEnd", s)
                                s = "<INPUT id=" + this.id + "__" + index.toString() + "__" + campo + "_CambioRepercutido type=hidden name=" + this.name + "$_" + index.toString() + "__" + campo + "_CambioRepercutido runat=server>"
                                this.Editor.insertAdjacentHTML("beforeEnd", s)
                            }

                            document.getElementById(this.id + "__" + index.toString() + "__" + campo).value = vValor[0]
                            document.getElementById(this.id + "__" + index.toString() + "__" + campo + "_MonRepercutido").value = vValor[1]
                            document.getElementById(this.id + "__" + index.toString() + "__" + campo + "_CambioRepercutido").value = vValor[2]
                        }
                        else {
                            var tipoGs = document.getElementById(this.id + "__" + index.toString() + "__" + campo).tipoGS
                            if (!tipoGs && document.getElementById(this.id + "__" + campo))
                                tipoGs = document.getElementById(this.id + "__" + campo).tipoGS
                            var Intro = document.getElementById(this.id + "__" + index.toString() + "__" + campo).intro
                            if (!Intro && document.getElementById(this.id + "__" + campo))
                                Intro = document.getElementById(this.id + "__" + campo).intro
                            var Tipo = document.getElementById(this.id + "__" + index.toString() + "__" + campo).tipo
                            if (!Tipo && document.getElementById(this.id + "__" + campo))
                                Tipo = document.getElementById(this.id + "__" + campo).tipo
                            //En lineas nuevas: lo coge bien. 
                            //En lineas viejas: hay ..._DescrComboPopUp
                            if (((tipoGs == 0) && (Intro == 1) && ((Tipo == 1) || (Tipo == 5) || (Tipo == 6))) || (document.getElementById(this.id + "__" + index.toString() + "__" + campo + "_DescrComboPopUp"))) {

                                if (vValor[0] != "")
                                    document.getElementById(this.id + "__" + index.toString() + "__" + campo).value = vValor[0]
                                else
                                    document.getElementById(this.id + "__" + index.toString() + "__" + campo).value = null

                                if (!document.getElementById(this.id + "__" + index.toString() + "__" + campo + "_DescrComboPopUp")) {
                                    var s = '';
                                    s = "<INPUT id=" + this.id + "__" + index.toString() + "__" + campo + "_DescrComboPopUp type=hidden name=" + this.name + "$_" + index.toString() + "__" + campo + "_DescrComboPopUp runat=server>"
                                    this.Editor.insertAdjacentHTML("beforeEnd", s)
                                }
                                if (vValor[0] != "")
                                    document.getElementById(this.id + "__" + index.toString() + "__" + campo + "_DescrComboPopUp").value = vValor[1]
                                else
                                    document.getElementById(this.id + "__" + index.toString() + "__" + campo + "_DescrComboPopUp").value = null
                            }
                            else {
                                if (document.getElementById(this.id + "__" + index.toString() + "__" + campo).tipoGS == 2000) {
                                    if (!document.getElementById(this.id + "__" + index.toString() + "__" + campo + "_IdSolicVinculada")) {
                                        s = "<INPUT id=" + this.id + "__" + index.toString() + "__" + campo + "_IdSolicVinculada type=hidden name=" + this.name + "$_" + index.toString() + "__" + campo + "_IdSolicVinculada runat=server>"
                                        this.Editor.insertAdjacentHTML("beforeEnd", s)
                                    }

                                    document.getElementById(this.id + "__" + index.toString() + "__" + campo).value = vValor[0]
                                    document.getElementById(this.id + "__" + index.toString() + "__" + campo + "_IdSolicVinculada").value = vValor[1]
                                }
                                else {
                                    //Si el valor del control es un array, pasamos s�lo el primer elemento, si no, todo.
                                    //Esto hace que los combos en los campos que se cargan desde servidor (por defecto) tomen su valor correcto
                                    if (isArray(vValor)) {
                                        document.getElementById(this.id + "__" + index.toString() + "__" + campo).value = vValor[0]
                                    } else {
                                        document.getElementById(this.id + "__" + index.toString() + "__" + campo).value = vValor
                                    }
                                }
                            }
                        }
                    }
                }

            bValor = false
            if (document.getElementById(this.id + "__" + index.toString() + "__" + campo)) {
                document.getElementById(this.id + "__" + index.toString() + "__" + campo).name = this.name + "$_" + index.toString() + "__" + campo
                if (document.getElementById(this.id + "__" + index.toString() + "__" + campo + "_hNew"))
                    document.getElementById(this.id + "__" + index.toString() + "__" + campo + "_hNew").name = this.name + "$_" + index.toString() + "__" + campo + "_hNew"
            }
        }
    }
}
/*
Revisado por: blp. Fecha: 04/10/2011
Funci�n que devuelve true si el objeto pasado el un array, false si no lo es
Llamada desde fsGeneralEntry_AddDesgloseLine. M�x<0,1 seg
*/
function isArray(testObject) {
    return testObject && !(testObject.propertyIsEnumerable('length')) && typeof testObject === 'object' && typeof testObject.length === 'number';
}
/*
Revisado por: blp. Fecha: 04/10/2011
Funci�n que a�ade al input hidden el id del archivo a�adido.
id: Id a a�adir.
Llamada desde addFile. M�x<0,1 seg
*/
function fsGeneralEntry_AddFile(id) {
    var inputFiles = document.getElementById(this.id + "__hNew")
    if (inputFiles.value != "")
        inputFiles.value = inputFiles.value + "xx" + id.toString()
    else
        inputFiles.value = id.toString()

    inputFiles = null;
}
/*
Revisado por: blp. Fecha: 04/10/2011
Funci�n que elimina del input hidden el id del adjunto q se pasa por par�metro.
tipo: Tipo del adjunto (normal o desglose).
id: id del adjunto.
Llamada desde addFile. M�x<0,1 seg
*/
function fsGeneralEntry_RemoveFile(tipo, id) {
    if (tipo == 1) {
        var inputFiles = document.getElementById(this.id + "__hAct")

        var re = eval("/xx" + id.toString() + "/i")
        var sValor = inputFiles.value
        sValor = sValor.replace(re, "")
        re = eval("/" + id.toString() + "/i")
        sValor = sValor.replace(re, "")
        re = /xx/
        if (sValor.search(re) == 0)
            sValor = sValor.replace(re, "")
        inputFiles.value = sValor
    }
    else {
        //hay que buscarlo en los 2 tanto en __hAct como __hNew
        var inputFiles = document.getElementById(this.id + "__hNew")
        var inputFilesAct = document.getElementById(this.id + "__hAct")

        var re = eval("/xx" + id.toString() + "/i")
        var sValor = inputFiles.value
        sValor = sValor.replace(re, "")
        re = eval("/" + id.toString() + "/i")
        sValor = sValor.replace(re, "")
        re = /xx/
        if (sValor.search(re) == 0)
            sValor = sValor.replace(re, "")
        inputFiles.value = sValor

        re = eval("/xx" + id.toString() + "/i")
        sValor = inputFilesAct.value
        sValor = sValor.replace(re, "")
        re = eval("/" + id.toString() + "/i")
        sValor = sValor.replace(re, "")
        re = /xx/
        if (sValor.search(re) == 0)
            sValor = sValor.replace(re, "")
        inputFilesAct.value = sValor
    }

}
function fsGeneralEntry_GetVisible() {
    return (this.Element.style.display != "none" && this.Element.style.visibility != "hidden")
}
function fsGeneralEntry_SetVisible(bVisible, left, top, width, height) {
    var wB
    if (this.readOnly)
        bVisible = false
    this.Element.style.display = (bVisible ? "" : "none");
    this.Element.style.visibility = (bVisible ? "visible" : "hidden");
    if (!bVisible)
        return
    this.Element.style.position = "absolute"
    if (top) {
        this.Element.style.top = top - 1
        this.Element.style.left = left - 1
        this.Element.style.height = height - 11
        this.Element.style.width = width
    }
}
/* Revisado por: blp. Fecha: 04/10/2011
''' <summary>
''' Devolver el valor de un entry.
''' </summary>
''' <returns>El valor de un entry</returns>
''' <remarks>Llamada desde: Cualquier javascript q use entry; Tiempo m�ximo: 0</remarks>*/
function fsGeneralEntry_GetDataValue() {
    var ret;
    switch (this.tipoGS) {
        case 11:  //proveedor adj.
        case 100: //proveedor
        case 103: //Material
        case 117: //Rol
        case 116: //ProveContacto
        case 115: //Persona
        case 129: //Centro de coste
        case 130: //Partida presupuestaria
        case 131: //Activo
        case 128: //Referencia a Solicitud
        case 144: //Comprador
        case 149:
            ret = this.Hidden.value;
            break;
        case 104: //CodArticulo
            ret = this.Editor.getValue();
            if (ret == "")
                ret = this.EditorDen.getValue()
            break;
        case 119: //CodArticulo Solicitudes Nuevas        
            //Para obtener el valor hay que acceder al textBox dentro del Editor, que es un tabla
            var idEditorInput = this.Editor.id + '_t';
            var oEditorInput = document.getElementById(idEditorInput);
            if (oEditorInput) {
                ret = oEditorInput.value;
                if (ret == "")
                    if (oEditorInput.EditorDen != null) {
                        var idEditorDen = oEditorInput.EditorDen.id + '_t';
                        var oEditorDen = document.getElementById(idEditorDen);
                        if (oEditorDen) ret = oEditorDen.value;
                    }
            }
            break;
        case 118: //Denominacion de articulo
            //Para obtener el valor hay que acceder al textBox dentro del Editor, que es un tabla
            var idEditorDen = this.EditorDen.id + '_t';
            var oEditorDen = document.getElementById(idEditorDen);
            if (oEditorDen) ret = oEditorDen.value;
            break;
        case 138: //Factura
            ret = this.Editor.getValue()
            break
        case 120:
            //ISG NumSolicitERP
            if (this.readOnly)
                ret = this.Hidden.value
            else
                ret = this.Editor.value
            break;
        case 106: //Desglose
            ret = null

            break;
        case 110: //presupuestos
        case 111:
        case 112:
        case 113:        
            ret = this.Hidden.value
            break;
        case 5: //Fecha Necesidad
        case 6: //Fecha inicio sum
        case 7: //Fecha fin sum
            var sFec = this.Editor.ID + "_p"
            var oFec = document.getElementById(sFec)
            ret = oFec.value
            break;
        case 3: //Importe
        case 10: //Precio Unitario Adj.
        case 12: //Cantidad Adj.
        case 4: //Cantidad
        case 9: //Precio unitario
            ret = this.Editor.getValue()
            break;
        case 8: //Archivos especificaciones
            ret = new Array()
            ret[0] = document.getElementById(this.id + "__hAct").value
            ret[1] = document.getElementById(this.id + "__hNew").value
            break;
        default: //Sin tipo
            if (this.TablaExterna > 0) {
                ret = this.dataValue;
            }
            else {
                switch (this.tipo) {
                    case 3: // Tipo Fecha
                        if (this.intro == 1) {
                            if (this.readOnly)
                                ret = this.Hidden.value
                            else
                                ret = this.dataValue
                        } else {
                            var sFec = this.Editor.ID + "_p"
                            var oFec = document.getElementById(sFec)
                            ret = oFec.value
                        }
                        break;
                    case 6:
                        if (this.readOnly)
                            ret = this.Hidden.value
                        else {
                            if (this.intro == 1)
                                ret = this.dataValue
                            else
                                ret = this.Editor.value
                        }
                        break;
                    case 7:
                        if (this.readOnly)
                            ret = this.Hidden.value
                        else {
                            if (this.intro == 1)
                                ret = this.dataValue
                            else
                                ret = this.Editor.value
                        }
                        break;
                    case 8: // Tipo Archivo
                        ret = new Array()
                        ret[0] = document.getElementById(this.id + "__hAct").value
                        ret[1] = document.getElementById(this.id + "__hNew").value
                        break;
                    case 4: // Boolean
                        ret = this.Hidden.value
                        break;
                    case 10:  //checkbox
                        if (this.Editor.checked)
                            ret = true
                        else
                            ret = false
                        break;
                    case 1:
                    case 5:
                        if (this.RecordarValores == true) {
                            if (this.readOnly)
                                ret = this.Hidden.value
                            else
                                ret = this.Editor.value
                        } else {
                            if (this.intro == 1) {
                                if (this.readOnly)
                                    ret = this.Hidden.value
                                else
                                    ret = this.dataValue
                            } else
                                ret = this.Editor.getValue()
                        }
                        break;
                    case 15:
                        ret = this.Hidden.value;
                        break;
                    case 16:
                        ret = this.Editor.text;
                        break;
                    default:
                        if (this.RecordarValores == true) {
                            if (this.readOnly)
                                ret = this.Hidden.value
                            else
                                ret = this.Editor.value
                        } else {
                            if (this.intro == 1) {
                                if (this.readOnly)
                                    ret = this.Hidden.value
                                else
                                    ret = this.dataValue
                            } else
                                ret = this.Editor.getValue()
                        }
                        break;
                }
            }
    }
    return (ret)
}
/* Revisado por: blp. Fecha: 04/10/2011
''' <summary>
''' Establecer el valor de un entry.
''' </summary>
''' <param name="newValue">nuevo valor, parametro sistema.</param>    
''' <returns>El valor de un entry</returns>
''' <remarks>Llamada desde: Cualquier javascript q use entry; Tiempo m�ximo: 0</remarks>*/
function fsGeneralEntry_SetDataValue(newValue) {
    this.dataValue = newValue;
    if ((this.intro == 1) && (this.tipoGS == 105) && (this.readOnly == false))
        SetIndexWebDropdown(this, newValue)
    if (this.isGridEditor) {
        var oCol = this.webGrid.Bands[0].getColumnFromKey("COLHID_" + this.idCampo.toString());
        this.webGrid.getActiveRow().getCell(oCol.Index).setValue(newValue.toString())
    }
    if (this.Hidden && this.TablaExterna == 0)
        this.Hidden.value = newValue

    if (this.tipo == 8) {
        document.getElementById(this.id + "__hAct").value = newValue[0]
        document.getElementById(this.id + "__hNew").value = newValue[1]
    }
}
function fsGeneralEntry_SetToolTip(newValue) {
    switch (this.tipoGS) {
        case 103:
            this.Editor.elem.title = newValue
            break;
        case 110:
        case 111:
        case 112:
        case 113:
            this.Editor.elem.title = newValue
            break;
    }
}
/* Revisado por: blp. Fecha: 04/10/2011
''' <summary>
''' Establecer el valor de un entry.
''' </summary>
''' <param name="newValue">nuevo valor, parametro sistema.</param>
''' <param name="bFireEvent">evento, parametro sistema</param>        
''' <returns>El valor de un entry</returns>
''' <remarks>Llamada desde: Cualquier javascript q use entry; Tiempo m�ximo: 0</remarks>*/
function fsGeneralEntry_SetValue(newValue, bFireEvent) {
    if (newValue == null && !this.readOnly) {
        newValue = this.nullMsg
    }
    
    switch (this.tipoGS) {
        case 100:
        case 110:
        case 111:
        case 112:
        case 113:
            this.Editor.setValue(newValue);
            this.Editor.elem.title = newValue;
            break;
        case 103: //Material
            if (newValue == "") {
                this.Editor.setValue("");
                this.Editor.title = "";
            } else {
                this.Editor.setValue(newValue)
                this.Editor.elem.title = newValue
            }
            break;
        case 104:
            if (newValue == "") {
                this.Editor.setValue("")
                this.EditorDen.setValue("")
            } else
                if (newValue != this.nullMsg) {
                    var sValor = newValue.split(" - ")
                    if (sValor.length > 2) {
                        this.Editor.setValue(sValor[0])
                        var sDen = sValor[1];
                        for (i = 2; i < sValor.length; i++)
                            sDen = sDen + ' - ' + sValor[i];

                        this.EditorDen.setValue(sDen);
                    } else {
                        this.Editor.setValue(sValor[0])
                        this.EditorDen.setValue(sValor[1])
                    }
                }
            break;
        case 119:  //CodArticulo Solicitudes Nuevas    
            //Para establecer el valor hay que acceder al textBox que hay dentro del Editor, que es una tabla          
            var idEditorInput = this.Editor.id + '_t';
            var oEditorInput = document.getElementById(idEditorInput);
            if (oEditorInput) {
                if (newValue == "") {
                    oEditorInput.value = "";
                    oEditorInput.title = "";
                    this.articuloCodificado = false;
                    if ($find(this.Editor.id + '_ext')) $find(this.Editor.id + '_ext').initializeCompletionList(oEditorInput);
                } else {
                    if (newValue != this.nullMsg) {
                        oEditorInput.value = newValue;
                        oEditorInput.title = newValue;
                    }
                }
                if (typeof (agregandoDesdeExcel) == 'undefined' || !agregandoDesdeExcel) VaciarCtrlsDependientes(this.Editor);
            }
            break;        
        case 118:        
            //Para establecer el valor hay que acceder al textBox que hay dentro del Editor, que es una tabla        
            var idEditorInput = this.EditorDen.id + '_t';
            var oEditorInput = document.getElementById(idEditorInput);
            if (oEditorInput) {
                if (newValue == "") {
                    oEditorInput.value = ""
                    oEditorInput.title = ""
                    if ($find(this.EditorDen.id + '_ext')) $find(this.EditorDen.id + '_ext').initializeCompletionList(oEditorInput);
                } else
                    if (newValue != this.nullMsg) {
                        oEditorInput.value = newValue
                        oEditorInput.title = newValue
                    }
            }
            break;
        case 149: //Empresa
            var idEditorInput = this.Editor.id + '_t';
            var oEditorInput = document.getElementById(idEditorInput);
            if (oEditorInput) {
                if (newValue == "") {
                    oEditorInput.value = "";
                    oEditorInput.title = "";
                    if ($find(this.Editor.id + '_ext')) $find(this.Editor.id + '_ext').initializeCompletionList(oEditorInput);
                } else {
                    if (newValue != this.nullMsg) {
                        oEditorInput.value = newValue;
                        oEditorInput.title = newValue;
                    }
                }
                if (typeof (agregandoDesdeExcel) == 'undefined' || !agregandoDesdeExcel) VaciarCtrlsDependientes(this.Editor);
            }
            break;
        case 121: //Unidad Organizativa
            if (newValue == "") {
                this.Editor.setValue("");
                this.Editor.title = "";
            } else {
                this.Editor.setValue(newValue);
                this.Editor.elem.title = newValue;
            }

            if (this.dependentfield) {
                //Vaciar Departamento. vaciarDropDown est� en alta/js/jsAlta.js
                vaciarDropDown(this.dependentfield);
            }

            break;
        case 123: //Organizacion de Compras
            SetValueWebDropdown(this, newValue);
            if (newValue == "") {
                this.Editor.title = ""

                if (this.idDataEntryDependent2 != null) {
                    vaciarDropDown(this.idDataEntryDependent2);
                }
            }
            break;
        case 124: //Centro
            SetValueWebDropdown(this, newValue)
            if (newValue == "") {
                this.setDataValue("")

                if (this.idDataEntryDependent3 != null) {
                    vaciarDropDown(this.idDataEntryDependent3);
                }
            }
            break;
        case 109: //Destino
            SetValueWebDropdown(this, newValue)
            if (newValue == "") {
                this.setDataValue("")

                if (this.idDataEntryDependent3 != null) {
                    vaciarDropDown(this.idDataEntryDependent3);
                }
            }
            break;
        case 107: //Pa�s
            SetValueWebDropdown(this, newValue);
            if (this.dependentfield) {
                //Vaciar Provincia. vaciarDropDown est� en alta/js/jsAlta.js
                vaciarDropDown(this.dependentfield);
            }
            break;
        case 116: //ProveContacto
            this.Editor.setValue(newValue)
            this.Editor.elem.title = newValue
            break;
        case 120: //ISG NumSolicitudERP
            this.Editor.value = newValue
            this.Editor.elem.title = newValue
            break;
        case 143: //Proveedor ERP
            SetValueWebDropdown(this, newValue);
            if (newValue == "")
                this.setDataValue("")
            break;
        default:
            switch (this.tipo) {
                case 3:
                    if ((this.intro == 1) && (!this.readOnly)) //fecha, tipo lista y solo lectura
                        this.Editor.value = newValue
                    else if (this.intro == 1) //fecha, tipo lista
                        SetValueWebDropdown(this, newValue)
                    else //fecha
                        this.Editor.setValue(newValue)
                    break;
                case 6: //tipo texto medio y largo
                case 7: //TipoGs=2000 (Desglose Vinculado)
                    switch (this.intro) {
                        case 1:
                            if (!this.readOnly) {
                                SetValueWebDropdown(this, newValue)
                            } else {
                                this.Editor.setValue(newValue)
                                this.Editor.elem.title = newValue
                            }
                            break;
                        case 2:
                            var idEditorInput = this.Editor.id + '_t';
                            var oEditorInput = document.getElementById(idEditorInput);
                            if (oEditorInput) {
                                oEditorInput.value = newValue;
                                oEditorInput.title = newValue;
                            }
                            break;
                        default:
                            if (this.intro == 0 && this.tipoGS != 2000 && this.tipoGS != 3000)
                                if (((this.tipoGS == 0) && (this.TablaExterna == 0)) || (this.tipoGS == 1) || (this.tipoGS == 2) || (this.tipoGS == 126) || (this.tipoGS == 133) || (this.tipoGS == 134)
                                || (this.tipoGS == 20) || (this.tipoGS == 23) || (this.tipoGS == 24) || (this.tipoGS == 25) || (this.tipoGS == 26) || (this.tipoGS == 30) || (this.tipoGS == 31)
                                || (this.tipoGS == 32) || (this.tipoGS == 35) || (this.tipoGS == 38) || (this.tipoGS == 39) || (this.tipoGS == 1020)) {
                                    this.Editor.value = newValue
                                } else {
                                    this.Editor.setValue(newValue)
                                    this.Editor.elem.title = newValue
                                }
                            else
                                if (this.tipoGS == 2000) {
                                    if (newValue == null)
                                        newValue = ''
                                    this.Editor.setValue(newValue)
                                    this.Editor.elem.title = newValue
                                } else {
                                    this.Editor.value = newValue
                                }
                    }
                    break;
                case 15:
                case 8:
                    if (this.PoseeDefecto == true && newValue != '') {
                        this.Editor.innerHTML = newValue
                        this.Editor.alt = newValue
                    }
                    else {
                        if (newValue == null) newValue = ''
                        this.Editor.innerHTML = newValue
                        this.Editor.alt = newValue
                    }
                    break;
                case 10:
                    if (newValue == true) {
                        this.Editor.checked = true
                        this.Editor.value = true
                    }
                    else {
                        this.Editor.checked = false
                        this.Editor.value = false
                    }
                    break;
                default:
                    /*
                    && ((this.tipoGS == 0) || (this.tipoGS == 46) || 115 ....)
                    EstadoNoConf	42
                    Subtipo     46                    
                    FormaPago	101
                    Moneda	102
                    Unidad	105
                    Pais	107
                    Provincia	108
                    Destino	    109
                    Contacto	114
                    Persona	    115                    
                    Departamento	122
                    Almac�n     125
                    */
                    if ((this.tipoGS != 119) && (this.tipoGS != 118) && (this.tipoGS != 104) && (this.tipoGS != 127) && (this.tipoGS != 121) && (this.tipoGS != 116) && (this.intro == 1) && (!this.readOnly))
                        SetValueWebDropdown(this, newValue)
                    else {
                        if (this.RecordarValores == true) {
                            this.Editor.value = newValue
                        } else {
                            if ((this.intro == 1) && (this.readOnly == false)) {
                                SetValueWebDropdown(this, newValue)
                            } else {
                                this.Editor.setValue(newValue)
                                this.Editor.elem.title = newValue
                            }
                        }
                    }
            }

            if (this.Hidden && this.TablaExterna > 0)
                this.Hidden.value = newValue
    }
}
//Revisado por: sra. Fecha: 12/07/2012
//funci�n que establece el indice del WebdropDown
//dropDown: Dataentry Webdropdow
//newValue: Valor a establecer
function SetIndexWebDropdown(dropDown, newValue) {
    var oControl = $find(dropDown.id + "__t");
    if (oControl) {
        var items = oControl.get_items();
        var index = -1
        for (var i = 0; i < items.getLength() ; i++) {
            var item = items.getItem(i);
            if (item.get_value() == newValue) {
                index = i;
                break;
            }
        }
        oControl.set_selectedItemIndex(index);
    }
}
//Revisado por: dsl. Fecha: 17/02/2012
//funcion que establece el valor del WebdropDown
//dropDown: Dataentry Webdropdow
//newValue: Valor a establecer
function SetValueWebDropdown(dropDown, newValue) {
    dropDown.Editor.value = newValue
    var oControl = $find(dropDown.id + "__t");
    if (oControl && oControl.set_currentValue && (newValue != undefined)) {
        //Los combos de listas enlazadas a otras listas dan problemas al actualizar el valro mostrado
        oControl.set_currentValue(newValue, true)
    }
    if (dropDown.idCampoHijaListaEnlazada != 0) {
        if (oControl && oControl._thisType && oControl._thisType == "dropDown") { //&& this.intro == 1
            var arrAux = dropDown.id.split("_")
            if (arrAux[1] == '')
                var sIdHijo = arrAux[0] + '__' + arrAux[2] + '_' + arrAux[3] + "_" + 'fsentry' + dropDown.idCampoHijaListaEnlazada;   //uwtGrupos_ctl01_578_fsentry28277"
            else
                sIdHijo = arrAux[0] + '_' + arrAux[1] + '_' + arrAux[2] + "_" + 'fsentry' + dropDown.idCampoHijaListaEnlazada;   //uwtGrupos_ctl01_578_fsentry28277"
            arrAux = null;
            //Vaciar dropdown dependiente. vaciarDropDown est� en alta/js/jsAlta.js
            vaciarDropDown(sIdHijo);
        }
    }
    oControl = null;
    if (dropDown.Hidden && dropDown.TablaExterna > 0)
        dropDown.Hidden.value = newValue
}
/* Revisado por: blp. Fecha: 04/10/2011
''' <summary>
''' Devolver el valor de un entry.
''' Problema (0 == "") esta dando true. Por lo q al copiar fila para numericos cuyo valor es 0, los esta limpiando.
''' </summary>
''' <param name="newValue">nuevo valor, parametro sistema. No es lo devuelve la funci�n</param>
''' <param name="bFireEvent">evento, parametro sistema</param>        
''' <returns>El valor de un entry</returns>
''' <remarks>Llamada desde: Cualquier javascript q use entry; Tiempo m�ximo: 0</remarks>*/
function fsGeneralEntry_GetValue(newValue, bFireEvent) {
    var ret;
    switch (this.tipoGS) {
        case 100: //Proveedor
            ret = this.Editor.getValue()
            break;
        case 103: //Material
            ret = this.Editor.getValue()
            break;
        case 104:
            ret = this.Editor.getValue() + ' - ' + this.EditorDen.getValue()
            break;
        case 119:  //CodArticulo Solicitudes Nuevas
        case 149:
            //Para obtener el valor hay que acceder al textBox que hay dentro del Editor, que es una tabla
            var idEditorInput = this.Editor.id + '_t';
            var oEditorInput = document.getElementById(idEditorInput);
            if (oEditorInput) {
                ret = oEditorInput.value;
            }
            break;
        case 118:
            //Para obtener el valor hay que acceder al textBox que hay dentro del Editor, que es una tabla
            var idEditorInput = this.EditorDen.id + '_t';
            var oEditorInput = document.getElementById(idEditorInput);
            if (oEditorInput) {
                ret = oEditorInput.value;
            }
            break;
        case 120:
            //ISG NumSolicitERP
            if (this.readOnly)
                ret = this.Hidden.value
            else
                ret = this.Editor.value
            break;
        case 110:
        case 111:
        case 112:
        case 113:
            ret = this.Editor.getValue()
            break;
        case 18:
            if (this.nullMsg == this.Editor.getValue())
                ret = ""
            else
                ret = this.Editor.getValue() + ' - ' + this.EditorDen.getValue()
            break
        case 2000: //Campo "Desglose Vinculado"
            ret = this.Editor.getValue()
            break;
        default:
            switch (this.tipo) {
                case 3:
                    if ((this.intro == 1) && (!this.readOnly))
                        ret = this.Editor.value
                    else
                        ret = this.Editor.getValue()
                    break;
                case 6: //tipo Texto medio y Texto Largo
                case 7:
                    if (this.intro == 2) {
                        var idEditorInput = this.Editor.id + '_t';
                        var oEditorInput = document.getElementById(idEditorInput);
                        if (oEditorInput) {
                            ret = oEditorInput.value;
                        }
                    }
                    else
                        if (this.intro == 0) {
                            if (((this.tipoGS == 0) && (this.TablaExterna == 0)) || (this.tipoGS == 1) || (this.tipoGS == 2) || (this.tipoGS == 126) || (this.tipoGS == 133) || (this.tipoGS == 134)
                            || (this.tipoGS == 20) || (this.tipoGS == 23) || (this.tipoGS == 24) || (this.tipoGS == 25) || (this.tipoGS == 26) || (this.tipoGS == 30) || (this.tipoGS == 31)
                            || (this.tipoGS == 32) || (this.tipoGS == 35) || (this.tipoGS == 38) || (this.tipoGS == 39) || (this.tipoGS == 1020)) {
                                if (this.readOnly)
                                    ret = this.Hidden.value
                                else
                                    ret = this.Editor.value
                            } else
                                ret = this.Editor.getValue()
                        } else {
                            if (this.readOnly)
                                ret = this.Editor.getValue()
                            else
                                ret = this.Editor.value
                        }
                    break;
                case 15:
                case 8:
                    if (this.idCampoPadre == 'null' || this.idCampoPadre == null || this.idCampoPadre == 0) {
                        ret = this.Editor.value
                    } else {
                        ret = document.getElementById(this.Editor.id).innerHTML
                    }
                    break;
                case 10:
                    if (this.Editor.checked)
                        ret = true
                    else
                        ret = false
                    break;
                case 16:
                    ret = this.Editor.text;
                    break;
                default:
                    /*
                    && ((this.tipoGS == 0) || (this.tipoGS == 46) || 115 ...)
                    EstadoNoConf	42
                    Subtipo     46                    
                    FormaPago	101
                    Moneda	102
                    Unidad	105
                    Pais	107
                    Provincia	108
                    Destino	    109
                    Contacto	114
                    Persona	    115                    
                    Departamento	122
                    */
                    if ((this.tipoGS != 119) && (this.tipoGS != 118) && (this.tipoGS != 104) && (this.tipoGS != 127) && (this.tipoGS != 121) && (this.tipoGS != 116) && (this.intro == 1) && (!this.readOnly))
                        ret = this.Editor.value
                    else
                        if (this.RecordarValores == true) {
                            ret = this.Editor.value
                        } else {
                            ret = this.Editor.getValue()
                        }
            }
            break;
    }

    if (this.isGridEditor) {
        var oCol = this.webGrid.Bands[0].getColumnFromKey("COLHID_" + this.idCampo.toString());
        if (oCol != null)
            this.webGrid.getActiveRow().getCell(oCol.Index).setValue(this.dataValue)
    }
    if (ret == this.nullMsg) {
        //Problema (0 == "") esta dando true. Por lo q al copiar fila para numericos cuyo valor es 0, los esta limpiando.
        if (this.tipo != 2) {
            ret = ""
        }
    }
    return (ret)
}
function fsGeneralEntry_addEventListener(eventName, fListener, oThis) {
    if (eventName == "blur")
        this.Element.onblur = fListener
    else if (eventName == "keydown")
        this.Element.onkeydown = fListener

    return true
}
function fsGeneralEntry_removeEventListener(eventName, fListener) {
    if (eventName == "blur")
        this.Editor.onblur = null
    else if (eventName == "keydown")
        this.Editor.onkeydown = null

    return true
}
function fsGeneralEntry_getRenderedValue(value) {
    switch (this.tipoGS) {
        case 103:
        case 100:
        case 110:
        case 111:
        case 112:
        case 113:
            ret = this.Editor.value
            break;
        case 18:
            if (this.Editor.getValue() == "")
                ret = ""
            else
                ret = this.Editor.getValue() + ' - ' + this.EditorDen.getValue()
            break
        default:
            switch (this.tipo) {
                case 3:
                    ret = this.Editor.getRenderedValue(this.Editor.getValue())
                    break;
                case 8:
                    ret = this.Editor.value
                    break;
                case 10:  //checkbox
                    if (this.Editor.checked)
                        ret = true
                    else
                        ret = false
                    break;
                default:
                    ret = this.Editor.getValue()

            }
            break;
    }

    if (ret == this.nullMsg)
        ret = ""
    return ret
}
function fsVaciaCombo(oEdit, text, oEvent) {
    if (oEvent.event.keyCode == 8 || oEvent.event.keyCode == 46 || oEvent.event.keyCode == 32) {
        if (oEdit.fsEntry.readOnly == false) {
            oEdit.setValue(null)
            oEdit.fsEntry.setValue(null)
            oEdit.fsEntry.setDataValue("")
            if (oEdit.fsEntry.tipoGS == 123) { //Organizacion de Compras
                try {
                    eval("sCodOrgComprasFORM" + oEdit.fsEntry.idCampo + "=" + "'" + "'");
                } catch (err) { }
            }
            if (oEdit.fsEntry.tipoGS == 100) { //Proveedor
                BorrarProveedoresERP(oEdit.fsEntry)
            }
        }
    }

    ddcloseDropDownEvent(oEdit, text, oEvent)
    if (!(oEvent.event.keyCode == 16 || (oEvent.event.keyCode >= 35 && oEvent.event.keyCode <= 40) || oEvent.event.keyCode == 9))
        oEvent.cancel = true
}
/* Revisado por: blp. Fecha: 04/10/2011
''' <summary>
''' Calcula el id a partir de los valores proporcionados
''' </summary>
''' <parameters>
''' id: id a modificar
''' direccion: Direccion
''' </parameters>
''' <remarks>Llamada desde: javascript ; Tiempo m�ximo: 0</remarks>*/
function calcularIdDataEntry(id, direccion) {
    var arrAux = id.split("_")
    if ((arrAux[0] == "ucDesglose") || (id.search("fsdsentry") >= 0)) {
        //Elemento del desglose "ucDesglose_fsdsentry_1_8345"		
        var sInit = id.substr(0, id.search("fsdsentry"));
        var sAux = id.substr(id.search("fsdsentry"), id.length);
        arrAux = sAux.split("_");
        arrAux[2] = parseInt(arrAux[2]) + direccion;
        return sInit + arrAux[0] + "_" + arrAux[1] + "_" + arrAux[2] + "__t";
    } else {
        var sInit = id.substr(0, id.search("fsentry"))
        var sAux = id.substr(id.search("fsentry"), id.length)
        arrAux = sAux.split("__")
        arrAux = arrAux[0].split("fsentry")
        arrAux[1] = parseInt(arrAux[1]) + direccion
        return sInit + "fsentry" + arrAux[1] + "__t";
    }
}
/* Revisado por: blp. Fecha: 04/10/2011
''' <summary>
''' Function que nos devuelve el ID del dataEntry que se encuentra en la celda anterior o posterior
''' </summary>
''' <parameters>
''' id: id del control
''' nColumna: Columna
''' nLinea: Linea
''' portal: Si viene de portal
''' </parameters>
''' <remarks>Llamada desde: javascript ; Tiempo m�ximo: 0</remarks>*/
function calcularIdDataEntryCelda(id, nColumna, nLinea, portal) {
    //Calcular EL N� de CELDA	
    var s = null;
    var mientry = null;
    if (id.search("__t") < 0)
        id = id + "__t"

    var arrAux = id.split("_")

    var LineaReal = nLinea
    var tabla;

    try {
        if ((arrAux[0] == "ucDesglose") || (arrAux[0] == "Desglose")) {
            for (var indice in htControlFilas) {
                //Por lo visto al hacer deleteRow se quita de document.all(arrAux[0] + "_tblDesglose").rows
                //la linea. Por ello la que al entrar era la 2 ahora es la 1 y la 3 es la 2
                //Esto hace que document.all(arrAux[0] + "_tblDesglose").rows[2] me de el identificador de la 
                //linea 3 cuando pido la 2 tras borrar la 1

                //PELIGRO: he recibido para dos lineas diferentes el mismo nLinea. Afortunadamente id era distinto
                if (arrAux[2] == indice.substr(indice.lastIndexOf("_") + 1)) {
                    LineaReal = htControlFilas[indice]
                }
            }
            if (document.getElementById(arrAux[0] + "_tblDesglose").rows[LineaReal])
                s = document.getElementById(arrAux[0] + "_tblDesglose").rows[LineaReal].cells[nColumna].innerHTML

        } else {
            cadBusqueda = "fsentry";
            if (id.search(cadBusqueda) >= 0)
                s = document.getElementById(arrAux[0] + "__" + arrAux[2] + "_" + arrAux[3] + "_tblDesglose").rows[nColumna].cells[3].innerHTML
            else
                if ((arrAux.length >= 9) || (arrAux.length <= 11)) {
                    if (arrAux.length == 9) //cuando no es popup						
                        tabla = arrAux[0] + "__" + arrAux[2] + "_" + arrAux[3] + "_tblDesglose"
                    else
                        if (arrAux.length == 10) { //Cuando el desglose no es una ventana emergente PORTAL
                            nLinea = nLinea + 1  //Por la Cabecera DESGLOSE
                            tabla = arrAux[0] + "__" + arrAux[2] + "_" + arrAux[3] + "_" + arrAux[3] + "_tblDesglose" //Elemento a buscar -->uwtGrupos__ctl0xxxx_8912_8912_tblDesglose								

                        } else
                            if (arrAux.length == 11) { //Cuando el desglose no es una ventana emergente
                                tabla = arrAux[0] + "__" + arrAux[2] + "_" + arrAux[3] + "_" + arrAux[3] + "_" + arrAux[5] + "_tblDesglose" //Elemento a buscar -->uwtGrupos__ctl0xxxxx_830_830_13395_tblDesglose
                                if (portal == 1) {
                                    nLinea = nLinea + 1;
                                }
                            }
                    if (tabla) {
                        LineaReal = nLinea

                        if (arrAux.length == 11) {
                            for (var indice in htControlFilas) {
                                if (arrAux[7] == indice.substr(indice.lastIndexOf("_") + 1)) {
                                    LineaReal = htControlFilas[indice]
                                    if (portal == 1) {
                                        LineaReal = parseInt(LineaReal) + 1
                                    }
                                }
                            }
                        }

                        s = document.getElementById(tabla).rows[LineaReal].cells[nColumna].innerHTML
                    }
                }


        }

        //********************************************************
        //Una vez Obtenido la CELDA, obtener el ID del DataEntry 
        //********************************************************
        var mientry = null;
        if (s) {
            var stemp = s
            var stemp2
            var tempinicio
            if (stemp.search(' id="') > 0) { //En FireFox los ids vienen con comillas
                tempinicio = stemp.search(' id="')
                tempinicio = tempinicio + 5
            } else {
                tempinicio = stemp.search(" id=") //En IE los ids vienen sin comillas
                tempinicio = tempinicio + 4
            }
            stemp2 = stemp.substr(tempinicio, stemp.length - tempinicio)
            var tempfin = stemp2.search("__tbl")
            mientry = stemp2.substr(0, tempfin)
            s = null;
        }

        return mientry

    } catch (err) {
        return null;
    }
}
function VaciarCtrlsDependientes(ctrl) {
    for (i in igedit_all) {
        try {
            if (igedit_all[i].fsEntry.dependentfield == ctrl.fsEntry.id) {
                igedit_all[i].fsEntry.setValue('');
                igedit_all[i].fsEntry.setDataValue('');
                igedit_all[i].codigoArticulo = null;
            }
        }
        catch (e) {
        }
    }
}
/*
''' <summary> 
''' Reeemplazar en un texto dado (es el name de un input) los $ por un texto que no sea caracter reservado de
''' la funci�n search de javascript. El problema q se daba es que nuestra replaceall no encontraba el texto $__
''' y en consecuencia no reemplazaba. En cambio, replaceall, no tiene problemas con ACambiar__
''' </summary>
''' <param name="texto">Texto con los $ a reeemplazar</param>      
''' <returns>Texto con los $ reeemplazados</returns>
''' <remarks>Llamada desde:fsGeneralEntry_AddDesgloseLine; Tiempo m�ximo:0</remarks>
*/
function ReplaceDolar(texto) {
    var sTemp = ""
    var i
    for (i = 0; i < texto.length; i++) {
        if (texto.charAt(i) == "$") {
            sTemp += "ACambiar"
        }
        else {
            sTemp += texto.charAt(i)
        }
    }
    return (sTemp)
}
/* Revisado por: blp. Fecha: 04/10/2011
''' <summary>
''' Desde un desglose popup de nwgestioninstancia hay q mantener una estructura de inputs hidden para controlar las
''' eliminaciones de lineas, movimientos de lineas y q las lineas nuevas tras guardar puedan ser movidas.
''' </summary>     
''' <param name="EsMovible">Indica si la linea es movible o no</param> 
''' <param name="lineaVinc">Numero de linea vinculada contando las movidas y eliminas</param> 
''' <remarks>Llamada desde:script/workflow/desglose.aspx javascript actualizarCampoDesglose; Tiempo m�ximo:0</remarks>
*/
function fsGeneralEntry_AddDesgloseLineVinc(EsMovible, lineaVinc) {
    var s = "";
    if (!document.getElementById(this.id + "__" + lineaVinc.toString() + "__LineaVincMovible")) {
        s = "<INPUT id=" + this.id + "__" + lineaVinc.toString() + "__LineaVincMovible type=hidden name=" + this.name + "$_" + lineaVinc.toString() + "__LineaVincMovible runat=server>"
        this.Editor.insertAdjacentHTML("beforeEnd", s)
    }
    document.getElementById(this.id + "__" + lineaVinc.toString() + "__LineaVincMovible").value = EsMovible

    if (!document.getElementById(this.id + "__" + lineaVinc.toString() + "__LineaVinc")) {
        s = "<INPUT id=" + this.id + "__" + lineaVinc.toString() + "__LineaVinc type=hidden name=" + this.name + "$_" + lineaVinc.toString() + "__LineaVinc value='' runat=server>"
        this.Editor.insertAdjacentHTML("beforeEnd", s)
    }

    if (!document.getElementById(this.id + "__" + lineaVinc.toString() + "__LineaOldVinc")) {
        s = "<INPUT id=" + this.id + "__" + lineaVinc.toString() + "__LineaOldVinc type=hidden name=" + this.name + "$_" + lineaVinc.toString() + "__LineaOldVinc value=0 runat=server>"
        this.Editor.insertAdjacentHTML("beforeEnd", s)
    }
    s = null;
}
function getIdLineaDesglose(oEntry) {
    ///<summary>'Funci�n que devuelve el identificador de la l�nea a la que corresponde el dataentry de tipo desglose</summary>
    ///<param name="oEntry">Objeto entry</param>
    ///<returns type="int">N�mero de linea, -1 si la linea no corresponde a desglose</returns>
    ///<remarks>llamada desde fsGeneralEntry.js, fsGeneralEntry_Setup</remarks>
    var len = oEntry.id.length;
    var i = oEntry.id.search('fsdentry_');
    var ii = oEntry.id.indexOf('fsdsentry');
    i = Math.max(i, ii);
    var s = oEntry.id.substr(i + 10, len - i + 1);
    var _i = s.search('_');
    var f = s.substr(0, _i);
    return f;
}
function isDesglose(oEntry) {
    ///<summary>Funci�n que devuelve si en entry es de tipo desglose</summary>
    ///<param name="oEntry">Objeto entry</param>
    ///<returns type="boolean"></returns>
    var pos = oEntry.id.indexOf('fsdentry');
    if (pos != -1)
        return true;
    else
        return (oEntry.id.indexOf('fsdsentry') != -1);
}