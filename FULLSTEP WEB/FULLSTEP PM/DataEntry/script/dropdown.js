// This file contains java script used by the DropDownGrid.aspx form.
//
// flag to avoid multiple listeners to same mouse-down event
var ddglobalListenerWasCreated = false;
// references to drop-down grids
var dropDownGrids = new Array();
// it is called when drop-down grid is initialized
var vTabContainer
function preInitGrid(id,dataField,displayField,editor, editorField, cloneEditor,cloneField)
{
vTabContainer=id
vDataField = dataField
vDisplayField = displayField
vEditor = editor
vEditorField = editorField
vCloneEditor = cloneEditor
vCloneField = cloneField
}
function ddinitGridEvent(gridName)
{
	var oGrid = igtbl_getGridById(gridName);
	if(oGrid == null)
	{
		alert("Error: \"" + gridName + "\" was not found.");
		return;
	}
	oGrid.Contenedor = vTabContainer
	if (oGrid.esDen)
		{
		oGrid.dataField =vDisplayField
		oGrid.displayField =  vDataField
		}
	else
		{
		oGrid.dataField = vDataField
		oGrid.displayField = vDisplayField
		}
	oGrid.editor = vEditor
	oGrid.editorField = vEditorField
	oGrid.cloneEditor = vCloneEditor
	oGrid.cloneField = vCloneField
	oGrid.mainElement = ig_csom.getElementById(gridName + "_main");
	if(oGrid.mainElement == null)
	{
		alert("Error: \"" + gridName + "_main\" was not found.");
		return;
	}
	oGrid.gridName=gridName
	// ensure absolute position
	// Note: it can be skipped if style in aspx has that
	oGrid.mainElement.style.position = "absolute";
	// assume that grid on start is visible
	// create boolean member variable, which simplifies visibility test
	oGrid.isDisplayed = true;
	// global cash of grid-reference
	for (i=0;i<dropDownGrids.length;i++)
		if (dropDownGrids[i].gridName==oGrid.gridName)
			break;
	dropDownGrids[i] = oGrid;
	// hide drop-down grid
	ddshowDropDown(oGrid, null, false);
}

/*
''' <summary>
''' it is called by date click events of UltraWebGrid
''' Note: that name should match with the ClientSideEvents.CellClickHandler property
''' which is set in aspx for UltraWebGrid
''' </summary>
''' <param name="gridName">nombre del grid donde cliqueaste</param>
''' <param name="cellId">id de la celda</param>        
''' <param name="button">bot�n pulsado</param>   
''' <remarks>Llamada desde: Todo pantalla con un grid con celdas cliqueables; Tiempo m�ximo:0</remarks>*/
function ddcellClickEvent(gridName, cellId, button)
{
	var oGrid = igtbl_getGridById(gridName);
	var oCell = igtbl_getCellById(cellId);
	if(oGrid == null || oCell == null)
	{
		alert("Error: \"" + gridName + "\" was not found.");
		return;
    }

    var TextAntes = oGrid.oEdit.text
    
	// update editor with latest text and hide grid
	var oRow = igtbl_getRowById(cellId);

	ddshowDropDown(oGrid, oRow.getCell(oGrid.displayField).getValue(), false, true);

	var TextDespues = oGrid.oEdit.text

	try {
	    if (oGrid.oEdit.fsEntry.CtrlCambioRechazo == true) {
	        if (TextAntes != TextDespues) {
	            fCambioNc(oGrid.oEdit, "", null);
	        }
	    }
	}
	catch (e) { }		
}
// it is called by custom-button click event of WebTextEdit
// Note: that name should match with the ClientSideEvents.CustomButtonPress property
//  which is set in aspx for WebTextEdit
function ddopenDropDownEvent(oEdit, text, oEvent)
{	

var i
	// open drop-down grid
	for (i=0;i<dropDownGrids.length ;i++)
	{
		ddshowDropDown(dropDownGrids[i], null, false);
	}
	ctlEdit = replaceAll(oEdit.ID,"__tden","")
	if (ctlEdit!=oEdit.ID)
		esDen = true
	else
		esDen = false
	ctlEdit = replaceAll(ctlEdit,"__t","")
	ctlEdit = replaceAll(ctlEdit,"__","x")
	ctlEdit = replaceAll(ctlEdit,"_","")
	for (i=0;i<dropDownGrids.length && replaceAll(dropDownGrids[i].gridName,"x","").search(replaceAll(oEdit.idGrid,"x","")) !=0;i++)
		{
		}
	if (dropDownGrids.length>0)
	   dropDownGrids[i].esDen = esDen
	   
	ddopenDropDown(oEdit, dropDownGrids[i]);
}
// it is called by spin and focus events of WebTextEdit
// Note: that name should match with the ClientSideEvents.KeyDown/Spin/Focus/etc. properties
//  which are set in aspx for WebTextEdit
function ddcloseDropDownEvent(oEdit, text, oEvent)
{
	// hide grid
	ddshowDropDown(oEdit.oGrid, null, false);
}
// open grid and attach it to WebTextEdit
// oEdit - reference to the owner of grid (WebTextEdit)
// oGrid - the UltraWebGrid which should be dropped and attached to oEdit
function ddopenDropDown(oEdit, oGrid)
{

	if(oGrid == null) return;
	// add listener to mouse click events for page
	if(!ddglobalListenerWasCreated)
		ig_csom.addEventListener(window.document, "mousedown", ddglobalMouseDown, false);
	ddglobalListenerWasCreated = true;
	// set reference of grid to editor:
	// create member variable, which points to drop-down grid
	oEdit.oGrid = oGrid;
	// if it belongs to another oEdit, then close oGrid
	if(oGrid.oEdit != oEdit)
	{
		ddshowDropDown(oGrid, null, false);
		// set reference in oGrid to this oEdit
		// create member variable, which points to the owner oEdit
	}
	oGrid.oEdit = oEdit;
	// show grid with text from editor
	// if grid is already opened, then hide grid (last param)
	ddshowDropDown(oGrid, oEdit.getText(), true, true, true);
}
// synchronize text in TextEdit with selected cell in grid
// and show/close grid
function ddshowDropDown(oGrid, text, show, update, toggle)
{
	if(oGrid == null) return;
	if(toggle == true && oGrid.isDisplayed == true)
		show = update = false;
	// update editor with latest text

	if(update == true)
	{
		if(oGrid.isDisplayed )
			{
			oGrid.oEdit.setText(text);

			if (oGridsDesglose)
			{
			
				for (i=0;i<oGridsDesglose.length;i++)
					{
					sAux = oGridsDesglose[i].IdEntryMaterial
					if (sAux)
						{
						re=/fsentry/
						sGridAux = sAux
						while (sGridAux.search(re)>=0)
							sGridAux = sGridAux.replace(re,"")
						sGridAux = "_" + sGridAux + "_"
						if (oGrid.oEdit.ID.search(sAux)>0)
							{
							sAux = oGrid.oEdit.ID
							if (sAux.search("fsdsentry")>=0)
								{
								sAux = sAux.substr(sAux.search("fsdsentry"), sAux.length)
								arrAux = sAux.split("_")
								lIndex = arrAux[1]
								}
							else
								{
								sAux = sAux.substr(sAux.search("fsentry"), sAux.length)
								lIndex ="-1"
								}


							vGrid = oGridsDesglose[i]
							sDropDown = vGrid.Editor
							if (lIndex=="-1")
								sDropDown= sDropDown + "_dd"
							else
								sDropDown= sDropDown.replace("fsentry","fsdsentry_" + lIndex.toString() + "_") + "_dd"
							
							while (sDropDown.search("__")>0)
								sDropDown=sDropDown.replace("__","x")
							while (sDropDown.search("_")>0)
								sDropDown=sDropDown.replace("_","x")
							dropDownGrid = igtbl_getGridById(sDropDown)
							if (dropDownGrid) 
								{
								dropDownGrid.sustituir=true
								}
							break;
							}
						}
					}
			}

			oRow = oGrid.getActiveRow()

			if (oRow) {
			    if (oGrid.oEdit.fsEntry.tipoGS == 105) {
			        //para el n� de decimales de la unidad
			        var NumDec;
			        var DenUni;
			        var oCell = oRow.getCellFromKey("NUMDEC");
			        NumDec = oCell.getValue();
			        oCell = oRow.getCell(oGrid.displayField);
			        DenUni = oCell.getValue();
			        if (oGrid.oEdit.fsEntry.idEntryCANT != null) {
			            o = fsGeneralEntry_getById(oGrid.oEdit.fsEntry.idEntryCANT)
			            if (o) {
			                o.NumeroDeDecimales = NumDec;
			                o.UnidadOculta = DenUni;
			                limiteDecimalesTextChanged(o.Editor, o.Editor.text, null);
			            }
			        }
			    }
                
			
				if (oGrid.oEdit.fsEntry.Hidden)
					{
					
					c = oRow.getCell(oGrid.displayField)
					oGrid.oEdit.setValue(c.getValue());
					if (oGrid.oEdit.fsEntry.tipo==4)
						oGrid.oEdit.fsEntry.Hidden.value = (oRow.getCell(oGrid.dataField).getValue()?1:0)
					else
						oGrid.oEdit.fsEntry.Hidden.value = oRow.getCell(oGrid.dataField).getValue()
					}
				else
					{
					c = oRow.getCell(oGrid.displayField)
					oGrid.oEdit.setValue(c.getValue());				
					}
			}
			if (oGrid.cloneEditor)
				{
					oCloneEditor = igedit_getById(oGrid.editor)
					oRow = oGrid.getActiveRow()
					c = oRow.getCell(oGrid.dataField)
					oCloneEditor.setValue(c.getValue())
					oCloneEditor = igedit_getById(oGrid.cloneEditor)
					oRow = oGrid.getActiveRow()
					c = oRow.getCell(oGrid.cloneField)
					oCloneEditor.setValue(c.getValue())
				}
			}
		else
		{
			// find cell in grid and select it
			var r = null, c = null, mr=null;
			for(var row = 0; row < 1000; row++)
			{
				if((r = oGrid.Rows.getRow(row)) == null) break;
				c = r.getCell(oGrid.displayField)
				if(c.getValue() == text){row = 1000; mr=r; break;}
			}
			if(mr != null)
			{
				mr.setSelected(true);
				mr.activate();

			}
			else
			{
				oGrid.clearSelectionAll();
				oGrid.setActiveCell(null);
			}
		}
	}
	// check current state of grid
	if(oGrid.isDisplayed == show)
		return;
	if (show) ddglobalMouseDown();
	// show/hide grid
	oGrid.mainElement.style.display = show ? "block" : "none";
	oGrid.mainElement.style.visibility = show ? "visible" : "hidden";
	oGrid.isDisplayed = show;
	if(show)
		ddpositionGrid(oGrid);
}

/*
''' <summary>
''' set position of grid below/above TextEdit
''' </summary>
''' <param name="oGrid">Combo a mostrar</param>      
''' <returns>Nada</returns>
''' <remarks>Llamada desde: common/js/jsalta.js	 dataentry/dropdrown.js; Tiempo m�ximo: 0</remarks>
*/
function ddpositionGrid(oGrid)
{
	//Por algun motivo, por mi desconocido, si se pone un grid de desglose que no sea popup en el 1er grupo  
	//el parentElement de unas columnas (las que cogen de bbdd) es el DIV del desglose y de otras columnas 
	//(las de si/no y lista, es decir, las de no de bbdd) es un TD. Ello hace que el, por ejemplo, 
	//style.top="323px" de uno y otro posicione en distinto punto el desplegable.
	//El desfase en el posicionamiento coordena TOP es la altura del propio grid menos la altura de la fila 
	//de titulos de columnas.
	//El desfase en el posicionamiento coordena LEFT es lo que este a la izquierda el propio grid
	//
	//Algo parecido pasa con los combos, que no sean de desglose ni tomen datos de bbdd, cuando entra en juego la
	//barra de desplazamiento. Esta vez la coordena LEFT esta bien, pero la coordenada TOP se ha visto decrecida
	//en el scrolltop del control uwtGrupos_div0 no siendo necesario esta resta, por eso posicionaba por arriba 
	//el combo.
	
	var AlturaGrid=213;
	var AlturaMedioGrid;
	var AlturaCabLin=24;
	var MovidaSiNo="";
	var NumMovidaSiNo=0;
	var ScrollMovidaSiNo=0;
	var PosicionMovidaSiNo=235;
	var TopGridMovidaSiNo=oGrid.mainElement.parentElement.offsetTop;
	var NumFilas=1;
	var LeftGrid=19;
	var scrollPantalla=0;
		
	var elem = oGrid.oEdit.Element;
	var jpaelem = oGrid.oEdit.Element;
	// left and top position of grid
	var x = 0, y = elem.offsetHeight;
	
	if(y == null) y = 0;
	
	//______________________________________________________________
	//calculo la y para jpa
	var jpa = jpaelem.offsetHeight;
	
	if(jpa == null) jpa = 0;

	while(jpaelem != null)
	{
		//alert("jpaelem.id: " + jpaelem.id)					    	
		if (oGrid.mainElement.parentElement.id == jpaelem.id && oGrid.mainElement.parentElement.id!="")
			jpaelem =null
		else																												
			if (jpaelem.id.search("uwtGrupos_div")!=-1)
				jpaelem =null
			else																																							
					{
					if (jpaelem.id != "tblRptParams")
						{if(jpaelem.offsetTop != null) jpa += jpaelem.offsetTop;}
					else
						{jpa+=2;}																				

					jpaelem = jpaelem.offsetParent;
					}		
	}																								
	//______________________________________________________________
									
	// width and height of super parent (document)
	var w = 0, h = 0;
	var noMas
	noMas = false
	//
	NumMovidaSiNo=oGrid.oEdit.ID.search("fsdsentry_")+10
	MovidaSiNo=oGrid.oEdit.fsEntry.id.substr(NumMovidaSiNo)
	NumMovidaSiNo=MovidaSiNo.search("_")
	MovidaSiNo=MovidaSiNo.substr(0,NumMovidaSiNo)
	//
	var BorradasComprobar=0
	if(oGrid.oEdit.Element.document.URLUnencoded.search("altacertificado.aspx")!=-1)
	{
		BorradasComprobar=1
	}
	if(oGrid.oEdit.Element.document.URLUnencoded.search("certificado.aspx")!=-1)
	{
		BorradasComprobar=1
	}
	if(oGrid.oEdit.Element.document.URLUnencoded.search("noconformidad.aspx")!=-1)
	{
		BorradasComprobar=1
	}	
	if(oGrid.oEdit.Element.document.URLUnencoded.search("altaNoConformidad.aspx")!=-1)
	{
	    BorradasComprobar = 1
	    LeftGrid = 7
	}
	if(oGrid.oEdit.Element.document.URLUnencoded.search("detalleNoConformidad.aspx")!=-1)
	{
	    BorradasComprobar = 1
	    LeftGrid = 10
	}
	if ((oGrid.oEdit.Element.document.URLUnencoded.toLowerCase().search("altacontrato.aspx") != -1) || (oGrid.oEdit.Element.document.URLUnencoded.toLowerCase().search("nwalta.aspx") != -1) || (oGrid.oEdit.Element.document.URLUnencoded.toLowerCase().search("nwdetallesolicitud.aspx") != -1) || (oGrid.oEdit.Element.document.URLUnencoded.toLowerCase().search("nwgestioninstancia.aspx") != -1) || (oGrid.oEdit.Element.document.URLUnencoded.toLowerCase().search("gestiontrasladada.aspx") != -1) || (oGrid.oEdit.Element.document.URLUnencoded.toLowerCase().search("aprobacion.aspx") != -1)) {
	    BorradasComprobar = 1
	}
	
	if (BorradasComprobar==1)
	{
		var indice=1
		var ifinal=MovidaSiNo
		
		for (indice;indice < ifinal;indice++)
		{
			LineaCertificado=oGrid.oEdit.ID.replace("fsdsentry_" + ifinal,"fsdsentry_" + indice)
			if (!document.all(LineaCertificado))
			{
				MovidaSiNo=MovidaSiNo-1
			}
		}
	}
	
	//
	while(elem != null)
	{
		
		if (oGrid.mainElement.parentElement.id == elem.id && oGrid.mainElement.parentElement.id!="")
			elem =null
		else
			{
			if (elem.id != "tblRptParams") {
				if(elem.offsetLeft != null) x += elem.offsetLeft;
				if(elem.offsetTop != null) y += elem.offsetTop;
				h = elem.offsetHeight;
				w = elem.offsetWidth;
			}
			else
			{
				x+=2;
				y+=2;
			}
			
			if (elem.id.search("_divDesglose") != -1){
				AlturaGrid=elem.offsetHeight;
			}			

			if (elem.id.search("_tblDesglose") != -1){
				var tbpositionTablaDesglose = findPositionWithScrolling(elem);
				if (elem.innerHTML.search("_TituloDesglose") != -1){
					PosicionMovidaSiNo=260;
				}
			}
			
			if (elem.tagName=="TABLE"){
				if (elem.id.search("_tblDesglose") != -1){
					if (elem.parentElement.id.search("_divDesglose") != -1){
						AlturaCabLin=elem.offsetHeight;
						NumFilas=1;
						while (NumFilas<elem.rows.length){
							AlturaCabLin=AlturaCabLin-25;		//fila dato		
							NumFilas=NumFilas+1;		
						}
						AlturaCabLin=AlturaCabLin-24;	//fila cabecera 1 lin. Cada linea adicional es 6px
					}
				}				
			}
			
			elem = elem.offsetParent;
			}		
	}
	
	AlturaCabLin=24+AlturaCabLin+2;
	
	PosicionMovidaSiNo=PosicionMovidaSiNo+TopGridMovidaSiNo;
	
	elem = oGrid.oEdit.Element;

	while(elem != null)
	{
	x-=elem.scrollLeft
	elem = elem.offsetParent;
	
	}
		
	elem = oGrid.Element;

	var alturaScrolls = 0;
	while(elem != null)
	{
		if (elem.tagName != "DIV")
			alturaScrolls+=elem.scrollTop;
		elem = elem.offsetParent;
	
	}	

	elem = oGrid.oEdit.Element;

	while(elem != null)
	{
		if ((oGrid.oEdit.fsEntry.tipoGS != 0) && (oGrid.oEdit.fsEntry.tipoGS != 46)){
			y-=elem.scrollTop;		
			ScrollMovidaSiNo=ScrollMovidaSiNo+elem.scrollTop;			
		}
		else {
			if (elem.id.search("uwtGrupos_div")==-1){
				y-=elem.scrollTop;	
				if (elem.id != ""){	
					ScrollMovidaSiNo=ScrollMovidaSiNo+elem.scrollTop;
				}
			}
		}		
		if (elem.id.search("uwtGrupos_div") != -1){
			scrollPantalla=elem.scrollTop;
		}
		elem = elem.offsetParent;
		
	}
	
	// check if grid fits below editor
	// if not, then move it above editor
	elem = oGrid.mainElement;
	elem.style.backgroundColor="white"
	oGrid.mainElement.style.zIndex=2000

	//jpa Incidencia 209
	//La anchura del frame mostrado sera la misma que la del dataentry	a no ser que sea Tipo 4 (booleano)

		if (oGrid.oEdit.fsEntry.tipo==4)	
			oGrid.mainElement.style.width= "100px"
		else	
			oGrid.mainElement.style.width= oGrid.oEdit.Element.offsetWidth
	
	//fin jpa Incidencia 209

	//jpa Incidencias 168 y 7806 
    //jpa debo controlar si estoy en un popup    
    if(oGrid.oEdit.Element.document.URLUnencoded.search("desglose.aspx")!=-1)
    {				
		//1� POP-UP   ----------------------------------------------------------------------------------------------------
		//alert("es un popup")
		var num5 = y
		num5 += oGrid.oEdit.Element.offsetHeight
		num5 += elem.offsetHeight
				
		var num6 = window.document.body.offsetHeight		    
	
		if (num5>num6)
		{	 y =99	}
	}
	else
    {				
		//2� NO ES POP-UP   ----------------------------------------------------------------------------------------------------
		//alert("NO es un popup")
		if (vTabContainer == '')
		{
			//3� NO ES CONTAINER   ----------------------------------------------------------------------------------------------------
			//alert("NO es un CONTAINER")
		}
		else   
		{
				//4� ES CONTAINER   ----------------------------------------------------------------------------------------------------
				//alert("es un CONTAINER")
				if (elem.id.search("uwtGrupos_div")==-1)
					{
					  //5� ESTA EN UN GRUPO  ----------------------------------------------------------------------------------------------------										
					  //alert("ESTA EN UN GRUPO")					  
						if (oGrid.oEdit.fsEntry.id.search("fsdsentry_")!=-1)						
						{						 
							//6� ESTA EN UN DESGLOSE  ----------------------------------------------------------------------------------------------------																	
							//elemento --------------------------------
							var oObject =  elem
							
							//el padre --------------------------------							
							var oParent = oObject.offsetParent
							var iClientHeight = oParent.clientHeight;							
																					
							//la altura del elemento
							var num12 = y
							//la altura del campo						
							num12 += oGrid.oEdit.Element.offsetHeight
							//la altura del desplegable
							num12 += elem.offsetHeight						
							num12 += oGrid.mainElement.parentElement.scrollTop
														
							if (iClientHeight<num12)		
							{	 								
								y -= (elem.offsetHeight + oGrid.oEdit.Element.offsetHeight)
							}														
						}					  
						else
						{
							if (oGrid.oEdit.fsEntry.id.search("fsentry_List_")!=-1)	
								{
								}								
							else
							{
								//--------------------------------------------------------------------------------------------------------------------------------------------						
								//7� CASO NORMAL							
								
								var num0 = jpa
								num0 -= oGrid.mainElement.parentElement.scrollTop								
								num0 = Math.abs(num0)									
								
								var num1 = num0
								num1 +=     elem.offsetHeight								
								num1 +=     oGrid.oEdit.Element.offsetHeight							
								num1 = Math.abs(num1)											

							    var altura
								//
								elem = oGrid.oEdit.Element;
								while(elem != null){
									if (elem.id.search("uwtGrupos_div")!=-1){
										altura=elem.offsetHeight;
										elem=null
									}
									else{
										elem = elem.offsetParent;
									}
								}			
								//
							
								elem = oGrid.mainElement;
								
								var num2 = altura -  num1							
								
									if (num2<0)
										{
											//si supera la altura del tab entra   
											y -= (elem.offsetHeight + oGrid.oEdit.Element.offsetHeight)
											// check if grid fits on the right from editor
											// if not, then move it to the left
											// 20 - extra shift for possible vertical scrollbar in browser
											//if (x + elem.offsetWidth + 20 > w + window.document.body.scrollLeft)
											//	{
											//		x = w - elem.offsetWidth - 2 + window.document.body.scrollLeft													
											//	}
										}
								//}							
								//--------------------------------------------------------------------------------------------------------------------------------------------
							}	
						}					  						
					}
					
		}		
	}	    
    //fin jpa Incidencias
	
	var elem1 = oGrid.oEdit.Element;
	var tbposition = findPositionWithScrolling(elem1);
	var posx = tbposition[0]
	var posy = tbposition[1]
	var bPortal = false;
	if (oGrid.oEdit.Element.document.URLUnencoded.search("/PMPortal") != -1 && oGrid.oEdit.Element.document.URLUnencoded.search("certificado") == -1) //Comprueba si esta en portal
		bPortal = true;

	var bRePosicionar = 0;
	var CtrlbRePosicionar = 0;
	if(oGrid.oEdit.Element.document.URLUnencoded.search("desglose.aspx")==-1){
		CtrlbRePosicionar = uwtGrupos_div0.offsetHeight;
		if (CtrlbRePosicionar != 0)
			if (oGrid.mainElement.style.left =="")
				if (oGrid.mainElement.style.visibility == "visible")
					if (oGrid.oEdit.fsEntry.tipo!=4)
						if (((oGrid.mainElement.parentElement.tagName == "TD") && (oGrid.oEdit.fsEntry.id.substr(0,13) == "fsentry_List_")) ||(oGrid.mainElement.parentElement.tagName != "TD"))
							bRePosicionar= 1;	
	}
								
	if (bRePosicionar == 1)
	{
		oGrid.mainElement.style.position="absolute"
		oGrid.mainElement.style.left = x + "px";
		oGrid.mainElement.style.top = y +alturaScrolls+ "px";
		
		oGrid.mainElement.style.visibility= "hidden";
		
		ddpositionGrid(oGrid);
		
		oGrid.mainElement.style.visibility= "visible";	
	}
	else
	{
		oGrid.mainElement.style.position="absolute"
		
		if(bPortal==true)
		{
			oGrid.mainElement.style.top = posy  + "px";	
			oGrid.mainElement.style.left = posx - 12 + "px";
		}else{
			oGrid.mainElement.style.top = posy  + 23 +  "px";
			oGrid.mainElement.style.left = posx + "px";
			
		}	
	}
	
	if ((oGrid.mainElement.parentElement.tagName == "TD") && (oGrid.oEdit.fsEntry.id.substr(0,13) != "fsentry_List_")){
		if (oGrid.oEdit.ID.search("ucDesglose_")==-1){
		//Ahora si es el portal y seguimiento entra por aqui siendo popup. Y no deber�a.	
			if(oGrid.oEdit.Element.document.URLUnencoded.search("desglose.aspx")==-1){				
											
				NumMovidaSiNo=PosicionMovidaSiNo+(25*(MovidaSiNo-1))-ScrollMovidaSiNo-scrollPantalla		
				if (AlturaCabLin>26){
					NumMovidaSiNo=NumMovidaSiNo+(AlturaCabLin-26)
				}
				y=NumMovidaSiNo
				
				EsQaOtroCalc=0
				
				if(oGrid.oEdit.Element.document.URLUnencoded.search("/PMPortal/")!=-1){					
					y=y+6
					if(oGrid.oEdit.Element.document.URLUnencoded.search("noconformidad.aspx")!=-1){
						y=y+6
						LeftGrid=26	
						EsQaOtroCalc=1
					}
					else
					{
						if(oGrid.oEdit.Element.document.URLUnencoded.search("certificado.aspx")!=-1)
						{	
							EsQaOtroCalc=1
						}						
						else
						{
							LeftGrid=32
						}
					}
					
					if (EsQaOtroCalc==1)
					{											
						if (AlturaGrid >237)
						{
							y=y+25-6
						}
						else
						{
							y=y-(213-AlturaGrid)-12
						}													
					}	
				}		
				
				EsQaOtroCalc=0
				
				if(oGrid.oEdit.Element.document.URLUnencoded.search("altacertificado.aspx")!=-1)
				{
					EsQaOtroCalc=1
				}
				if (oGrid.oEdit.Element.document.URLUnencoded.search("altaNoConformidad.aspx")!=-1)
				{
					EsQaOtroCalc=1
				}			
				if(oGrid.oEdit.Element.document.URLUnencoded.search("detalleNoConformidad.aspx")!=-1)
				{
					EsQaOtroCalc=1
	            }
	            if ((oGrid.oEdit.Element.document.URLUnencoded.toLowerCase().search("altacontrato.aspx") != -1) || (oGrid.oEdit.Element.document.URLUnencoded.toLowerCase().search("nwalta.aspx") != -1) || (oGrid.oEdit.Element.document.URLUnencoded.toLowerCase().search("nwdetallesolicitud.aspx") != -1) || (oGrid.oEdit.Element.document.URLUnencoded.toLowerCase().search("nwgestioninstancia.aspx") != -1) || (oGrid.oEdit.Element.document.URLUnencoded.toLowerCase().search("gestiontrasladada.aspx") != -1) || (oGrid.oEdit.Element.document.URLUnencoded.toLowerCase().search("aprobacion.aspx") != -1)) {
	                EsQaOtroCalc = 1
	            }
									
				if (EsQaOtroCalc==1)
				{
					y=y+6
										
					if (AlturaGrid >237)
					{
						y=y+25
					}
					else
					{
						y=y-(213-AlturaGrid)-6
					}						
				}
						
					
						
				y = y - AlturaGrid + scrollPantalla + 28;
				
				if(bPortal==true)
				{
					oGrid.mainElement.style.top = posy  + "px";
				    oGrid.mainElement.style.left = x - 12 +"px";		
				}else{
			    	oGrid.mainElement.style.top = posy + 23 + "px";
				    oGrid.mainElement.style.left = x+"px";		
				}
			}
		}
	}
}


//funcion que devuelve la posicion de un elemento
function findPositionWithScrolling(oElement) {
    if (typeof (oElement.offsetParent) != 'undefined') {
        var originalElement = oElement;
        for (var posX = 0, posY = 0; oElement; oElement = oElement.offsetParent) {
            posX += oElement.offsetLeft;
            posY += oElement.offsetTop;
            if (oElement != originalElement && oElement != document.body && oElement != document.documentElement) {
                posX -= oElement.scrollLeft;
                posY -= oElement.scrollTop;
            }
        }
        return [posX, posY];

    } else {

        return [oElement.x, oElement.y];
    }

}

// process mouse click events for page: close drop-down
function ddglobalMouseDown(evt)
{
	try{
		CerrarDesplegables(evt);
	}
	catch(e){
		ddCloseDropDowns(evt);
	}
}

function ddCloseDropDowns(evt){
	// reference to visible dropped-down grid
	var oGrid = null;
	var i = dropDownGrids.length;
	for(var i = 0; i < dropDownGrids.length; i++){
		if(dropDownGrids[i].isDisplayed){
			oGrid = dropDownGrids[i];
			// find source element
			if(evt == null) evt = window.event;
			if(evt != null)
			{
				var elem = evt.srcElement;
				if(elem == null) if((elem = evt.target) == null) o = this;
				while(elem != null)
				{
					// ignore events that belong to grid
					if(elem == oGrid.mainElement) return;
					elem = elem.offsetParent;
				}
			}
			// close grid
			ddshowDropDown(oGrid, null, false, false);
		}
	}
}

function replaceAll(sOriginal, sBuscar, sSustituir)
{
if (sOriginal==null)
	return null

re = eval("/" + sBuscar + "/")
var sTmp = ""

while (sOriginal.search(re)>=0)
	{
	sTmp += sOriginal.substr(0,sOriginal.search(re)) + sSustituir
	sOriginal = sOriginal.substr(sOriginal.search(re) + sBuscar.length,sOriginal.length)
	}
sTmp += sOriginal
return (sTmp)

}


function ddArtsTextChanged()
{
arguments[0].fsEntry.Editor.setValue(null)


}
function ddArtsCodChanged()
{
o = arguments[0].fsEntry

o.EditorDen.setValue(null)



}

function ddArtsCodUpdated()
{
	o = arguments[0].fsEntry
	if (o.Editor.getValue()=="")
		return
	idMat="";
	oMat = fsGeneralEntry_getById(o.dependentfield)
		
	if ((oMat) && (oMat.tipoGS==103)) {
		sMat = oMat.getDataValue();
		idMat = oMat.id;
	}else
		//sMat = oEdit.fsEntry.Dependent.value;
		sMat = null;
		
	if (sMat == null) {
		sMat = ""
		idMat="";
	}
	var idEntryPrec = null;
	var idEntryProv = null;
	var idEntryCC = null;
	
	//Obtener  el id del DataEntry del PRECIO si CARGAR_ULT_ADJ = 1
	if (o.idEntryPREC != null) {
		idEntryPrec = o.idEntryPREC;		
		oPrecio = fsGeneralEntry_getById(idEntryPrec);
		if ((oPrecio) && (oPrecio.cargarUltADJ == true)) 
			idEntryPrec = o.idEntryPREC;
		else
			idEntryPrec = "";
	}
	//Obtener  el id del DataEntry del PROVEEDOR si CARGAR_ULT_ADJ = 1
	if (o.idEntryPROV != null) {
		idEntryProv = o.idEntryPROV;		
		oProveedor = fsGeneralEntry_getById(idEntryProv);
		if ((oProveedor) && (oProveedor.cargarUltADJ == true)) 
			idEntryProv = o.idEntryPROV;
		else
			idEntryProv = "";
	}
		
		
	if (idEntryPrec== null)
		idEntryPrec = ""
	if (idEntryProv== null)
		idEntryProv = ""
	if (o.idEntryUNI == null)
		idEntryUNI=""
	else
		idEntryUNI = o.idEntryUNI
		
	window.open ("../_common/validarArticulo.aspx?MatClientId=" + Var2Param(idMat) + "&ArtClientId=" + Var2Param(o.id) + "&codart=" + Var2Param(o.Editor.getValue()) + "&codmat=" + Var2Param(sMat)+"&fsEntryPrec="+idEntryPrec+"&fsEntryProv="+idEntryProv+"&InstanciaMoneda="+o.instanciaMoneda+"&fsEntryUni="+idEntryUNI, "iframeWSServer")
}