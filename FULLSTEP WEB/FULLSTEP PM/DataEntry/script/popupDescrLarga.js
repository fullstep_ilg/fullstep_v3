
var ggWinPopUp;

/*
''' <summary>
''' Revisado por: SRA. Fecha: 30/03/2011
''' Crear el objeto necesario para gestionar textos largos
''' </summary>
''' <param name="p_item">Control a mostrar, para acceder al Entry</param>
''' <param name="p_value">valor, texto largo</param>              
''' <param name="p_WinPopUp">pantalla donde se muestra el texto y bt aceptar</param>
''' <param name="p_title">titulo</param>  
''' <param name="p_msgError">mensaje de error</param>
''' <param name="p_maxLength">longitud maxima</param>  
''' <param name="p_textoboton">texto del bot�n</param>
''' <param name="p_style">Estilo</param>  
''' <param name="p_readOnly">readOnly s�/no</param>
''' <param name="p_NCSinRevisar">texto 'sin revisar' para cambio en portal del estado de la no conformidad</param>  
''' <param name="p_textobotoncancelar">texto del bot�n cancelar</param>
''' <param name="p_textobotoncerrar">texto del bot�n aceptar</param>
''' <remarks>Llamada desde: Build; Tiempo m�ximo: 0</remarks>*/
function PopUp(p_item, p_value, p_WinPopUp, p_title, p_msgError, p_maxLength, p_textoboton, p_readOnly, p_NCSinRevisar, p_textobotoncancelar, p_textobotoncerrar,windowTitle) {

	if (p_WinPopUp == null)
		this.gWinPopUp = ggWinPopUp;
	else
		this.gWinPopUp = p_WinPopUp;
	
	this.gValue = p_value
	this.gWindowTitle = windowTitle;
	this.gTitle = p_title
	
	this.gMaxLength = p_maxLength
		
	this.gMsgError = p_msgError
	
	this.gTextoBoton = p_textoboton
		
	this.gReturnItem = p_item;

	this.gReadOnly = p_readOnly;

	this.gNCSinRevisar = p_NCSinRevisar;

	this.gTextoBotonCancelar = p_textobotoncancelar

	this.gTextoBotonCerrar = p_textobotoncerrar
}

/*
''' <summary>
''' A�adir al codigo de la pantalla las funciones javascript necesarias para dar funcionalidad
''' </summary>      
''' <returns>Las funciones javascript</returns>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo: 0</remarks>*/
PopUp.prototype.getCode = function() {
	var vCode = "";
	var vHeader_Code = "";
	var vData_Code = "";


	vCode += "<SCRIPT>"
	vCode += "function validarKeyPress(e,l)\n"
	vCode += "{\n"
	vCode += "if (l > 0)\n"
	vCode += "  {\n"
	vCode += "  if (e.value.length>=l)\n"
	vCode += "	    return false\n"
	vCode += "  else\n"
	vCode += "	    return true\n"
	vCode += "  }\n"
	vCode += "return true\n"
	vCode += "}\n"

	vCode += "function validarLength(e,l,msg)\n"
	vCode += "{\n"
	vCode += "\n"
	vCode += "if (l > 0)\n"
	vCode += "{\n"
	vCode += "      if (e.value.length>l)\n"
	vCode += "	    {\n"
	vCode += "	        if (msg!=\"null\")\n"
	if (this.gMsgError != "")
		vCode += "		    alert(msg + l)\n"
	vCode += "  	    return false\n"
	vCode += "  	}\n"
	vCode += "}\n"
	vCode += "if (e.func)\n"
	vCode += "	{\n"
	vCode += "	eval(e.func + \"(e)\")\n"
	vCode += "	}\n"
	vCode += "return true\n"
	vCode += "}\n"

	vCode += "function init()\n"
	vCode += "{t=document.getElementById(\"txtTextoLargo\")\n"
	vCode += "t.focus();t.value = t.value;}\n"
    
	vCode += "function salir()"
	vCode += "	{\n"
	vCode += "retorno = true; \n"
	vCode += "retorno =  validarLength(document.getElementById(\"txtTextoLargo\")," + this.gMaxLength + ",'" + this.gMsgError + "') \n"
	vCode += " if (retorno) \n"
	vCode += "	{\n"
	vCode += " var Antes \n"
	vCode += " var Despues \n"
	vCode += " Antes = self.opener.fsGeneralEntry_getById(\"" + this.gReturnItem.id + "\").getValue() \n"
	vCode += " self.opener.fsGeneralEntry_getById(\"" + this.gReturnItem.id + "\").setValue(document.getElementById(\"txtTextoLargo\").value); \n "
	vCode += " Despues = self.opener.fsGeneralEntry_getById(\"" + this.gReturnItem.id + "\").getValue() \n"
	vCode += " if (Antes != Despues) \n"
	vCode += " {\n"
	vCode += "		if (" + this.gReturnItem.fsEntry.CtrlCambioRechazo + ") \n"
	vCode += "		{\n"
	vCode += "			try{	\n"
	vCode += "				var arrDesglose = \"" + this.gReturnItem.id + "\".split(\"_\") \n"
	vCode += "				if (arrDesglose.length==11) \n"
	vCode += "				{ \n"
	vCode += "					var Grupo = arrDesglose[0] \n"
	vCode += "					var ctl = arrDesglose[2] \n"
	vCode += "					var idGrupo = arrDesglose[3] \n"
	vCode += "					var padre = arrDesglose[5] \n"
	vCode += "					var index = arrDesglose[7] \n"
	vCode += "					var desglose = Grupo.toString() +  \"__\" + ctl.toString() + \"_\" + idGrupo.toString()  + \"_\" + idGrupo.toString() + \"_\" + padre.toString() \n"
	vCode += "				} \n"
	vCode += "				else \n"
	vCode += "				{ \n"
	vCode += "					if (arrDesglose.length==9) \n"
	vCode += "					{ \n"
	vCode += "						var Grupo = arrDesglose[0] \n"
	vCode += "						var ctl = arrDesglose[2] \n"
	vCode += "						var idGrupo = arrDesglose[3] \n"
	vCode += "						var index = arrDesglose[5] \n"
	vCode += "						var desglose = Grupo.toString() +  \"__\" + ctl.toString() + \"_\" + idGrupo.toString() \n"
	vCode += "					} \n"
	vCode += "					else \n"
	vCode += "					{ \n"
	vCode += "						var idGrupo = \"\"  \n"
	vCode += "						var index = arrDesglose[2] \n"
	vCode += "						var desglose = arrDesglose[0] \n"
	vCode += "					} \n"
	vCode += "				} \n"
	vCode += "				oEntry = self.opener.fsGeneralEntry_getById(desglose + \"_fsdsentry_\" + index.toString() + \"_\" + idGrupo.toString() + \"1010\") \n"
	vCode += "				if (oEntry.getDataValue() == 3) \n"
	vCode += "				{ \n"
	vCode += "					oEntry.setValue(\"" + this.gNCSinRevisar + "\") \n"
	vCode += "					oEntry.setDataValue(1) \n"
	vCode += "					if (self.opener.document.getElementById(desglose + \"_cmdFinAccion_\" + index.toString())) \n"
	vCode += "						self.opener.document.getElementById(desglose + \"_cmdFinAccion_\" + index.toString()).style.visibility = \"visible\" \n"
	vCode += "					if (self.opener.document.getElementById(\"Notificar\")) \n"
	vCode += "						self.opener.document.getElementById(\"Notificar\").value = 1	\n"
	vCode += "				}\n"
	vCode += "			}\n"
	vCode += "			catch(e){ \n"
	vCode += "			}\n"
	vCode += "		}\n"	
	vCode += " }\n"
	vCode += " window.close(); \n "
	vCode += " if(self.opener.fsGeneralEntry_getById(\"" + this.gReturnItem.id + "\").Formato!=='') self.opener.document.getElementById(\"" + this.gReturnItem.id + "\").onchange();\n"
	vCode += "}\n"

	vCode += "}\n"

	vCode += "window.onbeforeunload = function () { window.opener.ggWinPopUp = null; }"

	vCode += "</SCRIPT>"
	vCode += "<CENTER>"
	vCode += "<TABLE WIDTH=100% HEIGHT = 100% BORDER = 0> "
	vCode += "<tr>"
	vCode += "<td width=100% height=95% align=center>"
	vCode += "<TEXTAREA name=txtTextoLargo "
	if (this.gReadOnly)
		vCode += " READONLY=true "
	vCode += "id=txtTextoLargo style='width:100%;height:97%' type=text onkeypress='return validarKeyPress(this," + this.gMaxLength + ")' onchange='return validarLength(this," + this.gMaxLength + ",\"" + this.gMsgError + "\")'>" + (this.gValue ? this.gValue : "") + "</TEXTAREA>"
	vCode += "</td>"
	vCode += "</tr>"
	vCode += "<tr>"
	vCode += "<td align=center valign=center>"
	if (this.gReadOnly)
		vCode += "<input type=button value='" + this.gTextoBotonCerrar + "' onclick='window.close()'>"
	else {
		vCode += "<input type=button value='" + this.gTextoBoton + "' onclick='salir()'>&nbsp;&nbsp;"
		vCode += "<input type=button value='" + this.gTextoBotonCancelar + "' onclick='window.close()'>"
	}
	vCode += "</td>"
	vCode += "</tr>"
	vCode += "</TABLE>"
	vCode += "</CENTER>"
	return vCode;
}

PopUp.prototype.wwrite = function(wtext) {
	this.gWinPopUp.document.writeln(wtext);
}

PopUp.prototype.show = function() {
	var vCode = "";
	
	this.gWinPopUp.document.open();

	// Setup the page...
	this.wwrite("<html>");
	this.wwrite("<head><title>" + this.gWindowTitle + " - " + this.gTitle + "</title></head>");

	this.wwrite("<body style='margin: 10px 15px;' onload='init()'>");
	vCode = this.getCode();
	this.wwrite(vCode);

	this.wwrite("</body></html>");
	this.gWinPopUp.document.close();
}

/*
''' <summary>
''' Revisado por: SRA. Fecha: 30/03/2011
''' Muestra el texto largo en un html anchor y si es editable lo modifica en la pantalla q llama.
''' </summary>
''' <param name="p_item">Control a mostrar, para acceder al Entry</param>
''' <param name="p_value">valor, texto largo</param>            
''' <param name="p_textoboton">texto del bot�n</param>
''' <param name="p_style">Estilo</param>  
''' <param name="p_textobotoncancelar">texto del bot�n cancelar</param>
''' <param name="p_textobotoncerrar">texto del bot�n aceptar</param>
''' <remarks>Llamada desde: jsAlta/show_popup; Tiempo m�ximo: 0</remarks>*/
function Build(p_item, p_value, p_textoboton, p_textobotoncancelar, p_textobotoncerrar,windowTitle) {
	if (arguments[1] == null)
		p_value = null;
	else
		p_value = arguments[1]
		
	var p_WinPopUp = ggWinPopUp;
	gPopUp = new PopUp(p_item, p_value, p_WinPopUp, p_item.fsEntry.title, p_item.fsEntry.errMsg, p_item.fsEntry.maxLength, p_textoboton, p_item.fsEntry.readOnly, p_item.fsEntry.NCSinRevisar, p_textobotoncancelar, p_textobotoncerrar,windowTitle);	
	gPopUp.show();
}

