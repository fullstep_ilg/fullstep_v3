
Imports System
Imports System.IO
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.Design

Public Class DataEntryDesigner
    Inherits System.Web.UI.Design.ControlDesigner

    Public Overrides Function GetDesignTimeHtml() As String
        'Component is the control instance, defined in the base
        'designer()
        Dim sHTML As String

        Dim ctl As Fullstep.DataEntry.GeneralEntry = CType(Me.Component,  _
            Fullstep.DataEntry.GeneralEntry)

        sHTML = "<input type=text width=140px>"

        Return sHTML
    End Function
End Class
