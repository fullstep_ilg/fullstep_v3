﻿<Serializable()> _
Public Class Equipos
    Inherits Security
    Private moEquipos As DataSet

    Public ReadOnly Property Data() As Data.DataSet
        Get
            Return moEquipos
        End Get
    End Property

    ''' <summary>
    ''' Procedimiento que carga los equipos
    ''' </summary>
    Public Sub Load()
        Authenticate()
        moEquipos = DBServer.Equipos_Load()
    End Sub

    Public Sub New(ByRef dbserver As FSNDataBaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
End Class
