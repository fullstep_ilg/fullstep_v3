﻿Imports System.Web
Imports System.Web.UI.WebControls.WebParts

Public Class ProveedorPersonalizacion
    Inherits PersonalizationProvider

    Private mApplicationName As String

    ''' <summary>
    ''' Obtiene o establece el nombre de la aplicación configurada para el proveedor.
    ''' </summary>
    ''' <value>La aplicación configurada para el proveedor de personalización.</value>
    Public Overrides Property ApplicationName() As String
        Get
            Return mApplicationName
        End Get
        Set(ByVal value As String)
            mApplicationName = value
        End Set
    End Property

#Region "Funciones no necesarias para esta implementación"

    ''' <summary>
    ''' Devuelve una colección que contiene cero o más objetos derivados de PersonalizationStateInfo en función del ámbito y los parámetros de consulta específicos.
    ''' </summary>
    ''' <param name="scope">Una enumeración PersonalizationScope con la información de personalización que se va a consultar. Este valor no puede ser nullNothingnullptrreferencia null (Nothing en Visual Basic).</param>
    ''' <param name="query">Una clase PersonalizationStateQuery que contiene una consulta. Este valor puede ser nullNothingnullptrreferencia null (Nothing en Visual Basic).</param>
    ''' <param name="pageIndex">La ubicación donde se inicia la consulta.</param>
    ''' <param name="pageSize">Número de registros que se van a devolver.</param>
    ''' <param name="totalRecords">Número total de registros disponibles.</param>
    ''' <returns>Una colección PersonalizationStateInfoCollection que contiene cero o más objetos derivados de PersonalizationStateInfo.</returns>
    ''' <remarks>No implementada.</remarks>
    Public Overrides Function FindState(ByVal scope As System.Web.UI.WebControls.WebParts.PersonalizationScope, ByVal query As System.Web.UI.WebControls.WebParts.PersonalizationStateQuery, ByVal pageIndex As Integer, ByVal pageSize As Integer, ByRef totalRecords As Integer) As System.Web.UI.WebControls.WebParts.PersonalizationStateInfoCollection
        Throw New Exception("Llamada a la función FindState del proveedor de personalización")
    End Function

    ''' <summary>
    ''' Elimina el estado de personalización del almacén de datos subyacente en función de los parámetros especificados.
    ''' </summary>
    ''' <param name="scope">Una enumeración PersonalizationScope de la información de personalización que se va a reiniciar. Este valor no puede ser nullNothingnullptrreferencia null (Nothing en Visual Basic).</param>
    ''' <param name="paths">Las rutas de acceso para información de personalización que se van a eliminar.</param>
    ''' <param name="usernames">Los nombres de usuario para información de personalización que se van a eliminar.</param>
    ''' <returns>Número de filas eliminadas.</returns>
    ''' <remarks>No implementada</remarks>
    Public Overrides Function ResetState(ByVal scope As System.Web.UI.WebControls.WebParts.PersonalizationScope, ByVal paths() As String, ByVal usernames() As String) As Integer
        Throw New Exception("Llamada a la función ResetState del proveedor de personalización")
    End Function

    ''' <summary>
    ''' Elimina los datos de personalización de elementos Web del almacén de datos subyacente en función de los parámetros especificados.
    ''' </summary>
    ''' <param name="path">La ruta de acceso de los datos de personalización que se van a eliminar. Este valor puede ser nullNothingnullptrreferencia null (Nothing en Visual Basic) pero no puede ser una cadena vacía ("").</param>
    ''' <param name="userInactiveSinceDate">La fecha que indica la última vez que un usuario del sitio Web cambió los datos de personalización.</param>
    ''' <returns>El número de filas eliminadas del almacén de datos subyacente.</returns>
    ''' <remarks>No implementada</remarks>
    Public Overrides Function ResetUserState(ByVal path As String, ByVal userInactiveSinceDate As Date) As Integer
        Throw New Exception("Llamada a la función ResetUserState del proveedor de personalización")
    End Function

#End Region

    ''' <summary>
    ''' Carga datos de personalización sin formato del almacén de datos subyacente.
    ''' </summary>
    ''' <param name="webPartManager">El objeto WebPartManager que administra los datos de personalización.</param>
    ''' <param name="path">La ruta de acceso para información de personalización que se va a utilizar como clave de recuperación.</param>
    ''' <param name="userName">El nombre de usuario para información de personalización que se va a utilizar como clave de recuperación.</param>
    ''' <param name="sharedDataBlob">Los datos devueltos para el ámbito Shared.</param>
    ''' <param name="userDataBlob">Los datos devueltos para el ámbito User.</param>
    ''' <remarks></remarks>
    Protected Overrides Sub LoadPersonalizationBlobs(ByVal webPartManager As System.Web.UI.WebControls.WebParts.WebPartManager, ByVal path As String, ByVal userName As String, ByRef sharedDataBlob() As Byte, ByRef userDataBlob() As Byte)
        Dim _FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim _FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
        userName = _FSNUser.Cod
        Dim oProveedorPersonalizacion_Security As FSNServer.ProveedorPersonalizacion_Security = _FSNServer.Get_Object(GetType(FSNServer.ProveedorPersonalizacion_Security))
        userDataBlob = oProveedorPersonalizacion_Security.LoadPersonalizationBlobs(mApplicationName, path, userName)
    End Sub

    ''' <summary>
    ''' Elimina datos de personalización sin formato del almacén de datos subyacente.
    ''' </summary>
    ''' <param name="webPartManager">El objeto WebPartManager que administra los datos de personalización.</param>
    ''' <param name="path">La ruta de acceso para información de personalización que se va a utilizar como clave de almacenamiento de datos.</param>
    ''' <param name="userName">El nombre de usuario para la información de personalización que se va a utilizar como clave de almacenamiento de datos.</param>
    ''' <remarks></remarks>
    Protected Overrides Sub ResetPersonalizationBlob(ByVal webPartManager As System.Web.UI.WebControls.WebParts.WebPartManager, ByVal path As String, ByVal userName As String)
        Dim _FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim _FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
        userName = _FSNUser.Cod
        Dim oProveedorPersonalizacion_Security As FSNServer.ProveedorPersonalizacion_Security = _FSNServer.Get_Object(GetType(FSNServer.ProveedorPersonalizacion_Security))
        oProveedorPersonalizacion_Security.ResetPersonalizationBlob(mApplicationName, path, userName)
    End Sub

    ''' <summary>
    ''' Guarda datos de personalización sin formato en el almacén de datos subyacente.
    ''' </summary>
    ''' <param name="webPartManager">El objeto WebPartManager que administra los datos de personalización.</param>
    ''' <param name="path">La ruta de acceso para información de personalización que se va a utilizar como clave de almacenamiento de datos.</param>
    ''' <param name="userName">El nombre de usuario para información de personalización que se va a utilizar como clave.</param>
    ''' <param name="dataBlob">La matriz de bytes de datos que se va a guardar.</param>
    ''' <remarks></remarks>
    Protected Overrides Sub SavePersonalizationBlob(ByVal webPartManager As System.Web.UI.WebControls.WebParts.WebPartManager, ByVal path As String, ByVal userName As String, ByVal dataBlob() As Byte)
        Dim _FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim _FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
        userName = _FSNUser.Cod
        Dim oProveedorPersonalizacion_Security As FSNServer.ProveedorPersonalizacion_Security = _FSNServer.Get_Object(GetType(FSNServer.ProveedorPersonalizacion_Security))
        oProveedorPersonalizacion_Security.SavePersonalizationBlob(mApplicationName, path, userName, dataBlob)
    End Sub

    ''' <summary>
    ''' Devuelve el número de filas del almacén de datos subyacente que existen dentro del ámbito especificado.
    ''' </summary>
    ''' <param name="scope">Una enumeración PersonalizationScope de la información de personalización que se va a consultar. Este valor no puede ser nullNothingnullptrreferencia null (Nothing en Visual Basic).</param>
    ''' <param name="query">Una clase PersonalizationStateQuery que contiene una consulta. Este valor puede ser nullNothingnullptrreferencia null (Nothing en Visual Basic).</param>
    ''' <returns>El número de filas del almacén de datos subyacente que existen para el parámetro scope especificado.</returns>
    ''' <remarks></remarks>
    Public Overrides Function GetCountOfState(ByVal scope As System.Web.UI.WebControls.WebParts.PersonalizationScope, ByVal query As System.Web.UI.WebControls.WebParts.PersonalizationStateQuery) As Integer
        Dim _FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim oProveedorPersonalizacion_Security As FSNServer.ProveedorPersonalizacion_Security = _FSNServer.Get_Object(GetType(FSNServer.ProveedorPersonalizacion_Security))
        oProveedorPersonalizacion_Security.GetCountOfState(mApplicationName, query)
    End Function

    ''' <summary>
    ''' Inicializa el proveedor.
    ''' </summary>
    ''' <param name="name">Nombre descriptivo del proveedor.</param>
    ''' <param name="config">Colección de los pares nombre/valor que representan los atributos específicos de proveedor concretados en la configuración de este proveedor.</param>
    ''' <remarks>La implementación de la clase base realiza un seguimiento interno del número de veces que se ha llamado al método Initialize del proveedor. Si se inicializa un proveedor más de una vez, se produce una excepción InvalidOperationException que explica que el proveedor ya está inicializado.</remarks>
    Public Overrides Sub Initialize(ByVal name As String, ByVal config As System.Collections.Specialized.NameValueCollection)
        MyBase.Initialize(name, config)
        mApplicationName = config.Item("applicationName")
    End Sub
End Class
