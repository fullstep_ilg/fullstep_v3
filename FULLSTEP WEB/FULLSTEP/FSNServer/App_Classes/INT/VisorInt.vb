﻿Public Class VisorInt
    Inherits Security


    ''' <summary>
    ''' Constructor de la clase Integracion
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si está autenticado</param>
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub

    <Serializable()>
    Public Enum SentidoINT As Integer
        Salida = 1
        Entrada = 2
        EntradaSalida = 3
    End Enum

    Public Enum EstadoINT As Integer
        Pendiente = 0
        Enviado = 1
        NoAplica = 2
        ErrorInt = 3
        Correcto = 4
    End Enum



#Region "VisorIntegracion"


    ''' <summary>
    ''' Lista de ERPs existentes.
    ''' </summary>
    ''' <param name="lPyme" >Identificador de la Pyme del usuario (modo Pymes)</param>
    ''' <returns>Lista de solicitudes</returns>
    ''' <remarks>Llamada desde: VisorIntegracion.aspx.vb; Tiempo máximo: 0,1</remarks>
    ''' Revisado: ngo 22/06/2012
    Public Function DevuelveERPs(Optional ByVal lPyme As Integer = 0) As DataSet
        Authenticate()
        Return DBServer.INT_DevuelveERPs(lPyme)
    End Function

    ''' <summary>
    ''' Lista de Entidades existentes.
    ''' </summary>
    ''' <returns>Lista de solicitudes</returns>
    ''' <remarks>Llamada desde: VisorIntegracion.aspx.vb; Tiempo máximo: 0,1</remarks>
    ''' Revisado: ngo 22/06/2012
    Public Function DevuelveEntidades(Optional ByVal CodERP As String = "") As DataSet
        Authenticate()
        Return DBServer.INT_DevuelveEntidades(CodERP)
    End Function

    ''' <summary>
    ''' Devuelve un listado con procesos existentes en las tablas de LOG (LOG_ADJ o LOG_PROCE), filtrando por los parámetros que se indiquen
    ''' </summary>
    ''' <param name="CodERP">Código del ERP para el que se buscan los datos</param>
    ''' <param name="iEntidad">Entidad para la que se buscan los datos (9=Adjudicaciones, 12=Procesos)</param>
    ''' <param name="Anyo">Filtro: Año del proceso</param>
    ''' <param name="GMN1">Filtro: GMN1 del proceso</param>
    ''' <param name="CodProce">Filtro: Código del proceso</param>
    ''' <returns>DataSet de procesos</returns>
    ''' <remarks>Llamada desde: VisorIntegracion.aspx.vb; Tiempo máximo: 0,1</remarks>
    ''' Revisado: ngo 22/06/2012
    Public Function DevuelveProcesos(ByVal CodERP As String, ByVal iEntidad As Integer, Optional ByVal Anyo As Short = 0, Optional ByVal GMN1 As String = "", Optional ByVal CodProce As Integer = 0) As DataSet
        Authenticate()
        Return DBServer.INT_DevuelveProcesos(CodERP, iEntidad, ANYO, GMN1, CodProce)
    End Function

    ''' <summary>
    ''' Devuelve un listado con los aÃ±os existentes con pedidos
    ''' </summary>
    ''' <returns>DataSet de aÃ±os con pedidos</returns>
    ''' <remarks>Llamada desde: VisorIntegracion.aspx.vb; Tiempo mÃ¡ximo: 0,1</remarks>
    Public Function DevuelveAnyo() As DataSet
        Authenticate()
        Return DBServer.INT_DevuelveAnyo()
    End Function

    ''' Revisado por: ngo; Fecha: 12/03/2012
    ''' <summary>
    ''' Carga la grid  del visor de integración
    ''' </summary>
    ''' <param name="sERP">Código del ERP seleccionado</param>
    ''' <param name="iEntidad">Identificador de la entidad (TABLAS_INTEGRACION_ERP.TABLA)</param>
    ''' <param name="iAnioProce">Año del proceso</param>
    ''' <param name="sGMN">GMN1 del proceso</param>
    ''' <param name="iCodProce">Código del proceso</param>
    ''' <param name="sCod">Código del dato a buscar</param>
    ''' <param name="sCodErp">Código ERP del dato a buscar</param>
    ''' <param name="sAlbaranErp">Código ERP del albarán</param> 
    ''' <param name="iAnioPedido">Año del pedido</param> 
    ''' <param name="iNumPedido">Número de pedido</param> 
    ''' <param name="iNumOrden">Número de orden</param> 
    ''' <param name="iIDLog">Identificador de la tabla LogGral</param> 
    ''' <param name="sAccion">Acción</param> 
    ''' <param name="iEstado">Estado de integración</param> 
    ''' <param name="iSentido">Sentido de integración</param> 
    ''' <param name="dFechaDesde">Filtro fecha desde</param> 
    ''' <param name="dFechaHasta">Filtro fecha hasta</param> 
    ''' <param name="sDateFormat">Formato de fecha</param>
    ''' <param name="sIdioma">Idioma del usuario</param>
    ''' <remarks>Llamada desde: Notificaciones,.aspx.vb ; Tiempo máximo: 0,3</remarks>
    ''' Revisado: ngo 22/06/2012
    Public Function CargaVisor(ByVal sERP As String, ByVal iEntidad As Integer, ByVal iAnioProce As Integer, ByVal sGMN As String, ByVal iCodProce As Integer, _
                                ByVal sCod As String, ByVal sCodErp As String, ByVal sAlbaranErp As String, ByVal iAnioPedido As Integer, ByVal iNumPedido As Integer, ByVal iNumOrden As Integer, _
                                ByVal iIDLog As Integer, ByVal sAccion As String, ByVal iEstado As Integer, ByVal iSentido As Integer, ByVal dFechaDesde As Date, ByVal dFechaHasta As Date, _
                                ByVal sDateFormat As String, ByVal sIdioma As String) As DataSet
        Authenticate()
        Dim ds As DataSet
        ds = DBServer.INT_CargarVisor(sERP, iEntidad, iAnioProce, sGMN, iCodProce, sCod, sCodErp, sAlbaranErp, iAnioPedido, iNumPedido, iNumOrden, iIDLog, sAccion, iEstado, iSentido, dFechaDesde, dFechaHasta, sDateFormat, sIdioma)
        If ds IsNot Nothing Then
            ds.Tables(0).TableName = "VISOR"
        End If
        Return ds
    End Function


    ''' <summary>
    ''' Lista de Entidades existentes.
    ''' </summary>
    ''' <returns>Lista de solicitudes</returns>
    ''' <remarks>Llamada desde: VisorIntegracion.aspx.vb; Tiempo máximo: 0,1</remarks>
    ''' Revisado: ngo 22/06/2012
    Public Function DevuelveErrorIntegracion(ByVal iIdLogGral As Integer) As DataSet
        Authenticate()
        Return DBServer.INT_DevuelveERPs(0) ''Se envía el mail de error a TODOS los notificados, independientemente de que sea modo Pymes o no
    End Function

    ''' <summary>
    ''' Carga los datos del error de integración para el ID indicado
    ''' </summary>
    ''' <param name="Identificador">Identificador de la tabla LOG_GRAL</param>
    ''' <returns>Un dataset con una tabla con los datos del error</returns>
    ''' <remarks>
    ''' Llamada desde: VisorIntegracion.aspx (al pusar en la celda de la grid con el error)
    ''' Tiempo máximo: 0 sec.
    ''' </remarks>
    ''' Revisado: ngo 22/06/2012
    Public Function CargarErrorIntegracion(ByVal Identificador As Long) As DataSet

        Dim data As DataSet

        data = DBServer.INT_DevuelveErrorIntegracion(Identificador)

        Return data

    End Function

    ''' <summary>
    ''' Carga las longitudes de los campos GMN1, GMN2, GMN3 y GMN4.
    ''' </summary>
    ''' <returns>Un dataset con una tabla con las longitudes de los campos GMNX.</returns>
    ''' <remarks>
    ''' Llamada desde: VisorIntegracion.aspx (al cargar la entidad Estructura de Materiales).
    ''' Tiempo máximo: 0 sec.
    ''' </remarks>
    ''' Revisado: aam 17/09/2013
    Public Function DevuelveLongitudGMN() As DataSet

        Dim data As DataSet

        data = DBServer.INT_DevuelveLongitudGMN()

        Return data

    End Function
#End Region


#Region "Monitorizacion Servicio Integracion"


    ''' <summary>
    ''' Comprueba si el servicio de integración está parado y en caso afirmativo, marca el registro correspondiente en INT_CHEQUO
    ''' </summary>
    ''' <param name="TiempoImp">Peridicidad de la importacion de datos (tiempo medio)</param>
    ''' <param name="TiempoExp">Periodicidad de la exportacion de datos (tiempo medio)</param>
    ''' <param name="TiempoEjecucion">Tiempo medio de ejecucion</param>
    ''' <param name="Fecha">SALIDA. Devuelve la fecha de parada de integracion </param>
    ''' <returns>TRUE si el servicio de integracio</returns>
    ''' <remarks>Llamada desde WebServicePM.INTServicio.asmx.vb; tiempo ejecucion:=0,2seg.</remarks>
    ''' Revisado: ngo 22/06/2012
    Public Function ComprobarParadaServicioIntegracion(ByVal TiempoImp As Double, ByVal TiempoExp As Double, ByVal TiempoEjecucion As Double,
                                                       ByRef Fecha As Date) As Boolean
        Authenticate()
        ComprobarParadaServicioIntegracion = DBServer.INT_CompruebaParadaServicioIntegracion(TiempoImp, TiempoExp, TiempoEjecucion, Fecha)
    End Function
    ''' <summary>
    ''' Envia un mail a los notificados de error en caso de que se haya producido una parada en el servicio de integracion
    ''' </summary>
    ''' <param name="Fecha">Fecha parada integracion</param>
    ''' <returns>TRUE si ha ido ok</returns>
    ''' <remarks>Llamada desde WebServicePM.INTServicio.asmx.vb; tiempo ejecucion:=0,3seg.</remarks>
    ''' Revisado: ngo 22/06/2012
    Public Function NotificarParadaIntegracion(ByVal Fecha As Date) As Boolean
        Authenticate()
        Dim oNotificador As New Notificar(DBServer, mIsAuthenticated)
        oNotificador.NotificacionINTParadaServicioIntegracion(Fecha)
        oNotificador.FuerzaFinalizeSmtpClient()
        oNotificador = Nothing
    End Function


    ''' <summary>
    ''' Consulta la tabla INT_CHEQUEO (desde el visor de integracion) para detectar si el servicio de integracion esta parado y debe mostrar la alerta
    ''' </summary>
    ''' <param name="FechaParada">SALIDA. Fecha de parada del servicio de integracion</param>
    ''' <returns></returns>
    ''' <remarks>Llamada desde: VisorIntgeracion.asp.vb->Page_Load y MenuMaster.vb.->CargarTextos. Tiempo maximo 0.1 </remarks>
    ''' Revisado ngo 22/06/2012
    Public Function HayParadaServicioInt(ByRef FechaParada As DateTime) As Boolean
        Authenticate()
        Dim dsInt As DataSet
        FechaParada = Nothing

        HayParadaServicioInt = False

        dsInt = DBServer.INT_HayParadaServicioInt()
        If dsInt.Tables.Count = 0 Then
            dsInt.Clear()
            dsInt.Dispose()
            dsInt = Nothing
            Exit Function
        Else
            ''EXPORTAR,PARADA,FECHA
            For Each fila As DataRow In dsInt.Tables(0).Rows
                If fila.Item("PARADA") = 1 Then
                    HayParadaServicioInt = True
                    FechaParada = fila.Item("FECHA")
                    Exit For
                End If
            Next

        End If
        dsInt.Clear()
        dsInt.Dispose()
        dsInt = Nothing
        Exit Function

    End Function
    ''' <summary>
    ''' Consulta INT_CHEQUEO para ver si ha habido alguna parada en el servicio de integracion (para mostrar icono correspondiente al historico de paradas)
    ''' </summary>
    ''' <returns>TRUE si ha habido alguna parada</returns>
    ''' <remarks>Llamada desde VisorIntgeracion.asp.vb->Page_Load. tiempo maximo 0.1 sg</remarks>
    ''' Revisado ngo 22/06/2012
    Public Function MostrarHistoricoParadasINT() As Boolean
        Authenticate()
        Dim dsInt As DataSet

        MostrarHistoricoParadasINT = False

        dsInt = DBServer.INT_MostrarHistoricoParadasINT()
        If dsInt.Tables.Count <> 0 Then
            MostrarHistoricoParadasINT = True
        End If
        dsInt.Clear()
        dsInt.Dispose()
        dsInt = Nothing
        Exit Function

    End Function

    ''' <summary>
    ''' Obtiene un dataSet con las paradas que ha habido en integración
    ''' </summary>
    ''' <returns>Dataset con las paradas que se han registrado en integración tanto al importar como al exportar</returns>
    ''' <remarks>
    ''' Llamada desde Historico.Master.vb
    ''' Última revisión asg 19/05/2014
    ''' </remarks>
    Public Function ObtenerHistoricoParadasINT() As DataSet
        Authenticate()

        ObtenerHistoricoParadasINT = Nothing

        Try
            ObtenerHistoricoParadasINT = DBServer.INT_MostrarHistoricoParadasINT()
        Catch e As Exception
        End Try
    End Function

    ''' <summary>
    ''' Se comprueba si ha habido un parada de la integración previa
    ''' </summary>
    ''' <param name="FechaParada">Fecha de la parada</param> 
    ''' <param name="TRecordAlerta">Tiempo de espera para eviar el mail recordatorio de alerta por tiempos excedidos</param> 
    ''' <param name="fRecordAlerta">Frecuencia con la que se produce el recordatorio de alerta</param> 
    ''' <returns>TRUE si ha habido alguna parada previa</returns>
    ''' <remarks>Llamada desde VisorInt.vb y Service.vb tiempo maximo 0.1 sg</remarks>
    ''' Revisado ijc 22/12/2012

    Public Function HayParadaIntegracionPrevia(ByVal FechaParada As DateTime, ByVal TRecordAlerta As Double, ByVal fRecordAlerta As String) As Boolean

        Dim bEnviarRecordatorio As Boolean
        Dim bParadaSolucionada As Boolean

        bEnviarRecordatorio = False
        bParadaSolucionada = False

        DBServer.INT_ComprobarParadaPrevia(FechaParada, TRecordAlerta, fRecordAlerta, bEnviarRecordatorio, bParadaSolucionada)

        If bParadaSolucionada Then
            NotificarIntegracionRestablecida(FechaParada, 1, 0)
        End If

        HayParadaIntegracionPrevia = bEnviarRecordatorio

    End Function

    ''' <summary>
    ''' Se comprueba si hay algún regisro en la tabla INT_CHEQUEO con error por alerta de tiempo excedido
    ''' </summary>
    ''' <param name="Fecha">Fecha de la alerta</param> 
    ''' <param name="TRecordAlerta">Tiempo de espera para eviar el mail recordatorio de alerta por tiempos excedidos</param> 
    ''' <param name="fRecordAlerta">Frecuencia con la que se produce el recordatorio de alerta</param> 
    ''' <remarks>Llamada desde VisorInt.vb y Service.vb tiempo maximo 0.1 sg</remarks>
    ''' Revisado ijc 22/12/2012

    Public Sub ComprobarAlertaTiempoExcedido(ByVal Fecha As Date, ByVal TRecordAlerta As Double, ByVal FRecordAlerta As String)
        Authenticate()

        Dim iTEsperaAcuse As Integer
        Dim iTEsperaXML As Integer
        Dim iTMonitorizacion As Integer

        Dim iErp As Integer

        Dim lError As Integer
        Dim lNumRegXML As Integer
        Dim lNumRegAcuse As Integer
        Dim lTMedioXML As Double
        Dim lTMedioAcuse As Double
        Dim dFechaIniXml As String
        Dim dFechaIniAcuse As String
        Dim bEnviarRecordatorio As Boolean
        Dim lMonitAcuse As Double
        Dim lMonitXML As Double
        Dim iNumTotalXML As Integer
        Dim iNumTotalAcuse As Integer

        Dim dsTiempos As DataSet
        Dim dsFechas As DataSet


        dsTiempos = DBServer.INT_ObtenerTiemposMaxEspera()

        For Each fila As DataRow In dsTiempos.Tables(0).Rows
            iTEsperaAcuse = CInt(fila.Item("T_ESPERA_ACUSE"))
            iTEsperaXML = CInt(fila.Item("T_ESPERA"))
            iTMonitorizacion = CInt(fila.Item("T_MONITORIZACION"))

            iErp = CInt(fila.Item("ERP"))

            DBServer.INT_ComprobarAlertaTiempoExcedido(iErp, iTMonitorizacion, iTEsperaXML, iTEsperaAcuse, lError, lNumRegXML, lNumRegAcuse, lTMedioXML, lTMedioAcuse, lMonitAcuse, lMonitXML)

            ''Se coprueba si en la tabla INT_CHEQUEO hay un registro ya para estas alertas y se actualiza la tabla.

            bEnviarRecordatorio = False
            dFechaIniXml = ""
            dFechaIniAcuse = ""

            dsFechas = DBServer.INT_ComprobarRegistroTiempoExcedido(lError, iErp, bEnviarRecordatorio, TRecordAlerta, FRecordAlerta, dFechaIniXml, dFechaIniAcuse)

            If lError > 0 And bEnviarRecordatorio Then
                NotificarAlertaTiempoExcedidoIntegracion(Fecha, iErp, iTEsperaXML, iTEsperaAcuse, lError, lNumRegXML, lNumRegAcuse, lTMedioXML, lTMedioAcuse, dFechaIniXml, dFechaIniAcuse, lMonitAcuse, lMonitXML)
            Else
                ''Mandar un email con todo OK
                If Not IsNothing(dsFechas) Then
                    If dsFechas.Tables(0).Rows.Count <> 0 Then

                        iNumTotalXML = 0
                        iNumTotalAcuse = 0

                        DBServer.INT_ObtenerNumRegistros(dsFechas, iNumTotalXML, iNumTotalAcuse)

                        NotificarIntegracionRestablecida(Fecha, lError, iErp, dsFechas, iNumTotalXML, iNumTotalAcuse, iTMonitorizacion, iTEsperaXML, iTEsperaAcuse)
                    End If
                End If
            End If
        Next
    End Sub

    ''' <summary>
    ''' Envia un mail a los notificados de alerta en caso de se esten produciendo tiempos elevados en la generación de ficheros y/o procesado de acuses
    ''' </summary>
    ''' <param name="Fecha">Fecha de la alerta de integración</param>
    ''' <param name="iErp">id del Erp</param>       
    ''' <param name="iTEsperaXML">Tiempo medio referencia para la generación de xmls</param> 
    ''' <param name="iTEsperaAcuse">Tiempo medio referencia para el procesado de acuses</param> 
    ''' <param name="lError">Identificador del error producido</param> 
    ''' <param name="lNumRegXML">Num de xmls pendientes de generar</param> 
    ''' <param name="lNumRegAcuse">Num de registros sin acuse</param> 
    ''' <param name="lTMedioXML">Tiempo medio de generacion de los xml</param> 
    ''' <param name="lTMedioAcuse">Tiempo medio de procesado de acuses</param> 
    ''' <param name="lMonitAcuse">Tiempo de monitorización del procesado de acuses</param> 
    ''' <param name="lMonitXML">Tiempo de monitorización de la generación de xmls</param> 
    ''' <remarks>Llamada desde VisorInt.vb; tiempo ejecucion:=0,3seg.</remarks>
    ''' Revisado: ijc 22/12/2016
    Public Sub NotificarAlertaTiempoExcedidoIntegracion(ByVal Fecha As Date, ByVal iErp As Integer, ByVal iTEsperaXML As Integer, ByVal iTEsperaAcuse As Integer,
                                                  ByRef lError As Integer, ByRef lNumRegXML As Integer, ByRef lNumRegAcuse As Integer, ByRef lTMedioXML As Double, ByRef lTMedioAcuse As Double,
                                                  ByVal dFechaIniXml As String, ByVal dFechaIniAcuse As String, ByVal lMonitAcuse As Double, ByVal lMonitXML As Double)
        Authenticate()
        Dim oNotificador As New Notificar(DBServer, mIsAuthenticated)
        oNotificador.NotificacionINTTiempoExcedidoIntegracion(Fecha, iErp, iTEsperaXML, iTEsperaAcuse, lError, lNumRegXML, lNumRegAcuse, lTMedioXML, lTMedioAcuse, dFechaIniXml, dFechaIniAcuse, lMonitAcuse, lMonitXML)
        oNotificador.FuerzaFinalizeSmtpClient()
        oNotificador = Nothing
    End Sub


    ''' <summary>
    ''' Envia un mail a los notificados de que se ha restablecido el servicio de integración:
    '''     lError = 1: Parada del servicio de integración
    '''     lError = 0: Alerta tiempos excesivos
    ''' </summary>
    ''' <param name="Fecha">Fecha de la alerta de integracion</param>
    ''' <param name="lError">Tipo de error solucionado: Parada(1) y Tiempos excesivos(0)</param>
    ''' <param name="iErp">Id del erp</param>
    ''' <param name="dsFechas">Dataset con las fechas de las paradas y alertas de integración</param>
    ''' <param name="iNumTotalXML">Número total de registros afectados por el tiempo excesivo en la generación de ficheros</param>
    ''' <param name="iNumTotalAcuse">Número total de registros afectados por el tiempo excesivo en el procesado de acuses</param>
    ''' <param name="lMonit">Tiempo de referencia de monitorización</param>
    ''' <param name="lTiempoXML">Tiempo de referencia de la generación de ficheros</param>
    ''' <param name="lTiempoAcuse">Tiempo de referencia del procesado de acuses</param>
    ''' <remarks>Llamada desde VisorInt.vb; tiempo ejecucion:=0,3seg.</remarks>
    ''' Revisado: ijc 22/12/2016
    Public Sub NotificarIntegracionRestablecida(ByVal Fecha As Date, ByVal lError As Integer, ByVal iErp As Integer, Optional ByVal dsFechas As DataSet = Nothing,
                                                Optional ByVal iNumTotalXML As Integer = 0, Optional ByVal iNumTotalAcuse As Integer = 0, Optional ByVal lMonit As Integer = 0,
                                                Optional ByVal lTiempoXML As Integer = 0, Optional ByVal lTiempoAcuse As Integer = 0)
        Authenticate()
        Dim oNotificador As New Notificar(DBServer, mIsAuthenticated)
        oNotificador.NotificacionINTIntegracionRestablecida(Fecha, lError, iErp, dsFechas, iNumTotalXML, iNumTotalAcuse, lMonit, lTiempoXML, lTiempoAcuse)
        oNotificador.FuerzaFinalizeSmtpClient()
        oNotificador = Nothing
    End Sub


#End Region

End Class
