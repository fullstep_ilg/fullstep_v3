﻿''' <summary>
''' Representación de los parámetros de acceso a una base de datos.
''' Los datos se almacenan encriptados (tal y como se encuentran en la Base de datos) y se desencriptan al ser leídos
''' </summary>
''' <remarks></remarks>
<System.Serializable()>
Public Class BaseDatos


    Private _index As Long
    Private _id As Long
    Private _denominacion As String
    Private _servidor As String
    Private _nombre As String
    Private _usuario As String
    Private _password As String
    Private _impersonate_user As String
    Private _impersonate_password As String
    Private _impersonate_dominio As String
    Private _principal As Boolean
    Private _login As Boolean
    Private _activa As Boolean

    Public Sub New(index As Long, id As Long, denominacion As String, servidor As String, nombre As String, usuario As String, password As String, _
                    impersonate_user As String, impersonate_password As String, impersonate_dominio As String, principal As Boolean, _
                    login As Boolean, activa As Boolean)
        Me._index = index
        Me._id = id
        Me._denominacion = denominacion
        Me._servidor = servidor
        Me._nombre = nombre
        Me._usuario = usuario
        Me._password = password
        Me._impersonate_user = impersonate_user
        Me._impersonate_password = impersonate_password
        Me._impersonate_dominio = _impersonate_dominio
        Me._principal = principal
        Me._login = login
        Me._activa = activa
    End Sub

    Public ReadOnly Property nombre As String
        Get
            Return FSNLibrary.Encrypter.Encrypt(_nombre, , False, FSNLibrary.Encrypter.TipoDeUsuario.Administrador)
        End Get
    End Property

    Public ReadOnly Property id As Long
        Get
            Return _id
        End Get
    End Property
    ''' <summary>
    ''' indica si la BD está activa
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function isActiva() As Boolean
        Return _activa
    End Function
    ''' <summary>
    ''' Indica si la BD es la principal
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function isPrincipal() As Boolean
        Return _principal
    End Function
    ''' <summary>
    ''' Indica si la base de datos es la de logeo
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function isLogin() As Boolean
        Return _login
    End Function


    Public Property Denominacion() As String
        Get
            Return _denominacion
        End Get
        Set(ByVal value As String)
            _denominacion = value
        End Set
    End Property

    Public Property Servidor() As String
        Get
            Return FSNLibrary.Encrypter.Encrypt(_servidor, , False, FSNLibrary.Encrypter.TipoDeUsuario.Administrador)
        End Get
        Set(ByVal value As String)
            _servidor = value
        End Set
    End Property

    Public Property Usuario() As String
        Get
            Return FSNLibrary.Encrypter.Encrypt(_usuario, , False, FSNLibrary.Encrypter.TipoDeUsuario.Administrador)
        End Get
        Set(ByVal value As String)
            _usuario = value
        End Set
    End Property

    Public Property Password() As String
        Get
            Return FSNLibrary.Encrypter.Encrypt(_password, , False, FSNLibrary.Encrypter.TipoDeUsuario.Administrador)
        End Get
        Set(ByVal value As String)
            _password = value
        End Set
    End Property

    Public Property Impersonate_user() As String
        Get
            Return FSNLibrary.Encrypter.Encrypt(_impersonate_user, , False, FSNLibrary.Encrypter.TipoDeUsuario.Administrador)
        End Get
        Set(ByVal value As String)
            _impersonate_user = value
        End Set
    End Property

    Public Property Impersonate_password() As String
        Get
            Return FSNLibrary.Encrypter.Encrypt(_impersonate_password, , False, FSNLibrary.Encrypter.TipoDeUsuario.Administrador)
        End Get
        Set(ByVal value As String)
            _impersonate_password = value
        End Set
    End Property

    Public Property Impersonate_dominio() As String
        Get
            Return FSNLibrary.Encrypter.Encrypt(_impersonate_dominio, , False, FSNLibrary.Encrypter.TipoDeUsuario.Administrador)
        End Get
        Set(ByVal value As String)
            _impersonate_dominio = value
        End Set
    End Property

    Public Property Index() As Long
        Get
            Return _index
        End Get
        Set(ByVal value As Long)
            _index = value
        End Set
    End Property

End Class
