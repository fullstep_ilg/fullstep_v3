﻿Public Class CentrosCoste
    Inherits Security

    Private moData As DataSet
    Public ReadOnly Property Data() As Data.DataSet
        Get
            Return moData
        End Get
    End Property
    ''' <summary>
    ''' Obtiene la denominación del centro a través del código que se pasa como parámetro.
    ''' </summary>
    ''' <param name="sUsuario">Código de la persona</param>
    ''' <param name="sIdioma">Idioma del usuario</param>
    ''' <param name="sCodigo">Código del centro a buscar</param>
    ''' <param name="bUON">Indica si hay que buscar el cod en las UONS o en la tabla CENTRO_SM</param>
    ''' <remarks>Llamada desde: PMWeb --> BuscadorCentroCosteServer.aspx; Tiempo máximo: 1 sg.</remarks>
    Public Sub BuscarCentrosCoste(ByVal sUsuario As String, ByVal sIdioma As String, ByVal sCodigo As String, Optional ByVal bUON As Boolean = True)
        Authenticate()
        moData = DBServer.CentrosCoste_Busqueda(sUsuario, sIdioma, sCodigo, bUON)
    End Sub
    ''SMServer
    ''' <summary>
    ''' Devuelve los centros de coste con toda la estructura organizativa o solo los centros de coste para mostrarlas en un treeview. AÃ±ade las relaciones a las tablas del dataset.
    ''' </summary>
    ''' <param name="sUsuario">Cod de la persona</param>
    ''' <param name="sIdioma">Idioma del usuario</param>
    ''' <param name="bVerUON">true: se ve la estructura organizativa; false: sÃ³lo los centros de coste</param>
    ''' <returns>Dataset con los centros de coste y su estructura</returns>
    ''' <remarks>Llamada desde: FSNWeb --> usercontrols/wucCentrosCoste.ascx; Tiempo mÃ¡ximo: 1 sg;</remarks>
    Public Function LoadData(ByVal sUsuario As String, ByVal sIdioma As String, Optional ByVal bVerUON As Boolean = False) As DataSet
        Authenticate()
		Dim cacheName As String = "dsCentrosCoste_" & sUsuario & "_" & sIdioma & IIf(String.IsNullOrEmpty(bVerUON) Or Not bVerUON, "", "_1")
		If System.Web.HttpContext.Current.Cache(cacheName) Is Nothing Then
            moData = DBServer.CentrosCoste_Load(sUsuario, sIdioma, bVerUON)
            System.Web.HttpContext.Current.Cache.Insert(cacheName, moData, Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration, New TimeSpan(0, System.Configuration.ConfigurationManager.AppSettings("TiempoExpiracionCacheMin"), 0))
        Else
            moData = CType(System.Web.HttpContext.Current.Cache(cacheName), DataSet)
        End If
        Return moData
    End Function
    ''' <summary>
    ''' Constructor de la clase centroscoste
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si está autenticado</param>
    Public Sub New(ByRef dbserver As FSNDataBaseServer.Root,ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
End Class