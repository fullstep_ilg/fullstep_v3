﻿Public Interface IFSNPage

    ReadOnly Property Idioma() As String

    Property ModuloIdioma() As FSNLibrary.TiposDeDatos.ModulosIdiomas

    ReadOnly Property Textos(ByVal iTexto As Integer) As String

    ReadOnly Property Usuario() As FSNServer.User

    ReadOnly Property Acceso() As FSNLibrary.TiposDeDatos.ParametrosGenerales

    ReadOnly Property ScriptMgr() As System.Web.UI.ScriptManager

    ReadOnly Property FSNServer() As FSNServer.Root

End Interface
