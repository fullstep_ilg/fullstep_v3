<Serializable()> _
    Public Class Personas
    Inherits Security
    Private moPersonas As DataSet
    Public ReadOnly Property Data() As Data.DataSet
        Get
            Return moPersonas
        End Get
    End Property
    ''' <summary>
    ''' Procedimiento que carga los datos de las personas
    ''' </summary>
    ''' <param name="sCod">Codigo de la persona</param>
    ''' <param name="sNom">Nombre de la persona</param>
    ''' <param name="sApe">Apellido de la persona</param>
    ''' <param name="sUON1">Codigo de la unidad organizativa de nivel 1 de la persona</param>
    ''' <param name="sUON2">Codigo de la unidad organizativa de nivel 2 de la persona</param>
    ''' <param name="sUON3">Codigo de la unidad organizativa de nivel 3 de la persona</param>
    ''' <param name="sDEP">Departamento de la persona</param>
    ''' <param name="bPM">Variable boolenaa que indica si la persona tiene acceso a PM o no</param>
    ''' <param name="bSoloUsuariosQa">Variable booleana que indica si s�lo se deben cargar usuarios de QA</param>
    ''' <param name="bCtlBajaLog">si se controla o no la bajalogica</param>
    ''' <param name="lDesdeFila">Posici�n de la primera fila a partir de ka cual se debe mostrar (no inclu�da)</param>
    ''' <param name="lNumFilas">N�mero de filas a extraer de la BDD</param>
    ''' <param name="bCM">Variable boolena que indica si la persona tiene acceso a CM o no</param> 
    ''' <remarks>
    ''' Llamada desde: PmWeb/Instancia/ComprobarPreCondiciones, PmWeb/detallepersona/Page_load, FSNWeb/Consultas/Obtener_DatosPersona, PmWeb/guardarInstancia/TrasladoUsu y Comprobarcondiciones, PmWeb/parametrosgenerales/page_preRender,PmWeb/usuarios/Page_load, PmWeb/campos/Page_lod y ComprobarCondiciones, PmWeb/desglose/Page_load   y CargarValoresDefecto, PmWeb/trasladarUsu/Page_load, PmWeb/trasladoOk/Page_load, 
    ''' Tiempo m�ximo: 0,5 seg</remarks>
    Public Sub LoadData(Optional ByVal sCod As String = Nothing, Optional ByVal sNom As String = Nothing, Optional ByVal sApe As String = Nothing,
                        Optional ByVal sUON1 As String = Nothing, Optional ByVal sUON2 As String = Nothing, Optional ByVal sUON3 As String = Nothing, Optional ByVal sDEP As String = Nothing,
                        Optional ByVal bPM As Boolean = False, Optional ByVal bSoloUsuariosQa As Boolean = False, Optional ByVal bCtlBajaLog As Boolean = True,
                        Optional ByVal lDesdeFila As Nullable(Of Long) = Nothing, Optional ByVal lNumFilas As Nullable(Of Long) = Nothing, Optional ByVal sEqp As String = Nothing,
                        Optional ByVal sUO As String = Nothing, Optional ByVal sMaterial As String = Nothing, Optional ByVal bCM As Boolean = False)
        Authenticate()
        moPersonas = DBServer.Personas_Load(sCod, sNom, sApe, sUON1, sUON2, sUON3, sDEP, bPM, bSoloUsuariosQa, bCtlBajaLog, lDesdeFila, lNumFilas, sEqp, sUO, sMaterial, bCM)
    End Sub

    Public Sub New(ByRef dbserver As FSNDataBaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
End Class