﻿Public Class Idiomas
    Inherits Security

    Private moIdiomas As Collection
    Private mData As DataSet
    ReadOnly Property data() As DataSet
        Get
            Return mData
        End Get
    End Property
    Public ReadOnly Property Idiomas() As Collection
        Get
            Return moIdiomas
        End Get
    End Property
    ''' <summary>
    ''' Función que devuelve un dataset con los idiomas de la aplicación
    ''' </summary>
    ''' <remarks>
    ''' Llamada desde: FSNWEB/Cabecera.master/Page_load
    ''' Tiempo: 0 seg
    ''' </remarks>
    Public Sub Load()
        Dim i As Integer

        If System.Web.HttpContext.Current Is Nothing OrElse System.Web.HttpContext.Current.Cache("idiomaAplicacion") Is Nothing Then
            mData = DBServer.Idiomas_Load()
            If Not System.Web.HttpContext.Current Is Nothing Then System.Web.HttpContext.Current.Cache.Insert("idiomaAplicacion", mData, Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration, New TimeSpan(0, System.Configuration.ConfigurationManager.AppSettings("TiempoExpiracionCacheMin"), 0))
        Else
            mData = CType(System.Web.HttpContext.Current.Cache("idiomaAplicacion"), DataSet)
        End If

        If Not data.Tables(0).Rows.Count = 0 Then
            For i = 0 To data.Tables(0).Rows.Count - 1
                Me.Add(mData.Tables(0).Rows(i).Item("COD").ToString(), mData.Tables(0).Rows(i).Item("DEN").ToString())
            Next
        End If
    End Sub
    Friend Sub Add(ByVal sCod As String, ByVal sDen As String)
        Dim oIdi As New Idioma(DBServer, mIsAuthenticated)
        If moIdiomas Is Nothing Then
            moIdiomas = New Collection
        End If
        oIdi.Cod = sCod
        oIdi.Den = sDen
        moIdiomas.Add(oIdi, sCod)
    End Sub
    ''' <summary>
    ''' Obtiene los idiomas activos en la aplicación
    ''' </summary>
    ''' <returns>Un dataset con los idiomas activos</returns>
    ''' <remarks></remarks>
    Public Function Get_IdiomasActivos() As DataTable
        Dim dsIdiomas As DataTable
        dsIdiomas = DBServer.Idiomas_Load().Tables(0)
        dsIdiomas.Columns.Add("DENOMINACION", System.Type.GetType("System.String"))

        Return dsIdiomas
    End Function
    ''' <summary>
    ''' Constructor de la clase Idiomas
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si está autenticado</param>
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
End Class