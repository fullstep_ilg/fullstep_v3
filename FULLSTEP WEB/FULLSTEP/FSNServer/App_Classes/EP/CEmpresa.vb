<Serializable()> _
Public Class CEmpresa
    Inherits Security

    Private m_lId As Integer 'Nos indica la posicion secuencial dentro de una Coleccion
    Private m_sNif As String
    Private m_sDen As String
    Private m_vDireccion As Object
    Private m_vPoblacion As Object
    Private m_vCP As Object
    Private m_vCodPais As Object
    Private m_vCodProvi As Object
    Private m_bERP As Boolean
    Private m_dtFecact As Date
    Private m_oProveERPs As CProveERPs
    Private m_oEnvFacturaDirecciones As CEnvFacturaDirecciones

    Public Property Nif() As String
        Get
            Nif = m_sNif
        End Get
        Set(ByVal Value As String)
            m_sNif = Value
        End Set
    End Property

    Public Property Den() As String
        Get
            Den = m_sDen
        End Get
        Set(ByVal Value As String)
            m_sDen = Value
        End Set
    End Property

    Public Property CP() As Object
        Get
            CP = m_vCP
        End Get
        Set(ByVal Value As Object)
            m_vCP = Value
        End Set
    End Property

    Public Property ERP() As Boolean
        Get
            ERP = m_bERP
        End Get
        Set(ByVal Value As Boolean)
            m_bERP = Value
        End Set
    End Property

    Public Property CodPais() As Object
        Get
            CodPais = m_vCodPais
        End Get
        Set(ByVal Value As Object)
            m_vCodPais = Value
        End Set
    End Property

    Public Property CodProvi() As Object
        Get
            CodProvi = m_vCodProvi
        End Get
        Set(ByVal Value As Object)
            m_vCodProvi = Value
        End Set
    End Property

    Public Property Direccion() As Object
        Get
            Direccion = m_vDireccion
        End Get
        Set(ByVal Value As Object)
            m_vDireccion = Value
        End Set
    End Property

    Public Property Poblacion() As Object
        Get
            Poblacion = m_vPoblacion
        End Get
        Set(ByVal Value As Object)
            m_vPoblacion = Value
        End Set
    End Property

    Public Property ID() As Integer
        Get
            ID = m_lId
        End Get
        Set(ByVal Value As Integer)
            m_lId = Value
        End Set
    End Property

    Public Property FecAct() As Date
        Get
            FecAct = m_dtFecact
        End Get
        Set(ByVal Value As Date)
            m_dtFecact = Value
        End Set
    End Property

    Public Property ProveERPs() As CProveERPs
        Get
            ProveERPs = m_oProveERPs
        End Get
        Set(ByVal Value As CProveERPs)
            m_oProveERPs = Value
        End Set
    End Property

    ''' <summary>
    ''' Direcciones de env�o de la empresa
    ''' </summary>
    Public Property DireccionesEnvioFactura() As CEnvFacturaDirecciones
        Get
            Return m_oEnvFacturaDirecciones
        End Get
        Set(ByVal value As CEnvFacturaDirecciones)
            m_oEnvFacturaDirecciones = value
        End Set
    End Property

    ''' <summary>
    ''' Devuelve un String con el NIF y la denominaci�n concatenados
    ''' </summary>
    ''' <returns>String con el NIF y la denominaci�n</returns>
    Public ReadOnly Property NifDen() As String
        Get
            If Not String.IsNullOrEmpty(m_sNif) Then
                If Not String.IsNullOrEmpty(m_sDen) Then
                    Return String.Concat(m_sNif, " - ", m_sDen)
                Else
                    Return m_sNif
                End If
            Else
                If Not String.IsNullOrEmpty(m_sDen) Then
                    Return m_sDen
                Else
                    Return Nothing
                End If
            End If
        End Get
    End Property
    ''' <summary>
    ''' Constructor de la clase CEmpresa
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si est� autenticado</param>
    Public Sub New(ByRef dbserver As Fullstep.FSNDataBaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub

End Class