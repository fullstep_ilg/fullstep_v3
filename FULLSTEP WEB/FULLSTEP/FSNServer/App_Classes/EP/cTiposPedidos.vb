﻿<Serializable()> _
Public Class cTiposPedidos
    Inherits Lista(Of cTipoPedido)

    ''' <summary>
    ''' Constructor del objeto cTipoPedido
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si está autenticado</param>
    ''' <remarks></remarks>
    Public Sub New(ByRef dbserver As Fullstep.FSNDataBaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
    End Sub
    Public Overloads ReadOnly Property Item(ByVal Cod As String) As cTipoPedido
        Get
            Return Me.Find(Function(elemento As cTipoPedido) elemento.Codigo = Cod)
        End Get
    End Property
    Public ReadOnly Property ItemId(ByVal Id As Integer) As cTipoPedido
        Get
            Return Me.Find(Function(elemento As cTipoPedido) elemento.Id = Id)
        End Get
    End Property
    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub
    ''' <summary>
    ''' Procedimiento que añade un nuevo objeto de tipo cTipoPedido a la colección
    ''' </summary>
    ''' <param name="Cod">Código del Tipo de Pedido</param>
    ''' <param name="Inv">Concepto del tipo de inversion del tipo de pedido</param>
    ''' <param name="Rec">Tipo de recepcionabilidad del pedido</param>
    ''' <param name="Alm">Tipo de almacenamiento del pedido</param>
    ''' <param name="Den">Denominación del tipo de pedido</param>
    ''' <param name="ID">Id en la BBDD del tipo de pedido</param>
    ''' <remarks></remarks>
    Public Overloads Sub Add(ByVal Cod As String, ByVal Inv As cTipoPedido.EnInvTipo, ByVal Rec As cTipoPedido.EnRecTipo, ByVal Alm As cTipoPedido.EnAlmacenTipo, ByVal Den As String, ByVal ID As Integer, ByVal bajaLogica As Boolean)
        Dim ObjNewMember As New cTipoPedido(DBServer, mIsAuthenticated)
        If Not IsNothing(Cod) Then ObjNewMember.Codigo = Cod

        ObjNewMember.Concepto = Inv
        ObjNewMember.Recepcionable = Rec
        ObjNewMember.Almacenable = Alm
        ObjNewMember.Denominacion = Den
        ObjNewMember.Id = ID
        ObjNewMember.BajaLogica = bajaLogica
        MyBase.Add(ObjNewMember)
    End Sub
    ''' <summary>
    ''' Función que devuelve todos los tipos de pedido posibles para seleccionar
    ''' </summary>
    ''' <param name="Idioma">Idioma de la aplicación</param>
    ''' <returns>Un objeto de la clase cTiposPedidos con todos los posibles tipos para seleccionar</returns>
    ''' <remarks>
    ''' Llamada desde: EPWEB\EmisionPedido.aspx.vb\DlCesta_ItemDataBound
    ''' Tiempo máximo: 0,2 seg</remarks>
    Public Function CargarTiposPedido(ByVal Idioma As FSNLibrary.Idioma) As cTiposPedidos
        Dim DsResul As New DataSet
        DsResul = DBServer.TiposPedidos_CargarTiposPedido(Idioma)
        If DsResul.Tables(0).Rows.Count > 0 Then
            For Each fila As DataRow In DsResul.Tables(0).Rows
                Me.Add(fila.Item("Cod"), fila.Item("Inv"), fila.Item("Recep"), fila.Item("Almacen"), fila.Item("Den"), fila.Item("ID"), fila.Item("BAJALOG"))
            Next
            Return Me
        Else
            Return Nothing
        End If
    End Function
End Class
