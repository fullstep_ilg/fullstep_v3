﻿Public Class CDatosGenerales
    Inherits Security
#Region "Properties"
    Private _id As String

    Public Property Id() As Integer
        Get
            Return _id
        End Get
        Set(ByVal value As Integer)
            _id = value
        End Set
    End Property
    Private _anyo As Integer
    Public Property Anyo() As Integer
        Get
            Return _anyo
        End Get
        Set(ByVal value As Integer)
            _anyo = value
        End Set
    End Property
    Private _numOrden As Integer
    Public Property NumOrden() As Integer
        Get
            Return _numOrden
        End Get
        Set(ByVal value As Integer)
            _numOrden = value
        End Set
    End Property
    Private _idPedido As Integer
    Public Property IdPedido() As Integer
        Get
            Return _idPedido
        End Get
        Set(ByVal value As Integer)
            _idPedido = value
        End Set
    End Property
    Private _numPedido As Integer
    Public Property NumPedido() As Integer
        Get
            Return _numPedido
        End Get
        Set(ByVal value As Integer)
            _numPedido = value
        End Set
    End Property
    Private _codProve As String
    Public Property CodProve() As String
        Get
            Return _codProve
        End Get
        Set(ByVal value As String)
            _codProve = value
        End Set
    End Property
    Private _denProve As String
    Public Property DenProve() As String
        Get
            Return _denProve
        End Get
        Set(ByVal value As String)
            _denProve = value
        End Set
    End Property
    Private _codProvePortal As String
    Public Property CodProvePortal() As String
        Get
            Return _codProvePortal
        End Get
        Set(ByVal value As String)
            _codProvePortal = value
        End Set
    End Property
    Private _bProveSolicitAceptacionRecepcion As Boolean
    Public Property ProveSolicitAceptacionRecepcion() As Boolean
        Get
            Return _bProveSolicitAceptacionRecepcion
        End Get
        Set(ByVal value As Boolean)
            _bProveSolicitAceptacionRecepcion = value
        End Set
    End Property
    Private _bOESolicitAceptacionProveedor As Boolean
    Public Property OESolicitAceptacionProveedor() As Boolean
        Get
            Return _bOESolicitAceptacionProveedor
        End Get
        Set(ByVal value As Boolean)
            _bOESolicitAceptacionProveedor = value
        End Set
    End Property
    Private _idEmpresa As Integer
    Public Property IdEmpresa() As Integer
        Get
            Return _idEmpresa
        End Get
        Set(ByVal value As Integer)
            _idEmpresa = value
        End Set
    End Property
    Private _emp As String
    Public Property Emp() As String
        Get
            Return _emp
        End Get
        Set(ByVal value As String)
            _emp = value
        End Set
    End Property
    Private _denEmp As String
    Public Property DenEmp() As String
        Get
            Return _denEmp
        End Get
        Set(ByVal value As String)
            _denEmp = value
        End Set
    End Property
    Private _codReceptor As String
    Public Property CodReceptor() As String
        Get
            Return _codReceptor
        End Get
        Set(ByVal Value As String)
            _codReceptor = Value
        End Set
    End Property
    Private _receptor As String
    Public Property Receptor() As String
        Get
            Return _receptor
        End Get
        Set(ByVal Value As String)
            _receptor = Value
        End Set
    End Property
    Private _referencia As String
    Public Property Referencia() As String
        Get
            Return _referencia
        End Get
        Set(ByVal Value As String)
            _referencia = Value
        End Set
    End Property
    Private _codErp As String
    Public Property CodErp() As String
        Get
            Return _codErp
        End Get
        Set(ByVal Value As String)
            _codErp = Value
        End Set
    End Property
    Private _codigoErp As String
    Public Property CodigoErp() As String
        Get
            Return _codigoErp
        End Get
        Set(ByVal Value As String)
            _codigoErp = Value
        End Set
    End Property
    Private _tipo As cTipoPedido
    Public Property Tipo() As cTipoPedido
        Get
            Tipo = _tipo
        End Get
        Set(ByVal Value As cTipoPedido)
            _tipo = Value
        End Set
    End Property
    Private _codGestor As String
    Public Property CodGestor() As String
        Get
            Return _codGestor
        End Get
        Set(ByVal Value As String)
            _codGestor = Value
        End Set
    End Property
    Private _gestor As String
    Public Property Gestor() As String
        Get
            Return _gestor
        End Get
        Set(ByVal Value As String)
            _gestor = Value
        End Set
    End Property
    Private _dirEnvioDen As String
    Public Property DirEnvioDen() As String
        Get
            Return _dirEnvioDen
        End Get
        Set(ByVal Value As String)
            _dirEnvioDen = Value
        End Set
    End Property
    Private _dirEnvio As Integer
    Public Property DirEnvio() As Integer
        Get
            Return _dirEnvio
        End Get
        Set(ByVal Value As Integer)
            _dirEnvio = Value
        End Set
    End Property
    Private _obs As String
    Public Property Obs() As String
        Get
            Return _obs
        End Get
        Set(ByVal Value As String)
            _obs = Value
        End Set
    End Property
    Private _moneda As String
    Public Property Moneda() As String
        Get
            Return _moneda
        End Get
        Set(ByVal Value As String)
            _moneda = Value
        End Set
    End Property
    Private _nomFav As String
    Public Property NomFav() As String
        Get
            Return _nomFav
        End Get
        Set(ByVal Value As String)
            _nomFav = Value
        End Set
    End Property
    Private mvCambio As Object
    Public Property Cambio() As Object
        Get
            Return mvCambio
        End Get
        Set(ByVal Value As Object)
            mvCambio = Value
        End Set
    End Property
    Private _estIntegracionOrden As Integer
    Public Property EstadoIntegracionOrden() As Integer
        Get
            Return _estIntegracionOrden
        End Get
        Set(ByVal Value As Integer)
            _estIntegracionOrden = Value
        End Set
    End Property
    Private _estOrden As Integer
    Public Property EstadoOrden() As Integer
        Get
            Return _estOrden
        End Get
        Set(ByVal Value As Integer)
            _estOrden = Value
        End Set
    End Property
    Private _estado As Integer
    Public Property Estado() As Integer
        Get
            Return _estado
        End Get
        Set(ByVal Value As Integer)
            _estado = Value
        End Set
    End Property
    Private _fecEstado As Object
    Public Property FechaEstadoOrden() As Object
        Get
            Return _fecEstado
        End Get
        Set(ByVal Value As Object)
            _fecEstado = Value
        End Set
    End Property
    Private _esPedidoAbierto As Boolean
    Public Property EsPedidoAbierto() As Boolean
        Get
            Return _esPedidoAbierto
        End Get
        Set(ByVal value As Boolean)
            _esPedidoAbierto = value
        End Set
    End Property
    Private _gs_pres1 As Pres1_GS
    Public Property GS_Pres1() As Pres1_GS
        Get
            Return _gs_pres1
        End Get
        Set(ByVal value As Pres1_GS)
            _gs_pres1 = value
        End Set
    End Property
    Private _gs_pres2 As Pres2_GS
    Public Property GS_Pres2() As Pres2_GS
        Get
            Return _gs_pres2
        End Get
        Set(ByVal value As Pres2_GS)
            _gs_pres2 = value
        End Set
    End Property
    Private _gs_pres3 As Pres3_GS
    Public Property GS_Pres3() As Pres3_GS
        Get
            Return _gs_pres3
        End Get
        Set(ByVal value As Pres3_GS)
            _gs_pres3 = value
        End Set
    End Property
    Private _gs_pres4 As Pres4_GS
    Public Property GS_Pres4() As Pres4_GS
        Get
            Return _gs_pres4
        End Get
        Set(ByVal value As Pres4_GS)
            _gs_pres4 = value
        End Set
    End Property
    Private _usarOrgCompras As Boolean
    Public Property UsarOrgCompras() As Boolean
        Get
            Return _usarOrgCompras
        End Get
        Set(ByVal value As Boolean)
            _usarOrgCompras = value
        End Set
    End Property
    Private _AcepProve As Nullable(Of Boolean)
    Public Property AcepProve() As Nullable(Of Boolean)
        Get
            Return _AcepProve
        End Get
        Set(ByVal value As Nullable(Of Boolean))
            _AcepProve = value
        End Set
    End Property
    Private _orgCompras As String
    Public Property OrgCompras() As String
        Get
            Return _orgCompras
        End Get
        Set(ByVal value As String)
            _orgCompras = value
        End Set
    End Property
    Private _ordenPedAbierto As Object
    Public Property OrdenPedAbierto As Object
        Get
            Return _ordenPedAbierto
        End Get
        Set(value As Object)
            _ordenPedAbierto = value
        End Set
    End Property
    Private _anyoPedidoAbierto As Object
    Public Property AnyoPedidoAbierto As Object
        Get
            Return _anyoPedidoAbierto
        End Get
        Set(value As Object)
            _anyoPedidoAbierto = value
        End Set
    End Property
    Private _cestaPedidoAbierto As Object
    Public Property CestaPedidoAbierto As Object
        Get
            Return (_cestaPedidoAbierto)
        End Get
        Set(value As Object)
            _cestaPedidoAbierto = value
        End Set
    End Property
    Private _pedidoPedidoAbierto As Object
    Public Property PedidoPedidoAbierto As Object
        Get
            Return _pedidoPedidoAbierto
        End Get
        Set(value As Object)
            _pedidoPedidoAbierto = value
        End Set
    End Property
    Public ReadOnly Property CodPedido As String
        Get
            Return _anyo & "/" & _numPedido & "/" & _numOrden
        End Get
    End Property
    Public ReadOnly Property CodPedidoAbierto As String
        Get
            Return _anyoPedidoAbierto & "/" & _cestaPedidoAbierto & "/" & _pedidoPedidoAbierto
        End Get
    End Property
    Private _tipoPedido As TipoDePedido
    Public Property TipoPedido As TipoDePedido
        Get
            Return _tipoPedido
        End Get
        Set(value As TipoDePedido)
            _tipoPedido = value
        End Set
    End Property
    Public Property Fecha As Object
    Public Property FechaIniPedAbierto As Object
    Public Property FechaFinPedAbierto As Object
    Public Property Aprovisionador As String
    Private _tipoRecepcionAutomatica As Integer
    Public Property TipoRecepcionAutomatica() As Integer
        Get
            Return _tipoRecepcionAutomatica
        End Get
        Set(ByVal value As Integer)
            _tipoRecepcionAutomatica = value
        End Set
    End Property

    Private _DesvioRecepcion As Double
    Public Property DesvioRecepcion() As Double
        Get
            Return _DesvioRecepcion
        End Get
        Set(ByVal value As Double)
            _DesvioRecepcion = value
        End Set
    End Property

    Private _Abono As Boolean
    Public Property Abono As Boolean
        Get
            Return _Abono
        End Get
        Set(ByVal value As Boolean)
            _Abono = value
        End Set
    End Property

    Private _BajaLogica As Boolean
    Public Property BajaLogica As Boolean
        Get
            Return _BajaLogica
        End Get
        Set(ByVal value As Boolean)
            _BajaLogica = value
        End Set
    End Property
    Private _FechaBajaLogica As Object
    Public Property FechaBajaLogica As Object

        Get
            Return _FechaBajaLogica
        End Get
        Set(ByVal value As Object)
            _FechaBajaLogica = value
        End Set
    End Property

    Private _idLogGral As Integer
    Public Property IdLogGral() As Integer
        Get
            Return _idLogGral
        End Get
        Set(ByVal value As Integer)
            _idLogGral = value
        End Set
    End Property

    Private _idEntidad As Integer
    Public Property IdEntidad() As Integer
        Get
            Return _idEntidad
        End Get
        Set(ByVal value As Integer)
            _idEntidad = value
        End Set
    End Property

    Private _idErp As Integer
    Public Property IdErp() As Integer
        Get
            Return _idErp
        End Get
        Set(ByVal value As Integer)
            _idErp = value
        End Set
    End Property

#End Region
    Public Sub New()
        Tipo = New cTipoPedido(DBServer, mIsAuthenticated)
    End Sub
End Class
Public Class Pres1_GS
    Private _activo As Boolean
    Public Property Activo() As Boolean
        Get
            Return _activo
        End Get
        Set(ByVal value As Boolean)
            _activo = value
        End Set
    End Property
    Private _denominacion As String
    Public Property Denominacion() As String
        Get
            Return _denominacion
        End Get
        Set(ByVal value As String)
            _denominacion = value
        End Set
    End Property
    Private _valor As List(Of Valor_GS_Pres)
    Public Property Valor() As List(Of Valor_GS_Pres)
        Get
            Return _valor
        End Get
        Set(ByVal value As List(Of Valor_GS_Pres))
            _valor = value
        End Set
    End Property
    Private _obligatorio As Boolean
    Public Property Obligatorio() As Boolean
        Get
            Return _obligatorio
        End Get
        Set(ByVal value As Boolean)
            _obligatorio = value
        End Set
    End Property
    Private _nivelMinimo As Integer
    Public Property NivelMinimo() As Integer
        Get
            Return _nivelMinimo
        End Get
        Set(ByVal value As Integer)
            _nivelMinimo = value
        End Set
    End Property
End Class
Public Class Pres2_GS
    Private _activo As Boolean
    Public Property Activo() As Boolean
        Get
            Return _activo
        End Get
        Set(ByVal value As Boolean)
            _activo = value
        End Set
    End Property
    Private _denominacion As String
    Public Property Denominacion() As String
        Get
            Return _denominacion
        End Get
        Set(ByVal value As String)
            _denominacion = value
        End Set
    End Property
    Private _valor As List(Of Valor_GS_Pres)
    Public Property Valor() As List(Of Valor_GS_Pres)
        Get
            Return _valor
        End Get
        Set(ByVal value As List(Of Valor_GS_Pres))
            _valor = value
        End Set
    End Property
    Private _obligatorio As Boolean
    Public Property Obligatorio() As Boolean
        Get
            Return _obligatorio
        End Get
        Set(ByVal value As Boolean)
            _obligatorio = value
        End Set
    End Property
    Private _nivelMinimo As Integer
    Public Property NivelMinimo() As Integer
        Get
            Return _nivelMinimo
        End Get
        Set(ByVal value As Integer)
            _nivelMinimo = value
        End Set
    End Property
End Class
Public Class Pres3_GS
    Private _activo As Boolean
    Public Property Activo() As Boolean
        Get
            Return _activo
        End Get
        Set(ByVal value As Boolean)
            _activo = value
        End Set
    End Property
    Private _denominacion As String
    Public Property Denominacion() As String
        Get
            Return _denominacion
        End Get
        Set(ByVal value As String)
            _denominacion = value
        End Set
    End Property
    Private _valor As List(Of Valor_GS_Pres)
    Public Property Valor() As List(Of Valor_GS_Pres)
        Get
            Return _valor
        End Get
        Set(ByVal value As List(Of Valor_GS_Pres))
            _valor = value
        End Set
    End Property
    Private _obligatorio As Boolean
    Public Property Obligatorio() As Boolean
        Get
            Return _obligatorio
        End Get
        Set(ByVal value As Boolean)
            _obligatorio = value
        End Set
    End Property
    Private _nivelMinimo As Integer
    Public Property NivelMinimo() As Integer
        Get
            Return _nivelMinimo
        End Get
        Set(ByVal value As Integer)
            _nivelMinimo = value
        End Set
    End Property
End Class
Public Class Pres4_GS
    Private _activo As Boolean
    Public Property Activo() As Boolean
        Get
            Return _activo
        End Get
        Set(ByVal value As Boolean)
            _activo = value
        End Set
    End Property
    Private _denominacion As String
    Public Property Denominacion() As String
        Get
            Return _denominacion
        End Get
        Set(ByVal value As String)
            _denominacion = value
        End Set
    End Property
    Private _valor As List(Of Valor_GS_Pres)
    Public Property Valor() As List(Of Valor_GS_Pres)
        Get
            Return _valor
        End Get
        Set(ByVal value As List(Of Valor_GS_Pres))
            _valor = value
        End Set
    End Property
    Private _obligatorio As Boolean
    Public Property Obligatorio() As Boolean
        Get
            Return _obligatorio
        End Get
        Set(ByVal value As Boolean)
            _obligatorio = value
        End Set
    End Property
    Private _nivelMinimo As Integer
    Public Property NivelMinimo() As Integer
        Get
            Return _nivelMinimo
        End Get
        Set(ByVal value As Integer)
            _nivelMinimo = value
        End Set
    End Property
End Class
Public Class Valor_GS_Pres
    Private _nivel As Integer
    Public Property Nivel() As Integer
        Get
            Return _nivel
        End Get
        Set(ByVal value As Integer)
            _nivel = value
        End Set
    End Property
    Private _id As Integer
    Public Property Id() As Integer
        Get
            Return _id
        End Get
        Set(ByVal value As Integer)
            _id = value
        End Set
    End Property
    Private _porcentaje As Double
    Public Property Porcentaje() As Double
        Get
            Return _porcentaje
        End Get
        Set(ByVal value As Double)
            _porcentaje = value
        End Set
    End Property

End Class