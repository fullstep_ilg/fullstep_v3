<Serializable()> _
Public Class CCampo
    Inherits Security

    Private m_lId As Integer
    Private m_iNum As Short
    Private m_sDen As Object
    Private m_sIdioma As String
    Private m_bObligatorio As String
    Private m_vValor As Object

    Public Property ID() As Integer
        Get
            ID = m_lId
        End Get
        Set(ByVal Value As Integer)
            m_lId = Value
        End Set
    End Property

    Public Property Num() As Short
        Get
            Num = m_iNum
        End Get
        Set(ByVal Value As Short)
            m_iNum = Value
        End Set
    End Property

    Public Property Den() As Object
        Get
            Den = m_sDen
        End Get
        Set(ByVal Value As Object)
            m_sDen = Value
        End Set
    End Property

    Public Property Idioma() As String
        Get
            Idioma = m_sIdioma
        End Get
        Set(ByVal Value As String)
            m_sIdioma = Value
        End Set
    End Property

    Public Property Obligatorio() As Boolean
        Get
            Obligatorio = CBool(m_bObligatorio)
        End Get
        Set(ByVal Value As Boolean)
            m_bObligatorio = CStr(Value)
        End Set
    End Property

    Public Property Valor() As Object
        Get
            Valor = m_vValor
        End Get
        Set(ByVal Value As Object)
            m_vValor = Value
        End Set
    End Property

    ''' <summary>
    ''' Constructor de la clase CCampo
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si est� autenticado</param>
    Public Sub New(ByRef dbserver As Fullstep.FSNDataBaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
    End Sub
    Public Sub New()
    End Sub

End Class