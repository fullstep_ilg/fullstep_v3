﻿<Serializable()> _
Public Class CProveedor
    Inherits Security

    Private msCod As String
    Private bAutofactura As Boolean

    Property Cod() As String
        Get
            Cod = msCod
        End Get
        Set(ByVal Value As String)
            msCod = Value
        End Set
    End Property

    Property Autofactura() As Boolean
        Get
            Return bAutofactura
        End Get
        Set(ByVal Value As Boolean)
            bAutofactura = Value
        End Set
    End Property

    ''' Revisado por:blp. Fecha: 17/07/2012
    ''' <summary>
    ''' Procedimiento que carga un proveedor dado un código y un idioma especifico
    ''' </summary>
    ''' <param name="CodProve">Código del proveedor</param>
    ''' <remarks>
    ''' Llamada desde FSNServer\App_Classes\EP\CPedido.vb-->Emitir y EmitirPedidoFavorito
    ''' </remarks>
    Public Sub getAutofactura(Optional ByVal CodProve As String = "")
        If CodProve Is Nothing OrElse CodProve = "" Then
            CodProve = msCod
        End If
        Authenticate()
        bAutofactura = DBServer.Proveedor_GetAutofactura(msCod)
    End Sub

    ''' <summary>
    ''' Constructor del objeto
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si está autenticado</param>
    ''' <remarks></remarks>
    Public Sub New(ByRef dbserver As Fullstep.FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
    ''' <summary>Marca la notificación al proveedor como enviada</summary>
    ''' <param name="dtIDs">Id notificación</param>    
    ''' <remarks>Llamada desde: Notificar.NotificarPasoProveedorQAaReal</remarks>
    Public Sub PasoQAaRealNotificado(ByVal dtIDs As DataTable)
        Authenticate()
        DBServer.Proveedor_PasoQAaRealNotificado(dtIDs)
    End Sub
End Class
