﻿Public Class CUnidadesOrganizativas
    Inherits Lista(Of CUnidadOrganizativa)

    ''' <summary>
    ''' Establece o devuelve el elemento con el código especificado
    ''' </summary>
    ''' <param name="Codigo">Código de la unidad organizativa</param>
    ''' <returns>El elemento con el código especificado</returns>
    Public Overloads Property Item(ByVal Codigo As String) As CUnidadOrganizativa
        Get
            Return Me.Find(Function(elemento As CUnidadOrganizativa) elemento.Codigo = Codigo)
        End Get
        Set(ByVal value As CUnidadOrganizativa)
            Dim ind As Integer = Me.FindIndex(Function(elemento As CUnidadOrganizativa) elemento.Codigo = Codigo)
            If ind = -1 Then
                Me.Add(value)
            Else
                Me.Item(ind) = value
            End If
        End Set
    End Property

    ''' <summary>
    ''' Constructor de la clase CUnidadesOrganizativas
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si está autenticado</param>
    Public Sub New(ByRef dbserver As Fullstep.FSNDataBaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
    End Sub

End Class
