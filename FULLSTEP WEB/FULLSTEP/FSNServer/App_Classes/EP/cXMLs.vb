﻿Imports System.Xml
<Serializable()> _
Public Class cXMLs
    Inherits Lista(Of cXML)

    Private strPaqueteSSIS As String
    Private strPathPaqueteSSIS As String
    Private sRPTName As String

    Public Property RPTName As String
        Get
            Return sRPTName
        End Get
        Set(value As String)
            sRPTName = value
        End Set
    End Property

    ''' <summary>
    ''' Propiedad para acceder al valor que contiene el nombre del archivo .Dtsx
    ''' </summary>
    ''' <value>Valor con el nombre del archivo .dtsx</value>
    ''' <returns>El nombre del paquete dtsx de SSIS</returns>
    ''' <remarks>
    ''' Llamada desde: Report_XML.aspx y EjecutarSSIS.aspx
    ''' Tiempo máximo: 0 seg</remarks>
    Public Property PathPaqueteSSIS() As String
        Get
            Return strPathPaqueteSSIS
        End Get
        Set(ByVal value As String)
            strPathPaqueteSSIS = value
        End Set
    End Property

    ''' <summary>
    ''' Propiedad para acceder al valor con el nombre del paquete .Dtsx
    ''' </summary>
    ''' <value>Valor con el nombre del archivo .dtsx</value>
    ''' <returns>El nombre del paquete dtsx de SSIS</returns>
    ''' <remarks>
    ''' Llamada desde: Report_XML.aspx y EjecutarSSIS.aspx
    ''' Tiempo máximo: 0 seg</remarks>
    Public Property PaqueteSSIS() As String
        Get
            Return strPaqueteSSIS
        End Get
        Set(ByVal value As String)
            strPaqueteSSIS = value
        End Set
    End Property

    ''' <summary>
    ''' Constructor del objeto cXMLs
    ''' </summary>
    ''' <param name="dbserver">Objeto servidor de la base de datos</param>
    ''' <param name="isAuthenticated">Variable booleana que indica si el usuario esta autenticado</param>
    ''' <remarks>
    ''' Llamada desde: Todos las clases y/o páginas donde se instancie al objeto.
    ''' Tiempo máximo: 0 seg</remarks>
    Public Sub New(ByRef dbserver As Fullstep.FSNDataBaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
    End Sub

    ''' <summary>
    ''' Función que se encarga de leer un archivo XML y pasar los datos a un objeto de tipo cXML por cada parámetro que contenga
    ''' </summary>
    ''' <param name="Ruta">Ruta completa que indica donde se encuentra el archivo XML a leer</param>
    ''' <remarks>
    ''' Llamada desde: La página Report_XML
    ''' Tiempo máximo: 0,3 seg</remarks>
    Public Sub LeerXML(ByVal Ruta As String)
        Dim XML As New cXML(mDBServer, mIsAuthenticated)
        Dim m_xmlr As New XmlDocument
        Dim TodosNodos As XmlNodeList
        Dim NodoPaqueteSSIS As XmlNode
        m_xmlr.Load(Ruta)
        TodosNodos = m_xmlr.ChildNodes
        TodosNodos = TodosNodos.Item(1).ChildNodes
        NodoPaqueteSSIS = TodosNodos.Item(0)
        PaqueteSSIS = NodoPaqueteSSIS.Attributes.GetNamedItem("Name").InnerText
        PathPaqueteSSIS = NodoPaqueteSSIS.Attributes.GetNamedItem("Path").InnerText
        Dim Parametros As XmlNodeList
        Parametros = TodosNodos.Item(1).ChildNodes
        Dim ValoresPorDefecto As XmlNodeList
        Dim DataSource As XmlNode
        Dim ContadorValoresDefecto As Integer = 0
        Dim ContadorParametros As Integer = 0
        Dim OrigenDatos As cXML.OrigenDeDatos
        Dim ContadorParametrosOrigenDatos As Integer = 0
        For Each NuevoParametro As XmlNode In Parametros
            XML = New cXML(mDBServer, mIsAuthenticated)
            ContadorValoresDefecto = 0
            XML.NombreParametro = NuevoParametro.Attributes.GetNamedItem("Name").InnerText
            XML.TextoAMostrar = NuevoParametro.Item("TextToShow").InnerText
            Select Case NuevoParametro.Item("DataType").InnerText
                Case "Entero"
                    XML.TipoDato = cXML.TipoDeDato.Entero
                Case "Texto"
                    XML.TipoDato = cXML.TipoDeDato.Texto
                Case "Booleano"
                    XML.TipoDato = cXML.TipoDeDato.Booleano
                Case "Real"
                    XML.TipoDato = cXML.TipoDeDato.Real
                Case "Fecha"
                    XML.TipoDato = cXML.TipoDeDato.Fecha
                Case "ArticulosCatalogo"
                    XML.TipoDato = cXML.TipoDeDato.ArticulosCatalogo
                Case "EstructuraMateriales"
                    XML.TipoDato = cXML.TipoDeDato.EstructuraMateriales
                Case "ArbolCategorias"
                    XML.TipoDato = cXML.TipoDeDato.ArbolCategorias
                Case Else
                    XML.TipoDato = cXML.TipoDeDato.Texto
            End Select
            XML.Requerido = NuevoParametro.Item("Required").InnerText = "True"
            XML.MultiplesValores = NuevoParametro.Item("AllowMultiple").InnerText = "True"
            XML.TieneRangoValores = NuevoParametro.Item("RangeValues").InnerText = "True"
            If XML.TieneRangoValores Then
                If Not NuevoParametro.Item("Values") Is Nothing Then
                    ValoresPorDefecto = NuevoParametro.Item("Values").ChildNodes
                    For Each NuevoValor As XmlNode In ValoresPorDefecto
                        ReDim Preserve XML.ValoresPorDefecto(ContadorValoresDefecto)
                        XML.ValoresPorDefecto(ContadorValoresDefecto).Valor = NuevoValor.Attributes.GetNamedItem("value").InnerText
                        XML.ValoresPorDefecto(ContadorValoresDefecto).Texto = NuevoValor.Attributes.GetNamedItem("text").InnerText
                        ContadorValoresDefecto = ContadorValoresDefecto + 1
                    Next
                Else
                    OrigenDatos = New cXML.OrigenDeDatos
                    DataSource = NuevoParametro.Item("DataSource")
                    If Not DataSource Is Nothing Then
                        If DataSource.Attributes.GetNamedItem("type").InnerText = "Stored" Then
                            OrigenDatos.Tipo = cXML.TipoOrigenDatos.Stored
                        End If
                        OrigenDatos.Nombre = DataSource.Item("Name").InnerText
                        If Not DataSource.Item("Parameters") Is Nothing Then
                            ContadorParametrosOrigenDatos = 0
                            For Each ParamDS As XmlNode In DataSource.Item("Parameters").ChildNodes
                                ReDim Preserve OrigenDatos.Parametros(ContadorParametrosOrigenDatos)
                                OrigenDatos.Parametros(ContadorParametrosOrigenDatos).Nombre = ParamDS.Attributes.GetNamedItem("Name").InnerText
                                Select Case ParamDS.Item("DataType").InnerText
                                    Case "Entero"
                                        OrigenDatos.Parametros(ContadorParametrosOrigenDatos).TipoDeDatos = cXML.TipoDeDato.Entero
                                    Case "Real"
                                        OrigenDatos.Parametros(ContadorParametrosOrigenDatos).TipoDeDatos = cXML.TipoDeDato.Real
                                    Case "Texto"
                                        OrigenDatos.Parametros(ContadorParametrosOrigenDatos).TipoDeDatos = cXML.TipoDeDato.Texto
                                    Case "Fecha"
                                        OrigenDatos.Parametros(ContadorParametrosOrigenDatos).TipoDeDatos = cXML.TipoDeDato.Fecha
                                    Case "Booleano"
                                        OrigenDatos.Parametros(ContadorParametrosOrigenDatos).TipoDeDatos = cXML.TipoDeDato.Booleano
                                    Case Else
                                        OrigenDatos.Parametros(ContadorParametrosOrigenDatos).TipoDeDatos = cXML.TipoDeDato.Texto
                                End Select
                                OrigenDatos.Parametros(ContadorParametrosOrigenDatos).ValorDato = ParamDS.Item("Valor").InnerText
                                ContadorParametrosOrigenDatos = ContadorParametrosOrigenDatos + 1
                            Next
                        End If
                        OrigenDatos.CampoAMostrar = DataSource.Item("DataTextField").InnerText
                        OrigenDatos.CampoValor = DataSource.Item("DataValueField").InnerText
                        XML.OrigenDatos = OrigenDatos
                    End If
                End If
            End If
            If NuevoParametro.Item("DefeaultValues").InnerText <> "Null" Then
                XML.ValorPorDefecto = NuevoParametro.Item("DefeaultValues").InnerText
            End If
            If Not NuevoParametro.Item("MinValue").InnerText = "Null" Then
                XML.ValorMinimo = NuevoParametro.Item("MinValue").InnerText
            End If
            If Not NuevoParametro.Item("MaxValue").InnerText = "Null" Then
                XML.ValorMaximo = NuevoParametro.Item("MaxValue").InnerText
            End If
            XML.Indice = ContadorParametros
            XML.TipoParametro = cXML.TipoDeParametro.ParametroForm
            ContadorParametros = ContadorParametros + 1
            Me.Add(XML)
        Next

        'Parámetros internos de configuración
        Dim ParametrosConfig As XmlNodeList
        If Not TodosNodos.Item(2) Is Nothing Then
            ParametrosConfig = TodosNodos.Item(2).ChildNodes
            Dim ContadorParametrosConfig As Integer = 0
            For Each NuevoParametroConfig As XmlNode In ParametrosConfig
                XML = New cXML(mDBServer, mIsAuthenticated)
                XML.NombreParametroConfig = NuevoParametroConfig.Attributes.GetNamedItem("Name").InnerText
                XML.ValorParametroConfig = NuevoParametroConfig.Item("Value").InnerText
                XML.IndiceParametroConfig = ContadorParametrosConfig
                XML.TipoParametro = cXML.TipoDeParametro.ParametroConfig
                ContadorParametrosConfig = ContadorParametrosConfig + 1
                Me.Add(XML)
            Next
        End If
    End Sub

    ''' <summary>
    ''' Función que devuelve el ConnectionString para acceder a la Base de Datos que actualmente este utilizando la aplicación
    ''' </summary>
    ''' <returns>El ConnectionString que utiliza el paquete SSIS para poder ejecutar los stored procedures</returns>
    ''' <remarks>
    ''' Llamada desde: EjecutarSSIS.aspx
    ''' Tiempo máximo: 0 seg</remarks>
    Public Function DevolverPropiedadesConexionAPaqueteSSIS() As String
        Dim ConnectionStringBBDD As String
        ConnectionStringBBDD = "Data Source=" & mDBServer.DBServer & ";User Id=" & mDBServer.DBLogin & ";Initial Catalog=" & mDBServer.DBName & ";Provider=SQLNCLI10.1;Persist Security Info=True;Auto Translate=False;password=" & mDBServer.DBPassword & ";"
        Return ConnectionStringBBDD
    End Function

    ''' <summary>
    ''' Funcion que devuelve el ConnectionString que utiliza el paquete SSIS, para conectarse a los archivos Excel
    ''' </summary>
    ''' <param name="PathArchivoCompleto">Ruta completa donde se encuentra el archivo Excel que quiere escribirse</param>
    ''' <returns>Un sring que contiene el ConnectionString completo para que el paquete SSIS acceda al archivo excel</returns>
    ''' <remarks>
    ''' Llamada desde: EjecutarSSIS.aspx
    ''' Tiempo máximo: 0 seg</remarks>
    Public Function DevolverPropiedadesconexionArchivoExcel(ByVal PathArchivoCompleto As String) As String
        Dim ConnectionString As String
        Dim sCabecera As String = "NO"
        If PathArchivoCompleto.IndexOf("ListadoContabilidad.xls") < 0 Then
            sCabecera = "NO"
        Else
            sCabecera = "YES"
        End If
        ConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & PathArchivoCompleto & ";Extended Properties=" & """Excel 12.0 Xml;HDR=" & sCabecera & """" & ";"
        Return ConnectionString
    End Function
End Class
