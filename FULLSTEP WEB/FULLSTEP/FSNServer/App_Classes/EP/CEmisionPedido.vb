﻿Public Class CEmisionPedido
    Inherits Security
    Public Sub New()
        Adjuntos = New List(Of Adjunto)
        OtrosDatos = New List(Of CAtributoPedido)
        Costes = New List(Of CAtributoPedido)
        Descuentos = New List(Of CAtributoPedido)
        Lineas = New List(Of CLineaPedido)
    End Sub
    Private _datosGenerales As CDatosGenerales
    Public Property DatosGenerales() As CDatosGenerales
        Get
            Return _datosGenerales
        End Get
        Set(ByVal value As CDatosGenerales)
            _datosGenerales = value
        End Set
    End Property
    Private _adjuntos As List(Of Adjunto)
    Public Property Adjuntos() As List(Of Adjunto)
        Get
            Return _adjuntos
        End Get
        Set(ByVal value As List(Of Adjunto))
            _adjuntos = value
        End Set
    End Property
    Private _otrosDatos As List(Of CAtributoPedido)
    Public Property OtrosDatos() As List(Of CAtributoPedido)
        Get
            Return _otrosDatos
        End Get
        Set(ByVal value As List(Of CAtributoPedido))
            _otrosDatos = value
        End Set
    End Property
    Private _costes As List(Of CAtributoPedido)
    Public Property Costes() As List(Of CAtributoPedido)
        Get
            Return _costes
        End Get
        Set(ByVal value As List(Of CAtributoPedido))
            _costes = value
        End Set
    End Property
    Private _descuentos As List(Of CAtributoPedido)
    Public Property Descuentos() As List(Of CAtributoPedido)
        Get
            Return _descuentos
        End Get
        Set(ByVal value As List(Of CAtributoPedido))
            _descuentos = value
        End Set
    End Property
    Private _lineas As List(Of CLineaPedido)
    Public Property Lineas() As List(Of CLineaPedido)
        Get
            Return _lineas
        End Get
        Set(ByVal value As List(Of CLineaPedido))
            _lineas = value
        End Set
    End Property
End Class