﻿Option Strict Off
Option Explicit On
Imports System.Data.SqlClient
Imports Fullstep.FSNLibrary

'Ahora definida como clase publica
<System.Runtime.InteropServices.ProgId("CCategorias_NET.CCategorias")> Public Class CParametros
    Inherits Security

    ''' <summary>
    ''' Constructor de la clase CParametros
    ''' </summary>
    ''' <param name="dbserver">Servidor de la BBDD</param>
    ''' <param name="isAuthenticated">Si está autenticado</param>
    ''' <remarks>
    ''' Llamada desde: Todos los sitios donde se instancie un objeto de esta clase
    ''' Tiempo máximo: 0 sec</remarks>
    Public Sub New(ByRef dbserver As Fullstep.FSNDataBaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
    End Sub
    ''' <summary>
    ''' Función que nos traerá un dataset con los datos necesarios para cargar el arbol de categorías
    ''' </summary>
    ''' <returns>el dataset antes comentado</returns>
    ''' <remarks>
    ''' Llamada desde: El procedimiento CargarArbolCategorías de Seguimiento.aspx.vb
    ''' Tiempo máximo: 0 seg.</remarks>
    Public Function CargarLiteralParametros(ByVal lId As Long, ByVal sIdi As String) As String
        Return DBServer.Parametros_CargarLiteralParametros(lId, sIdi)
    End Function
End Class