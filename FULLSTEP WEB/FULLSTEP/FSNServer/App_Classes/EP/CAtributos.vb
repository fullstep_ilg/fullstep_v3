<Serializable()> _
Public Class CAtributos
    Inherits Lista(Of CAtributo)

    ''' <summary>
    ''' Establece o devuelve el elemento con el ID especificado
    ''' </summary>
    ''' <param name="Id">ID del Atributo</param>
    ''' <returns>El elemento con el ID especificado</returns>
    Public Overloads Property Item(ByVal Id As String) As CAtributo
        Get
            Return Me.Find(Function(elemento As CAtributo) elemento.Id = Id)
        End Get
        Set(ByVal value As CAtributo)
            Dim ind As Integer = Me.FindIndex(Function(elemento As CAtributo) elemento.Id = Id)
            If ind = -1 Then
                Me.Add(value)
            Else
                Me.Item(ind) = value
            End If
        End Set
    End Property

    Public Overloads Function Add(ByVal Id As Object, ByVal Cod As String, ByVal Den As String, ByVal Intro As Short, ByVal Tipo As TiposDeDatos.TipoAtributo, ByVal Valor As Object, ByVal ValorMin As Object, ByVal ValorMax As Object, ByVal Oblig As Boolean, ByVal Operacion As Object, ByVal Aplicar As Object) As CAtributo
        Dim objnewmember As New CAtributo(mDBServer, mIsAuthenticated)

        objnewmember.Cod = Cod
        objnewmember.Den = Den
        objnewmember.Intro = Intro
        objnewmember.Tipo = Tipo
        objnewmember.Id = Id
        objnewmember.Obligatorio = Oblig

        If Not IsNothing(Valor) Then
            objnewmember.Valor = Valor
        Else
            objnewmember.Valor = System.DBNull.Value
        End If

        If Not IsNothing(ValorMin) Then
            objnewmember.ValorMin = ValorMin
        Else
            objnewmember.ValorMin = System.DBNull.Value
        End If

        If Not IsNothing(ValorMax) Then
            objnewmember.ValorMax = ValorMax
        Else
            objnewmember.ValorMax = System.DBNull.Value
        End If

        If IsNothing(Aplicar) Then
            Aplicar = System.DBNull.Value
        End If

        If IsNothing(Operacion) Then
            Operacion = System.DBNull.Value
        End If

        objnewmember.Aplicar = Aplicar
        objnewmember.Operacion = Operacion

        Me.Add(objnewmember)
        Return objnewmember

    End Function


    Public Overloads Function Add(ByVal Id As Object, ByVal Cod As String, ByVal Den As String, ByVal Intro As Short, ByVal Tipo As TiposDeDatos.TipoAtributo, ByVal Valor As Object, ByVal ValorMin As Object, ByVal ValorMax As Object, ByVal Oblig As Boolean) As CAtributo
        Dim objnewmember As New CAtributo(mDBServer, mIsAuthenticated)

        objnewmember.Cod = Cod
        objnewmember.Den = Den
        objnewmember.Intro = Intro
        objnewmember.Tipo = Tipo
        objnewmember.Id = Id
        objnewmember.Obligatorio = Oblig

        If Not IsNothing(Valor) Then
            objnewmember.Valor = Valor
        Else
            objnewmember.Valor = System.DBNull.Value
        End If

        If Not IsNothing(ValorMin) Then
            objnewmember.ValorMin = ValorMin
        Else
            objnewmember.ValorMin = System.DBNull.Value
        End If

        If Not IsNothing(ValorMax) Then
            objnewmember.ValorMax = ValorMax
        Else
            objnewmember.ValorMax = System.DBNull.Value
        End If

        Me.Add(objnewmember)
        Return objnewmember

    End Function

    ''' <summary>
    ''' Constructor de la clase CAtributos
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si est� autenticado</param>
    Public Sub New(ByRef dbserver As Fullstep.FSNDataBaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
    End Sub

End Class