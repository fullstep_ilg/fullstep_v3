﻿Public Class CUnidadOrganizativa
    Inherits Security

    Private _Codigo As String
    Private _Denominacion As String
    Private _ParentUON As CUnidadOrganizativa
    Private _ChildUONs As CUnidadesOrganizativas
    Private _BajaLogica As Boolean
    Private _Moneda As CMoneda
    Private _MesIniAnyoFiscal As Integer
    Private _ArbolPresupuestario As String

    Public Property Codigo() As String
        Get
            Return _Codigo
        End Get
        Set(ByVal value As String)
            _Codigo = value
        End Set
    End Property

    Public Property Denominacion() As String
        Get
            Return _Denominacion
        End Get
        Set(ByVal value As String)
            _Denominacion = value
        End Set
    End Property

    Public Property ParentUON() As CUnidadOrganizativa
        Get
            Return _ParentUON
        End Get
        Set(ByVal value As CUnidadOrganizativa)
            If Not _ParentUON Is Nothing And Not _ParentUON Is value Then
                _ParentUON.ChildUONs.Remove(Me)
            End If
            _ParentUON = value
            If Not value.ChildUONs.Contains(Me) Then
                value.ChildUONs.Add(Me)
            End If
        End Set
    End Property

    Public Property ChildUONs() As CUnidadesOrganizativas
        Get
            Return _ChildUONs
        End Get
        Set(ByVal value As CUnidadesOrganizativas)
            For Each el As CUnidadOrganizativa In value
                el.ParentUON = Me
            Next
            _ChildUONs = value
        End Set
    End Property

    Public Property BajaLogica() As Boolean
        Get
            Return _BajaLogica
        End Get
        Set(ByVal value As Boolean)
            _BajaLogica = value
        End Set
    End Property

    Public Property Moneda() As CMoneda
        Get
            Return _Moneda
        End Get
        Set(ByVal value As CMoneda)
            _Moneda = value
        End Set
    End Property

    Public Property CodMoneda() As String
        Get
            If _Moneda Is Nothing Then
                Return String.Empty
            Else
                Return _Moneda.Cod
            End If
        End Get
        Set(ByVal value As String)
            If _Moneda Is Nothing Then
                _Moneda = New CMoneda(mDBServer, mIsAuthenticated)
            End If
            _Moneda.Cod = value
        End Set
    End Property

    Public Property MesIniAnyoFiscal() As Integer
        Get
            Return _MesIniAnyoFiscal
        End Get
        Set(ByVal value As Integer)
            _MesIniAnyoFiscal = value
        End Set
    End Property

    Public ReadOnly Property Nivel() As Integer
        Get
            If _ParentUON Is Nothing Then
                Return 0
            Else
                Return _ParentUON.Nivel + 1
            End If
        End Get
    End Property

    Public ReadOnly Property UON1() As String
        Get
            Dim el As CUnidadOrganizativa = Me
            While el.Nivel > 0
                el = el.ParentUON
            End While
            Return el.Codigo
        End Get
    End Property

    Public ReadOnly Property UON2() As String
        Get
            If Me.Nivel < 1 Then
                Return String.Empty
            Else
                Dim el As CUnidadOrganizativa = Me
                While el.Nivel > 1
                    el = el.ParentUON
                End While
                Return el.Codigo
            End If
        End Get
    End Property

    Public ReadOnly Property UON3() As String
        Get
            If Me.Nivel < 2 Then
                Return String.Empty
            Else
                Dim el As CUnidadOrganizativa = Me
                While el.Nivel > 2
                    el = el.ParentUON
                End While
                Return el.Codigo
            End If
        End Get
    End Property

    Public ReadOnly Property UON4() As String
        Get
            If Me.Nivel < 3 Then
                Return String.Empty
            Else
                Return _Codigo
            End If
        End Get
    End Property

    ''' <summary>
    ''' Crea el objeto CUnidadOrganizativa y los objetos de nivel superior
    ''' </summary>
    ''' <param name="UON1">Código de la unidad organizativa de nivel 1</param>
    Public Sub SetUON(ByVal UON1 As String)
        SetUON(UON1, String.Empty, String.Empty, String.Empty)
    End Sub

    ''' <summary>
    ''' Crea el objeto CUnidadOrganizativa y los objetos de nivel superior
    ''' </summary>
    ''' <param name="UON1">Código de la unidad organizativa de nivel 1</param>
    ''' <param name="UON2">Código de la unidad organizativa de nivel 2</param>
    Public Sub SetUON(ByVal UON1 As String, ByVal UON2 As String)
        SetUON(UON1, UON2, String.Empty, String.Empty)
    End Sub

    ''' <summary>
    ''' Crea el objeto CUnidadOrganizativa y los objetos de nivel superior
    ''' </summary>
    ''' <param name="UON1">Código de la unidad organizativa de nivel 1</param>
    ''' <param name="UON2">Código de la unidad organizativa de nivel 2</param>
    ''' <param name="UON3">Código de la unidad organizativa de nivel 3</param>
    Public Sub SetUON(ByVal UON1 As String, ByVal UON2 As String, ByVal UON3 As String)
        SetUON(UON1, UON2, UON3, String.Empty)
    End Sub

    ''' <summary>
    ''' Crea el objeto CUnidadOrganizativa y los objetos de nivel superior
    ''' </summary>
    ''' <param name="UON1">Código de la unidad organizativa de nivel 1</param>
    ''' <param name="UON2">Código de la unidad organizativa de nivel 2</param>
    ''' <param name="UON3">Código de la unidad organizativa de nivel 3</param>
    ''' <param name="UON4">Código de la unidad organizativa de nivel 4</param>
    Public Sub SetUON(ByVal UON1 As String, ByVal UON2 As String, ByVal UON3 As String, ByVal UON4 As String)
        If String.IsNullOrEmpty(UON4) Then
            If String.IsNullOrEmpty(UON3) Then
                If String.IsNullOrEmpty(UON2) Then
                    _Codigo = UON1
                Else
                    _Codigo = UON2
                    Dim oUON1 As New CUnidadOrganizativa(mDBServer, mIsAuthenticated)
                    oUON1.SetUON(UON1)
                    Me.ParentUON = oUON1
                End If
            Else
                _Codigo = UON3
                Dim oUON2 As New CUnidadOrganizativa(mDBServer, mIsAuthenticated)
                oUON2.SetUON(UON1, UON2)
                Me.ParentUON = oUON2
            End If
        Else
            _Codigo = UON4
            Dim oUON3 As New CUnidadOrganizativa(mDBServer, mIsAuthenticated)
            oUON3.SetUON(UON1, UON2, UON3)
            Me.ParentUON = oUON3
        End If
    End Sub

    ''' <summary>
    ''' Constructor de la clase CUnidadOrganizativa
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si está autenticado</param>
    Public Sub New(ByRef dbserver As Fullstep.FSNDataBaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
    End Sub

End Class
