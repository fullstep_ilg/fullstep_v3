Option Strict Off
Option Explicit On

Imports System.Data.SqlClient
Imports Fullstep.FSNLibrary
<Serializable()> _
Public Class CProveERPs
    Inherits Lista(Of CProveERP)

    ''' <summary>
    ''' A�ade un objeto CProveERP a la lista
    ''' </summary>
    ''' <param name="sCod">C�digo ERP</param>
    ''' <param name="sDen">Denominaci�n</param>
    ''' <param name="vNIF">NIF</param>
    ''' <param name="vCampo1">Campo 1</param>
    ''' <param name="vCampo2">Campo 2</param>
    ''' <param name="vCampo3">Campo 3</param>
    ''' <param name="vCampo4">Campo 4</param>
    ''' <param name="bBajaLog">Baja L�gica (True/False)</param>
    ''' <param name="bUltimo">�ltimo (True/False)</param>
    ''' <returns>El objeto CProveERP a�adido</returns>
    Public Overloads Function Add(ByVal sCod As String, ByVal sDen As String, ByVal vNIF As Object, ByVal vCampo1 As Object, ByVal vCampo2 As Object, ByVal vCampo3 As Object, ByVal vCampo4 As Object, ByVal bBajaLog As Boolean, ByVal bUltimo As Boolean) As CProveERP
        Dim objnewmember As CProveERP
        objnewmember = New CProveERP
        'Inicializacion de los parametros de entrada que antes eran opcionales
        vNIF = Nothing

        IIf(IsNothing(vCampo1), Nothing, vCampo1)
        IIf(IsNothing(vCampo2), Nothing, vCampo2)
        IIf(IsNothing(vCampo3), Nothing, vCampo3)
        IIf(IsNothing(vCampo4), Nothing, vCampo4)

        bBajaLog = Nothing
        bUltimo = Nothing
        With objnewmember
            .Cod = sCod
            .Den = sDen
            .Nif = vNIF
            .Campo1 = vCampo1
            .Campo2 = vCampo2
            .Campo3 = vCampo3
            .Campo4 = vCampo4
            .BajaLogica = bBajaLog
            .Ultimo = bUltimo
        End With
        Me.Add(objnewmember)

        Return objnewmember
    End Function
    ''' <summary>
    ''' Devuelve los datos de los proveedores ERP para la el proveedor y la organizacion de compras, se utiliza para el campo proveedor ERP(143) de PM
    ''' </summary>
    ''' <param name="sProve">Codigo del proveedor</param>
    ''' <param name="sOrgCompras">Codigo de la organizacion de compras</param>
    ''' <returns>Dataset con los datos de los proveedores ERP</returns>
    ''' <remarks></remarks>
    Public Function CargarProveedoresERPtoDS(Optional ByVal sProve As String = Nothing, Optional ByVal sOrgCompras As String = Nothing, Optional ByVal sCodERP As String = Nothing) As DataSet
        Dim rs As New DataSet
        rs = mDBServer.ProvesErp_CargarProveedoresERPtoDS(sProve, sOrgCompras, sCodERP)
        Return rs
    End Function
    ''' <summary>
    ''' Constructor del objeto Empresas
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si est� autenticado</param>
    ''' <remarks></remarks>
    Public Sub New(ByRef dbserver As Fullstep.FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
    ''' <summary>
    ''' Comprueba si hay que mostrar o no la informacion del Proveedor ERP
    ''' </summary>
    ''' <param name="iEmpresa">Id de la empresa</param>
    ''' <returns>Devuelve true si hay que mostrar la informacion del Proveedor ERP y false en caso contrario</returns>
    ''' <remarks></remarks>
    Public Function CargarCodProveERP(ByVal iEmpresa As Integer) As Boolean
        Dim rs As New DataSet
        rs = mDBServer.ProvesErp_DevuelveCodERP(iEmpresa)
        If Not rs.Tables.Count = 0 Then
            If rs.Tables(0).Rows.Count = 0 Then
                Return False
            Else
                Return True
            End If
        Else
            Return True
        End If
    End Function
End Class