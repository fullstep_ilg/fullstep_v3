Imports System.Configuration
Imports System.Threading
Imports System.Web

<Serializable()> _
Public Class COrden
    Inherits Security

#Region "Properties"
    Private mlId As Integer
    Private mlPedidoId As Integer
    Private msCodProve As String
    Private msProveDen As String
    Private msCodProvePortal As String
    Private mlAnyo As Integer
    Private mlNum As Integer
    Private msNumExt As String
    Private mlEst As Integer
    Private mlTipo As Integer
    Private mvFecha As Object
    Private mdImporte As Double
    Private mlOrdenEst As Integer
    Private msReferencia As Object
    Private msObs As Object
    Private mvMoneda As Object
    Private mvCambio As Object
    Private mvReceptor As Object
    Private mvEmpresa As Object
    Private mvCodErp As Object
    Private oTipoPedido As cTipoPedido
    Private miDireccionEnvioFactura As Integer
    Private msDenominacion As String
    Private miCategoria As Integer
    Private miNivel As Integer
    Private msDenoCategoria As String
    Private moLineas As CLineas
    Private moRecepciones As CRecepciones
    Private moAdjuntos As CAdjuntos
    Private moPedido As CPedido
    Private mlOrdenEstIntegracion As Integer

    Public Property ID() As Integer
        Get
            Return mlId
        End Get
        Set(ByVal Value As Integer)
            mlId = Value
        End Set
    End Property
    Public Property PedidoId() As Integer
        Get
            Return mlPedidoId
        End Get
        Set(ByVal Value As Integer)
            mlPedidoId = Value
        End Set
    End Property
    Public Property Anyo() As Integer
        Get
            Return mlAnyo
        End Get
        Set(ByVal Value As Integer)
            mlAnyo = Value
        End Set
    End Property
    Public Property Prove() As String
        Get
            Return msCodProve
        End Get
        Set(ByVal Value As String)
            msCodProve = Value
        End Set
    End Property
    Public Property ProveDen() As String
        Get
            Return msProveDen
        End Get
        Set(ByVal value As String)
            msProveDen = value
        End Set
    End Property
    Public Property CodProvePortal() As String
        Get
            Return msCodProvePortal
        End Get
        Set(ByVal Value As String)
            msCodProvePortal = Value
        End Set
    End Property
    Public Property Num() As Integer
        Get
            Return mlNum
        End Get
        Set(ByVal Value As Integer)
            mlNum = Value
        End Set
    End Property
    Public Property NumExt() As String
        Get
            Return msNumExt
        End Get
        Set(ByVal Value As String)
            msNumExt = Value
        End Set
    End Property
    Public Property Est() As Integer
        Get
            Return mlEst
        End Get
        Set(ByVal Value As Integer)
            mlEst = Value
        End Set
    End Property
    Public Property Tipo() As Integer
        Get
            Return mlTipo
        End Get
        Set(ByVal Value As Integer)
            mlTipo = Value
        End Set
    End Property
    Public Property Fecha() As Object
        Get
            Return mvFecha
        End Get
        Set(ByVal Value As Object)
            mvFecha = Value
        End Set
    End Property
    Public Property Importe() As Double
        Get
            Return mdImporte
        End Get
        Set(ByVal Value As Double)
            mdImporte = Value
        End Set
    End Property
    Public ReadOnly Property ImporteLineas() As Double
        Get
            Dim dImporte As Double
            If moLineas IsNot Nothing Then
                For Each lin As CLinea In moLineas
                    dImporte = dImporte + lin.PrecUc * lin.FC * lin.CantPed
                Next
            End If
            Return dImporte
        End Get
    End Property
    Public Property OrdenEst() As Integer
        Get
            Return mlOrdenEst
        End Get
        Set(ByVal Value As Integer)
            mlOrdenEst = Value
        End Set
    End Property
    Public Property OrdenEstIntegracion() As Integer
        Get
            Return mlOrdenEstIntegracion
        End Get
        Set(ByVal Value As Integer)
            mlOrdenEstIntegracion = Value
        End Set
    End Property
    Public Property Moneda() As Object
        Get
            Return mvMoneda
        End Get
        Set(ByVal Value As Object)
            mvMoneda = Value
        End Set
    End Property
    Public Property Cambio() As Object
        Get
            Return mvCambio
        End Get
        Set(ByVal Value As Object)
            mvCambio = Value
        End Set
    End Property
    Public Property Referencia() As Object
        Get
            Return msReferencia
        End Get
        Set(ByVal Value As Object)
            msReferencia = Value
        End Set
    End Property
    Public Property Obs() As Object
        Get
            Return msObs
        End Get
        Set(ByVal Value As Object)
            msObs = Value
        End Set
    End Property
    Public Property Adjuntos() As CAdjuntos
        Get
            Return moAdjuntos
        End Get
        Set(ByVal Value As CAdjuntos)
            moAdjuntos = Value
        End Set
    End Property
    Public Property Recepciones() As CRecepciones
        Get


            Return moRecepciones

        End Get
        Set(ByVal Value As CRecepciones)


            moRecepciones = Value

        End Set
    End Property
    Public Property Lineas() As CLineas
        Get


            Return moLineas

        End Get
        Set(ByVal Value As CLineas)


            moLineas = Value

        End Set
    End Property
    Public Property CodErp() As Object
        Get
            Return mvCodErp
        End Get
        Set(ByVal Value As Object)
            mvCodErp = Value
        End Set
    End Property
    Public Property Empresa() As Object
        Get
            Return mvEmpresa
        End Get
        Set(ByVal Value As Object)
            mvEmpresa = Value
        End Set
    End Property
    Public Property Receptor() As Object
        Get
            Return mvReceptor
        End Get
        Set(ByVal Value As Object)
            mvReceptor = Value
        End Set
    End Property
    Public Property Pedido() As CPedido
        Get
            Return moPedido
        End Get
        Set(ByVal value As CPedido)
            moPedido = value
        End Set
    End Property
    Public Property TipoPedido() As cTipoPedido
        Get
            Return oTipoPedido
        End Get
        Set(ByVal value As cTipoPedido)
            oTipoPedido = value
        End Set
    End Property
    Public Property DireccionEnvioFactura() As Integer
        Get
            Return miDireccionEnvioFactura
        End Get
        Set(ByVal value As Integer)
            miDireccionEnvioFactura = value
        End Set
    End Property
    Public Property Categoria() As Integer
        Get
            Return miCategoria
        End Get
        Set(ByVal value As Integer)
            miCategoria = value
        End Set
    End Property
    Public Property Nivel() As Integer
        Get
            Return miNivel
        End Get
        Set(ByVal value As Integer)
            miNivel = value
        End Set
    End Property
    Public Property DenCategoria() As String
        Get
            Return msDenoCategoria
        End Get
        Set(ByVal Value As String)
            msDenoCategoria = Value
        End Set
    End Property
    Private _Ordenes As List(Of Integer)
    Public Property Ordenes() As List(Of Integer)
        Get
            Return _Ordenes
        End Get
        Set(ByVal value As List(Of Integer))
            _Ordenes = value
        End Set
    End Property
    ''' <summary>
    ''' Propiedad que corresponde a la clase COrdenFavoritos. 
    ''' Dado que en EmisionPedido se usa la clase COrden tanto para ordenes comunes como favoritos,
    ''' a�adimos esta propiedad, que s�lo se usar� para contener el dato DEN de un pedido favorito pero no existe para las ordenes comunes y por tanto no
    ''' se guarda en bdd ni nada similar en el caso de una orden com�n
    ''' </summary>
    Public Property Denominacion() As Object
        Get
            Return msDenominacion
        End Get
        Set(ByVal Value As Object)
            msDenominacion = Value
        End Set
    End Property
    Private _idOrdenPedidoAbierto As Integer
    Public Property IdOrdenPedidoAbierto() As Integer
        Get
            Return _idOrdenPedidoAbierto
        End Get
        Set(ByVal value As Integer)
            _idOrdenPedidoAbierto = value
        End Set
    End Property
    Private _anyo_PedidoAbierto As Integer
    Public Property Anyo_PedidoAbierto() As Integer
        Get
            Return _anyo_PedidoAbierto
        End Get
        Set(ByVal value As Integer)
            _anyo_PedidoAbierto = value
        End Set
    End Property
    Private _numPedido_PedidoAbierto As Integer
    Public Property NumPedido_PedidoAbierto() As Integer
        Get
            Return _numPedido_PedidoAbierto
        End Get
        Set(ByVal value As Integer)
            _numPedido_PedidoAbierto = value
        End Set
    End Property
    Private _numOrden_PedidoAbierto As Integer
    Public Property NumOrden_PedidoAbierto() As Integer
        Get
            Return _numOrden_PedidoAbierto
        End Get
        Set(ByVal value As Integer)
            _numOrden_PedidoAbierto = value
        End Set
    End Property
#End Region
    ''' <summary>
    ''' Devuelve un dataset con las l�neas de pedido correspondientes para la orden de entrega actual y los par�metros correspondientes
    ''' </summary>
    ''' <param name="sIdioma">C�digo del idioma</param>
    ''' <returns>Un dataset con las l�neas de pedido correspondientes para la orden de entrega actual y los par�metros correspondientes</returns>
    ''' <remarks>
    ''' Llamada desde: COrden.DevolverLineas
    ''' Tiempo m�ximo: 0 sec.</remarks>
    Public Function DevolverLineas(ByVal sIdioma As String) As DataSet
        Authenticate()
        Dim dsLineas As New DataSet
        dsLineas = DBServer.Orden_DevolverLineas(mlId, sIdioma)
        Return dsLineas
    End Function
    ''' <summary>
    ''' Env�a una notificaci�n indicando que se ha denegado parcialmente una orden
    ''' </summary>
    ''' <param name="Datos">Objeto Collection con los datos de la denegaci�n parcial</param>
    ''' <remarks>Este m�todo se utiliza para realizar el env�o as�ncrono de notificaciones</remarks>
    Private Sub NotificarDenegacionParcial(ByVal Datos As Collection)
        Dim oNotificador As New Notificar(DBServer, mIsAuthenticated)
        Dim mlId As Integer = Datos.Item("ID")
        oNotificador.gIdioma = Datos.Item("IDIOMA")
        oNotificador.NotificarOrdenDenegadaParcialmente(mlId, Nothing)
        oNotificador.FuerzaFinalizeSmtpClient()
        oNotificador = Nothing
    End Sub

    Public Sub NotificarLineasBorradas()
        Dim dt As DataTable
        dt = DBServer.Ordenes_DevolverLineasBorradasANotificar()
        If dt.Rows.Count > 0 Then
            For Each oRow In dt.Rows
                Dim Datos As New Collection()
                Datos.Add("", "IDIOMA")
                Datos.Add(oRow("ORDEN"), "ID")
                'NotificarOrdenBorrada, NotificarOrdenDeshacerBorrado, NotificarLineaBorrada
                Datos.Add("NotificarLineaBorrada", "DONDE")
                Datos.Add(oRow("ORDEN_PED_ABIERTO"), "idOrdenPedAbierto")
                ThreadPool.QueueUserWorkItem(New WaitCallback(AddressOf NotificarBorrado), Datos)
            Next
        End If

    End Sub
    ''' <summary>
    ''' Env�a una notificaci�n indicando que se ha borrado una orden
    ''' </summary>
    ''' <param name="Datos">Objeto Collection con los datos de la denegaci�n parcial</param>
    ''' <remarks>Este m�todo se utiliza para realizar el env�o as�ncrono de notificaciones</remarks>
    Private Sub NotificarBorrado(ByVal Datos As Collection)
        Dim oNotificador As New Notificar(DBServer, mIsAuthenticated)
        Dim mlId As Integer = Datos.Item("ID")
        oNotificador.gIdioma = Datos.Item("IDIOMA")
        oNotificador.NotificarOrdenBorrado(mlId, Nothing, Datos.Item("DONDE"), Datos.Item("idOrdenPedAbierto"))
        oNotificador.FuerzaFinalizeSmtpClient()
        oNotificador = Nothing
    End Sub

    ''' <summary>
    ''' Env�a una notificaci�n indicando que se ha denegado totalmente una orden
    ''' </summary>
    ''' <param name="Datos">Objeto Collection con los datos de la denegaci�n</param>
    ''' <remarks>Este m�todo se utiliza para realizar el env�o as�ncrono de notificaciones</remarks>
    Private Sub NotificarDenegacionTotal(ByVal Datos As Collection)
        Dim oNotificador As New Notificar(DBServer, mIsAuthenticated)
        Dim mlId As Integer = Datos.Item("ID")
        oNotificador.gIdioma = Datos.Item("IDIOMA")
        oNotificador.NotificarOrdenDenegadaTotalmente(mlId, Nothing)
        oNotificador.FuerzaFinalizeSmtpClient()
        oNotificador = Nothing
    End Sub
    ''' <summary>
    ''' Env�a una notificaci�n de emisi�n de pedido
    ''' </summary>
    ''' <param name="Datos">Objeto Collection con los datos del pedido</param>
    ''' <remarks>Este m�todo se utiliza para realizar el env�o as�ncrono de notificaciones</remarks>
    Private Sub NotificarEmision(ByVal Datos As Collection)
        Dim oNotificador As New Notificar(DBServer, mIsAuthenticated)
        Dim mlId As Integer = Datos.Item("ID")
        Dim addressFrom As String = Datos.Item("FROM")
        Dim sFSP As String = Datos.Item("CONEXIONPORTAL")

        If Datos.Item("NOTIFICAR_PROVE") = OrdenEntregaNotificacion.Notificado Then
            oNotificador.gIdioma = Datos.Item("IDIOMA")
            oNotificador.NotificarOrdenAProveedor(mlId, sFSP, addressFrom)
            oNotificador.FuerzaFinalizeSmtpClient()
            oNotificador = Nothing
        End If

        oNotificador = New Notificar(DBServer, mIsAuthenticated)
        oNotificador.gIdioma = Datos.Item("IDIOMA")
        oNotificador.NotificarEmisionAUsuario(mlId, addressFrom)
        oNotificador.FuerzaFinalizeSmtpClient()
        oNotificador = Nothing
    End Sub
    ''' <summary>
    ''' Comprueba si la orden de entrega puede ser denegada por un aprobador
    ''' </summary>
    ''' <param name="CodAprobador">C�digo de persona del aprobador</param>
    ''' <returns>Un valor que indica en qu� situaci�n quedar�a la orden de entrega:
    ''' 1: La orden se traspasar� a un nivel superior de aprobaci�n
    ''' 2: La orden quedar� pendiente de aprobar
    ''' 3: La orden ha sido eliminada por otro usuario
    ''' 4: No se puede emitir y no hay aprobador superior
    ''' 5: La orden quedar� parcialmente denegada
    ''' 6: La orden quedar� totalmente denegada
    ''' </returns>
    Public Function ConfirmarDenegacionPedido(ByVal CodAprobador As String, ByVal motivo As Byte, ByVal instanciaAprob As Long) As Integer
        Return mDBServer.Orden_ComprobarDenegacionOrden(mlId, CodAprobador, motivo, instanciaAprob)
    End Function
    ''' <summary>
    ''' Comprueba si una l�nea de la orden de entrega puede ser aprobada por un aprobador
    ''' </summary>
    ''' <param name="LineaId">ID de la l�nea a aprobar</param>
    ''' <param name="CodAprobador">C�digo de persona del aprobador</param>
    ''' <returns>Un valor con un c�digo que indica en qu� situaci�n quedar�a la orden de entrega:
    ''' 0: La l�nea se aprobar� y la orden quedar� pendiente de aprobaci�n
    ''' 1: Habr� que traspasar la orden a un aprobador superior
    ''' 2: La orden se emitir�
    ''' 3: La l�nea ha sido eliminada por otro usuario
    ''' 4: La orden deber�a ser traspasada pero no existe aprobador superior
    ''' 8: El pedido quedar� parcialmente denegado
    ''' </returns>
    Public Function ConfirmarAprobacionDenegacion(ByVal LineaId As Integer, ByVal CodAprobador As String,
                                                  ByVal Aprobar As Boolean, ByVal Motivo As String) As Integer
        Return DBServer.Orden_ComprobarAprobacionDenegacionLinea(mlId, LineaId, CodAprobador, Aprobar, Motivo)
    End Function
    ''' <summary>
    ''' Comprueba si una orden de entrega puede ser aprobada por un aprobador
    ''' </summary>
    ''' <param name="CodAprobador">C�digo de persona del aprobador</param>
    ''' <returns>Un objeto CTESError que indica en qu� situaci�n quedar�a la orden de entrega:
    ''' NumError 1: La orden se traspasar� a un nivel superior de aprobaci�n
    ''' NumError 2: La orden se emitir� al proveedor
    ''' NumError 3: La orden ha sido eliminada por otro usuario
    ''' NumError 4: No se puede emitir y no hay aprobador superior
    '''             Arg1: Importe del pedido
    '''             Arg2: Importe m�ximo del aprobador
    ''' NumError 5: Hay l�neas denegadas por el aprobador
    ''' NumError 6: Hay l�neas denegadas por otro usuario
    ''' NumError 7: Hay l�neas pendientes de otros aprobadores    
    ''' </returns>
    Public Function ConfirmarAprobacionPedido(ByVal CodAprobador As String, ByVal motivo As Byte, ByVal instanciaAprob As Long) As CTESError
        Dim oTESError As New CTESError()
        oTESError.Arg1 = 0
        oTESError.Arg2 = 0
        oTESError.NumError = DBServer.Orden_ComprobarAprobacionOrden(mlId, CodAprobador, oTESError.Arg1, oTESError.Arg2, motivo, instanciaAprob)
        Return oTESError
    End Function
    ''' <summary>
    ''' Funci�n que Reemite los pedidos parcialmente denegados
    ''' </summary>
    ''' <param name="CodAprovisionador">C�digo de persona del aprovisionador</param>
    ''' <remarks>
    ''' Llamada desde Seguimiento.aspx
    ''' Tiempo m�ximo: 1 sec.</remarks>
    Public Function Reemitir(ByVal CodAprovisionador As String, ByVal PedidoDirectoEnvMail As Boolean) As Long
        Authenticate()
        Dim dr As DataRow = DBServer.Parametros_Portal()
        Dim iINSTWEB As Integer = dr.Item("INSTWEB")
        Dim sFSP_BD As String = IIf(IsDBNull(dr.Item("FSP_BD")), String.Empty, dr.Item("FSP_BD"))
        Dim sFSP_SRV As String = IIf(IsDBNull(dr.Item("FSP_SRV")), String.Empty, dr.Item("FSP_SRV"))
        Dim sCiaComp As String = IIf(IsDBNull(dr.Item("FSP_CIA")), String.Empty, dr.Item("FSP_CIA"))
        dr = Nothing
        Return mDBServer.Orden_Reemitir(mlId, CodAprovisionador, mlPedidoId, iINSTWEB, msCodProve, PedidoDirectoEnvMail, sFSP_SRV, sFSP_BD, sCiaComp)
    End Function
    ''' <summary>
    ''' Notifica la emisi�n de un pedido
    ''' </summary>
    ''' <param name="addressFrom">Email del remitente</param>
    Public Sub NotificarOrden(ByVal addressFrom As String, Optional ByVal iNotificarProve As OrdenEntregaNotificacion = OrdenEntregaNotificacion.Notificado)
        Dim sFSP As String

        'Tratamiento notificaci�n al proveedor para las respuestas dadas en el caso de aprobaci�n por l�mite de importe
        If iNotificarProve = OrdenEntregaNotificacion.RespuestaComunicarEmitir Then
            iNotificarProve = OrdenEntregaNotificacion.Notificado
        ElseIf iNotificarProve = OrdenEntregaNotificacion.RespuestaComunicarIntegrar Then
            iNotificarProve = OrdenEntregaNotificacion.PendienteIntegracion
        ElseIf iNotificarProve = OrdenEntregaNotificacion.RespuestaNoComunicar Then
            iNotificarProve = OrdenEntregaNotificacion.NoNotificado
        ElseIf iNotificarProve = OrdenEntregaNotificacion.PendienteAprobacion Then
            iNotificarProve = OrdenEntregaNotificacion.Notificado
        End If

        Dim dr As DataRow = DBServer.Parametros_Portal()
        Dim iINSTWEB As Integer = dr.Item("INSTWEB")
        Dim sFSP_BD As String = IIf(IsDBNull(dr.Item("FSP_BD")), String.Empty, dr.Item("FSP_BD"))
        Dim sFSP_SRV As String = IIf(IsDBNull(dr.Item("FSP_SRV")), String.Empty, dr.Item("FSP_SRV"))
        Dim sCiaComp As String = IIf(IsDBNull(dr.Item("FSP_CIA")), String.Empty, dr.Item("FSP_CIA"))
        dr = Nothing
        Dim Datos As New Collection()
        Datos.Add(ConfigurationManager.AppSettings("idioma"), "IDIOMA")
        Datos.Add(mlId, "ID")
        Datos.Add(addressFrom, "FROM")
        Datos.Add(iNotificarProve, "NOTIFICAR_PROVE")
        If iINSTWEB = 1 Then
            Datos.Add(String.Empty, "CONEXIONPORTAL")
        Else
            sFSP = sFSP_SRV & "." & sFSP_BD & ".dbo."
            Datos.Add(sFSP, "CONEXIONPORTAL")
        End If
        ThreadPool.QueueUserWorkItem(New WaitCallback(AddressOf NotificarEmision), Datos)

        DBServer.Orden_TratarComunicion(Me.ID, Me.PedidoId, iNotificarProve)
    End Sub
    ''' <summary>Tratamiento de la notificaci�n en la orden</summary>
    ''' <param name="iNotificar">Estado de la notificaci�n</param>
    Public Sub TratarComunicacion(ByVal iNotificar As OrdenEntregaNotificacion)
        If iNotificar = OrdenEntregaNotificacion.RespuestaNoComunicar Then
            iNotificar = OrdenEntregaNotificacion.NoNotificado
        End If
        DBServer.Orden_TratarComunicion(Me.ID, Me.PedidoId, iNotificar)
    End Sub
    ''' <summary>
    ''' Notifica la denegacion parcial de un pedido
    ''' </summary>
    ''' <param name="addressFrom">Email del remitente</param>
    Public Sub NotificarDenegacionParcialOrden(ByVal addressFrom As String)
        Dim sFSP As String

        Dim dr As DataRow = DBServer.Parametros_Portal()
        Dim iINSTWEB As Integer = dr.Item("INSTWEB")
        Dim sFSP_BD As String = IIf(IsDBNull(dr.Item("FSP_BD")), String.Empty, dr.Item("FSP_BD"))
        Dim sFSP_SRV As String = IIf(IsDBNull(dr.Item("FSP_SRV")), String.Empty, dr.Item("FSP_SRV"))
        Dim sCiaComp As String = IIf(IsDBNull(dr.Item("FSP_CIA")), String.Empty, dr.Item("FSP_CIA"))
        dr = Nothing
        Dim Datos As New Collection()
        Datos.Add(ConfigurationManager.AppSettings("idioma"), "IDIOMA")
        Datos.Add(mlId, "ID")
        Datos.Add(addressFrom, "FROM")
        If iINSTWEB = 1 Then
            Datos.Add(String.Empty, "CONEXIONPORTAL")
        Else
            sFSP = sFSP_SRV & "." & sFSP_BD & ".dbo."
            Datos.Add(sFSP, "CONEXIONPORTAL")
        End If
        ThreadPool.QueueUserWorkItem(New WaitCallback(AddressOf NotificarDenegacionParcial), Datos)
    End Sub
    ''' <summary>
    ''' Notifica la denegacion total de un pedido
    ''' </summary>
    ''' <param name="addressFrom">Email del remitente</param>
    Public Sub NotificarDenegacionTotalOrden(ByVal addressFrom As String)
        Dim sFSP As String

        Dim dr As DataRow = DBServer.Parametros_Portal()
        Dim iINSTWEB As Integer = dr.Item("INSTWEB")
        Dim sFSP_BD As String = IIf(IsDBNull(dr.Item("FSP_BD")), String.Empty, dr.Item("FSP_BD"))
        Dim sFSP_SRV As String = IIf(IsDBNull(dr.Item("FSP_SRV")), String.Empty, dr.Item("FSP_SRV"))
        Dim sCiaComp As String = IIf(IsDBNull(dr.Item("FSP_CIA")), String.Empty, dr.Item("FSP_CIA"))
        dr = Nothing
        Dim Datos As New Collection()
        Datos.Add(ConfigurationManager.AppSettings("idioma"), "IDIOMA")
        Datos.Add(mlId, "ID")
        Datos.Add(addressFrom, "FROM")
        If iINSTWEB = 1 Then
            Datos.Add(String.Empty, "CONEXIONPORTAL")
        Else
            sFSP = sFSP_SRV & "." & sFSP_BD & ".dbo."
            Datos.Add(sFSP, "CONEXIONPORTAL")
        End If
        ThreadPool.QueueUserWorkItem(New WaitCallback(AddressOf NotificarDenegacionTotal), Datos)
    End Sub
    ''' <summary>
    ''' Funci�n que devolver� un objeto CAdjuntos con los adjuntos de la orden de entrega actual
    ''' </summary>
    ''' <param name="bCargarEnColeccion">Booleano que indicar� si debemos cargar el objeto en la colecci�n</param>
    ''' <returns>Un objeto CAdjuntos con los adjuntos de la orden de entrega actual</returns>
    ''' <remarks>
    ''' Llamada desde: Seguimiento.aspx
    ''' Tiempo m�ximo: 0 sec</remarks>
    Public Function CargarAdjuntos(ByVal bCargarEnColeccion As Boolean) As CAdjuntos
        Authenticate()
        Dim adoRes As New DataSet
        Dim oAdjuntos As CAdjuntos
        adoRes = mDBServer.Orden_CargarAdjuntos(mlId)

        oAdjuntos = New CAdjuntos(mDBServer, mIsAuthenticated)
        For Each fila As DataRow In adoRes.Tables(0).Rows
            oAdjuntos.Add(fila.Item("ID"), fila.Item("FECACT"), fila.Item("NOM"), fila.Item("DATASIZE"), fila.Item("COM"), Nothing, Nothing, Nothing, Nothing, TiposDeDatos.Adjunto.Tipo.Cesta)
        Next
        adoRes.Clear()
        adoRes = Nothing

        If bCargarEnColeccion Then
            moAdjuntos = oAdjuntos
        End If

        Return oAdjuntos

        oAdjuntos = Nothing

    End Function
    ''' <summary>
    ''' Aprueba una orden de entrega
    ''' </summary>
    ''' <param name="CodAprobador">C�digo de persona del aprobador</param>
    ''' <param name="Idioma">Variable de tipo Idioma con el idioma activo para el aprobador</param>
    ''' <param name="PedidoDirectoEnvMail">True contabiliza �nicamente los pedidos directos, False contabiliza todos</param>
    ''' <returns>
    ''' Un objeto CTESError con informaci�n sobre el resultado de la aprobaci�n
    ''' oTESError.NumError = 0:  Sin errores
    ''' oTESError.Arg1 = 1    :  Orden emitida al proveedor.
    ''' oTESError.Arg1 = 2    :  Lineas traspasadas a nivel superior de aprobaci�n.
    ''' oTESError.Arg1 = 3    :  Lineas pendientes de otros aprobadoRes
    ''' oTESError.Arg1 = 4    :  Lineas denegadas por el aprobador
    ''' oTESError.Arg1 = 5    :  Lineas denegadas por otros aprobadoRes
    ''' oTESError.NumError = 300: 'El pedido ha sido anulado
    ''' oTESError.NumError = 301: 'El pedido ha sido eliminado
    ''' oTESError.NumError = 302: ' El pedido ya ha sido emitida por otro usuario. No se puede cambiar el estado.
    ''' oTESError.NumError = 303: ' El aprobador es el �ltimo del workflow y no tiene importe suficiente para aprobar.
    ''' oTESError.NumError = 304: ' El pedido ya ha pasado de estar pendiente de aprobar.El campo arg1 tendr� el estado en el que se ecnuentra
    ''' </returns>
    ''' <remarks></remarks>
    Public Function AprobarPedido(ByVal CodAprobador As String, ByVal Idioma As FSNLibrary.Idioma, ByVal PedidoDirectoEnvMail As Boolean) As CTESError
        Dim lin As DataRowCollection = Nothing
        Return AprobarPedido(CodAprobador, Idioma, PedidoDirectoEnvMail)
    End Function
    ''' <summary>
    ''' Aprueba una orden de entrega
    ''' </summary>
    ''' <param name="CodAprobador">C�digo de persona del aprobador</param>
    ''' <param name="Idioma">Variable de tipo Idioma con el idioma activo para el aprobador</param>
    ''' <param name="PedidoDirectoEnvMail">True contabiliza todos los pedidos, False contabiliza todos menos los directos</param>
    ''' <param name="LinModif">Par�metro de salida que devuelve una colecci�n con las filas de cat�logo a las que se le ha modificado el precio</param>
    ''' <returns>
    ''' Un objeto CTESError con informaci�n sobre el resultado de la aprobaci�n
    ''' oTESError.NumError = 0:  Sin errores
    ''' oTESError.Arg1 = 1    :  Orden emitida al proveedor.
    ''' oTESError.Arg1 = 2    :  Lineas traspasadas a nivel superior de aprobaci�n.
    ''' oTESError.Arg1 = 3    :  Lineas pendientes de otros aprobadoRes
    ''' oTESError.Arg1 = 4    :  Lineas denegadas por el aprobador
    ''' oTESError.Arg1 = 5    :  Lineas denegadas por otros aprobadoRes
    ''' oTESError.NumError = 300: 'El pedido ha sido anulado
    ''' oTESError.NumError = 301: 'El pedido ha sido eliminado
    ''' oTESError.NumError = 302: ' El pedido ya ha sido emitida por otro usuario. No se puede cambiar el estado.
    ''' oTESError.NumError = 303: ' El aprobador es el �ltimo del workflow y no tiene importe suficiente para aprobar.
    ''' oTESError.NumError = 304: ' El pedido ya ha pasado de estar pendiente de aprobar.El campo arg1 tendr� el estado en el que se ecnuentra
    ''' </returns>
    Public Function AprobarPedido(ByVal CodAprobador As String, ByVal Idioma As FSNLibrary.Idioma, ByVal PedidoDirectoEnvMail As Boolean, ByRef LinModif As DataRowCollection) As CTESError
        Dim oTESError As New CTESError()
        DBServer.Orden_Aprobar(mlId, CodAprobador, PedidoDirectoEnvMail, oTESError.NumError)
        Return oTESError
    End Function
    ''' <summary>
    ''' Aprueba una orden de entrega
    ''' </summary>
    ''' <param name="CodAprobador">C�digo de persona del aprobador</param>
    ''' <param name="Idioma">Variable de tipo Idioma con el idioma activo para el aprobador</param>
    ''' <param name="Comentario">Comentario a incluir en las l�neas denegadas</param>
    ''' <param name="FuerzaDenegarTotal">1-> Desde Aprobacion.aspx. Denegara Total + Notif      
    ''' 2-> Desde AprobacionEmail.aspx O detalle Pedido + Accion Rechazar + No hay lineas aprobadas. Denegara Total      
    ''' 0-> eoc</param>
    ''' <returns>
    ''' Un objeto CTESError con informaci�n sobre el resultado de la denegaci�n
    ''' oTESError.NumError = 0:
    ''' oTESError.Arg1 = 2: Sube a un nivel superior
    ''' oTESError.Arg1 = 1: Queda pendiente de aprobar
    ''' oTESError.Arg1 = 3: Queda parcialx denegado, lineas aprobadas por otros
    ''' oTESError.Arg1 = 4: Pedido denegado del todo
    ''' oTESError.NumError = 300: 'El pedido ha sido anulado
    ''' oTESError.NumError = 301: 'El pedido ha sido eliminado
    ''' oTESError.NumError = 302: ' La orden ya ha sido emitida por otro usuario. No se puede cambiar el estado.
    ''' oTESError.NumError = 303: ' Queda pendiente y no hay nivel superior
    ''' oTESError.NumError = 304: ' La orden ya ha pasado de estar pendiente de aprobar.El campo arg1 tendr� el estado en el que se ecnuentra
    ''' </returns>
    Public Function DenegarPedido(ByVal CodAprobador As String, ByVal Idioma As FSNLibrary.Idioma, ByVal Comentario As String, Optional ByVal FuerzaDenegarTotal As Integer = 0) As CTESError
        Dim oTESError As New CTESError()
        Dim ds As DataSet = DBServer.Orden_Denegar(mlId, CodAprobador, Comentario _
                                                     , oTESError.NumError, oTESError.Arg1, oTESError.Arg2, oTESError.Arg3, FuerzaDenegarTotal)

        If (Not FuerzaDenegarTotal = 2) Then
            If oTESError.Arg1 = "3" And mlOrdenEst <> 1 Then
                Dim Datos As New Collection()
                Datos.Add(Idioma.ToString(), "IDIOMA")
                Datos.Add(mlId, "ID")
                ThreadPool.QueueUserWorkItem(New WaitCallback(AddressOf NotificarDenegacionParcial), Datos)
            End If
            If oTESError.Arg1 = "4" Then
                Dim Datos As New Collection()
                Datos.Add(Idioma.ToString(), "IDIOMA")
                Datos.Add(mlId, "ID")
                ThreadPool.QueueUserWorkItem(New WaitCallback(AddressOf NotificarDenegacionTotal), Datos)
            End If
        End If

        Return oTESError
    End Function

    ''' <summary>
    ''' Determina si hay alguna linea aprobada
    ''' </summary>
    ''' <returns></returns>
    Public Function Comprobar_HayLineasAprobadas() As Boolean
        Authenticate()
        Return DBServer.Comprobar_HayLineasAprobadas(mlId)
    End Function

    ''' <summary>Funci�n que devuelve un dataset con las UONs a las que el usuario no puede imputar presupuestos</summary>
    ''' <param name="CodAprovisionador">Codigo de la persona aprovisiondora</param>
    ''' <returns>Un dataset con las UONs a las que el usuario no puede imputar presupuestos</returns>
    ''' <remarks>
    ''' Llamada desde: Seguimiento.aspx
    ''' Tiempo m�ximo: 0 sec</remarks>
    Public Function NoImputacion(ByVal CodAprovisionador As String) As DataSet
        Authenticate()
        Return mDBServer.Orden_NoImputacion(mlId, CodAprovisionador)
    End Function
    ''' <summary>
    ''' Comprueba si una orden ha sido modificada despu�s de una fecha dada
    ''' </summary>
    ''' <param name="FechaModificacion">Fecha con la que comparar</param>
    ''' <returns>True si ha sido modificada, si no False</returns>
    Public Function Modificada(ByVal FechaModificacion As DateTime) As Boolean
        Return mDBServer.Orden_Modificada(mlId, FechaModificacion)
    End Function
    ''' <summary>
    ''' Comprueba si se ha realizado correctamente la integraci�n de la orden de entrega
    ''' </summary>
    ''' <returns>True si no hay integraci�n de salida o la orden de entrega se ha integrado correctamente.
    ''' Si no devuelve False.</returns>
    Public Function ValidarIntegracion() As Boolean
        Return mDBServer.Orden_ValidarIntegracion(mlId)
    End Function
    ''' <summary>
    ''' Actualiza la cabecera de la cesta con la informaci�n de la orden
    ''' </summary>
    Public Sub ActualizarCesta(ByVal Persona As String)
        Dim IdTipoPedido As Integer
        If oTipoPedido IsNot Nothing Then IdTipoPedido = oTipoPedido.Id
        mDBServer.Orden_ActualizarCesta(Persona, msCodProve, mvMoneda.ToString(), msReferencia, mvEmpresa, mvCodErp, mvReceptor, IdTipoPedido, msObs, miDireccionEnvioFactura)
    End Sub

    ''' <summary>Lleva a cabo el borrado l�gico de la orden</summary>
    ''' <param name="OrdenId">Id Orden</param>
    ''' <param name="Usu">Usu</param>
    ''' <param name="Per">Per</param>
    ''' <returns></returns>
    Public Function BorrarOrden(ByVal OrdenId As Integer, ByVal Usu As String, ByVal Per As String, ByVal AccesoFSIM As Boolean, ByVal dImporteTotalOrden As Double, ByVal dImporteCostes As Double, ByVal dImporteDescuentos As Double,
                                ByVal Idioma As FSNLibrary.Idioma, ByVal idOrdenPedAbierto As Integer) As Boolean
        Authenticate()
        Dim bCorrecto As Boolean = mDBServer.Orden_BorrarOrden(OrdenId, Usu, Per, AccesoFSIM, dImporteTotalOrden, dImporteCostes, dImporteDescuentos)
        If bCorrecto And idOrdenPedAbierto > 0 Then
            Dim Datos As New Collection()
            Datos.Add(Idioma.ToString(), "IDIOMA")
            Datos.Add(OrdenId, "ID")
            'NotificarOrdenBorrada, NotificarOrdenDeshacerBorrado, NotificarLineaBorrada, NotificarLineaDeshacerBorrado
            Datos.Add("NotificarOrdenBorrada", "DONDE")
            Datos.Add(idOrdenPedAbierto, "idOrdenPedAbierto")
            ThreadPool.QueueUserWorkItem(New WaitCallback(AddressOf NotificarBorrado), Datos)
        End If
        Return bCorrecto
    End Function
    Public Function DeshacerBorrarOrden(ByRef oDatosOrden As Object, ByVal bAccesoFSSM As Boolean, ByVal sUsuario As String, ByVal Idioma As FSNLibrary.Idioma, ByVal idOrdenPedAbierto As Integer) As Boolean
        Authenticate()
        Dim bCorrecto As Boolean = mDBServer.Orden_DeshacerBorrarOrden(oDatosOrden, bAccesoFSSM, sUsuario)
        If bCorrecto And idOrdenPedAbierto > 0 Then
            Dim Datos As New Collection()
            Datos.Add(Idioma.ToString(), "IDIOMA")
            Datos.Add(oDatosOrden("IdOrden"), "ID")
            'NotificarOrdenBorrada, NotificarOrdenDeshacerBorrado, NotificarLineaBorrada, NotificarLineaDeshacerBorrado
            Datos.Add("NotificarOrdenDeshacerBorrado", "DONDE")
            Datos.Add(idOrdenPedAbierto, "idOrdenPedAbierto")
            ThreadPool.QueueUserWorkItem(New WaitCallback(AddressOf NotificarBorrado), Datos)
        End If
        Return bCorrecto
    End Function

    ''' <summary>
    ''' Cambia el estado de la orden como par�metro a Anulado (estado 20) y envia un email de notificaci�n cuando la anulaci�n se efect�a.
    ''' </summary>
    ''' <param name="idioma">Idioma del usuario. Se usar� para la notificaci�n de la anulaci�n al proveedor cuando el proveedor no tenga una idioma configurado o no se pueda recuperar su idioma</param>
    ''' <param name="sCodPersona">Codigo Persona</param>
    ''' <returns>1 si ha ido bien, 0 si se ha producido un error</returns>
    ''' <remarks>Llamada desde seguimiento.aspx.vb->btnAceptarConfirm_click. Tiempo m�x. inferior a 1 seg</remarks>
    Public Function AnularOrden(ByVal idioma As FSNLibrary.Idioma, ByVal sCodPersona As String, ByVal bAccesoSM As Boolean) As Integer
        Authenticate()
        'Anular
        Dim rdoAnulacion As Integer = mDBServer.Orden_AnularOrden(mlId, sCodPersona, idioma, bAccesoSM)

        'Notificar al proveedor
        If rdoAnulacion = 1 And mlOrdenEst >= TipoEstadoOrdenEntrega.EmitidoAlProveedor Then
            Dim Datos As New Collection()
            Datos.Add(idioma.ToString(), "IDIOMA")
            Datos.Add(mlId, "ID")
            ThreadPool.QueueUserWorkItem(New WaitCallback(AddressOf NotificarAnulacion), Datos)
        End If

        Return rdoAnulacion
    End Function
    ''' <summary>
    ''' Procedimiento que enviara una notificaci�n cuando se haya producido un error en la aprobaci�n via mail
    ''' </summary>
    ''' <param name="iOrdenId">id de la orden</param>
    ''' <param name="sIdioma">idioma</param>
    ''' <param name="sCodUsuAprob">Codigo de usuario aprobador</param>
    ''' <param name="iCodSeguridad">Codigo de seguridad</param>
    ''' <remarks></remarks>
    Public Sub NotificarErrorAprobacion(ByVal iOrdenId As Integer, ByVal sIdioma As String, ByVal sCodUsuAprob As String, ByVal iCodSeguridad As Int32, ByVal Seguridad_Nivel As Byte)
        Dim oNotificador As New Notificar(DBServer, mIsAuthenticated)
        oNotificador.gIdioma = sIdioma
        oNotificador.NotificacionErrorEnAprobacion(iOrdenId, "", sIdioma, sCodUsuAprob, iCodSeguridad, Seguridad_Nivel)
        oNotificador.FuerzaFinalizeSmtpClient()
        oNotificador = Nothing
    End Sub
    ''' <summary>
    ''' Procedimiento que enviara una notificaci�n cuando se haya producido un error en el rechazo via mail
    ''' </summary>
    ''' <param name="iOrdenId">id de la orden</param>
    ''' <param name="sIdioma">idioma</param>
    ''' <param name="sCodUsuAprob">Codigo de usuario aprobador</param>
    ''' <param name="iCodSeguridad">Codigo de seguridad</param>
    ''' <remarks></remarks>
    Public Sub NotificarErrorRechazo(ByVal iOrdenId As Integer, ByVal sIdioma As String, ByVal sCodUsuAprob As String, ByVal iCodSeguridad As Int32, ByVal Seguridad_Nivel As Byte)
        Dim oNotificador As New Notificar(DBServer, mIsAuthenticated)
        oNotificador.gIdioma = sIdioma
        oNotificador.NotificacionErrorEnRechazo(iOrdenId, "", sIdioma, sCodUsuAprob, iCodSeguridad, Seguridad_Nivel)
        oNotificador.FuerzaFinalizeSmtpClient()
        oNotificador = Nothing
    End Sub
    ''' <summary>
    ''' Env�a una notificaci�n indicando que se ha anulado una orden
    ''' </summary>
    ''' <param name="Datos">Objeto Collection con los datos de la anulaci�n</param>
    ''' <remarks>Este m�todo se utiliza para realizar el env�o as�ncrono de notificaciones</remarks>
    Private Sub NotificarAnulacion(ByVal Datos As Collection)
        Dim oNotificador As New Notificar(DBServer, mIsAuthenticated)
        Dim mlId As Integer = Datos.Item("ID")
        Dim sFSP As String
        oNotificador.gIdioma = Datos.Item("IDIOMA")

        Dim dr As DataRow = DBServer.Parametros_Portal()
        Dim iINSTWEB As Integer = dr.Item("INSTWEB")
        Dim sFSP_BD As String = IIf(IsDBNull(dr.Item("FSP_BD")), String.Empty, dr.Item("FSP_BD"))
        Dim sFSP_SRV As String = IIf(IsDBNull(dr.Item("FSP_SRV")), String.Empty, dr.Item("FSP_SRV"))
        Dim sCiaComp As String = IIf(IsDBNull(dr.Item("FSP_CIA")), String.Empty, dr.Item("FSP_CIA"))
        dr = Nothing
        If iINSTWEB = 1 Then
            sFSP = String.Empty
        Else
            sFSP = sFSP_SRV & "." & sFSP_BD & ".dbo."
        End If

        oNotificador.NotificarAnulacionDeOrdenAProveedor(mlId, sFSP, Nothing)
        oNotificador.FuerzaFinalizeSmtpClient()
        oNotificador = Nothing
    End Sub
    ''' <summary>
    ''' M�todo que comprueba si el usuario es aprovisionador en las categor�as de las l�neas/articulos del favorito
    ''' </summary>
    ''' <returns>
    ''' Si lo es: devuelve un datatable vac�o 
    ''' Si NO lo es: devuelve un datatable con los art�culos que no puede emitir
    ''' </returns>
    ''' <param name="CodUsu">C�digo del usuario</param>
    ''' <remarks>Llamada desde EmisionPedido.aspx.vb-->Emitir-->userHasItemsCategories</remarks>
    Public Function UserCanIssueItemsCategories(ByVal CodUsu As String) As DataTable
        Dim oUserCanIssueItemsCategories As DataTable = mDBServer.User_CanIssueItemsCategories(CodUsu, Me.ID)
        Return oUserCanIssueItemsCategories
    End Function
    ''' <summary>
    ''' Funcion que comprueba si con el numero aleatorio que se pasa el la id de la orden se encuentra un pedido para aprobarse y devuelve el id del pedido
    ''' </summary>
    ''' <param name="iOrdenId">id de la orden</param>
    ''' <param name="iNumRnd">numero aleatorio que se pasa desde el link del mail de aprobacion de pedido</param>
    ''' <returns>id del pedido</returns>
    ''' <remarks></remarks>
    Public Function ValidarNumRnd(ByVal iOrdenId As Integer, ByVal iNumRnd As Integer) As String
        Return mDBServer.Orden_ValidarNumRnd(iOrdenId, iNumRnd)
    End Function
    ''' <summary>
    ''' Graba si se aprueba o rechaza un pedido desde email
    ''' </summary>
    ''' <param name="sAccion">accion que se realiza</param>
    ''' <param name="idOrden">id de la orden</param>
    ''' <param name="sIdioma">idioma</param>
    ''' <remarks></remarks>
    Public Sub GrabarAccionEmail(ByVal sAccion As String, ByVal idOrden As Long, ByVal sIdioma As String)
        Dim oNotificador As New Notificar(DBServer, mIsAuthenticated)
        oNotificador.gIdioma = sIdioma
        oNotificador.GrabarNumeroAleatorioLineasPedido(0, sIdioma, idOrden, sAccion)
        oNotificador.FuerzaFinalizeSmtpClient()
        oNotificador = Nothing
    End Sub
    ''' <summary>Realiza las comprobaciones previas al borrado (l�gico) de un pedido</summary>
    ''' <param name="oPedido">Objeto con los datos de la orden</param>    
    ''' <returns></returns>
    Public Function ComprobarBorrarOrden(ByVal oPedido As CDetallePedido, ByVal TipoAccesoFSIM As Integer, ByVal sIdioma As String, ByVal bHitosFact As Boolean) As Object
        'IdOrden: sID, Empresa: gDatosGenerales.IdEmpresa, EsPedidoAbierto: (gDatosGenerales.TipoPedido == 5), Estado: gDatosGenerales.Estado, EsAbonoONorecepcionable: (gDatosGenerales.Abono || (gDatosGenerales.Tipo.Recepcionable == 0))
        'IdOrden, Empresa, EsPedidoAbierto, Estado, EsAbonoONorecepcionable, IIf(FSNServer.TipoAcceso.g_bAccesoFSFA, 1, 2)
        Authenticate()
        Dim oRet As Object = DBServer.Orden_ComprobarBorrarOrden(oPedido.DatosGenerales.Id, (oPedido.DatosGenerales.TipoPedido = 5), oPedido.DatosGenerales.Estado, (oPedido.DatosGenerales.Abono Or oPedido.DatosGenerales.Tipo.Recepcionable = 0), TipoAccesoFSIM)
        If oRet = 0 Then
            'Comprobar hitos
            If bHitosFact AndAlso (oPedido.DatosGenerales.TipoPedido = TipoDePedido.Directo Or oPedido.DatosGenerales.TipoPedido = TipoDePedido.PedidosPM) Then
                If DBServer.Orden_ComprobarTieneHitosFacturacion(oPedido.DatosGenerales.Id) Then oRet = 8
            End If

            If oRet = 0 Then
                Dim bIntegracion As Boolean
                Dim cEmisionPedidos As New CEmisionPedidos(DBServer, mIsAuthenticated)
                If oPedido.DatosGenerales.IdEmpresa <> 0 Then bIntegracion = cEmisionPedidos.Comprobar_Integracion(oPedido.DatosGenerales.IdEmpresa)

                If bIntegracion Then
                    Try
                        'Llamada a comprobaciones mapper previas al borrado                      
                        Dim oEP_ValidacionesIntegracion As EP_ValidacionesIntegracion = New EP_ValidacionesIntegracion(DBServer, mIsAuthenticated)
                        If Not oEP_ValidacionesIntegracion.HayIntegracionSalidaWCF(TablasIntegracion.PED_Aprov, oPedido.DatosGenerales.IdEmpresa) Then
                            'Llamada a la mapper de vb6            
                            ''Obtiene el nombre de la Mapper en funci�n de la Empresa
                            Dim sNomMapper As String = DBServer.Devolver_Nombre_Mapper(, oPedido.DatosGenerales.Id)

                            Dim oMapper As Object = CreateObject(sNomMapper & ".clsInPedidosDirectos")
                            If Not oMapper Is Nothing Then
                                Dim Instancia As String = System.Configuration.ConfigurationManager.AppSettings("instanciamaper")
                                Dim sError As String = ""
                                Dim iNumError As Integer = 0
                                Dim arrIdLineas(0) As Integer
                                If Not oMapper.TratarLineaEliminada(Instancia, iNumError, sError, oPedido.DatosGenerales.Id, arrIdLineas, True) Then
                                    If iNumError = 0 And sError <> "" Then
                                        ''Si no hay identificativo del error, se muestra el texto:
                                        Return sError
                                    Else
                                        Dim oTextosIntegracion As New TextosIntegracion(DBServer, mIsAuthenticated)
                                        Return oTextosIntegracion.MensajeIntegracionError(MapperModuloMensaje.PedidoDirecto, iNumError, sIdioma, sError)
                                    End If
                                End If
                            End If
                        Else
                            'Llamada via web service                        
                            Dim strError As String = ""
                            Dim iErp As Integer
                            Dim oIntegracion As New Integracion(DBServer, True)
                            Dim oSerializer As New Script.Serialization.JavaScriptSerializer()
                            Dim sPedido As String = oSerializer.Serialize(oPedido)

                            iErp = CInt(oIntegracion.getErpFromEmpresa(oPedido.DatosGenerales.IdEmpresa))
                            If iErp > 0 Then 'Si esta comprobación no es cierta, no se integra ya no hay ERP.
                                'llamamos al webservice
                                Dim oEPVal As New EP_ValidacionesIntegracion(DBServer, mIsAuthenticated)
                                strError = oEPVal.RemoteValidarBorradoPedidoWCF(sPedido, TablasIntegracion.PED_Aprov, sIdioma, iErp)
                            End If

                            If IsNothing(strError) Or strError = "" Then
                                Return 0
                            Else
                                Return strError
                            End If
                        End If

                    Catch ex As Exception
                        'No se trata 
                        Return 0
                    End Try
                End If
            End If
        End If
        Return oRet
    End Function
    ''' <summary>Comprueba si un pedido o linea de pedido tiene hitos de facturacion</summary>
    ''' <returns>Booleano indicando si la orden o l�nea tienen hitos de facturaci�n</returns>
    Public Function ComprobarHitosFacturacion() As Boolean
        Authenticate()
        Return DBServer.Orden_ComprobarTieneHitosFacturacion(mlId)
    End Function
    ''' <summary>Realiza las comprobaciones previas a deshacer el borrado (l�gico) de un pedido</summary>
    ''' <param name="OrdenID">Id Orden</param>    
    ''' <param name="Estado">Estado orden</param>
    ''' <param name="TipoPedido">Tipo de pedido</param>
    ''' <returns></returns>
    Public Function ComprobarDeshacerBorrarOrden(ByVal OrdenID As Integer, ByVal Estado As Integer, ByVal TipoPedido As Integer) As Object
        Authenticate()
        Return DBServer.Orden_ComprobarDeshacerBorrarOrden(OrdenID, Estado, TipoPedido, String.Empty)
    End Function
    ''' <summary>Realiza las comprobaciones de recepcion de lineas para tipos pedido de recepcion obligatoria pedido</summary>
    ''' <param name="OrdenID">Id Orden</param>        
    ''' <returns></returns>
    Public Function ComprobarCerrarOrden(ByVal OrdenID As Integer) As Boolean
        Authenticate()
        Return DBServer.Orden_ComprobarCerrarOrden(OrdenID)
    End Function
    ''' Revisado por: blp. Fecha: 17/02/2012
    ''' <summary>
    ''' M�todo que cierra la orden
    ''' </summary>
    ''' <param name="Persona">C�digo de la persona que efect�a el cambio</param>
    ''' <remarks>llamada desde recepcion.aspx.vb. Tiempo M�x inferior a 1 seg.</remarks>
    Public Function CerrarOrden(ByVal Persona As String) As Integer
        Authenticate()
        Dim rdoCerrar As Integer = DBServer.Orden_CerrarOrden(ID, Persona)
        Return rdoCerrar
    End Function
    Public Function CerrarOrdenes(ByVal Persona As String) As Integer
        Authenticate()
        Dim rdoCerrar As Integer = DBServer.Orden_CerrarOrdenes(Ordenes, Persona)
        Return rdoCerrar
    End Function
    ''' Revisado por: blp. Fecha: 17/02/2012
    ''' <summary>
    ''' M�todo que reabre la orden
    ''' </summary>
    ''' <param name="Persona">C�digo de la persona que efect�a el cambio</param>
    ''' <returns>
    ''' Devuelve el estado en el que se deja la orden. 
    ''' En el stored se comprueba si la orden tiene facturas. Si tiene, no se actuliza y devuelve 0.
    ''' </returns>
    ''' <remarks>llamada desde recepcion.aspx.vb. Tiempo M�x inferior a 1 seg.</remarks>
    Public Function ReabrirOrden(ByVal Persona As String) As Integer
        Authenticate()
        Dim iEstadoNuevo As Integer = DBServer.Orden_ReabrirOrden(ID, Persona)
        Return iEstadoNuevo
    End Function
    ''' Revisado por: blp. Fecha: 28/02/2012
    ''' <summary>
    ''' Devuelve las observaciones de la orden
    ''' </summary>
    ''' <returns>String Observaciones</returns>
    ''' <remarks>Llamada desde Recepcion.vb->GetObservacionesOrden(). M�x 0,1 seg.</remarks>
    Public Function GetObservaciones() As String
        Authenticate()
        Dim sObservaciones As String = DBServer.Orden_GetObservaciones(ID)
        Return sObservaciones
    End Function
    ''' Revisado por: blp. Fecha: 28/02/2012
    ''' <summary>
    ''' Devuelve el comentario del proveedor de la orden
    ''' </summary>
    ''' <returns>String Comentario del proveedor</returns>
    ''' <remarks>Llamada desde Recepcion.vb->GetComentarioProveOrden(). M�x 0,1 seg.</remarks>
    Public Function GetComentarioProve() As String
        Authenticate()
        Dim sObservaciones As String = DBServer.Orden_GetComentarioProve(ID)
        Return sObservaciones
    End Function
    ''' Revisado por: dsl. Fecha: 10/12/2012
    ''' <summary>
    ''' Funcion que devuelve la fecha de la orden
    ''' </summary>
    ''' <param name="IdOrden">Id de la orden</param>
    ''' <returns>Fecha de la orden</returns>
    ''' <remarks></remarks>
    Public Function ObtenerFecha(ByVal IdOrden As Long) As Date
        Authenticate()
        Dim dFechaOrden As Date = DBServer.Orden_ObtenerFecha(IdOrden)
        Return dFechaOrden
    End Function
    ''' Revisado por: blp. fecha: 05/12/2012
    ''' <summary>
    ''' Indica si la orden tiene recepciones
    ''' </summary>
    ''' <returns>True-> Tiene recepciones. False-> No</returns>
    ''' <remarks>
    ''' Llamada desde: Seguimiento.aspx.vb
    ''' Tiempo m�ximo: 0,3 sec.</remarks>
    Public Function TieneRecepciones() As Boolean
        Dim bTieneRecepciones As Boolean = False
        bTieneRecepciones = DBServer.Orden_TieneRecepciones(mlId)
        Return bTieneRecepciones
    End Function

    Public Function TieneRecepcionesIntKO() As Boolean
        Dim bTieneRecepcionesIntKO As Boolean = False
        bTieneRecepcionesIntKO = DBServer.Orden_TieneRecepcionesIntKO(mlId)
        Return bTieneRecepcionesIntKO
    End Function
    Public Function IdOrdenDesdeInstancia(ByVal IdInstancia As Long) As Long
        Dim idOrden As Long
        idOrden = DBServer.Orden_getIdOrdenDesdeInstancia(IdInstancia)
        Return idOrden
    End Function
    ''' <summary>
    ''' Constructor de la clase
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si est� autenticado</param>
    ''' <remarks>
    ''' Llamada desde: Todos los sitios en los que se instancie un objeto de la clase COrden
    ''' Tiempo m�ximo: 0 sec
    ''' </remarks>
    Public Sub New(ByRef dbserver As Fullstep.FSNDataBaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
End Class