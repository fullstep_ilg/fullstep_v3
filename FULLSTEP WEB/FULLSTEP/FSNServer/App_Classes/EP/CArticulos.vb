Imports Fullstep.FSNLibrary
Imports System.Net
Imports System.Web
Imports System.Linq

<Serializable()> _
Public Class cArticulos
    Inherits Lista(Of CArticulo)

    Private mdImporteTotalCesta As Double
    Private mlNumArticulosCesta As Integer

    Private moAtributos As CAtributos

    Public Property Atributos() As CAtributos
        Get
            Atributos = moAtributos
        End Get
        Set(ByVal Value As CAtributos)
            moAtributos = Value
        End Set
    End Property

    Public Property ImporteTotalCesta() As Double
        Get
            ImporteTotalCesta = mdImporteTotalCesta
        End Get
        Set(ByVal Value As Double)
            mdImporteTotalCesta = Value
        End Set
    End Property

    Public Property NumArticulosCesta() As Integer
        Get
            NumArticulosCesta = mlNumArticulosCesta
        End Get
        Set(ByVal Value As Integer)
            mlNumArticulosCesta = Value
        End Set
    End Property

    ''' <summary>
    ''' Constructor de un objeto de la clase CArticulos
    ''' </summary>
    ''' <param name="dbserver">Servidor de la BBDD</param>
    ''' <param name="isAuthenticated">Si es una conexi�n remota</param>
    ''' <remarks>
    ''' Llamada desde: Todos los sitios donde se instancie esta clase
    ''' Tiempo m�ximo: 0 sec.</remarks>
    Public Sub New(ByRef dbserver As Fullstep.FSNDataBaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
    End Sub

    ''' <summary>
    ''' Agrega un elemento CArticulo a la lista
    ''' </summary>
    ''' <param name="lId">Id del art�culo</param>
    ''' <param name="sIdUnico">Id �nico</param>
    ''' <param name="sTooltip">Tooltip</param>
    ''' <param name="lAnyo">A�o</param>
    ''' <param name="sGMN1">C�digo GMN1</param>
    ''' <param name="lItem">Id de Item</param>
    ''' <param name="sCod">C�digo de art�culo</param>
    ''' <param name="sDen">Denominaci�n</param>
    ''' <param name="sProve">C�digo de proveedor</param>
    ''' <param name="sProveDen">Denominaci�n del proveedor</param>
    ''' <param name="sCodExt">C�digo externo</param>
    ''' <param name="dCantAdj">Cantidad adjudicada</param>
    ''' <param name="dPrecioUC">Precio por unidad de compra</param>
    ''' <param name="sUP">Unidad de pedido</param>
    ''' <param name="dFC">Factor de conversi�n de unidad de pedido</param>
    ''' <param name="dCantMinimaPedido">Cantidad m�nima de pedido</param>
    ''' <param name="dtFechaDespub">Fecha de despublicaci�n</param>
    ''' <param name="lCat1">C�digo de categor�a de nivel 1</param>
    ''' <param name="lCat2">C�digo de categor�a de nivel 2</param>
    ''' <param name="lCat3">C�digo de categor�a de nivel 3</param>
    ''' <param name="lCat4">C�digo de categor�a de nivel 4</param>
    ''' <param name="lCat5">C�digo de categor�a de nivel 5</param>
    ''' <param name="vSeguridad"></param>
    ''' <param name="bModDestino">Permiso de modificaci�n de destino</param>
    ''' <param name="ldatasizeThumbnail"></param>
    ''' <param name="sCategoria">Denominaci�n de la categor�a</param>
    ''' <param name="sRamaCategoria">Denominaci�n de la rama de categor�as</param>
    ''' <param name="sEsp">Especificaciones</param>
    ''' <param name="Cantidad">Cantidad</param>
    ''' <param name="Tipo"></param>
    ''' <param name="sDest">Destino</param>
    ''' <param name="oCampo1">Valor de campo1</param>
    ''' <param name="oCampo2">Valor de campo2</param>
    ''' <param name="bModifPrec">Permiso de modificaci�n de precio</param>
    ''' <param name="bActualizarCat"></param>
    ''' <param name="codMon">C�digo de moneda</param>
    ''' <param name="MonCambio">Cambio de moneda</param>
    ''' <returns>El elemento CArticulo agregado</returns>
    Public Overloads Function Add(ByVal lId As Object, ByVal sIdUnico As String, ByVal sTooltip As Object, ByVal lAnyo As Object, ByVal sGMN1 As Object, ByVal lItem As Object, ByVal sCod As Object, ByVal sDen As Object, ByVal sProve As Object, ByVal sProveDen As Object, ByVal sCodExt As Object, ByVal dCantAdj As Object, ByVal dPrecioUC As Object, ByVal sUP As Object, ByVal dFC As Object, ByVal dCantMinimaPedido As Object, ByVal dtFechaDespub As Object, ByVal lCat1 As Object, ByVal lCat2 As Object, ByVal lCat3 As Object, ByVal lCat4 As Object, ByVal lCat5 As Object, ByVal vSeguridad As Object, ByVal bModDestino As Object, ByVal ldatasizeThumbnail As Object, ByVal sCategoria As Object, ByRef sRamaCategoria As String, ByRef sEsp As String, ByVal Cantidad As Object, ByVal Tipo As Short, ByVal sDest As Object, ByRef oCampo1 As CCampo, ByRef oCampo2 As CCampo, ByVal bModifPrec As Object, ByVal bActualizarCat As Object, ByVal codMon As Object, ByRef MonCambio As Double, ByVal iTipoRecepcion As Integer) As CArticulo
        Dim objnewmember As CArticulo
        'Variables creadas para quitar los objetos por referencia y dejarlos como gen�ricos
        Dim bsRamaCategoria As Object
        Dim bsEsp As Object
        Dim bMonCambio As Object
        '****************************
        objnewmember = New CArticulo(mDBServer, mIsAuthenticated)

        With objnewmember
            'Antes IsMissing
            If IsNothing(lId) Then
                .ID = System.DBNull.Value
            Else
                .ID = lId
            End If
            .IdUnico = sIdUnico
            If IsDBNull(sTooltip) Then
                sTooltip = ""
            End If
            .Tooltip = sTooltip
            .Anyo = lAnyo
            .GMN1 = sGMN1
            .Item = lItem
            .Cod = sCod
            .Den = sDen
            .Prove = sProve
            .proveden = sProveDen
            .CodExt = sCodExt
            .CantAdj = dCantAdj
            .PrecioUC = dPrecioUC
            .UP = sUP
            .FC = dFC
            'A�adido el 1-3-2006
            'Se guarda el tipo de Moneda
            .Mon = codMon
            'A�adido el 14-3-2003
            'Se guarda la equivalencia respecto a MonCen
            'Antes IsMissing
            If Not IsNothing(MonCambio) Then
                bMonCambio = MonCambio
                .Cambio = bMonCambio
            End If
            .CantMinimaPedido = dCantMinimaPedido
            .FechaDespub = dtFechaDespub
            .Cat1 = lCat1
            .Cat2 = lCat2
            .Cat3 = lCat3
            .Cat4 = lCat4
            .Cat5 = lCat5
            .Seguridad = vSeguridad
            .ModDestino = bModDestino
            .DatasizeThumbnail = ldatasizeThumbnail
            .Categoria = sCategoria
            'A�adido para quitar los objetos por referencia(por un warning)
            bsRamaCategoria = sRamaCategoria
            '*******************************
            .RamaCategoria = bsRamaCategoria
            'A�adido para quitar los objetos por referencia(por un warning)
            bsEsp = sEsp
            '*******************************
            .Esp = bsEsp
            .Cantidad = Cantidad
            .Tipo = Tipo
            .Dest = sDest
            .Campo1 = oCampo1
            .Campo2 = oCampo2
            .iTipoRecepcion = iTipoRecepcion
            'Antes IsMissing
            If IsNothing(bModifPrec) Then bModifPrec = 0
            .ModificarPrec = bModifPrec
            'Antes IsMissing
            If IsNothing(bActualizarCat) Then bActualizarCat = 0
            .ActualizarCat = SQLBinaryToBoolean(bActualizarCat)
        End With

        Me.Add(objnewmember)
        Return objnewmember

    End Function

    ''' <summary>
    ''' Funci�n que devuelve un DataSet con todos los art�culos del catalogo del usuario pasado como par�metro, filtrado por las condiciones que se le pasan como par�metro, incluyendo los Ids de los art�culos por los que queremos filtrarnos.
    ''' </summary>
    ''' <param name="ID">Identificador del art�culo en caso de que se quiera filtrar el resultado por un s�lo art�culo.</param>
    ''' <param name="Cod">C�digo del art�culo en caso de que se quiera filtrar el resultado por el c�digo.</param>
    ''' <param name="Den">Denominacion del art�culo en caso de que se quieran filtrar los resultados por los que cumplan esta condici�n de denominaci�n.</param>
    ''' <param name="Prove">Par�metro que filtra los resultados que cumplan la condici�n de que pertenezcan al proveedor pasado por este par�metro.</param>
    ''' <param name="Catn1">Par�metro que filtra los resultados que cumplan la condici�n de que pertenezcan a las categorias de nivel 1 pasada por este par�metro.</param>
    ''' <param name="CatN2">Par�metro que filtra los resultados que cumplan la condici�n de que pertenezcan a las categorias de nivel 2 pasada por este par�metro.</param>
    ''' <param name="Catn3">Par�metro que filtra los resultados que cumplan la condici�n de que pertenezcan a las categorias de nivel 3 pasada por este par�metro.</param>
    ''' <param name="CatN4">Par�metro que filtra los resultados que cumplan la condici�n de que pertenezcan a las categorias de nivel 4 pasada por este par�metro.</param>
    ''' <param name="CatN5">Par�metro que filtra los resultados que cumplan la condici�n de que pertenezcan a las categorias de nivel 5 pasada por este par�metro.</param>
    ''' <param name="PrecioMax">Par�metro que filtra los resultados que cumplan la condici�n de que su precio m�ximo sea el valor del mismo.</param>
    ''' <param name="Per">Par�metro que filtra la busqueda de los art�culos del cat�logo asignados a la persona con este valor.</param>
    ''' <param name="CoincidenciaTotal">Par�metro que sirve para decidir si queremos que la coincidencia de la busqueda por la denominaci�n y c�digo de art�culo coincida totalmente con el valor que esta en la BD.</param>
    ''' <param name="sOrder">Par�metro que se utiliza para la ordenaci�n de los art�culos, siendo las posibilidades las siguientes: "cod":C�digo del art�culo,"Precio":Precio del art�culo,"Descripcion":Descripci�n del art�culo(alf�beticamente se entiende),"Proveedor":Proveedor del art�culo, tambi�n alfabeticamente,"Uso":N�mero de veces que se ha adquirido ese art�culo</param>
    ''' <param name="sCodExt">Par�metro que filtra los resultados que cumplan la condici�n de que contengan entre su C�digo externo el valor pasado por este par�metro.</param>
    ''' <param name="sDest">Par�metro que filtra los resultados que cumplan la condici�n de que el destino a enviar los art�culos sea el pasado por este par�metro.</param>
    ''' <param name="sDirec">Par�metro que sirve para determinar el sentido de ordenaci�n de los resultados, si debe ser ascendente o descendente</param>
    ''' <param name="ListaIds">Parametro con un array de strings con el valor de los Ids de los art�culos por los que queremos que nos filtre, normalmente viene de una busqueda de art�culos.</param>
    ''' <returns>Un dataset con los art�culos que cumplen los criterios de busqueda,ordenacion y sentido de la misma, pasados por los par�metros de la funci�n.</returns>
    ''' <remarks></remarks>
    Public Function FiltrarArticulosPorId(ByVal ID As Integer, ByVal Cod As Object, ByVal Den As Object, ByVal Prove As String, ByVal CatN1 As CCategoriasN1, ByVal CatN2 As CCategoriasN2, ByVal CatN3 As CCategoriasN3, ByVal CatN4 As CCategoriasN4, ByVal CatN5 As CCategoriasN5, ByVal PrecioMax As String, ByVal Per As Object, ByVal CoincidenciaTotal As Short, ByVal sDest As String, ByVal sOrder As String, ByVal sDirec As String, ByVal sCodExt As String, ByVal ListaIds As String()) As DataSet
        Dim ador As New DataSet
        If HttpContext.Current.Cache("DsetArticulos_" & Per) Is Nothing Then
            If Not ListaIds Is Nothing AndAlso ListaIds.Count > 0 Then
                For i As Integer = 0 To ListaIds.Count - 1
                    If Not ListaIds(i) Is Nothing AndAlso Not strToInt(ListaIds(i)) = 0 Then
                        Dim iIDArticulo As Integer = strToInt(ListaIds(i))
                        Dim dsArticulo As New DataSet
                        dsArticulo = DevolverArticulosCatalogo(iIDArticulo, Cod, Den, Prove, CatN1, CatN2, CatN3, CatN4, CatN5, PrecioMax, Per, CoincidenciaTotal, sDest, String.Empty, String.Empty, sCodExt)
                        If ador.Tables.Count = 0 Then
                            ador = dsArticulo.Copy
                        Else
                            ador.Tables(0).Merge(dsArticulo.Tables(0))
                        End If
                    End If
                Next
            Else
                Dim nuevatabla As New DataTable
                ador = New DataSet
                ador.Tables.Add(nuevatabla)
            End If
        Else
            ador = HttpContext.Current.Cache("DsetArticulos_" & Per)
            Dim consultaIds As String = ""
            Dim PrimeroA�adido As Boolean = False
            If Not ListaIds Is Nothing AndAlso ListaIds.Count > 0 Then
                For i As Integer = 0 To ListaIds.Count - 1
                    If Not ListaIds(i) Is Nothing AndAlso Not ListaIds(i) = "" Then
                        If Not PrimeroA�adido Then
                            consultaIds = "(" & "ID = " & ListaIds(i) & "" & ")"
                            PrimeroA�adido = True
                        Else
                            consultaIds = consultaIds & " or (" & "ID = " & ListaIds(i) & "" & ")"
                        End If
                    End If
                Next
                Dim rowsResul As DataRow()
                Dim nuevatabla As New DataTable
                Dim tablaResul As New DataTable
                tablaResul = ador.Tables(0)
                nuevatabla = tablaResul.Clone
                rowsResul = tablaResul.Select(consultaIds)
                tablaResul = New DataTable
                For Each fila As DataRow In rowsResul
                    nuevatabla.ImportRow(fila)
                Next
                ador = New DataSet
                ador.Tables.Add(nuevatabla)
            Else
                Dim nuevatabla As New DataTable
                ador = New DataSet
                ador.Tables.Add(nuevatabla)
            End If
        End If

        Return ador

    End Function

    ''' <summary>
    ''' Funci�n que devuelve un DataSet con todos los art�culos del catalogo del usuario pasado como par�metro en el idioma tambien pasado.
    ''' </summary>
    ''' <param name="ID">Identificador del art�culo en caso de que se quiera filtrar el resultado por un s�lo art�culo.</param>
    ''' <param name="Cod">C�digo del art�culo en caso de que se quiera filtrar el resultado por el c�digo.</param>
    ''' <param name="Den">Denominacion del art�culo en caso de que se quieran filtrar los resultados por los que cumplan esta condici�n de denominaci�n.</param>
    ''' <param name="Prove">Par�metro que filtra los resultados que cumplan la condici�n de que pertenezcan al proveedor pasado por este par�metro.</param>
    ''' <param name="CatN1">Par�metro que filtra los resultados que cumplan la condici�n de que pertenezcan a la categoria de nivel 1 pasada por este par�metro.</param>
    ''' <param name="CatN2">Par�metro que filtra los resultados que cumplan la condici�n de que pertenezcan a la categoria de nivel 2 pasada por este par�metro.</param>
    ''' <param name="CatN3">Par�metro que filtra los resultados que cumplan la condici�n de que pertenezcan a la categoria de nivel 3 pasada por este par�metro.</param>
    ''' <param name="CatN4">Par�metro que filtra los resultados que cumplan la condici�n de que pertenezcan a la categoria de nivel 4 pasada por este par�metro.</param>
    ''' <param name="CatN5">Par�metro que filtra los resultados que cumplan la condici�n de que pertenezcan a la categoria de nivel 5 pasada por este par�metro.</param>
    ''' <param name="PrecioMax">Par�metro que filtra los resultados que cumplan la condici�n de que su precio m�ximo sea el valor del mismo.</param>
    ''' <param name="Per">Par�metro que filtra la busqueda de los art�culos del cat�logo asignados a la persona con este valor.</param>
    ''' <param name="CoincidenciaTotal">Par�metro que sirve para decidir si queremos que la coincidencia de la busqueda por la denominaci�n y c�digo de art�culo coincida totalmente con el valor que esta en la BD.</param>
    ''' <param name="sOrder">Par�metro que se utiliza para la ordenaci�n de los art�culos, siendo las posibilidades las siguientes: "cod":C�digo del art�culo,"Precio":Precio del art�culo,"Descripcion":Descripci�n del art�culo(alf�beticamente se entiende),"Proveedor":Proveedor del art�culo, tambi�n alfabeticamente,"Uso":N�mero de veces que se ha adquirido ese art�culo</param>
    ''' <param name="sCodExt">Par�metro que filtra los resultados que cumplan la condici�n de que contengan entre su C�digo externo el valor pasado por este par�metro.</param>
    ''' <param name="sDest">Par�metro que filtra los resultados que cumplan la condici�n de que el destino a enviar los art�culos sea el pasado por este par�metro.</param>
    ''' <param name="sDirec">Par�metro que sirve para determinar el sentido de ordenaci�n de los resultados, si debe ser ascendente o descendente</param>
    ''' <returns>Un dataset con los pedidos que cumplen los criterios de busqueda,ordenacion y sentido de la misma, pasados por los par�metros de la funci�n.</returns>
    ''' <remarks></remarks>
    Public Function DevolverArticulosCatalogo(ByVal ID As Integer, ByVal Cod As Object, ByVal Den As Object, ByVal Prove As Object, ByVal CatN1 As CCategoriasN1, ByVal CatN2 As CCategoriasN2, ByVal CatN3 As CCategoriasN3, ByVal CatN4 As CCategoriasN4, ByVal CatN5 As CCategoriasN5, ByVal PrecioMax As Object, ByVal Per As Object, ByVal CoincidenciaTotal As Short, ByVal sDest As String, ByVal sOrder As String, ByVal sDirec As String, ByVal sCodExt As String) As DataSet
        Authenticate()
        Dim dsArticulos As New DataSet
        Dim sCat1 As String
        Dim sCat2 As String
        Dim sCat3 As String
        Dim sCat4 As String
        Dim sCat5 As String
        sCat1 = ""
        If CatN1 Is Nothing Then
            sCat1 = Nothing
        Else
            For Each cat1 As CCategoriaN1 In CatN1
                sCat1 = sCat1 & cat1.Id & ","
            Next
            If sCat1 <> "" Then
                sCat1 = Left(sCat1, Len(sCat1) - 1)
            Else
                sCat1 = Nothing
            End If
        End If
        sCat2 = ""
        If CatN2 Is Nothing Then
            sCat2 = Nothing
        Else
            For Each Cat2 As CCategoriaN2 In CatN2
                sCat2 = sCat2 & Cat2.Id & ","
            Next
            If sCat2 <> "" Then
                sCat2 = Left(sCat2, Len(sCat2) - 1)
            Else
                sCat2 = Nothing
            End If
        End If
        sCat3 = ""
        If CatN3 Is Nothing Then
            sCat3 = Nothing
        Else
            For Each Cat3 As CCategoriaN3 In CatN3
                sCat3 = sCat3 & Cat3.Id & ","
            Next
            If sCat3 <> "" Then
                sCat3 = Left(sCat3, Len(sCat3) - 1)
            Else
                sCat3 = Nothing
            End If
        End If
        sCat4 = ""
        If CatN4 Is Nothing Then
            sCat4 = Nothing
        Else
            For Each Cat4 As CCategoriaN4 In CatN4
                sCat4 = sCat4 & Cat4.Id & ","
            Next
            If sCat4 <> "" Then
                sCat4 = Left(sCat4, Len(sCat4) - 1)
            Else
                sCat4 = Nothing
            End If
        End If

        sCat5 = ""
        If CatN5 Is Nothing Then
            sCat5 = Nothing
        Else
            For Each Cat5 As CCategoriaN5 In CatN5
                sCat5 = sCat5 & Cat5.Id & ","
            Next
            If sCat5 <> "" Then
                sCat5 = Left(sCat5, Len(sCat5) - 1)
            Else
                sCat5 = Nothing
            End If
        End If
        Dim oServer As FSNServer.Root = CType(HttpContext.Current.Session("FSN_Server"), FSNServer.Root)
        Dim oUsuario As FSNServer.User = CType(HttpContext.Current.Session("FSN_USER"), FSNServer.User)
        'Para pasarle las empresas
        Dim Persona As FSNServer.CPersona
        Dim Empresas As FSNServer.CEmpresas
        Persona = oServer.Get_Object(GetType(FSNServer.CPersona))
        Persona.Cod = oUsuario.CodPersona
        Persona.CargarPersona()

        Dim oPargenSMS As FSNServer.PargensSMs
        oPargenSMS = oServer.Get_Object(GetType(FSNServer.PargensSMs))
        oPargenSMS.CargarConfiguracionSM(oUsuario.Cod, oUsuario.Idioma, True)
        Persona.CargarEmpresas(oPargenSMS, oUsuario.PedidosOtrasEmp)
        Empresas = Persona.Empresas
        Dim idEmpresas As String = ""
        If Empresas.Count > 0 Then
            idEmpresas = Empresas.Item(0).ID.ToString
            For i = 1 To Empresas.Count - 1
                idEmpresas += ","
                idEmpresas += Empresas.Item(i).ID.ToString
            Next
        End If
        
        dsArticulos = mDBServer.Articulos_DevolverArticulosCatalogo(ID, Cod, Den, Prove, sCat1, sCat2, sCat3, sCat4, sCat5, PrecioMax, Per, CoincidenciaTotal, sDest, sOrder, sDirec, sCodExt, idEmpresas)
        Dim key() As DataColumn = {dsArticulos.Tables(0).Columns("ID")}
        dsArticulos.Tables(0).PrimaryKey = key

        Return dsArticulos
        dsArticulos = Nothing
    End Function

    ''' <summary>
    ''' Funci�n que devuelve un dataset con los atributos para el Art�culo y proveedore seleccionados
    ''' </summary>
    ''' <param name="Art">c�digo del art�culo</param>
    ''' <param name="Prove">C�digo del proveedor</param>
    ''' <returns>un dataset con los atributos para el Art�culo y proveedore seleccionados</returns>
    ''' <remarks>
    ''' Llamada desde: Seguimiento.aspx
    ''' Tiempo m�ximo: 0 sec</remarks>
    Public Function DevolverAtributosArt(ByVal Art As String, ByVal Prove As String) As DataSet
        Authenticate()
        Return DBServer.Articulos_DevolverAtributosArt(Art, Prove)
    End Function

    ''' <summary>
    ''' Funci�n que devuelve un dataset con los atributos para el Art�culo y proveedore seleccionados
    ''' </summary>
    ''' <param name="Art">c�digo del art�culo</param>
    ''' <param name="Prove">C�digo del proveedor</param>
    ''' <returns>un dataset con los atributos para el Art�culo y proveedore seleccionados</returns>
    ''' <remarks>
    ''' Llamada desde: Seguimiento.aspx
    ''' Tiempo m�ximo: 0 sec</remarks>
    Public Function DevolverAtributosEspecificacionArt(ByVal Art As String, ByVal Prove As String) As DataSet
        Authenticate()
        Return DBServer.Articulos_DevolverAtributosEspecificacionArt(Art, Prove)
    End Function

    ''' <summary>
    ''' Funci�n que devuelve la imagen del art�culo requerido
    ''' </summary>
    ''' <param name="Art">C�digo del art�culo</param>
    ''' <param name="Prove">C�digo del proveedor del art�culo</param>
    ''' <returns>La imagen requerida</returns>
    ''' <remarks>Llamada desde CargarDetalleArticulo de Seguimiento.aspx
    ''' Tiempo m�ximo: 0 sec.</remarks>
    Public Function DevolverImagenArt(ByVal Art As String, ByVal Prove As String) As Byte()
        Authenticate()
        Return DBServer.Articulos_DevolverImagenArt(Art, Prove)
    End Function

    ''' <summary>
    ''' Funci�n que devuelve la Thumbnail del art�culo requerido
    ''' </summary>
    ''' <param name="Art">C�digo del art�culo</param>
    ''' <param name="Prove">C�digo del proveedor del art�culo</param>
    ''' <returns>La Thumbnail requerida</returns>
    ''' <remarks>Llamada desde CargarDetalleArticulo de Seguimiento.aspx
    ''' Tiempo m�ximo: 0 sec.</remarks>
    Public Function DevolverThumbnailArt(ByVal Art As String, ByVal Prove As String) As Object
        Authenticate()
        Return DBServer.Articulos_DevolverThumbnailArt(Art, Prove)
    End Function

    ''' <summary>
    ''' Funcion que carga los datos de un art�culo dado su Identificador
    ''' </summary>
    ''' <param name="lId">Identificador del ar�culo que se quiere cargar</param>
    ''' <returns>Un objeto de la clase art�culo con todos los datos necesarios para ser mostrados en la aplicaci�n.</returns>
    ''' <remarks></remarks>
    Public Function CargarArticulo(ByVal lId As Integer) As CArticulo
        Authenticate()
        Dim oArt As CArticulo
        Dim adores As New DataSet
        adores = mDBServer.Articulos_CargarArticulo(lId)
        oArt = New CArticulo(mDBServer, mIsAuthenticated)
        With oArt
            .ID = adores.Tables(0).Rows(0).Item("ID")
            .Anyo = adores.Tables(0).Rows(0).Item("ANYO")
            .GMN1 = adores.Tables(0).Rows(0).Item("GMN1")
            .Item = adores.Tables(0).Rows(0).Item("ITEM")
            .Cod = adores.Tables(0).Rows(0).Item("COD_ITEM")
            .Den = adores.Tables(0).Rows(0).Item("ART_DEN")
            .Prove = adores.Tables(0).Rows(0).Item("PROVE")
            .proveden = adores.Tables(0).Rows(0).Item("PROVEDEN")
            .CodExt = adores.Tables(0).Rows(0).Item("COD_EXT")
            .CantAdj = adores.Tables(0).Rows(0).Item("CANT_ADJ")
            .PrecioUC = adores.Tables(0).Rows(0).Item("PRECIO_UC")
            .UP = adores.Tables(0).Rows(0).Item("UP_DEF")
            .FC = adores.Tables(0).Rows(0).Item("FC_DEF")
            .Mon = adores.Tables(0).Rows(0).Item("CODMON")
            .Cambio = adores.Tables(0).Rows(0).Item("MONCAMBIO")
            .CantMinimaPedido = adores.Tables(0).Rows(0).Item("CANT_MIN_DEF")
            .FechaDespub = adores.Tables(0).Rows(0).Item("FECHA_DESPUB")
            .Cat1 = adores.Tables(0).Rows(0).Item("CAT1")
            .Cat2 = adores.Tables(0).Rows(0).Item("CAT2")
            .Cat3 = adores.Tables(0).Rows(0).Item("CAT3")
            .Cat4 = adores.Tables(0).Rows(0).Item("CAT4")
            .Cat5 = adores.Tables(0).Rows(0).Item("CAT5")
            .Seguridad = adores.Tables(0).Rows(0).Item("SEGURIDAD")
            .DatasizeThumbnail = adores.Tables(0).Rows(0).Item("THUMBNAIL")
            .Categoria = adores.Tables(0).Rows(0).Item("CATEGORIA")
            .RamaCategoria = adores.Tables(0).Rows(0).Item("CATEGORIARAMA")
            .ModificarPrec = SQLBinaryToBoolean(adores.Tables(0).Rows(0).Item("MODIF_PREC"))
            .Esp = adores.Tables(0).Rows(0).Item("ESP")
            .iTipoRecepcion = adores.Tables(0).Rows(0).Item("TIPORECEPCION")
        End With
        adores.Clear()
        adores = Nothing
        CargarArticulo = oArt
    End Function

    ''' <summary>
    ''' Funci�n que devuelve todas las categorias utilizadas en los pedidos
    ''' </summary>
    ''' <returns>Un DataSet con la informaci�n de las categorias de los pedidos</returns>
    ''' <remarks>
    ''' Llamada desde:EPWEB\Report_XML\CargarArbolCategorias</remarks>
    Public Function DevolverCategoriasPedidos() As DataSet
        Authenticate()
        Dim DSResul As New DataSet
        DSResul = DBServer.Articulos_DevolverCategoriasPedidos
        Return DSResul
    End Function

    ''' <summary>
    ''' Funci�n que devuelve Las especificaciones del articulo
    ''' </summary>
    ''' <param name="art">Codigo articulo</param>
    ''' <param name="Prove">Codigo proveedor</param>
    ''' <returns>string con Las especificaciones del articulo</returns>
    ''' <remarks>
    ''' Llamada desde:EPWEB\common\DetArticuloPedido.ascx.vb</remarks>
    Public Function DevolverEspecificacionesArt(ByVal Art As String, ByVal Prove As String) As String
        Authenticate()
        Dim resultado As String
        resultado = DBServer.Articulos_DevolverEspecificacionesArt(Art, Prove)
        Return resultado
    End Function

    ''' Revisado por: blp. Fecha: 23/01/2013
    ''' <summary>
    ''' Funci�n que devuelve un array de strings con las descripciones de los art�culos del catalogo del usuario pasado como par�metro, filtrados por el texto indicado como par�metro
    ''' </summary>
    ''' <param name="textoFiltroArticulo">C�digo del art�culo en caso de que se quiera filtrar el resultado por el c�digo.</param>
    ''' <param name="CodPer">Par�metro que filtra la busqueda de los art�culos del cat�logo asignados a la persona con este valor.</param>
    ''' <param name="VerPedidosOtrosUsuarios">Permiso para ver pedidos de otros usuarios (de los centros de coste en lo que puede imputar o de los que es gestor)</param>
    ''' <returns>array de descripciones de art�culos</returns>
    ''' <remarks>Llamada desde Seguimiento.aspx.vb. Max. 2 seg.</remarks>
    Public Function DevolverDescripcionesArticulos(ByVal textoFiltroArticulo As String, ByVal CodPer As String, ByVal VerPedidosOtrosUsuarios As Boolean) As DataSet
        Dim dsDenArticulos As New DataSet
        dsDenArticulos = mDBServer.Articulos_DevolverDescripcionesArticulos(textoFiltroArticulo, CodPer, VerPedidosOtrosUsuarios)
        Return dsDenArticulos
    End Function

End Class