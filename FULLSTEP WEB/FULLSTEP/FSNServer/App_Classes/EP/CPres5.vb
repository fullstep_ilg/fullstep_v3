﻿Public Class CPres5
    Inherits Security

    Private _Codigo As String
    Private _Denominacion As String
    Private _ParentPres5 As CPres5
    Private _ChildPres5s As CPres5s
    Private _BajaLogica As Boolean

    Public Property Codigo() As String
        Get
            Return _Codigo
        End Get
        Set(ByVal value As String)
            _Codigo = value
        End Set
    End Property

    Public Property Denominacion() As String
        Get
            Return _Denominacion
        End Get
        Set(ByVal value As String)
            _Denominacion = value
        End Set
    End Property

    Public Property ParentPres5() As CPres5
        Get
            Return _ParentPres5
        End Get
        Set(ByVal value As CPres5)
            _ParentPres5 = value
        End Set
    End Property

    Public Property ChildUONs() As CPres5s
        Get
            Return _ChildPres5s
        End Get
        Set(ByVal value As CPres5s)
            For Each el As CPres5 In value
                el.ParentPres5 = Me
            Next
            _ChildPres5s = value
        End Set
    End Property

    Public Property BajaLogica() As Boolean
        Get
            Return _BajaLogica
        End Get
        Set(ByVal value As Boolean)
            _BajaLogica = value
        End Set
    End Property

    Public ReadOnly Property Nivel() As Integer
        Get
            If _ParentPres5 Is Nothing Then
                Return 0
            Else
                Return _ParentPres5.Nivel + 1
            End If
        End Get
    End Property

    Public ReadOnly Property Pres0() As String
        Get
            Dim el As CPres5 = Me
            While el.Nivel > 0
                el = el.ParentPres5
            End While
            Return el.Codigo
        End Get
    End Property

    Public ReadOnly Property Pres1() As String
        Get
            If Me.Nivel < 1 Then
                Return String.Empty
            Else
                Dim el As CPres5 = Me
                While el.Nivel > 1
                    el = el.ParentPres5
                End While
                Return el.Codigo
            End If
        End Get
    End Property

    Public ReadOnly Property Pres2() As String
        Get
            If Me.Nivel < 2 Then
                Return String.Empty
            Else
                Dim el As CPres5 = Me
                While el.Nivel > 2
                    el = el.ParentPres5
                End While
                Return el.Codigo
            End If
        End Get
    End Property

    Public ReadOnly Property Pres3() As String
        Get
            If Me.Nivel < 3 Then
                Return String.Empty
            Else
                Dim el As CPres5 = Me
                While el.Nivel > 3
                    el = el.ParentPres5
                End While
                Return el.Codigo
            End If
        End Get
    End Property

    Public ReadOnly Property Pres4() As String
        Get
            If Me.Nivel < 4 Then
                Return String.Empty
            Else
                Return _Codigo
            End If
        End Get
    End Property

    ''' <summary>
    ''' Crea el objeto CPres5 y los objetos de nivel superior
    ''' </summary>
    ''' <param name="Pres0">Código del nodo presupuestario de nivel 0</param>
    Public Sub SetPres5(ByVal Pres0 As String)
        SetPres5(Pres0, String.Empty, String.Empty, String.Empty, String.Empty)
    End Sub

    ''' <summary>
    ''' Crea el objeto CPres5 y los objetos de nivel superior
    ''' </summary>
    ''' <param name="Pres0">Código del nodo presupuestario de nivel 0</param>
    ''' <param name="Pres1">Código del nodo presupuestario de nivel 1</param>
    Public Sub SetPres5(ByVal Pres0 As String, ByVal Pres1 As String)
        SetPres5(Pres0, Pres1, String.Empty, String.Empty, String.Empty)
    End Sub

    ''' <summary>
    ''' Crea el objeto CPres5 y los objetos de nivel superior
    ''' </summary>
    ''' <param name="Pres0">Código del nodo presupuestario de nivel 0</param>
    ''' <param name="Pres1">Código del nodo presupuestario de nivel 1</param>
    ''' <param name="Pres2">Código del nodo presupuestario de nivel 2</param>
    Public Sub SetPres5(ByVal Pres0 As String, ByVal Pres1 As String, ByVal Pres2 As String)
        SetPres5(Pres0, Pres1, Pres2, String.Empty, String.Empty)
    End Sub

    ''' <summary>
    ''' Crea el objeto CPres5 y los objetos de nivel superior
    ''' </summary>
    ''' <param name="Pres0">Código del nodo presupuestario de nivel 0</param>
    ''' <param name="Pres1">Código del nodo presupuestario de nivel 1</param>
    ''' <param name="Pres2">Código del nodo presupuestario de nivel 2</param>
    ''' <param name="Pres3">Código del nodo presupuestario de nivel 3</param>
    Public Sub SetPres5(ByVal Pres0 As String, ByVal Pres1 As String, ByVal Pres2 As String, ByVal Pres3 As String)
        SetPres5(Pres0, Pres1, Pres2, Pres3, String.Empty)
    End Sub

    ''' <summary>
    ''' Crea el objeto CPres5 y los objetos de nivel superior
    ''' </summary>
    ''' <param name="Pres0">Código del nodo presupuestario de nivel 0</param>
    ''' <param name="Pres1">Código del nodo presupuestario de nivel 1</param>
    ''' <param name="Pres2">Código del nodo presupuestario de nivel 2</param>
    ''' <param name="Pres3">Código del nodo presupuestario de nivel 3</param>
    ''' <param name="Pres4">Código del nodo presupuestario de nivel 4</param>
    Public Sub SetPres5(ByVal Pres0 As String, ByVal Pres1 As String, ByVal Pres2 As String, ByVal Pres3 As String, ByVal Pres4 As String)
        If String.IsNullOrEmpty(Pres4) Then
            If String.IsNullOrEmpty(Pres3) Then
                If String.IsNullOrEmpty(Pres2) Then
                    If String.IsNullOrEmpty(Pres1) Then
                        _Codigo = Pres0
                    Else
                        _Codigo = Pres1
                        Dim oPres5 As New CPres5(mDBServer, mIsAuthenticated)
                        oPres5.SetPres5(Pres0)
                        Me.ParentPres5 = oPres5
                    End If
                Else
                    _Codigo = Pres2
                    Dim oPres5 As New CPres5(mDBServer, mIsAuthenticated)
                    oPres5.SetPres5(Pres0, Pres1)
                    Me.ParentPres5 = oPres5
                End If
            Else
                _Codigo = Pres3
                Dim oPres5 As New CPres5(mDBServer, mIsAuthenticated)
                oPres5.SetPres5(Pres0, Pres1, Pres2)
                Me.ParentPres5 = oPres5
            End If
        Else
            _Codigo = Pres4
            Dim oPres5 As New CPres5(mDBServer, mIsAuthenticated)
            oPres5.SetPres5(Pres0, Pres1, Pres2, Pres3, Pres4)
            Me.ParentPres5 = oPres5
        End If
    End Sub

    ''' <summary>
    ''' Constructor de la clase CPres5
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si está autenticado</param>
    Public Sub New(ByRef dbserver As Fullstep.FSNDataBaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
    End Sub

End Class
