﻿Public Class CContacto
    Inherits Security

    Private _CodProveedor As String
    Private _DenProveedor As String
    Private _Id As Integer
    Private _Nombre As String
    Private _Apellidos As String
    Private _Departamento As String
    Private _Cargo As String
    Private _Telefono As String
    Private _Fax As String
    Private _Email As String
    Private _Telefono2 As String
    Private _TelefonoMovil As String
    Private _Idioma As FSNLibrary.Idioma
    Private _TipoEmail As FSNLibrary.TipoEmail
    Private _RecibePeticiones As Boolean
    Private _Aprovisionamiento As Boolean
    Private _Subasta As Boolean
    Private _Calidad As Boolean
    Private _DateFormat As System.Globalization.DateTimeFormatInfo
    Private _ThousanFmt As String
    Private _DecimalFmt As String
    Private _PrecisionFmt As Integer
    Private _NumberFormat As System.Globalization.NumberFormatInfo
    Private _DateFmt As String
    Private _ProveedorDePortal As Boolean
    Private _IdPortal As Integer

    Public Property CodProveedor() As String
        Get
            Return _CodProveedor
        End Get
        Set(ByVal value As String)
            _CodProveedor = value
        End Set
    End Property

    Public Property DenProveedor() As String
        Get
            Return _DenProveedor
        End Get
        Set(ByVal value As String)
            _DenProveedor = value
        End Set
    End Property

    Public Property Id() As Integer
        Get
            Return _Id
        End Get
        Set(ByVal value As Integer)
            _Id = value
        End Set
    End Property

    Public Property Nombre() As String
        Get
            Return _Nombre
        End Get
        Set(ByVal value As String)
            _Nombre = value
        End Set
    End Property

    Public Property Apellidos() As String
        Get
            Return _Apellidos
        End Get
        Set(ByVal value As String)
            _Apellidos = value
        End Set
    End Property

    Public ReadOnly Property NombreApellidos() As String
        Get
            Return _Nombre & " " & _Apellidos
        End Get
    End Property

    Public Property Departamento() As String
        Get
            Return _Departamento
        End Get
        Set(ByVal value As String)
            _Departamento = value
        End Set
    End Property

    Public Property Cargo() As String
        Get
            Return _Cargo
        End Get
        Set(ByVal value As String)
            _Cargo = value
        End Set
    End Property

    Public Property Telefono() As String
        Get
            Return _Telefono
        End Get
        Set(ByVal value As String)
            _Telefono = value
        End Set
    End Property

    Public Property Fax() As String
        Get
            Return _Fax
        End Get
        Set(ByVal value As String)
            _Fax = value
        End Set
    End Property

    Public Property Email() As String
        Get
            Return _Email
        End Get
        Set(ByVal value As String)
            _Email = value
        End Set
    End Property

    Public Property Telefono2() As String
        Get
            Return _Telefono2
        End Get
        Set(ByVal value As String)
            _Telefono2 = value
        End Set
    End Property

    Public Property TelefonoMovil() As String
        Get
            Return _TelefonoMovil
        End Get
        Set(ByVal value As String)
            _TelefonoMovil = value
        End Set
    End Property

    Public Property Idioma() As FSNLibrary.Idioma
        Get
            Return _Idioma
        End Get
        Set(ByVal value As FSNLibrary.Idioma)
            _Idioma = value
        End Set
    End Property

    Public Property TipoEmail() As FSNLibrary.TipoEmail
        Get
            Return _TipoEmail
        End Get
        Set(ByVal value As FSNLibrary.TipoEmail)
            _TipoEmail = value
        End Set
    End Property

    Public Property RecibePeticiones() As Boolean
        Get
            Return _RecibePeticiones
        End Get
        Set(ByVal value As Boolean)
            _RecibePeticiones = value
        End Set
    End Property

    Public Property Aprovisionamiento() As Boolean
        Get
            Return _Aprovisionamiento
        End Get
        Set(ByVal value As Boolean)
            _Aprovisionamiento = value
        End Set
    End Property

    Public Property Subasta() As Boolean
        Get
            Return _Subasta
        End Get
        Set(ByVal value As Boolean)
            _Subasta = value
        End Set
    End Property

    Public Property Calidad() As Boolean
        Get
            Return _Calidad
        End Get
        Set(ByVal value As Boolean)
            _Calidad = value
        End Set
    End Property

    Public Property ProveedorDePortal() As Boolean
        Get
            Return _ProveedorDePortal
        End Get
        Set(ByVal value As Boolean)
            _ProveedorDePortal = value
        End Set
    End Property

    Public Property IdPortal() As Integer
        Get
            Return _IdPortal
        End Get
        Set(ByVal value As Integer)
            _IdPortal = value
        End Set
    End Property

    Public Property ThousanFmt() As String
        Get
            If _ThousanFmt = Nothing Then
                _ThousanFmt = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberGroupSeparator
                _DecimalFmt = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator
            End If
            Return _ThousanFmt
        End Get
        Set(ByVal Value As String)
            _ThousanFmt = Value
        End Set
    End Property

    Public Property DecimalFmt() As String
        Get
            If _DecimalFmt = Nothing Then
                _ThousanFmt = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberGroupSeparator
                _DecimalFmt = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator
            End If
            Return _DecimalFmt
        End Get
        Set(ByVal Value As String)
            _DecimalFmt = Value
        End Set
    End Property

    Public Property PrecisionFmt() As String
        Get
            PrecisionFmt = _PrecisionFmt
        End Get
        Set(ByVal Value As String)
            _PrecisionFmt = Value
        End Set
    End Property

    Public Property NumberFormat() As System.Globalization.NumberFormatInfo
        Get
            If _NumberFormat Is Nothing Then
                _NumberFormat = New System.Globalization.CultureInfo(System.Globalization.CultureInfo.CurrentCulture.Name, False).NumberFormat
                _NumberFormat.NumberDecimalDigits = Me.PrecisionFmt
                _NumberFormat.NumberDecimalSeparator = Me.DecimalFmt
                _NumberFormat.NumberGroupSeparator = Me.ThousanFmt
            End If
            NumberFormat = _NumberFormat
        End Get
        Set(ByVal Value As System.Globalization.NumberFormatInfo)
            _NumberFormat = Value
            Me.PrecisionFmt = _NumberFormat.NumberDecimalDigits
            Me.DecimalFmt = _NumberFormat.NumberDecimalSeparator
            Me.ThousanFmt = _NumberFormat.NumberGroupSeparator
        End Set
    End Property

    Public Property DateFmt() As String
        Get
            DateFmt = _DateFmt
        End Get
        Set(ByVal Value As String)
            _DateFmt = Value
            _DateFormat = New System.Globalization.CultureInfo(System.Globalization.CultureInfo.CurrentCulture.Name, False).DateTimeFormat
            _DateFormat.ShortDatePattern = Replace(_DateFmt, "mm", "MM")
        End Set
    End Property

    Public Property DateFormat() As System.Globalization.DateTimeFormatInfo
        Get
            Return _DateFormat
        End Get
        Set(ByVal Value As System.Globalization.DateTimeFormatInfo)
            _DateFormat = Value
            _DateFmt = Replace(_DateFormat.ShortDatePattern, "MM", "mm")
        End Set
    End Property

    ''' <summary>
    ''' Constructor de la clase CContacto
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si está autenticado</param>
    Public Sub New(ByRef dbserver As Fullstep.FSNDataBaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
    End Sub

End Class
