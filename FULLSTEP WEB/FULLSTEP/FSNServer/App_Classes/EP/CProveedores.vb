Public Class CProveedores
    Inherits Security

    Public Sub New(ByRef dbserver As Fullstep.FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
    ''' <summary>
    ''' Funci�n que devuelve un dataset con los proveedores para hacer pedidos libres
    ''' </summary>
    ''' <param name="Per">Codigo de la persona</param>
    ''' <param name="iNivel">Nivel de categor�a</param>
    ''' <param name="Cat1">Cod de la categor�a de nivel 1</param>
    ''' <param name="Cat2">Cod de la categor�a de nivel 2</param>
    ''' <param name="Cat3">Cod de la categor�a de nivel 3</param>
    ''' <param name="Cat4">Cod de la categor�a de nivel 4</param>
    ''' <param name="Cat5">Cod de la categor�a de nivel 5</param>
    ''' <returns>Un dataset con los proveedores</returns>
    ''' <remarks>
    ''' Llamada desde PedidoLibre.aspx.vb
    ''' Tiempo: 1 seg.</remarks>
    Public Function DevolverProveedoresPedLibre(ByVal Per As String, ByVal iNivel As Short, ByVal Cat1 As Short, ByVal Cat2 As Short, ByVal Cat3 As Short, ByVal Cat4 As Short, ByVal Cat5 As Short) As DataSet
        Dim ador As DataSet
        Dim ParentColumns(4) As DataColumn
        Dim ChildColumns(4) As DataColumn

        ador = DBServer.Proveedores_DevolverProveedoresPedLibre(Per, iNivel, Cat1, Cat2, Cat3, Cat4, Cat5)

        ParentColumns(0) = ador.Tables(0).Columns("id_cat1")
        ChildColumns(0) = ador.Tables(1).Columns("id_cat1")
        ParentColumns(1) = ador.Tables(0).Columns("id_cat2")
        ChildColumns(1) = ador.Tables(1).Columns("id_cat2")
        ParentColumns(2) = ador.Tables(0).Columns("id_cat3")
        ChildColumns(2) = ador.Tables(1).Columns("id_cat3")
        ParentColumns(3) = ador.Tables(0).Columns("id_cat4")
        ChildColumns(3) = ador.Tables(1).Columns("id_cat4")
        ParentColumns(4) = ador.Tables(0).Columns("id_cat5")
        ChildColumns(4) = ador.Tables(1).Columns("id_cat5")

        ador.Relations.Add("myRelation", ParentColumns, ChildColumns, False)

        Return ador
    End Function
    ''' <summary>
    ''' Funci�n que Devuelve un DataSet con los proveedores que cumplan las opciones de filtro
    ''' </summary>
    ''' <param name="CodProve">C�digo del proveedor</param>
    ''' <param name="DenProve">Denominaci�n del proveedor</param>
    ''' <param name="PaisProve">Pa�s del proveedor</param>
    ''' <param name="ProvinciaProve">Provincia del proveedor</param>
    ''' <param name="PobProve">Poblaci�n del proveedor</param>
    ''' <param name="CPProve">Codigo postal del proveedor</param>
    ''' <param name="CIF">CIF</param>
    ''' <param name="CodPer">CodPer</param>
    ''' <param name="idioma">Idioma en el que se desea recuperar los datos multiidioma</param>
    ''' <returns> Un DataSet con los datos de todos los proveedores que cumplan con las opciones del filtro</returns>
    ''' <remarks>
    ''' Llamada desde:EPServer\Proveedores\DevolverProveedoresFiltrados
    ''' Tiempo M�ximo: 0.5 seg</remarks>
    Public Function DevolverProveedoresFiltrados(ByVal CodProve As String, ByVal CIF As String, ByVal DenProve As String, ByVal PaisProve As String, ByVal ProvinciaProve As String, ByVal PobProve As String, ByVal CPProve As String, ByVal CodPer As String, ByVal idioma As FSNLibrary.Idioma) As DataSet
        Dim DsResul As New DataSet
        DsResul = DBServer.Proveedores_DevolverProveedoresFiltrados(CodProve, CIF, DenProve, PaisProve, ProvinciaProve, PobProve, CPProve, CodPer, idioma)
        If DsResul.Tables(0).Rows.Count > 0 Then
            Return DsResul
        Else
            Return Nothing
        End If
    End Function
    ''' Revisado por: blp. Fecha:28/08/2012
    ''' <summary>
    ''' Devolver� una tabla con los datos de los proveedores cuyos c�digos se han recibido como par�metro
    ''' </summary>
    ''' <param name="idioma">Idioma en el que se desea recuperar los datos multiidioma</param>
    ''' <param name="ProveCods">Tabla con los c�digos de los proveedores de los que se quiere recuperar la informaci�n</param>
    ''' <returns>Tabla con los datos de los proveedores cuyos c�digos se han recibido como par�metro</returns>
    ''' <remarks>
    ''' Llamada desde: Recepcion.aspx.vb
    ''' Tiempo M�ximo: 0.5 seg</remarks>
    Public Function DevolverDatosProveedores(ByVal idioma As FSNLibrary.Idioma, ByVal ProveCods As DataTable) As DataTable
        Dim DtResul As New DataTable
        DtResul = DBServer.Ordenes_Recepciones_Obt_DatosProveedores(idioma, ProveCods)
        Return DtResul
    End Function
    '''Revisado por: blp. Fecha: 28/08/2012
    ''' <summary>
    ''' Funci�n que crea la estructura de una tabla que podemos usar para contener c�digos de proveedores 
    ''' </summary>
    ''' <returns>DataTable vac�o</returns>
    ''' <remarks>Llamada desde Recepciones.aspx.vb. M�x. 0,1 seg.</remarks>
    Public Function CrearTablaProveCods() As DataTable
        Dim dtTipoOrigen As DataTable = New DataTable("TBPROVECOD")
        dtTipoOrigen.Columns.Add("COD", GetType(System.String))
        Return dtTipoOrigen
    End Function
    ''' Revisado por: blp. Fecha: 23/01/2013
    ''' <summary>
    ''' Funci�n que devuelve los proveedores (c�digo/descripci�n) del usuario pasado como par�metro, filtrados por el texto indicado como par�metro
    ''' </summary>
    ''' <param name="textoFiltroProveedor">cadena por la que se quiere filtrar.</param>
    ''' <param name="CodPer">Par�metro que filtra la busqueda de los proveedores asignados a la persona con este valor.</param>
    ''' <param name="VerPedidosOtrosUsuarios">Permiso para ver pedidos de otros usuarios (de los centros de coste en lo que puede imputar o de los que es gestor)</param>
    ''' <returns>array de descripciones de art�culos</returns>
    ''' <remarks>Llamada desde Recepcion.aspx.vb. Max. 2 seg.</remarks>
    Public Function DevolverProveedoresCoincidentes(ByVal textoFiltroProveedor As String, ByVal CodPer As String, ByVal VerPedidosOtrosUsuarios As Boolean) As DataSet
        Dim dsProveedores As New DataSet
        dsProveedores = mDBServer.Proveedores_DevolverProveedoresCoincidentes(textoFiltroProveedor, CodPer, VerPedidosOtrosUsuarios)
        Return dsProveedores
    End Function
End Class