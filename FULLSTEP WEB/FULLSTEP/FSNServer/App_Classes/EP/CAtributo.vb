<Serializable()> _
Public Class CAtributo
    Inherits Security

    Private mvId As Object
    Private msCod As String
    Private msDen As String
    Private miIntro As Short
    Private mudtTipo As TiposDeDatos.TipoAtributo
    Private mvValor As Object
    Private mvValorMin As Object
    Private mvValorMax As Object
    Private mbObligatorio As Boolean
    Private msOperacion As Object
    Private miAplicar As Object
    Private moValores As CValoresAtrib
    Public Sub New()

    End Sub

    Public Property Id() As Object
        Get
            Id = mvId
        End Get
        Set(ByVal Value As Object)
            mvId = Value
        End Set
    End Property

    Public Property Cod() As String
        Get
            Cod = msCod
        End Get
        Set(ByVal Value As String)
            msCod = Value
        End Set
    End Property

    Public Property Den() As String
        Get
            Den = msDen
        End Get
        Set(ByVal Value As String)
            msDen = Value
        End Set
    End Property

    Public Property Intro() As Short
        Get
            Intro = miIntro
        End Get
        Set(ByVal Value As Short)
            miIntro = Value
        End Set
    End Property

    Public Property Tipo() As TiposDeDatos.TipoAtributo
        Get
            Tipo = mudtTipo
        End Get
        Set(ByVal Value As TiposDeDatos.TipoAtributo)
            mudtTipo = Value
        End Set
    End Property

    Public Property Obligatorio() As Boolean
        Get
            Obligatorio = mbObligatorio
        End Get
        Set(ByVal Value As Boolean)
            mbObligatorio = Value
        End Set
    End Property

    Public Property Operacion() As Object
        Get
            Operacion = msOperacion
        End Get
        Set(ByVal Value As Object)
            msOperacion = Value
        End Set
    End Property

    Public Property Aplicar() As Object
        Get
            Aplicar = miAplicar
        End Get
        Set(ByVal Value As Object)
            miAplicar = Value
        End Set
    End Property

    Public Property Valor() As Object
        Get
            Valor = mvValor
        End Get
        Set(ByVal Value As Object)
            On Error Resume Next
            Select Case mudtTipo
                Case 1
                    mvValor = CStr(Value)
                Case 2
                    mvValor = CDbl(Value)
                Case 3
                    mvValor = CDate(Value)
                Case 4
                    mvValor = CBool(Value)
            End Select

            'Antes IsEmpty
            If IsNothing(mvValor) Then
                mvValor = System.DBNull.Value
            End If
        End Set
    End Property

    Public Property ValorMin() As Object
        Get
            ValorMin = mvValorMin
        End Get
        Set(ByVal Value As Object)
            mvValorMin = Value
        End Set
    End Property

    Public Property ValorMax() As Object
        Get
            ValorMax = mvValorMax
        End Get
        Set(ByVal Value As Object)
            mvValorMax = Value
        End Set
    End Property

    Public Property Valores() As CValoresAtrib
        Get
            Valores = moValores
        End Get
        Set(ByVal Value As CValoresAtrib)
            moValores = Value
        End Set
    End Property

    ''' <summary>
    ''' Constructor de la clase CAtributo
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si est� autenticado</param>
    Public Sub New(ByRef dbserver As Fullstep.FSNDataBaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub

End Class