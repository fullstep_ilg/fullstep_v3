﻿Imports System.IO
Imports System.Xml
<Serializable()> _
Public Class cXML
    Inherits Security

    ''' <summary>
    ''' Constructor de la clase
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si está autenticado</param>
    ''' <remarks>
    ''' Llamada desde: Todos los sitios en los que se instancie un objeto de la clase CXML
    ''' Tiempo máximo: 0 sec
    ''' </remarks>
    Public Sub New(ByRef dbserver As Fullstep.FSNDataBaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
    End Sub
    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub
    Private strNombreParametro As String
    Private strTextoAMostrar As String
    Private enTipoDato As TipoDeDato
    Private boolRequerido As Boolean
    Private boolPermitirMultiple As Boolean
    Private arrayValoresPorDefecto As ValoresDefecto()
    Private strCabeceraInforme As String
    Private intIndice As Integer
    Private strValorDefecto As String
    Private strValorMinimo As String
    Private strValorMaximo As String
    Private strucOrigenDeDatos As OrigenDeDatos
    Private boolTieneRangoValores As Boolean
    'Valores de los parámetros internos de configuración
    Private enTipoParametro As TipoDeParametro
    Private strNombreParametroConfig As String
    Private strValorParametroConfig As String
    Private intIndiceParametroConfig As Integer
    <Serializable()> _
        Public Enum TipoDeDato
        Entero = 1
        Texto = 2
        Booleano = 3
        Real = 4
        Fecha = 5
        ArbolCategorias = 6
        EstructuraMateriales = 7
        ArticulosCatalogo = 8
        PathWinzip = 9
    End Enum
    <Serializable()> _
        Public Enum TipoOrigenDatos
        Stored = 1
        Tabla = 2
    End Enum
    <Serializable()> _
        Public Structure OrigenDeDatos
        Dim Tipo As TipoOrigenDatos
        Dim Nombre As String
        Dim CampoAMostrar As String
        Dim CampoValor As String
        Dim Parametros As FSNLibrary.TiposDeDatos.ParametrosOrigenDeDatos()
    End Structure
    <Serializable()> _
        Public Structure ValoresDefecto
        Dim Valor As String
        Dim Texto As String
    End Structure
    Public Property NombreParametro() As String
        Get
            Return strNombreParametro
        End Get
        Set(ByVal value As String)
            strNombreParametro = value
        End Set
    End Property
    Public Property TextoAMostrar() As String
        Get
            Return strTextoAMostrar
        End Get
        Set(ByVal value As String)
            strTextoAMostrar = value
        End Set
    End Property
    Public Property TipoDato() As TipoDeDato
        Get
            Return enTipoDato
        End Get
        Set(ByVal value As TipoDeDato)
            enTipoDato = value
        End Set
    End Property
    Public Property Requerido() As Boolean
        Get
            Return boolRequerido
        End Get
        Set(ByVal value As Boolean)
            boolRequerido = value
        End Set
    End Property
    Public Property MultiplesValores() As Boolean
        Get
            Return boolPermitirMultiple
        End Get
        Set(ByVal value As Boolean)
            boolPermitirMultiple = value
        End Set
    End Property
    Public Property ValoresPorDefecto() As ValoresDefecto()
        Get
            Return arrayValoresPorDefecto
        End Get
        Set(ByVal value As ValoresDefecto())
            arrayValoresPorDefecto = value
        End Set
    End Property
    Public Property CabeceraInforme() As String
        Get
            Return strCabeceraInforme
        End Get
        Set(ByVal value As String)
            strCabeceraInforme = value
        End Set
    End Property
    Public Property Indice() As Integer
        Get
            Return intIndice
        End Get
        Set(ByVal value As Integer)
            intIndice = value
        End Set
    End Property
    Public Property ValorPorDefecto() As String
        Get
            Return strValorDefecto
        End Get
        Set(ByVal value As String)
            strValorDefecto = value
        End Set
    End Property
    Public Property ValorMinimo() As String
        Get
            Return strValorMinimo
        End Get
        Set(ByVal value As String)
            strValorMinimo = value
        End Set
    End Property
    Public Property ValorMaximo() As String
        Get
            Return strValorMaximo
        End Get
        Set(ByVal value As String)
            strValorMaximo = value
        End Set
    End Property
    Public Property OrigenDatos() As OrigenDeDatos
        Get
            Return strucOrigenDeDatos
        End Get
        Set(ByVal value As OrigenDeDatos)
            strucOrigenDeDatos = value
        End Set
    End Property
    Public Property TieneRangoValores() As Boolean
        Get
            Return boolTieneRangoValores
        End Get
        Set(ByVal value As Boolean)
            boolTieneRangoValores = value
        End Set
    End Property
    'Propiedades de los parámetros internos de configuración
    <Serializable()> _
    Public Enum TipoDeParametro
        ParametroForm = 1
        ParametroConfig = 2
    End Enum
    Public Property TipoParametro() As TipoDeParametro
        Get
            Return enTipoParametro
        End Get
        Set(ByVal value As TipoDeParametro)
            enTipoParametro = value
        End Set
    End Property
    Public Property NombreParametroConfig() As String
        Get
            Return strNombreParametroConfig
        End Get
        Set(ByVal value As String)
            strNombreParametroConfig = value
        End Set
    End Property
    Public Property ValorParametroConfig() As String
        Get
            Return strValorParametroConfig
        End Get
        Set(ByVal value As String)
            strValorParametroConfig = value
        End Set
    End Property
    Public Property IndiceParametroConfig() As Integer
        Get
            Return intIndiceParametroConfig
        End Get
        Set(ByVal value As Integer)
            intIndiceParametroConfig = value
        End Set
    End Property
    ''' <summary>
    ''' Función que ejecuta un Procedimiento almacenado
    ''' </summary>
    ''' <param name="Nombre">Nombre del procedimiento almacenado</param>
    ''' <param name="Parametros">Array con los Nombres, Valores y tipos de datos de los parámetros del procedimiento almacenado, en orden de aparición en el Stored</param>
    ''' <returns>Un DataSet con el resultado de ejecutar el Stored procedure(Normalmente serán select)</returns>
    ''' <remarks>
    ''' Llamada desde: EjecutarSSIS.aspx
    ''' Tiempo máximo: Variable, depende del Procedimiento almacenado</remarks>
    Public Function EjecutaStoredProcedure(ByVal Nombre As String, ByVal Parametros() As FSNLibrary.TiposDeDatos.ParametrosOrigenDeDatos) As DataSet
        Dim DsetResul As New DataSet
        DsetResul = DBServer.cXML_EjecutaStored(Nombre, Parametros)
        Return DsetResul
    End Function
End Class
