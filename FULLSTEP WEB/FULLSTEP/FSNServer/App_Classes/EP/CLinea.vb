Imports System.Data.SqlClient

<Serializable()> _
Public Class CLinea
    Inherits Security

    Private mlId As Integer
    Private msCod As Object
    Private msGmn1 As Object
    Private msGmn2 As Object
    Private msGmn3 As Object
    Private msGmn4 As Object
    Private msProve As String
    Private msDen As String
    Private msDescripcionVisible As String
    Private msArtExt As Object
    Private msDest As String
    Private msGenerico As Integer
    Private msDestDen As String
    Private msUP As Object
    Private msUniDen As Object
    Private mdPrecUc As Object
    Private mdCantPed As Object
    Private mdCantRec As Object
    Private mdCantAdj As Object
    Private mdFC As Object
    Private mdImporte As Object
    Private msEst As Object
    Private mdtFecEmision As Object
    Private mdtFecEntrega As Object
    Private mdtFecEntregaProve As Object
    Private mbEntregaObl As Object
    Private mlCat1 As Object
    Private msCat1Den As Object
    Private mlCat2 As Object
    Private msCat2Den As Object
    Private mlCat3 As Object
    Private msCat3Den As Object
    Private mlCat4 As Object
    Private msCat4Den As Object
    Private mlCat5 As Object
    Private msCat5Den As Object
    Private msObs As Object
    Private msCat As Object
    Private msCatRama As Object
    Private msComent As Object
    Private miAnyoProce As Object
    Private mlProce As Object
    Private mlItem As Object
    Private mlLineaCatalogo As Object
    Private moAdjuntos As CAdjuntos
    Private moCampo1 As CCampo
    Private moCampo2 As CCampo
    Private mvObsAdjun As Object
    Private msGMN1Proce As Object
    Private iAlmacen As Integer
    Private strAlmacen As String
    Private intActivo As Integer
    Private mImputaciones As FSNServer.Imputaciones
    Private mImputaciones_EP As List(Of ImputacionPedido)
    Private mdCantMinimaPedido As Double
    Private mdCantMaximaPedido As Double
    Private mbModificarPrecio As Boolean
    Private mbModificarUnidad As Boolean
    Private moTipoArticulo As CArticulo.strTipoArticulo
    Private mTipoRecepcion As Integer
    Private mImportePedido As Double
    Private mGestorCod As String
    Private mDesvioRecepcion As Double
    Private mbNumLinea As Integer
    Private mCentroAprovisionamiento As String
    Private _otrosDatos As List(Of CAtributoPedido)
    Private _costes As List(Of CAtributoPedido)
    Private _descuentos As List(Of CAtributoPedido)
    Private _impuestos As List(Of CImpuestos)

    <Flags()> _
        Public Enum Estado As Short
        PendienteAprobar = 0
        Aceptada = 1
        Emitida = 1
        Aprobada = 1
        RecibidoParcialmente = 2
        RecibidoEnSuTotalidad = 3
        Denegada = 20
        RechazadaProveedor = 21
    End Enum
    Public Property ID() As Integer
        Get
            ID = mlId
        End Get
        Set(ByVal Value As Integer)
            mlId = Value
        End Set
    End Property
    Public Property Cod() As Object
        Get
            Cod = msCod
        End Get
        Set(ByVal Value As Object)
            msCod = Value
        End Set
    End Property
    Public Property GMN1() As Object
        Get
            GMN1 = msGmn1
        End Get
        Set(ByVal Value As Object)
            msGmn1 = Value
        End Set
    End Property
    Public Property GMN2() As Object
        Get
            GMN2 = msGmn2
        End Get
        Set(ByVal Value As Object)
            msGmn2 = Value
        End Set
    End Property
    Public Property GMN3() As Object
        Get
            GMN3 = msGmn3
        End Get
        Set(ByVal Value As Object)
            msGmn3 = Value
        End Set
    End Property
    Public Property GMN4() As Object
        Get
            GMN4 = msGmn4
        End Get
        Set(ByVal Value As Object)
            msGmn4 = Value
        End Set
    End Property
    Public Property Prove() As String
        Get
            Prove = msProve
        End Get
        Set(ByVal Value As String)
            msProve = Value
        End Set
    End Property
    Public Property Den() As String
        Get
            Den = msDen
        End Get
        Set(ByVal Value As String)
            msDen = Value
        End Set
    End Property
    Public Property DescripcionVisible() As String
        Get
            DescripcionVisible = msDescripcionVisible
        End Get
        Set(ByVal Value As String)
            msDescripcionVisible = Value
        End Set
    End Property
    Public Property ArtExt() As Object
        Get
            ArtExt = msArtExt
        End Get
        Set(ByVal Value As Object)
            msArtExt = Value
        End Set
    End Property
    Public Property Generico() As Integer
        Get
            Generico = msGenerico
        End Get
        Set(ByVal Value As Integer)
            msGenerico = Value
        End Set
    End Property
    Public Property Dest() As String
        Get
            Dest = msDest
        End Get
        Set(ByVal Value As String)
            msDest = Value
        End Set
    End Property
    Public Property DestDen() As String
        Get
            DestDen = msDestDen
        End Get
        Set(ByVal Value As String)
            msDestDen = Value
        End Set
    End Property
    Public Property UP() As Object
        Get
            UP = msUP
        End Get
        Set(ByVal Value As Object)
            msUP = Value
        End Set
    End Property
    Public Property UniDen() As Object
        Get
            UniDen = msUniDen
        End Get
        Set(ByVal Value As Object)
            msUniDen = Value
        End Set
    End Property
    Public Property PrecUc() As Object
        Get
            PrecUc = mdPrecUc
        End Get
        Set(ByVal Value As Object)
            mdPrecUc = Value
        End Set
    End Property
    Public Property CantPed() As Object
        Get
            CantPed = mdCantPed
        End Get
        Set(ByVal Value As Object)
            mdCantPed = Value
        End Set
    End Property
    Public Property CantRec() As Object
        Get
            CantRec = mdCantRec
        End Get
        Set(ByVal Value As Object)
            mdCantRec = Value
        End Set
    End Property
    Public Property CantAdj() As Object
        Get
            CantAdj = mdCantAdj
        End Get
        Set(ByVal Value As Object)
            mdCantAdj = Value
        End Set
    End Property
    Public Property FC() As Object
        Get
            FC = mdFC
        End Get
        Set(ByVal Value As Object)
            mdFC = Value
        End Set
    End Property
    Public Property Importe() As Object
        Get
            Importe = mdImporte
        End Get
        Set(ByVal Value As Object)
            mdImporte = Value
        End Set
    End Property
    Public Property Est() As Object
        Get
            Est = msEst
        End Get
        Set(ByVal Value As Object)
            msEst = Value
        End Set
    End Property
    Public Property FecEmision() As Object
        Get
            FecEmision = mdtFecEmision
        End Get
        Set(ByVal Value As Object)
            mdtFecEmision = Value
        End Set
    End Property
    Public Property FecEntrega() As Object
        Get
            FecEntrega = mdtFecEntrega
        End Get
        Set(ByVal Value As Object)
            mdtFecEntrega = Value
        End Set
    End Property
    Public Property FecEntregaProve() As Object
        Get
            FecEntregaProve = mdtFecEntregaProve
        End Get
        Set(ByVal Value As Object)
            mdtFecEntregaProve = Value
        End Set
    End Property
    Public Property EntregaObl() As Object
        Get
            EntregaObl = mbEntregaObl
        End Get
        Set(ByVal Value As Object)
            mbEntregaObl = Value
        End Set
    End Property
    Public Property Cat1() As Object
        Get
            Cat1 = mlCat1
        End Get
        Set(ByVal Value As Object)
            mlCat1 = Value
        End Set
    End Property
    Public Property Cat1Den() As Object
        Get
            Cat1Den = msCat1Den
        End Get
        Set(ByVal Value As Object)
            msCat1Den = Value
        End Set
    End Property
    Public Property Cat2() As Object
        Get
            Cat2 = mlCat2
        End Get
        Set(ByVal Value As Object)
            mlCat2 = Value
        End Set
    End Property
    Public Property Cat2Den() As Object
        Get
            Cat2Den = msCat2Den
        End Get
        Set(ByVal Value As Object)
            msCat2Den = Value
        End Set
    End Property
    Public Property Cat3() As Object
        Get
            Cat3 = mlCat3
        End Get
        Set(ByVal Value As Object)
            mlCat3 = Value
        End Set
    End Property
    Public Property Cat3Den() As Object
        Get
            Cat3Den = msCat3Den
        End Get
        Set(ByVal Value As Object)
            msCat3Den = Value
        End Set
    End Property
    Public Property Cat4() As Object
        Get
            Cat4 = mlCat4
        End Get
        Set(ByVal Value As Object)
            mlCat4 = Value
        End Set
    End Property
    Public Property Cat4Den() As Object
        Get
            Cat4Den = msCat4Den
        End Get
        Set(ByVal Value As Object)
            msCat4Den = Value
        End Set
    End Property
    Public Property Cat5() As Object
        Get
            Cat5 = mlCat5
        End Get
        Set(ByVal Value As Object)
            mlCat5 = Value
        End Set
    End Property
    Public Property Cat5Den() As Object
        Get
            Cat5Den = msCat5Den
        End Get
        Set(ByVal Value As Object)
            msCat5Den = Value
        End Set
    End Property
    Public Property Obs() As Object
        Get
            Obs = msObs
        End Get
        Set(ByVal Value As Object)
            msObs = Value
        End Set
    End Property
    Public Property Cat() As Object
        Get
            Cat = msCat
        End Get
        Set(ByVal Value As Object)
            msCat = Value
        End Set
    End Property
    Public Property CatRama() As Object
        Get
            CatRama = msCatRama
        End Get
        Set(ByVal Value As Object)
            msCatRama = Value
        End Set
    End Property
    Public Property Coment() As Object
        Get
            Coment = msComent
        End Get
        Set(ByVal Value As Object)
            msComent = Value
        End Set
    End Property
    Public Property AnyoProce() As Object
        Get
            AnyoProce = miAnyoProce
        End Get
        Set(ByVal Value As Object)
            miAnyoProce = Value
        End Set
    End Property
    Public Property Proce() As Object
        Get
            Proce = mlProce
        End Get
        Set(ByVal Value As Object)
            mlProce = Value
        End Set
    End Property
    Public Property Item() As Object
        Get
            Item = mlItem
        End Get
        Set(ByVal Value As Object)
            mlItem = Value
        End Set
    End Property
    Public Property LineaCatalogo() As Object
        Get
            LineaCatalogo = mlLineaCatalogo
        End Get
        Set(ByVal Value As Object)
            mlLineaCatalogo = Value
        End Set
    End Property
    Public Property Adjuntos() As CAdjuntos
        Get
            Adjuntos = moAdjuntos
        End Get
        Set(ByVal Value As CAdjuntos)
            moAdjuntos = Value
        End Set
    End Property
    Public Property Campo1() As CCampo
        Get
            Campo1 = moCampo1
        End Get
        Set(ByVal Value As CCampo)
            moCampo1 = Value
        End Set
    End Property
    Public Property Campo2() As CCampo
        Get
            Campo2 = moCampo2
        End Get
        Set(ByVal Value As CCampo)
            moCampo2 = Value
        End Set
    End Property
    Public Property ObsAdjun() As Object
        Get
            ObsAdjun = mvObsAdjun
        End Get
        Set(ByVal Value As Object)
            mvObsAdjun = Value
        End Set
    End Property
    Public Property GMN1Proce() As Object
        Get
            GMN1Proce = msGMN1Proce
        End Get
        Set(ByVal Value As Object)
            msGMN1Proce = Value
        End Set
    End Property
    'Propiedad candidata a quedar obsoleta, se usara Pres5_ImportesImputados_EP
    Public Property Pres5_ImportesImputados() As FSNServer.Imputaciones
        Get
            Return mImputaciones
        End Get
        Set(ByVal value As FSNServer.Imputaciones)
            mImputaciones = value
        End Set
    End Property
    Public Property Pres5_ImportesImputados_EP() As List(Of ImputacionPedido)
        Get
            Return mImputaciones_EP
        End Get
        Set(ByVal value As List(Of ImputacionPedido))
            mImputaciones_EP = value
        End Set
    End Property
    Public Property IdAlmacen() As Integer
        Get
            Return iAlmacen
        End Get
        Set(ByVal value As Integer)
            iAlmacen = value
        End Set
    End Property
    Public Property Almacen() As String
        Get
            Return strAlmacen
        End Get
        Set(ByVal value As String)
            strAlmacen = value
        End Set
    End Property
    Public Property Activo() As Integer
        Get
            Return intActivo
        End Get
        Set(ByVal value As Integer)
            intActivo = value
        End Set
    End Property
    Public Property CantMinimaPedido() As Double
        Get
            Return mdCantMinimaPedido
        End Get
        Set(ByVal value As Double)
            mdCantMinimaPedido = value
        End Set
    End Property
    Public Property CantMaximaPedido() As Double
        Get
            Return mdCantMaximaPedido
        End Get
        Set(ByVal value As Double)
            mdCantMaximaPedido = value
        End Set
    End Property
    Public Property ModificarPrecio() As Boolean
        Get
            Return mbModificarPrecio
        End Get
        Set(ByVal value As Boolean)
            mbModificarPrecio = value
        End Set
    End Property
    Public Property ModificarUnidad() As Boolean
        Get
            Return mbModificarUnidad
        End Get
        Set(ByVal value As Boolean)
            mbModificarUnidad = value
        End Set
    End Property
    Public Property TipoArticulo() As CArticulo.strTipoArticulo
        Get
            Return moTipoArticulo
        End Get
        Set(ByVal value As CArticulo.strTipoArticulo)
            moTipoArticulo = value
        End Set
    End Property
    Public Property TipoRecepcion() As Integer
        Get
            Return mTipoRecepcion
        End Get
        Set(value As Integer)
            mTipoRecepcion = value
        End Set
    End Property
    Public Property ImportePedido() As Double
        Get
            Return Me.mImportePedido
        End Get
        Set(value As Double)
            Me.mImportePedido = value
        End Set
    End Property
    Public Property DesvioRecepcion() As Double
        Get
            Return Me.mDesvioRecepcion
        End Get
        Set(value As Double)
            Me.mDesvioRecepcion = value
        End Set
    End Property
    Public Property GestorCod() As String
        Get
            Return Me.mGestorCod
        End Get
        Set(value As String)
            Me.mGestorCod = value
        End Set
    End Property
    Public Property NumLinea() As Integer
        Get
            Return mbNumLinea
        End Get
        Set(ByVal value As Integer)
            mbNumLinea = value
        End Set
    End Property
    Public Function isRecepcionCantidad() As Boolean
        Return mTipoRecepcion = 0
    End Function
    ''' <summary>
    ''' IMporte total recibido en todas las recepciones de esa linea
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function importeRecibido() As Double
        Return mDBServer.getImporteRecibido(Me.ID)
    End Function
    Public Property OtrosDatos() As List(Of CAtributoPedido)
        Get
            Return _otrosDatos
        End Get
        Set(ByVal value As List(Of CAtributoPedido))
            _otrosDatos = value
        End Set
    End Property
    Public Property Costes() As List(Of CAtributoPedido)
        Get
            Return _costes
        End Get
        Set(ByVal value As List(Of CAtributoPedido))
            _costes = value
        End Set
    End Property
    Public Property Descuentos() As List(Of CAtributoPedido)
        Get
            Return _descuentos
        End Get
        Set(ByVal value As List(Of CAtributoPedido))
            _descuentos = value
        End Set
    End Property
    Public Property Impuestos() As List(Of CImpuestos)
        Get
            Return _impuestos
        End Get
        Set(ByVal value As List(Of CImpuestos))
            _impuestos = value
        End Set
    End Property
    Private _esLineaPedidoAbierto As Boolean
    Public Property EsLineaPedidoAbierto() As Boolean
        Get
            Return _esLineaPedidoAbierto
        End Get
        Set(ByVal value As Boolean)
            _esLineaPedidoAbierto = value
        End Set
    End Property
    Public Property CentroAprovisionamiento() As String
        Get
            Return mCentroAprovisionamiento
        End Get
        Set(ByVal value As String)
            mCentroAprovisionamiento = value
        End Set
    End Property
    Public Property SalidaAlmacen As Boolean

    ''' <summary>
    ''' Devuelve un objeto CAdjuntos con los adjuntos para la l�nea de Pedido
    ''' </summary>
    ''' <param name="bCargarEnColeccion">Si debemos cargar el objeto nuevo en la colecci�n de Adjuntos</param>
    ''' <returns>Un objeto CAdjuntos con los adjuntos para la l�nea de Pedido</returns>
    ''' <remarks>
    ''' Llamada desde: CargarAdjuntos de CLinea
    ''' Tiempo M�ximo: 0 sec
    ''' </remarks>
    Public Function CargarAdjuntos(ByVal bCargarEnColeccion As Boolean) As CAdjuntos
        Authenticate()
        Dim adoRes As New DataSet
        Dim oAdjuntos As CAdjuntos
        adoRes = mDBServer.Linea_CargarAdjuntos(mlId)

        oAdjuntos = New CAdjuntos(mDBServer, mIsAuthenticated)
        For Each fila As DataRow In adoRes.Tables(0).Rows
            'He modificado la llamada al metodo, poniendo los parametros que estaban vacios por sus valores por defecto, para quitar la clausula "Optional"
            oAdjuntos.Add(fila.Item("ID"), fila.Item("FECACT"), fila.Item("NOM"), fila.Item("TAMANO"), fila.Item("COM"), Nothing, Nothing, Nothing, Nothing, TiposDeDatos.Adjunto.Tipo.Linea_Pedido)
        Next
        adoRes.Clear()
        adoRes = Nothing

        If bCargarEnColeccion Then
            moAdjuntos = oAdjuntos
        End If

        Return oAdjuntos
        oAdjuntos = Nothing
    End Function
    ''' <summary>
    ''' Actualiza la cesta con la informaci�n de la l�nea
    ''' </summary>
    ''' <remarks>Llamada desde cesta.aspx </remarks>
    Public Sub ActualizarCestaLinea()
        mDBServer.CestaLinea_ActualizarCestaPedido(mlId, DBNullToDbl(mdCantPed), msUP, mdFC, mdPrecUc, mTipoRecepcion, mImportePedido)
    End Sub
    ''' Revisado por: blp. Fecha: 28/02/2012
    ''' <summary>
    ''' Devuelve las observaciones de la l�nea
    ''' </summary>
    ''' <returns>String Observaciones</returns>
    ''' <remarks>Llamada desde Recepcion.vb->GetObservacionesLinea(). M�x 0,1 seg.</remarks>
    Public Function GetObservaciones() As String
        Authenticate()
        Dim sObservaciones As String = DBServer.Linea_GetObservaciones(ID)
        Return sObservaciones
    End Function
    ''' Revisado por: blp. Fecha: 20/05/2013
    ''' <summary>
    ''' Devuelve el centro de coste de una l�nea de pedido
    ''' </summary>
    ''' <returns>Centro de coste. Si no lo hay, devuelve NULL</returns>
    ''' <remarks>Llamada desde Recepcion.aspx.vb-->dlLineasPedido_ItemCommand. Maximo 0,1 seg.</remarks>
    Public Function DevolverCentro(ByVal idioma As FSNLibrary.Idioma) As DataSet
        Authenticate()
        Return DBServer.Linea_DevolverCentro(mlId, idioma)
    End Function
    ''' Revisado por: blp. Fecha: 23/05/2013
    ''' <summary>
    ''' Devuelve la denominaci�n del proveedor, el id de la orden y el c�digo de la moneda que corresponden a la l�nea de pedido pasada como par�metro
    ''' </summary>
    ''' <returns>datarow con los tres datos</returns>
    ''' <remarks>Llamada desde DetArticuloPedido.ascx.vb. Maximo 0,1 seg.</remarks>
    Public Function DevolverProveOrdenMon() As DataRow
        Authenticate()
        Return DBServer.Linea_DevolverProveOrdenMon(mlId)
    End Function
    ''' <summary>
    ''' Constructor de un objeto de la clase CLinea
    ''' </summary>
    ''' <param name="dbserver">Servidor de la BBDD</param>
    ''' <param name="isAuthenticated">Si est� autenticado</param>
    ''' <remarks>
    ''' Llamada desde: Todos los sitios desde donde se isntancien objetos CLInea
    ''' Tiempo m�ximo = 0 sec.</remarks>
    Public Sub New(ByRef dbserver As Fullstep.FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
        Pres5_ImportesImputados_EP = New List(Of ImputacionPedido)
        Impuestos = New List(Of CImpuestos)
    End Sub
    ''' <summary>
    ''' Constructor de un objeto de la clase CLinea
    ''' </summary>
    ''' <remarks>
    ''' Llamada desde: Todos los sitios desde donde se isntancien objetos CLInea
    ''' Tiempo m�ximo = 0 sec.</remarks>
    Public Sub New()
        Pres5_ImportesImputados_EP = New List(Of ImputacionPedido)
        Impuestos = New List(Of CImpuestos)
    End Sub
End Class