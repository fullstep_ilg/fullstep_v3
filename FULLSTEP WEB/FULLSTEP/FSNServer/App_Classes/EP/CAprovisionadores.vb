﻿Option Strict Off
Option Explicit On
Imports System.Data.SqlClient
'Ahora definida como clase publica
<System.Runtime.InteropServices.ProgId("CProveedores_NET.CAprovisionadores")> Public Class CAprovisionadores
    Inherits Security

    ''' <summary>
    ''' Constructor de la clase CAprovisionadores
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si está autenticado</param>
    ''' <remarks>
    ''' Llamada desde: Todos los sitios donde se instancien objetos de esta clase
    ''' Tiempo máximo: 0 sec. </remarks>
    Public Sub New(ByRef dbserver As Fullstep.FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub

    ''' <summary>
    ''' Función que devuelve un dataset con los aprovisionadores para el usuario actual
    ''' </summary>
    ''' <param name="sPer">Codigo de la persona actual</param>
    ''' <returns>un dataset con los aprovisionadores</returns>
    ''' <remarks>
    ''' Llamada desde: El Page_Init de Report_Consumos.aspx
    ''' Tiempo: 0 seg.</remarks>
    Public Function DevolverAprovisionadoresParaAprobador(ByVal sPer As String) As DataSet
        Return DBServer.Aprovisionadores_DevolverAprovisionadoresParaAprobador(sPer)
    End Function
End Class
