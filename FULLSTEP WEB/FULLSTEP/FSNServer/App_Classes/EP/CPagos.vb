﻿<Serializable()> _
Public Class CPagos
    Inherits Lista(Of CPago)

    ''' <summary>
    ''' Añade un nuevo elemento CPago a la lista
    ''' </summary>
    ''' <param name="IIdPago">Integer. ID de Pago</param>
    ''' <param name="IIdFra">Integer. ID de Factura</param>
    ''' <param name="sNumPago">String. Número de Pago</param>
    ''' <param name="sNumFra">String. Número de Factura</param>
    ''' <param name="dtFecha">Datetime. Fecha del Pago</param>
    ''' <param name="ILineaFactura">Integer. Linea de factura</param>
    ''' <param name="ILineaPedido">Integer. Id de la línea de pedido</param>
    ''' <returns>El elemento CPago añadido</returns>
    Public Overloads Function Add(ByVal IIdPago As Integer, ByVal IIdFra As Integer, ByVal sNumPago As String, ByVal sNumFra As String, ByVal dtFecha As DateTime, ByVal ILineaFactura As Integer, ByVal ILineaPedido As Integer) As CPago
        Dim objnewmember As CPago
        objnewmember = New CPago(mDBServer, mIsAuthenticated)

        With objnewmember
            .IdPago = IIdPago
            .IdFra = IIdFra
            .NumPago = sNumPago
            .NumFra = sNumFra
            .Fecha = dtFecha
            '.LineaFactura = ILineaFactura
            .LineaPedido = ILineaPedido
        End With

        Me.Add(objnewmember)
        Return objnewmember
    End Function
    ''' <summary>
    ''' Función que devuelve un objeto de tipo CPagos con los datos de los pagos vinculados a las facturas vinculadas a su vez a la linea de pedido dada. Si no hay línea de pedido, devuelve todas la líneas -> facturas -> pagos
    ''' </summary>
    ''' <param name="idioma">Idioma en el que se quieren recuperar los datos en multiidioma</param>
    ''' <param name="Linea_Pedido">Integer. Opcional. Id de la línea de pedido de la que se quiere recuperar los pagos vinculados a las facturas vinculadas a esa línea</param>
    ''' <returns>Un objeto de la clase CPagos con la relación de pagos-facturas-lineas de facturas-lineas de pedido</returns>
    ''' <remarks>
    ''' Llamada desde: </remarks>
    Public Function CargarPagos(ByVal idioma As FSNLibrary.Idioma, Optional ByVal Linea_Pedido As Integer = 0, Optional ByVal lIdFactura As Long = 0) As CPagos
        Dim oCPagos As CPagos
        Dim oCPago As CPago
        Dim oRoot As New Root

        oCPagos = New CPagos(DBServer, mIsAuthenticated)

        Dim DsResul As New DataSet
        DsResul = DBServer.Linea_DevolverPagos(idioma, Linea_Pedido, lIdFactura)
        If DsResul.Tables(0).Rows.Count > 0 Then
            For i As Long = 0 To DsResul.Tables(0).Rows.Count - 1
                oCPago = New CPago(DBServer, mIsAuthenticated)
                With DsResul.Tables(0).Rows(i)
                    oCPago.IdPago = .Item("PAGO_ID")
                    oCPago.IdFra = .Item("FAC_ID")
                    oCPago.NumPago = .Item("PAGO_NUM")
                    oCPago.NumFra = .Item("FAC_NUM")
                    oCPago.Fecha = .Item("FECHA")
                    oCPago.LineaPedido = .Item("LINEA_PEDIDO")

                    If .Item("EST_ID") Is DBNull.Value Then
                        oCPago.EstadoId = 0
                    Else
                        oCPago.EstadoId = .Item("EST_ID")
                    End If
                    If .Item("EST_DEN") Is DBNull.Value Then
                        oCPago.EstadoDen = ""
                    Else
                        oCPago.EstadoDen = .Item("EST_DEN")
                    End If

                End With
                oCPagos.Add(oCPago)
                oCPago = Nothing
            Next
        Else
            Return Nothing
        End If

        Return oCPagos
    End Function
    ''' <summary>
    ''' Función que devuelve un dataset con los estados de Pago existentes en el idioma que usa el usuario.
    ''' </summary>
    ''' <returns>un dataset con los estados de Pago existentes en el idioma que usa el usuario</returns>
    ''' <remarks>
    ''' Llamada desde: EpWeb.Seguimientoaspx.vb.EstadosPago</remarks>
    Public Function CargarEstadosPago(ByVal idioma As FSNLibrary.Idioma) As DataSet
        Authenticate()
        Dim dsEstadosPago As DataSet = DBServer.Pagos_DevolverEstadosPago(idioma.ToString)
        Return dsEstadosPago
    End Function
    ''' <summary>
    ''' Constructor de la clase CPagos
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si está autenticado</param>
    ''' <remarks></remarks>
    Public Sub New(ByRef dbserver As Fullstep.FSNDataBaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
    End Sub
End Class
