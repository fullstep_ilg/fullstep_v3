﻿Imports Fullstep.FSNLibrary
Imports System.Data.SqlClient
Imports System.Web

<Serializable()>
Public Class CEmisionPedidos
    Inherits Security

    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
    ''' <summary>
    ''' Función que devuelve los datos de DatosGenerales
    ''' </summary>
    ''' <returns>Un dataset con los datos de todos los materiales jerarquerizados en arbol</returns>
    ''' <remarks>
    ''' Llamada desde: La página Report_XML
    ''' Tiempo máximo: 1 seg</remarks>
    Public Function Cargar_Datos_Generales(ByVal UsuCod As String, ByVal iId As Integer, ByVal iFavorito As Integer, ByVal iCodFavorito As Integer, ByVal Idioma As FSNLibrary.Idioma, Optional ByVal iModiFav As Integer = 0) As CDatosGenerales
        Authenticate()
        Dim DsResul As New DataSet
        Dim datosGenerales As New CDatosGenerales
        DsResul = DBServer.Cargar_Datos_Generales(iId, iFavorito, iCodFavorito, iModiFav)

        If Not DsResul.Tables(0).Rows.Count = 0 Then
            Dim drDatosGenerales As DataRow = DsResul.Tables(0).Rows(0)
            If (iModiFav = 1) Then
                datosGenerales.Id = iCodFavorito
            Else
                datosGenerales.Id = iId
            End If
            datosGenerales.CodProve = DBNullToStr(drDatosGenerales.Item("PROVE"))
            datosGenerales.DenProve = DBNullToStr(drDatosGenerales.Item("PROVEDEN"))
            datosGenerales.CodProvePortal = DBNullToStr(drDatosGenerales.Item("FSP_COD"))
            datosGenerales.ProveSolicitAceptacionRecepcion = SQLBinaryToBoolean(drDatosGenerales.Item("SOLICITAR_ACEPTACION_RECEPCION"))
            datosGenerales.Emp = DBNullToStr(drDatosGenerales.Item("EMP"))
            datosGenerales.Receptor = DBNullToStr(drDatosGenerales.Item("RECEPTOR"))
            datosGenerales.CodErp = DBNullToStr(drDatosGenerales.Item("COD_PROVE_ERP"))
            datosGenerales.Cambio = drDatosGenerales.Item("EQUIV")
            datosGenerales.Referencia = DBNullToStr(drDatosGenerales.Item("NUM_PED_ERP"))
            If Not drDatosGenerales.IsNull("ACEP_PROVE") Then datosGenerales.AcepProve = SQLBinaryToBoolean(drDatosGenerales.Item("ACEP_PROVE"))
            If Not IsDBNull(drDatosGenerales.Item("TIPOPEDIDO")) Then
                Dim cTiposPedidos As cTiposPedidos
                cTiposPedidos = New cTiposPedidos(mDBServer, mIsAuthenticated)
                cTiposPedidos.CargarTiposPedido(Idioma)
                Dim oTipoPedido As cTipoPedido
                oTipoPedido = New cTipoPedido(mDBServer, mIsAuthenticated)
                oTipoPedido = cTiposPedidos.ItemId(DBNullToInteger(drDatosGenerales.Item("TIPOPEDIDO")))

                datosGenerales.Tipo = New cTipoPedido(mDBServer, mIsAuthenticated)
                datosGenerales.Tipo = oTipoPedido
            End If
            datosGenerales.DirEnvio = DBNullToInteger(drDatosGenerales.Item("FAC_DIR_ENVIO"))
            datosGenerales.Obs = DBNullToStr(drDatosGenerales.Item("OBS"))
            datosGenerales.Moneda = DBNullToStr(drDatosGenerales.Item("MON"))
            If Not iFavorito = 1 Then
                datosGenerales.EsPedidoAbierto = DBNullToBoolean(drDatosGenerales.Item("ORDEN_PED_ABIERTO"))
                If datosGenerales.EsPedidoAbierto Then datosGenerales.TipoPedido = TipoDePedido.ContraPedidoAbierto
                datosGenerales.OrgCompras = DBNullToStr(drDatosGenerales.Item("ORGCOMPRAS"))
            Else
                datosGenerales.OrgCompras = DBNullToStr(drDatosGenerales.Item("ORGCOMPRAS"))
            End If
            If Not DsResul.Tables(1).Rows.Count = 0 Then
                Dim drNomFav As DataRow = DsResul.Tables(1).Rows(0)
                datosGenerales.NomFav = DBNullToStr(drNomFav.Item("NOMFAV"))
            Else
                datosGenerales.NomFav = String.Empty
            End If

            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            With FSNServer.TipoAcceso
                'Miramos los pres1-4
                datosGenerales.GS_Pres1 = New Pres1_GS
                datosGenerales.GS_Pres1.Activo = (.gbUsarPres1 AndAlso .gbUsarPedPres1)
                datosGenerales.GS_Pres1.Denominacion = .gbPres1Denominacion(Idioma.ToString)
                datosGenerales.GS_Pres1.Obligatorio = .gbPres1Obligatorio
                datosGenerales.GS_Pres1.NivelMinimo = .giPres1Nivel

                datosGenerales.GS_Pres2 = New Pres2_GS
                datosGenerales.GS_Pres2.Activo = (.gbUsarPres2 AndAlso .gbUsarPedPres2)
                datosGenerales.GS_Pres2.Denominacion = .gbPres2Denominacion(Idioma.ToString)
                datosGenerales.GS_Pres2.Obligatorio = .gbPres2Obligatorio
                datosGenerales.GS_Pres2.NivelMinimo = .giPres2Nivel

                datosGenerales.GS_Pres3 = New Pres3_GS
                datosGenerales.GS_Pres3.Activo = (.gbUsarPres3 AndAlso .gbUsarPedPres3)
                datosGenerales.GS_Pres3.Denominacion = .gbPres3Denominacion(Idioma.ToString)
                datosGenerales.GS_Pres3.Obligatorio = .gbPres3Obligatorio
                datosGenerales.GS_Pres3.NivelMinimo = .giPres3Nivel

                datosGenerales.GS_Pres4 = New Pres4_GS
                datosGenerales.GS_Pres4.Activo = (.gbUsarPres4 AndAlso .gbUsarPedPres4)
                datosGenerales.GS_Pres4.Denominacion = .gbPres4Denominacion(Idioma.ToString)
                datosGenerales.GS_Pres4.Obligatorio = .gbPres4Obligatorio
                datosGenerales.GS_Pres4.NivelMinimo = .giPres4Nivel

                'Para saber si mostrar el combo de centros de aprovisionamiento
                datosGenerales.UsarOrgCompras = .gbUsar_OrgCompras
                datosGenerales.TipoRecepcionAutomatica = .gbEPRecepcionAutomatica
            End With
        End If

        Return datosGenerales
    End Function
    ''' <summary>
    ''' Función que devuelve las lineas
    ''' </summary>
    ''' <returns>Un dataset con los datos de todos los materiales jerarquerizados en arbol</returns>
    ''' <remarks>
    ''' Llamada desde: La página Report_XML
    ''' Tiempo máximo: 1 seg</remarks>
    Public Function Cargar_Lineas(ByVal UsuCod As String, ByVal Idioma As FSNLibrary.Idioma, ByVal iId As Integer, ByVal iDesdeFavorito As Integer, ByVal idEmpresa As Integer) As Object
        Authenticate()
        Dim DsResul As New DataSet
        Dim oLineas As CLineasPedido
        Dim i As Integer
        Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
        DsResul = DBServer.Ordenes_CargarLineasCesta(UsuCod, Idioma, iId, iDesdeFavorito)
        DsResul.Tables(0).TableName = "CESTA"
        'rellenar info con consulta
        oLineas = New CLineasPedido(mDBServer, mIsAuthenticated)

        Dim keycesta() As DataColumn = {DsResul.Tables(0).Columns("IDLINEA")}
        DsResul.Tables(0).PrimaryKey = keycesta
        Dim keyrelim() As DataColumn = {DsResul.Tables(2).Columns("CESTA")}
        DsResul.Relations.Add("REL_CESTA_IMPUTACIONES", keycesta, keyrelim, False)
        If Not iDesdeFavorito Then
            Dim keyrelPluri() As DataColumn = {DsResul.Tables(6).Columns("CESTA")}
            DsResul.Relations.Add("REL_CESTA_IMPUTACIONES_PLURIANUAL", keycesta, keyrelPluri, False)
        End If
        Dim keyrelimpu() As DataColumn = {DsResul.Tables(3).Columns("IDLINEA")}
        DsResul.Relations.Add("REL_IMPUESTOS", keycesta, keyrelimpu, False)
        If Not iDesdeFavorito Then
            Dim keyrelpres() As DataColumn = {DsResul.Tables(4).Columns("CESTA")}
            DsResul.Relations.Add("REL_PRESUPUESTOS", keycesta, keyrelpres, False)
        End If
        Dim keyreladj() As DataColumn
        keyreladj = {DsResul.Tables(1).Columns("CESTA_LINEA")}
        DsResul.Relations.Add("REL_ADJUNTOS", keycesta, keyreladj, False)
        If Not iDesdeFavorito Then
            Dim keyPlanEntrega() As DataColumn = {DsResul.Tables(5).Columns("CESTA")}
            DsResul.Relations.Add("REL_PLAN_ENTREGA", keycesta, keyPlanEntrega, False)
        End If
        Dim bRecalcularNumerosLinea As Boolean = False
        If Not DsResul.Tables(0).Rows.Count = 0 Then
            i = 0
            For Each linea As DataRow In DsResul.Tables(0).Rows
                Dim oLinea As New CLineaPedido(mDBServer, mIsAuthenticated)
                oLinea.ID = linea.Item("IDLINEA")
                If Not iDesdeFavorito Then oLinea.NumLinModificado = SQLBinaryToBoolean(linea.Item("EDIT_NUM"))
                'Numeracion lineas
                If oLinea.NumLinModificado Then
                    If Not IsDBNull(linea.Item("NUM_LINEA")) Then
                        oLinea.NumLinea = linea.Item("NUM_LINEA")
                    Else
                        If oLineas(i - 1).NumLinea = 0 Then
                            oLinea.NumLinea = 1000
                        Else
                            Select Case i
                                Case 1
                                    oLinea.NumLinea = oLineas(i - 1).NumLinea + oLineas(i - 1).NumLinea
                                Case Else
                                    oLinea.NumLinea = oLineas(i - 1).NumLinea + (oLineas(i - 1).NumLinea - oLineas(i - 2).NumLinea)
                            End Select
                            If oLinea.NumLinea > 999 Then oLinea.NumLinea = 1000
                        End If
                    End If
                Else
                    bRecalcularNumerosLinea = True
                End If

                i += 1
                oLinea.AnyoProce = linea.Item("ANYO")
                oLinea.GMN1Proce = linea.Item("GMN1")
                oLinea.Proce = linea.Item("PROCE")
                If Not iDesdeFavorito Then oLinea.Item = linea.Item("ITEM")
                oLinea.LineaCatalogo = DBNullToDbl(linea.Item("LINEA"))
                oLinea.Prove = DBNullToStr(linea.Item("PROVE"))
                oLinea.Cod = DBNullToStr(linea.Item("COD_ITEM"))
                oLinea.Den = linea.Item("ART_DEN")
                oLinea.Generico = DBNullToInteger(linea.Item("GENERICO")) 'PARA MODIFICAR LA DESCRIPCION
                oLinea.CantPed = linea.Item("CANT")
                If linea.Item("NUMDECUP") Is System.DBNull.Value Then
                    oLinea.NumDecUP = Nothing
                Else
                    oLinea.NumDecUP = CType(linea.Item("NUMDECUP"), Integer)
                End If
                oLinea.PrecUc = linea.Item("PREC")
                oLinea.FC = linea.Item("FC")
                oLinea.CantMinimaPedido = DBNullToDbl(linea.Item("CANT_MIN_DEF"))
                oLinea.CantMaximaPedido = DBNullToDbl(linea.Item("CANT_MAX_DEF"))
                oLinea.GMN1 = DBNullToStr(linea.Item("GMN1"))
                oLinea.Dest = DBNullToStr(linea.Item("DEST"))
                oLinea.DestDen = DBNullToStr(linea.Item("DEST_DEN"))
                oLinea.Cat1 = DBNullToDbl(linea.Item("CAT1"))
                oLinea.Cat2 = DBNullToDbl(linea.Item("CAT2"))
                oLinea.Cat3 = DBNullToDbl(linea.Item("CAT3"))
                oLinea.Cat4 = DBNullToDbl(linea.Item("CAT4"))
                oLinea.Cat5 = DBNullToDbl(linea.Item("CAT5"))
                oLinea.DesvioRecepcion = DBNullToDbl(linea.Item("PORCEN_DESVIO"))
                If Not IsDBNull(linea.Item("CAMPO1")) Then
                    oLinea.Campo1 = New CCampo(mDBServer, mIsAuthenticated)
                    With oLinea.Campo1
                        .Den = linea.Item("DEN1")
                        .ID = linea.Item("CAMPO1")
                        .Obligatorio = linea.Item("OBL1")
                        .Valor = DBNullToStr(linea.Item("VALOR1"))
                    End With
                End If
                If Not IsDBNull(linea.Item("CAMPO2")) Then
                    oLinea.Campo2 = New CCampo(mDBServer, mIsAuthenticated)
                    With oLinea.Campo2
                        .Den = linea.Item("DEN2")
                        .ID = linea.Item("CAMPO2")
                        .Obligatorio = linea.Item("OBL2")
                        .Valor = DBNullToStr(linea.Item("VALOR2"))
                    End With
                End If
                oLinea.ModificarPrecio = SQLBinaryToBoolean(linea.Item("MODIF_PREC"))
                oLinea.ModificarUnidad = SQLBinaryToBoolean(linea.Item("MODIF_UNI"))
                If linea.Item("FECENTREGA") IsNot DBNull.Value Then _
                    oLinea.FecEntrega = Format(linea.Item("FECENTREGA"), FSNUser.DateFormat.ShortDatePattern)
                oLinea.EntregaObl = SQLBinaryToBoolean(linea.Item("ENTREGA_OBL"))
                oLinea.Obs = DBNullToStr(linea.Item("OBS"))
                Dim oTipoArt As CArticulo.strTipoArticulo
                oTipoArt.Concepto = linea.Item("CONCEPTO")
                oTipoArt.TipoAlmacenamiento = linea.Item("ALMACENAR")
                oTipoArt.TipoRecepcion = linea.Item("RECEPCIONAR")
                oLinea.TipoArticulo = oTipoArt
                oLinea.IdAlmacen = DBNullToInteger(linea.Item("ALMACEN"))
                oLinea.Activo = DBNullToInteger(linea.Item("ACTIVO"))
                oLinea.DescripcionActivo = DBNullToStr(linea.Item("DEN_ACTIVO"))
                oLinea.ImportePedido = DBNullToDbl(linea.Item("IMPORTE_PED"))
                oLinea.TipoRecepcion = DBNullToInteger(linea.Item("TIPORECEPCION"))
                oLinea.UP = linea.Item("UP").ToString
                oLinea.Mon = linea.Item("MON").ToString
                If Not iDesdeFavorito Then oLinea.Solicit = DBNullToInteger(linea.Item("SOLICIT"))
                If Not iDesdeFavorito Then oLinea.Campo_Solicit = DBNullToInteger(linea.Item("CAMPO_SOLICIT"))
                If Not iDesdeFavorito Then oLinea.Linea_Solicit = DBNullToInteger(linea.Item("LINEA_SOLICIT"))
                If Not iDesdeFavorito Then oLinea.IdLineaPedidoAbierto = DBNullToInteger(linea.Item("LINEA_PED_ABIERTO"))
                If Not iDesdeFavorito Then oLinea.ConRecepcionAutomatica = DBNullToBoolean(linea.Item("CON_RECEPCION_AUTOMATICA"))
                If Not iDesdeFavorito Then oLinea.ConPlanEntrega = DBNullToBoolean(linea.Item("CON_PLAN_ENTREGA"))
                'Presupuestos GS
                Dim nivelPres As Integer
                Dim valor As String
                With oLinea
                    For Each pres As DataRow In linea.GetChildRows("REL_PRESUPUESTOS")
                        nivelPres = IIf(Not pres("PRES4").ToString = "", 4, IIf(Not pres("PRES3").ToString = "", 3, IIf(Not pres("PRES2").ToString = "", 2, 1)))
                        valor = nivelPres & "_" & pres("PRES1").ToString & pres("PRES2").ToString & pres("PRES3").ToString & pres("PRES4").ToString & "_" & Numero(pres("PORCEN"), ".", ",")
                        Select Case pres("TIPO")
                            Case 1
                                oLinea.Pres1 &= valor
                                oLinea.Pres1Den &= pres("COD").ToString & "(" & Numero(pres("PORCEN"), ".", ",") * 100 & "%);"
                            Case 2
                                oLinea.Pres2 &= valor
                                oLinea.Pres2Den &= pres("COD").ToString & "(" & Numero(pres("PORCEN"), ".", ",") * 100 & "%);"
                            Case 3
                                oLinea.Pres3 &= valor
                                oLinea.Pres3Den &= pres("COD").ToString & "(" & Numero(pres("PORCEN"), ".", ",") * 100 & "%);"
                            Case Else
                                oLinea.Pres4 &= valor
                                oLinea.Pres4Den &= pres("COD").ToString & "(" & Numero(pres("PORCEN"), ".", ",") * 100 & "%);"
                        End Select
                    Next
                End With

                'If Not iDesdeFavorito Then
                oLinea.CentroAprovisionamiento = linea("CENTROS").ToString
                oLinea.CentroAprovisionamiento_Den = linea("CENTROS").ToString & " - " & linea("CENTROS_DEN").ToString
                'End If

                oLinea.Pres5_ImportesImputados_EP = New List(Of Imputacion) 'FSNServer.Imputaciones(DBServer, mIsAuthenticated)
                For Each linimputacion As DataRow In linea.GetChildRows("REL_CESTA_IMPUTACIONES")
                    oLinea.GestorCod = DBNullToStr(linimputacion.Item("GESTOR"))
                    Dim oImp As FSNServer.Imputacion = New FSNServer.Imputacion(DBServer, mIsAuthenticated)
                    oImp.Id = linimputacion.Item("ID")
                    oImp.LineaId = linimputacion.Item("CESTA")
                    oImp.CentroCoste = New FSNServer.UON(DBServer, mIsAuthenticated)
                    oImp.CentroCoste.UON1 = DBNullToSomething(linimputacion.Item("UON1CS"))
                    oImp.CentroCoste.UON2 = DBNullToSomething(linimputacion.Item("UON2CS"))
                    oImp.CentroCoste.UON3 = DBNullToSomething(linimputacion.Item("UON3CS"))
                    oImp.CentroCoste.UON4 = DBNullToSomething(linimputacion.Item("UON4CS"))
                    oImp.CentroCoste.CentroSM = New FSNServer.Centro_SM(DBServer, mIsAuthenticated)
                    oImp.CentroCoste.CentroSM.Codigo = linimputacion.Item("CENTRO_SM")
                    oImp.CentroCoste.CentroSM.Denominacion = linimputacion.Item("DENCENTRO")
                    If Not IsDBNull(linimputacion.Item("PRES5_IMP")) Then
                        oImp.CentroCoste.CentroSM.PartidasPresupuestarias = New FSNServer.PartidasPRES5(DBServer, mIsAuthenticated)
                        Dim oPart As FSNServer.PartidaPRES5 = New FSNServer.PartidaPRES5(DBServer, mIsAuthenticated)
                        oPart.NIV0 = linimputacion.Item("PRES0")
                        oPart.NIV1 = linimputacion.Item("PRES1")
                        oPart.NIV2 = DBNullToSomething(linimputacion.Item("PRES2"))
                        oPart.NIV3 = DBNullToSomething(linimputacion.Item("PRES3"))
                        oPart.NIV4 = DBNullToSomething(linimputacion.Item("PRES4"))
                        oPart.PresupuestoId = linimputacion.Item("PRES5_IMP")
                        oPart.Denominacion = linimputacion.Item("DENPART")
                        oPart.FechaInicioPresupuesto = DBNullToSomething(linimputacion.Item("FECINI"))
                        oPart.FechaFinPresupuesto = DBNullToSomething(linimputacion.Item("FECFIN"))
                        oImp.Partida = oPart
                        oImp.CentroCoste.CentroSM.PartidasPresupuestarias.Add(oPart)
                    End If
                    oLinea.Pres5_ImportesImputados_EP.Add(oImp)
                Next

                '9624
                oLinea.PluriAnual = False
                If Not iDesdeFavorito Then
                    If linea.GetChildRows("REL_CESTA_IMPUTACIONES").Count = 0 Then
                        For Each linimputacion As DataRow In linea.GetChildRows("REL_CESTA_IMPUTACIONES_PLURIANUAL")
                            oLinea.PluriAnual = True
                            If idEmpresa = DBNullToDbl(linimputacion.Item("EMPRESA")) Then
                                oLinea.GestorCod = DBNullToStr(linimputacion.Item("GESTOR"))
                                Dim oImp As FSNServer.Imputacion = New FSNServer.Imputacion(DBServer, mIsAuthenticated)
                                oImp.Id = linimputacion.Item("ID")
                                oImp.LineaId = linimputacion.Item("CESTA")
                                oImp.CentroCoste = New FSNServer.UON(DBServer, mIsAuthenticated)
                                oImp.CentroCoste.UON1 = DBNullToSomething(linimputacion.Item("UON1CS"))
                                oImp.CentroCoste.UON2 = DBNullToSomething(linimputacion.Item("UON2CS"))
                                oImp.CentroCoste.UON3 = DBNullToSomething(linimputacion.Item("UON3CS"))
                                oImp.CentroCoste.UON4 = DBNullToSomething(linimputacion.Item("UON4CS"))
                                oImp.CentroCoste.CentroSM = New FSNServer.Centro_SM(DBServer, mIsAuthenticated)
                                oImp.CentroCoste.CentroSM.Codigo = linimputacion.Item("CENTRO_SM")
                                oImp.CentroCoste.CentroSM.Denominacion = linimputacion.Item("DENCENTRO")
                                If Not IsDBNull(linimputacion.Item("PRES5_IMP")) Then
                                    oImp.CentroCoste.CentroSM.PartidasPresupuestarias = New FSNServer.PartidasPRES5(DBServer, mIsAuthenticated)
                                    Dim oPart As FSNServer.PartidaPRES5 = New FSNServer.PartidaPRES5(DBServer, mIsAuthenticated)
                                    oPart.NIV0 = linimputacion.Item("PRES0")
                                    oPart.NIV1 = linimputacion.Item("PRES1")
                                    oPart.NIV2 = DBNullToSomething(linimputacion.Item("PRES2"))
                                    oPart.NIV3 = DBNullToSomething(linimputacion.Item("PRES3"))
                                    oPart.NIV4 = DBNullToSomething(linimputacion.Item("PRES4"))
                                    oPart.PresupuestoId = linimputacion.Item("PRES5_IMP")
                                    oPart.Denominacion = linimputacion.Item("DENPART")
                                    oPart.FechaInicioPresupuesto = DBNullToSomething(linimputacion.Item("FECINI"))
                                    oPart.FechaFinPresupuesto = DBNullToSomething(linimputacion.Item("FECFIN"))
                                    oImp.Partida = oPart
                                    oImp.CentroCoste.CentroSM.PartidasPresupuestarias.Add(oPart)
                                End If
                                oLinea.Pres5_ImportesImputados_EP.Add(oImp)
                            End If
                        Next
                    End If
                End If

                For Each rImpuesto As DataRow In linea.GetChildRows("REL_IMPUESTOS")
                    Dim oImpuesto As FSNServer.CImpuestos = New FSNServer.CImpuestos()
                    oImpuesto.Id = rImpuesto.Item("ID")
                    oImpuesto.Codigo = rImpuesto.Item("COD")
                    oImpuesto.Denominacion = rImpuesto.Item("DEN")
                    oImpuesto.Valor = rImpuesto.Item("VALOR")
                    oLinea.Impuestos.Add(oImpuesto)
                Next
                If Not iDesdeFavorito Then
                    Dim oPlanEntrega As PlanEntrega
                    For Each iPlanEntrega As DataRow In linea.GetChildRows("REL_PLAN_ENTREGA")
                        oPlanEntrega = New PlanEntrega
                        With oPlanEntrega
                            .IdPlanEntrega = iPlanEntrega("ID")
                            If Not IsDBNull(iPlanEntrega("FECHAENTREGA")) Then .FechaEntrega = CDate(iPlanEntrega("FECHAENTREGA"))
                            .CantidadEntrega = DBNullToDbl(iPlanEntrega("CANTIDAD"))
                            .ImporteEntrega = DBNullToDbl(iPlanEntrega("IMPORTE"))
                        End With
                        oLinea.PlanEntrega.Add(oPlanEntrega)
                    Next
                End If
                oLineas.Add(oLinea)
                For Each rAdjunto As DataRow In linea.GetChildRows("REL_ADJUNTOS")
                    Dim oAdjunto As FSNServer.Adjunto = New FSNServer.Adjunto()
                    'No hace falta que cargue todo, cuando vaya a mostrar los archivos recupera todos los datos
                    oAdjunto.Id = rAdjunto.Item("ID")
                    oAdjunto.Nombre = rAdjunto.Item("NOM")
                    If Not iDesdeFavorito Then
                        oAdjunto.tipo = TiposDeDatos.Adjunto.Tipo.Cesta
                    Else
                        oAdjunto.tipo = TiposDeDatos.Adjunto.Tipo.Favorito_Linea_Pedido
                    End If
                    oLinea.Adjuntos.Add(oAdjunto)
                Next
            Next
            If bRecalcularNumerosLinea Then RecalcularNumerosLinea(oLineas)
        End If
        For k As Integer = 0 To oLineas.Count - 1
            DsResul.Tables("CESTA").Rows(k)("NUM_LINEA") = oLineas(k).NumLinea
        Next

        DsResul.Relations.Clear()
        Return {DsResul, oLineas}
    End Function
    Private Sub RecalcularNumerosLinea(ByRef oLineas As CLineasPedido)
        Dim casoNumLinea As Integer
        Select Case oLineas.Count
            Case 1 To 99
                casoNumLinea = 1
            Case 100 To 200
                casoNumLinea = 2
            Case 201 To 499
                casoNumLinea = 3
            Case 500 To 999
                casoNumLinea = 4
            Case Else
                casoNumLinea = 5
        End Select
        For i As Integer = 0 To oLineas.Count - 1
            Select Case casoNumLinea
                Case 1
                    oLineas(i).NumLinea = (i + 1) * 10
                Case 2
                    oLineas(i).NumLinea = IIf(i = 0, 1, i * 5)
                Case 3
                    oLineas(i).NumLinea = (i * 2) + 1
                Case 4
                    oLineas(i).NumLinea = i + 1
                Case Else
                    If i > 998 Then
                        oLineas(i).NumLinea = 1000
                    Else
                        oLineas(i).NumLinea = i + 1
                    End If
            End Select
        Next
    End Sub
    ''' <summary>
    ''' Devuelve los datos de los proveedores ERP para la empresa dada, o el proveedor dado, o sólo uno si se incluye el código
    ''' </summary>
    ''' <param name="lEmpresa">Código de la empresa, si no se introduce ninguno, se devolvera para todas</param>
    ''' <param name="sProve">Código del proveedor, si no se introduce ninguno, se devolvera para todos</param>
    ''' <param name="sCodERP">Código Erp, si se introduce solo se sacaran los datos de este</param>
    ''' <remarks>
    ''' Llamada desde: EpWeb, página EmisionPedido, método DataList1_ItemDataBound
    ''' Tiempo máximo: 50 milisegundos</remarks>
    Public Function CargarProveedoresERP(ByVal UsuCod As String, ByVal lEmpresa As Integer, ByVal sProve As String, ByVal sCodERP As String) As CProveERPs
        Authenticate()
        Dim rs As New DataSet

        Dim oProvesERP As FSNServer.CProveERPs
        oProvesERP = New CProveERPs(mDBServer, True) 'FSNServer.Get_Object(GetType(FSNServer.CProveERPs))
        If sCodERP = "0" Then
            sCodERP = String.Empty
        End If
        rs = mDBServer.ProvesErp_CargarProveedoresERP(lEmpresa, sProve, sCodERP)
        If rs.Tables(0).Rows.Count = 0 Then
            rs.Clear()
            CargarProveedoresERP = oProvesERP
            Exit Function
        Else
            CargarProveedoresERP = oProvesERP
            For Each fila As DataRow In rs.Tables(0).Rows
                oProvesERP.Add(DBNullToStr(fila.Item("COD_ERP")), DBNullToStr(fila.Item("DEN_ERP")), DBNullToStr(fila.Item("NIF")), DBNullToStr(fila.Item("CAMPO1")), DBNullToStr(fila.Item("CAMPO2")), DBNullToStr(fila.Item("CAMPO3")), DBNullToStr(fila.Item("CAMPO4")), DBNullToDec(fila.Item("BAJALOG")), DBNullToDec(fila.Item("ULTIMO")))
            Next
            CargarProveedoresERP = oProvesERP
            rs.Clear()
            Exit Function

        End If
        rs.Dispose()
    End Function
    ''' <summary>
    ''' Carga los archivos del pedido
    ''' </summary>
    ''' <param name="UsuCod"></param>
    ''' <param name="iId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Cargar_Archivos(ByVal iDesdeFavorito As Integer, ByVal UsuCod As String, ByVal iId As Integer) As CAdjuntos
        Authenticate()

        Dim adoRes As New DataSet
        Dim oAdjuntos As CAdjuntos

        oAdjuntos = New CAdjuntos(mDBServer, mIsAuthenticated)


        adoRes = mDBServer.Cargar_Adjuntos(iDesdeFavorito, iId)
        'rellenar info con consulta

        For Each fila As DataRow In adoRes.Tables(0).Rows
            If iDesdeFavorito = 0 Then
                oAdjuntos.Add(fila.Item("ID"), fila.Item("FECACT"), fila.Item("NOM"), fila.Item("TAMANO"), fila.Item("COM"), Nothing, Nothing, "", Nothing, TiposDeDatos.Adjunto.Tipo.Cesta_Cabecera)
            Else
                oAdjuntos.Add(fila.Item("ID"), fila.Item("FECACT"), fila.Item("NOM"), fila.Item("TAMANO"), fila.Item("COM"), Nothing, Nothing, "", Nothing, TiposDeDatos.Adjunto.Tipo.Favorito_Orden_Entrega)
            End If
        Next
        adoRes.Clear()
        adoRes = Nothing

        Return (oAdjuntos)
    End Function


    ''' <summary>
    ''' Funcion que carga los documentos adjuntos de las lineas
    ''' </summary>
    ''' <param name="UsuCod"></param>
    ''' <param name="iId"></param>
    ''' <param name="iIdLinea"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Cargar_ArchivosLinea(ByVal iDesdeFavorito As Integer, ByVal UsuCod As String, ByVal iId As Integer, ByVal iIdLinea As Integer) As CAdjuntos
        Authenticate()

        Dim adoRes As New DataSet
        Dim oAdjuntos As CAdjuntos

        oAdjuntos = New CAdjuntos(mDBServer, mIsAuthenticated)


        adoRes = mDBServer.Cargar_AdjuntosLinea(iDesdeFavorito, iIdLinea)
        'rellenar info con consulta

        For Each fila As DataRow In adoRes.Tables(0).Rows
            If iDesdeFavorito = 0 Then
                oAdjuntos.Add(fila.Item("ID"), fila.Item("FECACT"), fila.Item("NOM"), fila.Item("TAMANO"), fila.Item("COM"), Nothing, Nothing, "", Nothing, TiposDeDatos.Adjunto.Tipo.Cesta)
            Else
                oAdjuntos.Add(fila.Item("ID"), fila.Item("FECACT"), fila.Item("NOM"), fila.Item("TAMANO"), fila.Item("COM"), Nothing, Nothing, "", Nothing, TiposDeDatos.Adjunto.Tipo.Favorito_Linea_Pedido)
            End If
        Next
        adoRes.Clear()
        adoRes = Nothing

        Return (oAdjuntos)
    End Function
    ''' <summary>
    ''' Función que devuelve los atributos del pedido
    ''' </summary>
    ''' <param name="iCodPedido"></param>
    ''' <param name="iIdTipoPedido"></param>
    ''' <param name="bHayIntegracion"></param>
    ''' <param name="idEmpresa"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Cargar_Atributos(ByVal iDesdeFavorito As Integer, ByVal iCodPedido As Integer, ByVal iIdTipoPedido As Integer, Optional ByVal bHayIntegracion As Boolean = False,
                                     Optional ByVal idEmpresa As Integer = 0, Optional ByVal EsPedidoAbierto As Boolean = False, Optional ByVal iCodOrdenEmitir As Long = 0, Optional ByVal Usuario As String = "") As CAtributosPedido
        Authenticate()

        Dim adoRes As New DataSet
        Dim oAtributos As CAtributosPedido
        oAtributos = New CAtributosPedido(mDBServer, mIsAuthenticated)
        oAtributos = oAtributos.CargarAtributosPedido(iDesdeFavorito, iCodPedido, iIdTipoPedido, bHayIntegracion, idEmpresa, EsPedidoAbierto, iCodOrdenEmitir, Usuario)

        Return oAtributos
    End Function
    ''' <summary>
    ''' Función que devuelve los atributos del pedido
    ''' </summary>
    ''' <param name="iCodPedido"></param>
    ''' <param name="iIdTipoPedido"></param>
    ''' <param name="bHayIntegracion"></param>
    ''' <param name="idEmpresa"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Cargar_Atributos_Integracion(ByVal iCodPedido As Integer, ByVal iIdTipoPedido As Integer, ByVal bHayIntegracion As Boolean, ByVal idEmpresa As Integer _
                                                 , ByVal Usuario As String) As CAtributosPedido
        Authenticate()

        Dim adoRes As New DataSet
        Dim oAtributos As CAtributosPedido
        oAtributos = New CAtributosPedido(mDBServer, mIsAuthenticated)
        oAtributos = oAtributos.CargarAtributosIntegracionPedido(iCodPedido, iIdTipoPedido, bHayIntegracion, idEmpresa, Usuario)

        Return oAtributos
    End Function
    ''' <summary>
    ''' Función que valida los tipos de pedido en funcion de las lineas
    ''' </summary>
    ''' <param name="iCodPedido"></param>
    ''' <param name="iIdTipoPedido"></param>
    ''' <param name="iEmp"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Comprobar_TipoPedido(ByVal iCodPedido As Integer, ByVal iIdTipoPedido As Integer, ByVal iEmp As Integer) As Object
        Authenticate()

        Dim oAtributos As CAtributosPedido
        oAtributos = New CAtributosPedido(mDBServer, mIsAuthenticated)
        Comprobar_TipoPedido = oAtributos.Comprobar_TipoPedido(iCodPedido, iIdTipoPedido, iEmp)

        Return Comprobar_TipoPedido
    End Function
    ''' <summary>
    ''' Función que devuelve los costes del pedido
    ''' </summary>
    ''' <param name="iCodPedido"></param>
    ''' <param name="iIdTipoPedido"></param>
    ''' <param name="sIdioma"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Cargar_Costes(ByVal iDesdeFavorito As Integer, ByVal iCodPedido As Integer, ByVal iIdTipoPedido As Integer,
                                  Optional ByVal sIdioma As String = "SPA", Optional ByVal EsPedidoAbierto As Boolean = False) As CAtributosPedido
        Authenticate()

        Dim adoRes As New DataSet
        Dim oAtributos As CAtributosPedido
        oAtributos = New CAtributosPedido(mDBServer, mIsAuthenticated)
        oAtributos = oAtributos.CargarAtributosCoste(iDesdeFavorito, iCodPedido, iIdTipoPedido, sIdioma, EsPedidoAbierto)

        Return oAtributos
    End Function
    ''' <summary>
    ''' Función que devuelve los costes de la linea
    ''' </summary>
    ''' <returns>Un dataset con los datos de todos los materiales jerarquerizados en arbol</returns>
    ''' <remarks>
    ''' Llamada desde: La página Report_XML
    ''' Tiempo máximo: 1 seg</remarks>
    Public Function Cargar_Costes_Linea(ByVal iDesdeFavorito As Integer, ByVal iCodLinea As Integer, ByVal iIdTipoPedido As Integer,
                                        Optional ByVal sIdioma As String = "SPA", Optional ByVal EsPedidoAbierto As Boolean = False) As CAtributosPedido
        Authenticate()

        Dim adoRes As New DataSet
        Dim oAtributos As CAtributosPedido
        oAtributos = New CAtributosPedido(mDBServer, mIsAuthenticated)
        oAtributos = oAtributos.CargarAtributosCosteLinea(iDesdeFavorito, iCodLinea, iIdTipoPedido, sIdioma, EsPedidoAbierto)

        Return oAtributos
    End Function
    ''' <summary>
    ''' Función que devuelve los descuentos del pedido
    ''' </summary>
    ''' <returns>Un dataset con los datos de todos los materiales jerarquerizados en arbol</returns>
    ''' <remarks>
    ''' Llamada desde: La página Report_XML
    ''' Tiempo máximo: 1 seg</remarks>
    Public Function Cargar_Descuentos(ByVal iCodPedido As Integer, ByVal iIdTipoPedido As Integer, Optional ByVal EsPedidoAbierto As Boolean = False) As CAtributosPedido
        Authenticate()

        Dim adoRes As New DataSet
        Dim oAtributos As CAtributosPedido
        oAtributos = New CAtributosPedido(mDBServer, mIsAuthenticated)
        oAtributos = oAtributos.CargarAtributosDescuento(iCodPedido, iIdTipoPedido, EsPedidoAbierto)

        Return oAtributos
    End Function
    ''' <summary>
    ''' Función que devuelve los atributos de la linea
    ''' </summary>
    ''' <returns>Un dataset con los datos de todos los materiales jerarquerizados en arbol</returns>
    ''' <remarks>
    ''' Llamada desde: La página Report_XML
    ''' Tiempo máximo: 1 seg</remarks>
    Public Function Cargar_Atributos_Linea(ByVal iDesdeFavorito As Integer, ByVal iCodLinea As Integer, ByVal iIdTipoPedido As Integer, ByVal bHayIntegracion As Boolean,
                                            ByVal idEmpresa As Integer, ByVal EsPedidoAbierto As Boolean, ByVal iCodLineaEmitir As Long, ByVal Usuario As String) As CAtributosPedido
        Authenticate()

        Dim adoRes As New DataSet
        Dim oAtributos As CAtributosPedido
        oAtributos = New CAtributosPedido(mDBServer, mIsAuthenticated)
        oAtributos = oAtributos.CargarAtributosLineaPedido(iDesdeFavorito, iIdTipoPedido, iCodLinea, bHayIntegracion, idEmpresa, EsPedidoAbierto, iCodLineaEmitir, Usuario)

        Return oAtributos
    End Function
    ''' <summary>
    ''' Función que devuelve los descuentos de la linea
    ''' </summary>
    ''' <returns>Un dataset con los datos de todos los materiales jerarquerizados en arbol</returns>
    ''' <remarks>
    ''' Llamada desde: La página Report_XML
    ''' Tiempo máximo: 1 seg</remarks>
    Public Function Cargar_Descuentos_Linea(ByVal iDesdeFavorito As Integer, ByVal iCodLinea As Integer, ByVal iIdTipoPedido As Integer, ByVal EsPedidoAbierto As Boolean) As CAtributosPedido
        Authenticate()

        Dim adoRes As New DataSet
        Dim oAtributos As CAtributosPedido
        oAtributos = New CAtributosPedido(mDBServer, mIsAuthenticated)
        oAtributos = oAtributos.CargarAtributosDescuentoLinea(iDesdeFavorito, iCodLinea, iIdTipoPedido, EsPedidoAbierto)

        Return oAtributos
    End Function
    ''' <summary>
    ''' Valida el receptor en funcion a la orden
    ''' </summary>
    ''' <param name="idOrden"></param>
    ''' <param name="sReceptor"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ValidarReceptor(ByVal idOrden As Integer, ByVal sReceptor As String) As String
        Dim dsLineas As DataSet
        Dim sLineasValidas As String = String.Empty
        Authenticate()

        'Vaciamos costes/descuentos de la cabecera
        dsLineas = mDBServer.ValidarReceptor(idOrden, sReceptor)
        If Not IsNothing(dsLineas) Then
            If dsLineas.Tables.Count <> 0 Then
                For Each oRow As DataRow In dsLineas.Tables(0).Rows
                    If sLineasValidas = String.Empty Then
                        sLineasValidas = DBNullToStr(oRow("ID"))
                    Else
                        sLineasValidas = sLineasValidas + "-" + DBNullToStr(oRow("ID"))
                    End If
                Next
            End If
        End If
        Return sLineasValidas
    End Function
    ''' <summary>
    ''' Función que valida la empresa en funcion de las lineas
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>
    ''' Tiempo máximo: 1 seg</remarks>
    Public Function Comprobar_Empresa(ByVal idLinea As Integer, ByVal iEmp As Integer, ByVal EsPedidoAbierto As Boolean, Optional ByVal sOrgCompras As String = Nothing, Optional ByVal modFav As Boolean = False) As Object
        Dim dsEmpresa As DataSet
        Dim bComprobado As Boolean = False
        Dim bEsDeAdj As Boolean

        Authenticate()

        'Primero se comprueba la empresa en el caso de que la línea de catálogo venga de adjudicación
        bEsDeAdj = True
        dsEmpresa = mDBServer.Comprobar_Empresa_Adj(idLinea, iEmp, EsPedidoAbierto, sOrgCompras, modFav)
        If Not IsNothing(dsEmpresa) Then
            If dsEmpresa.Tables.Count <> 0 Then
                If dsEmpresa.Tables(0).Rows.Count > 0 Then
                    'Si devuelve un datatable vacío es que ha encontrado el ítem pero no se corresponde la empresa con la distribución
                    'Si devuelve una línea con todo valores NULL es que no ha encontrado ítem
                    If dsEmpresa.Tables(0).Rows(0)("IDLINEA").Equals(DBNull.Value) Then
                        bEsDeAdj = False
                    Else
                        For Each oRow As DataRow In dsEmpresa.Tables(0).Rows
                            If Not IsDBNull(oRow("IDLINEA")) Then
                                If iEmp = DBNullToInteger(oRow("EMPRESA")) Then
                                    If sOrgCompras Is Nothing Or (Not sOrgCompras Is Nothing AndAlso sOrgCompras = DBNullToStr(oRow("ORGCOMPRAS"))) Then
                                        bComprobado = True
                                        Exit For
                                    End If
                                End If
                            End If
                        Next
                    End If
                End If
            End If
        End If

        If Not bEsDeAdj And Not bComprobado Then
            'Primero se comprueba la empresa en el caso de que la línea de catálogo venga del maestro de artículos
            dsEmpresa = mDBServer.Comprobar_Empresa(idLinea, iEmp, EsPedidoAbierto, sOrgCompras, modFav)
            If Not IsNothing(dsEmpresa) Then
                If dsEmpresa.Tables.Count <> 0 Then
                    If dsEmpresa.Tables(0).Rows.Count > 0 Then
                        For Each oRow As DataRow In dsEmpresa.Tables(0).Rows
                            If iEmp = DBNullToInteger(oRow("EMPRESA")) Then
                                If sOrgCompras Is Nothing Or (Not sOrgCompras Is Nothing AndAlso sOrgCompras = DBNullToStr(oRow("ORGCOMPRAS"))) Then
                                    bComprobado = True
                                    Exit For
                                End If
                            End If
                        Next
                    End If
                End If
            End If
        End If

        If bComprobado Then
            Return True
        Else
            Return idLinea
        End If
    End Function
    ''' <summary>
    ''' Función que valida las cantidades de una linea
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>
    ''' Tiempo máximo: 1 seg</remarks>
    Public Function ComprobarImporteBruto(ByVal idLinea As Integer, ByVal sUPed As String, ByVal dImporteBruto As Double, ByVal oLineas As List(Of CLineaPedido)) As Integer
        '0 -- No hay error
        '1 -- Cantidad no supera la minima establecida
        '2 -- Cantidad supera la maxima extablecida
        '3 -- Cantidad supera maxima total a pedir por linea de catalogo
        '4 -- Supera la cantidad maxima a pedir por linea de catalogo en unidad de pedido

        Authenticate()
        Dim dsCantidades As DataSet
        Dim dCantidadTotal, dCantidadPedida As Double
        dsCantidades = mDBServer.ComprobarCantidades(idLinea, sUPed)
        If Not IsNothing(dsCantidades) Then
            If dsCantidades.Tables.Count <> 0 Then
                For Each oRow As DataRow In dsCantidades.Tables(0).Rows

                    If Not IsDBNull(oRow("CANT_MIN_COM")) Then
                        If dImporteBruto < oRow("CANT_MIN_COM") Then
                            Return 1
                        End If
                    End If
                    If Not IsDBNull(oRow("CANT_MAX_COM")) Then
                        If dImporteBruto > DBNullToDbl(oRow("CANT_MAX_COM")) Then
                            Return 2
                        End If
                    End If
                    If Not IsDBNull(oRow("CANT_MAX_TOTAL_COM")) Then
                        dCantidadPedida = dImporteBruto
                        For Each oLinea As CLineaPedido In oLineas
                            If oLinea.LineaCatalogo = oRow("LINEA") Then
                                If oLinea.ID <> idLinea Then
                                    dCantidadPedida = dCantidadPedida + oLinea.CantPed
                                End If
                            End If
                        Next
                        For Each oRowCantidades As DataRow In dsCantidades.Tables(1).Rows
                            dCantidadTotal = DBNullToDbl(oRowCantidades("CANTPEDIDA")) + dCantidadPedida
                            If DBNullToDbl(oRow("CANT_MAX_TOTAL_COM")) < dCantidadTotal Then
                                Return 4
                            End If
                        Next
                    End If
                Next
            End If
        End If

        Return 0
    End Function
    ''' <summary>
    ''' Carga solo los atributos de integracion de la linea
    ''' </summary>
    ''' <param name="iDesdeFavorito"></param>
    ''' <param name="iIdTipoPedido"></param>
    ''' <param name="iCodLinea"></param>
    ''' <param name="bHayIntegracion"></param>
    ''' <param name="iEmpresa"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Cargar_Atributos_Integracion_Lineas(ByVal iDesdeFavorito As Integer, ByVal iIdTipoPedido As Integer, ByVal iCodLinea As Integer, ByVal bHayIntegracion As Boolean, ByVal iEmpresa As Integer, ByVal Usuario As String) As CAtributosPedido
        Authenticate()

        Dim adoRes As New DataSet
        Dim oAtributos As CAtributosPedido
        oAtributos = New CAtributosPedido(mDBServer, mIsAuthenticated)
        oAtributos = oAtributos.CargarAtributosIntegracionLineaPedido(iDesdeFavorito, iIdTipoPedido, iCodLinea, bHayIntegracion, iEmpresa, Usuario)

        Return oAtributos
    End Function
    ''' <summary>
    ''' Carga los atributos de la linea
    ''' </summary>
    ''' <param name="iDesdeFavorito"></param>
    ''' <param name="iIdTipoPedido"></param>
    ''' <param name="iCodLinea"></param>
    ''' <param name="bHayIntegracion"></param>
    ''' <param name="iEmpresa"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Cargar_Atributos_Lineas(ByVal iDesdeFavorito As Integer, ByVal iIdTipoPedido As Integer, ByVal iCodLinea As Integer, ByVal bHayIntegracion As Boolean,
                                            ByVal iEmpresa As Integer, ByVal EsPedidoAbierto As Boolean, ByVal iCodLineaEmitir As Long, ByVal Usuario As String) As CAtributosPedido
        Authenticate()

        Dim adoRes As New DataSet
        Dim oAtributos As CAtributosPedido
        oAtributos = New CAtributosPedido(mDBServer, mIsAuthenticated)
        oAtributos = oAtributos.CargarAtributosLineaPedido(iDesdeFavorito, iIdTipoPedido, iCodLinea, bHayIntegracion, iEmpresa, EsPedidoAbierto, iCodLineaEmitir, Usuario)

        Return oAtributos
    End Function
    ''' <summary>
    ''' Comprueba la integracion de la empresa
    ''' </summary>
    ''' <param name="idEmpresa"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Comprobar_Integracion(ByVal idEmpresa As Integer) As Boolean

        Authenticate()
        Dim dsIntegracion As DataSet
        dsIntegracion = mDBServer.Comprobar_Integracion(idEmpresa)
        If Not IsNothing(dsIntegracion) Then
            If dsIntegracion.Tables.Count <> 0 Then
                If dsIntegracion.Tables(0).Rows.Count <> 0 Then
                    Return True
                End If
            End If
        End If

        Return False
    End Function
    ''' <summary>
    ''' Comprueba las cantidades de la linea en funcion a los limites establecidos
    ''' </summary>
    ''' <param name="idLinea"></param>
    ''' <param name="sUPed"></param>
    ''' <param name="dCant"></param>
    ''' <param name="oLineas"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ComprobarCantidades(ByVal idLinea As Integer, ByVal sUPed As String, ByVal dCant As Double, ByVal oLineas As List(Of CLineaPedido)) As Integer
        '0 -- No hay error
        '1 -- Cantidad no supera la minima establecida
        '2 -- Cantidad supera la maxima extablecida
        '3 -- Cantidad supera maxima total a pedir por linea de catalogo
        '4 -- Supera la cantidad maxima a pedir por linea de catalogo en unidad de pedido

        Authenticate()
        Dim dsCantidades As DataSet
        Dim dCantidadTotal, dCantidadPedida As Double
        dsCantidades = mDBServer.ComprobarCantidades(idLinea, sUPed)
        If Not IsNothing(dsCantidades) Then
            If dsCantidades.Tables.Count <> 0 Then
                For Each oRow As DataRow In dsCantidades.Tables(0).Rows
                    If Not IsDBNull(oRow("CANT_MIN_COM")) Then
                        If dCant < oRow("CANT_MIN_COM") Then
                            Return 1
                        End If
                    End If
                    If Not IsDBNull(oRow("CANT_MAX_COM")) Then
                        If dCant > DBNullToDbl(oRow("CANT_MAX_COM")) Then
                            Return 2
                        End If
                    End If
                    If Not IsDBNull(oRow("CANT_MAX_TOTAL_COM")) Then
                        dCantidadPedida = dCant
                        For Each oLinea As CLineaPedido In oLineas
                            If oLinea.LineaCatalogo = oRow("LINEA") Then
                                If oLinea.UP = sUPed And oLinea.ID <> idLinea Then
                                    dCantidadPedida = dCantidadPedida + oLinea.CantPed
                                End If
                            End If
                        Next
                        If dsCantidades.Tables(1).Rows.Count > 0 Then
                            For Each oRowCantidades As DataRow In dsCantidades.Tables(1).Rows
                                If oRowCantidades("UP") = sUPed Then
                                    dCantidadTotal = DBNullToDbl(oRowCantidades("CANTPEDIDA")) + dCantidadPedida
                                    If DBNullToDbl(oRow("CANT_MAX_TOTAL_COM")) < dCantidadTotal Then
                                        Return 4
                                    End If
                                End If
                            Next
                        Else
                            If DBNullToDbl(oRow("CANT_MAX_TOTAL_COM")) < dCantidadPedida Then
                                Return 4
                            End If
                        End If
                    End If
                    dCantidadTotal = 0
                    dCantidadPedida = 0
                    If Not IsDBNull(oRow("cant_max_total_uc")) Then
                        For Each oLinea As CLineaPedido In oLineas
                            If oLinea.LineaCatalogo = oRow("LINEA") Then
                                If oLinea.UP = oRow("UC") Then
                                    If oLinea.ID = idLinea Then
                                        dCantidadPedida = dCantidadPedida + dCant
                                    Else
                                        dCantidadPedida = dCantidadPedida + oLinea.CantPed
                                    End If
                                Else
                                    Dim dCantidaCalculada As Double
                                    If oLinea.ID = idLinea Then
                                        If oLinea.FC <> 0 Then
                                            dCantidaCalculada = dCant * oLinea.FC
                                        Else
                                            dCantidaCalculada = dCant
                                        End If
                                    Else
                                        If oLinea.FC <> 0 Then
                                            dCantidaCalculada = oLinea.CantPed * oLinea.FC
                                        Else
                                            dCantidaCalculada = oLinea.CantPed
                                        End If
                                    End If
                                    dCantidadPedida = dCantidadPedida + dCantidaCalculada
                                End If
                            End If
                        Next
                        dCantidadTotal = dCantidadPedida
                        For Each oRowCantidades As DataRow In dsCantidades.Tables(1).Rows
                            If oRowCantidades("UP") = oRow("UC") Then
                                dCantidadTotal = dCantidadTotal + DBNullToDbl(oRowCantidades("CANTPEDIDA"))
                            Else
                                dCantidadTotal = dCantidadTotal + DBNullToDbl(oRowCantidades("CANTPEDIDA")) * DBNullToDbl(oRowCantidades("FC"))
                            End If
                        Next
                        If DBNullToDbl(oRow("CANT_MAX_TOTAL_UC")) < dCantidadTotal Then
                            Return 3
                        End If
                    End If
                Next
            End If
        End If
        Return 0
    End Function

    ''' <summary>
    ''' Updatea las lineas de pedido grabandoles el id de la instancia de la solicitud de pedido(tipo 10) de aprobacion
    ''' </summary>
    ''' <param name="drSeguridad">fila con el id de la categoria y el nivel que tiene la seguridad</param>
    ''' <param name="idPedido">id del pedido</param>
    ''' <param name="Instancia">id de la instancia de la solicitud de pedido</param>
    ''' <remarks></remarks>
    Public Sub ActualizarLineasPedido_InstanciaSolicitudPedido(ByVal drSeguridad As DataRow, ByVal idPedido As Long, ByVal Instancia As Long, Optional ByVal EsPedidoAbierto As Boolean = False)
        Authenticate()
        mDBServer.ActualizarLineasPedido_InstanciaSolicitudPedido(drSeguridad, idPedido, Instancia, EsPedidoAbierto)
    End Sub
    ''' <summary>
    ''' Devuelve el bloque de destino, el de origen y la accion para pasar desde el aprovisionador del pedido al aprobador
    ''' </summary>
    ''' <param name="idSolicitud">id de la solicitud</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ObtenerBloque_Accion(ByVal idSolicitud As Long) As DataSet
        Authenticate()

        Dim ds As DataSet
        ds = mDBServer.ObtenerBloque_Accion(idSolicitud)
        Return ds
    End Function
    ''' <summary>
    ''' Borra una linea del pedido y el pedido sino hay lineas
    ''' </summary>
    ''' <remarks>Llamada desde emisionpedidobis.aspx </remarks>
    Public Sub BorrarLinea(ByVal iDesdeFavorito As Integer, ByVal idLinea As Integer, ByVal idCabecera As Integer, ByVal UsuCod As String)
        Authenticate()
        DBServer.Ordenes_EliminarDeCestaEP(iDesdeFavorito, idLinea, idCabecera, UsuCod)
    End Sub

    ''' <summary>
    ''' Guarda el nuevo pedido favorito
    ''' </summary>
    ''' <param name="cEmisionPedido"></param>
    ''' <param name="sDescFavorito"></param>
    ''' <param name="dImporte"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GuardarNuevoFavorito(ByVal cEmisionPedido As CEmisionPedido, ByVal sDescFavorito As String, ByVal dImporte As Decimal) As Integer
        Authenticate()

        Dim oDatosGenerales As CDatosGenerales
        Dim iCodNuevo As Integer = 0
        oDatosGenerales = cEmisionPedido.DatosGenerales
        dImporte = dImporte * oDatosGenerales.Cambio
        Dim dsFavorito As New DataSet
        dsFavorito = mDBServer.Orden_GuardarNuevoFavorito(oDatosGenerales.Id, dImporte, sDescFavorito)
        If Not IsNothing(dsFavorito) Then
            If dsFavorito.Tables.Count <> 0 Then
                For Each oRow As DataRow In dsFavorito.Tables(0).Rows
                    iCodNuevo = oRow.Item(0)
                Next
            End If
        End If
        Return iCodNuevo

    End Function
    ''' <summary>
    ''' Actualizamos el pedido
    ''' </summary>
    ''' <param name="UsuCod"></param>
    ''' <param name="Idioma"></param>
    ''' <param name="Persona"></param>
    ''' <param name="Moneda"></param>
    ''' <param name="cEmisionPedido"></param>
    ''' <param name="bHayIntegracion"></param>
    ''' <param name="idEmpresa"></param>
    ''' <remarks></remarks>
    Public Sub ActualizarPedido(ByVal iDesdeFavorito As Integer, ByVal UsuCod As String, ByVal Idioma As String, ByVal Persona As String, ByVal Moneda As String,
                                      ByVal cEmisionPedido As CEmisionPedido, ByVal sDescFavorito As String, Optional ByVal bHayIntegracion As Boolean = False,
                                      Optional ByVal idEmpresa As Integer = 0, Optional ByVal ModoEdicion As Boolean = False, Optional ByVal EsPedidoAbierto As Boolean = False)
        Authenticate()

        'Datos Generales
        Dim oDatosGenerales As CDatosGenerales
        oDatosGenerales = cEmisionPedido.DatosGenerales

        Dim iDirEnvio As Integer
        If (oDatosGenerales.DirEnvio <> 0) Then
            iDirEnvio = Int32.Parse(oDatosGenerales.DirEnvio)
        Else
            iDirEnvio = 0
        End If
        Dim iTipo As Integer
        If Not IsNothing(oDatosGenerales.Tipo) Then
            iTipo = Int32.Parse(oDatosGenerales.Tipo.Id)
        Else
            iTipo = 0
        End If

        mDBServer.Orden_ActualizarCestaCabecera(iDesdeFavorito, oDatosGenerales.Id, IIf(iDesdeFavorito = 1, UsuCod, Persona), oDatosGenerales.CodProve, Moneda.ToString(), oDatosGenerales.Referencia, oDatosGenerales.Emp, oDatosGenerales.CodErp,
                                       oDatosGenerales.Receptor, iTipo, oDatosGenerales.Obs, iDirEnvio, sDescFavorito, oDatosGenerales.AcepProve, ModoEdicion, oDatosGenerales.OrgCompras)
        'Adjuntos
        For Each oadj As FSNServer.Adjunto In cEmisionPedido.Adjuntos
            Dim adjunto As New Adjunto(mDBServer, mIsAuthenticated)
            adjunto.DirectorioGuardar = oadj.DirectorioGuardar
            adjunto.Id = oadj.Id
            adjunto.IdEsp = oadj.IdEsp
            adjunto.Nombre = oadj.Nombre
            adjunto.Operacion = oadj.Operacion
            adjunto.FileName = oadj.FileName
            adjunto.tipo = oadj.tipo
            adjunto.Cod = oadj.Cod
            adjunto.Comentario = oadj.Comentario
            adjunto.dataSize = oadj.dataSize
            Select Case adjunto.Operacion
                Case "D"
                    If iDesdeFavorito = 0 Then
                        adjunto.EliminarAdjunto(TiposDeDatos.Adjunto.Tipo.Cesta_Cabecera)
                    Else
                        adjunto.EliminarAdjunto(TiposDeDatos.Adjunto.Tipo.Favorito_Orden_Entrega)
                    End If
                Case "I"
                    adjunto.IdEsp = cEmisionPedido.DatosGenerales.Id
                    If iDesdeFavorito = 0 Then
                        If oadj.Id Is Nothing OrElse oadj.Id = -1 Then
                            adjunto.GrabarAdjunto(oadj.FileName, oadj.DirectorioGuardar, TiposDeDatos.Adjunto.Tipo.Cesta_Cabecera, adjunto.IdEsp, adjunto.Comentario, adjunto.dataSize)
                        Else
                            adjunto.CopiarAdjunto(oadj, TiposDeDatos.Adjunto.Tipo.Cesta_Cabecera, oadj.IdEsp)
                        End If
                        adjunto.Operacion = ""
                    Else
                        If oadj.Id Is Nothing OrElse oadj.Id = -1 Then
                            adjunto.GrabarAdjunto(oadj.FileName, oadj.DirectorioGuardar, TiposDeDatos.Adjunto.Tipo.Favorito_Orden_Entrega, adjunto.IdEsp, adjunto.Comentario, adjunto.dataSize)
                        Else
                            adjunto.CopiarAdjunto(oadj, TiposDeDatos.Adjunto.Tipo.Favorito_Orden_Entrega, oadj.IdEsp)
                        End If
                        adjunto.Operacion = ""
                    End If
            End Select
        Next

        Dim oAtributos As CAtributosPedido = Nothing
        If Not cEmisionPedido.OtrosDatos Is Nothing Then
            If cEmisionPedido.OtrosDatos.Count = 0 Then
                oAtributos = Cargar_Atributos(iDesdeFavorito, cEmisionPedido.DatosGenerales.Id, iTipo, bHayIntegracion, idEmpresa, EsPedidoAbierto, 0, UsuCod)
            End If
        End If
        'Vaciamos costes/descuentos de la cabecera
        mDBServer.VaciarCostesDescuentosCabecera(iDesdeFavorito, cEmisionPedido.DatosGenerales.Id)

        'Costes 
        If Not cEmisionPedido.Costes Is Nothing Then
            For Each oAtributo As CAtributoPedido In cEmisionPedido.Costes
                If oAtributo.Seleccionado Or oAtributo.TipoCosteDescuento = 0 Then
                    If oAtributo.TipoCosteDescuento = 0 Then
                        mDBServer.ActualizarCostesDescuentosCabecera(iDesdeFavorito, oAtributo.Id, cEmisionPedido.DatosGenerales.Id, 2, oAtributo.Interno, True, oAtributo.Operacion, oAtributo.Valor, oAtributo.Importe, oAtributo.MostrarEnRecep)
                    Else
                        mDBServer.ActualizarCostesDescuentosCabecera(iDesdeFavorito, oAtributo.Id, cEmisionPedido.DatosGenerales.Id, 2, oAtributo.Interno, oAtributo.Obligatorio, oAtributo.Operacion, oAtributo.Valor, oAtributo.Importe, oAtributo.MostrarEnRecep)
                    End If
                End If
            Next
        End If
        'Descuentos
        If Not cEmisionPedido.Descuentos Is Nothing Then
            For Each oAtributo As CAtributoPedido In cEmisionPedido.Descuentos
                If oAtributo.Seleccionado Or oAtributo.TipoCosteDescuento = 0 Then
                    If oAtributo.TipoCosteDescuento = 0 Then
                        mDBServer.ActualizarCostesDescuentosCabecera(iDesdeFavorito, oAtributo.Id, cEmisionPedido.DatosGenerales.Id, 3, oAtributo.Interno, True, oAtributo.Operacion, oAtributo.Valor, oAtributo.Importe, oAtributo.MostrarEnRecep)
                    Else
                        mDBServer.ActualizarCostesDescuentosCabecera(iDesdeFavorito, oAtributo.Id, cEmisionPedido.DatosGenerales.Id, 3, oAtributo.Interno, oAtributo.Obligatorio, oAtributo.Operacion, oAtributo.Valor, oAtributo.Importe, oAtributo.MostrarEnRecep)
                    End If

                End If
            Next
        End If
        'OtrosDatos
        If Not cEmisionPedido.OtrosDatos Is Nothing Then
            If cEmisionPedido.OtrosDatos.Count <> 0 Then
                For Each oAtributo As CAtributoPedido In cEmisionPedido.OtrosDatos
                    If Not IsNothing(oAtributo.Valor) Then
                        If oAtributo.Tipo = 3 Then
                            If oAtributo.Valor.ToString() = "" Then
                                mDBServer.ActualizarAtributosCabecera(iDesdeFavorito, oAtributo.Id, cEmisionPedido.DatosGenerales.Id, oAtributo.Tipo, 1, DBNull.Value, oAtributo.Interno, oAtributo.Obligatorio, UsuCod, oAtributo.MostrarEnRecep)
                            Else
                                mDBServer.ActualizarAtributosCabecera(iDesdeFavorito, oAtributo.Id, cEmisionPedido.DatosGenerales.Id, oAtributo.Tipo, 1, oAtributo.Valor, oAtributo.Interno, oAtributo.Obligatorio, UsuCod, oAtributo.MostrarEnRecep)
                            End If
                        Else
                            If oAtributo.ListaExterna Then
                                If oAtributo.Valor.ToString() = "" Then
                                    mDBServer.ActualizarAtributosCabecera(iDesdeFavorito, oAtributo.Id, cEmisionPedido.DatosGenerales.Id, oAtributo.Tipo, 1, DBNull.Value, oAtributo.Interno, oAtributo.Obligatorio, UsuCod, oAtributo.MostrarEnRecep)
                                Else
                                    mDBServer.ActualizarAtributosCabecera(iDesdeFavorito, oAtributo.Id, cEmisionPedido.DatosGenerales.Id, oAtributo.Tipo, 1, oAtributo.Valores(0).Valor, oAtributo.Interno, oAtributo.Obligatorio, UsuCod, oAtributo.MostrarEnRecep, oAtributo.Valor)
                                End If
                            Else
                                mDBServer.ActualizarAtributosCabecera(iDesdeFavorito, oAtributo.Id, cEmisionPedido.DatosGenerales.Id, oAtributo.Tipo, 1, oAtributo.Valor, oAtributo.Interno, oAtributo.Obligatorio, UsuCod, oAtributo.MostrarEnRecep)
                            End If
                        End If
                    ElseIf oAtributo.Obligatorio Or oAtributo.Intro = 0 Then
                        mDBServer.ActualizarAtributosCabecera(iDesdeFavorito, oAtributo.Id, cEmisionPedido.DatosGenerales.Id, oAtributo.Tipo, 1, DBNullToSomething, oAtributo.Interno, oAtributo.Obligatorio, UsuCod, oAtributo.MostrarEnRecep)
                    End If
                Next
            Else
                For Each oAtributo As CAtributoPedido In oAtributos
                    If Not IsNothing(oAtributo.Valor) Then
                        If oAtributo.Tipo = 3 Then
                            If oAtributo.Valor.ToString() = "" Then
                                mDBServer.ActualizarAtributosCabecera(iDesdeFavorito, oAtributo.Id, cEmisionPedido.DatosGenerales.Id, oAtributo.Tipo, 1, DBNullToSomething, oAtributo.Interno, oAtributo.Obligatorio, UsuCod, oAtributo.MostrarEnRecep)
                            Else
                                mDBServer.ActualizarAtributosCabecera(iDesdeFavorito, oAtributo.Id, cEmisionPedido.DatosGenerales.Id, oAtributo.Tipo, 1, oAtributo.Valor, oAtributo.Interno, oAtributo.Obligatorio, UsuCod, oAtributo.MostrarEnRecep)
                            End If
                        Else
                            mDBServer.ActualizarAtributosCabecera(iDesdeFavorito, oAtributo.Id, cEmisionPedido.DatosGenerales.Id, oAtributo.Tipo, 1, oAtributo.Valor, oAtributo.Interno, oAtributo.Obligatorio, UsuCod, oAtributo.MostrarEnRecep)
                        End If
                    ElseIf oAtributo.Obligatorio Or oAtributo.Intro = 0 Then
                        mDBServer.ActualizarAtributosCabecera(iDesdeFavorito, oAtributo.Id, cEmisionPedido.DatosGenerales.Id, oAtributo.Tipo, 1, DBNullToSomething, oAtributo.Interno, oAtributo.Obligatorio, UsuCod, oAtributo.MostrarEnRecep)

                    End If
                Next
            End If
        End If
    End Sub
    ''' <summary>
    ''' Comprueba el aprovisionador
    ''' </summary>
    ''' <param name="codUsu"></param>
    ''' <param name="idFav"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ComprobarAprovisonador(ByVal codUsu As String, ByVal idFav As Integer) As DataTable
        Authenticate()
        Dim oUserItemsNotIssuable As DataTable = mDBServer.ComprobarAprovisonador(codUsu, idFav)
        Return oUserItemsNotIssuable
    End Function
    ''' <summary>
    ''' Emite el pedido
    ''' </summary>
    ''' <param name="UsuCod"></param>
    ''' <param name="Idioma"></param>
    ''' <param name="Persona"></param>
    ''' <param name="Moneda"></param>
    ''' <param name="cEmisionPedido"></param>
    ''' <param name="PagenSMs"></param>
    ''' <param name="Acceso"></param>
    ''' <param name="oPargenSM"></param>
    ''' <param name="idPedido"></param>
    ''' <param name="dsSolicitudesDePedido"></param>
    ''' <param name="dImporte"></param>
    ''' <param name="ped_num"></param>
    ''' <param name="oe_num"></param>
    ''' <param name="EstadoOrden"></param>
    ''' <remarks></remarks>
    Public Sub EmitirPedido(ByVal UsuCod As String, ByVal Idioma As String, ByVal Persona As String, ByVal Moneda As String,
                                      ByVal cEmisionPedido As CEmisionPedido, ByVal PagenSMs As FSNServer.PargensSMs,
                                      ByVal Acceso As FSNLibrary.TiposDeDatos.ParametrosGenerales, ByVal oPargenSM As FSNServer.PargenSM,
                                      ByRef idPedido As Integer, ByRef dsSolicitudesDePedido As DataSet, ByVal dImporte As Decimal,
                                      ByRef ped_num As Long, ByRef oe_num As Long, ByRef EstadoOrden As Short, ByVal UsuMail As String,
                                      ByVal iNotificar As OrdenEntregaNotificacion, ByVal sCodProveERP As String, ByRef idOrden As Long,
                                      ByVal EsPedidoAbierto As Boolean)
        Authenticate()
        Dim iOrdenLinea As Integer
        Dim dsPedido As DataSet
        Dim iNumPedido As Integer
        dsPedido = mDBServer.InsertarPedido(UsuCod, EsPedidoAbierto)
        'CREAR PEDIDO
        If Not IsNothing(dsPedido) Then
            If dsPedido.Tables.Count <> 0 Then
                For Each oRow As DataRow In dsPedido.Tables(0).Rows
                    idPedido = oRow("ID")
                    iNumPedido = oRow("NUM")
                    ped_num = iNumPedido
                Next
            End If
        End If
        If idPedido <> 0 Then
            Dim _mipargenSMs As FSNServer.PargensSMs
            Dim _mipargenSM As FSNServer.PargenSM
            If Acceso.gbAccesoFSSM Then
                _mipargenSMs = PagenSMs
                If _mipargenSMs.Count > 0 Then
                    _mipargenSM = _mipargenSMs(0)
                Else
                    _mipargenSM = Nothing
                End If
            Else
                _mipargenSM = Nothing
            End If

            'CREAR CABECERA PEDIDO
            'Dim oOrden As COrden
            Dim bAutofactura As Boolean = False
            If Acceso.g_bAccesoFSFA Then
                Dim oProve As New CProveedor(mDBServer, mIsAuthenticated)
                oProve.Cod = cEmisionPedido.DatosGenerales.CodProve
                oProve.getAutofactura() 'Recuperamos el valor de autofactura del proveedor (que se pasa a la propiedad Autofactura)
                If oProve.Autofactura Then
                    bAutofactura = True
                End If
            End If

            'Antes de insertar las líneas del pedido, comprobamos si hay facturacion activada. 
            '  Si la hay:
            '   Añadimos en las líneas el valor IM_FACTRASRECEP = SELECT TOP 1 IM_FACTRASRECEP FROM PARGEN_INTERNO WITH(NOLOCK)
            '   Dado que es un valor fijo, lo recuperamos de base de datos fuera del bucle de órdenes para acceder a base de datos una sola vez
            Dim iIM_Factrasrecep As Integer = 0
            Dim dsFact As DataSet
            If Acceso.g_bAccesoFSFA Then
                dsFact = mDBServer.Recuperar_FacturacionActivada()
                If Not IsNothing(dsFact) Then
                    If dsFact.Tables.Count <> 0 Then
                        For Each oRow As DataRow In dsFact.Tables(0).Rows
                            iIM_Factrasrecep = oRow(0)
                        Next
                    End If
                End If
                Select Case iIM_Factrasrecep
                    Case 0, 2
                        iIM_Factrasrecep = 0
                    Case 1
                        iIM_Factrasrecep = 1
                End Select
            End If

            dImporte = dImporte / cEmisionPedido.DatosGenerales.Cambio

            iOrdenLinea = mDBServer.CrearOrdenEntregaLinea(cEmisionPedido.DatosGenerales.Id, cEmisionPedido.DatosGenerales.Cambio, UsuCod, cEmisionPedido.DatosGenerales.CodProve, Moneda, idPedido, cEmisionPedido.DatosGenerales.Referencia,
                                        cEmisionPedido.DatosGenerales.Obs, cEmisionPedido.DatosGenerales.Emp, cEmisionPedido.DatosGenerales.Receptor, cEmisionPedido.DatosGenerales.CodErp,
                                        cEmisionPedido.DatosGenerales.Tipo.Id, cEmisionPedido.DatosGenerales.DirEnvio, bAutofactura, iIM_Factrasrecep, Idioma, dImporte, oe_num, EsPedidoAbierto, cEmisionPedido.DatosGenerales.OrgCompras,
                                        cEmisionPedido.DatosGenerales.AcepProve)
            idOrden = iOrdenLinea
            If iOrdenLinea <> 0 Then
                If cEmisionPedido.Costes IsNot Nothing Then
                    For Each oCoste As CAtributoPedido In cEmisionPedido.Costes
                        For Each oImpuesto As CImpuestos In oCoste.Impuestos
                            If oCoste.Seleccionado Or oCoste.TipoCosteDescuento = 0 Then
                                mDBServer.GuardarImpuestosCabecera(iOrdenLinea, oCoste.Id, oImpuesto.Id, oImpuesto.Valor)
                            End If
                        Next
                    Next
                End If
                If cEmisionPedido.Lineas IsNot Nothing Then
                    For Each oLinea As CLineaPedido In cEmisionPedido.Lineas
                        For Each oImpuesto As CImpuestos In oLinea.Impuestos
                            mDBServer.GuardarImpuestosLinea(idPedido, oImpuesto.Id, oImpuesto.Valor, oLinea.ID)
                        Next
                        For Each oCoste As CAtributoPedido In oLinea.Costes
                            For Each oImpuestoCoste As CImpuestos In oCoste.Impuestos
                                If oCoste.Seleccionado Or oCoste.TipoCosteDescuento = 0 Then
                                    mDBServer.GuardarImpuestosCostesLinea(idPedido, oCoste.Id, oImpuestoCoste.Id, oImpuestoCoste.Valor, oLinea.ID)
                                End If
                            Next
                        Next
                    Next
                End If


                'Actualizar Activo en las lineas de pedido      
                For Each oLinea As CLineaPedido In cEmisionPedido.Lineas
                    mDBServer.ActualizarActivoLineaPedido(iOrdenLinea, oLinea.Activo)
                Next

                ''Insertar Imputaciones líneas y actualizar el nuevo comprometido de ese contrato(si las hay)
                ''If Not oPargenSM Is Nothing AndAlso Not oPargenSM.Id = Nothing Then
                Dim iAcumularIva As Integer = 0
                If Not oPargenSM Is Nothing Then
                    If oPargenSM.ImputacionPedido <> FSNServer.PargenSM.EnumTipoImputacion.NoSeImputa Then
                        If oPargenSM.ImpuestoModo = 1 Or oPargenSM.ImpuestoModo = 2 Then
                            iAcumularIva = 1
                        Else
                            iAcumularIva = 0
                        End If

                        For Each linea As CLineaPedido In cEmisionPedido.Lineas
                            If Not linea.Pres5_ImportesImputados_EP Is Nothing Then
                                For Each imp As FSNServer.Imputacion In linea.Pres5_ImportesImputados_EP
                                    If imp.CentroCoste IsNot Nothing AndAlso Not String.IsNullOrEmpty(imp.CentroCoste.UON1) _
                                        And imp.Partida IsNot Nothing AndAlso Not String.IsNullOrEmpty(imp.Partida.NIV0) Then
                                        mDBServer.TratarImputacionesLinea(iOrdenLinea, linea.ID, idPedido, imp.CentroCoste.UON1, imp.CentroCoste.UON2, imp.CentroCoste.UON3, imp.CentroCoste.UON4,
                                        imp.Partida.NIV0, imp.Partida.NIV1, imp.Partida.NIV2, imp.Partida.NIV3, imp.Partida.NIV4, iAcumularIva)
                                    End If
                                Next
                            End If
                        Next
                    End If
                End If

                For Each lin As CLineaPedido In cEmisionPedido.Lineas
                    mDBServer.Ordenes_EliminarDeCestaEP(0, lin.ID, cEmisionPedido.DatosGenerales.Id, UsuCod)
                Next
                If EsPedidoAbierto Then
                    dsSolicitudesDePedido = DevolverSolicitud_PedidoContraAbierto(idPedido, EstadoOrden)
                Else
                    dsSolicitudesDePedido = ValidacionesEmisionPedidoCatalogo(idPedido, UsuCod, cEmisionPedido.DatosGenerales.Tipo.Id, cEmisionPedido.DatosGenerales.Emp, , EstadoOrden, iNotificar)
                End If

                Dim errorNotificacion As DataRowCollection = Nothing
                Try
                    'Notificaciones en caso de superar el limite de adjudicacion
                    If Not EsPedidoAbierto Then NotificarOrdenesLimAdjSup(UsuMail, Idioma, idPedido)

                    NotificarAProveedores(UsuMail, Idioma, idPedido, iNotificar, sCodProveERP)
                    NotificarPasoProveedorQAaReal(cEmisionPedido.DatosGenerales.CodProve, Date.Today.Year, ped_num, oe_num)
                Catch ex As Exception
                    Dim dtError As New DataTable
                    dtError.TableName = "Errores"
                    dtError.Columns.Add("ErrorID", TiposDeDatos.TiposErrores.TENotificacion.GetType)
                    Dim drError As DataRow = dtError.NewRow
                    drError.Item("ErrorID") = TiposDeDatos.TiposErrores.TENotificacion
                    dtError.Rows.Add(drError)
                    errorNotificacion = dtError.Rows

                    DBServer.Errores_Create("FULLSTEP EP. Emitir. Notificar. PedidoID:" & CStr(idPedido), DBServer.UsuarioConectado, ex.GetType().FullName, ex.Message, ex.StackTrace, "", "")
                End Try
            End If
        End If
    End Sub

    ''' <summary>
    ''' Updata (o no segun ModificarPreciosUsu) los precios de catalog-lin con la adjudicacion. Devuelve las lineas q toca, para poder actualizar la cache de "detalle articulos"
    ''' </summary>
    ''' <param name="idOrden">Orden</param>
    ''' <param name="idInstancia">Instancia para cononcer el peticionario q es el del permiso "para modificar los precios en la emisión de pedidos"</param>
    ''' <returns></returns>
    Public Function TrasCrearOrdenEntregaLineaActualizarCatalogo(ByVal idOrden As Long, ByVal idInstancia As Long) As DataRowCollection
        Dim oInstancia As New FSNServer.Instancia(mDBServer, True)
        oInstancia.ID = idInstancia
        Dim sPeticionario As String = oInstancia.Get_Peticionario_Instancia()

        Dim FSNUser As New FSNServer.User(mDBServer, sPeticionario, True)

        FSNUser.LoadUserData(sPeticionario)

        Return mDBServer.TrasCrearOrdenEntregaLineaActualizarCatalogo(idOrden, 0, sPeticionario, FSNUser.ModificarPrecios)
    End Function

    ''' <summary>
    ''' Updata (o no segun ModificarPreciosUsu) los precios de catalog-lin con la adjudicacion. Devuelve las lineas q toca, para poder actualizar la cache de "detalle articulos"
    ''' </summary>
    ''' <param name="idOrden">Orden</param>
    ''' <param name="idPedido">Pedido</param>
    ''' <param name="msAprov">Aprobador</param>
    ''' <param name="ModificarPreciosUsu">Updatar si o no</param>
    ''' <returns></returns>
    Public Function TrasCrearOrdenEntregaLineaActualizarCatalogo(ByVal idOrden As Long, ByVal idPedido As Long, ByVal msAprov As String, ByVal ModificarPreciosUsu As Boolean) As DataRowCollection
        Return mDBServer.TrasCrearOrdenEntregaLineaActualizarCatalogo(idOrden, idPedido, msAprov, ModificarPreciosUsu)
    End Function

    ''' <summary>
    ''' Emite las notificaciones a los aprobadores en caso de que se haya superado el lÃ­mite de adjudicaciÃ³n
    ''' </summary>
    ''' <param name="addressFrom">Mail del remitente de las notificaciones</param>
    ''' <param name="Idioma">CÃ³digo de idioma</param>
    ''' <param name="idPedido">id del pedido a notificar</param>
    Public Sub NotificarOrdenesLimAdjSup(ByVal addressFrom As String, ByVal Idioma As String, ByVal idPedido As Long)
        Dim oNotificador As Notificar
        Dim ador As New DataSet

        ador = DBServer.Notificador_CargarAprobadoresTipo1(idPedido)
        If Not ador.Tables(0).Rows.Count = 0 Then
            For Each fila As DataRow In ador.Tables(0).Rows
                oNotificador = New Notificar(DBServer, mIsAuthenticated)
                oNotificador.gIdioma = Idioma

                oNotificador.NotificacionAprobadoresTipo1(addressFrom, fila, True)

                oNotificador.FuerzaFinalizeSmtpClient()
                oNotificador = Nothing
            Next
        End If

        ador.Clear()
        ador = New DataSet
        ador = DBServer.Notificador_CargarNotificadosTipo1(idPedido)
        If Not ador.Tables(0).Rows.Count = 0 Then
            For Each fila As DataRow In ador.Tables(0).Rows
                oNotificador = New Notificar(DBServer, mIsAuthenticated)
                oNotificador.gIdioma = Idioma

                oNotificador.NotificacionAprobadoresTipo1(addressFrom, fila, False)

                oNotificador.FuerzaFinalizeSmtpClient()
                oNotificador = Nothing
            Next
        End If

        ador.Clear()
    End Sub
    ''' <summary>
    ''' Procedimiento que notifica la Emision de una orden
    ''' </summary>
    ''' <param name="OrdenId">Identificador de la orden a notificar</param>
    ''' <param name="From">Email del remitente</param>
    ''' <remarks>Llamada desde:NotificarAProveedores; Tiempo mÃ¡ximo:0</remarks>
    Private Sub NotificarEmisionNotificados(ByVal OrdenId As Integer, ByVal From As String, ByVal IdiomaUser As String)
        Dim ador As New DataSet
        Dim oNotificador As Notificar

        ador = DBServer.Notificador_CargarNotificadosTipo2(0, False, OrdenId, 0)

        For Each fila As DataRow In ador.Tables(0).Rows
            oNotificador = New Notificar(DBServer, mIsAuthenticated)
            oNotificador.gIdioma = IdiomaUser

            oNotificador.NotificarEmisionNotificados(OrdenId, From, fila)

            oNotificador.FuerzaFinalizeSmtpClient()
            oNotificador = Nothing
        Next

        ador.Clear()
    End Sub

    ''' <summary>
    ''' Notificar una emisiÃ³n
    ''' </summary>
    ''' <param name="IdiomaUser">Idioma de la aplicaciÃ³n</param>
    ''' <remarks>Llamada desde: Emitir/EmitirDesdefavoritos; Tiempo maximo: 0,2</remarks>
    Public Sub NotificarAProveedores(ByVal addressFrom As String, ByVal IdiomaUser As String, ByVal idPedido As Long, ByVal iNotificar As OrdenEntregaNotificacion, ByVal sCodProveERP As String)
        Dim oNotificador As Notificar
        Dim sFSP As String = ""
        Dim ordenID As Integer
        Dim iIdPedido As Integer
        Dim ador As New DataSet

        ador = mDBServer.Pedido_NotificarAProveedores(idPedido)
        If Not ador.Tables(0).Rows.Count = 0 Then
            For Each fila As DataRow In ador.Tables(0).Rows
                oNotificador = New Notificar(DBServer, mIsAuthenticated)

                oNotificador.gIdioma = IdiomaUser
                ordenID = fila.Item("ID")
                iIdPedido = fila.Item("PEDIDO")

                Dim dr As DataRow = DBServer.Parametros_Portal()
                Dim iINSTWEB As Integer = dr.Item("INSTWEB")
                Dim sFSP_BD As String = IIf(IsDBNull(dr.Item("FSP_BD")), String.Empty, dr.Item("FSP_BD"))
                Dim sFSP_SRV As String = IIf(IsDBNull(dr.Item("FSP_SRV")), String.Empty, dr.Item("FSP_SRV"))
                Dim sCiaComp As String = IIf(IsDBNull(dr.Item("FSP_CIA")), String.Empty, dr.Item("FSP_CIA"))
                dr = Nothing

                If iINSTWEB = 1 Then 'Buysite
                    'Lanzamos el mensaje de forma asÃ­ncrona
                    If iNotificar = OrdenEntregaNotificacion.Notificado Then oNotificador.NotificarOrdenAProveedor(ordenID, sFSP, addressFrom)
                    DBServer.Orden_TratarComunicion(ordenID, iIdPedido, iNotificar)

                    oNotificador.FuerzaFinalizeSmtpClient()
                    oNotificador = Nothing
                    NotificarEmisionNotificados(ordenID, addressFrom, IdiomaUser)
                Else 'Portal
                    If iNotificar = OrdenEntregaNotificacion.Notificado Then
                        'Lanzamos el mensaje de forma asÃ­ncrona
                        If Not String.IsNullOrEmpty(sFSP_SRV) AndAlso Not String.IsNullOrEmpty(sFSP_BD) Then
                            sFSP = sFSP_SRV & "." & sFSP_BD & ".dbo."
                        End If

                        oNotificador.NotificarOrdenAProveedor(ordenID, sFSP, addressFrom)
                    End If
                    DBServer.Orden_TratarComunicion(ordenID, iIdPedido, iNotificar)

                    oNotificador.FuerzaFinalizeSmtpClient()
                    oNotificador = Nothing
                    NotificarEmisionNotificados(ordenID, addressFrom, IdiomaUser)
                End If
                oNotificador = New Notificar(DBServer, mIsAuthenticated)
                oNotificador.gIdioma = IdiomaUser
                oNotificador.NotificarEmisionAUsuario(fila.Item("Id"), addressFrom)

                oNotificador.FuerzaFinalizeSmtpClient()
                oNotificador = Nothing
            Next
        End If

        ador.Clear()
    End Sub
    ''' <summary>Envía las notificaciones de paso de un proveedor de QA a real</summary>
    ''' <param name="sProve">Cod. proveedor</param>
    ''' <remarks>Llamada desde: EmitirPedido</remarks>
    Private Sub NotificarPasoProveedorQAaReal(ByVal sProve As String, ByVal iOrdenAnyo As Integer, ByVal iNumPedido As Integer, ByVal iOrdenNumero As Integer)
        Dim oNotificador As New Notificar(DBServer, mIsAuthenticated)
        oNotificador.NotificarPasoProveedorQAaReal(sProve, iOrdenAnyo, iNumPedido, iOrdenNumero)
        oNotificador.FuerzaFinalizeSmtpClient()
    End Sub
    ''' <summary>
    ''' Actualiza las lineas del pedido
    ''' </summary>
    ''' <param name="iDesdeFavorito"></param>
    ''' <param name="UsuCod"></param>
    ''' <param name="Idioma"></param>
    ''' <param name="Persona"></param>
    ''' <param name="linea"></param>
    ''' <param name="oCentroSM"></param>
    ''' <param name="oPartidaPres5"></param>
    ''' <param name="iCestaCabecera"></param>
    ''' <param name="bHayIntegracion"></param>
    ''' <param name="idEmpresa"></param>
    ''' <remarks></remarks>
    Public Sub ActualizarLineaPedido(ByVal iDesdeFavorito As Integer, ByVal UsuCod As String, ByVal Idioma As String, ByVal Persona As String,
                                    ByVal linea As CLineaPedido, ByVal oCentroSM As List(Of UON), ByVal oPartidaPres5 As List(Of PartidaPRES5),
                                    ByVal iCestaCabecera As Integer, Optional ByVal bHayIntegracion As Boolean = False, Optional ByVal idEmpresa As Integer = 0)
        Authenticate()

        Dim centroSM, PartidaPRES5 As String
        Dim fechaEntrega As Date
        'Lineas
        PartidaPRES5 = String.Empty
        'Vaciamos costes/descuentos de la cabecera
        mDBServer.VaciarCostesDescuentosLinea(iDesdeFavorito, linea.ID)
        If linea.Est = 99 Then
            mDBServer.Ordenes_EliminarDeCestaEP(iDesdeFavorito, linea.ID, iCestaCabecera, IIf(iDesdeFavorito = 1, UsuCod, Persona))
        Else
            If oCentroSM IsNot Nothing Then
                If oCentroSM.Count <> 0 Then
                    If oCentroSM(0).CentroSM IsNot Nothing Then
                        centroSM = oCentroSM(0).CentroSM.Codigo
                    Else
                        centroSM = String.Empty
                    End If
                    If oPartidaPres5 IsNot Nothing Then
                        If oPartidaPres5.Count <> 0 Then
                            PartidaPRES5 = oPartidaPres5(0).PresupuestoId
                        Else
                            PartidaPRES5 = 0
                        End If
                    End If
                Else
                    centroSM = String.Empty
                    PartidaPRES5 = 0
                End If
            Else
                centroSM = String.Empty
                PartidaPRES5 = 0
            End If
            If IsNothing(linea.FecEntrega) OrElse linea.ConPlanEntrega Then
                fechaEntrega = Date.MinValue
            Else
                fechaEntrega = linea.FecEntrega
            End If
            If IsNothing(linea.Campo1) Then
                Dim Campo As New CCampo
                Campo.Num = 0
                Campo.Valor = ""
                linea.Campo1 = Campo
            End If
            If IsNothing(linea.Campo2) Then
                Dim Campo As New CCampo
                Campo.Num = 0
                Campo.Valor = ""
                linea.Campo2 = Campo
            End If

            mDBServer.Linea_ActualizarCesta(iDesdeFavorito, linea.ID, linea.CantPed, linea.UP, linea.FC, linea.PrecUc, linea.Dest, fechaEntrega, IIf(linea.ConPlanEntrega, False, linea.EntregaObl), linea.Obs, linea.Campo1.Num, linea.Campo2.Num,
                                    linea.Campo1.Valor, linea.Campo2.Valor, linea.IdAlmacen, linea.Activo, centroSM, PartidaPRES5, "", linea.Den, linea.TipoRecepcion, linea.ImportePedido, UsuCod, linea.NumLinea,
                                    linea.CentroAprovisionamiento, linea.ConRecepcionAutomatica, linea.ConPlanEntrega, linea.DesvioRecepcion)

            'Adjuntos
            For Each oadj As FSNServer.Adjunto In linea.Adjuntos
                Dim adjunto As New Adjunto(mDBServer, mIsAuthenticated)
                adjunto.DirectorioGuardar = oadj.DirectorioGuardar
                adjunto.Id = oadj.Id
                adjunto.IdEsp = oadj.IdEsp
                adjunto.Nombre = oadj.Nombre
                adjunto.Operacion = oadj.Operacion
                adjunto.FileName = oadj.FileName
                adjunto.tipo = oadj.tipo
                adjunto.Cod = oadj.Cod
                adjunto.Comentario = oadj.Comentario
                adjunto.dataSize = oadj.dataSize
                Select Case adjunto.Operacion
                    Case "D"
                        If iDesdeFavorito = 0 Then
                            adjunto.EliminarAdjunto(TiposDeDatos.Adjunto.Tipo.Cesta)
                        Else
                            adjunto.EliminarAdjunto(TiposDeDatos.Adjunto.Tipo.Favorito_Linea_Pedido)
                        End If
                    Case "I"
                        adjunto.IdEsp = linea.ID
                        If iDesdeFavorito = 0 Then
                            If oadj.Id Is Nothing OrElse oadj.Id = -1 Then
                                Dim arrReturn(1) As Long
                                arrReturn = adjunto.GrabarAdjunto(oadj.FileName, oadj.DirectorioGuardar, TiposDeDatos.Adjunto.Tipo.Cesta, adjunto.IdEsp, adjunto.Comentario, adjunto.dataSize)
                            Else
                                adjunto.CopiarAdjunto(oadj, TiposDeDatos.Adjunto.Tipo.Cesta, adjunto.IdEsp)
                            End If
                            adjunto.Operacion = ""
                        Else
                            If oadj.Id Is Nothing OrElse oadj.Id = -1 Then
                                Dim arrReturn(1) As Long
                                arrReturn = adjunto.GrabarAdjunto(oadj.FileName, oadj.DirectorioGuardar, TiposDeDatos.Adjunto.Tipo.Favorito_Linea_Pedido, adjunto.IdEsp, adjunto.Comentario, adjunto.dataSize)
                            Else
                                adjunto.CopiarAdjunto(oadj, TiposDeDatos.Adjunto.Tipo.Favorito_Linea_Pedido, adjunto.IdEsp)
                            End If
                            adjunto.Operacion = ""
                        End If
                End Select
            Next

            Dim oAtribLinea As List(Of CAtributoPedido)
            oAtribLinea = linea.OtrosDatos

            Dim oCostesLinea As List(Of CAtributoPedido)
            oCostesLinea = linea.Costes

            Dim oDescuentosLinea As List(Of CAtributoPedido)
            oDescuentosLinea = linea.Descuentos
            If iDesdeFavorito = 0 Then
                If Not linea.Costes Is Nothing Then
                    For Each oAtributo As CAtributoPedido In linea.Costes
                        If oAtributo.Seleccionado Or oAtributo.TipoCosteDescuento = 0 Then
                            If oAtributo.TipoCosteDescuento = 0 Then
                                mDBServer.ActualizarCostesDescuentosLinea(linea.ID, oAtributo.Id, 2, oAtributo.Interno, True, oAtributo.Operacion, oAtributo.Valor, oAtributo.Importe, oAtributo.MostrarEnRecep)
                            Else
                                mDBServer.ActualizarCostesDescuentosLinea(linea.ID, oAtributo.Id, 2, oAtributo.Interno, oAtributo.Obligatorio, oAtributo.Operacion, oAtributo.Valor, oAtributo.Importe, oAtributo.MostrarEnRecep)
                            End If
                        End If
                    Next
                End If

                If Not linea.Descuentos Is Nothing Then
                    For Each oAtributo As CAtributoPedido In linea.Descuentos
                        If oAtributo.Seleccionado Or oAtributo.TipoCosteDescuento = 0 Then
                            If oAtributo.TipoCosteDescuento = 0 Then
                                mDBServer.ActualizarCostesDescuentosLinea(linea.ID, oAtributo.Id, 3, oAtributo.Interno, True, oAtributo.Operacion, oAtributo.Valor, oAtributo.Importe, oAtributo.MostrarEnRecep)
                            Else
                                mDBServer.ActualizarCostesDescuentosLinea(linea.ID, oAtributo.Id, 3, oAtributo.Interno, oAtributo.Obligatorio, oAtributo.Operacion, oAtributo.Valor, oAtributo.Importe, oAtributo.MostrarEnRecep)
                            End If
                        End If
                    Next
                End If
            End If
            If Not IsNothing(linea.OtrosDatos) Then
                For Each oAtributo As CAtributoPedido In linea.OtrosDatos
                    If Not IsNothing(oAtributo.Valor) Then
                        If oAtributo.Tipo = 3 Then
                            If oAtributo.Valor.ToString() = String.Empty Then
                                mDBServer.ActualizarAtributosLinea(iDesdeFavorito, oAtributo.Id, linea.ID, oAtributo.Tipo, 1, DBNullToSomething, oAtributo.Interno, oAtributo.Obligatorio, UsuCod, oAtributo.MostrarEnRecep)
                            Else
                                mDBServer.ActualizarAtributosLinea(iDesdeFavorito, oAtributo.Id, linea.ID, oAtributo.Tipo, 1, oAtributo.Valor, oAtributo.Interno, oAtributo.Obligatorio, UsuCod, oAtributo.MostrarEnRecep)
                            End If
                        Else
                            If oAtributo.ListaExterna AndAlso Not String.IsNullOrEmpty(oAtributo.Valor) Then
                                mDBServer.ActualizarAtributosLinea(iDesdeFavorito, oAtributo.Id, linea.ID, oAtributo.Tipo, 1, oAtributo.Valores(0).Valor, oAtributo.Interno, oAtributo.Obligatorio, UsuCod, oAtributo.MostrarEnRecep, oAtributo.Valor)
                            Else
                                mDBServer.ActualizarAtributosLinea(iDesdeFavorito, oAtributo.Id, linea.ID, oAtributo.Tipo, 1, oAtributo.Valor, oAtributo.Interno, oAtributo.Obligatorio, UsuCod, oAtributo.MostrarEnRecep)
                            End If
                        End If
                    ElseIf oAtributo.Obligatorio Or oAtributo.Intro = 0 Then
                        mDBServer.ActualizarAtributosLinea(iDesdeFavorito, oAtributo.Id, linea.ID, oAtributo.Tipo, 1, DBNullToSomething, oAtributo.Interno, oAtributo.Obligatorio, UsuCod, oAtributo.MostrarEnRecep)
                    End If
                Next
            End If
        End If
        'Actualizar presupuestos. Creamos una tabla con los valores de los presupuestos introducidos
        Dim dtPres As New DataTable
        With dtPres
            .Columns.Add("PRES1", GetType(System.Int64))
            .Columns.Add("PRES2", GetType(System.Int64))
            .Columns.Add("PRES3", GetType(System.Int64))
            .Columns.Add("PRES4", GetType(System.Int64))
            .Columns.Add("PORCEN", GetType(System.Double))
            .Columns.Add("TIPO", GetType(System.Int64))
        End With
        Dim rPres As DataRow
        Dim nivel As Integer
        If linea.Pres1 IsNot Nothing Then
            For Each pres As String In Split(linea.Pres1, "#")
                rPres = dtPres.NewRow
                nivel = Split(pres, "_")(0)
                If nivel = 1 Then rPres("PRES1") = Split(pres, "_")(1)
                If nivel = 2 Then rPres("PRES2") = Split(pres, "_")(1)
                If nivel = 3 Then rPres("PRES3") = Split(pres, "_")(1)
                If nivel = 4 Then rPres("PRES4") = Split(pres, "_")(1)
                rPres("PORCEN") = Split(pres, "_")(2)
                rPres("TIPO") = 1
                dtPres.Rows.Add(rPres)
            Next
        End If
        If linea.Pres2 IsNot Nothing Then
            For Each pres As String In Split(linea.Pres2, "#")
                rPres = dtPres.NewRow
                nivel = Split(pres, "_")(0)
                If nivel = 1 Then rPres("PRES1") = Split(pres, "_")(1)
                If nivel = 2 Then rPres("PRES2") = Split(pres, "_")(1)
                If nivel = 3 Then rPres("PRES3") = Split(pres, "_")(1)
                If nivel = 4 Then rPres("PRES4") = Split(pres, "_")(1)
                rPres("PORCEN") = Split(pres, "_")(2)
                rPres("TIPO") = 2
                dtPres.Rows.Add(rPres)
            Next
        End If
        If linea.Pres3 IsNot Nothing Then
            For Each pres As String In Split(linea.Pres3, "#")
                rPres = dtPres.NewRow
                nivel = Split(pres, "_")(0)
                If nivel = 1 Then rPres("PRES1") = Split(pres, "_")(1)
                If nivel = 2 Then rPres("PRES2") = Split(pres, "_")(1)
                If nivel = 3 Then rPres("PRES3") = Split(pres, "_")(1)
                If nivel = 4 Then rPres("PRES4") = Split(pres, "_")(1)
                rPres("PORCEN") = Split(pres, "_")(2)
                rPres("TIPO") = 3
                dtPres.Rows.Add(rPres)
            Next
        End If
        If linea.Pres4 IsNot Nothing Then
            For Each pres As String In Split(linea.Pres4, "#")
                rPres = dtPres.NewRow
                nivel = Split(pres, "_")(0)
                If nivel = 1 Then rPres("PRES1") = Split(pres, "_")(1)
                If nivel = 2 Then rPres("PRES2") = Split(pres, "_")(1)
                If nivel = 3 Then rPres("PRES3") = Split(pres, "_")(1)
                If nivel = 4 Then rPres("PRES4") = Split(pres, "_")(1)
                rPres("PORCEN") = Split(pres, "_")(2)
                rPres("TIPO") = 4
                dtPres.Rows.Add(rPres)
            Next
        End If
        Dim dtPlanEntrega As New DataTable
        With dtPlanEntrega
            .Columns.Add("FECHAENTREGA", GetType(System.DateTime))
            .Columns.Add("CANTIDAD", GetType(System.Double))
            .Columns.Add("IMPORTE", GetType(System.Double))
        End With
        Dim rPlanEntrega As DataRow
        If linea.ConPlanEntrega Then
            For Each iPlanEntrega As PlanEntrega In linea.PlanEntrega
                rPlanEntrega = dtPlanEntrega.NewRow
                With rPlanEntrega
                    .Item("FECHAENTREGA") = iPlanEntrega.FechaEntrega
                    .Item("CANTIDAD") = iPlanEntrega.CantidadEntrega
                    .Item("IMPORTE") = iPlanEntrega.ImporteEntrega
                End With
                dtPlanEntrega.Rows.Add(rPlanEntrega)
            Next
        End If
        If iDesdeFavorito = 0 Then
            mDBServer.ActualizarPlanesEntregaLinea(linea.ID, dtPlanEntrega)
            mDBServer.Actualizar_Pres_Cesta(linea.ID, dtPres)
        End If
    End Sub
    ''' <summary>
    ''' Realiza las validaciones antes de emitir un pedido, validaciones de limite de adjudicacion y de limite de pedido, y si se supera los limites de pedido devuelve las solicitudes que se deben iniciar
    ''' </summary>
    ''' <param name="idPedido">id del pedido</param>
    ''' <param name="idEmp">id de la empresa</param>
    ''' <returns>dataset con las solicitudes de los flujos de aprobacion a iniciar</returns>
    ''' <remarks></remarks>
    Public Function ValidacionesEmisionPedidoCatalogo(ByVal idPedido As Long, ByVal UsuCod As String, ByVal TipoPedido As Short, ByVal idEmp As Long, Optional ByVal bSoloLimites As Boolean = False,
                                                      Optional ByRef EstadoOrden As Short = -1, Optional ByVal iNotificar As OrdenEntregaNotificacion = 0) As DataSet
        Return mDBServer.ValidacionesEmisionPedidoCatalogo(idPedido, UsuCod, TipoPedido, idEmp, bSoloLimites, EstadoOrden, iNotificar)
    End Function
    Public Function DevolverSolicitud_PedidoContraAbierto(ByVal idPedido As Long, Optional ByRef EstadoOrden As Short = -1)
        Return mDBServer.DevolverSolicitud_PedidoContraAbierto(idPedido, EstadoOrden)
    End Function
    ''' <summary>
    ''' FunciÃ³n que devuelve los atributos del pedido relacionados con el tipo de pedido
    ''' </summary>
    ''' <param name="iCodPedido">i Cod Pedido</param>
    ''' <param name="iIdTipoPedido">Id Pedido</param>
    ''' <param name="bHayIntegracion">Si hay integraciÃ³n</param>
    ''' <param name="idEmpresa">Id Empresa</param> 
    ''' <returns>Atributos</returns>
    Public Function Cargar_Atributos_TipoPedido(ByVal iCodPedido As Integer, ByVal iIdTipoPedido As Integer, ByVal bHayIntegracion As Boolean, ByVal idEmpresa As Integer, ByVal Usuario As String) As CAtributosPedido
        Authenticate()

        Dim adoRes As New DataSet
        Dim oAtributos As CAtributosPedido
        oAtributos = New CAtributosPedido(mDBServer, mIsAuthenticated)
        oAtributos = oAtributos.CargarAtributosTipoPedido(iCodPedido, iIdTipoPedido, bHayIntegracion, idEmpresa, Usuario)

        Return oAtributos
    End Function

    ''' <summary>
    ''' Obtiene la empresa para la primera linea de la cesta con partida desde el pm.
    ''' </summary>
    ''' <param name="iId">Cesta cabecera</param>
    ''' <returns></returns>
    Public Function Cargar_Empresa_Primer_PluriAnual(ByVal iId As Long) As String
        Return mDBServer.Cargar_Empresa_Primer_PluriAnual(iId)
    End Function
End Class
