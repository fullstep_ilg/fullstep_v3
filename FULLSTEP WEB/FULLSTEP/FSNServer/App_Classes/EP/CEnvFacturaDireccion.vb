﻿<Serializable()> _
Public Class CEnvFacturaDireccion
    Inherits Security

    Private miIdEmpresa As Integer
    Private miID As Integer
    Private msDir As String
    Private msCP As String
    Private msPob As String
    Private msProvi As String
    Private msPai As String

    ''' <summary>
    ''' Id de la empresa a la que corresponde la direccion de envío de factura
    ''' </summary>
    Public Property IdEmpresa() As Integer
        Get
            Return miIdEmpresa
        End Get
        Set(ByVal value As Integer)
            miIdEmpresa = value
        End Set
    End Property

    ''' <summary>
    ''' Id de la direccion de envío de factura
    ''' </summary>
    Public Property ID() As Integer
        Get
            Return miID
        End Get
        Set(ByVal Value As Integer)
            miID = Value
        End Set
    End Property

    ''' <summary>
    ''' Direccion de envío de factura
    ''' </summary>
    Public Property Direccion() As String
        Get
            Return msDir
        End Get
        Set(ByVal Value As String)
            msDir = Value
        End Set
    End Property

    ''' <summary>
    ''' Población de la dirección de envío de factura
    ''' </summary>
    Public Property Poblacion() As String
        Get
            Return msPob
        End Get
        Set(ByVal Value As String)
            msPob = Value
        End Set
    End Property

    ''' <summary>
    ''' Código Postal de la dirección de envío de factura
    ''' </summary>
    Public Property CodigoPostal() As String
        Get
            Return msCP
        End Get
        Set(ByVal Value As String)
            msCP = Value
        End Set
    End Property

    ''' <summary>
    ''' Provincia de la dirección de envío de factura
    ''' </summary>
    Public Property Provincia() As String
        Get
            Return msProvi
        End Get
        Set(ByVal Value As String)
            msProvi = Value
        End Set
    End Property

    ''' <summary>
    ''' País de la dirección de envío de factura
    ''' </summary>
    Public Property Pais() As String
        Get
            Return msPai
        End Get
        Set(ByVal Value As String)
            msPai = Value
        End Set
    End Property


    ''' <summary>
    ''' Constructor de la clase CDirEnvFactura
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si está autenticado</param>
    Public Sub New(ByRef dbserver As Fullstep.FSNDataBaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub
End Class
