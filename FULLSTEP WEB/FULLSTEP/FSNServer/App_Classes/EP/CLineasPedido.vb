﻿<Serializable()> _
Public Class CLineasPedido
    Inherits Lista(Of CLineaPedido)
#Region "Properties"
    Public ReadOnly Property ItemId(ByVal Id As Integer) As CLineaPedido
        Get
            Return Me.Find(Function(elemento As CLineaPedido) Id = elemento.ID)
        End Get
    End Property
#End Region
    ''' <summary>
    ''' Función que devuelve e inserta en la colección un objeto CLinea con Cantidad Recibida y/o importe recibido
    ''' </summary>
    ''' <param name="lId">Id de la linea</param>
    ''' <param name="dCantRec">Cantidad Recibida</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Overloads Function Add(ByVal lId As Integer, ByVal dCantRec As Object) As CLineaPedido
        Return Add(lId, dCantRec, 0)
    End Function
    ''' <summary>
    ''' Función que devuelve e inserta en la colección un objeto CLinea con Cantidad Recibida
    ''' </summary>
    ''' <param name="lId">Id de la linea</param>
    ''' <param name="dCantRec">Cantidad Recibida</param>
    ''' <param name="Activo">Id del Activo</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Overloads Function Add(ByVal lId As Integer, ByVal dCantRec As Object, ByVal Activo As Integer, Optional ByVal iTipoRecepcion As Integer = 0, Optional ByVal dImporteRec As Double = 0) As CLineaPedido
        Dim objnewmember As CLineaPedido
        objnewmember = New CLineaPedido(mDBServer, mIsAuthenticated)

        With objnewmember
            .ID = lId
            .CantRec = dCantRec
            .Activo = Activo
            .Importe = dImporteRec
            .TipoRecepcion = iTipoRecepcion
        End With

        MyBase.Add(objnewmember)
        Return objnewmember
    End Function
    ''' <summary>
    ''' Contructor de los objetos de la clase CLineas
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si está autenticado</param>
    ''' <remarks>
    ''' Llamada desde: Todos los sitios donde instanciemos un objeto CLineas
    ''' Tiempo máximo: 0 sec.</remarks>
    Public Sub New(ByRef dbserver As Fullstep.FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
    'Private _resumenLineas As List(Of CResumenLineas)
    'Public Property ResumenLineas() As List(Of CResumenLineas)
    '    Get
    '        Return _resumenLineas
    '    End Get
    '    Set(ByVal value As List(Of CResumenLineas))
    '        _resumenLineas = value
    '    End Set
    'End Property
    'Public Sub New()
    '    ResumenLineas = New List(Of CResumenLineas)
    'End Sub
End Class