﻿Public Class TextosIntegracion
    Inherits Security
    Public Function MensajeIntegracionError(ByVal Modulo As Integer, ByVal ID As Integer, Optional ByVal Idioma As String = "SPA", Optional ByVal StrError As String = "") As String
        Dim sRes As String

        sRes = DBServer.TextoGs_Carga(Modulo, ID, Idioma)

        If InStr(sRes, "@") <> 0 Then
            sRes = Replace(sRes, "@", StrError)
        End If

        Return sRes
    End Function

    ''' <summary>
    ''' Constructor del objeto Activo
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si está autenticado</param>
    ''' <remarks></remarks>
    Public Sub New(ByRef dbserver As Fullstep.FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
End Class
