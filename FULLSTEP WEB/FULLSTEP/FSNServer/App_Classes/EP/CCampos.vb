Public Class CCampos
    Inherits Lista(Of CCampo)

    ''' <summary>
    ''' Agrega un elemento CCampo a la lista
    ''' </summary>
    ''' <param name="ID">ID del campo personalizado</param>
    ''' <param name="Idioma">Idioma para la denominación</param>
    ''' <param name="Num"></param>
    ''' <param name="Den">Denominación</param>
    ''' <param name="bObl">Boolean que indica si el campo es obligatorio</param>
    ''' <param name="vDato">Valor del campo</param>
    ''' <returns>El elemento CCampo agregado</returns>
    Public Overloads Function Add(ByVal ID As Integer, ByVal Idioma As String, ByVal Num As Short, ByVal Den As Object, ByVal bObl As Boolean, ByVal vDato As Object) As CCampo

        Dim objnewmember As New CCampo(mDBServer, mIsAuthenticated)

        objnewmember.ID = ID
        objnewmember.Idioma = Idioma
        objnewmember.Num = Num
        objnewmember.Den = Den
        objnewmember.Obligatorio = bObl
        objnewmember.Valor = vDato

        Me.Add(objnewmember)
        Return objnewmember
    End Function

    ''' <summary>
    ''' Constructor de la clase CCampos
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si está autenticado</param>
    Public Sub New(ByRef dbserver As Fullstep.FSNDataBaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
    End Sub

End Class