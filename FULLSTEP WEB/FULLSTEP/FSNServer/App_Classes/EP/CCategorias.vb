﻿Option Strict Off
Option Explicit On
Imports System.Data.SqlClient
Imports Fullstep.FSNLibrary

'Ahora definida como clase publica
<System.Runtime.InteropServices.ProgId("CCategorias_NET.CCategorias")>
Public Class CCategorias
    Inherits Security

    ''' <summary>
    ''' Constructor de un objeto de la clase CCategorias
    ''' </summary>
    ''' <param name="dbserver">Servidor de la BBDD</param>
    ''' <param name="isAuthenticated">Si está autenticado</param>
    ''' <remarks>
    ''' Llamada desde: Todos los sitios donde se instancia un objeto CCategorias
    ''' Tiempo máximo: 0 sec.</remarks>
    Public Sub New(ByRef dbserver As Fullstep.FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
    ''' <summary>
    ''' Función que nos traerá un dataset con los datos necesarios para cargar el arbol de categorías. Solo salen categorias en la q la persona sea aprobador. 
    ''' </summary>
    ''' <param name="Persona">aprobador</param>
    ''' <returns>el dataset antes comentado</returns>
    ''' <remarks>
    ''' Llamada desde: El procedimiento CargarArbolCategorías de Seguimiento.aspx.vb
    ''' Tiempo máximo: 0 seg.</remarks>
    Public Function CargarArbolCategorias(ByVal Persona As String, Optional ByVal conBajas As Integer = 0) As DataSet
        Authenticate()
        Return DBServer.Categorias_CargarArbolCategorias(Persona, conBajas)
    End Function
    ''' <summary>
    ''' Función que nos devuelve un dataset con una tabla y una única fila en ésta con los datos de la categoria solicitada
    ''' </summary>
    ''' <param name="CatN1">Categoria de nivel 1</param>
    ''' <param name="CatN2">Categoria de nivel 2</param>
    ''' <param name="CatN3">Categoria de nivel 3</param>
    ''' <param name="CatN4">Categoria de nivel 4</param>
    ''' <param name="CatN5">Categoria de nivel 5</param>
    ''' <param name="Nivel">Nivel al que pertenece la categoria</param>
    ''' <returns>un dataset con una tabla y una única fila en ésta con los datos de la categoria solicitada</returns>
    ''' <remarks>
    ''' Llamada desde: El procedimiento CargarDetalleCategoria de DetCategoria.ascx.vb
    ''' Tiempo máximo: 0 seg.</remarks>
    Public Function CargarDetalleCategoria(ByVal CatN1 As Object, ByVal CatN2 As Object, ByVal CatN3 As Object, ByVal CatN4 As Object, ByVal CatN5 As Object, ByVal Nivel As Integer) As DataSet
        Authenticate()

        Dim dsCategoria As DataSet
        dsCategoria = DBServer.Categorias_CargarDetalleCategoria(CatN1, CatN2, CatN3, CatN4, CatN5, Nivel)
        dsCategoria.Tables(0).TableName = "CATEGORIA"
        Dim key() As DataColumn = {dsCategoria.Tables("CATEGORIA").Columns("ID")}
        dsCategoria.Tables("CATEGORIA").PrimaryKey = key

        Return dsCategoria
    End Function
    ''' <summary>
    ''' Función que devuelve en un objeto de la clase cAdjuntos los archivos adjuntos de una Categoria
    ''' </summary>
    ''' <param name="CatN1">Categoria de nivel 1</param>
    ''' <param name="CatN2">Categoria de nivel 2</param>
    ''' <param name="CatN3">Categoria de nivel 3</param>
    ''' <param name="CatN4">Categoria de nivel 4</param>
    ''' <param name="CatN5">Categoria de nivel 5</param>
    ''' <param name="Nivel">Nivel al que pertenece la categoria</param>
    ''' <returns>un dataset con una tabla que contiene los adjuntos de la categoria solicitada</returns>
    ''' <remarks>
    ''' Llamada desde: La función DetCategoria.ascx.vb.CargarAdjuntosCategoria
    ''' Tiempo máximo: inferior 1 seg.</remarks>
    Public Function CargarAdjuntos(ByVal CatN1 As Object, ByVal CatN2 As Object, ByVal CatN3 As Object, ByVal CatN4 As Object, ByVal CatN5 As Object, ByVal Nivel As Integer) As CAdjuntos
        Dim ador As New DataSet
        Dim oAdjuntos As New CAdjuntos(mDBServer, mIsAuthenticated)
        ador = mDBServer.Categorias_CargarAdjuntosCategoria(CatN1, CatN2, CatN3, CatN4, CatN5, Nivel)
        Dim tipo As TiposDeDatos.Adjunto.Tipo
        Select Case Nivel
            Case 1
                tipo = TiposDeDatos.Adjunto.Tipo.Categoria1
            Case 2
                tipo = TiposDeDatos.Adjunto.Tipo.Categoria2
            Case 3
                tipo = TiposDeDatos.Adjunto.Tipo.Categoria3
            Case 4
                tipo = TiposDeDatos.Adjunto.Tipo.Categoria4
            Case 5
                tipo = TiposDeDatos.Adjunto.Tipo.Categoria5
        End Select
        For Each fila As DataRow In ador.Tables(0).Rows
            oAdjuntos.Add(fila.Item("ID"), fila.Item("FECACT"), fila.Item("NOM"), fila.Item("TAMANO"), fila.Item("COM"), Nothing, Nothing, Nothing, Nothing, tipo)
        Next
        ador.Clear()
        ador = Nothing

        Return oAdjuntos
        oAdjuntos = Nothing
    End Function
End Class
