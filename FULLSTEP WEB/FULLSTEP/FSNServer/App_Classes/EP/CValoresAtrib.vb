Option Strict Off
Option Explicit On
'Ahora definida como clase publica
<System.Runtime.InteropServices.ProgId("CValoresAtrib_NET.CValoresAtrib")> Public Class CValoresAtrib
    Implements System.Collections.IEnumerable

    Private Enum TipoDeError
        ConexionNoEstablecida = 613
    End Enum
    Private mCol As Collection
    Public ReadOnly Property Item(ByVal vntIndexKey As Object) As CValorAtrib
        Get
            On Error GoTo NoSeEncuentra

            Item = mCol.Item(vntIndexKey)

            Exit Property
NoSeEncuentra:
            Item = Nothing
        End Get
    End Property
    Public ReadOnly Property Count() As Integer
        Get
            If mCol Is Nothing Then
                Count = 0
            Else
                Count = mCol.Count()
            End If
        End Get
    End Property

    Public Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
        'Cambiado a fecha de 22/12/08 por sgp
        GetEnumerator = mCol.GetEnumerator
    End Function

    'Antes-->Public Function Add(ByVal vValor As Object, Optional ByVal vIndice As Object = Nothing) As CValorAtrib
    Public Function Add(ByVal vValor As Object, ByVal vIndice As Object) As CValorAtrib
        Dim objnewmember As CValorAtrib
        objnewmember = New CValorAtrib

        objnewmember.Valor = vValor

        'Antes IsMissing
        If Not IsNothing(vIndice) And Not IsDBNull(vIndice) Then
            objnewmember.Indice = vIndice
            mCol.Add(objnewmember, CStr(vIndice))
        Else
            mCol.Add(objnewmember, CStr(mCol.Count() + 1))
        End If

        Add = objnewmember
        objnewmember = Nothing
    End Function
    'Antes Class Initialize
    Private Sub Class_Initialize_Renamed()
        mCol = New Collection
    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()
    End Sub
    'Antes Class_terminate
    Private Sub Class_Terminate_Renamed()
        'Antes-->mCol = Nothing
        mCol.Clear()
    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()
    End Sub
End Class