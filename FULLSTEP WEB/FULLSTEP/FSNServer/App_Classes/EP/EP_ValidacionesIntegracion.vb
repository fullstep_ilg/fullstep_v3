﻿Imports System.ServiceModel
Imports System.Xml.Serialization
Imports System.IO
Imports System.Web.Script.Serialization
Imports System.Configuration

Public Class EP_ValidacionesIntegracion
	Inherits Security
	''' <summary>
	''' Función que indica si está activada o no la integración en sentido salida (o entrada y salida) para la entidad indicada (y cualquier ERP).
	''' Si está¡ activada, se harÃ¡ una llamada a la mapper (en caso de que exista) para que se realicen las validaciones especí­ficas del cliente,
	''' tanto en la emisió³n de pedidos como en las recepciones.
	''' Se comprueba de forma genérica si la entiedad tiene la integración activada en algÃºn ERP porque el filtrado por ERP se harÃ¡ a la hora de llamar a una u otra mapper.
	''' </summary>
	''' <param name="entidad" >Identificador de la entidad a comprobar. Valores: 11 (pedidos EP) y 12 (recepciones EP).
	''' aunque se comprobará tanto para los pedidos EP como pedidos GS (lo mismo con las recepciones)</param>
	''' <returns>Booleano</returns>
	''' <remarks>Llamada desde EmisionPedido.aspx Emitir y PageLoad
	''' Tiempo máximo 0 sec</remarks>
	''' <revision>ngo 07/02/2012</revision>
	Public Function HayIntegracion(ByVal entidad As Integer) As Boolean
		''entidad=11: Se comprueban la integraciÃ³n de pedidos EP (11) y pedidos GS (13)
		''entidad=12: Se comprueban la integraciÃ³n de recepciones EP (12) y recepciones GS (14)
		Dim bHayIntegracion As Boolean = False
		If entidad = TablasIntegracion.PED_Aprov Then
			bHayIntegracion = DBServer.HayIntegracion(TablasIntegracion.PED_Aprov, SentidoIntegracion.Salida)
			If Not bHayIntegracion Then bHayIntegracion = DBServer.HayIntegracion(TablasIntegracion.PED_directo, SentidoIntegracion.Salida)
		ElseIf entidad = TablasIntegracion.Rec_Aprov Then
			bHayIntegracion = DBServer.HayIntegracion(TablasIntegracion.Rec_Aprov, SentidoIntegracion.Salida)
			If Not bHayIntegracion Then bHayIntegracion = DBServer.HayIntegracion(TablasIntegracion.Rec_Directo, SentidoIntegracion.Salida)
		Else
			bHayIntegracion = False
		End If
		Return bHayIntegracion
	End Function
	''' <summary>
	''' Mira en la Base de datos si hay integración de salida para una determinada entidad en una empresa
	''' </summary>
	''' <param name="iEntidad">id de la Entidad para la que se mira si hay integración.</param>
	''' <param name="iEmpresa">id de la Empresa para la que se mira si hay itegración</param>
	''' <returns>True Si hay integración. False en caso contrario.</returns>
	''' <remarks>Llamada desde EmisionPedido.aspx.vb</remarks>
	''' <revision>asg 03/06/2014</revision>
	Public Function HayIntegracionSalidaEmpresa(ByVal iEntidad As Integer, ByVal iEmpresa As Integer) As Boolean
		Return DBServer.HayIntegracionSalidaEmpresa(iEntidad, iEmpresa)
	End Function
	''' <summary>
	''' Devuelve True si existe integración de salida (o entrada y salida) con WCF para una empresa y una entidad
	''' </summary>
	''' <param name="iEntidad">Entidad</param>
	''' <param name="iEmpresa">Empresa</param>
	''' <returns>True si existe integración. False si no.</returns>
	''' <remarks>
	''' Lllamada desde FSNWeb.App_Pages.EP.EmisionPedidoaspx.vb
	''' </remarks>
	''' <revision>asg 28/03/2014</revision>
	Public Function HayIntegracionSalidaWCF(ByVal iEntidad As Integer, Optional ByVal iEmpresa As Integer = 0, Optional iErp As Integer = 0) As Boolean
		Return DBServer.HayIntegracionSalidaWCF(iEntidad, iEmpresa, iErp)
	End Function
	''' <summary>
	''' Función que llama a la mapper para hacer las validaciones de integracion
	''' </summary>
	''' <param name="iNumError">Numero de error a devolver</param>
	''' <param name="strError">Mensjae de error a devolver</param>
	''' <param name="Empresa">Identificador de la empresa</param> 
	''' <param name="sTipoPedido">Código del Tipo de pedido de la orden de entrega</param>
	''' <param name="arrCentroSM">array con los Centros SM de la orden</param>
	''' <param name="arrContrato">array con el contrato de la orden</param>
	''' <param name="arrItemPrecio">array con los precios de los items de la orden</param>
	''' <param name="arrItemFecEntrega">Array con las fechas de entrega de las líneas de pedido</param> 
	''' <returns>Devuelve True si no ha habido errores, False si los ha habido</returns>
	''' <remarks>Llamada desde EPWeb.EmisionPedido.aspc.vb -> Emitir
	''' Tiempo máximo 1 sec
	''' revisado por ngo 23/01/2011
	''' </remarks>
	Public Function EvalMapper(ByRef iNumError As Short, ByRef strError As String, ByVal Empresa As Object,
							   ByVal sTipoPedido As String, ByVal arrCentroSM() As String,
							   ByVal arrContrato() As String, ByVal arrItemPrecio() As Double, ByVal arrItemFecEntrega() As Date,
							   ByVal arrCant() As Double, ByVal arrCat1Lin() As Integer, ByVal sMoneda As String, ByVal sCambio As Double, ByVal arrArticulos() As String, ByVal arrAtribItem() As Integer,
							   ByVal arrAtribItemId() As Integer, ByVal arrAtribItemValor() As String, ByVal arrAtribOrdenId() As Integer, ByVal arrAtribOrdenValor() As String, ByVal arrCodProveERP() As String, ByVal sUsuario As String,
							   ByVal sOrgCompras As String, ByRef arrCentroAprov() As String, ByRef arrPres1() As String, ByRef arrPres2() As String, ByRef arrPres3() As String, ByRef arrPres4() As String, ByVal iTipoPedido As Short) As Boolean
		Dim Maper As Object = ""
		Dim NomMapper As String = ""
		Dim Instancia As String = System.Configuration.ConfigurationManager.AppSettings("instanciamaper")
		Dim bEvalMapper As Boolean = True
		Dim arrEmpresa() As Integer
		ReDim arrEmpresa(0)

		Try
			''Obtiene el nombre de la Mapper en función de la Empresa
			NomMapper = DBServer.Devolver_Nombre_Mapper(Empresa)
			arrEmpresa(0) = Empresa

			If Not Maper Is Nothing Then
				Maper = Nothing
			End If
			Maper = CreateObject(NomMapper & ".clsInPedidosDirectos")

			''En segundo lugar, el resto de validaciones 
			If Maper Is Nothing Then
				Return bEvalMapper
			End If

			bEvalMapper = Maper.TratarLineaEP(iNumError, strError, sTipoPedido, arrContrato, arrCentroSM, arrItemPrecio, arrItemFecEntrega, arrCant, arrCat1Lin, sMoneda, sCambio, arrArticulos, arrAtribItem, arrAtribItemId, arrAtribItemValor, arrAtribOrdenId, arrAtribOrdenValor, arrEmpresa, arrCodProveERP, sUsuario, sOrgCompras, arrCentroAprov, arrPres1, arrPres2, arrPres3, arrPres4, iTipoPedido)
			Return bEvalMapper
		Catch ex As Exception
			bEvalMapper = True
			Return bEvalMapper
		Finally
			Maper = Nothing
		End Try
	End Function
	''' <summary>
	''' Función que nos dice si hay integracion entrada salida en alguna de las tablas de un array
	''' </summary>
	''' <param name="tablas">Array de Id's de tabla para los que se quiere averiguar si hay integración</param>
	''' <returns>Booleano</returns>
	''' <remarks>Llamada desde seguimiento.aspx.vb y Recepcion.aspx.vb
	''' Tiempo máximo 0,1 sec</remarks>
	Public Function HayIntegracionEntradaoEntradaSalida(ByVal tablas As Integer()) As Boolean
		Dim hayIntegracion As Boolean = False
		For Each tabla As Integer In tablas
			hayIntegracion = DBServer.HayIntegracion(tabla)
			If hayIntegracion Then Exit For
		Next
		Return hayIntegracion
	End Function
	''' <summary>
	''' Función que llama a la mapper para hacer las validaciones de integración en las recepciones
	''' </summary>
	''' <param name="iNumError">Numero de error a devolver</param>
	''' <param name="strError">Mensaje de error a devolver</param>
	''' <param name="arrIdOrden">Ids de las ordenes de entrega</param>
	''' <param name="sAlbaran">Albarán</param>
	''' <param name="sObs">Observaciones</param>
	''' <param name="arrIdLineas">Ids de las lineas del pedido</param>
	''' <param name="arrCantLineas">Array de las cantidades de línea</param> 
	''' <returns>Devuelve True si no ha habido errores, False si los ha habido</returns>
	''' <remarks>Llamada desde Recepcion.aspx.vb --> Recepcion
	''' Tiempo máximo 1 sec</remarks>
	''' <revision>ngo 07/02/2012</revision>
	Public Function EvalMapperRecepciones(ByRef iNumError As Short, ByRef strError As String, ByVal arrIdOrden() As Integer,
					   ByVal sAlbaran As String, ByVal sObs As String, ByVal arrIdLineas() As Integer, ByVal arrCantLineas() As Double) As Boolean
		Dim Maper As Object = ""
		Dim NomMapper As String = ""
		Dim sOrgCompras As String = ""
		Dim Instancia As String = System.Configuration.ConfigurationManager.AppSettings("instanciamaper")
		Dim bEvalMapperRecepciones As Boolean = True
		Try
			''Obtiene el nombre de la Mapper en función del ID de la orden de entrega
			NomMapper = DBServer.Devolver_Nombre_Mapper(, arrIdOrden(0)) ''Se coge el Id de la primera orden para obtener el nombre de la Mapper

			If Not Maper Is Nothing Then
				Maper = Nothing
			End If
			Maper = CreateObject(NomMapper & ".clsInPedidosDirectos")

			''En segundo lugar, el resto de validaciones 
			If Maper Is Nothing Then
				Return bEvalMapperRecepciones
			End If
			EvalMapperRecepciones = Maper.TratarRecepcion(iNumError, strError, sAlbaran, sObs, arrIdLineas, arrIdOrden, arrCantLineas)
			bEvalMapperRecepciones = EvalMapperRecepciones
			Return bEvalMapperRecepciones
		Catch ex As Exception
			bEvalMapperRecepciones = True
			Return bEvalMapperRecepciones
		Finally
			Maper = Nothing
		End Try
	End Function
	Public Function EvalMapperBorrarOrdenWCF(ByRef iNumError As Short, ByRef sError As String, ByVal OrdenID As Integer, ByRef arrIdLineas() As Integer) As Boolean
		Dim sOrgCompras As String = ""
		Dim Instancia As String = System.Configuration.ConfigurationManager.AppSettings("instanciamaper")
		Dim bEvalMapperBorrarOrden As Boolean = True
		Try
			''Obtiene el nombre de la Mapper en funciÃ³n del ID de la orden de entrega
			Dim NomMapper As String = DBServer.Devolver_Nombre_Mapper(, OrdenID) ''Se coge el Id de la primera orden para obtener el nombre de la Mapper            
			Dim oMapper As Object = CreateObject(NomMapper & ".clsValidacion")

			''En segundo lugar, el resto de validaciones 
			If oMapper IsNot Nothing Then bEvalMapperBorrarOrden = oMapper.ValidacionLineaEliminada(Instancia, iNumError, sError, OrdenID, arrIdLineas, True)
		Catch ex As Exception
		End Try

		Return bEvalMapperBorrarOrden
	End Function
	''' <summary>
	''' Constructor del objeto Activo
	''' </summary>
	''' <param name="dbserver">Servidor de la bbdd</param>
	''' <param name="isAuthenticated">Si está autenticado</param>
	''' <remarks></remarks>
	Public Sub New(ByRef dbserver As Fullstep.FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
		MyBase.New(dbserver, isAuthenticated)
	End Sub
	''' <summary>
	''' Llama al servicio de integración con la estructura de pedido y el idioma para comprobar que se puede realizar el pedido
	''' </summary>
	''' <param name="strError">Mensaje de error en caso de que no se pueda validar el pedido. Si todo ha ido bien, devuelve una cadena vacía</param>
	''' <param name="oOrden">Orden de pedido</param>
	''' <param name="idioma">Idioma para el mensaje de error</param>
	''' <returns>True si todo ha ido bien. False si ha fallado algo</returns>
	''' <remarks>
	''' Método utilizado en FSNWeb.App_Pages.EP.EmisionPedidoBis.aspx.vb
	''' </remarks>
	''' <revision>aam 29/07/2016</revision>
	Public Function ValidacionesWCF_EP(ByRef strError As String, ByVal oOrden As CEmisionPedido, ByVal idioma As FSNLibrary.Idioma, ByVal Est As Integer) As Boolean

		Dim iErp As Integer
		Dim oIntegracion As New Integracion(DBServer, True)
		Dim serializer As New JavaScriptSerializer()
		Dim sOrdenValidacion As String
		sOrdenValidacion = serializer.Serialize(oOrden)

		iErp = CInt(oIntegracion.getErpFromEmpresa(oOrden.DatosGenerales.Emp))
		If iErp > 0 Then 'Si esta comprobaciÃ³n no es cierta, no se integra ya no hay ERP.
			'llamamos al webservice
			strError = Me.RemoteValidacionesWCF(sOrdenValidacion, TablasIntegracion.PED_Aprov, idioma, iErp)
		End If

		If IsNothing(strError) Or strError = "" Then
			Return True
		Else
			Return False
		End If
	End Function


	''' <summary>
	''' Llama al servicio de integración con la estructura de pedido y el idioma para comprobar que se puede realizar el pedido
	''' </summary>
	''' <param name="strError">Mensaje de error en caso de que no se pueda validar el pedido. Si todo ha ido bien, devuelve una cadena vacía</param>
	''' <param name="oOrden">Orden de pedido</param>
	''' <param name="idioma">Idioma para el mensaje de error</param>
	''' <returns>True si todo ha ido bien. False si ha fallado algo</returns>
	''' <remarks>
	''' Método utilizado en FSNWeb.App_Pages.EP.EmisionPedidoBis.aspx.vb
	''' </remarks>
	''' <revision>aam 29/07/2016</revision>
	Public Function ValidacionesWCF_REC(ByRef strError As String, ByVal oRecep As CRecepcion, ByVal idioma As FSNLibrary.Idioma) As Boolean

		Dim oIntegracion As New Integracion(DBServer, True)
		Dim serializer As New JavaScriptSerializer()
		Dim sRecepcionValidacion As String
		sRecepcionValidacion = serializer.Serialize(oRecep)

		'llamamos al webservice
		strError = Me.RemoteValidacionesWCF_REC(sRecepcionValidacion, TablasIntegracion.Rec_Aprov, idioma)

		If IsNothing(strError) Or strError = "" Then
			Return True
		Else
			Return False
		End If
	End Function

	''' <summary>
	''' Llama al servicio de integración de FSIS de validación de pedidos.
	''' </summary>
	''' <param name="DS">DataSet con toda la información del pedido.</param>
	''' <param name="iEntidad">Número de entidad</param>
	''' <param name="sIdioma">Idioma del usuario</param>
	''' <param name="iErp">El ERP actual</param> 
	''' <returns>Devuelve el mensaje de error en caso de producirse o cadena vacía si todo va bien.</returns>
	''' <remarks>
	''' Método utilizado en FSNServer.App_Classes.EP.EP_ValidacionesIntegracion
	''' </remarks>
	''' <revision>aam 28/01/2015</revision>
	Private Function RemoteValidacionesWCF_REC(ByVal sValidacionRecep As String, iEntidad As Integer, sIdioma As String) As String
		Dim oIntegracion As New Integracion(DBServer, True)
		Dim sRuta, sNombre, sContrasenya As String
		Dim iServiceBindingType, iServiceSecurityMode, iClientCredentialType, iProxyCredentialType As Integer
		sNombre = Nothing : sContrasenya = Nothing
		Try
			oIntegracion.ObtenerCredencialesWCF(iEntidad, 0, iServiceBindingType, iServiceSecurityMode, iClientCredentialType, iProxyCredentialType, sNombre, sContrasenya)
			sRuta = oIntegracion.ObtenerRutaInstalacionFSIS()
			sRuta = Split(sRuta, "Exportar")(0) & "Validaciones.svc"

			Dim oValidacionesRecepcionRequest As ValidacionesRecepcionRequest
			Dim oValidacionesRecepcionResponse As ValidacionesRecepcionResponse

			oValidacionesRecepcionRequest = New ValidacionesRecepcionRequest(sValidacionRecep, iEntidad, sIdioma)

			Dim myChannelFactory As ChannelFactory(Of IValidaciones)
			Dim myEndpoint As New EndpointAddress(sRuta)

			Select Case iServiceBindingType
				Case 1 'WSHttpBinding
					Dim iSecurityMode As SecurityMode = iServiceSecurityMode
					Dim myBinding As New WSHttpBinding(iSecurityMode)
					With myBinding
						.SendTimeout = New TimeSpan(0, ConfigurationManager.AppSettings("TimeoutMinINT"), ConfigurationManager.AppSettings("TimeoutSegINT"))
						.Security.Transport.ClientCredentialType = iClientCredentialType
						.Security.Transport.ProxyCredentialType = iProxyCredentialType
					End With
					myChannelFactory = New ChannelFactory(Of IValidaciones)(myBinding, myEndpoint)
				Case Else 'BasicHttpBinding
					Dim iBasicSecurityMode As BasicHttpSecurityMode = iServiceSecurityMode
					Dim myBinding As New BasicHttpBinding(iBasicSecurityMode)
					With myBinding
						.SendTimeout = New TimeSpan(0, ConfigurationManager.AppSettings("TimeoutMinINT"), ConfigurationManager.AppSettings("TimeoutSegINT"))
						.Security.Transport.ClientCredentialType = iClientCredentialType
						.Security.Transport.ProxyCredentialType = iProxyCredentialType
					End With
					myChannelFactory = New ChannelFactory(Of IValidaciones)(myBinding, myEndpoint)
			End Select

			With (myChannelFactory.Credentials)
				Select Case iClientCredentialType
					Case HttpClientCredentialType.Windows
						With .Windows.ClientCredential
							.UserName = sNombre
							.Password = sContrasenya
						End With
					Case HttpClientCredentialType.Basic
						With .UserName
							.UserName = sNombre
							.Password = sContrasenya
						End With
					Case HttpClientCredentialType.Certificate
						.ClientCertificate.SetCertificate(System.Security.Cryptography.X509Certificates.StoreLocation.LocalMachine,
														  System.Security.Cryptography.X509Certificates.StoreName.My,
														  System.Security.Cryptography.X509Certificates.X509FindType.FindBySubjectName, "client")
				End Select
			End With

			Dim wcfProxyClient As IValidaciones = myChannelFactory.CreateChannel()
			oValidacionesRecepcionResponse = wcfProxyClient.ValidacionesRecepcion(oValidacionesRecepcionRequest)
			myChannelFactory.Close()

			Return oValidacionesRecepcionResponse.ValidacionesRecepcionResult
		Catch ex As Exception
			Return Nothing
		End Try

	End Function

	''' <summary>
	''' Llama al servicio de integración de FSIS de validación de pedidos.
	''' </summary>
	''' <param name="DS">DataSet con toda la información del pedido.</param>
	''' <param name="iEntidad">Número de entidad</param>
	''' <param name="sIdioma">Idioma del usuario</param>
	''' <param name="iErp">El ERP actual</param> 
	''' <returns>Devuelve el mensaje de error en caso de producirse o cadena vacía si todo va bien.</returns>
	''' <remarks>
	''' Método utilizado en FSNServer.App_Classes.EP.EP_ValidacionesIntegracion
	''' </remarks>
	''' <revision>aam 28/01/2015</revision>
	Private Function RemoteValidacionesWCF(DS As String, iEntidad As Integer, sIdioma As String, iErp As Integer) As String
		Dim oIntegracion As New Integracion(DBServer, True)
		Dim sRuta, sNombre, sContrasenya As String
		Dim iServiceBindingType, iServiceSecurityMode, iClientCredentialType, iProxyCredentialType As Integer
		sNombre = Nothing : sContrasenya = Nothing
		Try
			oIntegracion.ObtenerCredencialesWCF(iEntidad, iErp, iServiceBindingType, iServiceSecurityMode, iClientCredentialType, iProxyCredentialType, sNombre, sContrasenya)
			sRuta = oIntegracion.ObtenerRutaInstalacionFSIS()
			sRuta = Split(sRuta, "Exportar")(0) & "Validaciones.svc"

			Dim oValidacionesPedidoRequest As ValidacionesPedidoRequest
			Dim oValidacionesPedidoResponse As ValidacionesPedidoResponse

			oValidacionesPedidoRequest = New ValidacionesPedidoRequest(DS, iEntidad, sIdioma, iErp)

			Dim myChannelFactory As ChannelFactory(Of IValidaciones)
			Dim myEndpoint As New EndpointAddress(sRuta)

			Select Case iServiceBindingType
				Case 1 'WSHttpBinding
					Dim iSecurityMode As SecurityMode = iServiceSecurityMode
					Dim myBinding As New WSHttpBinding(iSecurityMode)
					With myBinding
						.SendTimeout = New TimeSpan(0, ConfigurationManager.AppSettings("TimeoutMinINT"), ConfigurationManager.AppSettings("TimeoutSegINT"))
						.Security.Transport.ClientCredentialType = iClientCredentialType
						.Security.Transport.ProxyCredentialType = iProxyCredentialType
					End With
					myChannelFactory = New ChannelFactory(Of IValidaciones)(myBinding, myEndpoint)
				Case Else 'BasicHttpBinding
					Dim iBasicSecurityMode As BasicHttpSecurityMode = iServiceSecurityMode
					Dim myBinding As New BasicHttpBinding(iBasicSecurityMode)
					With myBinding
						.SendTimeout = New TimeSpan(0, ConfigurationManager.AppSettings("TimeoutMinINT"), ConfigurationManager.AppSettings("TimeoutSegINT"))
						.Security.Transport.ClientCredentialType = iClientCredentialType
						.Security.Transport.ProxyCredentialType = iProxyCredentialType
					End With
					myChannelFactory = New ChannelFactory(Of IValidaciones)(myBinding, myEndpoint)
			End Select

			With (myChannelFactory.Credentials)
				Select Case iClientCredentialType
					Case HttpClientCredentialType.Windows
						With .Windows.ClientCredential
							.UserName = sNombre
							.Password = sContrasenya
						End With
					Case HttpClientCredentialType.Basic
						With .UserName
							.UserName = sNombre
							.Password = sContrasenya
						End With
					Case HttpClientCredentialType.Certificate
						.ClientCertificate.SetCertificate(System.Security.Cryptography.X509Certificates.StoreLocation.LocalMachine,
														  System.Security.Cryptography.X509Certificates.StoreName.My,
														  System.Security.Cryptography.X509Certificates.X509FindType.FindBySubjectName, "client")
				End Select
			End With

			Dim wcfProxyClient As IValidaciones = myChannelFactory.CreateChannel()
			oValidacionesPedidoResponse = wcfProxyClient.ValidacionesPedido(oValidacionesPedidoRequest)
			myChannelFactory.Close()

			Return oValidacionesPedidoResponse.ValidacionesPedidoResult
		Catch ex As Exception
			Return Nothing
		End Try

	End Function
	''' <summary>
	''' Llama al servicio de integraciÃ³n de FSIS de validaciÃ³n de pedidos.
	''' </summary>
	''' <param name="sPedido">DataSet con toda la informaciÃ³n del pedido.</param>
	''' <param name="iEntidad">NÃºmero de entidad</param>
	''' <param name="sIdioma">Idioma del usuario</param>
	''' <param name="iErp">El ERP actual</param> 
	''' <returns>Devuelve el mensaje de error en caso de producirse o cadena vacÃ­a si todo va bien.</returns>
	''' <remarks>
	''' MÃ©todo utilizado en FSNServer.App_Classes.EP.EP_ValidacionesIntegracion
	''' </remarks>
	''' <revision>aam 28/01/2015</revision>
	Public Function RemoteValidarBorradoPedidoWCF(sPedido As String, iEntidad As Integer, sIdioma As String, iErp As Integer) As String
		Dim sError As String = Nothing
		Dim iNumError As Integer

		Dim oIntegracion As New Integracion(DBServer, True)
		Dim sRuta, sNombre, sContrasenya As String
		Dim iServiceBindingType, iServiceSecurityMode, iClientCredentialType, iProxyCredentialType As Integer
		sNombre = Nothing : sContrasenya = Nothing
		Try
			oIntegracion.ObtenerCredencialesWCF(iEntidad, iErp, iServiceBindingType, iServiceSecurityMode, iClientCredentialType, iProxyCredentialType, sNombre, sContrasenya)
			sRuta = oIntegracion.ObtenerRutaInstalacionFSIS()
			sRuta = Split(sRuta, "Exportar")(0) & "Validaciones.svc"

			Dim myChannelFactory As ChannelFactory(Of IValidaciones)
			Dim myEndpoint As New EndpointAddress(sRuta)

			Select Case iServiceBindingType
				Case 1 'WSHttpBinding
					Dim iSecurityMode As SecurityMode = iServiceSecurityMode
					Dim myBinding As New WSHttpBinding(iSecurityMode)
					With myBinding
						.SendTimeout = New TimeSpan(0, ConfigurationManager.AppSettings("TimeoutMinINT"), ConfigurationManager.AppSettings("TimeoutSegINT"))
						.Security.Transport.ClientCredentialType = iClientCredentialType
						.Security.Transport.ProxyCredentialType = iProxyCredentialType
					End With
					myChannelFactory = New ChannelFactory(Of IValidaciones)(myBinding, myEndpoint)
				Case Else 'BasicHttpBinding
					Dim iBasicSecurityMode As BasicHttpSecurityMode = iServiceSecurityMode
					Dim myBinding As New BasicHttpBinding(iBasicSecurityMode)
					With myBinding
						.SendTimeout = New TimeSpan(0, ConfigurationManager.AppSettings("TimeoutMinINT"), ConfigurationManager.AppSettings("TimeoutSegINT"))
						.Security.Transport.ClientCredentialType = iClientCredentialType
						.Security.Transport.ProxyCredentialType = iProxyCredentialType
					End With
					myChannelFactory = New ChannelFactory(Of IValidaciones)(myBinding, myEndpoint)
			End Select

			With (myChannelFactory.Credentials)
				Select Case iClientCredentialType
					Case HttpClientCredentialType.Windows
						With .Windows.ClientCredential
							.UserName = sNombre
							.Password = sContrasenya
						End With
					Case HttpClientCredentialType.Basic
						With .UserName
							.UserName = sNombre
							.Password = sContrasenya
						End With
					Case HttpClientCredentialType.Certificate
						.ClientCertificate.SetCertificate(System.Security.Cryptography.X509Certificates.StoreLocation.LocalMachine,
														  System.Security.Cryptography.X509Certificates.StoreName.My,
														  System.Security.Cryptography.X509Certificates.X509FindType.FindBySubjectName, "client")
				End Select
			End With

			Dim oValidacionesBorradoPedidoRequest As ValidacionesBorradoPedidoRequest
			oValidacionesBorradoPedidoRequest = New ValidacionesBorradoPedidoRequest(iNumError, sError, sPedido, sIdioma, iErp.ToString, True)

			Dim wcfProxyClient As IValidaciones = myChannelFactory.CreateChannel()
			Dim oValidacionesBorradoPedidoResponse As ValidacionesBorradoPedidoResponse = wcfProxyClient.ValidacionesBorradoPedido(oValidacionesBorradoPedidoRequest)
			myChannelFactory.Close()

			If Not oValidacionesBorradoPedidoResponse.ValidacionesBorradoPedidoResult Then sError = oValidacionesBorradoPedidoResponse.sError
		Catch ex As Exception
		End Try

		Return sError
	End Function
    ''' <summary>
    ''' Función que llama a la mapper para obtener los valores de una lista externa
    ''' Cuando se le llama desde EP se pasa el Id de la empresa y no se pasa sCodOrgCompras y sUON
    ''' Cuando se le llama desde PM se pasa Empresa=0 y se pasa sCodOrgCompras o sUON
    ''' </summary>
    ''' <param name="Empresa">Identificador de la empresa</param> 
    ''' <param name="sCodOrgCompras">Cod. org. compras</param>
    ''' <param name="sUON">cod UONs separadas por #</param>
    ''' <returns>Devuelve True si no ha habido errores, False si los ha habido</returns>
    ''' <remarks>Llamada desde EPWeb.EmisionPedido.aspc.vb -> Emitir
    ''' Tiempo máximo 1 sec
    ''' revisado por ngo 23/01/2011
    ''' </remarks>
    Public Function EvalListaExterna(ByVal Empresa As Integer, ByVal IdAtributoListaExterna As Integer, ByVal lAtributos(,) As String, ByVal sEmisor As String, ByVal sCodigo As String, ByVal sDenominacion As String,
                                     Optional ByVal sCodOrgCompras As String = "", Optional ByVal sUON As String = "", Optional ByVal sCentro As String = "", Optional sCodProveedor As String = "") As String(,)
        Dim Mapper As Object = ""
        Dim NomMapper As String = ""
        Dim lResultados(,) As String
        Dim oIntegracion As New Integracion(DBServer, True)
        Dim bWCF As Boolean

        Try
            ''Obtiene el nombre de la Mapper en función de la Empresa
            NomMapper = DBServer.Devolver_Nombre_Mapper(Empresa)

            bWCF = False

            bWCF = HayIntegracionSalidaWCF(TablasIntegracion.PED_Aprov)

            If Not bWCF Then

                If Not Mapper Is Nothing Then
                    Mapper = Nothing
                End If

                Mapper = CreateObject(NomMapper & ".cListadoAtributos")

                lResultados = Mapper.devolverListaObjetos(Empresa, IdAtributoListaExterna, lAtributos, sEmisor, sCodigo, sDenominacion, sCodOrgCompras, sUON, sCentro, sCodProveedor)
                Return lResultados
            Else

                Dim dAtributos As New DataSet
                Dim dResul As New DataSet
                Dim colResultado As String(,)
                Dim iCont As Integer
                Dim i As Integer
                Dim dtAtributos As DataTable
                Dim keys(0) As DataColumn
                Dim dtNewRow As DataRow

                'Se transforma el objeto lAtributos(,) a un Dataset
                dtAtributos = dAtributos.Tables.Add("Atributos")
                dtAtributos.Columns.Add("Key", System.Type.GetType("System.String"))
                dtAtributos.Columns.Add("Value", System.Type.GetType("System.String"))
                keys(0) = dtAtributos.Columns("Atributos")
                dtAtributos.PrimaryKey = keys

                For i = 0 To UBound(lAtributos)
                    dtNewRow = dtAtributos.NewRow
                    dtNewRow.Item("Key") = CStr(lAtributos(i, 0))
                    dtNewRow.Item("Value") = CStr(lAtributos(i, 1))
                    dtAtributos.Rows.Add(dtNewRow)
                Next

                dResul = RemoteValidacionesWCF_ListaExt(Empresa, IdAtributoListaExterna, dAtributos, sEmisor, sCodigo, sDenominacion, sCodOrgCompras, sUON, sCentro, sCodProveedor)

                'Se transforma el objeto Dataset a un String(,)

                iCont = 0

                ReDim Preserve colResultado(dResul.Tables(0).Rows.Count - 1, 1)
                For Each oRow In dResul.Tables(0).Rows
                    colResultado(iCont, 0) = oRow("Key")
                    colResultado(iCont, 1) = oRow("Value")
                    iCont += 1
                Next

                Return colResultado

            End If

        Catch ex As Exception
            Return Nothing
        Finally
            Mapper = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Llama al servicio de integración de FSIS de validación de pedidos.
    ''' </summary>
    ''' <param name="DS">DataSet con toda la información del pedido.</param>
    ''' <param name="iEntidad">Número de entidad</param>
    ''' <param name="sIdioma">Idioma del usuario</param>
    ''' <param name="iErp">El ERP actual</param> 
    ''' <param name="sCodProveedor">Código del proveedor</param>
    ''' <returns>Devuelve el mensaje de error en caso de producirse o cadena vacía si todo va bien.</returns>
    ''' <remarks>
    ''' Método utilizado en FSNServer.App_Classes.EP.EP_ValidacionesIntegracion
    ''' </remarks>
    ''' <revision>aam 29/06/2017</revision>
    Private Function RemoteValidacionesWCF_ListaExt(ByVal Empresa As Integer, ByVal IdAtributoListaExterna As Integer, ByVal lAtributos As DataSet, ByVal sEmisor As String,
                                                    ByVal sCodigo As String, ByVal sDenominacion As String, Optional ByVal sCodOrgCompras As String = "", Optional ByVal sUON As String = "",
                                                    Optional ByVal sCentro As String = "", Optional sCodProveedor As String = "") As DataSet


        Dim oIntegracion As New Integracion(DBServer, True)
        Dim sRuta, sNombre, sContrasenya As String
        Dim iServiceBindingType, iServiceSecurityMode, iClientCredentialType, iProxyCredentialType As Integer
        sNombre = Nothing : sContrasenya = Nothing
        Try
            oIntegracion.ObtenerCredencialesWCF(TablasIntegracion.PED_Aprov, 0, iServiceBindingType, iServiceSecurityMode, iClientCredentialType, iProxyCredentialType, sNombre, sContrasenya)
            sRuta = oIntegracion.ObtenerRutaInstalacionFSIS()
            sRuta = Split(sRuta, "Exportar")(0) & "Validaciones.svc"

            Dim oDevolverListaObjetosRequest As DevolverListaObjetosRequest
            Dim oDevolverListaObjetosResponse As DevolverListaObjetosResponse

            oDevolverListaObjetosRequest = New DevolverListaObjetosRequest(Empresa, IdAtributoListaExterna, lAtributos, sEmisor, sCodigo, sDenominacion, sCodOrgCompras, sUON, sCentro, sCodProveedor)

            Dim myChannelFactory As ChannelFactory(Of IValidaciones)
            Dim myEndpoint As New EndpointAddress(sRuta)

            Select Case iServiceBindingType
                Case 1 'WSHttpBinding
                    Dim iSecurityMode As SecurityMode = iServiceSecurityMode
                    Dim myBinding As New WSHttpBinding(iSecurityMode)
                    With myBinding
                        .SendTimeout = New TimeSpan(0, ConfigurationManager.AppSettings("TimeoutMinINT"), ConfigurationManager.AppSettings("TimeoutSegINT"))
                        .Security.Transport.ClientCredentialType = iClientCredentialType
                        .Security.Transport.ProxyCredentialType = iProxyCredentialType
                    End With
                    myChannelFactory = New ChannelFactory(Of IValidaciones)(myBinding, myEndpoint)
                Case Else 'BasicHttpBinding
                    Dim iBasicSecurityMode As BasicHttpSecurityMode = iServiceSecurityMode
                    Dim myBinding As New BasicHttpBinding(iBasicSecurityMode)
                    With myBinding
                        .SendTimeout = New TimeSpan(0, ConfigurationManager.AppSettings("TimeoutMinINT"), ConfigurationManager.AppSettings("TimeoutSegINT"))
                        .Security.Transport.ClientCredentialType = iClientCredentialType
                        .Security.Transport.ProxyCredentialType = iProxyCredentialType
                    End With
                    myChannelFactory = New ChannelFactory(Of IValidaciones)(myBinding, myEndpoint)
            End Select

            With (myChannelFactory.Credentials)
                Select Case iClientCredentialType
                    Case HttpClientCredentialType.Windows
                        With .Windows.ClientCredential
                            .UserName = sNombre
                            .Password = sContrasenya
                        End With
                    Case HttpClientCredentialType.Basic
                        With .UserName
                            .UserName = sNombre
                            .Password = sContrasenya
                        End With
                    Case HttpClientCredentialType.Certificate
                        .ClientCertificate.SetCertificate(System.Security.Cryptography.X509Certificates.StoreLocation.LocalMachine,
                                                          System.Security.Cryptography.X509Certificates.StoreName.My,
                                                          System.Security.Cryptography.X509Certificates.X509FindType.FindBySubjectName, "client")
                End Select
            End With

            Dim wcfProxyClient As IValidaciones = myChannelFactory.CreateChannel()
            oDevolverListaObjetosResponse = wcfProxyClient.DevolverListaObjetos(oDevolverListaObjetosRequest)
            myChannelFactory.Close()

            Return oDevolverListaObjetosResponse.DevolverListaObjetosResult
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
End Class