<Serializable()> _
Public Class CPersonas
    Inherits Lista(Of CPersona)

    ''' <summary>
    ''' Funci�n que a�ade personas a la colecci�n de Personas
    ''' </summary>
    ''' <param name="sCod">C�digo</param>
    ''' <param name="sNom">Nombre</param>
    ''' <param name="sApe">Apellido</param>
    ''' <param name="sCargo">Cargo</param>
    ''' <param name="sDep">Departamento</param>
    ''' <param name="sEmail">Email</param>
    ''' <param name="sTfno">Tel�fono</param>
    ''' <param name="sTfno2">Tel�fono2</param>
    ''' <param name="sFax">Fax</param>
    ''' <param name="sUon1">Unidad Org de nivel 1</param>
    ''' <param name="sUon2">Unidad Org de nivel 2</param>
    ''' <param name="sUon3">Unidad Org de nivel 3</param>
    ''' <param name="sUON1DEN">Denominaci�n Unidad org nivel 1</param>
    ''' <param name="sUON2DEN">Denominaci�n Unidad org nivel 2</param>
    ''' <param name="sUON3DEN">Denominaci�n Unidad org nivel 3</param>
    ''' <param name="sEqp">Equipo</param>
    ''' <param name="sDepDen">Denominaci�n del departamento</param>
    ''' <returns>un objeto Persona con el nuevo miembro a�adido</returns>
    ''' <remarks>
    ''' Llamada desde: No es relevante
    ''' Tiempo m�ximo: 1 sec.</remarks>
    Public Overloads Function Add(ByVal sCod As String, ByVal sNom As Object, ByVal sApe As Object, ByVal sCargo As Object, ByVal sDep As Object, ByVal sEmail As Object, ByVal sTfno As Object, ByVal sTfno2 As Object, ByVal sFax As Object, ByVal sUon1 As Object, ByVal sUon2 As Object, ByVal sUon3 As Object, ByVal sUON1DEN As Object, ByVal sUON2DEN As Object, ByVal sUON3DEN As Object, ByVal sEqp As Object, ByVal sDepDen As Object) As CPersona
        'create a new object
        Dim objnewmember As CPersona
        objnewmember = New CPersona(DBServer, mIsAuthenticated)

        With objnewmember
            .Cod = sCod
            If IsDBNull(sNom) Then sNom = ""
            .Nombre = sNom
            .Apellidos = sApe
            .Cargo = sCargo
            .Dep = sDep
            .Email = sEmail
            .Fax = sFax
            .Tfno = sTfno
            .Tfno2 = sTfno2
            .UON1 = sUon1
            .UON2 = sUon2
            .UON3 = sUon3
            .UON1DEN = sUON1DEN
            .UON2DEN = sUON2DEN
            .UON3DEN = sUON3DEN
            .Eqp = sEqp
            .DepDen = sDepDen
            .NomApe = sNom & " " & sApe
        End With

        MyBase.Add(objnewmember)
        Return objnewmember
    End Function
    ''' <summary>
    ''' Carga todos los posibles receptores para un pedido para el usuario que este utilizando la aplicaci�n, pasandole como par�metro el c�digo de persona.
    ''' </summary>
    ''' <returns>Un objeto de la clase CPersonas con los datos de los posibles receptores de los pedidos que el usuario realice.</returns>
    ''' <remarks>
    ''' Llamada desde: NuevoFavorito.aspx
    ''' Tiempo m�ximo: 0 sec.
    ''' </remarks>
    Public Function CargarReceptores(ByVal sDest As String) As CPersonas
        Authenticate()

        Dim dsReceptores As New DataSet
        dsReceptores = mDBServer.Personas_CargarReceptores(sDest)

        Me.Clear()

        Dim dt As DataTable = dsReceptores.Tables(0)
        For Each oRow As DataRow In dt.Rows
            Me.Add(oRow.Item("COD"), DBNullToStr(oRow.Item("NOM")), oRow.Item("APE"), "", "", "", "", Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
        Next

        Dim query As Collections.Generic.List(Of CPersona) = Me.Distinct(New ComparadorCPersona()).ToList()
        Me.Clear()
        Me.AddRange(query)

        Return Me
    End Function
#Region "GESTORES"
    '''Revisado por: blp. Fecha: 11/07/2012
    ''' <summary>
    ''' Funci�n que crea la estructura de una tabla de c�digos de gestor
    ''' </summary>
    ''' <returns>DataTable vac�o</returns>
    ''' <remarks>Llamada desde Seguimiento.aspx.vb. M�x. 0,1 seg.</remarks>
    Public Function CrearTablaGestores() As DataTable
        Dim dtGestores As DataTable = New DataTable("GESTORES")
        dtGestores.Columns.Add("COD", GetType(System.String))
        Return dtGestores
    End Function
    '''Revisado por: blp. Fecha: 11/07/2012
    ''' <summary>
    ''' Carga en una tabla los c�digos, nombres y apellidos de los gestores a partir de una lista de c�digos
    ''' </summary>
    ''' <param name="CodGestores">Tabla con los c�digos de los gestores</param>
    ''' <returns>Una tabla con los c�digos, nombres y apellidos</returns>
    ''' <remarks>
    ''' Llamada desde: Seguimiento.aspx.vb
    ''' Tiempo m�ximo: 0,1 sec.
    ''' </remarks>
    Public Function CargarGestores(ByVal CodGestores As DataTable) As DataTable
        Authenticate()
        Dim dtGestores As New DataTable
        dtGestores = mDBServer.Personas_CargarGestores(CodGestores)
        Dim key(0) As DataColumn
        key(0) = dtGestores.Columns("GESTORCOD")
        dtGestores.PrimaryKey = key
        Return (dtGestores)
    End Function
#End Region
    ''' <summary>
    ''' Constructor del objeto Personas
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si est� autenticado</param>
    ''' <remarks></remarks>
    Public Sub New(ByRef dbserver As Fullstep.FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
End Class
Public Class ComparadorCPersona
    Implements Generic.IEqualityComparer(Of CPersona)

    Public Function Equals1(ByVal x As CPersona, ByVal y As CPersona) As Boolean Implements System.Collections.Generic.IEqualityComparer(Of CPersona).Equals
        Return (x.Cod = y.Cod)
    End Function
    Public Function GetHashCode1(ByVal obj As CPersona) As Integer Implements System.Collections.Generic.IEqualityComparer(Of CPersona).GetHashCode
        Return obj.Cod.GetHashCode()
    End Function
End Class