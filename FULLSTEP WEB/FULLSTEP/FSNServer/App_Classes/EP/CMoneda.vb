Public Class CMoneda
    Inherits Security
    ' *** Clase: CMoneda

    ' Variables privadas con la informacion de una moneda
    Private mvarCod As String
    Private mvarDen As String
    Private mvarEQUIV As Double
    Private mvarEQ_ACT As Object
    Private mvarFecact As Object

    Public Property EQUIV() As Double
        Get

            ' * Objetivo: Devolver la variable privada EQUIV
            ' * Recibe: Nada
            ' * Devuelve: Equivalencia de la moneda respecto a la central

            EQUIV = mvarEQUIV

        End Get
        Set(ByVal Value As Double)

            ' * Objetivo: Dar valor a la variable privada EQUIV
            ' * Recibe: Equivalencia de la moneda respecto a la central
            ' * Devuelve: Nada

            mvarEQUIV = Value

        End Set
    End Property

    Public Property EqAct() As Object
        Get

            ' * Objetivo: Devolver la variable privada EQUIV
            ' * Recibe: Nada
            ' * Devuelve: Equivalencia actualizable S/N

            EqAct = mvarEQ_ACT

        End Get
        Set(ByVal Value As Object)

            ' * Objetivo: Dar valor a la variable privada EQ_ACT
            ' * Recibe: Equivalencia actualizable S/N
            ' * Devuelve: Nada

            mvarEQ_ACT = Value

        End Set
    End Property

    Public Property FECACT() As Object
        Get

            ' * Objetivo: Devolver la variable privada FECACT
            ' * Recibe: Nada
            ' * Devuelve: Fecha de ultima actualizacion de la moneda

            FECACT = mvarFecact

        End Get
        Set(ByVal Value As Object)

            ' * Objetivo: Dar valor a la variable privada FECACT
            ' * Recibe: Fecha de ultima actualizacion de la moneda
            ' * Devuelve: Nada

            mvarFecact = Value

        End Set
    End Property

    Public Property Den() As String
        Get

            ' * Objetivo: Devolver la variable privada DEN
            ' * Recibe: Nada
            ' * Devuelve: Denominacion de la moneda

            Den = mvarDen

        End Get
        Set(ByVal Value As String)

            ' * Objetivo: Dar valor a la variable privada DEN
            ' * Recibe: Denominacion de la moneda
            ' * Devuelve: Nada

            mvarDen = Value

        End Set
    End Property

    Public Property Cod() As String
        Get

            ' * Objetivo: Devolver la variable privada COD
            ' * Recibe: Nada
            ' * Devuelve: Codigo de la moneda

            Cod = mvarCod

        End Get
        Set(ByVal Value As String)

            ' * Objetivo: Dar valor a la variable privada COD
            ' * Recibe: Codigo de la moneda
            ' * Devuelve: Nada

            mvarCod = Value

        End Set
    End Property

    ''' <summary>
    ''' Constructor de la clase CMoneda
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si est� autenticado</param>
    Public Sub New(ByRef dbserver As Fullstep.FSNDataBaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
    End Sub

End Class