Imports System.Web

<Serializable()> _
Public Class CAtributosPedido
    Inherits Lista(Of CAtributoPedido)

    ''' <summary>
    ''' Funcion que comprueba el tipo de pedido
    ''' </summary>
    ''' <param name="iCodPedido">iCodPedido</param>
    ''' <param name="iIdTipoPedido">iIdTipoPedido</param>
    ''' <param name="iEmp">sEmp</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Comprobar_TipoPedido(ByVal iCodPedido As Integer, ByVal iIdTipoPedido As Integer, ByVal iEmp As Integer) As Object
        Authenticate()

        Dim oTipoPedido() As Integer
        Dim oData As DataSet

        Comprobar_TipoPedido = Nothing

        oTipoPedido = DBServer.Comprobar_TipoPedido(iCodPedido, iIdTipoPedido, iEmp)
        If Not IsNothing(oTipoPedido) Then
            If oTipoPedido(1) <> 0 Then
                If oTipoPedido(0) <> 0 Then
                    Comprobar_TipoPedido = True
                Else
                    'Comprobar_TipoPedido = False
                    'mirar que fila no cumple
                    oData = DBServer.Linea_TipoPedido(iCodPedido, iIdTipoPedido, iEmp)
                    If Not IsNothing(oData) Then
                        If oData.Tables.Count <> 0 Then
                            If oData.Tables(0).Rows.Count <> 0 Then
                                For Each oRow As DataRow In oData.Tables(0).Rows
                                    Comprobar_TipoPedido = DBNullToInteger(oRow("LINEA"))
                                Next
                            Else
                                Comprobar_TipoPedido = True
                            End If
                        Else
                            Comprobar_TipoPedido = True
                        End If
                    Else
                        Comprobar_TipoPedido = True
                    End If
                End If
            Else
                Comprobar_TipoPedido = True
            End If
        Else
            'Comprobar_TipoPedido = False
            'mirar que fila no cumple
            oData = DBServer.Linea_TipoPedido(iCodPedido, iIdTipoPedido, iEmp)
            If Not IsNothing(oData) Then
                If oData.Tables.Count <> 0 Then
                    If oData.Tables(0).Rows.Count <> 0 Then
                        For Each oRow As DataRow In oData.Tables(0).Rows
                            Comprobar_TipoPedido = DBNullToInteger(oRow("LINEA"))
                        Next
                    Else
                        Comprobar_TipoPedido = True
                    End If
                Else
                    Comprobar_TipoPedido = True
                End If
            Else
                Comprobar_TipoPedido = True
            End If
        End If
    End Function
    ''' <summary>
    ''' Funcion que devuelve los atributos del pedido
    ''' </summary>
    ''' <param name="iCodPedido">iCodPedido</param>
    ''' <param name="iIdTipoPedido">iIdTipoPedido</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CargarAtributosPedido(ByVal iDesdeFavorito As Integer, ByVal iCodPedido As Integer, ByVal iIdTipoPedido As Integer, ByVal bHayIntegracion As Boolean,
                                          ByVal idEmpresa As Integer, ByVal EsPedidoAbierto As Boolean, ByVal iCodOrdenEmitir As Long, ByVal Usuario As String) As CAtributosPedido
        Authenticate()

        Dim oData As DataSet
        oData = DBServer.Cargar_Atributos_Pedido(iDesdeFavorito, iCodPedido, iIdTipoPedido, bHayIntegracion, idEmpresa, EsPedidoAbierto, iCodOrdenEmitir, Usuario)

        Dim oAtributos As CAtributosPedido
        oAtributos = New CAtributosPedido(mDBServer, mIsAuthenticated)
        oAtributos = CargarObjetoAtributosLisExterna(oData)

        Return oAtributos
    End Function
    ''' <summary>
    ''' Funcion que devuelve los atributos de integracion del pedido
    ''' </summary>
    ''' <param name="iCodPedido">iCodPedido</param>
    ''' <param name="iIdTipoPedido">iIdTipoPedido</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CargarAtributosIntegracionPedido(ByVal iCodPedido As Integer, ByVal iIdTipoPedido As Integer, ByVal bHayIntegracion As Boolean, ByVal idEmpresa As Integer, ByVal Usuario As String) As CAtributosPedido
        Authenticate()

        Dim oData As DataSet
        oData = DBServer.Cargar_Atributos_Integracion_Pedido(iCodPedido, iIdTipoPedido, bHayIntegracion, idEmpresa, Usuario)

        Dim oAtributos As CAtributosPedido
        oAtributos = New CAtributosPedido(mDBServer, mIsAuthenticated)
        oAtributos = CargarObjetoAtributosLisExterna(oData)

        Return oAtributos
    End Function
    ''' <summary>
    ''' Funcion que devuelve los costes del pedido
    ''' </summary>
    ''' <param name="iIdTipoPedido"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CargarAtributosCoste(ByVal iDesdeFavorito As Integer, ByVal iCodPedido As Integer, ByVal iIdTipoPedido As Integer, _
                                         Optional ByVal sIdioma As String = "SPA", Optional ByVal EsPedidoAbierto As Boolean = False) As CAtributosPedido
        Authenticate()

        Dim oData As DataSet
        Dim oDatos As DataSet
        oData = DBServer.Cargar_Atributos_Coste(iCodPedido, iIdTipoPedido, sIdioma, EsPedidoAbierto)

        Dim oAtributo As Fullstep.FSNServer.CAtributoPedido
        Dim oAtributos As CAtributosPedido
        Dim oValorAtrib As ValorAtrib
        oAtributos = New CAtributosPedido(mDBServer, mIsAuthenticated)
        If iDesdeFavorito = 0 Then
            Dim keycesta() As DataColumn = {oData.Tables(0).Columns("ID")}
            oData.Tables(0).PrimaryKey = keycesta
            Dim keyrelim() As DataColumn = {oData.Tables(1).Columns("IDATRIB")}
            oData.Relations.Add("REL_IMPUESTOS", keycesta, keyrelim, False)

            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim ind As Short = 0
            If Not IsNothing(oData) Then
                If oData.Tables.Count <> 0 Then
                    For Each oRow As DataRow In oData.Tables(0).Rows
                        oAtributo = New CAtributoPedido
                        oAtributo.Id = DBNullToInteger(oRow("ID"))
                        oAtributo.Codigo = DBNullToStr(oRow("COD"))
                        oAtributo.Denominacion = DBNullToStr(oRow("DEN"))
                        oAtributo.Tipo = oRow("TIPO")
                        oAtributo.Intro = DBNullToInteger(oRow("INTRO"))
                        oAtributo.Obligatorio = DBNullToBoolean(oRow("OBLIGATORIO"))
                        oAtributo.Interno = DBNullToBoolean(oRow("INTERNO"))
                        oAtributo.MostrarEnRecep = DBNullToBoolean(oRow("MOSTRARENRECEP"))

                        oAtributo.Operacion = DBNullToStr(oRow("OPERACION"))
                        oAtributo.TipoCosteDescuento = DBNullToInteger(oRow("TIPO_COSTE_DESCUENTO"))
                        oAtributo.GrupoCosteDescuento = DBNullToStr(oRow("GRUPO_COSTE_DESCUENTO"))
                        If DBNullToStr(oRow("SELEC")) = String.Empty Then
                            oAtributo.Seleccionado = False
                        Else
                            oAtributo.Seleccionado = True
                        End If
                        Select Case oAtributo.Tipo
                            Case 2
                                If Not IsDBNull(oRow("VALORMINNUM")) Then
                                    oAtributo.ValorMin = DBNullToStr(oRow("VALORMINNUM"))
                                Else
                                    oAtributo.ValorMin = System.DBNull.Value
                                End If
                                If Not IsDBNull(oRow("VALORMAXNUM")) Then
                                    oAtributo.ValorMax = DBNullToStr(oRow("VALORMAXNUM"))
                                Else
                                    oAtributo.ValorMax = System.DBNull.Value
                                End If
                            Case 3
                                If Not IsDBNull(oRow("VALORMINFEC")) Then
                                    oAtributo.ValorMin = DBNullToStr(oRow("VALORMINFEC"))
                                Else
                                    oAtributo.ValorMin = System.DBNull.Value
                                End If
                                If Not IsDBNull(oRow("VALORMAXFEC")) Then
                                    oAtributo.ValorMax = DBNullToStr(oRow("VALORMAXFEC"))
                                Else
                                    oAtributo.ValorMax = System.DBNull.Value
                                End If
                            Case Else
                                oAtributo.ValorMin = System.DBNull.Value
                                oAtributo.ValorMax = System.DBNull.Value
                        End Select


                        oDatos = DBServer.Cargar_Valores_Atributos_Pedido(oAtributo.Id)
                        If oAtributo.Intro = 1 Then
                            'Dim oChildRows() As DataRow = oRow.GetChildRows("RELACION_1")
                            Dim oValores As New List(Of ValorAtrib)
                            For Each oChildRow As DataRow In oDatos.Tables(0).Rows
                                oValorAtrib = New ValorAtrib
                                oValorAtrib.Valor = DBNullToInteger(oChildRow("VALOR_NUM_LISTA"))
                                oAtributo.Valor = DBNullToStr(oRow("VALOR_NUM"))
                                oValores.Add(oValorAtrib)
                                ind += 1
                            Next
                            oAtributo.Valores = oValores
                        Else
                            oAtributo.Valor = DBNullToDbl(oRow("VALOR_NUM"))
                        End If

                        For Each rImpuesto As DataRow In oRow.GetChildRows("REL_IMPUESTOS")
                            Dim oImpuesto As FSNServer.CImpuestos = New FSNServer.CImpuestos()
                            oImpuesto.Id = rImpuesto.Item("ID")
                            oImpuesto.Codigo = rImpuesto.Item("COD")
                            oImpuesto.Denominacion = rImpuesto.Item("DEN")
                            oImpuesto.Valor = rImpuesto.Item("VALOR")
                            oAtributo.Impuestos.Add(oImpuesto)
                        Next

                        oAtributos.Add(oAtributo)
                    Next
                End If
            End If
        End If
        Return oAtributos
    End Function
    ''' <summary>
    ''' Funcion que devuelve los costes del la linea
    ''' </summary>
    ''' <param name="iCodLinea">iCodLinea</param>
    ''' <param name="iIdTipoPedido"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CargarAtributosCosteLinea(ByVal iDesdeFavorito As Integer, ByVal iCodLinea As Integer, ByVal iIdTipoPedido As Integer, _
                                              Optional ByVal sIdioma As String = "SPA", Optional ByVal EsPedidoAbierto As Boolean = False) As CAtributosPedido
        Authenticate()

        Dim oData As DataSet
        Dim oDatos As DataSet
        Dim oImpuestos As DataSet
        oData = DBServer.Cargar_Atributos_Coste_Linea(iDesdeFavorito, iCodLinea, iIdTipoPedido, EsPedidoAbierto)

        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim oAtributos As CAtributosPedido
        Dim oAtributo As Fullstep.FSNServer.CAtributoPedido
        Dim oValorAtrib As ValorAtrib
        Dim ind As Short = 0
        oAtributos = New CAtributosPedido(mDBServer, mIsAuthenticated)
        If iDesdeFavorito = 0 Then
            If Not IsNothing(oData) Then
                If oData.Tables.Count <> 0 Then
                    For Each oRow As DataRow In oData.Tables(0).Rows
                        oAtributo = New CAtributoPedido
                        oAtributo.Id = DBNullToInteger(oRow("ID"))
                        oAtributo.Codigo = DBNullToStr(oRow("COD"))
                        oAtributo.Denominacion = DBNullToStr(oRow("DEN"))
                        oAtributo.Tipo = oRow("TIPO")
                        oAtributo.Intro = DBNullToInteger(oRow("INTRO"))
                        oAtributo.Obligatorio = DBNullToBoolean(oRow("OBLIGATORIO"))
                        oAtributo.Interno = DBNullToBoolean(oRow("INTERNO"))
                        oAtributo.MostrarEnRecep = DBNullToBoolean(oRow("MOSTRARENRECEP"))

                        oAtributo.Operacion = DBNullToStr(oRow("OPERACION"))
                        'oAtributo.TipoAtrib = DBNullToBoolean(oRow("TIPOATRIB"))
                        oAtributo.TipoCosteDescuento = DBNullToInteger(oRow("TIPO_COSTE_DESCUENTO"))
                        oAtributo.GrupoCosteDescuento = DBNullToStr(oRow("GRUPO_COSTE_DESCUENTO"))
                        If DBNullToStr(oRow("SELEC")) = String.Empty Then
                            oAtributo.Seleccionado = False
                        Else
                            oAtributo.Seleccionado = True
                        End If
                        Select Case oAtributo.Tipo
                            Case 2
                                If Not IsDBNull(oRow("VALORMINNUM")) Then
                                    oAtributo.ValorMin = DBNullToStr(oRow("VALORMINNUM"))
                                Else
                                    oAtributo.ValorMin = System.DBNull.Value
                                End If
                                If Not IsDBNull(oRow("VALORMAXNUM")) Then
                                    oAtributo.ValorMax = DBNullToStr(oRow("VALORMAXNUM"))
                                Else
                                    oAtributo.ValorMax = System.DBNull.Value
                                End If
                            Case 3
                                If Not IsDBNull(oRow("VALORMINFEC")) Then
                                    oAtributo.ValorMin = DBNullToStr(oRow("VALORMINFEC"))
                                Else
                                    oAtributo.ValorMin = System.DBNull.Value
                                End If
                                If Not IsDBNull(oRow("VALORMAXFEC")) Then
                                    oAtributo.ValorMax = DBNullToStr(oRow("VALORMAXFEC"))
                                Else
                                    oAtributo.ValorMax = System.DBNull.Value
                                End If
                            Case Else
                                oAtributo.ValorMin = System.DBNull.Value
                                oAtributo.ValorMax = System.DBNull.Value
                        End Select


                        oDatos = DBServer.Cargar_Valores_Atributos_Pedido(oAtributo.Id)
                        If oAtributo.Intro = 1 Then
                            'Dim oChildRows() As DataRow = oRow.GetChildRows("RELACION_1")
                            Dim oValores As New List(Of ValorAtrib)
                            For Each oChildRow As DataRow In oDatos.Tables(0).Rows
                                oValorAtrib = New ValorAtrib

                                oValorAtrib.Valor = DBNullToInteger(oChildRow("VALOR_NUM_LISTA"))
                                oAtributo.Valor = DBNullToStr(oRow("VALOR_NUM"))
                                oValores.Add(oValorAtrib)
                                ind += 1
                            Next
                            oAtributo.Valores = oValores
                        Else
                            oAtributo.Valor = DBNullToDbl(oRow("VALOR_NUM"))
                        End If

                        oImpuestos = DBServer.Cargar_Impuestos_Atributo(oAtributo.Id, sIdioma, iCodLinea, EsPedidoAbierto)

                        If oImpuestos.Tables.Count <> 0 Then
                            For Each rImpuesto As DataRow In oImpuestos.Tables(0).Rows
                                Dim oImpuesto As FSNServer.CImpuestos = New FSNServer.CImpuestos()
                                oImpuesto.Id = rImpuesto.Item("ID")
                                oImpuesto.Codigo = rImpuesto.Item("COD")
                                oImpuesto.Denominacion = rImpuesto.Item("DEN")
                                oImpuesto.Valor = rImpuesto.Item("VALOR")
                                oAtributo.Impuestos.Add(oImpuesto)
                            Next
                        End If
                        oAtributos.Add(oAtributo)
                    Next
                End If
            End If
        End If
        Return oAtributos
    End Function
    ''' <summary>
    ''' Funcion que devuelve los descuentos del pedido
    ''' </summary>
    ''' <param name="iIdTipoPedido"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CargarAtributosDescuento(ByVal iCodPedido As Integer, ByVal iIdTipoPedido As Integer, Optional ByVal EsPedidoAbierto As Boolean = False) As CAtributosPedido
        Authenticate()

        Dim oData As DataSet
        Dim oDatos As DataSet
        oData = DBServer.Cargar_Atributos_Descuento(iCodPedido, iIdTipoPedido, EsPedidoAbierto)

        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim oAtributos As CAtributosPedido
        Dim oAtributo As Fullstep.FSNServer.CAtributoPedido
        Dim oValorAtrib As ValorAtrib
        Dim ind As Short = 0
        oAtributos = New CAtributosPedido(mDBServer, mIsAuthenticated)
        If Not IsNothing(oData) Then
            If oData.Tables.Count <> 0 Then
                For Each oRow As DataRow In oData.Tables(0).Rows
                    oAtributo = New CAtributoPedido
                    oAtributo.Id = DBNullToInteger(oRow("ID"))
                    oAtributo.Codigo = DBNullToStr(oRow("COD"))
                    oAtributo.Denominacion = DBNullToStr(oRow("DEN"))
                    oAtributo.Tipo = oRow("TIPO")
                    oAtributo.Intro = DBNullToInteger(oRow("INTRO"))
                    oAtributo.Obligatorio = DBNullToBoolean(oRow("OBLIGATORIO"))
                    oAtributo.Interno = DBNullToBoolean(oRow("INTERNO"))
                    oAtributo.MostrarEnRecep = DBNullToBoolean(oRow("MOSTRARENRECEP"))

                    oAtributo.Operacion = DBNullToStr(oRow("OPERACION"))
                    'oAtributo.TipoAtrib = DBNullToBoolean(oRow("TIPOATRIB"))
                    oAtributo.TipoCosteDescuento = DBNullToInteger(oRow("TIPO_COSTE_DESCUENTO"))
                    oAtributo.GrupoCosteDescuento = DBNullToStr(oRow("GRUPO_COSTE_DESCUENTO"))
                    If DBNullToStr(oRow("SELEC")) = String.Empty Then
                        oAtributo.Seleccionado = False
                    Else
                        oAtributo.Seleccionado = True
                    End If
                    Select Case oAtributo.Tipo
                        Case 2
                            If Not IsDBNull(oRow("VALORMINNUM")) Then
                                oAtributo.ValorMin = DBNullToStr(oRow("VALORMINNUM"))
                            Else
                                oAtributo.ValorMin = System.DBNull.Value
                            End If
                            If Not IsDBNull(oRow("VALORMAXNUM")) Then
                                oAtributo.ValorMax = DBNullToStr(oRow("VALORMAXNUM"))
                            Else
                                oAtributo.ValorMax = System.DBNull.Value
                            End If
                        Case 3
                            If Not IsDBNull(oRow("VALORMINFEC")) Then
                                oAtributo.ValorMin = DBNullToStr(oRow("VALORMINFEC"))
                            Else
                                oAtributo.ValorMin = System.DBNull.Value
                            End If
                            If Not IsDBNull(oRow("VALORMAXFEC")) Then
                                oAtributo.ValorMax = DBNullToStr(oRow("VALORMAXFEC"))
                            Else
                                oAtributo.ValorMax = System.DBNull.Value
                            End If
                        Case Else
                            oAtributo.ValorMin = System.DBNull.Value
                            oAtributo.ValorMax = System.DBNull.Value
                    End Select


                    oDatos = DBServer.Cargar_Valores_Atributos_Pedido(oAtributo.Id)
                    If oAtributo.Intro = 1 Then
                        'Dim oChildRows() As DataRow = oRow.GetChildRows("RELACION_1")
                        Dim oValores As New List(Of ValorAtrib)
                        For Each oChildRow As DataRow In oDatos.Tables(0).Rows
                            oValorAtrib = New ValorAtrib

                            oValorAtrib.Valor = DBNullToInteger(oChildRow("VALOR_NUM_LISTA"))
                            oAtributo.Valor = DBNullToStr(oRow("VALOR_NUM"))
                            oValores.Add(oValorAtrib)
                            ind += 1
                        Next
                        oAtributo.Valores = oValores
                    Else
                        oAtributo.Valor = DBNullToDbl(oRow("VALOR_NUM"))
                    End If
                    oAtributos.Add(oAtributo)
                Next
            End If
        End If
        Return oAtributos
    End Function
    ''' <summary>
    ''' Funcion que devuelve los descuentos de la linea
    ''' </summary>
    ''' <param name="iIdTipoPedido"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CargarAtributosDescuentoLinea(ByVal iDesdeFavorito As Integer, ByVal iCodLinea As Integer, ByVal iIdTipoPedido As Integer, ByVal EsPedidoAbierto As Boolean) As CAtributosPedido
        Authenticate()

        Dim oData As DataSet
        Dim oDatos As DataSet
        oData = DBServer.Cargar_Atributos_Descuento_Linea(iDesdeFavorito, iCodLinea, iIdTipoPedido, EsPedidoAbierto)

        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim oAtributos As CAtributosPedido
        Dim oAtributo As Fullstep.FSNServer.CAtributoPedido
        Dim oValorAtrib As ValorAtrib
        Dim ind As Short = 0
        oAtributos = New CAtributosPedido(mDBServer, mIsAuthenticated)
        If Not IsNothing(oData) Then
            If oData.Tables.Count <> 0 Then
                For Each oRow As DataRow In oData.Tables(0).Rows
                    oAtributo = New CAtributoPedido
                    oAtributo.Id = DBNullToInteger(oRow("ID"))
                    oAtributo.Codigo = DBNullToStr(oRow("COD"))
                    oAtributo.Denominacion = DBNullToStr(oRow("DEN"))
                    oAtributo.Tipo = oRow("TIPO")
                    oAtributo.Intro = DBNullToInteger(oRow("INTRO"))
                    oAtributo.Obligatorio = DBNullToBoolean(oRow("OBLIGATORIO"))
                    oAtributo.Interno = DBNullToBoolean(oRow("INTERNO"))
                    oAtributo.MostrarEnRecep = DBNullToBoolean(oRow("MOSTRARENRECEP"))

                    oAtributo.Operacion = DBNullToStr(oRow("OPERACION"))
                    'oAtributo.TipoAtrib = DBNullToBoolean(oRow("TIPOATRIB"))
                    oAtributo.TipoCosteDescuento = DBNullToInteger(oRow("TIPO_COSTE_DESCUENTO"))
                    oAtributo.GrupoCosteDescuento = DBNullToStr(oRow("GRUPO_COSTE_DESCUENTO"))
                    If DBNullToStr(oRow("SELEC")) = String.Empty Then
                        oAtributo.Seleccionado = False
                    Else
                        oAtributo.Seleccionado = True
                    End If
                    Select Case oAtributo.Tipo
                        Case 2
                            If Not IsDBNull(oRow("VALORMINNUM")) Then
                                oAtributo.ValorMin = DBNullToStr(oRow("VALORMINNUM"))
                            Else
                                oAtributo.ValorMin = System.DBNull.Value
                            End If
                            If Not IsDBNull(oRow("VALORMAXNUM")) Then
                                oAtributo.ValorMax = DBNullToStr(oRow("VALORMAXNUM"))
                            Else
                                oAtributo.ValorMax = System.DBNull.Value
                            End If
                        Case 3
                            If Not IsDBNull(oRow("VALORMINFEC")) Then
                                oAtributo.ValorMin = DBNullToStr(oRow("VALORMINFEC"))
                            Else
                                oAtributo.ValorMin = System.DBNull.Value
                            End If
                            If Not IsDBNull(oRow("VALORMAXFEC")) Then
                                oAtributo.ValorMax = DBNullToStr(oRow("VALORMAXFEC"))
                            Else
                                oAtributo.ValorMax = System.DBNull.Value
                            End If
                        Case Else
                            oAtributo.ValorMin = System.DBNull.Value
                            oAtributo.ValorMax = System.DBNull.Value
                    End Select

                    oDatos = DBServer.Cargar_Valores_Atributos_Pedido(oAtributo.Id)
                    If oAtributo.Intro = 1 Then
                        'Dim oChildRows() As DataRow = oRow.GetChildRows("RELACION_1")
                        Dim oValores As New List(Of ValorAtrib)
                        For Each oChildRow As DataRow In oDatos.Tables(0).Rows
                            oValorAtrib = New ValorAtrib

                            oValorAtrib.Valor = DBNullToInteger(oChildRow("VALOR_NUM_LISTA"))
                            oAtributo.Valor = DBNullToStr(oRow("VALOR_NUM"))

                            oValores.Add(oValorAtrib)
                            ind += 1
                        Next
                        oAtributo.Valores = oValores
                    Else
                        oAtributo.Valor = DBNullToDbl(oRow("VALOR_NUM"))
                    End If
                    oAtributos.Add(oAtributo)
                Next
            End If
        End If
        Return oAtributos
    End Function
    ''' <summary>
    ''' Funcion que devuelve los atributos a nivel de proceso
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CargarAtributosLineaPedido(ByVal iDesdeFavorito As Integer, ByVal iIdTipoPedido As Integer, ByVal iCodLinea As Integer, ByVal bHayIntegracion As Boolean,
                                               ByVal idEmpresa As Integer, ByVal EsPedidoAbierto As Boolean, ByVal iCodLineaEmitir As Long, ByVal Usuario As String) As CAtributosPedido
        Authenticate()

        Dim oData As DataSet
        oData = DBServer.Cargar_Atributos_Linea_Pedido(iDesdeFavorito, iIdTipoPedido, iCodLinea, bHayIntegracion, idEmpresa, EsPedidoAbierto, iCodLineaEmitir, Usuario)

        Dim oAtributos As CAtributosPedido
        oAtributos = New CAtributosPedido(mDBServer, mIsAuthenticated)
        oAtributos = CargarObjetoAtributosLisExterna(oData)

        Return oAtributos
    End Function
    ''' <summary>
    ''' Funcion que devuelve los atributos de integracion a nivel de proceso
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CargarAtributosIntegracionLineaPedido(ByVal iDesdeFavorito As Integer, ByVal iIdTipoPedido As Integer, ByVal iCodLinea As Integer, ByVal bHayIntegracion As Boolean, ByVal idEmpresa As Integer, ByVal Usuario As String) As CAtributosPedido
        Authenticate()

        Dim oData As DataSet
        oData = DBServer.Cargar_Atributos_Integracion_Linea_Pedido(iDesdeFavorito, iIdTipoPedido, iCodLinea, bHayIntegracion, idEmpresa, Usuario)

        Dim oAtributos As CAtributosPedido
        oAtributos = New CAtributosPedido(mDBServer, mIsAuthenticated)
        oAtributos = CargarObjetoAtributosLisExterna(oData)

        Return oAtributos
    End Function
    ''' <summary>
    ''' Funcion que devuelve los costes de la linea de un pedido
    ''' </summary>
    ''' <param name="iCodLinea">iCodLinea</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CargarAtributosCosteLineaOrden(ByVal iCodLinea As Integer, Optional ByVal sIdioma As String = "SPA") As CAtributosPedido
        Authenticate()

        Dim oData As DataSet
        Dim oImpuestos As DataSet
        oData = DBServer.Cargar_Atributos_Coste_Linea_Orden(iCodLinea)

        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim oAtributos As CAtributosPedido
        Dim oAtributo As Fullstep.FSNServer.CAtributoPedido
        Dim ind As Short = 0
        oAtributos = New CAtributosPedido(mDBServer, mIsAuthenticated)
        If Not IsNothing(oData) Then
            If oData.Tables.Count <> 0 Then
                For Each oRow As DataRow In oData.Tables(0).Rows
                    oAtributo = New CAtributoPedido
                    oAtributo.Id = DBNullToInteger(oRow("ID"))
                    oAtributo.Codigo = DBNullToStr(oRow("COD"))
                    oAtributo.Denominacion = DBNullToStr(oRow("DEN"))
                    oAtributo.Tipo = oRow("TIPO")
                    oAtributo.Intro = DBNullToInteger(oRow("INTRO"))
                    oAtributo.Obligatorio = DBNullToBoolean(oRow("OBLIGATORIO"))
                    oAtributo.Importe = DBNullToStr(oRow("IMPORTE"))
                    oAtributo.ImporteBD = DBNullToStr(oRow("IMPORTE"))
                    oAtributo.Valor = DBNullToStr(oRow("VALOR"))
                    oAtributo.Operacion = DBNullToStr(oRow("OPERACION"))
                    oAtributo.Seleccionado = True

                    Select Case oAtributo.Tipo
                        Case 2
                            If Not IsDBNull(oRow("VALORMINNUM")) Then
                                oAtributo.ValorMin = DBNullToStr(oRow("VALORMINNUM"))
                            Else
                                oAtributo.ValorMin = System.DBNull.Value
                            End If
                            If Not IsDBNull(oRow("VALORMAXNUM")) Then
                                oAtributo.ValorMax = DBNullToStr(oRow("VALORMAXNUM"))
                            Else
                                oAtributo.ValorMax = System.DBNull.Value
                            End If
                        Case 3
                            If Not IsDBNull(oRow("VALORMINFEC")) Then
                                oAtributo.ValorMin = DBNullToStr(oRow("VALORMINFEC"))
                            Else
                                oAtributo.ValorMin = System.DBNull.Value
                            End If
                            If Not IsDBNull(oRow("VALORMAXFEC")) Then
                                oAtributo.ValorMax = DBNullToStr(oRow("VALORMAXFEC"))
                            Else
                                oAtributo.ValorMax = System.DBNull.Value
                            End If
                        Case Else
                            oAtributo.ValorMin = System.DBNull.Value
                            oAtributo.ValorMax = System.DBNull.Value
                    End Select

                    oImpuestos = DBServer.Cargar_Impuestos_Atributo_Linea_Orden(oAtributo.Id, sIdioma, iCodLinea)

                    If oImpuestos.Tables.Count <> 0 Then
                        For Each rImpuesto As DataRow In oImpuestos.Tables(0).Rows
                            Dim oImpuesto As FSNServer.CImpuestos = New FSNServer.CImpuestos()
                            oImpuesto.Id = rImpuesto.Item("ID")
                            oImpuesto.Codigo = rImpuesto.Item("COD")
                            oImpuesto.Denominacion = rImpuesto.Item("DEN")
                            oImpuesto.Valor = rImpuesto.Item("VALOR")
                            oAtributo.Impuestos.Add(oImpuesto)
                        Next
                    End If
                    oAtributos.Add(oAtributo)
                Next
            End If
        End If
        Return oAtributos
    End Function
    ''' <summary>
    ''' Funcion que devuelve los descuentos de la orden de entrega
    ''' </summary>
    ''' <param name="IdOrden">id de la orden de entrega</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CargarAtributosDescuentoOrden(ByVal IdOrden As Long) As CAtributosPedido
        Authenticate()

        Dim oData As DataSet
        Dim oDatos As DataSet
        oData = DBServer.Cargar_Atributos_Descuento_Orden(IdOrden)

        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim oAtributos As CAtributosPedido
        Dim oAtributo As Fullstep.FSNServer.CAtributoPedido
        Dim oValorAtrib As ValorAtrib
        Dim ind As Short = 0
        oAtributos = New CAtributosPedido(mDBServer, mIsAuthenticated)
        If Not IsNothing(oData) Then
            If oData.Tables.Count <> 0 Then
                For Each oRow As DataRow In oData.Tables(0).Rows
                    oAtributo = New CAtributoPedido
                    oAtributo.Id = DBNullToInteger(oRow("ID"))
                    oAtributo.Codigo = DBNullToStr(oRow("COD"))
                    oAtributo.Denominacion = DBNullToStr(oRow("DEN"))
                    oAtributo.Tipo = oRow("TIPO")
                    oAtributo.Intro = DBNullToInteger(oRow("INTRO"))
                    oAtributo.Obligatorio = DBNullToBoolean(oRow("OBLIGATORIO"))
                    oAtributo.Operacion = DBNullToStr(oRow("OPERACION"))
                    oAtributo.ImporteBD = DBNullToStr(oRow("IMPORTE"))
                    If DBNullToStr(oRow("SELEC")) = String.Empty Then
                        oAtributo.Seleccionado = False
                    Else
                        oAtributo.Seleccionado = True
                    End If
                    Select Case oAtributo.Tipo
                        Case 2
                            If Not IsDBNull(oRow("VALORMINNUM")) Then
                                oAtributo.ValorMin = DBNullToStr(oRow("VALORMINNUM"))
                            Else
                                oAtributo.ValorMin = System.DBNull.Value
                            End If
                            If Not IsDBNull(oRow("VALORMAXNUM")) Then
                                oAtributo.ValorMax = DBNullToStr(oRow("VALORMAXNUM"))
                            Else
                                oAtributo.ValorMax = System.DBNull.Value
                            End If
                        Case 3
                            If Not IsDBNull(oRow("VALORMINFEC")) Then
                                oAtributo.ValorMin = DBNullToStr(oRow("VALORMINFEC"))
                            Else
                                oAtributo.ValorMin = System.DBNull.Value
                            End If
                            If Not IsDBNull(oRow("VALORMAXFEC")) Then
                                oAtributo.ValorMax = DBNullToStr(oRow("VALORMAXFEC"))
                            Else
                                oAtributo.ValorMax = System.DBNull.Value
                            End If
                        Case Else
                            oAtributo.ValorMin = System.DBNull.Value
                            oAtributo.ValorMax = System.DBNull.Value
                    End Select


                    oDatos = DBServer.Cargar_Valores_Atributos_Orden(oAtributo.Id, IdOrden)
                    If oAtributo.Intro = 1 Then
                        Dim oValores As New List(Of ValorAtrib)
                        For Each oChildRow As DataRow In oDatos.Tables(0).Rows
                            oValorAtrib = New ValorAtrib

                            oValorAtrib.Valor = DBNullToInteger(oChildRow("VALOR_NUM_LISTA"))
                            oAtributo.Valor = DBNullToStr(oRow("VALOR_NUM"))
                            oValores.Add(oValorAtrib)
                            ind += 1
                        Next
                        oAtributo.Valores = oValores
                    Else
                        oAtributo.Valor = DBNullToDbl(oRow("VALOR"))
                    End If
                    oAtributos.Add(oAtributo)
                Next
            End If
        End If
        Return oAtributos
    End Function
    ''' <summary>
    ''' Funcion que devuelve los descuentos de la linea
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CargarAtributosDescuentoLineaOrden(ByVal iCodLinea As Integer) As CAtributosPedido
        Authenticate()

        Dim oData As DataSet
        oData = DBServer.Cargar_Atributos_Descuento_Linea_Orden(iCodLinea)

        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim oAtributos As CAtributosPedido
        Dim oAtributo As Fullstep.FSNServer.CAtributoPedido
        Dim ind As Short = 0
        oAtributos = New CAtributosPedido(mDBServer, mIsAuthenticated)
        If Not IsNothing(oData) Then
            If oData.Tables.Count <> 0 Then
                For Each oRow As DataRow In oData.Tables(0).Rows
                    oAtributo = New CAtributoPedido
                    oAtributo.Id = DBNullToInteger(oRow("ID"))
                    oAtributo.Codigo = DBNullToStr(oRow("COD"))
                    oAtributo.Denominacion = DBNullToStr(oRow("DEN"))
                    oAtributo.Tipo = oRow("TIPO")
                    oAtributo.Intro = DBNullToInteger(oRow("INTRO"))
                    oAtributo.Obligatorio = DBNullToBoolean(oRow("OBLIGATORIO"))
                    oAtributo.Importe = DBNullToStr(oRow("IMPORTE"))
                    oAtributo.ImporteBD = DBNullToStr(oRow("IMPORTE"))
                    oAtributo.Valor = DBNullToStr(oRow("VALOR"))
                    oAtributo.Operacion = DBNullToStr(oRow("OPERACION"))
                    oAtributo.Seleccionado = True

                    Select Case oAtributo.Tipo
                        Case 2
                            If Not IsDBNull(oRow("VALORMINNUM")) Then
                                oAtributo.ValorMin = DBNullToStr(oRow("VALORMINNUM"))
                            Else
                                oAtributo.ValorMin = System.DBNull.Value
                            End If
                            If Not IsDBNull(oRow("VALORMAXNUM")) Then
                                oAtributo.ValorMax = DBNullToStr(oRow("VALORMAXNUM"))
                            Else
                                oAtributo.ValorMax = System.DBNull.Value
                            End If
                        Case 3
                            If Not IsDBNull(oRow("VALORMINFEC")) Then
                                oAtributo.ValorMin = DBNullToStr(oRow("VALORMINFEC"))
                            Else
                                oAtributo.ValorMin = System.DBNull.Value
                            End If
                            If Not IsDBNull(oRow("VALORMAXFEC")) Then
                                oAtributo.ValorMax = DBNullToStr(oRow("VALORMAXFEC"))
                            Else
                                oAtributo.ValorMax = System.DBNull.Value
                            End If
                        Case Else
                            oAtributo.ValorMin = System.DBNull.Value
                            oAtributo.ValorMax = System.DBNull.Value
                    End Select
                    oAtributos.Add(oAtributo)
                Next
            End If
        End If
        Return oAtributos
    End Function
    ''' <summary>
    ''' Funcion que devuelve los atributos(campos de pedido) de la linea de pedido
    ''' </summary>
    ''' <param name="iCodLinea">id de la linea de pedido</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CargarAtributosLineaOrden(ByVal iCodLinea As Integer, Optional ByVal bConInternos As Boolean = True) As CAtributosPedido
        Authenticate()

        Dim oData As DataSet
        oData = DBServer.Cargar_Atributos_Linea_Orden(iCodLinea, bConInternos)

        Dim oAtributos As CAtributosPedido
        Dim oAtributo As Fullstep.FSNServer.CAtributoPedido
        Dim oValorAtrib As ValorAtrib
        Dim ind As Short = 0
        oAtributos = New CAtributosPedido(mDBServer, mIsAuthenticated)
        If Not IsNothing(oData) Then
            If oData.Tables.Count <> 0 Then
                For Each oRow As DataRow In oData.Tables(0).Rows
                    oAtributo = New CAtributoPedido
                    oAtributo.Id = DBNullToInteger(oRow("ID"))
                    oAtributo.Codigo = DBNullToStr(oRow("COD"))
                    oAtributo.Denominacion = DBNullToStr(oRow("DEN"))
                    oAtributo.Tipo = oRow("TIPO")
                    oAtributo.Intro = DBNullToInteger(oRow("INTRO"))
                    oAtributo.Obligatorio = DBNullToBoolean(oRow("OBLIGATORIO"))
                    oAtributo.ListaExterna = DBNullToBoolean(oRow("LISTA_EXTERNA"))
                    Select Case oAtributo.Tipo
                        Case 2
                            If Not IsDBNull(oRow("VALORMINNUM")) Then
                                oAtributo.ValorMin = DBNullToDbl(oRow("VALORMINNUM"))
                            Else
                                oAtributo.ValorMin = System.DBNull.Value
                            End If
                            If Not IsDBNull(oRow("VALORMAXNUM")) Then
                                oAtributo.ValorMax = DBNullToDbl(oRow("VALORMAXNUM"))
                            Else
                                oAtributo.ValorMax = System.DBNull.Value
                            End If
                        Case 3
                            If Not IsDBNull(oRow("VALORMINFEC")) Then
                                oAtributo.ValorMin = DBNullToStr(oRow("VALORMINFEC"))
                            Else
                                oAtributo.ValorMin = System.DBNull.Value
                            End If
                            If Not IsDBNull(oRow("VALORMAXFEC")) Then
                                oAtributo.ValorMax = DBNullToStr(oRow("VALORMAXFEC"))
                            Else
                                oAtributo.ValorMax = System.DBNull.Value
                            End If
                        Case Else
                            oAtributo.ValorMin = System.DBNull.Value
                            oAtributo.ValorMax = System.DBNull.Value
                    End Select

                    Select Case oAtributo.Tipo
                        Case 1
                            If oRow("VALTEXT") Is DBNull.Value Then
                                oAtributo.Valor = DBNullToStr(oRow("VALOR_TEXT"))
                            Else
                                oAtributo.Valor = DBNullToStr(oRow("VALTEXT"))
                            End If
                        Case 2
                            If oRow("VALNUM") Is DBNull.Value Then
                                oAtributo.Valor = DBNullToDbl(oRow("VALOR_NUM"))
                            Else
                                oAtributo.Valor = DBNullToDbl(oRow("VALNUM"))
                            End If
                        Case 3
                            If oRow("VALFEC") Is DBNull.Value Then
                                If oRow("VALOR_FEC") Is DBNull.Value Then
                                    oAtributo.Valor = DBNull.Value
                                Else
                                    oAtributo.Valor = CDate(oRow("VALOR_FEC"))
                                End If
                            Else
                                oAtributo.Valor = CDate(oRow("VALFEC"))
                            End If
                        Case 4
                            If oRow("VALBOOL") Is DBNull.Value Then
                                oAtributo.Valor = DBNullToStr(oRow("VALOR_BOOL"))
                            Else
                                oAtributo.Valor = DBNullToStr(oRow("VALBOOL"))
                            End If
                    End Select
                    If oAtributo.ListaExterna Then
                        oAtributo.Valor = oRow("VALTEXT").ToString
                        Dim oValores As New List(Of ValorAtrib)
                        If Not String.IsNullOrEmpty(oAtributo.Valor) Then
                            oValorAtrib = New ValorAtrib
                            oValorAtrib.Valor = oRow("VALOR_COD").ToString
                            oValores.Add(oValorAtrib)
                        End If
                        oAtributo.Valores = oValores
                    End If
                    oAtributos.Add(oAtributo)
                Next
            End If
        End If
        Return oAtributos
    End Function
    ''' <summary>
    ''' Funcion que devuelve los atributos de la orden
    ''' </summary>
    ''' <param name="iIdOrden">id de la orden de entrega</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CargarAtributosOrden(ByVal iIdOrden As Integer, Optional ByVal bConInternos As Boolean = True) As CAtributosPedido
        Authenticate()

        Dim oData As DataSet
        Dim oDatos As DataSet
        oData = DBServer.Cargar_Atributos_Orden(iIdOrden, bConInternos)

        Dim oAtributos As CAtributosPedido
        Dim oAtributo As Fullstep.FSNServer.CAtributoPedido
        Dim oValorAtrib As ValorAtrib
        Dim ind As Short = 0
        oAtributos = New CAtributosPedido(mDBServer, mIsAuthenticated)
        If Not IsNothing(oData) Then
            If oData.Tables.Count <> 0 Then
                For Each oRow As DataRow In oData.Tables(0).Rows
                    oAtributo = New CAtributoPedido
                    oAtributo.Id = DBNullToInteger(oRow("ID"))
                    oAtributo.Codigo = DBNullToStr(oRow("COD"))
                    oAtributo.Denominacion = DBNullToStr(oRow("DEN"))
                    oAtributo.Tipo = oRow("TIPO")
                    oAtributo.Intro = DBNullToInteger(oRow("INTRO"))
                    oAtributo.Obligatorio = DBNullToBoolean(oRow("OBLIGATORIO"))
                    oAtributo.Interno = DBNullToBoolean(oRow("INTERNO"))
                    oAtributo.ListaExterna = DBNullToBoolean(oRow("LISTA_EXTERNA"))
                    Select Case oAtributo.Tipo
                        Case 2
                            If Not IsDBNull(oRow("VALORMINNUM")) Then
                                oAtributo.ValorMin = DBNullToDbl(oRow("VALORMINNUM"))
                            Else
                                oAtributo.ValorMin = System.DBNull.Value
                            End If
                            If Not IsDBNull(oRow("VALORMAXNUM")) Then
                                oAtributo.ValorMax = DBNullToDbl(oRow("VALORMAXNUM"))
                            Else
                                oAtributo.ValorMax = System.DBNull.Value
                            End If
                        Case 3
                            If Not IsDBNull(oRow("VALORMINFEC")) Then
                                oAtributo.ValorMin = DBNullToDbl(oRow("VALORMINFEC"))
                            Else
                                oAtributo.ValorMin = System.DBNull.Value
                            End If
                            If Not IsDBNull(oRow("VALORMAXFEC")) Then
                                oAtributo.ValorMax = DBNullToDbl(oRow("VALORMAXFEC"))
                            Else
                                oAtributo.ValorMax = System.DBNull.Value
                            End If
                        Case Else
                            oAtributo.ValorMin = System.DBNull.Value
                            oAtributo.ValorMax = System.DBNull.Value
                    End Select

                    oDatos = DBServer.Cargar_Valores_Atributos_Orden(oAtributo.Id, iIdOrden)
                    If oAtributo.Intro = 1 AndAlso Not oAtributo.ListaExterna Then
                        Dim oValores As New List(Of ValorAtrib)
                        For Each oChildRow As DataRow In oDatos.Tables(0).Rows
                            oValorAtrib = New ValorAtrib
                            Select Case oAtributo.Tipo
                                Case 1
                                    oValorAtrib.Valor = DBNullToStr(oChildRow("VALOR_TEXT_LISTA"))
                                    If oRow("VALTEXT") Is DBNull.Value Then
                                        oAtributo.Valor = DBNullToStr(oRow("VALOR_TEXT"))
                                    Else
                                        oAtributo.Valor = DBNullToStr(oRow("VALTEXT"))
                                    End If
                                Case 2
                                    oValorAtrib.Valor = DBNullToDbl(oChildRow("VALOR_NUM_LISTA"))
                                    If oRow("VALNUM") Is DBNull.Value Then
                                        oAtributo.Valor = DBNullToDbl(oRow("VALOR_NUM"))
                                    Else
                                        oAtributo.Valor = DBNullToDbl(oRow("VALNUM"))
                                    End If
                                Case 3
                                    If oChildRow("VALOR_FEC_LISTA") Is DBNull.Value Then
                                        oValorAtrib.Valor = DBNull.Value
                                    Else
                                        oValorAtrib.Valor = CDate(oChildRow("VALOR_FEC_LISTA"))
                                    End If
                                    If oRow("VALFEC") Is DBNull.Value Then
                                        If oRow("VALOR_FEC") Is DBNull.Value Then
                                            oAtributo.Valor = DBNull.Value
                                        Else
                                            oAtributo.Valor = CDate(oRow("VALOR_FEC"))
                                        End If
                                    Else
                                        oAtributo.Valor = CDate(oRow("VALFEC"))
                                    End If
                            End Select
                            oValores.Add(oValorAtrib)
                            ind += 1
                        Next
                        oAtributo.Valores = oValores
                    Else
                        If oAtributo.ListaExterna Then
                            oAtributo.Valor = oRow("VALOR_COD").ToString
                            Dim oValores As New List(Of ValorAtrib)
                            If Not String.IsNullOrEmpty(oAtributo.Valor) Then
                                oValorAtrib = New ValorAtrib
                                oValorAtrib.Valor = IIf(String.IsNullOrEmpty(oRow("VALTEXT").ToString), oRow("VALOR_TEXT").ToString, oRow("VALTEXT").ToString)
                                oValores.Add(oValorAtrib)
                            End If
                            oAtributo.Valores = oValores
                        Else
                            Select Case oAtributo.Tipo
                                Case 1
                                    If oRow("VALTEXT") Is DBNull.Value Then
                                        oAtributo.Valor = DBNullToStr(oRow("VALOR_TEXT"))
                                    Else
                                        oAtributo.Valor = DBNullToStr(oRow("VALTEXT"))
                                    End If


                                Case 2
                                    If oRow("VALNUM") Is DBNull.Value Then
                                        oAtributo.Valor = DBNullToDbl(oRow("VALOR_NUM"))
                                    Else
                                        oAtributo.Valor = DBNullToDbl(oRow("VALNUM"))
                                    End If

                                Case 3
                                    If oRow("VALFEC") Is DBNull.Value Then
                                        If oRow("VALOR_FEC") Is DBNull.Value Then
                                            oAtributo.Valor = DBNull.Value
                                        Else
                                            oAtributo.Valor = CDate(oRow("VALOR_FEC"))
                                        End If
                                    Else
                                        oAtributo.Valor = CDate(oRow("VALFEC"))
                                    End If
                                Case 4
                                    If oRow("VALBOOL") Is DBNull.Value Then
                                        oAtributo.Valor = DBNullToStr(oRow("VALOR_BOOL"))
                                    Else
                                        oAtributo.Valor = DBNullToStr(oRow("VALBOOL"))
                                    End If
                            End Select
                        End If
                    End If
                    oAtributos.Add(oAtributo)
                Next
            End If
        End If
        Return oAtributos
    End Function
    ''' <summary>
    ''' Funcion que devuelve los costes del pedido
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CargarAtributosCosteOrden(ByVal idOrden As Long, Optional ByVal sIdioma As String = "SPA") As CAtributosPedido
        Authenticate()

        Dim oData As DataSet
        Dim oDatos As DataSet
        oData = DBServer.Cargar_Atributos_Coste_Orden(idOrden, sIdioma)

        Dim keyOrden() As DataColumn = {oData.Tables(0).Columns("ID")}
        oData.Tables(0).PrimaryKey = keyOrden
        Dim keyrelim() As DataColumn = {oData.Tables(1).Columns("IDATRIB")}
        oData.Relations.Add("REL_IMPUESTOS", keyOrden, keyrelim, False)

        Dim oAtributos As CAtributosPedido
        Dim oAtributo As Fullstep.FSNServer.CAtributoPedido
        Dim oValorAtrib As ValorAtrib
        Dim ind As Short = 0
        oAtributos = New CAtributosPedido(mDBServer, mIsAuthenticated)
        If Not IsNothing(oData) Then
            If oData.Tables.Count <> 0 Then
                For Each oRow As DataRow In oData.Tables(0).Rows
                    oAtributo = New CAtributoPedido
                    oAtributo.Id = DBNullToInteger(oRow("ID"))
                    oAtributo.Codigo = DBNullToStr(oRow("COD"))
                    oAtributo.Denominacion = DBNullToStr(oRow("DEN"))
                    oAtributo.Tipo = oRow("TIPO")
                    oAtributo.Intro = DBNullToInteger(oRow("INTRO"))
                    oAtributo.Obligatorio = DBNullToBoolean(oRow("OBLIGATORIO"))
                    oAtributo.ImporteBD = DBNullToStr(oRow("IMPORTE"))
                    oAtributo.Operacion = DBNullToStr(oRow("OPERACION"))
                    If DBNullToStr(oRow("SELEC")) = String.Empty Then
                        oAtributo.Seleccionado = False
                    Else
                        oAtributo.Seleccionado = True
                    End If
                    Select Case oAtributo.Tipo
                        Case 2
                            If Not IsDBNull(oRow("VALORMINNUM")) Then
                                oAtributo.ValorMin = DBNullToStr(oRow("VALORMINNUM"))
                            Else
                                oAtributo.ValorMin = System.DBNull.Value
                            End If
                            If Not IsDBNull(oRow("VALORMAXNUM")) Then
                                oAtributo.ValorMax = DBNullToStr(oRow("VALORMAXNUM"))
                            Else
                                oAtributo.ValorMax = System.DBNull.Value
                            End If
                        Case 3
                            If Not IsDBNull(oRow("VALORMINFEC")) Then
                                oAtributo.ValorMin = DBNullToStr(oRow("VALORMINFEC"))
                            Else
                                oAtributo.ValorMin = System.DBNull.Value
                            End If
                            If Not IsDBNull(oRow("VALORMAXFEC")) Then
                                oAtributo.ValorMax = DBNullToStr(oRow("VALORMAXFEC"))
                            Else
                                oAtributo.ValorMax = System.DBNull.Value
                            End If
                        Case Else
                            oAtributo.ValorMin = System.DBNull.Value
                            oAtributo.ValorMax = System.DBNull.Value
                    End Select

                    oDatos = DBServer.Cargar_Valores_Atributos_Coste_Orden(oRow("IDCOSTE"), oAtributo.Id)
                    If oAtributo.Intro = 1 Then
                        Dim oValores As New List(Of ValorAtrib)
                        For Each oChildRow As DataRow In oDatos.Tables(0).Rows
                            oValorAtrib = New ValorAtrib
                            oValorAtrib.Valor = DBNullToInteger(oChildRow("VALOR_NUM_LISTA"))
                            oValores.Add(oValorAtrib)
                            ind += 1
                        Next
                        oAtributo.Valores = oValores
                    End If
                    oAtributo.Valor = DBNullToDbl(oRow("VALOR"))

                    For Each rImpuesto As DataRow In oRow.GetChildRows("REL_IMPUESTOS")
                        Dim oImpuesto As FSNServer.CImpuestos = New FSNServer.CImpuestos()
                        oImpuesto.Id = rImpuesto.Item("ID")
                        oImpuesto.Codigo = rImpuesto.Item("COD")
                        oImpuesto.Denominacion = rImpuesto.Item("DEN")
                        oImpuesto.Valor = rImpuesto.Item("VALOR")
                        oAtributo.Impuestos.Add(oImpuesto)
                    Next
                    oAtributos.Add(oAtributo)
                Next
            End If
        End If
        Return oAtributos
    End Function
    ''' <summary>
    ''' Funci�n que devuelve los atributos del pedido relacionados con el tipo de pedido
    ''' </summary>
    ''' <param name="iCodPedido">i Cod Pedido</param>
    ''' <param name="iIdTipoPedido">Id Pedido</param>
    ''' <param name="bHayIntegracion">Si hay integraci�n</param>
    ''' <param name="idEmpresa">Id Empresa</param> 
    ''' <returns>Atributos</returns>
    Public Function CargarAtributosTipoPedido(ByVal iCodPedido As Integer, ByVal iIdTipoPedido As Integer, ByVal bHayIntegracion As Boolean, ByVal idEmpresa As Integer, ByVal Usuario As String) As CAtributosPedido
        Authenticate()

        Dim oData As DataSet
        oData = DBServer.Cargar_Atributos_TipoPedido(iCodPedido, iIdTipoPedido, bHayIntegracion, idEmpresa, Usuario)

        Dim oAtributos As CAtributosPedido
        oAtributos = New CAtributosPedido(mDBServer, mIsAuthenticated)
        oAtributos = CargarObjetoAtributosLisExterna(oData)

        Return oAtributos
    End Function
    Public Function CargarObjetoAtributosLisExterna(ByVal oData As DataSet) As CAtributosPedido
        Authenticate()

        Dim oDatos As DataSet
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim oAtributos As CAtributosPedido
        Dim oAtributo As Fullstep.FSNServer.CAtributoPedido
        Dim oValorAtrib As ValorAtrib
        Dim ind As Short = 0
        oAtributos = New CAtributosPedido(mDBServer, mIsAuthenticated)
        If Not IsNothing(oData) Then
            If oData.Tables.Count <> 0 Then
                For Each oRow As DataRow In oData.Tables(0).Rows
                    oAtributo = New CAtributoPedido
                    oAtributo.Id = DBNullToInteger(oRow("ID"))
                    oAtributo.Codigo = DBNullToStr(oRow("COD"))
                    oAtributo.Denominacion = DBNullToStr(oRow("DEN"))
                    oAtributo.Tipo = oRow("TIPO")
                    oAtributo.Intro = DBNullToInteger(oRow("INTRO"))
                    oAtributo.Obligatorio = DBNullToBoolean(oRow("OBLIGATORIO"))
                    oAtributo.Interno = DBNullToBoolean(oRow("INTERNO"))
                    oAtributo.Integracion = DBNullToBoolean(oRow("INTEGRACION"))
                    If oData.Tables(0).Columns.Contains("TIPOPED") Then
                        oAtributo.TipoPed = DBNullToBoolean(oRow("TIPOPED"))
                    Else
                        oAtributo.TipoPed = False
                    End If
                    oAtributo.ListaExterna = DBNullToBoolean(oRow("LISTA_EXTERNA"))
                    oAtributo.MostrarEnRecep = DBNullToBoolean(oRow("MOSTRARENRECEP"))

                    Select Case oAtributo.Tipo
                        Case 2
                            If Not IsDBNull(oRow("VALORMINNUM")) Then
                                oAtributo.ValorMin = DBNullToDbl(oRow("VALORMINNUM"))
                            Else
                                oAtributo.ValorMin = System.DBNull.Value
                            End If
                            If Not IsDBNull(oRow("VALORMAXNUM")) Then
                                oAtributo.ValorMax = DBNullToDbl(oRow("VALORMAXNUM"))
                            Else
                                oAtributo.ValorMax = System.DBNull.Value
                            End If
                        Case 3
                            If Not IsDBNull(oRow("VALORMINFEC")) Then
                                oAtributo.ValorMin = DBNullToDbl(oRow("VALORMINFEC"))
                            Else
                                oAtributo.ValorMin = System.DBNull.Value
                            End If
                            If Not IsDBNull(oRow("VALORMAXFEC")) Then
                                oAtributo.ValorMax = DBNullToDbl(oRow("VALORMAXFEC"))
                            Else
                                oAtributo.ValorMax = System.DBNull.Value
                            End If
                        Case Else
                            oAtributo.ValorMin = System.DBNull.Value
                            oAtributo.ValorMax = System.DBNull.Value
                    End Select

                    oDatos = DBServer.Cargar_Valores_Atributos_Pedido(oAtributo.Id)
                    If oAtributo.Intro = 1 AndAlso Not oAtributo.ListaExterna Then
                        Dim oValores As New List(Of ValorAtrib)
                        For Each oChildRow As DataRow In oDatos.Tables(0).Rows
                            oValorAtrib = New ValorAtrib
                            Select Case oAtributo.Tipo
                                Case 1
                                    oValorAtrib.Valor = DBNullToStr(oChildRow("VALOR_TEXT_LISTA"))
                                    If oRow("VALTEXT") Is DBNull.Value Then
                                        oAtributo.Valor = DBNullToStr(oRow("VALOR_TEXT"))
                                    Else
                                        oAtributo.Valor = DBNullToStr(oRow("VALTEXT"))
                                    End If
                                Case 2
                                    oValorAtrib.Valor = DBNullToDbl(oChildRow("VALOR_NUM_LISTA"))
                                    If oRow("VALNUM") Is DBNull.Value Then
                                        oAtributo.Valor = DBNullToDbl(oRow("VALOR_NUM"))
                                    Else
                                        oAtributo.Valor = DBNullToDbl(oRow("VALNUM"))
                                    End If
                                Case 3
                                    If oChildRow("VALOR_FEC_LISTA") Is DBNull.Value Then
                                        oValorAtrib.Valor = DBNull.Value
                                    Else
                                        oValorAtrib.Valor = CDate(oChildRow("VALOR_FEC_LISTA"))
                                    End If
                                    If oRow("VALFEC") Is DBNull.Value Then
                                        If oRow("VALOR_FEC") Is DBNull.Value Then
                                            oAtributo.Valor = DBNull.Value
                                        Else
                                            oAtributo.Valor = CDate(oRow("VALOR_FEC"))
                                        End If
                                    Else
                                        oAtributo.Valor = CDate(oRow("VALFEC"))
                                    End If
                            End Select
                            oValores.Add(oValorAtrib)
                            ind += 1
                        Next
                        oAtributo.Valores = oValores
                    Else
                        If oAtributo.ListaExterna Then
                            oAtributo.Valor = oRow("VALOR_COD").ToString
                            Dim oValores As New List(Of ValorAtrib)
                            If Not String.IsNullOrEmpty(oAtributo.Valor) Then
                                oValorAtrib = New ValorAtrib
                                oValorAtrib.Valor = IIf(String.IsNullOrEmpty(oRow("VALTEXT").ToString), DBNullToStr(oRow("VALOR_TEXT")), oRow("VALTEXT").ToString)
                                oValores.Add(oValorAtrib)
                            End If
                            oAtributo.Valores = oValores
                        Else
                            Select Case oAtributo.Tipo
                                Case 1
                                    If oRow("VALTEXT") Is DBNull.Value Then
                                        oAtributo.Valor = DBNullToStr(oRow("VALOR_TEXT"))
                                    Else
                                        oAtributo.Valor = DBNullToStr(oRow("VALTEXT"))
                                    End If


                                Case 2
                                    If oRow("VALNUM") Is DBNull.Value Then
                                        oAtributo.Valor = DBNullToDbl(oRow("VALOR_NUM"))
                                    Else
                                        oAtributo.Valor = DBNullToDbl(oRow("VALNUM"))
                                    End If

                                Case 3
                                    If oRow("VALFEC") Is DBNull.Value Then
                                        If oRow("VALOR_FEC") Is DBNull.Value Then
                                            oAtributo.Valor = DBNull.Value
                                        Else
                                            oAtributo.Valor = CDate(oRow("VALOR_FEC"))
                                        End If
                                    Else
                                        oAtributo.Valor = CDate(oRow("VALFEC"))
                                    End If
                                Case 4
                                    If oRow("VALBOOL") Is DBNull.Value Then
                                        oAtributo.Valor = DBNullToStr(oRow("VALOR_BOOL"))
                                    Else
                                        oAtributo.Valor = DBNullToStr(oRow("VALBOOL"))
                                    End If
                            End Select
                        End If
                    End If
                    oAtributos.Add(oAtributo)
                Next
            End If
        End If
        Return oAtributos
    End Function
    Public Function CargarObjetoAtributosSinLisExterna(ByVal oData As DataSet) As CAtributosPedido
        Authenticate()

        Dim oDatos As DataSet
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim oAtributos As CAtributosPedido
        Dim oAtributo As Fullstep.FSNServer.CAtributoPedido
        Dim oValorAtrib As ValorAtrib
        Dim ind As Short = 0
        oAtributos = New CAtributosPedido(mDBServer, mIsAuthenticated)
        If Not IsNothing(oData) Then
            If oData.Tables.Count <> 0 Then
                For Each oRow As DataRow In oData.Tables(0).Rows
                    oAtributo = New CAtributoPedido
                    oAtributo.Id = DBNullToInteger(oRow("ID"))
                    oAtributo.Codigo = DBNullToStr(oRow("COD"))
                    oAtributo.Denominacion = DBNullToStr(oRow("DEN"))
                    oAtributo.Tipo = oRow("TIPO")
                    oAtributo.Intro = DBNullToInteger(oRow("INTRO"))
                    oAtributo.Obligatorio = DBNullToBoolean(oRow("OBLIGATORIO"))
                    oAtributo.Interno = DBNullToBoolean(oRow("INTERNO"))
                    oAtributo.Integracion = DBNullToBoolean(oRow("INTEGRACION"))
                    If oData.Tables(0).Columns.Contains("TIPOPED") Then
                        oAtributo.TipoPed = DBNullToBoolean(oRow("TIPOPED"))
                    Else
                        oAtributo.TipoPed = False
                    End If
                    Select Case oAtributo.Tipo
                        Case 2
                            If Not IsDBNull(oRow("VALORMINNUM")) Then
                                oAtributo.ValorMin = DBNullToDbl(oRow("VALORMINNUM"))
                            Else
                                oAtributo.ValorMin = System.DBNull.Value
                            End If
                            If Not IsDBNull(oRow("VALORMAXNUM")) Then
                                oAtributo.ValorMax = DBNullToDbl(oRow("VALORMAXNUM"))
                            Else
                                oAtributo.ValorMax = System.DBNull.Value
                            End If
                        Case 3
                            If Not IsDBNull(oRow("VALORMINFEC")) Then
                                oAtributo.ValorMin = DBNullToDbl(oRow("VALORMINFEC"))
                            Else
                                oAtributo.ValorMin = System.DBNull.Value
                            End If
                            If Not IsDBNull(oRow("VALORMAXFEC")) Then
                                oAtributo.ValorMax = DBNullToDbl(oRow("VALORMAXFEC"))
                            Else
                                oAtributo.ValorMax = System.DBNull.Value
                            End If
                        Case Else
                            oAtributo.ValorMin = System.DBNull.Value
                            oAtributo.ValorMax = System.DBNull.Value
                    End Select


                    oDatos = DBServer.Cargar_Valores_Atributos_Pedido(oAtributo.Id)
                    If oAtributo.Intro = 1 Then
                        Dim oValores As New List(Of ValorAtrib)
                        For Each oChildRow As DataRow In oDatos.Tables(0).Rows
                            oValorAtrib = New ValorAtrib
                            Select Case oAtributo.Tipo
                                Case 1
                                    oValorAtrib.Valor = DBNullToStr(oChildRow("VALOR_TEXT_LISTA"))
                                    If oRow("VALTEXT") Is DBNull.Value Then
                                        oAtributo.Valor = DBNullToStr(oRow("VALOR_TEXT"))
                                    Else
                                        oAtributo.Valor = DBNullToStr(oRow("VALTEXT"))
                                    End If
                                Case 2
                                    oValorAtrib.Valor = DBNullToDbl(oChildRow("VALOR_NUM_LISTA"))
                                    If oRow("VALNUM") Is DBNull.Value Then
                                        oAtributo.Valor = DBNullToDbl(oRow("VALOR_NUM"))
                                    Else
                                        oAtributo.Valor = DBNullToDbl(oRow("VALNUM"))
                                    End If
                                Case 3
                                    If oChildRow("VALOR_FEC_LISTA") Is DBNull.Value Then
                                        oValorAtrib.Valor = DBNull.Value
                                    Else
                                        oValorAtrib.Valor = CDate(oChildRow("VALOR_FEC_LISTA"))
                                    End If
                                    If oRow("VALFEC") Is DBNull.Value Then
                                        If oRow("VALOR_FEC") Is DBNull.Value Then
                                            oAtributo.Valor = DBNull.Value
                                        Else
                                            oAtributo.Valor = CDate(oRow("VALOR_FEC"))
                                        End If
                                    Else
                                        oAtributo.Valor = CDate(oRow("VALFEC"))
                                    End If
                            End Select
                            oValores.Add(oValorAtrib)
                            ind += 1
                        Next
                        oAtributo.Valores = oValores
                    Else
                        Select Case oAtributo.Tipo
                            Case 1
                                If oRow("VALTEXT") Is DBNull.Value Then
                                    oAtributo.Valor = DBNullToStr(oRow("VALOR_TEXT"))
                                Else
                                    oAtributo.Valor = DBNullToStr(oRow("VALTEXT"))
                                End If
                            Case 2
                                If oRow("VALNUM") Is DBNull.Value Then
                                    oAtributo.Valor = DBNullToDbl(oRow("VALOR_NUM"))
                                Else
                                    oAtributo.Valor = DBNullToDbl(oRow("VALNUM"))
                                End If
                            Case 3
                                If oRow("VALFEC") Is DBNull.Value Then
                                    If oRow("VALOR_FEC") Is DBNull.Value Then
                                        oAtributo.Valor = DBNull.Value
                                    Else
                                        oAtributo.Valor = CDate(oRow("VALOR_FEC"))
                                    End If
                                Else
                                    oAtributo.Valor = CDate(oRow("VALFEC"))
                                End If
                            Case 4
                                If oRow("VALBOOL") Is DBNull.Value Then
                                    oAtributo.Valor = DBNullToStr(oRow("VALOR_BOOL"))
                                Else
                                    oAtributo.Valor = DBNullToStr(oRow("VALBOOL"))
                                End If
                        End Select
                    End If
                    oAtributos.Add(oAtributo)
                Next
            End If
        End If
        Return oAtributos
    End Function
    ''' <summary>
    ''' Constructor de la clase CAtributoPedidos
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si est� autenticado</param>
    Public Sub New(ByRef dbserver As Fullstep.FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
End Class