﻿<Serializable()> _
Public Class CFacturas
    Inherits Lista(Of CFactura)

    ''' <summary>
    ''' Añade un nuevo elemento CFactura a la lista
    ''' </summary>
    ''' <param name="IId">Integer. ID de factura</param>
    ''' <param name="sNum">String. Número de factura</param>
    ''' <param name="IEstadoId">Integer. Id del Estado</param>
    ''' <param name="sEstadoDen">String. Nombre del Estado</param>
    ''' <param name="dtFecha">Datetime. Fecha de la factura</param>
    ''' <param name="ILineaFactura">Integer. Linea de factura</param>
    ''' <param name="ILineaPedido">Integer. Id de la línea de pedido</param>
    ''' <returns>El elemento CFactura añadido</returns>
    Public Overloads Function Add(ByVal IId As Integer, ByVal sNum As String, ByVal IEstadoId As Integer, ByVal sEstadoDen As String, ByVal dtFecha As DateTime, ByVal ILineaFactura As Integer, ByVal ILineaPedido As Integer) As CFactura

        'create a new object
        Dim objnewmember As CFactura

        objnewmember = New CFactura(mDBServer, mIsAuthenticated)

        With objnewmember
            .Id = IId
            .Num = sNum
            .EstadoId = IEstadoId
            .EstadoDen = sEstadoDen
            .Fecha = dtFecha
            '.LineaFactura = ILineaFactura
            .LineaPedido = ILineaPedido
        End With

        Me.Add(objnewmember)
        Return objnewmember

    End Function


    ''' <summary>
    ''' Función que devuelve un objeto de tipo CFacturas con los datos de las facturas vinculadas a la linea de pedido dada. Si no hay línea de pedido, devuelve todas si no se le pasa linea de pedido
    ''' </summary>
    ''' <param name="idioma">Idioma en el que se quieren recuperar los datos en multiidioma</param>
    ''' <param name="Linea_Pedido">Integer. Opcional. Id de la línea de pedido de la que se quiere recuperar las facturas</param>
    ''' <returns>Un objeto de la clase CFacturas con la relación de facturas-lineas de facturas-lineas de pedido</returns>
    ''' <remarks>
    ''' Llamada desde: </remarks>
    Public Function CargarFacturas(ByVal idioma As FSNLibrary.Idioma, Optional ByVal Linea_Pedido As Integer = 0) As CFacturas
        Dim oCFacturas As CFacturas
        Dim oCFactura As CFactura
        Dim oRoot As New Root
        oCFacturas = New CFacturas(DBServer, mIsAuthenticated)

        Dim DsResul As New DataSet
        DsResul = DBServer.Linea_DevolverFacturas(idioma, Linea_Pedido)
        If DsResul.Tables(0).Rows.Count > 0 Then
            For i As Long = 0 To DsResul.Tables(0).Rows.Count - 1
                oCFactura = New CFactura(DBServer, True)
                With DsResul.Tables(0).Rows(i)
                    oCFactura.Id = .Item("FACTURA_ID")
                    oCFactura.Num = .Item("NUM")
                    'If IsDBNull(.Item("EST_ID")) then
                    'If DBNull.Value.Equals(.Item("EST_ID")) Then
                    If .Item("EST_ID") Is DBNull.Value Then
                        oCFactura.EstadoId = 0
                    Else
                        oCFactura.EstadoId = .Item("EST_ID")
                    End If
                    If .Item("EST_DEN") Is DBNull.Value Then
                        oCFactura.EstadoDen = ""
                    Else
                        oCFactura.EstadoDen = .Item("EST_DEN")
                    End If
                    oCFactura.Fecha = .Item("FECHA")
                    'oCFactura.LineaFactura = .Item("LINEA_FACTURA")
                    oCFactura.LineaPedido = .Item("LINEA_PEDIDO")
                    If .Item("IMPORTE") Is DBNull.Value Then
                        oCFactura.Importe = ""
                    Else
                        oCFactura.Importe = .Item("IMPORTE")
                    End If

                    If .Item("MON") Is DBNull.Value Then
                        oCFactura.Moneda = ""
                    Else
                        oCFactura.Moneda = .Item("MON")
                    End If
                End With
                oCFacturas.Add(oCFactura)
                oCFactura = Nothing
            Next
        Else
            Return Nothing
        End If

        Return oCFacturas

    End Function

    ''' <summary>
    ''' Función que devuelve un dataset con los estados de factura existentes en el idioma que usa el usuario.
    ''' </summary>
    ''' <returns>un dataset con los estados de factura existentes en el idioma que usa el usuario</returns>
    ''' <remarks>
    ''' Llamada desde: EpWeb.Seguimientoaspx.vb.EstadosFra</remarks>
    Public Function CargarEstadosFactura(ByVal idioma As FSNLibrary.Idioma) As DataSet
        Authenticate()
        Dim dsEstadosFactura As DataSet = DBServer.Facturas_DevolverEstadosFra(idioma.ToString)
        Return dsEstadosFactura
    End Function

    ''' <summary>
    ''' Constructor de la clase CFacturas
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si está autenticado</param>
    ''' <remarks></remarks>
    Public Sub New(ByRef dbserver As Fullstep.FSNDataBaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
    End Sub

End Class
