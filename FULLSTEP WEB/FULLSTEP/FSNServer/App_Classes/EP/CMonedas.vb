Imports Fullstep.FSNLibrary

Public Class CMonedas
    Inherits Lista(Of CMoneda)

    Public Overloads ReadOnly Property Item(ByVal Cod As String) As CMoneda
        Get
            Return Me.Find(Function(elemento As CMoneda) elemento.Cod = Cod)
        End Get
    End Property

    ''' <summary>
    ''' A�ade un elemento CMoneda a la lista
    ''' </summary>
    ''' <param name="Cod">C�digo</param>
    ''' <param name="Den">Denominaci�n</param>
    ''' <param name="EQUIV">Equivalencia</param>
    ''' <param name="EqAct">Equivalencia actualizable (S/N)</param>
    ''' <param name="FECACT">Fecha �ltima actualizaci�n</param>
    ''' <returns>Devuelve el elemento CMoneda agregado</returns>
    Public Overloads Function Add(ByVal Cod As String, ByVal Den As String, ByVal EQUIV As Double, ByVal EqAct As Boolean, ByVal FECACT As Object) As CMoneda

        ' * Objetivo: Anyadir una moneda a la coleccion
        ' * Recibe: Datos de la moneda
        ' * Devuelve: Moneda a�adida

        Dim objnewmember As CMoneda

        ' Creacion de objeto moneda

        objnewmember = New CMoneda(mDBServer, mIsAuthenticated)

        ' Paso de los parametros al nuevo objeto moneda

        objnewmember.Cod = Cod
        objnewmember.Den = Den
        objnewmember.EQUIV = EQUIV
        objnewmember.EqAct = EqAct
        objnewmember.FECACT = FECACT


        Me.Add(objnewmember)
        Return objnewmember

    End Function


    ''' <summary>
    ''' Constructor de la clase CMonedas
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si est� autenticado</param>
    ''' <remarks>
    ''' Llamada desde: Todos los sitios donde se instancien objetos de esta clase
    ''' Tiempo m�ximo: 0 sec. </remarks>
    Public Sub New(ByRef dbserver As Fullstep.FSNDataBaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
    End Sub

End Class