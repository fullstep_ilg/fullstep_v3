Imports Fullstep.FSNLibrary
Imports System.Collections.Generic
Imports System.Configuration

<Serializable()> _
Public Class CRecepcion
    Inherits Security

    Private mvarId As Integer
    Private mvarIds As Integer()
    Private mvarOrden As Integer
    Private mvarFecha As DateTime
    Private mvarAlbaran As String
    Private mvarNumRecepERP As String
    Private mvarCorrecto As Boolean
    Private mvarObs As String
    Private mvarLineas As CLineas
    Private m_iPedido As Integer
    Private m_bFras_No_Anul As Boolean
    Private m_iOrdenEstado As Integer
    Private m_iOrdenTipo As Integer
    Private m_iOrdenProveedor As String
    Private m_udtOrigen As Integer
    Private m_iOrdenIncorrecta As Integer
    Private m_iCantRecep As Double
    Private m_iImporteRecep As Double
    Private m_sUnidad As String
    Private m_sDenUnidad As String
    Private m_iNumDecUnidad As Byte
    Private m_codArt As String
    Private m_denArt As String
    Private m_Importe As Double
    Private m_ImportePed As Double
    Private m_iCantPedida As Double
    Private m_iPrecioUnitario As Double
    Private m_Moneda As String
    Private m_iLineaPedido As Integer
    Private m_iLineaRecep As Integer
    Private m_sNumPedido As String
    Private m_sNumERP As String
    Private m_iFactura As Integer
    Private m_iNumLineasAlbaran As Integer
    Private m_sReceptor As String
    Private m_bAlbaranBloqueadoParaFacturacion As Boolean
    Private m_sAlbaranBloqueadoParaFacturacionObservaciones As String
    Private mvarFechaSistema As DateTime
    Private m_tipoRecepcion As Integer

    Public Property ID() As Integer
        Get
            ID = mvarId
        End Get
        Set(ByVal Value As Integer)
            mvarId = Value
        End Set
    End Property
    ''' <summary>
    ''' Ids de las recepciones.
    ''' La recepci�n puede ser de una orden o m�s, por lo que, dado que para cada orden se graba un ID de recepci�n, cuando registramos una recepci�n, podemos tener un array de IDs
    ''' </summary>
    Public Property IDs() As Integer()
        Get
            IDs = mvarIds
        End Get
        Set(ByVal Value As Integer())
            mvarIds = Value
        End Set
    End Property
    Public Property NumDecUnidad() As Byte
        Get
            NumDecUnidad = m_iNumDecUnidad
        End Get
        Set(ByVal Value As Byte)
            m_iNumDecUnidad = Value
        End Set
    End Property
    Public Property LineaPedido() As Integer
        Get
            LineaPedido = m_iLineaPedido
        End Get
        Set(ByVal Value As Integer)
            m_iLineaPedido = Value
        End Set
    End Property
    Public Property NumLineasAlbaran() As Integer
        Get
            NumLineasAlbaran = m_iNumLineasAlbaran
        End Get
        Set(ByVal Value As Integer)
            m_iNumLineasAlbaran = Value
        End Set
    End Property
    Public Property LineaRecep() As Integer
        Get
            LineaRecep = m_iLineaRecep
        End Get
        Set(ByVal Value As Integer)
            m_iLineaRecep = Value
        End Set
    End Property
    Public Property Factura() As Integer
        Get
            Factura = m_iFactura
        End Get
        Set(ByVal Value As Integer)
            m_iFactura = Value
        End Set
    End Property
    Public Property Orden() As Integer
        Get
            Orden = mvarOrden
        End Get
        Set(ByVal Value As Integer)
            mvarOrden = Value
        End Set
    End Property
    Private _ordenes As List(Of Integer)
    Public Property Ordenes() As List(Of Integer)
        Get
            Return _ordenes
        End Get
        Set(ByVal value As List(Of Integer))
            _ordenes = value
        End Set
    End Property
    Public Property Fecha() As DateTime
        Get
            Fecha = mvarFecha
        End Get
        Set(ByVal Value As DateTime)
            mvarFecha = Value
        End Set
    End Property
    Public Property Albaran() As String
        Get
            Albaran = mvarAlbaran
        End Get
        Set(ByVal Value As String)
            mvarAlbaran = Value
        End Set
    End Property
    Public Property NumRecepERP() As String
        Get
            NumRecepERP = mvarNumRecepERP
        End Get
        Set(ByVal Value As String)
            mvarNumRecepERP = Value
        End Set
    End Property
    Public Property Moneda() As String
        Get
            Moneda = m_Moneda
        End Get
        Set(ByVal Value As String)
            m_Moneda = Value
        End Set
    End Property
    Public Property Correcto() As Boolean
        Get
            Correcto = mvarCorrecto
        End Get
        Set(ByVal Value As Boolean)
            mvarCorrecto = Value
        End Set
    End Property
    Public Property Obs() As String
        Get
            Obs = mvarObs
        End Get
        Set(ByVal Value As String)
            mvarObs = Value
        End Set
    End Property
    Public Property Lineas() As CLineas
        Get
            Return mvarLineas
        End Get
        Set(ByVal value As CLineas)
            mvarLineas = value
        End Set
    End Property
    Public Property Pedido() As Integer
        Get
            Pedido = m_iPedido
        End Get
        Set(ByVal Value As Integer)
            m_iPedido = Value
        End Set
    End Property
    Public Property NumPedido() As String
        Get
            NumPedido = m_sNumPedido
        End Get
        Set(ByVal Value As String)
            m_sNumPedido = Value
        End Set
    End Property
    Public Property NumERP() As String
        Get
            NumERP = m_sNumERP
        End Get
        Set(ByVal Value As String)
            m_sNumERP = Value
        End Set
    End Property
    Public Property Fras_No_Anul() As Boolean
        Get
            Fras_No_Anul = m_bFras_No_Anul
        End Get
        Set(ByVal Value As Boolean)
            m_bFras_No_Anul = Value
        End Set
    End Property
    Public Property OrdenTipo() As Integer
        Get
            OrdenTipo = m_iOrdenTipo
        End Get
        Set(ByVal value As Integer)
            m_iOrdenTipo = value
        End Set
    End Property
    Public Property OrdenEstado() As Integer
        Get
            OrdenEstado = m_iOrdenEstado
        End Get
        Set(ByVal value As Integer)
            m_iOrdenEstado = value
        End Set
    End Property
    Public Property OrdenProveedor() As String
        Get
            OrdenProveedor = m_iOrdenProveedor
        End Get
        Set(ByVal value As String)
            m_iOrdenProveedor = value
        End Set
    End Property
    Public Property OrdenIncorrecta() As Integer
        Get
            OrdenIncorrecta = m_iOrdenIncorrecta
        End Get
        Set(ByVal value As Integer)
            m_iOrdenIncorrecta = value
        End Set
    End Property
    Public Property CantRecep() As Double
        Get
            CantRecep = m_iCantRecep
        End Get
        Set(ByVal value As Double)
            m_iCantRecep = value
        End Set
    End Property
    Public Property ImporteRecep() As Double
        Get
            ImporteRecep = m_iImporteRecep
        End Get
        Set(ByVal value As Double)
            m_iImporteRecep = value
        End Set
    End Property
    Public Property CantPedida() As Double
        Get
            CantPedida = m_iCantPedida
        End Get
        Set(ByVal value As Double)
            m_iCantPedida = value
        End Set
    End Property
    Public Property ImportePedido() As Double
        Get
            ImportePedido = m_ImportePed
        End Get
        Set(ByVal value As Double)
            m_ImportePed = value
        End Set
    End Property
    Public Property Importe() As Double
        Get
            Importe = m_Importe
        End Get
        Set(ByVal value As Double)
            m_Importe = value
        End Set
    End Property
    Public Property PrecioUnitario() As Double
        Get
            PrecioUnitario = m_iPrecioUnitario
        End Get
        Set(ByVal value As Double)
            m_iPrecioUnitario = value
        End Set
    End Property
    Public Property Unidad() As String
        Get
            Unidad = m_sUnidad
        End Get
        Set(ByVal value As String)
            m_sUnidad = value
        End Set
    End Property
    Public Property DenUnidad() As String
        Get
            DenUnidad = m_sDenUnidad
        End Get
        Set(ByVal value As String)
            m_sDenUnidad = value
        End Set
    End Property
    Public Property CodArt() As String
        Get
            CodArt = m_codArt
        End Get
        Set(ByVal Value As String)
            m_codArt = Value
        End Set
    End Property
    Public Property DenArt() As String
        Get
            DenArt = m_denArt
        End Get
        Set(ByVal Value As String)
            m_denArt = Value
        End Set
    End Property
    ''' <summary>
    ''' C�digo de persona (USU.PER, PER.COD) que efect�a la recepci�n
    ''' </summary>
    Public Property Receptor() As String
        Get
            Return m_sReceptor
        End Get
        Set(ByVal Value As String)
            m_sReceptor = Value
        End Set
    End Property
    ''' <summary>
    ''' Indica si el albar�n de la orden (y por tanto la orden) est� bloqueado para facturaci�n
    ''' </summary>
    Public Property AlbaranBloqueadoParaFacturacion() As Boolean
        Get
            Return m_bAlbaranBloqueadoParaFacturacion
        End Get
        Set(ByVal Value As Boolean)
            m_bAlbaranBloqueadoParaFacturacion = Value
        End Set
    End Property
    ''' <summary>
    ''' Observaciones guardadas al bloquear el albar�n
    ''' </summary>
    Public Property AlbaranBloqueadoParaFacturacionObservaciones() As String
        Get
            Return m_sAlbaranBloqueadoParaFacturacionObservaciones
        End Get
        Set(ByVal Value As String)
            m_sAlbaranBloqueadoParaFacturacionObservaciones = Value
        End Set
    End Property
    ''' <summary>
    ''' Origen del cambio
    ''' </summary>
    Friend Property dtOrigen() As Integer
        Get
            Return m_udtOrigen
        End Get
        Set(ByVal Value As Integer)
            m_udtOrigen = Value
        End Set
    End Property
    ''' <summary>
    ''' Fecha en que se ha grabado la recepci�n en el sistema
    ''' </summary>
    Public Property FechaSistema() As DateTime
        Get
            FechaSistema = mvarFechaSistema
        End Get
        Set(ByVal Value As DateTime)
            mvarFechaSistema = Value
        End Set
    End Property
    Public Property TipoRecepcion() As Integer
        Get
            Return Me.m_tipoRecepcion
        End Get
        Set(value As Integer)
            Me.m_tipoRecepcion = value
        End Set
    End Property
    Private _dtFechaContable As DateTime
    Public Property FechaContable() As DateTime
        Get
            Return _dtFechaContable
        End Get
        Set(ByVal value As DateTime)
            _dtFechaContable = value
        End Set
    End Property
    ''' <summary>Constructor de la clase</summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si est� autenticado</param>
    ''' <remarks>
    ''' Llamada desde: Todos los sitios en los que se instancie un objeto de la clase COrden
    ''' Tiempo m�ximo: 0 sec
    ''' </remarks>
    Public Sub New(ByRef dbserver As Fullstep.FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
    ''' Revisado por: blp. Fecha: 19/11/2012
    ''' <summary>
    ''' M�todo que efect�a el registro de la recepci�n en las tablas correspondientes (incluidas las de log si corresponde)
    ''' </summary>
    ''' <param name="Subsanado">subsanado</param>
    ''' <param name="Persona">Persona</param>
    ''' <param name="Notificar">Notificar o no</param>
    ''' <param name="Cerrar">Cerrar o no la recepci�n</param>
    ''' <param name="ParGen">Parametros generales</param>
    ''' <remarks>llamada desde recepcion.aspx.vb. Tiempo M�x inferior a 1 seg.</remarks>
    Public Sub RegistrarRecepcion(ByVal Subsanado As Boolean, ByVal Persona As String, ByVal Notificar As Boolean, ByVal Cerrar As Boolean, ByVal ParGen As TiposDeDatos.ParametrosGenerales)
        Authenticate()

        Dim arlineas As New List(Of Object)
        Dim hayLineasRecepcionables As Boolean = False
        For i As Integer = 0 To mvarLineas.Count - 1
            Dim linea As CLinea = mvarLineas.Item(i)
            Dim oLin As Object = New With {.Id = mvarLineas.Item(i).ID, .Orden = _ordenes(i), .CantRec = mvarLineas.Item(i).CantRec, .Activo = mvarLineas.Item(i).Activo, .Importe = mvarLineas.Item(i).Importe, .SalidaAlmacen = mvarLineas.Item(i).SalidaAlmacen}

            'Nos aseguramos de que la l�nea no se ha recepcionado completamente
            If mvarLineas.Item(i).isRecepcionCantidad Then
                Dim lineaTotalmenteRecepcionada As Boolean = DBServer.Recepcion_lineaTotalmenteRecepcionada(mvarLineas.Item(i).ID)
                If lineaTotalmenteRecepcionada Then
                    oLin.CantRec = System.Double.NaN 'Con este valor nos aseguramos que la l�nea no se recepcione caso de haber m�s l�neas recepcionables en mvarLineas
                Else
                    hayLineasRecepcionables = True
                End If
            Else
                If linea.ImportePedido >= linea.Importe + linea.importeRecibido Then
                    hayLineasRecepcionables = True
                End If
            End If
            arlineas.Add(oLin)
        Next

        If hayLineasRecepcionables Then
            Dim bGrabarEnLog As Boolean = GrabarEnLog(ParGen.giRecepAprov, ParGen.giRecepDirecto, ParGen.gbActivLog)
            Dim oIntegracion As New Integracion(DBServer, True)
            Dim iCont As Integer

            mvarIds = DBServer.Recepcion_RegistrarRecepcion(_ordenes.Distinct.ToList, mvarFecha, mvarAlbaran, mvarCorrecto, mvarObs, Subsanado, Persona, Notificar,
                                                            Cerrar, arlineas, bGrabarEnLog, m_udtOrigen, mvarFechaSistema, _dtFechaContable)
            If bGrabarEnLog Then 'Si se graba en LOG, procederemos a llamar al Servicio de Integraci�n.
                For iCont = 0 To (mvarIds.Length - 1)
                    'Llamada a FSIS
                    oIntegracion.LlamarFSIS(TablasIntegracion.Rec_Aprov, mvarIds(iCont), Me.Pedido, _ordenes(iCont))
                    oIntegracion.LlamarFSIS(TablasIntegracion.PED_Aprov, _ordenes(iCont), Me.Pedido, , , , True)
                Next
            End If
        Else
            Dim e As New FSNException()
            e.Number = FSNException.TipoError.TodasLasLineasHanSidoRecepcionadas
            Throw e
        End If
    End Sub
    ''' Revisado por: blp. 28/05/2013
    ''' <summary>
    ''' Enviar un email por cada orden recepcionada al proveedor indicando que la recepci�n ha sido correcta
    ''' </summary>
    ''' <param name="addressFrom">Email desde el que se env�a el email</param>
    ''' <remarks>Llamada desde REcepcion.aspx.vb. 0,3 seg.</remarks>
    Public Sub NotificarRecepcionCorrecta(ByVal addressFrom As String)
        
        Dim dr As DataRow = DBServer.Parametros_Portal()
        Dim iINSTWEB As Integer = dr.Item("INSTWEB")
        Dim sFSP_BD As String = IIf(IsDBNull(dr.Item("FSP_BD")), String.Empty, dr.Item("FSP_BD"))
        Dim sFSP_SRV As String = IIf(IsDBNull(dr.Item("FSP_SRV")), String.Empty, dr.Item("FSP_SRV"))
        Dim sCiaComp As String = IIf(IsDBNull(dr.Item("FSP_CIA")), String.Empty, dr.Item("FSP_CIA"))
        dr = Nothing
        Dim _ordenesDistinct As List(Of Integer) = _ordenes.Distinct.ToList
        For i As Integer = 0 To _ordenesDistinct.Count - 1
            Dim oNotificador As New Notificar(DBServer, mIsAuthenticated)
            oNotificador.gIdioma = ConfigurationManager.AppSettings("idioma")

            If iINSTWEB = 1 Then
                oNotificador.NotificacionRecepcionCorrecta(_ordenesDistinct(i), mvarIds(i), Nothing, addressFrom)
            Else
                Dim sFSP As String = sFSP_SRV & "." & sFSP_BD & ".dbo."
                oNotificador.NotificacionRecepcionCorrecta(_ordenesDistinct(i), mvarIds(i), sFSP, addressFrom)
            End If

            oNotificador.FuerzaFinalizeSmtpClient()
            oNotificador = Nothing
        Next
    End Sub
    ''' Revisado por: blp. 28/05/2013
    ''' <summary>
    ''' Enviar un email por cada orden recepcionada al proveedor indicando que la recepci�n ha sido incorrecta
    ''' </summary>
    ''' <param name="addressFrom">Email desde el que se env�a el email</param>
    ''' <remarks>Llamada desde REcepcion.aspx.vb. 0,3 seg.</remarks>
    Public Sub NotificarRecepcionIncorrecta(ByVal addressFrom As String)
        
        Dim dr As DataRow = DBServer.Parametros_Portal()
        Dim iINSTWEB As Integer = dr.Item("INSTWEB")
        Dim sFSP_BD As String = IIf(IsDBNull(dr.Item("FSP_BD")), String.Empty, dr.Item("FSP_BD"))
        Dim sFSP_SRV As String = IIf(IsDBNull(dr.Item("FSP_SRV")), String.Empty, dr.Item("FSP_SRV"))
        Dim sCiaComp As String = IIf(IsDBNull(dr.Item("FSP_CIA")), String.Empty, dr.Item("FSP_CIA"))
        dr = Nothing
        Dim _ordenesDistinct As List(Of Integer) = _ordenes.Distinct.ToList
        For i As Integer = 0 To _ordenesDistinct.Count - 1
            Dim oNotificador As New Notificar(DBServer, mIsAuthenticated)
            oNotificador.gIdioma = ConfigurationManager.AppSettings("idioma")

            If iINSTWEB = 1 Then
                oNotificador.NotificacionRecepcionIncorrecta(_ordenesDistinct(i), mvarIds(i), Nothing, addressFrom)
            Else
                Dim sFSP As String = sFSP_SRV & "." & sFSP_BD & ".dbo."
                oNotificador.NotificacionRecepcionIncorrecta(_ordenesDistinct(i), mvarIds(i), sFSP, addressFrom)
            End If

            oNotificador.FuerzaFinalizeSmtpClient()
            oNotificador = Nothing
        Next
    End Sub
    Public Function Recepcion_ValidarModificacionLineaRecepcion(ByVal LineaRecep As Long, ByVal Cantidad As Double) As Byte
        Return mDBServer.Recepcion_ValidarModificacionLineaRecepcion(LineaRecep, Cantidad)
    End Function
    ''' <summary>
    ''' Actualiza la cantidad recepcionada de una l�nea de recepci�n
    ''' </summary>
    ''' <param name="LineaRecep">ID de la l�nea de la recepci�n </param>
    ''' <param name="Cantidad">Cantidad modificada</param>
    ''' <param name="idCaso">Caso</param>
    ''' <param name="sPer">Codigo de persona</param>
    ''' <returns>
    ''' El resultado del proceso de modificaci�n: Cantidad grabada en BD
    ''' </returns>
    ''' <remarks>
    ''' Llamada desde: Ws.Pedidos.asmx.vb.GrabarModificacionLineaRecepcion 
    ''' revisado por ngo(20/12/2011)
    ''' </remarks>
    Public Function Recepcion_GrabarModificacionLineaRecepcion(ByVal LineaRecep As Long, ByVal Cantidad As Double, ByVal idCaso As Byte, ByVal sPer As String, ByVal sCodUsu As String) As Double()
        Return mDBServer.Recepcion_GrabarModificacionLineaRecepcion(LineaRecep, Cantidad, idCaso, sCodUsu, sPer)
    End Function
    ''' Revisado por: blp 16/05/2013
    ''' <summary>
    ''' Notificar la recepci�n al aprovisionador
    ''' </summary>
    ''' <param name="CodReceptor">c�digo del receptor</param>
    ''' <param name="dtOrdenes">dataset con las �rdenes</param>
    ''' <param name="CodPersona">c�digo Persona del usuario</param>
    ''' <remarks>Llamada desde Recepcion.aspx.vb. 0,1 seg.</remarks>
    Public Sub NotificarRecepcionAprovisionador(ByVal CodReceptor As String, ByVal dtOrdenes As DataTable, ByVal CodPersona As String)
        Dim _ordenesDistinct As List(Of Integer) = _ordenes.Distinct.ToList
        For i As Integer = 0 To _ordenesDistinct.Count - 1
            'dtOrdenes lleva datos de las �rdenes, l�neas y recepciones, por lo que al filtrar por orden podemos obtener m�s de una fila
            Dim arrOrdenes As DataRow() = dtOrdenes.Select("ORDENID=" & _ordenesDistinct(i))
            Dim drOrden As DataRow = Nothing
            If arrOrdenes IsNot Nothing AndAlso arrOrdenes.Any Then
                drOrden = arrOrdenes(0)
            End If
            If drOrden IsNot Nothing AndAlso Not IsDBNull(drOrden.Item("APROVISIONADORCOD")) Then
                If drOrden.Item("APROVISIONADORCOD") <> CodPersona Then
                    Dim oNotificador As New Notificar(DBServer, mIsAuthenticated)
                    oNotificador.gIdioma = ConfigurationManager.AppSettings("idioma")
                    If mvarCorrecto Then
                        oNotificador.NotificacionRecepcionCorrectaAprov(_ordenesDistinct(i), mvarIds(i), CodReceptor)
                    Else
                        oNotificador.NotificacionRecepcionIncorrectaAprov(_ordenesDistinct(i), mvarIds(i), CodReceptor)
                    End If
                    oNotificador.FuerzaFinalizeSmtpClient()
                    oNotificador = Nothing
                End If
            End If
        Next
    End Sub
    ''' <summary>
    ''' Eliminar la l�nea de la recepcion indicada y actualizar el resto de datos (cantidad recepcionada, etc)
    ''' </summary>
    ''' <param name="idLinea">ID de la l�nea de la recepci�n a eliminar</param>
    ''' <param name="UserCode">C�digo del usuario que realiza la eliminaci�n de la l�nea</param>
    ''' <returns>
    ''' El resultado del proceso de eliminaci�n. Si el resultado es 0 es que se ha eliminado la l�nea de la recepcion sin problemas
    ''' </returns>
    ''' <remarks>
    ''' Llamada desde: Seguimiento.aspx.vb.grdRecepciones_OnRowCommand 
    ''' revisado por ngo(20/12/2011)</remarks>
    Public Function EliminarLineaDeRecepcion(ByVal idLinea As Integer, Optional ByVal UserCode As String = "") As Integer
        Authenticate()
        mDBServer.Recepcion_EliminarLineaRecepcion(idLinea, UserCode)
    End Function
    ''' <summary>
    ''' Nos indica si hay que grabar o no el cambio en el log (tabla LOG_PEDIDO_RECEP) y carga 
    ''' en la variable del m�dulo m_udtOrigen el origen del movimiento
    ''' </summary>
    ''' <returns>
    ''' True (hay que grabar) False (No hay que grabar)
    ''' </returns>
    ''' <remarks>
    ''' Llamada desde: CRecepcion.EliminarRecepcion 
    ''' </remarks>
    Friend Function GrabarEnLog(ByVal acceso_giRecepAprov As TiposDeDatos.OrigenRegistroRecepcionesPedido, ByVal acceso_giRecepDirecto As TiposDeDatos.OrigenRegistroRecepcionesPedido, ByVal ActivLog As Boolean) As Boolean
        'If m_udtTipoOrden = Aprovisionamiento Then
        If m_iOrdenTipo = 1 Then '1=Aprovisionamiento
            If ActivLog Or (acceso_giRecepAprov = TiposDeDatos.OrigenRegistroRecepcionesPedido.RegistroActividad Or acceso_giRecepAprov = TiposDeDatos.OrigenRegistroRecepcionesPedido.IntegracionyRegistroActividad) Then
                If ActivLog And (acceso_giRecepAprov = TiposDeDatos.OrigenRegistroRecepcionesPedido.RegistroActividad Or acceso_giRecepAprov = TiposDeDatos.OrigenRegistroRecepcionesPedido.IntegracionyRegistroActividad) Then
                    m_udtOrigen = TiposDeDatos.OrigenIntegracion.FSGSIntReg
                Else
                    If ActivLog Then
                        m_udtOrigen = TiposDeDatos.OrigenIntegracion.FSGSReg
                    Else
                        m_udtOrigen = TiposDeDatos.OrigenIntegracion.FSGSInt
                    End If
                End If
                GrabarEnLog = True
            Else
                GrabarEnLog = False
            End If
        Else
            If ActivLog Or (acceso_giRecepDirecto = TiposDeDatos.OrigenRegistroRecepcionesPedido.RegistroActividad Or acceso_giRecepDirecto = TiposDeDatos.OrigenRegistroRecepcionesPedido.IntegracionyRegistroActividad) Then
                If ActivLog And (acceso_giRecepDirecto = TiposDeDatos.OrigenRegistroRecepcionesPedido.RegistroActividad Or acceso_giRecepDirecto = TiposDeDatos.OrigenRegistroRecepcionesPedido.IntegracionyRegistroActividad) Then
                    m_udtOrigen = TiposDeDatos.OrigenIntegracion.FSGSIntReg
                Else
                    If ActivLog Then
                        m_udtOrigen = TiposDeDatos.OrigenIntegracion.FSGSReg
                    Else
                        m_udtOrigen = TiposDeDatos.OrigenIntegracion.FSGSInt
                    End If
                End If
                GrabarEnLog = True
            Else
                GrabarEnLog = False
            End If
        End If
    End Function
    ''' Revisado por: blp. Fecha: 10/05/2012
    ''' <summary>
    ''' Funci�n que bloquea el Albar�n para que no puedan facturarse las �rdenes asociadas al mismo
    ''' </summary>
    ''' <param name="Albaran">Albar�n a bloquear</param>
    ''' <param name="idOrden">Id de la orden de la cual hay que bloquear los albaranes que coincidan con el par�metro Albaran</param>
    ''' <param name="Observaciones">Comentarios relativos al bloqueo</param>
    ''' <returns>Resultado del bloqueo: 0 correcto. 1 Error: no se ha efectuado</returns>
    ''' <remarks></remarks>
    Public Function BloquearFacturacionAlbaran(ByVal Albaran As String, ByVal idOrden As Integer, ByVal Observaciones As String) As Integer
        Authenticate()
        Return mDBServer.Recepcion_BloquearFacturacionAlbaran(Albaran, idOrden, Observaciones)
    End Function
    ''' Revisado por: blp. Fecha: 10/05/2012
    ''' <summary>
    ''' Funci�n que desbloquea el Albar�n y permite por tanto que puedan facturarse las �rdenes asociadas al mismo
    ''' </summary>
    ''' <param name="Albaran">Albar�n a desbloquear</param>
    ''' <param name="idOrden">Id de la orden de la cual hay que desbloquear los albaranes que coincidan con el par�metro Albaran</param>
    ''' <returns>Resultado del bloqueo: 0 correcto. 1 Error: no se ha efectuado</returns>
    ''' <remarks></remarks>
    Public Function DesBloquearFacturacionAlbaran(ByVal Albaran As String, ByVal idOrden As Integer) As Integer
        Authenticate()
        Return mDBServer.Recepcion_DesbloquearFacturacionAlbaran(Albaran, idOrden)
    End Function
End Class