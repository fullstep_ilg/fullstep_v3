Public Class CContactos
    Inherits Lista(Of CContacto)

    ''' <summary>
    ''' Establece o devuelve el elemento con el c�digo de proveedor e ID especificados
    ''' </summary>
    ''' <param name="CodProveedor">C�digo del proveedor</param>
    ''' <param name="Id">ID del contacto</param>
    ''' <returns>El elemento con el c�digo de proveedor y el ID especificados</returns>
    Public Overloads Property Item(ByVal CodProveedor As String, ByVal Id As Integer) As CContacto
        Get
            Return Me.Find(Function(elemento As CContacto) elemento.CodProveedor = CodProveedor And elemento.Id = Id)
        End Get
        Set(ByVal value As CContacto)
            Dim ind As Integer = Me.FindIndex(Function(elemento As CContacto) elemento.CodProveedor = CodProveedor And elemento.Id = Id)
            If ind = -1 Then
                Me.Add(value)
            Else
                Me.Item(ind) = value
            End If
        End Set
    End Property

    ''' <summary>
    ''' Carga los datos del contacto principal del proveedor seleccionado
    ''' </summary>
    ''' <param name="Prove">c�digo del proveedor seleccionado</param>
    ''' <returns>Un dataset con una tabla con los datos del contacto</returns>
    ''' <remarks>
    ''' Llamada desde: PedidoLibre.aspx
    ''' Tiempo m�ximo: 0 sec.
    ''' </remarks>
    Public Function CargarDatosContacto(ByVal Prove As String) As DataSet

        Dim data As DataSet

        data = DBServer.Contactos_CargarDatosContacto(Prove)

        Return data

    End Function

    ''' <summary>
    ''' Constructor de la clase CContacto
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si est� autenticado</param>
    Public Sub New(ByRef dbserver As Fullstep.FSNDataBaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
    End Sub

End Class