<Serializable()> _
Public Class COrdenes
    Inherits Lista(Of COrden)

    Public Overloads ReadOnly Property Item(ByVal Id As String) As COrden
        Get
            Return Me.Find(Function(elemento As COrden) elemento.ID = Id)
        End Get
    End Property
    Public Overloads ReadOnly Property Item(ByVal Proveedor As String, ByVal Moneda As String) As COrden
        Get
            Return Me.Find(Function(elemento As COrden) elemento.Prove = Proveedor And elemento.Moneda = Moneda)
        End Get
    End Property
    Public Overloads ReadOnly Property Item(ByVal Proveedor As String, ByVal Moneda As String, ByVal Categoria As Integer, ByVal Nivel As Integer) As COrden
        Get
            Return Me.Find(Function(elemento As COrden) elemento.Prove = Proveedor And elemento.Moneda = Moneda And elemento.Categoria = Categoria And elemento.Nivel = Nivel)
        End Get
    End Property
    ''' <summary>
    ''' Funci�n que inserta una nueva orden de entrega en la colecci�n de ordenes de entrega.
    ''' </summary>
    ''' <param name="ID">Id de la orden de entrega</param>
    ''' <param name="Pedido">Id del Pedido</param>
    ''' <param name="Anyo">A�o del pedido</param>
    ''' <param name="Prove">Codigo del proveedor</param>
    ''' <param name="Num">Numero de cesta</param>
    ''' <param name="Est">Estado del pedido</param>
    ''' <param name="Tipo">Tipo de pedido</param>
    ''' <param name="Fecha">Fecha del pedido</param>
    ''' <param name="Importe">Importe del pedido</param>
    ''' <param name="NumExt">N�mero de pedido erp</param>
    ''' <param name="OrdenEst"></param>
    ''' <param name="CodProvePortal">C�digo del proveedor del portal</param>
    ''' <param name="sReferencia">Codigo del proveedor ERP</param>
    ''' <param name="sObs">Observaciones del pedido</param>
    ''' <param name="vMon">c�digo de moneda</param>
    ''' <param name="vCambio">cambio a aplicar</param>
    ''' <param name="lEmpresa">Id de empresa</param>
    ''' <param name="sReceptor">Codigo de Receptor</param>
    ''' <param name="sCodERP">C�digo de ERP</param>
    ''' <returns>Devuelve el objeto COrden insertado</returns>
    ''' <remarks>
    ''' Llamada desde: todos los sitios donde se inserte en la colecci�n de ordenes de entrega
    ''' Tiempo m�ximo: 0 sec</remarks>
    Public Overloads Function Add(ByVal ID As Integer, ByVal Pedido As Integer, ByVal Anyo As Integer, ByVal Prove As String, ByVal Num As Integer, ByVal Est As Integer, ByVal Tipo As Integer, ByVal Fecha As Object, ByVal Importe As Double, ByVal NumExt As Object, ByVal OrdenEst As Object, ByVal CodProvePortal As Object, ByVal sReferencia As Object, ByVal sObs As Object, ByVal vMon As Object, ByVal vCambio As Object, ByVal lEmpresa As Object, ByVal sReceptor As Object, ByVal sCodERP As Object, ByVal TipoPedido As cTipoPedido) As COrden

        'create a new object
        Dim objnewmember As COrden
        'Dim X As Object ' para pruebas
        objnewmember = New COrden(mDBServer, mIsAuthenticated)
        With objnewmember

            .ID = ID
            .PedidoId = Pedido
            .Anyo = Anyo
            .Prove = Prove
            .Num = Num
            .Est = Est
            .Tipo = Tipo
            .Fecha = Fecha
            .Importe = Importe
            .CodProvePortal = DBNullToStr(CodProvePortal)

            'Antes IsMissing
            If Not IsNothing(NumExt) And Not IsDBNull(NumExt) Then
                .NumExt = CStr(NumExt)
            End If

            'Antes IsMissing
            If Not IsNothing(OrdenEst) And Not IsDBNull(OrdenEst) Then
                .OrdenEst = CShort(OrdenEst)
            End If

            .Referencia = sReferencia
            .Obs = sObs

            'Antes IsMissing
            If Not IsNothing(vMon) And Not IsDBNull(vMon) Then
                .Moneda = vMon
            End If
            'Antes IsMissing
            If Not IsNothing(vCambio) And Not IsDBNull(vCambio) Then
                .Cambio = vCambio
            End If

            'Antes IsMissing
            If Not IsNothing(lEmpresa) Then
                .Empresa = lEmpresa
            Else
                .Empresa = Nothing
            End If

            'Antes IsMissing
            If Not IsNothing(sReceptor) Then
                .Receptor = sReceptor
            Else
                .Receptor = Nothing
            End If

            'Antes IsMissing
            If Not IsNothing(sCodERP) Then
                .CodErp = sCodERP
            Else
                .CodErp = Nothing
            End If

            If Not IsNothing(TipoPedido) Then
                .TipoPedido = TipoPedido
            Else
                .TipoPedido = Nothing
            End If
            Me.Add(objnewmember)
            Return objnewmember

        End With

    End Function
    ''' Revisado por: blp. Fecha:14/02/2012
    ''' <summary>
    ''' Trae todas las ordenes de entrega
    ''' </summary>
    ''' <param name="Idioma">Idioma del usuario</param>
    ''' <param name="codPer">Codigo del usuario aprovisionador</param>
    ''' <param name="Anyo">A�o del pedido</param>
    ''' <param name="NumPed">N�mero dado al pedido (cesta) emitido en la aplicaci�n</param>
    ''' <param name="NumOrden">N�mero dado a la orden (pedido) emitida en la aplicaci�n</param>
    ''' <param name="NumPedProve">N� de pedido dado por el proveedor</param>
    ''' <param name="NumPedidoErp">N�mero de pedido dado en el ERP del cliente a la orden</param>
    ''' <param name="FecEmisionDesde">Fecha a partir de la cual filtrar la fecha de emisi�n del pedido</param>
    ''' <param name="FecEmisionHasta">Fecha hasta la cual filtrar la fecha de emisi�n</param>
    ''' <param name="NumMeses">N�mero de meses desde el d�a de hoy hacia atr�s de los que hay que devolver los pedidos</param>
    ''' <param name="Empresa">C�digo de la empresa</param>
    ''' <param name="Receptor">Codigo del receptor</param>
    ''' <param name="Prove">Codigo del proveedor</param>
    ''' <param name="Prove_Erp">Codigo del proveedor en el ERP</param>
    ''' <param name="TipoPedido">Tipo de pedido</param>
    ''' <param name="Estados">Valor sumado de los estados del pedido que se desean ver</param>
    ''' <param name="Ver_Abonos">Valor 1: Ver los pedidos que son Abono. Valor 0: No verlos</param>
    ''' <param name="Ver_Ped_Ep">Valor 1: Ver los pedidos de tipo (origen) EP (TIPO=1). Valor 0: No verlos</param>
    ''' <param name="Ver_Ped_Ep_Libres">Valor 1: Ver los pedidos de tipo (origen) EP (TIPO=1) LIBRES. Valor 0: No verlos</param>
    ''' <param name="Ver_Ped_Neg">Valor 1: Ver los pedidos de tipo (origen) GS (TIPO=0). Valor 0: No verlos</param>
    ''' <param name="Ver_Ped_Erp">Valor 1: Ver los pedidos de tipo (origen) ERP (TIPO=2). Valor 0: No verlos</param>
    ''' <param name="Usu_Ped">Valor 1: Ver los pedidos del usuario. Valor 0: No verlos</param>
    ''' <param name="Cc_Ped">Valor 1: Ver los pedidos que no sean del aprovisionador y que sean de su CC. Valor 0: No verlos</param>
    ''' <param name="Articulo">Texto a buscar como una parte del c�digo del art�culo, de su descripci�n, o de otros campos descriptivos del art�culo</param>
    ''' <param name="Categorias">Conjunto de c�digos de partidas categor�as pasados en un tipo tabla</param>
    ''' <param name="CentroCoste">C�digo del centro de coste</param>
    ''' <param name="PartidasPres">Conjunto de c�digos de partidas presupuestarias pasados en un tipo tabla</param>
    ''' <param name="AnyoFra">A�o de la factura</param>
    ''' <param name="NumFra">N�mero de la factura (b�squeda exacta, no aproximada)</param>
    ''' <param name="EstFra">Estado de la factura</param>
    ''' <param name="FecFraDesde">Fecha a partir de la cual filtrar la fecha de factura</param>
    ''' <param name="FecFraHasta">Fecha hasta la cual filtrar la fecha de factura</param>
    ''' <param name="NumPago">N�mero del pago (b�squeda exacta, no aproximada)</param>
    ''' <param name="EstPago">Estado del pago</param>
    ''' <param name="FecPagoDesde">Fecha a partir de la cual filtrar la fecha de pago</param>
    ''' <param name="FecPagoHasta">Fecha hasta la cual filtrar la fecha de pago</param>
    ''' <param name="Gestor">gestor de las partidas a las que se han imputado las l�neas de cada pedido</param>
    ''' <param name="VerPedidosBloqueados">Ver los pedidos que tienen albaranes bloqueados</param>
    ''' <param name="PageNumber">N�mero de p�gina a devolver</param>
    ''' <param name="PageSize">N�mero de registros a devolver</param>
    ''' <param name="SortExpression">Campo por el que se va a filtrar</param>
    ''' <param name="SortOrder">Orden de los datos: Ascendente (ASC) o descendente (DESC). Por defecto ASC</param>
    ''' <param name="TotalRegistros"> OUTPUT (BYREF)
    '''		A trav�s de este par�metro devolvemos el total de registros. Se devuelve s�lo cuando se solicita la p�gina 1. Esto implica 
    '''		suponer que siempre que se cambien las condiciones de filtro se vuelve a la p�gina 1.
    '''		Valor 0: No hay registros.
    '''		Valor -1: Se est� cambiando de p�gina sobre un filtrado previo por lo que el valor no se devuelve y la aplicaci�n puede 
    '''		seguir usando el valor que conserve en memoria.
    '''     Valor -2: Se ha producido un error al recuperar los datos.
    ''' </param>
    ''' <param name="ParaExportacion">Indica si vamos a usar los datos para mostrar en Seguimiento o Exportar. Si es para exportar, no se paginan (se usa distinto Procedimiento almacenado)</param>
    ''' <returns>
    ''' Un dataset con una tablas con las �rdenes que cumplan los criterios de filtrado. 
    ''' Si no hay datos devuelve una tabla vac�a.
    ''' Si se produce un error devuelve nothing.
    ''' M�x. 2 seg.</returns>
    Public Function BuscarTodasOrdenes(ByVal Idioma As FSNLibrary.Idioma, ByVal codPer As String, ByVal Anyo As Integer, ByVal NumPed As Integer, ByVal NumOrden As Integer, ByVal NumPedProve As String, ByVal NumPedidoErp As String, ByVal FecEmisionDesde As Date, ByVal FecEmisionHasta As Date, ByVal NumMeses As Integer, ByVal Empresa As Integer, ByVal Receptor As String, ByVal Prove As String, ByVal Prove_Erp As String, ByVal TipoPedido As Integer, ByVal Estados As DataTable, ByVal Ver_Abonos As Boolean, ByVal Ver_Ped_Ep As Boolean, ByVal Ver_Ped_Ep_Libres As Boolean, ByVal Ver_Ped_Neg As Boolean, ByVal Ver_Ped_Erp As Boolean, ByVal Usu_Ped As Boolean, ByVal Cc_Ped As Boolean, ByVal Articulo As DataTable, ByVal Categorias As DataTable, ByVal CentroCoste As String, ByVal PartidasPres As DataTable, ByVal AnyoFra As Integer, ByVal NumFra As String, ByVal EstFra As Integer, ByVal FecFraDesde As Date, ByVal FecFraHasta As Date, ByVal NumPago As String, ByVal EstPago As Integer, ByVal FecPagoDesde As Date, ByVal FecPagoHasta As Date, ByVal Gestor As String, ByVal VerPedidosBloqueados As Boolean, ByVal PageNumber As Integer, ByVal PageSize As Integer, ByVal SortExpression As String, ByVal SortOrder As String, ByRef TotalRegistros As Integer, Optional ParaExportacion As Boolean = False, Optional ByVal Ver_ContraAbiertos As Boolean = False,
                                                Optional ByVal iAnyoPedAbierto As Integer = Nothing, Optional ByVal iCestaPedAbierto As Long = Nothing, Optional ByVal iPedidoPedAbierto As Long = Nothing, Optional ByVal Ver_Ped_Express As Boolean = False, Optional ByVal VerPedidosBorrados As Boolean = False) As DataSet
        Authenticate()
        Dim dsOrdenes As DataSet = DBServer.Ordenes_BuscarTodasOrdenes(Idioma, codPer, Anyo, NumPed, NumOrden, NumPedProve, NumPedidoErp, FecEmisionDesde, FecEmisionHasta, NumMeses, Empresa, Receptor, Prove, Prove_Erp, TipoPedido, Estados, Ver_Abonos, Ver_Ped_Ep, Ver_Ped_Ep_Libres, Ver_Ped_Neg, Ver_Ped_Erp, Usu_Ped, Cc_Ped, Articulo, Categorias, CentroCoste, PartidasPres, AnyoFra, NumFra, EstFra, FecFraDesde, FecFraHasta, NumPago, EstPago, FecPagoDesde, FecPagoHasta, Gestor, VerPedidosBloqueados, PageNumber, PageSize, SortExpression, SortOrder, TotalRegistros, ParaExportacion, Ver_ContraAbiertos, iAnyoPedAbierto, iCestaPedAbierto, iPedidoPedAbierto, Ver_Ped_Express:=Ver_Ped_Express, VerPedidosBorrados:=VerPedidosBorrados)
        dsOrdenes.Tables(0).TableName = "ORDENES"
        'Si la consulta se ha hecho sobre el procedimiento EP_OBT_ORDENES_SEGUIMIENTO, la primaryKey es el campo ID
        Dim key() As DataColumn
        If dsOrdenes.Tables("ORDENES").Columns("ID") IsNot Nothing Then
            key = {dsOrdenes.Tables("ORDENES").Columns("ID")}
        Else
            'Si no, es ORDENID, LINEAID
            key = {dsOrdenes.Tables("ORDENES").Columns("ORDENID"), dsOrdenes.Tables("ORDENES").Columns("LINEAID")}
        End If
        dsOrdenes.Tables("ORDENES").PrimaryKey = key
        Return dsOrdenes
    End Function
    ''' Revisado por: blp. Fecha: 17/01/2012
    ''' <summary>
    ''' Busca las ordenes de entrega RECEPCIONABLES.
    ''' </summary>
    ''' <param name="Idioma">C�digo de idioma</param>
    ''' <param name="CodPersona">C�digo de la tabla PER, del usuario del que queremos cargar las recepciones</param>
    ''' <param name="Anyo">A�o</param>
    ''' <param name="NumPedido">N� pedido</param>
    ''' <param name="NumOrden">N� de orden</param>
    ''' <param name="NumPedidoErp">N� de pedido en el ERP</param>
    ''' <param name="fecRecepDesde">Fecha de recepci�n del pedido desde la que se van a devolver datos</param>
    ''' <param name="fecRecepHasta">Fecha de recepci�n del pedido hasta la que se van a devolver datos</param>
    ''' <param name="NumAlbaran">N� Albar�n</param>
    ''' <param name="NumPedProve">N� de pedido en el proveedor</param>
    ''' <param name="Articulo">Parte o todo el c�digo o descripci�n del art�culo. Los par�metros Articulo y codArticulo no son excluyentes, dado que pueden servir para distinguir los art�culos gen�ricos.</param>
    ''' <param name="codArticulo">C�digo completo del art�culo. Los par�metros Articulo y codArticulo no son excluyentes, dado que pueden servir para distinguir los art�culos gen�ricos.</param>
    ''' <param name="CodProve">C�digo del proveedor</param>
    ''' <param name="ProveERP">C�digo del proveedor en el ERP</param>
    ''' <param name="TipoPedido">Tipo de pedido: 0 GS, 1 EP, 2 ERP</param>
    ''' <param name="Categorias">Categor�a de art�culos</param>
    ''' <param name="fecEntregaSolicitDesde">Fecha de entrega solicitada por el aprovisionador desde la que se van a devolver datos</param>
    ''' <param name="fecEntregaSolicitHasta">Fecha de entrega solicitada por el aprovisionador hasta la que se van a devolver datos</param>
    ''' <param name="CodEmpresa">Id de la empresa</param>
    ''' <param name="fecEntregaIndicadaDesde">Fecha de entrega indicada por el proveedor o indicada en el plan de entrega desde la que se van a devolver datos</param>
    ''' <param name="fecEntregaIndicadaHasta">Fecha de entrega indicada por el proveedor o indicada en el plan de entrega hasta la que se van a devolver datos</param>
    ''' <param name="fecEmisionDesde">Fecha de emisi�n del pedido desde la que se van a devolver datos</param>
    ''' <param name="fecEmisionHasta">Fecha de emisi�n del pedido hasta la que se van a devolver datos</param>
    ''' <param name="centroCoste">C�digo del centro de coste</param>
    ''' <param name="PartidasPres">Tabla con los PRES0, PRES1, PRES2,3,4 identificativos de las partidas</param>
    ''' <param name="TipoOrigen">Tipo de Origen</param>
    ''' <param name="Estados">Tabla con los estados del pedido</param>
    ''' <param name="CodGestor">C�digo del gestor</param>
    ''' <param name="bCargarPedidosUsu">1-> Mostrar los pedidos de los que el usuario es receptor</param>
    ''' <param name="bCargarPedidosImpCC">
    ''' 	1->
    ''' 	Si el usuario no es gestor: ver las recepciones que no sean del usuario y que sean de sus cc imputables. 
    ''' 	Si el usuario es gestor: �nicamente ver� aquellos pedidos imputados a partidas de las que es gestor. 
    ''' </param>
    ''' <param name="SmActivado">Estado del m�dulo de control presupuestario</param>
    ''' <returns>Un dataset con dos tablas "ENTREGAS" (las recepciones) y "PARTIDAS_DEN" (las denominaciones de las partidas presupuestarias</returns>
    ''' <remarks>Llamada desde Recepcion.aspx.vb-->GenerarDsetPedidos. M�x. 1 seg.</remarks>
    Public Function BuscarTodasOrdenesRecepcion(ByVal Idioma As FSNLibrary.Idioma, ByVal CodPersona As String, ByVal Anyo As Integer, ByVal NumPedido As Integer, ByVal NumOrden As Integer, ByVal NumPedidoErp As String, ByVal fecRecepDesde As Date, ByVal fecRecepHasta As Date, ByVal NumAlbaran As String, ByVal NumPedProve As String, ByVal Articulo As String, ByVal codArticulo As String, ByVal CodProve As String, ByVal ProveERP As String, ByVal TipoPedido As Integer, ByVal Categorias As DataTable, ByVal fecEntregaSolicitDesde As Date, ByVal fecEntregaSolicitHasta As Date, ByVal CodEmpresa As Integer, ByVal fecEntregaIndicadaDesde As Date, ByVal fecEntregaIndicadaHasta As Date, ByVal fecEmisionDesde As Date, ByVal fecEmisionHasta As Date, ByVal centroCoste As String, ByVal PartidasPres As DataTable, ByVal TipoOrigen As DataTable, ByVal Estados As DataTable, ByVal CodGestor As String, ByVal CodReceptor As String, ByVal bCargarPedidosUsu As Boolean, ByVal bCargarPedidosImpCC As Boolean, ByVal SmActivado As Boolean, ByVal bSoloPendientes As Boolean) As DataSet
        Authenticate()
        Dim dsOrdenes As DataSet = DBServer.Ordenes_BuscarTodasOrdenesRecepcion(Idioma, CodPersona, Anyo, NumPedido, NumOrden, NumPedidoErp, fecRecepDesde, fecRecepHasta, NumAlbaran, NumPedProve, Articulo, codArticulo, CodProve, ProveERP, TipoPedido, Categorias, fecEntregaSolicitDesde, fecEntregaSolicitHasta, CodEmpresa, fecEntregaIndicadaDesde, fecEntregaIndicadaHasta, fecEmisionDesde, fecEmisionHasta, centroCoste, PartidasPres, TipoOrigen, Estados, CodGestor, CodReceptor, bCargarPedidosUsu, bCargarPedidosImpCC, SmActivado, bSoloPendientes)

        dsOrdenes.Tables(0).TableName = "ORDENES"
        If SmActivado Then _
            dsOrdenes.Tables(1).TableName = "PARTIDAS_DEN"

        Return dsOrdenes
    End Function
    ''' Revisado por: blp. Fecha: 21/12/2011
    ''' <summary>
    ''' Busca las �rdenes en las que figura como aprobador la persona indicada
    ''' </summary>
    ''' <param name="Aprobador">C�digo de persona del aprobador</param>
    ''' <param name="Idioma">C�digo de idioma</param>
    ''' <param name="fecDesde">Fecha de emisi�n del pedido desde la que se van a devolver datos</param>
    ''' <param name="fecHasta">Fecha de emisi�n del pedido hasta la que se van a devolver datos</param>
    ''' <param name="incluirAprobados">
    ''' False: Incluir los pedidos ya aprobados.
    ''' True: No Incluir los pedidos ya aprobados.
    ''' </param>
    ''' <param name="incluirDenegados">
    ''' False: Incluir los pedidos denegados.
    ''' True: No Incluir los pedidos denegados.
    ''' </param>
    ''' <param name="incluirAprobNivelSup">
    ''' False: Incluir los pedidos pendientes de aprobar en un nivel superior.
    ''' True: No Incluir los pedidos pendientes de aprobar en un nivel superior.
    ''' </param>
    ''' <returns>Un dataset con las �rdenes recuperadas.</returns>
    Public Function BuscarOrdenesAprobador(ByVal Aprobador As String, ByVal Idioma As String, ByVal fecDesde As Date, ByVal fecHasta As Date, ByVal incluirAprobados As Boolean, ByVal incluirDenegados As Boolean, ByVal incluirAprobNivelSup As Boolean, Optional ByVal bIncluirPedidosCatalogo As Boolean = True, Optional ByVal bIncluirPedidosContraAbiertos As Boolean = True, _
                                           Optional ByVal iAnyoPedAbierto As Integer = Nothing, Optional ByVal iCestaPedAbierto As Long = Nothing, Optional ByVal iPedidoPedAbierto As Long = Nothing) As DataSet
        Authenticate()
        Dim dsOrdenes As DataSet = DBServer.Ordenes_BuscarOrdenesAprobador(Aprobador, Idioma, fecDesde, fecHasta, incluirAprobados, incluirDenegados, incluirAprobNivelSup, bIncluirPedidosCatalogo, bIncluirPedidosContraAbiertos, iAnyoPedAbierto, iCestaPedAbierto, iPedidoPedAbierto)
        dsOrdenes.Tables(0).TableName = "ORDENES"
        dsOrdenes.Tables(1).TableName = "LINEASPEDIDO"
        dsOrdenes.Tables(2).TableName = "GESTORES"
        dsOrdenes.Tables(3).TableName = "COMENTMAXLENGTH"
        dsOrdenes.Tables(4).TableName = "DDLINFO" 'Info para los dropdown de busqueda

        Dim keyOrdenInstancia(1) As DataColumn
        keyOrdenInstancia(0) = dsOrdenes.Tables("ORDENES").Columns("ID")
        keyOrdenInstancia(1) = dsOrdenes.Tables("ORDENES").Columns("INSTANCIA_APROB")
        dsOrdenes.Tables("ORDENES").PrimaryKey = keyOrdenInstancia

        Dim keylp() As DataColumn = {dsOrdenes.Tables("LINEASPEDIDO").Columns("LINEAID")}
        dsOrdenes.Tables("LINEASPEDIDO").PrimaryKey = keylp

        Dim keylpInstancia(1) As DataColumn
        keylpInstancia(0) = dsOrdenes.Tables("LINEASPEDIDO").Columns("ORDEN")
        keylpInstancia(1) = dsOrdenes.Tables("LINEASPEDIDO").Columns("INSTANCIA_APROB")
        dsOrdenes.Relations.Add("REL_ORDENES_LINEASPEDIDO", keyOrdenInstancia, keylpInstancia, False)

        Dim keyCML() As DataColumn = {dsOrdenes.Tables("COMENTMAXLENGTH").Columns("NAME")}
        dsOrdenes.Tables("COMENTMAXLENGTH").PrimaryKey = keyCML

        Return dsOrdenes
    End Function
    ''' Revisado por: blp. fecha: 20/07/2012
    ''' <summary>
    ''' Devolver� un dataset con las l�neas de pedido para todas las �rdenes pasadas por tabla
    ''' </summary>
    ''' <param name="Idioma">idioma</param>
    ''' <param name="OrdenesId">Tipo Tabla definido por el usuario para pasar una tabla de IDs de �rdenes al stored y filtrar por ellas</param>
    ''' <param name="SmActivado">Estado del m�dulo de control presupuestario</param>
    ''' <returns>Un dataset con las l�neas de pedido correspondientes para las ordenes de entrega pasadas</returns>
    ''' <remarks>
    ''' Llamada desde: Recepcion, Seguimiento
    ''' Tiempo m�ximo: 0 sec.</remarks>
    Public Function DevolverLineas(ByVal Idioma As FSNLibrary.Idioma, ByVal OrdenesId As DataTable, ByVal SmActivado As Boolean) As DataSet
        Authenticate()
        Dim dsLineas As New DataSet

        dsLineas = DBServer.Ordenes_DevolverLineas(Idioma, OrdenesId, SmActivado)

        Return dsLineas

    End Function
    ''' Revisado por: blp. fecha:05/03/2012
    ''' <summary>
    ''' Recupera los �ltimos pedidos del aprovisionador indicado.
    ''' </summary>
    ''' <param name="Estado">Filtro de pedidos seg�n su estado.</param>
    ''' <param name="Aprovisionador">C�digo del Aprovisionador</param>
    ''' <param name="NumLineas">Par�metro por referencia. Indica el n� de pedidos a devolver y devuelve el n� total de pedidos existente.</param>
    ''' <param name="pendientesAprobar">True->Mostrar pedidos pendientes de aprobar por el usuario al que corresponde el codAprovisionador</param>
    ''' <param name="pendientesRecepcionar">True->Mostrar pedidos pendientes de recepcionar por el usuario al que corresponde el codAprovisionador.
    ''' El stored tambi�n comprueba si tiene permiso para recepcionar pedidos de sus Centros de Coste y si tiene configurado que se muestren. Si es as�, tambi�n los muestra.
    ''' </param>
    ''' <returns>Un dataset con los pedidos que cumplen los criterios ordenados por fecha de emisi�n descendente.</returns>
    ''' <remarks></remarks>
    Public Function BuscarPorAprovisionador(ByVal Estado As Integer, ByVal Aprovisionador As String, _
                ByVal PermisoRecepcionarPedidosCCImputablesAprov As Boolean, ByVal RecepcionVerPedidosCCImputablesAprov As Boolean, _
                ByRef NumLineas As Integer, ByVal pendientesAprobar As Boolean, ByVal pendientesRecepcionar As Boolean) As DataSet
        Authenticate()
        Return DBServer.Ordenes_GetOrdenesAprovisionador(Estado, Aprovisionador, PermisoRecepcionarPedidosCCImputablesAprov, _
            RecepcionVerPedidosCCImputablesAprov, NumLineas, pendientesAprobar, pendientesRecepcionar)
    End Function
#Region "Cesta"
    ''' <summary>
    ''' Carga la informaci�n almacenada en la cesta de un usuario determinado
    ''' </summary>
    ''' <param name="CodPer">C�digo de Persona del usuario</param>
    ''' <param name="Idioma">Idioma actual</param>
    ''' <param name="codUsu">C�digo del usuario</param>
    Public Sub CargarCesta(ByVal CodPer As String, ByVal Idioma As FSNLibrary.Idioma, Optional ByVal codUsu As String = "", Optional ByVal idCabecera As Integer = 0)
        Dim ds As DataSet = mDBServer.Ordenes_CargarCesta(CodPer, Idioma)
        Dim adoRs As New DataSet
        ds.Tables(0).TableName = "CABECERA_CESTA"
        ds.Tables(1).TableName = "CESTA"
        ds.Tables(2).TableName = "IMPUTACIONES"
        ds.Tables(3).TableName = "ADJUNTOSCABECERAS"
        ds.Tables(4).TableName = "ADJUNTOSLINEAS"
        'Primary Key CABECERA_CESTA
        Dim keycab() As DataColumn = {ds.Tables("CABECERA_CESTA").Columns("ID")} '{ds.Tables("CABECERA_CESTA").Columns("PROVE"), ds.Tables("CABECERA_CESTA").Columns("MON")} '{ds.Tables("CABECERA_CESTA").Columns("ID")}
        ds.Tables("CABECERA_CESTA").PrimaryKey = keycab
        'Primary Key CESTA
        Dim keycesta() As DataColumn = {ds.Tables("CESTA").Columns("ID")}
        ds.Tables("CESTA").PrimaryKey = keycesta
        'Relacion CABECERA_CESTA y CESTA
        Dim keyrelcesta() As DataColumn = {ds.Tables("CESTA").Columns("CESTACABECERAID")} '{ds.Tables("CESTA").Columns("ID")} 
        ds.Relations.Add("REL_CABECERA_CESTA_CESTA", keycab, keyrelcesta, False)
        Dim cestaAnt As Integer
        Dim idAnt As Integer
        For Each fila As DataRow In ds.Tables("IMPUTACIONES").Rows
            If fila.Item("CESTA") = cestaAnt And fila.Item("ID") = idAnt Then
                fila.Delete()
            Else
                cestaAnt = fila.Item("CESTA")
                idAnt = fila.Item("ID")
            End If
        Next
        'Primary key IMPUTACIONES
        Dim keyim() As DataColumn = {ds.Tables("IMPUTACIONES").Columns("CESTA"), ds.Tables("IMPUTACIONES").Columns("ID")}
        ds.Tables("IMPUTACIONES").PrimaryKey = keyim
        'Relacion CESTA e IMPUTACIONES
        Dim keyrelim() As DataColumn = {ds.Tables("IMPUTACIONES").Columns("CESTA")}
        ds.Relations.Add("REL_CESTA_IMPUTACIONES", keycesta, keyrelim, False)
        'Primary Key ADJUNTOSCABECERAS
        Dim keyADJUNTOSCABECERAS() As DataColumn = {ds.Tables("ADJUNTOSCABECERAS").Columns("ID")}
        ds.Tables("ADJUNTOSCABECERAS").PrimaryKey = keyADJUNTOSCABECERAS
        'Relacion CESTA_CABECERA y ADJUNTOSCABECERAS
        Dim keyrelCABECERA_CESTA() As DataColumn = {ds.Tables("CABECERA_CESTA").Columns("ID")}
        Dim keyrelADJUNTOSCABECERAS() As DataColumn = {ds.Tables("ADJUNTOSCABECERAS").Columns("CESTACABECERAID")}
        ds.Relations.Add("REL_CABECERA_CESTA_ADJUNTOSCABECERAS", keyrelCABECERA_CESTA, keyrelADJUNTOSCABECERAS, False)
        'Primary Key ADJUNTOSLINEAS
        Dim keyADJUNTOSLINEAS() As DataColumn = {ds.Tables("ADJUNTOSLINEAS").Columns("ID")}
        ds.Tables("ADJUNTOSLINEAS").PrimaryKey = keyADJUNTOSLINEAS
        'Relacion CESTA y ADJUNTOSLINEAS
        Dim keyrelADJUNTOSLINEAS() As DataColumn = {ds.Tables("ADJUNTOSLINEAS").Columns("CESTA_LINEA")}
        ds.Relations.Add("REL_CABECERA_CESTA_ADJUNTOSLINEAS", keycesta, keyrelADJUNTOSLINEAS, False)
        Me.Clear()
        For Each orden As DataRow In ds.Tables("CABECERA_CESTA").Rows
            Dim oOrden As New COrden(mDBServer, mIsAuthenticated)
            oOrden.ID = orden.Item("ID")
            oOrden.Prove = DBNullToStr(orden.Item("PROVE"))
            oOrden.ProveDen = orden.Item("PROVEDEN")
            oOrden.Moneda = orden.Item("MON")
            oOrden.Cambio = orden.Item("EQUIV")
            oOrden.Categoria = orden.Item("CAT")
            oOrden.Nivel = orden.Item("NIVEL")
            If orden.Item("CAT") <> 0 Then
                adoRs = DBServer.ComprobarAtributoCabecera(oOrden.Categoria, oOrden.Nivel)
                If Not adoRs.Tables(0).Rows.Count = 0 Then
                    If Not IsDBNull(adoRs.Tables(0).Rows(0).Item("DEN")) Then
                        oOrden.DenCategoria = adoRs.Tables(0).Rows(0).Item("DEN").ToString()
                    End If
                End If
            End If
            oOrden.Referencia = DBNullToStr(orden.Item("NUM_PED_ERP"))
            oOrden.Empresa = DBNullToDbl(orden.Item("EMP"))
            oOrden.CodErp = DBNullToStr(orden.Item("COD_PROVE_ERP"))
            oOrden.Receptor = DBNullToStr(orden.Item("RECEPTOR"))
            oOrden.DireccionEnvioFactura = DBNullToInteger(orden.Item("FAC_DIR_ENVIO"))
            oOrden.Categoria = DBNullToInteger(orden.Item("CAT"))
            oOrden.Nivel = DBNullToInteger(orden.Item("NIVEL"))
            If Not IsDBNull(orden.Item("TIPOPEDIDO")) Then
                oOrden.TipoPedido = New cTipoPedido(mDBServer, mIsAuthenticated)
                oOrden.TipoPedido.Id = orden.Item("TIPOPEDIDO")
            End If
            oOrden.Obs = DBNullToStr(orden.Item("OBS"))
            oOrden.IdOrdenPedidoAbierto = DBNullToInteger(orden.Item("ORDEN_PED_ABIERTO"))
            oOrden.Anyo_PedidoAbierto = DBNullToInteger(orden.Item("ANYO_PEDIDOABIERTO"))
            oOrden.NumOrden_PedidoAbierto = DBNullToInteger(orden.Item("NUMORDEN_PEDIDOABIERTO"))
            oOrden.NumPedido_PedidoAbierto = DBNullToInteger(orden.Item("NUMPEDIDO_PEDIDOABIERTO"))
            'Adjuntos de la orden
            If orden.GetChildRows("REL_CABECERA_CESTA_ADJUNTOSCABECERAS").Count > 0 Then
                Dim oAdjuntos As New CAdjuntos(mDBServer, mIsAuthenticated)
                Dim dr() As DataRow = orden.GetChildRows("REL_CABECERA_CESTA_ADJUNTOSCABECERAS")
                For Each Adjunto As DataRow In dr
                    Dim oAdj As New Adjunto(mDBServer, mIsAuthenticated)
                    oAdj.IdEsp = Adjunto.Item("CESTACABECERAID")
                    oAdj.Id = Adjunto.Item("ID")
                    oAdj.Nombre = DBNullToStr(Adjunto.Item("NOM"))
                    oAdj.Comentario = Adjunto.Item("COM")
                    If Not IsDBNull(Adjunto.Item("FECACT")) Then
                        oAdj.Fecha = Adjunto.Item("FECACT")
                    End If
                    oAdj.dataSize = Adjunto.Item("DATASIZE")
                    oAdj.Cod = codUsu
                    oAdj.tipo = TiposDeDatos.Adjunto.Tipo.Cesta_Cabecera
                    oAdjuntos.Add(oAdj)
                Next
                oOrden.Adjuntos = oAdjuntos
                If Not IO.Directory.Exists(System.Configuration.ConfigurationManager.AppSettings("temp") & "\downloads") Then _
                    IO.Directory.CreateDirectory(System.Configuration.ConfigurationManager.AppSettings("temp") & "\downloads")
            End If
            'L�neas
            oOrden.Lineas = New CLineas(mDBServer, mIsAuthenticated)
            For Each linea As DataRow In orden.GetChildRows("REL_CABECERA_CESTA_CESTA")
                Dim oLinea As New CLinea(mDBServer, mIsAuthenticated)
                oLinea.ID = linea.Item("ID")
                oLinea.LineaCatalogo = DBNullToDbl(linea.Item("LINEA"))
                oLinea.Prove = DBNullToStr(linea.Item("PROVE"))
                oLinea.Cod = DBNullToStr(linea.Item("COD_ITEM"))
                oLinea.Den = linea.Item("ART_DEN")
                oLinea.Generico = DBNullToInteger(linea.Item("GENERICO")) 'PARA MODIFICAR LA DESCRIPCION
                oLinea.CantPed = linea.Item("CANT")
                oLinea.UP = linea.Item("UP")
                oLinea.PrecUc = linea.Item("PREC")
                oLinea.FC = linea.Item("FC")
                oLinea.CantMinimaPedido = DBNullToDbl(linea.Item("CANT_MIN_DEF"))
                oLinea.CantMaximaPedido = DBNullToDbl(linea.Item("CANT_MAX_DEF"))
                oLinea.GMN1 = DBNullToStr(linea.Item("GMN1"))
                oLinea.Dest = DBNullToStr(linea.Item("DEST"))
                oLinea.DestDen = DBNullToStr(linea.Item("DEST_DEN"))
                oLinea.Cat1 = DBNullToDbl(linea.Item("CAT1"))
                oLinea.Cat2 = DBNullToDbl(linea.Item("CAT2"))
                oLinea.Cat3 = DBNullToDbl(linea.Item("CAT3"))
                oLinea.Cat4 = DBNullToDbl(linea.Item("CAT4"))
                oLinea.Cat5 = DBNullToDbl(linea.Item("CAT5"))
                oLinea.EsLineaPedidoAbierto = Not (oOrden.IdOrdenPedidoAbierto = 0)
                If Not IsDBNull(linea.Item("CAMPO1")) Then
                    oLinea.Campo1 = New CCampo(mDBServer, mIsAuthenticated)
                    With oLinea.Campo1
                        .Den = linea.Item("DEN1")
                        .ID = linea.Item("CAMPO1")
                        .Obligatorio = linea.Item("OBL1")
                        .Valor = DBNullToStr(linea.Item("VALOR1"))
                    End With
                End If
                If Not IsDBNull(linea.Item("CAMPO2")) Then
                    oLinea.Campo2 = New CCampo(mDBServer, mIsAuthenticated)
                    With oLinea.Campo2
                        .Den = linea.Item("DEN2")
                        .ID = linea.Item("CAMPO2")
                        .Obligatorio = linea.Item("OBL2")
                        .Valor = DBNullToStr(linea.Item("VALOR2"))
                    End With
                End If
                oLinea.ModificarPrecio = SQLBinaryToBoolean(linea.Item("MODIF_PREC"))
                oLinea.ModificarUnidad = SQLBinaryToBoolean(linea.Item("MODIF_UNI"))
                If linea.Item("FECENTREGA") IsNot DBNull.Value Then _
                    oLinea.FecEntrega = linea.Item("FECENTREGA")
                oLinea.EntregaObl = SQLBinaryToBoolean(linea.Item("ENTREGA_OBL"))
                oLinea.Obs = DBNullToStr(linea.Item("OBS"))
                Dim oTipoArt As CArticulo.strTipoArticulo
                oTipoArt.Concepto = linea.Item("CONCEPTO")
                oTipoArt.TipoAlmacenamiento = linea.Item("ALMACENAR")
                oTipoArt.TipoRecepcion = linea.Item("RECEPCIONAR")
                oLinea.TipoArticulo = oTipoArt
                oLinea.IdAlmacen = DBNullToDbl(linea.Item("ALMACEN"))
                oLinea.Activo = DBNullToDbl(linea.Item("ACTIVO"))
                oLinea.ImportePedido = DBNullToDbl(linea.Item("IMPORTE_PED"))
                oLinea.TipoRecepcion = DBNullToInteger(linea.Item("TIPORECEPCION"))
				oLinea.DesvioRecepcion = DBNullToDbl(linea.Item("DESVIO_RECEPCION"))
				oLinea.Pres5_ImportesImputados = New FSNServer.Imputaciones(DBServer, mIsAuthenticated)

                For Each linimputacion As DataRow In linea.GetChildRows("REL_CESTA_IMPUTACIONES")
                    Dim oImp As FSNServer.Imputacion = New FSNServer.Imputacion(DBServer, mIsAuthenticated)
                    oImp.Id = linimputacion.Item("ID")
                    oImp.LineaId = linimputacion.Item("CESTA")
                    oImp.CentroCoste = New FSNServer.UON(DBServer, mIsAuthenticated)
                    oImp.CentroCoste.UON1 = DBNullToSomething(linimputacion.Item("UON1"))
                    oImp.CentroCoste.UON2 = DBNullToSomething(linimputacion.Item("UON2"))
                    oImp.CentroCoste.UON3 = DBNullToSomething(linimputacion.Item("UON3"))
                    oImp.CentroCoste.UON4 = DBNullToSomething(linimputacion.Item("UON4"))
                    oImp.CentroCoste.CentroSM = New FSNServer.Centro_SM(DBServer, mIsAuthenticated)
                    oImp.CentroCoste.CentroSM.Codigo = linimputacion.Item("CENTRO_SM")
                    If Not IsDBNull(linimputacion.Item("PRES5_IMP")) Then
                        oImp.CentroCoste.CentroSM.PartidasPresupuestarias = New FSNServer.PartidasPRES5(DBServer, mIsAuthenticated)
                        Dim oPart As FSNServer.PartidaPRES5 = New FSNServer.PartidaPRES5(DBServer, mIsAuthenticated)
                        oPart.NIV0 = linimputacion.Item("PRES0")
                        oPart.NIV1 = linimputacion.Item("PRES1")
                        oPart.NIV2 = DBNullToSomething(linimputacion.Item("PRES2"))
                        oPart.NIV3 = DBNullToSomething(linimputacion.Item("PRES3"))
                        oPart.NIV4 = DBNullToSomething(linimputacion.Item("PRES4"))
                        oPart.PresupuestoId = linimputacion.Item("PRES5_IMP")
                        oImp.Partida = oPart
                        oImp.CentroCoste.CentroSM.PartidasPresupuestarias.Add(oPart)
                    End If
                    oLinea.Pres5_ImportesImputados.Add(oImp)
                Next
                'Adjuntos de la linea
                If linea.GetChildRows("REL_CABECERA_CESTA_ADJUNTOSLINEAS").Count > 0 Then
                    Dim oAdjuntos As New CAdjuntos(mDBServer, mIsAuthenticated)
                    Dim dr() As DataRow = linea.GetChildRows("REL_CABECERA_CESTA_ADJUNTOSLINEAS")
                    For Each Adjunto As DataRow In dr
                        Dim oAdj As New Adjunto(mDBServer, mIsAuthenticated)
                        oAdj.Id = Adjunto.Item("ID")

                        'Al recuperar este valor, la descarga del archivo da problemas.
                        'Hasta determinar el motivo, se deshabilita
                        'oAdj.IdEsp = Adjunto.Item("CESTA_LINEA")

                        oAdj.Nombre = Adjunto.Item("NOM")
                        oAdj.Comentario = Adjunto.Item("COM")
                        oAdj.Fecha = Adjunto.Item("FECACT")
                        oAdj.dataSize = Adjunto.Item("DATASIZE")
                        oAdj.Cod = codUsu
                        oAdj.tipo = TiposDeDatos.Adjunto.Tipo.Cesta
                        oAdjuntos.Add(oAdj)
                    Next
                    oLinea.Adjuntos = oAdjuntos
                    If Not IO.Directory.Exists(System.Configuration.ConfigurationManager.AppSettings("temp") & "\downloads") Then _
                        IO.Directory.CreateDirectory(System.Configuration.ConfigurationManager.AppSettings("temp") & "\downloads")
                End If
                oOrden.Lineas.Add(oLinea)
            Next
            Me.Add(oOrden)
        Next
    End Sub
    ''' <summary>
    ''' Elimina un art�culo de la cesta dado su Identificador
    ''' </summary>
    ''' <param name="Id">Identificador de la l�nea a eliminar</param>
    ''' <remarks>
    ''' Llamada desde:La p�gina EmisionPedido, m�todo EliminarArticulo
    ''' </remarks>
    Public Sub EliminarDeCesta(ByVal Id As Integer, ByVal IdOrden As Integer, ByVal Persona As String)
        DBServer.Ordenes_EliminarDeCestaEP(0, Id, IdOrden, Persona)
    End Sub
    ''' <summary>
    ''' Funci�n que vac�a completamente la cesta de la compra de un usuario
    ''' </summary>
    ''' <param name="sPer">C�digo de la persona del usuario que esta utilizando la aplicaci�n</param>
    ''' <returns>Un n�mero entero que valdr� 0 si todo ha ido bien, y 1 en caso de error.</returns>
    ''' <remarks>
    ''' Llamada desde: La p�gina EmisionPedido, m�todo BtnAceptarVaciarCesta_Click
    ''' Tiempo m�ximo: 0,2 seg</remarks>
    Public Function VaciarCesta(ByVal sPer As String) As CTESError
        Dim Resultado As New CTESError
        Resultado.NumError = 0
        Resultado.NumError = mDBServer.Ordenes_VaciarCesta(sPer)
        Return Resultado
    End Function
#End Region
#Region "Estructuras para Tipos Tabla"
    '''Revisado por: blp. Fecha: 20/07/2012
    ''' <summary>
    ''' Funci�n que crea la estructura de una tabla de IDs de �rdenes
    ''' </summary>
    ''' <returns>DataTable vac�o</returns>
    ''' <remarks>Llamada desde COrdenes.vb. M�x. 0,1 seg.</remarks>
    Public Function CrearTablaOrdenesId() As DataTable
        Dim dtOrdenesId As DataTable = New DataTable("OrdenesId")
        dtOrdenesId.Columns.Add("ID", GetType(Integer))
        Return dtOrdenesId
    End Function
    '''Revisado por: blp. Fecha: 20/07/2012
    ''' <summary>
    ''' Carga en una tabla los Ids de Orden pasados por lista
    ''' </summary>
    ''' <param name="OrdenesId">Lista con los Ids de las �rdenes</param>
    ''' <returns>Una tabla con los c�digos, nombres y apellidos</returns>
    ''' <remarks>
    ''' Llamada desde: Seguimiento.aspx.vb
    ''' Tiempo m�ximo: 0,1 sec.
    ''' </remarks>
    Public Function CargarOrdenesId(ByVal OrdenesId As System.Collections.Generic.List(Of Object)) As DataTable
        Dim dtOrdenesId As DataTable = CrearTablaOrdenesId()
        For Each ordenId As Object In OrdenesId
            dtOrdenesId.Rows.Add({CInt(ordenId)})
        Next
        Return (dtOrdenesId)
    End Function
    '''Revisado por: blp. Fecha: 30/12/2012
    ''' <summary>
    ''' Funci�n que crea la estructura de la tabla de partidas que usaremos en los filtros para obtener las �rdenes
    ''' </summary>
    ''' <returns>DataTable vac�o</returns>
    ''' <remarks>Llamada desde COrdenes.vb. M�x. 0,1 seg.</remarks>
    Public Function CrearTablaPartidasEP() As DataTable
        Dim dtPartidas As DataTable = New DataTable("PARTIDASPRES")
        dtPartidas.Columns.Add("PRES0", GetType(System.String))
        dtPartidas.Columns.Add("PRES1", GetType(System.String))
        dtPartidas.Columns.Add("PRES2", GetType(System.String))
        dtPartidas.Columns.Add("PRES3", GetType(System.String))
        dtPartidas.Columns.Add("PRES4", GetType(System.String))
        Return dtPartidas
    End Function
    '''Revisado por: blp. Fecha: 21/08/2013
    ''' <summary>
    ''' Funci�n que crea la estructura de la tabla de partidas que usaremos en los filtros para obtener los contratos en el visor de contratos de PM
    ''' </summary>
    ''' <returns>DataTable vac�o</returns>
    ''' <remarks>Llamada desde COrdenes.vb. M�x. 0,1 seg.</remarks>
    Public Function CrearTablaPartidasPM() As DataTable
        Dim dtPartidas As DataTable = New DataTable("PARTIDASPRES")
        dtPartidas.Columns.Add("PRES0", GetType(System.String))
        dtPartidas.Columns.Add("UON1UON2UON3UON4PRESN", GetType(System.String))
        Return dtPartidas
    End Function
    '''Revisado por: blp. Fecha: 03/12/2012
    ''' <summary>
    ''' Carga en una tabla las partidas presupuestarias pasadas por lista
    ''' </summary>
    ''' <param name="PartidasPres">Lista con las partidas</param>
    ''' <returns>Una tabla con las partidas</returns>
    ''' <remarks>
    ''' Llamada desde: Seguimiento.aspx.vb
    ''' Tiempo m�ximo: 0,1 sec.
    ''' </remarks>
    Public Function CargarTablaPartidas(ByVal PartidasPres As String(), ByVal origen As TiposDeDatos.Aplicaciones) As DataTable
        Dim dtPartidasPres As DataTable
        If origen = TiposDeDatos.Aplicaciones.EP Then
            dtPartidasPres = CrearTablaPartidasEP()
        Else
            dtPartidasPres = CrearTablaPartidasPM()
        End If
        Dim arSeparadores As String()
        ReDim Preserve arSeparadores(0)
        For Each Partida As String In PartidasPres
            If origen = TiposDeDatos.Aplicaciones.EP Then
                arSeparadores(0) = TextosSeparadores.dosArrobas
                Dim arrayPartidas As String() = Partida.Split(arSeparadores, StringSplitOptions.None)
                Dim arrayPartidasTipado As String() = {"", "", "", "", ""}
                For x As Integer = 0 To arrayPartidas.Length - 1
                    arrayPartidasTipado(x) = arrayPartidas(x)
                Next
                dtPartidasPres.Rows.Add(arrayPartidasTipado)
            Else
                arSeparadores(0) = "#"
                Dim arrayPartidas As String() = Partida.Split(arSeparadores, StringSplitOptions.None)
                dtPartidasPres.Rows.Add({arrayPartidas(0), Partida})
            End If
        Next
        Return (dtPartidasPres)
    End Function
    '''Revisado por: blp. Fecha: 30/11/2012
    ''' <summary>
    ''' Funci�n que crea la estructura de la tabla de Categorias que usaremos en los filtros para obtener las �rdenes
    ''' </summary>
    ''' <returns>DataTable vac�o</returns>
    ''' <remarks>Llamada desde COrdenes.vb. M�x. 0,1 seg.</remarks>
    Public Function CrearTablaCategorias() As DataTable
        Dim dtPartidas As DataTable = New DataTable("CATEGORIAS")
        dtPartidas.Columns.Add("CAT1", GetType(System.String))
        dtPartidas.Columns.Add("CAT2", GetType(System.String))
        dtPartidas.Columns.Add("CAT3", GetType(System.String))
        dtPartidas.Columns.Add("CAT4", GetType(System.String))
        dtPartidas.Columns.Add("CAT5", GetType(System.String))
        Return dtPartidas
    End Function
    '''Revisado por: blp. Fecha: 03/12/2012
    ''' <summary>
    ''' Carga en una tabla las categor�as pasadas por lista
    ''' </summary>
    ''' <param name="Categorias">Lista con las Categor�as</param>
    ''' <returns>Una tabla con las partidas</returns>
    ''' <remarks>
    ''' Llamada desde: Seguimiento.aspx.vb
    ''' Tiempo m�ximo: 0,1 sec.
    ''' </remarks>
    Public Function CargarTablaCategorias(ByVal Categorias As String) As DataTable
        Dim dtCategorias As DataTable = CrearTablaCategorias()
        Dim arrayCategorias As String() = Split(Categorias, "_")
        Dim arrayCategoriasTipado As String() = {"", "", "", "", ""}
        For x As Integer = 0 To arrayCategorias.Length - 1
            arrayCategoriasTipado(x) = arrayCategorias(x)
        Next
        dtCategorias.Rows.Add(arrayCategoriasTipado)
        Return (dtCategorias)
    End Function
    '''Revisado por: blp. Fecha: 30/11/2012
    ''' <summary>
    ''' Funci�n que crea la estructura de la tabla de String Gen�rico que usaremos en los filtros para obtener las �rdenes
    ''' </summary>
    ''' <returns>DataTable vac�o</returns>
    ''' <remarks>Llamada desde COrdenes.vb. M�x. 0,1 seg.</remarks>
    Public Function CrearTablaStrings() As DataTable
        Dim dtPartidas As DataTable = New DataTable("STRINGS")
        dtPartidas.Columns.Add("STRING", GetType(System.String))
        Return dtPartidas
    End Function
    '''Revisado por: blp. Fecha: 03/12/2012
    ''' <summary>
    ''' Carga en una tabla los strings pasados por par�metro
    ''' </summary>
    ''' <param name="sStrings">Cadena con los Strings</param>
    ''' <param name="Separador">String separador</param>
    ''' <returns>Una tabla con los strings</returns>
    ''' <remarks>
    ''' Llamada desde: Seguimiento.aspx.vb
    ''' Tiempo m�ximo: 0,1 sec.
    ''' </remarks>
    Public Function CargarTablaStrings(ByVal sStrings As String, ByVal Separador As String) As DataTable
        Dim dtStrings As DataTable = CrearTablaStrings()
        Dim arrayStrings As String() = Split(sStrings, Separador)
        For Each sString As Object In arrayStrings
            dtStrings.Rows.Add({sString})
        Next
        Return (dtStrings)
    End Function
#End Region
    ''' <summary>
    ''' Constructor de la clase
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si est� autenticado</param>
    ''' <remarks>
    ''' Llamada desde: Todos los sitios en los que se instancie un objeto de la clase COrdenes
    ''' Tiempo m�ximo: 0 sec
    ''' </remarks>
    Public Sub New(ByRef dbserver As Fullstep.FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="Idioma"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Obtener_DetalleAlbaran(ByVal Albaran As String, ByVal FechaAlbaran As DateTime, _
                                           ByVal Prove As String, ByVal Idioma As String) As DataSet
        Return DBServer.Ordenes_Obtener_DetalleAlbaran(Albaran, FechaAlbaran, Prove, Idioma)
    End Function
    ''' <summary>
    ''' Lineas a recepcionar
    ''' </summary>
    ''' <param name="IdLineasPedido">l�neas de las que recuperar los datos</param>
    ''' <param name="Idioma">Idioma</param>
    ''' <param name="SMActivado">Sm Activado</param>
    ''' <returns>Dataset con las l�neas</returns>
    ''' <remarks>Llamadas desde Recepcion.aspx.vb. 1 seg.</remarks>    
    Public Function Ordenes_Obtener_Lineas_Recepcion(ByVal IdLineasPedido As String, ByVal Idioma As String, ByVal SMActivado As Boolean) As DataSet
        Return DBServer.Ordenes_Obtener_Lineas_Recepcion(IdLineasPedido, Idioma, SMActivado)
    End Function
    ''' <summary>
    ''' Lineas pendientes de recepcionar
    ''' </summary>
    ''' <param name="Ordenes">�rdenes de las que se quieren recuperar las l�neas</param>
    ''' <param name="Idioma">Idioma</param>
    ''' <param name="SMActivado">Sm Activado</param>
    ''' <returns>Dataset con las l�neas</returns>
    ''' <remarks>Llamadas desde Recepcion.aspx.vb. 1 seg.</remarks>
    Public Function Ordenes_Obtener_Lineas_PendientesRecepcionar(ByVal Ordenes As String, ByVal Idioma As String, ByVal SMActivado As Boolean) As DataSet
        Return DBServer.Ordenes_Obtener_Lineas_PendientesRecepcionar(Ordenes, Idioma, SMActivado)
    End Function
    ''' Revisado por: blp. Fecha: 17/01/2012
    ''' <summary>
    ''' Devuelve los datos para mostrar en los combos del buscador de seguimiento de pedidos en EP.
    ''' </summary>
    ''' <param name="Idioma">C�digo de idioma</param>
    ''' <param name="CodPersona">C�digo de la tabla PER, del usuario del que queremos cargar los combos</param>
    ''' <param name="VerPedidosOtrosUsuarios">Permiso para ver pedidos de otros usuarios</param>
    ''' <returns>dataset con varias tablas que contienen la informaci�n a mostrar en los combos</returns>
    ''' <remarks>Llamada desde Seguimiento.aspx.vb-->getDropDownsInfo. M�x. 1 seg.</remarks>
    Public Function ObtenerDatosCombosSeguimiento(ByVal Idioma As FSNLibrary.Idioma, ByVal CodPersona As String, ByVal VerPedidosOtrosUsuarios As Boolean) As DataSet
        Authenticate()
        Dim dsOrdenes As DataSet = DBServer.Ordenes_ObtenerDatosCombosSeguimiento(Idioma, CodPersona, VerPedidosOtrosUsuarios)

        dsOrdenes.Tables(0).TableName = "EMPRESAS"
        dsOrdenes.Tables(1).TableName = "RECEPTORES"
        dsOrdenes.Tables(2).TableName = "GESTORES"
        dsOrdenes.Tables(3).TableName = "PARTIDASPRES0"

        Return dsOrdenes
    End Function
    ''' Revisado por: blp. Fecha: 24/05/2013
    ''' <summary>
    ''' Recuperamos el a�o de la orden m�s antigua emitida.
    ''' </summary>
    ''' <returns>nullable integer con el a�o</returns>
    ''' <remarks>Llamada desde Aprobacion.aspx.vb, Recepcion.aspx.vb, Seguimiento.aspx.vb. M�ximo 0,1 seg.</remarks>
    Public Function ObtenerMinAnyo() As Nullable(Of Integer)
        Dim Anyo As Nullable(Of Integer) = DBServer.Ordenes_ObtenerMinAnyo()
        Return Anyo
    End Function
    ''' <summary>
    ''' Carga la informaci�n de las lineas de pedido de una orden de entrega
    ''' </summary>
    ''' <param name="CodPer">C�digo de Persona del usuario</param>
    ''' <param name="Idioma">Idioma actual</param>
    Public Function DevolverLineasOrden(ByVal CodPer As String, ByVal Idioma As FSNLibrary.Idioma, ByVal idOrden As Long, ByVal InstanciaAprob As Long, ByVal bMostrarLineasBajaLog As Boolean,
                                        Optional ByVal bDesdeAprob As Boolean = False) As DataSet
        Dim ds As DataSet = mDBServer.Ordenes_CargarLineasPedido(CodPer, Idioma, idOrden, InstanciaAprob, bMostrarLineasBajaLog, bDesdeAprob)
        Dim adoRs As New DataSet
        ds.Tables(0).TableName = "LINEAS_PEDIDO"
        Dim col As DataColumn() = New DataColumn(0) {}
        col(0) = ds.Tables("LINEAS_PEDIDO").Columns("IDLINEA")
        ds.Tables("LINEAS_PEDIDO").PrimaryKey = col
        Return (ds)
    End Function
    Public Function BuscarDetalleOrdenes(ByVal Idioma As FSNLibrary.Idioma, dtOrdenes As DataTable, bsmactivo As Boolean, iVerBajasOrdenes As Integer) As DataSet
        Authenticate()
        Dim dsOrdenes As DataSet = DBServer.CargarDetalleOrdenes(Idioma, dtOrdenes, bsmactivo, iVerBajasOrdenes)
        Return dsOrdenes
    End Function
#Region "Atributos Marcados para Mostrarse en Recepci�n"
    Public Function CargarAtributosMarcadosMostrarseRecepci�n(ByVal idOrden As Long) As DataSet
        Dim ds As DataSet = mDBServer.Cargar_Atributos_Orden(idOrden,, True)
        Return ds
    End Function
#End Region
End Class