
Public Class CCategoriasN2
    Inherits Lista(Of CCategoriaN2)

    ''' <summary>
    ''' Agrega un elemento CCategoriaN2 a la lista
    ''' </summary>
    ''' <param name="Id">ID de la categor�a</param>
    ''' <param name="Cod">C�digo de la categor�a</param>
    ''' <param name="Den">Denominaci�n</param>
    ''' <param name="Cat1">ID de la categor�a de nivel 1</param>
    ''' <param name="Cat1Cod">C�digo de la categor�a de nivel 1</param>
    ''' <param name="iNumHojas">N�mero de hijos de la categor�a</param>
    ''' <param name="vObs">Observaciones</param>
    ''' <returns>El elemento CCategoriaN2 agregado</returns>
    Public Overloads Function Add(ByVal Id As Integer, ByVal Cod As String, ByVal Den As String, ByVal Cat1 As Integer, ByVal Cat1Cod As String, ByVal iNumHojas As Short, ByVal vObs As Object) As CCategoriaN2
        Dim objnewmember As New CCategoriaN2(mDBServer, mIsAuthenticated)

        With objnewmember
            .Id = Id
            .Cod = Cod
            .Den = Den
            .Cat1 = Cat1
            .Cat1Cod = Cat1Cod
            .NumHojas = iNumHojas
            .Obs = vObs
        End With

        Me.Add(objnewmember)
        Return objnewmember
    End Function

    ''' <summary>
    ''' Constructor de la clase CCategoriasN2
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si est� autenticado</param>
    Public Sub New(ByRef dbserver As Fullstep.FSNDataBaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
    End Sub

End Class