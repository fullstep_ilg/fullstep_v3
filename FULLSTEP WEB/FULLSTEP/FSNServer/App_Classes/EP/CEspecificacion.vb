Imports Fullstep.FSNLibrary

<Serializable()> _
Public Class CEspecificacion
    Inherits Security

    Private moCat As Object
    Private mlId As Integer
    Private mlIdCatLin As Integer
    Private msComentario As String
    Private msNombre As String
    Private mdFecact As Date
    Private mlDataSize As Integer 'Tamanyo del contenido en bytes del campo mvarData

    Public Property DataSize() As Integer
        Get
            DataSize = mlDataSize
        End Get
        Set(ByVal Value As Integer)

            mlDataSize = Value
        End Set
    End Property

    Public Property Fecha() As Date
        Get
            Fecha = mdFecact
        End Get
        Set(ByVal Value As Date)
            mdFecact = Value
        End Set
    End Property

    Public Property Nombre() As String
        Get
            Nombre = msNombre
        End Get
        Set(ByVal Value As String)
            msNombre = Value
        End Set
    End Property

    Public Property Comentario() As String
        Get
            Comentario = msComentario
        End Get
        Set(ByVal Value As String)
            msComentario = Value
        End Set
    End Property

    Public Property Id() As Integer
        Get
            Id = mlId
        End Get
        Set(ByVal Value As Integer)
            mlId = Value
        End Set
    End Property

    Public Property IdCatLin() As Integer
        Get
            Return mlIdCatLin
        End Get
        Set(ByVal value As Integer)
            mlIdCatLin = value
        End Set
    End Property

    Public Property Cat() As Object
        Get
            Cat = moCat
        End Get
        Set(ByVal Value As Object)
            moCat = Value
        End Set
    End Property
    ''' <summary>
    ''' Constructor de la clase CEspecificacion
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si est� autenticado</param>
    Public Sub New(ByRef dbserver As Fullstep.FSNDataBaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
    End Sub
End Class