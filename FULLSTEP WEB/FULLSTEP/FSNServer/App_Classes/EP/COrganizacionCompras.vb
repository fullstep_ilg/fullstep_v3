﻿Imports System.Configuration
Imports System.Threading
Imports System.Web

<Serializable()> _
Public Class COrganizacionCompras
    Inherits Security

    Private m_sCod As String
    Private m_sDen As String

    Public Sub New(ByVal sCod As String, ByVal sDen As String)
        m_sCod = sCod
        m_sDen = sDen
    End Sub

    Public Property Cod() As String
        Get
            Cod = m_sCod
        End Get
        Set(ByVal Value As String)
            m_sCod = Value
        End Set
    End Property

    Public Property Den() As String
        Get
            Den = m_sDen
        End Get
        Set(ByVal Value As String)
            m_sDen = Value
        End Set
    End Property
End Class
