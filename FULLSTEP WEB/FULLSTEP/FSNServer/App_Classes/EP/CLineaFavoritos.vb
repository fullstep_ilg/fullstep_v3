Imports Fullstep.FSNLibrary

<Serializable()> _
Public Class CLineaFavoritos
    Inherits Security

    Private msUsu As String
    Private mlId As Integer
    Private msCod As Object
    Private msGmn1 As Object
    Private msGmn2 As Object
    Private msGmn3 As Object
    Private msGmn4 As Object
    Private msProve As String
    Private msDen As String
    Private msArtExt As Object
    Private msDescripcionVisible As String
    Private msGenerico As Integer 'Favoritos generico
    Private msDest As String
    Private msDestDen As String
    Private msUP As Object
    Private msUniDen As Object
    Private mdPrecUc As Object
    Private mdCantPed As Object
    Private mdCantAdj As Object
    Private mdFC As Object
    Private mdImporte As Object
    Private mdtFecEntrega As Object
    Private mbEntregaObl As Object
    Private mlCat1 As Object
    Private msCat1Den As Object
    Private mlCat2 As Object
    Private msCat2Den As Object
    Private mlCat3 As Object
    Private msCat3Den As Object
    Private mlCat4 As Object
    Private msCat4Den As Object
    Private mlCat5 As Object
    Private msCat5Den As Object
    Private msObs As Object
    Private msCat As Object
    Private msCatRama As Object
    Private msComent As Object
    Private miAnyoProce As Object
    Private mlProce As Object
    Private mlItem As Object
    Private mlLineaCatalogo As Object
    Private moAdjuntos As CAdjuntos
    Private moCampo1 As CCampo
    Private moCampo2 As CCampo
    Private mvObsAdjun As Object
    Private mdCant_Min As Object
    Private mbModifPrec As Object
    Private iAlmacen As Integer
    Private strAlmacen As String
    Private intActivo As Integer
    Private mImputaciones As FSNServer.Imputaciones
    Private mImputaciones_EP As List(Of ImputacionPedido)
    Private mTipoRecepcion As Integer
    Private mImportePedido As Double
    Private mGestorCod As String
    Private mDesvioRecepcion As Double
    Private _otrosDatos As List(Of CAtributoPedido)
    Private _costes As List(Of CAtributoPedido)
    Private _descuentos As List(Of CAtributoPedido)
    Private _impuestos As List(Of CImpuestos)
    Private mCentroAprovisionamiento As String

    Public Property ID() As Integer
        Get
            ID = mlId
        End Get
        Set(ByVal Value As Integer)
            mlId = Value
        End Set
    End Property
    Public Property Usuario() As String
        Get
            Usuario = msUsu
        End Get
        Set(ByVal Value As String)
            msUsu = Value
        End Set
    End Property
    Public Property Cod() As Object
        Get
            Cod = msCod
        End Get
        Set(ByVal Value As Object)
            msCod = Value
        End Set
    End Property
    Public Property GMN1() As Object
        Get
            GMN1 = msGmn1
        End Get
        Set(ByVal Value As Object)
            msGmn1 = Value
        End Set
    End Property
    Public Property GMN2() As Object
        Get
            GMN2 = msGmn2
        End Get
        Set(ByVal Value As Object)
            msGmn2 = Value
        End Set
    End Property
    Public Property GMN3() As Object
        Get
            GMN3 = msGmn3
        End Get
        Set(ByVal Value As Object)
            msGmn3 = Value
        End Set
    End Property
    Public Property GMN4() As Object
        Get
            GMN4 = msGmn4
        End Get
        Set(ByVal Value As Object)
            msGmn4 = Value
        End Set
    End Property
    Public Property Prove() As String
        Get
            Prove = msProve
        End Get
        Set(ByVal Value As String)
            msProve = Value
        End Set
    End Property
    Public Property Den() As String
        Get
            Den = msDen
        End Get
        Set(ByVal Value As String)
            msDen = Value
        End Set
    End Property
    Public Property ArtExt() As Object
        Get
            ArtExt = msArtExt
        End Get
        Set(ByVal Value As Object)
            msArtExt = Value
        End Set
    End Property
    Public Property Generico() As Integer
        Get
            Generico = msGenerico
        End Get
        Set(ByVal Value As Integer)
            msGenerico = Value
        End Set
    End Property
    Public Property DescripcionVisible() As String
        Get
            DescripcionVisible = msDescripcionVisible
        End Get
        Set(ByVal Value As String)
            msDescripcionVisible = Value
        End Set
    End Property
    Public Property Dest() As String
        Get
            Dest = msDest
        End Get
        Set(ByVal Value As String)
            msDest = Value
        End Set
    End Property
    Public Property DestDen() As String
        Get
            DestDen = msDestDen
        End Get
        Set(ByVal Value As String)
            msDestDen = Value
        End Set
    End Property
    Public Property UP() As Object
        Get
            UP = msUP
        End Get
        Set(ByVal Value As Object)
            msUP = Value
        End Set
    End Property
    Public Property UniDen() As Object
        Get
            UniDen = msUniDen
        End Get
        Set(ByVal Value As Object)
            msUniDen = Value
        End Set
    End Property
    Public Property PrecUc() As Object
        Get
            PrecUc = mdPrecUc
        End Get
        Set(ByVal Value As Object)
            mdPrecUc = Value
        End Set
    End Property
    Public Property CantPed() As Object
        Get
            CantPed = mdCantPed
        End Get
        Set(ByVal Value As Object)
            mdCantPed = Value
        End Set
    End Property
    Public Property CantAdj() As Object
        Get
            CantAdj = mdCantAdj
        End Get
        Set(ByVal Value As Object)
            mdCantAdj = Value
        End Set
    End Property
    Public Property FC() As Object
        Get
            FC = mdFC
        End Get
        Set(ByVal Value As Object)
            mdFC = Value
        End Set
    End Property
    Public Property Importe() As Object
        Get
            Importe = mdImporte
        End Get
        Set(ByVal Value As Object)
            mdImporte = Value
        End Set
    End Property
    Public Property FecEntrega() As Object
        Get
            FecEntrega = mdtFecEntrega
        End Get
        Set(ByVal Value As Object)
            mdtFecEntrega = Value
        End Set
    End Property
    Public Property EntregaObl() As Object
        Get
            EntregaObl = mbEntregaObl
        End Get
        Set(ByVal Value As Object)
            mbEntregaObl = Value
        End Set
    End Property
    Public Property Cat1() As Object
        Get
            Cat1 = mlCat1
        End Get
        Set(ByVal Value As Object)
            mlCat1 = Value
        End Set
    End Property
    Public Property Cat1Den() As Object
        Get
            Cat1Den = msCat1Den
        End Get
        Set(ByVal Value As Object)
            msCat1Den = Value
        End Set
    End Property
    Public Property Cat2() As Object
        Get
            Cat2 = mlCat2
        End Get
        Set(ByVal Value As Object)
            mlCat2 = Value
        End Set
    End Property
    Public Property Cat2Den() As Object
        Get
            Cat2Den = msCat2Den
        End Get
        Set(ByVal Value As Object)
            msCat2Den = Value
        End Set
    End Property
    Public Property Cat3() As Object
        Get
            Cat3 = mlCat3
        End Get
        Set(ByVal Value As Object)
            mlCat3 = Value
        End Set
    End Property
    Public Property Cat3Den() As Object
        Get
            Cat3Den = msCat3Den
        End Get
        Set(ByVal Value As Object)
            msCat3Den = Value
        End Set
    End Property
    Public Property Cat4() As Object
        Get
            Cat4 = mlCat4
        End Get
        Set(ByVal Value As Object)
            mlCat4 = Value
        End Set
    End Property
    Public Property Cat4Den() As Object
        Get
            Cat4Den = msCat4Den
        End Get
        Set(ByVal Value As Object)
            msCat4Den = Value
        End Set
    End Property
    Public Property Cat5() As Object
        Get
            Cat5 = mlCat5
        End Get
        Set(ByVal Value As Object)
            mlCat5 = Value
        End Set
    End Property
    Public Property Cat5Den() As Object
        Get
            Cat5Den = msCat5Den
        End Get
        Set(ByVal Value As Object)
            msCat5Den = Value
        End Set
    End Property
    Public Property Obs() As Object
        Get
            Obs = msObs
        End Get
        Set(ByVal Value As Object)
            msObs = Value
        End Set
    End Property
    Public Property Cat() As Object
        Get
            Cat = msCat
        End Get
        Set(ByVal Value As Object)
            msCat = Value
        End Set
    End Property
    Public Property CatRama() As Object
        Get
            CatRama = msCatRama
        End Get
        Set(ByVal Value As Object)
            msCatRama = Value
        End Set
    End Property
    Public Property Coment() As Object
        Get
            Coment = msComent
        End Get
        Set(ByVal Value As Object)
            msComent = Value
        End Set
    End Property
    Public Property AnyoProce() As Object
        Get
            AnyoProce = miAnyoProce
        End Get
        Set(ByVal Value As Object)
            miAnyoProce = Value
        End Set
    End Property
    Public Property Proce() As Object
        Get
            Proce = mlProce
        End Get
        Set(ByVal Value As Object)
            mlProce = Value
        End Set
    End Property
    Public Property Item() As Object
        Get
            Item = mlItem
        End Get
        Set(ByVal Value As Object)
            mlItem = Value
        End Set
    End Property
    Public Property LineaCatalogo() As Object
        Get
            LineaCatalogo = mlLineaCatalogo
        End Get
        Set(ByVal Value As Object)
            mlLineaCatalogo = Value
        End Set
    End Property
    Public Property Adjuntos() As CAdjuntos
        Get
            Adjuntos = moAdjuntos
        End Get
        Set(ByVal Value As CAdjuntos)
            moAdjuntos = Value
        End Set
    End Property
    Public Property Campo1() As CCampo
        Get
            Campo1 = moCampo1
        End Get
        Set(ByVal Value As CCampo)
            moCampo1 = Value
        End Set
    End Property
    Public Property Campo2() As CCampo
        Get
            Campo2 = moCampo2
        End Get
        Set(ByVal Value As CCampo)
            moCampo2 = Value
        End Set
    End Property
    Public Property ObsAdjun() As Object
        Get
            ObsAdjun = mvObsAdjun
        End Get
        Set(ByVal Value As Object)
            mvObsAdjun = Value
        End Set
    End Property
    Public Property Cant_Min() As Object
        Get
            Cant_Min = mdCant_Min
        End Get
        Set(ByVal Value As Object)
            mdCant_Min = Value
        End Set
    End Property
    Public Property ModifPrec() As Object
        Get
            ModifPrec = mbModifPrec
        End Get
        Set(ByVal Value As Object)
            mbModifPrec = Value
        End Set
    End Property
    Public Property IdAlmacen() As Integer
        Get
            Return iAlmacen
        End Get
        Set(ByVal value As Integer)
            iAlmacen = value
        End Set
    End Property
    Public Property Almacen() As String
        Get
            Return strAlmacen
        End Get
        Set(ByVal value As String)
            strAlmacen = value
        End Set
    End Property
    Public Property Activo() As Integer
        Get
            Return intActivo
        End Get
        Set(ByVal value As Integer)
            intActivo = value
        End Set
    End Property
    Public Property Pres5_ImportesImputados() As FSNServer.Imputaciones
        Get
            Return mImputaciones
        End Get
        Set(ByVal value As FSNServer.Imputaciones)
            mImputaciones = value
        End Set
    End Property
    Public Property Pres5_ImportesImputados_EP() As List(Of ImputacionPedido)
        Get
            Return mImputaciones_EP
        End Get
        Set(ByVal value As List(Of ImputacionPedido))
            mImputaciones_EP = value
        End Set
    End Property
    Public Property TipoRecepcion() As Integer
        Get
            Return mTipoRecepcion
        End Get
        Set(value As Integer)
            mTipoRecepcion = value
        End Set
    End Property
    Public Property ImportePedido() As Double
        Get
            Return Me.mImportePedido
        End Get
        Set(value As Double)
            Me.mImportePedido = value
        End Set
    End Property
    Public Property DesvioRecepcion() As Double
        Get
            Return Me.mDesvioRecepcion
        End Get
        Set(value As Double)
            Me.mDesvioRecepcion = value
        End Set
    End Property
    Public Property GestorCod() As String
        Get
            Return Me.mGestorCod
        End Get
        Set(value As String)
            Me.mGestorCod = value
        End Set
    End Property
    Public Property OtrosDatos() As List(Of CAtributoPedido)
        Get
            Return _otrosDatos
        End Get
        Set(ByVal value As List(Of CAtributoPedido))
            _otrosDatos = value
        End Set
    End Property
    Public Property Costes() As List(Of CAtributoPedido)
        Get
            Return _costes
        End Get
        Set(ByVal value As List(Of CAtributoPedido))
            _costes = value
        End Set
    End Property
    Public Property Descuentos() As List(Of CAtributoPedido)
        Get
            Return _descuentos
        End Get
        Set(ByVal value As List(Of CAtributoPedido))
            _descuentos = value
        End Set
    End Property
    Public Property Impuestos() As List(Of CImpuestos)
        Get
            Return _impuestos
        End Get
        Set(ByVal value As List(Of CImpuestos))
            _impuestos = value
        End Set
    End Property
    Public Property CentroAprovisionamiento() As String
        Get
            Return mCentroAprovisionamiento
        End Get
        Set(ByVal value As String)
            mCentroAprovisionamiento = value
        End Set
    End Property

    ''' <summary>
    ''' Funci�n que devuelve en un objeto de la clase cAdjuntos los archivos adjuntos de una l�nea de pedido favorito, dado su id
    ''' </summary>
    ''' <param name="bCargarEnColeccion">Variable booleana que indica si se debe cargar los adjuntos dentro de la colecci�n privada del objeto cLineaFavoritos que la llama</param>
    ''' <returns>Un objeto de la clase cAdjuntos con los datos de los archivos adjuntos de la l�nea de favoritos</returns>
    ''' <remarks>
    ''' Llamada desde: La p�gina Favoritos.aspx, m�todo grdLineasPedidos_RowCommand
    ''' Tiempo m�ximo: 0,32 seg</remarks>
    Public Function CargarAdjuntos(ByVal bCargarEnColeccion As Boolean) As CAdjuntos
        Dim ador As New DataSet
        Dim oAdjuntos As New CAdjuntos(mDBServer, mIsAuthenticated)
        ador = mDBServer.LineaFavoritos_CargarAdjuntos(mlId, msUsu)
        For Each fila As DataRow In ador.Tables(0).Rows
            'He modificado la llamada al metodo, poniendo los parametros que estaban vacios por sus valores por defecto, para quitar la clausula "Optional"
            oAdjuntos.Add(fila.Item("ID"), fila.Item("FECACT"), fila.Item("NOM"), fila.Item("TAMANO"), fila.Item("COM"), Nothing, Nothing, "", mlId, TiposDeDatos.Adjunto.Tipo.Favorito_Linea_Pedido)
        Next
        ador.Clear()
        ador = Nothing


        If bCargarEnColeccion Then
            moAdjuntos = oAdjuntos
        End If

        CargarAdjuntos = oAdjuntos
        Return oAdjuntos
        oAdjuntos = Nothing
    End Function
    ''' <summary>
    ''' Funci�n que elimina una l�nea de pedido favorito de la BBDD, eliminando los adjuntos, y actualizando el importe de la orden de favoritos
    ''' </summary>
    ''' <param name="lOrden">Identificador de la orden a la que pertenece la l�nea</param>
    ''' <returns>Un objeto de la clase cTESError</returns>
    ''' <remarks>
    ''' Llamada desde: La p�gina Favoritos.aspx, m�todo grdLineasPedidos_RowCommand
    ''' Tiempo m�ximo: 0,42 seg</remarks>
    Public Function EliminarFavorito(ByVal lOrden As Integer) As CTESError
        Dim TESError As New CTESError
        TESError.NumError = 0
        TESError.NumError = mDBServer.LineaFavoritos_EliminarFavorito(mlId, lOrden, msUsu)
        Return TESError
    End Function
    ''' <summary>
    ''' Constructor de un objeto de la clase CLineaFavoritos
    ''' </summary>
    ''' <param name="dbserver">Servidor de la BBDD</param>
    ''' <param name="isAuthenticated">Si est� autenticado</param>
    ''' <remarks>
    ''' Llamada desde: Todos los sitios desde donde se isntancien objetos CLIneaFavoritos
    ''' Tiempo m�ximo = 0 sec.</remarks>
    Public Sub New(ByRef dbserver As Fullstep.FSNDataBaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
        Pres5_ImportesImputados_EP = New List(Of ImputacionPedido)
        Impuestos = New List(Of CImpuestos)
    End Sub
    ''' <summary>
    ''' Constructor de un objeto de la clase CLinea
    ''' </summary>
    ''' <remarks>
    ''' Llamada desde: Todos los sitios desde donde se isntancien objetos CLInea
    ''' Tiempo m�ximo = 0 sec.</remarks>
    Public Sub New()
        Pres5_ImportesImputados_EP = New List(Of ImputacionPedido)
        Impuestos = New List(Of CImpuestos)
    End Sub
    ''' <summary>
    ''' Funci�n que actualiza una l�nea de favoritos de la Base de Datos
    ''' </summary>
    ''' <param name="CodUsu">C�digo de usuario que esta utilizando la aplicaci�n</param>
    ''' <returns>Un valor entero que ser� 0 si se ha ejecutado correctamente y 1 en caso contrario</returns>
    ''' <remarks>
    ''' Llamada desde: EPWeb\Favoritos\BtnAceptarAnyadido_Click.vb
    ''' Tiempo m�ximo: 0,2 seg</remarks>
    Public Function ActualizarLineaFavorito(ByVal CodUsu As String) As Integer
        Authenticate()
        Dim Resul As String
        Dim sCampo1 As String = ""
        Dim sCampo2 As String = ""
        Dim centroSM As String = ""
        Dim partida As Integer
        If Campo1 IsNot Nothing Then
            sCampo1 = DBNullToStr(Campo1.Valor)
        End If
        If Campo2 IsNot Nothing Then
            sCampo2 = DBNullToStr(Campo2.Valor)
        End If
        If Pres5_ImportesImputados IsNot Nothing AndAlso Pres5_ImportesImputados.Any AndAlso Pres5_ImportesImputados(0).CentroCoste IsNot Nothing Then
            If Pres5_ImportesImputados(0).CentroCoste.CentroSM IsNot Nothing Then centroSM = Pres5_ImportesImputados(0).CentroCoste.CentroSM.Codigo
            If Pres5_ImportesImputados(0).Partida IsNot Nothing Then partida = Pres5_ImportesImputados(0).Partida.PresupuestoId
        End If
        'En la tabla FAVORITOS_LINEAS_PEDIDO hay que meter el precio del art�culo teniendo en cuenta el factor de conversi�n.
        'Como el PrecUc es el precio unitario, tenemos que multiplicarlo por el factor de conversi�n.
        Dim dbPrecioArt As Double
        If Me.FC <> 0 Then
            dbPrecioArt = DBNullToDbl(Me.PrecUc) * DBNullToDbl(Me.FC)
        Else
            dbPrecioArt = DBNullToDbl(Me.PrecUc)
        End If
        Resul = DBServer.LineaFavoritos_ActualizarLineaFavoritos(DBNullToDec(Me.ID), DBNullToStr(Me.Dest), dbPrecioArt, DBNullToDbl(Me.CantPed), DBNullToStr(Me.Obs), sCampo1, sCampo2, CodUsu, FecEntrega, EntregaObl, IdAlmacen, Activo, centroSM, partida, DBNullToStr(Me.DescripcionVisible))
    End Function
End Class