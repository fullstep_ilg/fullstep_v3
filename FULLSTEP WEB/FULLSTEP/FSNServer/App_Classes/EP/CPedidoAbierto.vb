﻿Public Class CPedidoAbierto
    Inherits Security

    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
    Public Function Cargar_PedidosAbiertos(ByVal Usu As String, ByVal RestricEmpresaUsuPedidoAbierto As Boolean, ByVal RestricEmpresaPerfUsuPedidoAbierto As Boolean, _
                                           Optional ByVal anio As Integer = -1, Optional ByVal numPedido As Integer = 0, Optional ByVal numOrden As Integer = 0, _
                                           Optional ByVal idEmpresa As Integer = 0, Optional ByVal codArticulo As String = "", Optional ByVal denArticulo As String = "", _
                                           Optional ByVal fecIniDesde As Date = Nothing, Optional ByVal fecIniHasta As Date = Nothing, _
                                           Optional ByVal fecFinDesde As Date = Nothing, Optional ByVal fecFinHasta As Date = Nothing, Optional ByVal codProve As String = "") As DataSet
        Authenticate()
        Return DBServer.Cargar_PedidosAbiertos(Usu, RestricEmpresaUsuPedidoAbierto, RestricEmpresaPerfUsuPedidoAbierto, anio, numPedido, numOrden, idEmpresa, _
                                               codArticulo, denArticulo, fecIniDesde, fecIniHasta, fecFinDesde, fecFinHasta, codProve)
    End Function
    Public Function Cargar_Lineas_PedidoAbierto(ByVal PerCod As String, ByVal Orden As Integer, ByVal NumberFormat As System.Globalization.NumberFormatInfo, Optional LineaPedido As Integer = 0) As Object
        Authenticate()
        Dim dsResultado As DataSet = DBServer.Cargar_Lineas_PedidoAbierto(PerCod, Orden, LineaPedido)

        Return dsResultado.Tables(0).Rows.OfType(Of DataRow).Select(Function(x) New With {Key .id = x("ID").ToString, .numLineaPedidoAbierto = Right("000" & x("NUM"), 3),
                    .codArticulo = x("CODARTICULO").ToString, .denArticulo = IIf(x("DESCR_LIBRE").ToString <> "", x("DESCR_LIBRE").ToString, x("DESCR").ToString),
                    .abierto = IIf(CType(x("PED_ABIERTO_TIPO"), Integer) = 2, FSNLibrary.FormatNumber(CType(x("IMPORTE_PED"), Double), NumberFormat), FSNLibrary.FormatNumber(CType(x("CANT_PED"), Double), NumberFormat)),
                    .pedido = IIf(CType(x("PED_ABIERTO_TIPO"), Integer) = 2, FSNLibrary.FormatNumber(CType(x("IMPORTE_PED_ABIERTO"), Double), NumberFormat), FSNLibrary.FormatNumber(CType(x("CANTIDAD_PED_ABIERTO"), Double), NumberFormat)),
                    .enCesta = IIf(CType(x("PED_ABIERTO_TIPO"), Integer) = 2, FSNLibrary.FormatNumber(CType(x("IMPORTE_PED_CESTA"), Double), NumberFormat), FSNLibrary.FormatNumber(CType(x("CANT_PED_CESTA"), Double), NumberFormat)),
                    .resto = IIf(CType(x("PED_ABIERTO_TIPO"), Integer) = 2, FSNLibrary.FormatNumber(CType(x("IMPORTE_PED"), Double) - CType(x("IMPORTE_PED_ABIERTO"), Double) - CType(x("IMPORTE_PED_CESTA"), Double), NumberFormat),
                                 FSNLibrary.FormatNumber(CType(x("CANT_PED"), Double) - CType(x("CANTIDAD_PED_ABIERTO"), Double) - CType(x("CANT_PED_CESTA"), Double), NumberFormat)),
                    .unidad = x("UP").ToString, .moneda = x("MON").ToString,
                    .precioUnidad = IIf(IsDBNull(x("PREC_UP")), 0, FSNLibrary.FormatNumber(CType(x("PREC_UP").ToString, Double), NumberFormat)), .tipoPedidoAbierto = x("TIPO").ToString}).ToList()
    End Function
    Public Function Cargar_FiltrosAvanzados_PedidoAbierto(ByVal Usu As String, _
                        ByVal RestricEmpresaUsuPedidoAbierto As Boolean, ByVal RestricEmpresaPerfUsuPedidoAbierto As Boolean) As Object
        Authenticate()
        Dim dsResultado As DataSet = DBServer.Cargar_FiltrosAvanzados_PedidoAbierto(Usu, RestricEmpresaUsuPedidoAbierto, RestricEmpresaPerfUsuPedidoAbierto)
        If (dsResultado.Tables.Count = 0 OrElse dsResultado.Tables(0).Rows.Count = 0) Then Return Nothing
        Dim o As Object = New With {.ComboAnios = dsResultado.Tables(0).Rows.OfType(Of DataRow).Select(Function(x) New With {Key .anio = x("ANYO").ToString}).ToList, _
                         .ComboEmpresas = dsResultado.Tables(1).Rows.OfType(Of DataRow).Select(Function(x) New With {Key .idEmpresa = x("ID").ToString, .denEmpresa = x("DEN").ToString}).ToList, _
                         .BuscadorArticulos = dsResultado.Tables(2).Rows.OfType(Of DataRow).Select(Function(x) New With {Key .id = x("ART").ToString, .name = x("ART").ToString & " - " & x("DESCR").ToString & x("DESCR_LIBRE").ToString}).tolist}
        Return {o, dsResultado.Tables(0).Rows.OfType(Of DataRow).Select(Function(x) CType(x("ANYO"), Integer)).Max}
    End Function
    Public Sub Agregar_Cesta_PedidoAbierto(ByVal PerCod As String, ByVal Orden_PedidoAbierto As Integer, ByVal IdLineaPedido As Integer, _
                                                    ByVal Cantidad As Double, ByVal Importe As Double)
        Authenticate()
        DBServer.Articulo_Agregar_Cesta_PedidoAbierto(PerCod, Orden_PedidoAbierto, IdLineaPedido, Cantidad, Importe)
    End Sub
    Public Function Obtener_Info_LineaPedidoAbierto(ByVal PerCod As String, ByVal IdCesta As Integer) As Object
        Authenticate()
        Dim ds As DataSet
        ds = DBServer.Obtener_Info_LineaPedidoAbierto(PerCod, IdCesta)
        Return New With {.resto = IIf(CType(ds.Tables(0).Rows(0)("TIPORECEPCION"), Integer) = 1, _
                            CType(ds.Tables(0).Rows(0)("IMPORTE_PED"), Double) - CType(ds.Tables(0).Rows(0)("IMPORTE_PED_ABIERTO"), Double) - CType(ds.Tables(0).Rows(0)("IMPORTE_PEDIDO_EN_CESTA"), Double), _
                            CType(ds.Tables(0).Rows(0)("CANT_PED"), Double) - CType(ds.Tables(0).Rows(0)("CANTIDAD_PED_ABIERTO"), Double) - CType(ds.Tables(0).Rows(0)("CANTIDAD_PEDIDA_EN_CESTA"), Double)), _
                        .tipoRecepcion = CType(ds.Tables(0).Rows(0)("TIPORECEPCION"), Integer), _
                        .anterior = IIf(CType(ds.Tables(0).Rows(0)("TIPORECEPCION"), Integer) = 1, CType(ds.Tables(0).Rows(0)("IMPORTE_ANTERIOR"), Double), CType(ds.Tables(0).Rows(0)("CANTIDAD_ANTERIOR"), Double))}
    End Function
    Public Function Obtener_PedidosAbiertos_ArticuloBuscado(ByVal Usu As String, ByVal RestricEmpresaUsuPedidoAbierto As Boolean, ByVal RestricEmpresaPerfUsuPedidoAbierto As Boolean, ByVal artBuscado As String) As DataTable
        Authenticate()
        Return DBServer.Cargar_PedidosAbiertos(Usu, RestricEmpresaUsuPedidoAbierto, RestricEmpresaPerfUsuPedidoAbierto, denArticulo:=artBuscado).Tables(0)
    End Function
    Public Function Comprobar_Importes_Cantidades_PedidoAbierto(ByVal IdLineaPedidoAbierto As Integer)
        Authenticate()
        Dim dt As DataTable
        dt = DBServer.Comprobar_Importes_Cantidades_PedidoAbierto(IdLineaPedidoAbierto)
        Return New With {.CantPed = CType(dt.Rows(0)("CANT_PED"), Double), .CantPedAbierto = CType(dt.Rows(0)("CANTIDAD_PED_ABIERTO"), Double), _
                         .ImportePed = CType(dt.Rows(0)("IMPORTE_PED"), Double), .ImportePedAbierto = CType(dt.Rows(0)("IMPORTE_PED_ABIERTO"), Double)}
    End Function
End Class
