﻿Imports System.Data.SqlClient
Imports Fullstep.FSNLibrary

'Ahora definido como class
<Serializable()> _
Public Class COrganizacionesCompras
    Inherits Lista(Of COrganizacionCompras)

    ''' <summary>
    ''' Constructor del objeto Empresas
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>    
    ''' <param name="isAuthenticated">Si está autenticado</param>
    ''' <remarks></remarks>
    Public Sub New(ByRef dbserver As Fullstep.FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub

    ''' <summary>Carga las organizaciones de compras de una empresa</summary>
    ''' <param name="iEmpresa">Empresa</param>
    ''' <param name="sDenOrgCompras">Denominación de la Org. de compras (caracteres iniciales)</param>
    ''' <remarks>Llamda desde: EmisionPedido.CargarOrganizacionesCompras</remarks>
    Public Sub CargarOrganizacionesCompras(ByVal iEmpresa As Integer, Optional ByVal sDenOrgCompras As String = Nothing)
        Authenticate()

        Dim dsOrgCompras As DataSet = mDBServer.DevolverOrganizacionesCompras(iEmpresa, sDenOrgCompras)
        If dsOrgCompras.Tables(0).Rows.Count = 0 Then
            dsOrgCompras.Clear()
        Else
            For Each oRow As DataRow In dsOrgCompras.Tables(0).Rows
                Add(New COrganizacionCompras(oRow("ORGCOMPRAS"), oRow("DEN")))
            Next
            dsOrgCompras.Clear()
        End If
        dsOrgCompras.Dispose()
    End Sub
End Class
