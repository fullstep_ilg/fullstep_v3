<Serializable()> _
Public Class CDestino
    Inherits Security

    Private msCod As String 'local copy
    Private msDen As String 'local copy
    Private miSinTransporte As Short
    Private msDir As String
    Private msPob As String
    Private mvCP As Object
    Private msProvi As String
    Private msPai As String
    Private msUON1 As Object
    Private msUON2 As Object
    Private msUON3 As Object
    Private msTfno As String
    Private msFAX As String
    Private msEmail As String
    Private msUON1DEN As Object
    Private msUON2DEN As Object
    Private msUON3DEN As Object
    Private mbRecepcion As Boolean
    Private oAlmacenes As cAlmacenes

    Public Property Den() As String
        Get
            Den = msDen
        End Get
        Set(ByVal Value As String)
            msDen = Value
        End Set
    End Property

    Public Property Cod() As String
        Get
            Cod = msCod
        End Get
        Set(ByVal Value As String)
            msCod = Value
        End Set
    End Property

    Public Property SinTransporte() As Boolean
        Get
            SinTransporte = miSinTransporte
        End Get
        Set(ByVal Value As Boolean)
            miSinTransporte = Value
        End Set
    End Property

    Public Property Direccion() As String
        Get
            Return msDir
        End Get
        Set(ByVal Value As String)
            msDir = Value
        End Set
    End Property

    Public Property Pob() As String
        Get
            Pob = msPob
        End Get
        Set(ByVal Value As String)
            msPob = Value
        End Set
    End Property

    Public Property CP() As String
        Get
            CP = mvCP
        End Get
        Set(ByVal Value As String)
            mvCP = Value
        End Set
    End Property

    Public Property Provi() As String
        Get
            Provi = msProvi
        End Get
        Set(ByVal Value As String)
            msProvi = Value
        End Set
    End Property

    Public Property Pai() As String
        Get
            Pai = msPai
        End Get
        Set(ByVal Value As String)
            msPai = Value
        End Set
    End Property

    Public Property UON1() As Object
        Get
            UON1 = msUON1
        End Get
        Set(ByVal Value As Object)
            msUON1 = Value
        End Set
    End Property

    Public Property UON2() As Object
        Get
            UON2 = msUON2
        End Get
        Set(ByVal Value As Object)
            msUON2 = Value
        End Set
    End Property

    Public Property UON3() As Object
        Get
            UON3 = msUON3
        End Get
        Set(ByVal Value As Object)
            msUON3 = Value
        End Set
    End Property

    Public Property UON1DEN() As Object
        Get
            UON1DEN = msUON1DEN
        End Get
        Set(ByVal Value As Object)
            msUON1DEN = Value
        End Set
    End Property

    Public Property UON2DEN() As Object
        Get
            UON2DEN = msUON2DEN
        End Get
        Set(ByVal Value As Object)
            msUON2DEN = Value
        End Set
    End Property

    Public Property UON3DEN() As Object
        Get
            UON3DEN = msUON3DEN
        End Get
        Set(ByVal Value As Object)
            msUON3DEN = Value
        End Set
    End Property

    Public Property Recepcion() As Boolean
        Get
            Recepcion = mbRecepcion
        End Get
        Set(ByVal Value As Boolean)
            mbRecepcion = Value
        End Set
    End Property

    Public Property Almacenes() As cAlmacenes
        Get
            Return oAlmacenes
        End Get
        Set(ByVal value As cAlmacenes)
            oAlmacenes = value
        End Set
    End Property

    Public Property Tfno() As String
        Get
            Tfno = msTfno
        End Get
        Set(ByVal Value As String)
            msTfno = Value
        End Set
    End Property

    Public Property FAX() As String
        Get
            FAX = msFAX
        End Get
        Set(ByVal Value As String)
            msFAX = Value
        End Set
    End Property

    Public Property Email() As String
        Get
            Email = msEmail
        End Get
        Set(ByVal Value As String)
            msEmail = Value
        End Set
    End Property

    ''' <summary>
    ''' Constructor de la clase CDestino
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si est� autenticado</param>
    Public Sub New(ByRef dbserver As Fullstep.FSNDataBaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
    End Sub

    ''' <summary>
    ''' Devuelve un objeto de tipo cAlmacenes con los almacenes asociados a un destino
    ''' </summary>
    ''' <param name="CodDest">C�digo de destino del que cargar los almacenes</param>
    ''' <param name="Idioma">Idioma en el que cargar los datos</param>
    ''' <param name="InlcuirEnColeccion">Indica si se debe incluir en la coleccion de almacenes</param>
    ''' <returns>Un DataSet con los Datos de los almacenes asociados al Destino</returns>
    ''' <remarks>
    ''' Llamada desde: EPWeb\EmisionPedido.aspx.vb\GridLineas.RowDataBound
    ''' Tiempo m�ximo: 0,3 seg</remarks>
    Public Function DevolverAlmacenesDestino(ByVal CodDest As String, ByVal Idioma As String, Optional ByVal InlcuirEnColeccion As Boolean = False) As DataSet
        If InlcuirEnColeccion AndAlso Almacenes Is Nothing Then
            Almacenes = New cAlmacenes(DBServer, mIsAuthenticated)
        End If

        Dim DsResul As New DataSet
        DsResul = DBServer.Destino_DevolverAlmacenesDestino(CodDest)
        If DsResul.Tables(0).Rows.Count > 0 Then
            For Each fila As DataRow In DsResul.Tables(0).Rows
                If Me.Cod = CodDest Then
                    Almacenes.Add(fila.Item("Id"), fila.Item("Cod"), fila.Item("Den"), fila.Item("CodDest"), Me)
                Else
                    Dim oDestino As CDestino
                    oDestino = New CDestino(DBServer, mIsAuthenticated)
                    Dim DsDest As New DataSet
                    Dim oCDestinos As FSNServer.CDestinos = New CDestinos(DBServer, mIsAuthenticated)
                    DsDest = oCDestinos.CargarDatosDestino(Idioma, CodDest)
                    If Not DsDest.Tables(0).Rows.Count = 0 Then
                        With DsDest.Tables(0).Rows(0)
                            oDestino.Cod = DBNullToStr(.Item("Cod"))
                            oDestino.Den = DBNullToStr(.Item("Den"))
                            oDestino.Direccion = DBNullToStr(.Item("Dir"))
                            oDestino.CP = DBNullToStr(.Item("CP"))
                            oDestino.Pob = DBNullToStr(.Item("Pob"))
                            oDestino.Provi = DBNullToStr(.Item("Provi"))
                            oDestino.Pai = DBNullToStr(.Item("Pai"))
                        End With
                    End If
                    If InlcuirEnColeccion Then
                        Almacenes.Add(fila.Item("Id"), fila.Item("Cod"), fila.Item("Den"), fila.Item("CodDest"), oDestino)
                    End If
                End If
            Next
        End If
        Return DsResul
        DsResul.Clear()
    End Function

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub
End Class