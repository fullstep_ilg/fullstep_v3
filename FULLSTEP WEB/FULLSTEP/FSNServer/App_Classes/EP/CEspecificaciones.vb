<Serializable()> _
Public Class CEspecificaciones
    Inherits Lista(Of CEspecificacion)

    Public Overloads Property Item(ByVal Id As String) As CEspecificacion
        Get
            Return Me.Find(Function(elemento As CEspecificacion) elemento.Id = Id)
        End Get
        Set(ByVal value As CEspecificacion)
            Dim ind As Integer = Me.FindIndex(Function(elemento As CEspecificacion) elemento.Id = Id)
            If ind = -1 Then
                Me.Add(value)
            Else
                Me.Item(ind) = value
            End If
        End Set
    End Property

    ''' <summary>
    ''' Agrega un elemento CEspecificacion a la lista
    ''' </summary>
    ''' <param name="Nombre">Nombre del archivo</param>
    ''' <param name="ID">ID de la especificaci�n</param>
    ''' <param name="Fecha">Fecha de la especificaci�n</param>
    ''' <param name="oCat">ID de la categor�a</param>
    ''' <param name="Comentario">Comentario</param>
    ''' <param name="mdSize">Tama�o del archivo</param>
    ''' <returns>El elemento CEspecificacion a�adido</returns>
    Public Overloads Function Add(ByVal Nombre As String, ByVal ID As Short, ByVal Fecha As Date, ByVal oCat As Object, ByVal Comentario As String, ByVal mdSize As Double) As CEspecificacion
        Dim objnewmember As New CEspecificacion(mDBServer, mIsAuthenticated)

        With objnewmember
            If Not IsNothing(oCat) Then
                .Cat = oCat
                .IdCatLin = oCat.Id
            End If
            .Id = ID
            .Nombre = Nombre
            .Comentario = Comentario
            .Fecha = Fecha
            .DataSize = mdSize
        End With

        Me.Add(objnewmember)
        Return objnewmember

    End Function

    ''' <summary>
    ''' Constructor de la clase CEspecificaciones
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si est� autenticado</param>
    Public Sub New(ByRef dbserver As Fullstep.FSNDataBaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
    End Sub

End Class