﻿Imports Fullstep.FSNLibrary

<Serializable()> _
Public Class CFactura
    Inherits Security

    Private m_IId As Integer
    Private m_sNum As String
    Private m_IEstadoId As Integer
    Private m_sEstadoDen As String
    Private m_dtFecha As DateTime
    'Private m_ILineaFactura As Integer
    Private m_ILineaPedido As Integer
    Private m_sImporte As String
    Private m_sMoneda As String

    Public Property Id() As Integer
        Get
            Id = m_IId
        End Get
        Set(ByVal value As Integer)
            m_IId = value
        End Set
    End Property

    Public Property Num() As String
        Get
            Num = m_sNum
        End Get
        Set(ByVal value As String)
            m_sNum = value
        End Set
    End Property

    Public Property EstadoId() As Integer
        Get
            EstadoId = m_IEstadoId
        End Get
        Set(ByVal value As Integer)
            m_IEstadoId = value
        End Set
    End Property

    Public Property EstadoDen() As String
        Get
            EstadoDen = m_sEstadoDen
        End Get
        Set(ByVal value As String)
            m_sEstadoDen = value
        End Set
    End Property

    Public Property Fecha() As DateTime
        Get
            Fecha = m_dtFecha
        End Get
        Set(ByVal value As DateTime)
            m_dtFecha = value
        End Set
    End Property

    'Public Property LineaFactura() As Integer
    '    Get
    '        LineaFactura = m_ILineaFactura
    '    End Get
    '    Set(ByVal value As Integer)
    '        m_ILineaFactura = value
    '    End Set
    'End Property

    Public Property LineaPedido() As Integer
        Get
            LineaPedido = m_ILineaPedido
        End Get
        Set(ByVal value As Integer)
            m_ILineaPedido = value
        End Set
    End Property


    Public Property Importe() As String
        Get
            Importe = m_sImporte
        End Get
        Set(ByVal value As String)
            m_sImporte = value
        End Set
    End Property


    Public Property Moneda() As String
        Get
            Moneda = m_sMoneda
        End Get
        Set(ByVal value As String)
            m_sMoneda = value
        End Set
    End Property


    ''' <summary>
    ''' Constructor de la clase CFactura
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si está autenticado</param>
    Public Sub New(ByRef dbserver As Fullstep.FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
End Class
