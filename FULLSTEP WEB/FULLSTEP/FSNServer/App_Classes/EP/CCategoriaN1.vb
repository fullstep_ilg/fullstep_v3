
Public Class CCategoriaN1
    Inherits Security

    Private mlId As Integer
    Private msCod As String 'local copy
    Private msDen As String 'local copy
    Private moCategoriasN2 As CCategoriasN2
    Private miNumHojas As Short
    Private mvObs As Object
    Private moEspecificaciones As CEspecificaciones
    Private moCamposPers As CCampos
    Private moProvesPedLibre As CProveedores

    Public Property CamposPersonalizados() As CCampos
        Get
            CamposPersonalizados = moCamposPers
        End Get
        Set(ByVal Value As CCampos)
            moCamposPers = Value
        End Set
    End Property

    Public Property Id() As Integer
        Get
            Id = mlId
        End Get
        Set(ByVal Value As Integer)
            mlId = Value
        End Set
    End Property

    Public Property Cod() As String
        Get
            Cod = msCod
        End Get
        Set(ByVal Value As String)
            msCod = Value
        End Set
    End Property

    Public Property Den() As String
        Get
            Den = msDen
        End Get
        Set(ByVal Value As String)
            msDen = Value
        End Set
    End Property

    Public Property CategoriasNivel2() As CCategoriasN2
        Get
            CategoriasNivel2 = moCategoriasN2
        End Get
        Set(ByVal Value As CCategoriasN2)
            moCategoriasN2 = Value
        End Set
    End Property

    Public Property NumHojas() As Short
        Get
            NumHojas = miNumHojas
        End Get
        Set(ByVal Value As Short)
            miNumHojas = Value
        End Set
    End Property

    Public Property Obs() As Object
        Get
            Obs = mvObs
        End Get
        Set(ByVal Value As Object)
            mvObs = Value
        End Set
    End Property

    Public Property Especificaciones() As CEspecificaciones
        Get
            Especificaciones = moEspecificaciones
        End Get
        Set(ByVal Value As CEspecificaciones)
            moEspecificaciones = Value
        End Set
    End Property

    Public Property ProveedoresPedidoLibre() As CProveedores
        Get
            ProveedoresPedidoLibre = moProvesPedLibre
        End Get
        Set(ByVal Value As CProveedores)
            moProvesPedLibre = Value
        End Set
    End Property
    ''' <summary>
    ''' Constructor de la clase CCategoriaN1
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si est� autenticado</param>
    Public Sub New(ByRef dbserver As Fullstep.FSNDataBaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
    End Sub

End Class