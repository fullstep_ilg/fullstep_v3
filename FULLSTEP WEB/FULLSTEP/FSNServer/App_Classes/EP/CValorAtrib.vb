Option Strict Off
Option Explicit On
'Ahora definida como clase publica
<System.Runtime.InteropServices.ProgId("CValorAtrib_NET.CValorAtrib")> Public Class CValorAtrib

    ''' Variables privadas con la informacion de un valor

    Private mvValor As Object
    Private mvIndice As Integer

    Public Sub New()

    End Sub

    Public Property Indice() As Integer
        Get

            ' * Objetivo: Devolver el indice del pais en la coleccion
            ' * Recibe: Nada
            ' * Devuelve: Indice del pais en la coleccion

            Indice = mvIndice

        End Get
        Set(ByVal Value As Integer)

            ' * Objetivo: Dar valor al indice del pais en la coleccion
            ' * Recibe: Indice del pais en la coleccion
            ' * Devuelve: Nada

            mvIndice = Value

        End Set
    End Property

    Public Property Valor() As Object
        Get

            Valor = mvValor

        End Get
        Set(ByVal Value As Object)

            mvValor = Value

        End Set
    End Property
End Class