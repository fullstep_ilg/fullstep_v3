Imports Fullstep.FSNLibrary

<Serializable()> _
Public Class CArticulo
    Inherits Security

    Private mlId As Object
    Private msIdUnico As String
    Private msTooltip As Object
    Private mlAnyo As Object
    Private msGmn1 As Object
    Private msGmn2 As Object
    Private msGmn3 As Object
    Private msGmn4 As Object
    Private msGenerico As Object
    Private mlItem As Object
    Private msCod As Object
    Private msDen As Object
    Private msProve As Object
    Private msProveDen As Object
    Private msCodExt As Object
    Private mdCantAdj As Object
    Private mdPrecioUC As Object
    Private msUP As Object
    Private mdFC As Object
    Private mdCantMinimaPedido As Object
    Private mdtFechaDespub As Object
    Private mlCat1 As Object
    Private mlCat2 As Object
    Private mlCat3 As Object
    Private mlCat4 As Object
    Private mlCat5 As Object
    Private mvSeguridad As Object
    Private mbModDestino As Object
    Private mldatasizeThumbnail As Object
    Private msCategoria As Object
    Private msRamaCategoria As Object
    Private msEsp As Object
    Private msDest As Object

    Private moCampo1 As CCampo
    Private moCampo2 As CCampo

    Private mbModifPrec As Object
    Private mbActualizarCat As Object
    Private mbModificarUnidad As Boolean

    Private moAtributos As CAtributos
    Private moEspecificaciones As CEspecificaciones

    Private mdCantidad As Object
    Private miTipo As Short 'Tipo del pedido:0->Normal,1->pedido libre
    Private mvarMon As Object 'A�adido el 1-3-2006 Contiene el tipo de moneda
    Private mvarCambio As Object 'A�adido el 14-3-2006

    Private mdtFechaEntrega As Date
    Private mbFecEntregaObl As Boolean
    Private structureTipoArticulo As strTipoArticulo
    Private intAlmacen As Integer

    Private moImputaciones As FSNServer.Imputaciones
    Private miActivo As Integer
    Private miTipoRecepcion As Integer


    'Acordarse de meter esto en FSNLIbrary.TiposDeDatos
    <Serializable()> _
    Public Structure strTipoArticulo
        Dim Concepto As EnInvTipo
        Dim TipoRecepcion As EnRecTipo
        Dim TipoAlmacenamiento As EnAlmacenTipo
    End Structure

    <Serializable()> _
    Public Enum EnInvTipo
        Gasto = 0
        Inversion = 1
        Ambos = 2
    End Enum

    <Serializable()> _
    Public Enum EnRecTipo
        NoRecepcionable = 0
        Obligatorio = 1
        Opcional = 2
    End Enum

    <Serializable()> _
    Public Enum EnAlmacenTipo
        NoAlmacenable = 0
        Obligatorio = 1
        Opcional = 2
    End Enum
    'Hasta aqui
    Public Property ID() As Object
        Get
            ID = mlId
        End Get
        Set(ByVal Value As Object)
            mlId = Value
        End Set
    End Property

    Public Property IdUnico() As String
        Get
            IdUnico = msIdUnico
        End Get
        Set(ByVal Value As String)
            msIdUnico = Value
        End Set
    End Property

    Public Property Tooltip() As Object
        Get
            Tooltip = msTooltip
        End Get
        Set(ByVal Value As Object)
            msTooltip = Value
        End Set
    End Property

    Public Property Anyo() As Object
        Get
            Anyo = mlAnyo
        End Get
        Set(ByVal Value As Object)
            mlAnyo = Value
        End Set
    End Property

    Public Property GMN1() As Object
        Get
            GMN1 = msGmn1
        End Get
        Set(ByVal Value As Object)
            msGmn1 = Value
        End Set
    End Property

    Public Property GMN2() As Object
        Get
            GMN2 = msGmn2
        End Get
        Set(ByVal Value As Object)
            msGmn2 = Value
        End Set
    End Property

    Public Property GMN3() As Object
        Get
            GMN3 = msGmn3
        End Get
        Set(ByVal Value As Object)
            msGmn3 = Value
        End Set
    End Property

    Public Property GMN4() As Object
        Get
            GMN4 = msGmn4
        End Get
        Set(ByVal Value As Object)
            msGmn4 = Value
        End Set
    End Property

    Public Property Item() As Object
        Get
            Item = mlItem
        End Get
        Set(ByVal Value As Object)
            mlItem = Value
        End Set
    End Property
    Public Property Generico() As Object
        Get
            Generico = msGenerico
        End Get
        Set(ByVal Value As Object)
            msGenerico = DBNullToSomething(Value)
        End Set
    End Property
    Public Property Cod() As Object
        Get
            Cod = msCod
        End Get
        Set(ByVal Value As Object)
            msCod = DBNullToSomething(Value)
        End Set
    End Property

    Public Property Den() As Object
        Get
            Den = msDen
        End Get
        Set(ByVal Value As Object)
            msDen = Value
        End Set
    End Property

    Public Property Prove() As Object
        Get
            Prove = msProve
        End Get
        Set(ByVal Value As Object)
            msProve = Value
        End Set
    End Property

    Public Property proveden() As Object
        Get
            proveden = msProveDen
        End Get
        Set(ByVal Value As Object)
            msProveDen = Value
        End Set
    End Property

    Public Property CodExt() As Object
        Get
            CodExt = msCodExt
        End Get
        Set(ByVal Value As Object)
            msCodExt = Value
        End Set
    End Property

    Public Property CantAdj() As Object
        Get
            CantAdj = mdCantAdj
        End Get
        Set(ByVal Value As Object)
            mdCantAdj = Value
        End Set
    End Property

    Public Property Cantidad() As Object
        Get
            Cantidad = mdCantidad
        End Get
        Set(ByVal Value As Object)
            mdCantidad = Value
        End Set
    End Property

    Public Property PrecioUC() As Object
        Get
            PrecioUC = mdPrecioUC
        End Get
        Set(ByVal Value As Object)
            mdPrecioUC = Value
        End Set
    End Property

    Public Property UP() As Object
        Get
            UP = msUP
        End Get
        Set(ByVal Value As Object)
            msUP = Value
        End Set
    End Property

    Public Property FC() As Object
        Get
            'Antes IsEmpty()
            If IsNothing(mdFC) Then
                mdFC = 1
            End If
            FC = mdFC
        End Get
        Set(ByVal Value As Object)
            mdFC = Value
        End Set
    End Property

    'A�adido el 1-3-2006
    Public Property Mon() As Object
        Get
            Mon = mvarMon
        End Get
        Set(ByVal Value As Object)
            mvarMon = Value
        End Set
    End Property

    Public Property Cambio() As Object
        Get
            Cambio = mvarCambio
        End Get
        Set(ByVal Value As Object)
            mvarCambio = Value
        End Set
    End Property

    Public Property CantMinimaPedido() As Object
        Get
            CantMinimaPedido = mdCantMinimaPedido
        End Get
        Set(ByVal Value As Object)
            mdCantMinimaPedido = Value
        End Set
    End Property

    Public Property FechaDespub() As Object
        Get
            FechaDespub = mdtFechaDespub
        End Get
        Set(ByVal Value As Object)
            mdtFechaDespub = Value
        End Set
    End Property

    Public Property Cat1() As Object
        Get
            Cat1 = mlCat1
        End Get
        Set(ByVal Value As Object)
            mlCat1 = Value
        End Set
    End Property

    Public Property Cat2() As Object
        Get
            Cat2 = mlCat2
        End Get
        Set(ByVal Value As Object)
            mlCat2 = Value
        End Set
    End Property

    Public Property Cat3() As Object
        Get
            Cat3 = mlCat3
        End Get
        Set(ByVal Value As Object)
            mlCat3 = Value
        End Set
    End Property

    Public Property Cat4() As Object
        Get
            Cat4 = mlCat4
        End Get
        Set(ByVal Value As Object)
            mlCat4 = Value
        End Set
    End Property

    Public Property Cat5() As Object
        Get
            Cat5 = mlCat5
        End Get
        Set(ByVal Value As Object)
            mlCat5 = Value
        End Set
    End Property

    Public Property Seguridad() As Object
        Get
            Seguridad = mvSeguridad
        End Get
        Set(ByVal Value As Object)
            mvSeguridad = Value
        End Set
    End Property

    Public Property ModDestino() As Object
        Get
            ModDestino = mbModDestino
        End Get
        Set(ByVal Value As Object)
            mbModDestino = Value
        End Set
    End Property

    Public Property Dest() As Object
        Get
            Dest = msDest
        End Get
        Set(ByVal Value As Object)
            msDest = Value
        End Set
    End Property

    Public Property DatasizeThumbnail() As Object
        Get
            'Antes IsMissing()
            If IsNothing(mldatasizeThumbnail) Then
                mldatasizeThumbnail = System.DBNull.Value
            End If
            DatasizeThumbnail = mldatasizeThumbnail
        End Get
        Set(ByVal Value As Object)
            mldatasizeThumbnail = Value
        End Set
    End Property

    Public Property Categoria() As Object
        Get
            Categoria = msCategoria
        End Get
        Set(ByVal Value As Object)
            msCategoria = Value
        End Set
    End Property

    Public Property RamaCategoria() As Object
        Get
            RamaCategoria = msRamaCategoria
        End Get
        Set(ByVal Value As Object)
            msRamaCategoria = Value
        End Set
    End Property

    Public Property Esp() As Object
        Get
            Esp = msEsp
        End Get
        Set(ByVal Value As Object)
            msEsp = Value
        End Set
    End Property

    Public Property Atributos() As CAtributos
        Get
            Atributos = moAtributos
        End Get
        Set(ByVal Value As CAtributos)
            moAtributos = Value
        End Set
    End Property

    Public Property Campo1() As CCampo
        Get
            Campo1 = moCampo1
        End Get
        Set(ByVal Value As CCampo)
            moCampo1 = Value
        End Set
    End Property

    Public Property Campo2() As CCampo
        Get
            Campo2 = moCampo2
        End Get
        Set(ByVal Value As CCampo)
            moCampo2 = Value
        End Set
    End Property

    Public Property Especificaciones() As CEspecificaciones
        Get
            Especificaciones = moEspecificaciones
        End Get
        Set(ByVal Value As CEspecificaciones)
            moEspecificaciones = Value
        End Set
    End Property

    Public Property Tipo() As Short
        Get
            Tipo = miTipo
        End Get
        Set(ByVal Value As Short)
            miTipo = Value
        End Set
    End Property

    Public Property ModificarPrec() As Object
        Get
            ModificarPrec = mbModifPrec
        End Get
        Set(ByVal Value As Object)
            mbModifPrec = Value
        End Set
    End Property

    Public Property ModificarUnidad() As Boolean
        Get
            Return mbModificarUnidad
        End Get
        Set(ByVal value As Boolean)
            mbModificarUnidad = value
        End Set
    End Property

    Public Property ActualizarCat() As Object
        Get
            ActualizarCat = mbActualizarCat
        End Get
        Set(ByVal Value As Object)
            mbActualizarCat = Value
        End Set
    End Property

    Public Property FechaEntrega() As Date
        Get
            Return mdtFechaEntrega
        End Get
        Set(ByVal value As Date)
            mdtFechaEntrega = value
        End Set
    End Property

    Public Property FecEntregaObl() As Boolean
        Get
            Return mbFecEntregaObl
        End Get
        Set(ByVal value As Boolean)
            mbFecEntregaObl = value
        End Set
    End Property

    Public Property TipoArticulo() As strTipoArticulo
        Get
            Return structureTipoArticulo
        End Get
        Set(ByVal value As strTipoArticulo)
            structureTipoArticulo = value
        End Set
    End Property

    Public Property Almacen() As Integer
        Get
            Return intAlmacen
        End Get
        Set(ByVal value As Integer)
            intAlmacen = value
        End Set
    End Property

    Public Property Imputaciones() As FSNServer.Imputaciones
        Get
            Return moImputaciones
        End Get
        Set(ByVal value As FSNServer.Imputaciones)
            moImputaciones = value
        End Set
    End Property

    Public Property Activo() As Integer
        Get
            Return miActivo
        End Get
        Set(ByVal value As Integer)
            miActivo = value
        End Set
    End Property

    Public Property iTipoRecepcion() As Integer
        Get
            Return miTipoRecepcion
        End Get
        Set(ByVal value As Integer)
            miTipoRecepcion = value
        End Set
    End Property

    ''' <summary>
    ''' Carga las especificaciones de un art�culo concreto, para poder mostrar los datos necesarios en la aplicaci�n que lo llame.
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub CargarEspecificaciones()
        Authenticate()
        Dim adoRes As New DataSet
        adoRes = mDBServer.Articulo_CargarEspecificaciones(ID)
        moEspecificaciones = New CEspecificaciones(mDBServer, mIsAuthenticated)
        For Each fila As DataRow In adoRes.Tables(0).Rows
            Dim oEsp As CEspecificacion = moEspecificaciones.Add(fila.Item("NOM"), fila.Item("ID"), fila.Item("FECACT"), Nothing, DBNullToSomething(fila.Item("COM")), fila.Item("DATASIZE"))
            oEsp.IdCatLin = mlId
        Next
        adoRes.Clear()
        adoRes = Nothing
    End Sub

    ''' <summary>
    ''' Funcion que a�ade un art�culo a la cesta de la compra de un usuario dado.
    ''' </summary>
    ''' <param name="sPer">C�digo de la persona a la que a�adir el art�culo a la cesta</param>
    ''' <remarks></remarks>
    Public Sub AnyadirACesta(ByVal sPer As String)
        Authenticate()
        AnyadirACesta(sPer, FC, Prove, ID, DBNullToSomething(Cat1), DBNullToSomething(Cat2), DBNullToSomething(Cat3), DBNullToSomething(Cat4), DBNullToSomething(Cat5), Den, Cantidad, UP, PrecioUC, Tipo, Mon, iTipoRecepcion)
    End Sub
    Public Sub AnyadirACesta(ByVal sPer As String, ByVal FC As Double, ByVal Prove As String, ByVal ID As Integer, ByVal Cat1 As Integer, ByVal Cat2 As Integer, ByVal Cat3 As Integer, ByVal Cat4 As Integer, ByVal Cat5 As Integer, ByVal Den As String, ByVal Cantidad As Double, ByVal UP As String, ByVal PrecioUC As Double, ByVal Tipo As Short, ByVal Mon As String, Optional ByVal iTipoRecepcion As Integer = 0)
        Authenticate()
        Dim iCat As Integer
        Dim iNivel As Integer
        Dim adoRs As New DataSet

        iCat = 0
        iNivel = 0

        If Cat5 = 0 Then
            If Cat4 = 0 Then
                If Cat3 = 0 Then
                    If Cat2 = 0 Then
                        If Not Cat1 = 0 Then
                            iCat = 1
                            iNivel = Cat1
                        End If
                    Else
                        iCat = 2
                        iNivel = Cat2
                    End If
                Else
                    iCat = 3
                    iNivel = Cat3
                End If
            Else
                iCat = 4
                iNivel = Cat4
            End If
        Else
            iCat = 5
            iNivel = Cat5
        End If
        DBServer.Articulo_AnyadirACesta(sPer, FC, Prove, ID, Cat1, Cat2, Cat3, Cat4, Cat5, Den, Cantidad, UP, PrecioUC, Tipo, Mon, iCat, iNivel, iTipoRecepcion)
    End Sub
    Public Function AnyadirACestaFav(ByVal sPer As String, ByVal FC As Double, ByVal Prove As String, ByVal ID As Integer, ByVal Cat1 As Integer, ByVal Cat2 As Integer, ByVal Cat3 As Integer, ByVal Cat4 As Integer, ByVal Cat5 As Integer, ByVal Den As String, ByVal Cantidad As Double, ByVal UP As String, ByVal PrecioUC As Double, ByVal Tipo As Short, ByVal Mon As String, Optional ByVal iTipoRecepcion As Integer = 0) As Integer
        Authenticate()
        Dim iCat As Integer
        Dim iNivel As Integer
        Dim adoRs As New DataSet

        iCat = 0
        iNivel = 0

        If Cat5 = 0 Then
            If Cat4 = 0 Then
                If Cat3 = 0 Then
                    If Cat2 = 0 Then
                        If Not Cat1 = 0 Then
                            iCat = 1
                            iNivel = Cat1
                        End If
                    Else
                        iCat = 2
                        iNivel = Cat2
                    End If
                Else
                    iCat = 3
                    iNivel = Cat3
                End If
            Else
                iCat = 4
                iNivel = Cat4
            End If
        Else
            iCat = 5
            iNivel = Cat5
        End If
        Return DBServer.Articulo_AnyadirACesta(sPer, FC, Prove, ID, Cat1, Cat2, Cat3, Cat4, Cat5, Den, Cantidad, UP, PrecioUC, Tipo, Mon, iCat, iNivel, iTipoRecepcion, 1)
    End Function
    ''' <summary>
    ''' Carga los datos necesarios del art�culo favorito, para poder mostarlos en la aplicaci�n de la que se llama
    ''' </summary>
    ''' <param name="sIdioma">Idioma utilizado para la muestra de datos del art�culo.</param>
    ''' <param name="sPer">Persona a la que esta asignada el art�culo favorito.</param>
    ''' <remarks>revisado por MMV(25/11/2011)</remarks>
    Public Sub CargarDatosArticuloFavorito(ByVal sPer As String, ByVal sIdioma As String)
        Authenticate()
        Dim oCampo1 As CCampo
        Dim oCampo2 As CCampo
        Dim adoRs As New DataSet
        adoRs = mDBServer.Articulo_CargarDatosArticuloFavorito(sPer, sIdioma, ID)
        If Not adoRs.Tables(0).Rows.Count = 0 Then
            oCampo1 = Nothing
            If Not IsDBNull(adoRs.Tables(0).Rows(0).Item("CAMPO1")) Then
                oCampo1 = New CCampo(mDBServer, mIsAuthenticated)
                With oCampo1
                    .Den = adoRs.Tables(0).Rows(0).Item("DEN1")
                    .ID = adoRs.Tables(0).Rows(0).Item("CAMPO1")
                    .Obligatorio = adoRs.Tables(0).Rows(0).Item("OBL1")
                End With
            End If
            oCampo2 = Nothing
            If Not IsDBNull(adoRs.Tables(0).Rows(0).Item("CAMPO2")) Then
                oCampo2 = New CCampo(mDBServer, mIsAuthenticated)
                With oCampo2
                    .Den = adoRs.Tables(0).Rows(0).Item("DEN2")
                    .ID = adoRs.Tables(0).Rows(0).Item("CAMPO2")
                    .Obligatorio = adoRs.Tables(0).Rows(0).Item("OBL2")
                End With
            End If

            moCampo1 = oCampo1
            moCampo2 = oCampo2
            msDest = adoRs.Tables(0).Rows(0).Item("DEST")
            mdCantMinimaPedido = adoRs.Tables(0).Rows(0).Item("CANT_MIN_DEF")
            mlCat1 = adoRs.Tables(0).Rows(0).Item("CAT1")
            mlCat2 = adoRs.Tables(0).Rows(0).Item("CAT2")
            mlCat3 = adoRs.Tables(0).Rows(0).Item("CAT3")
            mlCat4 = adoRs.Tables(0).Rows(0).Item("CAT4")
            mlCat5 = adoRs.Tables(0).Rows(0).Item("CAT5")
            msCod = adoRs.Tables(0).Rows(0).Item("COD_ITEM")
            msDen = adoRs.Tables(0).Rows(0).Item("ART_DEN")
            msProve = adoRs.Tables(0).Rows(0).Item("PROVE")
            msProveDen = adoRs.Tables(0).Rows(0).Item("PROVEDEN")
            mdPrecioUC = adoRs.Tables(0).Rows(0).Item("PREC")
            msUP = adoRs.Tables(0).Rows(0).Item("UP")
            mdFC = adoRs.Tables(0).Rows(0).Item("FC")
            mbModifPrec = SQLBinaryToBoolean(adoRs.Tables(0).Rows(0).Item("MODIF_PREC"))
            msGenerico = adoRs.Tables(0).Rows(0).Item("GENERICO")
            miTipoRecepcion = adoRs.Tables(0).Rows(0).Item("TIPORECEPCION")

        End If
        adoRs.Clear()
        adoRs = Nothing
    End Sub
    ''' <summary>
    ''' Constructor de la clase CArticulo
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si est� autenticado</param>
    Public Sub New(ByRef dbserver As Fullstep.FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub

End Class