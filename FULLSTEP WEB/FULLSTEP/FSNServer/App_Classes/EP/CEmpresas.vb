<Serializable()> _
Public Class CEmpresas
    Inherits Lista(Of CEmpresa)

    ''' <summary>
    ''' Establece o devuelve el elemento con el ID especificado
    ''' </summary>
    ''' <param name="Id">ID de la Empresa</param>
    ''' <returns>El elemento con el ID especificado</returns>
    Public Overloads Property Item(ByVal Id As String) As CEmpresa
        Get
            Return Me.Find(Function(elemento As CEmpresa) elemento.ID = Id)
        End Get
        Set(ByVal value As CEmpresa)
            Dim ind As Integer = Me.FindIndex(Function(elemento As CEmpresa) elemento.ID = Id)
            If ind = -1 Then
                Me.Add(value)
            Else
                Me.Item(ind) = value
            End If
        End Set
    End Property

    ''' <summary>
    ''' A�ade un nuevo elemento CEmpresa a la lista
    ''' </summary>
    ''' <param name="lId">ID de la empresa</param>
    ''' <param name="sNif">NIF de la empresa</param>
    ''' <param name="sDen">Denominaci�n</param>
    ''' <param name="vDireccion">Direcci�n postal</param>
    ''' <param name="vCP">C�digo Postal</param>
    ''' <param name="vCodPais">C�digo de Pa�s</param>
    ''' <param name="vPoblacion">Poblaci�n</param>
    ''' <param name="vCodProvi">C�digo de Provincia</param>
    ''' <param name="bERP">Tiene integraci�n ERP</param>
    ''' <returns>El elemento CEmpresa a�adido</returns>
    Public Overloads Function Add(ByVal lId As Integer, ByVal sNif As String, ByVal sDen As String, ByVal vDireccion As Object, ByVal vCP As Object, ByVal vCodPais As Object, ByVal vPoblacion As Object, ByVal vCodProvi As Object, ByVal bERP As Boolean) As CEmpresa

        'create a new object
        Dim objnewmember As CEmpresa

        objnewmember = New CEmpresa(mDBServer, mIsAuthenticated)

        With objnewmember
            .ID = lId
            .Nif = sNif
            .Den = sDen
            'Antes IsMissing
            If Not IsNothing(vPoblacion) Then
                .Poblacion = vPoblacion
            Else
                .Poblacion = System.DBNull.Value
            End If
            'Antes isMissing
            If Not IsNothing(vDireccion) Then
                .Direccion = vDireccion
            Else
                .Direccion = System.DBNull.Value
            End If
            'Antes Ismissing
            If Not IsNothing(vCP) Then
                .CP = vCP
            Else
                .CP = System.DBNull.Value
            End If
            If Not IsNothing(vCodProvi) Then
                .CodProvi = vCodProvi
            Else
                .CodProvi = System.DBNull.Value
            End If
            'Antes Ismissing
            If Not IsNothing(vCodPais) Then
                .CodPais = vCodPais
            Else
                .CodPais = System.DBNull.Value
            End If

            .ERP = bERP
        End With

        Me.Add(objnewmember)
        Return objnewmember
    End Function

    ''' <summary>
    ''' A�ade un nuevo elemento CEmpresa a la lista
    ''' </summary>
    ''' <param name="lId">ID de la empresa</param>
    ''' <param name="sNif">NIF de la empresa</param>
    ''' <param name="sDen">Denominaci�n</param>
    ''' <param name="vDireccion">Direcci�n postal</param>
    ''' <param name="vCP">C�digo Postal</param>
    ''' <param name="vCodPais">C�digo de Pa�s</param>
    ''' <param name="vPoblacion">Poblaci�n</param>
    ''' <param name="vCodProvi">C�digo de Provincia</param>
    ''' <param name="bERP">Tiene integraci�n ERP</param>
    ''' <param name="vEnvFacturaDirecciones">Direcciones de env�o de facturas de la empresa</param>
    ''' <returns>El elemento CEmpresa a�adido</returns>
    Public Overloads Function Add(ByVal lId As Integer, ByVal sNif As String, ByVal sDen As String, ByVal vDireccion As Object, ByVal vCP As Object, ByVal vCodPais As Object, ByVal vPoblacion As Object, ByVal vCodProvi As Object, ByVal bERP As Boolean, ByVal vEnvFacturaDirecciones As CEnvFacturaDirecciones) As CEmpresa

        'create a new object
        Dim objnewmember As CEmpresa

        objnewmember = New CEmpresa(mDBServer, mIsAuthenticated)

        With objnewmember
            .ID = lId
            .Nif = sNif
            .Den = sDen
            'Antes IsMissing
            If Not IsNothing(vPoblacion) Then
                .Poblacion = vPoblacion
            Else
                .Poblacion = System.DBNull.Value
            End If
            'Antes isMissing
            If Not IsNothing(vDireccion) Then
                .Direccion = vDireccion
            Else
                .Direccion = System.DBNull.Value
            End If
            'Antes Ismissing
            If Not IsNothing(vCP) Then
                .CP = vCP
            Else
                .CP = System.DBNull.Value
            End If
            If Not IsNothing(vCodProvi) Then
                .CodProvi = vCodProvi
            Else
                .CodProvi = System.DBNull.Value
            End If
            'Antes Ismissing
            If Not IsNothing(vCodPais) Then
                .CodPais = vCodPais
            Else
                .CodPais = System.DBNull.Value
            End If
            .ERP = bERP
            .DireccionesEnvioFactura = vEnvFacturaDirecciones
        End With

        Me.Add(objnewmember)
        Return objnewmember
    End Function

    ''' <summary>
    ''' Constructor del objeto Empresas
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si est� autenticado</param>
    ''' <remarks></remarks>
    Public Sub New(ByRef dbserver As Fullstep.FSNDataBaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
    End Sub

    ''' <summary>
    ''' Devuelve todas las empresas.
    ''' </summary>
    ''' <returns>Un DataTable con todas las empresas</returns>
    ''' <remarks>
    ''' Llamada desde: pendiente</remarks>
    Public Function CargarDatosEmpresas() As DataTable
        Return mDBServer.Empresas_CargarDatos()
    End Function

    ''' <summary>
    ''' Carga la lista interna con todas las empresas
    ''' </summary>
    Public Sub CargarListaEmpresas()
        Dim dt As DataTable = CargarDatosEmpresas()
        Add(Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, False)
        For Each fila As DataRow In dt.Rows
            Me.Add(fila("ID"), fila("NIF"), fila("DEN"), fila("DIR"), fila("CP"), fila("PAI"), fila("POB"), fila("PROVI"), fila("ERP"))
        Next
    End Sub

    Public Sub CargarEmpresaPedidoAbierto(ByVal IdPedidoAbierto As Integer)
        Dim dt As DataTable = mDBServer.Cargar_Empresa_PedidoAbierto(IdPedidoAbierto).Tables(0)
        For Each fila As DataRow In dt.Rows
            Me.Add(fila("ID"), fila("NIF"), fila("DEN"), fila("DIR"), fila("CP"), fila("PAI"), fila("POB"), fila("PROVI"), fila("ERP"))
        Next
    End Sub
End Class