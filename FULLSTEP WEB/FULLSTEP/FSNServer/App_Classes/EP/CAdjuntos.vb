Option Strict Off
Option Explicit On

Imports Fullstep.FSNLibrary

'La clase ha sido cambiada a Public
<Serializable()> _
Public Class CAdjuntos
    Inherits Lista(Of Adjunto)

    Public Overloads ReadOnly Property Item(ByVal Id As String) As Adjunto
        Get
            Return Me.Find(Function(elemento As Adjunto) elemento.Id = Id)
        End Get
    End Property
    ''' <summary>
    ''' A�ade un archivo adjunto a la coleccion
    ''' </summary>
    ''' <param name="Id">Identificador del archivo adjunto</param>
    ''' <param name="Fecha">Fecha del a�adido</param>
    ''' <param name="Nombre">Nombre del archivo adjunto</param>
    ''' <param name="dataSize">Tama�o en Bytes del archivo adjunto</param>
    ''' <param name="Comentario">Comentario escrito por el usuario del archivo adjunto</param>
    ''' <param name="Cod">C�digo del usuario que ha a�adido el adjunto.</param>
    ''' <param name="oPer">Objeto de la clase cPersona con los datos del usuario que ha a�adido el adjunto</param>
    ''' <param name="OP">Operacion del adjunto</param>
    ''' <param name="IdEsp">Id en BBDD de la l�nea,o orden a la que pertenece el adjunto</param>
    ''' <returns>El objeto de la clase CAdjunto a�adido a la colecci�n</returns>
    ''' <remarks>Llamada desde: Todas las p�ginas que utilicen adjuntos para a�adir a la coleccion.
    ''' Tiempo m�ximo:0 seg</remarks>
    Public Overloads Function Add(ByVal Id As Integer, ByVal Fecha As Date, ByVal Nombre As String, ByVal dataSize As Object, ByVal Comentario As Object, ByVal Cod As Object, ByVal oPer As CPersona, ByVal OP As Object, ByVal IdEsp As Object, ByVal tipo As TiposDeDatos.Adjunto.Tipo) As Adjunto
        'create a new object
        'Dim sCod As String

        Dim objnewmember As New FSNServer.Adjunto(mDBServer, mIsAuthenticated)

        objnewmember.Fecha = Fecha
        objnewmember.Nombre = Nombre
        objnewmember.Id = Id

        'Antes era IsNull y IsMissing
        If IsNothing(dataSize) Or IsDBNull(dataSize) Then
            objnewmember.dataSize = 0
        Else
            objnewmember.dataSize = dataSize
        End If
        'Antes IsMissing
        If IsNothing(Cod) Then
            objnewmember.Cod = System.DBNull.Value
        Else
            objnewmember.Cod = DBNullToStr(Cod)
        End If

        If IsNothing(Comentario) Then
            objnewmember.Comentario = System.DBNull.Value
        Else
            objnewmember.Comentario = Comentario
        End If


        If IsNothing(OP) Then
            objnewmember.Operacion = "I"
        Else
            objnewmember.Operacion = OP
        End If

        'Antes IsMissing
        If IsNothing(IdEsp) Then
            objnewmember.IdEsp = System.DBNull.Value
        Else
            objnewmember.IdEsp = IdEsp
        End If

        objnewmember.Persona = oPer
        objnewmember.tipo = tipo

        Me.Add(objnewmember)
        Return objnewmember

    End Function
    ''' <summary>
    ''' Funcion que devolvera un dataset con los adjuntos de la linea de pedido y de la orden
    ''' </summary>
    ''' <param name="idOrden">identificador de la orden</param>
    ''' <param name="idLinea">identificador de la linea de pedido</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function DevolverAdjuntosLineaYOrden(ByVal idOrden As Long, ByVal idLinea As Long) As DataSet
        Dim DsResul As New DataSet
        DsResul = DBServer.Adjuntos_DevolverAdjuntosLineaYOrden(idOrden, idLinea)

        If DsResul.Tables.Count > 0 Then
            DsResul.Tables(0).TableName = "ADJUNTOS_LINEA"
            DsResul.Tables(1).TableName = "ADJUNTOS_ORDEN"
        End If
        Return DsResul
    End Function
    ''' <summary>
    ''' Funcion que devolvera un dataset con los adjuntos de la linea de pedido y de la orden
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function DevolverAdjuntosRecepcionPedido(ByVal pedidoRecep As Long) As DataSet
        Dim DsResul As New DataSet
        DsResul = DBServer.Adjuntos_DevolverAdjuntosRecepcionPedido(pedidoRecep)

        If DsResul.Tables.Count > 0 Then
            DsResul.Tables(0).TableName = "ADJUNTOS_RECEPCION_PEDIDO"
        End If
        Return DsResul
    End Function
    ''' <summary>
    ''' Constructor de la clase CAdjuntos
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si est� autenticado</param>
    ''' <remarks>
    ''' Llamada desde: Todos los sitios donde se instancien objetos de esta clase
    ''' Tiempo m�ximo: 0 sec. </remarks>
    Public Sub New(ByRef dbserver As Fullstep.FSNDataBaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
    End Sub
End Class