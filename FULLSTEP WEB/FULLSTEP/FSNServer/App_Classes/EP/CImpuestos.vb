Imports System.Data.SqlClient

<Serializable()> _
Public Class CImpuestos
    Public Sub New()

    End Sub
    Private _Id As Long
    Public Property Id() As Long
        Get
            Id = _Id
        End Get
        Set(ByVal Value As Long)
            _Id = Value
        End Set
    End Property

    Private _Codigo As String
    Public Property Codigo() As String
        Get
            Return _Codigo
        End Get
        Set(ByVal value As String)
            _Codigo = value
        End Set
    End Property
    Private _Denominacion As String
    Public Property Denominacion() As String
        Get
            Return _Denominacion
        End Get
        Set(ByVal value As String)
            _Denominacion = value
        End Set
    End Property
    Private _Valor As Object
    Public Property Valor() As Object
        Get
            Return _Valor
        End Get
        Set(ByVal value As Object)
            _Valor = value
        End Set
    End Property

    Private _Seleccionado As Boolean
    Public Property Seleccionado() As Boolean
        Get
            Seleccionado = _Seleccionado
        End Get
        Set(ByVal Value As Boolean)
            _Seleccionado = Value
        End Set
    End Property
End Class
