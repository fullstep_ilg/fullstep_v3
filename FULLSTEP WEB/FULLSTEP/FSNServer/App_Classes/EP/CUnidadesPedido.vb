Imports Fullstep.FSNLibrary

Public Class CUnidadesPedido
    Inherits Lista(Of CUnidadPedido)

    Public Overloads ReadOnly Property Item(ByVal Codigo As String) As CUnidadPedido
        Get
            Return Me.Find(Function(elemento As CUnidadPedido) elemento.Codigo = Codigo)
        End Get
    End Property

    ''' <summary>
    ''' Devuelve las unidades de pedido que se pueden aplicar a una l�nea de cat�logo dada
    ''' </summary>
    ''' <param name="CodIdioma">C�digo de idioma</param>
    ''' <param name="Linea">Id de la l�nea de cat�logo</param>
    ''' <returns>Devuelve un dataset con las unidades de pedido y su equivalencia con la unidad de compra</returns>
    ''' <remarks>Llamada desde EmisionPedido.aspx</remarks>
    Public Function DevolverUnidadesPedido(ByVal CodIdioma As FSNLibrary.Idioma, ByVal Linea As Integer) As DataSet
        Dim ds As DataSet = DBServer.UnidadesPedido_DevolverUnidadesPedido(CodIdioma, Linea)
        Me.Clear()
        For Each fila As DataRow In ds.Tables(0).Rows
            Dim unidad As New CUnidadPedido(mDBServer, mIsAuthenticated)
            unidad.Codigo = fila.Item("COD")
            unidad.Denominacion = fila.Item("DEN")
            unidad.FactorConversion = fila.Item("FC")
            unidad.CantidadMinima = DBNullToDbl(fila.Item("CANT_MIN"))
            If fila.Item("UC") IsNot DBNull.Value Then
                unidad.UnidadCompra = fila.Item("UC")
                unidad.DenUnidadCompra = fila.Item("DEN_UC")
            End If
            If Not DBNullToSomething(fila.Item("NUMDEC")) Is Nothing Then
                unidad.UnitFormat = New System.Globalization.NumberFormatInfo
                unidad.UnitFormat.NumberDecimalDigits = fila.Item("NUMDEC")
            End If
            If fila.Item("NUMDEC") Is System.DBNull.Value Then
                unidad.NumeroDeDecimales = Nothing
            Else
                unidad.NumeroDeDecimales = CType(fila.Item("NUMDEC"), Integer)
            End If
            Me.Add(unidad)
        Next
        Return ds
    End Function

    ''' <summary>
    ''' Carga todas las unidades en el idioma seleccionado
    ''' </summary>
    ''' <param name="CodIdioma">c�digo del idioma</param>
    ''' <returns>Un dataset con una tabla con las unidades</returns>
    ''' <remarks>
    ''' Llamada desde: PedidoLibre.aspx
    ''' Tiempo m�ximo: 0 sec.
    ''' </remarks>
    Public Function CargarTodasLasUnidades(ByVal CodIdioma As FSNLibrary.Idioma) As DataSet
        Dim data As DataSet = DBServer.UnidadesPedido_CargarTodasLasUnidades(CodIdioma)
        Me.Clear()
        For Each fila As DataRow In data.Tables(0).Rows
            Dim unidad As New CUnidadPedido(mDBServer, mIsAuthenticated)
            unidad.Codigo = fila.Item("COD")
            unidad.Denominacion = fila.Item("DEN")
            If Not DBNullToSomething(fila.Item("NUMDEC")) Is Nothing Then
                unidad.UnitFormat = New System.Globalization.NumberFormatInfo
                unidad.UnitFormat.NumberDecimalDigits = fila.Item("NUMDEC")
            End If
            If fila.Item("NUMDEC") Is System.DBNull.Value Then
                unidad.NumeroDeDecimales = Nothing
            Else
                unidad.NumeroDeDecimales = CType(fila.Item("NUMDEC"), Integer)
            End If
            Me.Add(unidad)
        Next
        Return data
    End Function

    ''' <summary>
    ''' Constructor de la clase CUnidadesPedido
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si est� autenticado</param>
    Public Sub New(ByRef dbserver As Fullstep.FSNDataBaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
    End Sub

End Class