﻿Imports Fullstep.FSNLibrary

<Serializable()> _
Public Class CPago
    Inherits Security

    Private m_IIdPago As Integer
    Private m_IIdFra As Integer
    Private m_sNumPago As String
    Private m_sNumFra As String
    Private m_dtFecha As DateTime
    'Private m_ILineaFactura As Integer
    Private m_ILineaPedido As Integer
    Private m_IEstadoId As Integer
    Private m_sEstadoDen As String

    Public Property IdPago() As Integer
        Get
            IdPago = m_IIdPago
        End Get
        Set(ByVal value As Integer)
            m_IIdPago = value
        End Set
    End Property

    Public Property IdFra() As Integer
        Get
            IdFra = m_IIdFra
        End Get
        Set(ByVal value As Integer)
            m_IIdFra = value
        End Set
    End Property

    Public Property NumPago() As String
        Get
            NumPago = m_sNumPago
        End Get
        Set(ByVal value As String)
            m_sNumPago = value
        End Set
    End Property

    Public Property NumFra() As String
        Get
            NumFra = m_sNumFra
        End Get
        Set(ByVal value As String)
            m_sNumFra = value
        End Set
    End Property

    Public Property Fecha() As DateTime
        Get
            Fecha = m_dtFecha
        End Get
        Set(ByVal value As DateTime)
            m_dtFecha = value
        End Set
    End Property

    'Public Property LineaFactura() As Integer
    '    Get
    '        LineaFactura = m_ILineaFactura
    '    End Get
    '    Set(ByVal value As Integer)
    '        m_ILineaFactura = value
    '    End Set
    'End Property

    Public Property LineaPedido() As Integer
        Get
            LineaPedido = m_ILineaPedido
        End Get
        Set(ByVal value As Integer)
            m_ILineaPedido = value
        End Set
    End Property

    Public Property EstadoId() As Integer
        Get
            EstadoId = m_IEstadoId
        End Get
        Set(ByVal value As Integer)
            m_IEstadoId = value
        End Set
    End Property

    Public Property EstadoDen() As String
        Get
            EstadoDen = m_sEstadoDen
        End Get
        Set(ByVal value As String)
            m_sEstadoDen = value
        End Set
    End Property

    ''' <summary>
    ''' Constructor de la clase CPago
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si está autenticado</param>
    Public Sub New(ByRef dbserver As Fullstep.FSNDataBaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
    End Sub
End Class
