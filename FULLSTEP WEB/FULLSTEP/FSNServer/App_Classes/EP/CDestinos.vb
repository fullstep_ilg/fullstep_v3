Imports System.Data.SqlClient
Imports Fullstep.FSNLibrary

'Ahora definido como class
<Serializable()> _
Public Class CDestinos
    Inherits Lista(Of CDestino)

    ''' <summary>
    ''' A�ade un nuevo elemento CDestino a la lista
    ''' </summary>
    ''' <param name="Cod">C�digo</param>
    ''' <param name="Den">Denominaci�n</param>
    ''' <param name="SinTransporte">Destino ST</param>
    ''' <param name="Direccion">Direcci�n</param>
    ''' <param name="Pob">Poblaci�n</param>
    ''' <param name="CP">C�digo Postal</param>
    ''' <param name="Provi">Provincia</param>
    ''' <param name="Pai">Pa�s</param>
    ''' <param name="sUON1">C�digo UON1</param>
    ''' <param name="sUON2">C�digo UON2</param>
    ''' <param name="sUON3">C�digo UON3</param>
    ''' <param name="sUON1DEN">Denominaci�n UON1</param>
    ''' <param name="sUON2DEN">Denominaci�n UON2</param>
    ''' <param name="sUON3DEN">Denominaci�n UON3</param>
    ''' <param name="almacenes">Objeto CDestino.Almacenes que contiene los almacenes del destino</param>
    ''' <param name="bRecepcion">Indica si tiene permiso de recepci�n</param>
    ''' <returns>El elemento CDestino a�adido</returns>
    ''' <remarks></remarks>
    ''' <revision>LTG 10/06/2013</revision>
    Public Overloads Function Add(ByVal Cod As String, ByVal Den As String, ByVal SinTransporte As Boolean, ByVal Direccion As Object, ByVal Pob As Object, ByVal CP As Object,
                                  ByVal Provi As Object, ByVal Pai As Object, ByVal sUON1 As Object, ByVal sUON2 As Object, ByVal sUON3 As Object, ByVal sUON1DEN As Object,
                                  ByVal sUON2DEN As Object, ByVal sUON3DEN As Object, Optional ByVal almacenes As cAlmacenes = Nothing, Optional ByVal bRecepcion As Boolean = False, _
                                  Optional ByVal sTfno As String = "", Optional ByVal sFAX As String = "", Optional ByVal sEmail As String = "") As CDestino
        'create a new object
        Dim objnewmember As CDestino
        objnewmember = New CDestino(mDBServer, mIsAuthenticated)
        With objnewmember
            .Cod = Cod
            .Den = Den
            .SinTransporte = SinTransporte
            .Direccion = IIf(IsNothing(Direccion) Or IsDBNull(Direccion), "", Direccion)
            .Pob = IIf(IsNothing(Pob) Or IsDBNull(Pob), "", Pob)
            .CP = IIf(IsNothing(CP) Or IsDBNull(CP), "", CP)
            .Provi = IIf(IsNothing(Provi) Or IsDBNull(Provi), "", Provi)
            .Pai = IIf(IsNothing(Pai) Or IsDBNull(Pai), "", Pai)
            .UON1 = sUON1
            .UON2 = sUON2
            .UON3 = sUON3
            .UON1DEN = sUON1DEN
            .UON2DEN = sUON2DEN
            .UON3DEN = sUON3DEN
            .Recepcion = bRecepcion
            .Almacenes = almacenes
            .Tfno = sTfno
            .FAX = sFAX
            .Email = sEmail
        End With

        Me.Add(objnewmember)
        Return objnewmember

    End Function

    Public Overloads ReadOnly Property Item(ByVal Cod As String) As CDestino
        Get
            Item = Me.Find(Function(elemento As CDestino) elemento.Cod = Cod)
            Return Item
        End Get
    End Property

    ''' <summary>
    ''' Constructor de la clase CDestinos
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si est� autenticado</param>
    ''' <remarks>
    ''' Llamada desde: Todos los sitios donde se instancien objetos de esta clase
    ''' Tiempo m�ximo: 0 sec. </remarks>
    Public Sub New(ByRef dbserver As Fullstep.FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub

    ''' <summary>
    ''' Devuelve todos los destinos posibles del usuario en el idioma seleccionado.
    ''' </summary>
    ''' <param name="sIdi">Idioma de la aplicacion</param>
    ''' <param name="usu">C�digo de usuario de la persona que este usando la aplicaci�n</param>
    ''' <returns>Un DataSet con todos los destinos posibles para los par�metros pasados</returns>
    ''' <remarks>
    ''' Llamada desde: La clase cDestinos, m�todo CargarTodosLosDestinos</remarks>
    Public Function CargarTodosLosDestinos(ByVal sIdi As String, ByVal usu As String) As DataSet
        Dim ador As New DataSet
        ador = mDBServer.Destinos_CargarTodosLosDestinos(sIdi, usu)
        If ador.Tables.Count = 0 Then
            ador.Clear()
            ador = Nothing
            CargarTodosLosDestinos = Nothing
            CargarTodosLosDestinos.Dispose()
            Exit Function
        Else
            CargarTodosLosDestinos = ador
            Return ador
            ador.Clear()
        End If

    End Function

    ''' <summary>
    ''' Funci�n que devuelve un dataset con los datos del destino aprovisionadores para el usuario actual
    ''' </summary>
    ''' <param name="sIdi">Codigo del idioma</param>
    ''' <param name="Destino">Codigo del destino</param>
    ''' <returns>un dataset con los datos del destino aprovisionadores para el usuario actual</returns>
    ''' <remarks>
    ''' Llamada desde: La funci�n CargarDatosDestino de Destino.vb
    ''' Tiempo: 0 seg</remarks>
    Public Function CargarDatosDestino(ByVal sIdi As String, ByVal Destino As String, Optional ByVal iCodLinea As Integer = 0) As DataSet
        Authenticate()
        Return DBServer.Destinos_CargarDatosDestino(sIdi, Destino, iCodLinea)
    End Function
    Public Sub CargarUnDestino(ByVal sIdi As String, Optional sIdDest As String = "", Optional ByVal iCodLinea As Integer = 0)
        Dim oDestinos As FSNServer.CDestinos = New CDestinos(DBServer, mIsAuthenticated)
        Dim dsDatosDestino As DataSet
        Dim oCDestinos As FSNServer.CDestinos = New CDestinos(DBServer, mIsAuthenticated)
        dsDatosDestino = oCDestinos.CargarDatosDestino(sIdi, sIdDest, iCodLinea)
        dsDatosDestino.Tables(0).TableName = "DESTINOS"
        Dim key() As DataColumn = {dsDatosDestino.Tables("DESTINOS").Columns("COD")}
        dsDatosDestino.Tables("DESTINOS").PrimaryKey = key


        For i As Integer = 0 To dsDatosDestino.Tables("DESTINOS").Rows.Count - 1
            With dsDatosDestino.Tables("DESTINOS").Rows(i)
                'He cambiado los parametros que estaban opcionales y sin valor en la llamada, por sus valores por defecto para quitar la clausula Optional
                Me.Add(DBNullToSomething(.Item("COD")), DBNullToSomething(.Item("DEN")), DBNullToSomething(.Item("SINTRANS")), DBNullToSomething(.Item("DIR")),
                    DBNullToSomething(.Item("POB")), DBNullToSomething(.Item("CP")), DBNullToSomething(.Item("PROVI")), DBNullToSomething(.Item("PAI")), Nothing,
                    Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, , .Item("TFNO").ToString, .Item("FAX").ToString, .Item("Email").ToString)
            End With
        Next

        dsDatosDestino.Clear()
        dsDatosDestino = Nothing

    End Sub

End Class