Imports System.Collections.Generic

<Serializable()> _
Public Class CRecepciones
    Inherits Lista(Of CRecepcion)

    ''' <summary>
    ''' Constructor de la clase
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si est� autenticado</param>
    ''' <remarks>
    ''' Llamada desde: Todos los sitios en los que se instancie un objeto de la clase COrden
    ''' Tiempo m�ximo: 0 sec
    ''' </remarks>
    Public Sub New(ByRef dbserver As Fullstep.FSNDataBaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
    Public Overloads Sub Add(ByVal Orden As Integer, ByVal Id As Integer, ByVal Fecha As String, ByVal Albaran As String, ByVal correcto As Boolean, ByVal Obs As String, ByVal pedido As Integer, ByVal Facturas_No_Anul As Boolean, ByVal OrdenTipo As Integer, ByVal OrdenEstado As Integer, ByVal OrdenProveedor As Integer, ByVal OrdenIncorrecta As Integer, ByVal CantRecep As Integer)

        Dim objnewmember As New CRecepcion(mDBServer, mIsAuthenticated)

        objnewmember.Orden = Orden
        If Not IsNothing(Id) Then
            objnewmember.ID = Id
        End If
        If Not IsNothing(Fecha) Then
            objnewmember.Fecha = Fecha
        End If
        If Not IsNothing(Albaran) Then
            objnewmember.Albaran = Albaran
        End If
        If Not IsNothing(correcto) Then
            objnewmember.Correcto = correcto
        End If
        If Not IsNothing(Obs) Then
            objnewmember.Obs = Obs
        End If
        If Not IsNothing(pedido) Then
            objnewmember.Pedido = pedido
        End If
        If Not IsNothing(Facturas_No_Anul) Then
            objnewmember.Fras_No_Anul = Facturas_No_Anul
        End If
        If Not IsNothing(OrdenTipo) Then
            objnewmember.OrdenTipo = OrdenTipo
        End If
        If Not IsNothing(OrdenEstado) Then
            objnewmember.OrdenEstado = OrdenEstado
        End If
        If Not IsNothing(OrdenProveedor) Then
            objnewmember.OrdenProveedor = OrdenProveedor
        End If
        If Not IsNothing(OrdenIncorrecta) Then
            objnewmember.OrdenIncorrecta = OrdenIncorrecta
        End If
        If Not IsNothing(CantRecep) Then
            objnewmember.CantRecep = CantRecep
        End If

        MyBase.Add(objnewmember)
        objnewmember = Nothing

    End Sub
    ''' Revisado por: blp. Fecha: 06/03/2012
    ''' <summary>
    ''' Funci�n que devuelve un objeto de tipo CRecepciones con los datos de las recepciones vinculadas a la linea de pedido dada. Si no hay l�nea de pedido, devuelve todas las recepciones
    ''' </summary>
    ''' <param name="Linea_Pedido">Integer. Id de la l�nea de pedido de la que se quiere recuperar las recepciones vinculadas a la l�nea</param>
    ''' <param name="FSFAActivado">Informa del estado activado o no del m�dulo de Facturas. En caso de estar activado se devolver�n las recepciones no vinculadas a facturas con estado no anulado</param>
    ''' <param name="sIdioma">Idioma del usuario</param>
    ''' <returns>Un objeto de la clase CRecepciones con la relaci�n de recepciones-lineas de pedido</returns>
    ''' <remarks>
    ''' Llamada desde: Seguimiento.aspx.vb. M�x. 0,5 seg.</remarks>
    Public Function CargarRecepciones(ByVal Linea_Pedido As Integer, ByVal FSFAActivado As Boolean, ByVal sIdioma As String) As CRecepciones
        Dim oCRecepciones As CRecepciones
        Dim oCRecepcion As CRecepcion
        Dim oRoot As New Root
        oCRecepciones = New CRecepciones(DBServer, mIsAuthenticated)

        Dim DsResul As New DataSet
        DsResul = DBServer.Linea_DevolverRecepciones(Linea_Pedido, FSFAActivado, sIdioma)
        If DsResul.Tables(0).Rows.Count > 0 Then
            For i As Long = 0 To DsResul.Tables(0).Rows.Count - 1
                oCRecepcion = New CRecepcion(DBServer, mIsAuthenticated)
                With DsResul.Tables(0).Rows(i)
                    oCRecepcion.ID = .Item("RECEP_ID")
                    oCRecepcion.Pedido = .Item("PEDIDO")
                    oCRecepcion.Orden = .Item("ORDEN")
                    oCRecepcion.Fecha = .Item("RECEP_FECHA")
                    oCRecepcion.Albaran = .Item("ALBARAN")
                    oCRecepcion.NumRecepERP = DBNullToStr(.Item("NUM_RECEP_ERP"))
                    oCRecepcion.Correcto = .Item("CORRECTO")
                    If .Item("RECEP_OBS") Is DBNull.Value Then
                        oCRecepcion.Obs = ""
                    Else
                        oCRecepcion.Obs = .Item("RECEP_OBS")
                    End If
                    oCRecepcion.OrdenEstado = DBNullToSomething(.Item("ORDEN_EST"))
                    oCRecepcion.OrdenProveedor = DBNullToStr(.Item("ORDEN_PROVE"))
                    oCRecepcion.OrdenTipo = DBNullToSomething(.Item("ORDEN_TIPO"))
                    oCRecepcion.OrdenIncorrecta = DBNullToSomething(.Item("INCORRECTA"))
                    oCRecepcion.CantRecep = DBNullToSomething(.Item("CANT"))
                    oCRecepcion.ImporteRecep = DBNullToSomething(.Item("IMPORTE"))
                    oCRecepcion.Unidad = DBNullToSomething(.Item("UNIDAD"))
                    oCRecepcion.DenUnidad = DBNullToSomething(.Item("DENUNIDAD"))
                    oCRecepcion.NumDecUnidad = DBNullToSomething(.Item("NUMDEC"))
                    oCRecepcion.CantPedida = DBNullToSomething(.Item("CANTIDAD"))
                    oCRecepcion.ImportePedido = DBNullToSomething(.Item("IMPORTE_PED"))
                    oCRecepcion.CodArt = DBNullToSomething(.Item("CODART"))
                    oCRecepcion.DenArt = DBNullToSomething(.Item("DESCR"))
                    oCRecepcion.Moneda = DBNullToSomething(.Item("MONEDA"))
                    oCRecepcion.PrecioUnitario = DBNullToSomething(.Item("PRECIOUNITARIO"))
                    oCRecepcion.LineaPedido = DBNullToSomething(.Item("LINEA_PEDIDO"))
                    oCRecepcion.LineaRecep = DBNullToSomething(.Item("LINEA_RECEP"))
                    oCRecepcion.NumPedido = DBNullToSomething(.Item("NUM_PEDIDO"))
                    oCRecepcion.NumERP = DBNullToSomething(.Item("NUM_PED_ERP"))
                    oCRecepcion.Factura = DBNullToSomething(.Item("FACTURA"))
                    oCRecepcion.Importe = DBNullToSomething(.Item("IMPORTE"))
                    oCRecepcion.NumLineasAlbaran = DBNullToInteger(.Item("NUM_LINEAS_ALBARAN"))
                    If FSFAActivado Then
                        oCRecepcion.Fras_No_Anul = SQLBinaryToBoolean(.Item("FRA_NO_ANUL_ASOCIADA"))
                    End If
                    oCRecepcion.Receptor = DBNullToSomething(.Item("RECEPTOR"))
                    oCRecepcion.AlbaranBloqueadoParaFacturacion = DBNullToSomething(.Item("IM_BLOQUEO"))
                    oCRecepcion.AlbaranBloqueadoParaFacturacionObservaciones = DBNullToStr(.Item("IM_BLOQUEO_OBS"))
                    oCRecepcion.TipoRecepcion = DBNullToDbl(.Item("TIPORECEPCION"))
                End With
                oCRecepciones.Add(oCRecepcion)
                oCRecepcion = Nothing
            Next
        Else
            Return Nothing
        End If

        Return oCRecepciones

    End Function
#Region "ALBARAN"
    ''' Revisado por: blp. Fecha: 06/03/2012
    ''' <summary>
    ''' Funci�n que devuelve un objeto de tipo CRecepciones con los datos de las recepciones que cumplen con los tres requisitos pasados como par�metros:
    ''' mismo n�mero de albar�n, fecha de recepci�n y c�digo de proveedor
    ''' </summary>
    ''' <param name="Albaran">N�mero de albar�n</param>
    ''' <param name="FechaAlbaran">Fecha de recepci�n del albar�n</param>
    ''' <param name="Prove">C�digo del proveedor</param>
    ''' <remarks>
    ''' Llamada desde: Seguimiento.aspx.vb. M�x. 0,5 seg.</remarks>
    Public Sub CargarRecepcionesAlbaran(ByVal Albaran As String, ByVal FechaAlbaran As Date, ByVal Prove As String)
        Dim oCRecepcion As CRecepcion
        Dim oRoot As New Root
        Dim DsResul As New DataSet
        DsResul = DBServer.Recepcion_DatosRecepcionAlbaran(Albaran, FechaAlbaran, Prove)
        If DsResul.Tables(0).Rows.Count > 0 Then
            For i As Long = 0 To DsResul.Tables(0).Rows.Count - 1
                oCRecepcion = New CRecepcion(DBServer, mIsAuthenticated)
                With DsResul.Tables(0).Rows(i)
                    oCRecepcion.OrdenEstado = DBNullToSomething(.Item("ORDEN_EST"))
                    oCRecepcion.OrdenIncorrecta = DBNullToSomething(.Item("INCORRECTA"))
                    oCRecepcion.OrdenTipo = DBNullToSomething(.Item("ORDEN_TIPO"))
                    oCRecepcion.Pedido = DBNullToSomething(.Item("PEDIDO"))
                    oCRecepcion.Correcto = DBNullToSomething(.Item("CORRECTO"))
                    oCRecepcion.ID = DBNullToSomething(.Item("RECEP_ID"))
                    oCRecepcion.Obs = DBNullToStr(.Item("RECEP_OBS"))
                    oCRecepcion.Orden = DBNullToSomething(.Item("ORDEN"))
                End With
                Me.Add(oCRecepcion)
                oCRecepcion = Nothing
            Next
        End If
    End Sub
    ''' <summary>
    ''' Funci�n que devuelve un dataset con los datos de todos los albaranes de una orden de entrega
    ''' mismo n�mero de albar�n, fecha de recepci�n y c�digo de proveedor
    ''' </summary>
    ''' <param name="IdOrden">Id de la orden de entrega</param>
    ''' <param name="sIdioma">Codigo del idioma de usuario</param>
    ''' <remarks>
    ''' Llamada desde: Seguimiento.aspx.vb </remarks>
    Public Function CargarAlbaranesOrden(ByVal IdOrden As Integer, Optional ByVal sIdioma As String = "SPA") As DataSet
        Dim oRoot As New Root
        Dim DsResul As New DataSet
        DsResul = DBServer.Orden_Albaranes(IdOrden, sIdioma)

        DsResul.Tables(0).TableName = "ALBARANES"
        DsResul.Tables(1).TableName = "LINEASALBARAN"
        'Creamos una key en funcion del albaran
        Dim key() As DataColumn = {DsResul.Tables("ALBARANES").Columns("ALBARAN")}
        DsResul.Tables("ALBARANES").PrimaryKey = key
        Dim keylp() As DataColumn = {DsResul.Tables("LINEASALBARAN").Columns("ALBARAN")}

        DsResul.Relations.Add("REL_ALBARANES_LINEASALBARAN", key, keylp, False)

        Return DsResul
    End Function
    ''' Revisado por: blp. Fecha: 09/10/2012
    ''' <summary>
    ''' Eliminar la recepcion indicada y restaurar los datos que fueron modificados al crearla (estado del pedido, etc)
    ''' </summary>
    ''' <param name="Albaran">N�mero de albar�n</param>
    ''' <param name="FechaAlbaran">Fecha de recepci�n del albar�n</param>
    ''' <param name="Prove">C�digo del proveedor</param>
    ''' <param name="acceso_giRecepAprov">Valor de INTEGRACION de las recepciones de EP</param>
    ''' <param name="acceso_giRecepDirecto">Valor de INTEGRACION de las recepciones de GS (Directas)</param>
    ''' <param name="codPersona">C�digo de persona de quien efect�a la eliminaci�n</param>
    ''' <param name="codUsuario">C�digo de usuario de quien efect�a la eliminaci�n</param>
    ''' <param name="Usu_idioma">Idioma en que trabaja el usuario</param>
    ''' <param name="ParGen">Par�metros Generales</param>
    ''' <returns>
    ''' El resultado del proceso de eliminaci�n. Si el resultado es 0 es que se ha eliminado la recepcion sin problemas
    ''' </returns>
    ''' <remarks>
    ''' Llamada desde: Seguimiento.aspx.vb.grdRecepciones_OnRowCommand 
    ''' </remarks>
    Public Function EliminarRecepcionesAlbaran(ByVal Albaran As String, ByVal FechaAlbaran As Date, ByVal Prove As String, ByVal acceso_giRecepAprov As TiposDeDatos.OrigenRegistroRecepcionesPedido, ByVal acceso_giRecepDirecto As TiposDeDatos.OrigenRegistroRecepcionesPedido, ByVal codPersona As String, ByVal codUsuario As String, ByVal Usu_idioma As FSNLibrary.Idioma, ByVal ParGen As TiposDeDatos.ParametrosGenerales) As String
        Authenticate()
        'Cargamos los datos de recepciones en la clase
        CargarRecepcionesAlbaran(Albaran, FechaAlbaran, Prove)
        'Si hay datos, continuamos con la eliminaci�n
        If Me.Any Then
            Dim rdoEliminacion As String = String.Empty
            For Each Recepcion As CRecepcion In Me
                Dim oIntegracion As New Integracion(DBServer, True)
                Dim bGrabarEnLog As Boolean = Recepcion.GrabarEnLog(acceso_giRecepAprov, acceso_giRecepDirecto, ParGen.gbActivLog)
                rdoEliminacion += DBServer.Recepcion_EliminarRecepcion(Recepcion.ID, Recepcion.OrdenEstado, Recepcion.Orden, Recepcion.Pedido, Prove, bGrabarEnLog, Recepcion.dtOrigen, Recepcion.OrdenTipo, FechaAlbaran, Albaran, Recepcion.Correcto, Recepcion.Obs, codPersona, codUsuario, Recepcion.OrdenIncorrecta, ParGen.gbAccesoFSSM, Usu_idioma, ParGen.g_bAccesoFSFA) & TextosSeparadores.dosArrobas

                If bGrabarEnLog Then 'Si se graba en LOG, procederemos a llamar al Servicio de Integraci�n.
                    oIntegracion.LlamarFSIS(TablasIntegracion.Rec_Aprov, Recepcion.ID, Recepcion.Pedido, Recepcion.Orden)
                End If
            Next
            If rdoEliminacion.EndsWith(TextosSeparadores.dosArrobas) Then _
                rdoEliminacion = rdoEliminacion.TrimEnd(TextosSeparadores.dosArrobas)
            Return rdoEliminacion
        Else
            Return "1" 'Error
        End If
    End Function
#End Region
    ''' Revisado por: blp. Fecha: 23/08/2012
    ''' <summary>
    ''' M�todo que obtiene la configuraci�n de los campos a mostrar y su ancho en el Visor de Entregas (EntregasPedidos.aspx) del portal para el usuario y la compa��a (proveedor) dados
    ''' </summary>
    ''' <param name="CodUsu">C�digo del usuario</param>
    ''' <remarks>Llamada desde PMPortalWeb\script\entregas\entregasPedidos.asmx.vb->CreatewhdgEntregasColumns</remarks>
    Public Function Obt_Conf_Visor_Recepciones(ByVal CodUsu As String) As DataTable
        Authenticate()
        Return DBServer.Ordenes_Recepciones_Obt_Conf_Visor_Recepciones(CodUsu)
    End Function
    ''' Revisado por: blp. Fecha: 23/08/2012
    ''' <summary>
    ''' Guarda la configuraci�n de los campos a mostrar y su ancho en el Visor de recepciones (recepciones.aspx) para el usuario dado
    ''' </summary>
    ''' <param name="CodUsu">C�digo del usuario</param>
    ''' <param name="DatosVisibilidadAnchoPosicion">Tabla con los datos de visibilidad, ancho y posici�n de cada campo</param>
    ''' <remarks>
    ''' Llamadas desde EntregasPedido.aspx. M�x 1 seg
    ''' </remarks>
    Public Sub Mod_Conf_Visor_Recepciones(ByVal CodUsu As String, ByVal DatosVisibilidadAnchoPosicion As DataTable)
        Authenticate()
        DBServer.Ordenes_Recepciones_Mod_Conf_Visor_Recepciones(CodUsu, DatosVisibilidadAnchoPosicion)
    End Sub
    '''Revisado por: blp. Fecha: 27/08/2012
    ''' <summary>
    ''' Funci�n que crea la estructura de la tabla de partidas que usaremos en los filtros para obtener las recepciones
    ''' </summary>
    ''' <returns>DataTable vac�o</returns>
    ''' <remarks>Llamada desde Recepciones.aspx.vb->ActualizarDsetpedidos. M�x. 0,1 seg.</remarks>
    Public Function CrearTablaEP_CONF_VISOR_RECEPCIONES() As DataTable
        Dim dtEP_CONF_VISOR_RECEPCIONES As DataTable = New DataTable("TB_EP_CONF_VISOR_RECEPCIONES")
        dtEP_CONF_VISOR_RECEPCIONES.Columns.Add("TIPOCAMPO", GetType(System.Int32))
        dtEP_CONF_VISOR_RECEPCIONES.Columns.Add("ID", GetType(System.String))
        dtEP_CONF_VISOR_RECEPCIONES.Columns.Add("VISIBLE", GetType(System.Boolean))
        dtEP_CONF_VISOR_RECEPCIONES.Columns.Add("POSICION", GetType(System.Int32))
        dtEP_CONF_VISOR_RECEPCIONES.Columns.Add("ANCHO", GetType(System.Double))
        Return dtEP_CONF_VISOR_RECEPCIONES
    End Function
End Class