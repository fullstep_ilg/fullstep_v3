﻿<Serializable()> _
Public Class cAlmacenes
    Inherits Lista(Of cAlmacen)

    ''' <summary>
    ''' Añade un nuevo elemento cAlmacen a la lista
    ''' </summary>
    ''' <param name="Cod">Código</param>
    ''' <param name="Den">Denominación</param>
    ''' <param name="Dest">Elemento de tipo cDestino</param>
    ''' <param name="CodDest">Codigo de Destino</param>
    ''' <returns>El elemento cAlmacen añadido</returns>
    ''' <remarks></remarks>
    Public Overloads Function Add(ByVal Id As Integer, ByVal Cod As String, ByVal Den As String, ByVal CodDest As String, ByVal Dest As CDestino) As cAlmacen
        'create a new object
        Dim objnewmember As cAlmacen
        objnewmember = New cAlmacen(mDBServer, mIsAuthenticated)
        With objnewmember
            .Id = Id
            .Codigo = Cod
            .Denominacion = Den
            .Destino = Dest
            .CodigoDestino = CodDest
        End With

        Me.Add(objnewmember)
        Return objnewmember

    End Function

    Public Overloads ReadOnly Property Item(ByVal Cod As String) As cAlmacen
        Get
            Return Me.Find(Function(elemento As cAlmacen) elemento.Codigo = Cod)
        End Get
    End Property

    ''' <summary>
    ''' Constructor de la clase cAlmacenes
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si está autenticado</param>
    ''' <remarks>
    ''' Llamada desde: Todos los sitios donde se instancien objetos de esta clase
    ''' Tiempo máximo: 0 sec. </remarks>
    Public Sub New(ByRef dbserver As Fullstep.FSNDataBaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
    End Sub

End Class
