Imports Fullstep.FSNLibrary

<Serializable()> _
Public Class CPersona
    Inherits Security

    Private msCod As String
    Private msNom As String
    Private msApe As String
    Private msNomApe As String
    Private msDep As String
    Private msDepDen As Object
    Private msCargo As Object
    Private msEmail As String
    Private msTfno As Object
    Private msTfno2 As Object
    Private msFax As Object
    Private msUON1 As Object
    Private msUON2 As Object
    Private msUON3 As Object
    Private msUON1DEN As Object
    Private msUON2DEN As Object
    Private msUON3DEN As Object
    Private msEqp As Object

    Private moDestinos As CDestinos
    Private mlEmpresa As Integer
    Private moEmpresas As CEmpresas

    Public Property Cod() As String
        Get
            Cod = msCod
        End Get
        Set(ByVal Value As String)
            msCod = Value
        End Set
    End Property
    Public Property Nombre() As String
        Get
            Nombre = msNom
        End Get
        Set(ByVal Value As String)
            msNom = Value
        End Set
    End Property
    Public Property Apellidos() As String
        Get
            Apellidos = msApe
        End Get
        Set(ByVal Value As String)
            msApe = Value
        End Set
    End Property
    Public Property NomApe() As String
        Get
            NomApe = msNomApe
        End Get
        Set(ByVal Value As String)
            msNomApe = Value
        End Set
    End Property
    Public Property Dep() As String
        Get
            Dep = msDep
        End Get
        Set(ByVal Value As String)
            msDep = Value
        End Set
    End Property
    Public Property DepDen() As Object
        Get
            DepDen = msDepDen
        End Get
        Set(ByVal Value As Object)
            msDepDen = Value
        End Set
    End Property
    Public Property Cargo() As Object
        Get
            Cargo = msCargo
        End Get
        Set(ByVal Value As Object)
            msCargo = Value
        End Set
    End Property
    Public Property Email() As String
        Get
            Email = msEmail
        End Get
        Set(ByVal Value As String)
            msEmail = Value
        End Set
    End Property
    Public Property Tfno() As Object
        Get
            Tfno = msTfno
        End Get
        Set(ByVal Value As Object)
            msTfno = Value
        End Set
    End Property
    Public Property Tfno2() As Object
        Get
            Tfno2 = msTfno2
        End Get
        Set(ByVal Value As Object)
            msTfno2 = Value
        End Set
    End Property
    Public Property Fax() As Object
        Get
            Fax = msFax
        End Get
        Set(ByVal Value As Object)
            msFax = Value
        End Set
    End Property
    Public Property UON1() As Object
        Get
            UON1 = msUON1
        End Get
        Set(ByVal Value As Object)
            msUON1 = Value
        End Set
    End Property
    Public Property UON2() As Object
        Get
            UON2 = msUON2
        End Get
        Set(ByVal Value As Object)
            msUON2 = Value
        End Set
    End Property
    Public Property UON3() As Object
        Get
            UON3 = msUON3
        End Get
        Set(ByVal Value As Object)
            msUON3 = Value
        End Set
    End Property
    Public Property UON1DEN() As Object
        Get
            UON1DEN = msUON1DEN
        End Get
        Set(ByVal Value As Object)
            msUON1DEN = Value
        End Set
    End Property
    Public Property UON2DEN() As Object
        Get
            UON2DEN = msUON2DEN
        End Get
        Set(ByVal Value As Object)
            msUON2DEN = Value
        End Set
    End Property
    Public Property UON3DEN() As Object
        Get
            UON3DEN = msUON3DEN
        End Get
        Set(ByVal Value As Object)
            msUON3DEN = Value
        End Set
    End Property
    Public Property Eqp() As Object
        Get
            Eqp = msEqp
        End Get
        Set(ByVal Value As Object)
            msEqp = Value
        End Set
    End Property
    Public Property Destinos() As CDestinos
        Get
            Destinos = moDestinos
        End Get
        Set(ByVal Value As CDestinos)
            moDestinos = Value
        End Set
    End Property
    Public Property Empresa() As Integer
        Get
            Empresa = mlEmpresa
        End Get
        Set(ByVal Value As Integer)
            mlEmpresa = Value
        End Set
    End Property
    Public Property Empresas() As CEmpresas
        Get
            Empresas = moEmpresas
        End Get
        Set(ByVal Value As CEmpresas)
            moEmpresas = Value
        End Set
    End Property
    Public Sub New(ByRef dbserver As Fullstep.FSNDataBaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
    End Sub
    ''' <summary>
    ''' Carga todas las personas posibles a las que poder enviar un pedido como destino para el usuario dado, en el idioma pasado como par�metro.
    ''' </summary>
    ''' <param name="sIdi">Idioma del usuario actual</param>
    ''' <remarks>
    ''' Llamada desde: p�gina NuevoFavorito.aspx
    ''' Tiempo m�ximo: 0 sec.
    ''' </remarks>
    Public Sub CargarDestinos(ByVal sIdi As String, Optional sIdDest As String = "", Optional ByVal SoloDestinoRecepcion As Boolean = False, Optional ByVal SoloDestinoEmision As Boolean = False)

        Dim adoRS As New DataSet
        adoRS = mDBServer.Persona_CargarDestinos(sIdi, Cod, sIdDest, SoloDestinoRecepcion, SoloDestinoEmision)

        adoRS.Tables(0).TableName = "DESTINOS"
        adoRS.Tables(1).TableName = "ALMACENES"
        Dim key() As DataColumn = {adoRS.Tables("DESTINOS").Columns("COD")}
        adoRS.Tables("DESTINOS").PrimaryKey = key
        Dim keylp() As DataColumn = {adoRS.Tables("ALMACENES").Columns("DESTALM")}
        adoRS.Tables("ALMACENES").PrimaryKey = keylp
        keylp(0) = adoRS.Tables("ALMACENES").Columns("CODDEST")
        adoRS.Relations.Add("REL_DESTINOS_ALMACENES", key, keylp, False)

        moDestinos = New CDestinos(mDBServer, mIsAuthenticated)

        For i As Integer = 0 To adoRS.Tables("DESTINOS").Rows.Count - 1
            With adoRS.Tables("DESTINOS").Rows(i)
                Dim moAlmacenes As cAlmacenes = New cAlmacenes(mDBServer, mIsAuthenticated)
                If .GetChildRows("REL_DESTINOS_ALMACENES").Count > 0 Then
                    For j As Integer = 0 To .GetChildRows("REL_DESTINOS_ALMACENES").Count - 1
                        With .GetChildRows("REL_DESTINOS_ALMACENES")(j)
                            moAlmacenes.Add(.Item("ID"), .Item("COD"), .Item("DEN"), .Item("CODDEST"), Nothing)
                        End With
                    Next
                Else
                    moAlmacenes = Nothing
                End If
                'He cambiado los parametros que estaban opcionales y sin valor en la llamada, por sus valores por defecto para quitar la clausula Optional
                moDestinos.Add(DBNullToSomething(.Item("COD")), DBNullToSomething(.Item("DEN")), DBNullToSomething(.Item("SINTRANS")), DBNullToSomething(.Item("DIR")),
                    DBNullToSomething(.Item("POB")), DBNullToSomething(.Item("CP")), DBNullToSomething(.Item("PROVIDEN")), DBNullToSomething(.Item("PAIDEN")), Nothing,
                    Nothing, Nothing, Nothing, Nothing, Nothing, moAlmacenes, DBNullToBoolean(.Item("RECEPCION")), .Item("TFNO").ToString, .Item("FAX").ToString, .Item("Email").ToString)

            End With
        Next

        adoRS.Clear()
        adoRS = Nothing
    End Sub
    ''' <summary>
    ''' Carga todas las empresas con posibilidad o no de emitir pedidos para un c�digo pasado como par�metro.
    ''' </summary>
    ''' <returns>Un objeto de la clase CEmpresas con todas las empresas a las que tiene la posibilidad de emitir un pedido.</returns>
    ''' <remarks>
    ''' Llamada desde: NuevoFavorito.aspx
    ''' Tiempo m�ximo: 0 sec.
    ''' </remarks>
    Public Function CargarEmpresas(pargensms As PargensSMs, ByVal PedidosOtrasEmp As Boolean) As CEmpresas
        Authenticate()
        Dim tipoImputacion As EnumTipoImputacion
        tipoImputacion = EnumTipoImputacion.NoSeImputa
        If Not pargensms Is Nothing AndAlso Not pargensms(0) Is Nothing Then
            tipoImputacion = pargensms(0).ImputacionPedido
        Else
            tipoImputacion = EnumTipoImputacion.NoSeImputa
        End If

        Dim dsEmpresas As New DataSet
        dsEmpresas = DBServer.Persona_CargarEmpresas(msCod, tipoImputacion, mlEmpresa, PedidosOtrasEmp)
        moEmpresas = New CEmpresas(mDBServer, mIsAuthenticated)

        For Each fila As DataRow In dsEmpresas.Tables(0).Rows
            If mlEmpresa = 0 Then
                mlEmpresa = fila.Item("ID")
            End If
            moEmpresas.Add(fila.Item("ID"), fila.Item("NIF"), fila.Item("DEN"), fila.Item("DIR"), fila.Item("CP"), fila.Item("PAI"), fila.Item("POB"), fila.Item("PROVI"), SQLBinaryToBoolean(fila.Item("ERP")))
        Next

        Return moEmpresas

        dsEmpresas.Clear()
        dsEmpresas = Nothing
    End Function
    ''' <summary>
    ''' Carga todos los datos de "persona" del usuario que este utilizando la aplicacion
    ''' </summary>
    ''' <remarks>
    ''' Llamada desde: la clase NuevoFavorito.aspx
    ''' Tiempo m�ximo: 0 sec.
    ''' </remarks>
    Public Sub CargarPersona()
        Authenticate()
        Dim ador As New DataSet
        ador = mDBServer.Persona_CargarPersona(Cod)

        Cod = DBNullToStr(ador.Tables(0).Rows(0).Item("COD"))
        Nombre = DBNullToStr(ador.Tables(0).Rows(0).Item("NOM"))
        Apellidos = DBNullToStr(ador.Tables(0).Rows(0).Item("APE"))
        Email = DBNullToStr(ador.Tables(0).Rows(0).Item("EMAIL"))
        Fax = DBNullToStr(ador.Tables(0).Rows(0).Item("FAX"))
        Tfno = DBNullToStr(ador.Tables(0).Rows(0).Item("TFNO"))
        Tfno2 = DBNullToStr(ador.Tables(0).Rows(0).Item("TFNO2"))
        Cargo = DBNullToStr(ador.Tables(0).Rows(0).Item("CAR"))
        DepDen = DBNullToStr(ador.Tables(0).Rows(0).Item("DEP"))
        UON1DEN = DBNullToStr(ador.Tables(0).Rows(0).Item("UON1"))
        UON2DEN = DBNullToStr(ador.Tables(0).Rows(0).Item("UON2"))
        UON3DEN = DBNullToStr(ador.Tables(0).Rows(0).Item("UON3"))

        ador.Clear()
        ador = Nothing
    End Sub
End Class