﻿<Serializable()> _
Public Class cAlmacen
    Inherits Security
    Private CodDest As String
    Private Dest As CDestino
    Private cod As String
    Private Den As String
    Private intID As Integer

    Public Property Destino() As CDestino
        Get
            Return Dest
        End Get
        Set(ByVal value As CDestino)
            Dest = value
        End Set
    End Property

    Public Property Codigo() As String
        Get
            Return cod
        End Get
        Set(ByVal value As String)
            cod = value
        End Set
    End Property

    Public Property Denominacion() As String
        Get
            Return Den
        End Get
        Set(ByVal value As String)
            Den = value
        End Set
    End Property

    Public Property CodigoDestino() As String
        Get
            Return CodDest
        End Get
        Set(ByVal value As String)
            CodDest = value
        End Set
    End Property

    Public Property Id() As Integer
        Get
            Return intID
        End Get
        Set(ByVal value As Integer)
            intID = value
        End Set
    End Property

    ''' <summary>
    ''' Constructor de la clase cAlmacen
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si está autenticado</param>
    Public Sub New(ByRef dbserver As Fullstep.FSNDataBaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub
End Class
