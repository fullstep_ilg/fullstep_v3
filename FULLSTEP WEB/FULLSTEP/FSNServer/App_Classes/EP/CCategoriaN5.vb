
Public Class CCategoriaN5
    Inherits Security

    Private mlId As Integer
    Private msCod As String 'local copy
    Private msDen As String 'local copy
    Private mlCat1 As Integer
    Private mlCat2 As Integer
    Private mlCat3 As Integer
    Private mlCat4 As Integer
    Private mlCat1Cod As String
    Private mlCat2Cod As String
    Private mlCat3Cod As String
    Private mlCat4Cod As String
    Private miNumHojas As Short
    Private mvObs As Object
    Private moEspecificaciones As CEspecificaciones
    Private moCamposPers As CCampos
    Private moProvesPedLibre As CProveedores

    Public Property CamposPersonalizados() As CCampos
        Get
            CamposPersonalizados = moCamposPers
        End Get
        Set(ByVal Value As CCampos)
            moCamposPers = Value
        End Set
    End Property

    Public Property Id() As Integer
        Get
            Id = mlId
        End Get
        Set(ByVal Value As Integer)
            mlId = Value
        End Set
    End Property

    Public Property Cod() As String
        Get
            Cod = msCod
        End Get
        Set(ByVal Value As String)
            msCod = Value
        End Set
    End Property

    Public Property Den() As String
        Get
            Den = msDen
        End Get
        Set(ByVal Value As String)
            msDen = Value
        End Set
    End Property

    Public Property Cat1() As Integer
        Get
            Cat1 = mlCat1
        End Get
        Set(ByVal Value As Integer)
            mlCat1 = Value
        End Set
    End Property

    Public Property Cat2() As Integer
        Get
            Cat2 = mlCat2
        End Get
        Set(ByVal Value As Integer)
            mlCat2 = Value
        End Set
    End Property

    Public Property Cat3() As Integer
        Get
            Cat3 = mlCat3
        End Get
        Set(ByVal Value As Integer)
            mlCat3 = Value
        End Set
    End Property

    Public Property Cat4() As Integer
        Get
            Cat4 = mlCat4
        End Get
        Set(ByVal Value As Integer)
            mlCat4 = Value
        End Set
    End Property

    Public Property Cat1Cod() As String
        Get
            Cat1Cod = mlCat1Cod
        End Get
        Set(ByVal Value As String)
            mlCat1Cod = Value
        End Set
    End Property

    Public Property Cat2Cod() As String
        Get
            Cat2Cod = mlCat2Cod
        End Get
        Set(ByVal Value As String)
            mlCat2Cod = Value
        End Set
    End Property

    Public Property Cat3Cod() As String
        Get
            Cat3Cod = mlCat3Cod
        End Get
        Set(ByVal Value As String)
            mlCat3Cod = Value
        End Set
    End Property

    Public Property Cat4Cod() As String
        Get
            Cat4Cod = mlCat4Cod
        End Get
        Set(ByVal Value As String)
            mlCat4Cod = Value
        End Set
    End Property

    Public Property NumHojas() As Short
        Get
            NumHojas = miNumHojas
        End Get
        Set(ByVal Value As Short)
            miNumHojas = Value
        End Set
    End Property

    Public Property Obs() As Object
        Get
            Obs = mvObs
        End Get
        Set(ByVal Value As Object)
            mvObs = Value
        End Set
    End Property

    Public Property Especificaciones() As CEspecificaciones
        Get
            Especificaciones = moEspecificaciones
        End Get
        Set(ByVal Value As CEspecificaciones)
            moEspecificaciones = Value
        End Set
    End Property

    Public Property ProveedoresPedidoLibre() As CProveedores
        Get
            ProveedoresPedidoLibre = moProvesPedLibre
        End Get
        Set(ByVal Value As CProveedores)
            moProvesPedLibre = Value
        End Set
    End Property
    ''' <summary>
    ''' Constructor de la clase CCategoriaN5
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si est� autenticado</param>
    Public Sub New(ByRef dbserver As Fullstep.FSNDataBaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
    End Sub
End Class