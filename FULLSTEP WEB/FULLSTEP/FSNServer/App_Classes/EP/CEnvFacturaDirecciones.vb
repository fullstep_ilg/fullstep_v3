﻿Imports System.Data.SqlClient
Imports Fullstep.FSNLibrary

<Serializable()> _
Public Class CEnvFacturaDirecciones
    Inherits Lista(Of CEnvFacturaDireccion)

    ''' <summary>
    ''' Añade un nuevo elemento CEnvFacturaDireccion a la lista
    ''' </summary>
    ''' <param name="idEmpresa">Id de la empresa a la que corresponde la dirección</param>
    ''' <param name="ID">ID</param>
    ''' <param name="Direccion">Dirección</param>
    ''' <param name="CP">Código Postal</param>
    ''' <param name="Pob">Población</param>
    ''' <param name="Provi">Provincia</param>
    ''' <param name="Pai">País</param>
    ''' <remarks></remarks>
    Public Overloads Sub Add(ByVal idEmpresa As Integer, ByVal ID As Integer, ByVal Direccion As String, ByVal CP As String, ByVal Pob As String, ByVal Provi As String, ByVal Pai As Object) ' As CEnvFacturaDireccion
        'create a new object
        Dim objnewmember As CEnvFacturaDireccion
        objnewmember = New CEnvFacturaDireccion(mDBServer, mIsAuthenticated)
        With objnewmember
            .IdEmpresa = idEmpresa
            .ID = ID
            .Direccion = Direccion
            .Poblacion = Pob
            .CodigoPostal = CP
            .Provincia = Provi
            .Pais = Pai
        End With

        Me.Add(objnewmember)
        'Return objnewmember

    End Sub

    ''' <summary>
    ''' Permite recuperar un objeto concreto de la lista de Direcciones de envío de factura
    ''' </summary>
    ''' <param name="ID">ID de la dirección buscada</param>
    ''' <returns>El objeto CEnvFacturaDireccion presente en la lista cEnvFacturaDirecciones que coincida con el ID pasado</returns>
    ''' <remarks></remarks>
    Public Overloads ReadOnly Property Item(ByVal ID As Integer) As CEnvFacturaDireccion
        Get
            Return Me.Find(Function(elemento As CEnvFacturaDireccion) elemento.ID = ID)
        End Get
    End Property

    ''' <summary>
    ''' Constructor de la clase CEnvFacturaDirecciones
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si está autenticado</param>
    ''' <remarks>
    ''' Llamada desde: Todos los sitios donde se instancien objetos de esta clase
    ''' Tiempo máximo: 0 sec. </remarks>
    Public Sub New(ByRef dbserver As Fullstep.FSNDataBaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
    End Sub
    ''' <summary>
    ''' Devuelve todas las direcciones de envío de factura para todas las empresas.
    ''' </summary>
    ''' <returns>Un DataSet con todas las direcciones de todas las empresas</returns>
    ''' <remarks>
    ''' Llamada desde: método CargarDireccionesDeEnvioDeFactura</remarks>
    Public Function DevolverDireccionesEnvioFacturas() As DataSet
        Dim ador As New DataSet
        ador = mDBServer.DireccionesEnvioFacturas_Devolver()
        If ador.Tables.Count = 0 Then
            ador.Clear()
            ador = Nothing
            Return Nothing
            Exit Function
        Else
            Return ador
            ador.Clear()
        End If
    End Function
    ''' <summary>
    ''' Carga en la lista de esta clase, todas las direcciones de envio de facturas de todas las empresas
    ''' </summary>
    ''' <remarks>Llamada desde los lugares de la aplicación que requieran su uso</remarks>
    Public Sub CargarDireccionesDeEnvioDeFactura()
        Dim ador As DataSet

        ador = DevolverDireccionesEnvioFacturas()
        Me.Clear()
        For Each fila As DataRow In ador.Tables(0).Rows
            Dim IdEmpresa As Integer = DBNullToInteger(fila.Item("ID_EMPRESA"))
            Dim ID As Integer = DBNullToInteger(fila.Item("ID_DIRECCION"))
            Dim Direccion As String = DBNullToStr(fila.Item("DIR"))
            Dim CP As String = DBNullToStr(fila.Item("CP"))
            Dim Poblacion As String = DBNullToStr(fila.Item("POB"))
            Dim Provincia As String = DBNullToStr(fila.Item("PROVI"))
            Dim Pais As String = DBNullToStr(fila.Item("PAI"))
            Me.Add(IdEmpresa, ID, Direccion, CP, Poblacion, Provincia, Pais)
        Next
        ador.Clear()
        ador = Nothing
    End Sub
End Class
