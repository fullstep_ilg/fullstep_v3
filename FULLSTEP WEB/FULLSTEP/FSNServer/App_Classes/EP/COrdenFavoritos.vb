Imports Fullstep.FSNLibrary

<Serializable()> _
Public Class COrdenFavoritos
    Inherits Security

#Region "Properties"
    Private mlId As Integer
    Private msUsu As String
    Private msDenominacion As String
    Private mlAnyo As Integer
    Private msCodProve As String
    Private mvFecha As Object
    Private mdImporte As Double
    Private msReferencia As Object
    Private msObs As Object
    Private mvMoneda As Object
    Private mvCambio As Object
    Private moLineas As CLineasFavoritos
    Private moAdjuntos As CAdjuntos
    Private mvReceptor As Object
    Private mvEmpresa As Object
    Private mvCodErp As Object
    Private miDireccionEnvioFactura As Integer
    Private oTipoPedido As cTipoPedido
    Private msOrgCompras As String

    Public Property ID() As Integer
        Get
            ID = mlId
        End Get
        Set(ByVal Value As Integer)
            mlId = Value
        End Set
    End Property
    Public Property Usuario() As String
        Get
            Usuario = msUsu
        End Get
        Set(ByVal Value As String)
            msUsu = Value
        End Set
    End Property
    Public Property Anyo() As Integer
        Get
            Anyo = mlAnyo
        End Get
        Set(ByVal Value As Integer)
            mlAnyo = Value
        End Set
    End Property
    Public Property Prove() As String
        Get
            Prove = msCodProve
        End Get
        Set(ByVal Value As String)
            msCodProve = Value
        End Set
    End Property
    Public Property Fecha() As Object
        Get
            Fecha = mvFecha
        End Get
        Set(ByVal Value As Object)
            mvFecha = Value
        End Set
    End Property
    Public Property Importe() As Double
        Get
            Importe = mdImporte
        End Get
        Set(ByVal Value As Double)
            mdImporte = Value
        End Set
    End Property
    ''' <summary>
    ''' NUM_PED_ERP
    ''' </summary>
    Public Property Referencia() As Object
        Get
            Referencia = msReferencia
        End Get
        Set(ByVal Value As Object)
            msReferencia = Value
        End Set
    End Property
    Public Property Obs() As Object
        Get
            Obs = msObs
        End Get
        Set(ByVal Value As Object)
            msObs = Value
        End Set
    End Property
    Public Property Adjuntos() As CAdjuntos
        Get
            Adjuntos = moAdjuntos
        End Get
        Set(ByVal Value As CAdjuntos)
            moAdjuntos = Value
        End Set
    End Property
    Public Property Denominacion() As Object
        Get
            Denominacion = msDenominacion
        End Get
        Set(ByVal Value As Object)
            msDenominacion = Value
        End Set
    End Property
    Public Property Lineas() As CLineasFavoritos
        Get
            Lineas = moLineas
        End Get
        Set(ByVal Value As CLineasFavoritos)
            moLineas = Value
        End Set
    End Property
    Public Property Moneda() As Object
        Get
            Moneda = mvMoneda
        End Get
        Set(ByVal Value As Object)
            mvMoneda = Value
        End Set
    End Property
    Public Property Cambio() As Object
        Get
            Cambio = mvCambio
        End Get
        Set(ByVal Value As Object)
            mvCambio = Value
        End Set
    End Property
    Public Property CodErp() As Object
        Get
            CodErp = mvCodErp
        End Get
        Set(ByVal Value As Object)
            mvCodErp = Value
        End Set
    End Property
    Public Property Empresa() As Object
        Get
            Empresa = mvEmpresa
        End Get
        Set(ByVal Value As Object)
            mvEmpresa = Value
        End Set
    End Property
    Public Property Receptor() As Object
        Get
            Receptor = mvReceptor
        End Get
        Set(ByVal Value As Object)
            mvReceptor = Value
        End Set
    End Property
    Public Property DireccionEnvioFactura() As Integer
        Get
            Return miDireccionEnvioFactura
        End Get
        Set(ByVal value As Integer)
            miDireccionEnvioFactura = value
        End Set
    End Property
    Public Property TipoPedido() As cTipoPedido
        Get
            Return oTipoPedido
        End Get
        Set(ByVal value As cTipoPedido)
            oTipoPedido = value
        End Set
    End Property
    Public Property OrgCompras() As String
        Get
            OrgCompras = msOrgCompras
        End Get
        Set(ByVal Value As String)
            msOrgCompras = Value
        End Set
    End Property
#End Region
    ''' <summary>
    ''' Recupera las lineas de pedido para la orden de favoritos pasada como par�metro
    ''' </summary>
    ''' <param name="sIdioma">Idioma utilizado para la denominaci�n de la moneda.</param>
    ''' <returns>Un dataset con los art�culos que componen la l�nea de pedido pasada como par�metro</returns>
    Public Function DevolverLineas(ByVal sIdioma As Object, Optional ByVal CargarEnColeccion As Boolean = False) As DataSet
        Dim ador As New DataSet
        ador = mDBServer.OrdenFavoritos_DevolverLineasEP(sIdioma, mlId)
        ador.Tables(0).TableName = "CESTAFAV"
        Dim keycab() As DataColumn = {ador.Tables("CESTAFAV").Columns("LINEAID")} '{ds.Tables("CABECERA_CESTA").Columns("PROVE"), ds.Tables("CABECERA_CESTA").Columns("MON")} '{ds.Tables("CABECERA_CESTA").Columns("ID")}
        ador.Tables("CESTAFAV").PrimaryKey = keycab
        If ador.Tables.Count = 0 Then
            ador.Clear()
            ador = Nothing
            Return Nothing
            ador.Dispose()
            Exit Function
        Else
            DevolverLineas = ador
            If CargarEnColeccion Then
                For Each fila As DataRow In ador.Tables(0).Rows
                    If Me.Lineas Is Nothing Then
                        Me.Lineas = New CLineasFavoritos(mDBServer, mIsAuthenticated)
                    End If
                    Dim oCampo1 As New CCampo(mDBServer, mIsAuthenticated)
                    Dim oCampo2 As New CCampo(mDBServer, mIsAuthenticated)
                    oCampo1.Den = DBNullToStr(fila.Item("DenCampo1"))
                    oCampo1.ID = DBNullToDec(fila.Item("Campo1"))
                    oCampo1.Obligatorio = DBNullToDec(fila.Item("Obl1"))
                    oCampo1.Valor = DBNullToStr(fila.Item("Valor1"))
                    oCampo2.Den = DBNullToStr(fila.Item("DenCampo2"))
                    oCampo2.ID = DBNullToDec(fila.Item("Campo2"))
                    oCampo2.Obligatorio = DBNullToDec(fila.Item("Obl2"))
                    oCampo2.Valor = DBNullToStr(fila.Item("Valor2"))
                    Dim Esta As Boolean = False
                    If Not Me.Lineas Is Nothing Then
                        For Each linea As CLineaFavoritos In Me.Lineas
                            If linea.ID = fila.Item("LINEAID") Then
                                Esta = True
                            End If
                        Next
                    End If
                    If Not Esta Then
                        Me.Lineas.Add(DBNullToDec(fila.Item("LineaID")), DBNullToStr(fila.Item("usu")), DBNullToStr(fila.Item("CodArt")), DBNullToStr(fila.Item("GMN1")), DBNullToStr(fila.Item("GMN2")), DBNullToStr(fila.Item("GMN3")), DBNullToStr(fila.Item("GMN4")), DBNullToStr(fila.Item("Prove")), DBNullToStr(fila.Item("DenArt")), DBNullToStr(fila.Item("Art_ext")), DBNullToStr(fila.Item("Destino")), DBNullToStr(fila.Item("DestDen")), DBNullToStr(fila.Item("Unidad")), DBNullToStr(fila.Item("UniDen")), DBNullToDbl(fila.Item("PrecioUnitario")), DBNullToDbl(fila.Item("Cantidad")), DBNullToDbl(fila.Item("CantAdjudicada")), DBNullToDbl(fila.Item("FC")), DBNullToDbl(fila.Item("Importe")), Nothing, Nothing, DBNullToDec(fila.Item("Cat1")), DBNullToStr(fila.Item("Cat1Den")), DBNullToDec(fila.Item("Cat2")), DBNullToStr(fila.Item("Cat2Den")), DBNullToDec(fila.Item("Cat3")), DBNullToStr(fila.Item("Cat3Den")), DBNullToDec(fila.Item("Cat4")), DBNullToStr(fila.Item("Cat4Den")), DBNullToDec(fila.Item("Cat5")), DBNullToStr(fila.Item("Cat5Den")), DBNullToStr(fila.Item("Obs")), DBNullToStr(fila.Item("Categoria")), DBNullToStr(fila.Item("CategoriaRama")), "", DBNullToStr(fila.Item("AnyoProce")), DBNullToDec(fila.Item("CodProce")), DBNullToStr(fila.Item("Item")), DBNullToDec(fila.Item("Linea")), Nothing, oCampo1, oCampo2, DBNullToStr(fila.Item("ObsAdjun")), "", DBNullToDbl(fila.Item("Cantidad")), (fila.Item("MODIF_PREC")), 0, DBNullToStr(fila.Item("CENTROS")))
                    End If
                Next
            End If
            Return ador
        End If
        ador.Dispose()
    End Function
    ''' <summary>
    ''' Recupera las lineas de pedido para la orden de favoritos pasada como par�metro
    ''' </summary>
    ''' <returns>Un dataset con los art�culos que componen la l�nea de pedido pasada como par�metro</returns>
    Public Function CrearCestaDesdeFavorito(ByVal idFila As Integer) As Integer
        Dim ador As New DataSet
        ador = mDBServer.CrearCestaDesdeFavorito(idFila)
        ador.Tables(0).TableName = "CESTAFAV"
        Dim keycab() As DataColumn = {ador.Tables("CESTAFAV").Columns("IDLINEA")} '{ds.Tables("CABECERA_CESTA").Columns("PROVE"), ds.Tables("CABECERA_CESTA").Columns("MON")} '{ds.Tables("CABECERA_CESTA").Columns("ID")}
        ador.Tables("CESTAFAV").PrimaryKey = keycab
        Return ador.Tables(0).Rows(0).Item(0)
    End Function
    ''' <summary>
    ''' Funci�n que carga los adjuntos de un pedido favorito
    ''' </summary>
    ''' <param name="bCargarEnColeccion">Variable booleana, que indica si se quiere cargar tambien en la clase</param>
    ''' <returns>Un objeto de la clase CAdjuntos con todos los adjuntos del pedido</returns>
    ''' <remarks>
    ''' Llamada desde: Pagina AnyadirAFavorito y NuevoFavorito
    ''' Tiempo m�ximo: 0,5 seg.
    ''' </remarks>
    Public Function CargarAdjuntos(ByVal bCargarEnColeccion As Boolean) As CAdjuntos
        Dim ador As New DataSet
        Dim oAdjuntos As CAdjuntos
        ador = mDBServer.OrdenFavoritos_CargarAdjuntos(mlId, msUsu)
        oAdjuntos = New CAdjuntos(mDBServer, mIsAuthenticated)
        For Each fila As DataRow In ador.Tables(0).Rows
            'He modificado la llamada al metodo, poniendo los parametros que estaban vacios por sus valores por defecto, para quitar la clausula "Optional"
            oAdjuntos.Add(fila.Item("ID"), fila.Item("FECACT"), fila.Item("NOM"), fila.Item("TAMANO"), fila.Item("COM"), msUsu, Nothing, "", Nothing, TiposDeDatos.Adjunto.Tipo.Favorito_Orden_Entrega)
        Next
        ador.Clear()
        ador = Nothing
        If bCargarEnColeccion Then
            moAdjuntos = oAdjuntos
        End If
        CargarAdjuntos = oAdjuntos
        Return oAdjuntos
        oAdjuntos = Nothing
        ador.Dispose()
    End Function
    ''' <summary>
    ''' Funci�n que elimina una orden de pedido favorito,sus adjuntos, as� como todas sus l�neas y los adjuntos de estas.
    ''' </summary>
    ''' <returns>Un objeto de la clase CTESError</returns>
    ''' <remarks>
    ''' Llamada desde: La p�gina Favoritos.aspx, m�todo dlOrdenes_ItemCommand
    ''' Tiempo m�ximo: 0,5 seg aproximadamente</remarks>
    Public Function EliminarFavorito() As CTESError
        Dim oTESError As New CTESError
        oTESError.NumError = mDBServer.OrdenFavoritos_EliminarFavorito(mlId, msUsu)
        Return oTESError
    End Function
    ''' <summary>
    ''' A�ade las l�neas de pedido a un pedido favorito existente
    ''' </summary>
    ''' <returns>Un Error de la clase CTESErrores que devuelve 1 si ha habido error y 0 si todo ha ido bien</returns>
    Public Function AnyadirLineas() As Integer()
        Dim Resul(1) As Integer
        For Each LIN As CLineaFavoritos In Me.Lineas
            With LIN
                If .ID = 0 Then 'ES NUEVA
                    Dim Campo1ID As Integer
                    Dim Campo1Val As String = String.Empty
                    Dim Campo2ID As Integer
                    Dim Campo2Val As String = String.Empty
                    If .Campo1 IsNot Nothing Then
                        Campo1ID = .Campo1.ID
                        Campo1Val = .Campo1.Valor
                    End If
                    If .Campo2 IsNot Nothing Then
                        Campo2ID = .Campo2.ID
                        Campo2Val = .Campo2.Valor
                    End If
                    Dim FechaEntrega As Object = Nothing
                    If .FecEntrega IsNot Nothing Then
                        FechaEntrega = .FecEntrega
                    End If
                    Dim EntregaObl As Object = Nothing
                    If .EntregaObl IsNot Nothing Then
                        EntregaObl = .EntregaObl
                    End If
                    Dim centroSM As String = ""
                    Dim partida As Integer
                    If .Pres5_ImportesImputados IsNot Nothing AndAlso .Pres5_ImportesImputados.Count > 0 AndAlso .Pres5_ImportesImputados(0).CentroCoste IsNot Nothing Then
                        If .Pres5_ImportesImputados(0).CentroCoste.CentroSM IsNot Nothing Then
                            centroSM = .Pres5_ImportesImputados(0).CentroCoste.CentroSM.Codigo
                        End If
                        If .Pres5_ImportesImputados(0).Partida IsNot Nothing Then
                            partida = .Pres5_ImportesImputados(0).Partida.PresupuestoId
                        End If
                    End If

                    Resul = mDBServer.OrdenFavoritos_AnyadirLinea(.LineaCatalogo, .PrecUc, ID, .Dest, .Usuario, .UP, .PrecUc * .CantPed, .CantPed, .LineaCatalogo, DBNullToStr(.Obs), .FC, .Den, _
                                                                  Campo1ID, Campo1Val, Campo2ID, Campo2Val, .ObsAdjun, FechaEntrega, EntregaObl, .IdAlmacen, .Activo, centroSM, partida)
                    If Resul(0) = 0 Then
                        .ID = Resul(1)
                    Else
                        Return Resul
                        Exit Function
                    End If
                    If Not .Adjuntos Is Nothing Then
                        For Each oAdj As Adjunto In .Adjuntos
                            If oAdj.tipo = TiposDeDatos.Adjunto.Tipo.EspecificacionLineaCatalogo Then
                                DBServer.OrdenFavoritos_AnyadirAdjuntoEspecificacionesALinea(.ID, .Usuario, .LineaCatalogo, oAdj.IdEsp)
                            Else
                                If oAdj.Operacion = "I" Then
                                    If oAdj.Id IsNot Nothing AndAlso oAdj.Id > 0 Then
                                        oAdj.CopiarAdjunto(oAdj, TiposDeDatos.Adjunto.Tipo.Favorito_Linea_Pedido, oAdj.IdEsp)
                                    Else
                                        oAdj.GrabarAdjunto(oAdj.FileName, oAdj.DirectorioGuardar, TiposDeDatos.Adjunto.Tipo.Favorito_Linea_Pedido, oAdj.IdEsp, oAdj.Comentario, oAdj.dataSize)
                                    End If
                                Else
                                    DBServer.OrdenFavoritos_AnyadirAdjuntoLinea(.ID, .Usuario, DBNullToStr(oAdj.Comentario), oAdj.Nombre, oAdj.Id)
                                End If
                            End If
                        Next
                    End If
                End If
            End With
        Next
        Return Resul
    End Function
    ''' <summary>
    ''' Constructor de la clase COrdenFavoritos
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si est� autenticado</param>
    ''' <remarks>
    ''' Llamada desde: Todos los sitios en los que se instancie un objeto de la clase COrden
    ''' Tiempo m�ximo: 0 sec
    ''' </remarks>
    Public Sub New(ByRef dbserver As Fullstep.FSNDataBaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
End Class