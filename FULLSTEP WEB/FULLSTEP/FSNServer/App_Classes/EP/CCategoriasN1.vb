
<Serializable()> _
Public Class CCategoriasN1
    Inherits Lista(Of CCategoriaN1)

    ''' <summary>
    ''' Agrega un elemento CCategoriaN1 a la lista
    ''' </summary>
    ''' <param name="Id">ID de la categor�a</param>
    ''' <param name="Cod">C�digo de la categor�a</param>
    ''' <param name="Den">Denominaci�n</param>
    ''' <param name="iNumHojas">N�mero de categ�r�as hijas</param>
    ''' <param name="vObs">Observaciones</param>
    ''' <returns>El elemento CCategoriaN1 agregado</returns>
    Public Overloads Function Add(ByVal Id As Integer, ByVal Cod As String, ByVal Den As String, ByVal iNumHojas As Short, ByVal vObs As Object) As CCategoriaN1
        Dim objnewmember As New CCategoriaN1(mDBServer, mIsAuthenticated)

        With objnewmember
            .Id = Id
            .Cod = Cod
            .Den = Den
            .NumHojas = iNumHojas
            .Obs = vObs
        End With

        Me.Add(objnewmember)
        Return objnewmember
    End Function
    ''' <summary>
    ''' Procedimiento que carga todas las categorias de una usuario dado su codigo de persona para un idioma tambien pasado como par�metro.
    ''' </summary>
    ''' <param name="Idioma">Idioma del usuario</param>
    ''' <param name="Per">C�digo de la persona para cargar todas las categorias de la misma.</param>
    ''' <remarks></remarks>
    Public Sub CargarTodasLasCategorias(ByVal Per As String, ByVal Idioma As String)
        Authenticate()
        Dim ador As New DataSet
        ador = mDBServer.CategoriasN1_CargarTodasLasCategorias(Per, Idioma)

        Me.Clear()
        If ador.Tables.Count > 0 Then
            For Each fila As DataRow In ador.Tables(0).Rows
                Dim oCat As New CCategoriaN1(mDBServer, mIsAuthenticated)
                oCat.Id = fila.Item("CAT1")
                oCat.Cod = fila.Item("COD")
                oCat.Den = fila.Item("DEN")
                oCat.Obs = fila.Item("OBS")
                If Not IsDBNull(fila.Item("CAMPO1")) Or Not IsDBNull(fila.Item("CAMPO2")) Then
                    oCat.CamposPersonalizados = New CCampos(mDBServer, mIsAuthenticated)
                    If Not IsDBNull(fila.Item("CAMPO1")) Then
                        oCat.CamposPersonalizados.Add(fila.Item("CAMPO1"), Idioma, 1, fila.Item("DENCAMPO1"), False, Nothing)
                    End If
                    If Not IsDBNull(fila.Item("CAMPO2")) Then
                        oCat.CamposPersonalizados.Add(fila.Item("CAMPO2"), Idioma, 2, fila.Item("DENCAMPO2"), False, Nothing)
                    End If
                End If
                Me.Add(oCat)
            Next
        End If
    End Sub
    ''' <summary>
    ''' Constructor de la clase CCategoriasN1
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si est� autenticado</param>
    Public Sub New(ByRef dbserver As Fullstep.FSNDataBaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
    End Sub
End Class