Imports Fullstep.FSNLibrary
Imports System.Data.SqlClient

<Serializable()> _
Public Class CPedido
    Inherits Security

    <Flags()>
    Public Enum Estado As Short
        SinEstado = 0
        PendienteAprobar = 1
        DenegadoParcialmente = 2
        EmitidoAlProveedor = 4
        AceptadoPorElProveedor = 8
        EnCamino = 16
        RecibidoParcialmente = 32
        Cerrado = 64
        Anulado = 128
        Rechazado = 256
        Denegado = 512
    End Enum

    Public Enum OtrosAtributosPedido As Integer
        Abono = 1024
        CatalogadoNegociado = 2048 'EP no libre
        Negociado = 4096 'GS
        Directos = 8192 'ERP
        CatalogadoLibres = 16384 'EP LIBRE
        ContraAbiertos = 32768
        Abiertos = 65536
        Express = 131072
    End Enum
    Public Enum TipoOrigenPedido As Short
        Negociado = 0 'GS
        CatalogadoNegociado = 1 'EP no libre
        Directos = 2 'ERP
        CatalogadoLibres = 3 'EP LIBRE
        Express = 4
        Abierto = 5
        ContraAbierto = 6
    End Enum

    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
End Class