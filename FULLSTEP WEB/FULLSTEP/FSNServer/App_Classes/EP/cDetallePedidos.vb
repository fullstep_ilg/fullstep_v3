﻿<Serializable()> _
Public Class cDetallePedidos
    Inherits Security

    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub

    ''' <summary>
    ''' Función que devuelve los adjuntos de la cabecera de la orden de entrega
    ''' </summary>
    ''' <param name="UsuCod">codigo del usuario</param>
    ''' <param name="iId">id de la orden de la que obtener los adjuntos</param>
    ''' <returns>Lista de cAdjunto</returns>
    ''' <remarks></remarks>
    Public Function Cargar_Adjuntos_Cabecera_Orden(ByVal UsuCod As String, ByVal iId As Integer) As CAdjuntos
        Authenticate()

        Dim adoRes As New DataSet
        Dim oAdjuntos As CAdjuntos

        oAdjuntos = New CAdjuntos(mDBServer, mIsAuthenticated)

        adoRes = mDBServer.Cargar_Adjuntos_Cabecera_Orden(iId)

        For Each fila As DataRow In adoRes.Tables(0).Rows
            oAdjuntos.Add(fila.Item("ID"), fila.Item("FECACT"), fila.Item("NOM"), fila.Item("TAMANO"), fila.Item("COM"), Nothing, Nothing, "", Nothing, TiposDeDatos.Adjunto.Tipo.Orden_Entrega)
        Next
        adoRes.Clear()
        adoRes = Nothing

        Return (oAdjuntos)
    End Function


    ''' <summary>
    ''' Función que devuelve los adjuntos de la linea de la orden de entrega
    ''' </summary>
    ''' <param name="UsuCod">codigo del usuario</param>
    ''' <param name="iIdLinea">id de la linea de la que obtener los adjuntos</param>
    ''' <returns>Lista de cAdjunto</returns>
    ''' <remarks></remarks>
    Public Function Cargar_Adjuntos_Linea_Orden(ByVal UsuCod As String, ByVal iIdLinea As Integer) As CAdjuntos
        Authenticate()

        Dim adoRes As New DataSet
        Dim oAdjuntos As CAdjuntos

        oAdjuntos = New CAdjuntos(mDBServer, mIsAuthenticated)


        adoRes = mDBServer.Cargar_Adjuntos_Linea_Orden(iIdLinea)

        For Each fila As DataRow In adoRes.Tables(0).Rows
            oAdjuntos.Add(fila.Item("ID"), fila.Item("FECACT"), fila.Item("NOM"), fila.Item("TAMANO"), fila.Item("COM"), Nothing, Nothing, "", Nothing, TiposDeDatos.Adjunto.Tipo.Linea_Pedido)
        Next
        adoRes.Clear()
        adoRes = Nothing

        Return (oAdjuntos)
    End Function

    ''' <summary>
    ''' Función que devuelve los datos de DatosGenerales de la orden
    ''' </summary>
    ''' <param name="UsuCod">cod de usuario</param>
    ''' <param name="iId">id de la orden de entrega</param>
    ''' <param name="sIdioma">idioma de la aplicacion</param>
    ''' <returns>objecto de la clase cDatosGenerales con los datos generales de la orden</returns>
    ''' <remarks></remarks>
    Public Function Cargar_Datos_Generales_Orden(ByVal UsuCod As String, ByVal iId As Integer, ByVal sIdioma As String) As Object
        Authenticate()
        Dim DsResul As New DataSet
        Dim datosGenerales As CDatosGenerales
        datosGenerales = New CDatosGenerales
        DsResul = DBServer.Cargar_Datos_Generales_Orden(iId, sIdioma)
        Dim IdPedido As Integer
        'rellenar info con consulta
        If Not DsResul.Tables(0).Rows.Count = 0 Then
            Dim drDatosGenerales As DataRow = DsResul.Tables(0).Rows(0)
            With datosGenerales
                .Id = iId
                .Anyo = DBNullToInteger(drDatosGenerales.Item("ANYO"))
                .NumOrden = DBNullToInteger(drDatosGenerales.Item("NUMORDEN"))
                IdPedido = DBNullToInteger(drDatosGenerales.Item("IDPEDIDO"))
                .NumPedido = DBNullToInteger(drDatosGenerales.Item("NUMPEDIDO"))
                .CodProve = DBNullToStr(drDatosGenerales.Item("PROVE"))
                .DenProve = DBNullToStr(drDatosGenerales.Item("PROVEDEN"))
                .IdEmpresa = DBNullToStr(drDatosGenerales.Item("EMPRESA"))
                .Emp = DBNullToStr(drDatosGenerales.Item("EMP"))
                .CodReceptor = DBNullToStr(drDatosGenerales.Item("CODRECEPTOR"))
                .Receptor = DBNullToStr(drDatosGenerales.Item("RECEPTOR"))
                .CodErp = DBNullToStr(drDatosGenerales.Item("DEN_PROVE_ERP"))
                .Cambio = drDatosGenerales.Item("EQUIV")
                If Not IsDBNull(drDatosGenerales.Item("TIPOPEDIDO")) Then
                    .Tipo = New cTipoPedido(mDBServer, mIsAuthenticated)
                    .Tipo.Id = drDatosGenerales.Item("TIPOPEDIDO")
                    .Tipo.Denominacion = drDatosGenerales.Item("TIPOPEDIDO_DEN")
                    .Tipo.Recepcionable = drDatosGenerales.Item("RECEPCIONAR")
                End If
                .DirEnvioDen = DBNullToStr(drDatosGenerales.Item("FAC_DIR_ENVIO"))
                .Obs = DBNullToStr(drDatosGenerales.Item("OBS"))
                .Moneda = DBNullToStr(drDatosGenerales.Item("MON"))
                .Gestor = DBNullToStr(drDatosGenerales.Item("GESTOR"))
                .CodGestor = DBNullToStr(drDatosGenerales.Item("CODGESTOR"))
                .Referencia = DBNullToStr(drDatosGenerales.Item("NUM_PED_ERP"))
                .CodigoErp = DBNullToStr(drDatosGenerales.Item("COD_PROVE_ERP"))
                .EstadoOrden = drDatosGenerales.Item("EST")
                .Estado = drDatosGenerales.Item("ORDEN_EST")
                .FechaEstadoOrden = drDatosGenerales.Item("FECEST")
                .OrdenPedAbierto = If(IsDBNull(drDatosGenerales.Item("ORDEN_PED_ABIERTO")), Nothing, drDatosGenerales.Item("ORDEN_PED_ABIERTO"))
                .AnyoPedidoAbierto = drDatosGenerales.Item("ANYO_PED_ABIERTO")
                .CestaPedidoAbierto = drDatosGenerales.Item("CESTA_PED_ABIERTO")
                .PedidoPedidoAbierto = drDatosGenerales.Item("PEDIDO_PED_ABIERTO")
                .TipoPedido = drDatosGenerales.Item("TIPO")
                .FechaIniPedAbierto = IIf(IsDBNull(drDatosGenerales.Item("PED_ABIERTO_FECINI")), Nothing, drDatosGenerales.Item("PED_ABIERTO_FECINI"))
                .FechaFinPedAbierto = IIf(IsDBNull(drDatosGenerales.Item("PED_ABIERTO_FECFIN")), Nothing, drDatosGenerales.Item("PED_ABIERTO_FECFIN"))
                .Aprovisionador = drDatosGenerales.Item("APROV")
                .Fecha = drDatosGenerales.Item("FECHA")
                .OESolicitAceptacionProveedor = SQLBinaryToBoolean(drDatosGenerales.Item("ACEP_PROVE"))
                .Abono = SQLBinaryToBoolean(drDatosGenerales.Item("ABONO"))
                .BajaLogica = SQLBinaryToBoolean(drDatosGenerales.Item("BAJA_LOG"))
                .FechaBajaLogica = IIf(IsDBNull(drDatosGenerales.Item("FEC_BAJA_LOG")), Nothing, drDatosGenerales.Item("FEC_BAJA_LOG"))
                .EstadoIntegracionOrden = DBNullToInteger(drDatosGenerales("ORDEN_EST_INT"))
                .IdLogGral = DBNullToInteger(drDatosGenerales("ID_LOG_GRAL"))
                .IdEntidad = DBNullToInteger(drDatosGenerales("ID_ENTIDAD"))
                .IdErp = DBNullToInteger(drDatosGenerales("ID_ERP"))
            End With
        End If
        Return {datosGenerales, IdPedido}
    End Function

    ''' <summary>
    ''' Función que devuelve los atributos de la orden de entrega
    ''' </summary>
    ''' <returns>Coleccion de atributos de la orden</returns>
    ''' <remarks></remarks>
    Public Function Cargar_Atributos(ByVal iIdOrden As Integer) As CAtributosPedido
        Authenticate()

        Dim adoRes As New DataSet
        Dim oAtributos As CAtributosPedido
        oAtributos = New CAtributosPedido(mDBServer, mIsAuthenticated)
        oAtributos = oAtributos.CargarAtributosOrden(iIdOrden)

        Return (oAtributos)
    End Function

    ''' <summary>
    ''' Función que devuelve los costes de de cabecera de la orden de entrega
    ''' </summary>
    ''' <returns>Devuelve una lista con los costes</returns>
    ''' <remarks>
    ''' Llamada desde: La página Report_XML
    ''' Tiempo máximo: 1 seg</remarks>
    Public Function Cargar_Costes(ByVal idOrden As Long, Optional ByVal sIdioma As String = "SPA") As CAtributosPedido
        Authenticate()

        Dim adoRes As New DataSet
        Dim oAtributos As CAtributosPedido
        oAtributos = New CAtributosPedido(mDBServer, mIsAuthenticated)
        oAtributos = oAtributos.CargarAtributosCosteOrden(idOrden, sIdioma)

        Return (oAtributos)
    End Function

    ''' <summary>
    ''' Función que devuelve los descuentos de cabecera de la orden de entrega
    ''' </summary>
    ''' <returns>Devuelve una lista con los descuentos</returns>
    ''' <remarks></remarks>
    Public Function Cargar_Descuentos(ByVal IdOrden As Long) As CAtributosPedido
        Authenticate()

        Dim oAtributos As CAtributosPedido
        oAtributos = New CAtributosPedido(mDBServer, mIsAuthenticated)
        oAtributos = oAtributos.CargarAtributosDescuentoOrden(IdOrden)

        Return (oAtributos)
    End Function

    ''' <summary>
    ''' Función que devuelve los datos de las lineas de pedido
    ''' </summary>
    ''' <param name="UsuCod">codigo del usuario</param>
    ''' <param name="Idioma">idioma de la aplicacion</param>
    ''' <param name="iId">id de la orden de entrega</param>
    ''' <returns>Una coleccion de lineas de pedido</returns>
    ''' <remarks></remarks>
    Public Function Cargar_Lineas(ByVal UsuCod As String, ByVal Idioma As FSNLibrary.Idioma, ByVal iId As Integer, ByVal instanciaAprob As Long, ByVal PresDen() As String, ByVal bMostrarLineasBajaLog As Boolean,
                                  Optional ByVal bDesdeAprob As Boolean = False) As CLineasPedido
        Authenticate()

        Dim DsResul As New DataSet
        Dim oLineas As CLineasPedido
        DsResul = DBServer.Ordenes_CargarLineasPedido(UsuCod, Idioma, iId, instanciaAprob, bMostrarLineasBajaLog, bDesdeAprob)

        Dim keypedido() As DataColumn = {DsResul.Tables(0).Columns("IDLINEA")}
        DsResul.Tables(0).PrimaryKey = keypedido
        Dim keyrelim() As DataColumn = {DsResul.Tables(2).Columns("IDLINEA")}
        DsResul.Relations.Add("REL_PEDIDO_IMPUTACIONES", keypedido, keyrelim, False)
        Dim keyrelimpu() As DataColumn = {DsResul.Tables(3).Columns("IDLINEA")}
        DsResul.Relations.Add("REL_IMPUESTOS", keypedido, keyrelimpu, False)
        Dim keyPlanEntrega() As DataColumn = {DsResul.Tables(5).Columns("LINEA_PEDIDO")}
        DsResul.Relations.Add("REL_PLAN_ENTREGA", keypedido, keyPlanEntrega, False)
        'rellenar info con consulta
        oLineas = New CLineasPedido(mDBServer, mIsAuthenticated)
        If Not DsResul.Tables(0).Rows.Count = 0 Then
            For Each linea As DataRow In DsResul.Tables(0).Rows
                Dim oLinea As New CLineaPedido(mDBServer, mIsAuthenticated)
                oLinea.ID = linea.Item("IDLINEA")
                oLinea.LineaCatalogo = DBNullToDbl(linea.Item("LINEA"))
                oLinea.Prove = linea.Item("PROVE")
                oLinea.Cod = DBNullToStr(linea.Item("COD_ITEM"))
                oLinea.Den = linea.Item("ART_DEN")
                oLinea.NumLinea = linea.Item("NUM_LINEA")
                oLinea.Generico = DBNullToInteger(linea.Item("GENERICO")) 'PARA MODIFICAR LA DESCRIPCION
                oLinea.CantPed = linea.Item("CANT")
                oLinea.CantRec = DBNullToDbl(linea.Item("CANT_REC"))
                oLinea.UP = linea.Item("UP")
                If linea.Item("NUMDECUP") Is System.DBNull.Value Then
                    oLinea.NumDecUP = Nothing
                Else
                    oLinea.NumDecUP = CType(linea.Item("NUMDECUP"), Integer)
                End If
                oLinea.PrecUc = linea.Item("PREC")
                oLinea.FC = linea.Item("FC")
                oLinea.Est = linea.Item("EST")
                oLinea.CantMinimaPedido = DBNullToDbl(linea.Item("CANT_MIN_DEF"))
                oLinea.ImportePendiente = DBNullToDbl(linea.Item("IMPORTE_PENDIENTE"))
                oLinea.GMN1 = DBNullToStr(linea.Item("GMN1"))
                oLinea.Dest = DBNullToStr(linea.Item("DESTINO"))
                oLinea.DestDen = DBNullToStr(linea.Item("DESTDEN"))
                oLinea.Cat1 = DBNullToDbl(linea.Item("CAT1"))
                oLinea.Cat2 = DBNullToDbl(linea.Item("CAT2"))
                oLinea.Cat3 = DBNullToDbl(linea.Item("CAT3"))
                oLinea.Cat4 = DBNullToDbl(linea.Item("CAT4"))
                oLinea.Cat5 = DBNullToDbl(linea.Item("CAT5"))
                oLinea.CatRama = DBNullToStr(linea.Item("CATEGORIARAMA"))
                oLinea.Gestionable = DBNullToBoolean(linea.Item("GESTIONABLE"))
                oLinea.ModificarPrecio = SQLBinaryToBoolean(linea.Item("MODIF_PREC"))
                'oLinea.ModificarUnidad = SQLBinaryToBoolean(linea.Item("MODIF_UNI"))
                If linea.Item("FECENTREGA") IsNot DBNull.Value Then _
                    oLinea.FecEntrega = linea.Item("FECENTREGA").ToString()
                oLinea.EntregaObl = SQLBinaryToBoolean(linea.Item("ENTREGA_OBL"))
                oLinea.Obs = DBNullToStr(linea.Item("OBS"))
                Dim oTipoArt As CArticulo.strTipoArticulo
                oTipoArt.Concepto = linea.Item("CONCEPTO")
                oTipoArt.TipoAlmacenamiento = linea.Item("ALMACENAR")
                oTipoArt.TipoRecepcion = linea.Item("RECEPCIONAR")
                oLinea.TipoArticulo = oTipoArt
                oLinea.IdAlmacen = DBNullToDbl(linea.Item("ALMACEN"))
                oLinea.Almacen = DBNullToStr(linea.Item("ALMACENDEN"))
                oLinea.TipoRecepcion = DBNullToInteger(linea.Item("TIPORECEPCION"))
                oLinea.ImportePedido = DBNullToDbl(linea.Item("IMPORTE_PEDIDO"))
                oLinea.MotivoRechazo = DBNullToStr(linea.Item("COMENT"))
                oLinea.CentroAprovisionamiento = DBNullToStr(linea.Item("CENTROS"))
                oLinea.Pres5_ImportesImputados_EP = New List(Of Imputacion)

                For Each linimputacion As DataRow In linea.GetChildRows("REL_PEDIDO_IMPUTACIONES")
                    oLinea.GestorCod = DBNullToDbl(linimputacion.Item("GESTOR"))
                    Dim oImp As FSNServer.Imputacion = New FSNServer.Imputacion(DBServer, mIsAuthenticated)
                    Dim oImpPed As FSNServer.ImputacionPedido = New FSNServer.ImputacionPedido(DBServer, mIsAuthenticated)

                    oImp.Id = linimputacion.Item("ID")
                    oImp.LineaId = linimputacion.Item("IDLINEA")
                    oImp.CentroCoste = New FSNServer.UON(DBServer, mIsAuthenticated)
                    oImp.CentroCoste.UON1 = DBNullToSomething(linimputacion.Item("UON1CS"))
                    oImp.CentroCoste.UON2 = DBNullToSomething(linimputacion.Item("UON2CS"))
                    oImp.CentroCoste.UON3 = DBNullToSomething(linimputacion.Item("UON3CS"))
                    oImp.CentroCoste.UON4 = DBNullToSomething(linimputacion.Item("UON4CS"))
                    oImp.CentroCoste.CentroSM = New FSNServer.Centro_SM(DBServer, mIsAuthenticated)
                    oImp.CentroCoste.CentroSM.Codigo = linimputacion.Item("CENTRO_SM")
                    oImp.CentroCoste.CentroSM.Denominacion = linimputacion.Item("CCDEN")
                    If Not IsDBNull(linimputacion.Item("PRES5_IMP")) Then
                        oImp.CentroCoste.CentroSM.PartidasPresupuestarias = New FSNServer.PartidasPRES5(DBServer, mIsAuthenticated)
                        Dim oPart As FSNServer.PartidaPRES5 = New FSNServer.PartidaPRES5(DBServer, mIsAuthenticated)
                        oPart.NIV0 = linimputacion.Item("PRES0")
                        oPart.NIV1 = DBNullToSomething(linimputacion.Item("PRES1"))
                        oPart.NIV1_Den = DBNullToSomething(linimputacion.Item("PRES1_DEN"))
                        oPart.NIV2 = DBNullToSomething(linimputacion.Item("PRES2"))
                        oPart.NIV2_Den = DBNullToSomething(linimputacion.Item("PRES2_DEN"))
                        oPart.NIV3 = DBNullToSomething(linimputacion.Item("PRES3"))
                        oPart.NIV3_Den = DBNullToSomething(linimputacion.Item("PRES3_DEN"))
                        oPart.NIV4 = DBNullToSomething(linimputacion.Item("PRES4"))
                        oPart.NIV4_Den = DBNullToSomething(linimputacion.Item("PRES4_DEN"))
                        oPart.PresupuestoId = linimputacion.Item("PRES5_IMP")
                        oPart.Denominacion = linimputacion.Item("PARTIDA_DEN")
                        If linimputacion.Item("FECINI") IsNot DBNull.Value Then _
                            oPart.FechaInicioPresupuesto = linimputacion.Item("FECINI")
                        If linimputacion.Item("FECFIN") IsNot DBNull.Value Then _
                            oPart.FechaFinPresupuesto = linimputacion.Item("FECFIN")
                        oImp.Partida = oPart
                        oImp.CentroCoste.CentroSM.PartidasPresupuestarias.Add(oPart)
                    End If
                    oLinea.Pres5_ImportesImputados_EP.Add(oImp)
                Next

                For Each rImpuesto As DataRow In linea.GetChildRows("REL_IMPUESTOS")
                    Dim oImpuesto As FSNServer.CImpuestos = New FSNServer.CImpuestos()
                    oImpuesto.Id = rImpuesto.Item("ID")
                    oImpuesto.Codigo = rImpuesto.Item("COD")
                    oImpuesto.Denominacion = rImpuesto.Item("DEN")
                    oImpuesto.Valor = rImpuesto.Item("VALOR")
                    oLinea.Impuestos.Add(oImpuesto)
                Next

                ''presupuestos
                Dim dr() As DataRow = DsResul.Tables(4).Select("ID=" & oLinea.ID)
                '' LINEA->presupestos
                '' presupuesto -> partidas
                '' partida -> denominación , porcentaje
                For Each r As DataRow In dr
                    If Not oLinea.Presupuestos.ContainsKey(r("ARBOL")) Then
                        oLinea.Presupuestos.Add(r("ARBOL"), New With {.den = PresDen(r("ARBOL") - 1), .partidas = New List(Of Object)})
                    End If
                    oLinea.Presupuestos(r("ARBOL")).partidas.add(New With {.den = r("COD"), .porcen = r("PORCEN")})
                Next

                Dim oPlanEntrega As PlanEntrega
                For Each iPlanEntrega As DataRow In linea.GetChildRows("REL_PLAN_ENTREGA")
                    oLinea.ConPlanEntrega = True
                    oPlanEntrega = New PlanEntrega
                    With oPlanEntrega
                        .IdPlanEntrega = iPlanEntrega("ID")
                        .FechaEntrega = CDate(iPlanEntrega("FECHA"))
                        .CantidadEntrega = DBNullToDbl(iPlanEntrega("CANTIDAD"))
                        .ImporteEntrega = DBNullToDbl(iPlanEntrega("IMPORTE"))
                        .Albaran = DBNullToStr(iPlanEntrega("ALBARAN"))
                    End With
                    oLinea.PlanEntrega.Add(oPlanEntrega)
                Next
                oLinea.ConRecepcionAutomatica = DBNullToBoolean(linea.Item("IM_RECEPAUTO"))
                oLinea.EstaDeBaja = DBNullToBoolean(linea.Item("BAJA_LOG"))
                oLineas.Add(oLinea)
            Next
        End If
        Return (oLineas)
    End Function

    ''' <summary>
    ''' Función que devuelve los costes de la linea
    ''' </summary>
    ''' <param name="iCodLinea">Id de la linea de pedido</param>
    ''' <param name="sIdioma">idioma</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Cargar_Costes_Linea(ByVal iCodLinea As Integer, Optional ByVal sIdioma As String = "SPA") As CAtributosPedido
        Authenticate()

        Dim adoRes As New DataSet
        Dim oAtributos As CAtributosPedido
        oAtributos = New CAtributosPedido(mDBServer, mIsAuthenticated)
        oAtributos = oAtributos.CargarAtributosCosteLineaOrden(iCodLinea, sIdioma)

        Return (oAtributos)
    End Function



    ''' <summary>
    ''' Función que devuelve los descuentos de la linea
    ''' </summary>
    ''' <param name="iCodLinea"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Cargar_Descuentos_Linea(ByVal iCodLinea As Integer) As CAtributosPedido
        Authenticate()

        Dim adoRes As New DataSet
        Dim oAtributos As CAtributosPedido
        oAtributos = New CAtributosPedido(mDBServer, mIsAuthenticated)
        oAtributos = oAtributos.CargarAtributosDescuentoLineaOrden(iCodLinea)

        Return oAtributos
    End Function

    ''' <summary>
    ''' Función que devuelve los atributos de la linea de orden de entrega(campos de pedido)
    ''' </summary>
    ''' <param name="iCodLinea">Id de la linea del pedido</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Cargar_Atributos_Linea(ByVal iCodLinea As Integer) As CAtributosPedido
        Authenticate()

        Dim adoRes As New DataSet
        Dim oAtributos As CAtributosPedido
        oAtributos = New CAtributosPedido(mDBServer, mIsAuthenticated)
        oAtributos = oAtributos.CargarAtributosLineaOrden(iCodLinea)

        Return oAtributos
    End Function
    ''' <summary>
    ''' Función que valida las cantidades de una linea de una orden
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>
    ''' Tiempo máximo: 1 seg</remarks>
    Public Function ComprobarCantidadesLineaOrden(ByVal idLinea As Integer, ByVal sUPed As String, ByVal dCant As Double, ByVal oLineas As List(Of CLineaPedido)) As Integer

        '0 -- No hay error
        '1 -- Cantidad no supera la minima establecida
        '2 -- Cantidad supera la maxima extablecida
        '3 -- Cantidad supera maxima total a pedir por linea de catalogo
        '4 -- Supera la cantidad maxima a pedir por linea de catalogo en unidad de pedido

        Authenticate()
        Dim dsCantidades As DataSet
        Dim dCantidadTotal, dCantidadPedida As Double
        dsCantidades = mDBServer.ComprobarCantidadesLineaOrden(idLinea, sUPed)
        If Not IsNothing(dsCantidades) Then
            If dsCantidades.Tables.Count <> 0 Then
                For Each oRow As DataRow In dsCantidades.Tables(0).Rows

                    If Not IsDBNull(oRow("CANT_MIN_COM")) Then
                        If dCant < oRow("CANT_MIN_COM") Then
                            Return 1
                        End If
                    ElseIf Not IsDBNull(oRow("CANT_MAX_COM")) Then
                        If dCant > DBNullToDbl(oRow("CANT_MAX_COM")) Then
                            Return 2
                        End If
                    End If
                    If Not IsDBNull(oRow("CANT_MAX_TOTAL_COM")) Then
                        dCantidadPedida = dCant
                        For Each oLinea As CLineaPedido In oLineas
                            If oLinea.LineaCatalogo = oRow("LINEA") Then
                                If oLinea.UP = sUPed And oLinea.ID <> idLinea Then
                                    dCantidadPedida = dCantidadPedida + oLinea.CantPed
                                End If
                            End If
                        Next
                        For Each oRowCantidades As DataRow In dsCantidades.Tables(1).Rows
                            If oRowCantidades("UP") = sUPed Then
                                dCantidadTotal = DBNullToDbl(oRowCantidades("CANTPEDIDA")) + dCantidadPedida
                                If DBNullToDbl(oRow("CANT_MAX_TOTAL_COM")) < dCantidadTotal Then
                                    Return 4
                                End If
                            End If
                        Next
                    End If
                    dCantidadTotal = 0
                    dCantidadPedida = 0
                    If Not IsDBNull(oRow("cant_max_total_uc")) Then
                        For Each oLinea As CLineaPedido In oLineas
                            If oLinea.LineaCatalogo = oRow("LINEA") Then
                                If oLinea.UP = oRow("UC") Then
                                    If oLinea.ID = idLinea Then
                                        dCantidadPedida = dCantidadPedida + dCant
                                    Else
                                        dCantidadPedida = dCantidadPedida + oLinea.CantPed
                                    End If
                                Else
                                    Dim dCantidaCalculada As Double
                                    If oLinea.ID = idLinea Then
                                        If oLinea.FC <> 0 Then
                                            dCantidaCalculada = dCant * oLinea.FC
                                        Else
                                            dCantidaCalculada = dCant
                                        End If
                                    Else
                                        If oLinea.FC <> 0 Then
                                            dCantidaCalculada = oLinea.CantPed * oLinea.FC
                                        Else
                                            dCantidaCalculada = oLinea.CantPed
                                        End If
                                    End If
                                    dCantidadPedida = dCantidadPedida + dCantidaCalculada
                                End If
                            End If
                        Next
                        dCantidadTotal = dCantidadPedida
                        For Each oRowCantidades As DataRow In dsCantidades.Tables(1).Rows
                            If oRowCantidades("UP") = oRow("UC") Then
                                dCantidadTotal = dCantidadTotal + DBNullToDbl(oRowCantidades("CANTPEDIDA"))
                            Else
                                dCantidadTotal = dCantidadTotal + DBNullToDbl(oRowCantidades("CANTPEDIDA")) * DBNullToDbl(oRowCantidades("FC"))
                            End If
                        Next
                        If DBNullToDbl(oRow("CANT_MAX_TOTAL_UC")) < dCantidadTotal Then
                            Return 3
                        End If
                    End If
                Next
            End If
        End If

        Return 0
    End Function
    ''' <summary>
    ''' Actualiza las lineas de pedido que se han modificado, con sus costes, descuentos, impuestos,imputacion de las lineas....
    ''' </summary>
    ''' <param name="UsuCod">cod de usuario</param>
    ''' <param name="Idioma">idioma de la aplicacion</param>
    ''' <param name="Persona">cod de persona</param>
    ''' <param name="Moneda">moneda</param>
    ''' <param name="oDetallePedido">objeto con todos los datos del pedido</param>
    ''' <param name="IdOrden">id de la orden de entrega</param>
    ''' <param name="oPargenSM">configuracion de la partida presupuestaria</param>
    ''' <param name="ImporteOrden">Importe total de la orden de entrega, con impuestos, costes....(orden_entrega.importe)</param>
    ''' <remarks></remarks>
    Public Sub ActualizarPedido(ByVal UsuCod As String, ByVal Idioma As String, ByVal Persona As String, ByVal Moneda As String,
                                      ByVal oDetallePedido As CDetallePedido, ByVal IdOrden As Long, ByVal oPargenSM As FSNServer.PargenSM, ByVal ImporteOrden As Double, ByVal ImporteSolicitud As Double, ByVal idInstancia As Long)
        Authenticate()

        Dim dtCostesDescuentosCabecera As New DataTable
        dtCostesDescuentosCabecera.Columns.Add("ID")
        dtCostesDescuentosCabecera.Columns.Add("IMPORTE")
        dtCostesDescuentosCabecera.Columns.Add("ORDEN")

        Dim dr As DataRow
        'Añado los costes de cabecera a una tabla, para luego actualizarlos en base de datos
        For Each oCoste As CAtributoPedido In oDetallePedido.Costes
            dr = dtCostesDescuentosCabecera.NewRow()
            dr("ID") = oCoste.Id
            dr("IMPORTE") = oCoste.Importe
            dr("ORDEN") = IdOrden
            dtCostesDescuentosCabecera.Rows.Add(dr)
        Next
        'Añado los descuentos de cabecera a una tabla, para luego actualizarlos en base de datos
        For Each oDescuento As CAtributoPedido In oDetallePedido.Descuentos
            dr = dtCostesDescuentosCabecera.NewRow()
            dr("ID") = oDescuento.Id
            dr("IMPORTE") = oDescuento.Importe
            dr("ORDEN") = IdOrden
            dtCostesDescuentosCabecera.Rows.Add(dr)
        Next

        mDBServer.Orden_ActualizarCostesDescuentosCabecera(dtCostesDescuentosCabecera)
        dtCostesDescuentosCabecera = Nothing

        Dim dtCostesDescuentosLinea As New DataTable
        dtCostesDescuentosLinea.Columns.Add("ID")
        dtCostesDescuentosLinea.Columns.Add("IMPORTE")
        dtCostesDescuentosLinea.Columns.Add("LINEA")


        Dim dtLineas As New DataTable
        dtLineas.Columns.Add("ID")
        dtLineas.Columns.Add("CANT")
        dtLineas.Columns.Add("PREC")
        dtLineas.Columns.Add("IMP_PED")
        dtLineas.Columns.Add("EST")
        dtLineas.Columns.Add("COMENT")
        dtLineas.Columns.Add("PER")
        dtLineas.Columns.Add("MODIFICADA")

        For Each oLineaPedido As CLineaPedido In oDetallePedido.Lineas
            For Each oCoste As CAtributoPedido In oLineaPedido.Costes
                dr = dtCostesDescuentosLinea.NewRow()
                dr("ID") = oCoste.Id
                dr("IMPORTE") = oCoste.Importe
                dr("LINEA") = oLineaPedido.ID
                dtCostesDescuentosLinea.Rows.Add(dr)
            Next
            For Each oDescuento As CAtributoPedido In oLineaPedido.Descuentos
                dr = dtCostesDescuentosLinea.NewRow()
                dr("ID") = oDescuento.Id
                dr("IMPORTE") = oDescuento.Importe
                dr("LINEA") = oLineaPedido.ID
                dtCostesDescuentosLinea.Rows.Add(dr)
            Next

            If oLineaPedido.Gestionable = True AndAlso (oLineaPedido.Est = 0 Or oLineaPedido.Est = 20 Or oLineaPedido.Est = 1) Then
                dr = dtLineas.NewRow
                dr("ID") = oLineaPedido.ID
                dr("EST") = oLineaPedido.Est
                dr("MODIFICADA") = IIf(oLineaPedido.Modificada, 1, 0)
                If oLineaPedido.TipoRecepcion = 1 Then
                    dr("IMP_PED") = oLineaPedido.ImportePedido
                    dr("CANT") = DBNull.Value
                    dr("PREC") = DBNull.Value
                Else
                    dr("CANT") = oLineaPedido.CantPed
                    dr("PREC") = oLineaPedido.PrecUc
                    dr("IMP_PED") = DBNull.Value
                End If
                If oLineaPedido.MotivoRechazo <> "" Then
                    dr("COMENT") = oLineaPedido.MotivoRechazo
                Else
                    dr("COMENT") = DBNull.Value
                End If
                dr("PER") = Persona
                dtLineas.Rows.Add(dr)
            End If
        Next
        mDBServer.Orden_ActualizarCostesDescuentosLineas(dtCostesDescuentosLinea)

        mDBServer.Orden_ActualizarLineas(dtLineas)

        mDBServer.Orden_ActualizarImporte(IdOrden, ImporteOrden, ImporteSolicitud, idInstancia)

        dtCostesDescuentosLinea = Nothing
        dtLineas = Nothing
    End Sub
    ''' <summary>
    ''' Actualiza el importe comprometido para las lineas de una orden
    ''' </summary>
    ''' <param name="idOrden">id de la orden</param>
    ''' <remarks></remarks>
    Public Sub ActualizarImputacion(ByVal idOrden As Integer)
        Authenticate()

        mDBServer.ActualizarImputacionesLineaPedidoEmitida(idOrden)
    End Sub
End Class
