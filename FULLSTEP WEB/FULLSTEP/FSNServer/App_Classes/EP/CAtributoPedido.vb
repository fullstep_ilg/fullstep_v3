
<Serializable()> _
Public Class CAtributoPedido
    Inherits Security

    Private mvId As Object
    Private msCod As String
    Private msDen As String
    Private miIntro As Short
    Private mudtTipo As TiposDeDatos.TipoAtributo
    Private mInterno As Boolean
    Private mvValor As Object
    Private mvImporte As Object
    Private mvImporteBD As Object ' Esta variable se usa para indicar el valor que existe en la BBDD en el campo IMPORTE. Se usa para imprimir a PDF el detalle de pedido.
    Private mvValorMin As Object
    Private mvValorMax As Object
    Private msOperacion As Object
    Private msTipoAtrib As Object
    Private msTipoCosteDescuento As Object
    Private msGrupoCosteDescuento As Object
    Private mbObligatorio As Boolean
    Private mbIntegracion As Boolean
    Private mbTipoPed As Boolean
    Private mbSeleccionado As Boolean
    Private moValores As CValoresAtrib
    Private moImpuestos As List(Of CImpuestos)

    Public Sub New()
        Impuestos = New List(Of CImpuestos)
    End Sub
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
        Impuestos = New List(Of CImpuestos)
    End Sub

    Private _Id As Long
    Public Property Id() As Long
        Get
            Id = _Id
        End Get
        Set(ByVal Value As Long)
            _Id = Value
        End Set
    End Property

    Private _Codigo As String
    Public Property Codigo() As String
        Get
            Return _Codigo
        End Get
        Set(ByVal value As String)
            _Codigo = value
        End Set
    End Property
    Private _Denominacion As String
    Public Property Denominacion() As String
        Get
            Return _Denominacion
        End Get
        Set(ByVal value As String)
            _Denominacion = value
        End Set
    End Property
    Private _Valor As Object
    Public Property Valor() As Object
        Get
            Select Case _Tipo
                Case 3
                    If _Valor Is DBNull.Value Or _Valor Is Nothing Or DBNullToStr(_Valor) = "" Then
                        Return _Valor
                    Else
                        Return CDate(_Valor)
                    End If
                Case Else
                    Return _Valor
            End Select
        End Get
        Set(ByVal value As Object)
            If _Tipo = 3 AndAlso Not value Is DBNull.Value AndAlso Not _Valor Is Nothing Then
                _Valor = CDate(value)
            Else
                _Valor = value
            End If

        End Set
    End Property

    Public Function getValueText(numberFormat As System.Globalization.NumberFormatInfo, dateFormat As String, sYes As String, sNo As String) As String
        If _Valor Is DBNull.Value Or _Valor Is Nothing Then
            Return ""
        End If
        Select Case _Tipo
            Case 2 'Numerico
                Return FSNLibrary.FormatNumber(_Valor, numberFormat)
            Case 3 'Fecha
                If _Valor Is DBNull.Value Or _Valor Is Nothing Or DBNullToStr(_Valor) = "" Then
                    Return _Valor
                Else
                    Return FSNLibrary.modUtilidades.VisualizacionFecha(CDate(_Valor), dateFormat)
                End If
            Case 4
                If _Valor = "1" Then
                    Return sYes
                ElseIf _Valor = "0" Then
                    Return sNo
                Else
                    Return ""
                End If
            Case Else
                Return _Valor
        End Select
    End Function

    Public Property Importe() As Object
        Get
            Importe = mvImporte
        End Get
        Set(ByVal Value As Object)
            mvImporte = Value
        End Set
    End Property

    Public Property ImporteBD() As Object
        Get
            ImporteBD = mvImporteBD
        End Get
        Set(ByVal Value As Object)
            mvImporteBD = Value
        End Set
    End Property

    Public Property ValorMin() As Object
        Get
            ValorMin = mvValorMin
        End Get
        Set(ByVal Value As Object)
            mvValorMin = Value
        End Set
    End Property

    Public Property ValorMax() As Object
        Get
            ValorMax = mvValorMax
        End Get
        Set(ByVal Value As Object)
            mvValorMax = Value
        End Set
    End Property
    Private _Valores As List(Of ValorAtrib)
    Public Property Valores() As List(Of ValorAtrib)
        Get
            Return _Valores
        End Get
        Set(ByVal value As List(Of ValorAtrib))
            _Valores = value
        End Set
    End Property
    Private _Intro As Short
    Public Property Intro() As Short
        Get
            Intro = _Intro
        End Get
        Set(ByVal Value As Short)
            _Intro = Value
        End Set
    End Property
    Private _Tipo As TiposDeDatos.TipoAtributo
    Public Property Tipo() As TiposDeDatos.TipoAtributo
        Get
            Tipo = _Tipo
        End Get
        Set(ByVal Value As TiposDeDatos.TipoAtributo)
            _Tipo = Value
        End Set
    End Property
    Private _Interno As Boolean
    Public Property Interno() As Boolean
        Get
            Interno = _Interno
        End Get
        Set(ByVal Value As Boolean)
            _Interno = Value
        End Set
    End Property
    Private _Obligatorio As Boolean
    Public Property Obligatorio() As Boolean
        Get
            Obligatorio = _Obligatorio
        End Get
        Set(ByVal Value As Boolean)
            _Obligatorio = Value
        End Set
    End Property

    Private _Seleccionado As Boolean
    Public Property Seleccionado() As Boolean
        Get
            Seleccionado = _Seleccionado
        End Get
        Set(ByVal Value As Boolean)
            _Seleccionado = Value
        End Set
    End Property

    Public Property Operacion() As Object
        Get
            Operacion = msOperacion
        End Get
        Set(ByVal Value As Object)
            msOperacion = Value
        End Set
    End Property

    Public Property Integracion() As Boolean
        Get
            Integracion = mbIntegracion
        End Get
        Set(ByVal Value As Boolean)
            mbIntegracion = Value
        End Set
    End Property

    Public Property TipoPed() As Boolean
        Get
            TipoPed = mbTipoPed
        End Get
        Set(ByVal Value As Boolean)
            mbTipoPed = Value
        End Set
    End Property

    Public Property TipoAtrib() As Object
        Get
            TipoAtrib = msTipoAtrib
        End Get
        Set(ByVal Value As Object)
            msTipoAtrib = Value
        End Set
    End Property

    Public Property TipoCosteDescuento() As Object
        Get
            TipoCosteDescuento = msTipoCosteDescuento
        End Get
        Set(ByVal Value As Object)
            msTipoCosteDescuento = Value
        End Set
    End Property

    Public Property GrupoCosteDescuento() As Object
        Get
            GrupoCosteDescuento = msGrupoCosteDescuento
        End Get
        Set(ByVal Value As Object)
            msGrupoCosteDescuento = Value
        End Set
    End Property

    Private _Impuestos As List(Of CImpuestos)
    Public Property Impuestos() As List(Of CImpuestos)
        Get
            Return _Impuestos
        End Get
        Set(ByVal value As List(Of CImpuestos))
            _Impuestos = value
        End Set
    End Property

    Private _listaExterna As Boolean
    Public Property ListaExterna() As Boolean
        Get
            Return _listaExterna
        End Get
        Set(ByVal value As Boolean)
            _listaExterna = value
        End Set
    End Property

    Private _MostrarEnRecep As Boolean
    Public Property MostrarEnRecep() As Boolean
        Get
            MostrarEnRecep = _MostrarEnRecep
        End Get
        Set(ByVal Value As Boolean)
            _MostrarEnRecep = Value
        End Set
    End Property
End Class

Public Class ValorAtrib
    Public Sub New()

    End Sub
    Private _Valor As Object
    Public Property Valor() As Object
        Get
            Return _Valor
        End Get
        Set(ByVal value As Object)
            _Valor = value
        End Set
    End Property
End Class
