﻿Imports Fullstep.FSNLibrary

Public Class CUnidadPedido
    Inherits Security

    Dim _sCodigo As String
    Dim _sDenominacion As String
    Dim _dFactorConversion As Double
    Dim _dCantidadMinima As Double
    Dim _sUnidadCompra As String
    Dim _sDenUnidadCompra As String
    Private _moUnitFormat As System.Globalization.NumberFormatInfo
    Private _mnNumDec As Nullable(Of Integer)

    ''' <summary>
    ''' Número de decimales que hay que mostrar de la unidad
    ''' También se lo asignamos a la propiedad UnitFormat.NumberDecimalDigits
    ''' Cuando es NULO no podemos asignarlo a UnitFormat.NumberDecimalDigits. Ponemos en cambio 99 en ese valor (función AsignarFormatoaUnidadPedido)
    ''' Y usamos esta propiedad para controlar si es nulo
    ''' </summary>
    Public Property NumeroDeDecimales() As Nullable(Of Integer)
        Get
            Return _mnNumDec
        End Get
        Set(ByVal value As Nullable(Of Integer))
            _mnNumDec = value
        End Set
    End Property

    ''' <summary>
    ''' Formato de las unidades a usar en la interfaz
    ''' </summary>
    Public Property UnitFormat() As System.Globalization.NumberFormatInfo
        Get
            UnitFormat = _moUnitFormat
        End Get
        Set(ByVal Value As System.Globalization.NumberFormatInfo)
            _moUnitFormat = Value
        End Set
    End Property



    Public Property Codigo() As String
        Get
            Return _sCodigo
        End Get
        Set(ByVal value As String)
            _sCodigo = value
        End Set
    End Property

    Public Property Denominacion() As String
        Get
            Return _sDenominacion
        End Get
        Set(ByVal value As String)
            _sDenominacion = value
        End Set
    End Property

    Public Property FactorConversion() As Double
        Get
            Return _dFactorConversion
        End Get
        Set(ByVal value As Double)
            _dFactorConversion = value
        End Set
    End Property

    Public Property CantidadMinima() As Double
        Get
            Return _dCantidadMinima
        End Get
        Set(ByVal value As Double)
            _dCantidadMinima = value
        End Set
    End Property

    Public Property UnidadCompra() As String
        Get
            Return _sUnidadCompra
        End Get
        Set(ByVal value As String)
            _sUnidadCompra = value
        End Set
    End Property

    Public Property DenUnidadCompra() As String
        Get
            Return _sDenUnidadCompra
        End Get
        Set(ByVal value As String)
            _sDenUnidadCompra = value
        End Set
    End Property

    ''' <summary>
    ''' Carga los datos de la unidad en el idioma especificado.
    ''' </summary>
    ''' <param name="CodIdioma">Código de idioma</param>
    ''' <remarks>Llamada desde Consultas.asmx</remarks>
    Public Sub CargarDatos(ByVal CodIdioma As FSNLibrary.Idioma)
        Dim ds As DataSet = DBServer.UnidadPedido_CargarDatosUnidad(CodIdioma, Me.Codigo)
        With ds.Tables(0).Rows(0)
            _sDenominacion = .Item("DEN")
        End With
    End Sub

    ''' <summary>
    ''' Carga los datos de la unidad en el idioma especificado.
    ''' </summary>
    ''' <param name="CodIdioma">Código de idioma</param>
    ''' <param name="Linea">Id de la línea de catálogo</param>
    ''' <remarks>Llamada desde Consultas.asmx</remarks>
    Public Sub CargarDatos(ByVal CodIdioma As FSNLibrary.Idioma, ByVal Linea As Integer)
        Dim ds As DataSet = DBServer.UnidadPedido_CargarDatosUnidad(CodIdioma, Me.Codigo, Linea)
        If ds.Tables.Count > 0 Then
            With ds.Tables(0).Rows(0)
                _sDenominacion = .Item("DEN")
                _dFactorConversion = .Item("FC")
                _dCantidadMinima = DBNullToDbl(.Item("CANT_MIN"))
                _sUnidadCompra = DBNullToSomething(.Item("UC"))
                _sDenUnidadCompra = DBNullToSomething(.Item("DEN_UC"))
                If Not DBNullToSomething(.Item("NUMDEC")) Is Nothing Then
                    _moUnitFormat = New System.Globalization.NumberFormatInfo
                    _moUnitFormat.NumberDecimalDigits = DBNullToSomething(.Item("NUMDEC"))
                End If
                If .Item("NUMDEC") Is System.DBNull.Value Then
                    _mnNumDec = Nothing
                Else
                    _mnNumDec = CType(.Item("NUMDEC"), Integer)
                End If
            End With
        End If
    End Sub

    ''' <summary>
    ''' Constructor de la clase CUnidadPedido
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si está autenticado</param>
    Public Sub New(ByRef dbserver As Fullstep.FSNDataBaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
    End Sub

    ''' <summary>
    ''' Método que, cuando el UnitFormat de la unidad Pedido está definido, 
    ''' reemplaza el NumberFormatInfo definido para la unidad de Pedido por el del Usuario excepto el único valor que
    ''' nos interesa conservar, el valor de la propiedad NumberDecimalDigits
    ''' y cuando el UnitFormat no está definido, le asigna el del usuario y asigna al NumberDecimalDigits el máximo valor posible
    ''' </summary>
    ''' <param name="NumerFormatDeUsuario">NumberFormatInfo del usuario</param>
    ''' <remarks>
    ''' Llamada desde EPWEb: Aprobacion.aspx.vb, CatalogoWeb.aspx.vb, detArticuloCatalogo.ascx.vb, etc
    ''' tiempo máx. inferior a 0,1 seg
    ''' </remarks>
    Public Sub AsignarFormatoaUnidadPedido(ByVal NumerFormatDeUsuario As System.Globalization.NumberFormatInfo)
        If Not Me.UnitFormat Is Nothing Then
            Dim iNumeroDecimalesTemporal As Integer = Me.UnitFormat.NumberDecimalDigits
            Me.UnitFormat = CType(NumerFormatDeUsuario.Clone(), System.Globalization.NumberFormatInfo)
            Me.UnitFormat.NumberDecimalDigits = iNumeroDecimalesTemporal
            'Me.UnitFormat.NumberDecimalSeparator = NumerFormatDeUsuario.NumberDecimalSeparator
            'Me.UnitFormat.NumberGroupSeparator = NumerFormatDeUsuario.NumberGroupSeparator
        Else
            Me.UnitFormat = New System.Globalization.NumberFormatInfo
            Me.UnitFormat = CType(NumerFormatDeUsuario.Clone(), System.Globalization.NumberFormatInfo)
            Me.UnitFormat.NumberDecimalDigits = 99 'Valor por defecto para unidades con un numero de decimales en base de datos = null
        End If
    End Sub
End Class
