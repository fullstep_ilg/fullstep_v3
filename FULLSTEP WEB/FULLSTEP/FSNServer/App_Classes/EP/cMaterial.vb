﻿Public Class cMaterial
    Inherits Security

    Private iNivel As Integer
    Private strDenominacion As String
    Private strCodGMN1 As String
    Private strCodGMN2 As String
    Private strCodGMN3 As String
    Private strCodGMN4 As String

    ''' <summary>
    ''' Constructor de la clase
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si está autenticado</param>
    ''' <remarks>
    ''' Llamada desde: Todos los sitios en los que se instancie un objeto de la clase cMaterial
    ''' Tiempo máximo: 0 sec
    ''' </remarks>
    Public Sub New(ByRef dbserver As Fullstep.FSNDataBaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
    End Sub

    Public Property Nivel() As Integer
        Get
            Return iNivel
        End Get
        Set(ByVal value As Integer)
            iNivel = value
        End Set
    End Property

    Public Property Denominacion() As String
        Get
            Return strDenominacion
        End Get
        Set(ByVal value As String)
            strDenominacion = value
        End Set
    End Property

    Public Property CodGMN1() As String
        Get
            Return strCodGMN1
        End Get
        Set(ByVal value As String)
            strCodGMN1 = value
        End Set
    End Property

    Public Property CodGMN2() As String
        Get
            Return strCodGMN2
        End Get
        Set(ByVal value As String)
            strCodGMN2 = value
        End Set
    End Property

    Public Property CodGMN3() As String
        Get
            Return strCodGMN3
        End Get
        Set(ByVal value As String)
            strCodGMN3 = value
        End Set
    End Property

    Public Property CodGMN4() As String
        Get
            Return strCodGMN4
        End Get
        Set(ByVal value As String)
            strCodGMN4 = value
        End Set
    End Property

    ''' <summary>
    ''' Función que devuelve un arbol con todos los materiales
    ''' </summary>
    ''' <returns>Un dataset con los datos de todos los materiales jerarquerizados en arbol</returns>
    ''' <remarks>
    ''' Llamada desde: La página Report_XML
    ''' Tiempo máximo: 1 seg</remarks>
    Public Function DevolverTodosMateriales() As DataSet
        Authenticate()
        Dim DsResul As New DataSet
        DsResul = DBServer.Material_DevolverArbolMateriales
        Return DsResul
    End Function
End Class
