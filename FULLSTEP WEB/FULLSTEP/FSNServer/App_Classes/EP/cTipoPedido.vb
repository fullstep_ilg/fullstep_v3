﻿<Serializable()> _
Public Class cTipoPedido
    Inherits Security

#Region " Constructor"

    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
        Inv = EnInvTipo.Ambos
        Rec = EnRecTipo.Opcional
        Alm = EnAlmacenTipo.Opcional
    End Sub
    Public Sub New()
        MyBase.New()
        Inv = EnInvTipo.Ambos
        Rec = EnRecTipo.Opcional
        Alm = EnAlmacenTipo.Opcional
    End Sub

#End Region

    Private strCod As String
    Private Inv As EnInvTipo
    Private Rec As EnRecTipo
    Private Alm As EnAlmacenTipo
    Private strDen As String
    Private intId As Integer
    Private bBajaLog As Boolean

    Public Enum EnInvTipo
        Gasto = 0
        Inversion = 1
        Ambos = 2
    End Enum

    Public Enum EnRecTipo
        NoRecepcionable = 0
        Obligatorio = 1
        Opcional = 2
    End Enum

    Public Enum EnAlmacenTipo
        NoAlmacenable = 0
        Obligatorio = 1
        Opcional = 2
    End Enum

    Public Property Denominacion() As String
        Get
            Return strDen
        End Get
        Set(ByVal value As String)
            strDen = value
        End Set
    End Property

    Public Property Codigo() As String
        Get
            Return strCod
        End Get
        Set(ByVal value As String)
            strCod = value
        End Set
    End Property

    Public Property Concepto() As EnInvTipo
        Get
            Return Inv
        End Get
        Set(ByVal value As EnInvTipo)
            Inv = value
        End Set
    End Property

    Public Property Recepcionable() As EnRecTipo
        Get
            Return Rec
        End Get
        Set(ByVal value As EnRecTipo)
            Rec = value
        End Set
    End Property

    Public Property Almacenable() As EnAlmacenTipo
        Get
            Return Alm
        End Get
        Set(ByVal value As EnAlmacenTipo)
            Alm = value
        End Set
    End Property

    Public Property BajaLogica() As Boolean
        Get
            Return bBajaLog
        End Get
        Set(ByVal value As Boolean)
            bBajaLog = value
        End Set
    End Property

    Public Property Id() As Integer
        Get
            Return intId
        End Get
        Set(ByVal value As Integer)
            intId = value
        End Set
    End Property

    ''' <summary>
    ''' Devuelve los datos del tipo de pedido pasado su codigo e idioma
    ''' </summary>
    ''' <param name="Idioma">Idioma de la apliación</param>
    ''' <param name="CodPedido">Codigo del tipo de pedido del que se quieren saber los datos</param>
    ''' <returns>Un DataSet con los datos del tipo de pedido del que se nos ha pasado el código</returns>
    ''' <remarks>
    ''' Llamada desde: EpWEB\Consultas.asmx.vb\TipoPedido_Datos
    ''' Tiempo máximo: 0,25 seg</remarks>
    Public Function CargarDatosTipoPedido(ByVal Idioma As String, ByVal CodPedido As String) As DataSet
        Dim DsResul As New DataSet
        DsResul = DBServer.TipoPedido_CargarDatosTipoPedido(Idioma, CodPedido)
        Return DsResul
    End Function
End Class
