﻿Public Class PlanEntrega
    Private _idPlanEntrega As Integer
    Public Property IdPlanEntrega() As Integer
        Get
            Return _idPlanEntrega
        End Get
        Set(ByVal value As Integer)
            _idPlanEntrega = value
        End Set
    End Property
    Private _fechaEntrega As Date
    Public Property FechaEntrega() As Date
        Get
            Return _fechaEntrega
        End Get
        Set(ByVal value As Date)
            _fechaEntrega = value
        End Set
    End Property
    Private _cantidadEntrega As Double
    Public Property CantidadEntrega() As Double
        Get
            Return _cantidadEntrega
        End Get
        Set(ByVal value As Double)
            _cantidadEntrega = value
        End Set
    End Property
    Private _importeEntrega As Double
    Public Property ImporteEntrega() As Double
        Get
            Return _importeEntrega
        End Get
        Set(ByVal value As Double)
            _importeEntrega = value
        End Set
    End Property
    Private _albaran As String
    Public Property Albaran() As String
        Get
            Return _albaran
        End Get
        Set(ByVal value As String)
            _albaran = value
        End Set
    End Property
End Class