
<Serializable()> _
Public Class CLineasFavoritos
    Inherits Lista(Of CLineaFavoritos)

    ''' <summary>
    ''' Recuperamos el item de la coleccion de lineas de pedido favorito
    ''' </summary>
    ''' <param name="Id">El Id de la linea de pedido favorito</param>
    ''' <value></value>
    ''' <returns>retorna el item de la colecci�n</returns>
    ''' <remarks>Llamada desde CatalogoWeb.aspx y Favorito.aspx
    ''' Tiempo maximo 0 sec</remarks>
    Public Overloads ReadOnly Property Item(ByVal Id As Integer) As CLineaFavoritos
        Get
            Return Me.Find(Function(elemento As CLineaFavoritos) elemento.ID = Id)
        End Get
    End Property

    ''' <summary>
    ''' A�ade un nuevo elemento a la coleccion de Lineas de pedido favorito
    ''' </summary>
    ''' <param name="lId"></param>
    ''' <param name="sUsu"></param>
    ''' <param name="sCod"></param>
    ''' <param name="sGMN1"></param>
    ''' <param name="sGMN2"></param>
    ''' <param name="sGMN3"></param>
    ''' <param name="sGMN4"></param>
    ''' <param name="sProve"></param>
    ''' <param name="sDen"></param>
    ''' <param name="sArtExt"></param>
    ''' <param name="sDest"></param>
    ''' <param name="sDestDen"></param>
    ''' <param name="sUP"></param>
    ''' <param name="sUniDen"></param>
    ''' <param name="dPrecUc"></param>
    ''' <param name="dCantPed"></param>
    ''' <param name="dCantAdj"></param>
    ''' <param name="dFC"></param>
    ''' <param name="dImporte"></param>
    ''' <param name="dtFecEntrega"></param>
    ''' <param name="bEntregaObl"></param>
    ''' <param name="lCat1"></param>
    ''' <param name="sCat1Den"></param>
    ''' <param name="lCat2"></param>
    ''' <param name="sCat2Den"></param>
    ''' <param name="lCat3"></param>
    ''' <param name="sCat3Den"></param>
    ''' <param name="lCat4"></param>
    ''' <param name="sCat4Den"></param>
    ''' <param name="lCat5"></param>
    ''' <param name="sCat5Den"></param>
    ''' <param name="sObs"></param>
    ''' <param name="sCat"></param>
    ''' <param name="sCatRama"></param>
    ''' <param name="sComent"></param>
    ''' <param name="iAnyoProce"></param>
    ''' <param name="lProce"></param>
    ''' <param name="lItem"></param>
    ''' <param name="lLineaCatalogo"></param>
    ''' <param name="oAdjuntos"></param>
    ''' <param name="oCampo1"></param>
    ''' <param name="oCampo2"></param>
    ''' <param name="vObsAdjun"></param>
    ''' <param name="DescripcionVisible"></param>
    ''' <param name="dCant_Min"></param>
    ''' <param name="bModifPrec"></param>
    ''' <returns>La coleccion de lineas de pedidos favoritos</returns>
    ''' <remarks>Llamada desde CatalogoWeb.aspx, COrdenFavoritos.vb, COrdenesFavoritos.vb, CLineasFavoritos.vb 
    ''' Tiempo maximo 1 sec</remarks>
    Public Overloads Function Add(ByVal lId As Integer, ByVal sUsu As String, ByVal sCod As Object, ByVal sGMN1 As Object, ByVal sGMN2 As Object, ByVal sGMN3 As Object, ByVal sGMN4 As Object, ByVal sProve As String, ByVal sDen As String, ByVal sArtExt As Object, ByVal sDest As String, ByVal sDestDen As String, ByVal sUP As Object, ByVal sUniDen As Object, ByVal dPrecUc As Object, ByVal dCantPed As Object, ByVal dCantAdj As Object, ByVal dFC As Object, ByVal dImporte As Object, ByVal dtFecEntrega As Object, ByVal bEntregaObl As Object, ByVal lCat1 As Object, ByVal sCat1Den As Object, ByVal lCat2 As Object, ByVal sCat2Den As Object, ByVal lCat3 As Object, ByVal sCat3Den As Object, ByVal lCat4 As Object, ByVal sCat4Den As Object, ByVal lCat5 As Object, ByVal sCat5Den As Object, ByVal sObs As Object, ByVal sCat As Object, ByVal sCatRama As Object, ByVal sComent As Object, ByVal iAnyoProce As Object, ByVal lProce As Object, ByVal lItem As Object, ByVal lLineaCatalogo As Object, ByVal oAdjuntos As CAdjuntos, ByVal oCampo1 As CCampo, ByVal oCampo2 As CCampo, ByVal vObsAdjun As Object, ByVal DescripcionVisible As String, ByVal dCant_Min As Object, ByVal bModifPrec As Object, ByVal iGenerico As Integer, ByVal sCentroAprovisionamiento As String) As CLineaFavoritos

        Dim objnewmember As New CLineaFavoritos(mDBServer, mIsAuthenticated)

        With objnewmember
            .ID = lId
            .Usuario = sUsu
            .Cod = sCod
            .GMN1 = sGMN1
            .GMN2 = sGMN2
            .GMN3 = sGMN3
            .GMN4 = sGMN4
            .Prove = sProve
            .Den = sDen
            .DescripcionVisible = sDen 'Porque ya no le pasamos la DescripcionVisible
            .ArtExt = sArtExt
            .Dest = sDest
            .DestDen = sDestDen
            .UP = sUP
            .UniDen = sUniDen
            .PrecUc = dPrecUc
            .CantPed = dCantPed
            .CantAdj = dCantAdj
            .FC = dFC
            .Importe = dImporte
            .FecEntrega = dtFecEntrega
            .EntregaObl = bEntregaObl
            .Cat1 = lCat1
            .Cat1Den = sCat1Den
            .Cat2 = lCat2
            .Cat2Den = sCat2Den
            .Cat3 = lCat3
            .Cat3Den = sCat3Den
            .Cat4 = lCat4
            .Cat4Den = sCat4Den
            .Cat5 = lCat5
            .Cat5Den = sCat5Den
            .Obs = sObs
            .Cat = sCat
            .CatRama = sCatRama
            .Coment = sComent
            .AnyoProce = iAnyoProce
            .Proce = lProce
            .Item = lItem
            .LineaCatalogo = lLineaCatalogo
            .ObsAdjun = vObsAdjun
            .Generico = iGenerico ' Inc 193 a�ado el generico a CArticulo

            If oAdjuntos Is Nothing Then
                .CargarAdjuntos(True)
            Else
                If oAdjuntos.Count = 0 Then
                    .CargarAdjuntos(True)
                Else
                    .Adjuntos = oAdjuntos
                End If
            End If

            .Campo1 = oCampo1
            .Campo2 = oCampo2

            .Cant_Min = dCant_Min
            .ModifPrec = bModifPrec

            .CentroAprovisionamiento = sCentroAprovisionamiento
        End With

        Me.Add(objnewmember)
        Return objnewmember

    End Function

    ''' <summary>
    ''' Constructor de la clase CLineasFavoritos
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si est� autenticado</param>
    ''' <remarks>
    ''' Llamada desde: Todos los sitios donde se instancien objetos de esta clase
    ''' Tiempo m�ximo: 0 sec. </remarks>
    Public Sub New(ByRef dbserver As Fullstep.FSNDataBaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
    End Sub

End Class