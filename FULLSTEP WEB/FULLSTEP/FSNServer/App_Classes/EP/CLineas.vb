<Serializable()> _
Public Class CLineas
    Inherits Lista(Of CLinea)

    Public ReadOnly Property ItemId(ByVal Id As Integer) As CLinea
        Get
            Return Me.Find(Function(elemento As CLinea) Id = elemento.ID)
        End Get
    End Property
    ''' <summary>
    ''' Funci�n que devuelve e inserta en la colecci�n un objeto CLinea con Cantidad Recibida y/o importe recibido
    ''' </summary>
    ''' <param name="lId">Id de la linea</param>
    ''' <param name="dCantRec">Cantidad Recibida</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Overloads Function Add(ByVal lId As Integer, ByVal dCantRec As Object, Optional ByVal bSalidaAlmacen As Boolean = False) As CLinea
        Return Add(lId, dCantRec, 0, bSalidaAlmacen:=bSalidaAlmacen)
    End Function
    ''' <summary>
    ''' Funci�n que devuelve e inserta en la colecci�n un objeto CLinea con Cantidad Recibida
    ''' </summary>
    ''' <param name="lId">Id de la linea</param>
    ''' <param name="dCantRec">Cantidad Recibida</param>
    ''' <param name="Activo">Id del Activo</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Overloads Function Add(ByVal lId As Integer, ByVal dCantRec As Object, ByVal Activo As Integer, Optional ByVal iTipoRecepcion As Integer = 0, Optional ByVal dImporteRec As Double = 0, Optional ByVal dImportePedido As Double = 0,
                                  Optional ByVal bSalidaAlmacen As Boolean = False) As CLinea
        Dim objnewmember As CLinea
        objnewmember = New CLinea(mDBServer, mIsAuthenticated)

        With objnewmember
            .ID = lId
            .CantRec = dCantRec
            .Activo = Activo
            .Importe = dImporteRec
            .TipoRecepcion = iTipoRecepcion
            .ImportePedido = dImportePedido
            .SalidaAlmacen = bSalidaAlmacen
        End With

        MyBase.Add(objnewmember)
        Return objnewmember
    End Function
    ''' <summary>
    ''' Contructor de los objetos de la clase CLineas
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si est� autenticado</param>
    ''' <remarks>
    ''' Llamada desde: Todos los sitios donde instanciemos un objeto CLineas
    ''' Tiempo m�ximo: 0 sec.</remarks>
    Public Sub New(ByRef dbserver As Fullstep.FSNDataBaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
    End Sub
End Class