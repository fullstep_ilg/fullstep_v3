Imports System.Data.SqlClient

Public Class COrdenesFavoritos
    Inherits Lista(Of COrdenFavoritos)

    ''' Revisado por:blp. Fecha: 15/02/2012
    ''' <summary>
    ''' Busca todas las lineas de un pedido favorito
    ''' </summary>
    ''' <param name="sUsuario">Usuario de la aplicaci�n</param>
    ''' <param name="Pedido">Identificador del pedido</param>
    ''' <param name="Prove">Proveedor del pedido</param>
    ''' <param name="codMon">Codigo de la moneda</param>
    ''' <returns>Un DataSet con los art�culos de ese pedido</returns>
    ''' <remarks>
    ''' Llamada desde:La p�gina SelecFavoritos.aspx
    ''' Tiempo m�ximo:Depende de los art�culos que tenga, pero alrededor de 0,5 seg</remarks>
    Public Function BuscarTodasOrdenes(ByVal sUsuario As String, ByVal Pedido As Integer, ByVal Prove As String, ByVal codMon As String) As DataSet
        Dim ador As New DataSet
        ador = mDBServer.OrdenesFavoritos_BuscarTodasOrdenes(sUsuario, Pedido, Prove, codMon, "")
        If ador.Tables.Count = 0 Then
            ador.Dispose()
            Return Nothing
        Else
            Return ador
        End If
    End Function

    ''' Revisado por: blp. Fecha:15/02/2012
    ''' <summary>
    ''' Busca todas las lineas de un pedido favorito
    ''' </summary>
    ''' <param name="sUsuario">Usuario de la aplicaci�n</param>
    ''' <param name="Pedido">Identificador del pedido</param>
    ''' <param name="Prove">Proveedor del pedido</param>
    ''' <param name="codMon">Codigo de la moneda</param>
    ''' <param name="CargarLineas">Par�metro booleano que indica si se quieren cargar las lineas de pedido favorito</param>
    ''' <param name="Idioma">Idioma de la aplicacion</param>
    ''' <returns>Un DataSet con los art�culos de ese pedido</returns>
    ''' <remarks>
    ''' Llamada desde:La p�gina SelecFavoritos.aspx
    ''' Tiempo m�ximo:Depende de los art�culos que tenga, pero alrededor de 0,5 seg</remarks>
    Public Function BuscarTodasOrdenes(ByVal sUsuario As String, ByVal Pedido As Integer, ByVal Prove As String, ByVal codMon As String, ByVal Idioma As String, ByVal CargarLineas As Boolean, Optional ByVal CargarEnColeccion As Boolean = False) As DataSet
        Dim ador As New DataSet
        ador = mDBServer.OrdenesFavoritos_BuscarTodasOrdenes(sUsuario, Pedido, Prove, codMon, Idioma, CargarLineas)
        If ador.Tables(0).Rows.Count = 0 Then
            ador.Dispose()
            Return Nothing
        Else
            If CargarEnColeccion Then
                Dim ContadorOrdenes As Integer = 0
                For Each fila As DataRow In ador.Tables(0).Rows
                    With fila
                        Dim sNumPedErp As String
                        If Not String.IsNullOrEmpty(.Item("NUM_PED_ERP")) Then
                            sNumPedErp = .Item("NUM_PED_ERP")
                        Else
                            sNumPedErp = .Item("Referencia")
                        End If
                        Me.Add(.Item("id"), .Item("usu"), .Item("Den"), .Item("Anyo"), .Item("ProveCod"), .Item("Fecha"), .Item("Importe"), sNumPedErp, .Item("Obs"), .Item("Empresa"), .Item("Receptor"), .Item("Cod_Prove_Erp"), .Item("Mon"), DBNullToInteger(.Item("FAC_DIR_ENVIO")), DBNullToStr(.Item("ORGCOMPRAS")))
                    End With
                    ContadorOrdenes = ContadorOrdenes + 1
                Next
            End If
            ador.Tables(0).TableName = "ORDENESFAVORITOS"
            If CargarLineas Then
                ador.Tables(1).TableName = "LINEASPEDIDO"
                ador.Tables(2).TableName = "IMPUTACIONES"
                Dim key() As DataColumn = {ador.Tables("ORDENESFAVORITOS").Columns("ID")}
                ador.Tables("ORDENESFAVORITOS").PrimaryKey = key
                Dim keylp() As DataColumn = {ador.Tables("LINEASPEDIDO").Columns("LINEAID")}
                ador.Tables("LINEASPEDIDO").PrimaryKey = keylp
                keylp(0) = ador.Tables("LINEASPEDIDO").Columns("ORDEN")
                ador.Relations.Add("REL_ORDENES_LINEASPEDIDO", key, keylp, False)

                Dim cestaAnt As Integer
                Dim idAnt As Integer
                For Each fila As DataRow In ador.Tables("IMPUTACIONES").Rows
                    If fila.Item("FAVORITO") = cestaAnt And fila.Item("ID") = idAnt Then
                        fila.Delete()
                    Else
                        cestaAnt = fila.Item("FAVORITO")
                        idAnt = fila.Item("ID")
                    End If
                Next

                Dim keyim() As DataColumn = {ador.Tables("IMPUTACIONES").Columns("FAVORITO"), ador.Tables("IMPUTACIONES").Columns("ID")}
                ador.Tables("IMPUTACIONES").PrimaryKey = keyim
                keylp(0) = ador.Tables("LINEASPEDIDO").Columns("LINEAID")
                Dim keyrelim() As DataColumn = {ador.Tables("IMPUTACIONES").Columns("FAVORITO")}
                ador.Relations.Add("REL_FAVORITO_IMPUTACIONES", keylp, keyrelim, False)
                If CargarEnColeccion Then
                    For Each linea As DataRow In ador.Tables("LINEASPEDIDO").Rows
                        For Each orden As COrdenFavoritos In Me
                            If linea.Item("Orden") = orden.ID Then
                                If orden.Lineas Is Nothing Then
                                    orden.Lineas = New CLineasFavoritos(mDBServer, mIsAuthenticated)
                                End If
                                With linea
                                    Dim oLineaFavorito As New CLineaFavoritos(mDBServer, mIsAuthenticated)
                                    
                                    oLineaFavorito.Pres5_ImportesImputados = New Imputaciones(DBServer, mIsAuthenticated)
                                    oLineaFavorito.ID = DBNullToDec(.Item("LineaId"))
                                    oLineaFavorito.Usuario = DBNullToStr(.Item("Usu"))
                                    oLineaFavorito.Cod = DBNullToStr(.Item("CodArt"))
                                    oLineaFavorito.GMN1 = DBNullToSomething(.Item("Gmn1"))
                                    oLineaFavorito.GMN2 = DBNullToSomething(.Item("Gmn2"))
                                    oLineaFavorito.GMN3 = DBNullToSomething(.Item("Gmn3"))
                                    oLineaFavorito.GMN4 = DBNullToSomething(.Item("Gmn4"))
                                    oLineaFavorito.Generico = DBNullToInteger(.Item("GENERICO"))
                                    oLineaFavorito.Prove = DBNullToStr(.Item("Prove"))
                                    oLineaFavorito.Den = DBNullToStr(.Item("DenArt"))
                                    oLineaFavorito.ArtExt = DBNullToStr(.Item("Art_Ext"))
                                    oLineaFavorito.Dest = DBNullToStr(.Item("Destino"))
                                    oLineaFavorito.DestDen = Nothing
                                    oLineaFavorito.UP = DBNullToStr(.Item("Unidad"))
                                    oLineaFavorito.UniDen = DBNullToStr(.Item("UniDen"))
                                    oLineaFavorito.PrecUc = DBNullToDbl(.Item("PrecioUnitario"))
                                    oLineaFavorito.CantPed = DBNullToDbl(.Item("Cantidad"))
                                    oLineaFavorito.CantAdj = DBNullToDbl(.Item("CantAdjudicada"))
                                    oLineaFavorito.FC = DBNullToDbl(.Item("FC"))
                                    oLineaFavorito.Importe = DBNullToDbl(.Item("Importe"))
                                    oLineaFavorito.FecEntrega = Nothing
                                    oLineaFavorito.EntregaObl = Nothing
                                    oLineaFavorito.Cat1 = DBNullToDec(.Item("Cat1"))
                                    oLineaFavorito.Cat1Den = DBNullToStr(.Item("Cat1Den"))
                                    oLineaFavorito.Cat2 = DBNullToDec(.Item("Cat2"))
                                    oLineaFavorito.Cat2Den = DBNullToStr(.Item("Cat2Den"))
                                    oLineaFavorito.Cat3 = DBNullToDec(.Item("Cat3"))
                                    oLineaFavorito.Cat3Den = DBNullToStr(.Item("Cat3Den"))
                                    oLineaFavorito.Cat4 = DBNullToDec(.Item("Cat4"))
                                    oLineaFavorito.Cat4Den = DBNullToStr(.Item("Cat4Den"))
                                    oLineaFavorito.Cat5 = DBNullToDec(.Item("Cat5"))
                                    oLineaFavorito.Cat5Den = DBNullToStr(.Item("Cat5Den"))
                                    oLineaFavorito.Obs = DBNullToStr(.Item("Obs"))
                                    oLineaFavorito.Cat = DBNullToStr(.Item("Categoria"))
                                    oLineaFavorito.CatRama = DBNullToStr(.Item("CategoriaRama"))
                                    oLineaFavorito.Coment = DBNullToStr(.Item("obs"))
                                    oLineaFavorito.AnyoProce = DBNullToSomething(.Item("AnyoProce"))
                                    oLineaFavorito.Proce = DBNullToSomething(.Item("CodProce"))
                                    oLineaFavorito.Item = DBNullToSomething(.Item("ITem"))
                                    oLineaFavorito.LineaCatalogo = DBNullToDec(.Item("Linea"))
                                    oLineaFavorito.Adjuntos = Nothing
                                    Dim oCampo1 As New CCampo(mDBServer, mIsAuthenticated)
                                    Dim oCampo2 As New CCampo(mDBServer, mIsAuthenticated)
                                    oCampo1.ID = DBNullToDec(.Item("Campo1"))
                                    oCampo1.Den = DBNullToStr(.Item("DenCampo1"))
                                    oCampo1.Valor = DBNullToStr(.Item("Valor1"))
                                    oCampo1.Obligatorio = DBNullToDec(.Item("Obl1"))
                                    oCampo2.ID = DBNullToDec(.Item("Campo2"))
                                    oCampo2.Den = DBNullToStr(.Item("DenCampo2"))
                                    oCampo2.Valor = DBNullToStr(.Item("Valor2"))
                                    oCampo2.Obligatorio = DBNullToDec(.Item("Obl2"))
                                    oLineaFavorito.Campo1 = oCampo1
                                    oLineaFavorito.Campo2 = oCampo2
                                    oLineaFavorito.ObsAdjun = DBNullToStr(.Item("ObsAdjun"))
                                    oLineaFavorito.DescripcionVisible = Nothing
                                    oLineaFavorito.Cant_Min = DBNullToDbl(System.DBNull.Value)
                                    oLineaFavorito.ModifPrec = DBNullToDbl(.Item("MODIF_PREC"))
                                    oLineaFavorito.CentroAprovisionamiento = DBNullToStr(.Item("CENTROS"))


                                    For Each linimputacion As DataRow In linea.GetChildRows("REL_FAVORITO_IMPUTACIONES")
                                        Dim oImp As FSNServer.Imputacion = New Imputacion(DBServer, mIsAuthenticated)
                                        oImp.Id = linimputacion.Item("ID")
                                        oImp.LineaId = linimputacion.Item("CESTA")
                                        oImp.CentroCoste = New UON(DBServer, mIsAuthenticated)
                                        oImp.CentroCoste.UON1 = DBNullToSomething(linimputacion.Item("UON1"))
                                        oImp.CentroCoste.UON2 = DBNullToSomething(linimputacion.Item("UON2"))
                                        oImp.CentroCoste.UON3 = DBNullToSomething(linimputacion.Item("UON3"))
                                        oImp.CentroCoste.UON4 = DBNullToSomething(linimputacion.Item("UON4"))
                                        oImp.CentroCoste.CentroSM = New Centro_SM(DBServer, mIsAuthenticated)
                                        oImp.CentroCoste.CentroSM.Codigo = linimputacion.Item("CENTRO_SM")
                                        If Not IsDBNull(linimputacion.Item("PRES5_IMP")) Then
                                            oImp.CentroCoste.CentroSM.PartidasPresupuestarias = New PartidasPRES5(DBServer, mIsAuthenticated)
                                            Dim oPart As FSNServer.PartidaPRES5 = New PartidaPRES5(DBServer, mIsAuthenticated)
                                            oPart.NIV0 = linimputacion.Item("PRES0")
                                            oPart.NIV1 = linimputacion.Item("PRES1")
                                            oPart.NIV2 = DBNullToSomething(linimputacion.Item("PRES2"))
                                            oPart.NIV3 = DBNullToSomething(linimputacion.Item("PRES3"))
                                            oPart.NIV4 = DBNullToSomething(linimputacion.Item("PRES4"))
                                            oPart.PresupuestoId = linimputacion.Item("PRES5_IMP")
                                            oImp.Partida = oPart
                                            oImp.CentroCoste.CentroSM.PartidasPresupuestarias.Add(oPart)
                                        End If
                                        oLineaFavorito.Pres5_ImportesImputados.Add(oImp)
                                    Next
                                    'orden.Lineas.Add(DBNullToDec(.Item("LineaId")), DBNullToStr(.Item("Usu")), DBNullToStr(.Item("CodArt")), DBNullToSomething(.Item("Gmn1")), DBNullToSomething(.Item("Gmn2")), DBNullToSomething(.Item("Gmn3")), DBNullToSomething(.Item("Gmn4")), DBNullToStr(.Item("Prove")), DBNullToStr(.Item("DenArt")), DBNullToStr(.Item("Art_Ext")), DBNullToStr(.Item("Destino")), Nothing, DBNullToStr(.Item("Unidad")), DBNullToStr(.Item("UniDen")), DBNullToDbl(.Item("PrecioUnitario")), DBNullToDbl(.Item("Cantidad")), DBNullToDbl(.Item("CantAdjudicada")), DBNullToDbl(.Item("FC")), DBNullToDbl(.Item("Importe")), Nothing, Nothing, DBNullToDec(.Item("Cat1")), DBNullToStr(.Item("Cat1Den")), DBNullToDec(.Item("Cat2")), DBNullToStr(.Item("Cat2Den")), DBNullToDec(.Item("Cat3")), DBNullToStr(.Item("Cat3Den")), DBNullToDec(.Item("Cat4")), DBNullToStr(.Item("Cat4Den")), DBNullToDec(.Item("Cat5")), DBNullToStr(.Item("Cat5Den")), DBNullToStr(.Item("Obs")), DBNullToStr(.Item("Categoria")), DBNullToStr(.Item("CategoriaRama")), DBNullToStr(.Item("obs")), DBNullToSomething(.Item("AnyoProce")), DBNullToSomething(.Item("CodProce")), DBNullToSomething(.Item("ITem")), DBNullToDec(.Item("Linea")), Nothing, oCampo1, oCampo2, DBNullToStr(.Item("ObsAdjun")), Nothing, DBNullToDbl(System.DBNull.Value), DBNullToDbl(.Item("MODIF_PREC")))
                                    orden.Lineas.Add(oLineaFavorito)
                                End With
                            End If
                        Next
                    Next
                End If
            End If
            Return ador
        End If
    End Function

    ''' <summary>
    ''' Recupera los pedidos favoritos del usuario indicado.
    ''' </summary>
    ''' <param name="Usuario">C�digo del usuario</param>
    ''' <param name="NumLineas">Par�metro de entrada/salida. Indica el n� de pedidos a devolver y devuelve el n� total de pedidos existente.</param>
    ''' <returns>Un dataset con los pedidos que cumplen los criterios.</returns>
    ''' <remarks>Llamadas desde: FSEPWebPartPedidosFav.CargarPedidos</remarks>
    Public Function BuscarPorUsuario(ByVal Usuario As String, ByRef NumLineas As Integer) As DataSet
        Authenticate()
        Return DBServer.OrdenesFavoritas_GetOrdenesUsuario(Usuario, NumLineas)
    End Function

    ''' <summary>
    ''' Agrega un elemento COrdenFavoritos a la lista
    ''' </summary>
    ''' <param name="ID">Id de la orden</param>
    ''' <param name="Usuario">C�digo de usuario</param>
    ''' <param name="Den">Denominaci�n</param>
    ''' <param name="Anyo">A�o</param>
    ''' <param name="Prove">C�digo de proveedor</param>
    ''' <param name="Fecha">Fecha</param>
    ''' <param name="Importe">Importe</param>
    ''' <param name="sReferencia">Referencia</param>
    ''' <param name="sObs">Observaciones</param>
    ''' <param name="lEmpresa">Empresa</param>
    ''' <param name="sReceptor">Receptor</param>
    ''' <param name="sCodERP">C�digo ERP del proveedor</param>
    ''' <param name="codMon">C�digo de moneda</param>
    ''' <returns>El elemento COrdenFavoritos agregado</returns>
    Public Overloads Function Add(ByVal ID As Integer, ByVal Usuario As String, ByVal Den As String, ByVal Anyo As Integer, ByVal Prove As String, ByVal Fecha As Object, ByVal Importe As Double, ByVal sReferencia As Object, ByVal sObs As Object, ByVal lEmpresa As Object, ByVal sReceptor As Object, ByVal sCodERP As Object, ByVal codMon As Object, ByVal OrgCompras As String) As COrdenFavoritos

        'create a new object
        Dim objnewmember As COrdenFavoritos

        objnewmember = New COrdenFavoritos(mDBServer, mIsAuthenticated)
        With objnewmember

            .ID = ID
            .Usuario = Usuario
            .Denominacion = Den
            .Anyo = Anyo
            .Prove = Prove
            .Fecha = Fecha
            .Importe = Importe
            .Referencia = sReferencia
            .Obs = sObs

            'Antes IsMissing
            If Not IsNothing(lEmpresa) Then
                .Empresa = lEmpresa
            Else
                .Empresa = Nothing
            End If

            'Antes IsMissing
            If Not IsNothing(sReceptor) Then
                .Receptor = sReceptor
            Else
                .Receptor = Nothing
            End If

            'Antes IsMissing
            If Not IsNothing(sCodERP) Then
                .CodErp = sCodERP
            Else
                .CodErp = Nothing
            End If
            'Antes IsMissing
            If Not IsNothing(codMon) Then
                .Moneda = codMon
            Else
                .Moneda = Nothing
            End If
            .CargarAdjuntos(True)

            .OrgCompras = OrgCompras

        End With

        Me.Add(objnewmember)
        Return objnewmember

    End Function

    ''' <summary>
    ''' Agrega un elemento COrdenFavoritos a la lista
    ''' </summary>
    ''' <param name="ID">Id de la orden</param>
    ''' <param name="Usuario">C�digo de usuario</param>
    ''' <param name="Den">Denominaci�n</param>
    ''' <param name="Anyo">A�o</param>
    ''' <param name="Prove">C�digo de proveedor</param>
    ''' <param name="Fecha">Fecha</param>
    ''' <param name="Importe">Importe</param>
    ''' <param name="sReferencia">Referencia</param>
    ''' <param name="sObs">Observaciones</param>
    ''' <param name="lEmpresa">Empresa</param>
    ''' <param name="sReceptor">Receptor</param>
    ''' <param name="sCodERP">C�digo ERP del proveedor</param>
    ''' <param name="codMon">C�digo de moneda</param>
    ''' <param name="DireccionEnvioFactura">ID de la direcci�n a la que se debe enviar la factura de la orden</param>
    ''' <returns>El elemento COrdenFavoritos agregado</returns>
    Public Overloads Function Add(ByVal ID As Integer, ByVal Usuario As String, ByVal Den As String, ByVal Anyo As Integer, ByVal Prove As String, ByVal Fecha As Object, ByVal Importe As Double, ByVal sReferencia As Object, ByVal sObs As Object, ByVal lEmpresa As Object, ByVal sReceptor As Object, ByVal sCodERP As Object, ByVal codMon As Object, ByVal DireccionEnvioFactura As Integer, ByVal OrgCompras As String) As COrdenFavoritos

        'create a new object
        Dim objnewmember As COrdenFavoritos

        objnewmember = New COrdenFavoritos(mDBServer, mIsAuthenticated)
        With objnewmember

            .ID = ID
            .Usuario = Usuario
            .Denominacion = Den
            .Anyo = Anyo
            .Prove = Prove
            .Fecha = Fecha
            .Importe = Importe
            .Referencia = sReferencia
            .Obs = sObs

            'Antes IsMissing
            If Not IsNothing(lEmpresa) Then
                .Empresa = lEmpresa
            Else
                .Empresa = Nothing
            End If

            'Antes IsMissing
            If Not IsNothing(sReceptor) Then
                .Receptor = sReceptor
            Else
                .Receptor = Nothing
            End If

            'Antes IsMissing
            If Not IsNothing(sCodERP) Then
                .CodErp = sCodERP
            Else
                .CodErp = Nothing
            End If
            'Antes IsMissing
            If Not IsNothing(codMon) Then
                .Moneda = codMon
            Else
                .Moneda = Nothing
            End If
            .CargarAdjuntos(True)

            If Not IsNothing(DireccionEnvioFactura) Then _
                .DireccionEnvioFactura = DireccionEnvioFactura

            If Not IsNothing(OrgCompras) Then
                .OrgCompras = OrgCompras
            Else
                .OrgCompras = Nothing
            End If
        End With

        Me.Add(objnewmember)
        Return objnewmember

    End Function


    ''' <summary>
    ''' Constructor de la clase COrdenesFavoritos
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si est� autenticado</param>
    ''' <remarks>
    ''' Llamada desde: Todos los sitios donde se instancien objetos de esta clase
    ''' Tiempo m�ximo: 0 sec. </remarks>
    Public Sub New(ByRef dbserver As Fullstep.FSNDataBaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
    End Sub

End Class