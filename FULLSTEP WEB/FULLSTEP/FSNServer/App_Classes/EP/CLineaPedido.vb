<Serializable()>
Public Class CLineaPedido
    Inherits Security
#Region "Properties"
    Private mlId As Integer
    Private msCod As Object
    Private msGmn1 As Object
    Private msGmn2 As Object
    Private msGmn3 As Object
    Private msGmn4 As Object
    Private msProve As String
    Private msDen As String
    Private msDescripcionVisible As String
    Private msArtExt As Object
    Private msDest As String
    Private msGenerico As Integer
    Private msDestDen As String
    Private msUP As Object
    Private msNumDecUP As Nullable(Of Integer)
    Private msUniDen As Object
    Private mdPrecUc As Object
    Private mdCantPed As Object
    Private mdCantRec As Object
    Private mdCantAdj As Object
    Private mdFC As Object
    Private mdImporte As Object
    Private msEst As Object
    Private mdtFecEmision As Object
    Private mdtFecEntrega As Object
    Private mdtFecEntregaProve As Object
    Private mbEntregaObl As Object
    Private mlCat1 As Object
    Private msCat1Den As Object
    Private mlCat2 As Object
    Private msCat2Den As Object
    Private mlCat3 As Object
    Private msCat3Den As Object
    Private mlCat4 As Object
    Private msCat4Den As Object
    Private mlCat5 As Object
    Private msCat5Den As Object
    Private msObs As Object
    Private msCat As Object
    Private msCatRama As Object
    Private msComent As Object
    Private miAnyoProce As Object
    Private mlProce As Object
    Private mlItem As Object
    Private mlLineaCatalogo As Object
    Private moAdjuntos As List(Of Adjunto)
    Private moCampo1 As CCampo
    Private moCampo2 As CCampo
    Private mvObsAdjun As Object
    Private msGMN1Proce As Object
    Private iAlmacen As Integer
    Private strAlmacen As String
    Private intActivo As Integer
    Private descActivo As String
    Private mImputaciones_EP As List(Of Imputacion)
    Private mdCantMinimaPedido As Double
    Private mdCantMaximaPedido As Double
    Private mbModificarPrecio As Boolean
    Private mbModificarUnidad As Boolean
    Private moTipoArticulo As CArticulo.strTipoArticulo
    Private mTipoRecepcion As Integer
    Private mImportePedido As Double
    Private mImportePendiente As Double
    Private mGestorCod As String
    Private mDesvioRecepcion As Double
    Private msMotivoRechazo As String
    Private mbGestionable As Boolean
    Private mbModificada As Boolean
    Private mbNumLinModificado As Boolean
    Private mbNumLinea As Integer
    Private _otrosDatos As List(Of CAtributoPedido)
    Private _costes As List(Of CAtributoPedido)
    Private _descuentos As List(Of CAtributoPedido)
    Private _impuestos As List(Of CImpuestos)
    Private msCentro As String
    Private _presupuestos As New Dictionary(Of String, Object)
    Private mbEstaDeBaja As Boolean
    Private mbPluriAnual As Boolean

    <Flags()>
    Public Enum Estado As Short
        PendienteAprobar = 0
        Aceptada = 1
        Emitida = 1
        Aprobada = 1
        RecibidoParcialmente = 2
        RecibidoEnSuTotalidad = 3
        Denegada = 20
        RechazadaProveedor = 21
        PendienteValidar = 22
        EnEsperaAprobacionLimiteAdjudicacion = 23
    End Enum
    Public Property ID() As Integer
        Get
            ID = mlId
        End Get
        Set(ByVal Value As Integer)
            mlId = Value
        End Set
    End Property
    Public Property Cod() As Object
        Get
            Cod = msCod
        End Get
        Set(ByVal Value As Object)
            msCod = Value
        End Set
    End Property
    Public Property GMN1() As Object
        Get
            GMN1 = msGmn1
        End Get
        Set(ByVal Value As Object)
            msGmn1 = Value
        End Set
    End Property
    Public Property GMN2() As Object
        Get
            GMN2 = msGmn2
        End Get
        Set(ByVal Value As Object)
            msGmn2 = Value
        End Set
    End Property
    Public Property GMN3() As Object
        Get
            GMN3 = msGmn3
        End Get
        Set(ByVal Value As Object)
            msGmn3 = Value
        End Set
    End Property
    Public Property GMN4() As Object
        Get
            GMN4 = msGmn4
        End Get
        Set(ByVal Value As Object)
            msGmn4 = Value
        End Set
    End Property
    Public Property Prove() As String
        Get
            Prove = msProve
        End Get
        Set(ByVal Value As String)
            msProve = Value
        End Set
    End Property
    Public Property Den() As String
        Get
            Den = msDen
        End Get
        Set(ByVal Value As String)
            msDen = Value
        End Set
    End Property
    Public Property DescripcionVisible() As String
        Get
            DescripcionVisible = msDescripcionVisible
        End Get
        Set(ByVal Value As String)
            msDescripcionVisible = Value
        End Set
    End Property
    Public Property ArtExt() As Object
        Get
            ArtExt = msArtExt
        End Get
        Set(ByVal Value As Object)
            msArtExt = Value
        End Set
    End Property
    Public Property Generico() As Integer
        Get
            Generico = msGenerico
        End Get
        Set(ByVal Value As Integer)
            msGenerico = Value
        End Set
    End Property
    Public Property Dest() As String
        Get
            Dest = msDest
        End Get
        Set(ByVal Value As String)
            msDest = Value
        End Set
    End Property
    Public Property DestDen() As String
        Get
            DestDen = msDestDen
        End Get
        Set(ByVal Value As String)
            msDestDen = Value
        End Set
    End Property
    Public Property UP() As Object
        Get
            UP = msUP
        End Get
        Set(ByVal Value As Object)
            msUP = Value
        End Set
    End Property
    Public Property NumDecUP() As Nullable(Of Integer)
        Get
            NumDecUP = msNumDecUP
        End Get
        Set(ByVal Value As Nullable(Of Integer))
            msNumDecUP = Value
        End Set
    End Property
    Public Property UniDen() As Object
        Get
            UniDen = msUniDen
        End Get
        Set(ByVal Value As Object)
            msUniDen = Value
        End Set
    End Property
    Public Property PrecUc() As Object
        Get
            PrecUc = mdPrecUc
        End Get
        Set(ByVal Value As Object)
            mdPrecUc = Value
        End Set
    End Property
    Public Property CantPed() As Object
        Get
            CantPed = mdCantPed
        End Get
        Set(ByVal Value As Object)
            mdCantPed = Value
        End Set
    End Property
    Public Property CantRec() As Object
        Get
            CantRec = mdCantRec
        End Get
        Set(ByVal Value As Object)
            mdCantRec = Value
        End Set
    End Property
    Public Property CantAdj() As Object
        Get
            CantAdj = mdCantAdj
        End Get
        Set(ByVal Value As Object)
            mdCantAdj = Value
        End Set
    End Property
    Public Property FC() As Object
        Get
            FC = mdFC
        End Get
        Set(ByVal Value As Object)
            mdFC = Value
        End Set
    End Property
    Public Property Importe() As Object
        Get
            Importe = mdImporte
        End Get
        Set(ByVal Value As Object)
            mdImporte = Value
        End Set
    End Property
    Public Property Est() As Object
        Get
            Est = msEst
        End Get
        Set(ByVal Value As Object)
            msEst = Value
        End Set
    End Property
    Public Property FecEmision() As Object
        Get
            FecEmision = mdtFecEmision
        End Get
        Set(ByVal Value As Object)
            mdtFecEmision = Value
        End Set
    End Property
    Public Property FecEntrega() As Object
        Get
            FecEntrega = mdtFecEntrega
        End Get
        Set(ByVal Value As Object)
            mdtFecEntrega = Value
        End Set
    End Property
    Public Property FecEntregaProve() As Object
        Get
            FecEntregaProve = mdtFecEntregaProve
        End Get
        Set(ByVal Value As Object)
            mdtFecEntregaProve = Value
        End Set
    End Property
    Public Property EntregaObl() As Object
        Get
            EntregaObl = mbEntregaObl
        End Get
        Set(ByVal Value As Object)
            mbEntregaObl = Value
        End Set
    End Property
    Public Property Cat1() As Object
        Get
            Cat1 = mlCat1
        End Get
        Set(ByVal Value As Object)
            mlCat1 = Value
        End Set
    End Property
    Public Property Cat1Den() As Object
        Get
            Cat1Den = msCat1Den
        End Get
        Set(ByVal Value As Object)
            msCat1Den = Value
        End Set
    End Property
    Public Property Cat2() As Object
        Get
            Cat2 = mlCat2
        End Get
        Set(ByVal Value As Object)
            mlCat2 = Value
        End Set
    End Property
    Public Property Cat2Den() As Object
        Get
            Cat2Den = msCat2Den
        End Get
        Set(ByVal Value As Object)
            msCat2Den = Value
        End Set
    End Property
    Public Property Cat3() As Object
        Get
            Cat3 = mlCat3
        End Get
        Set(ByVal Value As Object)
            mlCat3 = Value
        End Set
    End Property
    Public Property Cat3Den() As Object
        Get
            Cat3Den = msCat3Den
        End Get
        Set(ByVal Value As Object)
            msCat3Den = Value
        End Set
    End Property
    Public Property Cat4() As Object
        Get
            Cat4 = mlCat4
        End Get
        Set(ByVal Value As Object)
            mlCat4 = Value
        End Set
    End Property
    Public Property Cat4Den() As Object
        Get
            Cat4Den = msCat4Den
        End Get
        Set(ByVal Value As Object)
            msCat4Den = Value
        End Set
    End Property
    Public Property Cat5() As Object
        Get
            Cat5 = mlCat5
        End Get
        Set(ByVal Value As Object)
            mlCat5 = Value
        End Set
    End Property
    Public Property Cat5Den() As Object
        Get
            Cat5Den = msCat5Den
        End Get
        Set(ByVal Value As Object)
            msCat5Den = Value
        End Set
    End Property
    Public Property Obs() As Object
        Get
            Obs = msObs
        End Get
        Set(ByVal Value As Object)
            msObs = Value
        End Set
    End Property
    Public Property Cat() As Object
        Get
            Cat = msCat
        End Get
        Set(ByVal Value As Object)
            msCat = Value
        End Set
    End Property
    Public Property CatRama() As Object
        Get
            CatRama = msCatRama
        End Get
        Set(ByVal Value As Object)
            msCatRama = Value
        End Set
    End Property
    Public Property Coment() As Object
        Get
            Coment = msComent
        End Get
        Set(ByVal Value As Object)
            msComent = Value
        End Set
    End Property
    Public Property AnyoProce() As Object
        Get
            AnyoProce = miAnyoProce
        End Get
        Set(ByVal Value As Object)
            miAnyoProce = Value
        End Set
    End Property
    Public Property Proce() As Object
        Get
            Proce = mlProce
        End Get
        Set(ByVal Value As Object)
            mlProce = Value
        End Set
    End Property
    Public Property Item() As Object
        Get
            Item = mlItem
        End Get
        Set(ByVal Value As Object)
            mlItem = Value
        End Set
    End Property
    Public Property LineaCatalogo() As Object
        Get
            LineaCatalogo = mlLineaCatalogo
        End Get
        Set(ByVal Value As Object)
            mlLineaCatalogo = Value
        End Set
    End Property
    Public Property Adjuntos() As List(Of Adjunto)
        Get
            Adjuntos = moAdjuntos
        End Get
        Set(ByVal Value As List(Of Adjunto))
            moAdjuntos = Value
        End Set
    End Property
    Public Property Campo1() As CCampo
        Get
            Campo1 = moCampo1
        End Get
        Set(ByVal Value As CCampo)
            moCampo1 = Value
        End Set
    End Property
    Public Property Campo2() As CCampo
        Get
            Campo2 = moCampo2
        End Get
        Set(ByVal Value As CCampo)
            moCampo2 = Value
        End Set
    End Property
    Public Property ObsAdjun() As Object
        Get
            ObsAdjun = mvObsAdjun
        End Get
        Set(ByVal Value As Object)
            mvObsAdjun = Value
        End Set
    End Property
    Public Property GMN1Proce() As Object
        Get
            GMN1Proce = msGMN1Proce
        End Get
        Set(ByVal Value As Object)
            msGMN1Proce = Value
        End Set
    End Property
    Public Property Pres5_ImportesImputados_EP() As List(Of Imputacion)
        Get
            Return mImputaciones_EP
        End Get
        Set(ByVal value As List(Of Imputacion))
            mImputaciones_EP = value
        End Set
    End Property
    Public Property IdAlmacen() As Integer
        Get
            Return iAlmacen
        End Get
        Set(ByVal value As Integer)
            iAlmacen = value
        End Set
    End Property
    Public Property Almacen() As String
        Get
            Return strAlmacen
        End Get
        Set(ByVal value As String)
            strAlmacen = value
        End Set
    End Property
    Public Property Activo() As Integer
        Get
            Return intActivo
        End Get
        Set(ByVal value As Integer)
            intActivo = value
        End Set
    End Property
    Public Property DescripcionActivo() As String
        Get
            Return descActivo
        End Get
        Set(ByVal value As String)
            descActivo = value
        End Set
    End Property
    Public Property CantMinimaPedido() As Double
        Get
            Return mdCantMinimaPedido
        End Get
        Set(ByVal value As Double)
            mdCantMinimaPedido = value
        End Set
    End Property
    Public Property CantMaximaPedido() As Double
        Get
            Return mdCantMaximaPedido
        End Get
        Set(ByVal value As Double)
            mdCantMaximaPedido = value
        End Set
    End Property
    Public Property ModificarPrecio() As Boolean
        Get
            Return mbModificarPrecio
        End Get
        Set(ByVal value As Boolean)
            mbModificarPrecio = value
        End Set
    End Property
    Public Property ModificarUnidad() As Boolean
        Get
            Return mbModificarUnidad
        End Get
        Set(ByVal value As Boolean)
            mbModificarUnidad = value
        End Set
    End Property
    ''' <summary>
    ''' Indica si a la linea se le ha modificado o no la cantidad o precio en la pagina de detalle de pedido
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Modificada() As Boolean
        Get
            Return mbModificada
        End Get
        Set(ByVal value As Boolean)
            mbModificada = value
        End Set
    End Property
    ''' <summary>
    ''' Indica si el n�mero de l�nea ha sido editado y guardado en BD
    ''' </summary>
    Public Property NumLinModificado() As Boolean
        Get
            Return mbNumLinModificado
        End Get
        Set(ByVal value As Boolean)
            mbNumLinModificado = value
        End Set
    End Property
    Public Property NumLinea() As Integer
        Get
            Return mbNumLinea
        End Get
        Set(ByVal value As Integer)
            mbNumLinea = value
        End Set
    End Property
    Public Property TipoArticulo() As CArticulo.strTipoArticulo
        Get
            Return moTipoArticulo
        End Get
        Set(ByVal value As CArticulo.strTipoArticulo)
            moTipoArticulo = value
        End Set
    End Property
    Public Property TipoRecepcion() As Integer
        Get
            Return mTipoRecepcion
        End Get
        Set(ByVal value As Integer)
            mTipoRecepcion = value
        End Set
    End Property
    Public Property ImportePedido() As Double
        Get
            Return Me.mImportePedido
        End Get
        Set(ByVal value As Double)
            Me.mImportePedido = value
        End Set
    End Property
    Public Property ImportePendiente() As Double
        Get
            Return Me.mImportePendiente
        End Get
        Set(ByVal value As Double)
            Me.mImportePendiente = value
        End Set
    End Property
    Public Property DesvioRecepcion() As Double
        Get
            Return Me.mDesvioRecepcion
        End Get
        Set(ByVal value As Double)
            Me.mDesvioRecepcion = value
        End Set
    End Property
    Public Property GestorCod() As String
        Get
            Return Me.mGestorCod
        End Get
        Set(ByVal value As String)
            Me.mGestorCod = value
        End Set
    End Property
    Private _pres1 As String
    Public Property Pres1() As String
        Get
            Return _pres1
        End Get
        Set(ByVal value As String)
            _pres1 = value
        End Set
    End Property
    Private _pres2 As String
    Public Property Pres2() As String
        Get
            Return _pres2
        End Get
        Set(ByVal value As String)
            _pres2 = value
        End Set
    End Property
    Private _pres3 As String
    Public Property Pres3() As String
        Get
            Return _pres3
        End Get
        Set(ByVal value As String)
            _pres3 = value
        End Set
    End Property
    Private _pres4 As String
    Public Property Pres4() As String
        Get
            Return _pres4
        End Get
        Set(ByVal value As String)
            _pres4 = value
        End Set
    End Property
    Private _pres1Den As String
    Public Property Pres1Den() As String
        Get
            Return _pres1Den
        End Get
        Set(ByVal value As String)
            _pres1Den = value
        End Set
    End Property
    Private _pres2Den As String
    Public Property Pres2Den() As String
        Get
            Return _pres2Den
        End Get
        Set(ByVal value As String)
            _pres2Den = value
        End Set
    End Property
    Private _pres3Den As String
    Public Property Pres3Den() As String
        Get
            Return _pres3Den
        End Get
        Set(ByVal value As String)
            _pres3Den = value
        End Set
    End Property
    Private _pres4Den As String
    Public Property Pres4Den() As String
        Get
            Return _pres4Den
        End Get
        Set(ByVal value As String)
            _pres4Den = value
        End Set
    End Property
    Private _centroAprovisionamiento As String
    Public Property CentroAprovisionamiento() As String
        Get
            Return _centroAprovisionamiento
        End Get
        Set(ByVal value As String)
            _centroAprovisionamiento = value
        End Set
    End Property
    Private _centroAprovisionamiento_Den As String
    Public Property CentroAprovisionamiento_Den() As String
        Get
            Return _centroAprovisionamiento_Den
        End Get
        Set(ByVal value As String)
            _centroAprovisionamiento_Den = value
        End Set
    End Property
    Private _idLineaPedidoAbierto As Integer
    Public Property IdLineaPedidoAbierto() As Integer
        Get
            Return _idLineaPedidoAbierto
        End Get
        Set(ByVal value As Integer)
            _idLineaPedidoAbierto = value
        End Set
    End Property
    Private _solicit As Integer
    Public Property Solicit() As Integer
        Get
            Return _solicit
        End Get
        Set(ByVal value As Integer)
            _solicit = value
        End Set
    End Property
    Private _campo_Solicit As Integer
    Public Property Campo_Solicit() As Integer
        Get
            Return _campo_Solicit
        End Get
        Set(ByVal value As Integer)
            _campo_Solicit = value
        End Set
    End Property
    Private _linea_Solicit As Integer
    Public Property Linea_Solicit() As Integer
        Get
            Return _linea_Solicit
        End Get
        Set(ByVal value As Integer)
            _linea_Solicit = value
        End Set
    End Property
#End Region
    Public Property MotivoRechazo() As String
        Get
            Return msMotivoRechazo
        End Get
        Set(ByVal value As String)
            msMotivoRechazo = value
        End Set
    End Property
    Public Property Gestionable() As Boolean
        Get
            Return mbGestionable
        End Get
        Set(ByVal value As Boolean)
            mbGestionable = value
        End Set
    End Property
    Public Property OtrosDatos() As List(Of CAtributoPedido)
        Get
            Return _otrosDatos
        End Get
        Set(ByVal value As List(Of CAtributoPedido))
            _otrosDatos = value
        End Set
    End Property
    Public Property Costes() As List(Of CAtributoPedido)
        Get
            Return _costes
        End Get
        Set(ByVal value As List(Of CAtributoPedido))
            _costes = value
        End Set
    End Property
    Public Property Descuentos() As List(Of CAtributoPedido)
        Get
            Return _descuentos
        End Get
        Set(ByVal value As List(Of CAtributoPedido))
            _descuentos = value
        End Set
    End Property
    Public Property Impuestos() As List(Of CImpuestos)
        Get
            Return _impuestos
        End Get
        Set(ByVal value As List(Of CImpuestos))
            _impuestos = value
        End Set
    End Property
    Public Property Centro As String
        Get
            Return msCentro
        End Get
        Set(value As String)
            msCentro = value
        End Set
    End Property
    Private _mon As String
    Public Property Mon() As String
        Get
            Return _mon
        End Get
        Set(ByVal value As String)
            _mon = value
        End Set
    End Property
    Public Property Presupuestos As Dictionary(Of String, Object)
        Get
            Return _presupuestos
        End Get
        Set(value As Dictionary(Of String, Object))
            _presupuestos = value
        End Set
    End Property
    Private _conRecepcionAutomatica As Boolean
    Public Property ConRecepcionAutomatica() As Boolean
        Get
            Return _conRecepcionAutomatica
        End Get
        Set(ByVal value As Boolean)
            _conRecepcionAutomatica = value
        End Set
    End Property
    Private _conPlanEntrega As Boolean
    Public Property ConPlanEntrega() As Boolean
        Get
            Return _conPlanEntrega
        End Get
        Set(ByVal value As Boolean)
            _conPlanEntrega = value
        End Set
    End Property
    Private _planEntrega As List(Of PlanEntrega)
    Public Property PlanEntrega() As List(Of PlanEntrega)
        Get
            Return _planEntrega
        End Get
        Set(ByVal value As List(Of PlanEntrega))
            _planEntrega = value
        End Set
    End Property

    Public Property EstaDeBaja() As Boolean
        Get
            Return mbEstaDeBaja
        End Get
        Set(ByVal value As Boolean)
            mbEstaDeBaja = value
        End Set
    End Property

    ''' <summary>
    ''' Constructor de un objeto de la clase CLinea
    ''' </summary>
    ''' <param name="dbserver">Servidor de la BBDD</param>
    ''' <param name="isAuthenticated">Si est� autenticado</param>
    ''' <remarks>
    ''' Llamada desde: Todos los sitios desde donde se isntancien objetos CLInea
    ''' Tiempo m�ximo = 0 sec.</remarks>
    Public Sub New(ByRef dbserver As Fullstep.FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
        Pres5_ImportesImputados_EP = New List(Of Imputacion)
        Impuestos = New List(Of CImpuestos)
        Adjuntos = New List(Of Adjunto)
        PlanEntrega = New List(Of PlanEntrega)
    End Sub
    ''' <summary>
    ''' Constructor de un objeto de la clase CLinea
    ''' </summary>
    ''' <remarks>
    ''' Llamada desde: Todos los sitios desde donde se isntancien objetos CLInea
    ''' Tiempo m�ximo = 0 sec.</remarks>
    Public Sub New()
        Pres5_ImportesImputados_EP = New List(Of Imputacion)
        Impuestos = New List(Of CImpuestos)
        Adjuntos = New List(Of Adjunto)
        PlanEntrega = New List(Of PlanEntrega)
    End Sub

    Public Function DarDeBaja(ByVal AccesoFSSM As Boolean, ByVal IdOrden As Long, ByVal sUsuario As String _
                              , ByVal dImpPedido As Double, ByVal dImpCostes As Double, ByVal dImpDescuentos As Double) As Integer
        Return DBServer.LineaPedido_DarDeBaja(mlId, AccesoFSSM, IdOrden, sUsuario, False, dImpPedido, dImpCostes, dImpDescuentos)
    End Function

    Public Function ComprobarBorrarLinea(ByVal IdOrden As Long) As Integer
        Dim iRet As Integer = DBServer.LineaPedido_ComprobarBorrar(mlId, IdOrden)
        If iRet = 0 AndAlso DBServer.Orden_ComprobarTieneHitosFacturacion(IdOrden, mlId) Then iRet = 4
        Return iRet
    End Function

    Public Function DeshacerBaja(ByVal AccesoFSSM As Boolean, ByVal IdOrden As Long, ByVal sUsuario As String _
                                 , ByVal dImpPedido As Double, ByVal dImpCostes As Double, ByVal dImpDescuentos As Double) As Integer
        Return DBServer.LineaPedido_DarDeBaja(mlId, AccesoFSSM, IdOrden, sUsuario, True, dImpPedido, dImpCostes, dImpDescuentos)
    End Function

    Public Function ComprobarDeshacerBorrarLinea(ByVal IdOrden As Long, ByVal OrdenEstado As String, ByVal TipoPedido As TipoDePedido, ByRef LineasNoDeshacer As String, ByRef LineasADeshacer As String) As Integer
        ComprobarDeshacerBorrarLinea = DBServer.LineaPedido_ComprobarDeshacerBorrarLinea(mlId, OrdenEstado, IdOrden, TipoPedido, LineasNoDeshacer, LineasADeshacer)
    End Function

    Public Property PluriAnual() As Boolean
        Get
            Return mbPluriAnual
        End Get
        Set(ByVal value As Boolean)
            mbPluriAnual = value
        End Set
    End Property
End Class