
Public Class CCategoriasN5
    Inherits Lista(Of CCategoriaN5)

    ''' <summary>
    ''' Agrega un elemento CCategoriaN5 a la lista
    ''' </summary>
    ''' <param name="Id">ID de la categor�a</param>
    ''' <param name="Cod">C�digo de la categor�a</param>
    ''' <param name="Den">Denominaci�n</param>
    ''' <param name="Cat1">ID de la categor�a de nivel 1</param>
    ''' <param name="Cat1Cod">C�digo de la categor�a de nivel 1</param>
    ''' <param name="Cat2">ID de la categor�a de nivel 2</param>
    ''' <param name="Cat2Cod">C�digo de la categor�a de nivel 2</param>
    ''' <param name="Cat3">ID de la categor�a de nivel 3</param>
    ''' <param name="Cat3Cod">C�digo de la categor�a de nivel 3</param>
    ''' <param name="Cat4">ID de la categor�a de nivel 4</param>
    ''' <param name="Cat4Cod">C�digo de la categor�a de nivel 4</param>
    ''' <param name="vObs">Observaciones</param>
    ''' <returns>El elemento CCategoriaN5 agregado</returns>
    ''' <remarks></remarks>
    Public Overloads Function Add(ByVal Id As Integer, ByVal Cod As String, ByVal Den As String, ByVal Cat1 As Integer, ByVal Cat1Cod As String, ByVal Cat2 As Integer, ByVal Cat2Cod As String, ByVal Cat3 As Integer, ByVal Cat3Cod As String, ByVal Cat4 As Integer, ByVal Cat4Cod As String, ByVal vObs As Object) As CCategoriaN5
        Dim objnewmember As New CCategoriaN5(mDBServer, mIsAuthenticated)
        With objnewmember
            .Id = Id
            .Cod = Cod
            .Den = Den
            .Cat1 = Cat1
            .Cat1Cod = Cat1Cod
            .Cat2 = Cat2
            .Cat2Cod = Cat2Cod
            .Cat3 = Cat3
            .Cat3Cod = Cat3Cod
            .Cat4 = Cat4
            .Cat4Cod = Cat4Cod
            .Obs = vObs
        End With
        Me.Add(objnewmember)
        Return objnewmember
    End Function

    ''' <summary>
    ''' Constructor de la clase CCategoriasN4
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si est� autenticado</param>
    Public Sub New(ByRef dbserver As Fullstep.FSNDataBaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
    End Sub

End Class