Option Strict Off
Option Explicit On
Imports System.Data.SqlClient
'Ahora definida como clase publica
<System.Runtime.InteropServices.ProgId("CTESError_NET.CTESError")> Public Class CTESError
    Private mvarNumError As Short
    Private mvarArg1 As Object
    Private mvarArg2 As Object
    Private mvarArg3 As Object

    Public Property NumError() As Short
        Get
            NumError = mvarNumError
        End Get
        Set(ByVal Value As Short)
            mvarNumError = Value
        End Set
    End Property



    Public Property Arg1() As Object
        Get
            Arg1 = mvarArg1
        End Get
        Set(ByVal Value As Object)
            mvarArg1 = Value
        End Set
    End Property


    Public Property Arg2() As Object
        Get
            Arg2 = mvarArg2
        End Get
        Set(ByVal Value As Object)
            mvarArg2 = Value
        End Set
    End Property


    Public Property Arg3() As Object
        Get
            Arg3 = mvarArg3
        End Get
        Set(ByVal Value As Object)
            mvarArg3 = Value
        End Set
    End Property
End Class