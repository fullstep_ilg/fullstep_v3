﻿<Serializable()> _
Public Class Security
    Friend mDBServer As FSNDatabaseServer.Root
    Friend mIsAuthenticated As Boolean = False

    ''' <summary>
    ''' Procedimiento que instancia un nuevo objeto de la clase security
    ''' </summary>
    ''' <param name="dbserver">Servidor</param>
    ''' <param name="isAuthenticated">Si esta autenticado</param>
    ''' <remarks>
    ''' Llamada dessde: Todas las llamadas donde se instancie la clase
    ''' Tiempo mÃƒÂ¡ximo: 0 seg</remarks>
    Friend Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        mDBServer = dbserver
        mIsAuthenticated = isAuthenticated
    End Sub
    Friend Sub New()
    End Sub
    Friend Function DBServer() As FSNDatabaseServer.Root
        If mDBServer Is Nothing Then
            mDBServer = New FSNDatabaseServer.Root
        End If
        Return mDBServer
    End Function
    Friend Sub Authenticate()
        If Not mIsAuthenticated Then Throw New System.Security.SecurityException("User not authenticated!")
    End Sub
End Class