﻿Imports System.Security.Principal
Imports System.Threading
Imports Fullstep.FSNLibrary
Imports System.Web

<Serializable()> _
Public Class User
    Inherits Security
    Implements IPrincipal
#Region "Constructor"
    Private mUser As UserIdentity

    Public ReadOnly Property Identity() As System.Security.Principal.IIdentity Implements System.Security.Principal.IPrincipal.Identity
        Get
            Return mUser
        End Get
    End Property
    Public Function IsInRole(ByVal Action As String) As Boolean Implements System.Security.Principal.IPrincipal.IsInRole
        'La usaremos realmente para decir si contiene una acción de 
        ' seguridad específica
        Return mUser.HasAction(Action)
    End Function
    ''' <summary>
    ''' Constructor de la clase User
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="UserCode">Codigo de usuario</param>
    ''' <param name="isAuthenticated">Si está autenticado</param>
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal UserCode As String, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
        mUser = New UserIdentity(dbserver, UserCode, isAuthenticated)
        ResultadoLogin = TipoResultadoLogin.LoginOk
    End Sub
    ''' <summary>Constructor</summary>    
    ''' <param name="oResultadoLogin">Resultado de hacer el login</param>
    Public Sub New(ByVal oResultadoLogin As TipoResultadoLogin)
        MyBase.New(Nothing, False)
        ResultadoLogin = oResultadoLogin
    End Sub
#End Region
#Region "Accesos"
    'Los Dictionary, Collection etc necesitan una key y un valor. 
    'En nuestro caso solo necesitamos un array de enteros y pregutar
    'si esta o no ese valor. Para eso utilizamos el HashSet, que utiliza
    'el valor como key
    Private _Accesos As New HashSet(Of Integer)
    Private Function GetAcceso(ByVal key As Integer) As Object
        Return _Accesos.Contains(key)
    End Function
    Public ReadOnly Property AccesoSG() As Boolean
        Get
            AccesoSG = GetAcceso(TipoAccesoModulos.FSSG)
        End Get
    End Property
    Public ReadOnly Property AccesoGS() As Boolean
        Get
            AccesoGS = GetAcceso(TipoAccesoModulos.FSGS)
        End Get
    End Property
    Public ReadOnly Property AccesoPM() As Boolean
        Get
            AccesoPM = GetAcceso(TipoAccesoModulos.FSPM)
        End Get
    End Property
    Public ReadOnly Property AccesoQA() As Boolean
        Get
            AccesoQA = GetAcceso(TipoAccesoModulos.FSQA)
        End Get
    End Property
    Public ReadOnly Property AccesoEP() As Boolean
        Get
            AccesoEP = GetAcceso(TipoAccesoModulos.FSEP)
        End Get
    End Property
    Public ReadOnly Property AccesoINT() As Boolean
        Get
            AccesoINT = GetAcceso(TipoAccesoModulos.FSIS)
        End Get
    End Property
    Public ReadOnly Property AccesoFACT() As Boolean
        Get
            AccesoFACT = GetAcceso(TipoAccesoModulos.FSIM)
        End Get
    End Property
    Public ReadOnly Property AccesoContratos() As Boolean
        Get
            AccesoContratos = GetAcceso(TipoAccesoModulos.FSCM)
        End Get
    End Property
    Public Property AccesoCN() As Boolean
        Get
            AccesoCN = GetAcceso(TipoAccesoModulos.FSCN)
        End Get
        Set(ByVal value As Boolean)
            If value Then
                _Accesos.Add(TipoAccesoModulos.FSCN)
            Else
                _Accesos.Remove(TipoAccesoModulos.FSCN)
            End If
        End Set
    End Property
    Public ReadOnly Property AccesoSM() As Boolean
        Get
            AccesoSM = GetAcceso(TipoAccesoModulos.FSSM)
        End Get
    End Property
    Public ReadOnly Property AccesoBI() As Boolean
        Get
            AccesoBI = GetAcceso(TipoAccesoModulos.FSBI)
        End Get
    End Property
    Public ReadOnly Property AccesoFSAL() As Boolean
        Get
            AccesoFSAL = GetAcceso(TipoAccesoModulos.FSAL)
        End Get
    End Property
    Public ReadOnly Property INTRestriccion_RelanzarIntegracion() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.INTRelanzarIntegracion)
        End Get
    End Property
    Public ReadOnly Property AccesoNotificacionesEP() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.EPPermitirVisorNotificaciones)
        End Get
    End Property
    Public ReadOnly Property AccesoNotificacionesPM() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.PMPermitirVisorNotificaciones)
        End Get
    End Property
    Public ReadOnly Property AccesoNotificacionesGS() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.GSPermitirVisorNotificaciones)
        End Get
    End Property
    Public ReadOnly Property AccesoNotificacionesQA() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.QAPermitirVisorNotificaciones)
        End Get
    End Property
    Public ReadOnly Property AccesoNotificacionesIM() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.IMPermitirVisorNotificaciones)
        End Get
    End Property
    Public ReadOnly Property AccesoVisorNotificaciones() As Boolean
        Get
            Return AccesoNotificacionesEP Or AccesoNotificacionesQA Or
                AccesoNotificacionesGS Or AccesoNotificacionesIM Or
                AccesoNotificacionesPM Or AccesoNotificacionesCM
        End Get
    End Property
    Private _bEsPeticionarioSolicitudesQA As Boolean
    Public ReadOnly Property EsPeticionarioSolicitudesQA As Boolean
        Get
            Return _bEsPeticionarioSolicitudesQA
        End Get
    End Property
    Private _bEsParticipanteSolicitudesQA As Boolean
    Public ReadOnly Property EsParticipanteSolicitudesQA As Boolean
        Get
            Return _bEsParticipanteSolicitudesQA
        End Get
    End Property
    Private _bEsPeticionarioEncuestas As Boolean
    Public ReadOnly Property EsPeticionarioEncuestas As Boolean
        Get
            Return _bEsPeticionarioEncuestas
        End Get
    End Property
    Private _bEsParticipanteEncuestas As Boolean
    Public ReadOnly Property EsParticipanteEncuestas As Boolean
        Get
            Return _bEsParticipanteEncuestas
        End Get
    End Property
    Public Property ResultadoLogin As TipoResultadoLogin
    ''' <summary>
    ''' Devuelve un listado con las entidades de notificación a las que tiene acceso el usuario
    ''' </summary>
    ''' <returns>colección de Entidades de notificación</returns>
    ''' <remarks></remarks>
    Public Function getEntidadesNotificacion() As IList(Of Integer)
        Dim entidades As New List(Of Integer)
        If AccesoNotificacionesQA Then
            entidades.Add(EntidadNotificacion.Calificaciones)
            entidades.Add(EntidadNotificacion.Certificado)
            entidades.Add(EntidadNotificacion.NoConformidad)
            entidades.Add(EntidadNotificacion.EscalacionProveedores)
            entidades.Add(EntidadNotificacion.Encuesta)
            entidades.Add(EntidadNotificacion.SolicitudQA)
        End If

        If AccesoNotificacionesEP Then
            entidades.Add(EntidadNotificacion.Orden)
            entidades.Add(EntidadNotificacion.Recepcion)
        End If

        If AccesoNotificacionesCM Then
            entidades.Add(EntidadNotificacion.Contrato)
        End If

        If AccesoNotificacionesIM Then
            entidades.Add(EntidadNotificacion.AutoFactura)
            entidades.Add(EntidadNotificacion.Factura)
        End If

        If AccesoNotificacionesGS Then
            entidades.Add(EntidadNotificacion.ProcesoCompra)
            entidades.Add(EntidadNotificacion.PedDirecto)
        End If

        If AccesoNotificacionesGS Or Me.AccesoNotificacionesPM Then
            entidades.Add(EntidadNotificacion.Solicitud)
        End If

        If AccesoNotificacionesSM Then
            entidades.Add(EntidadNotificacion.ControlPresupuestario)
        End If

        Return entidades
    End Function
#End Region
#Region "Acciones"
    Private _AccionesUsuario As New HashSet(Of Integer)
    Public ReadOnly Property AccionUsuario(ByVal accion As Integer) As Boolean
        Get
            Return _AccionesUsuario.Contains(accion)
        End Get
    End Property
#End Region
#Region "Propiedades"
    Private d_FECPWD As Date
    Public Property FechaPassword() As Date
        Get
            Return d_FECPWD
        End Get
        Set(ByVal value As Date)
            d_FECPWD = value
        End Set
    End Property
    Private msCod As String
    Public Property Cod() As String
        Get
            Cod = msCod
        End Get
        Set(ByVal Value As String)
            msCod = Value
        End Set
    End Property
    Private msPassword As String
    Public Property Password() As String
        Get
            Password = msPassword
        End Get
        Set(ByVal Value As String)
            msPassword = Value
        End Set
    End Property
    Private msNombre As String
    Public Property Nombre() As String
        Get
            Return msNombre
        End Get
        Set(ByVal Value As String)
            msNombre = Value

        End Set
    End Property
    Private msCodPersona As String
    Public Property CodPersona() As String
        Get
            CodPersona = msCodPersona
        End Get
        Set(ByVal Value As String)
            msCodPersona = Value
        End Set
    End Property
    Private msThousanFmt As String
    Public Property ThousanFmt() As String
        Get
            ThousanFmt = msThousanFmt
        End Get
        Set(ByVal Value As String)
            msThousanFmt = Value

        End Set
    End Property
    Private msDecimalFmt As String
    Public Property DecimalFmt() As String
        Get
            If msDecimalFmt = Nothing Then
                msThousanFmt = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberGroupSeparator
                msDecimalFmt = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator
            End If
            DecimalFmt = msDecimalFmt
        End Get
        Set(ByVal Value As String)
            msDecimalFmt = Value
        End Set
    End Property
    Private miPrecisionFmt As Integer
    Public Property PrecisionFmt() As String
        Get
            PrecisionFmt = miPrecisionFmt
        End Get
        Set(ByVal Value As String)
            miPrecisionFmt = Value
        End Set
    End Property
    Private msDateFmt As String
    Public Property DateFmt() As String
        Get
            DateFmt = msDateFmt
        End Get
        Set(ByVal Value As String)
            msDateFmt = Value
        End Set
    End Property
    Private moNumberFormat As System.Globalization.NumberFormatInfo
    Public Property NumberFormat() As System.Globalization.NumberFormatInfo
        Get
            NumberFormat = moNumberFormat
        End Get
        Set(ByVal Value As System.Globalization.NumberFormatInfo)
            moNumberFormat = Value
        End Set
    End Property
    Private moDateFormat As System.Globalization.DateTimeFormatInfo
    Public Property DateFormat() As System.Globalization.DateTimeFormatInfo
        Get
            Return moDateFormat
        End Get
        Set(ByVal Value As System.Globalization.DateTimeFormatInfo)
            moDateFormat = Value
        End Set
    End Property
    Private msMon As String
    Public Property Mon() As String
        Get
            Mon = msMon
        End Get
        Set(ByVal Value As String)
            msMon = Value
        End Set
    End Property
    Private miTipoEmail As Integer
    Public Property TipoEmail() As Integer
        Get
            TipoEmail = miTipoEmail

        End Get
        Set(ByVal Value As Integer)
            miTipoEmail = Value
        End Set
    End Property
    Private msIdioma As FSNLibrary.Idioma
    Public Property Idioma() As FSNLibrary.Idioma
        Get
            Idioma = msIdioma
        End Get
        Set(ByVal Value As FSNLibrary.Idioma)
            msIdioma = Value
        End Set
    End Property

    Private _refCultural As String
    Public Property RefCultural() As String
        Get
            Return Me._refCultural
        End Get
        Set(ByVal value As String)
            Me._refCultural = value
            'provisional... algunas funciones de cn toman la ref del idioma.
            msIdioma.refCultural = value
        End Set
    End Property
    Private msEmail As String
    Public Property Email() As String
        Get
            Email = msEmail
        End Get
        Set(ByVal Value As String)
            msEmail = Value
        End Set
    End Property
    Private m_lPyme As Long
    Public Property Pyme() As Long
        Get
            Pyme = m_lPyme
        End Get
        Set(ByVal Value As Long)
            m_lPyme = Value
        End Set
    End Property
    Private msUON1 As String
    Public Property UON1() As String
        Get
            Return msUON1
        End Get
        Set(ByVal Value As String)
            msUON1 = Value
        End Set
    End Property
    Private msUON2 As String
    Public Property UON2() As String
        Get
            Return msUON2
        End Get
        Set(ByVal Value As String)
            msUON2 = Value
        End Set
    End Property
    Private msUON3 As String
    Public Property UON3() As String
        Get
            Return msUON3
        End Get
        Set(ByVal Value As String)
            msUON3 = Value
        End Set
    End Property
    Private msDep As String 'Añadido 6-4-2006 Version Qa 31000
    Public Property Dep() As String
        Get
            Return msDep
        End Get
        Set(ByVal Value As String)
            msDep = Value
        End Set
    End Property
    Public ReadOnly Property IdiomaCod() As String
        Get
            Return msIdioma.ToString
        End Get
    End Property
    Public ReadOnly Property Idioma_CodigoUniversal() As String
        Get
            Return Split(Me.RefCultural, "-")(0)
        End Get
    End Property
    Private msCentroLCX As String 'AÃ±adido para AutoLogin LCX '
    Public Property CentroLCX() As String
        Get
            Return msCentroLCX
        End Get
        Set(ByVal Value As String)
            msCentroLCX = Value
        End Set
    End Property
    Private msPerfilLCX As String 'AÃ±adido para AutoLogin LCX '
    Public Property PerfilLCX() As String
        Get
            PerfilLCX = msPerfilLCX
        End Get
        Set(ByVal Value As String)
            msPerfilLCX = Value
        End Set
    End Property
    Private miEmpresaLCX As Integer 'AÃ±adido para AutoLogin LCX '
    Public Property EmpresaLCX() As Integer
        Get
            EmpresaLCX = miEmpresaLCX
        End Get
        Set(ByVal Value As Integer)
            miEmpresaLCX = Value
        End Set
    End Property
#End Region
#Region "Propiedades GS"
    Public ReadOnly Property GSRestriccion_ProceCompra_AsignadoUsuario() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.GSEstProceRestComp)
        End Get
    End Property
    Public ReadOnly Property GSRestriccion_ProceCompra_AsignadoEquipo() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.GSEstProceRestEqp)
        End Get
    End Property
    Public ReadOnly Property GSRestriccion_ProceCompra_ResponsableUsuario() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.GSEstProceRestCompResp)
        End Get
    End Property
    Public ReadOnly Property GSRestriccion_ProceCompra_MaterialUsu() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.GSEstProceRestMatComp)
        End Get
    End Property
    Public ReadOnly Property GSRestriccion_ProceCompra_AbiertoPorUsuario() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.GSEstProceRestUsuAper)
        End Get
    End Property
    Public ReadOnly Property GSRestriccion_ProceCompra_UnidadOrganizativaUsuario() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.GSEstProceRestUsuUON)
        End Get
    End Property
    Public ReadOnly Property GSRestriccion_ProceCompra_DepartamentoUsuario() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.GSEstProceRestUsuDep)
        End Get
    End Property
    Public ReadOnly Property GSRestriccion_ProceCompra_UONsPerfilUsuario() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.GSestProceRestUsuPerfUON)
        End Get
    End Property
#End Region
#Region "Propiedades PM"
    Public ReadOnly Property PMPresUO() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.PMRestringirConceptosPresupuestariosUOUsuario)
        End Get
    End Property
    Public ReadOnly Property PMPermitirTraslados() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.PMPermitirTraslados)
        End Get
    End Property
    Public ReadOnly Property PMRestringirPersonasUOUsuario() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.PMRestringirPersonasUOUsuario)
        End Get
    End Property
    Public ReadOnly Property PMRestringirPersonasDepartamentoUsuario() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.PMRestringirPersonasDepartamentoUsuario)
        End Get
    End Property
    Public ReadOnly Property FSPMRestrMatUsu() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.PMRestringirMaterialAsignadoUsuario)
        End Get
    End Property
    Public ReadOnly Property PMRestriccionDestinosUOUsu() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.PMRestringirDestinosUOUsuario)
        End Get
    End Property
    Public ReadOnly Property PMRestricProveMat() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.PMRestringirProveedoresMaterialUsuario)
        End Get
    End Property
    'Restringir las solicitudes referenciadas a las abiertas en la UO del usuario
    Public ReadOnly Property PMRestRefAbiertasUO() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.PMRestringirSolicitudesReferenciadasAbiertasUOUsuario)
        End Get
    End Property
    Public ReadOnly Property PMRestringirEmpresasUSU() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.PMRestringirEmpresaUSU)
        End Get
    End Property
    Public ReadOnly Property PMRestringirEmpresasPerfilUSU() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.PMRestringirEmpresasPerfilUSU)
        End Get
    End Property
    Public ReadOnly Property FSPMVisualizar_Ult_ADJ() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.PMPermitirVerPreciosUltimasAdjudicacionesArticulos)
        End Get
    End Property

    'ESCENARIOS
    Public ReadOnly Property PMPermitirCrearEscenarios() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.PMPermitirCrearEscenarios)
        End Get
    End Property
    Public ReadOnly Property PMPermitirCrearEscenariosOtrosUsuarios() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.PMPermitirCrearEscenariosOtrosUsuarios)
        End Get
    End Property
    Public ReadOnly Property PMPermitirEditarEscenariosCompartidosNoCreadosPorUsuario() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.PMPermitirEditarEscenariosCompartidosNoCreadosPorUsuario)
        End Get
    End Property
    Public ReadOnly Property PMOcultarEscenarioDefecto() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.PMOcultarEscenarioDefecto)
        End Get
    End Property
    Public ReadOnly Property PMRestringirCrearEscenariosUsuariosUONUsu() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.PMRestringirCrearEscenariosUsuariosUONUsu)
        End Get
    End Property
    Public ReadOnly Property PMRestringirCrearEscenariosUsuariosUONsPerfilUsu() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.PMRestringirCrearEscenariosUsuariosUONsPerfilUsu)
        End Get
    End Property
    Public ReadOnly Property PMRestringirCrearEscenariosUsuariosDEPUsu() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.PMRestringirCrearEscenariosUsuariosDEPUsu)
        End Get
    End Property
    Public ReadOnly Property PMRestringirEdicionEscenariosUONUsu() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.PMRestringirEdicionEscenariosUONUsu)
        End Get
    End Property
    Public ReadOnly Property PMRestringirEdicionEscenariosUONsPerfilUsu() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.PMRestringirEdicionEscenariosUONsPerfilUsu)
        End Get
    End Property
    Public ReadOnly Property PMRestringirEdicionEscenariosDEPUsu() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.PMRestringirEdicionEscenariosDEPUsu)
        End Get
    End Property
    Public ReadOnly Property PMPermitirCrearEscenariosProve() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.PMPermitirCrearEscenariosProveedores)
        End Get
    End Property
    'OTROS
    Private mlPres1 As Object
    Public Property Pres1() As Object
        Get
            Return mlPres1
        End Get
        Set(ByVal Value As Object)
            mlPres1 = Value
        End Set
    End Property
    Private mlPres2 As Object
    Public Property Pres2() As Object
        Get
            Return mlPres2
        End Get
        Set(ByVal Value As Object)
            mlPres2 = Value
        End Set
    End Property
    Private mlPres3 As Object
    Public Property Pres3() As Object
        Get
            Return mlPres3
        End Get
        Set(ByVal Value As Object)
            mlPres3 = Value
        End Set
    End Property
    Private mlPres4 As Object
    Public Property Pres4() As Object
        Get
            Return mlPres4
        End Get
        Set(ByVal Value As Object)
            mlPres4 = Value
        End Set
    End Property
    Private m_bPMAnyadirProvFav As Boolean
    Public Property PMAnyadirProvFav() As Boolean
        Get
            PMAnyadirProvFav = m_bPMAnyadirProvFav
        End Get
        Set(ByVal Value As Boolean)
            m_bPMAnyadirProvFav = Value
        End Set
    End Property
    Private m_bPMMostrarDefProvMat As Boolean
    Public Property PMMostrarDefProvMat() As Boolean
        Get
            PMMostrarDefProvMat = m_bPMMostrarDefProvMat
        End Get
        Set(ByVal Value As Boolean)
            m_bPMMostrarDefProvMat = Value
        End Set
    End Property
    Private m_bPMAnyadirPresFav(3) As Boolean
    Public Property PMAnyadirPresFav(ByVal iTipo As Integer)
        Get
            PMAnyadirPresFav = m_bPMAnyadirPresFav(iTipo - 1)
        End Get
        Set(ByVal Value)
            m_bPMAnyadirPresFav(iTipo - 1) = Value
        End Set
    End Property
    Private m_bPMMostarProcesosPedidos As Boolean
    Public Property PMMostrarProcesosPedidos() As Boolean
        Get
            PMMostrarProcesosPedidos = m_bPMMostarProcesosPedidos
        End Get
        Set(ByVal Value As Boolean)
            m_bPMMostarProcesosPedidos = Value
        End Set
    End Property
    Private m_sPMCentroCosto As String
    Public Property PMCentroCosto() As String
        Get
            PMCentroCosto = m_sPMCentroCosto
        End Get
        Set(ByVal Value As String)
            m_sPMCentroCosto = Value
        End Set
    End Property
    Private m_iPMNumeroFilas As Integer
    Public Property PMNumeroFilas() As Integer
        Get
            PMNumeroFilas = m_iPMNumeroFilas
        End Get
        Set(ByVal Value As Integer)
            m_iPMNumeroFilas = Value
        End Set
    End Property
    Public ReadOnly Property PMRestriccionUONSolicitudAUONUsuario() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.PMRestringirUONSolicitudAUONUsuario)
        End Get
    End Property
    Public ReadOnly Property PMRestriccionUONSolicitudAUONPerfil() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.PMRestringirUONSolicitudAUONPerfil)
        End Get
    End Property
    Private mbPMAnyadirArticuloFav As Boolean
    Public Property PMAnyadirArticuloFav As Boolean
        Get
            PMAnyadirArticuloFav = mbPMAnyadirArticuloFav
        End Get
        Set(value As Boolean)
            mbPMAnyadirArticuloFav = value
        End Set
    End Property
    Private mbPMMostrarDefArticuloMat As Boolean
    Public Property PMMostrarDefArticuloMat As Boolean
        Get
            PMMostrarDefArticuloMat = mbPMMostrarDefArticuloMat
        End Get
        Set(value As Boolean)
            mbPMMostrarDefArticuloMat = value
        End Set
    End Property
#End Region
#Region "Propiedades EP"
    Public ReadOnly Property PedidoLibre() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.EPPermitirRealizarPedidosLibre)
        End Get
    End Property
    ''' <summary>
    ''' Permiso â€œRestringir los pedidos a la empresa del usuarioâ€ 
    ''' 
    ''' Tanto el stored FSEP_DEVOLVER_EMPRESAS_PERSONA (empresas accesibles para el usuario) como el FSSM_PARTIDAS_USU (centros accesibles para el usuario) 
    ''' interpretan el permiso â€œRestringir los pedidos a la empresa del usuarioâ€ de la siguiente manera:
    ''' @PED_OTRAS_EMPRESAS = 1
    ''' --En este caso, mandan los centros de coste, es decir, hay que tener en cuenta los permisos
    ''' --de imputaciÃ³n definidos en el SM para el usuario
    ''' --Para cada rango de uons al que el usuario pueda imputar (presente en la tabla USU_CC_IMPUTACION),
    ''' --mostraremos todas las empresas asociadas a alguna de las UONS desde el nivel 1 (UON1) al 4
    ''' @PED_OTRAS_EMPRESAS = 0
    ''' --Si es obligatorio SM y es obligatorio restringir a la empresa del usuario
    ''' --entonces, sÃ³lo puede salir la empresa del usuario (aquella de la que cuelgue. Si cuelga de mÃ¡s de una, la mÃ¡s cercana)
    ''' --siempre y cuando tenga algÃºn centro de coste a ese nivel o por debajo al que pueda imputar (es decir, que desde la posiciÃ³n de la empresa
    ''' --hacia abajo haya al menos un centro de coste imputable)
    ''' 
    ''' Por eso se pone NOT. Para q NO restringir saque varias empresas.
    ''' </summary>
    Public ReadOnly Property PedidosOtrasEmp() As Boolean
        Get
            Return Not AccionUsuario(AccionesDeSeguridad.EPRestringirPedidosEmpresaUsuario)
        End Get
    End Property
    Public ReadOnly Property ModificarPrecios() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.EPPermitirModificarPreciosEmisionPedido)
        End Get
    End Property
    Public ReadOnly Property AnularRecepciones() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.EPPermitirAnularRecepciones)
        End Get
    End Property
    ''' <summary>
    ''' Permiso para Cerrar Ordenes
    ''' </summary>
    Public ReadOnly Property PermisoCerrarOrden() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.EPPermitirCerrarPedido)
        End Get
    End Property
    ''' <summary>
    ''' Permiso para Reabrir Ordenes
    ''' </summary>
    Public ReadOnly Property PermisoReabrirOrden() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.EPPermitirReabrirPedido)
        End Get
    End Property
    Public ReadOnly Property PermisoVerPedidosCCImputables() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.EPPermitirVerPedidosCentrosCosteUsuario)
        End Get
    End Property
    ''' <summary>
    ''' Permiso para hacer recepciones de los pedidos del centro de coste del usuario
    ''' </summary>
    Public ReadOnly Property PermisoRecepcionarPedidosCCImputables() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.EPPermitirRecepcionarPedidosCentrosCosteUsuario)
        End Get
    End Property
    ''' <summary>
    ''' Permiso para bloquear la facturación de albaranes
    ''' </summary>
    Public ReadOnly Property BloquearFacturacionAlbaranes() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.EPPermitirActivarBloqueoAlbaranesFacturacion)
        End Get
    End Property

    ''' <summary>
    ''' Permiso para restringir la emisión de pedidos a los articulos de la UON del usuario
    ''' </summary>
    Public ReadOnly Property RestringirPedidosArticulosUONUsuario() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.EPRestringirPedidosArticulosUONUsuario)
        End Get
    End Property
    ''' <summary>
    ''' Permiso para restringir la emisión de pedidos a los articulos a la Uon del perfil de usuario
    ''' </summary>
    Public ReadOnly Property RestringirPedidosArticulosUONPerfil() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.EPRestringirPedidosArticulosUONPerfil)
        End Get
    End Property
    Public ReadOnly Property EPPermitirRealizarPedidosCatalogo() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.EPPermitirRealizarPedidosCatalogo)
        End Get
    End Property
    'Permisos Pedidos abiertos
    Public ReadOnly Property EPPermitirEmitirPedidosDesdePedidoAbierto() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.EPPermitirEmitirPedidosDesdePedidoAbierto)
        End Get
    End Property
    Public ReadOnly Property EPRestringirEmisionPedidosAbiertosEmpresaUsu() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.EPRestringirEmisionPedidosAbiertosEmpresaUsu)
        End Get
    End Property
    Public ReadOnly Property EPRestringirEmisionPedidosAbiertosEmpresasUONsPerfil() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.EPRestringirEmisionPedidosAbiertosEmpresasUONsPerfil)
        End Get
    End Property
    Public ReadOnly Property EPPermitirSeleccionarPresupuestosConcepto1UONUsu() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.EPPermitirSeleccionarPresupuestosConcepto1UONUsu)
        End Get
    End Property
    Public ReadOnly Property EPPermitirSeleccionarPresupuestosConcepto2UONUsu() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.EPPermitirSeleccionarPresupuestosConcepto2UONUsu)
        End Get
    End Property
    Public ReadOnly Property EPPermitirSeleccionarPresupuestosConcepto3UONUsu() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.EPPermitirSeleccionarPresupuestosConcepto3UONUsu)
        End Get
    End Property
    Public ReadOnly Property EPPermitirSeleccionarPresupuestosConcepto4UONUsu() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.EPPermitirSeleccionarPresupuestosConcepto4UONUsu)
        End Get
    End Property
    Public ReadOnly Property EPPermitirSeleccionarPresupuestosConcepto1UONsPorEncimaUsu() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.EPPermitirSeleccionarPresupuestosConcepto1UONsPorEncimaUsu)
        End Get
    End Property
    Public ReadOnly Property EPPermitirSeleccionarPresupuestosConcepto2UONsPorEncimaUsu() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.EPPermitirSeleccionarPresupuestosConcepto2UONsPorEncimaUsu)
        End Get
    End Property
    Public ReadOnly Property EPPermitirSeleccionarPresupuestosConcepto3UONsPorEncimaUsu() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.EPPermitirSeleccionarPresupuestosConcepto3UONsPorEncimaUsu)
        End Get
    End Property
    Public ReadOnly Property EPPermitirSeleccionarPresupuestosConcepto4UONsPorEncimaUsu() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.EPPermitirSeleccionarPresupuestosConcepto4UONsPorEncimaUsu)
        End Get
    End Property
    Public ReadOnly Property EPPermitirSeleccionarPresupuestosConcepto1UONsPerfil() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.EPPermitirSeleccionarPresupuestosConcepto1UONsPerfil)
        End Get
    End Property
    Public ReadOnly Property EPPermitirSeleccionarPresupuestosConcepto2UONsPerfil() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.EPPermitirSeleccionarPresupuestosConcepto2UONsPerfil)
        End Get
    End Property
    Public ReadOnly Property EPPermitirSeleccionarPresupuestosConcepto3UONsPerfil() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.EPPermitirSeleccionarPresupuestosConcepto3UONsPerfil)
        End Get
    End Property
    Public ReadOnly Property EPPermitirSeleccionarPresupuestosConcepto4UONsPerfil() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.EPPermitirSeleccionarPresupuestosConcepto4UONsPerfil)
        End Get
    End Property
    Public ReadOnly Property EPRestringirSeleccionCentroAprovUsuario() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.EPRestringirSeleccionCentroAprovUsuario)
        End Get
    End Property
    Public ReadOnly Property EPPermitirAnularPedidos() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.EPPermitirAnularPedidos)
        End Get
    End Property
    Public ReadOnly Property EPPermitirVerPedidosBorradosSeguimiento() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.EPPermitirVerPedidosBorradosSeguimiento)
        End Get
    End Property
    Public ReadOnly Property EPPermitirBorrarLineasPedido() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.EPPermitirBorrarLineasPedido)
        End Get
    End Property
    Public ReadOnly Property EPPermitirDeshacerBorradoLineasPedido() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.EPPermitirDeshacerBorradoLineasPedido)
        End Get
    End Property
	Public ReadOnly Property EPPermitirModificarPorcentajeDesvio() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.EPPermitirModificarPorcentajeDesvio)
        End Get
    End Property
    Public ReadOnly Property EPPermitirBorrarPedidos() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.EPPermitirBorrarPedidos)
        End Get
    End Property
    Public ReadOnly Property EPPermitirDeshacerBorradoPedidos() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.EPPermitirDeshacerBorradoPedidos)
        End Get
    End Property

    'OTROS
    Private miEPTipo As TipoAccesoFSEP
    Public Property EPTipo() As TipoAccesoFSEP
        Get
            EPTipo = miEPTipo
        End Get
        Set(ByVal Value As TipoAccesoFSEP)
            miEPTipo = Value
        End Set
    End Property

    Private _EsAprobadorSolicitudContraPedidoAbierto
    Public Property EsAprobadorSolicitudContraPedidoAbierto As Boolean
        Get
            Return _EsAprobadorSolicitudContraPedidoAbierto
        End Get
        Set(value As Boolean)
            _EsAprobadorSolicitudContraPedidoAbierto = value
        End Set
    End Property
    Private mbMostrarFmt As Boolean
    Public Property MostrarFMT() As Boolean
        Get
            MostrarFMT = mbMostrarFmt
        End Get
        Set(ByVal Value As Boolean)
            mbMostrarFmt = Value
        End Set
    End Property
    Private mdEquivalencia As Double
    Public Property Equivalencia() As Double
        Get
            Equivalencia = mdEquivalencia
        End Get
        Set(ByVal Value As Double)
            mdEquivalencia = Value
        End Set
    End Property
    Private msDestino As String
    Public Property Destino() As String
        Get
            Return msDestino
        End Get
        Set(ByVal value As String)
            msDestino = value
        End Set
    End Property
    Private mbMostrarAtrib As Boolean
    Public Property MostrarAtrib() As Boolean
        Get
            Return mbMostrarAtrib
        End Get
        Set(ByVal Value As Boolean)
            mbMostrarAtrib = Value
        End Set
    End Property
    Private mbMostrarCantMin As Short
    Public Property MostrarCantMin() As Boolean
        Get
            MostrarCantMin = mbMostrarCantMin
        End Get
        Set(ByVal Value As Boolean)
            mbMostrarCantMin = Value
        End Set
    End Property
    Private mbMostrarCodArt As Boolean
    Public Property MostrarCodArt() As Boolean
        Get
            MostrarCodArt = mbMostrarCodArt
        End Get
        Set(ByVal Value As Boolean)
            mbMostrarCodArt = Value
        End Set
    End Property
    Private mbMostrarCodProve As Boolean
    Public Property MostrarCodProve() As Boolean
        Get
            MostrarCodProve = mbMostrarCodProve
        End Get
        Set(ByVal Value As Boolean)
            mbMostrarCodProve = Value
        End Set
    End Property
    Private mbMostrarImagenArt As Boolean
    Public Property MostrarImagenArt() As Boolean
        Get
            MostrarImagenArt = mbMostrarImagenArt
        End Get

        Set(ByVal Value As Boolean)
            mbMostrarImagenArt = Value
        End Set
    End Property
    Private mlNumArticulosCesta As Integer
    Public Property NumArticulosCesta() As Integer
        Get
            NumArticulosCesta = mlNumArticulosCesta
        End Get
        Set(ByVal Value As Integer)
            mlNumArticulosCesta = Value
        End Set
    End Property
    Private mdImporteTotalCesta As Double
    Public Property ImporteTotalCesta() As Double
        Get
            ImporteTotalCesta = mdImporteTotalCesta
        End Get
        Set(ByVal Value As Double)
            mdImporteTotalCesta = Value
        End Set
    End Property
    Private msOrden As String
    Public Property Orden() As String
        Get
            Orden = msOrden
        End Get
        Set(ByVal Value As String)
            msOrden = Value
        End Set
    End Property
    Private msDirec As String
    Public Property Direc() As String
        Get
            Direc = msDirec
        End Get
        Set(ByVal Value As String)
            msDirec = Value
        End Set
    End Property
    Private m_bSeguimientoVerPedidosCCImputables As Boolean
    Public Property SeguimientoVerPedidosCCImputables() As Boolean
        Get
            SeguimientoVerPedidosCCImputables = m_bSeguimientoVerPedidosCCImputables
        End Get
        Set(ByVal value As Boolean)
            m_bSeguimientoVerPedidosCCImputables = value
        End Set
    End Property
    Private m_bSeguimientoVerPedidosUsuario As Boolean
    Public Property SeguimientoVerPedidosUsuario() As Boolean
        Get
            SeguimientoVerPedidosUsuario = m_bSeguimientoVerPedidosUsuario
        End Get
        Set(ByVal value As Boolean)
            m_bSeguimientoVerPedidosUsuario = value
        End Set
    End Property
    Private mbEPSM_Bar As Boolean
    Public Property EPSMBar() As Boolean
        Get
            EPSMBar = mbEPSM_Bar
        End Get
        Set(ByVal Value As Boolean)
            mbEPSM_Bar = Value
        End Set
    End Property
    Private m_bCargarCantPtesRecep As Boolean
    ''' <summary>
    ''' Cargar en la recepcion de pedidos, todas las cantidades pendientes de recepcionar en los textboxes
    ''' </summary>
    Public Property CargarCantPtesRecep() As Boolean
        Get
            Return m_bCargarCantPtesRecep
        End Get
        Set(ByVal value As Boolean)
            m_bCargarCantPtesRecep = value
        End Set
    End Property
    Private m_bRecepcionVerPedidosUsuario As Boolean
    ''' <summary>
    ''' Configuración en la pantalla Recepcion del check en el filtro para mostrar recepciones de usuario
    ''' </summary>
    Public Property RecepcionVerPedidosUsuario() As Boolean
        Get
            Return m_bRecepcionVerPedidosUsuario
        End Get
        Set(ByVal Value As Boolean)
            m_bRecepcionVerPedidosUsuario = Value
        End Set
    End Property
    Private m_bRecepcionVerPedidosCCImputables As Boolean
    ''' <summary>
    ''' Configuración en la pantalla Recepcion del check en el filtro para mostrar recepciones de los centros de coste del usuario
    ''' </summary>
    Public Property RecepcionVerPedidosCCImputables() As Boolean
        Get
            Return m_bRecepcionVerPedidosCCImputables
        End Get
        Set(ByVal Value As Boolean)
            m_bRecepcionVerPedidosCCImputables = Value
        End Set
    End Property
#End Region
#Region "Propiedades QA"
    Public ReadOnly Property QAModifProvCalidad() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.QAPermitirModificarModoTraspasoProveedoresPanelCalidad)
        End Get
    End Property
    Public ReadOnly Property QAContacAutom() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.QAPermitirModificarContactoCertificadosAutomaticos)
        End Get
    End Property
    Public ReadOnly Property QAModifNotifMateriales() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.QAPermitirModificarParametrosNotificacionesCambioMaterialGS)
        End Get
    End Property
    Public ReadOnly Property QACertifModExpiracion() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.QAPermitirModificarPeriodoAntelacionCertificadosProximosExpirar)
        End Get
    End Property
    Public ReadOnly Property QAModificarPeriodoValidezCertificados() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.QAPermitirModificarPeriodoValidezCertificadosExpirados)
        End Get
    End Property
    ''' <summary>
    ''' Devolver el valor del parametro de Seguridad de QA "Permitir modificar el parametro general de 
    ''' notificacion al proveedor por cambio de calificacion total"
    ''' </summary>
    ''' <returns>el valor del parametro </returns>
    ''' <remarks>Llamada desde: _common/parametrosGenerales.aspx; Tiempo máximo:0</remarks>
    Public ReadOnly Property QAModifNotifCalif() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.QAPermitirModificarParametroNotificacionProveedorCambioCalificacionTotal)
        End Get
    End Property
    ''' <summary>
    ''' Devolver el valor del parametro de Seguridad de QA "Permitir modificar los parámetros generales de notificación de proximidad de fechas de fin de una no conformidad"
    ''' </summary>
    ''' <returns>el valor del parametro</returns>
    ''' <remarks>Llamada desde: _common/parametrosGenerales.aspx; Tiempo máximo:0</remarks>
    Public ReadOnly Property QAModifNotifProximidadNc() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.QAPermitirModificarParametrosNotificacionProximidadFechaFinNoConformidad)
        End Get
    End Property
    Public ReadOnly Property QAPermisoModificarNivelesDefectoUNQA() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.QAPermitirModificarParametrosNotificacionProximidadFechaFinNoConformidad)
        End Get
    End Property
    Public ReadOnly Property QAPermisoModificarTipoFiltroMaterial() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.QAPermitirModificarTipoFiltroMaterial)
        End Get
    End Property
    'MANTENIMIENTOS
    '==============
    Public ReadOnly Property QAMantenimientoMat() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.QAPermitirGestionarMantenimientoMaterialesQA)
        End Get
    End Property
    Public ReadOnly Property QAMantenimientoProv() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.QAPermitirGestionarMantenimientoProveedoresQA)
        End Get
    End Property
    Public ReadOnly Property QAMantenimientoUNQA() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.QAPermitirGestionarMantenimientoUnidadesNegocioQA)
        End Get
    End Property
    Public ReadOnly Property AsigObjetivoySuelo() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.QAPermitirGestionarMantenimientoObjetivosSuelosVariablesCalidad)
        End Get
    End Property
    'PROVEEDORES
    '============
    Public ReadOnly Property QARestProvContacto() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.QARestringirProveedoresMinimoUnContactoCalidad)
        End Get
    End Property
    Public ReadOnly Property QARestProvMaterial() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.QARestringirProveedoresMaterialUsuario)
        End Get
    End Property
    Public ReadOnly Property QARestProvEquipo() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.QARestringirProveedoresEquipoUsuario)
        End Get
    End Property
    Public ReadOnly Property QAPermitirMantNivelesEscalacion() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.QAPermitirMantNivelesEscalacion)
        End Get
    End Property
    'CERTIFICADOS
    '============
    Public ReadOnly Property QAPermitirSolicitarCertificado() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.QAPermitirSolicitarCertificados)
        End Get
    End Property
    Public ReadOnly Property QAPermitirRenovarCertificado() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.QAPermitirRenovarCertificados)
        End Get
    End Property
    Public ReadOnly Property QAPermitirEnviarCertificado() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.QAPermitirEnviarCertificados)
        End Get
    End Property
    Public ReadOnly Property QACertifPubOtrosUsu() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.QAPermitirPublicarDespublicarCertificadosOtrosUsuarios)
        End Get
    End Property
    Public ReadOnly Property QACertifModifOtrosUsu() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.QAPermitirModificarDatosCertificadosOtrosUsuarios)
        End Get
    End Property
    Public ReadOnly Property QARestCertifUsu() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.QARestringirConsultaCertificadosSolicitadosUsuario)
        End Get
    End Property
    Public ReadOnly Property QARestCertifUO() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.QARestringirConsultaCertificadosSolicitadosUOUsuario)
        End Get
    End Property
    Public ReadOnly Property QARestCertifDep() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.QARestringirConsultaCertificadosSolicitadosDepartamentoUsuario)
        End Get
    End Property

    Public ReadOnly Property QARestCertifUNQAUsu() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.QARestringirConsultaCertificadosUnidadesNegocioUsuario)
        End Get
    End Property
    Public ReadOnly Property QARestCertifUNQA() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.QARestringirConsultaCertificadossUnidadesNegocio)
        End Get
    End Property

    'NO CONFORMIDAD
    '==============   
    Public ReadOnly Property QACierreNoConf() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.QAPermitirCerrarNoConformidadesConAccionesSinFinalizar)
        End Get
    End Property
    Public ReadOnly Property QAReabrirNoConf() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.QAPermitirReabrirNoConformidadesCerradas)
        End Get
    End Property
    Public ReadOnly Property FSQARevisorNoConf() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.QAPermitirRevisarCierreNoConformidades)
        End Get
    End Property
    Public ReadOnly Property QARestNoConformUsu() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.QARestringirConsultaNoConformidadesRealizadasUsuario)
        End Get
    End Property
    Public ReadOnly Property QARestNoConformUO() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.QARestringirConsultaNoConformidadesRealizadasUOUsuario)
        End Get
    End Property
    Public ReadOnly Property QARestNoConformDep() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.QARestringirConsultaNoConformidadesRealizadasDepartamentoUsuario)
        End Get
    End Property
    Public ReadOnly Property FSQARestRevisorUO() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.QARestringirSeleccionRevisoresUOUsuario)
        End Get
    End Property
    Public ReadOnly Property FSQARestRevisorDep() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.QARestringirSeleccionRevisoresDepartamentoUsuario)
        End Get
    End Property
    Public ReadOnly Property FSQARestNotificadosInternosUO() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.QARestringirSeleccionNotificadosInternosUOUsuario)
        End Get
    End Property
    Public ReadOnly Property FSQARestNotificadosInternosDep() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.QARestringirSeleccionNotificadosInternosDepartamentoUsuario)
        End Get
    End Property
    'OTROS
    '==============    
    Private m_bQAPPManual As Boolean
    Public Property QAPPManual() As Boolean
        Get
            QAPPManual = m_bQAPPManual
        End Get
        Set(ByVal Value As Boolean)
            m_bQAPPManual = Value
        End Set
    End Property
    Private m_bQAPPAuto As Boolean
    Public Property QAPPAuto() As Boolean
        Get
            QAPPAuto = m_bQAPPAuto
        End Get
        Set(ByVal Value As Boolean)
            m_bQAPPAuto = Value
        End Set
    End Property
    Private m_iQAPPNivel As Integer
    Public Property QAPPNivel() As Integer
        Get
            QAPPNivel = m_iQAPPNivel
        End Get
        Set(ByVal Value As Integer)
            m_iQAPPNivel = Value
        End Set
    End Property
    ''' <summary>
    ''' Es el nombre del grupo en el idioma del usuario
    ''' </summary>
    ''' <remarks></remarks>
    Private _NombreGruposIdiomaUsu As String
    Public Property NombreGruposIdiomaUsu() As String
        Get
            Return _NombreGruposIdiomaUsu
        End Get
        Set(ByVal value As String)
            _NombreGruposIdiomaUsu = value
        End Set
    End Property
    Private m_bQANotificarErrorCalculoPuntuaciones As Boolean
    Public Property QANotificarErrorCalculoPuntuaciones() As Boolean
        Get
            QANotificarErrorCalculoPuntuaciones = m_bQANotificarErrorCalculoPuntuaciones
        End Get
        Set(ByVal Value As Boolean)
            m_bQANotificarErrorCalculoPuntuaciones = Value
        End Set
    End Property
    Private m_bQAPuntuacionProveedores As Boolean
    Public Property QAPuntuacionProveedores() As Boolean
        'VERSION QA 3.0.600 - EPB
        Get
            QAPuntuacionProveedores = m_bQAPuntuacionProveedores
        End Get
        Set(ByVal Value As Boolean)
            m_bQAPuntuacionProveedores = Value
        End Set
    End Property
    'PUBLICAS
    '========
    Private msQAFiltroUNQA As String
    Public Property QAFiltroUNQA() As String
        Get
            QAFiltroUNQA = msQAFiltroUNQA
        End Get
        Set(ByVal Value As String)
            msQAFiltroUNQA = Value
        End Set
    End Property
    Private msQAFiltroVarCal As String
    Public Property QAFiltroVarCal() As String
        Get
            QAFiltroVarCal = msQAFiltroVarCal
        End Get
        Set(ByVal Value As String)
            msQAFiltroVarCal = Value
        End Set
    End Property
    Private msQACamposOcultos As String
    Public Property QACamposOcultos() As String
        Get
            QACamposOcultos = msQACamposOcultos
        End Get
        Set(ByVal Value As String)
            msQACamposOcultos = Value
        End Set
    End Property
    ''' Revisado por: Jbg. Fecha: 20/10/2011
    ''' <summary>
    ''' Guarda el orden de los datos del panel de calidad
    ''' </summary>
    ''' <remarks>Llamada desde: PanelCalidad.aspx.vb; Tiempo maximo:0</remarks>
    Private msQAOrdenacion As String
    Public Property QAOrdenacion() As String
        Get
            QAOrdenacion = msQAOrdenacion
        End Get
        Set(ByVal Value As String)
            msQAOrdenacion = Value
        End Set
    End Property
    Private m_bQAPanelEscalacionProveedores As Boolean
    Public Property QAPanelEscalacionProveedoresAccesible() As Boolean
        Get
            Return m_bQAPanelEscalacionProveedores
        End Get
        Set(ByVal value As Boolean)
            m_bQAPanelEscalacionProveedores = value
        End Set
    End Property
#End Region
#Region "Propiedades SM"
    Public ReadOnly Property SMConsultarActivos() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.SMConsultaActivos)
        End Get
    End Property
    ''' <summary>
    ''' Devuelve un valor Boolean que indica si el usuario puede dar de alta activos
    ''' </summary>
    ''' <returns>Valor Boolean que indica si el usuario puede dar de alta activos</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property SMAltaActivos() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.SMAltaNuevosActivos)
        End Get
    End Property
    ''' <summary>
    ''' Devuelve un valor Boolean que indica si el usuario puede modificar activos
    ''' </summary>
    ''' <returns>Valor Boolean que indica si el usuario puede modificar activos</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property SMModificarActivos() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.SMModificacionActivos)
        End Get
    End Property
    ''' <summary>
    ''' Devuelve un valor Boolean que indica si el usuario puede eliminar activos
    ''' </summary>
    ''' <returns>Valor Boolean que indica si el usuario puede eliminar activos</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property SMEliminarActivos() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.SMEliminacionActivos)
        End Get
    End Property
    ''' <summary>
    ''' Devuelve un valor Boolean que indica si el usuario puede ver las notificaciones de SM
    ''' </summary>
    ''' <returns>Valor Boolean que indica si el usuario puede ver las notificaciones de SM</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property AccesoNotificacionesSM() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.SMNotificaciones)
        End Get
    End Property

    Public ReadOnly Property SMControlPresupuestario() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.SMControlPresupuestario)
        End Get
    End Property
    Private moData As DataSet
    Public ReadOnly Property Data() As Data.DataSet
        Get
            Return moData
        End Get
    End Property
    Private moUONs As DataSet
    ReadOnly Property UONs() As DataSet
        Get
            Return moUONs
        End Get
    End Property
#End Region
#Region "Propiedades CN"
    Public ReadOnly Property CNPermisoAdministrarCategorias() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.CNPermitirOrganizarContenidosRedGestionCategorias)
        End Get
    End Property
    Public ReadOnly Property CNPermisoAdministrarGrupos() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.CNPermitirOrganizarMiembrosRedGestionGrupos)
        End Get
    End Property
    Public ReadOnly Property CNRestriccionOrganizarRed() As OrganizacionRed
        Get
            If AccionUsuario(AccionesDeSeguridad.CNRestringirGestionCategoriasGruposRedACategoriasGruposUsuario) Then
                Return OrganizacionRed.RestrUsu
            ElseIf AccionUsuario(AccionesDeSeguridad.CNRestringirGestionCategoriasGruposRedACategoriasGruposDepartamentoUsuario) Then
                Return OrganizacionRed.RestrDepUsu
            ElseIf AccionUsuario(AccionesDeSeguridad.CNRestringirGestionCategoriasGruposRedACategoriasGruposUOUsuario) Then
                Return OrganizacionRed.RestrUONUsu
            Else
                Return OrganizacionRed.NoRestr
            End If
        End Get
    End Property
    'Uso de la red de colaboración:
    Public ReadOnly Property CNPermisoCrearMensajes() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.CNUsoRedColaboracionCrearMensajes)
        End Get
    End Property
    Public ReadOnly Property CNPermisoCrearMensajesUrgentes() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.CNUsoRedColaboracionCrearMensajesUrgentes)
        End Get
    End Property
    Public ReadOnly Property CNRestriccionEnvioUON() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.CNEnvioMensajesEstructuraOrganizativaRestringirEnvioMiembrosUOUsuario)
        End Get
    End Property
    Public ReadOnly Property CNRestriccionEnvioDEP() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.CNEnvioMensajesEstructuraOrganizativaRestringirEnvioMiembrosDepartamentoUsuario)
        End Get
    End Property
    Public ReadOnly Property CNRestriccionEnvioProveMatUsu() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.CNEnvioMensajesProveedoresRestringirEnvioProveedoresMaterialUsuario)
        End Get
    End Property
    Public ReadOnly Property CNRestriccionEnvioProveEquipoComprasUsu() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.CNEnvioMensajesProveedoresRestringirEnvioProveedoresEquipoComprasUsuario)
        End Get
    End Property
    Public ReadOnly Property CNRestriccionEnvioConContactoQA() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.CNEnvioMensajesProveedoresRestringirEnvioProveedoresConContactoCalidad)
        End Get
    End Property
    Public ReadOnly Property CNRestriccion_EstrucCompras_EnvioEquipoComprasUsu() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.CNEnvioMensajesEstructuraComprasRestringirEnvioMiembrosEquipoComprasUsuario)
        End Get
    End Property
    'OTROS
    Private _EquipoUsu As String
    Public Property EquipoUsu() As String
        Get
            Return _EquipoUsu
        End Get
        Set(ByVal value As String)
            _EquipoUsu = value
        End Set
    End Property
    Private _CNGuidImagenUsu As String
    Public Property CNGuidImagenUsu() As String
        Get
            Return _CNGuidImagenUsu
        End Get
        Set(ByVal value As String)
            _CNGuidImagenUsu = value
        End Set
    End Property
    Public ReadOnly Property CNTieneImagen() As Boolean
        Get
            Return _CNGuidImagenUsu IsNot String.Empty
        End Get
    End Property
    Private _CNNotificarResumenActividad As Boolean
    Public Property CNNotificarResumenActividad() As Boolean
        Get
            Return _CNNotificarResumenActividad
        End Get
        Set(ByVal value As Boolean)
            _CNNotificarResumenActividad = value
        End Set
    End Property
    Private _CNNotificarIncluidoGrupo As Boolean
    Public Property CNNotificarIncluidoGrupo() As Boolean
        Get
            Return _CNNotificarIncluidoGrupo
        End Get
        Set(ByVal value As Boolean)
            _CNNotificarIncluidoGrupo = value
        End Set
    End Property
    Private _CNConfiguracionDesocultar As Boolean
    Public Property CNConfiguracionDesocultar() As Boolean
        Get
            Return _CNConfiguracionDesocultar
        End Get
        Set(ByVal value As Boolean)
            _CNConfiguracionDesocultar = value
        End Set
    End Property
    Private _DepDenominacion As String
    Public Property DepDenominacion() As String
        Get
            Return _DepDenominacion
        End Get
        Set(ByVal value As String)
            _DepDenominacion = value
        End Set
    End Property
#End Region
#Region "Propiedades BI"
    Public ReadOnly Property BIConfigurarCubos() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.BIConfigurarCubos)
        End Get
    End Property
    Public ReadOnly Property BIConfigurarQlikApps() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.BIConfigurarQlikApps)
        End Get
    End Property
    Public ReadOnly Property BIRestringirVisibilidadQlikAppsUONs() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.BIRestringirVisibilidadQlikAppsUONs)
        End Get
    End Property
    Public ReadOnly Property BIRestringirVisibilidadQlikAppsGMNs() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.BIRestringirVisibilidadQlikAppsGMNs)
        End Get
    End Property
#End Region
#Region "Propiedades AL"
    Public ReadOnly Property ALVisorActividad() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.ALVisorActividad)
        End Get
    End Property
#End Region
#Region "Propiedades IM"
    Public ReadOnly Property IMAltaFacturas() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.IMAltaFacturas)
        End Get
    End Property
    Public ReadOnly Property IMSeguimientoFacturas() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.IMSeguimientoFacturas)
        End Get
    End Property
    Public ReadOnly Property IMVisorAutofacturas() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.IMVisorAutofacturas)
        End Get
    End Property
#End Region
#Region "Propiedades CM"
    Public ReadOnly Property AccesoNotificacionesCM() As Boolean
        Get
            Return AccionUsuario(AccionesDeSeguridad.CMPermitirVisorNotificaciones)
        End Get
    End Property
#End Region
#Region "Métodos"
    ''' Revisado por: blp. Fecha: 31/10/2012
    ''' <summary>
    ''' Carga los datos de usuario, opciones y permisos
    ''' </summary>
    ''' <param name="sCod">Código de la persona que se ha validado en la aplicación.</param>       
    ''' <remarks>Llamada desde: FSNPage/login       Login.master/btnLogin_click; Tiempo maximo:0,2</remarks>
    Public Sub LoadUserData(ByVal sCod As String)
        Dim data As DataSet
        Dim row As DataRow

        Authenticate()
        msCod = sCod
        'Obtendremos de la tabla USU toda la información del usuario. Si este tiene un perfil asociado
        'obtendremos de la tabla PERF los accesos y de la tabla PERF_ACC las acciones que puede realizar. 
        'Si no tiene perfil asociado, obtendremos los accesos de la tabla USU y las acciones de USU_ACC
        data = DBServer.User_LoadUserData(msCod)

        For Each accionUsuario As DataRow In data.Tables(1).Rows
            _AccionesUsuario.Add(accionUsuario("ACC"))
        Next
        If Not data.Tables(2).Rows.Count = 0 AndAlso Not data.Tables(2).Rows(0)("CONTROLPRESUPUESTARIO") = 0 Then
            _AccionesUsuario.Add(AccionesDeSeguridad.SMControlPresupuestario)
        End If
        If Not data.Tables(0).Rows.Count = 0 Then
            row = data.Tables(0).Rows(0)
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'PASSWORD
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            msPassword = Encrypter.Encrypt(DBNullToSomething(row.Item("PWD")), msCod, False, Encrypter.TipoDeUsuario.Persona, row.Item("FECPWD"))
            d_FECPWD = DBNullToStr(row.Item("FECPWD"))
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'INFO
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            msNombre = DBNullToSomething(row.Item("NOMBRE"))
            msCodPersona = DBNullToSomething(row.Item("PER"))
            msIdioma = DBNullToStr(row.Item("IDIOMA"))
            RefCultural = DBNullToStr(row.Item("LANGUAGETAG"))
            msMon = DBNullToSomething(row.Item("MON"))
            msDep = DBNullToSomething(row.Item("DEP"))
            _EquipoUsu = DBNullToSomething(row("EQP"))
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'EMAIL
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            miTipoEmail = DBNullToSomething(row.Item("TIPOEMAIL"))
            msEmail = DBNullToSomething(row.Item("EMAIL"))
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'FORMATO FECHAS
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            msDateFmt = DBNullToSomething(row.Item("DATEFMT"))
            moDateFormat = New System.Globalization.CultureInfo(Me._refCultural, False).DateTimeFormat
            moDateFormat.FirstDayOfWeek = DayOfWeek.Monday
            If Not msDateFmt = Nothing Then
                moDateFormat.ShortDatePattern = Replace(msDateFmt, "mm", "MM")
                'El dateSeparator por defecto es "/"
                'Si en las opciones eliges un msDateFmt sin "/" pero no cambias el dateSeparator, a veces puede que aparezca la "/" en algunos sitios
                'Lo cambiamos desde código:
                Dim sSeparadorFecha As String = encontrarSeparadorFecha(msDateFmt)
                If Not String.IsNullOrEmpty(sSeparadorFecha) Then
                    moDateFormat.DateSeparator = sSeparadorFecha
                End If
            Else
                moDateFormat.ShortDatePattern = "dd/MM/yyyy"
            End If
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'FORMATO NUMEROS
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'FORMATO NUMEROS
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            msThousanFmt = DBNullToStr(row.Item("THOUSANFMT"))
            msDecimalFmt = DBNullToSomething(row.Item("DECIMALFMT"))
            miPrecisionFmt = DBNullToSomething(row.Item("PRECISIONFMT"))
            moNumberFormat = New System.Globalization.CultureInfo(Me._refCultural, False).NumberFormat

            If Not miPrecisionFmt = Nothing Then
                moNumberFormat.NumberDecimalDigits = miPrecisionFmt
            ElseIf miPrecisionFmt = 0 Then
                moNumberFormat.NumberDecimalDigits = miPrecisionFmt
            End If
            If Not msDecimalFmt = Nothing Then
                moNumberFormat.NumberDecimalSeparator = msDecimalFmt
            End If
            If Not msThousanFmt = Nothing Then
                moNumberFormat.NumberGroupSeparator = msThousanFmt
            End If

            'En bbdd usuario thousanfmt=null decimalfmt=null no entrara nunca en el if. Se usara la configuraciÃ³n del windows
            'thousanfmt=. decimalfmt=null   ->  moNumberFormat #.##0.00
            'thousanfmt=null decimalfmt=,   ->  moNumberFormat #,##0,00
            If moNumberFormat.NumberDecimalSeparator = moNumberFormat.NumberGroupSeparator Then
                '1234.56 mil doscientos treinta y cuatro con cincuenta y seis
                '   si formato #.##0.00 devuelve    1.23
                '   si formato #,##0,00 devuelve    1,23
                If msDecimalFmt Is Nothing Then
                    'En bbdd usuario decimalfmt=null
                    If moNumberFormat.NumberDecimalSeparator = moNumberFormat.CurrencyDecimalSeparator Then
                        moNumberFormat.NumberDecimalSeparator = moNumberFormat.CurrencyGroupSeparator
                    Else
                        moNumberFormat.NumberDecimalSeparator = moNumberFormat.CurrencyDecimalSeparator
                    End If
                Else
                    'En bbdd usuario thousanfmt=null
                    If moNumberFormat.NumberGroupSeparator = moNumberFormat.CurrencyGroupSeparator Then
                        moNumberFormat.NumberGroupSeparator = moNumberFormat.CurrencyDecimalSeparator
                    Else
                        moNumberFormat.NumberGroupSeparator = moNumberFormat.CurrencyGroupSeparator
                    End If
                End If
            End If
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'ACCESO ADMINISTRADOR
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If (msCod = FSNLibrary.Encrypter.Encrypt(row.Item("ADM"), msCod, False, FSNLibrary.Encrypter.TipoDeUsuario.Administrador, row.Item("FEC_USU"))) Then
                _Accesos.Add(TipoAccesoModulos.FSSG)
                _AccionesUsuario.Add(AccionesDeSeguridad.ALVisorActividad)
                _Accesos.Add(TipoAccesoModulos.FSAL)
            End If
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'ACCESO GS
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If row.Item("FSGS") Then _Accesos.Add(TipoAccesoModulos.FSGS)
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'ACCESO SM
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If row.Item("FSSM") Then _Accesos.Add(TipoAccesoModulos.FSSM)
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'ACCESO VISOR INTEGRACION
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If row.Item("FSIS") Then _Accesos.Add(TipoAccesoModulos.FSIS)
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'ACCESO MODULO FACTURACION
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If row.Item("FSIM") Then _Accesos.Add(TipoAccesoModulos.FSIM)
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'ACCESO CONTRATOS
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If row.Item("FSCM") Then _Accesos.Add(TipoAccesoModulos.FSCM)
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'ACCESO PM
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If row.Item("FSPM") Then _Accesos.Add(TipoAccesoModulos.FSPM)
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'ACCESO QA
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If row.Item("FSQA") OrElse (Not IsNothing(HttpContext.Current) AndAlso Not IsNothing(HttpContext.Current.Session) AndAlso HttpContext.Current.Session("desdeGS") = "1") Then _Accesos.Add(TipoAccesoModulos.FSQA)

            If AccesoPM OrElse AccesoFACT OrElse AccesoQA Then
                mlPres1 = DBNullToSomething(row.Item("FSWS_PRES1"))
                mlPres2 = DBNullToSomething(row.Item("FSWS_PRES2"))
                mlPres3 = DBNullToSomething(row.Item("FSWS_PRES3"))
                mlPres4 = DBNullToSomething(row.Item("FSWS_PRES4"))
                m_bPMAnyadirProvFav = SQLBinaryToBoolean(row.Item("ANYADIR_PROV_FAV"))
                m_bPMMostrarDefProvMat = SQLBinaryToBoolean(row.Item("MOSTRAR_DEF_PROV_MAT"))
                m_iPMNumeroFilas = DBNullToDbl(row.Item("PM_NUMFILAS"))
                For i As Integer = 0 To 3
                    m_bPMAnyadirPresFav(i) = SQLBinaryToBoolean(row.Item("PM_ANYADIR_PRES" & (i + 1).ToString() & "_FAV"))
                Next
                mbPMAnyadirArticuloFav = SQLBinaryToBoolean(row.Item("ANYADIR_ART_FAV"))
                mbPMMostrarDefArticuloMat = SQLBinaryToBoolean(row.Item("MOSTRAR_DEF_ART_MAT"))
            End If
            If AccesoQA Then
                m_bQAPPManual = SQLBinaryToBoolean(row.Item("FSQA_PP_MANUAL"))
                m_bQAPPAuto = SQLBinaryToBoolean(row.Item("FSQA_PP_AUTO"))
                m_iQAPPNivel = DBNullToDbl(row.Item("FSQA_PP_NIVEL"))
                _NombreGruposIdiomaUsu = DBNullToSomething(row.Item("GRUPOIDIOMAUSU"))
                m_bQANotificarErrorCalculoPuntuaciones = SQLBinaryToBoolean(row.Item("FSQA_NOTIFICACIONERRORCALCPUNT"))
                m_bQAPuntuacionProveedores = PanelCalidadAccesible()
                m_bQAPanelEscalacionProveedores = PanelEscalacionProveedoresAccesible()

                _bEsPeticionarioSolicitudesQA = ParticipaSolicitud(msCod, TiposDeDatos.TipoDeSolicitud.SolicitudQA, TipoParticipacionSolicitud.Peticionario)
                _bEsParticipanteSolicitudesQA = If(_bEsPeticionarioSolicitudesQA, _bEsPeticionarioSolicitudesQA, ParticipaSolicitud(msCod, TiposDeDatos.TipoDeSolicitud.SolicitudQA, TipoParticipacionSolicitud.Participante))
                _bEsPeticionarioEncuestas = ParticipaSolicitud(msCod, TiposDeDatos.TipoDeSolicitud.Encuesta, TipoParticipacionSolicitud.Peticionario)
                _bEsParticipanteEncuestas = If(_bEsPeticionarioEncuestas, _bEsPeticionarioEncuestas, ParticipaSolicitud(msCod, TiposDeDatos.TipoDeSolicitud.Encuesta, TipoParticipacionSolicitud.Participante))
            End If
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                'ACCESO EP
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                If row.Item("FSEP") Then _Accesos.Add(TipoAccesoModulos.FSEP)
            If AccesoEP Then
                mbMostrarFmt = DBNullToSomething(row.Item("MOSTRARFMT"))
                'TIPO FSEP-> 1:sólo aprovisionador; 2:sólo aprobador ; 3:aprobador y aprovisionador
                miEPTipo = DBNullToDbl(row.Item("FSEPTipo"))
                msDestino = DBNullToSomething(row.Item("DEST"))
                mbMostrarAtrib = (DBNullToSomething(row.Item("MOSTATRIB")) = 1)
                mbMostrarCantMin = (DBNullToSomething(row.Item("MOSTCANTMIN")) = 1)
                mbMostrarCodArt = (DBNullToSomething(row.Item("MOSTCODART")) = 1)
                mbMostrarCodProve = (DBNullToSomething(row.Item("MOSTCODPROVE")) = 1)
                mbMostrarImagenArt = (DBNullToSomething(row.Item("MOSTIMGART")) = 1)
                msOrden = DBNullToSomething(row.Item("ORDEN"))
                msDirec = DBNullToSomething(row.Item("DIREC"))

                mbEPSM_Bar = DBNullToSomething(row.Item("FSEP_SM_BAR"))
                m_bSeguimientoVerPedidosUsuario = SQLBinaryToBoolean(row.Item("FSEP_SEG_VER_PED_USU"))
                m_bSeguimientoVerPedidosCCImputables = SQLBinaryToBoolean(row.Item("FSEP_SEG_VER_PED_CCIMP"))
                m_bCargarCantPtesRecep = SQLBinaryToBoolean(row.Item("CARGAR_CANT_PEND_RECEP"))
                m_bRecepcionVerPedidosUsuario = SQLBinaryToBoolean(row.Item("FSEP_RECEP_VER_PED_USU"))
                m_bRecepcionVerPedidosCCImputables = SQLBinaryToBoolean(row.Item("FSEP_RECEP_VER_PED_CCIMP"))
                Dim drInfoCesta As DataRow = InfoCesta()
                mlNumArticulosCesta = DBNullToSomething(drInfoCesta.Item("NUMARTICULOS"))
                mdImporteTotalCesta = DBNullToSomething(drInfoCesta.Item("IMPORTE"))
                mdEquivalencia = DBNullToDbl(drInfoCesta.Item("EQUIV"))
                'Tipo de usuario de EP : Incidencia Pruebas 31900.9 / 2014 / 910
                If Me.EPTipo = TipoAccesoFSEP.SoloAprovisionador AndAlso DBServer.User_EsAprobadorSolicitudPedidoEP(Me.CodPersona) Then
                    EPTipo = TipoAccesoFSEP.AprobadorYAprovisionador
                End If
                'Tipo de usuario de EP : Incidencia Pruebas 31900.9 / 2014 / 1085
                If Me.EPTipo = TipoAccesoFSEP.Basico AndAlso DBServer.User_EsAprobadorSolicitudPedidoEP(Me.CodPersona) Then
                    EPTipo = TipoAccesoFSEP.SoloAprobador
                End If
                'Aprobador de pedidos abiertos
                If DBServer.User_EsAprobadorSolicitudContraPedidoAbierto(Me.CodPersona) Then
                    EsAprobadorSolicitudContraPedidoAbierto = True
                End If
            End If
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'ACCESO CN
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If AccesoCN Then
                data = DBServer.User_LoadUserDataCN(msCod)
                Dim rowCN As DataRow = data.Tables(0).Rows(0)
                _DepDenominacion = rowCN("DEPDEN")
                _CNGuidImagenUsu = rowCN("GUIDIMAGENUSU").ToString
                'Opciones de configuración
                _CNConfiguracionDesocultar = rowCN("FSCN_DESOCULTAR_AUT")
                'Opciones de notificación
                _CNNotificarResumenActividad = rowCN("FSCN_NOTIFICAR_RESUMEN_ACTIVIDAD")
                _CNNotificarIncluidoGrupo = rowCN("FSCN_NOTIFICAR_INCLUIDO_GRUPO")
            End If
            'ACCESO BI
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If row.Item("FSBI") Then _Accesos.Add(TipoAccesoModulos.FSBI)
            'ACCESO FSAL
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If row.Item("FSAL") Then
                _Accesos.Add(TipoAccesoModulos.FSAL)
                _AccionesUsuario.Add(AccionesDeSeguridad.ALVisorActividad)
            End If
        End If

        row = Nothing
        data = Nothing

        LoadUONPersona()
    End Sub
    ''' <summary>
    ''' Procedimiento que carga las unidades organizativas de la persona
    ''' </summary>
    ''' <remarks>
    ''' Llamada desde: FSNServer/User/LoadUserData
    ''' Tiempo máximo: 0,15 seg</remarks>
    Public Sub LoadUONPersona()
        Authenticate()

        Dim data As DataTable
        data = DBServer.Persona_Load(msCodPersona)

        If data.Rows.Count > 0 Then
            msUON1 = DBNullToSomething(data.Rows(0).Item("UON1"))
            msUON2 = DBNullToSomething(data.Rows(0).Item("UON2"))
            msUON3 = DBNullToSomething(data.Rows(0).Item("UON3"))
            m_lPyme = DBNullToDbl(data.Rows(0).Item("PYME"))
        End If
    End Sub
    ''' <summary>
    ''' Almacena las opciones de usuario.
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub SaveUserData()
        Authenticate()

        DBServer.User_SaveUserData(msCod, msThousanFmt, msDecimalFmt, miPrecisionFmt, msDateFmt, miTipoEmail, msIdioma)

        'recargo los datos para que se actualice el valor de culture
        Dim data As DataSet
        Dim row As DataRow
        data = DBServer.User_LoadUserData(msCod)
        row = data.Tables(0).Rows(0)
        Me.RefCultural = DBNullToStr(row.Item("LANGUAGETAG"))

        'formato de fechas:
        moDateFormat = New System.Globalization.CultureInfo(Me._refCultural, False).DateTimeFormat
        moDateFormat.FirstDayOfWeek = DayOfWeek.Monday
        If Not msDateFmt = Nothing Then
            moDateFormat.ShortDatePattern = Replace(msDateFmt, "mm", "MM")
        Else
            moDateFormat.ShortDatePattern = "dd/MM/yyyy"
        End If

        'formato numérico:
        moNumberFormat = New System.Globalization.CultureInfo(Me._refCultural, False).NumberFormat
        moNumberFormat.NumberDecimalDigits = miPrecisionFmt

        If Not msDecimalFmt = Nothing Then
            moNumberFormat.NumberDecimalSeparator = msDecimalFmt
        End If
        moNumberFormat.NumberGroupSeparator = msThousanFmt

    End Sub

    ''' <summary>
    ''' Función que actualiza los parametros de usuario en la BBDD con los datos introducidos por el usuario
    ''' </summary>
    ''' <param name="sThousanFmt">Formato de miles</param>
    ''' <param name="sDecimalFmt">Formato decimal</param>
    ''' <param name="iPrecisionFmt">Precision decimal</param>
    ''' <param name="sDateFmt">Formato de fechas</param>
    ''' <param name="iTipoEmail">Tipo de correo electronico</param>
    ''' <param name="sIdioma">Idioma de la aplicación</param>
    Public Sub UpdateUserData(ByVal sThousanFmt As String, ByVal sDecimalFmt As String, ByVal iPrecisionFmt As Integer,
                                           ByVal sDateFmt As String, ByVal iTipoEmail As Integer, ByVal sIdioma As String)
        Authenticate()

        DBServer.User_SaveUserData(msCod, sThousanFmt, sDecimalFmt, iPrecisionFmt, sDateFmt, iTipoEmail, sIdioma)

    End Sub


    ''' Revisado por: blp. Fecha: 07/03/2012    
    ''' <summary>
    ''' Función que actualiza los parametros de usuario en la BBDD con los datos introducidos por el usuario
    ''' </summary>
    ''' <returns>Un valor entero que sera 0 si todo ha ido bien y 1 si ha habido algun error</returns>
    ''' <remarks>
    ''' Llamada desde: La pagina ParametrosEP.aspx, evento BtnAceptar_Click
    ''' Tiempo maximo: 0,1 seg</remarks>
    Public Function GuardarOpcionesUsuario() As Integer
        'If Destino = "" Then
        '    Destino = Nothing
        'End If
        Return mDBServer.User_GuardarOpcionesUsuario(Destino, IIf(MostrarImagenArt, 1, 0), IIf(MostrarAtrib, 1, 0), IIf(MostrarCantMin, 1, 0), IIf(MostrarCodProve, 1, 0), IIf(MostrarCodArt, 1, 0), Cod, IIf(EPSMBar, 1, 0), m_bSeguimientoVerPedidosUsuario, m_bSeguimientoVerPedidosCCImputables, m_bCargarCantPtesRecep, m_bRecepcionVerPedidosUsuario, m_bRecepcionVerPedidosCCImputables)
    End Function
#End Region
#Region "Métodos QA"
    ''' <summary>
    ''' Para determinar si hay acceso al panel tanto en VAR_CAL_USU como en USU_UNQA debe el usuario tener registros.
    ''' </summary>
    ''' <returns>si hay acceso al panel o no</returns> 
    ''' <remarks>Llamada desde: LoadUserData ; Tiempo máximo: 0,1</remarks>
    Private Function PanelCalidadAccesible() As Boolean
        Dim data As DataSet
        Dim row As DataRow
        Dim Accesible As Boolean = False

        If AccesoQA Then
            Authenticate()

            data = DBServer.User_LoadUserDataPanel(msCod)
            If Not data.Tables(0).Rows.Count = 0 Then
                row = data.Tables(0).Rows(0)
                Accesible = SQLBinaryToBoolean(row.Item("FSQA_PUNTPROVE")) And SQLBinaryToBoolean(row.Item("FSQA_PUNTPROVEUNQA"))
            End If

            row = Nothing
            data = Nothing
        End If
        Return Accesible
    End Function
    Private Function PanelEscalacionProveedoresAccesible() As Boolean
        Dim Accesible As Boolean = False

        If AccesoQA Then
            Authenticate()
            Accesible = DBServer.User_LoadUserPanelEscalacion(msCod)
        End If
        Return Accesible
    End Function
    ''' <summary>
    ''' Guarda las modificaciones que se han producido en el parámetro que indica si se notifica al usuario de los
    ''' errores producidos en el cálculo de puntuaciones
    ''' </summary>
    ''' <param name="bNotificar">Si seleccionamos que el usuario sea notificado o no al producirse un error en el calculo de puntuaciones</param>
    ''' <remarks>
    ''' Llamada desde: PmWeb/parametrosgenerales/cmdAceptar_Click
    ''' Tiempo máximo: 0,2 seg</remarks>
    Public Sub Actualizar_QANotifErrorCalculoPuntuaciones(ByVal bNotificar As Boolean)
        Authenticate()
        DBServer.User_Actualizar_QANotifErrorCalculoPuntuaciones(Me.Cod, bNotificar)
    End Sub
    ''' Revisado por: Jbg. Fecha: 20/10/2011
    ''' <summary>
    ''' Actualiza la configuración del panel de calidad del usuario con la lista de Unidades de negoico y variables de calidad chequeadas en el filtro, y con los campos ocultos en el panel.
    ''' </summary>
    ''' <param name="sUserCode">Código del usuario</param>
    ''' <param name="FiltroUNQA">Lista de unqas chequeadas en el filtro(lista de IDs)</param>
    ''' <param name="FiltroVarCal">Lista de variables de calidad chequeadas en el filtro(lista de pares NIVEL-ID)</param>
    ''' <param name="CamposOcultos">Lista de campos ocultos en el panel (nombre de los campos)</param>
    ''' <param name="Ordenacion">orden de los datos del panel</param>
    ''' <remarks>Llamada desde=panelCalidad.aspx.vb; Tiempo máximo=0,1seg.</remarks>
    Public Sub Actualizar_ConfiguracionPanelCalidad(ByVal sUserCode As String, ByVal FiltroUNQA As String, ByVal FiltroVarCal As String, ByVal CamposOcultos As String, ByVal Ordenacion As String)
        Authenticate()
        DBServer.User_Actualizar_ConfiguracionPanelCalidad(sUserCode, FiltroUNQA, FiltroVarCal, CamposOcultos, Ordenacion)
    End Sub
    ''' Revisado por: Jbg. Fecha: 20/10/2011
    ''' <summary>
    ''' Obtiene la configuración del panel de calidad para un usurio y se carga en las propiedades del usuario:QAFiltroUNQA QAFiltroVarCal QACamposOcultos y QAordenacion
    ''' </summary>
    ''' <param name="sUserCode">Código del usuario</param>
    ''' <remarks>Llamada desde=panelCalidadFiltro.aspx..vb; Tiempo máximo=0,1seg.</remarks>
    Public Sub Cargar_ConfiguracionPanelCalidad(ByVal sUserCode As String)
        Authenticate()
        DBServer.User_Cargar_ConfiguracionPanelCalidad(sUserCode, msQAFiltroUNQA, msQAFiltroVarCal, msQACamposOcultos, msQAOrdenacion)
    End Sub
    ''' <summary>
    ''' Procedmiento que se encarga de actualizar el PP de forma manual o automática
    ''' </summary>
    ''' <param name="bManual">Incluir el manual o no</param>
    ''' <param name="bAuto">Actualizar automático</param>
    ''' <param name="iNivel">Nivel del PP</param>
    ''' <remarks>
    ''' Llamada desde: PmWeb/parametrosgenerales/cmdAceptar_Click
    ''' Tiempo máximo: 0,2 seg</remarks>
    Public Sub Actualizar_PPManualAuto(ByVal bManual As Boolean, ByVal bAuto As Boolean, ByVal iNivel As Integer)
        Authenticate()
        DBServer.User_Actualizar_PPManualAuto(Me.Cod, bManual, bAuto, iNivel)
    End Sub
#End Region
#Region "Métodos PM"
    Public Function ParticipaSolicitud(ByVal sUserCod As String, ByVal Tipo As TiposDeDatos.TipoDeSolicitud, ByVal TipoParticipacion As TipoParticipacionSolicitud) As Boolean
        Authenticate()
        Return DBServer.User_ParticipaSolicitud(sUserCod, Tipo, TipoParticipacion)
    End Function
    ''' <summary>
    ''' Procedmiento que actualiza los parámetros de usuario de PM
    ''' </summary>
    ''' <param name="bPMMostrarDefProvMat">Variable boolena que determina si se debe mostrar el material del proveedor por defecto</param>
    ''' <param name="iNumFilas">Número de filas a cargar por página para el usuario</param>
    ''' <remarks>
    ''' Llamada desde: PmWeb/parametrosPM/btnAceptar_click
    ''' Tiempo máximo: 0,35 seg</remarks>
    Public Sub Actualizar_ParametrosPM(ByVal bPMMostrarDefProvMat As Boolean, ByVal iNumFilas As Integer)
        Authenticate()
        DBServer.User_Actualizar_ParametrosPM(msCod, bPMMostrarDefProvMat, iNumFilas)
    End Sub
    ''' <summary>
    ''' Funcion que devuelve los datos con los detalles de una persona 
    ''' </summary>
    ''' <param name="lInstancia">identificador de la instancia</param>
    ''' <returns>Un DAtaSet con los datos de los detalles de la persona</returns>
    ''' <remarks>
    ''' Llamada desde: PmWeb/guardarInstancia/OcultarDetallePEr, PmWeb/NWComentariosolic/Page_load, PmWeb/NWhistoricoEstados/Page_load, PmWeb/flowDiagram/Page_load
    ''' Tiempo máximo: 0,25 seg</remarks>
    Public Function VerDetallesPersona(ByVal lInstancia As Long) As DataSet
        Authenticate()
        Return DBServer.User_VerDetallesPersona(Me.CodPersona, lInstancia)
    End Function
    ''' <summary>
    ''' Actualiza la tabla usu indicando si hay proveedores favoritos o no para el usuario
    ''' </summary>
    ''' <param name="bAnyadirProvFav">si tiene favoritos o no</param>
    ''' <remarks>
    ''' Llamada desde: PmWeb/proveedores/chkAnyadirProvFav_CheckedChanged
    ''' Tiempo máximo: 0,2 seg</remarks>
    Public Sub Actualizar_AnyadirProvFav(ByVal bAnyadirProvFav As Boolean)
        Authenticate()
        DBServer.User_Actualizar_AnyadirProvFav(Me.Cod, bAnyadirProvFav)
    End Sub
    ''' <summary>
    ''' Procedimiento que actualiza el parámetro de añadir presupuesto favorito de usuario
    ''' </summary>
    ''' <param name="iTipo">Tipo de presupuesto</param>
    ''' <param name="bAnyadirPresFav">Variable booleana que indica si se debe añadir un presupuesto favorito</param>
    ''' <remarks>
    ''' Llamada desde: PmWeb/PresuFavoritoServer/Page_load
    ''' Tiempo máximo: 0,25 seg</remarks>
    Public Sub Actualizar_AnyadirPresFav(ByVal iTipo As Integer, ByVal bAnyadirPresFav As Boolean)
        Authenticate()
        DBServer.User_Actualizar_AnyadirPresFav(Me.Cod, iTipo, bAnyadirPresFav)
        Me.PMAnyadirPresFav(iTipo) = bAnyadirPresFav
    End Sub
    ''' <summary>
    ''' Actualiza la tabla usu indicando si hay articulos favoritos o no para el usuario
    ''' </summary>
    ''' <param name="bMostrarArticuloFavMat">si está marcado o no</param>
    ''' <remarks>
    ''' Llamada desde: PMWeb/App_Pages/Script/_Common/articulosfavserver/chkAnyadirProvFav_CheckedChanged
    ''' Tiempo máximo: 0,2 seg</remarks>
    Public Sub Actualizar_MostrarArticuloFavMat(ByVal bMostrarArticuloFavMat As Boolean)
        Authenticate()
        DBServer.User_Actualizar_MostrarArticuloFavMat(Me.Cod, bMostrarArticuloFavMat)
    End Sub
    ''' <summary>
    ''' Actualiza la tabla usu indicando si se marca en lel frame de informacion de favoritos solo salen los favoritos del material seleccionado
    ''' </summary>
    ''' <param name="bAnyadirArticuloFav">si tiene favoritos o no</param>
    ''' <remarks>
    ''' Llamada desde: FSNWeb/App_Pages/_Common/BuscadorArticulos/chkAnyadirProvFav_CheckedChanged
    ''' Tiempo máximo: 0,2 seg</remarks>
    Public Sub Actualizar_AnyadirArticuloFav(ByVal bAnyadirArticuloFav As Boolean)
        Authenticate()
        DBServer.User_Actualizar_AnyadirArticuloFav(Me.Cod, bAnyadirArticuloFav)
    End Sub
#End Region
#Region "Métodos CN"
    ''' <summary>
    ''' Guarda las opciones de CN del usuario
    ''' </summary>
    ''' <param name="sUsuCod">Código de usuario </param>
    ''' <param name="bNotificarResumenActividad">Opción de usuario: Resumen de actividad</param>
    ''' <param name="bNotificarIncluidoGrupo">Opción de usuario: Me incluyen en un grupo corporativo</param>
    ''' <param name="bConfiguracionDesocultar">Volver a mostrar automáticamente los mensajes ocultos en los que haya actividad</param>
    ''' <remarks>Llamada desde: cn_UserProfile.aspx.vb ; Tiempo máximo: 0,1</remarks>
    ''' <revision>JVS 02/02/2012</revision>
    Public Sub CN_Update_Opciones_Usuario(ByVal sUsuCod As String,
                                          ByVal bNotificarResumenActividad As Boolean, ByVal bNotificarIncluidoGrupo As Boolean,
                                          ByVal bConfiguracionDesocultar As Boolean)
        Authenticate()

        DBServer.CN_Update_Opciones_Usuario(sUsuCod, bNotificarResumenActividad, bNotificarIncluidoGrupo, bConfiguracionDesocultar)
    End Sub
    ''' <summary>
    ''' Guarda la imagen del usuario
    ''' </summary>
    ''' <param name="sUsuCod">Código de usuario </param>
    ''' <remarks>Llamada desde: cn_UserProfile.aspx.vb ; Tiempo máximo: 0,1</remarks>
    ''' <revision>JVS 14/02/2012</revision>
    Public Sub CN_Actualizar_Imagen_Usuario(ByVal pathFoto As String, ByVal sUsuCod As String, ByRef DGuidImagen As String)
        Authenticate()
        DBServer.CN_Actualizar_Imagen_Usuario(pathFoto, sUsuCod, DGuidImagen)
    End Sub
#End Region
#Region "Métodos SM"
    ''' <summary>
    ''' Función que devuelve los datos de la persona asociada al usuario
    ''' </summary>
    ''' <param name="CodUser">Código de usuario</param>    
    ''' <returns>Un objeto Persona con los datos de la persona</returns>
    ''' <remarks>
    ''' Llamada desde: 
    ''' Tiempo máximo: 5 seg</remarks>
    Public Function DevolverPersona(ByVal CodUser As String) As DataTable
        Dim dsPersona As DataSet = DBServer.User_GetPerson(CodUser, msCodPersona)
        If Not dsPersona Is Nothing AndAlso dsPersona.Tables.Count > 0 Then
            Return dsPersona.Tables(0)
        Else
            Return Nothing
        End If
    End Function
#End Region
#Region "Métodos EP"
    ''' <summary>
    ''' Devuelve los datos actuales de la cesta del usuario actual
    ''' </summary>
    ''' <returns>Un datarow con el numero de artÃ­culos("NUMARTICULOS"), el importe total("IMPORTE"),
    ''' el código de moneda ("MON") y el cambio actual("EQUIV")</returns>
    ''' <remarks></remarks>
    Public Function InfoCesta() As DataRow
        Return InfoCesta(msCod)
    End Function
    ''' <summary>
    ''' Devuelve los datos actuales de la cesta del usuario especificado
    ''' </summary>
    ''' <param name="Usuario">Código de usuario</param>
    ''' <returns>Un datarow con el numero de artÃ­culos("NUMARTICULOS"), el importe total("IMPORTE"),
    ''' el código de moneda ("MON") y el cambio actual("EQUIV")</returns>
    ''' <remarks>Llamada desde InfoCesta, EPWebControls.FSEPWebPartCesta.CargarCesta</remarks>
    Public Function InfoCesta(ByVal Usuario As String) As DataRow
        Authenticate()
        Return DBServer.User_InfoCesta(Usuario)
    End Function

#End Region
End Class
