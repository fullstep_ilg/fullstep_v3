Imports System.Threading
<Serializable()> _
        Public Class Proveedores
    Inherits Security
    Private moData As DataSet

    Public ReadOnly Property Data() As Data.DataSet
        Get
            Return moData
        End Get

    End Property
    ''' <summary>
    ''' Procedimiento que carga los datos de los todos los proveedores, pudiendose filtrar por los par�metros de entrada
    ''' </summary>
    ''' <param name="sIdi">Idioma del usuario</param>
    ''' <param name="sCod">Codigo del proveedor</param>
    ''' <param name="sDen">Denominacion del proveedor</param>
    ''' <param name="sNif">NIF del proveedor</param>
    ''' <param name="sUser">Codigo de usuario</param>
    ''' <param name="sOrgCompras">Codigo de la organizacion de compras</param>
    ''' <param name="sGMN1">Codigo de la estructura de materiales, nivel 1</param>
    ''' <param name="sGMN2">Codigo de la estructura de materiales, nivel 2</param>
    ''' <param name="sGMN3">Codigo de la estructura de materiales, nivel 3</param>
    ''' <param name="sGMN4">Codigo de la estructura de materiales, nivel 4</param>
    ''' <param name="bSoloFav">Si carga solo favoritos o no</param>
    ''' <param name="bConContactos">Si solo va a mostrar los proveedores con contactos o todos</param>
    ''' <param name="CargaFormPago">Si se va a cargar Forma de pago</param>
    ''' <param name="UsarOrgCompras">Si se usa org compras</param>
    ''' <param name="AccesoFSSM">Si tiene Acceso FSSM</param>  
    ''' <param name="sUon1">Partida presupuestaria uon Nivel1</param>
    ''' <param name="sUon2">Partida presupuestaria uon Nivel2</param>
    ''' <param name="sUon3">Partida presupuestaria uon Nivel3</param>
    ''' <param name="sUon4">Partida presupuestaria uon Nivel4</param>
    ''' <remarks>
    ''' Llamada desde: PmWeb/guardarInstancia/cmdContinuarTraslado_Serverclick, PmWeb/proveedores/cmdBuscar_ServerClick, PmWeb/proveedoresTrasladados/cmdBuscar_ServerClick, PmWeb/traslados/cmdBuscar_ServerClick
    ''' Tiempo m�ximo: 1 seg</remarks>
    Public Sub LoadData(ByVal sIdi As String, Optional ByVal sCod As String = Nothing, Optional ByVal sDen As String = Nothing,
                Optional ByVal sNif As String = Nothing, Optional ByVal sUser As String = Nothing, Optional ByVal sOrgCompras As String = Nothing,
                Optional ByVal sGMN1 As String = "", Optional ByVal sGMN2 As String = "", Optional ByVal sGMN3 As String = "",
                Optional ByVal sGMN4 As String = "", Optional ByVal bSoloFav As Boolean = False, Optional ByVal lRol As Long = 0,
                Optional ByVal bConContactos As Boolean = False, Optional ByVal PMRestricProveMat As Boolean = False,
                Optional ByVal CargaFormPago As Boolean = False, Optional ByVal UsarOrgCompras As Boolean = False, Optional ByVal AccesoFSSM As Boolean = False,
                Optional ByVal sUon1 As String = Nothing, Optional ByVal sUon2 As String = Nothing, Optional ByVal sUon3 As String = Nothing, Optional ByVal sUon4 As String = Nothing,
                Optional ByVal sCodArt As String = Nothing, Optional bUsaLike As Boolean = True)
        Authenticate()
        moData = DBServer.Proveedores_Load(sIdi, sCod, sDen, sNif, bUsaLike, sUser, sOrgCompras, sGMN1, sGMN2, sGMN3, sGMN4, bSoloFav, lRol, bConContactos, PMRestricProveMat, CargaFormPago, UsarOrgCompras, AccesoFSSM, sUon1, sUon2, sUon3, sUon4, sCodArt)
    End Sub
    ''' <summary>
    ''' Procedimiento que carga los certificados de los proveedores
    ''' </summary>
    ''' <param name="sCod">Codigo del proveedor</param>
    ''' <param name="sDen">Denominacion del proveedor</param>
    ''' <param name="sNif">NIF del proveedor</param>
    ''' <param name="sUser">Codigo de usuario</param>
    ''' <param name="bDesdeProveedores">Variable boolena que indica si se cargan los certificados desde los proveedores</param>
    ''' <param name="bConNoConformidades">Variable booleana que indica si se deben cargar con las no conformidades</param>
    ''' <param name="bFuerzaBaja">Variable booleana que indica si se debe forzar la baja</param>
    ''' <param name="iTipo">1 del panel 2 potencial	3 todos</param>
    ''' <remarks>
    ''' Llamada desde: PmWeb/proveedores/cmdBuscar_ServerClick, PmWeb/proveedoresTraslados/cmdBuscar_ServerClick, PmWeb/noconformidad/PAge_load
    ''' Tiempo m�ximo: 0,6 seg</remarks>
    Public Sub Carga_Proveedores_QA(Optional ByVal sCod As String = Nothing, Optional ByVal sDen As String = Nothing,
            Optional ByVal sNif As String = Nothing, Optional ByVal sUser As String = Nothing, Optional ByVal bUsaLike As Boolean = False,
            Optional ByVal bDesdeProveedores As Boolean = False, Optional ByVal bConNoConformidades As Boolean = False,
            Optional ByVal bFuerzaBaja As Boolean = False, Optional ByVal bOrdenacionDenominacion As Boolean = False, Optional ByVal iTipo As Integer = 3,
            Optional ByVal RestricProvMat As Boolean = False, Optional ByVal RestricProvEqp As Boolean = False, Optional ByVal RestricProvCon As Boolean = False)
        Authenticate()
        moData = DBServer.Proveedores_CertifLoad(sCod:=sCod, sDen:=sDen, sNif:=sNif, bUsaLike:=bUsaLike, sUser:=sUser, bDesdeProveedores:=bDesdeProveedores,
                                                 bConNoConformidades:=bConNoConformidades, bFuerzaBaja:=bFuerzaBaja,
                                                 bOrdenadorPorDenominacion:=bOrdenacionDenominacion, iTipo:=iTipo,
                                                 RestricProvMat:=RestricProvMat, RestricProvEqp:=RestricProvEqp, RestricProvCon:=RestricProvCon)
    End Sub
    ''' <summary>
    ''' Procedimiento que carga los proveedores a los cuales se les puede enviar un mail
    ''' </summary>
    ''' <param name="sCod">Codigo de proveedor</param>
    ''' <param name="sDen">Denominacion del proveedor</param>
    ''' <param name="sNif">NIF del proveedor</param>    
    ''' <param name="iExacto">Variable booleana que indica si se debe buscar con like la denominacion</param>
    ''' <param name="bVerifProveQA">Indica si hay que verificar que el proveedor es de QA</param>
    ''' <remarks>
    ''' Llamada desde: PmWeb/EnviarMailProveedores/HypBuscarClick, PmWeb/detalleCertificado/Page_load, PmWeb/detalleNoConformidad/Page_load
    ''' tiempo m�ximo: 1 seg</remarks>
    Public Sub CargarEmailProv(Optional ByVal sCod As String = Nothing, Optional ByVal sDen As String = Nothing, Optional ByVal sNif As String = Nothing, Optional ByVal iExacto As Boolean = False,
                               Optional ByVal bVerifProveQA As Boolean = False)
        Authenticate()
        moData = DBServer.Proveedores_EmailProv(sCod, sDen, sNif, iExacto, bVerifProveQA)
    End Sub
    ''' <summary>Carga los proveedores para los que se pueden indicar objetivos y suelos</summary>
    ''' <param name="sUsu">Cod. usuario</param>
    ''' <param name="iIdMatQA">Id del material de QA</param>
    ''' <param name="iIdUNQA">Id de la unidad de negocio</param>
    ''' <param name="sCod">Cod. proveedor</param>
    ''' <param name="sDen">Den. proveedor</param>
    ''' <param name="sNif">Nif proveedor</param>
    ''' <param name="iNumReg">N� max. de registros a devolver</param>
    ''' <param name="bProvesCriticos">Indica si hay que filtrar s�lo por proveedores con objetivos y suelos espec�ficos</param>
    ''' <remarks>Llamada desde: BuscaodrProveedores.aspx, AutoComplete.asmx</remarks>
    Public Sub CargarProvObjSuelos(ByVal sUsu As String, ByVal iIdMatQA As Integer, ByVal iIdUNQA As Integer, Optional ByVal sCod As String = Nothing, Optional ByVal sDen As String = Nothing, Optional ByVal sNif As String = Nothing,
                                   Optional ByVal iNumReg As Integer = 0, Optional ByVal bProvesCriticos As Boolean = False)
        Authenticate()
        moData = DBServer.Proveedores_ObjSuelos(sUsu, iIdMatQA, iIdUNQA, sCod, sDen, sNif, bProvesCriticos, iNumReg)
    End Sub
    ''' <summary>
    ''' Procedimiento que devuelve los proveedores seleccionados
    ''' </summary>
    ''' <param name="sProveedores">Codigos de los proveedores</param>
    ''' <remarks>
    ''' Llamada desdE: PmWeb/altacertificado/PAge_load
    ''' Tiempo m�ximo: 0,3 seg</remarks>
    Public Sub DevolverProvedoresSeleccionados(ByVal sProveedores As String)
        Authenticate()
        moData = DBServer.Proveedores_ProvedoresSeleccionados(sProveedores)
    End Sub
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
    ''' Revisado por: Jbg. Fecha: 21/10/2011
    ''' <summary>
    ''' Funci�n que realiza el env�o de los emails.
    ''' </summary>
    ''' <param name="Para">El destinatario</param>
    ''' <param name="CC">Copia a</param>
    ''' <param name="CCO">Copia oculta</param>
    ''' <param name="Subject">El asunto</param>
    ''' <param name="Message">El mensaje</param>
    ''' <param name="Remitente">El from del mensaje</param>
    ''' <param name="Adjuntos">Cadena con los adjuntos</param>
    ''' <param name="Ruta">Ruta de los adjuntos</param>
    ''' <param name="bEsPM">Si estamos en PM</param>
    ''' <param name="lIdInstancia">Instancia que asociaremos al mail enviado</param> 
    ''' <param name="EntidadNotificacion">Identifica para q producto es el mail</param>
    ''' <remarks>Llamada desde: _common\MailProveedores.aspx.vb ; Tiempo maximo: 0,1 </remarks>
    Public Function EnviarMailAProveedores(ByVal Para As String, ByVal CC As String, ByVal CCO As String, ByVal Subject As String, ByVal Message As String, ByVal Remitente As String, _
                                           ByVal Adjuntos As String, ByVal Ruta As String, Optional ByVal bEsPm As Boolean = True, Optional ByVal lIdInstancia As Long = 0, _
                                           Optional ByVal EntidadNotificacion As Integer = 0, Optional ByVal Idioma As String = "SPA") As Integer
        Dim iError As Integer
        Dim strError As String = String.Empty
        Dim oNotificador As New Notificar(DBServer, mIsAuthenticated)
        Try
            Dim SQLPara As String = ""
            Dim arrayPara As String() = Split(Para, ";")
            For i As Integer = arrayPara.Length - 1 To 0 Step -1
                SQLPara = SQLPara & "'" & Trim(arrayPara(i)) & "'"
                If i > 0 Then SQLPara = SQLPara & ","
            Next

            oNotificador.EnviarMail(Remitente, Subject, Para, Message, iError, strError, CC, Adjuntos, Ruta, , CCO, Producto.QA, True, DameProveSegunEmail(SQLPara), _
                                    FSNLibrary.TipoNotificacion.EmailNoAutomatico, lIdInstancia, EntidadNotificacion, , , Idioma)

            Select Case iError
                Case 0 '   0- SmtpMail.Send(MyMail) casco
                    EnviarMailAProveedores = ErroresEMail.Send
                Case 1 '   1- Algun adjunto ha sido borrado
                    EnviarMailAProveedores = ErroresEMail.Adjuntos
                Case 2 '   2- Todo bien
                    EnviarMailAProveedores = ErroresEMail.SinError
                Case Else
                    Return -1
            End Select
        Catch ex As Exception '   3- Casque en creacion objeto o llamada
            EnviarMailAProveedores = ErroresEMail.CreacionObjeto
        Finally
            oNotificador.FuerzaFinalizeSmtpClient()
            oNotificador = Nothing
        End Try

        Return EnviarMailAProveedores
    End Function
    ''' <summary>
    ''' Obtiene la lista de proveedores q se corresponden con los emails de contacto del PARA. No mira CC ni CCO.
    ''' </summary>
    ''' <param name="Para">emails de contacto del PARA</param>
    ''' <returns>la lista de proveedores</returns>
    ''' <remarks>Llamada desde:EnviarMailAProveedores; Tiempo m�ximo: 0</remarks>
    Private Function DameProveSegunEmail(ByVal Para As String) As String
        Authenticate()

        DameProveSegunEmail = DBServer.DameProveSegunEmail(Para)
    End Function
    ''' <summary>
    ''' Procedure que carga los datos de los proveedores relacionados con las unidades de negocio de la grid proveedores
    ''' </summary>
    ''' <param name="sIdi">Idioma con el que se cargan las denominaciones</param>
    ''' <param name="sCod">Codigo proveedor (filtro)</param>        
    ''' <param name="sDen">Denominacion proveedor(filtro)</param>
    ''' <param name="sNif">Nif proveedor(filtro)</param>
    ''' <param name="bBaja">si hay que mostrar o no los proveedores dados de baja(filtro)</param>
    ''' <param name="iTipoProveedores">Tipo proveedor 0--> Todos los proveedores 1--> Proveedores Potenciales  2-->Proveedores de calidad  (filtro)</param>
    ''' <param name="UNQAs">Cadena con las Ids de las unidades de negocio seleccionadas</param>
    ''' <param name="bUsarLike">true(mostrar los datos con LIKE</param>
    ''' <param name="lIDPyme">Id de la pyme del usuario, si trabaja en modo pyme</param>
    ''' <param name="sSort">Cadena de ordenacion</param>
    ''' <remarks>Llamada desde=UnidadesNegocio.aspx.vb; Tiempo m�ximo=0,28seg.</remarks>
    Public Sub ProveedoresConUnidadesNegocio(ByVal USU As String, ByVal sIdi As String, _
        ByVal RestricProvCon As Boolean, ByVal RestricProvMat As Boolean, ByVal RestricProvEqp As Boolean, _
        Optional ByVal sCod As String = Nothing, Optional ByVal sDen As String = Nothing, Optional ByVal sNif As String = Nothing, Optional ByVal bBaja As Boolean = False, Optional ByVal iTipoProveedores As Byte = 1, Optional ByVal UNQAs As String = "", Optional ByVal bUsarLike As Boolean = False, Optional ByVal lIDPyme As Long = 0, Optional ByVal sSort As String = "")
        Authenticate()
        moData = DBServer.ProveedoresConUnidadesNegocio(USU, sIdi, _
            RestricProvCon, RestricProvMat, RestricProvEqp, _
            sCod, sDen, sNif, bBaja, iTipoProveedores, UNQAs, bUsarLike, lIDPyme, sSort)
    End Sub
    ''' <summary>
    ''' DEvuelve un dataset con 3 tablas para geenrar el panel de calidad:Maestro de proveedores, puntuaciones y masteriales de QA
    ''' </summary>
    ''' <param name="sUsu">Codigo de Usuario</param>
    ''' <param name="dtMaterialesQA">Filtro Lista de Materiales de QA separados por coma</param>
    ''' <param name="sUNQAs">Filtro Lista de UNQAS separados por coma</param>
    ''' <param name="sUNQAsPunt">Filtro Lista de UNQAS para mostrar punt separados por coma</param>
    ''' <param name="sVarCal">Filtro Lista de variabels de calidad</param>
    ''' <param name="shProveedores">Tip de proveedor</param>
    ''' <param name="sProve">Codigo de proveedor</param>
    ''' <param name="shProveBaja">Incluir proveedores de baja</param>
    ''' <param name="sIdi">Idioma</param>
    ''' <returns>Dataset</returns>
    ''' <remarks>Llamadas: PMweb/script/puntuacion/panelCaldiad.aspx.vb</remarks>
    Public Function DevolverPanelCalidad(ByVal sUsu As String, ByVal dtMaterialesQA As DataTable, ByVal sUNQAs As String, _
        ByVal sUNQAsPunt As String, ByVal sVarCal As String, ByVal shProveedores As Short, ByVal sProve As String, _
        ByVal shProveBaja As Short, ByVal sIdi As String, ByVal RestricProvCon As Boolean, _
        ByVal RestricProvMat As Boolean, ByVal RestricProvEqp As Boolean) As DataSet
        Authenticate()
        Return DBServer.Proveedores_DevolverPanelCalidad(sUsu, dtMaterialesQA, sUNQAs, sUNQAsPunt, sVarCal, shProveedores, _
                    sProve, shProveBaja, sIdi, RestricProvCon, RestricProvMat, RestricProvEqp)
    End Function
    ''' <summary>
    ''' Carga los proveedores, los que sean de calidad y no este dados de baja logica
    ''' Tabla 1 --> Proveedores con unidades de negocio relacionadas
    ''' Tabla 2 --> Proveedores sin unidades de negocio
    ''' </summary>
    ''' <remarks>Llamada desde=QA_Puntuaciones.vb; Tiempo m�ximo=0,1seg.</remarks>
    Public Sub cargarProvCalidad(Optional ByVal IdPyme As Integer = 0, Optional ByVal CodProve As String = Nothing)
        Authenticate()
        moData = DBServer.Proveedores_Calidad(IdPyme, CodProve)
        Dim dtProves As DataTable = New DataView(moData.Tables(0)).ToTable("PROVES", True, {"PROVE"})
        moData.Tables.Add(dtProves)
    End Sub
    ''' <summary>
    ''' Devuelve un dataset con una tabla que contiene los proveedores que cumplen con los par�metros de entrada junto con sus puntuaciones/calificaciones
    ''' para una unidad de negocio concreta y una variabel de calidad concreta, pasadas como par�metros.
    ''' Devuleve un dataset para que en el webpart de Panel de Calidad se pueda construir un gr�fico con las puntuaciones de los proveedores.
    ''' Si el gr�fico es de barras el dataset devolverr� una tabla con un regsitro por cada proveedor que cumple con los filtros, junto con su puntuaci�n y calificaci�n para
    ''' la varibale de calidad y la undiad de negocio establecidas en los filtros. Ademas el dataset devuelve otra tabla con las posibles calificaciones para la variable.
    ''' Si el gr�fico es de evoluci�n devuelve un dataset con una tabla con los proveedores que cumplen con los filtros, con las puntuaciones obtenidas entre las fechas establecidas en el filtro para la variabel de calidad y unidad de negoico estabn
    ''' </summary>
    ''' <param name="lVarCal">Id de la variable de calidad de la que obtener la punt</param>
    ''' <param name="iNivelVarCal">Nivel de la variable de calidad de la que obtener la punt</param>
    ''' <param name="lMaterialQA">Material de QA de los proveedores a mostrar</param>
    ''' <param name="sMaterialGS">Material de GS de los proveedores a mostrar</param>
    ''' <param name="sCodArt">Codigo del articulo que a los proveedores a mostrar se les ha adjudicado o se les ha pedido</param>
    ''' <param name="lUNQA">Id de la UNQA de la que hay que obtener la puntuaci�n</param>
    ''' <param name="sUNQA_PROVE">Lista de unqas a las que tienen que pertenecer los proveedores a mostrar (la chequeada, m�s su padre e hijas)</param>
    ''' <param name="iTipoProveedores">1-todos, 2-panel calidad, 3-Potenciales, 4-lista de proveedores </param>
    ''' <param name="sListaProves">Si se ha elegido la opci�n 4.lista de proveedores, cadena con los codigos de los proveedores</param>
    ''' <param name="bProveedoresBaja">Incluir o no a los proveedores de baja l�gica</param>
    ''' <param name="iTipoGrafico">0:Barras/1:Evoluci�n</param>
    ''' <param name="iDesdeMes">Si grafico es de evoluci�n Mes desde</param>
    ''' <param name="iDesdeAnyo">Si grafico es de evoluci�n A�o desde</param>
    ''' <param name="iHastaMes">Si grafico es de evoluci�n Mes hasta</param>
    ''' <param name="iHastaAnyo">Si grafico es de evoluci�n A�o hasta</param>
    ''' <param name="iOrden">0: Ascendente/1 :Descendente. El orden en el que se muestran los proveedores en el gr�fico variar� en funci�n del sentido de ordenaci�n de sus puntuaciones</param>
    ''' <param name="sUsu">Codigo de usurio</param>
    ''' <param name="dDesdePunt">Rango inferior de puntuaciones</param>
    ''' <param name="bDesdePuntNothing">Si hay que tener en cuenta o no el valor de dDesdePunt (por si vale 0 porque es nothing o porque realmente el usurio quiere obtener con puntuci�n a partir de 0)</param>
    ''' <param name="dHastaPunt">Rango superior de puntuaciones</param>
    ''' <param name="bHastaPuntNothing">Si hay que tener en cuenta o no el valor de dHastaPunt (por si vale 0 porque es nothing o porque realmente el usurio quiere obtener con puntuci�n m�xima de 0)</param>
    ''' <param name="sOperador">CAdena con el operador a aplicar al par�metro bDesdePuntNothing</param>
    ''' <param name="sCodIdioma">Idioma del usuario</param>
    ''' <param name="iNumeroProveedores">N�mero de proveedores a mostrar</param>
    ''' <remarks>Llamada desde PMWEbControls/FSQAPanelCalidad.vb Tiempo:0.0 seg</remarks>
    Public Sub CargarPuntuaciones(ByVal lVarCal As Long, ByVal iNivelVarCal As Integer, ByVal lMaterialQA As Long, _
        ByVal sMaterialGS As String, ByVal sCodArt As String, ByVal lUNQA As Long, ByVal sUNQA_PROVE As String, _
        ByVal iTipoProveedores As Integer, ByVal sListaProves As String, ByVal bProveedoresBaja As Boolean, _
        ByVal iTipoGrafico As Integer, ByVal iDesdeMes As Integer, ByVal iDesdeAnyo As Integer, ByVal iHastaMes As Integer, _
        ByVal iHastaAnyo As Integer, ByVal iOrden As Integer, ByVal sUsu As String, _
        ByVal RestricProvMat As Boolean, ByVal RestricProvEqp As Boolean, ByVal RestricProvCon As Boolean, _
        ByVal dDesdePunt As Double, ByVal bDesdePuntNothing As Boolean, ByVal dHastaPunt As Double, _
        ByVal bHastaPuntNothing As Boolean, Optional ByVal sOperador As String = "", _
        Optional ByVal sCodIdioma As String = "SPA", Optional ByVal iNumeroProveedores As Integer = 0)
        Authenticate()
        moData = DBServer.Proveedores_CargarPuntuaciones(lVarCal, iNivelVarCal, lMaterialQA, sMaterialGS, sCodArt, lUNQA, _
                sUNQA_PROVE, iTipoProveedores, sListaProves, bProveedoresBaja, iTipoGrafico, iDesdeMes, iDesdeAnyo, _
                iHastaMes, iHastaAnyo, iOrden, sUsu, RestricProvMat, RestricProvEqp, RestricProvCon, _
                dDesdePunt, bDesdePuntNothing, dHastaPunt, bHastaPuntNothing, _
                sOperador, sCodIdioma, iNumeroProveedores)
    End Sub
    ''' <summary>
    ''' Obtiene las fechas (MES/A�O) para las que existen puntuaciones y que por lo tanto podemos consultar
    ''' </summary>
    ''' <remarks>Llamada desde=PMWebControls/FSQAWEbPartCalidad.vb; Tiempo m�ximo=0.0 seg.</remarks>
    Public Sub ObtenerFechasHistPunt()
        Authenticate()
        moData = DBServer.Proveedores_ObtenerFechasHistPunt()
    End Sub
    ''' <summary>
    ''' Elimina de la base de datos Los posibles errores Objetivo, suelo o facturacion en el calculo de la puntuacion
    ''' del proveedor que se le pasa
    ''' </summary>
    ''' <param name="CodProve">Codigo del proveedor del que borrar los errores. Borra todos si no se le indica el codigo del proveedor expresamente</param>
    ''' <remarks>QA_Puntuaciones.vb; tiempo ejecucion:=0,3seg.</remarks>
    Public Sub EliminarPosiblesErroresPuntuacion(Optional ByVal CodProve As String = Nothing)
        Authenticate()
        DBServer.EliminarErroresPuntuacion(CodProve)
    End Sub
    ''' <summary>
    ''' Controla en un thread el envio de los mail de errores en el c�lculo de puntuaciones
    ''' </summary>
    ''' <param name="IdPyme">El Id de la pyme, si es necesaria</param>
    ''' <remarks>Llamada desde=QA_Puntuaciones.asmx;Tiempo maximo:0</remarks>
    Public Sub NotificacionesErroresCalculoPuntuaciones(Optional ByVal IdPyme As Integer = 0)
        Authenticate()

        Dim dDatos As New Dictionary(Of String, String)
        dDatos.Add("IdPyme", IdPyme)
        ThreadPool.QueueUserWorkItem(New WaitCallback(AddressOf NotificarErroresCalculoPuntuaciones), dDatos)
    End Sub
    ''' <summary>
    ''' Llama al notificador para enviar los mail de error en el c�lculo de puntuaciones
    ''' </summary>
    ''' <param name="dDatos">Los datos obtenidos antes, IdPyme y el from para el mail</param>
    ''' <remarks>Llamada desde=NotificacionesErroresCalculoPuntuaciones;Tiempo maximo:0</remarks>
    Private Sub NotificarErroresCalculoPuntuaciones(ByVal dDatos As Object)
        Dim oNotificador As New Notificar(DBServer, mIsAuthenticated)

        If CType(dDatos("IdPyme"), Integer) = 0 Then
            oNotificador.NotificacionErroresCalculoPuntuaciones()
        Else
            oNotificador.NotificacionErroresCalculoPuntuaciones(CType(dDatos("IdPyme"), Integer))
        End If

        oNotificador.FuerzaFinalizeSmtpClient()
        oNotificador = Nothing
    End Sub
    ''' <summary>
    ''' Proceso que devuelve todos los proveedores
    ''' </summary>
    ''' <remarks>Llamada desde:Page_load; Tiempo m�ximo:0,3seg.</remarks>
    Public Function CargarProveedores() As DataSet
        Authenticate()
        Return DBServer.Proveedores_LoadTodos()
    End Function
End Class