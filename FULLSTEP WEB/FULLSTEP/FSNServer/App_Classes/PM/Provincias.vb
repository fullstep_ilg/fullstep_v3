<Serializable()> _
    Public Class Provincias

    Inherits Security
    Private Contexto As System.Web.HttpContext
    Private moData As DataSet
    Private msPais As String

    Public Property Pais() As String
        Get
            Return msPais
        End Get
        Set(ByVal Value As String)
            msPais = Value
        End Set
    End Property
    Public ReadOnly Property Data() As Data.DataSet
        Get
            Return moData
        End Get
    End Property
    ''' <summary>
    ''' Carga las provincias de un pa�s
    ''' </summary>
    ''' <param name="sIdioma">idioma</param>
    ''' <param name="sCod">cod de la provincia</param>
    ''' <param name="sDen">denominaci�n de la provincia</param>
    ''' <remarks>Llamada desde: paiseserver.vb, campos.ascx, desglose.ascx, impexp.aspx; Tiempo m�ximo: 0s g.</remarks>
    Public Sub LoadData(ByVal sIdioma As String, Optional ByVal sCod As String = Nothing, Optional ByVal sDen As String = Nothing)
        Authenticate()

        Dim ds As New DataSet
        If sCod Is Nothing AndAlso sDen Is Nothing Then
            Contexto = System.Web.HttpContext.Current
            ds = CType(Contexto.Cache("dsProvincias_" & msPais & "_" & sIdioma), DataSet)
            If ds Is Nothing Then
                ds = DBServer.Provincias_Load(sIdioma, msPais, sCod, sDen, True)
                Contexto.Cache.Insert("dsProvincias_" & msPais & "_" & sIdioma, ds, Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration, New TimeSpan(0, System.Configuration.ConfigurationManager.AppSettings("TiempoExpiracionCacheMin"), 0))
            End If
        Else
            moData = DBServer.Provincias_Load(sIdioma, msPais, sCod, sDen, True)
        End If
        moData = ds
    End Sub
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
End Class