
<Serializable()> _
Public Class Accion
    Inherits Security

    Private mlId As Integer
    Private msDen As String
    Private miTipoRechazo As Integer
    Private mbGuardar As Boolean
    Private miTipo As Integer
    Private mbIncrementarId As Boolean
    Private mbCumpOblRol As Boolean
    Private mlIdLlamadaExterna As Long
    Private msNomXmlLlamadaExterna As String
    Private msPlantillaSOAP As String
    Private mbGenerarEFactura As Boolean


    Property Id() As Integer
        Get
            Return mlId
        End Get
        Set(ByVal Value As Integer)
            mlId = Value
        End Set
    End Property

    Property Den() As String
        Get
            Return msDen
        End Get
        Set(ByVal Value As String)
            msDen = Value
        End Set
    End Property

    Property TipoRechazo() As Integer
        Get
            Return miTipoRechazo
        End Get
        Set(ByVal Value As Integer)
            miTipoRechazo = Value
        End Set
    End Property

    Property Tipo() As Integer
        Get
            Return miTipo
        End Get
        Set(ByVal Value As Integer)
            miTipo = Value
        End Set
    End Property


    Property Guardar() As Boolean
        Get
            Return mbGuardar
        End Get
        Set(ByVal Value As Boolean)
            mbGuardar = Value
        End Set
    End Property

    Property IncrementarId() As Boolean
        Get
            Return mbIncrementarId
        End Get
        Set(ByVal Value As Boolean)
            mbIncrementarId = Value
        End Set
    End Property

    Property CumpOblRol() As Boolean
        Get
            Return mbCumpOblRol
        End Get
        Set(ByVal Value As Boolean)
            mbCumpOblRol = Value
        End Set
    End Property
    Property LlamadaExterna() As Long
        Get
            Return mlIdLlamadaExterna
        End Get
        Set(ByVal Value As Long)
            mlIdLlamadaExterna = Value
        End Set
    End Property
    Property NomXmlLlamadaExterna() As String
        Get
            Return msNomXmlLlamadaExterna
        End Get
        Set(ByVal Value As String)
            msNomXmlLlamadaExterna = Value
        End Set
    End Property

    Property PlantillaSOAP() As String
        Get
            Return msPlantillaSOAP
        End Get
        Set(ByVal Value As String)
            msPlantillaSOAP = Value
        End Set
    End Property
    
    Property GenerarEFactura() As Boolean
        Get
            Return mbGenerarEFactura
        End Get
        Set(ByVal Value As Boolean)
            mbGenerarEFactura = Value
        End Set
    End Property

    ''' <summary>
    ''' Carga una acci�n concreta de un usuario en un idioma
    ''' </summary>
    ''' <param name="sIdi">Idioma a devolver la accion</param>
    ''' <remarks>
    ''' Llamada desde: Rol.vb, Service.asmx.vb,NwDetalleSolicitud.aspx.vb, accionRealizadaOk.aspx.vb,accionInvalida.aspx.vb,NWgestionInstancia.aspx.vb
    ''' Tiempo m�ximo: 0 seg</remarks>
    Public Sub CargarAccion(ByVal sIdi As String)
        Authenticate()

        Dim oData As DataSet
        Dim oRow As DataRow

        oData = DBServer.Accion_Load(mlId, sIdi)
        If oData.Tables.Count > 0 AndAlso oData.Tables(0).Rows.Count > 0 Then
            oRow = oData.Tables(0).Rows(0)

            msDen = DBNullToSomething((oRow.Item("DEN")))
            miTipoRechazo = oRow.Item("TIPO_RECHAZO")
            miTipo = oRow.Item("TIPO")
            mbGuardar = SQLBinaryToBoolean(oRow.Item("GUARDA"))
            mbIncrementarId = SQLBinaryToBoolean(oRow.Item("INCREMENTAR_ID"))
            mbCumpOblRol = SQLBinaryToBoolean(oRow.Item("CUMP_OBL_ROL"))
            mlIdLlamadaExterna = DBNullToInteger(oRow.Item("LLAMADA_EXTERNA"))
            msNomXmlLlamadaExterna = DBNullToStr(oRow.Item("NOM_XML"))
            msPlantillaSOAP = DBNullToStr(oRow.Item("PLANTILLA_SOAP"))
            mbGenerarEFactura = SQLBinaryToBoolean(oRow.Item("FACTURA_E"))
        End If
    End Sub
    Public Sub New(ByRef dbserver As FSNDataBaseServer.Root,ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
End Class
