﻿Public Class Escenario
    Private _cargado As Boolean
    Public Property Cargado() As Boolean
        Get
            Return _cargado
        End Get
        Set(ByVal value As Boolean)
            _cargado = value
        End Set
    End Property
    Private _id As Integer
    Public Property Id() As Integer
        Get
            Return _id
        End Get
        Set(ByVal value As Integer)
            _id = value
        End Set
    End Property
    Private _propietario As String
    Public Property Propietario() As String
        Get
            Return _propietario
        End Get
        Set(ByVal value As String)
            _propietario = value
        End Set
    End Property
    Private _carpeta As String
    Public Property Carpeta() As String
        Get
            Return _carpeta
        End Get
        Set(ByVal value As String)
            _carpeta = value
        End Set
    End Property
    Private _nombre As String
    Public Property Nombre() As String
        Get
            Return _nombre
        End Get
        Set(ByVal value As String)
            _nombre = value
        End Set
    End Property
    Private _posicion As Integer
    Public Property Posicion() As Integer
        Get
            Return _posicion
        End Get
        Set(ByVal value As Integer)
            _posicion = value
        End Set
    End Property
    Private _defecto As Boolean
    Public Property Defecto() As Boolean
        Get
            Return _defecto
        End Get
        Set(ByVal value As Boolean)
            _defecto = value
        End Set
    End Property
    Private _favorito As Boolean
    Public Property Favorito() As Boolean
        Get
            Return _favorito
        End Get
        Set(ByVal value As Boolean)
            _favorito = value
        End Set
    End Property
    Private _aplicaTodas As Boolean
    Public Property AplicaTodas() As Boolean
        Get
            Return _aplicaTodas
        End Get
        Set(ByVal value As Boolean)
            _aplicaTodas = value
        End Set
    End Property
    Private _tipoVisor As Integer
    Public Property TipoVisor() As Integer
        Get
            Return _tipoVisor
        End Get
        Set(ByVal value As Integer)
            _tipoVisor = value
        End Set
    End Property   
    Private _modo As Byte
    Public Property Modo() As Byte
        Get
            Return _modo
        End Get
        Set(ByVal value As Byte)
            _modo = value
        End Set
    End Property
    Private _tieneOpcionesUsu As Boolean
    Public Property TieneOpcionesUsu() As Boolean
        Get
            Return _tieneOpcionesUsu
        End Get
        Set(ByVal value As Boolean)
            _tieneOpcionesUsu = value
        End Set
    End Property
    Private _editable As Boolean
    Public Property Editable() As Boolean
        Get
            Return _editable
        End Get
        Set(ByVal value As Boolean)
            _editable = value
        End Set
    End Property
    Private _solicitudFormularioVinculados As Dictionary(Of String, SerializableKeyValuePair(Of Integer, String))
    Public Property SolicitudFormularioVinculados() As Dictionary(Of String, SerializableKeyValuePair(Of Integer, String))
        Get
            Return _solicitudFormularioVinculados
        End Get
        Set(ByVal value As Dictionary(Of String, SerializableKeyValuePair(Of Integer, String)))
            _solicitudFormularioVinculados = value
        End Set
    End Property
    Private _escenarioFiltros As Dictionary(Of String, EscenarioFiltro)
    Public Property EscenarioFiltros() As Dictionary(Of String, EscenarioFiltro)
        Get
            Return _escenarioFiltros
        End Get
        Set(ByVal value As Dictionary(Of String, EscenarioFiltro))
            _escenarioFiltros = value
        End Set
    End Property
    Private _escenarioVistas As Dictionary(Of String, EscenarioVista)
    Private _portal As Boolean
    Public Property Portal() As Boolean
        Get
            Return _portal
        End Get
        Set(ByVal value As Boolean)
            _portal = value
        End Set
    End Property
    Public Property EscenarioVistas() As Dictionary(Of String, EscenarioVista)
        Get
            Return _escenarioVistas
        End Get
        Set(ByVal value As Dictionary(Of String, EscenarioVista))
            _escenarioVistas = value
        End Set
    End Property
    Public Sub New()
        SolicitudFormularioVinculados = New Dictionary(Of String, SerializableKeyValuePair(Of Integer, String))
        EscenarioFiltros = New Dictionary(Of String, EscenarioFiltro)
        EscenarioVistas = New Dictionary(Of String, EscenarioVista)
    End Sub
End Class


