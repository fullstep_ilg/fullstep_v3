﻿<Serializable()> _
Public Class Ppms
    Inherits Security
    ''' <summary>
    ''' Creación de una instancia de la clase
    ''' </summary>
    ''' <param name="dbserver">Objeto Root de PMDatabaseServer</param> 
    ''' <param name="isAuthenticated">Usuario atenticado sí/no</param>
    ''' <remarks>Llamada desde: fichaProveedor.aspx/MostrarPanelPPM; Tiempo máximo: 0</remarks>
    Public Sub New(ByRef dbserver As FSNDataBaseServer.Root,ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
    End Sub
    ''' <summary>
    ''' Función para obtener los PPms desde un mes inicial hasta otro final,  de un proveedor, unidad de negocio 
    ''' y  material de QA, correspondientes a una puntuación.
    ''' </summary>
    ''' <param name="Quien">1:ppm 2:cargos 3:tasa</param> 
    ''' <param name="Prove">Proveedor</param>
    ''' <param name="MatQa">Material Qa</param>
    ''' <param name="Unqa">Unidad de negocio</param>
    ''' <param name="Periodo">Periodo de la variable</param> 
    ''' <returns>Dataset con la variable de calidad de tipo PPM cargada</returns>
    ''' <remarks>Llamada desde: fichaProveedor.aspx/MostrarPanelPPM; Tiempo máximo: 0,1</remarks>
    Public Function DatosPuntuacionPPM(ByVal Quien As Integer, ByVal Prove As String, ByVal MatQa As String, ByVal Unqa As Long , ByVal Periodo As Integer) As DataSet
        Authenticate()
        Return DBServer.Puntuacion_DatosPuntuacionPPM(Quien, Prove, MatQa, Unqa, Periodo)
    End Function
End Class