<Serializable()> _
    Public Class Grupos
    Inherits Security

    Private moGrupos As Collection
    Public ReadOnly Property Grupos() As Collection
        Get
            Return moGrupos
        End Get
    End Property
    Friend Sub Add(ByVal lId As Long, ByVal oDen As MultiIdioma)
        Dim oGroup As New Grupo(DBServer, mIsAuthenticated)
        If moGrupos Is Nothing Then moGrupos = New Collection

        oGroup.Id = lId
        oGroup.CopyDen(oDen)

        moGrupos.Add(oGroup, lId.ToString)
    End Sub
    Friend Sub Add(ByRef oGrupo As Grupo)
        If moGrupos Is Nothing Then moGrupos = New Collection

        moGrupos.Add(oGrupo, oGrupo.Id.ToString)
    End Sub
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
End Class