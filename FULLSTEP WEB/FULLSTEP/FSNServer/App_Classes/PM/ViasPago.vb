﻿<Serializable()> _
Public Class ViasPago
    Inherits Security
    Private moViasPago As DataSet

    Public ReadOnly Property Data() As Data.DataSet
        Get
            Return moViasPago
        End Get

    End Property

    ''' <summary>
    ''' Procedimiento que carga los datos de las vías de pago disponibles
    ''' </summary>
    ''' <param name="sIdi">Idioma de la aplicación</param>
    ''' <param name="sCod">Código de la vía de pago</param>
    ''' <remarks>
    ''' Llamada desde: FSNWeb/App_Classes/PM/Facturas/DetalleFactura.aspx
    ''' Tiempo máximo: 0,32 seg </remarks>
    Public Sub LoadData(ByVal sIdi As String, Optional ByVal sCod As String = "")
        Authenticate()
        moViasPago = DBServer.ViasPago_Get(sIdi, sCod)
    End Sub


    Public Sub New(ByRef dbserver As FSNDataBaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub


End Class
