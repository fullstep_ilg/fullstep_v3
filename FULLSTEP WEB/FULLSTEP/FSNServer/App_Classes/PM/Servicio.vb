﻿<Serializable()> _
Public Class Servicio
    Inherits Security

    Private datosServicio As DataSet

    Public ReadOnly Property Data() As Data.DataSet
        Get
            Return datosServicio
        End Get
    End Property

    ''' <summary>
    ''' Procedimiento que carga los datos del servicio
    ''' </summary>
    ''' <param name="sServicio">Id del servicio</param>
    ''' <param name="sCampoId">Id del campo</param>
    ''' <param name="Instancia">Instancia</param>
    ''' <param name="Peticionario">Usuario conectado</param>
    ''' <remarks>
    ''' Llamada desde:Instancia.vb/ComprobarPrecondiciones      Consultas.asmx/Obtener_Servicio ; Tiempo máximo: 0,2sg
    ''' </remarks>
    Public Sub LoadData(ByVal sServicio As String, ByVal sCampoId As String, ByVal Instancia As Long, Optional ByVal Peticionario As String = "")
        Authenticate()

        datosServicio = DBServer.Servicio_Load(sServicio, sCampoId, Instancia, Peticionario)
    End Sub


    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub

End Class
