<Serializable()> _
Public Class OrganizacionesCompras
    Inherits Security

    Private Contexto As System.Web.HttpContext
    Private m_sCodOrganizacion As String
    Private moOrganizacionesCompras As DataSet

    Public ReadOnly Property Data() As Data.DataSet
        Get
            Return moOrganizacionesCompras
        End Get
    End Property

    ''' <summary>
    ''' Procedimiento que carga los datos de todas las organizaciones de compras disponibles
    ''' </summary>
    ''' <remarks>
    ''' Llamada desde: PmWeb/ArticulosServer/Page_load, PmWeb/OrganizacionComprasServer/Page_load, PmWeb/Proveedores/Page_load
    ''' Tiempo m�ximo: 1 seg</remarks>
    Public Sub LoadData(ByVal Usu As String, ByVal RestricUONSolUsuUON As Boolean, ByVal RestricUONSolPerfUON As Boolean)
        Authenticate()

        Dim ds As DataSet
        Contexto = System.Web.HttpContext.Current
        ds = CType(Contexto.Cache("dsTodasOrgCompras_" & Usu & "_" & RestricUONSolUsuUON & "_" & RestricUONSolPerfUON), DataSet)
        If ds Is Nothing Then
            ds = DBServer.OrganizacionesCompras_Load(Usu, RestricUONSolUsuUON, RestricUONSolPerfUON)
            Contexto.Cache.Insert("dsTodasOrgCompras_" & Usu & "_" & RestricUONSolUsuUON & "_" & RestricUONSolPerfUON, ds, Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration, New TimeSpan(0, System.Configuration.ConfigurationManager.AppSettings("TiempoExpiracionCacheMin"), 0))
        End If

        moOrganizacionesCompras = ds
    End Sub

    ''' <summary>
    ''' Funci�n que comprueba si una tabla de integraci�n admite multiples presupuestos
    ''' </summary>
    ''' <param name="CadenaOrgsCompras">Codigo de las organizaciones de compras</param>
    ''' <returns>�na variable booleana que detemrina si una tabla de integraci�n admite multiples presupuestos</returns>
    ''' <remarks>
    ''' Llamada desde: PmWeb/comprobarAdmiteMultiplesPres/Page_load
    ''' Tiempo m�ximo: 0,1 seg</remarks>
    Public Function OrgsAdmitenMultiplesPresupuestos(ByVal CadenaOrgsCompras As String) As Boolean
        Authenticate()
        Return DBServer.OrgsAdmitenMultiplesPresupuestos(CadenaOrgsCompras)
    End Function

    Public Sub New(ByRef dbserver As FSNDataBaseServer.Root,ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
    End Sub

End Class
