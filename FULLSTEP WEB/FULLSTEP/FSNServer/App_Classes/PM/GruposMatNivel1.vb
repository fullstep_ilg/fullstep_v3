<Serializable()> _
        Public Class GruposMatNivel1

    Inherits Security


    Private mCol As New Collection
    Public Function Add(ByVal sCod As String, ByVal sDen As String) As GrupoMatNivel1

        Dim oNewObject As New GrupoMatNivel1(DBServer, mIsAuthenticated)

        With oNewObject

            .Cod = sCod
            .Den = sDen
        End With

        mCol.Add(oNewObject, sCod)
        Return oNewObject
    End Function

    Public Function Add(ByVal oGrupoMatNivel1 As GrupoMatNivel1) As GrupoMatNivel1
        mCol.Add(oGrupoMatNivel1, oGrupoMatNivel1.Cod)
        Return oGrupoMatNivel1
    End Function

    Public Function Item(ByVal sCod As String) As GrupoMatNivel1
        Return mCol(sCod)

    End Function

    ''' <summary>
    ''' Procedimiento que carga todos los grupos de material a partir de unos filtros dados
    ''' </summary>
    ''' <param name="iNumMaximo">N�mero m�ximo a cargar</param>
    ''' <param name="sIdi">Idioma del usuario</param>
    ''' <param name="CaracteresInicialesCod">C�digo de los car�cteres iniciales</param>
    ''' <param name="CaracteresInicialesDen">Denominaci�n de los car�ceteres iniciales</param>
    ''' <param name="CoincidenciaTotal">Variable booleana que indica si debe haber coincidencia en loos car�cteres</param>
    ''' <param name="OrdenadosPorDen">Variable booleana que indica si deben estar ordenados por denominaci�n</param>
    ''' <remarks>
    ''' Llamada desde: PmWeb/articulosserver/Page_Load, PmWeb/proveedores/Page_Load, PmWeb/validararticulos/Page_Load, PmWeb/campos/Page_Load, PmWeb/desglose/Page_Load y CargarValoresDefecto, PmWeb/impexp/denGS
    ''' Tiempo m�ximo: 0,5 seg</remarks>
    Public Sub CargarTodosLosGruposMatDesde(ByVal iNumMaximo As Integer, Optional ByVal sIdi As String = "SPA", Optional ByVal CaracteresInicialesCod As String = Nothing, Optional ByVal CaracteresInicialesDen As String = Nothing, Optional ByVal CoincidenciaTotal As Boolean = False, Optional ByVal OrdenadosPorDen As Boolean = False)
        Dim oRow As DataRow
        Dim oData As DataSet
        Authenticate()
        
        oData = DBServer.GruposMatNivel1_CargarTodosLosGruposMatDesde(iNumMaximo, CaracteresInicialesCod, CaracteresInicialesDen, CoincidenciaTotal, sIdi, OrdenadosPorDen)

        If oData.Tables(0).Rows.Count = 0 Then

        Else
            For Each oRow In oData.Tables(0).Rows
                Me.Add(oRow.Item("COD"), oRow.Item("G1DEN"))
            Next

        End If
    End Sub

    Public Sub New(ByRef dbserver As FSNDataBaseServer.Root,ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
    End Sub
End Class
