<Serializable()> _
        Public Class GruposMatNivel3
    Inherits Security


    Private msGMN1Cod As String
    Private msGMN2Cod As String
    Private mCol As New Collection

    Public Function Add(ByVal sGMN1Cod As String, ByVal sGMN1Den As String, ByVal sGMN2Cod As String, ByVal sGMN2Den As String, ByVal sCod As String, ByVal sDen As String) As GrupoMatNivel3

        Dim oNewObject As New GrupoMatNivel3(DBServer, mIsAuthenticated)

        With oNewObject

            .Cod = sCod
            .Den = sDen
            .GMN1Cod = sGMN1Cod
            .GMN1Den = sGMN1Den
            .GMN2Cod = sGMN2Cod
            .GMN2Den = sGMN2Den

        End With

        mCol.Add(oNewObject, sCod)
        Return oNewObject
    End Function

    Public Function Add(ByVal oGrupoMatNivel3 As GrupoMatNivel3) As GrupoMatNivel3
        mCol.Add(oGrupoMatNivel3, oGrupoMatNivel3.Cod)
        Return oGrupoMatNivel3
    End Function
    Public Function Item(ByVal sCod As String) As GrupoMatNivel3
        Return mCol(sCod)

    End Function

    Property GMN1Cod() As String
        Get
            Return msGMN1Cod
        End Get
        Set(ByVal Value As String)
            msGMN1Cod = Value
        End Set
    End Property

    Property GMN2Cod() As String
        Get
            Return msGMN2Cod
        End Get
        Set(ByVal Value As String)
            msGMN2Cod = Value
        End Set
    End Property


    Public Sub New(ByRef dbserver As FSNDataBaseServer.Root,ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
    End Sub
End Class
