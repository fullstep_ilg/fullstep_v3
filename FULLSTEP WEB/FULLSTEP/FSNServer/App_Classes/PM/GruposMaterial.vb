<Serializable()> _
        Public Class GruposMaterial
    Inherits Security

    Private moData As DataSet

    Public ReadOnly Property Data() As Data.DataSet
        Get
            Return moData
        End Get

    End Property

    ''' <summary>
    ''' Procedimiento que carga los datos de los grupos de materiales
    ''' </summary>
    ''' <param name="sGMN1">Codigo de material, nivel 1</param>
    ''' <param name="sGMN2">Codigo de material, nivel 2</param>
    ''' <param name="sGMN3">Codigo de material, nivel 3</param>
    ''' <param name="sGMN4">Codigo de material, nivel 4</param>
    ''' <param name="sIdi">Idioma del usuario</param>
    ''' <param name="sPer">Codigo de persona</param>
    ''' <param name="iMaterialQA">Identificador del material de QA</param>
    ''' <param name="sCodProve">Codigo de proveedor</param>
    ''' <remarks>
    ''' Llamada desde: PMWeb/parametrosGenerales/CargaRArbolMateriales, PMWeb/materiales/Page_PreRender, 
    ''' tiempo m�ximo: 0,3 seg</remarks>
    Public Sub LoadData(Optional ByVal sGMN1 As String = Nothing, Optional ByVal sGMN2 As String = Nothing, _
            Optional ByVal sGMN3 As String = Nothing, Optional ByVal sGMN4 As String = Nothing, Optional ByVal sIdi As String = "SPA", _
            Optional ByVal sPer As String = Nothing, Optional ByVal iMaterialQA As Integer = 0, Optional ByVal PMRestricMatUsu As Boolean = False, _
            Optional ByVal sCodProve As String = Nothing, Optional ByVal iNivelSeleccion As Integer = 0)
        Authenticate()
        moData = DBServer.GruposMaterial_Get(sGMN1, sGMN2, sGMN3, sGMN4, sIdi, sPer, iMaterialQA, PMRestricMatUsu, sCodProve, iNivelSeleccion)
    End Sub

    ''' <summary>
    ''' Procedimiento que Modifica la notificacion a los usuarios de QA con los datos pasados
    ''' </summary>
    ''' <param name="oDS">DataSet con los datos que se deben modificar</param>
    ''' <remarks>
    ''' Llamada desde: PMWeb/Parametrosgenerales/cmdAceptar_CLick
    ''' Tiempo m�ximo: 0,75 seg</remarks>
    Public Sub ModificarNotificacionUsuariosQA(ByVal oDS As DataSet)
        Authenticate()
        DBServer.GruposMaterial_ModificarNotificacionUsuariosQA(oDS)
    End Sub

    Public Sub New(ByRef dbserver As FSNDataBaseServer.Root,ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
    End Sub

    ''' <summary>
    ''' Funcion que obtiene el GMN1 relacionado a la PYME
    ''' </summary>
    ''' <param name="lIdPyme">ID de la pyme</param>
    ''' <returns>Cadena con el GMN1 de la pyme</returns>
    ''' <remarks>Llamada desde=ParametrosGenerales.vb // Materiales.aspx.vb // articulosserver.aspx.vb 
    ''' Tiempo m�ximo=0,1seg.</remarks>
    Public Function DevolverGMN1_PYME(ByVal lIdPyme As Long) As String
        Authenticate()
        Return DBServer.GruposMaterial_DevolverGMN1_PYME(lIdPyme)
    End Function
End Class
