<Serializable()> _
        Public Class GrupoMatNivel1
    Inherits Security
    Implements IGmn

    Private msGmnQA As Integer
    Private moData As DataSet
    Private msCod As String
    Private msDen As String

    Private moGruposMatNivel2 As GruposMatNivel2
    Private _children As Dictionary(Of String, IGmn)
    Private _hasChild As Boolean
    Private _parent As IGmn

    Public Function title() As String Implements IGmn.title
        Return msCod & " - " & msDen
    End Function

    Public ReadOnly Property key As String Implements IGmn.key
        Get
            Return msGmnQA & "###" & msCod
        End Get
    End Property

    Public Property children() As Dictionary(Of String, IGmn) Implements IGmn.children
        Get
            If _children Is Nothing Then
                _children = New Dictionary(Of String, IGmn)
                moData = DBServer.GruposMatNivel2_CargarMaterialesProve(Me.Cod, user.QARestProvMaterial, user.CodPersona, user.Idioma, msGmnQA)
                For Each oRow In moData.Tables(0).Rows
                    Dim oMat As GrupoMatNivel2 = New GrupoMatNivel2(Me.DBServer, Me.mIsAuthenticated)
                    oMat.user = Me.user
                    oMat.gmnQAcod = Me.gmnQAcod
                    oMat.GMN1Cod = oRow.Item("GMN1")
                    oMat.Cod = oRow.Item("COD")
                    oMat.Den = oRow.Item("DEN")
                    oMat.parent = Me
                    oMat.EstructuraMateriales = Me.EstructuraMateriales
                    _children.Add(oMat.key, oMat)
                Next
            End If
            Return _children
        End Get
        Set(value As Dictionary(Of String, IGmn))
            _children = value
        End Set
    End Property

    Public Function child(node As IGmn) As IGmn Implements IGmn.child
        If children.ContainsKey(node.key) Then
            Return children(node.key)
        Else
            For Each hijo As IGmn In children.Values
                If hijo.gmn2Cod = node.gmn2Cod Then
                    Return hijo.child(node)
                End If
            Next
        End If
        Return Nothing
    End Function

    Public ReadOnly Property hasChildren As Boolean Implements IGmn.hasChildren
        Get
            Return True
        End Get
    End Property

    Public Function nivel() As Integer Implements IGmn.nivel
        Return 1
    End Function

    Public Property parent As IGmn Implements IGmn.parent
        Get
            Return _parent
        End Get
        Set(value As IGmn)
            _parent = value
        End Set
    End Property

    Public Function topparent() As IGmn Implements IGmn.topParent
        If Me.EstructuraMateriales.TipoEstructuraMateriales = Operadores.FiltroMaterial.FiltroMatGS Then
            Return Me
        Else
            Return Me.parent
        End If
    End Function


    Private _e As EstructuraMateriales
    Public Property EstructuraMateriales() As EstructuraMateriales Implements IGmn.Estructura
        Get
            Return _e
        End Get
        Set(value As EstructuraMateriales)
            _e = value
        End Set
    End Property


    Property Cod() As String
        Get
            Return msCod
        End Get
        Set(ByVal Value As String)
            msCod = Value
        End Set
    End Property

    Property Den() As String Implements IGmn.Den
        Get
            Return msDen
        End Get
        Set(ByVal Value As String)
            msDen = Value
        End Set
    End Property


    Property GruposMatNivel2() As GruposMatNivel2
        Get
            Return moGruposMatNivel2

        End Get
        Set(ByVal Value As GruposMatNivel2)
            moGruposMatNivel2 = Value
        End Set
    End Property

    Public ReadOnly Property Data() As Data.DataSet
        Get
            Return moData
        End Get

    End Property

    ''' <summary>
    ''' Carga los grupos de material.
    ''' </summary>
    ''' <param name="sGMN1">Grupo de material de nivel 1</param>
    ''' <param name="sGMN2">Grupo de material de nivel 2</param>
    ''' <param name="sGMN3">Grupo de material de nivel 3</param>
    ''' <param name="sGMN4">Grupo de material de nivel 4</param>
    ''' <param name="sIdi">Idioma del usuario</param>
    ''' <remarks>LLamada desde: VisorContratos.aspx, ConfigurarFiltrosContratos.aspx; Tiempo m�x: 0,1 sg</remarks>
    Public Sub LoadData(Optional ByVal sGMN1 As String = Nothing, Optional ByVal sGMN2 As String = Nothing, Optional ByVal sGMN3 As String = Nothing, Optional ByVal sGMN4 As String = Nothing, Optional ByVal sIdi As String = "SPA", Optional ByVal iNivelSeleccion As Integer = 0)
        Authenticate()
        moData = DBServer.GruposMaterial_Get(sGMN1, sGMN2, sGMN3, sGMN4, sIdi, , , , , iNivelSeleccion)
    End Sub

    ''' <summary>
    ''' Procedimiento que carga todos los grupos de material a partir de unos filtros dados
    ''' </summary>
    ''' <param name="iNumMaximo">N�mero m�ximo a cargar</param>
    ''' <param name="sIdi">Idioma del usuario</param>
    ''' <param name="CaracteresInicialesCod">C�digo de los car�cteres iniciales</param>
    ''' <param name="CaracteresInicialesDen">Denominaci�n de los car�ceteres iniciales</param>
    ''' <param name="CoincidenciaTotal">Variable booleana que indica si debe haber coincidencia en loos car�cteres</param>
    ''' <param name="OrdenadosPorDen">Variable booleana que indica si deben estar ordenados por denominaci�n</param>
    ''' <remarks>
    ''' Llamada desde: PmWeb/articulosserver/Page_Load, PmWeb/proveedores/Page_Load, PmWeb/validararticulos/Page_Load, PmWeb/campos/Page_Load, PmWeb/desglose/Page_Load y CargarValoresDefecto, PmWeb/impexp/denGS
    ''' Tiempo m�ximo: 0,5 seg</remarks>
    Public Sub CargarTodosLosGruposMatDesde(ByVal iNumMaximo As Integer, Optional ByVal sIdi As String = "SPA", Optional ByVal CaracteresInicialesCod As String = Nothing, Optional ByVal CaracteresInicialesDen As String = Nothing, Optional ByVal CoincidenciaTotal As Boolean = False, Optional ByVal OrdenadosPorDen As Boolean = False)
        Dim oRow As DataRow
        Authenticate()

        moData = DBServer.GrupoMatNivel1_CargarTodosLosGruposMatDesde(iNumMaximo, msCod, CaracteresInicialesCod, CaracteresInicialesDen, CoincidenciaTotal, sIdi, OrdenadosPorDen)

        If moData.Tables(0).Rows.Count = 0 Then
            moGruposMatNivel2 = New GruposMatNivel2(DBServer, mIsAuthenticated)
        Else
            moGruposMatNivel2 = New GruposMatNivel2(DBServer, mIsAuthenticated)
            For Each oRow In moData.Tables(0).Rows
                moGruposMatNivel2.Add(msCod, oRow.Item("G1DEN"), oRow.Item("COD"), oRow.Item("G2DEN"))
            Next
        End If
    End Sub
    Public Function incluye(o As IGmn) As Boolean Implements IGmn.incluye
        If o.nivel >= Me.nivel Then
            Return o.gmn1Cod = Me.cod1
        End If
        Return False
    End Function

    Public Function contiene(o As IGmn) As Boolean Implements IGmn.contiene
        If o.nivel > Me.nivel Then
            Return o.gmn1Cod = Me.cod1
        End If
        Return False
    End Function

    Public Property gmnQAcod As Integer Implements IGmn.GmnQAcod
        Get
            Return msGmnQA
        End Get
        Set(value As Integer)
            msGmnQA = value
        End Set
    End Property

    Public Property cod1 As String Implements IGmn.gmn1Cod
        Get
            Return Cod
        End Get
        Set(value As String)
            Cod = value
        End Set
    End Property
    Public Property cod2 As String Implements IGmn.gmn2Cod
        Get
            Return Nothing
        End Get
        Set(value As String)
            value = Nothing
        End Set
    End Property
    Public Property cod3 As String Implements IGmn.gmn3Cod
        Get
            Return Nothing
        End Get
        Set(value As String)

        End Set
    End Property
    Public Property cod4 As String Implements IGmn.gmn4Cod
        Get
            Return Nothing
        End Get
        Set(value As String)

        End Set
    End Property

    Private _user As FSNServer.User
    Public Property user As FSNServer.User Implements IGmn.User
        Get
            Return _user
        End Get
        Set(value As FSNServer.User)
            _user = value
        End Set
    End Property
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
End Class
