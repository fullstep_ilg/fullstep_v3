<Serializable()> _
        Public Class GruposMatNivel2
    Inherits Security


    Private msGMN1Cod As String

    Private mCol As New Collection
    Public Function Add(ByVal sGMN1Cod As String, ByVal sGMN1Den As String, ByVal sCod As String, ByVal sDen As String) As GrupoMatNivel2

        Dim oNewObject As New GrupoMatNivel2(DBServer, mIsAuthenticated)

        With oNewObject

            .Cod = sCod
            .Den = sDen
            .GMN1Cod = sGMN1Cod
            .GMN1Den = sGMN1Den
        End With

        mCol.Add(oNewObject, sCod)
        Return oNewObject
    End Function

    Public Function Add(ByVal oGrupoMatNivel2 As GrupoMatNivel2) As GrupoMatNivel2
        mCol.Add(oGrupoMatNivel2, oGrupoMatNivel2.Cod)
        Return oGrupoMatNivel2
    End Function
    Public Function Item(ByVal sCod As String) As GrupoMatNivel2
        Return mCol(sCod)

    End Function



    Property GMN1Cod() As String
        Get
            Return msGMN1Cod
        End Get
        Set(ByVal Value As String)
            msGMN1Cod = Value
        End Set
    End Property


    Public Sub New(ByRef dbserver As FSNDataBaseServer.Root,ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
    End Sub
End Class
