
<Serializable()> _
Public Class OrganizacionCompras
    Inherits Security

    Private msCod As String
    Private msDen As String

    Private moOrganizacionCompras As DataSet

    Public Property Cod() As String
        Get
            Return msCod
        End Get

        Set(ByVal Value As String)
            msCod = Value
        End Set
    End Property

    Public Property Den() As String
        Get
            Return msDen
        End Get

        Set(ByVal Value As String)
            msDen = Value
        End Set
    End Property

    Public ReadOnly Property Data() As Data.DataSet
        Get
            Return moOrganizacionCompras
        End Get

    End Property

    ''' <summary>
    ''' Procedimiento que carga los datos de organizaci�n de compras dado su c�digo
    ''' </summary>
    ''' <remarks>Llamada desde: PmWeb/ArticulosServer/Page_load, PmWeb/campos/page_load, PmWeb/desglose/CargarValoresDefecto PagE_load; Tiempo m�ximo: 0,25 seg</remarks>
    Public Sub LoadData()
        Authenticate()
        moOrganizacionCompras = DBServer.OrganizacionCompras_Get(msCod)
    End Sub

    ''' <summary>
    ''' Funci�n que comprueba si una tabla de integraci�n admite multiples presupuestos
    ''' </summary>
    ''' <returns>�na variable booleana que detemrina si una tabla de integraci�n admite multiples presupuestos</returns>
    ''' <remarks>
    ''' Llamada desde: PmWeb/PresAsig/Page_load
    ''' Tiempo m�ximo: 0,1 seg</remarks>
    Public Function AdmiteMultiplesPresupuestos() As Boolean
        Authenticate()
        Return DBServer.AdmiteMultiplesPresupuestos(msCod)
    End Function

    Public Sub New(ByRef dbserver As FSNDataBaseServer.Root,ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
    End Sub

End Class
