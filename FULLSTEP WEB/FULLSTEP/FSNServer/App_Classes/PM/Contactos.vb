<Serializable()> _
    Public Class Contactos
    Inherits Security

    Private moData As DataSet
    Private msProveedor As String

    Public Property Proveedor() As String
        Get
            Return msProveedor
        End Get
        Set(ByVal Value As String)
            msProveedor = Value
        End Set
    End Property
    Public ReadOnly Property Data() As Data.DataSet
        Get
            Return moData
        End Get

    End Property

    ''' <summary>
    ''' Carga los datos de los contactos dado un id
    ''' </summary>
    ''' <param name="lId">Id de los contactos a cargar</param>
    ''' <remarks>
    ''' Llamada desde: PmWeb/contactosserver/Page_load
    ''' Tiempo m�ximo: 0,15 seg</remarks>
    Public Sub LoadData(Optional ByVal lId As Long = Nothing)
        Authenticate()
        moData = DBServer.Contactos_Load(msProveedor, lId)
    End Sub


    Public Sub New(ByRef dbserver As FSNDataBaseServer.Root,ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
    End Sub


End Class
