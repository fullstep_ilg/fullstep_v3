﻿Public Class TipoPedido
    Inherits Security

    ''' <summary>
    ''' Obtiene el detalle de un tipo de pedido (cod, denominación, concepto, almacenamiento y recepcion)
    ''' </summary>
    ''' <param name="sCod">Cod del tipo de pedido</param>
    ''' <param name="sIdioma">Idioma del usuario</param>
    ''' <returns>El detalle del tipo de pedido</returns>
    ''' <remarks>Llamada desde: detalletipopedido.aspx; Tiempo máximo: 1 sg;</remarks>
    Public Function BuscarDetalleTipoPedido(ByVal sCod As String, ByVal sIdioma As String) As DataSet
        Authenticate()
        BuscarDetalleTipoPedido = DBServer.TipoPedido_Detalle(sCod, sIdioma)
    End Function

    ''' <summary>
    ''' Constructor de la clase tipo pedido
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si está autenticado</param>
    Public Sub New(ByRef dbserver As FSNDataBaseServer.Root,ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
    End Sub
End Class
