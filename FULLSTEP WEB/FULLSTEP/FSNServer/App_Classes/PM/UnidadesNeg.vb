<Serializable()> _
    Public Class UnidadesNeg
    Inherits Security

    Private moData As DataSet
    Private moDataUsu As DataSet

    Public ReadOnly Property Data() As Data.DataSet
        Get
            Return moData
        End Get
    End Property
    Public ReadOnly Property UsuData() As Data.DataSet
        Get
            Return moDataUsu
        End Get
    End Property
    '************************************************************************
    '*** Descripci�n:   Procedure que carga el arbol con las Unidades de negocio
    '*** Par�metros de entrada:     sIdi--> c�digo de idioma con el que se cargar�n las denominaciones de los nodos
    '                               lIDPadre-> Id del nodo padre
    '                               lIDAbuelo-> id del nodo abuelo
    '                               bBaja-->(true/false) Si se muestra o no los nodos que estan de bajas
    '                               lIdPyme--> Id de la pyme del usuario, si estuviera trabajando en modo pyme, 0 si no.
    '*** Llamada desde: UnidadesNegocio.aspx.vb  						                              
    '*** Tiempo m�ximo: 0,43seg  						                                       
    '************************************************************
    Public Sub LoadData(ByVal sIdi As String, ByVal Usuario As String, Optional ByVal lIDPadre As Long = 0, Optional ByVal lIDAbuelo As Long = 0, Optional ByVal bBaja As Boolean = False, Optional ByVal lIDPyme As Long = 0)
        Authenticate()
        moData = DBServer.UnidadesNegocio_Get(sIdi, Usuario, lIDPadre, lIDAbuelo, bBaja, lIDPyme)
    End Sub
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
    'ilg 26/01/2010
    'Que hace:
    '   Devolver un dataset con registro/s de unidades qa por usuario
    'Param. Entrada:
    '   Usuario     Usuario
    '   Idioma      Idioma. Saca codigo + den en idioma usuario
    '   MaxNivel    Maximo 3 niveles. Si solo 2 en la instalaci�n te ahorras 1 union
    '   Permiso1,Permiso2,Permiso3 y Permiso4: Son los posibles permisos que debe cumplir el usuario
    'sobre las unidades de negocio que deseamos obtener
    'Param. Salida:
    '   Lo dicho un recordset
    'Llamada desde: 
    '   noConformidad.aspx altanoConformidad.aspx detallenoConformidad.aspx
    Public Sub UsuLoadData(ByVal Usuario As String, ByVal Idioma As String, ByVal MaxNivel As Integer, ByVal Permiso1 As Integer, Optional ByVal Permiso2 As Integer = 0)
        Authenticate()
        moDataUsu = DBServer.UnidadesNegocioUsu_Get(Usuario, Idioma, MaxNivel, Permiso1, Permiso2)
    End Sub
    Public Function UsuLoadData_Escalacion(ByVal Usuario As String, ByVal IdSolicitud As Long, ByVal Idioma As String) As DataTable
        Authenticate()
        Return DBServer.UnidadesNegocioUsu_Escalacion_Get(Usuario, IdSolicitud, Idioma)
    End Function
    'jbg 260309
    'Que hace:
    '   Devolver un recordset con el registro de unidades qa indicado en el idioma indicado
    'Param. Entrada:
    '   Idioma      Idioma. Saca codigo + den en idioma usuario
    '   UnQa         Unidad a cargar. Caso de detalle de nc cerrada/prove de baja/...
    'Param. Salida:
    '   Lo dicho un recordset
    'Llamada desde: 
    '   detallenoConformidad.aspx
    Public Function LoadUnQa(ByVal Idioma As String, ByVal UnQa As Long) As DataTable
        Authenticate()
        LoadUnQa = DBServer.UnidadNegocio_Get(Idioma, UnQa).Tables(0)
    End Function
    ''' <summary>
    ''' Tal y como esta montada la tabla UNQA q te encuentras a veces con q el registro X esta en campo id 
    ''' como el mismo, otras en campo unqa1 como padre y en otras en campo unqa2 como abuelo, pues hacer con 
    ''' una select lo necesario para tarea 1021 pues sal�a select enorme. Mejor select sencilla y aqui proceso.
    ''' Nivel0 den
    ''' Nivel1 cod - den
    ''' Nivel2 cod1 - cod2 - den
    ''' Nivel3 cod1 - cod2 - cod3 - den
    ''' </summary>
    ''' <param name="TextBlanco">optional. si el combo va tener una linea en blanco o no (un espacio o un textoX hoy es espacio)</param>
    ''' <param name="RepetirPorPermiso">optional. Si es para cargar un combo pasaremos false, si es para obtener una lista de permisos sobre las
    ''' unidades de negocio pasaremos true</param>
    ''' <returns>datatable explicado en descripci�n. Notar q una linea de bbdd con departamento,planta y divisi�n genera 3 lineas en el datatable</returns>
    ''' <remarks>Llamada desde:altaNoConformidades.aspx detalleNoConformidades.aspx NoConformidades.aspx; Tiempo m�ximo:0,1</remarks>
    Public Function MontaDataSetUnaBanda(Optional ByVal TextBlanco As String = "", Optional ByVal RepetirPorPermiso As Boolean = False) As DataTable
        Dim ds As New DataSet
        Dim dt As DataTable
        Dim newRow As DataRow
        Dim EstaEnGrupo As Integer = -1
        Dim EstaEnDivi As Integer = -1
        Dim EstaEnPlta As Integer = -1
        Dim EstaEnDepartamento As Integer = -1

        dt = ds.Tables.Add("Banda0")
        dt.Columns.Add("ID", System.Type.GetType("System.Int32"))
        dt.Columns.Add("DEN", System.Type.GetType("System.String"))
        dt.Columns.Add("IDPERMISO", System.Type.GetType("System.String"))

        If TextBlanco <> "" Then
            newRow = dt.NewRow

            newRow("ID") = -1
            newRow("DEN") = TextBlanco
            newRow("IDPERMISO") = 0

            dt.Rows.Add(newRow)
        End If

        If moDataUsu.Tables.Count > 0 Then
            For Each row As DataRow In moDataUsu.Tables(0).Rows
                If (Not IsDBNull(row("G_ID"))) AndAlso EstaEnGrupo <> row("G_ID") Then
                    newRow = dt.NewRow

                    newRow("ID") = row("G_ID")
                    newRow("DEN") = row("G_I")
                    newRow("IDPERMISO") = newRow("ID") & "#" & row("Permiso")

                    dt.Rows.Add(newRow)

                    If Not RepetirPorPermiso Then
                        EstaEnGrupo = row("G_ID")
                    End If
                End If

                If (Not IsDBNull(row("D_ID"))) AndAlso EstaEnDivi <> row("D_ID") Then
                    newRow = dt.NewRow

                    newRow("ID") = row("D_ID")
                    newRow("DEN") = row("D_C") & " - " & row("D_I")
                    newRow("IDPERMISO") = newRow("ID") & "#" & row("Permiso")

                    dt.Rows.Add(newRow)

                    If Not RepetirPorPermiso Then
                        EstaEnDivi = row("D_ID")
                    End If
                End If

                If (Not IsDBNull(row("P_ID"))) AndAlso EstaEnPlta <> row("P_ID") Then
                    newRow = dt.NewRow

                    newRow("ID") = row("P_ID")
                    newRow("DEN") = row("D_C") & " - " & row("P_C") & " - " & row("P_I")
                    newRow("IDPERMISO") = newRow("ID") & "#" & row("Permiso")

                    dt.Rows.Add(newRow)

                    If Not RepetirPorPermiso Then
                        EstaEnPlta = row("P_ID")
                    End If
                End If

                If (Not IsDBNull(row("DE_ID"))) AndAlso EstaEnDepartamento <> row("DE_ID") Then
                    newRow = dt.NewRow

                    newRow("ID") = row("DE_ID")
                    newRow("DEN") = row("D_C") & " - " & row("P_C") & " - " & row("DE_C") & " - " & row("DE_I")
                    newRow("IDPERMISO") = newRow("ID") & "#" & row("Permiso")

                    dt.Rows.Add(newRow)

                    If Not RepetirPorPermiso Then
                        EstaEnDepartamento = row("DE_ID")
                    End If
                End If
            Next
        End If

        MontaDataSetUnaBanda = ds.Tables(0)
    End Function
    ''' <summary>
    ''' Funci�n que devuleve un dataset que contiene en una sola tabla todas las unidades de negoico de QA indicando si el usuario tiene o no acceso
    ''' _hds: El dataset se cargar� posteriormente en un HierarchicalDataSet. Para que pueda establecer la jerarquia la tabla del dataset tiene un campo que es la PK y otro que es la FK.
    ''' </summary>
    ''' <param name="sUsu">C�digo del usuario, para obtener si tiene acceso o no a las unqas</param>
    ''' <param name="sIdi">Idioma del usuario</param>
    ''' <param name="iPyme">Si la aplicaci�n est� en modo Pymes tiene el c�dugo de la Pyme del usuario</param>
    ''' <param name="Permiso1">se pasa el permiso del que se desean recoger las unidades de negocio. Pueden llegar a ser 3 tipos de permiso, consulta, edici�n y en el caso de las no conformidades de reabrir</param>
    ''' <param name="Permiso2">si es necesario un segundo tipo de permiso</param>
    ''' <param name="bNoConf">si se carga el arbol de unidades para no conform o para panel calidad</param>
    ''' <returns>Dataset</returns>
    ''' <remarks>Llamadas: PMWeb/script/puntuacion/panelCalidadFiltro.aspx.vb</remarks>
    Public Function UsuGetData_hds(ByVal sUsu As String, ByVal Permiso1 As Integer, Optional ByVal Permiso2 As Integer = 0, Optional ByVal sIdi As String = "SPA", Optional ByVal iPyme As Integer = 0, Optional ByVal bNoConf As Boolean = False) As DataSet
        Authenticate()
        Return DBServer.UnidadesNeg_UsuGetData_hds(sUsu, sIdi, iPyme, Permiso1, Permiso2, bNoConf)
    End Function
    ''' <summary>
    ''' Funci�n que devuleve un dataset que contiene en una sola tabla todas las unidades de negoico de QA indicando si el proveedor tiene o no acceso
    ''' _hds: El dataset se cargar� posteriormente en un HierarchicalDataSet. Para que pueda establecer la jerarquia la tabla del dataset tiene un campo que es la PK y otro que es la FK.
    ''' </summary>
    ''' <param name="sProve">C�digo del proveedor, para obtener si tiene acceso o no a las unqas</param>
    ''' <param name="sIdi">Idioma del usuario</param>
    ''' <param name="iPyme">Si la aplicaci�n est� en modo Pymes tiene el c�dugo de la Pyme del usuario</param>
    ''' <returns>Dataset</returns>
    ''' <remarks>Llamadas: PMWeb/script/puntuacion/panelCalidad.aspx.vb</remarks>
    Public Function ProveGetData_hds(ByVal sProve As String, Optional ByVal sIdi As String = "SPA", Optional ByVal iPyme As Integer = 0) As DataSet
        Authenticate()
        Return DBServer.UnidadesNeg_ProveGetData_hds(sProve, sIdi, iPyme)
    End Function
End Class
