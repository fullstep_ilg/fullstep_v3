﻿Imports Fullstep.FSNLibrary.Operadores

Public Class EstructuraMateriales
    Inherits Security

    Private _mapamateriales As Dictionary(Of String, IGmn)
    Private _materiales As Dictionary(Of String, IGmn)
    Private _gmnSeleccionados As New List(Of IGmn)
    Private _tipomaterial As FiltroMaterial
    Private _user As FSNServer.User
    Public Property User As FSNServer.User
        Get
            Return _user
        End Get
        Set(value As FSNServer.User)
            _user = value
        End Set
    End Property
    Private Function createGmn(gmnqa As Integer, Optional gmn1 As String = Nothing, Optional gmn2 As String = Nothing, Optional gmn3 As String = Nothing, Optional gmn4 As String = Nothing, Optional den As String = "") As IGmn
        Dim o As IGmn
        If gmn1 Is Nothing Then
            o = New GrupoMatQA(Me.DBServer, Me.mIsAuthenticated)
        ElseIf gmn2 Is Nothing Then
            o = New GrupoMatNivel1(Me.DBServer, Me.mIsAuthenticated)
            o.gmn1Cod = gmn1
        ElseIf gmn3 Is Nothing Then
            o = New GrupoMatNivel2(Me.DBServer, Me.mIsAuthenticated)
            o.gmn1Cod = gmn1
            o.gmn2Cod = gmn2
        ElseIf gmn4 Is Nothing Then
            o = New GrupoMatNivel3(Me.DBServer, Me.mIsAuthenticated)
            o.gmn1Cod = gmn1
            o.gmn2Cod = gmn2
            o.gmn3Cod = gmn3
        Else
            o = New GrupoMatNivel4(Me.DBServer, Me.mIsAuthenticated)
            o.gmn1Cod = gmn1
            o.gmn2Cod = gmn2
            o.gmn3Cod = gmn3
            o.gmn4Cod = gmn4
        End If
        o.GmnQAcod = gmnqa
        o.Den = den
        o.User = Me.User
        Return o
    End Function
    Public Property TipoEstructuraMateriales As FiltroMaterial
        Get
            Return _tipomaterial
        End Get
        Set(value As FiltroMaterial)
            _tipomaterial = value
        End Set
    End Property
    Public Property Materiales As Dictionary(Of String, IGmn)
        Get
            Return _materiales
        End Get
        Set(value As Dictionary(Of String, IGmn))

        End Set
    End Property

    Default Public ReadOnly Property children(key As String) As Dictionary(Of String, IGmn)
        Get
            If key <> "" Then
                Return Me.getNodo(key).children()
            Else
                Select Case Me.TipoEstructuraMateriales
                    Case FiltroMaterial.FiltroMatGS
                        If _materiales Is Nothing Then
                            CargarMaterialesGS(User.Idioma)
                        End If
                    Case FiltroMaterial.FiltroMatQAGS
                        If _materiales Is Nothing Then
                            CargarMaterialesQAGS(User.Idioma)
                        End If
                    Case Else
                        If _materiales Is Nothing Then
                            CargarMaterialesQA("", User.Idioma)
                        End If
                End Select
                Return _materiales
            End If
        End Get

    End Property
    Private Sub CargarMaterialesQA(ByVal sFiltro As String, Optional ByVal sIdi As String = "SPA")
        Authenticate()
        _materiales = New Dictionary(Of String, IGmn)
        _mapamateriales = New Dictionary(Of String, IGmn)
        Dim ds As DataSet = DBServer.MaterialesQA_Get(sIdi, , True, Me.User.Pyme, User.QARestProvMaterial, User.CodPersona)
        For Each r As DataRow In ds.Tables(0).Rows
            Dim m As GrupoMatQA = New GrupoMatQA(Me.DBServer, Me.mIsAuthenticated)
            m.Cod = r("ID")
            m.Den = r("DEN_" & sIdi)
            m.EstructuraMateriales = Me
            _materiales.Add(m.key, m)
            _mapamateriales.Add(m.key, m)
        Next
    End Sub

    Private Sub CargarMaterialesQAGS(Optional ByVal sIdi As String = "SPA")
        Authenticate()
        _materiales = New Dictionary(Of String, IGmn)
        _mapamateriales = New Dictionary(Of String, IGmn)
        Dim ds As DataSet = DBServer.MaterialesQA_Get(sIdi, , True, Me.User.Pyme, User.QARestProvMaterial, User.CodPersona)
        For Each r As DataRow In ds.Tables(0).Rows
            Dim m As GrupoMatQA = New GrupoMatQA(Me.DBServer, Me.mIsAuthenticated)
            m.Cod = r("ID")
            m.Den = r("DEN_" & sIdi)
            m.EstructuraMateriales = Me
            _materiales.Add(m.key, m)
            _mapamateriales.Add(m.key, m)
        Next
    End Sub

    Private Sub CargarMaterialesGS(Optional ByVal sIdi As String = "SPA")
        Dim oRow As DataRow
        Dim oData As DataSet
        Authenticate()
        _materiales = New Dictionary(Of String, IGmn)
        _mapamateriales = New Dictionary(Of String, IGmn)
        oData = DBServer.GruposMatNivel1_CargarMaterialesProve(User.QARestProvMaterial, User.CodPersona, User.Idioma, 0)


        For Each oRow In oData.Tables(0).Rows
            Dim m As GrupoMatNivel1 = New GrupoMatNivel1(Me.DBServer, Me.mIsAuthenticated)
            m.user = Me.User
            m.Cod = oRow("COD")
            m.Den = oRow("DEN")
            m.EstructuraMateriales = Me
            _materiales.Add(m.key, m)
            _mapamateriales.Add(m.key, m)
        Next


    End Sub

    Public Function devolverTodosLosMateriales(den As String) As List(Of IGmn)
        Authenticate()
        Dim lista As New List(Of IGmn)
        Dim ds As DataSet = DBServer.GruposMat_CargarTodasLasDenominaciones(den, User.QARestProvMaterial, User.CodPersona, User.Idioma, _
                        Me.TipoEstructuraMateriales = FiltroMaterial.FiltroMatQA Or Me.TipoEstructuraMateriales = FiltroMaterial.FiltroMatQAGS, _
                        Me.TipoEstructuraMateriales = FiltroMaterial.FiltroMatGS Or Me.TipoEstructuraMateriales = FiltroMaterial.FiltroMatQAGS)

        For Each oRow In ds.Tables(0).Rows
            Dim oMat As IGmn = Me.createGmn(DBNullToInteger(oRow("GMNQA")), DBNullToSomething(oRow("GMN1")), DBNullToSomething(oRow("GMN2")), DBNullToSomething(oRow("GMN3")), DBNullToSomething(oRow("GMN4")), oRow("DEN"))
            lista.Add(oMat)
        Next

        Return lista
    End Function
    ''' <summary>
    ''' Obiene el nodo a partir de una key
    ''' </summary>
    ''' <param name="key"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function getNodo(key As String) As IGmn
        Dim nodo As IGmn
        If Not _mapamateriales Is Nothing AndAlso _mapamateriales.ContainsKey(key) Then
            Return _mapamateriales(key)
        Else
            Dim gmn As String() = Split(key, "###")

            Select Case gmn.Length
                Case 1
                    nodo = Me.createGmn(IIf(gmn(0) = "", Nothing, gmn(0)))
                Case 2
                    nodo = Me.createGmn(IIf(gmn(0) = "", Nothing, gmn(0)), IIf(gmn(1) = "", Nothing, gmn(1)))
                Case 3
                    nodo = Me.createGmn(IIf(gmn(0) = "", Nothing, gmn(0)), IIf(gmn(1) = "", Nothing, gmn(1)), IIf(gmn(2) = "", Nothing, gmn(2)))
                Case 4
                    nodo = Me.createGmn(IIf(gmn(0) = "", Nothing, gmn(0)), IIf(gmn(1) = "", Nothing, gmn(1)), IIf(gmn(2) = "", Nothing, gmn(2)), IIf(gmn(3) = "", Nothing, gmn(3)))
                Case Else
                    nodo = Me.createGmn(IIf(gmn(0) = "", Nothing, gmn(0)), IIf(gmn(1) = "", Nothing, gmn(1)), IIf(gmn(2) = "", Nothing, gmn(2)), IIf(gmn(3) = "", Nothing, gmn(3)), IIf(gmn(0) = "", Nothing, gmn(4)))
            End Select

        End If
        Return nodo
    End Function
    Public Sub addSeleccion(key As String)
        If _mapamateriales.ContainsKey(key) Then
            Dim node As IGmn = _mapamateriales(key)
            If Not _gmnSeleccionados.Contains(node) Then
                Me.addSeleccion(node)
            End If
        Else
            Me.getNodo(key)
        End If
    End Sub
    Public Sub addSeleccion(node As IGmn)
        Dim listaborrar As New List(Of String)
        'si el nodo ya está en arbol o pertenece a un nodo que ya está en el no lo metemos
        If Not Me.incluye(node) Then
            'si el nodo contiene nodos del arbol, tendremos que borrar estos últimos
            For Each o As IGmn In Me._gmnSeleccionados
                If node.incluye(o) Then
                    listaborrar.Add(o.key)
                End If
            Next
            For Each clave As String In listaborrar
                removeSeleccion(clave)
            Next
            Me._gmnSeleccionados.Add(node)
        End If
    End Sub

    Public Sub removeSeleccion(key As String)
        Dim node As IGmn = _mapamateriales(key)
        If Me._gmnSeleccionados.Contains(node) Then
            Me.removeSeleccion(node)
        End If
    End Sub

    Public Sub removeSeleccion(node As IGmn)
        Dim listaborrar As New List(Of String)
        Dim listaAInsertar As New List(Of String)
        'si el nodo está contenido en alguno de los nodos ya seleccionado, debemos eliminar del arbol
        'a sus ascendientes padre y marcar el resto de hijos
        Dim asc As IGmn = ascendienteSeleccionado(node)
        If Not asc Is Nothing Then
            addDescendientes(asc)
            removeAscendientes(node)
        Else
            For Each o As IGmn In Me._gmnSeleccionados
                If o.contiene(node) Then
                    For Each hijo As IGmn In o.children.Values
                        listaAInsertar.Add(hijo.key)

                    Next
                    listaborrar.Add(o.key)
                End If
            Next
            For Each clave As String In listaborrar
                removeSeleccion(clave)
            Next
            For Each clave As String In listaAInsertar
                removeSeleccion(clave)
            Next
        End If
        Me._gmnSeleccionados.Remove(node)
    End Sub

    Public Sub removeAscendientes(o As IGmn)
        If Not o Is Nothing Then
            removeAscendientes(o.parent)
            removeSeleccion(o.key)
        End If
    End Sub

    Public Sub addDescendientes(o As IGmn)
        If Not o Is Nothing Then
            For Each hijo In o.children.Values
                addSeleccion(hijo.key)
            Next
        End If
    End Sub
    Public Function ascendienteSeleccionado(o As IGmn) As IGmn
        If Not o.parent Is Nothing Then
            If Me.incluye(o.parent) Then
                Return o.parent
            Else
                Return ascendienteSeleccionado(o.parent)
            End If
        Else
            Return Nothing
        End If
    End Function
    Private Function incluye(o As IGmn) As Boolean
        For Each oGmn In Me._gmnSeleccionados
            If oGmn.incluye(o) Then
                Return True
            End If
        Next
        Return False
    End Function

    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)

    End Sub
End Class
