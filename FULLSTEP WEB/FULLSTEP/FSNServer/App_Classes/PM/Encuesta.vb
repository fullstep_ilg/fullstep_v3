﻿<Serializable()>
Public Class Encuesta
    Inherits Security

    ''' <summary>Creación de una instancia del objeto</summary>
    ''' <param name="dbserver">Objeto Root de PMDatabaseServer</param> 
    ''' <param name="isAuthenticated">Usuario atenticado sí/no</param>
    ''' <remarks>Llamada desde: DetallePuntuacionEncuesta.aspx</remarks>
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub

    ''' <summary>Obtiene los datos de puntuación de las encuestas que intervienen en el cálculo de una var. de calidad de tipo encuesta.
    ''' Se devuelve un registro por cada encuesta con la puntuación de cada incógnita y la de la variable de calidad</summary>    
    ''' <param name="Prove">Proveedor</param>    
    ''' <param name="Unqa">Unidad de negocio</param>
    ''' <param name="Nivel">Periodo de la variable</param> 
    ''' <param name="VarCal">Id de la var. de calidad</param>    
    ''' <param name="sFormula">Fórmula de ponderación de la var. de calidad</param>
    ''' <returns>Dataset con los datos de puntuación de cada encuesta</returns>
    ''' <remarks>Llamada desde: DetallePuntuacionEncuesta.aspx</remarks>
    Public Function DatosPuntuacionEncuesta(ByVal Prove As String, ByVal Unqa As Long, ByVal Nivel As Integer, ByVal VarCal As Integer, ByVal sFormula As String) As DataSet
        Dim iEq As New USPExpress.USPExpression
        Dim dsEncuestas As DataSet

        Authenticate()

        Try
            Dim dsPuntVar As DataSet = DBServer.Puntuacion_DatosPuntuacionEncuesta(Prove, Unqa, Nivel, VarCal)

            If Not dsPuntVar.Tables Is Nothing AndAlso dsPuntVar.Tables.Count > 0 Then
                'Crear el datatable a devolver            
                dsEncuestas = New DataSet
                dsEncuestas.Tables.Add("Encuestas")
                Dim dtEncuestas As DataTable = dsEncuestas.Tables(0)
                With dtEncuestas
                    .Columns.Add("IdEncuesta", GetType(Integer))
                    .Columns.Add("FecAlta", GetType(Date))
                    .Columns.Add("FecCumpl", GetType(Date))
                    .Columns.Add("Peticionario", GetType(String))
                    .Columns.Add("Titulo", GetType(String))
                    If dsPuntVar.Tables(0).Rows.Count > 0 Then
                        'Columnas de las incógnitas                        
                        Dim dvInognitas As New DataView(dsPuntVar.Tables(1), "ID_ENC=" & dsPuntVar.Tables(0).Rows(0)("ID_ENC"), String.Empty, DataViewRowState.CurrentRows)
                        For Each dviIcognita As DataRowView In dvInognitas
                            .Columns.Add(dviIcognita("VARIABLEX"), GetType(Double))
                        Next
                    End If
                    .Columns.Add("Valor", GetType(Double))
                End With

                For Each oEnc As DataRow In dsPuntVar.Tables(0).Rows
                    Dim drEncuesta As DataRow = dtEncuestas.NewRow
                    drEncuesta("IdEncuesta") = oEnc("ID_ENC")
                    drEncuesta("FecAlta") = oEnc("FEC_ALTA")
                    drEncuesta("FecCumpl") = oEnc("FEC_FIN_FLUJO")
                    drEncuesta("Peticionario") = IIf(oEnc.IsNull("NOM"), oEnc("APE"), oEnc("NOM") & " " & oEnc("APE"))
                    drEncuesta("Titulo") = oEnc("TITULO")

                    Dim dvValores As New DataView(dsPuntVar.Tables(1), "ID_ENC=" & oEnc("ID_ENC"), String.Empty, DataViewRowState.CurrentRows)

                    Dim sVariables(dvValores.Count - 1) As String
                    Dim dValues(dvValores.Count - 1) As Double

                    Dim i As Integer = 0
                    For Each drValor As DataRowView In dvValores
                        drEncuesta(drValor("VARIABLEX")) = drValor("VALORX")

                        sVariables(i) = drValor("VARIABLEX")
                        dValues(i) = drValor("VALORX")
                        i += 1
                    Next

                    Try
                        iEq.Parse(sFormula, sVariables)
                        drEncuesta("Valor") = iEq.Evaluate(dValues)
                    Catch e As USPExpress.ParseException
                        drEncuesta("Valor") = DBNull.Value
                    Catch e0 As USPExpress.DivisionByZeroException
                        'La división por cero es un error controlado y supone un resultado de 0 para la fórmula                                
                        drEncuesta("Valor") = 0
                    End Try

                    dtEncuestas.Rows.Add(drEncuesta)
                Next
            End If

        Catch ex As Exception
            DBServer.Errores_Create("Encuesta.vb", DBServer.UsuarioConectado, ex.GetType().FullName, ex.Message, ex.StackTrace, "", "")
            Throw ex
        End Try

        Return dsEncuestas
    End Function
End Class
