﻿<Serializable()> _
Public Class FiltroFactura
    Inherits Security

#Region " PROPIEDADES "

    Private mIDFiltro As Integer
    Private mIDFiltroUsuario As Integer
    Private mNombreFiltro As String
    Private mPersona As String
    Private mPorDefecto As Boolean
    Private mIdioma As String
    Private mCamposConfigurados As DataSet
    Private mOrdenacion As String
    Private mSentidoOrdenacion As String
    Private mBusqueda As DataTable
    Private mCamposGenerales As DataTable
    Private mIsLoaded As Boolean
    Private msOrdenacion As String

    Property IDFiltroUsuario() As Integer
        Get
            IDFiltroUsuario = mIDFiltroUsuario
        End Get
        Set(ByVal Value As Integer)
            mIDFiltroUsuario = Value
        End Set
    End Property

    Property IDFiltro() As Integer
        Get
            Return mIDFiltro
        End Get
        Set(ByVal Value As Integer)
            mIDFiltro = Value
        End Set
    End Property

    Property NombreFiltro() As String
        Get
            NombreFiltro = mNombreFiltro
        End Get
        Set(ByVal Value As String)
            mNombreFiltro = Value
        End Set
    End Property

    Property Persona() As String
        Get
            Persona = mPersona
        End Get
        Set(ByVal Value As String)
            mPersona = Value
        End Set
    End Property

    Property Ordenacion() As String
        Get
            Return mOrdenacion
        End Get
        Set(ByVal Value As String)
            mOrdenacion = Value
        End Set
    End Property

    Property SentidoOrdenacion() As String
        Get
            Return mSentidoOrdenacion
        End Get
        Set(ByVal Value As String)
            mSentidoOrdenacion = Value
        End Set
    End Property

    Property Idioma() As String
        Get
            Return mIdioma
        End Get
        Set(ByVal Value As String)
            mIdioma = Value
        End Set
    End Property

    'Dataset con 2 tablas
    '(0) Campos Generales
    '(1) Campos Busqueda
    Property CamposConfigurados() As DataSet
        Get
            Return mCamposConfigurados
        End Get
        Set(ByVal Value As DataSet)
            mCamposConfigurados = Value
        End Set
    End Property

    Property PorDefecto() As Boolean
        Get
            Return mPorDefecto
        End Get
        Set(ByVal Value As Boolean)
            mPorDefecto = Value
        End Set
    End Property

    Property Busqueda() As DataTable
        Get
            Return mBusqueda
        End Get
        Set(ByVal value As DataTable)
            mBusqueda = value
        End Set
    End Property

    Property CamposGenerales() As DataTable
        Get
            Return mCamposGenerales
        End Get
        Set(ByVal value As DataTable)
            mCamposGenerales = value
        End Set
    End Property

    Property IsLoaded() As Boolean
        Get
            Return mIsLoaded
        End Get
        Set(ByVal value As Boolean)
            mIsLoaded = value
        End Set
    End Property
#End Region

    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub



    ''' <summary>
    ''' Obtiene la configuracion guardada de filtro con el identificador y el usuario especificados
    ''' </summary>
    ''' <remarks>Llamada desde: ConfigurarFiltrosFacturas.aspx</remarks>
    Public Sub LoadFiltro()
        Authenticate()
        Dim configuracion As DataSet = DBServer.FiltroFactura_Load(mIDFiltroUsuario, mPersona)
        If configuracion.Tables.Count > 0 Then
            'Table(0): filtro
            'Table(1): criterios de busqueda
            'Table(2): campos generales
            If configuracion.Tables(0).Rows.Count > 0 Then
                mIDFiltro = configuracion.Tables(0).Rows(0)("ID_FILTRO")
                mNombreFiltro = configuracion.Tables(0).Rows(0)("NOM_FILTRO")
                mPorDefecto = configuracion.Tables(0).Rows(0)("DEFECTO")
                mOrdenacion = DBNullToSomething(configuracion.Tables(0).Rows(0)("ORDENACION"))
                mSentidoOrdenacion = DBNullToSomething(configuracion.Tables(0).Rows(0)("SENTIDOORDENACION"))
                mIsLoaded = True
            End If
            mBusqueda = configuracion.Tables(1)
            mCamposGenerales = configuracion.Tables(2)
        End If
    End Sub

    ''' <summary>
    ''' Obtiene una instancia ejemplo que se cargará en la grid para que el usuario pueda ver como queda con datos,
    ''' y pueda mover el ancho y el orden de la columnas.
    ''' </summary>
    ''' <returns>Una instancia ejemplo</returns>
    ''' <remarks>Llamada desde: ConfigurarFiltrosFacturas.aspx; Tiempo máximo: 1 sg</remarks>
    Public Function LoadConfiguracionInstancia() As DataSet
        Authenticate()
        LoadConfiguracionInstancia = DBServer.FiltroFactura_LoadInstancia
    End Function

    ''' <summary>
    ''' Elimina la configuración del filtro.
    ''' </summary>
    ''' <remarks>Llamada desde: ConfigurarFiltrosFacturas.aspx</remarks>
    Public Sub EliminarConfiguracion(ByVal IDFiltroUsuario As Integer)
        Authenticate()
        DBServer.FiltroFactura_Eliminar(IDFiltroUsuario)
    End Sub

    ''' <summary>
    ''' Guarda la configuración del filtro definida por el usuario.
    ''' </summary>
    ''' <remarks>Llamada desde: Contratos/ConfigurarFiltrosContratos.aspx.</remarks>
    Public Sub GuardarConfiguracion()
        Authenticate()
        mIDFiltroUsuario = DBServer.FiltroFactura_SaveConfiguracion(mIDFiltro, mPersona, mNombreFiltro, mIDFiltroUsuario, mCamposConfigurados, mPorDefecto)
    End Sub
End Class
