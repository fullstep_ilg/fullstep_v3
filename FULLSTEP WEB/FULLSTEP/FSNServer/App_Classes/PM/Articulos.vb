<Serializable()> _
Public Class Articulos
    Inherits Security
    Private msGMN1 As String
    Private msGMN2 As String
    Private msGMN3 As String
    Private msGMN4 As String
    Private moArticulos As DataSet
    Private Contexto As System.Web.HttpContext
    Private oDataUltAdjVig As DataSet
    Property GMN1() As String
        Get
            GMN1 = msGMN1
        End Get
        Set(ByVal Value As String)
            msGMN1 = Value
        End Set
    End Property
    Property GMN2() As String
        Get
            GMN2 = msGMN2
        End Get
        Set(ByVal Value As String)
            msGMN2 = Value
        End Set
    End Property
    Property GMN3() As String
        Get
            GMN3 = msGMN3
        End Get
        Set(ByVal Value As String)
            msGMN3 = Value
        End Set
    End Property
    Property GMN4() As String
        Get
            GMN4 = msGMN4
        End Get
        Set(ByVal Value As String)
            msGMN4 = Value
        End Set
    End Property
    Public ReadOnly Property Data() As Data.DataSet
        Get
            Return moArticulos
        End Get
    End Property
    ''' <summary>
    ''' Datos del proceso y el proveedor de la �ltima adjudicacion vigente del art�culo
    ''' </summary>
    Public ReadOnly Property DataUltAdjVig() As Data.DataSet
        Get
            Return oDataUltAdjVig
        End Get
    End Property
    ''' <summary>
    ''' Obtiene los articulos seg�n los criterios establecidos en los par�metros que se le pasan
    ''' </summary>
    ''' <param name="sCod">Cod del art�culo</param>
    ''' <param name="sDen">Denominaci�n</param>
    ''' <param name="bCoincid">Si coincide o no exactamente</param>
    ''' <param name="sPer">Cod de persona</param>
    ''' <param name="iDesdeFila">fila desde</param>
    ''' <param name="iNumFilas">n� de registros a obtener</param>
    ''' <param name="sIdioma">cod del idioma</param>
    ''' <param name="iGenericos">tipo de art�culos (gen�ricos/ no gen�ricos o ambos)</param>
    ''' <param name="sMoneda">Moneda</param>
    ''' <param name="bCargarUltAdj">Si se cargan los datos de la �ltima adjudicaci�n</param>
    ''' <param name="sCodOrgCompras">Cod de la organizaci�n de compras</param>
    ''' <param name="sCodCentro">Cod de centro</param>
    ''' <param name="bCargarCC">Si hay que cargar el centro</param>
    ''' <param name="bCargarOrg">Si hay que cargar la organizaci�n</param>
    ''' <param name="bGasto">Tipo de art�culo de gasto</param>
    ''' <param name="bInversion">Tipo de art�culo de inversi�n</param>
    ''' <param name="bGastoInversion">Tipo de art�culo de gasto/inversi�n</param>
    ''' <param name="bNoAlmacenable">Tipo de art�culo no almacenable</param>
    ''' <param name="bAlmacenable">Tipo de art�culo almacenable</param>
    ''' <param name="bAlmacenOpcional">Tipo de art�culo almacenamiento opcional</param>
    ''' <param name="bRecepcionOblig">Tipo de art�culo recepci�n oblig</param>
    ''' <param name="bNoRecepcionable">Tipo de art�culo no recepcionable</param>
    ''' <param name="bRecepcionOpcional">Tipo de art�culo recepci�n opcional</param>
    ''' <param name="bSoloCatalogados">Solo catalogados</param>
    ''' <param name="Negociados">0- Todos 1- art negociado   2- art no negociado</param>
    ''' <param name="CodProveedor">Codigo de proveedor</param>
    ''' <param name="CargaFormPago">Si se va a cargar Forma de pago</param>
    ''' <param name="UsarOrgCompras">Si se usa org compras</param>
    ''' <param name="AccesoFSSM">Si tiene Acceso FSSM</param>  
    ''' <param name="sUon1">Partida presupuestaria uon Nivel1</param>
    ''' <param name="sUon2">Partida presupuestaria uon Nivel2</param>
    ''' <param name="sUon3">Partida presupuestaria uon Nivel3</param>
    ''' <param name="sUon4">Partida presupuestaria uon Nivel4</param> 
    ''' <param name="RestringirMaterialUsuario">Si PM ->(22005)PMRestringirMaterialAsignadoUsuario (NOTA no confundir con (22004)PMRestringirProveedoresMaterialUsuario) Si QA->0</param>
    ''' <param name="TopAutoComplete">N�mero m�ximo de art�culos a devolver</param>
    ''' <param name="sCodContiene">caracteres que contendr� el c�digo</param>
    ''' <param name="sDenContiene">caracteres que contendr� la denominaci�n</param>
    ''' <param name="CodProveedorSumiArt">C�digo del proveedor suministrador del art�culo</param>
    ''' <remarks>Llamada desde: PMWeb/campos.ascx, desglose.ascx, impexp.aspx, PMWEb2008/ BuscadorArticulos.aspx; Tiempo m�ximo: 2 sg</remarks>
    Public Sub LoadData(ByVal Usu As String, ByVal RestricUONSolUsuUON As Boolean, ByVal RestricUONSolPerfUON As Boolean,
                        Optional ByVal sCod As String = Nothing, Optional ByVal sDen As String = Nothing, Optional ByVal bCoincid As Boolean = False,
                        Optional ByVal sPer As String = Nothing, Optional ByVal iDesdeFila As Long = Nothing, Optional ByVal iNumFilas As Long = Nothing,
                        Optional ByVal sIdioma As String = Nothing, Optional ByVal iGenericos As Integer = 2, Optional ByVal sMoneda As String = "EUR",
                        Optional ByVal bCargarUltAdj As Boolean = False, Optional ByVal sCodOrgCompras As String = Nothing, Optional ByVal sCodCentro As String = Nothing,
                        Optional ByVal bCargarCC As Boolean = False,
                        Optional ByVal bCargarOrg As Boolean = False, Optional ByVal bGasto As Boolean = False, Optional ByVal bInversion As Boolean = False,
                        Optional ByVal bGastoInversion As Boolean = False, Optional ByVal bNoAlmacenable As Boolean = False, Optional ByVal bAlmacenable As Boolean = False,
                        Optional ByVal bAlmacenOpcional As Boolean = False, Optional ByVal bRecepcionOblig As Boolean = False, Optional ByVal bNoRecepcionable As Boolean = False,
                        Optional ByVal bRecepcionOpcional As Boolean = False, Optional ByVal bSoloCatalogados As Nullable(Of Boolean) = Nothing, Optional ByVal Negociados As Integer = 0,
                        Optional ByVal CodProveedor As String = Nothing, Optional ByVal CargaFormPago As Boolean = False, Optional ByVal UsarOrgCompras As Boolean = False,
                        Optional ByVal AccesoFSSM As Boolean = False, Optional ByVal sUon1 As String = Nothing, Optional ByVal sUon2 As String = Nothing,
                        Optional ByVal sUon3 As String = Nothing, Optional ByVal sUon4 As String = Nothing, Optional ByVal RestringirMaterialUsuario As Integer = 0,
                        Optional ByVal RestringirPedidosArticulosUONUsuario As Boolean = False, Optional ByVal RestringirPedidosArticulosUONPerfil As Boolean = False,
                        Optional ByVal UON1 As String = Nothing, Optional ByVal UON2 As String = Nothing, Optional ByVal UON3 As String = Nothing,
                        Optional ByVal AtribUON As DataTable = Nothing, Optional TopAutoComplete As Integer = 0, Optional ByVal sCodContiene As String = Nothing, Optional ByVal sDenContiene As String = Nothing,
                        Optional ByVal CodProveedorSumiArt As String = Nothing, Optional ByVal iFav As Integer = 0, Optional ByVal AccesoFSEP As Boolean = False)
        Authenticate()
        moArticulos = DBServer.Articulos_Get(sCod, sDen, bCoincid, sPer, iDesdeFila, iNumFilas, sIdioma, msGMN1, msGMN2, msGMN3, msGMN4, sMoneda, bCargarUltAdj,
                sCodOrgCompras, sCodCentro, iGenericos, bCargarCC, bCargarOrg, bGasto, bInversion, bGastoInversion, bNoAlmacenable,
                bAlmacenable, bAlmacenOpcional, bRecepcionOblig, bNoRecepcionable, bRecepcionOpcional, bSoloCatalogados, Negociados, CodProveedor,
                CargaFormPago, UsarOrgCompras, AccesoFSSM, sUon1, sUon2, sUon3, sUon4, RestringirMaterialUsuario,
                RestringirPedidosArticulosUONUsuario, RestringirPedidosArticulosUONPerfil,
                Usu, RestricUONSolUsuUON, RestricUONSolPerfUON, UON1, UON2, UON3, AtribUON, TopAutoComplete, sCodContiene, sDenContiene, CodProveedorSumiArt, iFav, AccesoFSEP)
    End Sub
    Public Function Articulo_Get_Properties(ByVal Cod As String) As PropiedadesArticulo
        Authenticate()
        Dim dt As DataTable = DBServer.Articulo_Get_Properties(Cod)
        If dt.Rows.Count > 0 Then
            Return New PropiedadesArticulo With {.ExisteArticulo = True, _
                                                .Codigo = Cod, _
                                                .Denominacion = dt.Rows(0)("DEN"), _
                                                .Concepto = dt.Rows(0)("CONCEPTO"), _
                                                .Almacenamiento = dt.Rows(0)("ALMACENAR"), _
                                                .Recepcion = dt.Rows(0)("RECEPCIONAR"), _
                                                .TipoRecepcion = dt.Rows(0)("TIPORECEPCION"), _
                                                .Generico = dt.Rows(0)("GENERICO")}
        Else
            Return New PropiedadesArticulo With {.ExisteArticulo = False, _
                                                .Codigo = Cod, _
                                                .Denominacion = String.Empty, _
                                                .Concepto = 0, _
                                                .Almacenamiento = 0, _
                                                .Recepcion = 0, _
                                                .TipoRecepcion = 0, _
                                                .Generico = 0}
        End If
    End Function
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
    ''' <summary>
    ''' Funcion que devuelve las �ltimas adjundicaciones de un art�culo
    ''' </summary>
    ''' <param name="sCod">codigo del articulo</param>
    ''' <param name="PrecisionFmt">Precision de Formato numerico</param>
    ''' <param name="sInstanciaMoneda">Codigo de la moneda</param>
    ''' <param name="sIdi">Idioma</param>
    ''' <param name="Proveedor">Codigo de proveedor</param>
    ''' <param name="OrgCompra">Organizacion de compra</param>
    ''' <param name="CargaFormPago">Si se va a cargar Forma de pago</param>
    ''' <param name="UsarOrgCompras">Si se usa org compras</param>
    ''' <param name="AccesoFSSM">Si tiene Acceso FSSM</param>  
    ''' <param name="sUon1">Partida presupuestaria uon Nivel1</param>
    ''' <param name="sUon2">Partida presupuestaria uon Nivel2</param>
    ''' <param name="sUon3">Partida presupuestaria uon Nivel3</param>
    ''' <param name="sUon4">Partida presupuestaria uon Nivel4</param>  
    ''' <returns>Devuelve los datos de los articulos cuyas adjudicaciones han sido las ultimas</returns>
    ''' <remarks>
    ''' Llamada desdE: PmWeb/UltimasAdjundicaciones/PAge_Load
    ''' Tiempo m�ximo: 1 seg</remarks>
    Public Function CargarUltimasAdjudicaciones(ByVal sCod As String, Optional ByVal PrecisionFmt As Byte = 2, Optional ByVal sInstanciaMoneda As String = "EUR",
            Optional ByVal sIdi As String = "SPA", Optional ByVal Proveedor As String = Nothing, Optional ByVal OrgCompra As String = Nothing,
            Optional ByVal CargaFormPago As Boolean = False, Optional ByVal UsarOrgCompras As Boolean = False, Optional ByVal AccesoFSSM As Boolean = False,
            Optional ByVal sUon1 As String = Nothing, Optional ByVal sUon2 As String = Nothing, Optional ByVal sUon3 As String = Nothing, Optional ByVal sUon4 As String = Nothing) As DataSet
        Authenticate()
        Return DBServer.Articulos_CargarUltimasAdjudicaciones(sCod, PrecisionFmt, sInstanciaMoneda, sIdi, Proveedor, OrgCompra, CargaFormPago, UsarOrgCompras, AccesoFSSM, sUon1, sUon2, sUon3, sUon4)
    End Function
    ''' <summary>
    ''' Funcion que devuelve el proceso y proveedor de la �ltima adjundicaci�n vigente de un art�culo (si la hay)
    ''' </summary>
    ''' <param name="sCod">codigo del articulo</param>
    ''' <remarks>
    ''' Llamada desdE: PmWeb/BuscadorArticulosServer.aspx.vb
    ''' Tiempo m�ximo: 1 seg</remarks>
    Public Sub CargarProveProveUltAdj(ByVal sCod As String, ByVal sCodOrgCompras As String, ByVal sCodCentro As String)
        Authenticate()
        oDataUltAdjVig = DBServer.Articulos_CargarProveProveUltAdj(sCod, sCodOrgCompras, sCodCentro)
    End Sub
    ''' <summary>
    ''' Revisado por: Sandra. Fecha: 21/03/2011.
    ''' Devuelve todos los c�digos y denominaciones de los art�culos a los que tiene acceso el usuario, para que aparezcan en el autocompletar.
    ''' Se cachea el dataset para que vaya m�s r�pido.
    ''' </summary>
    ''' <param name="sUsuario">C�digo de la persona</param>
    ''' <param name="iTipo">Tipo de solicitud</param>
    ''' <returns>Dataset con los c�digos y denominaciones.</returns>
    ''' <remarks>Llamada desde: Autocomplete.asmx.vb --> GetArticulos y GetArticulosContratos. Tiempo m�ximo: 3 sg.</remarks>
    Public Function DevolverCodigosYDenominaciones(ByVal sUsuario As String, Optional ByVal iTipo As Integer = Nothing) As DataSet
        Dim ds As DataSet
        Authenticate()
        Contexto = System.Web.HttpContext.Current
        ds = CType(Contexto.Cache("dsArticulos" & sUsuario & "_" & iTipo), DataSet)
        If ds Is Nothing Then
            ds = DBServer.ArticulosCodigosYDenominaciones_Get(sUsuario, iTipo)
            Contexto.Cache.Insert("dsArticulos" & sUsuario & "_" & iTipo, ds, Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration, New TimeSpan(0, System.Configuration.ConfigurationManager.AppSettings("TiempoExpiracionCacheMin"), 0))
        End If
        Return ds
    End Function
End Class
Public Class PropiedadesArticulo
    Private _existeArticulo As Boolean
    Public Property ExisteArticulo() As Boolean
        Get
            Return _existeArticulo
        End Get
        Set(ByVal value As Boolean)
            _existeArticulo = value
        End Set
    End Property
    Private _codigo As String
    Public Property Codigo() As String
        Get
            Return _codigo
        End Get
        Set(ByVal value As String)
            _codigo = value
        End Set
    End Property
    Private _denominacion As String
    Public Property Denominacion() As String
        Get
            Return _denominacion
        End Get
        Set(ByVal value As String)
            _denominacion = value
        End Set
    End Property
    Private _concepto As Integer
    Public Property Concepto() As Integer
        Get
            Return _concepto
        End Get
        Set(ByVal value As Integer)
            _concepto = value
        End Set
    End Property
    Private _almacenamiento As Integer
    Public Property Almacenamiento() As Integer
        Get
            Return _almacenamiento
        End Get
        Set(ByVal value As Integer)
            _almacenamiento = value
        End Set
    End Property
    Private _recepcion As Integer
    Public Property Recepcion() As Integer
        Get
            Return _recepcion
        End Get
        Set(ByVal value As Integer)
            _recepcion = value
        End Set
    End Property
    Private _tipoRecepcion As Integer
    Public Property TipoRecepcion() As Integer
        Get
            Return _tipoRecepcion
        End Get
        Set(ByVal value As Integer)
            _tipoRecepcion = value
        End Set
    End Property
    Private _generico As Integer
    Public Property Generico() As Integer
        Get
            Return _generico
        End Get
        Set(ByVal value As Integer)
            _generico = value
        End Set
    End Property
End Class