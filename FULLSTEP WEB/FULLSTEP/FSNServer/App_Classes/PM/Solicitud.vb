<Serializable()>
Public Class Solicitud
    Inherits Security
#Region "Properties"
    Private mlID As Long
    Private msCod As String
    Private moDen As MultiIdioma
    Private moDescr As MultiIdioma
    Private moValidacionesIntegracion As PM_ValidacionesIntegracion
    Private miTipo As Integer
    Private msGestor As String
    Private msNombreGestor As String
    Private mlFormulario As Long
    Private mlWorkflow As Long
    Private mlWorkflowModificacion As Long
    Private mlWorkflowBaja As Long
    Private mbPub As Boolean
    Private mdPedidoDirecto As Double
    Private mbBaja As Boolean
    Private moFormulario As Formulario
    Private mdsAdjuntos As DataTable
    Private msDenTipo As String
    Private miTipoSolicit As Integer
    Private miPlazoCumplimentacion As Integer
    Private miBloque As Integer
    Private msDenBloque As String
    Private moLongitudesDeCodigos As TiposDeDatos.LongitudesDeCodigos
    Private msMoneda As String
    Public Property BloqueActual() As Integer
        Get
            Return miBloque
        End Get
        Set(ByVal Value As Integer)
            miBloque = Value
        End Set
    End Property
    Public Property DenBloqueActual() As String
        Get
            Return msDenBloque
        End Get
        Set(ByVal Value As String)
            msDenBloque = Value
        End Set
    End Property
    Public Property dsAdjuntos() As DataTable
        Get
            dsAdjuntos = mdsAdjuntos
        End Get
        Set(ByVal Value As DataTable)
            mdsAdjuntos = Value
        End Set
    End Property
    Public Property ID() As Long
        Get
            Return mlID
        End Get

        Set(ByVal Value As Long)
            mlID = Value
        End Set
    End Property
    Public Property Codigo() As String
        Get
            Return msCod
        End Get
        Set(ByVal Value As String)
            msCod = Value
        End Set
    End Property
    Property Den(ByVal Idioma As String) As String
        Get
            Den = moDen(Idioma)

        End Get
        Set(ByVal Value As String)
            If Not moDen.Contains(Idioma) Then
                moDen.Add(Idioma, Value)
            Else
                moDen(Idioma) = Value
            End If

        End Set
    End Property
    Property Descr(ByVal Idioma As String) As String
        Get
            Descr = moDescr(Idioma)

        End Get
        Set(ByVal Value As String)
            If Not moDescr.Contains(Idioma) Then
                moDescr.Add(Idioma, Value)
            Else
                moDescr(Idioma) = Value
            End If

        End Set
    End Property
    Public Property DenTipo() As String
        Get
            DenTipo = msDenTipo
        End Get
        Set(ByVal Value As String)
            msDenTipo = Value
        End Set
    End Property
    Public Property Tipo() As Integer
        Get
            Tipo = miTipo
        End Get
        Set(ByVal Value As Integer)
            miTipo = Value
        End Set
    End Property
    Public Property Gestor() As String
        Get
            Gestor = msGestor
        End Get
        Set(ByVal Value As String)
            msGestor = Value
        End Set
    End Property
    Public Property NombreGestor() As String
        Get
            NombreGestor = msNombreGestor
        End Get
        Set(ByVal Value As String)
            msNombreGestor = Value
        End Set
    End Property
    Public Property Formulario() As Formulario
        Get
            Formulario = moFormulario
        End Get
        Set(ByVal Value As Formulario)
            moFormulario = Value
        End Set
    End Property
    Public Property Workflow() As Long
        Get
            Workflow = mlWorkflow
        End Get
        Set(ByVal Value As Long)
            mlWorkflow = Value
        End Set
    End Property
    Public Property WorkflowModificacion() As Long
        Get
            WorkflowModificacion = mlWorkflowModificacion
        End Get
        Set(ByVal Value As Long)
            mlWorkflowModificacion = Value
        End Set
    End Property
    Public Property WorkflowBaja() As Long
        Get
            WorkflowBaja = mlWorkflowBaja
        End Get
        Set(ByVal Value As Long)
            mlWorkflowBaja = Value
        End Set
    End Property
    Public Property Pub() As Boolean
        Get
            Pub = mbPub
        End Get
        Set(ByVal Value As Boolean)
            mbPub = Value
        End Set
    End Property
    Public Property PedidoDirecto() As Double
        Get
            Return mdPedidoDirecto

        End Get
        Set(ByVal Value As Double)
            mdPedidoDirecto = Value

        End Set
    End Property
    Public Property ValidacionesIntegracion() As PM_ValidacionesIntegracion
        Get
            ValidacionesIntegracion = moValidacionesIntegracion
        End Get
        Set(ByVal Value As PM_ValidacionesIntegracion)
            moValidacionesIntegracion = Value
        End Set
    End Property
    Public ReadOnly Property Baja() As Boolean
        Get
            Baja = mbBaja
        End Get
    End Property
    Public Property TipoSolicit() As Integer
        Get
            TipoSolicit = miTipoSolicit
        End Get
        Set(ByVal Value As Integer)
            miTipoSolicit = Value
        End Set
    End Property
    Public Property PlazoCumplimentacion() As Integer
        Get
            Return miPlazoCumplimentacion
        End Get
        Set(ByVal Value As Integer)
            miPlazoCumplimentacion = Value
        End Set
    End Property
    Public Property LongitudesDeCodigos() As TiposDeDatos.LongitudesDeCodigos
        Get
            Return moLongitudesDeCodigos
        End Get
        Set(ByVal Value As TiposDeDatos.LongitudesDeCodigos)
            moLongitudesDeCodigos = Value
        End Set
    End Property
    Private _es_Noconformidad_Escalacion As Boolean
    Public Property Es_Noconformidad_Escalacion() As Boolean
        Get
            Return _es_Noconformidad_Escalacion
        End Get
        Set(ByVal value As Boolean)
            _es_Noconformidad_Escalacion = value
        End Set
    End Property
    Private _nivel_Noconformidad_Escalacion As Integer
    Public Property Nivel_Noconformidad_Escalacion() As Integer
        Get
            Return _nivel_Noconformidad_Escalacion
        End Get
        Set(ByVal value As Integer)
            _nivel_Noconformidad_Escalacion = value
        End Set
    End Property
#End Region
    ''' <summary>
    ''' Procedimiento que carga los datos de una solicitud
    ''' </summary>
    ''' <param name="sIdi">Idioma de la aplicaci�n</param>
    ''' <param name="bPet">Codigo del peticionario de la solicitud</param>
    ''' <remarks>
    ''' Llamada desde: PmWeb/accionRealizadaOK/Page_load, PmWeb/guardarInstancia/Page_load, PmWeb/NwRealizarAccion/Page_load, PmWeb/solicitudesserver/Page_load, PmWeb/alta/GuardarSolicitud y Page_load, PmWeb/ayudacampo/Page_load, PmWeb/campos/Page_load, PmWeb/desglose/Page_load, PmWeb/emisionOk/Page_load, PmWeb/NWAlta/Page_load, PmWeb/pedidodirecto/Page_load, PmWeb/recalcularImportes/Page_load, PmWeb/solicitud/Page_load, PmWeb/altacertidicado/Page_load, PmWeb/detallecertificado/Page_load, PmWeb/altaNoConformidad/Page_load, PmWeb/cierre/Page_load, PmWeb/cierreRevisor/Page_load, PmWeb/cierreRevisorOK/Page_load, PmWeb/detalleNoConformidad/Page_load, PmWeb/comentarioSolic/Page_load, PmWeb/detalleSolHijas/Page_load, PmWeb/historicoEstados/Page_load, PmWeb/impexp/Page_load, PmWeb/NwcomentarioSolic/Page_load, PmWeb/NWDetalleSolic/Page_load, PmWeb/accionInvalida/Page_load, PmWeb/aprobacion/Page_load, PmWeb/devolucion/Page_load, PmWeb/flowDiagram/Page_load, PmWeb/flowDiagramExport/Page_load, PmWeb/gestionTrasladada/Page_load, PmWeb/imposibleaccion/Page_load, PmWeb/instanciaAsignada/Page_load, PmWeb/rechazoOk/Page_load, PmWeb/trasladarUsu/Page_load, PmWeb/traslados/Page_load, WebServicePM/Service/ServiceThread
    ''' Tiempo m�ximo: 1 seg</remarks>
    Public Sub Load(ByVal sIdi As String, Optional ByVal bPet As Boolean = False, Optional ByVal iTipoWorkflow As Integer = 0)
        Authenticate()

        Dim data As DataSet
        data = DBServer.Solicitud_Load(mlID, sIdi, bPet, iTipoWorkflow)
        If Not data.Tables(0).Rows.Count = 0 Then
            With data.Tables(0).Rows(0)
                msCod = .Item("COD").ToString
                moDen(sIdi) = .Item("DEN_" & sIdi).ToString
                moDescr(sIdi) = .Item("DESCR_" & sIdi).ToString
                miTipo = .Item("TIPO").ToString
                msDenTipo = DBNullToSomething(.Item("DENTIPO"))
                miTipoSolicit = .Item("TIPO_SOLICIT").ToString
                msGestor = .Item("GESTOR").ToString
                msNombreGestor = .Item("NOM_GESTOR").ToString
                mlFormulario = DBNullToSomething(.Item("FORMULARIO"))
                mlWorkflow = DBNullToSomething(.Item("WORKFLOW"))
                mlWorkflowModificacion = DBNullToSomething(.Item("WORKFLOW_MODIF"))
                mlWorkflowBaja = DBNullToSomething(.Item("WORKFLOW_BAJA"))
                mbPub = (DBNullToSomething(.Item("PUB")) = 1 OrElse DBNullToSomething(.Item("PUB")) = 3)
                mdPedidoDirecto = DBNullToSomething(.Item("PEDIDO_DIRECTO"))
                mbBaja = (.Item("BAJA") = 1)
                miPlazoCumplimentacion = .Item("PLAZO_CUMPL")

                msDenBloque = DBNullToSomething(.Item("DENBLOQUE"))
                miBloque = DBNullToSomething(.Item("BLOQUE"))

                _es_Noconformidad_Escalacion = Not IsDBNull(.Item("NOCONF_ESCALACION"))
                _nivel_Noconformidad_Escalacion = If(IsDBNull(.Item("NIVEL_ESCALACION")), 0, .Item("NIVEL_ESCALACION"))
            End With

            moFormulario = New Formulario(DBServer, mIsAuthenticated)
            moFormulario.Id = mlFormulario

            mdsAdjuntos = New DataTable
            mdsAdjuntos = data.Tables(1)

            moValidacionesIntegracion = New PM_ValidacionesIntegracion(DBServer, mIsAuthenticated)
        End If
        data = Nothing
    End Sub
    ''' <summary>
    ''' Completa el Dataset con toda la informaci�n de la pantalla de donde se graba con la informaci�n de los
    ''' campos ocultos o de solo lectura q el jsalta.js/montarformulariosubmit no tiene pq haber recogido.
    ''' </summary>
    ''' <param name="ds">Dataset con toda la informaci�n de la pantalla de donde se graba</param>
    ''' <param name="bNuevoWorkflow">True: Estamos en PM (Solicitude de compra) False: en QA (Certif. � No Conform.)</param>        
    ''' <param name="sPer">Persona conectada</param>   
    ''' <param name="bMaper">Si hay control de Mapper o no, es decir, integraci�n</param>   
    ''' <param name="b2Guardar">En Qa puedes dar de alta una No Conformidad y no cambiar de pantalla. Esto tiene 
    '''     el problema de que los ocultos ya no salen de formulario (form_campo, etc) sino de solicitud (copia_campo, etc)</param>   
    ''' <returns>Devuelve el objeto dataset generado</returns>
    ''' <remarks>Llamada desde: script/_common/guardarinstancia.aspx/GuardarSinWorkflow
    '''                 script/_common/guardarinstancia.aspx/GuardarConWorkflow
    '''                 script/_common/guardarinstancia.aspx/GenerarDataSetyHacerComprobaciones
    '''     ;Tiempo m�ximo: 0,1</remarks>
    Public Function CompletarFormulario(ByVal ds As DataSet, Optional ByVal bNuevoWorkflow As Boolean = False, Optional ByVal sPer As String = Nothing, Optional ByVal bMaper As Boolean = False, Optional ByVal b2Guardar As Boolean = False) As DataSet
        Dim dsInvisibles As DataSet
        Dim dRow As DataRow
        Dim dNewRow As DataRow
        Dim dRows() As DataRow
        Dim dRowsAdjun() As DataRow
        Dim bNoAdjQaPop As Boolean
        Dim bPadreNoVisibleHijoVa As Boolean = False

        dsInvisibles = DBServer.Solicitud_LoadInvisibles(mlID, bNuevoWorkflow, sPer)

        'METEMOS LOS CAMPOS SIMPLES QUE ESTUVIERAN OCULTOS JUNTO CON LOS VALORES
        For Each dRow In dsInvisibles.Tables(0).Rows
            dNewRow = ds.Tables("TEMP").NewRow
            dNewRow.Item("CAMPO") = dRow.Item("ID")
            dNewRow.Item("VALOR_TEXT") = dRow.Item("VALOR_TEXT")

            Try
                dNewRow.Item("VALOR_NUM") = dRow.Item("VALOR_NUM")
                dNewRow.Item("VALOR_FEC") = DevolverFechaRelativaA(DBNullToSomething(dRow.Item("VALOR_NUM")), DBNullToSomething(dRow.Item("VALOR_FEC")))
                dNewRow.Item("VALOR_BOOL") = dRow.Item("VALOR_BOOL")
            Catch ex As Exception
            End Try

            dNewRow.Item("ES_SUBCAMPO") = 0
            dNewRow.Item("SUBTIPO") = dRow.Item("SUBTIPO")
            dNewRow.Item("TIPOGS") = dRow.Item("TIPO_CAMPO_GS")
            If bMaper Then
                dNewRow.Item("GRUPO") = dRow.Item("GRUPO")
                dNewRow.Item("IDATRIB") = dRow.Item("ID_ATRIB_GS")
                dNewRow.Item("VALERP") = dRow.Item("VALIDACION_ERP")
            End If
            Try
                ds.Tables("TEMP").Rows.Add(dNewRow)
            Catch ex As Exception
            End Try
        Next

        'METEMOS LOS ADJUNTOS DE LOS CAMPOS SIMPLES OCULTOS
        If Not (ds.Tables("TEMPADJUN") Is Nothing) Then
            For Each dRow In dsInvisibles.Tables(5).Rows
                If ds.Tables("TEMPADJUN").Select("CAMPO=" + dRow.Item("CAMPO").ToString + " AND ID=" + dRow.Item("ID").ToString).Length = 0 Then
                    dNewRow = ds.Tables("TEMPADJUN").NewRow
                    dNewRow.Item("CAMPO") = dRow.Item("CAMPO")
                    dNewRow.Item("TIPO") = dRow.Item("TIPO")
                    dNewRow.Item("ID") = dRow.Item("ID")

                    Try
                        ds.Tables("TEMPADJUN").Rows.Add(dNewRow)
                    Catch ex As Exception
                    End Try
                End If
            Next
        End If

        'METEMOS LOS CAMPOS DE DESGLOSE QUE ESTUVIERAN OCULTOS
        For Each dRow In dsInvisibles.Tables(1).Rows
            If ds.Tables("TEMP").Select("CAMPO=" & dRow.Item("CAMPO_HIJO")).Length = 0 Then
                dNewRow = ds.Tables("TEMP").NewRow
                dNewRow.Item("CAMPO") = dRow.Item("CAMPO_HIJO")
                dNewRow.Item("ES_SUBCAMPO") = 1
                Try
                    ds.Tables("TEMP").Rows.Add(dNewRow)
                Catch ex As Exception
                End Try
            End If
        Next

        Dim iLinea As Integer
        For Each dRow In dsInvisibles.Tables(1).Rows
            'en este datatable vienen los visibles y los invisibles. 
            'Solo se deben a�adir los datos de los invisibles que tienen valor en el campo linea.
            If dRow.Item("VISIBLE") = 0 AndAlso Not IsDBNull(dRow.Item("LINEA")) Then
                ' Este IF comprueba que este campo invisible pertenece a un desglose con alg�n campo visible
                If UBound(dsInvisibles.Tables(1).Select("CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND VISIBLE = 1")) > 0 Then
                    'Si hay alg�n campo visible en este desglose habr� que obtener que nuevo n�mero de l�nea tiene ahora
                    dRows = ds.Tables("TEMPDESGLOSE").Select("CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND LINEA_OLD = " + dRow.Item("LINEA").ToString)
                    If dRows.Length > 0 Then 'si fuera igual a 0 indicar�a que se ha borrado esa l�nea y por tanto no hay que guardarlo
                        iLinea = dRows(0).Item("LINEA")
                        'comprobamos que no exista ya que los desgloses de popup si que se cargan aunque est� invisible
                        If ds.Tables("TEMPDESGLOSE").Select("CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND CAMPO_HIJO=" + dRow.Item("CAMPO_HIJO").ToString + " AND LINEA_OLD = " + dRow.Item("LINEA").ToString).Length = 0 Then
                            dNewRow = ds.Tables("TEMPDESGLOSE").NewRow
                            dNewRow.Item("LINEA") = iLinea
                            dNewRow.Item("CAMPO_PADRE") = dRow.Item("CAMPO_PADRE")
                            dNewRow.Item("CAMPO_HIJO") = dRow.Item("CAMPO_HIJO")
                            dNewRow.Item("VALOR_TEXT") = dRow.Item("VALOR_TEXT")
                            If (IsDBNull(dRow.Item("VALOR_TEXT"))) And (Not IsDBNull(dRow.Item("LISTA_TEXT"))) Then
                                dNewRow.Item("VALOR_TEXT") = dRow.Item("LISTA_TEXT")
                            End If
                            Try
                                dNewRow.Item("VALOR_NUM") = dRow.Item("VALOR_NUM")
                                dNewRow.Item("VALOR_FEC") = DevolverFechaRelativaA(DBNullToSomething(dRow.Item("VALOR_NUM")), DBNullToSomething(dRow.Item("VALOR_FEC")))
                                dNewRow.Item("VALOR_BOOL") = dRow.Item("VALOR_BOOL")
                            Catch ex As Exception
                            End Try
                            dNewRow.Item("LINEA_OLD") = dRow.Item("LINEA")
                            dNewRow.Item("TIPOGS") = dRow.Item("TIPO_CAMPO_GS")
                            If bMaper Then
                                dNewRow.Item("VALERP") = dRow.Item("VALIDACION_ERP")
                                dNewRow.Item("IDATRIB") = dRow.Item("ID_ATRIB_GS")
                            End If
                            Try
                                'caso Presupuesto nivel 4: Campo Oculto que controla por otro lado el maper.
                                'Intenta meter algo que ya he metido. Solo desglose popup.
                                ds.Tables("TEMPDESGLOSE").Rows.Add(dNewRow)
                            Catch ex As Exception
                            End Try

                            If Not (ds.Tables("TEMPDESGLOSEADJUN") Is Nothing) Then
                                dRowsAdjun = dsInvisibles.Tables(3).Select("CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND CAMPO_HIJO=" + dRow.Item("CAMPO_HIJO").ToString + " AND LINEA = " + iLinea.ToString)
                                If (dRowsAdjun.Length > 0) AndAlso (Not b2Guardar) Then
                                    For Each Adjun As DataRow In dRowsAdjun
                                        dNewRow = ds.Tables("TEMPDESGLOSEADJUN").NewRow

                                        dNewRow.Item("LINEA") = iLinea
                                        dNewRow.Item("CAMPO_PADRE") = dRow.Item("CAMPO_PADRE")
                                        dNewRow.Item("CAMPO_HIJO") = dRow.Item("CAMPO_HIJO")
                                        dNewRow.Item("ID") = Adjun.Item("ADJUN")
                                        dNewRow.Item("TIPO") = 3
                                        Try
                                            ds.Tables("TEMPDESGLOSEADJUN").Rows.Add(dNewRow)
                                        Catch ex As Exception
                                        End Try
                                    Next
                                End If
                            End If
                        Else
                            'Para que no se cargue CodArticulo =119 en el caso de que el NuevoCodArt este OCULTO y la DENOMINACION visible
                            Dim sql, sql2 As String
                            sql = "CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND LINEA = " + iLinea.ToString + " AND ((TIPO_CAMPO_GS= " & TiposDeDatos.TipoCampoGS.NuevoCodArticulo & " AND VISIBLE = 0)"
                            sql = sql + " OR (TIPO_CAMPO_GS =  " & TiposDeDatos.TipoCampoGS.DenArticulo & " AND VISIBLE = 1))"

                            sql2 = "CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND LINEA = " + iLinea.ToString + " AND ((TIPO_CAMPO_GS= " & TiposDeDatos.TipoCampoGS.NuevoCodArticulo & " OR TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.DenArticulo & "OR TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.CodArticulo & ") AND VISIBLE = 1)"

                            If ((DBNullToSomething(dRow.Item("TIPO_CAMPO_GS")) <> TiposDeDatos.TipoCampoGS.NuevoCodArticulo)) _
                                 Or ((dsInvisibles.Tables(1).Select(sql).Length < 2) And (DBNullToSomething(dRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.NuevoCodArticulo)) Then

                                dNewRow = ds.Tables("TEMPDESGLOSE").Select("CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND CAMPO_HIJO=" + dRow.Item("CAMPO_HIJO").ToString + " AND LINEA_OLD = " + dRow.Item("LINEA").ToString)(0)

                                'dNewRow.Item("VALOR_TEXT") tendr� el valor "nulo" cuando en el desglose se muestre solamente la denominacion el articulo no sea generico
                                'y introducimos una nueva denominacion
                                If IsDBNull(dNewRow.Item("VALOR_TEXT")) Then
                                    dNewRow.Item("VALOR_TEXT") = dRow.Item("VALOR_TEXT")
                                ElseIf dNewRow.Item("VALOR_TEXT") = "nulo" Then
                                    dNewRow.Item("VALOR_TEXT") = System.DBNull.Value
                                Else
                                    dNewRow.Item("VALOR_TEXT") = dNewRow.Item("VALOR_TEXT")
                                End If
                                If (IsDBNull(dNewRow.Item("VALOR_TEXT"))) And (Not IsDBNull(dRow.Item("LISTA_TEXT"))) Then
                                    dNewRow.Item("VALOR_TEXT") = dRow.Item("LISTA_TEXT")
                                End If
                                Try
                                    dNewRow.Item("VALOR_NUM") = IIf(IsDBNull(dNewRow.Item("VALOR_NUM")), dRow.Item("VALOR_NUM"), dNewRow.Item("VALOR_NUM"))
                                    dNewRow.Item("VALOR_FEC") = DevolverFechaRelativaA(DBNullToSomething(IIf(IsDBNull(dNewRow.Item("VALOR_NUM")), dRow.Item("VALOR_NUM"), dNewRow.Item("VALOR_NUM"))), DBNullToSomething(IIf(IsDBNull(dNewRow.Item("VALOR_FEC")), dRow.Item("VALOR_FEC"), dNewRow.Item("VALOR_FEC"))))
                                Catch ex As Exception
                                End Try
                                Try
                                    dNewRow.Item("VALOR_FEC") = IIf(IsDBNull(dNewRow.Item("VALOR_FEC")), dRow.Item("VALOR_FEC"), dNewRow.Item("VALOR_FEC"))
                                    dNewRow.Item("VALOR_BOOL") = IIf(IsDBNull(dNewRow.Item("VALOR_BOOL")), dRow.Item("VALOR_BOOL"), dNewRow.Item("VALOR_BOOL"))
                                Catch ex As Exception
                                End Try

                                dNewRow.Item("TIPOGS") = dRow.Item("TIPO_CAMPO_GS")
                                If bMaper Then
                                    dNewRow.Item("IDATRIB") = dRow.Item("ID_ATRIB_GS")
                                    dNewRow.Item("VALERP") = dRow.Item("VALIDACION_ERP")
                                End If
                            End If
                        End If

                    End If
                Else ' al no haber campos visibles de este desglose habr� que a�adirlo tal y como viene
                    bNoAdjQaPop = False
                    If Not bNuevoWorkflow Then
                        If ds.Tables("TEMPDESGLOSE").Select("CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND CAMPO_HIJO=" + dRow.Item("CAMPO_HIJO").ToString + " AND LINEA_OLD = " + dRow.Item("LINEA").ToString).Length = 0 Then
                        Else
                            bNoAdjQaPop = True
                        End If
                    End If

                    dNewRow = ds.Tables("TEMPDESGLOSE").NewRow
                    dNewRow.Item("LINEA") = dRow.Item("LINEA")
                    dNewRow.Item("CAMPO_PADRE") = dRow.Item("CAMPO_PADRE")
                    dNewRow.Item("CAMPO_HIJO") = dRow.Item("CAMPO_HIJO")
                    dNewRow.Item("VALOR_TEXT") = dRow.Item("VALOR_TEXT")
                    If (IsDBNull(dRow.Item("VALOR_TEXT"))) And (Not IsDBNull(dRow.Item("LISTA_TEXT"))) Then
                        dNewRow.Item("VALOR_TEXT") = dRow.Item("LISTA_TEXT")
                    End If

                    Try
                        dNewRow.Item("VALOR_NUM") = dRow.Item("VALOR_NUM")
                        dNewRow.Item("VALOR_FEC") = DevolverFechaRelativaA(DBNullToSomething(dRow.Item("VALOR_NUM")), DBNullToSomething(dRow.Item("VALOR_FEC")))
                        dNewRow.Item("VALOR_BOOL") = dRow.Item("VALOR_BOOL")
                    Catch ex As Exception
                    End Try

                    dNewRow.Item("LINEA_OLD") = dRow.Item("LINEA")
                    dNewRow.Item("TIPOGS") = dRow.Item("TIPO_CAMPO_GS")
                    If bMaper Then
                        dNewRow.Item("VALERP") = dRow.Item("VALIDACION_ERP")
                        dNewRow.Item("IDATRIB") = dRow.Item("ID_ATRIB_GS")
                    End If
                    Try
                        'caso Presupuesto nivel 4: Campo Oculto que controla por otro lado el maper.
                        'Intenta meter algo que ya he metido. Solo desglose popup.       
                        ds.Tables("TEMPDESGLOSE").Rows.Add(dNewRow)
                    Catch ex As Exception
                    End Try

                    If Not (ds.Tables("TEMPDESGLOSEADJUN") Is Nothing) Then
                        dRowsAdjun = dsInvisibles.Tables(3).Select("CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND CAMPO_HIJO=" + dRow.Item("CAMPO_HIJO").ToString + " AND LINEA = " + dRow.Item("LINEA").ToString)
                        If (dRowsAdjun.Length > 0) AndAlso (Not bNoAdjQaPop) Then
                            For Each Adjun As DataRow In dRowsAdjun
                                dNewRow = ds.Tables("TEMPDESGLOSEADJUN").NewRow

                                dNewRow.Item("LINEA") = dRow.Item("LINEA")
                                dNewRow.Item("CAMPO_PADRE") = dRow.Item("CAMPO_PADRE")
                                dNewRow.Item("CAMPO_HIJO") = dRow.Item("CAMPO_HIJO")

                                dNewRow.Item("ID") = Adjun.Item("ADJUN")
                                dNewRow.Item("TIPO") = 3
                                Try
                                    ds.Tables("TEMPDESGLOSEADJUN").Rows.Add(dNewRow)
                                Catch ex As Exception
                                End Try
                            Next
                        End If
                    End If
                End If
            End If
        Next

        '--------------------------------------------------------------
        Dim MaxLineas As Integer = -1
        Dim MaxLineasPadre As Long = -1

        For Each dRow In dsInvisibles.Tables(1).Rows
            'en este datatable vienen los visibles y los invisibles. 
            'Solo se deben a�adir los datos de los invisibles que tienen valor en el campo linea.
            If dRow.Item("VISIBLE") = 0 AndAlso Not IsDBNull(dRow.Item("LINEA")) Then
                ' Este IF comprueba que este campo invisible pertenece a un desglose con alg�n campo visible
                If UBound(dsInvisibles.Tables(1).Select("CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND VISIBLE = 1")) > 0 Then
                    If Not (MaxLineasPadre = dRow.Item("CAMPO_PADRE")) Then
                        MaxLineasPadre = dRow.Item("CAMPO_PADRE")

                        dRows = ds.Tables("TEMPDESGLOSE").Select("CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString)
                        MaxLineas = -1
                        For Each row As DataRow In dRows
                            If (MaxLineas < row.Item("LINEA")) Then
                                MaxLineas = row.Item("LINEA")
                            End If
                        Next
                    End If
                    If UBound(dsInvisibles.Tables(1).Select("CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND VISIBLE = 1")) > 0 Then
                        'Si hay alg�n campo visible en este desglose habr� que obtener que nuevo n�mero de l�nea tiene ahora
                        dRows = ds.Tables("TEMPDESGLOSE").Select("CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND LINEA_OLD = " + dRow.Item("LINEA").ToString)
                        If dRows.Length > 0 Then 'si fuera igual a 0 indicar�a que se ha borrado esa l�nea y por tanto no hay que guardarlo
                            iLinea = dRows(0).Item("LINEA")
                            'comprobamos que no exista ya que los desgloses de popup si que se cargan aunque est� invisible
                            If ds.Tables("TEMPDESGLOSE").Select("CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND CAMPO_HIJO=" + dRow.Item("CAMPO_HIJO").ToString + " AND LINEA_OLD = " + dRow.Item("LINEA").ToString).Length = 0 Then
                                'Y si hay mas lineas en el desglose?? El stored me ha traido q este campo es invisible. OK. Entonces para la linea 1 lo mete PERO si tengo mas lineas NO lo mete.
                                For iMasLIneas As Integer = iLinea + 1 To MaxLineas '+1 pq la 1ra ya la he metido el bucle anterior, solo para la 2da, 3ra l�nea, etc
                                    dNewRow = ds.Tables("TEMPDESGLOSE").NewRow
                                    dNewRow.Item("LINEA") = iMasLIneas
                                    dNewRow.Item("CAMPO_PADRE") = dRow.Item("CAMPO_PADRE")
                                    dNewRow.Item("CAMPO_HIJO") = dRow.Item("CAMPO_HIJO")
                                    dNewRow.Item("VALOR_TEXT") = dRow.Item("VALOR_TEXT")
                                    If (IsDBNull(dRow.Item("VALOR_TEXT"))) And (Not IsDBNull(dRow.Item("LISTA_TEXT"))) Then
                                        dNewRow.Item("VALOR_TEXT") = dRow.Item("LISTA_TEXT")
                                    End If
                                    Try
                                        dNewRow.Item("VALOR_NUM") = dRow.Item("VALOR_NUM")
                                        dNewRow.Item("VALOR_FEC") = DevolverFechaRelativaA(DBNullToSomething(dRow.Item("VALOR_NUM")), DBNullToSomething(dRow.Item("VALOR_FEC")))
                                        dNewRow.Item("VALOR_BOOL") = dRow.Item("VALOR_BOOL")
                                    Catch ex As Exception
                                    End Try
                                    dNewRow.Item("LINEA_OLD") = dRow.Item("LINEA")
                                    dNewRow.Item("TIPOGS") = dRow.Item("TIPO_CAMPO_GS")
                                    If bMaper Then
                                        dNewRow.Item("VALERP") = dRow.Item("VALIDACION_ERP")
                                        dNewRow.Item("IDATRIB") = dRow.Item("ID_ATRIB_GS")
                                    End If
                                    Try
                                        'caso Presupuesto nivel 4: Campo Oculto que controla por otro lado el maper.
                                        'Intenta meter algo que ya he metido. Solo desglose popup.
                                        ds.Tables("TEMPDESGLOSE").Rows.Add(dNewRow)
                                    Catch ex As Exception
                                    End Try
                                Next

                                If Not (ds.Tables("TEMPDESGLOSEADJUN") Is Nothing) Then
                                    dRowsAdjun = dsInvisibles.Tables(3).Select("CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND CAMPO_HIJO=" + dRow.Item("CAMPO_HIJO").ToString + " AND LINEA = " + iLinea.ToString)
                                    If (dRowsAdjun.Length > 0) AndAlso (Not b2Guardar) Then
                                        For Each Adjun As DataRow In dRowsAdjun
                                            'Y si hay mas lineas en el desglose?? El stored me ha traido q este campo es invisible. OK. Entonces para la linea 1 lo mete PERO si tengo mas lineas NO lo mete.
                                            For iMasLIneas As Integer = iLinea + 1 To MaxLineas
                                                dNewRow = ds.Tables("TEMPDESGLOSEADJUN").NewRow

                                                dNewRow.Item("LINEA") = iMasLIneas
                                                dNewRow.Item("CAMPO_PADRE") = dRow.Item("CAMPO_PADRE")
                                                dNewRow.Item("CAMPO_HIJO") = dRow.Item("CAMPO_HIJO")

                                                dNewRow.Item("ID") = Adjun.Item("ADJUN")
                                                dNewRow.Item("TIPO") = 3
                                                Try
                                                    ds.Tables("TEMPDESGLOSEADJUN").Rows.Add(dNewRow)
                                                Catch ex As Exception
                                                End Try
                                            Next
                                        Next
                                    End If
                                End If
                            End If
                        End If
                    End If
                End If
            End If
        Next
        '--------------------------------------------------------------
        For Each dRow In dsInvisibles.Tables(2).Rows
            'Si estaba entre los invisibles  el PADRE, ed, el DESGLOSE que mas da PM_CONF_CUMP_BLOQUE
            'del hijo, seguro que no esta por GenerarDataSet y habra que meterlo
            dRows = dsInvisibles.Tables(0).Select("ID=" + dRow.Item("CAMPO_PADRE").ToString)
            bPadreNoVisibleHijoVa = (dRows.Length > 0)
            ''
            If bPadreNoVisibleHijoVa Then
                dRows = ds.Tables("TEMPDESGLOSE").Select("CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND LINEA_OLD = " + dRow.Item("LINEA").ToString)
                If dRows.Length > 0 Then 'si fuera igual a 0 indicar�a que se ha borrado esa l�nea y por tanto no hay que guardarlo
                    iLinea = dRows(0).Item("LINEA")
                    'comprobamos que no exista ya que los desgloses de popup si que se cargan aunque est� invisible
                    If ds.Tables("TEMPDESGLOSE").Select("CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND CAMPO_HIJO=" + dRow.Item("CAMPO_HIJO").ToString + " AND LINEA_OLD = " + dRow.Item("LINEA").ToString).Length = 0 Then

                        dNewRow = ds.Tables("TEMPDESGLOSE").NewRow
                        dNewRow.Item("LINEA") = iLinea
                        dNewRow.Item("CAMPO_PADRE") = dRow.Item("CAMPO_PADRE")
                        dNewRow.Item("CAMPO_HIJO") = dRow.Item("CAMPO_HIJO")
                        dNewRow.Item("VALOR_TEXT") = dRow.Item("VALOR_TEXT")
                        If (IsDBNull(dRow.Item("VALOR_TEXT"))) And (Not IsDBNull(dRow.Item("LISTA_TEXT"))) Then
                            dNewRow.Item("VALOR_TEXT") = dRow.Item("LISTA_TEXT")
                        End If
                        Try
                            dNewRow.Item("VALOR_NUM") = dRow.Item("VALOR_NUM")
                            dNewRow.Item("VALOR_FEC") = DevolverFechaRelativaA(DBNullToSomething(dRow.Item("VALOR_NUM")), DBNullToSomething(dRow.Item("VALOR_FEC")))
                            dNewRow.Item("VALOR_BOOL") = dRow.Item("VALOR_BOOL")
                        Catch ex As Exception
                        End Try
                        dNewRow.Item("LINEA_OLD") = dRow.Item("LINEA")
                        dNewRow.Item("TIPOGS") = dRow.Item("TIPO_CAMPO_GS")
                        If bMaper Then
                            dNewRow.Item("VALERP") = dRow.Item("VALIDACION_ERP")
                            dNewRow.Item("IDATRIB") = dRow.Item("ID_ATRIB_GS")
                        End If
                        Try
                            'caso Presupuesto nivel 4: Campo Oculto que controla por otro lado el maper.
                            'Intenta meter algo que ya he metido. Solo desglose popup.
                            ds.Tables("TEMPDESGLOSE").Rows.Add(dNewRow)
                        Catch ex As Exception
                        End Try

                        If Not (ds.Tables("TEMPDESGLOSEADJUN") Is Nothing) Then
                            dRowsAdjun = dsInvisibles.Tables(4).Select("CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND CAMPO_HIJO=" + dRow.Item("CAMPO_HIJO").ToString + " AND LINEA = " + iLinea.ToString)
                            If (dRowsAdjun.Length > 0) AndAlso (Not b2Guardar) Then
                                For Each Adjun As DataRow In dRowsAdjun
                                    dNewRow = ds.Tables("TEMPDESGLOSEADJUN").NewRow

                                    dNewRow.Item("LINEA") = iLinea
                                    dNewRow.Item("CAMPO_PADRE") = dRow.Item("CAMPO_PADRE")
                                    dNewRow.Item("CAMPO_HIJO") = dRow.Item("CAMPO_HIJO")

                                    dNewRow.Item("ID") = Adjun.Item("ADJUN")
                                    dNewRow.Item("TIPO") = 3
                                    Try
                                        ds.Tables("TEMPDESGLOSEADJUN").Rows.Add(dNewRow)
                                    Catch ex As Exception
                                    End Try
                                Next
                            End If
                        End If
                    Else
                        'Para que no se cargue CodArticulo =119 en el caso de que el NuevoCodArt este OCULTO y la DENOMINACION visible
                        Dim sql, sql2 As String
                        sql = "CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString
                        sql = sql + " AND CAMPO_HIJO = " + dRow.Item("CAMPO_HIJO").ToString
                        sql = sql + " AND LINEA = " + iLinea.ToString
                        dRows = dsInvisibles.Tables(1).Select(sql)

                        sql = "CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND LINEA = " + iLinea.ToString + " AND ((TIPO_CAMPO_GS= " & TiposDeDatos.TipoCampoGS.NuevoCodArticulo & " AND VISIBLE = 0)"
                        sql = sql + " OR (TIPO_CAMPO_GS =  " & TiposDeDatos.TipoCampoGS.DenArticulo & " AND VISIBLE = 1))"

                        sql2 = "CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND LINEA = " + iLinea.ToString + " AND ((TIPO_CAMPO_GS= " & TiposDeDatos.TipoCampoGS.NuevoCodArticulo & " OR TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.DenArticulo & "OR TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.CodArticulo & ") AND VISIBLE = 1)"

                        If dRows.Length > 0 Then
                            If ((DBNullToSomething(dRows(0).Item("TIPO_CAMPO_GS")) <> TiposDeDatos.TipoCampoGS.NuevoCodArticulo) And (DBNullToSomething(dRows(0).Item("TIPO_CAMPO_GS")) <> TiposDeDatos.TipoCampoGS.Centro)) _
                            Or ((dsInvisibles.Tables(1).Select(sql).Length < 2) And (DBNullToSomething(dRows(0).Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.NuevoCodArticulo)) _
                            Or ((DBNullToSomething(dRows(0).Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Centro) And (dsInvisibles.Tables(1).Select(sql2).Length = 0)) Then
                                dNewRow = ds.Tables("TEMPDESGLOSE").Select("CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND CAMPO_HIJO=" + dRow.Item("CAMPO_HIJO").ToString + " AND LINEA_OLD = " + dRow.Item("LINEA").ToString)(0)

                                'dNewRow.Item("VALOR_TEXT") tendr� el valor "nulo" cuando en el desglose se muestre solamente la denominacion el articulo no sea generico
                                'y introducimos una nueva denominacion
                                If IsDBNull(dNewRow.Item("VALOR_TEXT")) Then
                                    dNewRow.Item("VALOR_TEXT") = dRow.Item("VALOR_TEXT")
                                ElseIf dNewRow.Item("VALOR_TEXT") = "nulo" Then
                                    dNewRow.Item("VALOR_TEXT") = System.DBNull.Value
                                Else
                                    dNewRow.Item("VALOR_TEXT") = dNewRow.Item("VALOR_TEXT")
                                End If
                                If (IsDBNull(dNewRow.Item("VALOR_TEXT"))) And (Not IsDBNull(dRow.Item("LISTA_TEXT"))) Then
                                    dNewRow.Item("VALOR_TEXT") = dRow.Item("LISTA_TEXT")
                                End If
                                Try
                                    dNewRow.Item("VALOR_NUM") = IIf(IsDBNull(dNewRow.Item("VALOR_NUM")), dRow.Item("VALOR_NUM"), dNewRow.Item("VALOR_NUM"))
                                    dNewRow.Item("VALOR_FEC") = DevolverFechaRelativaA(DBNullToSomething(IIf(IsDBNull(dNewRow.Item("VALOR_NUM")), dRow.Item("VALOR_NUM"), dNewRow.Item("VALOR_NUM"))), DBNullToSomething(IIf(IsDBNull(dNewRow.Item("VALOR_FEC")), dRow.Item("VALOR_FEC"), dNewRow.Item("VALOR_FEC"))))
                                Catch ex As Exception
                                End Try
                                Try
                                    dNewRow.Item("VALOR_FEC") = IIf(IsDBNull(dNewRow.Item("VALOR_FEC")), dRow.Item("VALOR_FEC"), dNewRow.Item("VALOR_FEC"))
                                    dNewRow.Item("VALOR_BOOL") = IIf(IsDBNull(dNewRow.Item("VALOR_BOOL")), dRow.Item("VALOR_BOOL"), dNewRow.Item("VALOR_BOOL"))
                                Catch ex As Exception
                                End Try

                                dNewRow.Item("TIPOGS") = dRows(0).Item("TIPO_CAMPO_GS")
                                If bMaper Then
                                    dNewRow.Item("IDATRIB") = dRows(0).Item("ID_ATRIB_GS")
                                    dNewRow.Item("VALERP") = dRows(0).Item("VALIDACION_ERP")
                                End If
                            End If
                        End If
                    End If
                Else ' al no haber campos visibles de este desglose habr� que a�adirlo tal y como viene
                    dNewRow = ds.Tables("TEMPDESGLOSE").NewRow
                    dNewRow.Item("LINEA") = dRow.Item("LINEA")
                    dNewRow.Item("CAMPO_PADRE") = dRow.Item("CAMPO_PADRE")
                    dNewRow.Item("CAMPO_HIJO") = dRow.Item("CAMPO_HIJO")
                    dNewRow.Item("VALOR_TEXT") = dRow.Item("VALOR_TEXT")
                    If (IsDBNull(dRow.Item("VALOR_TEXT"))) And (Not IsDBNull(dRow.Item("LISTA_TEXT"))) Then
                        dNewRow.Item("VALOR_TEXT") = dRow.Item("LISTA_TEXT")
                    End If

                    Try
                        dNewRow.Item("VALOR_NUM") = dRow.Item("VALOR_NUM")
                        dNewRow.Item("VALOR_FEC") = DevolverFechaRelativaA(DBNullToSomething(dRow.Item("VALOR_NUM")), DBNullToSomething(dRow.Item("VALOR_FEC")))
                        dNewRow.Item("VALOR_BOOL") = dRow.Item("VALOR_BOOL")
                    Catch ex As Exception
                    End Try

                    dNewRow.Item("LINEA_OLD") = dRow.Item("LINEA")
                    dNewRow.Item("TIPOGS") = dRow.Item("TIPO_CAMPO_GS")
                    If bMaper Then
                        dNewRow.Item("VALERP") = dRow.Item("VALIDACION_ERP")
                        dNewRow.Item("IDATRIB") = dRow.Item("ID_ATRIB_GS")
                    End If
                    Try
                        'caso Presupuesto nivel 4: Campo Oculto que controla por otro lado el maper.
                        'Intenta meter algo que ya he metido. Solo desglose popup.       
                        ds.Tables("TEMPDESGLOSE").Rows.Add(dNewRow)
                    Catch ex As Exception
                    End Try

                    If Not (ds.Tables("TEMPDESGLOSEADJUN") Is Nothing) Then
                        dRowsAdjun = dsInvisibles.Tables(4).Select("CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND CAMPO_HIJO=" + dRow.Item("CAMPO_HIJO").ToString + " AND LINEA = " + dRow.Item("LINEA").ToString)
                        If (dRowsAdjun.Length > 0) AndAlso (Not b2Guardar) Then
                            For Each Adjun As DataRow In dRowsAdjun
                                dNewRow = ds.Tables("TEMPDESGLOSEADJUN").NewRow

                                dNewRow.Item("LINEA") = dRow.Item("LINEA")
                                dNewRow.Item("CAMPO_PADRE") = dRow.Item("CAMPO_PADRE")
                                dNewRow.Item("CAMPO_HIJO") = dRow.Item("CAMPO_HIJO")

                                dNewRow.Item("ID") = Adjun.Item("ADJUN")
                                dNewRow.Item("TIPO") = 3
                                Try
                                    ds.Tables("TEMPDESGLOSEADJUN").Rows.Add(dNewRow)
                                Catch ex As Exception
                                End Try
                            Next
                        End If
                    End If
                End If
            End If
        Next

        Dim dDefaultRow As DataRow
        Dim lMaxLine As Long = 0
        Dim auxRow As DataRow
        Dim auxDataTable As New DataTable("GRUPOS")
        auxDataTable.Columns.Add("CAMPO_PADRE", System.Type.GetType("System.Int32"))
        auxDataTable.Columns.Add("LINEAS", System.Type.GetType("System.Int32"))

        dRows = ds.Tables("TEMPDESGLOSE").Select("LINEA_OLD IS NULL")

        For Each dRow In dRows
            If auxDataTable.Select("CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString).Length > 0 Then
                auxRow = auxDataTable.Select("CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString)(0)
            Else
                auxRow = auxDataTable.NewRow
                auxRow.Item("CAMPO_PADRE") = dRow.Item("CAMPO_PADRE")
                auxRow.Item("LINEAS") = 0
            End If

            If DBNullToSomething(auxRow.Item("LINEAS")) < DBNullToSomething(dRow.Item("LINEA")) Then
                auxRow.Item("LINEAS") = dRow.Item("LINEA")
            End If

            If auxRow.RowState = DataRowState.Detached Then
                auxDataTable.Rows.Add(auxRow)
            End If
        Next

        For Each auxRow In auxDataTable.Rows
            For i As Long = 1 To auxRow.Item("LINEAS")
                If ds.Tables("TEMPDESGLOSE").Select("LINEA=" + i.ToString()).Length > 0 Then
                    If dsInvisibles.Tables(1).Select("LINEA=" + i.ToString()).Length > 0 Then
                        For Each dDefaultRow In dsInvisibles.Tables(1).Select("CAMPO_PADRE = " + auxRow.Item("CAMPO_PADRE").ToString + " AND VISIBLE = 0 AND LINEA = 1")
                            If ds.Tables("TEMPDESGLOSE").Select("LINEA=" + i.ToString + " AND CAMPO_PADRE = " + dDefaultRow.Item("CAMPO_PADRE").ToString + " AND CAMPO_HIJO = " + dDefaultRow.Item("CAMPO_HIJO").ToString).Length = 0 Then
                                dNewRow = ds.Tables("TEMPDESGLOSE").NewRow
                                dNewRow.Item("LINEA") = i
                                dNewRow.Item("CAMPO_PADRE") = dDefaultRow.Item("CAMPO_PADRE").ToString
                                dNewRow.Item("CAMPO_HIJO") = dDefaultRow.Item("CAMPO_HIJO")
                                dNewRow.Item("VALOR_TEXT") = dDefaultRow.Item("VALOR_TEXT")
                                If (IsDBNull(dDefaultRow.Item("VALOR_TEXT"))) And (Not IsDBNull(dDefaultRow.Item("LISTA_TEXT"))) Then
                                    dNewRow.Item("VALOR_TEXT") = dDefaultRow.Item("LISTA_TEXT")
                                End If

                                Try
                                    dNewRow.Item("VALOR_NUM") = dDefaultRow.Item("VALOR_NUM")
                                    dNewRow.Item("VALOR_FEC") = DevolverFechaRelativaA(DBNullToSomething(dDefaultRow.Item("VALOR_NUM")), DBNullToSomething(dDefaultRow.Item("VALOR_FEC")))
                                    dNewRow.Item("VALOR_BOOL") = dDefaultRow.Item("VALOR_BOOL")
                                Catch ex As Exception
                                End Try

                                dNewRow.Item("TIPOGS") = dDefaultRow.Item("TIPO_CAMPO_GS")
                                If bMaper Then
                                    dNewRow.Item("VALERP") = dDefaultRow.Item("VALIDACION_ERP")
                                    dNewRow.Item("IDATRIB") = dDefaultRow.Item("ID_ATRIB_GS")
                                End If
                                Try
                                    'caso Presupuesto nivel 4: Campo Oculto que controla por otro lado el maper.
                                    'Intenta meter algo que ya he metido. Solo desglose popup.       
                                    ds.Tables("TEMPDESGLOSE").Rows.Add(dNewRow)
                                Catch ex As Exception
                                End Try
                            Else
                                dNewRow = ds.Tables("TEMPDESGLOSE").Select("LINEA=" + i.ToString + " AND CAMPO_PADRE = " + dDefaultRow.Item("CAMPO_PADRE").ToString + " AND CAMPO_HIJO = " + dDefaultRow.Item("CAMPO_HIJO").ToString)(0)
                                If DBNullToSomething(dDefaultRow.Item("TIPO_CAMPO_GS")) <> TiposDeDatos.TipoCampoGS.Material And DBNullToSomething(dDefaultRow.Item("TIPO_CAMPO_GS")) <> TiposDeDatos.TipoCampoGS.Unidad Then
                                    dNewRow.Item("VALOR_TEXT") = dDefaultRow.Item("VALOR_TEXT")
                                End If

                                If (IsDBNull(dDefaultRow.Item("VALOR_TEXT"))) And (Not IsDBNull(dDefaultRow.Item("LISTA_TEXT"))) Then
                                    dNewRow.Item("VALOR_TEXT") = dDefaultRow.Item("LISTA_TEXT")
                                End If

                                Try
                                    dNewRow.Item("VALOR_NUM") = dDefaultRow.Item("VALOR_NUM")
                                    dNewRow.Item("VALOR_FEC") = DevolverFechaRelativaA(DBNullToSomething(dDefaultRow.Item("VALOR_NUM")), DBNullToSomething(dDefaultRow.Item("VALOR_FEC")))
                                    dNewRow.Item("VALOR_BOOL") = dDefaultRow.Item("VALOR_BOOL")
                                Catch ex As Exception
                                End Try

                                dNewRow.Item("TIPOGS") = dDefaultRow.Item("TIPO_CAMPO_GS")
                                If bMaper Then
                                    dNewRow.Item("VALERP") = dDefaultRow.Item("VALIDACION_ERP")
                                    dNewRow.Item("IDATRIB") = dDefaultRow.Item("ID_ATRIB_GS")
                                End If
                            End If

                        Next
                    Else
                        For Each dDefaultRow In dsInvisibles.Tables(1).Select("CAMPO_PADRE = " + auxRow.Item("CAMPO_PADRE").ToString + " AND VISIBLE = 0")
                            If ds.Tables("TEMPDESGLOSE").Select("LINEA=" + i.ToString + " AND CAMPO_PADRE = " + dDefaultRow.Item("CAMPO_PADRE").ToString + " AND CAMPO_HIJO = " + dDefaultRow.Item("CAMPO_HIJO").ToString).Length = 0 Then
                                dNewRow = ds.Tables("TEMPDESGLOSE").NewRow
                                dNewRow.Item("LINEA") = i
                                dNewRow.Item("CAMPO_PADRE") = dDefaultRow.Item("CAMPO_PADRE").ToString
                                dNewRow.Item("CAMPO_HIJO") = dDefaultRow.Item("CAMPO_HIJO")
                                dNewRow.Item("VALOR_TEXT") = dDefaultRow.Item("VALOR_TEXT")
                                If (IsDBNull(dDefaultRow.Item("VALOR_TEXT"))) And (Not IsDBNull(dDefaultRow.Item("LISTA_TEXT"))) Then
                                    dNewRow.Item("VALOR_TEXT") = dDefaultRow.Item("LISTA_TEXT")
                                End If

                                Try
                                    dNewRow.Item("VALOR_NUM") = dDefaultRow.Item("VALOR_NUM")
                                    dNewRow.Item("VALOR_FEC") = DevolverFechaRelativaA(DBNullToSomething(dDefaultRow.Item("VALOR_NUM")), DBNullToSomething(dDefaultRow.Item("VALOR_FEC")))
                                    dNewRow.Item("VALOR_BOOL") = dDefaultRow.Item("VALOR_BOOL")
                                Catch ex As Exception
                                End Try

                                dNewRow.Item("TIPOGS") = dDefaultRow.Item("TIPO_CAMPO_GS")
                                If bMaper Then
                                    dNewRow.Item("VALERP") = dDefaultRow.Item("VALIDACION_ERP")
                                    dNewRow.Item("IDATRIB") = dDefaultRow.Item("ID_ATRIB_GS")
                                End If
                                Try
                                    ds.Tables("TEMPDESGLOSE").Rows.Add(dNewRow)
                                Catch ex As Exception
                                End Try
                            End If
                        Next
                    End If
                End If
            Next
        Next

        Return ds
    End Function
    ''' <summary>
    ''' Funcion que devuelve las precondiciones que tiene una accione
    ''' </summary>
    ''' <param name="lIdAccion">Identificador de la accion</param>
    ''' <param name="sIdi">Idioma del usuario</param>
    ''' <returns>Un DataSet con los datos de las precondiciones de la accion buscada</returns>
    ''' <remarks>
    ''' Llamada desde: PmWeb/GuardarInstancia/GenerarDataSetYHacerComprobaciones y GuardarConWorkFlow
    ''' Tiempo m�ximo: 0,2 seg</remarks>
    Public Function ObtenerPrecondicionesAccion(ByVal lIdAccion As Integer, ByVal sIdi As String) As DataSet
        Authenticate()
        Dim data As DataSet
        data = DBServer.Solicitud_LoadPrecondicionesAccion(lIdAccion, sIdi)
        Return data
    End Function
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
        moDen = New MultiIdioma
        moDescr = New MultiIdioma
    End Sub
    ''' <summary>
    ''' Funcion que devuelve la combo utilizada en el mapper(los campos)
    ''' </summary>
    ''' <param name="bSave">Si se debe coger el dato de Copia_campo o no</param>
    ''' <param name="Campo">Identificador del campo</param>
    ''' <param name="Idioma">Idioma en que se debe devolver</param>
    ''' <returns>Los datos de los campos que se cargan en el Maper</returns>
    ''' <remarks>
    ''' Llamada desde: PmServer/Instancia.vb/CargaComboMaper
    ''' Tiempo m�ximo: 0,5 seg</remarks>
    Public Function CargaComboMaper(ByVal bSave As Boolean, ByVal Campo As Integer, ByVal Idioma As String _
    , Optional ByVal UserCode As String = Nothing, Optional ByVal UserPassword As String = Nothing) As DataSet
        Try

            Return DBServer.DatosMapperBD(bSave, Campo, Idioma)

        Catch ex As Exception

        End Try

        Return Nothing
    End Function
    ''' <summary>
    ''' Procedimiento que envia una solicitud a las solicitudes favoritas del usuario
    ''' </summary>
    ''' <param name="lSolicitud">Identificador de la solicitud</param>
    ''' <param name="sDenFavorita">Denominacion de la solicitud favorita</param>
    ''' <param name="sUsu">Codigo de usuario</param>
    ''' <param name="oDs">DataSet con los datos de la solicitud favorita a guardar</param>
    ''' <remarks>
    ''' Llamada desde: PmWeb/guardarSOLFavorita/Page_Load
    ''' Tiempo m�ximo: 0,6 seg</remarks>
    Public Sub EnviarFavoritos(ByVal lSolicitud As Long, ByVal sDenFavorita As String, ByVal sUsu As String, ByVal oDs As DataSet)
        Authenticate()
        DBServer.Solicitud_EnviarFavoritos(lSolicitud, sDenFavorita, sUsu, oDs)
    End Sub
    ''' <summary>
    ''' Funcion que carga un campo de una solicitud
    ''' </summary>
    ''' <param name="lCampo">Identificador del campo</param>
    ''' <returns>Un DataSet con los datos del campo a cargar</returns>
    ''' <remarks>
    ''' Llamada desde: PmWeb/campos/ComprobarCondiciones, PmWeb/desglose/ComprobarCondiciones 
    ''' Tiempo m�ximo: 0,35 seg</remarks>
    Public Function cargarCampo(ByVal lCampo As Long) As DataSet
        Authenticate()
        Return DBServer.cargarCampo(lCampo, False)
    End Function
    ''' <summary>
    ''' Funcion que carga los datos del campo de un desglose
    ''' </summary>
    ''' <param name="lCampo">Identificador del campo</param>
    ''' <param name="nLinea">Identificador de la linea</param>
    ''' <returns>Un DataSet con los datos del campo dentro del desglose</returns>
    ''' <remarks>
    ''' Llamada desdE: PmWeb/desglose/ComprobarCondiciones
    ''' Tiempo m�ximo: 0,25 seg</remarks>
    Public Function cargarCampoDesglose(ByVal lCampo As Long, ByVal nLinea As Long) As Object
        Authenticate()

        Dim ds As DataSet
        ds = DBServer.cargarCampoDesglose(lCampo, nLinea, False)
        If ds.Tables(0).Rows.Count > 0 Then
            If DBNullToSomething(ds.Tables(0).Rows(0).Item("VALOR_TEXT")) <> Nothing Then
                Return DBNullToSomething(ds.Tables(0).Rows(0).Item("VALOR_TEXT"))
            ElseIf DBNullToSomething(ds.Tables(0).Rows(0).Item("VALOR_NUM")) <> Nothing Then
                Return DBNullToSomething(ds.Tables(0).Rows(0).Item("VALOR_NUM"))
            ElseIf DBNullToSomething(ds.Tables(0).Rows(0).Item("VALOR_BOOL")) <> Nothing Then
                Return DBNullToSomething(ds.Tables(0).Rows(0).Item("VALOR_BOOL"))
            ElseIf DBNullToSomething(ds.Tables(0).Rows(0).Item("VALOR_FEC")) <> Nothing Then
                Return DBNullToSomething(ds.Tables(0).Rows(0).Item("VALOR_FEC"))
            End If
        End If
        Return Nothing
    End Function
    ''' <summary>
    ''' Completa el Dataset con toda la informaci�n de la pantalla de donde se graba con la informaci�n de los
    ''' campos ocultos o de solo lectura q el jsalta.js/montarformulariosubmitFavoritos no tiene pq haber recogido.
    ''' el problema de que los ocultos ya no salen de formulario (form_campo, etc) sino de solicitud (copia_campo, etc)
    ''' </summary>
    ''' <param name="lSolicitud">ID de la solicitud</param>
    ''' <param name="bNuevoWorkflow">True: Estamos en PM (Solicitude de compra) False: en QA (Certif. � No Conform.)</param>        
    ''' <param name="sPer">Persona conectada</param>   
    ''' <param name="idInstancia">id de la instancia si se llama a esta funcion desde el detalle de una solicitud</param>        
    ''' <returns>Devuelve el objeto dataset generado</returns>
    ''' <remarks>Llamada desde:GuardarSOLFavorita.aspx.vb
    '''     ;Tiempo m�ximo: 0,1</remarks>
    Public Function CargarInvisibles(ByVal lSolicitud As Long, Optional ByVal bNuevoWorkflow As Boolean = False, Optional ByVal sPer As String = Nothing, Optional ByVal idInstancia As Long = 0) As DataSet
        Dim dsInvisibles As DataSet
        dsInvisibles = DBServer.Solicitud_LoadInvisibles(lSolicitud, bNuevoWorkflow, sPer, idInstancia)

        Return dsInvisibles
    End Function
    ''' <summary>
    ''' Indica si algun desglose del formulario de la solicitud tiene vinculaciones a otro/s desglose/s
    ''' </summary>
    ''' <returns>si algun desglose del formulario de la solicitud tiene vinculaciones o no </returns>
    ''' <remarks>Llamada desde: NWAlta.aspx/form_load; Tiempo maximo: 0,1</remarks>
    Public Function ExisteVinculaciones() As Boolean
        Authenticate()
        Return DBServer.Solicitud_ExisteVinculaciones(mlID)
    End Function
    ''' <summary>
    ''' En el caso de que la l�nea seleccionada, se hubiese vinculado ya a alguna l�nea de 
    ''' otra solicitud, en el caso de la SGNTJ se realizar� la siguiente validaci�n a medida:
    ''' Se comprobar� que la suma de las cantidades (campo de sistema cantidad) consumidas en cada una de las l�neas, no supere o 
    ''' haya alcanzado la cantidad de la l�nea origen
    ''' </summary>
    ''' <param name="sDatosOrigen">Instancia origen @ Desglose Origen (form_campo.id) @ linea origen</param> 
    ''' <param name="lCantidad">Cantidad de la l�nea</param>  
    ''' <returns>Si se ha superado o alcanzado la cantidad de la l�nea origen para la solicitud q se intenta grabar</returns>
    ''' <remarks>Llamada desde: controlarLineaVinculada.aspx/Page_Load; Tiempo m�ximo: 0,1 seg</remarks>
    Public Function ComprobarEmitirCantidadesVincular(ByVal sDatosOrigen As String, ByVal lCantidad As Double) As String
        Authenticate()

        Dim spli() As String = sDatosOrigen.Split("@")
        If UBound(spli) = 0 Then
            Return "OK"
        Else
            Dim ds As DataSet = DBServer.ComprobarEmitirCantidadesVincular(spli(0), spli(1), spli(2), 0, lCantidad)

            If DBNullToDbl(ds.Tables(0).Rows(0)("CANTLINEA")) >= 0 Then
                Return "OK"
            Else
                Return "KO"
            End If
        End If
    End Function
    Public Function ObtenerCamposImporteTotal(ByVal lIdsDesglose As String) As DataTable
        Authenticate()
        Return DBServer.ObtenerCamposImporteTotald(lIdsDesglose, True)
    End Function
End Class