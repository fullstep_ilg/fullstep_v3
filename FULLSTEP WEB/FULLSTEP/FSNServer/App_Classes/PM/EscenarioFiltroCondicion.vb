﻿Public Class EscenarioFiltroCondicion
    Private _idFiltro As Integer
    Public Property IdFiltro() As Integer
        Get
            Return _idFiltro
        End Get
        Set(ByVal value As Integer)
            _idFiltro = value
        End Set
    End Property
    Private _orden As Integer
    Public Property Orden() As Integer
        Get
            Return _orden
        End Get
        Set(ByVal value As Integer)
            _orden = value
        End Set
    End Property
    Private _idCampo As Integer
    Public Property IdCampo() As Integer
        Get
            Return _idCampo
        End Get
        Set(ByVal value As Integer)
            _idCampo = value
        End Set
    End Property
    Private _esCampoGeneral As Boolean
    Public Property EsCampoGeneral() As Boolean
        Get
            Return _esCampoGeneral
        End Get
        Set(ByVal value As Boolean)
            _esCampoGeneral = value
        End Set
    End Property
    Private _esLista As Boolean
    Public Property EsLista() As Boolean
        Get
            Return _esLista
        End Get
        Set(ByVal value As Boolean)
            _esLista = value
        End Set
    End Property
    Private _tipoCampo As TiposDeDatos.TipoGeneral
    Public Property TipoCampo() As TiposDeDatos.TipoGeneral
        Get
            Return _tipoCampo
        End Get
        Set(ByVal value As TiposDeDatos.TipoGeneral)
            _tipoCampo = value
        End Set
    End Property
    Private _tipoCampoGS As Integer
    Public Property TipoCampoGS() As Integer
        Get
            Return _tipoCampoGS
        End Get
        Set(ByVal value As Integer)
            _tipoCampoGS = value
        End Set
    End Property
    Private _denominacion As String
    Public Property Denominacion() As String
        Get
            Return _denominacion
        End Get
        Set(ByVal value As String)
            _denominacion = value
        End Set
    End Property
    Private _condicion As String
    Public Property Condicion() As String
        Get
            Return _condicion
        End Get
        Set(ByVal value As String)
            _condicion = value
        End Set
    End Property
    Private _denominacion_BD As String
    Public Property Denominacion_BD() As String
        Get
            Return _denominacion_BD
        End Get
        Set(ByVal value As String)
            _denominacion_BD = value
        End Set
    End Property
    Private _operador As Integer
    Public Property Operador() As Integer
        Get
            Return _operador
        End Get
        Set(ByVal value As Integer)
            _operador = value
        End Set
    End Property
    Private _valores As List(Of String)
    Public Property Valores() As List(Of String)
        Get
            Return _valores
        End Get
        Set(ByVal value As List(Of String))
            _valores = value
        End Set
    End Property
    Private _tieneError As Boolean
    Public Property TieneError() As Boolean
        Get
            Return _tieneError
        End Get
        Set(ByVal value As Boolean)
            _tieneError = value
        End Set
    End Property
    Public Sub New()
        Valores = New List(Of String)
    End Sub
End Class
