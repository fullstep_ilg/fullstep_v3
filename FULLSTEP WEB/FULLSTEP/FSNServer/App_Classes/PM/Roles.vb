<Serializable()> _
Public Class Roles
    Inherits Security

    Private moRoles As Collection
    Public ReadOnly Property Roles() As Collection
        Get
            Return moRoles
        End Get
    End Property
    Friend Sub Add(ByVal lId As Integer, ByVal sDen As String, ByVal iTipo As Integer, Optional ByVal sPer As String = Nothing, Optional ByVal lBloque As Integer = Nothing)
        Dim oRol As New Rol(DBServer, mIsAuthenticated)
        If moRoles Is Nothing Then
            moRoles = New Collection
        End If
        oRol.Id = lId
        oRol.Den = sDen
        oRol.Tipo = iTipo
        oRol.Persona = sPer
        oRol.Bloque = lBloque

        moRoles.Add(oRol, lId.ToString)
    End Sub
    Friend Sub Add(ByRef oRol As Rol)
        If moRoles Is Nothing Then
            moRoles = New Collection
        End If
        moRoles.Add(oRol, oRol.Id.ToString)
    End Sub
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
End Class