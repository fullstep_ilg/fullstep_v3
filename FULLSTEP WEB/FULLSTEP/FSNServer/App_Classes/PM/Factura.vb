﻿<Serializable()> _
Public Class Factura
    Inherits Security

    Private m_lID As Long
    Private m_lInstancia As Long
    Private m_sNumeroFactura As String
    Private m_sNumeroERP As String
    Private m_sCodProve As String
    Private m_sDenProve As String
    Private m_sCodPaisProve As String
    Private m_lIdEmpresa As Long
    Private m_sEmpresa As String
    Private m_dFechaFactura As Date
    Private m_dFechaContabilizacion As Date
    Private m_dImporte As Double
    Private m_dImporteBruto As Double
    Private m_dTotalRetencionGarantia As Double
    Private m_dTotalCostes As Double
    Private m_dTotalDescuentos As Double
    Private m_iEstado As Integer
    Private m_sEstadoDen As String
    Private m_iTipo As Integer
    Private m_sObservaciones As String
    Private m_sCodViaPago As String
    Private m_sViaPago As String
    Private m_sCodFormaPago As String
    Private m_sFormaPago As String
    Private m_dtCostes As DataTable
    Private m_dtDescuentos As DataTable
    Private m_dtLineas As DataTable
    Private m_dtRetencionGarantia As DataTable
    Private m_sMoneda As String
    Private m_dtImpuestosRepercutidos As DataTable
    Private m_dtImpuestosRetenidos As DataTable
    Private m_dtImpuestosFactura As DataTable
    Private m_dtImpuestosCabecera As DataTable
    Private m_dsDactura As DataSet
    'Detalle del albarán
    Private m_dFechaAlbaran As Date
    Private m_sNumRecepcionErp As String
    Private m_sCodReceptor As String
    Private m_sNombreReceptor As String
    Private m_sCargo As String
    Private m_sDepartamento As String
    Private m_sEmail As String
    Private m_sTelefono As String
    Private m_sFax As String
    Private m_sOrganizacion As String
    Private m_dtCostesAlbaran As DataTable
    Private m_dtDescuentosAlbaran As DataTable
    Private m_lIdEFactura As Long
    Private m_lIdCosteGenerico As Long
    Private m_lIdDescuentoGenerico As Long
    Private m_dFechaInicioOriginal As Date
    Private m_dFechaFinOriginal As Date
    Private m_sNumeroFacturaOriginal As String
    Private m_dToleranciaImporte As Double
    Private m_dToleranciaPorcentaje As Double
    Private m_bEsGestor As Boolean
    Private m_bEsReceptor As Boolean
    Private m_iConcepto As Integer
    Private m_bHayOtrosGestores As Boolean
#Region "Propiedades"
    Public Property ID() As Long
        Get
            Return m_lID
        End Get

        Set(ByVal Value As Long)
            m_lID = Value
        End Set
    End Property
    Public Property Instancia() As Long
        Get
            Return m_lInstancia
        End Get

        Set(ByVal Value As Long)
            m_lInstancia = Value
        End Set
    End Property
    Public Property NumeroFactura() As String
        Get
            Return m_sNumeroFactura
        End Get

        Set(ByVal Value As String)
            m_sNumeroFactura = Value
        End Set
    End Property
    Public Property NumeroERP() As String
        Get
            Return m_sNumeroERP
        End Get

        Set(ByVal Value As String)
            m_sNumeroERP = Value
        End Set
    End Property
    Public Property CodProve() As String
        Get
            Return m_sCodProve
        End Get

        Set(ByVal Value As String)
            m_sCodProve = Value
        End Set
    End Property
    Public Property DenProve() As String
        Get
            Return m_sDenProve
        End Get

        Set(ByVal Value As String)
            m_sDenProve = Value
        End Set
    End Property
    Public Property CodPaisProve() As String
        Get
            Return m_sCodPaisProve
        End Get

        Set(ByVal Value As String)
            m_sCodPaisProve = Value
        End Set
    End Property
    Public Property IdEmpresa() As Long
        Get
            Return m_lIdEmpresa
        End Get

        Set(ByVal Value As Long)
            m_lIdEmpresa = Value
        End Set
    End Property
    Public Property Empresa() As String
        Get
            Return m_sEmpresa
        End Get

        Set(ByVal Value As String)
            m_sEmpresa = Value
        End Set
    End Property
    Public Property FechaFactura() As Date
        Get
            Return m_dFechaFactura
        End Get

        Set(ByVal Value As Date)
            m_dFechaFactura = Value
        End Set
    End Property
    Public Property FechaContabilizacion() As Date
        Get
            Return m_dFechaContabilizacion
        End Get

        Set(ByVal Value As Date)
            m_dFechaContabilizacion = Value
        End Set
    End Property
    Public Property Importe() As Double
        Get
            Return m_dImporte
        End Get

        Set(ByVal Value As Double)
            m_dImporte = Value
        End Set
    End Property
    Public Property ImporteBruto() As Double
        Get
            Return m_dImporteBruto
        End Get

        Set(ByVal Value As Double)
            m_dImporteBruto = Value
        End Set
    End Property
    Public Property TotalRetencionGarantia() As Double
        Get
            Return m_dTotalRetencionGarantia
        End Get

        Set(ByVal Value As Double)
            m_dTotalRetencionGarantia = Value
        End Set
    End Property
    Public Property TotalCostes() As Double
        Get
            Return m_dTotalCostes
        End Get

        Set(ByVal Value As Double)
            m_dTotalCostes = Value
        End Set
    End Property
    Public Property TotalDescuentos() As Double
        Get
            Return m_dTotalDescuentos
        End Get

        Set(ByVal Value As Double)
            m_dTotalDescuentos = Value
        End Set
    End Property
    Public Property Estado() As Integer
        Get
            Return m_iEstado
        End Get

        Set(ByVal Value As Integer)
            m_iEstado = Value
        End Set
    End Property
    Public Property EstadoDen() As String
        Get
            Return m_sEstadoDen
        End Get

        Set(ByVal Value As String)
            m_sEstadoDen = Value
        End Set
    End Property
    Public Property Tipo() As Integer
        Get
            Return m_iTipo
        End Get

        Set(ByVal Value As Integer)
            m_iTipo = Value
        End Set
    End Property
    Public Property Observaciones() As String
        Get
            Return m_sObservaciones
        End Get

        Set(ByVal Value As String)
            m_sObservaciones = Value
        End Set
    End Property
    Public Property CodViaPago() As String
        Get
            Return m_sCodViaPago
        End Get

        Set(ByVal Value As String)
            m_sCodViaPago = Value
        End Set
    End Property
    Public Property ViaPago() As String
        Get
            Return m_sViaPago
        End Get

        Set(ByVal Value As String)
            m_sViaPago = Value
        End Set
    End Property
    Public Property CodFormaPago() As String
        Get
            Return m_sCodFormaPago
        End Get

        Set(ByVal Value As String)
            m_sCodFormaPago = Value
        End Set
    End Property
    Public Property FormaPago() As String
        Get
            Return m_sFormaPago
        End Get

        Set(ByVal Value As String)
            m_sFormaPago = Value
        End Set
    End Property
    Public ReadOnly Property Costes() As DataTable
        Get
            Return m_dtCostes
        End Get
    End Property
    Public ReadOnly Property Descuentos() As DataTable
        Get
            Return m_dtDescuentos
        End Get
    End Property
    Public ReadOnly Property Lineas() As DataTable
        Get
            Return m_dtLineas
        End Get
    End Property
    Public ReadOnly Property RetencionGarantia() As DataTable
        Get
            Return m_dtRetencionGarantia
        End Get
    End Property
    Public ReadOnly Property Moneda() As String
        Get
            Return m_sMoneda
        End Get
    End Property
    Public ReadOnly Property ImpuestosRepercutidos() As DataTable
        Get
            Return m_dtImpuestosRepercutidos
        End Get
    End Property
    Public ReadOnly Property ImpuestosRetenidos() As DataTable
        Get
            Return m_dtImpuestosRetenidos
        End Get
    End Property
    Public ReadOnly Property ImpuestosCabecera() As DataTable
        Get
            Return m_dtImpuestosCabecera
        End Get
    End Property
    Public ReadOnly Property ImpuestosFactura() As DataTable
        Get
            Return m_dtImpuestosFactura
        End Get
    End Property
    Public ReadOnly Property IdEFactura() As Long
        Get
            Return m_lIdEFactura
        End Get
    End Property
    Public ReadOnly Property DatosFactura() As DataSet
        Get
            Return m_dsDactura
        End Get
    End Property
    Public ReadOnly Property IdCosteGenerico() As Long
        Get
            Return m_lIdCosteGenerico
        End Get
    End Property
    Public ReadOnly Property IdDescuentoGenerico() As Long
        Get
            Return m_lIdDescuentoGenerico
        End Get
    End Property
    Public ReadOnly Property FechaInicioOriginal() As Date
        Get
            Return m_dFechaInicioOriginal
        End Get
    End Property
    Public ReadOnly Property FechaFinOriginal() As Date
        Get
            Return m_dFechaFinOriginal
        End Get
    End Property
    Public ReadOnly Property NumeroFacturaOriginal() As String
        Get
            Return m_sNumeroFacturaOriginal
        End Get
    End Property
    Public Property ToleranciaImporte() As Double
        Get
            Return m_dToleranciaImporte
        End Get

        Set(ByVal Value As Double)
            m_dToleranciaImporte = Value
        End Set
    End Property
    Public Property ToleranciaPorcentaje() As Double
        Get
            Return m_dToleranciaPorcentaje
        End Get

        Set(ByVal Value As Double)
            m_dToleranciaPorcentaje = Value
        End Set
    End Property
    Public Property EsGestor() As Boolean
        Get
            Return m_bEsGestor
        End Get

        Set(ByVal Value As Boolean)
            m_bEsGestor = Value
        End Set
    End Property
    Public Property EsReceptor() As Boolean
        Get
            Return m_bEsReceptor
        End Get

        Set(ByVal Value As Boolean)
            m_bEsReceptor = Value
        End Set
    End Property
    ''' <summary>
    ''' Concepto de la Factura:
    ''' 0: Gasto
    ''' 1: Inversión
    ''' Indican qué tipo de líneas de pedido agrupan. 
    ''' Líneas de pedido de tipo "Inversion": Factura de Inversión.
    ''' Líneas de pedido de tipo "Gasto" o "Ambos": Factura de Gasto.
    ''' El prototipo de la tarea 2572 aclara como saber si una línea de pedido es de Gasto o Inversión.
    ''' </summary>
    Public Property Concepto() As FSNLibrary.TiposDeDatos.ConceptoFactura
        Get
            Return m_iConcepto
        End Get

        Set(ByVal Value As FSNLibrary.TiposDeDatos.ConceptoFactura)
            m_iConcepto = Value
        End Set
    End Property
    Public ReadOnly Property HayOtrosGestores() As Boolean
        Get
            Return m_bHayOtrosGestores
        End Get
    End Property
#Region "Detalle del albarán"
    Public ReadOnly Property FechaAlbaran() As Date
        Get
            Return m_dFechaAlbaran
        End Get
    End Property
    Public ReadOnly Property NumRecepcionErp() As String
        Get
            Return m_sNumRecepcionErp
        End Get
    End Property
    Public ReadOnly Property CodReceptor() As String
        Get
            Return m_sCodReceptor
        End Get
    End Property
    Public ReadOnly Property NombreReceptor() As String
        Get
            Return m_sNombreReceptor
        End Get
    End Property
    Public ReadOnly Property Cargo() As String
        Get
            Return m_sCargo
        End Get
    End Property
    Public ReadOnly Property Departamento() As String
        Get
            Return m_sDepartamento
        End Get
    End Property
    Public ReadOnly Property Email() As String
        Get
            Return m_sEmail
        End Get
    End Property
    Public ReadOnly Property Telefono() As String
        Get
            Return m_sTelefono
        End Get
    End Property
    Public ReadOnly Property Fax() As String
        Get
            Return m_sFax
        End Get
    End Property
    Public ReadOnly Property Organizacion() As String
        Get
            Return m_sOrganizacion
        End Get
    End Property
    Public ReadOnly Property CostesAlbaran() As DataTable
        Get
            Return m_dtCostesAlbaran
        End Get
    End Property
    Public ReadOnly Property DescuentosAlbaran() As DataTable
        Get
            Return m_dtDescuentosAlbaran
        End Get
    End Property
#End Region
#End Region
    Private Enum TipoCD
        Coste = 0
        Descuento = 1
    End Enum
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
    ''' <summary>
    ''' Cargar los datos generales de la factura
    ''' </summary>
    ''' <param name="sIdi">Idioma</param>        
    ''' <remarks>Llamada desde: DetalleFactura.aspx; Tiempo máximo: 0,2</remarks>
    Public Sub Load(Optional ByVal sIdi As String = Nothing, Optional ByVal sPer As String = Nothing)
        Authenticate()
        Dim data As DataSet

        data = DBServer.Factura_Load(m_lID, sIdi, If(m_bEsGestor, sPer, Nothing), If(m_bEsReceptor, sPer, Nothing))

        If Not data.Tables.Count = 0 Then
            If Not data.Tables(0).Rows.Count = 0 Then
                m_lInstancia = DBNullToInteger(data.Tables(0).Rows(0).Item("INSTANCIA"))
                m_sNumeroFactura = DBNullToSomething(data.Tables(0).Rows(0).Item("NUM"))
                m_sNumeroERP = DBNullToSomething(data.Tables(0).Rows(0).Item("NUM_ERP"))
                m_lIdEmpresa = DBNullToSomething(data.Tables(0).Rows(0).Item("ID_EMPRESA"))
                m_sEmpresa = DBNullToSomething(data.Tables(0).Rows(0).Item("DEN_EMPRESA"))
                m_sCodProve = DBNullToSomething(data.Tables(0).Rows(0).Item("COD_PROVE"))
                m_sDenProve = DBNullToSomething(data.Tables(0).Rows(0).Item("DEN_PROVE"))
                m_dFechaFactura = DBNullToSomething(data.Tables(0).Rows(0).Item("FECHA"))
                m_dFechaContabilizacion = DBNullToSomething(data.Tables(0).Rows(0).Item("FEC_CONTA"))
                m_dImporte = DBNullToDbl(data.Tables(0).Rows(0).Item("IMPORTE"))
                m_dImporteBruto = DBNullToDbl(data.Tables(0).Rows(0).Item("IMPORTE_BRUTO"))
                m_dTotalRetencionGarantia = DBNullToDbl(data.Tables(0).Rows(0).Item("RET_GARANTIA"))
                m_sObservaciones = DBNullToSomething(data.Tables(0).Rows(0).Item("OBS"))
                m_sCodViaPago = DBNullToSomething(data.Tables(0).Rows(0).Item("VIA_PAG"))
                m_sViaPago = DBNullToSomething(data.Tables(0).Rows(0).Item("VIA_PAGO"))
                m_sCodFormaPago = DBNullToSomething(data.Tables(0).Rows(0).Item("PAG"))
                m_sFormaPago = DBNullToSomething(data.Tables(0).Rows(0).Item("FORMA_PAGO"))
                m_iEstado = DBNullToInteger(data.Tables(0).Rows(0).Item("ESTADO"))
                m_sEstadoDen = DBNullToSomething(data.Tables(0).Rows(0).Item("DEN"))
                m_iTipo = DBNullToInteger(data.Tables(0).Rows(0).Item("TIPO"))
                m_sMoneda = DBNullToSomething(data.Tables(0).Rows(0).Item("MON"))
                m_lIdEFactura = DBNullToInteger(data.Tables(0).Rows(0).Item("EFACTURA"))
                m_sCodPaisProve = DBNullToStr(data.Tables(0).Rows(0).Item("PAIS_PROVE"))

                m_dFechaInicioOriginal = DBNullToSomething(data.Tables(0).Rows(0).Item("FEC_ORIGINAL_INI"))
                m_dFechaFinOriginal = DBNullToSomething(data.Tables(0).Rows(0).Item("FEC_ORIGINAL_FIN"))
                m_sNumeroFacturaOriginal = DBNullToSomething(data.Tables(0).Rows(0).Item("NUM_ORIGINAL"))

                'Tolerancia
                m_dToleranciaImporte = DBNullToDbl(data.Tables(0).Rows(0).Item("TOLERANCIA_IMP"))
                m_dToleranciaPorcentaje = DBNullToDbl(data.Tables(0).Rows(0).Item("TOLERANCIA_PORCEN"))
                'No deberia haber valores nulos en Concepto. En caso de haberlos, se van a pasar a 0, o sea, Gasto.
                m_iConcepto = CType(DBNullToInteger(data.Tables(0).Rows(0).Item("CONCEPTO")), FSNLibrary.TiposDeDatos.ConceptoFactura)
            End If

            m_dTotalCostes = DBNullToDbl(data.Tables(1).Rows(0).Item(0))
            m_dTotalDescuentos = DBNullToDbl(data.Tables(2).Rows(0).Item(0))

            If data.Tables(15).Rows.Count > 0 Then m_lIdCosteGenerico = DBNullToInteger(data.Tables(15).Rows(0).Item(0))
            If data.Tables(16).Rows.Count > 0 Then m_lIdDescuentoGenerico = DBNullToInteger(data.Tables(16).Rows(0).Item(0))
            If data.Tables(17).Rows.Count > 0 Then m_bHayOtrosGestores = (DBNullToInteger(data.Tables(17).Rows(0).Item(0)) > 1)

            m_dtCostes = data.Tables(3)
            m_dtDescuentos = data.Tables(4)
            m_dtRetencionGarantia = data.Tables(5)
            m_dtLineas = data.Tables(6)

            Dim dcImporteCD As New DataColumn
            dcImporteCD.ColumnName = "IMPORTE_CD"
            dcImporteCD.DataType = System.Type.GetType("System.Double")
            dcImporteCD.DefaultValue = 0
            data.Tables(7).Columns.Add(dcImporteCD)
            dcImporteCD = New DataColumn
            dcImporteCD.ColumnName = "IMPORTE_CD"
            dcImporteCD.DataType = System.Type.GetType("System.Double")
            dcImporteCD.DefaultValue = 0
            data.Tables(8).Columns.Add(dcImporteCD)
            dcImporteCD = New DataColumn
            dcImporteCD.ColumnName = "IMPORTE_CD"
            dcImporteCD.DataType = System.Type.GetType("System.Double")
            dcImporteCD.DefaultValue = 0
            data.Tables(9).Columns.Add(dcImporteCD)
            m_dtImpuestosCabecera = data.Tables(7)
            m_dtImpuestosRepercutidos = data.Tables(8)
            m_dtImpuestosRetenidos = data.Tables(9)
            m_dtImpuestosFactura = data.Tables(10)
        End If

        m_dsDactura = data
        data = Nothing
    End Sub
    ''' <summary>
    ''' Dado un Id de instancia devuelve los mail que se han enviado a lo largo de su tratamiento
    ''' </summary>
    ''' <remarks>Llamada desde: detalleFactura.aspx</remarks>
    Public Function Load_Historico_Notificaciones(Optional ByVal sUsu As String = Nothing) As DataSet
        Authenticate()
        Return DBServer.Factura_Load_Historico_Notificaciones(m_lInstancia, sUsu)
    End Function
    ''' <summary>
    ''' Devuelve los atributos para el buscador de costes que cumplen los criterios pasados como parámetros.
    ''' </summary>
    ''' <remarks>Llamada desde: detalleFactura.aspx</remarks>
    Public Function Load_Atributos_BuscadorCostes(ByVal sCod As String, ByVal sDen As String, ByVal sCodArt As String, ByVal sCodMat As String) As DataSet
        Authenticate()
        Return DBServer.Factura_Load_Atributos_Buscador(TipoCD.Coste, sCod, sDen, sCodArt, sCodMat)
    End Function
    ''' <summary>
    ''' Devuelve los atributos para el buscador de descuentos que cumplen los criterios pasados como parámetros.
    ''' </summary>
    ''' <remarks>Llamada desde: detalleFactura.aspx</remarks>
    Public Function Load_Atributos_BuscadorDescuentos(ByVal sCod As String, ByVal sDen As String, ByVal sCodArt As String, ByVal sCodMat As String) As DataSet
        Authenticate()
        Return DBServer.Factura_Load_Atributos_Buscador(TipoCD.Descuento, sCod, sDen, sCodArt, sCodMat)
    End Function
    ''' <summary>
    ''' Dado un Id de instancia devuelve los mail que se han enviado a lo largo de su tratamiento
    ''' </summary>
    ''' <remarks>Llamada desde: detalleFactura.aspx</remarks>
    Public Function DevolverMotivoAnulacion()
        Authenticate()
        Return DBServer.Factura_DevolverMotivoAnulacion(m_lID)
    End Function
    ''' <summary>
    ''' Dado un Id de instancia devuelve los mail que se han enviado a lo largo de su tratamiento
    ''' </summary>
    ''' <remarks>Llamada desde: detalleFactura.aspx</remarks>
    Public Function BuscarFactura(ByVal sNumFactura As String, ByVal sCodProveedor As String) As Long
        Authenticate()
        Return DBServer.Factura_Buscar(sNumFactura, sCodProveedor)
    End Function
    ''' <summary>
    ''' Dado un Id de instancia devuelve el numero de la factura
    ''' </summary>
    ''' <remarks>Llamada desde: detalleFactura.aspx</remarks>
    Public Function DevolverNumeroFactura(ByVal lInstancia As Long) As String
        Authenticate()
        Return DBServer.Factura_DevolverNumeroFactura(lInstancia)
    End Function
    ''' <summary>
    ''' Guarda la factura electrónica generada
    ''' </summary>
    ''' <param name="sNom">Nombre de la factura</param>
    ''' <param name="sPath">Path donde se encuentra</param>
    ''' <param name="lFactura">Id de la factura</param>
    ''' <returns>El id de la factura electrónica, una vez insertada en base de datos</returns>
    ''' <remarks>Llamada desde: Service.asmx.vb --> TratarXML.</remarks>
    Public Function Save_EFactura(ByVal sNom As String, ByVal sPath As String, ByVal lFactura As Long) As Long()
        Authenticate()
        Return DBServer.Factura_Save_EFactura(sNom, sPath, DBServer.DBName, DBServer.DBServer, lFactura)
    End Function
    Public Function LeerEFactura(ByVal lIdEFactura As Long) As Byte()
        Authenticate()
        Return DBServer.Factura_LeerEFactura(lIdEFactura)
    End Function
#Region "Impuestos"
#End Region
#Region "Albarán y costes/descuentos del albarán"
    ''' <summary>Funcion que devuelve el detalle de un albarán con sus costes y descuentos</summary>
    ''' <remarks>
    ''' Llamada desde: detalleFactura.aspx
    ''' Tiempo máximo:0,2 seg</remarks>
    Public Sub LoadDetalleAlbaran(ByVal sAlbaran As String)
        Authenticate()

        Dim oDS As DataSet
        oDS = DBServer.Factura_DevolverDetalleAlbaran(m_lID, sAlbaran)
        If Not oDS.Tables(0).Rows.Count = 0 Then
            m_dFechaAlbaran = DBNullToStr(oDS.Tables(0).Rows(0).Item("FECHA"))
            m_sNumRecepcionErp = DBNullToStr(oDS.Tables(0).Rows(0).Item("NUM_ERP"))
            m_sCodReceptor = DBNullToStr(oDS.Tables(0).Rows(0).Item("COD"))
            m_sNombreReceptor = DBNullToStr(oDS.Tables(0).Rows(0).Item("NOMBRE"))
            m_sCargo = DBNullToStr(oDS.Tables(0).Rows(0).Item("CARGO"))
            m_sDepartamento = DBNullToStr(oDS.Tables(0).Rows(0).Item("DEPARTAMENTO"))
            m_sEmail = DBNullToStr(oDS.Tables(0).Rows(0).Item("EMAIL"))
            m_sTelefono = DBNullToStr(oDS.Tables(0).Rows(0).Item("TFNO"))
            m_sFax = DBNullToStr(oDS.Tables(0).Rows(0).Item("FAX"))
            m_sOrganizacion = DBNullToStr(oDS.Tables(0).Rows(0).Item("ORGANIZACION"))
        End If

        m_dtCostesAlbaran = oDS.Tables(1)
        m_dtDescuentosAlbaran = oDS.Tables(2)

        oDS = Nothing
    End Sub
#End Region
End Class
