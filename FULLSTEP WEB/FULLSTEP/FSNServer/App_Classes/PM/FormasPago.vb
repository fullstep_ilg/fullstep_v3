<Serializable()> _
    Public Class FormasPago
    Inherits Security

    Private Contexto As System.Web.HttpContext
    Private moFormasPago As DataSet
    Public ReadOnly Property Data() As Data.DataSet
        Get
            Return moFormasPago
        End Get
    End Property
    ''' <summary>
    ''' Procedimiento que carga los datos de las formas de pago disponibles
    ''' </summary>
    ''' <param name="sIdi">Idioma de la aplicaci�n</param>
    ''' <param name="sCod">C�digo de la forma de pago</param>
    ''' <remarks>
    ''' Llamada desde:PmWeb/denominacionFPago/Page_load,  PmWeb/formaspagoserver/Page_load, PmWeb/campos/Page_load, PmWeb/desglose/Page_load - cargarvaloresdefecto, PmWeb/impexp/DenGS
    ''' Tiempo m�ximo: 0,32 seg </remarks>
    Public Sub LoadData(ByVal sIdi As String, Optional ByVal sCod As String = Nothing)
        Authenticate()

        Dim ds As DataSet
        If sCod Is Nothing Then
            Contexto = System.Web.HttpContext.Current
            ds = CType(Contexto.Cache("dsTodasFormasPago" & sIdi), DataSet)
            If ds Is Nothing Then
                ds = DBServer.FormasPago_Get(sIdi, sCod)
                Contexto.Cache.Insert("dsTodasFormasPago" & sIdi, ds, Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration, New TimeSpan(0, System.Configuration.ConfigurationManager.AppSettings("TiempoExpiracionCacheMin"), 0))
            End If
        Else
            ds = DBServer.FormasPago_Get(sIdi, sCod)
        End If
        moFormasPago = ds
    End Sub
    Public Sub New(ByRef dbserver As FSNDataBaseServer.Root,ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
    End Sub
End Class