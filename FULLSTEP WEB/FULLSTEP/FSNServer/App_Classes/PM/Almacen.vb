
<Serializable()> _
Public Class Almacen
    Inherits Security

    Private mlID As Long
    Private msCod As String
    Private msDen As String

    Private moAlmacen As DataSet

    Public Property Id() As Long
        Get
            Return mlID
        End Get

        Set(ByVal Value As Long)
            mlID = Value
        End Set
    End Property


    Public Property Cod() As String
        Get
            Return msCod
        End Get

        Set(ByVal Value As String)
            msCod = Value
        End Set
    End Property

    Public Property Den() As String
        Get
            Return msDen
        End Get

        Set(ByVal Value As String)
            msDen = Value
        End Set
    End Property

    Public ReadOnly Property Data() As Data.DataSet
        Get
            Return moAlmacen
        End Get

    End Property

    ''' <summary>
    ''' Devuelve los datos del almacen
    ''' </summary>
    ''' <remarks>
    ''' Llamada desde:PmWeb/Campos/Page_Load , PMWeb/Desglose/Page_Load
    ''' Tiempo m�ximo: 0,3 seg</remarks>
    Public Sub LoadData()
        Authenticate()
        moAlmacen = DBServer.Almacen_Get(mlID)
    End Sub


    Public Sub New(ByRef dbserver As FSNDataBaseServer.Root,ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
    End Sub

End Class
