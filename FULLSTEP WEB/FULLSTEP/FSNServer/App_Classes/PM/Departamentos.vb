<Serializable()> _
Public Class Departamentos
    Inherits Security

    Private moDepartamentos As DataSet

    Public ReadOnly Property Data() As Data.DataSet
        Get
            Return moDepartamentos
        End Get

    End Property

    ''' <summary>
    ''' Carga los departamentos dependiendo del usuario (si se le pasa como par�metro) o la pyme a la que pertenece el usuario (entonces solo cargar� los departamentos de esa pyme)
    ''' </summary>
    ''' <param name="NivelACargar">Nivel de uons</param>
    ''' <param name="UON1">cod de la uon1</param>
    ''' <param name="UON2">cod de la uon2</param>
    ''' <param name="UON3">cod de la uon3</param>
    ''' <param name="sUsu">cod del usuario</param>
    ''' <param name="lPyme">id de la pyme</param>
    ''' <remarks>Llamada desde: PMWeb2008 --> wucBusquedaAvanzada.ascx.vb --> Page_Load; Tiempo m�ximo: 0,1</remarks>
    Public Sub LoadData(Optional ByVal NivelACargar As String = Nothing, Optional ByVal UON1 As String = Nothing, _
            Optional ByVal UON2 As String = Nothing, Optional ByVal UON3 As String = Nothing, _
            Optional ByVal sUsu As String = Nothing, Optional ByVal lPyme As Long = Nothing, _
            Optional ByVal RestricDEPTraslado As Boolean = False)
        Authenticate()
        moDepartamentos = DBServer.Departamentos_Load(NivelACargar, UON1, UON2, UON3, sUsu, lPyme, RestricDEPTraslado)
    End Sub

    ''' <summary>
    ''' Carga los departamentos dependiendo del usuario (si se le pasa como par�metro) o la pyme a la que pertenece el usuario (entonces solo cargar� los departamentos de esa pyme)
    ''' </summary>
    ''' <param name="NivelACargar">Nivel de uons</param>
    ''' <param name="UON1">cod de la uon1</param>
    ''' <param name="UON2">cod de la uon2</param>
    ''' <param name="UON3">cod de la uon3</param>
    ''' <param name="sUsu">cod del usuario</param>
    ''' <param name="lPyme">id de la pyme</param>
    ''' <remarks>Llamada desde: PMWeb --> usuarios.vb --> CargarDepartamentos; Tiempo m�ximo: 0,1</remarks>
    Public Sub LoadDataQA(Optional ByVal NivelACargar As String = Nothing, Optional ByVal UON1 As String = Nothing, Optional ByVal UON2 As String = Nothing, Optional ByVal UON3 As String = Nothing, Optional ByVal sUsu As String = Nothing, Optional ByVal bRestriccionDepartamento As Boolean = False, Optional ByVal lPyme As Long = Nothing)
        Authenticate()
        moDepartamentos = DBServer.Departamentos_LoadQA(NivelACargar, UON1, UON2, UON3, sUsu, bRestriccionDepartamento, lPyme)
    End Sub

    Public Sub New(ByRef dbserver As FSNDataBaseServer.Root,ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
    End Sub

End Class
