﻿Imports System.IO
<Serializable()>
Public Class Proyecto
    Inherits Security

    Public Structure AltaTarea
        Dim CodNivel1 As String
        Dim DescNivel1 As String
    End Structure

    Private miNumFasesProyecto As Byte
    Private msEstadoDefecto As String

    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
    ''' <summary>
    ''' Id del proyecto a cargar en el grid
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property NumFasesProyecto() As Byte
        Get
            Return miNumFasesProyecto
        End Get
        Set(ByVal value As Byte)
            miNumFasesProyecto = value
        End Set
    End Property
    ''' <summary>
    ''' Id del proyecto a cargar en el grid
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property EstadoDefecto() As String
        Get
            Return msEstadoDefecto
        End Get
        Set(ByVal value As String)
            msEstadoDefecto = value
        End Set
    End Property
    ''' <summary>
    ''' Funcion que devuelve las tareas de un proyecto
    ''' </summary>
    ''' <param name="lIdProyecto">Id del proyecto</param>
    ''' <returns>tareas del proyecto</returns>
    ''' <remarks>Desgloseactividad.ascx</remarks>
    Public Function DevolverTareasProyecto(ByVal lIdProyecto As Long, ByVal sIdioma As String) As DataSet
        Authenticate()
        Return DBServer.Proyecto_DevolverTareasProyecto(lIdProyecto, miNumFasesProyecto, msEstadoDefecto, sIdioma)
    End Function
    ''' <summary>
    ''' Funcion que devuelve los estados posibles para una tarea
    ''' </summary>
    ''' <param name="sIdioma">Idioma de la aplicacion</param>
    ''' <returns>datatable con los estados</returns>
    ''' <remarks></remarks>
    Public Function DevolverEstadosTareaDeProyecto(ByVal sIdioma As String) As DataTable
        Authenticate()
        Return DBServer.Proyecto_DevolverEstadosTareaDeProyecto(sIdioma)
    End Function
    ''' <summary>
    ''' Funcion que devuelve las denominaciones de las fases en esta instalación
    ''' </summary>
    ''' <returns>datatable con las denominaciones</returns>
    ''' <remarks>Desgloseactividad.ascx</remarks>
    Public Function DevolverDenominacionFases(ByVal sIdioma As String) As DataTable
        Authenticate()
        Return DBServer.Proyecto_DevolverDenominacionFases(sIdioma)
    End Function
    ''' <summary>
    ''' Funcion que comprueba si la tarea que se va a eliminar tiene horas imputadas
    ''' </summary>
    ''' <param name="idTarea">identificador de la tarea</param>
    ''' <returns>Devolvera "1" si tiene horas o "0" si no tiene</returns>
    ''' <remarks></remarks>
    Public Function ComprobarEliminarTarea(Optional ByVal idTarea As Long = 0, Optional ByVal idProy As Long = 0) As String
        Authenticate()
        Return DBServer.Proyecto_ComprobarEliminarTarea(idTarea, idProy)
    End Function
    ''' <summary>
    ''' Funcion que crea la plantilla excel para dar de alta tareas o Hitos, se generaran macros para validar el contenido
    ''' </summary>
    ''' <param name="NumFases">Numero de fases del proyecto</param>
    ''' <param name="sIdi">idioma</param>
    ''' <param name="sPlantilla">ruta de la plantilla</param>
    ''' <param name="sRutaTemp">ruta temporal donde se dejara para que se descargue</param>
    ''' <returns>True si todo ha ido correcto, false si hay error</returns>
    ''' <remarks></remarks>
    Public Function CrearPlantillaExcel(ByVal NumFases As Short, ByVal sIdi As String, ByVal sPlantilla As String, ByVal sRutaTemp As String, ByVal sNomPlantilla As String, ByVal LongitudesDeCodigos As TiposDeDatos.LongitudesDeCodigos) As String
        Dim sConnectionString As String
        sConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;" & _
            "Data Source=" & sPlantilla & ";" & _
            "Extended Properties=Excel 8.0;"
        Dim objConn As New System.Data.OleDb.OleDbConnection(sConnectionString)
        Dim objCmd As New System.Data.OleDb.OleDbCommand()

        Dim dtFases As DataTable
        Dim oTextos As DataTable
        Dim sConsulta As String

        Dim oDict As Dictionary = New Dictionary(DBServer, mIsAuthenticated)
        Dim dtEstados As DataTable = DevolverEstadosTareaDeProyecto(sIdi)

        oDict.LoadData(TiposDeDatos.ModulosIdiomas.DesgloseActividad, sIdi)
        oTextos = oDict.Data.Tables(0)

        dtFases = DevolverDenominacionFases(sIdi)

        'Establecer una conexion con el origen de datos. 
        objConn.Open()

        objCmd.Connection = objConn
        sConsulta = "Update [DENOMINACIONES] set NUM='" & oTextos(4)(1) & "',COD_NIV1='" & oTextos(21)(1) & "." & dtFases(0)(1) & "', DEN_NIV1='" & dtFases(0)(1) & "'"
        sConsulta = sConsulta & ",COD_NIV2='" & oTextos(21)(1) & "." & dtFases(1)(1) & "', DEN_NIV2='" & dtFases(1)(1) & "'"
        sConsulta = sConsulta & ",COD_NIV3='" & oTextos(21)(1) & "." & dtFases(2)(1) & "', DEN_NIV3='" & dtFases(2)(1) & "'"
        sConsulta = sConsulta & ",COD_NIV4='" & oTextos(21)(1) & "." & dtFases(3)(1) & "', DEN_NIV4='" & dtFases(3)(1) & "'"
        sConsulta = sConsulta & ",COD_TAREA='" & oTextos(21)(1) & "." & dtFases(4)(1) & "', DEN_TAREA='" & dtFases(4)(1) & "'"
        sConsulta = sConsulta & ",HITO='" & oTextos(14)(1) & "', FEC_INI='" & oTextos(1)(1) & "'"
        sConsulta = sConsulta & ",FEC_FIN='" & oTextos(2)(1) & "', ESTADO='" & oTextos(6)(1) & "'"
        sConsulta = sConsulta & ",HORAS='" & oTextos(3)(1) & "', GRADO='" & oTextos(5)(1) & "'"
        sConsulta = sConsulta & ",OBS='" & oTextos(12)(1) & "',FASES=" & NumFases

        objCmd.CommandText = sConsulta
        objCmd.ExecuteNonQuery()

        sConsulta = "Update [LONGITUDES] set LONG_COD=" & LongitudesDeCodigos.giLongCodFSGA_CAT_TARE_COD & ",LONG_DEN=" & LongitudesDeCodigos.giLongCodFSGA_CAT_TARE_DEN
        sConsulta = sConsulta & ", LONG_COD_TARE=" & LongitudesDeCodigos.giLongCodFSGA_TARE_COD & ", LONG_DEN_TARE=" & LongitudesDeCodigos.giLongCodFSGA_TARE_DEN
        sConsulta = sConsulta & ", LONG_OBS=" & LongitudesDeCodigos.giLongCodFSGA_TARE_OBS
        objCmd.CommandText = sConsulta
        objCmd.ExecuteNonQuery()

        sConsulta = "UPDATE [ESTADOS] SET ESTADO=''"
        objCmd.CommandText = sConsulta
        objCmd.ExecuteNonQuery()

        Dim ind As Short = 1
        For Each dr As DataRow In dtEstados.Rows
            sConsulta = "UPDATE [ESTADOS] SET ESTADO='" & dr(0) & "-" & dr(1).ToString & "' WHERE ID=" & ind.ToString
            objCmd.CommandText = sConsulta
            objCmd.ExecuteNonQuery()
            ind += 1
        Next

        objConn.Close()
        objCmd = Nothing
        objConn = Nothing

        Dim sPath As String
        Dim sCarpeta As String

        sCarpeta = modUtilidades.GenerateRandomPath()
        sPath = sRutaTemp + "\" + sCarpeta
        If Not Directory.Exists(sPath) Then
            Directory.CreateDirectory(sPath)
        End If
        File.Copy(sPlantilla, sPath & "\" & sNomPlantilla, True)
        Dim MyFile As New FileInfo(sPath & "\" & sNomPlantilla)
        Dim FileSize As Long = MyFile.Length
        FileSize = FileSize / 1024
        Return sCarpeta & "#" & FileSize.ToString

    End Function
    ''' <summary>
    ''' Funcion que lee la plantilla excel subida por el usuario con las tareas del proyecto
    ''' </summary>
    ''' <param name="sPath">Ruta de la plantilla</param>
    ''' <param name="sIdi">Idioma</param>
    ''' <returns>Retorna si todo es correcto un string con toda la plantilla o ERROR si algo falla</returns>
    ''' <remarks></remarks>
    Public Function LeerPlantillaExcel(ByVal sPath As String, ByVal sIdi As String) As String
        Dim sConnectionString As String
        Dim adaptador1 As OleDb.OleDbDataAdapter
        Dim bFilaVacia As Boolean
        Dim ds As DataSet
        sConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;" & _
            "Data Source=" & sPath & ";" & _
            "Extended Properties=Excel 8.0;"
        Dim objCmd As New System.Data.OleDb.OleDbCommand()
        Dim con As System.Data.OleDb.OleDbConnection = New System.Data.OleDb.OleDbConnection(sConnectionString)

        Try

            adaptador1 = New System.Data.OleDb.OleDbDataAdapter("SELECT * FROM [Sheet1$]", con)
            ds = New DataSet
            adaptador1.Fill(ds, "Datos")

            DevolverTareasProyecto(0, sIdi)

            Dim sValores As String = ""

            Select Case miNumFasesProyecto
                Case 1
                    For Each dr As DataRow In ds.Tables(0).Rows
                        bFilaVacia = False
                        For cCnt = 0 To 2
                            If cCnt = 0 Then
                                If dr(cCnt) Is System.DBNull.Value OrElse dr(cCnt).ToString = "" Then
                                    bFilaVacia = True
                                    Exit For
                                End If
                                sValores = sValores & dr(cCnt)
                            Else
                                If IsDate(dr(cCnt)) Then
                                    sValores = sValores & "$$" & CDate(dr(cCnt)).ToString("dd/MM/yyyy")
                                ElseIf dr(cCnt) Is System.DBNull.Value Then
                                    sValores = sValores & "$$" & ""
                                Else
                                    sValores = sValores & "$$" & dr(cCnt)
                                End If
                            End If
                        Next
                        If bFilaVacia = False Then
                            For cCnt = 9 To 17
                                If cCnt = 0 Then
                                    If dr(cCnt) Is System.DBNull.Value OrElse dr(cCnt).ToString = "" Then
                                        bFilaVacia = True
                                        Exit For
                                    End If
                                    sValores = sValores & dr(cCnt)
                                Else
                                    If IsDate(dr(cCnt)) Then
                                        sValores = sValores & "$$" & CDate(dr(cCnt)).ToString("dd/MM/yyyy")
                                    ElseIf dr(cCnt) Is System.DBNull.Value Then
                                        sValores = sValores & "$$" & ""
                                    Else
                                        sValores = sValores & "$$" & dr(cCnt)
                                    End If
                                End If
                            Next
                        End If
                        If bFilaVacia = False Then
                            sValores = sValores & "&&"
                        End If
                    Next
                Case 2
                    For Each dr As DataRow In ds.Tables(0).Rows
                        bFilaVacia = False
                        For cCnt = 0 To 4
                            If cCnt = 0 Then
                                If dr(cCnt) Is System.DBNull.Value OrElse dr(cCnt).ToString = "" Then
                                    bFilaVacia = True
                                    Exit For
                                End If
                                sValores = sValores & dr(cCnt)
                            Else
                                If IsDate(dr(cCnt)) Then
                                    sValores = sValores & "$$" & CDate(dr(cCnt)).ToString("dd/MM/yyyy")
                                ElseIf dr(cCnt) Is System.DBNull.Value Then
                                    sValores = sValores & "$$" & ""
                                Else
                                    sValores = sValores & "$$" & dr(cCnt)
                                End If
                            End If
                        Next
                        If bFilaVacia = False Then
                            For cCnt = 9 To 17
                                If cCnt = 0 Then
                                    If dr(cCnt) Is System.DBNull.Value OrElse dr(cCnt).ToString = "" Then
                                        bFilaVacia = True
                                        Exit For
                                    End If
                                    sValores = sValores & dr(cCnt)
                                Else
                                    If IsDate(dr(cCnt)) Then
                                        sValores = sValores & "$$" & CDate(dr(cCnt)).ToString("dd/MM/yyyy")
                                    ElseIf dr(cCnt) Is System.DBNull.Value Then
                                        sValores = sValores & "$$" & ""
                                    Else
                                        sValores = sValores & "$$" & dr(cCnt)
                                    End If
                                End If
                            Next
                        End If
                        If bFilaVacia = False Then
                            sValores = sValores & "&&"
                        End If
                    Next
                Case 3
                    For Each dr As DataRow In ds.Tables(0).Rows
                        bFilaVacia = False
                        For cCnt = 0 To 6
                            If cCnt = 0 Then
                                If dr(cCnt) Is System.DBNull.Value OrElse dr(cCnt).ToString = "" Then
                                    bFilaVacia = True
                                    Exit For
                                End If
                                sValores = sValores & dr(cCnt)
                            Else
                                If IsDate(dr(cCnt)) Then
                                    sValores = sValores & "$$" & CDate(dr(cCnt)).ToString("dd/MM/yyyy")
                                ElseIf dr(cCnt) Is System.DBNull.Value Then
                                    sValores = sValores & "$$" & ""
                                Else
                                    sValores = sValores & "$$" & dr(cCnt)
                                End If
                            End If
                        Next
                        If bFilaVacia = False Then
                            For cCnt = 9 To 17
                                If cCnt = 0 Then
                                    If dr(cCnt) Is System.DBNull.Value OrElse dr(cCnt).ToString = "" Then
                                        bFilaVacia = True
                                        Exit For
                                    End If
                                    sValores = sValores & dr(cCnt)
                                Else
                                    If IsDate(dr(cCnt)) Then
                                        sValores = sValores & "$$" & CDate(dr(cCnt)).ToString("dd/MM/yyyy")
                                    ElseIf dr(cCnt) Is System.DBNull.Value Then
                                        sValores = sValores & "$$" & ""
                                    Else
                                        sValores = sValores & "$$" & dr(cCnt)
                                    End If
                                End If
                            Next
                        End If
                        If bFilaVacia = False Then
                            sValores = sValores & "&&"
                        End If
                    Next
                Case 4
                    For Each dr As DataRow In ds.Tables(0).Rows
                        bFilaVacia = False
                        For cCnt = 0 To 17
                            If cCnt = 0 Then
                                If dr(cCnt) Is System.DBNull.Value OrElse dr(cCnt).ToString = "" Then
                                    bFilaVacia = True
                                    Exit For
                                End If
                                sValores = sValores & dr(cCnt)
                            Else
                                If IsDate(dr(cCnt)) Then
                                    sValores = sValores & "$$" & CDate(dr(cCnt)).ToString("dd/MM/yyyy")
                                ElseIf dr(cCnt) Is System.DBNull.Value Then
                                    sValores = sValores & "$$" & ""
                                Else
                                    sValores = sValores & "$$" & dr(cCnt)
                                End If
                            End If
                        Next
                        If bFilaVacia = False Then
                            sValores = sValores & "&&"
                        End If
                    Next
            End Select
            sValores = sValores.Substring(0, sValores.Length - 2)

            Return sValores
        Catch ex As Exception
            Return "ERROR"

        Finally
            con.Dispose()
            objCmd.Dispose()
            con = Nothing
            objCmd = Nothing
        End Try

    End Function
    ''' <summary>
    ''' Comprueba los usuarios que tengan una tarea de FSGA asignada próxima a iniciarse. El periodo de antelación para el 
    ''' aviso viene dado por el parámetro de PARGEN_INTERNO FSGA_PERIODO_NOTIF
    ''' </summary>
    ''' <remarks>Llamada desde: FSGA_Notificaciones.asmx; Tiempo maximo: 0,2</remarks>
    Public Sub Notificar_InicioTarea_FSGA()
        Authenticate()
        Dim oNotificador As New Notificar(DBServer, mIsAuthenticated)
        oNotificador.NotificacionInicioTareaFSGA()
        oNotificador.FuerzaFinalizeSmtpClient()
        oNotificador = Nothing
    End Sub
End Class