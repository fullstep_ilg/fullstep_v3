<Serializable()> _
    Public Class Users
    Inherits Security

    Private moUsers As DataSet

    Public ReadOnly Property Data() As Data.DataSet
        Get
            Return moUsers
        End Get

    End Property
    ''' <summary>
    ''' Carga en un dataset todos los posibles valores del combo de revisores, posibles segun las restricciones del usuario conectado, que 
    ''' sea usuario de qa, que no este de baja como personay/o usuario y que este marcado como revisor. Restricciones del usuario conectado
    '''  pueden ser a los de su mismo departamento o de su misma UO.
    ''' Tambien puede usarse para cargar el nombre de un revisor concreto, de haberlo, si se le pasa algo en el parametro sCodigo (en 
    ''' esto NO se mira que efectivamente sea revisor pq Fagor tiene a Miren q no estaba como revisor a 2503 y sin esto no se vería).
    ''' </summary>
    ''' <param name="FSQARestRevisorUO">Restricción a los de su misma UO</param>
    ''' <param name="FSQARestRevisorDep">Restricción a los de su mismo departamento</param>
    ''' <param name="UON1">Optional. Si restricción a UO contiene UO1 del usuario conectado</param>
    ''' <param name="UON2">Optional. Si restricción a UO contiene UO2 del usuario conectado</param>
    ''' <param name="UON3">Optional. Si restricción a UO contiene UO3 del usuario conectado</param>
    ''' <param name="Dep">Optional. Si restricción a departamento contiene departamento del usuario conectado</param>
    ''' <param name="Codigo">Optional. Codigo de ususario q es revisor </param>
    ''' <remarks>Llamadas: AltaNoConformidad.aspx/page_load   DetalleNoConformidad.aspx/page_load     DetalleNoConformidad.aspx/CargaInicialRevisor; Tiempo: 0,1</remarks>
    Public Sub LoadData(ByVal FSQARestRevisorUO As Boolean, ByVal FSQARestRevisorDep As Boolean, Optional ByVal UON1 As String = Nothing, Optional ByVal UON2 As String = Nothing, _
    Optional ByVal UON3 As String = Nothing, Optional ByVal Dep As String = Nothing, Optional ByVal Codigo As String = Nothing)
        moUsers = DBServer.CargarRevisores(FSQARestRevisorUO, FSQARestRevisorDep, UON1, UON2, UON3, Dep, Codigo)
    End Sub
    Public Sub New(ByRef dbserver As FSNDataBaseServer.Root,ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
    End Sub
End Class
