﻿Public Class Partida
    Inherits Security

    Private moData As DataSet
    Private sPres0 As String
    Private sCod As String
    Private sDenominacion As String
    Private dPresupuestado As Double
    Private dComprometido As Double
    Private dSolicitado As Double
    Private sMoneda As String
    Private dFC As Double
    Public ReadOnly Property Data() As Data.DataSet
        Get
            Return moData
        End Get
    End Property
    Public Property Pres0() As String
        Get
            Return sPres0
        End Get
        Set(ByVal value As String)
            sPres0 = value
        End Set
    End Property
    Public Property Cod() As String
        Get
            Return sCod
        End Get
        Set(ByVal value As String)
            sCod = value
        End Set
    End Property
    Public Property Denominacion() As String
        Get
            Return sDenominacion
        End Get
        Set(ByVal value As String)
            sDenominacion = value
        End Set
    End Property
    Public Property Presupuestado() As Double
        Get
            Return dPresupuestado
        End Get
        Set(ByVal value As Double)
            dPresupuestado = value
        End Set
    End Property
    Public Property Comprometido() As Double
        Get
            Return dComprometido
        End Get
        Set(ByVal value As Double)
            dComprometido = value
        End Set
    End Property
    Public Property Solicitado() As Double
        Get
            Return dSolicitado
        End Get
        Set(ByVal value As Double)
            dSolicitado = value
        End Set
    End Property
    Public Property Moneda() As String
        Get
            Return sMoneda
        End Get
        Set(ByVal value As String)
            sMoneda = value
        End Set
    End Property
    Public Property FC() As Double
        Get
            Return dFC
        End Get
        Set(ByVal value As Double)
            dFC = value
        End Set
    End Property

    ''' <summary>
    ''' Obtiene la denominación de la partida presupuestaria a través del código que se pasa como parámetro.
    ''' </summary>
    ''' <param name="sUsuario">Código de la persona</param>
    ''' <param name="sIdioma">Idioma del usuario</param>
    ''' <param name="sPres5">Código de la raiz presupuestaria</param>
    ''' <param name="sCodigo">Código de la partida a buscar</param>
    ''' <param name="sCentroCoste">Centro de coste donde buscar (uon1#uon2#uon3#uon4)</param>
    ''' <remarks>Llamada desde: PMWeb --> BuscadorPartidaServer.aspx; Tiempo máximo: 1sg</remarks>
    Public Sub BuscarPartida(ByVal sUsuario As String, ByVal sIdioma As String, ByVal sPres5 As String, ByVal sCodigo As String, Optional ByVal sCentroCoste As String = Nothing)
        Authenticate()
        moData = DBServer.Partida_Busqueda(sUsuario, sIdioma, sPres5, sCodigo, sCentroCoste)
    End Sub
    ''' <summary>
    ''' Obtiene el detalle de una partida (cod, denominación, centro, fechas de inicio y de fin)
    ''' </summary>
    ''' <param name="sPRES5">Cod partida presupuestaria</param>
    ''' <param name="sTexto">Cadena que contiene las uons y los códigos de pres5</param>
    ''' <param name="sIdioma">Idioma del usuario</param>
    ''' <returns>El detalle de la partida</returns>
    ''' <remarks>Llamada desde: detallepartida.aspx; Tiempo máximo: 1 sg;</remarks>
    Public Function BuscarDetallePartida(ByVal sPRES5 As String, ByVal sTexto As String, ByVal sIdioma As String) As DataSet
        Authenticate()
        BuscarDetallePartida = DBServer.Partida_Detalle(sPRES5, sTexto, sIdioma)
    End Function
    ''' <summary>
    ''' Devuelve la denominación de la partida en el idioma del usuario
    ''' </summary>
    ''' <param name="sPRES5">Cod de pres5_niv0</param>
    ''' <param name="sIdioma">Idioma del usuario</param>
    ''' <returns>La denominación de la partida en el idioma del usuario</returns>
    ''' <remarks>LLamada desde: guardarinstancia.aspx; Tiempo máximo: 0 sg</remarks>
    Public Function ObtenerDenominacionPartida(ByVal sPRES5 As String, ByVal sIdioma As String) As String
        Authenticate()
        ObtenerDenominacionPartida = DBServer.Partida_Denominacion(sPRES5, sIdioma)
    End Function
    ''' <summary>
    ''' Comprueba que las fechas de inicio y/o fin de suministro están dentro de la vigencia de la partida
    ''' </summary>
    ''' <param name="sPRES5">Cod de pres5_niv0</param>
    ''' <param name="sPartida">Cod de pres5_niv1, ni2, ni3, ... separdos por |</param>
    ''' <param name="dFechaIniSuministro">Fecha de inicio de suministro a comprobar</param>
    ''' <param name="dFechaFinSuministro">Fecha de fin de suministro a comprobar</param>
    ''' <returns>True si cumple el control de fechas; False en caso contrario</returns>
    ''' <remarks>Llamada desde: guardarinstancia.aspx; Tiempo máximo: 0 sg;</remarks>
    Public Function CumpleControlFechas(ByVal sPRES5 As String, ByVal sPartida As String, Optional ByVal dFechaIniSuministro As DateTime = Nothing, Optional ByVal dFechaFinSuministro As DateTime = Nothing) As Boolean
        Authenticate()
        CumpleControlFechas = DBServer.Partida_CumpleControlFechas(sPRES5, sPartida, dFechaIniSuministro, dFechaFinSuministro)
    End Function
    Public Function ActualizarImportesPartida(ByVal Pres0 As String, ByVal Pres1 As String, ByVal Pres2 As String, ByVal Pres3 As String, ByVal Pres4 As String, ByVal TieneControlDisponible As Boolean,
                                        ByVal IdInstancia As Integer, ByVal Moneda As String, ByVal Solicitado As Double, ByVal Idioma As String, ByVal AvisoConfirmado As Boolean,
                                        Optional ByVal CantidadPedida As Double = 0, Optional ByVal ImportePedido As Double = 0, Optional ByVal IdCampoSolicit As Integer = 0,
                                        Optional ByVal LineaPedido As Integer = 0, Optional ByVal bControlDispConPres As Boolean = False, Optional ByVal iAnyo As Integer = 0,
                                        Optional ByVal IdCesta As Long = 0) As Object
        Authenticate()
        Return DBServer.Partida_ActualizarImportesPartida(Pres0, Pres1, Pres2, Pres3, Pres4, TieneControlDisponible, IdInstancia, Moneda, Solicitado, Idioma, AvisoConfirmado,
                                                          CantidadPedida, ImportePedido, IdCampoSolicit, LineaPedido, bControlDispConPres, iAnyo, IdCesta)
    End Function
    Public Function ActualizarImportesPartidaRechazoAnulacion(ByVal IdInstancia As Integer, ByVal Idioma As String, Optional ByVal EsPartidaPlurianual As Boolean = False) As Object
		Authenticate()
        Return DBServer.Partida_ActualizarImportesPartida("", "", "", "", "", False, IdInstancia, "", 0, Idioma, False, 0, 0, 0, 0, False, IIf(EsPartidaPlurianual, -1, 0), 0)
    End Function
	Public Sub ActualizarDatosInstanciaPartida(ByVal Instancia As Integer, ByVal xmlDatosInstanciaPartida As String, ByVal EsAltaSolicitud As Boolean, Optional EsPartidaPlurianual As Boolean = False)
		Authenticate()
		DBServer.Partida_ActualizarDatosInstanciaPartida(Instancia, xmlDatosInstanciaPartida, EsAltaSolicitud, EsPartidaPlurianual)
	End Sub
	Public Sub CancelarCambiosPartidasPresupuestarias(ByVal Instancia As Integer, ByVal xmlDatosInstanciaPartidaCancelar As String)
        Authenticate()
        DBServer.Partida_CancelarCambiosPartidasPresupuestarias(Instancia, xmlDatosInstanciaPartidaCancelar)
    End Sub
#Region "Constructor"
    ''' <summary>
    ''' Constructor de la clase partida
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si está autenticado</param>
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
#End Region
End Class
