﻿<Serializable()> _
    Public Class Filtros
    Inherits Security
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
    ''' <summary>
    ''' Carga los formularios disponibles para que el usuario pueda configurar sus filtros.
    ''' </summary>
    ''' <param name="sUsuario">Cod. de persona.</param>
    ''' <param name="iTipo">Tipo de solicitud</param>
    ''' <returns>Dataset con los formularios disponibles</returns>
    ''' <remarks></remarks>
    Public Function LoadDisponibles(ByVal sUsuario As String, Optional ByVal iTipo As Integer = TiposDeDatos.TipoDeSolicitud.SolicitudDeCompras, Optional ByVal EsPyme As Boolean = False, Optional ByVal Uon1 As String = Nothing, Optional ByVal Uon2 As String = Nothing, Optional ByVal Uon3 As String = Nothing) As DataSet
        Authenticate()
        LoadDisponibles = DBServer.Filtros_Get(sUsuario, iTipo, EsPyme, Uon1, Uon2, Uon3)
    End Function
    ''' <summary>
    ''' Obtiene los filtros configurados por el usuario con el nÂº total de solicitudes por filtro.
    ''' </summary>
    ''' <param name="sUsuario">Cod. de persona</param>
    ''' <param name="sTipo">Tipo de solicitud</param>
    ''' <returns>Dataset con los filtros configurados para cargar las pestaÃ±as</returns>
    ''' <remarks>Llamada desde: VisorSolicitudes.aspx. Tiempo mÃ¡ximo: 10 sg (aprox. 1 sg por filtro)</remarks>
    Public Function LoadConfigurados(ByVal sUsuario As String, Optional ByVal sTipo As Short = TiposDeDatos.TipoDeSolicitud.SolicitudDeCompras) As DataSet
        Authenticate()
        LoadConfigurados = DBServer.Filtros_GetConfigurados(sUsuario, sTipo)
    End Function
    ''' <summary>
    ''' Obtiene los filtros disponibles que ha configurado el usuario.
    ''' </summary>
    ''' <param name="sUsuario">Cod. de la persona</param>
    ''' <returns>Dataset con el ID y Nombre de los filtros disponibles que ha configurado el usuario.</returns>
    ''' <remarks>Llamada desde: FSPMWebPartFiltrosDisponibles. Tiempo máximo:0 sg.</remarks>
    Public Function LoadDisponiblesYaConfigurados(ByVal sUsuario As String, Optional ByVal iTipo As Short = 0) As DataSet
        Authenticate()
        LoadDisponiblesYaConfigurados = DBServer.Filtros_GetDisponibles(sUsuario, iTipo)
    End Function
    ''' <summary>
    ''' Obtiene los filtros disponibles que ha seleccionado el usuario.
    ''' </summary>
    ''' <param name="sUsuario">Cod. de la persona</param>
    ''' <returns>Dataset con el ID y Nombre de los filtros disponibles que ha seleccionado el usuario.</returns>
    ''' <remarks>Llamada desde: FSPMWebPartFiltrosDisponibles. Tiempo máximo:0 sg.</remarks>
    Public Function LoadDisponiblesSeleccionados(ByVal sUsuario As String, Optional ByVal sFiltros As String = Nothing) As DataSet
        Authenticate()
        LoadDisponiblesSeleccionados = DBServer.Filtros_GetDisponiblesSeleccionados(sUsuario, sFiltros)
    End Function
End Class
