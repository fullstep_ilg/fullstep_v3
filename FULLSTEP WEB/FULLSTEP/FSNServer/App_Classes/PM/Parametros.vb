<Serializable()> _
Public Class Parametros
    Inherits Security

#Region "Properties"
    Private m_iQAProxExpirar As Integer
    Private m_bPotAValAdjudicacion As Boolean
    Private m_bPotAValPedDirGS As Boolean
    Private m_bPotAValPedEP As Boolean
    Private m_bPotAValProvisionalSelProve As Boolean
    Private m_bNotifUsuAsigMatVal As Boolean
    Private m_bNotifUsuAsigMatPot As Boolean
    Private m_bNotifUsuDesAsigMatVal As Boolean
    Private m_bNotifUsuDesAsigMatPot As Boolean
    Private m_bNotifUsuAltaMatGS As Boolean
    Private m_sContactoAutom As String
    Private m_iQAPeriodoValidezCertificado As Integer
    Private m_bNotifProveCambioCalif As Boolean
    Private m_iTipoNotifProveCambioCalif As Byte
    Private m_bNotifProximFinResolucionNc As Boolean
    Private m_bNotifProximFinAccionesNc As Boolean
    Private m_iQAPeriodoNotificacionNc As Integer
    Private m_sQANivelesDefectoUNQAProveedor As String
    Private m_iMaximoNivelUNQA As Integer

    Public Property QAProxExpirar() As Integer
        Get
            QAProxExpirar = m_iQAProxExpirar
        End Get
        Set(ByVal Value As Integer)
            m_iQAProxExpirar = Value
        End Set
    End Property
    Public Property PotAValAdjudicacion() As Boolean
        Get
            PotAValAdjudicacion = m_bPotAValAdjudicacion
        End Get
        Set(ByVal Value As Boolean)
            m_bPotAValAdjudicacion = Value
        End Set
    End Property
    Public Property PotAValPedDirGS() As Boolean
        Get
            PotAValPedDirGS = m_bPotAValPedDirGS
        End Get
        Set(ByVal Value As Boolean)
            m_bPotAValPedDirGS = Value
        End Set
    End Property
    Public Property PotAValPedEP() As Boolean
        Get
            PotAValPedEP = m_bPotAValPedEP
        End Get
        Set(ByVal Value As Boolean)
            m_bPotAValPedEP = Value
        End Set
    End Property
    Public Property PotAValProvisionalSelProve As Boolean
        Get
            PotAValProvisionalSelProve = m_bPotAValProvisionalSelProve
        End Get
        Set(value As Boolean)
            m_bPotAValProvisionalSelProve = value
        End Set
    End Property
    Public Property NotifUsuAsigMatVal() As Boolean
        Get
            NotifUsuAsigMatVal = m_bNotifUsuAsigMatVal
        End Get
        Set(ByVal Value As Boolean)
            m_bNotifUsuAsigMatVal = Value
        End Set
    End Property
    Public Property NotifUsuAsigMatPot() As Boolean
        Get
            NotifUsuAsigMatPot = m_bNotifUsuAsigMatPot
        End Get
        Set(ByVal Value As Boolean)
            m_bNotifUsuAsigMatPot = Value
        End Set
    End Property
    Public Property NotifUsuDesAsigMatVal() As Boolean
        Get
            NotifUsuDesAsigMatVal = m_bNotifUsuDesAsigMatVal
        End Get
        Set(ByVal Value As Boolean)
            m_bNotifUsuDesAsigMatVal = Value
        End Set
    End Property
    Public Property NotifUsuDesAsigMatPot() As Boolean
        Get
            NotifUsuDesAsigMatPot = m_bNotifUsuDesAsigMatPot
        End Get
        Set(ByVal Value As Boolean)
            m_bNotifUsuDesAsigMatPot = Value
        End Set
    End Property
    Public Property NotifUsuAltaMatGS() As Boolean
        Get
            NotifUsuAltaMatGS = m_bNotifUsuAltaMatGS
        End Get
        Set(ByVal Value As Boolean)
            m_bNotifUsuAltaMatGS = Value
        End Set
    End Property
    Public Property ContactoAutomQA() As String
        Get
            ContactoAutomQA = m_sContactoAutom
        End Get
        Set(ByVal Value As String)
            m_sContactoAutom = Value
        End Set
    End Property
    Public Property QAPeriodoValidezCertificado() As Integer
        Get
            QAPeriodoValidezCertificado = m_iQAPeriodoValidezCertificado
        End Get
        Set(ByVal Value As Integer)
            m_iQAPeriodoValidezCertificado = Value
        End Set
    End Property
    ''' <summary>
    ''' Es un string separado por # con los niveles por defecto que se muestran en la ficha de calidad de portal
    ''' si el usuario no ha guardado una configuraci�n personal de la pantalla
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property QANivelesDefectoUNQAProveedor() As String
        Get
            QANivelesDefectoUNQAProveedor = m_sQANivelesDefectoUNQAProveedor
        End Get
        Set(ByVal Value As String)
            m_sQANivelesDefectoUNQAProveedor = Value
        End Set
    End Property
    ''' <summary>
    ''' Es el m�ximo nivel de la base de datos para las UNQA
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property QAMaximoNivelUNQA() As Integer
        Get
            QAMaximoNivelUNQA = m_iMaximoNivelUNQA
        End Get
        Set(ByVal Value As Integer)
            m_iMaximoNivelUNQA = Value
        End Set
    End Property
    Private _fitroMatQA As Integer
    Public Property FiltroMaterialQA() As Operadores.FiltroMaterial
        Get
            Return _fitroMatQA
        End Get
        Set(value As Operadores.FiltroMaterial)
            _fitroMatQA = value
        End Set
    End Property
    ''' <summary>
    ''' Devolver/establecer el valor del parametro de Seguridad de QA "Notificar a los proveedores los 
    ''' cambios de calificaci�n en la puntuaci�n total"
    ''' </summary>
    ''' <returns>valor del parametro de Seguridad de QA</returns>
    ''' <remarks>Llamada desde: _common/parametrosGenerales.aspx; Tiempo m�ximo:0</remarks>
    Public Property NotifProveCambioCalif() As Boolean
        Get
            NotifProveCambioCalif = m_bNotifProveCambioCalif
        End Get
        Set(ByVal Value As Boolean)
            m_bNotifProveCambioCalif = Value
        End Set
    End Property
    Public Property TipoNotifProveCambioCalif As Byte
        Get
            Return m_iTipoNotifProveCambioCalif
        End Get
        Set(ByVal Value As Byte)
            m_iTipoNotifProveCambioCalif = Value
        End Set
    End Property
    ''' <summary>
    ''' Devolver/establecer el valor del parametro de Seguridad de QA "Notificar a los proveedores de la 
    ''' pr�ximidad de la fecha fin de resoluci�n de una no conformidad abierta"
    ''' </summary>
    ''' <returns>valor del parametro de Seguridad de QA</returns>
    ''' <remarks>Llamada desde: _common/parametrosGenerales.aspx; Tiempo m�ximo:0</remarks>
    Public Property NotifProximFinResolucionNc() As Boolean
        Get
            NotifProximFinResolucionNc = m_bNotifProximFinResolucionNc
        End Get
        Set(ByVal Value As Boolean)
            m_bNotifProximFinResolucionNc = Value
        End Set
    End Property
    ''' <summary>
    ''' Devolver/establecer el valor del parametro de Seguridad de QA "Notificar a los proveedores de la 
    ''' pr�ximidad de las fechas l�mite de las acciones solicitidas en una no conformidad abierta"
    ''' </summary>
    ''' <returns>valor del parametro de Seguridad de QA</returns>
    ''' <remarks>Llamada desde: _common/parametrosGenerales.aspx; Tiempo m�ximo:0</remarks>
    Public Property NotifProximFinAccionesNc() As Boolean
        Get
            NotifProximFinAccionesNc = m_bNotifProximFinAccionesNc
        End Get
        Set(ByVal Value As Boolean)
            m_bNotifProximFinAccionesNc = Value
        End Set
    End Property
    ''' <summary>
    ''' Devolver/establecer el valor del parametro de Seguridad de QA "Enviar los avisos de recordatorio 
    ''' por proximidad de fechas de fin de las no conformidades con la siguiente antelaci�n"
    ''' </summary>
    ''' <returns>valor del parametro de Seguridad de QA</returns>
    ''' <remarks>Llamada desde: _common/parametrosGenerales.aspx; Tiempo m�ximo:0</remarks>
    Public Property QAPeriodoNotificacionNc() As Integer
        Get
            QAPeriodoNotificacionNc = m_iQAPeriodoNotificacionNc
        End Get
        Set(ByVal Value As Integer)
            m_iQAPeriodoNotificacionNc = Value
        End Set
    End Property
#End Region
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
    ''' <summary>
    ''' Carga los parametros contenidos en PARGEN_QA
    ''' </summary>
    ''' <remarks>Llamada desde: parametrosgenerales.aspx/Page_PreRender 
    ''' parametrosgenerales.aspx/cmdAceptar_Click; Tiempo m�ximo: 0,1</remarks>
    ''' revisado por ilg(15/11/2011)
    Public Sub LoadData()
        Authenticate()

        Dim data As DataSet
        Dim row As DataRow
        data = DBServer.Parametros_LoadData()
        If Not data.Tables(0).Rows.Count = 0 Then
            row = data.Tables(0).Rows(0)

            m_iQAProxExpirar = DBNullToSomething(row.Item("FSQA_PROX_EXPIRAR"))
            m_bPotAValAdjudicacion = SQLBinaryToBoolean(row.Item("POT_A_VAL_ADJUDICACION"))
            m_bPotAValPedDirGS = SQLBinaryToBoolean(row.Item("POT_A_VAL_PEDDIR_GS"))
            m_bPotAValPedEP = SQLBinaryToBoolean(row.Item("POT_A_VAL_PED_EP"))
            m_bPotAValProvisionalSelProve = SQLBinaryToBoolean(row.Item("POT_A_VAL_PROVISIONAL_SEL_PROVE"))
            m_bNotifUsuAsigMatVal = SQLBinaryToBoolean(row.Item("NOTIF_USU_ASIG_MAT_VAL"))
            m_bNotifUsuAsigMatPot = SQLBinaryToBoolean(row.Item("NOTIF_USU_ASIG_MAT_POT"))
            m_bNotifUsuDesAsigMatVal = SQLBinaryToBoolean(row.Item("NOTIF_USU_DESASIG_MAT_VAL"))
            m_bNotifUsuDesAsigMatPot = SQLBinaryToBoolean(row.Item("NOTIF_USU_DESASIG_MAT_POT"))
            m_bNotifUsuAltaMatGS = SQLBinaryToBoolean(row.Item("NOTIF_USU_ALTA_MAT_GS"))
            m_sContactoAutom = "" & row.Item("CONTACCERTAUTOM")

            m_iQAPeriodoValidezCertificado = DBNullToSomething(row.Item("PERIODO_VALIDEZ_CERT_EXP"))
            m_bNotifProveCambioCalif = SQLBinaryToBoolean(row.Item("NOTIF_PROVE_CAMBIOS_CAL"))
            m_iTipoNotifProveCambioCalif = row.Item("TIPO_NOTIF_PROVE_CAMBIOS_CAL")

            m_bNotifProximFinResolucionNc = SQLBinaryToBoolean(row.Item("NOTIF_NC_FECFINRESOLUCION"))
            m_bNotifProximFinAccionesNc = SQLBinaryToBoolean(row.Item("NOTIF_NC_FECFINACCIONES"))
            m_iQAPeriodoNotificacionNc = DBNullToSomething(row.Item("PERIODO_NOTIF_NC"))

            m_sQANivelesDefectoUNQAProveedor = DBNullToSomething(row.Item("NIVELES_DEFECTO_UNQA"))
            m_iMaximoNivelUNQA = DBNullToSomething(row.Item("MAXIMONIVELUNQA"))
            Me._fitroMatQA = IIf(IsDBNull(row.Item("FILTRO_MAT")), 0, row.Item("FILTRO_MAT"))
        End If

        row = Nothing
        data = Nothing
    End Sub
    ''' <summary>
    ''' Carga los parametros de PM
    ''' </summary>
    ''' <remarks>Llamada desde: VisorContratos.aspx    </remarks>
    Public Function LoadPMData() As DataSet
        Authenticate()

        Dim data As DataSet
        data = DBServer.Parametros_GetData()

        Return data
    End Function
    ''' <summary>
    ''' Guarda los parametros de PARGEN_QA
    ''' </summary>
    ''' <remarks>Llamada desde: _common/parametrosGenerales.aspx/cmdAceptar_Click; Tiempo m�ximo:0,1</remarks>
    Public Sub GuardarOpciones()
        Authenticate()
        DBServer.Parametros_GuardarOpciones(m_iQAProxExpirar, m_bPotAValAdjudicacion, m_bPotAValPedDirGS, m_bPotAValPedEP, m_bPotAValProvisionalSelProve, m_bNotifUsuAsigMatVal, m_bNotifUsuAsigMatPot, m_bNotifUsuDesAsigMatVal,
                                            m_bNotifUsuDesAsigMatPot, m_bNotifUsuAltaMatGS, m_sContactoAutom, m_iQAPeriodoValidezCertificado, m_bNotifProveCambioCalif, m_bNotifProximFinResolucionNc, m_bNotifProximFinAccionesNc,
                                            m_iQAPeriodoNotificacionNc, m_sQANivelesDefectoUNQAProveedor, m_iTipoNotifProveCambioCalif, FiltroMaterialQA)
    End Sub
    ''' <summary>
    ''' Sirve para saber si el tipo de instalaci�n es de tipo pymes
    ''' </summary>
    ''' <returns>Si es de tipo pymes devuelve un listado de las pymes, si no devuelve una tabla vacia</returns>
    ''' <remarks>Llamada desde=QA_Puntuaciones.asmx\CalcularPuntuacionesInstalacion();</remarks>
    Public Function ObtenerTipoInstalacion() As DataSet
        Authenticate()
        Return DBServer.ObtenerTipoInstalacion()
    End Function
End Class
