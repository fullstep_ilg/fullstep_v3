<Serializable()> _
Public Class Almacenes
    Inherits Security

    Private Contexto As System.Web.HttpContext
    Private moAlmacenes As DataSet

    Public ReadOnly Property Data() As Data.DataSet
        Get
            Return moAlmacenes
        End Get
    End Property
    ''' <summary>
    ''' Devuelve los almacenes de un centro con sus datos, o uno en concreto
    ''' </summary>
    ''' <param name="sCodCentro">Codigo del centro del que se quieren devolver los almacenes</param>
    ''' <remarks>
    ''' Llamada desde: PMWeb/almacenesserver/Page_Load
    ''' Tiempo m�ximo: 0,3 seg</remarks>
    Public Sub LoadData(Optional ByVal sCodCentro As String = Nothing, Optional ByVal sCodDest As String = Nothing)
        Authenticate()

        Dim ds As DataSet
        Contexto = System.Web.HttpContext.Current
        ds = CType(Contexto.Cache("dsTodosAlmacenes_Centro_" & If(Not IsNothing(sCodCentro), "_" & sCodCentro, String.Empty) & "_Dest_" & If(Not IsNothing(sCodDest), "_" & sCodDest, String.Empty)), DataSet)
        If ds Is Nothing Then
            ds = DBServer.Almacenes_Load(sCodCentro, sCodDest)
            Contexto.Cache.Insert("dsTodosAlmacenes_Centro_" & If(Not IsNothing(sCodCentro), "_" & sCodCentro, String.Empty) & "_Dest_" & If(Not IsNothing(sCodDest), "_" & sCodDest, String.Empty), ds, Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration, New TimeSpan(0, System.Configuration.ConfigurationManager.AppSettings("TiempoExpiracionCacheMin"), 0))
        End If
        moAlmacenes = ds
    End Sub
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
End Class