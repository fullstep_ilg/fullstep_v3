<Serializable()> _
        Public Class ProveedoresFavoritos
    Inherits Security

    Private moData As DataSet

    Public ReadOnly Property Data() As Data.DataSet
        Get
            Return moData
        End Get

    End Property

    ''' <summary>
    ''' Procedimiento que carga los proveedores favoritos del usuario
    ''' </summary>
    ''' <param name="sUser">Codigo de usuario</param>
    ''' <param name="sOrgCompras">Codigo de la organizacion de compras</param>
    ''' <param name="sGMN1">Codigo de material de nivel 1</param>
    ''' <param name="sGMN2">Codigo de material de nivel 2</param>
    ''' <param name="sGMN3">Codigo de material de nivel 3</param>
    ''' <param name="sGMN4">Codigo de material de nivel 4</param>
    ''' <remarks>Llamada desde: PmWeb/proveedores/Page_Load - cmdBuscar_ServerClick, PmWeb/provfavServer/Page_Load, 
    ''' Tiempo m�ximo: 0,6 seg</remarks>
    Public Sub LoadData(ByVal sUser As String, Optional ByVal sOrgCompras As String = Nothing, Optional ByVal sGMN1 As String = Nothing, Optional ByVal sGMN2 As String = Nothing, Optional ByVal sGMN3 As String = Nothing, Optional ByVal sGMN4 As String = Nothing)
        Authenticate()
        moData = DBServer.Proveedores_FavoritosLoad(sUser, sOrgCompras, sGMN1, sGMN2, sGMN3, sGMN4)
    End Sub

    ''' <summary>
    ''' Procedimiento que inserta un nuevo pedido favorito para el usuario dado
    ''' </summary>
    ''' <param name="sUser">C�digo de usuario</param>
    ''' <param name="sProve">C�digo del proveedor</param>
    ''' <remarks>
    ''' Llamada desde: PmWeb/PRoveedoresFavoritos/PAge_load
    ''' Tiempo m�ximo: 0,5 seg</remarks>
    Public Sub Insertar(ByVal sUser As String, ByVal sProve As String)
        Authenticate()
        DBServer.ProveFavorito_Insertar(sUser, sProve)
    End Sub

    ''' <summary>
    ''' Procedimiento que elimina un proveedor favorito del usuario
    ''' </summary>
    ''' <param name="sUser">Codigo de usuario</param>
    ''' <param name="sProve">Codigo de proveedor</param>
    ''' <remarks>
    ''' Llamada desde: PmWeb/ProveFavoritoServer/PAge_load
    ''' Tiempo m�ximo: 0,3 seg</remarks>
    Public Sub Eliminar(ByVal sUser As String, ByVal sProve As String)
        Authenticate()
        DBServer.ProveFavorito_Eliminar(sUser, sProve)
    End Sub

    Public Sub New(ByRef dbserver As FSNDataBaseServer.Root,ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
    End Sub
End Class
