<Serializable()> _
    Public Class Destinos
    Inherits Security

    Private Contexto As System.Web.HttpContext
    Private moDestinos As DataSet

    Public ReadOnly Property Data() As Data.DataSet
        Get
            Return moDestinos
        End Get
    End Property

    ''' <summary>
    ''' Procedimiento que carga los datos del destino pasando su codigo
    ''' </summary>
    ''' <param name="sIdi">Idioma de la aplicaci�n</param>
    ''' <param name="sPer">C�digo de persona del usuario que utiliza la aplicaci�n</param>
    ''' <param name="sCod">C�digo del destino</param>
    ''' <remarks>Llamada desde: PmWeb/destinosserver/PAgE_Load , PmWeb/campos/PAgE_Load, PmWeb/desglose/PAgE_Load y CargarValoresDefecto , 
    ''' Tiempo m�ximo: 0,23 seg </remarks>
    Public Sub LoadData(ByVal sIdi As String, ByVal sPer As String, Optional ByVal sCod As String = Nothing)
        Authenticate()

        Dim ds As DataSet
        If sCod Is Nothing Then
            Contexto = System.Web.HttpContext.Current
            ds = CType(Contexto.Cache("dsTodosDestinos" & sIdi & "_" & sPer), DataSet)
            If ds Is Nothing Then
                ds = DBServer.Destinos_Get(sIdi, sPer, sCod)
                Contexto.Cache.Insert("dsTodosDestinos" & sIdi & "_" & sPer, ds, Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration, New TimeSpan(0, System.Configuration.ConfigurationManager.AppSettings("TiempoExpiracionCacheMin"), 0))
            End If
        Else
            ds = DBServer.Destinos_Get(sIdi, sPer, sCod)
        End If
        moDestinos = ds
    End Sub


    Public Sub New(ByRef dbserver As FSNDataBaseServer.Root,ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
    End Sub


End Class
