﻿<Serializable()> _
Public Class EstadosFactura
    Inherits Security
    Private moEstados As DataSet

    Public ReadOnly Property Data() As Data.DataSet
        Get
            Return moEstados
        End Get
    End Property

    ''' <summary>
    ''' Procedimiento que carga los estados de las facturas según el idioma
    ''' </summary>
    ''' <param name="sIdi">Idioma del usuario</param>
    ''' <remarks></remarks>
    Public Sub LoadData(ByVal sIdi As String)
        Authenticate()
        moEstados = DBServer.EstadosFactura_Get(sIdi)
    End Sub

    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub

End Class
