<Serializable()> _
    Public Class Campo
    Inherits Security
#Region "Properties"
    Private mlId As Integer
    Private mlIdGrupo As Integer
    Private mlIdCopiaCampo As Integer
    Private moDen As MultiIdioma
    Private moAyuda As MultiIdioma
    Private miTipo As Integer
    Private miSubtipo As Integer
    Private miIntro As Integer
    Private mdValorNum As Double
    Private msValorText As String
    Private mdtValorDate As Date
    Private mbValorBool As Boolean
    Private mdMinNum As Double
    Private mdMaxNum As Double
    Private mdtMinDate As Date
    Private mdtMaxDate As Date
    Private mlIdAtribGS As Long
    Private mlIdTipoCampoGS As TiposDeDatos.TipoCampoGS
    Private miOrden As Integer
    Private mlIdSolicitud As Long
    Private mlIdInstancia As Long
    Private msGMN1 As String
    Private msGMN2 As String
    Private msGMN3 As String
    Private msGMN4 As String
    Private msFormula As String
    Private mlOrigenCalculo As Integer
    Private mlCampoPadre As Integer
    Private mbValidacion_ERP As Boolean
    Private mbVinculado As Boolean
    Private mbPermiteExcel As Boolean
    Private msListaVinculados As String
    Private mbMoverLinea As Boolean
    Private mbRecienPublicado As Boolean
    Property CampoPadre() As Integer
        Get
            Return mlCampoPadre
        End Get
        Set(ByVal Value As Integer)
            mlCampoPadre = Value
        End Set
    End Property
    Property OrigenCalculo() As Integer
        Get
            Return mlOrigenCalculo
        End Get
        Set(ByVal Value As Integer)
            mlOrigenCalculo = Value
        End Set
    End Property
    Private miLineasPreconf As Integer
    Private moData As DataSet
    Private moDenSolicitud As MultiIdioma
    Private moDenGrupo As MultiIdioma
    Property IdInstancia() As Long
        Get
            Return mlIdInstancia
        End Get
        Set(ByVal Value As Long)
            mlIdInstancia = Value
        End Set
    End Property
    Property IdSolicitud() As Long
        Get
            Return mlIdSolicitud
        End Get
        Set(ByVal Value As Long)
            mlIdSolicitud = Value
        End Set
    End Property
    Property Id() As Integer
        Get
            Id = mlId
        End Get
        Set(ByVal Value As Integer)
            mlId = Value
        End Set
    End Property
    Property IdCopiaCampo() As Integer
        Get
            IdCopiaCampo = mlIdCopiaCampo
        End Get
        Set(ByVal Value As Integer)
            mlIdCopiaCampo = Value
        End Set
    End Property
    Property Den(ByVal Idioma As String) As String
        Get
            Den = moDen(Idioma)

        End Get
        Set(ByVal Value As String)
            If Not moDen.Contains(Idioma) Then
                moDen.Add(Idioma, Value)
            Else
                moDen(Idioma) = Value
            End If

        End Set
    End Property
    Property Ayuda(ByVal Idioma As String) As String
        Get
            Ayuda = moAyuda(Idioma)

        End Get
        Set(ByVal Value As String)
            If Not moAyuda.Contains(Idioma) Then
                moAyuda.Add(Idioma, Value)
            Else
                moAyuda(Idioma) = Value
            End If

        End Set
    End Property
    Property Tipo() As Integer
        Get
            Tipo = miTipo
        End Get
        Set(ByVal Value As Integer)
            miTipo = Value
        End Set
    End Property
    Property Subtipo() As Integer
        Get
            Subtipo = miSubtipo
        End Get
        Set(ByVal Value As Integer)
            miSubtipo = Value
        End Set
    End Property
    Property Intro() As Integer
        Get
            Intro = miIntro
        End Get
        Set(ByVal Value As Integer)
            miIntro = Value
        End Set
    End Property
    Property ValorNum() As Double
        Get
            ValorNum = mdValorNum
        End Get
        Set(ByVal Value As Double)
            mdValorNum = Value
        End Set
    End Property
    Property ValorText() As String
        Get
            ValorText = msValorText
        End Get
        Set(ByVal Value As String)
            msValorText = Value
        End Set
    End Property
    Property ValorDate()
        Get
            ValorDate = mdtValorDate
        End Get
        Set(ByVal Value)
            mdtValorDate = Value
        End Set
    End Property
    Property ValorBool() As Boolean
        Get
            ValorBool = mbValorBool
        End Get
        Set(ByVal Value As Boolean)
            mbValorBool = Value
        End Set
    End Property
    Property MinNum() As Double
        Get
            MinNum = mdMinNum
        End Get
        Set(ByVal Value As Double)
            mdMinNum = Value
        End Set
    End Property
    Property MaxNum() As Double
        Get
            MaxNum = mdMaxNum
        End Get
        Set(ByVal Value As Double)
            mdMaxNum = Value
        End Set
    End Property
    Property MinDate() As Date
        Get
            MinDate = mdtMinDate
        End Get
        Set(ByVal Value As Date)
            mdtMinDate = Value
        End Set
    End Property
    Property MaxDate() As Date
        Get
            MaxDate = mdtMaxDate
        End Get
        Set(ByVal Value As Date)
            mdtMaxDate = Value
        End Set
    End Property
    Property IdAtribGS() As Long
        Get
            IdAtribGS = mlIdAtribGS
        End Get
        Set(ByVal Value As Long)
            mlIdAtribGS = Value
        End Set
    End Property
    Property IdTipoCampoGS() As TiposDeDatos.TipoCampoGS
        Get
            IdTipoCampoGS = mlIdTipoCampoGS
        End Get
        Set(ByVal Value As TiposDeDatos.TipoCampoGS)
            mlIdTipoCampoGS = Value
        End Set
    End Property
    Property Orden() As Integer
        Get
            Orden = miOrden
        End Get
        Set(ByVal Value As Integer)
            miOrden = Value
        End Set
    End Property
    Property DenGrupo(ByVal Idioma As String) As String
        Get
            DenGrupo = moDenGrupo(Idioma)

        End Get
        Set(ByVal Value As String)
            If Not moDenGrupo.Contains(Idioma) Then
                moDenGrupo.Add(Idioma, Value)
            Else
                moDenGrupo(Idioma) = Value
            End If


        End Set
    End Property
    Property DenSolicitud(ByVal Idioma As String) As String
        Get
            DenSolicitud = moDenSolicitud(Idioma)

        End Get
        Set(ByVal Value As String)
            If Not moDenSolicitud.Contains(Idioma) Then
                moDenSolicitud.Add(Idioma, Value)
            Else
                moDenSolicitud(Idioma) = Value
            End If


        End Set
    End Property
    Property Data() As DataSet
        Get
            Return moData
        End Get
        Set(ByVal Value As DataSet)
            moData = Value
        End Set
    End Property
    Property LineasPreconf() As Integer
        Get
            Return miLineasPreconf
        End Get
        Set(ByVal Value As Integer)
            miLineasPreconf = Value
        End Set
    End Property
    Property GMN1() As String
        Get
            Return msGMN1
        End Get
        Set(ByVal Value As String)
            msGMN1 = Value
        End Set
    End Property
    Property GMN2() As String
        Get
            Return msGMN2
        End Get
        Set(ByVal Value As String)
            msGMN2 = Value
        End Set
    End Property
    Property GMN3() As String
        Get
            Return msGMN3
        End Get
        Set(ByVal Value As String)
            msGMN3 = Value
        End Set
    End Property
    Property GMN4() As String
        Get
            Return msGMN4
        End Get
        Set(ByVal Value As String)
            msGMN4 = Value
        End Set
    End Property
    Property Formula() As String
        Get
            Return msFormula
        End Get
        Set(ByVal Value As String)
            msFormula = Value
        End Set
    End Property
    Property IdGrupo() As Integer
        Get
            Return mlIdGrupo
        End Get
        Set(ByVal Value As Integer)
            mlIdGrupo = Value
        End Set
    End Property
    Property Validacion_ERP() As Integer
        Get
            Return mbValidacion_ERP
        End Get
        Set(ByVal Value As Integer)
            mbValidacion_ERP = Value
        End Set
    End Property
    ''' <summary>
    ''' Indica si un campo desglose ha sido vinculado o no
    ''' </summary>
    ''' <remarks>Llamad desde: alta\desglose.ascx; Tiempo maximo:0</remarks>
    Property Vinculado() As Integer
        Get
            Return mbVinculado
        End Get
        Set(ByVal Value As Integer)
            mbVinculado = Value
        End Set
    End Property

    ''' <summary>
    ''' Indica si un campo desglose permite a�adir lineas a partir de un excel
    ''' </summary>
    ''' <remarks>Llamad desde: alta\desglose.ascx; Tiempo maximo:0</remarks>
    Property PermiteExcel() As Boolean
        Get
            Return mbPermiteExcel
        End Get
        Set(ByVal Value As Boolean)
            mbPermiteExcel = Value
        End Set
    End Property

    ''' <summary>
    ''' Indica si las lineas de un campo desglose pueden ser movidas entre solicitudes o no
    ''' </summary>
    ''' <remarks>Llamad desde: alta\desglose.ascx; Tiempo maximo:0</remarks>
    Property MoverLinea() As Integer
        Get
            Return mbMoverLinea
        End Get
        Set(ByVal Value As Integer)
            mbMoverLinea = Value
        End Set
    End Property
    ''' <summary>
    ''' Indica si un campo desglose de un certificado ha sido publicado en la version mlVersionCert y no estaba publicado en la version de pantalla detalleCertificados
    ''' </summary>
    ''' <remarks>Llamad desde: certificados\detalleCertificados.ascx; Tiempo maximo:0</remarks>
    Property RecienPublicado() As Boolean
        Get
            Return mbRecienPublicado
        End Get
        Set(ByVal value As Boolean)
            mbRecienPublicado = value
        End Set
    End Property
    Private _tipoSolicitud As Integer
    Public Property TipoSolicitud() As Integer
        Get
            Return _tipoSolicitud
        End Get
        Set(ByVal value As Integer)
            _tipoSolicitud = value
        End Set
    End Property
    Private _monedaSolicitud As String
    Public Property MonedaSolicitud() As String
        Get
            Return _monedaSolicitud
        End Get
        Set(ByVal value As String)
            _monedaSolicitud = value
        End Set
    End Property
#End Region
    ''' <summary>
    ''' Funcion que carga los campos de una solicitud en el idioma especificado
    ''' </summary>
    ''' <param name="sIdi">Idioma a devolver</param>
    ''' <param name="lSolicitud">Identificador de la solicitud</param>
    ''' <remarks>
    ''' Llamada desde: PmWeb/arachedfiles/PAge_load ,PmWeb/preASig/Page_Load ,PmWeb/presupuestos/Page_load ,PmWeb/presupuestosreadonly/Page_load ,PmWeb/alta/GuardarSolicitud y recorrerDesglose ,PmWeb/ayudacampo/Page_load ,PmWeb/campos/Page_load ,PmWeb/desglose/Page_Load 
    ''' Tiempo m�ximo: 0,6 seg</remarks>
    Public Sub Load(ByVal sIdi As String, ByVal lSolicitud As Long)
        Authenticate()
        Dim data As DataSet
        Dim oRow As DataRow

        data = DBServer.Campo_Load(mlId, sIdi, lSolicitud)

        oRow = data.Tables(0).Rows(0)
        mlId = oRow.Item("ID")

        moDen(sIdi) = oRow.Item("DEN_" & sIdi).ToString
        moAyuda(sIdi) = oRow.Item("AYUDA_" & sIdi).ToString
        moDenGrupo(sIdi) = oRow.Item("DENGRUPO_" & sIdi).ToString
        moDenSolicitud(sIdi) = oRow.Item("DENSOL_" & sIdi).ToString

        mlIdSolicitud = oRow.Item("SOLICITUD")
        miTipo = oRow.Item("TIPO")
        miSubtipo = oRow.Item("SUBTIPO")
        miIntro = oRow.Item("INTRO")
        mdValorNum = DBNullToSomething(oRow.Item("VALOR_NUM"))
        mdtValorDate = DBNullToSomething(oRow.Item("VALOR_FEC"))
        mbValorBool = (DBNullToSomething(oRow.Item("VALOR_BOOL")) = 1)
        msValorText = DBNullToSomething(oRow.Item("VALOR_TEXT"))
        mdMinNum = DBNullToSomething(oRow.Item("MINNUM"))
        mdMaxNum = DBNullToSomething(oRow.Item("MAXNUM"))
        mdtMinDate = DBNullToSomething(oRow.Item("MINFEC"))
        mdtMaxDate = DBNullToSomething(oRow.Item("MAXFEC"))
        miOrden = oRow.Item("ORDEN")
        mlIdAtribGS = DBNullToSomething(oRow.Item("ID_ATRIB_GS"))
        mlIdTipoCampoGS = DBNullToSomething(oRow.Item("TIPO_CAMPO_GS"))
        miLineasPreconf = DBNullToSomething(oRow.Item("LINEAS_PRECONF"))
        msGMN1 = DBNullToSomething(oRow.Item("GMN1"))
        msGMN2 = DBNullToSomething(oRow.Item("GMN2"))
        msGMN3 = DBNullToSomething(oRow.Item("GMN3"))
        msGMN4 = DBNullToSomething(oRow.Item("GMN4"))
        msFormula = DBNullToSomething(oRow.Item("FORMULA"))
        mlOrigenCalculo = DBNullToSomething(oRow.Item("ORIGEN_CALC_DESGLOSE"))
        mlCampoPadre = DBNullToSomething(oRow.Item("CAMPO_PADRE"))
        mbValidacion_ERP = DBNullToSomething(oRow.Item("VALIDACION_ERP"))
        mbVinculado = DBNullToSomething(oRow.Item("VINCULADO"))
        mbPermiteExcel = DBNullToSomething(oRow.Item("PERMITIR_EXCEL"))

        msListaVinculados = ""
        If mbVinculado Then
            For Each RowCampo As DataRow In data.Tables(2).Rows
                msListaVinculados = msListaVinculados & "#" & DBNullToSomething(RowCampo.Item("CAMPO_HIJO_VINCULADO")) & "@"
                msListaVinculados = msListaVinculados & DBNullToSomething(RowCampo.Item("CAMPO_HIJO_VINCULADO")) & "#"
            Next
        End If

        mbMoverLinea = False
        data = Nothing
    End Sub
    ''' <summary>
    ''' Cargar todo la informaci�n de un campo desglose. NO carga informaci�n de lineas, solo del campo padre, ese es el desglose.
    ''' </summary>
    ''' <param name="lInstancia">Instancia</param>
    ''' <param name="sIdi">Idioma</param>      
    ''' <param name="sPer">Cod de la persona</param>    
    ''' <remarks>Llamada desde:_common\atachedfiles.ascx    _common\guardarinstancia.aspx
    ''' _common\presAsig.aspx    _common\presupuestos      _common\presupuestosreadonly.aspx
    ''' alta\alta.vb       alta\ayudacampo.aspx     alta\campos.ascx      alta\desglose.ascx
    ''' certificados\desglose.aspx      noconformidad\desglose.aspx    seguimiento\desglose.aspx
    ''' workflow\desglose.aspx; Tiempo m�ximo:0,1</remarks>
    Public Sub LoadInst(ByVal lInstancia As Long, Optional ByVal sIdi As String = Nothing, Optional ByVal sPer As String = Nothing)
        Authenticate()
        Dim data As DataSet
        Dim oRow As DataRow
        Dim oIdiomas As New Idiomas(DBServer, mIsAuthenticated)
        Dim oIdi As Idioma

        data = DBServer.CampoInst_Load(lInstancia, mlId, sIdi, sPer)

        oRow = data.Tables(0).Rows(0)
        mlId = oRow.Item("ID")
        If sIdi = Nothing Then
            oIdiomas.Load()
        Else
            oIdiomas.Add(sIdi, Nothing)
        End If

        For Each oIdi In oIdiomas.Idiomas
            moDen(oIdi.Cod) = oRow.Item("DEN_" & oIdi.Cod).ToString
            moAyuda(oIdi.Cod) = oRow.Item("AYUDA_" & oIdi.Cod).ToString
            moDenGrupo(oIdi.Cod) = oRow.Item("DENGRUPO_" & oIdi.Cod).ToString
            moDenSolicitud(oIdi.Cod) = oRow.Item("DENSOL_" & oIdi.Cod).ToString
        Next
        mlIdSolicitud = oRow.Item("SOLICITUD")
        _tipoSolicitud = oRow.Item("TIPO_SOLICITUD")
        _monedaSolicitud = oRow.Item("MON")
        mlIdInstancia = oRow.Item("INSTANCIA")
        mlIdGrupo = DBNullToSomething(oRow.Item("GRUPO"))
        miTipo = oRow.Item("TIPO")
        miSubtipo = oRow.Item("SUBTIPO")
        miIntro = oRow.Item("INTRO")
        mdValorNum = DBNullToSomething(oRow.Item("VALOR_NUM"))
        mdtValorDate = DBNullToSomething(oRow.Item("VALOR_FEC"))
        mbValorBool = (DBNullToSomething(oRow.Item("VALOR_BOOL")) = 1)
        msValorText = DBNullToSomething(oRow.Item("VALOR_TEXT"))
        mdMinNum = DBNullToSomething(oRow.Item("MINNUM"))
        mdMaxNum = DBNullToSomething(oRow.Item("MAXNUM"))
        mdtMinDate = DBNullToSomething(oRow.Item("MINFEC"))
        mdtMaxDate = DBNullToSomething(oRow.Item("MAXFEC"))
        miOrden = oRow.Item("ORDEN")
        mlIdAtribGS = DBNullToSomething(oRow.Item("ID_ATRIB_GS"))
        mlIdTipoCampoGS = DBNullToSomething(oRow.Item("TIPO_CAMPO_GS"))
        miLineasPreconf = DBNullToSomething(oRow.Item("LINEAS_PRECONF"))
        msGMN1 = DBNullToSomething(oRow.Item("GMN1"))
        msGMN2 = DBNullToSomething(oRow.Item("GMN2"))
        msGMN3 = DBNullToSomething(oRow.Item("GMN3"))
        msGMN4 = DBNullToSomething(oRow.Item("GMN4"))
        msFormula = DBNullToSomething(oRow.Item("FORMULA"))
        mlOrigenCalculo = DBNullToSomething(oRow.Item("ORIGEN_CALC_DESGLOSE"))
        mlCampoPadre = DBNullToSomething(oRow.Item("CAMPO_PADRE"))
        mlIdCopiaCampo = DBNullToSomething(oRow.Item("COPIA_CAMPO_DEF"))
        'ISG
        mbValidacion_ERP = DBNullToSomething(oRow.Item("VALIDACION_ERP"))
        mbVinculado = DBNullToSomething(oRow.Item("VINCULADO"))
        mbPermiteExcel = DBNullToSomething(oRow.Item("PERMITIR_EXCEL"))

        msListaVinculados = ""

        mbMoverLinea = False
        If Not data.Tables(2) Is Nothing Then
            If data.Tables(2).Rows.Count > 0 Then
                mbMoverLinea = DBNullToSomething(data.Tables(2).Rows(0).Item("MOVER_LINEAS"))
            End If
        End If


        If mbVinculado Then
            For Each RowCampo As DataRow In data.Tables(3).Rows
                msListaVinculados = msListaVinculados & "#" & DBNullToSomething(RowCampo.Item("CAMPO_HIJO_A_VINCULAR")) & "@"
                msListaVinculados = msListaVinculados & DBNullToSomething(RowCampo.Item("CAMPO_HIJO_VINCULADO")) & "#"
            Next
        End If
        '
        data = Nothing
        oIdiomas = Nothing
    End Sub
    ''' <summary>
    ''' Carga los datos de un desglose
    ''' </summary>
    ''' <param name="sIdi">Idioma del usuario</param>
    ''' <param name="lSolicitud">Id de la solicitud</param>
    ''' <param name="sPer">Cod de la persona</param>
    ''' <param name="bFavorito">Si viene de una solicitud favorita o no</param>
    ''' <param name="lFavorito">El id de la solicitud favorita</param>
    ''' <param name="sFormatoFecha">Formato de la fecha</param>
    ''' <param name="bSacarNumLinea">Si se saca NumLinea en el desglose o no . Tarea 3369</param>
    ''' <returns>Los datos del desglose</returns>
    ''' <remarks>Llamada desde: PMWeb/Campos.ascx, desglose.ascx, alta.vb Tiempo m�ximo: 0,1 sg</remarks>
    Public Function LoadDesglose(Optional ByVal sIdi As String = Nothing, Optional ByVal lSolicitud As Long = Nothing, Optional ByVal sPer As String = Nothing, Optional ByVal bFavorito As Boolean = False, Optional ByVal lFavorito As Long = 0, Optional ByVal sFormatoFecha As String = Nothing, Optional ByVal bSacarNumLinea As Boolean = False, Optional InstanciaImportar As Long = 0) As DataSet
        Authenticate()
        moData = DBServer.Campo_LoadDesglose(mlId, sIdi, lSolicitud, sPer, bFavorito, lFavorito, sFormatoFecha, mbVinculado, bSacarNumLinea, InstanciaImportar)
        Return moData
    End Function
    ''' <summary>
    ''' Funcion que devuelve el desglose de un campo de una instancia
    ''' </summary>
    ''' <param name="sIdi">Idioma de la aplicacion</param>
    ''' <param name="bDefecto">Booleano que indica si es la instancia por defecto</param>
    ''' <param name="bNuevoWorkflow">Booleano que indica si es un nuevo proceso</param>
    ''' <param name="bObservador">Booleano que indica si hay observador</param>
    ''' <param name="lInstancia">Id de la instancia</param>
    ''' <param name="lVersion">Version del Desglose</param>
    ''' <param name="sProve">Proveedor</param>
    ''' <param name="sUsu">Usuario</param>
    ''' <param name="lVersionBd">Version de la instancia en bbdd. Los certificados, una vez q hay instancia, para determinar si los 
    ''' campos del desglose son visible/editables usan la version de la tabla certificado.
    ''' En caso de desgloses No pop siempre llega a cero, pero esta bien pq @VERSION contendra la version de bbdd y se asigna @VERSION_BD_CERT=@VERSION</param> 
    ''' <param name="sFormatoFecha">Formato fecha usuario</param>
    ''' <param name="lBloque">Id del bloque actual</param>
    ''' <param name="lRol">Id del rol actual</param>
    ''' <param name="bSacarNumLinea">Si se saca NumLinea en el desglose o no . Tarea 3369</param> 
    ''' <returns>Un dataset con los datos de desglose de instancia</returns>
    ''' <remarks>
    ''' Llamada desde: PMWEB/Alta/GuardarSolicitud y RecorrerDesglose, PMWEB/Campos/Page_Load, PMWEB/Desglose/Page_Load y CargarValoresDefecto, PMWEB/detalleNoConformidad/CargarEstadosNoConformidad,PMWEB/impexp/Page_Load,PMWEB/detallleSolicConsulta/CargarDesglose
    ''' Tiempo m�ximo: 0,5 seg </remarks>
    Public Function LoadInstDesglose(Optional ByVal sIdi As String = Nothing, Optional ByVal lInstancia As Long = Nothing, Optional ByVal sUsu As String = Nothing, _
                                     Optional ByVal lVersion As Long = Nothing, Optional ByVal sProve As String = Nothing, Optional ByVal bNuevoWorkflow As Boolean = False, _
                                     Optional ByVal bObservador As Boolean = False, Optional ByVal bDefecto As Boolean = False, Optional ByVal lVersionBd As Long = Nothing, _
                                     Optional ByVal sFormatoFecha As String = "", Optional ByVal lBloque As Long = 0, Optional ByVal lRol As Long = 0, _
                                     Optional ByVal bSacarNumLinea As Boolean = False) As DataSet
        Authenticate()

        mbRecienPublicado = False
        moData = DBServer.CampoInst_LoadDesglose(mlId, mlIdCopiaCampo, sIdi, lInstancia, sUsu, lVersion, sProve, bNuevoWorkflow, bObservador, _
                                                 bDefecto, lVersionBd, sFormatoFecha, mbVinculado, mbRecienPublicado, lBloque, lRol, bSacarNumLinea)
        Return moData
    End Function
    ''' <summary>
    ''' Funcion que carga los campos calculados
    ''' </summary>
    ''' <param name="lSolicitud">Id de la solicitud</param>
    ''' <param name="sPer">C�digo de la persona</param>
    ''' <returns>Los campos calculados de la solicitud</returns>
    ''' <remarks>
    ''' Llamada desdE: PmWeb/GuardarInstancia/GuardarConWorkFlow y GuardarSinWorkFlow y GenerarDataSetYHacerComprobaciones, PmWeb/Alta/GuardarInstancia, PmWeb/AyudaCampo/Page_Load, PmWeb/RecalcularImportes/Page_Load 
    ''' Tiempo m�ximo: 0,4 seg</remarks>
    Public Function LoadCalculados(ByVal lSolicitud As Long, Optional ByVal sPer As String = Nothing) As DataSet
        Authenticate()
        moData = DBServer.Campo_LoadCamposCalculados(mlId, lSolicitud, sPer)
        Return moData
    End Function
    ''' <summary>
    ''' Funcion que carga los campos calculados de la instancia
    ''' </summary>
    ''' <param name="lInstancia">Id de la instancia</param>
    ''' <param name="sUsu">C�digo de usuario</param>
    ''' <returns>Los campos calculados de la instancia</returns>
    ''' <remarks>
    ''' Llamada desdE: PmWeb/GuardarInstancia/GuardarConWorkFlow y GuardarSinWorkFlow y GenerarDataSetYHacerComprobaciones, PmWeb/Alta/GuardarInstancia, PmWeb/AyudaCampo/Page_Load, PmWeb/RecalcularImportes/Page_Load 
    ''' Tiempo m�ximo: 0,4 seg</remarks>
    Public Function LoadInstCalculados(ByVal lInstancia As Long, ByVal sUsu As String) As DataSet
        Authenticate()
        moData = DBServer.Campo_LoadInstCamposCalculados(mlId, lInstancia, sUsu)
        Return moData
    End Function
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
        moDen = New MultiIdioma
        moAyuda = New MultiIdioma
        moDenSolicitud = New MultiIdioma
        moDenGrupo = New MultiIdioma
    End Sub
    ''' <summary>
    ''' Devuelve la descripci�n en el idioma que se le pasa como par�metro
    ''' </summary>
    ''' <param name="Text_Den">Texto con las denominaciones en todos los idiomas de la aplicaci�n</param>
    ''' <param name="Idioma">Cod del idioma</param>
    ''' <param name="SepTextos">Caracter separador de los textos</param>
    ''' <param name="SepIdioma">Caracter separador de los idiomas</param>
    ''' <returns>la descripci�n en el idioma correspondiente</returns>
    ''' <remarks>Llamada desde: un mont�n de sitios; Tiempo m�ximo: 0sg.</remarks>
    Public Shared Function ExtraerDescrIdioma(ByVal Text_Den As String, ByVal Idioma As String, Optional ByVal SepTextos As Char = "#", Optional ByVal SepIdioma As Char = "|") As String

        If Not Text_Den.Contains(Idioma & SepIdioma) Then
            Return Text_Den
        Else
            If Text_Den.EndsWith(SepTextos) Then Text_Den = Text_Den.Substring(0, Text_Den.Length - 1)
            Dim arrTextos() As String = Text_Den.Split(SepTextos)
            If arrTextos.Length > 1 Then
                For Each sTexto As String In arrTextos
                    If Left(sTexto, sTexto.IndexOf(SepIdioma)) = Idioma Then Return Right(sTexto, sTexto.Length - sTexto.IndexOf(SepIdioma) - 1)
                Next
            Else
                If Text_Den.IndexOf(SepIdioma) > 0 AndAlso Left(Text_Den, Text_Den.IndexOf(SepIdioma)) = Idioma Then
                    Return Right(Text_Den, Text_Den.Length - Text_Den.IndexOf(SepIdioma) - 1)
                Else
                    Return Text_Den
                End If
            End If
            Return String.Empty
        End If
    End Function
    ''' <summary>
    ''' Devuelve los valores que se han ido autocompletando en un campo
    ''' </summary>
    ''' <param name="idCampo">Campo del que se retornan los valores</param>
    ''' <param name="Per">Usuario para el que se retornas los campos</param>
    ''' <returns>valores que se han ido autocompletando en un campo</returns>
    ''' <remarks></remarks>
    Public Function DevolverValoresCampo(ByVal idCampo As Integer, ByVal Per As String) As DataSet
        Authenticate()
        Return DBServer.Campo_Load_ValoresCampo(idCampo, Per)
    End Function
    ''' <summary>
    ''' Elimina el valor del campo
    ''' </summary>
    ''' <param name="id">id del valor a eliminar</param>
    ''' <param name="idCampo">id del campo</param>
    ''' <param name="Per">Usuario</param>
    ''' <remarks></remarks>
    Public Sub EliminarValoresCampo(ByVal id As Integer, ByVal idCampo As Integer, ByVal Per As String)
        Authenticate()
        DBServer.Campo_Eliminar_ValoresCampo(id, idCampo, Per)
    End Sub
    Public Function DevolverLista(ByVal lIdCampo As Long, ByVal iOrdenCampoPadre As Integer, ByVal sIdi As String, ByVal lInstancia As Long, ByVal lBloque As Long, ByVal lSolicitud As Long, ByVal sCodPer As String, ByVal sFormatoFecha As String, ByVal bValorNum As Boolean) As DataSet
        Authenticate()
        Return DBServer.Campo_DevolverLista(lIdCampo, iOrdenCampoPadre, sIdi, lInstancia, lBloque, lSolicitud, sCodPer, sFormatoFecha, bValorNum)
    End Function
    ''' <summary>Devuelve los valores de una lista cuyo origen es un campo de tipo material</summary>
    ''' <param name="lIdCampo">Id del campo</param>
    ''' <param name="sGMN1">GMN1</param>
    ''' <param name="sGMN2">GMN2</param>
    ''' <param name="sGMN3">GMN3</param>
    ''' <param name="sGMN4">GMN4</param>
    ''' <returns>Dataset con los valores de la lista</returns>
    ''' <remarks>Llamada desde: ConsultasPMWEB.asmx</remarks>
    Public Function DevolverListaCampoPadreMaterial(ByVal lIdCampo As Long, ByVal sGMN1 As String, ByVal sGMN2 As String, ByVal sGMN3 As String, ByVal sGMN4 As String, ByVal sIdi As String) As DataSet
        Authenticate()
        Return DBServer.Campo_DevolverListaCampoPadreMaterial(lIdCampo, sGMN1, sGMN2, sGMN3, sGMN4, sIdi)
    End Function
    ''' <summary>
    ''' Para la configuraci�n de los desgloses vinculados, si un campo tambien ha sido vinculado, el campo no sera editable. 
    ''' </summary>
    ''' <param name="lIdCampo">campo</param>
    ''' <returns>si un campo tambien ha sido vinculado o no</returns>
    ''' <remarks>Llamada desde: Desglose.ascx; Tiempo maximo:0</remarks>
    Public Function EsteEsCampoVinculado(ByVal lIdCampo As Long) As Boolean
        Return (InStr(msListaVinculados, "#" & CStr(lIdCampo) & "@", CompareMethod.Text) > 0)
    End Function
    ''' <summary>
    ''' Para los desgloses vinculados, si un campo ha sido vinculado, aqui se indica quien es el origen (form_campo.id)
    ''' </summary>
    ''' <param name="lIdCampo">campo</param>
    ''' <returns>origen (form_campo.id) del campo vinculado</returns>
    ''' <remarks>Llamada desde: Desglose.ascx; Tiempo maximo:0</remarks>
    Public Function CampoVinculadoConQuien(ByVal lIdCampo As Long) As String
        Dim sLista As String = msListaVinculados
        Dim iPosIdCampo As Integer = InStr(sLista, "#" & CStr(lIdCampo) & "@", CompareMethod.Text)
        sLista = sLista.Substring(iPosIdCampo + Len("#" & CStr(lIdCampo) & "@") - 1)

        Return sLista.Substring(0, InStr(sLista, "#") - 1)
    End Function
    ''' <summary>
    ''' Cargar para un campo desglose si esta vinculado o no
    ''' </summary>
    ''' <remarks>Llamada desde:script/workflow/detalleSolicConsulta/CargarDesglose; Tiempo m�ximo:0,1</remarks>
    Public Sub LoadInstVinculado()
        Authenticate()
        Dim data As DataSet

        data = DBServer.CampoInst_LoadVinculado(mlId)

        mbVinculado = DBNullToSomething(data.Tables(0).Rows(0).Item("VINCULADO"))
    End Sub
    ''' <summary>
    ''' Comprueba si la asignaci�n del participante proveedor esta asociado a este campo del formulario
    ''' </summary>
    ''' <returns>True: es un campo asociado/False: es un campo proveedor normal</returns>
    ''' <remarks></remarks>
    Public Function ProveedorParticipanteAsociado() As Boolean
        Authenticate()
        Return DBServer.Campo_ProveedorParticipanteAsociado(mlIdInstancia, mlIdSolicitud, mlId)
    End Function
End Class
