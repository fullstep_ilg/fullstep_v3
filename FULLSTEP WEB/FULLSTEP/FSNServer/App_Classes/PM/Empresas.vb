﻿<Serializable()> _
Public Class Empresas
    Inherits Security
    Private Contexto As System.Web.HttpContext

    ''' <summary>
    ''' Procedimiento que carga los datos de las empresas
    ''' </summary>
    ''' <param name="sIdi">Idioma de la aplicación</param>
    ''' <param name="sNIF">NIF o parte del NIF que se pasa como criterio de búsqueda</param>
    ''' <param name="sDen">Nombre o parte del nombre de la empresa que se pasa como criterio de búsqueda</param>
    ''' <param name="sCP">CP o parte del CP que se pasa como criterio de búsqueda</param>
    ''' <param name="sDir">Dirección o parte de la dirección que se pasa como criterio de búsqueda</param>
    ''' <param name="sPob">Población o parte de la población que se pasa como criterio de búsqueda</param>
    ''' <param name="sPais">País o parte del país que se pasa como criterio de búsqueda</param>
    ''' <param name="sProvincia">Codigo de provincia que se pasa como criterio de busqueda</param>
    ''' <param name="lSolicitud">ID Solicitud</param>
    ''' <param name="lRol">ID Rol, para saber si tiene alguna empresa relacionada</param>
    ''' <param name="bCopia">Si hay que acceder a pm_copia_rol o no</param>
    ''' <param name="lContrato">Id del contrato</param>
    ''' <param name="iAnyo">Año del proceso</param>
    ''' <param name="sGMN1">GMN1 del proceso</param>
    ''' <param name="iProce">Id del proceso</param>
    ''' <remarks>Llamada desde: PMWeb2008/script/_common/BuscadorEmpresas.aspx, PMWeb/script/contratos/altacontrato.aspx, PMWeb/script/contratos/detallecontrato.aspx
    ''' Tiempo máximo: 0,2 seg </remarks>
    Public Function ObtenerEmpresas(ByVal sIdi As String, Optional ByVal sNIF As String = Nothing, Optional ByVal sDen As String = Nothing, Optional ByVal sCP As String = Nothing, Optional ByVal sDir As String = Nothing,
                                    Optional ByVal sPob As String = Nothing, Optional ByVal sPais As String = Nothing, Optional ByVal sProvincia As String = Nothing, Optional ByVal lSolicitud As Long = 0,
                                    Optional ByVal lRol As Long = 0, Optional ByVal bCopia As Boolean = False, Optional ByVal lContrato As Long = Nothing, Optional ByVal iAnyo As Integer = Nothing,
                                    Optional ByVal sGMN1 As String = Nothing, Optional ByVal iProce As Integer = Nothing, Optional ByVal lPyme As Long = Nothing,
                                    Optional ByVal lId As Long = Nothing, Optional ByVal UsuCod As String = Nothing, Optional ByVal AutoCompletar As String = Nothing,
                                    Optional ByVal RestriccionEmpresaUsuario As Boolean = False, Optional ByVal RestriccionEmpresaPerfilUsuario As Boolean = False) As DataTable
        Dim dtEmpresas As DataTable = DBServer.Empresas_Get(sIdi, sNIF, sDen, sCP, sDir, sPob, sPais, sProvincia, lSolicitud, lRol, bCopia, lContrato, iAnyo, sGMN1, iProce, lPyme,
                                                            lId, UsuCod, AutoCompletar, RestriccionEmpresaUsuario, RestriccionEmpresaPerfilUsuario)
        dtEmpresas.TableName = "EMPRESAS_USU"
        Return dtEmpresas
    End Function
    ''' <summary>
    ''' Procedimiento que carga todas las empresas. Si se trata de una pyme cargará solo las empresas de esa pyme.
    ''' </summary>
    ''' <param name="sIdi">Idioma del usuario</param>
    ''' <param name="lPyme">Id de la pyme. Si no, 0</param>
    ''' <returns>Devuelve el dataset con las empresas</returns>
    ''' <remarks>LLamada desde: /PMWeb2008/Autocomplete.asmx</remarks>
    Public Function CargarEmpresas(ByVal sIdi As String, Optional ByVal lPyme As Long = Nothing) As DataSet
        Dim ds As DataSet
        Authenticate()
        Contexto = System.Web.HttpContext.Current
        ds = CType(Contexto.Cache("dsTodasEmpresas" & sIdi & "_" & lPyme), DataSet)
        If ds Is Nothing Then
            ds = DBServer.Empresas_GetDataSet(sIdi, lPyme)
            Contexto.Cache.Insert("dsTodasEmpresas" & sIdi & "_" & lPyme, ds, Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration, New TimeSpan(0, System.Configuration.ConfigurationManager.AppSettings("TiempoExpiracionCacheMin"), 0))
        End If
        Return ds
    End Function
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
End Class
