<Serializable()>
Public Class Bloque
    Inherits Security

    Private mlId As Integer
    Private moRoles As Roles
    Property Id() As Integer
        Get
            Return mlId
        End Get
        Set(ByVal Value As Integer)
            mlId = Value
        End Set
    End Property
    Property Roles() As Roles
        Get
            Return moRoles
        End Get
        Set(ByVal Value As Roles)
            moRoles = Value
        End Set
    End Property
    ''' <summary>
    ''' Procedimiento que carga los datos de los roles relacionados con un bloque
    ''' </summary>
    ''' <param name="lId">Identificador del bloque</param>
    ''' <remarks>
    ''' Llamada desde: PmWeb/NWAlta/Page_load
    ''' Tiempo m�ximo: 0,2 seg</remarks>
    Public Sub CargarRolesBloque(ByVal lId As Integer, ByVal Per As String)
        Authenticate()

        Dim oData As DataSet
        Dim oRow As DataRow
        mlId = lId
        oData = DBServer.Roles_Load(lId, Per)
        If oData.Tables.Count > 0 AndAlso oData.Tables(0).Rows.Count > 0 Then
            moRoles = New Roles(DBServer, mIsAuthenticated)
            For Each oRow In oData.Tables(0).Rows
                moRoles.Add(oRow.Item("ROL"), oRow.Item("DEN"), oRow.Item("TIPO"), DBNullToSomething(oRow.Item("PER")), oRow.Item("BLOQUE"))
            Next
        End If
    End Sub
    ''' <summary>
    ''' Funcion que devuelve los datos de las etapas que hacen bloqueo en la salida
    ''' </summary>
    ''' <param name="sIdi">Idioma de la aplicaci�n</param>
    ''' <returns>Un DataSet con los datos de las etapas del bloqueo de la salida</returns>
    ''' <remarks>
    ''' Llamada desdE: PmWeb/NWGestionInstancia/PagE_load
    ''' Tiempo m�ximo: 0,2 seg</remarks>
    Public Function DevolverEtapasBloqueoSalida(ByVal sIdi As String) As DataSet
        Authenticate()

        Dim data As DataSet
        data = DBServer.Bloque_LoadEtapasBloqueoSalida(mlId, sIdi)

        Return data
    End Function
    ''' <summary>
    ''' Funcion que devuelve los datos de la persona inicial del traslado de un bloque
    ''' </summary>
    ''' <param name="lInstancia">Identificador de la instancia</param>
    ''' <param name="lBloque">Identificador del bloque</param>
    ''' <returns>Un DataSet con los datos de la persona que realizo el traslado inicial del bloque</returns>
    ''' <remarks>
    ''' Llamada desde: WebServicePm/Service/ServiceThread
    ''' Tiempo m�ximo: 0,3 seg</remarks>
    Public Function Devolver_Persona_Inicial_Traslado(ByVal lInstancia As Integer, ByVal lBloque As Integer) As DataSet
        Authenticate()

        Dim data As DataSet
        data = DBServer.Bloque_Devolver_Persona_Inicial_Traslado(lInstancia, lBloque)

        Return data
    End Function
    ''' <summary>
    ''' Obtiene el n�mero random del bloque pasado como par�metro para hacer la comparaci�n con el que llega por querystring
    ''' </summary>
    ''' <param name="lBloque">Id del bloque de la instancia</param>
    ''' <returns>El n�mero random obtenido de la consulta</returns>
    ''' <remarks>Llamada desde: Pmweb2008/AprobacionRechazo.asmx. Tiempo m�x: 0,1 sg.</remarks>
    Public Function ObtenerRandom(ByVal lInstancia As Long, ByVal lBloque As Long) As Integer
        Authenticate()
        Return DBServer.Bloque_ObtenerRandom(lInstancia, lBloque)
    End Function
    ''' <summary>
    ''' Graba la accion que se realiza desde el email
    ''' </summary>
    ''' <param name="idAccion">id de la accion</param>
    ''' <param name="iBloque">id del bloque</param>
    ''' <remarks></remarks>
    Public Sub GrabarAccionEmail(ByVal idAccion As Long, ByVal lInstancia As Long, ByVal iBloque As Long)
        Authenticate()
        DBServer.Notificador_GuardarNumeroRandom(lInstancia, iBloque, 0, idAccion)
    End Sub
    ''' <summary>
    ''' Funcion que retorna la fecha cuando ha pasado a ese bloque la instancia
    ''' </summary>
    ''' <param name="idBloque">id del bloque</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ObtenerFecha(ByVal idInstancia As Long, ByVal idBloque As Long) As Date
        Authenticate()
        Return DBServer.Bloque_ObtenerFecha(idInstancia, idBloque)
    End Function
    Public Function EsBloqueActual(ByVal idInstancia As Long, ByVal idBloque As Long) As Boolean
        Authenticate()
        Dim bloques As DataSet
        Dim oRow As DataRow
        Dim result As Boolean = False
        bloques = DBServer.Instancia_DevolverBloqueActual(idInstancia)
        If bloques.Tables.Count > 0 Then
            If bloques.Tables(0).Rows.Count > 0 Then
                'Comprobar si idBloque esta entre el/los bloques actuales
                For Each oRow In bloques.Tables(0).Rows
                    If idBloque = oRow.Item("BLOQUE") Then
                        result = True
                        Exit For
                    End If
                Next
            Else
                'Si guardo cuando estoy en alta devolver� la tabla vac�a porque a�n no hay registros en INSTANCIA_BLOQUE pero tengo que dejarle pasar
                result = True
            End If
        End If
        Return result
    End Function
    Public Sub New(ByRef dbserver As FSNDataBaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)

    End Sub
End Class