﻿<Serializable()> _
Public Class FiltroContrato
    Inherits Security

#Region " PROPIEDADES "

    Private mIDFiltro As Integer
    Private mIDFiltroUsuario As Integer
    Private mNombreFiltro As String
    Private mPersona As String
    Private mPorDefecto As Boolean
    Private mIdioma As String
    Private mCamposConfigurados As DataSet
    Private mOrdenacion As String
    Private mSentidoOrdenacion As String
    Private mBusqueda As DataTable
    Private mCamposGenerales As DataTable
    Private mCamposFormulario As DataSet
    Private mCamposOrdenadosParaVisor As DataTable
    Private mIsLoaded As Boolean
    Private msOrdenacion As String
    Private mlVigentes As Long
    Private mlExpirado As Long
    Private mlProximoAExpirar As Long
    Private mlGuardado As Long
    Private mlRechazados As Long
    Private mlAnulados As Long
    Private mlEnCursoAprobacion As Long
    Private mlTodas As Long
    Private mdsContratos As Data.DataSet

    Property IDFiltroUsuario() As Integer
        Get
            IDFiltroUsuario = mIDFiltroUsuario
        End Get
        Set(ByVal Value As Integer)
            mIDFiltroUsuario = Value
        End Set
    End Property
    Property IDFiltro() As Integer
        Get
            Return mIDFiltro
        End Get
        Set(ByVal Value As Integer)
            mIDFiltro = Value
        End Set
    End Property
    Property NombreFiltro() As String
        Get
            NombreFiltro = mNombreFiltro
        End Get
        Set(ByVal Value As String)
            mNombreFiltro = Value
        End Set
    End Property
    Property Persona() As String
        Get
            Persona = mPersona
        End Get
        Set(ByVal Value As String)
            mPersona = Value
        End Set
    End Property
    Property Ordenacion() As String
        Get
            Return mOrdenacion
        End Get
        Set(ByVal Value As String)
            mOrdenacion = Value
        End Set
    End Property
    Property SentidoOrdenacion() As String
        Get
            Return mSentidoOrdenacion
        End Get
        Set(ByVal Value As String)
            mSentidoOrdenacion = Value
        End Set
    End Property
    Property Idioma() As String
        Get
            Return mIdioma
        End Get
        Set(ByVal Value As String)
            mIdioma = Value
        End Set
    End Property
    'Dataset con 2 tablas
    '(0) Campos Generales y de formulario
    '(1) Campos Busqueda
    Property CamposConfigurados() As DataSet
        Get
            Return mCamposConfigurados
        End Get
        Set(ByVal Value As DataSet)
            mCamposConfigurados = Value
        End Set
    End Property
    Property PorDefecto() As Boolean
        Get
            Return mPorDefecto
        End Get
        Set(ByVal Value As Boolean)
            mPorDefecto = Value
        End Set
    End Property
    Property Busqueda() As DataTable
        Get
            Return mBusqueda
        End Get
        Set(ByVal value As DataTable)
            mBusqueda = value
        End Set
    End Property
    Property CamposGenerales() As DataTable
        Get
            Return mCamposGenerales
        End Get
        Set(ByVal value As DataTable)
            mCamposGenerales = value
        End Set
    End Property
    Property CamposFormulario() As DataSet
        Get
            Return mCamposFormulario
        End Get
        Set(ByVal value As DataSet)
            mCamposFormulario = value
        End Set
    End Property
    ''' <summary>
    ''' Da aceso a una tabla con los campos, todos generales y de formulario, ordenados por la posiciÃ³n en q se pondran en el grid. AsÃ­ se evitan problemas con el visibleindex en el VisorContratos.
    ''' </summary>
    ''' <remarks>Llamada desde: VisorContratos.aspx.vb; Tiempo maximo: 0</remarks>
    Property CamposOrdenadosParaVisor As DataTable
        Get
            Return mCamposOrdenadosParaVisor
        End Get
        Set(value As DataTable)
            mCamposOrdenadosParaVisor = value
        End Set
    End Property
    Property IsLoaded() As Boolean
        Get
            Return mIsLoaded
        End Get
        Set(ByVal value As Boolean)
            mIsLoaded = value
        End Set
    End Property
#Region " ESTADOS "
    ReadOnly Property Vigentes() As Long
        Get
            Vigentes = mlVigentes
        End Get
    End Property
    ReadOnly Property Expirados() As Long
        Get
            Expirados = mlExpirado
        End Get
    End Property
    ReadOnly Property ProximoAExpirar() As Long
        Get
            ProximoAExpirar = mlProximoAExpirar
        End Get
    End Property
    ReadOnly Property Guardados() As Long
        Get
            Guardados = mlGuardado
        End Get
    End Property
    ReadOnly Property Rechazados() As Long
        Get
            Rechazados = mlRechazados
        End Get
    End Property
    ReadOnly Property Anulados() As Long
        Get
            Anulados = mlAnulados
        End Get
    End Property
    ReadOnly Property EnCursoAprobacion() As Long
        Get
            EnCursoAprobacion = mlEnCursoAprobacion
        End Get
    End Property
    ReadOnly Property Todas() As Long
        Get
            Todas = mlTodas
        End Get
    End Property
#End Region
    Property ContratosVisor() As DataSet
        Get
            ContratosVisor = mdsContratos
        End Get
        Set(ByVal Value As DataSet)
            mdsContratos = Value
        End Set
    End Property
#End Region
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
    ''' <summary>Obtiene los campos del formulario para que el usuario elija los que quiera mostrar en el filtro. 
    ''' Solo se muestran los campos visibles para el rol del usuario en cualquiera de las etapas.
    ''' Se excluyen los campos tipo desglose y los que estan dentro de un desglose.
    ''' </summary>
    ''' <param name="sUsuario">Codigo de la persona para obtener el rol</param>
    ''' <param name="iFiltroId">Codigo del filtro a configurar</param>
    ''' <param name="sIdi">Idioma en el que se va a mostrar la denominacion del campo</param>
    ''' <returns>Todos los grupos y los campos simples del formulario visibles para el usuario</returns>
    ''' <remarks>Llamada desde: ConfigruarFiltros.aspx; Tiempo maximo: 0 sg</remarks>
    Public Function LoadCampos(ByVal sUsuario As String, ByVal iFiltroId As Integer, ByVal sIdi As String) As DataSet
        Authenticate()
        LoadCampos = DBServer.FiltroContrato_GetCampos(sUsuario, iFiltroId, sIdi)
    End Function
    ''' <summary>
    ''' Guarda la configuración del filtro definida por el usuario.
    ''' </summary>
    ''' <remarks>Llamada desde: Contratos/ConfigurarFiltrosContratos.aspx.</remarks>
    Public Sub GuardarConfiguracion()
        Authenticate()
        mIDFiltroUsuario = DBServer.FiltroContrato_SaveConfiguracion(mIDFiltro, mPersona, mNombreFiltro, mIDFiltroUsuario, mCamposConfigurados, mPorDefecto)
    End Sub
    ''' <summary>
    ''' Elimina la configuración del filtro.
    ''' </summary>
    ''' <remarks>Llamada desde: ConfigurarFiltrosContratos.aspx</remarks>
    Public Sub EliminarConfiguracion(ByVal IDFiltroUsuario As Integer)
        Authenticate()
        DBServer.FiltroContrato_Eliminar(IDFiltroUsuario)
    End Sub
    ''' <summary>
    ''' Obtiene la configuracion guardada de filtro con el identificador y el usuario especificados
    ''' </summary>
    ''' <remarks>Llamada desde: ConfigurarFiltrosContratos.aspx</remarks>
    Public Sub LoadFiltro()
        Authenticate()
        Dim configuracion As DataSet = DBServer.FiltroContrato_Load(mIDFiltroUsuario, mPersona, mIdioma)
        If configuracion.Tables.Count > 0 Then
            'Table(0): filtro
            'Table(1): criterios de busqueda
            'Table(2): campos generales
            'Table(3): grupos de los campos de formulario
            'Table(4): campos de formulario
            If configuracion.Tables(0).Rows.Count > 0 Then
                mIDFiltro = configuracion.Tables(0).Rows(0)("ID_FILTRO")
                mNombreFiltro = configuracion.Tables(0).Rows(0)("NOM_FILTRO")
                mPorDefecto = configuracion.Tables(0).Rows(0)("DEFECTO")
                mOrdenacion = DBNullToSomething(configuracion.Tables(0).Rows(0)("ORDENACION"))
                mSentidoOrdenacion = DBNullToSomething(configuracion.Tables(0).Rows(0)("SENTIDOORDENACION"))
                mIsLoaded = True
            End If
            mBusqueda = configuracion.Tables(1)
            mCamposGenerales = configuracion.Tables(2)
            mCamposFormulario = New DataSet
            mCamposFormulario.Tables.Add(configuracion.Tables(3).Copy)
            mCamposFormulario.Tables.Add(configuracion.Tables(4).Copy)
            mCamposFormulario.Relations.Add("REL_GRUPO_CAMPOS", mCamposFormulario.Tables(0).Columns("ID"), mCamposFormulario.Tables(1).Columns("PADRE"), False)
            mCamposOrdenadosParaVisor = configuracion.Tables(5)
        End If
    End Sub
    ''' <summary>
    ''' Obtiene una instancia ejemplo que se cargará en la grid para que el usuario pueda ver como queda con datos,
    ''' y pueda mover el ancho y el orden de la columnas.
    ''' </summary>
    ''' <param name="iFiltroId">Id del filtro que se está configurando</param>
    ''' <param name="sIdioma">Idioma del usuario</param>
    ''' <returns>Una instancia ejemplo</returns>
    ''' <remarks>Llamada desde: ConfigurarFiltrosContratos.aspx; Tiempo máximo: 1 sg</remarks>
    Public Function LoadConfiguracionInstancia(ByVal iFiltroId As Integer, ByVal sIdioma As String) As DataSet
        Authenticate()
        LoadConfiguracionInstancia = DBServer.FiltroContrato_LoadInstancia(iFiltroId, sIdioma)
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sUsu"></param>
    ''' <param name="sIdioma"></param>
    ''' <param name="sCodigoContrato"></param>
    ''' <param name="sTipo"></param>
    ''' <param name="sDescripcion"></param>
    ''' <param name="sProve"></param>
    ''' <param name="sPeticionario"></param>
    ''' <param name="dFechaDesde"></param>
    ''' <param name="dFechaHasta"></param>
    ''' <param name="sGMN1"></param>
    ''' <param name="sCodArticulo"></param>
    ''' <param name="sNombreTabla"></param>
    ''' <param name="sPalabraClave">Búsqueda por palabra clave</param>
    ''' <param name="PageNumberBD">número de página</param>
    ''' <param name="PageSizeBD">tamaño de la página</param>
    ''' <param name="ObtenerTodosContratos">Obtener todos los contratos</param>
    ''' <param name="CentroCoste">Código del centro de coste</param>
    ''' <param name="PartidasPres">CONJUNTO DE CÓDIGOS DE PARTIDAS PRESUPUESTARIAS PASADOS EN UN TIPO TABLA</param>
    ''' <param name="iAlertaExpiracion">alerta antes de n dias</param>
    ''' <param name="sNotificado">Notificados alerta</param>
    ''' <remarks>LLamada desde: PMWeb2008/VisorContratos.aspx</remarks>
    Public Sub Visor_DevolverContratos(ByVal sUsu As String, ByVal sIdioma As String, ByVal sEstadoContadores As String, Optional ByVal sCodigoContrato As String = "",
                                       Optional ByVal sTipo As String = "", Optional ByVal sDescripcion As String = "", Optional ByVal sProve As String = "",
                                       Optional ByVal sPeticionario As String = "", Optional ByVal dFechaDesde As Date = Nothing, Optional ByVal dFechaHasta As Date = Nothing,
                                       Optional ByVal sMaterial As String = "", Optional ByVal sCodArticulo As String = "", Optional ByVal sCodEmpresa As String = "",
                                       Optional ByVal sGMN1 As String = "", Optional ByVal sCodProceso As Integer = 0, Optional ByVal sAnyo As Short = 0, Optional ByVal sEstado As String = "",
                                       Optional ByVal sNombreTabla As String = "", Optional ByVal sOrdenacion As String = "", Optional ByVal sPalabraClave As String = "",
                                       Optional ByVal PageNumberBD As Integer = 1, Optional ByVal PageSizeBD As Integer = 1000, Optional ByVal ObtenerTodosContratos As Boolean = False,
                                       Optional ByVal CentroCoste As String = "", Optional ByVal PartidasPres As DataTable = Nothing,
                                       Optional ByVal iAlertaExpiracion As Integer = 0, Optional ByVal sNotificado As String = "")
        Authenticate()

        Dim ds As DataSet
        ds = DBServer.Contratos_Load_New(sUsu, sIdioma, sEstadoContadores, sCodigoContrato, sTipo, sDescripcion, sProve, sPeticionario, dFechaDesde,
                                         dFechaHasta, sMaterial, sCodArticulo, sCodEmpresa, sGMN1, sCodProceso, sAnyo, sEstado, sNombreTabla, sOrdenacion,
                                         sPalabraClave, PageNumberBD, PageSizeBD, ObtenerTodosContratos, CentroCoste, PartidasPres, iAlertaExpiracion, sNotificado)

        If ds.Tables(1).Rows.Count > 0 Then
            mlGuardado = ds.Tables(1).Rows(0).Item("GUARDADAS")
            mlEnCursoAprobacion = ds.Tables(1).Rows(0).Item("EN_CURSO_APROBACION")
            mlVigentes = ds.Tables(1).Rows(0).Item("VIGENTES")
            mlProximoAExpirar = ds.Tables(1).Rows(0).Item("PROXIMO_A_EXPIRAR")
            mlExpirado = ds.Tables(1).Rows(0).Item("EXPIRADOS")
            mlRechazados = ds.Tables(1).Rows(0).Item("RECHAZADOS")
            mlAnulados = ds.Tables(1).Rows(0).Item("ANULADOS")
            mlTodas = ds.Tables(1).Rows(0).Item("TODAS")
        End If

        For Each oColumn As DataColumn In ds.Tables(0).Columns
            If UCase(oColumn.DataType.FullName) = "SYSTEM.DATETIME" Then
                For Each oRow As DataRow In ds.Tables(0).Rows
                    If Not IsDBNull(oRow.Item(oColumn.ColumnName)) Then
                        oRow.Item(oColumn.ColumnName) = Format(oRow.Item(oColumn.ColumnName), "d")
                    End If
                Next
            End If
        Next

        ds.Tables(0).Columns.Add("APROBAR", System.Type.GetType("System.Int16"))
        ds.Tables(0).Columns.Add("RECHAZAR", System.Type.GetType("System.Int16"))

        mdsContratos = ds
    End Sub
    ''' <summary>
    ''' Obtiene la configuración de los campos de búsqueda definidos por el usuario en ese filtro
    ''' </summary>
    ''' <param name="sIdi">Codigo idioma</param>
    ''' <remarks>Llamada desde: VisorContratos.aspx --> CargarBusquedaAvanzada // ConfigurarFiltros --> CargarBusquedaAvanzada. Tiempo máximo: 1 sg.</remarks>
    Public Sub LoadConfiguracionCamposBusqueda(ByVal sIdi As String)
        Authenticate()

        Dim dr As SqlClient.SqlDataReader = Nothing
        Dim dtNewRow As DataRow
        Dim dt As DataTable = Nothing

        DBServer.FiltroContratos_GetCamposBusqueda(mIDFiltroUsuario, sIdi, dr)

        If mCamposConfigurados Is Nothing Then
            mCamposConfigurados = New DataSet
        End If

        If mCamposConfigurados.Tables("CAMPOSBUSQUEDA") Is Nothing Then
            dt = mCamposConfigurados.Tables.Add("CAMPOSBUSQUEDA")
            dt.Columns.Add("PROCE_ANYO", System.Type.GetType("System.Int32"))
            dt.Columns.Add("PROCE_GMN1", System.Type.GetType("System.String"))
            dt.Columns.Add("PROCE_COD", System.Type.GetType("System.Int32"))
            dt.Columns.Add("PROCE_DEN", System.Type.GetType("System.String"))
            dt.Columns.Add("TIPO", System.Type.GetType("System.String"))
            dt.Columns.Add("DESCRIPCION", System.Type.GetType("System.String"))
            dt.Columns.Add("PROVEEDOR", System.Type.GetType("System.String"))
            dt.Columns.Add("PROVEDEN", System.Type.GetType("System.String"))
            dt.Columns.Add("GMN1", System.Type.GetType("System.String"))
            dt.Columns.Add("GMN2", System.Type.GetType("System.String"))
            dt.Columns.Add("GMN3", System.Type.GetType("System.String"))
            dt.Columns.Add("GMN4", System.Type.GetType("System.String"))
            dt.Columns.Add("MATDEN", System.Type.GetType("System.String"))
            dt.Columns.Add("EMP", System.Type.GetType("System.String"))
            dt.Columns.Add("EMPDEN", System.Type.GetType("System.String"))
            dt.Columns.Add("ARTICULO", System.Type.GetType("System.String"))
            dt.Columns.Add("ARTDEN", System.Type.GetType("System.String"))
            dt.Columns.Add("ESTADO", System.Type.GetType("System.String"))
            dt.Columns.Add("PALABRA_CLAVE", System.Type.GetType("System.String"))
            dt.Columns.Add("FEC_ALTA", System.Type.GetType("System.DateTime"))
            dt.Columns.Add("FECHA_INICIO", System.Type.GetType("System.DateTime"))
            dt.Columns.Add("FECHA_EXP", System.Type.GetType("System.DateTime"))
            dt.Columns.Add("PETICIONARIO", System.Type.GetType("System.String"))
            dt.Columns.Add("CENTRO_COSTE", System.Type.GetType("System.String"))
            dt.Columns.Add("PARTIDA1", System.Type.GetType("System.String"))
            dt.Columns.Add("PARTIDA2", System.Type.GetType("System.String"))
            dt.Columns.Add("PARTIDA3", System.Type.GetType("System.String"))
            dt.Columns.Add("PARTIDA4", System.Type.GetType("System.String"))
            dt.Columns.Add("ALERTA_EXPIRACION", System.Type.GetType("System.Int32"))
            dt.Columns.Add("ALERTA_PERIODO", System.Type.GetType("System.Int32"))
            dt.Columns.Add("ALERTA_NOTIFICADO", System.Type.GetType("System.String"))
        End If

        If dr.Read() Then
            dtNewRow = dt.NewRow
            dtNewRow.Item("TIPO") = dr("TIPO")
            dtNewRow.Item("DESCRIPCION") = dr("DESCRIPCION")
            dtNewRow.Item("PROVEEDOR") = dr("PROVEEDOR")
            dtNewRow.Item("PROVEDEN") = dr("PROVEDEN")
            dtNewRow.Item("GMN1") = dr("GMN1")
            dtNewRow.Item("GMN2") = dr("GMN2")
            dtNewRow.Item("GMN3") = dr("GMN3")
            dtNewRow.Item("GMN4") = dr("GMN4")
            dtNewRow.Item("PROCE_ANYO") = dr("PROCE_ANYO")
            dtNewRow.Item("PROCE_COD") = dr("PROCE_COD")
            dtNewRow.Item("PROCE_GMN1") = dr("PROCE_GMN1")
            dtNewRow.Item("PROCE_DEN") = dr("PROCE_DEN")
            dtNewRow.Item("EMP") = dr("EMP")
            dtNewRow.Item("EMPDEN") = dr("EMPDEN")
            dtNewRow.Item("MATDEN") = dr("MATDEN")
            dtNewRow.Item("ARTICULO") = dr("ARTICULO")
            dtNewRow.Item("ARTDEN") = dr("ARTDEN")
            dtNewRow.Item("PALABRA_CLAVE") = dr("PALABRA_CLAVE")
            dtNewRow.Item("ESTADO") = DBNullToStr(dr("ESTADO"))
            If Not IsDBNull(dr("FECHA_INICIO")) Then
                dtNewRow.Item("FECHA_INICIO") = dr("FECHA_INICIO")
            End If
            If Not IsDBNull(dr("FECHA_EXP")) Then
                dtNewRow.Item("FECHA_EXP") = dr("FECHA_EXP")
            End If
            
            dtNewRow.Item("PETICIONARIO") = DBNullToStr(dr("PETICIONARIO"))
            dtNewRow.Item("CENTRO_COSTE") = DBNullToStr(dr("CENTRO_COSTE"))
            dtNewRow.Item("PARTIDA1") = DBNullToStr(dr("PARTIDA1"))
            dtNewRow.Item("PARTIDA2") = DBNullToStr(dr("PARTIDA2"))
            dtNewRow.Item("PARTIDA3") = DBNullToStr(dr("PARTIDA3"))
            dtNewRow.Item("PARTIDA4") = DBNullToStr(dr("PARTIDA4"))
            dtNewRow.Item("ALERTA_EXPIRACION") = dr("ALERTA_EXPIRACION")
            dtNewRow.Item("ALERTA_PERIODO") = dr("ALERTA_PERIODO")
            dtNewRow.Item("ALERTA_NOTIFICADO") = DBNullToStr(dr("ALERTA_NOTIFICADO"))

            dt.Rows.Add(dtNewRow)
        End If

        If Not dr Is Nothing AndAlso Not dr.IsClosed Then
            dr.Close()
        End If
    End Sub
    ''' <summary>
    ''' Obtiene la ordenacion de un filtro.
    ''' </summary>
    ''' <remarks>Llamada desde: NoConformidades/NoConformidades.aspx. Tiempo máximo: 0 sg.</remarks>
    Public Sub cargarOrdenacionFiltro()
        Authenticate()
        msOrdenacion = DBServer.FiltroContratos_CargarOrdenacion(mIDFiltroUsuario)
    End Sub


    Public Function GetContratosBuscador(ByVal sidioma As String, ByVal sCodigo As String, ByVal speticionario As String, ByVal ltipo As Long,
                    ByVal sarticulo As String, ByVal sproveedor As String, ByVal lempresa As Nullable(Of Long),
                    ByVal lestado As Nullable(Of Integer), ByVal dfechaDesde As Nullable(Of Date), ByVal dfechaHasta As Nullable(Of Date)) As DataTable

        Authenticate()
        Return DBServer.Contratos_CargarContratosParaBuscador(sidioma, sCodigo, speticionario, ltipo, sarticulo, sproveedor, lempresa, lestado, dfechaDesde, dfechaHasta)

    End Function


End Class