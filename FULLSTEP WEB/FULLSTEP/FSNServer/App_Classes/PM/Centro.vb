
<Serializable()> _
Public Class Centro
    Inherits Security

    Private msCod As String
    Private msDen As String
    Private _oAlmacenes As cAlmacenes

    Private moCentro As DataSet

    Public Property Cod() As String
        Get
            Return msCod
        End Get

        Set(ByVal Value As String)
            msCod = Value
        End Set
    End Property

    Public Property Den() As String
        Get
            Return msDen
        End Get

        Set(ByVal Value As String)
            msDen = Value
        End Set
    End Property

    Public ReadOnly Property Data() As Data.DataSet
        Get
            Return moCentro
        End Get

    End Property

    Public ReadOnly Property Almacenes As cAlmacenes
        Get
            Return _oAlmacenes
        End Get
    End Property

    ''' <summary>
    ''' Procedimiento que carga los datos del centro
    ''' </summary>
    ''' <remarks>
    ''' Llamada desde: PmWeb/ArticulosServer/Page_Load, PmWeb/Campos/Page_Load, PmWeb/Desglose/Page_Load y CargarValoresDefecto
    ''' Tiempo m�ximo: 0,4 seg</remarks>
    Public Sub LoadData()
        Authenticate()
        moCentro = DBServer.Centro_Get(msCod)
    End Sub


    Public Sub New(ByRef dbserver As FSNDataBaseServer.Root,ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
    End Sub

    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean, ByVal sCod As String, ByVal sDen As String)
        MyBase.New(dbserver, isAuthenticated)
        msCod = sCod
        msDen = sDen
    End Sub
    ''' <summary>Carga los almacenes correspondientes al centro de aprovisionamiento</summary>
    ''' <param name="sCentro">Centro de aprovisionamiento</param>
    ''' <remarks>Llamada desde: FSNWeb.EmisionPedidos.CargarAlmacenes</remarks>
    Public Sub CargarAlmacenesCentroAprovisionamiento(ByVal sCentro As String)
        Authenticate()
        Dim dsAlmacenes As DataSet = DBServer.DevolverAlmacenesCentroAprovisionamiento(sCentro)
        If Not dsAlmacenes Is Nothing AndAlso dsAlmacenes.Tables.Count > 0 Then
            _oAlmacenes = New cAlmacenes(mDBServer, mIsAuthenticated)

            For Each oRow As DataRow In dsAlmacenes.Tables(0).Rows
                _oAlmacenes.Add(oRow("ID"), oRow("COD"), oRow("DEN"), Nothing, Nothing)
            Next
        End If
    End Sub

End Class
