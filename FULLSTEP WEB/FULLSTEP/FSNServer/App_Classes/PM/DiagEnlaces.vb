<Serializable()> _
    Public Class DiagEnlaces
    Inherits Security
    Private moDiagEnlaces As DataSet


    Public ReadOnly Property Data() As Data.DataSet
        Get
            Return moDiagEnlaces
        End Get

    End Property

    ''' <summary>
    ''' Procedimiento que devuelve los Diagolos de enlaces(comentarios)
    ''' </summary>
    ''' <param name="lID">Identificador del bloque</param>
    ''' <param name="sIdi">Idioma de la aplicaci�n</param>
    ''' <remarks>
    ''' Llamada desde: PmWeb/flowDiagram/Page_Load, PmWeb/FlowdiagramExport/Page_Load
    ''' Tiempo m�ximo: 0,8 seg</remarks>
    Public Sub LoadData(ByVal lID As Long, ByVal sIdi As String)
        Authenticate()
        moDiagEnlaces = DBServer.DiagEnlaces_Get(lID, sIdi)
    End Sub


    Public Sub New(ByRef dbserver As FSNDataBaseServer.Root,ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
    End Sub


End Class

