Imports System.Configuration
<Serializable()> _
Public Class Adjuntos
    Inherits Security

    Private moData As DataSet

    Property Data() As DataSet
        Get
            Return moData
        End Get
        Set(ByVal Value As DataSet)
            moData = Value
        End Set
    End Property


    ''' <summary>
    ''' Carga los adjuntos indicados.
    ''' </summary>
    ''' <param name="iTipo">Para quien sacar los adjuntos 1:Campos 3:Desglose</param>
    ''' <param name="sIdAdjuntos">Id en bbdd de adjuntos grabados de antes, Estaran en CAMPO_ADJUN/LINEA_DESGLOSE_ADJUN</param>
    ''' <param name="sIdAdjuntosNew">Id en bbdd de adjuntos aun no grabados en la instancia, Estaran en CAMPO_ADJUN/LINEA_DESGLOSE_ADJUN</param>
    ''' <param name="bFavorito">Indica si estamos cargando o no un desglose pop-up favorito</param>
    ''' <remarks>Llamada desde: desglose.ascx; Tiempo m�ximo: 0sg</remarks>
    Public Sub Load(ByVal iTipo As Integer, Optional ByVal sIdAdjuntos As String = Nothing, Optional ByVal sIdAdjuntosNew As String = Nothing, Optional ByVal bFavorito As Boolean = False)
        Authenticate()
        moData = DBServer.Adjuntos_Load(iTipo, sIdAdjuntos, sIdAdjuntosNew, bFavorito)
    End Sub
    ''' <summary>
    ''' Devuelve los adjuntos de tipo contrato que tenga ese campo
    ''' </summary>
    ''' <param name="idContrato">id del contrato</param>
    ''' <param name="idArchivoContrato">id del adjunto del contrato</param>
    ''' <param name="idCampo">id del campo</param>
    ''' <remarks></remarks>
    Public Sub LoadInst_Adj_Contrato(ByVal idContrato As Long, ByVal idArchivoContrato As Long, ByVal idCampo As Long)
        Authenticate()
        moData = DBServer.Adjuntos_Contrato_Load(idContrato, idArchivoContrato, idCampo)
    End Sub

    ''' <summary>
    ''' Carga los adjuntos indicados de la instancia
    ''' </summary>
    ''' <param name="iTipo">Tipo de adjuntos</param>
    ''' <param name="sIdAdjuntos">Ids de los adjuntos</param>
    ''' <param name="sIdAdjuntosNew">Ids nuevos de adjuntos</param>
    ''' <remarks>
    ''' Llamada desde: PmWeb/attachedfiles/Page_Load , PMWeb/desglose/Page_Load
    ''' Tiempo m�ximo:1 seg </remarks>
    Public Sub LoadInst(ByVal iTipo As Integer, Optional ByVal sIdAdjuntos As String = Nothing, Optional ByVal sIdAdjuntosNew As String = Nothing)
        Authenticate()
        moData = DBServer.Adjuntos_InstLoad(iTipo, sIdAdjuntos, sIdAdjuntosNew)
    End Sub


    ''' <summary>
    ''' Devuelve los archivos adjuntos de un proceso
    ''' </summary>
    ''' <param name="lAnyo">A�o del proceso</param>
    ''' <param name="sGMN1">Material de nivel 1 del proceso</param>
    ''' <param name="lProce">Codigo del proceso</param>
    ''' <param name="sGrupo">Grupo del proceso</param>
    ''' <param name="lItem">Codigo del item</param>
    ''' <param name="sProve">Proveedor del item</param>
    ''' <param name="lOfe">Tama�o del adjunto</param>
    ''' <remarks>
    ''' Llamada desde: PMWeb/attachedfilesproceso/P�ge_Load
    ''' Tiempo m�ximo: 2 seg</remarks>
    Public Sub LoadAdjuntosProceso(ByVal lAnyo As Long, ByVal sGMN1 As String, ByVal lProce As Long, Optional ByVal sGrupo As String = Nothing, Optional ByVal lItem As Integer = Nothing, Optional ByVal sProve As String = Nothing, Optional ByVal lOfe As Long = Nothing)
        Authenticate()
        moData = DBServer.Adjuntos_LoadProceso(lAnyo, sGMN1, lProce, sGrupo, lItem, sProve, lOfe)
    End Sub

    ''' <summary>
    ''' Devuelve los archivos adjuntos de un pedido
    ''' </summary>
    ''' <param name="lPedido">Id del pedido</param>
    ''' <param name="lLinea">Id de la linea</param>
    ''' <remarks>
    ''' Llamada desde:PmWeb/attachedFilesPedido/Page_Load
    ''' tiempo m�ximo: 2 seg</remarks>
    Public Sub LoadAdjuntosPedido(ByVal lPedido As Long, Optional ByVal lLinea As Long = Nothing)
        Authenticate()
        moData = DBServer.Adjuntos_LoadPedido(lPedido, lLinea)
    End Sub

    ''' <summary>
    ''' Devuelve los adjuntos por instancia y campo
    ''' </summary>
    ''' <param name="lId">Id del adjunto</param>
    ''' <param name="lCampo">Codigo del campo</param>
    ''' <remarks>
    ''' Llamada desde:PmWeb2008/VisorSolicitudes/gvSolicitudes_RowDataBound
    ''' Tiempo m�ximo: 1 seg</remarks>
    Public Sub LoadAdjuntosPorInstanciayCampo(ByVal lId As Long, ByVal lCampo As Long)
        Authenticate()
        moData = DBServer.Adjuntos_LoadIdsPorInstanciayCampo(lId, lCampo)
    End Sub
    ''' <summary>
    ''' Cuando se copia una fila de un desglose esta funcion crea los adjuntos que se copian
    ''' </summary>
    ''' <param name="AdjuntosAct">Adjuntos actuales</param>
    ''' <param name="AdjuntosNew">Adjuntos que se acaban de adjuntar</param>
    ''' <param name="bDefecto">true si son adjuntos por defecto</param>
    ''' <returns>un array con los ids de los nuevos adjuntos copiados</returns>
    ''' <remarks></remarks>
    Public Function CrearAdjuntosCopiados(ByVal AdjuntosAct As String, ByVal AdjuntosNew As String, ByVal bDefecto As Boolean) As String()
        Return DBServer.Adjuntos_CrearAdjuntosCopiados(AdjuntosAct, AdjuntosNew, bDefecto)
    End Function

    Public Sub LoadAdjuntosPorInstancia(ByVal lId As Long)
        moData = DBServer.Adjuntos_LoadPorInstancia(lId)
    End Sub


    Public Sub New(ByRef dbserver As FSNDataBaseServer.Root,ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
    End Sub



End Class
