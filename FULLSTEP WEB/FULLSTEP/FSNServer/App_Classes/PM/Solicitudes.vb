Imports System.Configuration

<Serializable()> _
Public Class Solicitudes
    Inherits Security

    Private moSolicitudes As DataSet
    Public ReadOnly Property Data() As Data.DataSet
        Get
            Return moSolicitudes
        End Get

    End Property
    ''' <summary>
    ''' Lista de solicitudes q tiene un usuario.
    ''' </summary>
    ''' <param name="sUsu">usuario</param>
    ''' <param name="sIdi">idioma en q ver los textos</param>
    ''' <param name="bEsCombo">sql para cargar combo o no</param>
    ''' <param name="iTipoSolicitud">tipo de solicitud</param>
    ''' <param name="bNuevoWorkfl">Saca informaci�n de solicitudes (1) � certificados/no conformidades (0)</param>
    ''' <param name="DesdeView">la carga del combo de seguimiento tambien depende del combo 'abiertas por usted', 'procesos en los q participa' , ...</param>
    ''' <param name="bComboAltaNC">la carga del combo de alta de no conformidades es: los q no tengan peticionario y los q el usuario conectado es peticionario</param>
    ''' <param name="sCampoOrdenacion">Campo por el que se quiere ordenar</param>
    ''' <param name="sCodPer">Codigo Persona</param>
    ''' <param name="bEscalacion">Segun el prototipo "escalaci�n de proveedores" NO se deja en alta de nc, crear nc nivel 4</param>
    ''' <remarks>Llamada desde: solicitudes.aspx, solicitarcertificados.aspx, noconformidad.aspx, NWSeguimiento.aspx, wucbusquedaAvanzada.ascx, wucbusquedaAvanzadaNC.ascx-->CargarTiposSolicitudes; Tiempo m�ximo: 0,3 sg</remarks>
    Public Sub LoadData(ByVal sUsu As String, Optional ByVal sIdi As String = "SPA", Optional ByVal bEsCombo As Integer = 0, Optional ByVal iTipoSolicitud As Integer = Nothing _
                        , Optional ByVal bNuevoWorkfl As Boolean = False, Optional ByVal DesdeView As Boolean = False, Optional ByVal bComboAltaNC As Boolean = False, Optional ByVal sCampoOrdenacion As String = "" _
                        , Optional ByVal sCodPer As String = "", Optional ByVal bEscalacion As Boolean = False)
        Authenticate()
        moSolicitudes = DBServer.Solicitudes_Get(sUsu, sIdi, bEsCombo, iTipoSolicitud, bNuevoWorkfl, DesdeView, bComboAltaNC, sCampoOrdenacion, sCodPer, bEscalacion)
    End Sub
    ''' <summary>
    ''' Lista de solicitudes q tiene un usuario.
    ''' </summary>
    ''' <param name="sUsu">usuario</param>
    ''' <param name="sIdi">idioma en q ver los textos</param>
    ''' <returns>Dataset con los tipos de solicitudes</returns>
    ''' <remarks>Llamada desde: wucbusquedaAvanzada.ascx; Tiempo m�ximo: 0sg</remarks>
    Public Function LoadTiposSolicitudes(ByVal sUsu As String, ByVal sIdi As String, ByVal iTipo As Integer)
        Authenticate()

        Dim Contexto As System.Web.HttpContext
        Dim ds As DataSet

        Contexto = System.Web.HttpContext.Current
        ds = CType(Contexto.Cache("dsTipos_" & sUsu & "_" & sIdi & "_" & iTipo), DataSet)
        If ds Is Nothing Then
            ds = DBServer.Solicitudes_LoadTiposSolicitudes(sUsu, sIdi, iTipo)
            Contexto.Cache.Insert("dsTipos_" & sUsu & "_" & sIdi & "_" & iTipo, ds, Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration, New TimeSpan(0, System.Configuration.ConfigurationManager.AppSettings("TiempoExpiracionCacheMin"), 0))
        End If
        LoadTiposSolicitudes = ds
    End Function
    ''' <summary>
    ''' Lista de solicitudes q tiene un desglose vinculado.
    ''' </summary>
    ''' <param name="idioma">idioma en q ve los textos</param>
    ''' <param name="Instancia">Id de la instancia</param>
    ''' <param name="IdCampo">Id del desglose vinculado</param>
    ''' <returns>Lista de solicitudes</returns>
    ''' <remarks>Llamada desde: PMWeb2008\script\_common\BuscadorSolicitudes.aspx; Tiempo m�ximo: 0,1</remarks>
    Public Function LoadTiposSolicitudes(ByVal idioma As String, ByVal Instancia As Long, ByVal IdCampo As Long) As DataSet
        Authenticate()
        Return DBServer.Solicitudes_LoadTiposSolicitudes(idioma, Instancia, IdCampo)
    End Function
    ''' <summary>
    ''' Procedimiento que carga los datos de los certificados
    ''' </summary>
    ''' <param name="sIdi">Idioma de la aplicaci�n</param>
    ''' <param name="iMaterialQA">Indetificador del material de QA</param>
    ''' <param name="sUsu">Codigo de usuario</param>
    ''' <param name="bBajas">Variable booleana que indica si se deben cargar las solicitudes que estan de baja</param>
    ''' <param name="sOrdenar">Por que campo ordenar</param>  
    ''' <remarks>
    ''' Llamada desde: PmWeb/asignarTiposCertif/Page_load, PmWeb/altaMaterialQA/Page_load, PmWeb/filtroCertificcados/CargarCertificados 
    ''' Tiempo m�ximo: 0,35 seg</remarks>
    Public Sub LoadCertificados(Optional ByVal sIdi As String = "SPA", Optional ByVal iMaterialQA As Integer = 0, _
            Optional ByVal sUsu As String = Nothing, Optional ByVal bBajas As Boolean = False, _
            Optional ByVal sOrdenar As String = "S.ID", Optional ByVal RestricSolicUsu As Boolean = False, _
            Optional ByVal RestricSolicUO As Boolean = False, Optional ByVal RestricSolicDep As Boolean = False)
        Authenticate()
        moSolicitudes = DBServer.Solicitudes_LoadCertificados(sIdi, iMaterialQA, sUsu, bBajas, sOrdenar)
    End Sub
    ''' <summary>
    ''' Procedimiento que carga todas las solicitudes favoritas de un usuario
    ''' </summary>
    ''' <param name="lIdSolicitud">Identificador de la solicitud, en caso de querer cargarse solo 1</param>
    ''' <param name="sUsu">Codigo de usuario</param>
    ''' <remarks>
    ''' Llamada desde: PmWeb/solicitudesFavoritas/Page_load
    ''' Tiempo m�ximo: 0,4 seg</remarks>
    Public Sub LoadFavoritos(ByVal lIdSolicitud As Long, ByVal sUsu As String)
        Authenticate()
        moSolicitudes = DBServer.Solicitudes_LoadFavoritos(lIdSolicitud, sUsu)
    End Sub
    ''' <summary>
    ''' Procedimiento que elimina una solicitud favorita
    ''' </summary>
    ''' <param name="lIdSolicitudFav">Identificador de la solicitud favorita</param>
    ''' <remarks>
    ''' Llamada desde: PmWeb/solicitudesfavoritas/Page_load
    ''' Tiempo m�ximo: 0,2 seg</remarks>
    Public Sub EliminarSolicitudFavorita(ByVal lIdSolicitudFav As Long)
        Authenticate()
        DBServer.Solicitudes_EliminarSolicitudFavoritos(lIdSolicitudFav)
    End Sub
    ''' <summary>
    ''' Obtiene las denominaciones de los tipos de solicitudes publicadas (no dadas de baja) y para las que 
    ''' el usuario est� configurado como rol peticionario.
    ''' </summary>
    ''' <param name="sUsu">Cod de la persona</param>
    ''' <param name="sIdi">Idioma para obtener la denominac��n</param>
    ''' <remarks>Llamada desde: FSPMWebPartAltaSolicitudes.vb. Tiempo m�ximo: 0 sg.</remarks>
    Public Sub LoadTiposSolicitudesRolPeticionario(ByVal sUsu As String, Optional ByVal sIdi As String = "SPA", Optional ByVal bPeticionario As Boolean = False,
                                                   Optional ByVal iTipoSolicitud As Short = TiposDeDatos.TipoDeSolicitud.Otros)
        Authenticate()
        moSolicitudes = DBServer.Solicitudes_LoadTiposSolicitudesRolPeticionario(sUsu, sIdi, bPeticionario, iTipoSolicitud)
    End Sub
    ''' <summary>
    ''' Obtiene las solicitudes favoritas del usuario con el tipo de solicitud.
    ''' </summary>
    ''' <param name="sUsu">Cod de la persona</param>
    ''' <param name="sIdi">Idioma para obtener la denominac��n</param>
    ''' <remarks>Llamada desde: FSPMWebPartAltaSolicitudes.vb. Tiempo m�ximo: 0 sg.</remarks>
    Public Sub LoadSolicitudesFavoritas(ByVal sUsu As String, ByVal sIdi As String, ByVal TipoSolicitud As TiposDeDatos.TipoDeSolicitud)
        Authenticate()
        moSolicitudes = DBServer.Solicitudes_LoadSolicitudesFavoritas(sUsu, sIdi, TipoSolicitud)
    End Sub
    ''' <summary>
    ''' Obtiene las solicitudes favoritas y las solicitudes "tipo" que haya seleccionado el usuario.
    ''' </summary>
    ''' <param name="sUsu">Cod de la persona</param>
    ''' <param name="sIdi">Idioma para obtener la denominac��n</param>
    ''' <remarks>Llamada desde: FSPMWebPartAltaSolicitudes.vb. Tiempo m�ximo: 0 sg.</remarks>
    Public Sub LoadAltaSolicitudes(ByVal sUsu As String, ByVal sIdi As String, ByVal sTipos As String, ByVal sFavoritas As String, ByVal TipoSolicitudes As TiposDeDatos.TipoDeSolicitud)
        Authenticate()
        moSolicitudes = DBServer.Solicitudes_LoadAltaSolicitudes(sUsu, sIdi, sTipos, sFavoritas, TipoSolicitudes)
    End Sub
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
    ''' <summary>
    ''' Para la tarea 1959 (Vinculaciones entre desgloses) se ha hecho un buscador de Instancias. Esta funci�n devuelve las
    ''' instancias de la solicitud origen.
    ''' </summary>
    ''' <param name="Instancia">0-Busca desde NWAlta Eoc-Buscas desde NWGestionInstancia, sera la instancia a la q traer instancias o lineas</param>
    ''' <param name="idCampo">Desglose al q a�adir los datos, con el se sacan los posibles origenes</param>
    ''' <param name="idioma">Idioma</param>
    ''' <param name="identificador">identificador de instancia</param>
    ''' <param name="denominacion">denominacion de instancia</param>
    ''' <param name="fechaAltaDesde">fecha Alta Desde de instancia</param>
    ''' <param name="fechaAltaHasta">fecha Alta Hasta de instancia</param>
    ''' <param name="importeDesde">importe Desde de instancia</param>
    ''' <param name="importeHasta">importe Hasta de instancia</param>
    ''' <param name="peticionario">peticionario de instancia</param>
    ''' <param name="tipo">tipo de solictud</param>
    ''' <param name="estado">estado de instancia</param>
    ''' <param name="articulo">articulo de instancia</param>
    ''' <param name="InstanciaMover">instancia de la q mueves una linea</param>
    ''' <returns>Lista de instancia q cumplen con las parametros dados</returns>
    ''' <remarks>Llamada desde:_common\BuscadorSolicitudes.aspx; tiempo maximo:0,1</remarks>
    Public Function GetSolicitudesBuscador(ByVal Instancia As Long, ByVal idCampo As Long, ByVal idioma As String, ByVal identificador As String, _
                                           ByVal denominacion As String, ByVal fechaAltaDesde As Nullable(Of Date), ByVal fechaAltaHasta As Nullable(Of Date), _
                                           ByVal importeDesde As Nullable(Of Double), ByVal importeHasta As Nullable(Of Double), _
                                           ByVal peticionario As String, ByVal tipo As Nullable(Of Long), ByVal estado As Nullable(Of Integer), _
                                           ByVal articulo As String, ByVal InstanciaMover As Nullable(Of Long)) As DataTable
        Authenticate()
        Return DBServer.Solicitudes_CargarSolicitudesParaBuscador(Instancia, idCampo, idioma, identificador, denominacion, fechaAltaDesde, fechaAltaHasta, importeDesde, importeHasta, peticionario, tipo, estado, articulo, InstanciaMover)
    End Function
    Public Function Obtener_CarpetasSolicitudes()
        Authenticate()

        moSolicitudes = DBServer.Obtener_CarpetasSolicitudes()
        Dim oCarpetasSolicitudes As New List(Of cn_fsTreeViewItem)
        Try
            Dim oCarpetas As cn_fsTreeViewItem
            Dim idAnterior As Integer = 0
            If Not moSolicitudes Is Nothing Then
                For Each row As DataRow In moSolicitudes.Tables(0).Rows
                    If Not idAnterior = row("ID") Then
                        oCarpetas = New cn_fsTreeViewItem

                        With oCarpetas
                            .value = row("ID")
                            .text = row("DEN")
                            .nivel = 1
                            .selectable = True
                            .type = 1
                            .children = New List(Of cn_fsTreeViewItem)
                            .children = CarpetasChildren(moSolicitudes, 1, row("ID"))
                        End With
                        idAnterior = row("ID")
                        oCarpetasSolicitudes.Add(oCarpetas)
                    End If
                Next
            End If

            Return oCarpetasSolicitudes
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    Public Function Obtener_NivelesCarpetasSolicitudes(idCarpeta As Integer, iNivel As Integer)
        Authenticate()
        moSolicitudes = DBServer.Obtener_NivelesCarpetasSolicitudes(idCarpeta, iNivel)
        Try
            Return moSolicitudes
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    Private Function CarpetasChildren(ByVal dsDatos As DataSet, ByVal Nivel As Integer, ByVal Padre As String) As List(Of cn_fsTreeViewItem)
        Dim oCarpetasSolicitudes As New List(Of cn_fsTreeViewItem)
        Dim oCarpetas As cn_fsTreeViewItem
        Dim idAnterior As Integer = 0
        Try
            For Each row As DataRow In dsDatos.Tables(Nivel).Select("CSN1='" & Padre & "'", "DEN ASC")
                If Not idAnterior = row("ID") Then
                    oCarpetas = New cn_fsTreeViewItem

                    With oCarpetas
                        .value = row("ID")
                        .text = row("DEN")
                        .nivel = Nivel + 1
                        .selectable = True
                        .type = 1

                        .children = New List(Of cn_fsTreeViewItem)
                        .children = CarpetasChildrenNivel2(dsDatos, 2, row("ID"))
                    End With
                    idAnterior = row("ID")

                    oCarpetasSolicitudes.Add(oCarpetas)
                End If
            Next
            Return oCarpetasSolicitudes
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    Private Function CarpetasChildrenNivel2(ByVal dsDatos As DataSet, ByVal Nivel As Integer, ByVal Padre As String) As List(Of cn_fsTreeViewItem)
        Dim oCarpetasSolicitudes As New List(Of cn_fsTreeViewItem)
        Dim oCarpetas As cn_fsTreeViewItem
        Dim idAnterior As Integer = 0
        Try
            For Each row As DataRow In dsDatos.Tables(Nivel).Select("CSN2='" & Padre & "'", "DEN ASC")
                If Not idAnterior = row("ID") Then
                    oCarpetas = New cn_fsTreeViewItem

                    With oCarpetas
                        .value = row("ID")
                        .text = row("DEN")
                        .nivel = Nivel + 1
                        .selectable = True
                        .type = 1
                        .children = New List(Of cn_fsTreeViewItem)
                        .children = CarpetasChildrenNivel3(dsDatos, 3, row("ID"))
                    End With
                    idAnterior = row("ID")

                    oCarpetasSolicitudes.Add(oCarpetas)
                End If
            Next
            Return oCarpetasSolicitudes
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    Private Function CarpetasChildrenNivel3(ByVal dsDatos As DataSet, ByVal Nivel As Integer, ByVal Padre As String) As List(Of cn_fsTreeViewItem)
        Dim oCarpetasSolicitudes As New List(Of cn_fsTreeViewItem)
        Dim oCarpetas As cn_fsTreeViewItem
        Dim idAnterior As Integer = 0
        Try
            For Each row As DataRow In dsDatos.Tables(Nivel).Select("CSN3='" & Padre & "'", "DEN ASC")
                If Not idAnterior = row("ID") Then
                    oCarpetas = New cn_fsTreeViewItem

                    With oCarpetas
                        .value = row("ID")
                        .text = row("DEN")
                        .nivel = Nivel + 1
                        .selectable = True
                        .type = 1
                    End With
                    idAnterior = row("ID")

                    oCarpetasSolicitudes.Add(oCarpetas)
                End If
            Next
            Return oCarpetasSolicitudes
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    Public Function Obtener_TiposSolicitudes(ByVal IdClasificacion As Integer, ByVal txtClasificacion As String)
        Authenticate()

        Try
            moSolicitudes = DBServer.Obtener_TiposSolicitudes(IdClasificacion, txtClasificacion)
            Return moSolicitudes
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    ''' <summary>
    ''' Ejecuta la Notificaci�n de las solicitudes de PM en base a campos de tipo fecha.
    ''' </summary>
    ''' <remarks>Llamada desde: PM_Notificaciones/Solicitudes_NotificarExpiracion;Tiempo m�ximo: 0,2</remarks>
    Public Sub NotificarExpirados()
        Authenticate()

        Dim ds As DataSet
        ds = DBServer.ObtenerSolicitudesExpiracionInstancias()
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                Dim oNotificador As New Notificar(DBServer, mIsAuthenticated)
                Dim idInstancia As String
                Dim idAviso As String

                For Each row As DataRow In ds.Tables(0).Rows
                    idInstancia = row.Item("IDINSTANCIA").ToString
                    idAviso = row.Item("IDAVISO").ToString

                    oNotificador = New Notificar(DBServer, mIsAuthenticated)
                    oNotificador.gIdioma = ConfigurationManager.AppSettings("idioma")

                    oNotificador.NotificacionAvisoExpiracion(idInstancia, idAviso, ConfigurationManager.AppSettings("idioma"))
                    oNotificador.FuerzaFinalizeSmtpClient()
                    oNotificador = Nothing
                Next
            End If
        End If
    End Sub
End Class