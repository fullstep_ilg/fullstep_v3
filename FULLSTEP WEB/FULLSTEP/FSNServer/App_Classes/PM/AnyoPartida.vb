﻿<Serializable()>
Public Class AnyoPartida
    Inherits Security
    Private Contexto As System.Web.HttpContext
    Private moData As DataSet
    Private msPartida0 As String
    Private msPartida As String

    Public Property Partida0() As String
        Get
            Return msPartida0
        End Get
        Set(ByVal Value As String)
            msPartida0 = Value
        End Set
    End Property
    Public Property Partida() As String
        Get
            Return msPartida
        End Get
        Set(ByVal Value As String)
            msPartida = Value
        End Set
    End Property

    Public ReadOnly Property Data() As Data.DataSet
        Get
            Return moData
        End Get
    End Property
    Public Sub LoadData()
        Authenticate()

        moData = DBServer.AnyoPartida_Load(msPartida0, msPartida)
    End Sub
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
End Class
