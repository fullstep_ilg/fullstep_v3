﻿<Serializable()>
Public Class TiposPedido
    Inherits Security

    Private _dsFormasPago As DataSet
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub

    ''' <summary>
    ''' Carga en un dataset los tipos de pedido
    ''' </summary>
    ''' <param name="sIdi">Idioma en el que hay que devolver los datos</param>
    ''' <returns>Dataset con los tipos de pedido</returns>
    ''' <remarks>Llamada desde: TiposPedidoServer.aspx.vb; Tiempo máximo: 0,1 sg</remarks>
    Public Function LoadData(ByVal sIdi As String) As DataSet
        Authenticate()

        _dsFormasPago = CType(System.Web.HttpContext.Current.Cache("dsTiposPedido" & sIdi), DataSet)
        If _dsFormasPago Is Nothing Then
            _dsFormasPago = DBServer.TiposPedido_Get(sIdi)
            System.Web.HttpContext.Current.Cache.Insert("dsTiposPedido" & sIdi, _dsFormasPago, Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration, New TimeSpan(0, System.Configuration.ConfigurationManager.AppSettings("TiempoExpiracionCacheMin"), 0))
        End If

        Return _dsFormasPago
    End Function

End Class
