
<Serializable()> _
Public Class Unidad
    Inherits Security

    Private msCod As String
    Private msDen As String

    Public Property Cod() As String
        Get
            Return msCod
        End Get

        Set(ByVal Value As String)
            msCod = Value
        End Set
    End Property

    Public Property Den() As String
        Get
            Return msDen
        End Get

        Set(ByVal Value As String)
            msDen = Value
        End Set
    End Property


    ''' <summary>
    ''' Carga las unidades
    ''' </summary>
    ''' <param name="sIdi">Idioma</param>
    ''' <remarks>LLamada desde: denominacionUP.aspx; Tiempo m�ximo: 0s g.</remarks>
    Public Sub Load(Optional ByVal sIdi As String = "SPA")
        Authenticate()
        Dim data As DataSet

        data = DBServer.Unidad_Load(msCod, sIdi)

        If Not data.Tables(0).Rows.Count = 0 Then
            msCod = data.Tables(0).Rows(0).Item("COD")
            msDen = DBNullToSomething(data.Tables(0).Rows(0).Item("DEN"))
        End If

        data = Nothing
    End Sub

    Public Sub New(ByRef dbserver As FSNDataBaseServer.Root,ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
    End Sub
End Class
