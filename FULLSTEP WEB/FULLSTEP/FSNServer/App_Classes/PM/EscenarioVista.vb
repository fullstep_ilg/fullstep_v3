﻿Public Class EscenarioVista
    Private _id As Integer
    Public Property Id() As Integer
        Get
            Return _id
        End Get
        Set(ByVal value As Integer)
            _id = value
        End Set
    End Property
    Private _escenario As Integer
    Public Property Escenario() As Integer
        Get
            Return _escenario
        End Get
        Set(ByVal value As Integer)
            _escenario = value
        End Set
    End Property
    Private _nombre As String
    Public Property Nombre() As String
        Get
            Return _nombre
        End Get
        Set(ByVal value As String)
            _nombre = value
        End Set
    End Property
    Private _posicion As Integer
    Public Property Posicion() As Integer
        Get
            Return _posicion
        End Get
        Set(ByVal value As Integer)
            _posicion = value
        End Set
    End Property
    Private _defecto As Boolean
    Public Property Defecto() As Boolean
        Get
            Return _defecto
        End Get
        Set(ByVal value As Boolean)
            _defecto = value
        End Set
    End Property
    Private _orden As String
    Public Property Orden() As String
        Get
            Return _orden
        End Get
        Set(ByVal value As String)
            _orden = value
        End Set
    End Property
    Private _campos_Vista As List(Of Escenario_Campo)
    Public Property Campos_Vista() As List(Of Escenario_Campo)
        Get
            Return _campos_Vista
        End Get
        Set(ByVal value As List(Of Escenario_Campo))
            _campos_Vista = value
        End Set
    End Property
    Public Sub New()
        Campos_Vista = New List(Of Escenario_Campo)
    End Sub
End Class
