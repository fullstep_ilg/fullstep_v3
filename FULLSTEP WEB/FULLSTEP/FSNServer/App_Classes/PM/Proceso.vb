<Serializable()> _
Public Class Proceso
    Inherits Security

    Private m_lAnyo As Long
    Private m_lNumProce As Long
    Private m_sGMN1 As String
    Private m_sGMN2 As String
    Private m_sGMN3 As String
    Private m_sGMN4 As String
    Private m_sDenGMN4 As String
    Private m_sMoneda As String
    Private m_sDenMoneda As String
    Private m_dFecLimOfe As Date
    Private m_dFecNecesidad As Date
    Private m_dFecAdjudicacion As Date
    Private m_sDestCod As String
    Private m_sDestDen As String
    Private m_sFPagoCod As String
    Private m_sFPagoDen As String
    Private m_sProvActualCod As String
    Private m_sProvActualDen As String
    Private m_dFecIniSuministro As Date
    Private m_dFecFinSuministro As Date
    Private m_iDefDestino As Integer
    Private m_iDefFPago As Integer
    Private m_iDefProvActual As Integer
    Private m_iDefFecSuministro As Integer
    Private m_sDen As String
    Private m_sResp As String
    Private m_dFecAper As Date
    Private m_iEstado As Integer
    Private m_lInstancia As Long
    Private m_sEspecific As String
    Private moMateriales As DataTable
    Private moGrupos As DataTable
    Private moEspecProce As DataTable
    Private moProvAdjs As DataTable

    Public Property Anyo() As Long
        Get
            Return m_lAnyo
        End Get

        Set(ByVal Value As Long)
            m_lAnyo = Value
        End Set
    End Property
    Public Property NumProce() As Long
        Get
            Return m_lNumProce
        End Get

        Set(ByVal Value As Long)
            m_lNumProce = Value
        End Set
    End Property
    Public Property GMN1() As String
        Get
            Return m_sGMN1
        End Get

        Set(ByVal Value As String)
            m_sGMN1 = Value
        End Set
    End Property
    Public Property Den() As String
        Get
            Return m_sDen
        End Get

        Set(ByVal Value As String)
            m_sDen = Value
        End Set
    End Property
    Public Property FecAper() As Date
        Get
            Return m_dFecAper
        End Get

        Set(ByVal Value As Date)
            m_dFecAper = Value
        End Set
    End Property
    Public Property Responsable() As String
        Get
            Return m_sResp
        End Get

        Set(ByVal Value As String)
            m_sResp = Value
        End Set
    End Property
    Public Property Estado() As Integer
        Get
            Return m_iEstado
        End Get

        Set(ByVal Value As Integer)
            m_iEstado = Value
        End Set
    End Property
    Public Property Instancia() As Long
        Get
            Return m_lInstancia
        End Get

        Set(ByVal Value As Long)
            m_lInstancia = Value
        End Set
    End Property
    Public Property Materiales() As DataTable
        Get
            Return moMateriales
        End Get

        Set(ByVal Value As DataTable)
            moMateriales = Value
        End Set
    End Property
    Public Property Grupos() As DataTable
        Get
            Return moGrupos
        End Get

        Set(ByVal Value As DataTable)
            moGrupos = Value
        End Set
    End Property
    Public Property Moneda() As String
        Get
            Return m_sMoneda
        End Get

        Set(ByVal Value As String)
            m_sMoneda = Value
        End Set
    End Property
    Public Property DenMoneda() As String
        Get
            Return m_sDenMoneda
        End Get

        Set(ByVal Value As String)
            m_sDenMoneda = Value
        End Set
    End Property
    Public Property FecLimOfe() As Date
        Get
            Return m_dFecLimOfe
        End Get

        Set(ByVal Value As Date)
            m_dFecLimOfe = Value
        End Set
    End Property
    Public Property FecNecesidad() As Date
        Get
            Return m_dFecNecesidad
        End Get

        Set(ByVal Value As Date)
            m_dFecNecesidad = Value
        End Set
    End Property
    Public Property FecAdjudicacion() As Date
        Get
            Return m_dFecAdjudicacion
        End Get

        Set(ByVal Value As Date)
            m_dFecAdjudicacion = Value
        End Set
    End Property
    Public Property DefDestino() As Integer
        Get
            Return m_iDefDestino
        End Get

        Set(ByVal Value As Integer)
            m_iDefDestino = Value
        End Set
    End Property
    Public Property DefFormaPago() As Integer
        Get
            Return m_iDefFPago
        End Get

        Set(ByVal Value As Integer)
            m_iDefFPago = Value
        End Set
    End Property
    Public Property DefProvActual() As Integer
        Get
            Return m_iDefProvActual
        End Get

        Set(ByVal Value As Integer)
            m_iDefProvActual = Value
        End Set
    End Property
    Public Property DefFechasSuministro() As Integer
        Get
            Return m_iDefFecSuministro
        End Get

        Set(ByVal Value As Integer)
            m_iDefFecSuministro = Value
        End Set
    End Property
    Public Property DestinoCod() As String
        Get
            Return m_sDestCod
        End Get

        Set(ByVal Value As String)
            m_sDestCod = Value
        End Set
    End Property
    Public Property DestinoDen() As String
        Get
            Return m_sDestDen
        End Get

        Set(ByVal Value As String)
            m_sDestDen = Value
        End Set
    End Property
    Public Property FPagoCod() As String
        Get
            Return m_sFPagoCod
        End Get

        Set(ByVal Value As String)
            m_sFPagoCod = Value
        End Set
    End Property
    Public Property FPagoDen() As String
        Get
            Return m_sFPagoDen
        End Get

        Set(ByVal Value As String)
            m_sFPagoDen = Value
        End Set
    End Property
    Public Property ProvActualCod() As String
        Get
            Return m_sProvActualCod
        End Get

        Set(ByVal Value As String)
            m_sProvActualCod = Value
        End Set
    End Property
    Public Property ProvActualDen() As String
        Get
            Return m_sProvActualDen
        End Get

        Set(ByVal Value As String)
            m_sProvActualDen = Value
        End Set
    End Property
    Public Property FecIniSuministro() As Date
        Get
            Return m_dFecIniSuministro
        End Get

        Set(ByVal Value As Date)
            m_dFecIniSuministro = Value
        End Set
    End Property
    Public Property FecFinSuministro() As Date
        Get
            Return m_dFecFinSuministro
        End Get

        Set(ByVal Value As Date)
            m_dFecFinSuministro = Value
        End Set
    End Property
    Public Property Especificaciones() As String
        Get
            Return m_sEspecific
        End Get

        Set(ByVal Value As String)
            m_sEspecific = Value
        End Set
    End Property
    Public Property EspecificProce() As DataTable
        Get
            Return moEspecProce
        End Get

        Set(ByVal Value As DataTable)
            moEspecProce = Value
        End Set
    End Property
    Public Property ProveedoresAdj() As DataTable
        Get
            Return moProvAdjs
        End Get

        Set(ByVal Value As DataTable)
            moProvAdjs = Value
        End Set
    End Property
    ''' <summary>
    ''' Procedimiento que carga los datos de un proceso en el idioma del usuario
    ''' </summary>
    ''' <param name="sIdioma">Idioma del usuario</param>
    ''' <remarks>
    ''' Llamada desde: PmWeb/arachedfilesProceso/Page_load, PmWeb/detalleprocesovis/Page_load 
    ''' Tiempo m�ximo: 0,35 seg</remarks>
    Public Sub Load(ByVal sIdioma As String)
        Authenticate()

        Dim data As DataSet
        data = DBServer.Proceso_Load(m_lAnyo, m_sGMN1, m_lNumProce, m_lInstancia, sIdioma)

        If Not data.Tables(0).Rows.Count = 0 Then
            'Datos generales del proceso
            m_sDen = DBNullToSomething(data.Tables(0).Rows(0).Item("DEN"))
            m_sResp = DBNullToSomething(data.Tables(0).Rows(0).Item("RESPONSABLE"))
            m_dFecAper = DBNullToSomething(data.Tables(0).Rows(0).Item("FECAPE"))
            m_iEstado = DBNullToSomething(data.Tables(0).Rows(0).Item("EST"))
            m_sMoneda = DBNullToSomething(data.Tables(0).Rows(0).Item("MON"))
            m_sDenMoneda = DBNullToSomething(data.Tables(0).Rows(0).Item("DEN_MON"))
            m_dFecLimOfe = DBNullToSomething(data.Tables(0).Rows(0).Item("FECLIMOFE"))
            m_dFecNecesidad = DBNullToSomething(data.Tables(0).Rows(0).Item("FECNEC"))
            m_dFecAdjudicacion = DBNullToSomething(data.Tables(0).Rows(0).Item("FECPRES"))
            m_iDefDestino = data.Tables(0).Rows(0).Item("DEF_DEST")
            m_iDefFPago = data.Tables(0).Rows(0).Item("DEF_PAG")
            m_iDefProvActual = data.Tables(0).Rows(0).Item("DEF_PROVE")
            m_iDefFecSuministro = data.Tables(0).Rows(0).Item("DEF_FECSUM")
            m_sDestCod = DBNullToSomething(data.Tables(0).Rows(0).Item("DEST_COD"))
            m_sDestDen = DBNullToSomething(data.Tables(0).Rows(0).Item("DEST_DEN"))
            m_sFPagoCod = DBNullToSomething(data.Tables(0).Rows(0).Item("PAG_COD"))
            m_sFPagoDen = DBNullToSomething(data.Tables(0).Rows(0).Item("PAG_DEN"))
            m_sProvActualCod = DBNullToSomething(data.Tables(0).Rows(0).Item("PROVE_COD"))
            m_sProvActualDen = DBNullToSomething(data.Tables(0).Rows(0).Item("PROVE_DEN"))
            m_dFecIniSuministro = DBNullToSomething(data.Tables(0).Rows(0).Item("FECINI"))
            m_dFecFinSuministro = DBNullToSomething(data.Tables(0).Rows(0).Item("FECFIN"))
            m_sEspecific = DBNullToSomething(data.Tables(0).Rows(0).Item("ESP"))

            'Materiales del proceso
            moMateriales = data.Tables(1)

            'Especificaciones del proceso
            moEspecProce = data.Tables(2)

            'Proveedores adjudicados
            moProvAdjs = data.Tables(3)

            'Grupos:
            moGrupos = data.Tables(4)

        End If


        data = Nothing
    End Sub
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
    Public Function BuscarProcesos(ByVal codigoPersona As String, ByVal anyo As Nullable(Of Integer), ByVal commodity As String, ByVal codigoProceso As Nullable(Of Integer), ByVal denominacionProceso As String, _
                                   ByVal gmn1 As String, ByVal gmn2 As String, ByVal gmn3 As String, ByVal gmn4 As String, ByVal art As String, ByVal prove As String, _
                                   ByVal estados() As Integer, ByVal mon As String, ByVal solicitud As String, _
                                   ByVal presupuestoDesde As Nullable(Of Double), ByVal presupuestoHasta As Nullable(Of Double), _
                                   ByVal fechaDesde As Nullable(Of Date), ByVal fechaHasta As Nullable(Of Date), _
                                   ByVal adjudicacionDirecta As Boolean) As DataTable
        Return DBServer.Procesos_CargarProcesosParaBusqueda(codigoPersona, anyo, commodity, codigoProceso, denominacionProceso, gmn1, gmn2, gmn3, gmn4, art, prove, estados, mon, solicitud, presupuestoDesde, presupuestoHasta, fechaDesde, fechaHasta, adjudicacionDirecta)
    End Function
End Class