﻿<Serializable()>
Public Class ArticulosFavoritos
    Inherits Security

    Private moData As DataSet
    Public ReadOnly Property Data() As Data.DataSet
        Get
            Return moData
        End Get
    End Property
    ''' <summary>
    ''' Procedimiento que carga los artículos favoritos del usuario
    ''' </summary>
    ''' <param name="sUser">Codigo de usuario</param>
    ''' <remarks>Llamada desde: PmWeb/articulosfavserver.aspx/Page_Load, 
    ''' Tiempo máximo: 0,6 seg</remarks>
    Public Sub LoadData(ByVal sUser As String, Optional ByVal sGMN1 As String = "", Optional ByVal sGMN2 As String = "", Optional ByVal sGMN3 As String = "", Optional ByVal sGMN4 As String = "",
                        Optional ByVal sOrgCompras As String = "", Optional ByVal sCentro As String = "", Optional ByVal sUON1 As String = "", Optional ByVal sUON2 As String = "",
                        Optional ByVal sUON3 As String = "", Optional sProveSumiArt As String = "", Optional ByVal bSoloArtMAT As Boolean = False)
        Authenticate()
        moData = DBServer.Articulos_FavoritosLoad(sUser, sGMN1, sGMN2, sGMN3, sGMN4, sOrgCompras, sCentro, sUON1, sUON2, sUON3, sProveSumiArt, bSoloArtMAT)
    End Sub

    ''' <summary>
    ''' Procedimiento que inserta un nuevo artículo favorito para el usuario dado
    ''' </summary>
    ''' <param name="sUser">Código de usuario</param>
    ''' <param name="sArt">Código del artículo</param>
    ''' <remarks>
    ''' Llamada desde: FSNWEB/APP_pages/_Common/BuscadorArticulos
    ''' Tiempo máximo: 0,5 seg</remarks>
    Public Sub Insertar(ByVal sUser As String, ByVal sArt As String)
        Authenticate()
        DBServer.ArticuloFavorito_Insertar(sUser, sArt)
    End Sub

    ''' <summary>
    ''' Procedimiento que elimina un artículo favorito del usuario
    ''' </summary>
    ''' <param name="sUser">Codigo de usuario</param>
    ''' <param name="sArt">Codigo de artículo</param>
    ''' <remarks>
    ''' Llamada desde: FSNWEB/APP_pages/_Common/BuscadorArticulos
    ''' Tiempo máximo: 0,3 seg</remarks>
    Public Sub Eliminar(ByVal sUser As String, ByVal sArt As String)
        Authenticate()
        DBServer.ArticuloFavorito_Eliminar(sUser, sArt)
    End Sub

    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
End Class
