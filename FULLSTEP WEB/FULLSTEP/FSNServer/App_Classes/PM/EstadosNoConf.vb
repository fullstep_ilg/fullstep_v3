<Serializable()> _
    Public Class EstadosNoConf

    Inherits Security
    Private moEstados As DataSet

    Public ReadOnly Property Data() As Data.DataSet
        Get
            Return moEstados
        End Get

    End Property

    ''' <summary>
    ''' Procedimiento que carga los Estados de no conformidad
    ''' </summary>
    ''' <param name="lInstancia">Instancia</param>
    ''' <param name="sDen">Denominaci�n del estado</param>
    ''' <param name="sIdi">Idioma de la aplicaci�n</param>
    ''' <remarks>Llamada desde: PmWeb\Desglose\Page_Load - CargarValoresDefecto, PmWeb\estadoserver\Page_Load,Tiempo m�ximo: 0,25 seg</remarks>
    Public Sub LoadData(ByVal lInstancia As Long, Optional ByVal sDen As String = Nothing, Optional ByVal sIdi As String = Nothing)
        Authenticate()
        moEstados = DBServer.EstadosNoConf_Get(lInstancia, sDen, sIdi)
    End Sub

    Public Sub New(ByRef dbserver As FSNDataBaseServer.Root,ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
    End Sub

End Class
