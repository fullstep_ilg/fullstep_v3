<Serializable()> _
    Public Class Formulario
    Inherits Security
    Private mlId As Long
    Private msCod As String
    Private moGrupos As Grupos

    Property Id() As Long
        Get
            Id = mlId
        End Get
        Set(ByVal Value As Long)
            mlId = Value
        End Set
    End Property
    Property Cod() As String
        Get
            Cod = msCod
        End Get
        Set(ByVal Value As String)
            msCod = Value
        End Set
    End Property
    Property Grupos() As Grupos
        Get
            Grupos = moGrupos
        End Get
        Set(ByVal Value As Grupos)
            moGrupos = Value
        End Set
    End Property
    ''' <summary>
    ''' Revisado por: Sandra. Fecha: 14/03/2011
    ''' Carga el formulario en el alta de solicitudes/no conformidades o certificados
    ''' </summary>
    ''' <param name="sIdi">Idioma</param>
    ''' <param name="lSolicitud">Id de la solicitud</param>
    ''' <param name="lBloque">Id del bloque</param>
    ''' <param name="sPer">Cod de la persona</param>
    ''' <param name="bFavorito">Si viene de una solicitud favorita o no</param>
    ''' <param name="sFormatoFecha">Formato de fechas del usuario</param>
    ''' <param name="iTipoWorkflow">Tipo de flujo (0:alta/1: modificacion/2: baja)</param>
    ''' <remarks>Llamada desde: NWAlta.aspx, alta.vb, alta.aspx --> Load; Tiempo m�ximo: 0,2 sg</remarks>
    Public Sub Load(ByVal sIdi As String, Optional ByVal lSolicitud As Long = Nothing, Optional ByVal lBloque As Long = Nothing,
                    Optional ByVal sPer As String = Nothing, Optional ByVal bFavorito As Boolean = False, Optional ByVal lFavorito As Long = 0,
                    Optional ByVal sFormatoFecha As String = Nothing, Optional ByVal iTipoWorkflow As Integer = 0, Optional ByVal InstanciaImportar As Long = 0)
        Authenticate()
        Dim data As DataSet
        Dim oFormRow, oGroupRow As DataRow
        Dim oGrupo As Grupo

        data = DBServer.Formulario_Load(mlId, sIdi, lSolicitud, lBloque, sPer, bFavorito, lFavorito, sFormatoFecha, iTipoWorkflow, InstanciaImportar)
        If Not data.Tables(0).Rows.Count = 0 Then
            For Each oFormRow In data.Tables(0).Rows
                msCod = DBNullToSomething(oFormRow.Item("COD").ToString)
                moGrupos = New Grupos(DBServer, mIsAuthenticated)

                For Each oGroupRow In oFormRow.GetChildRows("REL_FORM_GRUPO")
                    oGrupo = New Grupo(DBServer, mIsAuthenticated)
                    oGrupo.Id = oGroupRow.Item("ID")
                    oGrupo.NumCampos = oGroupRow.Item("NUMCAMPOS")
                    oGrupo.Den(sIdi) = oGroupRow.Item("DEN_" & sIdi).ToString
                    oGrupo.DSCampos = New DataSet
                    oGrupo.DSCampos.Merge(oGroupRow.GetChildRows("REL_GRUPO_CAMPO"))
                    oGrupo.DSCampos.Merge(oGroupRow.GetChildRows("REL_GRUPO_LISTA"))

                    If oGrupo.DSCampos.Tables.Count > 1 Then
                        oGrupo.DSCampos.Relations.Add("REL_CAMPO_LISTA", oGrupo.DSCampos.Tables(0).Columns("ID"), oGrupo.DSCampos.Tables(1).Columns("CAMPO"), False)
                    End If

                    oGrupo.DSCampos.Merge(oGroupRow.GetChildRows("REL_GRUPO_ADJUNTO"))

                    If oGrupo.DSCampos.Tables.Count > 2 Then
                        oGrupo.DSCampos.Relations.Add("REL_CAMPO_ADJUN", oGrupo.DSCampos.Tables(0).Columns("ID"), oGrupo.DSCampos.Tables(2).Columns("CAMPO"), False)
                    ElseIf (oGrupo.DSCampos.Tables.Count = 2 And oGrupo.DSCampos.Relations.Count = 0) Then
                        oGrupo.DSCampos.Relations.Add("REL_CAMPO_ADJUN", oGrupo.DSCampos.Tables(0).Columns("ID"), oGrupo.DSCampos.Tables(1).Columns("CAMPO"), False)
                    End If

                    Dim aux, aux2 As Integer
                    aux = oGrupo.DSCampos.Tables.Count()
                    oGrupo.DSCampos.Merge(oGroupRow.GetChildRows("REL_CAMPO_BLOQUEO"))
                    aux2 = oGrupo.DSCampos.Tables.Count()
                    If aux < aux2 Then
                        oGrupo.DSCampos.Relations.Add("REL_CAMPO_BLOQUEO", oGrupo.DSCampos.Tables(0).Columns("ID"), oGrupo.DSCampos.Tables(aux).Columns("CAMPO"), False)
                    End If

                    moGrupos.Add(oGrupo)
                Next
            Next
        End If
        data = Nothing
    End Sub
    ''' <summary>
    ''' Funcion que devuelve los datos de campos calculados
    ''' </summary>
    ''' <param name="lIdSolicitud">Id de la solicitud</param>
    ''' <param name="sPer">Codigo de persona</param>
    ''' <returns>Un Dataset con los datos de los campos calculados</returns>
    ''' <remarks>Llamada desde: PmWeb/guardarinstancia/GuardarConWorkFlow - GuardarSinWorkFlow - GenerarDataSetYHacercomprobaciones, PmWeb/alta/GuardarSolicitud, PmWeb/ayudacampo/Page_load, PmWeb/recalcularimportes/Page_load, 
    ''' Tiempo m�ximo: 0,432 seg</remarks>
    Public Function LoadCamposCalculados(ByVal lIdSolicitud As Long, Optional ByVal sPer As String = Nothing) As DataSet
        Authenticate()
        Dim data As DataSet
        data = DBServer.Formulario_LoadCamposCalculados(mlId, lIdSolicitud, sPer)
        Return data
    End Function
    ''' <summary>
    ''' Funcion que devuelve los datos de cargar el desglose del padre de un campo
    ''' </summary>
    ''' <param name="lIdSolicitud">Id de la solicitud</param>
    ''' <param name="lIdCampo">Id del campo del cual cargar el padre</param>
    ''' <param name="sPer">Codigo de persona</param>
    ''' <returns>Un data set con el desglose del padre del campo dado</returns>
    ''' <remarks>
    ''' Llamada desde: PmWeb/recalcularimportes/PAge_Load, PmWeb/guardarInstancia/GenerarDataSetYHacerComprobaciones
    ''' Tiempo m�ximo: 0,35 seg</remarks>
    Public Function LoadDesglosePadreVisible(ByVal lIdSolicitud As Long, ByVal lIdCampo As Integer, Optional ByVal sPer As String = Nothing) As DataSet
        Authenticate()
        Dim data As DataSet
        data = DBServer.Formulario_LoadDesglosePadreVisible(mlId, lIdCampo, Nothing, lIdSolicitud, sPer)
        Return data
    End Function
    ''' <summary>
    ''' Funcion que nos indica si el formulario tiene o no un campo de tipo Importe
    ''' </summary>
    ''' <returns>True/false si dispone o no de campo importe en el formulario</returns>
    ''' <remarks>
    ''' Llamada desde: PmWeb/recalcularimportes/PAge_Load, PmWeb/guardarInstancia/GenerarDataSetYHacerComprobaciones
    ''' Tiempo m�ximo: 0,35 seg</remarks>
    Public Function ConCampoImporte() As Object
        Authenticate()
        Return DBServer.Formulario_ConCampoImporte(mlId)
    End Function
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)

    End Sub
End Class