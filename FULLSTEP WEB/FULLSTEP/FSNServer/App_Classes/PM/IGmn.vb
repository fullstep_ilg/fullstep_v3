﻿Public Interface IGmn
    Property gmn1Cod As String
    Property gmn2Cod As String
    Property gmn3Cod As String
    Property gmn4Cod As String
    Property Den As String
    Property GmnQAcod As Integer
    ''' <summary>
    ''' Titulo de la familia de material : cod1 - cod2 --- codN - den
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function title() As String
    ReadOnly Property key() As String
    ''' <summary>
    ''' colección de hijos
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Property children() As Dictionary(Of String, IGmn)
    ''' <summary>
    ''' busca un hijo según clave, si no lo encuentra busca en sus descendientes recursivamente
    ''' </summary>
    ''' <param name="key"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function child(key As IGmn) As IGmn
    ''' <summary>
    ''' Indica si el nodo tiene descendientes
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    ReadOnly Property hasChildren() As Boolean
    ''' <summary>
    ''' Devuelve el elemento superior de esta familia de materiales
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function topParent() As IGmn

    Function nivel() As Integer
    ''' <summary>
    ''' Indica si la familia de materiales contiene (es ascendiente ) o es ella misma, de la pasada por parámetro
    ''' </summary>
    ''' <param name="oGmn"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function incluye(oGmn As IGmn) As Boolean
    ''' <summary>
    ''' Indica si la familia de materiales contiene (es ascendiente )  de la pasada por parámetro
    ''' </summary>
    ''' <param name="oGmn"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function contiene(oGmn As IGmn) As Boolean
    ''' <summary>
    ''' Familia padre
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Property parent As IGmn
    ''' <summary>
    ''' Estructura de materiales a la que está asociada la familia
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Property Estructura As EstructuraMateriales
    Property User As FSNServer.User
End Interface
