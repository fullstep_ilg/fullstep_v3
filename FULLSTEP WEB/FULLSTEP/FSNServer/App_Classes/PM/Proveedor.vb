<Serializable()> _
        Public Class Proveedor
    Inherits Security

#Region "Properties"
    Private msCod As String
    Private msDen As String
    Private msDir As String
    Private msCP As String
    Private msPob As String
    Private msPaiCod As String
    Private msPaiDen As String
    Private msProviCod As String
    Private msProviDen As String
    Private msMonCod As String
    Private msMonDen As String
    Private msObs As String
    Private msNIF As String
    Private msUrl As String
    Private moContactos As DataSet
    Private moVariables As DataSet
    Private mdPuntuacion As Double
    Private mbPotencial As Short
    Private mbBajaQA As Boolean
    Private msEmail As String 'Email del proveedor en QA se utiliza contacto de calidad
    Private moData As DataSet
    Private moUsuarios_Categorias As DataSet

    Public ReadOnly Property Data() As Data.DataSet
        Get
            Return moData
        End Get
    End Property
    Property Cod() As String
        Get
            Cod = msCod
        End Get
        Set(ByVal Value As String)
            msCod = Value
        End Set
    End Property
    Property Den() As String
        Get
            Den = msDen
        End Get
        Set(ByVal Value As String)
            msDen = Value
        End Set
    End Property
    Property Dir() As String
        Get
            Dir = msDir
        End Get
        Set(ByVal Value As String)
            msDir = Value
        End Set
    End Property
    Property CP() As String
        Get
            CP = msCP
        End Get
        Set(ByVal Value As String)
            msCP = Value
        End Set
    End Property
    Property Pob() As String
        Get
            Pob = msPob
        End Get
        Set(ByVal Value As String)
            msPob = Value
        End Set
    End Property
    Property PaiCod() As String
        Get
            PaiCod = msPaiCod
        End Get
        Set(ByVal Value As String)
            msPaiCod = Value
        End Set
    End Property
    Property PaiDen() As String
        Get
            PaiDen = msPaiDen
        End Get
        Set(ByVal Value As String)
            msPaiDen = Value
        End Set
    End Property
    Property ProviCod() As String
        Get
            ProviCod = msProviCod
        End Get
        Set(ByVal Value As String)
            msProviCod = Value

        End Set
    End Property
    Property ProviDen() As String
        Get
            ProviDen = msProviDen
        End Get
        Set(ByVal Value As String)
            msProviDen = Value
        End Set
    End Property
    Property MonCod() As String
        Get
            MonCod = msMonCod
        End Get
        Set(ByVal Value As String)
            msMonCod = Value
        End Set
    End Property
    Property MonDen() As String
        Get
            MonDen = msMonDen
        End Get
        Set(ByVal Value As String)
            msMonDen = Value
        End Set
    End Property
    Property Obs() As String
        Get
            Obs = msObs
        End Get
        Set(ByVal Value As String)
            msObs = Value
        End Set
    End Property
    Property NIF() As String
        Get
            NIF = msNIF
        End Get
        Set(ByVal Value As String)
            msNIF = Value
        End Set
    End Property
    Property Url() As String
        Get
            Url = msUrl
        End Get
        Set(ByVal Value As String)
            msUrl = Value
        End Set
    End Property
    Public Property Contactos() As DataSet
        Get
            Return moContactos
        End Get
        Set(ByVal Value As DataSet)
            moContactos = Value
        End Set
    End Property
    Public Property UsuariosyCategorias() As DataSet
        Get
            Return moUsuarios_Categorias
        End Get
        Set(ByVal Value As DataSet)
            moUsuarios_Categorias = Value
        End Set
    End Property
    Public Property Variables() As DataSet
        Get
            Return moVariables
        End Get
        Set(ByVal Value As DataSet)
            moVariables = Value
        End Set
    End Property
    Property Puntuacion() As Double
        Get
            Puntuacion = mdPuntuacion
        End Get
        Set(ByVal Value As Double)
            mdPuntuacion = Value
        End Set
    End Property
    Property Potencial() As Short
        Get
            Potencial = mbPotencial
        End Get
        Set(ByVal Value As Short)
            mbPotencial = Value
        End Set
    End Property
    Property BajaQA() As Boolean
        Get
            BajaQA = mbBajaQA
        End Get
        Set(ByVal Value As Boolean)
            mbBajaQA = Value
        End Set
    End Property
    Property Email() As String
        Get
            Email = msEmail
        End Get
        Set(ByVal Value As String)
            msEmail = Value
        End Set
    End Property
#End Region
    ''' <summary>
    ''' Procedimiento que carga un proveedor dado un código y un idioma especifico
    ''' </summary>
    ''' <param name="sIdi">Idioma de la aplicacion</param>
    ''' <param name="SoloTablaProveedor">Si solo necesitamos los datos de la tabla proveedor (y no la denominacion de pais, provincia y moneda)
    ''' para que la consulta vaya mas rapido</param>
    ''' <remarks>
    ''' Llamada desde:FsnWeb/Consultas.asmx/Mainthread,  /PmServer/Proveedor/ , /PmServer/detalleProveedor/Page_load , /PmServer/guardarinstancia/Page_load , PmServer/campos/Page_load , 
    '''PmServer/desglose/Page_load - CargarValoresDefecto , PmServer/impexp/DenGS , PmServer/trasladarUsu/Page_load , PmServer/trasladoOk/Page_load , 
    ''' Tiempo máximo: 0,34 seg
    ''' </remarks>
    Public Sub Load(ByVal sIdi As String, Optional SoloTablaProveedor As Boolean = False)
        Authenticate()

        Dim oDS As DataSet
        oDS = DBServer.Proveedor_Get(sIdi, msCod, SoloTablaProveedor)
        If Not oDS.Tables(0).Rows.Count = 0 Then
            With oDS.Tables(0).Rows(0)
                msCod = .Item("COD")
                msDen = .Item("DEN")
                msDir = DBNullToSomething(.Item("DIR"))
                msCP = DBNullToSomething(.Item("CP"))
                msPob = DBNullToSomething(.Item("POB"))
                msPaiCod = DBNullToSomething(.Item("PAICOD"))
                If Not SoloTablaProveedor Then msPaiDen = DBNullToSomething(.Item("PAIDEN"))
                msProviCod = DBNullToSomething(.Item("PROVICOD"))
                If Not SoloTablaProveedor Then msProviDen = DBNullToSomething(.Item("PROVIDEN"))
                msMonCod = DBNullToSomething(.Item("MONCOD"))
                If Not SoloTablaProveedor Then msMonDen = DBNullToSomething(.Item("MONDEN"))
                msObs = DBNullToSomething(.Item("OBS"))
                msNIF = DBNullToSomething(.Item("NIF"))
                msUrl = DBNullToSomething(.Item("URLPROVE"))
            End With
        End If

        oDS = Nothing
    End Sub
    ''' <summary>
    ''' Obtiene los datos del proveedor, a diferencia del stored FSWS_PROVE, obtiene tb el mail del contacto de calidad del proveedor.
    ''' Carga el las variables de la clase los datos obtenidos en el stored
    ''' </summary>
    ''' <param name="sIdi">Codigo idioma</param>   
    ''' <remarks>Llamada desde:= (Consultas.asmx.vb --> Obtener_DatosProveedorQA) ; Tiempo máximo=0,2seg.</remarks>
    Public Sub LoadDatosQA(ByVal sIdi As String)
        Authenticate()

        Dim oDS As DataSet
        oDS = DBServer.Proveedor_Get_QA(sIdi, msCod)
        If Not oDS.Tables(0).Rows.Count = 0 Then
            With oDS.Tables(0).Rows(0)
                msCod = .Item("COD")
                msDen = .Item("DEN")
                msDir = DBNullToSomething(.Item("DIR"))
                msCP = DBNullToSomething(.Item("CP"))
                msPob = DBNullToSomething(.Item("POB"))
                msPaiCod = DBNullToSomething(.Item("PAICOD"))
                msPaiDen = DBNullToSomething(.Item("PAIDEN"))
                msProviCod = DBNullToSomething(.Item("PROVICOD"))
                msProviDen = DBNullToSomething(.Item("PROVIDEN"))
                msMonCod = DBNullToSomething(.Item("MONCOD"))
                msMonDen = DBNullToSomething(.Item("MONDEN"))
                msObs = DBNullToSomething(.Item("OBS"))
                msNIF = DBNullToSomething(.Item("NIF"))
                msUrl = DBNullToSomething(.Item("URLPROVE"))
                msEmail = DBNullToSomething(.Item("EMAIL"))
            End With
        End If

        oDS = Nothing
    End Sub
    ''' <summary>
    ''' Procedimiento que carga los contactos de un proveedor
    ''' </summary>
    ''' <param name="bSoloPortal">Variable booleana que indica si solo se deben cargar los contactos del portal</param>
    ''' <remarks>
    ''' Llamada desde: PmWeb/GuardarInstancia/trasladarUsu, PmWeb/trasladarUsu/Page_Load, PmWeb/trasladoOk/Page_Load
    ''' Tiempo máximo: 0,45 seg</remarks>
    Public Sub CargarContactos(Optional ByVal bSoloPortal As Boolean = False)
        Authenticate()
        moContactos = DBServer.Proveedor_CargarContactos(msCod, bSoloPortal)
    End Sub
    ''' <summary>
    ''' Procedimiento que carga
    '''           La moneda del proveedor       Tabla 0
    '''           los contactos de un proveedor Tabla 1
    '''           las categorias y costes/hora  Tabla 2
    ''' </summary>
    ''' <param name="sIdi">Codigo Idioma</param>
    ''' <remarks>
    ''' Llamada desde: PmWeb/GuardarInstancia/trasladarUsu, PmWeb/trasladarUsu/Page_Load, PmWeb/trasladoOk/Page_Load
    ''' Tiempo máximo: 0,35 seg</remarks>
    Public Sub CargarContactosyCategorias(ByVal sIdi As String)
        Authenticate()

        moUsuarios_Categorias = DBServer.Proveedor_CargarContactosyCategorias(msCod, sIdi)
        msMonCod = DBNullToStr(moUsuarios_Categorias.Tables(0).Rows(0).Item(0))
    End Sub
    ''' <summary>Procedimiento que carga los contactos de QA</summary>
    ''' <param name="bSoloPortal">Variable booleana que indica si los contactos a cargar son solo de portal</param>
    ''' <param name="bSoloQA">Variable booleana que indica si los contactos a cargar son solo de QA</param>
    ''' <remarks>Llamada desde: PmWeb/Contactos/PagE_load,PmWeb/ContactosServer2/PagE_load</remarks>
    Public Sub CargarContactosQA(Optional ByVal bSoloPortal As Boolean = False, Optional ByVal bSoloQA As Boolean = False)
        Authenticate()
        moContactos = DBServer.Proveedor_CargarContactos_QA(msCod, bSoloPortal, bSoloQA)
    End Sub
    ''' <summary>Procedimiento que asigna contactos de QA</summary>
    ''' <param name="Contactos">Array con los contactos a asignar</param>
    ''' <remarks>Llamada desde: PmWeb/Contactos/AsignarContactosQA</remarks>
    Public Sub AsignarContactosQA(ByVal Contactos As Integer())
        Authenticate()
        DBServer.Proveedor_AsignarContactosQA(msCod, Contactos)
    End Sub

    ''' <summary>
    ''' Función que devuelve un dataset con las puntuaciones de las variables de calidad de un proveedor para las unidades de negocio a las que pertenece
    ''' </summary>
    ''' <param name="sProve">Codigo Proveedor</param>
    ''' <param name="sUsu">Codigo Usuario</param>
    ''' <param name="sUNQAs">Lista de ids de las unqas del proveedor</param>
    ''' <param name="sVarCal">Lista de las variables de calidad que afectan al proveedor</param>
    ''' <param name="sIdi">Idioma del usuario</param>
    ''' <returns>Dataset con las puntuaciones del proveedor para las unidades de negoico en cada variable</returns>
    ''' <remarks>Llamadas: PMWeb/script/puntuacion/panelCalidad.aspx.vb</remarks>
    Public Function DevolverFichaCalidad(ByVal sProve As String, ByVal sUsu As String, ByVal sUNQAs As String, ByVal sVarCal As String, Optional ByVal sIdi As String = "SPA") As DataSet
        Authenticate()
        Return DBServer.Proveedor_DevolverFichaCalidad(sProve, sUsu, sUNQAs, sVarCal, sIdi)
    End Function
    ''' <summary>
    ''' Función que devuleve un dataset que contiene en una sola tabla todas los materiales de QA y GS de un proveedor asociados a los materiales de GS
    ''' _hds: El dataset se cargará posteriormente en un HierarchicalDataSet. Para que pueda establecer la jerarquia la tabla del dataset tiene un campo que es la PK y otro que es la FK.
    ''' </summary>
    ''' <param name="sProve">Código del proveedor</param>
    ''' <param name="sMaterialesQA">Lista de materiales de QA del proveedor</param>
    ''' <param name="sIdi">Idioma del usuario</param>
    ''' <param name="bEtiq">Si la primera linea es el material qa (0) o una etiqueta 'Materiales de GS' (1)</param>
    ''' <returns>Dataset con una jerarquia de materiales de QA y GS del proveedor</returns>
    ''' <remarks>Llamadas: PMWeb/script/puntuacion/panelCalidad.aspx.vb</remarks>
    Public Function DevolverMaterialesGS_hds(ByVal sProve As String, ByVal sMaterialesQA As String, Optional ByVal sIdi As String = "SPA", Optional ByVal bEtiq As Boolean = False) As DataSet
        Authenticate()
        Return DBServer.Proveedor_DevolverMaterialesGS_hds(sProve, sMaterialesQA, sIdi, bEtiq)
    End Function
    Public Function DevolverMaterialesQA(ByVal sProve As String, Optional ByVal sIdi As String = "SPA") As DataTable
        Authenticate()
        Return DBServer.Proveedor_DevolverMaterialesQA(sProve, sIdi)
    End Function
    Public Function DevolverUNQAs(ByVal sProve As String, Optional ByVal sIdi As String = "SPA") As DataTable
        Authenticate()
        Return DBServer.Proveedor_DevolverUNQAs(sProve, sIdi)
    End Function
    ''' <summary>
    ''' Cargar en la coleccion moVariables las variables de calidad para el calculo de las puntuaciones
    ''' </summary>
    ''' <remarks>Llamada desde=QA_puntuaciones.vb-->CalcularPuntuaciones; 
    '''         Tiempo máximo=1seg</remarks>
    Public Sub CargarVariablesCalculo(Optional ByVal IdPyme As Integer = 0)
        Authenticate()
        moVariables = DBServer.Proveedor_VariablesCalc(msCod, IdPyme)
    End Sub
    ''' <summary>
    ''' Cargar en la coleccion moVariables las variables de calidad para la simulacion
    ''' </summary>
    ''' <param name="lUNQA">Unidad de negocio</param>
    ''' <remarks>Llamada desde=QA_puntuaciones.vb-->CalcularPuntuaciones; 
    '''         Tiempo máximo=1seg.</remarks>
    Public Sub CargarVariablesSimulacion(ByVal lUNQA As Long)
        Authenticate()
        moVariables = DBServer.Proveedor_VariablesSimulacion(msCod, lUNQA)
    End Sub
    ''' <summary>
    ''' Almacena las puntuaciones de las variables de calidad del proveedor y devuelve true o false 
    ''' en función de si se ha guardado el histórico o no
    ''' </summary>
    ''' <returns>true o false en función de si se ha guardado el histórico o no</returns>
    ''' <remarks>Llamada desde=QA_puntuaciones.vb-->CalcularPuntuaciones; 
    '''         Tiempo máximo=6segseg.</remarks>
    Public Function GuardarVariables() As Boolean
        Authenticate()
        Return DBServer.Proveedor_GuardarVariables(moVariables, msCod)
    End Function
    ''' <summary>
    ''' Almacena las puntuaciones que han variado en la simulacion
    ''' </summary>
    ''' <param name="dsVariablesSimulacion">Dataset con las Variables que han cambiado</param>
    ''' <remarks>Llamada desde=QA_puntuaciones.vb-->EjecutarSimularPuntuacion; 
    '''         Tiempo máximo=3seg.</remarks>
    Public Sub GuardarVariablesSimulacion(ByVal dsVariablesSimulacion As DataSet)
        Authenticate()
        DBServer.Proveedor_GuardarVariablesSimulacion(dsVariablesSimulacion)
    End Sub
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
    ''' <summary>
    ''' Este método obtiene de BD un dataset, usando DBServer.Proveedor_VerSiCambioCalificaciónTotal, con 
    ''' tantos registros como unidades de negocio en las que se cumpla que la calificación de la variable 
    ''' TOTAL DEL PROVEEDOR es distinta en los 2 últimos meses. Si hay registros entonces llamamos al 
    ''' Notificador pasándole el proveedor y dataset con las calificaciones que han variado.
    ''' El dataset tambien contiene las traducciones multiidioma de unqa, meses, calificaciones 
    ''' y texto 'calificación total actual/anterior'.
    ''' </summary>
    ''' <remarks>Llamada desde: QA_Puntuaciones/NotificaciónCambioCalificaciónTotal; Tiempo máximo:0,1</remarks>
    Public Sub NotificacionCambioCalificacionTotal()
        Authenticate()

        moData = DBServer.Proveedor_VerSiCambioCalificaciónTotal(msCod)

        'Notificar si hay datos
        If moData.Tables(0).Rows.Count > 0 Then
            Dim oNotificador As New Notificar(DBServer, mIsAuthenticated)

            oNotificador.NotificaciónCambioCalificaciónTotalProveedor(msCod, moData)
            oNotificador.FuerzaFinalizeSmtpClient()
            oNotificador = Nothing
        End If
    End Sub
    ''' <summary>
    ''' Devuelve los datos del error producido en el cálculo de puntuaciones para el proveedor seleccionado
    ''' </summary>
    ''' <param name="sProve">El código del proveedor</param>
    ''' <param name="sIdi">El idioma</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function DevolverDetalleErrorProveedor(ByVal sProve As String, ByVal sUsu As String, Optional ByVal sIdi As String = "SPA") As DataSet
        Authenticate()
        Return DBServer.Proveedor_DevolverDetalleError(sProve, sUsu, sIdi)
    End Function
    ''' <summary>Devuelve si el campo de proveedor en ERP debe estar visible</summary>
    ''' <param name="iUNQA">Id UNQA</param>
    ''' <returns>booleano indicando la visibilidad</returns>
    ''' <remarks>Llamada desde: altaNoconformidad.aspx.vb</remarks>
    Public Function VisibilidadProveERP_NC(ByVal iUNQA As Integer) As DataSet
        Authenticate()
        Return DBServer.Proveedor_VisibilidadProveERP_NC(iUNQA)
    End Function
End Class