<Serializable()>
Public Class PresProyectosNivel1
    Inherits Security
    Private moData As DataSet
    Public ReadOnly Property Data() As Data.DataSet
        Get
            Return moData
        End Get
    End Property
    ''' <summary>
    ''' Procedimiento que carga los datos de los presupuestos de los proyectos dados los identificadores de los niveles presupuestarios
    ''' </summary>
    ''' <param name="iPres1">Identificador del presupuesto, nivel 1</param>
    ''' <param name="iPres2">Identificador del presupuesto, nivel 2</param>
    ''' <param name="iPres3">Identificador del presupuesto, nivel 3</param>
    ''' <param name="iPres4">Identificador del presupuesto, nivel 4</param>
    ''' <remarks>
    ''' Llamada desde: PmWeb/PresAsig/Page_load, PmWeb/presupuestos/Page_load, PmWeb/campos/Page_load, PmWeb/desglose/Page_load - cargarValoresDefecto 
    ''' Tiempo m�ximo: 0,43 seg</remarks>
    Public Sub LoadData(ByVal iPres1 As Integer, Optional ByVal iPres2 As Integer = Nothing, Optional ByVal iPres3 As Integer = Nothing, Optional ByVal iPres4 As Integer = Nothing)
        Authenticate()
        moData = DBServer.PresProyectos_Get(iPres1, iPres2, iPres3, iPres4)
    End Sub
    ''' <summary>
    ''' Procedimiento que carga el detalle de un presupuesto dados los datos que vengan de la entrada
    ''' </summary>
    ''' <param name="iTipo">Tipo del presupuesto</param>
    ''' <param name="iNivel">Nivel de detalle a cargar</param>
    ''' <param name="lIdPres">Identificador del presupuesto</param>
    ''' <remarks>
    ''' Llamada desde: PmWeb/presupuestosreadOnly/Page_load
    ''' Tiempo m�ximo: 0,35 seg</remarks>
    Public Sub DetallePresupuesto(ByVal iTipo As Integer, ByVal iNivel As Integer, ByVal lIdPres As Long)
        Authenticate()
        moData = DBServer.DetallePresupuesto(iTipo, iNivel, lIdPres)
    End Sub
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
    Public Function GetNivelPres1() As Integer
        Authenticate()
        Return DBServer.GetNivelPres(1)
    End Function
End Class