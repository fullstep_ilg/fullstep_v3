Imports System.Configuration
<Serializable()>
Partial Public Class Adjunto
    Inherits Security

    Private msNom As String
    Private msComent As String
    Private msRuta As String
    Private msIdioma As String
    Private mdtFecAlta As Date
    Private msPer As String

    Property Nom() As String
        Get
            Nom = msNom
        End Get
        Set(ByVal Value As String)
            msNom = Value
        End Set
    End Property
    Property Coment() As String
        Get
            Return msComent
        End Get
        Set(ByVal Value As String)
            msComent = Value
        End Set
    End Property
    Property Per() As String
        Get
            Return msPer

        End Get
        Set(ByVal Value As String)
            msPer = Value
        End Set
    End Property

    ''' <summary>
    ''' Carga los archivos adjuntos dado su id
    ''' </summary>
    ''' <remarks>
    ''' Llamada desde: PMWeb/Espec.aspx/PAge_Load
    ''' Tiempo m�ximo: 0,2 seg</remarks>
    Public Sub Load()
        Authenticate()
        Dim data As DataSet
        Dim oRow As DataRow

        data = DBServer.Adjunto_Load(mlId)

        oRow = data.Tables(0).Rows(0)
        mlId = oRow.Item("ID")
        msNom = oRow.Item("NOM")
        msComent = DBNullToSomething(oRow.Item("COMENT"))
        mlDataSize = oRow.Item("DATASIZE")

    End Sub
    ''' <summary>
    ''' Guarda un archivo adjunto en el disco del usuario
    ''' </summary>
    ''' <param name="iTipo">Tipo del archivo adjunto a guardar</param>
    ''' <param name="bInstancia">Booleano que indica si viene de portal</param>
    ''' <returns>un string con el tama�o del adjunto</returns>
    ''' <remarks>
    ''' Llamada desde: PMWEB/Atach.aspx/Page_Load,PMWEB/Campos/ObtenerPAthAdjunto,PMWEB/desglose/ObtenerPAthAdjunto,PMWEB/detallesolicConsulta/ObtenerPAthAdjunto,PMWEB/Espec/Page_Load
    ''' Tiempo m�ximo: Depende del tama�o, pero alrededor de 1 seg</remarks>
    Public Function SaveAdjunToDisk(ByVal iTipo As Integer, Optional ByVal bInstancia As Boolean = False) As String
        Dim sPath As String = ConfigurationManager.AppSettings("temp") + "\" + modUtilidades.GenerateRandomPath()

        If Not System.IO.Directory.Exists(sPath) Then System.IO.Directory.CreateDirectory(sPath)
        sPath += "\" + msNom

        Dim byteBuffer() As Byte

        If bInstancia Then
            byteBuffer = DBServer.Adjunto_InstReadData(mlId, iTipo)
        Else
            byteBuffer = DBServer.Adjunto_ReadData(mlId, iTipo)
        End If
        System.IO.File.WriteAllBytes(sPath, byteBuffer)

        Return sPath
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="iTipo"></param>
    ''' <param name="sPath"></param>
    ''' <param name="bInstancia"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function SaveAdjunToDiskZip(ByVal iTipo As Integer, ByVal sPath As String, Optional ByVal bInstancia As Boolean = False) As String
        Dim sFolderPath As String
        If Not System.IO.Directory.Exists(sPath) Then System.IO.Directory.CreateDirectory(sPath)

        sFolderPath = sPath
        sPath += "\" + msNom
        Dim sFileName As String = System.IO.Path.GetFileNameWithoutExtension(sPath)
        Dim sExtension As String = System.IO.Path.GetExtension(sPath)
        'Se comprueba si ya existe el fichero ante la posibilidad de que adjunten 2 veces un fichero con el mismo nombre
        If System.IO.File.Exists(sPath) Then
            Dim ind As Byte = 1
            While System.IO.File.Exists(sFolderPath & "\" & sFileName & "_" & ind.ToString & sExtension)
                ind += 1
            End While

            sPath = sFolderPath & "\" & sFileName & "_" & ind.ToString & sExtension
        End If


        Dim byteBuffer() As Byte
        If bInstancia Then
            byteBuffer = DBServer.Adjunto_InstReadData(mlId, iTipo)
        Else
            byteBuffer = DBServer.Adjunto_ReadData(mlId, iTipo)
        End If

        System.IO.File.WriteAllBytes(sPath, byteBuffer)

        Return sPath
    End Function
    ''' <summary>
    ''' Guarda en el disco un archivo adjunto que viene de un proceso
    ''' </summary>
    ''' <param name="iAnyo">A�o del proceso</param>
    ''' <param name="sGMN1">Nivel 1 del material</param>
    ''' <param name="lProce">Id del Proceso</param>
    ''' <param name="sGrupo">Grupo del proceso</param>
    ''' <param name="lItem">Cod del item</param>
    ''' <param name="sProve">Cod del proveedor</param>
    ''' <param name="lOfe">Tama�o del adjunto</param>
    ''' <returns>El propio archivo adjunto</returns>
    ''' <remarks>
    ''' Llamada desde:PMWeb/arrach/Page_load
    ''' Tiempo m�ximo: 1 seg, aunque depende del tama�o</remarks>
    Public Function SaveAdjunToDiskProce(ByVal iAnyo As Integer, ByVal sGMN1 As String, ByVal lProce As Long, Optional ByVal sGrupo As String = Nothing, Optional ByVal lItem As Long = Nothing, Optional ByVal sProve As String = Nothing, Optional ByVal lOfe As Long = Nothing) As String
        Dim sPath As String = ConfigurationManager.AppSettings("temp") + "\" + modUtilidades.GenerateRandomPath()

        If Not System.IO.Directory.Exists(sPath) Then System.IO.Directory.CreateDirectory(sPath)

        sPath += "\" + msNom

        Dim byteBuffer() As Byte
        byteBuffer = DBServer.Adjunto_ReadDataProceso(mlId, iAnyo, sGMN1, lProce, sGrupo, lItem, sProve, lOfe)
        System.IO.File.WriteAllBytes(sPath, byteBuffer)

        Return sPath
    End Function
    ''' <summary>
    ''' Guarda un arhivo adjunto en el objeto
    ''' </summary>
    ''' <param name="iTipo">Tipo de archivo adjunto</param>
    ''' <remarks>
    ''' Llamada desde: PmWeb/Attach/Page_Load, PMWeb/Campos/ObtenerPathAdjunto, PMWeb/desglose/ObtenerPathAdjunto, PMWeb/detallesolicconsulta/ObtenerPathAdjunto, PMWeb/VisorSolicitudes/gvwSolicitudes_RowDataBound
    ''' Tiempo m�ximo: 1 seg</remarks>
    Public Sub LoadFromRequest(ByVal iTipo As Integer)
        Authenticate()
        Dim data As DataSet
        Dim oRow As DataRow

        data = DBServer.Adjunto_LoadFromRequest(iTipo, mlId)

        If data.Tables(0).Rows.Count > 0 Then
            oRow = data.Tables(0).Rows(0)
            mlId = oRow.Item("ID")
            msNom = oRow.Item("NOM")
            msPer = DBNullToSomething(oRow.Item("PER"))
            msComent = DBNullToSomething(oRow.Item("COMENT"))
            mlDataSize = oRow.Item("DATASIZE")
        End If

    End Sub
    ''' <summary>
    ''' Devuelve un archivo adjunto que viene desde una instancia
    ''' </summary>
    ''' <param name="iTipo">Tipo de archivo adjunto</param>
    ''' <remarks>
    ''' Llamada desde: PmWeb/Attach/Page_Load, PMWeb/Campos/ObtenerPathAdjunto, PMWeb/desglose/ObtenerPathAdjunto, PMWeb/detallesolicconsulta/ObtenerPathAdjunto, PMWeb/VisorSolicitudes/gvwSolicitudes_RowDataBound
    ''' Tiempo m�ximo:1 seg</remarks>
    Public Sub LoadInstFromRequest(ByVal iTipo As Integer)
        Authenticate()
        Dim data As DataSet
        Dim oRow As DataRow

        data = DBServer.Adjunto_LoadInstFromRequest(iTipo, mlId)

        Try
            oRow = data.Tables(0).Rows(0)
            mlId = oRow.Item("ID")
            msNom = oRow.Item("NOM")
            msPer = DBNullToSomething(oRow.Item("PER"))
            msComent = DBNullToSomething(oRow.Item("COMENT"))
            mlDataSize = DBNullToSomething(oRow.Item("DATASIZE"))
        Catch ex As Exception
            mlDataSize = -1000
        End Try

    End Sub
    ''' <summary>
    ''' Devuelve un archivo adjunto que viene de proceso
    ''' </summary>
    ''' <param name="iAnyo">A�o del proceso</param>
    ''' <param name="sGMN1">Material de nivel 1</param>
    ''' <param name="lProce">Codigo del proceso</param>
    ''' <param name="sGrupo">Grupo del proceso</param>
    ''' <param name="lItem">C�digo del item</param>
    ''' <param name="sProve">Proveedor del item</param>
    ''' <param name="lOfe">Tama�o m�ximo del archivo</param>
    ''' <remarks>
    ''' Llamada desde: PMWeb/attach/Page_Load
    ''' Tiempo m�ximo: 1 seg</remarks>
    Public Sub LoadFromProceso(ByVal iAnyo As Integer, ByVal sGMN1 As String, ByVal lProce As Long, Optional ByVal sGrupo As String = Nothing, Optional ByVal lItem As Long = Nothing, Optional ByVal sProve As String = Nothing, Optional ByVal lOfe As Long = Nothing)
        Authenticate()
        Dim data As DataSet
        Dim oRow As DataRow

        data = DBServer.Adjunto_LoadFromProceso(mlId, iAnyo, sGMN1, lProce, sGrupo, lItem, sProve, lOfe)

        oRow = data.Tables(0).Rows(0)
        mlId = oRow.Item("ID")
        msNom = oRow.Item("NOM")
        msComent = DBNullToSomething(oRow.Item("COMENT"))
        mlDataSize = oRow.Item("BYTES")

    End Sub
    ''' <summary>
    ''' Devuelve un archivo adjunto que viene de un pedido
    ''' </summary>
    ''' <param name="lOrden">Id de la orden</param>
    ''' <param name="lLinea">Id de la linea</param>
    ''' <remarks>
    ''' Llamada desde:PmWeb/attach/Page_Load
    ''' Tiempo m�ximo:1 seg</remarks>
    Public Sub LoadFromPedido(ByVal lOrden As Long, Optional ByVal lLinea As Long = Nothing)
        Authenticate()
        Dim data As DataSet
        Dim oRow As DataRow

        data = DBServer.Adjunto_LoadFromPedido(mlId, lOrden, lLinea)

        oRow = data.Tables(0).Rows(0)
        mlId = oRow.Item("ID")
        msNom = oRow.Item("NOM")
        msComent = DBNullToSomething(oRow.Item("COMENT"))
        mlDataSize = oRow.Item("BYTES")

    End Sub
    ''' <summary>
    ''' guarda el comentario del adjunto
    ''' </summary>
    ''' <param name="idAdjunto">id del adjunto a grabar</param>
    ''' <param name="tipo">tipo del adjunto</param>
    ''' <param name="Comentario">comentario a grabar</param>
    ''' <param name="Instancia">Instancia del adjunto</param>
    ''' <remarks></remarks>
    Public Sub GuardarComentarioAdjunto(ByVal idAdjunto As Integer, ByVal tipo As Integer, ByVal Comentario As String, ByVal Instancia As Integer)
        Authenticate()
        DBServer.GuardarComentario(idAdjunto, tipo, Comentario, Instancia)
    End Sub

    Public Function LeerContratoAdjunto(ByVal lIdContrato As Long, ByVal lIdArchivoContrato As Long) As Byte()
        Authenticate()

        LeerContratoAdjunto = DBServer.Adjunto_LeerContratoAdjunto(lIdContrato, lIdArchivoContrato)
    End Function
    ''' <summary>
    ''' Graba el fichero adjunto de Contrato que se adjunta desde el Wizard de GS
    ''' </summary>
    ''' <param name="sPer">Cod de persona</param>
    ''' <param name="sProve">Cod de proveedor</param>
    ''' <param name="sNom">Nombre del fichero</param>
    ''' <param name="sPath">Path donde esta el fichero adjunto</param>
    ''' <returns>retorna el id del fichero grabado</returns>
    ''' <remarks></remarks>
    Public Function Save_Adjun_Contrato_Wizard(ByVal sPer As String, ByVal sProve As String, ByVal sNom As String, ByVal sPath As String, Optional ByVal idContrato As Long = Nothing, Optional ByVal sComent As String = "") As Long()
        Authenticate()

        Return DBServer.Adjunto_Save_Adjun_Contrato_Wizard(sPer, sProve, sNom, sPath, DBServer.DBName, DBServer.DBServer, idContrato, sComent)
    End Function
    ''' <summary>
    ''' Funcion que graba el adjunto en FILESTREAM y que retorna el id del adjunto grabado y su tama�o en bytes
    ''' </summary>
    ''' <param name="sPer"></param>
    ''' <param name="sProve"></param>
    ''' <param name="sNom"></param>
    ''' <param name="sPath"></param>
    ''' <param name="idContrato"></param>
    ''' <param name="sComent"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Save_Adjun(ByVal sPer As String, ByVal sProve As String, ByVal sNom As String, ByVal sPath As String, Optional ByVal idContrato As Long = Nothing, Optional ByVal sComent As String = "", Optional ByVal Solicitud As Long = Nothing, Optional ByVal Instancia As Byte = Nothing, Optional ByVal Campo As Long = Nothing, Optional ByVal Linea As Long = Nothing, Optional ByVal CampoPadre As Long = Nothing, Optional ByVal CampoHijo As Long = Nothing, Optional ByVal DataSize As Long = Nothing, Optional ByVal Idioma As String = Nothing, Optional ByVal Tipo As Byte = Nothing, Optional ByVal FecAct As DateTime = Nothing) As Long()
        Authenticate()

        msNom = sNom
        Return DBServer.Adjunto_Save_Adjun(sPer, sProve, sNom, sPath, DBServer.DBName, DBServer.DBServer, idContrato, sComent, Solicitud, Instancia, Campo, Linea, CampoPadre, CampoHijo, DataSize, Idioma, Tipo, FecAct)
    End Function
    ''' <summary>
    ''' Funcion que devuelve el fichero adjunto a GS desde el web Service
    ''' </summary>
    ''' <param name="Id">Identificador del adjunto</param>
    ''' <param name="Tipo">Tipo de adjunto</param>
    ''' <param name="bInstancia">True si el adjunto esta en una instancia</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Leer_Adjun(ByVal Id As Long, ByVal Tipo As Byte, Optional ByVal bInstancia As Boolean = False) As Byte()
        Authenticate()
        If bInstancia Then
            Return DBServer.Adjunto_InstReadData(Id, Tipo)
        Else
            Return DBServer.Adjunto_ReadData(Id, Tipo)
        End If
    End Function
    ''' <summary>
    ''' Sustituye un fichero Adjunto desde GS
    ''' </summary>
    ''' <param name="Id">id del adjunto a sustituir</param>
    ''' <param name="Tipo">Tipo del adjunto a sustituir(De solicitud, de campo....)</param>
    ''' <param name="sNom">Nombre del nuevo fichero</param>
    ''' <param name="rutaTemp">Ruta del nuevo fichero</param>
    ''' <param name="sComent">Comentario del nuevo fichero</param>
    ''' <param name="DataSize">Tama�o del nuevo fichero</param>
    ''' <param name="Idioma">Idioma</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Update_Adjun(ByVal Id As Long, ByVal Tipo As Byte, ByVal sNom As String, ByVal rutaTemp As String, Optional ByVal sComent As String = "", Optional ByVal DataSize As Long = Nothing, Optional ByVal Idioma As String = Nothing) As Long()
        Authenticate()

        Return DBServer.Adjunto_Update_Adjun(Id, sNom, rutaTemp, sComent, DataSize, Idioma, Tipo)
    End Function
End Class
