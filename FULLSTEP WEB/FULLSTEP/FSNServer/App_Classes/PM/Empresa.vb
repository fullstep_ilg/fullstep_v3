﻿<Serializable()> _
        Public Class Empresa
    Inherits Security

    Private mlId As Long
    Private msNIF As String
    Private msDen As String
    Private msDir As String
    Private msCP As String
    Private msPoblacion As String
    Private msPais As String
    Private msProvincia As String
#Region "Propiedades"
    Property ID() As Long
        Get
            ID = mlId
        End Get
        Set(ByVal Value As Long)
            mlId = Value
        End Set
    End Property
    Property NIF() As String
        Get
            NIF = msNIF
        End Get
        Set(ByVal Value As String)
            msNIF = Value
        End Set
    End Property
    Property Den() As String
        Get
            Den = msDen
        End Get
        Set(ByVal Value As String)
            msDen = Value
        End Set
    End Property
    Property Dir() As String
        Get
            Dir = msDir
        End Get
        Set(ByVal Value As String)
            msDir = Value
        End Set
    End Property
    Property CP() As String
        Get
            CP = msCP
        End Get
        Set(ByVal Value As String)
            msCP = Value
        End Set
    End Property
    Property Poblacion() As String
        Get
            Poblacion = msPoblacion
        End Get
        Set(ByVal Value As String)
            msPoblacion = Value
        End Set
    End Property
    Property Pais() As String
        Get
            Pais = msPais
        End Get
        Set(ByVal Value As String)
            msPais = Value
        End Set
    End Property
    Property Provincia() As String
        Get
            Provincia = msProvincia
        End Get
        Set(ByVal Value As String)
            msProvincia = Value
        End Set
    End Property
#End Region
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
    ''' <summary>
    ''' Procedimiento que carga una empresa dado un código y un idioma especifico
    ''' </summary>
    ''' <param name="sIdi">Idioma</param>
    ''' <remarks>Llamada desde: AltaContrato.aspx.vb --> ConfigurarCabeceraContrato</remarks>
    Public Sub Load(ByVal sIdi As String)
        Dim oDS As DataSet

        Authenticate()
        oDS = DBServer.Empresa_Load(mlId, sIdi)

        If Not oDS.Tables(0).Rows.Count = 0 Then
            msNIF = oDS.Tables(0).Rows(0).Item("NIF")
            msDen = oDS.Tables(0).Rows(0).Item("DEN_EMPRESA")
            msDir = DBNullToStr(oDS.Tables(0).Rows(0).Item("DIR"))
            msCP = DBNullToStr(oDS.Tables(0).Rows(0).Item("CP"))
            msPoblacion = DBNullToStr(oDS.Tables(0).Rows(0).Item("POB"))
            msPais = DBNullToStr(oDS.Tables(0).Rows(0).Item("PAIS"))
            msProvincia = DBNullToStr(oDS.Tables(0).Rows(0).Item("PROVINCIA"))
        End If

        oDS = Nothing
    End Sub
    ''' <summary>
    ''' comprueba si el usuario puede descargar el archivo adjunto
    ''' </summary>
    ''' <param name="usuario">string del usuario actual</param>
    ''' <param name="perfil">string con el perfil del usuario pueden ser "Consulta" '"Gestor" "Administrador" "Auditor"</param>
    ''' <param name="instancia">int con el la instancia actual</param>
    ''' <param name="empresa">string con la empresa</param>
    ''' <returns>Una variable booleana que indica si podemos o no descargar el archivo adjunto</returns>
    ''' <remarks>Llamada desde: </remarks>
    Public Function ComprobarDescargaLCX(ByVal usuario As String, ByVal perfil As String, ByVal instancia As Integer, Optional ByVal empresa As Integer = 0) As Boolean
        Authenticate()

        Dim existecentroCoste As Boolean
        existecentroCoste = DBServer.ComprobarDescargaLCX(usuario, perfil, instancia, empresa)
        Return existecentroCoste
    End Function
End Class

