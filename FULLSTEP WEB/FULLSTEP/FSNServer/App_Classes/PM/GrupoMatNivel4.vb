<Serializable()> _
        Public Class GrupoMatNivel4
    Inherits Security
    Implements IGmn

    Private moData As DataSet
    Private msGmnQA As Integer
    Private msGMN1Cod As String
    Private msGMN1Den As String
    Private msGMN2Cod As String
    Private msGMN2Den As String
    Private msGMN3Cod As String
    Private msGMN3Den As String
    Private msCod As String
    Private msDen As String
    Private _children As Dictionary(Of String, IGmn)
    Private _hasChild As Boolean
    Private _parent As IGmn

    Public Function title() As String Implements IGmn.title
        Return msGMN1Cod & " - " & msGMN2Cod & " - " & msGMN3Cod & " - " & msCod & " - " & msDen
    End Function
    Public ReadOnly Property key As String Implements IGmn.key
        Get
            Return msGmnQA & "###" & msGMN1Cod & "###" & msGMN2Cod & "###" & msGMN3Cod & "###" & msCod
        End Get
    End Property
    Public Property children As Dictionary(Of String, IGmn) Implements IGmn.children
        Get
            Return New Dictionary(Of String, IGmn)
        End Get
        Set(value As Dictionary(Of String, IGmn))
            _children = value
        End Set
    End Property
    Public Function child(node As IGmn) As IGmn Implements IGmn.child
        Return Nothing
    End Function
    Public ReadOnly Property hasChildren As Boolean Implements IGmn.hasChildren
        Get
            Return False
        End Get
    End Property
    Public Function nivel() As Integer Implements IGmn.nivel
        Return 4
    End Function
    Public Property parent As IGmn Implements IGmn.parent
        Get
            Return _parent
        End Get
        Set(value As IGmn)
            _parent = value
        End Set
    End Property
    Public Function topparent() As IGmn Implements IGmn.topParent
        Return Me.parent.topParent
    End Function
    Private _e As EstructuraMateriales
    Public Property EstructuraMateriales() As EstructuraMateriales Implements IGmn.Estructura
        Get
            Return _e
        End Get
        Set(value As EstructuraMateriales)
            _e = value
        End Set
    End Property
    Property Cod() As String
        Get
            Return msCod
        End Get
        Set(ByVal Value As String)
            msCod = Value
        End Set
    End Property
    Property Den() As String Implements IGmn.Den
        Get
            Return msDen
        End Get
        Set(ByVal Value As String)
            msDen = Value
        End Set
    End Property
    Public Property gmnQAcod As Integer Implements IGmn.GmnQAcod
        Get
            Return msGmnQA
        End Get
        Set(value As Integer)
            msGmnQA = value
        End Set
    End Property
    Property GMN1Cod() As String
        Get
            Return msGMN1Cod
        End Get
        Set(ByVal Value As String)
            msGMN1Cod = Value
        End Set
    End Property
    Property GMN1Den() As String
        Get
            Return msGMN1Den
        End Get
        Set(ByVal Value As String)
            msGMN1Den = Value
        End Set
    End Property
    Property GMN2Cod() As String
        Get
            Return msGMN2Cod
        End Get
        Set(ByVal Value As String)
            msGMN2Cod = Value
        End Set
    End Property
    Property GMN2Den() As String
        Get
            Return msGMN2Den
        End Get
        Set(ByVal Value As String)
            msGMN2Den = Value
        End Set
    End Property
    Property GMN3Cod() As String
        Get
            Return msGMN3Cod
        End Get
        Set(ByVal Value As String)
            msGMN3Cod = Value
        End Set
    End Property
    Property GMN3Den() As String
        Get
            Return msGMN3Den
        End Get
        Set(ByVal Value As String)
            msGMN3Den = Value
        End Set
    End Property
    Public ReadOnly Property Data() As Data.DataSet
        Get
            Return moData
        End Get
    End Property
    Public Function incluye(o As IGmn) As Boolean Implements IGmn.incluye
        If o.nivel >= Me.nivel Then
            Return o.gmn1Cod = Me.cod1 And o.gmn2Cod = Me.cod2 And o.gmn3Cod = Me.cod3 And o.gmn4Cod = Me.cod4
        End If
        Return False
    End Function
    Public Function contiene(o As IGmn) As Boolean Implements IGmn.contiene
        If o.nivel > Me.nivel Then
            Return o.gmn1Cod = Me.cod1 And o.gmn2Cod = Me.cod2 And o.gmn3Cod = Me.cod3 And o.gmn4Cod = Me.cod4
        End If
        Return False
    End Function
    Public Property cod1 As String Implements IGmn.gmn1Cod
        Get
            Return msGMN1Cod
        End Get
        Set(value As String)
            msGMN1Cod = value
        End Set
    End Property
    Public Property cod2 As String Implements IGmn.gmn2Cod
        Get
            Return msGMN2Cod
        End Get
        Set(value As String)
            msGMN2Cod = value
        End Set
    End Property
    Public Property cod3 As String Implements IGmn.gmn3Cod
        Get
            Return msGMN3Cod
        End Get
        Set(value As String)
            msGMN3Cod = value
        End Set
    End Property
    Public Property cod4 As String Implements IGmn.gmn4Cod
        Get
            Return msCod
        End Get
        Set(value As String)
            msCod = value
        End Set
    End Property
    Private _user As FSNServer.User
    Public Property user As FSNServer.User Implements IGmn.User
        Get
            Return _user
        End Get
        Set(value As FSNServer.User)
            _user = value
        End Set
    End Property
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
End Class
