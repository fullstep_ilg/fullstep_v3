﻿<Serializable()> _
Public Class Puntuacion
    Inherits Security
    ''' <summary>
    ''' Creación de una instancia de la clase
    ''' </summary>
    ''' <param name="dbserver">Objeto Root de PMDatabaseServer</param> 
    ''' <param name="isAuthenticated">Usuario atenticado sí/no</param>
    ''' <remarks>Llamada desde: fichaProveedor.aspx/MostrarPanelPPM; Tiempo máximo: 0</remarks>
    Public Sub New(ByRef dbserver As FSNDataBaseServer.Root,ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
    End Sub
    ''' <summary>
    ''' Función que devuelve un dataset que contiene la variable de calidad de tipo PPM cargada
    ''' </summary>
    ''' <param name="Variable">Id de la variable de calidad</param>
    ''' <param name="Nivel">Nivel de la variable de calidad</param>
    ''' <param name="Idioma">Idioma</param>
    ''' <param name="Prove">Proveedor</param>
    ''' <param name="Unqa">Unqa</param>
    ''' <param name="Subtipo">Subtipo de la variable</param>
    ''' <returns>Dataset con la variable de calidad de tipo PPM cargada</returns>
    ''' <remarks>Llamada desde: fichaProveedor.aspx/MostrarPanelPPM; Tiempo máximo: 0,1</remarks>
    Public Function DetallePuntuacion(ByVal Variable As Long, ByVal Nivel As Integer, ByVal Idioma As String, ByVal Prove As String, ByVal Unqa As Long, ByVal Subtipo As Integer) As DataSet
        Authenticate()

        Return DBServer.Puntuacion_DetallePuntuacion(Variable, Nivel, Idioma, Prove, Unqa, Subtipo)
    End Function
End Class