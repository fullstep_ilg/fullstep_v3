<Serializable()> _
Public Class Instancias
    Inherits Security

    Private moInstancias As DataSet
    Private Contexto As System.Web.HttpContext

    Public ReadOnly Property Data() As Data.DataSet
        Get
            Return moInstancias
        End Get
    End Property
    ''' <summary>
    ''' Obtiene las solicitudes pendientes que haya seleccionado el usuario en el webpart.
    ''' </summary>
    ''' <param name="sUsu">Cod de la persona</param>
    ''' <param name="sIdi">Idioma para obtener la denominaci�n</param>
    ''' <param name="nTop">N� de solicitudes a obtener</param>
    ''' <param name="sTipos">Cadena con los tipos de solicitudes seleccionadas</param>
    ''' <param name="dImporteDesde">Importe desde establecido</param>
    ''' <param name="dImporteHasta">Importe hasta establecido</param>
    ''' <param name="dFechaDesde">Fecha desde establecido</param>
    ''' <param name="dFechaHasta">Fecha hasta establecido</param>
    ''' <param name="sFormatoFecha">Formato de la fecha</param>
    ''' <param name="sEstado">Cadena con los estados seleccionados</param>
    ''' <param name="bAbiertasPorUsted">Si hay que obtener o no las solicitudes abiertas por el usuario</param>
    ''' <param name="bParticipo">Si hay que obtener o no las solicitudes en las que ha participado</param>
    ''' <param name="bTrasladadas">Si hay que obtener o no las solicitudes trasladadas</param>
    ''' <param name="bOtras">Si hay que obtener o no las solicitudes en las que el usuario es observador</param>
    ''' <remarks>Llamada desde: FSPMWebPartBusquedaSolicitudes.vb. Tiempo m�ximo: 3 sg.</remarks>
    Public Sub LoadResultadoBusqueda(ByVal sUsu As String, ByVal sIdi As String, Optional ByVal nTop As Integer = 0,
                                     Optional ByVal sTipos As String = Nothing, Optional ByVal dImporteDesde As Double = Nothing,
                                     Optional ByVal dImporteHasta As Double = Nothing, Optional ByVal dFechaDesde As Date = Nothing,
                                     Optional ByVal dFechaHasta As Date = Nothing, Optional ByVal sFormatoFecha As String = Nothing,
                                     Optional ByVal sEstado As String = Nothing, Optional ByVal bPendientesTratar As Boolean = False,
                                     Optional ByVal bAbiertasPorUsted As Boolean = False, Optional ByVal bParticipo As Boolean = False,
                                     Optional ByVal bTrasladadas As Boolean = False, Optional ByVal bPendientesDevolucion As Boolean = False,
                                     Optional ByVal bOtras As Boolean = False, Optional ByVal TipoSolicitud As TiposDeDatos.TipoDeSolicitud = TiposDeDatos.TipoDeSolicitud.Otros)
        Authenticate()
        moInstancias = DBServer.Instancias_LoadResultadoBusqueda(sUsu, sIdi, nTop, sTipos, dImporteDesde, dImporteHasta, dFechaDesde, dFechaHasta, sFormatoFecha, sEstado,
                                                                 bPendientesTratar, bAbiertasPorUsted, bParticipo, bTrasladadas, bPendientesDevolucion, bOtras, TipoSolicitud)
    End Sub
    ''' <summary>
    ''' Obtiene las solicitudes que est�n marcadas para su monitorizaci�n y que cumplen las condiciones establecidas por el usuario.
    ''' </summary>
    ''' <param name="sUsu">Cod de la persona</param>
    ''' <param name="sIdi">Idioma del usuario</param>
    ''' <param name="sTipos">Cadena con los tipos de solicitudes seleccionadas</param>
    ''' <param name="dImporteDesde">Importe desde establecido</param>
    ''' <param name="dImporteHasta">Importe hasta establecido</param>
    ''' <param name="dFechaDesde">Fecha desde establecido</param>
    ''' <param name="dFechaHasta">Fecha hasta establecido</param>
    ''' <param name="sFormatoFecha">Formato de la fecha</param>
    ''' <remarks>Llamada desde: FSPMWebPartMonitorizacionSolicitudes. Tiempo m�ximo: 1 sg.</remarks>
    Public Sub LoadMonitorizacion(ByVal sUsu As String, Optional ByVal sIdi As String = "SPA", Optional ByVal sTipos As String = Nothing, Optional ByVal dImporteDesde As Double = Nothing, Optional ByVal dImporteHasta As Double = Nothing, Optional ByVal dFechaDesde As Date = Nothing, Optional ByVal dFechaHasta As Date = Nothing, Optional ByVal sFormatoFecha As String = Nothing)
        Authenticate()
        moInstancias = DBServer.Instancias_LoadMonitorizacion(sUsu, sIdi, sTipos, dImporteDesde, dImporteHasta, dFechaDesde, dFechaHasta, sFormatoFecha)
    End Sub
    ''' <summary>
    ''' Obtiene las solicitudes de un usuario, seg�n un estado y unos criterios definidos.
    ''' </summary>
    ''' <param name="CodPersona"></param>
    ''' <param name="Idioma"></param>
    ''' <param name="IdFormulario"></param>
    ''' <param name="SentenciaWhere"></param>
    ''' <param name="SentenciaOrder"></param>
    ''' <param name="SentenciaWhere_CamposGenerales"></param>
    ''' <param name="Pagina"></param>
    ''' <param name="NumeroRegistros"></param>
    ''' <param name="BuscarProveArt"></param>
    ''' <returns>Devuelve las solicitudes que cumplen con todas las condiciones</returns>
    ''' <remarks>LLamada desde: VisorSolicitudes.aspx; Tiempo m�ximo: 7 sg</remarks>
    Public Function DevolverInstancias(ByVal CodPersona As String, ByVal Idioma As String, IdFormulario As Long,
             ByVal SentenciaWhere As String, ByVal SentenciaOrder As String, ByVal SentenciaWhere_CamposGenerales As String,
             ByVal Pagina As Integer, ByVal NumeroRegistros As Integer, ByVal BuscarProveArt As Integer,
             Optional ByVal AbtasUsted As Boolean = True, Optional ByVal Participo As Boolean = True, Optional ByVal Pendientes As Boolean = True,
             Optional ByVal PendientesDevolucion As Boolean = True, Optional ByVal Trasladadas As Boolean = True, Optional ByVal Otras As Boolean = True,
             Optional ByVal ObservadorSustitucion As Boolean = True, Optional ByVal ObtenerProcesos As Boolean = False, Optional ObtenerPedidos As Boolean = False,
             Optional ByVal OblCodPedDir As Boolean = False, Optional ByVal FiltroDesglose As Boolean = False, Optional ByVal VistaDesglose As Boolean = False,
             Optional ByVal TipoVisor As FSNLibrary.TipoVisor = FSNLibrary.TipoVisor.Solicitudes) As DataSet
        Authenticate()

        Dim TipoSolicitud As Integer?
        Select Case TipoVisor
            Case FSNLibrary.TipoVisor.NoConformidades
                TipoSolicitud = TiposDeDatos.TipoDeSolicitud.NoConformidad
            Case FSNLibrary.TipoVisor.Certificados
                TipoSolicitud = TiposDeDatos.TipoDeSolicitud.Certificado
            Case FSNLibrary.TipoVisor.Contratos
                TipoSolicitud = TiposDeDatos.TipoDeSolicitud.Contrato
            Case FSNLibrary.TipoVisor.Facturas
                TipoSolicitud = TiposDeDatos.TipoDeSolicitud.Factura
            Case FSNLibrary.TipoVisor.Encuestas
                TipoSolicitud = TiposDeDatos.TipoDeSolicitud.Encuesta
            Case FSNLibrary.TipoVisor.SolicitudesQA
                TipoSolicitud = TiposDeDatos.TipoDeSolicitud.SolicitudQA
            Case Else
                TipoSolicitud = Nothing
        End Select

        Return DBServer.InstanciasPM_Get(CodPersona, Idioma, IdFormulario, SentenciaWhere, SentenciaOrder, SentenciaWhere_CamposGenerales,
                                         Pagina, NumeroRegistros, BuscarProveArt, AbtasUsted, Participo, Pendientes, PendientesDevolucion,
                                         Trasladadas, Otras, ObservadorSustitucion, ObtenerProcesos, ObtenerPedidos, OblCodPedDir, FiltroDesglose,
                                         VistaDesglose, TipoSolicitud)
    End Function
    ''' <summary>
    ''' Obtiene el n� total de solicitudes encontradas para un usuario con unos criterios de filtro.
    ''' </summary>
    ''' <param name="sUsu">Cod. de la persona</param>
    ''' <param name="sTiposSolicitud">Cadena con los tipos de solicitud seleccionados. Si hay m�s de uno, vendr�n separados por ","</param>
    ''' <param name="dImporteDesde">Importe desde introducido</param>
    ''' <param name="dImporteHasta">Importe hasta introducido</param>
    ''' <param name="dFechaDesde">Fecha desde introducida</param>
    ''' <param name="dFechaHasta">Fecha hasta introducida</param>
    ''' <param name="sFormatoFecha">Formato de la fecha del usuario</param>
    ''' <param name="sEstado">Cadena con los estados seleccionados</param>
    ''' <param name="bAbiertasPorUsted">Si hay que mostrar las solicitudes abiertas por el usuario</param>
    ''' <param name="bParticipo">Si hay que mostrar las solicitudes en las que particip�</param>
    ''' <param name="bTrasladadas">Si hay que mostrar las solicitudes trasladadas</param>
    ''' <param name="bOtras">Si hay que mostrar las solicitudes en las que est� como observador</param>
    ''' <returns>El n� total de solicitudes</returns>
    ''' <remarks>Llamada desde: FSPMWebPartBusquedaSolicitudes; Tiempo m�ximo: 2 sg</remarks>
    Public Function DevolverNumInstanciasBusqueda(ByVal sUsu As String, ByVal sTiposSolicitud As String, ByVal dImporteDesde As Double, ByVal dImporteHasta As Double,
                                                  ByVal dFechaDesde As Date, ByVal dFechaHasta As Date, ByVal sFormatoFecha As String, Optional ByVal sEstado As String = "",
                                                  Optional ByVal bPendientesTratar As Boolean = False, Optional ByVal bAbiertasPorUsted As Boolean = False,
                                                  Optional ByVal bParticipo As Boolean = False, Optional ByVal bTrasladadas As Boolean = False,
                                                  Optional ByVal bPendientesDevolucion As Boolean = False, Optional ByVal bOtras As Boolean = False,
                                                  Optional ByVal TipoSolicitud As TiposDeDatos.TipoDeSolicitud = TiposDeDatos.TipoDeSolicitud.Otros) As Long
        Authenticate()
        DevolverNumInstanciasBusqueda = DBServer.InstanciasBusqueda_Get(sUsu, sTiposSolicitud, dImporteDesde, dImporteHasta, dFechaDesde, dFechaHasta, sFormatoFecha, sEstado,
                                                                        bPendientesTratar, bAbiertasPorUsted, bParticipo, bTrasladadas, bPendientesDevolucion, bOtras, TipoSolicitud)
    End Function
    ''' <summary>
    ''' Procedimiento que devuelve las solicitudes de un padre de una instancia, que cumplan los filtros pasados
    ''' </summary>
    ''' <param name="sIdi">Idioma de la aplicaci�n</param>
    ''' <param name="sMon">Codigo de moneda</param>
    ''' <param name="idSolPadre">Id de la solicitud Padre</param>
    ''' <param name="lTipoSol">tipo de la solicitud</param>
    ''' <param name="sDesc">Descripcion</param>
    ''' <param name="sPeticionario">Codigo del peticionario</param> 
    ''' <param name="sUON1">Codigo de la Uon de nivel 1</param>
    ''' <param name="sUON2">Codigo de la Uon de nivel 2</param>
    ''' <param name="sUON3">Codigo de la Uon de nivel 3</param>
    ''' <param name="lFormID">Id del formulario</param>
    ''' <param name="lInstancia">Id de la instancia</param>
    ''' <remarks>Llamada desde: PmWeb/DetalleSolicitudpadre/Page_load, PmWeb/GuardarInstancia/Page_load - GuardarConWorkFlow - cmdAceptar_Serverclick, PmWeb/SolicitudesServer/cmdAceptar_ServerClick
    ''' Tiempo m�ximo: 1 seg</remarks>
    Public Sub DevolverSolicitudesPadre(ByVal sIdi As String, Optional ByVal sMon As String = Nothing, Optional ByVal idSolPadre As Integer = 0, Optional ByVal lTipoSol As Long = Nothing, _
                                        Optional ByVal sDesc As String = Nothing, Optional ByVal sPeticionario As String = Nothing, Optional ByVal sUON1 As String = Nothing, _
                                        Optional ByVal sUON2 As String = Nothing, Optional ByVal sUON3 As String = Nothing, Optional ByVal lFormID As Long = 0, Optional ByVal lInstancia As Long = 0)
        Authenticate()
        moInstancias = DBServer.Instancias_Padre(sIdi, sMon, idSolPadre, lTipoSol, sDesc, sPeticionario, sUON1, sUON2, sUON3, lFormID, lInstancia)
    End Sub
    ''' <summary>
    ''' Procedimiento que actualiza el estado de una instancia a En proceso
    ''' </summary>
    ''' <param name="sInstancias">Identificador de la instancia</param>
    ''' <param name="sUsuario">Codigo de usuario</param>
    ''' <remarks>
    ''' Llamada desde: PmWeb/detalleSolicConsulta/AprobarRechazarInstancia, PmWeb/PROCESARMultAprobRech/Page_load , PmWeb/visorSolicitudes/Procesar_Acciones
    ''' tiempo m�ximo: 0,5 seg</remarks>
    Public Sub Actualizar_En_Proceso2(ByVal sInstancias As String, ByVal sUsuario As String)
        Authenticate()
        DBServer.Instancia_Actualizar_En_Proceso2(sInstancias, sUsuario)
    End Sub
    ''' <summary>
    ''' Funcion que devuelve las instancias pendientes de validacion
    ''' </summary>
    ''' <returns>Un dataset con los datos de las instancias pendientes de validacion</returns>
    ''' <remarks>
    ''' Llamada desde: WebServicePM/Service.asmx/RealizarValidacionesthread
    ''' Tiempo m�ximo: 1 seg</remarks>
    Public Function InstanciasPdtesValidacion() As DataSet
        Authenticate()
        Return DBServer.InstanciasPendientesValidacion_Get()
    End Function
    ''' <summary>
    ''' Funcion que devuelve los datos de las instancias validadas
    ''' </summary>
    ''' <returns>Un dataset con los datos de las instancias que estan validadas</returns>
    ''' <remarks>Llamada desdE: webServicePm/Servicie.asmx/ejecutarAcciones
    ''' Tiempo m�ximo:1 seg </remarks>
    Public Function InstanciasValidadas() As DataSet
        Authenticate()
        Return DBServer.InstanciasValidadas_Get()
    End Function
    ''' <summary>
    ''' Revisado por: Sandra. Fecha: 21/03/2011.
    ''' Devuelve todos las descripciones de los contratos de un usuario. Carga los datos en cach�.
    ''' </summary>
    ''' <param name="sUsuario">Cod. de la persona</param>
    ''' <returns>Dataset con todas las descripciones de los contratos de ese usuario.</returns>
    ''' <remarks>Llamada desde: Autocomplete.asmx; Tiempo m�ximo: 1 sg.</remarks>
    Public Function DevolverDescripciones(ByVal sUsuario As String) As DataSet
        Dim ds As DataSet
        Authenticate()
        Contexto = System.Web.HttpContext.Current
        ds = CType(Contexto.Cache("dsDescripciones" & sUsuario), DataSet)
        If ds Is Nothing Then
            ds = DBServer.InstanciasDescripciones_Get(sUsuario)
            Contexto.Cache.Insert("dsDescripciones" & sUsuario, ds, Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration, New TimeSpan(0, System.Configuration.ConfigurationManager.AppSettings("TiempoExpiracionCacheMin"), 0))
        End If
        Return ds
    End Function
    ''' <summary>
    ''' Funcion que comprueba si una instancia se encuentra procesada
    ''' </summary>
    ''' <param name="lInstancia">Id de la instancia</param>
    ''' <param name="lBloque">Id del bloque</param>
    ''' <returns>Una variable booleana que indica si la instancia esta o no procesada</returns>
    ''' <remarks>LLamada desde; PmWeb/deralleSolcConsulta/AprobarRechazarinstancia
    ''' tiempo m�ximo: </remarks>
    Public Function ComprobarSiEstaProcesada(ByVal lInstancia As Long, ByVal lBloque As Long)
        Authenticate()
        Return DBServer.Instancia_ComprobarSiEstaProcesada(lInstancia, lBloque)
    End Function
    ''' <summary>
    ''' Si desde GS se ha cambiado el estado a Rechazado/Anulado/Cerrado, NO se puede aprobar/rechazar
    ''' </summary>
    ''' <param name="lInstancia">Id de la instancia</param>
    ''' <returns>Llamada desde:AprobacionEmail.asmx.vb  Tiempo m�ximo: 0,1</returns>
    Public Function ComprobarEstadoIncompatibleGS(ByVal lInstancia As Long)
        Authenticate()
        Return DBServer.Instancia_ComprobarEstadoIncompatibleGS(lInstancia)
    End Function

    ''' <summary>
    ''' Si desde EP se ha cambiado el estado a Rechazado/Anulado/Cerrado, NO se puede aprobar/rechazar
    ''' </summary>
    ''' <param name="lInstancia">Id de la instancia</param>
    ''' <returns>Llamada desde:AprobacionEmail.asmx.vb  Tiempo m�ximo: 0,1</returns>
    Public Function ComprobarEstadoIncompatibleCesta(ByVal lInstancia As Long)
        Authenticate()
        Return DBServer.Instancia_ComprobarEstadoIncompatibleCesta(lInstancia)
    End Function

    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
    ''' Revisado por: Jbg; Fecha: 03/11/2011
    ''' <summary>
    ''' Devuelve todas las instancias y t�tulos de los certificados o no conformidades de un usuario. Carga los datos en cach�.
    ''' </summary>
    ''' <param name="sUsuario">Cod. del usuario</param>
    ''' <param name="sIdioma">Idioma</param>
    ''' <param name="sTipoEntidad">certificados o no conformidades</param>
    ''' <returns>Dataset con todas las instancias y t�tulos de los certificados o no conformidades de un usuario.</returns>
    ''' <remarks>Llamada desde: Autocomplete.asmx; Tiempo m�ximo: 1 sg.</remarks>
    Public Function DevolverTitulosQA(ByVal sUsuario As String, ByVal sIdioma As String, ByVal sTipoEntidad As String, _
            ByVal RestricSolicUsu As Boolean, ByVal RestricSolicUO As Boolean, ByVal RestricSolicDep As Boolean, _
            ByVal RestricProvMat As Boolean, ByVal RestricProvEqp As Boolean, ByVal RestricProvCon As Boolean) As DataSet
        Dim ds As DataSet
        Authenticate()
        Contexto = System.Web.HttpContext.Current
        ds = CType(Contexto.Cache("dsTitulosQA" & sUsuario & "Tipo" & CStr(sTipoEntidad)), DataSet)
        If ds Is Nothing Then
            ds = DBServer.InstanciasTitulosQA_Get(sUsuario, sIdioma, sTipoEntidad, _
                    RestricSolicUsu, RestricSolicUO, RestricSolicDep, RestricProvMat, RestricProvEqp, RestricProvCon)
            Contexto.Cache.Insert("dsTitulosQA" & sUsuario & "Tipo" & CStr(sTipoEntidad), ds, Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration, New TimeSpan(0, System.Configuration.ConfigurationManager.AppSettings("TiempoExpiracionCacheMin"), 0))
        End If
        Return ds
    End Function
    ''' Revisado por: Jim; Fecha: 03/11/2011
    ''' <summary>
    ''' Devuelve todas las instancias del notificador. Carga los datos en cach�.
    ''' </summary>
    ''' <param name="sIdioma">Idioma</param>
    ''' <param name="sTipoEntidad">Tipo entidad</param>
    ''' <returns>Dataset con todas las instancias</returns>
    ''' <remarks>Llamada desde: Autocomplete.asmx; Tiempo m�ximo: 1 sg.</remarks>
    Public Function DevolverInstanciasNotificacion(ByVal sIdioma As String, ByVal sTipoEntidad As String) As DataSet
        Dim ds As DataSet
        Authenticate()
        Contexto = System.Web.HttpContext.Current
        ds = CType(Contexto.Cache("dsDatosNotificadorTipo" & CStr(sTipoEntidad)), DataSet)
        If ds Is Nothing Then
            ds = DBServer.DevolverInstanciasNotificacion(sIdioma, sTipoEntidad)
            Contexto.Cache.Insert("dsDatosNotificadorTipo", ds, Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration, New TimeSpan(0, System.Configuration.ConfigurationManager.AppSettings("TiempoExpiracionCacheMin"), 0))
        End If
        Return ds
    End Function
    Public Function Devolver_Adjudicaciones_Linea_Instancia(ByVal lInstancia As Long, ByVal lCampo As Long, ByVal lLinea As Long, ByVal bPreadj As Boolean) As DataSet
        Return DBServer.Devolver_Adjudicaciones_Linea_Instancia(lInstancia, lCampo, lLinea, bPreadj)
    End Function
End Class