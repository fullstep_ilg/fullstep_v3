﻿Imports System.Configuration

<Serializable()> _
Public Class Escenarios
    Inherits Security
    ''' <summary>
    ''' Constructor de la clase Escenarios
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si está autenticado</param>
    ''' <revision></revision>
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
    ''' <summary>
    ''' Obtiene los escenarios que el usuario puede utilizar por tipo de visor
    ''' </summary>
    ''' <param name="UsuCod">Código de usuario </param>
    ''' <param name="TipoVisor">El tipo de visor para obtener los escenarios (1-solicitudes,2-no conf,3-certif,4-contratos)</param>
    ''' <remarks></remarks>
    Public Function FSN_Get_Escenarios_Usuario(ByVal UsuCod As String, ByVal Idioma As String, ByVal TipoVisor As FSNLibrary.TipoVisor,
                ByVal OcultarEscenarioDefecto As Boolean, ByVal PermisoEditarEscenariosOtrosUsu As Boolean,
                ByVal RestricEdicionUsuDep As Boolean, ByVal RestricEdicionUsuUON As Boolean, ByVal RestricEdicionUsuPerfUON As Boolean) As Object
        Authenticate()

        Dim idEscenarioSeleccionado As Nullable(Of Integer)
        Dim dEscenarios As New Dictionary(Of String, Escenario)
        Dim iEscenario As Escenario
        Dim dsEscenarios As DataSet = DBServer.FSN_Get_Escenarios_Usuario(UsuCod, Idioma, TipoVisor, 0, OcultarEscenarioDefecto, False,
                                                                          RestricEdicionUsuDep, RestricEdicionUsuUON, RestricEdicionUsuPerfUON)
        Dim ordenesCarpeta As New Dictionary(Of String, Integer)
        'Creamos la estructura del escenario, con sus filtros vinculados y estos a sus vistas.
        For Each escenario As DataRow In dsEscenarios.Tables("ESCENARIOS_USUARIO").Select("", "CARPETA ASC")
            If CType(escenario("ESCENARIOSELECCIONADO"), Boolean) Then idEscenarioSeleccionado = CType(escenario("ID"), Integer)
            If CType(escenario("ID"), Integer) = 0 Then
                iEscenario = FSN_Get_Escenario_Defecto(UsuCod, Idioma, TipoVisor, escenario("DEFECTO"), escenario("POSICION"), escenario("FAVORITO"))
                iEscenario.Cargado = True
            Else
                iEscenario = Create_Escenario(Idioma, escenario,
                        dsEscenarios.Tables("ESCENARIO_SOLICITUD_FORMULARIOS_USUARIO").Select("ESCENARIO=" & escenario("ID")),
                        dsEscenarios.Tables("ESCENARIO_FILTROS_USUARIO").Select("ESCENARIO=" & escenario("ID")),
                        dsEscenarios.Tables("ESCENARIO_FILTRO_CAMPOS_USUARIO").Select("ESCENARIO=" & escenario("ID")),
                        dsEscenarios.Tables("ESCENARIO_FILTRO_CONDICIONES_USUARIO").Select("ESCENARIO=" & escenario("ID")),
                        dsEscenarios.Tables("ESCENARIO_VISTAS_USUARIO").Select("ESCENARIO=" & escenario("ID")),
                        dsEscenarios.Tables("ESCENARIO_VISTA_CAMPOS_USUARIO").Select("ESCENARIO=" & escenario("ID")), PermisoEditarEscenariosOtrosUsu)
            End If
            If ordenesCarpeta.ContainsKey(iEscenario.Carpeta) Then
                iEscenario.Posicion = ordenesCarpeta(iEscenario.Carpeta)
                ordenesCarpeta(iEscenario.Carpeta) += 1
            Else
                iEscenario.Posicion = 0
                ordenesCarpeta(iEscenario.Carpeta) = 1
            End If
            iEscenario.Editable = IIf(iEscenario.Propietario = UsuCod, True, iEscenario.Editable)
            dEscenarios(iEscenario.Id) = iEscenario
        Next

        Return {dEscenarios, idEscenarioSeleccionado}
    End Function
    Public Function FSN_Get_Escenario_Defecto(ByVal UsuCod As String, ByVal Idioma As String, ByVal TipoVisor As Integer, _
                    ByVal EscenarioPorDefecto As Integer, ByVal posicionEscenarioDefecto As Integer, ByVal escenarioDefectoFavorito As Boolean) As Escenario
        Dim Textos As DataTable
        Dim oDict As Dictionary = New Dictionary(DBServer, mIsAuthenticated)
        oDict.LoadData(TiposDeDatos.ModulosIdiomas.Seguimiento, Idioma)
        Textos = oDict.Data.Tables(0)

        Dim iEscenario As New Escenario
        Dim iEscenarioFiltro As EscenarioFiltro
        Dim iEscenarioFiltroCampo, iEscenarioCampoVista As Escenario_Campo
        Dim iEscenarioVista As EscenarioVista
        Dim iEscenarioFiltroCondicion As EscenarioFiltroCondicion
        Dim ordenCampoVista As Integer = 0

        With iEscenario
            .Cargado = True
            .Id = 0
            .Carpeta = 0
            .Nombre = Textos(1)(1)
            .Defecto = EscenarioPorDefecto
            .Favorito = escenarioDefectoFavorito
            .AplicaTodas = True
            .TipoVisor = TipoVisor
            .Posicion = posicionEscenarioDefecto

            For i As Integer = 1 To 7
                iEscenarioFiltro = New EscenarioFiltro
                With iEscenarioFiltro
                    .Id = i
                    .Escenario = iEscenario.Id
                    .Nombre = Textos(1 + i)(1)
                    .FormulaAvanzada = False
                    .Defecto = (i = 1)
                    iEscenarioFiltroCondicion = New EscenarioFiltroCondicion
                    For Each escenarioFiltroCampo As Integer In {CamposGeneralesVisor.DENOMINACION, CamposGeneralesVisor.ORIGEN, CamposGeneralesVisor.FECHAALTA, _
                            CamposGeneralesVisor.IMPORTE, CamposGeneralesVisor.TIPOSOLICITUD, CamposGeneralesVisor.PETICIONARIO, CamposGeneralesVisor.ESTADO}
                        iEscenarioFiltroCampo = New Escenario_Campo
                        With iEscenarioFiltroCampo
                            iEscenarioFiltroCampo = Get_Campo_General(escenarioFiltroCampo, Idioma)
                        End With
                        .Filtro_Campos.Add(iEscenarioFiltroCampo)
                    Next
                    'EN EL ESCENARIO POR DEFECTO FILTRAREMOS POR ESTADO Y OBTENDREMOS LAS SOLICITUDES HASTA HOY Y DESDE EL PARAMETRO QUE INDIQUE EL WEB.CONFIG
                    'AÑADIMOS LA CONDICION DEL ESTADO
                    iEscenarioFiltroCondicion = New EscenarioFiltroCondicion
                    With iEscenarioFiltroCondicion
                        .IdFiltro = iEscenarioFiltro.Id
                        .Orden = 1
                        .EsLista = True
                        .IdCampo = CamposGeneralesVisor.ESTADO
                        .TipoCampo = TiposDeDatos.TipoGeneral.TipoString
                        .EsCampoGeneral = True
                        .Denominacion = Textos(89)(1)
                        .Denominacion_BD = "ESTADO"
                        .Operador = Operadores.Campos.ES
                        Select Case i
                            Case 1
                                .Valores.Add("1000###" & Textos(2)(1)) 'Pendientes
                            Case 2
                                .Valores.Add("0###" & Textos(3)(1)) 'Guardadas
                            Case 3
                                .Valores.Add("2###" & Textos(4)(1)) 'En curso
                            Case 4
                                .Valores.Add("6###" & Textos(5)(1)) 'Rechazadas
                            Case 5
                                .Valores.Add("8###" & Textos(6)(1)) 'Anuladas
                            Case 6
                                .Valores.Add("100#101#102#103###" & Textos(7)(1)) 'Finalizadas
                            Case Else
                                .Valores.Add("104###" & Textos(8)(1)) 'Cerradas
                        End Select
                    End With
                    .FormulaCondiciones.Add(iEscenarioFiltroCondicion)
                    If i > 1 Then 'Para las pendientes no limitamos la busqueda a los ultimos * mesese
                        'AÑADIMOS LA CONDICION AND
                        iEscenarioFiltroCondicion = New EscenarioFiltroCondicion
                        iEscenarioFiltroCondicion.IdCampo = 0
                        iEscenarioFiltroCondicion.Operador = Operadores.Formula.Y
                        iEscenarioFiltroCondicion.Orden = 2
                        .FormulaCondiciones.Add(iEscenarioFiltroCondicion)
                        iEscenarioFiltroCondicion = New EscenarioFiltroCondicion
                        With iEscenarioFiltroCondicion
                            .IdFiltro = iEscenarioFiltro.Id
                            .Orden = 3
                            .IdCampo = CamposGeneralesVisor.FECHAALTA
                            .TipoCampo = TiposDeDatos.TipoGeneral.TipoFecha
                            .EsCampoGeneral = True
                            .Denominacion = Textos(43)(1)
                            .Denominacion_BD = "FECHA"
                            .Operador = Operadores.Campos.MAYORIGUAL
                            TimeZoneInfo.GetSystemTimeZones()
                            .Valores.Add(DateAdd(DateInterval.Hour, _
                                                 TimeZoneInfo.Local.GetUtcOffset(Now).Hours, _
                                                 DateAdd(DateInterval.Month, _
                                                        (-1) * CType(ConfigurationManager.AppSettings("MesesVisor"), Integer), _
                                                        Now)).ToUniversalTime.ToString("o"))
                        End With
                        .FormulaCondiciones.Add(iEscenarioFiltroCondicion)
                    End If
                End With
                .EscenarioFiltros(iEscenarioFiltro.Id) = iEscenarioFiltro
            Next
            iEscenarioVista = New EscenarioVista
            With iEscenarioVista
                .Id = 1
                .Escenario = iEscenario.Id
                .Nombre = Textos(9)(1)
                .Defecto = True
                .Orden = "FECHA DESC"
                For Each campoVista As Integer In {CamposGeneralesVisor.TIPOSOLICITUD, CamposGeneralesVisor.DENOMINACION, _
                            CamposGeneralesVisor.IDENTIFICADOR, CamposGeneralesVisor.FECHAALTA, CamposGeneralesVisor.USUARIO, _
                            CamposGeneralesVisor.SITUACIONACTUAL, CamposGeneralesVisor.PETICIONARIO, _
                            CamposGeneralesVisor.DEPARTAMENTO, CamposGeneralesVisor.IMPORTE, CamposGeneralesVisor.MARCASEGUIMIENTO}
                    iEscenarioCampoVista = Get_Campo_General(campoVista, Idioma)
                    iEscenarioCampoVista.OrdenVisualizacion = ordenCampoVista
                    .Campos_Vista.Add(iEscenarioCampoVista)
                    ordenCampoVista += 1
                Next
            End With
            .EscenarioVistas(iEscenarioVista.Id) = iEscenarioVista
        End With

        Return iEscenario
    End Function
    Public Function FSN_Get_Carpetas_Usuario(ByVal UsuCod As String, ByVal TipoVisor As Integer) As List(Of cn_fsTreeViewItem)
        Authenticate()
        Dim dtCarpetas As DataTable = DBServer.FSN_Get_Carpetas_Usuario(UsuCod, TipoVisor)
        Return Carpetas_Usuario(dtCarpetas)
    End Function
    Public Function FSN_Insert_Carpeta_Usuario(ByVal UsuCod As String, ByVal Nombre As String, ByVal Carpeta As Integer, ByVal TipoVisor As Integer) As List(Of cn_fsTreeViewItem)
        Authenticate()
        Dim dtCarpetas As DataTable = DBServer.FSN_Insert_Carpeta_Usuario(UsuCod, Nombre, Carpeta, TipoVisor)
        Return Carpetas_Usuario(dtCarpetas)
    End Function
    Public Function FSN_Update_Carpeta_Usuario(ByVal IdCarpeta As Integer, ByVal UsuCod As String, ByVal Nombre As String, ByVal TipoVisor As Integer) As List(Of cn_fsTreeViewItem)
        Authenticate()
        Dim dtCarpetas As DataTable = DBServer.FSN_Update_Carpeta_Usuario(IdCarpeta, UsuCod, Nombre, TipoVisor)
        Return Carpetas_Usuario(dtCarpetas)
    End Function
    Public Function FSN_Delete_Carpeta_Usuario(ByVal IdCarpeta As Integer, ByVal UsuCod As String, ByVal TipoVisor As Integer) As List(Of cn_fsTreeViewItem)
        Authenticate()
        Dim dtCarpetas As DataTable = DBServer.FSN_Delete_Carpeta_Usuario(IdCarpeta, UsuCod, TipoVisor)
        Return Carpetas_Usuario(dtCarpetas)
    End Function
    Public Function FSN_Paste_Carpeta_Usuario(ByVal IdCarpeta As Integer, ByVal Id_Destino As Integer, ByVal UsuCod As String, ByVal TipoVisor As Integer) As List(Of cn_fsTreeViewItem)
        Authenticate()
        Dim dtCarpetas As DataTable = DBServer.FSN_Paste_Carpeta_Usuario(IdCarpeta, Id_Destino, UsuCod, TipoVisor)
        Return Carpetas_Usuario(dtCarpetas)
    End Function
    Public Function FSN_Get_Escenario_Usuario(ByVal IdEscenario As Integer, ByVal UsuCod As String, ByVal Idioma As String, ByVal TipoVisor As Integer, ByVal Edicion As Boolean, _
            ByVal PermisoEditarEscenariosOtrosUsu As Boolean, ByVal RestricEdicionUsuDep As Boolean, ByVal RestricEdicionUsuUON As Boolean, ByVal RestricEdicionUsuPerfUON As Boolean) As List(Of Object)
        Authenticate()

        Dim dsEscenario As DataSet = DBServer.FSN_Get_Escenarios_Usuario(UsuCod, Idioma, TipoVisor, IdEscenario, True, Edicion, RestricEdicionUsuDep, RestricEdicionUsuUON, RestricEdicionUsuPerfUON)
        Dim iEscenario As New Escenario
        Dim lCarpetas As New List(Of cn_fsItem)
        Dim lSolicitudes As New List(Of cn_fsItem)
        If Not dsEscenario.Tables("ESCENARIOS_USUARIO").Rows.Count = 0 Then
            Dim escenario As DataRow = dsEscenario.Tables("ESCENARIOS_USUARIO").Rows(0)
            iEscenario = Create_Escenario(Idioma, escenario, _
                    dsEscenario.Tables("ESCENARIO_SOLICITUD_FORMULARIOS_USUARIO").Select("ESCENARIO=" & escenario("ID")), _
                    dsEscenario.Tables("ESCENARIO_FILTROS_USUARIO").Select("ESCENARIO=" & escenario("ID")), _
                    dsEscenario.Tables("ESCENARIO_FILTRO_CAMPOS_USUARIO").Select("ESCENARIO=" & escenario("ID")), _
                    dsEscenario.Tables("ESCENARIO_FILTRO_CONDICIONES_USUARIO").Select("ESCENARIO=" & escenario("ID")), _
                    dsEscenario.Tables("ESCENARIO_VISTAS_USUARIO").Select("ESCENARIO=" & escenario("ID")), _
                    dsEscenario.Tables("ESCENARIO_VISTA_CAMPOS_USUARIO").Select("ESCENARIO=" & escenario("ID")), PermisoEditarEscenariosOtrosUsu)
        End If
        If Edicion Then
            If Not dsEscenario.Tables("SOLICITUD_FORMULARIO_USUARIO").Rows.Count = 0 Then
                Dim oSolicitud As cn_fsItem
                For Each solicitud As DataRow In dsEscenario.Tables("SOLICITUD_FORMULARIO_USUARIO").Rows
                    oSolicitud = New cn_fsItem
                    oSolicitud.value = String.Format("{0}###{1}", DBNullToInteger(solicitud("FORMULARIO")), DBNullToInteger(solicitud("ID")))
                    oSolicitud.text = solicitud("DEN").ToString
                    lSolicitudes.Add(oSolicitud)
                Next
            End If
            If Not dsEscenario.Tables("CARPETAS_USUARIO").Rows.Count = 0 Then
                Dim oCarpeta As cn_fsItem
                oCarpeta = New cn_fsItem
                oCarpeta.value = 0
                oCarpeta.text = ""
                lCarpetas.Add(oCarpeta)
                For Each carpeta As DataRow In dsEscenario.Tables("CARPETAS_USUARIO").Rows
                    oCarpeta = New cn_fsItem
                    oCarpeta.value = DBNullToInteger(carpeta("ID"))
                    oCarpeta.text = carpeta("NOMBRE").ToString
                    lCarpetas.Add(oCarpeta)
                Next
            End If
        End If
        iEscenario.Editable = IIf(iEscenario.Propietario = UsuCod, True, iEscenario.Editable)
        Dim oResultado As New List(Of Object)
        With oResultado
            .Add(iEscenario)
            .Add(lSolicitudes)
            .Add(lCarpetas)
        End With
        Dim lCamposGenerales As New Dictionary(Of String, Escenario_Campo)
        For Each campoGeneral As Integer In [Enum].GetValues(GetType(CamposGeneralesVisor))
            lCamposGenerales(campoGeneral) = Get_Campo_General(campoGeneral, Idioma)
        Next
        oResultado.Add(lCamposGenerales)
        Return oResultado
    End Function
    ''' <summary>
    ''' Carga los campos del escenario
    ''' </summary>
    ''' <param name="PerCod">Persona</param>
    ''' <param name="Idioma">Idioma</param>
    ''' <param name="IdFormulario">Formulario</param>
    ''' <param name="Paso">1-filtro 2-wizard 3-Vista</param>
    ''' <returns>lista de Escenario_Campo</returns>
    ''' <remarks>llamada desde:VisorSolicitudes.aspx.vb/Obtener_Campos_Formulario; Tiempo mÃƒÂ¡ximo:0,2</remarks>
    Public Function FSN_Get_Campos_Formulario(ByVal PerCod As String, ByVal Idioma As String, ByVal IdFormulario As Integer, ByVal Paso As Integer) As Dictionary(Of String, Escenario_Campo)
        Authenticate()
        Dim dtCamposFormulario As DataTable = DBServer.FSN_Get_Campos_Formulario(PerCod, Idioma, IdFormulario, (Paso = 3))
        Dim dCampos_Formulario As New Dictionary(Of String, Escenario_Campo)
        Dim oCampo_Formulario, oCampo_Formulario_Desglose As Escenario_Campo
        Dim ordenFormulario, ordenFormularioDesglose As Integer
        ordenFormulario = 0
        Dim campoPadre As Integer
        For Each campo As DataRow In dtCamposFormulario.Select("ES_SUBCAMPO=0", "ORDEN_GRUPO,ORDEN_CAMPO")
            oCampo_Formulario = New Escenario_Campo
            With oCampo_Formulario
                .EsCampoGeneral = False
                .EsCampoDesglose = False
                .Id = DBNullToInteger(campo("ID"))
                .Denominacion = campo("DEN").ToString
                .TipoCampo = DBNullToInteger(campo("SUBTIPO"))
                .TipoCampoGS = DBNullToInteger(campo("TIPO_CAMPO_GS"))
                .EsLista = DBNullToBoolean(campo("INTRO"))
                Select Case .TipoCampoGS
                    Case TiposDeDatos.TipoCampoGS.FormaPago, TiposDeDatos.TipoCampoGS.Moneda, TiposDeDatos.TipoCampoGS.Unidad, _
                        TiposDeDatos.TipoCampoGS.UnidadPedido, TiposDeDatos.TipoCampoGS.Dest, TiposDeDatos.TipoCampoGS.Departamento, _
                        TiposDeDatos.TipoCampoGS.OrganizacionCompras, TiposDeDatos.TipoCampoGS.Centro, TiposDeDatos.TipoCampoGS.Almacen, _
                        TiposDeDatos.TipoCampoGS.TipoPedido
                        .EsLista = True
                    Case TiposDeDatos.TipoCampoGS.Pais
                        .EsLista = True
                        campoPadre = .Id
                    Case TiposDeDatos.TipoCampoGS.Provincia
                        .EsLista = True
                        .CampoPadre = campoPadre
                        dCampos_Formulario(campoPadre).CampoHijo = .Id
                    Case TiposDeDatos.TipoCampoGS.PRES1, TiposDeDatos.TipoCampoGS.Pres2, TiposDeDatos.TipoCampoGS.Pres3, TiposDeDatos.TipoCampoGS.Pres4, _
                        TiposDeDatos.TipoCampoGS.Partida, TiposDeDatos.TipoCampoGS.CentroCoste, TiposDeDatos.TipoCampoGS.Factura, _
                        TiposDeDatos.TipoCampoGS.InicioAbono, TiposDeDatos.TipoCampoGS.FinAbono, TiposDeDatos.TipoCampoGS.RetencionEnGarantia
                        .NoFiltrar = True
                End Select
                .Denominacion_BD = "C_" & DBNullToInteger(campo("TIPO_CAMPO_GS")) & "_" & DBNullToInteger(campo("ID"))
                .OrdenFormulario = ordenFormulario
                ordenFormularioDesglose = 0
                .CamposDesglose = New Dictionary(Of String, Escenario_Campo)
                If .TipoCampo = TiposDeDatos.TipoGeneral.TipoDesglose Then
                    For Each campoDesglose As DataRow In dtCamposFormulario.Select("CAMPO_PADRE=" & oCampo_Formulario.Id, "ORDEN_CAMPO")
                        oCampo_Formulario_Desglose = New Escenario_Campo
                        With oCampo_Formulario_Desglose
                            .EsCampoGeneral = False
                            .EsCampoDesglose = True
                            .Id = DBNullToInteger(campoDesglose("ID"))
                            .CampoDesglose = DBNullToInteger(campoDesglose("CAMPO_PADRE"))
                            .Denominacion = campoDesglose("DEN").ToString
                            .TipoCampo = DBNullToInteger(campoDesglose("SUBTIPO"))
                            .TipoCampoGS = DBNullToInteger(campoDesglose("TIPO_CAMPO_GS"))
                            .EsLista = DBNullToBoolean(campoDesglose("INTRO"))
                            Select Case .TipoCampoGS
                                Case TiposDeDatos.TipoCampoGS.FormaPago, TiposDeDatos.TipoCampoGS.Moneda, TiposDeDatos.TipoCampoGS.Unidad, _
                                    TiposDeDatos.TipoCampoGS.UnidadPedido, TiposDeDatos.TipoCampoGS.Dest, TiposDeDatos.TipoCampoGS.Departamento, _
                                    TiposDeDatos.TipoCampoGS.OrganizacionCompras, TiposDeDatos.TipoCampoGS.Centro, TiposDeDatos.TipoCampoGS.Almacen, _
                                    TiposDeDatos.TipoCampoGS.TipoPedido
                                    .EsLista = True
                                Case TiposDeDatos.TipoCampoGS.Pais
                                    .EsLista = True
                                    campoPadre = .Id
                                Case TiposDeDatos.TipoCampoGS.Provincia
                                    .EsLista = True
                                    .CampoPadre = campoPadre
                                    oCampo_Formulario.CamposDesglose(campoPadre).CampoHijo = .Id
                                Case TiposDeDatos.TipoCampoGS.PRES1, TiposDeDatos.TipoCampoGS.Pres2, TiposDeDatos.TipoCampoGS.Pres3, TiposDeDatos.TipoCampoGS.Pres4, _
                                    TiposDeDatos.TipoCampoGS.Partida, TiposDeDatos.TipoCampoGS.CentroCoste, TiposDeDatos.TipoCampoGS.Factura, _
                                    TiposDeDatos.TipoCampoGS.InicioAbono, TiposDeDatos.TipoCampoGS.FinAbono, TiposDeDatos.TipoCampoGS.RetencionEnGarantia
                                    .NoFiltrar = True
                            End Select
                            .Denominacion_BD = "C_" & DBNullToInteger(campoDesglose("CAMPO_PADRE")) & "_" & DBNullToInteger(campoDesglose("TIPO_CAMPO_GS")) & "_" & DBNullToInteger(campoDesglose("ID"))
                            .OrdenFormulario = ordenFormularioDesglose
                            ordenFormularioDesglose += 1
                        End With
                        .CamposDesglose(oCampo_Formulario_Desglose.Id) = oCampo_Formulario_Desglose
                    Next
                End If
            End With
            ordenFormulario += 1
            dCampos_Formulario(oCampo_Formulario.Id) = oCampo_Formulario
        Next
        Return dCampos_Formulario
    End Function
    Public Function FSN_Insert_Escenario_Usuario(ByVal UsuCod As String, ByVal Idioma As String, ByVal EscenarioNuevo As Escenario, ByVal OcultarEscenarioDefecto As Boolean, _
                            ByVal PermisoEditarEscenariosOtrosUsu As Boolean, ByVal RestricEdicionUsuDep As Boolean, ByVal RestricEdicionUsuUON As Boolean, ByVal RestricEdicionUsuPerfUON As Boolean) As Escenario
        Authenticate()
        Dim dsFiltrosVistas As New DataSet
        Dim dtEscenarioFitros As New DataTable
        Dim drEscenarioFiltro As DataRow
        With dtEscenarioFitros
            .Columns.Add("ID", GetType(System.Int32))
            .Columns.Add("ESCENARIO", GetType(System.Int32))
            .Columns.Add("USUARIO", GetType(System.String))
            .Columns.Add("NOMBRE", GetType(System.String))
            .Columns.Add("DEFECTO", GetType(System.Boolean))
            .Columns.Add("FORMULAAVANZADA", GetType(System.Boolean))
            .TableName = "FILTROS"
        End With
        Dim dtEscenarioFiltroCampos As New DataTable
        Dim drEscenarioFiltroCampo As DataRow
        With dtEscenarioFiltroCampos
            .Columns.Add("FILTRO", GetType(System.Int32))
            .Columns.Add("CAMPO", GetType(System.Int32))
            .Columns.Add("ESGENERAL", GetType(System.Boolean))
            .Columns.Add("VISIBLE", GetType(System.Boolean))
            .TableName = "FILTRO_CAMPOS"
        End With
        Dim dtEscenarioFiltroCondiciones As New DataTable
        Dim drEscenarioFiltroCondicion As DataRow
        With dtEscenarioFiltroCondiciones
            .Columns.Add("FILTRO", GetType(System.Int32))
            .Columns.Add("ORDEN", GetType(System.Int32))
            .Columns.Add("CAMPO", GetType(System.Int32))
            .Columns.Add("ESGENERAL", GetType(System.Boolean))
            .Columns.Add("DENOMINACION", GetType(System.String))
            .Columns.Add("DENOMINACION_BD", GetType(System.String))
            .Columns.Add("OPERADOR", GetType(System.Int16))
            .Columns.Add("VALOR", GetType(System.String))
            .TableName = "FILTRO_CONDICIONES"
        End With
        Dim dtEscenarioVistas As New DataTable
        Dim drEscenarioVista As DataRow
        With dtEscenarioVistas
            .Columns.Add("ID", GetType(System.Int32))
            .Columns.Add("ESCENARIO", GetType(System.Int32))
            .Columns.Add("USUARIO", GetType(System.String))
            .Columns.Add("NOMBRE", GetType(System.String))
            .Columns.Add("DEFECTO", GetType(System.Boolean))
            .Columns.Add("ORDEN", GetType(System.String))
            .TableName = "VISTAS"
        End With
        Dim dtEscenarioVistaCampos As New DataTable
        Dim drEscenarioVistaCampo As DataRow
        With dtEscenarioVistaCampos
            .Columns.Add("VISTA", GetType(System.Int32))
            .Columns.Add("CAMPO", GetType(System.Int32))
            .Columns.Add("ESGENERAL", GetType(System.Boolean))
            .Columns.Add("ESDESGLOSE", GetType(System.Boolean))
            .Columns.Add("NOMBREPERSONALIZADO", GetType(System.String))
            .Columns.Add("ANCHO", GetType(System.Double))
            .Columns.Add("POSICION", GetType(System.Int32))
            .TableName = "VISTA_CAMPOS"
        End With

        For Each Filtro As KeyValuePair(Of String, EscenarioFiltro) In EscenarioNuevo.EscenarioFiltros
            drEscenarioFiltro = dtEscenarioFitros.NewRow
            drEscenarioFiltro("ID") = Filtro.Key
            drEscenarioFiltro("ESCENARIO") = Filtro.Value.Escenario
            drEscenarioFiltro("USUARIO") = UsuCod
            drEscenarioFiltro("NOMBRE") = Filtro.Value.Nombre
            drEscenarioFiltro("DEFECTO") = Filtro.Value.Defecto
            drEscenarioFiltro("FORMULAAVANZADA") = Filtro.Value.FormulaAvanzada
            dtEscenarioFitros.Rows.Add(drEscenarioFiltro)

            For Each campoFiltro As Escenario_Campo In EscenarioNuevo.EscenarioFiltros(Filtro.Key).Filtro_Campos
                drEscenarioFiltroCampo = dtEscenarioFiltroCampos.NewRow
                drEscenarioFiltroCampo("FILTRO") = Filtro.Key
                drEscenarioFiltroCampo("CAMPO") = campoFiltro.Id
                drEscenarioFiltroCampo("ESGENERAL") = campoFiltro.EsCampoGeneral
                drEscenarioFiltroCampo("VISIBLE") = campoFiltro.Visible
                dtEscenarioFiltroCampos.Rows.Add(drEscenarioFiltroCampo)
            Next

            Dim sValorCondicion As String
            For Each condicionFiltro As EscenarioFiltroCondicion In EscenarioNuevo.EscenarioFiltros(Filtro.Key).FormulaCondiciones
                drEscenarioFiltroCondicion = dtEscenarioFiltroCondiciones.NewRow
                drEscenarioFiltroCondicion("FILTRO") = Filtro.Key
                drEscenarioFiltroCondicion("ORDEN") = condicionFiltro.Orden
                drEscenarioFiltroCondicion("CAMPO") = condicionFiltro.IdCampo
                drEscenarioFiltroCondicion("ESGENERAL") = condicionFiltro.EsCampoGeneral
                drEscenarioFiltroCondicion("DENOMINACION") = condicionFiltro.Denominacion
                drEscenarioFiltroCondicion("DENOMINACION_BD") = condicionFiltro.Denominacion_BD
                drEscenarioFiltroCondicion("OPERADOR") = condicionFiltro.Operador
                sValorCondicion = String.Empty
                If Not condicionFiltro.IdCampo = 0 Then
                    For Each valor As String In condicionFiltro.Valores
                        sValorCondicion &= valor & "#|#"
                    Next
                    sValorCondicion = Left(sValorCondicion, sValorCondicion.Length - 3)
                End If
                drEscenarioFiltroCondicion("VALOR") = sValorCondicion
                dtEscenarioFiltroCondiciones.Rows.Add(drEscenarioFiltroCondicion)
            Next
        Next
        Dim key As String
        For Each Vista As KeyValuePair(Of String, EscenarioVista) In EscenarioNuevo.EscenarioVistas
            key = Vista.Key
            drEscenarioVista = dtEscenarioVistas.NewRow
            drEscenarioVista("ID") = Vista.Key
            drEscenarioVista("ESCENARIO") = Vista.Value.Escenario
            drEscenarioVista("USUARIO") = UsuCod
            drEscenarioVista("NOMBRE") = Vista.Value.Nombre
            drEscenarioVista("DEFECTO") = Vista.Value.Defecto
            drEscenarioVista("ORDEN") = String.Join(",", Split(Vista.Value.Orden, ",").Where(Function(x) EscenarioNuevo.EscenarioVistas(key).Campos_Vista.Any(Function(y) y.Denominacion_BD = Split(x, " ")(0))))
            dtEscenarioVistas.Rows.Add(drEscenarioVista)

            For Each campoVista As Escenario_Campo In EscenarioNuevo.EscenarioVistas(Vista.Key).Campos_Vista
                drEscenarioVistaCampo = dtEscenarioVistaCampos.NewRow
                drEscenarioVistaCampo("VISTA") = Vista.Key
                drEscenarioVistaCampo("CAMPO") = campoVista.Id
                drEscenarioVistaCampo("ESGENERAL") = campoVista.EsCampoGeneral
                drEscenarioVistaCampo("ESDESGLOSE") = campoVista.EsCampoDesglose
                drEscenarioVistaCampo("NOMBREPERSONALIZADO") = campoVista.NombrePersonalizado
                drEscenarioVistaCampo("ANCHO") = campoVista.AnchoVisualizacion
                drEscenarioVistaCampo("POSICION") = campoVista.OrdenVisualizacion
                dtEscenarioVistaCampos.Rows.Add(drEscenarioVistaCampo)
            Next
        Next
        dsFiltrosVistas.Tables.Add(dtEscenarioFitros)
        dsFiltrosVistas.Tables.Add(dtEscenarioFiltroCampos)
        dsFiltrosVistas.Tables.Add(dtEscenarioFiltroCondiciones)
        dsFiltrosVistas.Tables.Add(dtEscenarioVistas)
        dsFiltrosVistas.Tables.Add(dtEscenarioVistaCampos)

        Dim dtFormulariosVinculados As New DataTable
        Dim drFormulariosVinculados As DataRow
        With dtFormulariosVinculados
            .Columns.Add("SOLICITUD", GetType(System.Int32))
            .Columns.Add("FORMULARIO", GetType(System.Int32))
        End With
        For Each formulario As KeyValuePair(Of String, SerializableKeyValuePair(Of Integer, String)) In EscenarioNuevo.SolicitudFormularioVinculados
            drFormulariosVinculados = dtFormulariosVinculados.NewRow
            drFormulariosVinculados("SOLICITUD") = CType(formulario.Key, Integer)
            drFormulariosVinculados("FORMULARIO") = formulario.Value.Key
            dtFormulariosVinculados.Rows.Add(drFormulariosVinculados)
        Next

        Dim dsEscenario As DataSet = DBServer.FSN_Insert_Escenario_Usuario(UsuCod, Idioma, EscenarioNuevo.Carpeta, EscenarioNuevo.Nombre, _
                EscenarioNuevo.Defecto, EscenarioNuevo.Favorito, EscenarioNuevo.AplicaTodas, EscenarioNuevo.TipoVisor, _
                dtFormulariosVinculados, dsFiltrosVistas, OcultarEscenarioDefecto, EscenarioNuevo.Posicion, RestricEdicionUsuDep, RestricEdicionUsuUON, RestricEdicionUsuPerfUON)

        Dim iEscenario As New Escenario
        'Creamos la estructura del escenario, con sus filtros vinculados y estos a sus vistas.
        If Not dsEscenario.Tables("ESCENARIOS_USUARIO").Rows.Count = 0 Then
            Dim escenario As DataRow = dsEscenario.Tables("ESCENARIOS_USUARIO").Rows(0)
            iEscenario = Create_Escenario(Idioma, escenario, _
                    dsEscenario.Tables("ESCENARIO_SOLICITUD_FORMULARIOS_USUARIO").Select("ESCENARIO=" & escenario("ID")), _
                    dsEscenario.Tables("ESCENARIO_FILTROS_USUARIO").Select("ESCENARIO=" & escenario("ID")), _
                    dsEscenario.Tables("ESCENARIO_FILTRO_CAMPOS_USUARIO").Select("ESCENARIO=" & escenario("ID")), _
                    dsEscenario.Tables("ESCENARIO_FILTRO_CONDICIONES_USUARIO").Select("ESCENARIO=" & escenario("ID")), _
                    dsEscenario.Tables("ESCENARIO_VISTAS_USUARIO").Select("ESCENARIO=" & escenario("ID")), _
                    dsEscenario.Tables("ESCENARIO_VISTA_CAMPOS_USUARIO").Select("ESCENARIO=" & escenario("ID")), PermisoEditarEscenariosOtrosUsu)
        End If
        iEscenario.Editable = IIf(iEscenario.Propietario = UsuCod, True, iEscenario.Editable)
        Return iEscenario
    End Function
    Public Sub FSN_Delete_Escenario_Usuario(ByVal Usuario As String, ByVal IdEscenario As Integer, ByVal Carpeta As Integer)
        Authenticate()

        Dim idEscenarioPorDefecto As Integer = 0
        Dim dEscenarios As New Dictionary(Of String, Escenario)
        DBServer.FSN_Delete_Escenario_Usuario(Usuario, IdEscenario, Carpeta)
    End Sub
    Public Function FSN_Edit_Escenario_Usuario(ByVal UsuCod As String, ByVal Idioma As String, ByVal EscenarioNuevo As Escenario, _
                ByVal PermisoEditarEscenariosOtrosUsu As Boolean, ByVal RestricEdicionUsuDep As Boolean, ByVal RestricEdicionUsuUON As Boolean, ByVal RestricEdicionUsuPerfUON As Boolean) As Escenario
        Authenticate()

        Dim dsEscenario As DataSet = DBServer.FSN_Edit_Escenario_Usuario(UsuCod, Idioma, EscenarioNuevo.Id, EscenarioNuevo.Carpeta, EscenarioNuevo.Nombre, _
                EscenarioNuevo.Defecto, EscenarioNuevo.Favorito, EscenarioNuevo.AplicaTodas, EscenarioNuevo.TipoVisor, RestricEdicionUsuDep, RestricEdicionUsuUON, RestricEdicionUsuPerfUON)

        Dim iEscenario As New Escenario
        'Creamos la estructura del escenario, con sus filtros vinculados y estos a sus vistas.
        If Not dsEscenario.Tables("ESCENARIOS_USUARIO").Rows.Count = 0 Then
            Dim escenario As DataRow = dsEscenario.Tables("ESCENARIOS_USUARIO").Rows(0)
            iEscenario = Create_Escenario(Idioma, escenario, _
                    dsEscenario.Tables("ESCENARIO_SOLICITUD_FORMULARIOS_USUARIO").Select("ESCENARIO=" & escenario("ID")), _
                    dsEscenario.Tables("ESCENARIO_FILTROS_USUARIO").Select("ESCENARIO=" & escenario("ID")), _
                    dsEscenario.Tables("ESCENARIO_FILTRO_CAMPOS_USUARIO").Select("ESCENARIO=" & escenario("ID")), _
                    dsEscenario.Tables("ESCENARIO_FILTRO_CONDICIONES_USUARIO").Select("ESCENARIO=" & escenario("ID")), _
                    dsEscenario.Tables("ESCENARIO_VISTAS_USUARIO").Select("ESCENARIO=" & escenario("ID")), _
                    dsEscenario.Tables("ESCENARIO_VISTA_CAMPOS_USUARIO").Select("ESCENARIO=" & escenario("ID")), PermisoEditarEscenariosOtrosUsu)
        End If
        iEscenario.Editable = IIf(iEscenario.Propietario = UsuCod, True, iEscenario.Editable)
        Return iEscenario
    End Function
    Public Function FSN_Insert_Escenario_Filtro_Usuario(ByVal Usuario As String, ByVal Idioma As String, ByVal FiltroNuevo As EscenarioFiltro, ByVal TipoVisor As Integer) As EscenarioFiltro
        Authenticate()
        Dim dtEscenarioFiltroCampos As New DataTable
        dtEscenarioFiltroCampos.Columns.Add("CAMPO", GetType(System.Int32))
        dtEscenarioFiltroCampos.Columns.Add("ESGENERAL", GetType(System.Boolean))
        dtEscenarioFiltroCampos.Columns.Add("VISIBLE", GetType(System.Boolean))
        Dim rEscenarioFiltroCampo As DataRow
        For Each campoFiltro As Escenario_Campo In FiltroNuevo.Filtro_Campos
            rEscenarioFiltroCampo = dtEscenarioFiltroCampos.NewRow
            rEscenarioFiltroCampo("CAMPO") = campoFiltro.Id
            rEscenarioFiltroCampo("ESGENERAL") = campoFiltro.EsCampoGeneral
            rEscenarioFiltroCampo("VISIBLE") = campoFiltro.Visible
            dtEscenarioFiltroCampos.Rows.Add(rEscenarioFiltroCampo)
        Next
        Dim dtEscenarioFiltroCondiciones As New DataTable
        dtEscenarioFiltroCondiciones.Columns.Add("ORDEN", GetType(System.Int32))
        dtEscenarioFiltroCondiciones.Columns.Add("CAMPO", GetType(System.Int32))
        dtEscenarioFiltroCondiciones.Columns.Add("ESGENERAL", GetType(System.Boolean))
        dtEscenarioFiltroCondiciones.Columns.Add("DENOMINACION", GetType(System.String))
        dtEscenarioFiltroCondiciones.Columns.Add("DENOMINACION_BD", GetType(System.String))
        dtEscenarioFiltroCondiciones.Columns.Add("OPERADOR", GetType(System.Int16))
        dtEscenarioFiltroCondiciones.Columns.Add("VALOR", GetType(System.String))
        Dim rEscenarioFiltroCondicion As DataRow
        Dim sValorCondicion As String
        For Each condicionFiltro As EscenarioFiltroCondicion In FiltroNuevo.FormulaCondiciones
            rEscenarioFiltroCondicion = dtEscenarioFiltroCondiciones.NewRow
            rEscenarioFiltroCondicion("ORDEN") = condicionFiltro.Orden
            rEscenarioFiltroCondicion("CAMPO") = condicionFiltro.IdCampo
            rEscenarioFiltroCondicion("ESGENERAL") = condicionFiltro.EsCampoGeneral
            rEscenarioFiltroCondicion("DENOMINACION") = condicionFiltro.Denominacion
            rEscenarioFiltroCondicion("DENOMINACION_BD") = condicionFiltro.Denominacion_BD
            rEscenarioFiltroCondicion("OPERADOR") = condicionFiltro.Operador
            sValorCondicion = String.Empty
            For Each valor As String In condicionFiltro.Valores
                sValorCondicion &= IIf(sValorCondicion Is String.Empty, "", "#|#") & valor
            Next
            rEscenarioFiltroCondicion("VALOR") = sValorCondicion
            dtEscenarioFiltroCondiciones.Rows.Add(rEscenarioFiltroCondicion)
        Next

        Dim oInfoEscenarioNuevo As Object = DBServer.FSN_Insert_Escenario_Filtro_Usuario(Usuario, Idioma, FiltroNuevo.Escenario, TipoVisor, _
                FiltroNuevo.Nombre, FiltroNuevo.Defecto, FiltroNuevo.FormulaAvanzada, FiltroNuevo.Posicion, dtEscenarioFiltroCampos, dtEscenarioFiltroCondiciones)
        Dim dsEscenario As DataSet = oInfoEscenarioNuevo(0)
        Dim iEscenario As New Escenario
        'Creamos la estructura del escenario, con sus filtros vinculados y estos a sus vistas.
        If Not dsEscenario.Tables("ESCENARIOS_USUARIO").Rows.Count = 0 Then
            Dim escenario As DataRow = dsEscenario.Tables("ESCENARIOS_USUARIO").Rows(0)
            iEscenario = Create_Escenario(Idioma, escenario, _
                    dsEscenario.Tables("ESCENARIO_SOLICITUD_FORMULARIOS_USUARIO").Select("ESCENARIO=" & escenario("ID")), _
                    dsEscenario.Tables("ESCENARIO_FILTROS_USUARIO").Select("ESCENARIO=" & escenario("ID")), _
                    dsEscenario.Tables("ESCENARIO_FILTRO_CAMPOS_USUARIO").Select("ESCENARIO=" & escenario("ID")), _
                    dsEscenario.Tables("ESCENARIO_FILTRO_CONDICIONES_USUARIO").Select("ESCENARIO=" & escenario("ID")), _
                    dsEscenario.Tables("ESCENARIO_VISTAS_USUARIO").Select("ESCENARIO=" & escenario("ID")), _
                    dsEscenario.Tables("ESCENARIO_VISTA_CAMPOS_USUARIO").Select("ESCENARIO=" & escenario("ID")))
        End If

        Return iEscenario.EscenarioFiltros(CType(oInfoEscenarioNuevo(1), Integer))
    End Function
    Public Sub FSN_Delete_Escenario_Filtro_Usuario(ByVal IdEscenario As Integer, ByVal IdEscenarioFiltro As Integer)
        Authenticate()

        Dim idEscenarioPorDefecto As Integer = 0
        Dim dEscenarios As New Dictionary(Of String, Escenario)
        DBServer.FSN_Delete_Escenario_Filtro_Usuario(IdEscenario, IdEscenarioFiltro)
    End Sub
    Public Function FSN_Edit_Escenario_Filtro_Usuario(ByVal Usuario As String, ByVal Idioma As String, ByVal Filtro As EscenarioFiltro, ByVal TipoVisor As Integer) As EscenarioFiltro
        Authenticate()
        Dim dtEscenarioFiltroCampos As New DataTable
        With dtEscenarioFiltroCampos
            .Columns.Add("IDFILTRO", GetType(System.Int32))
            .Columns.Add("CAMPO", GetType(System.Int32))
            .Columns.Add("ESGENERAL", GetType(System.Boolean))
            .Columns.Add("VISIBLE", GetType(System.Boolean))
        End With
        Dim dtEscenarioFiltroCondiciones As New DataTable
        With dtEscenarioFiltroCondiciones
            .Columns.Add("IDFILTRO", GetType(System.Int32))
            .Columns.Add("ORDEN", GetType(System.Int32))
            .Columns.Add("CAMPO", GetType(System.Int32))
            .Columns.Add("ESGENERAL", GetType(System.Boolean))
            .Columns.Add("DENOMINACION", GetType(System.String))
            .Columns.Add("DENOMINACION_BD", GetType(System.String))
            .Columns.Add("OPERADOR", GetType(System.Int16))
            .Columns.Add("VALOR", GetType(System.String))
        End With

        Dim sValorCondicion As String
        Dim rEscenarioFiltroCampo As DataRow
        Dim rEscenarioFiltroCondicion As DataRow
        For Each campoFiltro As Escenario_Campo In Filtro.Filtro_Campos
            rEscenarioFiltroCampo = dtEscenarioFiltroCampos.NewRow
            rEscenarioFiltroCampo("CAMPO") = campoFiltro.Id
            rEscenarioFiltroCampo("ESGENERAL") = campoFiltro.EsCampoGeneral
            rEscenarioFiltroCampo("VISIBLE") = campoFiltro.Visible
            dtEscenarioFiltroCampos.Rows.Add(rEscenarioFiltroCampo)
        Next

        For Each condicionFiltro As EscenarioFiltroCondicion In Filtro.FormulaCondiciones
            rEscenarioFiltroCondicion = dtEscenarioFiltroCondiciones.NewRow
            rEscenarioFiltroCondicion("ORDEN") = condicionFiltro.Orden
            rEscenarioFiltroCondicion("CAMPO") = condicionFiltro.IdCampo
            rEscenarioFiltroCondicion("ESGENERAL") = condicionFiltro.EsCampoGeneral
            rEscenarioFiltroCondicion("DENOMINACION") = condicionFiltro.Denominacion
            rEscenarioFiltroCondicion("DENOMINACION_BD") = condicionFiltro.Denominacion_BD
            rEscenarioFiltroCondicion("OPERADOR") = condicionFiltro.Operador
            sValorCondicion = String.Empty
            For Each valor As String In condicionFiltro.Valores
                sValorCondicion &= IIf(sValorCondicion Is String.Empty, "", "#|#") & valor
            Next
            rEscenarioFiltroCondicion("VALOR") = sValorCondicion
            dtEscenarioFiltroCondiciones.Rows.Add(rEscenarioFiltroCondicion)
        Next

        Dim dsEscenario As Object = DBServer.FSN_Edit_Escenario_Filtro_Usuario(Usuario, Idioma, Filtro.Escenario, TipoVisor, _
                Filtro.Id, Filtro.Nombre, Filtro.Defecto, Filtro.FormulaAvanzada, dtEscenarioFiltroCampos, dtEscenarioFiltroCondiciones)

        Dim iEscenario As New Escenario
        'Creamos la estructura del escenario, con sus filtros vinculados y estos a sus vistas.
        If Not dsEscenario.Tables("ESCENARIOS_USUARIO").Rows.Count = 0 Then
            Dim escenario As DataRow = dsEscenario.Tables("ESCENARIOS_USUARIO").Rows(0)
            iEscenario = Create_Escenario(Idioma, escenario, _
                    dsEscenario.Tables("ESCENARIO_SOLICITUD_FORMULARIOS_USUARIO").Select("ESCENARIO=" & escenario("ID")), _
                    dsEscenario.Tables("ESCENARIO_FILTROS_USUARIO").Select("ESCENARIO=" & escenario("ID")), _
                    dsEscenario.Tables("ESCENARIO_FILTRO_CAMPOS_USUARIO").Select("ESCENARIO=" & escenario("ID")), _
                    dsEscenario.Tables("ESCENARIO_FILTRO_CONDICIONES_USUARIO").Select("ESCENARIO=" & escenario("ID")), _
                    dsEscenario.Tables("ESCENARIO_VISTAS_USUARIO").Select("ESCENARIO=" & escenario("ID")), _
                    dsEscenario.Tables("ESCENARIO_VISTA_CAMPOS_USUARIO").Select("ESCENARIO=" & escenario("ID")))
        End If

        Return iEscenario.EscenarioFiltros(Filtro.Id)
    End Function
    Public Function FSN_Insert_Escenario_Vista_Usuario(ByVal Usuario As String, ByVal Idioma As String, ByVal VistaNueva As EscenarioVista, ByVal TipoVisor As FSNLibrary.TipoVisor) As EscenarioVista
        Authenticate()
        Dim dtEscenarioVistaCampos As New DataTable
        dtEscenarioVistaCampos.Columns.Add("CAMPO", GetType(System.Int32))
        dtEscenarioVistaCampos.Columns.Add("ESGENERAL", GetType(System.Boolean))
        dtEscenarioVistaCampos.Columns.Add("ESDESGLOSE", GetType(System.Boolean))
        dtEscenarioVistaCampos.Columns.Add("NOMBREPERSONALIZADO", GetType(System.String))
        dtEscenarioVistaCampos.Columns.Add("ANCHO", GetType(System.Double))
        dtEscenarioVistaCampos.Columns.Add("POSICION", GetType(System.Int32))
        Dim rEscenarioVistaCampo As DataRow
        For Each campoVista As Escenario_Campo In VistaNueva.Campos_Vista
            rEscenarioVistaCampo = dtEscenarioVistaCampos.NewRow
            rEscenarioVistaCampo("CAMPO") = campoVista.Id
            rEscenarioVistaCampo("ESGENERAL") = campoVista.EsCampoGeneral
            rEscenarioVistaCampo("ESDESGLOSE") = campoVista.EsCampoDesglose
            If Not campoVista.NombrePersonalizado Is String.Empty Then rEscenarioVistaCampo("NOMBREPERSONALIZADO") = campoVista.NombrePersonalizado
            rEscenarioVistaCampo("ANCHO") = campoVista.AnchoVisualizacion
            rEscenarioVistaCampo("POSICION") = campoVista.OrdenVisualizacion
            dtEscenarioVistaCampos.Rows.Add(rEscenarioVistaCampo)
        Next

        Dim oInfoEscenarioNuevo As Object = DBServer.FSN_Insert_Escenario_Vista_Usuario(Usuario, Idioma, VistaNueva.Escenario,
                TipoVisor, VistaNueva.Nombre, VistaNueva.Defecto, VistaNueva.Posicion, dtEscenarioVistaCampos, VistaNueva.Orden)
        Dim dsEscenario As DataSet = oInfoEscenarioNuevo(0)
        Dim iEscenario As New Escenario
        'Creamos la estructura del escenario, con sus filtros vinculados y estos a sus vistas.
        If Not dsEscenario.Tables("ESCENARIOS_USUARIO").Rows.Count = 0 Then
            Dim escenario As DataRow = dsEscenario.Tables("ESCENARIOS_USUARIO").Rows(0)
            iEscenario = Create_Escenario(Idioma, escenario,
                    dsEscenario.Tables("ESCENARIO_SOLICITUD_FORMULARIOS_USUARIO").Select("ESCENARIO=" & escenario("ID")),
                    dsEscenario.Tables("ESCENARIO_FILTROS_USUARIO").Select("ESCENARIO=" & escenario("ID")),
                    dsEscenario.Tables("ESCENARIO_FILTRO_CAMPOS_USUARIO").Select("ESCENARIO=" & escenario("ID")),
                    dsEscenario.Tables("ESCENARIO_FILTRO_CONDICIONES_USUARIO").Select("ESCENARIO=" & escenario("ID")),
                    dsEscenario.Tables("ESCENARIO_VISTAS_USUARIO").Select("ESCENARIO=" & escenario("ID")),
                    dsEscenario.Tables("ESCENARIO_VISTA_CAMPOS_USUARIO").Select("ESCENARIO=" & escenario("ID")))
        End If

        Return iEscenario.EscenarioVistas(CType(oInfoEscenarioNuevo(1), Integer))
    End Function
    Public Sub FSN_Delete_Escenario_Vista_Usuario(ByVal IdEscenarioVista As Integer, ByVal IdEscenario As Integer)
        Authenticate()

        Dim idEscenarioPorDefecto As Integer = 0
        Dim dEscenarios As New Dictionary(Of String, Escenario)
        DBServer.FSN_Delete_Escenario_Vista_Usuario(IdEscenarioVista, IdEscenario)
    End Sub
    Public Function FSN_Edit_Escenario_Vista_Usuario(ByVal Usuario As String, ByVal Idioma As String, ByVal Vista As EscenarioVista, ByVal TipoVisor As Integer) As EscenarioVista
        Authenticate()
        Dim dtEscenarioVistaCampos As New DataTable
        dtEscenarioVistaCampos.Columns.Add("CAMPO", GetType(System.Int32))
        dtEscenarioVistaCampos.Columns.Add("ESGENERAL", GetType(System.Boolean))
        dtEscenarioVistaCampos.Columns.Add("ESDESGLOSE", GetType(System.Boolean))
        dtEscenarioVistaCampos.Columns.Add("NOMBREPERSONALIZADO", GetType(System.String))
        dtEscenarioVistaCampos.Columns.Add("ANCHO", GetType(System.Double))
        dtEscenarioVistaCampos.Columns.Add("POSICION", GetType(System.Int32))
        dtEscenarioVistaCampos.Columns.Add("ORDEN", GetType(System.String))
        Dim rEscenarioVistaCampo As DataRow
        For Each campoVista As Escenario_Campo In Vista.Campos_Vista
            rEscenarioVistaCampo = dtEscenarioVistaCampos.NewRow
            rEscenarioVistaCampo("CAMPO") = campoVista.Id
            rEscenarioVistaCampo("ESGENERAL") = campoVista.EsCampoGeneral
            rEscenarioVistaCampo("ESDESGLOSE") = campoVista.EsCampoDesglose
            rEscenarioVistaCampo("NOMBREPERSONALIZADO") = campoVista.NombrePersonalizado
            rEscenarioVistaCampo("ANCHO") = campoVista.AnchoVisualizacion
            rEscenarioVistaCampo("POSICION") = campoVista.OrdenVisualizacion
            dtEscenarioVistaCampos.Rows.Add(rEscenarioVistaCampo)
        Next
        Vista.Orden = String.Join(",", Split(Vista.Orden, ",").Where(Function(x) Vista.Campos_Vista.Any(Function(y) y.Denominacion_BD = Split(x, " ")(0))))
        Dim dsEscenario As Object = DBServer.FSN_Edit_Escenario_Vista_Usuario(Usuario, Idioma, Vista.Escenario, _
                TipoVisor, Vista.Id, Vista.Nombre, Vista.Defecto, Vista.Orden, dtEscenarioVistaCampos)

        Dim iEscenario As New Escenario
        'Creamos la estructura del escenario, con sus filtros vinculados y estos a sus vistas.
        If Not dsEscenario.Tables("ESCENARIOS_USUARIO").Rows.Count = 0 Then
            Dim escenario As DataRow = dsEscenario.Tables("ESCENARIOS_USUARIO").Rows(0)
            iEscenario = Create_Escenario(Idioma, escenario, _
                    dsEscenario.Tables("ESCENARIO_SOLICITUD_FORMULARIOS_USUARIO").Select("ESCENARIO=" & escenario("ID")), _
                    dsEscenario.Tables("ESCENARIO_FILTROS_USUARIO").Select("ESCENARIO=" & escenario("ID")), _
                    dsEscenario.Tables("ESCENARIO_FILTRO_CAMPOS_USUARIO").Select("ESCENARIO=" & escenario("ID")), _
                    dsEscenario.Tables("ESCENARIO_FILTRO_CONDICIONES_USUARIO").Select("ESCENARIO=" & escenario("ID")), _
                    dsEscenario.Tables("ESCENARIO_VISTAS_USUARIO").Select("ESCENARIO=" & escenario("ID")), _
                    dsEscenario.Tables("ESCENARIO_VISTA_CAMPOS_USUARIO").Select("ESCENARIO=" & escenario("ID")))
        End If

        Return iEscenario.EscenarioVistas(Vista.Id)
    End Function
    Public Function FSN_Get_EscenariosFavoritos_Usuario(ByVal UsuCod As String, ByVal Idioma As String, ByVal TipoVisor As Integer, ByVal OcultarEscenarioDefecto As Boolean) As List(Of cn_fsItem)
        Authenticate()
        Dim dsEscenarios As DataSet = DBServer.FSN_Get_EscenariosFavoritos_Usuario(UsuCod, TipoVisor, OcultarEscenarioDefecto)
        Dim lEscenariosFavoritos As New List(Of cn_fsItem)
        Dim iEscenarioFavorito As cn_fsItem
        'Creamos la estructura del escenario, con sus filtros vinculados y estos a sus vistas.
        For Each escenario As DataRow In dsEscenarios.Tables(0).Rows
            iEscenarioFavorito = New cn_fsItem
            With iEscenarioFavorito
                If DBNullToInteger(escenario("ID")) = 0 Then
                    Dim Textos As DataTable
                    Dim oDict As Dictionary = New Dictionary(DBServer, mIsAuthenticated)
                    oDict.LoadData(TiposDeDatos.ModulosIdiomas.Seguimiento, Idioma)
                    Textos = oDict.Data.Tables(0)
                    .value = "0#0"
                    .text = Textos(1)(1)
                Else
                    .value = DBNullToInteger(escenario("CARPETA")) & "#" & DBNullToInteger(escenario("ID"))
                    .text = escenario("NOMBRE").ToString
                End If
            End With
            lEscenariosFavoritos.Add(iEscenarioFavorito)
        Next

        Return lEscenariosFavoritos
    End Function
    Public Sub FSN_Establecer_Escenario_Favorito(ByVal UsuCod As String, ByVal TipoVisor As Integer, ByVal IdEscenario As Integer, ByVal Favorito As Boolean)
        Authenticate()
        DBServer.FSN_Establecer_Escenario_Favorito(UsuCod, TipoVisor, IdEscenario, Favorito)
    End Sub
    Public Sub FSN_Ordenar_Filtros(ByVal IdEscenario As Integer, ByVal IdFiltro As Integer, _
                                      ByVal PosicionAnterior As Integer, ByVal PosicionActual As Integer)
        Authenticate()
        DBServer.FSN_Ordenar_Filtros(IdEscenario, IdFiltro, PosicionAnterior, PosicionActual)
    End Sub
    Public Sub FSN_Ordenar_Vistas(ByVal IdEscenario As Integer, ByVal IdVista As Integer, _
                                      ByVal PosicionAnterior As Integer, ByVal PosicionActual As Integer)
        Authenticate()
        DBServer.FSN_Ordenar_Vistas(IdEscenario, IdVista, PosicionAnterior, PosicionActual)
    End Sub
    Public Sub FSN_Establecer_VistaDefecto_Filtro(ByVal IdFiltro As Integer, ByVal IdVista As Integer)
        Authenticate()
        DBServer.FSN_Establecer_VistaDefecto_Filtro(IdFiltro, IdVista)
    End Sub
    Public Function FSN_Obtener_Campos_Generales(ByVal Idioma As String) As Dictionary(Of String, Escenario_Campo)
        Dim lCamposGenerales As New Dictionary(Of String, Escenario_Campo)
        For Each campoGeneral As Integer In [Enum].GetValues(GetType(CamposGeneralesVisor))
            lCamposGenerales(campoGeneral) = Get_Campo_General(campoGeneral, Idioma)
        Next
        Return lCamposGenerales
    End Function
    ' Obtiene los usuarios con los que se comparte el escenario
    Public Function FSN_Get_TextoLargo_Instancia(ByVal Instancia As Integer, ByVal IdCampoOrigen As Integer, ByVal EsDesglose As Boolean) As String
        Authenticate()
        Return DBServer.FSN_Get_TextoLargo_Instancia(Instancia, IdCampoOrigen, EsDesglose)
    End Function
    ' Actualiza los usuarios con los que se comparte el escenario
    Public Sub FSN_Edit_Escenario_Compartir(ByVal IdEscenario As Integer, ByVal CompartirUON0 As Boolean, ByVal Usuarios As String, ByVal UONs As String, ByVal CompartirPortal As Boolean)
        Authenticate()

        DBServer.FSN_Edit_Escenario_Compartir(IdEscenario, CompartirUON0, Usuarios, UONs, CompartirPortal)
    End Sub
    'Obtener los usuarios con los que se comparte el usuario
    Public Function FSN_Get_Escenario_Compartir_Usu(ByVal IdEscenario As Integer) As DataTable
        Authenticate()

        Return DBServer.FSN_Get_Escenario_Compartir_Usu(IdEscenario)
    End Function
    'Obtener las UON con las que se comparte el escenario
    Public Function FSN_Get_Escenario_Compartir_UON(ByVal IdEscenario As Integer, ByVal Idioma As String) As DataTable
        Authenticate()

        Return DBServer.FSN_Get_Escenario_Compartir_UON(IdEscenario, Idioma)
    End Function
    'Establecer opciones de usuario de los escenarios (defecto, posicion, favorito)
    Public Sub FSN_Establecer_Escenario_Opciones_Usu(ByVal Usu As String, ByVal listaEscenarios As List(Of Escenario))
        Authenticate()
        Dim dtEscenarioOpciones As New DataTable
        Dim drEscenarioOpciones As DataRow
        With dtEscenarioOpciones
            .Columns.Add("ESCENARIO", GetType(System.Int32))
            .Columns.Add("USU", GetType(System.String))
            .Columns.Add("CARPETA", GetType(System.Int32))
            .Columns.Add("DEFECTO", GetType(System.Boolean))
            .Columns.Add("FAVORITO", GetType(System.Boolean))
            .Columns.Add("POSICION", GetType(System.Int32))
            .Columns.Add("TIPOVISOR", GetType(System.Int32))
        End With
        For Each escenario As Escenario In listaEscenarios
            drEscenarioOpciones = dtEscenarioOpciones.NewRow
            drEscenarioOpciones("ESCENARIO") = escenario.Id
            drEscenarioOpciones("USU") = Usu
            drEscenarioOpciones("CARPETA") = escenario.Carpeta
            drEscenarioOpciones("DEFECTO") = escenario.Defecto
            drEscenarioOpciones("FAVORITO") = escenario.Favorito
            drEscenarioOpciones("POSICION") = escenario.Posicion
            drEscenarioOpciones("TIPOVISOR") = escenario.TipoVisor
            dtEscenarioOpciones.Rows.Add(drEscenarioOpciones)
        Next

        DBServer.FSN_Establecer_Escenario_Opciones_Usu(dtEscenarioOpciones)
    End Sub
    'Establecer opciones de usuario de los filtros (defecto, posicion, idvistadefecto)
    Public Sub FSN_Establecer_Filtro_Opciones_Usu(ByVal Usu As String, ByVal listaFiltros As Dictionary(Of String, EscenarioFiltro))
        Authenticate()
        Dim dtFiltroOpciones As New DataTable
        Dim drFiltroOpciones As DataRow
        With dtFiltroOpciones
            .Columns.Add("FILTRO", GetType(System.Int32))
            .Columns.Add("DEFECTO", GetType(System.Boolean))
            .Columns.Add("POSICION", GetType(System.Int32))
            .Columns.Add("IDVISTADEFECTO", GetType(System.Int32))
        End With
        For Each filtro As KeyValuePair(Of String, EscenarioFiltro) In listaFiltros
            drFiltroOpciones = dtFiltroOpciones.NewRow
            With drFiltroOpciones
                .Item("FILTRO") = filtro.Value.Id
                .Item("DEFECTO") = filtro.Value.Defecto
                .Item("POSICION") = filtro.Value.Posicion
                .Item("IDVISTADEFECTO") = filtro.Value.IdVistaDefecto
            End With
            dtFiltroOpciones.Rows.Add(drFiltroOpciones)
        Next

        DBServer.FSN_Establecer_Filtro_Opciones_Usu(Usu, dtFiltroOpciones)
    End Sub
    'Establecer opciones de usuario de los filtros (defecto, posicion, idvistadefecto)
    Public Sub FSN_Establecer_Vista_Opciones_Usu(ByVal Usu As String, ByVal listaVistas As Dictionary(Of String, EscenarioVista))
        Authenticate()
        Dim dtVistaOpciones As New DataTable
        Dim drVistaOpciones As DataRow
        With dtVistaOpciones
            .Columns.Add("VISTA", GetType(System.Int32))
            .Columns.Add("DEFECTO", GetType(System.Boolean))
            .Columns.Add("ORDEN", GetType(System.String))
            .Columns.Add("POSICION", GetType(System.Int32))
        End With
        For Each vista As KeyValuePair(Of String, EscenarioVista) In listaVistas
            drVistaOpciones = dtVistaOpciones.NewRow
            With drVistaOpciones
                .Item("VISTA") = vista.Value.Id
                .Item("DEFECTO") = vista.Value.Defecto
                .Item("ORDEN") = vista.Value.Orden
                .Item("POSICION") = vista.Value.Posicion
            End With
            dtVistaOpciones.Rows.Add(drVistaOpciones)
        Next

        DBServer.FSN_Establecer_Vista_Opciones_Usu(Usu, dtVistaOpciones)
    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="UsuCod"></param>
    ''' <param name="Idioma"></param>
    ''' <param name="UON1"></param>
    ''' <param name="UON2"></param>
    ''' <param name="UON3"></param>
    ''' <param name="DEP"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function FSN_Obtener_UONsCompartir(ByVal UsuCod As String, ByVal Idioma As String, ByVal UON1 As String, _
                                     ByVal UON2 As String, ByVal UON3 As String, ByVal DEP As String, _
                                     ByVal Restr_UON As Boolean, ByVal Restr_Dep As Boolean, ByVal Restr_UON_Perf As Boolean, _
                                     Optional ByVal Busqueda As String = "", Optional ByVal Nivel As Integer = -1) As DataSet
        Authenticate()

        Dim dsDatos As New DataSet
        dsDatos = DBServer.FSN_Obtener_UONsCompartir(UsuCod, Idioma, UON1, UON2, UON3, DEP, Restr_UON, Restr_Dep, Restr_UON_Perf, busqueda, Nivel)
        Return dsDatos
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="UsuCod"></param>
    ''' <param name="Idioma"></param>
    ''' <param name="UON1"></param>
    ''' <param name="UON2"></param>
    ''' <param name="UON3"></param>
    ''' <param name="DEP"></param>
    ''' <param name="Restr_UON"></param>
    ''' <param name="Restr_Dep"></param>
    ''' <param name="Restr_UON_Perf"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function FSN_Obtener_USUCompartir(ByVal UsuCod As String, ByVal Idioma As String, _
                                    ByVal codigo As String, ByVal nombre As String, ByVal apellido As String, ByVal busqueda As String, _
                                    ByVal UON1 As String, ByVal UON2 As String, ByVal UON3 As String, ByVal DEP As String, _
                                    ByVal Restr_UON As Boolean, ByVal Restr_Dep As Boolean, ByVal Restr_UON_Perf As Boolean, _
                                    Optional ByVal UON1Buscado As String = "", Optional ByVal UON2Buscado As String = "", _
                                    Optional ByVal UON3Buscado As String = "", Optional ByVal DepBuscado As String = "") As DataSet
        Authenticate()

        Dim dsDatos As New DataSet
        dsDatos = DBServer.FSN_Obtener_USUCompartir(UsuCod, Idioma, codigo, nombre, apellido, busqueda, _
                                                    UON1, UON2, UON3, DEP, Restr_UON, Restr_Dep, Restr_UON_Perf, _
                                                    UON1Buscado, UON2Buscado, UON3Buscado, DepBuscado)
        Return dsDatos
    End Function
    Public Function FSN_Edit_Escenario_Filtro_Usuario_Condiciones(ByVal Usuario As String, ByVal Idioma As String, ByVal Filtro As EscenarioFiltro, ByVal TipoVisor As Integer) As EscenarioFiltro
        Authenticate()
        Dim dtEscenarioFiltroCondiciones As New DataTable
        With dtEscenarioFiltroCondiciones
            .Columns.Add("IDFILTRO", GetType(System.Int32))
            .Columns.Add("ORDEN", GetType(System.Int32))
            .Columns.Add("CAMPO", GetType(System.Int32))
            .Columns.Add("ESGENERAL", GetType(System.Boolean))
            .Columns.Add("DENOMINACION", GetType(System.String))
            .Columns.Add("DENOMINACION_BD", GetType(System.String))
            .Columns.Add("OPERADOR", GetType(System.Int16))
            .Columns.Add("VALOR", GetType(System.String))
        End With

        Dim sValorCondicion As String
        Dim rEscenarioFiltroCondicion As DataRow
        For Each condicionFiltro As EscenarioFiltroCondicion In Filtro.FormulaCondiciones
            rEscenarioFiltroCondicion = dtEscenarioFiltroCondiciones.NewRow
            rEscenarioFiltroCondicion("ORDEN") = condicionFiltro.Orden
            rEscenarioFiltroCondicion("CAMPO") = condicionFiltro.IdCampo
            rEscenarioFiltroCondicion("ESGENERAL") = condicionFiltro.EsCampoGeneral
            rEscenarioFiltroCondicion("DENOMINACION") = condicionFiltro.Denominacion
            rEscenarioFiltroCondicion("DENOMINACION_BD") = condicionFiltro.Denominacion_BD
            rEscenarioFiltroCondicion("OPERADOR") = condicionFiltro.Operador
            sValorCondicion = String.Empty
            For Each valor As String In condicionFiltro.Valores
                sValorCondicion &= IIf(sValorCondicion Is String.Empty, "", "#|#") & valor
            Next
            rEscenarioFiltroCondicion("VALOR") = sValorCondicion
            dtEscenarioFiltroCondiciones.Rows.Add(rEscenarioFiltroCondicion)
        Next

        Dim dsEscenario As Object = DBServer.FSN_Edit_Escenario_Filtro_Usuario_Condiciones(Usuario, Idioma, Filtro.Escenario, TipoVisor, Filtro.Id, dtEscenarioFiltroCondiciones)
        Dim iEscenario As New Escenario
        'Creamos la estructura del escenario, con sus filtros vinculados y estos a sus vistas.
        If Not dsEscenario.Tables("ESCENARIOS_USUARIO").Rows.Count = 0 Then
            Dim escenario As DataRow = dsEscenario.Tables("ESCENARIOS_USUARIO").Rows(0)
            iEscenario = Create_Escenario(Idioma, escenario, _
                    dsEscenario.Tables("ESCENARIO_SOLICITUD_FORMULARIOS_USUARIO").Select("ESCENARIO=" & escenario("ID")), _
                    dsEscenario.Tables("ESCENARIO_FILTROS_USUARIO").Select("ESCENARIO=" & escenario("ID")), _
                    dsEscenario.Tables("ESCENARIO_FILTRO_CAMPOS_USUARIO").Select("ESCENARIO=" & escenario("ID")), _
                    dsEscenario.Tables("ESCENARIO_FILTRO_CONDICIONES_USUARIO").Select("ESCENARIO=" & escenario("ID")), _
                    dsEscenario.Tables("ESCENARIO_VISTAS_USUARIO").Select("ESCENARIO=" & escenario("ID")), _
                    dsEscenario.Tables("ESCENARIO_VISTA_CAMPOS_USUARIO").Select("ESCENARIO=" & escenario("ID")))
        End If

        Return iEscenario.EscenarioFiltros(Filtro.Id)
    End Function
    Public Function FSN_Edit_Escenario_Vista_Usuario_Configuracion(ByVal Usuario As String, ByVal Idioma As String, ByVal Vista As EscenarioVista, ByVal TipoVisor As Integer) As EscenarioVista
        Authenticate()
        Dim dtEscenarioVistaCampos As New DataTable
        dtEscenarioVistaCampos.Columns.Add("CAMPO", GetType(System.Int32))
        dtEscenarioVistaCampos.Columns.Add("ESGENERAL", GetType(System.Boolean))
        dtEscenarioVistaCampos.Columns.Add("ESDESGLOSE", GetType(System.Boolean))
        dtEscenarioVistaCampos.Columns.Add("NOMBREPERSONALIZADO", GetType(System.String))
        dtEscenarioVistaCampos.Columns.Add("ANCHO", GetType(System.Double))
        dtEscenarioVistaCampos.Columns.Add("POSICION", GetType(System.Int32))
        dtEscenarioVistaCampos.Columns.Add("ORDEN", GetType(System.String))
        Dim rEscenarioVistaCampo As DataRow
        For Each campoVista As Escenario_Campo In Vista.Campos_Vista
            rEscenarioVistaCampo = dtEscenarioVistaCampos.NewRow
            rEscenarioVistaCampo("CAMPO") = campoVista.Id
            rEscenarioVistaCampo("ESGENERAL") = campoVista.EsCampoGeneral
            rEscenarioVistaCampo("ESDESGLOSE") = campoVista.EsCampoDesglose
            rEscenarioVistaCampo("NOMBREPERSONALIZADO") = campoVista.NombrePersonalizado
            rEscenarioVistaCampo("ANCHO") = campoVista.AnchoVisualizacion
            rEscenarioVistaCampo("POSICION") = campoVista.OrdenVisualizacion
            dtEscenarioVistaCampos.Rows.Add(rEscenarioVistaCampo)
        Next
        Vista.Orden = String.Join(",", Split(Vista.Orden, ",").Where(Function(x) Vista.Campos_Vista.Any(Function(y) y.Denominacion_BD = Split(x, " ")(0))))
        Dim dsEscenario As Object = DBServer.FSN_Edit_Escenario_Vista_Usuario_Configuracion(Usuario, Idioma, Vista.Escenario, TipoVisor, Vista.Id, dtEscenarioVistaCampos)
        Dim iEscenario As New Escenario
        'Creamos la estructura del escenario, con sus filtros vinculados y estos a sus vistas.
        If Not dsEscenario.Tables("ESCENARIOS_USUARIO").Rows.Count = 0 Then
            Dim escenario As DataRow = dsEscenario.Tables("ESCENARIOS_USUARIO").Rows(0)
            iEscenario = Create_Escenario(Idioma, escenario, _
                    dsEscenario.Tables("ESCENARIO_SOLICITUD_FORMULARIOS_USUARIO").Select("ESCENARIO=" & escenario("ID")), _
                    dsEscenario.Tables("ESCENARIO_FILTROS_USUARIO").Select("ESCENARIO=" & escenario("ID")), _
                    dsEscenario.Tables("ESCENARIO_FILTRO_CAMPOS_USUARIO").Select("ESCENARIO=" & escenario("ID")), _
                    dsEscenario.Tables("ESCENARIO_FILTRO_CONDICIONES_USUARIO").Select("ESCENARIO=" & escenario("ID")), _
                    dsEscenario.Tables("ESCENARIO_VISTAS_USUARIO").Select("ESCENARIO=" & escenario("ID")), _
                    dsEscenario.Tables("ESCENARIO_VISTA_CAMPOS_USUARIO").Select("ESCENARIO=" & escenario("ID")))
        End If

        Return iEscenario.EscenarioVistas(Vista.Id)
    End Function
    Public Sub FSN_Establecer_Filtro_Defecto(ByVal UsuCod As String, ByVal TipoVisor As Integer, ByVal IdFiltro As Integer, ByVal Defecto As Boolean)
        Authenticate()
        DBServer.FSN_Establecer_Filtro_Defecto(UsuCod, TipoVisor, IdFiltro, Defecto)
    End Sub
#Region "Opciones tipo lista"
    Public Function Get_Opciones_Lista_Boolean(ByVal Textos As DataTable)
        Dim lOpcionesLista As New List(Of cn_fsItem)
        Dim iOpcionLista As cn_fsItem
        iOpcionLista = New cn_fsItem
        iOpcionLista.value = ""
        iOpcionLista.text = ""
        lOpcionesLista.Add(iOpcionLista)
        iOpcionLista = New cn_fsItem
        iOpcionLista.value = 1
        iOpcionLista.text = Textos(110)(1)
        lOpcionesLista.Add(iOpcionLista)
        iOpcionLista = New cn_fsItem
        iOpcionLista.value = 0
        iOpcionLista.text = Textos(111)(1)
        lOpcionesLista.Add(iOpcionLista)
        Return lOpcionesLista
    End Function
    Private Function Get_Opciones_Lista_Estado(ByVal Textos As DataTable) As List(Of cn_fsItem)
        Dim lOpcionesLista As New List(Of cn_fsItem)
        Dim iOpcionLista As cn_fsItem
        For estado As Integer = 1 To 8
            iOpcionLista = New cn_fsItem
            iOpcionLista.text = Textos(1 + estado)(1)
            Select Case estado
                Case 1 'Pendientes
                    iOpcionLista.value = "1000"
                Case 2 'Guardadas
                    iOpcionLista.value = "0"
                Case 3 'En curso
                    iOpcionLista.value = "2"
                Case 4 'Rechazadas
                    iOpcionLista.value = "6"
                Case 5 'Anuladas
                    iOpcionLista.value = "8"
                Case 6 'Finalizadas
                    iOpcionLista.value = "100#101#102#103"
                Case 7 'Cerradas
                    iOpcionLista.value = "104"
                Case Else
                    iOpcionLista.text = Textos(125)(1)
                    iOpcionLista.value = "1000#0#2#6#8#100#101#102#103#104"
            End Select

            lOpcionesLista.Add(iOpcionLista)
        Next
        Return lOpcionesLista
    End Function
    Private Function Get_Opciones_Lista_MotivoVisibilidad(ByVal Textos As DataTable) As List(Of cn_fsItem)
        Dim lOpcionesLista As New List(Of cn_fsItem)
        Dim iOpcionLista As cn_fsItem
        For Each motivoVisibilidad As Integer In [Enum].GetValues(GetType(MotivoVisibilidadSolicitud))
            iOpcionLista = New cn_fsItem
            iOpcionLista.value = motivoVisibilidad
            Select Case motivoVisibilidad
                Case MotivoVisibilidadSolicitud.SolicitudAbiertaUsted  'Solicitudes abiertas por usted
                    iOpcionLista.text = Textos(128)(1)
                Case MotivoVisibilidadSolicitud.SolicitudPendiente  'Solicitudes pendientes 
                    iOpcionLista.text = Textos(129)(1)
                Case MotivoVisibilidadSolicitud.SolicitudPenditenteDevolucion  'Solicitudes pendientes de devolucion
                    iOpcionLista.text = Textos(130)(1)
                Case MotivoVisibilidadSolicitud.SolicitudTrasladadaEsperaDevolucion 'Solicitudes trasladadas en espera de devolución
                    iOpcionLista.text = Textos(131)(1)
                Case MotivoVisibilidadSolicitud.SolicitudParticipo 'Procesos en los que ha participado
                    iOpcionLista.text = Textos(132)(1)
                Case MotivoVisibilidadSolicitud.SolicitudObservador   'es observador
                    iOpcionLista.text = Textos(133)(1)
                Case MotivoVisibilidadSolicitud.SolicitudObservadorSustitucion
                    iOpcionLista.text = Textos(213)(1)
                Case Else '
                    iOpcionLista.text = ""
            End Select
            lOpcionesLista.Add(iOpcionLista)
        Next
        Return lOpcionesLista
    End Function
    Public Function FSN_Get_Opciones_Lista_TipoSolicitud(ByVal Usuario As String, ByVal Idioma As String, ByVal IdsSolicitud As String, ByVal TipoVisor As TipoVisor)
        Authenticate()
        Dim TipoSolicitud As Integer?
        Select Case TipoVisor
            Case TipoVisor.NoConformidades
                TipoSolicitud = TiposDeDatos.TipoDeSolicitud.NoConformidad
            Case TipoVisor.Certificados
                TipoSolicitud = TiposDeDatos.TipoDeSolicitud.Certificado
            Case TipoVisor.Contratos
                TipoSolicitud = TiposDeDatos.TipoDeSolicitud.Contrato
            Case TipoVisor.Facturas
                TipoSolicitud = TiposDeDatos.TipoDeSolicitud.Factura
            Case TipoVisor.SolicitudesQA
                TipoSolicitud = TiposDeDatos.TipoDeSolicitud.SolicitudQA
            Case TipoVisor.Encuestas
                TipoSolicitud = TiposDeDatos.TipoDeSolicitud.Encuesta
            Case Else
                TipoSolicitud = Nothing
        End Select
        Dim dtEscenarios As DataTable = DBServer.FSN_Get_Escenarios_TiposSolicitud_Formulario(Usuario, Idioma, IdsSolicitud, TipoSolicitud)
        Dim lTiposSolicitud As New List(Of cn_fsItem)
        Dim iTipoSolicitud As cn_fsItem
        For Each rTipoSolicitud As DataRow In dtEscenarios.Rows
            iTipoSolicitud = New cn_fsItem
            With iTipoSolicitud
                .value = rTipoSolicitud("ID")
                .text = rTipoSolicitud("DEN")
            End With
            lTiposSolicitud.Add(iTipoSolicitud)
        Next
        Return lTiposSolicitud
    End Function
    Public Function FSN_Get_Opciones_Lista(ByVal Idioma As String, ByVal IdCampo As Integer, ByVal TipoCampo As TiposDeDatos.TipoGeneral)
        Authenticate()
        Dim dtEscenarios As DataTable = DBServer.FSN_Get_Opciones_Lista_Campo(IdCampo)
        Dim lTiposSolicitud As New List(Of cn_fsItem)
        Dim iTipoSolicitud As cn_fsItem
        For Each tipoSolicitud As DataRow In dtEscenarios.Rows
            iTipoSolicitud = New cn_fsItem
            With iTipoSolicitud
                .value = tipoSolicitud("ORDEN")
                Select Case TipoCampo
                    Case TiposDeDatos.TipoGeneral.TipoNumerico
                        .text = tipoSolicitud("VALOR_NUM")
                    Case TiposDeDatos.TipoGeneral.TipoFecha
                        .text = tipoSolicitud("VALOR_FEC")
                    Case Else
                        .text = tipoSolicitud("VALOR_TEXT_" & Idioma)
                End Select
            End With
            lTiposSolicitud.Add(iTipoSolicitud)
        Next
        Return lTiposSolicitud
    End Function
    Public Function FSN_Get_Opciones_Lista_EstadoHomologacion(ByVal Idioma As String, SolicitudFormularioVinculados As Dictionary(Of String, SerializableKeyValuePair(Of Integer, String)))
        Authenticate()
        Dim dtEstados As DataTable = DBServer.FSN_Get_Opciones_Lista_EstadoHomologacion(SolicitudFormularioVinculados)
        Dim lEstados As New List(Of cn_fsItem)
        Dim oEstado As cn_fsItem
        For Each drEstado As DataRow In dtEstados.Rows
            oEstado = New cn_fsItem
            With oEstado
                .value = drEstado("ORDEN")
                .text = drEstado("VALOR_TEXT_" & Idioma)
            End With
            lEstados.Add(oEstado)
        Next
        Return lEstados
    End Function

#End Region
#Region "Funciones Utiles"
    Private Function Carpetas_Usuario(ByVal dtCarpetas As DataTable) As List(Of cn_fsTreeViewItem)
        Dim lCarpetas As New List(Of cn_fsTreeViewItem)
        Dim oCarpeta As cn_fsTreeViewItem
        oCarpeta = New cn_fsTreeViewItem
        With oCarpeta
            .value = 0
            .text = "."
            .selectable = True
            .expanded = True
            .type = 10
            .nivel = 0
            .children = New List(Of cn_fsTreeViewItem)
            .children = SubCarpetas_Usuario(dtCarpetas, .value, 0)
        End With
        lCarpetas.Add(oCarpeta)
        Return lCarpetas
    End Function
    Private Function SubCarpetas_Usuario(ByVal dtCarpetas As DataTable, ByVal Carpeta As Integer, ByVal nivel As Integer) As List(Of cn_fsTreeViewItem)
        Dim lSubCarpetas As New List(Of cn_fsTreeViewItem)
        Dim oSubCarpeta As cn_fsTreeViewItem
        For Each subcarpeta As DataRow In dtCarpetas.Select(IIf(Carpeta = 0, "CARPETA IS NULL", "CARPETA=" & Carpeta))
            oSubCarpeta = New cn_fsTreeViewItem
            With oSubCarpeta
                .value = CType(subcarpeta("ID"), Integer)
                .text = subcarpeta("NOMBRE").ToString
                .selectable = True
                .type = 10
                .nivel = nivel + 1
                .expanded = False
                .children = New List(Of cn_fsTreeViewItem)
                .children = SubCarpetas_Usuario(dtCarpetas, .value, .nivel)
            End With
            lSubCarpetas.Add(oSubCarpeta)
        Next
        Return lSubCarpetas
    End Function
    Private Function Create_Escenario(ByVal Idioma As String, ByVal Escenario As DataRow, ByVal EscenarioSolicitudFormulario As DataRow(), ByVal EscenarioFiltros As DataRow(), _
                ByVal EscenarioFiltroCampos As DataRow(), ByVal EscenarioFiltroCondiciones As DataRow(), _
                ByVal EscenarioVistas As DataRow(), ByVal EscenarioVistaCampos As DataRow(), Optional ByVal PermisoEditarEscenariosOtrosUsu As Boolean = False) As Escenario
        Dim Textos As DataTable
        Dim oDict As Dictionary = New Dictionary(DBServer, mIsAuthenticated)
        oDict.LoadData(TiposDeDatos.ModulosIdiomas.Seguimiento, Idioma)
        Textos = oDict.Data.Tables(0)
        Dim ordenFiltro, ordenVista As Integer
        Dim iEscenario As New Escenario
        Dim iEscenarioFiltro As EscenarioFiltro
        Dim iEscenarioFiltroCampo As Escenario_Campo
        Dim iEscenarioFiltroCondicion As EscenarioFiltroCondicion
        Dim iEscenarioVista As EscenarioVista
        Dim iEscenarioVistaCampo As Escenario_Campo
        Dim dtEscenarioFiltroCampos As New DataTable
        Dim campoPadre As Integer
        With iEscenario
            .Cargado = (EscenarioFiltros.Length > 0 AndAlso EscenarioVistas.Length > 0)
            .Id = DBNullToInteger(Escenario("ID"))
            .Propietario = Escenario("USUARIOCREADOR").ToString()
            .Carpeta = DBNullToInteger(Escenario("CARPETA"))
            .Nombre = Escenario("NOMBRE").ToString
            .Defecto = DBNullToBoolean(Escenario("DEFECTO"))
            .Favorito = DBNullToBoolean(Escenario("FAVORITO"))
            .AplicaTodas = DBNullToBoolean(Escenario("APLICATODAS"))
            .TipoVisor = DBNullToInteger(Escenario("TIPOVISOR"))
            .Posicion = DBNullToInteger(Escenario("POSICION"))
            .Modo = DBNullToInteger(Escenario("MODO"))
            .TieneOpcionesUsu = Not (.Posicion = -1)
            .Editable = (PermisoEditarEscenariosOtrosUsu AndAlso DBNullToBoolean(Escenario("EDITABLE")))
            .Portal = DBNullToBoolean(Escenario("PORTAL"))
            'Anotamos las solicitudes a las que se aplica el escenario
            For Each formularioVinculado As DataRow In EscenarioSolicitudFormulario
                .SolicitudFormularioVinculados(formularioVinculado("SOLICITUD")) = New SerializableKeyValuePair(Of Integer, String)(CType(formularioVinculado("FORMULARIO"), Integer), formularioVinculado("DEN"))
            Next
            ordenFiltro = 0
            'Añadimos los filtros del escenario
            For Each escenarioFiltro As DataRow In EscenarioFiltros
                iEscenarioFiltro = New EscenarioFiltro
                With iEscenarioFiltro
                    .Id = DBNullToInteger(escenarioFiltro("ID"))
                    .Escenario = DBNullToInteger(escenarioFiltro("ESCENARIO"))
                    .Nombre = escenarioFiltro("NOMBRE").ToString
                    .Defecto = DBNullToBoolean(escenarioFiltro("DEFECTO"))
                    .FormulaAvanzada = DBNullToBoolean(escenarioFiltro("FORMULAAVANZADA"))
                    .ErrorFormula = DBNullToBoolean(escenarioFiltro("ERROR_FORMULA"))
                    .Posicion = ordenFiltro
                    ordenFiltro += 1
                    .IdVistaDefecto = DBNullToInteger(escenarioFiltro("IDVISTADEFECTO"))
                    If Not EscenarioFiltroCampos.Length = 0 Then dtEscenarioFiltroCampos = EscenarioFiltroCampos.CopyToDataTable

                    For Each escenarioFiltroCampo As DataRow In (From campo In EscenarioFiltroCampos Where campo.Item("FILTRO") = iEscenarioFiltro.Id)
                        iEscenarioFiltroCampo = New Escenario_Campo
                        With iEscenarioFiltroCampo
                            If escenarioFiltroCampo("ESGENERAL") Then
                                iEscenarioFiltroCampo = Get_Campo_General(DBNullToInteger(escenarioFiltroCampo("CAMPO")), Idioma)
                            Else
                                .Id = DBNullToInteger(escenarioFiltroCampo("CAMPO"))
                                .EsCampoGeneral = DBNullToBoolean(escenarioFiltroCampo("ESGENERAL"))
                                .Denominacion = escenarioFiltroCampo("DEN_" & Idioma).ToString
                                .TipoCampo = DBNullToInteger(escenarioFiltroCampo("SUBTIPO"))
                                .EsLista = (DBNullToBoolean(escenarioFiltroCampo("INTRO")) OrElse .TipoCampo = TiposDeDatos.TipoGeneral.TipoBoolean)
                                .TipoCampoGS = DBNullToInteger(escenarioFiltroCampo("TIPO_CAMPO_GS"))
                                Select Case .TipoCampoGS
                                    Case TiposDeDatos.TipoCampoGS.FormaPago, TiposDeDatos.TipoCampoGS.Moneda, TiposDeDatos.TipoCampoGS.Unidad, _
                                            TiposDeDatos.TipoCampoGS.UnidadPedido, TiposDeDatos.TipoCampoGS.Dest, TiposDeDatos.TipoCampoGS.Departamento, _
                                            TiposDeDatos.TipoCampoGS.OrganizacionCompras, TiposDeDatos.TipoCampoGS.Centro, TiposDeDatos.TipoCampoGS.Almacen, _
                                            TiposDeDatos.TipoCampoGS.TipoPedido
                                        .EsLista = True
                                    Case TiposDeDatos.TipoCampoGS.Pais
                                        .EsLista = True
                                        campoPadre = .Id
                                    Case TiposDeDatos.TipoCampoGS.Provincia
                                        .EsLista = True
                                        .CampoPadre = campoPadre
                                        iEscenarioFiltro.Filtro_Campos(iEscenarioFiltro.Filtro_Campos.Count - 1).CampoHijo = .Id
                                End Select
                                .CampoDesglose = DBNullToInteger(escenarioFiltroCampo("CAMPO_PADRE"))
                                .Denominacion_BD = "C_" & IIf(IsDBNull(escenarioFiltroCampo("CAMPO_PADRE")), "", escenarioFiltroCampo("CAMPO_PADRE") & "_") & _
                                    DBNullToInteger(escenarioFiltroCampo("TIPO_CAMPO_GS")) & "_" & DBNullToInteger(escenarioFiltroCampo("CAMPO"))
                                If .EsCampoGeneral AndAlso .Id = 14 Then .OpcionesLista = Get_Opciones_Lista_Estado(Textos)
                            End If
                            iEscenarioFiltroCampo.Visible = DBNullToBoolean(escenarioFiltroCampo("VISIBLE"))
                        End With
                        .Filtro_Campos.Add(iEscenarioFiltroCampo)
                    Next
                    For Each escenarioFiltroCondicion As DataRow In (From campo In EscenarioFiltroCondiciones Where campo.Item("FILTRO") = iEscenarioFiltro.Id)
                        iEscenarioFiltroCondicion = New EscenarioFiltroCondicion
                        With iEscenarioFiltroCondicion
                            If DBNullToBoolean(escenarioFiltroCondicion("ESGENERAL")) Then
                                Dim oCampo As Escenario_Campo = Get_Campo_General(DBNullToInteger(escenarioFiltroCondicion("CAMPO")), Idioma)
                                .EsCampoGeneral = True
                                .IdCampo = oCampo.Id
                                .EsLista = oCampo.EsLista
                                .TipoCampo = oCampo.TipoCampo
                                .TipoCampoGS = oCampo.TipoCampoGS
                                .Denominacion = oCampo.Denominacion
                                .Denominacion_BD = oCampo.Denominacion_BD
                            Else
                                .EsCampoGeneral = False
                                If IsDBNull(escenarioFiltroCondicion("CAMPO")) Then
                                    .IdCampo = -1
                                Else
                                    .IdCampo = DBNullToInteger(escenarioFiltroCondicion("CAMPO"))
                                End If
                                .TipoCampo = DBNullToInteger(escenarioFiltroCondicion("SUBTIPO"))
                                .TipoCampoGS = DBNullToInteger(escenarioFiltroCondicion("TIPO_CAMPO_GS"))
                                If {TiposDeDatos.TipoCampoGS.FormaPago, TiposDeDatos.TipoCampoGS.Moneda, TiposDeDatos.TipoCampoGS.Unidad, _
                                    TiposDeDatos.TipoCampoGS.UnidadPedido, TiposDeDatos.TipoCampoGS.Dest, TiposDeDatos.TipoCampoGS.Departamento, _
                                    TiposDeDatos.TipoCampoGS.OrganizacionCompras, TiposDeDatos.TipoCampoGS.Centro, TiposDeDatos.TipoCampoGS.Almacen, _
                                    TiposDeDatos.TipoCampoGS.TipoPedido, TiposDeDatos.TipoCampoGS.Pais, TiposDeDatos.TipoCampoGS.Provincia}.Contains(.TipoCampoGS) Then
                                    .EsLista = True
                                Else
                                    .EsLista = (DBNullToBoolean(escenarioFiltroCondicion("INTRO")) OrElse .TipoCampo = TiposDeDatos.TipoGeneral.TipoBoolean)
                                End If
                                .Denominacion = escenarioFiltroCondicion("DENOMINACION").ToString
                                .Denominacion_BD = escenarioFiltroCondicion("DENOMINACION_BD").ToString
                                .TieneError = IsDBNull(escenarioFiltroCondicion("CAMPO"))
                            End If
                            .IdFiltro = iEscenarioFiltro.Id
                            .Orden = DBNullToInteger(escenarioFiltroCondicion("ORDEN"))
                            .Operador = DBNullToInteger(escenarioFiltroCondicion("OPERADOR"))
                            'For Each valor As String In Split(escenarioFiltroCondicion("VALOR"), "#|#")
                            For Each valor As String In Split(DBNullToStr(escenarioFiltroCondicion("VALOR")), "#|#")
                                .Valores.Add(valor)
                            Next
                        End With
                        .FormulaCondiciones.Add(iEscenarioFiltroCondicion)
                    Next
                End With
                .EscenarioFiltros(escenarioFiltro("ID")) = iEscenarioFiltro
            Next
            ordenVista = 0
            'Añadimos las vistas del escenario
            For Each escenarioVista As DataRow In EscenarioVistas
                iEscenarioVista = New EscenarioVista
                With iEscenarioVista
                    .Id = DBNullToInteger(escenarioVista("ID"))
                    .Escenario = DBNullToInteger(escenarioVista("ESCENARIO"))
                    .Nombre = escenarioVista("NOMBRE").ToString
                    .Defecto = DBNullToBoolean(escenarioVista("DEFECTO"))
                    .Orden = escenarioVista("ORDEN").ToString
                    .Posicion = ordenVista
                    ordenVista += 1
                    For Each EscenarioVistaCampo As DataRow In (From campo In EscenarioVistaCampos Where campo.Item("VISTA") = iEscenarioVista.Id)
                        iEscenarioVistaCampo = New Escenario_Campo
                        If DBNullToBoolean(EscenarioVistaCampo("ESGENERAL")) Then
                            iEscenarioVistaCampo = Get_Campo_General(EscenarioVistaCampo("CAMPO"), Idioma)
                        Else
                            With iEscenarioVistaCampo
                                .Id = DBNullToInteger(EscenarioVistaCampo("CAMPO"))
                                .EsCampoGeneral = False
                                .EsCampoDesglose = DBNullToBoolean(EscenarioVistaCampo("ESDESGLOSE"))
                                .TipoCampo = DBNullToInteger(EscenarioVistaCampo("SUBTIPO"))
                                .TipoCampoGS = DBNullToInteger(EscenarioVistaCampo("TIPO_CAMPO_GS"))
                                .Denominacion = EscenarioVistaCampo("DEN_" & Idioma).ToString
                                If .EsCampoDesglose Then
                                    .Denominacion_BD = "C_" & DBNullToInteger(EscenarioVistaCampo("CAMPO_PADRE")) & "_" & DBNullToInteger(EscenarioVistaCampo("TIPO_CAMPO_GS")) & "_" & DBNullToInteger(EscenarioVistaCampo("CAMPO"))
                                Else
                                    .Denominacion_BD = "C_" & DBNullToInteger(EscenarioVistaCampo("TIPO_CAMPO_GS")) & "_" & DBNullToInteger(EscenarioVistaCampo("CAMPO"))
                                End If
                            End With
                        End If
                        iEscenarioVistaCampo.NombrePersonalizado = EscenarioVistaCampo("NOMBREPERSONALIZADO").ToString
                        iEscenarioVistaCampo.AnchoVisualizacion = DBNullToDbl(EscenarioVistaCampo("ANCHO"))
                        iEscenarioVistaCampo.OrdenVisualizacion = DBNullToInteger(EscenarioVistaCampo("POSICION"))
                        .Campos_Vista.Add(iEscenarioVistaCampo)
                    Next
                End With
                .EscenarioVistas(escenarioVista("ID")) = iEscenarioVista
            Next
        End With
        Return iEscenario
    End Function
    Public Function Get_Campo_General(ByVal Id As Integer, ByVal Idioma As String) As Escenario_Campo
        Dim Textos As DataTable
        Dim oDict As Dictionary = New Dictionary(DBServer, mIsAuthenticated)
        oDict.LoadData(TiposDeDatos.ModulosIdiomas.Seguimiento, Idioma)
        Textos = oDict.Data.Tables(0)

        Dim oCampo_General As Escenario_Campo
        oCampo_General = New Escenario_Campo
        With oCampo_General
            .EsCampoGeneral = True
            .Visible = True
            .EsCampoDesglose = False
            .Id = Id
            Select Case Id
                Case CamposGeneralesVisor.DENOMINACION  'denominación
                    .Denominacion = Textos(41)(1)
                    .TipoCampo = TiposDeDatos.TipoGeneral.TipoString
                    .Denominacion_BD = "DEN"
                    .AnchoVisualizacion = 200
                Case CamposGeneralesVisor.ORIGEN   'departamento peticionario
                    .Denominacion = Textos(42)(1)
                    .EsLista = True
                    .TipoCampo = TiposDeDatos.TipoGeneral.TipoString
                    .Denominacion_BD = "ORIGEN"
                Case CamposGeneralesVisor.FECHAALTA  'fecha de alta
                    .Denominacion = Textos(43)(1)
                    .TipoCampo = TiposDeDatos.TipoGeneral.TipoFecha
                    .Denominacion_BD = "FECHA"
                Case CamposGeneralesVisor.IDENTIFICADOR  'identificador
                    .Denominacion = Textos(44)(1)
                    .TipoCampo = TiposDeDatos.TipoGeneral.TipoNumerico
                    .Denominacion_BD = "ID"
                Case CamposGeneralesVisor.IMPORTE  'importe
                    .Denominacion = Textos(45)(1)
                    .TipoCampo = TiposDeDatos.TipoGeneral.TipoNumerico
                    .Denominacion_BD = "IMPORTE"
                Case CamposGeneralesVisor.INFOPEDIDOSASOCIADOS  'Información de pedidos asociados a la solicitud
                    .Denominacion = Textos(46)(1)
                    .EsLista = True
                    .TipoCampo = TiposDeDatos.TipoGeneral.TipoString
                    .Denominacion_BD = "PEDIDOS"
                Case CamposGeneralesVisor.PETICIONARIO 'peticionario
                    .Denominacion = Textos(47)(1)
                    .TipoCampo = TiposDeDatos.TipoGeneral.TipoString
                    .TipoCampoGS = -1
                    .Denominacion_BD = "PETICIONARIO"
                Case CamposGeneralesVisor.INFOPROCESOSASOCIADOS  'Información de procesos asociados a la solicitud
                    .Denominacion = Textos(48)(1)
                    .EsLista = True
                    .TipoCampo = TiposDeDatos.TipoGeneral.TipoString
                    .Denominacion_BD = "PROCESOS"
                Case CamposGeneralesVisor.SITUACIONACTUAL  'situación actual
                    .Denominacion = Textos(49)(1)
                    .EsLista = True
                    .TipoCampo = TiposDeDatos.TipoGeneral.TipoString
                    .Denominacion_BD = "ETAPA"
                Case CamposGeneralesVisor.TIPOSOLICITUD  'tipo solicitud
                    .Denominacion = Textos(29)(1)
                    .EsLista = True
                    .TipoCampo = TiposDeDatos.TipoGeneral.TipoString
                    .Denominacion_BD = "ID_SOLIC"
                    .AnchoVisualizacion = 100
                Case CamposGeneralesVisor.USUARIO  'usuario
                    .Denominacion = Textos(50)(1)
                    .TipoCampo = TiposDeDatos.TipoGeneral.TipoString
                    .TipoCampoGS = -2
                    .Denominacion_BD = "USUARIO"
                Case CamposGeneralesVisor.MOTIVOVISIBILIDAD  '¿visibilidad?
                    .Denominacion = Textos(51)(1)
                    .EsLista = True
                    .TipoCampo = TiposDeDatos.TipoGeneral.TipoString
                    .Denominacion_BD = "MOTIVO_VISIBILIDAD"
                    .OpcionesLista = Get_Opciones_Lista_MotivoVisibilidad(Textos)
                Case CamposGeneralesVisor.MARCASEGUIMIENTO  'Marca para monitorización de solicitudes
                    .Denominacion = Textos(52)(1)
                    .EsLista = True
                    .TipoCampo = TiposDeDatos.TipoGeneral.TipoBoolean
                    .OpcionesLista = Get_Opciones_Lista_Boolean(Textos)
                    .Denominacion_BD = "SEG"
                    .AnchoVisualizacion = 25
                Case CamposGeneralesVisor.ESTADO  'Estado (Guardadas, En curso, Cerradas, Anuladas, Rechazadas, Finalizadas y Pendientes)
                    .Denominacion = Textos(89)(1)
                    .EsLista = True
                    .TipoCampo = TiposDeDatos.TipoGeneral.TipoString
                    .Denominacion_BD = "ESTADO"
                    .OpcionesLista = Get_Opciones_Lista_Estado(Textos)
                Case CamposGeneralesVisor.FECHATRASLADO  'Fecha de traslado
                    .Denominacion = Textos(106)(1)
                    .EsLista = False
                    .TipoCampo = TiposDeDatos.TipoGeneral.TipoFecha
                    .Denominacion_BD = "FECHA_ACT"
                Case CamposGeneralesVisor.PERSONATRASLADO  'Persona a la que se ha trasladado
                    .Denominacion = Textos(107)(1)
                    .EsLista = False
                    .TipoCampo = TiposDeDatos.TipoGeneral.TipoString
                    .TipoCampoGS = TiposDeDatos.TipoCampoGS.Persona
                    .Denominacion_BD = "TRASLADO_A_USU"
                Case CamposGeneralesVisor.PROVEEDORTRASLADO  'Proveedor a la que se ha trasladado
                    .Denominacion = Textos(108)(1)
                    .EsLista = False
                    .TipoCampo = TiposDeDatos.TipoGeneral.TipoString
                    .TipoCampoGS = TiposDeDatos.TipoCampoGS.Proveedor
                    .Denominacion_BD = "TRASLADO_A_PROV"
                Case CamposGeneralesVisor.FECHALIMITEDEVOLUCION  'Fecha limite devolucion
                    .Denominacion = Textos(109)(1)
                    .EsLista = False
                    .TipoCampo = TiposDeDatos.TipoGeneral.TipoFecha
                    .Denominacion_BD = "FECHA_LIMITE"
                Case CamposGeneralesVisor.PROVEEDOR
                    .Denominacion = Textos(154)(1)
                    .EsLista = False
                    .TipoCampo = TiposDeDatos.TipoGeneral.TipoString
                    .TipoCampoGS = TiposDeDatos.TipoCampoGS.Proveedor
                    .Denominacion_BD = "IDM.PROVE"
                Case CamposGeneralesVisor.ARTICULO
                    .Denominacion = Textos(155)(1)
                    .EsLista = False
                    .TipoCampo = TiposDeDatos.TipoGeneral.TipoString
                    .TipoCampoGS = TiposDeDatos.TipoCampoGS.NuevoCodArticulo
                    .Denominacion_BD = "IDM.ART4"
                Case CamposGeneralesVisor.FECHAFINFLUJO
                    .Denominacion = Textos(156)(1)
                    .EsLista = False
                    .TipoCampo = TiposDeDatos.TipoGeneral.TipoFecha
                    .Denominacion_BD = "FEC_FIN_FLUJO"
                Case CamposGeneralesVisor.DEPARTAMENTO
                    .Denominacion = Textos(182)(1)
                    .EsLista = True
                    .TipoCampo = TiposDeDatos.TipoGeneral.TipoString
                    .Denominacion_BD = "DEP"
                Case CamposGeneralesVisor.FECHAULTIMAAPROBACION
                    .Denominacion = Textos(228)(1)
                    .EsLista = False
                    .TipoCampo = TiposDeDatos.TipoGeneral.TipoFecha
                    .Denominacion_BD = "FECULTAPROBACION"
                Case CamposGeneralesVisor.ESTADOHOMOLOGACION
                    .Denominacion = Textos(229)(1)
                    .EsLista = True
                    .TipoCampo = TiposDeDatos.TipoGeneral.TipoNumerico
                    .TipoCampoGS = TiposDeDatos.TipoCampoGS.EstadoHomologacion
                    .Denominacion_BD = "ESTADO_HOMOLOGACION"
            End Select
        End With
        Return oCampo_General
    End Function
#End Region
    Public Sub GuardarOrdenacion(ByVal IdVista As Integer, ByVal sOrden As String)
        Authenticate()

        DBServer.FSN_Update_Escenario_Vista(IdVista, sOrden)
    End Sub
End Class
