﻿Public Class EscenarioFiltro
    Private _id As Integer
    Public Property Id() As Integer
        Get
            Return _id
        End Get
        Set(ByVal value As Integer)
            _id = value
        End Set
    End Property
    Private _escenario As Integer
    Public Property Escenario() As Integer
        Get
            Return _escenario
        End Get
        Set(ByVal value As Integer)
            _escenario = value
        End Set
    End Property
    Private _nombre As String
    Public Property Nombre() As String
        Get
            Return _nombre
        End Get
        Set(ByVal value As String)
            _nombre = value
        End Set
    End Property
    Private _posicion As Integer
    Public Property Posicion() As Integer
        Get
            Return _posicion
        End Get
        Set(ByVal value As Integer)
            _posicion = value
        End Set
    End Property
    Private _defecto As Boolean
    Public Property Defecto() As Boolean
        Get
            Return _defecto
        End Get
        Set(ByVal value As Boolean)
            _defecto = value
        End Set
    End Property
    Private _filtro_Campos As List(Of Escenario_Campo)
    Public Property Filtro_Campos() As List(Of Escenario_Campo)
        Get
            Return _filtro_Campos
        End Get
        Set(ByVal value As List(Of Escenario_Campo))
            _filtro_Campos = value
        End Set
    End Property
    Private _formulaAvanzada As Boolean
    Public Property FormulaAvanzada() As Boolean
        Get
            Return _formulaAvanzada
        End Get
        Set(ByVal value As Boolean)
            _formulaAvanzada = value
        End Set
    End Property
    Private _formulaCondiciones As List(Of EscenarioFiltroCondicion)
    Public Property FormulaCondiciones() As List(Of EscenarioFiltroCondicion)
        Get
            Return _formulaCondiciones
        End Get
        Set(ByVal value As List(Of EscenarioFiltroCondicion))
            _formulaCondiciones = value
        End Set
    End Property
    Private _errorFormula As Boolean
    Public Property ErrorFormula() As Boolean
        Get
            Return _errorFormula
        End Get
        Set(ByVal value As Boolean)
            _errorFormula = value
        End Set
    End Property
    Private _idVistaDefecto As Integer
    Public Property IdVistaDefecto() As Integer
        Get
            Return _idVistaDefecto
        End Get
        Set(ByVal value As Integer)
            _idVistaDefecto = value
        End Set
    End Property
    Public Sub New()
        Filtro_Campos = New List(Of Escenario_Campo)
        FormulaCondiciones = New List(Of EscenarioFiltroCondicion)
    End Sub
End Class
