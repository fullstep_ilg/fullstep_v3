﻿Imports System.Threading

<Serializable()> _
Public Class Condicion
    Inherits Security

    Private _oValorIzq As Object
    Private _oValorDer As Object
    Private _sOperador As String
    Private _iTipo As Integer

#Region "Propiedades"

    Public Property ValorIzq() As Object
        Get
            Return _oValorIzq
        End Get

        Set(ByVal Value As Object)
            _oValorIzq = Value
        End Set
    End Property

    Public Property ValorDer() As Object
        Get
            Return _oValorDer
        End Get

        Set(ByVal Value As Object)
            _oValorDer = Value
        End Set
    End Property

    Public Property Operador() As String
        Get
            Return _sOperador
        End Get

        Set(ByVal Value As String)
            _sOperador = Value
        End Set
    End Property

    Public Property Tipo() As Integer
        Get
            Return _iTipo
        End Get

        Set(ByVal Value As Integer)
            _iTipo = Value
        End Set
    End Property

#End Region

#Region "Constructor"

    ''' <summary>Constructor</summary>    
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean, ByVal oValorIzq As Object, ByVal oValorDer As Object, ByVal sOperador As String, ByVal iTipo As Integer)
        MyBase.New(dbserver, isAuthenticated)

        _oValorIzq = oValorIzq
        _oValorDer = oValorDer
        _sOperador = sOperador
        _iTipo = iTipo
    End Sub

#End Region

#Region "Métodos públicos"

    ''' <summary>Valida la condición</summary>
    ''' <returns>Booleano indicando si la validación ha sido correcta</returns>

    Public Function Validar() As Boolean
        Dim bOk As Boolean

        'Evaluar condición
        Select Case _iTipo
            Case 2, 3   'Numérico, fecha
                Select Case _sOperador
                    Case ">"
                        bOk = (_oValorIzq > _oValorDer)
                    Case "<"
                        bOk = (_oValorIzq < _oValorDer)
                    Case ">="
                        bOk = (_oValorIzq >= _oValorDer)
                    Case "<="
                        bOk = (_oValorIzq <= _oValorDer)
                    Case "="
                        bOk = (_oValorIzq = _oValorDer)
                    Case "<>"
                        bOk = (_oValorIzq <> _oValorDer)
                End Select
            Case 4    'Booleano            
                bOk = (IIf(IsNothing(_oValorIzq), -1, _oValorIzq) = IIf(IsNothing(_oValorDer), -1, _oValorDer))
            Case Else   'Texto
                Select Case UCase(_sOperador)
                    Case "="
                        bOk = (UCase(_oValorIzq) = UCase(_oValorDer))
                    Case "LIKE"
                        If Left(_oValorDer, 1) = "*" Then
                            If Right(_oValorDer, 1) = "*" Then
                                _oValorDer = _oValorDer.ToString.Replace("*", "")
                                bOk = (InStr(_oValorIzq.ToString, _oValorDer.ToString) >= 0)
                            Else
                                _oValorDer = _oValorDer.ToString.Replace("*", "")
                                bOk = (_oValorIzq.ToString.EndsWith(_oValorDer.ToString))
                            End If
                        Else
                            If Right(_oValorDer, 1) = "*" Then
                                _oValorDer = _oValorDer.ToString.Replace("*", "")
                                bOk = (_oValorIzq.ToString.StartsWith(_oValorDer.ToString))
                            Else
                                bOk = (UCase(_oValorIzq.ToString) = UCase(_oValorDer.ToString))
                            End If
                        End If
                    Case "<>"
                        Dim sValorIzq As String = String.Empty
                        If Not _oValorIzq Is Nothing Then sValorIzq = _oValorIzq.ToString
                        Dim sValorDer As String = String.Empty
                        If Not _oValorDer Is Nothing Then sValorDer = _oValorDer.ToString
                        bOk = UCase(sValorIzq) <> UCase(sValorDer)
                End Select
        End Select

        Return bOk
    End Function

#End Region    

End Class
