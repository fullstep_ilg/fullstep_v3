<Serializable()> _
    Public Class TablaExterna
    Inherits Security

#Region "Varialbes privadas"
    Private mlID As Long
    Private moDefTabla As DataSet
    Private moData As DataSet
    Private msPrimaryKey As String
    Private msArtKey As String
    Private msCampoMostrar As String
    Private msNombre As String
#End Region
#Region "Propiedades"
    Public Property ID() As Long
        Get
            Return mlID
        End Get
        Set(ByVal Value As Long)
            If mlID <> Value Then
                mlID = Value
                moData = Nothing
                moDefTabla = Nothing
                msPrimaryKey = Nothing
                msArtKey = Nothing
                msCampoMostrar = Nothing
                msNombre = Nothing
            End If
        End Set
    End Property
    Public ReadOnly Property Nombre() As String
        Get
            Return msNombre
        End Get
    End Property
    Public ReadOnly Property DefTabla() As Data.DataSet
        Get
            Return moDefTabla
        End Get

    End Property
    Public ReadOnly Property Data() As Data.DataSet
        Get
            Return moData
        End Get
    End Property
    Public ReadOnly Property PrimaryKey() As String
        Get
            Return msPrimaryKey
        End Get
    End Property
    Public ReadOnly Property ArtKey() As String
        Get
            Return msArtKey
        End Get
    End Property
    Public ReadOnly Property CampoMostrar() As String
        Get
            Return msCampoMostrar
        End Get
    End Property
#End Region
#Region "M�todos"
    Public Sub LoadDefTabla()
        LoadDefTabla(mlID)
    End Sub
    Public Sub LoadDefTabla(ByVal bFiltroArticulo As Boolean)
        LoadDefTabla(mlID, Nothing, bFiltroArticulo)
    End Sub
    Public Sub LoadDefTabla(ByVal lId As Long)
        LoadDefTabla(lId, Nothing, True)
    End Sub
    Public Sub LoadDefTabla(ByVal lId As Long, ByVal bFiltroArticulo As Boolean)
        LoadDefTabla(lId, Nothing, bFiltroArticulo)
    End Sub
    Public Sub LoadDefTabla(ByVal sArt As String)
        LoadDefTabla(mlID, sArt, True)
    End Sub
    ''' <summary>
    ''' Procedimiento que carga la definicion de una tabla externa
    ''' </summary>
    ''' <param name="lId">Identificador de la tabla externa</param>
    ''' <param name="sArt">Codigo de articulo</param>
    ''' <param name="bFiltroArticulo">Variable booleana que indica si se debe aplicar el filtro de articulo</param>
    ''' <remarks>
    ''' Llamada desde: PmWeb/tablaexternaServer/Page_load, PmServer/TablaExterna/LoadDefTabla
    ''' Tiempo m�ximo: 0,4 seg</remarks>
    Public Sub LoadDefTabla(ByVal lId As Long, ByVal sArt As String, ByVal bFiltroArticulo As Boolean)
        Authenticate()

        moDefTabla = DBServer.TablaExterna_LoadDefTabla(lId, sArt, bFiltroArticulo)

        mlID = lId
        msNombre = DBNullToSomething(moDefTabla.Tables(1).Rows(0).Item("DENOMINACION"))
        msPrimaryKey = moDefTabla.Tables(1).Rows(0).Item("PRIMARYKEY")
        msArtKey = DBNullToSomething(moDefTabla.Tables(1).Rows(0).Item("ARTKEY"))
        msCampoMostrar = DBNullToSomething(moDefTabla.Tables(1).Rows(0).Item("CAMPO_MOSTRAR"))
    End Sub
    Public Sub LoadData()
        LoadData(mlID)
    End Sub
    Public Sub LoadData(ByVal bFiltroArticulo As Boolean)
        LoadData(mlID, Nothing, bFiltroArticulo)
    End Sub
    Public Sub LoadData(ByVal lId As Long)
        LoadData(lId, Nothing, True)
    End Sub
    Public Sub LoadData(ByVal lId As Long, ByVal bFiltroArticulo As Boolean)
        LoadData(lId, Nothing, bFiltroArticulo)
    End Sub
    Public Sub LoadData(ByVal sArt As String)
        LoadData(mlID, sArt, True)
    End Sub
    Public Sub LoadData(ByVal lId As Long, ByVal sArt As String, ByVal bFiltroArticulo As Boolean)
        LoadData(lId, sArt, bFiltroArticulo, Nothing, Nothing, Nothing, Nothing)
    End Sub
    ''' <summary>
    ''' Procedimiento que cargala descripcion registrada de una tabla externa
    ''' </summary>
    ''' <param name="lId">Identificador de la tabla externa</param>
    ''' <param name="bFiltroArticulo">Variable booleana que indica si se debe aplicar el filtro de articulo</param>
    ''' <param name="lFilaIni">Fila de inicio</param>
    ''' <param name="Filtro">Filtro a aplicar</param>
    ''' <param name="iNumFilas">Cantidad de filas a cargar</param>
    ''' <param name="Orden">Orden a aplicar de las filas</param>
    ''' <param name="sArt">Codigo de articulo</param>
    ''' <remarks>
    ''' Llamada desde: PmWeb/tablaexterna/Actualizar_Grid y cmdAceptar_CLick y Page_load
    ''' Tiempo m�ximo: 0,5 seg</remarks>
    Public Sub LoadData(ByVal lId As Long, ByVal sArt As String, ByVal bFiltroArticulo As Boolean, ByVal iNumFilas As Integer, ByVal lFilaIni As Long, ByVal Orden As String, ByVal Filtro As String)
        Authenticate()
        moData = DBServer.TablaExterna_LoadData(lId, sArt, bFiltroArticulo, iNumFilas, lFilaIni, Orden, Filtro)
    End Sub
    ''' <summary>
    ''' Funcion que devuelve un registro de una tabla externa
    ''' </summary>
    ''' <param name="sTexto">Texto a devolver</param>
    ''' <param name="sArt">Codigo de articulo</param>
    ''' <param name="bBusqExacta">Variable booleana que indica si se debe realizar una b�squeda exacta</param>
    ''' <returns>Un registro que coincide con el que estamos buscando en la tabla externa</returns>
    ''' <remarks>
    ''' Llamada desde: PmWeb/desglose/page_load, PmWeb/campos/page_load, PmWeb/tablaexternaserver/page_load, PmWeb/tablaexternareg/page_load, PmWeb/tablaexterna/SeleccionarValor, PmServer/TablaExterna/CodigoArticuloReg
    ''' Tiempo m�ximo: 0,4 seg</remarks>
    Public Function BuscarRegistro(ByVal sTexto As String, Optional ByVal sArt As String = Nothing, Optional ByVal bBusqExacta As Boolean = False) As DataRow
        Authenticate()
        Return DBServer.TablaExterna_BuscarRegistro(mlID, sTexto, bBusqExacta, sArt)
    End Function
    Public Function DescripcionReg(ByVal ValorKey As Object, ByVal sIdi As String, ByVal DateFormat As Globalization.DateTimeFormatInfo, ByVal NumberFormat As Globalization.NumberFormatInfo) As String
        Dim sValor As String
        Dim oRow As DataRow = BuscarRegistro(ValorKey, Nothing, True)
        sValor = DescripcionReg(oRow, sIdi, DateFormat, NumberFormat)
        Return sValor
    End Function
    Public Function DescripcionReg(ByVal RegTabla As DataRow, ByVal sIdi As String, ByVal DateFormat As Globalization.DateTimeFormatInfo, ByVal NumberFormat As Globalization.NumberFormatInfo) As String
        Dim sValor As String = String.Empty
        If Not (moDefTabla Is Nothing Or RegTabla Is Nothing) Then
            Dim sTipoClave As String = String.Empty
            Dim sTipoMostrar As String = String.Empty
            For Each oRow As DataRow In moDefTabla.Tables(0).Rows
                If UCase(oRow.Item("CAMPO")) = UCase(msPrimaryKey) Then
                    sTipoClave = oRow.Item("TIPOCAMPO")
                ElseIf UCase(oRow.Item("CAMPO")) = UCase(msCampoMostrar) Then
                    sTipoMostrar = oRow.Item("TIPOCAMPO")
                End If
                If Not sTipoClave Is Nothing And (Not sTipoMostrar Is Nothing Or msCampoMostrar Is Nothing) Then _
                    Exit For
            Next
            If Not sTipoClave Is Nothing Then
                Dim dsTextos As DataSet = DBServer.Dictionary_GetData(TiposDeDatos.ModulosIdiomas.TablaExterna, sIdi)
                Dim oRow As DataRow = RegTabla
                If Not IsNothing(oRow) Then
                    Select Case sTipoClave
                        Case "FECHA"
                            sValor = CType(oRow.Item(msPrimaryKey), Date).ToString("d", DateFormat)
                        Case "NUMERICO"
                            Dim dblValor As Double = CType(oRow.Item(msPrimaryKey), Double)
                            sValor = dblValor.ToString("N" & IIf((dblValor Mod 1) > 0, "", "0"), NumberFormat)
                        Case Else
                            sValor = oRow.Item(msPrimaryKey).ToString()
                    End Select
                    If Not msCampoMostrar Is Nothing Then
                        Select Case sTipoMostrar
                            Case "FECHA"
                                sValor = sValor & CType(oRow.Item(msCampoMostrar), Date).ToString("d", DateFormat)
                            Case "NUMERICO"
                                Dim dblValor As Double = CType(oRow.Item(msCampoMostrar), Double)
                                sValor = sValor & dblValor.ToString("N" & IIf((dblValor Mod 1) > 0, "", "0"), NumberFormat)
                            Case Else
                                If oRow.Table.Columns(msCampoMostrar).DataType.ToString() = "System.Boolean" Then
                                    If oRow.Item(msCampoMostrar) Then
                                        sValor = sValor & " " & dsTextos.Tables(0).Select("ID=4")(0).Item(1)
                                    Else
                                        sValor = sValor & " " & dsTextos.Tables(0).Select("ID=5")(0).Item(1)
                                    End If
                                Else
                                    sValor = sValor & " " & oRow.Item(msCampoMostrar).ToString()
                                End If
                        End Select
                    End If
                End If
            End If
        End If
        Return sValor
    End Function
    Public Function CodigoArticuloReg(ByVal ValorKey As Object)
        If msArtKey Is Nothing Then
            Return Nothing
        Else
            Dim oRow As DataRow = BuscarRegistro(ValorKey.ToString(), Nothing, True)
            If Not oRow Is Nothing Then Return CodigoArticuloReg(oRow)
        End If
        Return Nothing
    End Function
    Public Function CodigoArticuloReg(ByVal FilaReg As DataRow)
        If msArtKey Is Nothing Then
            Return Nothing
        Else
            Return FilaReg.Item(msArtKey)
        End If
    End Function
#End Region
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
#Region "M�todos privados"
    Private Function QuitarEspacios(ByVal Cadena As String) As String
        Cadena = Trim(Cadena)
        Do While InStr(Cadena, "  ") > 0
            Replace(Cadena, "  ", " ")
        Loop
        Return Cadena
    End Function
#End Region
End Class