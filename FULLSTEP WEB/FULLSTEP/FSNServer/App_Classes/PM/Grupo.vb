<Serializable()> _
    Public Class Grupo
    Inherits Security

    Private mlId As Long

    Private moDen As MultiIdioma
    Private mlNumCampos As Long



    Private mdsCampos As DataSet


    Property Id() As Long
        Get
            Id = mlId
        End Get
        Set(ByVal Value As Long)
            mlId = Value
        End Set
    End Property
    Property Den(ByVal Idioma As String) As String
        Get
            Den = moDen(Idioma)

        End Get
        Set(ByVal Value As String)
            If Not moDen.Contains(Idioma) Then
                moDen.Add(Idioma, Value)
            Else
                moDen(Idioma) = Value
            End If

        End Set
    End Property

    Property NumCampos() As Long
        Get
            Return mlNumCampos
        End Get
        Set(ByVal Value As Long)
            mlNumCampos = Value
        End Set
    End Property

    Property DSCampos() As DataSet
        Get
            DSCampos = mdsCampos
        End Get
        Set(ByVal Value As DataSet)
            mdsCampos = Value
        End Set
    End Property


    Friend Sub CopyDen(ByVal oDen As MultiIdioma)
        moDen = oDen
    End Sub

    Public Sub New(ByRef dbserver As FSNDataBaseServer.Root,ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
        moDen = New MultiIdioma
    End Sub

End Class
