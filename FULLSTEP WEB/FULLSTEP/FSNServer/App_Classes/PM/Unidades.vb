<Serializable()> _
Public Class Unidades
    Inherits Security

    Private Contexto As System.Web.HttpContext
    Private moData As DataSet

    Public ReadOnly Property Data() As Data.DataSet
        Get
            Return moData
        End Get
    End Property

    ''' <summary>
    ''' Procedimiento que carga los datos de unidades
    ''' </summary>
    ''' <param name="sIdi">Idioma de la aplicaci�n</param>
    ''' <param name="sCod">Codigo de la moneda</param>
    ''' <param name="bLike">Variable booleana que indica si se debe usar like</param>
    ''' <param name="sDen">Denominacion de la unidad a cargar</param>
    ''' <remarks>
    ''' Llamada desde: PmWeb/impexp/DenGS ,PmWeb/desglose/PagE_load y CargarValoresDefecto ,PmWeb/campos/Page_load ,PmWeb/unidadesServer/PAge_load
    ''' Tiempo m�ximo: 0,25 seg</remarks>
    Public Sub LoadData(ByVal sIdi As String, Optional ByVal sCod As String = Nothing, Optional ByVal sDen As String = Nothing, Optional ByVal bLike As Boolean = True)
        Authenticate()

        Dim ds As DataSet
        If sCod Is Nothing AndAlso sDen Is Nothing Then
            Contexto = System.Web.HttpContext.Current
            ds = CType(Contexto.Cache("dsTodasUnidades" & sIdi), DataSet)
            If ds Is Nothing Then
                ds = DBServer.Unidades_Load(sIdi, sCod, sDen, bLike)
                Contexto.Cache.Insert("dsTodasUnidades" & sIdi, ds, Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration, New TimeSpan(0, System.Configuration.ConfigurationManager.AppSettings("TiempoExpiracionCacheMin"), 0))
            End If
        Else
            ds = DBServer.Unidades_Load(sIdi, sCod, sDen, bLike)
        End If
        moData = ds
    End Sub




    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
End Class
