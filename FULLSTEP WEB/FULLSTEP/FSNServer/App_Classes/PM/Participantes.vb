<Serializable()> _
    Public Class Participantes
    Inherits Security

    Private moData As DataSet
    Private mlInstancia As Integer
    Private mlRol As Integer
    Public Property Instancia() As Integer
        Get
            Return mlInstancia
        End Get
        Set(ByVal Value As Integer)
            mlInstancia = Value
        End Set
    End Property
    Public Property Rol() As Integer
        Get
            Return mlRol
        End Get
        Set(ByVal Value As Integer)
            mlRol = Value
        End Set
    End Property
    Public ReadOnly Property Data() As Data.DataSet
        Get
            Return moData
        End Get
    End Property
    ''' <summary>
    ''' Procedimiento que carga los Datos de las personas que son participanes de ina instancia del proceso, con los filtros pasados
    ''' </summary>
    ''' <param name="sCod">C�digo de usuario de la persona</param>
    ''' <param name="sNom">Nombre de la persona</param>
    ''' <param name="sApe">Apellido de la persona</param>
    ''' <param name="sUON1">Unidad Organizativa de primer nivel de la persona</param>
    ''' <param name="sUON2">Unidad Organizativa de segundo nivel de la persona</param>
    ''' <param name="sUON3">Unidad Organizativa de tercer nivel de la persona</param>
    ''' <param name="sDep">Departamento</param>
    ''' <param name="lDesdeFila">Posici�n de la primera fila a partir de ka cual se debe mostrar (no inclu�da)</param>
    ''' <param name="lNumFilas">N�mero de filas a extraer de la BDD</param>
    ''' <remarks>
    ''' Llamada desde: PmWeb/ParticipantesServer/PAge_load, PmWeb/Usuarios/cmdBuscar_ServerClick
    ''' Tiempo m�ximo: 0,35 seg</remarks>
    Public Sub LoadDataPersonas(Optional ByVal sCod As String = Nothing, Optional ByVal sNom As String = Nothing, Optional ByVal sApe As String = Nothing, Optional ByVal sUON1 As String = Nothing, Optional ByVal sUON2 As String = Nothing, Optional ByVal sUON3 As String = Nothing, Optional ByVal sDep As String = Nothing, Optional ByVal lDesdeFila As Long = Nothing, Optional ByVal lNumFilas As Long = Nothing)
        Authenticate()
        moData = DBServer.Participantes_Persona_Load(mlRol, mlInstancia, sCod, sNom, sApe, sUON1, sUON2, sUON3, sDep, lDesdeFila, lNumFilas)
    End Sub
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
End Class