<Serializable()> _
    Public Class Departamento
    Inherits Security

    Private msCod As String
    Private msDen As String

    Private moDepartamento As DataSet

    Public Property Cod() As String
        Get
            Return msCod
        End Get

        Set(ByVal Value As String)
            msCod = Value
        End Set
    End Property

    Public Property Den() As String
        Get
            Return msDen
        End Get

        Set(ByVal Value As String)
            msDen = Value
        End Set
    End Property

    Public ReadOnly Property Data() As Data.DataSet
        Get
            Return moDepartamento
        End Get

    End Property

    ''' <summary>
    ''' Procedimiento que carga los datos del departamento dado su c�digo
    ''' </summary>
    ''' <remarks>
    ''' Llamada desde: PmWeb/Campos/PAge_load , PmWeb/desglose/PAge_load y CargarValoresDefecto 
    ''' tiempo m�ximo: 0,25 seg</remarks>
    Public Sub LoadData()
        Authenticate()
        moDepartamento = DBServer.Departamento_Get(msCod)
    End Sub


    Public Sub New(ByRef dbserver As FSNDataBaseServer.Root,ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
    End Sub

    Public Function EstaDeBaja(ByVal sCod As String) As Boolean
        EstaDeBaja = DBServer.Departamento_EstaDeBaja(sCod)
    End Function
End Class

