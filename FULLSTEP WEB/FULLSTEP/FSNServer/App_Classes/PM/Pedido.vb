
<Serializable()> _
Public Class Pedido
    Inherits Security

    Private m_lInstancia As Long
    Private m_sDenInstancia As String
    Private mlID As Long
    Private m_lAnyo As Long
    Private m_lNumPedido As Long
    Private m_lNumOrden As Long
    Private m_sProv As String
    Private m_sDenProv As String
    Private m_iEstado As Integer
    Private m_dImporte As Double
    Private m_dCambio As Double
    Private m_sMoneda As String
    Private m_dFecha As Date
    Private m_sIDPeticionario As String
    Private m_sPeticionario As String
    Private m_sIDReceptor As String
    Private m_sReceptor As String
    Private m_sObservaciones As String
    Private m_sNumPedERP As String
    Private m_iTipo As Integer
    Private m_sDenTipoPedido As String
    Private m_sNumPedidoCompleto As String
    Private m_oAtributos As DataTable
    Private m_oAdjuntos As DataTable
    Private m_oLineas As DataTable
    Private m_bBajaLog As Boolean

    Public Property Instancia() As Long
        Get
            Return m_lInstancia
        End Get

        Set(ByVal Value As Long)
            m_lInstancia = Value
        End Set
    End Property

    Public Property DenInstacia() As String
        Get
            Return m_sDenInstancia
        End Get
        Set(ByVal Value As String)
            m_sDenInstancia = Value
        End Set
    End Property

    Public Property ID() As Long
        Get
            Return mlID
        End Get

        Set(ByVal Value As Long)
            mlID = Value
        End Set
    End Property

    Public Property Anyo() As Long
        Get
            Return m_lAnyo
        End Get

        Set(ByVal Value As Long)
            m_lAnyo = Value
        End Set
    End Property

    Public Property NumPedido() As Long
        Get
            Return m_lNumPedido
        End Get

        Set(ByVal Value As Long)
            m_lNumPedido = Value
        End Set
    End Property

    Public Property NumOrden() As Long
        Get
            Return m_lNumOrden
        End Get

        Set(ByVal Value As Long)
            m_lNumOrden = Value
        End Set
    End Property

    Public Property Prov() As String
        Get
            Return m_sProv
        End Get

        Set(ByVal Value As String)
            m_sProv = Value
        End Set
    End Property

    Public Property DenProv() As String
        Get
            Return m_sDenProv
        End Get

        Set(ByVal Value As String)
            m_sDenProv = Value
        End Set
    End Property

    Public Property Estado() As Integer
        Get
            Return m_iEstado
        End Get

        Set(ByVal Value As Integer)
            m_iEstado = Value
        End Set
    End Property

    Public Property Importe() As Double
        Get
            Return m_dImporte
        End Get
        Set(ByVal Value As Double)
            m_dImporte = Value
        End Set
    End Property

    Public Property Cambio() As Double
        Get
            Return m_dCambio
        End Get
        Set(ByVal Value As Double)
            m_dCambio = Value
        End Set
    End Property

    Public Property Moneda() As String
        Get
            Return m_sMoneda
        End Get
        Set(ByVal Value As String)
            m_sMoneda = Value
        End Set
    End Property

    Public Property Fecha() As Date
        Get
            Return m_dFecha
        End Get

        Set(ByVal Value As Date)
            m_dFecha = Value
        End Set
    End Property

    Public Property IDPeticionario() As String
        Get
            Return m_sIDPeticionario
        End Get
        Set(ByVal Value As String)
            m_sIDPeticionario = Value
        End Set
    End Property

    Public Property Peticionario() As String
        Get
            Return m_sPeticionario
        End Get
        Set(ByVal Value As String)
            m_sPeticionario = Value
        End Set
    End Property

    Public Property IDReceptor() As String
        Get
            Return m_sIDReceptor
        End Get
        Set(ByVal Value As String)
            m_sIDReceptor = Value
        End Set
    End Property

    Public Property Receptor() As String
        Get
            Return m_sReceptor
        End Get
        Set(ByVal Value As String)
            m_sReceptor = Value
        End Set
    End Property

    Public Property Observaciones() As String
        Get
            Return m_sObservaciones
        End Get
        Set(ByVal Value As String)
            m_sObservaciones = Value
        End Set
    End Property

    Public Property Tipo() As Integer
        Get
            Return m_iTipo
        End Get

        Set(ByVal Value As Integer)
            m_iTipo = Value
        End Set
    End Property

    Public Property NumPedErp() As String
        Get
            Return m_sNumPedERP
        End Get

        Set(ByVal Value As String)
            m_sNumPedERP = Value
        End Set
    End Property

    Public Property NumPedidoCompleto() As String
        Get
            Return m_sNumPedidoCompleto
        End Get

        Set(ByVal Value As String)
            m_sNumPedidoCompleto = Value
        End Set
    End Property

    Public Property DenTipoPedido() As String
        Get
            Return m_sDenTipoPedido
        End Get

        Set(ByVal Value As String)
            m_sDenTipoPedido = Value
        End Set
    End Property

    Public Property Atributos() As DataTable
        Get
            Return m_oAtributos
        End Get

        Set(ByVal Value As DataTable)
            m_oAtributos = Value
        End Set
    End Property

    Public Property Adjuntos() As DataTable
        Get
            Return m_oAdjuntos
        End Get

        Set(ByVal Value As DataTable)
            m_oAdjuntos = Value
        End Set
    End Property

    Public Property Lineas() As DataTable
        Get
            Return m_oLineas
        End Get

        Set(ByVal Value As DataTable)
            m_oLineas = Value
        End Set
    End Property
    Public Property BajaLog() As Boolean
        Get
            Return m_bBajaLog
        End Get
        Set(ByVal Value As Boolean)
            m_bBajaLog = Value
        End Set
    End Property

    ''' <summary>
    ''' Procedimiento que carga los datos de un pedido en el idioma del usuario
    ''' </summary>
    ''' <param name="sIdioma">Idioma de la aplicaci�n</param>
    ''' <remarks>
    ''' Llamada desde: PmWeb/atachedfilesPedido/Page_load, PmWeb/detallepedidovis/Page_load
    ''' Tiempo m�ximo: 0,6 seg</remarks>
    Public Sub Load(ByVal sIdioma As String)
        Authenticate()
        Dim data As DataSet
        Dim i As Integer

        data = DBServer.Pedido_Load(mlID, m_lInstancia, sIdioma)

        If Not data.Tables(0).Rows.Count = 0 Then

            For i = 0 To data.Tables(0).Rows.Count - 1
                m_lInstancia = data.Tables(0).Rows(i).Item("ID_INST")
                m_sDenInstancia = data.Tables(0).Rows(i).Item("DESCR_INST")
                m_lAnyo = data.Tables(0).Rows(i).Item("ANYO")
                m_lNumPedido = data.Tables(0).Rows(i).Item("PNUM")
                m_lNumOrden = data.Tables(0).Rows(i).Item("ONUM")
                m_sProv = DBNullToSomething(data.Tables(0).Rows(i).Item("PROVE"))
                m_sDenProv = DBNullToSomething(data.Tables(0).Rows(i).Item("PROVE_DEN"))
                m_iEstado = DBNullToSomething(data.Tables(0).Rows(i).Item("EST"))
                m_dImporte = DBNullToSomething(data.Tables(0).Rows(i).Item("IMPORTE"))
                m_dCambio = DBNullToSomething(data.Tables(0).Rows(i).Item("CAMBIO"))
                m_sMoneda = DBNullToSomething(data.Tables(0).Rows(i).Item("MON"))
                m_dFecha = DBNullToSomething(data.Tables(0).Rows(i).Item("FECEMISION"))
                m_sIDPeticionario = DBNullToSomething(data.Tables(0).Rows(i).Item("ID_PETICIONARIO"))
                m_sPeticionario = DBNullToSomething(data.Tables(0).Rows(i).Item("NOM_PETICIONARIO"))
                m_sIDReceptor = DBNullToSomething(data.Tables(0).Rows(i).Item("ID_RECEPTOR"))
                m_sReceptor = DBNullToSomething(data.Tables(0).Rows(i).Item("NOM_RECEPTOR"))
                m_sObservaciones = DBNullToSomething(data.Tables(0).Rows(i).Item("OBS"))
                m_sNumPedERP = DBNullToSomething(data.Tables(0).Rows(i).Item("NUM_PED_ERP"))
                m_iTipo = DBNullToSomething(data.Tables(0).Rows(i).Item("TIPO"))
                m_bBajaLog = DBNullToBoolean(data.Tables(0).Rows(i).Item("BAJA_LOG"))
            Next

            m_oAtributos = data.Tables(1)
            m_oAdjuntos = data.Tables(2)

            m_oLineas = data.Tables(3)
        End If

        data = Nothing
    End Sub

    ''' <summary>Funcion que devuelve el detalle de un pedido</summary>
    ''' <param name="sIdioma">Idioma</param>
    ''' <param name="Instancia">id del flujo de aprobaci�n</param>
    ''' <remarks>
    ''' Llamada desde: detalleFactura.aspx
    ''' Tiempo m�ximo:0,2 seg</remarks>
    Public Sub LoadDetalle(ByVal sIdioma As String, Optional ByVal Instancia As Long = 0)
        Dim oDS As DataSet

        Authenticate()
        oDS = DBServer.Pedido_DevolverDetalle(mlID, sIdioma, Instancia)

        If Not oDS.Tables(0).Rows.Count = 0 Then

            m_sNumPedidoCompleto = DBNullToStr(oDS.Tables(0).Rows(0).Item("PEDIDO"))
            If Instancia = 0 Then
                m_sPeticionario = DBNullToStr(oDS.Tables(0).Rows(0).Item("PETICIONARIO"))
                m_dFecha = DBNullToStr(oDS.Tables(0).Rows(0).Item("FECHA"))
                m_iEstado = DBNullToStr(oDS.Tables(0).Rows(0).Item("EST"))
                m_sDenTipoPedido = DBNullToStr(oDS.Tables(0).Rows(0).Item("TIPO_PEDIDO"))
                m_sNumPedERP = DBNullToStr(oDS.Tables(0).Rows(0).Item("NUM_PED_ERP"))
            End If
        End If

        oDS = Nothing
    End Sub

    Public Sub New(ByRef dbserver As FSNDataBaseServer.Root,ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
    End Sub

End Class

