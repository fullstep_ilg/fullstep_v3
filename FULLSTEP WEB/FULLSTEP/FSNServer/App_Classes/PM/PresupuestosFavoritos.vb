<Serializable()>
Public Class PresupuestosFavoritos
    Inherits Security
    Private moData As DataSet
    Public ReadOnly Property Data() As Data.DataSet
        Get
            Return moData
        End Get

    End Property
    ''' <summary>
    ''' Procedimiento que carga los Datos de los presupuestos favoritos, dados los datos de entrada
    ''' </summary>
    ''' <param name="sUser">Codigo de Usuario</param>
    ''' <param name="iTipo">Tipo de presupuesto</param>
    ''' <param name="sOrgCompras">Codigo de la organizacion de compras</param>
    ''' <remarks>
    ''' Llamada desde: PmWeb/PresAsig/Page_load, PmWeb/presupfavServer/Page_load
    ''' Tiempo m�ximo: 0,4 seg</remarks>
    Public Sub LoadData(ByVal sUser As String, ByVal iTipo As Integer, Optional ByVal sOrgCompras As String = "")
        Authenticate()
        moData = DBServer.Presupuestos_FavoritosLoad(sUser, iTipo, sOrgCompras)
    End Sub
    ''' <summary>
    ''' Funcion que inserta un nuevo presupuesto en los presupuestos favoritos del usuario, con los datos necesarios
    ''' </summary>
    ''' <param name="sUser">C�digo de usuario que esta utilizando la aplicaci�n</param>
    ''' <param name="iTipo">Tipo de presupuesto</param>
    ''' <param name="lPresupID">Identificador del presupuesto</param>
    ''' <param name="iNivel">Nivel del presupuesto</param>
    ''' <returns> Una variable de tipo entero que devolvera 0 si ha sido error y 1 en caso contrario</returns>
    ''' <remarks>
    ''' Llamada desde: PmWeb/PresuFavoritoServer/Page_load
    ''' Tiempo m�ximo: 0,25 seg</remarks>
    Public Function Insertar(ByVal sUser As String, ByVal iTipo As Integer, ByVal lPresupID As Long, ByVal iNivel As Integer) As Long
        Dim lNuevoId As Long
        Authenticate()
        lNuevoId = DBServer.PresupuestoFavorito_Insertar(sUser, iTipo, lPresupID, iNivel)
        Insertar = lNuevoId
    End Function
    ''' <summary>
    ''' funci�n que elimina un presupuesto favorito que tenga guardado un usuario de sus presupuestos favoritos dado su id y usuario
    ''' </summary>
    ''' <param name="sUser">C�digo de usuario de la persona que este utilizando la aplicaci�n</param>
    ''' <param name="iTipo">Tipo de presupuesto</param>
    ''' <param name="lID">Identificador del presupuesto</param>
    ''' <remarks>
    ''' Llamada desde: PmWeb/PresuFavoritoServer/Page_load
    ''' Tiempo m�ximo: 0,2 seg</remarks>
    Public Sub Eliminar(ByVal sUser As String, ByVal iTipo As Integer, ByVal lID As Long)
        Authenticate()
        DBServer.PresupuestoFavorito_Eliminar(sUser, iTipo, lID)
    End Sub
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
End Class