
<Serializable()> _
Public Class Procesos
    Inherits Security

    Private moProcesos As Collection
    Private moData As DataSet

    Public Sub New(ByRef dbserver As FSNDataBaseServer.Root,ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
    End Sub
    Public ReadOnly Property Procesos() As Collection
        Get
            Return moProcesos
        End Get

    End Property

    Public ReadOnly Property Data() As Data.DataSet
        Get
            Return moData
        End Get

    End Property

    Friend Sub Add(ByVal lId As Long, ByVal oDen As MultiIdioma)
        Dim oGroup As New Grupo(DBServer, mIsAuthenticated)
        If moProcesos Is Nothing Then
            moProcesos = New Collection
        End If
        oGroup.Id = lId
        oGroup.CopyDen(oDen)


        moProcesos.Add(oGroup, lId.ToString)

    End Sub

    Friend Sub Add(ByRef oGrupo As Grupo)
        If moProcesos Is Nothing Then
            moProcesos = New Collection
        End If
        moProcesos.Add(oGrupo, oGrupo.Id.ToString)
    End Sub

    'Friend Sub New(ByRef dbserver As Fullstep.WSDatabaseServer.Root, ByVal UserCode As String, ByVal UserPassword As String, ByVal isAuthenticated As Boolean)
    'MyBase.New(dbserver,isAuthenticated)
    'End Sub

    ''' <summary>
    ''' Procedimiento que carga los procesos
    ''' </summary>
    ''' <param name="sIdioma">Idioma del usuario</param>
    ''' <remarks>
    ''' Llamada desde: PmWeb/arachedfilesProceso/Page_load, PmWeb/detalleprocesovis/Page_load 
    ''' Tiempo m�ximo: 0,35 seg</remarks>
    Public Sub CargarProcesos(ByVal sIdioma As String, ByVal Anyo As Short, ByVal GMN1 As String)
        Authenticate()
        moData = DBServer.CargarProcesos(sIdioma, Anyo, GMN1)
    End Sub

End Class


