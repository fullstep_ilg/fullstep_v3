﻿<Serializable()> _
Public Class FiltrosContrato
    Inherits Security

    Public Sub New(ByRef dbserver As FSNDataBaseServer.Root,ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
    End Sub
    ''' <summary>
    ''' Carga los formularios disponibles para que el usuario pueda configurar sus filtros.
    ''' </summary>
    ''' <param name="sUsuario">Cod. de persona.</param>
    ''' <returns>Dataset con los formularios disponibles</returns>
    ''' <remarks></remarks>
    Public Function LoadDisponibles(ByVal sUsuario As String) As DataSet
        Authenticate()
        LoadDisponibles = DBServer.Filtros_Get(sUsuario, TiposDeDatos.TipoDeSolicitud.Contrato)
    End Function
    ''' <summary>
    ''' Obtiene los filtros configurados por el usuario
    ''' </summary>
    ''' <param name="sUsuario">Cod. de usuario</param>
    ''' <returns>Dataset con los filtros configurados para cargar las pestañas</returns>
    ''' <remarks>Llamada desde: VisorContratos.aspx. Tiempo máximo: 0,1seg.</remarks>
    Public Function LoadConfigurados(ByVal sUsuario As String) As DataSet
        Authenticate()
        LoadConfigurados = DBServer.Filtros_GetConfigurados(sUsuario, TiposDeDatos.TipoDeSolicitud.Contrato)
    End Function
End Class
