<Serializable()> _
    Public Class DiagBloques
    Inherits Security
    Private moDiagBloques As DataSet


    Public ReadOnly Property Data() As Data.DataSet
        Get
            Return moDiagBloques
        End Get

    End Property

    ''' <summary>
    ''' Procedimiento que devuelve los Diagolos de bloques(comentarios)
    ''' </summary>
    ''' <param name="lID">Identificador del bloque</param>
    ''' <param name="sIdi">Idioma de la aplicaci�n</param>
    ''' <remarks>
    ''' Llamada desde: PmWeb/flowDiagram/Page_Load, PmWeb/FlowdiagramExport/Page_Load
    ''' Tiempo m�ximo: 0,8 seg</remarks>
    Public Sub LoadData(ByVal lID As Long, ByVal sIdi As String)
        Authenticate()
        moDiagBloques = DBServer.DiagBloques_Get(lID, sIdi)
    End Sub


    Public Sub New(ByRef dbserver As FSNDataBaseServer.Root,ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
    End Sub


End Class

