<Serializable()>
Public Class PresConceptos3Nivel1
    Inherits Security
    Private moData As DataSet
    Public ReadOnly Property Data() As Data.DataSet
        Get
            Return moData
        End Get
    End Property
    ''' <summary>
    ''' Procedimiento que carga los Datos de los Conceptos presupuestarios dado unos niveles de 1 a 4
    ''' </summary>
    ''' <param name="iPres1">Codigo presupuestario de nivel 1</param>
    ''' <param name="iPres2">Codigo presupuestario de nivel 2</param>
    ''' <param name="iPres3">Codigo presupuestario de nivel 3</param>
    ''' <param name="iPres4">Codigo presupuestario de nivel 4</param>
    ''' <remarks>Llamada desde: PmWeb/PresAsig/Page_Load, PmWeb/presupuestos/Page_Load, PmWeb/campos/Page_Load, PmWeb/desglose/Page_Load - CargarValoresDefecto
    ''' Tiempo m�ximo: 0,34 seg</remarks>
    Public Sub LoadData(ByVal iPres1 As Integer, Optional ByVal iPres2 As Integer = Nothing, Optional ByVal iPres3 As Integer = Nothing, Optional ByVal iPres4 As Integer = Nothing)
        Authenticate()
        moData = DBServer.PresConceptos3_Get(iPres1, iPres2, iPres3, iPres4)
    End Sub
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
    Public Function GetNivelPres3() As Integer
        Authenticate()
        Return DBServer.GetNivelPres(3)
    End Function
End Class