<Serializable()> _
Public Class Centros    
    Inherits Lista(Of Centro)

    Private moCentros As DataSet
    Public ReadOnly Property Data() As Data.DataSet
        Get
            Return moCentros
        End Get
    End Property
    ''' <summary>Contructor</summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si est� autenticado</param>
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
    ''' <summary>Funci�n que devuelve e inserta en la colecci�n un objeto Centro</summary>
    Public Overloads Function Add(ByRef oCentro As Centro) As Centro
        Return Add(oCentro)
    End Function
    ''' <summary>
    ''' Funcion que carga los datos de los centros
    ''' </summary>
    ''' <param name="Usu">Codigo de usuario</param>
    ''' <param name="RestricUONSolUsuUON">Restriccion del usuario a las unidades organizativas del usuario</param>
    ''' <param name="RestricUONSolPerfUON">Restriccion del usuario a las unidades organizativas del perfil del usuario</param>
    ''' <param name="sCodOrgCompras">Codigo de la organizacion de compras</param>
    ''' <remarks>Llamada desde:PmWeb/ArticulosServer/Page_Load y ugtxtOrganizacionCompras_SelectedRowChanged, PmWeb/ArticulosServer/Page_Load, PmWeb/ArticulosServer/Page_Load
    ''' Tiempo m�ximo: 0,25 seg</remarks>
    Public Sub LoadData(ByVal Usu As String, ByVal RestricUONSolUsuUON As Boolean, ByVal RestricUONSolPerfUON As Boolean, _
                        Optional ByVal sCodOrgCompras As String = Nothing)
        Authenticate()
        If Not IsNothing(sCodOrgCompras) Then
            moCentros = DBServer.Centros_Load(Usu, RestricUONSolUsuUON, RestricUONSolPerfUON, sCodOrgCompras)
        Else
            moCentros = DBServer.Centros_Load(Usu, RestricUONSolUsuUON, RestricUONSolPerfUON)
        End If
    End Sub
    ''' <summary>Devuelve los centros asociados a un art�culo</summary>       
    ''' <param name="sCodArt">Cod. art�culo</param>
    ''' <param name="bAtravesItem">A trav�s de �tem</param>
    ''' <param name="iAnyo">Anyo</param>
    ''' <param name="sGMN1">GMN1</param>
    ''' <param name="iProce">Cod. proceso</param>
    ''' <param name="iItem">Id. �tem</param>
    ''' <param name="sCodCentro">Cod. centro</param>
    ''' <param name="sOrgCompras">Org. compras</param>
    ''' <param name="bAlmacen">Almac�n</param>
    ''' <remarks>Llamada desde: </remarks>
    Public Function DevolverCentrosPorArticulo(ByVal sCodArt As String, Optional ByVal bAtravesItem As Boolean = False, Optional ByVal iAnyo As Integer = 0, Optional ByVal sGMN1 As String = Nothing, Optional ByVal iProce As Integer = 0,
                                               Optional ByVal iItem As Long = 0, Optional ByVal sCodCentro As String = Nothing, Optional ByVal sOrgCompras As String = Nothing, Optional ByVal bAlmacen As Boolean = False,
                                               Optional ByVal sUsu As String = Nothing) As IEnumerable(Of Object)
        Authenticate()
        Dim ds As DataSet = DBServer.DevolverCentrosArticulo(sCodArt, bAtravesItem, iAnyo, sGMN1, iProce, iItem, sCodCentro, sOrgCompras, bAlmacen, sUsu)
        Return ds.Tables(0).Rows.OfType(Of DataRow).Select(Function(x) New With {Key .cod = x("CENTROS").ToString, .den = x("ALMACEN").ToString}).ToList
    End Function
    ''' <summary>Devuelve los centros asociados a un item</summary>       
    ''' <param name="iAnyo">Anyo</param>
    ''' <param name="sGMN1">GMN1</param>
    ''' <param name="iProce">Cod. proceso</param>
    ''' <param name="iItem">Id �tem</param>
    ''' <param name="sOrgCompras">Org. compras</param>
    ''' <remarks>Llamada desde: </remarks>
    Public Function DevolverCentrosPorItem(ByVal iAnyo As Integer, ByVal sGMN1 As String, ByVal iProce As Integer, ByVal iItem As Integer, Optional ByVal sOrgCompras As String = Nothing, Optional ByVal sUsu As String = Nothing) As IEnumerable(Of Object)
        Authenticate()
        Dim ds As DataSet = DBServer.DevolverCentrosItem(iAnyo, sGMN1, iProce, iItem, sOrgCompras, sUsu)
        Return ds.Tables(0).Rows.OfType(Of DataRow).Select(Function(x) New With {Key .cod = x("COD").ToString, .den = x("DEN").ToString}).ToList
    End Function
    ''' <summary>Devuelve los centros asociados a un �tem no codificado (si art�culo)</summary>       
    ''' <param name="iAnyo">Anyo</param>
    ''' <param name="sGMN1">GMN1</param>
    ''' <param name="iProce">Cod. proceso</param>
    ''' <param name="iItem">Id �tem</param>
    ''' <param name="sCentro">Centro</param>
    ''' <remarks>Llamada desde: </remarks>
    Public Function DevolverCentrosPorItemNoCodificado(ByVal iAnyo As Integer, ByVal sGMN1 As String, ByVal iProce As Integer, ByVal iItem As Integer, Optional ByVal sCentro As String = Nothing,
                                                       Optional ByVal sOrgCompras As String = Nothing, Optional ByVal sUsu As String = Nothing) As IEnumerable(Of Object)
        Authenticate()
        Dim ds As DataSet = DBServer.DevolverCentrosPorItemNoCodificado(iAnyo, sGMN1, iProce, iItem, sCentro,, sUsu)
        Return ds.Tables(0).Rows.OfType(Of DataRow).Select(Function(x) New With {Key .cod = x("COD").ToString, .den = x("DEN").ToString}).ToList
    End Function
    ''' <summary>Devuelve los centros asociados a un �tem no codificado (si art�culo)</summary>       
    ''' <remarks>Llamada desde: </remarks>
    Public Function DevolverCentrosOrgCompras(ByVal Usu As String, ByVal RestricUONSolUsuUON As Boolean, ByVal RestricUONSolPerfUON As Boolean, ByVal sOrgCompras As String) As IEnumerable(Of Object)
        Authenticate()
        Dim ds As DataSet = DBServer.Centros_Load(Usu, RestricUONSolUsuUON, RestricUONSolPerfUON, sOrgCompras)
        Return ds.Tables(0).Rows.OfType(Of DataRow).Select(Function(x) New With {Key .cod = x("COD").ToString, .den = x("DEN").ToString}).ToList
    End Function
End Class