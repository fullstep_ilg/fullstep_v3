Imports System.Threading
Imports System.Web
Imports System.Data.SqlClient

<Serializable()>
Public Class Instancia
	Inherits Security
#Region "Properties"
	Private mlID As Long
	Private mlSolicitud As Long
	Private moSolicitud As Solicitud
	Private moGrupos As Grupos
	Private m_sPeticionario As String
	Private m_sPeticionarioProve As String
	Private m_iPeticionarioProveContacto As Integer
	Private m_lEstado As Long
	Private m_lVersion As Long
	Private m_dFecAlta As Date
	Private m_sNombrePet As String
	Private m_oHistoricoEst As DataSet
	Private m_dImporte As Double
	Private m_sMoneda As String
	Private m_dCambio As Double
	Private m_sDenBloqueActual As String
	Private moDen As MultiIdioma
	Private moProcesos As DataSet
	Private moPedidos As DataSet
	Private m_lIdHistorico As Long
	Private moCamposDesglose As Collection
	Private m_sAprobadorActual As String
	Private m_sNomAprobadorActual As String
	Private m_sFecAsigAprobador As Date
	Private m_sDestinatarioEst As String
	Private m_sNomDestinatario As String
	Private m_sPersonaEst As String
	Private m_sNomPersonaEst As String
	Private m_bTrasladada As Boolean
	Private m_dFechaEstado As Date
	Private m_dFecLimiteTraslado As Date
	Private m_sComentario As String
	Private m_sCampoImporte As String
	Private m_sUnidadYDep As String
	Private m_iAccion As Integer
	Private moData As DataSet
	Private miEtapaActual As Long
	Private msDenEtapaActual As String
	Private miRolActual As Long
	Private miInstanciaBloque As Long
	Private mbInstanciaBloqueTrasladada As Boolean
	Private miInstanciaBloqueBloq As Integer
	Private m_iEnProceso As Integer
	Private m_iTipoVersion As Integer
	Private m_bCertifActivo As Boolean
	Private m_oSolicitudesHijas As DataSet
	Private mlNuevoID As Long
	Private m_iEstadoValidacion As Integer
	Private m_sErrorValidacion As String
	Private mbVerDetalleFlujo As Boolean
	Private mbVerDetallePersona As Boolean
	Private m_lIdFavorita As Long
	Private m_lIdInstanciaOrigen As Long
	Private mlDiferenciaUTCServidor As Long
	Private mbPermitirTraslados As Boolean
	Private m_bEsObservador As Boolean
	Private m_bDetalleEditable As Boolean
	Private m_sComprador As String
	Property Pedidos() As DataSet
		Get
			Pedidos = moPedidos
		End Get

		Set(ByVal Value As DataSet)
			moPedidos = Value
		End Set
	End Property
	Property Procesos() As DataSet
		Get
			Procesos = moProcesos
		End Get

		Set(ByVal Value As DataSet)
			moProcesos = Value
		End Set
	End Property
	Property Den(ByVal Idioma As String) As String
		Get
			Den = moDen(Idioma)
		End Get

		Set(ByVal Value As String)
			If Not moDen.Contains(Idioma) Then
				moDen.Add(Idioma, Value)
			Else
				moDen(Idioma) = Value
			End If

		End Set
	End Property
	Public Property BloqueActual() As String
		Get
			Return m_sDenBloqueActual
		End Get

		Set(ByVal Value As String)
			m_sDenBloqueActual = Value
		End Set
	End Property
	Public Property Moneda() As String
		Get
			Return m_sMoneda
		End Get

		Set(ByVal Value As String)
			m_sMoneda = Value
		End Set
	End Property
	Public Property Cambio As Double
		Get
			Return m_dCambio
		End Get
		Set(value As Double)
			m_dCambio = value
		End Set
	End Property
	Public Property Importe() As Double
		Get
			Return m_dImporte
		End Get
		Set(ByVal Value As Double)
			m_dImporte = Value
		End Set
	End Property
	Public Property HistoricoEst() As DataSet
		Get
			Return m_oHistoricoEst
		End Get

		Set(ByVal Value As DataSet)
			m_oHistoricoEst = Value
		End Set
	End Property
	Public Property ID() As Long
		Get
			Return mlID
		End Get

		Set(ByVal Value As Long)
			mlID = Value
		End Set
	End Property
	Public Property Solicitud() As Solicitud
		Get
			Solicitud = moSolicitud
		End Get
		Set(ByVal Value As Solicitud)
			moSolicitud = Value
		End Set
	End Property
	Property Grupos() As Grupos
		Get
			Grupos = moGrupos

		End Get
		Set(ByVal Value As Grupos)
			moGrupos = Value
		End Set
	End Property
	Public Property Peticionario() As String
		Get
			Return m_sPeticionario
		End Get
		Set(ByVal Value As String)
			m_sPeticionario = Value
		End Set
	End Property
	Public Property PeticionarioProve() As String
		Get
			Return m_sPeticionarioProve
		End Get

		Set(ByVal Value As String)
			m_sPeticionarioProve = Value
		End Set
	End Property
	Public Property PeticionarioProveContacto() As Integer
		Get
			Return m_iPeticionarioProveContacto
		End Get

		Set(ByVal Value As Integer)
			m_iPeticionarioProveContacto = Value
		End Set
	End Property
	Public Property NombrePeticionario() As String
		Get
			Return m_sNombrePet
		End Get

		Set(ByVal Value As String)
			m_sNombrePet = Value
		End Set
	End Property
	Public Property Estado() As Long
		Get
			Return m_lEstado
		End Get

		Set(ByVal Value As Long)
			m_lEstado = Value
		End Set
	End Property
	Public Property Version() As Long
		Get
			Return m_lVersion
		End Get

		Set(ByVal Value As Long)
			m_lVersion = Value
		End Set
	End Property
	Public Property FechaAlta() As Date
		Get
			Return m_dFecAlta
		End Get

		Set(ByVal Value As Date)
			m_dFecAlta = Value
		End Set
	End Property
	Public Property IdHistorico() As Long
		Get
			Return m_lIdHistorico
		End Get

		Set(ByVal Value As Long)
			m_lIdHistorico = Value
		End Set
	End Property
	Public Property AprobadorActual() As String
		Get
			Return m_sAprobadorActual
		End Get

		Set(ByVal Value As String)
			m_sAprobadorActual = Value
		End Set
	End Property
	Public Property NombreAprobadorActual() As String
		Get
			Return m_sNomAprobadorActual
		End Get

		Set(ByVal Value As String)
			m_sNomAprobadorActual = Value
		End Set
	End Property
	Public Property FecAsigAprobador() As Date
		Get
			Return m_sFecAsigAprobador
		End Get

		Set(ByVal Value As Date)
			m_sFecAsigAprobador = Value
		End Set
	End Property
	Public Property DestinatarioEst() As String
		Get
			Return m_sDestinatarioEst
		End Get

		Set(ByVal Value As String)
			m_sDestinatarioEst = Value
		End Set
	End Property
	Public Property NombreDestinatario() As String
		Get
			Return m_sNomDestinatario
		End Get

		Set(ByVal Value As String)
			m_sNomDestinatario = Value
		End Set
	End Property
	Public Property PersonaEst() As String
		Get
			Return m_sPersonaEst
		End Get

		Set(ByVal Value As String)
			m_sPersonaEst = Value
		End Set
	End Property
	Public Property NombrePersonaEst() As String
		Get
			Return m_sNomPersonaEst
		End Get

		Set(ByVal Value As String)
			m_sNomPersonaEst = Value
		End Set
	End Property
	Public Property Trasladada() As Boolean
		Get
			Return m_bTrasladada
		End Get

		Set(ByVal Value As Boolean)
			m_bTrasladada = Value
		End Set
	End Property
	Public Property FechaEstado() As Date
		Get
			Return m_dFechaEstado
		End Get

		Set(ByVal Value As Date)
			m_dFechaEstado = Value
		End Set
	End Property
	Public Property FecLimiteTraslado() As Date
		Get
			Return m_dFecLimiteTraslado
		End Get

		Set(ByVal Value As Date)
			m_dFecLimiteTraslado = Value
		End Set
	End Property
	Public Property ComentarioEstado() As String
		Get
			Return m_sComentario
		End Get

		Set(ByVal Value As String)
			m_sComentario = Value
		End Set
	End Property
	Public Property CampoImporte() As String
		Get
			Return m_sCampoImporte
		End Get

		Set(ByVal Value As String)
			m_sCampoImporte = Value
		End Set
	End Property
	Public Property UnidadYDep() As String
		Get
			Return m_sUnidadYDep
		End Get

		Set(ByVal Value As String)
			m_sUnidadYDep = Value
		End Set
	End Property
	Public Property Accion() As Integer
		Get
			Return m_iAccion
		End Get
		Set(ByVal Value As Integer)
			m_iAccion = Value
		End Set
	End Property
	Public Property Etapa() As Long
		Get
			Return miEtapaActual
		End Get
		Set(ByVal Value As Long)
			miEtapaActual = Value
		End Set
	End Property
	Public Property DenEtapaActual() As String
		Get
			Return msDenEtapaActual
		End Get
		Set(ByVal Value As String)
			msDenEtapaActual = Value
		End Set
	End Property
	Public Property RolActual() As Long
		Get
			Return miRolActual
		End Get
		Set(ByVal Value As Long)
			miRolActual = Value
		End Set
	End Property
	Public Property InstanciaBloque() As Integer
		Get
			Return miInstanciaBloque
		End Get
		Set(ByVal Value As Integer)
			miInstanciaBloque = Value
		End Set
	End Property
	Public Property InstanciaBloqueTrasladada() As Boolean
		Get
			Return mbInstanciaBloqueTrasladada
		End Get
		Set(ByVal Value As Boolean)
			mbInstanciaBloqueTrasladada = Value
		End Set
	End Property
	Public Property InstanciaBloqueBloq() As Integer
		Get
			Return miInstanciaBloqueBloq
		End Get
		Set(ByVal Value As Integer)
			miInstanciaBloqueBloq = Value
		End Set
	End Property
	Public Property EnProceso() As Integer
		Get
			Return m_iEnProceso
		End Get
		Set(ByVal Value As Integer)
			m_iEnProceso = Value
		End Set
	End Property
	Public Property TipoVersion() As Integer
		Get
			Return m_iTipoVersion
		End Get
		Set(ByVal Value As Integer)
			m_iTipoVersion = Value
		End Set
	End Property
	Public Property CertifActivo() As Boolean
		Get
			Return m_bCertifActivo
		End Get
		Set(ByVal Value As Boolean)
			m_bCertifActivo = Value
		End Set
	End Property
	Public Property DetalleSolicitudesHijas() As DataSet
		Get
			Return m_oSolicitudesHijas
		End Get

		Set(ByVal Value As DataSet)
			m_oSolicitudesHijas = Value
		End Set
	End Property
	Public Property NuevoID() As Long
		Get
			Return mlNuevoID
		End Get

		Set(ByVal Value As Long)
			mlNuevoID = Value
		End Set
	End Property
	Public Property EstadoValidacion() As Integer
		Get
			Return m_iEstadoValidacion
		End Get
		Set(ByVal Value As Integer)
			m_iEstadoValidacion = Value
		End Set
	End Property
	Public Property ErrorValidacion() As String
		Get
			Return m_sErrorValidacion
		End Get
		Set(ByVal Value As String)
			m_sErrorValidacion = Value
		End Set
	End Property
	Public Property CamposDesglose() As Collection
		Get
			Return moCamposDesglose
		End Get
		Set(ByVal Value As Collection)
			moCamposDesglose = Value
		End Set
	End Property
	Public Property VerDetalleFlujo() As Boolean
		Get
			Return mbVerDetalleFlujo
		End Get
		Set(ByVal Value As Boolean)
			mbVerDetalleFlujo = Value
		End Set
	End Property
	Public Property VerDetallePersona() As Boolean
		Get
			Return mbVerDetallePersona
		End Get
		Set(ByVal Value As Boolean)
			mbVerDetallePersona = Value
		End Set
	End Property
	Public Property IdFavorita() As Long
		Get
			Return m_lIdFavorita
		End Get

		Set(ByVal Value As Long)
			m_lIdFavorita = Value
		End Set
	End Property
	Public Property IdInstanciaOrigen() As Long
		Get
			Return m_lIdInstanciaOrigen
		End Get

		Set(ByVal Value As Long)
			m_lIdInstanciaOrigen = Value
		End Set
	End Property
	Public Property DiferenciaUTCServidor() As Long
		Get
			Return mlDiferenciaUTCServidor
		End Get

		Set(ByVal Value As Long)
			mlDiferenciaUTCServidor = Value
		End Set
	End Property
	Public Property PermitirTraslados() As Boolean
		Get
			Return mbPermitirTraslados
		End Get
		Set(ByVal Value As Boolean)
			mbPermitirTraslados = Value
		End Set
	End Property
	Public Property EsObservador() As Boolean
		Get
			Return m_bEsObservador
		End Get
		Set(ByVal Value As Boolean)
			m_bEsObservador = Value
		End Set
	End Property
	Public Property DetalleEditable() As Boolean
		Get
			Return m_bDetalleEditable
		End Get
		Set(ByVal Value As Boolean)
			m_bDetalleEditable = Value
		End Set
	End Property
	Public Property Comprador() As String
		Get
			Return m_sComprador
		End Get
		Set(ByVal Value As String)
			m_sComprador = Value
		End Set
	End Property
	Private _idFormulario As Long
	Public Property IdFormulario() As Long
		Get
			Return _idFormulario
		End Get
		Set(ByVal value As Long)
			_idFormulario = value
		End Set
	End Property
	Private _idWorkFlow As Long
	Public Property IdWorkflow() As Long
		Get
			Return _idWorkFlow
		End Get
		Set(ByVal value As Long)
			_idWorkFlow = value
		End Set
	End Property
	Private _pedidoDirecto As Double
	Public Property PedidoDirecto() As Double
		Get
			Return _pedidoDirecto
		End Get
		Set(ByVal value As Double)
			_pedidoDirecto = value
		End Set
	End Property
#End Region
	''' <summary>
	''' Cargar las propiedades de la Instancia
	''' </summary>
	''' <param name="sIdi">Idioma</param>        
	''' <param name="bNuevoWorkflow">True Pm False Qa</param>
	''' <remarks>Llamada desde: Multiples pantallas (todos los oIntancia.load); Tiempo m�ximo: 0,2</remarks>
	Public Sub Load(ByVal sIdi As String, Optional ByVal bNuevoWorkflow As Boolean = False)
		Authenticate()

		Dim data As DataSet
		data = DBServer.Instancia_Load(mlID, sIdi, bNuevoWorkflow)

		If Not data.Tables.Count = 0 AndAlso Not data.Tables(0).Rows.Count = 0 Then
			m_sPeticionario = data.Tables(0).Rows(0).Item("PETICIONARIO").ToString
			m_sNombrePet = data.Tables(0).Rows(0).Item("NOMBRE_PET").ToString
			m_lEstado = DBNullToSomething(data.Tables(0).Rows(0).Item("ESTADO"))
			m_lVersion = DBNullToSomething(data.Tables(0).Rows(0).Item("NUM_VERSION"))
			m_dFecAlta = DBNullToSomething(data.Tables(0).Rows(0).Item("FEC_ALTA"))
			m_dImporte = DBNullToSomething(data.Tables(0).Rows(0).Item("IMPORTE"))
			m_sMoneda = DBNullToSomething(data.Tables(0).Rows(0).Item("MON"))
			m_lIdHistorico = DBNullToSomething(data.Tables(0).Rows(0).Item("INSTANCIA_EST"))
			_idFormulario = DBNullToInteger(data.Tables(0).Rows(0).Item("FORMULARIO"))
			_idWorkFlow = DBNullToInteger(data.Tables(0).Rows(0).Item("WORKFLOW"))
			m_sPeticionarioProve = DBNullToSomething(data.Tables(0).Rows(0).Item("PETICIONARIO_PROVE"))
			m_iPeticionarioProveContacto = DBNullToSomething(data.Tables(0).Rows(0).Item("PETICIONARIO_CON"))


			mlSolicitud = DBNullToSomething(data.Tables(0).Rows(0).Item("SOLICITUD"))
			moSolicitud = New Solicitud(DBServer, mIsAuthenticated)
			moSolicitud.ID = mlSolicitud
			moSolicitud.TipoSolicit = DBNullToSomething(data.Tables(0).Rows(0).Item("TIPO"))
			moSolicitud.Pub = DBNullToBoolean(data.Tables(0).Rows(0).Item("SOLICITUD_PUBLICADA"))
			If bNuevoWorkflow Then
				moSolicitud.Codigo = DBNullToSomething(data.Tables(0).Rows(0).Item("COD_SOLICIT"))
				moSolicitud.Den(sIdi) = DBNullToSomething(data.Tables(0).Rows(0).Item("DEN_SOLICIT"))
				moSolicitud.Descr(sIdi) = DBNullToSomething(data.Tables(0).Rows(0).Item("DESCR_SOLICIT"))
			End If
			moSolicitud.ValidacionesIntegracion = New PM_ValidacionesIntegracion(DBServer, mIsAuthenticated)

			moSolicitud.Formulario = New Formulario(DBServer, mIsAuthenticated)
			moSolicitud.Formulario.Id = _idFormulario

			m_sPersonaEst = DBNullToSomething(data.Tables(0).Rows(0).Item("PERSONA_ESTADO"))
			If bNuevoWorkflow Then m_sNomPersonaEst = DBNullToSomething(data.Tables(0).Rows(0).Item("NOMBRE_PERSONA_EST"))
			m_dFechaEstado = DBNullToSomething(data.Tables(0).Rows(0).Item("FECHA_ESTADO"))
			m_sComentario = DBNullToSomething(data.Tables(0).Rows(0).Item("COMENT"))

			m_bTrasladada = IIf(data.Tables(0).Rows(0).Item("TRASLADADA") = 1, True, False)
			m_sCampoImporte = DBNullToSomething(data.Tables(0).Rows(0).Item("CAMPO_IMPORTE"))
			m_dCambio = DBNullToSomething(data.Tables(0).Rows(0).Item("CAMBIO"))

			moDen(sIdi) = data.Tables(0).Rows(0).Item("DESCR").ToString

			m_iAccion = DBNullToSomething(data.Tables(0).Rows(0).Item("ACCION"))
			m_iEnProceso = DBNullToSomething(data.Tables(0).Rows(0).Item("EN_PROCESO"))

			If Not bNuevoWorkflow Then m_iTipoVersion = DBNullToSomething(data.Tables(0).Rows(0).Item("VITIPO"))
		End If
		data = Nothing
	End Sub
	''' <summary></summary>
	''' <param name="sIdi">Codigo idioma</param>
	''' <param name="sUsuario">Codigo usuario</param> 
	''' <param name="sProve">Codigo proveedor</param>       
	''' <param name="bNuevoWorkfl">si tiene workflow o no</param>
	''' <param name="bObservador">Si se trata de un observador o no</param>
	''' <param name="sFormatoFecha">Formato fecha usuario</param>
	''' <param name="bEsObsYPart">True si el usuario ha participado en el flujo con un rol y tambien es observador</param>
	''' <remarks>Llamada desde=Detalle.aspx.vb // NWgestionInstancia.aspx.vb
	'''                        instanciaasignada.aspx.vb   // gestiontrasladada.aspx.vb
	'''                        flowDiagramExport.aspx.vb   // flowDiagram.aspx.vb
	'''                        detalleSolicConsulta.aspx.vb// aprobacion.aspx.vb
	'''                        NWDetalleSolicitud.aspx.vb  // impexp.aspx.vb
	'''                        noconformidadRevisor.aspx.vb// service.asmx.vb
	'''                        detalleCertificado.aspx.vb
	'''; Tiempo m�ximo=depende de los campos..grupos.</remarks>
	Public Sub CargarCamposInstancia(ByVal sIdi As String, Optional ByVal sUsuario As String = Nothing, Optional ByVal sProve As String = Nothing,
									 Optional ByVal bNuevoWorkfl As Boolean = False, Optional ByVal bObservador As Boolean = False,
									 Optional ByVal sFormatoFecha As String = "", Optional ByVal lTipoWorkflow As Long = 0, Optional ByVal lBloque As Long = 0,
									 Optional ByVal lRol As Long = 0, Optional ByVal bEsObsYPart As Boolean = False)
		Authenticate()

		Dim data As DataSet
		Dim oGroupRow As DataRow
		Dim oGrupo As Grupo
		data = DBServer.Instancia_CargarGruposYCampos(mlID, m_lVersion, sIdi, sUsuario, sProve, bNuevoWorkfl, bObservador, sFormatoFecha, lTipoWorkflow, lBloque, lRol, bEsObsYPart)
		If Not data.Tables(0).Rows.Count = 0 Then
			If Not data.Tables(1).Rows.Count = 0 And Not data.Tables(2).Rows.Count = 0 Then
				moGrupos = New Grupos(DBServer, mIsAuthenticated)
				For Each oGroupRow In data.Tables(0).Rows(0).GetChildRows("REL_INST_GRUPO")
					oGrupo = New Grupo(DBServer, mIsAuthenticated)
					oGrupo.Id = oGroupRow.Item("ID")
					oGrupo.NumCampos = oGroupRow.Item("NUMCAMPOS")
					oGrupo.Den(sIdi) = oGroupRow.Item("DEN_" & sIdi).ToString
					oGrupo.DSCampos = New DataSet
					oGrupo.DSCampos.Merge(oGroupRow.GetChildRows("REL_GRUPO_CAMPO"))
					oGrupo.DSCampos.Merge(oGroupRow.GetChildRows("REL_GRUPO_LISTA"))
					If oGrupo.DSCampos.Tables.Count > 1 Then
						oGrupo.DSCampos.Relations.Add("REL_CAMPO_LISTA", oGrupo.DSCampos.Tables(0).Columns("ID"), oGrupo.DSCampos.Tables(1).Columns("CAMPO_DEF"), False)
					End If
					oGrupo.DSCampos.Merge(oGroupRow.GetChildRows("REL_GRUPO_ADJUNTO"))
					If oGrupo.DSCampos.Tables.Count > 2 Then
						oGrupo.DSCampos.Relations.Add("REL_CAMPO_ADJUN", oGrupo.DSCampos.Tables(0).Columns("ID_CAMPO"), oGrupo.DSCampos.Tables(2).Columns("CAMPO"), False)
					ElseIf (oGrupo.DSCampos.Tables.Count = 2 And oGrupo.DSCampos.Relations.Count = 0) Then
						oGrupo.DSCampos.Relations.Add("REL_CAMPO_ADJUN", oGrupo.DSCampos.Tables(0).Columns("ID_CAMPO"), oGrupo.DSCampos.Tables(1).Columns("CAMPO"), False)
					End If

					Dim aux, aux2 As Integer
					aux = oGrupo.DSCampos.Tables.Count()
					oGrupo.DSCampos.Merge(oGroupRow.GetChildRows("REL_CAMPO_BLOQUEO"))
					aux2 = oGrupo.DSCampos.Tables.Count()
					If aux < aux2 Then
						oGrupo.DSCampos.Relations.Add("REL_CAMPO_BLOQUEO", oGrupo.DSCampos.Tables(0).Columns("ID"), oGrupo.DSCampos.Tables(aux).Columns("CAMPO"), False)
					End If

					moGrupos.Add(oGrupo)
				Next
			End If
			''Carga el la instancia la diferencia horaria del servidor si es de QA
			If Not bNuevoWorkfl Then
				If data.Tables.Count >= 5 Then
					mlDiferenciaUTCServidor = data.Tables(5).Rows(0).Item("DIFERENCIA")
				End If
			End If
		End If

		If moSolicitud Is Nothing Then
			mlSolicitud = data.Tables(0).Rows(0)("SOLICITUD")
			moSolicitud = New Solicitud(DBServer, mIsAuthenticated)
			moSolicitud.ID = mlSolicitud
			_idFormulario = data.Tables(0).Rows(0)("FORMULARIO")
			moSolicitud.Formulario = New Formulario(DBServer, mIsAuthenticated)
			moSolicitud.Formulario.Id = _idFormulario
			moSolicitud.ValidacionesIntegracion = New PM_ValidacionesIntegracion(DBServer, mIsAuthenticated)
		End If

		data = Nothing
	End Sub
	Public Sub CargarCamposDesglose(ByVal sIdi As String, ByVal sPer As String)
		Authenticate()
		moCamposDesglose = New Collection
		For Each oGrupo As Grupo In moGrupos.Grupos
			For Each dr As DataRow In oGrupo.DSCampos.Tables(0).Select("SUBTIPO=9")
				Dim oCampo As Campo = New Campo(DBServer, mIsAuthenticated)
				oCampo.Id = dr.Item("ID")
				oCampo.LoadInstDesglose(sIdi, mlID, sPer, m_lVersion, , True)
				moCamposDesglose.Add(oCampo, "Campo" & oCampo.Id.ToString())
			Next
		Next
	End Sub
	Public Sub CargarCamposDesglosePortal(ByVal sIdi As String, ByVal sCodProve As String)
		Authenticate()
		moCamposDesglose = New Collection
		For Each oGrupo As Grupo In moGrupos.Grupos
			For Each dr As DataRow In oGrupo.DSCampos.Tables(0).Select("SUBTIPO=9")
				Dim oCampo As Campo = New Campo(DBServer, mIsAuthenticated)
				oCampo.Id = dr.Item("ID")
				oCampo.LoadInstDesglose(sIdi, mlID, lVersion:=m_lVersion, sProve:=sCodProve, bNuevoWorkflow:=True)
				moCamposDesglose.Add(oCampo, "Campo" & oCampo.Id.ToString())
			Next
		Next
	End Sub
	''' <summary>
	''' Procedimiento que carga el historico de los estados de la instancia
	''' </summary>
	''' <param name="sIdi">Idioma de la aplicaci�n</param>
	''' <remarks>
	''' Llamada desde: PmWeb/cometariossolic/page_load, PmWeb/historicoestados/page_load, PmWeb/NwComentariossolic/page_load, PmWeb/NWHistoricoestados/page_load
	''' Tiempo m�ximo: 1 seg</remarks>
	Public Sub CargarHistoricoEstados(Optional ByVal sIdi As String = Nothing)
		Authenticate()
		m_oHistoricoEst = DBServer.Instancia_CargarHistoricoEstados(mlID, sIdi)
	End Sub
	''' <summary>
	''' Inserta los datos generales de la instancia.
	''' </summary>
	''' <param name="idEmpSolicitudPedido">Id de la empresa del pedido que inicia una solicitud de pedido</param> 
	''' <remarks>Llamada desde: guardarinstancia.aspx.vb; Tiempo m�ximo: 1 sg.</remarks>
	Public Sub Create_Prev(Optional ByVal idEmpSolicitudPedido As Long = Nothing, Optional ByVal sMonPedido As String = Nothing, Optional ByVal IdInstanciaImportar As Long = Nothing)
		Authenticate()

		Dim sMon As String
		If IsNothing(sMonPedido) Then
			sMon = Moneda
		Else
			sMon = sMonPedido
		End If
		DBServer.Instancia_Create_Prev(Solicitud.ID, Solicitud.Formulario.Id, CLng(Solicitud.Workflow.ToString), Solicitud.PedidoDirecto,
												Peticionario, sMon, mlID, IdFavorita, Comprador, idEmpSolicitudPedido, IdInstanciaOrigen)
	End Sub
	''' <summary>
	''' Procedimiento que crea una nueva instancia de proceso
	''' </summary>
	''' <param name="oDs">DataSet con los datos de los campos de la instancia</param>
	''' <param name="bGuardar">Variable booleana que indica si se debe guardar</param>
	''' <param name="bPedidoAut">Variable booleana que indica si debe ser un pedido autom�tico</param>
	''' <param name="sComentario">Comentario del peticionario</param>
	''' <param name="lNoconformidad">Id de la no conformidad</param>
	''' <param name="lCertificado">Id del certificado</param>
	''' <param name="bPortal">Variable booleana que indica si biene de portal</param>
	''' <param name="sProveGS">Proveedor de GS</param>
	''' <param name="lIdContrato">Id de Contrato</param>
	'''  <param name="lSolicitud">Id de la solicitud</param>
	''' <param name="sStoredEnEjecucion">Stored donde falla</param>
	''' <remarks>
	''' Llamada desde: PmWeb/GuardarInstancia/Create, PmWeb/alta/GuardarConWorkFlow, WebServicePM/Service.asmx/ServiceThread
	''' Tiempo m�ximo: 1,5 seg</remarks>
	Public Sub Create(ByVal oDs As DataSet, ByVal bGuardar As Boolean, ByRef tipoOperacionProcesamientoGuardado As Integer,
					Optional ByVal bPedidoAut As Boolean = False, Optional ByVal sComentario As String = Nothing,
					Optional ByVal lNoconformidad As Long = 0, Optional ByVal lCertificado As Long = 0,
					Optional ByVal bPortal As Boolean = False, Optional ByVal sProveGS As String = "",
					Optional ByVal lSolicitud As Long = 0, Optional ByVal lIdContrato As Long = 0,
					Optional ByRef sStoredEnEjecucion As String = "", Optional ByVal sIdioma As String = "",
					Optional ByVal lIdArchivoContrato As Long = 0, Optional ByVal sUsuNom As String = "",
					Optional ByVal tipo As Integer = 0, Optional ByVal tipoSolicitud As Integer = 0, Optional ByVal Rol As Long = 0)
		Authenticate()

		If bPortal Then
			DBServer.Instancia_Create(lSolicitud, _idFormulario, _idWorkFlow, 0, Peticionario, PeticionarioProve, PeticionarioProveContacto, oDs, mlID, bGuardar, m_dImporte, bPedidoAut,
									m_lEstado, sComentario, lNoconformidad, lCertificado, bPortal, sProveGS,
									lIdContrato, sStoredEnEjecucion, sIdioma, lIdArchivoContrato, sUsuNom, tipo, tipoSolicitud, Rol, tipoOperacionProcesamientoGuardado)
		Else
			DBServer.Instancia_Create(lSolicitud, _idFormulario, _idWorkFlow, _pedidoDirecto, Peticionario, Nothing, Nothing,
									oDs, mlID, bGuardar, m_dImporte, bPedidoAut, m_lEstado, sComentario, lNoconformidad, lCertificado, bPortal,
									sProveGS, lIdContrato, sStoredEnEjecucion, sIdioma, lIdArchivoContrato, sUsuNom, tipo, tipoSolicitud, Rol, tipoOperacionProcesamientoGuardado)
		End If

		'si no se ha producido ning�n error env�a el email a los Notificados
		Dim dDatos As New Dictionary(Of String, String)
		dDatos.Add("Instancia", mlID)
		dDatos.Add("From", String.Empty)
		Select Case tipoSolicitud
			Case TiposDeDatos.TipoDeSolicitud.Autofactura
				ThreadPool.QueueUserWorkItem(New WaitCallback(AddressOf NotificarCreateFactura), dDatos)
		End Select
	End Sub
	''' <summary>
	''' Funcion que salva una instancia creada anteriormente con los nuevos datos pasados
	''' </summary>
	''' <param name="sPer">Codigo de persona</param>
	''' <param name="oDs">DataSet con los datos de los campos</param>
	''' <param name="bEnviar">Variable booleana que indica si se debe enviar</param>
	''' <param name="dImporte">Importe del pedido</param>
	''' <param name="bPedidoAut">Variable booleana que indica si se trata de un pedido autom�tico</param>
	''' <param name="lCertificado">Id del certificado</param>
	''' <param name="lNoConformidad">Id de la no conformidad</param>
	''' <param name="bPortal">Variable booleana que indica si viene de portal</param>
	''' <param name="sproveGS">Codigo del proveedor de GS</param>
	''' <param name="sUsuNom">Nombre de usuario</param>
	''' <param name="bNotificar">Variable booleana que indica si se debe notificar</param>
	''' <param name="sEmail">Email del peticionario</param>
	''' <param name="sIdiomaProve">Idioma del proveeedor</param>
	''' <param name="sComentario">Comentario de la NC</param> 
	''' <param name="sStoredEnEjecucion">Stored donde falla</param>   
	''' <remarks>
	''' LLamada desde: PmWeb/guardarinstancia/GuardarconWorkFlow, PmWeb/alta/GuardarSolicitud, WebServicePM/service.asmx/Servicethread, 
	''' Tiempo m�ximo: 1,5 seg</remarks>
	Public Sub Save(ByVal oDs As DataSet, ByVal sPer As String, ByVal bEnviar As Boolean, ByRef tipoOperacionProcesamientoGuardado As Integer,
					Optional ByVal dImporte As Double = 0, Optional ByVal bPedidoAut As Boolean = False, Optional ByVal lCertificado As Long = Nothing,
					Optional ByVal lNoConformidad As Long = Nothing, Optional ByVal sEmail As String = Nothing, Optional ByVal bNotificar As Boolean = False,
					Optional ByVal bPortal As Boolean = False, Optional ByVal sProveGS As String = "", Optional ByVal sUsuNom As String = "",
					Optional ByVal sIdiomaProve As String = "", Optional ByVal sComentario As String = Nothing, Optional ByRef sStoredEnEjecucion As String = "",
					Optional ByVal sIdioma As String = "", Optional ByVal lIdContrato As Long = 0, Optional ByVal lIdArchivoContrato As Long = 0,
					Optional ByVal lIdFactura As Long = 0, Optional ByVal lSolicitudDePedido As Byte = 0, Optional ByVal bCertPdteEnviar As Boolean = False, Optional ByVal tipoSolicitud As Integer = 0)
		DBServer.Instancia_Save(ID, sPer, oDs, bEnviar, dImporte, bPedidoAut, lCertificado, lNoConformidad,
										 m_lEstado, bPortal, sProveGS, sUsuNom, sIdiomaProve, sComentario, sStoredEnEjecucion, sIdioma,
										 lIdContrato, lIdArchivoContrato, lIdFactura, lSolicitudDePedido, bCertPdteEnviar, tipoOperacionProcesamientoGuardado, tipoSolicitud)
	End Sub
	''' <summary>
	''' En una solicitud de pedido, una vez que se ha realizado la accion en el web service, se mira si la orden ha quedado como emitida al proveedor
	''' </summary>
	''' <returns>Devuelve el id de la orden si ha sido emitida, 0 si no ha sido emitida</returns>
	''' <remarks></remarks>
	Public Function ComprobarEstadoOrdenEntrega() As DataSet
		Authenticate()
		Return DBServer.Instancia_ComprobarEstadoOrdenEntrega(Me.ID)
	End Function
	''' <summary>En una no conformidad con flujo, una vez ha llegado a fin el flujo, se mira si la no conformidad ha quedado como emitida</summary>
	''' <returns>DataTable con los datos del estado de la instancia y de la no conformidad</returns>
	''' <remarks>Llamada desde: TratamientoInstancias</remarks>
	Public Function ComprobarEstadoNoConformidad() As DataTable
		Authenticate()
		Return DBServer.Instancia_ComprobarEstadoNoConformidad(Me.ID)
	End Function
	''' <summary>
	''' Procedimiento que elimina una instancia
	''' </summary>
	''' <param name="sUsu">Codigo de usuario que realiza la accion</param>
	''' <remarks>
	''' LLamada desde: PmWeb/Eliminarsolicitud/page_load
	''' Tiempo m�ximo: 0,5 seg</remarks>
	Public Sub Eliminar(Optional ByVal sUsu As String = Nothing)
		Authenticate()
		DBServer.EliminarInstancia(mlID, 0, sUsu)
	End Sub
	''' <summary>
	''' Procedimiento que elimina una instancia con el contrato relacionado
	''' </summary>
	''' <param name="lIdContrato">ID Contrato</param>
	''' <param name="sUsu">Codigo Usuario</param>
	''' <remarks>
	''' LLamada desde: PmWeb/Contratos/EliminarContrato/page_load
	''' Tiempo m�ximo: 0,5 seg</remarks>
	Public Sub EliminarContrato(ByVal lIdContrato As Long, ByVal sUsu As String)
		Authenticate()
		DBServer.EliminarInstancia(mlID, lIdContrato, sUsu)
	End Sub
	''' <summary>
	''' Procedimiento que elimina una instancia con la factura relacionada
	''' </summary>
	''' <param name="lIdFactura">ID Factura</param>
	''' <param name="sUsu">Codigo Usuario</param>
	''' <remarks>
	''' LLamada desde: App_Pages/Facturas/EliminarFactura/page_load
	''' Tiempo m�ximo: 0,5 seg</remarks>
	Public Sub EliminarFactura(ByVal lIdFactura As Long, ByVal sUsu As String)
		Authenticate()
		DBServer.EliminarInstancia(mlID, Nothing, sUsu, lIdFactura)
	End Sub
	''' <summary>
	''' Revisado por: Sandra. Fecha: 14/03/2011.
	''' Si un contrato est� finalizado, se puede iniciar un flujo de baja o de modificaci�n
	''' Si el flujo es de modificaci�n el estado es un 3, si es una baja, un 4.
	''' </summary>
	''' <param name="lIdContrato">Id del contrato</param>
	''' <param name="lEstado">Estado (3/4)</param>
	''' <remarks>LLamada desde: PMWeb/Contratos/IniciarWorkflow.aspx/Page_Load</remarks>
	Public Sub IniciarWorkflowContrato(ByVal lIdContrato As Long, ByVal lEstado As Long, ByVal lIdWorkflow As Long, ByVal sPer As String, ByVal Rol As Long)
		Authenticate()
		DBServer.Instancia_IniciarWorkflowContrato(mlID, lIdContrato, lEstado, lIdWorkflow, sPer, Rol)
	End Sub
	''' <summary>
	''' Traslada una instancia a un usuario interno o a un proveedor.
	''' </summary>
	''' <param name="sPer">Cod de la persona</param>
	''' <param name="sEMail">Email para la notificaci�n</param>
	''' <param name="sDestinatario">Destinatario</param>
	''' <param name="sComentario">Comentario</param>
	''' <param name="FechaLimite">Fecha l�mite de devoluci�n de la instancia</param>
	''' <param name="bProveedor">Si el traslado se ha hecho a un proveedor</param>
	''' <param name="lContacto">Contacto del proveedor</param>
	''' <param name="lBloque">Id del Bloque</param>
	''' <param name="sPerOrigen">Persona de Origen</param>
	''' <param name="sIdiomaDefecto">Idioma por defecto</param>
	''' <remarks>Llamada desde: WebServicePM/service.asmx/ServiceThread; Tiempo m�ximo: 0,5 sg</remarks>
	Public Sub Trasladar(ByVal sPer As String, ByVal sEMail As String, ByVal sDestinatario As String, Optional ByVal sComentario As String = Nothing,
						Optional ByVal FechaLimite As Object = Nothing, Optional ByVal bProveedor As Boolean = False, Optional ByVal lContacto As Long = Nothing,
						Optional ByVal lBloque As Integer = 0, Optional ByVal sPerOrigen As String = "",
						Optional ByVal iTipoSolicitud As TiposDeDatos.TipoDeSolicitud = TiposDeDatos.TipoDeSolicitud.SolicitudDeCompras,
						Optional ByVal sIdiomaDefecto As String = Nothing)
		Authenticate()

		Dim dDatos As New Dictionary(Of String, String)
		DBServer.Instancia_TrasladarInstancia(mlID, sPer, sDestinatario, sComentario, FechaLimite, bProveedor, lBloque, lContacto, sPerOrigen)

		dDatos.Add("Email", sEMail)
		dDatos.Add("Contacto", lContacto)
		dDatos.Add("Bloque", lBloque)
		dDatos.Add("TipoSolicitud", iTipoSolicitud)
		dDatos.Add("IdiomaDefecto", sIdiomaDefecto)

		ThreadPool.QueueUserWorkItem(New WaitCallback(AddressOf NotificarTrasladar), dDatos)
	End Sub
	''' <summary>
	''' Envia una notificaci�n indicando que se ha realizado un traslado sobre la solicitud
	''' </summary>
	''' <param name="obj">Objeto que contiene todos los datos necesarios para hacer la notificaci�n.</param>
	''' <remarks>Llamada desde: Trasladar; Tiempo m�ximo: 2 sg.</remarks>
	Private Sub NotificarTrasladar(ByVal obj As Object)
		Dim oNotificador As New Notificar(DBServer, mIsAuthenticated)
		oNotificador.NotificacionTrasladoSolicitud(mlID, obj.Item("Email"), CLng(obj.Item("Contacto")), CLng(obj.Item("Bloque")), obj.Item("TipoSolicitud"), obj.Item("IdiomaDefecto"))
		oNotificador.FuerzaFinalizeSmtpClient()
		oNotificador = Nothing
	End Sub
	''' <summary>
	''' Se le llama cuando se devuelve una instancia que viene de un traslado. Devuelve la instancia a qui�n la traslad� y
	''' le env�a una notificaci�n.
	''' </summary>
	''' <param name="sPer">Cod de la persona (si se traslad� a un usuario)</param>
	''' <param name="sEMail">Email</param>
	''' <param name="sComentario">Comentario de la confirmaci�n del traslado</param>
	''' <param name="lBloque">Id del bloque</param>
	''' <param name="sProveedor">Cod del proveedor (si se traslad� a un proveedor)</param>
	''' <param name="bPortal">Si hay q mandar mail desde el servidor de portal (true) o de gs (false)</param>
	''' <param name="sIdiomaDefecto">Idioma por defecto</param>
	''' <remarks>Llamada desde: WebServicePM/service.asmx --> ServiceThread; Tiempo maximo: 2 sg</remarks>
	Public Sub Devolucion(ByVal sPer As String, ByVal sFrom As String, Optional ByVal sComentario As String = Nothing, Optional ByVal lBloque As Integer = 0,
						  Optional ByVal sProveedor As String = Nothing, Optional ByVal sCodCia As String = Nothing, Optional ByVal sDenCia As String = Nothing,
						  Optional ByVal sNIFCia As String = Nothing, Optional ByVal sNombre As String = Nothing, Optional ByVal sApellidos As String = Nothing,
						  Optional ByVal sTelefono As String = Nothing, Optional ByVal sEmail As String = Nothing, Optional ByVal sFax As String = Nothing,
						  Optional ByVal iTipoSolicitud As TiposDeDatos.TipoDeSolicitud = TiposDeDatos.TipoDeSolicitud.SolicitudDeCompras,
						  Optional ByVal bPortal As Boolean = False, Optional ByVal sIdiomaDefecto As String = Nothing)
		Authenticate()

		Dim dDatos As New Dictionary(Of String, String)
		DBServer.Instancia_DevolverInstancia(mlID, sPer, sComentario, lBloque, sProveedor)

		dDatos.Add("From", sFrom)
		dDatos.Add("Bloque", lBloque)
		dDatos.Add("Proveedor", sProveedor)
		dDatos.Add("CodCia", sCodCia)
		dDatos.Add("DenCia", sDenCia)
		dDatos.Add("NIFCia", sNIFCia)
		dDatos.Add("Nombre", sNombre)
		dDatos.Add("Apellidos", sApellidos)
		dDatos.Add("Telefono", sTelefono)
		dDatos.Add("Email", sEmail)
		dDatos.Add("Fax", sFax)
		dDatos.Add("TipoSolicitud", iTipoSolicitud)
		dDatos.Add("Portal", bPortal)
		dDatos.Add("IdiomaDefecto", sIdiomaDefecto)
		ThreadPool.QueueUserWorkItem(New WaitCallback(AddressOf NotificarDevolucion), dDatos)
	End Sub
	''' <summary>
	''' Envia una notificaci�n indicando que se le ha devuelto la solicitud
	''' </summary>
	''' <param name="obj">Objeto que contiene todos los datos necesarios para hacer la notificaci�n.</param>
	''' <remarks>Llamada desde: Devolucion; Tiempo m�ximo: 2 sg.</remarks>
	Private Sub NotificarDevolucion(ByVal obj As Object)
		Dim oNotificador As New Notificar(DBServer, mIsAuthenticated, , CBool(obj.Item("Portal")))
		oNotificador.NotificacionDevolucionSolicitud(mlID, obj.Item("From"), CLng(obj.Item("Bloque")), IIf(obj.Item("Proveedor") <> Nothing, True, False), obj.Item("CodCia"), obj.Item("DenCia"), obj.Item("NIFCia"), obj.Item("Nombre"), obj.Item("Apellidos"), obj.Item("Telefono"), obj.Item("Email"), obj.Item("Fax"), obj.Item("TipoSolicitud"), obj.Item("IdiomaDefecto"))
		oNotificador.FuerzaFinalizeSmtpClient()
		oNotificador = Nothing
	End Sub
	''' <summary>
	''' Procedimiento que devuelve los procesos relacionados con una instancia
	''' </summary>
	''' <remarks>
	''' Llamada desde: PmWeb/guardarInstancia/ComprobarCondiciones, PmWeb/campos/ComprobarCondiciones, PmWeb/desglose/ComprobarCondiciones, PmWeb/comentariossolic/PagE_load, PmWeb/historicoestados/PagE_load, PmWeb/NwComentariosSolic/PagE_load, PmWeb/NWHistoricoEstados/PagE_load
	''' Tiempo m�ximo: 1 seg</remarks>
	Public Sub DevolverProcesosRelacionados()
		Authenticate()
		moProcesos = DBServer.Instancia_CargarProcesos(mlID)
	End Sub
	''' Revisado por: blp. Fecha: 17/07/2013
	''' <summary>
	''' Procedimiento que devuelve los pedidos relacionados de una instancia
	''' </summary>
	''' <param name="iTipo">Tipo de pedido (0:GS, 1:EP, 2:Integraci�n). Este par�metro no tiene prioridad ni sobre bAbono ni sobre bLibre</param>
	''' <param name="bLibre">True: queremos cargar los pedidos libres. Si se pasa este par�metro, se ignorar�n tanto bAbono como iTipo</param>
	''' <param name="bAbono">True: Queremos cargar los pedidos de abono. Si se pasa este par�metro, se ignorar� iTipo pero no bLibre, que tiene prioridad</param>
	''' <param name="bOblCodPedDir">Configuraci�n general: Existe campo para la codificaci�n personalizada de pedidos directos</param> 
	''' <remarks>
	''' LLamada desde: PmWeb/comentariosolic/Page_load, PmWeb/historicoestados/Page_load, PmWeb/NWcomentariosolic/Page_load, PmWeb/NWHistoricoEstados/Page_load, 
	''' Tiempo m�ximo: 1 seg</remarks>
	Public Sub DevolverPedidosRelacionados(Optional ByVal iTipo As Integer = Nothing, Optional ByVal bLibre As Boolean = False, Optional ByVal bAbono As Boolean = False, Optional ByVal bOblCodPedDir As Boolean = False)
		Authenticate()
		moPedidos = DBServer.Instancia_CargarPedidos(mlID, iTipo, bLibre, bAbono, bOblCodPedDir)
	End Sub
	''' <summary>
	''' Procedimiento que devuelve las lineas de catalogo relacionadas con una instancia
	''' </summary>
	''' <remarks>
	''' LLamada desde: PmWeb/comentariosolic/Page_load, PmWeb/historicoestados,Page_load, PmWeb/NWcomentariosolic/Page_load, PmWeb/NWHistoricoEstados/Page_load
	''' tiempo m�ximo:0,75 seg</remarks>
	Public Sub DevolverLineasCatRelacionadas()
		Authenticate()
		moPedidos = DBServer.Instancia_CargarLineasCatalogo(mlID)
	End Sub
	''' <summary>
	''' Procedimiento que carga el comentario de estado de una instancia
	''' </summary>
	''' <param name="ID">Identificador de la instancia</param>
	''' <returns>Un string que se corresponde con el comentario de estado de una instancia</returns>
	''' <remarks>
	''' Llamada desde: PmWeb/comentariosolic/Page_load, PmWeb/comentestado/Page_load, 
	''' Tiempo m�ximo: 0,3 seg</remarks>
	Public Function CargarComentarioEstado(ByVal ID As Long) As String
		Authenticate()
		CargarComentarioEstado = DBServer.Instancia_DevolverComentarioEstado(ID)
	End Function
	''' <summary>
	''' Devuelve el comentario del �ltimo cambio de etapa.
	''' </summary>
	''' <param name="ID">Instancia</param>
	''' <returns>Ultimo comentario</returns>
	''' <remarks>Llamada desde: VisorSolicitudes.aspx; Tiempo M�ximo: 0 sg.</remarks>
	Public Function DevolverUltimoComentario(ByVal ID As Long) As String
		Authenticate()
		DevolverUltimoComentario = DBServer.Instancia_DevolverUltimoComentario(ID)
	End Function
	''' <summary>
	''' Devuelve la denominaci�n de la etapa en curso de la solicitud
	''' </summary>
	''' <param name="sIdioma">Codigo del idioma del usuario</param>
	''' <returns>Un entero que devolvera el estado de la aprobacion</returns>
	''' <remarks>
	''' Llamada desde: Ninguna
	''' tiempo m�ximo: 0,4 seg</remarks>
	Public Function CargarEstapaActual(ByVal sIdioma As String, Optional ByVal bLitEstado As Boolean = False) As String
		Authenticate()
		CargarEstapaActual = DBServer.Instancia_CargarEstapaActual(mlID, sIdioma, bLitEstado)
	End Function
	''' <summary>
	''' Procedimiento que devuelve los campos calculadosde una instancia
	''' </summary>
	''' <param name="sUsuario">Codigo de usuario</param>
	''' <param name="sProve">Codigo de proveedor</param>
	''' <param name="bWorkflow">Si viene o no de QA</param>
	''' <returns>Un dataset con los campos calculados de una instancia</returns>
	''' <remarks>Llamada desde: PmWeb/guardarinstancia/GenerarDataSetYHacercomprobaciones - GuardarConWorkflow 6 GuardarSinWorkFlow,
	''' PmWeb/alta/GuardarSolicitud, PmWeb/ayudacampo/Page_load, PmWeb/recalcularimportes/Page_load,
	''' Tiempo m�ximo: 1 seg</remarks>
	Public Function LoadCamposCalculados(Optional ByVal sUsuario As String = Nothing, Optional ByVal sProve As String = Nothing, Optional ByVal bWorkflow As Boolean = True) As DataSet
		Authenticate()
		Dim data As DataSet
		data = DBServer.Instancia_LoadCamposCalculados(mlID, m_lVersion, Nothing, sUsuario, sProve, bWorkflow)
		Return data
	End Function
	''' <summary>
	''' `Procedimiento que carga el desglose visible del proceso padre de la instacia
	''' </summary>
	''' <param name="lIdCampo">Id del campo</param>
	''' <param name="sUsuario">Codigo de usuario</param>
	''' <param name="sProve">Codigo de proveedor</param>
	''' <returns>Un DataSet con los Datos del padre del proceso</returns>
	''' <remarks>
	''' Llamada desde: PmWeb/guardarInstancia/GenerarDataSetyHacerComprobaciones, PmWeb/RecalcularImportes/Page_Load, 
	''' Tiempo m�ximo: 1 seg</remarks>
	Public Function LoadDesglosePadreVisible(ByVal lIdCampo As Integer, Optional ByVal sUsuario As String = Nothing, Optional ByVal sProve As String = Nothing) As DataSet
		Authenticate()
		moData = DBServer.Instancia_LoadDesglosePadreVisible(mlID, lIdCampo, m_lVersion, Nothing, sUsuario, sProve)
		Return moData
	End Function
	''' <summary>
	''' Se llama a esta funci�n para completar los datos cargados en la solicitud con         
	'''                todos aquellos campos que bien por estar ocultos o ser de solo lectura no se    
	'''                han podido recoger del formulario.
	''' </summary>
	''' <param name="ds"> dataset con los campos recogidos de la solicitud (escritura)</param>
	''' <param name="sPer">c�digo de la persona </param>        
	''' <param name="bNuevoWorkflow">true/false</param>
	''' <param name="bMaper">true/false. Si hay integraci�n y se est� cargando el dataset para pasarselo al mapper.</param>   
	''' <param name="sIdioma">idioma del usuario</param>
	''' <param name="bUsarCampoDef">Si se usa o no el campo COPIA_CAMPO_DEF.ID y la tabla temporal TEMPADJUN. Maper no usa, resto usan</param> 
	''' <returns>ds: dataset completado con los campos no visibles y de solo lectura </returns>
	''' <remarks>Llamada desde:guardarInstancia.aspx --> GuardarSinWorkflow, GuardarConWorkflow, GenerarDataSetyHacerComprobaciones; Tiempo m�ximo:0 sg</remarks>
	Public Function CompletarFormulario(ByVal ds As DataSet, ByVal sPer As String, Optional ByVal bNuevoWorkflow As Boolean = False, Optional ByVal bMaper As Boolean = False,
										Optional ByVal sIdioma As String = "", Optional ByVal bUsarCampoDef As Boolean = False) As DataSet
		Dim idCampo As Long
		Dim dRow, dNewRow, dRows(), dRowsAdjun() As DataRow
		Dim bPadreNoVisibleHijoVa As Boolean = False
		Dim dsInvisibles As DataSet
		Dim dRowsCcd() As DataRow = Nothing

		If ds.Tables("TEMP").Rows.Count < 1 Then
			Return ds
		Else
			idCampo = ds.Tables("TEMP").Rows(0).Item("CAMPO")
		End If

		dsInvisibles = DBServer.Instancia_LoadInvisibles(idCampo, sPer, bNuevoWorkflow, sIdioma)

		'METEMOS LOS CAMPOS SIMPLES QUE ESTUVIERAN OCULTOS JUNTO CON LOS VALORES
		For Each dRow In dsInvisibles.Tables(0).Rows
			If bUsarCampoDef AndAlso ds.Tables("TEMP").Select("CAMPO=" & dRow.Item("ID") & " AND CAMPO_DEF=-1000").Length = 1 Then
				'lo ha metido el jsalta aun siendo oculto.
				dNewRow = ds.Tables("TEMP").Select("CAMPO=" & dRow.Item("ID"))(0)
				dNewRow.Item("CAMPO_DEF") = dRow.Item("COPIA_CAMPO_DEF")
			Else
				If ds.Tables("TEMP").Select("CAMPO=" & dRow.Item("ID")).Length = 0 Then
					dNewRow = ds.Tables("TEMP").NewRow
					dNewRow.Item("CAMPO") = dRow.Item("ID")
					dNewRow.Item("VALOR_TEXT") = dRow.Item("VALOR_TEXT")
					dNewRow.Item("VALOR_NUM") = dRow.Item("VALOR_NUM")
					If Not bMaper Then dNewRow.Item("CAMBIO") = dRow.Item("CAMBIO")
					Try
						If dRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoFecha Then
							dNewRow.Item("VALOR_FEC") = DevolverFechaRelativaA(DBNullToSomething(dRow.Item("VALOR_NUM")), DBNullToSomething(dRow.Item("VALOR_FEC")))
						End If
					Catch ex As Exception
					End Try
					dNewRow.Item("VALOR_BOOL") = dRow.Item("VALOR_BOOL")
					dNewRow.Item("ES_SUBCAMPO") = 0
					dNewRow.Item("SUBTIPO") = dRow.Item("SUBTIPO")
					dNewRow.Item("TIPOGS") = dRow.Item("TIPO_CAMPO_GS")
					If bUsarCampoDef Then dNewRow.Item("CAMPO_DEF") = dRow.Item("COPIA_CAMPO_DEF")
					If bMaper Then
						dNewRow.Item("GRUPO") = dRow.Item("GRUPO")
						dNewRow.Item("IDATRIB") = dRow.Item("ID_ATRIB_GS")
						dNewRow.Item("VALERP") = dRow.Item("VALIDACION_ERP")
					End If
					ds.Tables("TEMP").Rows.Add(dNewRow)
				End If
			End If
		Next
		'METEMOS LOS CAMPOS DE DESGLOSE QUE ESTUVIERAN OCULTOS
		For Each dRow In dsInvisibles.Tables(1).Rows
			If ds.Tables("TEMP").Select("CAMPO=" & dRow.Item("CAMPO_HIJO")).Length = 0 Then
				dNewRow = ds.Tables("TEMP").NewRow
				dNewRow.Item("CAMPO") = dRow.Item("CAMPO_HIJO")
				dNewRow.Item("ES_SUBCAMPO") = 1
				If bUsarCampoDef Then dNewRow.Item("CAMPO_DEF") = dRow.Item("COPIA_CAMPO_DEF")
				ds.Tables("TEMP").Rows.Add(dNewRow)
			ElseIf bUsarCampoDef Then
				'No popup, los invisibles pero relacionados (ej: material-articulo) si q estaran en TEMP pero con el CAMPO_DEF a -1000
				If ds.Tables("TEMP").Select("CAMPO=" & dRow.Item("CAMPO_HIJO") & " AND CAMPO_DEF=-1000").Length = 1 Then
					dNewRow = ds.Tables("TEMP").Select("CAMPO=" & dRow.Item("CAMPO_HIJO"))(0)
					dNewRow.Item("CAMPO_DEF") = dRow.Item("COPIA_CAMPO_DEF")

					dRowsCcd = ds.Tables("TEMPDESGLOSE").Select("CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND CAMPO_HIJO=" + dRow.Item("CAMPO_HIJO").ToString)
					For Each Row As DataRow In dRowsCcd
						Row.Item("CAMPO_HIJO_DEF") = dRow.Item("COPIA_CAMPO_DEF")
					Next
				End If
			End If
		Next

		For Each dRow In dsInvisibles.Tables(4).Rows
			If ds.Tables("TEMP").Select("CAMPO=" & dRow.Item("CAMPO_HIJO")).Length = 0 Then
				dNewRow = ds.Tables("TEMP").NewRow
				dNewRow.Item("CAMPO") = dRow.Item("CAMPO_HIJO")
				dNewRow.Item("ES_SUBCAMPO") = 1
				If bUsarCampoDef Then dNewRow.Item("CAMPO_DEF") = dRow.Item("COPIA_CAMPO_DEF")
				ds.Tables("TEMP").Rows.Add(dNewRow)

				dNewRow = ds.Tables("TEMPDESGLOSE").NewRow
				dNewRow.Item("LINEA") = 0
				dNewRow.Item("CAMPO_PADRE") = dRow.Item("CAMPO_PADRE")
				dNewRow.Item("CAMPO_HIJO") = dRow.Item("CAMPO_HIJO")

				dRowsCcd = ds.Tables("TEMP").Select("CAMPO=" & dRow.Item("CAMPO_PADRE"))
				If bUsarCampoDef Then
					dNewRow.Item("CAMPO_PADRE_DEF") = dRowsCcd(0).Item("CAMPO_DEF")
					dNewRow.Item("CAMPO_HIJO_DEF") = dRow.Item("COPIA_CAMPO_DEF")
				End If
				Try
					ds.Tables("TEMPDESGLOSE").Rows.Add(dNewRow)
				Catch ex As Exception
				End Try
			ElseIf bUsarCampoDef Then
				'No popup, los invisibles pero relacionados (ej: material-articulo) si q estaran en TEMP pero con el CAMPO_DEF a -1000
				If ds.Tables("TEMP").Select("CAMPO=" & dRow.Item("CAMPO_HIJO") & " AND CAMPO_DEF=-1000").Length = 1 Then
					dNewRow = ds.Tables("TEMP").Select("CAMPO=" & dRow.Item("CAMPO_HIJO"))(0)
					dNewRow.Item("CAMPO_DEF") = dRow.Item("COPIA_CAMPO_DEF")

					dRowsCcd = ds.Tables("TEMPDESGLOSE").Select("CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND CAMPO_HIJO=" + dRow.Item("CAMPO_HIJO").ToString)
					For Each Row As DataRow In dRowsCcd
						Row.Item("CAMPO_HIJO_DEF") = dRow.Item("COPIA_CAMPO_DEF")
					Next
				End If
			End If

		Next

		If bUsarCampoDef Then
			'METEMOS LOS adjuntos DE CAMPOS QUE ESTUVIERAN OCULTOS
			For Each dRow In dsInvisibles.Tables(5).Rows
				If ds.Tables("TEMPADJUN").Select("CAMPO=" + dRow.Item("CAMPO").ToString + " AND ID=" + dRow.Item("ID").ToString).Length = 0 Then
					dNewRow = ds.Tables("TEMPADJUN").NewRow
					dNewRow.Item("CAMPO") = dRow.Item("CAMPO")
					dNewRow.Item("ID") = dRow.Item("ID")
					dNewRow.Item("TIPO") = 1
					If bUsarCampoDef Then dNewRow.Item("CAMPO_DEF") = dRow.Item("COPIA_CAMPO_DEF")
					Try
						ds.Tables("TEMPADJUN").Rows.Add(dNewRow)
					Catch ex As Exception
					End Try
				End If
			Next
		End If

		Dim iLinea As Integer
		For Each dRow In dsInvisibles.Tables(1).Rows
			If dRow.Item("VISIBLE") = 0 OrElse dRow.Item("ESCRITURA") = 0 Then 'en este datatable vienen los visibles y los invisibles. Solo se deben a�adir los datos de los invisibles. sra: tb se hay que a�adir los datos de los de solo lectura.
				' Este IF comprueba que este campo invisible pertenece a un desglose con alg�n campo visible
				If UBound(dsInvisibles.Tables(1).Select("CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND VISIBLE = 1")) > 0 Then

					dRows = ds.Tables("TEMPDESGLOSE").Select("CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND LINEA_OLD = " + dRow.Item("LINEA").ToString)
					If dRows.Length > 0 Then 'si fuera igual a 0 indicar�a que se ha borrado esa l�nea y por tanto no hay que guardarlo
						iLinea = dRows(0).Item("LINEA")
						'comprobamos que no exista ya que los desgloses de popup si que se cargan aunque est� invisible
						If ds.Tables("TEMPDESGLOSE").Select("CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND CAMPO_HIJO=" + dRow.Item("CAMPO_HIJO").ToString + " AND LINEA_OLD = " + dRow.Item("LINEA").ToString).Length = 0 Then
							Dim cont = 0
							If DBNullToSomething(dRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Material OrElse DBNullToSomething(dRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Unidad Then
								''En el caso de que sea el campo sea la estructura de MATERIAL o la UNIDAD hay que comprobar que si el articulo tambi�n est� oculto que cargue el material o la unidad.
								Dim sql As String
								sql = "CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND LINEA = " + iLinea.ToString + " AND VISIBLE=1 AND ESCRITURA=1 AND (TIPO_CAMPO_GS =  " & TiposDeDatos.TipoCampoGS.CodArticulo & " OR TIPO_CAMPO_GS =  " & TiposDeDatos.TipoCampoGS.NuevoCodArticulo & " OR TIPO_CAMPO_GS =  " & TiposDeDatos.TipoCampoGS.DenArticulo & ")"
								cont = dsInvisibles.Tables(1).Select(sql).Length
							End If

							If (DBNullToSomething(dRow.Item("TIPO_CAMPO_GS")) <> TiposDeDatos.TipoCampoGS.Material AndAlso DBNullToSomething(dRow.Item("TIPO_CAMPO_GS")) <> TiposDeDatos.TipoCampoGS.Unidad) _
							 OrElse (DBNullToSomething(dRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Material And cont = 0) _
							 OrElse (DBNullToSomething(dRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Unidad And cont = 0) Then
								dNewRow = ds.Tables("TEMPDESGLOSE").NewRow
								dNewRow.Item("LINEA") = iLinea
								dNewRow.Item("CAMPO_PADRE") = dRow.Item("CAMPO_PADRE")
								dNewRow.Item("CAMPO_HIJO") = dRow.Item("CAMPO_HIJO")
								dNewRow.Item("VALOR_TEXT") = dRow.Item("VALOR_TEXT")
								If (IsDBNull(dRow.Item("VALOR_TEXT"))) And (Not IsDBNull(dRow.Item("LISTA_TEXT"))) Then
									dNewRow.Item("VALOR_TEXT") = dRow.Item("LISTA_TEXT")
								End If
								dNewRow.Item("VALOR_NUM") = dRow.Item("VALOR_NUM")
								Try
									If dRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoFecha Then
										dNewRow.Item("VALOR_FEC") = DevolverFechaRelativaA(DBNullToSomething(dRow.Item("VALOR_NUM")), DBNullToSomething(dRow.Item("VALOR_FEC")))
									End If
								Catch ex As Exception

								End Try
								dNewRow.Item("VALOR_BOOL") = dRow.Item("VALOR_BOOL")
								dNewRow.Item("LINEA_OLD") = dRow.Item("LINEA")
								If Not bMaper Then dNewRow.Item("CAMBIO") = dRow.Item("CAMBIO")
								dNewRow.Item("TIPOGS") = dRow.Item("TIPO_CAMPO_GS")

								dRowsCcd = ds.Tables("TEMP").Select("CAMPO=" & dRow.Item("CAMPO_PADRE"))
								If bUsarCampoDef Then
									dNewRow.Item("CAMPO_PADRE_DEF") = dRowsCcd(0).Item("CAMPO_DEF")
									dNewRow.Item("CAMPO_HIJO_DEF") = dRow.Item("COPIA_CAMPO_DEF")
								End If

								If bMaper Then
									dNewRow.Item("VALERP") = dRow.Item("VALIDACION_ERP")
									dNewRow.Item("IDATRIB") = dRow.Item("ID_ATRIB_GS")
								End If
								Try
									'caso Presupuesto nivel 4: Campo Oculto que controla por otro lado el maper.
									'Intenta meter algo que ya he metido. Solo desglose popup.
									ds.Tables("TEMPDESGLOSE").Rows.Add(dNewRow)
								Catch ex As Exception
								End Try

								If Not (ds.Tables("TEMPDESGLOSEADJUN") Is Nothing) Then
									dRowsAdjun = dsInvisibles.Tables(2).Select("CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND CAMPO_HIJO=" + dRow.Item("CAMPO_HIJO").ToString + " AND LINEA = " + iLinea.ToString)
									For Each Adjun As DataRow In dRowsAdjun
										dNewRow = ds.Tables("TEMPDESGLOSEADJUN").NewRow

										dNewRow.Item("LINEA") = iLinea
										dNewRow.Item("CAMPO_PADRE") = dRow.Item("CAMPO_PADRE")
										dNewRow.Item("CAMPO_HIJO") = dRow.Item("CAMPO_HIJO")

										dRowsCcd = ds.Tables("TEMP").Select("CAMPO=" & dRow.Item("CAMPO_PADRE"))
										If bUsarCampoDef Then
											dNewRow.Item("CAMPO_PADRE_DEF") = dRowsCcd(0).Item("CAMPO_DEF")
											dNewRow.Item("CAMPO_HIJO_DEF") = Adjun.Item("COPIA_CAMPO_DEF")
										End If

										dNewRow.Item("ID") = Adjun.Item("ADJUN")
										dNewRow.Item("TIPO") = 1
										Try
											ds.Tables("TEMPDESGLOSEADJUN").Rows.Add(dNewRow)
										Catch ex As Exception
										End Try
									Next
								End If
							End If
						Else
							Dim sql, sql2, sql3 As String
							sql = "CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND LINEA = " + iLinea.ToString + " AND ((TIPO_CAMPO_GS= " & TiposDeDatos.TipoCampoGS.NuevoCodArticulo & " AND VISIBLE = 0)"
							sql = sql + " OR (TIPO_CAMPO_GS =  " & TiposDeDatos.TipoCampoGS.DenArticulo & " AND VISIBLE = 1))"
							sql2 = "CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND LINEA = " + iLinea.ToString + " AND ((TIPO_CAMPO_GS= " & TiposDeDatos.TipoCampoGS.NuevoCodArticulo & " OR TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.DenArticulo & "OR TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.CodArticulo & ") AND VISIBLE = 1)"
							sql3 = "CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND LINEA = " + iLinea.ToString + " AND ((TIPO_CAMPO_GS= " & TiposDeDatos.TipoCampoGS.NuevoCodArticulo & " AND VISIBLE = 1)"
							sql3 = sql3 + " OR (TIPO_CAMPO_GS =  " & TiposDeDatos.TipoCampoGS.DenArticulo & " AND VISIBLE = 1))"

							dNewRow = ds.Tables("TEMPDESGLOSE").Select("CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND CAMPO_HIJO=" + dRow.Item("CAMPO_HIJO").ToString + " AND LINEA_OLD = " + dRow.Item("LINEA").ToString)(0)

							If ((DBNullToSomething(dRow.Item("TIPO_CAMPO_GS")) <> TiposDeDatos.TipoCampoGS.NuevoCodArticulo) And (DBNullToSomething(dRow.Item("TIPO_CAMPO_GS")) <> TiposDeDatos.TipoCampoGS.Material)) _
								Or ((dsInvisibles.Tables(1).Select(sql).Length < 2) And (DBNullToSomething(dRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.NuevoCodArticulo)) _
								Or ((dsInvisibles.Tables(1).Select(sql3).Length = 0) And (DBNullToSomething(dRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Material)) _
								Or ((DBNullToSomething(dRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Material) _
									AndAlso (IsDBNull(dNewRow.Item("VALOR_TEXT"))) AndAlso (Not IsDBNull(dRow.Item("VALOR_TEXT")))) Then

								If IsDBNull(dNewRow.Item("VALOR_TEXT")) Then
									dNewRow.Item("VALOR_TEXT") = dRow.Item("VALOR_TEXT")
								ElseIf dNewRow.Item("VALOR_TEXT") = "nulo" Then
									dNewRow.Item("VALOR_TEXT") = System.DBNull.Value
								Else
									dNewRow.Item("VALOR_TEXT") = dNewRow.Item("VALOR_TEXT")
								End If
								dNewRow.Item("VALOR_NUM") = IIf(IsDBNull(dNewRow.Item("VALOR_NUM")), dRow.Item("VALOR_NUM"), dNewRow.Item("VALOR_NUM"))
								dNewRow.Item("VALOR_FEC") = IIf(IsDBNull(dNewRow.Item("VALOR_FEC")), dRow.Item("VALOR_FEC"), dNewRow.Item("VALOR_FEC"))
								dNewRow.Item("VALOR_BOOL") = IIf(IsDBNull(dNewRow.Item("VALOR_BOOL")), dRow.Item("VALOR_BOOL"), dNewRow.Item("VALOR_BOOL"))
								If Not bMaper Then dNewRow.Item("CAMBIO") = IIf(IsDBNull(dNewRow.Item("CAMBIO")), dRow.Item("CAMBIO"), dNewRow.Item("CAMBIO"))
								dNewRow.Item("TIPOGS") = dRow.Item("TIPO_CAMPO_GS")

								dRowsCcd = ds.Tables("TEMP").Select("CAMPO=" & dRow.Item("CAMPO_PADRE"))
								If bUsarCampoDef Then
									dNewRow.Item("CAMPO_PADRE_DEF") = dRowsCcd(0).Item("CAMPO_DEF")
									dNewRow.Item("CAMPO_HIJO_DEF") = dRow.Item("COPIA_CAMPO_DEF")
								End If

								If bMaper Then
									dNewRow.Item("IDATRIB") = dRow.Item("ID_ATRIB_GS")
									dNewRow.Item("VALERP") = dRow.Item("VALIDACION_ERP")
								End If
							End If

						End If

					End If
				Else ' al no haber campos visibles de este desglose habr� que a�adirlo tal y como viene
					dNewRow = ds.Tables("TEMPDESGLOSE").NewRow
					dNewRow.Item("LINEA") = dRow.Item("LINEA")
					dNewRow.Item("CAMPO_PADRE") = dRow.Item("CAMPO_PADRE")
					dNewRow.Item("CAMPO_HIJO") = dRow.Item("CAMPO_HIJO")
					dNewRow.Item("VALOR_TEXT") = dRow.Item("VALOR_TEXT")
					If (IsDBNull(dRow.Item("VALOR_TEXT"))) And (Not IsDBNull(dRow.Item("LISTA_TEXT"))) Then
						dNewRow.Item("VALOR_TEXT") = dRow.Item("LISTA_TEXT")
					End If
					dNewRow.Item("VALOR_NUM") = dRow.Item("VALOR_NUM")
					If dRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoFecha Then
						dNewRow.Item("VALOR_FEC") = DevolverFechaRelativaA(DBNullToSomething(dRow.Item("VALOR_NUM")), DBNullToSomething(dRow.Item("VALOR_FEC")))
					End If
					dNewRow.Item("VALOR_BOOL") = dRow.Item("VALOR_BOOL")
					If Not bMaper Then dNewRow.Item("CAMBIO") = dRow.Item("CAMBIO")
					dNewRow.Item("LINEA_OLD") = dRow.Item("LINEA")
					dNewRow.Item("TIPOGS") = dRow.Item("TIPO_CAMPO_GS")

					dRowsCcd = ds.Tables("TEMP").Select("CAMPO=" & dRow.Item("CAMPO_PADRE"))
					If bUsarCampoDef Then
						dNewRow.Item("CAMPO_PADRE_DEF") = dRowsCcd(0).Item("CAMPO_DEF")
						dNewRow.Item("CAMPO_HIJO_DEF") = dRow.Item("COPIA_CAMPO_DEF")
					End If

					If bMaper Then
						dNewRow.Item("VALERP") = dRow.Item("VALIDACION_ERP")
						dNewRow.Item("IDATRIB") = dRow.Item("ID_ATRIB_GS")
					End If
					Try
						'caso Presupuesto nivel 4: Campo Oculto que controla por otro lado el maper.
						'Intenta meter algo que ya he metido. Solo desglose popup.       
						ds.Tables("TEMPDESGLOSE").Rows.Add(dNewRow)
					Catch ex As Exception
					End Try

					If Not (ds.Tables("TEMPDESGLOSEADJUN") Is Nothing) Then
						dRowsAdjun = dsInvisibles.Tables(2).Select("CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND CAMPO_HIJO=" + dRow.Item("CAMPO_HIJO").ToString + " AND LINEA = " + dRow.Item("LINEA").ToString)
						For Each Adjun As DataRow In dRowsAdjun
							dNewRow = ds.Tables("TEMPDESGLOSEADJUN").NewRow

							dNewRow.Item("LINEA") = dRow.Item("LINEA")
							dNewRow.Item("CAMPO_PADRE") = dRow.Item("CAMPO_PADRE")
							dNewRow.Item("CAMPO_HIJO") = dRow.Item("CAMPO_HIJO")

							dRowsCcd = ds.Tables("TEMP").Select("CAMPO=" & dRow.Item("CAMPO_PADRE"))
							If bUsarCampoDef Then
								dNewRow.Item("CAMPO_PADRE_DEF") = dRowsCcd(0).Item("CAMPO_DEF")
								dNewRow.Item("CAMPO_HIJO_DEF") = Adjun.Item("COPIA_CAMPO_DEF")
							End If

							dNewRow.Item("ID") = Adjun.Item("ADJUN")
							dNewRow.Item("TIPO") = 1
							Try
								ds.Tables("TEMPDESGLOSEADJUN").Rows.Add(dNewRow)
							Catch ex As Exception
							End Try
						Next
					End If
				End If
			End If
		Next

		For Each dRow In dsInvisibles.Tables(1).Rows
			'Si estaba entre los invisibles  el PADRE, ed, el DESGLOSE que mas da PM_CONF_CUMP_BLOQUE
			'del hijo, seguro que no esta por GenerarDataSet y habra que meterlo
			dRows = dsInvisibles.Tables(0).Select("ID=" + dRow.Item("CAMPO_PADRE").ToString)
			bPadreNoVisibleHijoVa = (dRows.Length > 0)
			''
			If bPadreNoVisibleHijoVa Then
				dRows = ds.Tables("TEMPDESGLOSE").Select("CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND LINEA_OLD = " + dRow.Item("LINEA").ToString)
				If dRows.Length > 0 Then 'si fuera igual a 0 indicar�a que se ha borrado esa l�nea y por tanto no hay que guardarlo
					iLinea = dRows(0).Item("LINEA")
					'comprobamos que no exista ya que los desgloses de popup si que se cargan aunque est� invisible
					If ds.Tables("TEMPDESGLOSE").Select("CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND CAMPO_HIJO=" + dRow.Item("CAMPO_HIJO").ToString + " AND LINEA_OLD = " + dRow.Item("LINEA").ToString).Length = 0 Then

						dNewRow = ds.Tables("TEMPDESGLOSE").NewRow
						dNewRow.Item("LINEA") = iLinea
						dNewRow.Item("CAMPO_PADRE") = dRow.Item("CAMPO_PADRE")
						dNewRow.Item("CAMPO_HIJO") = dRow.Item("CAMPO_HIJO")
						dNewRow.Item("VALOR_TEXT") = dRow.Item("VALOR_TEXT")
						If (IsDBNull(dRow.Item("VALOR_TEXT"))) And (Not IsDBNull(dRow.Item("LISTA_TEXT"))) Then
							dNewRow.Item("VALOR_TEXT") = dRow.Item("LISTA_TEXT")
						End If
						Try
							dNewRow.Item("VALOR_NUM") = dRow.Item("VALOR_NUM")
							If dRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoFecha Then
								dNewRow.Item("VALOR_FEC") = DevolverFechaRelativaA(DBNullToSomething(dRow.Item("VALOR_NUM")), DBNullToSomething(dRow.Item("VALOR_FEC")))
							End If
							dNewRow.Item("VALOR_BOOL") = dRow.Item("VALOR_BOOL")
							If Not bMaper Then dNewRow.Item("CAMBIO") = dRow.Item("CAMBIO")
						Catch ex As Exception
						End Try
						dNewRow.Item("LINEA_OLD") = dRow.Item("LINEA")
						dNewRow.Item("TIPOGS") = dRow.Item("TIPO_CAMPO_GS")

						dRowsCcd = ds.Tables("TEMP").Select("CAMPO=" & dRow.Item("CAMPO_PADRE"))
						If bUsarCampoDef Then
							dNewRow.Item("CAMPO_PADRE_DEF") = dRowsCcd(0).Item("CAMPO_DEF")
							dNewRow.Item("CAMPO_HIJO_DEF") = dRow.Item("COPIA_CAMPO_DEF")
						End If

						If bMaper Then
							dNewRow.Item("VALERP") = dRow.Item("VALIDACION_ERP")
							dNewRow.Item("IDATRIB") = dRow.Item("ID_ATRIB_GS")
						End If
						Try
							'caso Presupuesto nivel 4: Campo Oculto que controla por otro lado el maper.
							'Intenta meter algo que ya he metido. Solo desglose popup.
							ds.Tables("TEMPDESGLOSE").Rows.Add(dNewRow)
						Catch ex As Exception
						End Try

						If Not (ds.Tables("TEMPDESGLOSEADJUN") Is Nothing) Then
							dRowsAdjun = dsInvisibles.Tables(2).Select("CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND CAMPO_HIJO=" + dRow.Item("CAMPO_HIJO").ToString + " AND LINEA = " + iLinea.ToString)
							For Each Adjun As DataRow In dRowsAdjun
								dNewRow = ds.Tables("TEMPDESGLOSEADJUN").NewRow

								dNewRow.Item("LINEA") = iLinea
								dNewRow.Item("CAMPO_PADRE") = dRow.Item("CAMPO_PADRE")
								dNewRow.Item("CAMPO_HIJO") = dRow.Item("CAMPO_HIJO")

								dRowsCcd = ds.Tables("TEMP").Select("CAMPO=" & dRow.Item("CAMPO_PADRE"))
								If bUsarCampoDef Then
									dNewRow.Item("CAMPO_PADRE_DEF") = dRowsCcd(0).Item("CAMPO_DEF")
									dNewRow.Item("CAMPO_HIJO_DEF") = Adjun.Item("COPIA_CAMPO_DEF")
								End If

								dNewRow.Item("ID") = Adjun.Item("ADJUN")
								dNewRow.Item("TIPO") = 1
								Try
									ds.Tables("TEMPDESGLOSEADJUN").Rows.Add(dNewRow)
								Catch ex As Exception
								End Try
							Next
						End If
					Else
						'Para que no se cargue CodArticulo =119 en el caso de que el NuevoCodArt este OCULTO y la DENOMINACION visible
						Dim sql, sql2 As String
						sql = "CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString
						sql = sql + " AND CAMPO_HIJO = " + dRow.Item("CAMPO_HIJO").ToString
						sql = sql + " AND LINEA = " + iLinea.ToString
						dRows = dsInvisibles.Tables(1).Select(sql)

						sql = "CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND LINEA = " + iLinea.ToString + " AND ((TIPO_CAMPO_GS= " & TiposDeDatos.TipoCampoGS.NuevoCodArticulo & " AND VISIBLE = 0)"
						sql = sql + " OR (TIPO_CAMPO_GS =  " & TiposDeDatos.TipoCampoGS.DenArticulo & " AND VISIBLE = 1))"

						sql2 = "CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND LINEA = " + iLinea.ToString + " AND ((TIPO_CAMPO_GS= " & TiposDeDatos.TipoCampoGS.NuevoCodArticulo & " OR TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.DenArticulo & "OR TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.CodArticulo & ") AND VISIBLE = 1)"

						If dRows.Length > 0 Then
							If ((DBNullToSomething(dRows(0).Item("TIPO_CAMPO_GS")) <> TiposDeDatos.TipoCampoGS.NuevoCodArticulo) And (DBNullToSomething(dRows(0).Item("TIPO_CAMPO_GS")) <> TiposDeDatos.TipoCampoGS.Centro)) _
							Or ((dsInvisibles.Tables(1).Select(sql).Length < 2) And (DBNullToSomething(dRows(0).Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.NuevoCodArticulo)) _
							Or ((DBNullToSomething(dRows(0).Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Centro) And (dsInvisibles.Tables(1).Select(sql2).Length = 0)) Then
								dNewRow = ds.Tables("TEMPDESGLOSE").Select("CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND CAMPO_HIJO=" + dRow.Item("CAMPO_HIJO").ToString + " AND LINEA_OLD = " + dRow.Item("LINEA").ToString)(0)

								'dNewRow.Item("VALOR_TEXT") tendr� el valor "nulo" cuando en el desglose se muestre solamente la denominacion el articulo no sea generico
								'y introducimos una nueva denominacion
								If IsDBNull(dNewRow.Item("VALOR_TEXT")) Then
									dNewRow.Item("VALOR_TEXT") = dRow.Item("VALOR_TEXT")
								ElseIf dNewRow.Item("VALOR_TEXT") = "nulo" Then
									dNewRow.Item("VALOR_TEXT") = System.DBNull.Value
								Else
									dNewRow.Item("VALOR_TEXT") = dNewRow.Item("VALOR_TEXT")
								End If
								If (IsDBNull(dNewRow.Item("VALOR_TEXT"))) And (Not IsDBNull(dRow.Item("LISTA_TEXT"))) Then
									dNewRow.Item("VALOR_TEXT") = dRow.Item("LISTA_TEXT")
								End If
								Try
									dNewRow.Item("VALOR_NUM") = IIf(IsDBNull(dNewRow.Item("VALOR_NUM")), dRow.Item("VALOR_NUM"), dNewRow.Item("VALOR_NUM"))
									If dRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoFecha Then
										dNewRow.Item("VALOR_FEC") = DevolverFechaRelativaA(DBNullToSomething(IIf(IsDBNull(dNewRow.Item("VALOR_NUM")), dRow.Item("VALOR_NUM"), dNewRow.Item("VALOR_NUM"))), DBNullToSomething(IIf(IsDBNull(dNewRow.Item("VALOR_FEC")), dRow.Item("VALOR_FEC"), dNewRow.Item("VALOR_FEC"))))
									End If
									If Not bMaper Then dNewRow.Item("CAMBIO") = IIf(IsDBNull(dNewRow.Item("CAMBIO")), dRow.Item("CAMBIO"), dNewRow.Item("CAMBIO"))
								Catch ex As Exception
								End Try
								Try
									dNewRow.Item("VALOR_FEC") = IIf(IsDBNull(dNewRow.Item("VALOR_FEC")), dRow.Item("VALOR_FEC"), dNewRow.Item("VALOR_FEC"))
									dNewRow.Item("VALOR_BOOL") = IIf(IsDBNull(dNewRow.Item("VALOR_BOOL")), dRow.Item("VALOR_BOOL"), dNewRow.Item("VALOR_BOOL"))
								Catch ex As Exception
								End Try

								dNewRow.Item("TIPOGS") = dRows(0).Item("TIPO_CAMPO_GS")

								dRowsCcd = ds.Tables("TEMP").Select("CAMPO=" & dRow.Item("CAMPO_PADRE"))
								If bUsarCampoDef Then
									dNewRow.Item("CAMPO_PADRE_DEF") = dRowsCcd(0).Item("CAMPO_DEF")
									dNewRow.Item("CAMPO_HIJO_DEF") = dRow.Item("COPIA_CAMPO_DEF")
								End If

								If bMaper Then
									dNewRow.Item("IDATRIB") = dRows(0).Item("ID_ATRIB_GS")
									dNewRow.Item("VALERP") = dRows(0).Item("VALIDACION_ERP")
								End If
							End If
						End If
					End If
				Else ' al no haber campos visibles de este desglose habr� que a�adirlo tal y como viene
					If bUsarCampoDef Then
						dRowsCcd = ds.Tables("TEMP").Select("CAMPO=" & dRow.Item("CAMPO_PADRE"))

						If dRowsCcd.Length = 0 Then
							dRowsCcd = dsInvisibles.Tables(6).Select("CAMPO_PADRE=" & dRow.Item("CAMPO_PADRE"))

							dNewRow = ds.Tables("TEMP").NewRow
							dNewRow.Item("CAMPO") = dRowsCcd(0)("CAMPO_PADRE")
							dNewRow.Item("ES_SUBCAMPO") = 0
							dNewRow.Item("SUBTIPO") = dRowsCcd(0)("SUBTIPO_PADRE")
							dNewRow.Item("TIPOGS") = dRowsCcd(0)("TIPO_CAMPO_GS_PADRE")
							If bUsarCampoDef Then dNewRow.Item("CAMPO_DEF") = dRowsCcd(0)("COPIA_CAMPO_DEF_PADRE")
							If bMaper Then dNewRow.Item("GRUPO") = dRow.Item("GRUPO")
							Try
								ds.Tables("TEMP").Rows.Add(dNewRow)
							Catch ex As Exception
							End Try
							dRowsCcd = ds.Tables("TEMP").Select("CAMPO=" & dRow.Item("CAMPO_PADRE"))
						End If
					End If

					dNewRow = ds.Tables("TEMPDESGLOSE").NewRow
					dNewRow.Item("LINEA") = dRow.Item("LINEA")
					dNewRow.Item("CAMPO_PADRE") = dRow.Item("CAMPO_PADRE")
					dNewRow.Item("CAMPO_HIJO") = dRow.Item("CAMPO_HIJO")
					dNewRow.Item("VALOR_TEXT") = dRow.Item("VALOR_TEXT")
					If (IsDBNull(dRow.Item("VALOR_TEXT"))) And (Not IsDBNull(dRow.Item("LISTA_TEXT"))) Then
						dNewRow.Item("VALOR_TEXT") = dRow.Item("LISTA_TEXT")
					End If

					Try
						dNewRow.Item("VALOR_NUM") = dRow.Item("VALOR_NUM")
						If dRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoFecha Then
							dNewRow.Item("VALOR_FEC") = DevolverFechaRelativaA(DBNullToSomething(dRow.Item("VALOR_NUM")), DBNullToSomething(dRow.Item("VALOR_FEC")))
						End If
						dNewRow.Item("VALOR_BOOL") = dRow.Item("VALOR_BOOL")
						If Not bMaper Then dNewRow.Item("CAMBIO") = dRow.Item("CAMBIO")
					Catch ex As Exception
					End Try

					dNewRow.Item("LINEA_OLD") = dRow.Item("LINEA")
					dNewRow.Item("TIPOGS") = dRow.Item("TIPO_CAMPO_GS")

					If bUsarCampoDef Then
						dNewRow.Item("CAMPO_PADRE_DEF") = dRowsCcd(0).Item("CAMPO_DEF")
						dNewRow.Item("CAMPO_HIJO_DEF") = dRow.Item("COPIA_CAMPO_DEF")
					End If

					If bMaper Then
						dNewRow.Item("VALERP") = dRow.Item("VALIDACION_ERP")
						dNewRow.Item("IDATRIB") = dRow.Item("ID_ATRIB_GS")
					End If
					Try
						'caso Presupuesto nivel 4: Campo Oculto que controla por otro lado el maper.
						'Intenta meter algo que ya he metido. Solo desglose popup.       
						ds.Tables("TEMPDESGLOSE").Rows.Add(dNewRow)
					Catch ex As Exception
					End Try

					If Not (ds.Tables("TEMPDESGLOSEADJUN") Is Nothing) Then
						dRowsAdjun = dsInvisibles.Tables(2).Select("CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND CAMPO_HIJO=" + dRow.Item("CAMPO_HIJO").ToString + " AND LINEA = " + dRow.Item("LINEA").ToString)
						For Each Adjun As DataRow In dRowsAdjun
							dNewRow = ds.Tables("TEMPDESGLOSEADJUN").NewRow

							dNewRow.Item("LINEA") = dRow.Item("LINEA")
							dNewRow.Item("CAMPO_PADRE") = dRow.Item("CAMPO_PADRE")
							dNewRow.Item("CAMPO_HIJO") = dRow.Item("CAMPO_HIJO")

							dRowsCcd = ds.Tables("TEMP").Select("CAMPO=" & dRow.Item("CAMPO_PADRE"))
							If bUsarCampoDef Then
								dNewRow.Item("CAMPO_PADRE_DEF") = dRowsCcd(0).Item("CAMPO_DEF")
								dNewRow.Item("CAMPO_HIJO_DEF") = Adjun.Item("COPIA_CAMPO_DEF")
							End If

							dNewRow.Item("ID") = Adjun.Item("ADJUN")
							dNewRow.Item("TIPO") = 1
							Try
								ds.Tables("TEMPDESGLOSEADJUN").Rows.Add(dNewRow)
							Catch ex As Exception
							End Try
						Next
					End If
				End If
			End If
		Next

		Dim dDefaultRow As DataRow
		Dim lMaxLine As Long = 0
		Dim auxRow As DataRow
		Dim auxDataTable As New DataTable("GRUPOS")
		auxDataTable.Columns.Add("CAMPO_PADRE", System.Type.GetType("System.Int32"))
		auxDataTable.Columns.Add("LINEAS", System.Type.GetType("System.Int32"))

		dRows = ds.Tables("TEMPDESGLOSE").Select("LINEA_OLD IS NULL")

		For Each dRow In dRows
			If auxDataTable.Select("CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString).Length > 0 Then
				auxRow = auxDataTable.Select("CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString)(0)
			Else
				auxRow = auxDataTable.NewRow
				auxRow.Item("CAMPO_PADRE") = dRow.Item("CAMPO_PADRE")
				auxRow.Item("LINEAS") = 0
			End If

			If DBNullToSomething(auxRow.Item("LINEAS")) < DBNullToSomething(dRow.Item("LINEA")) Then
				auxRow.Item("LINEAS") = dRow.Item("LINEA")
			End If

			If auxRow.RowState = DataRowState.Detached Then
				auxDataTable.Rows.Add(auxRow)
			End If
		Next

		For Each auxRow In auxDataTable.Rows
			For i As Long = 1 To auxRow.Item("LINEAS")
				If ds.Tables("TEMPDESGLOSE").Select("LINEA=" + i.ToString()).Length > 0 Then
					If dsInvisibles.Tables(1).Select("LINEA=" + i.ToString()).Length > 0 Then
						For Each dDefaultRow In dsInvisibles.Tables(1).Select("CAMPO_PADRE = " + auxRow.Item("CAMPO_PADRE").ToString + " AND VISIBLE = 0 AND LINEA = " + i.ToString)
							If ds.Tables("TEMPDESGLOSE").Select("LINEA=" + i.ToString + " AND CAMPO_PADRE = " + dDefaultRow.Item("CAMPO_PADRE").ToString + " AND CAMPO_HIJO = " + dDefaultRow.Item("CAMPO_HIJO").ToString).Length = 0 Then
								dNewRow = ds.Tables("TEMPDESGLOSE").NewRow
								dNewRow.Item("LINEA") = i
								dNewRow.Item("CAMPO_PADRE") = dDefaultRow.Item("CAMPO_PADRE").ToString
								dNewRow.Item("CAMPO_HIJO") = dDefaultRow.Item("CAMPO_HIJO")
								dNewRow.Item("VALOR_TEXT") = dDefaultRow.Item("VALOR_TEXT")
								If (IsDBNull(dDefaultRow.Item("VALOR_TEXT"))) And (Not IsDBNull(dDefaultRow.Item("LISTA_TEXT"))) Then
									dNewRow.Item("VALOR_TEXT") = dDefaultRow.Item("LISTA_TEXT")
								End If
								dNewRow.Item("VALOR_NUM") = dDefaultRow.Item("VALOR_NUM")
								If dDefaultRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoFecha Then
									dNewRow.Item("VALOR_FEC") = DevolverFechaRelativaA(DBNullToSomething(dDefaultRow.Item("VALOR_NUM")), DBNullToSomething(dDefaultRow.Item("VALOR_FEC")))
								End If
								dNewRow.Item("VALOR_BOOL") = dDefaultRow.Item("VALOR_BOOL")
								If Not bMaper Then dNewRow.Item("CAMBIO") = dDefaultRow.Item("CAMBIO")
								dNewRow.Item("TIPOGS") = dDefaultRow.Item("TIPO_CAMPO_GS")

								dRowsCcd = ds.Tables("TEMP").Select("CAMPO=" & dDefaultRow.Item("CAMPO_PADRE"))
								If bUsarCampoDef Then
									dNewRow.Item("CAMPO_PADRE_DEF") = dRowsCcd(0).Item("CAMPO_DEF")
									dNewRow.Item("CAMPO_HIJO_DEF") = dDefaultRow.Item("COPIA_CAMPO_DEF")
								End If

								If bMaper Then
									dNewRow.Item("VALERP") = dDefaultRow.Item("VALIDACION_ERP")
									dNewRow.Item("IDATRIB") = dDefaultRow.Item("ID_ATRIB_GS")
								End If
								Try
									'caso Presupuesto nivel 4: Campo Oculto que controla por otro lado el maper.
									'Intenta meter algo que ya he metido. Solo desglose popup.       
									ds.Tables("TEMPDESGLOSE").Rows.Add(dNewRow)
								Catch ex As Exception
								End Try
							Else
								dNewRow = ds.Tables("TEMPDESGLOSE").Select("LINEA=" + i.ToString + " AND CAMPO_PADRE = " + dDefaultRow.Item("CAMPO_PADRE").ToString + " AND CAMPO_HIJO = " + dDefaultRow.Item("CAMPO_HIJO").ToString)(0)
								If IsDBNull(dNewRow.Item("LINEA_OLD")) Then
									dNewRow.Item("VALOR_TEXT") = dDefaultRow.Item("VALOR_TEXT")
									If (IsDBNull(dDefaultRow.Item("VALOR_TEXT"))) And (Not IsDBNull(dDefaultRow.Item("LISTA_TEXT"))) Then
										dNewRow.Item("VALOR_TEXT") = dDefaultRow.Item("LISTA_TEXT")
									End If
									dNewRow.Item("VALOR_NUM") = dDefaultRow.Item("VALOR_NUM")
									If dDefaultRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoFecha Then
										dNewRow.Item("VALOR_FEC") = DevolverFechaRelativaA(DBNullToSomething(dDefaultRow.Item("VALOR_NUM")), DBNullToSomething(dDefaultRow.Item("VALOR_FEC")))
									End If
									dNewRow.Item("VALOR_BOOL") = dDefaultRow.Item("VALOR_BOOL")
									If Not bMaper Then dNewRow.Item("CAMBIO") = dDefaultRow.Item("CAMBIO")
									dNewRow.Item("TIPOGS") = dDefaultRow.Item("TIPO_CAMPO_GS")

									dRowsCcd = ds.Tables("TEMP").Select("CAMPO=" & dDefaultRow.Item("CAMPO_PADRE"))
									If bUsarCampoDef Then
										dNewRow.Item("CAMPO_PADRE_DEF") = dRowsCcd(0).Item("CAMPO_DEF")
										dNewRow.Item("CAMPO_HIJO_DEF") = dDefaultRow.Item("COPIA_CAMPO_DEF")
									End If

									If bMaper Then
										dNewRow.Item("VALERP") = dDefaultRow.Item("VALIDACION_ERP")
										dNewRow.Item("IDATRIB") = dDefaultRow.Item("ID_ATRIB_GS")
									End If
								End If
							End If
						Next
					Else
						For Each dDefaultRow In dsInvisibles.Tables(4).Rows
							If auxRow.Item("CAMPO_PADRE") = dDefaultRow.Item("CAMPO_PADRE") Then
								If ds.Tables("TEMPDESGLOSE").Select("LINEA IS NULL AND CAMPO_HIJO = " + dDefaultRow.Item("CAMPO_HIJO").ToString).Length = 0 Then
									dNewRow = ds.Tables("TEMPDESGLOSE").NewRow
									dNewRow.Item("LINEA") = i
									dNewRow.Item("CAMPO_PADRE") = auxRow.Item("CAMPO_PADRE")
									dNewRow.Item("CAMPO_HIJO") = dDefaultRow.Item("CAMPO_HIJO")
									dNewRow.Item("VALOR_TEXT") = dDefaultRow.Item("VALOR_TEXT")
									If (IsDBNull(dDefaultRow.Item("VALOR_TEXT"))) And (Not IsDBNull(dDefaultRow.Item("LISTA_TEXT"))) Then
										dNewRow.Item("VALOR_TEXT") = dDefaultRow.Item("LISTA_TEXT")
									End If
									dNewRow.Item("VALOR_NUM") = dDefaultRow.Item("VALOR_NUM")
									If dDefaultRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoFecha Then
										dNewRow.Item("VALOR_FEC") = DevolverFechaRelativaA(DBNullToSomething(dDefaultRow.Item("VALOR_NUM")), DBNullToSomething(dDefaultRow.Item("VALOR_FEC")))
									End If
									dNewRow.Item("VALOR_BOOL") = dDefaultRow.Item("VALOR_BOOL")
									If Not bMaper Then dNewRow.Item("CAMBIO") = dDefaultRow.Item("CAMBIO")

									dRowsCcd = ds.Tables("TEMP").Select("CAMPO=" & dDefaultRow.Item("CAMPO_PADRE"))
									If bUsarCampoDef Then
										dNewRow.Item("CAMPO_PADRE_DEF") = dRowsCcd(0).Item("CAMPO_DEF")
										dNewRow.Item("CAMPO_HIJO_DEF") = dDefaultRow.Item("COPIA_CAMPO_DEF")
									End If

									dNewRow.Item("TIPOGS") = dDefaultRow.Item("TIPO_CAMPO_GS")
									If bMaper Then
										dNewRow.Item("VALERP") = dDefaultRow.Item("VALIDACION_ERP")
										dNewRow.Item("IDATRIB") = dDefaultRow.Item("ID_ATRIB_GS")
									End If
									Try
										ds.Tables("TEMPDESGLOSE").Rows.Add(dNewRow)
									Catch ex As Exception
									End Try
								End If
							End If
						Next
					End If
				End If
			Next
		Next

		'pm-> la tables(7) es Vincular. la tables(8) es "se escapa el caso de desglose no popup invisible sin lineas en la versi�n". Devuelve 8 u 9 tablas.
		'qa-> la tables(7) es "se escapa el caso de desglose no popup invisible sin lineas en la versi�n". Devuelve 7 u 8 tablas.
		If (bNuevoWorkflow AndAlso dsInvisibles.Tables.Count > 8) OrElse (Not bNuevoWorkflow AndAlso dsInvisibles.Tables.Count > 7) Then
			Dim IdTblsOcultas As Integer = 8
			If Not bNuevoWorkflow Then IdTblsOcultas = 7

			'METEMOS LOS CAMPOS DE DESGLOSE QUE ESTUVIERAN OCULTOS. El motivo de estar ocultos es q el desglose esta oculto y en ningun  momento ha tenido lineas
			For Each dRow In dsInvisibles.Tables(IdTblsOcultas).Rows
				If ds.Tables("TEMP").Select("CAMPO=" & dRow.Item("CAMPO_HIJO")).Length = 0 Then
					dNewRow = ds.Tables("TEMP").NewRow
					dNewRow.Item("CAMPO") = dRow.Item("CAMPO_HIJO")
					dNewRow.Item("ES_SUBCAMPO") = 1
					If bUsarCampoDef Then dNewRow.Item("CAMPO_DEF") = dRow.Item("CAMPO_HIJO_DEF")
					Try
						ds.Tables("TEMP").Rows.Add(dNewRow)
					Catch ex As Exception
					End Try

					dNewRow = ds.Tables("TEMPDESGLOSE").NewRow
					dNewRow.Item("LINEA") = 0
					dNewRow.Item("CAMPO_PADRE") = dRow.Item("CAMPO_PADRE")
					dNewRow.Item("CAMPO_HIJO") = dRow.Item("CAMPO_HIJO")
					If bUsarCampoDef Then
						dNewRow.Item("CAMPO_PADRE_DEF") = dRow.Item("CAMPO_PADRE_DEF")
						dNewRow.Item("CAMPO_HIJO_DEF") = dRow.Item("CAMPO_HIJO_DEF")
					End If
					Try
						ds.Tables("TEMPDESGLOSE").Rows.Add(dNewRow)
					Catch ex As Exception
					End Try
				End If
			Next
		End If

		'Vincular
		If Not (ds.Tables("VINCULAR") Is Nothing) AndAlso (bNuevoWorkflow) Then
			'Qa tiene la tabla pero no usa.
			For Each dRow In dsInvisibles.Tables(7).Rows
				dNewRow = ds.Tables("VINCULAR").NewRow

				dNewRow.Item("DESGLOSEVINCULADA") = dRow.Item("DESGLOSEVINCULADA")
				dNewRow.Item("LINEAVINCULADA") = dRow.Item("LINEAVINCULADA")
				dNewRow.Item("INSTANCIAORIGEN") = dRow.Item("INSTANCIAORIGEN")
				dNewRow.Item("DESGLOSEORIGEN") = dRow.Item("DESGLOSEORIGEN")
				dNewRow.Item("LINEAORIGEN") = dRow.Item("LINEAORIGEN")

				ds.Tables("VINCULAR").Rows.Add(dNewRow)
			Next
		End If

		Return ds
	End Function
	''' <summary>
	''' Procedimiento que carga los tipos de estado de la accion
	''' </summary>
	''' <returns>Un DataSet con los datos del tipo de estado</returns>
	''' <remarks>
	''' Llamada desde: PmWeb/desglose/Page_load - CargarValoresDefecto, PmWeb/impexp/Page_load, 
	''' Tiempo m�ximo: 0,5 seg</remarks>
	Public Function CargarTiposEstadoAccion() As DataSet
		Authenticate()
		Dim data As DataSet
		data = DBServer.Instancia_CargarTiposEstadoAccion(mlID)
		Return data
	End Function
	Public Sub CargarCumplimentacionFactura(Optional ByVal sUsuario As String = Nothing, Optional ByVal sProve As String = Nothing, Optional ByVal bEsGestor As Boolean = False, Optional ByVal bEsReceptor As Boolean = False)
		Authenticate()
		m_bDetalleEditable = DBServer.Instancia_CargarCumplimentacionFactura(mlID, m_lVersion, sUsuario, sProve, bEsGestor, bEsReceptor)
	End Sub
	''' <summary>
	''' Indica si el detalle de pedido es editable o no
	''' </summary>
	''' <remarks></remarks>
	Public Sub CargarCumplimentacionDesglosePedido()
		Authenticate()
		m_bDetalleEditable = DBServer.Instancia_CargarCumplimentacionDesglosePedido(mlID, m_lVersion, Etapa, RolActual)
	End Sub
	Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
		MyBase.New(dbserver, isAuthenticated)
		moDen = New MultiIdioma
	End Sub
	''' <summary>
	''' Graba en bbdd una acci�n cualquiera que incluya cambio de etapa � una devoluci�n
	''' </summary>
	''' <param name="lAccion">Id de la acc�on a realizar</param> 
	''' <param name="sPer">Codigo de persona conectada</param>
	''' <param name="sFrom">From del email de notificaci�n que se enviar�</param>
	''' <param name="sComent">Comentario de la acci�n</param>
	''' <param name="oRoles">Lista de roles implicados en la acci�n</param>
	''' <param name="sProve">Codigo del proveedor en caso de que sea de portal</param>
	''' <param name="sCodCia">C�digo de la compa��a en el caso de que sea de portal</param>
	''' <param name="sDenCia">Denominaci�n de la compa��a en el caso de que sea de portal</param>
	''' <param name="sNIFCia">NIF de la compa��a en el caso de que sea de portal</param>
	''' <param name="sNombre">Nombre del proveedor</param>
	''' <param name="sApellidos">Apellidos del proveedor</param>
	''' <param name="sTelefono">Tel�fono del proveedor</param>
	''' <param name="sEmail">Email del proveedor</param>
	''' <param name="sFax">Fax del proveedor</param>
	''' <param name="AccionGuardar">Si es True es que la accion no cambia de etapa, es un guardado</param>
	''' <param name="iTipoSolicitud">TipoDeSolicitud</param>
	''' <param name="bPortal">Si hay q mandar mail desde el servidor de portal (true) o de gs (false)</param>
	''' <param name="sIdiomaDefecto">Idioma por defecto</param>
	''' <remarks>Llamada desde: WebServicePM/service.asmx/ServiceThread; Tiempo m�ximo:0,5</remarks>
	Public Sub RealizarAccion(ByVal lAccion As Integer, Optional ByVal sPer As String = Nothing, Optional ByVal sFrom As String = Nothing,
								   Optional ByVal sComent As String = Nothing, Optional ByVal oRoles As DataSet = Nothing, Optional ByVal sProve As String = Nothing,
								   Optional ByVal sCodCia As String = Nothing, Optional ByVal sDenCia As String = Nothing, Optional ByVal sNIFCia As String = Nothing,
								   Optional ByVal sNombre As String = Nothing, Optional ByVal sApellidos As String = Nothing, Optional ByVal sTelefono As String = Nothing,
								   Optional ByVal sEmail As String = Nothing, Optional ByVal sFax As String = Nothing,
								   Optional ByVal iTipoSolicitud As TiposDeDatos.TipoDeSolicitud = TiposDeDatos.TipoDeSolicitud.SolicitudDeCompras,
								   Optional ByVal AccionGuardar As Boolean = False, Optional ByVal bPortal As Boolean = False, Optional ByVal sIdiomaDefecto As String = Nothing)
		If Not AccionGuardar Then
			DBServer.Instancia_RealizarAccion(ID, lAccion, sPer, oRoles, sComent, NuevoID, sProve, iTipoSolicitud)
		End If

		'si no se ha producido ning�n error env�a el email a los Notificados:
		If Not lAccion = 0 Then
			Dim dDatos As New Dictionary(Of String, String)
			dDatos.Add("Accion", lAccion)
			dDatos.Add("From", sFrom)
			dDatos.Add("Per", sPer)
			dDatos.Add("Prove", sProve)
			dDatos.Add("CodCia", sCodCia)
			dDatos.Add("DenCia", sDenCia)
			dDatos.Add("NIFCia", sNIFCia)
			dDatos.Add("Nombre", sNombre)
			dDatos.Add("Apellidos", sApellidos)
			dDatos.Add("Telefono", sTelefono)
			dDatos.Add("Email", sEmail)
			dDatos.Add("Fax", sFax)
			dDatos.Add("TipoSolicitud", iTipoSolicitud)
			dDatos.Add("Portal", bPortal)
			dDatos.Add("IdiomaDefecto", sIdiomaDefecto)

			ThreadPool.QueueUserWorkItem(New WaitCallback(AddressOf NotificarRealizarAccion), dDatos)
		End If
	End Sub
	''' <summary>
	''' Envia una notificaci�n indicando que se ha realizado una acci�n sobre la solicitud
	''' </summary>
	''' <param name="obj">Objeto que contiene todos los datos necesarios para hacer la notificaci�n.</param>
	''' <remarks>Llamada desde: RealizarAccion; Tiempo m�ximo: 2 sg.</remarks>
	Private Sub NotificarRealizarAccion(ByVal obj As Object)
		Dim oNotificador As New Notificar(DBServer, mIsAuthenticated, , CBool(obj.Item("Portal")))
		m_lVersion = DBServer.Instancia_ObtenerUltimaVersion(Me.ID)
		oNotificador.NotificacionRealizarAccion(Me.ID, CLng(obj.item("Accion")), obj.item("From"), m_lVersion, obj.item("Per"), IIf(obj.item("Prove") <> Nothing, True, False),
												obj.item("CodCia"), obj.item("DenCia"), obj.item("NIFCia"), obj.item("Nombre"), obj.item("Apellidos"), obj.item("Telefono"),
												obj.item("Email"), obj.item("Fax"), obj.item("TipoSolicitud"), obj.item("IdiomaDefecto"))
		oNotificador.FuerzaFinalizeSmtpClient()
		oNotificador = Nothing
	End Sub
	''' <summary>
	''' Envia notificados cuando se crean las auto facturas
	''' </summary>
	''' <param name="obj">Objeto que contiene todos los datos necesarios para hacer la notificaci�n.</param>
	''' <remarks>Llamada desde: RealizarAccion; Tiempo m�ximo: 2 sg.</remarks>
	Private Sub NotificarCreateFactura(ByVal obj As Object)
		Dim oNotificador As New Notificar(DBServer, mIsAuthenticated)
		oNotificador.NotificacionAltaAutofactura(CLng(obj.item("Instancia")), obj.item("From"))
		oNotificador.FuerzaFinalizeSmtpClient()
		oNotificador = Nothing
	End Sub
	''' <summary>
	''' Obtendr� la etapa actual en la que se encuentra la instancia bas�ndose en la persona o proveedor que se pasa por par�metro, 
	''' que ser� un usuario/proveedor al que se le ha trasladado la solicitud
	''' </summary>
	''' <param name="sIdi">Idioma</param>
	''' <param name="sPer">Cod de la persona</param>
	''' <param name="lBloque">Id del bloque</param>
	''' <remarks>Llamada desde: PMWeb --> workflow/NWGestionInstancia.aspx.vb; Tiempo m�ximo: 1 sg</remarks>
	Public Sub DevolverEtapaActual(ByVal sIdi As String, Optional ByVal sPer As String = Nothing, Optional ByVal lBloque As Long = Nothing, Optional ByVal bEsGestor As Boolean = False, Optional ByVal bEsReceptor As Boolean = False)
		Authenticate()

		Dim data As DataSet
		data = DBServer.Instancia_DevolverEtapaActual(mlID, sIdi, sPer, lBloque, bEsGestor, bEsReceptor)
		If Not data.Tables(0).Rows.Count = 0 Then
			m_lEstado = DBNullToSomething(data.Tables(0).Rows(0).Item("ESTADO"))
			msDenEtapaActual = DBNullToSomething(data.Tables(0).Rows(0).Item("BLOQUE_DEN"))
			miEtapaActual = DBNullToSomething(data.Tables(0).Rows(0).Item("BLOQUE"))
			miRolActual = DBNullToSomething(data.Tables(0).Rows(0).Item("ROL"))
			miInstanciaBloque = DBNullToSomething(data.Tables(0).Rows(0).Item("ID"))
			mbInstanciaBloqueTrasladada = SQLBinaryToBoolean(data.Tables(0).Rows(0).Item("TRASLADADA"))
			miInstanciaBloqueBloq = data.Tables(0).Rows(0).Item("BLOQ")
			m_sDestinatarioEst = DBNullToSomething(data.Tables(0).Rows(0).Item("CUMPLIMENTADOR"))
			If DBNullToDbl(data.Tables(0).Rows(0).Item("COMO_ASIGNAR")) = 3 And IsDBNull(data.Tables(0).Rows(0).Item("PER_ROL")) Then
				m_sAprobadorActual = Nothing 'Solo lo uso para saber si est� asignada una persona al rol
			Else
				m_sAprobadorActual = DBNullToSomething(data.Tables(0).Rows(0).Item("PER_ROL"))
			End If
			mbVerDetalleFlujo = SQLBinaryToBoolean(data.Tables(0).Rows(0).Item("VER_FLUJO"))
			mbVerDetallePersona = SQLBinaryToBoolean(data.Tables(0).Rows(0).Item("VER_DETALLE_PER"))
			mbPermitirTraslados = SQLBinaryToBoolean(data.Tables(0).Rows(0).Item("PERMITIR_TRASLADO"))
		End If

		data = Nothing
	End Sub
	''' <summary>
	''' Obtendr� la etapa actual en la que se encuentra la instancia bas�ndose en la persona o proveedor que se pasa por par�metro, 
	''' que ser� un usuario/proveedor al que se le ha trasladado la solicitud
	''' </summary>
	''' <param name="sIdi">Idioma</param>
	''' <remarks>Llamada desde: PMWeb --> workflow/NWGestionInstancia.aspx.vb; Tiempo m�ximo: 1 sg</remarks>
	Public Sub DevolverEtapaActualPortal(ByVal sIdi As String, ByVal sProve As String)
		Authenticate()

		Dim data As DataSet

		Dim i As Integer
		Dim bCarga As Boolean

		data = DBServer.Instancia_DevolverEtapaActual(mlID, sIdi, sProve)

		If Not data.Tables(0).Rows.Count = 0 Then
			For i = 0 To data.Tables(0).Rows.Count - 1
				bCarga = True
				'Hay un caso que no funciona bien. Tengo el proveedor definido como rol en la 
				'etapa y aparte se le ha trasladado la solicitud.
				'En este caso salen 2 lineas y la valida es la de trasladada.
				If data.Tables(0).Rows.Count > 1 Then
					If IsDBNull(data.Tables(0).Rows(i).Item("CUMPLIMENTADOR")) Then
						bCarga = False
					End If
				End If
				If bCarga Then
					msDenEtapaActual = DBNullToSomething(data.Tables(0).Rows(i).Item("BLOQUE_DEN"))
					miEtapaActual = DBNullToSomething(data.Tables(0).Rows(i).Item("BLOQUE"))
					miRolActual = DBNullToSomething(data.Tables(0).Rows(i).Item("ROL"))
					miInstanciaBloque = DBNullToSomething(data.Tables(0).Rows(i).Item("ID"))
					mbInstanciaBloqueTrasladada = SQLBinaryToBoolean(data.Tables(0).Rows(i).Item("TRASLADADA"))
					m_sDestinatarioEst = DBNullToSomething(data.Tables(0).Rows(i).Item("CUMPLIMENTADOR"))
					miInstanciaBloqueBloq = data.Tables(0).Rows(i).Item("BLOQ")
					mbVerDetalleFlujo = SQLBinaryToBoolean(data.Tables(0).Rows(i).Item("VER_FLUJO"))
				End If
			Next
		End If

		data = Nothing
	End Sub
	''' <summary>
	''' Obtendr� el nombre de la etapa actual en la que se encuentra la instancia cuando el pedido se ha emitido al proveedor
	''' </summary>
	''' <param name="sIdi">Idioma</param>
	Public Sub DevolverEtapaActualEmitido(ByVal sIdi As String)
		Authenticate()

		Dim sDenEtapa As String = ""
		sDenEtapa = DBServer.Instancia_DevolverEtapaActualEmitido(mlID, sIdi)
		msDenEtapaActual = sDenEtapa
	End Sub
	''' <summary>
	''' Obtendr� la etapa actual en la que se encuentra la instancia bas�ndose en la persona o proveedor que se pasa por par�metro, 
	''' que ser� un usuario/proveedor al que se le ha trasladado la solicitud
	''' </summary>
	''' <param name="sIdi">Idioma</param>
	''' <remarks>Llamada desde: PMWeb --> workflow/gestiontrasladada.aspx.vb; Tiempo m�ximo: 1 sg</remarks>
	Public Sub DevolverEtapaActualTraslado(ByVal sIdi As String)
		Authenticate()

		Dim data As DataSet
		Dim i As Integer
		data = DBServer.Instancia_DevolverEtapaActualTraslado(mlID, sIdi)

		If Not data.Tables(0).Rows.Count = 0 Then
			For i = 0 To data.Tables(0).Rows.Count - 1
				msDenEtapaActual = DBNullToSomething(data.Tables(0).Rows(i).Item("BLOQUE_DEN"))
				miEtapaActual = DBNullToSomething(data.Tables(0).Rows(i).Item("BLOQUE"))
				miRolActual = DBNullToSomething(data.Tables(0).Rows(i).Item("ROL"))
				miInstanciaBloque = DBNullToSomething(data.Tables(0).Rows(i).Item("ID"))
				mbInstanciaBloqueTrasladada = SQLBinaryToBoolean(data.Tables(0).Rows(i).Item("TRASLADADA"))
				miInstanciaBloqueBloq = data.Tables(0).Rows(i).Item("BLOQ")
				m_sDestinatarioEst = DBNullToSomething(data.Tables(0).Rows(i).Item("CUMPLIMENTADOR"))
				If IsDBNull(data.Tables(0).Rows(i).Item("PER_ROL")) Then
					m_sAprobadorActual = Nothing 'Solo lo uso para saber si est� asignada una persona al rol
				Else
					m_sAprobadorActual = data.Tables(0).Rows(i).Item("PER_ROL")
				End If
				mbVerDetalleFlujo = SQLBinaryToBoolean(data.Tables(0).Rows(i).Item("VER_FLUJO"))
				mbVerDetallePersona = SQLBinaryToBoolean(data.Tables(0).Rows(i).Item("VER_DETALLE_PER"))
				mbPermitirTraslados = SQLBinaryToBoolean(data.Tables(0).Rows(i).Item("PERMITIR_TRASLADO"))
			Next
		End If

		data = Nothing
	End Sub
	''' <summary>
	''' Funcion que devuelve un DataSet con los datos de las siguientes etapas
	''' </summary>
	''' <param name="lAccion">Identificador de la accion</param>
	''' <param name="sPer">Codigo de persona del usuario que esta utilizando la aplicaci�n</param>
	''' <param name="sIdi">Idioma de la aplicacion</param>
	''' <param name="ds">DataSet con los datos de la accion</param>
	''' <param name="sRolPorWebService">Gestamp tiene 100 etapas a las q ir desde peticionario en varios flujos. Se quitan para q solo sea una etapa y un web 
	''' service desde integraci�n nos dice cuales son las personas de autoasignaci�n. Aqui se devuelve el rol. -1 indica No es el caso 100 etapas. Por si hay
	''' etapas en paralelo, lo q se devuelve es un string con los bloque@rol separados por comas.Ejemplo: -1 � 12596 � 12596,16324</param> 
	''' <param name="TipoSolicitud">Tipo de solicitud de la instancia</param> 
	''' <returns>Un DataSet con los datos de las siguientes etapas de la instancia</returns>
	''' <remarks>
	''' Llamada desde: PmWeb/guardarinstancia/Page_load, PmWeb/NWRealizarAccion/Page_load, 
	''' tiempo m�ximo: 0,8 seg</remarks>
	Public Function DevolverSiguientesEtapas(ByVal lAccion As Integer, ByVal sPer As String, ByVal sIdi As String, Optional ByVal ds As DataSet = Nothing,
										 Optional ByVal bEsGestor As Boolean = False, Optional ByVal bEsReceptor As Boolean = False,
										 Optional ByRef sRolPorWebService As String = "-1", Optional ByVal TipoSolicitud As Short = -1) As DataSet
		Authenticate()

		moData = DBServer.Instancia_ObtenerSiguientesEtapas(mlID, lAccion, sPer, sIdi, m_dImporte, ds, bEsGestor, bEsReceptor, sRolPorWebService, TipoSolicitud)
		Return moData
	End Function
	''' <summary>
	''' Funcion que devuelve la informacion en formulario de las siguientes etapas
	''' </summary>
	''' <param name="lAccion">Id de la accion</param>
	''' <param name="sPer">Codigo de persona del usuario que esta utilizando la aplicaci�n</param>
	''' <param name="sIdi">Idioma de la aplicacion</param>
	''' <param name="ds">DataSet con los datos de los campos de la etapa</param>
	''' <param name="sRolPorWebService">Gestamp tiene 100 etapas a las q ir desde peticionario en varios flujos. Se quitan para q solo sea una etapa y un web 
	''' service desde integraci�n nos dice cuales son las personas de autoasignaci�n. Aqui se devuelve el rol. -1 indica No es el caso 100 etapas. Por si hay
	''' etapas en paralelo, lo q se devuelve es un string con los bloque@rol separados por comas.Ejemplo: -1 � 12596 � 12596,16324</param> 
	''' <returns>Un DataSet con los datos de las siguientes etapas</returns>    
	''' <remarks>
	''' Llamada desde: PmWeb/GuardarInstancia/Page_load
	''' Tiempo m�ximo: 0,8 seg</remarks>
	Public Function DevolverSiguientesEtapasForm(ByVal lAccion As Integer, ByVal sPer As String, ByVal sIdi As String, ByVal ds As DataSet, ByRef sRolPorWebService As String,
												 ByVal lBloque As Long, ByVal lWorkflow As Long) As DataSet
		Authenticate()

		moData = DBServer.Instancia_ObtenerSiguientesEtapasForm(mlID, lAccion, sPer, sIdi, ds, m_dImporte, sRolPorWebService, lBloque, lWorkflow)
		Return moData
	End Function
	''' <summary>
	''' Funci�n que actualiza la persona que abrueba en caso de que la aprobacion sea por centroSM y devuelve si queda alguien por aprobar
	''' </summary>
	''' <param name="sPer">Codigo de persona del usuario que esta utilizando la aplicaci�n</param>
	''' Llamada desde: detallepedido.aspx.vb
	Public Function AprobacionCompletaCentroSM(ByVal sPer As String, ByVal idBloqueActual As Long, ByVal idAccion As Long) As Boolean
		Authenticate()

		Dim bTodasAprovabadas As Boolean = DBServer.Instancia_AprobacionCompletaCentroSM(ID, RolActual, idBloqueActual, sPer, idAccion)

		Return bTodasAprovabadas
	End Function
	''' <summary>
	''' Procedimiento que devuelve los datos del traslado
	''' </summary>
	''' <param name="sPer">Codigo de persona del usuario que esta utilizando la aplicaci�n</param>
	''' <param name="bTraslado">Variable booleana que indica si se trata de un traslado</param>
	''' <remarks>
	''' Llamada desde: PmWeb/devolucion/page_load, PmWeb/devolucionOK/page_load, PmWeb/gestionTrasladada/page_load, 
	''' Tiempo m�ximo: 0,5 seg</remarks>
	Public Sub DevolverDatosTraslado(ByVal sPer As String, Optional ByVal bTraslado As Boolean = False)
		Authenticate()

		Dim data As DataSet
		Dim i As Integer
		data = DBServer.Instancia_DevolverDatosTraslado(mlID, sPer, bTraslado)
		If Not data.Tables(0).Rows.Count = 0 Then
			For i = 0 To data.Tables(0).Rows.Count - 1
				m_sNomPersonaEst = DBNullToSomething(data.Tables(0).Rows(i).Item("NOM_PERSONA_EST"))
				m_sPersonaEst = DBNullToSomething(data.Tables(0).Rows(i).Item("PERSONA_EST"))
				m_dFechaEstado = DBNullToSomething(data.Tables(0).Rows(i).Item("FECHA_EST"))
				m_dFecLimiteTraslado = DBNullToSomething(data.Tables(0).Rows(i).Item("FEC_LIMITE"))
				m_sDestinatarioEst = DBNullToSomething(data.Tables(0).Rows(i).Item("DESTINATARIO"))
				m_sNomDestinatario = DBNullToSomething(data.Tables(0).Rows(i).Item("NOM_DESTINATARIO"))
				m_sComentario = DBNullToSomething(data.Tables(0).Rows(i).Item("COMENT"))
			Next
		End If

		data = Nothing
	End Sub
	''' <summary>
	''' Funcion que devuelve el importe de adjudicado
	''' </summary>
	''' <returns>Un valor Doble que corresponde al importe del proceso</returns>
	''' <remarks>
	''' Llamada desde: PmWeb/guardarInstancia/comprobarCondiciones, PmWeb/campos/comprobarCondiciones, PmWeb/desglose/comprobarCondiciones
	''' Tiempo m�ximo:0,3 seg </remarks>
	Public Function ObtenerImporteAdjudicado() As Double
		Authenticate()

		Dim dImporte As Double
		dImporte = DBServer.Instancia_ObtenerImporteAdjudicado(mlID)
		Return dImporte
	End Function
	''' <summary>
	''' funcion que devuelve las precondiciones que se tienen que cumplir para que se ejecute una acci�n
	''' </summary>
	''' <param name="lIdAccion">Identificador de la acci�n</param>
	''' <param name="sIdi">Idioma de la aplicaci�n</param>
	''' <param name="iVersion">Versi�n para obtener los datos de copia_campo</param>
	''' <returns>Un dataset con los datos de las precondiciones que se deben cumplir para que se ejecute una accion</returns>
	''' <remarks>Llamada desde: PmWeb/guardarinstancia/GenerarDataSetYHacerComprobaciones,GuardarconWorkFlow; tiempo maximo: 0,75 seg </remarks>
	Public Function ObtenerPrecondicionesAccion(ByVal lIdAccion As Integer, ByVal sIdi As String, Optional ByVal iVersion As Integer = 0) As DataSet
		Authenticate()
		Dim data As DataSet
		data = DBServer.Instancia_LoadPrecondicionesAccion(mlID, lIdAccion, sIdi, iVersion)
		Return data
	End Function
	''' <summary>
	''' Funci�n que lanza las validaciones de la integraci�n 
	''' </summary>
	''' <param name="oDs">Dataset con los datos para validar</param>
	''' <param name="oDsAux">DataSet auxilia</param>
	''' <param name="Idioma">Codigo de idioma</param>
	''' <param name="sPer">Codigo de persona</param>
	''' <param name="AccionRol">Identificador de la acci�n</param>
	''' <param name="sBloquesDestino">identificadores de los bloques destino (concatenados con ",")</param>
	''' <param name="bAlta">TRUE cuando es el alta de la solicitud, FALSE en caso contrario</param>
	''' <returns>Una posible excepciion que pueda dar</returns>
	''' <remarks>Llamada desde: GuardarInstancia.aspx\GenerarDataSetyHacerComprobaciones(),GuardarInstancia.aspx\GenerarConWorkflow
	''' Tiempo m�ximo 1 sec</remarks>
	Public Function ControlMapper(ByVal oDs As DataSet, ByVal oDsAux As DataSet, ByVal Idioma As String, ByVal sPer As String,
								  ByVal AccionRol As Long, ByVal sBloquesDestino As String, ByVal bAlta As Boolean) As Object
		Authenticate()

		Dim iNumError As Short
		Dim strError As String = String.Empty

		If Solicitud.ValidacionesIntegracion Is Nothing Then
			Solicitud.ValidacionesIntegracion = New PM_ValidacionesIntegracion(DBServer, mIsAuthenticated)
		End If

		Dim iTipoValidacionIntegracion As TipoValidacionIntegracion
		iTipoValidacionIntegracion = Solicitud.ValidacionesIntegracion.ControlMapper(oDs, oDsAux, Idioma, iNumError, strError, sPer, IIf(bAlta, 0, ID),
																					 Solicitud.ID, AccionRol, sBloquesDestino)
		If Not iTipoValidacionIntegracion = TipoValidacionIntegracion.SinMensaje Then
			Return New With {.tipoValidacionIntegracion = iTipoValidacionIntegracion, .iNumError = iNumError, .strError = strError}
		End If

		ControlMapper = Nothing
	End Function
	''' <summary>
	''' Funci�n que lanza las validaciones de la integraci�n por el camino de la aprobaci�n multiple
	''' </summary>
	''' <param name="Idioma">Codigo de idioma</param>
	''' <param name="sPer">Codigo de persona</param>
	''' <param name="lAccion"> Identificador de la acci�n (PM_COPIA_ACCIONES)</param> 
	''' <param name="dsSiguienteEtapa">Dataset con los identificadores de las siguientes etapas</param> 
	''' <returns>Una posible excepciion que pueda dar</returns>
	''' <remarks>Llamada desde: Instancia.vb\ComprobarMapper() (por el camino de la aprobaci�n multiple)
	''' Tiempo m�ximo 1 sec</remarks>
	Public Function ControlMapper(ByVal Idioma As String, ByVal sPer As String, ByVal lAccion As Long, ByVal dsSiguienteEtapa As DataSet) As Object
		Authenticate()


		Dim iNumError As Short
		Dim strError As String = String.Empty
		If moSolicitud.ValidacionesIntegracion Is Nothing Then
			moSolicitud.ValidacionesIntegracion = New PM_ValidacionesIntegracion(DBServer, mIsAuthenticated)
		End If
		moSolicitud.ValidacionesIntegracion.Load_LongitudesDeCodigos()
		If moCamposDesglose Is Nothing Then CargarCamposDesglose(Idioma, sPer)
		Dim dsMatArt As DataSet = CrearListasIntegracion(sPer, lAccion)

		Dim sBloquesDestino As String = ""
		If (dsSiguienteEtapa.Tables.Count > 0) AndAlso (dsSiguienteEtapa.Tables.Count > 1) Then
			For Each oRow As DataRow In dsSiguienteEtapa.Tables(0).Rows
				sBloquesDestino = sBloquesDestino & oRow.Item("BLOQUE") & ","
			Next
			sBloquesDestino = IIf(Right(sBloquesDestino, 1) = ",", Left(sBloquesDestino, Len(sBloquesDestino) - 1), sBloquesDestino)
		End If

		Dim iTipoValidacionIntegracion As TipoValidacionIntegracion
		If Not moSolicitud.ValidacionesIntegracion.HayIntegracionSalidaWCF(TablasIntegracion.PM) Then
			iTipoValidacionIntegracion = moSolicitud.ValidacionesIntegracion.EvalMapper(dsMatArt, Idioma, iNumError, strError, sPer, mlID, Me.mlSolicitud, lAccion, sBloquesDestino)
			If Not iTipoValidacionIntegracion = TipoValidacionIntegracion.SinMensaje Then
				Return New With {.tipoValidacionIntegracion = iTipoValidacionIntegracion, .iNumError = iNumError, .strError = strError}
			End If
		Else
			iTipoValidacionIntegracion = moSolicitud.ValidacionesIntegracion.ValidacionesWCF_PM(strError, dsMatArt, Idioma, sBloquesDestino)
			If Not iTipoValidacionIntegracion = TipoValidacionIntegracion.SinMensaje Then
				Return New With {.tipoValidacionIntegracion = iTipoValidacionIntegracion, .iNumError = iNumError, .strError = strError}
			End If
		End If

		Return Nothing
	End Function
	''' <summary>
	''' Funci�n que crea un dataset con todas las tablas que vamos a necesitar para las validaciones de integraci�n
	''' </summary>
	''' <returns>Devuelve el dataset</returns>
	''' <remarks>Llamada desde ControlMapper(); Tiempo m�ximo 0-1 seg</remarks>
	Private Function CrearListasIntegracion(Optional ByVal sPer As String = "", Optional ByVal lAccion As Long = 0) As DataSet
		Dim dsMatArt As New DataSet
		Dim dtNoDesglMat As DataTable
		Dim dtNoDesglArt As DataTable
		Dim dtNoDesglAlm As DataTable
		Dim dtNoDesglPre As DataTable
		Dim dtNoDesglAtb As DataTable
		Dim dtNoDesglProve As DataTable
		Dim dtNoDesglCentroSM As DataTable
		Dim dtNoDesglTipoPedido As DataTable
		Dim dtNoDesglPartida As DataTable
		Dim dtNoDesglFecIniRectif As DataTable
		Dim dtNoDesglFecFinRectif As DataTable
		Dim dtNoDesglFecIniSuministro As DataTable
		Dim dtNoDesglFecFinSuministro As DataTable
		Dim dtNoDesglOrgCompras As DataTable
		Dim dtNoDesglCen As DataTable
		Dim dtNoDesglPersona As DataTable
		Dim dtNoDesglMoneda As DataTable
		Dim dtSiDesglMat As DataTable
		Dim dtSiDesglArt As DataTable
		Dim dtSiDesglAlm As DataTable
		Dim dtSiDesglPre As DataTable
		Dim dtSiDesglAtb As DataTable
		Dim dtSiDesglCen As DataTable
		Dim dtSiDesglOrgCompras As DataTable
		Dim dtSiDesglTablaExt As DataTable
		Dim dtSiDesglImporteLin As DataTable
		Dim dtSiDesglCantLin As DataTable
		Dim dtSiDesglPrecUn As DataTable
		Dim dtSiDesglPartida As DataTable
		Dim dtSiDesglFecIniRectif As DataTable
		Dim dtSiDesglFecFinRectif As DataTable
		Dim dtSiDesglFecIniSuministro As DataTable
		Dim dtSiDesglFecFinSuministro As DataTable

		Dim dsInforGeneral As New DataTable

		dsInforGeneral = dsMatArt.Tables.Add("InforGen")

		dtNoDesglMat = dsMatArt.Tables.Add("NoDMa")
		dtNoDesglArt = dsMatArt.Tables.Add("NoDAr")

		dtNoDesglAlm = dsMatArt.Tables.Add("NoDAl")
		dtNoDesglPre = dsMatArt.Tables.Add("NoDPr")
		dtNoDesglAtb = dsMatArt.Tables.Add("NoDAt")

		dtNoDesglProve = dsMatArt.Tables.Add("NoDProve")
		dtNoDesglCentroSM = dsMatArt.Tables.Add("NoDCenSM")
		dtNoDesglTipoPedido = dsMatArt.Tables.Add("NoDTipoPed")
		dtNoDesglPartida = dsMatArt.Tables.Add("NoDPartida")

		dtNoDesglFecIniRectif = dsMatArt.Tables.Add("NoDFecIniRectif")
		dtNoDesglFecFinRectif = dsMatArt.Tables.Add("NoDFecFinRectif")
		dtNoDesglFecIniSuministro = dsMatArt.Tables.Add("NoDFecIniSuministro")
		dtNoDesglFecFinSuministro = dsMatArt.Tables.Add("NoDFecFinSuministro")
		dtNoDesglOrgCompras = dsMatArt.Tables.Add("NoDOr")
		dtNoDesglCen = dsMatArt.Tables.Add("NoDCe")
		dtNoDesglPersona = dsMatArt.Tables.Add("NoDPersona")
		dtNoDesglMoneda = dsMatArt.Tables.Add("NoDMoneda")

		dtSiDesglMat = dsMatArt.Tables.Add("SiDMa")
		dtSiDesglArt = dsMatArt.Tables.Add("SiDAr")

		dtSiDesglAlm = dsMatArt.Tables.Add("SiDAl")
		dtSiDesglPre = dsMatArt.Tables.Add("SiDPr")

		dtSiDesglAtb = dsMatArt.Tables.Add("SiDAt")
		dtSiDesglCen = dsMatArt.Tables.Add("SiDCe")
		dtSiDesglOrgCompras = dsMatArt.Tables.Add("SiDOr")

		dtSiDesglTablaExt = dsMatArt.Tables.Add("SiDTExt")
		dtSiDesglImporteLin = dsMatArt.Tables.Add("SiDIm")

		dtSiDesglCantLin = dsMatArt.Tables.Add("SiDCant")
		dtSiDesglPrecUn = dsMatArt.Tables.Add("SiDPrec")

		dtSiDesglPartida = dsMatArt.Tables.Add("SiDPartida")

		dtSiDesglFecIniRectif = dsMatArt.Tables.Add("SiDFecIniRectif")
		dtSiDesglFecFinRectif = dsMatArt.Tables.Add("SiDFecFinRectif")
		dtSiDesglFecIniSuministro = dsMatArt.Tables.Add("SiDFecIniSuministro")
		dtSiDesglFecFinSuministro = dsMatArt.Tables.Add("SiDFecFinSuministro")

		dtNoDesglMat.Columns.Add("GRUPO", System.Type.GetType("System.Int32"))
		dtNoDesglMat.Columns.Add("ORDEN", System.Type.GetType("System.Int32"))
		dtNoDesglMat.Columns.Add("GMN1", System.Type.GetType("System.String"))
		dtNoDesglMat.Columns.Add("GMN2", System.Type.GetType("System.String"))
		dtNoDesglMat.Columns.Add("GMN3", System.Type.GetType("System.String"))
		dtNoDesglMat.Columns.Add("GMN4", System.Type.GetType("System.String"))

		dtNoDesglArt.Columns.Add("GRUPO", System.Type.GetType("System.Int32"))
		dtNoDesglArt.Columns.Add("ORDEN", System.Type.GetType("System.Int32"))
		dtNoDesglArt.Columns.Add("ART4", System.Type.GetType("System.String"))

		dtNoDesglAlm.Columns.Add("GRUPO", System.Type.GetType("System.Int32"))
		dtNoDesglAlm.Columns.Add("ALMACEN", System.Type.GetType("System.String"))

		dtNoDesglPre.Columns.Add("GRUPO", System.Type.GetType("System.Int32"))
		dtNoDesglPre.Columns.Add("PRESUP4", System.Type.GetType("System.String"))

		dtNoDesglProve.Columns.Add("GRUPO", System.Type.GetType("System.Int32"))
		dtNoDesglProve.Columns.Add("PROVE", System.Type.GetType("System.String"))

		dtNoDesglCentroSM.Columns.Add("GRUPO", System.Type.GetType("System.Int32"))
		dtNoDesglCentroSM.Columns.Add("CENTRO_SM", System.Type.GetType("System.String"))

		dtNoDesglTipoPedido.Columns.Add("GRUPO", System.Type.GetType("System.Int32"))
		dtNoDesglTipoPedido.Columns.Add("TIPO_PEDIDO", System.Type.GetType("System.String"))

		dtNoDesglAtb.Columns.Add("GRUPO", System.Type.GetType("System.Int32"))
		dtNoDesglAtb.Columns.Add("ID", System.Type.GetType("System.Int32"))
		dtNoDesglAtb.Columns.Add("VALOR", System.Type.GetType("System.String"))
		dtNoDesglAtb.Columns.Add("VALERP", System.Type.GetType("System.Int32"))

		dtNoDesglPartida.Columns.Add("GRUPO", System.Type.GetType("System.Int32"))
		dtNoDesglPartida.Columns.Add("PARTIDA", System.Type.GetType("System.String"))

		dtNoDesglFecIniRectif.Columns.Add("GRUPO", System.Type.GetType("System.Int32"))
		dtNoDesglFecIniRectif.Columns.Add("FECINIRECTIF", System.Type.GetType("System.DateTime"))

		dtNoDesglFecFinRectif.Columns.Add("GRUPO", System.Type.GetType("System.Int32"))
		dtNoDesglFecFinRectif.Columns.Add("FECFINRECTIF", System.Type.GetType("System.DateTime"))

		dtNoDesglFecIniSuministro.Columns.Add("GRUPO", System.Type.GetType("System.Int32"))
		dtNoDesglFecIniSuministro.Columns.Add("FECINISUMINISTRO", System.Type.GetType("System.DateTime"))

		dtNoDesglFecFinSuministro.Columns.Add("GRUPO", System.Type.GetType("System.Int32"))
		dtNoDesglFecFinSuministro.Columns.Add("FECFINSUMINISTRO", System.Type.GetType("System.DateTime"))

		dtNoDesglOrgCompras.Columns.Add("GRUPO", System.Type.GetType("System.Int32"))
		dtNoDesglOrgCompras.Columns.Add("ORGCOMPRAS", System.Type.GetType("System.String"))
		dtNoDesglCen.Columns.Add("GRUPO", System.Type.GetType("System.Int32"))
		dtNoDesglCen.Columns.Add("CENTRO", System.Type.GetType("System.String"))

		dtNoDesglPersona.Columns.Add("GRUPO", System.Type.GetType("System.Int32"))
		dtNoDesglPersona.Columns.Add("PER", System.Type.GetType("System.String"))

		dtNoDesglMoneda.Columns.Add("GRUPO", System.Type.GetType("System.Int32"))
		dtNoDesglMoneda.Columns.Add("MONEDA", System.Type.GetType("System.String"))

		dtSiDesglMat.Columns.Add("LINEA", System.Type.GetType("System.Int32"))
		dtSiDesglMat.Columns.Add("PADRE", System.Type.GetType("System.Int32"))
		dtSiDesglMat.Columns.Add("GMN1", System.Type.GetType("System.String"))
		dtSiDesglMat.Columns.Add("GMN2", System.Type.GetType("System.String"))
		dtSiDesglMat.Columns.Add("GMN3", System.Type.GetType("System.String"))
		dtSiDesglMat.Columns.Add("GMN4", System.Type.GetType("System.String"))

		dtSiDesglArt.Columns.Add("LINEA", System.Type.GetType("System.Int32"))
		dtSiDesglArt.Columns.Add("PADRE", System.Type.GetType("System.Int32"))
		dtSiDesglArt.Columns.Add("ART4", System.Type.GetType("System.String"))

		dtSiDesglAlm.Columns.Add("LINEA", System.Type.GetType("System.Int32"))
		dtSiDesglAlm.Columns.Add("PADRE", System.Type.GetType("System.Int32"))
		dtSiDesglAlm.Columns.Add("ALMACEN", System.Type.GetType("System.String"))

		dtSiDesglPre.Columns.Add("LINEA", System.Type.GetType("System.Int32"))
		dtSiDesglPre.Columns.Add("PADRE", System.Type.GetType("System.Int32"))
		dtSiDesglPre.Columns.Add("PRESUP4", System.Type.GetType("System.String"))

		dtSiDesglAtb.Columns.Add("LINEA", System.Type.GetType("System.Int32"))
		dtSiDesglAtb.Columns.Add("PADRE", System.Type.GetType("System.Int32"))
		dtSiDesglAtb.Columns.Add("ID", System.Type.GetType("System.Int32"))
		dtSiDesglAtb.Columns.Add("VALOR", System.Type.GetType("System.String"))
		dtSiDesglAtb.Columns.Add("VALERP", System.Type.GetType("System.String"))

		dtSiDesglCen.Columns.Add("LINEA", System.Type.GetType("System.Int32"))
		dtSiDesglCen.Columns.Add("PADRE", System.Type.GetType("System.Int32"))
		dtSiDesglCen.Columns.Add("CENTRO", System.Type.GetType("System.String"))

		dtSiDesglOrgCompras.Columns.Add("LINEA", System.Type.GetType("System.Int32"))
		dtSiDesglOrgCompras.Columns.Add("PADRE", System.Type.GetType("System.Int32"))
		dtSiDesglOrgCompras.Columns.Add("ORGCOMPRAS", System.Type.GetType("System.String"))

		dtSiDesglTablaExt.Columns.Add("LINEA", System.Type.GetType("System.Int32"))
		dtSiDesglTablaExt.Columns.Add("PADRE", System.Type.GetType("System.Int32"))
		dtSiDesglTablaExt.Columns.Add("TABLA_EXTERNA", System.Type.GetType("System.Int32"))
		dtSiDesglTablaExt.Columns.Add("VALOR_TABLAEXT", System.Type.GetType("System.String"))

		dtSiDesglImporteLin.Columns.Add("LINEA", System.Type.GetType("System.Int32"))
		dtSiDesglImporteLin.Columns.Add("PADRE", System.Type.GetType("System.Int32"))
		dtSiDesglImporteLin.Columns.Add("CALCULADO", System.Type.GetType("System.Int32"))
		dtSiDesglImporteLin.Columns.Add("VALOR_IMPORTE", System.Type.GetType("System.String"))

		dtSiDesglCantLin.Columns.Add("LINEA", System.Type.GetType("System.Int32"))
		dtSiDesglCantLin.Columns.Add("PADRE", System.Type.GetType("System.Int32"))
		dtSiDesglCantLin.Columns.Add("CANTIDAD", System.Type.GetType("System.Double"))

		dtSiDesglPrecUn.Columns.Add("LINEA", System.Type.GetType("System.Int32"))
		dtSiDesglPrecUn.Columns.Add("PADRE", System.Type.GetType("System.Int32"))
		dtSiDesglPrecUn.Columns.Add("PRECIO", System.Type.GetType("System.Double"))

		dtSiDesglPartida.Columns.Add("LINEA", System.Type.GetType("System.Int32"))
		dtSiDesglPartida.Columns.Add("PADRE", System.Type.GetType("System.Int32"))
		dtSiDesglPartida.Columns.Add("PARTIDA", System.Type.GetType("System.String"))

		dtSiDesglFecIniRectif.Columns.Add("LINEA", System.Type.GetType("System.Int32"))
		dtSiDesglFecIniRectif.Columns.Add("PADRE", System.Type.GetType("System.Int32"))
		dtSiDesglFecIniRectif.Columns.Add("FECINIRECTIF", System.Type.GetType("System.DateTime"))

		dtSiDesglFecFinRectif.Columns.Add("LINEA", System.Type.GetType("System.Int32"))
		dtSiDesglFecFinRectif.Columns.Add("PADRE", System.Type.GetType("System.Int32"))
		dtSiDesglFecFinRectif.Columns.Add("FECFINRECTIF", System.Type.GetType("System.DateTime"))

		dtSiDesglFecIniSuministro.Columns.Add("LINEA", System.Type.GetType("System.Int32"))
		dtSiDesglFecIniSuministro.Columns.Add("PADRE", System.Type.GetType("System.Int32"))
		dtSiDesglFecIniSuministro.Columns.Add("FECINISUMINISTRO", System.Type.GetType("System.DateTime"))

		dtSiDesglFecFinSuministro.Columns.Add("LINEA", System.Type.GetType("System.Int32"))
		dtSiDesglFecFinSuministro.Columns.Add("PADRE", System.Type.GetType("System.Int32"))
		dtSiDesglFecFinSuministro.Columns.Add("FECFINSUMINISTRO", System.Type.GetType("System.DateTime"))

		dsInforGeneral.Columns.Add("PERSONA", System.Type.GetType("System.String"))
		dsInforGeneral.Columns.Add("INSTANCIA", System.Type.GetType("System.Int32"))
		dsInforGeneral.Columns.Add("SOLICITUD", System.Type.GetType("System.Int32"))
		dsInforGeneral.Columns.Add("ACCION", System.Type.GetType("System.Int32"))

		Dim dtNew As DataRow
		Dim sMaterial As String

		''Informaci�n General
		dtNew = dsInforGeneral.NewRow
		dtNew.Item("PERSONA") = IIf(IsNothing(sPer), "", sPer)
		dtNew.Item("INSTANCIA") = IIf(IsNothing(mlID), 0, mlID)
		dtNew.Item("SOLICITUD") = IIf(IsNothing(Me.mlSolicitud), 0, Me.mlSolicitud)
		dtNew.Item("ACCION") = IIf(IsNothing(lAccion), 0, lAccion)
		dsInforGeneral.Rows.Add(dtNew)

		For Each oGrupo As Grupo In moGrupos.Grupos
			For Each dr As DataRow In oGrupo.DSCampos.Tables(0).Select("(NOT TIPO_CAMPO_GS IS NULL OR NOT ID_ATRIB_GS IS NULL ) AND NOT (VALOR_TEXT IS NULL AND VALOR_NUM IS NULL)")
				If Not IsDBNull(dr.Item("VALOR_TEXT")) Then
					Select Case CType(DBNullToInteger(dr.Item("TIPO_CAMPO_GS")), TiposDeDatos.TipoCampoGS)
						Case TiposDeDatos.TipoCampoGS.Material
							dtNew = dtNoDesglMat.NewRow
							dtNew.Item("GRUPO") = dr.Item("GRUPO")
							sMaterial = dr.Item("VALOR_TEXT")
							dtNew.Item("GMN1") = Trim(Left(sMaterial, moSolicitud.LongitudesDeCodigos.giLongCodGMN1))
							sMaterial = Mid(sMaterial, moSolicitud.LongitudesDeCodigos.giLongCodGMN1 + 1)
							dtNew.Item("GMN2") = Trim(Left(sMaterial, moSolicitud.LongitudesDeCodigos.giLongCodGMN2))
							sMaterial = Mid(sMaterial, moSolicitud.LongitudesDeCodigos.giLongCodGMN2 + 1)
							dtNew.Item("GMN3") = Trim(Left(sMaterial, moSolicitud.LongitudesDeCodigos.giLongCodGMN3))
							sMaterial = Mid(sMaterial, moSolicitud.LongitudesDeCodigos.giLongCodGMN3 + 1)
							dtNew.Item("GMN4") = Trim(sMaterial)
							dtNoDesglMat.Rows.Add(dtNew)
						Case TiposDeDatos.TipoCampoGS.NuevoCodArticulo
							dtNew = dtNoDesglArt.NewRow
							dtNew.Item("GRUPO") = dr.Item("GRUPO")
							dtNew.Item("ART4") = dr.Item("VALOR_TEXT")
							dtNoDesglArt.Rows.Add(dtNew)
						Case TiposDeDatos.TipoCampoGS.Pres4
							dtNew = dtNoDesglPre.NewRow
							dtNew.Item("GRUPO") = dr.Item("GRUPO")
							dtNew.Item("PRESUP4") = dr.Item("VALOR_TEXT")
							dtNoDesglPre.Rows.Add(dtNew)
						Case TiposDeDatos.TipoCampoGS.Proveedor
							dtNew = dtNoDesglProve.NewRow
							dtNew.Item("GRUPO") = dr.Item("GRUPO")
							dtNew.Item("PROVE") = dr.Item("VALOR_TEXT")
							dtNoDesglProve.Rows.Add(dtNew)
						Case TiposDeDatos.TipoCampoGS.CentroCoste
							dtNew = dtNoDesglCentroSM.NewRow
							dtNew.Item("GRUPO") = dr.Item("GRUPO")
							dtNew.Item("CENTRO_SM") = dr.Item("VALOR_TEXT")
							dtNoDesglCentroSM.Rows.Add(dtNew)
						Case TiposDeDatos.TipoCampoGS.TipoPedido
							dtNew = dtNoDesglTipoPedido.NewRow
							dtNew.Item("GRUPO") = dr.Item("GRUPO")
							dtNew.Item("TIPO_PEDIDO") = dr.Item("VALOR_TEXT")
							dtNoDesglTipoPedido.Rows.Add(dtNew)
						Case TiposDeDatos.TipoCampoGS.Partida
							dtNew = dtNoDesglPartida.NewRow
							dtNew.Item("GRUPO") = dr.Item("GRUPO")
							dtNew.Item("PARTIDA") = dr.Item("VALOR_TEXT")
							dtNoDesglPartida.Rows.Add(dtNew)
						Case TiposDeDatos.TipoCampoGS.OrganizacionCompras
							dtNew = dtNoDesglOrgCompras.NewRow
							dtNew.Item("GRUPO") = dr.Item("GRUPO")
							dtNew.Item("ORGCOMPRAS") = dr.Item("VALOR_TEXT")
							dtNoDesglOrgCompras.Rows.Add(dtNew)
						Case TiposDeDatos.TipoCampoGS.Centro
							dtNew = dtNoDesglCen.NewRow
							dtNew.Item("GRUPO") = dr.Item("GRUPO")
							dtNew.Item("CENTRO") = dr.Item("VALOR_TEXT")
							dtNoDesglCen.Rows.Add(dtNew)
						Case TiposDeDatos.TipoCampoGS.Persona
							dtNew = dtNoDesglPersona.NewRow
							dtNew.Item("GRUPO") = dr.Item("GRUPO")
							dtNew.Item("PER") = dr.Item("VALOR_TEXT")
							dtNoDesglPersona.Rows.Add(dtNew)
						Case TiposDeDatos.TipoCampoGS.Moneda
							dtNew = dtNoDesglMoneda.NewRow
							dtNew.Item("GRUPO") = dr.Item("GRUPO")
							dtNew.Item("MONEDA") = dr.Item("VALOR_TEXT")
							dtNoDesglMoneda.Rows.Add(dtNew)
						Case TiposDeDatos.TipoCampoGS.SinTipo
							If Not IsDBNull(dr.Item("ID_ATRIB_GS")) Then
								dtNew = dtNoDesglAtb.NewRow
								dtNew.Item("GRUPO") = dr.Item("GRUPO")
								dtNew.Item("ID") = dr.Item("ID_ATRIB_GS")
								dtNew.Item("VALOR") = dr.Item("VALOR_TEXT")
								dtNew.Item("VALERP") = dr.Item("VALIDACION_ERP")
								dtNoDesglAtb.Rows.Add(dtNew)
							End If
					End Select
				Else
					If CType(DBNullToInteger(dr.Item("TIPO_CAMPO_GS")), TiposDeDatos.TipoCampoGS) = TiposDeDatos.TipoCampoGS.Almacen Then
						dtNew = dtNoDesglAlm.NewRow
						dtNew.Item("GRUPO") = dr.Item("GRUPO")
						dtNew.Item("ALMACEN") = dr.Item("VALOR_NUM")
						dtNoDesglAlm.Rows.Add(dtNew)
					ElseIf CType(DBNullToInteger(dr.Item("TIPO_CAMPO_GS")), TiposDeDatos.TipoCampoGS) = TiposDeDatos.TipoCampoGS.SinTipo Then
						If Not IsDBNull(dr.Item("ID_ATRIB_GS")) Then
							dtNew = dtNoDesglAtb.NewRow
							dtNew.Item("GRUPO") = dr.Item("GRUPO")
							dtNew.Item("ID") = dr.Item("ID_ATRIB_GS")
							dtNew.Item("VALOR") = dr.Item("VALOR_NUM")
							dtNew.Item("VALERP") = dr.Item("VALIDACION_ERP")
							dtNoDesglAtb.Rows.Add(dtNew)
						End If
					ElseIf Not IsDBNull(dr.Item("VALOR_FEC")) Then
						If CType(DBNullToInteger(dr.Item("TIPO_CAMPO_GS")), TiposDeDatos.TipoCampoGS) = TiposDeDatos.TipoCampoGS.InicioAbono Then
							dtNew = dtNoDesglFecIniRectif.NewRow
							dtNew.Item("GRUPO") = dr.Item("GRUPO")
							dtNew.Item("FECINIRECTIF") = dr.Item("VALOR_FEC")
							dtNoDesglFecIniRectif.Rows.Add(dtNew)
						End If
						If CType(DBNullToInteger(dr.Item("TIPO_CAMPO_GS")), TiposDeDatos.TipoCampoGS) = TiposDeDatos.TipoCampoGS.FinAbono Then
							dtNew = dtNoDesglFecFinRectif.NewRow
							dtNew.Item("GRUPO") = dr.Item("GRUPO")
							dtNew.Item("FECFINRECTIF") = dr.Item("VALOR_FEC")
							dtNoDesglFecFinRectif.Rows.Add(dtNew)
						End If
						If CType(DBNullToInteger(dr.Item("TIPO_CAMPO_GS")), TiposDeDatos.TipoCampoGS) = TiposDeDatos.TipoCampoGS.IniSuministro Then
							dtNew = dtNoDesglFecIniSuministro.NewRow
							dtNew.Item("GRUPO") = dr.Item("GRUPO")
							dtNew.Item("FECINISUMINISTRO") = dr.Item("VALOR_FEC")
							dtNoDesglFecIniSuministro.Rows.Add(dtNew)
						End If
						If CType(DBNullToInteger(dr.Item("TIPO_CAMPO_GS")), TiposDeDatos.TipoCampoGS) = TiposDeDatos.TipoCampoGS.FinSuministro Then
							dtNew = dtNoDesglFecFinSuministro.NewRow
							dtNew.Item("GRUPO") = dr.Item("GRUPO")
							dtNew.Item("FECFINSUMINISTRO") = dr.Item("VALOR_FEC")
							dtNoDesglFecFinSuministro.Rows.Add(dtNew)
						End If
					End If
				End If
			Next
		Next

		For Each oCampo As Campo In moCamposDesglose
			If oCampo.Data.Tables.Count > 2 Then
				For Each dr As DataRow In oCampo.Data.Tables(2).Rows
					Dim drconf As DataRow = oCampo.Data.Tables(0).Select("ID=" & dr.Item("CAMPO_HIJO"))(0)
					If Not IsDBNull(drconf.Item("TABLA_EXTERNA")) Then
						If Not IsDBNull(dr.Item("VALOR_TEXT")) Then
							dtNew = dtSiDesglTablaExt.NewRow
							dtNew.Item("LINEA") = dr.Item("LINEA")
							dtNew.Item("PADRE") = dr.Item("CAMPO_PADRE")
							dtNew.Item("VALOR_TABLAEXT") = dr.Item("VALOR_TEXT")
							dtNew.Item("TABLA_EXTERNA") = drconf.Item("TABLA_EXTERNA")
							dtSiDesglTablaExt.Rows.Add(dtNew)
						ElseIf Not IsDBNull(dr.Item("VALOR_NUM")) Then
							dtNew = dtSiDesglTablaExt.NewRow
							dtNew.Item("LINEA") = dr.Item("LINEA")
							dtNew.Item("PADRE") = dr.Item("CAMPO_PADRE")
							dtNew.Item("VALOR_TABLAEXT") = dr.Item("VALOR_NUM")
							dtNew.Item("TABLA_EXTERNA") = drconf.Item("TABLA_EXTERNA")
							dtSiDesglTablaExt.Rows.Add(dtNew)
						End If
					ElseIf CType(drconf.Item("TIPO"), TipoCampoPredefinido) = TipoCampoPredefinido.Calculado _
						And (UCase(drconf.Item(2)) Like "*IMPORTE*LIN*" Or UCase(drconf.Item(2)) Like "*IMPORTE*L�N*") Then
						dtNew = dtSiDesglImporteLin.NewRow
						dtNew.Item("LINEA") = dr.Item("LINEA")
						dtNew.Item("PADRE") = dr.Item("CAMPO_PADRE")
						dtNew.Item("CALCULADO") = 1
						dtNew.Item("VALOR_IMPORTE") = dr.Item("VALOR_NUM")
						dtSiDesglImporteLin.Rows.Add(dtNew)
					Else
						If Not IsDBNull(dr.Item("VALOR_TEXT")) Then
							Select Case CType(DBNullToInteger(dr.Item("TIPO_CAMPO_GS")), TiposDeDatos.TipoCampoGS)
								Case TiposDeDatos.TipoCampoGS.Material
									dtNew = dtSiDesglMat.NewRow
									dtNew.Item("LINEA") = dr.Item("LINEA")
									dtNew.Item("PADRE") = dr.Item("CAMPO_PADRE")
									sMaterial = dr.Item("VALOR_TEXT")
									dtNew.Item("GMN1") = Trim(Left(sMaterial, moSolicitud.LongitudesDeCodigos.giLongCodGMN1))
									sMaterial = Mid(sMaterial, moSolicitud.LongitudesDeCodigos.giLongCodGMN1 + 1)
									dtNew.Item("GMN2") = Trim(Left(sMaterial, moSolicitud.LongitudesDeCodigos.giLongCodGMN2))
									sMaterial = Mid(sMaterial, moSolicitud.LongitudesDeCodigos.giLongCodGMN2 + 1)
									dtNew.Item("GMN3") = Trim(Left(sMaterial, moSolicitud.LongitudesDeCodigos.giLongCodGMN3))
									sMaterial = Mid(sMaterial, moSolicitud.LongitudesDeCodigos.giLongCodGMN3 + 1)
									dtNew.Item("GMN4") = Trim(sMaterial)
									dtSiDesglMat.Rows.Add(dtNew)
								Case TiposDeDatos.TipoCampoGS.NuevoCodArticulo
									dtNew = dtSiDesglArt.NewRow
									dtNew.Item("LINEA") = dr.Item("LINEA")
									dtNew.Item("PADRE") = dr.Item("CAMPO_PADRE")
									dtNew.Item("ART4") = Trim(dr.Item("VALOR_TEXT"))
									dtSiDesglArt.Rows.Add(dtNew)
								Case TiposDeDatos.TipoCampoGS.Pres4
									dtNew = dtSiDesglPre.NewRow
									dtNew.Item("LINEA") = dr.Item("LINEA")
									dtNew.Item("PADRE") = dr.Item("CAMPO_PADRE")
									dtNew.Item("PRESUP4") = dr.Item("VALOR_TEXT")
									dtSiDesglPre.Rows.Add(dtNew)
								Case TiposDeDatos.TipoCampoGS.SinTipo
									If Not IsDBNull(dr.Item("ID_ATRIB_GS")) Then
										dtNew = dtSiDesglAtb.NewRow
										dtNew.Item("LINEA") = dr.Item("LINEA")
										dtNew.Item("PADRE") = dr.Item("CAMPO_PADRE")
										dtNew.Item("ID") = dr.Item("ID_ATRIB_GS")
										dtNew.Item("VALERP") = drconf.Item("VALIDACION_ERP")
										dtNew.Item("VALOR") = dr.Item("VALOR_TEXT")
										dtSiDesglAtb.Rows.Add(dtNew)
									End If
								Case TiposDeDatos.TipoCampoGS.Almacen
									dtNew = dtSiDesglAlm.NewRow
									dtNew.Item("LINEA") = dr.Item("LINEA")
									dtNew.Item("PADRE") = dr.Item("CAMPO_PADRE")
									dtNew.Item("ALMACEN") = dr.Item("VALOR_TEXT")
									dtSiDesglAlm.Rows.Add(dtNew)
								Case TiposDeDatos.TipoCampoGS.Centro
									dtNew = dtSiDesglCen.NewRow
									dtNew.Item("LINEA") = dr.Item("LINEA")
									dtNew.Item("PADRE") = dr.Item("CAMPO_PADRE")
									dtNew.Item("CENTRO") = dr.Item("VALOR_TEXT")
									dtSiDesglCen.Rows.Add(dtNew)
								Case TiposDeDatos.TipoCampoGS.OrganizacionCompras
									dtNew = dtSiDesglOrgCompras.NewRow
									dtNew.Item("LINEA") = dr.Item("LINEA")
									dtNew.Item("PADRE") = dr.Item("CAMPO_PADRE")
									dtNew.Item("ORGCOMPRAS") = dr.Item("VALOR_TEXT")
									dtSiDesglOrgCompras.Rows.Add(dtNew)
								Case TiposDeDatos.TipoCampoGS.Partida
									dtNew = dtSiDesglPartida.NewRow
									dtNew.Item("LINEA") = dr.Item("LINEA")
									dtNew.Item("PADRE") = dr.Item("CAMPO_PADRE")
									dtNew.Item("PARTIDA") = dr.Item("VALOR_TEXT")
									dtSiDesglPartida.Rows.Add(dtNew)
								Case TiposDeDatos.TipoCampoGS.SinTipo
									If Not IsDBNull(dr.Item("ID_ATRIB_GS")) Then
										dtNew = dtSiDesglAtb.NewRow
										dtNew.Item("LINEA") = dr.Item("LINEA")
										dtNew.Item("PADRE") = dr.Item("CAMPO_PADRE")
										dtNew.Item("ID") = dr.Item("ID_ATRIB_GS")
										dtNew.Item("VALERP") = drconf.Item("VALIDACION_ERP")
										dtNew.Item("VALOR") = dr.Item("VALOR_TEXT")
										dtSiDesglAtb.Rows.Add(dtNew)
									End If
							End Select
						ElseIf Not IsDBNull(dr.Item("VALOR_NUM")) Then
							Select Case CType(DBNullToInteger(dr.Item("TIPO_CAMPO_GS")), TiposDeDatos.TipoCampoGS)
								Case TiposDeDatos.TipoCampoGS.Almacen
									dtNew = dtSiDesglAlm.NewRow
									dtNew.Item("LINEA") = dr.Item("LINEA")
									dtNew.Item("PADRE") = dr.Item("CAMPO_PADRE")
									dtNew.Item("ALMACEN") = dr.Item("VALOR_NUM")
									dtSiDesglAlm.Rows.Add(dtNew)
								Case TiposDeDatos.TipoCampoGS.Cantidad
									dtNew = dtSiDesglCantLin.NewRow
									dtNew.Item("LINEA") = dr.Item("LINEA")
									dtNew.Item("PADRE") = dr.Item("CAMPO_PADRE")
									dtNew.Item("CANTIDAD") = dr.Item("VALOR_NUM")
									dtSiDesglCantLin.Rows.Add(dtNew)
								Case TiposDeDatos.TipoCampoGS.PrecioUnitario
									dtNew = dtSiDesglPrecUn.NewRow
									dtNew.Item("LINEA") = dr.Item("LINEA")
									dtNew.Item("PADRE") = dr.Item("CAMPO_PADRE")
									dtNew.Item("PRECIO") = dr.Item("VALOR_NUM")
									dtSiDesglPrecUn.Rows.Add(dtNew)
								Case TiposDeDatos.TipoCampoGS.SinTipo
									If Not IsDBNull(dr.Item("ID_ATRIB_GS")) Then
										dtNew = dtSiDesglAtb.NewRow
										dtNew.Item("LINEA") = dr.Item("LINEA")
										dtNew.Item("PADRE") = dr.Item("CAMPO_PADRE")
										dtNew.Item("ID") = dr.Item("ID_ATRIB_GS")
										dtNew.Item("VALERP") = drconf.Item("VALIDACION_ERP")
										dtNew.Item("VALOR") = dr.Item("VALOR_NUM")
										dtSiDesglAtb.Rows.Add(dtNew)
									End If
							End Select
						ElseIf Not IsDBNull(dr.Item("VALOR_FEC")) Then
							Select Case CType(DBNullToInteger(dr.Item("TIPO_CAMPO_GS")), TiposDeDatos.TipoCampoGS)
								Case TiposDeDatos.TipoCampoGS.InicioAbono
									dtNew = dtSiDesglFecIniRectif.NewRow
									dtNew.Item("LINEA") = dr.Item("LINEA")
									dtNew.Item("PADRE") = dr.Item("CAMPO_PADRE")
									dtNew.Item("FECINIRECTIF") = dr.Item("VALOR_FEC")
									dtSiDesglFecIniRectif.Rows.Add(dtNew)
								Case TiposDeDatos.TipoCampoGS.FinAbono
									dtNew = dtSiDesglFecFinRectif.NewRow
									dtNew.Item("LINEA") = dr.Item("LINEA")
									dtNew.Item("PADRE") = dr.Item("CAMPO_PADRE")
									dtNew.Item("FECFINRECTIF") = dr.Item("VALOR_FEC")
									dtSiDesglFecFinRectif.Rows.Add(dtNew)
								Case TiposDeDatos.TipoCampoGS.IniSuministro
									dtNew = dtSiDesglFecIniSuministro.NewRow
									dtNew.Item("LINEA") = dr.Item("LINEA")
									dtNew.Item("PADRE") = dr.Item("CAMPO_PADRE")
									dtNew.Item("FECINISUMINISTRO") = dr.Item("VALOR_FEC")
									dtSiDesglFecIniSuministro.Rows.Add(dtNew)
								Case TiposDeDatos.TipoCampoGS.FinSuministro
									dtNew = dtSiDesglFecFinSuministro.NewRow
									dtNew.Item("LINEA") = dr.Item("LINEA")
									dtNew.Item("PADRE") = dr.Item("CAMPO_PADRE")
									dtNew.Item("FECFINSUMINISTRO") = dr.Item("VALOR_FEC")
									dtSiDesglFecFinSuministro.Rows.Add(dtNew)
								Case TiposDeDatos.TipoCampoGS.SinTipo
									If Not IsDBNull(dr.Item("ID_ATRIB_GS")) Then
										dtNew = dtSiDesglAtb.NewRow
										dtNew.Item("LINEA") = dr.Item("LINEA")
										dtNew.Item("PADRE") = dr.Item("CAMPO_PADRE")
										dtNew.Item("ID") = dr.Item("ID_ATRIB_GS")
										dtNew.Item("VALERP") = drconf.Item("VALIDACION_ERP")
										dtNew.Item("VALOR") = dr.Item("VALOR_FEC")
										dtSiDesglAtb.Rows.Add(dtNew)
									End If
							End Select
						ElseIf Not IsDBNull(dr.Item("VALOR_BOOL")) Then
							Select Case CType(DBNullToInteger(dr.Item("TIPO_CAMPO_GS")), TiposDeDatos.TipoCampoGS)
								Case TiposDeDatos.TipoCampoGS.SinTipo
									If Not IsDBNull(dr.Item("ID_ATRIB_GS")) Then
										dtNew = dtSiDesglAtb.NewRow
										dtNew.Item("LINEA") = dr.Item("LINEA")
										dtNew.Item("PADRE") = dr.Item("CAMPO_PADRE")
										dtNew.Item("ID") = dr.Item("ID_ATRIB_GS")
										dtNew.Item("VALERP") = drconf.Item("VALIDACION_ERP")
										dtNew.Item("VALOR") = dr.Item("VALOR_BOOL")
										dtSiDesglAtb.Rows.Add(dtNew)
									End If
							End Select
						End If
						''----------------------------------------------
					End If
				Next
			End If
		Next

		Return dsMatArt
	End Function
	''' <summary>
	''' Funcion que carga la combo del maper para los datos de los campos
	''' </summary>
	''' <param name="bSave">Si se debe coger el valor de copia_campo</param>
	''' <param name="Campo">Id del campo</param>
	''' <param name="Idioma">Idioma de la aplicacion</param>
	''' <returns>Un DataSet con los datos de los campos a cargar en el Maper</returns>
	''' <remarks>
	''' Llamada desde: PmWen/GuardarInstancia/DameDescripcion
	''' Tiempo m�ximo: 0,5 seg</remarks>
	Public Function CargaComboMaper(ByVal bSave As Boolean, ByVal Campo As Integer, ByVal Idioma As String) As DataSet
		Authenticate()

		Return Solicitud.CargaComboMaper(bSave, Campo, Idioma)
	End Function
	''' <summary>
	''' Procedimiento que devuelve el titulo de una instancia
	''' </summary>
	''' <param name="sIdi">Idioma de la aplicacion</param>
	''' <returns>Un string que ser� el titulo dado a la instancia</returns>
	''' <remarks>
	''' Llamada desde: PmWeb/campos/Page_Load - ESCRIBIRvALORcAMPO
	''' tiempo m�ximo: 0,3 SEG</remarks>
	Public Function DevolverTitulo(ByVal sIdi As String) As String
		Authenticate()

		Dim data As DataSet
		data = DBServer.Instancia_DevolverTitulo(mlID, sIdi)
		Try
			If data.Tables.Count > 0 AndAlso data.Tables(0).Rows.Count > 0 Then
				Return data.Tables(0).Rows(0).Item(0)
			Else
				Return String.Empty
			End If
		Catch ex As Exception
			Return ""
		End Try
	End Function
	''' <summary>
	''' Procedimiento que devuelve el importe de las solicitudes vinculadas con la instancia
	''' </summary>
	''' <returns>Un texto con el varlor en la moneda dada de las solicitudes vinculadas con la instancia</returns>
	''' <remarks>
	''' Llamada desde: PmWeb/Campos/Page_load
	''' Tiempo m�ximo: 0,4 seg</remarks>
	Public Function DevolverImporteSolicitudesVinculadas() As String
		Authenticate()

		Dim data As DataSet
		data = DBServer.Instancia_DevolverImporteSolicitudesVinculadas(mlID)
		Try
			If data.Tables.Count > 0 AndAlso data.Tables(0).Rows.Count > 0 Then
				Return data.Tables(0).Rows(0).Item(1)
			Else
				Return String.Empty
			End If
		Catch ex As Exception
			Return 0
		End Try
	End Function
	''' <summary>
	''' Procedimiento que actualiza a En proceso una instancia y un bloque
	''' </summary>
	''' <param name="iEnProceso">Id del proceso</param>
	''' <param name="iBloque">Id del bloque</param>
	''' <remarks>
	''' Llamada desde: PmWeb/guardarInstancia/GuardarConWorkFlowThread, WebServicePM/Service.asmx/ServiceThread
	''' Tiempo m�ximo: 0,8 seg</remarks>
	Public Sub Actualizar_En_proceso(ByVal iEnProceso As Integer, Optional ByVal iBloque As Integer = Nothing)
		Authenticate()
		DBServer.Instancia_Actualizar_En_Proceso(mlID, iEnProceso, iBloque)
	End Sub
	''' <summary>
	''' Actualiza en la instancia el campo de seguimiento para monitorizarla o no.
	''' </summary>
	''' <param name="iSeg">(1/0) 1: se monitorizar�</param>
	''' <param name="sPersona">cod. de la persona</param>
	''' <remarks>Llamada desde: monitorizarSolicitud.aspx; Tiempo m�ximo: 0sg.</remarks>
	Public Sub ActualizarMonitorizacion(ByVal iSeg As Integer, ByVal sPersona As String)
		Authenticate()
		DBServer.Instancia_ActualizarMonitorizacion(mlID, iSeg, sPersona)
	End Sub
	''' <summary>
	''' Actualiza el tiempo de procesamiento de las solicitudes
	''' </summary>
	''' <param name="lIDTiempoProc">Id (campo identity). Si no tiene nada hay que hacer una insert, si no, una update.</param>
	''' <param name="sCodUsuario">Cod del usuario</param>
	''' <param name="iFecha">Fecha que hay que actualizar (inicio, fin,..)</param>
	''' <remarks>Llamada desde: PMServer --> Instancia.vb -->ActualizarTiempoProcesamiento; Tiempo m�ximo: 0sg.</remarks>
	Public Sub ActualizarTiempoProcesamiento(Optional ByRef lIDTiempoProc As Long = Nothing, Optional ByVal sCodUsuario As String = Nothing,
											 Optional ByVal lBloque As Long = 0, Optional ByVal iFecha As Integer = 0, Optional ByVal HayError As Boolean = False,
											 Optional ByVal EsPortal As Nullable(Of Boolean) = Nothing, Optional TipoGuardado As Nullable(Of Integer) = Nothing,
											 Optional ByVal TipoAccion As Nullable(Of Integer) = Nothing)
		Authenticate()
		DBServer.Instancia_ActualizarTiempoProcesamiento(lIDTiempoProc, mlID, sCodUsuario, lBloque, iFecha, HayError, EsPortal, TipoGuardado, TipoAccion)
	End Sub
	Public Sub BorrarTiempoProcesamiento(ByVal lIDTiempoProc As Long)
		Authenticate()
		DBServer.Instancia_BorrarTiempoProcesamiento(lIDTiempoProc)
	End Sub
	''' <summary>
	''' Procedimiento que carga el detalle de las solicitudes hijas de un proceso
	''' </summary>
	''' <param name="sIdi">Idioma de la aplicaci�n</param>
	''' <remarks>
	''' Llamada desde: PmWeb/detallesolHijas/Page_load
	''' tiempo m�ximo: 1 seg</remarks>
	Public Sub CargarDetalleSolicitudesHijas(Optional ByVal sIdi As String = Nothing)
		Authenticate()
		m_oSolicitudesHijas = DBServer.Instancia_CargarDetalleSolicitudesHijas(mlID, sIdi)
	End Sub
	''' <summary>
	''' funcion que devuelve los items de un proceso de solicitud
	''' </summary>
	''' <returns>Un DataSet con los datos de los items de un proceso de solicitud</returns>
	''' <remarks>
	''' Llamada desde: PmWeb/EliminarSolicitud/Page_load
	''' Tiempo m�ximo: 0,6 seg</remarks>
	Public Function ItemsProcesoSolicitud() As DataSet
		Authenticate()
		moData = DBServer.Instancia_ItemsProcesoSolicitud(mlID)
		Return moData
	End Function
	''' <summary>
	''' Procedimiento que reserva una instancia por id
	''' </summary>
	''' <returns>Un entero que se corresponde con el codigo de error</returns>
	''' <remarks>
	''' Llamada desde; PmWeb/guardarInstancia/Page_load
	''' Tiempo m�ximo: 0,3 seg</remarks>
	Public Function ReservarInstanciaID() As Long
		Authenticate()
		ReservarInstanciaID = DBServer.Instancia_ReservarID(mlID)
	End Function
	''' <summary>
	''' Procedimiento que elimina un proceso
	''' </summary>
	''' <param name="lContrato">ID Contrato</param>
	''' <param name="sUsu">Codigo de usuario</param>
	''' <remarks>
	''' Llamada desde: PmWeb/guardarInstancia/cmdCancelar_ServerClick
	''' Tiempo m�ximo: 0,5 seg</remarks>
	Public Sub EliminarNuevo(ByVal lContrato As Long, Optional ByVal sUsu As String = Nothing)
		Authenticate()
		DBServer.EliminarInstancia(mlNuevoID, lContrato, sUsu)
	End Sub
	''' <summary>
	''' Funcion que carga un campo de una solicitud
	''' </summary>
	''' <param name="lCampo">Identificador del campo</param>
	''' <returns>Un DataSet con los datos del campo a cargar</returns>
	''' <remarks>
	''' Llamada desde: PmWeb/campos/ComprobarCondiciones, PmWeb/desglose/ComprobarCondiciones 
	''' Tiempo m�ximo: 0,35 seg</remarks>
	Public Function cargarCopiaCampo(ByVal lCampo As Long) As DataSet
		Authenticate()
		Return DBServer.cargarCopiaCampo(lCampo)
	End Function
	''' <summary>
	''' Funcion que carga un campo de una solicitud
	''' </summary>
	''' <param name="lCampo">Identificador del campo</param>
	''' <returns>Un DataSet con los datos del campo a cargar</returns>
	''' <remarks>
	''' Llamada desde: Editor.aspx
	''' Tiempo m�ximo: 0,35 seg</remarks>
	Public Function cargarCampoOrigen(ByVal lInstancia As Long, ByVal lCampo As Long) As DataSet
		Authenticate()
		Return DBServer.cargarCampoOrigen(lInstancia, lCampo)
	End Function
	''' <summary>
	''' Funcion que carga un campo de una solicitud
	''' </summary>
	''' <param name="lCampo">Identificador del campo</param>
	''' <returns>Un DataSet con los datos del campo a cargar</returns>
	''' <remarks>
	''' Llamada desde: PmWeb/campos/ComprobarCondiciones, PmWeb/desglose/ComprobarCondiciones 
	''' Tiempo m�ximo: 0,35 seg</remarks>
	Public Function cargarCampo(ByVal lCampo As Long) As DataSet
		Authenticate()
		Return DBServer.cargarCampo(lCampo, True, m_lVersion)
	End Function
	''' <summary>
	''' Funcion que carga los datos del campo de un desglose
	''' </summary>
	''' <param name="lCampo">Identificador del campo</param>
	''' <param name="nLinea">Identificador de la linea</param>
	''' <returns>Un DataSet con los datos del campo dentro del desglose</returns>
	''' <remarks>
	''' Llamada desdE: PmWeb/desglose/ComprobarCondiciones
	''' Tiempo m�ximo: 0,25 seg</remarks>
	Public Function cargarCampoDesglose(ByVal lCampo As Long, ByVal nLinea As Long) As Object
		Authenticate()

		Dim dsValorCampo As DataSet
		dsValorCampo = DBServer.cargarCampoDesglose(lCampo, nLinea, True, m_lVersion)

		Select Case dsValorCampo.Tables(0).Rows(0).Item("SUBTIPO")
			Case TiposDeDatos.TipoGeneral.TipoNumerico
				Return DBNullToSomething(dsValorCampo.Tables(0).Rows(0).Item("VALOR_NUM"))
			Case TiposDeDatos.TipoGeneral.TipoFecha
				Return DBNullToSomething(dsValorCampo.Tables(0).Rows(0).Item("VALOR_FEC"))
			Case TiposDeDatos.TipoGeneral.TipoBoolean
				Return DBNullToSomething(dsValorCampo.Tables(0).Rows(0).Item("VALOR_BOOL"))
			Case Else
				Return DBNullToSomething(dsValorCampo.Tables(0).Rows(0).Item("VALOR_TEXT"))
		End Select
	End Function
	''' <summary>
	''' Comprueba si el usuario pasado como par�metro es observador para la instancia
	''' </summary>
	''' <param name="sPer">Cod de la persona</param>
	''' <param name="bEsContrato">La instancia es un contrato o no</param> 
	''' <returns>True: Es observador/False: No es observador</returns>
	''' <remarks>Llamada desde: /workflow/comprobaraprob.aspx.vb; Tiempo m�ximo: 1 sg.</remarks>
	Public Function ComprobarEsObservador(ByVal sPer As String, Optional ByVal bEsContrato As Boolean = False) As Boolean
		Dim bRes As Boolean
		Authenticate()
		bRes = DBServer.Instancia_ComprobarEsObservador(mlID, sPer, bEsContrato)
		Return bRes
	End Function
	''' <summary>
	''' Procedimiento que carga una instancia
	''' </summary>
	''' <param name="sIdi">Idioma de la aplicacion</param>
	''' <remarks>
	''' Llamada desde: PmWeb/detalleSolicConsulta/Page_load, PmWeb/NWGestionInstancia/Page_load, WebServicePM/Service.asmx/Servicethread
	''' Tiempo m�ximo: 1 seg</remarks>
	Public Sub Cargar(ByVal sIdi As String)
		Authenticate()

		Dim data As DataSet
		Dim i As Integer
		data = DBServer.Instancia_Cargar(mlID, sIdi)

		If Not data.Tables.Count = 0 Then
			If Not data.Tables(0).Rows.Count = 0 Then
				For i = 0 To data.Tables(0).Rows.Count - 1 'EN REALIDAD SOLO DEVOLVER� UNA FILA
					m_sPeticionario = data.Tables(0).Rows(i).Item("PETICIONARIO").ToString
					m_sPeticionarioProve = DBNullToSomething(data.Tables(0).Rows(0).Item("PETICIONARIO_PROVE"))
					m_iPeticionarioProveContacto = DBNullToSomething(data.Tables(0).Rows(0).Item("PETICIONARIO_CON"))
					m_sNombrePet = data.Tables(0).Rows(i).Item("NOMBRE_PET").ToString
					m_lEstado = DBNullToSomething(data.Tables(0).Rows(i).Item("ESTADO"))
					m_lVersion = DBNullToSomething(data.Tables(0).Rows(i).Item("NUM_VERSION"))
					m_dFecAlta = DBNullToSomething(data.Tables(0).Rows(i).Item("FEC_ALTA"))
					m_dImporte = DBNullToSomething(data.Tables(0).Rows(i).Item("IMPORTE"))
					m_sMoneda = DBNullToSomething(data.Tables(0).Rows(i).Item("MON"))
					m_sCampoImporte = DBNullToSomething(data.Tables(0).Rows(i).Item("CAMPO_IMPORTE"))
					_idFormulario = DBNullToInteger(data.Tables(0).Rows(i).Item("FORMULARIO"))

					'Carga tambi�n la solicitud
					mlSolicitud = DBNullToSomething(data.Tables(0).Rows(i).Item("ID_SOLICIT"))
					moSolicitud = New Solicitud(DBServer, mIsAuthenticated)
					moSolicitud.ID = mlSolicitud
					moSolicitud.Codigo = DBNullToSomething(data.Tables(0).Rows(i).Item("COD_SOLICIT"))
					moSolicitud.Den(sIdi) = DBNullToSomething(data.Tables(0).Rows(i).Item("DEN_SOLICIT"))
					moSolicitud.Descr(sIdi) = DBNullToSomething(data.Tables(0).Rows(i).Item("DESCR_SOLICIT"))

					moSolicitud.Tipo = DBNullToSomething(data.Tables(0).Rows(i).Item("TIPO_SOLICIT"))
					moSolicitud.DenTipo = DBNullToSomething(data.Tables(0).Rows(i).Item("DENTIPO"))
					moSolicitud.TipoSolicit = DBNullToSomething(data.Tables(0).Rows(i).Item("TIPO_TIPOSOLICIT"))
					moSolicitud.ValidacionesIntegracion = New PM_ValidacionesIntegracion(DBServer, mIsAuthenticated)

					moSolicitud.Formulario = New Formulario(DBServer, mIsAuthenticated)
					moSolicitud.Formulario.Id = _idFormulario
				Next
			End If
		End If
		data = Nothing
	End Sub
	''' <summary>
	''' Funcion que Comprueba el Estado En proceso de una instancia
	''' </summary>
	''' <returns>Un entero que se corresponde con el error si existe</returns>
	''' <remarks>
	''' Llamada desde: PmWeb/comprobarEnProcesoServer/Page_load, PmWeb/comprobarEnProceso/Page_load
	''' Tiempo m�ximo: 0,25 seg</remarks>
	Public Function ComprobarEnProceso() As Integer
		Authenticate()
		ComprobarEnProceso = DBServer.Instancia_ComprobarEnProceso(mlID)
	End Function
	''' <summary>
	''' En aprobaciones/denegaciones masivas (desde Tareas) hay que controlar los obligatorios por si hay que
	''' impedir los cambios de etapas.
	''' </summary>
	''' <param name="sIdi">Idioma de usuario</param>   
	''' <returns>Si cumple con los obligatorios o no cumple</returns>
	''' <remarks>Llamada desde: service.asmx/RealizarValidacionesThread; Tiempo m�ximo:0,1</remarks>
	Public Function ComprobarCamposObligatorios(ByVal sIdi As String) As Boolean
		Dim bFaltan As Boolean = False
		For Each oGrupo As Grupo In moGrupos.Grupos
			For Each dr As DataRow In oGrupo.DSCampos.Tables(0).Select("OBLIGATORIO=1")
				Select Case CType(dr.Item("SUBTIPO"), TiposDeDatos.TipoGeneral)
					Case TiposDeDatos.TipoGeneral.TipoNumerico
						If IsDBNull(dr.Item("VALOR_NUM")) Then
							bFaltan = True
							Exit For
						End If
					Case TiposDeDatos.TipoGeneral.TipoFecha
						If IsDBNull(dr.Item("VALOR_FEC")) Then
							bFaltan = True
							Exit For
						End If
					Case TiposDeDatos.TipoGeneral.TipoBoolean
						If IsDBNull(dr.Item("VALOR_BOOL")) Then
							bFaltan = True
							Exit For
						End If
					Case TiposDeDatos.TipoGeneral.TipoDesglose
						Dim oCampo As Campo = New Campo(DBServer, mIsAuthenticated)
						oCampo.Id = dr.Item("ID")
						Dim ds As DataSet = CType(moCamposDesglose.Item("Campo" & dr.Item("ID").ToString()), Campo).Data
						If ds.Tables.Count < 3 Then
							bFaltan = True
							Exit For
						Else
							If ds.Tables(2).Rows.Count = 0 Then
								bFaltan = True
								Exit For
							End If
						End If
					Case Else
						If IsDBNull(dr.Item("VALOR_TEXT")) Then
							bFaltan = True
							Exit For
						End If
				End Select
			Next
			If bFaltan Then Exit For
		Next
		If Not bFaltan Then
			For Each oCampo As Campo In moCamposDesglose
				If oCampo.Data.Tables.Count > 2 Then
					For Each dr As DataRow In oCampo.Data.Tables(0).Select("OBLIGATORIO=1")
						For Each dr2 As DataRow In oCampo.Data.Tables(2).Select("CAMPO_HIJO=" & dr.Item("ID"))
							Select Case CType(dr.Item("SUBTIPO"), TiposDeDatos.TipoGeneral)
								Case TiposDeDatos.TipoGeneral.TipoNumerico
									If IsDBNull(dr2.Item("VALOR_NUM")) Then
										bFaltan = True
										Exit For
									End If
								Case TiposDeDatos.TipoGeneral.TipoFecha
									If IsDBNull(dr2.Item("VALOR_FEC")) Then
										bFaltan = True
										Exit For
									End If
								Case TiposDeDatos.TipoGeneral.TipoBoolean
									If IsDBNull(dr2.Item("VALOR_BOOL")) Then
										bFaltan = True
										Exit For
									End If
								Case Else
									If IsDBNull(dr2.Item("VALOR_TEXT")) Then
										bFaltan = True
										Exit For
									End If
							End Select
						Next
						If bFaltan Then Exit For
					Next
				End If
				If bFaltan Then Exit For
			Next
		End If
		If bFaltan Then
			Dim oDict As FSNServer.Dictionary = New FSNServer.Dictionary()
			ActualizarEstadoValidacion(FSNLibrary.EstadoValidacion.CamposObligatorios, oDict.Texto(TiposDeDatos.ModulosIdiomas.Aprobacion, 31, sIdi))
		End If
		Return Not bFaltan
	End Function
	Public Function ComprobarPrecondiciones(ByVal lAccion As Long, ByVal sIdi As String) As Boolean
		Dim sVariables() As String = Nothing
		Dim dValues() As Double = Nothing
		Dim i As Integer
		Dim oValorCampo As Object = Nothing
		Dim oValor As Object = Nothing
		Dim iTipo As TiposDeDatos.TipoGeneral
		Dim dsPrecondiciones As DataSet
		dsPrecondiciones = ObtenerPrecondicionesAccion(lAccion, sIdi)
		If dsPrecondiciones.Tables.Count > 0 Then
			Try
				i = 0
				Dim bCumple As Boolean = True
				For Each drPrecondicion As DataRow In dsPrecondiciones.Tables(0).Rows
					If drPrecondicion.Item("TIPO") = 2 Then
						Dim drCondiciones As DataRow() = drPrecondicion.GetChildRows("PRECOND_CONDICIONES")
						For Each oCond As DataRow In drCondiciones
							Dim drCampo As DataRow = Nothing
							ReDim Preserve sVariables(i)
							ReDim Preserve dValues(i)
							sVariables(i) = oCond.Item("COD")
							dValues(i) = 0
							Select Case oCond.Item("TIPO_CAMPO")
								Case 1 ' Campo de formulario
									For Each oGrupo As Grupo In moGrupos.Grupos
										If oGrupo.DSCampos.Tables(0).Select("ID_CAMPO=" & oCond.Item("CAMPO")).Length > 0 Then
											drCampo = oGrupo.DSCampos.Tables(0).Select("ID_CAMPO=" & oCond.Item("CAMPO"))(0)
											Exit For
										End If
									Next
									If Not drCampo Is Nothing Then
										Select Case drCampo.Item("SUBTIPO")
											Case 2
												oValorCampo = DBNullToSomething(drCampo.Item("VALOR_NUM"))
											Case 3
												oValorCampo = DBNullToSomething(drCampo.Item("VALOR_FEC"))
											Case 4
												oValorCampo = DBNullToSomething(drCampo.Item("VALOR_BOOL"))
											Case Else
												oValorCampo = DBNullToSomething(drCampo.Item("VALOR_TEXT"))
										End Select
										iTipo = drCampo.Item("SUBTIPO")
										'Si es del tipo 7 habr� que hacer una llamada al webservice para la validaci�n
										If oCond.Item("TIPO") = 7 Then
											Dim sUrl, nomResult, nomEntrada, campoId As String
											nomResult = String.Empty : nomEntrada = String.Empty
											Dim pair As KeyValuePair(Of String, FSNServer.ParametroServicio)
											'Desde aqu� no puedo acceder a PMWeb/Consultas.asmx as� que pongo el c�digo de su m�todo
											'Obtener_Servicio
											'params = WS_Consultas.Obtener_Servicio(oCond.Item("SERVICIO"), oCond.Item("CAMPO"))
											Dim paramEnt As DataTable
											Dim paramSal As DataTable
											Dim j As Integer

											Dim oParam As FSNServer.ParametroServicio
											Dim paramsEnt As New Dictionary(Of String, FSNServer.ParametroServicio)
											Dim paramsSal As New Dictionary(Of String, FSNServer.ParametroServicio)
											Dim oServicio As New Servicio(DBServer, mIsAuthenticated)
											If DBNullToStr(oCond.Item("CAMPO_ORIGEN")) = "" Then
												campoId = oCond.Item("CAMPO")
											Else
												campoId = oCond.Item("CAMPO_ORIGEN")
											End If
											oServicio.LoadData(oCond.Item("SERVICIO"), campoId, mlID)
											sUrl = oServicio.Data.Tables("URL").Rows(0).Item("URL")
											paramEnt = oServicio.Data.Tables("PARAM_ENTRADA")
											paramSal = oServicio.Data.Tables("PARAM_SALIDA")

											For j = 0 To paramEnt.Rows.Count - 1
												oParam = New FSNServer.ParametroServicio
												oParam.Den = paramEnt.Rows(j).Item("DEN")
												oParam.Directo = paramEnt.Rows(j).Item("DIRECTO")
												oParam.valorText = DBNullToStr(paramEnt.Rows(j).Item("VALOR_TEXT"))
												oParam.valorNum = DBNullToInteger(paramEnt.Rows(j).Item("VALOR_NUM"))
												oParam.valorFec = DBNullToSomething(paramEnt.Rows(j).Item("VALOR_FEC"))
												oParam.valorBool = DBNullToBoolean(paramEnt.Rows(j).Item("VALOR_BOOL"))
												paramsEnt(oParam.Den) = oParam
											Next
											For j = 0 To paramSal.Rows.Count - 1
												oParam = New FSNServer.ParametroServicio
												oParam.Den = paramSal.Rows(j).Item("DEN")
												oParam.IndError = paramSal.Rows(j).Item("INDICADOR_ERROR")
												oParam.Campo = paramSal.Rows(j).Item("CAMPO")
												paramsSal(oParam.Den) = oParam
											Next
											''''''''''''''''''''''''''''''''''''''

											For Each pair In paramsEnt
												nomEntrada = paramsEnt(pair.Key).Den
											Next
											For Each pair In paramsSal
												If paramsSal(pair.Key).IndError = 1 Then
													nomResult = paramsSal(pair.Key).Den
												End If
											Next
											oValorCampo = LlamarWSServicio(sUrl, nomEntrada, oValorCampo, nomResult)
										End If
									End If
								Case 2 'Peticionario
									oValorCampo = m_sPeticionario
									iTipo = TiposDeDatos.TipoGeneral.TipoTextoMedio
								Case 3 ' Departamento del peticionario
									Dim oPer As Persona = New Persona(mDBServer, mIsAuthenticated)
									oPer.LoadData(m_sPeticionario)
									oValorCampo = oPer.Departamento
									iTipo = TiposDeDatos.TipoGeneral.TipoTextoMedio
								Case 4 'UON del peticionario
									Dim oPer As Persona = New Persona(mDBServer, mIsAuthenticated)
									oPer.LoadData(m_sPeticionario)
									oValorCampo = oPer.UONs
									iTipo = TiposDeDatos.TipoGeneral.TipoTextoMedio
								Case 5 'N� de procesos de compra abiertos
									DevolverProcesosRelacionados()
									oValorCampo = moProcesos.Tables(0).Rows.Count
									iTipo = TiposDeDatos.TipoGeneral.TipoNumerico
								Case 6 'Importe adj
									oValorCampo = ObtenerImporteAdjudicado()
									iTipo = TiposDeDatos.TipoGeneral.TipoNumerico
								Case 7 'Importe de la solicitud de compra
									oValorCampo = m_dImporte
									iTipo = TiposDeDatos.TipoGeneral.TipoNumerico
							End Select

							Select Case oCond.Item("TIPO_VALOR")
								Case 1 ' Campo de formulario
									For Each oGrupo As Grupo In moGrupos.Grupos
										If oGrupo.DSCampos.Tables(0).Select("ID_CAMPO=" & oCond.Item("CAMPO_VALOR")).Length > 0 Then
											drCampo = oGrupo.DSCampos.Tables(0).Select("ID_CAMPO=" & oCond.Item("CAMPO_VALOR"))(0)
										End If
									Next
									If Not drCampo Is Nothing Then
										Select Case drCampo.Item("SUBTIPO")
											Case 2
												oValorCampo = DBNullToSomething(drCampo.Item("VALOR_NUM"))
											Case 3
												oValorCampo = DBNullToSomething(drCampo.Item("VALOR_FEC"))
											Case 4
												oValorCampo = DBNullToSomething(drCampo.Item("VALOR_BOOL"))
											Case Else
												oValorCampo = DBNullToSomething(drCampo.Item("VALOR_TEXT"))
										End Select
									End If
								Case 2, 10 'Valor est�tico
									Select Case iTipo
										Case TiposDeDatos.TipoGeneral.TipoNumerico
											oValor = DBNullToSomething(oCond.Item("VALOR_NUM"))
										Case TiposDeDatos.TipoGeneral.TipoFecha
											oValor = DBNullToSomething(oCond.Item("VALOR_FEC"))
										Case TiposDeDatos.TipoGeneral.TipoBoolean
											oValor = DBNullToSomething(oCond.Item("VALOR_BOOL"))
										Case Else
											If oCond.Item("TIPO_CAMPO") = 4 Then 'UON DEL PETICIONARIO
												Dim oUON() As String
												Dim iIndice As Integer
												oValor = Nothing
												oUON = Split(oCond.Item("VALOR_TEXT"), "-")
												For iIndice = 0 To UBound(oUON)
													If oUON(iIndice) <> "" Then
														oValor = oValor & Trim(oUON(iIndice)) & "-"
													End If
												Next
												oValor = Left(oValor, Len(oValor) - 1)
											Else
												oValor = DBNullToSomething(oCond.Item("VALOR_TEXT"))
											End If
									End Select
							End Select

							Select Case iTipo
								Case TiposDeDatos.TipoGeneral.TipoNumerico, TiposDeDatos.TipoGeneral.TipoFecha
									Select Case oCond.Item("OPERADOR")
										Case ">"
											dValues(i) = IIf(oValorCampo > oValor, 1, 0)
										Case "<"
											dValues(i) = IIf(oValorCampo < oValor, 1, 0)
										Case ">="
											dValues(i) = IIf(oValorCampo >= oValor, 1, 0)
										Case "<="
											dValues(i) = IIf(oValorCampo <= oValor, 1, 0)
										Case "="
											dValues(i) = IIf(oValorCampo = oValor, 1, 0)
										Case "<>"
											dValues(i) = IIf(oValorCampo = oValor, 1, 0)
									End Select
								Case TiposDeDatos.TipoGeneral.TipoBoolean
									dValues(i) = IIf(oValorCampo = oValor, 1, 0)
								Case Else
									Select Case UCase(oCond.Item("OPERADOR"))
										Case "="
											dValues(i) = IIf(UCase(oValorCampo) = UCase(oValor), 1, 0)
										Case "LIKE"
											If Left(oValor, 1) = "*" Then
												If Right(oValor, 1) = "*" Then
													oValor = oValor.ToString().Replace("*", "")
													dValues(i) = IIf(InStr(UCase(oValorCampo.ToString()), UCase(oValor.ToString())) >= 0, 1, 0)
												Else
													oValor = oValor.ToString().Replace("*", "")
													dValues(i) = IIf(UCase(oValorCampo.ToString()).EndsWith(UCase(oValor.ToString())), 1, 0)
												End If
											Else
												If Right(oValor, 1) = "*" Then
													oValor = oValor.ToString().Replace("*", "")
													dValues(i) = IIf(UCase(oValorCampo.ToString()).StartsWith(UCase(oValor.ToString())), 1, 0)
												Else
													dValues(i) = IIf(UCase(oValorCampo.ToString()) = UCase(oValor.ToString()), 1, 0)
												End If
											End If
										Case "<>"
											dValues(i) = IIf(UCase(oValorCampo.ToString()) <> UCase(oValor.ToString()), 1, 0)
									End Select
							End Select
							i += 1
						Next
						oValor = Nothing
						Try
							Dim iEq As New USPExpress.USPExpression
							iEq.Parse(drPrecondicion.Item("FORMULA"), sVariables)
							oValor = iEq.Evaluate(dValues)
						Catch ex As USPExpress.ParseException
						Catch e As Exception
						End Try
						If oValor > 0 Then
							ActualizarEstadoValidacion(FSNLibrary.EstadoValidacion.Precondiciones, drPrecondicion.Item("DEN"))
							Return False
						End If
					End If
				Next
			Catch ex As Exception
			End Try
		End If
		Return True
	End Function
	''' <summary>
	''' Comprueba si la asignaci�n ya est� asignada a otra persona y si no est� asignado a ninguna se asigna a la persona.
	''' </summary>
	''' <param name="CodPersona">Cod de la persona</param>
	''' <param name="sIdi">Idioma</param>
	''' <returns>True: La asignaci�n se ha realizado correctamente o est� asignado ya a esa persona; False: ya estaba asignada a otra persona</returns>
	''' <remarks>Llamada desde: Service.asmx/RealizarValidacionesThread; Tiempo m�ximo: 1 sg.</remarks>
	Public Function ComprobarAsignacion(ByVal CodPersona As String, ByVal sIdi As String) As Boolean
		DevolverEtapaActual(sIdi, CodPersona)
		Dim oRol As Rol = New Rol(mDBServer, mIsAuthenticated)
		oRol.Id = RolActual
		oRol.IdInstancia = mlID
		oRol.Load()
		If oRol.Persona = Nothing Then 'no est� asignado a ning�n usuario
			oRol.AsignarRol(CodPersona)
			ComprobarAsignacion = True
		Else
			If oRol.Persona <> CodPersona AndAlso oRol.Sustituto <> CodPersona Then 'est� asignado a otro usuario
				Dim oDict As FSNServer.Dictionary = New FSNServer.Dictionary()
				ActualizarEstadoValidacion(FSNLibrary.EstadoValidacion.YaAsignadaAOtroUsuario, oDict.Texto(TiposDeDatos.ModulosIdiomas.Aprobacion, 17, sIdi))
				ComprobarAsignacion = False
			Else
				ComprobarAsignacion = True
			End If
		End If
	End Function
	''' <summary>
	''' Comprobar si tiene siguiente etapa la instancia en la q estas para la accion q vas a realizar
	''' </summary>
	''' <param name="lAccion">accion q vas a realizar</param>
	''' <param name="CodPersona">persona q va a realizar la accion</param>
	''' <param name="sIdi">idioma de la persona</param>
	''' <param name="sRolPorWebService">Gestamp tiene 100 etapas a las q ir desde peticionario en varios flujos. Se quitan para q solo sea una etapa y un web 
	''' service desde integraci�n nos dice cuales son las personas de autoasignaci�n. Aqui se devuelve el rol. -1 indica No es el caso 100 etapas. Por si hay
	''' etapas en paralelo, lo q se devuelve es un string con los bloque@rol separados por comas.Ejemplo: -1 � 12596 � 12596,16324</param> 
	''' <returns>Siguiente etapa</returns>
	''' <remarks>Llamada desde: FSNWebService\Service.asmx.vb\RealizarValidacionesThread; tiempo m�ximo: 0,5 seg</remarks>
	Public Function ComprobarTieneSiguienteEtapa(ByVal lAccion As Long, ByVal CodPersona As String, ByVal sIdi As String,
					Optional ByRef sRolPorWebService As String = "-1") As DataSet
		Dim oEtapas As DataSet = DevolverSiguientesEtapas(lAccion, CodPersona, sIdi, sRolPorWebService:=sRolPorWebService)
		If Not (oEtapas.Tables.Count > 1) Then
			Dim oDict As FSNServer.Dictionary = New FSNServer.Dictionary()
			ActualizarEstadoValidacion(FSNLibrary.EstadoValidacion.SiguienteEtapa, oDict.Texto(TiposDeDatos.ModulosIdiomas.Aprobacion, 44, sIdi))
		End If

		ComprobarTieneSiguienteEtapa = oEtapas
	End Function
	''' <summary>
	''' Comprobar si tiene siguiente etapa la instancia en la q estas para la accion q vas a realizar
	''' </summary>
	''' <param name="lAccion">accion q vas a realizar</param>
	''' <param name="sIdi">idioma de la persona</param> 
	''' <returns>Siguiente etapa</returns>
	''' <remarks>Llamada desde: FSNWebService\Service.asmx.vb\RealizarValidacionesThread; tiempo m�ximo: 0,5 seg</remarks>
	Public Function DevolverSiguientesEtapasPortal(ByVal lAccion As Integer, ByVal sIdi As String, ByVal lBloque As Long, ByVal lRol As Long, Optional ByVal dsFactura As DataSet = Nothing) As DataSet
		Authenticate()
		Return DBServer.Instancia_ObtenerSiguientesEtapasPortal(mlID, lAccion, sIdi, lBloque, lRol, dsFactura)
	End Function
	Public Function ComprobarParticipantes(ByVal sIdi As String, ByVal lBloque As Long) As Boolean
		Dim oRol As Rol = New Rol(mDBServer, mIsAuthenticated)
		oRol.Id = miRolActual
		oRol.Bloque = lBloque
		oRol.CargarParticipantes(sIdi, mlID)
		Dim bParticipantes As Boolean = True
		For Each dr As DataRow In oRol.Participantes.Tables(0).Rows
			bParticipantes = bParticipantes And Not IsDBNull(dr.Item("PART"))
			If Not bParticipantes Then
				Dim oDict As FSNServer.Dictionary = New FSNServer.Dictionary()
				ActualizarEstadoValidacion(FSNLibrary.EstadoValidacion.ProximosParticipantes, oDict.Texto(TiposDeDatos.ModulosIdiomas.Aprobacion, 35, sIdi))
				Exit For
			End If
		Next
		Return bParticipantes
	End Function
	Public Function ComprobarBloqueosSolicitudesPadre(ByVal sIdi As String, ByVal oNumberFormat As System.Globalization.NumberFormatInfo) As Boolean
		Dim bBloqueos As Boolean = True
		For Each oGrupo As Grupo In moGrupos.Grupos
			For Each dr As DataRow In oGrupo.DSCampos.Tables(0).Select("TIPO_CAMPO_GS=128 AND AVISOBLOQUEOIMPACUM=0 AND NOT VALOR_NUM IS NULL")
				Dim ds As DataSet = DevolverSolicitudPadre(sIdi, dr.Item("VALOR_NUM"))
				If ds.Tables(0).Rows.Count > 0 Then
					bBloqueos = bBloqueos And (ds.Tables(0).Rows(0).Item("Disponible") - m_dImporte) >= 0
					If Not bBloqueos Then
						Dim oDict As FSNServer.Dictionary = New FSNServer.Dictionary()
						oDict.LoadData(TiposDeDatos.ModulosIdiomas.GuardarInstancia, sIdi)
						With oDict.Data.Tables(0)
							ActualizarEstadoValidacion(FSNLibrary.EstadoValidacion.ControlImportes, .Rows(2).Item(1) & " " &
								ds.Tables(0).Rows(0).Item("DESCR") & vbCrLf &
								.Rows(4).Item(1) & " = " & CDbl(ds.Tables(0).Rows(0).Item("Importe") - ds.Tables(0).Rows(0).Item("Disponible") + m_dImporte).ToString("N", oNumberFormat) & " " & DBNullToStr(ds.Tables(0).Rows(0).Item("Moneda")) & vbCrLf &
								.Rows(5).Item(1) & " = " & CDbl(ds.Tables(0).Rows(0).Item("Importe")).ToString("N", oNumberFormat) & " " & DBNullToStr(ds.Tables(0).Rows(0).Item("Moneda")))
							Exit For
						End With
					End If
				End If
			Next
			If Not bBloqueos Then Exit For
		Next
		Return bBloqueos
	End Function
	''' <summary>
	''' Procedimiento que realiza las validaciones de integraci�n para el caso de aprobaci�n desde el visor 
	''' </summary>
	''' <param name="sIdi">Idioma del usuario </param>
	''' <param name="sPer">Codigo de la persona que lanza la acci�n sobre la solicitud</param>
	''' <param name="lAccion">Identificador de la acci�n</param> 
	''' <param name="dsSiguienteEtapa">Dataset con los identificadores de las siguientes etapas</param> 
	''' <remarks>
	''' Llamada desde: FSNWebService\Service.asm.vb: RealizarValidacionesThread
	''' tiempo m�ximo: 1 seg (variable en funci�n de las validaciones implementadas en la mapper, accesos externos, etc)</remarks>
	Public Function ComprobarMapper(ByVal sIdi As String, ByVal sPer As String, ByVal lAccion As Long, ByVal dsSiguienteEtapa As DataSet) As Boolean
		Dim bMapper As Boolean = moSolicitud.ValidacionesIntegracion.ControlaConMaper()
		If bMapper Then
			Dim oErrorMapper As Object = ControlMapper(sIdi, sPer, lAccion, dsSiguienteEtapa)
			If Not oErrorMapper Is Nothing Then
				Dim TextosGS As TextosGS = New TextosGS(mDBServer, mIsAuthenticated)
				Dim iNumError As Integer = CInt(oErrorMapper.iNumError)
				Dim MapperStr As String
				If iNumError = -100 Then
					MapperStr = oErrorMapper.strError
				Else
					MapperStr = TextosGS.MensajeError(FSNLibrary.MapperModuloMensaje.PedidoDirecto, iNumError, sIdi, oErrorMapper.strError)
					If MapperStr = "" Then 'no se han podido ejecutar las validaciones, int�ntelo m�s tarde
						MapperStr = TextosGS.MensajeError(FSNLibrary.MapperModuloMensaje.PedidoDirecto, 134, sIdi, oErrorMapper.strError)
					End If
				End If

				ActualizarEstadoValidacion(FSNLibrary.EstadoValidacion.Integracion, MapperStr)
				Return False

				Exit Function
			End If
		End If
		Return True
	End Function
	''' <summary>
	''' Procedimiento que actualiza el estad de validaci�n
	''' </summary>
	''' <param name="iEstado">Id del estado</param>
	''' <param name="sError">Codigo del error</param>
	''' <remarks>
	''' Llamada desde: PmServer/Instancia/ComprobarCamposObligatorios, PmServer/Instancia/ComprobarValidaciones, PmServer/Instancia/ActualizarSiguienteEtapa,PmServer/Instancia/ComprobarAsignacion, PmServer/Instancia/ComprobarParticipanes, PmServer/Instancia/ComprobarBloqueosSolicitudesPadre, PmServer/Instancia/ComprobarMapper, WebServicePM/Service.asmx/ActualizarEstadoValidacion,PmServer/Instancia/ActualizarEstadoValidacion, WebServicePm/service.asmx/EjecutarAcciones, WebServicePm/service.asmx/RealizarValidacionesThread, WebServicePm/service.asmx/EjecutarAcciones
	''' tiempo m�ximo: 1 seg</remarks>
	Public Sub ActualizarEstadoValidacion(ByVal iEstado As EstadoValidacion, ByVal sError As String)
		Authenticate()
		m_iEstadoValidacion = iEstado
		m_sErrorValidacion = sError
		DBServer.Instancia_ActualizarEstadoValidacion(mlID, iEstado, sError)
	End Sub
	''' <summary>
	''' Funcion que devuelve la solicitud Padre de una instancia
	''' </summary>
	''' <param name="sIdi">Idioma de la aplicaci�n</param>
	''' <param name="idSolPadre">Id de la solicitud padre</param>
	''' <returns>Un DataSet con los datos de la solicitud padre de una instancia</returns>
	''' <remarks>
	''' Llamada desde: PmServer/Instancia/ComprobarBloqueosSolicitudesPadre 
	''' Tiempo m�ximo: 0,5 seg</remarks>
	Public Function DevolverSolicitudPadre(ByVal sIdi As String, ByVal idSolPadre As Integer) As DataSet
		Authenticate()
		Return DBServer.Instancias_Padre(sIdi:=sIdi, idSolPadre:=idSolPadre, lInstancia:=mlID)
	End Function
	''' <summary>
	''' Notificar Error Validacion
	''' </summary>
	''' <param name="sIdi">Idioma</param>
	''' <param name="sUserEmail">Email del usuario</param>
	''' <param name="TipoEmail">HTML / Texto</param>
	''' <param name="NumberFormat">Formato numerico</param>
	''' <param name="DateFmt">Formato fecha</param>
	''' <remarks>Llamada desde: FSNWebService\Service.asmx.vbd; Tiempo m�ximo: 0,3 seg</remarks>
	Public Sub NotificarErrorValidacion(ByVal sIdi As String, ByVal sUserEmail As String, ByVal TipoEmail As Integer,
										ByVal NumberFormat As System.Globalization.NumberFormatInfo, ByVal DateFmt As String)
		Dim sSubject As String
		Dim oDict As FSNServer.Dictionary = New FSNServer.Dictionary()
		sSubject = oDict.Texto(TiposDeDatos.ModulosIdiomas.Otros, 27, sIdi)
		Dim oNotificador As New Notificar(DBServer, mIsAuthenticated)
		oNotificador.NotificacionErrorValidacion(sUserEmail, sSubject, sIdi, TipoEmail, mlID, DevolverTitulo(sIdi), m_dFecAlta, m_dImporte,
												 m_sMoneda, m_iEstadoValidacion, m_sErrorValidacion, NumberFormat, DateFmt)
		oNotificador.FuerzaFinalizeSmtpClient()
		oNotificador = Nothing
	End Sub
	''' <summary>
	''' Funcion que devuelve si una instancia tiene sustitucion de destinatario
	''' </summary>
	''' <param name="sPer">Codigo de persona</param>
	''' <param name="sDestinatario">codigo de destinatario</param>
	''' <returns>Una variable booleana que indica si existe un sustituto de destinatario</returns>
	''' <remarks>
	''' Llamada desde: PmWeb/gestiontrasladada/Page_load
	''' Tiempo m�ximo: 0,3 seg</remarks>
	Public Function EsSustitutodeDestinatario(ByVal sPer As String, ByVal sDestinatario As String) As Boolean
		Authenticate()
		Return DBServer.Instancia_EsSustitutodeDestinatario(sPer, sDestinatario)
	End Function
	''' <summary>
	''' Revisado por: Sandra. Fecha: 11/03/2011
	''' Funcion que devuelve si un usuario es peticionario del flujo del baja/modif del contrato
	''' </summary>
	''' <param name="lSolicitud">Id de la solicitud</param>
	''' <param name="lTipoWorkflow">Tipo de workflow (baja/modif)</param>
	''' <param name="sPer">Codigo de persona</param>
	''' <returns>Una variable booleana que indica si es o no peticionario</returns>
	''' <remarks>
	''' Llamada desde: PmWeb/detalleContrato/ConfigurarCabecera
	''' Tiempo m�ximo: 0,1 seg</remarks>
	Public Function EsPeticionarioFlujo(ByVal lSolicitud As Long, ByVal lTipoWorkflow As Long, ByVal sPer As String) As Object
		Authenticate()
		Return DBServer.Instancia_EsPeticionarioFlujo(lSolicitud, lTipoWorkflow, sPer)
	End Function
	''' <summary>
	''' Revisado por: MPF Fecha: 21/08/2012
	''' Funcion que devuelve si un usuario es peticionario del flujo del baja/modif del contrato</summary>
	''' <param name="iContrato">Id del contrato</param>
	''' <param name="iEmpresa">Sociedad de la empresa </param>
	''' <param name="sPerfil">String con el tipo de perfil en este caso pueden ser "Consulta" '"Gestor" "Administrador" "Auditor"</param>
	''' <param name="sCodUsu">c�digo del usuario</param>
	''' <returns>Una variable booleana que indica si es o no peticionario</returns>
	''' <remarks>
	''' Llamada desde: PmWeb/detalleContrato.aspx.vb
	''' Tiempo m�ximo: seg</remarks>
	Public Function EsPeticionarioFlujo_LCX(ByVal iContrato As Integer, ByVal iEmpresa As Integer, ByVal sPerfil As String, ByVal sCodUsu As String) As Boolean
		Authenticate()
		Return DBServer.Instancia_EsPeticionarioFlujo_LCX(iContrato, iEmpresa, sPerfil, sCodUsu)
	End Function
	''' <summary>
	''' Nos devuelva de las tablas INSTANCIA y VERSION_INSTANCIA el valor de los campos EN_PROCESO, 
	''' NUM_VERSION,  TIPO de la �ltima versi�n de VERSION_INSTANCIA y si el Certificado sigue siendo 
	''' el activo para el proveedor (Si existe en la tabla PROVE_CERTIF)
	''' </summary>
	''' <param name="lCertificado">Id de certificado</param>
	''' <remarks>Llamada desde: controlarVersionQA.aspx; Tiempo m�ximo: 0,1</remarks>
	Public Sub ComprobarGrabacionCertif(ByVal lCertificado As Long)
		Authenticate()

		Dim data As DataSet
		Dim i As Integer
		data = DBServer.Instancia_ComprobarGrabacionCertif(mlID, lCertificado)

		For i = 0 To data.Tables(0).Rows.Count - 1 'EN REALIDAD SOLO DEVOLVER� UNA FILA
			m_lVersion = DBNullToSomething(data.Tables(0).Rows(i).Item("NUM_VERSION"))
			m_iEnProceso = DBNullToSomething(data.Tables(0).Rows(i).Item("EN_PROCESO"))
			m_iTipoVersion = DBNullToSomething(data.Tables(0).Rows(i).Item("VITIPO"))
			m_bCertifActivo = SQLBinaryToBoolean(data.Tables(0).Rows(i).Item("HAY_CERTIF"))
		Next
	End Sub
	''' <summary>
	''' Cargar las propiedades de la Instancia: EN_PROCESO, NUM_VERSION, TIPO de la �ltima versi�n 
	''' de VERSION_INSTANCIA.
	''' </summary>
	''' <remarks>Llamada desde: controlarVersionQA.aspx/ComprobacionesNoConform; Tiempo m�ximo: 0,1</remarks>
	Public Sub ComprobarGrabacionNC()
		Authenticate()

		Dim data As DataSet
		data = DBServer.Instancia_ComprobarGrabacionNC(mlID)

		If Not data.Tables.Count = 0 AndAlso Not data.Tables(0).Rows.Count = 0 Then
			m_lVersion = DBNullToSomething(data.Tables(0).Rows(0).Item("NUM_VERSION"))
			m_iEnProceso = DBNullToSomething(data.Tables(0).Rows(0).Item("EN_PROCESO"))
			m_iTipoVersion = DBNullToSomething(data.Tables(0).Rows(0).Item("VITIPO"))
		End If

		data = Nothing
	End Sub
	''' <summary>
	''' Funcion que devuelve un DataSet con las lineas del desglose indicado para la ultima version de la instancia dada
	''' Devuelve la estructura del desglose (subtipo, intro y campo_origen), denominacion de columnas y los datos del desglose  
	''' </summary>
	''' <param name="lIdDesgloseOrigen">Id del desglose Origen</param> 
	''' <param name="sIdi">Idioma de la aplicaci�n</param>
	''' <param name="lIdVinculado">Id de la vinculaci�n (DESGLOSE_VINCULADO.ID)</param> 
	''' <returns>Un DataSet con las lineas del desglose indicado para la ultima version de la instancia dada</returns>
	''' <remarks>Llamada desde: BuscadorSolicitudes.aspx/BuscarLineas; Tiempo m�ximo: 0,1 seg</remarks>
	Public Function GetLineasAVincular(ByVal lIdDesgloseOrigen As Long, ByVal sIdi As String, ByVal lIdVinculado As Long) As DataSet
		Authenticate()
		Return DBServer.GetLineasVincular(mlID, lIdDesgloseOrigen, sIdi, lIdVinculado)
	End Function
	''' <summary>
	''' En el caso de que la l�nea seleccionada, se hubiese vinculado ya a alguna l�nea de 
	''' otra solicitud, en el caso de la SGNTJ se realizar� la siguiente validaci�n a medida:
	''' Se comprobar� que la suma de las cantidades (campo de sistema cantidad) consumidas en cada una de las l�neas, no supere o 
	''' haya alcanzado la cantidad de la l�nea origen
	''' </summary>
	''' <param name="bSacarTexto">False- saber si supera o no True- Texto con instancias vinculadas</param> 
	''' <param name="lIdDesgloseOrigen">Id del desglose Origen</param>
	''' <param name="iLinea">l�nea seleccionada Origen</param>
	''' <param name="sIdi">Idioma de la aplicaci�n</param> 
	''' <param name="sFormatoFecha">Formato fecha usuario</param>  
	''' <returns>Si se ha superado o alcanzado la cantidad de la l�nea origen � Texto con instancias vinculadas</returns>
	''' <remarks>Llamada desde: controlarLineaVinculada.aspx/Page_Load; Tiempo m�ximo: 0,1 seg</remarks>
	Public Function ComprobarCantidadesVincular(ByVal bSacarTexto As Boolean, ByVal lIdDesgloseOrigen As Long, ByVal iLinea As Integer,
												ByVal sIdi As String, ByVal sFormatoFecha As String, Optional ByVal InstanciaVinc As Long = 0,
												Optional ByVal lCantVinc As Double = 0) As String
		Authenticate()

		Dim ds As DataSet = DBServer.ComprobarCantidadesVincular(bSacarTexto, mlID, lIdDesgloseOrigen, iLinea, sIdi, sFormatoFecha)

		If Not bSacarTexto Then
			If DBNullToDbl(ds.Tables(0).Rows(0)("CANTLINEA")) > 0 Then
				Return "OK"
			Else
				Return "KO"
			End If
		Else
			Dim dblCantCtrolar As Double = DBNullToDbl(ds.Tables(0).Rows(0)("CANTORIGEN"))

			Dim dblSumCantVinc As Double = 0
			Dim sSumCantVinc As String = CStr(dblCantCtrolar) & "@"

			Dim bRespuesta As Boolean = True

			For Each row As DataRow In ds.Tables(1).Rows
				dblSumCantVinc = dblSumCantVinc + row("CANTVINCULADA")

				If InstanciaVinc > 0 Then
					If row("VINC") = InstanciaVinc Then
						sSumCantVinc = sSumCantVinc & row("DENINSTANCIA") & "|" & CStr(lCantVinc) & "#"
					Else
						sSumCantVinc = sSumCantVinc & row("DENINSTANCIA") & "|" & CStr(row("CANTVINCULADA")) & "#"
					End If
				Else
					sSumCantVinc = sSumCantVinc & row("DENINSTANCIA") & "|" & CStr(row("CANTVINCULADA")) & "#"
				End If

				If dblSumCantVinc >= dblCantCtrolar Then
					bRespuesta = False
				End If
			Next

			Return sSumCantVinc
		End If
	End Function
	''' <summary>
	''' Funcion que devuelve un DataSet con la linea indicada del desglose indicado para la ultima version de la instancia dada
	''' Devuelve la estructura del desglose (subtipo, intro y campo_origen), denominacion de columnas y los datos del desglose  
	''' </summary>
	''' <param name="lIdDesgloseOrigen">Id del desglose Origen</param> 
	''' <param name="iLinea">Linea a mostrar</param> 
	''' <param name="sIdi">Idioma de la aplicaci�n</param>
	''' <returns>Un DataSet con la linea indicada del desglose indicado para la ultima version de la instancia dada</returns>
	''' <remarks>Llamada desde: DetalleSolicitudes.aspx/CargarGrid; Tiempo m�ximo: 0,1 seg</remarks>
	Public Function GetLineaVinculada(ByVal lIdDesgloseOrigen As Long, ByVal iLinea As Integer, ByVal sIdi As String) As DataSet
		Authenticate()
		Return DBServer.GetLineasVincular(mlID, lIdDesgloseOrigen, sIdi, 0, iLinea)
	End Function
	''' <summary>
	''' Indica si algun desglose de la instancia tiene vinculaciones a otro/s desglose/s
	''' </summary>
	''' <returns>si algun desglose dela instancia tiene vinculaciones o no </returns>
	''' <remarks>Llamada desde: NWGestionInstancia.aspx/form_load; Tiempo maximo: 0,1</remarks>
	Public Function ExisteVinculaciones() As Boolean
		Authenticate()
		Return DBServer.Instancia_ExisteVinculaciones(mlID)
	End Function
	''' <summary>
	''' En el caso de que la l�nea seleccionada, se hubiese vinculado ya a alguna l�nea de 
	''' otra solicitud, en el caso de la SGNTJ se realizar� la siguiente validaci�n a medida:
	''' Se comprobar� que la suma de las cantidades (campo de sistema cantidad) consumidas en cada una de las l�neas, no supere o 
	''' haya alcanzado la cantidad de la l�nea origen
	''' </summary>
	''' <param name="sDatosOrigen">Instancia origen @ Desglose Origen (form_campo.id) @ linea origen</param> 
	''' <param name="lCantidad">Cantidad de la l�nea</param>  
	''' <returns>Si se ha superado o alcanzado la cantidad de la l�nea origen para la instancia q se intentar grabar</returns>
	''' <remarks>Llamada desde: controlarLineaVinculada.aspx/Page_Load; Tiempo m�ximo: 0,1 seg</remarks>
	Public Function ComprobarEmitirCantidadesVincular(ByVal sDatosOrigen As String, ByVal lCantidad As Double) As String
		Authenticate()

		Dim spli() As String = sDatosOrigen.Split("@")
		If UBound(spli) = 0 Then
			Return "OK"
		Else
			Dim ds As DataSet = DBServer.ComprobarEmitirCantidadesVincular(spli(0), spli(1), spli(2), mlID, lCantidad)
			If DBNullToDbl(ds.Tables(0).Rows(0)("CANTLINEA")) >= 0 Then
				Return "OK"
			Else
				Return "KO"
			End If
		End If
	End Function
	Public Function AccionSinControlDisponible(ByVal lIdAccion As Long) As Boolean
		Authenticate()
		Return DBServer.Instancia_AccionSinControlDisponible(lIdAccion)
	End Function
	''' <summary>
	''' Indica si la linea indicada del desglose indicado tiene vinculaciones como origen a otro/s desglose/s 
	''' </summary>
	''' <param name="lIdDesglose">Id del desglose</param> 
	''' <param name="iLinea">Linea</param>  
	''' <returns>si algun desglose dela instancia tiene vinculaciones o no </returns>
	''' <remarks>Llamada desde: NWGestionInstancia.aspx/form_load; Tiempo maximo: 0,1</remarks>
	Public Function ExisteLineaVinculacion(ByVal lIdDesglose As Long, ByVal iLinea As Integer) As Boolean
		Authenticate()

		Return DBServer.Instancia_ExisteLineaVinculacion(mlID, lIdDesglose, iLinea)
	End Function
	Private Function LlamarWSServicio(ByVal sUrl As String, ByVal nomEntrada As String, ByVal valor As String, ByVal nomResult As String) As String
		Dim valorResult As String = String.Empty
		Dim url, metodo, sSoapAction, sXml As String
		Dim num As Integer

		num = sUrl.IndexOf(".asmx")
		url = sUrl.Substring(0, num + 5)
		metodo = sUrl.Substring(num + 6)
		sSoapAction = "http://tempuri.org/" + metodo

		sXml = "<?xml version=""1.0"" encoding=""utf-8""?>"
		sXml = sXml + "<soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">"
		sXml = sXml + "<soap:Body>"
		sXml = sXml + "<" + metodo + " xmlns=""http://tempuri.org"">"
		sXml = sXml + "<" + nomEntrada + ">" + valor + "</" + nomEntrada + ">"
		sXml = sXml + "</" + metodo + ">"
		sXml = sXml + "</soap:Body>"
		sXml = sXml + "</soap:Envelope>"

		Dim targetURI As New Uri(url)
		Dim req As System.Net.HttpWebRequest = DirectCast(System.Net.WebRequest.Create(targetURI), System.Net.HttpWebRequest)
		req.Headers.Add("SOAPAction", sSoapAction)
		req.ContentType = "text/xml; charset=""utf-8"""
		req.Accept = "text/xml"
		req.Method = "POST"
		Dim stm As System.IO.Stream = req.GetRequestStream
		Dim stmw As New System.IO.StreamWriter(stm)
		stmw.Write(sXml)
		stmw.Close()
		stm.Close()

		'Se realiza la llamada al WS
		Dim response As System.Net.WebResponse
		response = req.GetResponse()
		Dim responseStream As System.IO.Stream = response.GetResponseStream

		Dim sr As New System.IO.StreamReader(responseStream)
		Dim soapResult As String
		'Recogemos la respuesta del WS
		soapResult = sr.ReadToEnd
		Dim xmlResponse As New System.Xml.XmlDocument
		xmlResponse.LoadXml(soapResult)
		Dim xmlResult As System.Xml.XmlNode
		xmlResult = xmlResponse.GetElementsByTagName(nomResult).Item(0)
		If Not xmlResult Is Nothing Then
			'Recojo el valor del indicador de error
			valorResult = xmlResult.InnerText
		End If
		Return valorResult
	End Function
	''' <summary>
	''' Updatar los participantes obtenidos por el webservice. Tarea 3400
	''' </summary>
	''' <param name="oDTPMParticipantes">los participantes obtenidos por el webservice. Tarea 3400</param> 
	''' <remarks>Llamada desde: FSNWebService/Service.asmx.vb; Tiempo maximo:0</remarks>
	Public Sub Actualizar_ParticipantesExternos(ByVal oDTPMParticipantes As DataTable)
		Authenticate()
		DBServer.Actualizar_ParticipantesExternos(mlID, oDTPMParticipantes)
	End Sub
	''' Revisado por: ILG; Fecha: 13/02/2015
	''' <summary>
	''' Devuelve el codigo de la persona que actua como peticionaria. Si el peticionario tiene permiso en VIEW_ROL_PETICIONARIO para la solicitud dada devolvera el peticionario, sino, devolvera el codigo de la persona a la que sustituye
	''' </summary>
	''' <param name="IdInstancia"></param>
	''' <returns>Codigo de persona que actua como peticionario para el calculo de condiciones</returns>
	''' <remarks></remarks>
	Public Function GetPeticionario_Sustitucion(ByVal IdInstancia As Long) As String
		Authenticate()
		Return DBServer.GetPeticionario_Sustitucion(IdInstancia)
	End Function
	''' <summary>
	''' Devuelve el codigo de la persona que actua como peticionaria. Se coge el valor PER de PMCOPIA_ROL para tener el caso de peticionarios que se han dado de baja
	''' </summary>
	''' <param name="IdInstancia"></param>
	''' <returns>Codigo de persona que est� como peticionario en PM_COPIA_ROL</returns>
	''' <remarks></remarks>
	Public Function GetPeticionario_Rol(ByVal IdInstancia As Long) As String
		Authenticate()
		Return DBServer.GetPeticionario_Rol(IdInstancia)
	End Function
	Public Function ComprobarLineasAsociadas(ByVal idDesglose As Long, ByVal numLinea As Integer) As Boolean
		Authenticate()
		Return DBServer.Instancia_ComprobarLineasAsociadas(idDesglose, numLinea)
	End Function
	Public Function Obtener_Info_Procesamiento(ByVal lIDTiempoProc As Long) As Integer
		Authenticate()
		Dim result As SqlClient.SqlDataReader = Nothing
		DBServer.Instancia_Obtener_Info_Procesamiento(lIDTiempoProc, result)

		If result.Read Then
			If String.IsNullOrEmpty(result("FEC_INI_BD").ToString) Then
				Return TipoErrorProcesamiento.SinIniciarProceso
			Else
				If String.IsNullOrEmpty(result("FEC_FIN").ToString) Then
					Return TipoErrorProcesamiento.EnProceso
				Else
					If String.IsNullOrEmpty(result("ERROR").ToString) Then
						Return TipoErrorProcesamiento.Procesada
					Else
						Return TipoErrorProcesamiento.ProcesoFallido
					End If
				End If
			End If
		Else
			Throw New Exception("Registro de Tiempo de procesamiento no encontrado: " & lIDTiempoProc)
		End If

		If Not result Is Nothing AndAlso Not result.IsClosed Then result.Close()
	End Function
	Public Function ComprobarEtapasEnParaleloEnProceso(ByVal lBloque As Long) As Boolean
		Authenticate()
		Return DBServer.Instancia_ComprobarEtapasEnParaleloEnProceso(mlID, lBloque)
	End Function
	Public Function Get_Peticionario_Instancia() As String
		Authenticate()
		Return DBServer.Instancia_Get_Peticionario(mlID)
	End Function
	Public Function ObtenerCamposImporteTotal(ByVal lIdsDesglose As String) As DataTable
		Authenticate()
		Return DBServer.ObtenerCamposImporteTotald(lIdsDesglose)
	End Function
	Public Function GetCampoDef(ByVal IdCampoDef As Integer) As DataRow
		Authenticate()
		Return DBServer.ObtenerCampoDef(IdCampoDef)
	End Function
End Class