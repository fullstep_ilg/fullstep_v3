'''VERSION QA 3.0.600 - EPB
<Serializable()> _
Public Class Ponderacion
    Inherits Security

    Private mdblPuntos As Object
    Private mlIDCalif As Long
    Private msCalif As String
    Private mdFecha As DateTime
    Private msVariableXi As String  'Almacena el valor de las incognitas de la formula
    '                               'Ejemplo: x1= 6;575,576,580,581.../x2=2;1210,/x3... (No Conformidades)
    Private miTipoError As Integer  'En el caso de las variables de tipo no conformidad
    '                                , PPM, tasa de servicio o cargo a proveedores
    '                                , nos pueden faltar los valores Facturaci�n total, Objetivos o suelos. (Para identificar el error (El valor de la X;X1->1;X2->2...)
    Private msDenError As String    'Guardaremos tambi�n el error que se produzca al calcular la puntuaci�n
    Private miVarXError As Integer
    Private msValoresFormulaError As String
    Private miIdFormulaError As Integer

    Property IDCalif() As Long
        Get
            IDCalif = mlIDCalif
        End Get
        Set(ByVal Value As Long)
            mlIDCalif = Value
        End Set
    End Property

    Property Calificacion() As String
        Get
            Calificacion = msCalif
        End Get
        Set(ByVal Value As String)
            msCalif = Value
        End Set
    End Property

    Property Fecha() As DateTime
        Get
            Fecha = mdFecha
        End Get
        Set(ByVal Value As DateTime)
            mdFecha = Value
        End Set
    End Property

    Property Puntuacion() As Object
        Get
            Puntuacion = mdblPuntos
        End Get
        Set(ByVal Value As Object)
            mdblPuntos = Value
        End Set
    End Property

    Property VariablesXi() As String
        Get
            VariablesXi = msVariableXi
        End Get
        Set(ByVal Value As String)
            msVariableXi = Value
        End Set
    End Property

    Property TipoError() As Integer
        Get
            TipoError = miTipoError
        End Get
        Set(ByVal Value As Integer)
            miTipoError = Value
        End Set
    End Property

    Property DenError() As String
        Get
            DenError = msDenError
        End Get
        Set(ByVal Value As String)
            msDenError = Value
        End Set
    End Property

    Property VarXError() As Integer
        Get
            VarXError = miVarXError
        End Get
        Set(ByVal Value As Integer)
            miVarXError = Value
        End Set
    End Property

    Property ValoresFormulaError() As String
        Get
            ValoresFormulaError = msValoresFormulaError
        End Get
        Set(ByVal Value As String)
            msValoresFormulaError = Value
        End Set
    End Property

    Property IdFormulaError() As Integer
        Get
            IdFormulaError = miIdFormulaError
        End Get
        Set(ByVal Value As Integer)
            miIdFormulaError = Value
        End Set
    End Property

    ''' <summary>
    ''' Devuelve la puntuacion de las variable de calidad de tipo Integ
    ''' </summary>
    ''' <param name="sProve">Codigo de proveedor</param>
    ''' <param name="lIdUNQA">Id Unidad Negocio</param>        
    ''' <param name="lVarCal">ID Variable Calidad</param>   
    ''' <param name="iNivel">Nivel Variable</param>
    ''' <param name="sIdi">Codigo Idioma para la denominacion calificacion</param>       
    ''' <remarks>Llamada desde=QA_Puntuaciones.vb -->CalculoPonderacion; Tiempo m�ximo=0,1</remarks>
    Public Sub PonderacionIntegracion(ByVal sProve As String, ByVal lIdUNQA As Long, ByVal lVarCal As Integer, ByVal iNivel As Short, ByVal sIdi As String)
        Dim oDS As DataSet
        Dim sFormula As String
        Dim iTipoPond As Short
        Dim dValor As Nullable(Of Double)
        Dim iEq As New USPExpress.USPExpression
        Dim sVariables() As String
        Dim dValues() As Double
        Dim sVariableX As String = ""

        Try
            Authenticate()

            oDS = DBServer.Ponderacion_GetWeighValueINT(lVarCal, iNivel, sProve, lIdUNQA, sIdi)

            If oDS.Tables(0).Rows.Count > 0 Then
                If IsDBNull(oDS.Tables(0).Rows(0).Item("VALOR_INT")) Then
                    mdblPuntos = oDS.Tables(0).Rows(0).Item("VALOR_DEF")
                    ObtenerCalificacion(lVarCal, iNivel, sIdi)
                Else
                    sFormula = DBNullToSomething(oDS.Tables(0).Rows(0).Item("FORMULA"))
                    miIdFormulaError = DBNullToSomething(oDS.Tables(0).Rows(0).Item("ID_FORMULA"))
                    iTipoPond = DBNullToSomething(oDS.Tables(0).Rows(0).Item("TIPOPOND"))
                    dValor = oDS.Tables(0).Rows(0).Item("VALOR_INT")
                    mdblPuntos = oDS.Tables(0).Rows(0).Item("POND")
                    mlIDCalif = DBNullToSomething(oDS.Tables(0).Rows(0).Item("IDCAL"))
                    msCalif = DBNullToSomething(oDS.Tables(0).Rows(0).Item("CAL"))

                    If iTipoPond = 5 And dValor.HasValue Then 'Formula
                        ReDim sVariables(0)
                        ReDim dValues(0)
                        sVariables(0) = "X"
                        dValues(0) = dValor
                        msValoresFormulaError = dValues(0) & ";"
                        sVariableX = sVariableX + "X" + "=" + CStr(dValues(0)) + "/"

                        If sFormula <> Nothing Then
                            Try
                                iEq.Parse(sFormula, sVariables)
                                mdblPuntos = iEq.Evaluate(dValues)
                                msVariableXi = sVariableX
                                mlIDCalif = Nothing
                                msCalif = Nothing
                                miTipoError = 0
                            Catch ex0 As Exception
                                miTipoError = 6
                                miVarXError = 0
                                msDenError = ex0.Message
                            End Try
                        End If
                        ObtenerCalificacion(lVarCal, iNivel, sIdi)

                    End If
                End If
                End If
            oDS = Nothing
        Catch ex As Exception
            miTipoError = 6
            miVarXError = 0
            msDenError = ex.Message
        Finally
            oDS = Nothing
        End Try
    End Sub
    ''' <summary>
    ''' Obtiene la puntuacionen de las variable de calidad simple de tipo NoConformidad
    ''' </summary>
    ''' <param name="sProve">C�digo proveedor</param>    
    ''' <param name="lVarcal">ID variable Calidad</param>      
    ''' <param name="iNivel">Nivel de la variable</param>
    ''' <param name="lIdUNQA">Id Unidad de negocio</param>
    ''' <param name="lNivelUnqa">Nivel Unidad de negocio</param>
    ''' <param name="iTipoPond">Tipo ponderacion (Media, suma,...)</param>
    ''' <param name="sIdi">Cod Idioma para obtener la denominacion Calificacion</param>
    ''' <remarks>Llamada desde=QA_Puntuaciones.vb; Tiempo m�ximo=1seg.</remarks>
    Public Sub PonderacionNoConformidad(ByVal sProve As String, ByVal lVarCal As Integer, ByVal iNivel As Short, ByVal lIdUNQA As Long, ByVal lNivelUnqa As Long, ByVal iTipoPond As Integer, ByVal sIdi As String)
        Dim oDS As DataSet
        Dim oTbl As DataTable
        Dim sFormula As String
        Dim iEq As New USPExpress.USPExpression
        Dim sVariables() As String
        Dim dValues() As Double
        Dim x, i As Integer
        Dim sNC_IDs As String
        Dim sNC_IDs_int As String
        Dim cont As Integer
        Dim dValor As Double
        Dim iPos As Integer

        'Para poder obtener en el guardado de las variables de calidad
        'el valor de las incognitas y el ID de las noConformidad, se realiza el siguiente proceso
        'incognita1=Valor;IDsNoConfomidades/incognita2=Valor;IDsNoConfomidades...
        'ejem-> x1= 6;575,576,580,581.../x2=2;1210,/x3...

        Try
            Authenticate()
            oDS = DBServer.Ponderacion_GetWeighValueNC(lVarCal, iNivel, sProve, lIdUNQA, lNivelUnqa)

            If oDS.Tables(0).Rows.Count > 0 Then
                sFormula = oDS.Tables(0).Rows(0).Item("FORMULA")
                miIdFormulaError = oDS.Tables(0).Rows(0).Item("ID_FORMULA")

                ReDim sVariables(oDS.Tables.Count - 2)
                ReDim dValues(oDS.Tables.Count - 2)
                sNC_IDs = ""

                Dim j As Integer = 1

                For i = 1 To 7
                    iPos = InStr(UCase(sFormula), "X" & CStr(i))

                    If iPos > 0 Then 'La variable est� en la f�rmula
                        If i <= 4 Then
                            sVariables(j - 1) = "X" & CStr(i)

                            sNC_IDs_int = ""
                            dValor = 0
                            cont = 0
                            'n� de NoConformidades
                            If oDS.Tables(j).Rows.Count = 0 Then
                                dValues(j - 1) = 0
                                msValoresFormulaError = msValoresFormulaError & dValues(j - 1) & ";"
                            Else
                                For x = 0 To oDS.Tables(j).Rows.Count - 1
                                    If sNC_IDs_int <> "" Then sNC_IDs_int += ","
                                    sNC_IDs_int = sNC_IDs_int & CStr(oDS.Tables(j).Rows(x).Item("ID"))
                                    If iTipoPond = 1 Or iTipoPond = 2 Then
                                        dValor = dValor + oDS.Tables(j).Rows(x).Item("PESO")
                                    End If
                                    cont = cont + 1
                                Next
                            End If
                            If iTipoPond = 0 Then
                                'n� de NoConformidades
                                dValues(j - 1) = cont
                                msValoresFormulaError = msValoresFormulaError & dValues(j - 1) & ";"
                            ElseIf iTipoPond = 1 Then
                                'Media
                                dValues(j - 1) = IIf(cont > 0, dValor / cont, 0)
                                msValoresFormulaError = msValoresFormulaError & dValues(j - 1) & ";"
                            ElseIf iTipoPond = 2 Then
                                'Suma
                                dValues(j - 1) = dValor
                                msValoresFormulaError = msValoresFormulaError & dValues(j - 1) & ";"
                            End If
                            sNC_IDs = sNC_IDs + "X" & CStr(i) + "=" + CStr(dValues(j - 1)) + ";" + sNC_IDs_int + "/"
                        Else
                            If oDS.Tables(j).Rows.Count = 0 OrElse IsDBNull(oDS.Tables(j).Rows(0).Item("X" & CStr(i))) Then
                                'Si no tiene asignado objetivo y suelo y est�n en la formula generamos error.
                                miVarXError = i
                                Select Case i
                                    Case 6
                                        miTipoError = 1
                                        msDenError = "No tiene objetivo"
                                    Case 7
                                        miTipoError = 2
                                        msDenError = "No tiene suelo"
                                End Select
                                msValoresFormulaError = msValoresFormulaError & ";"
                            Else
                                sVariables(j - 1) = "X" & CStr(i)
                                dValues(j - 1) = oDS.Tables(j).Rows(0).Item("X" & CStr(i))
                                msValoresFormulaError = msValoresFormulaError & dValues(j - 1) & ";"
                                sNC_IDs = sNC_IDs + "X" & CStr(i) + "=" + CStr(dValues(j - 1)) + ";" + "/"
                            End If
                        End If
                        j += 1
                    End If
                Next

                If miTipoError = 0 Then
                    Try
                        iEq.Parse(sFormula, sVariables)
                        mdblPuntos = iEq.Evaluate(dValues)
                        msVariableXi = sNC_IDs
                        miTipoError = 0
                    Catch ex0 As Exception
                        miTipoError = 6
                        miVarXError = 0
                        msDenError = ex0.Message
                    End Try

                    ObtenerCalificacion(lVarCal, iNivel)
                End If
            End If
            oTbl = Nothing
            oDS = Nothing
        Catch ex As Exception
            miTipoError = 6
            miVarXError = 0
            msDenError = ex.Message
        Finally
            oTbl = Nothing
            oDS = Nothing
        End Try
    End Sub
    ''' <summary>
    ''' Calculo de variables de tipo certificado
    ''' </summary>
    ''' <param name="sProve">Proveedor</param>
    ''' <param name="lVarcal">ID variable de calidad</param>  
    ''' <param name="iNivel">Nivel de la variable</param> 
    ''' <param name="sIdi">Nivel de la variable</param>
    ''' <param name="iOrigen">Origen</param>
    ''' <param name="iIdFormula">Id f�rmula</param>
    ''' <param name="sFormula">F�rmula</param>
    ''' <param name="dValorDef">Valor por defecto</param>
    ''' <param name="dValorCertSinCumpl">Valor por defecto para certificados sin cumplimentar</param> 
    ''' <remarks>Llamada desde=QA_Puntuaciones.vb; Tiempo m�ximo= 0,1 sg </remarks>
    Public Sub PonderacionCertificado(ByVal sProve As String, ByVal lVarCal As Integer, ByVal iNivel As Short, ByVal sIdi As String, ByVal QAPeriodoValidezCertificado As Integer,
                                      ByVal iOrigen As Integer, ByVal iIdFormula As Integer, ByVal sFormula As String, ByVal dValorDef As Double, ByVal dValorCertSinCumpl As Double)
        Dim oDS As DataSet
        Dim oTbl As DataTable
        Dim oRow As DataRow
        Dim oLista As DataRow
        Dim sFormulaAux As String = ""
        Dim iEq As New USPExpress.USPExpression
        Dim sVariables() As String = Nothing
        Dim dValues() As Double = Nothing
        Dim i As Integer
        Dim sVar() As String
        Dim dVal() As Double
        Dim sVariableX As String = ""
        Dim iPos As Integer

        Try
            Authenticate()
            oDS = DBServer.Ponderacion_GetValuesCetificado(lVarCal, iNivel, sProve, iOrigen)

            miIdFormulaError = iIdFormula
            'Tengo una f�rmula auxiliar para buscar las posibles variables.
            'Ej: f�rmula: X10, si busco X10 est�, pero tb est� X1, 
            'por lo que cada vez que encuentre una variable la borraremos de sFormulaAux
            sFormulaAux = sFormula

            If oDS.Tables(0).Rows.Count = 0 Then
                'El certificado no se ha solicitado
                mdblPuntos = dValorDef
            ElseIf oDS.Tables(0).Rows.Count = 1 AndAlso oDS.Tables(0).Rows(0).IsNull("FEC_CUMPLIM") Then
                'El certificado se ha solicitado pero no se ha cumplimentado 
                mdblPuntos = dValorCertSinCumpl
            Else
                i = 0
                If oDS.Tables(1).Rows.Count > 0 Then
                    For Each oRow In oDS.Tables(1).Rows
                        Dim sVarCod As String = If(SQLBinaryToBoolean(oRow.Item("GEN")), oRow.Item("COD"), "X" & oRow.Item("NUM"))

                        'Hemos recogido la formula y todos los campos. Solo trataremos los que pertenezcan a la formula.
                        'Si esta en la formula, lo ponderamos y guardamos sus valores.      
                        iPos = InStr(UCase(sFormulaAux), sVarCod)
                        If iPos > 0 Then
                            If sVariables Is Nothing Then ReDim sVariables(0) Else ReDim Preserve sVariables(sVariables.Length)
                            If dValues Is Nothing Then ReDim dValues(0) Else ReDim Preserve dValues(dValues.Length)

                            sFormulaAux = Replace(sFormulaAux, sVarCod, "")
                            sVariables(sVariables.Length - 1) = sVarCod

                            Select Case oRow.Item("TIPOPOND")
                                Case TipoPonderacionCert.EscalaContinua
                                    Dim TienePonderacion As Boolean = False
                                    If oRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoNumerico Then
                                        If Not IsDBNull(oRow.Item("COPIA_NUM")) Then
                                            For Each oLista In oRow.GetChildRows("LISTA")
                                                If CDec(oLista.Item("DESDE_NUM")) <= CDec(oRow.Item("COPIA_NUM")) And
                                               CDec(oRow.Item("COPIA_NUM")) <= CDec(oLista.Item("HASTA_NUM")) Then
                                                    dValues(dValues.Length - 1) = oLista.Item("VALOR_POND")
                                                    msValoresFormulaError = msValoresFormulaError & dValues(dValues.Length - 1) & ";"
                                                    TienePonderacion = True
                                                    Exit For
                                                End If
                                            Next
                                        End If
                                    ElseIf oRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoFecha Then
                                        Dim sCampoFec As String = If(SQLBinaryToBoolean(oRow.Item("GEN")) And oRow.Item("ID") = VarCalPCertCampoGeneral.FecExpiracion, "FEC_EXPIRACION", "COPIA_FEC")

                                        If Not IsDBNull(oRow.Item(sCampoFec)) Then
                                            For Each oLista In oRow.GetChildRows("LISTA")
                                                If CDate(oLista.Item("DESDE_FEC")) <= CDate(oRow.Item(sCampoFec)) And CDate(oRow.Item(sCampoFec)) <= CDate(oLista.Item("HASTA_FEC")) Then
                                                    dValues(dValues.Length - 1) = oLista.Item("VALOR_POND")
                                                    msValoresFormulaError = msValoresFormulaError & dValues(dValues.Length - 1) & ";"
                                                    TienePonderacion = True
                                                    Exit For
                                                End If
                                            Next
                                        End If
                                    End If
                                    If Not TienePonderacion Then
                                        miTipoError = 4
                                        miVarXError = oRow.Item("NUM")
                                        msValoresFormulaError = msValoresFormulaError & ";"
                                    End If
                                Case TipoPonderacionCert.EscalaDiscreta
                                    Dim TienePonderacion As Boolean = False
                                    If oRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoNumerico Then
                                        If Not IsDBNull(oRow.Item("COPIA_NUM")) Then
                                            For Each oLista In oRow.GetChildRows("LISTA")
                                                If CDec(oLista.Item("VALOR_NUM")) = CDec(oRow.Item("COPIA_NUM")) Then
                                                    dValues(dValues.Length - 1) = oLista.Item("VALOR_POND")
                                                    msValoresFormulaError = msValoresFormulaError & dValues(dValues.Length - 1) & ";"
                                                    TienePonderacion = True
                                                    Exit For
                                                End If
                                            Next
                                        End If
                                    ElseIf oRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoFecha Then
                                        Dim sCampoFec As String = If(SQLBinaryToBoolean(oRow.Item("GEN")) And oRow.Item("ID") = VarCalPCertCampoGeneral.FecExpiracion, "FEC_EXPIRACION", "COPIA_FEC")

                                        If Not IsDBNull(oRow.Item(sCampoFec)) Then
                                            For Each oLista In oRow.GetChildRows("LISTA")
                                                If CDate(oLista.Item("VALOR_FEC")) = CDate(oRow.Item(sCampoFec)) Then
                                                    dValues(dValues.Length - 1) = oLista.Item("VALOR_POND")
                                                    msValoresFormulaError = msValoresFormulaError & dValues(dValues.Length - 1) & ";"
                                                    TienePonderacion = True
                                                    Exit For
                                                End If
                                            Next
                                        End If
                                    Else
                                        Select Case oRow.Item("SUBTIPO")
                                            Case TiposDeDatos.TipoGeneral.TipoString, TiposDeDatos.TipoGeneral.TipoTextoCorto, TiposDeDatos.TipoGeneral.TipoTextoLargo, TiposDeDatos.TipoGeneral.TipoTextoMedio
                                                If Not IsDBNull(oRow.Item("COPIA_NUM")) Then
                                                    For Each oLista In oRow.GetChildRows("LISTA")
                                                        If CInt(oLista.Item("VALOR_NUM")) = CInt(oRow.Item("COPIA_NUM")) Then
                                                            dValues(dValues.Length - 1) = oLista.Item("VALOR_POND")
                                                            msValoresFormulaError = msValoresFormulaError & dValues(dValues.Length - 1) & ";"
                                                            TienePonderacion = True
                                                            Exit For
                                                        End If
                                                    Next
                                                End If
                                        End Select
                                    End If
                                    If Not TienePonderacion Then
                                        miTipoError = 4
                                        miVarXError = oRow.Item("NUM")
                                        msValoresFormulaError = msValoresFormulaError & ";"
                                    End If
                                Case TipoPonderacionCert.Formula
                                    If Not IsDBNull(oRow.Item("COPIA_NUM")) Then
                                        ReDim sVar(0)
                                        ReDim dVal(0)
                                        sVar(0) = "X"
                                        dVal(0) = oRow.Item("COPIA_NUM")
                                        Try
                                            iEq.Parse(oRow.Item("FORMULA"), sVar)
                                            dValues(dValues.Length - 1) = iEq.Evaluate(dVal)
                                            msValoresFormulaError = msValoresFormulaError & dValues(dValues.Length - 1) & ";"
                                        Catch ex0 As Exception
                                            miTipoError = 5
                                            miVarXError = oRow.Item("COPIA_NUM")
                                            msDenError = ex0.Message
                                            msValoresFormulaError = msValoresFormulaError & ";"
                                        End Try
                                    End If
                                Case TipoPonderacionCert.PorCaducidad
                                    Dim sCampoFecha As String
                                    Dim AuxQAPeriodoValidezCertificado As Integer

                                    If SQLBinaryToBoolean(oRow.Item("GEN")) And oRow.Item("ID") = VarCalPCertCampoGeneral.FecExpiracion Then
                                        sCampoFecha = "FEC_EXPIRACION"
                                        AuxQAPeriodoValidezCertificado = QAPeriodoValidezCertificado
                                    Else
                                        sCampoFecha = "COPIA_FEC"
                                        AuxQAPeriodoValidezCertificado = 0
                                    End If

                                    If Not IsDBNull(oRow.Item(sCampoFecha)) Then
                                        If DateDiff(DateInterval.Day, CDate(oRow.Item(sCampoFecha)), Today) < AuxQAPeriodoValidezCertificado Then
                                            dValues(dValues.Length - 1) = oRow.Item("PUNT_SI")
                                            msValoresFormulaError = msValoresFormulaError & dValues(dValues.Length - 1) & ";"
                                        Else
                                            dValues(dValues.Length - 1) = oRow.Item("PUNT_NO")
                                            msValoresFormulaError = msValoresFormulaError & dValues(dValues.Length - 1) & ";"
                                        End If
                                    ElseIf sCampoFecha = "FEC_EXPIRACION" Then
                                        dValues(dValues.Length - 1) = oRow.Item("PUNT_SI")
                                        msValoresFormulaError = msValoresFormulaError & dValues(dValues.Length - 1) & ";"
                                    Else
                                        dValues(dValues.Length - 1) = oRow.Item("PUNT_NO")
                                        msValoresFormulaError = msValoresFormulaError & dValues(dValues.Length - 1) & ";"
                                    End If
                                Case TipoPonderacionCert.Automatico
                                    If oRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoBoolean Then
                                        If Not IsDBNull(oRow.Item("COPIA_BOOL")) Then
                                            If oRow.Item("COPIA_BOOL") = 1 Then
                                                dValues(dValues.Length - 1) = oRow.Item("PUNT_SI")
                                                msValoresFormulaError = msValoresFormulaError & dValues(dValues.Length - 1) & ";"
                                            Else
                                                dValues(dValues.Length - 1) = oRow.Item("PUNT_NO")
                                                msValoresFormulaError = msValoresFormulaError & dValues(dValues.Length - 1) & ";"
                                            End If
                                        End If
                                    ElseIf oRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoArchivo Then
                                        If DBNullToSomething(oRow.Item("TIENECERTIFICADO")) > 0 Then
                                            dValues(dValues.Length - 1) = oRow.Item("PUNT_SI")
                                            msValoresFormulaError = msValoresFormulaError & dValues(dValues.Length - 1) & ";"
                                        Else
                                            dValues(dValues.Length - 1) = oRow.Item("PUNT_NO")
                                            msValoresFormulaError = msValoresFormulaError & dValues(dValues.Length - 1) & ";"
                                        End If
                                    ElseIf oRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoFecha Then
                                        'Caso de campo general del certificado fecha cumplimentaci�n
                                        If SQLBinaryToBoolean(oRow.Item("GEN")) And oRow.Item("ID") = VarCalPCertCampoGeneral.FecCumplimentacion Then
                                            If oDS.Tables(0).Rows(0).IsNull("FEC_LIM_CUMPLIM") Then
                                                dValues(dValues.Length - 1) = oRow.Item("PUNT_SI")
                                                msValoresFormulaError = msValoresFormulaError & dValues(dValues.Length - 1) & ";"
                                            Else
                                                If DateDiff(DateInterval.Day, CDate(oDS.Tables(0).Rows(0)("FEC_CUMPLIM")), CDate(oDS.Tables(0).Rows(0)("FEC_LIM_CUMPLIM"))) >= 0 Then
                                                    dValues(dValues.Length - 1) = oRow.Item("PUNT_SI")
                                                    msValoresFormulaError = msValoresFormulaError & dValues(dValues.Length - 1) & ";"
                                                Else
                                                    dValues(dValues.Length - 1) = oRow.Item("PUNT_NO")
                                                    msValoresFormulaError = msValoresFormulaError & dValues(dValues.Length - 1) & ";"
                                                End If
                                            End If
                                        End If
                                    End If
                            End Select

                            sVariableX = sVariableX & sVarCod & "=" & CStr(dValues(dValues.Length - 1)) & "/"
                            If Not SQLBinaryToBoolean(oRow.Item("GEN")) Then i += 1
                        End If
                    Next

                    If miTipoError = 0 Then
                        Try
                            iEq.Parse(sFormula, sVariables)
                            mdblPuntos = iEq.Evaluate(dValues)
                            msVariableXi = sVariableX
                            miTipoError = 0
                        Catch e As USPExpress.ParseException
                            miTipoError = 3
                            miVarXError = 0
                            msDenError = e.Message
                        Catch ex0 As Exception
                            miTipoError = 6
                            miVarXError = 0
                            msDenError = ex0.Message
                        End Try
                    End If
                Else
                    mdblPuntos = dValorDef
                End If
            End If

            ObtenerCalificacion(lVarCal, iNivel, sIdi)
            oTbl = Nothing
            oDS = Nothing
        Catch ex As Exception
            miTipoError = 6
            miVarXError = 0
            msDenError = ex.Message
        Finally
            oTbl = Nothing
            oDS = Nothing
        End Try
    End Sub
    ''' <summary>Calculo de variables de tipo encuesta</summary>
    ''' <param name="sProve">Proveedor</param>
    ''' <param name="lVarcal">ID variable de calidad</param>  
    ''' <param name="iNivel">Nivel de la variable</param> 
    ''' <param name="lIdUNQA">Id de la UNQA</param>
    ''' <param name="sIdi">Nivel de la variable</param>
    ''' <param name="iOrigen">Origen</param>
    ''' <param name="iIdFormula">Id f�rmula</param>
    ''' <param name="sFormula">F�rmula</param>
    ''' <param name="dValorDef">Valor por defecto</param> 
    ''' <remarks>Llamada desde=QA_Puntuaciones.vb</remarks>
    Public Sub PonderacionEncuesta(ByVal sProve As String, ByVal lVarCal As Integer, ByVal iNivel As Short, ByVal lIdUNQA As Long, ByVal sIdi As String, ByVal iOrigen As Integer, ByVal iIdFormula As Integer, ByVal sFormula As String,
                                   ByVal dValorDef As Double)
        Dim oDS As DataSet
        Dim oTbl As DataTable
        Dim sFormulaAux As String = String.Empty
        Dim iEq As New USPExpress.USPExpression
        Dim sVar() As String
        Dim dVal() As Double
        Dim iPos As Integer

        'En el string con los valores de las inc�gnitas de la f�rmula se guarda tambi�n el id de la encuesta
        'IdEncuesta1;incognita1=valor1,incognita2=valor2/IdEncuesta2;incognita1=valor1,incognita2=valor2...
        'Ej: 1000;x1=6,x2=3/1001;x1=4,x2=7...

        Try
            Authenticate()
            oDS = DBServer.Ponderacion_GetValuesEncuesta(lVarCal, iNivel, lIdUNQA, sProve, iOrigen)

            miIdFormulaError = iIdFormula

            If Not oDS.Tables(0) Is Nothing AndAlso oDS.Tables(0).Rows.Count > 0 Then
                'Tengo una f�rmula auxiliar para buscar las posibles variables.
                'Ej: f�rmula: X10, si busco X10 est�, pero tb est� X1, por lo que cada vez que encuentre una variable la borraremos de sFormulaAux    
                Dim sVariableX As String = String.Empty

                For Each oEncuesta As DataRow In oDS.Tables(0).Rows
                    Dim sVariables() As String = Nothing
                    Dim dValues() As Double = Nothing
                    Dim i As Integer = 0
                    sFormulaAux = sFormula.ToUpper

                    If oDS.Tables(1).Rows.Count > 0 Then
                        'Dataview con los campos de la vista
                        Dim dvCamposEnc As New DataView(oDS.Tables(1), "INSTANCIA=" & oEncuesta("ID"), "NUM DESC", DataViewRowState.CurrentRows)

                        Dim sVarX As String = String.Empty
                        For Each oCampo As DataRowView In dvCamposEnc
                            'Hemos recogido la formula y todos los campos. Solo trataremos los que pertenezcan a la formula.
                            'Si esta en la formula, lo ponderamos y guardamos sus valores.
                            iPos = InStr(UCase(sFormulaAux), "X" & oCampo("NUM"))

                            If iPos > 0 Then
                                ReDim Preserve sVariables(i)
                                ReDim Preserve dValues(i)
                                sFormulaAux = Replace(sFormulaAux, "X" & oCampo("NUM"), "")
                                sVariables(i) = "X" & oCampo("NUM")

                                Select Case oCampo("TIPOPOND")
                                    Case TipoPonderacionCert.EscalaContinua
                                        Dim bTienePonderacion As Boolean = False
                                        If oCampo("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoNumerico Then
                                            If Not oCampo.Row.IsNull("COPIA_NUM") Then
                                                For Each oLista As DataRow In oCampo.Row.GetChildRows("LISTA")
                                                    If CDec(oLista("DESDE_NUM")) <= CDec(oCampo("COPIA_NUM")) And CDec(oCampo("COPIA_NUM")) <= CDec(oLista("HASTA_NUM")) Then
                                                        dValues(i) = oLista("VALOR_POND")
                                                        msValoresFormulaError = msValoresFormulaError & dValues(i) & ";"
                                                        bTienePonderacion = True
                                                        Exit For
                                                    End If
                                                Next
                                            End If
                                        ElseIf oCampo("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoFecha Then
                                            If Not oCampo.Row.IsNull("COPIA_FEC") Then
                                                For Each oLista As DataRow In oCampo.Row.GetChildRows("LISTA")
                                                    If CDate(oLista.Item("DESDE_FEC")) <= CDate(oCampo("COPIA_FEC")) And CDate(oCampo("COPIA_FEC")) <= CDate(oLista.Item("HASTA_FEC")) Then
                                                        dValues(i) = oLista("VALOR_POND")
                                                        msValoresFormulaError = msValoresFormulaError & dValues(i) & ";"
                                                        bTienePonderacion = True
                                                        Exit For
                                                    End If
                                                Next
                                            End If
                                        End If
                                        If Not bTienePonderacion Then
                                            miTipoError = 4
                                            miVarXError = oCampo("NUM")
                                            msValoresFormulaError = msValoresFormulaError & ";"
                                        End If
                                    Case TipoPonderacionCert.EscalaDiscreta
                                        Dim bTienePonderacion As Boolean = False
                                        If oCampo("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoNumerico Then
                                            If Not oCampo.Row.IsNull("COPIA_NUM") Then
                                                For Each oLista In oCampo.Row.GetChildRows("LISTA")
                                                    If CDec(oLista("VALOR_NUM")) = CDec(oCampo("COPIA_NUM")) Then
                                                        dValues(i) = oLista("VALOR_POND")
                                                        msValoresFormulaError = msValoresFormulaError & dValues(i) & ";"
                                                        bTienePonderacion = True
                                                        Exit For
                                                    End If
                                                Next
                                            End If
                                        ElseIf oCampo("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoFecha Then
                                            If Not oCampo.Row.IsNull("COPIA_FEC") Then
                                                For Each oLista As DataRow In oCampo.Row.GetChildRows("LISTA")
                                                    If CDate(oLista("VALOR_FEC")) = CDate(oCampo("COPIA_FEC")) Then
                                                        dValues(i) = oLista.Item("VALOR_POND")
                                                        msValoresFormulaError = msValoresFormulaError & dValues(i) & ";"
                                                        bTienePonderacion = True
                                                        Exit For
                                                    End If
                                                Next
                                            End If
                                        Else
                                            Select Case oCampo("SUBTIPO")
                                                Case TiposDeDatos.TipoGeneral.TipoString, TiposDeDatos.TipoGeneral.TipoTextoCorto, TiposDeDatos.TipoGeneral.TipoTextoLargo, TiposDeDatos.TipoGeneral.TipoTextoMedio
                                                    If Not oCampo.Row("COPIA_NUM") Then
                                                        For Each oLista As DataRow In oCampo.Row.GetChildRows("LISTA")
                                                            If CInt(oLista.Item("VALOR_NUM")) = CInt(oCampo("COPIA_NUM")) Then
                                                                dValues(i) = oLista("VALOR_POND")
                                                                msValoresFormulaError = msValoresFormulaError & dValues(i) & ";"
                                                                bTienePonderacion = True
                                                                Exit For
                                                            End If
                                                        Next
                                                    End If
                                            End Select
                                        End If
                                        If Not bTienePonderacion Then
                                            miTipoError = 4
                                            miVarXError = oCampo("NUM")
                                            msValoresFormulaError = msValoresFormulaError & ";"
                                        End If
                                    Case TipoPonderacionCert.Formula
                                        If Not oCampo.Row.Item("COPIA_NUM") Then
                                            ReDim sVar(0)
                                            ReDim dVal(0)
                                            sVar(0) = "X"
                                            dVal(0) = oCampo("COPIA_NUM")
                                            Try
                                                iEq.Parse(oCampo("FORMULA"), sVar)
                                                dValues(i) = iEq.Evaluate(dVal)
                                                msValoresFormulaError = msValoresFormulaError & dValues(i) & ";"
                                            Catch ex0 As Exception
                                                miTipoError = 5
                                                miVarXError = oCampo("COPIA_NUM")
                                                msDenError = ex0.Message
                                                msValoresFormulaError = msValoresFormulaError & ";"
                                            End Try
                                        End If
                                    Case TipoPonderacionCert.Automatico
                                        If oCampo("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoBoolean Then
                                            If Not oCampo.Row.IsNull("COPIA_BOOL") Then
                                                If oCampo("COPIA_BOOL") = 1 Then
                                                    dValues(i) = oCampo("PUNT_SI")
                                                    msValoresFormulaError = msValoresFormulaError & dValues(i) & ";"
                                                Else
                                                    dValues(i) = oCampo("PUNT_NO")
                                                    msValoresFormulaError = msValoresFormulaError & dValues(i) & ";"
                                                End If
                                            End If
                                        End If
                                End Select

                                sVarX = sVarX & "X" & CStr(oCampo("NUM")) & "=" & CStr(dValues(i)).Replace(",", ".") & "|"
                                i += 1
                            End If
                        Next

                        sVariableX &= oEncuesta("ID") & ";" & sVarX & "/"

                        If miTipoError = 0 Then
                            Try
                                iEq.Parse(sFormula, sVariables)
                                mdblPuntos += iEq.Evaluate(dValues)     'Se va acumulando el valor de la var. para cada encuesta
                                msVariableXi = sVariableX
                                miTipoError = 0
                            Catch e As USPExpress.ParseException
                                miTipoError = 3
                                miVarXError = 0
                                msDenError = e.Message
                                Exit For
                            Catch e0 As USPExpress.DivisionByZeroException
                                'La divisi�n por cero es un error controlado y supone un resultado de 0 para la f�rmula  
                                mdblPuntos += 0
                                msVariableXi = sVariableX
                                miTipoError = 0
                            Catch ex0 As Exception
                                miTipoError = 6
                                miVarXError = 0
                                msDenError = ex0.Message
                                Exit For
                            End Try
                        End If
                    End If
                Next

                'La puntuaci�n de la var. es la media de los valores de todas las encuestas
                mdblPuntos = mdblPuntos / oDS.Tables(0).Rows.Count
            Else
                mdblPuntos = dValorDef
            End If

            ObtenerCalificacion(lVarCal, iNivel, sIdi)
            oTbl = Nothing
            oDS = Nothing
        Catch ex As Exception
            miTipoError = 6
            miVarXError = 0
            msDenError = ex.Message
        Finally
            oTbl = Nothing
            oDS = Nothing
        End Try
    End Sub
    ''' <summary>
    ''' Obtiene la puntuacion de una variable de caldidad de tipo PPM
    ''' </summary>
    ''' <param name="sProve">Codigo proveedor</param>
    ''' <param name="lVarCal">ID variable de calidad</param>    
    ''' <param name="iNivel">Nivel variable de calidad</param> 
    ''' <param name="lIdUNQA">ID Unidad Negocio</param>     
    ''' <param name="lNivelUnqa">Nivel UNQA</param>
    ''' <param name="sIdi">Cod Idioma para obtener la denominacion Calificacion</param>
    ''' <remarks>Llamada desde=QA_Puntuaciones.vb//; Tiempo m�ximo=0,5seg.</remarks>
    Public Sub PonderacionPPM(ByVal sProve As String, ByVal lVarCal As Integer, ByVal iNivel As Short, ByVal lIdUNQA As Long, ByVal lNivelUnqa As Long, ByVal sIdi As String)
        Dim oDS As DataSet
        Dim oTbl As DataTable
        Dim sFormula As String
        Dim iEq As New USPExpress.USPExpression
        Dim sVariables() As String
        Dim dValues() As Double
        Dim i As Integer
        Dim sVariableX As String = ""
        Dim iPos As Integer

        Try
            Authenticate()
            oDS = DBServer.Ponderacion_GetWeighValuePPM(lVarCal, iNivel, sProve, lIdUNQA, lNivelUnqa)

            If oDS.Tables(0).Rows.Count > 0 Then
                sFormula = oDS.Tables(0).Rows(0).Item("FORMULA")
                miIdFormulaError = oDS.Tables(0).Rows(0).Item("ID_FORMULA")

                ReDim sVariables(oDS.Tables.Count - 2)
                ReDim dValues(oDS.Tables.Count - 2)

                Dim j As Integer = 1

                For i = 1 To 4
                    'Vemos si la variable est� en la formula. Si no esta no hacemos nada
                    iPos = InStr(UCase(sFormula), "X" & CStr(i))
                    If iPos > 0 Then
                        'Si que est� en la formula                    
                        If oDS.Tables(j).Rows.Count = 0 OrElse IsDBNull(oDS.Tables(j).Rows(0).Item("X" & CStr(i))) Then
                            If i > 2 Then
                                'Si no tiene asignado objetivo y suelo y est�n en la formula generamos error.
                                miVarXError = i
                                Select Case i
                                    Case 3
                                        miTipoError = 1
                                        msDenError = "No tiene objetivo"
                                    Case 4
                                        miTipoError = 2
                                        msDenError = "No tiene suelo"
                                End Select
                                msValoresFormulaError = msValoresFormulaError & ";"
                            Else
                                dValues(j - 1) = 0
                                msValoresFormulaError = msValoresFormulaError & dValues(j - 1) & ";"
                            End If
                        Else
                            sVariables(j - 1) = "X" & CStr(i)
                            dValues(j - 1) = oDS.Tables(j).Rows(0).Item("X" & CStr(i))
                            msValoresFormulaError = msValoresFormulaError & dValues(j - 1) & ";"
                            sVariableX = sVariableX + "X" + CStr(i) + "=" + CStr(dValues(j - 1)) + "/"
                            j += 1
                        End If
                    End If
                Next

                If miTipoError = 0 Then
                    Try
                        iEq.Parse(sFormula, sVariables)
                        mdblPuntos = iEq.Evaluate(dValues)
                        msVariableXi = sVariableX
                        miTipoError = 0
                    Catch ex0 As Exception
                        miTipoError = 6
                        miVarXError = 0
                        msDenError = ex0.Message
                    End Try

                    ObtenerCalificacion(lVarCal, iNivel, sIdi)
                End If
            End If
            oTbl = Nothing
            oDS = Nothing
        Catch ex As Exception
            miTipoError = 6
            miVarXError = 0
            msDenError = ex.Message
        Finally
            oTbl = Nothing
            oDS = Nothing
        End Try
    End Sub
    ''' <summary>
    ''' Obtiene la puntuacion de una variable de caldidad de tipo Cargo Proveedores
    ''' </summary>
    ''' <param name="sProve">Codigo proveedor</param>
    ''' <param name="lVarCal">ID variable de calidad</param>    
    ''' <param name="iNivel">Nivel variable de calidad</param> 
    ''' <param name="lIdUNQA">ID Unidad Negocio</param>     
    ''' <param name="lNivelUnqa">Nivel UNQA</param>
    ''' <param name="sIdi">Codigo Idioma</param>
    ''' <remarks>Llamada desde=QA_Puntuaciones.vb//; Tiempo m�ximo=0,4seg.</remarks>
    Public Sub PonderacionCargoProveedores(ByVal sProve As String, ByVal lVarCal As Integer, ByVal iNivel As Short, ByVal lIdUNQA As Long, ByVal lNivelUnqa As Long, ByVal sIdi As String)
        Dim oDS As DataSet
        Dim oTbl As DataTable
        Dim sFormula As String
        Dim iEq As New USPExpress.USPExpression
        Dim sVariables() As String
        Dim dValues() As Double
        Dim i As Integer
        Dim sVariableX As String = ""
        Dim iPos As Integer

        Try
            Authenticate()
            oDS = DBServer.Ponderacion_GetWeighValueCargoProveedores(lVarCal, iNivel, sProve, lIdUNQA, lNivelUnqa)

            If oDS.Tables(0).Rows.Count > 0 Then
                sFormula = oDS.Tables(0).Rows(0).Item("FORMULA")
                miIdFormulaError = oDS.Tables(0).Rows(0).Item("ID_FORMULA")

                ReDim sVariables(oDS.Tables.Count - 2)
                ReDim dValues(oDS.Tables.Count - 2)

                Dim j As Integer = 1

                For i = 1 To 4
                    'Vemos si la variable est� en la formula. Si no esta no hacemos nada
                    iPos = InStr(UCase(sFormula), "X" & CStr(i))
                    If iPos > 0 Then
                        'Si que est� en la formula                    
                        If oDS.Tables(j).Rows.Count = 0 OrElse IsDBNull(oDS.Tables(j).Rows(0).Item("X" & CStr(i))) Then
                            If i > 2 Then
                                'Si no tiene asignado objetivo y suelo y est�n en la formula generamos error.
                                miVarXError = i
                                Select Case i
                                    Case 3
                                        miTipoError = 1
                                        msDenError = "No tiene objetivo"
                                    Case 4
                                        miTipoError = 2
                                        msDenError = "No tiene suelo"
                                End Select
                                msValoresFormulaError = msValoresFormulaError & ";"
                            Else
                                dValues(j - 1) = 0
                                msValoresFormulaError = msValoresFormulaError & dValues(j - 1) & ";"
                            End If
                        Else
                            sVariables(j - 1) = "X" & CStr(i)
                            dValues(j - 1) = oDS.Tables(j).Rows(0).Item("X" & CStr(i))
                            msValoresFormulaError = msValoresFormulaError & dValues(j - 1) & ";"
                            sVariableX = sVariableX + "X" + CStr(i) + "=" + CStr(dValues(j - 1)) + "/"
                            j += 1
                        End If
                    End If
                Next

                If miTipoError = 0 Then
                    Try
                        iEq.Parse(sFormula, sVariables)
                        mdblPuntos = iEq.Evaluate(dValues)
                        msVariableXi = sVariableX
                        miTipoError = 0
                    Catch ex0 As Exception
                        miTipoError = 6
                        miVarXError = 0
                        msDenError = ex0.Message
                    End Try

                    ObtenerCalificacion(lVarCal, iNivel, sIdi)
                End If
            End If
            oTbl = Nothing
            oDS = Nothing
        Catch ex As Exception
            miTipoError = 6
            miVarXError = 0
            msDenError = ex.Message
        Finally
            oTbl = Nothing
            oDS = Nothing
        End Try
    End Sub
    ''' <summary>
    ''' Obtiene la puntuacion de una variable de caldidad de tipo Tasa de Servicios
    ''' </summary>
    ''' <param name="sProve">Codigo proveedor</param>
    ''' <param name="lVarCal">ID variable de calidad</param>    
    ''' <param name="iNivel">Nivel variable de calidad</param> 
    ''' <param name="lIdUNQA">ID Unidad Negocio</param>     
    ''' <param name="lNivelUnqa">Nivel UNQA</param>
    ''' <param name="sIdi">Codigo Idioma</param>
    ''' <remarks>Llamada desde=QA_Puntuaciones.vb//; Tiempo m�ximo=0,4seg.</remarks>
    Public Sub PonderacionTasaServicios(ByVal sProve As String, ByVal lVarCal As Integer, ByVal iNivel As Short, ByVal lIdUNQA As Long, ByVal lNivelUnqa As Long, ByVal sIdi As String)
        Dim oDS As DataSet
        Dim oTbl As DataTable
        Dim sFormula As String
        Dim iEq As New USPExpress.USPExpression
        Dim sVariables() As String
        Dim dValues() As Double
        Dim i As Integer
        Dim sVariableX As String = ""
        Dim iPos As Integer

        Try
            Authenticate()
            oDS = DBServer.Ponderacion_GetWeighValueTasaServicios(lVarCal, iNivel, sProve, lIdUNQA, lNivelUnqa)

            If oDS.Tables(0).Rows.Count > 0 Then
                sFormula = oDS.Tables(0).Rows(0).Item("FORMULA")
                miIdFormulaError = oDS.Tables(0).Rows(0).Item("ID_FORMULA")

                ReDim sVariables(oDS.Tables.Count - 2)
                ReDim dValues(oDS.Tables.Count - 2)

                Dim j As Integer = 1

                For i = 1 To 4
                    'Vemos si la variable est� en la formula. Si no esta no hacemos nada
                    iPos = InStr(UCase(sFormula), "X" & CStr(i))
                    If iPos > 0 Then
                        'Si que est� en la formula                    
                        If oDS.Tables(j).Rows.Count = 0 OrElse IsDBNull(oDS.Tables(j).Rows(0).Item("X" & CStr(i))) Then
                            If i > 2 Then
                                'Si no tiene asignado objetivo y suelo y est�n en la formula generamos error.
                                miVarXError = i
                                Select Case i
                                    Case 3
                                        miTipoError = 1
                                        msDenError = "No tiene objetivo"
                                    Case 4
                                        miTipoError = 2
                                        msDenError = "No tiene suelo"
                                End Select
                                msValoresFormulaError = msValoresFormulaError & ";"
                            Else
                                dValues(j - 1) = 0
                                msValoresFormulaError = msValoresFormulaError & dValues(j - 1) & ";"
                            End If
                        Else
                            sVariables(j - 1) = "X" & CStr(i)
                            dValues(j - 1) = oDS.Tables(j).Rows(0).Item("X" & CStr(i))
                            msValoresFormulaError = msValoresFormulaError & dValues(j - 1) & ";"
                            sVariableX = sVariableX + "X" + CStr(i) + "=" + CStr(dValues(j - 1)) + "/"
                            j += 1
                        End If
                    End If
                Next

                If miTipoError = 0 Then
                    Try
                        iEq.Parse(sFormula, sVariables)
                        mdblPuntos = iEq.Evaluate(dValues)
                        msVariableXi = sVariableX
                        miTipoError = 0
                    Catch ex0 As Exception
                        miTipoError = 6
                        miVarXError = 0
                        msDenError = ex0.Message
                    End Try

                    ObtenerCalificacion(lVarCal, iNivel, sIdi)
                End If
            End If
            oTbl = Nothing
            oDS = Nothing
        Catch ex As Exception
            miTipoError = 6
            miVarXError = 0
            msDenError = ex.Message
        Finally
            oTbl = Nothing
            oDS = Nothing
        End Try
    End Sub
    ''' <summary>
    ''' Obtiene la puntuacion de una variable de caldidad de tipo Manual
    ''' </summary>
    ''' <param name="sProve">Codigo proveedor</param>
    ''' <param name="lVarCal">ID variable de calidad</param>    
    ''' <param name="iNivel">Nivel variable de calidad</param> 
    ''' <param name="lIdUNQA">ID Unidad Negocio</param>     
    ''' <param name="sIdi">Codigo Idioma</param>
    ''' <remarks>Llamada desde=QA_Puntuaciones.vb//; Tiempo m�ximo=0,2seg.</remarks>
    Public Sub PonderacionManual(ByVal sProve As String, ByVal lVarCal As Integer, ByVal iNivel As Short, ByVal lIdUnqa As Long, Optional ByVal sIdi As String = "SPA")
        Dim oDS As DataSet
        Authenticate()
        oDS = DBServer.Ponderacion_GetWeighValueManual(lVarCal, iNivel, sProve, lIdUnqa, sIdi)

        If oDS.Tables(0).Rows.Count > 0 Then
            If IsDBNull(oDS.Tables(0).Rows(0).Item("VALOR")) Then
                mdblPuntos = oDS.Tables(0).Rows(0).Item("VALOR_DEF")
                ObtenerCalificacion(lVarCal, iNivel, sIdi)
            Else
                mdblPuntos = DBNullToDbl(oDS.Tables(0).Rows(0).Item("VALOR"))
                mlIDCalif = DBNullToSomething(oDS.Tables(0).Rows(0).Item("IDCAL"))
                msCalif = DBNullToSomething(oDS.Tables(0).Rows(0).Item("CAL"))
            End If

        End If

        oDS = Nothing

    End Sub
    ''' <summary>
    ''' Carga el ID y la Denominacion de la calificacion que le corresponden a los puntos
    ''' </summary>
    ''' <param name="lVarCal">ID variable de calidad</param>    
    ''' <param name="iNivel">Nivel variable de calidad</param>      
    ''' <param name="sIdi">Cod Idioma para obtener la denominacion Calificacion</param>
    ''' <remarks>Llamada desde=QA_Puntuaciones.vb//; Tiempo m�ximo=0,1seg.</remarks>
    Public Sub ObtenerCalificacion(ByVal lVarCal As Integer, ByVal iNivel As Short, Optional ByVal sIdi As String = "")
        Dim oDS As DataSet
        Dim iEq As New USPExpress.USPExpression

        If IsNothing(mdblPuntos) Then
            mlIDCalif = Nothing
            msCalif = Nothing
            Exit Sub
        End If

        Authenticate()
        oDS = DBServer.Ponderacion_GetCalification(lVarCal, iNivel, mdblPuntos, sIdi)

        If oDS.Tables(0).Rows.Count > 0 Then
            mlIDCalif = DBNullToSomething(oDS.Tables(0).Rows(0).Item("IDCAL"))
            msCalif = DBNullToSomething(oDS.Tables(0).Rows(0).Item("CAL"))
        End If

        oDS = Nothing

    End Sub
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
End Class
