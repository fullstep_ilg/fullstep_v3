﻿<Serializable()> _
Public Class Facturas
    Inherits Security


    Private m_lNumeroFacturasPendientes As Long
    Public Property NumeroFacturasPendientes() As Long
        Get
            Return m_lNumeroFacturasPendientes
        End Get

        Set(ByVal Value As Long)
            m_lNumeroFacturasPendientes = Value
        End Set
    End Property

    Private Enum Tipo
        Coste = 0
        Descuento = 1
    End Enum

    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub

    ''' <summary>
    ''' Devuelve los costes que anteriormente ha seleccionado un proveedor en sus facturas
    ''' </summary>
    ''' <param name="sCodProve">codigo del proveedor</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function LoadCostesFacturas(ByVal sCodProve As String) As DataSet
        Authenticate()
        Return DBServer.Facturas_LoadCostesDescuentos(sCodProve, Tipo.Coste)
    End Function

    ''' <summary>
    ''' Devuelve los costes que anteriormente ha seleccionado un proveedor en sus líneas de facturas
    ''' </summary>
    ''' <param name="sCodProve">codigo del proveedor</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function LoadCostesLineaFacturas(ByVal sCodProve As String) As DataSet
        Authenticate()
        Return DBServer.Factura_LoadCostesDescuentosLinea(sCodProve, Tipo.Coste)
    End Function

    ''' <summary>
    ''' Devuelve los descuentos que anteriormente ha seleccionado un proveedor en sus facturas
    ''' </summary>
    ''' <param name="sCodProve">codigo del proveedor</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function LoadDescuentosFacturas(ByVal sCodProve As String) As DataSet
        Authenticate()
        Return DBServer.Facturas_LoadCostesDescuentos(sCodProve, Tipo.Descuento)
    End Function

    ''' <summary>
    ''' Devuelve los descuentos que anteriormente ha seleccionado un proveedor en sus líneas de facturas
    ''' </summary>
    ''' <param name="sCodProve">codigo del proveedor</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function LoadDescuentosLineaFacturas(ByVal sCodProve As String) As DataSet
        Authenticate()
        Return DBServer.Factura_LoadCostesDescuentosLinea(sCodProve, Tipo.Descuento)
    End Function
    Public Function LoadWebPart(ByVal sUsu As String, Optional ByVal sIdi As String = "SPA", Optional ByVal nTop As Integer = 0, Optional ByVal sTipos As String = Nothing, Optional ByVal sFiltros As String = Nothing, Optional ByVal dImporteDesde As Double = Nothing, Optional ByVal dImporteHasta As Double = Nothing, Optional ByVal dFechaDesde As Date = Nothing, Optional ByVal dFechaHasta As Date = Nothing, Optional ByVal sEstado As String = Nothing, Optional ByVal bAbiertasPorUsted As Boolean = True, Optional ByVal bParticipo As Boolean = True, Optional ByVal bPendientes As Boolean = True, Optional ByVal bPendientesDevolucion As Boolean = True, Optional ByVal bTrasladadas As Boolean = True, Optional ByVal bOtras As Boolean = True) As DataSet
        Authenticate()
        Return DBServer.Facturas_LoadWebPart(sUsu, sIdi, nTop, sTipos, sFiltros, dImporteDesde, dImporteHasta, dFechaDesde, dFechaHasta, sEstado, bAbiertasPorUsted, bParticipo, bPendientes, bPendientesDevolucion, bTrasladadas, bOtras)
    End Function

    ''' <summary>
    ''' Obtiene los impuestos para aplicar a una línea de factura, según unos criterios de selección indicados
    ''' </summary>
    ''' <param name="sIdioma">Idioma del usuario</param>
    ''' <param name="sCod">Cod del impuesto</param>
    ''' <param name="sDesc">Descripción del impuesto</param>
    ''' <param name="sPais">Cod del país</param>
    ''' <param name="sProv">Cod de la provincia</param>
    ''' <param name="sCodArt">Cod del artículo</param>
    ''' <param name="sCodMat">Material</param>
    ''' <param name="bRepercutido">Si hay que devolver los impuestos repercutidos</param>
    ''' <param name="bRetenido">Si hay que devolver los impuestos retenidos</param>
    ''' <param name="iConceptoImpuesto">Concepto del impuesto</param>
    ''' <returns>Los impuestos que cumplen con los criterios</returns>
    ''' <remarks>LLamada desde: DetalleFactura.aspx.vb;</remarks>
    Public Function LoadImpuestosFacturas(ByVal sIdioma As String, ByVal sCod As String, ByVal sDesc As String, ByVal sPais As String, ByVal sProv As String, ByVal sCodArt As String, ByVal sCodMat As String, ByVal bRepercutido As Boolean, ByVal bRetenido As Boolean, ByVal iConceptoImpuesto As Nullable(Of TiposDeDatos.ConceptoImpuesto)) As DataSet
        Authenticate()
        Return DBServer.Facturas_LoadImpuestosFacturas(sIdioma, sCod, sDesc, sPais, sProv, sCodArt, sCodMat, bRepercutido, bRetenido, iConceptoImpuesto)
    End Function

    ''' <summary>
    ''' Carga todas las facturas para el usuario pasado como parámetro
    ''' </summary>
    ''' <param name="sUsu">Cod de la persona</param>
    ''' <param name="sIdioma">Idioma en la que salen las denominaciones</param>
    ''' <returns>las facturas de ese usuario</returns>
    ''' <remarks>Llamada desde: VisorFacturas.aspx</remarks>
    Public Function Visor_Facturas(ByVal sUsu As String, ByVal sIdioma As String, ByVal Proveedor As String, ByVal Empresa As Long, ByVal NumFactura As String, ByVal FecFacturaDesde As Date, ByVal FecFacturaHasta As Date, ByVal FecContaDesde As Date, ByVal FecContaHasta As Date, ByVal ImporteDesde As Double, ByVal ImporteHasta As Double, ByVal CentroCoste As String, ByVal Partidas As DataTable, ByVal Estado As Integer, ByVal AnyoPedido As String, ByVal NumCesta As Long, ByVal NumPedido As Long, ByVal Articulo As String, ByVal Albaran As String, ByVal PedidoERP As String, ByVal NumFacturaSAP As String, ByVal Gestor As String, ByVal FacturaOriginal As Boolean, ByVal FacturaRectificativa As Boolean) As DataSet

        Authenticate()

        Dim ds As DataSet

        ds = DBServer.Facturas_Load(sUsu, sIdioma, Proveedor, Empresa, NumFactura, FecFacturaDesde, FecFacturaHasta, FecContaDesde, FecContaHasta, ImporteDesde, ImporteHasta, CentroCoste, Partidas, Estado, AnyoPedido, NumCesta, NumPedido, Articulo, Albaran, PedidoERP, NumFacturaSAP, Gestor, FacturaOriginal, FacturaRectificativa)

        For Each oColumn As DataColumn In ds.Tables(0).Columns
            If UCase(oColumn.DataType.FullName) = "SYSTEM.DATETIME" Then
                For Each oRow As DataRow In ds.Tables(0).Rows
                    If Not IsDBNull(oRow.Item(oColumn.ColumnName)) Then
                        oRow.Item(oColumn.ColumnName) = Format(oRow.Item(oColumn.ColumnName), "d")
                    End If
                Next
            End If
        Next

        Visor_Facturas = ds
    End Function

    ''' <summary>
    ''' Obtiene el total de facturas pendientes para el usuario
    ''' </summary>
    ''' <param name="sUsu">Cod de la persona</param>
    ''' <returns>El total de facturas pendientes</returns>
    ''' <remarks>Llamada desde: VisorFacturas.aspx.vb. Tiempo máx: 0,1 sg</remarks>
    Public Function DevolverNumeroFacturasPendientes(ByVal sUsu As String) As Long
        Authenticate()
        DevolverNumeroFacturasPendientes = DBServer.Facturas_NumeroPendientes(sUsu)
    End Function


    Public Function BuscadorFacturas(ByVal sIdioma As String, ByVal Proveedor As String, ByVal Empresa As Long, ByVal FecFacturaDesde As Date, ByVal FecFacturaHasta As Date, ByVal FecContaDesde As Date, ByVal FecContaHasta As Date, ByVal ImporteDesde As Double, ByVal ImporteHasta As Double, ByVal CentroCoste As String, ByVal Partidas As DataTable, ByVal Estado As Integer, ByVal AnyoPedido As String, ByVal NumCesta As Long, ByVal NumPedido As Long, ByVal Articulo As String, ByVal Albaran As String, ByVal PedidoERP As String, ByVal NumFacturaSAP As String, ByVal Gestor As String) As DataSet
        Authenticate()
        BuscadorFacturas = DBServer.Facturas_Buscador(sIdioma, Proveedor, Empresa, FecFacturaDesde, FecFacturaHasta, FecContaDesde, FecContaHasta, ImporteDesde, ImporteHasta, CentroCoste, Partidas, Estado, AnyoPedido, NumCesta, NumPedido, Articulo, Albaran, PedidoERP, NumFacturaSAP, Gestor)
    End Function
End Class
