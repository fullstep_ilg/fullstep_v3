<Serializable()> _
Public Class ConfCampos
    Inherits Security

    Private moVisibles As DataSet

    Public ReadOnly Property Data() As Data.DataSet
        Get
            Return moVisibles
        End Get

    End Property

    ''' <summary>
    ''' Procedimiento que devuelve la configuraci�n que ser� visible de los campos
    ''' </summary>
    ''' <param name="sUsu">C�digo de usuario</param>
    ''' <param name="sModulo">M�dulo del campo</param>
    ''' <param name="Real">Variable booleana que indica si se trata de un campo real o no</param>
    ''' <remarks>
    ''' Llamada desdE:   PmWeb/materialesGSProveedor/ConfigurarGrid, PmWeb/selectorCampos/PAge_Load y cmdTemp_ServerClick, PmWeb/certifBusqueda/ConfigurarGrid, PmWeb/mantenimientoMat/ConfigurarGrid
    ''' tiempo m�ximo: 0,78 seg</remarks>
    Public Sub DevolverConfiguracionVisible(ByVal sUsu As String, ByVal sModulo As String, Optional ByVal Real As Short = 1)
        Authenticate()
        moVisibles = DBServer.ConfCampos_DevolverConfiguracion(sUsu, sModulo, Real)
    End Sub

    Public Sub New(ByRef dbserver As FSNDataBaseServer.Root,ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
    End Sub

    ''' <summary>
    ''' Procedimiento que modifica la visibilidad de los campos
    ''' </summary>
    ''' <param name="sUsu">C�digo de usuario</param>
    ''' <param name="oDS">DataSet con los datos de los campos</param>
    ''' <param name="sForm">Formulario</param>
    ''' <remarks>
    ''' Llamada desde:  PmWeb/selectorCampos/cmdTemp_Server_Click
    ''' Tiempo m�ximo: 0,45 seg</remarks>
    Public Sub ModificarVisibilidadCampos(ByVal sUsu As String, ByVal oDS As DataSet, ByVal sForm As String)
        Authenticate()
        DBServer.ConfCampos_ModificarVisibilidad(sUsu, oDS, sForm)
    End Sub
End Class
