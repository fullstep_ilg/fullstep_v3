<Serializable()> _
        Public Class UnidadesOrg
    Inherits Security

    Private moData As DataSet
    Private msUON1 As String
    Property UON1() As String
        Get
            Return msUON1

        End Get
        Set(ByVal Value As String)
            msUON1 = Value
        End Set
    End Property
    Private msUON2 As String
    Property UON2() As String
        Get
            Return msUON2

        End Get
        Set(ByVal Value As String)
            msUON2 = Value
        End Set
    End Property
    Private msUON3 As String
    Property UON3() As String
        Get
            Return msUON3

        End Get
        Set(ByVal Value As String)
            msUON3 = Value
        End Set
    End Property
    Public ReadOnly Property Data() As Data.DataSet
        Get
            Return moData
        End Get

    End Property

    Public Class Atributo
        Public Sub New()

        End Sub
        Private _Id As Long
        Public Property Id() As Long
            Get
                Id = _Id
            End Get
            Set(ByVal Value As Long)
                _Id = Value
            End Set
        End Property
        Private _Codigo As String
        Public Property Codigo() As String
            Get
                Return _Codigo
            End Get
            Set(ByVal value As String)
                _Codigo = value
            End Set
        End Property
        Private _Denominacion As String
        Public Property Denominacion() As String
            Get
                Return _Denominacion
            End Get
            Set(ByVal value As String)
                _Denominacion = value
            End Set
        End Property
        Private _Valor As Object
        Public Property Valor() As Object
            Get
                If _Tipo = 3 Then
                    If _Valor Is DBNull.Value Or _Valor Is Nothing Then
                        Return _Valor
                    Else
                        Return CDate(_Valor)
                    End If
                Else
                    Return _Valor
                End If

            End Get
            Set(ByVal value As Object)
                If _Tipo = 3 AndAlso Not value Is DBNull.Value AndAlso Not _Valor Is Nothing Then
                    _Valor = CDate(value)
                Else
                    _Valor = value
                End If

            End Set
        End Property
        Private _Valores As List(Of ValorAtrib)
        Public Property Valores() As List(Of ValorAtrib)
            Get
                Return _Valores
            End Get
            Set(ByVal value As List(Of ValorAtrib))
                _Valores = value
            End Set
        End Property
        Private _Intro As Short
        Public Property Intro() As Short
            Get
                Intro = _Intro
            End Get
            Set(ByVal Value As Short)
                _Intro = Value
            End Set
        End Property
        Private _Tipo As TiposDeDatos.TipoAtributo
        Public Property Tipo() As TiposDeDatos.TipoAtributo
            Get
                Tipo = _Tipo
            End Get
            Set(ByVal Value As TiposDeDatos.TipoAtributo)
                _Tipo = Value
            End Set
        End Property
        Private _Orden As Short 'Indicara el orden del valor seleccionado si el atributo es de tipo lista
        Public Property Orden() As Short
            Get
                Orden = _Orden
            End Get
            Set(ByVal Value As Short)
                _Orden = Value
            End Set
        End Property
        Private _UON1 As String
        Public Property UON1() As String
            Get
                Return _UON1
            End Get
            Set(ByVal value As String)
                _UON1 = value
            End Set
        End Property
        Private _UON2 As String
        Public Property UON2() As String
            Get
                Return _UON2
            End Get
            Set(ByVal value As String)
                _UON2 = value
            End Set
        End Property
        Private _UON3 As String
        Public Property UON3() As String
            Get
                Return _UON3
            End Get
            Set(ByVal value As String)
                _UON3 = value
            End Set
        End Property
    End Class

    Public Class ValorAtrib
        Public Sub New()

        End Sub
        Private _Orden As Short
        Public Property Orden As Short
            Get
                Return _Orden
            End Get
            Set(ByVal value As Short)
                _Orden = value
            End Set
        End Property
        Private _Valor As Object
        Public Property Valor() As Object
            Get
                Return _Valor
            End Get
            Set(ByVal value As Object)
                _Valor = value
            End Set
        End Property
    End Class
    ''' <summary>
    ''' Procedimiento que carga todas las unidades organizativas que tengan presupuestos
    ''' </summary>
    ''' <param name="iTipo">Tipo de presupuesto</param>
    ''' <param name="iAnyo">A�o del presupuesto</param>
    ''' <param name="sIdi">Idioma de la aplicaci�n</param>
    ''' <param name="sPer">C�digo de persona</param>
    ''' <remarks>
    ''' Llamada desde: PmWeb/presASig/Page_load
    ''' Tiempo m�ximo: 0,3 seg</remarks>
    Public Sub CargarUnidadesConPresupuestos(ByVal iTipo As Integer, ByVal RestricPresUOUsu As Boolean, ByVal iAnyo As Integer, ByVal sIdi As String, _
            ByVal sPer As String, ByVal sUON1 As String, ByVal sUON2 As String, ByVal sUON3 As String, ByVal EsPedidoAbierto As Boolean, _
            ByVal RestricPresUORama As Boolean, ByVal RestricPresPerfUO As Boolean)
        Authenticate()
        moData = DBServer.UnidadesConPresupuestos_Get(iTipo, RestricPresUOUsu, iAnyo, sIdi, sPer, sUON1, sUON2, sUON3, EsPedidoAbierto, RestricPresUORama, RestricPresPerfUO)
    End Sub
    ''' <summary>
    ''' Procedimiento que devuelve los presupuestos de las unidades organizativas con los filtros pasados
    ''' </summary>
    ''' <param name="iTipo">Tipo de presupuesto</param>
    ''' <param name="iEsUONVacia">Variable booleana que determina si se trata de una unidad organizativa vac�a</param>
    ''' <param name="iAnyo">A�o del presupuesto</param>
    ''' <param name="sUon1">C�digo de la unidad organizativa, nivel 1</param>
    ''' <param name="sUon2">C�digo de la unidad organizativa, nivel 2</param>
    ''' <param name="sUon3">C�digo de la unidad organizativa, nivel 3</param>
    ''' <param name="sIdi">Idioma de la aplicaci�n</param>
    ''' <param name="Codigo">Codigo del presupuesto</param>
    ''' <param name="Denominacion">Denominaci�n del presupuesto</param>
    ''' <remarks>
    ''' Llamada desdE: PmWeb/pres/CargarPResupuestos, PmWeb/presASig/CargarMisPResupuestos, 
    ''' Tiempo m�ximo: 0,4 seg</remarks>
    Public Sub CargarPresupuestos(ByVal iTipo As Integer, ByVal iEsUONVacia As Integer, Optional ByVal iAnyo As Integer = Nothing, Optional ByVal sUon1 As String = Nothing,
                                  Optional ByVal sUon2 As String = Nothing, Optional ByVal sUon3 As String = Nothing, Optional ByVal sIdi As String = "spa",
                                  Optional ByVal Codigo As String = Nothing, Optional ByVal Denominacion As String = Nothing)
        Authenticate()
        moData = DBServer.Presupuestos_Get(iTipo, iEsUONVacia, iAnyo, sUon1, sUon2, sUon3, sIdi, Codigo, Denominacion)
    End Sub
    ''' <summary>
    ''' Procedimiento que carga todas las unidades organizativas
    ''' </summary>
    ''' <param name="Idi">Idioma de la aplicaci�n</param>
    ''' <remarks>
    ''' Llamada desde: PmWeb/UnidadesOrganizativas/Page_Load
    ''' Tiempo m�ximo: 0,2 seg</remarks>
    Public Sub CargarTodasUnidadesOrganizativas(ByVal Usu As String, ByVal Idi As String, ByVal RestricUONSolUsuUON As Boolean, ByVal RestricUONSolPerfUON As Boolean)
        Authenticate()
        moData = DBServer.UnidadesOrganizativas_Get(Usu, Idi, RestricUONSolUsuUON, RestricUONSolPerfUON)
    End Sub
    ''' <summary>
    ''' Procedimiento que carga las unidades organizativas del usuario
    ''' </summary>
    ''' <param name="sIdi">Idioma de la aplicaci�n</param>
    ''' <param name="sUsu">Codigo de usuario</param>
    ''' <remarks>
    ''' Llamada desde: PmWeb/guardaInstancia/Page_load, PmWeb/usuarios/Page_load, PmWeb/traslados/Page_load
    ''' Tiempo m�ximo: 0,3 seg</remarks>
    Public Sub CargarUnidadesOrganizativas(Optional ByVal sIdi As String = "spa", Optional ByVal sUsu As String = Nothing, Optional ByVal RestricUOTraslado As Boolean = False)
        Authenticate()
        moData = DBServer.UnidadesOrg_DevolverUO(sIdi, sUsu, RestricUOTraslado)
    End Sub
    ''' <summary>
    ''' Procedimiento que carga la cadena del presupuestos de las unidades organizativas
    ''' </summary>
    ''' <param name="iTipo">Tipo de presupuesto</param>
    ''' <param name="sIdi">Idioma de la aplicaci�n</param>
    ''' <remarks>
    ''' Llamada desde: PmWeb/comprobarAdmiteMultiplesPres/MostrarMensaje, PmWeb/presAsig/Page_Load 
    ''' Tiempo m�ximo: 0,25 seg</remarks>
    Public Sub CargarCadenaPresupuesto(ByVal iTipo As Integer, Optional ByVal sIdi As String = "spa")
        Authenticate()
        moData = DBServer.UnidadesOrg_DevolverCadenaPresupuesto(iTipo, sIdi)
    End Sub
    ''' <summary>
    ''' Procedimiento que carga el estado de una unidad organizativa
    ''' </summary>
    ''' <param name="sUON1">Codigo de unidad organizativa, nivel 1</param>
    ''' <param name="sUon2">Codigo de unidad organizativa, nivel 2</param>
    ''' <param name="sUon3">Codigo de unidad organizativa, nivel 3</param>
    ''' <param name="sIdi">Idioma de la aplicaci�n</param>
    ''' <remarks>
    ''' Llamada desde: PmWeb/campos/Page_Load, PmWeb/desglose/Page_Load
    ''' Tiempo m�ximo: 0,35 seg</remarks>
    Public Sub CargarEstado(ByVal sUON1 As String, Optional ByVal sUon2 As String = Nothing, Optional ByVal sUon3 As String = Nothing, Optional ByVal sIdi As String = "spa")
        Authenticate()
        moData = DBServer.UnidadesOrg_DevolverEstado(sUON1, sUon2, sUon3, sIdi)
    End Sub
    ''' <summary>
    ''' Procedimiento que carga la unidad organizativa de un art�culo
    ''' </summary>
    ''' <param name="sArticulo">Codigo de art�culo</param>
    ''' <remarks>
    ''' Llamada desde: PmWeb/PresAsig/Page_load
    ''' Tiempo m�ximo: 0,3 seg</remarks>
    Public Sub CargarUnidadconArticulo(ByVal sArticulo As String)
        Authenticate()
        moData = DBServer.UnidadesOrg_DevolverUOconART(sArticulo)
    End Sub
    Public Sub CargarUnidadesOrganizativasQA(ByVal sUser As String, ByVal bRestriccionUO As Boolean, Optional ByVal sIdi As String = "spa")
        Authenticate()
        moData = DBServer.UnidadesOrg_DevolverUOQA(sUser, bRestriccionUO, sIdi)
    End Sub
    Public Function CargarAtributosDeUnidadOrganizativa(ByVal sUon1 As String, Optional ByVal sUon2 As String = Nothing, Optional ByVal sUon3 As String = Nothing) As List(Of Atributo)
        Authenticate()

        Dim oData As DataSet
        Dim oAtributos As New List(Of Atributo)
        Dim oAtributo As Atributo
        Dim oValorAtrib As ValorAtrib

        oData = DBServer.UnidadesOrg_DevolverAtribUO(sUon1, sUon2, sUon3)
        For Each oRow As DataRow In oData.Tables(0).Rows
            oAtributo = New Atributo
            oAtributo.Id = DBNullToInteger(oRow("ID"))
            oAtributo.Codigo = DBNullToStr(oRow("COD"))
            oAtributo.Denominacion = DBNullToStr(oRow("DEN"))
            oAtributo.Tipo = oRow("TIPO")
            oAtributo.Intro = DBNullToInteger(oRow("INTRO"))
            If oAtributo.Intro = 1 Then
                Dim oChildRows() As DataRow = oRow.GetChildRows("RELACION_1")
                Dim oValores As New List(Of ValorAtrib)
                For Each oChildRow As DataRow In oChildRows
                    oValorAtrib = New ValorAtrib
                    Select Case oAtributo.Tipo
                        Case 1
                            oValorAtrib.Valor = DBNullToStr(oChildRow("VALOR_TEXT_LISTA"))
                        Case 2
                            oValorAtrib.Valor = DBNullToStr(oChildRow("VALOR_NUM_LISTA"))
                        Case 3
                            If oChildRow("VALOR_FEC_LISTA") Is DBNull.Value Then
                                oValorAtrib.Valor = DBNull.Value
                            Else
                                oValorAtrib.Valor = CDate(oChildRow("VALOR_FEC_LISTA"))
                            End If
                    End Select
                    oValorAtrib.Orden = DBNullToDbl(oChildRow("ORDEN"))
                    oValores.Add(oValorAtrib)
                Next
                oAtributo.Valores = oValores
                Select Case oAtributo.Tipo
                    Case 1
                        oAtributo.Valor = DBNullToStr(oRow("VALOR_TEXT"))
                    Case 2
                        oAtributo.Valor = DBNullToStr(oRow("VALOR_NUM"))
                    Case 3
                        If oRow("VALOR_FEC") Is DBNull.Value Then
                            oAtributo.Valor = DBNull.Value
                        Else
                            oAtributo.Valor = CDate(oRow("VALOR_FEC"))
                        End If
                    Case 4
                        oAtributo.Valor = DBNullToStr(oRow("VALOR_BOOL"))
                End Select
            Else
                Select Case oAtributo.Tipo
                    Case 1
                        oAtributo.Valor = DBNullToStr(oRow("VALOR_TEXT"))
                    Case 2
                        oAtributo.Valor = DBNullToStr(oRow("VALOR_NUM"))
                    Case 3
                        If oRow("VALOR_FEC") Is DBNull.Value Then
                            oAtributo.Valor = DBNull.Value
                        Else
                            oAtributo.Valor = CDate(oRow("VALOR_FEC"))
                        End If
                    Case 4
                        oAtributo.Valor = DBNullToStr(oRow("VALOR_BOOL"))
                End Select
            End If
            oAtributos.Add(oAtributo)
        Next
        Return oAtributos
    End Function
    Public Function CargarAtributosArticuloDeUnidadOrg(ByVal codArt As String, ByVal sUon1 As String, Optional ByVal sUon2 As String = Nothing, Optional ByVal sUon3 As String = Nothing) As List(Of Atributo)
        Authenticate()

        Dim oData As DataSet
        Dim oAtributos As New List(Of Atributo)
        Dim oAtributo As Atributo

        oData = DBServer.UnidadesOrg_DevolverAtribArtUO(codArt, sUon1, sUon2, sUon3)
        For Each oRow As DataRow In oData.Tables(0).Rows
            oAtributo = New Atributo
            oAtributo.Id = DBNullToInteger(oRow("ATRIB"))
            oAtributo.Denominacion = DBNullToStr(oRow("DEN"))
            oAtributo.Tipo = oRow("TIPO")
            oAtributo.UON1 = DBNullToStr(oRow("UON1"))
            oAtributo.UON2 = DBNullToStr(oRow("UON2"))
            oAtributo.UON3 = DBNullToStr(oRow("UON3"))
            Select Case oAtributo.Tipo
                Case 1
                    oAtributo.Valor = DBNullToStr(oRow("VALOR_TEXT"))
                Case 2
                    oAtributo.Valor = DBNullToStr(oRow("VALOR_NUM"))
                Case 3
                    If oRow("VALOR_FEC") Is DBNull.Value Then
                        oAtributo.Valor = DBNull.Value
                    Else
                        oAtributo.Valor = CDate(oRow("VALOR_FEC"))
                    End If
                Case 4
                    If oRow("VALOR_BOOL") Is DBNull.Value Then
                        oAtributo.Valor = ""
                    ElseIf DBNullToStr(oRow("VALOR_BOOL")) = 0 Then
                        oAtributo.Valor = "NO"
                    ElseIf DBNullToStr(oRow("VALOR_BOOL")) = 1 Then
                        oAtributo.Valor = "SI"
                    End If
            End Select
            oAtributos.Add(oAtributo)
        Next
        Return oAtributos
    End Function
    Public Function CargarDenUnidadOrganizativa(ByVal sUON1 As String, Optional ByVal sUon2 As String = Nothing, Optional ByVal sUon3 As String = Nothing) As String
        Authenticate()
        Dim sDenUon As String
        sDenUon = DBServer.UnidadesOrg_DevolverDenUO(sUON1, sUon2, sUon3)
        Return sDenUon
    End Function
    Public Sub CargarUonDeOrgCompras(ByVal sOrgCompras As String, Optional ByVal sCentro As String = Nothing)
        Authenticate()
        moData = DBServer.UnidadesOrg_DevolverUonDeOrgCompras(sOrgCompras, sCentro)
    End Sub
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
    Public Function GetNivelPres(ByVal Nivel As Integer) As Integer
        Authenticate()
        Return DBServer.GetNivelPres(Nivel)
    End Function
    Public Function Get_Pres(ByVal Nivel As Integer, ByVal Pres4_Niv1 As String, Optional Pres4_Niv2 As String = Nothing,
                               Optional ByVal Pres4_Niv3 As String = Nothing, Optional ByVal Pres4_Niv4 As String = Nothing, Optional ByVal Anyo As Integer = 0) As DataTable
        Authenticate()
        Return DBServer.GetPres(Nivel, Pres4_Niv1, Pres4_Niv2, Pres4_Niv3, Pres4_Niv4, Anyo)
    End Function
End Class
