<Serializable()> _
Public Class Destino
    Inherits Security

    Private msCod As String
    Private msDen As String
    Private msDir As String
    Private msCP As String
    Private msPoblacion As String
    Private msPais As String
    Private msProvincia As String

    Public Property Cod() As String
        Get
            Return msCod
        End Get

        Set(ByVal Value As String)
            msCod = Value
        End Set
    End Property
    Public Property Den() As String
        Get
            Return msDen
        End Get

        Set(ByVal Value As String)
            msDen = Value
        End Set
    End Property
    Public Property Dir() As String
        Get
            Return msDir
        End Get

        Set(ByVal Value As String)
            msDir = Value
        End Set
    End Property
    Public Property CP() As String
        Get
            Return msCP
        End Get

        Set(ByVal Value As String)
            msCP = Value
        End Set
    End Property
    Public Property Poblacion() As String
        Get
            Return msPoblacion
        End Get

        Set(ByVal Value As String)
            msPoblacion = Value
        End Set
    End Property
    Public Property Pais() As String
        Get
            Return msPais
        End Get

        Set(ByVal Value As String)
            msPais = Value
        End Set
    End Property
    Public Property Provincia() As String
        Get
            Return msProvincia
        End Get

        Set(ByVal Value As String)
            msProvincia = Value
        End Set
    End Property
    ''' <summary>
    ''' Procedimiento que carga los datos de un destino dado su c�digo
    ''' </summary>
    ''' <param name="sIdi">Idioma de la aplicaci�n</param>
    ''' <remarks>
    ''' Llamada desde: PmWeb/denominacionesdest/Page_load, PmWeb/impexp/denGS
    ''' Tiempo m�ximo: 0,18 seg</remarks>
    Public Sub Load(ByVal sIdi As String)
        Authenticate()

        Dim data As DataSet
        Dim i As Integer
        data = DBServer.Destino_Load(sIdi, msCod)

        If Not data.Tables(0).Rows.Count = 0 Then

            For i = 0 To data.Tables(0).Rows.Count - 1
                msCod = data.Tables(0).Rows(i).Item("COD")
                msDen = DBNullToSomething(data.Tables(0).Rows(i).Item("DEN"))
                msCP = DBNullToSomething(data.Tables(0).Rows(i).Item("CP"))
                msDir = DBNullToSomething(data.Tables(0).Rows(i).Item("DIR"))
                msPoblacion = DBNullToSomething(data.Tables(0).Rows(i).Item("POB"))
                msPais = DBNullToSomething(data.Tables(0).Rows(i).Item("PAI"))
                msProvincia = DBNullToSomething(data.Tables(0).Rows(i).Item("PROVI"))
            Next
        End If

        data = Nothing
    End Sub
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
End Class