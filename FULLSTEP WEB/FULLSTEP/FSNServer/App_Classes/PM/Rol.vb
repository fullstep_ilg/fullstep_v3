<Serializable()> _
Public Class Rol
    Inherits Security

#Region "Properties"
    Private mlId As Integer
    Private msDen As String
    Private miTipo As Short
    Private msPer As String
    Private moParticipantes As DataSet
    Private mlBloque As Integer
    Private msSustituto As String
    Property Id() As Integer
        Get
            Return mlId
        End Get
        Set(ByVal Value As Integer)
            mlId = Value
        End Set
    End Property
    Property Den() As String
        Get
            Return msDen
        End Get
        Set(ByVal Value As String)
            msDen = Value
        End Set
    End Property
    Property Tipo() As Short
        Get
            Return miTipo
        End Get
        Set(ByVal Value As Short)
            miTipo = Value
        End Set
    End Property
    Property Persona() As String
        Get
            Return msPer
        End Get
        Set(ByVal Value As String)
            msPer = Value
        End Set
    End Property
    Public Property Participantes() As DataSet
        Get
            Participantes = moParticipantes
        End Get
        Set(ByVal Value As DataSet)
            moParticipantes = Value
        End Set
    End Property
    Property Bloque() As Integer
        Get
            Return mlBloque
        End Get
        Set(ByVal Value As Integer)
            mlBloque = Value
        End Set
    End Property
    Property Sustituto() As String
        Get
            Return msSustituto
        End Get
        Set(ByVal Value As String)
            msSustituto = Value
        End Set
    End Property
    Private _idInstancia As Long
    Public Property IdInstancia() As Long
        Get
            Return _idInstancia
        End Get
        Set(ByVal value As Long)
            _idInstancia = value
        End Set
    End Property
#End Region
    ''' <summary>
    ''' Carga las propiedades del rol
    ''' </summary>
    ''' <remarks>Llamada desde: PMDatabaseServer --> Instancia.vb --> ComprobarAsignacion; Tiempo m�ximo: 0 sg</remarks>
    Public Sub Load()
        Authenticate()

        Dim data As DataSet
        Dim i As Integer
        data = DBServer.Rol_Load(mlId, _idInstancia)
        If Not data.Tables(0).Rows.Count = 0 Then
            For i = 0 To data.Tables(0).Rows.Count - 1
                msDen = data.Tables(0).Rows(i).Item("DEN")
                miTipo = data.Tables(0).Rows(i).Item("TIPO")
                msPer = DBNullToSomething(data.Tables(0).Rows(i).Item("PER"))
                mlBloque = DBNullToDbl(data.Tables(0).Rows(i).Item("BLOQUE"))
                msSustituto = DBNullToSomething(data.Tables(0).Rows(i).Item("SUSTITUTO"))
            Next
        End If

        data = Nothing
    End Sub
    ''' <summary>
    ''' Procedimiento que asigna un rol a un usuario
    ''' </summary>
    ''' <param name="sUsu">Codigo de usuario</param>
    ''' <remarks>
    ''' Llamada desdE: PmServer/Instancia/ComprobarAsignacion, PmWeb/aprobacion/cmdSi_Click
    ''' Tiempo m�ximo: 0,4 seg</remarks>
    Public Sub AsignarRol(ByVal sUsu As String)
        Authenticate()
        DBServer.Rol_AsignarRol(mlId, sUsu, _idInstancia)
    End Sub
    ''' <summary>
    ''' Procedimiento que carga las acciones relacionadas con un rol
    ''' </summary>
    ''' <param name="sIdi">Idioma de la aplicaci�n</param>
    ''' <param name="bSoloRechazadas">Variable booleana que indica si se deben cargar solo las acciones rechazadas</param>
    ''' <returns>Un DataSet con las acciones que corresponden al rol</returns>
    ''' <remarks>
    ''' Llamada desdE: PmWeb/NWAlta/Page_load, PmWeb/NWDetalleSolicitud/Page_load, PmWeb/NWGestionInstancia/Page_load 
    ''' tiempo m�ximo: 0.6 seg</remarks>
    Public Function CargarAcciones(ByVal sIdi As String, Optional ByVal bSoloRechazadas As Boolean = False) As DataSet
        Authenticate()

        Dim oData As DataSet
        oData = DBServer.Rol_LoadAcciones(sIdi, mlId, mlBloque, bSoloRechazadas)
        Return oData
    End Function
    ''' <summary>
    ''' Funcion que carga los listados relacionados con un rol
    ''' </summary>
    ''' <param name="sIdi">Idioma de la aplicaci�n</param>
    ''' <returns>Un DataSet con los datos de los listados relacionados con el rol</returns>
    ''' <remarks>
    ''' Llamada desdE: PmWeb/NWGestionInstancia/Page_Load, PmWeb/NWDetalleSolicitud/Page_Load
    ''' Tiempo m�ximo: 0,3 seg</remarks>
    Public Function CargarListados(ByVal sIdi As String) As DataSet
        Authenticate()

        Dim oData As DataSet
        oData = DBServer.Rol_LoadListados(mlId, mlBloque, sIdi)
        Return oData
    End Function
    ''' <summary>
    ''' Funcion que carga los datos de los participantes en un rol
    ''' </summary>
    ''' <param name="sIdioma">Idioma de la aplicaci�n</param>
    ''' <param name="lInstancia">Id de la instancia a cargar</param>
    ''' <remarks>
    ''' Un DataSet con los datos de los participantes en el rol
    ''' Llamada desdE: PMServer/Instancia/ComprobarParticipantes, PmWeb/NWAlta/Page_load, PmWeb/NWDetalleSolicitud/Page_load, PmWeb/NWGestorInstancia/Page_load
    ''' tiempo m�ximo: 0,45 seg</remarks>
    Public Sub CargarParticipantes(ByVal sIdioma As String, Optional ByVal lInstancia As Integer = Nothing, _
                                    Optional ByVal lFavorita As String = Nothing)
        Authenticate()

        Dim oData As DataSet
        oData = DBServer.Rol_LoadParticipantes(sIdioma, mlId, mlBloque, lInstancia, lFavorita)
        moParticipantes = oData
    End Sub
    ''' Revisado por: blp. Fecha: 12/09/2013
    ''' <summary>
    ''' Obtiene los peticionarios para cargarlos en una combo. Si se le pasa el id de un filtro, solo se obtendr�n
    ''' los peticionarios de las solicitudes que utilizan el formulario relacionado con el filtro.
    ''' </summary>
    ''' <param name="sUsuario">C�digo del usuario</param>
    ''' <returns>Dataset con los peticionarios (cod persona, nombre y apellidos)</returns>
    ''' <remarks>Llamada desde: wucBusquedaAvanzada.ascx; Tiempo m�ximo: 0 sg</remarks>
    Public Function CargarPeticionariosWebService(Optional ByVal sUsuario As String = Nothing) As DataSet
        Authenticate()

        Dim ds As DataSet
        ds = DBServer.Rol_LoadPeticionarios(sUsuario, Nothing)
        Return ds
    End Function
    ''' Revisado por: blp. Fecha: 28/08/2013
    ''' <summary>
    ''' Obtiene los datos de los peticionarios pasados por par�metro
    ''' </summary>
    ''' <param name="CodPeticionarios">tabla con los c�digos de los peticionarios cuyos datos queremos recuperar</param>
    ''' <returns>Datatable con los peticionarios</returns>
    ''' <remarks>Llamada desde: wucBusquedaAvanzadaContratos.ascx; Tiempo m�ximo: 0,1 sg</remarks>
    Public Function CargarPeticionarios_filtrado(ByVal CodPeticionarios As DataTable) As DataTable
        Authenticate()

        Dim ds As DataTable
        ds = DBServer.Rol_LoadPeticionarios_Filtrado(CodPeticionarios)
        Return ds
    End Function
    ''' <summary>
    ''' Funcion que comprueba que existe una persona especifica en la base de datos
    ''' </summary>
    ''' <param name="UON1">Unidad organizativa a la que pertenece la persona, nivel 1</param>
    ''' <param name="UON2">Unidad organizativa a la que pertenece la persona, nivel 2</param>
    ''' <param name="UON3">Unidad organizativa a la que pertenece la persona, nivel 3</param>
    ''' <param name="DEP">Departamento de la persona</param>
    ''' <param name="PER">Codigo de la persona</param>
    ''' <returns>Una variable booleana que ser� true si existe esa persona y False en caso contrario</returns>
    ''' <remarks>
    ''' LLamada desde: PmWeb/NWAlta/Page_load
    ''' Tiempo m�ximo: 0,2 seg</remarks>
    Public Function ExistePersona(ByVal UON1 As String, ByVal UON2 As String, ByVal UON3 As String, ByVal DEP As String, ByVal PER As String) As Boolean
        Authenticate()

        Dim bExiste As Boolean
        bExiste = DBServer.Rol_ExistePersona(mlId, UON1, UON2, UON3, DEP, PER)
        Return bExiste
    End Function
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
    ''' <summary>
    ''' Funcion que carga el Id de la accion "guardar", si es q existe.
    ''' Tarea 1959 Vinculaciones Desglose
    ''' Tras un guardado con mover fila y/o eliminar fila se ha actualiza la tabla INSTANCIA_LINEAS_VINCULAR y la q era la
    ''' linea x ahora es la linea y. Las diferentes llamadas a popupDesgloseClickEvent se crean una vez al abrir el detalle
    ''' de la solicitud y por ello el index tras el guardado ha dejado de ser valido. 
    ''' Con este id en NWgestionInstancia.aspx le indico si debe usar la linea de popupDesgloseClickEvent o calcular la linea
    ''' a partir de la q tenga popupDesgloseClickEvent.
    ''' </summary>
    ''' <returns>Id de la accion "guardar", si es q existe</returns>
    ''' <remarks>Llamada desde:NWgestionInstancia.aspx; Tiempo m�ximo:0,1</remarks>
    Public Function CargarIdGuardar() As Long
        Authenticate()

        Dim oData As DataSet
        oData = DBServer.Rol_LoadIdGuardar(mlId, mlBloque)
        If oData.Tables(0).Rows.Count = 0 Then
            Return 0
        Else
            Return oData.Tables(0).Rows(0).Item("ACCION")
        End If
    End Function
End Class
