Imports System.Configuration
Imports System.ServiceModel

<Serializable()>
Public Class PM_ValidacionesIntegracion
	Inherits Security

	Private mlID As Long
	Private msCod As String
	Private moDen As MultiIdioma
	Private moDescr As MultiIdioma
	Private miTipo As Integer
	Private msGestor As String
	Private msNombreGestor As String
	Private mlFormulario As Long
	Private mlWorkflow As Long
	Private mbPub As Boolean
	Private mdPedidoDirecto As Double
	Private mbBaja As Boolean
	Private moFormulario As Formulario
	Private mdsAdjuntos As DataTable
	Private msDenTipo As String
	Private miTipoSolicit As Integer
	Private miBloque As Integer
	Private msDenBloque As String
	Private moLongitudesDeCodigos As TiposDeDatos.LongitudesDeCodigos
	'' Private msMoneda As String
	Public Property BloqueActual() As Integer
		Get
			Return miBloque
		End Get
		Set(ByVal Value As Integer)
			miBloque = Value
		End Set
	End Property
	Public Property DenBloqueActual() As String
		Get
			Return msDenBloque
		End Get
		Set(ByVal Value As String)
			msDenBloque = Value
		End Set
	End Property
	Public Property dsAdjuntos() As DataTable
		Get
			dsAdjuntos = mdsAdjuntos
		End Get
		Set(ByVal Value As DataTable)
			mdsAdjuntos = Value
		End Set
	End Property
	Public Property ID() As Long
		Get
			Return mlID
		End Get

		Set(ByVal Value As Long)
			mlID = Value
		End Set
	End Property
	Public Property Codigo() As String
		Get
			Return msCod
		End Get
		Set(ByVal Value As String)
			msCod = Value
		End Set
	End Property
	Property Den(ByVal Idioma As String) As String
		Get
			Den = moDen(Idioma)

		End Get
		Set(ByVal Value As String)
			If Not moDen.Contains(Idioma) Then
				moDen.Add(Idioma, Value)
			Else
				moDen(Idioma) = Value
			End If

		End Set
	End Property
	Property Descr(ByVal Idioma As String) As String
		Get
			Descr = moDescr(Idioma)

		End Get
		Set(ByVal Value As String)
			If Not moDescr.Contains(Idioma) Then
				moDescr.Add(Idioma, Value)
			Else
				moDescr(Idioma) = Value
			End If

		End Set
	End Property
	Public Property DenTipo() As String
		Get
			DenTipo = msDenTipo
		End Get
		Set(ByVal Value As String)
			msDenTipo = Value
		End Set
	End Property
	Public Property Tipo() As Integer
		Get
			Tipo = miTipo
		End Get
		Set(ByVal Value As Integer)
			miTipo = Value
		End Set
	End Property
	Public Property Gestor() As String
		Get
			Gestor = msGestor
		End Get
		Set(ByVal Value As String)
			msGestor = Value
		End Set
	End Property
	Public Property NombreGestor() As String
		Get
			NombreGestor = msNombreGestor
		End Get
		Set(ByVal Value As String)
			msNombreGestor = Value
		End Set
	End Property
	Public Property Formulario() As Formulario
		Get
			Formulario = moFormulario
		End Get
		Set(ByVal Value As Formulario)
			moFormulario = Value
		End Set
	End Property
	Public Property Workflow() As Long
		Get
			Workflow = mlWorkflow
		End Get
		Set(ByVal Value As Long)
			mlWorkflow = Value
		End Set
	End Property
	Public Property Pub() As Boolean
		Get
			Pub = mbPub
		End Get
		Set(ByVal Value As Boolean)
			mbPub = Value
		End Set
	End Property
	Public Property PedidoDirecto() As Double
		Get
			Return mdPedidoDirecto

		End Get
		Set(ByVal Value As Double)
			mdPedidoDirecto = Value

		End Set
	End Property
	Public ReadOnly Property Baja() As Boolean
		Get
			Baja = mbBaja
		End Get
	End Property
	Public Property TipoSolicit() As Integer
		Get
			TipoSolicit = miTipoSolicit
		End Get
		Set(ByVal Value As Integer)
			miTipoSolicit = Value
		End Set
	End Property
	''' <summary>
	''' Funci�n que nos dice si debemos hacer control de mapper
	''' </summary>
	''' <param name="TablaIntegracion">Parametro opcional con la entidad a analizar</param>
	''' <returns>Booleano</returns>
	''' <remarks>Llamada desde NWAlta.aspx, NWDetalleSolicitud.aspx, NWGEstionInstancia.aspx, Instancia.vb\ComprobarMapper, AltaNoConformidad.aspx, DetalleNoConformidad.aspx 
	''' Tiempo m�ximo 0 sec</remarks>
	Public Function ControlaConMaper(Optional ByVal TablaIntegracion As Integer = TablasIntegracion.PM) As Boolean
		ControlaConMaper = DBServer.IntegracionPM(TablaIntegracion)
	End Function
	''' <summary>
	''' Funci�n que har� las validaciones de integraci�n en el momento de emitir un solicitud PM 
	''' </summary>
	''' <param name="oDs">Dataset con los datos para validar</param>
	''' <param name="oDsAux">DataSet auxiliar</param>
	''' <param name="Idioma">Codigo de idioma</param>
	''' <param name="iNumError">Posible numero de error</param>
	''' <param name="strError">posible texto de error</param>
	''' <param name="sPer">Codigo de persona</param>
	''' <param name="IdInstancia">Id de instancia</param>
	''' <param name=" IdSolicitud ">Identificador de la solicitud</param>
	''' <param name="lAccionRol">Identificador de la acci�n</param>
	''' <param name="sBloquesDestino">Identificadores de los bloques destinos</param>
	''' <returns>True si ha ido Ok, False si ha habido error</returns>
	''' <remarks>Llamada desde Instancia.vb\ControlMapper()
	''' Tiempo m�ximo 1 sec.</remarks>
	Public Function ControlMapper(ByVal oDs As DataSet, ByVal oDsAux As DataSet, ByVal Idioma As String, ByRef iNumError As Short, ByRef strError As String,
								  Optional ByVal sPer As String = Nothing, Optional ByVal IdInstancia As Long = Nothing, Optional ByVal IdSolicitud As Long = Nothing,
								  Optional ByVal lAccionRol As Long = 0, Optional ByVal sBloquesDestino As String = "") As TipoValidacionIntegracion
		Dim dsMatArt As DataSet

		ControlMapper = TipoValidacionIntegracion.SinMensaje
		strError = ""
		Try
			Load_LongitudesDeCodigos()
			dsMatArt = CreaListas(oDs, oDsAux, Idioma, sPer, IdInstancia, IdSolicitud, lAccionRol)

			If Not HayIntegracionSalidaWCF(TablasIntegracion.PM) Then
				ControlMapper = EvalMapper(dsMatArt, Idioma, iNumError, strError, sPer, IdInstancia, IdSolicitud, lAccionRol, sBloquesDestino)
			Else
				iNumError = -100
				ControlMapper = ValidacionesWCF_PM(strError, dsMatArt, Idioma, sBloquesDestino)
			End If
		Catch ex As Exception 'ok no hay mapper
			ControlMapper = TipoValidacionIntegracion.SinMensaje
		End Try
	End Function
	''' <summary>
	''' Funci�n que har� las validaciones de integraci�n en el momento de emitir un solicitud PM 
	''' </summary>
	''' <param name="oDs">Dataset con los datos para validar</param>    
	''' <param name="Idioma">Codigo de idioma</param>
	''' <param name="iNumError">Posible numero de error</param>
	''' <param name="strError">posible texto de error</param>
	''' <returns>True si ha ido Ok, False si ha habido error</returns>
	''' <remarks>Llamada desde Instancia.vb\ControlMapper()
	''' Tiempo m�ximo 1 sec.</remarks>
	Public Function ControlMapperQA(ByVal oDs As DataSet, ByVal Idioma As String, ByRef iNumError As Short,
									ByRef strError As String) As TipoValidacionIntegracion
		Dim dsMatArt As DataSet

		ControlMapperQA = TipoValidacionIntegracion.SinMensaje
		strError = ""
		Try
			Load_LongitudesDeCodigos()
			dsMatArt = CreaListasQA(oDs)
			If Not HayIntegracionSalidaWCF(TablasIntegracion.PM) Then
				ControlMapperQA = EvalMapperQA(dsMatArt, Idioma, iNumError, strError)
			Else
				iNumError = -100
				ControlMapperQA = ValidacionesWCF_QA(strError, dsMatArt, Idioma)
			End If
		Catch ex As Exception 'ok no hay mapper
			ControlMapperQA = TipoValidacionIntegracion.SinMensaje
		End Try
	End Function
	''' <summary>
	''' Funci�n que llama a la mapper para hacer las validaciones de integracion
	''' </summary>
	''' <param name="dsMatArt">dataset con las tablas con los datos a validar</param>
	''' <param name="Idioma">Codigo de idioma</param>
	''' <param name="iNumError">Numero de error a devolver</param>
	''' <param name="strError">Mensjae de error a devolver</param>
	''' <param name="sPer">C�digod de persona</param>
	''' <param name="IdInstancia">Id de Instancia</param>
	''' <param name="IdSolicitud">Identificador de la solicitud</param>
	''' <param name="lAccionRol">Identificador de la acci�n</param>
	''' <param name="sBloquesDestino">Array con los identificadores de los bloques destino, separados por ","</param>
	''' <returns>Devuelve True si no ha habido errores, False si los ha habido</returns>
	''' <remarks>Llamada desde Instancia.ControlMapper() y ValidacionesIntegracion.ControlMapper()
	''' Tiempo m�ximo 1 sec</remarks>
	Public Function EvalMapper(ByVal dsMatArt As DataSet, ByVal Idioma As String, ByRef iNumError As Short, ByRef strError As String,
							   Optional ByVal sPer As String = Nothing, Optional ByVal IdInstancia As Long = Nothing, Optional ByVal IdSolicitud As Long = Nothing,
							   Optional ByVal lAccionRol As Long = 0, Optional ByVal sBloquesDestino As String = "") As TipoValidacionIntegracion
		Dim Maper As Object = Nothing
		Dim Instancia As String = System.Configuration.ConfigurationManager.AppSettings("instanciamaper")
		Try
			EvalMapper = TipoValidacionIntegracion.SinMensaje

			Dim arrMatGmn1() As String
			ReDim arrMatGmn1(0)
			Dim arrMatGmn2() As String
			ReDim arrMatGmn2(0)
			Dim arrMatGmn3() As String
			ReDim arrMatGmn3(0)
			Dim arrMatGmn4() As String
			ReDim arrMatGmn4(0)
			Dim arrArt() As String
			ReDim arrArt(0)
			Dim arrItem() As String
			ReDim arrItem(0)
			Dim arrAlm() As String
			ReDim arrAlm(0)
			Dim arrPres() As String
			ReDim arrPres(0)
			Dim arrAtribItem() As String
			ReDim arrAtribItem(0)
			Dim arrAtribId() As Integer
			ReDim arrAtribId(0)
			Dim arrAtribValor() As String
			ReDim arrAtribValor(0)
			Dim arrAtribValERP() As String
			ReDim arrAtribValERP(0)
			Dim arrCentro() As String
			ReDim arrCentro(0)
			Dim arrOrgCompras() As String
			ReDim arrOrgCompras(0)
			Dim arrProceAtribOrden() As String
			ReDim arrProceAtribOrden(0)
			Dim arrProceAtribId() As Integer
			ReDim arrProceAtribId(0)
			Dim arrProceAtribValor() As String
			ReDim arrProceAtribValor(0)
			Dim arrProceAtribValERP() As String
			ReDim arrProceAtribValERP(0)
			Dim arrTablaExternaItem() As String
			ReDim arrTablaExternaItem(0)
			Dim arrTablaExternaId() As Integer
			ReDim arrTablaExternaId(0)
			Dim arrTablaExternaValor() As String
			ReDim arrTablaExternaValor(0)
			Dim arrImporteLinItem() As String
			ReDim arrImporteLinItem(0)
			Dim arrImporteLinValor() As Double
			ReDim arrImporteLinValor(0)
			Dim arrProve() As String
			ReDim arrProve(0)
			Dim arrCentroSM() As String
			ReDim arrCentroSM(0)
			Dim arrCantidad() As Double
			ReDim arrCantidad(0)
			Dim arrPrecUni() As Double
			ReDim arrPrecUni(0)
			Dim arrTipoPedido() As String
			ReDim arrTipoPedido(0)
			Dim arrPartida() As String
			ReDim arrPartida(0)
			Dim arrPartidaNoDesglose() As String
			ReDim arrPartidaNoDesglose(0)
			Dim arrFecIniRectifAbono() As String
			ReDim arrFecIniRectifAbono(0)
			Dim arrFecFinRectifAbono() As String
			ReDim arrFecFinRectifAbono(0)
			Dim arrOrgComprasNoDesgl() As String
			ReDim arrOrgComprasNoDesgl(0)
			Dim arrCentroNoDesgl() As String
			ReDim arrCentroNoDesgl(0)
			Dim arrPresCab() As String
			ReDim arrPresCab(0)
			Dim arrFechaIniSuministro() As String
			ReDim arrFechaIniSuministro(0)
			Dim arrFechaFinSuministro() As String
			ReDim arrFechaFinSuministro(0)
			Dim arrFechaIniSuministroC() As String
			ReDim arrFechaIniSuministroC(0)
			Dim arrFechaFinSuministroC() As String
			ReDim arrFechaFinSuministroC(0)
			Dim arrPersona() As String
			ReDim arrPersona(0)

			Dim HayDatos As Boolean
			strError = ""

			HayDatos = CreaArrays(dsMatArt, arrMatGmn1, arrMatGmn2, arrMatGmn3, arrMatGmn4, arrArt, arrItem _
			, arrAlm, arrPres, arrAtribItem, arrAtribId, arrAtribValor, arrAtribValERP, arrCentro, arrOrgCompras _
			, arrProceAtribOrden, arrProceAtribId, arrProceAtribValor, arrProceAtribValERP, arrTablaExternaItem, arrTablaExternaId _
			, arrTablaExternaValor, arrImporteLinItem, arrImporteLinValor, arrProve, arrCentroSM, arrCantidad, arrPrecUni _
			, arrTipoPedido, arrPartida, arrPartidaNoDesglose, arrFecIniRectifAbono, arrFecFinRectifAbono, arrOrgComprasNoDesgl, arrCentroNoDesgl, arrPresCab _
			, arrFechaIniSuministroC, arrFechaFinSuministroC, arrFechaIniSuministro, arrFechaFinSuministro, arrPersona)

			''Obtiene el nombre de la Mapper en funci�n de la Organizaci�n de Compras
			Dim NomMapper As String = ""
			Dim orgcompras As String = ""
			If arrOrgComprasNoDesgl.Length > 0 Then
				For i As Integer = 0 To arrOrgComprasNoDesgl.Length - 1
					If arrOrgComprasNoDesgl(i) <> Nothing Then
						orgcompras = arrOrgComprasNoDesgl(i)
						Exit For
					End If
				Next
			End If
			If orgcompras = "" Then
				If arrOrgCompras.Length > 0 Then
					For i As Integer = 0 To arrOrgCompras.Length - 1
						If arrOrgCompras(i) <> Nothing Then
							orgcompras = arrOrgCompras(i)
							Exit For
						End If
					Next
				End If
			End If

			Dim NomMapperDat As DataSet = ObtenerNomMapper(orgcompras)
			If Not NomMapperDat.Tables(0).Rows.Count = 0 Then
				NomMapper = NomMapperDat.Tables(0).Rows(0).Item(0)
			End If

			If HayDatos And NomMapper <> "" Then
				''----   VALIDACIONES EXTERNAS DE INTEGRACI�N: Valida el valor de un atributo contra una funci�n (BAPI) de SAP------------
				''El centro puede estar fuera o dentro del desglose
				Dim sCentroAprov As String = ""
				sCentroAprov = IIf(arrCentroNoDesgl(0) = Nothing, IIf(arrCentro(0) = Nothing, "", arrCentro(0)), arrCentroNoDesgl(0))

				''1.- Atributos con ValidacionERP a nivel de l�nea de desglose:
				Dim arrAtribMapId() As Integer
				ReDim arrAtribMapId(0)
				Dim arrAtribMapValor() As String
				ReDim arrAtribMapValor(0)
				Dim arrParam2() As Boolean
				ReDim arrParam2(0)
				Dim arrParam3() As Boolean
				ReDim arrParam3(0)

				''Inicializa arrParam2:
				For i As Integer = 0 To UBound(arrAtribId, 1)
					ReDim Preserve arrParam2(i)
					arrParam2(i) = False
				Next
				If NecesarioValidarOrden(arrAtribId, arrAtribValor, arrAtribValERP, arrAtribMapId, arrAtribMapValor) Then
					If Not Maper Is Nothing Then
						Maper = Nothing
					End If
					Maper = CreateObject(NomMapper & ".clsValidacion")
					If Maper Is Nothing Then
						EvalMapper = TipoValidacionIntegracion.SinMensaje
						Exit Function
					End If
					strError = Maper.fStrValidarAtributoPM(Instancia, arrAtribId, arrAtribValor, sCentroAprov, Idioma, arrParam2, IdInstancia, lAccionRol)
					''Se pasan los arrays de atributos completos (en lugar de arratribMapId) para que actualice con el �ndice correcto arrParam2
					If strError <> "" Then
						iNumError = -100
						EvalMapper = TipoValidacionIntegracion.Mensaje_Bloqueo
					End If
				End If

				''2.- Atributos con ValidacionERP fuera de desglose:
				If EvalMapper = TipoValidacionIntegracion.SinMensaje Then
					ReDim arrAtribMapId(0)
					ReDim arrAtribMapValor(0)
					If NecesarioValidarOrden(arrProceAtribId, arrProceAtribValor, arrProceAtribValERP, arrAtribMapId, arrAtribMapValor) Then
						If Not Maper Is Nothing Then
							Maper = Nothing
						End If
						Maper = CreateObject(NomMapper & ".clsValidacion")
						If Maper Is Nothing Then
							EvalMapper = TipoValidacionIntegracion.SinMensaje
							Exit Function
						End If
						strError = Maper.fStrValidarAtributoPM(Instancia, arrAtribMapId, arrAtribMapValor, sCentroAprov, Idioma, arrParam3, IdInstancia, lAccionRol)
						''No se usa arrParam3, se mantiene porque es necesario en la funci�n de la mapper
						If strError <> "" Then
							iNumError = -100
							EvalMapper = TipoValidacionIntegracion.Mensaje_Bloqueo
						End If
					End If
				End If
				''----------------------------------------------------------------------------------------------

				If Not Maper Is Nothing Then
					Maper = Nothing
				End If
				Maper = CreateObject(NomMapper & ".clsInPedidosDirectos")

				''En segundo lugar, el resto de validaciones 
				If EvalMapper = TipoValidacionIntegracion.SinMensaje Then
					If Maper Is Nothing Then
						EvalMapper = TipoValidacionIntegracion.SinMensaje
						Exit Function
					End If
					'Aqui es donde puede ser cualquier valor de TipoValidacionIntegracion
					EvalMapper = Maper.TratarLineaPM(iNumError, strError _
						, arrMatGmn1, arrMatGmn2, arrMatGmn3, arrMatGmn4, arrArt, arrItem, arrAlm, arrPres _
						, arrAtribItem, arrAtribId, arrAtribValor _
						, arrProceAtribOrden, arrProceAtribId, arrProceAtribValor, arrOrgCompras, arrCentro, arrParam2, sPer _
						, IdInstancia, arrTablaExternaItem, arrTablaExternaId, arrTablaExternaValor, arrImporteLinItem _
						, arrImporteLinValor, arrProve, arrCentroSM, arrCantidad, arrPrecUni, IdSolicitud, arrTipoPedido _
						, arrPartida, arrPartidaNoDesglose, lAccionRol, arrFecIniRectifAbono, arrFecFinRectifAbono _
						, arrOrgComprasNoDesgl, arrCentroNoDesgl, arrPresCab, arrFechaIniSuministroC, arrFechaFinSuministroC, arrFechaIniSuministro, arrFechaFinSuministro, sBloquesDestino, arrPersona)
				End If
			End If
		Catch ex As Exception
			EvalMapper = TipoValidacionIntegracion.SinMensaje
		Finally
			Maper = Nothing
		End Try
	End Function
	''' <summary>
	''' Funci�n que llama a la mapper para hacer las validaciones de integracion
	''' </summary>
	''' <param name="dsMatArt">dataset con las tablas con los datos a validar</param>
	''' <param name="Idioma">Codigo de idioma</param>
	''' <param nFame="iNumError">Numero de error a devolver</param>
	''' <param name="strError">Mensjae de error a devolver</param>
	''' <returns>Devuelve True si no ha habido errores, False si los ha habido</returns>
	''' <remarks>Llamada desde Instancia.ControlMapper() y ValidacionesIntegracion.ControlMapper()
	''' Tiempo m�ximo 1 sec</remarks>
	Public Function EvalMapperQA(ByVal dsMatArt As DataSet, ByVal Idioma As String, ByRef iNumError As Short, ByRef strError As String) As Boolean
		Dim Maper As Object = Nothing
		Dim Instancia As String = System.Configuration.ConfigurationManager.AppSettings("instanciamaper")
		Try
			EvalMapperQA = True

			Dim arrMatGmn1() As String
			ReDim arrMatGmn1(0)
			Dim arrMatGmn2() As String
			ReDim arrMatGmn2(0)
			Dim arrMatGmn3() As String
			ReDim arrMatGmn3(0)
			Dim arrMatGmn4() As String
			ReDim arrMatGmn4(0)
			Dim arrArt() As String
			ReDim arrArt(0)
			Dim arrItem() As String
			ReDim arrItem(0)
			Dim arrTablaExternaItem() As String
			ReDim arrTablaExternaItem(0)
			Dim arrTablaExternaId() As Integer
			ReDim arrTablaExternaId(0)
			Dim arrTablaExternaValor() As String
			ReDim arrTablaExternaValor(0)

			Dim HayDatos As Boolean

			HayDatos = CreaArraysQA(dsMatArt, arrMatGmn1, arrMatGmn2, arrMatGmn3, arrMatGmn4, arrArt, arrItem _
			, arrTablaExternaItem, arrTablaExternaId, arrTablaExternaValor)

			''Obtiene el nombre de la Mapper en funci�n de la Organizaci�n de Compras
			Dim NomMapper As String = ""
			Dim orgcompras As String = ""

			Dim NomMapperDat As DataSet = ObtenerNomMapper(orgcompras)
			If Not NomMapperDat.Tables(0).Rows.Count = 0 Then
				NomMapper = NomMapperDat.Tables(0).Rows(0).Item(0)
			End If

			If HayDatos And NomMapper <> "" Then

				''En primer lugar valida las ordenes de coste o inversi�n
				Dim arrAtribMapId() As Integer
				ReDim arrAtribMapId(0)
				Dim arrAtribMapValor() As String
				ReDim arrAtribMapValor(0)

				If Not Maper Is Nothing Then
					Maper = Nothing
				End If
				Maper = CreateObject(NomMapper & ".clsInPedidosDirectos")

				''En segundo lugar, el resto de validaciones 
				If EvalMapperQA Then
					If Maper Is Nothing Then
						EvalMapperQA = True
						Exit Function
					End If
					EvalMapperQA = Maper.TratarValidacionQA(iNumError, strError _
						, arrArt, arrItem _
						, arrTablaExternaItem, arrTablaExternaId, arrTablaExternaValor)
				End If
			End If
		Catch ex As Exception
			EvalMapperQA = True
		Finally
			Maper = Nothing
		End Try
	End Function
	Private Function NecesarioValidarOrden(ByRef arrAtribId() As Integer, ByRef arrAtribValor() As String _
	, ByRef arrAtribValERP() As String, ByRef arrAtribMapId() As Integer, ByRef arrAtribMapValor() As String) As Boolean
		Dim k As Integer = -1

		For i As Integer = 0 To UBound(arrAtribId, 1)
			If arrAtribValor(i) <> "" And arrAtribValERP(i) = "1" Then
				ReDim Preserve arrAtribMapId(k + 1)
				ReDim Preserve arrAtribMapValor(k + 1)

				arrAtribMapId(k + 1) = arrAtribId(i)
				arrAtribMapValor(k + 1) = arrAtribValor(i)

				k = k + 1
			End If
		Next

		Return (k <> -1)
	End Function
	''' <summary>
	''' Esta funci�n devuelve un data set con los campos de la instancia necesarios 
	''' para las validaciones de integraci�n desde la mapper.
	''' </summary>
	''' <param name="oDs">DataSet con los datos de entrada</param>
	''' <param name="oDsAux">DataSet auxiliar</param>
	''' <param name="Idioma">Idioma del usuario</param>
	''' <returns>DataSet con los datos</returns>
	''' <remarks>Llamada desde: FSNServer.ValidacionesIntegracion.ControlMapper. Tiempo m�ximo: 0,2</remarks>
	Private Function CreaListas(ByVal oDs As DataSet, ByVal oDsAux As DataSet, ByVal Idioma As String, Optional ByVal sPer As String = Nothing, Optional ByVal IdInstancia As Long = Nothing, Optional ByVal IdSolicitud As Long = Nothing, Optional ByVal lAccionRol As Long = 0, Optional ByVal sBloquesDestino As String = "") As DataSet
		Dim dsMatArt As New DataSet
		Dim dtNoDesglMat As DataTable
		Dim dtNoDesglArt As DataTable
		Dim dtNoDesglAlm As DataTable
		Dim dtNoDesglPre As DataTable
		Dim dtNoDesglAtb As DataTable
		Dim dtNoDesglProve As DataTable
		Dim dtNoDesglCentroSM As DataTable
		Dim dtNoDesglTipoPedido As DataTable
		Dim dtNoDesglPartida As DataTable
		Dim dtNoDesglFecIniRectif As DataTable
		Dim dtNoDesglFecFinRectif As DataTable
		Dim dtNoDesglFecIniSuministro As DataTable
		Dim dtNoDesglFecFinSuministro As DataTable
		Dim dtNoDesglOrgCompras As DataTable
		Dim dtNoDesglCen As DataTable
		Dim dtNoDesglPersona As DataTable
		Dim dtNoDesglMoneda As DataTable
		Dim dtSiDesglMat As DataTable
		Dim dtSiDesglArt As DataTable
		Dim dtSiDesglAlm As DataTable
		Dim dtSiDesglPre As DataTable
		Dim dtSiDesglAtb As DataTable
		Dim dtSiDesglCen As DataTable
		Dim dtSiDesglOrgCompras As DataTable
		Dim dtSiDesglTablaExt As DataTable
		Dim dtSiDesglImporteLin As DataTable
		Dim dtSiDesglCant As DataTable
		Dim dtSiDesglPrecUn As DataTable
		Dim dtSiDesglPartida As DataTable
		Dim dtSiDesglFecIniRectif As DataTable
		Dim dtSiDesglFecFinRectif As DataTable
		Dim dtSiDesglFecIniSuministro As DataTable
		Dim dtSiDesglFecFinSuministro As DataTable

		Dim dsInforGeneral As New DataTable

		dsInforGeneral = dsMatArt.Tables.Add("InforGen")

		dtNoDesglMat = dsMatArt.Tables.Add("NoDMa")
		dtNoDesglArt = dsMatArt.Tables.Add("NoDAr")
		dtNoDesglAlm = dsMatArt.Tables.Add("NoDAl")
		dtNoDesglPre = dsMatArt.Tables.Add("NoDPr")
		dtNoDesglAtb = dsMatArt.Tables.Add("NoDAt")
		dtNoDesglProve = dsMatArt.Tables.Add("NoDProve")
		dtNoDesglCentroSM = dsMatArt.Tables.Add("NoDCenSM")
		dtNoDesglTipoPedido = dsMatArt.Tables.Add("NoDTipoPed")
		dtNoDesglPartida = dsMatArt.Tables.Add("NoDPartida")
		dtNoDesglFecIniRectif = dsMatArt.Tables.Add("NoDFecIniRectif")
		dtNoDesglFecFinRectif = dsMatArt.Tables.Add("NoDFecFinRectif")
		dtNoDesglFecIniSuministro = dsMatArt.Tables.Add("NoDFecIniSuministro")
		dtNoDesglFecFinSuministro = dsMatArt.Tables.Add("NoDFecFinSuministro")
		dtNoDesglOrgCompras = dsMatArt.Tables.Add("NoDOr")
		dtNoDesglCen = dsMatArt.Tables.Add("NoDCe")
		dtNoDesglPersona = dsMatArt.Tables.Add("NoDPersona")
		dtNoDesglMoneda = dsMatArt.Tables.Add("NoDMoneda")

		dtSiDesglMat = dsMatArt.Tables.Add("SiDMa")
		dtSiDesglArt = dsMatArt.Tables.Add("SiDAr")

		dtSiDesglAlm = dsMatArt.Tables.Add("SiDAl")
		dtSiDesglPre = dsMatArt.Tables.Add("SiDPr")

		dtSiDesglAtb = dsMatArt.Tables.Add("SiDAt")
		dtSiDesglCen = dsMatArt.Tables.Add("SiDCe")

		dtSiDesglOrgCompras = dsMatArt.Tables.Add("SiDOr")

		dtSiDesglTablaExt = dsMatArt.Tables.Add("SiDTExt")
		dtSiDesglImporteLin = dsMatArt.Tables.Add("SiDIm")

		dtSiDesglCant = dsMatArt.Tables.Add("SiDCant")
		dtSiDesglPrecUn = dsMatArt.Tables.Add("SiDPrec")

		dtSiDesglPartida = dsMatArt.Tables.Add("SiDPartida")

		dtSiDesglFecIniRectif = dsMatArt.Tables.Add("SiDFecIniRectif")
		dtSiDesglFecFinRectif = dsMatArt.Tables.Add("SiDFecFinRectif")
		dtSiDesglFecIniSuministro = dsMatArt.Tables.Add("SiDFecIniSuministro")
		dtSiDesglFecFinSuministro = dsMatArt.Tables.Add("SiDFecFinSuministro")

		dtNoDesglMat.Columns.Add("GRUPO", System.Type.GetType("System.Int32"))
		dtNoDesglMat.Columns.Add("ORDEN", System.Type.GetType("System.Int32"))
		dtNoDesglMat.Columns.Add("GMN1", System.Type.GetType("System.String"))
		dtNoDesglMat.Columns.Add("GMN2", System.Type.GetType("System.String"))
		dtNoDesglMat.Columns.Add("GMN3", System.Type.GetType("System.String"))
		dtNoDesglMat.Columns.Add("GMN4", System.Type.GetType("System.String"))

		dtNoDesglArt.Columns.Add("GRUPO", System.Type.GetType("System.Int32"))
		dtNoDesglArt.Columns.Add("ORDEN", System.Type.GetType("System.Int32"))
		dtNoDesglArt.Columns.Add("ART4", System.Type.GetType("System.String"))

		dtNoDesglAlm.Columns.Add("GRUPO", System.Type.GetType("System.Int32"))
		dtNoDesglAlm.Columns.Add("ALMACEN", System.Type.GetType("System.String"))

		dtNoDesglPre.Columns.Add("GRUPO", System.Type.GetType("System.Int32"))
		dtNoDesglPre.Columns.Add("PRESUP4", System.Type.GetType("System.String"))

		dtNoDesglProve.Columns.Add("GRUPO", System.Type.GetType("System.Int32"))
		dtNoDesglProve.Columns.Add("PROVE", System.Type.GetType("System.String"))

		dtNoDesglCentroSM.Columns.Add("GRUPO", System.Type.GetType("System.Int32"))
		dtNoDesglCentroSM.Columns.Add("CENTRO_SM", System.Type.GetType("System.String"))

		dtNoDesglTipoPedido.Columns.Add("GRUPO", System.Type.GetType("System.Int32"))
		dtNoDesglTipoPedido.Columns.Add("TIPO_PEDIDO", System.Type.GetType("System.String"))

		dtNoDesglAtb.Columns.Add("GRUPO", System.Type.GetType("System.Int32"))
		dtNoDesglAtb.Columns.Add("ID", System.Type.GetType("System.Int32"))
		dtNoDesglAtb.Columns.Add("VALOR", System.Type.GetType("System.String"))
		dtNoDesglAtb.Columns.Add("VALERP", System.Type.GetType("System.Int32"))

		dtNoDesglPartida.Columns.Add("GRUPO", System.Type.GetType("System.Int32"))
		dtNoDesglPartida.Columns.Add("PARTIDA", System.Type.GetType("System.String"))

		dtNoDesglFecIniRectif.Columns.Add("GRUPO", System.Type.GetType("System.Int32"))
		dtNoDesglFecIniRectif.Columns.Add("FECINIRECTIF", System.Type.GetType("System.DateTime"))

		dtNoDesglFecFinRectif.Columns.Add("GRUPO", System.Type.GetType("System.Int32"))
		dtNoDesglFecFinRectif.Columns.Add("FECFINRECTIF", System.Type.GetType("System.DateTime"))

		dtNoDesglFecIniSuministro.Columns.Add("GRUPO", System.Type.GetType("System.Int32"))
		dtNoDesglFecIniSuministro.Columns.Add("FECINISUMINISTRO", System.Type.GetType("System.DateTime"))

		dtNoDesglFecFinSuministro.Columns.Add("GRUPO", System.Type.GetType("System.Int32"))
		dtNoDesglFecFinSuministro.Columns.Add("FECFINSUMINISTRO", System.Type.GetType("System.DateTime"))

		dtNoDesglOrgCompras.Columns.Add("GRUPO", System.Type.GetType("System.Int32"))
		dtNoDesglOrgCompras.Columns.Add("ORGCOMPRAS", System.Type.GetType("System.String"))
		dtNoDesglCen.Columns.Add("GRUPO", System.Type.GetType("System.Int32"))
		dtNoDesglCen.Columns.Add("CENTRO", System.Type.GetType("System.String"))

		dtNoDesglPersona.Columns.Add("GRUPO", System.Type.GetType("System.Int32"))
		dtNoDesglPersona.Columns.Add("PER", System.Type.GetType("System.String"))

		dtNoDesglMoneda.Columns.Add("GRUPO", System.Type.GetType("System.Int32"))
		dtNoDesglMoneda.Columns.Add("MONEDA", System.Type.GetType("System.String"))

		dtSiDesglMat.Columns.Add("LINEA", System.Type.GetType("System.Int32"))
		dtSiDesglMat.Columns.Add("PADRE", System.Type.GetType("System.Int32"))
		dtSiDesglMat.Columns.Add("GMN1", System.Type.GetType("System.String"))
		dtSiDesglMat.Columns.Add("GMN2", System.Type.GetType("System.String"))
		dtSiDesglMat.Columns.Add("GMN3", System.Type.GetType("System.String"))
		dtSiDesglMat.Columns.Add("GMN4", System.Type.GetType("System.String"))

		dtSiDesglArt.Columns.Add("LINEA", System.Type.GetType("System.Int32"))
		dtSiDesglArt.Columns.Add("PADRE", System.Type.GetType("System.Int32"))
		dtSiDesglArt.Columns.Add("ART4", System.Type.GetType("System.String"))

		dtSiDesglAlm.Columns.Add("LINEA", System.Type.GetType("System.Int32"))
		dtSiDesglAlm.Columns.Add("PADRE", System.Type.GetType("System.Int32"))
		dtSiDesglAlm.Columns.Add("ALMACEN", System.Type.GetType("System.String"))

		dtSiDesglPre.Columns.Add("LINEA", System.Type.GetType("System.Int32"))
		dtSiDesglPre.Columns.Add("PADRE", System.Type.GetType("System.Int32"))
		dtSiDesglPre.Columns.Add("PRESUP4", System.Type.GetType("System.String"))

		dtSiDesglAtb.Columns.Add("LINEA", System.Type.GetType("System.Int32"))
		dtSiDesglAtb.Columns.Add("PADRE", System.Type.GetType("System.Int32"))
		dtSiDesglAtb.Columns.Add("ID", System.Type.GetType("System.Int32"))
		dtSiDesglAtb.Columns.Add("VALOR", System.Type.GetType("System.String"))
		dtSiDesglAtb.Columns.Add("VALERP", System.Type.GetType("System.String"))

		dtSiDesglCen.Columns.Add("LINEA", System.Type.GetType("System.Int32"))
		dtSiDesglCen.Columns.Add("PADRE", System.Type.GetType("System.Int32"))
		dtSiDesglCen.Columns.Add("CENTRO", System.Type.GetType("System.String"))

		dtSiDesglOrgCompras.Columns.Add("LINEA", System.Type.GetType("System.Int32"))
		dtSiDesglOrgCompras.Columns.Add("PADRE", System.Type.GetType("System.Int32"))
		dtSiDesglOrgCompras.Columns.Add("ORGCOMPRAS", System.Type.GetType("System.String"))

		dtSiDesglTablaExt.Columns.Add("LINEA", System.Type.GetType("System.Int32"))
		dtSiDesglTablaExt.Columns.Add("PADRE", System.Type.GetType("System.Int32"))
		dtSiDesglTablaExt.Columns.Add("TABLA_EXTERNA", System.Type.GetType("System.Int32"))
		dtSiDesglTablaExt.Columns.Add("VALOR_TABLAEXT", System.Type.GetType("System.String"))

		dtSiDesglImporteLin.Columns.Add("LINEA", System.Type.GetType("System.Int32"))
		dtSiDesglImporteLin.Columns.Add("PADRE", System.Type.GetType("System.Int32"))
		dtSiDesglImporteLin.Columns.Add("CALCULADO", System.Type.GetType("System.Int32"))
		dtSiDesglImporteLin.Columns.Add("VALOR_IMPORTE", System.Type.GetType("System.String"))

		dtSiDesglCant.Columns.Add("LINEA", System.Type.GetType("System.Int32"))
		dtSiDesglCant.Columns.Add("PADRE", System.Type.GetType("System.Int32"))
		dtSiDesglCant.Columns.Add("CANTIDAD", System.Type.GetType("System.Double"))

		dtSiDesglPrecUn.Columns.Add("LINEA", System.Type.GetType("System.Int32"))
		dtSiDesglPrecUn.Columns.Add("PADRE", System.Type.GetType("System.Int32"))
		dtSiDesglPrecUn.Columns.Add("PRECIO", System.Type.GetType("System.Double"))

		dtSiDesglPartida.Columns.Add("LINEA", System.Type.GetType("System.Int32"))
		dtSiDesglPartida.Columns.Add("PADRE", System.Type.GetType("System.Int32"))
		dtSiDesglPartida.Columns.Add("PARTIDA", System.Type.GetType("System.String"))

		dtSiDesglFecIniRectif.Columns.Add("LINEA", System.Type.GetType("System.Int32"))
		dtSiDesglFecIniRectif.Columns.Add("PADRE", System.Type.GetType("System.Int32"))
		dtSiDesglFecIniRectif.Columns.Add("FECINIRECTIF", System.Type.GetType("System.DateTime"))

		dtSiDesglFecFinRectif.Columns.Add("LINEA", System.Type.GetType("System.Int32"))
		dtSiDesglFecFinRectif.Columns.Add("PADRE", System.Type.GetType("System.Int32"))
		dtSiDesglFecFinRectif.Columns.Add("FECFINRECTIF", System.Type.GetType("System.DateTime"))

		dtSiDesglFecIniSuministro.Columns.Add("LINEA", System.Type.GetType("System.Int32"))
		dtSiDesglFecIniSuministro.Columns.Add("PADRE", System.Type.GetType("System.Int32"))
		dtSiDesglFecIniSuministro.Columns.Add("FECINISUMINISTRO", System.Type.GetType("System.DateTime"))

		dtSiDesglFecFinSuministro.Columns.Add("LINEA", System.Type.GetType("System.Int32"))
		dtSiDesglFecFinSuministro.Columns.Add("PADRE", System.Type.GetType("System.Int32"))
		dtSiDesglFecFinSuministro.Columns.Add("FECFINSUMINISTRO", System.Type.GetType("System.DateTime"))

		dsInforGeneral.Columns.Add("PERSONA", System.Type.GetType("System.String"))
		dsInforGeneral.Columns.Add("INSTANCIA", System.Type.GetType("System.Int32"))
		dsInforGeneral.Columns.Add("SOLICITUD", System.Type.GetType("System.Int32"))
		dsInforGeneral.Columns.Add("ACCION", System.Type.GetType("System.Int32"))

		Dim row As DataRow
		Dim dtNew As DataRow
		Dim sMaterial As String
		Dim iDato As Integer
		Dim iGrupo As Integer
		Dim lTablaExt As Long

		Dim TipoGs As Integer

		''Informaci�n General
		dtNew = dsInforGeneral.NewRow
		dtNew.Item("PERSONA") = IIf(IsNothing(sPer), "", sPer)
		dtNew.Item("INSTANCIA") = IIf(IsNothing(IdInstancia), 0, IdInstancia)
		dtNew.Item("SOLICITUD") = IIf(IsNothing(IdSolicitud), 0, IdSolicitud)
		dtNew.Item("ACCION") = IIf(IsNothing(lAccionRol), 0, lAccionRol)
		dsInforGeneral.Rows.Add(dtNew)

		For Each row In oDs.Tables("TEMP").Rows
			If (IsDBNull(row("TIPOGS"))) Then
			Else
				If Not IsDBNull(row("VALOR_TEXT")) OrElse Not IsDBNull(row("VALOR_NUM")) OrElse Not IsDBNull(row("VALOR_FEC")) OrElse Not IsDBNull(row("VALOR_BOOL")) Then

					TipoGs = row("TIPOGS")
					If Not IsDBNull(row("GRUPO")) Then
						iGrupo = row("GRUPO")
					Else
						iGrupo = 0
					End If

					If Not IsDBNull(row("VALOR_TEXT")) Then
						Select Case TipoGs
							Case TiposDeDatos.TipoCampoGS.Material
								dtNew = dtNoDesglMat.NewRow

								dtNew.Item("GRUPO") = iGrupo
								dtNew.Item("ORDEN") = iDato

								sMaterial = row("VALOR_TEXT")
								dtNew.Item("GMN1") = Trim(Left(sMaterial, moLongitudesDeCodigos.giLongCodGMN1))
								sMaterial = Mid(sMaterial, moLongitudesDeCodigos.giLongCodGMN1 + 1)
								dtNew.Item("GMN2") = Trim(Left(sMaterial, moLongitudesDeCodigos.giLongCodGMN2))
								sMaterial = Mid(sMaterial, moLongitudesDeCodigos.giLongCodGMN2 + 1)
								dtNew.Item("GMN3") = Trim(Left(sMaterial, moLongitudesDeCodigos.giLongCodGMN3))
								sMaterial = Mid(sMaterial, moLongitudesDeCodigos.giLongCodGMN3 + 1)
								dtNew.Item("GMN4") = Trim(sMaterial)

								dtNoDesglMat.Rows.Add(dtNew)
							Case TiposDeDatos.TipoCampoGS.NuevoCodArticulo
								dtNew = dtNoDesglArt.NewRow

								dtNew.Item("GRUPO") = iGrupo
								dtNew.Item("ORDEN") = iDato
								dtNew.Item("ART4") = row("VALOR_TEXT")

								dtNoDesglArt.Rows.Add(dtNew)
							Case TiposDeDatos.TipoCampoGS.Pres4
								dtNew = dtNoDesglPre.NewRow

								dtNew.Item("GRUPO") = iGrupo
								dtNew.Item("PRESUP4") = row("VALOR_TEXT")

								dtNoDesglPre.Rows.Add(dtNew)
							Case TiposDeDatos.TipoCampoGS.Proveedor
								dtNew = dtNoDesglProve.NewRow

								dtNew.Item("GRUPO") = iGrupo
								dtNew.Item("PROVE") = row("VALOR_TEXT")

								dtNoDesglProve.Rows.Add(dtNew)
							Case TiposDeDatos.TipoCampoGS.CentroCoste
								dtNew = dtNoDesglCentroSM.NewRow

								dtNew.Item("GRUPO") = iGrupo
								dtNew.Item("CENTRO_SM") = row("VALOR_TEXT")

								dtNoDesglCentroSM.Rows.Add(dtNew)
							Case TiposDeDatos.TipoCampoGS.TipoPedido
								dtNew = dtNoDesglTipoPedido.NewRow

								dtNew.Item("GRUPO") = iGrupo
								dtNew.Item("TIPO_PEDIDO") = row("VALOR_TEXT")

								dtNoDesglTipoPedido.Rows.Add(dtNew)
							Case TiposDeDatos.TipoCampoGS.Partida
								dtNew = dtNoDesglPartida.NewRow

								dtNew.Item("GRUPO") = iGrupo
								dtNew.Item("PARTIDA") = row("VALOR_TEXT")

								dtNoDesglPartida.Rows.Add(dtNew)
							Case TiposDeDatos.TipoCampoGS.OrganizacionCompras
								dtNew = dtNoDesglOrgCompras.NewRow
								dtNew.Item("GRUPO") = iGrupo
								dtNew.Item("ORGCOMPRAS") = row("VALOR_TEXT")
								dtNoDesglOrgCompras.Rows.Add(dtNew)
							Case TiposDeDatos.TipoCampoGS.Centro
								dtNew = dtNoDesglCen.NewRow
								dtNew.Item("GRUPO") = iGrupo
								dtNew.Item("CENTRO") = row("VALOR_TEXT")
								dtNoDesglCen.Rows.Add(dtNew)
							Case TiposDeDatos.TipoCampoGS.SinTipo
								If Not IsDBNull(row("IDATRIB")) Then
									dtNew = dtNoDesglAtb.NewRow

									dtNew.Item("GRUPO") = iGrupo
									dtNew.Item("ID") = row("IDATRIB")
									dtNew.Item("VALERP") = row("VALERP")
									dtNew.Item("VALOR") = row("VALOR_TEXT")

									dtNoDesglAtb.Rows.Add(dtNew)
								End If
							Case TiposDeDatos.TipoCampoGS.Persona
								dtNew = dtNoDesglPersona.NewRow
								dtNew.Item("GRUPO") = iGrupo
								dtNew.Item("PER") = row("VALOR_TEXT")
								dtNoDesglPersona.Rows.Add(dtNew)
							Case TiposDeDatos.TipoCampoGS.Moneda
								dtNew = dtNoDesglMoneda.NewRow
								dtNew.Item("GRUPO") = iGrupo
								dtNew.Item("MONEDA") = row("VALOR_TEXT")
								dtNoDesglMoneda.Rows.Add(dtNew)
							Case Else
								'Nada
						End Select
					ElseIf Not IsDBNull(row("VALOR_NUM")) Then
						Select Case TipoGs
							Case TiposDeDatos.TipoCampoGS.Almacen
								dtNew = dtNoDesglAlm.NewRow
								dtNew.Item("GRUPO") = iGrupo
								dtNew.Item("ALMACEN") = row("VALOR_NUM")
								dtNoDesglAlm.Rows.Add(dtNew)
							Case TiposDeDatos.TipoCampoGS.SinTipo
								If Not IsDBNull(row("IDATRIB")) Then
									dtNew = dtNoDesglAtb.NewRow
									dtNew.Item("GRUPO") = iGrupo
									dtNew.Item("ID") = row("IDATRIB")
									dtNew.Item("VALERP") = row("VALERP")
									dtNew.Item("VALOR") = row("VALOR_NUM")
									dtNoDesglAtb.Rows.Add(dtNew)
								End If
						End Select
					ElseIf Not IsDBNull(row("VALOR_FEC")) Then
						If TipoGs = TiposDeDatos.TipoCampoGS.InicioAbono Then
							dtNew = dtNoDesglFecIniRectif.NewRow

							dtNew.Item("GRUPO") = iGrupo
							dtNew.Item("FECINIRECTIF") = row("VALOR_FEC")

							dtNoDesglFecIniRectif.Rows.Add(dtNew)
						End If
						If TipoGs = TiposDeDatos.TipoCampoGS.FinAbono Then
							dtNew = dtNoDesglFecFinRectif.NewRow

							dtNew.Item("GRUPO") = iGrupo
							dtNew.Item("FECFINRECTIF") = row("VALOR_FEC")

							dtNoDesglFecFinRectif.Rows.Add(dtNew)
						End If
						If TipoGs = TiposDeDatos.TipoCampoGS.IniSuministro Then
							dtNew = dtNoDesglFecIniSuministro.NewRow

							dtNew.Item("GRUPO") = iGrupo
							dtNew.Item("FECINISUMINISTRO") = row("VALOR_FEC")

							dtNoDesglFecIniSuministro.Rows.Add(dtNew)
						End If
						If TipoGs = TiposDeDatos.TipoCampoGS.FinSuministro Then
							dtNew = dtNoDesglFecFinSuministro.NewRow

							dtNew.Item("GRUPO") = iGrupo
							dtNew.Item("FECFINSUMINISTRO") = row("VALOR_FEC")

							dtNoDesglFecFinSuministro.Rows.Add(dtNew)
						End If
					ElseIf Not IsDBNull(row("VALOR_BOOL")) Then
						TipoGs = row("TIPOGS")
						Select Case TipoGs
							Case TiposDeDatos.TipoCampoGS.SinTipo
								If Not IsDBNull(row("IDATRIB")) Then
									dtNew = dtNoDesglAtb.NewRow

									dtNew.Item("GRUPO") = iGrupo
									dtNew.Item("ID") = row("IDATRIB")
									dtNew.Item("VALERP") = row("VALERP")
									dtNew.Item("VALOR") = row("VALOR_BOOL")
									dtNoDesglAtb.Rows.Add(dtNew)
								End If
						End Select
					End If
				End If
			End If
		Next

		Dim dtNewS As DataRow
		For Each row In oDs.Tables("TEMPDESGLOSE").Rows
			'Tipo GS:
			If Not IsDBNull(row("TIPOGS")) Then
				If Not IsDBNull(row("VALOR_TEXT")) OrElse Not IsDBNull(row("VALOR_NUM")) Then
					TipoGs = row("TIPOGS")
					If Not IsDBNull(row("VALOR_TEXT")) Then
						Select Case TipoGs
							Case TiposDeDatos.TipoCampoGS.Material
								dtNewS = dtSiDesglMat.NewRow

								dtNewS.Item("LINEA") = row("LINEA")
								dtNewS.Item("PADRE") = row("CAMPO_PADRE")

								sMaterial = row("VALOR_TEXT")

								dtNewS.Item("GMN1") = Trim(Left(sMaterial, moLongitudesDeCodigos.giLongCodGMN1))
								sMaterial = Mid(sMaterial, moLongitudesDeCodigos.giLongCodGMN1 + 1)
								dtNewS.Item("GMN2") = Trim(Left(sMaterial, moLongitudesDeCodigos.giLongCodGMN2))
								sMaterial = Mid(sMaterial, moLongitudesDeCodigos.giLongCodGMN2 + 1)
								dtNewS.Item("GMN3") = Trim(Left(sMaterial, moLongitudesDeCodigos.giLongCodGMN3))
								sMaterial = Mid(sMaterial, moLongitudesDeCodigos.giLongCodGMN3 + 1)
								dtNewS.Item("GMN4") = Trim(sMaterial)

								dtSiDesglMat.Rows.Add(dtNewS)
							Case TiposDeDatos.TipoCampoGS.NuevoCodArticulo
								dtNewS = dtSiDesglArt.NewRow

								dtNewS.Item("LINEA") = row("LINEA")
								dtNewS.Item("PADRE") = row("CAMPO_PADRE")
								dtNewS.Item("ART4") = Trim(row("VALOR_TEXT"))

								dtSiDesglArt.Rows.Add(dtNewS)
							Case TiposDeDatos.TipoCampoGS.Pres4
								dtNewS = dtSiDesglPre.NewRow

								dtNewS.Item("LINEA") = row("LINEA")
								dtNewS.Item("PADRE") = row("CAMPO_PADRE")
								dtNewS.Item("PRESUP4") = row("VALOR_TEXT")

								dtSiDesglPre.Rows.Add(dtNewS)
							Case TiposDeDatos.TipoCampoGS.SinTipo
								If Not IsDBNull(row("IDATRIB")) Then
									dtNewS = dtSiDesglAtb.NewRow

									dtNewS.Item("LINEA") = row("LINEA")
									dtNewS.Item("PADRE") = row("CAMPO_PADRE")

									dtNewS.Item("ID") = row("IDATRIB")
									dtNewS.Item("VALERP") = row("VALERP")

									dtNewS.Item("VALOR") = row("VALOR_TEXT")

									dtSiDesglAtb.Rows.Add(dtNewS)
									'TablaExterna
								ElseIf Not IsDBNull(row("TABLA_EXTERNA")) Then
									lTablaExt = row("TABLA_EXTERNA")
									If Not IsDBNull(row("VALOR_TEXT")) Then
										dtNewS = dtSiDesglTablaExt.NewRow

										dtNewS.Item("LINEA") = row("LINEA")
										dtNewS.Item("PADRE") = row("CAMPO_PADRE")
										dtNewS.Item("VALOR_TABLAEXT") = row("VALOR_TEXT")
										dtNewS.Item("TABLA_EXTERNA") = row("TABLA_EXTERNA")
										dtSiDesglTablaExt.Rows.Add(dtNewS)
									ElseIf Not IsDBNull(row("VALOR_NUM")) Then
										dtNewS = dtSiDesglTablaExt.NewRow

										dtNewS.Item("LINEA") = row("LINEA")
										dtNewS.Item("PADRE") = row("CAMPO_PADRE")
										dtNewS.Item("VALOR_TABLAEXT") = row("VALOR_NUM")
										dtNewS.Item("TABLA_EXTERNA") = row("TABLA_EXTERNA")
										dtSiDesglTablaExt.Rows.Add(dtNewS)
									End If
								End If
							Case TiposDeDatos.TipoCampoGS.Almacen
								dtNewS = dtSiDesglAlm.NewRow

								dtNewS.Item("LINEA") = row("LINEA")
								dtNewS.Item("PADRE") = row("CAMPO_PADRE")
								dtNewS.Item("ALMACEN") = row("VALOR_TEXT")

								dtSiDesglAlm.Rows.Add(dtNewS)
							Case TiposDeDatos.TipoCampoGS.Centro
								dtNew = dtSiDesglCen.NewRow

								dtNew.Item("LINEA") = row("LINEA")
								dtNew.Item("PADRE") = row("CAMPO_PADRE")
								dtNew.Item("CENTRO") = row("VALOR_TEXT")

								dtSiDesglCen.Rows.Add(dtNew)

							Case TiposDeDatos.TipoCampoGS.OrganizacionCompras
								dtNew = dtSiDesglOrgCompras.NewRow

								dtNew.Item("LINEA") = row("LINEA")
								dtNew.Item("PADRE") = row("CAMPO_PADRE")
								dtNew.Item("ORGCOMPRAS") = row("VALOR_TEXT")

								dtSiDesglOrgCompras.Rows.Add(dtNew)

							Case TiposDeDatos.TipoCampoGS.Partida
								dtNew = dtSiDesglPartida.NewRow

								dtNew.Item("LINEA") = row("LINEA")
								dtNew.Item("PADRE") = row("CAMPO_PADRE")
								dtNew.Item("PARTIDA") = row("VALOR_TEXT")

								dtSiDesglPartida.Rows.Add(dtNew)

							Case Else
								'Nada
						End Select
					ElseIf Not IsDBNull(row("VALOR_NUM")) Then
						Select Case TipoGs
							Case TiposDeDatos.TipoCampoGS.Almacen
								dtNewS = dtSiDesglAlm.NewRow

								dtNewS.Item("LINEA") = row("LINEA")
								dtNewS.Item("PADRE") = row("CAMPO_PADRE")
								dtNewS.Item("ALMACEN") = row("VALOR_NUM")

								dtSiDesglAlm.Rows.Add(dtNewS)

							Case TiposDeDatos.TipoCampoGS.Cantidad
								dtNew = dtSiDesglCant.NewRow

								dtNew.Item("LINEA") = row("LINEA")
								dtNew.Item("PADRE") = row("CAMPO_PADRE")
								dtNew.Item("CANTIDAD") = row("VALOR_NUM")

								dtSiDesglCant.Rows.Add(dtNew)

							Case TiposDeDatos.TipoCampoGS.PrecioUnitario
								dtNew = dtSiDesglPrecUn.NewRow

								dtNew.Item("LINEA") = row("LINEA")
								dtNew.Item("PADRE") = row("CAMPO_PADRE")
								dtNew.Item("PRECIO") = row("VALOR_NUM")

								dtSiDesglPrecUn.Rows.Add(dtNew)
							Case TiposDeDatos.TipoCampoGS.SinTipo
								If Not IsDBNull(row("IDATRIB")) Then
									dtNewS = dtSiDesglAtb.NewRow

									dtNewS.Item("LINEA") = row("LINEA")
									dtNewS.Item("PADRE") = row("CAMPO_PADRE")

									dtNewS.Item("ID") = row("IDATRIB")
									dtNewS.Item("VALERP") = row("VALERP")

									dtNewS.Item("VALOR") = row("VALOR_NUM")

									dtSiDesglAtb.Rows.Add(dtNewS)
								ElseIf Not IsDBNull(row("CALCULADO")) Then
									If (UCase(row("CALCULADO_TIT")) Like "*IMPORTE*LIN*" Or UCase(row("CALCULADO_TIT")) Like "*IMPORTE*L�N*") Then
										dtNewS = dtSiDesglImporteLin.NewRow

										dtNewS.Item("LINEA") = row("LINEA")
										dtNewS.Item("PADRE") = row("CAMPO_PADRE")
										dtNewS.Item("CALCULADO") = row("CALCULADO")
										dtNewS.Item("VALOR_IMPORTE") = 0 'Inicializo
										'El valor del campo est� actualizado en el dataset dsAux;
										'Buscamos el valor del campo
										Dim dtAux As DataRow
										Dim find(2) As Object
										find(0) = row("LINEA")
										find(1) = row("CAMPO_PADRE")
										find(2) = row("CAMPO_HIJO")
										dtAux = oDsAux.Tables("TEMPDESGLOSE").Rows.Find(find)
										dtNewS.Item("VALOR_IMPORTE") = dtAux("VALOR_NUM")
										dtSiDesglImporteLin.Rows.Add(dtNewS)
										dtAux = Nothing
										find = Nothing
									End If
								End If
						End Select
					End If
				ElseIf Not IsDBNull(row("VALOR_FEC")) Then
					TipoGs = row("TIPOGS")
					Select Case TipoGs
						Case TiposDeDatos.TipoCampoGS.InicioAbono
							dtNewS = dtSiDesglFecIniRectif.NewRow

							dtNewS.Item("LINEA") = row("LINEA")
							dtNewS.Item("PADRE") = row("CAMPO_PADRE")
							dtNewS.Item("FECINIRECTIF") = row("VALOR_FEC")

							dtSiDesglFecIniRectif.Rows.Add(dtNewS)

						Case TiposDeDatos.TipoCampoGS.FinAbono
							dtNewS = dtSiDesglFecFinRectif.NewRow

							dtNewS.Item("LINEA") = row("LINEA")
							dtNewS.Item("PADRE") = row("CAMPO_PADRE")
							dtNewS.Item("FECFINRECTIF") = row("VALOR_FEC")

							dtSiDesglFecFinRectif.Rows.Add(dtNewS)

						Case TiposDeDatos.TipoCampoGS.IniSuministro
							dtNewS = dtSiDesglFecIniSuministro.NewRow

							dtNewS.Item("LINEA") = row("LINEA")
							dtNewS.Item("PADRE") = row("CAMPO_PADRE")
							dtNewS.Item("FECINISUMINISTRO") = row("VALOR_FEC")

							dtSiDesglFecIniSuministro.Rows.Add(dtNewS)

						Case TiposDeDatos.TipoCampoGS.FinSuministro
							dtNewS = dtSiDesglFecFinSuministro.NewRow

							dtNewS.Item("LINEA") = row("LINEA")
							dtNewS.Item("PADRE") = row("CAMPO_PADRE")
							dtNewS.Item("FECFINSUMINISTRO") = row("VALOR_FEC")

							dtSiDesglFecFinSuministro.Rows.Add(dtNewS)
						Case TiposDeDatos.TipoCampoGS.SinTipo
							If Not IsDBNull(row("IDATRIB")) Then
								dtNewS = dtSiDesglAtb.NewRow

								dtNewS.Item("LINEA") = row("LINEA")
								dtNewS.Item("PADRE") = row("CAMPO_PADRE")

								dtNewS.Item("ID") = row("IDATRIB")
								dtNewS.Item("VALERP") = row("VALERP")

								dtNewS.Item("VALOR") = row("VALOR_FEC")

								dtSiDesglAtb.Rows.Add(dtNewS)
							End If
					End Select
				ElseIf Not IsDBNull(row("VALOR_BOOL")) Then
					TipoGs = row("TIPOGS")
					Select Case TipoGs
						Case TiposDeDatos.TipoCampoGS.SinTipo
							If Not IsDBNull(row("IDATRIB")) Then
								dtNewS = dtSiDesglAtb.NewRow

								dtNewS.Item("LINEA") = row("LINEA")
								dtNewS.Item("PADRE") = row("CAMPO_PADRE")

								dtNewS.Item("ID") = row("IDATRIB")
								dtNewS.Item("VALERP") = row("VALERP")

								dtNewS.Item("VALOR") = row("VALOR_BOOL")

								dtSiDesglAtb.Rows.Add(dtNewS)
							End If
					End Select
				End If
				'Importe L�nea
			End If
		Next

		Return dsMatArt
	End Function
	''' <summary>
	''' Esta funci�n devuelve un data set con los campos de la instancia necesarios 
	''' para las validaciones de integraci�n desde la mapper.
	''' </summary>
	''' <param name="oDs">DataSet con los datos de entrada</param>
	''' <returns>DataSet con los datos</returns>
	''' <remarks>Llamada desde: FSNServer.ValidacionesIntegracion.ControlMapper. Tiempo m�ximo: 0,2</remarks>
	Private Function CreaListasQA(ByVal oDs As DataSet) As DataSet
		Dim dsMatArt As New DataSet
		Dim dtNoDesglArt As DataTable
		Dim dtNoDesglTablaExt As DataTable
		Dim dtSiDesglMat As DataTable
		Dim dtSiDesglArt As DataTable
		Dim dtSiDesglTablaExt As DataTable

		dtNoDesglArt = dsMatArt.Tables.Add("NoDAr")
		dtNoDesglTablaExt = dsMatArt.Tables.Add("NoDTExt")

		dtSiDesglMat = dsMatArt.Tables.Add("SiDMa")
		dtSiDesglArt = dsMatArt.Tables.Add("SiDAr")
		dtSiDesglTablaExt = dsMatArt.Tables.Add("SiDTExt")

		dtNoDesglArt.Columns.Add("GRUPO", System.Type.GetType("System.Int32"))
		dtNoDesglArt.Columns.Add("ORDEN", System.Type.GetType("System.Int32"))
		dtNoDesglArt.Columns.Add("ART4", System.Type.GetType("System.String"))

		dtNoDesglTablaExt.Columns.Add("GRUPO", System.Type.GetType("System.Int32"))
		dtNoDesglTablaExt.Columns.Add("TABLA_EXTERNA", System.Type.GetType("System.Int32"))
		dtNoDesglTablaExt.Columns.Add("VALOR_TABLAEXT", System.Type.GetType("System.String"))

		'Solo por linea dtNoDesglAtb.Columns.Add("VALERP", System.Type.GetType("System.Int32"))

		dtSiDesglMat.Columns.Add("LINEA", System.Type.GetType("System.Int32"))
		dtSiDesglMat.Columns.Add("PADRE", System.Type.GetType("System.Int32"))
		dtSiDesglMat.Columns.Add("GMN1", System.Type.GetType("System.String"))
		dtSiDesglMat.Columns.Add("GMN2", System.Type.GetType("System.String"))
		dtSiDesglMat.Columns.Add("GMN3", System.Type.GetType("System.String"))
		dtSiDesglMat.Columns.Add("GMN4", System.Type.GetType("System.String"))

		dtSiDesglArt.Columns.Add("LINEA", System.Type.GetType("System.Int32"))
		dtSiDesglArt.Columns.Add("PADRE", System.Type.GetType("System.Int32"))
		dtSiDesglArt.Columns.Add("ART4", System.Type.GetType("System.String"))

		dtSiDesglTablaExt.Columns.Add("LINEA", System.Type.GetType("System.Int32"))
		dtSiDesglTablaExt.Columns.Add("PADRE", System.Type.GetType("System.Int32"))
		dtSiDesglTablaExt.Columns.Add("TABLA_EXTERNA", System.Type.GetType("System.Int32"))
		dtSiDesglTablaExt.Columns.Add("VALOR_TABLAEXT", System.Type.GetType("System.String"))

		Dim row As DataRow
		Dim dtNew As DataRow
		'Dim sMaterial As String
		Dim iGrupo As Integer
		Dim lTablaExt As Long

		Dim TipoGs As Integer
		Dim dtNewS As DataRow

		For Each row In oDs.Tables("TEMP").Rows

			If Not IsDBNull(row("GRUPO")) Then
				iGrupo = row("GRUPO")
			Else
				iGrupo = 0
			End If

			If Not (IsDBNull(row("TIPOGS"))) Then
				If Not IsDBNull(row("VALOR_TEXT")) OrElse Not IsDBNull(row("VALOR_NUM")) Then

					TipoGs = row("TIPOGS")

					If Not IsDBNull(row("VALOR_TEXT")) Then
						Select Case TipoGs
							Case TiposDeDatos.TipoCampoGS.NuevoCodArticulo
								dtNew = dtNoDesglArt.NewRow

								dtNew.Item("GRUPO") = iGrupo
								dtNew.Item("ART4") = row("VALOR_TEXT")

								dtNoDesglArt.Rows.Add(dtNew)

							Case Else
								'Nada
						End Select

					End If
				End If
			ElseIf Not IsDBNull(row("TABLA_EXTERNA")) Then
				lTablaExt = row("TABLA_EXTERNA")
				If Not IsDBNull(row("VALOR_TEXT")) Then
					dtNewS = dtNoDesglTablaExt.NewRow

					dtNewS.Item("GRUPO") = iGrupo
					'dtNewS.Item("ORDEN") = iDato
					dtNewS.Item("VALOR_TABLAEXT") = row("VALOR_TEXT")
					dtNewS.Item("TABLA_EXTERNA") = row("TABLA_EXTERNA")
					dtNoDesglTablaExt.Rows.Add(dtNewS)
				ElseIf Not IsDBNull(row("VALOR_NUM")) Then
					dtNewS = dtNoDesglTablaExt.NewRow

					dtNewS.Item("GRUPO") = iGrupo
					'dtNewS.Item("ORDEN") = iDato
					dtNewS.Item("VALOR_TABLAEXT") = row("VALOR_NUM")
					dtNewS.Item("TABLA_EXTERNA") = row("TABLA_EXTERNA")
					dtNoDesglTablaExt.Rows.Add(dtNewS)
				Else
					'Se a�ade la fila sin valor 
					dtNewS = dtNoDesglTablaExt.NewRow
					dtNewS.Item("GRUPO") = iGrupo
					dtNewS.Item("VALOR_TABLAEXT") = ""
					dtNewS.Item("TABLA_EXTERNA") = row("TABLA_EXTERNA")
					dtNoDesglTablaExt.Rows.Add(dtNewS)
				End If
			End If
		Next

		Return dsMatArt
	End Function
	''' <summary>Funci�n que lee los campos del DataSet y crea los arrays con los valores 
	''' correspondientes. Estos arrays se pasar�n a la funci�n que realiza las validaciones
	''' definidas para cada ERP en la mapper.
	''' </summary>
	''' <param name="dsMatArt">DataSet con los datos de la instancia</param>
	''' <param name="arrMatGmn1">Array con los GMN1 del art�culo</param>
	''' <param name="arrMatGmn2">Array con los GMN2 del art�culo</param>
	''' <param name="arrMatGmn3">Array con los GMN3 del art�culo</param>
	''' <param name="arrMatGmn4">Array con los GMN4 del art�culo</param>
	''' <param name="arrArt">Array con los c�digos de art�culos</param>
	''' <param name="arrItem">Array con los items</param>
	''' <param name="arrAlm">Array con los ID de los almacenes (por l�nea)</param>
	''' <param name="arrPres">Array con los presupuestos</param>
	''' <param name="arrAtribItem">Array con los ID de los items (por cada atributo e item)</param>
	''' <param name="arrAtribId">Array con los ID de los atributos</param>
	''' <param name="arrAtribValor">Array con los valores de los atributos</param>
	''' <param name="arrAtribValERP">Array con los </param>
	''' <param name="arrCentro">Array con los centros</param>
	''' <param name="arrOrgCompras">Array con las organziaciones de compras</param>
	''' <param name="arrProceAtribOrden">Array con los ID de los campos de tipo atributo a nivel de cabecera</param>
	''' <param name="arrProceAtribId">Array con los ID de los atributos a nivel de cabecera</param>
	''' <param name="arrProceAtribValor">Array con los valores de los atributos a nivel de cabecera</param>
	''' <param name="arrTablaExternaItem">Array con los ID de los campos de tipo tabla externa</param>
	''' <param name="arrTablaExternaId">Array con los ID de las tablas externas</param>
	''' <param name="arrTablaExternaValor">Array con los valores de las tablas externas</param>
	''' <param name="arrImporteLinItem">Array con los ID de los campos de tipo importe de las l�neas</param>
	''' <param name="arrImporteLinValor">Array con el valor de los importes de las l�neas</param>    
	''' <param name="arrProve">Array con los proveedores</param>
	''' <param name="arrCentroSM">Array con los centros de coste (Centros SM)</param>
	''' <param name="arrCantItem">Cantidad del item</param>
	''' <param name="arrTipoPedido">Tipo de Pedido</param>
	''' <param name="arrPartida">Partida presupuestaria</param>
	''' <param name="arrPartidaNoDesglose">Partida presupuestaria (fuera del desglose)</param>
	''' <returns>Todos los arrays que se han creado</returns>
	''' <remarks>Llamada desde: FSNServer.ValidacionesIntegracion.EvalMapper. Tiempo m�ximo:0,2 </remarks>
	Private Function CreaArrays(ByVal dsMatArt As DataSet, ByRef arrMatGmn1() As String, ByRef arrMatGmn2() As String, ByRef arrMatGmn3() As String,
				ByRef arrMatGmn4() As String, ByRef arrArt() As String, ByRef arrItem() As String, ByRef arrAlm() As String, ByRef arrPres() As String,
				ByRef arrAtribItem() As String, ByRef arrAtribId() As Integer, ByRef arrAtribValor() As String, ByRef arrAtribValERP() As String,
				ByRef arrCentro() As String, ByRef arrOrgCompras() As String, ByRef arrProceAtribOrden() As String, ByRef arrProceAtribId() As Integer,
				ByRef arrProceAtribValor() As String, ByRef arrProceAtribValERP() As String, ByRef arrTablaExternaItem() As String, ByRef arrTablaExternaId() As Integer,
				ByRef arrTablaExternaValor() As String, ByRef arrImporteLinItem() As String, ByRef arrImporteLinValor() As Double, ByRef arrProve() As String,
				ByRef arrCentroSM() As String, ByRef arrCantItem() As Double, ByRef arrPrecItem() As Double, ByRef arrTipoPedido() As String, ByRef arrPartida() As String,
				ByRef arrPartidaNoDesglose() As String, ByRef arrFecIniRectifAbono() As String, ByRef arrFecFinRectifAbono() As String, ByRef arrOrgComprasNoDesgl() As String,
				ByRef arrCentroNoDesgl() As String, ByRef arrPresCab() As String, ByRef arrFechaIniSuministroC() As String, ByRef arrFechaFinSuministroC() As String,
				ByRef arrFechaIniSuministro() As String, ByRef arrFechaFinSuministro() As String, ByRef arrPersona() As String) As Boolean
		Dim iIndice As Integer = -1
		Dim iIndiceOrgLin As Integer = -1
		Dim EnCurso As String = ""
		Dim CentroLinea As String = ""
		Dim OrgComprasLinea As String = ""
		Dim iIndiceAtrib As Integer = -1
		Dim iIndiceAtribLin As Integer = -1
		Dim iIndiceTablaExtLin As Integer = -1
		Dim iIndiceImporteLin As Integer = -1
		Dim iIndiceProve As Integer = -1
		Dim iIndiceCentroSM As Integer = -1
		Dim iIndiceTipoPed As Integer = -1
		Dim iIndicePartida As Integer = -1
		Dim iIndiceFecIniRectifAbono As Integer = -1
		Dim iIndiceFecFinRectifAbono As Integer = -1
		Dim iIndiceFecIniSuministro As Integer = -1
		Dim iIndiceFecFinSuministro As Integer = -1
		Dim iIndiceFecIniSuministroC As Integer = -1
		Dim iIndiceFecFinSuministroC As Integer = -1
		Dim iIndiceOrg As Integer = -1
		Dim iIndiceCentro As Integer = -1
		Dim iIndicePres As Integer = -1

		CreaArrays = True

		ReDim Preserve arrMatGmn1(iIndice + 1)
		ReDim Preserve arrMatGmn2(iIndice + 1)
		ReDim Preserve arrMatGmn3(iIndice + 1)
		ReDim Preserve arrMatGmn4(iIndice + 1)
		ReDim Preserve arrArt(iIndice + 1)
		ReDim Preserve arrItem(iIndice + 1)
		ReDim Preserve arrAlm(iIndice + 1)
		ReDim Preserve arrPresCab(iIndice + 1)
		ReDim Preserve arrProve(iIndice + 1)
		ReDim Preserve arrCentroSM(iIndice + 1)
		ReDim Preserve arrTipoPedido(iIndice + 1)
		ReDim Preserve arrPartidaNoDesglose(iIndice + 1)
		ReDim Preserve arrOrgComprasNoDesgl(iIndice + 1)
		ReDim Preserve arrCentroNoDesgl(iIndice + 1)
		'Inicializa los arrays
		arrMatGmn1(iIndice + 1) = ""
		arrMatGmn2(iIndice + 1) = ""
		arrMatGmn3(iIndice + 1) = ""
		arrMatGmn4(iIndice + 1) = ""
		arrItem(iIndice + 1) = ""
		arrArt(iIndice + 1) = ""
		arrAlm(iIndice + 1) = ""
		arrPresCab(iIndice + 1) = ""
		arrProve(iIndice + 1) = ""
		arrCentroSM(iIndice + 1) = ""
		arrTipoPedido(iIndice + 1) = ""
		arrPartidaNoDesglose(iIndice + 1) = ""
		arrOrgComprasNoDesgl(iIndice + 1) = ""
		arrCentroNoDesgl(iIndice + 1) = ""

		iIndice = -1
		For Each rowMat As DataRow In dsMatArt.Tables("NoDMa").Rows
			arrMatGmn1(iIndice + 1) = rowMat("GMN1")
			arrMatGmn2(iIndice + 1) = rowMat("GMN2")
			arrMatGmn3(iIndice + 1) = rowMat("GMN3")
			arrMatGmn4(iIndice + 1) = rowMat("GMN4")
			arrItem(iIndice + 1) = rowMat("GRUPO")
			For Each rowArt As DataRow In dsMatArt.Tables("NoDAr").Rows
				If rowArt("GRUPO") = rowMat("GRUPO") Then
					If rowArt("ORDEN") - 1 = rowMat("ORDEN") Then
						arrArt(iIndice + 1) = rowArt("ART4")
						Exit For
					End If
				End If
			Next

			iIndice = iIndice + 1
		Next
		'
		iIndice = -1
		For Each rowAlm As DataRow In dsMatArt.Tables("NoDAl").Rows
			arrAlm(iIndice + 1) = rowAlm("ALMACEN")
			iIndice = iIndice + 1
		Next
		'
		iIndice = -1
		For Each rowPr As DataRow In dsMatArt.Tables("NoDPr").Rows
			arrPresCab(iIndice + 1) = IIf(rowPr("PRESUP4") <> "", rowPr("PRESUP4"), "")
			iIndice = iIndice + 1
		Next
		'
		For Each rowIt As DataRow In dsMatArt.Tables("NoDAt").Rows
			ReDim Preserve arrProceAtribOrden(iIndiceAtrib + 1)
			ReDim Preserve arrProceAtribId(iIndiceAtrib + 1)
			ReDim Preserve arrProceAtribValor(iIndiceAtrib + 1)
			ReDim Preserve arrProceAtribValERP(iIndiceAtrib + 1)

			arrProceAtribOrden(iIndiceAtrib + 1) = "PM"
			arrProceAtribId(iIndiceAtrib + 1) = rowIt("ID")
			arrProceAtribValor(iIndiceAtrib + 1) = rowIt("VALOR")
			arrProceAtribValERP(iIndiceAtrib + 1) = rowIt("VALERP")

			iIndiceAtrib = iIndiceAtrib + 1
		Next
		'
		For Each rowProve As DataRow In dsMatArt.Tables("NoDProve").Rows
			ReDim Preserve arrProve(iIndiceProve + 1)

			arrProve(iIndiceProve + 1) = rowProve("PROVE")

			iIndiceProve = iIndiceProve + 1
		Next

		For Each rowCentroSM As DataRow In dsMatArt.Tables("NoDCenSM").Rows
			ReDim Preserve arrCentroSM(iIndiceCentroSM + 1)

			arrCentroSM(iIndiceCentroSM + 1) = rowCentroSM("CENTRO_SM")

			iIndiceCentroSM = iIndiceProve + 1
		Next

		For Each rowCentroSM As DataRow In dsMatArt.Tables("NoDTipoPed").Rows
			ReDim Preserve arrTipoPedido(iIndiceTipoPed + 1)

			arrTipoPedido(iIndiceTipoPed + 1) = rowCentroSM("TIPO_PEDIDO")

			iIndiceTipoPed = iIndiceTipoPed + 1
		Next

		For Each rowPartida As DataRow In dsMatArt.Tables("NoDPartida").Rows
			ReDim Preserve arrPartidaNoDesglose(iIndicePartida + 1)

			arrPartidaNoDesglose(iIndicePartida + 1) = rowPartida("PARTIDA")

			iIndicePartida = iIndicePartida + 1
		Next

		For Each rowFecIniRectif As DataRow In dsMatArt.Tables("NoDFecIniRectif").Rows
			ReDim Preserve arrFecIniRectifAbono(iIndiceFecIniRectifAbono + 1)

			arrFecIniRectifAbono(iIndiceFecIniRectifAbono + 1) = rowFecIniRectif("FECINIRECTIF")

			iIndiceFecIniRectifAbono = iIndiceFecIniRectifAbono + 1
		Next

		For Each rowFecFinRectif As DataRow In dsMatArt.Tables("NoDFecFinRectif").Rows
			ReDim Preserve arrFecFinRectifAbono(iIndiceFecFinRectifAbono + 1)

			arrFecFinRectifAbono(iIndiceFecFinRectifAbono + 1) = rowFecFinRectif("FECFINRECTIF")

			iIndiceFecFinRectifAbono = iIndiceFecFinRectifAbono + 1
		Next


		For Each rowFecIniSumninistroC As DataRow In dsMatArt.Tables("NoDFecIniSuministro").Rows
			ReDim Preserve arrFechaIniSuministroC(iIndiceFecIniSuministroC + 1)

			arrFechaIniSuministroC(iIndiceFecIniSuministroC + 1) = CDate(rowFecIniSumninistroC("FECINISUMINISTRO")).ToString("yyyy-MM-dd")

			iIndiceFecIniSuministroC = iIndiceFecIniSuministroC + 1
		Next


		For Each rowFecFinSuministroC As DataRow In dsMatArt.Tables("NoDFecFinSuministro").Rows
			ReDim Preserve arrFechaFinSuministroC(iIndiceFecFinSuministroC + 1)

			arrFechaFinSuministroC(iIndiceFecFinSuministroC + 1) = CDate(rowFecFinSuministroC("FECFINSUMINISTRO")).ToString("yyyy-MM-dd")

			iIndiceFecFinSuministroC = iIndiceFecFinSuministroC + 1
		Next

		For Each rowOrgCompras As DataRow In dsMatArt.Tables("NoDOr").Rows
			ReDim Preserve arrOrgCompras(iIndiceOrg + 1)
			arrOrgComprasNoDesgl(iIndiceOrg + 1) = rowOrgCompras("ORGCOMPRAS")
			iIndiceOrg = iIndiceOrg + 1
		Next

		For Each rowCentro As DataRow In dsMatArt.Tables("NoDCe").Rows
			ReDim Preserve arrCentroNoDesgl(iIndiceCentro + 1)
			arrCentroNoDesgl(iIndiceCentro + 1) = rowCentro("CENTRO")
			iIndiceCentro = iIndiceCentro + 1
		Next

		iIndice = -1
		For Each rowMat As DataRow In dsMatArt.Tables("SiDMa").Rows
			ReDim Preserve arrMatGmn1(iIndice + 1)
			ReDim Preserve arrMatGmn2(iIndice + 1)
			ReDim Preserve arrMatGmn3(iIndice + 1)
			ReDim Preserve arrMatGmn4(iIndice + 1)
			ReDim Preserve arrArt(iIndice + 1)
			ReDim Preserve arrItem(iIndice + 1)
			ReDim Preserve arrAlm(iIndice + 1)
			ReDim Preserve arrPres(iIndice + 1)
			ReDim Preserve arrCentro(iIndice + 1)
			ReDim Preserve arrOrgCompras(iIndice + 1)
			ReDim Preserve arrCantItem(iIndice + 1)
			ReDim Preserve arrPrecItem(iIndice + 1)
			ReDim Preserve arrPartida(iIndice + 1)

			arrMatGmn1(iIndice + 1) = rowMat("GMN1")
			arrMatGmn2(iIndice + 1) = rowMat("GMN2")
			arrMatGmn3(iIndice + 1) = rowMat("GMN3")
			arrMatGmn4(iIndice + 1) = rowMat("GMN4")
			arrArt(iIndice + 1) = ""
			arrItem(iIndice + 1) = rowMat("PADRE") & rowMat("LINEA")
			arrAlm(iIndice + 1) = ""
			arrPres(iIndice + 1) = ""
			'
			For Each rowArt As DataRow In dsMatArt.Tables("SiDAr").Rows
				If rowMat("PADRE") = rowArt("PADRE") Then
					If rowMat("LINEA") = rowArt("LINEA") Then
						arrArt(iIndice + 1) = rowArt("ART4")
						Exit For
					End If
				End If
			Next
			'
			For Each rowAlm As DataRow In dsMatArt.Tables("SiDAl").Rows
				If rowAlm("PADRE") = rowMat("PADRE") AndAlso rowAlm("LINEA") = rowMat("LINEA") Then
					arrAlm(iIndice + 1) = rowAlm("ALMACEN")
					Exit For
				End If
			Next
			'
			For Each rowPr As DataRow In dsMatArt.Tables("SiDPr").Rows
				If rowPr("PADRE") = rowMat("PADRE") AndAlso rowPr("LINEA") = rowMat("LINEA") Then
					arrPres(iIndice + 1) = IIf(rowPr("PRESUP4") <> "", rowPr("PRESUP4"), "")
					Exit For
				End If
			Next
			'
			''Centros:
			For Each rowCe As DataRow In dsMatArt.Tables("SiDCe").Rows
				If rowCe("PADRE") = rowMat("PADRE") AndAlso rowCe("LINEA") = rowMat("LINEA") Then
					arrCentro(iIndice + 1) = rowCe("CENTRO")
					Exit For
				End If
			Next
			''Organizaci�n compras:
			For Each rowOr As DataRow In dsMatArt.Tables("SiDOr").Rows
				If rowOr("PADRE") = rowMat("PADRE") AndAlso rowOr("LINEA") = rowMat("LINEA") Then
					arrOrgCompras(iIndice + 1) = rowOr("ORGCOMPRAS")
					Exit For
				End If
			Next
			'
			''Cantidad Item:
			For Each rowOr As DataRow In dsMatArt.Tables("SiDCant").Rows
				If rowOr("PADRE") = rowMat("PADRE") AndAlso rowOr("LINEA") = rowMat("LINEA") Then
					arrCantItem(iIndice + 1) = rowOr("CANTIDAD")
					Exit For
				End If
			Next
			'
			''Precio Item:
			For Each rowOr As DataRow In dsMatArt.Tables("SiDPrec").Rows
				If rowOr("PADRE") = rowMat("PADRE") AndAlso rowOr("LINEA") = rowMat("LINEA") Then
					arrPrecItem(iIndice + 1) = rowOr("PRECIO")
					Exit For
				End If
			Next
			'
			''Partida:
			For Each rowOr As DataRow In dsMatArt.Tables("SiDPartida").Rows
				If rowOr("PADRE") = rowMat("PADRE") AndAlso rowOr("LINEA") = rowMat("LINEA") Then
					arrPartida(iIndice + 1) = rowOr("PARTIDA")
					Exit For
				End If
			Next
			'
			iIndice = iIndice + 1
		Next
		'
		For Each rowIt As DataRow In dsMatArt.Tables("SiDAt").Rows
			ReDim Preserve arrAtribItem(iIndiceAtribLin + 1)
			ReDim Preserve arrAtribId(iIndiceAtribLin + 1)
			ReDim Preserve arrAtribValor(iIndiceAtribLin + 1)
			ReDim Preserve arrAtribValERP(iIndiceAtribLin + 1)

			arrAtribItem(iIndiceAtribLin + 1) = CStr(rowIt("PADRE")) & CStr(rowIt("LINEA"))

			arrAtribId(iIndiceAtribLin + 1) = rowIt("ID")
			arrAtribValor(iIndiceAtribLin + 1) = rowIt("VALOR")
			arrAtribValERP(iIndiceAtribLin + 1) = rowIt("VALERP")

			iIndiceAtribLin = iIndiceAtribLin + 1
		Next
		'TablasExternas
		For Each rowTe As DataRow In dsMatArt.Tables("SiDTExt").Rows
			ReDim Preserve arrTablaExternaItem(iIndiceTablaExtLin + 1)
			ReDim Preserve arrTablaExternaId(iIndiceTablaExtLin + 1)
			ReDim Preserve arrTablaExternaValor(iIndiceTablaExtLin + 1)

			arrTablaExternaItem(iIndiceTablaExtLin + 1) = CStr(rowTe("PADRE")) & CStr(rowTe("LINEA"))

			arrTablaExternaId(iIndiceTablaExtLin + 1) = rowTe("TABLA_EXTERNA")
			arrTablaExternaValor(iIndiceTablaExtLin + 1) = rowTe("VALOR_TABLAEXT")


			iIndiceTablaExtLin = iIndiceTablaExtLin + 1
		Next
		'Importe L�nea
		For Each rowIl As DataRow In dsMatArt.Tables("SiDIm").Rows
			ReDim Preserve arrImporteLinItem(iIndiceImporteLin + 1)
			ReDim Preserve arrImporteLinValor(iIndiceImporteLin + 1)

			arrImporteLinItem(iIndiceImporteLin + 1) = CStr(rowIl("PADRE")) & CStr(rowIl("LINEA"))
			If IsDBNull(rowIl("VALOR_IMPORTE")) Then
				arrImporteLinValor(iIndiceImporteLin + 1) = 0
			Else
				arrImporteLinValor(iIndiceImporteLin + 1) = rowIl("VALOR_IMPORTE")
			End If
			iIndiceImporteLin = iIndiceImporteLin + 1
		Next
		'Fecha Inicio Rectificacion Abono
		For Each rowFecIni As DataRow In dsMatArt.Tables("SiDFecIniRectif").Rows
			ReDim Preserve arrFecIniRectifAbono(iIndiceFecIniRectifAbono + 1)

			arrFecIniRectifAbono(iIndiceFecIniRectifAbono + 1) = CStr(rowFecIni("PADRE")) & CStr(rowFecIni("LINEA"))
			If IsDBNull(rowFecIni("FECINIRECTIF")) Then
				arrFecIniRectifAbono(iIndiceFecIniRectifAbono + 1) = 0
			Else
				arrFecIniRectifAbono(iIndiceFecIniRectifAbono + 1) = rowFecIni("FECINIRECTIF")
			End If
			iIndiceFecIniRectifAbono = iIndiceFecIniRectifAbono + 1
		Next
		'Fecha Fin Rectificacion Abono
		For Each rowFecFin As DataRow In dsMatArt.Tables("SiDFecFinRectif").Rows
			ReDim Preserve arrFecFinRectifAbono(iIndiceFecFinRectifAbono + 1)

			arrFecFinRectifAbono(iIndiceFecFinRectifAbono + 1) = CStr(rowFecFin("PADRE")) & CStr(rowFecFin("LINEA"))
			If IsDBNull(rowFecFin("FECFINRECTIF")) Then
				arrFecFinRectifAbono(iIndiceFecFinRectifAbono + 1) = 0
			Else
				arrFecFinRectifAbono(iIndiceFecFinRectifAbono + 1) = rowFecFin("FECFINRECTIF")
			End If
			iIndiceFecFinRectifAbono = iIndiceFecFinRectifAbono + 1
		Next

		'Fecha Inicio Suministro
		For Each rowFecIniSuministro As DataRow In dsMatArt.Tables("SiDFecIniSuministro").Rows
			ReDim Preserve arrFechaIniSuministro(iIndiceFecIniSuministro + 1)

			arrFechaIniSuministro(iIndiceFecIniSuministro + 1) = CStr(rowFecIniSuministro("PADRE")) & CStr(rowFecIniSuministro("LINEA"))
			If IsDBNull(rowFecIniSuministro("FECINISUMINISTRO")) Then
				arrFechaIniSuministro(iIndiceFecIniSuministro + 1) = 0
			Else
				arrFechaIniSuministro(iIndiceFecIniSuministro + 1) = CDate(rowFecIniSuministro("FECINISUMINISTRO")).ToString("yyyy-MM-dd")
			End If
			iIndiceFecIniSuministro = iIndiceFecIniSuministro + 1
		Next

		'Fecha Fin Suministro
		For Each rowFecFinSuministro As DataRow In dsMatArt.Tables("SiDFecFinSuministro").Rows
			ReDim Preserve arrFechaFinSuministro(iIndiceFecFinSuministro + 1)

			arrFechaFinSuministro(iIndiceFecFinSuministro + 1) = CStr(rowFecFinSuministro("PADRE")) & CStr(rowFecFinSuministro("LINEA"))
			If IsDBNull(rowFecFinSuministro("FECFINSUMINISTRO")) Then
				arrFechaFinSuministro(iIndiceFecFinSuministro + 1) = 0
			Else
				arrFechaFinSuministro(iIndiceFecFinSuministro + 1) = CDate(rowFecFinSuministro("FECFINSUMINISTRO")).ToString("yyyy-MM-dd")
			End If
			iIndiceFecFinSuministro = iIndiceFecFinSuministro + 1
		Next

		iIndice = 0
		For Each drPersona As DataRow In dsMatArt.Tables("NoDPersona").Rows
			arrPersona(iIndice) = drPersona("PER")
			iIndice += 1
		Next
	End Function
	''' <summary>Funci�n que lee los campos del DataSet y crea los arrays con los valores 
	''' correspondientes. Estos arrays se pasar�n a la funci�n que realiza las validaciones
	''' definidas para cada ERP en la mapper.
	''' </summary>
	''' <param name="dsMatArt">DataSet con los datos de la instancia</param>
	''' <param name="arrMatGmn1">Array con los GMN1 del art�culo</param>
	''' <param name="arrMatGmn2">Array con los GMN2 del art�culo</param>
	''' <param name="arrMatGmn3">Array con los GMN3 del art�culo</param>
	''' <param name="arrMatGmn4">Array con los GMN4 del art�culo</param>
	''' <param name="arrArt">Array con los c�digos de art�culos</param>
	''' <param name="arrItem">Array con los items</param>
	''' <param name="arrTablaExternaItem">Array con los ID de los campos de tipo tabla externa</param>
	''' <param name="arrTablaExternaId">Array con los ID de las tablas externas</param>
	''' <param name="arrTablaExternaValor">Array con los valores de las tablas externas</param>
	''' <returns>Todos los arrays que se han creado</returns>
	''' <remarks>Llamada desde: FSNServer.ValidacionesIntegracion.EvalMapper. Tiempo m�ximo:0,2 </remarks>
	Private Function CreaArraysQA(ByVal dsMatArt As DataSet, ByRef arrMatGmn1() As String, ByRef arrMatGmn2() As String _
	, ByRef arrMatGmn3() As String, ByRef arrMatGmn4() As String, ByRef arrArt() As String, ByRef arrItem() As String _
	, ByRef arrTablaExternaItem() As String, ByRef arrTablaExternaId() As Integer, ByRef arrTablaExternaValor() As String) As Boolean
		Dim iIndice As Integer = -1
		Dim iIndiceTablaExt As Integer = -1

		CreaArraysQA = True

		For Each rowArt As DataRow In dsMatArt.Tables("NoDAr").Rows

			ReDim Preserve arrArt(iIndice + 1)
			ReDim Preserve arrItem(iIndice + 1)
			ReDim Preserve arrTablaExternaId(iIndice + 1)
			ReDim Preserve arrTablaExternaValor(iIndice + 1)

			arrArt(iIndice + 1) = rowArt("ART4")
			arrItem(iIndice + 1) = rowArt("GRUPO")
			arrTablaExternaId(iIndice + 1) = 0
			arrTablaExternaValor(iIndice + 1) = ""
			arrArt(iIndice + 1) = rowArt("ART4")

			'TablasExternas 
			For Each rowTe As DataRow In dsMatArt.Tables("NoDTExt").Rows
				If rowTe("GRUPO") = rowArt("GRUPO") Then
					arrTablaExternaItem(iIndice + 1) = rowArt("GRUPO")
					arrTablaExternaId(iIndice + 1) = rowTe("TABLA_EXTERNA")
					arrTablaExternaValor(iIndice + 1) = rowTe("VALOR_TABLAEXT")
					Exit For
				End If
			Next

			iIndice = iIndice + 1
		Next
	End Function
	''' <summary>
	''' Procedimiento que carga las longitudes de los c�digos
	''' </summary>
	''' <remarks>
	''' Llamada desde: Las clases que lo necesiten
	''' Tiempo m�ximo: 0,3 seg</remarks>
	Friend Sub Load_LongitudesDeCodigos()
		Dim data As DataSet
		Dim row As DataRow
		Dim oLongs As TiposDeDatos.LongitudesDeCodigos
		On Error Resume Next
		Authenticate()
		data = DBServer.LongitudesCampo_Get()

		oLongs.giLongCodART = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 10)("LONGITUD")
		oLongs.giLongCodCAL = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 20)("LONGITUD")
		oLongs.giLongCodCOM = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 30)("LONGITUD")
		oLongs.giLongCodDEP = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 50)("LONGITUD")
		oLongs.giLongCodDEST = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 60)("LONGITUD")
		oLongs.giLongCodEQP = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 70)("LONGITUD")
		oLongs.giLongCodGMN1 = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 80)("LONGITUD")
		oLongs.giLongCodGMN2 = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 90)("LONGITUD")
		oLongs.giLongCodGMN3 = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 100)("LONGITUD")
		oLongs.giLongCodGMN4 = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 110)("LONGITUD")
		oLongs.giLongCodMON = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 120)("LONGITUD")
		oLongs.giLongCodOFEEST = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 130)("LONGITUD")
		oLongs.giLongCodPAG = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 140)("LONGITUD")
		oLongs.giLongCodPAI = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 150)("LONGITUD")
		oLongs.giLongCodPER = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 160)("LONGITUD")
		oLongs.giLongCodPERF = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 170)("LONGITUD")
		oLongs.giLongCodPRESCON1 = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 180)("LONGITUD")
		oLongs.giLongCodPRESCON2 = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 190)("LONGITUD")
		oLongs.giLongCodPRESCON3 = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 200)("LONGITUD")
		oLongs.giLongCodPRESCON4 = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 210)("LONGITUD")
		oLongs.giLongCodPRESPROY1 = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 220)("LONGITUD")
		oLongs.giLongCodPRESPROY2 = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 230)("LONGITUD")
		oLongs.giLongCodPRESPROY3 = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 240)("LONGITUD")
		oLongs.giLongCodPRESPROY4 = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 250)("LONGITUD")
		oLongs.giLongCodPROVE = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 260)("LONGITUD")
		oLongs.giLongCodPROVI = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 270)("LONGITUD")
		oLongs.giLongCodROL = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 280)("LONGITUD")
		oLongs.giLongCodUNI = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 290)("LONGITUD")
		oLongs.giLongCodUON1 = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 300)("LONGITUD")
		oLongs.giLongCodUON2 = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 310)("LONGITUD")
		oLongs.giLongCodUON3 = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 320)("LONGITUD")
		oLongs.giLongCodUON4 = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 481)("LONGITUD")
		oLongs.giLongCodUSU = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 330)("LONGITUD")
		oLongs.giLongCodACT1 = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 340)("LONGITUD")
		oLongs.giLongCodACT2 = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 350)("LONGITUD")
		oLongs.giLongCodACT3 = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 360)("LONGITUD")
		oLongs.giLongCodACT4 = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 370)("LONGITUD")
		oLongs.giLongCodACT5 = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 380)("LONGITUD")
		oLongs.giLongCodPRESCONCEP31 = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 390)("LONGITUD")
		oLongs.giLongCodPRESCONCEP32 = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 400)("LONGITUD")
		oLongs.giLongCodPRESCONCEP33 = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 410)("LONGITUD")
		oLongs.giLongCodPRESCONCEP34 = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 420)("LONGITUD")
		oLongs.giLongCodPRESCONCEP41 = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 430)("LONGITUD")
		oLongs.giLongCodPRESCONCEP42 = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 440)("LONGITUD")
		oLongs.giLongCodPRESCONCEP43 = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 450)("LONGITUD")
		oLongs.giLongCodPRESCONCEP44 = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 460)("LONGITUD")
		oLongs.giLongCodCAT1 = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 461)("LONGITUD")
		oLongs.giLongCodCAT2 = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 462)("LONGITUD")
		oLongs.giLongCodCAT3 = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 463)("LONGITUD")
		oLongs.giLongCodCAT4 = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 464)("LONGITUD")
		oLongs.giLongCodCAT5 = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 465)("LONGITUD")
		oLongs.giLongCodGRUPOPROCE = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 467)("LONGITUD")
		oLongs.giLongCodDENART = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 470)("LONGITUD")
		oLongs.giLongCodACTIVO = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 485)("LONGITUD")
		oLongs.giLongCodCENTROCOSTE = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 486)("LONGITUD")

		oLongs.giLongPRES5_NIV0Cod = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 471)("LONGITUD")
		oLongs.giLongPRES5_NIV1Cod = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 472)("LONGITUD")
		oLongs.giLongPRES5_NIV2Cod = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 473)("LONGITUD")
		oLongs.giLongPRES5_NIV3Cod = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 474)("LONGITUD")
		oLongs.giLongPRES5_NIV4Cod = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 475)("LONGITUD")

		moLongitudesDeCodigos = oLongs
	End Sub
	''' <summary>
	''' Funcion que Carga el combo maper, con los datos de los campos
	''' </summary>
	''' <param name="bSave">Si debe cogerse de copia campo o no</param>
	''' <param name="Campo">Id del campo</param>
	''' <param name="Idioma">Idioma de la aplicaci�n</param>
	''' <returns>Un DataSet con los datos de los campos para cargar la combo</returns>
	''' <remarks>
	''' Llamada desde: Ninguna
	''' tiempo m�ximo: 0,5 seg</remarks>
	Public Function CargaComboMaper(ByVal bSave As Boolean, ByVal Campo As Integer, ByVal Idioma As String,
				Optional ByVal UserCode As String = Nothing, Optional ByVal UserPassword As String = Nothing) As DataSet
		Try
			Return DBServer.DatosMapperBD(bSave, Campo, Idioma)
		Catch ex As Exception
		End Try
		Return Nothing
	End Function
	''' <summary>
	''' Funcion que devuelve los datos del Mapper de una solicitud
	''' </summary>
	''' <param name="sOrgCompras">Codigo de la organizacion de compras</param>
	''' <returns>Un DataSet con los datos del mapper a cargar</returns>
	''' <remarks>
	''' LLamada desdE: PmServer/ValidacionesIntegracion/EvalMapper
	''' tiempo m�ximo: 0,2 seg</remarks>
	Public Function ObtenerNomMapper(ByVal sOrgCompras As String) As DataSet
		Authenticate()
		Dim data As DataSet
		data = DBServer.Solicitud_Devolver_Mapper(sOrgCompras)
		Return data
	End Function
	Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
		MyBase.New(dbserver, isAuthenticated)
		moDen = New MultiIdioma
		moDescr = New MultiIdioma
	End Sub
	''' <summary>
	''' Procedimiento que carga los datos de una solicitud
	''' </summary>
	''' <param name="sIdi">Idioma de la aplicaci�n</param>
	''' <param name="bPet">Codigo del peticionario de la solicitud</param>
	''' <remarks>
	''' Llamada desde: Ninguna
	''' Tiempo m�ximo: 1 seg</remarks>
	Public Sub Load(Optional ByVal sIdi As String = Nothing, Optional ByVal bPet As Boolean = False)
		Authenticate()

		Dim data As DataSet
		Dim i As Integer
		Dim oIdiomas As New Idiomas(DBServer, mIsAuthenticated)
		Dim oIdi As Idioma

		data = DBServer.Solicitud_Load(mlID, sIdi, bPet)

		If Not data.Tables(0).Rows.Count = 0 Then
			If sIdi = Nothing Then
				oIdiomas.Load()
			Else
				oIdiomas.Add(sIdi, Nothing)
			End If

			For i = 0 To data.Tables(0).Rows.Count - 1 'EN REALIDAD SOLO DEVOLVER� UNA FILA

				msCod = data.Tables(0).Rows(i).Item("COD").ToString
				For Each oIdi In oIdiomas.Idiomas
					moDen(oIdi.Cod) = data.Tables(0).Rows(i).Item("DEN_" & oIdi.Cod).ToString
					moDescr(oIdi.Cod) = data.Tables(0).Rows(i).Item("DESCR_" & oIdi.Cod).ToString
				Next
				miTipo = data.Tables(0).Rows(i).Item("TIPO").ToString
				msDenTipo = DBNullToSomething(data.Tables(0).Rows(i).Item("DENTIPO"))
				miTipoSolicit = data.Tables(0).Rows(i).Item("TIPO_SOLICIT").ToString
				msGestor = data.Tables(0).Rows(i).Item("GESTOR").ToString
				msNombreGestor = data.Tables(0).Rows(i).Item("NOM_GESTOR").ToString
				mlFormulario = DBNullToSomething(data.Tables(0).Rows(i).Item("FORMULARIO"))
				mlWorkflow = DBNullToSomething(data.Tables(0).Rows(i).Item("WORKFLOW"))
				mbPub = (DBNullToSomething(data.Tables(0).Rows(i).Item("PUB")) = 1)
				mdPedidoDirecto = DBNullToSomething(data.Tables(0).Rows(i).Item("PEDIDO_DIRECTO"))
				mbBaja = (data.Tables(0).Rows(i).Item("BAJA") = 1)

				msDenBloque = DBNullToSomething(data.Tables(0).Rows(i).Item("DENBLOQUE"))
				miBloque = DBNullToSomething(data.Tables(0).Rows(i).Item("BLOQUE"))


				moFormulario = New Formulario(DBServer, mIsAuthenticated)
				moFormulario.Id = mlFormulario
				mdsAdjuntos = New DataTable

				mdsAdjuntos = data.Tables(1)
			Next
		End If
		data = Nothing
	End Sub

	''' <summary>
	''' Devuelve True si existe integraci�n de salida (o entrada y salida) con WCF para una empresa y una entidad
	''' </summary>
	''' <param name="iEntidad">Entidad</param>
	''' <param name="iEmpresa">Empresa</param>
	''' <returns>True si existe integraci�n. False si no.</returns>
	''' <remarks>
	''' Lllamada desde FSNWeb.App_Pages.EP.EmisionPedidoaspx.vb
	''' </remarks>
	''' <revision>asg 28/03/2014</revision>
	Public Function HayIntegracionSalidaWCF(ByVal iEntidad As Integer, Optional ByVal iEmpresa As Integer = 0, Optional iErp As Integer = 0) As Boolean
		Return DBServer.HayIntegracionSalidaWCF(iEntidad, iEmpresa, iErp)
	End Function



	''' <summary>
	''' Llama al servicio de integraci�n con la estructura de pedido y el idioma para comprobar que se puede realizar el pedido
	''' </summary>
	''' <param name="strError">Mensaje de error en caso de que no se pueda validar el pedido. Si todo ha ido bien, devuelve una cadena vac�a</param>
	''' <param name="oSolicitud">Orden de pedido</param>
	''' <param name="idioma">Idioma para el mensaje de error</param>
	''' <returns>True si todo ha ido bien. False si ha fallado algo</returns>
	''' <remarks>
	''' M�todo utilizado en FSNWeb.App_Pages.EP.EmisionPedidoBis.aspx.vb
	''' </remarks>
	''' <revision>aam 29/07/2016</revision>
	Public Function ValidacionesWCF_PM(ByRef strError As String, ByVal oSolicitud As DataSet, ByVal idioma As String,
									   Optional ByVal sBloquesDestino As String = "") As TipoValidacionIntegracion
		'Llamamos al webservice
		Dim objectInfoValidacion As CRespuestaValidacion = RemoteValidacionesWCF(oSolicitud, TablasIntegracion.PM, idioma, sBloquesDestino)
		strError = objectInfoValidacion.mensajeField
		Return CType(objectInfoValidacion.bloqueoField, TipoValidacionIntegracion)
	End Function


	''' <summary>
	''' Llama al servicio de integraci�n con la estructura de pedido y el idioma para comprobar que se puede realizar el pedido
	''' </summary>
	''' <param name="strError">Mensaje de error en caso de que no se pueda validar el pedido. Si todo ha ido bien, devuelve una cadena vac�a</param>
	''' <param name="oSolicitud">Orden de pedido</param>
	''' <param name="idioma">Idioma para el mensaje de error</param>
	''' <returns>True si todo ha ido bien. False si ha fallado algo</returns>
	''' <remarks>
	''' M�todo utilizado en FSNWeb.App_Pages.EP.EmisionPedidoBis.aspx.vb
	''' </remarks>
	''' <revision>aam 29/07/2016</revision>
	Public Function ValidacionesWCF_QA(ByRef strError As String, ByVal oSolicitud As DataSet, ByVal idioma As String) As Boolean
		'Llamamos al webservice
		Dim objectInfoValidacion As CRespuestaValidacion = RemoteValidacionesWCF_QA(oSolicitud, TablasIntegracion.PM, idioma)
		strError = objectInfoValidacion.mensajeField
		Return CType(objectInfoValidacion.bloqueoField, TipoValidacionIntegracion)
	End Function

	''' <summary>
	''' Llama al servicio de integraci�n de FSIS de validaci�n de pedidos.
	''' </summary>
	''' <param name="DS">DataSet con toda la informaci�n del pedido.</param>
	''' <param name="iEntidad">N�mero de entidad</param>
	''' <param name="sIdioma">Idioma del usuario</param>
	''' <param name="sBloquesDestino">***</param> 
	''' <returns>Devuelve el mensaje de error en caso de producirse o cadena vac�a si todo va bien.</returns>
	''' <remarks>
	''' M�todo utilizado en FSNServer.App_Classes.EP.EP_ValidacionesIntegracion
	''' </remarks>
	''' <revision>aam 28/01/2015</revision>
	Private Function RemoteValidacionesWCF(ByVal DS As DataSet, iEntidad As Integer, sIdioma As String,
										   ByVal sBloquesDestino As String) As CRespuestaValidacion
		Dim oIntegracion As New Integracion(DBServer, True)
		Dim sRuta, sNombre, sContrasenya As String
		Dim iServiceBindingType, iServiceSecurityMode, iClientCredentialType, iProxyCredentialType As Integer
		sNombre = Nothing : sContrasenya = Nothing
		Try
			''Se llama a un m�todo de FSIS para deducir el erp y de esta forma hacer las validaciones correspondientes
			oIntegracion.ObtenerCredencialesWCF(iEntidad, 0, iServiceBindingType, iServiceSecurityMode, iClientCredentialType, iProxyCredentialType, sNombre, sContrasenya)
			sRuta = oIntegracion.ObtenerRutaInstalacionFSIS()
			sRuta = Split(sRuta, "Exportar")(0) & "Validaciones.svc"

			Dim oValidacionesSolicitudRequest As ValidacionesSolicitudRequest
			Dim oValidacionesSolicitudResponse As ValidacionesSolicitudResponse

			oValidacionesSolicitudRequest = New ValidacionesSolicitudRequest(DS, iEntidad, sIdioma, sBloquesDestino)

			Dim myChannelFactory As ChannelFactory(Of IValidaciones)
			Dim myEndpoint As New EndpointAddress(sRuta)

			Select Case iServiceBindingType
				Case 1 'WSHttpBinding
					Dim iSecurityMode As SecurityMode = iServiceSecurityMode
					Dim myBinding As New WSHttpBinding(iSecurityMode)
					With myBinding
						.SendTimeout = New TimeSpan(0, ConfigurationManager.AppSettings("TimeoutMinINT"), ConfigurationManager.AppSettings("TimeoutSegINT"))
						.Security.Transport.ClientCredentialType = iClientCredentialType
						.Security.Transport.ProxyCredentialType = iProxyCredentialType
					End With
					myChannelFactory = New ChannelFactory(Of IValidaciones)(myBinding, myEndpoint)
				Case Else 'BasicHttpBinding
					Dim iBasicSecurityMode As BasicHttpSecurityMode = iServiceSecurityMode
					Dim myBinding As New BasicHttpBinding(iBasicSecurityMode)
					With myBinding
						.SendTimeout = New TimeSpan(0, ConfigurationManager.AppSettings("TimeoutMinINT"), ConfigurationManager.AppSettings("TimeoutSegINT"))
						.Security.Transport.ClientCredentialType = iClientCredentialType
						.Security.Transport.ProxyCredentialType = iProxyCredentialType
					End With
					myChannelFactory = New ChannelFactory(Of IValidaciones)(myBinding, myEndpoint)
			End Select

			With (myChannelFactory.Credentials)
				Select Case iClientCredentialType
					Case HttpClientCredentialType.Windows
						With .Windows.ClientCredential
							.UserName = sNombre
							.Password = sContrasenya
						End With
					Case HttpClientCredentialType.Basic
						With .UserName
							.UserName = sNombre
							.Password = sContrasenya
						End With
					Case HttpClientCredentialType.Certificate
						.ClientCertificate.SetCertificate(System.Security.Cryptography.X509Certificates.StoreLocation.LocalMachine,
														  System.Security.Cryptography.X509Certificates.StoreName.My,
														  System.Security.Cryptography.X509Certificates.X509FindType.FindBySubjectName, "client")
				End Select
			End With

			Dim wcfProxyClient As IValidaciones = myChannelFactory.CreateChannel()
			oValidacionesSolicitudResponse = wcfProxyClient.ValidacionesSolicitud(oValidacionesSolicitudRequest)
			myChannelFactory.Close()

			Return oValidacionesSolicitudResponse.ValidacionesSolicitudResult
		Catch ex As Exception
			DBServer.Errores_Create("FULLSTEP INT. RemoteValidacionesWCF: Entidad" & CStr(iEntidad) & " Bloque destino: " & sBloquesDestino, "", ex.GetType().FullName, ex.Message, ex.StackTrace, "", "")
			Return Nothing
		End Try
	End Function

	''' <summary>
	''' Llama al servicio de integraci�n de FSIS de validaci�n de pedidos.
	''' </summary>
	''' <param name="DS">DataSet con toda la informaci�n del pedido.</param>
	''' <param name="iEntidad">N�mero de entidad</param>
	''' <param name="sIdioma">Idioma del usuario</param>
	''' <returns>Devuelve el mensaje de error en caso de producirse o cadena vac�a si todo va bien.</returns>
	''' <remarks>
	''' M�todo utilizado en FSNServer.App_Classes.EP.EP_ValidacionesIntegracion
	''' </remarks>
	''' <revision>aam 28/01/2015</revision>
	Private Function RemoteValidacionesWCF_QA(ByVal DS As DataSet, iEntidad As Integer, sIdioma As String) As CRespuestaValidacion
		Dim oIntegracion As New Integracion(DBServer, True)
		Dim sRuta, sNombre, sContrasenya As String
		Dim iServiceBindingType, iServiceSecurityMode, iClientCredentialType, iProxyCredentialType As Integer
		sNombre = Nothing : sContrasenya = Nothing
		Try
			''Se llama a un m�todo de FSIS para deducir el erp y de esta forma hacer las validaciones correspondientes
			oIntegracion.ObtenerCredencialesWCF(iEntidad, 0, iServiceBindingType, iServiceSecurityMode, iClientCredentialType, iProxyCredentialType, sNombre, sContrasenya)
			sRuta = oIntegracion.ObtenerRutaInstalacionFSIS()
			sRuta = Split(sRuta, "Exportar")(0) & "Validaciones.svc"

			Dim oValidacionesSolicitudQARequest As ValidacionesSolicitudQARequest
			Dim oValidacionesSolicitudQAResponse As ValidacionesSolicitudQAResponse

			oValidacionesSolicitudQARequest = New ValidacionesSolicitudQARequest(DS, iEntidad, sIdioma)

			Dim myChannelFactory As ChannelFactory(Of IValidaciones)
			Dim myEndpoint As New EndpointAddress(sRuta)

			Select Case iServiceBindingType
				Case 1 'WSHttpBinding
					Dim iSecurityMode As SecurityMode = iServiceSecurityMode
					Dim myBinding As New WSHttpBinding(iSecurityMode)
					With myBinding
						.SendTimeout = New TimeSpan(0, ConfigurationManager.AppSettings("TimeoutMinINT"), ConfigurationManager.AppSettings("TimeoutSegINT"))
						.Security.Transport.ClientCredentialType = iClientCredentialType
						.Security.Transport.ProxyCredentialType = iProxyCredentialType
					End With
					myChannelFactory = New ChannelFactory(Of IValidaciones)(myBinding, myEndpoint)
				Case Else 'BasicHttpBinding
					Dim iBasicSecurityMode As BasicHttpSecurityMode = iServiceSecurityMode
					Dim myBinding As New BasicHttpBinding(iBasicSecurityMode)
					With myBinding
						.SendTimeout = New TimeSpan(0, ConfigurationManager.AppSettings("TimeoutMinINT"), ConfigurationManager.AppSettings("TimeoutSegINT"))
						.Security.Transport.ClientCredentialType = iClientCredentialType
						.Security.Transport.ProxyCredentialType = iProxyCredentialType
					End With
					myChannelFactory = New ChannelFactory(Of IValidaciones)(myBinding, myEndpoint)
			End Select

			With (myChannelFactory.Credentials)
				Select Case iClientCredentialType
					Case HttpClientCredentialType.Windows
						With .Windows.ClientCredential
							.UserName = sNombre
							.Password = sContrasenya
						End With
					Case HttpClientCredentialType.Basic
						With .UserName
							.UserName = sNombre
							.Password = sContrasenya
						End With
					Case HttpClientCredentialType.Certificate
						.ClientCertificate.SetCertificate(System.Security.Cryptography.X509Certificates.StoreLocation.LocalMachine,
														  System.Security.Cryptography.X509Certificates.StoreName.My,
														  System.Security.Cryptography.X509Certificates.X509FindType.FindBySubjectName, "client")
				End Select
			End With

			Dim wcfProxyClient As IValidaciones = myChannelFactory.CreateChannel()
			oValidacionesSolicitudQAResponse = wcfProxyClient.ValidacionesSolicitudQA(oValidacionesSolicitudQARequest)
			myChannelFactory.Close()

			Return oValidacionesSolicitudQAResponse.ValidacionesSolicitudQAResult
		Catch ex As Exception
			Return Nothing
		End Try

	End Function


End Class