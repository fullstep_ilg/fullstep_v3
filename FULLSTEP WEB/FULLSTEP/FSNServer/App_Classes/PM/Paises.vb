<Serializable()> _
    Public Class Paises
    Inherits Security

    Private Contexto As System.Web.HttpContext
    Private moPaises As DataSet

    Public ReadOnly Property Data() As Data.DataSet
        Get
            Return moPaises
        End Get
    End Property

    ''' <summary>
    ''' Carga los pa�ses
    ''' </summary>
    ''' <param name="sIdioma">Idioma</param>
    ''' <param name="sCod">C�digo del pa�s</param>
    ''' <param name="sDen">Denominaci�n del pa�s</param>
    ''' <param name="bCoincid">Si ha de coincidir o no exactamente</param>
    ''' <remarks>Llamada desde: paiseserver.vb, campos.ascx, desglose.ascx, impexp.aspx. Tiempo m�ximo: 0 sg.</remarks>
    Public Sub LoadData(ByVal sIdioma As String, Optional ByVal sCod As String = Nothing, Optional ByVal sDen As String = Nothing, Optional ByVal bCoincid As Boolean = False)
        Authenticate()

        Dim ds As DataSet
        If sCod Is Nothing AndAlso sDen Is Nothing Then
            Contexto = System.Web.HttpContext.Current
            ds = CType(Contexto.Cache("dsTodosPaises" & sIdioma), DataSet)
            If ds Is Nothing Then
                ds = DBServer.Paises_Get(sIdioma, sCod, sDen, bCoincid)
                Contexto.Cache.Insert("dsTodosPaises" & sIdioma, ds, Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration, New TimeSpan(0, System.Configuration.ConfigurationManager.AppSettings("TiempoExpiracionCacheMin"), 0))
            End If
        Else
            ds = DBServer.Paises_Get(sIdioma, sCod, sDen, bCoincid)
        End If
        moPaises = ds
    End Sub

    Public Sub New(ByRef dbserver As FSNDataBaseServer.Root,ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
    End Sub

End Class
