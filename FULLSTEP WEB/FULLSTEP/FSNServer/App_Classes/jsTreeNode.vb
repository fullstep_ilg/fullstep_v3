﻿Public Class jsTreeNode
    Public Property id As String
    Public Property text As String
    Public Property icon As String
    Public Property state As jsTreeNodeState
    Public Property parent As String

    Public Sub New(ByVal sId As String, ByVal sText As String, ByVal sIcon As String, ByVal oState As jsTreeNodeState, Optional ByVal sParent As String = "")
        id = sId
        text = sText
        icon = sIcon
        state = oState
        parent = sParent
    End Sub
End Class

Public Class jsTreeNodeState
    Public Property opened As Boolean
    Public Property disabled As Boolean
    Public Property selected As Boolean

    Public Sub New(ByVal bOpened As Boolean, ByVal bDisabled As Boolean, ByVal bSelected As Boolean)
        opened = bOpened
        disabled = bDisabled
        selected = bSelected
    End Sub
End Class