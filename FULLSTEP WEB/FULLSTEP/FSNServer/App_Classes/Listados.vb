﻿Public Class Listados
    Inherits Fullstep.FSNServer.Security

    Private moData As DataSet
    Public ReadOnly Property Data() As Data.DataSet
        Get
            Return moData
        End Get
    End Property
    ''' <summary>
    ''' Función que devuelve un dataset con una tabla que contendrá los datos solicitudos para el parámetro
    ''' </summary>
    ''' <param name="sTabla">Nombre de la tabla a la que debemos acceder en la consulta</param>
    ''' <param name="sCampo1">Nombre del primer campo a traer de la tabla </param>
    ''' <param name="sCampo2">Nombre del segundo campo a traer de la tabla (puede ser vacío)</param>
    ''' <returns>Un dataset con la tabla con los valores requeridos</returns>
    ''' <remarks>
    ''' Llamada desde: La función CargarParámetrosEnPágina de Report.aspx
    ''' Tiempo máximo: Variable en función de la tabla a acceder
    ''' </remarks>
    Public Function CargarDatos_TablaIndefinida(ByVal sTabla As String, ByVal sCampo1 As String, ByVal sCampo2 As String) As DataSet
        Authenticate()

        Dim dtDatos As New DataSet
        dtDatos = DBServer.Listados_Cargar_DatosTablaIndefinida(sTabla, sCampo1, sCampo2)

        CargarDatos_TablaIndefinida = dtDatos
    End Function
    ''' <summary>
    ''' Procedimiento que carga para un usuario concreto los listados personalizados a los que puede acceder
    ''' </summary>
    ''' <param name="sUsu">Código del USUARIO</param>
    ''' <param name="bPM">Si queremos los listados de PM</param>
    ''' <param name="bQA">Si queremos los listados de QA</param>
    ''' <param name="bEP">Si queremos los listados de EP</param>
    ''' <remarks>Llamada desde: Informes.aspx y Menu.master.vb
    ''' Tiempo máximo: 0 sec.</remarks>
    Public Sub LoadListadosPersonalizados(ByVal sUsu As String, Optional ByVal bPM As Boolean = False, Optional ByVal bQA As Boolean = False, Optional ByVal bEP As Boolean = False)
        Authenticate()

        Dim sNomCache As String = "listadosPersonalizados_" & If(bPM, "1", "0") & If(bQA, "1", "0") & If(bEP, "1", "0") & sUsu
        If System.Web.HttpContext.Current Is Nothing OrElse System.Web.HttpContext.Current.Cache(sNomCache) Is Nothing Then
            moData = DBServer.User_LoadListados(sUsu, bPM, bQA, bEP)
            If Not System.Web.HttpContext.Current Is Nothing Then System.Web.HttpContext.Current.Cache.Insert(sNomCache, moData, Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration, New TimeSpan(0, System.Configuration.ConfigurationManager.AppSettings("TiempoExpiracionCacheMin"), 0))
        Else
            moData = CType(System.Web.HttpContext.Current.Cache(sNomCache), DataSet)
        End If
    End Sub
    ''' <summary>
    ''' Constructor de la clase Listados
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si está autenticado</param>
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
End Class
