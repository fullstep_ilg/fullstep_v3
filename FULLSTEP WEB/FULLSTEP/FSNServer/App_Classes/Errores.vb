Imports System.IO
<Serializable()>
Public Class Errores
    Inherits Security
    Private mlID As Long
    Public Property ID() As Long
        Get
            Return mlID
        End Get
        Set(ByVal Value As Long)
            mlID = Value
        End Set
    End Property
    ''' <summary>
    ''' Procedimiento que crea un objeto de tipo error
    ''' </summary>
    ''' <param name="sPagina">P�gina donde se ha generado el error</param>
    ''' <param name="sUsuario">Usuario que ha generado el error</param>
    ''' <param name="sEx_FullName">Nombre de la excepcion generada</param>
    ''' <param name="sEx_Message">Mensaje de la excepcion generada</param>
    ''' <param name="sEx_StackTrace">Datos de la pila de errores</param>
    ''' <param name="sSv_Query_String">Datos del Query String pasado</param>
    ''' <param name="sSv_Http_User_Agent">Tipo de excepcion devuelta</param>
    ''' <remarks>
    ''' Llamada desde: PMWeb/Errores/Page_load, WebServicePM/QAPuntuaciones/CalcularPuntuacion - CalcularPuntuaciones - RecalcularVariables Tiempo m�ximo: 0,4 seg
    ''' </remarks>
    Public Sub Create(ByVal sPagina As String, ByVal sUsuario As String, ByVal sEx_FullName As String, ByVal sEx_Message As String, ByVal sEx_StackTrace As String, ByVal sSv_Query_String As String, ByVal sSv_Http_User_Agent As String)
        Authenticate()
        mlID = DBServer.Errores_Create(sPagina, sUsuario, sEx_FullName, sEx_Message, sEx_StackTrace, sSv_Query_String, sSv_Http_User_Agent)
    End Sub
    ''' <summary>
    ''' - Los errores no solo se producen en paginas tambien en servicios. En este caso no hay pagina
    '''   y en consecuencia muchos de los campos a grabar en la tabla Errores tienen poco sentido, tiene
    '''   mas sentido grabar datos relacionados con el fallo de bbdd.
    ''' - Tambien podr�a interesar grabar en la tabla Errores los errores en bbdd producidos en paginas
    '''   (tal vez, si deja de interesar escribir todo en log) para este caso esta el parametro opcional Usuario.
    ''' - Establece el Id del registro insertado por si interesase.
    ''' </summary>
    ''' <param name="sDonde">String libre indicador de donde se produjo el error</param>
    ''' <param name="sEx_Message">Descripci�n del error producido</param>
    ''' <param name="sUsuario">Usuario conectado en el momento de producirse el error</param>
    ''' <remarks>Llamada desde: 09042010 De momento solo esta para un servicio-> solo en root llamada a Errores_BD;Tiempo maximo: 0,1</remarks>
    Public Sub Create(ByVal sDonde As String, ByVal sEx_Message As String, Optional ByVal sUsuario As String = "")
        Authenticate()
        mlID = DBServer.Errores_BD(sDonde, sEx_Message, sUsuario)
    End Sub
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
    Public Sub SendErrorEmail(ByVal sError As String, ByVal from As String, ByVal nombreFuncion As String, ByVal usu As String, ByVal queryString As String, ByVal fromInfoDirectory As String)
        Dim objReader As New StreamReader(fromInfoDirectory)
        Dim Para As String = objReader.ReadLine
        Dim iError As Integer = 0
        Dim oNotificador As New Notificar(DBServer, mIsAuthenticated)
        oNotificador.EnviarMail(From:=from,
                                Subject:="Error Win Service XML",
                                Para:=Para,
                                Message:="Error producido en el servicio Windows",
                                iError:=iError,
                                strError:=sError & " - " & queryString,
                                isHTML:=True,
                                Producto:=Producto.OTRO,
                                bGrabaEnvio:=False)
    End Sub
End Class