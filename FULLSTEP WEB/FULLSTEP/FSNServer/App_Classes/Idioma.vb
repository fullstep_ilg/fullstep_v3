﻿Public Class Idioma
    Inherits Security

    Private msCod As String
    Private msDen As String
    Property Den() As String
        Get
            Den = msDen
        End Get
        Set(ByVal Value As String)
            msDen = Value
        End Set
    End Property
    Property Cod() As String
        Get
            Cod = msCod
        End Get
        Set(ByVal Value As String)
            msCod = Value
        End Set
    End Property
    ''' <summary>
    ''' Constructor de la clase Idioma
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si está autenticado</param>
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
End Class