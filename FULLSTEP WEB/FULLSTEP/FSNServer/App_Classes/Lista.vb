﻿Imports System.Collections.Generic
<Serializable()> _
Public MustInherit Class Lista(Of T)
    Inherits Security
    Implements IList(Of T)

    Private _lista As List(Of T)
    Public Property Capacity() As Integer
        Get
            Return _lista.Capacity
        End Get
        Set(ByVal value As Integer)
            _lista.Capacity = value
        End Set
    End Property
    Public ReadOnly Property Count() As Integer Implements System.Collections.Generic.ICollection(Of T).Count
        Get
            Return _lista.Count
        End Get
    End Property
    Default Public Property Item(ByVal index As Integer) As T Implements System.Collections.Generic.IList(Of T).Item
        Get
            If _lista.Count > 0 Then
                Return _lista.Item(index)
            Else
                Return Nothing
            End If
        End Get
        Set(ByVal value As T)
            _lista.Item(index) = value
        End Set
    End Property
    ''' <summary>
    ''' Constructor de la clase
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si está autenticado</param>
    ''' <remarks>
    ''' Llamada desde: Todos los sitios en los que se instancie un objeto de la clase COrden
    ''' Tiempo máximo: 0 sec
    ''' </remarks>
    Public Sub New(ByRef dbserver As Fullstep.FSNDataBaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
        _lista = New List(Of T)
    End Sub
    Public Sub New(ByRef dbserver As Fullstep.FSNDatabaseServer.Root, ByVal UserCode As String, ByVal UserPassword As String, ByVal isAuthenticated As Boolean, ByVal capacity As Integer)
        MyBase.New(dbserver, isAuthenticated)
        _lista = New List(Of T)(capacity)
    End Sub
    Public Sub New(ByRef dbserver As Fullstep.FSNDatabaseServer.Root, ByVal UserCode As String, ByVal UserPassword As String, ByVal isAuthenticated As Boolean, ByVal collection As System.Collections.Generic.IEnumerable(Of T))
        MyBase.New(dbserver, isAuthenticated)
        _lista = New List(Of T)(collection)
    End Sub
    Public Sub Add(ByVal item As T) Implements System.Collections.Generic.ICollection(Of T).Add
        _lista.Add(item)
    End Sub
    Public Sub AddRange(ByVal collection As System.Collections.Generic.IEnumerable(Of T))
        _lista.AddRange(collection)
    End Sub
    Public Function AsReadOnly() As System.Collections.ObjectModel.ReadOnlyCollection(Of T)
        Return _lista.AsReadOnly()
    End Function
    Public Function BinarySearch(ByVal index As Integer, ByVal count As Integer, ByVal item As T, ByVal comparer As System.Collections.Generic.IComparer(Of T)) As Integer
        Return _lista.BinarySearch(index, count, item, comparer)
    End Function
    Public Function BinarySearch(ByVal item As T) As Integer
        Return _lista.BinarySearch(item)
    End Function
    Public Function BinarySearch(ByVal item As T, ByVal comparer As System.Collections.Generic.IComparer(Of T)) As Integer
        Return _lista.BinarySearch(item, comparer)
    End Function
    Public Sub Clear() Implements System.Collections.Generic.ICollection(Of T).Clear
        _lista.Clear()
    End Sub
    Public Function Contains(ByVal item As T) As Boolean Implements System.Collections.Generic.ICollection(Of T).Contains
        Return _lista.Contains(item)
    End Function
    Public Function ConvertAll(Of TOutput)(ByVal converter As System.Converter(Of T, TOutput)) As System.Collections.Generic.List(Of TOutput)
        Return _lista.ConvertAll(converter)
    End Function
    Public Sub CopyTo(ByVal index As Integer, ByVal array() As T, ByVal arrayIndex As Integer, ByVal count As Integer)
        _lista.CopyTo(index, array, arrayIndex, count)
    End Sub
    Public Sub CopyTo(ByVal array() As T)
        _lista.CopyTo(array)
    End Sub
    Public Sub CopyTo(ByVal array() As T, ByVal arrayIndex As Integer) Implements System.Collections.Generic.ICollection(Of T).CopyTo
        _lista.CopyTo(array, arrayIndex)
    End Sub
    Public Function Exists(ByVal match As System.Predicate(Of T)) As Boolean
        Return _lista.Exists(match)
    End Function
    Public Function Find(ByVal match As System.Predicate(Of T)) As T
        Return _lista.Find(match)
    End Function
    Public Function FindAll(ByVal match As System.Predicate(Of T)) As System.Collections.Generic.List(Of T)
        Return _lista.FindAll(match)
    End Function
    Public Function FindIndex(ByVal startIndex As Integer, ByVal count As Integer, ByVal match As System.Predicate(Of T)) As Integer
        Return _lista.FindIndex(startIndex, count, match)
    End Function
    Public Function FindIndex(ByVal startIndex As Integer, ByVal match As System.Predicate(Of T)) As Integer
        Return _lista.FindIndex(startIndex, match)
    End Function
    Public Function FindIndex(ByVal match As System.Predicate(Of T)) As Integer
        Return _lista.FindIndex(match)
    End Function
    Public Function FindLast(ByVal match As System.Predicate(Of T)) As T
        Return _lista.FindLast(match)
    End Function
    Public Function FindLastIndex(ByVal startIndex As Integer, ByVal count As Integer, ByVal match As System.Predicate(Of T)) As Integer
        Return _lista.FindLastIndex(startIndex, count, match)
    End Function
    Public Function FindLastIndex(ByVal startIndex As Integer, ByVal match As System.Predicate(Of T)) As Integer
        Return _lista.FindLastIndex(startIndex, match)
    End Function
    Public Function FindLastIndex(ByVal match As System.Predicate(Of T)) As Integer
        Return _lista.FindLastIndex(match)
    End Function
    Public Sub ForEach(ByVal action As System.Action(Of T))
        _lista.ForEach(action)
    End Sub
    Public Function GetEnumerator() As System.Collections.Generic.IEnumerator(Of T) Implements System.Collections.Generic.IEnumerable(Of T).GetEnumerator
        Return _lista.GetEnumerator()
    End Function
    Public Function GetEnumerator1() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
        Return _lista.GetEnumerator()
    End Function
    Public Function GetRange(ByVal index As Integer, ByVal count As Integer) As System.Collections.Generic.List(Of T)
        Return _lista.GetRange(index, count)
    End Function
    Public Function IndexOf(ByVal item As T) As Integer Implements System.Collections.Generic.IList(Of T).IndexOf
        Return _lista.IndexOf(item)
    End Function
    Public Function IndexOf(ByVal item As T, ByVal index As Integer) As Integer
        Return _lista.IndexOf(item, index)
    End Function
    Public Function IndexOf(ByVal item As T, ByVal index As Integer, ByVal count As Integer) As Integer
        Return _lista.IndexOf(item, index, count)
    End Function
    Public Sub Insert(ByVal index As Integer, ByVal item As T) Implements System.Collections.Generic.IList(Of T).Insert
        _lista.Insert(index, item)
    End Sub
    Public Sub InsertRange(ByVal index As Integer, ByVal collection As System.Collections.Generic.IEnumerable(Of T))
        _lista.InsertRange(index, collection)
    End Sub
    Public ReadOnly Property IsReadOnly() As Boolean Implements System.Collections.Generic.ICollection(Of T).IsReadOnly
        Get
            Return False
        End Get
    End Property
    Public Function LastIndexOf(ByVal item As T) As Integer
        Return _lista.LastIndexOf(item)
    End Function
    Public Function LastIndexOf(ByVal item As T, ByVal index As Integer) As Integer
        Return _lista.LastIndexOf(item, index)
    End Function
    Public Function LastIndexOf(ByVal item As T, ByVal index As Integer, ByVal count As Integer) As Integer
        Return _lista.LastIndexOf(item, index, count)
    End Function
    Public Function Remove(ByVal item As T) As Boolean Implements System.Collections.Generic.ICollection(Of T).Remove
        Return _lista.Remove(item)
    End Function
    Public Function RemoveAll(ByVal match As System.Predicate(Of T)) As Integer
        Return _lista.RemoveAll(match)
    End Function
    Public Sub RemoveAt(ByVal index As Integer) Implements System.Collections.Generic.IList(Of T).RemoveAt
        _lista.RemoveAt(index)
    End Sub
    Public Sub RemoveRange(ByVal index As Integer, ByVal count As Integer)
        _lista.RemoveRange(index, count)
    End Sub
    Public Sub Reverse()
        _lista.Reverse()
    End Sub
    Public Sub Reverse(ByVal index As Integer, ByVal count As Integer)
        _lista.Reverse(index, count)
    End Sub
    Public Sub Sort()
        _lista.Sort()
    End Sub
    Public Sub Sort(ByVal index As Integer, ByVal count As Integer, ByVal comparer As System.Collections.Generic.IComparer(Of T))
        _lista.Sort(index, count, comparer)
    End Sub
    Public Sub Sort(ByVal comparer As System.Collections.Generic.IComparer(Of T))
        _lista.Sort(comparer)
    End Sub
    Public Sub Sort(ByVal comparison As System.Comparison(Of T))
        _lista.Sort(comparison)
    End Sub
    Public Function ToArray() As T()
        Return _lista.ToArray()
    End Function
    Public Overrides Function ToString() As String
        Return _lista.ToString()
    End Function
    Public Sub TrimExcess()
        _lista.TrimExcess()
    End Sub
    Public Function TrueForAll(ByVal match As System.Predicate(Of T)) As Boolean
        Return _lista.TrueForAll(match)
    End Function
End Class
