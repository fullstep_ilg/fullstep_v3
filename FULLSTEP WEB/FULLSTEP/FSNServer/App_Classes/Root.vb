﻿Imports System.Threading
Imports System.Reflection
Imports System.Runtime.Remoting
Imports System.Runtime.Remoting.Channels
Imports System.Runtime.Remoting.Channels.Http
Imports System.Configuration
Imports System.Security.Principal
Imports Fullstep.FSNDatabaseServer
Imports Fullstep.FSNLibrary

<Serializable()>
Public Class Root
    Private mUserCode As String
    'Clase de acceso a datos
    Private mDBServer As FSNDatabaseServer.Root
    Private ReadOnly Property DBServer() As FSNDatabaseServer.Root
        Get
            If mDBServer Is Nothing Then
                mDBServer = New FSNDatabaseServer.Root
            End If
            Return mDBServer
        End Get
    End Property
    'Autenticación
    Private mIsAuthenticated As Boolean = False
    Private Sub Authenticate()
        If Not mIsAuthenticated Then Throw New System.Security.SecurityException("User not authenticated!")
    End Sub
    Private _baseDatos As BaseDatos
    Public Property BDSecundaria() As BaseDatos
        Get
            Return _baseDatos
        End Get
        Set(ByVal value As BaseDatos)
            _baseDatos = value
        End Set
    End Property
    Private _BDLogin As BaseDatos
    Public Property BDLogin As BaseDatos
        Get
            Return _BDLogin
        End Get
        Set(value As BaseDatos)
            _BDLogin = value
        End Set
    End Property
#Region "Constructor"
    Public Sub New()
        Instanciar()
    End Sub
    Public Sub New(ByVal isWindowsService As Boolean)
        If isWindowsService Then
            mIsAuthenticated = True
            DBServer.LoginWindowsServiceTratamientoInstancias()
        Else
            Instanciar()
        End If
    End Sub
    Private Sub Instanciar()
        ' see if we need to configure remoting at all
        If Len(DB_SERVER) > 0 Then
            ' create and register our custom HTTP channel
            ' that uses the binary formatter
            Dim properties As New Hashtable
            properties("name") = "HttpBinary"

            Dim formatter As New BinaryClientFormatterSinkProvider
            Dim channel As New HttpChannel(properties, formatter, Nothing)
            Try
                ChannelServices.RegisterChannel(channel, False)
            Catch
                'Puede que ya esté redirigido
            End Try

            ' register the data portal types as being remote
            If Len(DB_SERVER) > 0 Then
                Try
                    RemotingConfiguration.RegisterWellKnownClientType(
                      GetType(FSNDatabaseServer.Root), DB_SERVER)
                Catch
                    'Puede que ya esté redirigido
                End Try
            End If
        End If
    End Sub
    Private Function DB_SERVER() As String
        Return ConfigurationManager.AppSettings("DBServer")
    End Function
#End Region
#Region "Generacion de instancias de la libreri FSNServer. UTILIZAR Get_Object"
    ''' <summary>
    ''' Función que instancia y devuelve un objeto de la clase especificada
    ''' </summary>
    ''' <param name="Tipo">Tipo de datos de la clase FSNServer a devolver</param>
    ''' <returns>Un objeto de la clase especificada</returns>
    Public Function Get_Object(ByVal Tipo As Type) As Object
        Authenticate()
        Dim pars() As Object
        If Tipo.Name = "User" Then
            pars = {DBServer, mUserCode, mIsAuthenticated}
        Else
            pars = {DBServer, mIsAuthenticated}
        End If
        Return Activator.CreateInstance(Tipo, pars)
    End Function
    '====================================================
    '====== NECESARIO ===================================
    '====================================================
    'Estos métodos son necesarios para el GeneralEntry. Como esta compartido para
    'Portal y Web, no vale el Get_Object ya que no podriamos diferenciar entre FSNServer."Clase" y la de portal.
    Public Function Get_Adjuntos() As Adjuntos
        Dim oAdjuns As Adjuntos
        Authenticate()
        oAdjuns = New Adjuntos(DBServer, mIsAuthenticated)
        Return oAdjuns
    End Function
    Public Function Get_Dictionary() As FSNServer.Dictionary
        Authenticate()
        Dim oDictionary As New FSNServer.Dictionary(Me.DBServer, mIsAuthenticated)
        Return oDictionary
    End Function
    Public Function Get_Provincias() As Provincias
        Dim oProvincias As Provincias
        Authenticate()
        oProvincias = New Provincias(DBServer, mIsAuthenticated)
        Return oProvincias
    End Function
    Public Function Get_Almacenes() As Almacenes
        Dim oAlmacenes As Almacenes
        Authenticate()
        oAlmacenes = New Almacenes(DBServer, mIsAuthenticated)
        Return oAlmacenes
    End Function
    Public Function Get_Centros() As Centros
        Dim oCentros As Centros
        Authenticate()
        oCentros = New Centros(DBServer, mIsAuthenticated)
        Return oCentros
    End Function
    Public Function Get_Departamentos() As Departamentos
        Dim oDepartamentos As Departamentos
        Authenticate()
        oDepartamentos = New Departamentos(DBServer, mIsAuthenticated)
        Return oDepartamentos
    End Function
    '====================================================
    '====== NECESARIO ===================================
    '====================================================
#End Region
#Region "Datos del Servidor de BD "
    Private mDBLogin As String
    Private mDBPassword As String
    Private mDBName As String
    Private mDBServidor As String
    Property DBLogin() As String
        Get
            Return mDBLogin
        End Get
        Set(ByVal Value As String)
            mDBLogin = Value
        End Set
    End Property
    Property DBPassword() As String
        Get
            Return mDBPassword
        End Get
        Set(ByVal Value As String)
            mDBPassword = Value
        End Set
    End Property
    Property DBServidor() As String
        Get
            Return mDBServidor
        End Get
        Set(ByVal Value As String)
            mDBServidor = Value
        End Set
    End Property
    Property DBName() As String
        Get
            Return mDBName
        End Get
        Set(ByVal Value As String)
            mDBName = Value
        End Set
    End Property
    ''' <summary>
    ''' Procedimiento en el que recuperamos las propiedades de Conexión a la BBDD
    ''' </summary>
    ''' <remarks>
    ''' Llamada desde: Report.aspx y Report_Consumos.aspx
    ''' Tiempo máximo: 0 seg</remarks>
    Public Sub get_PropiedadesConexion()
        mDBLogin = DBServer.DBLogin
        mDBPassword = DBServer.DBPassword
        mDBName = DBServer.DBName
        mDBServidor = DBServer.DBServer
    End Sub
#End Region
#Region "Datos de Instalacion: Tipo Acceso/longitudes de código"
    Private bAcceso As Boolean = False
    Private moTipoAcceso As TiposDeDatos.ParametrosGenerales
    Private moSentidoIntegracion As TiposDeDatos.ParametrosIntegracion
    Public ReadOnly Property TipoAcceso() As TiposDeDatos.ParametrosGenerales
        Get
            If Not (bAcceso) Then
                GetAccessType()
                bAcceso = True
            End If
            Return moTipoAcceso
        End Get
    End Property
    ''' <summary>
    ''' Carga los Parametros Integracion usados en mant perfiles
    ''' </summary>
    ''' <returns>Parametros Integracion</returns>
    ''' <remarks>Llamadad desde: DetallePerfiles.aspx.vb; Tiempo máximo:0,2</remarks>
    Public ReadOnly Property TipoAccesoI() As TiposDeDatos.ParametrosIntegracion
        Get
            If Not (bAcceso) Then
                GetAccessType()
                bAcceso = True
            End If
            Return moSentidoIntegracion
        End Get
    End Property
    ''' Revisado por: blp. Fecha: 31/10/2012
    ''' <summary>
    ''' Lee y devuelve los parámetros generales de la aplicación.
    ''' </summary>
    ''' <remarks>Llamadas desde: Login.master.btnLogin_Click</remarks>
    Private Sub GetAccessType()
        Dim data As DataSet
        Dim sAccesoQA As String
        Authenticate()
        data = DBServer.Parametros_GetData

        Select Case Encrypter.EncriptarAcceso("1", DBNullToSomething(data.Tables(0).Rows(0).Item("ACCESO_FSWS")), False)
            Case "FULLSTEP WS 1"
                moTipoAcceso.gsAccesoFSPM = TipoAccesoFSPM.AccesoFSPMSolicCompra
                moTipoAcceso.gbSolicitudesCompras = True
            Case "FULLSTEP WS 2"
                moTipoAcceso.gsAccesoFSPM = TipoAccesoFSPM.AccesoFSPMCompleto
                moTipoAcceso.gbSolicitudesCompras = True
            Case Else
                moTipoAcceso.gsAccesoFSPM = TipoAccesoFSPM.SinAcceso
                moTipoAcceso.gbSolicitudesCompras = False
        End Select
        If DBNullToStr(data.Tables(0).Rows(0).Item("ACCESO_FSGS")) = "" Then
            moTipoAcceso.gbAccesoFSGS = False
        Else
            moTipoAcceso.gbAccesoFSGS = True
        End If
        moTipoAcceso.giProce_Prove_Grupos = DBNullToInteger(data.Tables(0).Rows(0).Item("PROCE_PROVE_GRUPOS"))
        moTipoAcceso.giWinSecurityWeb = DBNullToDec(data.Tables(0).Rows(0).Item("Win_Sec_Web"))
        moTipoAcceso.giWinSecurity = DBNullToDec(data.Tables(0).Rows(0).Item("Win_Sec"))
        moTipoAcceso.gbAccesoQANoConformidad = False
        moTipoAcceso.gbAccesoQACertificados = False
        moTipoAcceso.gbBajaLOGPedidos = DBNullToBoolean(data.Tables(0).Rows(0).Item("ACTIVAR_BORRADO_LOGICO_PEDIDOS"))
        sAccesoQA = Encrypter.EncriptarAcceso("QA", DBNullToSomething(data.Tables(0).Rows(0).Item("ACCESO_FSQA")), False)
        If sAccesoQA = "FULLSTEP QM" Then
            moTipoAcceso.gsAccesoFSQA = TipoAccesoFSQA.AccesoFSQABasico
            moTipoAcceso.gbAccesoQANoConformidad = False
            moTipoAcceso.gbAccesoQACertificados = False
        Else
            If InStr(1, sAccesoQA, "//CERTIF//") Then
                moTipoAcceso.gbAccesoQACertificados = True
                moTipoAcceso.gsAccesoFSQA = TipoAccesoFSQA.Modulos
            End If
            If InStr(1, sAccesoQA, "//REC//") Then
                moTipoAcceso.gbAccesoQANoConformidad = True
                moTipoAcceso.gsAccesoFSQA = TipoAccesoFSQA.Modulos
                If DBNullToSomething(data.Tables(0).Rows(0).Item("FSQA_REVISOR")) = 1 Then
                    moTipoAcceso.gbFSQA_Revisor = True
                Else
                    moTipoAcceso.gbFSQA_Revisor = False
                End If
            End If
            If moTipoAcceso.gbAccesoQANoConformidad = False And moTipoAcceso.gbAccesoQACertificados = False Then
                moTipoAcceso.gsAccesoFSQA = TipoAccesoFSQA.SinAcceso
            End If
        End If
        If DBNullToSomething(data.Tables(0).Rows(0).Item("APROVISION")) = 1 Then
            moTipoAcceso.gbAccesoFSEP = True
        Else
            moTipoAcceso.gbAccesoFSEP = False
        End If
        moTipoAcceso.g_bAccesoFSFA = Encrypter.EncriptarAcceso("FA", modUtilidades.DBNullToStr(data.Tables(0).Rows(0).Item("ACCESO_FSFA")), False) = "FULLSTEP FA"
        moTipoAcceso.gbAccesoFSSM = Encrypter.EncriptarAcceso("SM", modUtilidades.DBNullToStr(data.Tables(0).Rows(0).Item("ACCESO_FSSM")), False) = "FULLSTEP SM"


        moTipoAcceso.gbAccesoFSCN = Encrypter.EncriptarAcceso("CN", modUtilidades.DBNullToStr(data.Tables(0).Rows(0).Item("ACCESO_FSCN")), False) = "FULLSTEP CN"

        Select Case Encrypter.EncriptarAcceso("BI", modUtilidades.DBNullToStr(data.Tables(0).Rows(0).Item("ACCESO_FSBI")), False)
            Case "FULLSTEP BQ"
                moTipoAcceso.gbAccesoFSBI = True
                moTipoAcceso.giAccesoFSBITipo = TiposDeDatos.TipoAccesoFSBI.Completo
            Case "FULLSTEP BI"
                moTipoAcceso.gbAccesoFSBI = True
                moTipoAcceso.giAccesoFSBITipo = TiposDeDatos.TipoAccesoFSBI.SoloCubosBI
            Case "FULLSTEP QL"
                moTipoAcceso.gbAccesoFSBI = True
                moTipoAcceso.giAccesoFSBITipo = TiposDeDatos.TipoAccesoFSBI.SoloQlikApps
            Case Else
                moTipoAcceso.gbAccesoFSBI = False
                moTipoAcceso.giAccesoFSBITipo = TiposDeDatos.TipoAccesoFSBI.SinAcceso
        End Select

        moTipoAcceso.gbAccesoFSAL = Encrypter.EncriptarAcceso("AL", modUtilidades.DBNullToStr(data.Tables(0).Rows(0).Item("ACCESO_FSAL")), False) = "FULLSTEP AL"
        moTipoAcceso.gbAccesoContratos = Encrypter.EncriptarAcceso("CO", modUtilidades.DBNullToStr(data.Tables(0).Rows(0).Item("ACCESO_CONTRATO")), False) = "FULLSTEP CONTRATOS"

        moTipoAcceso.gbEPRecepcionAutomatica = data.Tables(0).Rows(0).Item("IM_RECEPAUTO")
        moTipoAcceso.gbEPMarcarAcepProve = data.Tables(0).Rows(0).Item("EP_MARCAR_ACEP_PROVE")
        moTipoAcceso.gbMantEstIntEnTraspasoAdj = data.Tables(0).Rows(0).Item("INT_MANT_EST_TRASP_ADJ")
        moTipoAcceso.gbMostrarRecepSalidaAlmacen = data.Tables(0).Rows(0).Item("MOSTRAR_RECEP_SALIDA_ALMACEN")
        moTipoAcceso.gbEPNotifProveAdjuntarPedidoPDF = data.Tables(0).Rows(0).Item("EP_NOTIFPROVE_ADJUNTAR_PEDIDO_PDF")

        ' Parámetros relacionados con el tipo de código de pedido ERP
        moTipoAcceso.gbOblCodPedido = data.Tables(1).Rows(0).Item("OBLCODPEDIDO")
        moTipoAcceso.gbOblCodPedDir = data.Tables(1).Rows(0).Item("OBLCODPEDDIR")
        moTipoAcceso.gbOblCodPedidoObl = data.Tables(1).Rows(0).Item("OBLCODPEDIDO_OBL")
        moTipoAcceso.gbOblCodPedidoBloq = data.Tables(1).Rows(0).Item("BLOQCODPEDIDO")
        moTipoAcceso.gbOblPedidoFecEntrega = data.Tables(1).Rows(0).Item("OBLFECENTREGA")

        '% Desvío recepción defecto. Cada pedido puede tener el suyo y si no tiene indicado por defecto.
        moTipoAcceso.gdDesvioRecepcionDefecto = data.Tables(1).Rows(0).Item("DESVIO_RECEPCION")

        ''''''''''''''''''
        Dim textnomPedERP() As DataRow = data.Tables(2).Select("ID=31")
        Dim textnomPedDirERP() As DataRow = data.Tables(2).Select("ID=36")
        'Campos para proveedores
        Dim textCampo1() As DataRow = data.Tables(2).Select("ID=32")
        Dim textCampo2() As DataRow = data.Tables(2).Select("ID=33")
        Dim textCampo3() As DataRow = data.Tables(2).Select("ID=34")
        Dim textCampo4() As DataRow = data.Tables(2).Select("ID=35")

        With moTipoAcceso
            'Nombre empresa y codigos UON1,UON2,UON3
            .gbEmpresa = data.Tables(2).Select("ID=1").First.Item("DEN")
            .gbUON1Cod = data.Tables(2).Select("ID=3").First.Item("DEN")
            .gbUON2Cod = data.Tables(2).Select("ID=5").First.Item("DEN")
            .gbUON3Cod = data.Tables(2).Select("ID=7").First.Item("DEN")

            'Codigos de GMN1,GMN2,GMN3,GMN4
            .gbGMN1Cod = data.Tables(2).Select("ID=9").First.Item("DEN")
            .gbGMN2Cod = data.Tables(2).Select("ID=11").First.Item("DEN")
            .gbGMN3Cod = data.Tables(2).Select("ID=13").First.Item("DEN")
            .gbGMN4Cod = data.Tables(2).Select("ID=15").First.Item("DEN")
        End With

        moTipoAcceso.nomPedERP = New Dictionary(Of FSNLibrary.Idioma, String)
        moTipoAcceso.nomPedDirERP = New Dictionary(Of FSNLibrary.Idioma, String)
        'Campos para proveedores
        moTipoAcceso.nomCampo1 = New Dictionary(Of FSNLibrary.Idioma, String)
        moTipoAcceso.nomCampo2 = New Dictionary(Of FSNLibrary.Idioma, String)
        moTipoAcceso.nomCampo3 = New Dictionary(Of FSNLibrary.Idioma, String)
        moTipoAcceso.nomCampo4 = New Dictionary(Of FSNLibrary.Idioma, String)

        For Each fila As DataRow In textnomPedERP
            moTipoAcceso.nomPedERP.Add(CType(fila.Item("IDI").ToString(), FSNLibrary.Idioma), fila.Item("DEN").ToString())
        Next

        For Each fila As DataRow In textnomPedDirERP
            moTipoAcceso.nomPedDirERP.Add(CType(fila.Item("IDI").ToString(), FSNLibrary.Idioma), fila.Item("DEN").ToString())
        Next

        'Campos para proveedores
        For Each fila As DataRow In textCampo1
            moTipoAcceso.nomCampo1.Add(CType(fila.Item("IDI").ToString(), FSNLibrary.Idioma), fila.Item("DEN").ToString())
        Next

        For Each fila As DataRow In textCampo2
            moTipoAcceso.nomCampo2.Add(CType(fila.Item("IDI").ToString(), FSNLibrary.Idioma), fila.Item("DEN").ToString())
        Next

        For Each fila As DataRow In textCampo3
            moTipoAcceso.nomCampo3.Add(CType(fila.Item("IDI").ToString(), FSNLibrary.Idioma), fila.Item("DEN").ToString())
        Next

        For Each fila As DataRow In textCampo4
            moTipoAcceso.nomCampo4.Add(CType(fila.Item("IDI").ToString(), FSNLibrary.Idioma), fila.Item("DEN").ToString())
        Next
        '''''''''''''''''''
        ' Codigo ERP de la recepcion 
        moTipoAcceso.gbCodRecepERP = data.Tables(1).Rows(0).Item("CODRECEPERP")

        moTipoAcceso.gbUsar_OrgCompras = DBNullToSomething(data.Tables(0).Rows(0).Item("USAR_ORGCOMPRAS"))
        moTipoAcceso.gbArticulosGenericos = SQLBinaryToBoolean(DBNullToSomething(data.Tables(0).Rows(0).Item("ARTGEN"))) ' edu 436
        moTipoAcceso.gbVarCal_Materiales = SQLBinaryToBoolean(DBNullToSomething(data.Tables(0).Rows(0).Item("VARCAL_MAT")))
        moTipoAcceso.gbUsarART4_UON = SQLBinaryToBoolean(DBNullToSomething(data.Tables(0).Rows(0).Item("USAR_ART4_UON")))
        moTipoAcceso.gbOblAsignarPresArt = SQLBinaryToBoolean(DBNullToSomething(data.Tables(0).Rows(0).Item("OBLASIG_PRES_ART_PM")))

        moTipoAcceso.gbTrasladoAProvERP = SQLBinaryToBoolean(DBNullToSomething(data.Tables(0).Rows(0).Item("TRASLADO_APROV_ERP")))
        moTipoAcceso.gbActivLog = SQLBinaryToBoolean(DBNullToSomething(data.Tables(3).Rows(0).Item("ACTIVLOG")))
        moTipoAcceso.giPedEmail = data.Tables(3).Rows(0).Item("PED_EMAIL")
        moTipoAcceso.giPedEmailModo = data.Tables(3).Rows(0).Item("PED_EMAIL_MODO")
        moTipoAcceso.giPedPubPortal = data.Tables(3).Rows(0).Item("PED_PUB_PORTAL")
        moTipoAcceso.gbPedidosHitosFacturacion = data.Tables(3).Rows(0).Item("HITOSFAC")

        If moTipoAcceso.gbActivLog Then
            moTipoAcceso.giRecepAprov = TiposDeDatos.OrigenRegistroRecepcionesPedido.RegistroActividad
            moTipoAcceso.giRecepDirecto = TiposDeDatos.OrigenRegistroRecepcionesPedido.RegistroActividad
        Else
            moTipoAcceso.giRecepAprov = TiposDeDatos.OrigenRegistroRecepcionesPedido.Ninguno
            moTipoAcceso.giRecepDirecto = TiposDeDatos.OrigenRegistroRecepcionesPedido.Ninguno
        End If
        If data.Tables(0).Rows(0).Item("INTEGRACION") = 1 Then
            moTipoAcceso.g_bAccesoIS = True
            For Each filas As DataRow In data.Tables(4).Rows
                If filas.Item("ACTIVA") = 1 AndAlso (filas.Item("SENTIDO") = 1 Or filas.Item("SENTIDO") = 3) Then
                    moTipoAcceso.giRecepAprov = TiposDeDatos.OrigenRegistroRecepcionesPedido.IntegracionyRegistroActividad
                    Exit For
                End If
            Next
            For Each filas As DataRow In data.Tables(5).Rows
                If filas.Item("ACTIVA") = 1 AndAlso (filas.Item("SENTIDO") = 1 Or filas.Item("SENTIDO") = 3) Then
                    moTipoAcceso.giRecepDirecto = TiposDeDatos.OrigenRegistroRecepcionesPedido.IntegracionyRegistroActividad
                    Exit For
                End If
            Next
            For Each filas As DataRow In data.Tables(10).Rows
                If (filas("SMAX") = SentidoIntegracion.Entrada) AndAlso (filas("SMIN") = SentidoIntegracion.Entrada) Then
                    Select Case filas("TABLA")
                        Case TablasIntegracion.Mon
                            moSentidoIntegracion.gbSoloImportar_Mon = True
                        Case TablasIntegracion.Pag
                            moSentidoIntegracion.gbSoloImportar_Pag = True
                        Case TablasIntegracion.ViaPag
                            moSentidoIntegracion.gbSoloImportar_ViaPag = True
                        Case TablasIntegracion.Pai
                            moSentidoIntegracion.gbSoloImportar_Pais = True
                        Case TablasIntegracion.Provi
                            moSentidoIntegracion.gbSoloImportar_Provi = True
                        Case TablasIntegracion.Uni
                            moSentidoIntegracion.gbSoloImportar_Uni = True
                        Case TablasIntegracion.Dest
                            moSentidoIntegracion.gbSoloImportar_Dest = True
                        Case TablasIntegracion.art4
                            moSentidoIntegracion.gbSoloImportar_Art4 = True
                        Case TablasIntegracion.Prove
                            moSentidoIntegracion.gbSoloImportar_Prove = True
                        Case TablasIntegracion.PED_directo
                            moSentidoIntegracion.gbSoloImportar_PedDirecto = True
                    End Select
                ElseIf filas("TABLA") = TablasIntegracion.Adj Then
                    If (filas("SMAX") = SentidoIntegracion.Salida) OrElse (filas("SMAX") = SentidoIntegracion.EntradaYSalida) _
                    OrElse (filas("SMIN") = SentidoIntegracion.Salida) OrElse (filas("SMIN") = SentidoIntegracion.EntradaYSalida) Then
                        moSentidoIntegracion.gbExportar_Adj = True
                    End If
                End If
            Next
        Else
            moTipoAcceso.g_bAccesoIS = False
        End If

        moTipoAcceso.giEDAD_MIN_PWD = DBNullToDec(data.Tables(3).Rows(0).Item("EDAD_MIN_PWD"))
        moTipoAcceso.giEDAD_MAX_PWD = DBNullToDec(data.Tables(3).Rows(0).Item("EDAD_MAX_PWD"))
        moTipoAcceso.giHISTORICO_PWD = DBNullToDec(data.Tables(3).Rows(0).Item("HISTORICO_PWD"))
        moTipoAcceso.gbCOMPLEJIDAD_PWD = SQLBinaryToBoolean(data.Tables(3).Rows(0).Item("COMPLEJIDAD_PWD"))
        moTipoAcceso.giMIN_SIZE_PWD = DBNullToDec(data.Tables(3).Rows(0).Item("MIN_SIZE_PWD"))

        moTipoAcceso.gbCampo1 = SQLBinaryToBoolean(data.Tables(3).Rows(0).Item("ACTCAMPO1"))
        moTipoAcceso.gbCampo2 = SQLBinaryToBoolean(data.Tables(3).Rows(0).Item("ACTCAMPO2"))
        moTipoAcceso.gbCampo3 = SQLBinaryToBoolean(data.Tables(3).Rows(0).Item("ACTCAMPO3"))
        moTipoAcceso.gbCampo4 = SQLBinaryToBoolean(data.Tables(3).Rows(0).Item("ACTCAMPO4"))

        If data.Tables(6).Rows.Count > 0 Then
            moTipoAcceso.gWinSecAdminUser = DBNullToStr(data.Tables(6).Rows(0).Item("USU"))
            Dim WinSecAdminEncryptedPwd = DBNullToStr(data.Tables(6).Rows(0).Item("PWD"))
            Dim WinSecAdminPwdDate As Date = DBNullToSomething(data.Tables(6).Rows(0).Item("FECPWD"))
            moTipoAcceso.gWinSecAdminPwd = Encrypter.Encrypt(WinSecAdminEncryptedPwd, moTipoAcceso.gWinSecAdminUser, False, Encrypter.TipoDeUsuario.Persona, WinSecAdminPwdDate)
        End If

        Dim textos() As DataRow = data.Tables(2).Select("ID=1")
        moTipoAcceso.UON0_Denominacion = New Dictionary(Of FSNLibrary.Idioma, String)
        For Each fila As DataRow In textos
            moTipoAcceso.UON0_Denominacion.Add(CType(fila.Item("IDI").ToString(), FSNLibrary.Idioma), fila.Item("DEN").ToString())
        Next

        With moTipoAcceso
            .gbControlFechaRecepcion = (data.Tables(0).Rows(0).Item("RECDIR_FECHA_EP") = 1)
            .gbControlCantidadRecepcion = (data.Tables(0).Rows(0).Item("RECDIR_CANT_EP") = 1)
            .gbVerNumeroProveedor = (data.Tables(0).Rows(0).Item("VER_NUMEXT") = 1)
            .gbDirEnvFacObl = (data.Tables(0).Rows(0).Item("PEDDIRFAC") = 1)
            .gbEPDesactivarCargaArticulos = (data.Tables(0).Rows(0).Item("EP_DESACTIVAR_CARGA_ART") = 1)
            .gbEPActivarCargaSolPM = (data.Tables(0).Rows(0).Item("EP_ACTIVAR_CARGA_SOL_PM") = 1)

            'Moneda Central
            .gsMonedaCentral = DBNullToSomething(data.Tables(7).Rows(0).Item("MONCEN"))

            .gbAccesoLCX = (data.Tables(0).Rows(0).Item("ACCESO_LCX") = 1)

            .gbCostesPorLinea = (data.Tables(0).Rows(0).Item("IM_NO_COSTES_LINEA") = 0) 'si 1, no se permitirá la introducción de costes a nivel de línea
            .gbFechaRecepBloqueada = (data.Tables(0).Rows(0).Item("FSEP_BLOQ_FEC_RECEP") = 1)
            .gbPedidoLibre = (data.Tables(0).Rows(0).Item("PEDIDO_LIBRE") = 1)

            .gbSubasta = DBNullToBoolean(data.Tables(0).Rows(0).Item("SUBASTA"))
            .gbPedidosAprov = DBNullToBoolean(data.Tables(0).Rows(0).Item("APROVISION"))
            .gbPedidosDirectos = DBNullToBoolean(data.Tables(0).Rows(0).Item("PEDIDO"))
            .gbPedidosERP = (data.Tables(9).Rows(0).Item("PEDIDOSERP") > 0)
            .gEPAvisoEmitirPedido = DBNullToStr(data.Tables(0).Rows(0).Item("EP_AVISO_EMISION_PED"))

            .gbUsarPres1 = DBNullToBoolean(data.Tables(3).Rows(0).Item("USAPRES1"))
            .gbUsarPres2 = DBNullToBoolean(data.Tables(3).Rows(0).Item("USAPRES2"))
            .gbUsarPres3 = DBNullToBoolean(data.Tables(3).Rows(0).Item("USAPRES3"))
            .gbUsarPres4 = DBNullToBoolean(data.Tables(3).Rows(0).Item("USAPRES4"))

            .gbPres1Denominacion = New Dictionary(Of FSNLibrary.Idioma, String)
            .gbPres2Denominacion = New Dictionary(Of FSNLibrary.Idioma, String)
            .gbPres3Denominacion = New Dictionary(Of FSNLibrary.Idioma, String)
            .gbPres4Denominacion = New Dictionary(Of FSNLibrary.Idioma, String)

            For Each item As DataRow In data.Tables(2).Select("ID=20")
                .gbPres1Denominacion.Add(CType(item("IDI").ToString(), FSNLibrary.Idioma), item("DEN").ToString)
            Next
            For Each item As DataRow In data.Tables(2).Select("ID=21")
                .gbPres2Denominacion.Add(CType(item("IDI").ToString(), FSNLibrary.Idioma), item("DEN").ToString)
            Next
            For Each item As DataRow In data.Tables(2).Select("ID=27")
                .gbPres3Denominacion.Add(CType(item("IDI").ToString(), FSNLibrary.Idioma), item("DEN").ToString)
            Next
            For Each item As DataRow In data.Tables(2).Select("ID=28")
                .gbPres4Denominacion.Add(CType(item("IDI").ToString(), FSNLibrary.Idioma), item("DEN").ToString)
            Next

            .gbUsarPedPres1 = DBNullToBoolean(data.Tables(1).Rows(0).Item("PEDPRES1"))
            .gbUsarPedPres2 = DBNullToBoolean(data.Tables(1).Rows(0).Item("PEDPRES2"))
            .gbUsarPedPres3 = DBNullToBoolean(data.Tables(1).Rows(0).Item("PEDPRES3"))
            .gbUsarPedPres4 = DBNullToBoolean(data.Tables(1).Rows(0).Item("PEDPRES4"))

            .gbPres1Obligatorio = DBNullToBoolean(data.Tables(1).Rows(0).Item("OBLPRES1"))
            .gbPres2Obligatorio = DBNullToBoolean(data.Tables(1).Rows(0).Item("OBLPRES2"))
            .gbPres3Obligatorio = DBNullToBoolean(data.Tables(1).Rows(0).Item("OBLPRES3"))
            .gbPres4Obligatorio = DBNullToBoolean(data.Tables(1).Rows(0).Item("OBLPRES4"))

            .giPres1Nivel = DBNullToInteger(data.Tables(1).Rows(0).Item("NIVPRES1"))
            .giPres2Nivel = DBNullToInteger(data.Tables(1).Rows(0).Item("NIVPRES2"))
            .giPres3Nivel = DBNullToInteger(data.Tables(1).Rows(0).Item("NIVPRES3"))
            .giPres4Nivel = DBNullToInteger(data.Tables(1).Rows(0).Item("NIVPRES4"))

            .gbArticulosNegociadosPedExpress = data.Tables(0).Rows(0).Item("PED_EXPRESS_ART_NEGOCIADOS")
            .gbUsarPedidosAbiertos = data.Tables(1).Rows(0).Item("ACT_PED_ABIERTO")

            'Parámetros QA
            .gbProveERPVisibleNC = SQLBinaryToBoolean(data.Tables(0).Rows(0).Item("PROVE_ERP_NC"))

            .gbMostrarFechaContable = modUtilidades.DBNullToBoolean(data.Tables(0).Rows(0).Item("MOSTRAR_FECHA_CONTABLE"))

            .gbMaterialVerTodosNiveles = DBNullToBoolean(data.Tables(0).Rows(0).Item("MOSTRAR_TODOS_NIVELES_MATERIAL"))
            .gbCMVerTriangulo = DBNullToBoolean(data.Tables(0).Rows(0).Item("CM_VERTRIANGULO"))
        End With


        If Not data.Tables(11) Is Nothing Then
            moTipoAcceso.gbPotAValProvisionalSelProve = SQLBinaryToBoolean(data.Tables(11).Rows(0).Item("POT_A_VAL_PROVISIONAL_SEL_PROVE"))
        End If
    End Sub
    ''' <summary>
    ''' Propiedad que devuelve el parametro de Pargen_interno WIN_Sec_Web, no hace falta autenticate
    ''' </summary>
    ''' <remarks>
    ''' Llamada desde: FSNWeb/Login.master.vb/Page_load
    ''' Tiempo maximo: Automatico</remarks>
    Private _TipoDeAutenticacion As TiposDeAutenticacion
    Private bAutenticacion As Boolean = False
    Public ReadOnly Property TipoDeAutenticacion() As TiposDeAutenticacion
        Get
            If Not bAutenticacion Then
                'Dim FSNDBServer As New FSNDatabaseServer.Root
                _TipoDeAutenticacion = DBServer.TipoDeAcceso_Get
                bAutenticacion = True
            End If
            Return _TipoDeAutenticacion
        End Get
    End Property
    Private moLongitudesDeCodigos As TiposDeDatos.LongitudesDeCodigos
    Property LongitudesDeCodigos() As TiposDeDatos.LongitudesDeCodigos
        Get
            Return moLongitudesDeCodigos
        End Get
        Set(ByVal Value As TiposDeDatos.LongitudesDeCodigos)
            moLongitudesDeCodigos = Value
        End Set
    End Property
    ''' <summary>
    ''' Procedimiento que carga las longitudes de los campos utilizados en todo el producto
    ''' </summary>
    ''' <remarks>
    ''' Llamada desde: EPWeb/FSNPage/Login , FSNWeb/FSNPage/Login , FSNWeb/Inicio/Page_Load ,FSNWeb/Login.Master/btnLogin_click ,PMWeb2008/FSPMPage/Login ,  SMWeb/FSSMPAge/Login
    ''' Tiempo maximo: 0,2 seg</remarks>
    Public Sub Load_LongitudesDeCodigos()
        Dim data As DataSet
        Dim row As DataRow
        Dim oLongs As TiposDeDatos.LongitudesDeCodigos
        On Error Resume Next
        Authenticate()
        data = DBServer.LongitudesCampo_Get()

        oLongs.giLongCodART = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 10)("LONGITUD")
        oLongs.giLongCodCAL = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 20)("LONGITUD")
        oLongs.giLongCodCOM = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 30)("LONGITUD")
        oLongs.giLongCodDEP = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 50)("LONGITUD")
        oLongs.giLongCodDEST = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 60)("LONGITUD")
        oLongs.giLongCodEQP = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 70)("LONGITUD")
        oLongs.giLongCodGMN1 = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 80)("LONGITUD")
        oLongs.giLongCodGMN2 = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 90)("LONGITUD")
        oLongs.giLongCodGMN3 = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 100)("LONGITUD")
        oLongs.giLongCodGMN4 = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 110)("LONGITUD")
        oLongs.giLongCodMON = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 120)("LONGITUD")
        oLongs.giLongCodOFEEST = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 130)("LONGITUD")
        oLongs.giLongCodPAG = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 140)("LONGITUD")
        oLongs.giLongCodPAI = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 150)("LONGITUD")
        oLongs.giLongCodPER = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 160)("LONGITUD")
        oLongs.giLongCodPERF = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 170)("LONGITUD")
        oLongs.giLongCodPRESCON1 = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 180)("LONGITUD")
        oLongs.giLongCodPRESCON2 = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 190)("LONGITUD")
        oLongs.giLongCodPRESCON3 = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 200)("LONGITUD")
        oLongs.giLongCodPRESCON4 = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 210)("LONGITUD")
        oLongs.giLongCodPRESPROY1 = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 220)("LONGITUD")
        oLongs.giLongCodPRESPROY2 = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 230)("LONGITUD")
        oLongs.giLongCodPRESPROY3 = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 240)("LONGITUD")
        oLongs.giLongCodPRESPROY4 = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 250)("LONGITUD")
        oLongs.giLongCodPROVE = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 260)("LONGITUD")
        oLongs.giLongCodPROVI = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 270)("LONGITUD")
        oLongs.giLongCodROL = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 280)("LONGITUD")
        oLongs.giLongCodUNI = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 290)("LONGITUD")
        oLongs.giLongCodUON1 = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 300)("LONGITUD")
        oLongs.giLongCodUON2 = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 310)("LONGITUD")
        oLongs.giLongCodUON3 = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 320)("LONGITUD")
        oLongs.giLongCodUON4 = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 481)("LONGITUD")
        oLongs.giLongCodUSU = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 330)("LONGITUD")
        oLongs.giLongCodACT1 = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 340)("LONGITUD")
        oLongs.giLongCodACT2 = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 350)("LONGITUD")
        oLongs.giLongCodACT3 = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 360)("LONGITUD")
        oLongs.giLongCodACT4 = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 370)("LONGITUD")
        oLongs.giLongCodACT5 = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 380)("LONGITUD")
        oLongs.giLongCodPRESCONCEP31 = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 390)("LONGITUD")
        oLongs.giLongCodPRESCONCEP32 = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 400)("LONGITUD")
        oLongs.giLongCodPRESCONCEP33 = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 410)("LONGITUD")
        oLongs.giLongCodPRESCONCEP34 = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 420)("LONGITUD")
        oLongs.giLongCodPRESCONCEP41 = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 430)("LONGITUD")
        oLongs.giLongCodPRESCONCEP42 = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 440)("LONGITUD")
        oLongs.giLongCodPRESCONCEP43 = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 450)("LONGITUD")
        oLongs.giLongCodPRESCONCEP44 = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 460)("LONGITUD")
        oLongs.giLongCodCAT1 = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 461)("LONGITUD")
        oLongs.giLongCodCAT2 = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 462)("LONGITUD")
        oLongs.giLongCodCAT3 = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 463)("LONGITUD")
        oLongs.giLongCodCAT4 = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 464)("LONGITUD")
        oLongs.giLongCodCAT5 = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 465)("LONGITUD")
        oLongs.giLongCodGRUPOPROCE = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 467)("LONGITUD")
        oLongs.giLongCodDENART = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 470)("LONGITUD")
        oLongs.giLongCodACTIVO = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 485)("LONGITUD")
        oLongs.giLongCodCENTROCOSTE = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 486)("LONGITUD")

        oLongs.giLongPRES5_NIV0Cod = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 471)("LONGITUD")
        oLongs.giLongPRES5_NIV1Cod = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 472)("LONGITUD")
        oLongs.giLongPRES5_NIV2Cod = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 473)("LONGITUD")
        oLongs.giLongPRES5_NIV3Cod = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 474)("LONGITUD")
        oLongs.giLongPRES5_NIV4Cod = data.Tables(0).AsEnumerable.ToList.Find(Function(x) x("ID") = 475)("LONGITUD")

        moLongitudesDeCodigos = oLongs
    End Sub
    Private miNivelesUnQa As Integer
    ReadOnly Property NivelesUnQa() As String
        Get
            Return miNivelesUnQa
        End Get
    End Property
    'jbg 160309
    'Que hace: Devolver un recordset con maximo nivel de unidades qa            
    'Param. Entrada:-
    'Param. Salida: Lo dicho un recordset
    'Llamada desde: global.aspx login.aspx
    'Prototipo 1021. Guardar en un variable Application el maximo nivel de unidades qa de la instalación
    Public Sub Load_UnQa()
        Authenticate()

        Dim data As DataSet
        Dim row As DataRow
        On Error Resume Next

        data = DBServer.Nivel_UnQa_Get()
        miNivelesUnQa = data.Tables(0).Rows(0).Item(0)
    End Sub
    Public Function Load_AdjuntosDefecto(ByVal Solicitud As Long, ByVal CampoPadre As Long, Optional ByVal UserCode As String = "", Optional ByVal UserPassword As String = "") As DataSet
        Return DBServer.Campo_Load_AdjuntosDefecto(Solicitud, CampoPadre)
    End Function
    Private _TipoDeAccesoLCX As Boolean
    Private bAutenticacionLCX As Boolean = False
    Public ReadOnly Property TipoDeAccesoLCX() As Boolean
        Get
            If Not bAutenticacionLCX Then
                _TipoDeAccesoLCX = DBServer.TipoDeAccesoLCX_Get
                bAutenticacionLCX = True
            End If
            Return _TipoDeAccesoLCX
        End Get
    End Property
    Private _ComprobarSociedadLCX As Boolean
    Private bSociedadLCX As Boolean = False
    Public ReadOnly Property ComprobarEmpresaLCX(ByVal Empresa As String) As Boolean
        Get
            If Not bSociedadLCX Then
                _ComprobarSociedadLCX = DBServer.ComprobarEMpresa(Empresa)
                bSociedadLCX = True
            End If
            Return _ComprobarSociedadLCX
        End Get
    End Property
    Private _ComprobarCentroCosteLCX As Boolean
    Private bCentroLCX As Boolean = False
    Public ReadOnly Property ComprobarCentroCosteLCX(ByVal CentroCoste As String) As Boolean
        Get
            If Not bCentroLCX Then
                _ComprobarCentroCosteLCX = DBServer.ComprobarCentroCosteLCX(CentroCoste)
                bCentroLCX = True
            End If
            Return _ComprobarCentroCosteLCX
        End Get
    End Property
#End Region
#Region "Login"
    Public Function Login(ByVal usercode As String, Optional ByVal OmitirPassword As Boolean = False) As User
        Return Login(usercode, "", False, False, OmitirPassword)
    End Function
    Public Function Login(ByVal usercode As String, ByVal userpassword As String) As User
        Return Login(usercode, userpassword, False, False)
    End Function
    Public Function Login(ByVal usercode As String, ByVal userpassword As String, ByVal isEncrypted As Boolean) As User
        Return Login(usercode, userpassword, isEncrypted, False)
    End Function
    ''' <summary>
    ''' Loguea el usuario introducido para el acceso a la aplicaciÃ³n
    ''' </summary>
    ''' <param name="usercode">Cod Usuario</param>
    ''' <param name="userpassword">pwd Usuario encriptada</param>      
    ''' <param name="isEncrypted">Variable booleana que indica si la clave esta encriptada</param>  
    ''' <param name="isHash">Variable booleana que indica si es una clave Hash</param>
    ''' <returns>Clase usuario</returns>
    ''' <remarks>Llamada desde=FSNWEb/Inicio/ObtenerAcceso 
    '''  Tiempo maximo=0,3seg</remarks>
    Public Function Login(ByVal usercode As String, ByVal userpassword As String, ByVal isEncrypted As Boolean, ByVal isHash As Boolean,
                Optional ByVal OmitirPassword As Boolean = False, Optional ByVal BDPassword As Boolean = False) As User
        'Dim FSNServer As New FSNServer.Root
        Dim oUser As FSNServer.User
        Dim currentdomain As AppDomain = Thread.GetDomain
        Dim OldPrincipal As IPrincipal = Thread.CurrentPrincipal
        'logeo principal
        'Si la base de datos de login no es nula , puede no ser la que vien en el license, actualizamos la cadena de conexion actual
        If Not BDLogin Is Nothing Then
            Me.actualizarCadenaConexion(BDLogin)
        End If
        oUser = logeo(usercode, userpassword, isEncrypted, isHash, OmitirPassword, BDPassword, currentdomain)
        'fin del logeo principal
        'Tarea 3415: ahora si tenemos una base de datos secundaria seleccionada 
        'volvemos a logearnos con la cadena de conexion correspondiente
        If Not oUser Is Nothing AndAlso Not Me.BDSecundaria Is Nothing AndAlso UCase(Me.BDSecundaria.nombre) <> UCase(DBServer.DBName) Then
            Me.actualizarCadenaConexion(BDSecundaria)
            oUser = logeo(usercode, userpassword, isEncrypted, isHash, True, BDPassword, currentdomain)
        End If

        Thread.CurrentPrincipal = oUser
        Try
            If Not TypeOf OldPrincipal Is User Then
                currentdomain.SetThreadPrincipal(oUser)
            End If
        Catch
        End Try
        Return oUser
    End Function
    Private Function logeo(ByVal usercode As String, ByVal userpassword As String, ByVal isEncrypted As Boolean, ByVal isHash As Boolean,
                Optional ByVal OmitirPassword As Boolean = False, Optional ByVal BDPassword As Boolean = False, Optional currentdomain As AppDomain = Nothing) As User
        Select Case TipoDeAutenticacion
            Case TiposDeAutenticacion.Windows, TiposDeAutenticacion.LDAP, TiposDeAutenticacion.ADFS
                DBServer.ObtenerPool(usercode)
                If TipoDeAutenticacion = TiposDeAutenticacion.Windows Then
                    currentdomain.SetPrincipalPolicy(PrincipalPolicy.WindowsPrincipal)
                Else
                    currentdomain.SetPrincipalPolicy(PrincipalPolicy.UnauthenticatedPrincipal)
                End If
                mIsAuthenticated = True
                mUserCode = usercode

                Return New User(DBServer, mUserCode, mIsAuthenticated)
            Case Else
                'Encriptamos la password para que no viaje desencriptada si esta el remotting activado
                If Not isEncrypted And OmitirPassword = False Then
                    userpassword = Encrypter.Encrypt(userpassword)
                End If
                Dim oResLogin As TipoResultadoLogin = DBServer.Login(usercode, userpassword, , OmitirPassword, BDPassword)
                If oResLogin <> TipoResultadoLogin.LoginOk Then
                    Return New User(oResLogin)
                Else
                    currentdomain.SetPrincipalPolicy(PrincipalPolicy.UnauthenticatedPrincipal)
                    mIsAuthenticated = True
                    mUserCode = usercode

                    Return New User(DBServer, mUserCode, mIsAuthenticated)
                End If
        End Select
    End Function
    Public Function LoginPortal(ByVal usercode As String, ByVal userpassword As String) As User
        'Es necesario hacer el login para que se establezca la prop. mIsAuthenticated del DBServer. Si no es así no se puede crear después la instancia de User
        If DBServer.Login(usercode, userpassword, True) <> TipoResultadoLogin.LoginOk Then
            Return Nothing
        Else
            mIsAuthenticated = True
            mUserCode = usercode
            Return New User(DBServer, usercode, mIsAuthenticated)
        End If
    End Function
    ''' <summary>
    ''' Login tipo Caixa asigna permisos de imputacion al centro de coste pasado por parametro
    ''' </summary>
    ''' <param name="usercode">Cod Usuario</param>
    ''' <param name="codEmpresa">cod de la empresa</param>
    ''' <param name="codCentro">Cod del centro de coste</param>    
    ''' <returns>Clase usuario</returns>
    ''' <remarks>Llamada desde=GestionLogin; Tiempo maximo=XX</remarks>
    Public Function LoginLCX(ByVal usercode As String, ByVal codEmpresa As String, ByVal codCentro As String, ByVal perfil As String, ByVal oUser As User) As String
        Authenticate()

        Dim codigoCentro As String
        codigoCentro = DBServer.LoginLCX(usercode, codEmpresa, codCentro, perfil)
        If codEmpresa <> "" Then
            oUser.EmpresaLCX = DBServer.ObtenerIdEmp(codEmpresa)
            oUser.CentroLCX = codigoCentro
        End If
        Return codigoCentro
    End Function
    ''' <summary>Gestión el bloqueo de la cuenta por intentos de login fallidos</summary>
    ''' <param name="UserCode">Código de usuario</param>
    ''' <returns>Objeto con los datos de bloqueo</returns>
    Public Function LoginGestionBloqueoCuenta(ByVal UserCode As String) As Object
        Dim dtDatos As DataTable = DBServer.LoginGestionBloqueoCuenta(UserCode)
        Return New With {.LogPreBloq = dtDatos.Rows(0)("LOGPREBLOQ"), .Bloq = dtDatos.Rows(0)("BLOQ")}
    End Function
#End Region
#Region "Gestión de Password"
    ''' <summary>
    ''' Función que cambia la Password del usuario, comprobando previamente que cumple con las restricciones de seguridad establecidas, y devolviendo un código de error en caso de que no se cumpla alguna e ellas o 0 si todo ha ido bien
    ''' </summary>
    ''' <param name="ADM">0 si el usuario no es administrador, y 1 si lo es</param>
    ''' <param name="Usuario">Codigo de usuario</param>
    ''' <param name="PWD_NUEVA">Nueva Pwd del usuario</param>
    ''' <param name="fecha_Pwd">Fecha de la contraseña</param>
    ''' <param name="ichange">Variable booleana que indica si se debe cambiar la contraseña en el siguiente inicio de sesión</param>
    ''' <param name="Usu_nuevo">Código de usuario nuevo</param>
    ''' <param name="HISTORICO_PWD">Número de contraseñas a comprobar en caso de que este activada la opción Complejidad de contraseña</param>
    ''' <returns>Un valor entero con el código de respuesta</returns>
    ''' <remarks>
    ''' Llamada desde: WebServiceCambioPWD/CambioPWD/CambiarLogin
    ''' Tiempo máximo: 0,4 seg</remarks>
    Public Function CambiarPwd(ByVal ADM As Integer, ByVal Usuario As String, ByVal PWD_NUEVA As String, ByVal fecha_Pwd As Date, ByVal ichange As Boolean, ByVal Usu_nuevo As String, ByVal HISTORICO_PWD As Integer)
        Authenticate()
        Return DBServer.CambiarPwdDB(ADM, Usuario, PWD_NUEVA, fecha_Pwd, ichange, Usu_nuevo, HISTORICO_PWD)
    End Function
    ''' <summary>
    ''' Funcion que comprueba si la contraseña pasada se encuentra en el histórico de contraseñas
    ''' </summary>
    ''' <param name="usu">Codigo de usuario</param>
    ''' <param name="PWD">Contraseña del usuario</param>
    ''' <returns>Una variable booleana que será true si la contraseña está repetida y 0 si no lo esta</returns>
    ''' <remarks>
    ''' Llamada desde: WebServiceCambioPWD/CambioPWD/ComprobarHistorialContraseña
    ''' Tiempo máximo: 0,2 seg</remarks>
    Public Function ComprobarPWDRepetido(ByVal USU As String, ByVal PWD As String) As Boolean
        Authenticate()
        Return DBServer.ComprobarPWDRepetido(USU, PWD)
    End Function
    Public Function CumpleVigenciaPWD(ByVal Usu As String) As Boolean
        Authenticate()
        Return DBServer.ComprobarVigenciaPWD(Usu)
    End Function
#End Region
#Region "Impersonate"
    Private mImpersonateLogin As String = ""
    Property ImpersonateLogin() As String
        Get
            Return mImpersonateLogin
        End Get
        Set(ByVal Value As String)
            mImpersonateLogin = Value
        End Set
    End Property
    Private mImpersonatePassword As String = ""
    Property ImpersonatePassword() As String
        Get
            Return mImpersonatePassword
        End Get
        Set(ByVal Value As String)
            mImpersonatePassword = Value
        End Set
    End Property
    Private mImpersonateDominio As String = ""
    Property ImpersonateDominio() As String
        Get
            Return mImpersonateDominio
        End Get
        Set(ByVal Value As String)
            mImpersonateDominio = Value
        End Set
    End Property
    Public Sub Impersonate()
        Authenticate()
        mImpersonateLogin = DBServer.ImpersonateLogin
        mImpersonatePassword = DBServer.ImpersonatePassword
        mImpersonateDominio = DBServer.ImpersonateDominio
    End Sub
#End Region
    Public Function obtenerBasesDatosSecundarias() As List(Of BaseDatos)
        Dim BDSecundarias As New List(Of BaseDatos)
        Dim dt As DataTable
        dt = DBServer.obtenerBasesDatosSecundarias()
        For Each dr As DataRow In dt.Rows
            BDSecundarias.Add(New BaseDatos(BDSecundarias.Count, dr("ID"), dr("DENOMINACION"), dr("SERVIDOR"), dr("NOMBRE"), dr("USUARIO"), dr("PASSWORD"), null2str(dr("IMP_USUARIO")), null2str(dr("IMP_PASSWORD")), null2str(dr("IMP_DOMINIO")), dr("PRINCIPAL"), dr("LOGIN"), dr("ACTIVA")))
        Next
        Return BDSecundarias
    End Function
    Public Function obtenerBaseDatosSecundaria(id As Integer) As BaseDatos
        Dim dt As DataTable
        Dim dr As DataRow
        dt = DBServer.obtenerBasesDatosSecundarias(id)
        If dt.Rows.Count = 1 Then
            dr = dt.Rows(0)
            Return New BaseDatos(1, dr("ID"), dr("DENOMINACION"), dr("SERVIDOR"), dr("NOMBRE"), dr("USUARIO"), dr("PASSWORD"), null2str(dr("IMP_USUARIO")), null2str(dr("IMP_PASSWORD")), null2str(dr("IMP_DOMINIO")), dr("PRINCIPAL"), dr("LOGIN"), dr("ACTIVA"))
        Else
            Return Nothing
        End If
    End Function
    Public Sub actualizarCadenaConexion(db As BaseDatos)
        DBServer.DBServer = db.Servidor
        DBServer.DBName = db.nombre
        DBServer.DBLogin = db.Usuario
        DBServer.DBPassword = db.Password
        DBServer.ImpersonateLogin = db.Impersonate_user
        DBServer.ImpersonateDominio = db.Impersonate_dominio
        DBServer.ImpersonatePassword = db.Impersonate_password
        DBServer.ActualizarCadenaConexion()
    End Sub

    ''' <summary>Genera un nuevo registro en la tabla LOG con los datos indicados</summary>
    ''' <param name="sCodUsu">Cod. usuario</param>
    ''' <param name="Acc">Id accion</param>
    ''' <param name="sDat">Datos</param>
    Public Sub RegistrarAccion(ByVal sCodUsu As String, ByVal Acc As Integer, ByVal sDat As String)
        Authenticate()
        DBServer.RegistrarAccion(sCodUsu, Acc, sDat)
    End Sub
End Class