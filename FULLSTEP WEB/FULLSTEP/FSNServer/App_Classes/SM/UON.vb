﻿Imports Microsoft.VisualBasic
Imports System.Data

<Serializable()> _
Public Class UON
    Inherits Security
    Private sUON1 As String
    Private sUON2 As String
    Private sUON3 As String
    Private sUON4 As String
    Private sDenominacion As String
    Private _ChildUONs As UONs
    Private _ChildUONs_EP As List(Of UON)
    Private _CentroSM As Centro_SM

    Public Property UON1() As String
        Get
            Return sUON1
        End Get
        Set(ByVal value As String)
            sUON1 = value
        End Set
    End Property

    Public Property UON2() As String
        Get
            Return sUON2
        End Get
        Set(ByVal value As String)
            sUON2 = value
        End Set
    End Property

    Public Property UON3() As String
        Get
            Return sUON3
        End Get
        Set(ByVal value As String)
            sUON3 = value
        End Set
    End Property

    Public Property UON4() As String
        Get
            Return sUON4
        End Get
        Set(ByVal value As String)
            sUON4 = value
        End Set
    End Property

    Public Property Denominacion() As String
        Get
            Return sDenominacion
        End Get
        Set(ByVal value As String)
            sDenominacion = value
        End Set
    End Property

    Public Property CentroSM() As Centro_SM
        Get
            Return _CentroSM
        End Get
        Set(ByVal value As Centro_SM)
            _CentroSM = value
        End Set
    End Property

    Public Property ChildUONs() As UONs
        Get
            Return _ChildUONs
        End Get
        Set(ByVal value As UONs)
            _ChildUONs = value
        End Set
    End Property

    Public Property ChildUONs_EP() As List(Of UON)
        Get
            Return _ChildUONs_EP
        End Get
        Set(ByVal value As List(Of UON))
            _ChildUONs_EP = value
        End Set
    End Property

    Public Overrides Function ToString() As String
        Dim uons As New Text.StringBuilder(sUON1)
        If Not String.IsNullOrEmpty(sUON2) Then
            uons.Append("#" & sUON2)
            If Not String.IsNullOrEmpty(sUON3) Then
                uons.Append("#" & sUON3)
                If Not String.IsNullOrEmpty(sUON4) Then
                    uons.Append("#" & sUON4)
                End If
            End If
        End If
        Return uons.ToString()
    End Function

#Region " Constructor"
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
    Public Sub New()

    End Sub
#End Region

End Class
