﻿<Serializable()> _
Public Class ImputacionesPedido
    Inherits List(Of ImputacionPedido)

    Public Property ItemId(ByVal Id As Integer) As ImputacionPedido
        Get
            Return Me.Find(Function(el As ImputacionPedido) el.Id = Id)
        End Get
        Set(ByVal value As ImputacionPedido)
            Dim ind As Integer = Me.FindIndex(Function(el As ImputacionPedido) el.Id = Id)
            If ind = -1 Then
                Me.Add(value)
            Else
                Me.Item(ind) = value
            End If
        End Set
    End Property


#Region " Constructor"

    ''' <summary>
    ''' Constructor de la clase Imputaciones
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si está autenticado</param>
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New()
    End Sub


#End Region

End Class
