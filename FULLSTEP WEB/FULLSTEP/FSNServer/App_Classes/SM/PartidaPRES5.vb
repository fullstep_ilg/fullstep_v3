﻿Imports Microsoft.VisualBasic
Imports System.Data

<Serializable()> _
Public Class PartidaPRES5
    Inherits Security

    Private sNiv0 As String
    Private sNiv1 As String
    Private sNiv2 As String
    Private sNiv3 As String
    Private sNiv4 As String
    Private sNiv0_Den As String
    Private sNiv1_Den As String
    Private sNiv2_Den As String
    Private sNiv3_Den As String
    Private sNiv4_Den As String
    Private sDenominacion As String
    Private _ChildPRES5 As List(Of PartidaPRES5)
    Private _ChildPRES5_EP As List(Of PartidaPRES5)
    Private dFechaIniPresup As Nullable(Of DateTime)
    Private dFechaFinPresup As Nullable(Of DateTime)
    Private iPresupuestoId As Integer
    Private sGestorCod As String
    Private sGestorDen As String
    Private sCodCentro As String
    Private sDenCentro As String

    Private moDataAnyoPartida As DataSet

    Public Property NIV0() As String
        Get
            Return sNiv0
        End Get
        Set(ByVal value As String)
            sNiv0 = value
        End Set
    End Property
    Public Property NIV0_Den() As String
        Get
            Return sNiv0_Den
        End Get
        Set(ByVal value As String)
            sNiv0_Den = value
        End Set
    End Property

    Public Property NIV1() As String
        Get
            Return sNiv1
        End Get
        Set(ByVal value As String)
            sNiv1 = value
        End Set
    End Property
    Public Property NIV1_Den() As String
        Get
            Return sNiv1_Den
        End Get
        Set(ByVal value As String)
            sNiv1_Den = value
        End Set
    End Property

    Public Property NIV2() As String
        Get
            Return sNiv2
        End Get
        Set(ByVal value As String)
            sNiv2 = value
        End Set
    End Property
    Public Property NIV2_Den() As String
        Get
            Return sNiv2_Den
        End Get
        Set(ByVal value As String)
            sNiv2_Den = value
        End Set
    End Property
    Public Property NIV3() As String
        Get
            Return sNiv3
        End Get
        Set(ByVal value As String)
            sNiv3 = value
        End Set
    End Property
    Public Property NIV3_Den() As String
        Get
            Return sNiv3_Den
        End Get
        Set(ByVal value As String)
            sNiv3_Den = value
        End Set
    End Property
    Public Property NIV4() As String
        Get
            Return sNiv4
        End Get
        Set(ByVal value As String)
            sNiv4 = value
        End Set
    End Property
	Public Property NIV4_Den() As String
		Get
			Return sNiv4_Den
		End Get
		Set(ByVal value As String)
			sNiv4_Den = value
		End Set
	End Property
	Private _anyoPartida As Integer
	Public Property AnyoPartida() As Integer
		Get
			Return _anyoPartida
		End Get
		Set(ByVal value As Integer)
			_anyoPartida = value
		End Set
	End Property
	Public Property Denominacion() As String
        Get
            Return sDenominacion
        End Get
        Set(ByVal value As String)
            sDenominacion = value
        End Set
    End Property

    Public Property ChildPRES5() As List(Of PartidaPRES5)
        Get
            Return _ChildPRES5
        End Get
        Set(ByVal value As List(Of PartidaPRES5))
            _ChildPRES5 = value
        End Set
    End Property

    Public Property ChildPRES5_EP() As List(Of PartidaPRES5)
        Get
            Return _ChildPRES5_EP
        End Get
        Set(ByVal value As List(Of PartidaPRES5))
            _ChildPRES5_EP = value
        End Set
    End Property

    Public Property FechaInicioPresupuesto() As Nullable(Of DateTime)
        Get
            Return dFechaIniPresup
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            dFechaIniPresup = value
        End Set
    End Property

    Public Property FechaFinPresupuesto() As Nullable(Of DateTime)
        Get
            Return dFechaFinPresup
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            dFechaFinPresup = value
        End Set
    End Property

    Public Property PresupuestoId() As Integer
        Get
            Return iPresupuestoId
        End Get
        Set(ByVal value As Integer)
            iPresupuestoId = value
        End Set
    End Property

    Public ReadOnly Property TienePresupuestoVigente() As Boolean
        Get
            Return ((dFechaIniPresup < Now() Or Not dFechaIniPresup.HasValue) And (dFechaFinPresup > Now() Or Not dFechaFinPresup.HasValue))
        End Get
    End Property

    ''' <summary>
    ''' 100.000 implica todos los años cumplen. 
    ''' Eoc implica el año q no cumple
    ''' </summary>
    ''' <param name="LineaId"></param>
    ''' <returns></returns>
    Public ReadOnly Property TienePresupuestoVigentePluriAnual(ByVal LineaId As Long) As Integer
        Get
            Return mDBServer.TienePresupuestoVigente(LineaId, iPresupuestoId)
        End Get
    End Property


    ''' <summary>
    ''' Código del gestor (vinculado al campo COD de la tabla PER)
    ''' </summary>
    Public Property GestorCod As String
        Get
            Return sGestorCod
        End Get
        Set(ByVal value As String)
            sGestorCod = value
        End Set
    End Property

    ''' <summary>
    ''' Descripción del gestor: nombre y apellidos
    ''' </summary>
    Public Property GestorDen As String
        Get
            Return sGestorDen
        End Get
        Set(ByVal value As String)
            sGestorDen = value
        End Set
    End Property

    Public Property CodCentro() As String
        Get
            Return sCodCentro
        End Get
        Set(ByVal value As String)
            sCodCentro = value
        End Set
    End Property
    Public Property DenCentro() As String
        Get
            Return sDenCentro
        End Get
        Set(ByVal value As String)
            sDenCentro = value
        End Set
    End Property

    Public Overrides Function ToString() As String
        Dim cods As New Text.StringBuilder(NIV0)
        If Not String.IsNullOrEmpty(NIV1) Then
            cods.Append("#" & NIV1)
            If Not String.IsNullOrEmpty(NIV2) Then
                cods.Append("#" & NIV2)
                If Not String.IsNullOrEmpty(NIV3) Then
                    cods.Append("#" & NIV3)
                    If Not String.IsNullOrEmpty(NIV4) Then cods.Append("#" & NIV4)
                End If
            End If
        End If
        Return cods.ToString()
    End Function

#Region " Constructor"
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
        ChildPRES5_EP = New List(Of PartidaPRES5)
    End Sub
    Public Sub New()
        ChildPRES5_EP = New List(Of PartidaPRES5)
    End Sub
#End Region

#Region "Año partida"
    Public ReadOnly Property DataAnyoPartida() As Data.DataSet
        Get
            Return moDataAnyoPartida
        End Get
    End Property

    Public Sub LoadDataAnyoPartida(ByVal sPartida0 As String, ByVal sPartida As String)
        Authenticate()

        moDataAnyoPartida = DBServer.AnyoPartida_Load(sPartida0, sPartida)
    End Sub
#End Region
End Class
