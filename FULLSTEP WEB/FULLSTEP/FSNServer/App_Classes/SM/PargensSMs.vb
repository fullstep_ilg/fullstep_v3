﻿<Serializable()> _
Public Class PargensSMs
    Inherits Lista(Of PargenSM)

    ''' <param name="isAuthenticated">Si está autenticado</param>
    ''' <remarks></remarks>
    Public Sub New(ByRef dbserver As Fullstep.FSNDataBaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
    Public Overloads ReadOnly Property Item(ByVal Id As Integer) As PargenSM
        Get
            Return Me.Find(Function(elemento As PargenSM) elemento.Id = Id)
        End Get
    End Property
	Public Overloads ReadOnly Property Item(ByVal pres5 As String) As PargenSM
		Get
			Return Me.Find(Function(elemento As PargenSM) elemento.Pres5 = pres5)
		End Get
	End Property
	Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub
    ''' <summary>
    ''' Procedimiento que añade un nuevo objeto de tipo cTipoPPRES5IMPORTE_UONedido a la colección
    ''' </summary>
    ''' <remarks>
    ''' Llamada desde:Esta misma clase
    ''' Tiempo máximo:0 seg</remarks>
    Public Overloads Function Add(ByVal id As Integer, ByVal Pres5 As String, ByVal PresModo As EnumModoPresupuestacion, ByVal ImpModo As EnumModoImputacion, ByVal ImpSC As EnumTipoImputacion, ByVal ImpPedido As EnumTipoImputacion, ByVal ImpRecep As EnumTipoImputacion, ByVal ImpNivel As Integer, ByVal Vigencia As TipoAvisoBloqueo, ByVal DispSc As TipoAvisoBloqueo, ByVal DispPedido As TipoAvisoBloqueo, ByVal DispRecep As TipoAvisoBloqueo, ByVal DenNivel As String, ByVal ComprAcum As EnumTipoAcumulacion) As PargenSM
        Dim ObjNewMember As New PargenSM(DBServer, mIsAuthenticated)
        If Not IsNothing(id) AndAlso Not id = 0 AndAlso Not IsDBNull(id) Then
            ObjNewMember.Id = id
        Else
            Dim MaxId As Integer = 0
            For i As Integer = 0 To MyBase.Count - 1
                If MyBase.Item(i).Id > MaxId Then
                    MaxId = MyBase.Item(i).Id
                End If
            Next
            ObjNewMember.Id = MaxId + 1
        End If
        If Not Pres5 Is Nothing Then
            ObjNewMember.Pres5 = Pres5
        Else
            ObjNewMember.Pres5 = Nothing
        End If
        ObjNewMember.ModoImputacion = ImpModo
        ObjNewMember.ImputacionSolicitudCompra = ImpSC
        ObjNewMember.ImputacionPedido = ImpPedido
        ObjNewMember.ImputacionRecepcion = ImpRecep
        ObjNewMember.ImputacionNivel = ImpNivel
        ObjNewMember.DenNivel = DenNivel
        ObjNewMember.Vigencia = Vigencia
        ObjNewMember.ControlDispSC = DispSc
        ObjNewMember.AcumularComprometido = ComprAcum
        MyBase.Add(ObjNewMember)
        Return ObjNewMember
        ObjNewMember = Nothing
    End Function
    ''' <summary>
    ''' Función que devuelve un objeto de tipo PargenSM con las configuraciones del Arbol Presupuestario para el usuario dado
    ''' </summary>
    ''' <param name="Usu">Codigo de usuario</param>
    ''' <returns>Un objeto de la clase PargenSM con las configuraciones del Control Presupuestario</returns>
    ''' <remarks>
    ''' Llamada desde: EPWEB\EmisionPedido.aspx.vb\GridLineas_RowDataBound, DlCesta_ItemDataBound</remarks>
    Public Function CargarConfiguracionSM(ByVal Usu As String, ByVal Idioma As String, Optional ByVal CargarEnColeccion As Boolean = False) As DataSet
        Return CargarConfiguracionSM(Usu, Idioma, "", CargarEnColeccion)
    End Function
    ''' <summary>
    ''' Función que devuelve un objeto de tipo PargenSM con las configuraciones del Arbol Presupuestario para el usuario dado
    ''' </summary>
    ''' <param name="Usu">Codigo de usuario</param>
    ''' <returns>Un objeto de la clase PargenSM con las configuraciones del Control Presupuestario</returns>
    ''' <remarks>
    ''' Llamada desde: EPWEB\EmisionPedido.aspx.vb\GridLineas_RowDataBound, DlCesta_ItemDataBound</remarks>
    Public Function CargarConfiguracionSM(ByVal Usu As String, ByVal Idioma As String, ByVal Pres5_niv0 As String, Optional ByVal CargarEnColeccion As Boolean = False) As DataSet
        Dim oPargenSM As PargenSM
        Dim DsResul As New DataSet
        DsResul = DBServer.PargensSMs_CargarConfiguracionSM(Usu, Idioma, Pres5_niv0)
        If DsResul.Tables(0).Rows.Count > 0 Then
            If CargarEnColeccion Then
                For i As Integer = 0 To DsResul.Tables(0).Rows.Count - 1
                    oPargenSM = New PargenSM(DBServer, mIsAuthenticated)
                    With DsResul.Tables(0).Rows(i)
                        oPargenSM.Pres5 = DBNullToStr(.Item("PRES5"))
						oPargenSM.ModoImputacion = DBNullToDec(.Item("IMPMODO"))
						oPargenSM.ImputacionSolicitudCompra = DBNullToDec(.Item("IMP_SC"))
                        oPargenSM.ImputacionPedido = DBNullToDec(.Item("IMP_PEDIDO"))
                        oPargenSM.ImputacionRecepcion = DBNullToDec(.Item("IMP_RECEP"))
                        oPargenSM.ImputacionNivel = DBNullToDec(.Item("IMP_NIVEL"))
                        oPargenSM.Vigencia = DBNullToDec(.Item("VIGENCIA"))
                        oPargenSM.ControlDispSC = DBNullToDec(.Item("DISP_SC"))
                        oPargenSM.AcumularComprometido = DBNullToDec(.Item("COMPR_ACUM"))
                        oPargenSM.DenNivel = DBNullToStr(.Item("DEN_Nivel"))
                        oPargenSM.ImpuestoModo = DBNullToStr(.Item("ACUMULARIMP"))
                        oPargenSM.ControlDispConPres = DBNullToBoolean(.Item("CONTROL_DISP_SC_CON_PRES"))
                        oPargenSM.Plurianual = DBNullToBoolean(.Item("PLURIANUAL"))
                        Me.Add(oPargenSM)
                    End With
                Next
            End If
            Return DsResul
        Else
            Return Nothing
        End If
    End Function
    'SMServer
    Public Enum EnumModoPresupuestacion
        IndependienteEstructuraOrganizativa = 0
        DependienteEstructuraOrganizativa = 1
    End Enum
    Public Enum EnumModoImputacion
        NivelCabecera = 0
        NivelLinea = 1
    End Enum
    Public Enum EnumTipoImputacion
        NoSeImputa = 0
        Opcional = 1
        Obligatorio = 2
    End Enum
    Public Enum EnumTipoAcumulacion
        SolicitudCompra = 0
        EmisionPedido = 1
        RecepcionPedido = 2
    End Enum
    ''' <summary>
    ''' Obtiene las cabeceras del grid de partidas presupuestarias que estan en la tabla PARGEN_SM_IDIOMA
    ''' </summary>
    ''' <param name="Idioma">Idioma en que se obtienes los tÃ­tulos de las cabeceras</param>
    ''' <returns>Un dataRow con las cabeceras en cada campo</returns>
    ''' <remarks></remarks>
    Public Function GetHeadersPartidasPresupuestarias(ByVal Idioma As String) As DataRow
        Return DBServer.PargensSMs_GetHeadersPartidasPresupuestarias(Idioma)
    End Function
End Class