﻿<Serializable()> _
Public Class SMEmpresas
    Inherits Security

    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub

    Private sEmpresa1 As Nullable(Of Integer)
    Private sEmpresa2 As Nullable(Of Integer)
    Private sEmpresa3 As Nullable(Of Integer)
    Private sEmpresa4 As Nullable(Of Integer)

    Public Property Empresa1() As Nullable(Of Integer)
        Get
            Return sEmpresa1
        End Get
        Set(ByVal value As Nullable(Of Integer))
            sEmpresa1 = value
        End Set
    End Property

    Public Property Empresa2() As Nullable(Of Integer)
        Get
            Return sEmpresa2
        End Get
        Set(ByVal value As Nullable(Of Integer))
            sEmpresa2 = value
        End Set
    End Property

    Public Property Empresa3() As Nullable(Of Integer)
        Get
            Return sEmpresa3
        End Get
        Set(ByVal value As Nullable(Of Integer))
            sEmpresa3 = value
        End Set
    End Property

    Public Property Empresa4() As Nullable(Of Integer)
        Get
            Return sEmpresa4
        End Get
        Set(ByVal value As Nullable(Of Integer))
            sEmpresa4 = value
        End Set
    End Property
End Class
