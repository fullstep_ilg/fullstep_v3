﻿<Serializable()> _
Public Class PargenSM
    Inherits Security

#Region " Constructor"
    Public Sub New(ByRef dbserver As FSNDataBaseServer.Root,ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
#End Region

    Private StrPres5 As String
    Private PresModo As EnumModoPresupuestacion
    Private ImpModo As EnumModoImputacion
    Private ImpSc As EnumTipoImputacion
    Private ImpPed As EnumTipoImputacion
    Private ImpRecep As EnumTipoImputacion
    Private ImpNiv As Integer
    Private strDenNivel As String
    Private AvisoVigencia As TipoVigencia
    Private DispSc As TipoAvisoBloqueo
    Private DispPed As TipoAvisoBloqueo
    Private DispRecep As TipoAvisoBloqueo
    Private ComprAcum As EnumTipoAcumulacion
    Private ModoImpuesto As Integer
    Private IntId As Integer
    Private m_bControlDispConPres As Boolean
    Private m_bPlurianual As Boolean


    Public Property Pres5() As String
        Get
            Return StrPres5
        End Get
        Set(ByVal value As String)
            StrPres5 = value
        End Set
    End Property

    Public Property ModoImputacion() As EnumModoImputacion
        Get
            Return ImpModo
        End Get
        Set(ByVal value As EnumModoImputacion)
            ImpModo = value
        End Set
    End Property

    Public Property ControlDispConPres() As Boolean
        Get
            Return m_bControlDispConPres
        End Get
        Set(ByVal value As Boolean)
            m_bControlDispConPres = value
        End Set
    End Property

    Public Property Plurianual() As Boolean
        Get
            Return m_bPlurianual
        End Get
        Set(ByVal value As Boolean)
            m_bPlurianual = value
        End Set
    End Property
    Public Property ImputacionSolicitudCompra() As EnumTipoImputacion
        Get
            Return ImpSc
        End Get
        Set(ByVal value As EnumTipoImputacion)
            ImpSc = value
        End Set
    End Property
    Public Property ImputacionPedido() As EnumTipoImputacion
        Get
            Return ImpPed
        End Get
        Set(ByVal value As EnumTipoImputacion)
            ImpPed = value
        End Set
    End Property

    Public Property ImputacionRecepcion() As EnumTipoImputacion
        Get
            Return ImpRecep
        End Get
        Set(ByVal value As EnumTipoImputacion)
            ImpRecep = value
        End Set
    End Property

    Public Property ImputacionNivel() As Integer
        Get
            Return ImpNiv
        End Get
        Set(ByVal value As Integer)
            ImpNiv = value
        End Set
    End Property

    Public Property Vigencia() As TipoVigencia
        Get
            Return AvisoVigencia
        End Get
        Set(ByVal value As TipoVigencia)
            AvisoVigencia = value
        End Set
    End Property

    Public Property ControlDispSC() As TipoAvisoBloqueo
        Get
            Return DispSc
        End Get
        Set(ByVal value As TipoAvisoBloqueo)
            DispSc = value
        End Set
    End Property

    Public Property AcumularComprometido() As EnumTipoAcumulacion
        Get
            Return ComprAcum
        End Get
        Set(ByVal value As EnumTipoAcumulacion)
            ComprAcum = value
        End Set
    End Property

    Public Property ImpuestoModo() As Integer
        Get
            Return ModoImpuesto
        End Get
        Set(ByVal value As Integer)
            ModoImpuesto = value
        End Set
    End Property

    Public Property DenNivel() As String
        Get
            Return strDenNivel
        End Get
        Set(ByVal value As String)
            strDenNivel = value
        End Set
    End Property

    Public Property Id() As Integer
        Get
            Return IntId
        End Get
        Set(ByVal value As Integer)
            IntId = value
        End Set
    End Property
    Public Enum EnumModoPresupuestacion
        IndependienteEstructuraOrganizativa = 0
        DependienteEstructuraOrganizativa = 1
    End Enum

    Public Enum EnumModoImputacion
        NivelCabecera = 0
        NivelLinea = 1
    End Enum

    Public Enum EnumTipoImputacion
        NoSeImputa = 0
        Opcional = 1
        Obligatorio = 2
    End Enum

    Public Enum EnumTipoAcumulacion
        SolicitudCompra = 0
        EmisionPedido = 1
        RecepcionPedido = 2
    End Enum


End Class
