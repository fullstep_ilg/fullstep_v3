﻿Imports Microsoft.VisualBasic
Imports System.Data


<Serializable()> _
Public Class PRES5
    Inherits Security

    Private Contexto As System.Web.HttpContext
    Private moData As DataSet
    Private sNiv0 As String

    Public ReadOnly Property Data() As DataSet
        Get
            Return moData
        End Get
    End Property
    Public Property NIV0() As String
        Get
            Return sNiv0
        End Get
        Set(ByVal value As String)
            sNiv0 = value
        End Set
    End Property
    ''' <summary>
    ''' Devuelve las partidas y las uons para mostrarlas en el autocompletar del buscador de partidas. 
    ''' </summary>
    ''' <param name="sPRES5">Código de la partida presupuetaria</param>
    ''' <param name="sUsuario">Código de la persona</param>
    ''' <param name="sIdioma">Idioma del usuario</param>
    ''' <param name="sCentroCoste">Código del centro de coste del que se quieren mostrar las partidas</param>
    ''' <returns>Devuelve un dataset</returns>
    ''' <remarks>Llamada desde: FSNWeb --> wucPartidas.ascx; Tiempo máximo: 1 sg;</remarks>
    Public Function LoadAutoCompletar(ByVal sPRES5 As String, ByVal sUsuario As String, ByVal sIdioma As String, Optional ByVal sCentroCoste As String = Nothing) As DataSet
        Authenticate()

        Dim ds As DataSet
        Contexto = System.Web.HttpContext.Current
        ds = CType(Contexto.Cache("dsPartidas_" & sUsuario & "_" & sCentroCoste), DataSet)
        If ds Is Nothing Then
            ds = DBServer.PRES5_LoadAutoCompletar(sPRES5, sUsuario, sIdioma, sCentroCoste)
            Contexto.Cache.Insert("dsPartidas_" & sUsuario & "_" & sCentroCoste, ds, Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration, New TimeSpan(0, System.Configuration.ConfigurationManager.AppSettings("TiempoExpiracionCacheMin"), 0))
        End If
        Return ds
    End Function
    ''' <summary>
    ''' Devuelve las partidas y las uons para mostrarlas en un treeview. Añade las relaciones a las tablas del dataset.
    ''' </summary>
    ''' <param name="sPRES5">Código de la partida presupuetaria</param>
    ''' <param name="sUsuario">Código de la persona</param>
    ''' <param name="sIdioma">Idioma del usuario</param>
    ''' <param name="bPlurianual">Indica si la partida presupuestaria es plurianual</param>
    ''' <param name="sCentroCoste">Código del centro de coste del que se quieren mostrar las partidas</param>
    ''' <param name="bVigentes">Si true, muestra las partidas vigentes, si false, todas las partidas</param>
    ''' <param name="dFechaInicioDesde">Fecha de inicio desde para buscar partidas</param>
    ''' <param name="dFechaFinHasta">Fecha fin hasta para buscar partidas</param>
    ''' <param name="sFormatoFecha">Formato en el que llegan las fechas</param>
    ''' <returns>Dataset con todas las partidas y sus uons relacionadas</returns>
    Public Function Partidas_LoadData(ByVal sPRES5 As String, ByVal sUsuario As String, ByVal sIdioma As String, ByVal bPlurianual As Boolean, Optional ByVal sCentroCoste As String = Nothing, Optional ByVal bVigentes As Boolean = True, Optional ByVal dFechaInicioDesde As Date = Nothing, Optional ByVal dFechaFinHasta As Date = Nothing, Optional ByVal sFormatoFecha As String = Nothing) As DataSet
        Authenticate()
        'Dim cacheName As String = "dsPartidas_" & sUsuario & "_" & sIdioma & IIf(String.IsNullOrEmpty(sCentroCoste), "", "_" & sCentroCoste) & IIf(bVigentes, "_v", "") & dFechaInicioDesde.ToShortDateString & dFechaFinHasta.ToShortDateString
        'If System.Web.HttpContext.Current.Cache(cacheName) Is Nothing Then
        moData = DBServer.Partidas_Load(sPRES5, sUsuario, sIdioma, bPlurianual, sCentroCoste, bVigentes, dFechaInicioDesde, dFechaFinHasta, sFormatoFecha)
        '    System.Web.HttpContext.Current.Cache.Insert(cacheName, moData, Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration, New TimeSpan(0, System.Configuration.ConfigurationManager.AppSettings("TiempoExpiracionCacheMin"), 0))
        'Else
        '    moData = CType(System.Web.HttpContext.Current.Cache(cacheName), DataSet)
        'End If
        Partidas_LoadData = moData

    End Function
    Public Function Partidas_LoadDataEnIM(ByVal sPRES5 As String, ByVal sUsuario As String, ByVal sIdioma As String, ByVal bPlurianual As Boolean, Optional ByVal sCentroCoste As String = Nothing, Optional ByVal bVigentes As Boolean = True, Optional ByVal dFechaInicioDesde As Date = Nothing, Optional ByVal dFechaFinHasta As Date = Nothing, Optional ByVal sFormatoFecha As String = Nothing) As DataSet
        Authenticate()
        Partidas_LoadDataEnIM = DBServer.Partidas_LoadEnIM(sPRES5, sUsuario, sIdioma, bPlurianual, sCentroCoste, bVigentes, dFechaInicioDesde, dFechaFinHasta, sFormatoFecha)
    End Function
    ''' <summary>
    ''' Devuelve un dataset con las partidas presupuestarias de cada nivel y relacionadas entre si para poder montar el 
    ''' ultrawebgrid por bandas
    ''' </summary>
    ''' <param name="sUON1">Código de UON1 del centro de coste que obtenemos las partidas presupuestarias</param>
    ''' <param name="sUON2">Código de UON2 del centro de coste que obtenemos las partidas presupuestarias</param>
    ''' <param name="sUON3">Código de UON3 del centro de coste que obtenemos las partidas presupuestarias</param>
    ''' <param name="sUON4">Código de UON4 del centro de coste que obtenemos las partidas presupuestarias</param>
    ''' <param name="sUsu">Código del usuario que obtiene las partidas presupuestarias para comprobar los permisos</param>
    ''' <param name="sIdioma">Idioma en que se obtendran las partidas presupuestarias</param>
    ''' <returns>Dataset con tantas tablas como niveles de partidas presupuestarias con sus respectivas relaciones</returns>
    ''' <remarks></remarks>
    Public Function DevolverArbolPartidas(ByVal sUON1 As String, ByVal sUON2 As String, _
                                          ByVal sUON3 As String, ByVal sUON4 As String, _
                                          ByVal sUsu As String, ByVal sIdioma As String) As DataSet
        Authenticate()
        DevolverArbolPartidas = DBServer.Partidas_ArbolJerarquico(sUON1, sUON2, sUON3, sUON4, sUsu, sIdioma)
    End Function
    Public Function LoadPartidasInstalacion(ByVal sIdioma As String) As DataTable
        Authenticate()
        LoadPartidasInstalacion = DBServer.PRES5_LoadPartidasInstalacion(sIdioma)
    End Function
    ''' Revisado por: blp. Fecha: 25/01/2013
    ''' <summary>
    ''' Devuelve las partidas presentes en los pedidos que un usuario puede consultar en la pantalla de seguimiento de EP
    ''' </summary>
    ''' <param name="codPer">Código del usuario</param>
    ''' <param name="idioma">Idioma</param>
    ''' <param name="VerPedidosOtrosUsuarios">Permiso del usuario para ver pedidos de otros usuarios</param>
    ''' <returns>Dataset con las partidas</returns>
    ''' <remarks>Llamada desde Seguimiento.aspx.vb->DevolverPartidasPedidos. Máximo 4,5 seg.</remarks>
    Public Function DevolverPartidasPedidos(ByVal codPer As String, ByVal idioma As FSNLibrary.Idioma, ByVal VerPedidosOtrosUsuarios As Boolean) As DataSet
        Authenticate()
        Return DBServer.Partidas_DevolverPartidasPedidos(codPer, idioma, VerPedidosOtrosUsuarios)
    End Function
    ''' Revisado por: blp. Fecha: 25/01/2013
    ''' <summary>
    ''' Devuelve las partidas presentes en los pedidos que un usuario puede consultar en la pantalla de seguimiento de EP
    ''' </summary>
    ''' <param name="idioma">Idioma</param>
    ''' <returns>Dataset con las partidas</returns>
    ''' <remarks>Llamada desde Seguimiento.aspx.vb->DevolverPartidasPedidos. Máximo 4,5 seg.</remarks>
    Public Function DevolverPartidasEP(ByVal Usuario As String, ByVal Idioma As FSNLibrary.Idioma, _
                    ByVal Pres5 As String, ByVal PedidosOtrasEmp As Boolean, ByVal sCentro As String, _
                    Optional ByVal IdEmpresa As Integer = Nothing, Optional ByVal sTextoFiltro As String = Nothing) As DataSet
        Authenticate()
        Return DBServer.Partidas_DevolverPartidasEP(Usuario, Idioma, Pres5, PedidosOtrasEmp, sCentro, IdEmpresa, sTextoFiltro)
    End Function
    '' Revisado por blp. Fecha 20/08/2013
    ''' <summary>
    ''' Cargar las partidas de nivel 0
    ''' </summary>
    ''' <param name="idioma">Idioma en el que devolver las partidas</param>
    ''' <remarks>Llamada desde wucBusquedaAvanzadaContratos.ascx.vb. Maximo 0,2 seg.</remarks>
    Public Function DevolverPartidasPres0PM(ByVal idioma As FSNLibrary.Idioma) As DataTable
        Authenticate()
        Return DBServer.Partidas_LoadPartidasPres0(idioma)
    End Function
    '' Revisado por blp. Fecha 20/08/2013
    ''' <summary>
    ''' Cargar las partidas
    ''' </summary>
    ''' <param name="idioma">Idioma en el que devolver las partidas</param>
    ''' <param name="filtro">Texto por el que filtrar</param>
    ''' <returns>Datatable con los datos de las partidas</returns>
    ''' <remarks>Llamada desde wucBusquedaAvanzadaContratos.ascx.vb. Maximo 0,2 seg.</remarks>
    Public Function DevolverPartidasPM(ByVal idioma As FSNLibrary.Idioma, ByVal filtro As String) As DataSet
        Authenticate()
        Return DBServer.Partidas_LoadPartidas(idioma, filtro)
    End Function
    '' Revisado por blp. Fecha 26/08/2013
    ''' <summary>
    ''' Cargar las UON de las partidas
    ''' </summary>
    ''' <param name="partidasPres">tipo tabla con listado de partidas</param>
    ''' <returns>Datatable con los datos de UONS de las partidas</returns>
    ''' <remarks>Llamada desde wucBusquedaAvanzadaContratos.ascx.vb. Maximo 0,2 seg.</remarks>
    Public Function DevolverPartidasUONsPM(ByVal partidasPres As DataTable) As DataTable
        Authenticate()
        Return DBServer.Partidas_LoadPartidasUONs(partidasPres)
    End Function
    '' Revisado por blp. Fecha 20/08/2013
    ''' <summary>
    ''' Cargar las partidas
    ''' </summary>
    ''' <param name="sPartidaPres">CÓDIGO DE LA PARTIDA: PRES0#PRES1#PRES2#PRES3#PRES4</param>
    ''' <param name="idioma">Idioma en el que devolver las partidas</param>
    ''' <returns>Datatable con los datos de la partida</returns>
    ''' <remarks>Llamada desde wucBusquedaAvanzadaContratos.ascx.vb. Maximo 0,2 seg.</remarks>
    Public Function DevolverPartidaPM(ByVal sPartidaPres As String, ByVal idioma As FSNLibrary.Idioma) As DataTable
        Authenticate()
        Return DBServer.Partidas_LoadPartida(sPartidaPres, idioma)
    End Function

    ''' <summary>
    ''' Saca de la configuración a q nivel son aceptables las partidas
    ''' </summary>
    ''' <param name="PartidaNivel0">CÓDIGO DE LA PARTIDA</param>
    ''' <returns>SELECT IMP_NIVEL FROM PARGEN_SM WITH(NOLOCK) WHERE PRES5=@PRES5</returns>
    Public Function DameNivelAceptablePartida(ByVal PartidaNivel0 As String) As Integer
        Authenticate()
        DameNivelAceptablePartida = DBServer.Partida_DameNivelAceptable(PartidaNivel0)
    End Function
#Region " Constructor"
    ''' <summary>
    ''' Constructor de la clase PRES5
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si estÃ¡ autenticado</param>
    ''' <remarks>El constructor debe ser pÃºblico para que pueda utilizarse con la funciÃ³n GetObject de la clase Root</remarks>
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
#End Region
End Class

