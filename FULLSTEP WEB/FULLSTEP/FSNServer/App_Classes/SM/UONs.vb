﻿<Serializable()> _
Public Class UONs
    Inherits Lista(Of UON)
    Public Overloads Property Item(ByVal Codigo As String) As UON
        Get
            Return Me.Find(Function(el As UON) el.ToString() = Codigo)
        End Get
        Set(ByVal value As UON)
            Dim ind As Integer = Me.FindIndex(Function(el As UON) el.ToString() = Codigo)
            If ind = -1 Then
                Me.Add(value)
            Else
                Me.Item(ind) = value
            End If
        End Set
    End Property
    ''' <summary>
    ''' Añade un elemento de tipo UON a la colección
    ''' </summary>
    ''' <param name="UON1">Código UON de nivel 1</param>
    ''' <param name="UON2">Código UON de nivel 2</param>
    ''' <param name="UON3">Código UON de nivel 3</param>
    ''' <param name="UON4">Código UON de nivel 4</param>
    ''' <param name="Denominacion">Denominación</param>
    ''' <returns>El elemento de tipo UON añadido</returns>
    Public Overloads Function Add(ByVal UON1 As String, ByVal UON2 As String, ByVal UON3 As String, ByVal UON4 As String, ByVal Denominacion As String) As UON
        Dim mUON As UON = New UON(mDBServer, mIsAuthenticated)
        mUON.UON1 = UON1
        mUON.UON2 = UON2
        mUON.UON3 = UON3
        mUON.UON4 = UON4
        mUON.Denominacion = Denominacion
        Me.Add(mUON)
        Return mUON
    End Function
    ''' <summary>
    ''' Busca un elemento que cumpla las condiciones definidas por el predicado especificado y devuelve la primera aparición en toda la estructura organizativa
    ''' </summary>
    ''' <param name="match">Delegado Predicate(Of UON) que define las condiciones del elemento que se va a buscar. </param>
    ''' <returns>Primer elemento que coincide con las condiciones definidas por el predicado especificado, si se encuentra; de lo contrario Nothing.</returns>
    Public Function FindInTree(ByVal match As System.Predicate(Of UON)) As UON
        Dim result As UON = Me.Find(match)
        If result Is Nothing Then
            For Each o As UON In Me
                If o.ChildUONs IsNot Nothing Then
                    result = o.ChildUONs.FindInTree(match)
                    If result IsNot Nothing Then Exit For
                End If
            Next
            Return result
        Else
            Return result
        End If
    End Function
    ''' Revisado por: blp. Fecha: 03/10/2012
    ''' <summary>
    ''' Devuelve el árbol de partidas presupuestarias a las que un usuario tiene permiso de imputación
    ''' para un árbol presupuestario especificado
    ''' </summary>
    ''' <param name="Usuario">Código de usuario</param>
    ''' <param name="Idioma">Idioma</param>
    ''' <param name="Pres5">Código del árbol de partidas presupuestarias</param>
    ''' <returns>El árbol de unidades organizativas con centros de coste y partidas presupuestarias</returns>
    Public Function ArbolPartidasImputacion(ByVal Usuario As String, ByVal Idioma As FSNLibrary.Idioma, _
            ByVal Pres5 As String, ByVal PedidosOtrasEmp As Boolean, _
            Optional ByVal IdEmpresa As Integer = Nothing) As UONs
        Authenticate()
        Dim result As DataTable

        result = DBServer.PRES5_PartidasImputacion(Usuario, Idioma, Pres5, PedidosOtrasEmp, IdEmpresa)

        Me.Clear()
        Dim query = From Datos In result _
                  Order By Datos.Item("UON1") _
                  Let UON1 = Datos.Item("UON1").ToString(), UON2 = String.Empty, UON3 = String.Empty, UON4 = String.Empty, Denominacion = Datos.Item("UON1_DEN").ToString() _
                  Select UON1, UON2, UON3, UON4, Denominacion Distinct
        For Each o In query
            Me.Add(o.UON1, o.UON2, o.UON3, o.UON4, o.Denominacion)
        Next
        query = From Datos In result _
              Where Datos.Item("UON2") IsNot DBNull.Value _
              Order By Datos.Item("UON1"), Datos.Item("UON2") _
              Let UON1 = Datos.Item("UON1").ToString(), UON2 = Datos.Item("UON2").ToString, UON3 = String.Empty, UON4 = String.Empty, Denominacion = Datos.Item("UON2_DEN").ToString() _
              Select UON1, UON2, UON3, UON4, Denominacion Distinct
        For Each o In query
            Dim u1 As UON = Me.Item(o.UON1.ToString())
            If u1.ChildUONs Is Nothing Then u1.ChildUONs = New UONs(mDBServer, mIsAuthenticated)
            u1.ChildUONs.Add(o.UON1, o.UON2, o.UON3, o.UON4, o.Denominacion)
        Next
        query = From Datos In result _
               Where Datos.Item("UON2") IsNot DBNull.Value And Datos.Item("UON3") IsNot DBNull.Value _
               Order By Datos.Item("UON1"), Datos.Item("UON2"), Datos.Item("UON3") _
               Let UON1 = Datos.Item("UON1").ToString(), UON2 = Datos.Item("UON2").ToString(), UON3 = Datos.Item("UON3").ToString, UON4 = String.Empty, Denominacion = Datos.Item("UON3_DEN").ToString() _
               Select UON1, UON2, UON3, UON4, Denominacion Distinct
        For Each o In query
            Dim u1 As UON = Me.Item(o.UON1.ToString())
            Dim u2 As UON = u1.ChildUONs.Item(String.Concat(o.UON1, "#", o.UON2))
            If u2.ChildUONs Is Nothing Then u2.ChildUONs = New UONs(mDBServer, mIsAuthenticated)
            u2.ChildUONs.Add(o.UON1, o.UON2, o.UON3, o.UON4, o.Denominacion)
        Next
        query = From Datos In result _
              Where Datos.Item("UON2") IsNot DBNull.Value And Datos.Item("UON3") IsNot DBNull.Value And Datos.Item("UON4") IsNot DBNull.Value _
              Order By Datos.Item("UON1"), Datos.Item("UON2"), Datos.Item("UON3"), Datos.Item("UON4") _
              Let UON1 = Datos.Item("UON1").ToString(), UON2 = Datos.Item("UON2").ToString(), UON3 = Datos.Item("UON3").ToString(), UON4 = Datos.Item("UON4").ToString, Denominacion = Datos.Item("UON4_DEN").ToString() _
              Select UON1, UON2, UON3, UON4, Denominacion Distinct
        For Each o In query
            Dim u1 As UON = Me.Item(o.UON1.ToString())
            Dim u2 As UON = u1.ChildUONs.Item(String.Concat(o.UON1, "#", o.UON2))
            Dim u3 As UON = u2.ChildUONs.Item(String.Concat(o.UON1, "#", o.UON2, "#", o.UON3))
            If u3.ChildUONs Is Nothing Then u3.ChildUONs = New UONs(mDBServer, mIsAuthenticated)
            u3.ChildUONs.Add(o.UON1, o.UON2, o.UON3, o.UON4, o.Denominacion)
        Next
        Dim partidas = From Datos In result _
                       Order By DBNullToStr(Datos.Item("UON1")), DBNullToStr(Datos.Item("UON2")), DBNullToStr(Datos.Item("UON3")), DBNullToStr(Datos.Item("UON4")) _
                       Let UON1 = Datos.Item("UON1").ToString(), UON2 = DBNullToStr(Datos.Item("UON2")), UON3 = DBNullToStr(Datos.Item("UON3")), UON4 = DBNullToStr(Datos.Item("UON4")), _
                            CentroSM = Datos.Item("CENTRO_SM").ToString(), CentroDenominacion = Datos.Item("CENTRO_SM_DEN").ToString(), _
                            PRES0 = Datos.Item("PRES0").ToString(), PRES1 = Datos.Item("PRES1").ToString(), PRES2 = DBNullToStr(Datos.Item("PRES2")), PRES3 = DBNullToStr(Datos.Item("PRES3")), PRES4 = DBNullToStr(Datos.Item("PRES4")), _
                            PRES5Denominacion = Datos.Item("PRES5_DEN").ToString(), PresupuestoId = Datos.Item("PRES5_IMP"), FechaInicio = Datos.Item("FECINI").ToString, FechaFin = Datos.Item("FECFIN").ToString, _
                            EMPRESA1 = Datos.Item("EMP1").ToString(), EMPRESA2 = Datos.Item("EMP2").ToString(), EMPRESA3 = Datos.Item("EMP3").ToString(), EMPRESA4 = Datos.Item("EMP4").ToString() _
                        Select UON1, UON2, UON3, UON4, CentroSM, CentroDenominacion, PRES0, PRES1, PRES2, PRES3, PRES4, PRES5Denominacion, PresupuestoId, FechaInicio, FechaFin, EMPRESA1, EMPRESA2, EMPRESA3, EMPRESA4 Distinct

        For Each o In partidas
            Dim codUON As String = String.Concat(o.UON1, IIf(String.IsNullOrEmpty(o.UON2), String.Empty, "#" & o.UON2), _
                                    IIf(String.IsNullOrEmpty(o.UON3), String.Empty, "#" & o.UON3), IIf(String.IsNullOrEmpty(o.UON4), String.Empty, "#" & o.UON4))
            Dim u As UON = Me.FindInTree(Function(el As UON) el.ToString() = codUON)
            If u.CentroSM Is Nothing Then
                u.CentroSM = New Centro_SM(mDBServer, mIsAuthenticated)
                u.CentroSM.Codigo = o.CentroSM
                u.CentroSM.Denominacion = o.CentroDenominacion
                u.CentroSM.Empresas = New SMEmpresas(mDBServer, mIsAuthenticated)
                u.CentroSM.Empresas.Empresa1 = strToInt(o.EMPRESA1)
                u.CentroSM.Empresas.Empresa2 = strToInt(o.EMPRESA2)
                u.CentroSM.Empresas.Empresa3 = strToInt(o.EMPRESA3)
                u.CentroSM.Empresas.Empresa4 = strToInt(o.EMPRESA4)
            End If
            If u.CentroSM.PartidasPresupuestarias Is Nothing Then
                u.CentroSM.PartidasPresupuestarias = New PartidasPRES5(mDBServer, mIsAuthenticated)
            End If
            If u.CentroSM.PartidasPresupuestarias_EP Is Nothing Then
                u.CentroSM.PartidasPresupuestarias_EP = New List(Of PartidaPRES5)
            End If
            Dim oPartida As New PartidaPRES5(mDBServer, mIsAuthenticated)
            oPartida.NIV0 = o.PRES0
            oPartida.NIV1 = o.PRES1
            oPartida.NIV2 = o.PRES2
            oPartida.NIV3 = o.PRES3
            oPartida.NIV4 = o.PRES4
            oPartida.Denominacion = o.PRES5Denominacion
            oPartida.PresupuestoId = o.PresupuestoId
            If Not o.FechaInicio = String.Empty Then
                oPartida.FechaInicioPresupuesto = CType(o.FechaInicio, DateTime)
            End If
            If Not o.FechaFin = String.Empty Then
                oPartida.FechaFinPresupuesto = CType(o.FechaFin, DateTime)
            End If
            u.CentroSM.PartidasPresupuestarias.Add(oPartida)
            u.CentroSM.PartidasPresupuestarias_EP.Add(oPartida)
        Next
        Return Me
    End Function
    ''' <summary>
    ''' Crea un árbol con las entradas correspondientes al árbol presupuestario indicado
    ''' </summary>
    ''' <param name="Pres5">Código del árbol presupuestario</param>
    ''' <returns>Árbol de la estrucutra organizativa con centros de coste y partidas del árbol presupuestario indicado</returns>
    Public Function FiltrarPorArbolPresupuestario(ByVal Pres5 As String) As UONs
        Return FiltrarPorArbolPresupuestario(Pres5, False)
    End Function
    ''' <summary>
    ''' Crea un árbol con las entradas correspondientes al árbol presupuestario indicado
    ''' </summary>
    ''' <param name="Pres5">Código del árbol presupuestario</param>
    ''' <param name="SoloVigentes">Si True sólo muestra las partidas presupuestarias con presupuestos vigentes</param>
    ''' <returns>Árbol de la estrucutra organizativa con centros de coste y partidas del árbol presupuestario indicado</returns>
    Public Function FiltrarPorArbolPresupuestario(ByVal Pres5 As String, ByVal SoloVigentes As Boolean) As UONs
        Dim arbol As New UONs(mDBServer, mIsAuthenticated)
        If TienePartidasDePres5(Me, Pres5, SoloVigentes) Then
            For Each u1 As UON In Me
                If TienePartidasDePres5(u1, Pres5, SoloVigentes) Then
                    AnyadirUONFiltroPorArbolPres(arbol, u1, Pres5, SoloVigentes)
                End If
            Next
        End If
        Return arbol
    End Function
    ''' Revisado por: blp. Fecha: 03/10/2012
    ''' <summary>
    ''' Rutina recursiva para añadir uons al árbol filtrado por el código del árbol presupuestario
    ''' </summary>
    ''' <param name="arbol">Colección UONs a la que añadir la UON</param>
    ''' <param name="uon">UON a añadir a la colección</param>
    ''' <param name="Pres5">Código del árbol presupeustario por el que filtrar</param>
    Private Sub AnyadirUONFiltroPorArbolPres(ByVal arbol As UONs, ByVal uon As UON, ByVal Pres5 As String, ByVal SoloVigentes As Boolean)
        Dim nueva As UON = arbol.Add(uon.UON1, uon.UON2, uon.UON3, uon.UON4, uon.Denominacion)
        If uon.CentroSM IsNot Nothing AndAlso uon.CentroSM.PartidasPresupuestarias IsNot Nothing _
            AndAlso uon.CentroSM.PartidasPresupuestarias.Find(Function(el As PartidaPRES5) el.NIV0 = Pres5 And (Not SoloVigentes Or el.TienePresupuestoVigente)) IsNot Nothing Then
            nueva.CentroSM = New Centro_SM(mDBServer, mIsAuthenticated)
            nueva.CentroSM.Codigo = uon.CentroSM.Codigo
            nueva.CentroSM.Denominacion = uon.CentroSM.Denominacion
            nueva.CentroSM.Empresas = New SMEmpresas(mDBServer, mIsAuthenticated)
            nueva.CentroSM.Empresas.Empresa1 = uon.CentroSM.Empresas.Empresa1
            nueva.CentroSM.Empresas.Empresa2 = uon.CentroSM.Empresas.Empresa2
            nueva.CentroSM.Empresas.Empresa3 = uon.CentroSM.Empresas.Empresa3
            nueva.CentroSM.Empresas.Empresa4 = uon.CentroSM.Empresas.Empresa4
            nueva.CentroSM.PartidasPresupuestarias = New PartidasPRES5(mDBServer, mIsAuthenticated)
            nueva.CentroSM.PartidasPresupuestarias_EP = New List(Of PartidaPRES5)
            For Each p As PartidaPRES5 In uon.CentroSM.PartidasPresupuestarias
                If p.NIV0 = Pres5 And (Not SoloVigentes Or p.TienePresupuestoVigente) Then
                    nueva.CentroSM.PartidasPresupuestarias.Add(p)
                    nueva.CentroSM.PartidasPresupuestarias_EP.Add(p)
                End If
            Next
        End If
        If uon.ChildUONs IsNot Nothing Then
            For Each u As UON In uon.ChildUONs
                If TienePartidasDePres5(u, Pres5, SoloVigentes) Then
                    If nueva.ChildUONs Is Nothing Then nueva.ChildUONs = New UONs(mDBServer, mIsAuthenticated)
                    AnyadirUONFiltroPorArbolPres(nueva.ChildUONs, u, Pres5, SoloVigentes)
                End If
            Next
        End If
    End Sub
    ''' <summary>
    ''' Comprueba si el árbol tiene alguna entrada con partidas presupuestarias del árbol presupuestario indicado
    ''' </summary>
    ''' <param name="Arbol">Estructura organizativa</param>
    ''' <param name="Pres5">Código del árbol presupuestario</param>
    ''' <returns>Un Boolean, True si existe alguna entrada, si no False</returns>
    Private Function TienePartidasDePres5(ByVal Arbol As UONs, ByVal Pres5 As String, ByVal SoloVigentes As Boolean) As Boolean
        Dim o As UON = Arbol.FindInTree(Function(el As UON) _
                             el.CentroSM IsNot Nothing _
                             AndAlso el.CentroSM.PartidasPresupuestarias IsNot Nothing _
                             AndAlso el.CentroSM.PartidasPresupuestarias.Find(Function(pel As PartidaPRES5) pel.NIV0 = Pres5 And (Not SoloVigentes Or pel.TienePresupuestoVigente)) IsNot Nothing)
        Return (o IsNot Nothing)
    End Function
    ''' <summary>
    ''' Comprueba si la UON tiene alguna entrada con partidas presupuestarias del árbol presupuestario indicado
    ''' </summary>
    ''' <param name="uon">Tipo UON en el que buscar</param>
    ''' <param name="Pres5">Código del árbol presupuestario</param>
    ''' <returns>True si existe alguna entrada, si no False</returns>
    Private Function TienePartidasDePres5(ByVal uon As UON, ByVal Pres5 As String, ByVal SoloVigentes As Boolean) As Boolean
        If uon.CentroSM IsNot Nothing AndAlso uon.CentroSM.PartidasPresupuestarias IsNot Nothing _
            AndAlso uon.CentroSM.PartidasPresupuestarias.Find(Function(el As PartidaPRES5) el.NIV0 = Pres5 And (Not SoloVigentes Or el.TienePresupuestoVigente)) IsNot Nothing Then
            Return True
        ElseIf uon.ChildUONs IsNot Nothing Then
            Return TienePartidasDePres5(uon.ChildUONs, Pres5, SoloVigentes)
        Else
            Return False
        End If
    End Function
    ''' <summary>
    ''' Devuelve una lista de los centros de coste correspondientes a las UONs de la colección
    ''' </summary>
    ''' <returns>Elemento Centros_SM con los centros de coste de las UONs</returns>
    Public Function DevolverCentrosdeCoste() As Centros_SM
        Dim lista As New Centros_SM(mDBServer, mIsAuthenticated)
        AnyadirCentrosDeCoste(Me, lista)
        Return lista
    End Function
    ''' Revisado por: blp. Fecha: 29/11/2011
    ''' <summary>
    ''' Rutina recursiva para añadir centros de coste de las UONs a la lista especificada
    ''' </summary>
    ''' <param name="arbol">Objeto UONs con el árbol en el que buscar</param>
    ''' <param name="listacentros">Objeto Centros_SM en el que añadir los centros de coste</param>
    ''' <remarks>Llamada desde: DevolverCentrosdeCoste. Máx. 0,5 seg.</remarks>
    Private Sub AnyadirCentrosDeCoste(ByVal arbol As UONs, ByVal listacentros As Centros_SM)
        For Each u As UON In arbol
            If u.CentroSM IsNot Nothing Then
                If listacentros.Item(u.CentroSM.Codigo) Is Nothing Then
                    Dim oCentroSM As New Fullstep.FSNServer.Centro_SM(mDBServer, mIsAuthenticated)
                    oCentroSM = u.CentroSM
                    Dim oUON As New Fullstep.FSNServer.UON(mDBServer, mIsAuthenticated)
                    oUON.UON1 = u.UON1
                    oUON.UON2 = u.UON2
                    oUON.UON3 = u.UON3
                    oUON.UON4 = u.UON4
                    oCentroSM.UONS = oUON
                    listacentros.Add(oCentroSM)
                ElseIf u.CentroSM.PartidasPresupuestarias IsNot Nothing Then
                    If listacentros.Item(u.CentroSM.Codigo).PartidasPresupuestarias Is Nothing Then
                        listacentros.Item(u.CentroSM.Codigo).PartidasPresupuestarias = u.CentroSM.PartidasPresupuestarias
                        listacentros.Item(u.CentroSM.Codigo).PartidasPresupuestarias_EP = u.CentroSM.PartidasPresupuestarias_EP
                    Else
                        For Each p As PartidaPRES5 In u.CentroSM.PartidasPresupuestarias
                            If listacentros.Item(u.CentroSM.Codigo).PartidasPresupuestarias.Item(p.ToString()) Is Nothing Then
                                listacentros.Item(u.CentroSM.Codigo).PartidasPresupuestarias.Add(p)
                                listacentros.Item(u.CentroSM.Codigo).PartidasPresupuestarias_EP.Add(p)
                            End If
                        Next
                    End If
                End If
            End If
            If u.ChildUONs IsNot Nothing Then
                AnyadirCentrosDeCoste(u.ChildUONs, listacentros)
            End If
        Next
    End Sub
    ''' <summary>
    ''' Devuelve una lista de todas las UONs del árbol
    ''' </summary>
    ''' <returns>Un objeto UONs con tolos los elementos UON del árbol</returns>
    Public Function ListaUONs() As UONs
        Dim lista As New UONs(mDBServer, mIsAuthenticated)
        For Each o As UON In Me
            AnyadirUONALista(lista, o)
        Next
        Return lista
    End Function
    ''' <summary>
    ''' Rutina recursiva para generar la lista de UONs
    ''' </summary>
    ''' <param name="lista">Objeto UONs en el que generar la lista</param>
    ''' <param name="el">Objeto UON a añadir</param>
    Private Sub AnyadirUONALista(ByVal lista As UONs, ByVal el As UON)
        lista.Add(el)
        If el.ChildUONs IsNot Nothing Then
            For Each o As UON In el.ChildUONs
                AnyadirUONALista(lista, o)
            Next
        End If
    End Sub
#Region " Constructor"
    ''' <summary>
    ''' Constructor de la clase UONs
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si está autenticado</param>
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
#End Region
End Class
