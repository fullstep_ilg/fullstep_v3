﻿
<Serializable()> _
Public Class Activos
    Inherits Lista(Of Activo)

    Private moData As DataTable

    Public ReadOnly Property Data() As DataTable
        Get
            Return moData
        End Get
    End Property

    ''' <summary>
    ''' Constructor del objeto Activo
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si está autenticado</param>
    ''' <remarks></remarks>
    Public Sub New(ByRef dbserver As Fullstep.FSNDataBaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
    End Sub

    ''' <summary>
    ''' Devuelve el objeto Activo correspondiente al índice especificado.
    ''' </summary>
    ''' <param name="Id">Valor Integer con el índice del elemento</param>
    ''' <returns>Objeto Activo correspondiente al índice, si exite; Nothing en caso contrario.</returns>
    ''' <remarks></remarks>
    Public Overloads ReadOnly Property Item(ByVal Id As Integer) As Activo
        Get
            Return Me.Find(Function(elemento As Activo) elemento.ID = Id)
        End Get
    End Property

    ''' <summary>
    ''' Devuelve el objeto Activo correspondiente al código especificado.
    ''' </summary>
    ''' <param name="Cod">Valor String con el código del elemento</param>
    ''' <returns>Objeto Activo correspondiente al código, si exite; Nothing en caso contrario.</returns>
    ''' <remarks></remarks>
    Public Overloads ReadOnly Property Item(ByVal Cod As String) As Activo
        Get
            Return Me.Find(Function(elemento As Activo) elemento.Codigo = Cod)
        End Get
    End Property

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

    ''' <summary>
    ''' Procedimiento que añade un nuevo objeto de tipo Activo a la colección
    ''' </summary>
    ''' <param name="Centro">Código del centro de coste(UON) asignado al activo</param>
    ''' <param name="Cod">Código del activo</param>
    ''' <param name="Cod_Erp">Código ERP del artículo</param>
    ''' <param name="Den">Denominación del activo</param>
    ''' <param name="Empresa">Empresa asignada al activo</param>
    ''' <param name="Id">Identificador del activo en la BBDD</param>
    ''' <remarks>
    ''' Llamada desde: Esta misma clase
    ''' Tiempo máximo: 0 seg</remarks>
    Public Overloads Sub Add(ByVal Id As Integer, ByVal Cod As String, ByVal Den As String, ByVal Cod_Erp As String, ByVal Empresa As String, ByVal Centro As String)
        Dim ObjNewMember As New Activo(DBServer, mIsAuthenticated)
        If Not IsNothing(Id) AndAlso Not Id = 0 AndAlso Not IsDBNull(Id) Then
            ObjNewMember.ID = Id
        Else
            Dim MaxId As Integer = 0
            For i As Integer = 0 To MyBase.Count - 1
                If MyBase.Item(i).ID > MaxId Then
                    MaxId = MyBase.Item(i).ID
                End If
            Next
            ObjNewMember.ID = MaxId + 1
        End If
        ObjNewMember.Codigo = Cod
        ObjNewMember.Denominacion = Den
        ObjNewMember.Cod_Erp = Cod_Erp
        ObjNewMember.Empresa = Empresa
        ObjNewMember.Centro_SM = Centro

        MyBase.Add(ObjNewMember)
        ObjNewMember = Nothing
    End Sub

    ''' <summary>
    ''' Devuelve un DataSet con todos los activos con la denominación en el idioma especificado,
    ''' y con las partidas presupuestarias correspondientes
    ''' </summary>
    ''' <param name="Idioma">Código de idioma</param>
    ''' <returns>Un DataSet con la tabla ACTIVO y la tabla </returns>
    Public Function ObtenerActivos(ByVal Idioma As String, ByVal Empresa As Nullable(Of Integer), ByVal CentroCoste As String, Optional ByVal CargarEnColeccion As Boolean = False) As DataTable
        Authenticate()
        Dim dtActivos As DataTable = DBServer.Activos_Cargar(Idioma, Empresa, CentroCoste)
        dtActivos.TableName = "ACTIVO"
        If CargarEnColeccion Then
            For Each fila As DataRow In dtActivos.Rows
                With fila
                    Me.Add(DBNullToDec(.Item("ID")), DBNullToStr(.Item("Cod")), DBNullToStr(.Item("Den_Activo")), DBNullToStr(.Item("Cod_Erp")), DBNullToDec(.Item("Empresa")), DBNullToStr(.Item("Centro_Sm")))
                End With
            Next
        End If
        Return dtActivos
    End Function

    ''' <summary>
    ''' Devuelve los activos asociados a los centros de coste SM a los que el usuario puede imputar
    ''' </summary>
    ''' <param name="Idioma">Idioma del usuario</param>
    ''' <param name="usu">Código del usuario</param>
    ''' <returns>Un DataTable con los activos</returns>
    Public Function ObtenerActivos_CC_Imputacion(ByVal Idioma As String, ByVal usu As String, Optional ByVal UON1 As String = Nothing, Optional ByVal UON2 As String = Nothing, Optional ByVal UON3 As String = Nothing, Optional ByVal UON4 As String = Nothing, Optional ByVal Empresa As Integer = 0) As DataTable
        Dim dtActivos As DataTable = DBServer.Activos_CC_Imputacion_Cargar(Idioma, usu, UON1, UON2, UON3, UON4, Empresa)
        dtActivos.TableName = "ACTIVOS_USU"
        moData = dtActivos
        Return dtActivos
    End Function

    ''' <summary>
    ''' Devuelve un valor que indica si existe integracion en sentido salida o entrada/salida
    ''' </summary>
    ''' <returns>Un valor Boolean que indica si existe integracion en sentido salida o entrada/salida</returns>
    ''' <remarks></remarks>
    Public Function HayIntegracionSentidoSalida() As Boolean
        Authenticate()
        Return DBServer.HayIntegracion(TablasIntegracion.Activos, SentidoIntegracion.Salida)
    End Function

End Class
