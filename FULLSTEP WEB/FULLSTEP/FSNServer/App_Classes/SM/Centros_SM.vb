﻿Public Class Centros_SM
    Inherits Lista(Of Centro_SM)

    Public Overloads Property Item(ByVal Codigo As String) As Centro_SM
        Get
            Return Me.Find(Function(el As Centro_SM) el.Codigo = Codigo)
        End Get
        Set(ByVal value As Centro_SM)
            Dim ind As Integer = Me.FindIndex(Function(el As Centro_SM) el.Codigo = Codigo)
            If ind = -1 Then
                Me.Add(value)
            Else
                Me.Item(ind) = value
            End If
        End Set
    End Property
    ''' <summary>
    ''' Devuelve los centros de coste con toda la estructura organizativa o solo los centros de coste para mostrarlas en un treeview. Añade las relaciones a las tablas del dataset.
    ''' </summary>
    ''' <param name="sUsuario">Cod de la persona</param>
    ''' <param name="sIdioma">Idioma del usuario</param>
    ''' <param name="bVerUON">true: se ve la estructura organizativa; false: sólo los centros de coste</param>
    ''' <param name="sDesde">De donde se realiza la llamada: (1) desde Control Presupuestario</param>
    ''' <returns>Dataset con los centros de coste y su estructura</returns>
    ''' <remarks>Llamada desde: FSNWeb --> usercontrols/wucCentrosCoste.ascx; Tiempo mÃ¡ximo: 1 sg;</remarks>
    Public Function LoadData(ByVal sUsuario As String, ByVal sIdioma As String, Optional ByVal bVerUON As Boolean = False, Optional ByVal sDesde As String = "", Optional ByVal iIdEmpresa As Integer = 0) As DataSet
        Authenticate()
        LoadData = DBServer.CentrosCoste_Load(sUsuario, sIdioma, bVerUON, sDesde, iIdEmpresa)
    End Function
    ''' <summary>
    ''' Devuelve los centros de coste con toda la estructura organizativa o solo los centros de coste para mostrarlas en un treeview. Añade las relaciones a las tablas del dataset.
    ''' </summary>
    ''' <param name="sUsuario">Cod de la persona</param>
    ''' <param name="sIdioma">Idioma del usuario</param>
    ''' <param name="bVerUON">true: se ve la estructura organizativa; false: sólo los centros de coste</param>
    ''' <param name="sDesde">De donde se realiza la llamada: (1) desde Control Presupuestario</param>
    ''' <returns>Dataset con los centros de coste y su estructura</returns>
    ''' <remarks>Llamada desde: FSNWeb --> usercontrols/wucCentrosCoste.ascx; Tiempo mÃ¡ximo: 1 sg;</remarks>
    Public Function LoadDataEP(ByVal sUsuario As String, ByVal sIdioma As String, ByVal Empresa As String, Optional ByVal bVerUON As Boolean = False, Optional ByVal sDesde As String = "", Optional ByVal sTextoFiltro As String = "") As DataSet
        Authenticate()
        LoadDataEP = DBServer.CentrosCoste_LoadEP(sUsuario, sIdioma, Empresa, bVerUON, sDesde, sTextoFiltro)
    End Function
    Public Function LoadDataEnIM(ByVal sUsuario As String, ByVal sIdioma As String, Optional ByVal bVerUON As Boolean = False, Optional ByVal sDesde As String = "") As DataSet
        Authenticate()
        LoadDataEnIM = DBServer.CentrosCoste_LoadEnIM(sUsuario, sIdioma, bVerUON, sDesde)
    End Function
    Public Function Obtener_Centros_Coste(ByVal CodUsuario As String, ByVal Idioma As String, ByVal listaUON As String,
                                          ByVal EsAutocompletar As Boolean, ByVal filtroCodigo As String, ByVal filtroDenominacion As String) As List(Of Centro_SM)
        Authenticate()
        Dim iCentroCoste As Centro_SM
        Dim lCentrosCoste As New List(Of Centro_SM)
        For Each dr As DataRow In DBServer.Obtener_Centros_SM(CodUsuario, Idioma, listaUON, EsAutocompletar, filtroCodigo, filtroDenominacion).Rows
            iCentroCoste = New Centro_SM
            iCentroCoste.Codigo = dr("CODIGO")
            iCentroCoste.Denominacion = dr("DENOMINACION")
            lCentrosCoste.Add(iCentroCoste)
        Next
        Return lCentrosCoste
    End Function
    ''' <summary>
    ''' Constructor de la clase CentrosCoste
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si está autenticado</param>
    ''' <remarks>El constructor debe ser público para que pueda utilizarse con la función GetObject de la clase Root</remarks>
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
    End Sub
End Class
