﻿<Serializable()> _
Public Class Imputaciones
    Inherits Lista(Of Imputacion)

    Public Property ItemId(ByVal Id As Integer) As Imputacion
        Get
            Return Me.Find(Function(el As Imputacion) el.Id = Id)
        End Get
        Set(ByVal value As Imputacion)
            Dim ind As Integer = Me.FindIndex(Function(el As Imputacion) el.Id = Id)
            If ind = -1 Then
                Me.Add(value)
            Else
                Me.Item(ind) = value
            End If
        End Set
    End Property


#Region " Constructor"

    ''' <summary>
    ''' Constructor de la clase Imputaciones
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si está autenticado</param>
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub


#End Region

End Class
