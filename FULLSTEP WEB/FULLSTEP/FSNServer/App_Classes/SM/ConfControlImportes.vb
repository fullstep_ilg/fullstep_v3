﻿Imports Microsoft.VisualBasic
Imports System.Data

<Serializable()> _
Public Class ConfControlImportes
    Inherits Security

    Private sCodUsu As String
    Private bVisualizarCodigo As Boolean
    Private bVisualizarDenominacion As Boolean
    Private bVisualizarLeyendas As Boolean
    Private bVisualizarTipoControl As Boolean
    Private iVistaPorDefecto As Integer

    Public Property CodUsu() As String
        Get
            Return sCodUsu
        End Get
        Set(ByVal value As String)
            sCodUsu = value
        End Set
    End Property

    Public Property VisualizarCodigo() As Boolean
        Get
            Return bVisualizarCodigo
        End Get
        Set(ByVal value As Boolean)
            bVisualizarCodigo = value
        End Set
    End Property

    Public Property VisualizarDenominacion() As Boolean
        Get
            Return bVisualizarDenominacion
        End Get
        Set(ByVal value As Boolean)
            bVisualizarDenominacion = value
        End Set
    End Property

    Public Property VisualizarLeyendas() As Boolean
        Get
            Return bVisualizarLeyendas
        End Get
        Set(ByVal value As Boolean)
            bVisualizarLeyendas = value
        End Set
    End Property

    Public Property VisualizarTipoControl() As Boolean
        Get
            Return bVisualizarTipoControl
        End Get
        Set(ByVal value As Boolean)
            bVisualizarTipoControl = value
        End Set
    End Property

    Public Property VistaPorDefecto() As Integer
        Get
            Return iVistaPorDefecto
        End Get
        Set(ByVal value As Integer)
            iVistaPorDefecto = value
        End Set
    End Property

    ''' <summary>
    ''' Procedimiento que carga los datos de la configuracion del control de importes de un usuario
    ''' </summary>
    ''' <remarks>
    ''' Llamada desdE: Ninguna
    ''' Tiempo mÃ¡ximo: 0,2 seg</remarks>
    Public Sub LoadData()
        Dim ds As DataSet
        Authenticate()
        
        ds = DBServer.ConfControlImportes_LoadData(sCodUsu)

        bVisualizarCodigo = ds.Tables(0).Rows(0).Item(1)
        bVisualizarDenominacion = ds.Tables(0).Rows(0).Item(2)
        bVisualizarTipoControl = ds.Tables(0).Rows(0).Item(3)
        bVisualizarLeyendas = ds.Tables(0).Rows(0).Item(4)
        iVistaPorDefecto = ds.Tables(0).Rows(0).Item(5)
    End Sub

    ''' <summary>
    ''' Procedimiento que actualiza los datos de la configuracion de control de importes
    ''' </summary>
    ''' <remarks>
    ''' Llamada desde: Ninguna
    ''' Tiempo mÃ¡ximo: 0,15 seg</remarks>
    Public Sub Actualizar()
        Authenticate()
        DBServer.ConfControlImportes_Actualizar(sCodUsu, bVisualizarCodigo, bVisualizarDenominacion, bVisualizarTipoControl, bVisualizarLeyendas, iVistaPorDefecto)
    End Sub
#Region " Constructor"
    Public Sub New(ByRef dbserver As FSNDataBaseServer.Root,ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
    End Sub
#End Region

End Class
