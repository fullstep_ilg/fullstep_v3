﻿
<Serializable()> _
Public Class PartidasPRES5
    Inherits Lista(Of PartidaPRES5)

#Region " Constructor"
    ''' <summary>
    ''' Constructor de la clase PartidasPRES5
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si está autenticado</param>
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
    End Sub
#End Region
    Public Overloads Property Item(ByVal Codigo As String) As PartidaPRES5
        Get
            Return Me.Find(Function(el As PartidaPRES5) el.ToString() = Codigo)
        End Get
        Set(ByVal value As PartidaPRES5)
            Dim ind As Integer = Me.FindIndex(Function(el As PartidaPRES5) el.ToString() = Codigo)
            If ind = -1 Then
                Me.Add(value)
            Else
                Me.Item(ind) = value
            End If
        End Set
    End Property
    ''' <summary>
    ''' Función que devuelve los datos correspondientes a una partida dada, para un centro de coste específico
    ''' </summary>
    ''' <param name="idioma">idioma en que se quiere recuperar la información</param>
    ''' <param name="PRES0">Código del árbol presupuestario del que se quiere recuperar la partida (PRES5 en otros sitios)</param>
    ''' <param name="PRES1">Código de la partida de nivel 1</param>
    ''' <param name="PRES2">Código de la partida de nivel 2 (si la hay)</param>
    ''' <param name="PRES3">Código de la partida de nivel 3 (si la hay)</param>
    ''' <param name="PRES4">Código de la partida de nivel 4 (si la hay)</param>
    ''' <param name="CENTROSM">Código del centro de coste</param>
    ''' <returns>objeto Datatable con la información</returns>
    ''' <remarks>Llamada desde EPWeb\Consultas.asmx.vb -> PartidaPresupuestaria_Datos. Tiempo máximo inferior a 1 seg.</remarks>
    Public Function Datos_Partida(ByVal idioma As FSNLibrary.Idioma, ByVal PRES0 As String, ByVal PRES1 As String, ByVal PRES2 As String, ByVal PRES3 As String, ByVal PRES4 As String, ByVal CENTROSM As String) As DataTable
        Dim dtDatosPartida As DataTable = DBServer.PRES5_Datos_Partida(idioma, PRES0, PRES1, PRES2, PRES3, PRES4, CENTROSM)
        Dim oPartidaPres5 As New PartidaPRES5(mDBServer, mIsAuthenticated)

        If Not dtDatosPartida Is Nothing AndAlso dtDatosPartida.Rows.Count > 0 Then
            dtDatosPartida.TableName = "DATOS_PARTIDA"
            Return dtDatosPartida
        Else
            Return Nothing
        End If
    End Function
    ''' Revisado por: blp. Fecha: 03/11/2011
    ''' <summary>
    ''' Función que devuelve la denominación de una partida dada
    ''' </summary>
    ''' <param name="idioma">idioma en que se quiere recuperar la información</param>
    ''' <param name="PRES0">Código del árbol presupuestario del que se quiere recuperar la partida (PRES5 en otros sitios)</param>
    ''' <param name="PRES1">Código de la partida de nivel 1</param>
    ''' <param name="PRES2">Código de la partida de nivel 2 (si la hay)</param>
    ''' <param name="PRES3">Código de la partida de nivel 3 (si la hay)</param>
    ''' <param name="PRES4">Código de la partida de nivel 4 (si la hay)</param>    
    ''' <param name="soloDEN">
    ''' Si es TRUE, devolvemos sólo la denominación
    ''' Si es FALSE, devolvemos Codigo - Denominación
    ''' </param>
    ''' <returns>string con la denominación</returns>
    ''' <remarks>Llamada desde SMWeb\HistoricoCambios.asmx.vb -> CargarHistorico. Tiempo máximo inferior a 1 seg.</remarks>
    ''' 
    Public Function DenominacionPartidaPRES5(ByVal idioma As FSNLibrary.Idioma, ByVal PRES0 As String, ByVal PRES1 As String, ByVal PRES2 As String, ByVal PRES3 As String, ByVal PRES4 As String, Optional ByVal soloDEN As Boolean = False) As String
        Authenticate()

        Dim dsDenPartida As DataSet = DBServer.PRES5_Denominacion_Partida(idioma, PRES0, PRES1, PRES2, PRES3, PRES4)
        If Not dsDenPartida Is Nothing AndAlso dsDenPartida.Tables.Count > 0 And Not dsDenPartida.Tables(0) Is Nothing AndAlso dsDenPartida.Tables(0).Rows.Count > 0 Then
            If soloDEN Then
                Return dsDenPartida.Tables(0).Rows(0)("DEN")
            Else
                Return dsDenPartida.Tables(0).Rows(0)("COD") & " - " & dsDenPartida.Tables(0).Rows(0)("DEN")
            End If
        Else
            Return String.Empty
        End If
    End Function
	''' <summary>
	''' Función que devuelve el histórico de una partida dada
	''' </summary>       
	''' <param name="PRES0">Código del árbol presupuestario del que se quiere recuperar la partida (PRES5 en otros sitios)</param>
	''' <param name="PRES1">Código de la partida de nivel 1</param>
	''' <param name="PRES2">Código de la partida de nivel 2 (si la hay)</param>
	''' <param name="PRES3">Código de la partida de nivel 3 (si la hay)</param>
	''' <param name="PRES4">Código de la partida de nivel 4 (si la hay)</param>    
	''' <returns>objeto Datatable con la información</returns>
	''' <remarks>Llamada desde SMWeb\HistoricoCambios.asmx.vb -> CargarHistorico. Tiempo máximo inferior a 1 seg.</remarks>
	Public Function HistoricoPartida(ByVal PRES0 As String, ByVal PRES1 As String, ByVal PRES2 As String, ByVal PRES3 As String, ByVal PRES4 As String, ByVal Anyo As Integer) As DataTable
		Authenticate()

		Dim ds As DataSet
		If Anyo = 0 Then
			ds = DBServer.PRES5_HistoricoPartida(PRES0, PRES1, PRES2, PRES3, PRES4)
		Else
			ds = DBServer.PRES5_HistoricoPartida_Anyo(PRES0, PRES1, PRES2, PRES3, PRES4, Anyo)
		End If

		If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
			Return ds.Tables(0)
		Else
			Return Nothing
		End If
	End Function
	''' <summary>
	''' Función que devuelve el histórico de una partida dada
	''' </summary>       
	''' <param name="Usu">Usuario</param>
	''' <param name="PRES0">Código del árbol presupuestario del que se quiere recuperar la partida (PRES5 en otros sitios)</param>
	''' <param name="PRES1">Código de la partida de nivel 1</param>
	''' <param name="PRES2">Código de la partida de nivel 2 (si la hay)</param>
	''' <param name="PRES3">Código de la partida de nivel 3 (si la hay)</param>
	''' <param name="PRES4">Código de la partida de nivel 4 (si la hay)</param>    
	''' <returns>objeto Datatable con la información</returns>
	''' <remarks>Llamada desde SMWeb. Tiempo máximo inferior a 1 seg.</remarks>
	Public Function PermisosUsuCCPartida(ByVal Usu As String, ByVal PRES0 As String, ByVal PRES1 As String, ByVal PRES2 As String, ByVal PRES3 As String, ByVal PRES4 As String) As DataTable
        Authenticate()

        Dim dsPermisos As DataSet = DBServer.PRES5_PermisosUsuCCPartida(Usu, PRES0, PRES1, PRES2, PRES3, PRES4)
        If Not dsPermisos Is Nothing AndAlso dsPermisos.Tables.Count > 0 Then
            Return (dsPermisos.Tables(0))
        Else
            Return Nothing
        End If
    End Function
    ''' <summary>
    ''' Función que actualiza el comentario de un registro de histórico de una partida
    ''' </summary>       
    ''' <param name="ID">Id del histórico</param>
    ''' <param name="Coment">Nuevo comentario</param>
    ''' <param name="Usu_Coment">Usuario que hace el comentario</param>           
    ''' <remarks>Llamada desde SMWeb. Tiempo máximo inferior a 1 seg.</remarks>
    Public Sub ActualizarComentHistorico(ByVal ID As Integer, ByVal Coment As String, ByVal Usu_Coment As String)
        Authenticate()

        DBServer.PRES5_ActualizarComentHistorico(ID, Coment, Usu_Coment)
    End Sub
    ''' <summary>
    ''' Obtiene los datos de denominación, código y presupuestación de una partida presupuestaria
    ''' </summary>
    ''' <param name="PRES0">Código PRES0 de la partida presupuestaria</param>
    ''' <param name="PRES1">Código PRES1 de la partida presupuestaria</param>
    ''' <param name="PRES2">Código PRES2 de la partida presupuestaria</param>
    ''' <param name="PRES3">Código PRES3 de la partida presupuestaria</param>
    ''' <param name="PRES4">Código PRES4 de la partida presupuestaria</param>
    ''' <param name="ObtenerMonedas">Si se obtiene un listado con las monedas o no para rellenar el combo de monedas</param>
    ''' <returns>Dataset con denominaciones, datos y presupuestación. Si es necesario otra tabla con las monedas</returns>
    ''' <remarks></remarks>
    Public Function DevolverDatosPartidaPRES5(ByVal PRES0 As String, ByVal PRES1 As String, ByVal PRES2 As String, _
                                            ByVal PRES3 As String, ByVal PRES4 As String, _
                                            ByVal ObtenerMonedas As Boolean) As DataSet
        Authenticate()
        Return DBServer.Partida_PRES5_DevolverDatosPartidaPRES5(PRES0, PRES1, PRES2, PRES3, PRES4, ObtenerMonedas)
    End Function
    ''' <summary>
    ''' Obtiene todas las posibles partidas para un centro de coste que se cargarán para el Autocomplete
    ''' </summary>
    ''' <param name="sUON1">Código UON1 del centro de coste</param>
    ''' <param name="sUON2">Código UON2 del centro de coste</param>
    ''' <param name="sUON3">Código UON3 del centro de coste</param>
    ''' <param name="sUON4">Código UON4 del centro de coste</param>
    ''' <param name="sUsu">Código de usuario para comprobar permisos</param>
    ''' <param name="sIdioma">Idioma en que se mostrarán las partidas</param>
    ''' <param name="bBajaLogica">Si se tienen en cuenta las que estan de baja logica</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetPartidasPresupuestarias(ByVal sUON1 As String, ByVal sUON2 As String, _
                                               ByVal sUON3 As String, ByVal sUON4 As String, _
                                               ByVal sUsu As String, ByVal sIdioma As String, _
                                               ByVal bBajaLogica As Boolean) As DataTable
        Authenticate()
        Return DBServer.Partida_PRES5_GetPartidasPresupuestarias(sUON1, sUON2, sUON3, sUON4, sUsu, sIdioma, bBajaLogica)
    End Function
    ''' <summary>
    ''' Agrega una nueva partida presupuestaria
    ''' </summary>
    ''' <param name="UON1">Código UON1 del centro de coste donde se añade la partida presupuestaria</param>
    ''' <param name="UON2">Código UON2 del centro de coste donde se añade la partida presupuestaria</param>
    ''' <param name="UON3">Código UON3 del centro de coste donde se añade la partida presupuestaria</param>
    ''' <param name="UON4">Código UON4 del centro de coste donde se añade la partida presupuestaria</param>
    ''' <param name="PRES0">Código PRES0 de la partida presupuestaria</param>
    ''' <param name="PRES1">Código PRES1 de la partida presupuestaria</param>
    ''' <param name="PRES2">Código PRES2 de la partida presupuestaria</param>
    ''' <param name="PRES3">Código PRES3 de la partida presupuestaria</param>
    ''' <param name="PRES4">Código PRES4 de la partida presupuestaria</param>
    ''' <param name="dtDenominaciones">Tabla de las denominaciones agregadas</param>
    ''' <param name="FecIni">Fecha de inicio</param>
    ''' <param name="FecFin">Fecha de fin</param>
    ''' <param name="Mon">Moneda de la partida</param>
    ''' <param name="Pres">Importe presupuestado</param>
    ''' <param name="Usu">Usuario que añade la partida</param>
    ''' <remarks></remarks>
    Public Function AgregarPartidaPresupuestaria(ByVal UON1 As String, ByVal UON2 As String, ByVal UON3 As String, ByVal UON4 As String, _
                                            ByVal PRES0 As String, ByVal PRES1 As String, ByVal PRES2 As String, _
                                            ByVal PRES3 As String, ByVal PRES4 As String, ByVal dtDenominaciones As DataTable, _
                                            ByVal FecIni As Nullable(Of DateTime), ByVal FecFin As Nullable(Of DateTime), _
                                            ByVal Mon As String, ByVal Pres As Nullable(Of Double), ByVal Usu As String) As Integer
        Authenticate()
        Return DBServer.AgregarPartidaPresupuestaria(UON1, UON2, UON3, UON4, PRES0, PRES1, PRES2, PRES3, PRES4, dtDenominaciones, FecIni, FecFin, Mon, Pres, Usu)
    End Function
    ''' <summary>
    ''' Modifica los datos de una partida presupuestaria ya existente
    ''' </summary>
    ''' <param name="UON1">Código UON1 del centro de coste donde se modifica la partida presupuestaria</param>
    ''' <param name="UON2">Código UON2 del centro de coste donde se modifica la partida presupuestaria</param>
    ''' <param name="UON3">Código UON3 del centro de coste donde se modifica la partida presupuestaria</param>
    ''' <param name="UON4">Código UON4 del centro de coste donde se modifica la partida presupuestaria</param>
    ''' <param name="PRES0">Código PRES0 de la partida presupuestaria</param>
    ''' <param name="PRES1">Código PRES1 de la partida presupuestaria</param>
    ''' <param name="PRES2">Código PRES2 de la partida presupuestaria</param>
    ''' <param name="PRES3">Código PRES3 de la partida presupuestaria</param>
    ''' <param name="PRES4">Código PRES4 de la partida presupuestaria</param>
    ''' <param name="dtDenominaciones">Tabla de las denominaciones modificadas</param>
    ''' <param name="FecIni">Fecha de inicio</param>
    ''' <param name="FecFin">Fecha de fin</param>
    ''' <param name="Mon">Moneda de la partida</param>
    ''' <param name="Pres">Importe presupuestado</param>
    ''' <param name="Observaciones">Observaciones a la modificación</param> 
    ''' <param name="Usu">Usuario que modifica la partida</param>
    ''' <remarks></remarks>
    Public Sub ModificarPartidaPresupuestaria(ByVal UON1 As String, ByVal UON2 As String, ByVal UON3 As String, ByVal UON4 As String, _
                                            ByVal PRES0 As String, ByVal PRES1 As String, ByVal PRES2 As String, _
                                            ByVal PRES3 As String, ByVal PRES4 As String, ByVal BajaLogica As Boolean, _
                                            ByVal dtDenominaciones As DataTable, _
                                            ByVal FecIni As Nullable(Of DateTime), ByVal FecFin As Nullable(Of DateTime), _
                                            ByVal Mon As String, ByVal Pres As Nullable(Of Double), ByVal Observaciones As String, ByVal Usu As String)
        Authenticate()
        DBServer.ModificarPartidaPresupuestaria(UON1, UON2, UON3, UON4, PRES0, PRES1, PRES2, PRES3, PRES4, BajaLogica, dtDenominaciones, FecIni, FecFin, Mon, Pres, Observaciones, Usu)
    End Sub
    ''' <summary>
    ''' Obtenemos de base de datos la longitud máxima permitida para los códigos de las partidas presupuestarias
    ''' </summary>
    ''' <returns>Datatable con las longitudes</returns>
    ''' <remarks></remarks>
    Public Function ObtenerLongitudesCodigos() As DataTable
        Authenticate()
        Return DBServer.ObtenerLongitudesCodigos()
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="UON1">Código UON1 del centro de coste donde se modifica la partida presupuestaria</param>
    ''' <param name="UON2">Código UON2 del centro de coste donde se modifica la partida presupuestaria</param>
    ''' <param name="UON3">Código UON3 del centro de coste donde se modifica la partida presupuestaria</param>
    ''' <param name="UON4">Código UON4 del centro de coste donde se modifica la partida presupuestaria</param>
    ''' <param name="PRES0">Código PRES0 de la partida presupuestaria</param>
    ''' <param name="PRES1">Código PRES1 de la partida presupuestaria</param>
    ''' <param name="PRES2">Código PRES2 de la partida presupuestaria</param>
    ''' <param name="PRES3">Código PRES3 de la partida presupuestaria</param>
    ''' <param name="PRES4">Código PRES4 de la partida presupuestaria</param>
    ''' <param name="Abierto">Si se puede modificar la partida presupuestaria externa(1) o no (0)</param>
    ''' <param name="Usu">Usuario que modifica la partida</param>
    ''' <param name="Idioma">Idioma en el que se muestran los datos</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ModificarControlCambiosPartidaPresupuestaria(ByVal UON1 As String, ByVal UON2 As String, ByVal UON3 As String, ByVal UON4 As String, _
                                            ByVal PRES0 As String, ByVal PRES1 As String, ByVal PRES2 As String, _
                                            ByVal PRES3 As String, ByVal PRES4 As String, ByVal Abierto As Integer, _
                                            ByVal Usu As String, ByVal Idioma As String) As DataSet

        Authenticate()
        Return DBServer.ModificarControlCambiosPartidaPresupuestaria(UON1, UON2, UON3, UON4, PRES0, PRES1, PRES2, PRES3, PRES4, Abierto, Usu, Idioma)
    End Function
    ''' Revisado por: blp. Fecha: 27/06/2012
    ''' <summary>
    ''' Recuperar los gestores de las partidas a las que puede imputar el usuario
    ''' </summary>
    ''' <param name="Usu">Usuario que modifica la partida</param>
    ''' <param name="PRES0">Código PRES0 de la partida presupuestaria</param>
    ''' <remarks>Llamada desde EmisionPedido.aspx.vb. Máx inferior 1 seg</remarks>
    Public Sub ObtGestoresPartidasUsu(ByVal Usu As String, ByVal PRES0 As String)
        Authenticate()
        Dim dt As DataTable = DBServer.PRES5_ObtGestoresPartidasUsu(Usu, PRES0)
        If dt.Rows.Count > 0 Then
            For Each rowPartida As DataRow In dt.Rows
                Dim oPartidaPres5 As New PartidaPRES5(mDBServer, mIsAuthenticated)
                oPartidaPres5.NIV0 = rowPartida("PRES0")
                oPartidaPres5.NIV1 = DBNullToStr(rowPartida("PRES1"))
                oPartidaPres5.NIV2 = DBNullToStr(rowPartida("PRES2"))
                oPartidaPres5.NIV3 = DBNullToStr(rowPartida("PRES3"))
                oPartidaPres5.NIV4 = DBNullToStr(rowPartida("PRES4"))
                oPartidaPres5.GestorCod = DBNullToStr(rowPartida("gestorCod"))
                oPartidaPres5.GestorDen = DBNullToStr(rowPartida("gestorDen"))
                oPartidaPres5.FechaInicioPresupuesto = DBNullToSomething(rowPartida("FECINI"))
                oPartidaPres5.FechaFinPresupuesto = DBNullToSomething(rowPartida("FECFIN"))
                Me.Add(oPartidaPres5)
            Next
        End If
    End Sub
    Public Sub NotificarDisponibleNegativo()
        Authenticate()
        Dim dSUsuariosNotifControlDisponible As DataSet = DBServer.PartidasPRES5_UsuariosNotifControlDisponible()
        Dim dtUsuariosNotifControlDisponible As DataTable = dSUsuariosNotifControlDisponible.Tables(0)
        If dtUsuariosNotifControlDisponible.Rows.Count > 0 Then
            Dim oNotificador As New Notificar(DBServer, mIsAuthenticated)
            For Each rowUsuario As DataRow In dtUsuariosNotifControlDisponible.Rows
                Dim dtPartidasDisponibleNegativo As DataTable = DBServer.PartidasPRES5_NotificarDisponibleNegativo(DBNullToStr(rowUsuario("COD")), DBNullToStr(rowUsuario("IDIOMA")))
                If dtPartidasDisponibleNegativo.Rows.Count > 0 Then
                    oNotificador.NotificacionDisponibleNegativo(dtPartidasDisponibleNegativo, rowUsuario,
                                                                dSUsuariosNotifControlDisponible.Tables(1).Rows(0)("TEXT_" & DBNullToStr(rowUsuario("IDIOMA"))).ToString,
                                                                dSUsuariosNotifControlDisponible.Tables(2).Rows(0)("CUENTAMAIL").ToString)
                End If
            Next
            oNotificador.FuerzaFinalizeSmtpClient()
            oNotificador = Nothing
        End If
    End Sub
	Public Function Obtener_Partidas_Presupuestarias(ByVal CodUsuario As String, ByVal Idioma As String, ByVal CodCentroCoste As String, ByVal PRES0 As String,
													 ByVal EsAutocompletar As Boolean, ByVal filtroCodigo As String, ByVal filtroDenominacion As String) As Object
		Authenticate()
		Return DBServer.Obtener_Partidas_Presupuestarias(CodUsuario, Idioma, CodCentroCoste, PRES0, EsAutocompletar, filtroCodigo, filtroDenominacion) _
				.Rows.OfType(Of DataRow).Select(Function(x) New With {.Codigo = x("CODIGO"), .Denominacion = x("DENOMINACION")}).ToList()
	End Function
	Public Function AgregarAnyoPartidaPresupuestaria(ByVal IdPartida As Integer, ByVal PRES0 As String, ByVal PRES1 As String, ByVal PRES2 As String,
											ByVal PRES3 As String, ByVal PRES4 As String, ByVal Anyo As Integer, ByVal Pres As Nullable(Of Double), ByVal Usu As String) As Integer
		Authenticate()
		Return DBServer.AgregarAnyoPartidaPresupuestaria(IdPartida, PRES0, PRES1, PRES2, PRES3, PRES4, Anyo, Pres, Usu)
	End Function
	Public Function ModificarAnyoPartidaPresupuestaria(ByVal IdPartida As Integer, ByVal PRES0 As String, ByVal PRES1 As String, ByVal PRES2 As String,
											ByVal PRES3 As String, ByVal PRES4 As String, ByVal Anyo As Integer, ByVal Pres As Nullable(Of Double),
											ByVal Observaciones As String, ByVal Usu As String) As Integer
		Authenticate()
		Return DBServer.ModificarAnyoPartidaPresupuestaria(IdPartida, PRES0, PRES1, PRES2, PRES3, PRES4, Anyo, Pres, Observaciones, Usu)
	End Function
	Public Function DevolverDatosPartidaPRES5Anyo(ByVal IdPartida As Integer, ByVal Anyo As Integer) As DataRow
		Authenticate()
		Dim dt As DataTable = DBServer.Partida_PRES5_DevolverDatosPartidaPRES5Anyo(IdPartida, Anyo)
		If dt.Rows.Count = 1 Then
			Return dt.Rows(0)
		End If
	End Function
End Class