﻿<Serializable()> _
Public Class Imputacion
    Inherits Security

    Private _Id As Integer
    Private _lineaId As Integer
    Private _CentroCoste As UON
    Private _Partida As PartidaPRES5
    Public Property Id() As Integer
        Get
            Return _Id
        End Get
        Set(ByVal value As Integer)
            _Id = value
        End Set
    End Property
    Public Property LineaId() As Integer
        Get
            Return _lineaId
        End Get
        Set(ByVal value As Integer)
            _lineaId = value
        End Set
    End Property
    Public Property CentroCoste() As UON
        Get
            Return _CentroCoste
        End Get
        Set(ByVal value As UON)
            _CentroCoste = value
        End Set
    End Property
    Public Property Partida() As PartidaPRES5
        Get
            Return _Partida
        End Get
        Set(ByVal value As PartidaPRES5)
            _Partida = value
        End Set
    End Property
#Region " Constructor"
    ''' <summary>
    ''' Constructor de la clase Imputacion
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si está autenticado</param>
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
        Partida = New PartidaPRES5(dbserver, mIsAuthenticated)
    End Sub
    ''' <summary>
    ''' Constructor de la clase Imputacion
    ''' </summary>
    Public Sub New()
        Partida = New PartidaPRES5(DBServer, mIsAuthenticated)
    End Sub
#End Region
End Class
