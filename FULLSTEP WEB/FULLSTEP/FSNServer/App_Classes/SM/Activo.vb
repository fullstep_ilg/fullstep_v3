﻿<Serializable()> _
Public Class Activo
    Inherits Security

#Region " Constructor"
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
#End Region

    Private intID As Integer
    Private strCod As String
    Private strDen As String
    Private strCod_ERP As String
    Private strEmp As String
    Private strCentro As String
    Private mdsData As DataSet

    Public Property ID() As Integer
        Get
            Return intID
        End Get
        Set(ByVal value As Integer)
            intID = value
        End Set
    End Property

    Public Property Codigo() As String
        Get
            Return strCod
        End Get
        Set(ByVal value As String)
            strCod = value
        End Set
    End Property

    Public Property Denominacion() As String
        Get
            Return strDen
        End Get
        Set(ByVal value As String)
            strDen = value
        End Set
    End Property

    Public Property Cod_Erp() As String
        Get
            Return strCod_ERP
        End Get
        Set(ByVal value As String)
            strCod_ERP = value
        End Set
    End Property

    Public Property Empresa() As String
        Get
            Return strEmp
        End Get
        Set(ByVal value As String)
            strEmp = value
        End Set
    End Property

    Public Property Centro_SM() As String
        Get
            Return strCentro
        End Get
        Set(ByVal value As String)
            strCentro = value
        End Set
    End Property

    Public ReadOnly Property Data() As Data.DataSet
        Get
            Return mdsData
        End Get
    End Property

    ''' <summary>
    ''' Devuelve el activo para el usuario y centro de coste especificados
    ''' </summary>
    ''' <param name="sIdioma">Idioma del usuario</param>
    ''' <param name="sUsuario">Identificador del usuario</param>
    ''' <param name="sCodActivo">Código del activo que se quiere buscar</param>
    ''' <param name="sCentroCoste">Código del centro de coste</param>
    ''' <returns>Dataset con información del activo (código, descripcioón, código y descripción del centro, etc)</returns>
    ''' <remarks></remarks>
    Public Function BuscarActivo(ByVal sIdioma As String, ByVal sUsuario As String, ByVal sCodActivo As String, Optional ByVal sCentroCoste As String = Nothing) As DataSet
        Authenticate()
        mdsData = DBServer.Activo_Buscar(sIdioma, sUsuario, sCodActivo, sCentroCoste)
        Return mdsData
    End Function

    ''' <summary>
    ''' Devuelve el activo
    ''' </summary>
    ''' <param name="sIdioma">Idioma del usuario</param>
    ''' <param name="sCodActivo">Código del activo que se quiere buscar</param>
    ''' <returns>Dataset con información del activo (código, descripcioón, código y descripción del centro, etc)</returns>
    ''' <remarks></remarks>
    Public Function DetalleActivo(ByVal sIdioma As String, ByVal sCodActivo As String) As DataSet
        Authenticate()
        mdsData = DBServer.Activo_Detalle(sIdioma, sCodActivo)
        Return mdsData
    End Function

End Class
