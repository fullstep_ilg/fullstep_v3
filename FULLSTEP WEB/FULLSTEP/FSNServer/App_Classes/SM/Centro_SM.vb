﻿Imports System.Web

<Serializable()> _
Public Class Centro_SM
    Inherits Security

#Region " Constructor"
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
    Public Sub New()
    End Sub
#End Region

    Private strCod As String
    Private strDen As String
    Private oUONS As UON
    Private m_iEmpresa As Nullable(Of Integer)
    Private _PartidasPresupuestarias As PartidasPRES5
    Private _PartidasPresupuestarias_EP As List(Of PartidaPRES5)
    Private oEmpresas As SMEmpresas

    Public Property Codigo() As String
        Get
            Return strCod
        End Get
        Set(ByVal value As String)
            strCod = value
        End Set
    End Property
    Public Property Denominacion() As String
        Get
            Return strDen
        End Get
        Set(ByVal value As String)
            strDen = value
        End Set
    End Property

    Public Property UONS() As UON
        Get
            Return oUONS
        End Get
        Set(ByVal value As UON)
            oUONS = value
        End Set
    End Property
    Public Property Empresa() As Nullable(Of Integer)
        Get
            Return m_iEmpresa
        End Get
        Set(ByVal value As Nullable(Of Integer))
            m_iEmpresa = value
        End Set
    End Property
    Public Property PartidasPresupuestarias() As PartidasPRES5
        Get
            Return _PartidasPresupuestarias
        End Get
        Set(ByVal value As PartidasPRES5)
            _PartidasPresupuestarias = value
        End Set
    End Property
    Public Property PartidasPresupuestarias_EP() As List(Of PartidaPRES5)
        Get
            Return _PartidasPresupuestarias_EP
        End Get
        Set(ByVal value As List(Of PartidaPRES5))
            _PartidasPresupuestarias_EP = value
        End Set
    End Property
    Public Property Empresas() As SMEmpresas
        Get
            Return oEmpresas
        End Get
        Set(ByVal value As SMEmpresas)
            oEmpresas = value
        End Set
    End Property
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sUON"></param>s
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Load(ByVal Idioma As String, ByVal sUON As String) As Centro_SM
        If Not String.IsNullOrEmpty(sUON) Then
            Dim info As DataTable = DBServer.Centro_SM_Info(Idioma, sUON)
            If Not info Is Nothing AndAlso info.Rows.Count > 0 Then
                strCod = info.Rows(0)("COD")
                strDen = info.Rows(0)("DEN")

                oUONS = New UON(Me.DBServer, mIsAuthenticated)
                If Not oUONS Is Nothing Then
                    Dim aUON() As String = sUON.Split(New Char() {"#"})
                    If aUON.Length > 0 Then
                        If aUON.Length = 1 Then
                            oUONS.UON1 = aUON(0)
                        ElseIf aUON.Length = 2 Then
                            oUONS.UON1 = aUON(0)
                            oUONS.UON2 = aUON(1)
                        ElseIf aUON.Length = 3 Then
                            oUONS.UON1 = aUON(0)
                            oUONS.UON2 = aUON(1)
                            oUONS.UON3 = aUON(2)
                        ElseIf aUON.Length = 4 Then
                            oUONS.UON1 = aUON(0)
                            oUONS.UON2 = aUON(1)
                            oUONS.UON3 = aUON(2)
                            oUONS.UON4 = aUON(3)
                        End If
                    End If
                End If
                m_iEmpresa = DBNullToSomething(info.Rows(0)("EMPRESA"))
            End If
            Return Me
        Else
            Return Nothing
        End If
    End Function

    ''' <summary>
    ''' Obtenemos la información de si el usuario tiene permiso unicamente a un centro de coste o varios
    ''' </summary>
    ''' <param name="sUsu">Código del usuario que quiere obtener las partidas presupuestarias</param>
    ''' <param name="sIdioma">Idioma de los centros de coste</param>
    ''' <remarks></remarks>
    Public Sub DevolverCCUnico(ByVal sUsu As String, ByVal sIdioma As String)
        Dim Resultado(2) As String

        Resultado = DBServer.Centro_SM_DevolverCCUnico(sUsu, sIdioma)

        If Resultado Is Nothing Then Exit Sub

        strCod = Resultado(0)
        strDen = Resultado(1)
        If Resultado(2) <> "" Then
            oUONS = New UON(Me.DBServer, mIsAuthenticated)
            If Not oUONS Is Nothing Then
                Dim aUON() As String = Resultado(2).Split(New Char() {"#"})
                If aUON.Length > 0 Then
                    If aUON.Length = 1 Then
                        oUONS.UON1 = aUON(0)
                    ElseIf aUON.Length = 2 Then
                        oUONS.UON1 = aUON(0)
                        oUONS.UON2 = aUON(1)
                    ElseIf aUON.Length = 3 Then
                        oUONS.UON1 = aUON(0)
                        oUONS.UON2 = aUON(1)
                        oUONS.UON3 = aUON(2)
                    ElseIf aUON.Length = 4 Then
                        oUONS.UON1 = aUON(0)
                        oUONS.UON2 = aUON(1)
                        oUONS.UON3 = aUON(2)
                        oUONS.UON4 = aUON(3)
                    End If
                End If
            End If
        End If
    End Sub
    ''' <summary>
    ''' Obtiene los permisos del usuario (alta, modificación y presupuestación) sobre el centro de coste seleccionado
    ''' </summary>
    ''' <param name="sUON1">Código UON1 del centro de coste</param>
    ''' <param name="sUON2">Código UON1 del centro de coste</param>
    ''' <param name="sUON3">Código UON1 del centro de coste</param>
    ''' <param name="sUON4">Código UON1 del centro de coste</param>
    ''' <param name="sUsu">Código del usuario</param>
    ''' <returns>Devuelve un datarow con los permisos</returns>
    ''' <remarks></remarks>
    Public Function DevolverPermisosCentroCoste(ByVal sUON1 As String, ByVal sUON2 As String, ByVal sUON3 As String, ByVal sUON4 As String, ByVal sUsu As String) As DataRow
        Authenticate()
        DevolverPermisosCentroCoste = DBServer.Centro_SM_DevolverPermisos(sUON1, sUON2, sUON3, sUON4, sUsu)
    End Function
    ''' Revisado por: blp. Fecha: 29/01/2013
    ''' <summary>
    ''' Devuelve los centros presentes en los pedidos que un usuario puede consultar en la pantalla de seguimiento de EP
    ''' </summary>
    ''' <param name="codPer">Código del usuario</param>
    ''' <param name="idioma">Idioma</param>
    ''' <param name="VerPedidosOtrosUsuarios">Permiso del usuario para ver pedidos de otros usuarios</param>
    ''' <returns>Dataset con los centros</returns>
    ''' <remarks>Llamada desde Consultas.asmx.vb->DevolverCentrosPedidos. Máximo 1,5 seg.</remarks>
    Public Function DevolverCentrosPedidos(ByVal codPer As String, ByVal idioma As FSNLibrary.Idioma, ByVal VerPedidosOtrosUsuarios As Boolean) As DataSet
        Authenticate()
        Return DBServer.Centros_DevolverCentrosPedidos(codPer, idioma, VerPedidosOtrosUsuarios)
    End Function
    ''' Revisado por: blp. Fecha: 20/08/2013
    ''' <summary>
    ''' Devuelve todos los centros con un formato que nos permite usarlos en el visor de contratos
    ''' </summary>
    ''' <param name="idioma">Idioma</param>
    ''' <returns>Dataset con los centros</returns>
    ''' <remarks>Llamada desde Consultas.asmx.vb. Máximo 1,5 seg.</remarks>
    Public Function DevolverCentrosPM(ByVal idioma As FSNLibrary.Idioma) As DataSet
        Authenticate()
        Return DBServer.Centros_DevolverCentrosPM(idioma)
    End Function
    ''' Revisado por: blp. Fecha: 28/08/2013
    ''' <summary>
    ''' Devuelve los datos del centro solicitado
    ''' </summary>
    ''' <param name="CentroCoste">CÓDIGO DE UONS DEL CENTRO DE COSTE: UON1#UON2#UON3#UON4</param>
    ''' <param name="idioma">Idioma</param>
    ''' <returns>Datatable con los datos del centro</returns>
    ''' <remarks>Llamada desde Consultas.asmx.vb. Máximo 1,5 seg.</remarks>
    Public Function DevolverCentroPM(ByVal CentroCoste As String, ByVal idioma As FSNLibrary.Idioma) As DataTable
        Authenticate()
        Return DBServer.Centros_DevolverCentroPM(CentroCoste, idioma)
    End Function
End Class
