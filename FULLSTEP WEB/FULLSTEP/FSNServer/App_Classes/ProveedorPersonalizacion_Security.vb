﻿Public Class ProveedorPersonalizacion_Security
    Inherits Security

    ''' <summary>
    ''' Carga datos de personalización sin formato del almacén de datos subyacente.
    ''' </summary>
    ''' <param name="mApplicationName">mApplicationName</param>
    ''' <param name="path">La ruta de acceso para información de personalización que se va a utilizar como clave de recuperación.</param>
    ''' <param name="userName">El nombre de usuario para información de personalización que se va a utilizar como clave de recuperación.</param>
    ''' <remarks></remarks>
    Public Function LoadPersonalizationBlobs(ByVal mApplicationName As String, ByVal path As String, ByVal userName As String) As Byte()
        Return DBServer.Personalizacion_GetUserData(mApplicationName, path, userName)
    End Function

    ''' <summary>
    ''' Elimina datos de personalización sin formato del almacén de datos subyacente.
    ''' </summary>
    ''' <param name="mApplicationName">mApplicationName</param>
    ''' <param name="path">La ruta de acceso para información de personalización que se va a utilizar como clave de almacenamiento de datos.</param>
    ''' <param name="userName">El nombre de usuario para la información de personalización que se va a utilizar como clave de almacenamiento de datos.</param>
    ''' <remarks></remarks>
    Public Sub ResetPersonalizationBlob(ByVal mApplicationName As String, ByVal path As String, ByVal userName As String)
        DBServer.Personalizacion_DelUserData(mApplicationName, path, userName)
    End Sub

    ''' <summary>
    ''' Guarda datos de personalización sin formato en el almacén de datos subyacente.
    ''' </summary>
    ''' <param name="mApplicationName">mApplicationName</param>
    ''' <param name="path">La ruta de acceso para información de personalización que se va a utilizar como clave de almacenamiento de datos.</param>
    ''' <param name="userName">El nombre de usuario para información de personalización que se va a utilizar como clave.</param>
    ''' <param name="dataBlob">La matriz de bytes de datos que se va a guardar.</param>
    ''' <remarks></remarks>
    Public Sub SavePersonalizationBlob(ByVal mApplicationName As String, ByVal path As String, ByVal userName As String, ByVal dataBlob() As Byte)
        DBServer.Personalizacion_SetUserData(mApplicationName, path, userName, dataBlob)
    End Sub

    ''' <summary>
    ''' Devuelve el número de filas del almacén de datos subyacente que existen dentro del ámbito especificado.
    ''' </summary>
    ''' <param name="mApplicationName">mApplicationName</param>
    ''' <param name="query">Una clase PersonalizationStateQuery que contiene una consulta. Este valor puede ser nullNothingnullptrreferencia null (Nothing en Visual Basic).</param>
    ''' <returns>El número de filas del almacén de datos subyacente que existen para el parámetro scope especificado.</returns>
    ''' <remarks></remarks>
    Public Function GetCountOfState(ByVal mApplicationName As String, ByVal query As System.Web.UI.WebControls.WebParts.PersonalizationStateQuery) As Integer
        Return DBServer.Personalizacion_NumRegs(mApplicationName, query.PathToMatch, query.UserInactiveSinceDate, query.UsernameToMatch)
    End Function

    ''' <summary>
    ''' Procedimiento que instancia un nuevo objeto de la clase security
    ''' </summary>
    ''' <param name="dbserver">Servidor</param>
    ''' <param name="isAuthenticated">Si esta autenticado</param>
    ''' <remarks>
    ''' Llamada dessde: Todas las llamadas donde se instancie la clase
    ''' Tiempo maximo: 0 seg</remarks>
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
End Class
