
<Serializable()> _
Public Class TextosGS
    Inherits Security

    Public Sub New(ByRef dbserver As FSNDataBaseServer.Root,ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
    End Sub

    ''' <summary>
    ''' Funcion que carga el texto de mensaje de error utilizado en GS
    ''' </summary>
    ''' <param name="Modulo">Modulo de idioma</param>
    ''' <param name="ID">Id del texto</param>
    ''' <param name="Idioma">Idioma de la aplicaci�n</param>
    ''' <param name="StrError">Error</param>
    ''' <returns>Una variable de tipo string con el error del texto de GS</returns>
    ''' <remarks>
    ''' Llamada desde: PmWeb/guardarInstancia/GenerarDataSetYHAcerComprobaciones
    ''' Tiempo m�ximo: 0,2 seg</remarks>
    Public Function MensajeError(ByVal Modulo As Integer, ByVal ID As Integer, Optional ByVal Idioma As String = "SPA", Optional ByVal StrError As String = "") As String
        Dim sRes As String
        sRes = DBServer.TextoGs_Carga(Modulo, ID, Idioma)

        If InStr(sRes, "@") <> 0 Then
            sRes = Replace(sRes, "@", StrError)
        End If

        MensajeError = sRes
    End Function
End Class
