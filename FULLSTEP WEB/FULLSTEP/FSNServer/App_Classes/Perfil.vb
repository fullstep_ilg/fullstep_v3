﻿Public Class Perfil
    Public Sub New()
        Id = 0
        AccionesPerfil = New Dictionary(Of String, Integer)
        MenuAcciones = New Dictionary(Of String, List(Of cn_fsTreeViewItem))
        AccesosPerfil = New Dictionary(Of String, Boolean)
        Denominaciones = New Dictionary(Of String, String)
        UONS = New List(Of cn_fsTreeViewItem)
        AccionesDependientesCheckCheck = New Dictionary(Of String, List(Of Integer))
        AccionesDependientesCheckUnCheck = New Dictionary(Of String, List(Of Integer))
        AccionesDependientesUnCheckCheck = New Dictionary(Of String, List(Of Integer))
        AccionesDependientesUnCheckUnCheck = New Dictionary(Of String, List(Of Integer))
        QlikUONS = New List(Of cn_fsTreeViewItem)
        QlikGMNS = New List(Of cn_fsTreeViewItem)
        UNQA = New List(Of cn_fsTreeViewItem)
    End Sub
    Private _Id As Integer
    Public Property Id() As Integer
        Get
            Return _Id
        End Get
        Set(ByVal value As Integer)
            _Id = value
        End Set
    End Property
    Private _Codigo As String
    Public Property Codigo() As String
        Get
            Return _Codigo
        End Get
        Set(ByVal value As String)
            _Codigo = value
        End Set
    End Property
    Private _PerfilAdministrador As Boolean
    Public Property PerfilAdministrador() As Boolean
        Get
            Return _PerfilAdministrador
        End Get
        Set(ByVal value As Boolean)
            _PerfilAdministrador = value
        End Set
    End Property
    Private _Denominaciones As Dictionary(Of String, String)
    Public Property Denominaciones() As Dictionary(Of String, String)
        Get
            Return _Denominaciones
        End Get
        Set(ByVal value As Dictionary(Of String, String))
            _Denominaciones = value
        End Set
    End Property
    Private _AccionesPerfil As Dictionary(Of String, Integer)
    Public Property AccionesPerfil() As Dictionary(Of String, Integer)
        Get
            Return _AccionesPerfil
        End Get
        Set(ByVal value As Dictionary(Of String, Integer))
            _AccionesPerfil = value
        End Set
    End Property
    Private _MenuAcciones As Dictionary(Of String, List(Of cn_fsTreeViewItem))
    Public Property MenuAcciones() As Dictionary(Of String, List(Of cn_fsTreeViewItem))
        Get
            Return _MenuAcciones
        End Get
        Set(ByVal value As Dictionary(Of String, List(Of cn_fsTreeViewItem)))
            _MenuAcciones = value
        End Set
    End Property
    Private _AccesosPerfil As Dictionary(Of String, Boolean)
    Public Property AccesosPerfil() As Dictionary(Of String, Boolean)
        Get
            Return _AccesosPerfil
        End Get
        Set(ByVal value As Dictionary(Of String, Boolean))
            _AccesosPerfil = value
        End Set
    End Property
    Private _UNQA As List(Of cn_fsTreeViewItem)
    Public Property UNQA() As List(Of cn_fsTreeViewItem)
        Get
            Return _UNQA
        End Get
        Set(ByVal value As List(Of cn_fsTreeViewItem))
            _UNQA = value
        End Set
    End Property
    Private _UONS As List(Of cn_fsTreeViewItem)
    Public Property UONS() As List(Of cn_fsTreeViewItem)
        Get
            Return _UONS
        End Get
        Set(ByVal value As List(Of cn_fsTreeViewItem))
            _UONS = value
        End Set
    End Property

    Private _IdiomaDefecto As String
    Public Property IdiomaDefecto() As String
        Get
            Return _IdiomaDefecto
        End Get
        Set(ByVal value As String)
            _IdiomaDefecto = value
        End Set
    End Property
    Private _AccionesDependientesCheckCheck As Dictionary(Of String, List(Of Integer))
    Public Property AccionesDependientesCheckCheck() As Dictionary(Of String, List(Of Integer))
        Get
            Return _AccionesDependientesCheckCheck
        End Get
        Set(ByVal value As Dictionary(Of String, List(Of Integer)))
            _AccionesDependientesCheckCheck = value
        End Set
    End Property
    Private _AccionesDependientesCheckUnCheck As Dictionary(Of String, List(Of Integer))
    Public Property AccionesDependientesCheckUnCheck() As Dictionary(Of String, List(Of Integer))
        Get
            Return _AccionesDependientesCheckUnCheck
        End Get
        Set(ByVal value As Dictionary(Of String, List(Of Integer)))
            _AccionesDependientesCheckUnCheck = value
        End Set
    End Property
    Private _AccionesDependientesUnCheckCheck As Dictionary(Of String, List(Of Integer))
    Public Property AccionesDependientesUnCheckCheck() As Dictionary(Of String, List(Of Integer))
        Get
            Return _AccionesDependientesUnCheckCheck
        End Get
        Set(ByVal value As Dictionary(Of String, List(Of Integer)))
            _AccionesDependientesUnCheckCheck = value
        End Set
    End Property
    Private _AccionesDependientesUnCheckUnCheck As Dictionary(Of String, List(Of Integer))
    Public Property AccionesDependientesUnCheckUnCheck() As Dictionary(Of String, List(Of Integer))
        Get
            Return _AccionesDependientesUnCheckUnCheck
        End Get
        Set(ByVal value As Dictionary(Of String, List(Of Integer)))
            _AccionesDependientesUnCheckUnCheck = value
        End Set
    End Property
    Private _QlikUONS As List(Of cn_fsTreeViewItem)
    Public Property QlikUONS() As List(Of cn_fsTreeViewItem)
        Get
            Return _QlikUONS
        End Get
        Set(ByVal value As List(Of cn_fsTreeViewItem))
            _QlikUONS = value
        End Set
    End Property
    Private _QlikGMNS As List(Of cn_fsTreeViewItem)
    Public Property QlikGMNS() As List(Of cn_fsTreeViewItem)
        Get
            Return _QlikGMNS
        End Get
        Set(ByVal value As List(Of cn_fsTreeViewItem))
            _QlikGMNS = value
        End Set
    End Property
End Class