﻿Imports Fullstep.FSNServer.Root
Imports System.Security.Principal
Imports System.Threading

<Serializable()> _
Friend Class UserIdentity
    Inherits Security
    Implements IIdentity
    Private mUserProfile As String = ""
    Private mUserActions As FSNDatabaseServer.Root.UserIdentity_Actions
    Private UserCode As String

#Region "Business methods"
    Public ReadOnly Property AuthenticationType() As String Implements System.Security.Principal.IIdentity.AuthenticationType
        Get
            Return "FSWS"
        End Get
    End Property
    Public ReadOnly Property IsAuthenticated() As Boolean Implements System.Security.Principal.IIdentity.IsAuthenticated
        Get
            If Len(UserCode) > 0 Then
                Return True
            Else
                Return False
            End If
        End Get
    End Property
    Public ReadOnly Property Name() As String Implements System.Security.Principal.IIdentity.Name
        Get
            Return UserCode
        End Get
    End Property
    Friend Function HasAction(ByVal Action As String) As Boolean
        Dim i As Integer = 0
        Dim x As Integer
        If mUserActions.ID Is Nothing Then Return False
        x = mUserActions.ID.Length
        While i < x
            If mUserActions.ID(i).ToString.Equals(Action) Then
                Return True
            Else
                i = i + 1
            End If
        End While
        Return False
    End Function
#End Region
#Region " Constructor"
    ''' <summary>
    ''' Procedimiento que instancia una clase UserIdentity
    ''' </summary>
    ''' <param name="dbserver">Servidor</param>
    ''' <param name="UserCode">Codigo de usuario</param>
    ''' <param name="isAuthenticated">Si esta autenticado</param>
    ''' <remarks>
    ''' Llamada desde: FSNServer/User/New
    ''' Tiempo mÃ¡ximo: 0 seg</remarks>
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal UserCode As String, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
        UserCode = UserCode
        mUserActions = dbserver.UserIdentity_GetActions()
    End Sub
#End Region
End Class