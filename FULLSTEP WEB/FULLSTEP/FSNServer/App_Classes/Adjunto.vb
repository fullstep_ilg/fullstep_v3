﻿Imports System.Configuration
Public Class Adjunto
#Region "Properties"
    Private mlId As Object
    Private mlIdEsp As Object

    Private mdtFecha As Date
    Private msNombre As String
    Private msComentario As Object
    Private mlDataSize As Integer
    Private msOP As String
    Private msCod As Object
    Private moPer As CPersona
    Private mDirectorio As String
    Private _filename As String
    Private mTipo As TiposDeDatos.Adjunto.Tipo

    Public Property DirectorioGuardar() As String
        Get
            DirectorioGuardar = mDirectorio
        End Get
        Set(ByVal value As String)
            mDirectorio = value
        End Set
    End Property

    Public Property dataSize() As Integer
        Get
            dataSize = mlDataSize
        End Get
        Set(ByVal Value As Integer)
            mlDataSize = Value
        End Set
    End Property

    Public Property Fecha() As Date
        Get
            Fecha = mdtFecha
        End Get
        Set(ByVal Value As Date)
            mdtFecha = Value
        End Set
    End Property

    Public Property Nombre() As String
        Get
            Nombre = msNombre
        End Get
        Set(ByVal Value As String)
            msNombre = Value
        End Set
    End Property

    Public Property Operacion() As String
        Get
            Operacion = msOP
        End Get
        Set(ByVal Value As String)
            msOP = Value
        End Set
    End Property

    Public Property Comentario() As Object
        Get
            If Not IsDBNull(msComentario) Then
                Comentario = msComentario
            Else
                Comentario = String.Empty
            End If
        End Get
        Set(ByVal Value As Object)
            msComentario = Value
        End Set
    End Property

    ''' <summary>
    ''' Códido del usuario (Tabla USU, campo COD)
    ''' </summary>
    Public Property Cod() As Object
        Get
            Cod = msCod
        End Get
        Set(ByVal Value As Object)
            msCod = Value
        End Set
    End Property

    Public Property Persona() As CPersona
        Get
            Persona = moPer
        End Get
        Set(ByVal Value As CPersona)
            moPer = Value
        End Set
    End Property

    Public Property Id() As Object
        Get
            Id = mlId
        End Get
        Set(ByVal Value As Object)
            mlId = Value
        End Set
    End Property

    ''' <summary>
    ''' Originalmente era únicamente el id de Especificadcion de linea de catalogo al que correspondía el adjunto. Ahora ser usa para indicar el ID del elemento al que corresponde el adjunto, sea cual sea, bien orden de entrega, línea de pedido, cabecera de cesta, linea de cesta, orden favorita, etc.
    ''' </summary>
    Public Property IdEsp() As Object
        Get
            IdEsp = mlIdEsp
        End Get
        Set(ByVal Value As Object)
            mlIdEsp = Value
        End Set
    End Property

    ''' <summary>
    ''' Devuelve o establece el nombre del archivo físico en disco
    ''' </summary>
    ''' <returns>Nombre del archivo</returns>
    Public Property FileName() As String
        Get
            Return _filename
        End Get
        Set(ByVal value As String)
            _filename = value
        End Set
    End Property

    Public Property tipo As TiposDeDatos.Adjunto.Tipo
        Get
            Return mTipo
        End Get
        Set(value As TiposDeDatos.Adjunto.Tipo)
            mTipo = value
        End Set
    End Property
#End Region

#Region "Methods"
    Public Sub New()

    End Sub
    Public Sub New(ByRef DBServer As Fullstep.FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(DBServer, isAuthenticated)
    End Sub
    ''' Revisado por: blp. Fecha: 31/12/2012
    ''' <summary>
    ''' Descarga un archivo adjunto al disco duro del usuario
    ''' </summary>
    ''' <param name="sPath">Ruta completa donde se encuentra el archivo adjunto(Descargado temporalmente antes)</param>
    ''' <param name="sTarget">Nombre del archivo a descargar</param>
    ''' <param name="Tipo">Tipo de adjunto a descargar; 1: Solicitud, 2: Orden entrega, 3: Linea de pedido, 4: Orden Favoritos, 5: Linea de Orden Favorito,
    ''' 6: categoria Nivel 1, 7: categoria Nivel 2, 8: categoria Nivel 3, 9: categoria Nivel 4, 10: categoria Nivel 5
    ''' </param>
    ''' <returns>Un objeto de la clase cTESError donde devolvera el error si es que hay</returns>
    ''' <remarks>
    ''' Llamada desde: Las páginas AnyadiAFavorito, NuevoFavorito, EmisionPedido
    ''' Tiempo máximo: Depende del tamaño del adjunto, pero no mas de 1 seg</remarks>
    Public Function EscribirADisco(ByVal sPath As String, ByRef sTarget As String, ByVal Tipo As TiposDeDatos.Adjunto.Tipo) As CTESError
        Dim i As Integer
        Dim oTESError As New CTESError
        Dim byteBuffer() As Byte
        ReDim byteBuffer(mlDataSize)
        Dim sTargetOriginal As String = sTarget

        oTESError.NumError = 0
        oTESError.Arg1 = sTarget
        i = 0
        Try
            CrearArbol(sPath)
            While IO.File.Exists(sPath & sTarget) = True
                sTarget = Left(sTargetOriginal, Len(sTargetOriginal) - 4) & "[" & i & "]." & Right(sTargetOriginal, 3)
                i = i + 1
            End While
            oTESError.Arg1 = sTarget
            Dim oFileStream As New System.IO.FileStream(sPath & sTarget, IO.FileMode.CreateNew)
            byteBuffer = LeerAdjunto(Id, Tipo)
            oFileStream.Write(byteBuffer, 0, byteBuffer.Length)
            oFileStream.Close()
            Return oTESError
        Catch ex As Exception
            Return oTESError
        End Try
    End Function
    ''' <summary>
    ''' Funcion que devuelve el fichero adjunto
    ''' </summary>
    ''' <param name="Id">Identificador del adjunto</param>
    ''' <param name="Tipo">Tipo de adjunto</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function LeerAdjunto(ByVal Id As Long, ByVal Tipo As TiposDeDatos.Adjunto.Tipo, Optional ByVal ChunkNumber As Long = 0, Optional ByVal ChunkSize As Long = 0, Optional ByVal ID_Rel As String = "") As Byte()
        Authenticate()
        If ID_Rel = Nothing Then
            ID_Rel = ""
        End If
        Return DBServer.Adjunto_LeerAdjunto(Id, Tipo, ChunkNumber, ChunkSize, ID_Rel)

    End Function

    Public Function GrabarAdjunto(ByVal sNom As String, ByVal sPath As String, ByVal Tipo As TiposDeDatos.Adjunto.Tipo, ByVal ID_Rel As String, ByVal sComentario As String, ByVal DataSize As Long) As Long()
        Authenticate()
        If ID_Rel = "" Then
            If Not (mlIdEsp = Nothing) Then
                ID_Rel = mlIdEsp.ToString
            End If
        End If
            If Not IO.Directory.Exists(sPath) Then
            sPath = ConfigurationManager.AppSettings("temp") & "\" & sPath
        End If
        Return DBServer.Adjunto_GrabarAdjunto(sNom, sPath, Tipo, ID_Rel, sComentario, DataSize)
    End Function
    ''' <summary>
    ''' Copia un archivo adjunto en la base de datos
    ''' </summary>
    ''' <param name="Adjunto">Adjunto que vamos a copiar en el proceso de guardado</param>
    ''' <param name="TipoDestino">Tipo de adjunto a insertar en la Base de Datos.</param>
    ''' <returns>Devuelve un integer con el ID del adjunto</returns>
    ''' <remarks></remarks>
    Public Function CopiarAdjunto(ByVal Adjunto As Adjunto, ByVal TipoDestino As TiposDeDatos.Adjunto.Tipo, ByVal ID_Rel As Long, Optional Linea As Long = 0) As Integer
        mlId = DBServer.Adjunto_CopiarAdjunto(Adjunto.Id, TipoDestino, mTipo, ID_Rel, Linea)
        Return mlId
    End Function

    ''' <summary>
    ''' Elimina un archivo adjunto de la Base de Datos
    ''' </summary>
    ''' <param name="Tipo">Tipo de Adjunto a Eliminar; 2: Orden, 3: Linea Pedido, 4: Favorito Orden, 5: Favorito Linea</param>
    ''' <remarks>
    ''' Llamada desde: La clase Página AnyadirAFavorito y NuevoFavorito 
    ''' Tiempo máximo: 0'4 sec.
    ''' </remarks>
    Public Sub EliminarAdjunto(ByVal Tipo As TiposDeDatos.Adjunto.Tipo)
        Dim resul As Integer
        Dim cont As Integer = 0
seguir:
        resul = DBServer.Adjunto_EliminarAdjunto(Id, Tipo)
        cont = cont + 1
        If resul = 0 Then
            Exit Sub
        Else
            If cont < 4 Then GoTo seguir
        End If
    End Sub
    ''' <summary>
    ''' Cambia el comentario del archivo adjunto en la BBDD
    ''' </summary>
    ''' <param name="tipo">Tipo de Adjunto a Cambiar el comentario en la BBDD; 2: Orden, 3: Linea Pedido, 4: Favorito Orden, 5: Favorito Linea</param>
    ''' <param name="NuevoComentario">El nuevo comentario a introducir(puede estar vacío, en ese caso cogeriamos el que tenga el objeto)</param>
    ''' <returns>Un entero que sera 0 si todo ha ido bien, y 1 en caso contrario</returns>
    ''' <remarks>
    ''' Llamada desde: La página EmisionPedido, método BtnAceptarObsAdjunPed_Click
    ''' Tiempo máximo:0 seg</remarks>
    Public Function CambiarComentario(ByVal tipo As TiposDeDatos.Adjunto.Tipo, Optional ByVal NuevoComentario As String = "") As Integer
        Dim CodError As Integer = 0
        Dim comentario As String
        If NuevoComentario = "" Then
            comentario = Me.Comentario
        Else
            comentario = NuevoComentario
            Me.Comentario = NuevoComentario
        End If
        CodError = DBServer.Adjunto_CambiarComentario(Me.Id, tipo, comentario)
        Return CodError
    End Function
    ''' Revisado por: blp. Fecha: 28/02/2012
    ''' <summary>
    ''' Devuelve el comentario del adjunto
    ''' </summary>
    ''' <param name="TipoAdjunto">Tipo del adjunto. En principio la función se desarrolla sólo para dos tipos: Orden y Línea</param>
    ''' <returns>String con el Comentario</returns>
    ''' <remarks>Llamada desde Recepcion.aspx.vb->GetComentariosAdjunto. Máx. 0,1 seg. </remarks>
    Public Function GetComentarios(ByVal TipoAdjunto As TiposDeDatos.Adjunto.Tipo) As String
        Authenticate()
        Dim sComentarios As String = String.Empty
        Dim sTipoAdjunto As String = String.Empty
        Select Case TipoAdjunto
            Case TiposDeDatos.Adjunto.Tipo.Linea_Pedido
                sTipoAdjunto = "LineaPedido"
            Case TiposDeDatos.Adjunto.Tipo.Orden_Entrega
                sTipoAdjunto = "Orden"
        End Select
        sComentarios = DBServer.Adjunto_GetComentarios(Id, sTipoAdjunto)
        Return sComentarios
    End Function
#End Region

End Class
