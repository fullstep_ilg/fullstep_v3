﻿Public Class cnCategorias
    Inherits Security
    ''' <summary>
    ''' Constructor de la clase cnCategorias
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si está autenticado</param>
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
    ''' <summary>
    ''' Obtiene una lista de las categorias en las que el usuario puede emitir mensajes
    ''' </summary>
    ''' <param name="UsuCod"></param>
    ''' <param name="Idioma"></param>
    ''' <param name="UON1"></param>
    ''' <param name="UON2"></param>
    ''' <param name="UON3"></param>
    ''' <param name="DEP"></param>
    ''' <param name="AccesoEP"></param>
    ''' <param name="AccesoGS"></param>
    ''' <param name="AccesoPM"></param>
    ''' <param name="AccesoQA"></param>
    ''' <param name="AccesoSM"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Categorias_Permiso_EmitirMensajeUsuario(ByVal UsuCod As String, ByVal Idioma As String, _
                                           ByVal UON1 As String, ByVal UON2 As String, ByVal UON3 As String, _
                                           ByVal DEP As String, ByVal AccesoEP As Boolean, ByVal AccesoGS As Boolean, _
                                           ByVal AccesoPM As Boolean, ByVal AccesoQA As Boolean, ByVal AccesoSM As Boolean, _
                                           Optional ByVal Consulta As Boolean = False) As List(Of cn_fsTreeViewItem)
        Authenticate()

        Dim dtDatos As New DataTable
        dtDatos = DBServer.CN_Categorias_Permiso_EmitirMensajeUsuario(UsuCod, Idioma, UON1, UON2, UON3, DEP, _
                                                     AccesoEP, AccesoGS, AccesoPM, AccesoQA, AccesoSM)

        Dim oCategorias_Permiso_EmitirUsuario As New List(Of cn_fsTreeViewItem)
        Try
            Dim ocnCategoria As cn_fsTreeViewItem
            Dim idAnterior As Integer = 0
            For Each row As DataRow In dtDatos.Select("NIVEL=1", "DEN ASC")
                If Not idAnterior = row("ID") Then
                    ocnCategoria = New cn_fsTreeViewItem

                    With ocnCategoria
                        .value = row("ID")
                        .text = row("DEN")
                        .nivel = 1
                        If row("PERMISOPARAPUB") Is DBNull.Value Then
                            .selectable = False
                        Else
                            If Consulta Then
                                .selectable = True
                            Else
                                .selectable = CType(row("PERMISO"), Boolean)
                            End If
                        End If
                        If row("PERMISOPARAPUB") Is DBNull.Value Then
                            .type = 1
                        Else
                            .type = IIf(row("PERMISOPARAPUB") = 0, 2, 0)
                        End If

                        .children = New List(Of cn_fsTreeViewItem)
                        .children = CategoriasChildren(dtDatos, CType(row("NIVEL"), Integer) + 1, row("ID"), Consulta)
                    End With
                    idAnterior = row("ID")

                    oCategorias_Permiso_EmitirUsuario.Add(ocnCategoria)
                End If
            Next

            Return oCategorias_Permiso_EmitirUsuario
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    Private Function CategoriasChildren(ByVal dtDatos As DataTable, ByVal Nivel As Integer, ByVal Padre As String, ByVal Consulta As Boolean) As List(Of cn_fsTreeViewItem)
        Dim oCategorias_Permiso_EmitirUsuario As New List(Of cn_fsTreeViewItem)
        Dim ocnCategoria As cn_fsTreeViewItem
        Dim idAnterior As Integer = 0

        Try
            For Each row As DataRow In dtDatos.Select("NIVEL=" & Nivel & " AND CAT='" & Padre & "'", "DEN ASC")
                If Not idAnterior = row("ID") Then
                    ocnCategoria = New cn_fsTreeViewItem

                    With ocnCategoria
                        .value = row("ID")
                        .text = row("DEN")
                        .nivel = Nivel
                        If row("PERMISOPARAPUB") Is DBNull.Value Then
                            .selectable = False
                        Else
                            If Consulta Then
                                .selectable = True
                            Else
                                .selectable = CType(row("PERMISO"), Boolean)
                            End If
                        End If
                        If row("PERMISOPARAPUB") Is DBNull.Value Then
                            .type = 1
                        Else
                            .type = IIf(row("PERMISOPARAPUB") = 0, 2, 0)
                        End If

                        .children = New List(Of cn_fsTreeViewItem)
                        .children = CategoriasChildren(dtDatos, CType(row("NIVEL"), Integer) + 1, row("ID"), Consulta)
                    End With
                    idAnterior = row("ID")

                    oCategorias_Permiso_EmitirUsuario.Add(ocnCategoria)
                End If
            Next
            Return oCategorias_Permiso_EmitirUsuario
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    ''' <summary>
    ''' Obtiene las categorías en las que el usuario tiene mensajes que puede consultar
    ''' </summary>
    ''' <param name="UsuCod">código del usuario</param>
    ''' <param name="Idioma">idioma en el que obtener los datos</param>
    ''' <param name="AccesoEP">Si el usuario tiene acceso a EP</param>
    ''' <param name="AccesoGS">Si el usuario tiene acceso a GS</param>
    ''' <param name="AccesoPM">Si el usuario tiene acceso a PM</param>
    ''' <param name="AccesoQA">Si el usuario tiene acceso a QA</param>
    ''' <param name="AccesoSM">Si el usuario tiene acceso a SM</param>
    ''' <returns>las categorías</returns>
    ''' <remarks>Llamada desde: CN.ASMX.VB/.Cargar_Categorias_MensajesUsuario; Tiempo máximo:0</remarks>
    Public Function Cargar_Categorias_MensajesUsuario(ByVal UsuCod As String, ByVal Idioma As String, ByVal AccesoEP As Boolean, ByVal AccesoGS As Boolean, _
     ByVal AccesoPM As Boolean, ByVal AccesoQA As Boolean, ByVal AccesoSM As Boolean)
        Authenticate()
        Dim dtDatos As New DataTable
        dtDatos = DBServer.CN_Cargar_Categorias_MensajesUsuario(UsuCod, Idioma, AccesoEP, AccesoGS, AccesoPM, AccesoQA, AccesoSM)

        Dim oCategorias_MensajesUsuario As New List(Of cn_fsTreeViewItem)
        Try
            Dim ocnCategoria As cn_fsTreeViewItem
            Dim idAnterior As Integer = 0
            For Each row As DataRow In dtDatos.Select("NIVEL=1", "DEN ASC")
                If Not idAnterior = row("ID") Then
                    ocnCategoria = New cn_fsTreeViewItem

                    With ocnCategoria
                        .value = row("ID")
                        .text = row("DEN")
                        .nivel = 1
                        .selectable = Not IsDBNull(row("PERMISOPARAPUB"))
                        If row("PERMISOPARAPUB") Is DBNull.Value Then
                            .type = 1
                        Else
                            .type = IIf(row("PERMISOPARAPUB") = 0, 2, 0)
                        End If

                        .children = New List(Of cn_fsTreeViewItem)
                        .children = CategoriasChildren_MensajesUsuario(dtDatos, CType(row("NIVEL"), Integer) + 1, row("ID"))
                    End With
                    idAnterior = row("ID")
                    oCategorias_MensajesUsuario.Add(ocnCategoria)
                End If
            Next
            Return oCategorias_MensajesUsuario
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    Private Function CategoriasChildren_MensajesUsuario(ByVal dtDatos As DataTable, ByVal Nivel As Integer, ByVal Padre As String) As List(Of cn_fsTreeViewItem)
        Dim oCategorias_MensajesUsuario As New List(Of cn_fsTreeViewItem)
        Dim ocnCategoria As cn_fsTreeViewItem
        Dim idAnterior As Integer = 0
        Try
            For Each row As DataRow In dtDatos.Select("NIVEL=" & Nivel & " AND CAT='" & Padre & "'", "DEN ASC")
                If Not idAnterior = row("ID") Then
                    ocnCategoria = New cn_fsTreeViewItem

                    With ocnCategoria
                        .value = row("ID")
                        .text = row("DEN")
                        .nivel = Nivel
                        .selectable = Not IsDBNull(row("PERMISOPARAPUB"))
                        If row("PERMISOPARAPUB") Is DBNull.Value Then
                            .type = 1
                        Else
                            .type = IIf(row("PERMISOPARAPUB") = 0, 2, 0)
                        End If

                        .children = New List(Of cn_fsTreeViewItem)
                        .children = CategoriasChildren_MensajesUsuario(dtDatos, CType(row("NIVEL"), Integer) + 1, row("ID"))
                    End With
                    idAnterior = row("ID")

                    oCategorias_MensajesUsuario.Add(ocnCategoria)
                End If
            Next
            Return oCategorias_MensajesUsuario
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    Private Function CrearCategoria(ByVal row As DataRow, ByVal Categorias As DataTable) As cnCategoria
        Dim ocnCategoria As New cnCategoria
        Dim oIdiomas As New Idiomas(DBServer, mIsAuthenticated)
        Authenticate()

        With ocnCategoria
            .Id = row("ID")
            .Nivel = row("NIVEL")
            If Not row("CAT") Is DBNull.Value Then
                .CategoriaPadre = row("CAT")
            End If
            oIdiomas.Load()
            For Each oIdi In oIdiomas.Idiomas
                .Denominaciones(oIdi.Cod) = DBNullToSomething(row.Item("DENCAT_" & oIdi.Cod).ToString)
            Next

            If Not row("PERMISOPARAPUB") Is DBNull.Value Then
                .selectable = True
            Else
                .selectable = False
            End If
            .CategoriasHijo = New List(Of cnCategoria)
            Dim idAnterior As Integer = 0
            With .CategoriasHijo
                For Each rowHijo As DataRow In Categorias.Select("NIVEL=" & row("NIVEL") + 1 & " AND CAT=" & row("ID"), "DEN ASC")
                    If Not idAnterior = rowHijo("ID") Then
                        .Add(CrearCategoria(rowHijo, Categorias))
                        idAnterior = rowHijo("ID")
                    End If
                Next
            End With
        End With
        Return ocnCategoria
    End Function
    ''' <summary>
    ''' Indica si hay registros despublicados de categorías
    ''' </summary>
    ''' <param name="sUsuCod">Código de usuario </param>
    ''' <param name="sIdioma">Idioma en q mostrar los textos</param> 
    ''' <param name="vRestricc">Restricción categorí­as:
    '''					  1 - dadas de alta por el usuario
    '''					  2 - dadas de alta en el departamento del usuario
    '''					  3 - dadas de alta en la unidad organizativa del usuario
    '''					  4 - sin restricción </param>
    ''' <returns>0/1</returns>
    ''' <remarks>Llamado desde: cnCategorias.aspx  ; Tiempo máximo: 0,3</remarks>
    ''' <revisado>JVS 08/05/2012</revisado>
    Public Function Despublicados_Categorias(ByVal sUsuCod As String, ByVal sIdioma As String, ByVal vRestricc As OrganizacionRed) As Integer
        Authenticate()
        Return DBServer.CN_Despublicados_Categorias(sUsuCod, sIdioma, vRestricc)
    End Function
    ''' <summary>
    ''' Carga de categorías
    ''' </summary>
    ''' <param name="sUsuCod">Código de usuario </param>
    ''' <param name="sIdioma">Idioma en q mostrar los textos</param> 
    ''' <param name="vRestricc">Restricción categorías:
    '''					  1 - dadas de alta por el usuario
    '''					  2 - dadas de alta en el departamento del usuario
    '''					  3 - dadas de alta en la unidad organizativa del usuario
    '''					  4 - sin restricción </param>
    ''' <returns>Las categorías</returns>
    ''' <remarks>Llamado desde: cnCategorias.aspx  ; Tiempo máximo: 0,3</remarks>
    ''' <revisado>JVS 16/02/2012</revisado>
    Public Function Load_Categorias(ByVal sUsuCod As String, ByVal sIdioma As String, ByVal vRestricc As OrganizacionRed) As DataSet
        Authenticate()
        Return DBServer.CN_Load_Categorias(sUsuCod, sIdioma, vRestricc)
    End Function
End Class
