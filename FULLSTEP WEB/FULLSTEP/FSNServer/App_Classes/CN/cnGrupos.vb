﻿<Serializable()> _
Public Class cnGrupos
    Inherits Security

    ''' <summary>
    ''' Constructor de la clase cnGrupos
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si está autenticado</param>
    ''' <revision>JVS 30/01/2012</revision>
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub

    ''' Revisado por: JVS; Fecha: 30/01/2012
    ''' <summary>
    ''' Carga los grupos a los que pertenece el usuario
    ''' </summary>
    ''' <param name="sUsuCod">Código de usuario </param>
    ''' <param name="sIdioma">Idioma en q mostrar los textos</param>
    ''' <remarks>Llamada desde: cn_UserProfile.aspx ; Tiempo máximo: 0,3</remarks>
    Public Function CN_Load_Grupos_Usuario(ByVal sUsuCod As String, ByVal sIdioma As String) As DataSet
        Authenticate()

        Return DBServer.CN_Load_Grupos_Usuario(sUsuCod, sIdioma)
    End Function

    ''' Revisado por: JVS; Fecha: 27/03/2012
    ''' <summary>
    ''' Carga de grupos
    ''' </summary>
    ''' <param name="sUsuCod">Código de usuario </param>
    ''' <param name="sIdioma">Idioma en q mostrar los textos</param>
    ''' <param name="vRestricc">Restricción categorías:
    '''					  1 - dadas de alta por el usuario
    '''					  2 - dadas de alta en el departamento del usuario
    '''					  3 - dadas de alta en la unidad organizativa del usuario
    '''					  4 - sin restricción </param>
    ''' <remarks>Llamada desde: cn_Grupos.aspx ; Tiempo máximo: 0,3</remarks>
    Public Function Load_Grupos(ByVal sUsuCod As String, ByVal sIdioma As String, ByVal vRestricc As OrganizacionRed) As DataTable
        Authenticate()

        Return DBServer.CN_Load_Grupos(sUsuCod, sIdioma, vRestricc)
    End Function
End Class
