﻿Public Class cn_fsItem
    Private _value As String
    Public Property value() As String
        Get
            Return _value
        End Get
        Set(ByVal value As String)
            _value = value
        End Set
    End Property
    Private _text As String
    Public Property text() As String
        Get
            Return _text
        End Get
        Set(ByVal value As String)
            _text = value
        End Set
    End Property
    Private _imgSrc As String
    Public Property imgSrc() As String
        Get
            Return _imgSrc
        End Get
        Set(ByVal value As String)
            _imgSrc = value
        End Set
    End Property
End Class
