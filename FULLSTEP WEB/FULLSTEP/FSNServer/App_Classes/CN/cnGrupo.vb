﻿<Serializable()> _
Public Class cnGrupo
    Inherits Security

#Region " Constructor"
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
        _NomGrupo = New cnMultiIdioma
        _DescrGrupo = New cnMultiIdioma
    End Sub
    Public Sub New()

    End Sub
#End Region
    Private _Data As DataSet
    Public ReadOnly Property Data() As Data.DataSet
        Get
            Return _Data
        End Get
    End Property
    Private _IdGrupo As Integer
    Public Property IdGrupo() As Integer
        Get
            Return _IdGrupo
        End Get
        Set(ByVal value As Integer)
            _IdGrupo = value
        End Set
    End Property
    Private _NomGrupo As cnMultiIdioma
    Public ReadOnly Property NomGrupo() As cnMultiIdioma
        Get
            Return _NomGrupo
        End Get
    End Property
    Private _DescrGrupo As cnMultiIdioma
    Public ReadOnly Property DescrGrupo() As cnMultiIdioma
        Get
            Return _DescrGrupo
        End Get
    End Property
    ''' <summary>
    ''' Guarda en base de datos los datos del nuevo grupo
    ''' </summary>
    ''' <param name="sUsu">Código de usuario </param>
    ''' <param name="sIdioma">Idioma en q mostrar los textos</param>
    ''' <param name="sNomGrupo">nombre</param>
    ''' <param name="sDescGrupo">descripción</param>
    ''' <param name="dtUON">tabla de UON</param>
    ''' <remarks>Llamado desde: Grupo.asmx.vb  ; Tiempo máximo: 0,1</remarks>
    ''' <revisado>JVS 26/03/2012</revisado>
    Public Function Insertar_Grupo(ByVal sUsu As String, ByVal sIdioma As String, ByVal sNomGrupo As cnMultiIdioma, _
                              ByVal sDescGrupo As cnMultiIdioma, ByVal dtUON As DataTable) As Integer
        Dim dtDenom As DataTable
        Dim dtNewRow As DataRow

        Dim oIdiomas As New Idiomas(DBServer, mIsAuthenticated)
        Authenticate()

        dtDenom = New DataTable

        dtDenom.Columns.Add("IDI", System.Type.GetType("System.String"))

        dtDenom.Columns.Add("DEN", System.Type.GetType("System.String"))
        dtDenom.Columns("DEN").AllowDBNull = True

        dtDenom.Columns.Add("DESCR", System.Type.GetType("System.String"))
        dtDenom.Columns("DESCR").AllowDBNull = True

        oIdiomas.Load()

        For Each oIdi In oIdiomas.Idiomas
            dtNewRow = dtDenom.NewRow
            dtNewRow.Item("IDI") = oIdi.Cod
            dtNewRow.Item("DEN") = sNomGrupo(oIdi.Cod)
            dtNewRow.Item("DESCR") = sDescGrupo(oIdi.Cod)
            dtDenom.Rows.Add(dtNewRow)
        Next

        Return DBServer.CN_InsertarGrupo(sUsu, sIdioma, dtDenom, dtUON)
    End Function
    ''' <summary>
    ''' Guarda en base de datos los datos del grupo
    ''' </summary>
    ''' <param name="sIdioma">Idioma en q mostrar los textos</param>
    ''' <param name="sNomGrupo">nombre</param>
    ''' <param name="sDescGrupo">descripción</param>
    ''' <remarks>Llamado desde: Grupo.asmx.vb  ; Tiempo máximo: 0,1</remarks>
    ''' <revisado>JVS 26/03/2012</revisado>
    Public Sub Update_Grupo(ByVal sIdioma As String, ByVal sNomGrupo As cnMultiIdioma, _
                                ByVal sDescGrupo As cnMultiIdioma, ByRef dtUON As DataTable)

        Dim dtDenom As DataTable
        Dim dtNewRow As DataRow

        Dim oIdiomas As New Idiomas(DBServer, mIsAuthenticated)
        Authenticate()

        dtDenom = New DataTable

        dtDenom.Columns.Add("IDI", System.Type.GetType("System.String"))
        dtDenom.Columns.Add("DEN", System.Type.GetType("System.String"))
        dtDenom.Columns("DEN").AllowDBNull = True
        dtDenom.Columns.Add("DESCR", System.Type.GetType("System.String"))
        dtDenom.Columns("DESCR").AllowDBNull = True

        oIdiomas.Load()

        For Each oIdi In oIdiomas.Idiomas
            dtNewRow = dtDenom.NewRow
            dtNewRow.Item("IDI") = oIdi.Cod
            dtNewRow.Item("DEN") = sNomGrupo(oIdi.Cod)
            dtNewRow.Item("DESCR") = sDescGrupo(oIdi.Cod)
            dtDenom.Rows.Add(dtNewRow)
        Next

        DBServer.CN_Update_Grupo(_IdGrupo, sIdioma, dtDenom, dtUON)
    End Sub
    ''' <summary>
    ''' Borra en base de datos el grupo
    ''' </summary>
    ''' <remarks>Llamado desde: Grupo.asmx.vb  ; Tiempo máximo: 0,1</remarks>
    ''' <revisado>JVS 26/03/2012</revisado>
    Public Sub Delete_Grupo()
        Authenticate()

        DBServer.CN_Delete_Grupo(_IdGrupo)
    End Sub
    ''' <summary>
    ''' Carga los datos del grupo
    ''' </summary>
    ''' <param name="iIdGrupo">Grupo </param>   
    ''' <remarks>Llamada desde: grupos.asmx.vb; Tiempo máximo:0,2</remarks>
    ''' <revisado>JVS 29/03/2012</revisado>
    Public Sub LoadGrupo(ByVal iIdGrupo As Integer)
        Dim data As DataTable
        Dim row As DataRow
        Dim oIdiomas As New Idiomas(DBServer, mIsAuthenticated)
        Authenticate()

        data = DBServer.CN_Load_Datos_Grupo(iIdGrupo)


        If Not data.Rows.Count = 0 Then
            row = data.Rows(0)

            _IdGrupo = DBNullToSomething(row.Item("IDGRUPO"))
            oIdiomas.Load()
            For Each oIdi In oIdiomas.Idiomas
                _NomGrupo(oIdi.Cod) = DBNullToSomething(row.Item("NOMGRUPO_" & oIdi.Cod).ToString)
                _DescrGrupo(oIdi.Cod) = DBNullToSomething(row.Item("DESCRGRUPO_" & oIdi.Cod).ToString)
            Next
        End If
        row = Nothing
        data = Nothing

    End Sub
    ''' <summary>
    ''' Carga los miembros de la red con permiso de publicación en la grupo
    ''' </summary>
    ''' <param name="Idi"></param>
    ''' <param name="IdGrupo"></param>
    ''' <remarks>Llamada desde: CN.asmx.vb; Tiempo máximo:0,2</remarks>
    ''' <revisado>JVS 28/03/2012</revisado>
    Public Function Load_Miembros_Grupo(ByVal Idi As String, _
                                            ByVal IdGrupo As Nullable(Of Integer)) As List(Of Object)
        Authenticate()

        Dim dsDatos As DataTable

        Try
            dsDatos = DBServer.CN_Load_Miembros_Grupo(Idi, IdGrupo)

            Dim oMiembrosGrupo As New List(Of Object)

            Dim oParaPorGrupo As New List(Of cn_fsItem)
            Dim iPara As cn_fsItem

            For Each row As DataRow In dsDatos.Rows
                iPara = New cn_fsItem
                With iPara
                    .value = "itemParaUON" & _
                        IIf(IsDBNull(row("UON0")), "", "-UON0_" & row("UON0")) & _
                        IIf(IsDBNull(row("UON1")), "", "-UON1_" & row("UON1")) & _
                        IIf(IsDBNull(row("UON2")), "", "-UON2_" & row("UON2")) & _
                        IIf(IsDBNull(row("UON3")), "", "-UON3_" & row("UON3")) & _
                        IIf(IsDBNull(row("DEP")), "", "-DEP_" & row("DEP")) & _
                        IIf(IsDBNull(row("USU")), "", "-USU_" & row("USU"))
                    .text = IIf(IsDBNull(row("UON1")), "", row("UON1") & "-") & _
                        IIf(IsDBNull(row("UON2")), "", row("UON2") & "-") & _
                        IIf(IsDBNull(row("UON3")), "", row("UON3") & "-") & _
                        IIf(IsDBNull(row("DEP")), "", row("DEP")) & _
                        IIf(IsDBNull(row("USU")), "", "-USU_" & row("USU") & "-") & row("DEN").ToString
                End With
                oParaPorGrupo.Add(iPara)
            Next

            With oMiembrosGrupo
                .Add(oParaPorGrupo)
            End With

            Return oMiembrosGrupo
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    ''' <summary>
    ''' Obtener UONs para asignar al grupo
    ''' </summary>
    ''' <param name="Idioma"></param>
    ''' <returns>List(Of cn_fsTreeViewItem)</returns>
    ''' <remarks>Llamado desde: CN.asmx.vb  ; Tiempo máximo: 0,3</remarks>
    ''' <revisado>JVS 09/03/2012</revisado>
    Public Function Obtener_UONs_Grupo(ByVal Idioma As String) As List(Of cn_fsTreeViewItem)
        Authenticate()

        Try
            Dim dsDatos As New DataSet
            dsDatos = DBServer.CN_Obtener_UONs_Grupo(Idioma)

            Dim oUONPara As New List(Of cn_fsTreeViewItem)
            Dim item0, item1, item2, item3, item4, item5 As cn_fsTreeViewItem
            ' UON0 - LA EMPRESA
            item0 = New cn_fsTreeViewItem

            With item0
                .value = dsDatos.Tables(0).Rows(0)("COD")
                .text = dsDatos.Tables(0).Rows(0)("DEN")
                .selectable = 1
                .type = IIf(.selectable, 3, 0)
                .nivel = "UON0"
                .expanded = True

                'UON1 Y UON0_DEP
                .children = New List(Of cn_fsTreeViewItem)
                With .children
                    For Each row1 As DataRow In dsDatos.Tables(2).Rows
                        item1 = New cn_fsTreeViewItem

                        With item1
                            .value = row1("COD")
                            .text = row1("COD") & " - " & row1("DEN")
                            .selectable = 1
                            .type = IIf(.selectable, 3, 0)

                            If row1("ESDEPARTAMENTO") Then
                                .nivel = "DEP"
                                .expanded = False
                                .children = New List(Of cn_fsTreeViewItem)
                                With .children
                                    For Each USU1 As DataRow In dsDatos.Tables(1).Select("UON1 IS NULL AND UON2 IS NULL AND UON3 IS NULL AND DEP='" & row1("COD") & "'")
                                        item2 = New cn_fsTreeViewItem

                                        With item2
                                            .value = USU1("COD")
                                            .text = USU1("NOM") & " " & USU1("APE") & IIf(USU1("EMAIL") Is DBNull.Value, "", " (" & USU1("EMAIL") & ")")
                                            .selectable = 1
                                            .type = 3
                                            .nivel = "USU"
                                        End With
                                        .Add(item2)
                                    Next
                                End With
                            Else
                                .nivel = "UON1"
                                .expanded = False
                                'UON2 Y UON1_DEP
                                .children = New List(Of cn_fsTreeViewItem)
                                With .children
                                    For Each row2 As DataRow In dsDatos.Tables(3).Select("UON1='" & row1("COD") & "'")
                                        item2 = New cn_fsTreeViewItem

                                        With item2
                                            .value = row2("COD")
                                            .text = row2("COD") & " - " & row2("DEN")
                                            .selectable = 1
                                            .type = IIf(.selectable, 3, 0)

                                            If row2("ESDEPARTAMENTO") Then
                                                .nivel = "DEP"
                                                .expanded = False
                                                .children = New List(Of cn_fsTreeViewItem)
                                                With .children
                                                    For Each USU2 As DataRow In dsDatos.Tables(1).Select("UON1='" & row1("COD") & "' AND UON2 IS NULL AND UON3 IS NULL AND DEP='" & row2("COD") & "'")
                                                        item3 = New cn_fsTreeViewItem

                                                        With item3
                                                            .value = USU2("COD")
                                                            .text = USU2("NOM") & " " & USU2("APE") & IIf(USU2("EMAIL") Is DBNull.Value, "", " (" & USU2("EMAIL") & ")")
                                                            .selectable = 1
                                                            .type = 3
                                                            .nivel = "USU"
                                                        End With
                                                        .Add(item3)
                                                    Next
                                                End With
                                            Else
                                                .nivel = "UON2"
                                                .expanded = False
                                                'UON3 Y UON2_DEP
                                                .children = New List(Of cn_fsTreeViewItem)
                                                With .children
                                                    For Each row3 As DataRow In dsDatos.Tables(4).Select("UON1='" & row1("COD") & "' AND UON2='" & row2("COD") & "'")
                                                        item3 = New cn_fsTreeViewItem

                                                        With item3
                                                            .value = row3("COD")
                                                            .text = row3("COD") & " - " & row3("DEN")
                                                            .selectable = 1
                                                            .type = IIf(.selectable, 3, 0)

                                                            If row3("ESDEPARTAMENTO") Then
                                                                .nivel = "DEP"
                                                                .expanded = False
                                                                .children = New List(Of cn_fsTreeViewItem)
                                                                With .children
                                                                    For Each USU3 As DataRow In dsDatos.Tables(1).Select("UON1='" & row1("COD") & "' AND UON2='" & row2("COD") & "' AND UON3 IS NULL AND DEP='" & row3("COD") & "'")
                                                                        item4 = New cn_fsTreeViewItem

                                                                        With item4
                                                                            .value = USU3("COD")
                                                                            .text = USU3("NOM") & " " & USU3("APE") & IIf(USU3("EMAIL") Is DBNull.Value, "", " (" & USU3("EMAIL") & ")")
                                                                            .selectable = 1
                                                                            .type = 3
                                                                            .nivel = "USU"
                                                                        End With
                                                                        .Add(item4)
                                                                    Next
                                                                End With
                                                            Else
                                                                .nivel = "UON3"
                                                                .expanded = False
                                                                'UON3_DEP
                                                                .children = New List(Of cn_fsTreeViewItem)
                                                                With .children
                                                                    For Each row4 As DataRow In dsDatos.Tables(5).Select("UON1='" & row1("COD") & "' AND UON2='" & row2("COD") & "' AND UON3='" & row3("COD") & "'")
                                                                        item4 = New cn_fsTreeViewItem

                                                                        With item4
                                                                            .value = row4("COD")
                                                                            .text = row4("COD") & " - " & row4("DEN")
                                                                            .selectable = 1
                                                                            .type = IIf(.selectable, 3, 0)
                                                                            .nivel = "DEP"
                                                                            .expanded = False

                                                                            .children = New List(Of cn_fsTreeViewItem)
                                                                            With .children
                                                                                For Each USU4 As DataRow In dsDatos.Tables(1).Select("UON1='" & row1("COD") & "' AND UON2='" & row2("COD") & "' AND UON3='" & row3("COD") & "' AND DEP='" & row4("COD") & "'")
                                                                                    item5 = New cn_fsTreeViewItem

                                                                                    With item5
                                                                                        .value = USU4("COD")
                                                                                        .text = USU4("NOM") & " " & USU4("APE") & IIf(USU4("EMAIL") Is DBNull.Value, "", " (" & USU4("EMAIL") & ")")
                                                                                        .selectable = 1
                                                                                        .type = 3
                                                                                        .nivel = "USU"
                                                                                    End With
                                                                                    .Add(item5)
                                                                                Next
                                                                            End With
                                                                        End With
                                                                        .Add(item4)
                                                                    Next
                                                                End With
                                                            End If
                                                        End With
                                                        .Add(item3)
                                                    Next
                                                End With
                                            End If
                                        End With
                                        .Add(item2)
                                    Next
                                End With
                            End If
                        End With
                        .Add(item1)
                    Next
                End With
            End With

            oUONPara.Add(item0)

            Return oUONPara
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    ''' <summary>
    ''' Notificar Alta Grupo cn
    ''' </summary>
    ''' <param name="EFrom">quien notifica</param>
    ''' <param name="Notificados">a quien se notifica</param>
    ''' <param name="idGrupo">Grupo cn</param>
    ''' <remarks>Llamada desde: CN\App_Services\Grupo.asmx.vb; Tiempo maximo:0</remarks>
    Public Sub Notificar_AltaGrupo(ByVal EFrom As String, ByVal Notificados As DataTable, ByVal idGrupo As Integer)
        Authenticate()

        Dim oNotificador As New Notificar(DBServer, mIsAuthenticated)
        oNotificador.NotificacionAltaGrupo(EFrom, Notificados, idGrupo)

        oNotificador.FuerzaFinalizeSmtpClient()
        oNotificador = Nothing
    End Sub
End Class
