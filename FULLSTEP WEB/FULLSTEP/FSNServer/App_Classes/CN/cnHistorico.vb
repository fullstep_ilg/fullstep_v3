﻿Public Class cnHistorico
    Inherits Security

    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub


    ''' <summary>
    ''' Backup de CN
    ''' </summary>
    ''' <param name="iPeriodoExclusion">Periodo de Exclusion</param>
    ''' <param name="iLimiteRegistros">Limite de Registros</param>
    ''' <remarks></remarks>
    Public Sub BackupCN_InicioTarea_FSCN(ByVal iPeriodoExclusion As Integer, ByVal iLimiteRegistros As Integer)
        Authenticate()
        DBServer.CN_Historico_BackupCNInicioTarea(iPeriodoExclusion, iLimiteRegistros)
    End Sub
End Class
