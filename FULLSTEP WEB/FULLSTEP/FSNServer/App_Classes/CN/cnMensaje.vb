﻿Imports System.Threading

Public Class cnMensaje
#Region "Propiedades"
    Private _IdMensaje As Nullable(Of Integer)
    Public Property IdMensaje() As Nullable(Of Integer)
        Get
            Return _IdMensaje
        End Get
        Set(ByVal value As Nullable(Of Integer))
            _IdMensaje = value
        End Set
    End Property
    Private _EnProceso As String
    Public Property EnProceso() As String
        Get
            Return _EnProceso
        End Get
        Set(ByVal value As String)
            _EnProceso = value
        End Set
    End Property
    Private _Categoria As cnCategoria
    Public Property Categoria() As cnCategoria
        Get
            Return _Categoria
        End Get
        Set(ByVal value As cnCategoria)
            _Categoria = value
        End Set
    End Property
    Private _Tipo As Integer
    Public Property Tipo() As Integer
        Get
            Return _Tipo
        End Get
        Set(ByVal value As Integer)
            _Tipo = value
        End Set
    End Property
    Private _InfoUsuario As cnUsuario
    Public Property InfoUsuario() As cnUsuario

        Get
            Return _InfoUsuario
        End Get
        Set(ByVal value As cnUsuario)
            _InfoUsuario = value
        End Set
    End Property
    Private _Titulo As String
    Public Property Titulo() As String
        Get
            Return _Titulo
        End Get
        Set(ByVal value As String)
            _Titulo = value
        End Set
    End Property
    Private _Contenido As String
    Public Property Contenido() As String
        Get
            Return _Contenido
        End Get
        Set(ByVal value As String)
            _Contenido = value
        End Set
    End Property
    Private _Fecha As Nullable(Of DateTime)
    Public Property Fecha() As Nullable(Of DateTime)
        Get
            Return _Fecha
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            _Fecha = value
        End Set
    End Property
    Private _Cuando As String
    Public Property Cuando() As String
        Get
            Return _Cuando
        End Get
        Set(ByVal value As String)
            _Cuando = value
        End Set
    End Property
    Private _Donde As String
    Public Property Donde() As String
        Get
            Return _Donde
        End Get
        Set(ByVal value As String)
            _Donde = value
        End Set
    End Property
    Private _MesCorto As String
    Public Property MesCorto() As String
        Get
            Return _MesCorto
        End Get
        Set(ByVal value As String)
            _MesCorto = value
        End Set
    End Property
    Private _Dia As Int16
    Public Property Dia() As Int16
        Get
            Return _Dia
        End Get
        Set(ByVal value As Int16)
            _Dia = value
        End Set
    End Property
    Private _Hora As String
    Public Property Hora() As String
        Get
            Return _Hora
        End Get
        Set(ByVal value As String)
            _Hora = value
        End Set
    End Property
    Private _Adjuntos As List(Of cnAdjunto)
    Public Property Adjuntos() As List(Of cnAdjunto)
        Get
            Return _Adjuntos
        End Get
        Set(ByVal value As List(Of cnAdjunto))
            _Adjuntos = value
        End Set
    End Property
    Private _FechaAlta As DateTime
    Public Property FechaAlta() As DateTime
        Get
            Return _FechaAlta
        End Get
        Set(ByVal value As DateTime)
            _FechaAlta = value
        End Set
    End Property
    Private _FechaAltaRelativa As String
    Public Property FechaAltaRelativa() As String
        Get
            Return _FechaAltaRelativa
        End Get
        Set(ByVal value As String)
            _FechaAltaRelativa = value
        End Set
    End Property
    Private _FechaActualizacion As DateTime
    Public Property FechaActualizacion() As DateTime
        Get
            Return _FechaActualizacion
        End Get
        Set(ByVal value As DateTime)
            _FechaActualizacion = value
        End Set
    End Property
    Private _FechaActualizacionRelativa As String
    Public Property FechaActualizacionRelativa() As String
        Get
            Return _FechaActualizacionRelativa
        End Get
        Set(ByVal value As String)
            _FechaActualizacionRelativa = value
        End Set
    End Property
    Private _MeGusta As cnMeGusta
    Public Property MeGusta() As cnMeGusta
        Get
            Return _MeGusta
        End Get
        Set(ByVal value As cnMeGusta)
            _MeGusta = value
        End Set
    End Property
    Private _Respuestas As List(Of cnRespuesta)
    Public Property Respuestas() As List(Of cnRespuesta)
        Get
            Return _Respuestas
        End Get
        Set(ByVal value As List(Of cnRespuesta))
            _Respuestas = value
        End Set
    End Property
    Private _NumeroRespuestas As Integer
    Public Property NumeroRespuestas() As Integer
        Get
            Return _NumeroRespuestas
        End Get
        Set(ByVal value As Integer)
            _NumeroRespuestas = value
        End Set
    End Property
    Private _Para As cnMensajePara
    Public Property Para() As cnMensajePara
        Get
            Return _Para
        End Get
        Set(ByVal value As cnMensajePara)
            _Para = value
        End Set
    End Property
    Private _Leido As Boolean
    Public Property Leido() As Boolean
        Get
            Return _Leido
        End Get
        Set(ByVal value As Boolean)
            _Leido = value
        End Set
    End Property
    Private _Oculto As Boolean
    Public Property Oculto() As Boolean
        Get
            Return _Oculto
        End Get
        Set(ByVal value As Boolean)
            _Oculto = value
        End Set
    End Property
    Private _PermisoEditar As Boolean
    Public Property PermisoEditar() As Boolean
        Get
            Return _PermisoEditar
        End Get
        Set(ByVal value As Boolean)
            _PermisoEditar = value
        End Set
    End Property
    Private _TieneRespuestasOcultas As Boolean
    Public Property TieneRespuestasOcultas() As Boolean
        Get
            Return _TieneRespuestasOcultas
        End Get
        Set(ByVal value As Boolean)
            _TieneRespuestasOcultas = value
        End Set
    End Property
    Private _MensajeHistorico As Boolean
    Public Property MensajeHistorico() As Boolean
        Get
            Return _MensajeHistorico
        End Get
        Set(ByVal value As Boolean)
            _MensajeHistorico = value
        End Set
    End Property
    Private _IdDiscrepancia As Integer
    Public Property IdDiscrepancia() As Integer
        Get
            Return _IdDiscrepancia
        End Get
        Set(ByVal value As Integer)
            _IdDiscrepancia = value
        End Set
    End Property
    Private _DiscrepanciaCerrada As Boolean
    Public Property DiscrepanciaCerrada() As Boolean
        Get
            Return _DiscrepanciaCerrada
        End Get
        Set(ByVal value As Boolean)
            _DiscrepanciaCerrada = value
        End Set
    End Property
#End Region
End Class
