﻿<Serializable()> _
Public Class cnMiembros
    Inherits Security

    ''' <summary>
    ''' Constructor de la clase cnMiembros
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si está autenticado</param>
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub

    Public Sub New()

    End Sub

    ''' Revisado por: JVS; Fecha: 30/01/2012
    ''' <summary>
    ''' Carga los miembros de los grupos de CN
    ''' </summary>
    ''' <remarks>Llamada desde: cn_UserProfile.aspx ; Tiempo máximo: 0,3</remarks>
    Public Function CN_Load_Miembros_Grupos() As DataSet
        Authenticate()

        Return DBServer.CN_Load_Miembros_Grupos()
    End Function
End Class
