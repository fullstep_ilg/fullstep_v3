﻿<Serializable()> _
Public Class cnMiembroCateg
    Inherits Security

    '#Region " Constructor"

    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub

    Public Sub New()

    End Sub

    '#End Region

    Private _Para_UON_UON0 As Boolean
    Public Property Para_UON_UON0() As Boolean
        Get
            Return _Para_UON_UON0
        End Get
        Set(ByVal value As Boolean)
            _Para_UON_UON0 = value
        End Set
    End Property

    Private _Para_UON As DataTable
    Public Property Para_UON() As DataTable
        Get
            Return _Para_UON
        End Get
        Set(ByVal value As DataTable)
            _Para_UON = value
        End Set
    End Property

    Private _Para_Grupo_EP As Boolean
    Public Property Para_Grupo_EP() As Boolean
        Get
            Return _Para_Grupo_EP
        End Get
        Set(ByVal value As Boolean)
            _Para_Grupo_EP = value
        End Set
    End Property

    Private _Para_Grupo_GS As Boolean
    Public Property Para_Grupo_GS() As Boolean
        Get
            Return _Para_Grupo_GS
        End Get
        Set(ByVal value As Boolean)
            _Para_Grupo_GS = value
        End Set
    End Property

    Private _Para_Grupo_PM As Boolean
    Public Property Para_Grupo_PM() As Boolean
        Get
            Return _Para_Grupo_PM
        End Get
        Set(ByVal value As Boolean)
            _Para_Grupo_PM = value
        End Set
    End Property

    Private _Para_Grupo_QA As Boolean
    Public Property Para_Grupo_QA() As Boolean
        Get
            Return _Para_Grupo_QA
        End Get
        Set(ByVal value As Boolean)
            _Para_Grupo_QA = value
        End Set
    End Property

    Private _Para_Grupo_SM As Boolean
    Public Property Para_Grupo_SM() As Boolean
        Get
            Return _Para_Grupo_SM
        End Get
        Set(ByVal value As Boolean)
            _Para_Grupo_SM = value
        End Set
    End Property

    Private _Para_Grupos_Grupos As DataTable
    Public Property Para_Grupos_Grupos() As DataTable
        Get
            Return _Para_Grupos_Grupos
        End Get
        Set(ByVal value As DataTable)
            _Para_Grupos_Grupos = value
        End Set
    End Property
End Class

