﻿Imports System.Globalization
Imports Fullstep.FSNLibrary
Imports System.Configuration

Public Class cnDiscrepancias
    Inherits Security
    ''' <summary>
    ''' Constructor de la clase cnMensajes
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si está autenticado</param>
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub

    ''' <summary>
    ''' Obtiene la información de los contactos de proveedor a los cuales se dirigira la discrepancia
    ''' </summary>
    ''' <param name="Prove">Código del proveedor</param>
    ''' <param name="Idi">Idioma en el que se devolverán los datos</param>    
    ''' <returns>Lista de objetos cn_fsItem con los datos</returns>
    ''' <remarks>Llamada desde: Facturas.asmx.vb</remarks>
    ''' 
    Public Function Obtener_Datos_NuevaDiscrepancia(ByVal Prove As String, ByVal Idi As String) As List(Of cn_fsItem)
        Authenticate()
        Dim dsDatos As DataSet
        Try
            dsDatos = DBServer.CN_Obtener_Datos_NuevaDiscrepancia(Prove, Idi)
            Dim lItems As New List(Of cn_fsItem)
            Dim item As cn_fsItem
            With dsDatos
                If .Tables("CATEGORIA").Rows.Count = 0 Then Return Nothing
                item = New cn_fsItem
                item.value = .Tables("CATEGORIA").Rows(0)("ID")
                item.text = .Tables("CATEGORIA").Rows(0)("NOMBRECOMPLETO")
                lItems.Add(item)
                For Each row As DataRow In .Tables("CONTACTOS").Rows
                    item = New cn_fsItem
                    With item
                        .value = row("PROVE") & "-" & row("ID")
                        .text = row("PROVE") & " - " & row("DEN") & " (" & row("EMAIL").ToString & ")"
                    End With
                    lItems.Add(item)
                Next
            End With
            Return lItems
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    ''' <summary>Inserta un nuevo mensaje</summary>
    ''' <param name="Mensaje">Objeto con los datos del mensaje</param>
    ''' <param name="Factura">Id. de factura</param>    
    ''' <param name="Linea">Linea de factura</param>    
    ''' <param name="dtAdjuntos">Datos de los adjuntos del mensaje</param>    
    ''' <returns>Entero</returns>
    ''' <remarkas>Llamada desde: CN.asmx.vb</remarkas>
    ''' 
    Public Function InsertarDiscrepancia(ByVal Mensaje As cnMensaje, ByVal Factura As Integer, ByVal Linea As Integer, ByRef dtAdjuntos As DataTable) As Integer
        Authenticate()
        With Mensaje
            DBServer.CN_InsertarDiscrepancia(.InfoUsuario.Cod, _
                .Tipo, .FechaAlta, .Titulo, .Contenido, .Fecha, .Donde, .Categoria.Id, .Para.Para_UON_UON0, _
                .Para.Para_UON, .Para.Para_Prove_Tipo, .Para.Para_Prove_Con, _
                .Para.Para_Material_GMN1, .Para.Para_Material_GMN2, .Para.Para_Material_GMN3, .Para.Para_Material_GMN4, .Para.Para_Material_QA, _
                .Para.Para_ProceCompra_Anyo, .Para.Para_ProceCompra_GMN1, .Para.Para_ProceCompra_Cod, _
                .Para.Para_ProcesoCompra_Responsable, .Para.Para_ProcesoCompra_Invitados, _
                .Para.Para_ProcesoCompra_Compradores, .Para.Para_ProcesoCompra_Proveedores, _
                .Para.Para_EstructuraCompras_Equipo, .Para.Para_EstructuraCompras_MaterialGS, _
                .Para.Para_Grupo_EP, .Para.Para_Grupo_GS, .Para.Para_Grupo_PM, .Para.Para_Grupo_QA, .Para.Para_Grupo_SM, _
                .Para.Para_Grupos_Grupos, dtAdjuntos, Factura, Linea)
        End With
    End Function
    ''' <summary>
    ''' Obtiene las líneas de una factura
    ''' </summary>
    ''' <param name="sIdioma">Idioma en q mostrar los textos</param> 
    ''' <param name="iFactura">Id de factura </param>
    ''' <returns>Las líneas de la factura</returns>
    ''' <remarks>Llamado desde: Factura.asmx.vb  ; Tiempo máximo: 0,3</remarks>
    ''' <revisado>JVS 22/06/2012</revisado>
    Public Function Obtener_Lineas_Factura(ByVal sIdioma As String, ByVal iFactura As Integer, ByVal Usu As String, ByVal Linea As Integer, _
                                ByVal UsuFormat As System.Globalization.NumberFormatInfo) As List(Of cnLineaFactura)
        Authenticate()

        Dim dtDatos As DataTable
        Try
            dtDatos = DBServer.CN_Obtener_Lineas_Factura(sIdioma, iFactura, Usu, Linea)

            Dim oLineas As New List(Of cnLineaFactura)
            Dim oLineaFact As cnLineaFactura

            For Each row As DataRow In dtDatos.Rows
                oLineaFact = New cnLineaFactura
                With oLineaFact
                    .Id = row("LINEA")
                    .NumLinea = row("NUM")
                    .Codigo = row("ART_INT").ToString
                    .Denominacion = row("ART_DEN").ToString
                    If Not IsDBNull(row("CANT_PED")) Then
                        .Cantidad = CType(row("CANT_PED"), Double)
                        .CantidadFormatoUsu = FormatNumber(.Cantidad, UsuFormat) & " " & row("UP")
                    End If
                    .Unidad = row("UP")
                    If Not IsDBNull(row("PREC_UP")) Then
                        .Precio = CType(row("PREC_UP"), Double)
                        .PrecioFormatoUsu = FormatNumber(.Precio, UsuFormat) & " " & row("MON")
                    End If
                    If Not IsDBNull(row("TOTAL_COSTES")) Then
                        .TotalCostes = row("TOTAL_COSTES")
                        .TotalCostesFormatoUsu = FormatNumber(.TotalCostes, UsuFormat) & " " & row("MON")
                    End If
                    If Not IsDBNull(row("TOTAL_DCTOS")) Then
                        .TotalDescuentos = row("TOTAL_DCTOS")
                        .TotalDescuentosFormatoUsu = FormatNumber(.TotalDescuentos, UsuFormat) & " " & row("MON")
                    End If
                    If Not IsDBNull(row("IMPORTE")) Then
                        .Importe = row("IMPORTE")
                        .ImporteFormatoUsu = FormatNumber(.Importe, UsuFormat) & " " & row("MON")
                    End If
                    .Unidad = row("UP")
                    .Moneda = row("MON")
                    .Obs = row("OBS").ToString
                    .PedidoFs = row("PEDIDO_FS").ToString
                    .RefFactura = row("NUM_PED_ERP").ToString
                    .Albaran = row("ALBARAN").ToString
                    .DocSAP = row("NUM_FACTURA_SAP").ToString
                End With
                oLineas.Add(oLineaFact)
            Next

            Return oLineas
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    ''' <summary>Cierra una discrepancia</summary>
    ''' <param name="Factura">Id de factura</param> 
    ''' <param name="Linea">Linea de factura</param>    
    ''' <param name="IdDiscrepancia">Id discrepancia</param> 
    ''' <remarks>Llamado desde: Factura.asmx.vb</remarks>
    ''' 
    Public Sub Cerrar_Discrepancias(Optional Factura As Integer = 0, _
                                Optional Linea As Integer = 0, Optional ByVal IdDiscrepancia As Integer = 0)
        Authenticate()
        Try
            DBServer.CN_Cerrar_Discrepancia(Factura, Linea, IdDiscrepancia)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>Obtiene los datos de las discrepancias de la factura y linea dados para saber tras cerrar el pop up de discrepancias 
    ''' si hay aún discrepancias abiertas o todas están cerradas</summary>
    ''' <param name="IdFactura">Id de factura</param> 
    ''' <param name="Linea">Linea de factura</param> 
    ''' <returns>Num. de discrepancias afectadas</returns>       
    ''' <remarks>Llamado desde: Factura.asmx.vb</remarks>
    ''' 
    Public Function Comprobar_DiscrepanciasCerradas(ByVal IdFactura As Integer, ByVal Linea As Integer) As Integer
        Authenticate()
        Try
            Return DBServer.CN_Comprobar_DiscrepanciasCerradas(IdFactura, Linea)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class
