﻿Public Class cnMensajePara
    Private _Para_UON_UON0 As Boolean
    Public Property Para_UON_UON0() As Boolean
        Get
            Return _Para_UON_UON0
        End Get
        Set(ByVal value As Boolean)
            _Para_UON_UON0 = value
        End Set
    End Property

    Private _Para_UON As DataTable
    Public Property Para_UON() As DataTable
        Get
            Return _Para_UON
        End Get
        Set(ByVal value As DataTable)
            _Para_UON = value
        End Set
    End Property

    ''' <summary>
    ''' Tipo de proveedor al que dirigiremos el mensaje. 
    ''' Tipos: 1-Todos;2-Calidad(Pot+Real);3-Potenciales;4-Reales;5-de Material QA;6-de Material GS;
    ''' </summary>
    ''' <remarks></remarks>
    Private _Para_Prove_Tipo As Integer
    Public Property Para_Prove_Tipo() As Integer
        Get
            Return _Para_Prove_Tipo
        End Get
        Set(ByVal value As Integer)
            _Para_Prove_Tipo = value
        End Set
    End Property

    Private _Para_Material_GMN1 As String
    Public Property Para_Material_GMN1() As String
        Get
            Return _Para_Material_GMN1
        End Get
        Set(ByVal value As String)
            _Para_Material_GMN1 = value
        End Set
    End Property

    Private _Para_Material_GMN2 As String
    Public Property Para_Material_GMN2() As String
        Get
            Return _Para_Material_GMN2
        End Get
        Set(ByVal value As String)
            _Para_Material_GMN2 = value
        End Set
    End Property

    Private _Para_Material_GMN3 As String
    Public Property Para_Material_GMN3() As String
        Get
            Return _Para_Material_GMN3
        End Get
        Set(ByVal value As String)
            _Para_Material_GMN3 = value
        End Set
    End Property

    Private _Para_Material_GMN4 As String
    Public Property Para_Material_GMN4() As String
        Get
            Return _Para_Material_GMN4
        End Get
        Set(ByVal value As String)
            _Para_Material_GMN4 = value
        End Set
    End Property

    Private _Para_Material_QA As Nullable(Of Integer)
    Public Property Para_Material_QA() As Nullable(Of Integer)
        Get
            Return _Para_Material_QA
        End Get
        Set(ByVal value As Nullable(Of Integer))
            _Para_Material_QA = value
        End Set
    End Property

    Private _Para_Prove_Con As DataTable
    Public Property Para_Prove_Con() As DataTable
        Get
            Return _Para_Prove_Con
        End Get
        Set(ByVal value As DataTable)
            _Para_Prove_Con = value
        End Set
    End Property

    Private _Para_ProceCompra_Anyo As Nullable(Of Integer)
    Public Property Para_ProceCompra_Anyo() As Nullable(Of Integer)
        Get
            Return _Para_ProceCompra_Anyo
        End Get
        Set(ByVal value As Nullable(Of Integer))
            _Para_ProceCompra_Anyo = value
        End Set
    End Property

    Private _Para_ProceCompra_GMN1 As String
    Public Property Para_ProceCompra_GMN1() As String
        Get
            Return _Para_ProceCompra_GMN1
        End Get
        Set(ByVal value As String)
            _Para_ProceCompra_GMN1 = value
        End Set
    End Property

    Private _Para_ProceCompra_Cod As Nullable(Of Integer)
    Public Property Para_ProceCompra_Cod() As Nullable(Of Integer)
        Get
            Return _Para_ProceCompra_Cod
        End Get
        Set(ByVal value As Nullable(Of Integer))
            _Para_ProceCompra_Cod = value
        End Set
    End Property

    Private _Para_ProcesoCompra_Responsable As Boolean
    Public Property Para_ProcesoCompra_Responsable() As Boolean
        Get
            Return _Para_ProcesoCompra_Responsable
        End Get
        Set(ByVal value As Boolean)
            _Para_ProcesoCompra_Responsable = value
        End Set
    End Property

    Private _Para_ProcesoCompra_Invitados As Boolean
    Public Property Para_ProcesoCompra_Invitados() As Boolean
        Get
            Return _Para_ProcesoCompra_Invitados
        End Get
        Set(ByVal value As Boolean)
            _Para_ProcesoCompra_Invitados = value
        End Set
    End Property

    Private _Para_ProcesoCompra_Compradores As Boolean
    Public Property Para_ProcesoCompra_Compradores() As Boolean
        Get
            Return _Para_ProcesoCompra_Compradores
        End Get
        Set(ByVal value As Boolean)
            _Para_ProcesoCompra_Compradores = value
        End Set
    End Property

    Private _Para_ProcesoCompra_Proveedores As Boolean
    Public Property Para_ProcesoCompra_Proveedores() As Boolean
        Get
            Return _Para_ProcesoCompra_Proveedores
        End Get
        Set(ByVal value As Boolean)
            _Para_ProcesoCompra_Proveedores = value
        End Set
    End Property

    Private _Para_EstructuraCompras_MaterialGS As Boolean
    Public Property Para_EstructuraCompras_MaterialGS() As Boolean
        Get
            Return _Para_EstructuraCompras_MaterialGS
        End Get
        Set(ByVal value As Boolean)
            _Para_EstructuraCompras_MaterialGS = value
        End Set
    End Property

    Private _Para_EstructuraCompras_Equipo As DataTable
    Public Property Para_EstructuraCompras_Equipo() As DataTable
        Get
            Return _Para_EstructuraCompras_Equipo
        End Get
        Set(ByVal value As DataTable)
            _Para_EstructuraCompras_Equipo = value
        End Set
    End Property

    Private _Para_Grupo_EP As Boolean
    Public Property Para_Grupo_EP() As Boolean
        Get
            Return _Para_Grupo_EP
        End Get
        Set(ByVal value As Boolean)
            _Para_Grupo_EP = value
        End Set
    End Property

    Private _Para_Grupo_GS As Boolean
    Public Property Para_Grupo_GS() As Boolean
        Get
            Return _Para_Grupo_GS
        End Get
        Set(ByVal value As Boolean)
            _Para_Grupo_GS = value
        End Set
    End Property

    Private _Para_Grupo_PM As Boolean
    Public Property Para_Grupo_PM() As Boolean
        Get
            Return _Para_Grupo_PM
        End Get
        Set(ByVal value As Boolean)
            _Para_Grupo_PM = value
        End Set
    End Property

    Private _Para_Grupo_QA As Boolean
    Public Property Para_Grupo_QA() As Boolean
        Get
            Return _Para_Grupo_QA
        End Get
        Set(ByVal value As Boolean)
            _Para_Grupo_QA = value
        End Set
    End Property

    Private _Para_Grupo_SM As Boolean
    Public Property Para_Grupo_SM() As Boolean
        Get
            Return _Para_Grupo_SM
        End Get
        Set(ByVal value As Boolean)
            _Para_Grupo_SM = value
        End Set
    End Property

    Private _Para_Grupos_Grupos As DataTable
    Public Property Para_Grupos_Grupos() As DataTable
        Get
            Return _Para_Grupos_Grupos
        End Get
        Set(ByVal value As DataTable)
            _Para_Grupos_Grupos = value
        End Set
    End Property
End Class
