﻿Public Class cn_Data
    Inherits Security
    ''' <summary>
    ''' Constructor de la clase cnMensajes
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si está autenticado</param>
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="UsuCod"></param>
    ''' <param name="Idioma"></param>
    ''' <param name="UON1"></param>
    ''' <param name="UON2"></param>
    ''' <param name="UON3"></param>
    ''' <param name="DEP"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Obtener_UONsPara(ByVal UsuCod As String, ByVal Idioma As String, ByVal UON1 As String, _
                                     ByVal UON2 As String, ByVal UON3 As String, ByVal DEP As String, _
                                     ByVal Restr_UON As Boolean, ByVal Restr_Dep As Boolean) As List(Of cn_fsTreeViewItem)
        Authenticate()

        Dim dsDatos As New DataSet
        dsDatos = DBServer.CN_Obtener_UONsPara(UsuCod, Idioma, UON1, UON2, UON3, DEP, Restr_UON, Restr_Dep)

        Dim oUONPara As New List(Of cn_fsTreeViewItem)
        Dim item0, item1, item2, item3, item4, item5 As cn_fsTreeViewItem
        ' UON0 - LA EMPRESA
        item0 = New cn_fsTreeViewItem

        With item0
            .value = dsDatos.Tables(0).Rows(0)("COD")
            .text = dsDatos.Tables(0).Rows(0)("DEN")
            .selectable = dsDatos.Tables(0).Rows(0)("PERMISO")
            .type = IIf(.selectable, 3, 0)
            .nivel = "UON0"
            .expanded = True

            'UON1 Y UON0_DEP
            .children = New List(Of cn_fsTreeViewItem)
            With .children
                For Each row1 As DataRow In dsDatos.Tables(2).Rows
                    item1 = New cn_fsTreeViewItem

                    With item1
                        .value = row1("COD")
                        .text = row1("COD") & " - " & row1("DEN")
                        .selectable = row1("PERMISO")
                        .type = IIf(.selectable, 3, 0)

                        If row1("ESDEPARTAMENTO") Then
                            .nivel = "DEP"
                            .expanded = False
                            .children = New List(Of cn_fsTreeViewItem)
                            With .children
                                For Each USU1 As DataRow In dsDatos.Tables(1).Select("UON1 IS NULL AND UON2 IS NULL AND UON3 IS NULL AND DEP='" & row1("COD") & "'")
                                    item2 = New cn_fsTreeViewItem

                                    With item2
                                        .value = USU1("COD")
                                        .text = USU1("NOM") & " " & USU1("APE") & IIf(USU1("EMAIL") Is DBNull.Value, "", " (" & USU1("EMAIL") & ")")
                                        .selectable = 1
                                        .type = 3
                                        .nivel = "USU"
                                    End With
                                    .Add(item2)
                                Next
                            End With
                        Else
                            .nivel = "UON1"
                            .expanded = False
                            'UON2 Y UON1_DEP
                            .children = New List(Of cn_fsTreeViewItem)
                            With .children
                                For Each row2 As DataRow In dsDatos.Tables(3).Select("UON1='" & row1("COD") & "'")
                                    item2 = New cn_fsTreeViewItem

                                    With item2
                                        .value = row2("COD")
                                        .text = row2("COD") & " - " & row2("DEN")
                                        .selectable = row2("PERMISO")
                                        .type = IIf(.selectable, 3, 0)

                                        If row2("ESDEPARTAMENTO") Then
                                            .nivel = "DEP"
                                            .expanded = False
                                            .children = New List(Of cn_fsTreeViewItem)
                                            With .children
                                                For Each USU2 As DataRow In dsDatos.Tables(1).Select("UON1='" & row1("COD") & "' AND UON2 IS NULL AND UON3 IS NULL AND DEP='" & row2("COD") & "'")
                                                    item3 = New cn_fsTreeViewItem

                                                    With item3
                                                        .value = USU2("COD")
                                                        .text = USU2("NOM") & " " & USU2("APE") & IIf(USU2("EMAIL") Is DBNull.Value, "", " (" & USU2("EMAIL") & ")")
                                                        .selectable = 1
                                                        .type = 3
                                                        .nivel = "USU"
                                                    End With
                                                    .Add(item3)
                                                Next
                                            End With
                                        Else
                                            .nivel = "UON2"
                                            .expanded = False
                                            'UON3 Y UON2_DEP
                                            .children = New List(Of cn_fsTreeViewItem)
                                            With .children
                                                For Each row3 As DataRow In dsDatos.Tables(4).Select("UON1='" & row1("COD") & "' AND UON2='" & row2("COD") & "'")
                                                    item3 = New cn_fsTreeViewItem

                                                    With item3
                                                        .value = row3("COD")
                                                        .text = row3("COD") & " - " & row3("DEN")
                                                        .selectable = row3("PERMISO")
                                                        .type = IIf(.selectable, 3, 0)

                                                        If row3("ESDEPARTAMENTO") Then
                                                            .nivel = "DEP"
                                                            .expanded = False
                                                            .children = New List(Of cn_fsTreeViewItem)
                                                            With .children
                                                                For Each USU3 As DataRow In dsDatos.Tables(1).Select("UON1='" & row1("COD") & "' AND UON2='" & row2("COD") & "' AND UON3 IS NULL AND DEP='" & row3("COD") & "'")
                                                                    item4 = New cn_fsTreeViewItem

                                                                    With item4
                                                                        .value = USU3("COD")
                                                                        .text = USU3("NOM") & " " & USU3("APE") & IIf(USU3("EMAIL") Is DBNull.Value, "", " (" & USU3("EMAIL") & ")")
                                                                        .selectable = 1
                                                                        .type = 3
                                                                        .nivel = "USU"
                                                                    End With
                                                                    .Add(item4)
                                                                Next
                                                            End With
                                                        Else
                                                            .nivel = "UON3"
                                                            .expanded = False
                                                            'UON3_DEP
                                                            .children = New List(Of cn_fsTreeViewItem)
                                                            With .children
                                                                For Each row4 As DataRow In dsDatos.Tables(5).Select("UON1='" & row1("COD") & "' AND UON2='" & row2("COD") & "' AND UON3='" & row3("COD") & "'")
                                                                    item4 = New cn_fsTreeViewItem

                                                                    With item4
                                                                        .value = row4("COD")
                                                                        .text = row4("COD") & " - " & row4("DEN")
                                                                        .selectable = row4("PERMISO")
                                                                        .type = IIf(.selectable, 3, 0)
                                                                        .nivel = "DEP"
                                                                        .expanded = False

                                                                        .children = New List(Of cn_fsTreeViewItem)
                                                                        With .children
                                                                            For Each USU4 As DataRow In dsDatos.Tables(1).Select("UON1='" & row1("COD") & "' AND UON2='" & row2("COD") & "' AND UON3='" & row3("COD") & "' AND DEP='" & row4("COD") & "'")
                                                                                item5 = New cn_fsTreeViewItem

                                                                                With item5
                                                                                    .value = USU4("COD")
                                                                                    .text = USU4("NOM") & " " & USU4("APE") & IIf(USU4("EMAIL") Is DBNull.Value, "", " (" & USU4("EMAIL") & ")")
                                                                                    .selectable = 1
                                                                                    .type = 3
                                                                                    .nivel = "USU"
                                                                                End With
                                                                                .Add(item5)
                                                                            Next
                                                                        End With
                                                                    End With
                                                                    .Add(item4)
                                                                Next
                                                            End With
                                                        End If
                                                    End With
                                                    .Add(item3)
                                                Next
                                            End With
                                        End If
                                    End With
                                    .Add(item2)
                                Next
                            End With
                        End If
                    End With
                    .Add(item1)
                Next
            End With
        End With
        oUONPara.Add(item0)

        Return oUONPara
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="UsuCod"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Obtener_ProceCompras_Anyos(ByVal UsuCod As String, ByVal UON1 As String, ByVal UON2 As String, ByVal UON3 As String, _
                                             ByVal Dep As String, ByVal Equipo As String, ByVal RestricMat As Boolean, _
                                             ByVal RestricAsigUsu As Boolean, ByVal RestricAsigEqpUsu As Boolean, _
                                             ByVal RestricUsuDep As Boolean, ByVal RestricUsuUO As Boolean, _
                                             ByVal RestricUsuAbto As Boolean, ByVal RestricUsuResponsable As Boolean, _
                                             ByVal RestricPerfUsuUO As Boolean) As List(Of cn_fsItem)
        Authenticate()

        Dim dtDatos As DataTable
        Dim oAniosProceCompras As New List(Of cn_fsItem)
        Dim item As cn_fsItem

        dtDatos = DBServer.CN_Obtener_ProcesoCompra_Anyos(UsuCod, UON1, UON2, UON3, Dep, Equipo, RestricMat, RestricAsigUsu, RestricAsigEqpUsu, _
                                            RestricUsuDep, RestricUsuUO, RestricUsuAbto, RestricUsuResponsable, RestricPerfUsuUO)
        For Each row As DataRow In dtDatos.Rows
            item = New cn_fsItem
            item.value = row("ANYO")
            item.text = row("ANYO")

            oAniosProceCompras.Add(item)
        Next

        Return oAniosProceCompras
    End Function
    ''' <summary>Devuelve los diferentes códigos GMN1 de los procesos de compra que puede consultar el usuario</summary>
    ''' <param name="UsuCod">Código del usuario</param>
    ''' <param name="Idioma">Código del idioma</param>
    ''' <param name="UON1"> Código UON1 al que pertenece el usuario</param>
    ''' <param name="UON2">Código UON2 al que pertenece el usuario</param>
    ''' <param name="UON3">Código UON3 al que pertenece el usuario</param>
    ''' <param name="Dep">Código DEP al que pertenece el usuario</param>
    ''' <param name="Equipo">Código del equipo al que pertenece el usuario</param>
    ''' <param name="RestricMat">Si el usuario tiene restricción al material de usuario</param>
    ''' <param name="RestricAsigUsu">Si el usuario tiene restricción a los compradores asignados</param>
    ''' <param name="RestricAsigEqpUsu">Si el usuario tiene restricción al equipo asignado</param>
    ''' <param name="RestricUsuDep">Si el usuario tiene restricción al departamento de usuario</param>
    ''' <param name="RestricUsuUO">Si el usuario tiene restricción a la uon de usuario</param>
    ''' <param name="RestricUsuAbto">Si el usuario tiene restricción a los procesos abiertos por el usuario</param>
    ''' <param name="RestricUsuResponsable">Si el usuario tiene restricción a los procesos en los que el usuario es responsable</param>
    ''' <param name="RestricPerfUsuUO">Si el usuario tiene restricción a las uon del perfil de usuario</param>
    ''' <param name="Anyo"> El año por el que filtraremos la busqueda</param>    
    ''' <returns>Lista de cn_fsItem con los datos</returns>
    ''' <revision>LTG 26/08/2013</revision>
    ''' 
    Public Function Obtener_ProceCompras_GMNs(ByVal UsuCod As String, ByVal Idioma As String, ByVal UON1 As String, ByVal UON2 As String, ByVal UON3 As String, _
                                             ByVal Dep As String, ByVal Equipo As String, ByVal RestricMat As Boolean, _
                                             ByVal RestricAsigUsu As Boolean, ByVal RestricAsigEqpUsu As Boolean, _
                                             ByVal RestricUsuDep As Boolean, ByVal RestricUsuUO As Boolean, _
                                             ByVal RestricUsuAbto As Boolean, ByVal RestricUsuResponsable As Boolean, ByVal RestricPerfUsuUO As Boolean, _
                                             ByVal Anyo As Integer) As List(Of cn_fsItem)
        Authenticate()

        Dim dtDatos As DataTable
        Dim oAniosProceCompras As New List(Of cn_fsItem)
        Dim item As cn_fsItem

        dtDatos = DBServer.CN_Obtener_ProcesoCompra_GMNs(UsuCod, Idioma, UON1, UON2, UON3, Dep, Equipo, RestricMat, RestricAsigUsu, RestricAsigEqpUsu, _
                                            RestricUsuDep, RestricUsuUO, RestricUsuAbto, RestricUsuResponsable, RestricPerfUsuUO, Anyo)
        For Each row As DataRow In dtDatos.Rows
            item = New cn_fsItem
            item.value = row("GMN1")
            item.text = row("GMN1") & " - " & row("DEN")

            oAniosProceCompras.Add(item)
        Next

        Return oAniosProceCompras
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="UsuCod"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Obtener_ProceCompras_CodDen(ByVal UsuCod As String, ByVal UON1 As String, ByVal UON2 As String, ByVal UON3 As String, _
                                             ByVal Dep As String, ByVal Equipo As String, ByVal RestricMat As Boolean, _
                                             ByVal RestricAsigUsu As Boolean, ByVal RestricAsigEqpUsu As Boolean, _
                                             ByVal RestricUsuDep As Boolean, ByVal RestricUsuUO As Boolean, _
                                             ByVal RestricUsuAbto As Boolean, ByVal RestricUsuResponsable As Boolean, ByVal RestricPerfUsuUO As Boolean, _
                                             ByVal Anyo As Integer, ByVal GMN1 As String, Optional ByVal Cod As Integer = 0) As List(Of cn_fsItem)
        Authenticate()

        Dim dtDatos As DataTable
        Dim oAniosProceCompras As New List(Of cn_fsItem)
        Dim item As cn_fsItem

        dtDatos = DBServer.CN_Obtener_ProcesoCompra_CodDen(UsuCod, UON1, UON2, UON3, Dep, Equipo, RestricMat, RestricAsigUsu, RestricAsigEqpUsu, _
                                            RestricUsuDep, RestricUsuUO, RestricUsuAbto, RestricUsuResponsable, RestricPerfUsuUO, Anyo, GMN1, Cod)
        For Each row As DataRow In dtDatos.Rows
            item = New cn_fsItem
            item.value = row("COD")
            item.text = row("COD") & " - " & row("DEN")

            oAniosProceCompras.Add(item)
        Next

        Return oAniosProceCompras
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="UsuCod"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ProveedoresContacto_Permiso_EmitirMensajeUsuario(ByVal UsuCod As String, ByVal EquipoUsu As String, ByVal RestriccionEnvioProveedoresMaterialUsu As Boolean, _
                                               ByVal RestriccionEnvioProveedoresEquipoComprasUsu As Boolean, ByVal RestriccionEnvioProveedoresConContactoCalidad As Boolean, _
                                               ByVal CodigoBuscado As String, ByVal NIFBuscado As String, ByVal DenominacionBuscada As String) As List(Of cn_fsItem)
        Authenticate()

        Dim dtDatos As DataTable
        Dim oProveedoresContactos As New List(Of cn_fsItem)
        Dim iProveedorContacto As cn_fsItem

        dtDatos = DBServer.CN_ProveedoresContacto_Permiso_EmitirMensajeUsuario(UsuCod, EquipoUsu, RestriccionEnvioProveedoresMaterialUsu, RestriccionEnvioProveedoresEquipoComprasUsu, _
                                               RestriccionEnvioProveedoresConContactoCalidad, CodigoBuscado, NIFBuscado, DenominacionBuscada)
        For Each row As DataRow In dtDatos.Rows
            iProveedorContacto = New cn_fsItem
            With iProveedorContacto
                .value = row("PROVE") & "-" & row("ID")
                .text = row("PROVE") & " - " & row("DEN") & " (" & row("EMAIL").ToString & ")"
            End With
            oProveedoresContactos.Add(iProveedorContacto)
        Next

        Return oProveedoresContactos
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="UsuCod"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GruposCorporativos_Permiso_EmitirMensajeUsuario(ByVal UsuCod As String, ByVal Idi As String, _
                                                                    ByVal UON1 As String, ByVal UON2 As String, ByVal UON3 As String, _
                                                                    ByVal DEP As String) As List(Of cn_fsItem)
        Authenticate()

        Dim dtDatos As DataTable
        Dim oGruposCorporativos As New List(Of cn_fsItem)
        Dim iGrupoCorporativo As cn_fsItem

        dtDatos = DBServer.CN_GruposCorporativos_Permiso_EmitirMensajeUsuario(UsuCod, Idi, UON1, UON2, UON3, DEP)
        For Each row As DataRow In dtDatos.Rows
            iGrupoCorporativo = New cn_fsItem
            With iGrupoCorporativo
                .value = row("ID")
                .text = row("DEN")
            End With
            oGruposCorporativos.Add(iGrupoCorporativo)
        Next

        Return oGruposCorporativos
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="UsuCod"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function EquiposCompra_Permiso_EmitirMensajeUsuario(ByVal UsuCod As String, ByVal EquipoUsuario As String, _
                                                               ByVal RestriccionEquipoUsuario As Boolean) As List(Of cn_fsTreeViewItem)
        Authenticate()

        Dim dtDatos As DataSet
        Dim oEquiposCompra As New List(Of cn_fsTreeViewItem)
        Dim iEquipoCompra As cn_fsTreeViewItem

        dtDatos = DBServer.CN_EquiposCompra_Permiso_EmitirMensajeUsuario(UsuCod, EquipoUsuario, RestriccionEquipoUsuario)
        For Each row As DataRow In dtDatos.Tables("EQUIPOS").Rows
            iEquipoCompra = New cn_fsTreeViewItem
            With iEquipoCompra
                .value = row("COD")
                .text = row("DEN")
                .nivel = 0
                .expanded = False
                .selectable = True
                .type = 4
                .children = New List(Of cn_fsTreeViewItem)
                .children = Obtener_PersonasEquipoCompra(row("COD"), dtDatos.Tables("PERSONAS"))
            End With
            oEquiposCompra.Add(iEquipoCompra)
        Next

        Return oEquiposCompra
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="CodEqp"></param>
    ''' <param name="tPersonas"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function Obtener_PersonasEquipoCompra(ByVal CodEqp As String, ByVal tPersonas As DataTable) As List(Of cn_fsTreeViewItem)
        Dim Personas As New List(Of cn_fsTreeViewItem)
        Dim Persona As cn_fsTreeViewItem

        For Each row As DataRow In tPersonas.Select("EQP='" & CodEqp & "'")
            Persona = New cn_fsTreeViewItem
            With Persona
                .value = row("COD")
                .text = IIf(row("NOM") Is DBNull.Value, row("APE"), row("NOM") & " " & row("APE"))
                .nivel = 1
                .expanded = True
                .selectable = True
                .type = 4
            End With
            Personas.Add(Persona)
        Next

        Return Personas
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Obtener_BuscadorEntidad_Tipos(ByVal UsuCod As String, ByVal Idioma As String, ByVal TipoEntidad As Integer) As List(Of cn_fsItem)
        Authenticate()

        Dim dtDatos As DataTable
        Dim oTiposNoConformidad As New List(Of cn_fsItem)
        Dim iTipoNoConformidad As cn_fsItem

        dtDatos = DBServer.CN_Obtener_BuscadorEntidad_Tipos(UsuCod, Idioma, TipoEntidad)
        For Each row As DataRow In dtDatos.Rows
            iTipoNoConformidad = New cn_fsItem
            With iTipoNoConformidad
                .value = row("ID")
                .text = row("COD") & " - " & row("DEN")
            End With
            oTiposNoConformidad.Add(iTipoNoConformidad)
        Next

        Return oTiposNoConformidad
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="UsuCod"></param>
    ''' <param name="TipoEntidad"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Obtener_BuscadorEntidad_Proveedores(ByVal UsuCod As String, _
            ByVal RestricProvMat As Boolean, ByVal RestricProvEqp As Boolean, ByVal RestricProvCon As Boolean, _
            Optional ByVal TipoEntidad As Integer = 0, Optional ByVal ProveCod As String = "") As List(Of cn_fsItem)
        Authenticate()

        Dim dtDatos As DataTable
        Dim oProveedoresNoConformidad As New List(Of cn_fsItem)
        Dim iProveedorNoConformidad As cn_fsItem

        dtDatos = DBServer.CN_Obtener_BuscadorEntidad_Proveedores(UsuCod, TipoEntidad, ProveCod, _
                            RestricProvMat, RestricProvEqp, RestricProvCon)
        For Each row As DataRow In dtDatos.Rows
            iProveedorNoConformidad = New cn_fsItem
            With iProveedorNoConformidad
                .value = row("COD")
                .text = row("COD") & " - " & row("DEN")
            End With
            oProveedoresNoConformidad.Add(iProveedorNoConformidad)
        Next

        Return oProveedoresNoConformidad
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="UsuCod"></param>
    ''' <param name="Idioma"></param>
    ''' <param name="TipoEntidad"></param>
    ''' <param name="TipoSolicitud"></param>
    ''' <param name="ProveCod"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Obtener_BuscadorEntidad_Identificador(ByVal UsuCod As String, ByVal Idioma As String, _
        ByVal TipoEntidad As Integer, ByVal TipoSolicitud As Integer, ByVal ProveCod As String, _
        ByVal RestricSolicUsu As Boolean, ByVal RestricSolicUO As Boolean, ByVal RestricSolicDep As Boolean, _
        ByVal RestricProvMat As Boolean, ByVal RestricProvEqp As Boolean, ByVal RestricProvCon As Boolean) As List(Of cn_fsItem)
        Authenticate()

        Dim dtDatos As DataTable
        Dim oIdentificadoresNoConformidad As New List(Of cn_fsItem)
        Dim iIdentificadorNoConformidad As cn_fsItem

        dtDatos = DBServer.CN_Obtener_BuscadorEntidad_Identificador(UsuCod, Idioma, TipoEntidad, TipoSolicitud, ProveCod, _
                    RestricSolicUsu, RestricSolicUO, RestricSolicDep, RestricProvMat, RestricProvEqp, RestricProvCon)
        For Each row As DataRow In dtDatos.Rows
            iIdentificadorNoConformidad = New cn_fsItem
            With iIdentificadorNoConformidad
                .value = row("INSTANCIA")
                .text = row("INSTANCIA") & " - " & row("EXT_TITULO")
            End With
            oIdentificadoresNoConformidad.Add(iIdentificadorNoConformidad)
        Next

        Return oIdentificadoresNoConformidad
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="UsuCod"></param>
    ''' <param name="Idioma"></param>
    ''' <param name="TipoEntidad"></param>
    ''' <param name="Identificador"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Comprobar_BuscadorEntidad_Identificador(ByVal UsuCod As String, ByVal Idioma As String, _
            ByVal TipoEntidad As Integer, ByVal Identificador As Integer, _
            ByVal RestricSolicUsu As Boolean, ByVal RestricSolicUO As Boolean, ByVal RestricSolicDep As Boolean, _
            ByVal RestricProvMat As Boolean, ByVal RestricProvEqp As Boolean, ByVal RestricProvCon As Boolean, _
            Optional ByVal ComprobarEnlace As Boolean = False) As cn_fsItem
        Authenticate()

        Dim dtDatos As DataTable
        dtDatos = DBServer.CN_Comprobar_BuscadorEntidad_Identificador(UsuCod, Idioma, TipoEntidad, Identificador, _
            RestricSolicUsu, RestricSolicUO, RestricSolicDep, RestricProvMat, RestricProvEqp, RestricProvCon)

        If dtDatos.Rows.Count = 0 Then
            Return Nothing
        Else
            Dim iEntidad As New cn_fsItem
            With iEntidad
                If ComprobarEnlace Then
                    If TipoEntidad = 2 OrElse TipoEntidad = 3 Then
                        .value = dtDatos.Rows(0)("ID")
                    Else
                        .value = dtDatos.Rows(0)("INSTANCIA")
                    End If
                Else
                    .value = dtDatos.Rows(0)("INSTANCIA")
                End If
                If TipoEntidad = 5 Then
                    .text = dtDatos.Rows(0)("CODIGO") & " - " & dtDatos.Rows(0)("DEN")
                Else
                    .text = dtDatos.Rows(0)("INSTANCIA") & " - " & dtDatos.Rows(0)("DEN")
                End If
            End With

            Return iEntidad
        End If
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="UsuCod"></param>
    ''' <param name="CodProve"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Comprobar_Buscador_Proveedor(ByVal UsuCod As String, ByVal CodProve As String, _
            ByVal RestricProvMat As Boolean, ByVal RestricProvEqp As Boolean, ByVal RestricProvCon As Boolean) As cn_fsItem
        Authenticate()

        Dim dtDatos As DataTable
        dtDatos = DBServer.CN_Obtener_BuscadorEntidad_Proveedores(UsuCod, 0, CodProve, RestricProvMat, RestricProvEqp, RestricProvCon)

        If dtDatos.Rows.Count = 0 Then
            Return Nothing
        Else
            Dim iProveedor As New cn_fsItem
            With iProveedor
                .value = dtDatos.Rows(0)("COD")
                .text = dtDatos.Rows(0)("COD") & " - " & dtDatos.Rows(0)("DEN")
            End With

            Return iProveedor
        End If
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="Idi"></param>
    ''' <param name="Pyme"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Obtener_MaterialesQA_Proveedor(ByVal Idi As String, ByVal Pyme As Integer) As List(Of cn_fsItem)
        Authenticate()

        Dim dtDatos As DataTable
        dtDatos = DBServer.CN_Obtener_MaterialesQA(Idi, Pyme)

        If dtDatos.Rows.Count = 0 Then
            Return Nothing
        Else
            Dim oMaterialesQA As New List(Of cn_fsItem)
            Dim iMaterial As cn_fsItem
            For Each row As DataRow In dtDatos.Rows
                iMaterial = New cn_fsItem
                With iMaterial
                    .value = row("ID")
                    .text = row("DEN_" & Idi).ToString
                End With
                oMaterialesQA.Add(iMaterial)
            Next

            Return oMaterialesQA
        End If
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="Idi"></param>
    ''' <param name="Pyme"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Obtener_MaterialesGS_Proveedor(ByVal Usu As String, ByVal Idi As String, _
                                ByVal Pyme As Integer, ByVal Restr_Prove_Mat As Boolean) As List(Of cn_fsTreeViewItem)
        Authenticate()

        Dim dsDatos As DataSet
        dsDatos = DBServer.CN_Obtener_MaterialesGS(Usu, Idi, Pyme, Restr_Prove_Mat)

        Dim oMaterialesGS As New List(Of cn_fsTreeViewItem)
        Dim iGMN1, iGMN2, iGMN3, iGMN4 As cn_fsTreeViewItem

        For Each iMaterial1 As DataRow In dsDatos.Tables(0).Rows
            iGMN1 = New cn_fsTreeViewItem
            With iGMN1
                .nivel = "GMN1"
                .expanded = False
                .selectable = CType(iMaterial1("SELECTABLE"), Boolean)
                .type = IIf(.selectable, 3, 0)
                .value = iMaterial1("COD").ToString
                .text = iMaterial1("DEN").ToString
                .children = New List(Of cn_fsTreeViewItem)
                With .children
                    For Each iMaterial2 As DataRow In iMaterial1.GetChildRows("REL_NIV1_NIV2")
                        iGMN2 = New cn_fsTreeViewItem
                        With iGMN2
                            .nivel = "GMN2"
                            .expanded = False
                            .selectable = CType(iMaterial2("SELECTABLE"), Boolean)
                            .type = IIf(.selectable, 3, 0)
                            .value = iMaterial2("COD").ToString
                            .text = iMaterial2("DEN").ToString
                            .children = New List(Of cn_fsTreeViewItem)
                            With .children
                                For Each iMaterial3 As DataRow In iMaterial2.GetChildRows("REL_NIV2_NIV3")
                                    iGMN3 = New cn_fsTreeViewItem
                                    With iGMN3
                                        .nivel = "GMN3"
                                        .expanded = False
                                        .selectable = CType(iMaterial3("SELECTABLE"), Boolean)
                                        .type = IIf(.selectable, 3, 0)
                                        .value = iMaterial3("COD").ToString
                                        .text = iMaterial3("DEN").ToString
                                        .children = New List(Of cn_fsTreeViewItem)
                                        With .children
                                            For Each iMaterial4 As DataRow In iMaterial3.GetChildRows("REL_NIV3_NIV4")
                                                iGMN4 = New cn_fsTreeViewItem
                                                With iGMN4
                                                    .nivel = "GMN4"
                                                    .expanded = False
                                                    .selectable = CType(iMaterial4("SELECTABLE"), Boolean)
                                                    .type = IIf(.selectable, 3, 0)
                                                    .value = iMaterial4("COD").ToString
                                                    .text = iMaterial4("DEN").ToString
                                                End With
                                                .Add(iGMN4)
                                            Next
                                        End With
                                    End With
                                    .Add(iGMN3)
                                Next
                            End With
                        End With
                        .Add(iGMN2)
                    Next
                End With
            End With
            oMaterialesGS.Add(iGMN1)
        Next
        Return oMaterialesGS
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="Usu"></param>
    ''' <param name="Idi"></param>
    ''' <param name="IdMensaje"></param>
    ''' <param name="IdCategoria"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Obtener_DatosAdicionales_Mensaje(ByVal Usu As String, ByVal Idi As String, _
                ByVal IdMensaje As Nullable(Of Integer), ByVal IdCategoria As Nullable(Of Integer), _
                ByVal UON1 As String, ByVal UON2 As String, ByVal UON3 As String, ByVal DEP As String, _
                ByVal Restr_UON As Boolean, ByVal Restr_Dep As Boolean, _
                ByVal EqpUsu As String, ByVal Restr_Eqp As Boolean) As List(Of Object)
        Authenticate()

        Dim dsDatos As DataSet
        dsDatos = DBServer.CN_Obtener_DatosAdicionales_Mensaje(Usu, Idi, IdMensaje, IdCategoria, _
                                                                UON1, UON2, UON3, DEP, Restr_UON, Restr_Dep,
                                                                EqpUsu, Restr_Eqp)

        Dim oInfoDatosAdicionales As New List(Of Object)
        Dim oCategoria As New cnCategoria
        With oCategoria
            .Id = IIf(IsDBNull(dsDatos.Tables(0).Rows(0)("IDCATEGORIA")), 0, dsDatos.Tables(0).Rows(0)("IDCATEGORIA"))
        End With

        Dim oParaPorCategoria As New List(Of cn_fsItem)
        Dim iPara As cn_fsItem

        For Each row As DataRow In dsDatos.Tables("PARAUON").Rows
            iPara = New cn_fsItem
            With iPara
                .value = "itemParaUON" & _
                    IIf(IsDBNull(row("UON0")), "", "-UON0_" & row("UON0")) & _
                    IIf(IsDBNull(row("UON1")), "", "-UON1_" & row("UON1")) & _
                    IIf(IsDBNull(row("UON2")), "", "-UON2_" & row("UON2")) & _
                    IIf(IsDBNull(row("UON3")), "", "-UON3_" & row("UON3")) & _
                    IIf(IsDBNull(row("DEP")), "", "-DEP_" & row("DEP")) & _
                    IIf(IsDBNull(row("USU")), "", "-USU_" & row("USU"))
                .text = IIf(IsDBNull(row("UON1")), "", row("UON1") & "-") & _
                    IIf(IsDBNull(row("UON2")), "", row("UON2") & "-") & _
                    IIf(IsDBNull(row("UON3")), "", row("UON3") & "-") & _
                    IIf(IsDBNull(row("DEP")), "", row("DEP") & "-") & row("DEN").ToString
            End With
            oParaPorCategoria.Add(iPara)
        Next

        For Each row As DataRow In dsDatos.Tables("PARAPROVE").Rows
            iPara = New cn_fsItem
            With iPara
                Select Case row("PROVEPARA")
                    Case 1
                        .value = "itemParaProve-Todos"
                    Case 2
                        .value = "itemParaProve-Calidad"
                    Case 3
                        .value = "itemParaProve-Potenciales"
                    Case 4
                        .value = "itemParaProve-Reales"
                    Case 5
                        .value = "itemParaProve-MatQA-" & row("MATERIALES_QA")
                    Case 6
                        .value = "itemParaProve-MatGS" & IIf(IsDBNull(row("GMN1")), "", "-" & row("GMN1")) & _
                            IIf(IsDBNull(row("GMN2")), "", "-" & row("GMN2")) & _
                            IIf(IsDBNull(row("GMN3")), "", "-" & row("GMN3")) & _
                            IIf(IsDBNull(row("GMN4")), "", "-" & row("GMN4"))
                    Case Else
                        .value = "itemParaProve-Uno-" & row("PROVECOD") & "-" & row("PROVECON")
                End Select
                .text = IIf(IsDBNull(row("GMN1")), "", "-" & row("GMN1")) & _
                            IIf(IsDBNull(row("GMN2")), "", "-" & row("GMN2")) & _
                            IIf(IsDBNull(row("GMN3")), "", "-" & row("GMN3")) & _
                            IIf(IsDBNull(row("GMN4")), "", "-" & row("GMN4")) & row("DEN").ToString
            End With
            oParaPorCategoria.Add(iPara)
        Next

        For Each row As DataRow In dsDatos.Tables("PARAPROCECOMPRAS").Rows
            iPara = New cn_fsItem
            With iPara
                If row("PROCEPARARESP") AndAlso row("PROCEPARAINVITADO") _
                    AndAlso row("PROCEPARACOMP") AndAlso row("PROCEPARAPROVE") Then
                    .value = "itemParaProceCompras-Todos-" & row("PROCE_ANYO") & "-" & row("PROCE_GMN1") & "-" & row("PROCE_COD")
                ElseIf row("PROCEPARARESP") Then
                    .value = "itemParaProceCompras-Responsable-" & row("PROCE_ANYO") & "-" & row("PROCE_GMN1") & "-" & row("PROCE_COD")
                ElseIf row("PROCEPARAINVITADO") Then
                    .value = "itemParaProceCompras-Invitados-" & row("PROCE_ANYO") & "-" & row("PROCE_GMN1") & "-" & row("PROCE_COD")
                ElseIf row("PROCEPARACOMP") Then
                    .value = "itemParaProceCompras-Compradores-" & row("PROCE_ANYO") & "-" & row("PROCE_GMN1") & "-" & row("PROCE_COD")
                ElseIf row("PROCEPARAPROVE") Then
                    .value = "itemParaProceCompras-Proveedores-" & row("PROCE_ANYO") & "-" & row("PROCE_GMN1") & "-" & row("PROCE_COD")
                End If

                .text = ""
            End With
            oParaPorCategoria.Add(iPara)
        Next

        For Each row As DataRow In dsDatos.Tables("PARAESTRUCCOMPRAS").Rows
            iPara = New cn_fsItem
            With iPara
                If IsDBNull(row("EQP")) Then
                    .value = "itemParaEstrucCompras-MatGS" & IIf(IsDBNull(row("GMN1")), "", "-" & row("GMN1")) & _
                            IIf(IsDBNull(row("GMN2")), "", "-" & row("GMN2")) & _
                            IIf(IsDBNull(row("GMN3")), "", "-" & row("GMN3")) & _
                            IIf(IsDBNull(row("GMN4")), "", "-" & row("GMN4"))
                Else
                    .value = "itemParaEstrucCompras-Eqp-0_" & row("EQP")
                End If

                .text = IIf(IsDBNull(row("GMN1")), "", "-" & row("GMN1")) & _
                            IIf(IsDBNull(row("GMN2")), "", "-" & row("GMN2")) & _
                            IIf(IsDBNull(row("GMN3")), "", "-" & row("GMN3")) & _
                            IIf(IsDBNull(row("GMN4")), "", "-" & row("GMN4")) & row("DEN").ToString
            End With
            oParaPorCategoria.Add(iPara)
        Next

        For Each row As DataRow In dsDatos.Tables("PARAGRUPOS").Rows
            If Not IsDBNull(row("GRUPO")) Then
                iPara = New cn_fsItem
                iPara.value = "itemParaGrupo-Uno-" & row("GRUPO")
                iPara.text = row("DEN").ToString
                oParaPorCategoria.Add(iPara)
            Else
                If row("USUEPPARA") Then
                    iPara = New cn_fsItem
                    iPara.value = "itemParaGrupo-EP"
                    iPara.text = row("DEN").ToString
                    oParaPorCategoria.Add(iPara)
                End If
                If row("USUGSPARA") Then
                    iPara = New cn_fsItem
                    iPara.value = "itemParaGrupo-GS"
                    iPara.text = row("DEN").ToString
                    oParaPorCategoria.Add(iPara)
                End If
                If row("USUPMPARA") Then
                    iPara = New cn_fsItem
                    iPara.value = "itemParaGrupo-PM"
                    iPara.text = row("DEN").ToString
                    oParaPorCategoria.Add(iPara)
                End If
                If row("USUQAPARA") Then
                    iPara = New cn_fsItem
                    iPara.value = "itemParaGrupo-QA"
                    iPara.text = row("DEN").ToString
                    oParaPorCategoria.Add(iPara)
                End If
                If row("USUSMPARA") Then
                    iPara = New cn_fsItem
                    iPara.value = "itemParaGrupo-SM"
                    iPara.text = row("DEN").ToString
                    oParaPorCategoria.Add(iPara)
                End If
            End If
        Next

        With oInfoDatosAdicionales
            .Add(oCategoria)
            .Add(oParaPorCategoria)
        End With

        Return oInfoDatosAdicionales
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="Usu"></param>
    ''' <param name="Idi"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Obtener_BuscadorSolicitudes_Tipos(ByVal Usu As String, ByVal Idi As String) As List(Of cn_fsItem)
        Authenticate()

        Dim dtDatos As DataTable
        dtDatos = DBServer.CN_Obtener_BuscadorInstancia_Usu(Usu, Idi, TipoInfo:=1)

        Dim oTiposSolicitud As New List(Of cn_fsItem)
        Dim iTipoSolicitud As cn_fsItem
        For Each row As DataRow In dtDatos.Rows
            iTipoSolicitud = New cn_fsItem
            With iTipoSolicitud
                .value = row("ID")
                .text = row("DEN").ToString
            End With
            oTiposSolicitud.Add(iTipoSolicitud)
        Next

        Return oTiposSolicitud
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="Usu"></param>
    ''' <param name="Idi"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Obtener_BuscadorSolicitudes_Identificador(ByVal Usu As String, ByVal Idi As String, _
                                                                  ByVal TipoSolicitud As Integer) As List(Of cn_fsItem)
        Authenticate()

        Dim dtDatos As DataTable
        dtDatos = DBServer.CN_Obtener_BuscadorInstancia_Usu(Usu, Idi, TipoSolicitud:=TipoSolicitud)

        Dim oIdentificadoresSolicitud As New List(Of cn_fsItem)
        Dim iIdentificadorSolicitud As cn_fsItem
        For Each row As DataRow In dtDatos.Rows
            iIdentificadorSolicitud = New cn_fsItem
            With iIdentificadorSolicitud
                .value = row("INSTANCIA")
                .text = row("INSTANCIA") & "-" & row("DEN").ToString
            End With
            oIdentificadoresSolicitud.Add(iIdentificadorSolicitud)
        Next

        Return oIdentificadoresSolicitud
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="Usu"></param>
    ''' <param name="Idi"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Obtener_BuscadorContrato_Tipos(ByVal Usu As String, ByVal Idi As String) As List(Of cn_fsItem)
        Authenticate()

        Dim dtDatos As DataTable
        dtDatos = DBServer.CN_Obtener_BuscadorInstancia_Usu(Usu, Idi, _
                                                TiposDeDatos.TipoDeSolicitud.Contrato, TipoInfo:=1)

        Dim oTiposContrato As New List(Of cn_fsItem)
        Dim iTipoContrato As cn_fsItem
        For Each row As DataRow In dtDatos.Rows
            iTipoContrato = New cn_fsItem
            With iTipoContrato
                .value = row("ID")
                .text = row("DEN").ToString
            End With
            oTiposContrato.Add(iTipoContrato)
        Next

        Return oTiposContrato
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="Usu"></param>
    ''' <param name="Idi"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Obtener_BuscadorContrato_Proveedor(ByVal Usu As String, ByVal Idi As String, _
                                                                  ByVal TipoSolicitud As Integer) As List(Of cn_fsItem)
        Authenticate()

        Dim dtDatos As DataTable
        dtDatos = DBServer.CN_Obtener_BuscadorInstancia_Usu(Usu, Idi, _
                TiposDeDatos.TipoDeSolicitud.Contrato, TipoSolicitud:=TipoSolicitud, TipoInfo:=2)

        Dim oProveedoresContrato As New List(Of cn_fsItem)
        Dim iProveedorContrato As cn_fsItem
        For Each row As DataRow In dtDatos.Rows
            iProveedorContrato = New cn_fsItem
            With iProveedorContrato
                .value = row("COD")
                .text = row("COD") & "-" & row("DEN").ToString
            End With
            oProveedoresContrato.Add(iProveedorContrato)
        Next

        Return oProveedoresContrato
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="Usu"></param>
    ''' <param name="Idi"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Obtener_BuscadorContrato_Identificador(ByVal Usu As String, ByVal Idi As String, _
                                                ByVal TipoSolicitud As Integer, ByVal ProveCod As String) As List(Of cn_fsItem)
        Authenticate()

        Dim dtDatos As DataTable
        dtDatos = DBServer.CN_Obtener_BuscadorInstancia_Usu(Usu, Idi, _
                TiposDeDatos.TipoDeSolicitud.Contrato, TipoSolicitud:=TipoSolicitud, Prove:=ProveCod, TipoInfo:=3)

        Dim oIdentificadoresContrato As New List(Of cn_fsItem)
        Dim iIdentificadorContrato As cn_fsItem
        For Each row As DataRow In dtDatos.Rows
            iIdentificadorContrato = New cn_fsItem
            With iIdentificadorContrato
                .value = row("INSTANCIA")
                .text = row("INSTANCIA") & "-" & row("DEN").ToString
            End With
            oIdentificadoresContrato.Add(iIdentificadorContrato)
        Next

        Return oIdentificadoresContrato
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="PerCod"></param>
    ''' <param name="PermisoVerPedidosCCImputables"></param>
    ''' <param name="TipoInfo"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Obtener_Pedidos(ByVal PerCod As String, ByVal PermisoVerPedidosCCImputables As Boolean, ByVal TipoInfo As Integer, _
                                          Optional ByVal Anio As Nullable(Of Integer) = Nothing, _
                                          Optional ByVal NumPedido As Nullable(Of Integer) = Nothing, _
                                          Optional ByVal NumOrden As Nullable(Of Integer) = Nothing) As List(Of cn_fsItem)
        Authenticate()

        Dim dtDatos As DataTable
        dtDatos = DBServer.CN_Obtener_Pedidos(PerCod, PermisoVerPedidosCCImputables, Anio, NumPedido, NumOrden)

        Dim key As String
        Select Case TipoInfo
            Case 1
                key = "ANYO"
            Case 2
                key = "NUMPEDIDO"
            Case 3
                key = "NUMORDEN"
            Case Else
                key = "ANYO"
        End Select

        Dim oAniosPedidos As New List(Of cn_fsItem)
        Dim item As cn_fsItem
        For Each row As DataRow In dtDatos.DefaultView.ToTable(True, key).Rows
            item = New cn_fsItem
            item.value = row(key)
            item.text = row(key)

            oAniosPedidos.Add(item)
        Next

        Return oAniosPedidos
    End Function
    ''' <summary>
    ''' Obtiene las categorías de un nivel que puede administrar un usuario
    ''' </summary>
    ''' <param name="sUsuCod">Código de usuario </param>
    ''' <param name="sIdioma">Idioma en q mostrar los textos</param> 
    ''' <param name="vRestricc">Restricción categorías:
    '''					  1 - dadas de alta por el usuario
    '''					  2 - dadas de alta en el departamento del usuario
    '''					  3 - dadas de alta en la unidad organizativa del usuario
    '''					  4 - sin restricción </param>
    ''' <param name="iIdCat">Id de categoría </param>
    ''' <returns>Las categorías</returns>
    ''' <remarks>Llamado desde: cnCategorias.aspx  ; Tiempo máximo: 0,3</remarks>
    ''' <revisado>JVS 01/03/2012</revisado>
    Public Function Obtener_ListaCategorias(ByVal sUsuCod As String, ByVal sIdioma As String,
                                      ByVal vRestricc As OrganizacionRed, Optional ByVal iIdCat As Integer = 0) As List(Of FSNServer.cn_fsItem)
        Authenticate()

        Dim dtDatos As DataTable
        dtDatos = DBServer.CN_Obtener_ListaCategorias(sUsuCod, sIdioma, vRestricc, iIdCat)

        Dim oCategorias As New List(Of cn_fsItem)
        Dim item As cn_fsItem

        For Each row As DataRow In dtDatos.Rows
            item = New cn_fsItem
            item.value = row("ID")
            item.text = row("ID") & " - " & row("DENCAT")

            oCategorias.Add(item)
        Next

        Return oCategorias
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sUsuCod"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Comprobar_HayMensajesHistoricos(ByVal sUsuCod As String) As Boolean
        Authenticate()

        Try
            Return DBServer.CN_Comprobar_HayMensajesHistoricos(sUsuCod)
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    Public Function Comprobar_PermisoConsulta_Factura(ByVal Usu As String, ByVal Idioma As String, ByVal IdFactura As Integer) As Dictionary(Of String, Integer)
        Authenticate()

        Dim dt As DataTable = DBServer.CN_Comprobar_PermisoConsulta_Factura(Usu, Idioma, IdFactura)
        Dim oFact As New System.Collections.Generic.Dictionary(Of String, Integer)
        If dt.Rows.Count = 1 Then
            oFact("idFact") = dt.Rows(0)("ID")
            oFact("gestorFact") = dt.Rows(0)("GESTOR")
            oFact("receptorFact") = dt.Rows(0)("RECEPTOR")
        End If
        Return oFact
    End Function
End Class