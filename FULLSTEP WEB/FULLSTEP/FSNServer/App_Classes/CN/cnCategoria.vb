﻿<Serializable()> _
Public Class cnCategoria
    Inherits Security

#Region "Constructor"
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
        _Denominaciones = New cnMultiIdioma
    End Sub
    Public Sub New()

    End Sub
#End Region
#Region "Properties"
    Private _Id As Integer
    Public Property Id() As Integer
        Get
            Return _Id
        End Get
        Set(ByVal value As Integer)
            _Id = value
        End Set
    End Property
    Private _Nivel As Integer
    Public Property Nivel() As Integer
        Get
            Return _Nivel
        End Get
        Set(ByVal value As Integer)
            _Nivel = value
        End Set
    End Property
    Private _CategoriaPadre As Nullable(Of Integer)
    Public Property CategoriaPadre() As Nullable(Of Integer)
        Get
            Return _CategoriaPadre
        End Get
        Set(ByVal value As Nullable(Of Integer))
            _CategoriaPadre = value
        End Set
    End Property
    Private _PathCategoria As String
    Public Property PathCategoria() As String
        Get
            Return _PathCategoria
        End Get
        Set(ByVal value As String)
            _PathCategoria = value
        End Set
    End Property
    Private _Denominacion As String
    Public Property Denominacion() As String
        Get
            Return _Denominacion
        End Get
        Set(ByVal value As String)
            _Denominacion = value
        End Set
    End Property
    Private _Denominaciones As cnMultiIdioma
    Public ReadOnly Property Denominaciones() As cnMultiIdioma
        Get
            Return _Denominaciones
        End Get
    End Property
    Private _Despublicado As Boolean
    Public Property Despublicado() As Boolean
        Get
            Return _Despublicado
        End Get
        Set(ByVal value As Boolean)
            _Despublicado = value
        End Set
    End Property
    Private _PermisoParaPub As PermisoPublicacion
    Public Property PermisoParaPub() As PermisoPublicacion
        Get
            Return _PermisoParaPub
        End Get
        Set(ByVal value As PermisoPublicacion)
            _PermisoParaPub = value
        End Set
    End Property
    Private _CategoriasHijo As List(Of cnCategoria)
    Public Property CategoriasHijo() As List(Of cnCategoria)
        Get
            Return _CategoriasHijo
        End Get
        Set(ByVal value As List(Of cnCategoria))
            _CategoriasHijo = value
        End Set
    End Property
    Private _selectable As Nullable(Of Boolean)
    Public Property selectable() As Nullable(Of Boolean)
        Get
            Return _selectable
        End Get
        Set(ByVal value As Nullable(Of Boolean))
            _selectable = value
        End Set
    End Property
    Private _Mensajes As List(Of cnMensaje)
    Public Property Mensajes() As List(Of cnMensaje)
        Get
            Return _Mensajes
        End Get
        Set(ByVal value As List(Of cnMensaje))
            _Mensajes = value
        End Set
    End Property
#End Region
#Region "Data"
    ''' <summary>
    ''' Carga los datos de la categoria
    ''' </summary>
    ''' <param name="iIdCat">Id de la categoría.</param>
    ''' <param name="sIdi">Idioma del usuario.</param>
    ''' <remarks>Llamada desde: cn_categorias.aspx.vb; Tiempo máximo:0,2</remarks>
    ''' <revisado>JVS 05/03/2012</revisado>
    Public Sub LoadCategoria(ByVal iIdCat As Integer, ByVal sIdi As String)
        Dim data As DataSet
        Dim row As DataRow
        Dim oIdiomas As New Idiomas(DBServer, mIsAuthenticated)
        Authenticate()

        data = DBServer.CN_LoadCategoriaData(iIdCat, sIdi)

        If Not data.Tables(0).Rows.Count = 0 Then
            row = data.Tables(0).Rows(0)

            _Id = DBNullToSomething(row.Item("ID"))

            oIdiomas.Load()
            For Each oIdi In oIdiomas.Idiomas
                _Denominaciones(oIdi.Cod) = DBNullToSomething(row.Item("DENCAT_" & oIdi.Cod).ToString)
                If sIdi = oIdi.Cod Then
                    _Denominacion = DBNullToSomething(row.Item("DENCAT_" & oIdi.Cod).ToString)
                End If
            Next

            If IsDBNull(row.Item("PERMISOPARAPUB")) Then
                _PermisoParaPub = PermisoPublicacion.SinPermiso
            Else
                If row.Item("PERMISOPARAPUB") Then
                    _PermisoParaPub = PermisoPublicacion.Publico
                Else
                    _PermisoParaPub = PermisoPublicacion.Privado
                End If
            End If
            _Despublicado = DBNullToSomething(row.Item("DESPUBENRED"))
            _PathCategoria = DBNullToSomething(row.Item("DENCATEGPADRE"))
        End If
        row = Nothing
        data = Nothing

    End Sub
    ''' <summary>
    ''' Carga los miembros de la red con permiso de publicación en la categoría
    ''' </summary>
    ''' <param name="Idi"></param>
    ''' <param name="IdCategoria"></param>
    ''' <remarks>Llamada desde: CN.asmx.vb; Tiempo máximo:0,2</remarks>
    ''' <revisado>JVS 08/03/2012</revisado>
    Public Function Load_Miembros_Categoria(ByVal Idi As String, _
                                            ByVal IdCategoria As Nullable(Of Integer)) As List(Of Object)
        Authenticate()

        Dim dsDatos As DataSet

        Try
            dsDatos = DBServer.CN_Load_Miembros_Categoria(Idi, IdCategoria)

            Dim oMiembrosCat As New List(Of Object)

            Dim oParaPorCategoria As New List(Of cn_fsItem)
            Dim iPara As cn_fsItem

            For Each row As DataRow In dsDatos.Tables("PARAUON").Rows
                iPara = New cn_fsItem
                With iPara
                    .value = "itemParaUON" & _
                        IIf(IsDBNull(row("UON0")), "", "-UON0_" & row("UON0")) & _
                        IIf(IsDBNull(row("UON1")), "", "-UON1_" & row("UON1")) & _
                        IIf(IsDBNull(row("UON2")), "", "-UON2_" & row("UON2")) & _
                        IIf(IsDBNull(row("UON3")), "", "-UON3_" & row("UON3")) & _
                        IIf(IsDBNull(row("DEP")), "", "-DEP_" & row("DEP"))
                    .text = IIf(IsDBNull(row("UON1")), "", row("UON1") & "-") & _
                        IIf(IsDBNull(row("UON2")), "", row("UON2") & "-") & _
                        IIf(IsDBNull(row("UON3")), "", row("UON3") & "-") & _
                        IIf(IsDBNull(row("DEP")), "", row("DEP") & "-") & row("DEN").ToString
                End With
                oParaPorCategoria.Add(iPara)
            Next

            For Each row As DataRow In dsDatos.Tables("PARAGRUPOS").Rows
                iPara = New cn_fsItem
                With iPara
                    If Not IsDBNull(row("GRUPO")) Then
                        .value = "itemParaGrupo-Uno-" & row("GRUPO")
                    End If

                    .text = row("DEN").ToString
                End With
                oParaPorCategoria.Add(iPara)
            Next

            With oMiembrosCat
                .Add(oParaPorCategoria)
            End With

            Return oMiembrosCat
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    ''' <summary>
    ''' Obtener UONs para asignar a la Categoría
    ''' </summary>
    ''' <param name="UsuCod"></param>
    ''' <param name="Idioma"></param>
    ''' <returns>List(Of cn_fsTreeViewItem)</returns>
    ''' <remarks>Llamado desde: CN.asmx.vb  ; Tiempo máximo: 0,3</remarks>
    ''' <revisado>JVS 09/03/2012</revisado>
    Public Function Obtener_UONs_Categoria(ByVal UsuCod As String, ByVal Idioma As String) As List(Of cn_fsTreeViewItem)
        Authenticate()

        Try
            Dim dsDatos As New DataSet
            dsDatos = DBServer.CN_Obtener_UONs_Categoria(UsuCod, Idioma)

            Dim oUONPara As New List(Of cn_fsTreeViewItem)
            Dim item0, item1, item2, item3, item4 As cn_fsTreeViewItem
            ' UON0 - LA EMPRESA
            item0 = New cn_fsTreeViewItem

            With item0
                .value = dsDatos.Tables(0).Rows(0)("COD")
                .text = dsDatos.Tables(0).Rows(0)("DEN")
                .selectable = 1
                .type = IIf(.selectable, 3, 0)
                .nivel = "UON0"
                .expanded = True

                'UON1 Y UON0_DEP
                .children = New List(Of cn_fsTreeViewItem)
                With .children
                    For Each row1 As DataRow In dsDatos.Tables(1).Rows
                        item1 = New cn_fsTreeViewItem

                        With item1
                            .value = row1("COD")
                            .text = row1("COD") & " - " & row1("DEN")
                            .selectable = 1
                            .type = IIf(.selectable, 3, 0)

                            If row1("ESDEPARTAMENTO") Then
                                .nivel = "DEP"
                                .expanded = False
                                .children = New List(Of cn_fsTreeViewItem)
                            Else
                                .nivel = "UON1"
                                .expanded = False
                                'UON2 Y UON1_DEP
                                .children = New List(Of cn_fsTreeViewItem)
                                With .children
                                    For Each row2 As DataRow In dsDatos.Tables(2).Select("UON1='" & row1("COD") & "'")
                                        item2 = New cn_fsTreeViewItem

                                        With item2
                                            .value = row2("COD")
                                            .text = row2("COD") & " - " & row2("DEN")
                                            .selectable = 1
                                            .type = IIf(.selectable, 3, 0)

                                            If row2("ESDEPARTAMENTO") Then
                                                .nivel = "DEP"
                                                .expanded = False
                                            Else
                                                .nivel = "UON2"
                                                .expanded = False
                                                'UON3 Y UON2_DEP
                                                .children = New List(Of cn_fsTreeViewItem)
                                                With .children
                                                    For Each row3 As DataRow In dsDatos.Tables(3).Select("UON1='" & row1("COD") & "' AND UON2='" & row2("COD") & "'")
                                                        item3 = New cn_fsTreeViewItem

                                                        With item3
                                                            .value = row3("COD")
                                                            .text = row3("COD") & " - " & row3("DEN")
                                                            .selectable = 1
                                                            .type = IIf(.selectable, 3, 0)

                                                            If row3("ESDEPARTAMENTO") Then
                                                                .nivel = "DEP"
                                                                .expanded = False
                                                            Else
                                                                .nivel = "UON3"
                                                                .expanded = False
                                                                'UON3 Y UON2_DEP
                                                                .children = New List(Of cn_fsTreeViewItem)
                                                                With .children
                                                                    For Each row4 As DataRow In dsDatos.Tables(4).Select("UON1='" & row1("COD") & "' AND UON2='" & row2("COD") & "' AND UON3='" & row3("COD") & "'")
                                                                        item4 = New cn_fsTreeViewItem

                                                                        With item4
                                                                            .value = row4("COD")
                                                                            .text = row4("COD") & " - " & row4("DEN")
                                                                            .selectable = 1
                                                                            .type = IIf(.selectable, 3, 0)

                                                                            If row4("ESDEPARTAMENTO") Then
                                                                                .nivel = "DEP"
                                                                                .expanded = False
                                                                            Else
                                                                            End If
                                                                        End With
                                                                        .Add(item4)
                                                                    Next
                                                                End With
                                                            End If
                                                        End With
                                                        .Add(item3)
                                                    Next
                                                End With
                                            End If
                                        End With
                                        .Add(item2)
                                    Next
                                End With
                            End If
                        End With
                        .Add(item1)
                    Next
                End With
            End With

            oUONPara.Add(item0)

            Return oUONPara
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    ''' <summary>
    ''' Obtener grupos para asignar a la Categoría
    ''' </summary>
    ''' <param name="UsuCod"></param>
    ''' <param name="Idioma"></param>
    ''' <returns>List(Of cn_fsItem)</returns>
    ''' <remarks>Llamado desde: CN.asmx.vb  ; Tiempo máximo: 0,3</remarks>
    ''' <revisado>JVS 13/03/2012</revisado>
    Public Function Obtener_Grupos_Categoria(ByVal UsuCod As String, ByVal Idioma As String) As List(Of cn_fsItem)
        Authenticate()

        Dim dtDatos As DataTable
        Try
            dtDatos = DBServer.CN_Obtener_Grupos_Categoria(UsuCod, Idioma)

            Dim oGruposCorporativos As New List(Of cn_fsItem)
            Dim iGrupoCorporativo As cn_fsItem
            For Each row As DataRow In dtDatos.Rows
                iGrupoCorporativo = New cn_fsItem
                With iGrupoCorporativo
                    .value = row("ID")
                    .text = row("DEN")
                End With
                oGruposCorporativos.Add(iGrupoCorporativo)
            Next

            Return oGruposCorporativos
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    ''' <summary>
    ''' Guarda en base de datos los datos de la nueva categoría
    ''' </summary>
    ''' <param name="sUsu">Código de usuario </param>
    ''' <param name="sIdioma">Idioma en q mostrar los textos</param>
    ''' <param name="tPermisoPublic">Permiso para publicar (privado, público, sin permiso)</param>
    ''' <param name="bDespublicado">Despublicado</param>
    ''' <param name="dFechaAlta">Fecha de alta</param>
    ''' <param name="sDenomCat">Denominación</param>
    ''' <param name="dtUON">tabla de UON</param>
    ''' <param name="dtGrupos">tabla de grupos</param>
    ''' <remarks>Llamado desde: CN.asmx.vb  ; Tiempo máximo: 0,1</remarks>
    ''' <revisado>JVS 14/03/2012</revisado>
    Public Sub Insertar_Categoria(ByVal sUsu As String,
                                ByVal sIdioma As String,
                                ByVal tPermisoPublic As PermisoPublicacion,
                                ByVal bDespublicado As Boolean,
                                ByVal dFechaAlta As DateTime,
                                ByVal sDenomCat As cnMultiIdioma,
                                ByVal dtUON As DataTable,
                                ByVal dtGrupos As DataTable)
        Authenticate()

        Dim dtDenomCat As DataTable
        Dim dtNewRow As DataRow

        Dim oIdiomas As New Idiomas(DBServer, mIsAuthenticated)
        Authenticate()

        dtDenomCat = New DataTable

        dtDenomCat.Columns.Add("IDI", System.Type.GetType("System.String"))

        dtDenomCat.Columns.Add("DEN", System.Type.GetType("System.String"))
        dtDenomCat.Columns("DEN").AllowDBNull = True

        oIdiomas.Load()

        For Each oIdi In oIdiomas.Idiomas
            dtNewRow = dtDenomCat.NewRow
            dtNewRow.Item("IDI") = oIdi.Cod
            dtNewRow.Item("DEN") = sDenomCat(oIdi.Cod)
            dtDenomCat.Rows.Add(dtNewRow)
        Next

        DBServer.CN_InsertarCategoria(_CategoriaPadre, sUsu, sIdioma, _Nivel, tPermisoPublic, bDespublicado, dFechaAlta, dtDenomCat, dtUON, dtGrupos)
    End Sub
    ''' <summary>
    ''' Guarda en base de datos los datos de la categoría
    ''' </summary>
    ''' <param name="sIdioma">Idioma en q mostrar los textos</param>
    ''' <param name="tPermisoPublic">Permiso para publicar (privado, público, sin permiso)</param>
    ''' <param name="bDespublicado">Despublicado</param>
    ''' <param name="sDenomCat">Denominación</param>
    ''' <remarks>Llamado desde: CN.asmx.vb  ; Tiempo máximo: 0,1</remarks>
    ''' <revisado>JVS 13/03/2012</revisado>
    Public Sub Update_Categoria(ByVal sIdioma As String,
                                ByVal tPermisoPublic As PermisoPublicacion,
                                ByVal bDespublicado As Boolean,
                                ByVal sDenomCat As cnMultiIdioma,
                                ByVal dtUON As DataTable,
                                ByVal dtGrupos As DataTable)
        Dim dtDenomCat As DataTable
        Dim dtNewRow As DataRow

        Dim oIdiomas As New Idiomas(DBServer, mIsAuthenticated)
        Authenticate()

        dtDenomCat = New DataTable

        dtDenomCat.Columns.Add("IDI", System.Type.GetType("System.String"))

        dtDenomCat.Columns.Add("DEN", System.Type.GetType("System.String"))
        dtDenomCat.Columns("DEN").AllowDBNull = True

        oIdiomas.Load()

        For Each oIdi In oIdiomas.Idiomas
            dtNewRow = dtDenomCat.NewRow
            dtNewRow.Item("IDI") = oIdi.Cod
            dtNewRow.Item("DEN") = sDenomCat(oIdi.Cod)
            dtDenomCat.Rows.Add(dtNewRow)
        Next

        DBServer.CN_Update_Categoria(_Id, sIdioma, tPermisoPublic, bDespublicado, dtDenomCat, dtUON, dtGrupos)
    End Sub
    ''' <summary>
    ''' Borra en base de datos la categoría
    ''' </summary>
    ''' <remarks>Llamado desde: CN.asmx.vb  ; Tiempo máximo: 0,1</remarks>
    ''' <revisado>JVS 13/03/2012</revisado>
    Public Sub Delete_Categoria()
        Authenticate()

        DBServer.CN_Delete_Categoria(_Id)
    End Sub

    ''' <summary>
    ''' Si la categoría tiene mensajes no se deja eliminar
    ''' </summary>
    ''' <returns>Si la categoría tiene mensajes</returns>
    ''' <remarks>Llamado desde: CN.asmx.vb  ; Tiempo máximo: 0,1</remarks>
    Public Function ComprobarDeleteCategoria() As Boolean
        Authenticate()

        Return DBServer.CN_ComprobarDelete_Categoria(_Id)
    End Function
#End Region
End Class
