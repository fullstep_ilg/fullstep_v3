﻿Public Class cnMiembro
    Private _Data As DataSet
    Public ReadOnly Property Data() As Data.DataSet
        Get
            Return _Data
        End Get
    End Property

    Private _IdGrupo As Integer
    Public Property IdGrupo() As Integer
        Get
            Return _IdGrupo
        End Get
        Set(ByVal value As Integer)
            _IdGrupo = value
        End Set
    End Property

    Private _Estruc As String
    Public Property Estruc() As String
        Get
            Return _Estruc
        End Get
        Set(ByVal value As String)
            _Estruc = value
        End Set
    End Property

    Private _Nombre As String
    Public Property Nombre() As String
        Get
            Return _Nombre
        End Get
        Set(ByVal value As String)
            _Nombre = value
        End Set
    End Property

    Private _Email As String
    Public Property Email() As String
        Get
            Return _Email
        End Get
        Set(ByVal value As String)
            _Email = value
        End Set
    End Property

    Private _Cargo As String
    Public Property Cargo() As String
        Get
            Return _Cargo
        End Get
        Set(ByVal value As String)
            _Cargo = value
        End Set
    End Property

    Private _Mensajes As Integer
    Public Property Mensajes() As Integer
        Get
            Return _Mensajes
        End Get
        Set(ByVal value As Integer)
            _Mensajes = value
        End Set
    End Property
End Class