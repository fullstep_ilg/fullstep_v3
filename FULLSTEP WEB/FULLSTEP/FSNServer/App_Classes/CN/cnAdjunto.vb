﻿Public Class cnAdjunto
    Private _IdAdjunto As Integer
    Public Property IdAdjunto() As Integer
        Get
            Return _IdAdjunto
        End Get
        Set(ByVal value As Integer)
            _IdAdjunto = value
        End Set
    End Property
    Private _EnProceso As String
    Public Property EnProceso() As String
        Get
            Return _EnProceso
        End Get
        Set(ByVal value As String)
            _EnProceso = value
        End Set
    End Property
    Private _Nombre As String
    Public Property Nombre() As String
        Get
            Return _Nombre
        End Get
        Set(ByVal value As String)
            _Nombre = value
        End Set
    End Property
    Private _Guid As String
    Public Property Guid() As String
        Get
            Return _Guid
        End Get
        Set(ByVal value As String)
            _Guid = value
        End Set
    End Property
    Private _Url As String
    Public Property Url() As String
        Get
            Return _Url
        End Get
        Set(ByVal value As String)
            _Url = value
        End Set
    End Property
    Private _Size As Double
    Public Property Size() As Double
        Get
            Return _Size
        End Get
        Set(ByVal value As Double)
            _Size = value
        End Set
    End Property
    Private _SizeUnit As String
    Public Property SizeUnit() As String
        Get
            Return _SizeUnit
        End Get
        Set(ByVal value As String)
            _SizeUnit = value
        End Set
    End Property
    Private _SizeToString As String
    Public Property SizeToString() As String
        Get
            Return _SizeToString
        End Get
        Set(ByVal value As String)
            _SizeToString = value
        End Set
    End Property
    Private _TipoAdjunto As Integer
    Public Property TipoAdjunto() As Integer
        Get
            Return _TipoAdjunto
        End Get
        Set(ByVal value As Integer)
            _TipoAdjunto = value
        End Set
    End Property
    Private _IdMensaje As Integer
    Public Property IdMensaje() As Integer
        Get
            Return _IdMensaje
        End Get
        Set(ByVal value As Integer)
            _IdMensaje = value
        End Set
    End Property
    Private _IdRespuesta As Integer
    Public Property IdRespuesta() As Integer
        Get
            Return _IdRespuesta
        End Get
        Set(ByVal value As Integer)
            _IdRespuesta = value
        End Set
    End Property
    Private _Thumbnail_Url As String
    Public Property Thumbnail_Url() As String
        Get
            Return _Thumbnail_Url
        End Get
        Set(ByVal value As String)
            _Thumbnail_Url = value
        End Set
    End Property
End Class
