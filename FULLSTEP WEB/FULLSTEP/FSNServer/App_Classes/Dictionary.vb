﻿
<Serializable()> _
Public Class Dictionary
    Inherits Security
    Private mvarData As Data.DataSet

#Region " Bussiness methods "
    Public Function GetText(ByVal ID As Integer) As String
        Dim datarow() As Data.DataRow
        If mvarData Is Nothing Then Return ""
        datarow = mvarData.Tables(0).Select("ID=" & ID.ToString)
        If datarow.Length = 0 Then Return ""
        Return datarow(0).Item(1)

    End Function

    Public ReadOnly Property Data() As Data.DataSet
        Get
            Return mvarData
        End Get
    End Property

    ''' <summary>
    ''' Carga en la variable <c>mvarData</c> un Dataset con los textos del módulo indicado.
    ''' </summary>
    ''' <param name="ModuleID">Id del módulo de textos</param>
    ''' <remarks>Tiempo estimado: 0.0 segundos</remarks>
    Public Sub LoadData(ByVal ModuleID As FSNLibrary.TiposDeDatos.ModulosIdiomas)
        Me.LoadData(ModuleID, "SPA")
    End Sub

    ''' <summary>
    ''' Carga en la variable <c>mvarData</c> un Dataset con los textos del módulo indicado.
    ''' </summary>
    ''' <param name="ModuleID">Id del módulo de textos. Dependiendo del valor del módulo se obtendrá la aplicación para la que se recuperan los datos.</param>
    ''' <param name="Language">Idioma</param>
    ''' <remarks>
    ''' Llamadas desde: LoadData, FSNWeb.FSNPage.Textos
    ''' Tiempo estimado: 0.0 segundos
    ''' </remarks>
    Public Sub LoadData(ByVal ModuleID As FSNLibrary.TiposDeDatos.ModulosIdiomas, ByVal Language As String)
        Dim app As FSNLibrary.TiposDeDatos.Aplicaciones
        If (ModuleID And Not FSNLibrary.TiposDeDatos.Aplicaciones.EP) = 0 Then
            app = FSNLibrary.TiposDeDatos.Aplicaciones.PM
        Else
            app = FSNLibrary.TiposDeDatos.Aplicaciones.EP
        End If

        If System.Web.HttpContext.Current Is Nothing OrElse System.Web.HttpContext.Current.Cache("Textos_" & Language & "_" & ModuleID) Is Nothing Then
            Me.LoadData(ModuleID, Language, app)
            If Not System.Web.HttpContext.Current Is Nothing Then
                System.Web.HttpContext.Current.Cache.Insert("Textos_" & Language & "_" & ModuleID, mvarData, Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration, New TimeSpan(0, System.Configuration.ConfigurationManager.AppSettings("TiempoExpiracionCacheMin"), 0))
            End If
        Else
            mvarData = System.Web.HttpContext.Current.Cache("Textos_" & Language & "_" & ModuleID)
        End If
    End Sub

    ''' <summary>
    ''' Carga en la variable <c>mvarData</c> un Dataset con los textos del módulo indicado.
    ''' </summary>
    ''' <param name="ModuleID">Id del módulo de textos</param>
    ''' <param name="Language">Idioma</param>
    ''' <param name="Aplicacion">Aplicación para la que se recuperan los textos. Miembro de la enumeración FSNLibrary.FSNTipos.Aplicacion</param>
    ''' <remarks>
    ''' Llamadas desde: LoadData
    ''' Tiempo estimado: 0.0 segundos
    ''' </remarks>
    Public Sub LoadData(ByVal ModuleID As FSNLibrary.TiposDeDatos.ModulosIdiomas, ByVal Language As String, ByVal Aplicacion As FSNLibrary.TiposDeDatos.Aplicaciones)
        Authenticate()
        mvarData = DBServer.Dictionary_GetData(ModuleID, Language, Aplicacion)
    End Sub

    ''' <summary>
    ''' Funcion que devuelve el texto que se corresponde con un modulo, termino y en el lenguaje especifico
    ''' </summary>
    ''' <param name="ModuleID">Identificador del mÃ³dulo</param>
    ''' <param name="TerminoID">Identificador del tÃ©rmino</param>
    ''' <param name="Language">Idioma del texto a devolver</param>
    ''' <returns>Una variable de tipo string con el texto buscado</returns>
    ''' <remarks>
    ''' Llamada desde: FSNWeb/FSNINfoSoporte/CargarRecursos , PmServer/Instancia/NotificarErrorValidacion y ComprobarParticipantes y ComprobarTieneSiguienteEtapa y ComprobarAsignacion y ComprobarCamposObligatorios
    ''' Tiempo mÃ¡ximo: 0,1 seg</remarks>
    Public Function Texto(ByVal ModuleID As FSNLibrary.TiposDeDatos.ModulosIdiomas, ByVal TerminoID As Integer, ByVal Language As String) As String
        Authenticate()
        Return DBServer.Dictionary_GetText(ModuleID, TerminoID, Language)
    End Function
#End Region

#Region " Constructor "

    ''' <summary>
    ''' Constructor de la clase Dictionary
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si está autenticado</param>
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub

    Public Sub New()
        MyBase.New(New FSNDatabaseServer.Root(), True)
    End Sub

#End Region

End Class