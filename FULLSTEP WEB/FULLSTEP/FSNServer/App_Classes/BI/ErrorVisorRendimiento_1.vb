﻿Public Class ErrorVisorRendimiento
    Inherits Security

    Public id As Long
    Public idError As Integer
    Public rank As Integer
    Public fechaError As String
    Public exFullName As String
    Public data As String
    Public fecAct As DateTime

    ''' <summary>
    ''' Constructor de la clase ErrorVisorActividades
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si está autenticado</param>
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub

End Class

Public Class ErroresVisorRendimiento
    Inherits Security

    Private moErrores As Collection
    Private mData As DataSet
    Private oError As ErrorVisorRendimiento

    ReadOnly Property data() As DataSet
        Get
            Return mData
        End Get
    End Property

    Public ReadOnly Property Errores() As Collection
        Get
            Return moErrores
        End Get
    End Property

    ''' <summary>
    ''' Función que devuelve un dataset con todos los accesos en una hora determinada a un determinado producto
    ''' </summary>
    ''' <remarks>
    ''' Llamada desde: 
    ''' Tiempo: 0 seg
    ''' </remarks>
    Public Function Load(fechaInicio As DateTime, fechaFin As DateTime, entorno As String, pyme As String)
        Dim i As Integer
        mData = DBServer.ErroresLoad(fechaInicio, fechaFin, entorno, pyme)
        If Not data.Tables(0).Rows.Count = 0 Then
            For i = 0 To data.Tables(0).Rows.Count - 1
                If (Not mData.Tables(0).Rows(i).Item("EX_FULLNAME").indexOf("Null") = -1) Then
                    mData.Tables(0).Rows(i).Item("EX_FULLNAME") = mData.Tables(0).Rows(i).Item("EX_FULLNAME").Insert(mData.Tables(0).Rows(i).Item("EX_FULLNAME").indexOf("Null"), " ")
                End If
                If (Not mData.Tables(0).Rows(i).Item("EX_FULLNAME").indexOf("Exception") = -1) Then
                    mData.Tables(0).Rows(i).Item("EX_FULLNAME") = mData.Tables(0).Rows(i).Item("EX_FULLNAME").Insert(mData.Tables(0).Rows(i).Item("EX_FULLNAME").indexOf("Exception"), " ")
                End If
                Me.Add(mData.Tables(0).Rows(i).Item("ID"), mData.Tables(0).Rows(i).Item("ID_ERROR"), mData.Tables(0).Rows(i).Item("RANK"), mData.Tables(0).Rows(i).Item("FECHA_ERROR"), mData.Tables(0).Rows(i).Item("EX_FULLNAME"), mData.Tables(0).Rows(i).Item("DATA").ToString(), mData.Tables(0).Rows(i).Item("FECHA_ERROR"))
            Next
        End If
        Return moErrores
    End Function

    ''' <summary>
    ''' Función que añade un acceso a la lista de accesos
    ''' </summary>

    Friend Sub Add(ByVal id As Long, ByVal idError As Integer, ByVal rank As Integer, ByVal fechaError As DateTime, ByVal exFullName As String, ByVal data As String, ByVal fecAct As DateTime)
        Dim oError As New ErrorVisorRendimiento(DBServer, mIsAuthenticated)
        If moErrores Is Nothing Then
            moErrores = New Collection
        End If

        oError.id = id
        oError.idError = idError
        oError.rank = rank
        oError.fechaError = New Date(fechaError.Year, fechaError.Month, fechaError.Day).ToString("dd-MM-yyyy")
        oError.exFullName = exFullName
        oError.data = data
        oError.fecAct = fecAct
        moErrores.Add(oError, id)
    End Sub

    ''' <summary>
    ''' Constructor de la clase Accesos
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si está autenticado</param>
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
End Class