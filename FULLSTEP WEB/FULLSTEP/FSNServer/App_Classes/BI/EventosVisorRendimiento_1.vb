﻿Imports System.Web

Public Class EventoVisorRendimiento
    Inherits Security

    Public numEvento As String
    Public mediaEvento As String
    Public fechaEvento As String
    Public tipoEvento As String
    Public data As String

    ''' <summary>
    ''' Constructor de la clase ErrorVisorActividades
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si está autenticado</param>
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
End Class

Public Class EventosVisorRendimiento
    Inherits Security

    Private moEventos As Collection
    Private mData As DataSet
    Private oEvento As ErrorVisorRendimiento

    ReadOnly Property data() As DataSet
        Get
            Return mData
        End Get
    End Property
    Public ReadOnly Property Eventos() As Collection
        Get
            Return moEventos
        End Get
    End Property
    ''' <summary>
    ''' Función que devuelve un dataset con todos los eventos en una hora determinada a un determinado producto
    ''' </summary>
    ''' <remarks>
    ''' Llamada desde: 
    ''' Tiempo: 0 seg
    ''' </remarks>
    Public Function Load(fechaInicio As DateTime, fechaFin As DateTime, entorno As String, pyme As String, tipoEvento As String, rango As String)
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim Usuario As FSNServer.User = HttpContext.Current.Session("FSN_User")
        Dim idioma As String = Usuario.IdiomaCod
        Select Case UCase(rango)
            Case "HORA"
                Dim i As Integer
                mData = DBServer.EventosLoad_Hora(fechaInicio, fechaFin, entorno, pyme, tipoEvento, idioma)
                If Not data.Tables(0).Rows.Count = 0 Then
                    For i = 0 To data.Tables(0).Rows.Count - 1
                        Me.Add(mData.Tables(0).Rows(i).Item("FECHA_EVENTO"), mData.Tables(0).Rows(i).Item("DEN"), mData.Tables(0).Rows(i).Item("rank"), mData.Tables(0).Rows(i).Item("AVG_EVENTOS"), mData.Tables(0).Rows(i).Item("data"))
                    Next
                End If
                Return moEventos
            Case "DIA"
                Dim i As Integer
                mData = DBServer.EventosLoad_Dia(fechaInicio, fechaFin, entorno, pyme, tipoEvento, idioma)
                If Not data.Tables(0).Rows.Count = 0 Then
                    For i = 0 To data.Tables(0).Rows.Count - 1
                        Me.Add(mData.Tables(0).Rows(i).Item("FECHA_EVENTO"), mData.Tables(0).Rows(i).Item("DEN"), mData.Tables(0).Rows(i).Item("rank"), mData.Tables(0).Rows(i).Item("AVG_EVENTOS"), mData.Tables(0).Rows(i).Item("data"))
                    Next
                End If
                Return moEventos
            Case "SEMANA"
                Dim i As Integer
                mData = DBServer.EventosLoad_Semana(fechaInicio, fechaFin, entorno, pyme, tipoEvento, idioma)
                If Not data.Tables(0).Rows.Count = 0 Then
                    For i = 0 To data.Tables(0).Rows.Count - 1
                        Me.Add(mData.Tables(0).Rows(i).Item("FECHA_EVENTO"), mData.Tables(0).Rows(i).Item("DEN"), mData.Tables(0).Rows(i).Item("rank"), mData.Tables(0).Rows(i).Item("AVG_EVENTOS"), mData.Tables(0).Rows(i).Item("data"))
                    Next
                End If
                Return moEventos
            Case "MES"
                Dim i As Integer
                mData = DBServer.EventosLoad_Mes(fechaInicio, fechaFin, entorno, pyme, tipoEvento, idioma)
                If Not data.Tables(0).Rows.Count = 0 Then
                    For i = 0 To data.Tables(0).Rows.Count - 1
                        Me.Add(mData.Tables(0).Rows(i).Item("FECHA_EVENTO"), mData.Tables(0).Rows(i).Item("DEN"), mData.Tables(0).Rows(i).Item("rank"), mData.Tables(0).Rows(i).Item("AVG_EVENTOS"), mData.Tables(0).Rows(i).Item("data"))
                    Next
                End If
                Return moEventos
            Case "ANYO"
                Dim i As Integer
                mData = DBServer.EventosLoad_Anyo(fechaInicio, fechaFin, entorno, pyme, tipoEvento, idioma)
                If Not data.Tables(0).Rows.Count = 0 Then
                    For i = 0 To data.Tables(0).Rows.Count - 1
                        Me.Add(mData.Tables(0).Rows(i).Item("FECHA_EVENTO"), mData.Tables(0).Rows(i).Item("DEN"), mData.Tables(0).Rows(i).Item("rank"), mData.Tables(0).Rows(i).Item("AVG_EVENTOS"), mData.Tables(0).Rows(i).Item("data"))
                    Next
                End If
                Return moEventos
            Case Else
                Return Nothing
        End Select
    End Function
    ''' <summary>
    ''' Función que añade un evento a la lista de eventos
    ''' </summary>
    Friend Sub Add(ByVal fechaEvento As DateTime, ByVal tipoEvento As String, ByVal rank As Integer, ByVal media As Double, ByVal data As String)
        Dim oEvento As New EventoVisorRendimiento(DBServer, mIsAuthenticated)
        If moEventos Is Nothing Then moEventos = New Collection

        oEvento.numEvento = rank
        oEvento.mediaEvento = media
        oEvento.fechaEvento = New Date(fechaEvento.Year, fechaEvento.Month, fechaEvento.Day, fechaEvento.Hour, fechaEvento.Minute, fechaEvento.Second).ToString()
        oEvento.tipoEvento = tipoEvento
        oEvento.data = data
        moEventos.Add(oEvento)
    End Sub
    ''' <summary>
    ''' Constructor de la clase Accesos
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si está autenticado</param>
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
End Class

Public Class TipoEventoVisorRendimiento
    Inherits Security
    Public id As Long
    Public denominacion As String

    Private moTipoEvento As Collection
    Private mData As DataSet
    Private oTipoEvento As ErrorVisorRendimiento

    ReadOnly Property data() As DataSet
        Get
            Return mData
        End Get
    End Property
    Public ReadOnly Property TipoEvento() As Collection
        Get
            Return moTipoEvento
        End Get
    End Property
    ''' <summary>
    ''' Función que añade un evento a la lista de eventos
    ''' </summary>
    Friend Sub AddTipoEvento(ByVal id As Long, ByVal denominacion As String)
        Dim oTipoEvento As New TipoEventoVisorRendimiento(DBServer, mIsAuthenticated)
        If moTipoEvento Is Nothing Then moTipoEvento = New Collection

        oTipoEvento.id = id
        oTipoEvento.denominacion = denominacion

        moTipoEvento.Add(oTipoEvento, id)
    End Sub
    ''' <summary>
    ''' Constructor de la clase Accesos
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si está autenticado</param>
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
End Class