﻿Public Class Entorno
    Inherits Security

    Private msId As Long
    Private msCod As String
    Private msIdCliente As Integer
    Private msLocalizacion As String
    Private msVersion As String
    Private msFultins As String
    Private msRespins As String
    Private msRespimp As String
    Private msIntegracion As Integer
    Private msBaja As Integer
    Private msFecAct As String
    Private msDenominacion As String

    Property Id() As Long
        Get
            Id = msId
        End Get
        Set(ByVal Value As Long)
            msId = Value
        End Set
    End Property
    Property Cod() As String
        Get
            Cod = msCod
        End Get
        Set(ByVal Value As String)
            msCod = Value
        End Set
    End Property
    Property IdCliente() As Long
        Get
            IdCliente = msIdCliente
        End Get
        Set(ByVal Value As Long)
            msIdCliente = Value
        End Set
    End Property
    Property Localizacion() As String
        Get
            Localizacion = msLocalizacion
        End Get
        Set(ByVal Value As String)
            msLocalizacion = Value
        End Set
    End Property
    Property Version() As String
        Get
            Version = msVersion
        End Get
        Set(ByVal Value As String)
            msVersion = Value
        End Set
    End Property
    Property Fultins() As DateTime
        Get
            Fultins = msFultins
        End Get
        Set(ByVal Value As DateTime)
            msFultins = Value
        End Set
    End Property
    Property Respins() As String
        Get
            Respins = msRespins
        End Get
        Set(ByVal Value As String)
            msRespins = Value
        End Set
    End Property
    Property Respimp() As String
        Get
            Respimp = msRespimp
        End Get
        Set(ByVal Value As String)
            msRespimp = Value
        End Set
    End Property
    Property Integracion() As Integer
        Get
            Integracion = msIntegracion
        End Get
        Set(ByVal Value As Integer)
            msIntegracion = Value
        End Set
    End Property
    Property Baja() As Integer
        Get
            Baja = msBaja
        End Get
        Set(ByVal Value As Integer)
            msBaja = Value
        End Set
    End Property
    Property FecAct() As DateTime
        Get
            FecAct = msFecAct
        End Get
        Set(ByVal Value As DateTime)
            msFecAct = Value
        End Set
    End Property
    Property Denominacion() As String
        Get
            Denominacion = msDenominacion
        End Get
        Set(ByVal Value As String)
            msDenominacion = Value
        End Set
    End Property
    ''' <summary>
    ''' Constructor de la clase Entorno
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si está autenticado</param>
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
End Class
Public Class Entornos
    Inherits Security

    Private moEntornos As Collection
    Private mData As DataSet
    Private oEntorno As Entorno

    ReadOnly Property data() As DataSet
        Get
            Return mData
        End Get
    End Property
    Public ReadOnly Property Entornos() As Collection
        Get
            Return moEntornos
        End Get
    End Property
    ''' <summary>
    ''' Función que devuelve un dataset con todos los entornos de la aplicación
    ''' </summary>
    ''' <remarks>
    ''' Llamada desde: 
    ''' Tiempo: 0 seg
    ''' </remarks>
    Public Sub Load(ByVal idioma As String)
        Dim i As Integer
        mData = DBServer.Entornos_Load(idioma)
        If Not data.Tables(0).Rows.Count = 0 Then
            For i = 0 To data.Tables(0).Rows.Count - 1
                If (Not mData.Tables(0).Rows(i).Item("FULTINS") Is Nothing) Then
                    mData.Tables(0).Rows(i).Item("FULTINS") = DateTime.Now()
                End If
                Me.Add(mData.Tables(0).Rows(i).Item("ID").ToString(), mData.Tables(0).Rows(i).Item("COD").ToString(), mData.Tables(0).Rows(i).Item("ID_CLIENTE").ToString(), mData.Tables(0).Rows(i).Item("LOCALIZACION").ToString(), mData.Tables(0).Rows(i).Item("VERSION").ToString(), mData.Tables(0).Rows(i).Item("FULTINS"), mData.Tables(0).Rows(i).Item("RESPINS").ToString(), mData.Tables(0).Rows(i).Item("RESPIMP").ToString(), mData.Tables(0).Rows(i).Item("INTEGRACION").ToString(), mData.Tables(0).Rows(i).Item("BAJA").ToString(), mData.Tables(0).Rows(i).Item("FECACT"), mData.Tables(0).Rows(i).Item("DEN").ToString())
            Next
        End If
    End Sub
    ''' <summary>
    ''' Función que añade un cubo activo a la lista de cubos
    ''' </summary>
    Friend Sub Add(ByVal sId As String, ByVal sCod As String, ByVal sIdCliente As String, ByVal sLocalizacion As String, ByVal sVersion As String, ByVal sFultins As DateTime, ByVal sRespins As String, ByVal sRespimp As String, ByVal sIntegracion As String, ByVal sBaja As String, ByVal sFecAct As DateTime, ByVal sDen As String)
        Dim oEntorno As New Entorno(DBServer, mIsAuthenticated)
        If moEntornos Is Nothing Then
            moEntornos = New Collection
        End If
        oEntorno.Id = sId
        oEntorno.Cod = sCod
        oEntorno.IdCliente = sIdCliente
        oEntorno.Localizacion = sLocalizacion
        oEntorno.Version = sVersion
        oEntorno.Fultins = sFultins
        oEntorno.Respins = sRespins
        oEntorno.Respimp = sRespimp
        oEntorno.Integracion = sIntegracion
        oEntorno.Baja = sBaja
        oEntorno.FecAct = sFecAct
        oEntorno.Denominacion = sDen

        moEntornos.Add(oEntorno, sId)
    End Sub
    ''' <summary>
    ''' Constructor de la clase Entornos
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si está autenticado</param>
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
End Class