﻿Public Class Cubo
    Inherits Security

    Private msID As Long
    Private msDen As String
    Private msSvc_Url As String
    Private msSvc_Usu As String
    Private msSvc_Pwd As String
    Private msSvc_Dom As String
    Private msSsas_Server As String
    Private msSsas_Usu As String
    Private msSsas_Pwd As String
    Private msSsas_Dom As String
    Private msSsas_BD As String
    Private msSsas_Cube As String
    Private msActivo As Boolean
    Private msFecAct As Date
    Private msAvailable_Measures As String
    Private msDefault_Measures As String
    Private msExtended_Dashboard As Boolean
    Private msMoneda As String
    Private msCli As String

    Property Den() As String
        Get
            Den = msDen
        End Get
        Set(ByVal Value As String)
            msDen = Value
        End Set
    End Property
    Property Id() As Long
        Get
            Id = msID
        End Get
        Set(ByVal Value As Long)
            msID = Value
        End Set
    End Property
    Property Svc_Url() As String
        Get
            Svc_Url = msSvc_Url
        End Get
        Set(ByVal Value As String)
            msSvc_Url = Value
        End Set
    End Property
    Property Svc_Usu() As String
        Get
            Svc_Usu = msSvc_Usu
        End Get
        Set(ByVal Value As String)
            msSvc_Usu = Value
        End Set
    End Property
    Property Svc_Pwd() As String
        Get
            Svc_Pwd = msSvc_Pwd
        End Get
        Set(ByVal Value As String)
            msSvc_Pwd = Value
        End Set
    End Property
    Property Svc_Dom() As String
        Get
            Svc_Dom = msSvc_Dom
        End Get
        Set(ByVal Value As String)
            msSvc_Dom = Value
        End Set
    End Property
    Property Ssas_Server() As String
        Get
            Ssas_Server = msSsas_Server
        End Get
        Set(ByVal Value As String)
            msSsas_Server = Value
        End Set
    End Property
    Property Ssas_Usu() As String
        Get
            Ssas_Usu = msSsas_Usu
        End Get
        Set(ByVal Value As String)
            msSsas_Usu = Value
        End Set
    End Property
    Property Ssas_Pwd() As String
        Get
            Ssas_Pwd = msSsas_Pwd
        End Get
        Set(ByVal Value As String)
            msSsas_Pwd = Value
        End Set
    End Property
    Property Ssas_Dom() As String
        Get
            Ssas_Dom = msSsas_Dom
        End Get
        Set(ByVal Value As String)
            msSsas_Dom = Value
        End Set
    End Property
    Property Ssas_BD() As String
        Get
            Ssas_BD = msSsas_BD
        End Get
        Set(ByVal Value As String)
            msSsas_BD = Value
        End Set
    End Property
    Property Ssas_Cube() As String
        Get
            Ssas_Cube = msSsas_Cube
        End Get
        Set(ByVal Value As String)
            msSsas_Cube = Value
        End Set
    End Property
    Property Activo() As Boolean
        Get
            Activo = msActivo
        End Get
        Set(ByVal Value As Boolean)
            msActivo = Value
        End Set
    End Property
    Property FecAct() As Date
        Get
            FecAct = msFecAct
        End Get
        Set(ByVal Value As Date)
            msFecAct = Value
        End Set
    End Property
    Property Available_Measures() As String
        Get
            Available_Measures = msAvailable_Measures
        End Get
        Set(ByVal Value As String)
            msAvailable_Measures = Value
        End Set
    End Property
    Property Default_Measures() As String
        Get
            Default_Measures = msDefault_Measures
        End Get
        Set(ByVal Value As String)
            msDefault_Measures = Value
        End Set
    End Property
    Property Extended_Dashboard() As Boolean
        Get
            Extended_Dashboard = msExtended_Dashboard
        End Get
        Set(ByVal Value As Boolean)
            msExtended_Dashboard = Value
        End Set
    End Property
    Property Mon() As String
        Get
            Mon = msMoneda
        End Get
        Set(ByVal Value As String)
            msMoneda = Value
        End Set
    End Property
    Property Cli() As String
        Get
            Cli = msCli
        End Get
        Set(ByVal Value As String)
            msCli = Value
        End Set
    End Property
    ''' <summary>
    ''' Constructor de la clase Cubo
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si está autenticado</param>
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
End Class
Public Class Cubos
    Inherits Security

    Private moCubos As Collection
    Private mData As DataSet
    Private oCubo As Cubo

    ReadOnly Property data() As DataSet
        Get
            Return mData
        End Get
    End Property
    Public ReadOnly Property Cubos() As Collection
        Get
            Return moCubos
        End Get
    End Property
    ''' <summary>
    ''' Función que devuelve un dataset con todos los cubos, activos o no, de la aplicación
    ''' </summary>
    ''' <remarks>
    ''' Llamada desde: 
    ''' Tiempo: 0 seg
    ''' </remarks>
    Public Sub FullLoad(ByVal idioma As String)
        Dim i As Integer
        mData = DBServer.Cubos_FullLoad(idioma)
        If Not data.Tables(0).Rows.Count = 0 Then
            For i = 0 To data.Tables(0).Rows.Count - 1
                Me.Add(mData.Tables(0).Rows(i).Item("ID").ToString(), mData.Tables(0).Rows(i).Item("DEN").ToString(), mData.Tables(0).Rows(i).Item("SVC_URL").ToString(), mData.Tables(0).Rows(i).Item("SVC_USU").ToString(), Encrypter.EncriptarSQLtod("", mData.Tables(0).Rows(i).Item("SVC_PWD").ToString(), False), mData.Tables(0).Rows(i).Item("SVC_DOM").ToString(), mData.Tables(0).Rows(i).Item("SSAS_SERVER").ToString(), mData.Tables(0).Rows(i).Item("SSAS_USU").ToString(), Encrypter.EncriptarSQLtod("", mData.Tables(0).Rows(i).Item("SSAS_PWD").ToString(), False), mData.Tables(0).Rows(i).Item("SSAS_DOM").ToString(), mData.Tables(0).Rows(i).Item("SSAS_BD").ToString(), mData.Tables(0).Rows(i).Item("SSAS_CUBE").ToString(), mData.Tables(0).Rows(i).Item("ACTIVO").ToString, mData.Tables(0).Rows(i).Item("AVAILABLE_MEASURES").ToString(), mData.Tables(0).Rows(i).Item("DEFAULT_MEASURES").ToString(), mData.Tables(0).Rows(i).Item("EXTENDED_DASHBOARD"), mData.Tables(0).Rows(i).Item("MON"), mData.Tables(0).Rows(i).Item("CLI"))
            Next
        End If
    End Sub
    ''' <summary>
    ''' Función que añade un cubo activo a la lista de cubos
    ''' </summary>
    Friend Sub Add(ByVal sId As String, ByVal sDen As String, ByVal sSvc_Url As String, ByVal sSvc_Usu As String, ByVal sSvc_Pwd As String, ByVal sSvc_Dom As String, ByVal sSsas_Server As String, ByVal sSsas_Usu As String, ByVal sSsas_Pwd As String, ByVal sSsas_Dom As String, ByVal sSsas_BD As String, ByVal sSsas_Cube As String, ByVal sAvailable_Measures As String, ByVal sDefault_Measures As String, ByVal sExtended_Dashboard As Boolean, ByVal sMoneda As String, ByVal sCli As String)
        Dim oCubo As New Cubo(DBServer, mIsAuthenticated)
        If moCubos Is Nothing Then moCubos = New Collection

        oCubo.Id = sId
        oCubo.Den = sDen
        oCubo.Svc_Url = sSvc_Url
        oCubo.Svc_Usu = sSvc_Usu
        oCubo.Svc_Pwd = sSvc_Pwd
        oCubo.Svc_Dom = sSvc_Dom
        oCubo.Ssas_Server = sSsas_Server
        oCubo.Ssas_Usu = sSsas_Usu
        oCubo.Ssas_Pwd = sSsas_Pwd
        oCubo.Ssas_Dom = sSsas_Dom
        oCubo.Ssas_BD = sSsas_BD
        oCubo.Ssas_Cube = sSsas_Cube
        oCubo.Available_Measures = sAvailable_Measures
        oCubo.Default_Measures = sDefault_Measures
        oCubo.Extended_Dashboard = sExtended_Dashboard
        oCubo.Mon = sMoneda
        oCubo.Cli = sCli

        moCubos.Add(oCubo, sId)
    End Sub
    ''' <summary>
    ''' Función que añade un cubo, activo o no, a la lista de cubos
    ''' </summary>
    Friend Sub Add(ByVal sId As String, ByVal sDen As String, ByVal sSvc_Url As String, ByVal sSvc_Usu As String, ByVal sSvc_Pwd As String, ByVal sSvc_Dom As String, ByVal sSsas_Server As String, ByVal sSsas_Usu As String, ByVal sSsas_Pwd As String, ByVal sSsas_Dom As String, ByVal sSsas_BD As String, ByVal sSsas_Cube As String, ByVal sActivo As String, ByVal sAvailable_Measures As String, ByVal sDefault_Measures As String, ByVal sExtended_Dashboard As Boolean, ByVal sMon As String, ByVal sCli As String)
        Dim dsDatos As New DataSet
        Dim oCubo As New Cubo(DBServer, mIsAuthenticated)
        If moCubos Is Nothing Then moCubos = New Collection

        oCubo.Id = sId
        oCubo.Svc_Url = sSvc_Url
        oCubo.Svc_Usu = sSvc_Usu
        oCubo.Svc_Pwd = sSvc_Pwd
        oCubo.Svc_Dom = sSvc_Dom
        oCubo.Ssas_Server = sSsas_Server
        oCubo.Ssas_Usu = sSsas_Usu
        oCubo.Ssas_Pwd = sSsas_Pwd
        oCubo.Ssas_Dom = sSsas_Dom
        oCubo.Ssas_BD = sSsas_BD
        oCubo.Ssas_Cube = sSsas_Cube
        oCubo.Activo = sActivo
        oCubo.Available_Measures = sAvailable_Measures
        oCubo.Default_Measures = sDefault_Measures
        oCubo.Extended_Dashboard = sExtended_Dashboard
        oCubo.Mon = sMon
        oCubo.Cli = sCli
        moCubos.Add(oCubo, sId)
    End Sub
    ''' <summary>
    ''' Constructor de la clase Cubos
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si está autenticado</param>
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
    ''' <summary>
    ''' Inserta un nuevo Cubo en la BBDD
    ''' </summary>
    ''' <param name="activo">Cubo activo o no</param>
    ''' <param name="url">Url del cubo</param>
    ''' <param name="usuario">Usuario del cubo</param>
    ''' <param name="password">Contraseña del cubo</param>
    ''' <param name="dominio">Dominio del cubo</param>
    ''' <param name="url2">Url SSAS del cubo</param>
    ''' <param name="usuario2">Usuario SSAS del cubo</param>
    ''' <param name="password2">Contraseña SSAS del cubo</param>
    ''' <param name="dominio2">Dominio SSAS del cubo</param>
    ''' <param name="BD">BD SSAS del cubo</param>
    ''' <param name="cubo">Cubo SSAS</param>
    ''' <remarks>Llamada desde: Sistema, Tiempo maximo: 0</remarks>
    Public Function Cubos_Insert(ByVal url As String, ByVal usuario As String, ByVal password As String, ByVal dominio As String, ByVal url2 As String, ByVal usuario2 As String, ByVal password2 As String, ByVal dominio2 As String, ByVal BD As String, ByVal cubo As String, ByVal activo As Boolean, ByVal availableMeasures As String, ByVal defaultMeasures As String) As Integer
        Try
            password = Encrypter.EncriptarSQLtod("", password, True)
            password2 = Encrypter.EncriptarSQLtod("", password2, True)
            Return DBServer.Cubos_Insert(url, usuario, password, dominio, url2, usuario2, password2, dominio2, BD, cubo, activo, availableMeasures, defaultMeasures)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Sub Cubos_Update(ByVal idCubo As Integer, ByVal url As String, ByVal usuario As String, ByVal password As String, ByVal dominio As String, ByVal url2 As String, ByVal usuario2 As String, ByVal password2 As String, ByVal dominio2 As String, ByVal BD As String, ByVal cubo As String, ByVal activo As Boolean, ByVal availableMeasures As String, ByVal defaultMeasures As String)
        Try
            password = Encrypter.EncriptarSQLtod("", password, True)
            password2 = Encrypter.EncriptarSQLtod("", password2, True)
            DBServer.Cubos_Update(idCubo, url, usuario, password, dominio, url2, usuario2, password2, dominio2, BD, cubo, activo, availableMeasures, defaultMeasures)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    ''' <summary>
    ''' Borra un Cubo de la BBDD
    ''' </summary>
    ''' <param name="id">id Cubo</param>
    ''' <remarks>Llamada desde: Sistema, Tiempo maximo: 0</remarks>
    Public Sub Cubos_Delete(ByVal id As Integer)
        Try
            DBServer.Cubos_Delete(id)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    ''' <summary>
    ''' Devuelve un Cubo de la BBDD
    ''' </summary>
    ''' <param name="id">id Cubo</param>
    ''' <remarks>Llamada desde: Sistema, Tiempo maximo: 0</remarks>
    Public Function Get_Cubo(ByVal id As Integer) As DataSet
        Try
            Return DBServer.Cubos_GetCubo(id)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class
