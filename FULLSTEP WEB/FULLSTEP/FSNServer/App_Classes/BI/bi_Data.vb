﻿Public Class bi_Data
    Inherits Security

    ''' <summary>
    ''' Constructor de la clase 
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si está autenticado</param>
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
    ''' <summary>
    ''' Recupera los UON y los usuarios y crea el treeview de los permisos para el cubo
    ''' </summary>
    ''' <param name="Idioma"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Obtener_UONsPara(ByVal Idioma As String) As List(Of cn_fsTreeViewItem)
        Authenticate()

        Try
            Dim dsDatos As New DataSet
            dsDatos = DBServer.BI_Obtener_UONsPara(Idioma)

            Dim oUONPara As New List(Of cn_fsTreeViewItem)
            Dim item, item0, item0Anterior, item1, item2, itemUsu As cn_fsTreeViewItem
            item0 = Nothing : item1 = Nothing : item2 = Nothing
            Dim UON1ANT As String = ""
            Dim UON2ANT As String = ""
            Dim UON3ANT As String = ""
            item0Anterior = New cn_fsTreeViewItem : item0Anterior.value = ""

            Dim Textos As Dictionary = New Dictionary()
            Textos.LoadData(TiposDeDatos.ModulosIdiomas.Cubos)
            Dim texto As String = Textos.GetText(32)
            Dim itemTodos As cn_fsTreeViewItem = New cn_fsTreeViewItem
            itemTodos.text = texto
            itemTodos.selectable = True
            itemTodos.type = 3
            itemTodos.expanded = True
            itemTodos.children = New List(Of cn_fsTreeViewItem)
            oUONPara.Add(itemTodos)

            Dim NivelActual As Long
            NivelActual = 0
            For Each DR As DataRow In dsDatos.Tables(0).Rows
                If ((IsDBNull(DR("UON1")) AndAlso Not IsDBNull(("PER")))) Then
                    'Añado el item de UON1
                    item = New cn_fsTreeViewItem
                    With item
                        .value = DR("COD")
                        .text = DR("NOM") & " " & DR("APE")
                        .selectable = True
                        .type = 3
                        .nivel = "USU"
                        .expanded = False
                    End With
                    itemTodos.children.Add(item)
                End If

                If (Not IsDBNull(DR("UON1"))) AndAlso DR("UON1") <> UON1ANT Then
                    'Añado el item de UON1
                    item0 = New cn_fsTreeViewItem
                    With item0
                        .value = DR("UON1")
                        .text = DR("DEN1")
                        .selectable = True
                        .type = 3
                        .nivel = "UON1"
                        .expanded = False
                        .children = New List(Of cn_fsTreeViewItem)
                    End With
                    UON1ANT = DR("UON1")
                    item0Anterior = item0
                    NivelActual = 1
                    UON2ANT = ""
                    UON3ANT = ""
                    itemTodos.children.Add(item0)
                End If

                If (Not IsDBNull(DR("UON2"))) AndAlso DR("UON2") <> UON2ANT Then
                    'Añado el item de UON2
                    item1 = New cn_fsTreeViewItem
                    With item1
                        .value = DR("UON2")
                        .text = DR("DEN2")
                        .selectable = True
                        .type = 3
                        .nivel = "UON2"
                        .expanded = False
                        .children = New List(Of cn_fsTreeViewItem)
                    End With
                    UON2ANT = DR("UON2")
                    item0.children.Add(item1)
                    NivelActual = 2
                    UON3ANT = ""
                End If

                If (Not IsDBNull(DR("UON3"))) AndAlso DR("UON3") <> UON3ANT Then
                    'Añado el item de UON3
                    item2 = New cn_fsTreeViewItem
                    With item2
                        .value = DR("UON3")
                        .text = DR("DEN3")
                        .selectable = True
                        .type = 3
                        .nivel = "UON3"
                        .expanded = False
                        .children = New List(Of cn_fsTreeViewItem)
                    End With
                    UON3ANT = DR("UON3")
                    item1.children.Add(item2)
                    NivelActual = 3
                End If

                itemUsu = New cn_fsTreeViewItem
                With itemUsu
                    .value = DR("COD")
                    .text = DR("NOM") & " " & DR("APE")
                    .selectable = True
                    .type = 3
                    .nivel = "USU"
                    .expanded = False
                    .children = New List(Of cn_fsTreeViewItem)
                End With

                Select Case NivelActual
                    Case 1
                        item0.children.Add(itemUsu)
                    Case 2
                        item1.children.Add(itemUsu)
                    Case 3
                        item2.children.Add(itemUsu)
                End Select
            Next

            Return oUONPara
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    ''' <summary>
    ''' Guarda los permisos de acceso al cubo de los usuarios y de las UONs
    ''' </summary>
    ''' <param name="idCubo"></param>
    ''' <param name="idUsu"></param>
    ''' <param name="uon1"></param>
    ''' <param name="uon2"></param>
    ''' <param name="uon3"></param>
    ''' <remarks></remarks>
    Public Sub GuardarPermisosCubo(ByVal idCubo As Integer, ByVal idUsu As String, ByVal uon1 As String, ByVal uon2 As String, ByVal uon3 As String)
        Try
            If (idUsu = "") Then
                DBServer.CubosUon_Add(idCubo, uon1, uon2, uon3)
            Else
                DBServer.CubosUsu_Add(idCubo, idUsu)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    ''' <summary>
    ''' Guarda permisos para el cubo de todas las UONs
    ''' </summary>
    ''' <param name="idCubo"></param>
    ''' <remarks></remarks>
    Public Sub GuardarPermisosCuboTodosUON(idCubo)
        Try
            DBServer.CubosUon_FullAdd(idCubo)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    ''' <summary>
    ''' Recupera los permisos para el cubo con idCubo de los usuarios y de las UONs y crea el id correspondiente
    ''' </summary>
    ''' <param name="idCubo"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ObtenerPermisosUON(ByVal idCubo) As String()
        Try
            Dim mDataUsu As DataSet = DBServer.CubosUsu_Load(idCubo)
            Dim mDataUON As DataSet = DBServer.CubosUon_Load(idCubo)
            Dim Usuarios As String()
            ReDim Usuarios(mDataUsu.Tables(0).Rows.Count + mDataUON.Tables(0).Rows.Count)

            If Not mDataUsu.Tables(0).Rows.Count = 0 Then
                For i = 0 To mDataUsu.Tables(0).Rows.Count - 1
                    Usuarios(i) = "CUBO_" & idCubo & "_USU_" & mDataUsu.Tables(0).Rows(i).Item("COD") & " || " & mDataUsu.Tables(0).Rows(i).Item("NOM") & " " & mDataUsu.Tables(0).Rows(i).Item("APE") & " " & mDataUsu.Tables(0).Rows(i).Item("EMAIL")
                Next
            End If

            If Not mDataUON.Tables(0).Rows.Count = 0 Then
                For i = (mDataUsu.Tables(0).Rows.Count) To (mDataUON.Tables(0).Rows.Count + mDataUsu.Tables(0).Rows.Count - 1)
                    Usuarios(i) = "CUBO_" & idCubo & "_UON1_" & mDataUON.Tables(0).Rows(i - mDataUsu.Tables(0).Rows.Count).Item("UON1")
                    If (Not mDataUON.Tables(0).Rows(i - mDataUsu.Tables(0).Rows.Count).Item("UON2") = "") Then
                        Usuarios(i) = Usuarios(i) & "_UON2_" & mDataUON.Tables(0).Rows(i - mDataUsu.Tables(0).Rows.Count).Item("UON2")
                    End If
                    If (Not mDataUON.Tables(0).Rows(i - mDataUsu.Tables(0).Rows.Count).Item("UON3") = "") Then
                        Usuarios(i) = Usuarios(i) & "_UON3_" & mDataUON.Tables(0).Rows(i - mDataUsu.Tables(0).Rows.Count).Item("UON3")
                    End If
                    Usuarios(i) = Usuarios(i) & " || " & mDataUON.Tables(0).Rows(i - mDataUsu.Tables(0).Rows.Count).Item("DEN1") & " " & mDataUON.Tables(0).Rows(i - mDataUsu.Tables(0).Rows.Count).Item("DEN2") & " " & mDataUON.Tables(0).Rows(i - mDataUsu.Tables(0).Rows.Count).Item("DEN3")
                Next
            End If

            Return Usuarios
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Sub EliminarPermisoUON(ByVal idCubo, ByVal uon1, ByVal uon2, ByVal uon3)
        DBServer.CubosUon_Delete(idCubo, uon1, uon2, uon3)
    End Sub
    Public Sub EliminarPermisoUsu(ByVal idCubo, ByVal idUsu)
        DBServer.CubosUsu_Delete(idCubo, idUsu)
    End Sub
    Public Sub CopiarTodosPermisos(ByVal idCuboACopiar, ByVal idCubo)
        Dim ds As DataSet = DBServer.CubosUsu_Load(idCuboACopiar)
        For i = 0 To ds.Tables(0).Rows.Count - 1
            DBServer.CubosUsu_Add(idCubo, ds.Tables(0).Rows(i).Item("COD"))
        Next
        Dim ds2 As DataSet = DBServer.CubosUon_Load(idCuboACopiar)
        For i = 0 To ds2.Tables(0).Rows.Count - 1
            DBServer.CubosUon_Add(idCubo, ds2.Tables(0).Rows(i).Item("UON1"), ds2.Tables(0).Rows(i).Item("UON2"), ds2.Tables(0).Rows(i).Item("UON3"))
        Next
    End Sub
End Class
