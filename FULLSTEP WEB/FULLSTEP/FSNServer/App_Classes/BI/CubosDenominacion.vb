﻿Public Class CuboDenominacion
    Inherits Security

    Private msID As Long
    Private msIdi As String
    Private msDen As String

    ''' <summary>
    ''' Constructor de la clase CuboDenominacion
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si está autenticado</param>
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
    Property Id() As Long
        Get
            Id = msID
        End Get
        Set(ByVal Value As Long)
            msID = Value
        End Set
    End Property
    Property Idi() As String
        Get
            Idi = msIdi
        End Get
        Set(value As String)
            msIdi = value
        End Set
    End Property
    Property Den() As String
        Get
            Den = msDen
        End Get
        Set(ByVal Value As String)
            msDen = Value
        End Set
    End Property
End Class

Public Class CubosDenominacion
    Inherits Security
    Private moCubosDen As Collection
    Private mData As DataSet
    Private _Denominaciones As Dictionary(Of String, String)

    ''' <summary>
    ''' Constructor de la clase CubosDenominacion
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si está autenticado</param>
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
    ReadOnly Property data() As DataSet
        Get
            Return mData
        End Get
    End Property
    Public Property Denominaciones() As Dictionary(Of String, String)
        Get
            Return _Denominaciones
        End Get
        Set(ByVal value As Dictionary(Of String, String))
            _Denominaciones = value
        End Set
    End Property
    Public ReadOnly Property CubosDen() As Collection
        Get
            Return moCubosDen
        End Get
    End Property
    ''' <summary>
    ''' Carga las denominaciones por defecto
    ''' </summary>
    Public Sub EmptyLoad()
        If Denominaciones Is Nothing Then Denominaciones = New Dictionary(Of String, String)
       
        Dim id As String = " "
        Dim idi As String = "SPA"
        Dim den As String = " "
        Denominaciones.Add(idi, den)
        idi = "ENG"
        Denominaciones.Add(idi, den)
        idi = "GER"
        Denominaciones.Add(idi, den)
        idi = "FRA"
        Denominaciones.Add(idi, den)
    End Sub
    ''' <summary>
    ''' Carga las denominaciones de los cubos en los distintos idiomas
    ''' </summary>
    Public Sub Load()
        mData = DBServer.CubosDen_Load()
        If Denominaciones Is Nothing Then
            Denominaciones = New Dictionary(Of String, String)
        End If
        For Each row As DataRow In mData.Tables(0).Rows
            Dim id As String = row("ID_CUBO")
            Dim idi As String = row("IDIOMA")
            Dim den As String = row("DEN")
            Denominaciones.Add(idi, den)
        Next
    End Sub
    ''' <summary>
    ''' Carga las denominaciones de los cubos en los distintos idiomas
    ''' </summary>
    Public Function Load(ByVal idCubo As String) As Dictionary(Of String, String)
        mData = DBServer.CubosDen_Load_By_ID(idCubo)
        If Denominaciones Is Nothing Then
            Denominaciones = New Dictionary(Of String, String)
        End If
        For Each row As DataRow In mData.Tables(0).Rows
            Dim id As String = row("ID_CUBO")
            Dim idi As String = row("IDIOMA")
            Dim den As String = row("DEN")
            Denominaciones.Add(idi, den)
        Next
        Return Denominaciones
    End Function
    ''' <summary>
    ''' Inserta una nueva denominacion del Cubo en la BBDD
    ''' </summary>
    ''' <param name="idCubo">id Cubo</param>
    ''' <param name="denominaciones">denominaciones en distintos idiomas Cubo</param> 
    ''' <remarks>Llamada desde: Sistema, Tiempo maximo: 0</remarks>
    Public Sub CubosDenominacion_Insert(ByVal idCubo As Integer, ByVal denominaciones As Dictionary(Of String, String))
        Try
            Dim dr As DataRow
            Dim dtDenominacionesCubo As New DataTable
            dtDenominacionesCubo.Columns.Add("IDIOMA", System.Type.GetType("System.String"))
            dtDenominacionesCubo.Columns.Add("DEN", System.Type.GetType("System.String"))
            For Each item As KeyValuePair(Of String, String) In denominaciones
                dr = dtDenominacionesCubo.NewRow
                dr("IDIOMA") = item.Key
                dr("DEN") = item.Value
                dtDenominacionesCubo.Rows.Add(dr)
            Next
            DBServer.CubosDen_Add(idCubo, dtDenominacionesCubo)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    ''' <summary>
    ''' Inserta una nueva denominacion del Cubo en la BBDD
    ''' </summary>
    ''' <param name="idCubo">id Cubo</param>
    ''' <param name="denominaciones">denominaciones en distintos idiomas Cubo</param> 
    ''' <remarks>Llamada desde: Sistema, Tiempo maximo: 0</remarks>
    Public Sub CubosDenominacion_Update(ByVal idCubo As Integer, ByVal denominaciones As Dictionary(Of String, String))
        Try
            Dim dr As DataRow
            Dim dtDenominacionesCubo As New DataTable
            dtDenominacionesCubo.Columns.Add("IDIOMA", System.Type.GetType("System.String"))
            dtDenominacionesCubo.Columns.Add("DEN", System.Type.GetType("System.String"))
            For Each item As KeyValuePair(Of String, String) In denominaciones
                dr = dtDenominacionesCubo.NewRow
                dr("IDIOMA") = item.Key
                dr("DEN") = item.Value
                dtDenominacionesCubo.Rows.Add(dr)
            Next
            DBServer.CubosDen_Update(idCubo, dtDenominacionesCubo)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub CubosDenominacion_Delete(ByVal id As Integer)
        Try
            DBServer.CubosDen_Delete(id)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    ''' <summary>
    ''' Devuelve los cubos de la BBDD en la que tienen permiso el usuario 
    ''' </summary>
    ''' <param name="idUsu">id Usuario</param>
    ''' <param name="idi">idioma</param>
    ''' <param name="uon1">un. organ. nivel 1</param>
    ''' <param name="uon2">un. organ. nivel 2</param>
    ''' <param name="uon3">un. organ. nivel 3</param>
    ''' <remarks>Llamada desde: Sistema, Tiempo maximo: 0</remarks>
    Public Function ObtenerCubosPermisosUsu(ByVal idUsu, ByVal idi, ByVal uon1, ByVal uon2, ByVal uon3)
        Dim data As DataSet = DBServer.CubosUsu_Load(idUsu, idi)
        Dim data2 As DataSet = DBServer.CubosUon_Load(idUsu, idi, uon1, uon2, uon3)
        data.Merge(data2)
        If Not data.Tables(0).Rows.Count = 0 Then
            For i = 0 To data.Tables(0).Rows.Count - 1
                Me.Add(data.Tables(0).Rows(i).Item("ID_CUBO").ToString(), data.Tables(0).Rows(i).Item("DEN").ToString())
            Next
        End If
        Return data
    End Function
    Private Function Add(idCubo As String, den As String) As Integer
        Dim dsDatos As New DataSet
        Dim oCuboDen As New CuboDenominacion(DBServer, mIsAuthenticated)
        If moCubosDen Is Nothing Then
            moCubosDen = New Collection
        End If
        Dim encontrado As Boolean = False
        oCuboDen.Id = idCubo
        oCuboDen.Den = den
        For Each cuboDen As CuboDenominacion In moCubosDen
            If (oCuboDen.Id = cuboDen.Id.ToString() And oCuboDen.Den = cuboDen.Den) Then
                encontrado = True
            End If
        Next
        If (Not encontrado) Then
            moCubosDen.Add(oCuboDen, idCubo)
            encontrado = False
        End If
    End Function
End Class