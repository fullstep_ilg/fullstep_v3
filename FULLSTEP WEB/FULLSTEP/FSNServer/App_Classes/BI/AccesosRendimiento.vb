﻿Imports System.Web

Public Class AccesoRendimiento
    Inherits Security
    Public idKPI As Integer
    Public denKPI As String
    Public fechaHoraKPI As String
    Public rendimientoKPI As Double
    Public numAccesosKPI As Integer
    Public targetKPI As Double
    Public tiempoMaxKPI As Double
    Public tiempoMinKPI As Double
    Public tiempoAvgKPI As Double
    Public tiempoAccesoKPI As Double
    Public usuAccesoKPI As String
    Public uonAccesoKPI As String
    Public radialGaugeValue As Double
    Public media As Double
    Public tiempo As TimeSpan

    ''' <summary>
    ''' Constructor de la clase AccesoRendimiento
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si está autenticado</param>
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
End Class

Public Class AccesosRendimiento
    Inherits Security
    Private moAccesosRendimiento As Collection
    Private mData As DataSet
    Private mData1 As DataSet
    Private mDataTotal As DataSet
    Private oAccesoRendimiento As AccesoRendimiento
    Private textosBI As ArrayList

    ReadOnly Property data() As DataSet
        Get
            Return mData
        End Get
    End Property
    Public ReadOnly Property AccesosRendimiento() As Collection
        Get
            Return moAccesosRendimiento
        End Get
    End Property
    ''' <summary>
    ''' Constructor de la clase AccesosRendimiento
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si está autenticado</param>
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
    Public Function KPILoad(ByVal idi As String)
        Authenticate()
        Return DBServer.KPI_Load(idi)
    End Function
    Public Sub LoadPorRango(ByVal horaInicio As DateTime, ByVal horaFin As DateTime, ByVal entorno As String, ByVal pyme As String, ByVal rango As String)
        Select Case UCase(rango)
            Case "HORA"
                mData = DBServer.AccesosKPI_Load_Por_Hora(horaInicio, horaFin, entorno, pyme)
                If Not mData Is Nothing AndAlso Not mData.Tables(0).Rows.Count = 0 Then
                    For i = 0 To mData.Tables(0).Rows.Count - 1
                        Me.AddPerformanceHoursAndMins(mData.Tables(0).Rows(i).Item("FECHA"), mData.Tables(0).Rows(i).Item("RENDIMIENTO"))
                    Next
                End If
            Case "DIA"
                mData = DBServer.AccesosKPI_Load_Por_Dia(horaInicio, horaFin, entorno, pyme)
                If Not mData Is Nothing AndAlso Not mData.Tables(0).Rows.Count = 0 Then
                    For i = 0 To mData.Tables(0).Rows.Count - 1
                        Me.AddPerformanceHours(mData.Tables(0).Rows(i).Item("FECHA"), mData.Tables(0).Rows(i).Item("RENDIMIENTO"))
                    Next
                End If
            Case "SEMANA"
                horaFin = New DateTime(horaFin.Year, horaFin.Month, horaFin.Day, 23, 59, 59)
                mData = DBServer.AccesosKPI_Load_Por_Semana(horaInicio, horaFin, entorno, pyme)
                If Not mData Is Nothing AndAlso Not mData.Tables(0).Rows.Count = 0 Then
                    For i = 0 To mData.Tables(0).Rows.Count - 1
                        Me.AddPerformanceDaysInWeek(horaInicio, mData.Tables(0).Rows(i).Item("FECHA"), mData.Tables(0).Rows(i).Item("RENDIMIENTO"))
                    Next
                End If
            Case "MES"
                mData = DBServer.AccesosKPI_Load_Por_Mes(horaInicio, horaFin, entorno, pyme)
                If Not mData Is Nothing AndAlso Not mData.Tables(0).Rows.Count = 0 Then
                    For i = 0 To mData.Tables(0).Rows.Count - 1
                        Me.AddPerformanceDays(mData.Tables(0).Rows(i).Item("FECHA"), mData.Tables(0).Rows(i).Item("RENDIMIENTO"))
                    Next
                End If
            Case "ANYO"
                mData = DBServer.AccesosKPI_Load_Por_Anyo(horaInicio, horaFin, entorno, pyme)
                If (textosBI Is Nothing) Then
                    Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
                    textosBI = DBServer.TextoBi_Carga(181, FSNUser.IdiomaCod)
                End If
                If Not mData Is Nothing AndAlso Not mData.Tables(0).Rows.Count = 0 Then
                    For i = 0 To mData.Tables(0).Rows.Count - 1
                        Dim mesAnyo As String = String.Empty
                        Select Case i
                            Case 0 : mesAnyo = textosBI(0) '"Jan"
                            Case 1 : mesAnyo = textosBI(1) '"Feb"
                            Case 2 : mesAnyo = textosBI(2) '"Mar"
                            Case 3 : mesAnyo = textosBI(3) '"Apr"
                            Case 4 : mesAnyo = textosBI(4) '"May"
                            Case 5 : mesAnyo = textosBI(5) '"Jun"
                            Case 6 : mesAnyo = textosBI(6) '"Jul"
                            Case 7 : mesAnyo = textosBI(7) '"Aug"
                            Case 8 : mesAnyo = textosBI(8) '"Sep"
                            Case 9 : mesAnyo = textosBI(9) '"Oct"
                            Case 10 : mesAnyo = textosBI(10) '"Nov"
                            Case 11 : mesAnyo = textosBI(11) '"Dec"
                        End Select
                        Me.AddPerformanceMonths(mesAnyo, mData.Tables(0).Rows(i).Item("RENDIMIENTO"))
                    Next i
                End If
        End Select
    End Sub
    'Horas y minutos para la grafica de rendimiento
    Friend Sub AddPerformanceHoursAndMins(ByVal fechaHoraKPI As String, ByVal rendimientoKPI As Double)
        Dim oAccesoRendimiento As New AccesoRendimiento(DBServer, mIsAuthenticated)

        If moAccesosRendimiento Is Nothing Then moAccesosRendimiento = New Collection

        oAccesoRendimiento.fechaHoraKPI = fechaHoraKPI
        oAccesoRendimiento.rendimientoKPI = rendimientoKPI

        moAccesosRendimiento.Add(oAccesoRendimiento)
    End Sub
    'Horas para la grafica de rendimiento
    Friend Sub AddPerformanceHours(ByVal fechaHoraKPI As String, ByVal rendimientoKPI As Double)
        Dim oAccesoRendimiento As New AccesoRendimiento(DBServer, mIsAuthenticated)

        If moAccesosRendimiento Is Nothing Then moAccesosRendimiento = New Collection

        oAccesoRendimiento.fechaHoraKPI = fechaHoraKPI
        oAccesoRendimiento.rendimientoKPI = rendimientoKPI

        moAccesosRendimiento.Add(oAccesoRendimiento)
    End Sub
    'Dias de la semana para la grafica de rendimiento(semana)
    Friend Sub AddPerformanceDaysInWeek(ByVal fechaDesde As DateTime, ByVal fechaHoraKPI As String, ByVal rendimientoKPI As Double)
        Dim oAccesoRendimiento As New AccesoRendimiento(DBServer, mIsAuthenticated)

        If moAccesosRendimiento Is Nothing Then moAccesosRendimiento = New Collection

        Dim dia() = fechaHoraKPI.Split("/")
        Dim diaInt As Integer
        Integer.TryParse(dia(2), diaInt)
        Dim fechaDate = New DateTime(dia(2), dia(1), dia(0))
        Dim diaSemana = (fechaDate - fechaDesde).TotalDays

        If (textosBI Is Nothing) Then
            Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
            textosBI = DBServer.TextoBi_Carga(181, FSNUser.IdiomaCod)
        End If

        If (diaSemana = 0) Then
            oAccesoRendimiento.fechaHoraKPI = textosBI(41) '"Mon"
        ElseIf (diaSemana = 1) Then
            oAccesoRendimiento.fechaHoraKPI = textosBI(42) '"Tue"
        ElseIf (diaSemana = 2) Then
            oAccesoRendimiento.fechaHoraKPI = textosBI(43) '"Wed"
        ElseIf (diaSemana = 3) Then
            oAccesoRendimiento.fechaHoraKPI = textosBI(44) '"Thu"
        ElseIf (diaSemana = 4) Then
            oAccesoRendimiento.fechaHoraKPI = textosBI(45) '"Fri"
        ElseIf (diaSemana = 5) Then
            oAccesoRendimiento.fechaHoraKPI = textosBI(46) '"Sat"
        ElseIf (diaSemana = 6) Then
            oAccesoRendimiento.fechaHoraKPI = textosBI(47) '"Sun"
        End If
        oAccesoRendimiento.rendimientoKPI = rendimientoKPI

        moAccesosRendimiento.Add(oAccesoRendimiento)
    End Sub
    'Dias para la grafica de rendimiento(mes)
    Friend Sub AddPerformanceDays(ByVal fechaHoraKPI As String, ByVal rendimientoKPI As Double)
        Dim oAccesoRendimiento As New AccesoRendimiento(DBServer, mIsAuthenticated)
        Dim dia() = fechaHoraKPI.Split("/")

        If moAccesosRendimiento Is Nothing Then moAccesosRendimiento = New Collection

        oAccesoRendimiento.fechaHoraKPI = dia(0)
        oAccesoRendimiento.rendimientoKPI = rendimientoKPI

        moAccesosRendimiento.Add(oAccesoRendimiento)
    End Sub
    'Meses para la grafica de rendimiento(anyo)
    Friend Sub AddPerformanceMonths(ByVal fechaHoraKPI As String, ByVal rendimientoKPI As Double)
        Dim oAccesoRendimiento As New AccesoRendimiento(DBServer, mIsAuthenticated)

        If moAccesosRendimiento Is Nothing Then moAccesosRendimiento = New Collection

        oAccesoRendimiento.fechaHoraKPI = fechaHoraKPI
        oAccesoRendimiento.rendimientoKPI = rendimientoKPI

        moAccesosRendimiento.Add(oAccesoRendimiento)
    End Sub
    Public Sub LoadPorRangoRadialGauge(ByVal horaInicio As DateTime, ByVal horaFin As DateTime, ByVal entorno As String, ByVal pyme As String, ByVal rango As String)
        Select Case UCase(rango)
            Case "HORA"
                mData = DBServer.AccesosKPI_Load_Gauge_Hora(horaInicio, horaFin, entorno, pyme)
                If Not mData Is Nothing AndAlso Not mData.Tables(0).Rows.Count = 0 Then
                    For i = 0 To mData.Tables(0).Rows.Count - 1
                        Me.AddRadialGaudeMedia(mData.Tables(0).Rows(i).Item("RENDIMIENTO"))
                    Next
                End If
            Case "DIA"
                mData = DBServer.AccesosKPI_Load_Gauge_Dia(horaInicio, horaFin, entorno, pyme)
                If Not mData Is Nothing AndAlso Not mData.Tables(0).Rows.Count = 0 Then
                    For i = 0 To mData.Tables(0).Rows.Count - 1
                        Me.AddRadialGaudeMedia(mData.Tables(0).Rows(i).Item("RENDIMIENTO"))
                    Next
                End If
            Case "SEMANA"
                horaFin = New DateTime(horaFin.Year, horaFin.Month, horaFin.Day, 23, 59, 59)
                mData = DBServer.AccesosKPI_Load_Gauge_Semana(horaInicio, horaFin, entorno, pyme)
                If Not mData Is Nothing AndAlso Not mData.Tables(0).Rows.Count = 0 Then
                    For i = 0 To mData.Tables(0).Rows.Count - 1
                        Me.AddRadialGaudeMedia(mData.Tables(0).Rows(i).Item("RENDIMIENTO"))
                    Next
                End If
            Case "MES"
                mData = DBServer.AccesosKPI_Load_Gauge_Mes(horaInicio, horaFin, entorno, pyme)
                If Not mData Is Nothing AndAlso Not mData.Tables(0).Rows.Count = 0 Then
                    For i = 0 To mData.Tables(0).Rows.Count - 1
                        Me.AddRadialGaudeMedia(mData.Tables(0).Rows(i).Item("RENDIMIENTO"))
                    Next
                End If
            Case "ANYO"
                mData = DBServer.AccesosKPI_Load_Gauge_Anyo(horaInicio, horaFin, entorno, pyme)
                If Not mData Is Nothing AndAlso Not mData.Tables(0).Rows.Count = 0 Then
                    For i = 0 To mData.Tables(0).Rows.Count - 1
                        Me.AddRadialGaudeMedia(mData.Tables(0).Rows(i).Item("RENDIMIENTO"))
                    Next i
                End If
        End Select
    End Sub
    Public Sub AddRadialGaudeMedia(ByVal radialGaugeValue As Double)
        Dim oAccesoRendimiento As New AccesoRendimiento(DBServer, mIsAuthenticated)
        If moAccesosRendimiento Is Nothing Then
            moAccesosRendimiento = New Collection
        End If
        oAccesoRendimiento.radialGaugeValue = radialGaugeValue
        moAccesosRendimiento.Add(oAccesoRendimiento)
    End Sub
    Public Sub LoadPorRangoKPI(ByVal horaInicio As DateTime, ByVal horaFin As DateTime, ByVal entorno As String, ByVal pyme As String, ByVal rango As String, ByVal kpi As String, ByVal idioma As String)
        Select Case UCase(rango)
            Case "HORA"
                mData = DBServer.AccesosKPI_Load_Por_Hora_KPI(horaInicio, horaFin, entorno, pyme, kpi, idioma)
                If Not mData Is Nothing AndAlso Not mData.Tables(0).Rows.Count = 0 Then
                    Dim maxTargetKPI As Integer = 0
                    For i = 0 To mData.Tables(0).Rows.Count - 1
                        Me.AddActivityHoursAndMinsKPI(mData.Tables(0).Rows(i).Item("FECHA"), mData.Tables(0).Rows(i).Item("KPI"), mData.Tables(0).Rows(i).Item("DEN_KPI"), mData.Tables(0).Rows(i).Item("ACCESOS"), mData.Tables(0).Rows(i).Item("MAX_TACCESO"), mData.Tables(0).Rows(i).Item("MIN_TACCESO"), mData.Tables(0).Rows(i).Item("AVG_TACCESO"), mData.Tables(0).Rows(i).Item("TARGET"), mData.Tables(0).Rows(i).Item("RENDIMIENTO"))
                        If mData.Tables(0).Rows(i).Item("TARGET") >= maxTargetKPI Then
                            maxTargetKPI = mData.Tables(0).Rows(i).Item("TARGET")
                        End If
                    Next
                    For Each item As AccesoRendimiento In moAccesosRendimiento
                        item.targetKPI = maxTargetKPI
                    Next
                End If
            Case "DIA"
                mData = DBServer.AccesosKPI_Load_Por_Dia_KPI(horaInicio, horaFin, entorno, pyme, kpi, idioma)
                If Not mData Is Nothing AndAlso Not mData.Tables(0).Rows.Count = 0 Then
                    Dim maxTargetKPI As Integer = 0
                    For i = 0 To mData.Tables(0).Rows.Count - 1
                        Me.AddActivityHoursKPI(mData.Tables(0).Rows(i).Item("FECHA"), mData.Tables(0).Rows(i).Item("KPI"), mData.Tables(0).Rows(i).Item("DEN_KPI"), mData.Tables(0).Rows(i).Item("ACCESOS"), mData.Tables(0).Rows(i).Item("MAX_TACCESO"), mData.Tables(0).Rows(i).Item("MIN_TACCESO"), mData.Tables(0).Rows(i).Item("AVG_TACCESO"), mData.Tables(0).Rows(i).Item("TARGET"), mData.Tables(0).Rows(i).Item("RENDIMIENTO"))
                        If mData.Tables(0).Rows(i).Item("TARGET") >= maxTargetKPI Then
                            maxTargetKPI = mData.Tables(0).Rows(i).Item("TARGET")
                        End If
                    Next
                    For Each item As AccesoRendimiento In moAccesosRendimiento
                        item.targetKPI = maxTargetKPI
                    Next
                End If
            Case "SEMANA"
                horaFin = New DateTime(horaFin.Year, horaFin.Month, horaFin.Day, 23, 59, 59)
                mData = DBServer.AccesosKPI_Load_Por_Semana_KPI(horaInicio, horaFin, entorno, pyme, kpi, idioma)
                If Not mData Is Nothing AndAlso Not mData.Tables(0).Rows.Count = 0 Then
                    Dim maxTargetKPI As Integer = 0
                    For i = 0 To mData.Tables(0).Rows.Count - 1
                        Me.AddActivityDaysInWeekKPI(horaInicio, mData.Tables(0).Rows(i).Item("FECHA"), mData.Tables(0).Rows(i).Item("KPI"), mData.Tables(0).Rows(i).Item("DEN_KPI"), mData.Tables(0).Rows(i).Item("ACCESOS"), mData.Tables(0).Rows(i).Item("MAX_TACCESO"), mData.Tables(0).Rows(i).Item("MIN_TACCESO"), mData.Tables(0).Rows(i).Item("AVG_TACCESO"), mData.Tables(0).Rows(i).Item("TARGET"), mData.Tables(0).Rows(i).Item("RENDIMIENTO"))
                        If mData.Tables(0).Rows(i).Item("TARGET") >= maxTargetKPI Then
                            maxTargetKPI = mData.Tables(0).Rows(i).Item("TARGET")
                        End If
                    Next
                    For Each item As AccesoRendimiento In moAccesosRendimiento
                        item.targetKPI = maxTargetKPI
                    Next
                End If
            Case "MES"
                mData = DBServer.AccesosKPI_Load_Por_Mes_KPI(horaInicio, horaFin, entorno, pyme, kpi, idioma)
                If Not mData Is Nothing AndAlso Not mData.Tables(0).Rows.Count = 0 Then
                    Dim maxTargetKPI As Integer = 0
                    For i = 0 To mData.Tables(0).Rows.Count - 1
                        Me.AddActivityDaysKPI(mData.Tables(0).Rows(i).Item("FECHA"), mData.Tables(0).Rows(i).Item("KPI"), mData.Tables(0).Rows(i).Item("DEN_KPI"), mData.Tables(0).Rows(i).Item("ACCESOS"), mData.Tables(0).Rows(i).Item("MAX_TACCESO"), mData.Tables(0).Rows(i).Item("MIN_TACCESO"), mData.Tables(0).Rows(i).Item("AVG_TACCESO"), mData.Tables(0).Rows(i).Item("TARGET"), mData.Tables(0).Rows(i).Item("RENDIMIENTO"))
                        If mData.Tables(0).Rows(i).Item("TARGET") >= maxTargetKPI Then
                            maxTargetKPI = mData.Tables(0).Rows(i).Item("TARGET")
                        End If
                    Next
                    For Each item As AccesoRendimiento In moAccesosRendimiento
                        item.targetKPI = maxTargetKPI
                    Next
                End If
            Case "ANYO"
                mData = DBServer.AccesosKPI_Load_Por_Anyo_KPI(horaInicio, horaFin, entorno, pyme, kpi, idioma)
                If (textosBI Is Nothing) Then
                    Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
                    textosBI = DBServer.TextoBi_Carga(181, FSNUser.IdiomaCod)
                End If
                If Not mData Is Nothing AndAlso Not mData.Tables(0).Rows.Count = 0 Then
                    Dim maxTargetKPI As Integer = 0
                    For i = 0 To mData.Tables(0).Rows.Count - 1
                        Dim mesAnyo As String = String.Empty
                        Select Case i
                            Case 0 : mesAnyo = textosBI(0) '"Jan"
                            Case 1 : mesAnyo = textosBI(1) '"Feb"
                            Case 2 : mesAnyo = textosBI(2) '"Mar"
                            Case 3 : mesAnyo = textosBI(3) '"Apr"
                            Case 4 : mesAnyo = textosBI(4) '"May"
                            Case 5 : mesAnyo = textosBI(5) '"Jun"
                            Case 6 : mesAnyo = textosBI(6) '"Jul"
                            Case 7 : mesAnyo = textosBI(7) '"Aug"
                            Case 8 : mesAnyo = textosBI(8) '"Sep"
                            Case 9 : mesAnyo = textosBI(9) '"Oct"
                            Case 10 : mesAnyo = textosBI(10) '"Nov"
                            Case 11 : mesAnyo = textosBI(11) '"Dec"
                        End Select
                        Me.AddActivityMonthsKPI(mesAnyo, mData.Tables(0).Rows(i).Item("KPI"), mData.Tables(0).Rows(i).Item("DEN_KPI"), mData.Tables(0).Rows(i).Item("ACCESOS"), mData.Tables(0).Rows(i).Item("MAX_TACCESO"), mData.Tables(0).Rows(i).Item("MIN_TACCESO"), mData.Tables(0).Rows(i).Item("AVG_TACCESO"), mData.Tables(0).Rows(i).Item("TARGET"), mData.Tables(0).Rows(i).Item("RENDIMIENTO"))
                        If mData.Tables(0).Rows(i).Item("TARGET") >= maxTargetKPI Then
                            maxTargetKPI = mData.Tables(0).Rows(i).Item("TARGET")
                        End If
                    Next i
                    For Each item As AccesoRendimiento In moAccesosRendimiento
                        item.targetKPI = maxTargetKPI
                    Next
                End If
        End Select
    End Sub
    'Horas y minutos para la grafica de actividad reciente
    Friend Sub AddActivityHoursAndMinsKPI(ByVal fechaHoraKPI As String, ByVal idKPI As Integer, ByVal denKPI As String, ByVal numAccesosKPI As Integer, ByVal tiempoMaxKPI As Double, ByVal tiempoMinKPI As Double, ByVal tiempoAvgKPI As Double, ByVal targetKPI As Double, ByVal rendimientoKPI As Double)
        Dim oAccesoRendimiento As New AccesoRendimiento(DBServer, mIsAuthenticated)

        If moAccesosRendimiento Is Nothing Then moAccesosRendimiento = New Collection

        oAccesoRendimiento.fechaHoraKPI = fechaHoraKPI
        oAccesoRendimiento.idKPI = idKPI
        oAccesoRendimiento.denKPI = denKPI
        oAccesoRendimiento.numAccesosKPI = numAccesosKPI
        oAccesoRendimiento.tiempoMaxKPI = tiempoMaxKPI
        oAccesoRendimiento.tiempoMinKPI = tiempoMinKPI
        oAccesoRendimiento.tiempoAvgKPI = tiempoAvgKPI
        oAccesoRendimiento.targetKPI = targetKPI
        oAccesoRendimiento.rendimientoKPI = rendimientoKPI

        moAccesosRendimiento.Add(oAccesoRendimiento)
    End Sub
    'Horas para la grafica de actividad reciente
    Friend Sub AddActivityHoursKPI(ByVal fechaHoraKPI As String, ByVal idKPI As Integer, ByVal denKPI As String, ByVal numAccesosKPI As Integer, ByVal tiempoMaxKPI As Double, ByVal tiempoMinKPI As Double, ByVal tiempoAvgKPI As Double, ByVal targetKPI As Double, ByVal rendimientoKPI As Double)
        Dim oAccesoRendimiento As New AccesoRendimiento(DBServer, mIsAuthenticated)

        If moAccesosRendimiento Is Nothing Then moAccesosRendimiento = New Collection

        oAccesoRendimiento.fechaHoraKPI = fechaHoraKPI
        oAccesoRendimiento.idKPI = idKPI
        oAccesoRendimiento.denKPI = denKPI
        oAccesoRendimiento.numAccesosKPI = numAccesosKPI
        oAccesoRendimiento.tiempoMaxKPI = tiempoMaxKPI
        oAccesoRendimiento.tiempoMinKPI = tiempoMinKPI
        oAccesoRendimiento.tiempoAvgKPI = tiempoAvgKPI
        oAccesoRendimiento.targetKPI = targetKPI
        oAccesoRendimiento.rendimientoKPI = rendimientoKPI

        moAccesosRendimiento.Add(oAccesoRendimiento)
    End Sub
    'Dias de la semana para la grafica de actividad reciente(semana)
    Friend Sub AddActivityDaysInWeekKPI(ByVal fechaDesde As DateTime, ByVal fechaHoraKPI As String, ByVal idKPI As Integer, ByVal denKPI As String, ByVal numAccesosKPI As Integer, ByVal tiempoMaxKPI As Double, ByVal tiempoMinKPI As Double, ByVal tiempoAvgKPI As Double, ByVal targetKPI As Double, ByVal rendimientoKPI As Double)
        Dim oAccesoRendimiento As New AccesoRendimiento(DBServer, mIsAuthenticated)

        If moAccesosRendimiento Is Nothing Then moAccesosRendimiento = New Collection

        Dim dia() = fechaHoraKPI.Split("/")
        Dim diaInt As Integer
        Integer.TryParse(dia(2), diaInt)
        Dim fechaDate = New DateTime(dia(2), dia(1), dia(0))
        Dim diaSemana = (fechaDate - fechaDesde).TotalDays

        If (textosBI Is Nothing) Then
            Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
            textosBI = DBServer.TextoBi_Carga(181, FSNUser.IdiomaCod)
        End If

        If (diaSemana = 0) Then
            oAccesoRendimiento.fechaHoraKPI = textosBI(41) '"Mon"
        ElseIf (diaSemana = 1) Then
            oAccesoRendimiento.fechaHoraKPI = textosBI(42) '"Tue"
        ElseIf (diaSemana = 2) Then
            oAccesoRendimiento.fechaHoraKPI = textosBI(43) '"Wed"
        ElseIf (diaSemana = 3) Then
            oAccesoRendimiento.fechaHoraKPI = textosBI(44) '"Thu"
        ElseIf (diaSemana = 4) Then
            oAccesoRendimiento.fechaHoraKPI = textosBI(45) '"Fri"
        ElseIf (diaSemana = 5) Then
            oAccesoRendimiento.fechaHoraKPI = textosBI(46) '"Sat"
        ElseIf (diaSemana = 6) Then
            oAccesoRendimiento.fechaHoraKPI = textosBI(47) '"Sun"
        End If
        oAccesoRendimiento.idKPI = idKPI
        oAccesoRendimiento.denKPI = denKPI
        oAccesoRendimiento.numAccesosKPI = numAccesosKPI
        oAccesoRendimiento.tiempoMaxKPI = tiempoMaxKPI
        oAccesoRendimiento.tiempoMinKPI = tiempoMinKPI
        oAccesoRendimiento.tiempoAvgKPI = tiempoAvgKPI
        oAccesoRendimiento.targetKPI = targetKPI
        oAccesoRendimiento.rendimientoKPI = rendimientoKPI

        moAccesosRendimiento.Add(oAccesoRendimiento)
    End Sub
    'Dias para la grafica de actividad reciente(mes)
    Friend Sub AddActivityDaysKPI(ByVal fechaHoraKPI As String, ByVal idKPI As Integer, ByVal denKPI As String, ByVal numAccesosKPI As Integer, ByVal tiempoMaxKPI As Double, ByVal tiempoMinKPI As Double, ByVal tiempoAvgKPI As Double, ByVal targetKPI As Double, ByVal rendimientoKPI As Double)
        Dim oAccesoRendimiento As New AccesoRendimiento(DBServer, mIsAuthenticated)
        Dim dia() = fechaHoraKPI.Split("/")

        If moAccesosRendimiento Is Nothing Then  moAccesosRendimiento = New Collection

        oAccesoRendimiento.fechaHoraKPI = dia(0)
        oAccesoRendimiento.idKPI = idKPI
        oAccesoRendimiento.denKPI = denKPI
        oAccesoRendimiento.numAccesosKPI = numAccesosKPI
        oAccesoRendimiento.tiempoMaxKPI = tiempoMaxKPI
        oAccesoRendimiento.tiempoMinKPI = tiempoMinKPI
        oAccesoRendimiento.tiempoAvgKPI = tiempoAvgKPI
        oAccesoRendimiento.targetKPI = targetKPI
        oAccesoRendimiento.rendimientoKPI = rendimientoKPI

        moAccesosRendimiento.Add(oAccesoRendimiento)
    End Sub
    'Meses para la grafica de actividad reciente(anyo)
    Friend Sub AddActivityMonthsKPI(ByVal fechaHoraKPI As String, ByVal idKPI As Integer, ByVal denKPI As String, ByVal numAccesosKPI As Integer, ByVal tiempoMaxKPI As Double, ByVal tiempoMinKPI As Double, ByVal tiempoAvgKPI As Double, ByVal targetKPI As Double, ByVal rendimientoKPI As Double)
        Dim oAccesoRendimiento As New AccesoRendimiento(DBServer, mIsAuthenticated)

        If moAccesosRendimiento Is Nothing Then moAccesosRendimiento = New Collection

        oAccesoRendimiento.fechaHoraKPI = fechaHoraKPI
        oAccesoRendimiento.idKPI = idKPI
        oAccesoRendimiento.denKPI = denKPI
        oAccesoRendimiento.numAccesosKPI = numAccesosKPI
        oAccesoRendimiento.tiempoMaxKPI = tiempoMaxKPI
        oAccesoRendimiento.tiempoMinKPI = tiempoMinKPI
        oAccesoRendimiento.tiempoAvgKPI = tiempoAvgKPI
        oAccesoRendimiento.targetKPI = targetKPI
        oAccesoRendimiento.rendimientoKPI = rendimientoKPI

        moAccesosRendimiento.Add(oAccesoRendimiento)
    End Sub
    Public Sub LoadListaKPIsPorRango(ByVal horaInicio As DateTime, ByVal horaFin As DateTime, ByVal entorno As String, ByVal pyme As String, ByVal rango As String, ByVal idioma As String)
        Select Case UCase(rango)
            Case "HORA"
                mData = DBServer.AccesosKPI_Load_List_Por_Hora_KPI(horaInicio, horaFin, entorno, pyme, idioma)
            Case "DIA"
                mData = DBServer.AccesosKPI_Load_List_Por_Dia_KPI(horaInicio, horaFin, entorno, pyme, idioma)
            Case "SEMANA"
                horaFin = New DateTime(horaFin.Year, horaFin.Month, horaFin.Day, 23, 59, 59)
                mData = DBServer.AccesosKPI_Load_List_Por_Semana_KPI(horaInicio, horaFin, entorno, pyme, idioma)
            Case "MES"
                mData = DBServer.AccesosKPI_Load_List_Por_Mes_KPI(horaInicio, horaFin, entorno, pyme, idioma)
            Case "ANYO"
                mData = DBServer.AccesosKPI_Load_List_Por_Anyo_KPI(horaInicio, horaFin, entorno, pyme, idioma)
        End Select

        If Not mData Is Nothing AndAlso Not mData.Tables(0).Rows.Count = 0 Then
            For i = 0 To mData.Tables(0).Rows.Count - 1
                If mData.Tables(0).Rows(i).Item("KPI") > 0 Then
                    Me.AddListKPIS(mData.Tables(0).Rows(i).Item("KPI"), mData.Tables(0).Rows(i).Item("DEN_KPI"), mData.Tables(0).Rows(i).Item("ACCESOS"), mData.Tables(0).Rows(i).Item("MAX_TACCESO"), mData.Tables(0).Rows(i).Item("MIN_TACCESO"), mData.Tables(0).Rows(i).Item("AVG_TACCESO"), mData.Tables(0).Rows(i).Item("TARGET"), mData.Tables(0).Rows(i).Item("RENDIMIENTO"))
                End If
            Next
        End If
    End Sub
    'KPIS por hora para el grid de KPIs en el rango
    Friend Sub AddListKPIS(ByVal idKPI As Integer, ByVal denKPI As String, ByVal numAccesosKPI As Integer, ByVal tiempoMaxKPI As Double, ByVal tiempoMinKPI As Double, ByVal tiempoAvgKPI As Double, ByVal targetKPI As Double, ByVal rendimientoKPI As Double)
        Dim oAccesoRendimiento As New AccesoRendimiento(DBServer, mIsAuthenticated)

        If moAccesosRendimiento Is Nothing Then moAccesosRendimiento = New Collection

        oAccesoRendimiento.idKPI = idKPI
        oAccesoRendimiento.denKPI = denKPI
        oAccesoRendimiento.numAccesosKPI = numAccesosKPI
        oAccesoRendimiento.tiempoMaxKPI = Math.Round((tiempoMaxKPI / 1000), 2)
        oAccesoRendimiento.tiempoMinKPI = Math.Round((tiempoMinKPI / 1000), 2)
        oAccesoRendimiento.tiempoAvgKPI = Math.Round((tiempoAvgKPI / 1000), 2)
        oAccesoRendimiento.targetKPI = Math.Round((targetKPI / 1000), 2)
        oAccesoRendimiento.rendimientoKPI = rendimientoKPI

        moAccesosRendimiento.Add(oAccesoRendimiento)
    End Sub
    Public Sub LoadListaPorRangoKPI(ByVal horaInicio As DateTime, ByVal horaFin As DateTime, ByVal entorno As String, ByVal pyme As String, ByVal rango As String, ByVal kpi As String, ByVal idioma As String)
        Dim dMaxTAcceso As Double
        Dim dMinTAcceso As Double
        Dim dAvgTAcceso As Double
        Dim dTarget As Double
        Select Case UCase(rango)
            Case "HORA"
                mData = DBServer.AccesosKPI_Load_Por_Hora_KPI(horaInicio, horaFin, entorno, pyme, kpi, idioma)
                If Not mData Is Nothing AndAlso Not mData.Tables(0).Rows.Count = 0 Then
                    For i = 0 To mData.Tables(0).Rows.Count - 1
                        If (mData.Tables(0).Rows(i).Item("KPI") > 0) AndAlso (mData.Tables(0).Rows(i).Item("ACCESOS") > 0) Then
                            dMaxTAcceso = IIf(IsNumeric(mData.Tables(0).Rows(i).Item("MAX_TACCESO")), Math.Round(mData.Tables(0).Rows(i).Item("MAX_TACCESO") / 1000, 2), 0)
                            dMinTAcceso = IIf(IsNumeric(mData.Tables(0).Rows(i).Item("MIN_TACCESO")), Math.Round(mData.Tables(0).Rows(i).Item("MIN_TACCESO") / 1000, 2), 0)
                            dAvgTAcceso = IIf(IsNumeric(mData.Tables(0).Rows(i).Item("AVG_TACCESO")), Math.Round(mData.Tables(0).Rows(i).Item("AVG_TACCESO") / 1000, 2), 0)
                            dTarget = IIf(IsNumeric(mData.Tables(0).Rows(i).Item("TARGET")), Math.Round(mData.Tables(0).Rows(i).Item("TARGET") / 1000, 2), 0)
                            Me.AddActivityHoursAndMinsKPI(mData.Tables(0).Rows(i).Item("FECHA"), mData.Tables(0).Rows(i).Item("KPI"), mData.Tables(0).Rows(i).Item("DEN_KPI"), mData.Tables(0).Rows(i).Item("ACCESOS"), dMaxTAcceso, dMinTAcceso, dAvgTAcceso, dTarget, mData.Tables(0).Rows(i).Item("RENDIMIENTO"))
                        End If
                    Next
                End If
            Case "DIA"
                mData = DBServer.AccesosKPI_Load_Por_Dia_KPI(horaInicio, horaFin, entorno, pyme, kpi, idioma)
                If Not mData Is Nothing AndAlso Not mData.Tables(0).Rows.Count = 0 Then
                    For i = 0 To mData.Tables(0).Rows.Count - 1
                        If (mData.Tables(0).Rows(i).Item("KPI") > 0) AndAlso (mData.Tables(0).Rows(i).Item("ACCESOS") > 0) Then
                            dMaxTAcceso = IIf(IsNumeric(mData.Tables(0).Rows(i).Item("MAX_TACCESO")), Math.Round(mData.Tables(0).Rows(i).Item("MAX_TACCESO") / 1000, 2), 0)
                            dMinTAcceso = IIf(IsNumeric(mData.Tables(0).Rows(i).Item("MIN_TACCESO")), Math.Round(mData.Tables(0).Rows(i).Item("MIN_TACCESO") / 1000, 2), 0)
                            dAvgTAcceso = IIf(IsNumeric(mData.Tables(0).Rows(i).Item("AVG_TACCESO")), Math.Round(mData.Tables(0).Rows(i).Item("AVG_TACCESO") / 1000, 2), 0)
                            dTarget = IIf(IsNumeric(mData.Tables(0).Rows(i).Item("TARGET")), Math.Round(mData.Tables(0).Rows(i).Item("TARGET") / 1000, 2), 0)
                            Me.AddActivityHoursKPI(mData.Tables(0).Rows(i).Item("FECHA"), mData.Tables(0).Rows(i).Item("KPI"), mData.Tables(0).Rows(i).Item("DEN_KPI"), mData.Tables(0).Rows(i).Item("ACCESOS"), dMaxTAcceso, dMinTAcceso, dAvgTAcceso, dTarget, mData.Tables(0).Rows(i).Item("RENDIMIENTO"))
                        End If
                    Next
                End If
            Case "SEMANA"
                horaFin = New DateTime(horaFin.Year, horaFin.Month, horaFin.Day, 23, 59, 59)
                mData = DBServer.AccesosKPI_Load_Por_Semana_KPI(horaInicio, horaFin, entorno, pyme, kpi, idioma)
                If Not mData Is Nothing AndAlso Not mData.Tables(0).Rows.Count = 0 Then
                    For i = 0 To mData.Tables(0).Rows.Count - 1
                        If (mData.Tables(0).Rows(i).Item("KPI") > 0) AndAlso (mData.Tables(0).Rows(i).Item("ACCESOS") > 0) Then
                            dMaxTAcceso = IIf(IsNumeric(mData.Tables(0).Rows(i).Item("MAX_TACCESO")), Math.Round(mData.Tables(0).Rows(i).Item("MAX_TACCESO") / 1000, 2), 0)
                            dMinTAcceso = IIf(IsNumeric(mData.Tables(0).Rows(i).Item("MIN_TACCESO")), Math.Round(mData.Tables(0).Rows(i).Item("MIN_TACCESO") / 1000, 2), 0)
                            dAvgTAcceso = IIf(IsNumeric(mData.Tables(0).Rows(i).Item("AVG_TACCESO")), Math.Round(mData.Tables(0).Rows(i).Item("AVG_TACCESO") / 1000, 2), 0)
                            dTarget = IIf(IsNumeric(mData.Tables(0).Rows(i).Item("TARGET")), Math.Round(mData.Tables(0).Rows(i).Item("TARGET") / 1000, 2), 0)
                            Me.AddActivityDaysInWeekKPI(horaInicio, mData.Tables(0).Rows(i).Item("FECHA"), mData.Tables(0).Rows(i).Item("KPI"), mData.Tables(0).Rows(i).Item("DEN_KPI"), mData.Tables(0).Rows(i).Item("ACCESOS"), dMaxTAcceso, dMinTAcceso, dAvgTAcceso, dTarget, mData.Tables(0).Rows(i).Item("RENDIMIENTO"))
                        End If
                    Next
                End If
            Case "MES"
                mData = DBServer.AccesosKPI_Load_Por_Mes_KPI(horaInicio, horaFin, entorno, pyme, kpi, idioma)
                If Not mData Is Nothing AndAlso Not mData.Tables(0).Rows.Count = 0 Then
                    For i = 0 To mData.Tables(0).Rows.Count - 1
                        If (mData.Tables(0).Rows(i).Item("KPI") > 0) AndAlso (mData.Tables(0).Rows(i).Item("ACCESOS") > 0) Then
                            dMaxTAcceso = IIf(IsNumeric(mData.Tables(0).Rows(i).Item("MAX_TACCESO")), Math.Round(mData.Tables(0).Rows(i).Item("MAX_TACCESO") / 1000, 2), 0)
                            dMinTAcceso = IIf(IsNumeric(mData.Tables(0).Rows(i).Item("MIN_TACCESO")), Math.Round(mData.Tables(0).Rows(i).Item("MIN_TACCESO") / 1000, 2), 0)
                            dAvgTAcceso = IIf(IsNumeric(mData.Tables(0).Rows(i).Item("AVG_TACCESO")), Math.Round(mData.Tables(0).Rows(i).Item("AVG_TACCESO") / 1000, 2), 0)
                            dTarget = IIf(IsNumeric(mData.Tables(0).Rows(i).Item("TARGET")), Math.Round(mData.Tables(0).Rows(i).Item("TARGET") / 1000, 2), 0)
                            Me.AddActivityDaysKPI(mData.Tables(0).Rows(i).Item("FECHA"), mData.Tables(0).Rows(i).Item("KPI"), mData.Tables(0).Rows(i).Item("DEN_KPI"), mData.Tables(0).Rows(i).Item("ACCESOS"), dMaxTAcceso, dMinTAcceso, dAvgTAcceso, dTarget, mData.Tables(0).Rows(i).Item("RENDIMIENTO"))
                        End If
                    Next
                End If
            Case "ANYO"
                mData = DBServer.AccesosKPI_Load_Por_Anyo_KPI(horaInicio, horaFin, entorno, pyme, kpi, idioma)
                If (textosBI Is Nothing) Then
                    Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
                    textosBI = DBServer.TextoBi_Carga(181, FSNUser.IdiomaCod)
                End If
                If Not mData Is Nothing AndAlso Not mData.Tables(0).Rows.Count = 0 Then
                    For i = 0 To mData.Tables(0).Rows.Count - 1
                        Dim mesAnyo As String = String.Empty
                        Select Case i
                            Case 0 : mesAnyo = textosBI(0) '"Jan"
                            Case 1 : mesAnyo = textosBI(1) '"Feb"
                            Case 2 : mesAnyo = textosBI(2) '"Mar"
                            Case 3 : mesAnyo = textosBI(3) '"Apr"
                            Case 4 : mesAnyo = textosBI(4) '"May"
                            Case 5 : mesAnyo = textosBI(5) '"Jun"
                            Case 6 : mesAnyo = textosBI(6) '"Jul"
                            Case 7 : mesAnyo = textosBI(7) '"Aug"
                            Case 8 : mesAnyo = textosBI(8) '"Sep"
                            Case 9 : mesAnyo = textosBI(9) '"Oct"
                            Case 10 : mesAnyo = textosBI(10) '"Nov"
                            Case 11 : mesAnyo = textosBI(11) '"Dec"
                        End Select
                        If (mData.Tables(0).Rows(i).Item("KPI") > 0) AndAlso (mData.Tables(0).Rows(i).Item("ACCESOS") > 0) Then
                            dMaxTAcceso = IIf(IsNumeric(mData.Tables(0).Rows(i).Item("MAX_TACCESO")), Math.Round(mData.Tables(0).Rows(i).Item("MAX_TACCESO") / 1000, 2), 0)
                            dMinTAcceso = IIf(IsNumeric(mData.Tables(0).Rows(i).Item("MIN_TACCESO")), Math.Round(mData.Tables(0).Rows(i).Item("MIN_TACCESO") / 1000, 2), 0)
                            dAvgTAcceso = IIf(IsNumeric(mData.Tables(0).Rows(i).Item("AVG_TACCESO")), Math.Round(mData.Tables(0).Rows(i).Item("AVG_TACCESO") / 1000, 2), 0)
                            dTarget = IIf(IsNumeric(mData.Tables(0).Rows(i).Item("TARGET")), Math.Round(mData.Tables(0).Rows(i).Item("TARGET") / 1000, 2), 0)
                            Me.AddActivityMonthsKPI(mesAnyo, mData.Tables(0).Rows(i).Item("KPI"), mData.Tables(0).Rows(i).Item("DEN_KPI"), mData.Tables(0).Rows(i).Item("ACCESOS"), dMaxTAcceso, dMinTAcceso, dAvgTAcceso, dTarget, mData.Tables(0).Rows(i).Item("RENDIMIENTO"))
                        End If
                    Next i
                End If
        End Select
    End Sub
    Public Function ExistenRegistrosKPI(ByVal horaInicio As DateTime, ByVal horaFin As DateTime, ByVal entorno As String, ByVal pyme As String, ByVal rango As String, ByVal kpi As String) As Boolean
        mData = DBServer.AccesosKPI_En_Tabla_Accesos_Por_Rango_KPI(horaInicio, horaFin, entorno, pyme, kpi)
        If Not mData Is Nothing AndAlso Not mData.Tables(0).Rows.Count = 0 Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Sub LoadAccesosPorRangoKPI(ByVal horaInicio As DateTime, ByVal horaFin As DateTime, ByVal entorno As String, ByVal pyme As String, ByVal rango As String, ByVal kpi As String)
        mData = DBServer.AccesosKPI_De_Tabla_Accesos_Por_Rango_KPI(horaInicio, horaFin, entorno, pyme, kpi)
        If Not mData Is Nothing AndAlso Not mData.Tables(0).Rows.Count = 0 Then
            For i = 0 To mData.Tables(0).Rows.Count - 1
                Me.AddLastKPIAccessKPI(mData.Tables(0).Rows(i).Item("FECHA"), mData.Tables(0).Rows(i).Item("USUARIO"), mData.Tables(0).Rows(i).Item("UON"), mData.Tables(0).Rows(i).Item("TACCESO"), mData.Tables(0).Rows(i).Item("TARGET"), mData.Tables(0).Rows(i).Item("RENDIMIENTO"))
            Next
        End If
    End Sub
    'Accesos por rango para el grid de accesos KPI en el rango
    Friend Sub AddLastKPIAccessKPI(ByVal fechaHoraKPI As String, ByVal usuAccesoKPI As String, ByVal uonAccesoKPI As String, ByVal tiempoAccesoKPI As Double, ByVal targetKPI As Integer, ByVal rendimientoKPI As Double)
        Dim oAccesoRendimiento As New AccesoRendimiento(DBServer, mIsAuthenticated)

        If moAccesosRendimiento Is Nothing Then moAccesosRendimiento = New Collection

        oAccesoRendimiento.fechaHoraKPI = fechaHoraKPI
        oAccesoRendimiento.usuAccesoKPI = usuAccesoKPI
        oAccesoRendimiento.uonAccesoKPI = uonAccesoKPI
        oAccesoRendimiento.tiempoAccesoKPI = Math.Round(tiempoAccesoKPI, 2)
        oAccesoRendimiento.targetKPI = Math.Round(targetKPI, 2)
        oAccesoRendimiento.rendimientoKPI = Math.Round(rendimientoKPI, 2)

        moAccesosRendimiento.Add(oAccesoRendimiento)
    End Sub
End Class