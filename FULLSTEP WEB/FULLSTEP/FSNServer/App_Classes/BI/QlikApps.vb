﻿Imports System.IO
Imports System.Net
Imports System.Security.Cryptography.X509Certificates
Imports System.Text
Imports System.Web.Script.Serialization

Public Class FSNQlikApps
    Inherits Security

    Private oFSNQlikApps As New Collection
    Private dsData As DataSet
    Private oFSNQlikApp As FSNQlikApp

    ReadOnly Property Data() As DataSet
        Get
            Return dsData
        End Get
    End Property

    Public ReadOnly Property FSNQlikApps() As Collection
        Get
            Return oFSNQlikApps
        End Get
    End Property

    Public Property FSNQlikApp() As FSNQlikApp
        Get
            FSNQlikApp = oFSNQlikApp
        End Get
        Set(value As FSNQlikApp)
            oFSNQlikApp = value
        End Set
    End Property

    ''' <summary>
    ''' Constructor de la clase FSNQlikApps
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si está autenticado</param>
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub

    ''' <summary>
    ''' Devuelve una coleccion con todas las QlikApps de la tabla QLIK_APPS, o solo activas o todas
    ''' </summary>
    ''' <param name="bSoloActivas">Booleano para indicar si obtener todas o solo las activas</param>
    ''' <remarks>
    ''' Llamada desde: QlikApps.aspx.vb
    ''' Tiempo: 0 seg
    ''' </remarks>
    Public Sub ObtenerFSNQlikApps(ByVal bSoloActivas As Boolean)

        dsData = DBServer.QlikApps_Lista_Obtener(bSoloActivas)

        If Not dsData.Tables(0).Rows.Count = 0 Then
            For i As Integer = 0 To dsData.Tables(0).Rows.Count - 1
                Dim oQlikApp As New FSNQlikApp(DBServer, mIsAuthenticated)
                oQlikApp.Id = dsData.Tables(0).Rows(i).Item("ID")
                oQlikApp.Nombre = dsData.Tables(0).Rows(i).Item("NOMBRE")
                oQlikApp.Url = dsData.Tables(0).Rows(i).Item("URL")
                oQlikApp.IdApp = dsData.Tables(0).Rows(i).Item("ID_APP")
                oQlikApp.App = dsData.Tables(0).Rows(i).Item("APP")
                oQlikApp.IdSheet = dsData.Tables(0).Rows(i).Item("ID_SHEET")
                oQlikApp.Sheet = dsData.Tables(0).Rows(i).Item("SHEET")
                oQlikApp.RestAPIPort = dsData.Tables(0).Rows(i).Item("REST_API_PORT")
                oQlikApp.Activa = dsData.Tables(0).Rows(i).Item("ACTIVA")
                oQlikApp.FecAlta = dsData.Tables(0).Rows(i).Item("FEC_ALTA")
                oQlikApp.FecAct = dsData.Tables(0).Rows(i).Item("FECACT")
                Me.oFSNQlikApps.Add(oQlikApp)
            Next i
        End If

    End Sub

    ''' <summary>
    ''' Inserta o modifica una Qlik App en/de la tabla QLIK_APPS
    ''' </summary>
    ''' <returns>Identificador del registro creado o modificado</returns>
    ''' <remarks>
    ''' Llamada desde: QlikApps.aspx.vb
    ''' Tiempo: 0 seg
    ''' </remarks>
    Public Function GuardarFSNQlikApp() As Long
        Dim lResultadoGuardado As Long = 0

        If Me.FSNQlikApp.Id = 0 Then
            lResultadoGuardado = DBServer.QlikApps_QlikApp_Insertar(Me.FSNQlikApp.Id, Me.FSNQlikApp.Nombre, Me.FSNQlikApp.Activa, Me.FSNQlikApp.Url, Me.FSNQlikApp.RestAPIPort, Me.FSNQlikApp.IdApp, Me.FSNQlikApp.App, Me.FSNQlikApp.IdSheet, Me.FSNQlikApp.Sheet)
        ElseIf Me.FSNQlikApp.Id > 0 Then
            lResultadoGuardado = DBServer.QlikApps_QlikApp_Modificar(Me.FSNQlikApp.Id, Me.FSNQlikApp.Nombre, Me.FSNQlikApp.Activa, Me.FSNQlikApp.Url, Me.FSNQlikApp.RestAPIPort, Me.FSNQlikApp.IdApp, Me.FSNQlikApp.App, Me.FSNQlikApp.IdSheet, Me.FSNQlikApp.Sheet)
        End If

        Return lResultadoGuardado
    End Function

    ''' <summary>
    ''' Obtiene la QlikApp indicada de la tabla QLIK_APPS
    ''' </summary>
    ''' <param name="lId">Identificador de la QlikApp en la tabla QLIK_APPS</param>
    ''' <remarks>
    ''' Llamada desde: QlikApps.aspx.vb
    ''' Tiempo: 0 seg
    ''' </remarks>
    Public Sub ObtenerFSNQlikApp(ByVal lId As Long)
        Dim oQlikApp As New FSNQlikApp(DBServer, mIsAuthenticated)

        dsData = DBServer.QlikApps_QlikApp_Obtener(lId)

        If dsData.Tables(0).Rows.Count = 1 Then
            oQlikApp.Id = dsData.Tables(0).Rows(0).Item("ID")
            oQlikApp.Nombre = dsData.Tables(0).Rows(0).Item("NOMBRE")
            oQlikApp.Url = dsData.Tables(0).Rows(0).Item("URL")
            oQlikApp.IdApp = dsData.Tables(0).Rows(0).Item("ID_APP")
            oQlikApp.App = dsData.Tables(0).Rows(0).Item("APP")
            oQlikApp.IdSheet = dsData.Tables(0).Rows(0).Item("ID_SHEET")
            oQlikApp.Sheet = dsData.Tables(0).Rows(0).Item("SHEET")
            oQlikApp.RestAPIPort = dsData.Tables(0).Rows(0).Item("REST_API_PORT")
            oQlikApp.Activa = dsData.Tables(0).Rows(0).Item("ACTIVA")
            oQlikApp.FecAlta = dsData.Tables(0).Rows(0).Item("FEC_ALTA")
            oQlikApp.FecAct = dsData.Tables(0).Rows(0).Item("FECACT")
        End If

        Me.FSNQlikApp = oQlikApp
    End Sub

    ''' <summary>
    ''' Elimina la Qlik App indicada de la tabla QLIK_APPS
    ''' </summary>
    ''' <param name="lId">Identificador de la QlikApp en la tabla QLIK_APPS</param>
    ''' <returns>Identificador del registro eliminado</returns>
    ''' <remarks>
    ''' Llamada desde: QlikApps.aspx.vb
    ''' Tiempo: 0 seg
    ''' </remarks>
    Public Function EliminarFSNQlikApp(ByVal lId As Long) As Long
        Dim lResultadoGuardado As Long = 0

        lResultadoGuardado = DBServer.QlikApps_QlikApp_Eliminar(lId)

        Return lResultadoGuardado
    End Function

End Class

Public Class FSNQlikApp
    Inherits Security

    Private lId As Long
    Private sNombre As String
    Private sUrl As String
    Private sIdApp As String
    Private sApp As String
    Private sIdSheet As String
    Private sSheet As String
    Private lRestAPIPort As Long
    Private bActiva As Boolean
    Private dFecAlta As Date
    Private dFecAct As Date

    Public Property Id() As Long
        Get
            Id = lId
        End Get
        Set(ByVal Value As Long)
            lId = Value
        End Set
    End Property

    Public Property Nombre() As String
        Get
            Nombre = sNombre
        End Get
        Set(ByVal Value As String)
            sNombre = Value
        End Set
    End Property

    Public Property Url() As String
        Get
            Url = sUrl
        End Get
        Set(ByVal Value As String)
            If Value.EndsWith("/") Then
                Value = Value.Substring(0, Value.Length - 1)
            End If
            sUrl = Value
        End Set
    End Property

    Public Property IdApp() As String
        Get
            IdApp = sIdApp
        End Get
        Set(ByVal Value As String)
            sIdApp = Value
        End Set
    End Property

    Public Property App() As String
        Get
            App = sApp
        End Get
        Set(ByVal Value As String)
            sApp = Value
        End Set
    End Property

    Public Property IdSheet() As String
        Get
            IdSheet = sIdSheet
        End Get
        Set(ByVal Value As String)
            sIdSheet = Value
        End Set
    End Property

    Public Property Sheet() As String
        Get
            Sheet = sSheet
        End Get
        Set(ByVal Value As String)
            sSheet = Value
        End Set
    End Property

    Public Property RestAPIPort() As Long
        Get
            RestAPIPort = lRestAPIPort
        End Get
        Set(ByVal Value As Long)
            lRestAPIPort = Value
        End Set
    End Property

    Public Property Activa() As Boolean
        Get
            Activa = bActiva
        End Get
        Set(ByVal Value As Boolean)
            bActiva = Value
        End Set
    End Property

    Public Property FecAlta() As Date
        Get
            FecAlta = dFecAlta
        End Get
        Set(ByVal Value As Date)
            dFecAlta = Value
        End Set
    End Property

    Public Property FecAct() As Date
        Get
            FecAct = dFecAct
        End Get
        Set(ByVal Value As Date)
            dFecAct = Value
        End Set
    End Property


    ''' <summary>
    ''' Constructor de la clase FSNQlikApp
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si está autenticado</param>
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub

End Class

Public Class FSNQlikConfig
    Inherits Security

    ''' <summary>
    ''' Constructor de la clase FSNQlikConfig
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si está autenticado</param>
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub

    ''' <summary>
    ''' Obtiene el primer codigo de cliente de la tabla CLIENTES
    ''' </summary>
    ''' <returns>Codigo de cliente</returns>
    ''' <remarks>
    ''' Llamada desde: QlikApps.aspx.vb, VisorQlik.aspx.vb
    ''' Tiempo: 0 seg
    ''' </remarks>
    Public Function ObtenerCodigoCliente() As String
        Dim sCodigoCliente As String = ""

        sCodigoCliente = DBServer.QlikApps_Cliente_Codigo_Obtener()

        Return sCodigoCliente
    End Function



    ''' <summary>
    ''' Obtiene el identificador del Usuario Qlik correspondiente al identificador de Perfil FSN pasado, desde la tabla QLIK_PERF_USUQLIK
    ''' </summary>
    ''' <param name="sCodUsuFSN">Codigo del Usuario FSN para el que se quiere obtener el Usuario Qlik</param>
    ''' <returns>Identificador del Usuario Qlik</returns>
    ''' <remarks>
    ''' Llamada desde: VisorQlik.aspx.vb
    ''' Tiempo: 0 seg
    ''' </remarks>
    Public Function ObtenerUsuarioQlik(ByVal sCodUsuFSN As String) As Dictionary(Of String, String)
        Dim dicIdUrlUsuarioQlik As New Dictionary(Of String, String)

        dicIdUrlUsuarioQlik = DBServer.QlikApps_Usuario_Qlik_Obtener(sCodUsuFSN)

        Return dicIdUrlUsuarioQlik
    End Function

    ''' <summary>
    ''' Obtiene los registros de perfiles de la plataforma y usuarios Qlik asignados desde la tabla QLIK_PERF_USUQLIK
    ''' </summary>
    ''' <param name="sIdiomaCod">Codigo de idioma del usuario de la plataforma</param>
    ''' <returns>Dataset con los registros</returns>
    ''' <remarks>
    ''' Llamada desde: QlikApps.aspx.vb
    ''' Tiempo: 0 seg
    ''' </remarks>
    Public Function ObtenerPerfilesUsuariosQlikAsignados(ByVal sIdiomaCod As String) As DataSet
        Dim dsPerfUsuQlik As New DataSet

        dsPerfUsuQlik = DBServer.ObtenerPerfilesUsuariosQlikAsignados(sIdiomaCod)

        Return dsPerfUsuQlik
    End Function

    ''' <summary>
    ''' Obtiene los servidores Qlik configurados en la tabla QLIK_APPS
    ''' </summary>
    ''' <returns>Dataset con los registros</returns>
    ''' <remarks>
    ''' Llamada desde: QlikApps.aspx.vb
    ''' Tiempo: 0 seg
    ''' </remarks>
    Public Function ObtenerServidoresQlikConfigurados() As DataSet
        Dim dsServidoresQlik As New DataSet

        dsServidoresQlik = DBServer.ObtenerServidoresQlikConfigurados()

        Return dsServidoresQlik
    End Function

    ''' <summary>
    ''' Obtiene los perfiles de la plataforma que todavia no tienen asignado un usuario Qlik
    ''' </summary>
    ''' <param name="sIdiomaCod">Codigo de idioma del usuario de la plataforma</param>
    ''' <returns>Dataset con los registros</returns>
    ''' <remarks>
    ''' Llamada desde: QlikApps.aspx.vb
    ''' Tiempo: 0 seg
    ''' </remarks>
    Public Function ObtenerPerfilesFSNNoAsignados(ByVal sIdiomaCod As String) As DataSet
        Dim dsPerfilesFSN As New DataSet

        dsPerfilesFSN = DBServer.ObtenerPerfilesFSNNoAsignados(sIdiomaCod)

        Return dsPerfilesFSN
    End Function

    ''' <summary>
    ''' Guarda las asignaciones de perfiles de la plataforma y usuarios de Qlik en la tabla QLIK_PERF_USUQLIK
    ''' </summary>
    ''' <param name="dtPerfUsuQlik">Datatable con los registros a insertar/modificar</param>
    ''' <returns>Numero de registros creados</returns>
    ''' <remarks>
    ''' Llamada desde: QlikApps.aspx.vb
    ''' Tiempo: 0 seg
    ''' </remarks>
    Public Function GuardarPerfilesUsuariosQlik(ByVal dtPerfUsuQlik As DataTable) As Integer
        Dim lResultadoGuardado As Long = 0

        lResultadoGuardado = DBServer.QlikApps_Perf_UsuQlik_Insertar(dtPerfUsuQlik)

        Return lResultadoGuardado
    End Function

End Class

Public Class QlikSenseServer
    Inherits Security

    Private jsSerializer As New JavaScriptSerializer()

    Private sUrlServidorQlik As String
    Private sUrlRepositoryServiceAPI As String
    Private bExisteRepositorioQlik As Boolean
    Private oServicioProxyLocal As Object
    Private iRestAPIPort As Integer
    Private sUrlProxyServiceAPI As String

#Region "Inicializacion y carga de Propiedades iniciales"

#Region "Inicializacion"

    ''' <summary>
    ''' Constructor de la clase QlikSenseServer
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si esta autenticado</param>
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
        jsSerializer.MaxJsonLength = Int32.MaxValue
    End Sub

#End Region

#Region "Propiedades Servidor Qlik Sense"

    Public Property UrlServidorQlik As String
        Get
            UrlServidorQlik = sUrlServidorQlik
        End Get
        Set(value As String)
            Me.sUrlServidorQlik = value
            Me.sUrlRepositoryServiceAPI = ObtenerUrlRepositoryServiceAPI()
            Me.bExisteRepositorioQlik = ComprobarRepositorioQlik()
            If Not bExisteRepositorioQlik Then
                Exit Property
            End If
            Me.oServicioProxyLocal = ObtenerLocalProxyService()
            Me.iRestAPIPort = ObtenerRestAPIPort()
            Me.sUrlProxyServiceAPI = ObtenerUrlProxyServiceAPI()
        End Set
    End Property

#End Region

#Region "Propiedades del Repositorio de Qlik"

    ''' <summary>
    ''' Obtiene la URL donde atacar la API del repositorio
    ''' </summary>
    ''' <returns>Devuelve la URL donde atacar la API del repositorio, para el Servidor Qlik Sense creado</returns>
    Private Function ObtenerUrlRepositoryServiceAPI() As String
        Dim sUrlRepositorio = sUrlServidorQlik

        If sUrlRepositorio.StartsWith("http://") Then
            sUrlRepositorio = sUrlRepositorio.Replace("http://", "https://") ''El protocolo para el uso de APIs de Qlik es el https
        End If

        If sUrlRepositorio.EndsWith("/") Then
            sUrlRepositorio = sUrlRepositorio.Remove(sUrlRepositorio.Length - 1)
        End If

        Dim delim As String() = New String(0) {"https://"}
        If (sUrlRepositorio.Split(delim, StringSplitOptions.None).Length = 2) AndAlso (sUrlRepositorio.Split(delim, StringSplitOptions.None)(1).Contains(":")) Then
            sUrlRepositorio = delim(0).ToString() & sUrlRepositorio.Split(delim, StringSplitOptions.None)(1).Split(":")(0)
        End If

        sUrlRepositorio &= ":4242" ''El puerto de la API del servicio de repositorio de Qlik no se puede configurar y es el 4242

        Return sUrlRepositorio
    End Function

    Public ReadOnly Property UrlRepositoryServiceAPI As String
        Get
            UrlRepositoryServiceAPI = sUrlRepositoryServiceAPI
        End Get
    End Property

    ''' <summary>
    ''' Comprueba la URL de la API del repositorio creada a partir de la URL del servidor
    ''' </summary>
    ''' <returns>false: El repositorio no responde en la URL creada, true: el repositorio existe</returns>
    Private Function ComprobarRepositorioQlik() As Boolean
        ''Lanzamos un Ping a la API de Qlik que nos dice si el ping ha sido correcto o no
        Dim bExiste = False

        Dim alHeaders As New ArrayList()
        Dim dicHeader As New Dictionary(Of String, String)

        dicHeader.Add("X-Qlik-User", "UserDirectory=INTERNAL; UserId=sa_repository")
        alHeaders.Add(dicHeader)

        Dim oComunicadorQlikAPI As New QlikAPIsComm(DBServer, mIsAuthenticated)
        Dim stStream As Stream = oComunicadorQlikAPI.Execute(New Object, "GET", ObtenerUrlRepositoryServiceAPI() & "/ssl/", "ping", alHeaders, New ArrayList())

        If Not IsNothing(stStream) AndAlso Not String.IsNullOrEmpty(stStream.ToString()) AndAlso (stStream.ToString() <> "[]") Then
            Dim sPing As String = jsSerializer.Deserialize(Of String)(New StreamReader(stStream).ReadToEnd())
            If (Not IsNothing(sPing)) AndAlso (sPing <> "") AndAlso (sPing = "Ping successful.") Then
                bExiste = True
            End If
        End If

        Return bExiste
    End Function

    Public ReadOnly Property ExisteRepositorioQlik As Boolean
        Get
            ExisteRepositorioQlik = bExisteRepositorioQlik
        End Get
    End Property

#End Region

#Region "Propiedades del Proxy de Qlik"

    ''' <summary>
    ''' Obtiene el objeto del proxy de autenticacion
    ''' </summary>
    ''' <returns>Devuelve un objeto con las propiedades del proxy de autenticacion</returns>
    Private Function ObtenerLocalProxyService() As Object
        ''Lanzamos una consulta a la API de Qlik para obtener la configuracion del proxy
        Dim alHeaders As New ArrayList()
        Dim dicHeader As New Dictionary(Of String, String)

        dicHeader.Add("X-Qlik-User", "UserDirectory=INTERNAL; UserId=sa_repository")
        alHeaders.Add(dicHeader)

        Dim oComunicadorQlikAPI As New QlikAPIsComm(DBServer, mIsAuthenticated)
        Dim stStream As Stream = oComunicadorQlikAPI.Execute(New Object, "GET", ObtenerUrlRepositoryServiceAPI() & "/qrs/proxyservice/", "local", alHeaders, New ArrayList())

        If Not IsNothing(stStream) AndAlso Not String.IsNullOrEmpty(stStream.ToString()) AndAlso (stStream.ToString() <> "[]") Then
            Return jsSerializer.Deserialize(Of Object)(New StreamReader(stStream).ReadToEnd())
        Else
            Return Nothing
        End If

    End Function

    Public ReadOnly Property ServicioProxyLocal As Object
        Get
            ServicioProxyLocal = oServicioProxyLocal
        End Get
    End Property

    ''' <summary>
    ''' Obtiene el puerto donde atacar la API del proxy de autenticacion
    ''' </summary>
    ''' <returns>Devuelve el puerto donde atacar la API del proxy de autenticacion, para el Servidor Qlik Sense creado</returns>
    Private Function ObtenerRestAPIPort() As Integer
        ''El puerto de la API del servicio de autenticacion lo obtenemos de la configuracion del Proxy Local
        Dim iPuerto As Integer = 0
        Dim oSPLTratado As Dictionary(Of String, Object) = oServicioProxyLocal

        If (Not IsNothing(oSPLTratado)) AndAlso (oSPLTratado.Count > 0) AndAlso _
            (oSPLTratado.Keys.Contains("settings")) AndAlso _
            (Not IsNothing(oSPLTratado.Item("settings"))) Then

            Dim oSettingsTratado As Dictionary(Of String, Object) = oSPLTratado.Item("settings")

            If (oSettingsTratado.Keys.Contains("restListenPort")) AndAlso _
                (Not IsNothing(oSettingsTratado.Item("restListenPort"))) Then

                iPuerto = oSettingsTratado.Item("restListenPort")

            End If

        End If

        Return iPuerto
    End Function

    Public ReadOnly Property RestAPIPort As Integer
        Get
            RestAPIPort = iRestAPIPort
        End Get
    End Property

    ''' <summary>
    ''' Obtiene la URL donde atacar la API del proxy de autenticacion
    ''' </summary>
    ''' <returns>Devuelve la URL donde atacar la API del proxy de autenticacion, para el Servidor Qlik Sense creado</returns>
    Private Function ObtenerUrlProxyServiceAPI() As String
        Dim sUrlAutenticacion As String = sUrlServidorQlik

        If sUrlAutenticacion.StartsWith("http://") Then
            sUrlAutenticacion = sUrlAutenticacion.Replace("http://", "https://") ''El protocolo para el uso de APIs de Qlik es el https
        End If

        If sUrlAutenticacion.EndsWith("/") Then
            sUrlAutenticacion = sUrlAutenticacion.Remove(sUrlAutenticacion.Length - 1)
        End If

        Dim delim As String() = New String(0) {"https://"}
        If (sUrlAutenticacion.Split(delim, StringSplitOptions.None).Length = 2) AndAlso (sUrlAutenticacion.Split(delim, StringSplitOptions.None)(1).Contains(":")) Then
            sUrlAutenticacion = delim(0).ToString() & sUrlAutenticacion.Split(delim, StringSplitOptions.None)(1).Split(":")(0)
        End If

        sUrlAutenticacion &= ":" & iRestAPIPort ''El puerto de la API del servicio de autenticacion lo obtenemos de la configuracion del Proxy Local

        Return sUrlAutenticacion
    End Function

    Public ReadOnly Property UrlProxyServiceAPI As String
        Get
            UrlProxyServiceAPI = sUrlProxyServiceAPI
        End Get
    End Property

#End Region

#End Region

#Region "Metodos para las Entidades de Qlik"

    Private Structure CountResponse

        Dim value As String

    End Structure

    ''' <summary>
    ''' Cuenta cuantas entidades especificadas hay, en funcion del filtro indicado
    ''' </summary>
    ''' <param name="sEntidadQlik">Entidad de Qlik Sense a contar</param>
    ''' <param name="sFiltro">Filtro para la busqueda</param>
    ''' <returns>Tipo respuesta de contador con el numero de entidades</returns>
    Private Function ContarEntidadesQlik(ByVal sEntidadQlik As String, ByVal sFiltro As String) As CountResponse
        ''Lanzamos una consulta a la API de Qlik que nos dice cuantas entidades indicadas existen en Qlik con el filtro indicado
        Dim alHeaders As New ArrayList()
        Dim dicHeader As New Dictionary(Of String, String)

        dicHeader.Add("X-Qlik-User", "UserDirectory=INTERNAL; UserId=sa_repository")
        alHeaders.Add(dicHeader)

        Dim alParams As New ArrayList()
        Dim dicParam As New Dictionary(Of String, String)

        dicParam.Add("filter", sFiltro)
        alParams.Add(dicParam)

        Dim oComunicadorQlikAPI As New QlikAPIsComm(DBServer, mIsAuthenticated)
        Dim stStream As Stream = oComunicadorQlikAPI.Execute(New Object, "GET", ObtenerUrlRepositoryServiceAPI() & "/qrs/" & sEntidadQlik & "/", "count", alHeaders, alParams)

        If Not IsNothing(stStream) AndAlso Not String.IsNullOrEmpty(stStream.ToString()) AndAlso (stStream.ToString() <> "[]") Then
            Return jsSerializer.Deserialize(Of CountResponse)(New StreamReader(stStream).ReadToEnd())
        Else
            Dim contedorVacio As New CountResponse()
            Return contedorVacio
        End If

    End Function

    ''' <summary>
    ''' Comprueba si existe alguna entidad, en funcion del filtro indicado
    ''' </summary>
    ''' <param name="sEntidadQlik">Entidad de Qlik Sense a comprobar</param>
    ''' <param name="sFiltro">Filtro para la busqueda</param>
    ''' <returns>false: No existe, true: Existe (al menos alguna)</returns>
    Private Function ComprobarEntidadesQlik(ByVal sEntidadQlik As String, ByVal sFiltro As String) As Boolean
        ''Consultamos cuantas entidades indicadas existen en Qlik
        Dim bExiste = False
        Dim ContadorEntidades As CountResponse = ContarEntidadesQlik(sEntidadQlik, sFiltro)

        If (Not IsNothing(ContadorEntidades)) AndAlso (Not IsNothing(ContadorEntidades.value)) AndAlso (IsNumeric(ContadorEntidades.value)) AndAlso (CInt(ContadorEntidades.value) > 0) Then
            bExiste = True
        End If

        Return bExiste
    End Function


    ''' <summary>
    ''' Obtiene las entidades especificadas, en funcion del filtro indicado
    ''' </summary>
    ''' <param name="sEntidadQlik">Tipo de Entidad de Qlik Sense a obtener</param>
    ''' <param name="sFiltro">Filtro para la busqueda</param>
    ''' <returns>Lista de objetos de entidades especificadas</returns>
    Private Function ObtenerEntidadesQlik(ByVal sEntidadQlik As String, ByVal sFiltro As String) As List(Of Object)
        ''Lanzamos una consulta a la API de Qlik para obtener las entidades indicadas de Qlik con el filtro indicado
        Dim alHeaders As New ArrayList()
        Dim dicHeader As New Dictionary(Of String, String)

        dicHeader.Add("X-Qlik-User", "UserDirectory=INTERNAL; UserId=sa_repository")
        alHeaders.Add(dicHeader)

        Dim alParams As New ArrayList()
        Dim dicParam As New Dictionary(Of String, String)

        dicParam.Add("filter", sFiltro)
        alParams.Add(dicParam)

        Dim oComunicadorQlikAPI As New QlikAPIsComm(DBServer, mIsAuthenticated)
        Dim stStream As Stream = oComunicadorQlikAPI.Execute(New Object, "GET", ObtenerUrlRepositoryServiceAPI() & "/qrs/", sEntidadQlik, alHeaders, alParams)

        If Not IsNothing(stStream) AndAlso Not String.IsNullOrEmpty(stStream.ToString()) AndAlso (stStream.ToString() <> "[]") Then
            Return jsSerializer.Deserialize(Of List(Of Object))(New StreamReader(stStream).ReadToEnd())
        Else
            Return Nothing
        End If

    End Function

    Public Class FiltroEntidadesAccionUsuario
        Public resourceType As String
        Public Action As String
        Public UserId As String
    End Class

    ''' <summary>
    ''' Obtiene las entidades que tienen habilitada la accion indicada para el usuario indicado
    ''' </summary>
    ''' <param name="sEntidadQlik">Tipo de Entidad de Qlik Sense a obtener</param>
    ''' <param name="sAccion">Accion sobre la que tiene permiso</param>
    ''' <param name="sId">Id del Usuario de Qlik</param>
    ''' <returns>Lista de objetos de entidades especificadas</returns>
    Private Function ObtenerEntidadesAccionUsuarioQlik(ByVal sEntidadQlik As String, ByVal sAccion As String, ByVal sId As String) As List(Of Object)
        ''Lanzamos una consulta a la API de Qlik para obtener las entidades indicadas de Qlik con el permiso indicado para el usuario indicado
        Dim alHeaders As New ArrayList()
        Dim dicHeader As New Dictionary(Of String, String)

        dicHeader.Add("X-Qlik-User", "UserDirectory=INTERNAL; UserId=sa_repository")
        alHeaders.Add(dicHeader)

        Dim oFiltroEntidadesAccionUsuario As New FiltroEntidadesAccionUsuario With {.resourceType = sEntidadQlik, .Action = sAccion, .UserId = sId}

        Dim oComunicadorQlikAPI As New QlikAPIsComm(DBServer, mIsAuthenticated)
        Dim stStream As Stream = oComunicadorQlikAPI.Execute(oFiltroEntidadesAccionUsuario, "POST", ObtenerUrlRepositoryServiceAPI() & "/qrs/systemrule/security/audit", "accessibleobjects", alHeaders, New ArrayList())

        If Not IsNothing(stStream) AndAlso Not String.IsNullOrEmpty(stStream.ToString()) AndAlso (stStream.ToString() <> "[]") Then
            Return jsSerializer.Deserialize(Of List(Of Object))(New StreamReader(stStream).ReadToEnd())
        Else
            Return Nothing
        End If

    End Function

    ''' <summary>
    ''' Obtiene las entidades publicadas en el Stream indicado
    ''' </summary>
    ''' <param name="sEntidadQlik">Tipo de Entidad de Qlik Sense a obtener</param>
    ''' <param name="sStreamName">Nombre del Stream Qlik en la que se ha publicado la entidad</param>
    ''' <returns>Lista de objetos de entidades especificadas</returns>
    Private Function ObtenerEntidadesStreamQlik(ByVal sEntidadQlik As String, ByVal sStreamName As String) As List(Of Object)
        ''Lanzamos una consulta a la API de Qlik para obtener las entidades indicadas de Qlik publicadas en el Stream indicado
        Dim alHeaders As New ArrayList()
        Dim dicHeader As New Dictionary(Of String, String)

        dicHeader.Add("X-Qlik-User", "UserDirectory=INTERNAL; UserId=sa_repository")
        alHeaders.Add(dicHeader)

        Dim alParams As New ArrayList()
        Dim dicParam As New Dictionary(Of String, String)

        dicParam.Add("filter", "published eq true and stream.name eq '" & sStreamName & "'")
        alParams.Add(dicParam)

        Dim oComunicadorQlikAPI As New QlikAPIsComm(DBServer, mIsAuthenticated)
        Dim stStream As Stream = oComunicadorQlikAPI.Execute(New Object, "GET", ObtenerUrlRepositoryServiceAPI() & "/qrs/", sEntidadQlik, alHeaders, alParams)

        If Not IsNothing(stStream) AndAlso Not String.IsNullOrEmpty(stStream.ToString()) AndAlso (stStream.ToString() <> "[]") Then
            Return jsSerializer.Deserialize(Of List(Of Object))(New StreamReader(stStream).ReadToEnd())
        Else
            Return Nothing
        End If

    End Function

    ''' <summary>
    ''' Obtiene las entidades que tienen habilitada la accion indicada para el usuario indicado
    ''' </summary>
    ''' <param name="tTipoClaseEntidad">Clase con las propiedades del Tipo de Entidad de Qlik Sense a obtener</param>
    ''' <param name="sEntidadQlik">Tipo de Entidad de Qlik Sense a obtener</param>
    ''' <param name="sIdEntidad">Id de la entidad</param>
    ''' <returns>Lista de objetos de entidades especificadas</returns>
    Private Function ObtenerEntidadQlik(ByVal tTipoClaseEntidad As Type, ByVal sEntidadQlik As String, ByVal sIdEntidad As String) As Object
        ''Lanzamos una consulta a la API de Qlik para obtener las entidades indicadas de Qlik con el filtro indicado
        Dim obj As Object = System.Activator.CreateInstance(tTipoClaseEntidad)
        Dim alHeaders As New ArrayList()
        Dim dicHeader As New Dictionary(Of String, String)

        dicHeader.Add("X-Qlik-User", "UserDirectory=INTERNAL; UserId=sa_repository")
        alHeaders.Add(dicHeader)

        Dim oComunicadorQlikAPI As New QlikAPIsComm(DBServer, mIsAuthenticated)
        Dim stStream As Stream = oComunicadorQlikAPI.Execute(New Object, "GET", ObtenerUrlRepositoryServiceAPI() & "/qrs/" & sEntidadQlik & "/", sIdEntidad, alHeaders, New ArrayList())

        If Not IsNothing(stStream) AndAlso Not String.IsNullOrEmpty(stStream.ToString()) AndAlso (stStream.ToString() <> "[]") Then
            obj = jsSerializer.Deserialize(New StreamReader(stStream).ReadToEnd(), tTipoClaseEntidad)
            Return obj
        Else
            Return Nothing
        End If

    End Function

#End Region

#Region "Entidades de Qlik"

#Region "Streams Qlik"

    Public Class QlikStream

        Public id As String
        Public createdDate As DateTime
        Public modifiedDate As DateTime
        Public modifiedByUserName As String
        Public owner As QlikUser
        Public customProperties As Object
        Public name As String
        Public tags As Object
        Public privileges As Object
        Public schemaPath As String

    End Class

    ''' <summary>
    ''' Obtiene los Streams de Qlik Sense disponibles
    ''' </summary>
    ''' <returns>Lista de Streams de tipo QlikStream</returns>
    Public Function ObtenerStreamsQlik() As List(Of QlikStream)
        ''Lanzamos una consulta a la API de Qlik para obtener todos los streams
        Dim sFiltro As String = "name ne ''"
        Dim oListaQlikStreams As New List(Of QlikStream)
        Dim oStreamsObtenidos As List(Of Object) = ObtenerEntidadesQlik("stream", sFiltro)

        If (Not IsNothing(oStreamsObtenidos)) AndAlso (oStreamsObtenidos.Count > 0) Then
            oListaQlikStreams = jsSerializer.Deserialize(Of List(Of QlikStream))(jsSerializer.Serialize(oStreamsObtenidos))
        End If

        Return oListaQlikStreams
    End Function

    ''' <summary>
    ''' Obtiene el Stream de Qlik Sense indicado
    ''' </summary>
    ''' <param name="sId">Id del Stream</param>
    ''' <returns>Lista de objetos de entidades especificadas</returns>
    Public Function ObtenerStreamQlik(ByVal sId As String) As QlikStream
        ''Lanzamos una consulta a la API de Qlik para obtener el stream indicado
        Dim oQlikStream As QlikStream = ObtenerEntidadQlik(GetType(QlikStream), "stream", sId)

        Return oQlikStream
    End Function

    ''' <summary>
    ''' Comprueba si existe algun Stream de Qlik Sense con el Id indicado
    ''' </summary>
    ''' <param name="sId">Id del Stream</param>
    ''' <returns>false: No existe, true: Existe (al menos alguno)</returns>
    Public Function ExisteStreamQlik(ByVal sId As String) As Boolean
        ''Comprobamos si existe el stream Qlik indicado
        Dim sFiltro As String = "id eq " & sId

        Return ComprobarEntidadesQlik("stream", sFiltro)
    End Function

    ''' <summary>
    ''' Obtiene los Streams que tienen habilitada la accion indicada para el usuario indicado
    ''' </summary>
    ''' <param name="sAccion">Accion sobre la que tiene permiso</param>
    ''' <param name="sIdUsu">Id del Usuario de Qlik</param>
    ''' <returns>Lista de Streams de tipo QlikStream</returns>
    Public Function ObtenerStreamsAccionUsuarioQlik(ByVal sAccion As String, ByVal sIdUsu As String) As List(Of QlikStream)
        ''Lanzamos una consulta a la API de Qlik para obtener los Streams con el permiso indicado para el usuario indicado
        Dim oListaQlikStreams As New List(Of QlikStream)
        Dim oStreamsObtenidos As List(Of Object) = ObtenerEntidadesAccionUsuarioQlik("stream", sAccion, sIdUsu)

        If (Not IsNothing(oStreamsObtenidos)) AndAlso (oStreamsObtenidos.Count > 0) Then
            oListaQlikStreams = jsSerializer.Deserialize(Of List(Of QlikStream))(jsSerializer.Serialize(oStreamsObtenidos))
        End If

        Return oListaQlikStreams
    End Function

#End Region

#Region "Usuarios y Dominios Qlik"

    Public Class QlikUser

        Public id As String
        Public createdDate As DateTime
        Public modifiedDate As DateTime
        Public modifiedByUserName As String
        Public customProperties As Object
        Public userId As String
        Public userDirectory As String
        Public name As String
        Public roles As Object
        Public attributes As Object
        Public inactive As Boolean
        Public removedExternally As Boolean
        Public blacklisted As Boolean
        Public tags As Object
        Public recentApps As Object
        Public followingApps As Object
        Public privileges As Object
        Public schemaPath As String

    End Class

    ''' <summary>
    ''' Obtiene los Usuarios de Qlik Sense disponibles
    ''' </summary>
    ''' <param name="sUserDirectory">Opcional. Directorio o Dominio por el que filtrar los usuarios</param>
    ''' <param name="bSoloActivos">Opcional. Obtener solo los usuarios activos o todos</param>
    ''' <returns>Lista de Usuarios de tipo QlikUser</returns>
    Public Function ObtenerUsuariosQlik(Optional ByVal sUserDirectory As String = "", Optional ByVal bSoloActivos As Boolean = True) As List(Of QlikUser)
        ''Lanzamos una consulta a la API de Qlik para obtener los usuarios existentes
        Dim sFiltro As String = ""

        If bSoloActivos Then
            sFiltro = "inactive eq false and blacklisted eq false"
        End If

        If sFiltro <> "" Then
            sFiltro &= " and userDirectory ne 'INTERNAL'"
        Else
            sFiltro &= "userDirectory ne 'INTERNAL'"
        End If

        If sUserDirectory <> "" Then
            If sFiltro <> "" Then
                sFiltro &= " and userDirectory eq '" & sUserDirectory & "'"
            Else
                sFiltro &= "userDirectory eq '" & sUserDirectory & "'"
            End If
        End If

        Dim oListaQlikUsers As New List(Of QlikUser)
        Dim oUsersObtenidos As List(Of Object) = ObtenerEntidadesQlik("user", sFiltro)

        If (Not IsNothing(oUsersObtenidos)) AndAlso (oUsersObtenidos.Count > 0) Then
            oListaQlikUsers = jsSerializer.Deserialize(Of List(Of QlikUser))(jsSerializer.Serialize(oUsersObtenidos))
        End If

        Return oListaQlikUsers
    End Function

    ''' <summary>
    ''' Obtiene el Usuario de Qlik Sense indicado
    ''' </summary>
    ''' <param name="sUserDirectory">Directorio o Dominio del usuario</param>
    ''' <param name="sUserId">UserId del Usuario</param>
    ''' <returns>Usuario de Qlik Sense de tipo QlikUser</returns>
    Public Function ObtenerUsuarioQlik(ByVal sUserDirectory As String, ByVal sUserId As String) As QlikUser
        ''Lanzamos una consulta a la API de Qlik para obtener el usuario indicado
        Dim sFiltro As String = "userDirectory eq '" & sUserDirectory & "' and userID eq '" & sUserId & "'"
        Dim oQlikUser As New QlikUser
        Dim oListaQlikUsers As New List(Of QlikUser)
        Dim oUsersObtenidos As List(Of Object) = ObtenerEntidadesQlik("user", sFiltro)

        If (Not IsNothing(oUsersObtenidos)) AndAlso (oUsersObtenidos.Count = 1) Then
            oListaQlikUsers = jsSerializer.Deserialize(Of List(Of QlikUser))(jsSerializer.Serialize(oUsersObtenidos))
            oQlikUser = oListaQlikUsers(0)
        End If

        Return oQlikUser
    End Function

    ''' <summary>
    ''' Obtiene el Usuario de Qlik Sense indicado
    ''' </summary>
    ''' <param name="sId">Id del Usuario</param>
    ''' <returns>Usuario de Qlik Sense de tipo QlikUser</returns>
    Public Function ObtenerUsuarioQlik(ByVal sId As String) As QlikUser
        ''Lanzamos una consulta a la API de Qlik para obtener el usuario indicado
        Dim oQlikUser As QlikUser = ObtenerEntidadQlik(GetType(QlikUser), "user", sId)

        Return oQlikUser
    End Function

    ''' <summary>
    ''' Comprueba si existe algun Usuario de Qlik Sense con el Directorio y UserId indicados
    ''' </summary>
    ''' <param name="sUserDirectory">Directorio o Dominio del usuario</param>
    ''' <param name="sUserId">UserId del Usuario</param>
    ''' <returns>false: No existe, true: Existe (al menos alguno)</returns>
    Public Function ExisteUsuarioQlik(ByVal sUserDirectory As String, ByVal sUserId As String) As Boolean
        ''Comprobamos si existe el usuario Qlik indicado
        Dim sFiltro As String = "userDirectory eq '" & sUserDirectory & "' and userID eq '" & sUserId & "'"

        Return ComprobarEntidadesQlik("user", sFiltro)
    End Function


    Public Class QlikUserDirectory

        Public userDirectory As String

    End Class

    ''' <summary>
    ''' Obtiene los Directorios o Dominios de usuarios de Qlik Sense disponibles
    ''' </summary>
    ''' <returns>Lista de Directorios o Dominios de usuarios de tipo QlikUserDirectory</returns>
    Public Function ObtenerDominiosQlik() As List(Of QlikUserDirectory)
        ''Lanzamos una consulta a la API de Qlik para obtener los usuarios existentes
        Dim oListaQlikUsers As List(Of QlikUser) = ObtenerUsuariosQlik()

        Dim oListaQlikUserDirectories As New List(Of QlikUserDirectory)
        For Each oQlikUser As QlikUser In oListaQlikUsers
            Dim sUserDirectory As String = oQlikUser.userDirectory
            If IsNothing(oListaQlikUserDirectories.Find(Function(qud) qud.userDirectory = sUserDirectory)) Then
                oListaQlikUserDirectories.Add(New QlikUserDirectory With {.userDirectory = sUserDirectory})
            End If
        Next

        Return oListaQlikUserDirectories
    End Function

#End Region

#Region "Aplicaciones Qlik"

    Public Class QlikApp

        Public id As String
        Public createdDate As DateTime
        Public modifiedDate As DateTime
        Public modifiedByUserName As String
        Public owner As QlikUser
        Public customProperties As Object
        Public name As String
        Public appId As String
        Public publishTime As DateTime
        Public published As Boolean
        Public tags As Object
        Public description As String
        Public stream As QlikStream
        Public fileSize As Long
        Public lastReloadTime As DateTime
        Public thumbnail As String
        Public savedInProductVersion As String
        Public migrationHash As String
        Public dynamicColor As String
        Public availabilityStatus As Integer
        Public privileges As Object
        Public schemaPath As String

    End Class

    ''' <summary>
    ''' Obtiene las Aplicaciones de Qlik Sense disponibles
    ''' </summary>
    ''' <param name="bSoloPublicadas">Opcional. Obtener solo las aplicaciones publicadas o todas</param>
    ''' <returns>Lista de Aplicaciones de tipo QlikApp</returns>
    Public Function ObtenerAplicacionesQlik(Optional ByVal bSoloPublicadas As Boolean = True) As List(Of QlikApp)
        ''Lanzamos una consulta a la API de Qlik para obtener las aplicaciones publicadas o todas
        Dim sFiltro As String = IIf(bSoloPublicadas, "owner.userDirectory ne 'INTERNAL' and published eq true", "owner.userDirectory ne 'INTERNAL' and published eq true or published eq false")
        Dim oListaQlikApps As New List(Of QlikApp)
        Dim oAppsObtenidas As List(Of Object) = ObtenerEntidadesQlik("app", sFiltro)

        If (Not IsNothing(oAppsObtenidas)) AndAlso (oAppsObtenidas.Count > 0) Then
            oListaQlikApps = jsSerializer.Deserialize(Of List(Of QlikApp))(jsSerializer.Serialize(oAppsObtenidas))
        End If

        Return oListaQlikApps
    End Function

    ''' <summary>
    ''' Obtiene las Aplicaciones que tienen habilitada la accion indicada para el usuario indicado
    ''' </summary>
    ''' <param name="sAccion">Accion sobre la que tiene permiso</param>
    ''' <param name="sIdUsu">Id del Usuario de Qlik</param>
    ''' <param name="bSoloPublicadas">Opcional. Obtener solo las aplicaciones publicadas o todas</param>
    ''' <returns>Lista de Aplicaciones de tipo QlikApp</returns>
    Public Function ObtenerAplicacionesAccionUsuarioQlik(ByVal sAccion As String, ByVal sIdUsu As String, Optional ByVal bSoloPublicadas As Boolean = True) As List(Of QlikApp)
        ''Lanzamos una consulta a la API de Qlik para obtener las aplicaciones con el permiso indicado para el usuario indicado
        Dim oListaQlikApps As New List(Of QlikApp)
        Dim oAppsObtenidas As List(Of Object) = ObtenerEntidadesAccionUsuarioQlik("app", sAccion, sIdUsu)

        If (Not IsNothing(oAppsObtenidas)) AndAlso (oAppsObtenidas.Count > 0) Then
            oListaQlikApps = jsSerializer.Deserialize(Of List(Of QlikApp))(jsSerializer.Serialize(oAppsObtenidas))
        End If

        If bSoloPublicadas Then
            Dim oListaTemporal As New List(Of QlikApp)
            For Each oQlikApp As QlikApp In oListaQlikApps
                If oQlikApp.published Then
                    oListaTemporal.Add(oQlikApp)
                End If
            Next
            oListaQlikApps = oListaTemporal
        End If

        Return oListaQlikApps
    End Function

    ''' <summary>
    ''' Obtiene las Aplicaciones que se han publicado en el stream indicado
    ''' </summary>
    ''' <param name="sStreamName">Nombre del Stream de Qlik</param>
    ''' <returns>Lista de Aplicaciones de tipo QlikApp</returns>
    Public Function ObtenerAplicacionesStreamQlik(ByVal sStreamName As String) As List(Of QlikApp)
        ''Lanzamos una consulta a la API de Qlik para obtener las aplicaciones publicadas en el Stream indicado
        Dim oListaQlikApps As New List(Of QlikApp)
        Dim oAppsObtenidas As List(Of Object) = ObtenerEntidadesStreamQlik("app", sStreamName)

        If (Not IsNothing(oAppsObtenidas)) AndAlso (oAppsObtenidas.Count > 0) Then
            oListaQlikApps = jsSerializer.Deserialize(Of List(Of QlikApp))(jsSerializer.Serialize(oAppsObtenidas))
        End If

        Return oListaQlikApps
    End Function

    ''' <summary>
    ''' Obtiene la Aplicacion de Qlik Sense indicada
    ''' </summary>
    ''' <param name="sId">Id de la Aplicacion</param>
    ''' <returns>Aplicacion de Qlik Sense de tipo QlikApp</returns>
    Public Function ObtenerAplicacionQlik(ByVal sId As String) As QlikApp
        ''Lanzamos una consulta a la API de Qlik para obtener la aplicacion indicada
        Dim oQlikApp As QlikApp = ObtenerEntidadQlik(GetType(QlikApp), "app", sId)

        Return oQlikApp
    End Function

    ''' <summary>
    ''' Comprueba si existe alguna Aplicacion de Qlik Sense con el Id indicado
    ''' </summary>
    ''' <param name="sId">Id de la Aplicacion</param>
    ''' <returns>false: No existe, true: Existe (al menos alguna)</returns>
    Public Function ExisteAplicacionQlik(ByVal sId As String) As Boolean
        ''Comprobamos si existe la aplicacion Qlik indicada
        Dim sFiltro As String = "id eq " & sId

        Return ComprobarEntidadesQlik("app", sFiltro)
    End Function

#End Region

#Region "Objetos de aplicaciones Qlik"

    Public Class QlikAppObject

        Public id As String
        Public createdDate As DateTime
        Public modifiedDate As DateTime
        Public modifiedByUserName As String
        Public owner As QlikUser
        Public engineObjectType As String
        Public description As String
        Public attributes As String
        Public objectType As String
        Public publishTime As DateTime
        Public published As Boolean
        Public approved As Boolean
        Public tags As Object
        Public sourceObject As String
        Public draftObject As String
        Public name As String
        Public engineObjectId As String
        Public app As QlikApp
        Public contentHash As String
        Public size As Long
        Public privileges As Object
        Public schemaPath As String

    End Class

    ''' <summary>
    ''' Obtiene los Objetos de Aplicacion de Qlik Sense disponibles
    ''' </summary>
    ''' <param name="sIdApp">Opcional. Id de la Aplkicacion por la que filtrar</param>
    ''' <param name="sObjeto">Opcional. Tipo de Objeto de Aplicacion por el que filtrar</param>
    ''' <param name="bSoloPublicados">Opcional. Obtener solo los objetos publicados o todos</param>
    ''' <returns>Lista de Objetos de Aplicacion de tipo QlikAppObject</returns>
    Public Function ObtenerObjetosQlik(Optional ByVal sIdApp As String = "", Optional ByVal sObjeto As String = "", Optional ByVal bSoloPublicados As Boolean = True) As List(Of QlikAppObject)
        ''Lanzamos una consulta a la API de Qlik para obtener los objetos de aplicacion publicados o todos
        Dim sFiltro As String = ""
        If sIdApp <> "" Then
            sFiltro = "app.id eq " & sIdApp
        End If
        If sObjeto <> "" Then
            If sFiltro <> "" Then
                sFiltro &= " and objectType eq '" & sObjeto & "'"
            Else
                sFiltro = "objectType eq '" & sObjeto & "'"
            End If
        End If
        If bSoloPublicados Then
            If sFiltro <> "" Then
                sFiltro &= " and published eq true"
            Else
                sFiltro = "published eq true"
            End If
        Else
            If sFiltro = "" Then
                sFiltro = "published eq true or published eq false"
            End If
        End If

        Dim oListaQlikAppObjects As New List(Of QlikAppObject)
        Dim oAppObjectsObtenidos As List(Of Object) = ObtenerEntidadesQlik("app/object", sFiltro)

        If (Not IsNothing(oAppObjectsObtenidos)) AndAlso (oAppObjectsObtenidos.Count > 0) Then
            oListaQlikAppObjects = jsSerializer.Deserialize(Of List(Of QlikAppObject))(jsSerializer.Serialize(oAppObjectsObtenidos))
        End If

        Return oListaQlikAppObjects
    End Function

    ''' <summary>
    ''' Obtiene los Objetos de Aplicacion que tienen habilitada la accion indicada para el usuario indicado
    ''' </summary>
    ''' <param name="sAccion">Accion sobre la que tiene permiso</param>
    ''' <param name="sIdUsu">Id del Usuario de Qlik</param>
    ''' <param name="sIdApp">Opcional. Id de la Aplkicacion por la que filtrar</param>
    ''' <param name="sObjeto">Opcional. Tipo de Objeto de Aplicacion por el que filtrar</param>
    ''' <param name="bSoloPublicados">Opcional. Obtener solo los objetos publicados o todos</param>
    ''' <returns>Lista de Objetos de Aplicacion de tipo QlikAppObject</returns>
    Public Function ObtenerObjetosAccionUsuarioQlik(ByVal sAccion As String, ByVal sIdUsu As String, Optional ByVal sIdApp As String = "", Optional ByVal sObjeto As String = "", Optional ByVal bSoloPublicados As Boolean = True) As List(Of QlikAppObject)
        ''Lanzamos una consulta a la API de Qlik para obtener los objetos de aplicacion con el permiso indicado para el usuario indicado de la aplicacion indicada
        Dim oListaQlikAppObjects As New List(Of QlikAppObject)
        Dim oAppObjectsObtenidos As List(Of Object) = ObtenerEntidadesAccionUsuarioQlik("app.object", sAccion, sIdUsu)

        If (Not IsNothing(oAppObjectsObtenidos)) AndAlso (oAppObjectsObtenidos.Count > 0) Then
            oListaQlikAppObjects = jsSerializer.Deserialize(Of List(Of QlikAppObject))(jsSerializer.Serialize(oAppObjectsObtenidos))
        End If

        If sIdApp <> "" Then
            Dim oListaTemporal As New List(Of QlikAppObject)
            For Each oQlikAppObject As QlikAppObject In oListaQlikAppObjects
                If oQlikAppObject.app.id = sIdApp Then
                    oListaTemporal.Add(oQlikAppObject)
                End If
            Next
            oListaQlikAppObjects = oListaTemporal
        End If

        If sObjeto <> "" Then
            Dim oListaTemporal As New List(Of QlikAppObject)
            For Each oQlikAppObject As QlikAppObject In oListaQlikAppObjects
                If oQlikAppObject.objectType = sObjeto Then
                    oListaTemporal.Add(oQlikAppObject)
                End If
            Next
            oListaQlikAppObjects = oListaTemporal
        End If

        If bSoloPublicados Then
            Dim oListaTemporal As New List(Of QlikAppObject)
            For Each oQlikAppObject As QlikAppObject In oListaQlikAppObjects
                If oQlikAppObject.published Then
                    oListaTemporal.Add(oQlikAppObject)
                End If
            Next
            oListaQlikAppObjects = oListaTemporal
        End If

        Return oListaQlikAppObjects
    End Function

    ''' <summary>
    ''' Obtiene el Objeto de Aplicacion de Qlik Sense indicado
    ''' </summary>
    ''' <param name="sObjeto">Tipo de Objeto de Aplicacion por el que filtrar</param>
    ''' <param name="sEngineObjectId">EngineObjectId (GUID que aparece en la Url) del objeto</param>
    ''' <returns>Objeto de Aplicacion de Qlik Sense de tipo QlikAppObject</returns>
    Public Function ObtenerObjetoQlik(ByVal sObjeto As String, ByVal sEngineObjectId As String) As QlikAppObject
        ''Lanzamos una consulta a la API de Qlik para obtener el objeto de aplicacion indicado
        Dim sFiltro As String = "objectType eq '" & sObjeto & "' and engineObjectId eq '" & sEngineObjectId & "'"
        Dim oQlikAppObject As New QlikAppObject
        Dim oListaQlikAppObjects As New List(Of QlikAppObject)
        Dim oAppObjectsObtenidos As List(Of Object) = ObtenerEntidadesQlik("app/object", sFiltro)

        If (Not IsNothing(oAppObjectsObtenidos)) AndAlso (oAppObjectsObtenidos.Count = 1) Then
            oListaQlikAppObjects = jsSerializer.Deserialize(Of List(Of QlikAppObject))(jsSerializer.Serialize(oAppObjectsObtenidos))
            oQlikAppObject = oListaQlikAppObjects(0)
        End If

        Return oQlikAppObject
    End Function

    ''' <summary>
    ''' Obtiene el Objeto de Aplicacion de Qlik Sense indicado
    ''' </summary>
    ''' <param name="sId">Id del Objeto de Aplicacion</param>
    ''' <returns>Objeto de Aplicacion de Qlik Sense de tipo QlikAppObject</returns>
    Public Function ObtenerObjetoQlik(ByVal sId As String) As QlikAppObject
        ''Lanzamos una consulta a la API de Qlik para obtener el objeto de aplicacion indicado
        Dim oQlikAppObject As QlikAppObject = ObtenerEntidadQlik(GetType(QlikAppObject), "app/object", sId)

        Return oQlikAppObject
    End Function

    ''' <summary>
    ''' Comprueba si existe alguna Objeto de Aplicacion de Qlik Sense para el tipo de objeto y con el Id indicado
    ''' </summary>
    ''' <param name="sObjeto">Tipo de Objeto de Aplicacion por el que filtrar</param>
    ''' <param name="sId">Id del Objeto de Aplicacion</param>
    ''' <returns>false: No existe, true: Existe (al menos alguno)</returns>
    Public Function ExisteObjetoQlik(ByVal sObjeto As String, ByVal sId As String) As Boolean
        ''Comprobamos si existe el objeto de aplicacion indicado
        Dim sFiltro As String = "objectType eq '" & sObjeto & "' and id eq " & sId

        Return ComprobarEntidadesQlik("app/object", sFiltro)
    End Function

#End Region

#End Region

#Region "Ticket de autenticacion Qlik"

    Public Class QlikTicket
        Inherits Security

        Private jsSerializer As New JavaScriptSerializer()

        Public RestAPIUri As String

        Public UserDirectory As String
        Public UserId As String
        Public TargetId As String
        Public Attributes As New ArrayList()

        Public Class TicketResponse

            Public UserDirectory As String
            Public UserId As String
            Public Attributes As New ArrayList()
            Public Ticket As String
            Public TargetUri As String

        End Class

        ''' <summary>
        ''' Constructor de la clase
        ''' </summary>
        ''' <param name="sRestAPIUri">URL de la API de autenticacion en Proxy</param>
        ''' <param name="sUserDirectory">Dominio del usuario de Qlik para el que se pide el Ticket</param>
        ''' <param name="sUserId">ID del usuario de Qlik para el que se pide el Ticket</param>
        ''' <param name="alAttributes">Opcional, lista de diccionarios con atributos del usuario</param>
        ''' <param name="sTargetId">Opcional</param>
        Public Sub New(ByVal sRestAPIUri As String, ByVal sUserDirectory As String, ByVal sUserId As String, Optional ByVal alAttributes As ArrayList = Nothing, Optional ByVal sTargetId As String = Nothing)
            Me.RestAPIUri = sRestAPIUri
            Me.UserDirectory = sUserDirectory
            Me.UserId = sUserId
            Me.TargetId = sTargetId
            Me.Attributes = alAttributes
            jsSerializer.MaxJsonLength = Int32.MaxValue
        End Sub

        ''' <summary>
        ''' Consulta a la API de autenticacion en Proxy de Qlik por un Ticket de autenticacion.
        ''' </summary>
        ''' <returns>Devuelve un objeto Ticket con las propiedades que retorna la API</returns>
        Public Function ObtenerTicket() As TicketResponse
            Dim oComunicadorQlikAPI As New QlikAPIsComm(DBServer, mIsAuthenticated)
            Dim stStream As Stream = oComunicadorQlikAPI.Execute(Me, "POST", RestAPIUri & "/qps/", "ticket", New ArrayList(), New ArrayList())

            If Not IsNothing(stStream) AndAlso Not String.IsNullOrEmpty(stStream.ToString()) Then
                Return jsSerializer.Deserialize(Of TicketResponse)(New StreamReader(stStream).ReadToEnd())
            Else
                Dim ticketVacio As New TicketResponse()
                Return ticketVacio
            End If

        End Function

    End Class

    ''' <summary>
    ''' Consulta a la API de autenticacion en Proxy de Qlik por un Ticket de autenticacion.
    ''' </summary>
    ''' <param name="sDominioUsuarioQlik">Dominio del usuario de Qlik para el que se pide el Ticket</param>
    ''' <param name="sUsuarioQlik">ID del usuario de Qlik para el que se pide el Ticket</param>
    ''' <param name="alAttributes">Opcional, lista de diccionarios con atributos del usuario</param>
    ''' <param name="sTargetId">Opcional</param>
    ''' <returns>Devuelve un objeto Ticket con las propiedades que retorna la API</returns>
    Public Function ObtenerTicketQlik(ByVal sDominioUsuarioQlik As String, ByVal sUsuarioQlik As String, Optional ByVal alAttributes As ArrayList = Nothing, Optional ByVal sTargetId As String = Nothing) As QlikTicket.TicketResponse
        Dim ticket As New QlikTicket(Me.sUrlProxyServiceAPI, sDominioUsuarioQlik, sUsuarioQlik, alAttributes, sTargetId)
        Return ticket.ObtenerTicket()
    End Function

#End Region

End Class

Public Class QlikAPIsComm
    Inherits Security

    Private jsSerializer As New JavaScriptSerializer()

    Private cersCertificadosValidos As System.Security.Cryptography.X509Certificates.X509Certificate2Collection
    Private cerCertificado As System.Security.Cryptography.X509Certificates.X509Certificate2

    ''' <summary>
    ''' Constructor de la clase QlikApp
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si está autenticado</param>
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
        jsSerializer.MaxJsonLength = Int32.MaxValue
    End Sub

    ''' <summary>
    ''' Genera una cadena aleatoria para usarla como XrfKey 
    ''' </summary>
    ''' <returns>Cadena aleatoria de 16 caracteres</returns>
    Private Function GenerateXrfKey() As String
        Dim caAllowedChars As String = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ0123456789".ToCharArray()
        Dim caChars As Char()
        ReDim caChars(15)
        Dim rdRandom As New Random()

        For i As Integer = 0 To 15
            caChars(i) = caAllowedChars(rdRandom.Next(0, caAllowedChars.Length))
        Next i

        Return New String(caChars)

    End Function

    ''' <summary>
    ''' Localiza el certificado de Qlik en la maquina
    ''' </summary>
    ''' <param name="sUrlDestino">Url de la API sin el endpoint que se quiere consultar</param>
    Private Sub LocateCertificate(ByVal sUrlDestino As String)
        '' Consultamos los certificados alojados como personales a nivel de máquina
        Dim CertificateStore As X509Store = New X509Store(StoreName.My, StoreLocation.LocalMachine)
        CertificateStore.Open(OpenFlags.ReadOnly)
        '' Obtenemos los certificados válidos por su período de validez en función de la fecha actual
        cersCertificadosValidos = CertificateStore.Certificates.Find(X509FindType.FindByTimeValid, DateTime.Now, False)
        For Each cerCertificadoValido As X509Certificate2 In cersCertificadosValidos
            '' Si el certificado es de Qlik Sense y la Url de destino contiene la IP almacenada en la propiedad editable, nos quedamos con él 
            If ((cerCertificadoValido.SubjectName.Name.Contains("Qlik")) AndAlso (sUrlDestino.Contains(cerCertificadoValido.FriendlyName))) Then
                cerCertificado = cerCertificadoValido
                Exit For
            End If
        Next
        ''cerCertificado = CertificateStore.Certificates.Cast(Of X509Certificate2).FirstOrDefault(Function(c) c.FriendlyName = "QlikClient")
        CertificateStore.Close()
        ServicePointManager.ServerCertificateValidationCallback = AddressOf AcceptAllCertifications

    End Sub

    ''' <summary>
    ''' Devuelve True
    ''' </summary>
    ''' <returns>True</returns>
    Private Function AcceptAllCertifications(ByVal sender As Object, ByVal certification As System.Security.Cryptography.X509Certificates.X509Certificate2, ByVal chain As System.Security.Cryptography.X509Certificates.X509Chain, ByVal sslPolicyErrors As System.Net.Security.SslPolicyErrors) As Boolean
        Return True
    End Function

    ''' <summary>
    ''' Ejecuta la consulta a la API de Qlik indicada
    ''' </summary>
    ''' <param name="oClaseASerializar">Clase con propiedades para serializar e incrustar en el body en caso de llamada con POST</param>
    ''' <param name="sQlikAPIMethod">GET, POST, ...</param>
    ''' <param name="sQlikAPIUri">URL de la API sin el Endpoint</param>
    ''' <param name="sEndPoint">Endpoint de la API</param>
    ''' <param name="alHeaders">Lista con pares claves valor para insertar como cabeceras en la llamada a la API</param>
    ''' <param name="alParams">Lista con pares claves valor para insertar como parametros QueryString en la llamada a la API</param>
    ''' <returns>Un stream con la respuesta de la API de Qlik consultada</returns>
    Public Function Execute(ByVal oClaseASerializar As Object, ByVal sQlikAPIMethod As String, ByVal sQlikAPIUri As String, ByVal sEndPoint As String, ByVal alHeaders As ArrayList, ByVal alParams As ArrayList) As Stream

        Try

            ''Serializamos la clase con las propiedades a JSON
            Dim json As String = jsSerializer.Serialize(oClaseASerializar)

            ''Creamos la URL con el EndPoint de la API
            Dim url As Uri = CombineUri(sQlikAPIUri, sEndPoint)

            ''Obtenemos el certificado
            LocateCertificate(sQlikAPIUri)

            ''Creamos el HTTP Request con las cabeceras y el contenido
            Dim sXrfkey As String = GenerateXrfKey()

            Dim sQueryString As String = "?Xrfkey=" & sXrfkey
            For i As Integer = 0 To (alParams.Count - 1)
                Dim diParametro As Dictionary(Of String, String) = alParams(i)
                sQueryString &= "&" & diParametro.Keys(0).ToString() & "=" & diParametro.Values(0).ToString()
            Next i

            Dim hwrRequest As HttpWebRequest = DirectCast(WebRequest.Create(url.ToString() & sQueryString), HttpWebRequest)

            hwrRequest.Method = sQlikAPIMethod
            hwrRequest.Accept = "application/json"
            hwrRequest.Headers.Add("X-Qlik-Xrfkey", sXrfkey)
            For i As Integer = 0 To (alHeaders.Count - 1)
                Dim diHeader As Dictionary(Of String, String) = alHeaders(i)
                hwrRequest.Headers.Add(diHeader.Keys(0).ToString(), diHeader.Values(0).ToString())
            Next i

            If cerCertificado Is Nothing Then
                Return Nothing
            End If

            hwrRequest.ClientCertificates.Add(cerCertificado)

            Dim baBodyBytes As Byte() = Encoding.UTF8.GetBytes(json)

            If Not String.IsNullOrEmpty(json) AndAlso (json <> "{}") Then
                hwrRequest.ContentType = "application/json"
                hwrRequest.ContentLength = baBodyBytes.Length
                Dim stRequestStream As Stream = hwrRequest.GetRequestStream()
                stRequestStream.Write(baBodyBytes, 0, baBodyBytes.Length)
                stRequestStream.Close()
            End If

            ''Lanzamos el Request
            Dim hwrResponse As HttpWebResponse = DirectCast(hwrRequest.GetResponse(), HttpWebResponse)

            Return hwrResponse.GetResponseStream()

        Catch ex As Exception

            Return Nothing

        End Try

    End Function

    ''' <summary>
    ''' Genera la Uri de la API sin parametros
    ''' </summary>
    ''' <param name="sBaseUri">Ruta de la API sin el Endpoint</param>
    ''' <param name="sRelativeOrAbsoluteUri">Endpoint de la API</param>
    ''' <returns>La Uri de la API</returns>
    Private Function CombineUri(ByVal sBaseUri As String, ByVal sRelativeOrAbsoluteUri As String) As Uri

        If Not sBaseUri.EndsWith("/") Then
            sBaseUri &= "/"
        End If

        Return New Uri(New Uri(sBaseUri), sRelativeOrAbsoluteUri)

    End Function

End Class
