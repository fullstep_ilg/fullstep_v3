﻿Imports System.Web

Public Class Acceso
    Inherits Security

    Public horaAcceso As String
    Public numUsuariosPortal As Integer
    Public numUsuariosWeb As Integer
    Public numUsuariosGS As Integer
    Public avgUsuariosPortal As Double
    Public avgUsuariosWeb As Double
    Public avgUsuariosGS As Double
    Public nowUsuariosPortal As Integer
    Public nowUsuariosWeb As Integer
    Public nowUsuariosGS As Integer
    Public totalUsuariosGS As Integer
    Public totalUsuariosWeb As Integer
    Public totalUsuariosPortal As Integer
    Public tipoEvento As String
    Public numUsuariosActividad As Integer
    Public avgUsuariosActividad As Double
    Public nowUsuariosActividad As Integer
    Public totalUsuariosActividad As Integer
    Public horaAccesoActividad As String
    ''' <summary>
    ''' Constructor de la clase Acceso
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si está autenticado</param>
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
End Class
Public Class Accesos
    Inherits Security

    Private moAccesos As Collection
    Private mData As DataSet
    Private mDataTotal As DataSet
    Private oAcceso As Acceso
    Private textosBI As ArrayList

    ReadOnly Property data() As DataSet
        Get
            Return mData
        End Get
    End Property
    Public ReadOnly Property Accesos() As Collection
        Get
            Return moAccesos
        End Get
    End Property
    ''' <summary>
    ''' Función que devuelve un dataset con todos los accesos en una hora determinada a un determinado producto
    ''' </summary>
    ''' <remarks>
    ''' Llamada desde:  Tiempo: 0 seg
    ''' </remarks>
    Public Function LoadPorRango(ByVal horaInicio As DateTime, ByVal horaFin As DateTime, ByVal rango As String, ByVal entorno As String, ByVal pyme As String)
        Dim i As Integer
        Select Case UCase(rango)
            Case "HORA-NOW"
                Dim avgUsuariosGS As Integer = 0
                Dim avgUsuariosPortal As Integer = 0
                Dim avgUsuariosWeb As Integer = 0
                mData = DBServer.Accesos_Load_Por_Cada_Cinco_Minutos(horaInicio, horaFin, entorno, pyme)
                mDataTotal = DBServer.Accesos_Load_Por_Cada_Cinco_Minutos_Totales_Now(horaInicio, horaFin, entorno, pyme)
                If Not data Is Nothing AndAlso Not data.Tables(0).Rows.Count = 0 Then
                    If (mDataTotal.Tables(0).Rows.Count = 0) Then
                        For i = 0 To data.Tables(0).Rows.Count - 1
                            Me.AddHoursAndMins(i, mData.Tables(0).Rows(i).Item("FECHA"), mData.Tables(0).Rows(i).Item("ACTIVOS_PORTAL"), mData.Tables(0).Rows(i).Item("ACTIVOS_WEB"), mData.Tables(0).Rows(i).Item("ACTIVOS_GS"), mData.Tables(0).Rows(i).Item("AVG_PORTAL"), mData.Tables(0).Rows(i).Item("AVG_WEB"), mData.Tables(0).Rows(i).Item("AVG_GS"), 0, 0, 0)
                        Next
                    Else
                        For i = 0 To data.Tables(0).Rows.Count - 1
                            Me.AddHoursAndMins(i, mData.Tables(0).Rows(i).Item("FECHA"), mData.Tables(0).Rows(i).Item("ACTIVOS_PORTAL"), mData.Tables(0).Rows(i).Item("ACTIVOS_WEB"), mData.Tables(0).Rows(i).Item("ACTIVOS_GS"), mData.Tables(0).Rows(i).Item("AVG_PORTAL"), mData.Tables(0).Rows(i).Item("AVG_WEB"), mData.Tables(0).Rows(i).Item("AVG_GS"), mDataTotal.Tables(0).Rows(0).Item("GS"), mDataTotal.Tables(0).Rows(0).Item("WEB"), mDataTotal.Tables(0).Rows(0).Item("PORTAL"))
                        Next
                    End If
                End If
                Return (moAccesos)
            Case "HORA"
                Dim avgUsuariosGS As Integer = 0
                Dim avgUsuariosPortal As Integer = 0
                Dim avgUsuariosWeb As Integer = 0
                mData = DBServer.Accesos_Load_Por_Cada_Cinco_Minutos(horaInicio, horaFin, entorno, pyme)
                mDataTotal = DBServer.Accesos_Load_Por_Cada_Cinco_Minutos_Totales(horaInicio, horaFin, entorno, pyme)
                If Not data Is Nothing AndAlso Not data.Tables(0).Rows.Count = 0 Then
                    If (mDataTotal.Tables(0).Rows.Count = 0) Then
                        For i = 0 To data.Tables(0).Rows.Count - 1
                            Me.AddHoursAndMins(i, mData.Tables(0).Rows(i).Item("FECHA"), mData.Tables(0).Rows(i).Item("ACTIVOS_PORTAL"), mData.Tables(0).Rows(i).Item("ACTIVOS_WEB"), mData.Tables(0).Rows(i).Item("ACTIVOS_GS"), mData.Tables(0).Rows(i).Item("AVG_PORTAL"), mData.Tables(0).Rows(i).Item("AVG_WEB"), mData.Tables(0).Rows(i).Item("AVG_GS"), 0, 0, 0)
                        Next
                    Else
                        For i = 0 To data.Tables(0).Rows.Count - 1
                            Me.AddHoursAndMins(i, mData.Tables(0).Rows(i).Item("FECHA"), mData.Tables(0).Rows(i).Item("ACTIVOS_PORTAL"), mData.Tables(0).Rows(i).Item("ACTIVOS_WEB"), mData.Tables(0).Rows(i).Item("ACTIVOS_GS"), mData.Tables(0).Rows(i).Item("AVG_PORTAL"), mData.Tables(0).Rows(i).Item("AVG_WEB"), mData.Tables(0).Rows(i).Item("AVG_GS"), mDataTotal.Tables(0).Rows(0).Item("TOTAL_GS"), mDataTotal.Tables(0).Rows(0).Item("TOTAL_WEB"), mDataTotal.Tables(0).Rows(0).Item("TOTAL_PORTAL"))

                        Next
                    End If
                End If
                Return (moAccesos)
            Case "DIA"
                mData = DBServer.Accesos_Load_Por_Hora(horaInicio, horaFin, entorno, pyme)
                mDataTotal = DBServer.Accesos_Load_Por_Hora_Totales(horaInicio, horaFin, entorno, pyme)
                If Not data.Tables(0).Rows.Count = 0 Then
                    If (mDataTotal.Tables(0).Rows.Count = 0) Then
                        For i = 0 To data.Tables(0).Rows.Count - 1
                            Me.AddHours(mData.Tables(0).Rows(i).Item("HORA_ACCESO"), mData.Tables(0).Rows(i).Item("ACTIVOS_GS"), mData.Tables(0).Rows(i).Item("ACTIVOS_WEB"), mData.Tables(0).Rows(i).Item("ACTIVOS_PORTAL"), mData.Tables(0).Rows(i).Item("AVG_GS"), mData.Tables(0).Rows(i).Item("AVG_WEB"), mData.Tables(0).Rows(i).Item("AVG_PORTAL"), 0, 0, 0)
                        Next
                    Else
                        For i = 0 To data.Tables(0).Rows.Count - 1
                            Me.AddHours(mData.Tables(0).Rows(i).Item("HORA_ACCESO"), mData.Tables(0).Rows(i).Item("ACTIVOS_GS"), mData.Tables(0).Rows(i).Item("ACTIVOS_WEB"), mData.Tables(0).Rows(i).Item("ACTIVOS_PORTAL"), mData.Tables(0).Rows(i).Item("AVG_GS"), mData.Tables(0).Rows(i).Item("AVG_WEB"), mData.Tables(0).Rows(i).Item("AVG_PORTAL"), mDataTotal.Tables(0).Rows(0).Item("TOTAL_GS"), mDataTotal.Tables(0).Rows(0).Item("TOTAL_WEB"), mDataTotal.Tables(0).Rows(0).Item("TOTAL_PORTAL"))
                        Next
                    End If
                End If
                Return moAccesos
            Case "SEMANA"
                mData = DBServer.Accesos_Load_Por_Dia_Semana(horaInicio, horaFin, entorno, pyme)
                mDataTotal = DBServer.Accesos_Load_Por_Dia_Semana_Totales(horaInicio, horaFin, entorno, pyme)
                If Not data.Tables(0).Rows.Count = 0 Then
                    If (mDataTotal.Tables(0).Rows.Count = 0) Then
                        For i = 0 To data.Tables(0).Rows.Count - 1
                            Me.AddDaysInWeek(i, horaInicio, mData.Tables(0).Rows(i).Item("FECHA"), mData.Tables(0).Rows(i).Item("ACTIVOS_PORTAL"), mData.Tables(0).Rows(i).Item("ACTIVOS_WEB"), mData.Tables(0).Rows(i).Item("ACTIVOS_GS"), mData.Tables(0).Rows(i).Item("AVG_PORTAL"), mData.Tables(0).Rows(i).Item("AVG_WEB"), mData.Tables(0).Rows(i).Item("AVG_GS"), 0, 0, 0)
                        Next
                    Else
                        For i = 0 To data.Tables(0).Rows.Count - 1
                            Me.AddDaysInWeek(i, horaInicio, mData.Tables(0).Rows(i).Item("FECHA"), mData.Tables(0).Rows(i).Item("ACTIVOS_PORTAL"), mData.Tables(0).Rows(i).Item("ACTIVOS_WEB"), mData.Tables(0).Rows(i).Item("ACTIVOS_GS"), mData.Tables(0).Rows(i).Item("AVG_PORTAL"), mData.Tables(0).Rows(i).Item("AVG_WEB"), mData.Tables(0).Rows(i).Item("AVG_GS"), mDataTotal.Tables(0).Rows(0).Item("TOTAL_GS"), mDataTotal.Tables(0).Rows(0).Item("TOTAL_WEB"), mDataTotal.Tables(0).Rows(0).Item("TOTAL_PORTAL"))
                        Next
                    End If
                End If
                Return moAccesos
            Case "MES"
                mData = DBServer.Accesos_Load_Por_Dia_Mes(horaInicio, horaFin, entorno, pyme)
                mDataTotal = DBServer.Accesos_Load_Por_Dia_Mes_Totales(horaInicio, horaFin, entorno, pyme)
                If Not data.Tables(0).Rows.Count = 0 Then
                    If (mDataTotal.Tables(0).Rows.Count = 0) Then
                        For i = 0 To data.Tables(0).Rows.Count - 1
                            Me.AddDays(i, mData.Tables(0).Rows(i).Item("FECHA"), mData.Tables(0).Rows(i).Item("ACTIVOS_PORTAL"), mData.Tables(0).Rows(i).Item("ACTIVOS_WEB"), mData.Tables(0).Rows(i).Item("ACTIVOS_GS"), mData.Tables(0).Rows(i).Item("AVG_PORTAL"), mData.Tables(0).Rows(i).Item("AVG_WEB"), mData.Tables(0).Rows(i).Item("AVG_GS"), 0, 0, 0)
                        Next
                    Else
                        For i = 0 To data.Tables(0).Rows.Count - 1
                            Me.AddDays(i, mData.Tables(0).Rows(i).Item("FECHA"), mData.Tables(0).Rows(i).Item("ACTIVOS_PORTAL"), mData.Tables(0).Rows(i).Item("ACTIVOS_WEB"), mData.Tables(0).Rows(i).Item("ACTIVOS_GS"), mData.Tables(0).Rows(i).Item("AVG_PORTAL"), mData.Tables(0).Rows(i).Item("AVG_WEB"), mData.Tables(0).Rows(i).Item("AVG_GS"), mDataTotal.Tables(0).Rows(0).Item("TOTAL_GS"), mDataTotal.Tables(0).Rows(0).Item("TOTAL_WEB"), mDataTotal.Tables(0).Rows(0).Item("TOTAL_PORTAL"))
                        Next
                    End If
                End If
                Return moAccesos
            Case "ANYO"
                Dim year As String = horaInicio.Year
                mData = DBServer.Accesos_Load_Por_Mes(year, entorno, pyme)
                mDataTotal = DBServer.Accesos_Load_Por_Mes_Totales(year, entorno, pyme)
                If (textosBI Is Nothing) Then
                    Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
                    textosBI = DBServer.TextoBi_Carga(181, FSNUser.IdiomaCod)
                End If
                If Not data.Tables(0).Rows.Count = 0 Then
                    If (mDataTotal.Tables(0).Rows.Count = 0) Then
                        For i = 0 To data.Tables(0).Rows.Count - 1
                            Dim mesAnyo As String = String.Empty
                            Select Case i
                                Case 0 : mesAnyo = textosBI(0) '"Jan"
                                Case 1 : mesAnyo = textosBI(1) '"Feb"
                                Case 2 : mesAnyo = textosBI(2) '"Mar"
                                Case 3 : mesAnyo = textosBI(3) '"Apr"
                                Case 4 : mesAnyo = textosBI(4) '"May"
                                Case 5 : mesAnyo = textosBI(5) '"Jun"
                                Case 6 : mesAnyo = textosBI(6) '"Jul"
                                Case 7 : mesAnyo = textosBI(7) '"Aug"
                                Case 8 : mesAnyo = textosBI(8) '"Sep"
                                Case 9 : mesAnyo = textosBI(9) '"Oct"
                                Case 10 : mesAnyo = textosBI(10) '"Nov"
                                Case 11 : mesAnyo = textosBI(11) '"Dec"
                            End Select
                            Me.AddMonths(i, mesAnyo, mData.Tables(0).Rows(i).Item("ACTIVOS_PORTAL"), mData.Tables(0).Rows(i).Item("ACTIVOS_WEB"), mData.Tables(0).Rows(i).Item("ACTIVOS_GS"), mData.Tables(0).Rows(i).Item("AVG_PORTAL"), mData.Tables(0).Rows(i).Item("AVG_WEB"), mData.Tables(0).Rows(i).Item("AVG_GS"), 0, 0, 0)

                        Next
                    Else
                        For i = 0 To data.Tables(0).Rows.Count - 1
                            Dim mesAnyo As String = String.Empty
                            Select Case i
                                Case 0 : mesAnyo = textosBI(0) '"Jan"
                                Case 1 : mesAnyo = textosBI(1) '"Feb"
                                Case 2 : mesAnyo = textosBI(2) '"Mar"
                                Case 3 : mesAnyo = textosBI(3) '"Apr"
                                Case 4 : mesAnyo = textosBI(4) '"May"
                                Case 5 : mesAnyo = textosBI(5) '"Jun"
                                Case 6 : mesAnyo = textosBI(6) '"Jul"
                                Case 7 : mesAnyo = textosBI(7) '"Aug"
                                Case 8 : mesAnyo = textosBI(8) '"Sep"
                                Case 9 : mesAnyo = textosBI(9) '"Oct"
                                Case 10 : mesAnyo = textosBI(10) '"Nov"
                                Case 11 : mesAnyo = textosBI(11) '"Dec"
                            End Select
                            Me.AddMonths(i, mesAnyo, mData.Tables(0).Rows(i).Item("ACTIVOS_PORTAL"), mData.Tables(0).Rows(i).Item("ACTIVOS_WEB"), mData.Tables(0).Rows(i).Item("ACTIVOS_GS"), mData.Tables(0).Rows(i).Item("AVG_PORTAL"), mData.Tables(0).Rows(i).Item("AVG_WEB"), mData.Tables(0).Rows(i).Item("AVG_GS"), mDataTotal.Tables(0).Rows(0).Item("TOTAL_GS"), mDataTotal.Tables(0).Rows(0).Item("TOTAL_WEB"), mDataTotal.Tables(0).Rows(0).Item("TOTAL_PORTAL"))
                        Next
                    End If
                End If
                Return moAccesos
            Case Else
                Return Nothing
        End Select
    End Function
    ''' <summary>
    ''' Función que devuelve un dataset con todos los eventos producidos en una hora determinada a un determinado producto
    ''' </summary>
    ''' <remarks>
    ''' Llamada desde:  Tiempo: 0 seg
    ''' </remarks>
    Public Function LoadPorRangoRecentActivity(fechaDesde, fechaHasta, entorno, rango, tipoEvento, pyme)
        Dim i As Integer
        Select Case UCase(rango)
            Case "HORA-NOW", "HORA"
                Dim avgUsuariosGS As Integer = 0
                Dim avgUsuariosPortal As Integer = 0
                Dim avgUsuariosWeb As Integer = 0
                mData = DBServer.Activity_Load_Por_Cada_Cinco_Minutos(fechaDesde, fechaHasta, entorno, tipoEvento, pyme)
                If Not data Is Nothing AndAlso Not data.Tables(0).Rows.Count = 0 Then
                    For i = 0 To data.Tables(0).Rows.Count - 1
                        Me.AddActivityHoursAndMins(mData.Tables(0).Rows(i).Item("FECHA"), mData.Tables(0).Rows(i).Item("NUM_EVENTOS"), mData.Tables(0).Rows(i).Item("AVG_EVENTOS"))
                    Next
                End If
                Return (moAccesos)
            Case "DIA"
                mData = DBServer.Activity_Load_Por_Hora(fechaDesde, fechaHasta, entorno, tipoEvento, pyme)
                If Not data.Tables(0).Rows.Count = 0 Then
                    For i = 0 To data.Tables(0).Rows.Count - 1
                        Me.AddActivityHours(mData.Tables(0).Rows(i).Item("HORA_ACCESO"), mData.Tables(0).Rows(i).Item("NUM_EVENTOS"), mData.Tables(0).Rows(i).Item("AVG_EVENTOS"))
                    Next
                End If
                Return moAccesos
            Case "SEMANA"
                mData = DBServer.Activity_Load_Por_Dia_Semana(fechaDesde, fechaHasta, entorno, tipoEvento, pyme)
                If Not data.Tables(0).Rows.Count = 0 Then
                    For i = 0 To data.Tables(0).Rows.Count - 1
                        Me.AddActivityDaysInWeek(fechaDesde, mData.Tables(0).Rows(i).Item("FECHA"), mData.Tables(0).Rows(i).Item("NUM_EVENTOS"), mData.Tables(0).Rows(i).Item("AVG_EVENTOS"))
                    Next
                End If
                Return moAccesos
            Case "MES"
                mData = DBServer.Activity_Load_Por_Dia_Mes(fechaDesde, fechaHasta, entorno, tipoEvento, pyme)
                If Not data.Tables(0).Rows.Count = 0 Then
                    For i = 0 To data.Tables(0).Rows.Count - 1
                        Me.AddActivityDays(mData.Tables(0).Rows(i).Item("FECHA"), mData.Tables(0).Rows(i).Item("NUM_EVENTOS"), mData.Tables(0).Rows(i).Item("AVG_EVENTOS"))
                    Next
                End If
                Return moAccesos
            Case "ANYO"
                Dim year As String = fechaDesde.Year
                mData = DBServer.Activity_Load_Por_Mes(fechaDesde, fechaHasta, entorno, tipoEvento, pyme)
                If (textosBI Is Nothing) Then
                    Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
                    textosBI = DBServer.TextoBi_Carga(181, FSNUser.IdiomaCod)
                End If
                If Not data.Tables(0).Rows.Count = 0 Then
                    For i = 0 To data.Tables(0).Rows.Count - 1
                        Dim mesAnyo As String = mData.Tables(0).Rows(i).Item("FECHA")
                        Dim partes() = mesAnyo.Split("/")
                        Dim fecha As String = String.Empty
                        Select Case partes(0)
                            Case "01" : fecha = textosBI(0) '"Jan"
                            Case "02" : fecha = textosBI(1) '"Feb"
                            Case "03" : fecha = textosBI(2) '"Mar"
                            Case "04" : fecha = textosBI(3) '"Apr"
                            Case "05" : fecha = textosBI(4) '"May"
                            Case "06" : fecha = textosBI(5) '"Jun"
                            Case "07" : fecha = textosBI(6) '"Jul"
                            Case "08" : fecha = textosBI(7) '"Aug"
                            Case "09" : fecha = textosBI(8) '"Sep"
                            Case "10" : fecha = textosBI(9) '"Oct"
                            Case "11" : fecha = textosBI(10) '"Nov"
                            Case "12" : fecha = textosBI(11) '"Dec"
                        End Select
                        Me.AddActivityMonths(fecha, mData.Tables(0).Rows(i).Item("NUM_EVENTOS"), mData.Tables(0).Rows(i).Item("AVG_EVENTOS"))
                    Next
                End If
                Return moAccesos
            Case Else
                Return Nothing
        End Select
    End Function
    'Horas y minutos para la grafica de actividad
    Friend Sub AddActivityHoursAndMins(ByVal horaAcceso As String, ByVal numEventos As Integer, ByVal avgEventos As Double)
        Dim oAcceso As New Acceso(DBServer, mIsAuthenticated)
        If moAccesos Is Nothing Then
            moAccesos = New Collection
        End If
        oAcceso.numUsuariosActividad = numEventos
        oAcceso.avgUsuariosActividad = avgEventos
        oAcceso.horaAccesoActividad = horaAcceso
        moAccesos.Add(oAcceso)
    End Sub
    'Horas para la grafica de actividad
    Friend Sub AddActivityHours(ByVal fecha As String, ByVal numEventos As Integer, ByVal avgEventos As Double)
        Dim oAcceso As New Acceso(DBServer, mIsAuthenticated)
        If moAccesos Is Nothing Then
            moAccesos = New Collection
        End If
        oAcceso.horaAccesoActividad = fecha
        oAcceso.numUsuariosActividad = numEventos
        oAcceso.avgUsuariosActividad = avgEventos
        moAccesos.Add(oAcceso)
    End Sub
    'Dias de la semana para la grafica de actividad(semana) 
    Friend Sub AddActivityDaysInWeek(ByVal fechaDesde As DateTime, ByVal fecha As String, ByVal numEventos As Integer, ByVal avgEventos As Double)
        Dim oAcceso As New Acceso(DBServer, mIsAuthenticated)
        If moAccesos Is Nothing Then
            moAccesos = New Collection
        End If
        Dim dia() = fecha.Split("/")
        Dim diaInt As Integer
        Integer.TryParse(dia(2), diaInt)
        Dim fechaDate = New DateTime(dia(2), dia(1), dia(0))
        Dim diaSemana = (fechaDate - fechaDesde).TotalDays
        If (textosBI Is Nothing) Then
            Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
            textosBI = DBServer.TextoBi_Carga(181, FSNUser.IdiomaCod)
        End If
        If (diaSemana = 0) Then
            oAcceso.horaAccesoActividad = textosBI(41) '"Mon"
        ElseIf (diaSemana = 1) Then
            oAcceso.horaAccesoActividad = textosBI(42) '"Tue"
        ElseIf (diaSemana = 2) Then
            oAcceso.horaAccesoActividad = textosBI(43) '"Wed"
        ElseIf (diaSemana = 3) Then
            oAcceso.horaAccesoActividad = textosBI(44) '"Thu"
        ElseIf (diaSemana = 4) Then
            oAcceso.horaAccesoActividad = textosBI(45) '"Fri"
        ElseIf (diaSemana = 5) Then
            oAcceso.horaAccesoActividad = textosBI(46) '"Sat"
        ElseIf (diaSemana = 6) Then
            oAcceso.horaAccesoActividad = textosBI(47) '"Sun"
        End If

        oAcceso.numUsuariosActividad = numEventos
        oAcceso.avgUsuariosActividad = avgEventos
        moAccesos.Add(oAcceso)
    End Sub
    'Dias para la grafica de actividad(mes)
    Friend Sub AddActivityDays(ByVal fecha As String, ByVal numEventos As Integer, ByVal avgEventos As Double)
        Dim oAcceso As New Acceso(DBServer, mIsAuthenticated)
        If moAccesos Is Nothing Then
            moAccesos = New Collection
        End If
        Dim dia() = fecha.Split("/")
        oAcceso.horaAccesoActividad = dia(0)
        oAcceso.numUsuariosActividad = numEventos
        oAcceso.avgUsuariosActividad = avgEventos
        moAccesos.Add(oAcceso)
    End Sub
    'Meses para la grafica de actividad(anyo)
    Friend Sub AddActivityMonths(ByVal mes As String, ByVal numEventos As Integer, ByVal avgEventos As Double)
        Dim oAcceso As New Acceso(DBServer, mIsAuthenticated)
        If moAccesos Is Nothing Then
            moAccesos = New Collection
        End If
        oAcceso.horaAccesoActividad = mes
        oAcceso.numUsuariosActividad = numEventos
        oAcceso.avgUsuariosActividad = avgEventos
        moAccesos.Add(oAcceso)
    End Sub
    ''' <summary>
    ''' Añade accesos por horas para la grafica de usuarios online (dia)
    ''' </summary>
    Friend Sub AddHours(ByVal horaAcceso As String, ByVal activosGs As Integer, ByVal activosWeb As Integer, ByVal activosPortal As Integer, ByVal avgGS As Integer, ByVal avgWeb As Integer, ByVal avgPortal As Integer, ByVal totalGs As Integer, ByVal totalWeb As Integer, ByVal totalPortal As Integer)
        Dim oAcceso As New Acceso(DBServer, mIsAuthenticated)
        If moAccesos Is Nothing Then
            moAccesos = New Collection
        End If

        oAcceso.horaAcceso = horaAcceso
        oAcceso.numUsuariosPortal = activosPortal
        oAcceso.numUsuariosWeb = activosWeb
        '************ DSL: Expediente PRUEBAS_31900_9/2015/276 ************************************************
        'oAcceso.numUsuariosGS = numUsuariosGS
        Dim laHora As String() = horaAcceso.Split(":")
        If DatePart(DateInterval.Hour, Now()) >= CInt(laHora(0)) Then
            oAcceso.numUsuariosGS = activosGs
        Else
            oAcceso.numUsuariosGS = 0
        End If
        '*******************************************************************************************************
        oAcceso.numUsuariosGS = activosGs
        oAcceso.avgUsuariosGS = avgGS
        oAcceso.avgUsuariosPortal = avgPortal
        oAcceso.avgUsuariosWeb = avgWeb
        oAcceso.totalUsuariosGS = totalGs
        oAcceso.totalUsuariosWeb = totalWeb
        oAcceso.totalUsuariosPortal = totalPortal
        moAccesos.Add(oAcceso)

    End Sub
    ''' <summary>
    ''' Añade un acceso a la lista de accesos
    ''' </summary>
    Friend Sub AddHoursAndMins(ByVal cont As Integer, ByVal horaAcceso As DateTime, ByVal numUsuariosPortal As Integer, ByVal numUsuariosWeb As Integer, ByVal numUsuariosGS As Integer, ByVal avgUsuariosPortal As Double, ByVal avgUsuariosWeb As Double, ByVal avgUsuariosGS As Double, ByVal totalGS As Double, ByVal totalWeb As Double, ByVal totalPortal As Double)
        Dim oAcceso As New Acceso(DBServer, mIsAuthenticated)
        If moAccesos Is Nothing Then
            moAccesos = New Collection
        End If

        If (horaAcceso.Hour < 10) Then
            oAcceso.horaAcceso = "0" & horaAcceso.Hour & ":"
        Else
            oAcceso.horaAcceso = horaAcceso.Hour & ":"
        End If
        If (horaAcceso.Minute < 10) Then
            oAcceso.horaAcceso &= "0" & horaAcceso.Minute
        Else
            oAcceso.horaAcceso &= horaAcceso.Minute
        End If
        oAcceso.numUsuariosPortal = numUsuariosPortal
        oAcceso.numUsuariosWeb = numUsuariosWeb
        '************ DSL: Expediente PRUEBAS_31900_9/2015/276 ************************************************
        'oAcceso.numUsuariosGS = numUsuariosGS
        If DateDiff(DateInterval.Minute, Now(), horaAcceso) <= 0 Then
            'Dim x As Long = DateDiff(DateInterval.Minute, Now, horaAcceso)
            oAcceso.numUsuariosGS = numUsuariosGS

        Else
            oAcceso.numUsuariosGS = 0
        End If
        '******************************************************************************************************
        oAcceso.avgUsuariosPortal = avgUsuariosPortal
        oAcceso.avgUsuariosWeb = avgUsuariosWeb
        oAcceso.avgUsuariosGS = avgUsuariosGS
        oAcceso.totalUsuariosGS = totalGS
        oAcceso.totalUsuariosPortal = totalPortal
        oAcceso.totalUsuariosWeb = totalWeb
        moAccesos.Add(oAcceso, cont)
    End Sub
    ''' <summary>
    ''' Función que añade un acceso a la lista de accesos
    ''' </summary>
    Friend Sub AddDaysInWeek(ByVal cont As Integer, ByVal fechadesde As DateTime, ByVal fecha As String, ByVal numUsuariosPortal As Integer, ByVal numUsuariosWeb As Integer, ByVal numUsuariosGS As Integer, ByVal avgUsuariosPortal As Integer, ByVal avgUsuariosWeb As Integer, ByVal avgUsuariosGS As Integer, ByVal totalUsuariosGS As Integer, ByVal totalUsuariosWeb As Integer, ByVal totalUsuariosPortal As Integer)
        Dim oAcceso As New Acceso(DBServer, mIsAuthenticated)
        If moAccesos Is Nothing Then
            moAccesos = New Collection
        End If

        Dim dia() = fecha.Split("/")
        Dim diaInt As Integer
        Integer.TryParse(dia(2), diaInt)
        Dim fechaDate = New DateTime(dia(2), dia(1), dia(0))
        Dim diaSemana = (fechaDate - fechadesde).TotalDays
        If (textosBI Is Nothing) Then
            Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
            textosBI = DBServer.TextoBi_Carga(181, FSNUser.IdiomaCod)
        End If
        If (diaSemana = 0) Then
            oAcceso.horaAcceso = textosBI(41) '"Mon"
        ElseIf (diaSemana = 1) Then
            oAcceso.horaAcceso = textosBI(42) '"Tue"
        ElseIf (diaSemana = 2) Then
            oAcceso.horaAcceso = textosBI(43) '"Wed"
        ElseIf (diaSemana = 3) Then
            oAcceso.horaAcceso = textosBI(44) '"Thu"
        ElseIf (diaSemana = 4) Then
            oAcceso.horaAcceso = textosBI(45) '"Fri"
        ElseIf (diaSemana = 5) Then
            oAcceso.horaAcceso = textosBI(46) '"Sat"
        ElseIf (diaSemana = 6) Then
            oAcceso.horaAcceso = textosBI(47) '"Sun"
        End If

        oAcceso.numUsuariosPortal = numUsuariosPortal
        oAcceso.numUsuariosWeb = numUsuariosWeb
        oAcceso.numUsuariosGS = numUsuariosGS
        oAcceso.avgUsuariosPortal = avgUsuariosPortal
        oAcceso.avgUsuariosWeb = avgUsuariosWeb
        oAcceso.avgUsuariosGS = avgUsuariosGS
        oAcceso.totalUsuariosGS = totalUsuariosGS
        oAcceso.totalUsuariosPortal = totalUsuariosPortal
        oAcceso.totalUsuariosWeb = totalUsuariosWeb

        moAccesos.Add(oAcceso, cont)
    End Sub
    ''' <summary>
    ''' Función que añade un acceso a la lista de accesos
    ''' </summary>
    Friend Sub AddDays(ByVal cont As Integer, ByVal fecha As String, ByVal numUsuariosPortal As Integer, ByVal numUsuariosWeb As Integer, ByVal numUsuariosGS As Integer, ByVal avgUsuariosPortal As Integer, ByVal avgUsuariosWeb As Integer, ByVal avgUsuariosGS As Integer, ByVal totalUsuariosGS As Integer, ByVal totalUsuariosWeb As Integer, ByVal totalUsuariosPortal As Integer)
        Dim oAcceso As New Acceso(DBServer, mIsAuthenticated)
        If moAccesos Is Nothing Then
            moAccesos = New Collection
        End If
        Dim dia() = fecha.Split("/")
        oAcceso.horaAcceso = dia(0)
        oAcceso.numUsuariosPortal = numUsuariosPortal
        oAcceso.numUsuariosWeb = numUsuariosWeb
        oAcceso.numUsuariosGS = numUsuariosGS
        oAcceso.avgUsuariosPortal = avgUsuariosPortal
        oAcceso.avgUsuariosWeb = avgUsuariosWeb
        oAcceso.avgUsuariosGS = avgUsuariosGS
        oAcceso.totalUsuariosGS = totalUsuariosGS
        oAcceso.totalUsuariosWeb = totalUsuariosWeb
        oAcceso.totalUsuariosPortal = totalUsuariosPortal
        moAccesos.Add(oAcceso, cont)
    End Sub
    ''' <summary>
    ''' Función que añade un acceso a la lista de accesos
    ''' </summary>
    Friend Sub AddMonths(ByVal cont As Integer, ByVal mesAcceso As String, ByVal numUsuariosPortal As Integer, ByVal numUsuariosWeb As Integer, ByVal numUsuariosGS As Integer, ByVal avgUsuariosPortal As Integer, ByVal avgUsuariosWeb As Integer, ByVal avgUsuariosGS As Integer, ByVal totalUsuariosGS As Integer, ByVal totalUsuariosWeb As Integer, ByVal totalUsuariosPortal As Integer)
        Dim oAcceso As New Acceso(DBServer, mIsAuthenticated)
        If moAccesos Is Nothing Then
            moAccesos = New Collection
        End If


        oAcceso.horaAcceso = mesAcceso
        oAcceso.numUsuariosPortal = numUsuariosPortal
        oAcceso.numUsuariosWeb = numUsuariosWeb
        oAcceso.numUsuariosGS = numUsuariosGS
        oAcceso.avgUsuariosPortal = avgUsuariosPortal
        oAcceso.avgUsuariosWeb = avgUsuariosWeb
        oAcceso.avgUsuariosGS = avgUsuariosGS
        oAcceso.totalUsuariosGS = totalUsuariosGS
        oAcceso.totalUsuariosWeb = totalUsuariosWeb
        oAcceso.totalUsuariosPortal = totalUsuariosPortal
        moAccesos.Add(oAcceso, cont)
    End Sub
    ''' <summary>
    ''' Recupera el entorno en el que estamos(primer registro de la tabla FSAL_ENTORNOS)
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function RecuperarEntorno()
        Return DBServer.RecuperarEntorno()
    End Function
    ''' <summary>
    ''' Constructor de la clase Accesos
    ''' </summary>
    ''' <param name="dbserver">Servidor de la bbdd</param>
    ''' <param name="isAuthenticated">Si está autenticado</param>
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
End Class
