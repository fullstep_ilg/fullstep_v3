﻿Public Class InfoPlantilla
    Public Sub New()
        menuData = New Dictionary(Of String, String)
        documentoData = New Dictionary(Of String, String)
        formatoData = New Dictionary(Of String, String)
        vistaData = New Dictionary(Of String, String)
        UONsPlantilla = New List(Of cn_fsItem)
        languageInfo = New Dictionary(Of String, Plantilla)
    End Sub
    Private _Id As Long
    Public Property Id() As Long
        Get
            Return _Id
        End Get
        Set(ByVal value As Long)
            _Id = value
        End Set
    End Property
    Private _per As String
    Public Property Per() As String
        Get
            Return _per
        End Get
        Set(ByVal value As String)
            _per = value
        End Set
    End Property
    Private _menu As Integer
    Public Property Menu() As Integer
        Get
            Return _menu
        End Get
        Set(ByVal value As Integer)
            _menu = value
        End Set
    End Property
    Private _documento As Integer
    Public Property Documento() As Integer
        Get
            Return _documento
        End Get
        Set(ByVal value As Integer)
            _documento = value
        End Set
    End Property
    Private _vista As Integer
    Public Property Vista() As Integer
        Get
            Return _vista
        End Get
        Set(ByVal value As Integer)
            _vista = value
        End Set
    End Property
    Private _plantillaDefecto As Boolean
    Public Property PlantillaDefecto() As Boolean
        Get
            Return _plantillaDefecto
        End Get
        Set(ByVal value As Boolean)
            _plantillaDefecto = value
        End Set
    End Property
    Private _formatoDefecto As Integer
    Public Property FormatoDefecto() As Integer
        Get
            Return _formatoDefecto
        End Get
        Set(ByVal value As Integer)
            _formatoDefecto = value
        End Set
    End Property
    Private _menuData As Dictionary(Of String, String)
    Public Property menuData() As Dictionary(Of String, String)
        Get
            Return _menuData
        End Get
        Set(ByVal value As Dictionary(Of String, String))
            _menuData = value
        End Set
    End Property
    Private _documentoData As Dictionary(Of String, String)
    Public Property documentoData() As Dictionary(Of String, String)
        Get
            Return _documentoData
        End Get
        Set(ByVal value As Dictionary(Of String, String))
            _documentoData = value
        End Set
    End Property
    Private _vistaData As Dictionary(Of String, String)
    Public Property vistaData() As Dictionary(Of String, String)
        Get
            Return _vistaData
        End Get
        Set(ByVal value As Dictionary(Of String, String))
            _vistaData = value
        End Set
    End Property
    Private _formatoData As Dictionary(Of String, String)
    Public Property formatoData() As Dictionary(Of String, String)
        Get
            Return _formatoData
        End Get
        Set(ByVal value As Dictionary(Of String, String))
            _formatoData = value
        End Set
    End Property
    Private _languageInfo As Dictionary(Of String, Plantilla)
    Public Property languageInfo() As Dictionary(Of String, Plantilla)
        Get
            Return _languageInfo
        End Get
        Set(ByVal value As Dictionary(Of String, Plantilla))
            _languageInfo = value
        End Set
    End Property
    Private _uonsPlantilla As List(Of cn_fsItem)
    Public Property UONsPlantilla() As List(Of cn_fsItem)
        Get
            Return _uonsPlantilla
        End Get
        Set(ByVal value As List(Of cn_fsItem))
            _uonsPlantilla = value
        End Set
    End Property
End Class
Public Class Plantilla
    Private _idioma As String
    Public Property Idioma() As String
        Get
            Return _idioma
        End Get
        Set(ByVal value As String)
            _idioma = value
        End Set
    End Property
    Private _denominacionIdioma As String
    Public Property DenominacionIdioma() As String
        Get
            Return _denominacionIdioma
        End Get
        Set(ByVal value As String)
            _denominacionIdioma = value
        End Set
    End Property
    Private _idiomaDefecto As Boolean
    Public Property IdiomaDefecto() As Boolean
        Get
            Return _idiomaDefecto
        End Get
        Set(ByVal value As Boolean)
            _idiomaDefecto = value
        End Set
    End Property
    Private _denominacion As String
    Public Property Denominacion() As String
        Get
            Return _denominacion
        End Get
        Set(ByVal value As String)
            _denominacion = value
        End Set
    End Property
    Private _comentario As String
    Public Property Comentario() As String
        Get
            Return _comentario
        End Get
        Set(ByVal value As String)
            _comentario = value
        End Set
    End Property
    Private _nombreArchivo As String
    Public Property NombreArchivo() As String
        Get
            Return _nombreArchivo
        End Get
        Set(ByVal value As String)
            _nombreArchivo = value
        End Set
    End Property
    Private _filesize As String
    Public Property filesize() As String
        Get
            Return _filesize
        End Get
        Set(ByVal value As String)
            _filesize = value
        End Set
    End Property
    Private _size As Long
    Public Property size() As Long
        Get
            Return _size
        End Get
        Set(ByVal value As Long)
            _size = value
        End Set
    End Property
    Private _imgUrl As String
    Public Property imgUrl() As String
        Get
            Return _imgUrl
        End Get
        Set(ByVal value As String)
            _imgUrl = value
        End Set
    End Property
    Private _path As String
    Public Property path() As String
        Get
            Return _path
        End Get
        Set(ByVal value As String)
            _path = value
        End Set
    End Property
    Private _tipoadjunto As Integer
    Public Property tipoadjunto() As Integer
        Get
            Return _tipoadjunto
        End Get
        Set(ByVal value As Integer)
            _tipoadjunto = value
        End Set
    End Property
    Private _thumbnail_url As String
    Public Property thumbnail_url() As String
        Get
            Return _thumbnail_url
        End Get
        Set(ByVal value As String)
            _thumbnail_url = value
        End Set
    End Property
End Class
