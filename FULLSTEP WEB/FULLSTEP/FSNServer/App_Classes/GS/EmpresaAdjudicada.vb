﻿Public Class EmpresaAdjudicada
    Private _Id As Long
    Public Property Id() As Long
        Get
            Return _Id
        End Get
        Set(ByVal value As Long)
            _Id = value
        End Set
    End Property
    Private _Denominacion As String
    Public Property Denominacion() As String
        Get
            Return _Denominacion
        End Get
        Set(ByVal value As String)
            _Denominacion = value
        End Set
    End Property
    Private _ProveedoresAdjudicados As New List(Of ProveedorAdjudicado)
    Public Property ProveedoresAdjudicados() As List(Of ProveedorAdjudicado)
        Get
            Return _ProveedoresAdjudicados
        End Get
        Set(ByVal value As List(Of ProveedorAdjudicado))
            _ProveedoresAdjudicados = value
        End Set
    End Property
    ''' <summary>Constructor por defecto</summary>   
    ''' <remarks></remarks>    
    Public Sub New()
    End Sub
    ''' <summary>Constructor</summary>
    ''' <param name="Id">Id empresa</param>
    ''' <param name="Denomincacion">Denominación empresa</param>    
    ''' <remarks></remarks>
    Public Sub New(ByVal Id As Long, ByVal Denomincacion As String)
        _Id = Id
        _Denominacion = Denomincacion
        _ProveedoresAdjudicados = New List(Of ProveedorAdjudicado)
    End Sub
End Class