﻿Public Class Adjudicacion

    Private _EmpresasAdjudicadas As New List(Of EmpresaAdjudicada)
    Public Property EmpresasAdjudicadas() As List(Of EmpresaAdjudicada)
        Get
            Return _EmpresasAdjudicadas
        End Get
        Set(ByVal value As List(Of EmpresaAdjudicada))
            _EmpresasAdjudicadas = value
        End Set
    End Property
    Public Sub New()

    End Sub
    ''' <summary>Constructor</summary>
    ''' <param name="dvAdjudicaciones">DataView con los datos para crear la adjudicación</param>  
    ''' <remarks></remarks>
    Public Sub New(dvAdjudicaciones As DataView)
        If Not dvAdjudicaciones Is Nothing AndAlso dvAdjudicaciones.Count > 0 Then            
            For Each oRowIterate As DataRowView In dvAdjudicaciones
                Dim oRow As DataRowView = oRowIterate
                'Buscar empresa
                Dim oEmpresaAdj As EmpresaAdjudicada = _EmpresasAdjudicadas.FirstOrDefault(Function(q) q.Id = oRow("EMPRESA"))
                If oEmpresaAdj Is Nothing Then
                    oEmpresaAdj = New EmpresaAdjudicada(oRow("EMPRESA"), String.Empty)
                    _EmpresasAdjudicadas.Add(oEmpresaAdj)
                End If

                'Buscar proveedor
                Dim oProveAdj As ProveedorAdjudicado = oEmpresaAdj.ProveedoresAdjudicados.FirstOrDefault(Function(q) q.Codigo = oRow("PROVECOD"))
                If oProveAdj Is Nothing Then
                    oProveAdj = New ProveedorAdjudicado(oRow("PROVECOD"), oRow("PROVEDEN"), oRow("PROVE_ERP"), oRow("DEN_ERP"))
                    oEmpresaAdj.ProveedoresAdjudicados.Add(oProveAdj)
                End If

                'Buscar grupo
                Dim oGrupoAdj As ProveedorAdjudicado.Grupo = oProveAdj.Grupos.FirstOrDefault(Function(q) q.Anyo = oRow("ANYO") And q.GMN1 = oRow("GMN1_PROCE") And q.Proce = oRow("PROCECOD") And q.Id = oRow("GRUPOID"))
                If oGrupoAdj Is Nothing Then
                    oGrupoAdj = New ProveedorAdjudicado.Grupo(oRow("ANYO"), oRow("GMN1_PROCE"), oRow("PROCECOD"), oRow("GRUPOID"), oRow("GRUPOCOD"), oRow("GRUPODEN"), oRow("ESCALADOS"))
                    oProveAdj.Grupos.Add(oGrupoAdj)
                End If

                'Item                
                Dim oItemAdj As New ProveedorAdjudicado.Item(DBNullToInteger(oRow("ID_ITEM")), oRow("ESTADOINT"), DBNullToStr(oRow("ART")), DBNullToStr(oRow("DESCR")), DBNullToDbl(oRow("PREC_ADJ")), _
                                                                DBNullToDbl(oRow("CANTADJ")), DBNullToStr(oRow("CENTRO")), DBNullToStr(oRow("FECINI")), DBNullToStr(oRow("FECFIN")), Trim(DBNullToStr(oRow("PROVE_ERP"))), _
                                                                DBNullToStr(oRow("DEN_ERP")), DBNullToStr(oRow("UNI")), DBNullToInteger(oRow("ID")), oGrupoAdj.TieneEscalados)
                oGrupoAdj.Items.Add(oItemAdj)
            Next
        End If
    End Sub
End Class
