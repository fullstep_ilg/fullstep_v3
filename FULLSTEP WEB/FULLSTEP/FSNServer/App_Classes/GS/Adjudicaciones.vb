﻿Imports System.Web

<Serializable()> _
Public Class Adjudicaciones
    Inherits Security

    ''' <summary>
    ''' Funcion que devuelve un dataset con los datos de las adjudicaciones de un proceso.Se devuelve las empresas,proveedores y grupos
    ''' </summary>
    ''' <param name="Anyo">Año del proceso</param>
    ''' <param name="Gmn1">Commodity del proceso</param>
    ''' <param name="CodigoProce">Codigo del proceso</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ObtenerAdjudicacionesProceso(ByVal Anyo As Short, ByVal Gmn1 As String, ByVal CodigoProce As Integer) As DataSet
        Dim oData As DataSet

        Authenticate()

        oData = DBServer.Adjudicaciones_ObtenerAdjudicacionesProceso(Anyo, Gmn1, CodigoProce)

        Return oData
    End Function
    ''' <summary>
    ''' Funcion que devuelve los proveedores adjudicados al proceso de compra
    ''' </summary>
    ''' <param name="sIdioma">Idioma</param>
    ''' <param name="IdEmp">Id de la empresa</param>
    ''' <param name="Anyo">Año del proceso</param>
    ''' <param name="Gmn1">Commodity del proceso</param>
    ''' <param name="CodigoProce">Codigo del proceso</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ObtenerProveedoresAdjudicados(ByVal sIdioma As String, ByVal IdEmp As Long, ByVal Anyo As Short, ByVal Gmn1 As String, ByVal CodigoProce As Integer) As DataSet
        Dim oData As DataSet

        Authenticate()

        oData = DBServer.Adjudicaciones_ObtenerProveedoresAdjudicados(sIdioma, IdEmp, Anyo, Gmn1, CodigoProce)

        Return oData
    End Function
    ''' <summary>Devuelve las adjudicaciones anteriores de los artículos de un proceso</summary>    
    ''' <param name="Anyo">Año del proceso</param>
    ''' <param name="Gmn1">Commodity del proceso</param>
    ''' <param name="IdEmp">Id empresa</param>
    ''' <param name="codProce">Codigo del proceso</param>
    ''' <param name="sCodProve">Codigo del proveedor</param>
    ''' <param name="iIdGrupo">Id del grupo</param>
    ''' <param name="iIdItem">Id ítem</param>
    ''' <param name="AnyoProceAdj">Año del proceso al que pertenecen las adjudicaciones que se están buscando</param>
    ''' <param name="Gmn1ProceAdj">GMN del proceso al que pertenecen las adjudicaciones que se están buscando</param>
    ''' <param name="codProceProceAdj">Cod. del proceso al que pertenecen las adjudicaciones que se están buscando</param>
    ''' <param name="sCodProveProceAdj">Cod. del proveedor al que pertenecen las adjudicaciones que se están buscando</param>
    ''' <param name="EstadoActual">Estado de integración actual de los ítems para los que se busca las adj. anteriores</param>
    ''' <returns>dataset con los datos</returns>
    ''' <remarks>Llamada desde: TraspasoAdjudicaciones.CargarAdjsAntEmpresa</remarks>
    Public Function ObtenerAdjsAnterioresProceso(ByVal Anyo As Integer, ByVal Gmn1 As String, ByVal codProce As Integer, ByVal IdEmp As Integer, Optional ByVal sCodProve As String = Nothing, Optional ByVal iIdGrupo As Integer = 0,
                                                 Optional ByVal iIdItem As Integer = 0, Optional ByVal AnyoProceAdj As Integer = 0, Optional ByVal Gmn1ProceAdj As String = Nothing, Optional ByVal codProceProceAdj As Integer = 0,
                                                 Optional ByVal sCodProveProceAdj As String = Nothing, Optional ByVal EstadoActual As Short = -2) As DataSet
        Authenticate()
        Dim dsData As DataSet = DBServer.ObtenerAdjsAnterioresProceso(Anyo, Gmn1, codProce, IdEmp, sCodProve, iIdGrupo, iIdItem, AnyoProceAdj, Gmn1ProceAdj, codProceProceAdj, sCodProveProceAdj, EstadoActual)

        'Añadir una columna para check del grid
        Dim oCol As New DataColumn("SEL", GetType(Boolean))
        oCol.AllowDBNull = False
        oCol.DefaultValue = False
        dsData.Tables(0).Columns.Add(oCol)

        Return dsData
    End Function
    ''' <summary>
    ''' Funcion que devuelve los atributos a nivel de proceso
    ''' </summary>
    ''' <param name="Anyo">Año del proceso</param>
    ''' <param name="Gmn1">Commodity del proceso</param>
    ''' <param name="CodigoProce">Codigo del proceso</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CargarAtributosProceso(ByVal Anyo As Short, ByVal Gmn1 As String, ByVal CodigoProce As Integer, ByVal idEmp As Long, ByVal CodProve As String) As List(Of ProveedorAdjudicado.Atributo)
        Dim oData As DataSet

        Authenticate()

        oData = DBServer.Adjudicaciones_CargarAtributosProceso(Anyo, Gmn1, CodigoProce, idEmp, CodProve)


        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim oAtributos As New List(Of ProveedorAdjudicado.Atributo)
        Dim oAtributo As Fullstep.FSNServer.ProveedorAdjudicado.Atributo
        Dim oValorAtrib As ProveedorAdjudicado.ValorAtrib
        Dim ind As Short = 0

        For Each oRow As DataRow In oData.Tables(0).Rows
            oAtributo = New ProveedorAdjudicado.Atributo
            oAtributo.Id = DBNullToInteger(oRow("ID"))
            oAtributo.Codigo = DBNullToStr(oRow("COD"))
            oAtributo.Denominacion = DBNullToStr(oRow("DEN"))
            oAtributo.Tipo = oRow("TIPO")
            oAtributo.Intro = DBNullToInteger(oRow("INTRO"))
            oAtributo.Obligatorio = DBNullToBoolean(oRow("OBLIGATORIO"))
            oAtributo.Validacion_ERP = DBNullToBoolean(oRow("VALIDACION_ERP"))
            If oAtributo.Intro = 1 Then
                Dim oChildRows() As DataRow = oRow.GetChildRows("RELACION_1")
                Dim oValores As New List(Of ProveedorAdjudicado.ValorAtrib)
                For Each oChildRow As DataRow In oChildRows
                    oValorAtrib = New ProveedorAdjudicado.ValorAtrib
                    Select Case oAtributo.Tipo
                        Case 1
                            oValorAtrib.Valor = DBNullToStr(oChildRow("VALOR_TEXT_LISTA"))
                            oAtributo.Valor = DBNullToStr(oRow("VALOR_TEXT"))
                        Case 2
                            oValorAtrib.Valor = DBNullToInteger(oChildRow("VALOR_NUM_LISTA"))
                            oAtributo.Valor = DBNullToStr(oRow("VALOR_NUM"))
                        Case 3
                            If oChildRow("VALOR_FEC_LISTA") Is DBNull.Value Then
                                oValorAtrib.Valor = DBNull.Value
                            Else
                                oValorAtrib.Valor = CDate(oChildRow("VALOR_FEC_LISTA"))
                            End If
                            oAtributo.Valor = DBNullToStr(oRow("VALOR_FEC"))
                    End Select
                    oValorAtrib.Orden = DBNullToDbl(oChildRow("ORDEN"))
                    oValores.Add(oValorAtrib)
                    ind += 1
                Next
                oAtributo.Valores = oValores
            Else
                Select Case oAtributo.Tipo
                    Case 1
                        oAtributo.Valor = DBNullToStr(oRow("VALOR_TEXT"))
                    Case 2
                        oAtributo.Valor = DBNullToInteger(oRow("VALOR_NUM"))
                    Case 3
                        If oRow("VALOR_FEC") Is DBNull.Value Then
                            oAtributo.Valor = DBNull.Value
                        Else
                            oAtributo.Valor = CDate(oRow("VALOR_FEC"))
                        End If
                    Case 4
                        oAtributo.Valor = DBNullToStr(oRow("VALOR_BOOL"))
                End Select
            End If
            oAtributos.Add(oAtributo)
        Next
        Return oAtributos
    End Function
    ''' <summary>
    ''' Funcion que devuelve los atributos a nivel de grupo
    ''' </summary>
    ''' <param name="Anyo">Año del proceso</param>
    ''' <param name="Gmn1">Commodity del proceso</param>
    ''' <param name="CodigoProce">Cod del proceso</param>
    ''' <param name="CodProve">Codigo del proveedor adjudicado</param>
    ''' <param name="idGrupo">id del grupo seleccionado</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CargarAtributosItem(ByVal Anyo As Short, ByVal Gmn1 As String, ByVal CodigoProce As Integer, ByVal CodProve As String, ByVal idEmp As Short, ByVal idItem As Short, ByVal idLIA As Long, ByVal idGrupo As Short) As List(Of ProveedorAdjudicado.Atributo)
        Dim oData As DataSet

        Authenticate()

        oData = DBServer.Adjudicaciones_CargarAtributosItem(Anyo, Gmn1, CodigoProce, CodProve, idEmp, idItem, idLIA, idGrupo)

        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim oAtributos As New List(Of ProveedorAdjudicado.Atributo)
        Dim oAtributo As Fullstep.FSNServer.ProveedorAdjudicado.Atributo
        Dim oValorAtrib As ProveedorAdjudicado.ValorAtrib
        Dim ind As Short = 0

        For Each oRow As DataRow In oData.Tables(0).Rows
            oAtributo = New ProveedorAdjudicado.Atributo
            oAtributo.Id = DBNullToInteger(oRow("ID"))
            oAtributo.Codigo = DBNullToStr(oRow("COD"))
            oAtributo.Denominacion = DBNullToStr(oRow("DEN"))
            oAtributo.Tipo = oRow("TIPO")
            oAtributo.Intro = DBNullToInteger(oRow("INTRO"))
            oAtributo.Obligatorio = DBNullToBoolean(oRow("OBLIGATORIO"))
            oAtributo.Validacion_ERP = DBNullToBoolean(oRow("VALIDACION_ERP"))

            If oAtributo.Intro = 1 Then
                Dim oChildRows() As DataRow = oRow.GetChildRows("RELACION_1")
                Dim oValores As New List(Of ProveedorAdjudicado.ValorAtrib)
                For Each oChildRow As DataRow In oChildRows
                    oValorAtrib = New ProveedorAdjudicado.ValorAtrib
                    Select Case oAtributo.Tipo
                        Case 1
                            oValorAtrib.Valor = DBNullToStr(oChildRow("VALOR_TEXT_LISTA"))
                            If oValorAtrib.Valor = DBNullToStr(oRow("VALOR_TEXT")) Then
                                oAtributo.Orden = DBNullToDbl(oChildRow("ORDEN"))
                            End If
                        Case 2
                            oValorAtrib.Valor = DBNullToInteger(oChildRow("VALOR_NUM_LISTA"))
                            If CStr(oValorAtrib.Valor) = DBNullToStr(oRow("VALOR_NUM")) Then
                                oAtributo.Orden = DBNullToDbl(oChildRow("ORDEN"))
                            End If
                        Case 3
                            If oChildRow("VALOR_FEC_LISTA") Is DBNull.Value Then
                                oValorAtrib.Valor = DBNull.Value
                            Else
                                oValorAtrib.Valor = CDate(oChildRow("VALOR_FEC_LISTA"))
                                If CStr(oValorAtrib.Valor) = DBNullToStr(oRow("VALOR_FEC")) Then
                                    oAtributo.Orden = DBNullToDbl(oChildRow("ORDEN"))
                                End If
                            End If
                    End Select
                    oValorAtrib.Orden = DBNullToDbl(oChildRow("ORDEN"))
                    oValores.Add(oValorAtrib)
                    ind += 1
                Next
                oAtributo.Valores = oValores
                Select Case oAtributo.Tipo
                    Case 1
                        oAtributo.Valor = DBNullToStr(oRow("VALOR_TEXT"))
                    Case 2
                        oAtributo.Valor = DBNullToInteger(oRow("VALOR_NUM"))
                    Case 3
                        If oRow("VALOR_FEC") Is DBNull.Value Then
                            oAtributo.Valor = DBNull.Value
                        Else
                            oAtributo.Valor = CDate(oRow("VALOR_FEC"))
                        End If
                    Case 4
                        oAtributo.Valor = DBNullToStr(oRow("VALOR_BOOL"))
                End Select
            Else
                Select Case oAtributo.Tipo
                    Case 1
                        oAtributo.Valor = DBNullToStr(oRow("VALOR_TEXT"))
                    Case 2
                        oAtributo.Valor = DBNullToInteger(oRow("VALOR_NUM"))
                    Case 3
                        If oRow("VALOR_FEC") Is DBNull.Value Then
                            oAtributo.Valor = DBNull.Value
                        Else
                            oAtributo.Valor = CDate(oRow("VALOR_FEC"))
                        End If
                    Case 4
                        oAtributo.Valor = DBNullToStr(oRow("VALOR_BOOL"))
                End Select
            End If
            oAtributos.Add(oAtributo)
        Next
        Return oAtributos
    End Function
    ''' <summary>
    ''' Funcion que devuelve los atributos a nivel de grupo
    ''' </summary>
    ''' <param name="Anyo">Año del proceso</param>
    ''' <param name="Gmn1">Commodity del proceso</param>
    ''' <param name="CodigoProce">Cod del proceso</param>
    ''' <param name="CodProve">Codigo del proveedor adjudicado</param>
    ''' <param name="idGrupo">id del grupo seleccionado</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CargarAtributosGrupo(ByVal Anyo As Short, ByVal Gmn1 As String, ByVal CodigoProce As Integer, ByVal CodProve As String, ByVal idGrupo As Short, ByVal idEmp As Short) As List(Of ProveedorAdjudicado.Atributo)
        Dim oData As DataSet

        Authenticate()

        oData = DBServer.Adjudicaciones_CargarAtributosGrupo(Anyo, Gmn1, CodigoProce, CodProve, idGrupo, idEmp)


        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim oAtributos As New List(Of ProveedorAdjudicado.Atributo)
        Dim oAtributo As Fullstep.FSNServer.ProveedorAdjudicado.Atributo
        Dim oValorAtrib As ProveedorAdjudicado.ValorAtrib
        Dim ind As Short = 0

        For Each oRow As DataRow In oData.Tables(0).Rows
            oAtributo = New ProveedorAdjudicado.Atributo
            oAtributo.Id = DBNullToInteger(oRow("ID"))
            oAtributo.Codigo = DBNullToStr(oRow("COD"))
            oAtributo.Denominacion = DBNullToStr(oRow("DEN"))
            oAtributo.Tipo = oRow("TIPO")
            oAtributo.Intro = DBNullToInteger(oRow("INTRO"))
            oAtributo.Obligatorio = DBNullToBoolean(oRow("OBLIGATORIO"))
            oAtributo.Validacion_ERP = DBNullToBoolean(oRow("VALIDACION_ERP"))
            If oAtributo.Intro = 1 Then
                Dim oChildRows() As DataRow = oRow.GetChildRows("RELACION_1")
                Dim oValores As New List(Of ProveedorAdjudicado.ValorAtrib)
                For Each oChildRow As DataRow In oChildRows
                    oValorAtrib = New ProveedorAdjudicado.ValorAtrib
                    Select Case oAtributo.Tipo
                        Case 1
                            oValorAtrib.Valor = DBNullToStr(oChildRow("VALOR_TEXT_LISTA"))
                        Case 2
                            oValorAtrib.Valor = DBNullToInteger(oChildRow("VALOR_NUM_LISTA"))
                        Case 3
                            If oChildRow("VALOR_FEC_LISTA") Is DBNull.Value Then
                                oValorAtrib.Valor = DBNull.Value
                            Else
                                oValorAtrib.Valor = CDate(oChildRow("VALOR_FEC_LISTA"))
                            End If
                    End Select
                    oValorAtrib.Orden = DBNullToDbl(oChildRow("ORDEN"))
                    oValores.Add(oValorAtrib)
                    ind += 1
                Next
                oAtributo.Valores = oValores
                Select Case oAtributo.Tipo
                    Case 1
                        oAtributo.Valor = DBNullToStr(oRow("VALOR_TEXT"))
                    Case 2
                        oAtributo.Valor = DBNullToInteger(oRow("VALOR_NUM"))
                    Case 3
                        If oRow("VALOR_FEC") Is DBNull.Value Then
                            oAtributo.Valor = DBNull.Value
                        Else
                            oAtributo.Valor = CDate(oRow("VALOR_FEC"))
                        End If
                    Case 4
                        oAtributo.Valor = DBNullToStr(oRow("VALOR_BOOL"))
                End Select
            Else
                Select Case oAtributo.Tipo
                    Case 1
                        oAtributo.Valor = DBNullToStr(oRow("VALOR_TEXT"))
                    Case 2
                        oAtributo.Valor = DBNullToInteger(oRow("VALOR_NUM"))
                    Case 3
                        If oRow("VALOR_FEC") Is DBNull.Value Then
                            oAtributo.Valor = DBNull.Value
                        Else
                            oAtributo.Valor = CDate(oRow("VALOR_FEC"))
                        End If
                    Case 4
                        oAtributo.Valor = DBNullToStr(oRow("VALOR_BOOL"))
                End Select
            End If
            Dim oDataAtrib As DataSet
            oDataAtrib = DBServer.Adjudicaciones_ObtenerValorAtribOferta(Anyo, Gmn1, CodigoProce, CodProve, oAtributo.Id, idGrupo, 0)
            For Each oDataAtribRow As DataRow In oDataAtrib.Tables(0).Rows
                Select Case oDataAtribRow("TIPO")
                    Case 1
                        If Not IsDBNull(oDataAtribRow("VALOR_TEXT")) Then oAtributo.Valor = oDataAtribRow("VALOR_TEXT")
                    Case 2
                        If Not IsDBNull(oDataAtribRow("VALOR_NUM")) Then oAtributo.Valor = oDataAtribRow("VALOR_NUM")
                    Case 3
                        If Not oDataAtribRow("VALOR_FEC") Is DBNull.Value Then
                            oAtributo.Valor = CDate(oDataAtribRow("VALOR_FEC"))
                        End If
                    Case 4
                        If Not IsDBNull(oDataAtribRow("VALOR_BOOL")) Then oAtributo.Valor = oDataAtribRow("VALOR_BOOL")
                End Select
            Next
            oAtributos.Add(oAtributo)
        Next
        Return oAtributos
    End Function
    Public Function ObtenerItemsGrupo(ByVal codGrupo As String, ByVal Anyo As Short, ByVal Gmn1 As String, ByVal CodProce As Long, ByVal CodProve As String, ByVal idEmp As Long, ByVal NumberFormat As System.Globalization.NumberFormatInfo, ByVal codUsu As String, ByVal DateFormat As System.Globalization.DateTimeFormatInfo) As List(Of ProveedorAdjudicado.Item)
        Dim oData As DataSet
        Dim bEncontrado As Boolean
        Dim bProvEncontrado As Boolean
        Dim bEmpEncontrada As Boolean
        Dim oGrupoReturn As ProveedorAdjudicado.Grupo = Nothing
        Authenticate()

        oData = DBServer.Adjudicaciones_ObtenerItemsGrupo(codGrupo, Anyo, Gmn1, CodProce, CodProve, idEmp, NumberFormat, DateFormat)
        Dim Contexto As System.Web.HttpContext
        Contexto = System.Web.HttpContext.Current
        Dim oAdjudicacion As FSNServer.Adjudicacion
        oAdjudicacion = Contexto.Cache("oAdjudicacion_" & codUsu)

        If Not oAdjudicacion Is Nothing Then
            For Each EmpAdjudicada As FSNServer.EmpresaAdjudicada In oAdjudicacion.EmpresasAdjudicadas
                If EmpAdjudicada.Id = idEmp Then
                    bEmpEncontrada = True
                End If
            Next
            If bEmpEncontrada = False Then
                Dim oEmp As New EmpresaAdjudicada
                oEmp.Id = idEmp
                oAdjudicacion.EmpresasAdjudicadas.Add(oEmp)
            End If
            For Each EmpAdjudicada As FSNServer.EmpresaAdjudicada In oAdjudicacion.EmpresasAdjudicadas
                If EmpAdjudicada.Id = idEmp Then
                    For Each ProvAdjudicado As FSNServer.ProveedorAdjudicado In EmpAdjudicada.ProveedoresAdjudicados
                        If ProvAdjudicado.Codigo = CodProve Then
                            bProvEncontrado = True
                        End If
                    Next
                    If bProvEncontrado = False Then
                        Dim oProve As New ProveedorAdjudicado
                        oProve.Codigo = CodProve
                        EmpAdjudicada.ProveedoresAdjudicados.Add(oProve)
                    End If
                End If
            Next
        End If
        If Not oAdjudicacion Is Nothing Then
            For Each EmpAdjudicada As FSNServer.EmpresaAdjudicada In oAdjudicacion.EmpresasAdjudicadas
                If EmpAdjudicada.Id = idEmp Then
                    For Each ProvAdjudicado As FSNServer.ProveedorAdjudicado In EmpAdjudicada.ProveedoresAdjudicados
                        If ProvAdjudicado.Codigo = CodProve Then
                            For Each oGrupo As ProveedorAdjudicado.Grupo In ProvAdjudicado.Grupos
                                If oGrupo.Codigo = codGrupo And oGrupo.Visto = 1 Then 'Si ya se ha entrado en el grupo no se cargaran los items y atributos
                                    bEncontrado = True
                                    Exit For
                                ElseIf oGrupo.Codigo = codGrupo And oGrupo.Visto = 0 Then 'Si se ha creado el grupo desde la parte de cliente(al traspasar desde empresa o proveedor) sin haber entrado en el grupo se borra para que cargue los items y atributos
                                    ProvAdjudicado.Grupos.Remove(oGrupo)
                                End If
                            Next
                            If bEncontrado = False Then
                                Dim oGrupo As New ProveedorAdjudicado.Grupo
                                oGrupo.Codigo = codGrupo
                                Dim oItem As New ProveedorAdjudicado.Item
                                If oData.Tables.Count = 4 Then
                                    'Si tiene escalados
                                    For Each oRow As DataRow In oData.Tables(0).Rows
                                        oItem = New ProveedorAdjudicado.Item
                                        Dim oAtribRows() As DataRow = oRow.GetChildRows("RELACION_1")
                                        Dim oAtributo As ProveedorAdjudicado.Atributo
                                        For Each oAtribRow As DataRow In oAtribRows
                                            oAtributo = New ProveedorAdjudicado.Atributo
                                            oAtributo.Id = oAtribRow("ATRIBID")
                                            oAtributo.Codigo = oAtribRow("ATRIBCOD")
                                            oAtributo.Denominacion = oAtribRow("ATRIBDEN")
                                            oAtributo.Tipo = oAtribRow("ATRIBTIPO")
                                            oAtributo.Obligatorio = oAtribRow("ATRIBOBLIGATORIO")
                                            Select Case oAtribRow("ATRIBTIPO")
                                                Case 1
                                                    oAtributo.Valor = oAtribRow("ATRIBVALOR_TEXT")
                                                Case 2
                                                    oAtributo.Valor = oAtribRow("ATRIBVALOR_NUM")
                                                Case 3
                                                    If oAtribRow("ATRIBVALOR_FEC") Is DBNull.Value Then
                                                        oAtributo.Valor = DBNull.Value
                                                    Else
                                                        oAtributo.Valor = CDate(oAtribRow("ATRIBVALOR_FEC"))
                                                    End If
                                                Case 4
                                                    oAtributo.Valor = oAtribRow("ATRIBVALOR_BOOL")
                                            End Select
                                            oItem.Atributos.Add(oAtributo)
                                        Next
                                        oItem.Id = DBNullToInteger(oRow("ID_ITEM"))
                                        If oRow("ESTADOINT") Is DBNull.Value Then
                                            oItem.Estado = -1
                                        Else
                                            oItem.Estado = DBNullToInteger(oRow("ESTADOINT"))
                                        End If

                                        oItem.Codigo = DBNullToStr(oRow("CODART"))
                                        oItem.Denominacion = DBNullToStr(oRow("DENART"))

                                        Dim oEscalado As ProveedorAdjudicado.Escalado
                                        Dim oEscRows() As DataRow = oRow.GetChildRows("RELACION_2")
                                        For Each oEscRow As DataRow In oEscRows
                                            oEscalado = New ProveedorAdjudicado.Escalado
                                            oEscalado.Id = oEscRow("ESC_ID")
                                            oEscalado.Inicio = DBNullToDbl(oEscRow("INICIO"))
                                            oEscalado.Fin = DBNullToDbl(oEscRow("FIN"))
                                            oEscalado.Precio = FSNLibrary.FormatNumber(DBNullToDbl(oEscRow("PREC_ADJ")), NumberFormat)
                                            oEscalado.Rango = IIf(oEscRow("FIN") Is DBNull.Value, 0, 1)
                                            oEscalado.Unidad = DBNullToStr(oEscRow("ESCALAUNI"))
                                            oEscalado.Moneda = DBNullToStr(oEscRow("MON"))
                                            oItem.Escalados.Add(oEscalado)
                                        Next

                                        oItem.Centro = DBNullToStr(oRow("CENTRO"))
                                        oItem.FecIniSum = DBNullToStr(oRow("FECINI_SUM"))
                                        oItem.FecFinSum = DBNullToStr(oRow("FECFIN_SUM"))
                                        oItem.CodERP = Trim(DBNullToStr(oRow("PROVE_ERP")))
                                        oItem.DenERP = DBNullToStr(oRow("DEN_ERP"))
                                        oItem.Unidad = DBNullToStr(oRow("UNI"))
                                        oItem.LIAId = DBNullToInteger(oRow("LIA_ID"))
                                        oItem.TieneEscalados = 1

                                        oGrupo.TieneEscalados = 1
                                        oGrupo.Items.Add(oItem)
                                    Next
                                Else
                                    For Each oRow As DataRow In oData.Tables(0).Rows
                                        oItem = New ProveedorAdjudicado.Item
                                        Dim oAtribRows() As DataRow = oRow.GetChildRows("RELACION_1")
                                        Dim oAtributo As ProveedorAdjudicado.Atributo
                                        For Each oAtribRow As DataRow In oAtribRows
                                            oAtributo = New ProveedorAdjudicado.Atributo
                                            oAtributo.Id = oAtribRow("ATRIBID")
                                            oAtributo.Codigo = oAtribRow("ATRIBCOD")
                                            oAtributo.Denominacion = oAtribRow("ATRIBDEN")
                                            oAtributo.Tipo = oAtribRow("ATRIBTIPO")
                                            oAtributo.Obligatorio = oAtribRow("ATRIBOBLIGATORIO")
                                            Select Case oAtribRow("ATRIBTIPO")
                                                Case 1
                                                    oAtributo.Valor = oAtribRow("ATRIBVALOR_TEXT")
                                                Case 2
                                                    oAtributo.Valor = oAtribRow("ATRIBVALOR_NUM")
                                                Case 3
                                                    If oAtribRow("ATRIBVALOR_FEC") Is DBNull.Value Then
                                                        oAtributo.Valor = DBNull.Value
                                                    Else
                                                        oAtributo.Valor = CDate(oAtribRow("ATRIBVALOR_FEC"))
                                                    End If
                                                Case 4
                                                    oAtributo.Valor = oAtribRow("ATRIBVALOR_BOOL")
                                            End Select
                                            oItem.Atributos.Add(oAtributo)
                                        Next

                                        oItem.Id = DBNullToInteger(oRow("ID_ITEM"))
                                        If oRow("ESTADOINT") Is DBNull.Value Then
                                            oItem.Estado = -1
                                        Else
                                            oItem.Estado = DBNullToInteger(oRow("ESTADOINT"))
                                        End If
                                        oItem.Codigo = DBNullToStr(oRow("CODART"))
                                        oItem.Denominacion = DBNullToStr(oRow("DENART"))
                                        oItem.PrecUni = DBNullToDbl(oRow("PREC_ADJ"))
                                        oItem.CantAdj = DBNullToDbl(oRow("CANT_ADJ"))
                                        oItem.Centro = DBNullToStr(oRow("CENTRO"))
                                        oItem.FecIniSum = DBNullToStr(oRow("FECINI_SUM"))
                                        oItem.FecFinSum = DBNullToStr(oRow("FECFIN_SUM"))
                                        oItem.CodERP = Trim(DBNullToStr(oRow("PROVE_ERP")))
                                        oItem.DenERP = DBNullToStr(oRow("DEN_ERP"))
                                        oItem.Unidad = DBNullToStr(oRow("UNI"))
                                        oItem.LIAId = DBNullToInteger(oRow("LIA_ID"))
                                        oItem.TieneEscalados = 0
                                        oGrupo.TieneEscalados = 0
                                        oGrupo.Items.Add(oItem)
                                    Next
                                End If
                                oGrupoReturn = oGrupo
                                ProvAdjudicado.Grupos.Add(oGrupo)
                            End If
                            Exit For
                        End If
                    Next
                    Exit For
                End If
            Next
        End If
        Return oGrupoReturn.Items
    End Function
    ''' <summary>
    ''' Devuelve los items del grupo seleccionado
    ''' </summary>
    ''' <param name="codGrupo">codigo del grupo seleccionado</param>
    ''' <param name="Anyo">Año del proceso</param>
    ''' <param name="Gmn1">Commodity del proceso</param>
    ''' <param name="CodProce">Codigo del proceso</param>
    ''' <param name="CodProve">Codigo del proveedor seleccionado</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ObtenerDsItemsGrupo(ByVal codGrupo As String, ByVal Anyo As Short, ByVal Gmn1 As String, ByVal CodProce As Long, ByVal CodProve As String, ByVal idEmp As Long, ByVal NumberFormat As System.Globalization.NumberFormatInfo, ByVal codUsu As String, ByVal DateFormat As System.Globalization.DateTimeFormatInfo, Optional ByVal cambio As Boolean = False) As DataSet
        Dim oData As DataSet
        Dim bEncontrado As Boolean
        Dim bProvEncontrado As Boolean
        Dim bEmpEncontrada As Boolean
        Dim oGrupoABorrar As ProveedorAdjudicado.Grupo = Nothing
        Dim oProvAdjudicado As ProveedorAdjudicado = Nothing
        Authenticate()

        oData = DBServer.Adjudicaciones_ObtenerItemsGrupo(codGrupo, Anyo, Gmn1, CodProce, CodProve, idEmp, NumberFormat, DateFormat)
        Dim Contexto As System.Web.HttpContext
        Contexto = System.Web.HttpContext.Current
        Dim oAdjudicacion As FSNServer.Adjudicacion
        oAdjudicacion = Contexto.Cache("oAdjudicacion_" & codUsu)

        If Not oAdjudicacion Is Nothing Then
            For Each EmpAdjudicada As FSNServer.EmpresaAdjudicada In oAdjudicacion.EmpresasAdjudicadas
                If EmpAdjudicada.Id = idEmp Then
                    bEmpEncontrada = True
                End If
            Next
            If bEmpEncontrada = False Then
                Dim oEmp As New EmpresaAdjudicada
                oEmp.Id = idEmp
                oAdjudicacion.EmpresasAdjudicadas.Add(oEmp)
            End If
            For Each EmpAdjudicada As FSNServer.EmpresaAdjudicada In oAdjudicacion.EmpresasAdjudicadas
                If EmpAdjudicada.Id = idEmp Then
                    For Each ProvAdjudicado As FSNServer.ProveedorAdjudicado In EmpAdjudicada.ProveedoresAdjudicados
                        If ProvAdjudicado.Codigo = CodProve Then
                            bProvEncontrado = True
                        End If
                    Next
                    If bProvEncontrado = False Then
                        Dim oProve As New ProveedorAdjudicado
                        oProve.Codigo = CodProve
                        EmpAdjudicada.ProveedoresAdjudicados.Add(oProve)
                    End If
                End If
            Next
        End If
        If Not oAdjudicacion Is Nothing Then
            Dim EmpAdjudicada As FSNServer.EmpresaAdjudicada = oAdjudicacion.EmpresasAdjudicadas.Find(Function(o As FSNServer.EmpresaAdjudicada) (o.Id = idEmp))
            If EmpAdjudicada IsNot Nothing Then
                Dim ProvAdjudicado As FSNServer.ProveedorAdjudicado = EmpAdjudicada.ProveedoresAdjudicados.Find(Function(o As FSNServer.ProveedorAdjudicado) (o.Codigo = CodProve))
                If ProvAdjudicado IsNot Nothing Then
                    Dim oGrupoBuscar As ProveedorAdjudicado.Grupo
                    For Each oGrupoBuscar In ProvAdjudicado.Grupos
                        If oGrupoBuscar.Codigo = codGrupo And (oGrupoBuscar.Visto = 1 Or cambio) Then 'Si ya se ha entrado en el grupo no se cargaran los items y atributos
                            bEncontrado = True
                            Exit For
                        ElseIf oGrupoBuscar.Codigo = codGrupo And (oGrupoBuscar.Visto = 0) Then 'Si se ha creado el grupo desde la parte de cliente(al traspasar desde empresa o proveedor) sin haber entrado en el grupo se guarda para borrarlo posteriormente y para que cargue los items y atributos
                            oGrupoABorrar = oGrupoBuscar
                            oProvAdjudicado = ProvAdjudicado
                        End If
                    Next
                    If Not bEncontrado Then
                        Dim oGrupo As New ProveedorAdjudicado.Grupo
                        oGrupo.Codigo = codGrupo

                        If oData.Tables.Count = 4 Then
                            'Si tiene escalados
                            For Each oRow As DataRow In oData.Tables(0).Rows
                                AgregarItemEscaladoAGrupo(oGrupo, oRow, Anyo, Gmn1, CodProce, CodProve, oData, NumberFormat)
                            Next
                        Else
                            For Each oRow As DataRow In oData.Tables(0).Rows
                                AgregarItemAGrupo(oGrupo, oRow, Anyo, Gmn1, CodProce, CodProve, oData)
                            Next
                        End If

                        ProvAdjudicado.Grupos.Add(oGrupo)
                    End If
                End If
            End If
        End If

        If Not oGrupoABorrar Is Nothing Then
            oProvAdjudicado.Grupos.Remove(oGrupoABorrar)
        End If

        Contexto.Cache.Insert("oAdjudicacion_" & codUsu, oAdjudicacion, Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration, New TimeSpan(0, System.Configuration.ConfigurationManager.AppSettings("TiempoExpiracionCacheMin_2"), 0))

        Return (oData)
    End Function
    Public Sub AgregarItemAGrupo(ByRef oGrupo As ProveedorAdjudicado.Grupo, ByRef oRow As DataRow, ByVal Anyo As Short, ByVal Gmn1 As String, ByVal CodProce As Long, ByVal CodProve As String, ByRef oData As DataSet,
                                 Optional ByVal bMirarValoresAtribOferta As Boolean = True, Optional ByVal bObtenerValorAtribsOrigen As Boolean = True)
        Dim oItem As New ProveedorAdjudicado.Item
        Dim oAtribRows() As DataRow = oRow.GetChildRows("RELACION_1")
        Dim oAtributo As ProveedorAdjudicado.Atributo
        For Each oAtribRow As DataRow In oAtribRows
            oAtributo = New ProveedorAdjudicado.Atributo
            oAtributo.Id = oAtribRow("ATRIBID")
            oAtributo.Codigo = oAtribRow("ATRIBCOD")
            oAtributo.Denominacion = oAtribRow("ATRIBDEN")
            oAtributo.Tipo = oAtribRow("ATRIBTIPO")
            oAtributo.Intro = oAtribRow("ATRIBINTRO")
            oAtributo.Obligatorio = oAtribRow("ATRIBOBLIGATORIO")
            If bObtenerValorAtribsOrigen Then
                Select Case oAtribRow("ATRIBTIPO")
                    Case 1
                        oAtributo.Valor = oAtribRow("ATRIBVALOR_TEXT")
                    Case 2
                        oAtributo.Valor = oAtribRow("ATRIBVALOR_NUM")
                    Case 3
                        If oAtribRow("ATRIBVALOR_FEC") Is DBNull.Value Then
                            oAtributo.Valor = DBNull.Value
                        Else
                            oAtributo.Valor = CDate(oAtribRow("ATRIBVALOR_FEC"))
                        End If
                    Case 4
                        oAtributo.Valor = oAtribRow("ATRIBVALOR_BOOL")
                End Select
            Else
                oAtributo.Valor = oRow("ATRIB_" & oAtributo.Tipo & IIf(oAtributo.Obligatorio, "_1_", "_0_") & oAtributo.Intro & "_" & oAtributo.Codigo)
            End If
            'Miramos si el atributo tiene valor en la oferta o especificaciones 
            If bMirarValoresAtribOferta Then
                Dim oDataAtrib As DataSet
                oDataAtrib = DBServer.Adjudicaciones_ObtenerValorAtribOferta(Anyo, Gmn1, CodProce, CodProve, oAtributo.Id, oGrupo.Id, oRow("ID_ITEM"))
                For Each oDataAtribRow As DataRow In oDataAtrib.Tables(0).Rows
                    Select Case oDataAtribRow("TIPO")
                        Case 1
                            If Not IsDBNull(oDataAtribRow("VALOR_TEXT")) Then oAtributo.Valor = oDataAtribRow("VALOR_TEXT")
                        Case 2
                            If Not IsDBNull(oDataAtribRow("VALOR_NUM")) Then oAtributo.Valor = oDataAtribRow("VALOR_NUM")
                        Case 3
                            If Not oDataAtribRow("VALOR_FEC") Is DBNull.Value Then
                                oAtributo.Valor = CDate(oDataAtribRow("VALOR_FEC"))
                            End If
                        Case 4
                            If Not IsDBNull(oDataAtribRow("VALOR_BOOL")) Then oAtributo.Valor = oDataAtribRow("VALOR_BOOL")
                    End Select
                    For j As Integer = 0 To oData.Tables(0).Rows.Count - 1
                        If oData.Tables(0).Rows(j)("ID_ITEM") = oRow("ID_ITEM") Then
                            oData.Tables(0).Rows(j)("ATRIB_" & oAtributo.Tipo & IIf(oAtributo.Obligatorio, "_1_", "_0_") & oAtributo.Intro & "_" & oAtributo.Codigo) = oAtributo.Valor
                            Exit For
                        End If
                    Next
                Next
            End If
            oItem.Atributos.Add(oAtributo)
        Next

        oItem.Id = DBNullToInteger(oRow("ID_ITEM"))
        If oRow("ESTADOINT") Is DBNull.Value Then
            oItem.Estado = -1
        Else
            oItem.Estado = DBNullToInteger(oRow("ESTADOINT"))
        End If
        oItem.Codigo = DBNullToStr(oRow("CODART"))
        oItem.Denominacion = DBNullToStr(oRow("DENART"))
        oItem.PrecUni = DBNullToDbl(oRow("PREC_ADJ"))
        oItem.CantAdj = DBNullToDbl(oRow("CANT_ADJ"))
        oItem.Centro = DBNullToStr(oRow("CENTRO"))
        oItem.FecIniSum = DBNullToStr(oRow("FECINI_SUM"))
        oItem.FecFinSum = DBNullToStr(oRow("FECFIN_SUM"))
        oItem.CodERP = Trim(DBNullToStr(oRow("PROVE_ERP")))
        oItem.DenERP = DBNullToStr(oRow("DEN_ERP"))
        oItem.Unidad = DBNullToStr(oRow("UNI"))
        oItem.LIAId = DBNullToInteger(oRow("LIA_ID"))
        oItem.TieneEscalados = 0
        oGrupo.TieneEscalados = 0
        oGrupo.Items.Add(oItem)
    End Sub
    Public Sub AgregarItemEscaladoAGrupo(ByRef oGrupo As ProveedorAdjudicado.Grupo, ByRef oRow As DataRow, ByVal Anyo As Short, ByVal Gmn1 As String, ByVal CodProce As Long, ByVal CodProve As String, ByRef oData As DataSet,
                                          ByVal NumberFormat As System.Globalization.NumberFormatInfo, Optional ByVal bMirarValoresAtribOferta As Boolean = True, Optional ByVal bObtenerValorAtribsOrigen As Boolean = True)
        Dim oItem As New ProveedorAdjudicado.Item
        Dim oAtribRows() As DataRow = oRow.GetChildRows("RELACION_1")
        Dim oAtributo As ProveedorAdjudicado.Atributo
        For Each oAtribRow As DataRow In oAtribRows
            oAtributo = New ProveedorAdjudicado.Atributo
            oAtributo.Id = oAtribRow("ATRIBID")
            oAtributo.Codigo = oAtribRow("ATRIBCOD")
            oAtributo.Denominacion = oAtribRow("ATRIBDEN")
            oAtributo.Tipo = oAtribRow("ATRIBTIPO")
            oAtributo.Intro = oAtribRow("ATRIBINTRO")
            oAtributo.Obligatorio = oAtribRow("ATRIBOBLIGATORIO")
            If bObtenerValorAtribsOrigen Then
                Select Case oAtribRow("ATRIBTIPO")
                    Case 1
                        oAtributo.Valor = oAtribRow("ATRIBVALOR_TEXT")
                    Case 2
                        oAtributo.Valor = oAtribRow("ATRIBVALOR_NUM")
                    Case 3
                        If oAtribRow("ATRIBVALOR_FEC") Is DBNull.Value Then
                            oAtributo.Valor = DBNull.Value
                        Else
                            oAtributo.Valor = CDate(oAtribRow("ATRIBVALOR_FEC"))
                        End If
                    Case 4
                        oAtributo.Valor = oAtribRow("ATRIBVALOR_BOOL")
                End Select
            Else
                oAtributo.Valor = oRow("ATRIB_" & oAtributo.Tipo & IIf(oAtributo.Obligatorio, "_1_", "_0_") & oAtributo.Intro & "_" & oAtributo.Codigo)
            End If
            'Miramos si el atributo tiene valor en la oferta o especificaciones 
            If bMirarValoresAtribOferta Then
                Dim oDataAtrib As DataSet
                oDataAtrib = DBServer.Adjudicaciones_ObtenerValorAtribOferta(Anyo, Gmn1, CodProce, CodProve, oAtributo.Id, oGrupo.Id, oRow("ID_ITEM"))
                For Each oDataAtribRow As DataRow In oDataAtrib.Tables(0).Rows
                    Select Case oDataAtribRow("TIPO")
                        Case 1
                            If Not IsDBNull(oDataAtribRow("VALOR_TEXT")) Then oAtributo.Valor = oDataAtribRow("VALOR_TEXT")
                        Case 2
                            If Not IsDBNull(oDataAtribRow("VALOR_NUM")) Then oAtributo.Valor = oDataAtribRow("VALOR_NUM")
                        Case 3
                            If Not oDataAtribRow("VALOR_FEC") Is DBNull.Value Then
                                oAtributo.Valor = CDate(oDataAtribRow("VALOR_FEC"))
                            End If
                        Case 4
                            If Not IsDBNull(oDataAtribRow("VALOR_BOOL")) Then oAtributo.Valor = oDataAtribRow("VALOR_BOOL")
                    End Select
                    For j As Integer = 0 To oData.Tables(0).Rows.Count - 1
                        If oData.Tables(0).Rows(j)("ID_ITEM") = oRow("ID_ITEM") Then
                            oData.Tables(0).Rows(j)("ATRIB_" & oAtributo.Tipo & IIf(oAtributo.Obligatorio, "_1_", "_0_") & oAtributo.Intro & "_" & oAtributo.Codigo) = oAtributo.Valor
                            Exit For
                        End If
                    Next
                Next
            End If

            oItem.Atributos.Add(oAtributo)
        Next
        oItem.Id = DBNullToInteger(oRow("ID_ITEM"))
        If oRow("ESTADOINT") Is DBNull.Value Then
            oItem.Estado = -1
        Else
            oItem.Estado = DBNullToInteger(oRow("ESTADOINT"))
        End If

        oItem.Codigo = DBNullToStr(oRow("CODART"))
        oItem.Denominacion = DBNullToStr(oRow("DENART"))

        Dim oEscalado As ProveedorAdjudicado.Escalado
        Dim oEscRows() As DataRow = oRow.GetChildRows("RELACION_2")
        For Each oEscRow As DataRow In oEscRows
            oEscalado = New ProveedorAdjudicado.Escalado
            oEscalado.Id = oEscRow("ESC_ID")
            oEscalado.Inicio = DBNullToDbl(oEscRow("INICIO"))
            oEscalado.Fin = DBNullToDbl(oEscRow("FIN"))
            oEscalado.Precio = FSNLibrary.FormatNumber(DBNullToDbl(oEscRow("PREC_ADJ")), NumberFormat)
            oEscalado.Rango = IIf(oEscRow("FIN") Is DBNull.Value, 0, 1)
            oEscalado.Unidad = DBNullToStr(oEscRow("ESCALAUNI"))
            oEscalado.Moneda = DBNullToStr(oEscRow("MON"))
            oItem.Escalados.Add(oEscalado)
        Next

        oItem.Centro = DBNullToStr(oRow("CENTRO"))
        oItem.FecIniSum = DBNullToStr(oRow("FECINI_SUM"))
        oItem.FecFinSum = DBNullToStr(oRow("FECFIN_SUM"))
        oItem.CodERP = Trim(DBNullToStr(oRow("PROVE_ERP")))
        oItem.DenERP = DBNullToStr(oRow("DEN_ERP"))
        oItem.Unidad = DBNullToStr(oRow("UNI"))
        oItem.LIAId = DBNullToInteger(oRow("LIA_ID"))
        oItem.TieneEscalados = 1

        oGrupo.TieneEscalados = 1
        oGrupo.Items.Add(oItem)
    End Sub
    ''' <summary>
    ''' Funcion que devuelve los datos del proveedor adjudicado
    ''' </summary>
    ''' <param name="idEmp">id de la empresa</param>
    ''' <param name="CodProve">Codigo del proveedor</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ObtenerDatosProveedorAdjudicado(ByVal Anyo As Short, ByVal sGmn1 As String, ByVal codProce As Long, ByVal idEmp As Long, ByVal CodProve As String, ByVal sIdi As String) As ProveedorAdjudicado
        Dim oData As DataSet

        Authenticate()

        oData = DBServer.Adjudicaciones_ObtenerDatosProveedorAdjudicado(Anyo, sGmn1, codProce, idEmp, CodProve, sIdi)

        Dim oProveedorAdjudicado As New ProveedorAdjudicado
        Dim oCodigoERP As FSNServer.ProveedorAdjudicado.CodigoERP

        oProveedorAdjudicado.Codigo = DBNullToStr(oData.Tables(0)(0)("PROVECOD"))
        oProveedorAdjudicado.Denominacion = DBNullToStr(oData.Tables(0)(0)("PROVEDEN"))
        oProveedorAdjudicado.EstadoIntegracion = IIf(DBNullToStr(oData.Tables(0)(0)("EST_INTEGRACION")) = "", -1, DBNullToInteger(oData.Tables(0)(0)("EST_INTEGRACION")))
        For Each drProv As DataRow In oData.Tables(0).Rows
            oCodigoERP = New ProveedorAdjudicado.CodigoERP
            oCodigoERP.CodigoERP = Trim(DBNullToStr(drProv("COD_ERP")))
            oCodigoERP.DenERP = DBNullToStr(drProv("DEN_ERP"))
            For i = 7 To drProv.Table.Columns.Count - 1
                Select Case i
                    Case 7
                        oCodigoERP.DenCampoPer1 = DBNullToStr(drProv.Table.Columns(i).ColumnName)
                        oCodigoERP.DatoCampoPer1 = drProv(i).ToString
                        oCodigoERP.NumCamposPersonalizadosAct = 1
                    Case 8
                        oCodigoERP.DenCampoPer2 = DBNullToStr(drProv.Table.Columns(i).ColumnName)
                        oCodigoERP.DatoCampoPer2 = drProv(i).ToString
                        oCodigoERP.NumCamposPersonalizadosAct = 2
                    Case 9
                        oCodigoERP.DenCampoPer3 = DBNullToStr(drProv.Table.Columns(i).ColumnName)
                        oCodigoERP.DatoCampoPer3 = drProv(i).ToString
                        oCodigoERP.NumCamposPersonalizadosAct = 3
                    Case 10
                        oCodigoERP.DenCampoPer4 = DBNullToStr(drProv.Table.Columns(i).ColumnName)
                        oCodigoERP.DatoCampoPer4 = drProv(i).ToString
                        oCodigoERP.NumCamposPersonalizadosAct = 4
                End Select
            Next
            If DBNullToStr(drProv("COD_ERP")) <> String.Empty Then
                oProveedorAdjudicado.CodigosERP.Add(oCodigoERP)
            End If
        Next

        Return oProveedorAdjudicado
    End Function
    Public Function ObtenerCentrosItem(ByVal Uon1 As String, ByVal Uon2 As String, ByVal Uon3 As String, ByVal codArt As String) As DataSet
        Authenticate()

        Dim oData As DataSet
        oData = DBServer.Adjudicaciones_ObtenerCentrosItem(Uon1, Uon2, Uon3, codArt)

        Return oData
    End Function
    ''' <summary>
    ''' Devuelve los distribuidores del proveedor
    ''' </summary>
    ''' <param name="CodProve">Codigo del proveedor</param>
    ''' <param name="idEmp">id de la empresa</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ObtenerDistribuidoresProveedor(ByVal CodProve As String, ByVal idEmp As Long) As List(Of ProveedorAdjudicado.Distribuidor)
        Authenticate()

        Dim oData As DataSet
        oData = DBServer.Adjudicaciones_ObtenerDistribuidoresProveedor(CodProve, idEmp)

        Dim oDistribuidores As New List(Of ProveedorAdjudicado.Distribuidor)
        Dim oDistribuidor As ProveedorAdjudicado.Distribuidor
        Dim oCodigoERP As FSNServer.ProveedorAdjudicado.CodigoERP
        Dim ind As Short = 0
        For Each drProv As DataRow In oData.Tables(0).Rows
            oDistribuidor = New ProveedorAdjudicado.Distribuidor
            oDistribuidor.Id = ind
            oDistribuidor.Codigo = DBNullToStr(drProv("COD"))
            oDistribuidor.Denominacion = DBNullToStr(drProv("DEN"))

            Dim CodERPsChilRows() As DataRow = drProv.GetChildRows("RELACION_1")

            For Each drCodERP As DataRow In CodERPsChilRows
                oCodigoERP = New ProveedorAdjudicado.CodigoERP
                oCodigoERP.CodigoERP = Trim(DBNullToStr(drCodERP("COD_ERP")))
                oCodigoERP.DenERP = Trim(DBNullToStr(drCodERP("DEN_ERP")))
                oDistribuidor.CodigosERP.Add(oCodigoERP)
            Next
            oDistribuidores.Add(oDistribuidor)
            ind += 1
        Next

        Return oDistribuidores
    End Function
    ''' <summary>
    ''' Devuelve los distribuidores del proveedor
    ''' </summary>
    ''' <param name="CodProve">Codigo del proveedor</param>
    ''' <param name="idEmp">id de la empresa</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ObtenerDistribuidoresProveedorItem(ByVal CodProve As String, ByVal idEmp As Long, ByVal Anyo As Long, ByVal sGmn1 As String, ByVal codProce As Long, ByVal idItem As Long, ByVal sUon1 As String, ByVal sUon2 As String, ByVal sUon3 As String) As List(Of ProveedorAdjudicado.Distribuidor)
        Dim oData As DataSet

        Authenticate()

        oData = DBServer.Adjudicaciones_ObtenerDistribuidoresProveedorItem(CodProve, idEmp, Anyo, sGmn1, codProce, idItem, sUon1, sUon2, sUon3)
        Dim bTraspasado As Boolean
        Dim oDistribuidores As New List(Of ProveedorAdjudicado.Distribuidor)
        Dim oDistribuidor As ProveedorAdjudicado.Distribuidor
        Dim oCodigoERP As FSNServer.ProveedorAdjudicado.CodigoERP
        Dim ind As Short = 0
        'Comprobar que si sólo es uno (él mismo) no añada oDistribuidores
        If Not (oData.Tables.Count > 0 AndAlso oData.Tables(0).Rows.Count = 1 AndAlso oData.Tables(0).Rows(0).Item("COD") = CodProve) Then
            For Each drProv As DataRow In oData.Tables(0).Rows
                oDistribuidor = New ProveedorAdjudicado.Distribuidor
                oDistribuidor.Id = ind
                oDistribuidor.Codigo = Trim(DBNullToStr(drProv("COD")))
                oDistribuidor.Denominacion = DBNullToStr(drProv("DEN"))
                oDistribuidor.PorcentajeDistribuido = DBNullToDbl(drProv("PORCEN"))
                oDistribuidor.Cantidad_Adj = DBNullToDbl(drProv("IA_CANT_ADJ"))
                Dim CodERPsChilRows() As DataRow = drProv.GetChildRows("RELACION_1")

                For Each drCodERP As DataRow In CodERPsChilRows
                    oCodigoERP = New ProveedorAdjudicado.CodigoERP
                    oCodigoERP.CodigoERP = Trim(DBNullToStr(drCodERP("COD_ERP")))
                    oCodigoERP.DenERP = Trim(DBNullToStr(drCodERP("DEN_ERP")))
                    oCodigoERP.Traspasado = IIf(drCodERP("PROVE_ERP_SEL") Is DBNull.Value, False, True)
                    If oCodigoERP.Traspasado Then
                        bTraspasado = True
                        oDistribuidor.CodErpSeleccionado = oCodigoERP.DenERP
                        oDistribuidor.Cantidad_Adj = DBNullToDbl(drProv("LIA_CANT_ADJ"))
                    Else

                    End If
                    oDistribuidor.CodigosERP.Add(oCodigoERP)
                Next
                oDistribuidores.Add(oDistribuidor)
                ind += 1
            Next
        End If

        If bTraspasado Then 'Si el item ha sido traspasado se borrara la cantidad de los distribuidores a los que no se haya distribuido
            For Each Distribuidor In oDistribuidores
                bTraspasado = False
                For Each CodERP In Distribuidor.CodigosERP
                    If CodERP.Traspasado Then
                        bTraspasado = True
                    End If
                Next
                If bTraspasado = False Then
                    Distribuidor.Cantidad_Adj = 0
                End If
            Next
        End If

        Return oDistribuidores
    End Function
    ''' <summary>
    ''' Traspasa un item al erp, comprobando previamente si el item está integrado en el erp
    ''' </summary>
    ''' <param name="oAdjudicacion"></param>
    ''' <param name="Anyo"></param>
    ''' <param name="Gmn1"></param>
    ''' <param name="codProce"></param>
    ''' <param name="idEmp"></param>
    ''' <param name="codProve"></param>
    ''' <param name="codGrupo"></param>
    ''' <param name="idItem"></param>
    ''' <param name="hayEscalados"></param>
    ''' <param name="codUsu"></param>
    ''' <param name="oAdjsEliminar">Adjudicaciones a eliminar</param>
    ''' <returns>1 traspaso correcto,2 el item no está integrado error en la integración </returns>
    ''' <remarks></remarks>
    Public Function TraspasarItemToERP(ByVal oAdjudicacion As Adjudicacion, ByVal Anyo As Short, ByVal Gmn1 As String, ByVal codProce As Integer, ByVal idEmp As Short, ByVal codProve As String, ByVal codGrupo As String, ByVal idItem As Short,
                                       ByVal hayEscalados As Byte, ByVal codUsu As String, ByVal centro As String, ByVal oAdjsEliminar As Fullstep.FSNServer.Adjudicacion, ByVal bMantEstIntEnTraspasoAdj As Boolean) As String
        Dim Cantidad_Adj As Double
        Dim Cod_ERP As String = String.Empty
        Dim sRetorno As String
        Dim dtGrupo As DataTable = NuevaTablaTraspasarGrupoAERP()
        Dim dtEscalados As New DataTable
        Dim gItem As ProveedorAdjudicado.Item = Nothing
        Dim Contexto As System.Web.HttpContext
        Dim dtGrupoBorrar As DataTable

        Contexto = System.Web.HttpContext.Current

        'Borrar adjudicaciones anteriores
        If Not oAdjsEliminar Is Nothing Then
            dtGrupoBorrar = NuevaTablaTraspasarGrupoAERP()
            For Each EmpAdjudicada As FSNServer.EmpresaAdjudicada In oAdjsEliminar.EmpresasAdjudicadas
                For Each ProvAdjudicado As FSNServer.ProveedorAdjudicado In EmpAdjudicada.ProveedoresAdjudicados
                    For Each oGrupo As ProveedorAdjudicado.Grupo In ProvAdjudicado.Grupos
                        Dim drItem As DataRow
                        For Each oItem As ProveedorAdjudicado.Item In oGrupo.Items
                            If oGrupo.CodErpSeleccionado <> "" Then
                                drItem = dtGrupoBorrar.NewRow
                                drItem("ANYO") = Anyo
                                drItem("GMN1") = Gmn1
                                drItem("PROCE") = codProce
                                drItem("ID_EMP") = idEmp
                                drItem("ID_ITEM") = oItem.Id
                                drItem("PROVE_PADRE") = codProve
                                drItem("PROVE") = codProve
                                drItem("CANT_ADJ") = oItem.CantAdj
                                drItem("CENTRO") = oItem.Centro
                                drItem("PROVE_ERP") = oGrupo.CodErpSeleccionado
                                drItem("LIA_ID") = oItem.LIAId
                                drItem("CODART") = oItem.Codigo
                                dtGrupoBorrar.Rows.Add(drItem)
                            Else
                                drItem = dtGrupoBorrar.NewRow
                                drItem("ANYO") = Anyo
                                drItem("GMN1") = Gmn1
                                drItem("PROCE") = codProce
                                drItem("ID_EMP") = idEmp
                                drItem("ID_ITEM") = oItem.Id
                                drItem("PROVE_PADRE") = codProve
                                drItem("PROVE") = codProve
                                drItem("CANT_ADJ") = oItem.CantAdj
                                drItem("CENTRO") = oItem.Centro
                                drItem("PROVE_ERP") = oItem.CodERP
                                drItem("LIA_ID") = oItem.LIAId
                                drItem("CODART") = oItem.Codigo
                                dtGrupoBorrar.Rows.Add(drItem)
                            End If
                        Next
                    Next
                Next
            Next

            DBServer.Adjudicaciones_BorrarDeERP(dtGrupoBorrar, codUsu)
        End If

        For Each EmpAdjudicada As FSNServer.EmpresaAdjudicada In oAdjudicacion.EmpresasAdjudicadas
            If EmpAdjudicada.Id = idEmp Then
                For Each ProvAdjudicado As FSNServer.ProveedorAdjudicado In EmpAdjudicada.ProveedoresAdjudicados
                    If ProvAdjudicado.Codigo = codProve Then
                        For Each oGrupo As ProveedorAdjudicado.Grupo In ProvAdjudicado.Grupos
                            If oGrupo.Codigo = codGrupo Then
                                Dim drItem As DataRow
                                For Each oItem As ProveedorAdjudicado.Item In oGrupo.Items
                                    If oItem.Id = idItem AndAlso oItem.Centro = centro Then
                                        gItem = oItem
                                        If oItem.Distribuidores.Count > 0 Then
                                            For Each oDistr As ProveedorAdjudicado.Distribuidor In oItem.Distribuidores
                                                Dim ProvPrincipal As String = oItem.Distribuidores(0).Codigo
                                                drItem = dtGrupo.NewRow
                                                drItem("ANYO") = Anyo
                                                drItem("GMN1") = Gmn1
                                                drItem("PROCE") = codProce
                                                drItem("ID_EMP") = idEmp
                                                drItem("ID_ITEM") = oItem.Id
                                                drItem("PROVE_PADRE") = ProvPrincipal
                                                drItem("PROVE") = oDistr.Codigo
                                                drItem("CANT_ADJ") = oDistr.Cantidad_Adj
                                                drItem("CENTRO") = oItem.Centro
                                                drItem("CODART") = oItem.Codigo
                                                If oDistr.CodErpSeleccionado <> "" Then
                                                    Cod_ERP = oDistr.CodErpSeleccionado
                                                ElseIf ProvAdjudicado.CodErpSeleccionado <> "" Then
                                                    Cod_ERP = ProvAdjudicado.CodErpSeleccionado
                                                End If
                                                drItem("PROVE_ERP") = Cod_ERP
                                                drItem("LIA_ID") = oItem.LIAId
                                                If hayEscalados = 0 AndAlso oDistr.Cantidad_Adj <> 0 AndAlso (oItem.Estado = -1 Or oItem.Estado = 3 Or (bMantEstIntEnTraspasoAdj And oItem.Estado = 4)) Then
                                                    dtGrupo.Rows.Add(drItem)
                                                ElseIf hayEscalados = 1 AndAlso (oItem.Estado = -1 Or oItem.Estado = 3 Or (bMantEstIntEnTraspasoAdj And oItem.Estado = 4)) Then
                                                    dtGrupo.Rows.Add(drItem)
                                                End If
                                            Next
                                        Else
                                            If ProvAdjudicado.Distribuidores.Count > 0 Then
                                                For Each oDistrProv In ProvAdjudicado.Distribuidores
                                                    Dim ProvPrincipal As String = ProvAdjudicado.Distribuidores(0).Codigo
                                                    If oDistrProv.PorcentajeDistribuido > 0 Then
                                                        drItem = dtGrupo.NewRow
                                                        drItem("ANYO") = Anyo
                                                        drItem("GMN1") = Gmn1
                                                        drItem("PROCE") = codProce
                                                        drItem("ID_EMP") = idEmp
                                                        drItem("ID_ITEM") = oItem.Id
                                                        Cantidad_Adj = oItem.CantAdj * (oDistrProv.PorcentajeDistribuido / 100)
                                                        drItem("PROVE_PADRE") = ProvPrincipal
                                                        drItem("PROVE") = oDistrProv.Codigo
                                                        drItem("CANT_ADJ") = Cantidad_Adj
                                                        drItem("CENTRO") = oItem.Centro
                                                        drItem("CODART") = oItem.Codigo
                                                        If oDistrProv.CodErpSeleccionado <> "" Then
                                                            Cod_ERP = oDistrProv.CodErpSeleccionado
                                                        ElseIf oItem.CodERP <> "" Then
                                                            Cod_ERP = oItem.CodERP
                                                        End If
                                                        drItem("PROVE_ERP") = Cod_ERP
                                                        drItem("LIA_ID") = oItem.LIAId
                                                        If hayEscalados = 0 AndAlso Cantidad_Adj > 0 AndAlso (oItem.Estado = -1 Or oItem.Estado = 3 Or (bMantEstIntEnTraspasoAdj And oItem.Estado = 4)) Then
                                                            dtGrupo.Rows.Add(drItem)
                                                        ElseIf hayEscalados = 1 AndAlso (oItem.Estado = -1 Or oItem.Estado = 3 Or (bMantEstIntEnTraspasoAdj And oItem.Estado = 4)) Then
                                                            dtGrupo.Rows.Add(drItem)
                                                        End If
                                                    End If
                                                Next
                                            Else
                                                If oGrupo.CodErpSeleccionado <> "" Then
                                                    drItem = dtGrupo.NewRow
                                                    drItem("ANYO") = Anyo
                                                    drItem("GMN1") = Gmn1
                                                    drItem("PROCE") = codProce
                                                    drItem("ID_EMP") = idEmp
                                                    drItem("ID_ITEM") = oItem.Id
                                                    drItem("PROVE_PADRE") = codProve
                                                    drItem("PROVE") = codProve
                                                    drItem("CANT_ADJ") = oItem.CantAdj
                                                    drItem("CENTRO") = oItem.Centro
                                                    drItem("PROVE_ERP") = oGrupo.CodErpSeleccionado
                                                    drItem("LIA_ID") = oItem.LIAId
                                                    drItem("CODART") = oItem.Codigo
                                                    If hayEscalados = 0 AndAlso oItem.CantAdj > 0 AndAlso (oItem.Estado = -1 Or oItem.Estado = 3 Or (bMantEstIntEnTraspasoAdj And oItem.Estado = 4)) Then
                                                        dtGrupo.Rows.Add(drItem)
                                                    ElseIf hayEscalados = 1 AndAlso (oItem.Estado = -1 Or oItem.Estado = 3 Or (bMantEstIntEnTraspasoAdj And oItem.Estado = 4)) Then
                                                        dtGrupo.Rows.Add(drItem)
                                                    End If
                                                Else
                                                    drItem = dtGrupo.NewRow
                                                    drItem("ANYO") = Anyo
                                                    drItem("GMN1") = Gmn1
                                                    drItem("PROCE") = codProce
                                                    drItem("ID_EMP") = idEmp
                                                    drItem("ID_ITEM") = oItem.Id
                                                    drItem("PROVE_PADRE") = codProve
                                                    drItem("PROVE") = codProve
                                                    drItem("CANT_ADJ") = oItem.CantAdj
                                                    drItem("CENTRO") = oItem.Centro
                                                    drItem("PROVE_ERP") = oItem.CodERP
                                                    drItem("LIA_ID") = oItem.LIAId
                                                    drItem("CODART") = oItem.Codigo
                                                    If hayEscalados = 0 AndAlso oItem.CantAdj > 0 AndAlso (oItem.Estado = -1 Or oItem.Estado = 3 Or (bMantEstIntEnTraspasoAdj And oItem.Estado = 4)) Then
                                                        dtGrupo.Rows.Add(drItem)
                                                    ElseIf hayEscalados = 1 AndAlso (oItem.Estado = -1 Or oItem.Estado = 3 Or (bMantEstIntEnTraspasoAdj And oItem.Estado = 4)) Then
                                                        dtGrupo.Rows.Add(drItem)
                                                    End If
                                                End If
                                            End If

                                        End If
                                        Exit For
                                    End If

                                Next
                                Exit For
                            End If
                        Next
                        Exit For
                    End If
                Next
                Exit For
            End If
        Next

        'comprobar integración del articulo

        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
        Dim orgCompras As Boolean = FSNServer.TipoAcceso.gbUsar_OrgCompras

        Dim bLLamarFSIS As Boolean = False
        Dim oIntegracion As FSNServer.Integracion
        oIntegracion = FSNServer.Get_Object(GetType(FSNServer.Integracion))
        Dim ierp As Integer = oIntegracion.getErpFromEmpresa(idEmp)
        If oIntegracion.hayIntegracionAdjudicaciones(ierp) Then
            If oIntegracion.hayIntegracionArticulos(ierp) Then
                If gItem.Codigo <> "" Then
                    If (Not oIntegracion.estaIntegradoArticulo(gItem.Codigo, ierp)) Then
                        Return gItem.Codigo
                    End If
                End If
            End If
            If oIntegracion.IntegracionWCFActiva(TablasIntegracion.Adj, ierp) Then bLLamarFSIS = True
        End If

        Dim dt As DataTable
        dt = DBServer.Adjudicaciones_TraspasarToERP(dtGrupo, codUsu)
        DBServer.Adjudicaciones_ActualizarProceInt(Anyo, Gmn1, codProce)

        Dim lLogItemsAdj As New List(Of Long)

        'Incluir las adjudicaciones borradas en la llamada a FSIS
        If bLLamarFSIS Then
            If Not dtGrupoBorrar Is Nothing AndAlso dtGrupoBorrar.rows.count > 0 Then
                For Each dr As DataRow In dtGrupoBorrar.Rows
                    lLogItemsAdj.Add(dr("LIA_ID"))
                Next
            End If
        End If

        If Not dt Is Nothing Then
            gItem.LIAId = dt(0)("LIA_ID")

            'Si la integracion esta activada llamamos al FSIS a traves del LibraryCOM para exportar las adjudicaciones
            If bLLamarFSIS Then
                lLogItemsAdj.Add(gItem.LIAId)
            End If

            dtEscalados = dt.Clone
            Dim dtAtributos As New DataTable

            dtAtributos.Columns.Add("LIA_ID", System.Type.GetType("System.Int32"))
            dtAtributos.Columns.Add("ID_ATRIB", System.Type.GetType("System.Int32"))
            dtAtributos.Columns.Add("AMBITO", System.Type.GetType("System.Int32"))
            dtAtributos.Columns.Add("VALOR_NUM", System.Type.GetType("System.Double"))
            dtAtributos.Columns.Add("VALOR_TEXT", System.Type.GetType("System.String"))
            dtAtributos.Columns.Add("VALOR_FEC", System.Type.GetType("System.DateTime"))
            dtAtributos.Columns.Add("VALOR_BOOL", System.Type.GetType("System.Int32"))
            Dim drAtrib As DataRow
            For Each EmpAdjudicada As FSNServer.EmpresaAdjudicada In oAdjudicacion.EmpresasAdjudicadas
                If EmpAdjudicada.Id = idEmp Then
                    For Each ProvAdjudicado As FSNServer.ProveedorAdjudicado In EmpAdjudicada.ProveedoresAdjudicados
                        If ProvAdjudicado.Codigo = codProve Then
                            If ProvAdjudicado.Atributos.Count > 0 Then
                                'Se recorren los atributos a nivel de proceso
                                For Each oRow As DataRow In dt.Rows
                                    For Each oAtributo As ProveedorAdjudicado.Atributo In ProvAdjudicado.Atributos
                                        drAtrib = dtAtributos.NewRow
                                        drAtrib("LIA_ID") = oRow("LIA_ID")
                                        drAtrib("ID_ATRIB") = oAtributo.Id
                                        drAtrib("AMBITO") = 1
                                        If oAtributo.Valor Is Nothing Then oAtributo.Valor = DBNull.Value
                                        Select Case oAtributo.Tipo
                                            Case 1
                                                drAtrib("VALOR_TEXT") = oAtributo.Valor
                                            Case 2
                                                If DBNullToStr(oAtributo.Valor) = "" Then oAtributo.Valor = DBNull.Value
                                                drAtrib("VALOR_NUM") = oAtributo.Valor
                                            Case 3
                                                If DBNullToStr(oAtributo.Valor) = "" Then oAtributo.Valor = DBNull.Value
                                                drAtrib("VALOR_FEC") = oAtributo.Valor
                                            Case 4
                                                If DBNullToStr(oAtributo.Valor) = "" Then oAtributo.Valor = DBNull.Value
                                                drAtrib("VALOR_BOOL") = oAtributo.Valor
                                        End Select
                                        dtAtributos.Rows.Add(drAtrib)
                                    Next
                                Next
                            End If
                            For Each oGrupo As ProveedorAdjudicado.Grupo In ProvAdjudicado.Grupos
                                If oGrupo.Codigo = codGrupo Then
                                    If ProvAdjudicado.Atributos.Count = 0 Then
                                        'Si no se ha pasado por el panel del proveedor se mira si estan grabados los atributos de proceso en el grupo
                                        If oGrupo.AtributosProceso.Count > 0 Then
                                            'Se recorren los atributos a nivel de grupo
                                            For Each oRow As DataRow In dt.Rows
                                                For Each oAtributo As ProveedorAdjudicado.Atributo In oGrupo.AtributosProceso
                                                    drAtrib = dtAtributos.NewRow
                                                    drAtrib("LIA_ID") = oRow("LIA_ID")
                                                    drAtrib("ID_ATRIB") = oAtributo.Id
                                                    drAtrib("AMBITO") = 1
                                                    If oAtributo.Valor Is Nothing Then oAtributo.Valor = DBNull.Value
                                                    Select Case oAtributo.Tipo
                                                        Case 1
                                                            drAtrib("VALOR_TEXT") = oAtributo.Valor
                                                        Case 2
                                                            If DBNullToStr(oAtributo.Valor) = "" Then oAtributo.Valor = DBNull.Value
                                                            drAtrib("VALOR_NUM") = oAtributo.Valor
                                                        Case 3
                                                            If DBNullToStr(oAtributo.Valor) = "" Then oAtributo.Valor = DBNull.Value
                                                            drAtrib("VALOR_FEC") = oAtributo.Valor
                                                        Case 4
                                                            If DBNullToStr(oAtributo.Valor) = "" Then oAtributo.Valor = DBNull.Value
                                                            drAtrib("VALOR_BOOL") = oAtributo.Valor
                                                    End Select
                                                    dtAtributos.Rows.Add(drAtrib)
                                                Next
                                            Next
                                        End If
                                    End If
                                    If oGrupo.Atributos.Count > 0 Then
                                        'Se recorren los atributos a nivel de grupo
                                        For Each oRow As DataRow In dt.Rows
                                            For Each oAtributo As ProveedorAdjudicado.Atributo In oGrupo.Atributos
                                                drAtrib = dtAtributos.NewRow
                                                drAtrib("LIA_ID") = oRow("LIA_ID")
                                                drAtrib("ID_ATRIB") = oAtributo.Id
                                                drAtrib("AMBITO") = 2
                                                If oAtributo.Valor Is Nothing Then oAtributo.Valor = DBNull.Value
                                                Select Case oAtributo.Tipo
                                                    Case 1
                                                        drAtrib("VALOR_TEXT") = oAtributo.Valor
                                                    Case 2
                                                        If DBNullToStr(oAtributo.Valor) = "" Then oAtributo.Valor = DBNull.Value
                                                        drAtrib("VALOR_NUM") = oAtributo.Valor
                                                    Case 3
                                                        If DBNullToStr(oAtributo.Valor) = "" Then oAtributo.Valor = DBNull.Value
                                                        drAtrib("VALOR_FEC") = oAtributo.Valor
                                                    Case 4
                                                        If DBNullToStr(oAtributo.Valor) = "" Then oAtributo.Valor = DBNull.Value
                                                        drAtrib("VALOR_BOOL") = oAtributo.Valor
                                                End Select
                                                dtAtributos.Rows.Add(drAtrib)
                                            Next
                                        Next
                                    End If
                                    For Each oItem As ProveedorAdjudicado.Item In oGrupo.Items
                                        If oItem.Atributos.Count > 0 AndAlso oItem.Id = idItem AndAlso oItem.Centro = centro Then
                                            'Se recorren los atributos a nivel de Item
                                            For Each oRow As DataRow In dt.Rows
                                                For Each oAtributo As ProveedorAdjudicado.Atributo In oItem.Atributos
                                                    drAtrib = dtAtributos.NewRow
                                                    drAtrib("LIA_ID") = oRow("LIA_ID")
                                                    drAtrib("ID_ATRIB") = oAtributo.Id
                                                    drAtrib("AMBITO") = 3
                                                    If oAtributo.Valor Is Nothing Then oAtributo.Valor = DBNull.Value
                                                    Select Case oAtributo.Tipo
                                                        Case 1
                                                            drAtrib("VALOR_TEXT") = oAtributo.Valor
                                                        Case 2
                                                            If DBNullToStr(oAtributo.Valor) = "" Then oAtributo.Valor = DBNull.Value
                                                            drAtrib("VALOR_NUM") = oAtributo.Valor
                                                        Case 3
                                                            If DBNullToStr(oAtributo.Valor) = "" Then oAtributo.Valor = DBNull.Value
                                                            drAtrib("VALOR_FEC") = oAtributo.Valor
                                                        Case 4
                                                            If DBNullToStr(oAtributo.Valor) = "" Then oAtributo.Valor = DBNull.Value
                                                            drAtrib("VALOR_BOOL") = oAtributo.Valor
                                                    End Select
                                                    dtAtributos.Rows.Add(drAtrib)
                                                Next
                                            Next
                                            Exit For
                                        End If
                                    Next
                                End If
                            Next
                        End If
                    Next
                End If
            Next
            If DBServer.Adjudicaciones_TraspasarAtributosToERP(dtAtributos) = 1 Then
                If hayEscalados = 1 Then
                    Dim ind As Short = 0
                    For Each drRow As DataRow In dt.Rows
                        Dim drEsc As DataRow
                        drEsc = dtEscalados.NewRow
                        drEsc("ANYO") = drRow("ANYO")
                        drEsc("GMN1") = drRow("GMN1")
                        drEsc("PROCE") = drRow("PROCE")
                        drEsc("ID_EMP") = drRow("ID_EMP")
                        drEsc("ID_ITEM") = drRow("ID_ITEM")
                        drEsc("PROVE_PADRE") = drRow("PROVE_PADRE")
                        drEsc("PROVE") = drRow("PROVE")
                        drEsc("CANT_ADJ") = drRow("CANT_ADJ")
                        drEsc("CENTRO") = drRow("CENTRO")
                        drEsc("PROVE_ERP") = drRow("PROVE_ERP")
                        drEsc("LIA_ID") = drRow("LIA_ID")
                        dtEscalados.Rows.Add(drEsc)
                    Next
                    Contexto.Cache.Insert("oAdjudicacion_" & codUsu, oAdjudicacion, Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration, New TimeSpan(0, System.Configuration.ConfigurationManager.AppSettings("TiempoExpiracionCacheMin_2"), 0))
                    sRetorno = DBServer.Adjudicaciones_TraspasarEscaladosToERP(dtEscalados)
                Else
                    Contexto.Cache.Insert("oAdjudicacion_" & codUsu, oAdjudicacion, Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration, New TimeSpan(0, System.Configuration.ConfigurationManager.AppSettings("TiempoExpiracionCacheMin_2"), 0))
                    sRetorno = 1
                End If
            Else
                sRetorno = 0
            End If

        Else
            sRetorno = 0
        End If

        'Si la integraciÃ³n estÃ¡ activada llamamos a FSIS.
        If bLLamarFSIS Then
            oIntegracion.LlamarFSISLista(TablasIntegracion.Adj, lLogItemsAdj, ierp)
        End If
        Return sRetorno
    End Function
    ''' <summary>
    ''' Traspasa los items de un grupo al erp, comprobando previamente que todos los artículos estén correctamente integrados.
    ''' En caso de no estarlo se cancela la integración.
    ''' </summary>
    ''' <param name="oAdjudicacion"></param>
    ''' <param name="Anyo"></param>
    ''' <param name="Gmn1"></param>
    ''' <param name="codProce"></param>
    ''' <param name="idEmp"></param>
    ''' <param name="codProve"></param>
    ''' <param name="codGrupo"></param>
    ''' <param name="hayEscalados"></param>
    ''' <param name="codUsu"></param>
    ''' <param name="oAdjsEliminar">Adjudicaciones a eliminar</param>
    ''' <returns>1 si el traspaso ha sido correcto, 2 = error , alguno de los artículos no está correctamente integrado</returns>
    ''' <remarks></remarks>
    Public Function TraspasarGrupoToERP(ByVal oAdjudicacion As Adjudicacion, ByVal Anyo As Short, ByVal Gmn1 As String, ByVal codProce As Integer, ByVal idEmp As Short, ByVal codProve As String, ByVal codGrupo As String, ByVal hayEscalados As Byte,
                                        ByVal codUsu As String, ByRef articulosNoIntegrados As String, ByVal oAdjsEliminar As Fullstep.FSNServer.Adjudicacion, ByVal bMantEstIntEnTraspasoAdj As Boolean) As String
        Dim Cantidad_Adj As Double
        Dim Cod_ERP As String = String.Empty
        Dim sRetorno As String
        Dim dtGrupo As DataTable = NuevaTablaTraspasarGrupoAERP()
        Dim dtEscalados As New DataTable
        Dim gGrupo As ProveedorAdjudicado.Grupo = Nothing
        Dim Contexto As System.Web.HttpContext
        Contexto = System.Web.HttpContext.Current
        Dim items As New List(Of ProveedorAdjudicado.Item)
        Dim dtGrupoBorrar As DataTable

        'Borrar adjudicaciones anteriores
        If Not oAdjsEliminar Is Nothing Then
            dtGrupoBorrar = NuevaTablaTraspasarGrupoAERP()
            For Each EmpAdjudicada As FSNServer.EmpresaAdjudicada In oAdjsEliminar.EmpresasAdjudicadas
                For Each ProvAdjudicado As FSNServer.ProveedorAdjudicado In EmpAdjudicada.ProveedoresAdjudicados
                    For Each oGrupo As ProveedorAdjudicado.Grupo In ProvAdjudicado.Grupos
                        Dim drItem As DataRow
                        For Each oItem As ProveedorAdjudicado.Item In oGrupo.Items
                            drItem = dtGrupoBorrar.NewRow
                            drItem("ANYO") = oGrupo.Anyo
                            drItem("GMN1") = oGrupo.GMN1
                            drItem("PROCE") = oGrupo.Proce
                            drItem("ID_EMP") = EmpAdjudicada.Id
                            drItem("ID_ITEM") = oItem.Id
                            drItem("PROVE_PADRE") = ProvAdjudicado.Codigo
                            drItem("PROVE") = ProvAdjudicado.Codigo
                            drItem("CANT_ADJ") = oItem.CantAdj
                            drItem("CENTRO") = oItem.Centro
                            drItem("LIA_ID") = oItem.LIAId
                            drItem("CODART") = oItem.Codigo
                            If oGrupo.CodErpSeleccionado <> "" Then
                                drItem("PROVE_ERP") = oGrupo.CodErpSeleccionado
                            Else
                                drItem("PROVE_ERP") = oItem.CodERP
                            End If
                            dtGrupoBorrar.Rows.Add(drItem)
                        Next
                    Next
                Next
            Next

            DBServer.Adjudicaciones_BorrarDeERP(dtGrupoBorrar, codUsu)
        End If

        For Each EmpAdjudicada As FSNServer.EmpresaAdjudicada In oAdjudicacion.EmpresasAdjudicadas
            If EmpAdjudicada.Id = idEmp Then
                For Each ProvAdjudicado As FSNServer.ProveedorAdjudicado In EmpAdjudicada.ProveedoresAdjudicados
                    If ProvAdjudicado.Codigo = codProve Then
                        For Each oGrupo As ProveedorAdjudicado.Grupo In ProvAdjudicado.Grupos
                            If oGrupo.Codigo = codGrupo Then
                                gGrupo = oGrupo
                                Dim drItem As DataRow
                                For Each oItem As ProveedorAdjudicado.Item In oGrupo.Items
                                    items.Add(oItem)
                                    If oItem.Distribuidores.Count > 0 Then
                                        For Each oDistr As ProveedorAdjudicado.Distribuidor In oItem.Distribuidores
                                            Dim ProvPrincipal As String = oItem.Distribuidores(0).Codigo
                                            drItem = dtGrupo.NewRow
                                            drItem("ANYO") = Anyo
                                            drItem("GMN1") = Gmn1
                                            drItem("PROCE") = codProce
                                            drItem("ID_EMP") = idEmp
                                            drItem("ID_ITEM") = oItem.Id
                                            drItem("PROVE_PADRE") = Trim(ProvPrincipal)
                                            drItem("PROVE") = Trim(oDistr.Codigo)
                                            drItem("CANT_ADJ") = oDistr.Cantidad_Adj
                                            drItem("CENTRO") = oItem.Centro
                                            drItem("LIA_ID") = oItem.LIAId
                                            drItem("CODART") = oItem.Codigo
                                            If oDistr.CodErpSeleccionado <> "" Then
                                                Cod_ERP = oDistr.CodErpSeleccionado
                                            ElseIf ProvAdjudicado.CodErpSeleccionado <> "" Then
                                                Cod_ERP = ProvAdjudicado.CodErpSeleccionado
                                            End If
                                            drItem("PROVE_ERP") = Cod_ERP
                                            If hayEscalados = 0 AndAlso oDistr.Cantidad_Adj <> 0 AndAlso (oItem.Estado = -1 Or oItem.Estado = 3 Or (bMantEstIntEnTraspasoAdj And oItem.Estado = 4)) Then
                                                dtGrupo.Rows.Add(drItem)
                                            ElseIf hayEscalados = 1 AndAlso (oItem.Estado = -1 Or oItem.Estado = 3 Or (bMantEstIntEnTraspasoAdj And oItem.Estado = 4)) Then
                                                dtGrupo.Rows.Add(drItem)
                                            End If
                                        Next
                                    Else
                                        If ProvAdjudicado.Distribuidores.Count > 0 Then
                                            For Each oDistrProv In ProvAdjudicado.Distribuidores
                                                Dim ProvPrincipal As String = ProvAdjudicado.Distribuidores(0).Codigo
                                                If oDistrProv.PorcentajeDistribuido > 0 Then
                                                    drItem = dtGrupo.NewRow
                                                    drItem("ANYO") = Anyo
                                                    drItem("GMN1") = Gmn1
                                                    drItem("PROCE") = codProce
                                                    drItem("ID_EMP") = idEmp
                                                    drItem("ID_ITEM") = oItem.Id
                                                    Cantidad_Adj = oItem.CantAdj * (oDistrProv.PorcentajeDistribuido / 100)
                                                    drItem("PROVE_PADRE") = Trim(ProvPrincipal)
                                                    drItem("PROVE") = Trim(oDistrProv.Codigo)
                                                    drItem("CANT_ADJ") = Cantidad_Adj
                                                    drItem("CENTRO") = oItem.Centro
                                                    drItem("LIA_ID") = oItem.LIAId
                                                    drItem("CODART") = oItem.Codigo
                                                    If oDistrProv.CodErpSeleccionado <> "" Then
                                                        Cod_ERP = oDistrProv.CodErpSeleccionado
                                                    ElseIf ProvAdjudicado.CodErpSeleccionado <> "" Then
                                                        Cod_ERP = ProvAdjudicado.CodErpSeleccionado
                                                    ElseIf oItem.CodERP <> "" Then
                                                        Cod_ERP = oItem.CodERP
                                                    End If
                                                    drItem("PROVE_ERP") = Cod_ERP
                                                    If hayEscalados = 0 AndAlso Cantidad_Adj > 0 AndAlso (oItem.Estado = -1 Or oItem.Estado = 3 Or (bMantEstIntEnTraspasoAdj And oItem.Estado = 4)) Then
                                                        dtGrupo.Rows.Add(drItem)
                                                    ElseIf hayEscalados = 1 AndAlso (oItem.Estado = -1 Or oItem.Estado = 3 Or (bMantEstIntEnTraspasoAdj And oItem.Estado = 4)) Then
                                                        dtGrupo.Rows.Add(drItem)
                                                    End If
                                                    'ElseIf oGrupo.CodErpSeleccionado <> "" Then
                                                    '    drItem = dtGrupo.NewRow
                                                    '    drItem("ANYO") = Anyo
                                                    '    drItem("GMN1") = Gmn1
                                                    '    drItem("PROCE") = codProce
                                                    '    drItem("ID_EMP") = idEmp
                                                    '    drItem("ID_ITEM") = oItem.Id
                                                    '    drItem("PROVE_PADRE") = codProve
                                                    '    drItem("PROVE") = codProve
                                                    '    drItem("CANT_ADJ") = oItem.CantAdj
                                                    '    drItem("CENTRO") = oItem.Centro
                                                    '    drItem("LIA_ID") = oItem.LIAId
                                                    '    drItem("PROVE_ERP") = oGrupo.CodErpSeleccionado
                                                    '    drItem("CODART") = oItem.Codigo
                                                    '    If hayEscalados = 0 AndAlso oItem.CantAdj > 0 AndAlso (oItem.Estado = -1 Or oItem.Estado = 3 Or (bMantEstIntEnTraspasoAdj And oItem.Estado = 4)) Then
                                                    '        dtGrupo.Rows.Add(drItem)
                                                    '    ElseIf hayEscalados = 1 AndAlso (oItem.Estado = -1 Or oItem.Estado = 3 Or (bMantEstIntEnTraspasoAdj And oItem.Estado = 4)) Then
                                                    '        dtGrupo.Rows.Add(drItem)
                                                    '    End If
                                                End If
                                            Next
                                        Else
                                            If oGrupo.CodErpSeleccionado <> "" Then
                                                drItem = dtGrupo.NewRow
                                                drItem("ANYO") = Anyo
                                                drItem("GMN1") = Gmn1
                                                drItem("PROCE") = codProce
                                                drItem("ID_EMP") = idEmp
                                                drItem("ID_ITEM") = oItem.Id
                                                drItem("PROVE_PADRE") = codProve
                                                drItem("PROVE") = codProve
                                                drItem("CANT_ADJ") = oItem.CantAdj
                                                drItem("CENTRO") = oItem.Centro
                                                drItem("LIA_ID") = oItem.LIAId
                                                drItem("PROVE_ERP") = oGrupo.CodErpSeleccionado
                                                drItem("CODART") = oItem.Codigo
                                                If hayEscalados = 0 AndAlso oItem.CantAdj > 0 AndAlso (oItem.Estado = -1 Or oItem.Estado = 3 Or (bMantEstIntEnTraspasoAdj And oItem.Estado = 4)) Then
                                                    dtGrupo.Rows.Add(drItem)
                                                ElseIf hayEscalados = 1 AndAlso (oItem.Estado = -1 Or oItem.Estado = 3 Or (bMantEstIntEnTraspasoAdj And oItem.Estado = 4)) Then
                                                    dtGrupo.Rows.Add(drItem)
                                                End If
                                            ElseIf ProvAdjudicado.CodErpSeleccionado <> "" Then
                                                drItem = dtGrupo.NewRow
                                                drItem("ANYO") = Anyo
                                                drItem("GMN1") = Gmn1
                                                drItem("PROCE") = codProce
                                                drItem("ID_EMP") = idEmp
                                                drItem("ID_ITEM") = oItem.Id
                                                drItem("PROVE_PADRE") = codProve
                                                drItem("PROVE") = codProve
                                                drItem("CANT_ADJ") = oItem.CantAdj
                                                drItem("CENTRO") = oItem.Centro
                                                drItem("LIA_ID") = oItem.LIAId
                                                drItem("PROVE_ERP") = ProvAdjudicado.CodErpSeleccionado
                                                drItem("CODART") = oItem.Codigo
                                                If hayEscalados = 0 AndAlso oItem.CantAdj > 0 AndAlso (oItem.Estado = -1 Or oItem.Estado = 3 Or (bMantEstIntEnTraspasoAdj And oItem.Estado = 4)) Then
                                                    dtGrupo.Rows.Add(drItem)
                                                ElseIf hayEscalados = 1 AndAlso (oItem.Estado = -1 Or oItem.Estado = 3 Or (bMantEstIntEnTraspasoAdj And oItem.Estado = 4)) Then
                                                    dtGrupo.Rows.Add(drItem)
                                                End If
                                            Else
                                                drItem = dtGrupo.NewRow
                                                drItem("ANYO") = Anyo
                                                drItem("GMN1") = Gmn1
                                                drItem("PROCE") = codProce
                                                drItem("ID_EMP") = idEmp
                                                drItem("ID_ITEM") = oItem.Id
                                                drItem("PROVE_PADRE") = codProve
                                                drItem("PROVE") = codProve
                                                drItem("CANT_ADJ") = oItem.CantAdj
                                                drItem("CENTRO") = oItem.Centro
                                                drItem("LIA_ID") = oItem.LIAId
                                                drItem("PROVE_ERP") = oItem.CodERP
                                                drItem("CODART") = oItem.Codigo
                                                If hayEscalados = 0 AndAlso oItem.CantAdj > 0 AndAlso (oItem.Estado = -1 Or oItem.Estado = 3 Or (bMantEstIntEnTraspasoAdj And oItem.Estado = 4)) Then
                                                    dtGrupo.Rows.Add(drItem)
                                                ElseIf hayEscalados = 1 AndAlso (oItem.Estado = -1 Or oItem.Estado = 3 Or (bMantEstIntEnTraspasoAdj And oItem.Estado = 4)) Then
                                                    dtGrupo.Rows.Add(drItem)
                                                End If
                                            End If
                                        End If

                                    End If
                                Next
                                Exit For
                            End If
                        Next
                        Exit For
                    End If
                Next
                Exit For
            End If

        Next
        Dim dt As DataTable

        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        '' Validar Integración ''''''''''''''''''''''''''''''''''''''''''''''''''
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Dim bLLamarFSIS As Boolean = False
        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim FSNUser As FSNServer.User = HttpContext.Current.Session("FSN_User")
        Dim oIntegracion As FSNServer.Integracion
        oIntegracion = FSNServer.Get_Object(GetType(FSNServer.Integracion))
        Dim borgCompras As Boolean = FSNServer.TipoAcceso.gbUsar_OrgCompras
        Dim sErp As String = oIntegracion.getErpFromEmpresa(idEmp)
        If oIntegracion.hayIntegracionAdjudicaciones(sErp) Then
            If oIntegracion.hayIntegracionArticulos(sErp) Then
                Dim oAdjudicaciones As Fullstep.FSNServer.Adjudicaciones
                oAdjudicaciones = FSNServer.Get_Object(GetType(FSNServer.Adjudicaciones))
                Dim setArticulos As ISet(Of String) = New HashSet(Of String)
                For Each item In items
                    If Not IsNothing(item.Codigo) And item.Codigo <> "" Then
                        'inserto codigos de articulos en un conjunto para evitar duplicados
                        setArticulos.Add(item.Codigo)
                    End If

                Next

                For Each codarticulo As String In setArticulos
                    If codarticulo <> "" Then
                        If Not oIntegracion.estaIntegradoArticulo(codarticulo, sErp) Then
                            articulosNoIntegrados &= codarticulo & ","
                        End If
                    End If
                Next
                If articulosNoIntegrados.Count > 0 Then
                    Return "2"
                End If
            End If
            If oIntegracion.IntegracionWCFActiva(TablasIntegracion.Adj, sErp) Then bLLamarFSIS = True
        End If
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        dt = DBServer.Adjudicaciones_TraspasarToERP(dtGrupo, codUsu)

        DBServer.Adjudicaciones_ActualizarProceInt(Anyo, Gmn1, codProce)

        Dim lLogItemsAdj As New List(Of Long)

        'Incluir las adjudicaciones borradas en la llamada a FSIS
        If bLLamarFSIS Then
            If Not dtGrupoBorrar Is Nothing AndAlso dtGrupoBorrar.rows.count > 0 Then
                For Each dr As DataRow In dtGrupoBorrar.Rows
                    lLogItemsAdj.Add(dr("LIA_ID"))
                Next
            End If
        End If

        If Not dt Is Nothing Then
            'Actualizo la propiedad LIAid de los items al grabarse en LOG_ITEM_ADJ
            For Each oItem As ProveedorAdjudicado.Item In gGrupo.Items
                For Each oRow As DataRow In dt.Rows
                    If oItem.Id = oRow("ID_ITEM") Then
                        oItem.LIAId = oRow("LIA_ID")
                        lLogItemsAdj.Add(oItem.LIAId)
                    End If
                Next
            Next

            dtEscalados = dt.Clone
            Dim dtAtributos As New DataTable
            dtAtributos.Columns.Add("LIA_ID", System.Type.GetType("System.Int32"))
            dtAtributos.Columns.Add("ID_ATRIB", System.Type.GetType("System.Int32"))
            dtAtributos.Columns.Add("AMBITO", System.Type.GetType("System.Int32"))
            dtAtributos.Columns.Add("VALOR_NUM", System.Type.GetType("System.Double"))
            dtAtributos.Columns.Add("VALOR_TEXT", System.Type.GetType("System.String"))
            dtAtributos.Columns.Add("VALOR_FEC", System.Type.GetType("System.DateTime"))
            dtAtributos.Columns.Add("VALOR_BOOL", System.Type.GetType("System.Int32"))

            Dim drAtrib As DataRow
            For Each EmpAdjudicada As FSNServer.EmpresaAdjudicada In oAdjudicacion.EmpresasAdjudicadas
                If EmpAdjudicada.Id = idEmp Then
                    For Each ProvAdjudicado As FSNServer.ProveedorAdjudicado In EmpAdjudicada.ProveedoresAdjudicados
                        If ProvAdjudicado.Codigo = codProve Then
                            If ProvAdjudicado.Atributos.Count > 0 Then
                                'Se recorren los atributos a nivel de proceso
                                For Each oRow As DataRow In dt.Rows
                                    For Each oAtributo As ProveedorAdjudicado.Atributo In ProvAdjudicado.Atributos
                                        drAtrib = dtAtributos.NewRow
                                        drAtrib("LIA_ID") = oRow("LIA_ID")
                                        drAtrib("ID_ATRIB") = oAtributo.Id
                                        drAtrib("AMBITO") = 1
                                        If oAtributo.Valor Is Nothing Then oAtributo.Valor = DBNull.Value
                                        Select Case oAtributo.Tipo
                                            Case 1
                                                drAtrib("VALOR_TEXT") = oAtributo.Valor
                                            Case 2
                                                If DBNullToStr(oAtributo.Valor) = "" Then oAtributo.Valor = DBNull.Value
                                                drAtrib("VALOR_NUM") = oAtributo.Valor
                                            Case 3
                                                If DBNullToStr(oAtributo.Valor) = "" Then oAtributo.Valor = DBNull.Value
                                                drAtrib("VALOR_FEC") = oAtributo.Valor
                                            Case 4
                                                If DBNullToStr(oAtributo.Valor) = "" Then oAtributo.Valor = DBNull.Value
                                                drAtrib("VALOR_BOOL") = oAtributo.Valor
                                        End Select
                                        dtAtributos.Rows.Add(drAtrib)
                                    Next
                                Next
                            End If
                            For Each oGrupo As ProveedorAdjudicado.Grupo In ProvAdjudicado.Grupos
                                If oGrupo.Codigo = codGrupo Then
                                    If ProvAdjudicado.Atributos.Count = 0 Then
                                        'Si no se ha pasado por el panel del proveedor se mira si estan grabados los atributos de proceso en el grupo
                                        If oGrupo.AtributosProceso.Count > 0 Then
                                            'Se recorren los atributos a nivel de grupo
                                            For Each oRow As DataRow In dt.Rows
                                                For Each oAtributo As ProveedorAdjudicado.Atributo In oGrupo.AtributosProceso
                                                    drAtrib = dtAtributos.NewRow
                                                    drAtrib("LIA_ID") = oRow("LIA_ID")
                                                    drAtrib("ID_ATRIB") = oAtributo.Id
                                                    drAtrib("AMBITO") = 1
                                                    If oAtributo.Valor Is Nothing Then oAtributo.Valor = DBNull.Value
                                                    Select Case oAtributo.Tipo
                                                        Case 1
                                                            drAtrib("VALOR_TEXT") = oAtributo.Valor
                                                        Case 2
                                                            If DBNullToStr(oAtributo.Valor) = "" Then oAtributo.Valor = DBNull.Value
                                                            drAtrib("VALOR_NUM") = oAtributo.Valor
                                                        Case 3
                                                            If DBNullToStr(oAtributo.Valor) = "" Then oAtributo.Valor = DBNull.Value
                                                            drAtrib("VALOR_FEC") = oAtributo.Valor
                                                        Case 4
                                                            If DBNullToStr(oAtributo.Valor) = "" Then oAtributo.Valor = DBNull.Value
                                                            drAtrib("VALOR_BOOL") = oAtributo.Valor
                                                    End Select
                                                    dtAtributos.Rows.Add(drAtrib)
                                                Next
                                            Next
                                        End If
                                    End If
                                    If oGrupo.Atributos.Count > 0 Then
                                        'Se recorren los atributos a nivel de grupo
                                        For Each oRow As DataRow In dt.Rows
                                            For Each oAtributo As ProveedorAdjudicado.Atributo In oGrupo.Atributos
                                                drAtrib = dtAtributos.NewRow
                                                drAtrib("LIA_ID") = oRow("LIA_ID")
                                                drAtrib("ID_ATRIB") = oAtributo.Id
                                                drAtrib("AMBITO") = 2
                                                If oAtributo.Valor Is Nothing Then oAtributo.Valor = DBNull.Value
                                                Select Case oAtributo.Tipo
                                                    Case 1
                                                        drAtrib("VALOR_TEXT") = oAtributo.Valor
                                                    Case 2
                                                        If DBNullToStr(oAtributo.Valor) = "" Then oAtributo.Valor = DBNull.Value
                                                        drAtrib("VALOR_NUM") = oAtributo.Valor
                                                    Case 3
                                                        If DBNullToStr(oAtributo.Valor) = "" Then oAtributo.Valor = DBNull.Value
                                                        drAtrib("VALOR_FEC") = oAtributo.Valor
                                                    Case 4
                                                        If DBNullToStr(oAtributo.Valor) = "" Then oAtributo.Valor = DBNull.Value
                                                        drAtrib("VALOR_BOOL") = oAtributo.Valor
                                                End Select
                                                dtAtributos.Rows.Add(drAtrib)
                                            Next
                                        Next
                                    End If
                                    For Each oItem As ProveedorAdjudicado.Item In oGrupo.Items
                                        If oItem.Atributos.Count > 0 Then
                                            'Se recorren los atributos a nivel de item
                                            For Each oRow As DataRow In dt.Rows
                                                If oRow("ID_ITEM") = oItem.Id Then
                                                    For Each oAtributo As ProveedorAdjudicado.Atributo In oItem.Atributos
                                                        drAtrib = dtAtributos.NewRow
                                                        drAtrib("LIA_ID") = oRow("LIA_ID")
                                                        drAtrib("ID_ATRIB") = oAtributo.Id
                                                        drAtrib("AMBITO") = 3
                                                        If oAtributo.Valor Is Nothing Then oAtributo.Valor = DBNull.Value
                                                        Select Case oAtributo.Tipo
                                                            Case 1
                                                                drAtrib("VALOR_TEXT") = oAtributo.Valor
                                                            Case 2
                                                                If DBNullToStr(oAtributo.Valor) = "" Then oAtributo.Valor = DBNull.Value
                                                                drAtrib("VALOR_NUM") = oAtributo.Valor
                                                            Case 3
                                                                If DBNullToStr(oAtributo.Valor) = "" Then oAtributo.Valor = DBNull.Value
                                                                drAtrib("VALOR_FEC") = oAtributo.Valor
                                                            Case 4
                                                                If DBNullToStr(oAtributo.Valor) = "" Then oAtributo.Valor = DBNull.Value
                                                                drAtrib("VALOR_BOOL") = oAtributo.Valor
                                                        End Select
                                                        dtAtributos.Rows.Add(drAtrib)
                                                    Next
                                                    Exit For
                                                End If
                                            Next
                                        End If
                                    Next
                                End If
                            Next
                        End If
                    Next
                End If
            Next
            If DBServer.Adjudicaciones_TraspasarAtributosToERP(dtAtributos) = 1 Then
                If hayEscalados = 1 Then
                    Dim ind As Short = 0
                    For Each drRow As DataRow In dt.Rows
                        Dim drEsc As DataRow
                        drEsc = dtEscalados.NewRow
                        drEsc("ANYO") = drRow("ANYO")
                        drEsc("GMN1") = drRow("GMN1")
                        drEsc("PROCE") = drRow("PROCE")
                        drEsc("ID_EMP") = drRow("ID_EMP")
                        drEsc("ID_ITEM") = drRow("ID_ITEM")
                        drEsc("PROVE_PADRE") = drRow("PROVE_PADRE")
                        drEsc("PROVE") = drRow("PROVE")
                        drEsc("CANT_ADJ") = drRow("CANT_ADJ")
                        drEsc("CENTRO") = drRow("CENTRO")
                        drEsc("PROVE_ERP") = drRow("PROVE_ERP")
                        drEsc("LIA_ID") = drRow("LIA_ID")
                        drEsc("CODART") = drRow("CODART")
                        dtEscalados.Rows.Add(drEsc)
                    Next
                    Contexto.Cache.Insert("oAdjudicacion_" & codUsu, oAdjudicacion, Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration, New TimeSpan(0, System.Configuration.ConfigurationManager.AppSettings("TiempoExpiracionCacheMin_2"), 0))
                    sRetorno = DBServer.Adjudicaciones_TraspasarEscaladosToERP(dtEscalados)
                Else
                    Contexto.Cache.Insert("oAdjudicacion_" & codUsu, oAdjudicacion, Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration, New TimeSpan(0, System.Configuration.ConfigurationManager.AppSettings("TiempoExpiracionCacheMin_2"), 0))
                    sRetorno = "1"
                End If
            Else
                sRetorno = "0"
            End If

        Else
            sRetorno = "0"
        End If

        'Si la integracion esta activada llamamos al FSIS a traves del LibraryCOM para exportar las adjudicaciones
        If bLLamarFSIS Then
            oIntegracion.LlamarFSISLista(TablasIntegracion.Adj, lLogItemsAdj, sErp)
        End If
        Return sRetorno
    End Function
    Public Function BorrarGrupoDeERP(ByVal oAdjudicacion As Adjudicacion, ByVal Anyo As Short, ByVal Gmn1 As String, ByVal codProce As Integer, ByVal idEmp As Short, ByVal codProve As String, ByVal codGrupo As String, ByVal codUsu As String) As Byte
        Dim Cantidad_Adj As Double
        Dim Cod_ERP As String = String.Empty
        Dim Resultado As Byte
        Dim dtGrupo As DataTable = NuevaTablaTraspasarGrupoAERP()
        For Each EmpAdjudicada As FSNServer.EmpresaAdjudicada In oAdjudicacion.EmpresasAdjudicadas
            If EmpAdjudicada.Id = idEmp Then
                For Each ProvAdjudicado As FSNServer.ProveedorAdjudicado In EmpAdjudicada.ProveedoresAdjudicados
                    If ProvAdjudicado.Codigo = codProve Then
                        For Each oGrupo As ProveedorAdjudicado.Grupo In ProvAdjudicado.Grupos
                            If oGrupo.Codigo = codGrupo Then
                                Dim drItem As DataRow
                                For Each oItem As ProveedorAdjudicado.Item In oGrupo.Items
                                    If oItem.Distribuidores.Count > 0 Then
                                        For Each oDistr As ProveedorAdjudicado.Distribuidor In oItem.Distribuidores
                                            Dim ProvPrincipal As String = oItem.Distribuidores(0).Codigo
                                            drItem = dtGrupo.NewRow
                                            drItem("ANYO") = Anyo
                                            drItem("GMN1") = Gmn1
                                            drItem("PROCE") = codProce
                                            drItem("ID_EMP") = idEmp
                                            drItem("ID_ITEM") = oItem.Id
                                            drItem("PROVE_PADRE") = ProvPrincipal
                                            drItem("PROVE") = oDistr.Codigo
                                            drItem("CANT_ADJ") = oDistr.Cantidad_Adj
                                            drItem("CENTRO") = oItem.Centro
                                            If oDistr.CodErpSeleccionado <> "" Then
                                                Cod_ERP = oItem.CodERP
                                            ElseIf ProvAdjudicado.CodErpSeleccionado <> "" Then
                                                Cod_ERP = ProvAdjudicado.CodErpSeleccionado
                                            End If
                                            drItem("PROVE_ERP") = Cod_ERP
                                            drItem("LIA_ID") = oItem.LIAId
                                            drItem("CODART") = oItem.Codigo
                                            If oItem.Estado = 4 Then
                                                dtGrupo.Rows.Add(drItem)
                                            End If
                                        Next
                                    Else
                                        If ProvAdjudicado.Distribuidores.Count > 0 Then
                                            For Each oDistrProv In ProvAdjudicado.Distribuidores
                                                Dim ProvPrincipal As String = ProvAdjudicado.Distribuidores(0).Codigo
                                                If oDistrProv.PorcentajeDistribuido > 0 Then
                                                    drItem = dtGrupo.NewRow
                                                    drItem("ANYO") = Anyo
                                                    drItem("GMN1") = Gmn1
                                                    drItem("PROCE") = codProce
                                                    drItem("ID_EMP") = idEmp
                                                    drItem("ID_ITEM") = oItem.Id
                                                    Cantidad_Adj = oItem.CantAdj * (oDistrProv.PorcentajeDistribuido / 100)
                                                    drItem("PROVE_PADRE") = ProvPrincipal
                                                    drItem("PROVE") = oDistrProv.Codigo
                                                    drItem("CANT_ADJ") = Cantidad_Adj
                                                    drItem("CENTRO") = oItem.Centro
                                                    If oItem.CodERP <> "" Then
                                                        Cod_ERP = oItem.CodERP
                                                    ElseIf ProvAdjudicado.CodErpSeleccionado <> "" Then
                                                        Cod_ERP = ProvAdjudicado.CodErpSeleccionado
                                                    End If
                                                    drItem("PROVE_ERP") = Cod_ERP
                                                    drItem("LIA_ID") = oItem.LIAId
                                                    drItem("CODART") = oItem.Codigo
                                                    If oItem.Estado = 4 Then
                                                        dtGrupo.Rows.Add(drItem)
                                                    End If
                                                ElseIf oGrupo.CodErpSeleccionado <> "" Then
                                                    drItem = dtGrupo.NewRow
                                                    drItem("ANYO") = Anyo
                                                    drItem("GMN1") = Gmn1
                                                    drItem("PROCE") = codProce
                                                    drItem("ID_EMP") = idEmp
                                                    drItem("ID_ITEM") = oItem.Id
                                                    drItem("PROVE_PADRE") = codProve
                                                    drItem("PROVE") = codProve
                                                    drItem("CANT_ADJ") = oItem.CantAdj
                                                    drItem("CENTRO") = oItem.Centro
                                                    drItem("PROVE_ERP") = oGrupo.CodErpSeleccionado
                                                    drItem("LIA_ID") = oItem.LIAId
                                                    drItem("CODART") = oItem.Codigo
                                                    If oItem.Estado = 4 Then
                                                        dtGrupo.Rows.Add(drItem)
                                                    End If
                                                End If
                                            Next
                                        Else
                                            If oGrupo.CodErpSeleccionado <> "" Then
                                                drItem = dtGrupo.NewRow
                                                drItem("ANYO") = Anyo
                                                drItem("GMN1") = Gmn1
                                                drItem("PROCE") = codProce
                                                drItem("ID_EMP") = idEmp
                                                drItem("ID_ITEM") = oItem.Id
                                                drItem("PROVE_PADRE") = codProve
                                                drItem("PROVE") = codProve
                                                drItem("CANT_ADJ") = oItem.CantAdj
                                                drItem("CENTRO") = oItem.Centro
                                                drItem("PROVE_ERP") = oGrupo.CodErpSeleccionado
                                                drItem("LIA_ID") = oItem.LIAId
                                                drItem("CODART") = oItem.Codigo
                                                If oItem.Estado = 4 Then
                                                    dtGrupo.Rows.Add(drItem)
                                                End If
                                            Else
                                                drItem = dtGrupo.NewRow
                                                drItem("ANYO") = Anyo
                                                drItem("GMN1") = Gmn1
                                                drItem("PROCE") = codProce
                                                drItem("ID_EMP") = idEmp
                                                drItem("ID_ITEM") = oItem.Id
                                                drItem("PROVE_PADRE") = codProve
                                                drItem("PROVE") = codProve
                                                drItem("CANT_ADJ") = oItem.CantAdj
                                                drItem("CENTRO") = oItem.Centro
                                                drItem("PROVE_ERP") = oItem.CodERP
                                                drItem("LIA_ID") = oItem.LIAId
                                                drItem("CODART") = oItem.Codigo
                                                If oItem.Estado = 4 Then
                                                    dtGrupo.Rows.Add(drItem)
                                                End If
                                            End If
                                        End If

                                    End If
                                Next
                                Exit For
                            End If
                        Next
                        Exit For
                    End If
                Next
                Exit For
            End If
        Next

        If dtGrupo.Rows.Count = 0 Then
            Return 1
        Else
            Resultado = DBServer.Adjudicaciones_BorrarDeERP(dtGrupo, codUsu)
            Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
            Dim oIntegracion As FSNServer.Integracion
            oIntegracion = FSNServer.Get_Object(GetType(FSNServer.Integracion))
            Dim ierp As Integer = oIntegracion.getErpFromEmpresa(idEmp)

            'Si la integraciÃ³n estÃ¡ activada llamamos a FSIS.
            If oIntegracion.IntegracionWCFActiva(TablasIntegracion.Adj, ierp) Then
                Dim lLogItemsAdj As List(Of Long)
                lLogItemsAdj = oIntegracion.ObtenerIdentificadoresBorradosADJ(Anyo, Gmn1, codProce, ierp)
                oIntegracion.LlamarFSISLista(TablasIntegracion.Adj, lLogItemsAdj, ierp)
            End If


            Return Resultado
        End If
    End Function
    Private Function NuevaTablaTraspasarGrupoAERP() As DataTable
        Dim dtGrupo As New DataTable
        dtGrupo.Columns.Add("ANYO")
        dtGrupo.Columns.Add("GMN1")
        dtGrupo.Columns.Add("PROCE")
        dtGrupo.Columns.Add("ID_ITEM")
        dtGrupo.Columns.Add("ID_EMP")
        dtGrupo.Columns.Add("PROVE_PADRE")
        dtGrupo.Columns.Add("PROVE")
        dtGrupo.Columns.Add("CANT_ADJ")
        dtGrupo.Columns.Add("CENTRO")
        dtGrupo.Columns.Add("PROVE_ERP")
        dtGrupo.Columns.Add("LIA_ID")
        dtGrupo.Columns.Add("CODART")
        Return dtGrupo
    End Function
    Public Function BorrarItemDeERP(ByVal oAdjudicacion As Adjudicacion, ByVal Anyo As Short, ByVal Gmn1 As String, ByVal codProce As Integer, ByVal idEmp As Short, ByVal codProve As String, ByVal codGrupo As String, ByVal idItem As Long, ByVal codUsu As String, ByVal centro As String) As Byte
        Dim Cantidad_Adj As Double
        Dim Cod_ERP As String = String.Empty
        Dim dtGrupo As New DataTable
        For Each EmpAdjudicada As FSNServer.EmpresaAdjudicada In oAdjudicacion.EmpresasAdjudicadas
            If EmpAdjudicada.Id = idEmp Then
                For Each ProvAdjudicado As FSNServer.ProveedorAdjudicado In EmpAdjudicada.ProveedoresAdjudicados
                    If ProvAdjudicado.Codigo = codProve Then
                        For Each oGrupo As ProveedorAdjudicado.Grupo In ProvAdjudicado.Grupos
                            If oGrupo.Codigo = codGrupo Then
                                dtGrupo.Columns.Add("ANYO")
                                dtGrupo.Columns.Add("GMN1")
                                dtGrupo.Columns.Add("PROCE")
                                dtGrupo.Columns.Add("ID_ITEM")
                                dtGrupo.Columns.Add("ID_EMP")
                                dtGrupo.Columns.Add("PROVE_PADRE")
                                dtGrupo.Columns.Add("PROVE")
                                dtGrupo.Columns.Add("CANT_ADJ")
                                dtGrupo.Columns.Add("CENTRO")
                                dtGrupo.Columns.Add("PROVE_ERP")
                                dtGrupo.Columns.Add("LIA_ID")
                                dtGrupo.Columns.Add("CODART")
                                Dim drItem As DataRow
                                For Each oItem As ProveedorAdjudicado.Item In oGrupo.Items
                                    If oItem.Id = idItem AndAlso oItem.Centro = centro Then
                                        If oItem.Distribuidores.Count > 0 Then
                                            For Each oDistr As ProveedorAdjudicado.Distribuidor In oItem.Distribuidores
                                                Dim ProvPrincipal As String = oItem.Distribuidores(0).Codigo
                                                drItem = dtGrupo.NewRow
                                                drItem("ANYO") = Anyo
                                                drItem("GMN1") = Gmn1
                                                drItem("PROCE") = codProce
                                                drItem("ID_EMP") = idEmp
                                                drItem("ID_ITEM") = oItem.Id
                                                drItem("PROVE_PADRE") = ProvPrincipal
                                                drItem("PROVE") = oDistr.Codigo
                                                drItem("CANT_ADJ") = oDistr.Cantidad_Adj
                                                drItem("CENTRO") = oItem.Centro
                                                drItem("CODART") = oItem.Codigo
                                                If oDistr.CodErpSeleccionado <> "" Then
                                                    Cod_ERP = oItem.CodERP
                                                ElseIf ProvAdjudicado.CodErpSeleccionado <> "" Then
                                                    Cod_ERP = ProvAdjudicado.CodErpSeleccionado
                                                End If
                                                drItem("PROVE_ERP") = Cod_ERP
                                                drItem("LIA_ID") = oItem.LIAId
                                                If oItem.Estado = 4 Then
                                                    dtGrupo.Rows.Add(drItem)
                                                End If
                                            Next
                                        Else
                                            If ProvAdjudicado.Distribuidores.Count > 0 Then
                                                For Each oDistrProv In ProvAdjudicado.Distribuidores
                                                    Dim ProvPrincipal As String = ProvAdjudicado.Distribuidores(0).Codigo
                                                    If oDistrProv.PorcentajeDistribuido > 0 Then
                                                        drItem = dtGrupo.NewRow
                                                        drItem("ANYO") = Anyo
                                                        drItem("GMN1") = Gmn1
                                                        drItem("PROCE") = codProce
                                                        drItem("ID_EMP") = idEmp
                                                        drItem("ID_ITEM") = oItem.Id
                                                        Cantidad_Adj = oItem.CantAdj * (oDistrProv.PorcentajeDistribuido / 100)
                                                        drItem("PROVE_PADRE") = ProvPrincipal
                                                        drItem("PROVE") = oDistrProv.Codigo
                                                        drItem("CANT_ADJ") = Cantidad_Adj
                                                        drItem("CENTRO") = oItem.Centro
                                                        If oItem.CodERP <> "" Then
                                                            Cod_ERP = oItem.CodERP
                                                        ElseIf ProvAdjudicado.CodErpSeleccionado <> "" Then
                                                            Cod_ERP = ProvAdjudicado.CodErpSeleccionado
                                                        End If
                                                        drItem("PROVE_ERP") = Cod_ERP
                                                        drItem("LIA_ID") = oItem.LIAId
                                                        drItem("CODART") = oItem.Codigo
                                                        If oItem.Estado = 4 Then
                                                            dtGrupo.Rows.Add(drItem)
                                                        End If
                                                    End If
                                                Next
                                            Else
                                                If oGrupo.CodErpSeleccionado <> "" Then
                                                    drItem = dtGrupo.NewRow
                                                    drItem("ANYO") = Anyo
                                                    drItem("GMN1") = Gmn1
                                                    drItem("PROCE") = codProce
                                                    drItem("ID_EMP") = idEmp
                                                    drItem("ID_ITEM") = oItem.Id
                                                    drItem("PROVE_PADRE") = codProve
                                                    drItem("PROVE") = codProve
                                                    drItem("CANT_ADJ") = oItem.CantAdj
                                                    drItem("CENTRO") = oItem.Centro
                                                    drItem("PROVE_ERP") = oGrupo.CodErpSeleccionado
                                                    drItem("LIA_ID") = oItem.LIAId
                                                    drItem("CODART") = oItem.Codigo
                                                    If oItem.Estado = 4 Then
                                                        dtGrupo.Rows.Add(drItem)
                                                    End If
                                                Else
                                                    drItem = dtGrupo.NewRow
                                                    drItem("ANYO") = Anyo
                                                    drItem("GMN1") = Gmn1
                                                    drItem("PROCE") = codProce
                                                    drItem("ID_EMP") = idEmp
                                                    drItem("ID_ITEM") = oItem.Id
                                                    drItem("PROVE_PADRE") = codProve
                                                    drItem("PROVE") = codProve
                                                    drItem("CANT_ADJ") = oItem.CantAdj
                                                    drItem("CENTRO") = oItem.Centro
                                                    drItem("PROVE_ERP") = oItem.CodERP
                                                    drItem("LIA_ID") = oItem.LIAId
                                                    drItem("CODART") = oItem.Codigo
                                                    If oItem.Estado = 4 Then
                                                        dtGrupo.Rows.Add(drItem)
                                                    End If
                                                End If
                                            End If

                                        End If
                                        Exit For
                                    End If
                                Next
                                Exit For
                            End If
                        Next
                        Exit For
                    End If
                Next
                Exit For
            End If
        Next

        Dim Retorno As Byte
        Retorno = DBServer.Adjudicaciones_BorrarDeERP(dtGrupo, codUsu)

        Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
        Dim oIntegracion As FSNServer.Integracion
        oIntegracion = FSNServer.Get_Object(GetType(FSNServer.Integracion))
        Dim ierp As Integer = oIntegracion.getErpFromEmpresa(idEmp)
        'Si la integraciÃ³n estÃ¡ activada llamamos a FSIS.
        If oIntegracion.IntegracionWCFActiva(TablasIntegracion.Adj, ierp) Then
            Dim lLogItemsAdj As List(Of Long)
            lLogItemsAdj = oIntegracion.ObtenerIdentificadoresBorradosADJ(Anyo, Gmn1, codProce, ierp)
            oIntegracion.LlamarFSISLista(TablasIntegracion.Adj, lLogItemsAdj, ierp)
        End If

        Return Retorno
    End Function
    Public Function ObtenerNombreMapper(ByVal sOrgCompras As String, ByVal idEmpresa As Short) As String
        Return DBServer.Adjudicaciones_ObtenerNombreMapper(sOrgCompras, idEmpresa)
    End Function
    ''' <summary>
    ''' Validacion segun Erp
    ''' </summary>
    ''' <param name="sNomMapper"></param>
    ''' <param name="idAtrib">Atributo</param>
    ''' <param name="sValor">Valor Atributo</param>
    ''' <param name="sIdioma">Idioma</param>
    ''' <returns>Respuesta a la Validacion</returns>
    ''' <remarks>Llamada desde: TraspasoAdjudicaciones.vb/Validacion_ERP; Tiempo mÃ¡ximo:0</remarks>
    Public Function Validacion_ERP(ByVal sNomMapper As String, ByVal idAtrib As String, ByVal sValor As String, ByVal sIdioma As String) As String
        Try

            Dim oMapper As Object

            oMapper = CreateObject(sNomMapper & ".clsValidacion")
            Dim sMensaje As String = oMapper.fStrValidarAtributo("", idAtrib, sValor, sIdioma)

            Return sMensaje
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' Cargar el Mensaje Error Integracion
    ''' </summary>
    ''' <param name="Id_LIA">LOG_GRAL.ID_TABLA</param>
    ''' <returns>Mensaje</returns>
    ''' <remarks>Llamada desde: TraspasoAdjudicaciones.vb/Devolver_MensajeErrorIntegracion; Tiempo mÃ¡ximo:0</remarks>
    Public Function Devolver_MensajeErrorIntegracion(ByVal Id_LIA As Long) As String
        Try

            Return DBServer.Adjudicaciones_Devolver_MensajeErrorIntegracion(Id_LIA)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
End Class
