﻿Public Class ProveedorAdjudicado
    Private _Codigo As String
    Public Property Codigo() As String
        Get
            Return _Codigo
        End Get
        Set(ByVal value As String)
            _Codigo = value
        End Set
    End Property
    Private _Denominacion As String
    Public Property Denominacion() As String
        Get
            Return _Denominacion
        End Get
        Set(ByVal value As String)
            _Denominacion = value
        End Set
    End Property
    Private _CodErpSeleccionado As String
    Public Property CodErpSeleccionado() As String
        Get
            Return _CodErpSeleccionado
        End Get
        Set(ByVal value As String)
            _CodErpSeleccionado = value
        End Set
    End Property
    Private _DenErpSeleccionado As String
    Public Property DenErpSeleccionado() As String
        Get
            Return _DenErpSeleccionado
        End Get
        Set(ByVal value As String)
            _DenErpSeleccionado = value
        End Set
    End Property
    Private _CodigosERP As New List(Of CodigoERP)
    Public Property CodigosERP() As List(Of CodigoERP)
        Get
            Return _CodigosERP
        End Get
        Set(ByVal value As List(Of CodigoERP))
            _CodigosERP = value
        End Set
    End Property
    Private _Atributos As New List(Of Atributo)
    Public Property Atributos() As List(Of Atributo)
        Get
            Return _Atributos
        End Get
        Set(ByVal value As List(Of Atributo))
            _Atributos = value
        End Set
    End Property
    Private _Distribuidores As New List(Of Distribuidor)
    Public Property Distribuidores() As List(Of Distribuidor)
        Get
            Return _Distribuidores
        End Get
        Set(ByVal value As List(Of Distribuidor))
            _Distribuidores = value
        End Set
    End Property
    Private _Grupos As New List(Of ProveedorAdjudicado.Grupo)
    Public Property Grupos() As List(Of ProveedorAdjudicado.Grupo)
        Get
            Return _Grupos
        End Get
        Set(ByVal value As List(Of ProveedorAdjudicado.Grupo))
            _Grupos = value
        End Set
    End Property
    Private _EstadoIntegracion As Short
    Public Property EstadoIntegracion() As Short
        Get
            Return _EstadoIntegracion
        End Get
        Set(ByVal value As Short)
            _EstadoIntegracion = value
        End Set
    End Property
    ''' <summary>Constructor por defecto</summary>    
    ''' <remarks></remarks>
    Public Sub New()
    End Sub
    ''' <summary>Constructor</summary>
    ''' <param name="Cod">Cod. proveedor</param>
    ''' <param name="Den">Denominación proveedor</param>    
    ''' <param name="CodERP">CodERP</param> 
    ''' <param name="DenErp">DenErpSeleccionado</param> 
    ''' <remarks></remarks>
    Public Sub New(ByVal Cod As String, ByVal Den As String, ByVal CodERP As String, ByVal DenErp As String)
        _Codigo = Cod
        _Denominacion = Den
        _CodErpSeleccionado = CodERP
        _DenErpSeleccionado = DenErp
        _Atributos = New List(Of Atributo)
        _Distribuidores = New List(Of Distribuidor)
        _Grupos = New List(Of Grupo)
    End Sub
#Region " CodigoERP "
    Public Class CodigoERP
        Private _CodigoERP As String
        Public Property CodigoERP() As String
            Get
                Return _CodigoERP
            End Get
            Set(ByVal value As String)
                _CodigoERP = value
            End Set
        End Property
        Private _DenERP As String
        Public Property DenERP() As String
            Get
                Return _DenERP
            End Get
            Set(ByVal value As String)
                _DenERP = value
            End Set
        End Property
        Private _Traspasado As Boolean
        Public Property Traspasado() As Boolean
            Get
                Return _Traspasado
            End Get
            Set(ByVal value As Boolean)
                _Traspasado = value
            End Set
        End Property
        Private _NumCamposPersonalizadosAct As Byte
        Public Property NumCamposPersonalizadosAct() As Byte
            Get
                Return _NumCamposPersonalizadosAct
            End Get
            Set(ByVal value As Byte)
                _NumCamposPersonalizadosAct = value
            End Set
        End Property
        Private _DenCampoPer1 As String
        Public Property DenCampoPer1() As String
            Get
                Return _DenCampoPer1
            End Get
            Set(ByVal value As String)
                _DenCampoPer1 = value
            End Set
        End Property
        Private _DatoCampoPer1 As String
        Public Property DatoCampoPer1() As String
            Get
                Return _DatoCampoPer1
            End Get
            Set(ByVal value As String)
                _DatoCampoPer1 = value
            End Set
        End Property
        Private _DenCampoPer2 As String
        Public Property DenCampoPer2() As String
            Get
                Return _DenCampoPer2
            End Get
            Set(ByVal value As String)
                _DenCampoPer2 = value
            End Set
        End Property
        Private _DatoCampoPer2 As String
        Public Property DatoCampoPer2() As String
            Get
                Return _DatoCampoPer2
            End Get
            Set(ByVal value As String)
                _DatoCampoPer2 = value
            End Set
        End Property
        Private _DenCampoPer3 As String
        Public Property DenCampoPer3() As String
            Get
                Return _DenCampoPer3
            End Get
            Set(ByVal value As String)
                _DenCampoPer3 = value
            End Set
        End Property
        Private _DatoCampoPer3 As String
        Public Property DatoCampoPer3() As String
            Get
                Return _DatoCampoPer3
            End Get
            Set(ByVal value As String)
                _DatoCampoPer3 = value
            End Set
        End Property
        Private _DenCampoPer4 As String
        Public Property DenCampoPer4() As String
            Get
                Return _DenCampoPer4
            End Get
            Set(ByVal value As String)
                _DenCampoPer4 = value
            End Set
        End Property
        Private _DatoCampoPer4 As String
        Public Property DatoCampoPer4() As String
            Get
                Return _DatoCampoPer4
            End Get
            Set(ByVal value As String)
                _DatoCampoPer4 = value
            End Set
        End Property
    End Class
#End Region
#Region " Distribuidor "
    Public Class Distribuidor
        Private _Id As Short
        Public Property Id() As Short
            Get
                Return _Id
            End Get
            Set(ByVal value As Short)
                _Id = value
            End Set
        End Property
        Private _Codigo As String
        Public Property Codigo() As String
            Get
                Return _Codigo
            End Get
            Set(ByVal value As String)
                _Codigo = value
            End Set
        End Property
        Private _Denominacion As String
        Public Property Denominacion() As String
            Get
                Return _Denominacion
            End Get
            Set(ByVal value As String)
                _Denominacion = value
            End Set
        End Property
        Private _PorcentajeDistribuido As Double
        Public Property PorcentajeDistribuido() As Double
            Get
                Return _PorcentajeDistribuido
            End Get
            Set(ByVal value As Double)
                _PorcentajeDistribuido = value
            End Set
        End Property
        Private _Cantidad_Adj As Double
        Public Property Cantidad_Adj() As Double
            Get
                Return _Cantidad_Adj
            End Get
            Set(ByVal value As Double)
                _Cantidad_Adj = value
            End Set
        End Property
        Private _CodigosERP As New List(Of CodigoERP)
        Public Property CodigosERP() As List(Of CodigoERP)
            Get
                Return _CodigosERP
            End Get
            Set(ByVal value As List(Of CodigoERP))
                _CodigosERP = value
            End Set
        End Property
        Private _CodErpSeleccionado As String
        Public Property CodErpSeleccionado() As String
            Get
                Return _CodErpSeleccionado
            End Get
            Set(ByVal value As String)
                _CodErpSeleccionado = value
            End Set
        End Property
        Private _DenErpSeleccionado As String
        Public Property DenErpSeleccionado() As String
            Get
                Return _DenErpSeleccionado
            End Get
            Set(ByVal value As String)
                _DenErpSeleccionado = value
            End Set
        End Property
        Public Sub New()

        End Sub
    End Class
#End Region
#Region " Atributo "
    Public Class Atributo
        Public Sub New()

        End Sub
        Private _Id As Long
        Public Property Id() As Long
            Get
                Id = _Id
            End Get
            Set(ByVal Value As Long)
                _Id = Value
            End Set
        End Property
        Private _Codigo As String
        Public Property Codigo() As String
            Get
                Return _Codigo
            End Get
            Set(ByVal value As String)
                _Codigo = value
            End Set
        End Property
        Private _Denominacion As String
        Public Property Denominacion() As String
            Get
                Return _Denominacion
            End Get
            Set(ByVal value As String)
                _Denominacion = value
            End Set
        End Property
        Private _Validacion_ERP As Boolean
        Public Property Validacion_ERP() As Boolean
            Get
                Return _Validacion_ERP
            End Get
            Set(ByVal value As Boolean)
                _Validacion_ERP = value
            End Set
        End Property
        Private _Valor As Object
        Public Property Valor() As Object
            Get
                If _Tipo = 3 Then
                    If _Valor Is DBNull.Value Or _Valor Is Nothing Then
                        Return _Valor
                    Else
                        Return CDate(_Valor)
                    End If
                Else
                    Return _Valor
                End If

            End Get
            Set(ByVal value As Object)
                If _Tipo = 3 AndAlso Not value Is DBNull.Value AndAlso Not _Valor Is Nothing Then
                    _Valor = CDate(value)
                Else
                    _Valor = value
                End If

            End Set
        End Property
        Private _Valores As List(Of ProveedorAdjudicado.ValorAtrib)
        Public Property Valores() As List(Of ProveedorAdjudicado.ValorAtrib)
            Get
                Return _Valores
            End Get
            Set(ByVal value As List(Of ProveedorAdjudicado.ValorAtrib))
                _Valores = value
            End Set
        End Property
        Private _Intro As Short
        Public Property Intro() As Short
            Get
                Intro = _Intro
            End Get
            Set(ByVal Value As Short)
                _Intro = Value
            End Set
        End Property
        Private _Tipo As TiposDeDatos.TipoAtributo
        Public Property Tipo() As TiposDeDatos.TipoAtributo
            Get
                Tipo = _Tipo
            End Get
            Set(ByVal Value As TiposDeDatos.TipoAtributo)
                _Tipo = Value
            End Set
        End Property
        Private _Obligatorio As Boolean
        Public Property Obligatorio() As Boolean
            Get
                Obligatorio = _Obligatorio
            End Get
            Set(ByVal Value As Boolean)
                _Obligatorio = Value
            End Set
        End Property
        Private _Orden As Short 'Indicara el orden del valor seleccionado si el atributo es de tipo lista
        Public Property Orden() As Short
            Get
                Orden = _Orden
            End Get
            Set(ByVal Value As Short)
                _Orden = Value
            End Set
        End Property
    End Class
#End Region
#Region " ValorAtrib "
    Public Class ValorAtrib
        Public Sub New()

        End Sub
        Private _Orden As Short
        Public Property Orden As Short
            Get
                Return _Orden
            End Get
            Set(ByVal value As Short)
                _Orden = value
            End Set
        End Property
        Private _Valor As Object
        Public Property Valor() As Object
            Get
                Return _Valor
            End Get
            Set(ByVal value As Object)
                _Valor = value
            End Set
        End Property
    End Class
#End Region
#Region " Grupo "
    Public Class Grupo
        Private _Anyo As Integer
        Public Property Anyo() As Integer
            Get
                Return _Anyo
            End Get
            Set(ByVal value As Integer)
                _Anyo = value
            End Set
        End Property
        Private _GMN1 As String
        Public Property GMN1() As String
            Get
                Return _GMN1
            End Get
            Set(ByVal value As String)
                _GMN1 = value
            End Set
        End Property
        Private _Proce As Integer
        Public Property Proce() As Integer
            Get
                Return _Proce
            End Get
            Set(ByVal value As Integer)
                _Proce = value
            End Set
        End Property
        Private _Id As Long
        Public Property Id() As Long
            Get
                Return _Id
            End Get
            Set(ByVal value As Long)
                _Id = value
            End Set
        End Property
        Private _Codigo As String
        Public Property Codigo() As String
            Get
                Return _Codigo
            End Get
            Set(ByVal value As String)
                _Codigo = value
            End Set
        End Property
        Private _Denominacion As String
        Public Property Denominacion() As String
            Get
                Return _Denominacion
            End Get
            Set(ByVal value As String)
                _Denominacion = value
            End Set
        End Property
        Private _CodErpSeleccionado As String
        Public Property CodErpSeleccionado() As String
            Get
                Return _CodErpSeleccionado
            End Get
            Set(ByVal value As String)
                _CodErpSeleccionado = value
            End Set
        End Property
        Private _DenErpSeleccionado As String
        Public Property DenErpSeleccionado() As String
            Get
                Return _DenErpSeleccionado
            End Get
            Set(ByVal value As String)
                _DenErpSeleccionado = value
            End Set
        End Property
        Private _TieneEscalados As Byte
        Public Property TieneEscalados() As Byte
            Get
                Return _TieneEscalados
            End Get
            Set(ByVal value As Byte)
                _TieneEscalados = value
            End Set
        End Property
        Private _Visto As Byte
        Public Property Visto() As Byte
            Get
                Return _Visto
            End Get
            Set(ByVal value As Byte)
                _Visto = value
            End Set
        End Property
        Private _Atributos As New List(Of Atributo)
        Public Property Atributos() As List(Of Atributo)
            Get
                Return _Atributos
            End Get
            Set(ByVal value As List(Of Atributo))
                _Atributos = value
            End Set
        End Property
        Private _AtributosProceso As New List(Of Atributo)
        Public Property AtributosProceso() As List(Of Atributo)
            Get
                Return _AtributosProceso
            End Get
            Set(ByVal value As List(Of Atributo))
                _AtributosProceso = value
            End Set
        End Property
        Private _Items As New List(Of ProveedorAdjudicado.Item)
        Public Property Items() As List(Of ProveedorAdjudicado.Item)
            Get
                Return _Items
            End Get
            Set(ByVal value As List(Of ProveedorAdjudicado.Item))
                _Items = value
            End Set
        End Property
        Private _Filtrado As Boolean
        Public Property Filtrado As Boolean
            Get
                Return _Filtrado
            End Get
            Set(value As Boolean)
                _Filtrado = value
            End Set
        End Property
        ''' <summary>Constructor por defecto</summary>    
        ''' <remarks></remarks>
        Public Sub New()
        End Sub
        ''' <summary>Constructor</summary>
        ''' <param name="Anyo">Anyo</param>
        ''' <param name="GMN1">GMN1</param>    
        ''' <param name="Proce">Proce</param> 
        ''' <param name="IdGrupo">Id. grupo</param>
        ''' <param name="CodGrupo">Cod. grupo</param>    
        ''' <param name="DenGrupo">Den. grupo</param> 
        ''' <param name="bTieneEscalados">Indica si el grupo tiene escalados</param> 
        ''' <remarks></remarks>
        Public Sub New(ByVal Anyo As Integer, ByVal GMN1 As String, ByVal Proce As Integer, ByVal IdGrupo As Integer, ByVal CodGrupo As String, ByVal DenGrupo As String, ByVal bTieneEscalados As Boolean)
            _Anyo = Anyo
            _GMN1 = GMN1
            _Proce = Proce
            _Atributos = New List(Of Atributo)
            _AtributosProceso = New List(Of Atributo)
            _Items = New List(Of Item)
            _Id = IdGrupo
            _Codigo = CodGrupo
            _Denominacion = DenGrupo
            _Visto = 0
            _TieneEscalados = bTieneEscalados
        End Sub
    End Class
#End Region
#Region " Item "
    Public Class Item
        Private _Id As Long
        Public Property Id() As Long
            Get
                Return _Id
            End Get
            Set(ByVal value As Long)
                _Id = value
            End Set
        End Property
        Private _LIAId As Long
        Public Property LIAId() As Long
            Get
                Return _LIAId
            End Get
            Set(ByVal value As Long)
                _LIAId = value
            End Set
        End Property
        Private _Codigo As String
        Public Property Codigo() As String
            Get
                Return _Codigo
            End Get
            Set(ByVal value As String)
                _Codigo = value
            End Set
        End Property
        Private _Denominacion As String
        Public Property Denominacion() As String
            Get
                Return _Denominacion
            End Get
            Set(ByVal value As String)
                _Denominacion = value
            End Set
        End Property
        Private _Estado As Short
        Public Property Estado() As Short
            Get
                Return _Estado
            End Get
            Set(ByVal value As Short)
                _Estado = value
            End Set
        End Property
        Private _CantAdj As Double
        Public Property CantAdj() As Double
            Get
                Return _CantAdj
            End Get
            Set(ByVal value As Double)
                _CantAdj = value
            End Set
        End Property
        Private _PrecUni As Double
        Public Property PrecUni() As Double
            Get
                Return _PrecUni
            End Get
            Set(ByVal value As Double)
                _PrecUni = value
            End Set
        End Property
        Private _Unidad As String
        Public Property Unidad() As String
            Get
                Return _Unidad
            End Get
            Set(ByVal value As String)
                _Unidad = value
            End Set
        End Property
        Private _FecIniSum As String
        Public Property FecIniSum() As String
            Get
                Return _FecIniSum
            End Get
            Set(ByVal value As String)
                _FecIniSum = value
            End Set
        End Property
        Private _FecFinSum As String
        Public Property FecFinSum() As String
            Get
                Return _FecFinSum
            End Get
            Set(ByVal value As String)
                _FecFinSum = value
            End Set
        End Property
        Private _Centro As String
        Public Property Centro() As String
            Get
                Return _Centro
            End Get
            Set(ByVal value As String)
                _Centro = value
            End Set
        End Property
        Private _CodERP As String
        Public Property CodERP() As String
            Get
                Return _CodERP
            End Get
            Set(ByVal value As String)
                _CodERP = value
            End Set
        End Property
        Private _DenERP As String
        Public Property DenERP() As String
            Get
                Return _DenERP
            End Get
            Set(ByVal value As String)
                _DenERP = value
            End Set
        End Property
        Private _TieneEscalados As Byte
        Public Property TieneEscalados() As Byte
            Get
                Return _TieneEscalados
            End Get
            Set(ByVal value As Byte)
                _TieneEscalados = value
            End Set
        End Property
        Private _Escalados As New List(Of ProveedorAdjudicado.Escalado)
        Public Property Escalados() As List(Of ProveedorAdjudicado.Escalado)
            Get
                Return _Escalados
            End Get
            Set(ByVal value As List(Of ProveedorAdjudicado.Escalado))
                _Escalados = value
            End Set
        End Property
        Private _Distribuidores As New List(Of ProveedorAdjudicado.Distribuidor)
        Public Property Distribuidores() As List(Of ProveedorAdjudicado.Distribuidor)
            Get
                Return _Distribuidores
            End Get
            Set(ByVal value As List(Of ProveedorAdjudicado.Distribuidor))
                _Distribuidores = value
            End Set
        End Property
        Private _Atributos As New List(Of ProveedorAdjudicado.Atributo)
        Public Property Atributos() As List(Of ProveedorAdjudicado.Atributo)
            Get
                Return _Atributos
            End Get
            Set(ByVal value As List(Of ProveedorAdjudicado.Atributo))
                _Atributos = value
            End Set
        End Property
        Private _Visto As Byte
        Public Property Visto() As Byte
            Get
                Return _Visto
            End Get
            Set(ByVal value As Byte)
                _Visto = value
            End Set
        End Property
        ''' <summary>Constructor por defecto</summary>    
        ''' <remarks></remarks>
        Public Sub New()
        End Sub
        ''' <summary>Constructor</summary>
        ''' <param name="Id">Id del ítem</param>
        ''' <param name="Estado">Estado de integración</param>    
        ''' <param name="CodArt">Cod. artículo</param> 
        ''' <param name="DenArt">Den. artículo</param>
        ''' <param name="PrecAdj">Precio</param>    
        ''' <param name="CantAdj">Cantidad</param> 
        ''' <param name="Centro">Centro</param> 
        ''' <param name="FecIniSum">Fecha inicio suministro</param> 
        ''' <param name="FecFinSum">Fecha fin suministro</param> 
        ''' <param name="CodERP">Cod. ERP</param> 
        ''' <param name="DenERP">Den. ERP</param> 
        ''' <param name="Unidad">Unidad</param> 
        ''' <param name="LIAId">Id en LOG_ITEM_ADJ</param> 
        ''' <param name="TieneEscalados">Indica si el ítem tiene escalados</param> 
        ''' <remarks></remarks>
        Public Sub New(ByVal Id As Integer, ByVal Estado As Short, ByVal CodArt As String, ByVal DenArt As String, ByVal PrecAdj As Double, ByVal CantAdj As Double, ByVal Centro As String, _
                       ByVal FecIniSum As String, ByVal FecFinSum As String, ByVal CodERP As String, ByVal DenERP As String, ByVal Unidad As String, ByVal LIAId As Long, ByVal TieneEscalados As Boolean)
            _Id = Id
            _Estado = Estado
            _Codigo = CodArt
            _Denominacion = DenArt
            _PrecUni = PrecAdj
            _CantAdj = CantAdj
            _Centro = Centro
            _FecIniSum = FecIniSum
            _FecFinSum = FecIniSum
            _CodERP = CodERP
            _DenERP = DenERP
            _Unidad = Unidad
            _LIAId = LIAId
            _TieneEscalados = TieneEscalados
        End Sub
    End Class
#End Region
#Region " Escalado "
    Public Class Escalado
        Private _Id As Long
        Public Property Id() As Long
            Get
                Return _Id
            End Get
            Set(ByVal value As Long)
                _Id = value
            End Set
        End Property
        Private _Inicio As Double
        Public Property Inicio() As Double
            Get
                Return _Inicio
            End Get
            Set(ByVal value As Double)
                _Inicio = value
            End Set
        End Property
        Private _Fin As Double
        Public Property Fin() As Double
            Get
                Return _Fin
            End Get
            Set(ByVal value As Double)
                _Fin = value
            End Set
        End Property
        Private _Precio As String
        Public Property Precio() As String
            Get
                Return _Precio
            End Get
            Set(ByVal value As String)
                _Precio = value
            End Set
        End Property
        Private _Rango As Byte
        Public Property Rango() As Byte
            Get
                Return _Rango
            End Get
            Set(ByVal value As Byte)
                _Rango = value
            End Set
        End Property
        Private _Unidad As String
        Public Property Unidad() As String
            Get
                Return _Unidad
            End Get
            Set(ByVal value As String)
                _Unidad = value
            End Set
        End Property
        Private _Moneda As String
        Public Property Moneda() As String
            Get
                Return _Moneda
            End Get
            Set(ByVal value As String)
                _Moneda = value
            End Set
        End Property
    End Class
#End Region
End Class