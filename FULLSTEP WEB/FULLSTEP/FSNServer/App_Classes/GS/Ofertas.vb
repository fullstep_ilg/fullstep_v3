﻿Imports System.Configuration
<Serializable()> _
Public Class Ofertas
    Inherits Security

    Private msNomAdjunto As String
    Private mlDataSize As Long
    Private miAnyo As Integer
    Private msGMN As String
    Private miProce As Integer
    Private msProve As String
    Private mlId As Long
    Property Anyo() As Integer
        Get
            Anyo = miAnyo
        End Get
        Set(ByVal Value As Integer)
            miAnyo = Value
        End Set
    End Property
    Property GMN() As String
        Get
            GMN = msGMN
        End Get
        Set(ByVal Value As String)
            msGMN = Value
        End Set
    End Property
    Property Proce() As Integer
        Get
            Proce = miProce
        End Get
        Set(ByVal Value As Integer)
            miProce = Value
        End Set
    End Property
    Property Proveedor() As String
        Get
            Proveedor = msProve
        End Get
        Set(ByVal Value As String)
            msProve = Value
        End Set
    End Property
    Property Id() As Long
        Get
            Id = mlId
        End Get
        Set(ByVal Value As Long)
            mlId = Value
        End Set
    End Property
    Property NomAdjunto() As String
        Get
            NomAdjunto = msNomAdjunto
        End Get
        Set(ByVal Value As String)
            msNomAdjunto = Value
        End Set
    End Property
    Property DataSize()
        Get
            DataSize = mlDataSize
        End Get
        Set(ByVal Value)
            mlDataSize = Value
        End Set
    End Property
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
    ''' <summary>
    ''' Devuelve los archivos adjuntso de toda la oferta en los tres ámbitos
    ''' </summary>
    ''' <returns>Devuelve un dataset con los adjuntos</returns>
    ''' <remarks>llamada desde downloadAdjuntosZip.aspx.vb</remarks>
    Public Function ObtenerAdjuntos() As DataSet
        Authenticate()
        Dim data As DataSet = DBServer.Ofertas_ObtenerAdjuntos(miAnyo, msGMN, miProce, msProve, mlId)
        Return data
    End Function
    Public Sub TransferirAdjuntoDePortal(ByVal iTipo As Integer, ByVal iId As Integer, ByVal iIdPortal As Integer)
        Authenticate()
        DBServer.Ofertas_TransferirAdjuntoDePortal(iTipo, iId, iIdPortal)
    End Sub
    ''' <summary>
    ''' Salva los ficheros en un directorio temporal
    ''' </summary>
    ''' <param name="sPath">Ruta donde descargar los ficheros</param>
    ''' <param name="iId">ID del Adjunto</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function SaveAdjunToDiskZip(ByVal sPath As String, ByVal iId As Integer) As String
        Dim sFolderPath As String
        If Not System.IO.Directory.Exists(sPath) Then System.IO.Directory.CreateDirectory(sPath)

        sFolderPath = sPath
        sPath += "\" + msNomAdjunto
        Dim sFileName As String = System.IO.Path.GetFileNameWithoutExtension(sPath)
        Dim sExtension As String = System.IO.Path.GetExtension(sPath)
        'Se comprueba si ya existe el fichero ante la posibilidad de que adjunten 2 veces un fichero con el mismo nombre
        If System.IO.File.Exists(sPath) Then
            Dim ind As Byte = 1
            While System.IO.File.Exists(sFolderPath & "\" & sFileName & "_" & ind.ToString & sExtension)
                ind += 1
            End While

            sPath = sFolderPath & "\" & sFileName & "_" & ind.ToString & sExtension
        End If

        Dim byteBuffer() As Byte
        byteBuffer = Me.LeerAdjuntoOferta(iId)
        System.IO.File.WriteAllBytes(sPath, byteBuffer)

        Return sPath
    End Function
    Public Function LeerAdjuntoOferta(ByVal Id As Long, Optional ByVal ChunkNumber As Long = 0, Optional ByVal ChunkSize As Long = 0) As Byte()
        Authenticate()
        Return DBServer.Ofertas_LeerAdjuntoOferta(Id, ChunkNumber, ChunkSize)
    End Function
End Class
