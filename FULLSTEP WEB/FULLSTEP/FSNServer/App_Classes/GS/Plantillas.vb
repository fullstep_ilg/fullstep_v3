﻿Imports System.Configuration
Public Class Plantillas
    Inherits Security
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
    Public Function GS_Plantillas_Visor(ByVal Idioma As String) As DataSet
        Authenticate()
        Dim ds As DataSet
        Dim uon As String
        ds = DBServer.GS_Plantillas_Visor(Idioma)
        For Each row As DataRow In ds.Tables("PLANTILLAS").Rows
            For Each child As DataRow In row.GetChildRows("REL_ID_CUSTOM_TEMPLATE")
                uon = IIf(IsDBNull(child("UON1")), "", child("UON1") & "-") & _
                            IIf(IsDBNull(child("UON2")), "", child("UON2") & "-") & _
                            IIf(IsDBNull(child("UON3")), "", child("UON3") & "-") & child("DEN").ToString
                row("UONS") = IIf(row("UONS") = String.Empty, uon, row("UONS") & "," & uon)
            Next
        Next
        Return ds
    End Function
    Public Function GS_Visor_Plantillas_DetallePlantilla(ByVal Id As Integer, ByVal Idioma As String) As InfoPlantilla
        Authenticate()
        Dim Textos As DataTable
        Dim oDict As Dictionary = New Dictionary(DBServer, mIsAuthenticated)
        oDict.LoadData(TiposDeDatos.ModulosIdiomas.VisorPlantillasGS, Idioma)
        Textos = oDict.Data.Tables(0)
        Dim dsDatos As DataSet = DBServer.GS_Visor_Plantillas_DetallePlantilla(Id, Idioma)
        Dim oPlantilla As New InfoPlantilla
        With oPlantilla
            .menuData(MenuPlantillaGS.Reuniones) = Textos(18)(1)
            .documentoData(DocumentoPlantillaGS.Agenda) = Textos(19)(1)
            .documentoData(DocumentoPlantillaGS.Acta) = Textos(20)(1)
            .formatoData(FormatoPlantillaGS.DOC) = Textos(30)(1)
            .formatoData(FormatoPlantillaGS.DOCX) = Textos(31)(1)
            .formatoData(FormatoPlantillaGS.PDF) = Textos(32)(1)
            .formatoData(FormatoPlantillaGS.ODT) = Textos(33)(1)
            .formatoData(FormatoPlantillaGS.EPUB) = Textos(34)(1)
            .vistaData(VistaPlantillaGS.ActaProveedor) = Textos(21)(1)
            .vistaData(VistaPlantillaGS.ActaItem) = Textos(22)(1)
            Dim infoIdioma As Plantilla
            For Each row As DataRow In dsDatos.Tables("IDIOMAS").Rows
                infoIdioma = New Plantilla
                infoIdioma.Idioma = row("COD").ToString
                infoIdioma.DenominacionIdioma = row("DEN").ToString
                infoIdioma.IdiomaDefecto = (row("COD").ToString = Idioma)
                infoIdioma.Comentario = ""
                infoIdioma.Denominacion = ""
                infoIdioma.NombreArchivo = ""
                infoIdioma.imgUrl = ConfigurationManager.AppSettings("ruta") & "images/" & row("LANGUAGETAG") & ".png"
                .languageInfo(row("COD")) = infoIdioma
            Next
            If Not Id = 0 Then
                .Id = Id
                .Menu = dsDatos.Tables(1).Rows(0)("MENU")
                .Documento = dsDatos.Tables(1).Rows(0)("DOCUMENTO")
                .PlantillaDefecto = dsDatos.Tables(1).Rows(0)("DEFECTO")
                .FormatoDefecto = dsDatos.Tables(1).Rows(0)("FORMATO")
                .Vista = DBNullToInteger(dsDatos.Tables(1).Rows(0)("VISTA"))
                .Per = dsDatos.Tables(1).Rows(0)("PER")
                For Each row As DataRow In dsDatos.Tables(2).Rows
                    .languageInfo(row("IDIOMA")).Comentario = row("COMMENT")
                    .languageInfo(row("IDIOMA")).Denominacion = row("DEN")
                    .languageInfo(row("IDIOMA")).NombreArchivo = row("NOM")
                    .languageInfo(row("IDIOMA")).size = row("SIZE")
                    Select Case Split(row("NOM"), ".")(Split(row("NOM"), ".").Length - 1)
                        Case "doc", "docx"
                            .languageInfo(row("IDIOMA")).tipoadjunto = 12
                            .languageInfo(row("IDIOMA")).thumbnail_url = ConfigurationManager.AppSettings("ruta") & "images/wordAttach.png"
                        Case "pdf"
                            .languageInfo(row("IDIOMA")).tipoadjunto = 13
                            .languageInfo(row("IDIOMA")).thumbnail_url = ConfigurationManager.AppSettings("ruta") & "images/pdfAttach.png"
                        Case "odt"
                            .languageInfo(row("IDIOMA")).tipoadjunto = 14
                            .languageInfo(row("IDIOMA")).thumbnail_url = ConfigurationManager.AppSettings("ruta") & "images/odtAttach.png"
                        Case "epub"
                            .languageInfo(row("IDIOMA")).tipoadjunto = 15
                            .languageInfo(row("IDIOMA")).thumbnail_url = ConfigurationManager.AppSettings("ruta") & "images/epubAttach.png"
                        Case Else
                            .languageInfo(row("IDIOMA")).tipoadjunto = 10
                            .languageInfo(row("IDIOMA")).thumbnail_url = ConfigurationManager.AppSettings("ruta") & "images/Attach.png"
                    End Select
                Next
                Dim iUON As cn_fsItem
                For Each row As DataRow In dsDatos.Tables(3).Rows
                    iUON = New cn_fsItem
                    With iUON
                        .value = "itemUONPlantilla" & _
                            IIf(IsDBNull(row("UON1")), "", "-UON1_" & row("UON1")) & _
                            IIf(IsDBNull(row("UON2")), "", "-UON2_" & row("UON2")) & _
                            IIf(IsDBNull(row("UON3")), "", "-UON3_" & row("UON3")) 
                        .text = IIf(IsDBNull(row("UON1")), "", row("UON1") & "-") & _
                            IIf(IsDBNull(row("UON2")), "", row("UON2") & "-") & _
                            IIf(IsDBNull(row("UON3")), "", row("UON3") & "-") & row("DEN").ToString
                    End With
                    .UONsPlantilla.Add(iUON)
                Next
            End If
        End With
        Return oPlantilla
    End Function
    Public Sub GS_Plantilla_Alta(ByVal oInfoPlantilla As InfoPlantilla, ByVal dtUONsPlantilla As DataTable)
        Dim oPlantilla As Plantilla
        Dim dtAdjuntos As New DataTable
        Dim rAdjunto As DataRow
        dtAdjuntos.Columns.Add("IDIOMA", GetType(System.String))
        dtAdjuntos.Columns.Add("NOMBRE", GetType(System.String))
        dtAdjuntos.Columns.Add("DENOMINACION", GetType(System.String))
        dtAdjuntos.Columns.Add("COMENTARIOS", GetType(System.String))
        dtAdjuntos.Columns.Add("PATH", GetType(System.String))
        dtAdjuntos.Columns.Add("SIZE", GetType(System.Int64))

        For Each item As KeyValuePair(Of String, Plantilla) In oInfoPlantilla.languageInfo
            rAdjunto = dtAdjuntos.NewRow
            rAdjunto("IDIOMA") = item.Key
            oPlantilla = CType(item.Value, Plantilla)
            rAdjunto("NOMBRE") = oPlantilla.NombreArchivo
            rAdjunto("DENOMINACION") = oPlantilla.Denominacion
            rAdjunto("COMENTARIOS") = oPlantilla.Comentario
            rAdjunto("PATH") = oPlantilla.path
            Dim input As System.IO.FileStream = New System.IO.FileStream(oPlantilla.path, System.IO.FileMode.Open, System.IO.FileAccess.Read)
            rAdjunto("SIZE") = input.Length
            dtAdjuntos.Rows.Add(rAdjunto)
        Next
        With oInfoPlantilla
            DBServer.GS_Plantilla_Alta(.Per, .PlantillaDefecto, .FormatoDefecto, .Documento, _
                                       IIf(.Documento = DocumentoPlantillaGS.Agenda, 0, .Vista), dtUONsPlantilla, dtAdjuntos)
        End With
    End Sub
    Public Function GS_Plantillas_GetAdjuntoPlantilla(ByVal IdPlantilla As Integer, ByVal Idioma As String) As Byte()
        Authenticate()
        Return DBServer.GS_Plantillas_GetAdjuntoPlantilla(IdPlantilla, Idioma)
    End Function
    Public Sub GS_Plantilla_Editar(ByVal oInfoPlantilla As InfoPlantilla, ByVal dtUONsPlantilla As DataTable)
        Authenticate()
        Dim oPlantilla As Plantilla
        Dim dtAdjuntos As New DataTable
        Dim rAdjunto As DataRow
        dtAdjuntos.Columns.Add("IDIOMA", GetType(System.String))
        dtAdjuntos.Columns.Add("NOMBRE", GetType(System.String))
        dtAdjuntos.Columns.Add("DENOMINACION", GetType(System.String))
        dtAdjuntos.Columns.Add("COMENTARIOS", GetType(System.String))
        dtAdjuntos.Columns.Add("PATH", GetType(System.String))
        dtAdjuntos.Columns.Add("SIZE", GetType(System.Int64))

        For Each item As KeyValuePair(Of String, Plantilla) In oInfoPlantilla.languageInfo
            rAdjunto = dtAdjuntos.NewRow
            rAdjunto("IDIOMA") = item.Key
            oPlantilla = CType(item.Value, Plantilla)
            rAdjunto("NOMBRE") = oPlantilla.NombreArchivo
            rAdjunto("DENOMINACION") = oPlantilla.Denominacion
            rAdjunto("COMENTARIOS") = oPlantilla.Comentario
            rAdjunto("PATH") = oPlantilla.path
            If oPlantilla.path = String.Empty Then
                rAdjunto("SIZE") = oPlantilla.size
            Else
                Dim input As System.IO.FileStream = New System.IO.FileStream(oPlantilla.path, System.IO.FileMode.Open, System.IO.FileAccess.Read)
                rAdjunto("SIZE") = input.Length
            End If
            dtAdjuntos.Rows.Add(rAdjunto)
        Next
        With oInfoPlantilla
            DBServer.GS_Plantilla_Editar(.Id, .Per, .PlantillaDefecto, .FormatoDefecto, .Documento, _
                                         IIf(.Documento = DocumentoPlantillaGS.Agenda, 0, .Vista), dtUONsPlantilla, dtAdjuntos)
        End With
    End Sub
    Public Sub GS_Plantilla_Eliminar(ByVal idsPlantilla As DataTable)
        Authenticate()
        DBServer.GS_Plantilla_Eliminar(idsPlantilla)
    End Sub
    Public Sub GS_Plantilla_Defecto(ByVal idPlantilla As Integer)
        Authenticate()
        DBServer.GS_Plantilla_Defecto(idPlantilla)
    End Sub
    Public Function GS_Obtener_UONs(ByVal Idioma As String) As List(Of cn_fsTreeViewItem)
        Authenticate()
        Try
            Dim dsDatos As New DataSet
            dsDatos = DBServer.GS_Plantillas_UONs(Idioma)

            Dim oUONPara As New List(Of cn_fsTreeViewItem)
            Dim item0, item1, item2, item3 As cn_fsTreeViewItem
            ' UON0 - LA EMPRESA
            item0 = New cn_fsTreeViewItem
            With item0
                .value = dsDatos.Tables(0).Rows(0)("COD")
                .text = dsDatos.Tables(0).Rows(0)("DEN")
                .selectable = dsDatos.Tables(0).Rows(0)("PERMISO")
                .type = IIf(.selectable, 3, 0)
                .nivel = "UON0"
                .expanded = True
                'UON1
                .children = New List(Of cn_fsTreeViewItem)
                With .children
                    For Each row1 As DataRow In dsDatos.Tables(1).Rows
                        item1 = New cn_fsTreeViewItem
                        With item1
                            .value = row1("COD")
                            .text = row1("COD") & " - " & row1("DEN")
                            .selectable = row1("PERMISO")
                            .type = IIf(.selectable, 3, 0)
                            .nivel = "UON1"
                            .expanded = False
                            'UON2
                            .children = New List(Of cn_fsTreeViewItem)
                            With .children
                                For Each row2 As DataRow In dsDatos.Tables(2).Select("UON1='" & row1("COD") & "'")
                                    item2 = New cn_fsTreeViewItem
                                    With item2
                                        .value = row2("COD")
                                        .text = row2("COD") & " - " & row2("DEN")
                                        .selectable = row2("PERMISO")
                                        .type = IIf(.selectable, 3, 0)
                                        .nivel = "UON2"
                                        .expanded = False
                                        'UON3
                                        .children = New List(Of cn_fsTreeViewItem)
                                        With .children
                                            For Each row3 As DataRow In dsDatos.Tables(3).Select("UON1='" & row1("COD") & "' AND UON2='" & row2("COD") & "'")
                                                item3 = New cn_fsTreeViewItem
                                                With item3
                                                    .value = row3("COD")
                                                    .text = row3("COD") & " - " & row3("DEN")
                                                    .selectable = row3("PERMISO")
                                                    .type = IIf(.selectable, 3, 0)
                                                    .nivel = "UON3"
                                                    .expanded = False
                                                End With
                                                .Add(item3)
                                            Next
                                        End With
                                    End With
                                    .Add(item2)
                                Next
                            End With
                        End With
                        .Add(item1)
                    Next
                End With
            End With
            oUONPara.Add(item0)
            Return oUONPara
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    Public Function GS_Obtener_Plantillas_Usu_Documento(ByVal Usu As String, ByVal Documento As DocumentoPlantillaGS, ByVal Idioma As String, _
            ByVal UON1 As String, ByVal UON2 As String, ByVal UON3 As String) As List(Of InfoPlantilla)
        Authenticate()
        Try
            Dim dsDatos As New DataSet
            dsDatos = DBServer.GS_Obtener_Plantillas_Usu_Documento(Usu, Documento, Idioma, UON1, UON2, UON3)
            Dim plantillas As New List(Of InfoPlantilla)
            Dim plantilla As InfoPlantilla
            Dim infoIdioma As Plantilla
            For Each row As DataRow In dsDatos.Tables(0).Rows
                plantilla = New InfoPlantilla
                With plantilla
                    .Id = row("ID")
                    .Vista = DBNullToInteger(row("VISTA"))
                    .PlantillaDefecto = DBNullToBoolean(row("DEFECTO"))
                    .FormatoDefecto = DBNullToInteger(row("FORMATO"))
                    infoIdioma = New Plantilla
                    infoIdioma.NombreArchivo = DBNullToStr(row("NOM"))
                    .languageInfo.Add(Idioma, infoIdioma)
                End With
                plantillas.Add(plantilla)
            Next
            Return plantillas
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    Public Sub GS_Plantilla_Defecto_Usu(ByVal Usu As String, ByVal idPlantilla As Integer, ByVal formato As Integer)
        Authenticate()
        DBServer.GS_Plantilla_Defecto_Usu(Usu, idPlantilla, formato)
    End Sub
    Public Sub GS_Plantilla_FormatoDefecto(ByVal idsPlantilla As DataTable, ByVal formato As Integer)
        Authenticate()
        DBServer.GS_Plantilla_FormatoDefecto(idsPlantilla, formato)
    End Sub
End Class
