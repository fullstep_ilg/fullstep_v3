﻿Public Class Reuniones
    Inherits Security
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
    ''' <summary>
    ''' Carga los datos de la reunión
    ''' </summary>
    ''' <param name="fecha">fecha de la reunión</param>
    ''' <param name="Idioma">Idioma en el q ver el impreso</param>
    ''' <returns>datos de la reunión</returns>
    ''' <remarks>Llamada desde: agendaExport.aspx.vb/Page_Load ; Tiempo máximo:0,2</remarks>
    Public Function Reunion_Agenda(ByVal fecha As DateTime, ByVal Idioma As String) As DataSet
        Authenticate()
        Dim dsDatosProceso As DataSet = DBServer.GS_Reunion_Agenda(fecha, Idioma)
        Dim view0 As New DataView(dsDatosProceso.Tables("DATOSGRUPOS"))
        Dim dt0 As DataTable = view0.ToTable(True, "ID", "ANYO", "GMN1", "PROCE", "COD_GRUPO", "DEN_GRUPO", "COD_DEST_GR", "DEN_DEST_GR", _
                "COD_PAGO_GR", "DEN_PAGO_GR", "FINI_SUM_GR", "FFIN_SUM_GR")
        dt0.TableName = "GRUPO"
        dsDatosProceso.Tables.Add(dt0)

        dsDatosProceso.Tables("PROCESO").TableName = "PROCESO_OLD"
        view0 = New DataView(dsDatosProceso.Tables("PROCESO_OLD"))
        dt0 = view0.ToTable(True, "ANYO", "GMN1", "COD", "HORA_PROCESO", "COD_PROCESO", "DEN_PROCESO", "PRES_TOTAL", "UON1_APER", "UON2_APER", "UON3_APER", _
                            "UON_COD_PROCESO", "UON1_APER_DEN", "UON2_APER_DEN", "UON3_APER_DEN", "UON_DEN_PROCESO", "VOL_PROCESO", "MON_PROCESO", "COMENT_PROCESO", _
                            "FECAPER", "FECLIMOFE", "FECPRES", "FECNEC", "NOM_REFERENCIA", "REFERENCIA", "MONEDA", "CAMBIO", "TIPO_REUNION", "DEFDEST", "COD_DEST_PROCE", _
                            "DEN_DEST_PROCE", "DEFPAG", "COD_PAGO_PROCE", "DEN_PAGO_PROCE", "DEFFECSUM", "FINI_SUM_PROCE", "FFIN_SUM_PROCE", "COD_RESPONSABLE", _
                            "NOM_RESPONSABLE", "APE_RESPONSABLE", "DEQP_RESPONSABLE", "EQP_RESPONSABLE", "TLFNO_RESPONSABLE", "MAIL_RESPONSABLE", "FAX_RESPONSABLE", _
                            "DEST_COD", "DEST_DEN", "DEST_DIR", "DEST_POB", "DEST_CP", "DEST_PROV", "DEST_PAIS", "PAGO_COD", "PAGO_DEN")
        dt0.TableName = "PROCESO"
        Dim NumProceso As New DataColumn("NUM_PROCESO", GetType(System.Int64))
        dt0.Columns.Add(NumProceso)
        For i As Integer = 0 To dt0.Rows.Count - 1
            dt0.Rows(i)("NUM_PROCESO") = i + 1
        Next
        dsDatosProceso.Tables.Add(dt0)
        dt0 = view0.ToTable(True, "ANYO", "GMN1", "COD", "MAT1_PROCESO", "MAT2_PROCESO", "MAT3_PROCESO", "MAT4_PROCESO", "DMAT4_PROCESO", "DMAT3_PROCESO", "DMAT2_PROCESO", "DMAT1_PROCESO")
        dt0.TableName = "MAT_PROCESO"
        dsDatosProceso.Tables.Add(dt0)
        dsDatosProceso.Tables.Remove("PROCESO_OLD")
        Dim parentCols(2) As DataColumn
        Dim childCols(2) As DataColumn
        parentCols(0) = dsDatosProceso.Tables("PROCESO").Columns("ANYO")
        parentCols(1) = dsDatosProceso.Tables("PROCESO").Columns("GMN1")
        parentCols(2) = dsDatosProceso.Tables("PROCESO").Columns("COD")
        childCols(0) = dsDatosProceso.Tables("MAT_PROCESO").Columns("ANYO")
        childCols(1) = dsDatosProceso.Tables("MAT_PROCESO").Columns("GMN1")
        childCols(2) = dsDatosProceso.Tables("MAT_PROCESO").Columns("COD")
        dsDatosProceso.Relations.Add("PROCE_MAT_PROCESO", parentCols, childCols, False)

        dsDatosProceso.Tables("PROCESO").Columns.Add("MAT1_PROCESO", GetType(System.String))
        dsDatosProceso.Tables("PROCESO").Columns.Add("DMAT1_PROCESO", GetType(System.String))
        dsDatosProceso.Tables("PROCESO").Columns.Add("MAT2_PROCESO", GetType(System.String))
        dsDatosProceso.Tables("PROCESO").Columns.Add("DMAT2_PROCESO", GetType(System.String))
        dsDatosProceso.Tables("PROCESO").Columns.Add("MAT3_PROCESO", GetType(System.String))
        dsDatosProceso.Tables("PROCESO").Columns.Add("DMAT3_PROCESO", GetType(System.String))
        dsDatosProceso.Tables("PROCESO").Columns.Add("MAT4_PROCESO", GetType(System.String))
        dsDatosProceso.Tables("PROCESO").Columns.Add("DMAT4_PROCESO", GetType(System.String))
        Dim mat As DataRow
        For Each row As DataRow In dsDatosProceso.Tables("PROCESO").Rows
            If Not row.GetChildRows("PROCE_MAT_PROCESO").Length = 0 Then
                mat = row.GetChildRows("PROCE_MAT_PROCESO")(0)
                row("MAT1_PROCESO") = mat("MAT1_PROCESO")
                row("MAT2_PROCESO") = mat("MAT2_PROCESO")
                row("MAT3_PROCESO") = mat("MAT3_PROCESO")
                row("MAT4_PROCESO") = mat("MAT4_PROCESO")
                row("DMAT1_PROCESO") = mat("DMAT1_PROCESO")
                row("DMAT2_PROCESO") = mat("DMAT2_PROCESO")
                row("DMAT3_PROCESO") = mat("DMAT3_PROCESO")
                row("DMAT4_PROCESO") = mat("DMAT4_PROCESO")
            End If
        Next

        ReDim parentCols(2)
        ReDim childCols(2)
        parentCols(0) = dsDatosProceso.Tables("PROCESO").Columns("ANYO")
        parentCols(1) = dsDatosProceso.Tables("PROCESO").Columns("GMN1")
        parentCols(2) = dsDatosProceso.Tables("PROCESO").Columns("COD")
        childCols(0) = dsDatosProceso.Tables("GRUPO").Columns("ANYO")
        childCols(1) = dsDatosProceso.Tables("GRUPO").Columns("GMN1")
        childCols(2) = dsDatosProceso.Tables("GRUPO").Columns("PROCE")
        dsDatosProceso.Relations.Add("PROCE_GRUPO", parentCols, childCols, False)

        ReDim parentCols(3)
        ReDim childCols(3)
        parentCols(0) = dsDatosProceso.Tables("GRUPO").Columns("ANYO")
        parentCols(1) = dsDatosProceso.Tables("GRUPO").Columns("GMN1")
        parentCols(2) = dsDatosProceso.Tables("GRUPO").Columns("PROCE")
        parentCols(3) = dsDatosProceso.Tables("GRUPO").Columns("ID")
        childCols(0) = dsDatosProceso.Tables("ITEM").Columns("ANYO")
        childCols(1) = dsDatosProceso.Tables("ITEM").Columns("GMN1_PROCE")
        childCols(2) = dsDatosProceso.Tables("ITEM").Columns("PROCE")
        childCols(3) = dsDatosProceso.Tables("ITEM").Columns("GRUPO")
        dsDatosProceso.Relations.Add("GRUPO_ITEM", parentCols, childCols, False)

        ReDim parentCols(2)
        ReDim childCols(2)
        Dim query1 = From Datos In dsDatosProceso.Tables("PROCESO").AsEnumerable() _
            Where (Datos.Item("DEFDEST") = 1) _
            Order By Datos.Item("NUM_PROCESO") _
            Select Datos Distinct
        If query1.Any Then
            dsDatosProceso.Tables.Add(query1.CopyToDataTable)
            dsDatosProceso.Tables(dsDatosProceso.Tables.Count - 1).TableName = "PROCE_DEST"
            parentCols(0) = dsDatosProceso.Tables("PROCESO").Columns("ANYO")
            parentCols(1) = dsDatosProceso.Tables("PROCESO").Columns("GMN1")
            parentCols(2) = dsDatosProceso.Tables("PROCESO").Columns("COD")
            childCols(0) = dsDatosProceso.Tables("PROCE_DEST").Columns("ANYO")
            childCols(1) = dsDatosProceso.Tables("PROCE_DEST").Columns("GMN1")
            childCols(2) = dsDatosProceso.Tables("PROCE_DEST").Columns("COD")
            dsDatosProceso.Relations.Add("PROCE_PROCE_DEST", parentCols, childCols, False)
        Else
            dsDatosProceso.Tables.Add(New DataTable)
            dsDatosProceso.Tables(dsDatosProceso.Tables.Count - 1).TableName = "PROCE_DEST"
        End If
        Dim query2 = From Datos In dsDatosProceso.Tables("PROCESO").AsEnumerable() _
            Where (Datos.Item("DEFPAG") = 1) _
            Order By Datos.Item("NUM_PROCESO") _
            Select Datos Distinct
        If query2.Any Then
            dsDatosProceso.Tables.Add(query2.CopyToDataTable)
            dsDatosProceso.Tables(dsDatosProceso.Tables.Count - 1).TableName = "PROCE_PAGO"
            parentCols(0) = dsDatosProceso.Tables("PROCESO").Columns("ANYO")
            parentCols(1) = dsDatosProceso.Tables("PROCESO").Columns("GMN1")
            parentCols(2) = dsDatosProceso.Tables("PROCESO").Columns("COD")
            childCols(0) = dsDatosProceso.Tables("PROCE_PAGO").Columns("ANYO")
            childCols(1) = dsDatosProceso.Tables("PROCE_PAGO").Columns("GMN1")
            childCols(2) = dsDatosProceso.Tables("PROCE_PAGO").Columns("COD")
            dsDatosProceso.Relations.Add("PROCE_PROCE_PAGO", parentCols, childCols, False)
        Else
            dsDatosProceso.Tables.Add(New DataTable)
            dsDatosProceso.Tables(dsDatosProceso.Tables.Count - 1).TableName = "PROCE_PAGO"
        End If
        Dim query3 = From Datos In dsDatosProceso.Tables("PROCESO").AsEnumerable() _
            Where (Datos.Item("DEFFECSUM") = 1) _
           Order By Datos.Item("NUM_PROCESO") _
           Select Datos Distinct
        If query3.Any Then
            dsDatosProceso.Tables.Add(query3.CopyToDataTable)
            dsDatosProceso.Tables(dsDatosProceso.Tables.Count - 1).TableName = "PROCE_FECSUM"
            parentCols(0) = dsDatosProceso.Tables("PROCESO").Columns("ANYO")
            parentCols(1) = dsDatosProceso.Tables("PROCESO").Columns("GMN1")
            parentCols(2) = dsDatosProceso.Tables("PROCESO").Columns("COD")
            childCols(0) = dsDatosProceso.Tables("PROCE_FECSUM").Columns("ANYO")
            childCols(1) = dsDatosProceso.Tables("PROCE_FECSUM").Columns("GMN1")
            childCols(2) = dsDatosProceso.Tables("PROCE_FECSUM").Columns("COD")
            dsDatosProceso.Relations.Add("PROCE_PROCE_FECSUM", parentCols, childCols, False)
        Else
            dsDatosProceso.Tables.Add(New DataTable)
            dsDatosProceso.Tables(dsDatosProceso.Tables.Count - 1).TableName = "PROCE_FECSUM"
        End If
        Dim query4 = From Datos In dsDatosProceso.Tables("PROCESO").AsEnumerable() _
            Where (Datos.Item("COD_RESPONSABLE") IsNot Nothing) _
           Order By Datos.Item("NUM_PROCESO") _
           Select Datos Distinct
        If query4.Any Then
            dsDatosProceso.Tables.Add(query4.CopyToDataTable)
            dsDatosProceso.Tables(dsDatosProceso.Tables.Count - 1).TableName = "OPC_RESPONSABLE"
            parentCols(0) = dsDatosProceso.Tables("PROCESO").Columns("ANYO")
            parentCols(1) = dsDatosProceso.Tables("PROCESO").Columns("GMN1")
            parentCols(2) = dsDatosProceso.Tables("PROCESO").Columns("COD")
            childCols(0) = dsDatosProceso.Tables("OPC_RESPONSABLE").Columns("ANYO")
            childCols(1) = dsDatosProceso.Tables("OPC_RESPONSABLE").Columns("GMN1")
            childCols(2) = dsDatosProceso.Tables("OPC_RESPONSABLE").Columns("COD")
            dsDatosProceso.Relations.Add("PROCE_OPC_RESPONSABLE", parentCols, childCols, False)
        Else
            dsDatosProceso.Tables.Add(New DataTable)
            dsDatosProceso.Tables(dsDatosProceso.Tables.Count - 1).TableName = "OPC_RESPONSABLE"
        End If
        ReDim parentCols(3)
        ReDim childCols(3)
        Dim query5 = From procs In dsDatosProceso.Tables("PROCESO").AsEnumerable Join grps In dsDatosProceso.Tables("DATOSGRUPOS").AsEnumerable _
                On procs.Item("ANYO") Equals grps.Item("ANYO") And procs.Item("GMN1") Equals grps.Item("GMN1") And procs.Item("COD") Equals grps.Item("PROCE") _
            Where (procs.Item("DEFDEST") = 2 AndAlso Not IsDBNull(grps.Item("DATO")) AndAlso grps.Item("DATO") = "DEST") _
            Select grps Distinct
        If query5.Any Then
            dsDatosProceso.Tables.Add(query5.CopyToDataTable)
            dsDatosProceso.Tables(dsDatosProceso.Tables.Count - 1).TableName = "GR_DEST"
            parentCols(0) = dsDatosProceso.Tables("GRUPO").Columns("ANYO")
            parentCols(1) = dsDatosProceso.Tables("GRUPO").Columns("GMN1")
            parentCols(2) = dsDatosProceso.Tables("GRUPO").Columns("PROCE")
            parentCols(3) = dsDatosProceso.Tables("GRUPO").Columns("ID")
            childCols(0) = dsDatosProceso.Tables("GR_DEST").Columns("ANYO")
            childCols(1) = dsDatosProceso.Tables("GR_DEST").Columns("GMN1")
            childCols(2) = dsDatosProceso.Tables("GR_DEST").Columns("PROCE")
            childCols(3) = dsDatosProceso.Tables("GR_DEST").Columns("ID")
            dsDatosProceso.Relations.Add("PROCE_GR_DEST", parentCols, childCols, False)
        Else
            dsDatosProceso.Tables.Add(New DataTable)
            dsDatosProceso.Tables(dsDatosProceso.Tables.Count - 1).TableName = "GR_DEST"
        End If
        Dim query6 = From procs In dsDatosProceso.Tables("PROCESO").AsEnumerable() Join grps In dsDatosProceso.Tables("DATOSGRUPOS").AsEnumerable() _
                On procs.Item("ANYO") Equals grps.Item("ANYO") And procs.Item("GMN1") Equals grps.Item("GMN1") And procs.Item("COD") Equals grps.Item("PROCE") _
            Where (procs.Item("DEFFECSUM") = 2 AndAlso Not IsDBNull(grps.Item("DATO")) AndAlso grps.Item("DATO") = "FECSUM") _
            Order By procs.Item("NUM_PROCESO") _
            Select grps Distinct
        If query6.Any Then
            dsDatosProceso.Tables.Add(query6.CopyToDataTable)
            dsDatosProceso.Tables(dsDatosProceso.Tables.Count - 1).TableName = "GR_FECSUM"
            parentCols(0) = dsDatosProceso.Tables("GRUPO").Columns("ANYO")
            parentCols(1) = dsDatosProceso.Tables("GRUPO").Columns("GMN1")
            parentCols(2) = dsDatosProceso.Tables("GRUPO").Columns("PROCE")
            parentCols(3) = dsDatosProceso.Tables("GRUPO").Columns("ID")
            childCols(0) = dsDatosProceso.Tables("GR_FECSUM").Columns("ANYO")
            childCols(1) = dsDatosProceso.Tables("GR_FECSUM").Columns("GMN1")
            childCols(2) = dsDatosProceso.Tables("GR_FECSUM").Columns("PROCE")
            childCols(3) = dsDatosProceso.Tables("GR_FECSUM").Columns("ID")
            dsDatosProceso.Relations.Add("PROCE_GR_FECSUM", parentCols, childCols, False)
        Else
            dsDatosProceso.Tables.Add(New DataTable)
            dsDatosProceso.Tables(dsDatosProceso.Tables.Count - 1).TableName = "GR_FECSUM"
        End If
        Dim query7 = From procs In dsDatosProceso.Tables("PROCESO").AsEnumerable() Join grps In dsDatosProceso.Tables("DATOSGRUPOS").AsEnumerable() _
                On procs.Item("ANYO") Equals grps.Item("ANYO") And procs.Item("GMN1") Equals grps.Item("GMN1") And procs.Item("COD") Equals grps.Item("PROCE") _
            Where (procs.Item("DEFPAG") = 2 AndAlso Not IsDBNull(grps.Item("DATO")) AndAlso grps.Item("DATO") = "PAG") _
            Order By procs.Item("NUM_PROCESO") _
            Select grps Distinct
        If query7.Any Then
            dsDatosProceso.Tables.Add(query7.CopyToDataTable)
            dsDatosProceso.Tables(dsDatosProceso.Tables.Count - 1).TableName = "GR_PAGO"
            parentCols(0) = dsDatosProceso.Tables("GRUPO").Columns("ANYO")
            parentCols(1) = dsDatosProceso.Tables("GRUPO").Columns("GMN1")
            parentCols(2) = dsDatosProceso.Tables("GRUPO").Columns("PROCE")
            parentCols(3) = dsDatosProceso.Tables("GRUPO").Columns("ID")
            childCols(0) = dsDatosProceso.Tables("GR_PAGO").Columns("ANYO")
            childCols(1) = dsDatosProceso.Tables("GR_PAGO").Columns("GMN1")
            childCols(2) = dsDatosProceso.Tables("GR_PAGO").Columns("PROCE")
            childCols(3) = dsDatosProceso.Tables("GR_PAGO").Columns("ID")
            dsDatosProceso.Relations.Add("PROCE_GR_PAGO", parentCols, childCols, False)
        Else
            dsDatosProceso.Tables.Add(New DataTable)
            dsDatosProceso.Tables(dsDatosProceso.Tables.Count - 1).TableName = "GR_PAGO"
        End If
        Dim view1 As New DataView(dsDatosProceso.Tables("IMPLICADO"))
        Dim dt1 As DataTable = view1.ToTable(True, "ANYO", "GMN1", "PROCE")
        dt1.TableName = "IMPLICADOS"
        dsDatosProceso.Tables.Add(dt1)

        ReDim parentCols(2)
        ReDim childCols(2)
        parentCols(0) = dsDatosProceso.Tables("PROCESO").Columns("ANYO")
        parentCols(1) = dsDatosProceso.Tables("PROCESO").Columns("GMN1")
        parentCols(2) = dsDatosProceso.Tables("PROCESO").Columns("COD")
        childCols(0) = dsDatosProceso.Tables("IMPLICADOS").Columns("ANYO")
        childCols(1) = dsDatosProceso.Tables("IMPLICADOS").Columns("GMN1")
        childCols(2) = dsDatosProceso.Tables("IMPLICADOS").Columns("PROCE")
        dsDatosProceso.Relations.Add("PROCE_IMPLICADOS", parentCols, childCols, False)

        parentCols(0) = dsDatosProceso.Tables("IMPLICADOS").Columns("ANYO")
        parentCols(1) = dsDatosProceso.Tables("IMPLICADOS").Columns("GMN1")
        parentCols(2) = dsDatosProceso.Tables("IMPLICADOS").Columns("PROCE")
        childCols(0) = dsDatosProceso.Tables("IMPLICADO").Columns("ANYO")
        childCols(1) = dsDatosProceso.Tables("IMPLICADO").Columns("GMN1")
        childCols(2) = dsDatosProceso.Tables("IMPLICADO").Columns("PROCE")
        dsDatosProceso.Relations.Add("IMPLICADOS_IMPLICADO", parentCols, childCols, False)

        Dim itemsDest As DataRow() = dsDatosProceso.Tables("PROCESO").Select("DEFDEST=2 OR DEFDEST=3")
        Dim itemsPag As DataRow() = dsDatosProceso.Tables("PROCESO").Select("DEFPAG=2 OR DEFPAG=3")
        Dim itemsFecSum As DataRow() = dsDatosProceso.Tables("PROCESO").Select("DEFFECSUM=2 OR DEFFECSUM=3")
        If Not itemsDest.Length = 0 Then
            For Each row As DataRow In itemsDest
                If row("DEFDEST") = 3 Then
                    For Each item As DataRow In dsDatosProceso.Tables("ITEM").Select("ANYO=" & row("ANYO") & " AND GMN1_PROCE='" & row("GMN1") & _
                                                "' AND PROCE=" & row("COD"))
                        item("ITEM_DEST") = 1
                    Next
                Else
                    For Each datosGrupo As DataRow In dsDatosProceso.Tables("GRUPO").Select("ANYO=" & row("ANYO") & " AND GMN1='" & row("GMN1") & "' AND PROCE=" & row("COD"))
                        If dsDatosProceso.Tables("GR_DEST").Rows.Count = 0 OrElse dsDatosProceso.Tables("GR_DEST").Select("ANYO=" & datosGrupo("ANYO") & " AND GMN1='" & datosGrupo("GMN1") & _
                                                "' AND PROCE=" & datosGrupo("PROCE") & " AND ID=" & datosGrupo("ID")).Length = 0 Then
                            For Each item As DataRow In dsDatosProceso.Tables("ITEM").Select("ANYO=" & datosGrupo("ANYO") & " AND GMN1_PROCE='" & datosGrupo("GMN1") & _
                                                "' AND PROCE=" & datosGrupo("PROCE") & " AND GRUPO=" & datosGrupo("ID"))
                                item("ITEM_DEST") = 1
                            Next
                        End If
                    Next
                End If
            Next
        End If
        If Not itemsPag.Length = 0 Then
            For Each row As DataRow In itemsPag
                If row("DEFPAG") = 3 Then
                    For Each item As DataRow In dsDatosProceso.Tables("ITEM").Select("ANYO=" & row("ANYO") & " AND GMN1_PROCE='" & row("GMN1") & _
                                                "' AND PROCE=" & row("COD"))
                        item("ITEM_PAG") = 1
                    Next
                Else
                    For Each datosGrupo As DataRow In dsDatosProceso.Tables("GRUPO").Select("ANYO=" & row("ANYO") & " AND GMN1='" & row("GMN1") & "' AND PROCE=" & row("COD"))
                        If dsDatosProceso.Tables("GR_PAGO").Rows.Count = 0 OrElse dsDatosProceso.Tables("GR_PAGO").Select("ANYO=" & datosGrupo("ANYO") & " AND GMN1='" & datosGrupo("GMN1") & _
                                                "' AND PROCE=" & datosGrupo("PROCE") & " AND ID=" & datosGrupo("ID")).Length = 0 Then
                            For Each grupo As DataRow In dsDatosProceso.Tables("ITEM").Select("ANYO=" & datosGrupo("ANYO") & " AND GMN1_PROCE='" & datosGrupo("GMN1") & _
                                                "' AND PROCE=" & datosGrupo("PROCE") & " AND GRUPO=" & datosGrupo("ID"))
                                grupo("ITEM_PAG") = 1
                            Next
                        End If
                    Next
                End If
            Next
        End If
        If Not itemsFecSum.Length = 0 Then
            For Each row As DataRow In itemsFecSum
                If row("DEFFECSUM") = 3 Then
                    For Each item As DataRow In dsDatosProceso.Tables("ITEM").Select("ANYO=" & row("ANYO") & " AND GMN1_PROCE='" & row("GMN1") & _
                                                "' AND PROCE=" & row("COD"))
                        item("ITEM_FECSUM") = 1
                    Next
                Else
                    For Each datosGrupo As DataRow In dsDatosProceso.Tables("GRUPO").Select("ANYO=" & row("ANYO") & " AND GMN1='" & row("GMN1") & "' AND PROCE=" & row("COD"))
                        If dsDatosProceso.Tables("GR_FECSUM").Rows.Count = 0 OrElse dsDatosProceso.Tables("GR_FECSUM").Select("ANYO=" & datosGrupo("ANYO") & " AND GMN1='" & datosGrupo("GMN1") & _
                                                "' AND PROCE=" & datosGrupo("PROCE") & " AND ID=" & datosGrupo("ID")).Length = 0 Then
                            For Each grupo As DataRow In dsDatosProceso.Tables("ITEM").Select("ANYO=" & datosGrupo("ANYO") & " AND GMN1_PROCE='" & datosGrupo("GMN1") & _
                                                "' AND PROCE=" & datosGrupo("PROCE") & " AND GRUPO=" & datosGrupo("ID"))
                                grupo("ITEM_FECSUM") = 1
                            Next
                        End If
                    Next
                End If
            Next
        End If
        Dim timeDif As Integer = 0
        Dim horaAnterior As DateTime
        'La reunión no tiene pq tener aún procesos
        If dsDatosProceso.Tables("PROCESO").Rows.Count > 0 Then horaAnterior = CType(dsDatosProceso.Tables("PROCESO").Rows(0)("HORA_PROCESO"), DateTime)
        Dim UON_COD_PROCESO, UON_DEN_PROCESO As String
        UON_COD_PROCESO = String.Empty
        UON_DEN_PROCESO = String.Empty
        For Each Row As DataRow In dsDatosProceso.Tables("PROCESO").Rows
            UON_COD_PROCESO = String.Empty
            UON_DEN_PROCESO = String.Empty
            timeDif += DateDiff(DateInterval.Second, horaAnterior, CType(Row("HORA_PROCESO"), DateTime))
            horaAnterior = CType(Row("HORA_PROCESO"), DateTime)
            UON_COD_PROCESO = IIf(IsDBNull(Row("UON1_APER")), "", Row("UON1_APER") & _
                                IIf(IsDBNull(Row("UON2_APER")), "", "-" & Row("UON2_APER") & _
                                    IIf(IsDBNull(Row("UON3_APER")), "", "-" & Row("UON3_APER"))))
            UON_DEN_PROCESO = IIf(IsDBNull(Row("UON1_APER")), "", _
                                  IIf(IsDBNull(Row("UON2_APER")), Row("UON1_APER_DEN"), _
                                    IIf(IsDBNull(Row("UON3_APER")), Row("UON2_APER_DEN"), Row("UON3_APER_DEN"))))
            Row("UON_COD_PROCESO") = UON_COD_PROCESO
            Row("UON_DEN_PROCESO") = "-" & UON_DEN_PROCESO
        Next
        dsDatosProceso.Tables("AGENDA").Rows(0)("HORA_FIN_REU") = DateAdd(DateInterval.Second, timeDif, dsDatosProceso.Tables("AGENDA").Rows(0)("HORA_INI_REU"))

        Return dsDatosProceso
    End Function
    Public Function Reunion_Acta_AdjudicacionesProveedorPorItem(ByVal fecha As DateTime, ByVal Idioma As String, _
                                                                Optional ByVal ProveedoresNoAdjudicados As Boolean = False) As DataSet
        Authenticate()
        Dim AdminPublica As Boolean
        Dim removeRows As New List(Of DataRow)
        Dim dsDatosProceso As DataSet = DBServer.GS_Reunion_Acta(fecha, Idioma)
        Dim dsResultado As New DataSet
        dsResultado.Tables.Add(dsDatosProceso.Tables("ACTA").Copy)
        'Desencripto el campo EST de los sobres para si es administración pública saber si el sobre esta cerrado/abierto
        For Each row As DataRow In dsDatosProceso.Tables("DATOSGRUPOS").Select("SOBRE IS NOT NULL")
            row("EST") = FSNLibrary.Encrypter.EncriptarAperturaSobre(row("ANYO") & row("PROCE") & row("SOBRE"), row("EST"), False)
        Next
        Dim parentCols(2) As DataColumn
        Dim childCols(2) As DataColumn
        parentCols(0) = dsDatosProceso.Tables("PROCESO").Columns("ANYO")
        parentCols(1) = dsDatosProceso.Tables("PROCESO").Columns("GMN1")
        parentCols(2) = dsDatosProceso.Tables("PROCESO").Columns("COD")
        childCols(0) = dsDatosProceso.Tables("MATERIALES_PROCESO").Columns("ANYO")
        childCols(1) = dsDatosProceso.Tables("MATERIALES_PROCESO").Columns("GMN1")
        childCols(2) = dsDatosProceso.Tables("MATERIALES_PROCESO").Columns("PROCE")
        dsDatosProceso.Relations.Add("PROCE_MATERIALES_PROCESO", parentCols, childCols, False)
        'Asigno a cada proceso la info del material
        Dim mat As DataRow
        For Each row As DataRow In dsDatosProceso.Tables("PROCESO").Rows
            If Not row.GetChildRows("PROCE_MATERIALES_PROCESO").Length = 0 Then
                mat = row.GetChildRows("PROCE_MATERIALES_PROCESO")(0)
                row("MAT1_PROCESO") = mat("MAT1_PROCESO")
                row("MAT2_PROCESO") = mat("MAT2_PROCESO")
                row("MAT3_PROCESO") = mat("MAT3_PROCESO")
                row("MAT4_PROCESO") = mat("MAT4_PROCESO")
                row("DMAT1_PROCESO") = mat("DMAT1_PROCESO")
                row("DMAT2_PROCESO") = mat("DMAT2_PROCESO")
                row("DMAT3_PROCESO") = mat("DMAT3_PROCESO")
                row("DMAT4_PROCESO") = mat("DMAT4_PROCESO")
            End If
        Next
        dsResultado.Tables.Add(dsDatosProceso.Tables("PROCESO").Copy)
        Dim timeDif As Integer = 0
        Dim horaAnterior As DateTime
        If Not dsResultado.Tables("PROCESO").Rows.Count = 0 Then horaAnterior = CType(dsResultado.Tables("PROCESO").Rows(0)("HORA_PROCESO"), DateTime)
        Dim UON_COD_PROCESO, UON_DEN_PROCESO As String
        UON_COD_PROCESO = String.Empty
        UON_DEN_PROCESO = String.Empty
        For Each Row As DataRow In dsResultado.Tables("PROCESO").Rows
            UON_COD_PROCESO = String.Empty
            UON_DEN_PROCESO = String.Empty
            timeDif += DateDiff(DateInterval.Second, horaAnterior, CType(Row("HORA_PROCESO"), DateTime))
            horaAnterior = CType(Row("HORA_PROCESO"), DateTime)
            UON_COD_PROCESO = IIf(IsDBNull(Row("UON1_APER")), "", Row("UON1_APER") & _
                                IIf(IsDBNull(Row("UON2_APER")), "", "-" & Row("UON2_APER") & _
                                    IIf(IsDBNull(Row("UON3_APER")), "", "-" & Row("UON3_APER"))))
            UON_DEN_PROCESO = IIf(IsDBNull(Row("UON1_APER")), "", _
                                  IIf(IsDBNull(Row("UON2_APER")), Row("UON1_APER_DEN"), _
                                    IIf(IsDBNull(Row("UON3_APER")), Row("UON2_APER_DEN"), Row("UON3_APER_DEN"))))
            Row("UON_COD_PROCESO") = UON_COD_PROCESO
            Row("UON_DEN_PROCESO") = "-" & UON_DEN_PROCESO
        Next
        dsResultado.Tables("ACTA").Rows(0)("HORA_FIN_REU") = DateAdd(DateInterval.Second, timeDif, dsResultado.Tables("ACTA").Rows(0)("HORA_INI_REU"))
        'Obtenemos los diferentes grupos de cada proceso.
        Dim view As New DataView(dsDatosProceso.Tables("DATOSGRUPOS"))
        Dim dt As DataTable = view.ToTable(True, "ID", "ANYO", "GMN1", "PROCE", "COD_GRUPO", "DEN_GRUPO", "COD_DEST_GR", "DEN_DEST_GR", "EST", _
                "COD_PAGO_GR", "DEN_PAGO_GR", "FINI_SUM_GR", "FFIN_SUM_GR")
        dt.TableName = "GRUPO"
        dsResultado.Tables.Add(dt)
        parentCols(0) = dsResultado.Tables("PROCESO").Columns("ANYO")
        parentCols(1) = dsResultado.Tables("PROCESO").Columns("GMN1")
        parentCols(2) = dsResultado.Tables("PROCESO").Columns("COD")
        childCols(0) = dsResultado.Tables("GRUPO").Columns("ANYO")
        childCols(1) = dsResultado.Tables("GRUPO").Columns("GMN1")
        childCols(2) = dsResultado.Tables("GRUPO").Columns("PROCE")
        dsResultado.Relations.Add("PROCE_GRUPO", parentCols, childCols, False)
        For Each row As DataRow In dsResultado.Tables("GRUPO").Rows
            If row.GetParentRow("PROCE_GRUPO") IsNot Nothing Then
                AdminPublica = CType(row.GetParentRow("PROCE_GRUPO")("ADMIN_PUB"), Boolean)
            Else
                AdminPublica = False
            End If
            If AdminPublica AndAlso row("EST") = "Cerrado" Then
                removeRows.Add(row)
            End If
        Next
        For Each row As DataRow In removeRows
            dsResultado.Tables("GRUPO").Rows.Remove(row)
        Next
        'Si mostramos la info de destino en el proceso
        Dim query1 = From Datos In dsResultado.Tables("PROCESO").AsEnumerable() _
            Where (Datos.Item("DEFDEST") = 1) _
            Order By Datos.Item("NUM_PROCESO") _
            Select Datos Distinct
        If query1.Any Then
            dsResultado.Tables.Add(query1.CopyToDataTable)
            dsResultado.Tables(dsResultado.Tables.Count - 1).TableName = "DEST_PROCE"
            parentCols(0) = dsResultado.Tables("PROCESO").Columns("ANYO")
            parentCols(1) = dsResultado.Tables("PROCESO").Columns("GMN1")
            parentCols(2) = dsResultado.Tables("PROCESO").Columns("COD")
            childCols(0) = dsResultado.Tables("DEST_PROCE").Columns("ANYO")
            childCols(1) = dsResultado.Tables("DEST_PROCE").Columns("GMN1")
            childCols(2) = dsResultado.Tables("DEST_PROCE").Columns("COD")
            dsResultado.Relations.Add("PROCE_DEST_PROCE", parentCols, childCols, False)
        Else
            dsResultado.Tables.Add(New DataTable)
            dsResultado.Tables(dsResultado.Tables.Count - 1).TableName = "DEST_PROCE"
        End If
        'Si mostramos la info de forma de pago en el proceso
        Dim query2 = From Datos In dsResultado.Tables("PROCESO").AsEnumerable() _
            Where (Datos.Item("DEFPAG") = 1) _
            Order By Datos.Item("NUM_PROCESO") _
            Select Datos Distinct
        If query2.Any Then
            dsResultado.Tables.Add(query2.CopyToDataTable)
            dsResultado.Tables(dsResultado.Tables.Count - 1).TableName = "PAGO_PROCE"
            parentCols(0) = dsResultado.Tables("PROCESO").Columns("ANYO")
            parentCols(1) = dsResultado.Tables("PROCESO").Columns("GMN1")
            parentCols(2) = dsResultado.Tables("PROCESO").Columns("COD")
            childCols(0) = dsResultado.Tables("PAGO_PROCE").Columns("ANYO")
            childCols(1) = dsResultado.Tables("PAGO_PROCE").Columns("GMN1")
            childCols(2) = dsResultado.Tables("PAGO_PROCE").Columns("COD")
            dsResultado.Relations.Add("PROCE_PAGO_PROCE", parentCols, childCols, False)
        Else
            dsResultado.Tables.Add(New DataTable)
            dsResultado.Tables(dsResultado.Tables.Count - 1).TableName = "PAGO_PROCE"
        End If
        'Si mostramos la info de fechas de suministro en el proceso
        Dim query3 = From Datos In dsResultado.Tables("PROCESO").AsEnumerable() _
            Where (Datos.Item("DEFFECSUM") = 1) _
           Order By Datos.Item("NUM_PROCESO") _
           Select Datos Distinct
        If query3.Any Then
            dsResultado.Tables.Add(query3.CopyToDataTable)
            dsResultado.Tables(dsResultado.Tables.Count - 1).TableName = "FECSUM_PROCE"
            parentCols(0) = dsResultado.Tables("PROCESO").Columns("ANYO")
            parentCols(1) = dsResultado.Tables("PROCESO").Columns("GMN1")
            parentCols(2) = dsResultado.Tables("PROCESO").Columns("COD")
            childCols(0) = dsResultado.Tables("FECSUM_PROCE").Columns("ANYO")
            childCols(1) = dsResultado.Tables("FECSUM_PROCE").Columns("GMN1")
            childCols(2) = dsResultado.Tables("FECSUM_PROCE").Columns("COD")
            dsResultado.Relations.Add("PROCE_FECSUM_PROCE", parentCols, childCols, False)
        Else
            dsResultado.Tables.Add(New DataTable)
            dsResultado.Tables(dsResultado.Tables.Count - 1).TableName = "FECSUM_PROCE"
        End If
        'La info del responsable del proceso
        Dim query4 = From Datos In dsResultado.Tables("PROCESO").AsEnumerable() _
            Where (Datos.Item("COD_RESPONSABLE") IsNot Nothing) _
           Order By Datos.Item("NUM_PROCESO") _
           Select Datos Distinct
        If query4.Any Then
            dsResultado.Tables.Add(query4.CopyToDataTable)
            dsResultado.Tables(dsResultado.Tables.Count - 1).TableName = "OPC_RESPONSABLE"
            parentCols(0) = dsResultado.Tables("PROCESO").Columns("ANYO")
            parentCols(1) = dsResultado.Tables("PROCESO").Columns("GMN1")
            parentCols(2) = dsResultado.Tables("PROCESO").Columns("COD")
            childCols(0) = dsResultado.Tables("OPC_RESPONSABLE").Columns("ANYO")
            childCols(1) = dsResultado.Tables("OPC_RESPONSABLE").Columns("GMN1")
            childCols(2) = dsResultado.Tables("OPC_RESPONSABLE").Columns("COD")
            dsResultado.Relations.Add("PROCE_OPC_RESPONSABLE", parentCols, childCols, False)
        Else
            dsResultado.Tables.Add(New DataTable)
            dsResultado.Tables(dsResultado.Tables.Count - 1).TableName = "OPC_RESPONSABLE"
        End If
        'Implicados en el proceso
        view = New DataView(dsDatosProceso.Tables("IMPLICADO"))
        dt = New DataTable
        dt = view.ToTable(True, "ANYO", "GMN1", "PROCE")
        dt.TableName = "IMPLICADOS" 'Solo nos sirve para saber si mostrar la lista de implicados
        dsResultado.Tables.Add(dt)
        parentCols(0) = dsResultado.Tables("PROCESO").Columns("ANYO")
        parentCols(1) = dsResultado.Tables("PROCESO").Columns("GMN1")
        parentCols(2) = dsResultado.Tables("PROCESO").Columns("COD")
        childCols(0) = dsResultado.Tables("IMPLICADOS").Columns("ANYO")
        childCols(1) = dsResultado.Tables("IMPLICADOS").Columns("GMN1")
        childCols(2) = dsResultado.Tables("IMPLICADOS").Columns("PROCE")
        dsResultado.Relations.Add("PROCE_IMPLICADOS", parentCols, childCols, False)
        dsResultado.Tables.Add(dsDatosProceso.Tables("IMPLICADO").Copy)
        parentCols(0) = dsResultado.Tables("IMPLICADOS").Columns("ANYO")
        parentCols(1) = dsResultado.Tables("IMPLICADOS").Columns("GMN1")
        parentCols(2) = dsResultado.Tables("IMPLICADOS").Columns("PROCE")
        childCols(0) = dsResultado.Tables("IMPLICADO").Columns("ANYO")
        childCols(1) = dsResultado.Tables("IMPLICADO").Columns("GMN1")
        childCols(2) = dsResultado.Tables("IMPLICADO").Columns("PROCE")
        dsResultado.Relations.Add("IMPLICADOS_IMPLICADO", parentCols, childCols, False)
        If Not dsDatosProceso.Tables("ASISTENTE").Rows.Count = 0 Then
            view = New DataView(dsDatosProceso.Tables("ASISTENTE"))
            dt = New DataTable
            dt = view.ToTable(True, "ASISTENTE")
            dt.TableName = "ASISTENTES" 'Solo nos sirve para saber si mostrar la lista de ASISTENTES
            dsResultado.Tables.Add(dt)
            dsResultado.Tables.Add(dsDatosProceso.Tables("ASISTENTE").Copy)
            For Each row As DataRow In dsResultado.Tables("ASISTENTE").Rows
                row("UO_ASIST") = IIf(IsDBNull(row("UON1")), "", row("UON1")) & _
                                    IIf(IsDBNull(row("UON2")), "", "-" & row("UON2")) & _
                                    IIf(IsDBNull(row("UON3")), "", "-" & row("UON3"))
            Next
        End If
        'Datos del resumen economico
        view = New DataView(dsDatosProceso.Tables("PROCESO"))
        dt = New DataTable
        dt = view.ToTable(True, "ANYO", "GMN1", "COD", "ADMIN_PUB", "PRES_GEN", "ADJ_GEN", "AHORRO_GEN", "PORCEN_GEN")
        dt.TableName = "OPT_RESUMEN"
        dsResultado.Tables.Add(dt)
        view = New DataView(dsDatosProceso.Tables("DATOSGRUPOS"))
        dt = New DataTable
        dt = view.ToTable(True, "ID", "ANYO", "GMN1", "PROCE", "COD_GRUPO", "DEN_GRUPO", "EST", "PRES_GR", "ADJ_GR", "AHORRO_GR", "PORCEN_GR_RES")
        dt.TableName = "RESUMEN_GR"
        dt.Columns("COD_GRUPO").ColumnName = "GR_COD_RES"
        dt.Columns("DEN_GRUPO").ColumnName = "GR_DEN_RES"
        dsResultado.Tables.Add(dt)
        parentCols(0) = dsResultado.Tables("PROCESO").Columns("ANYO")
        parentCols(1) = dsResultado.Tables("PROCESO").Columns("GMN1")
        parentCols(2) = dsResultado.Tables("PROCESO").Columns("COD")
        childCols(0) = dsResultado.Tables("OPT_RESUMEN").Columns("ANYO")
        childCols(1) = dsResultado.Tables("OPT_RESUMEN").Columns("GMN1")
        childCols(2) = dsResultado.Tables("OPT_RESUMEN").Columns("COD")
        dsResultado.Relations.Add("PROCE_OPT_RESUMEN", parentCols, childCols, False)
        parentCols(0) = dsResultado.Tables("OPT_RESUMEN").Columns("ANYO")
        parentCols(1) = dsResultado.Tables("OPT_RESUMEN").Columns("GMN1")
        parentCols(2) = dsResultado.Tables("OPT_RESUMEN").Columns("COD")
        childCols(0) = dsResultado.Tables("RESUMEN_GR").Columns("ANYO")
        childCols(1) = dsResultado.Tables("RESUMEN_GR").Columns("GMN1")
        childCols(2) = dsResultado.Tables("RESUMEN_GR").Columns("PROCE")
        dsResultado.Relations.Add("OPT_RESUMEN_RESUMEN_GR", parentCols, childCols, False)

        removeRows.Clear()
        Dim PROCE_PROVE_GRUPOS As Boolean = CType(dsDatosProceso.Tables("ACTA").Rows(0)("PROCE_PROVE_GRUPOS"), Boolean)
        For Each row As DataRow In dsResultado.Tables("RESUMEN_GR").Rows
            row("PRES_GR") = 0
            row("ADJ_GR") = 0
            row("AHORRO_GR") = 0
            If row.GetParentRow("OPT_RESUMEN_RESUMEN_GR") IsNot Nothing Then
                AdminPublica = row.GetParentRow("OPT_RESUMEN_RESUMEN_GR")("ADMIN_PUB")
            Else
                AdminPublica = False
            End If
            If AdminPublica AndAlso row("EST") = "Cerrado" Then
                removeRows.Add(row)
            Else
                For Each prove As DataRow In dsDatosProceso.Tables("ADJUDICACIONES").Select("ANYO=" & row("ANYO") & " AND GMN1='" & row("GMN1") & _
                                                "' AND PROCE=" & row("PROCE") & " AND GRUPO=" & row("ID"))
                    If Not PROCE_PROVE_GRUPOS OrElse Not dsDatosProceso.Tables("GRUPOS_ASIGNACIONES").Select("ANYO=" & row("ANYO") & " AND GMN1='" & row("GMN1") & _
                                                "' AND PROCE=" & row("PROCE") & " AND ID=" & row("ID") & " AND PROVE='" & prove("PROVE") & "'").Length = 0 Then
                        row("PRES_GR") += DBNullToDbl(prove("CONSUMIDO"))
                        row("ADJ_GR") += DBNullToDbl(prove("ADJUDICADO"))
                    End If
                Next
                row("AHORRO_GR") += DBNullToDbl(row("PRES_GR")) - DBNullToDbl(row("ADJ_GR"))
                If IsDBNull(row("PRES_GR")) OrElse row("PRES_GR") = 0 Then
                    row("PORCEN_GR_RES") = 0
                Else
                    row("PORCEN_GR_RES") = (row("AHORRO_GR") / row("PRES_GR")) * 100
                End If
            End If
        Next
        For Each row As DataRow In removeRows
            dsResultado.Tables("RESUMEN_GR").Rows.Remove(row)
        Next
        view = New DataView(dsDatosProceso.Tables("PROVEEDORES"))
        dt = New DataTable
        dt = view.ToTable(True, "ANYO", "GMN1", "PROCE", "PROVECOD", "PROVEDEN")
        dt.TableName = "RESUMEN_PROV"
        dt.Columns("PROVECOD").ColumnName = "PROV_COD_RES"
        dt.Columns("PROVEDEN").ColumnName = "PROV_DEN_RES"
        dt.Columns.Add("PRES_TOT_PROV", GetType(System.Double))
        dt.Columns.Add("OFE_TOT_PROV", GetType(System.Double))
        dt.Columns.Add("AHORRO_TOT_PROV", GetType(System.Double))
        dt.Columns.Add("PORCEN_TOT_PROV", GetType(System.Double))
        dsResultado.Tables.Add(dt)

        For Each rowIterate As DataRow In dsResultado.Tables("RESUMEN_PROV").Rows
            Dim row As DataRow = rowIterate
            row("PRES_TOT_PROV") = 0
            row("OFE_TOT_PROV") = 0
            row("AHORRO_TOT_PROV") = 0
            row("PORCEN_TOT_PROV") = 0
            If Not dsResultado.Tables("RESUMEN_GR").Select("ANYO=" & row("ANYO") & " AND GMN1='" & row("GMN1") & "' AND PROCE=" & row("PROCE")).Length = 0 Then
                For Each grupo As DataRow In dsDatosProceso.Tables("ADJUDICACIONES").Select("ANYO=" & row("ANYO") & " AND GMN1='" & row("GMN1") & _
                                                "' AND PROCE=" & row("PROCE") & " AND PROVE='" & row("PROV_COD_RES") & "'")
                    If Not PROCE_PROVE_GRUPOS OrElse Not dsDatosProceso.Tables("GRUPOS_ASIGNACIONES").Select("ANYO=" & row("ANYO") & " AND GMN1='" & row("GMN1") & _
                                                    "' AND PROCE=" & row("PROCE") & " AND ID=" & grupo("GRUPO") & " AND PROVE='" & row("PROV_COD_RES") & "'").Length = 0 Then
                        row("PRES_TOT_PROV") = dsDatosProceso.Tables("ADJUDICACIONES").Compute("SUM(CONSUMIDO)", _
                                    "ANYO=" & row("ANYO") & " AND GMN1='" & row("GMN1") & "' AND PROCE=" & row("PROCE") & " AND PROVE='" & row("PROV_COD_RES") & "'")
                        row("OFE_TOT_PROV") = dsDatosProceso.Tables("ADJUDICACIONES").Compute("SUM(ADJUDICADO)", _
                                    "ANYO=" & row("ANYO") & " AND GMN1='" & row("GMN1") & "' AND PROCE=" & row("PROCE") & " AND PROVE='" & row("PROV_COD_RES") & "'")
                    End If
                Next
                Dim atributos = From atrib In dsDatosProceso.Tables("ATRIBUTOS").AsEnumerable()
                    Join adj In dsDatosProceso.Tables("ADJUDICACIONES").AsEnumerable() On atrib.Item("ANYO") Equals adj.Item("ANYO") _
                        And atrib.Item("GMN1") Equals adj.Item("GMN1") And atrib.Item("PROCE") Equals adj.Item("PROCE") _
                        And atrib.Item("PROVE") Equals adj.Item("PROVE") And atrib.Item("OFE") Equals adj.Item("OFE") _
                    Join prove In dsDatosProceso.Tables("PROVEEDORES").AsEnumerable On prove.Item("ANYO") Equals adj.Item("ANYO") _
                        And prove.Item("GMN1") Equals adj.Item("GMN1") And prove.Item("PROCE") Equals adj.Item("PROCE") _
                        And prove.Item("PROVECOD") Equals adj.Item("PROVE") _
                Where (atrib.Item("ANYO") = row("ANYO") And atrib.Item("GMN1") = row("GMN1") And atrib.Item("PROCE") = row("PROCE") _
                    And atrib.Item("PROVE") = row("PROV_COD_RES") And atrib.Item("APLIC_PREC") = 0)
                Select atrib Distinct
                If atributos.Any Then
                    For Each atributo As DataRow In atributos
                        Select Case atributo("OP_PREC")
                            Case "+"
                                row("OFE_TOT_PROV") = DBNullToDbl(row("OFE_TOT_PROV")) + atributo("VALOR_NUM")
                            Case "-"
                                row("OFE_TOT_PROV") = DBNullToDbl(row("OFE_TOT_PROV")) - atributo("VALOR_NUM")
                            Case "/"
                                If IsDBNull(atributo("VALOR_NUM")) OrElse atributo("VALOR_NUM") = 0 Then
                                    row("OFE_TOT_PROV") = 0
                                Else
                                    row("OFE_TOT_PROV") = DBNullToDbl(row("OFE_TOT_PROV")) / atributo("VALOR_NUM")
                                End If
                            Case "*"
                                row("OFE_TOT_PROV") = DBNullToDbl(row("OFE_TOT_PROV")) * atributo("VALOR_NUM")
                            Case "+%"
                                row("OFE_TOT_PROV") = DBNullToDbl(row("OFE_TOT_PROV")) + ((atributo("VALOR_NUM") / 100) * DBNullToDbl(row("OFE_TOT_PROV")))
                            Case "-%"
                                row("OFE_TOT_PROV") = DBNullToDbl(row("OFE_TOT_PROV")) - ((atributo("VALOR_NUM") / 100) * DBNullToDbl(row("OFE_TOT_PROV")))
                        End Select
                    Next
                End If
                row("AHORRO_TOT_PROV") = DBNullToDbl(row("PRES_TOT_PROV")) - DBNullToDbl(row("OFE_TOT_PROV"))
                If IsDBNull(row("PRES_TOT_PROV")) OrElse row("PRES_TOT_PROV") = 0 Then
                    row("PORCEN_TOT_PROV") = 0
                Else
                    row("PORCEN_TOT_PROV") = (row("AHORRO_TOT_PROV") / row("PRES_TOT_PROV")) * 100
                End If
            End If
        Next
        For Each row As DataRow In dsResultado.Tables("OPT_RESUMEN").Rows
            row("PRES_GEN") = dsResultado.Tables("RESUMEN_PROV").Compute("SUM(PRES_TOT_PROV)", _
                                "ANYO=" & row("ANYO") & " AND GMN1='" & row("GMN1") & "' AND PROCE=" & row("COD"))
            row("ADJ_GEN") = dsResultado.Tables("RESUMEN_PROV").Compute("SUM(OFE_TOT_PROV)", _
                                "ANYO=" & row("ANYO") & " AND GMN1='" & row("GMN1") & "' AND PROCE=" & row("COD"))
            row("AHORRO_GEN") = DBNullToDbl(row("PRES_GEN")) - DBNullToDbl(row("ADJ_GEN"))
            If IsDBNull(row("PRES_GEN")) OrElse row("PRES_GEN") = 0 Then
                row("PORCEN_GEN") = 0
            Else
                row("PORCEN_GEN") = (row("AHORRO_GEN") / row("PRES_GEN")) * 100
            End If
        Next
        dt = New DataTable
        dt = dsResultado.Tables("OPT_RESUMEN").Copy
        dt.TableName = "OPT_RESUMEN_PROV"
        dt.Columns("PRES_GEN").ColumnName = "PRES_TOT"
        dt.Columns("ADJ_GEN").ColumnName = "OFE_TOT"
        dt.Columns("AHORRO_GEN").ColumnName = "AHORRO_TOT"
        dt.Columns("PORCEN_GEN").ColumnName = "PORCEN_TOT"
        dsResultado.Tables.Add(dt)
        parentCols(0) = dsResultado.Tables("PROCESO").Columns("ANYO")
        parentCols(1) = dsResultado.Tables("PROCESO").Columns("GMN1")
        parentCols(2) = dsResultado.Tables("PROCESO").Columns("COD")
        childCols(0) = dsResultado.Tables("OPT_RESUMEN_PROV").Columns("ANYO")
        childCols(1) = dsResultado.Tables("OPT_RESUMEN_PROV").Columns("GMN1")
        childCols(2) = dsResultado.Tables("OPT_RESUMEN_PROV").Columns("COD")
        dsResultado.Relations.Add("PROCE_OPT_RESUMEN_PROV", parentCols, childCols, False)
        parentCols(0) = dsResultado.Tables("OPT_RESUMEN_PROV").Columns("ANYO")
        parentCols(1) = dsResultado.Tables("OPT_RESUMEN_PROV").Columns("GMN1")
        parentCols(2) = dsResultado.Tables("OPT_RESUMEN_PROV").Columns("COD")
        childCols(0) = dsResultado.Tables("RESUMEN_PROV").Columns("ANYO")
        childCols(1) = dsResultado.Tables("RESUMEN_PROV").Columns("GMN1")
        childCols(2) = dsResultado.Tables("RESUMEN_PROV").Columns("PROCE")
        dsResultado.Relations.Add("OPT_RESUMEN_PROV_RESUMEN_PROV", parentCols, childCols, False)
        If ProveedoresNoAdjudicados Then
            view = New DataView(dsDatosProceso.Tables("PROVEEDORES_NOADJUDICADOS"))
            dt = New DataTable
            dt = view.ToTable(True, "ANYO", "GMN1", "PROCE")
            dt.TableName = "OPT_RESUMEN_PROV_NOADJ"
            dsResultado.Tables.Add(dt)
            view = New DataView(dsDatosProceso.Tables("PROVEEDORES_NOADJUDICADOS"))
            dt = New DataTable
            dt = view.ToTable(True, "ANYO", "GMN1", "PROCE", "PROVECOD", "PROVEDEN")
            dt.TableName = "RESUMEN_PROV_NOAJ"
            dt.Columns("PROVECOD").ColumnName = "PROV_COD_RES_NOADJ"
            dt.Columns("PROVEDEN").ColumnName = "PROV_DEN_RES_NOADJ"
            dt.Columns.Add("PRES_TOT_PROV_NOADJ", GetType(System.Double))
            dt.Columns.Add("OFE_TOT_PROV_NOADJ", GetType(System.Double))
            dt.Columns.Add("AHORRO_TOT_PROV_NOADJ", GetType(System.Double))
            dt.Columns.Add("PORCEN_TOT_PROV_NOADJ", GetType(System.Double))
            dt.Columns.Add("OFE", GetType(System.Int32))
            dsResultado.Tables.Add(dt)
            parentCols(0) = dsResultado.Tables("PROCESO").Columns("ANYO")
            parentCols(1) = dsResultado.Tables("PROCESO").Columns("GMN1")
            parentCols(2) = dsResultado.Tables("PROCESO").Columns("COD")
            childCols(0) = dsResultado.Tables("OPT_RESUMEN_PROV_NOADJ").Columns("ANYO")
            childCols(1) = dsResultado.Tables("OPT_RESUMEN_PROV_NOADJ").Columns("GMN1")
            childCols(2) = dsResultado.Tables("OPT_RESUMEN_PROV_NOADJ").Columns("PROCE")
            dsResultado.Relations.Add("PROCE_OPT_RESUMEN_PROV_NOADJ", parentCols, childCols, False)
            parentCols(0) = dsResultado.Tables("OPT_RESUMEN_PROV_NOADJ").Columns("ANYO")
            parentCols(1) = dsResultado.Tables("OPT_RESUMEN_PROV_NOADJ").Columns("GMN1")
            parentCols(2) = dsResultado.Tables("OPT_RESUMEN_PROV_NOADJ").Columns("PROCE")
            childCols(0) = dsResultado.Tables("RESUMEN_PROV_NOAJ").Columns("ANYO")
            childCols(1) = dsResultado.Tables("RESUMEN_PROV_NOAJ").Columns("GMN1")
            childCols(2) = dsResultado.Tables("RESUMEN_PROV_NOAJ").Columns("PROCE")
            dsResultado.Relations.Add("OPT_RESUMEN_PROV_NOADJ_RESUMEN_PROV_NOAJ", parentCols, childCols, False)
            Dim atributosProveNoAdj As DataRow()
            Dim ConsumidoFicticio As Double
            For Each row As DataRow In dsResultado.Tables("RESUMEN_PROV_NOAJ").Rows
                row("PRES_TOT_PROV_NOADJ") = 0
                row("OFE_TOT_PROV_NOADJ") = 0
                row("AHORRO_TOT_PROV_NOADJ") = 0
                row("PORCEN_TOT_PROV_NOADJ") = 0
                row("OFE") = 0
                ConsumidoFicticio = 0
                For Each grupo As DataRow In dsDatosProceso.Tables("ADJUDICACIONES").Select("ANYO=" & row("ANYO") & " AND GMN1='" & row("GMN1") & _
                                                "' AND PROCE=" & row("PROCE") & " AND PROVE='" & row("PROV_COD_RES_NOADJ") & "'  AND NOT IMPORTE=0")
                    row("OFE") = grupo("OFE")
                    ConsumidoFicticio += dsDatosProceso.Tables("ADJUDICACIONES").Compute("SUM(CONSUMIDO)", _
                                "ANYO=" & row("ANYO") & " AND GMN1='" & row("GMN1") & "' AND PROCE=" & row("PROCE") & " AND GRUPO=" & grupo("GRUPO"))
                    If Not PROCE_PROVE_GRUPOS OrElse Not dsDatosProceso.Tables("GRUPOS_ASIGNACIONES").Select("ANYO=" & row("ANYO") & " AND GMN1='" & row("GMN1") & _
                                    "' AND PROCE=" & row("PROCE") & " AND ID=" & grupo("GRUPO") & " AND PROVE='" & row("PROV_COD_RES_NOADJ") & "'").Length = 0 Then
                        row("PRES_TOT_PROV_NOADJ") = ConsumidoFicticio
                        ' ANTES ESTABA: 
                        'dsDatosProceso.Tables("ADJUDICACIONES").Compute("SUM(ABIERTO)", _
                        '"ANYO=" & row("ANYO") & " AND GMN1='" & row("GMN1") & "' AND PROCE=" & row("PROCE") & " AND PROVE='" & row("PROV_COD_RES_NOADJ") & "'")
                        'Pero debido a que ahora no se actualiza el campo abierto, calculamos el consumido
                        row("OFE_TOT_PROV_NOADJ") = dsDatosProceso.Tables("ADJUDICACIONES").Compute("SUM(IMPORTE)", _
                                    "ANYO=" & row("ANYO") & " AND GMN1='" & row("GMN1") & "' AND PROCE=" & row("PROCE") & " AND PROVE='" & row("PROV_COD_RES_NOADJ") & "'")
                    End If
                Next
                atributosProveNoAdj = dsDatosProceso.Tables("ATRIBUTOS").Select("ANYO=" & row("ANYO") & " AND GMN1='" & row("GMN1") & _
                            "' AND PROCE=" & row("PROCE") & " AND OFE=" & row("OFE") & " AND PROVE='" & row("PROV_COD_RES_NOADJ") & "' AND APLIC_PREC=0", "ORDEN")
                If Not atributosProveNoAdj.Length = 0 Then
                    For Each atributo As DataRow In atributosProveNoAdj
                        Select Case atributo("OP_PREC")
                            Case "+"
                                row("OFE_TOT_PROV_NOADJ") = DBNullToDbl(row("OFE_TOT_PROV_NOADJ")) + atributo("VALOR_NUM")
                            Case "-"
                                row("OFE_TOT_PROV_NOADJ") = DBNullToDbl(row("OFE_TOT_PROV_NOADJ")) - atributo("VALOR_NUM")
                            Case "/"
                                If IsDBNull(atributo("VALOR_NUM")) OrElse atributo("VALOR_NUM") = 0 Then
                                    row("OFE_TOT_PROV_NOADJ") = 0
                                Else
                                    row("OFE_TOT_PROV_NOADJ") = DBNullToDbl(row("OFE_TOT_PROV_NOADJ")) / atributo("VALOR_NUM")
                                End If
                            Case "*"
                                row("OFE_TOT_PROV_NOADJ") = DBNullToDbl(row("OFE_TOT_PROV_NOADJ")) * atributo("VALOR_NUM")
                            Case "+%"
                                row("OFE_TOT_PROV_NOADJ") = DBNullToDbl(row("OFE_TOT_PROV_NOADJ")) + ((atributo("VALOR_NUM") / 100) * DBNullToDbl(row("OFE_TOT_PROV_NOADJ")))
                            Case "-%"
                                row("OFE_TOT_PROV_NOADJ") = DBNullToDbl(row("OFE_TOT_PROV_NOADJ")) - ((atributo("VALOR_NUM") / 100) * DBNullToDbl(row("OFE_TOT_PROV_NOADJ")))
                        End Select
                    Next
                End If
                row("AHORRO_TOT_PROV_NOADJ") = ConsumidoFicticio - DBNullToDbl(row("OFE_TOT_PROV_NOADJ"))
                If ConsumidoFicticio = 0 Then
                    row("PORCEN_TOT_PROV_NOADJ") = 0
                Else
                    row("PORCEN_TOT_PROV_NOADJ") = (row("AHORRO_TOT_PROV_NOADJ") / ConsumidoFicticio) * 100
                End If
            Next
        End If
        removeRows.Clear()
        For Each row As DataRow In dsResultado.Tables("OPT_RESUMEN_PROV").Rows
            If row.GetChildRows("OPT_RESUMEN_PROV_RESUMEN_PROV").Length = 0 Then
                removeRows.Add(row)
            End If
        Next
        Dim removeGroups As New List(Of DataRow)
        Dim removeProveedoresNoAdj As New List(Of DataRow)
        For Each row As DataRow In removeRows
            dsResultado.Tables("OPT_RESUMEN").Rows.Remove(dsResultado.Tables("OPT_RESUMEN").Select("ANYO=" & row("ANYO") & _
                                                            " AND GMN1='" & row("GMN1") & "' AND COD=" & row("COD"))(0))
            For Each grupo As DataRow In dsResultado.Tables("GRUPO").Select("ANYO=" & row("ANYO") & _
                                                            " AND GMN1='" & row("GMN1") & "' AND PROCE=" & row("COD"))
                removeGroups.Add(grupo)
            Next
            If ProveedoresNoAdjudicados Then
                For Each prove As DataRow In dsResultado.Tables("OPT_RESUMEN_PROV_NOADJ").Select("ANYO=" & row("ANYO") & _
                                                            " AND GMN1='" & row("GMN1") & "' AND PROCE=" & row("COD"))
                    removeProveedoresNoAdj.Add(prove)
                Next
            End If
        Next
        For Each row As DataRow In removeRows
            dsResultado.Tables("OPT_RESUMEN_PROV").Rows.Remove(row)
        Next
        For Each row As DataRow In removeGroups
            dsResultado.Tables("GRUPO").Rows.Remove(row)
        Next
        If ProveedoresNoAdjudicados Then
            For Each row As DataRow In removeProveedoresNoAdj
                dsResultado.Tables("OPT_RESUMEN_PROV_NOADJ").Rows.Remove(row)
            Next
        End If
        ReDim parentCols(3)
        ReDim childCols(3)
        'Si mostramos la info del destino en el grupo
        Dim query5 = From procs In dsDatosProceso.Tables("PROCESO").AsEnumerable Join grps In dsDatosProceso.Tables("DATOSGRUPOS").AsEnumerable _
                On procs.Item("ANYO") Equals grps.Item("ANYO") And procs.Item("GMN1") Equals grps.Item("GMN1") And procs.Item("COD") Equals grps.Item("PROCE") _
            Where (procs.Item("DEFDEST") = 2 And grps.Item("DATO") = "DEST") _
            Select grps Distinct
        If query5.Any Then
            dsResultado.Tables.Add(query5.CopyToDataTable)
            dsResultado.Tables(dsResultado.Tables.Count - 1).TableName = "GR_DEST"
            parentCols(0) = dsResultado.Tables("GRUPO").Columns("ANYO")
            parentCols(1) = dsResultado.Tables("GRUPO").Columns("GMN1")
            parentCols(2) = dsResultado.Tables("GRUPO").Columns("PROCE")
            parentCols(3) = dsResultado.Tables("GRUPO").Columns("ID")
            childCols(0) = dsResultado.Tables("GR_DEST").Columns("ANYO")
            childCols(1) = dsResultado.Tables("GR_DEST").Columns("GMN1")
            childCols(2) = dsResultado.Tables("GR_DEST").Columns("PROCE")
            childCols(3) = dsResultado.Tables("GR_DEST").Columns("ID")
            dsResultado.Relations.Add("PROCE_GR_DEST", parentCols, childCols, False)
        Else
            dsResultado.Tables.Add(New DataTable)
            dsResultado.Tables(dsResultado.Tables.Count - 1).TableName = "GR_DEST"
        End If
        'Si mostramos la info de fechas de suministro en el grupo
        Dim query6 = From procs In dsDatosProceso.Tables("PROCESO").AsEnumerable() Join grps In dsDatosProceso.Tables("DATOSGRUPOS").AsEnumerable() _
                On procs.Item("ANYO") Equals grps.Item("ANYO") And procs.Item("GMN1") Equals grps.Item("GMN1") And procs.Item("COD") Equals grps.Item("PROCE") _
            Where (procs.Item("DEFFECSUM") = 2 And grps.Item("DATO") = "FECSUM") _
            Order By procs.Item("NUM_PROCESO") _
            Select grps Distinct
        If query6.Any Then
            dsResultado.Tables.Add(query6.CopyToDataTable)
            dsResultado.Tables(dsResultado.Tables.Count - 1).TableName = "GR_FECSUM"
            parentCols(0) = dsResultado.Tables("GRUPO").Columns("ANYO")
            parentCols(1) = dsResultado.Tables("GRUPO").Columns("GMN1")
            parentCols(2) = dsResultado.Tables("GRUPO").Columns("PROCE")
            parentCols(3) = dsResultado.Tables("GRUPO").Columns("ID")
            childCols(0) = dsResultado.Tables("GR_FECSUM").Columns("ANYO")
            childCols(1) = dsResultado.Tables("GR_FECSUM").Columns("GMN1")
            childCols(2) = dsResultado.Tables("GR_FECSUM").Columns("PROCE")
            childCols(3) = dsResultado.Tables("GR_FECSUM").Columns("ID")
            dsResultado.Relations.Add("PROCE_GR_FECSUM", parentCols, childCols, False)
        Else
            dsResultado.Tables.Add(New DataTable)
            dsResultado.Tables(dsResultado.Tables.Count - 1).TableName = "GR_FECSUM"
        End If
        'Si mostramos la info de forma de pago del grupo
        Dim query7 = From procs In dsDatosProceso.Tables("PROCESO").AsEnumerable() Join grps In dsDatosProceso.Tables("DATOSGRUPOS").AsEnumerable() _
                On procs.Item("ANYO") Equals grps.Item("ANYO") And procs.Item("GMN1") Equals grps.Item("GMN1") And procs.Item("COD") Equals grps.Item("PROCE") _
            Where (procs.Item("DEFPAG") = 2 And grps.Item("DATO") = "PAG") _
            Order By procs.Item("NUM_PROCESO") _
            Select grps Distinct
        If query7.Any Then
            dsResultado.Tables.Add(query7.CopyToDataTable)
            dsResultado.Tables(dsResultado.Tables.Count - 1).TableName = "GR_PAGO"
            parentCols(0) = dsResultado.Tables("GRUPO").Columns("ANYO")
            parentCols(1) = dsResultado.Tables("GRUPO").Columns("GMN1")
            parentCols(2) = dsResultado.Tables("GRUPO").Columns("PROCE")
            parentCols(3) = dsResultado.Tables("GRUPO").Columns("ID")
            childCols(0) = dsResultado.Tables("GR_PAGO").Columns("ANYO")
            childCols(1) = dsResultado.Tables("GR_PAGO").Columns("GMN1")
            childCols(2) = dsResultado.Tables("GR_PAGO").Columns("PROCE")
            childCols(3) = dsResultado.Tables("GR_PAGO").Columns("ID")
            dsResultado.Relations.Add("PROCE_GR_PAGO", parentCols, childCols, False)
        Else
            dsResultado.Tables.Add(New DataTable)
            dsResultado.Tables(dsResultado.Tables.Count - 1).TableName = "GR_PAGO"
        End If
        dsResultado.Tables.Add(dsDatosProceso.Tables("ITEM_TOD").Copy)
        parentCols(0) = dsResultado.Tables("GRUPO").Columns("ANYO")
        parentCols(1) = dsResultado.Tables("GRUPO").Columns("GMN1")
        parentCols(2) = dsResultado.Tables("GRUPO").Columns("PROCE")
        parentCols(3) = dsResultado.Tables("GRUPO").Columns("ID")
        childCols(0) = dsResultado.Tables("ITEM_TOD").Columns("ANYO")
        childCols(1) = dsResultado.Tables("ITEM_TOD").Columns("GMN1_PROCE")
        childCols(2) = dsResultado.Tables("ITEM_TOD").Columns("PROCE")
        childCols(3) = dsResultado.Tables("ITEM_TOD").Columns("GRUPO")
        dsResultado.Relations.Add("GRUPO_ITEM_TOD", parentCols, childCols, False)

        'Para saber si mostramos la info de destino, forma de pago y fechas de suministro de los item
        Dim itemsDest As DataRow() = dsResultado.Tables("PROCESO").Select("DEFDEST=2 OR DEFDEST=3")
        Dim itemsPag As DataRow() = dsResultado.Tables("PROCESO").Select("DEFPAG=2 OR DEFPAG=3")
        Dim itemsFecSum As DataRow() = dsResultado.Tables("PROCESO").Select("DEFFECSUM=2 OR DEFFECSUM=3")
        If Not itemsDest.Length = 0 Then
            For Each row As DataRow In itemsDest
                If row("DEFDEST") = 3 Then
                    For Each item As DataRow In dsResultado.Tables("ITEM_TOD").Select("ANYO=" & row("ANYO") & " AND GMN1_PROCE='" & row("GMN1") & _
                                                "' AND PROCE=" & row("COD"))
                        item("ITEM_DEST") = 1
                    Next
                Else
                    For Each datosGrupo As DataRow In dsResultado.Tables("GRUPO").Select("ANYO=" & row("ANYO") & " AND GMN1='" & row("GMN1") & "' AND PROCE=" & row("COD"))
                        If dsResultado.Tables("GR_DEST").Rows.Count = 0 OrElse dsResultado.Tables("GR_DEST").Select("ANYO=" & datosGrupo("ANYO") & " AND GMN1='" & datosGrupo("GMN1") & _
                                                "' AND PROCE=" & datosGrupo("PROCE") & " AND ID=" & datosGrupo("ID")).Length = 0 Then
                            For Each item As DataRow In dsResultado.Tables("ITEM_TOD").Select("ANYO=" & datosGrupo("ANYO") & " AND GMN1_PROCE='" & datosGrupo("GMN1") & _
                                                "' AND PROCE=" & datosGrupo("PROCE") & " AND GRUPO=" & datosGrupo("ID"))
                                item("ITEM_DEST") = 1
                            Next
                        End If
                    Next
                End If
            Next
        End If
        If Not itemsPag.Length = 0 Then
            For Each row As DataRow In itemsPag
                If row("DEFPAG") = 3 Then
                    For Each item As DataRow In dsResultado.Tables("ITEM_TOD").Select("ANYO=" & row("ANYO") & " AND GMN1_PROCE='" & row("GMN1") & _
                                                "' AND PROCE=" & row("COD"))
                        item("ITEM_PAG") = 1
                    Next
                Else
                    For Each datosGrupo As DataRow In dsResultado.Tables("GRUPO").Select("ANYO=" & row("ANYO") & " AND GMN1='" & row("GMN1") & "' AND PROCE=" & row("COD"))
                        If dsResultado.Tables("GR_PAGO").Rows.Count = 0 OrElse dsResultado.Tables("GR_PAGO").Select("ANYO=" & datosGrupo("ANYO") & " AND GMN1='" & datosGrupo("GMN1") & _
                                                "' AND PROCE=" & datosGrupo("PROCE") & " AND ID=" & datosGrupo("ID")).Length = 0 Then
                            For Each grupo As DataRow In dsResultado.Tables("ITEM_TOD").Select("ANYO=" & datosGrupo("ANYO") & " AND GMN1_PROCE='" & datosGrupo("GMN1") & _
                                                "' AND PROCE=" & datosGrupo("PROCE") & " AND GRUPO=" & datosGrupo("ID"))
                                grupo("ITEM_PAG") = 1
                            Next
                        End If
                    Next
                End If
            Next
        End If
        If Not itemsFecSum.Length = 0 Then
            For Each row As DataRow In itemsFecSum
                If row("DEFFECSUM") = 3 Then
                    For Each item As DataRow In dsResultado.Tables("ITEM_TOD").Select("ANYO=" & row("ANYO") & " AND GMN1_PROCE='" & row("GMN1") & _
                                                "' AND PROCE=" & row("COD"))
                        item("ITEM_FECSUM") = 1
                    Next
                Else
                    For Each datosGrupo As DataRow In dsResultado.Tables("GRUPO").Select("ANYO=" & row("ANYO") & " AND GMN1='" & row("GMN1") & "' AND PROCE=" & row("COD"))
                        If dsResultado.Tables("GR_FECSUM").Rows.Count = 0 OrElse dsResultado.Tables("GR_FECSUM").Select("ANYO=" & datosGrupo("ANYO") & " AND GMN1='" & datosGrupo("GMN1") & _
                                                "' AND PROCE=" & datosGrupo("PROCE") & " AND ID=" & datosGrupo("ID")).Length = 0 Then
                            For Each grupo As DataRow In dsResultado.Tables("ITEM_TOD").Select("ANYO=" & datosGrupo("ANYO") & " AND GMN1_PROCE='" & datosGrupo("GMN1") & _
                                                "' AND PROCE=" & datosGrupo("PROCE") & " AND GRUPO=" & datosGrupo("ID"))
                                grupo("ITEM_FECSUM") = 1
                            Next
                        End If
                    Next
                End If
            Next
        End If
        Dim query8 = From items In dsResultado.Tables("ITEM_TOD").AsEnumerable() _
            Where (items.Item("ITEM_DEST") = 1) _
            Select items Distinct
        If query8.Any Then
            dsResultado.Tables.Add(query8.CopyToDataTable)
            dsResultado.Tables(dsResultado.Tables.Count - 1).TableName = "IT_DEST"
            parentCols(0) = dsResultado.Tables("ITEM_TOD").Columns("ANYO")
            parentCols(1) = dsResultado.Tables("ITEM_TOD").Columns("GMN1_PROCE")
            parentCols(2) = dsResultado.Tables("ITEM_TOD").Columns("PROCE")
            parentCols(3) = dsResultado.Tables("ITEM_TOD").Columns("ID")
            childCols(0) = dsResultado.Tables("IT_DEST").Columns("ANYO")
            childCols(1) = dsResultado.Tables("IT_DEST").Columns("GMN1_PROCE")
            childCols(2) = dsResultado.Tables("IT_DEST").Columns("PROCE")
            childCols(3) = dsResultado.Tables("IT_DEST").Columns("ID")
            dsResultado.Relations.Add("ITEM_TOD_IT_DEST", parentCols, childCols, False)
        Else
            dsResultado.Tables.Add(New DataTable)
            dsResultado.Tables(dsResultado.Tables.Count - 1).TableName = "IT_DEST"
        End If
        Dim query9 = From items In dsResultado.Tables("ITEM_TOD").AsEnumerable() _
            Where (items.Item("ITEM_PAG") = 1) _
            Select items Distinct
        If query9.Any Then
            dsResultado.Tables.Add(query9.CopyToDataTable)
            dsResultado.Tables(dsResultado.Tables.Count - 1).TableName = "IT_PAGO"
            parentCols(0) = dsResultado.Tables("ITEM_TOD").Columns("ANYO")
            parentCols(1) = dsResultado.Tables("ITEM_TOD").Columns("GMN1_PROCE")
            parentCols(2) = dsResultado.Tables("ITEM_TOD").Columns("PROCE")
            parentCols(3) = dsResultado.Tables("ITEM_TOD").Columns("ID")
            childCols(0) = dsResultado.Tables("IT_PAGO").Columns("ANYO")
            childCols(1) = dsResultado.Tables("IT_PAGO").Columns("GMN1_PROCE")
            childCols(2) = dsResultado.Tables("IT_PAGO").Columns("PROCE")
            childCols(3) = dsResultado.Tables("IT_PAGO").Columns("ID")
            dsResultado.Relations.Add("ITEM_TOD_IT_PAGO", parentCols, childCols, False)
        Else
            dsResultado.Tables.Add(New DataTable)
            dsResultado.Tables(dsResultado.Tables.Count - 1).TableName = "IT_PAGO"
        End If
        Dim query10 = From items In dsResultado.Tables("ITEM_TOD").AsEnumerable() _
            Where (items.Item("ITEM_FECSUM") = 1) _
            Select items Distinct
        If query10.Any Then
            dsResultado.Tables.Add(query10.CopyToDataTable)
            dsResultado.Tables(dsResultado.Tables.Count - 1).TableName = "IT_FECSUM"
            parentCols(0) = dsResultado.Tables("ITEM_TOD").Columns("ANYO")
            parentCols(1) = dsResultado.Tables("ITEM_TOD").Columns("GMN1_PROCE")
            parentCols(2) = dsResultado.Tables("ITEM_TOD").Columns("PROCE")
            parentCols(3) = dsResultado.Tables("ITEM_TOD").Columns("ID")
            childCols(0) = dsResultado.Tables("IT_FECSUM").Columns("ANYO")
            childCols(1) = dsResultado.Tables("IT_FECSUM").Columns("GMN1_PROCE")
            childCols(2) = dsResultado.Tables("IT_FECSUM").Columns("PROCE")
            childCols(3) = dsResultado.Tables("IT_FECSUM").Columns("ID")
            dsResultado.Relations.Add("ITEM_TOD_IT_FECSUM", parentCols, childCols, False)
        Else
            dsResultado.Tables.Add(New DataTable)
            dsResultado.Tables(dsResultado.Tables.Count - 1).TableName = "IT_FECSUM"
        End If
        dt = New DataTable
        dt = dsDatosProceso.Tables("ADJUDICACIONES_ITEM").Copy
        dt.TableName = "PROVEEDOR"
        dsResultado.Tables.Add(dt)
        parentCols(0) = dsResultado.Tables("ITEM_TOD").Columns("ANYO")
        parentCols(1) = dsResultado.Tables("ITEM_TOD").Columns("GMN1_PROCE")
        parentCols(2) = dsResultado.Tables("ITEM_TOD").Columns("PROCE")
        parentCols(3) = dsResultado.Tables("ITEM_TOD").Columns("ID")
        childCols(0) = dsResultado.Tables("PROVEEDOR").Columns("ANYO")
        childCols(1) = dsResultado.Tables("PROVEEDOR").Columns("GMN1")
        childCols(2) = dsResultado.Tables("PROVEEDOR").Columns("PROCE")
        childCols(3) = dsResultado.Tables("PROVEEDOR").Columns("ITEM")
        dsResultado.Relations.Add("ITEM_TOD_PROVEEDOR", parentCols, childCols, False)
        Dim atributosItem As DataRow()
        For Each row As DataRow In dsResultado.Tables("ITEM_TOD").Rows
            For Each child As DataRow In row.GetChildRows("ITEM_TOD_PROVEEDOR")
                child("COD_UNI_PROVEEDOR") = row("COD_UNI")
                child("DEN_UNI_PROVEEDOR") = row("DEN_UNI")
                child("CANT_PROVEEDOR") = (child("PORCEN") / 100) * row("CANTIDAD")
                child("TOTAL_PROVEEDOR") = child("CANT_PROVEEDOR") * child("PRECIO_PROVEEDOR")
                atributosItem = dsDatosProceso.Tables("ATRIBUTOS").Select("APLIC_PREC=2 AND (" & _
                    "(ANYO=" & row("ANYO") & " AND GMN1='" & row("GMN1_PROCE") & "' AND PROCE=" & row("PROCE") & " AND GRUPO=0 AND ITEM=0) OR " & _
                    "(ANYO=" & row("ANYO") & " AND GMN1='" & row("GMN1_PROCE") & "' AND PROCE=" & row("PROCE") & " AND GRUPO=" & row("GRUPO") & " AND ITEM=0) OR " & _
                    "(ANYO=" & row("ANYO") & " AND GMN1='" & row("GMN1_PROCE") & "' AND PROCE=" & row("PROCE") & " AND GRUPO=0 AND ITEM=" & row("ID") & "))", "ORDEN")
                For Each atributo As DataRow In atributosItem
                    Select Case atributo("OP_PREC")
                        Case "+"
                            child("TOTAL_PROVEEDOR") = DBNullToDbl(child("TOTAL_PROVEEDOR")) + atributo("VALOR_NUM")
                        Case "-"
                            child("TOTAL_PROVEEDOR") = DBNullToDbl(child("TOTAL_PROVEEDOR")) - atributo("VALOR_NUM")
                        Case "/"
                            If IsDBNull(atributo("VALOR_NUM")) OrElse atributo("VALOR_NUM") = 0 Then
                                child("TOTAL_PROVEEDOR") = 0
                            Else
                                child("TOTAL_PROVEEDOR") = DBNullToDbl(child("TOTAL_PROVEEDOR")) / atributo("VALOR_NUM")
                            End If
                        Case "*"
                            child("TOTAL_PROVEEDOR") = DBNullToDbl(child("TOTAL_PROVEEDOR")) * atributo("VALOR_NUM")
                        Case "+%"
                            child("TOTAL_PROVEEDOR") = DBNullToDbl(child("TOTAL_PROVEEDOR")) + ((atributo("VALOR_NUM") / 100) * DBNullToDbl(child("TOTAL_PROVEEDOR")))
                        Case "-%"
                            child("TOTAL_PROVEEDOR") = DBNullToDbl(child("TOTAL_PROVEEDOR")) - ((atributo("VALOR_NUM") / 100) * DBNullToDbl(child("TOTAL_PROVEEDOR")))
                    End Select
                Next
            Next
        Next
        removeRows.Clear()
        If ProveedoresNoAdjudicados Then
            view = New DataView(dsDatosProceso.Tables("ADJUDICACIONES_ITEM"))
            dt = New DataTable
            dt = view.ToTable(True, "ANYO", "GMN1", "PROCE", "COD_PROVEEDOR", "ITEM")
            For Each row As DataRow In dt.Rows
                For Each prove As DataRow In dsDatosProceso.Tables("OFERTAS_ITEM").Select("ANYO=" & row("ANYO") & " AND GMN1='" & row("GMN1") & _
                                            "' AND PROCE=" & row("PROCE") & " AND COD_PROVEEDOR_NOADJ='" & row("COD_PROVEEDOR") & "' AND ITEM=" & row("ITEM"))
                    removeRows.Add(prove)
                Next
            Next
            For Each row As DataRow In removeRows
                dsDatosProceso.Tables("OFERTAS_ITEM").Rows.Remove(row)
            Next
            removeRows.Clear()
            For Each row As DataRow In dsDatosProceso.Tables("PROVEEDORES_NOADJUDICADOS").Rows
                If dsDatosProceso.Tables("ULTIMAS_OFERTAS_GRUPO").Select("ANYO=" & row("ANYO") & " AND GMN1='" & row("GMN1") & _
                                            "' AND PROCE=" & row("PROCE") & " AND PROVE='" & row("PROVECOD") & "'").Length = 0 Then
                    For Each prove As DataRow In dsDatosProceso.Tables("OFERTAS_ITEM").Select("ANYO=" & row("ANYO") & " AND GMN1='" & row("GMN1") & _
                                            "' AND PROCE=" & row("PROCE") & " AND COD_PROVEEDOR_NOADJ='" & row("PROVECOD") & "'")
                        removeRows.Add(prove)
                    Next
                End If
            Next
            For Each row As DataRow In removeRows
                dsDatosProceso.Tables("OFERTAS_ITEM").Rows.Remove(row)
            Next
            dt = New DataTable
            dt = dsDatosProceso.Tables("OFERTAS_ITEM").Copy
            dt.TableName = "PROVEEDOR_NOADJ"
            dsResultado.Tables.Add(dt)
            parentCols(0) = dsResultado.Tables("ITEM_TOD").Columns("ANYO")
            parentCols(1) = dsResultado.Tables("ITEM_TOD").Columns("GMN1_PROCE")
            parentCols(2) = dsResultado.Tables("ITEM_TOD").Columns("PROCE")
            parentCols(3) = dsResultado.Tables("ITEM_TOD").Columns("ID")
            childCols(0) = dsResultado.Tables("PROVEEDOR_NOADJ").Columns("ANYO")
            childCols(1) = dsResultado.Tables("PROVEEDOR_NOADJ").Columns("GMN1")
            childCols(2) = dsResultado.Tables("PROVEEDOR_NOADJ").Columns("PROCE")
            childCols(3) = dsResultado.Tables("PROVEEDOR_NOADJ").Columns("ITEM")
            dsResultado.Relations.Add("ITEM_TOD_PROVEEDOR_NOADJ", parentCols, childCols, False)
        End If
        Return dsResultado
    End Function
    Public Function Reunion_Acta_ItemsPorProveedorAdjudicado(ByVal fecha As DateTime, ByVal Idioma As String, _
                                                              Optional ByVal ProveedoresNoAdjudicados As Boolean = False) As DataSet
        Authenticate()
        Dim AdminPublica As Boolean
        Dim removeRows As New List(Of DataRow)
        Dim dsDatosProceso As DataSet = DBServer.GS_Reunion_Acta(fecha, Idioma)
        Dim dsResultado As New DataSet
        dsResultado.Tables.Add(dsDatosProceso.Tables("ACTA").Copy)
        'Desencripto el campo EST de los sobres para si es administración pública saber si el sobre esta cerrado/abierto
        For Each row As DataRow In dsDatosProceso.Tables("DATOSGRUPOS").Select("SOBRE IS NOT NULL")
            row("EST") = FSNLibrary.Encrypter.EncriptarAperturaSobre(row("ANYO") & row("PROCE") & row("SOBRE"), row("EST"), False)
        Next
        Dim parentCols(2) As DataColumn
        Dim childCols(2) As DataColumn
        parentCols(0) = dsDatosProceso.Tables("PROCESO").Columns("ANYO")
        parentCols(1) = dsDatosProceso.Tables("PROCESO").Columns("GMN1")
        parentCols(2) = dsDatosProceso.Tables("PROCESO").Columns("COD")
        childCols(0) = dsDatosProceso.Tables("MATERIALES_PROCESO").Columns("ANYO")
        childCols(1) = dsDatosProceso.Tables("MATERIALES_PROCESO").Columns("GMN1")
        childCols(2) = dsDatosProceso.Tables("MATERIALES_PROCESO").Columns("PROCE")
        dsDatosProceso.Relations.Add("PROCE_MATERIALES_PROCESO", parentCols, childCols, False)
        'Asigno a cada proceso la info del material
        Dim mat As DataRow
        For Each row As DataRow In dsDatosProceso.Tables("PROCESO").Rows
            If Not row.GetChildRows("PROCE_MATERIALES_PROCESO").Length = 0 Then
                mat = row.GetChildRows("PROCE_MATERIALES_PROCESO")(0)
                row("MAT1_PROCESO") = mat("MAT1_PROCESO")
                row("MAT2_PROCESO") = mat("MAT2_PROCESO")
                row("MAT3_PROCESO") = mat("MAT3_PROCESO")
                row("MAT4_PROCESO") = mat("MAT4_PROCESO")
                row("DMAT1_PROCESO") = mat("DMAT1_PROCESO")
                row("DMAT2_PROCESO") = mat("DMAT2_PROCESO")
                row("DMAT3_PROCESO") = mat("DMAT3_PROCESO")
                row("DMAT4_PROCESO") = mat("DMAT4_PROCESO")
            End If
        Next
        dsResultado.Tables.Add(dsDatosProceso.Tables("PROCESO").Copy)
        Dim timeDif As Integer = 0
        Dim horaAnterior As DateTime
        If Not dsResultado.Tables("PROCESO").Rows.Count = 0 Then horaAnterior = CType(dsResultado.Tables("PROCESO").Rows(0)("HORA_PROCESO"), DateTime)
        Dim UON_COD_PROCESO, UON_DEN_PROCESO As String
        UON_COD_PROCESO = String.Empty
        UON_DEN_PROCESO = String.Empty
        For Each Row As DataRow In dsResultado.Tables("PROCESO").Rows
            UON_COD_PROCESO = String.Empty
            UON_DEN_PROCESO = String.Empty
            timeDif += DateDiff(DateInterval.Second, horaAnterior, CType(Row("HORA_PROCESO"), DateTime))
            horaAnterior = CType(Row("HORA_PROCESO"), DateTime)
            UON_COD_PROCESO = IIf(IsDBNull(Row("UON1_APER")), "", Row("UON1_APER") & _
                                IIf(IsDBNull(Row("UON2_APER")), "", "-" & Row("UON2_APER") & _
                                    IIf(IsDBNull(Row("UON3_APER")), "", "-" & Row("UON3_APER"))))
            UON_DEN_PROCESO = IIf(IsDBNull(Row("UON1_APER")), "", _
                                  IIf(IsDBNull(Row("UON2_APER")), Row("UON1_APER_DEN"), _
                                    IIf(IsDBNull(Row("UON3_APER")), Row("UON2_APER_DEN"), Row("UON3_APER_DEN"))))
            Row("UON_COD_PROCESO") = UON_COD_PROCESO
            Row("UON_DEN_PROCESO") = "-" & UON_DEN_PROCESO
        Next
        dsResultado.Tables("ACTA").Rows(0)("HORA_FIN_REU") = DateAdd(DateInterval.Second, timeDif, dsResultado.Tables("ACTA").Rows(0)("HORA_INI_REU"))
        'Si mostramos la info de destino en el proceso
        Dim query1 = From Datos In dsResultado.Tables("PROCESO").AsEnumerable() _
            Where (Datos.Item("DEFDEST") = 1) _
            Order By Datos.Item("NUM_PROCESO") _
            Select Datos Distinct
        If query1.Any Then
            dsResultado.Tables.Add(query1.CopyToDataTable)
            dsResultado.Tables(dsResultado.Tables.Count - 1).TableName = "DEST_PROCE"
            parentCols(0) = dsResultado.Tables("PROCESO").Columns("ANYO")
            parentCols(1) = dsResultado.Tables("PROCESO").Columns("GMN1")
            parentCols(2) = dsResultado.Tables("PROCESO").Columns("COD")
            childCols(0) = dsResultado.Tables("DEST_PROCE").Columns("ANYO")
            childCols(1) = dsResultado.Tables("DEST_PROCE").Columns("GMN1")
            childCols(2) = dsResultado.Tables("DEST_PROCE").Columns("COD")
            dsResultado.Relations.Add("PROCE_DEST_PROCE", parentCols, childCols, False)
        Else
            dsResultado.Tables.Add(New DataTable)
            dsResultado.Tables(dsResultado.Tables.Count - 1).TableName = "DEST_PROCE"
        End If
        'Si mostramos la info de forma de pago en el proceso
        Dim query2 = From Datos In dsResultado.Tables("PROCESO").AsEnumerable() _
            Where (Datos.Item("DEFPAG") = 1) _
            Order By Datos.Item("NUM_PROCESO") _
            Select Datos Distinct
        If query2.Any Then
            dsResultado.Tables.Add(query2.CopyToDataTable)
            dsResultado.Tables(dsResultado.Tables.Count - 1).TableName = "PAGO_PROCE"
            parentCols(0) = dsResultado.Tables("PROCESO").Columns("ANYO")
            parentCols(1) = dsResultado.Tables("PROCESO").Columns("GMN1")
            parentCols(2) = dsResultado.Tables("PROCESO").Columns("COD")
            childCols(0) = dsResultado.Tables("PAGO_PROCE").Columns("ANYO")
            childCols(1) = dsResultado.Tables("PAGO_PROCE").Columns("GMN1")
            childCols(2) = dsResultado.Tables("PAGO_PROCE").Columns("COD")
            dsResultado.Relations.Add("PROCE_PAGO_PROCE", parentCols, childCols, False)
        Else
            dsResultado.Tables.Add(New DataTable)
            dsResultado.Tables(dsResultado.Tables.Count - 1).TableName = "PAGO_PROCE"
        End If
        'Si mostramos la info de fechas de suministro en el proceso
        Dim query3 = From Datos In dsResultado.Tables("PROCESO").AsEnumerable() _
            Where (Datos.Item("DEFFECSUM") = 1) _
           Order By Datos.Item("NUM_PROCESO") _
           Select Datos Distinct
        If query3.Any Then
            dsResultado.Tables.Add(query3.CopyToDataTable)
            dsResultado.Tables(dsResultado.Tables.Count - 1).TableName = "FECSUM_PROCE"
            parentCols(0) = dsResultado.Tables("PROCESO").Columns("ANYO")
            parentCols(1) = dsResultado.Tables("PROCESO").Columns("GMN1")
            parentCols(2) = dsResultado.Tables("PROCESO").Columns("COD")
            childCols(0) = dsResultado.Tables("FECSUM_PROCE").Columns("ANYO")
            childCols(1) = dsResultado.Tables("FECSUM_PROCE").Columns("GMN1")
            childCols(2) = dsResultado.Tables("FECSUM_PROCE").Columns("COD")
            dsResultado.Relations.Add("PROCE_FECSUM_PROCE", parentCols, childCols, False)
        Else
            dsResultado.Tables.Add(New DataTable)
            dsResultado.Tables(dsResultado.Tables.Count - 1).TableName = "FECSUM_PROCE"
        End If
        'La info del responsable del proceso
        Dim query4 = From Datos In dsResultado.Tables("PROCESO").AsEnumerable() _
            Where (Datos.Item("COD_RESPONSABLE") IsNot Nothing) _
           Order By Datos.Item("NUM_PROCESO") _
           Select Datos Distinct
        If query4.Any Then
            dsResultado.Tables.Add(query4.CopyToDataTable)
            dsResultado.Tables(dsResultado.Tables.Count - 1).TableName = "OPC_RESPONSABLE"
            parentCols(0) = dsResultado.Tables("PROCESO").Columns("ANYO")
            parentCols(1) = dsResultado.Tables("PROCESO").Columns("GMN1")
            parentCols(2) = dsResultado.Tables("PROCESO").Columns("COD")
            childCols(0) = dsResultado.Tables("OPC_RESPONSABLE").Columns("ANYO")
            childCols(1) = dsResultado.Tables("OPC_RESPONSABLE").Columns("GMN1")
            childCols(2) = dsResultado.Tables("OPC_RESPONSABLE").Columns("COD")
            dsResultado.Relations.Add("PROCE_OPC_RESPONSABLE", parentCols, childCols, False)
        Else
            dsResultado.Tables.Add(New DataTable)
            dsResultado.Tables(dsResultado.Tables.Count - 1).TableName = "OPC_RESPONSABLE"
        End If
        'Implicados en el proceso
        Dim view As New DataView(dsDatosProceso.Tables("IMPLICADO"))
        Dim dt As DataTable
        dt = view.ToTable(True, "ANYO", "GMN1", "PROCE")
        dt.TableName = "IMPLICADOS" 'Solo nos sirve para saber si mostrar la lista de implicados
        dsResultado.Tables.Add(dt)
        parentCols(0) = dsResultado.Tables("PROCESO").Columns("ANYO")
        parentCols(1) = dsResultado.Tables("PROCESO").Columns("GMN1")
        parentCols(2) = dsResultado.Tables("PROCESO").Columns("COD")
        childCols(0) = dsResultado.Tables("IMPLICADOS").Columns("ANYO")
        childCols(1) = dsResultado.Tables("IMPLICADOS").Columns("GMN1")
        childCols(2) = dsResultado.Tables("IMPLICADOS").Columns("PROCE")
        dsResultado.Relations.Add("PROCE_IMPLICADOS", parentCols, childCols, False)
        dsResultado.Tables.Add(dsDatosProceso.Tables("IMPLICADO").Copy)
        parentCols(0) = dsResultado.Tables("IMPLICADOS").Columns("ANYO")
        parentCols(1) = dsResultado.Tables("IMPLICADOS").Columns("GMN1")
        parentCols(2) = dsResultado.Tables("IMPLICADOS").Columns("PROCE")
        childCols(0) = dsResultado.Tables("IMPLICADO").Columns("ANYO")
        childCols(1) = dsResultado.Tables("IMPLICADO").Columns("GMN1")
        childCols(2) = dsResultado.Tables("IMPLICADO").Columns("PROCE")
        dsResultado.Relations.Add("IMPLICADOS_IMPLICADO", parentCols, childCols, False)
        If Not dsDatosProceso.Tables("ASISTENTE").Rows.Count = 0 Then
            view = New DataView(dsDatosProceso.Tables("ASISTENTE"))
            dt = New DataTable
            dt = view.ToTable(True, "ASISTENTE")
            dt.TableName = "ASISTENTES" 'Solo nos sirve para saber si mostrar la lista de ASISTENTES
            dsResultado.Tables.Add(dt)
            dsResultado.Tables.Add(dsDatosProceso.Tables("ASISTENTE").Copy)
            For Each row As DataRow In dsResultado.Tables("ASISTENTE").Rows
                row("UO_ASIST") = IIf(IsDBNull(row("UON1")), "", row("UON1")) & _
                                    IIf(IsDBNull(row("UON2")), "", "-" & row("UON2")) & _
                                    IIf(IsDBNull(row("UON3")), "", "-" & row("UON3"))
            Next
        End If
        'Datos del resumen economico
        view = New DataView(dsDatosProceso.Tables("PROCESO"))
        dt = New DataTable
        dt = view.ToTable(True, "ANYO", "GMN1", "COD", "ADMIN_PUB", "PRES_GEN", "ADJ_GEN", "AHORRO_GEN", "PORCEN_GEN")
        dt.TableName = "OPT_RESUMEN"
        dsResultado.Tables.Add(dt)
        view = New DataView(dsDatosProceso.Tables("DATOSGRUPOS"))
        dt = New DataTable
        dt = view.ToTable(True, "ID", "ANYO", "GMN1", "PROCE", "COD_GRUPO", "DEN_GRUPO", "EST", "PRES_GR", "ADJ_GR", "AHORRO_GR", "PORCEN_GR_RES")
        dt.TableName = "RESUMEN_GR"
        dt.Columns("COD_GRUPO").ColumnName = "GR_COD_RES"
        dt.Columns("DEN_GRUPO").ColumnName = "GR_DEN_RES"
        dsResultado.Tables.Add(dt)
        parentCols(0) = dsResultado.Tables("PROCESO").Columns("ANYO")
        parentCols(1) = dsResultado.Tables("PROCESO").Columns("GMN1")
        parentCols(2) = dsResultado.Tables("PROCESO").Columns("COD")
        childCols(0) = dsResultado.Tables("OPT_RESUMEN").Columns("ANYO")
        childCols(1) = dsResultado.Tables("OPT_RESUMEN").Columns("GMN1")
        childCols(2) = dsResultado.Tables("OPT_RESUMEN").Columns("COD")
        dsResultado.Relations.Add("PROCE_OPT_RESUMEN", parentCols, childCols, False)
        parentCols(0) = dsResultado.Tables("OPT_RESUMEN").Columns("ANYO")
        parentCols(1) = dsResultado.Tables("OPT_RESUMEN").Columns("GMN1")
        parentCols(2) = dsResultado.Tables("OPT_RESUMEN").Columns("COD")
        childCols(0) = dsResultado.Tables("RESUMEN_GR").Columns("ANYO")
        childCols(1) = dsResultado.Tables("RESUMEN_GR").Columns("GMN1")
        childCols(2) = dsResultado.Tables("RESUMEN_GR").Columns("PROCE")
        dsResultado.Relations.Add("OPT_RESUMEN_RESUMEN_GR", parentCols, childCols, False)
        removeRows.Clear()
        Dim PROCE_PROVE_GRUPOS As Boolean = CType(dsDatosProceso.Tables("ACTA").Rows(0)("PROCE_PROVE_GRUPOS"), Boolean)
        For Each row As DataRow In dsResultado.Tables("RESUMEN_GR").Rows
            row("PRES_GR") = 0
            row("ADJ_GR") = 0
            row("AHORRO_GR") = 0
            If row.GetParentRow("OPT_RESUMEN_RESUMEN_GR") IsNot Nothing Then
                AdminPublica = row.GetParentRow("OPT_RESUMEN_RESUMEN_GR")("ADMIN_PUB")
            Else
                AdminPublica = False
            End If
            If AdminPublica AndAlso row("EST") = "Cerrado" Then
                removeRows.Add(row)
            Else
                For Each prove As DataRow In dsDatosProceso.Tables("ADJUDICACIONES").Select("ANYO=" & row("ANYO") & " AND GMN1='" & row("GMN1") & _
                                                "' AND PROCE=" & row("PROCE") & " AND GRUPO=" & row("ID"))
                    If Not PROCE_PROVE_GRUPOS OrElse Not dsDatosProceso.Tables("GRUPOS_ASIGNACIONES").Select("ANYO=" & row("ANYO") & " AND GMN1='" & row("GMN1") & _
                                                "' AND PROCE=" & row("PROCE") & " AND ID=" & row("ID") & " AND PROVE='" & prove("PROVE") & "'").Length = 0 Then
                        row("PRES_GR") += DBNullToDbl(prove("CONSUMIDO"))
                        row("ADJ_GR") += DBNullToDbl(prove("ADJUDICADO"))
                    End If
                Next
                row("AHORRO_GR") += DBNullToDbl(row("PRES_GR")) - DBNullToDbl(row("ADJ_GR"))
                If IsDBNull(row("PRES_GR")) OrElse row("PRES_GR") = 0 Then
                    row("PORCEN_GR_RES") = 0
                Else
                    row("PORCEN_GR_RES") = (row("AHORRO_GR") / row("PRES_GR")) * 100
                End If
            End If
        Next
        For Each row As DataRow In removeRows
            dsResultado.Tables("RESUMEN_GR").Rows.Remove(row)
        Next
        view = New DataView(dsDatosProceso.Tables("PROVEEDORES"))
        dt = New DataTable
        dt = view.ToTable(True, "ANYO", "GMN1", "PROCE", "PROVECOD", "PROVEDEN")
        dt.TableName = "RESUMEN_PROV"
        dt.Columns("PROVECOD").ColumnName = "PROV_COD_RES"
        dt.Columns("PROVEDEN").ColumnName = "PROV_DEN_RES"
        dt.Columns.Add("PRES_TOT_PROV", GetType(System.Double))
        dt.Columns.Add("OFE_TOT_PROV", GetType(System.Double))
        dt.Columns.Add("AHORRO_TOT_PROV", GetType(System.Double))
        dt.Columns.Add("PORCEN_TOT_PROV", GetType(System.Double))
        dsResultado.Tables.Add(dt)
        For Each rowIterate As DataRow In dsResultado.Tables("RESUMEN_PROV").Rows
            Dim row As DataRow = rowIterate
            row("PRES_TOT_PROV") = 0
            row("OFE_TOT_PROV") = 0
            row("AHORRO_TOT_PROV") = 0
            row("PORCEN_TOT_PROV") = 0
            If Not dsResultado.Tables("RESUMEN_GR").Select("ANYO=" & row("ANYO") & " AND GMN1='" & row("GMN1") & "' AND PROCE=" & row("PROCE")).Length = 0 Then
                For Each grupo As DataRow In dsDatosProceso.Tables("ADJUDICACIONES").Select("ANYO=" & row("ANYO") & " AND GMN1='" & row("GMN1") & _
                                                "' AND PROCE=" & row("PROCE") & " AND PROVE='" & row("PROV_COD_RES") & "'")
                    If Not PROCE_PROVE_GRUPOS OrElse Not dsDatosProceso.Tables("GRUPOS_ASIGNACIONES").Select("ANYO=" & row("ANYO") & " AND GMN1='" & row("GMN1") & _
                                                    "' AND PROCE=" & row("PROCE") & " AND ID=" & grupo("GRUPO") & " AND PROVE='" & row("PROV_COD_RES") & "'").Length = 0 Then
                        row("PRES_TOT_PROV") = dsDatosProceso.Tables("ADJUDICACIONES").Compute("SUM(CONSUMIDO)", _
                                    "ANYO=" & row("ANYO") & " AND GMN1='" & row("GMN1") & "' AND PROCE=" & row("PROCE") & " AND PROVE='" & row("PROV_COD_RES") & "'")
                        row("OFE_TOT_PROV") = dsDatosProceso.Tables("ADJUDICACIONES").Compute("SUM(ADJUDICADO)", _
                                    "ANYO=" & row("ANYO") & " AND GMN1='" & row("GMN1") & "' AND PROCE=" & row("PROCE") & " AND PROVE='" & row("PROV_COD_RES") & "'")
                    End If
                Next
                Dim atributos = From atrib In dsDatosProceso.Tables("ATRIBUTOS").AsEnumerable()
                    Join adj In dsDatosProceso.Tables("ADJUDICACIONES").AsEnumerable() On atrib.Item("ANYO") Equals adj.Item("ANYO") _
                        And atrib.Item("GMN1") Equals adj.Item("GMN1") And atrib.Item("PROCE") Equals adj.Item("PROCE") _
                        And atrib.Item("PROVE") Equals adj.Item("PROVE") And atrib.Item("OFE") Equals adj.Item("OFE") _
                    Join prove In dsDatosProceso.Tables("PROVEEDORES").AsEnumerable On prove.Item("ANYO") Equals adj.Item("ANYO") _
                        And prove.Item("GMN1") Equals adj.Item("GMN1") And prove.Item("PROCE") Equals adj.Item("PROCE") _
                        And prove.Item("PROVECOD") Equals adj.Item("PROVE") _
                Where (atrib.Item("ANYO") = row("ANYO") And atrib.Item("GMN1") = row("GMN1") And atrib.Item("PROCE") = row("PROCE") _
                    And atrib.Item("PROVE") = row("PROV_COD_RES") And atrib.Item("APLIC_PREC") = 0)
                Select atrib Distinct
                If atributos.Any Then
                    For Each atributo As DataRow In atributos
                        Select Case atributo("OP_PREC")
                            Case "+"
                                row("OFE_TOT_PROV") = DBNullToDbl(row("OFE_TOT_PROV")) + atributo("VALOR_NUM")
                            Case "-"
                                row("OFE_TOT_PROV") = DBNullToDbl(row("OFE_TOT_PROV")) - atributo("VALOR_NUM")
                            Case "/"
                                If IsDBNull(atributo("VALOR_NUM")) OrElse atributo("VALOR_NUM") = 0 Then
                                    row("OFE_TOT_PROV") = 0
                                Else
                                    row("OFE_TOT_PROV") = DBNullToDbl(row("OFE_TOT_PROV")) / atributo("VALOR_NUM")
                                End If
                            Case "*"
                                row("OFE_TOT_PROV") = DBNullToDbl(row("OFE_TOT_PROV")) * atributo("VALOR_NUM")
                            Case "+%"
                                row("OFE_TOT_PROV") = DBNullToDbl(row("OFE_TOT_PROV")) + ((atributo("VALOR_NUM") / 100) * DBNullToDbl(row("OFE_TOT_PROV")))
                            Case "-%"
                                row("OFE_TOT_PROV") = DBNullToDbl(row("OFE_TOT_PROV")) - ((atributo("VALOR_NUM") / 100) * DBNullToDbl(row("OFE_TOT_PROV")))
                        End Select
                    Next
                End If
                row("AHORRO_TOT_PROV") = DBNullToDbl(row("PRES_TOT_PROV")) - DBNullToDbl(row("OFE_TOT_PROV"))
                If IsDBNull(row("PRES_TOT_PROV")) OrElse row("PRES_TOT_PROV") = 0 Then
                    row("PORCEN_TOT_PROV") = 0
                Else
                    row("PORCEN_TOT_PROV") = (row("AHORRO_TOT_PROV") / row("PRES_TOT_PROV")) * 100
                End If
            End If
        Next
        For Each row As DataRow In dsResultado.Tables("OPT_RESUMEN").Rows
            row("PRES_GEN") = dsResultado.Tables("RESUMEN_PROV").Compute("SUM(PRES_TOT_PROV)", _
                                "ANYO=" & row("ANYO") & " AND GMN1='" & row("GMN1") & "' AND PROCE=" & row("COD"))
            row("ADJ_GEN") = dsResultado.Tables("RESUMEN_PROV").Compute("SUM(OFE_TOT_PROV)", _
                                "ANYO=" & row("ANYO") & " AND GMN1='" & row("GMN1") & "' AND PROCE=" & row("COD"))
            row("AHORRO_GEN") = DBNullToDbl(row("PRES_GEN")) - DBNullToDbl(row("ADJ_GEN"))
            If IsDBNull(row("PRES_GEN")) OrElse row("PRES_GEN") = 0 Then
                row("PORCEN_GEN") = 0
            Else
                row("PORCEN_GEN") = (row("AHORRO_GEN") / row("PRES_GEN")) * 100
            End If
        Next
        view = New DataView(dsResultado.Tables("PROCESO"))
        dt = New DataTable
        dt = dsResultado.Tables("OPT_RESUMEN").Copy
        dt.TableName = "OPT_RESUMEN_PROV"
        dt.Columns("PRES_GEN").ColumnName = "PRES_TOT"
        dt.Columns("ADJ_GEN").ColumnName = "OFE_TOT"
        dt.Columns("AHORRO_GEN").ColumnName = "AHORRO_TOT"
        dt.Columns("PORCEN_GEN").ColumnName = "PORCEN_TOT"
        dsResultado.Tables.Add(dt)
        parentCols(0) = dsResultado.Tables("PROCESO").Columns("ANYO")
        parentCols(1) = dsResultado.Tables("PROCESO").Columns("GMN1")
        parentCols(2) = dsResultado.Tables("PROCESO").Columns("COD")
        childCols(0) = dsResultado.Tables("OPT_RESUMEN_PROV").Columns("ANYO")
        childCols(1) = dsResultado.Tables("OPT_RESUMEN_PROV").Columns("GMN1")
        childCols(2) = dsResultado.Tables("OPT_RESUMEN_PROV").Columns("COD")
        dsResultado.Relations.Add("PROCE_OPT_RESUMEN_PROV", parentCols, childCols, False)
        parentCols(0) = dsResultado.Tables("OPT_RESUMEN_PROV").Columns("ANYO")
        parentCols(1) = dsResultado.Tables("OPT_RESUMEN_PROV").Columns("GMN1")
        parentCols(2) = dsResultado.Tables("OPT_RESUMEN_PROV").Columns("COD")
        childCols(0) = dsResultado.Tables("RESUMEN_PROV").Columns("ANYO")
        childCols(1) = dsResultado.Tables("RESUMEN_PROV").Columns("GMN1")
        childCols(2) = dsResultado.Tables("RESUMEN_PROV").Columns("PROCE")
        dsResultado.Relations.Add("OPT_RESUMEN_PROV_RESUMEN_PROV", parentCols, childCols, False)
        If ProveedoresNoAdjudicados Then
            view = New DataView(dsDatosProceso.Tables("PROVEEDORES_NOADJUDICADOS"))
            dt = New DataTable
            dt = view.ToTable(True, "ANYO", "GMN1", "PROCE")
            dt.TableName = "OPT_RESUMEN_PROV_NOADJ"
            dsResultado.Tables.Add(dt)
            view = New DataView(dsDatosProceso.Tables("PROVEEDORES_NOADJUDICADOS"))
            dt = New DataTable
            dt = view.ToTable(True, "ANYO", "GMN1", "PROCE", "PROVECOD", "PROVEDEN")
            dt.TableName = "RESUMEN_PROV_NOAJ"
            dt.Columns("PROVECOD").ColumnName = "PROV_COD_RES_NOADJ"
            dt.Columns("PROVEDEN").ColumnName = "PROV_DEN_RES_NOADJ"
            dt.Columns.Add("PRES_TOT_PROV_NOADJ", GetType(System.Double))
            dt.Columns.Add("OFE_TOT_PROV_NOADJ", GetType(System.Double))
            dt.Columns.Add("AHORRO_TOT_PROV_NOADJ", GetType(System.Double))
            dt.Columns.Add("PORCEN_TOT_PROV_NOADJ", GetType(System.Double))
            dt.Columns.Add("OFE", GetType(System.Int32))
            dsResultado.Tables.Add(dt)
            parentCols(0) = dsResultado.Tables("PROCESO").Columns("ANYO")
            parentCols(1) = dsResultado.Tables("PROCESO").Columns("GMN1")
            parentCols(2) = dsResultado.Tables("PROCESO").Columns("COD")
            childCols(0) = dsResultado.Tables("OPT_RESUMEN_PROV_NOADJ").Columns("ANYO")
            childCols(1) = dsResultado.Tables("OPT_RESUMEN_PROV_NOADJ").Columns("GMN1")
            childCols(2) = dsResultado.Tables("OPT_RESUMEN_PROV_NOADJ").Columns("PROCE")
            dsResultado.Relations.Add("PROCE_OPT_RESUMEN_PROV_NOADJ", parentCols, childCols, False)
            parentCols(0) = dsResultado.Tables("OPT_RESUMEN_PROV_NOADJ").Columns("ANYO")
            parentCols(1) = dsResultado.Tables("OPT_RESUMEN_PROV_NOADJ").Columns("GMN1")
            parentCols(2) = dsResultado.Tables("OPT_RESUMEN_PROV_NOADJ").Columns("PROCE")
            childCols(0) = dsResultado.Tables("RESUMEN_PROV_NOAJ").Columns("ANYO")
            childCols(1) = dsResultado.Tables("RESUMEN_PROV_NOAJ").Columns("GMN1")
            childCols(2) = dsResultado.Tables("RESUMEN_PROV_NOAJ").Columns("PROCE")
            dsResultado.Relations.Add("OPT_RESUMEN_PROV_NOADJ_RESUMEN_PROV_NOAJ", parentCols, childCols, False)
            Dim atributosProveNoAdj As DataRow()
            For Each row As DataRow In dsResultado.Tables("RESUMEN_PROV_NOAJ").Rows
                row("PRES_TOT_PROV_NOADJ") = 0
                row("OFE_TOT_PROV_NOADJ") = 0
                row("AHORRO_TOT_PROV_NOADJ") = 0
                row("PORCEN_TOT_PROV_NOADJ") = 0
                row("OFE") = 0
                For Each grupo As DataRow In dsDatosProceso.Tables("ADJUDICACIONES").Select("ANYO=" & row("ANYO") & " AND GMN1='" & row("GMN1") & _
                                                "' AND PROCE=" & row("PROCE") & " AND PROVE='" & row("PROV_COD_RES_NOADJ") & "'")
                    row("OFE") = grupo("OFE")
                    If Not PROCE_PROVE_GRUPOS OrElse Not dsDatosProceso.Tables("GRUPOS_ASIGNACIONES").Select("ANYO=" & row("ANYO") & " AND GMN1='" & row("GMN1") & _
                                                    "' AND PROCE=" & row("PROCE") & " AND ID=" & grupo("GRUPO") & " AND PROVE='" & row("PROV_COD_RES_NOADJ") & "'").Length = 0 Then
                        row("PRES_TOT_PROV_NOADJ") = dsDatosProceso.Tables("ADJUDICACIONES").Compute("SUM(CONSUMIDO)", _
                                    "ANYO=" & row("ANYO") & " AND GMN1='" & row("GMN1") & "' AND PROCE=" & row("PROCE"))
                        row("OFE_TOT_PROV_NOADJ") = dsDatosProceso.Tables("ADJUDICACIONES").Compute("SUM(ADJUDICADO)", _
                                    "ANYO=" & row("ANYO") & " AND GMN1='" & row("GMN1") & "' AND PROCE=" & row("PROCE"))
                    End If
                Next
                atributosProveNoAdj = dsDatosProceso.Tables("ATRIBUTOS").Select("ANYO=" & row("ANYO") & " AND GMN1='" & row("GMN1") & _
                            "' AND PROCE=" & row("PROCE") & " AND OFE=" & row("OFE") & " AND PROVE='" & row("PROV_COD_RES_NOADJ") & "' AND APLIC_PREC=0", "ORDEN")
                If Not atributosProveNoAdj.Length = 0 Then
                    For Each atributo As DataRow In atributosProveNoAdj
                        Select Case atributo("OP_PREC")
                            Case "+"
                                row("OFE_TOT_PROV_NOADJ") = DBNullToDbl(row("OFE_TOT_PROV_NOADJ")) + atributo("VALOR_NUM")
                            Case "-"
                                row("OFE_TOT_PROV_NOADJ") = DBNullToDbl(row("OFE_TOT_PROV_NOADJ")) - atributo("VALOR_NUM")
                            Case "/"
                                If IsDBNull(atributo("VALOR_NUM")) OrElse atributo("VALOR_NUM") = 0 Then
                                    row("OFE_TOT_PROV_NOADJ") = 0
                                Else
                                    row("OFE_TOT_PROV_NOADJ") = DBNullToDbl(row("OFE_TOT_PROV_NOADJ")) / atributo("VALOR_NUM")
                                End If
                            Case "*"
                                row("OFE_TOT_PROV_NOADJ") = DBNullToDbl(row("OFE_TOT_PROV_NOADJ")) * atributo("VALOR_NUM")
                            Case "+%"
                                row("OFE_TOT_PROV_NOADJ") = DBNullToDbl(row("OFE_TOT_PROV_NOADJ")) + ((atributo("VALOR_NUM") / 100) * DBNullToDbl(row("OFE_TOT_PROV_NOADJ")))
                            Case "-%"
                                row("OFE_TOT_PROV_NOADJ") = DBNullToDbl(row("OFE_TOT_PROV_NOADJ")) - ((atributo("VALOR_NUM") / 100) * DBNullToDbl(row("OFE_TOT_PROV_NOADJ")))
                        End Select
                    Next
                End If
                row("AHORRO_TOT_PROV_NOADJ") = DBNullToDbl(row("PRES_TOT_PROV_NOADJ")) - DBNullToDbl(row("OFE_TOT_PROV_NOADJ"))
                If IsDBNull(row("PRES_TOT_PROV_NOADJ")) OrElse row("PRES_TOT_PROV_NOADJ") = 0 Then
                    row("PORCEN_TOT_PROV_NOADJ") = 0
                Else
                    row("PORCEN_TOT_PROV_NOADJ") = (row("AHORRO_TOT_PROV_NOADJ") / row("PRES_TOT_PROV_NOADJ")) * 100
                End If
            Next
        End If
        removeRows.Clear()
        For Each row As DataRow In dsResultado.Tables("OPT_RESUMEN_PROV").Rows
            If row.GetChildRows("OPT_RESUMEN_PROV_RESUMEN_PROV").Length = 0 Then
                removeRows.Add(row)
            End If
        Next
        Dim removeGroups As New List(Of DataRow)
        'Obtenemos los diferentes grupos de cada proceso.
        view = New DataView(dsDatosProceso.Tables("DATOSGRUPOS"))
        dt = New DataTable
        dt = view.ToTable(True, "ID", "ANYO", "GMN1", "PROCE", "COD_GRUPO", "DEN_GRUPO", "COD_DEST_GR", "DEN_DEST_GR", "EST", _
                "COD_PAGO_GR", "DEN_PAGO_GR", "FINI_SUM_GR", "FFIN_SUM_GR")
        dt.TableName = "GRUPO"
        dsDatosProceso.Tables.Add(dt)
        dsDatosProceso.Tables("GRUPO").Columns.Add("PROVE", GetType(System.String))
        For Each row As DataRow In removeRows
            dsResultado.Tables("OPT_RESUMEN").Rows.Remove(dsResultado.Tables("OPT_RESUMEN").Select("ANYO=" & row("ANYO") & _
                                                            " AND GMN1='" & row("GMN1") & "' AND COD=" & row("COD"))(0))
            For Each grupo As DataRow In dsDatosProceso.Tables("GRUPO").Select("ANYO=" & row("ANYO") & _
                                                            " AND GMN1='" & row("GMN1") & "' AND PROCE=" & row("COD"))
                removeGroups.Add(grupo)
            Next
        Next
        For Each row As DataRow In removeRows
            dsResultado.Tables("OPT_RESUMEN_PROV").Rows.Remove(row)
        Next
        For Each row As DataRow In removeGroups
            dsDatosProceso.Tables("GRUPO").Rows.Remove(row)
        Next
        If ProveedoresNoAdjudicados Then
            removeRows.Clear()
            For Each row As DataRow In dsResultado.Tables("OPT_RESUMEN_PROV_NOADJ").Rows
                If row.GetChildRows("OPT_RESUMEN_PROV_NOADJ_RESUMEN_PROV_NOAJ").Length = 0 Then
                    removeRows.Add(row)
                End If
            Next
            For Each row As DataRow In removeRows
                dsResultado.Tables("OPT_RESUMEN_PROV_NOADJ").Rows.Remove(row)
            Next
        End If
        dt = New DataTable
        dt = dsDatosProceso.Tables("ADJUDICACIONES_ITEM").Clone
        dt.TableName = "PROVEEDOR"
        dt.Columns.Add("ADJ_PROV", GetType(System.Double))
        dt.Columns.Add("AHORR_PROV", GetType(System.Double))
        dt.Columns.Add("PORCEN_PROV", GetType(System.Double))
        dsResultado.Tables.Add(dt)
        Dim proveedor As DataRow()
        Dim proveedorAdj As DataRow
        For Each row As DataRow In dsDatosProceso.Tables("PROVEEDORES").Rows
            proveedor = dsResultado.Tables("RESUMEN_PROV").Select("ANYO=" & row("ANYO") & " AND GMN1='" & row("GMN1") & "' AND PROCE=" & row("PROCE") & _
                                                                  " AND PROV_COD_RES='" & row("PROVECOD") & "'")
            If proveedor.Length = 1 Then
                proveedorAdj = dsResultado.Tables("PROVEEDOR").NewRow
                proveedorAdj("ANYO") = row("ANYO")
                proveedorAdj("GMN1") = row("GMN1")
                proveedorAdj("PROCE") = row("PROCE")
                proveedorAdj("COD_PROVEEDOR") = row("PROVECOD")
                proveedorAdj("NOM_PROVEEDOR") = row("PROVEDEN")
                proveedorAdj("PROV_PROVEEDOR") = row("PROV_PROVEEDOR")
                proveedorAdj("PAIS_PROVEEDOR") = row("PAIS_PROVEEDOR")
                proveedorAdj("APE_COMPRADOR") = row("APE_COMPRADOR")
                proveedorAdj("NOM_COMPRADOR") = row("NOM_COMPRADOR")
                proveedorAdj("PRECIO_PROVEEDOR") = proveedor(0)("PRES_TOT_PROV")
                proveedorAdj("ADJ_PROV") = proveedor(0)("OFE_TOT_PROV")
                proveedorAdj("AHORR_PROV") = proveedor(0)("AHORRO_TOT_PROV")
                proveedorAdj("PORCEN_PROV") = proveedor(0)("PORCEN_TOT_PROV")
                dsResultado.Tables("PROVEEDOR").Rows.Add(proveedorAdj)
            End If
        Next
        parentCols(0) = dsResultado.Tables("PROCESO").Columns("ANYO")
        parentCols(1) = dsResultado.Tables("PROCESO").Columns("GMN1")
        parentCols(2) = dsResultado.Tables("PROCESO").Columns("COD")
        childCols(0) = dsResultado.Tables("PROVEEDOR").Columns("ANYO")
        childCols(1) = dsResultado.Tables("PROVEEDOR").Columns("GMN1")
        childCols(2) = dsResultado.Tables("PROVEEDOR").Columns("PROCE")
        dsResultado.Relations.Add("PROCESO_PROVEEDOR", parentCols, childCols, False)
        dsDatosProceso.Tables("ADJUDICACIONES").DefaultView.Sort = "COD_GRUPO"
        dsResultado.Tables.Add(dsDatosProceso.Tables("ADJUDICACIONES").DefaultView.ToTable)
        dsResultado.Tables("ADJUDICACIONES").TableName = "GRUPO"
        dsResultado.Tables("GRUPO").Columns("CONSUMIDO").ColumnName = "PRES_AHORR"
        dsResultado.Tables("GRUPO").Columns("ADJUDICADO").ColumnName = "ADJ_AHORR"
        dsResultado.Tables("GRUPO").Columns.Add("AHORR_GR", GetType(System.Double), "PRES_AHORR-ADJ_AHORR")
        dsResultado.Tables("GRUPO").Columns.Add("PORCEN_GR", GetType(System.Double), "IIF(PRES_AHORR=0,0,(AHORR_GR/PRES_AHORR)*100)")
        ReDim parentCols(3)
        ReDim childCols(3)
        parentCols(0) = dsResultado.Tables("PROVEEDOR").Columns("ANYO")
        parentCols(1) = dsResultado.Tables("PROVEEDOR").Columns("GMN1")
        parentCols(2) = dsResultado.Tables("PROVEEDOR").Columns("PROCE")
        parentCols(3) = dsResultado.Tables("PROVEEDOR").Columns("COD_PROVEEDOR")
        childCols(0) = dsResultado.Tables("GRUPO").Columns("ANYO")
        childCols(1) = dsResultado.Tables("GRUPO").Columns("GMN1")
        childCols(2) = dsResultado.Tables("GRUPO").Columns("PROCE")
        childCols(3) = dsResultado.Tables("GRUPO").Columns("PROVE")
        dsResultado.Relations.Add("PROVEEDOR_GRUPO", parentCols, childCols, False)
        ReDim parentCols(3)
        ReDim childCols(3)
        'Si mostramos la info del destino en el grupo
        Dim query5 = From procs In dsDatosProceso.Tables("PROCESO").AsEnumerable Join grps In dsDatosProceso.Tables("DATOSGRUPOS").AsEnumerable _
                On procs.Item("ANYO") Equals grps.Item("ANYO") And procs.Item("GMN1") Equals grps.Item("GMN1") And procs.Item("COD") Equals grps.Item("PROCE") _
            Where (procs.Item("DEFDEST") = 2 And grps.Item("DATO") = "DEST") _
            Select grps Distinct
        If query5.Any Then
            dsResultado.Tables.Add(query5.CopyToDataTable)
            dsResultado.Tables(dsResultado.Tables.Count - 1).TableName = "GR_DEST"
            parentCols(0) = dsResultado.Tables("GRUPO").Columns("ANYO")
            parentCols(1) = dsResultado.Tables("GRUPO").Columns("GMN1")
            parentCols(2) = dsResultado.Tables("GRUPO").Columns("PROCE")
            parentCols(3) = dsResultado.Tables("GRUPO").Columns("GRUPO")
            childCols(0) = dsResultado.Tables("GR_DEST").Columns("ANYO")
            childCols(1) = dsResultado.Tables("GR_DEST").Columns("GMN1")
            childCols(2) = dsResultado.Tables("GR_DEST").Columns("PROCE")
            childCols(3) = dsResultado.Tables("GR_DEST").Columns("ID")
            dsResultado.Relations.Add("PROCE_GR_DEST", parentCols, childCols, False)
        Else
            dsResultado.Tables.Add(New DataTable)
            dsResultado.Tables(dsResultado.Tables.Count - 1).TableName = "GR_DEST"
        End If
        'Si mostramos la info de fechas de suministro en el grupo
        Dim query6 = From procs In dsDatosProceso.Tables("PROCESO").AsEnumerable() Join grps In dsDatosProceso.Tables("DATOSGRUPOS").AsEnumerable() _
                On procs.Item("ANYO") Equals grps.Item("ANYO") And procs.Item("GMN1") Equals grps.Item("GMN1") And procs.Item("COD") Equals grps.Item("PROCE") _
            Where (procs.Item("DEFFECSUM") = 2 And grps.Item("DATO") = "FECSUM") _
            Order By procs.Item("NUM_PROCESO") _
            Select grps Distinct
        If query6.Any Then
            dsResultado.Tables.Add(query6.CopyToDataTable)
            dsResultado.Tables(dsResultado.Tables.Count - 1).TableName = "GR_FECSUM"
            parentCols(0) = dsResultado.Tables("GRUPO").Columns("ANYO")
            parentCols(1) = dsResultado.Tables("GRUPO").Columns("GMN1")
            parentCols(2) = dsResultado.Tables("GRUPO").Columns("PROCE")
            parentCols(3) = dsResultado.Tables("GRUPO").Columns("GRUPO")
            childCols(0) = dsResultado.Tables("GR_FECSUM").Columns("ANYO")
            childCols(1) = dsResultado.Tables("GR_FECSUM").Columns("GMN1")
            childCols(2) = dsResultado.Tables("GR_FECSUM").Columns("PROCE")
            childCols(3) = dsResultado.Tables("GR_FECSUM").Columns("ID")
            dsResultado.Relations.Add("PROCE_GR_FECSUM", parentCols, childCols, False)
        Else
            dsResultado.Tables.Add(New DataTable)
            dsResultado.Tables(dsResultado.Tables.Count - 1).TableName = "GR_FECSUM"
        End If
        'Si mostramos la info de forma de pago del grupo
        Dim query7 = From procs In dsDatosProceso.Tables("PROCESO").AsEnumerable() Join grps In dsDatosProceso.Tables("DATOSGRUPOS").AsEnumerable() _
                On procs.Item("ANYO") Equals grps.Item("ANYO") And procs.Item("GMN1") Equals grps.Item("GMN1") And procs.Item("COD") Equals grps.Item("PROCE") _
            Where (procs.Item("DEFPAG") = 2 And grps.Item("DATO") = "PAG") _
            Order By procs.Item("NUM_PROCESO") _
            Select grps Distinct
        If query7.Any Then
            dsResultado.Tables.Add(query7.CopyToDataTable)
            dsResultado.Tables(dsResultado.Tables.Count - 1).TableName = "GR_PAGO"
            parentCols(0) = dsResultado.Tables("GRUPO").Columns("ANYO")
            parentCols(1) = dsResultado.Tables("GRUPO").Columns("GMN1")
            parentCols(2) = dsResultado.Tables("GRUPO").Columns("PROCE")
            parentCols(3) = dsResultado.Tables("GRUPO").Columns("GRUPO")
            childCols(0) = dsResultado.Tables("GR_PAGO").Columns("ANYO")
            childCols(1) = dsResultado.Tables("GR_PAGO").Columns("GMN1")
            childCols(2) = dsResultado.Tables("GR_PAGO").Columns("PROCE")
            childCols(3) = dsResultado.Tables("GR_PAGO").Columns("ID")
            dsResultado.Relations.Add("PROCE_GR_PAGO", parentCols, childCols, False)
        Else
            dsResultado.Tables.Add(New DataTable)
            dsResultado.Tables(dsResultado.Tables.Count - 1).TableName = "GR_PAGO"
        End If
        ReDim parentCols(4)
        ReDim childCols(4)
        view = New DataView(dsResultado.Tables("GRUPO"))
        dt = New DataTable
        dt = view.ToTable(True, "ANYO", "GMN1", "PROCE", "GRUPO", "PROVE")
        dt.TableName = "ITEM_TOD"
        dsResultado.Tables.Add(dt)
        parentCols(0) = dsResultado.Tables("GRUPO").Columns("ANYO")
        parentCols(1) = dsResultado.Tables("GRUPO").Columns("GMN1")
        parentCols(2) = dsResultado.Tables("GRUPO").Columns("PROCE")
        parentCols(3) = dsResultado.Tables("GRUPO").Columns("GRUPO")
        parentCols(4) = dsResultado.Tables("GRUPO").Columns("PROVE")
        childCols(0) = dsResultado.Tables("ITEM_TOD").Columns("ANYO")
        childCols(1) = dsResultado.Tables("ITEM_TOD").Columns("GMN1")
        childCols(2) = dsResultado.Tables("ITEM_TOD").Columns("PROCE")
        childCols(3) = dsResultado.Tables("ITEM_TOD").Columns("GRUPO")
        childCols(4) = dsResultado.Tables("ITEM_TOD").Columns("PROVE")
        dsResultado.Relations.Add("GRUPO_ITEM_TOD", parentCols, childCols, False)
        dsDatosProceso.Tables("ITEM_TOD").Columns.Add("PROVE", GetType(System.String))
        dsDatosProceso.Tables("ITEM_TOD").Columns.Add("PRECOFE", GetType(System.Double))
        dsDatosProceso.Tables("ITEM_TOD").Columns.Add("CONSUMIDO", GetType(System.Double))
        dsDatosProceso.Tables("ITEM_TOD").Columns.Add("IMPORTE", GetType(System.Double))
        dsDatosProceso.Tables("ITEM_TOD").Columns.Add("AHORRO", GetType(System.Double), "CONSUMIDO-IMPORTE")
        dsDatosProceso.Tables("ITEM_TOD").Columns.Add("AHORROPORCEN", GetType(System.Double), "IIF(CONSUMIDO=0,0,(AHORRO/CONSUMIDO)*100)")
        dt = New DataTable
        dt = dsDatosProceso.Tables("ITEM_TOD").Clone
        dt.TableName = "ITEM"
        dsResultado.Tables.Add(dt)
        Dim itemProveAdj As DataRow
        For Each itemProve As DataRow In dsDatosProceso.Tables("ADJUDICACIONES_ITEM").Rows
            For Each item As DataRow In dsDatosProceso.Tables("ITEM_TOD").Select("ANYO=" & itemProve("ANYO") & " AND GMN1_PROCE='" & itemProve("GMN1") & _
                                                "' AND PROCE=" & itemProve("PROCE") & " AND ID=" & itemProve("ITEM"))
                itemProveAdj = dsResultado.Tables("ITEM").NewRow
                itemProveAdj.ItemArray = item.ItemArray
                itemProveAdj("PROVE") = itemProve("COD_PROVEEDOR")
                dsResultado.Tables("ITEM").Rows.Add(itemProveAdj)
            Next
        Next
        ReDim parentCols(4)
        ReDim childCols(4)
        parentCols(0) = dsResultado.Tables("ITEM_TOD").Columns("ANYO")
        parentCols(1) = dsResultado.Tables("ITEM_TOD").Columns("GMN1")
        parentCols(2) = dsResultado.Tables("ITEM_TOD").Columns("PROCE")
        parentCols(3) = dsResultado.Tables("ITEM_TOD").Columns("GRUPO")
        parentCols(4) = dsResultado.Tables("ITEM_TOD").Columns("PROVE")
        childCols(0) = dsResultado.Tables("ITEM").Columns("ANYO")
        childCols(1) = dsResultado.Tables("ITEM").Columns("GMN1_PROCE")
        childCols(2) = dsResultado.Tables("ITEM").Columns("PROCE")
        childCols(3) = dsResultado.Tables("ITEM").Columns("GRUPO")
        childCols(4) = dsResultado.Tables("ITEM").Columns("PROVE")
        dsResultado.Relations.Add("ITEM_TOD_ITEM", parentCols, childCols, False)
        'Para saber si mostramos la info de destino, forma de pago y fechas de suministro de los item
        Dim itemsDest As DataRow() = dsResultado.Tables("PROCESO").Select("DEFDEST=2 OR DEFDEST=3")
        Dim itemsPag As DataRow() = dsResultado.Tables("PROCESO").Select("DEFPAG=2 OR DEFPAG=3")
        Dim itemsFecSum As DataRow() = dsResultado.Tables("PROCESO").Select("DEFFECSUM=2 OR DEFFECSUM=3")
        If Not itemsDest.Length = 0 Then
            For Each row As DataRow In itemsDest
                If row("DEFDEST") = 3 Then
                    For Each item As DataRow In dsResultado.Tables("ITEM").Select("ANYO=" & row("ANYO") & " AND GMN1_PROCE='" & row("GMN1") & _
                                                "' AND PROCE=" & row("COD"))
                        item("ITEM_DEST") = 1
                    Next
                Else
                    For Each datosGrupo As DataRow In dsResultado.Tables("GRUPO").Select("ANYO=" & row("ANYO") & " AND GMN1='" & row("GMN1") & "' AND PROCE=" & row("COD"))
                        If dsResultado.Tables("GR_DEST").Rows.Count = 0 OrElse dsResultado.Tables("GR_DEST").Select("ANYO=" & datosGrupo("ANYO") & " AND GMN1='" & datosGrupo("GMN1") & _
                                                "' AND PROCE=" & datosGrupo("PROCE") & " AND ID=" & datosGrupo("GRUPO")).Length = 0 Then
                            For Each item As DataRow In dsResultado.Tables("ITEM").Select("ANYO=" & datosGrupo("ANYO") & " AND GMN1_PROCE='" & datosGrupo("GMN1") & _
                                                "' AND PROCE=" & datosGrupo("PROCE") & " AND GRUPO=" & datosGrupo("GRUPO"))
                                item("ITEM_DEST") = 1
                            Next
                        End If
                    Next
                End If
            Next
        End If
        If Not itemsPag.Length = 0 Then
            For Each row As DataRow In itemsPag
                If row("DEFPAG") = 3 Then
                    For Each item As DataRow In dsResultado.Tables("ITEM").Select("ANYO=" & row("ANYO") & " AND GMN1_PROCE='" & row("GMN1") & _
                                                "' AND PROCE=" & row("COD"))
                        item("ITEM_PAG") = 1
                    Next
                Else
                    For Each datosGrupo As DataRow In dsResultado.Tables("GRUPO").Select("ANYO=" & row("ANYO") & " AND GMN1='" & row("GMN1") & "' AND PROCE=" & row("COD"))
                        If dsResultado.Tables("GR_PAGO").Rows.Count = 0 OrElse dsResultado.Tables("GR_PAGO").Select("ANYO=" & datosGrupo("ANYO") & " AND GMN1='" & datosGrupo("GMN1") & _
                                                "' AND PROCE=" & datosGrupo("PROCE") & " AND ID=" & datosGrupo("GRUPO")).Length = 0 Then
                            For Each grupo As DataRow In dsResultado.Tables("ITEM").Select("ANYO=" & datosGrupo("ANYO") & " AND GMN1_PROCE='" & datosGrupo("GMN1") & _
                                                "' AND PROCE=" & datosGrupo("PROCE") & " AND GRUPO=" & datosGrupo("GRUPO"))
                                grupo("ITEM_PAG") = 1
                            Next
                        End If
                    Next
                End If
            Next
        End If
        If Not itemsFecSum.Length = 0 Then
            For Each row As DataRow In itemsFecSum
                If row("DEFFECSUM") = 3 Then
                    For Each item As DataRow In dsResultado.Tables("ITEM").Select("ANYO=" & row("ANYO") & " AND GMN1_PROCE='" & row("GMN1") & _
                                                "' AND PROCE=" & row("COD"))
                        item("ITEM_FECSUM") = 1
                    Next
                Else
                    For Each datosGrupo As DataRow In dsResultado.Tables("GRUPO").Select("ANYO=" & row("ANYO") & " AND GMN1='" & row("GMN1") & "' AND PROCE=" & row("COD"))
                        If dsResultado.Tables("GR_FECSUM").Rows.Count = 0 OrElse dsResultado.Tables("GR_FECSUM").Select("ANYO=" & datosGrupo("ANYO") & " AND GMN1='" & datosGrupo("GMN1") & _
                                                "' AND PROCE=" & datosGrupo("PROCE") & " AND ID=" & datosGrupo("GRUPO")).Length = 0 Then
                            For Each grupo As DataRow In dsResultado.Tables("ITEM").Select("ANYO=" & datosGrupo("ANYO") & " AND GMN1_PROCE='" & datosGrupo("GMN1") & _
                                                "' AND PROCE=" & datosGrupo("PROCE") & " AND GRUPO=" & datosGrupo("GRUPO"))
                                grupo("ITEM_FECSUM") = 1
                            Next
                        End If
                    Next
                End If
            Next
        End If
        ReDim parentCols(3)
        ReDim childCols(3)
        Dim query8 = From items In dsResultado.Tables("ITEM").AsEnumerable() _
            Where (items.Item("ITEM_DEST") = 1) _
            Select items Distinct
        If query8.Any Then
            dsResultado.Tables.Add(query8.CopyToDataTable)
            dsResultado.Tables(dsResultado.Tables.Count - 1).TableName = "IT_DEST"
            parentCols(0) = dsResultado.Tables("ITEM").Columns("ANYO")
            parentCols(1) = dsResultado.Tables("ITEM").Columns("GMN1_PROCE")
            parentCols(2) = dsResultado.Tables("ITEM").Columns("PROCE")
            parentCols(3) = dsResultado.Tables("ITEM").Columns("ID")
            childCols(0) = dsResultado.Tables("IT_DEST").Columns("ANYO")
            childCols(1) = dsResultado.Tables("IT_DEST").Columns("GMN1_PROCE")
            childCols(2) = dsResultado.Tables("IT_DEST").Columns("PROCE")
            childCols(3) = dsResultado.Tables("IT_DEST").Columns("ID")
            dsResultado.Relations.Add("ITEM_IT_DEST", parentCols, childCols, False)
        Else
            dsResultado.Tables.Add(New DataTable)
            dsResultado.Tables(dsResultado.Tables.Count - 1).TableName = "IT_DEST"
        End If
        Dim query9 = From items In dsResultado.Tables("ITEM").AsEnumerable() _
            Where (items.Item("ITEM_PAG") = 1) _
            Select items Distinct
        If query9.Any Then
            dsResultado.Tables.Add(query9.CopyToDataTable)
            dsResultado.Tables(dsResultado.Tables.Count - 1).TableName = "IT_PAGO"
            parentCols(0) = dsResultado.Tables("ITEM").Columns("ANYO")
            parentCols(1) = dsResultado.Tables("ITEM").Columns("GMN1_PROCE")
            parentCols(2) = dsResultado.Tables("ITEM").Columns("PROCE")
            parentCols(3) = dsResultado.Tables("ITEM").Columns("ID")
            childCols(0) = dsResultado.Tables("IT_PAGO").Columns("ANYO")
            childCols(1) = dsResultado.Tables("IT_PAGO").Columns("GMN1_PROCE")
            childCols(2) = dsResultado.Tables("IT_PAGO").Columns("PROCE")
            childCols(3) = dsResultado.Tables("IT_PAGO").Columns("ID")
            dsResultado.Relations.Add("ITEM_IT_PAGO", parentCols, childCols, False)
        Else
            dsResultado.Tables.Add(New DataTable)
            dsResultado.Tables(dsResultado.Tables.Count - 1).TableName = "IT_PAGO"
        End If
        Dim query10 = From items In dsResultado.Tables("ITEM").AsEnumerable() _
            Where (items.Item("ITEM_FECSUM") = 1) _
            Select items Distinct
        If query10.Any Then
            dsResultado.Tables.Add(query10.CopyToDataTable)
            dsResultado.Tables(dsResultado.Tables.Count - 1).TableName = "IT_FECSUM"
            parentCols(0) = dsResultado.Tables("ITEM").Columns("ANYO")
            parentCols(1) = dsResultado.Tables("ITEM").Columns("GMN1_PROCE")
            parentCols(2) = dsResultado.Tables("ITEM").Columns("PROCE")
            parentCols(3) = dsResultado.Tables("ITEM").Columns("ID")
            childCols(0) = dsResultado.Tables("IT_FECSUM").Columns("ANYO")
            childCols(1) = dsResultado.Tables("IT_FECSUM").Columns("GMN1_PROCE")
            childCols(2) = dsResultado.Tables("IT_FECSUM").Columns("PROCE")
            childCols(3) = dsResultado.Tables("IT_FECSUM").Columns("ID")
            dsResultado.Relations.Add("ITEM_IT_FECSUM", parentCols, childCols, False)
        Else
            dsResultado.Tables.Add(New DataTable)
            dsResultado.Tables(dsResultado.Tables.Count - 1).TableName = "IT_FECSUM"
        End If
        Dim atributosItem As DataRow()
        For Each item As DataRow In dsResultado.Tables("ITEM").Rows
            For Each itemProve As DataRow In dsDatosProceso.Tables("ADJUDICACIONES_ITEM").Select("ANYO=" & item("ANYO") & " AND GMN1='" & item("GMN1_PROCE") & "'" & _
                                                " AND PROCE=" & item("PROCE") & " AND ITEM=" & item("ID") & " AND COD_PROVEEDOR='" & item("PROVE") & "'")
                item("CONSUMIDO") = item("CANTIDAD") * item("PREC") * (itemProve("PORCEN") / 100)
                item("CANTIDAD") = item("CANTIDAD") * (dsDatosProceso.Tables("ADJUDICACIONES_GRUPO").Select("ANYO=" & item("ANYO") & _
                    " AND GMN1='" & item("GMN1_PROCE") & "'" & " AND PROCE=" & item("PROCE") & " AND GRUPO=" & item("GRUPO") & _
                    " AND ITEM=" & item("ID") & " AND PROVE='" & item("PROVE") & "'")(0)("PORCEN") / 100)
                item("PRECOFE") = itemProve("PRECIO_PROVEEDOR")
                item("IMPORTE") = item("CANTIDAD") * item("PRECOFE") * (itemProve("PORCEN") / 100)
                atributosItem = dsDatosProceso.Tables("ATRIBUTOS").Select("APLIC_PREC=2 AND (" & _
                    "(ANYO=" & item("ANYO") & " AND GMN1='" & item("GMN1_PROCE") & "' AND PROCE=" & item("PROCE") & " AND GRUPO=0 AND ITEM=0) OR " & _
                    "(ANYO=" & item("ANYO") & " AND GMN1='" & item("GMN1_PROCE") & "' AND PROCE=" & item("PROCE") & " AND GRUPO=" & item("GRUPO") & " AND ITEM=0) OR " & _
                    "(ANYO=" & item("ANYO") & " AND GMN1='" & item("GMN1_PROCE") & "' AND PROCE=" & item("PROCE") & " AND GRUPO=0 AND ITEM=" & item("ID") & "))", "ORDEN")
                For Each atributo As DataRow In atributosItem
                    Select Case atributo("OP_PREC")
                        Case "+"
                            item("IMPORTE") = DBNullToDbl(item("IMPORTE")) + atributo("VALOR_NUM")
                        Case "-"
                            item("IMPORTE") = DBNullToDbl(item("IMPORTE")) - atributo("VALOR_NUM")
                        Case "/"
                            If IsDBNull(atributo("VALOR_NUM")) OrElse atributo("VALOR_NUM") = 0 Then
                                item("IMPORTE") = 0
                            Else
                                item("IMPORTE") = DBNullToDbl(item("IMPORTE")) / atributo("VALOR_NUM")
                            End If
                        Case "*"
                            item("IMPORTE") = DBNullToDbl(item("IMPORTE")) * atributo("VALOR_NUM")
                        Case "+%"
                            item("IMPORTE") = DBNullToDbl(item("IMPORTE")) + ((atributo("VALOR_NUM") / 100) * DBNullToDbl(item("IMPORTE")))
                        Case "-%"
                            item("IMPORTE") = DBNullToDbl(item("IMPORTE")) - ((atributo("VALOR_NUM") / 100) * DBNullToDbl(item("IMPORTE")))
                    End Select
                Next
            Next
        Next
        removeRows.Clear()
        For Each row As DataRow In dsResultado.Tables("ITEM_TOD").Rows
            If row.GetChildRows("ITEM_TOD_ITEM").Length = 0 Then
                removeRows.Add(row)
            End If
        Next
        For Each row As DataRow In removeRows
            dsResultado.Tables("ITEM_TOD").Rows.Remove(row)
        Next
        If ProveedoresNoAdjudicados Then
            dt = New DataTable
            dt = dsDatosProceso.Tables("OFERTAS_ITEM").Clone
            dt.TableName = "PROVEEDOR_NOADJ"
            dt.Columns.Add("PRESUPUESTO_NOADJ", GetType(System.Double))
            dt.Columns.Add("IMPORTE_NOADJ", GetType(System.Double))
            dt.Columns.Add("AHORRO_NOADJ", GetType(System.Double))
            dt.Columns.Add("PORCEN_NOADJ", GetType(System.Double), "IIF(PRESUPUESTO_NOADJ=0,0,(AHORRO_NOADJ/IIF(PRESUPUESTO_NOADJ<0,-PRESUPUESTO_NOADJ,PRESUPUESTO_NOADJ))*100)")
            dsResultado.Tables.Add(dt)
            Dim proveNoAdj As DataRow
            For Each row As DataRow In dsDatosProceso.Tables("PROVEEDORES_NOADJUDICADOS").Rows
                proveNoAdj = dsResultado.Tables("PROVEEDOR_NOADJ").NewRow
                proveNoAdj("ANYO") = row("ANYO")
                proveNoAdj("GMN1") = row("GMN1")
                proveNoAdj("PROCE") = row("PROCE")
                proveNoAdj("COD_PROVEEDOR_NOADJ") = row("PROVECOD")
                proveNoAdj("NOM_PROVEEDOR_NOADJ") = row("PROVEDEN")
                proveNoAdj("PROV_PROVEEDOR_NOADJ") = row("PROV_PROVEEDOR_NOADJ")
                proveNoAdj("PAIS_PROVEEDOR_NOADJ") = row("PAIS_PROVEEDOR_NOADJ")
                proveNoAdj("APE_COMPRADOR_NOADJ") = row("APE_COMPRADOR_NOADJ")
                proveNoAdj("NOM_COMPRADOR_NOADJ") = row("NOM_COMPRADOR_NOADJ")

                proveNoAdj("PRESUPUESTO_NOADJ") = DBNullToDbl(dsDatosProceso.Tables("ADJUDICACIONES").Compute("SUM(ABIERTO)", _
                                        "ANYO=" & row("ANYO") & " AND GMN1='" & row("GMN1") & "' AND PROCE=" & row("PROCE") & " AND PROVE='" & row("PROVECOD") & "'"))
                proveNoAdj("IMPORTE_NOADJ") = DBNullToDbl(dsDatosProceso.Tables("ADJUDICACIONES").Compute("SUM(IMPORTE)", _
                                        "ANYO=" & row("ANYO") & " AND GMN1='" & row("GMN1") & "' AND PROCE=" & row("PROCE") & " AND PROVE='" & row("PROVECOD") & "'"))
                proveNoAdj("AHORRO_NOADJ") = DBNullToDbl(dsDatosProceso.Tables("ADJUDICACIONES").Compute("SUM(ABIERTO)", _
                                        "ANYO=" & row("ANYO") & " AND GMN1='" & row("GMN1") & "' AND PROCE=" & row("PROCE") & " AND PROVE='" & row("PROVECOD") & "'"))
                proveNoAdj("PORCEN_NOADJ") = 0
                dsResultado.Tables("PROVEEDOR_NOADJ").Rows.Add(proveNoAdj)
            Next
            ReDim parentCols(2)
            ReDim childCols(2)
            parentCols(0) = dsResultado.Tables("PROCESO").Columns("ANYO")
            parentCols(1) = dsResultado.Tables("PROCESO").Columns("GMN1")
            parentCols(2) = dsResultado.Tables("PROCESO").Columns("COD")
            childCols(0) = dsResultado.Tables("PROVEEDOR_NOADJ").Columns("ANYO")
            childCols(1) = dsResultado.Tables("PROVEEDOR_NOADJ").Columns("GMN1")
            childCols(2) = dsResultado.Tables("PROVEEDOR_NOADJ").Columns("PROCE")
            dsResultado.Relations.Add("PROCESO_PROVEEDOR_NOADJ", parentCols, childCols, False)
            dt = New DataTable
            dt = dsResultado.Tables("GRUPO").Clone
            dt.TableName = "GRUPO_NOADJ"
            dt.Columns("COD_GRUPO").ColumnName = "COD_GRUPO_NOADJ"
            dt.Columns("DEN_GRUPO").ColumnName = "DEN_GRUPO_NOADJ"
            dt.Columns("PRES_AHORR").ColumnName = "PRES_AHORR_NOADJ"
            dt.Columns("ADJ_AHORR").ColumnName = "IMPORTE_OFERTA_NOADJ"
            dt.Columns("AHORR_GR").ColumnName = "AHORR_GR_NOADJ"
            dt.Columns("PORCEN_GR").ColumnName = "PORCEN_GR_NOADJ"
            dsResultado.Tables.Add(dt)
            Dim grupoNoAdj As DataRow
            For Each row As DataRow In dsResultado.Tables("PROVEEDOR_NOADJ").Rows
                For Each adj As DataRow In dsDatosProceso.Tables("ADJUDICACIONES").Select("ANYO=" & row("ANYO") & _
                                            " AND GMN1='" & row("GMN1") & "' AND PROCE=" & row("PROCE") & " AND PROVE='" & row("COD_PROVEEDOR_NOADJ") & "'")

                    view = New DataView(dsDatosProceso.Tables("DATOSGRUPOS").Select("ANYO=" & row("ANYO") & _
                                            " AND GMN1='" & row("GMN1") & "' AND PROCE=" & row("PROCE") & " AND ID=" & adj("GRUPO")).CopyToDataTable())
                    grupoNoAdj = view.ToTable(True, "ANYO", "GMN1", "PROCE", "ID", "COD_GRUPO", "DEN_GRUPO").Rows(0)
                    proveNoAdj = dsResultado.Tables("GRUPO_NOADJ").NewRow
                    proveNoAdj("ANYO") = row("ANYO")
                    proveNoAdj("GMN1") = row("GMN1")
                    proveNoAdj("PROCE") = row("PROCE")
                    proveNoAdj("PROVE") = row("COD_PROVEEDOR_NOADJ")
                    proveNoAdj("GRUPO") = grupoNoAdj("ID")
                    proveNoAdj("COD_GRUPO_NOADJ") = grupoNoAdj("COD_GRUPO")
                    proveNoAdj("DEN_GRUPO_NOADJ") = grupoNoAdj("DEN_GRUPO")
                    proveNoAdj("PRES_AHORR_NOADJ") = adj("ABIERTO")
                    proveNoAdj("IMPORTE_OFERTA_NOADJ") = adj("IMPORTE")
                    proveNoAdj("AHORR_GR_NOADJ") = adj("AHORRO_OFE")
                    proveNoAdj("PORCEN_GR_NOADJ") = adj("AHORRO_OFE_PORCEN")
                    dsResultado.Tables("GRUPO_NOADJ").Rows.Add(proveNoAdj)
                Next
            Next
            ReDim parentCols(3)
            ReDim childCols(3)
            parentCols(0) = dsResultado.Tables("PROVEEDOR_NOADJ").Columns("ANYO")
            parentCols(1) = dsResultado.Tables("PROVEEDOR_NOADJ").Columns("GMN1")
            parentCols(2) = dsResultado.Tables("PROVEEDOR_NOADJ").Columns("PROCE")
            parentCols(3) = dsResultado.Tables("PROVEEDOR_NOADJ").Columns("COD_PROVEEDOR_NOADJ")
            childCols(0) = dsResultado.Tables("GRUPO_NOADJ").Columns("ANYO")
            childCols(1) = dsResultado.Tables("GRUPO_NOADJ").Columns("GMN1")
            childCols(2) = dsResultado.Tables("GRUPO_NOADJ").Columns("PROCE")
            childCols(3) = dsResultado.Tables("GRUPO_NOADJ").Columns("PROVE")
            dsResultado.Relations.Add("PROVEEDOR_NOADJ_GRUPO_NOADJ", parentCols, childCols, False)
            'Si mostramos la info del destino en el grupo
            Dim query11 = From procs In dsDatosProceso.Tables("PROCESO").AsEnumerable Join grps In dsDatosProceso.Tables("DATOSGRUPOS").AsEnumerable _
                    On procs.Item("ANYO") Equals grps.Item("ANYO") And procs.Item("GMN1") Equals grps.Item("GMN1") And procs.Item("COD") Equals grps.Item("PROCE") _
                Where (procs.Item("DEFDEST") = 2 And grps.Item("DATO") = "DEST") _
                Select grps Distinct
            If query11.Any Then
                dsResultado.Tables.Add(query11.CopyToDataTable)
                dsResultado.Tables(dsResultado.Tables.Count - 1).TableName = "GR_DEST_NOADJ"
                parentCols(0) = dsResultado.Tables("GRUPO_NOADJ").Columns("ANYO")
                parentCols(1) = dsResultado.Tables("GRUPO_NOADJ").Columns("GMN1")
                parentCols(2) = dsResultado.Tables("GRUPO_NOADJ").Columns("PROCE")
                parentCols(3) = dsResultado.Tables("GRUPO_NOADJ").Columns("GRUPO")
                childCols(0) = dsResultado.Tables("GR_DEST_NOADJ").Columns("ANYO")
                childCols(1) = dsResultado.Tables("GR_DEST_NOADJ").Columns("GMN1")
                childCols(2) = dsResultado.Tables("GR_DEST_NOADJ").Columns("PROCE")
                childCols(3) = dsResultado.Tables("GR_DEST_NOADJ").Columns("ID")
                dsResultado.Relations.Add("GRUPO_NOADJ_GR_DEST_NOADJ", parentCols, childCols, False)
            Else
                dsResultado.Tables.Add(New DataTable)
                dsResultado.Tables(dsResultado.Tables.Count - 1).TableName = "GR_DEST_NOADJ"
            End If
            'Si mostramos la info de fechas de suministro en el grupo
            Dim query12 = From procs In dsDatosProceso.Tables("PROCESO").AsEnumerable() Join grps In dsDatosProceso.Tables("DATOSGRUPOS").AsEnumerable() _
                    On procs.Item("ANYO") Equals grps.Item("ANYO") And procs.Item("GMN1") Equals grps.Item("GMN1") And procs.Item("COD") Equals grps.Item("PROCE") _
                Where (procs.Item("DEFFECSUM") = 2 And grps.Item("DATO") = "FECSUM") _
                Order By procs.Item("NUM_PROCESO") _
                Select grps Distinct
            If query12.Any Then
                dsResultado.Tables.Add(query12.CopyToDataTable)
                dsResultado.Tables(dsResultado.Tables.Count - 1).TableName = "GR_FECSUM_NOADJ"
                parentCols(0) = dsResultado.Tables("GRUPO_NOADJ").Columns("ANYO")
                parentCols(1) = dsResultado.Tables("GRUPO_NOADJ").Columns("GMN1")
                parentCols(2) = dsResultado.Tables("GRUPO_NOADJ").Columns("PROCE")
                parentCols(3) = dsResultado.Tables("GRUPO_NOADJ").Columns("GRUPO")
                childCols(0) = dsResultado.Tables("GR_FECSUM_NOADJ").Columns("ANYO")
                childCols(1) = dsResultado.Tables("GR_FECSUM_NOADJ").Columns("GMN1")
                childCols(2) = dsResultado.Tables("GR_FECSUM_NOADJ").Columns("PROCE")
                childCols(3) = dsResultado.Tables("GR_FECSUM_NOADJ").Columns("ID")
                dsResultado.Relations.Add("GRUPO_NOADJ_GR_FECSUM_NOADJ", parentCols, childCols, False)
            Else
                dsResultado.Tables.Add(New DataTable)
                dsResultado.Tables(dsResultado.Tables.Count - 1).TableName = "GR_FECSUM_NOADJ"
            End If
            'Si mostramos la info de forma de pago del grupo
            Dim query13 = From procs In dsDatosProceso.Tables("PROCESO").AsEnumerable() Join grps In dsDatosProceso.Tables("DATOSGRUPOS").AsEnumerable() _
                    On procs.Item("ANYO") Equals grps.Item("ANYO") And procs.Item("GMN1") Equals grps.Item("GMN1") And procs.Item("COD") Equals grps.Item("PROCE") _
                Where (procs.Item("DEFPAG") = 2 And grps.Item("DATO") = "PAG") _
                Order By procs.Item("NUM_PROCESO") _
                Select grps Distinct
            If query13.Any Then
                dsResultado.Tables.Add(query13.CopyToDataTable)
                dsResultado.Tables(dsResultado.Tables.Count - 1).TableName = "GR_PAGO_NOADJ"
                parentCols(0) = dsResultado.Tables("GRUPO_NOADJ").Columns("ANYO")
                parentCols(1) = dsResultado.Tables("GRUPO_NOADJ").Columns("GMN1")
                parentCols(2) = dsResultado.Tables("GRUPO_NOADJ").Columns("PROCE")
                parentCols(3) = dsResultado.Tables("GRUPO_NOADJ").Columns("GRUPO")
                childCols(0) = dsResultado.Tables("GR_PAGO_NOADJ").Columns("ANYO")
                childCols(1) = dsResultado.Tables("GR_PAGO_NOADJ").Columns("GMN1")
                childCols(2) = dsResultado.Tables("GR_PAGO_NOADJ").Columns("PROCE")
                childCols(3) = dsResultado.Tables("GR_PAGO_NOADJ").Columns("ID")
                dsResultado.Relations.Add("GRUPO_NOADJ_GR_PAGO_NOADJ", parentCols, childCols, False)
            Else
                dsResultado.Tables.Add(New DataTable)
                dsResultado.Tables(dsResultado.Tables.Count - 1).TableName = "GR_PAGO_NOADJ"
            End If
            view = New DataView(dsResultado.Tables("GRUPO_NOADJ"))
            dt = New DataTable
            dt = view.ToTable(True, "ANYO", "GMN1", "PROCE", "GRUPO", "PROVE")
            dt.TableName = "ITEM_TOD_NOADJ"
            dsResultado.Tables.Add(dt)
            ReDim parentCols(4)
            ReDim childCols(4)
            parentCols(0) = dsResultado.Tables("GRUPO_NOADJ").Columns("ANYO")
            parentCols(1) = dsResultado.Tables("GRUPO_NOADJ").Columns("GMN1")
            parentCols(2) = dsResultado.Tables("GRUPO_NOADJ").Columns("PROCE")
            parentCols(3) = dsResultado.Tables("GRUPO_NOADJ").Columns("GRUPO")
            parentCols(4) = dsResultado.Tables("GRUPO_NOADJ").Columns("PROVE")
            childCols(0) = dsResultado.Tables("ITEM_TOD_NOADJ").Columns("ANYO")
            childCols(1) = dsResultado.Tables("ITEM_TOD_NOADJ").Columns("GMN1")
            childCols(2) = dsResultado.Tables("ITEM_TOD_NOADJ").Columns("PROCE")
            childCols(3) = dsResultado.Tables("ITEM_TOD_NOADJ").Columns("GRUPO")
            childCols(4) = dsResultado.Tables("ITEM_TOD_NOADJ").Columns("PROVE")
            dsResultado.Relations.Add("GRUPO_NOADJ_ITEM_TOD_NOADJ", parentCols, childCols, False)
            dt = New DataTable
            dt = dsDatosProceso.Tables("ITEM_TOD").Clone
            dt.TableName = "ITEM_NOADJ"
            dt.Columns("COD_ITEM").ColumnName = "COD_ITEM_NOADJ"
            dt.Columns("DEN_ITEM").ColumnName = "DEN_ITEM_NOADJ"
            dt.Columns("COD_DEST_ITEM").ColumnName = "COD_DEST_ITEM_NOADJ"
            dt.Columns("DEN_DEST_ITEM").ColumnName = "DEN_DEST_ITEM_NOADJ"
            dt.Columns("FINI_SUM_ITEM").ColumnName = "FINI_SUM_ITEM_NOADJ"
            dt.Columns("FFIN_SUM_ITEM").ColumnName = "FFIN_SUM_ITEM_NOADJ"
            dt.Columns("CANTIDAD").ColumnName = "CANTIDAD_NOADJ"
            dt.Columns("COD_UNI").ColumnName = "COD_UNI_NOADJ"
            dt.Columns("PRECOFE").ColumnName = "PRECOFE_NOADJ"
            dt.Columns("IMPORTE").ColumnName = "IMPORTE_ITEM_NOADJ"
            dt.Columns("AHORRO").ColumnName = "AHORRO_ITEM_NOADJ"
            dt.Columns("AHORROPORCEN").ColumnName = "AHORROPORCEN_NOADJ"
            dt.Columns("ITEM_DEST").ColumnName = "IT_DEST_NOADJ"
            dt.Columns("ITEM_FECSUM").ColumnName = "IT_FECSUM_NOADJ"
            dsResultado.Tables.Add(dt)
            For Each proveNoAdjudicado As DataRow In dsResultado.Tables("PROVEEDOR_NOADJ").Rows
                For Each itemProve As DataRow In dsDatosProceso.Tables("OFERTAS_ITEM").Select("ANYO=" & proveNoAdjudicado("ANYO") & _
                            " AND GMN1='" & proveNoAdjudicado("GMN1") & "' AND PROCE=" & proveNoAdjudicado("PROCE") & _
                            " AND COD_PROVEEDOR_NOADJ='" & proveNoAdjudicado("COD_PROVEEDOR_NOADJ") & "'")
                    For Each item As DataRow In dsDatosProceso.Tables("ITEM_TOD").Select("ANYO=" & itemProve("ANYO") & " AND GMN1_PROCE='" & itemProve("GMN1") & _
                                                        "' AND PROCE=" & itemProve("PROCE") & " AND ID=" & itemProve("ITEM"))
                        itemProveAdj = dsResultado.Tables("ITEM_NOADJ").NewRow
                        itemProveAdj.ItemArray = item.ItemArray
                        itemProveAdj("PROVE") = itemProve("COD_PROVEEDOR_NOADJ")
                        dsResultado.Tables("ITEM_NOADJ").Rows.Add(itemProveAdj)
                    Next
                Next
            Next
            ReDim parentCols(4)
            ReDim childCols(4)
            parentCols(0) = dsResultado.Tables("ITEM_TOD_NOADJ").Columns("ANYO")
            parentCols(1) = dsResultado.Tables("ITEM_TOD_NOADJ").Columns("GMN1")
            parentCols(2) = dsResultado.Tables("ITEM_TOD_NOADJ").Columns("PROCE")
            parentCols(3) = dsResultado.Tables("ITEM_TOD_NOADJ").Columns("GRUPO")
            parentCols(4) = dsResultado.Tables("ITEM_TOD_NOADJ").Columns("PROVE")
            childCols(0) = dsResultado.Tables("ITEM_NOADJ").Columns("ANYO")
            childCols(1) = dsResultado.Tables("ITEM_NOADJ").Columns("GMN1_PROCE")
            childCols(2) = dsResultado.Tables("ITEM_NOADJ").Columns("PROCE")
            childCols(3) = dsResultado.Tables("ITEM_NOADJ").Columns("GRUPO")
            childCols(4) = dsResultado.Tables("ITEM_NOADJ").Columns("PROVE")
            dsResultado.Relations.Add("ITEM_TOD_NOADJ_ITEM_NOADJ", parentCols, childCols, False)
        End If
        Return dsResultado
    End Function
End Class
