<Serializable()> _
    Public Class Monedas
    Inherits Security

    Private Contexto As System.Web.HttpContext
    Private moMonedas As DataSet
    Public ReadOnly Property Data() As Data.DataSet
        Get
            Return moMonedas
        End Get

    End Property
    Public Sub LoadData(ByVal sIdi As String, Optional ByVal sCod As String = Nothing, Optional ByVal sDen As String = Nothing, Optional ByVal bCoincid As Boolean = False, Optional ByVal bAllDatos As Boolean = False)
        'jbg160309
        'Que hace: 
        '   Devolver en un dataset 2(cod + den ) o todos los campos de la tabla monedas
        'Param. Entrada:
        '   sIdi    idioma
        '   sCod    codigo de moneda
        '   sDen    denominaci�n de moneda
        '   bCoincid    coincidencia total o por like 'sCod%'
        '   bAllDatos   opcional.2(cod + den ) o todos los campos de la tabla
        'Param. Salida:
        '   Lo dicho en "que hace", un recordset
        'Llamada desde: 
        '   Todo combo de monedas. Carga de monedas para comprobar/traer datos...Vamos N aspx, pmserver, pmdatab...
        'Tiempo:    
        '   Instantaneo
        Dim ds As DataSet
        If sCod Is Nothing AndAlso sDen Is Nothing AndAlso Not bAllDatos Then 'desde LinkMonedas se llama a este m�todo con bAllDatos a true para sacar m�s campos.
            Contexto = System.Web.HttpContext.Current
            ds = CType(Contexto.Cache("dsTodasMonedas" & sIdi), DataSet)
            If ds Is Nothing Then
                ds = DBServer.Monedas_Get(sIdi, sCod, sDen, bCoincid, bAllDatos)
                Contexto.Cache.Insert("dsTodasMonedas" & sIdi, ds, Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration, New TimeSpan(0, System.Configuration.ConfigurationManager.AppSettings("TiempoExpiracionCacheMin"), 0))
            End If
        Else
            ds = DBServer.Monedas_Get(sIdi, sCod, sDen, bCoincid, bAllDatos)
        End If

        moMonedas = ds
    End Sub
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
    ''' <summary>
    ''' Devolver en un dataset 2(cod + den ) o todos los campos de la tabla monedas
    ''' </summary>
    ''' <param name="sIdi">idioma</param>
    ''' <param name="sCod">codigo de moneda</param>
    ''' <param name="sDen">denominación de moneda</param>
    ''' <param name="bCoincid">coincidencia total o por like 'sCod%'</param>
    ''' <param name="bAllDatos">opcional.2(cod + den ) o todos los campos de la tabla</param>
    ''' <remarks>Lo dicho en "que hace", un recordset</remarks>
    Public Function Get_Monedas(ByVal sIdi As String, Optional ByVal sCod As String = Nothing, Optional ByVal sDen As String = Nothing, Optional ByVal bCoincid As Boolean = False, Optional ByVal bAllDatos As Boolean = False) As DataSet
        Authenticate()
        Return DBServer.Monedas_Get(sIdi, sCod, sDen, bCoincid, bAllDatos)
    End Function
    ''' <summary>
    ''' Devuelve código y denominación de la moneda central
    ''' </summary>
    ''' <param name="sIdi">Idioma en la que se devuelve la denominación de la moneda</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Get_MonedaCentral(ByVal sIdi As String) As DataRow
        Authenticate()
        Return DBServer.Monedas_Get_MonedaCentral(sIdi)
    End Function
End Class
