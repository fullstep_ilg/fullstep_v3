﻿Imports System.Configuration

Public Class Perfiles
    Inherits Security
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
    Public Function Load(ByVal Idioma As String) As DataSet
        Authenticate()
        Load = DBServer.SG_Perfiles_Load(Idioma)
    End Function
    Public Function AddPerfil(ByVal Perfil As Perfil) As Integer
        Authenticate()
        Dim dr As DataRow
        Dim dtDenominacionesPerfil As New DataTable
        dtDenominacionesPerfil.Columns.Add("IDI", System.Type.GetType("System.String"))
        dtDenominacionesPerfil.Columns.Add("DEN", System.Type.GetType("System.String"))
        For Each item As KeyValuePair(Of String, String) In Perfil.Denominaciones
            dr = dtDenominacionesPerfil.NewRow
            dr("IDI") = item.Key
            dr("DEN") = item.Value
            dtDenominacionesPerfil.Rows.Add(dr)
        Next

        Dim dtAccionesPerfil As New DataTable
        dtAccionesPerfil.Columns.Add("ACC", System.Type.GetType("System.Int32"))
        For Each item As KeyValuePair(Of String, Integer) In Perfil.AccionesPerfil
            dr = dtAccionesPerfil.NewRow
            dr("ACC") = item.Value
            dtAccionesPerfil.Rows.Add(dr)
        Next

        Dim dtUONsPerfil As New DataTable
        dtUONsPerfil.Columns.Add("UON1", System.Type.GetType("System.String"))
        dtUONsPerfil.Columns.Add("UON2", System.Type.GetType("System.String"))
        dtUONsPerfil.Columns.Add("UON3", System.Type.GetType("System.String"))
        For Each UON0 As cn_fsTreeViewItem In Perfil.UONS
            For Each UON1 As cn_fsTreeViewItem In UON0.children
                If UON1.checked Then
                    dr = dtUONsPerfil.NewRow
                    dr("UON1") = UON1.value
                    dtUONsPerfil.Rows.Add(dr)
                End If
                For Each UON2 As cn_fsTreeViewItem In UON1.children
                    If UON2.checked Then
                        dr = dtUONsPerfil.NewRow
                        dr("UON1") = UON1.value
                        dr("UON2") = UON2.value
                        dtUONsPerfil.Rows.Add(dr)
                    End If
                    For Each UON3 As cn_fsTreeViewItem In UON2.children
                        If UON3.checked Then
                            dr = dtUONsPerfil.NewRow
                            dr("UON1") = UON1.value
                            dr("UON2") = UON2.value
                            dr("UON3") = UON3.value
                            dtUONsPerfil.Rows.Add(dr)
                        End If
                    Next
                Next
            Next
        Next

        Dim dtQlikUONsPerfil As New DataTable
        dtQlikUONsPerfil.Columns.Add("UON1", System.Type.GetType("System.String"))
        dtQlikUONsPerfil.Columns.Add("UON2", System.Type.GetType("System.String"))
        dtQlikUONsPerfil.Columns.Add("UON3", System.Type.GetType("System.String"))
        For Each UON0 As cn_fsTreeViewItem In Perfil.QlikUONS
            For Each UON1 As cn_fsTreeViewItem In UON0.children
                If UON1.checked And Not UON0.checked Then
                    dr = dtQlikUONsPerfil.NewRow
                    dr("UON1") = UON1.value
                    dtQlikUONsPerfil.Rows.Add(dr)
                End If
                For Each UON2 As cn_fsTreeViewItem In UON1.children
                    If UON2.checked And Not UON1.checked Then
                        dr = dtQlikUONsPerfil.NewRow
                        dr("UON1") = UON1.value
                        dr("UON2") = UON2.value
                        dtQlikUONsPerfil.Rows.Add(dr)
                    End If
                    For Each UON3 As cn_fsTreeViewItem In UON2.children
                        If UON3.checked And Not UON2.checked Then
                            dr = dtQlikUONsPerfil.NewRow
                            dr("UON1") = UON1.value
                            dr("UON2") = UON2.value
                            dr("UON3") = UON3.value
                            dtQlikUONsPerfil.Rows.Add(dr)
                        End If
                    Next
                Next
            Next
        Next

        Dim dtQlikGMNsPerfil As New DataTable
        dtQlikGMNsPerfil.Columns.Add("GMN1", System.Type.GetType("System.String"))
        dtQlikGMNsPerfil.Columns.Add("GMN2", System.Type.GetType("System.String"))
        dtQlikGMNsPerfil.Columns.Add("GMN3", System.Type.GetType("System.String"))
        dtQlikGMNsPerfil.Columns.Add("GMN4", System.Type.GetType("System.String"))
        For Each GMN0 As cn_fsTreeViewItem In Perfil.QlikGMNS
            For Each GMN1 As cn_fsTreeViewItem In GMN0.children
                If GMN1.checked And Not GMN0.checked Then
                    dr = dtQlikGMNsPerfil.NewRow
                    dr("GMN1") = GMN1.value
                    dtQlikGMNsPerfil.Rows.Add(dr)
                End If
                For Each GMN2 As cn_fsTreeViewItem In GMN1.children
                    If GMN2.checked And Not GMN1.checked Then
                        dr = dtQlikGMNsPerfil.NewRow
                        dr("GMN1") = GMN1.value
                        dr("GMN2") = GMN2.value
                        dtQlikGMNsPerfil.Rows.Add(dr)
                    End If
                    For Each GMN3 As cn_fsTreeViewItem In GMN2.children
                        If GMN3.checked And Not GMN2.checked Then
                            dr = dtQlikGMNsPerfil.NewRow
                            dr("GMN1") = GMN1.value
                            dr("GMN2") = GMN2.value
                            dr("GMN3") = GMN3.value
                            dtQlikGMNsPerfil.Rows.Add(dr)
                        End If
                        For Each GMN4 As cn_fsTreeViewItem In GMN3.children
                            If GMN4.checked And Not GMN3.checked Then
                                dr = dtQlikGMNsPerfil.NewRow
                                dr("GMN1") = GMN1.value
                                dr("GMN2") = GMN2.value
                                dr("GMN3") = GMN3.value
                                dr("GMN4") = GMN4.value
                                dtQlikGMNsPerfil.Rows.Add(dr)
                            End If
                        Next
                    Next
                Next
            Next
        Next

        Return DBServer.SG_Perfiles_Add(Perfil.Codigo, dtDenominacionesPerfil, Perfil.AccesosPerfil, dtAccionesPerfil, dtUONsPerfil, dtQlikUONsPerfil, dtQlikGMNsPerfil)
    End Function
    Public Sub UpdatePerfil(ByVal Perfil As Perfil)
        Authenticate()
        Dim dr As DataRow
        Dim dtDenominacionesPerfil As New DataTable
        dtDenominacionesPerfil.Columns.Add("IDI", System.Type.GetType("System.String"))
        dtDenominacionesPerfil.Columns.Add("DEN", System.Type.GetType("System.String"))
        For Each item As KeyValuePair(Of String, String) In Perfil.Denominaciones
            dr = dtDenominacionesPerfil.NewRow
            dr("IDI") = item.Key
            dr("DEN") = item.Value
            dtDenominacionesPerfil.Rows.Add(dr)
        Next

        Dim dtAccionesPerfil As New DataTable
        dtAccionesPerfil.Columns.Add("ACC", System.Type.GetType("System.Int32"))
        For Each item As KeyValuePair(Of String, Integer) In Perfil.AccionesPerfil
            dr = dtAccionesPerfil.NewRow
            dr("ACC") = item.Value
            dtAccionesPerfil.Rows.Add(dr)
        Next

        Dim dtUONsPerfil As New DataTable
        dtUONsPerfil.Columns.Add("UON1", System.Type.GetType("System.String"))
        dtUONsPerfil.Columns.Add("UON2", System.Type.GetType("System.String"))
        dtUONsPerfil.Columns.Add("UON3", System.Type.GetType("System.String"))
        For Each UON0 As cn_fsTreeViewItem In Perfil.UONS
            For Each UON1 As cn_fsTreeViewItem In UON0.children
                If UON1.checked And Not UON0.checked Then
                    dr = dtUONsPerfil.NewRow
                    dr("UON1") = UON1.value
                    dtUONsPerfil.Rows.Add(dr)
                End If
                For Each UON2 As cn_fsTreeViewItem In UON1.children
                    If UON2.checked And Not UON1.checked Then
                        dr = dtUONsPerfil.NewRow
                        dr("UON1") = UON1.value
                        dr("UON2") = UON2.value
                        dtUONsPerfil.Rows.Add(dr)
                    End If
                    For Each UON3 As cn_fsTreeViewItem In UON2.children
                        If UON3.checked And Not UON2.checked Then
                            dr = dtUONsPerfil.NewRow
                            dr("UON1") = UON1.value
                            dr("UON2") = UON2.value
                            dr("UON3") = UON3.value
                            dtUONsPerfil.Rows.Add(dr)
                        End If
                    Next
                Next
            Next
        Next

        Dim dtQlikUONsPerfil As New DataTable
        dtQlikUONsPerfil.Columns.Add("UON1", System.Type.GetType("System.String"))
        dtQlikUONsPerfil.Columns.Add("UON2", System.Type.GetType("System.String"))
        dtQlikUONsPerfil.Columns.Add("UON3", System.Type.GetType("System.String"))
        For Each UON0 As cn_fsTreeViewItem In Perfil.QlikUONS
            For Each UON1 As cn_fsTreeViewItem In UON0.children
                If UON1.checked And Not UON0.checked Then
                    dr = dtQlikUONsPerfil.NewRow
                    dr("UON1") = UON1.value
                    dtQlikUONsPerfil.Rows.Add(dr)
                End If
                For Each UON2 As cn_fsTreeViewItem In UON1.children
                    If UON2.checked And Not UON1.checked Then
                        dr = dtQlikUONsPerfil.NewRow
                        dr("UON1") = UON1.value
                        dr("UON2") = UON2.value
                        dtQlikUONsPerfil.Rows.Add(dr)
                    End If
                    For Each UON3 As cn_fsTreeViewItem In UON2.children
                        If UON3.checked And Not UON2.checked Then
                            dr = dtQlikUONsPerfil.NewRow
                            dr("UON1") = UON1.value
                            dr("UON2") = UON2.value
                            dr("UON3") = UON3.value
                            dtQlikUONsPerfil.Rows.Add(dr)
                        End If
                    Next
                Next
            Next
        Next

        Dim dtQlikGMNsPerfil As New DataTable
        dtQlikGMNsPerfil.Columns.Add("GMN1", System.Type.GetType("System.String"))
        dtQlikGMNsPerfil.Columns.Add("GMN2", System.Type.GetType("System.String"))
        dtQlikGMNsPerfil.Columns.Add("GMN3", System.Type.GetType("System.String"))
        dtQlikGMNsPerfil.Columns.Add("GMN4", System.Type.GetType("System.String"))
        For Each GMN0 As cn_fsTreeViewItem In Perfil.QlikGMNS
            For Each GMN1 As cn_fsTreeViewItem In GMN0.children
                If GMN1.checked And Not GMN0.checked Then
                    dr = dtQlikGMNsPerfil.NewRow
                    dr("GMN1") = GMN1.value
                    dtQlikGMNsPerfil.Rows.Add(dr)
                End If
                For Each GMN2 As cn_fsTreeViewItem In GMN1.children
                    If GMN2.checked And Not GMN1.checked Then
                        dr = dtQlikGMNsPerfil.NewRow
                        dr("GMN1") = GMN1.value
                        dr("GMN2") = GMN2.value
                        dtQlikGMNsPerfil.Rows.Add(dr)
                    End If
                    For Each GMN3 As cn_fsTreeViewItem In GMN2.children
                        If GMN3.checked And Not GMN2.checked Then
                            dr = dtQlikGMNsPerfil.NewRow
                            dr("GMN1") = GMN1.value
                            dr("GMN2") = GMN2.value
                            dr("GMN3") = GMN3.value
                            dtQlikGMNsPerfil.Rows.Add(dr)
                        End If
                        For Each GMN4 As cn_fsTreeViewItem In GMN3.children
                            If GMN4.checked And Not GMN3.checked Then
                                dr = dtQlikGMNsPerfil.NewRow
                                dr("GMN1") = GMN1.value
                                dr("GMN2") = GMN2.value
                                dr("GMN3") = GMN3.value
                                dr("GMN4") = GMN4.value
                                dtQlikGMNsPerfil.Rows.Add(dr)
                            End If
                        Next
                    Next
                Next
            Next
        Next

        Dim dtUNQA As New DataTable
        dtUNQA.Columns.Add("UNQA", System.Type.GetType("System.Int32"))
        For Each UNQA0 As cn_fsTreeViewItem In Perfil.UNQA

            If UNQA0.checked Then
                dr = dtUNQA.NewRow
                dr("UNQA") = UNQA0.value
                dtUNQA.Rows.Add(dr)
            End If

            For Each UNQA1 As cn_fsTreeViewItem In UNQA0.children
                If UNQA1.checked Then
                    dr = dtUNQA.NewRow
                    dr("UNQA") = UNQA1.value
                    dtUNQA.Rows.Add(dr)
                End If
                For Each UNQA2 As cn_fsTreeViewItem In UNQA1.children
                    If UNQA2.checked Then
                        dr = dtUNQA.NewRow
                        dr("UNQA") = UNQA2.value
                        dtUNQA.Rows.Add(dr)
                    End If
                Next
            Next
        Next


        DBServer.SG_Perfiles_Update(Perfil.Id, Perfil.Codigo, dtDenominacionesPerfil, Perfil.AccesosPerfil, dtAccionesPerfil, dtUONsPerfil, dtQlikUONsPerfil, dtQlikGMNsPerfil, dtUNQA)
    End Sub
    Public Sub UpdatePerfil_Acceso(ByVal idPerfil As Integer, ByVal tipoAcceso As String, ByVal checked As Boolean)
        Authenticate()
        DBServer.SG_Perfiles_Update_Acceso(idPerfil, tipoAcceso, checked)
    End Sub
    Public Function ComprobarPosibleEliminarPerfil(ByVal idPerfil As Integer) As Boolean
        Authenticate()
        Return DBServer.SG_Perfiles_ComprobarPosibleEliminarPerfil(idPerfil)
    End Function
    Public Sub DeletePerfil(ByVal idPerfil As Integer)
        Authenticate()
        DBServer.SG_Perfiles_Delete(idPerfil)
    End Sub
    ''' <summary>
    ''' Carga el perfil indicado
    ''' </summary>
    ''' <param name="idPerfil">id del perfil</param>
    ''' <param name="Idioma">idioma del usuario</param>
    ''' <param name="ModulosActivos">lista de modulos activos separados por comas</param>
    ''' <param name="ParametrosGenerales">Parametros Generales instalación</param>
    ''' <param name="ParametrosIntegracion">Parametros Integracion instalación</param>
    ''' <returns>Objeto perfil</returns>
    ''' <remarks>Llamada desde: SG\DetallePerfil.aspx.vb; Tiempo maximo:0,2</remarks>
    Public Function Obtener_Perfil(ByVal idPerfil As Integer, ByVal Idioma As String, ByVal ModulosActivos As String, _
                                   ByVal ParametrosGenerales As TiposDeDatos.ParametrosGenerales, ByVal ParametrosIntegracion As TiposDeDatos.ParametrosIntegracion) As Perfil
        Authenticate()
        Dim dsDatos As New DataSet
        dsDatos = DBServer.SG_Perfiles_Obtener_Detalle(idPerfil, Idioma, ModulosActivos)
        Dim oModulosActivos As String() = Split(ModulosActivos, ",")
        Try
            'Creamos los arboles de los menus, y como options las opciones de cada item del menu
            Dim oMenus_Acciones As New Dictionary(Of TipoAccesoModulos, List(Of cn_fsTreeViewItem))
            Dim oPerfil As New Perfil
            With oPerfil
                .Id = idPerfil
                .Codigo = dsDatos.Tables(2).Rows(0)("COD").ToString
                .PerfilAdministrador = CType(dsDatos.Tables(3).Rows(0)("ADM"), Boolean)
                'Obtenemos los menus de las acciones dependiendo de si el modulo esta activo en la instalacion o no
                Dim bHaySolicitudes As Boolean = False  'Controla si hay que mostrar el menú de acciones comunes a los módulos que utilizan solicitudes
                For Each acceso As String In oModulosActivos
                    Select Case CType(acceso, Integer)
                        Case 1
                            .MenuAcciones(TipoAccesoModulos.FSGS) = Get_MenuItems(dsDatos, TipoAccesoModulos.FSGS, ParametrosGenerales, ParametrosIntegracion)
                            .AccesosPerfil(TipoAccesoModulos.FSGS) = IIf(idPerfil = 0, False, CType(dsDatos.Tables(2).Rows(0)("FSGS"), Boolean))
                        Case 2
                            bHaySolicitudes = True
                            .MenuAcciones(TipoAccesoModulos.FSPM) = Get_MenuItems(dsDatos, TipoAccesoModulos.FSPM, ParametrosGenerales, ParametrosIntegracion)
                            .AccesosPerfil(TipoAccesoModulos.FSPM) = IIf(idPerfil = 0, False, CType(dsDatos.Tables(2).Rows(0)("FSPM"), Boolean))
                        Case 3
                            .MenuAcciones(TipoAccesoModulos.FSEP) = Get_MenuItems(dsDatos, TipoAccesoModulos.FSEP, ParametrosGenerales, ParametrosIntegracion)
                            .AccesosPerfil(TipoAccesoModulos.FSEP) = IIf(idPerfil = 0, False, CType(dsDatos.Tables(2).Rows(0)("FSEP"), Boolean))
                        Case 4
                            bHaySolicitudes = True
                            .MenuAcciones(TipoAccesoModulos.FSCM) = Get_MenuItems(dsDatos, TipoAccesoModulos.FSCM, ParametrosGenerales, ParametrosIntegracion)
                            .AccesosPerfil(TipoAccesoModulos.FSCM) = IIf(idPerfil = 0, False, CType(dsDatos.Tables(2).Rows(0)("FSCM"), Boolean))
                        Case 5
                            .MenuAcciones(TipoAccesoModulos.FSSM) = Get_MenuItems(dsDatos, TipoAccesoModulos.FSSM, ParametrosGenerales, ParametrosIntegracion)
                            .AccesosPerfil(TipoAccesoModulos.FSSM) = IIf(idPerfil = 0, False, CType(dsDatos.Tables(2).Rows(0)("FSSM"), Boolean))
                        Case 6
                            bHaySolicitudes = True
                            .MenuAcciones(TipoAccesoModulos.FSIM) = Get_MenuItems(dsDatos, TipoAccesoModulos.FSIM, ParametrosGenerales, ParametrosIntegracion)
                            .AccesosPerfil(TipoAccesoModulos.FSIM) = IIf(idPerfil = 0, False, CType(dsDatos.Tables(2).Rows(0)("FSIM"), Boolean))
                        Case 7
                            bHaySolicitudes = True
                            .MenuAcciones(TipoAccesoModulos.FSQA) = Get_MenuItems(dsDatos, TipoAccesoModulos.FSQA, ParametrosGenerales, ParametrosIntegracion)
                            .AccesosPerfil(TipoAccesoModulos.FSQA) = IIf(idPerfil = 0, False, CType(dsDatos.Tables(2).Rows(0)("FSQA"), Boolean))
                        Case 8
                            .MenuAcciones(TipoAccesoModulos.FSIS) = Get_MenuItems(dsDatos, TipoAccesoModulos.FSIS, ParametrosGenerales, ParametrosIntegracion)
                            .AccesosPerfil(TipoAccesoModulos.FSIS) = IIf(idPerfil = 0, False, CType(dsDatos.Tables(2).Rows(0)("FSIS"), Boolean))
                        Case 9
                            .MenuAcciones(TipoAccesoModulos.FSCN) = Get_MenuItems(dsDatos, TipoAccesoModulos.FSCN, ParametrosGenerales, ParametrosIntegracion)
                            .AccesosPerfil(TipoAccesoModulos.FSCN) = True
                        Case 10
                            .MenuAcciones(TipoAccesoModulos.FSBI) = Get_MenuItems(dsDatos, TipoAccesoModulos.FSBI, ParametrosGenerales, ParametrosIntegracion)
                            .AccesosPerfil(TipoAccesoModulos.FSBI) = IIf(idPerfil = 0, False, CType(dsDatos.Tables(2).Rows(0)("FSBI"), Boolean))
                        Case 11
                            .MenuAcciones(TipoAccesoModulos.FSAL) = Get_MenuItems(dsDatos, TipoAccesoModulos.FSAL, ParametrosGenerales, ParametrosIntegracion)
                            .AccesosPerfil(TipoAccesoModulos.FSAL) = IIf(idPerfil = 0, False, CType(dsDatos.Tables(2).Rows(0)("FSAL"), Boolean))
                    End Select
                Next
                If bHaySolicitudes Then
                    .MenuAcciones(TipoAccesoModulos.FSSOL) = Get_MenuItems(dsDatos, TipoAccesoModulos.FSSOL, ParametrosGenerales, ParametrosIntegracion)
                    .AccesosPerfil(TipoAccesoModulos.FSSOL) = IIf(idPerfil = 0, False, CType(dsDatos.Tables(2).Rows(0)("FSSOL"), Boolean))
                End If
                For Each rowDenominacion As DataRow In dsDatos.Tables(3).Rows
                    .Denominaciones(rowDenominacion("IDIOMA")) = rowDenominacion("DEN")
                Next
                For Each row As DataRow In dsDatos.Tables(4).Rows
                    .AccionesPerfil(CType(row("ACC"), String)) = CType(row("ACC"), Integer)
                Next


                Dim item0, item1, item2, item3 As cn_fsTreeViewItem
                item0 = New cn_fsTreeViewItem
                With item0
                    .value = dsDatos.Tables(6).Rows(0)("COD")
                    .text = dsDatos.Tables(6).Rows(0)("COD") & " - " & dsDatos.Tables(6).Rows(0)("DEN")
                    .selectable = False
                    .type = 5
                    .nivel = "UON0"
                    .expanded = True
                    .checked = False

                    .children = New List(Of cn_fsTreeViewItem)
                    With .children
                        For Each row1 As DataRow In dsDatos.Tables(7).Rows
                            item1 = New cn_fsTreeViewItem
                            With item1
                                .value = row1("COD")
                                .text = row1("COD") & " - " & row1("DEN")
                                .selectable = True
                                .type = 5
                                .nivel = "UON1"
                                .checked = Not IsDBNull(row1("CHECKED"))
                                .expanded = .checked
                                .children = New List(Of cn_fsTreeViewItem)
                                With .children
                                    For Each row2 As DataRow In row1.GetChildRows("REL_NIV1_NIV2")
                                        item2 = New cn_fsTreeViewItem
                                        With item2
                                            .value = row2("COD")
                                            .text = row2("COD") & " - " & row2("DEN")
                                            .selectable = True
                                            .type = 5
                                            .nivel = "UON2"
                                            .checked = Not IsDBNull(row2("CHECKED"))
                                            .expanded = .checked
                                            .children = New List(Of cn_fsTreeViewItem)
                                            With .children
                                                For Each row3 As DataRow In row2.GetChildRows("REL_NIV2_NIV3")
                                                    item3 = New cn_fsTreeViewItem
                                                    With item3
                                                        .value = row3("COD")
                                                        .text = row3("COD") & " - " & row3("DEN")
                                                        .selectable = True
                                                        .type = 5
                                                        .nivel = "UON3"
                                                        .checked = Not IsDBNull(row3("CHECKED"))
                                                        .expanded = .checked
                                                        If .expanded Then item2.expanded = True
                                                    End With
                                                    .Add(item3)
                                                Next
                                            End With
                                            If .expanded Then item1.expanded = True
                                        End With
                                        .Add(item2)
                                    Next
                                End With
                            End With
                            .Add(item1)
                        Next
                    End With

                End With
                .UONS.Add(item0)

                If Not dsDatos.Tables(10).Rows.Count = 0 Then
                    Dim view As New DataView(dsDatos.Tables(10))
                    Dim dtAcciones As DataTable = view.ToTable(True, "ACC")
                    Dim listaAcciones As List(Of Integer)
                    For Each Accion As DataRow In dtAcciones.Rows
                        listaAcciones = New List(Of Integer) 'Acciones que al ser checkeadas hacen que se checkeen otras
                        For Each accionCheckCheck As DataRow In dsDatos.Tables(10).Select("ACC=" & Accion("ACC") & " AND EST=1 AND EST_DEPEN=1")
                            listaAcciones.Add(accionCheckCheck("ACC_DEPEN"))
                        Next
                        .AccionesDependientesCheckCheck(Accion("ACC")) = listaAcciones
                        listaAcciones = New List(Of Integer) 'Acciones que al ser checkeadas hacen que se descheckeen otras
                        For Each accionCheckUnCheck As DataRow In dsDatos.Tables(10).Select("ACC=" & Accion("ACC") & " AND EST=1 AND EST_DEPEN=0")
                            listaAcciones.Add(accionCheckUnCheck("ACC_DEPEN"))
                        Next
                        .AccionesDependientesCheckUnCheck(Accion("ACC")) = listaAcciones
                        listaAcciones = New List(Of Integer) 'Acciones que al ser descheckeadas hacen que se checkeen otras
                        For Each accionUnCheckCheck As DataRow In dsDatos.Tables(10).Select("ACC=" & Accion("ACC") & " AND EST=0 AND EST_DEPEN=1")
                            listaAcciones.Add(accionUnCheckCheck("ACC_DEPEN"))
                        Next
                        .AccionesDependientesUnCheckCheck(Accion("ACC")) = listaAcciones
                        listaAcciones = New List(Of Integer) 'Acciones que al ser descheckeadas hacen que se descheckeen otras
                        For Each accionUnCheckUnCheck As DataRow In dsDatos.Tables(10).Select("ACC=" & Accion("ACC") & " AND EST=0 AND EST_DEPEN=0")
                            listaAcciones.Add(accionUnCheckUnCheck("ACC_DEPEN"))
                        Next
                        .AccionesDependientesUnCheckUnCheck(Accion("ACC")) = listaAcciones
                    Next
                End If



                If Not IsNothing(dsDatos.Tables(17)) AndAlso dsDatos.Tables(17).Rows.Count > 0 Then

                    For Each row1 As DataRow In dsDatos.Tables(17).Select("NIVEL=1")
                        item1 = New cn_fsTreeViewItem
                        With item1

                            .value = row1("ID")
                            .text = row1("COD") & " - " & row1("DEN")
                            .selectable = True
                            .type = 5
                            .nivel = "UNQA1"
                            .checked = Not IsDBNull(row1("CHECKED"))
                            .expanded = .checked
                            .children = New List(Of cn_fsTreeViewItem)
                            With .children
                                For Each row2 As DataRow In dsDatos.Tables(17).Select("UNQA1=" & row1("ID") & " AND NIVEL=2")
                                    item2 = New cn_fsTreeViewItem
                                    With item2
                                        .value = row2("ID")
                                        .text = row2("COD") & " - " & row2("DEN")
                                        .selectable = True
                                        .type = 5
                                        .nivel = "UNQA2"
                                        .checked = Not IsDBNull(row2("CHECKED"))
                                        .expanded = .checked
                                        .children = New List(Of cn_fsTreeViewItem)
                                        With .children
                                            For Each row3 As DataRow In dsDatos.Tables(17).Select("UNQA1=" & row2("ID") & " AND UNQA2=" & row1("ID") & " AND NIVEL=3")
                                                item3 = New cn_fsTreeViewItem
                                                With item3
                                                    .value = row3("ID")
                                                    .text = row3("COD") & " - " & row3("DEN")
                                                    .selectable = True
                                                    .type = 5
                                                    .nivel = "UNQA3"
                                                    .checked = Not IsDBNull(row3("CHECKED"))
                                                    .expanded = .checked
                                                    If .expanded Then item2.expanded = True
                                                End With
                                                .Add(item3)
                                            Next
                                        End With
                                        If .expanded Then item1.expanded = True
                                    End With
                                    .Add(item2)
                                Next
                            End With
                        End With
                        .UNQA.Add(item1)
                    Next
                    '    End With
                    'End With
                    '.UNQA.Add(item0)
                End If




                item0 = New cn_fsTreeViewItem
                With item0
                    .value = dsDatos.Tables(6).Rows(0)("COD")
                    .text = dsDatos.Tables(6).Rows(0)("COD") & " - " & dsDatos.Tables(6).Rows(0)("DEN")
                    .selectable = False
                    .type = 5
                    .nivel = "UON0"
                    .expanded = True
                    .checked = False

                    .children = New List(Of cn_fsTreeViewItem)
                    With .children
                        For Each row1 As DataRow In dsDatos.Tables(7).Rows
                            item1 = New cn_fsTreeViewItem
                            With item1
                                .value = row1("COD")
                                .text = row1("COD") & " - " & row1("DEN")
                                .selectable = True
                                .type = 5
                                .nivel = "UON1"
                                .checked = Not IsDBNull(row1("CHECKED_QLIK"))
                                .expanded = .checked
                                .children = New List(Of cn_fsTreeViewItem)
                                With .children
                                    For Each row2 As DataRow In row1.GetChildRows("REL_NIV1_NIV2")
                                        item2 = New cn_fsTreeViewItem
                                        With item2
                                            .value = row2("COD")
                                            .text = row2("COD") & " - " & row2("DEN")
                                            .selectable = True
                                            .type = 5
                                            .nivel = "UON2"
                                            .checked = Not IsDBNull(row2("CHECKED_QLIK"))
                                            .expanded = .checked
                                            .children = New List(Of cn_fsTreeViewItem)
                                            With .children
                                                For Each row3 As DataRow In row2.GetChildRows("REL_NIV2_NIV3")
                                                    item3 = New cn_fsTreeViewItem
                                                    With item3
                                                        .value = row3("COD")
                                                        .text = row3("COD") & " - " & row3("DEN")
                                                        .selectable = True
                                                        .type = 5
                                                        .nivel = "UON3"
                                                        .checked = Not IsDBNull(row3("CHECKED_QLIK"))
                                                        .expanded = .checked
                                                        If .expanded Then item2.expanded = True
                                                    End With
                                                    .Add(item3)
                                                Next
                                            End With
                                            If .expanded Then item1.expanded = True
                                        End With
                                        .Add(item2)
                                    Next
                                End With
                            End With
                            .Add(item1)
                        Next
                    End With
                End With
                .QlikUONS.Add(item0)

                Dim item4 As cn_fsTreeViewItem
                item0 = New cn_fsTreeViewItem
                With item0
                    .value = dsDatos.Tables(6).Rows(0)("COD")
                    .text = dsDatos.Tables(6).Rows(0)("COD") & " - " & dsDatos.Tables(6).Rows(0)("DEN")
                    .selectable = False
                    .type = 5
                    .nivel = "GMN0"
                    .expanded = True
                    .checked = False

                    .children = New List(Of cn_fsTreeViewItem)
                    With .children
                        For Each row1 As DataRow In dsDatos.Tables(13).Rows
                            item1 = New cn_fsTreeViewItem
                            With item1
                                .value = row1("COD")
                                .text = row1("COD") & " - " & row1("DEN")
                                .selectable = True
                                .type = 5
                                .nivel = "GMN1"
                                .checked = Not IsDBNull(row1("CHECKED"))
                                .expanded = .checked
                                .children = New List(Of cn_fsTreeViewItem)
                                With .children
                                    For Each row2 As DataRow In row1.GetChildRows("REL_NIV1_NIV2_GMNS")
                                        item2 = New cn_fsTreeViewItem
                                        With item2
                                            .value = row2("COD")
                                            .text = row2("COD") & " - " & row2("DEN")
                                            .selectable = True
                                            .type = 5
                                            .nivel = "GMN2"
                                            .checked = Not IsDBNull(row2("CHECKED"))
                                            .expanded = .checked
                                            .children = New List(Of cn_fsTreeViewItem)
                                            With .children
                                                For Each row3 As DataRow In row2.GetChildRows("REL_NIV2_NIV3_GMNS")
                                                    item3 = New cn_fsTreeViewItem
                                                    With item3
                                                        .value = row3("COD")
                                                        .text = row3("COD") & " - " & row3("DEN")
                                                        .selectable = True
                                                        .type = 5
                                                        .nivel = "GMN3"
                                                        .checked = Not IsDBNull(row3("CHECKED"))
                                                        .expanded = .checked
                                                        .children = New List(Of cn_fsTreeViewItem)
                                                        With .children
                                                            For Each row4 As DataRow In row3.GetChildRows("REL_NIV3_NIV4_GMNS")
                                                                item4 = New cn_fsTreeViewItem
                                                                With item4
                                                                    .value = row4("COD")
                                                                    .text = row4("COD") & " - " & row4("DEN")
                                                                    .selectable = True
                                                                    .type = 5
                                                                    .nivel = "GMN4"
                                                                    .checked = Not IsDBNull(row4("CHECKED"))
                                                                    .expanded = .checked
                                                                    If .expanded Then item3.expanded = True
                                                                End With
                                                                .Add(item4)
                                                            Next
                                                        End With
                                                        If .expanded Then item2.expanded = True
                                                    End With
                                                    .Add(item3)
                                                Next
                                            End With
                                            If .expanded Then item1.expanded = True
                                        End With
                                        .Add(item2)
                                    Next
                                End With
                            End With
                            .Add(item1)
                        Next
                    End With
                End With
                .QlikGMNS.Add(item0)
            End With
            Return oPerfil
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' Carga los menus de un modulo dado
    ''' </summary>
    ''' <param name="dsDatos">tabla con todas las accmen</param>
    ''' <param name="Modulo">1-gs 2-pm 3-ep ...</param>
    ''' <param name="ParametrosGenerales">ParametrosGenerales</param>
    ''' <param name="ParametrosIntegracion">ParametrosIntegracion</param>
    ''' <returns>Lista de items, donde el item es un menu</returns>
    ''' <remarks>Llamada desde: Obtener_Perfil; Tiempo maximo:0</remarks>
    Private Function Get_MenuItems(ByVal dsDatos As DataSet, ByVal Modulo As Integer, _
                    ByVal ParametrosGenerales As TiposDeDatos.ParametrosGenerales, ByVal ParametrosIntegracion As TiposDeDatos.ParametrosIntegracion) As List(Of cn_fsTreeViewItem)
        Dim oMenu_Items As New List(Of cn_fsTreeViewItem)
        Try
            Dim oMenu_Item As cn_fsTreeViewItem
            Dim idAnterior As Integer = 0
            For Each row As DataRow In dsDatos.Tables(0).Select("ACCMEN_ID IS NULL AND MODULO=" & Modulo, "ORDEN ASC")
                If Not idAnterior = row("ID") Then
                    oMenu_Item = New cn_fsTreeViewItem

                    With oMenu_Item
                        .value = row("ID")
                        .text = row("DEN")
                        .nivel = 1

                        .children = New List(Of cn_fsTreeViewItem)
                        .children = Children_MenuItems(dsDatos, row("ID"), Modulo, 1, ParametrosGenerales, ParametrosIntegracion)
                        .options = Options_MenuItem(dsDatos.Tables(1), row("ID"), ParametrosGenerales, ParametrosIntegracion, (dsDatos.Tables(12).Rows(0)("OBLPROVEEQP") = 0))
                        .selectable = IIf(.options.Count > 0, True, False)
                        .type = IIf(.options.Count > 0, 3, 0)
                    End With
                    idAnterior = row("ID")
                    If Not oMenu_Item.children.Count = 0 OrElse Not oMenu_Item.options.Count = 0 Then
                        If row("ID") = 1124 Then
                            If Not ((dsDatos.Tables(11).Rows(0)("WIN_SEC") = TiposDeAutenticacion.Windows OrElse _
                                        dsDatos.Tables(11).Rows(0)("WIN_SEC") = TiposDeAutenticacion.LDAP) AndAlso _
                                    (dsDatos.Tables(11).Rows(0)("WIN_SEC_WEB") = TiposDeAutenticacion.Windows OrElse _
                                        dsDatos.Tables(11).Rows(0)("WIN_SEC_WEB") = TiposDeAutenticacion.LDAP)) Then oMenu_Items.Add(oMenu_Item)
                        Else
                            oMenu_Items.Add(oMenu_Item)
                        End If
                    End If
                End If
            Next

            Return oMenu_Items
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' Carga los submenus de un menu dado
    ''' </summary>
    ''' <param name="dsDatos">tabla con todas las acc</param>
    ''' <param name="IdPadre">menu padre</param>
    ''' <param name="Modulo">1-gs 2-pm 3-ep ...</param>
    ''' <param name="Nivel">Nivel submenus</param>
    ''' <param name="ParametrosGenerales">ParametrosGenerales</param>
    ''' <param name="ParametrosIntegracion">ParametrosIntegracion</param>
    ''' <returns>Lista de items, donde el item es un submenu</returns>
    ''' <remarks>Llamada desde: Get_MenuItems; Tiempo maximo:0</remarks>
    Private Function Children_MenuItems(ByVal dsDatos As DataSet, ByVal IdPadre As String, ByVal Modulo As Integer, ByVal Nivel As Integer, _
                ByVal ParametrosGenerales As TiposDeDatos.ParametrosGenerales, ByVal ParametrosIntegracion As TiposDeDatos.ParametrosIntegracion) As List(Of cn_fsTreeViewItem)
        Dim oMenu_Items As New List(Of cn_fsTreeViewItem)
        Try
            Dim oMenu_Item As cn_fsTreeViewItem
            Dim idAnterior As Integer = 0
            Dim AgregarItem As Boolean
            For Each row As DataRow In dsDatos.Tables(0).Select("ACCMEN_ID=" & IdPadre & " AND MODULO=" & Modulo, "ORDEN ASC")
                If Not idAnterior = row("ID") Then
                    oMenu_Item = New cn_fsTreeViewItem

                    With oMenu_Item
                        .value = row("ID")
                        .text = row("DEN")
                        .nivel = Nivel + 1

                        .children = New List(Of cn_fsTreeViewItem)
                        .children = Children_MenuItems(dsDatos, row("ID"), Modulo, Nivel + 1, ParametrosGenerales, ParametrosIntegracion)
                        .options = Options_MenuItem(dsDatos.Tables(1), row("ID"), ParametrosGenerales, ParametrosIntegracion, (dsDatos.Tables(12).Rows(0)("OBLPROVEEQP") = 0))
                        .selectable = IIf(.options.Count > 0, True, False)
                        .type = IIf(.options.Count > 0, 3, 0)
                    End With
                    idAnterior = row("ID")
                    AgregarItem = False
                    If Not oMenu_Item.children.Count = 0 OrElse Not oMenu_Item.options.Count = 0 Then
                        AgregarItem = True
                        Select Case row("ID")
                            Case 1129 'Menu seguridad,Seguridad>Cambio contraseña
                                If ((dsDatos.Tables(11).Rows(0)("WIN_SEC") = TiposDeAutenticacion.Windows OrElse _
                                        dsDatos.Tables(11).Rows(0)("WIN_SEC") = TiposDeAutenticacion.LDAP) AndAlso _
                                    (dsDatos.Tables(11).Rows(0)("WIN_SEC_WEB") = TiposDeAutenticacion.Windows OrElse _
                                        dsDatos.Tables(11).Rows(0)("WIN_SEC_WEB") = TiposDeAutenticacion.LDAP)) Then AgregarItem = False
                            Case 1059 'Proveedores de portal
                                If Not dsDatos.Tables(11).Rows(0)("INSTWEB") = 2 Then AgregarItem = False
                            Case 1062, 1063 'Proveedores>Equipos por proveedor, Proveedores>Proveedores por equipo
                                If Not dsDatos.Tables(12).Rows(0)("OBLPROVEEQP") = 2 Then AgregarItem = False
                            Case 1161 'Presupuestos>Partidas de control presupuestario
                                If Not ParametrosGenerales.gbAccesoFSSM Then AgregarItem = False
                            Case 1160 'Presupuestos anuales
                                If Not CType(dsDatos.Tables(12).Rows(0)("USAPRES1"), Boolean) AndAlso Not CType(dsDatos.Tables(12).Rows(0)("USAPRES2"), Boolean) Then AgregarItem = False
                            Case 1066, 1102 'Presupuesto anual proyecto 1,Informes>Aplicados>proyecto 1
                                If Not CType(dsDatos.Tables(12).Rows(0)("USAPRES1"), Boolean) Then
                                    AgregarItem = False
                                Else
                                    oMenu_Item.text = dsDatos.Tables(6).Select("ID=22")(0)("DEN")
                                End If
                            Case 1067, 1103 'Presupuesto anual proyecto 2,Informes>Aplicados>proyecto 2
                                If Not CType(dsDatos.Tables(12).Rows(0)("USAPRES2"), Boolean) Then
                                    AgregarItem = False
                                Else
                                    oMenu_Item.text = dsDatos.Tables(6).Select("ID=23")(0)("DEN")
                                End If
                            Case 1068, 1097, 1105 'Presupuesto proyecto 3,Informes>Negociados>proyecto 3,Informes>Aplicados>proyecto 3
                                If Not CType(dsDatos.Tables(12).Rows(0)("USAPRES3"), Boolean) Then
                                    AgregarItem = False
                                Else
                                    oMenu_Item.text = dsDatos.Tables(6).Select("ID=29")(0)("DEN")
                                End If
                            Case 1069, 1098, 1106 'Presupuesto proyecto 4,Informes>Negociados>proyecto 4,Informes>Aplicados>proyecto 4
                                If Not CType(dsDatos.Tables(12).Rows(0)("USAPRES4"), Boolean) Then
                                    AgregarItem = False
                                Else
                                    oMenu_Item.text = dsDatos.Tables(6).Select("ID=30")(0)("DEN")
                                End If
                            Case 1158, 1159, 1072 'Solicitudes>Formularios,Solicitudes>Cumplimentación,Procesos>Solicitudes de compras
                                If Not ParametrosGenerales.gbSolicitudesCompras Then AgregarItem = False
                            Case 1085 'Catalogo
                                If Not ParametrosGenerales.gbAccesoFSEP Then AgregarItem = False
                            Case 1088 'Pedido directo
                                If Not CType(dsDatos.Tables(11).Rows(0)("PEDIDO"), Boolean) Then AgregarItem = False
                            Case 1089, 1090 'Pedidos>Seguimiento,Pedidos>Recepcion
                                If Not ParametrosGenerales.gbAccesoFSEP OrElse Not CType(dsDatos.Tables(11).Rows(0)("PEDIDO"), Boolean) Then AgregarItem = False
                            Case 1111 'Parámetros>Integración
                                If Not ParametrosGenerales.g_bAccesoIS Then AgregarItem = False
                            Case 1108   'Informes>Evolución de PROY1 
                                If Not CType(dsDatos.Tables(12).Rows(0)("USAPRES1"), Boolean) Then
                                    AgregarItem = False
                                Else
                                    oMenu_Item.text = oMenu_Item.text.Replace("PROY1", dsDatos.Tables(6).Select("ID=22")(0)("DEN"))
                                End If
                            Case 1109   'Informes>Evolución de PROY2
                                If Not CType(dsDatos.Tables(12).Rows(0)("USAPRES2"), Boolean) Then
                                    AgregarItem = False
                                Else
                                    oMenu_Item.text = oMenu_Item.text.Replace("PROY2", dsDatos.Tables(6).Select("ID=23")(0)("DEN"))
                                End If                            
                        End Select
                    End If
                    If AgregarItem Then oMenu_Items.Add(oMenu_Item)
                End If
            Next

            Return oMenu_Items
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' Carga las restricciones/permisos de un menu dado
    ''' </summary>
    ''' <param name="dtDatos">tabla con todas las acc</param>
    ''' <param name="IdMenuItem">menu padre</param>
    ''' <param name="ParametrosGenerales">ParametrosGenerales</param>
    ''' <param name="ParametrosIntegracion">ParametrosIntegracion</param>
    ''' <param name="OblProveEqp">en tabla 12. Pargen_gest.OBLPROVEEQP</param>
    ''' <returns>Lista de items, donde el item es una restriccion/permiso </returns>
    ''' <remarks>Llamada desde: Children_MenuItems     Get_MenuItems; Tiempo maximo:0</remarks>
    Private Function Options_MenuItem(ByVal dtDatos As DataTable, ByVal IdMenuItem As Integer, ByVal ParametrosGenerales As TiposDeDatos.ParametrosGenerales _
                                      , ByVal ParametrosIntegracion As TiposDeDatos.ParametrosIntegracion, ByVal OblProveEqp As Boolean) As List(Of cn_fsItem)
        Dim oOptions_MenuItem As New List(Of cn_fsItem)
        Try
            Dim oOption_MenuItem As cn_fsItem
            Dim idAnterior As Integer = 0
            Dim AgregarItem As Boolean

            For Each row As DataRow In dtDatos.Select("ACCMEN_ID=" & IdMenuItem, "ORDEN ASC")
                If Not idAnterior = row("ID") Then
                    oOption_MenuItem = New cn_fsItem

                    With oOption_MenuItem
                        .value = row("ID")
                        .text = row("DEN")

                        If CType(row("UON"), Boolean) Then
                            .imgSrc = ConfigurationManager.AppSettings("ruta") & "images/UON.png"
                        ElseIf CType(row("UNQA"), Boolean) Then
                            .imgSrc = ConfigurationManager.AppSettings("ruta") & "images/UNQA.png"
                        Else
                            .imgSrc = String.Empty
                        End If
                        '.imgSrc = IIf(CType(row("UON"), Boolean), ConfigurationManager.AppSettings("ruta") & "images/UON.png", "")
                    End With
                    idAnterior = row("ID")

                    AgregarItem = False
                    Select Case row("ID")
                        Case 23006 'Permitir ver los pedidos de los centros de coste del usuario
                            If ParametrosGenerales.gbAccesoFSSM Then AgregarItem = True
                        Case 23007 'Permitir recepcionar pedidos de los centros de coste del usuario
                            If ParametrosGenerales.gbAccesoFSSM Then AgregarItem = True
                        Case 23008 'Permitir activar el bloqueo de albaranes para facturación
                            If ParametrosGenerales.g_bAccesoFSFA Then AgregarItem = True

                        Case 24202 'Restringir a proveedores del equipo del usuario
                            If OblProveEqp Then AgregarItem = True
                        Case 23000 'Permitir realizar pedidos libres
                            AgregarItem = ParametrosGenerales.gbPedidoLibre
                        Case AccionesDeSeguridad.EPRestringirSeleccionCentroAprovUsuario 'Restringir el centro de aprovisionamiento al usuario
                            AgregarItem = ParametrosGenerales.gbUsar_OrgCompras
                        Case 24003 'Permitir modificar el periodo de antelación para considerar los certificados próximos a caducar
                            AgregarItem = ParametrosGenerales.gbAccesoQACertificados
                        Case 24300 'Permitir solicitar certificados
                            AgregarItem = ParametrosGenerales.gbAccesoQACertificados
                        Case 24301 'Permitir renovar certificados
                            AgregarItem = ParametrosGenerales.gbAccesoQACertificados
                        Case 24302 'Permitir enviar certificados
                            AgregarItem = ParametrosGenerales.gbAccesoQACertificados
                        Case 24303 'Permitir publicar/despublicar certificados de otros usuarios
                            AgregarItem = ParametrosGenerales.gbAccesoQACertificados
                        Case 24304 'Permitir modificar datos de certificados de otros usuarios
                            AgregarItem = ParametrosGenerales.gbAccesoQACertificados
                        Case 24305 'Restringir la consulta de certificados a los solicitados por el usuario
                            AgregarItem = ParametrosGenerales.gbAccesoQACertificados
                        Case 24306 'Restringir la consulta de certificados a los solicitados en la unidad organizativa del usuario
                            AgregarItem = ParametrosGenerales.gbAccesoQACertificados
                        Case 24307 'Restringir la consulta de certificados a los solicitados en el departamento del usuario
                            AgregarItem = ParametrosGenerales.gbAccesoQACertificados
                        Case 24308 'Restringir la consulta a los certificados de proveedores vinculados a unidades de negocio del usuario
                            AgregarItem = ParametrosGenerales.gbAccesoQACertificados
                        Case 24309 'Restringir la consulta a los certificados de proveedores vinculados a las unidades de negocio especificadas
                            AgregarItem = ParametrosGenerales.gbAccesoQACertificados
                        Case 24400 'Permitir cerrar no conformidades con acciones sin finalizar
                            AgregarItem = ParametrosGenerales.gbAccesoQANoConformidad
                        Case 24401 'Permitir reabrir no conformidades cerradas
                            AgregarItem = ParametrosGenerales.gbAccesoQANoConformidad
                        Case 24402 'Permitir revisar el cierre de las no conformidades
                            AgregarItem = ParametrosGenerales.gbAccesoQANoConformidad
                        Case 24403 'Restringir la consulta de no conformidades a las realizadas por el usuario
                            AgregarItem = ParametrosGenerales.gbAccesoQANoConformidad
                        Case 24404 'Restringir la consulta de no conformidades a las realizadas en la unidad organizativa del usuario
                            AgregarItem = ParametrosGenerales.gbAccesoQANoConformidad
                        Case 24405 'Restringir la consulta de no conformidades a las realizadas en el departamento del usuario
                            AgregarItem = ParametrosGenerales.gbAccesoQANoConformidad
                        Case 24406 'Restringir la selección de revisores a la unidad organizativa del usuario
                            If Not ParametrosGenerales.gbFSQA_Revisor Then
                                AgregarItem = False
                            Else
                                AgregarItem = ParametrosGenerales.gbAccesoQANoConformidad
                            End If
                        Case 24407 'Restringir la selección de revisores al departamento del usuario
                            If Not ParametrosGenerales.gbFSQA_Revisor Then
                                AgregarItem = False
                            Else
                                AgregarItem = ParametrosGenerales.gbAccesoQANoConformidad
                            End If
                        Case 11205 'Restringir las acciones a los proveedores asociados al equipo del comprador
                            If OblProveEqp Then AgregarItem = True
                        Case 11502 'Restringir la selección del proveedor a los que tengan asociado el equipo del comprador
                            If OblProveEqp Then AgregarItem = True
                        Case 11603 'Restringir las acciones a los proveedores asociados al equipo del comprador
                            If OblProveEqp Then AgregarItem = True
                        Case 12017 'Permitir ver si el proceso ha sido marcado como de subasta
                            AgregarItem = ParametrosGenerales.gbSubasta
                        Case 12018 'Permitir modificar las opciones de subasta de un proceso
                            AgregarItem = ParametrosGenerales.gbSubasta
                        Case 11207 'Consulta de datos de acceso web
                            'SI INSTALACION NO BUYSITE (gParametrosGenerales.giINSTWEB <> conweb) no acciones de acceso web en prove
                            AgregarItem = False
                        Case 11208 'Modificación de datos de acceso web
                            'SI INSTALACION NO BUYSITE (gParametrosGenerales.giINSTWEB <> conweb) no acciones de acceso web en prove
                            AgregarItem = False
                        Case 20202, 20207, 20235, 20304, 20305
                            '20202	Seguimiento de pedidos de aprovisionamiento
                            '20235	Permitir modificar el precio y cantidad de las líneas de pedido que no procedan de adjudicación
                            '20207	Restringir las acciones si el usuario es de aprovisionamiento
                            '20304	Recepción de pedidos de aprovisionamiento
                            '20305	Mostrar sólo los pedidos en los que es receptor
                            AgregarItem = ParametrosGenerales.gbPedidosAprov
                        Case 20201, 20302, 20223, 20234
                            '20223	Permitir modificar el precio y la cantidad de los pedidos emitidos
                            '20234	Permitir modificar el precio y cantidad de las líneas de pedido que procedan de comité
                            '20302	Recepción de pedidos negociados
                            '20201	Seguimiento de pedidos negociados
                            AgregarItem = ParametrosGenerales.gbPedidosDirectos
                        Case 20239, 20309
                            '20239	Seguimiento de pedidos de ERP
                            '20309	Recepción de pedidos de ERP
                            AgregarItem = ParametrosGenerales.gbPedidosERP
                        Case 20224, 20225, 20116, 20117, 11702, 12026, 14701, 18102, 23016, 23020, 23024, 20132, 20245
                            '18102	Restringir las acciones a la unidad organizativa del usuario
                            '14701	Restringir las acciones a la unidad organizativa del usuario
                            '12026	Restringir la distribución en presupuestos por concepto 1 a la unidad organizativa del usuario
                            '11702	Restringir las acciones a la unidad organizativa del usuario
                            '20116	Restringir la selección de presupuestos por concepto 1 a la unidad organizativa del usuario
                            '20117	Permitir seleccionar presupuestos por concepto 1 de las unidades organizativas situadas por encima del usuario
                            '20224	Restringir la selección de presupuestos por concepto 1 a la unidad organizativa del usuario
                            '20225	Permitir seleccionar presupuestos por concepto 1 de las unidades organizativas situadas por encima del usuario
                            '23016  Permitir seleccionar presupuestos por concepto 1 de las unidades organizativas situadas por encima del usuario
                            '23020  Restringir la selección de presupuestos por concepto 1 a la unidad organizativa del usuario
                            '23024  Restringir la selección de presupuestos por concepto 1 a las unidades organizativas del perfil
                            AgregarItem = ParametrosGenerales.gbUsarPres1
                        Case 20226, 20227, 20118, 20119, 11902, 12027, 14801, 19102, 23017, 23021, 23025, 20133, 20246
                            '14801	Restringir las acciones a la unidad organizativa del usuario
                            '12027	Restringir la distribución en presupuestos por concepto 2 a la unidad organizativa del usuario
                            '19102	Restringir las acciones a la unidad organizativa del usuario
                            '11902	Restringir las acciones a la unidad organizativa del usuario
                            '20118	Restringir la selección de presupuestos por concepto 2 a la unidad organizativa del usuario
                            '20119	Permitir seleccionar presupuestos por concepto 2 de las unidades organizativas situadas por encima del usuario
                            '20226	Restringir la selección de presupuestos por concepto 2 a la unidad organizativa del usuario
                            '20227	Permitir seleccionar presupuestos por concepto 2 de las unidades organizativas situadas por encima del usuario
                            '23017  Permitir seleccionar presupuestos por concepto 2 de las unidades organizativas situadas por encima del usuario
                            '23021  Restringir la selección de presupuestos por concepto 2 a la unidad organizativa del usuario
                            '23025  Restringir la selección de presupuestos por concepto 2 a las unidades organizativas del perfil
                            '23060  Permitir modificar presupuestos por concepto 2 una vez integrado el pedido
                            AgregarItem = ParametrosGenerales.gbUsarPres2
                        Case 20228, 20229, 20120, 20121, 19202, 12028, 19401, 19601, 23018, 23022, 23026, 20134, 20247
                            '19401	Restringir las acciones a la unidad organizativa del usuario
                            '19601	Restringir las acciones a la unidad organizativa del usuario
                            '12028	Restringir la distribución en presupuestos por concepto 3 a la unidad organizativa del usuario
                            '19202	Restringir las acciones a la unidad organizativa del usuario
                            '20120	Restringir la selección de presupuestos por concepto 3 a la unidad organizativa del usuario
                            '20121	Permitir seleccionar presupuestos por concepto 3 de las unidades organizativas situadas por encima del usuario
                            '20228	Restringir la selección de presupuestos por concepto 3 a la unidad organizativa del usuario
                            '20229	Permitir seleccionar presupuestos por concepto 3 de las unidades organizativas situadas por encima del usuario
                            '23018  Permitir seleccionar presupuestos por concepto 3 de las unidades organizativas situadas por encima del usuario
                            '23022  Restringir la selección de presupuestos por concepto 3 a la unidad organizativa del usuario
                            '23026  Restringir la selección de presupuestos por concepto 3 a las unidades organizativas del perfil
                            AgregarItem = ParametrosGenerales.gbUsarPres3
                        Case 20230, 20231, 20122, 20123, 19302, 12029, 19501, 19701, 23019, 23023, 23027, 20135, 20248
                            '19501	Restringir las acciones a la unidad organizativa del usuario
                            '19701	Restringir las acciones a la unidad organizativa del usuario
                            '12029	Restringir la distribución en presupuestos por concepto 4 a la unidad organizativa del usuario
                            '19302	Restringir las acciones a la unidad organizativa del usuario
                            '20122	Restringir la selección de presupuestos por concepto 4 a la unidad organizativa del usuario
                            '20123	Permitir seleccionar presupuestos por concepto 4 de las unidades organizativas situadas por encima del usuario
                            '20230	Restringir la selección de presupuestos por concepto 4 a la unidad organizativa del usuario
                            '20231	Permitir seleccionar presupuestos por concepto 4 de las unidades organizativas situadas por encima del usuario
                            '23019  Permitir seleccionar presupuestos por concepto 4 de las unidades organizativas situadas por encima del usuario
                            '23023  Restringir la selección de presupuestos por concepto 4 a la unidad organizativa del usuario
                            '23027  Restringir la selección de presupuestos por concepto 4 a las unidades organizativas del perfil
                            AgregarItem = ParametrosGenerales.gbUsarPres4
                        Case 20259
                            AgregarItem = ParametrosGenerales.gbUsarPres1 And ParametrosGenerales.gbPedidosERP
                            '23059  Permitir modificar presupuestos por concepto 1 una vez integrado el pedido
                        Case 20260
                            AgregarItem = ParametrosGenerales.gbUsarPres2 And ParametrosGenerales.gbPedidosERP
                            '23060  Permitir modificar presupuestos por concepto 2 una vez integrado el pedido
                        Case 20261
                            AgregarItem = ParametrosGenerales.gbUsarPres3 And ParametrosGenerales.gbPedidosERP
                            '23061  Permitir modificar presupuestos por concepto 3 una vez integrado el pedido
                        Case 20262
                            AgregarItem = ParametrosGenerales.gbUsarPres4 And ParametrosGenerales.gbPedidosERP
                            '23061  Permitir modificar presupuestos por concepto 4 una vez integrado el pedido
                        Case 20224, 20225, 20116, 20117
                            '20116	Restringir la selección de presupuestos por concepto 1 a la unidad organizativa del usuario
                            '20117	Permitir seleccionar presupuestos por concepto 1 de las unidades organizativas situadas por encima del usuario
                            '20224	Restringir la selección de presupuestos por concepto 1 a la unidad organizativa del usuario
                            '20225	Permitir seleccionar presupuestos por concepto 1 de las unidades organizativas situadas por encima del usuario
                            AgregarItem = ParametrosGenerales.gbUsarPedPres1
                        Case 20226, 20227, 20118, 20119
                            '20226	Restringir la selección de presupuestos por concepto 2 a la unidad organizativa del usuario
                            '20227	Permitir seleccionar presupuestos por concepto 2 de las unidades organizativas situadas por encima del usuario
                            '20118	Restringir la selección de presupuestos por concepto 2 a la unidad organizativa del usuario
                            '20119	Permitir seleccionar presupuestos por concepto 2 de las unidades organizativas situadas por encima del usuario
                            AgregarItem = ParametrosGenerales.gbUsarPedPres2
                        Case 20228, 20229, 20120, 20121
                            '20228	Restringir la selección de presupuestos por concepto 3 a la unidad organizativa del usuario
                            '20229	Permitir seleccionar presupuestos por concepto 3 de las unidades organizativas situadas por encima del usuario
                            '20120	Restringir la selección de presupuestos por concepto 3 a la unidad organizativa del usuario
                            '20121	Permitir seleccionar presupuestos por concepto 3 de las unidades organizativas situadas por encima del usuario
                            AgregarItem = ParametrosGenerales.gbUsarPedPres3
                        Case 20230, 20231, 20122, 20123
                            '20230	Restringir la selección de presupuestos por concepto 4 a la unidad organizativa del usuario
                            '20231	Permitir seleccionar presupuestos por concepto 4 de las unidades organizativas situadas por encima del usuario
                            '20122	Restringir la selección de presupuestos por concepto 4 a la unidad organizativa del usuario
                            '20123	Permitir seleccionar presupuestos por concepto 4 de las unidades organizativas situadas por encima del usuario
                            AgregarItem = ParametrosGenerales.gbUsarPedPres4
                        Case 20265, 20266
                            AgregarItem = ParametrosGenerales.gbBajaLOGPedidos
                        Case 12061, 12137, 13028, 20257, 20139, 23013, 23014, 23015  'acciones de pedidos abiertos
                            AgregarItem = ParametrosGenerales.gbUsarPedidosAbiertos
                        Case 10101, 10102
                            '10101	Modificación de monedas
                            '10102	Restringir la modificación a la equivalencia
                            AgregarItem = Not (ParametrosIntegracion.gbSoloImportar_Mon)
                        Case 17201
                            '17201	Modificación de formas de pago
                            AgregarItem = Not (ParametrosIntegracion.gbSoloImportar_Pag)
                        Case 17401
                            '17401	Modificación de vías de pago
                            AgregarItem = Not (ParametrosIntegracion.gbSoloImportar_ViaPag)
                        Case 10201
                            '10201 Modificación de países y provincias
                            AgregarItem = Not (ParametrosIntegracion.gbSoloImportar_Pais)
                        Case 10301
                            '10301 PROVIModificar
                            AgregarItem = Not (ParametrosIntegracion.gbSoloImportar_Provi)
                        Case 10501
                            '10501	Modificación de unidades
                            AgregarItem = Not (ParametrosIntegracion.gbSoloImportar_Uni)
                        Case 10605
                            '10605	Modificación de destinos
                            AgregarItem = Not (ParametrosIntegracion.gbSoloImportar_Dest)
                        Case 10702
                            '10702	Modificación de artículos
                            'Tarea 3016: Ya no restringimos la modificación de artículos ya que no podremos dar de alta artículos en 
                            'las uon con erp de entrada.
                            AgregarItem = True
                        Case 11201, 11202, 11203
                            '11201  Añadir Proveedores
                            '11202  Eliminar Proveedores
                            '11203	Modificación de los datos generales de proveedores
                            AgregarItem = Not (ParametrosIntegracion.gbSoloImportar_Prove)
                        Case 20101, 20102, 20125, 20103
                            '20101	Emitir pedidos negociados
                            '20102	Modificación de precios
                            '20125	Permitir modificar los precios de pedido de las líneas que procedan de comité
                            '20103	Permitir superar la cantidad adjudicada
                            AgregarItem = Not (ParametrosIntegracion.gbSoloImportar_PedDirecto)
                        Case 12033, 12034, 12035, 12036, 20016, 20017, 20018, 20019, 20210, 20211, 20212, 20213, 12032
                            '12032	Permitir modificar estructura de un proceso creado desde una solicitud de compras
                            '12033	Permitir asignar solicitudes
                            '12034	Restringir la asignación de solicitudes a las solicitudes asignadas al usuario
                            '12035	Restringir la asignación de solicitudes a las solicitudes asignadas al equipo del comprador
                            '12036	Restringir la asignación de solicitudes a la unidad organizativa del usuario
                            '20016	Permitir asignar solicitudes
                            '20017	Restringir la asignación de solicitudes a las solicitudes asignadas al usuario
                            '20018	Restringir la asignación de solicitudes a las solicitudes asignadas al equipo del comprador
                            '20019	Restringir la asignación de solicitudes a la unidad organizativa del usuario
                            '20210	Seguimiento sólamente de pedidos con solicitudes asignadas al usuario
                            '20211	Restringir la búsqueda de solicitudes a las solicitudes asignadas al usuario
                            '20212	Restringir la búsqueda de solicitudes a las solicitudes asignadas al equipo del comprador
                            '20213	Restringir la búsqueda de solicitudes a la unidad organizativa del usuario
                            AgregarItem = ParametrosGenerales.gbSolicitudesCompras
                            'Case PEDSegModifCodERP no lo encuentro en GS
                            '    AgregarItem = Not (ParametrosGenerales.gbCodPersonalizDirecto = False And ParametrosGenerales.gbCodPersonalizPedido = False)
                        Case 12129, 13021 'Permitir realizar el traspaso de adjudicaciones a ERP
                            AgregarItem = ParametrosIntegracion.gbExportar_Adj
                        Case 17311 'Restringir la selección de proveedores a los del equipo del comprador
                            AgregarItem = OblProveEqp
                        Case 19800 'Permitir crear árboles de partidas de control de aprovisionamiento
                            AgregarItem = ParametrosGenerales.gbAccesoFSSM
                        Case 12133, 13024 'Permitir crear y modificar vistas de comparativa de calidad
                            AgregarItem = Not (ParametrosGenerales.gsAccesoFSQA = TipoAccesoFSQA.SinAcceso)
                        Case 10607, 10608
                            '10607 Gestionar Centros de Coste
                            '10608 Consultar Centros de Coste
                            AgregarItem = ParametrosGenerales.gbAccesoFSSM
                        Case 12520, 12521
                            '12520 Permitir asignar/desasignar grupos a proveedores.
                            '12521 Permitir bloquear/desbloquear el envío de ofertas en determinados grupos a proveedores.
                            If ParametrosGenerales.giProce_Prove_Grupos = 1 Then AgregarItem = True
                        Case 20240, 20128 'Permitir modificar auto factura
                            AgregarItem = ParametrosGenerales.g_bAccesoFSFA
                        Case 10709 'Consulta de impuestos
                            AgregarItem = ParametrosGenerales.g_bAccesoFSFA
                        Case 10710 'Modificación de impuestos
                            AgregarItem = ParametrosGenerales.g_bAccesoFSFA
                        Case 27000 'Configurar cubos
                            AgregarItem = ((ParametrosGenerales.giAccesoFSBITipo = TiposDeDatos.TipoAccesoFSBI.Completo) Or (ParametrosGenerales.giAccesoFSBITipo = TiposDeDatos.TipoAccesoFSBI.SoloCubosBI))
                        Case 27001, 27002
                            '27001 Configurar aplicaciones Qlik
                            '27002 Restringir la visibilidad a las unidades organizativas del perfil
                            AgregarItem = ((ParametrosGenerales.giAccesoFSBITipo = TiposDeDatos.TipoAccesoFSBI.Completo) Or (ParametrosGenerales.giAccesoFSBITipo = TiposDeDatos.TipoAccesoFSBI.SoloQlikApps))
                        Case 23030, 23031, 23032, 23033, 23034
                            AgregarItem = ParametrosGenerales.gbBajaLOGPedidos
                        Case Else
                            AgregarItem = True
                    End Select
                    If AgregarItem Then oOptions_MenuItem.Add(oOption_MenuItem)
                End If
            Next

            Return oOptions_MenuItem
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function CompararPerfiles(ByVal IdsPerfiles As String, ByVal Idioma As String, _
                                    ByVal ModulosActivos As String) As DataSet
        Authenticate()
        'Creamos una tabla con las columnas que nos serviran para gestionar el grid de la comparativa. Expandiremos y contraeremos las filas
        'en servidor por lo que necesitamos saber si una fila tiene hijas, si esta expandida, visible....
        Dim dsDatos As DataSet = DBServer.SG_Perfiles_CompararPerfiles(IdsPerfiles, ModulosActivos, Idioma)
        Dim dtEsquema As New DataTable
        Dim row, rowAccesos As DataRow
        dtEsquema.Columns.Add("NIVEL", System.Type.GetType("System.Int32"))
        dtEsquema.Columns.Add("TIENEHIJOS", System.Type.GetType("System.Boolean"))
        dtEsquema.Columns.Add("ESMODULO", System.Type.GetType("System.Boolean"))
        dtEsquema.Columns.Add("ESCONTENEDOR", System.Type.GetType("System.Boolean"))
        dtEsquema.Columns.Add("RESALTARFILA", System.Type.GetType("System.Boolean"))
        dtEsquema.Columns.Add("ID", System.Type.GetType("System.Int32"))
        dtEsquema.Columns.Add("IDPADRE", System.Type.GetType("System.Int32"))
        dtEsquema.Columns.Add("DEN", System.Type.GetType("System.String"))
        dtEsquema.Columns.Add("VISIBLE", System.Type.GetType("System.Boolean"))
        dtEsquema.Columns.Add("EXPANDED", System.Type.GetType("System.Boolean"))
        For Each idPerfil As String In Split(IdsPerfiles, ",")
            dtEsquema.Columns.Add(idPerfil.ToString, System.Type.GetType("System.Boolean"))
        Next
        For Each modulo As String In Split(ModulosActivos, ",")
            row = dtEsquema.NewRow
            row("NIVEL") = 1
            row("ESMODULO") = True
            row("ID") = CType(modulo, Integer)
            row("IDPADRE") = DBNull.Value
            row("DEN") = ""
            row("VISIBLE") = True
            row("EXPANDED") = False
            row("ESCONTENEDOR") = False
            row("RESALTARFILA") = False
            For Each idPerfil As String In Split(IdsPerfiles, ",")
                rowAccesos = dsDatos.Tables(2).Select("ID=" & CType(idPerfil, Integer))(0)
                Select Case CType(modulo, Integer)
                    Case TipoAccesoModulos.FSGS
                        row(idPerfil) = rowAccesos("FSGS")
                    Case TipoAccesoModulos.FSPM
                        row(idPerfil) = rowAccesos("FSPM")
                    Case TipoAccesoModulos.FSEP
                        row(idPerfil) = rowAccesos("FSEP")
                    Case TipoAccesoModulos.FSCM
                        row(idPerfil) = rowAccesos("FSCM")
                    Case TipoAccesoModulos.FSSM
                        row(idPerfil) = rowAccesos("FSSM")
                    Case TipoAccesoModulos.FSIM
                        row(idPerfil) = rowAccesos("FSIM")
                    Case TipoAccesoModulos.FSQA
                        row(idPerfil) = rowAccesos("FSQA")
                    Case TipoAccesoModulos.FSIS
                        row(idPerfil) = rowAccesos("FSIS")
                    Case TipoAccesoModulos.FSCN
                        row(idPerfil) = True
                    Case TipoAccesoModulos.FSBI
                        row(idPerfil) = rowAccesos("FSBI")
                    Case TipoAccesoModulos.FSAL
                        row(idPerfil) = rowAccesos("FSAL")
                End Select
            Next
            row("TIENEHIJOS") = True
            Dim menusHijo As DataRow() = dsDatos.Tables(3).Select("MODULO=" & modulo & " AND ACCMEN_ID IS NULL")
            dtEsquema.Rows.Add(row)
            If menusHijo.Length = 1 Then menusHijo = dsDatos.Tables(3).Select("MODULO IS NULL AND ACCMEN_ID=" & menusHijo(0)("ID"))
            MenuAcciones(modulo, 1, "", dsDatos.Tables(3), menusHijo, dtEsquema, IdsPerfiles)
        Next
        Dim dsResultado As New DataSet
        dsResultado.Tables.Add(dtEsquema)
        dsResultado.Tables(0).TableName = "ESQUEMA"
        dsResultado.Tables("ESQUEMA").PrimaryKey = New DataColumn() {dsResultado.Tables("ESQUEMA").Columns("ID")}
        CheckAcciones(dsResultado.Tables(0), dsDatos.Tables(1))
        ResaltarFilas(dsResultado, IdsPerfiles)
        dsResultado.Tables.Add(dsDatos.Tables(0).Copy)
        dsResultado.Tables(1).TableName = "DENOMINACIONES_PERFIL"
        Return dsResultado
    End Function
    Public Sub MenuAcciones(ByVal IdPadre As Integer, ByVal nivel As Integer, ByVal Arbol As String, ByVal dtDatos As DataTable, _
                            ByVal rows As DataRow(), ByRef dtResultado As DataTable, ByVal IdsPerfiles As String)
        Dim rowMenu As DataRow
        Dim rowsHijos As DataRow()
        For Each drow As DataRow In rows
            rowMenu = dtResultado.NewRow
            rowMenu("NIVEL") = nivel + 1
            rowMenu("VISIBLE") = False
            rowMenu("EXPANDED") = False
            rowMenu("ESMODULO") = False
            rowMenu("ID") = drow("ID")
            rowMenu("IDPADRE") = IdPadre
            rowMenu("DEN") = drow("DEN")
            rowMenu("ESCONTENEDOR") = Not IsDBNull(drow("MODULO"))
            rowMenu("RESALTARFILA") = False
            For Each idPerfil As String In Split(IdsPerfiles, ",")
                rowMenu(idPerfil) = IIf(IsDBNull(drow("MODULO")), False, DBNull.Value)
            Next
            rowsHijos = dtDatos.Select("ACCMEN_ID=" & drow("ID"))
            rowMenu("TIENEHIJOS") = (rowsHijos.Length > 0)
            dtResultado.Rows.Add(rowMenu)
            MenuAcciones(drow("ID"), nivel + 1, IIf(Arbol = "", "", Arbol & "#") & IdPadre, dtDatos, rowsHijos, dtResultado, IdsPerfiles)
        Next
    End Sub
    Private Sub CheckAcciones(ByRef dtResultado As DataTable, ByVal dtAcciones As DataTable)
        For Each accion As DataRow In dtAcciones.Rows
            If Not dtResultado.Rows.Find(accion("ACC")) Is Nothing Then
                dtResultado.Rows.Find(accion("ACC"))(accion("PERF").ToString) = True
            End If
        Next
    End Sub
    Private Sub ResaltarFilas(ByRef ds As DataSet, ByVal IdsPerfiles As String)
        Dim value As New Nullable(Of Boolean)
        For Each row As DataRow In ds.Tables(0).Select("NOT ESCONTENEDOR")
            value = New Nullable(Of Boolean)
            For Each idPerfil As String In Split(IdsPerfiles, ",")
                If value.HasValue Then
                    If Not value = row(idPerfil) Then
                        row("RESALTARFILA") = True
                        If Not IsDBNull(row("IDPADRE")) AndAlso ds.Tables(0).Select("ID=" & row("IDPADRE")).Length > 0 Then ResaltarPadres(ds, row("IDPADRE"))
                        Exit For
                    End If
                Else
                    value = row(idPerfil)
                End If
            Next
        Next
    End Sub
    Private Sub ResaltarPadres(ByRef ds As DataSet, ByVal idPadre As Integer)
        'Si algun valor de la fila es diferente daremos un color a la fila y por tanto a su padre
        Dim drow As DataRow = ds.Tables(0).Select("ID=" & idPadre)(0)
        drow("RESALTARFILA") = True
        If Not IsDBNull(drow("IDPADRE")) AndAlso ds.Tables(0).Select("ID=" & drow("IDPADRE")).Length > 0 Then ResaltarPadres(ds, drow("IDPADRE"))
    End Sub
    Public Function Comprobar_CodigoPerfilValido(ByVal IdPerfil As Integer, ByVal CodPerfil As String) As Boolean
        Authenticate()
        Dim ds As DataTable = DBServer.SG_Comprobar_CodigoPerfilValido(IdPerfil, CodPerfil)
        Return (ds.Rows.Count = 0)
    End Function
End Class
