﻿Imports System.Xml
Imports MigraDoc.Rendering
Imports PdfSharp.Pdf
Imports Fullstep.FSNServer.CFacturae
Imports MigraDoc.DocumentObjectModel
Imports MigraDoc.DocumentObjectModel.Tables
Imports System.Data.SqlClient

<Serializable()> _
Public Class CFacturaePDF
    Inherits Security

    Private Const center As Integer = 0
    Private Const top As Integer = 1
    Private Const bottom As Integer = 2
    Private Const left As Integer = 3
    Private Const right As Integer = 4
    Private Const justify As Integer = 5
    Private Const prioridadFilas As Integer = 0
    Private Const prioridadColumnas As Integer = 1
    Private oTextos As DataTable
    Private sIdi As String

    ''' <summary>
    ''' Constructura del objeto. Solo llama a la constructora de su superclase.
    ''' </summary>
    ''' <param name="poDbserver"></param>
    ''' <param name="pbIsAuthenticated"></param>
    ''' <remarks>Llamado desde: CFacturaePDF.,   
    ''' Tiempo máximo:
    ''' Revisado por: auv. 28/05/2012</remarks>
    Public Sub New(ByRef poDbserver As FSNDatabaseServer.Root, ByVal pbIsAuthenticated As Boolean)
        MyBase.New(poDbserver, pbIsAuthenticated)
    End Sub
    ''' <summary>
    ''' Funcionalidad que permite dado un fichero XML de Facturae V3.2, generar un pdf que muestre visualmente la facturae.
    ''' </summary>
    ''' <param name="psNomFichXMLFac">Nombre+Path del fichero XML con al facturae v3.2</param>
    ''' <param name="psNomFichPDFFac">Nombre+Path del lugar donde se deberá dejar el fichero pdf generado</param>
    ''' <param name="psIdioma">Idioma que emplea la aplicación FS</param>
    ''' <remarks>Llamado desde: ¿?
    ''' Tiempo máximo: variable, depende del número de items, cargos, descuentos, albaranes, impuestos repercutidos e
    ''' impuestos retenidos que tenga el documento parámetro    
    ''' Revisado por: auv. 28/05/2012</remarks>
    Public Sub crearPDFFacV3_2(ByVal psNomFichXMLFac As String, ByVal psNomFichPDFFac As String, ByVal psIdioma As String)
        Dim facturae As CFacturae = Nothing
        Dim document As MigraDoc.DocumentObjectModel.Document = Nothing
        Dim pdfRenderer As PdfDocumentRenderer = Nothing
        Dim section As Section = Nothing

        Try
            Dim doc As XmlDocument = Nothing

            sIdi = psIdioma

            Dim oDict As Dictionary = New Dictionary(DBServer, mIsAuthenticated)
            oDict.LoadData(TiposDeDatos.ModulosIdiomas.FacturaPDF, sIdi)
            oTextos = oDict.Data.Tables(0)

            doc = New XmlDocument
            doc.Load(psNomFichXMLFac)
            facturae = New CFacturae(Me.mDBServer, Me.mIsAuthenticated, doc)

            document = New MigraDoc.DocumentObjectModel.Document
            If Not IsNothing(facturae) Then
                Me.escribirFactura(facturae, document)
            End If
            pdfRenderer = New PdfDocumentRenderer(False, PdfFontEmbedding.Always)

            pdfRenderer.Document = document
            pdfRenderer.RenderDocument()
            pdfRenderer.PdfDocument.Save(psNomFichPDFFac)

            facturae = Nothing
        Catch ex As Exception
            'No se pudo generar el pdf de la facturae
            document = New MigraDoc.DocumentObjectModel.Document

            section = document.AddSection()
            section.AddParagraph("ERROR WHEN BUILDING THE ISSUE VERSION 3.2: " & ex.Message & "|||||STACKTRACE =" & ex.StackTrace)

            pdfRenderer = New PdfDocumentRenderer(False, PdfFontEmbedding.Always)

            pdfRenderer.Document = document
            pdfRenderer.RenderDocument()
            pdfRenderer.PdfDocument.Save(psNomFichPDFFac)

            facturae = Nothing
        End Try
    End Sub
    ''' <summary>
    ''' Imprime la facturae contenida en el objeto CFacturae en el objeto que representa el documento PDF a crear
    ''' </summary>
    ''' <param name="facturae">Objeto CFacturae que contiene la facturae a imprimir</param>
    ''' <param name="doc">Es el objeto que representa el documento PDF a crear</param>
    ''' <remarks>Llamado desde: CFacturaePDF.crearPDFFacV3_2
    ''' Tiempo máximo: depende del tamaño del XML, del número de items, cargos, descuentos, albaranes, impuestos repercutidos e
    ''' impuestos retenidos que tenga la facturae parámetro    
    ''' Revisado por: auv. 28/05/2012</remarks>
    Private Sub escribirFactura(ByRef facturae As CFacturae, ByRef doc As MigraDoc.DocumentObjectModel.Document)
        Dim factura As InvoiceType = Nothing
        Dim par As Paragraph = Nothing
        Dim section As Section = Nothing

        If Not IsNothing(facturae.invoices) AndAlso facturae.invoices.Count > 0 Then
            factura = facturae.invoices.ElementAt(0)

            section = doc.AddSection()

            section.PageSetup.TopMargin = doc.DefaultPageSetup.TopMargin
            section.PageSetup.LeftMargin = doc.DefaultPageSetup.LeftMargin
            section.PageSetup.RightMargin = doc.DefaultPageSetup.RightMargin
            section.PageSetup.BottomMargin = doc.DefaultPageSetup.BottomMargin

            par = section.AddParagraph(UCase(CStr(oTextos(0)(1)))) : Me.ponerFormatoParrafo(par, True, 10, True, Colors.LightBlue, True, True, , , True, Underline.Single, , , True, True, True, True)
            par = section.AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, , , , , , , , , , , True, True, True, True)
            Me.escribirResumenDeFactura(factura, doc)
            If Not (IsNothing(factura.invoiceHeader)) Then
                If Not (IsNothing(factura.invoiceHeader.corrective)) Then
                    par = section.AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, , , , , , , , , , , True, True, True, True)
                    Me.escribirCorrectiva(factura, doc)
                End If
            End If
            par = section.AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, , , , , , , , , , , True, True, True, True)
            Me.escribirTotales(factura, doc)
            If Not IsNothing(factura.invoiceTotals) Then
                If Not (IsNothing(factura.invoiceTotals.generalSurcharges)) Then
                    par = section.AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, , , , , , , , , , , True, True, True, True)
                    Me.escribirCargosGenerales(factura, doc)
                End If
                If Not (IsNothing(factura.invoiceTotals.generalDiscounts)) Then
                    par = section.AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, , , , , , , , , , , True, True, True, True)
                    Me.escribirDescuentosGenerales(factura, doc)
                End If
            End If
            If Not (IsNothing(factura.taxesOutputs)) Then
                par = section.AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, , , , , , , , , , , True, True, True, True)
                Me.escribirImpuestosRepercutidos(factura, doc)
            End If
            If Not (IsNothing(factura.taxesWithheld)) Then
                par = section.AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, , , , , , , , , , , True, True, True, True)
                Me.escribirImpuestosRetenidos(factura, doc)
            End If
            If Not (IsNothing(factura.invoiceTotals)) Then
                If Not (IsNothing(factura.invoiceTotals.amountsWithheld)) Then
                    par = section.AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, , , , , , , , , , , True, True, True, True)
                    Me.escribirRetencionesDeGarantia(factura, doc)
                End If
            End If
            If Not (IsNothing(factura.additionalData)) Then
                par = section.AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, , , , , , , , , , , True, True, True, True)
                Me.escribirDatosAdicionales(factura, doc)
            End If
            par = section.AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, , , , , , , , , , , True, True, True, True)
            Me.escribirEmisor(facturae, doc)
            par = section.AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, , , , , , , , , , , True, True, True, True)
            Me.escribirReceptor(facturae, doc)
            par = section.AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, , , , , , , , , , , True, True, True, True)
            Me.escribirAlbaranesDeFactura(factura, doc)
            par = section.AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, , , , , , , , , , , True, True, True, True)
            Me.escribirDetallesDeFactura(factura, doc)
        End If
    End Sub
    ''' <summary>
    ''' Imprime parte de la facturae contenida en el objeto CFacturae en el objeto que representa el documento PDF a crear.
    ''' Concretamente imprime la parte del vendedor.
    ''' </summary>
    ''' <param name="facturae">Objeto CFacturae que contiene la facturae a imprimir</param>
    ''' <param name="doc">Es el objeto que representa el documento PDF a crear</param>
    ''' <remarks>Llamado desde: CFacturaePDF.escribirFactura 
    ''' Tiempo máximo: menor a 0,1
    ''' Revisado por: auv. 28/05/2012</remarks>
    Private Sub escribirEmisor(ByRef facturae As CFacturae, ByRef doc As MigraDoc.DocumentObjectModel.Document)
        Dim par As Paragraph = Nothing
        Dim section As Section = Nothing
        Dim le As LegalEntityType = Nothing

        section = doc.LastSection

        par = section.AddParagraph(UCase(CStr(oTextos(1)(1)))) : Me.ponerFormatoParrafo(par, True, 10, True, Colors.LightBlue, True, True, , , True, Underline.Single, , , True, True, True, True)
        par = section.AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, , , , , , , , , , , True, True, True, True)
        Me.escribirEmisorTablaInformacionGeneral(facturae, doc)
        If Not IsNothing(facturae.parties) AndAlso Not IsNothing(facturae.parties.sellerParty) AndAlso _
            Not IsNothing(facturae.parties.sellerParty.legalEntity) AndAlso Not IsNothing(facturae.parties.sellerParty.legalEntity.contactDetails) Then
            par = section.AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, , , , , , , , , , , True, True, True, True)
            Me.escribirEmisorTablaDatosDeContacto(facturae, doc)
        End If
    End Sub
    ''' <summary>
    ''' Imprime parte de la facturae contenida en el objeto CFacturae en el objeto que representa el documento PDF a crear.
    ''' Concretamente imprime el nombre, los datos de identificación fiscal y la dirección del vendedor.
    ''' </summary>
    ''' <param name="facturae">Objeto CFacturae que contiene la facturae a imprimir</param>
    ''' <param name="doc">Es el objeto que representa el documento PDF a crear</param>
    ''' <remarks>Llamado desde: CFacturaePDF.escribirEmisor
    ''' Tiempo máximo: menor a 0,1  
    ''' Revisado por: auv. 28/05/2012</remarks>
    Private Sub escribirEmisorTablaInformacionGeneral(ByRef facturae As CFacturae, ByRef doc As MigraDoc.DocumentObjectModel.Document)
        Authenticate()
        Dim obj_DatosBD As DataTable = Nothing
        Dim par As Paragraph = Nothing
        Dim section As Section = Nothing
        Dim tab As Table = Nothing
        Dim anchCol As Double() = Nothing
        Dim colAux As Integer = Nothing
        Dim nFil As Integer = Nothing

        section = doc.LastSection

        par = section.AddParagraph(CStr(oTextos(2)(1))) : Me.ponerFormatoParrafo(par, True, 8, True, Colors.LightBlue, , , True, True, , , , , True, True, True, True)
        par = section.AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, , , , , , , , , , , True, True, True, True)

        ReDim Preserve anchCol(0 To 15)
        anchCol(0) = 1 : anchCol(1) = 1 : anchCol(2) = 1 : anchCol(3) = 1 : anchCol(4) = 1 : anchCol(5) = 1 : anchCol(6) = 1 : anchCol(7) = 1
        anchCol(8) = 1 : anchCol(9) = 1 : anchCol(10) = 1 : anchCol(11) = 1 : anchCol(12) = 1 : anchCol(13) = 1 : anchCol(14) = 1 : anchCol(15) = 1

        nFil = 12

        tab = Me.constTab(section, anchCol, nFil, Colors.White)

        colAux = IIf(UBound(anchCol) + 1 < tab.Columns.Count, 1, 0)

        Me.alinHorContTab(tab, CFacturaePDF.center) : Me.alinVerContTab(tab, CFacturaePDF.center)

        par = tab.Rows(0).Cells(0 + colAux).AddParagraph(UCase(CStr(oTextos(3)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        If Not IsNothing(facturae.parties) AndAlso Not IsNothing(facturae.parties.sellerParty) AndAlso _
            Not IsNothing(facturae.parties.sellerParty.legalEntity) AndAlso Not IsNothing(facturae.parties.sellerParty.legalEntity.corporateName) Then
            par = tab.Rows(1).Cells(0 + colAux).AddParagraph(facturae.parties.sellerParty.legalEntity.corporateName) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
        Else
            par = tab.Rows(1).Cells(0 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
        End If
        par = tab.Rows(2).Cells(0 + colAux).AddParagraph(UCase(CStr(oTextos(4)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        If Not IsNothing(facturae.parties) AndAlso Not IsNothing(facturae.parties.sellerParty) AndAlso _
            Not IsNothing(facturae.parties.sellerParty.taxIdentification) AndAlso Not IsNothing(facturae.parties.sellerParty.taxIdentification.personTypeCode) Then
            par = tab.Rows(3).Cells(0 + colAux).AddParagraph(personTypeCode_Obj2PDF(facturae.parties.sellerParty.taxIdentification.personTypeCode.value)) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
        Else
            par = tab.Rows(3).Cells(0 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
        End If
        par = tab.Rows(2).Cells(6 + colAux).AddParagraph(UCase(CStr(oTextos(5)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        If Not IsNothing(facturae.parties) AndAlso Not IsNothing(facturae.parties.sellerParty) AndAlso _
            Not IsNothing(facturae.parties.sellerParty.taxIdentification) AndAlso Not IsNothing(facturae.parties.sellerParty.taxIdentification.taxIdentificationNumber) Then
            par = tab.Rows(3).Cells(6 + colAux).AddParagraph(facturae.parties.sellerParty.taxIdentification.taxIdentificationNumber) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
        Else
            par = tab.Rows(3).Cells(6 + colAux).AddParagraph(facturae.parties.sellerParty.taxIdentification.taxIdentificationNumber) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
        End If

        par = tab.Rows(4).Cells(0 + colAux).AddParagraph(UCase(CStr(oTextos(6)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        If Not IsNothing(facturae.parties) AndAlso Not IsNothing(facturae.parties.sellerParty) AndAlso _
            Not IsNothing(facturae.parties.sellerParty.taxIdentification) AndAlso Not IsNothing(facturae.parties.sellerParty.taxIdentification.residenceTypeCode) Then
            par = tab.Rows(5).Cells(0 + colAux).AddParagraph(residenceTypeCode_Obj2PDF(facturae.parties.sellerParty.taxIdentification.residenceTypeCode.value)) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
        Else
            par = tab.Rows(5).Cells(0 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
        End If

        par = tab.Rows(6).Cells(0 + colAux).AddParagraph(UCase(CStr(oTextos(7)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        par = tab.Rows(10).Cells(0 + colAux).AddParagraph(UCase(CStr(oTextos(8)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        par = tab.Rows(10).Cells(6 + colAux).AddParagraph(UCase(CStr(oTextos(9)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        If Not (IsNothing(facturae.parties.sellerParty.legalEntity.addressInSpain)) Then
            If Not IsNothing(facturae.parties) AndAlso Not IsNothing(facturae.parties.sellerParty) AndAlso _
                Not IsNothing(facturae.parties.sellerParty.legalEntity) AndAlso Not IsNothing(facturae.parties.sellerParty.legalEntity.addressInSpain) AndAlso _
                Not IsNothing(facturae.parties.sellerParty.legalEntity.addressInSpain.address) Then
                par = tab.Rows(7).Cells(0 + colAux).AddParagraph(facturae.parties.sellerParty.legalEntity.addressInSpain.address) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
            Else
                par = tab.Rows(7).Cells(0 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
            End If
            par = tab.Rows(8).Cells(0 + colAux).AddParagraph(UCase(CStr(oTextos(10)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
            par = tab.Rows(8).Cells(4 + colAux).AddParagraph(UCase(CStr(oTextos(11)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
            If Not IsNothing(facturae.parties) AndAlso Not IsNothing(facturae.parties.sellerParty) AndAlso _
                Not IsNothing(facturae.parties.sellerParty.legalEntity) AndAlso Not IsNothing(facturae.parties.sellerParty.legalEntity.addressInSpain) _
                AndAlso Not IsNothing(facturae.parties.sellerParty.legalEntity.addressInSpain.postCode) Then
                par = tab.Rows(9).Cells(0 + colAux).AddParagraph(facturae.parties.sellerParty.legalEntity.addressInSpain.postCode.value) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
            Else
                par = tab.Rows(9).Cells(0 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
            End If
            If Not IsNothing(facturae.parties) AndAlso Not IsNothing(facturae.parties.sellerParty) AndAlso _
                Not IsNothing(facturae.parties.sellerParty.legalEntity) AndAlso Not IsNothing(facturae.parties.sellerParty.legalEntity.addressInSpain) _
                AndAlso Not IsNothing(facturae.parties.sellerParty.legalEntity.addressInSpain.town) Then
                par = tab.Rows(9).Cells(4 + colAux).AddParagraph(facturae.parties.sellerParty.legalEntity.addressInSpain.town) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
            Else
                par = tab.Rows(9).Cells(4 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
            End If
            If Not IsNothing(facturae.parties) AndAlso Not IsNothing(facturae.parties.sellerParty) AndAlso _
                Not IsNothing(facturae.parties.sellerParty.legalEntity) AndAlso Not IsNothing(facturae.parties.sellerParty.legalEntity.addressInSpain) _
                AndAlso Not IsNothing(facturae.parties.sellerParty.legalEntity.addressInSpain.province) Then
                par = tab.Rows(11).Cells(0 + colAux).AddParagraph(facturae.parties.sellerParty.legalEntity.addressInSpain.province) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
            Else
                par = tab.Rows(11).Cells(0 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
            End If
            If Not IsNothing(facturae.parties) AndAlso Not IsNothing(facturae.parties.sellerParty) AndAlso _
                Not IsNothing(facturae.parties.sellerParty.legalEntity) AndAlso Not IsNothing(facturae.parties.sellerParty.legalEntity.addressInSpain) _
                AndAlso Not IsNothing(facturae.parties.sellerParty.legalEntity.addressInSpain.countryCode) Then
                obj_DatosBD = DBServer.obtNombrePais(facturae.parties.sellerParty.legalEntity.addressInSpain.countryCode.value, sIdi)
            Else
                obj_DatosBD = Nothing
            End If
            If Not IsNothing(obj_DatosBD) AndAlso obj_DatosBD.Rows.Count > 0 Then
                If Not IsNothing(obj_DatosBD.Rows(0)("DEN")) Then
                    par = tab.Rows(11).Cells(6 + colAux).AddParagraph(CStr(obj_DatosBD.Rows(0)("DEN"))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                Else
                    par = tab.Rows(11).Cells(6 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                End If
            Else
                par = tab.Rows(11).Cells(6 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
            End If
        End If
        If Not (IsNothing(facturae.parties.sellerParty.legalEntity.overseasAddress)) Then
            If Not IsNothing(facturae.parties) AndAlso Not IsNothing(facturae.parties.sellerParty) AndAlso _
                Not IsNothing(facturae.parties.sellerParty.legalEntity) AndAlso Not IsNothing(facturae.parties.sellerParty.legalEntity.overseasAddress) _
                AndAlso Not IsNothing(facturae.parties.sellerParty.legalEntity.overseasAddress.address) Then
                par = tab.Rows(7).Cells(0 + colAux).AddParagraph(facturae.parties.sellerParty.legalEntity.overseasAddress.address) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
            Else
                par = tab.Rows(7).Cells(0 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
            End If
            par = tab.Rows(8).Cells(0 + colAux).AddParagraph(UCase(CStr(oTextos(12)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
            If Not IsNothing(facturae.parties) AndAlso Not IsNothing(facturae.parties.sellerParty) AndAlso _
                Not IsNothing(facturae.parties.sellerParty.legalEntity) AndAlso Not IsNothing(facturae.parties.sellerParty.legalEntity.overseasAddress) _
                AndAlso Not IsNothing(facturae.parties.sellerParty.legalEntity.overseasAddress.postCodeAndTown) Then
                par = tab.Rows(9).Cells(0 + colAux).AddParagraph(facturae.parties.sellerParty.legalEntity.overseasAddress.postCodeAndTown) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
            Else
                par = tab.Rows(9).Cells(0 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
            End If
            If Not IsNothing(facturae.parties) AndAlso Not IsNothing(facturae.parties.sellerParty) AndAlso _
                Not IsNothing(facturae.parties.sellerParty.legalEntity) AndAlso Not IsNothing(facturae.parties.sellerParty.legalEntity.overseasAddress) _
                AndAlso Not IsNothing(facturae.parties.sellerParty.legalEntity.overseasAddress.province) Then
                par = tab.Rows(11).Cells(0 + colAux).AddParagraph(facturae.parties.sellerParty.legalEntity.overseasAddress.province) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
            Else
                par = tab.Rows(11).Cells(0 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
            End If
            If Not IsNothing(facturae.parties) AndAlso Not IsNothing(facturae.parties.sellerParty) AndAlso _
                Not IsNothing(facturae.parties.sellerParty.legalEntity) AndAlso Not IsNothing(facturae.parties.sellerParty.legalEntity.overseasAddress) _
                AndAlso Not IsNothing(facturae.parties.sellerParty.legalEntity.overseasAddress.countryCode) Then
                obj_DatosBD = DBServer.obtNombrePais(facturae.parties.sellerParty.legalEntity.overseasAddress.countryCode.value, sIdi)
            Else
                obj_DatosBD = Nothing
            End If
            If Not IsNothing(obj_DatosBD) AndAlso obj_DatosBD.Rows.Count > 0 Then
                If Not IsNothing(obj_DatosBD.Rows(0)("DEN")) Then
                    par = tab.Rows(11).Cells(6 + colAux).AddParagraph(CStr(obj_DatosBD.Rows(0)("DEN"))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                Else
                    par = tab.Rows(11).Cells(6 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                End If
            Else
                par = tab.Rows(11).Cells(6 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
            End If
        End If

        tab.Rows(0).KeepWith = tab.Rows.Count - 1
        Me.bordeColumnas(tab, Colors.LightBlue, colAux = 1)
        Me.bordeFilas(tab, Colors.LightBlue, colAux = 1)
        Me.autoUnirCeldasDeTabla(tab, 0, colAux, CFacturaePDF.prioridadFilas, CFacturaePDF.prioridadColumnas)
    End Sub
    ''' <summary>
    ''' Imprime parte de la facturae contenida en el objeto CFacturae en el objeto que representa el documento PDF a crear.
    ''' Concretamente imprime los datos de contacto del vendedor
    ''' </summary>
    ''' <param name="facturae">Objeto CFacturae que contiene la facturae a imprimir</param>
    ''' <param name="doc">Es el objeto que representa el documento PDF a crear</param>
    ''' <remarks>Llamado desde: CFacturaePDF.escribirEmisor
    ''' Tiempo máximo: menor a 0,1
    ''' Revisado por: auv. 28/05/2012</remarks>
    Private Sub escribirEmisorTablaDatosDeContacto(ByRef facturae As CFacturae, ByRef doc As MigraDoc.DocumentObjectModel.Document)
        Dim par As Paragraph = Nothing
        Dim section As Section = Nothing
        Dim tab As Table = Nothing
        Dim anchCol As Double() = Nothing
        Dim colAux As Integer = Nothing
        Dim nFil As Integer = Nothing

        section = doc.LastSection

        par = section.AddParagraph(CStr(oTextos(13)(1))) : Me.ponerFormatoParrafo(par, True, 8, True, Colors.LightBlue, , , True, True, , , , , True, True, True, True)
        par = section.AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, , , , , , , , , , , True, True, True, True)

        ReDim Preserve anchCol(0 To 15)
        anchCol(0) = 1 : anchCol(1) = 1 : anchCol(2) = 1 : anchCol(3) = 1 : anchCol(4) = 1 : anchCol(5) = 1 : anchCol(6) = 1 : anchCol(7) = 1
        anchCol(8) = 1 : anchCol(9) = 1 : anchCol(10) = 1 : anchCol(11) = 1 : anchCol(12) = 1 : anchCol(13) = 1 : anchCol(14) = 1 : anchCol(15) = 1

        'NÚMERO DE FILAS MÍNIMO QUE TENDRÁ LA TABLA
        nFil = 6

        tab = Me.constTab(section, anchCol, nFil, Colors.White)

        colAux = IIf(UBound(anchCol) + 1 < tab.Columns.Count, 1, 0)

        Me.alinHorContTab(tab, CFacturaePDF.center) : Me.alinVerContTab(tab, CFacturaePDF.center)

        par = tab.Rows(0).Cells(0 + colAux).AddParagraph(UCase(CStr(oTextos(14)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        If Not (IsNothing(facturae.parties.sellerParty.legalEntity.contactDetails.telephone)) Then
            par = tab.Rows(1).Cells(0 + colAux).AddParagraph(facturae.parties.sellerParty.legalEntity.contactDetails.telephone) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
        Else
            par = tab.Rows(1).Cells(0 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
        End If
        par = tab.Rows(0).Cells(4 + colAux).AddParagraph(UCase(CStr(oTextos(15)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        If Not (IsNothing(facturae.parties.sellerParty.legalEntity.contactDetails.teleFax)) Then
            par = tab.Rows(1).Cells(4 + colAux).AddParagraph(facturae.parties.sellerParty.legalEntity.contactDetails.teleFax) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
        Else
            par = tab.Rows(1).Cells(4 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
        End If
        par = tab.Rows(2).Cells(0 + colAux).AddParagraph(UCase(CStr(oTextos(16)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        If Not (IsNothing(facturae.parties.sellerParty.legalEntity.contactDetails.electronicMail)) Then
            par = tab.Rows(3).Cells(0 + colAux).AddParagraph(facturae.parties.sellerParty.legalEntity.contactDetails.electronicMail) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
        Else
            par = tab.Rows(3).Cells(0 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
        End If
        par = tab.Rows(4).Cells(0 + colAux).AddParagraph(UCase(CStr(oTextos(17)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        If Not (IsNothing(facturae.parties.sellerParty.legalEntity.contactDetails.contactPersons)) Then
            par = tab.Rows(5).Cells(0 + colAux).AddParagraph(facturae.parties.sellerParty.legalEntity.contactDetails.contactPersons) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
        Else
            par = tab.Rows(5).Cells(0 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
        End If

        tab.Rows(0).KeepWith = tab.Rows.Count - 1
        Me.bordeColumnas(tab, Colors.LightBlue, colAux = 1)
        Me.bordeFilas(tab, Colors.LightBlue, colAux = 1)
        Me.autoUnirCeldasDeTabla(tab, 0, colAux, CFacturaePDF.prioridadFilas, CFacturaePDF.prioridadColumnas)
    End Sub
    ''' <summary>
    ''' Imprime parte de la facturae contenida en el objeto CFacturae en el objeto que representa el documento PDF a crear.
    ''' Concretamente imprime la parte del comprador.
    ''' </summary>
    ''' <param name="facturae">Objeto CFacturae que contiene la facturae a imprimir</param>
    ''' <param name="doc">Es el objeto que representa el documento PDF a crear</param>
    ''' <remarks>Llamado desde: CFacturaePDF.escribirFactura
    ''' Tiempo máximo: menor a 0,1    
    ''' Revisado por: auv. 28/05/2012</remarks>
    Private Sub escribirReceptor(ByRef facturae As CFacturae, ByRef doc As MigraDoc.DocumentObjectModel.Document)
        Authenticate()
        Dim obj_DatosBD As DataTable = Nothing
        Dim par As Paragraph = Nothing
        Dim section As Section = Nothing
        Dim tab As Table = Nothing
        Dim anchCol As Double() = Nothing
        Dim colAux As Integer = Nothing
        Dim nFil As Integer = Nothing

        section = doc.LastSection

        par = section.AddParagraph(UCase(CStr(oTextos(18)(1)))) : Me.ponerFormatoParrafo(par, True, 10, True, Colors.LightBlue, True, True, , , True, Underline.Single, , , True, True, True, True)
        par = section.AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, , , , , , , , , , , True, True, True, True)
        par = section.AddParagraph(CStr(oTextos(2)(1))) : Me.ponerFormatoParrafo(par, True, 8, True, Colors.LightBlue, , , True, True, , , , , True, True, True, True)
        par = section.AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, , , , , , , , , , , True, True, True, True)

        ReDim Preserve anchCol(0 To 15)
        anchCol(0) = 1 : anchCol(1) = 1 : anchCol(2) = 1 : anchCol(3) = 1 : anchCol(4) = 1 : anchCol(5) = 1 : anchCol(6) = 1 : anchCol(7) = 1
        anchCol(8) = 1 : anchCol(9) = 1 : anchCol(10) = 1 : anchCol(11) = 1 : anchCol(12) = 1 : anchCol(13) = 1 : anchCol(14) = 1 : anchCol(15) = 1

        nFil = 12

        tab = Me.constTab(section, anchCol, nFil, Colors.White)

        colAux = IIf(UBound(anchCol) + 1 < tab.Columns.Count, 1, 0)

        Me.alinHorContTab(tab, CFacturaePDF.center) : Me.alinVerContTab(tab, CFacturaePDF.center)

        par = tab.Rows(0).Cells(0 + colAux).AddParagraph(UCase(CStr(oTextos(3)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        If Not IsNothing(facturae.parties) AndAlso Not IsNothing(facturae.parties.buyerParty) AndAlso _
           Not IsNothing(facturae.parties.buyerParty.legalEntity) AndAlso Not IsNothing(facturae.parties.buyerParty.legalEntity.corporateName) Then
            par = tab.Rows(1).Cells(0 + colAux).AddParagraph(facturae.parties.buyerParty.legalEntity.corporateName) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
        Else
            par = tab.Rows(1).Cells(0 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
        End If
        par = tab.Rows(2).Cells(0 + colAux).AddParagraph(UCase(CStr(oTextos(4)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        If Not IsNothing(facturae.parties) AndAlso Not IsNothing(facturae.parties.buyerParty) AndAlso _
           Not IsNothing(facturae.parties.buyerParty.taxIdentification) AndAlso Not IsNothing(facturae.parties.buyerParty.taxIdentification.personTypeCode) Then
            par = tab.Rows(3).Cells(0 + colAux).AddParagraph(personTypeCode_Obj2PDF(facturae.parties.buyerParty.taxIdentification.personTypeCode.value)) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
        Else
            par = tab.Rows(3).Cells(0 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
        End If
        par = tab.Rows(2).Cells(6 + colAux).AddParagraph(UCase(CStr(oTextos(5)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        If Not IsNothing(facturae.parties) AndAlso Not IsNothing(facturae.parties.buyerParty) AndAlso _
           Not IsNothing(facturae.parties.buyerParty.taxIdentification) AndAlso Not IsNothing(facturae.parties.buyerParty.taxIdentification.taxIdentificationNumber) Then
            par = tab.Rows(3).Cells(6 + colAux).AddParagraph(facturae.parties.buyerParty.taxIdentification.taxIdentificationNumber) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
        Else
            par = tab.Rows(3).Cells(6 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
        End If
        par = tab.Rows(4).Cells(0 + colAux).AddParagraph(UCase(CStr(oTextos(6)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        If Not IsNothing(facturae.parties) AndAlso Not IsNothing(facturae.parties.buyerParty) AndAlso _
           Not IsNothing(facturae.parties.buyerParty.taxIdentification) AndAlso Not IsNothing(facturae.parties.buyerParty.taxIdentification.residenceTypeCode) Then
            par = tab.Rows(5).Cells(0 + colAux).AddParagraph(residenceTypeCode_Obj2PDF(facturae.parties.buyerParty.taxIdentification.residenceTypeCode.value)) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
        Else
            par = tab.Rows(5).Cells(0 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
        End If
        par = tab.Rows(6).Cells(0 + colAux).AddParagraph(UCase(CStr(oTextos(7)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        par = tab.Rows(10).Cells(0 + colAux).AddParagraph(UCase(CStr(oTextos(8)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        par = tab.Rows(10).Cells(6 + colAux).AddParagraph(UCase(CStr(oTextos(9)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        If Not (IsNothing(facturae.parties.buyerParty.legalEntity.addressInSpain)) Then
            If Not IsNothing(facturae.parties) AndAlso Not IsNothing(facturae.parties.buyerParty) AndAlso _
               Not IsNothing(facturae.parties.buyerParty.legalEntity) AndAlso Not IsNothing(facturae.parties.buyerParty.legalEntity.addressInSpain) _
               AndAlso Not IsNothing(facturae.parties.buyerParty.legalEntity.addressInSpain.address) Then
                par = tab.Rows(7).Cells(0 + colAux).AddParagraph(facturae.parties.buyerParty.legalEntity.addressInSpain.address) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
            Else
                par = tab.Rows(7).Cells(0 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
            End If
            par = tab.Rows(8).Cells(0 + colAux).AddParagraph(UCase(CStr(oTextos(10)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
            par = tab.Rows(8).Cells(4 + colAux).AddParagraph(UCase(CStr(oTextos(11)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
            If Not IsNothing(facturae.parties) AndAlso Not IsNothing(facturae.parties.buyerParty) AndAlso _
               Not IsNothing(facturae.parties.buyerParty.legalEntity) AndAlso Not IsNothing(facturae.parties.buyerParty.legalEntity.addressInSpain) AndAlso _
               Not IsNothing(facturae.parties.buyerParty.legalEntity.addressInSpain.postCode) Then
                par = tab.Rows(9).Cells(0 + colAux).AddParagraph(facturae.parties.buyerParty.legalEntity.addressInSpain.postCode.value) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
            Else
                par = tab.Rows(9).Cells(0 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
            End If
            If Not IsNothing(facturae.parties) AndAlso Not IsNothing(facturae.parties.buyerParty) AndAlso _
               Not IsNothing(facturae.parties.buyerParty.legalEntity) AndAlso Not IsNothing(facturae.parties.buyerParty.legalEntity.addressInSpain) AndAlso _
               Not IsNothing(facturae.parties.buyerParty.legalEntity.addressInSpain.town) Then
                par = tab.Rows(9).Cells(4 + colAux).AddParagraph(facturae.parties.buyerParty.legalEntity.addressInSpain.town) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
            Else
                par = tab.Rows(9).Cells(4 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
            End If
            If Not IsNothing(facturae.parties) AndAlso Not IsNothing(facturae.parties.buyerParty) AndAlso _
               Not IsNothing(facturae.parties.buyerParty.legalEntity) AndAlso Not IsNothing(facturae.parties.buyerParty.legalEntity.addressInSpain) AndAlso _
               Not IsNothing(facturae.parties.buyerParty.legalEntity.addressInSpain.province) Then
                par = tab.Rows(11).Cells(0 + colAux).AddParagraph(facturae.parties.buyerParty.legalEntity.addressInSpain.province) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
            Else
                par = tab.Rows(11).Cells(0 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
            End If
            If Not IsNothing(facturae.parties) AndAlso Not IsNothing(facturae.parties.buyerParty) AndAlso _
               Not IsNothing(facturae.parties.buyerParty.legalEntity) AndAlso Not IsNothing(facturae.parties.buyerParty.legalEntity.addressInSpain) AndAlso _
               Not IsNothing(facturae.parties.buyerParty.legalEntity.addressInSpain.countryCode) Then
                obj_DatosBD = DBServer.obtNombrePais(facturae.parties.buyerParty.legalEntity.addressInSpain.countryCode.value, sIdi)
            Else
                obj_DatosBD = Nothing
            End If
            If Not IsNothing(obj_DatosBD) AndAlso obj_DatosBD.Rows.Count > 0 Then
                If Not IsNothing(obj_DatosBD.Rows(0)("DEN")) Then
                    par = tab.Rows(11).Cells(6 + colAux).AddParagraph(CStr(obj_DatosBD.Rows(0)("DEN"))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                Else
                    par = tab.Rows(11).Cells(6 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                End If
            Else
                par = tab.Rows(11).Cells(6 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
            End If
        End If
        If Not (IsNothing(facturae.parties.buyerParty.legalEntity.overseasAddress)) Then
            If Not IsNothing(facturae.parties) AndAlso Not IsNothing(facturae.parties.buyerParty) AndAlso _
               Not IsNothing(facturae.parties.buyerParty.legalEntity) AndAlso Not IsNothing(facturae.parties.buyerParty.legalEntity.overseasAddress) AndAlso _
               Not IsNothing(facturae.parties.buyerParty.legalEntity.overseasAddress.address) Then
                par = tab.Rows(7).Cells(0 + colAux).AddParagraph(facturae.parties.buyerParty.legalEntity.overseasAddress.address) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
            Else
                par = tab.Rows(7).Cells(0 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
            End If
            par = tab.Rows(8).Cells(0 + colAux).AddParagraph(UCase(CStr(oTextos(12)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
            If Not IsNothing(facturae.parties) AndAlso Not IsNothing(facturae.parties.buyerParty) AndAlso _
               Not IsNothing(facturae.parties.buyerParty.legalEntity) AndAlso Not IsNothing(facturae.parties.buyerParty.legalEntity.overseasAddress) AndAlso _
               Not IsNothing(facturae.parties.buyerParty.legalEntity.overseasAddress.postCodeAndTown) Then
                par = tab.Rows(9).Cells(0 + colAux).AddParagraph(facturae.parties.buyerParty.legalEntity.overseasAddress.postCodeAndTown) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
            Else
                par = tab.Rows(9).Cells(0 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
            End If
            If Not IsNothing(facturae.parties) AndAlso Not IsNothing(facturae.parties.buyerParty) AndAlso _
               Not IsNothing(facturae.parties.buyerParty.legalEntity) AndAlso Not IsNothing(facturae.parties.buyerParty.legalEntity.overseasAddress) AndAlso _
               Not IsNothing(facturae.parties.buyerParty.legalEntity.overseasAddress.province) Then
                par = tab.Rows(11).Cells(0 + colAux).AddParagraph(facturae.parties.buyerParty.legalEntity.overseasAddress.province) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
            Else
                par = tab.Rows(11).Cells(0 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
            End If
            If Not IsNothing(facturae.parties) AndAlso Not IsNothing(facturae.parties.buyerParty) AndAlso _
               Not IsNothing(facturae.parties.buyerParty.legalEntity) AndAlso Not IsNothing(facturae.parties.buyerParty.legalEntity.overseasAddress) AndAlso _
               Not IsNothing(facturae.parties.buyerParty.legalEntity.overseasAddress.countryCode) Then
                obj_DatosBD = DBServer.obtNombrePais(facturae.parties.buyerParty.legalEntity.overseasAddress.countryCode.value, sIdi)
            Else
                obj_DatosBD = Nothing
            End If
            If Not IsNothing(obj_DatosBD) AndAlso obj_DatosBD.Rows.Count > 0 Then
                If Not IsNothing(obj_DatosBD.Rows(0)("DEN")) Then
                    par = tab.Rows(11).Cells(6 + colAux).AddParagraph(CStr(obj_DatosBD.Rows(0)("DEN"))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                Else
                    par = tab.Rows(11).Cells(6 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                End If
            Else
                par = tab.Rows(11).Cells(6 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
            End If
        End If

        tab.Rows(0).KeepWith = tab.Rows.Count - 1
        Me.bordeColumnas(tab, Colors.LightBlue, colAux = 1)
        Me.bordeFilas(tab, Colors.LightBlue, colAux = 1)
        Me.autoUnirCeldasDeTabla(tab, 0, colAux, CFacturaePDF.prioridadFilas, CFacturaePDF.prioridadColumnas)
    End Sub
    ''' <summary>
    ''' Imprime parte de la facturae contenida en el objeto CFacturae en el objeto que representa el documento PDF a crear.
    ''' Concretamente imprime el número de la factura, la fecha de emisión y la moneda de facturación, para la factura parámetro.
    ''' </summary>
    ''' <param name="factura">Un objeto que representa una factura de las contenidas en una facturae</param>
    ''' <param name="doc">Es el objeto que representa el documento PDF a crear</param>
    ''' <remarks>Llamado desde: CFacturaePDF.escribirFactura
    ''' Tiempo máximo: menor a 0,1   
    ''' Revisado por: auv. 28/05/2012</remarks>
    Private Sub escribirResumenDeFactura(ByRef factura As InvoiceType, ByRef doc As MigraDoc.DocumentObjectModel.Document)
        Authenticate()
        Dim obj_DatosBD As DataTable = Nothing
        'DECLARACIÓN DE LAS VARIABLES LOCALES
        Dim par As Paragraph = Nothing
        Dim section As Section = Nothing
        Dim tab As Table = Nothing
        Dim anchCol As Double() = Nothing
        Dim colAux As Integer = Nothing
        Dim nFil As Integer = Nothing

        'OBTENER LA ÚLTIMA SECCIÓN EN LA CUAL SE ESCRIBIRÁ
        section = doc.LastSection

        'SE DECIDE EL NÚMERO DE COLUMNAS Y SU ANCHO
        ReDim Preserve anchCol(0 To 15)
        anchCol(0) = 1 : anchCol(1) = 1 : anchCol(2) = 1 : anchCol(3) = 1 : anchCol(4) = 1 : anchCol(5) = 1 : anchCol(6) = 1 : anchCol(7) = 1
        anchCol(8) = 1 : anchCol(9) = 1 : anchCol(10) = 1 : anchCol(11) = 1 : anchCol(12) = 1 : anchCol(13) = 1 : anchCol(14) = 1 : anchCol(15) = 1

        'NÚMERO DE FILAS MÍNIMO QUE TENDRÁ LA TABLA
        nFil = 3

        'SE EXAMINA QUÉ DATOS NO OBLIGATORIOS ESTÁN PRESENTES

        par = section.AddParagraph(CStr(oTextos(19)(1))) : Me.ponerFormatoParrafo(par, True, 8, True, Colors.LightBlue, , , True, True, , , , , True, True, True, True)
        par = section.AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, , , , , , , , , , , True, True, True, True)

        tab = Me.constTab(section, anchCol, nFil, Colors.White)

        colAux = IIf(UBound(anchCol) + 1 < tab.Columns.Count, 1, 0)

        Me.alinHorContTab(tab, CFacturaePDF.center) : Me.alinVerContTab(tab, CFacturaePDF.center)

        par = tab.Rows(0).Cells(0 + colAux).AddParagraph(UCase(CStr(oTextos(20)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        par = tab.Rows(0).Cells(8 + colAux).AddParagraph(factura.invoiceHeader.invoiceNumber) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
        par = tab.Rows(1).Cells(0 + colAux).AddParagraph(UCase(CStr(oTextos(21)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        par = tab.Rows(1).Cells(8 + colAux).AddParagraph(factura.invoiceIssueData.issueDate.ToString) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
        par = tab.Rows(2).Cells(0 + colAux).AddParagraph(UCase(CStr(oTextos(22)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )

        If Not IsNothing(factura.invoiceIssueData) AndAlso Not IsNothing(factura.invoiceIssueData.invoiceCurrencyCode) Then
            obj_DatosBD = DBServer.obtNombreMoneda(factura.invoiceIssueData.invoiceCurrencyCode.value, sIdi)
        End If
        If Not IsNothing(obj_DatosBD) AndAlso obj_DatosBD.Rows.Count > 0 Then
            If Not IsNothing(obj_DatosBD.Rows(0)("DEN")) Then
                par = tab.Rows(2).Cells(8 + colAux).AddParagraph(CStr(obj_DatosBD.Rows(0)("DEN"))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
            Else
                par = tab.Rows(2).Cells(8 + colAux).AddParagraph(CStr("")) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
            End If
        Else
            par = tab.Rows(2).Cells(8 + colAux).AddParagraph(CStr("")) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
        End If

        tab.Rows(0).KeepWith = tab.Rows.Count - 1
        Me.bordeColumnas(tab, Colors.LightBlue, colAux = 1)
        Me.bordeFilas(tab, Colors.LightBlue, colAux = 1)
        Me.autoUnirCeldasDeTabla(tab, 0, colAux, CFacturaePDF.prioridadFilas, CFacturaePDF.prioridadColumnas)
    End Sub
    ''' <summary>
    ''' Imprime parte de la facturae contenida en el objeto CFacturae en el objeto que representa el documento PDF a crear.
    ''' Concretamente imprime los datos de la rectificativa de la factura, si es que se trata de una factura rectificativa, la factura parámetro.
    ''' </summary>
    ''' <param name="factura">Un objeto que representa una factura de las contenidas en una facturae</param>
    ''' <param name="doc">Es el objeto que representa el documento PDF a crear</param>
    ''' <remarks>Llamado desde: CFacturaePDF.escribirFactura
    ''' Tiempo máximo: menor a 0,1 
    ''' Revisado por: auv. 28/05/2012</remarks>
    Private Sub escribirCorrectiva(ByRef factura As InvoiceType, ByRef doc As MigraDoc.DocumentObjectModel.Document)
        'DECLARACIÓN DE LAS VARIABLES LOCALES
        Dim par As Paragraph = Nothing
        Dim section As Section = Nothing
        Dim tab As Table = Nothing
        Dim anchCol As Double() = Nothing
        Dim colAux As Integer = Nothing
        Dim nFil As Integer = Nothing

        'OBTENER LA ÚLTIMA SECCIÓN EN LA CUAL SE ESCRIBIRÁ
        section = doc.LastSection

        'SE DECIDE EL NÚMERO DE COLUMNAS Y SU ANCHO
        ReDim Preserve anchCol(0 To 15)
        anchCol(0) = 1 : anchCol(1) = 1 : anchCol(2) = 1 : anchCol(3) = 1 : anchCol(4) = 1 : anchCol(5) = 1 : anchCol(6) = 1 : anchCol(7) = 1
        anchCol(8) = 1 : anchCol(9) = 1 : anchCol(10) = 1 : anchCol(11) = 1 : anchCol(12) = 1 : anchCol(13) = 1 : anchCol(14) = 1 : anchCol(15) = 1

        'NÚMERO DE FILAS MÍNIMO QUE TENDRÁ LA TABLA
        nFil = 7

        'SE EXAMINA QUÉ DATOS NO OBLIGATORIOS ESTÁN PRESENTES

        par = section.AddParagraph(CStr(oTextos(107)(1))) : Me.ponerFormatoParrafo(par, True, 8, True, Colors.LightBlue, , , True, True, , , , , True, True, True, True)
        par = section.AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, , , , , , , , , , , True, True, True, True)

        tab = Me.constTab(section, anchCol, nFil, Colors.White)

        colAux = IIf(UBound(anchCol) + 1 < tab.Columns.Count, 1, 0)

        Me.alinHorContTab(tab, CFacturaePDF.center) : Me.alinVerContTab(tab, CFacturaePDF.center)

        par = tab.Rows(0).Cells(0 + colAux).AddParagraph(UCase(CStr(oTextos(108)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        If Not IsNothing(factura.invoiceHeader.corrective.reasonDescription) Then
            par = tab.Rows(1).Cells(0 + colAux).AddParagraph(UCase(reasonDescription_Obj2PDF(factura.invoiceHeader.corrective.reasonDescription.value))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        Else
            par = tab.Rows(1).Cells(0 + colAux).AddParagraph(UCase("")) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        End If
        par = tab.Rows(2).Cells(0 + colAux).AddParagraph(UCase(CStr(oTextos(109)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        par = tab.Rows(3).Cells(0 + colAux).AddParagraph(UCase(CStr(oTextos(110)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        par = tab.Rows(3).Cells(8 + colAux).AddParagraph(UCase(CStr(oTextos(111)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        If Not IsNothing(factura.invoiceHeader.corrective.taxPeriod) AndAlso Not IsNothing(factura.invoiceHeader.corrective.taxPeriod.startDate) Then
            par = tab.Rows(4).Cells(0 + colAux).AddParagraph(UCase(date_ObjId2Str(factura.invoiceHeader.corrective.taxPeriod.startDate))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        Else
            par = tab.Rows(4).Cells(0 + colAux).AddParagraph(UCase("")) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        End If
        If Not IsNothing(factura.invoiceHeader.corrective.taxPeriod) AndAlso Not IsNothing(factura.invoiceHeader.corrective.taxPeriod.endDate) Then
            par = tab.Rows(4).Cells(8 + colAux).AddParagraph(UCase(date_ObjId2Str(factura.invoiceHeader.corrective.taxPeriod.endDate))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        Else
            par = tab.Rows(4).Cells(8 + colAux).AddParagraph(UCase("")) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        End If
        par = tab.Rows(5).Cells(0 + colAux).AddParagraph(UCase(CStr(oTextos(112)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        If Not IsNothing(factura.invoiceHeader.corrective.correctionMethodDescription) Then
            par = tab.Rows(6).Cells(0 + colAux).AddParagraph(UCase(correctionMethodDescription_Obj2PDF(factura.invoiceHeader.corrective.correctionMethodDescription.value))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        Else
            par = tab.Rows(6).Cells(0 + colAux).AddParagraph(UCase("")) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        End If

        tab.Rows(0).KeepWith = tab.Rows.Count - 1
        Me.bordeColumnas(tab, Colors.LightBlue, colAux = 1)
        Me.bordeFilas(tab, Colors.LightBlue, colAux = 1)
        Me.autoUnirCeldasDeTabla(tab, 0, colAux, CFacturaePDF.prioridadFilas, CFacturaePDF.prioridadColumnas)
    End Sub
    ''' <summary>
    ''' Imprime parte de la facturae contenida en el objeto CFacturae en el objeto que representa el documento PDF a crear.
    ''' Concretamente imprime los totales a nivel de factura, para la factura parámetro.
    ''' </summary>
    ''' <param name="factura">Un objeto que representa una factura de las contenidas en una facturae</param>
    ''' <param name="doc">Es el objeto que representa el documento PDF a crear</param>
    ''' <remarks>Llamado desde: CFacturaePDF.escribirFactura   
    ''' Tiempo máximo: menor a 0,1
    ''' Revisado por: auv. 28/05/2012</remarks>
    Private Sub escribirTotales(ByRef factura As InvoiceType, ByRef doc As MigraDoc.DocumentObjectModel.Document)
        'DECLARACIÓN DE LAS VARIABLES LOCALES
        Dim par As Paragraph = Nothing
        Dim section As Section = Nothing
        Dim tab As Table = Nothing
        Dim anchCol As Double() = Nothing
        Dim colAux As Integer = Nothing
        Dim nFil As Integer = Nothing
        Dim hayTotalDeDescuentos As Boolean = Nothing
        Dim hayTotalDeCargos As Boolean = Nothing

        'OBTENER LA ÚLTIMA SECCIÓN EN LA CUAL SE ESCRIBIRÁ
        section = doc.LastSection

        'SE DECIDE EL NÚMERO DE COLUMNAS Y SU ANCHO
        ReDim Preserve anchCol(0 To 15)
        anchCol(0) = 1 : anchCol(1) = 1 : anchCol(2) = 1 : anchCol(3) = 1 : anchCol(4) = 1 : anchCol(5) = 1 : anchCol(6) = 1 : anchCol(7) = 1
        anchCol(8) = 1 : anchCol(9) = 1 : anchCol(10) = 1 : anchCol(11) = 1 : anchCol(12) = 1 : anchCol(13) = 1 : anchCol(14) = 1 : anchCol(15) = 1

        'NÚMERO DE FILAS MÍNIMO QUE TENDRÁ LA TABLA
        nFil = 5

        If Not IsNothing(factura.invoiceTotals) Then
            If Not IsNothing(factura.invoiceTotals.totalGeneralDiscounts) Then
                hayTotalDeDescuentos = True
            Else
                hayTotalDeDescuentos = False
            End If
            If Not IsNothing(factura.invoiceTotals.totalGeneralSurcharges) Then
                hayTotalDeCargos = True
            Else
                hayTotalDeCargos = False
            End If
        Else
            hayTotalDeDescuentos = False
            hayTotalDeCargos = False
        End If

        nFil = IIf(hayTotalDeDescuentos, nFil + 1, nFil) : nFil = IIf(hayTotalDeCargos, nFil + 1, nFil)

        par = section.AddParagraph(CStr(oTextos(23)(1))) : Me.ponerFormatoParrafo(par, True, 8, True, Colors.LightBlue, , , True, True, , , , , True, True, True, True)
        par = section.AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, , , , , , , , , , , True, True, True, True)

        tab = Me.constTab(section, anchCol, nFil, Colors.White)

        colAux = IIf(UBound(anchCol) + 1 < tab.Columns.Count, 1, 0)

        Me.alinHorContTab(tab, CFacturaePDF.center) : Me.alinVerContTab(tab, CFacturaePDF.center)

        par = tab.Rows(0).Cells(0 + colAux).AddParagraph(UCase(CStr(oTextos(49)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        If Not IsNothing(factura.invoiceTotals) AndAlso Not IsNothing(factura.invoiceTotals.totalGrossAmount) Then
            par = tab.Rows(0).Cells(8 + colAux).AddParagraph(dbl2str(str2Dbl(factura.invoiceTotals.totalGrossAmount.dValue), ".")) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
        Else
            par = tab.Rows(0).Cells(8 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
        End If
        If Not IsNothing(factura.invoiceTotals) AndAlso Not (IsNothing(factura.invoiceTotals.totalGeneralSurcharges)) AndAlso hayTotalDeCargos Then
            par = tab.Rows(1).Cells(0 + colAux).AddParagraph(UCase(CStr(oTextos(25)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
            par = tab.Rows(1).Cells(8 + colAux).AddParagraph(dbl2str(str2Dbl(factura.invoiceTotals.totalGeneralSurcharges.dValue), ".")) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
        End If
        If Not IsNothing(factura.invoiceTotals) AndAlso Not (IsNothing(factura.invoiceTotals.totalGeneralDiscounts)) AndAlso hayTotalDeDescuentos Then
            par = tab.Rows(1 + (IIf(hayTotalDeCargos, 1, 0))).Cells(0 + colAux).AddParagraph(UCase(CStr(oTextos(26)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
            par = tab.Rows(1 + (IIf(hayTotalDeCargos, 1, 0))).Cells(8 + colAux).AddParagraph(dbl2str(str2Dbl(factura.invoiceTotals.totalGeneralDiscounts.dValue), ".")) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
        End If
        par = tab.Rows(1 + (IIf(hayTotalDeDescuentos, 1, 0)) + (IIf(hayTotalDeCargos, 1, 0))).Cells(0 + colAux).AddParagraph(UCase(CStr(oTextos(27)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        If Not IsNothing(factura.invoiceTotals) AndAlso Not IsNothing(factura.invoiceTotals.totalGrossAmountBeforeTaxes) Then
            par = tab.Rows(1 + (IIf(hayTotalDeDescuentos, 1, 0)) + (IIf(hayTotalDeCargos, 1, 0))).Cells(8 + colAux).AddParagraph(dbl2str(str2Dbl(factura.invoiceTotals.totalGrossAmountBeforeTaxes.dValue), ".")) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
        Else
            par = tab.Rows(1 + (IIf(hayTotalDeDescuentos, 1, 0)) + (IIf(hayTotalDeCargos, 1, 0))).Cells(8 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
        End If
        par = tab.Rows(2 + (IIf(hayTotalDeDescuentos, 1, 0)) + (IIf(hayTotalDeCargos, 1, 0))).Cells(0 + colAux).AddParagraph(UCase(CStr(oTextos(28)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        If Not IsNothing(factura.invoiceTotals) AndAlso Not IsNothing(factura.invoiceTotals.totalTaxOutputs) Then
            par = tab.Rows(2 + (IIf(hayTotalDeDescuentos, 1, 0)) + (IIf(hayTotalDeCargos, 1, 0))).Cells(8 + colAux).AddParagraph(dbl2str(str2Dbl(factura.invoiceTotals.totalTaxOutputs.dValue), ".")) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
        Else
            par = tab.Rows(2 + (IIf(hayTotalDeDescuentos, 1, 0)) + (IIf(hayTotalDeCargos, 1, 0))).Cells(8 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
        End If
        par = tab.Rows(3 + (IIf(hayTotalDeDescuentos, 1, 0)) + (IIf(hayTotalDeCargos, 1, 0))).Cells(0 + colAux).AddParagraph(UCase(CStr(oTextos(29)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        If Not IsNothing(factura.invoiceTotals) AndAlso Not IsNothing(factura.invoiceTotals.totalTaxesWithheld) Then
            par = tab.Rows(3 + (IIf(hayTotalDeDescuentos, 1, 0)) + (IIf(hayTotalDeCargos, 1, 0))).Cells(8 + colAux).AddParagraph(dbl2str(str2Dbl(factura.invoiceTotals.totalTaxesWithheld.dValue), ".")) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
        Else
            par = tab.Rows(3 + (IIf(hayTotalDeDescuentos, 1, 0)) + (IIf(hayTotalDeCargos, 1, 0))).Cells(8 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
        End If
        par = tab.Rows(4 + (IIf(hayTotalDeDescuentos, 1, 0)) + (IIf(hayTotalDeCargos, 1, 0))).Cells(0 + colAux).AddParagraph(UCase(CStr(oTextos(30)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        If Not IsNothing(factura.invoiceTotals) AndAlso Not IsNothing(factura.invoiceTotals.invoiceTotal) Then
            par = tab.Rows(4 + (IIf(hayTotalDeDescuentos, 1, 0)) + (IIf(hayTotalDeCargos, 1, 0))).Cells(8 + colAux).AddParagraph(dbl2str(str2Dbl(factura.invoiceTotals.invoiceTotal.dValue), ".")) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
        Else
            par = tab.Rows(4 + (IIf(hayTotalDeDescuentos, 1, 0)) + (IIf(hayTotalDeCargos, 1, 0))).Cells(8 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
        End If

        tab.Rows(0).KeepWith = tab.Rows.Count - 1
        Me.bordeColumnas(tab, Colors.LightBlue, colAux = 1)
        Me.bordeFilas(tab, Colors.LightBlue, colAux = 1)
        Me.autoUnirCeldasDeTabla(tab, 0, colAux, CFacturaePDF.prioridadFilas, CFacturaePDF.prioridadColumnas)
    End Sub
    ''' <summary>
    ''' Imprime parte de la facturae contenida en el objeto CFacturae en el objeto que representa el documento PDF a crear.
    ''' Concretamente imprime los albaranes de la factura parámetro.
    ''' </summary>
    ''' <param name="factura">Un objeto que representa una factura de las contenidas en una facturae</param>
    ''' <param name="doc">Es el objeto que representa el documento PDF a crear</param>
    ''' <remarks>Llamado desde: CFacturaePDF.escribirFactura
    ''' Tiempo máximo: variable, depende del número de items, cargos, descuentos, albaranes, impuestos repercutidos e
    ''' impuestos retenidos que tenga la factura parámetro
    ''' Revisado por: auv. 28/05/2012</remarks>
    Private Sub escribirAlbaranesDeFactura(ByRef factura As InvoiceType, ByRef doc As MigraDoc.DocumentObjectModel.Document)
        Dim contadorDeItems As Integer = Nothing
        Dim item As ItemType = Nothing
        Dim contadorDeAlbaranesDelItem As Integer = Nothing
        Dim contadorDeAlbaranes As Integer = Nothing
        Dim codigosDeAlbaranes() As String = Nothing
        Dim numeroDeCodigosDeAlbaranes As Integer = Nothing

        Dim esta As Boolean = Nothing
        Dim par As Paragraph = Nothing
        Dim section As Section = Nothing
        Dim articulos() As ItemType = Nothing
        Dim numeroDeArticulos As Integer = Nothing
        Dim contadorDeArticulos As Integer = Nothing

        Dim hayDescuentos As Boolean = Nothing
        Dim hayCargos As Boolean = Nothing
        Dim hayImpuestosRepercutidos As Boolean = Nothing
        Dim hayImpuestosRetenidos As Boolean = Nothing

        section = doc.LastSection
        numeroDeCodigosDeAlbaranes = 0

        contadorDeItems = 0
        If Not IsNothing(factura.items) Then
            While contadorDeItems < factura.items.Count
                item = factura.items.ElementAt(contadorDeItems)
                If Not IsNothing(item) AndAlso Not IsNothing(item.deliveryNotesReferences) Then
                    contadorDeAlbaranesDelItem = 0
                    While contadorDeAlbaranesDelItem < item.deliveryNotesReferences.Count
                        contadorDeAlbaranes = 0
                        esta = False
                        While contadorDeAlbaranes < numeroDeCodigosDeAlbaranes And Not esta
                            If Not IsNothing(item.deliveryNotesReferences(contadorDeAlbaranesDelItem).deliveryNoteNumber) AndAlso _
                                item.deliveryNotesReferences(contadorDeAlbaranesDelItem).deliveryNoteNumber = codigosDeAlbaranes(contadorDeAlbaranes) Then
                                esta = True
                            Else
                                contadorDeAlbaranes = contadorDeAlbaranes + 1
                            End If
                        End While
                        If Not esta Then
                            If Not IsNothing(item.deliveryNotesReferences(contadorDeAlbaranesDelItem).deliveryNoteNumber) Then
                                ReDim Preserve codigosDeAlbaranes(0 To numeroDeCodigosDeAlbaranes)
                                codigosDeAlbaranes(numeroDeCodigosDeAlbaranes) = item.deliveryNotesReferences(contadorDeAlbaranesDelItem).deliveryNoteNumber
                                numeroDeCodigosDeAlbaranes = numeroDeCodigosDeAlbaranes + 1
                            End If
                        End If
                        contadorDeAlbaranesDelItem = contadorDeAlbaranesDelItem + 1
                    End While
                End If
                contadorDeItems = contadorDeItems + 1
            End While
        End If

        contadorDeAlbaranes = 0
        While contadorDeAlbaranes < numeroDeCodigosDeAlbaranes
            hayDescuentos = False
            hayCargos = False
            hayImpuestosRepercutidos = False
            hayImpuestosRetenidos = False
            numeroDeArticulos = 0
            contadorDeArticulos = 0
            If Not IsNothing(factura.items) Then
                While contadorDeArticulos < factura.items.Count
                    If Not IsNothing(factura.items.ElementAt(contadorDeArticulos)) AndAlso _
                        Not IsNothing(factura.items.ElementAt(contadorDeArticulos).deliveryNotesReferences) Then
                        If Not IsNothing(factura.items.ElementAt(contadorDeArticulos).deliveryNotesReferences.ElementAt(0)) AndAlso Not IsNothing(factura.items.ElementAt(contadorDeArticulos).deliveryNotesReferences.ElementAt(0).deliveryNoteNumber) AndAlso _
                            factura.items.ElementAt(contadorDeArticulos).deliveryNotesReferences.ElementAt(0).deliveryNoteNumber = codigosDeAlbaranes(contadorDeAlbaranes) Then
                            ReDim Preserve articulos(0 To numeroDeArticulos)
                            articulos(numeroDeArticulos) = factura.items.ElementAt(contadorDeArticulos)
                            If Not IsNothing(articulos(numeroDeArticulos).discountsAndRebates) AndAlso articulos(numeroDeArticulos).discountsAndRebates.Count > 0 Then
                                hayDescuentos = True
                            End If
                            If Not IsNothing(articulos(numeroDeArticulos).charges) AndAlso articulos(numeroDeArticulos).charges.Count > 0 Then
                                hayCargos = True
                            End If
                            If Not IsNothing(articulos(numeroDeArticulos).taxesOutputs) AndAlso articulos(numeroDeArticulos).taxesOutputs.Count > 0 Then
                                hayImpuestosRepercutidos = True
                            End If
                            If Not IsNothing(articulos(numeroDeArticulos).taxesWithheld) AndAlso articulos(numeroDeArticulos).taxesWithheld.Count > 0 Then
                                hayImpuestosRetenidos = True
                            End If
                            numeroDeArticulos = numeroDeArticulos + 1
                        End If
                    End If
                    contadorDeArticulos = contadorDeArticulos + 1
                End While
            End If
            par = section.AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, , , , , , , , , , , True, True, True, True)
            If Not IsNothing(codigosDeAlbaranes(contadorDeAlbaranes)) Then
                par = section.AddParagraph(UCase(CStr(oTextos(31)(1))) & """" & codigosDeAlbaranes(contadorDeAlbaranes) & """") : Me.ponerFormatoParrafo(par, True, 10, True, Colors.LightBlue, True, True, , , True, Underline.Single, , , True, True, True, True)
            Else
                par = section.AddParagraph(UCase(CStr(oTextos(31)(1))) & """" & "" & """") : Me.ponerFormatoParrafo(par, True, 10, True, Colors.LightBlue, True, True, , , True, Underline.Single, , , True, True, True, True)
            End If

            par = section.AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, , , , , , , , , , , True, True, True, True)
            Me.escribirAlbaranTablaDatosGenerales(articulos, doc)
            par = section.AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, , , , , , , , , , , True, True, True, True)
            Me.escribirAlbaranTablaTotales(articulos, doc)
            If hayCargos Then
                par = section.AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, , , , , , , , , , , True, True, True, True)
                Me.escribirAlbaranTablaCargos(articulos, doc)
            End If
            If hayDescuentos Then
                par = section.AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, , , , , , , , , , , True, True, True, True)
                Me.escribirAlbaranTablaDescuentos(articulos, doc)
            End If
            If hayImpuestosRepercutidos Then
                par = section.AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, , , , , , , , , , , True, True, True, True)
                Me.escribirAlbaranTablaImpuestosRepercutidos(articulos, doc)
            End If
            If hayImpuestosRetenidos Then
                par = section.AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, , , , , , , , , , , True, True, True, True)
                Me.escribirAlbaranTablaImpuestosRetenidos(articulos, doc)
            End If
            contadorDeAlbaranes = contadorDeAlbaranes + 1
        End While
    End Sub
    ''' <summary>
    ''' Imprime parte de la facturae contenida en el objeto CFacturae en el objeto que representa el documento PDF a crear.
    ''' Concretamente imprime el número y la fecha del albarán al que pertenecen todos los items parámetro
    ''' </summary>
    ''' <param name="items">Array que contiene objetos que representan todos los items del albarán a escribir</param>
    ''' <param name="doc">Es el objeto que representa el documento PDF a crear</param>
    ''' <remarks>Llamado desde: CFacturaePDF.escribirAlbaranesDeFactura
    ''' Tiempo máximo:menor a 0,1   
    ''' Revisado por: auv. 28/05/2012</remarks>
    Private Sub escribirAlbaranTablaDatosGenerales(ByRef items() As ItemType, ByRef doc As MigraDoc.DocumentObjectModel.Document)
        'DECLARACIÓN DE LAS VARIABLES LOCALES
        Dim par As Paragraph = Nothing
        Dim section As Section = Nothing
        Dim tab As Table = Nothing
        Dim anchCol As Double() = Nothing
        Dim colAux As Integer = Nothing
        Dim nFil As Integer = Nothing
        Dim contadorDeItems As Integer = Nothing
        Dim numeroDelAlbaran As String = Nothing
        Dim fechaDelAlbaran As String = Nothing

        'OBTENER LA ÚLTIMA SECCIÓN EN LA CUAL SE ESCRIBIRÁ
        section = doc.LastSection

        'SE DECIDE EL NÚMERO DE COLUMNAS Y SU ANCHO
        ReDim Preserve anchCol(0 To 15)
        anchCol(0) = 1 : anchCol(1) = 1 : anchCol(2) = 1 : anchCol(3) = 1 : anchCol(4) = 1 : anchCol(5) = 1 : anchCol(6) = 1 : anchCol(7) = 1
        anchCol(8) = 1 : anchCol(9) = 1 : anchCol(10) = 1 : anchCol(11) = 1 : anchCol(12) = 1 : anchCol(13) = 1 : anchCol(14) = 1 : anchCol(15) = 1

        'NÚMERO DE FILAS MÍNIMO QUE TENDRÁ LA TABLA
        nFil = 2

        par = section.AddParagraph(CStr(oTextos(2)(1))) : Me.ponerFormatoParrafo(par, True, 8, True, Colors.LightBlue, , , True, True, , , , , True, True, True, True)
        par = section.AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, , , , , , , , , , , True, True, True, True)

        tab = Me.constTab(section, anchCol, nFil, Colors.White)

        colAux = IIf(UBound(anchCol) + 1 < tab.Columns.Count, 1, 0)

        Me.alinHorContTab(tab, CFacturaePDF.center) : Me.alinVerContTab(tab, CFacturaePDF.center)

        contadorDeItems = 0
        While contadorDeItems < items.Count And (IsNothing(numeroDelAlbaran) Or IsNothing(fechaDelAlbaran))
            If Not IsNothing(items.ElementAt(contadorDeItems).deliveryNotesReferences) AndAlso _
                Not IsNothing(items.ElementAt(contadorDeItems).deliveryNotesReferences.ElementAt(0)) Then
                If Not IsNothing(items.ElementAt(contadorDeItems).deliveryNotesReferences.ElementAt(0).deliveryNoteNumber) Then
                    numeroDelAlbaran = items.ElementAt(contadorDeItems).deliveryNotesReferences.ElementAt(0).deliveryNoteNumber
                End If
                If Not IsNothing(items.ElementAt(contadorDeItems).deliveryNotesReferences.ElementAt(0).deliveryNoteDate) Then
                    fechaDelAlbaran = items.ElementAt(contadorDeItems).deliveryNotesReferences.ElementAt(0).deliveryNoteDate.ToString
                End If
            End If
            contadorDeItems = contadorDeItems + 1
        End While

        par = tab.Rows(0).Cells(0 + colAux).AddParagraph(UCase(CStr(oTextos(20)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        If Not IsNothing(numeroDelAlbaran) Then
            par = tab.Rows(1).Cells(0 + colAux).AddParagraph(numeroDelAlbaran) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
        Else
            par = tab.Rows(1).Cells(0 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
        End If
        par = tab.Rows(0).Cells(8 + colAux).AddParagraph(UCase(CStr(oTextos(21)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        If Not IsNothing(fechaDelAlbaran) Then
            par = tab.Rows(1).Cells(8 + colAux).AddParagraph(fechaDelAlbaran) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
        Else
            par = tab.Rows(1).Cells(8 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
        End If

        tab.Rows(0).KeepWith = tab.Rows.Count - 1
        Me.bordeColumnas(tab, Colors.LightBlue, colAux = 1)
        Me.bordeFilas(tab, Colors.LightBlue, colAux = 1)
        Me.autoUnirCeldasDeTabla(tab, 0, colAux, CFacturaePDF.prioridadFilas, CFacturaePDF.prioridadColumnas)
    End Sub
    ''' <summary>
    ''' Imprime parte de la facturae contenida en el objeto CFacturae en el objeto que representa el documento PDF a crear.
    ''' Concretamente imprime los totales del albarán
    ''' </summary>
    ''' <param name="items">Array que contiene objetos que representan todos los items del albarán a escribir</param>
    ''' <param name="doc">Es el objeto que representa el documento PDF a crear</param>
    ''' <remarks>Llamado desde: CFacturaePDF.escribirAlbaranesDeFactura   
    ''' Tiempo máximo: variable, depende del número de items, cargos, descuentos, impuestos repercutidos e impuestos 
    ''' retenidos que tenga el albarán (implícito en el parámetro de array de items)  
    ''' Revisado por: auv. 28/05/2012</remarks>
    Private Sub escribirAlbaranTablaTotales(ByRef items() As ItemType, ByRef doc As MigraDoc.DocumentObjectModel.Document)
        'DECLARACIÓN DE LAS VARIABLES LOCALES
        Dim par As Paragraph = Nothing
        Dim section As Section = Nothing
        Dim tab As Table = Nothing
        Dim anchCol As Double() = Nothing
        Dim colAux As Integer = Nothing
        Dim nFil As Integer = Nothing
        Dim contadorDeItems As Integer = Nothing
        Dim totalImporteBrutoDelAlbaran As String = Nothing
        Dim totalDescuentosDelAlbaran As String = Nothing
        Dim totalCargosDelAlbaran As String = Nothing
        Dim totalImpuestosRepercutidosDelAlbaran As String = Nothing
        Dim totalImpuestosRetenidosDelAlbaran As String = Nothing
        Dim contadorDeDescuentosDeItem As Integer = Nothing
        Dim contadorDeCargosDeItem As Integer = Nothing
        Dim contadorDeImpuestosRepercutidosDeItem As Integer = Nothing
        Dim contadorDeImpuestosRetenidosDeItem As Integer = Nothing

        'OBTENER LA ÚLTIMA SECCIÓN EN LA CUAL SE ESCRIBIRÁ
        section = doc.LastSection

        'SE DECIDE EL NÚMERO DE COLUMNAS Y SU ANCHO
        ReDim Preserve anchCol(0 To 15)
        anchCol(0) = 1 : anchCol(1) = 1 : anchCol(2) = 1 : anchCol(3) = 1 : anchCol(4) = 1 : anchCol(5) = 1 : anchCol(6) = 1 : anchCol(7) = 1
        anchCol(8) = 1 : anchCol(9) = 1 : anchCol(10) = 1 : anchCol(11) = 1 : anchCol(12) = 1 : anchCol(13) = 1 : anchCol(14) = 1 : anchCol(15) = 1

        'NÚMERO DE FILAS MÍNIMO QUE TENDRÁ LA TABLA
        nFil = 5

        par = section.AddParagraph(CStr(oTextos(32)(1))) : Me.ponerFormatoParrafo(par, True, 8, True, Colors.LightBlue, , , True, True, , , , , True, True, True, True)
        par = section.AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, , , , , , , , , , , True, True, True, True)

        tab = Me.constTab(section, anchCol, nFil, Colors.White)

        colAux = IIf(UBound(anchCol) + 1 < tab.Columns.Count, 1, 0)

        Me.alinHorContTab(tab, CFacturaePDF.center) : Me.alinVerContTab(tab, CFacturaePDF.center)

        totalImporteBrutoDelAlbaran = "0,00"
        contadorDeItems = 0
        While contadorDeItems < items.Count
            If Not IsNothing(items.ElementAt(contadorDeItems).grossAmount) Then
                totalImporteBrutoDelAlbaran = dbl2str((str2Dbl(totalImporteBrutoDelAlbaran) + str2Dbl(items.ElementAt(contadorDeItems).grossAmount.dValue)), ",")
            End If
            contadorDeItems = contadorDeItems + 1
        End While
        totalCargosDelAlbaran = "0,00"
        contadorDeItems = 0
        While contadorDeItems < items.Count
            contadorDeCargosDeItem = 0
            If Not IsNothing(items.ElementAt(contadorDeItems).charges) Then
                While contadorDeCargosDeItem < items.ElementAt(contadorDeItems).charges.Count
                    If Not IsNothing(items.ElementAt(contadorDeItems).charges.ElementAt(contadorDeCargosDeItem)) AndAlso _
                        Not IsNothing(items.ElementAt(contadorDeItems).charges.ElementAt(contadorDeCargosDeItem).chargeAmount) Then
                        totalCargosDelAlbaran = dbl2str((str2Dbl(totalCargosDelAlbaran) + str2Dbl(items.ElementAt(contadorDeItems).charges.ElementAt(contadorDeCargosDeItem).chargeAmount.dValue)), ",")
                    End If
                    contadorDeCargosDeItem = contadorDeCargosDeItem + 1
                End While
            End If
            contadorDeItems = contadorDeItems + 1
        End While

        totalDescuentosDelAlbaran = "0,00"
        contadorDeItems = 0
        While contadorDeItems < items.Count
            contadorDeDescuentosDeItem = 0
            If Not IsNothing(items.ElementAt(contadorDeItems).discountsAndRebates) Then
                While contadorDeDescuentosDeItem < items.ElementAt(contadorDeItems).discountsAndRebates.Count
                    If Not IsNothing(items.ElementAt(contadorDeItems).discountsAndRebates.ElementAt(contadorDeDescuentosDeItem)) AndAlso _
                        Not IsNothing(items.ElementAt(contadorDeItems).discountsAndRebates.ElementAt(contadorDeDescuentosDeItem).discountAmount) Then
                        totalDescuentosDelAlbaran = dbl2str((str2Dbl(totalDescuentosDelAlbaran) + str2Dbl(items.ElementAt(contadorDeItems).discountsAndRebates.ElementAt(contadorDeDescuentosDeItem).discountAmount.dValue)), ",")
                    End If
                    contadorDeDescuentosDeItem = contadorDeDescuentosDeItem + 1
                End While
            End If
            contadorDeItems = contadorDeItems + 1
        End While

        totalImpuestosRepercutidosDelAlbaran = "0,00"
        contadorDeItems = 0
        While contadorDeItems < items.Count
            contadorDeImpuestosRepercutidosDeItem = 0
            If Not IsNothing(items.ElementAt(contadorDeItems).taxesOutputs) Then
                While contadorDeImpuestosRepercutidosDeItem < items.ElementAt(contadorDeItems).taxesOutputs.Count
                    If Not IsNothing(items.ElementAt(contadorDeItems).taxesOutputs.ElementAt(contadorDeImpuestosRepercutidosDeItem)) AndAlso _
                        Not IsNothing(items.ElementAt(contadorDeItems).taxesOutputs.ElementAt(contadorDeImpuestosRepercutidosDeItem).taxAmount) AndAlso _
                        Not IsNothing(items.ElementAt(contadorDeItems).taxesOutputs.ElementAt(contadorDeImpuestosRepercutidosDeItem).taxAmount.totalAmount) Then
                        totalImpuestosRepercutidosDelAlbaran = dbl2str((str2Dbl(totalImpuestosRepercutidosDelAlbaran) + str2Dbl(items.ElementAt(contadorDeItems).taxesOutputs.ElementAt(contadorDeImpuestosRepercutidosDeItem).taxAmount.totalAmount.dValue)), ",")
                        If Not IsNothing(items.ElementAt(contadorDeItems).taxesOutputs.ElementAt(contadorDeImpuestosRepercutidosDeItem).equivalenceSurchargeAmount) AndAlso _
                            Not IsNothing(items.ElementAt(contadorDeItems).taxesOutputs.ElementAt(contadorDeImpuestosRepercutidosDeItem).equivalenceSurchargeAmount.totalAmount) Then
                            totalImpuestosRepercutidosDelAlbaran = dbl2str((str2Dbl(totalImpuestosRepercutidosDelAlbaran) + str2Dbl(items.ElementAt(contadorDeItems).taxesOutputs.ElementAt(contadorDeImpuestosRepercutidosDeItem).equivalenceSurchargeAmount.totalAmount.dValue)), ",")
                        End If
                    End If
                    contadorDeImpuestosRepercutidosDeItem = contadorDeImpuestosRepercutidosDeItem + 1
                End While
            End If
            contadorDeItems = contadorDeItems + 1
        End While

        totalImpuestosRetenidosDelAlbaran = "0,00"
        contadorDeItems = 0
        While contadorDeItems < items.Count
            contadorDeImpuestosRetenidosDeItem = 0
            If Not IsNothing(items.ElementAt(contadorDeItems).taxesWithheld) Then
                While contadorDeImpuestosRetenidosDeItem < items.ElementAt(contadorDeItems).taxesWithheld.Count
                    If Not IsNothing(items.ElementAt(contadorDeItems).taxesWithheld.ElementAt(contadorDeImpuestosRetenidosDeItem)) AndAlso _
                        Not IsNothing(items.ElementAt(contadorDeItems).taxesWithheld.ElementAt(contadorDeImpuestosRetenidosDeItem).taxAmount) AndAlso _
                        Not IsNothing(items.ElementAt(contadorDeItems).taxesWithheld.ElementAt(contadorDeImpuestosRetenidosDeItem).taxAmount.totalAmount) Then
                        totalImpuestosRetenidosDelAlbaran = dbl2str((str2Dbl(totalImpuestosRetenidosDelAlbaran) + str2Dbl(items.ElementAt(contadorDeItems).taxesWithheld.ElementAt(contadorDeImpuestosRetenidosDeItem).taxAmount.totalAmount.dValue)), ",")
                    End If
                    contadorDeImpuestosRetenidosDeItem = contadorDeImpuestosRetenidosDeItem + 1
                End While
            End If
            contadorDeItems = contadorDeItems + 1
        End While

        par = tab.Rows(0).Cells(0 + colAux).AddParagraph(UCase(CStr(oTextos(24)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        par = tab.Rows(0).Cells(8 + colAux).AddParagraph(dbl2str(str2Dbl(totalImporteBrutoDelAlbaran), ".")) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
        par = tab.Rows(1).Cells(0 + colAux).AddParagraph(UCase(CStr(oTextos(25)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        par = tab.Rows(1).Cells(8 + colAux).AddParagraph(dbl2str(str2Dbl(totalCargosDelAlbaran), ".")) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
        par = tab.Rows(2).Cells(0 + colAux).AddParagraph(UCase(CStr(oTextos(26)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        par = tab.Rows(2).Cells(8 + colAux).AddParagraph(dbl2str(str2Dbl(totalDescuentosDelAlbaran), ".")) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
        par = tab.Rows(3).Cells(0 + colAux).AddParagraph(UCase(CStr(oTextos(28)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        par = tab.Rows(3).Cells(8 + colAux).AddParagraph(dbl2str(str2Dbl(totalImpuestosRepercutidosDelAlbaran), ".")) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
        par = tab.Rows(4).Cells(0 + colAux).AddParagraph(UCase(CStr(oTextos(29)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        par = tab.Rows(4).Cells(8 + colAux).AddParagraph(dbl2str(str2Dbl(totalImpuestosRetenidosDelAlbaran), ".")) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )

        tab.Rows(0).KeepWith = tab.Rows.Count - 1
        Me.bordeColumnas(tab, Colors.LightBlue, colAux = 1)
        Me.bordeFilas(tab, Colors.LightBlue, colAux = 1)
        Me.autoUnirCeldasDeTabla(tab, 0, colAux, CFacturaePDF.prioridadFilas, CFacturaePDF.prioridadColumnas)
    End Sub
    ''' <summary>
    ''' Imprime parte de la facturae contenida en el objeto CFacturae en el objeto que representa el documento PDF a crear.
    ''' Concretamente imprime los cargos del albarán.
    ''' </summary>
    ''' <param name="items">Array que contiene objetos que representan todos los items del albarán a escribir</param>
    ''' <param name="doc">Es el objeto que representa el documento PDF a crear</param>
    ''' <remarks>Llamado desde: CFacturaePDF.escribirAlbaranesDeFactura   
    ''' Tiempo máximo: variable, depende del número de items y cargos que tenga el albarán (implícito en el 
    ''' parámetro de array de items)    
    ''' Revisado por: auv. 28/05/2012</remarks>
    Private Sub escribirAlbaranTablaCargos(ByRef items() As ItemType, ByRef doc As MigraDoc.DocumentObjectModel.Document)
        'DECLARACIÓN DE LAS VARIABLES LOCALES
        Dim par As Paragraph = Nothing
        Dim section As Section = Nothing
        Dim tab As Table = Nothing
        Dim anchCol As Double() = Nothing
        Dim colAux As Integer = Nothing
        Dim nFil As Integer = Nothing
        Dim contadorDeItems As Integer = Nothing
        Dim contadorDeCargos As Integer = Nothing
        Dim contadorDeCargosDeItem As Integer = Nothing
        Dim cargo As ChargeType = Nothing

        'OBTENER LA ÚLTIMA SECCIÓN EN LA CUAL SE ESCRIBIRÁ
        section = doc.LastSection

        'SE DECIDE EL NÚMERO DE COLUMNAS Y SU ANCHO
        ReDim Preserve anchCol(0 To 15)
        anchCol(0) = 1 : anchCol(1) = 1 : anchCol(2) = 1 : anchCol(3) = 1 : anchCol(4) = 1 : anchCol(5) = 1 : anchCol(6) = 1 : anchCol(7) = 1
        anchCol(8) = 1 : anchCol(9) = 1 : anchCol(10) = 1 : anchCol(11) = 1 : anchCol(12) = 1 : anchCol(13) = 1 : anchCol(14) = 1 : anchCol(15) = 1

        contadorDeCargos = 0
        contadorDeItems = LBound(items)
        While contadorDeItems <= UBound(items)
            If Not IsNothing(items(contadorDeItems).charges) Then
                contadorDeCargos = contadorDeCargos + items(contadorDeItems).charges.Count
            End If
            contadorDeItems = contadorDeItems + 1
        End While

        'NÚMERO DE FILAS MÍNIMO QUE TENDRÁ LA TABLA
        nFil = 1 + contadorDeCargos

        par = section.AddParagraph(CStr(oTextos(33)(1))) : Me.ponerFormatoParrafo(par, True, 8, True, Colors.LightBlue, , , True, True, , , , , True, True, True, True)
        par = section.AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, , , , , , , , , , , True, True, True, True)

        tab = Me.constTab(section, anchCol, nFil, Colors.White)

        colAux = IIf(UBound(anchCol) + 1 < tab.Columns.Count, 1, 0)

        Me.alinHorContTab(tab, CFacturaePDF.center) : Me.alinVerContTab(tab, CFacturaePDF.center)

        par = tab.Rows(0).Cells(0 + colAux).AddParagraph(UCase(CStr(oTextos(34)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        par = tab.Rows(0).Cells(10 + colAux).AddParagraph(UCase(CStr(oTextos(35)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        par = tab.Rows(0).Cells(12 + colAux).AddParagraph(UCase(CStr(oTextos(36)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )

        contadorDeCargos = 0
        contadorDeItems = LBound(items)
        While contadorDeItems <= UBound(items)
            If Not IsNothing(items(contadorDeItems).charges) Then
                contadorDeCargosDeItem = LBound(items(contadorDeItems).charges)
                While contadorDeCargosDeItem <= UBound(items(contadorDeItems).charges)
                    cargo = items(contadorDeItems).charges(contadorDeCargosDeItem)
                    If Not IsNothing(cargo) Then
                        If Not IsNothing(cargo.chargeReason) Then
                            par = tab.Rows(1 + contadorDeCargos).Cells(0 + colAux).AddParagraph(cargo.chargeReason) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                        Else
                            par = tab.Rows(1 + contadorDeCargos).Cells(0 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                        End If
                        If Not IsNothing(cargo.chargeRate) Then
                            par = tab.Rows(1 + contadorDeCargos).Cells(10 + colAux).AddParagraph(dbl2str(str2Dbl(cargo.chargeRate.dValue), ".")) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                        Else
                            par = tab.Rows(1 + contadorDeCargos).Cells(10 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                        End If
                        If Not IsNothing(cargo.chargeAmount) Then
                            par = tab.Rows(1 + contadorDeCargos).Cells(12 + colAux).AddParagraph(dbl2str(str2Dbl(cargo.chargeAmount.dValue), ".")) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                        Else
                            par = tab.Rows(1 + contadorDeCargos).Cells(12 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                        End If
                    End If
                    contadorDeCargos = contadorDeCargos + 1
                    contadorDeCargosDeItem = contadorDeCargosDeItem + 1
                End While
            End If
            contadorDeItems = contadorDeItems + 1
        End While

        If tab.Rows.Count > 1 Then
            tab.Rows(0).KeepWith = 1
        End If
        Me.bordeColumnas(tab, Colors.LightBlue, colAux = 1)
        Me.bordeFilas(tab, Colors.LightBlue, colAux = 1)
        Me.autoUnirCeldasDeTabla(tab, 0, colAux, CFacturaePDF.prioridadFilas, CFacturaePDF.prioridadColumnas)
    End Sub
    ''' <summary>
    ''' Imprime parte de la facturae contenida en el objeto CFacturae en el objeto que representa el documento PDF a crear.
    ''' Concretamente imprime los descuentos del albarán.
    ''' </summary>
    ''' <param name="items">Array que contiene objetos que representan todos los items del albarán a escribir</param>
    ''' <param name="doc">Es el objeto que representa el documento PDF a crear</param>
    ''' <remarks>Llamado desde: CFacturaePDF.escribirAlbaranesDeFactura   
    ''' Tiempo máximo: variable, depende del número de items y descuentos que tenga el albarán (implícito en el 
    ''' parámetro de array de items)    
    ''' Revisado por: auv. 28/05/2012</remarks>
    Private Sub escribirAlbaranTablaDescuentos(ByRef items() As ItemType, ByRef doc As MigraDoc.DocumentObjectModel.Document)
        'DECLARACIÓN DE LAS VARIABLES LOCALES
        Dim par As Paragraph = Nothing
        Dim section As Section = Nothing
        Dim tab As Table = Nothing
        Dim anchCol As Double() = Nothing
        Dim colAux As Integer = Nothing
        Dim nFil As Integer = Nothing
        Dim contadorDeItems As Integer = Nothing
        Dim contadorDeDescuentos As Integer = Nothing
        Dim contadorDeDescuentosDeItem As Integer = Nothing
        Dim descuento As DiscountType = Nothing

        'OBTENER LA ÚLTIMA SECCIÓN EN LA CUAL SE ESCRIBIRÁ
        section = doc.LastSection

        'SE DECIDE EL NÚMERO DE COLUMNAS Y SU ANCHO
        ReDim Preserve anchCol(0 To 15)
        anchCol(0) = 1 : anchCol(1) = 1 : anchCol(2) = 1 : anchCol(3) = 1 : anchCol(4) = 1 : anchCol(5) = 1 : anchCol(6) = 1 : anchCol(7) = 1
        anchCol(8) = 1 : anchCol(9) = 1 : anchCol(10) = 1 : anchCol(11) = 1 : anchCol(12) = 1 : anchCol(13) = 1 : anchCol(14) = 1 : anchCol(15) = 1

        contadorDeDescuentos = 0
        contadorDeItems = LBound(items)
        While contadorDeItems <= UBound(items)
            If Not (IsNothing(items(contadorDeItems).discountsAndRebates)) Then
                contadorDeDescuentos = contadorDeDescuentos + items(contadorDeItems).discountsAndRebates.Count
            End If
            contadorDeItems = contadorDeItems + 1
        End While

        'NÚMERO DE FILAS MÍNIMO QUE TENDRÁ LA TABLA
        nFil = 1 + contadorDeDescuentos

        par = section.AddParagraph(CStr(oTextos(37)(1))) : Me.ponerFormatoParrafo(par, True, 8, True, Colors.LightBlue, , , True, True, , , , , True, True, True, True)
        par = section.AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, , , , , , , , , , , True, True, True, True)

        tab = Me.constTab(section, anchCol, nFil, Colors.White)

        colAux = IIf(UBound(anchCol) + 1 < tab.Columns.Count, 1, 0)

        Me.alinHorContTab(tab, CFacturaePDF.center) : Me.alinVerContTab(tab, CFacturaePDF.center)

        par = tab.Rows(0).Cells(0 + colAux).AddParagraph(UCase(CStr(oTextos(34)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        par = tab.Rows(0).Cells(10 + colAux).AddParagraph(UCase(CStr(oTextos(35)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        par = tab.Rows(0).Cells(12 + colAux).AddParagraph(UCase(CStr(oTextos(36)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )

        contadorDeDescuentos = 0
        contadorDeItems = LBound(items)
        While contadorDeItems <= UBound(items)
            If Not (IsNothing(items(contadorDeItems).discountsAndRebates)) Then
                contadorDeDescuentosDeItem = LBound(items(contadorDeItems).discountsAndRebates)
                While contadorDeDescuentosDeItem <= UBound(items(contadorDeItems).discountsAndRebates)
                    descuento = items(contadorDeItems).discountsAndRebates(contadorDeDescuentosDeItem)
                    If Not IsNothing(descuento) Then
                        If Not IsNothing(descuento.discountReason) Then
                            par = tab.Rows(1 + contadorDeDescuentos).Cells(0 + colAux).AddParagraph(descuento.discountReason) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                        Else
                            par = tab.Rows(1 + contadorDeDescuentos).Cells(0 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                        End If
                        If Not IsNothing(descuento.discountRate) Then
                            par = tab.Rows(1 + contadorDeDescuentos).Cells(10 + colAux).AddParagraph(dbl2str(str2Dbl(descuento.discountRate.dValue), ".")) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                        Else
                            par = tab.Rows(1 + contadorDeDescuentos).Cells(10 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                        End If
                        If Not IsNothing(descuento.discountAmount) Then
                            par = tab.Rows(1 + contadorDeDescuentos).Cells(12 + colAux).AddParagraph(dbl2str(str2Dbl(descuento.discountAmount.dValue), ".")) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                        Else
                            par = tab.Rows(1 + contadorDeDescuentos).Cells(12 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                        End If
                    End If
                    contadorDeDescuentos = contadorDeDescuentos + 1
                    contadorDeDescuentosDeItem = contadorDeDescuentosDeItem + 1
                End While
            End If
            contadorDeItems = contadorDeItems + 1
        End While

        If tab.Rows.Count > 1 Then
            tab.Rows(0).KeepWith = 1
        End If
        Me.bordeColumnas(tab, Colors.LightBlue, colAux = 1)
        Me.bordeFilas(tab, Colors.LightBlue, colAux = 1)
        Me.autoUnirCeldasDeTabla(tab, 0, colAux, CFacturaePDF.prioridadFilas, CFacturaePDF.prioridadColumnas)
    End Sub
    ''' <summary>
    ''' Imprime parte de la facturae contenida en el objeto CFacturae en el objeto que representa el documento PDF a crear.
    ''' Concretamente imprime los impuestos repercutidos del albarán.
    ''' </summary>
    ''' <param name="items">Array que contiene objetos que representan todos los items del albarán a escribir</param>
    ''' <param name="doc">Es el objeto que representa el documento PDF a crear</param>
    ''' <remarks>Llamado desde: CFacturaePDF.escribirAlbaranesDeFactura   
    ''' Tiempo máximo: variable, depende del número de items e impuestos repercutidos que tenga el albarán (implícito 
    ''' en el parámetro de array de items)  
    ''' Revisado por: auv. 28/05/2012</remarks>
    Private Sub escribirAlbaranTablaImpuestosRepercutidos(ByRef items() As ItemType, ByRef doc As MigraDoc.DocumentObjectModel.Document)
        'DECLARACIÓN DE LAS VARIABLES LOCALES
        Dim par As Paragraph = Nothing
        Dim section As Section = Nothing
        Dim tab As Table = Nothing
        Dim anchCol As Double() = Nothing
        Dim colAux As Integer = Nothing
        Dim nFil As Integer = Nothing
        Dim contadorDeItems As Integer = Nothing
        Dim contadorDeImpuestosRepercutidos As Integer = Nothing
        Dim contadorDeImpuestosRepercutidosDeItem As Integer = Nothing
        Dim impuestoRepercutido As TaxOutputType = Nothing
        Dim impuestosRepercutidosAgrupados() As TaxOutputType = Nothing

        'OBTENER LA ÚLTIMA SECCIÓN EN LA CUAL SE ESCRIBIRÁ
        section = doc.LastSection

        'SE DECIDE EL NÚMERO DE COLUMNAS Y SU ANCHO
        ReDim Preserve anchCol(0 To 15)
        anchCol(0) = 1 : anchCol(1) = 1 : anchCol(2) = 1 : anchCol(3) = 1 : anchCol(4) = 1 : anchCol(5) = 1 : anchCol(6) = 1 : anchCol(7) = 1
        anchCol(8) = 1 : anchCol(9) = 1 : anchCol(10) = 1 : anchCol(11) = 1 : anchCol(12) = 1 : anchCol(13) = 1 : anchCol(14) = 1 : anchCol(15) = 1

        impuestosRepercutidosAgrupados = New TaxOutputType() {}
        contadorDeImpuestosRepercutidos = 0
        contadorDeItems = 0
        While contadorDeItems < items.Count
            contadorDeImpuestosRepercutidosDeItem = 0
            If Not IsNothing(items.ElementAt(contadorDeItems).taxesOutputs) Then
                While contadorDeImpuestosRepercutidosDeItem < items.ElementAt(contadorDeItems).taxesOutputs.Count
                    If Not IsNothing(items.ElementAt(contadorDeItems).taxesOutputs.ElementAt(contadorDeImpuestosRepercutidosDeItem)) Then
                        ReDim Preserve impuestosRepercutidosAgrupados(0 To contadorDeImpuestosRepercutidos)
                        impuestosRepercutidosAgrupados(contadorDeImpuestosRepercutidos) = items.ElementAt(contadorDeItems).taxesOutputs.ElementAt(contadorDeImpuestosRepercutidosDeItem)
                        contadorDeImpuestosRepercutidos = contadorDeImpuestosRepercutidos + 1
                    End If
                    contadorDeImpuestosRepercutidosDeItem = contadorDeImpuestosRepercutidosDeItem + 1
                End While
            End If
            contadorDeItems = contadorDeItems + 1
        End While

        If Not IsNothing(impuestosRepercutidosAgrupados) Then
            impuestosRepercutidosAgrupados = Me.agruparImpuestosRepercutidos(impuestosRepercutidosAgrupados)
            If Not IsNothing(impuestosRepercutidosAgrupados) Then

                contadorDeImpuestosRepercutidos = impuestosRepercutidosAgrupados.Count

                'NÚMERO DE FILAS MÍNIMO QUE TENDRÁ LA TABLA
                nFil = 1 + contadorDeImpuestosRepercutidos

                par = section.AddParagraph(CStr(oTextos(38)(1))) : Me.ponerFormatoParrafo(par, True, 8, True, Colors.LightBlue, , , True, True, , , , , True, True, True, True)
                par = section.AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, , , , , , , , , , , True, True, True, True)

                tab = Me.constTab(section, anchCol, nFil, Colors.White)

                colAux = IIf(UBound(anchCol) + 1 < tab.Columns.Count, 1, 0)

                Me.alinHorContTab(tab, CFacturaePDF.center) : Me.alinVerContTab(tab, CFacturaePDF.center)

                par = tab.Rows(0).Cells(0 + colAux).AddParagraph(UCase(CStr(oTextos(39)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
                par = tab.Rows(0).Cells(4 + colAux).AddParagraph(UCase(CStr(oTextos(35)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
                par = tab.Rows(0).Cells(8 + colAux).AddParagraph(UCase(CStr(oTextos(87)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
                par = tab.Rows(0).Cells(12 + colAux).AddParagraph(UCase(CStr(oTextos(40)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )

                contadorDeImpuestosRepercutidos = 0
                While contadorDeImpuestosRepercutidos < impuestosRepercutidosAgrupados.Count
                    impuestoRepercutido = impuestosRepercutidosAgrupados.ElementAt(contadorDeImpuestosRepercutidos)
                    If Not IsNothing(impuestoRepercutido) Then
                        If Not IsNothing(impuestoRepercutido.taxTypeCode) Then
                            par = tab.Rows(1 + contadorDeImpuestosRepercutidos).Cells(0 + colAux).AddParagraph(taxTypeCode_Obj2PDF(impuestoRepercutido.taxTypeCode.value)) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                        Else
                            par = tab.Rows(1 + contadorDeImpuestosRepercutidos).Cells(0 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                        End If
                        If Not IsNothing(impuestoRepercutido.taxRate) Then
                            par = tab.Rows(1 + contadorDeImpuestosRepercutidos).Cells(4 + colAux).AddParagraph(dbl2str(str2Dbl(impuestoRepercutido.taxRate.dValue), ".")) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                        Else
                            par = tab.Rows(1 + contadorDeImpuestosRepercutidos).Cells(4 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                        End If
                        If Not IsNothing(impuestoRepercutido.taxableBase) AndAlso Not IsNothing(impuestoRepercutido.taxableBase.totalAmount) Then
                            par = tab.Rows(1 + contadorDeImpuestosRepercutidos).Cells(8 + colAux).AddParagraph(dbl2str(str2Dbl(impuestoRepercutido.taxableBase.totalAmount.dValue), ".")) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                        Else
                            par = tab.Rows(1 + contadorDeImpuestosRepercutidos).Cells(8 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                        End If
                        If Not IsNothing(impuestoRepercutido.taxAmount) AndAlso Not IsNothing(impuestoRepercutido.taxAmount.totalAmount) Then
                            par = tab.Rows(1 + contadorDeImpuestosRepercutidos).Cells(12 + colAux).AddParagraph(dbl2str(str2Dbl(impuestoRepercutido.taxAmount.totalAmount.dValue), ".")) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                        Else
                            par = tab.Rows(1 + contadorDeImpuestosRepercutidos).Cells(12 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                        End If
                    End If
                    contadorDeImpuestosRepercutidos = contadorDeImpuestosRepercutidos + 1
                End While

                If tab.Rows.Count > 1 Then
                    tab.Rows(0).KeepWith = 1
                End If
                Me.bordeColumnas(tab, Colors.LightBlue, colAux = 1)
                Me.bordeFilas(tab, Colors.LightBlue, colAux = 1)
                Me.autoUnirCeldasDeTabla(tab, 0, colAux, CFacturaePDF.prioridadFilas, CFacturaePDF.prioridadColumnas)
            End If
        End If
    End Sub
    ''' <summary>
    ''' Imprime parte de la facturae contenida en el objeto CFacturae en el objeto que representa el documento PDF a crear.
    ''' Concretamente imprime los impuestos retenidos del albarán.
    ''' </summary>
    ''' <param name="items">Array que contiene objetos que representan todos los items del albarán a escribir</param>
    ''' <param name="doc">Es el objeto que representa el documento PDF a crear</param>
    ''' <remarks>Llamado desde: CFacturaePDF.escribirAlbaranesDeFactura  
    ''' Tiempo máximo: variable, depende del número de items e impuestos retenidos que tenga el albarán (implícito en el 
    ''' parámetro de array de items)   
    ''' Revisado por: auv. 28/05/2012</remarks>
    Private Sub escribirAlbaranTablaImpuestosRetenidos(ByRef items() As ItemType, ByRef doc As MigraDoc.DocumentObjectModel.Document)
        'DECLARACIÓN DE LAS VARIABLES LOCALES
        Dim par As Paragraph = Nothing
        Dim section As Section = Nothing
        Dim tab As Table = Nothing
        Dim anchCol As Double() = Nothing
        Dim colAux As Integer = Nothing
        Dim nFil As Integer = Nothing
        Dim contadorDeItems As Integer = Nothing
        Dim contadorDeImpuestosRetenidos As Integer = Nothing
        Dim contadorDeImpuestosRetenidosDeItem As Integer = Nothing
        Dim impuestoRetenido As TaxWithheldType = Nothing
        Dim impuestosRetenidosAgrupados() As TaxWithheldType = Nothing

        'OBTENER LA ÚLTIMA SECCIÓN EN LA CUAL SE ESCRIBIRÁ
        section = doc.LastSection

        'SE DECIDE EL NÚMERO DE COLUMNAS Y SU ANCHO
        ReDim Preserve anchCol(0 To 15)
        anchCol(0) = 1 : anchCol(1) = 1 : anchCol(2) = 1 : anchCol(3) = 1 : anchCol(4) = 1 : anchCol(5) = 1 : anchCol(6) = 1 : anchCol(7) = 1
        anchCol(8) = 1 : anchCol(9) = 1 : anchCol(10) = 1 : anchCol(11) = 1 : anchCol(12) = 1 : anchCol(13) = 1 : anchCol(14) = 1 : anchCol(15) = 1

        impuestosRetenidosAgrupados = New TaxWithheldType() {}
        contadorDeImpuestosRetenidos = 0
        contadorDeItems = 0
        While contadorDeItems < items.Count
            contadorDeImpuestosRetenidosDeItem = 0
            If Not IsNothing(items.ElementAt(contadorDeItems).taxesWithheld) Then
                While contadorDeImpuestosRetenidosDeItem < items.ElementAt(contadorDeItems).taxesWithheld.Count
                    If Not IsNothing(items.ElementAt(contadorDeItems).taxesWithheld.ElementAt(contadorDeImpuestosRetenidosDeItem)) Then
                        ReDim Preserve impuestosRetenidosAgrupados(0 To contadorDeImpuestosRetenidos)
                        impuestosRetenidosAgrupados(contadorDeImpuestosRetenidos) = items.ElementAt(contadorDeItems).taxesWithheld.ElementAt(contadorDeImpuestosRetenidosDeItem)
                        contadorDeImpuestosRetenidos = contadorDeImpuestosRetenidos + 1
                    End If
                    contadorDeImpuestosRetenidosDeItem = contadorDeImpuestosRetenidosDeItem + 1
                End While
            End If
            contadorDeItems = contadorDeItems + 1
        End While

        If Not IsNothing(impuestosRetenidosAgrupados) Then
            impuestosRetenidosAgrupados = Me.agruparImpuestosRetenidos(impuestosRetenidosAgrupados)
            If Not IsNothing(impuestosRetenidosAgrupados) Then

                contadorDeImpuestosRetenidos = impuestosRetenidosAgrupados.Count

                'NÚMERO DE FILAS MÍNIMO QUE TENDRÁ LA TABLA
                nFil = 1 + contadorDeImpuestosRetenidos

                par = section.AddParagraph(CStr(oTextos(41)(1))) : Me.ponerFormatoParrafo(par, True, 8, True, Colors.LightBlue, , , True, True, , , , , True, True, True, True)
                par = section.AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, , , , , , , , , , , True, True, True, True)

                tab = Me.constTab(section, anchCol, nFil, Colors.White)

                colAux = IIf(UBound(anchCol) + 1 < tab.Columns.Count, 1, 0)

                Me.alinHorContTab(tab, CFacturaePDF.center) : Me.alinVerContTab(tab, CFacturaePDF.center)

                par = tab.Rows(0).Cells(0 + colAux).AddParagraph(UCase(CStr(oTextos(39)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
                par = tab.Rows(0).Cells(4 + colAux).AddParagraph(UCase(CStr(oTextos(35)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
                par = tab.Rows(0).Cells(8 + colAux).AddParagraph(UCase(CStr(oTextos(87)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
                par = tab.Rows(0).Cells(12 + colAux).AddParagraph(UCase(CStr(oTextos(40)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )

                contadorDeImpuestosRetenidos = 0
                While contadorDeImpuestosRetenidos < impuestosRetenidosAgrupados.Count
                    impuestoRetenido = impuestosRetenidosAgrupados.ElementAt(contadorDeImpuestosRetenidos)
                    If Not IsNothing(impuestoRetenido) Then
                        If Not IsNothing(impuestoRetenido.taxTypeCode) Then
                            par = tab.Rows(1 + contadorDeImpuestosRetenidos).Cells(0 + colAux).AddParagraph(taxTypeCode_Obj2PDF(impuestoRetenido.taxTypeCode.value)) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                        Else
                            par = tab.Rows(1 + contadorDeImpuestosRetenidos).Cells(0 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                        End If
                        If Not IsNothing(impuestoRetenido.taxRate) Then
                            par = tab.Rows(1 + contadorDeImpuestosRetenidos).Cells(4 + colAux).AddParagraph(dbl2str(str2Dbl(impuestoRetenido.taxRate.dValue), ".")) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                        Else
                            par = tab.Rows(1 + contadorDeImpuestosRetenidos).Cells(4 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                        End If
                        If Not IsNothing(impuestoRetenido.taxableBase) AndAlso Not IsNothing(impuestoRetenido.taxableBase.totalAmount) Then
                            par = tab.Rows(1 + contadorDeImpuestosRetenidos).Cells(8 + colAux).AddParagraph(dbl2str(str2Dbl(impuestoRetenido.taxableBase.totalAmount.dValue), ".")) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                        Else
                            par = tab.Rows(1 + contadorDeImpuestosRetenidos).Cells(8 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                        End If
                        If Not IsNothing(impuestoRetenido.taxAmount) AndAlso Not IsNothing(impuestoRetenido.taxAmount.totalAmount) Then
                            par = tab.Rows(1 + contadorDeImpuestosRetenidos).Cells(12 + colAux).AddParagraph(dbl2str(str2Dbl(impuestoRetenido.taxAmount.totalAmount.dValue), ".")) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                        Else
                            par = tab.Rows(1 + contadorDeImpuestosRetenidos).Cells(12 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                        End If
                    End If
                    contadorDeImpuestosRetenidos = contadorDeImpuestosRetenidos + 1
                End While

                If tab.Rows.Count > 1 Then
                    tab.Rows(0).KeepWith = 1
                End If
                Me.bordeColumnas(tab, Colors.LightBlue, colAux = 1)
                Me.bordeFilas(tab, Colors.LightBlue, colAux = 1)
                Me.autoUnirCeldasDeTabla(tab, 0, colAux, CFacturaePDF.prioridadFilas, CFacturaePDF.prioridadColumnas)
            End If
        End If
    End Sub
    ''' <summary>
    ''' Imprime parte de la facturae contenida en el objeto CFacturae en el objeto que representa el documento PDF a crear.
    ''' Concretamente imprime los items de la factura parámetro
    ''' </summary>
    ''' <param name="factura">Un objeto que representa una factura de las contenidas en una facturae</param>
    ''' <param name="doc">Es el objeto que representa el documento PDF a crear</param>
    ''' <remarks>Llamado desde: CFacturaePDF.escribirFactura
    ''' Tiempo máximo: variable, depende del número de items, cargos, descuentos, impuestos retenidos e impuestos repercutidos
    ''' que tenga la factura parámetro    
    ''' Revisado por: auv. 28/05/2012</remarks>
    Private Sub escribirDetallesDeFactura(ByRef factura As InvoiceType, ByRef doc As MigraDoc.DocumentObjectModel.Document)
        Dim par As Paragraph = Nothing
        Dim section As Section = Nothing
        Dim contadorDeItems As Integer = Nothing
        Dim item As ItemType = Nothing

        section = doc.LastSection

        contadorDeItems = 0
        If Not IsNothing(factura.items) Then
            While contadorDeItems < factura.items.Count
                item = factura.items.ElementAt(contadorDeItems)
                If Not IsNothing(item) Then
                    par = section.AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, , , , , , , , , , , True, True, True, True)
                    Me.escribirItemDeFactura(item, doc)
                End If
                contadorDeItems = contadorDeItems + 1
            End While
        End If
    End Sub
    ''' <summary>
    ''' Imprime parte de la facturae contenida en el objeto CFacturae en el objeto que representa el documento PDF a crear.
    ''' Concretamente imprime el item parámetro
    ''' </summary>
    ''' <param name="item">Un objeto que representa el item que se debe describir</param>
    ''' <param name="doc">Es el objeto que representa el documento PDF a crear</param>
    ''' <remarks>Llamado desde: CFacturaePDF.escribirDetallesDeFactura
    ''' Tiempo máximo: variable, depende del número de cargos, descuentos, impuestos retenidos e impuestos repercutidos que
    ''' tenga el item parámetro    
    ''' Revisado por: auv. 28/05/2012</remarks>
    Private Sub escribirItemDeFactura(ByRef item As ItemType, ByRef doc As MigraDoc.DocumentObjectModel.Document)
        Dim par As Paragraph = Nothing
        Dim section As Section = Nothing

        section = doc.LastSection

        If Not IsNothing(item.itemDescription) Then
            par = section.AddParagraph(UCase(CStr(oTextos(42)(1))) & """" & item.itemDescription & """") : Me.ponerFormatoParrafo(par, True, 10, True, Colors.LightBlue, True, True, , , True, Underline.Single, , , True, True, True, True)
        Else
            par = section.AddParagraph(UCase(CStr(oTextos(42)(1))) & "") : Me.ponerFormatoParrafo(par, True, 10, True, Colors.LightBlue, True, True, , , True, Underline.Single, , , True, True, True, True)
        End If
        par = section.AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, , , , , , , , , , , True, True, True, True)
        Me.escribirItemTablaDatosGenerales(item, doc)
        If Not (IsNothing(item.deliveryNotesReferences)) Then
            par = section.AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, , , , , , , , , , , True, True, True, True)
            Me.escribirItemTablaAlbaranes(item, doc)
        End If
        par = section.AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, , , , , , , , , , , True, True, True, True)
        Me.escribirItemImportes(item, doc)
        If Not (IsNothing(item.specialTaxableEvent)) Then
            par = section.AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, , , , , , , , , , , True, True, True, True)
            Me.escribirItemTablaEventoEspecialImpuesto(item, doc)
        End If
    End Sub
    ''' <summary>
    ''' Imprime parte de la facturae contenida en el objeto CFacturae en el objeto que representa el documento PDF a crear.
    ''' Concretamente del item parámetro imprime, su número de línea, su cantidad, su unidad de medida y su descripción
    ''' </summary>
    ''' <param name="item">Un objeto que representa el item que se debe describir</param>
    ''' <param name="doc">Es el objeto que representa el documento PDF a crear</param>
    ''' <remarks>Llamado desde: CFacturaePDF.escribirItemDeFactura
    ''' Tiempo máximo: menor a 0,1   
    ''' Revisado por: auv. 28/05/2012</remarks>
    Private Sub escribirItemTablaDatosGenerales(ByRef item As ItemType, ByRef doc As MigraDoc.DocumentObjectModel.Document)
        Authenticate()
        Dim obj_DatosBD As DataTable = Nothing
        'DECLARACIÓN DE LAS VARIABLES LOCALES
        Dim par As Paragraph = Nothing
        Dim section As Section = Nothing
        Dim tab As Table = Nothing
        Dim anchCol As Double() = Nothing
        Dim colAux As Integer = Nothing
        Dim nFil As Integer = Nothing

        'OBTENER LA ÚLTIMA SECCIÓN EN LA CUAL SE ESCRIBIRÁ
        section = doc.LastSection

        'SE DECIDE EL NÚMERO DE COLUMNAS Y SU ANCHO
        ReDim Preserve anchCol(0 To 15)
        anchCol(0) = 1 : anchCol(1) = 1 : anchCol(2) = 1 : anchCol(3) = 1 : anchCol(4) = 1 : anchCol(5) = 1 : anchCol(6) = 1 : anchCol(7) = 1
        anchCol(8) = 1 : anchCol(9) = 1 : anchCol(10) = 1 : anchCol(11) = 1 : anchCol(12) = 1 : anchCol(13) = 1 : anchCol(14) = 1 : anchCol(15) = 1

        'NÚMERO DE FILAS MÍNIMO QUE TENDRÁ LA TABLA
        nFil = 16

        par = section.AddParagraph(CStr(oTextos(2)(1))) : Me.ponerFormatoParrafo(par, True, 8, True, Colors.LightBlue, , , True, True, , , , , True, True, True, True)
        par = section.AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, , , , , , , , , , , True, True, True, True)

        tab = Me.constTab(section, anchCol, nFil, Colors.White)

        colAux = IIf(UBound(anchCol) + 1 < tab.Columns.Count, 1, 0)

        Me.alinHorContTab(tab, CFacturaePDF.center) : Me.alinVerContTab(tab, CFacturaePDF.center)

        par = tab.Rows(0).Cells(0 + colAux).AddParagraph(UCase(CStr(oTextos(43)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        If Not IsNothing(item.sequenceNumber) Then
            par = tab.Rows(1).Cells(0 + colAux).AddParagraph(CStr(item.sequenceNumber)) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
        Else
            par = tab.Rows(1).Cells(0 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
        End If
        par = tab.Rows(0).Cells(8 + colAux).AddParagraph(UCase(CStr(oTextos(44)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        If Not IsNothing(item.quantity) Then
            par = tab.Rows(1).Cells(8 + colAux).AddParagraph(dbl2str(item.quantity, ".")) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
        Else
            par = tab.Rows(1).Cells(8 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
        End If
        par = tab.Rows(2).Cells(0 + colAux).AddParagraph(UCase(CStr(oTextos(45)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        If Not (IsNothing(item.unitOfMeasure)) Then
            obj_DatosBD = DBServer.obtNombreUnidad(item.unitOfMeasure.value, sIdi)
        Else
            obj_DatosBD = Nothing
        End If
        If Not IsNothing(obj_DatosBD) AndAlso obj_DatosBD.Rows.Count > 0 Then
            If Not IsNothing(obj_DatosBD.Rows(0)("DEN")) Then
                par = tab.Rows(3).Cells(0 + colAux).AddParagraph(CStr(obj_DatosBD.Rows(0)("DEN"))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
            Else
                par = tab.Rows(3).Cells(0 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
            End If
        Else
            par = tab.Rows(3).Cells(0 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
        End If
        par = tab.Rows(2).Cells(8 + colAux).AddParagraph(UCase(CStr(oTextos(46)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        If Not (IsNothing(item.itemDescription)) Then
            If Not (IsNothing(item.articleCode)) Then
                par = tab.Rows(3).Cells(8 + colAux).AddParagraph(item.articleCode & ": " & item.itemDescription) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
            Else
                par = tab.Rows(3).Cells(8 + colAux).AddParagraph("" & ": " & item.itemDescription) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
            End If
        Else
            If Not (IsNothing(item.articleCode)) Then
                par = tab.Rows(3).Cells(8 + colAux).AddParagraph(item.articleCode & ": " & "") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
            Else
                par = tab.Rows(3).Cells(8 + colAux).AddParagraph("" & ": " & "") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
            End If
        End If

        tab.Rows(0).KeepWith = tab.Rows.Count - 1
        Me.bordeColumnas(tab, Colors.LightBlue, colAux = 1)
        Me.bordeFilas(tab, Colors.LightBlue, colAux = 1)
        Me.autoUnirCeldasDeTabla(tab, 0, colAux, CFacturaePDF.prioridadFilas, CFacturaePDF.prioridadColumnas)
    End Sub
    ''' <summary>
    ''' Imprime parte de la facturae contenida en el objeto CFacturae en el objeto que representa el documento PDF a crear.
    ''' Concretamente del item parámetro imprime el número de albarán al que pertenece y la fecha de éste.
    ''' </summary>
    ''' <param name="item">Un objeto que representa el item que se debe describir</param>
    ''' <param name="doc">Es el objeto que representa el documento PDF a crear</param>
    ''' <remarks>Llamado desde: CFacturaePDF.escribirItemDeFactura   
    ''' Tiempo máximo: menor a 0,1    
    ''' Revisado por: auv. 28/05/2012</remarks>
    Private Sub escribirItemTablaAlbaranes(ByRef item As ItemType, ByRef doc As MigraDoc.DocumentObjectModel.Document)
        'DECLARACIÓN DE LAS VARIABLES LOCALES
        Dim par As Paragraph = Nothing
        Dim section As Section = Nothing
        Dim tab As Table = Nothing
        Dim anchCol As Double() = Nothing
        Dim colAux As Integer = Nothing
        Dim nFil As Integer = Nothing
        Dim contadorDeAlbaranes As Integer = Nothing
        Dim albaran As DeliveryNoteType = Nothing

        'OBTENER LA ÚLTIMA SECCIÓN EN LA CUAL SE ESCRIBIRÁ
        section = doc.LastSection

        'SE DECIDE EL NÚMERO DE COLUMNAS Y SU ANCHO
        ReDim Preserve anchCol(0 To 15)
        anchCol(0) = 1 : anchCol(1) = 1 : anchCol(2) = 1 : anchCol(3) = 1 : anchCol(4) = 1 : anchCol(5) = 1 : anchCol(6) = 1 : anchCol(7) = 1
        anchCol(8) = 1 : anchCol(9) = 1 : anchCol(10) = 1 : anchCol(11) = 1 : anchCol(12) = 1 : anchCol(13) = 1 : anchCol(14) = 1 : anchCol(15) = 1

        'NÚMERO DE FILAS MÍNIMO QUE TENDRÁ LA TABLA
        nFil = 1

        If Not IsNothing(item.deliveryNotesReferences) Then
            nFil = nFil + item.deliveryNotesReferences.Count

            par = section.AddParagraph(CStr(oTextos(47)(1))) : Me.ponerFormatoParrafo(par, True, 8, True, Colors.LightBlue, , , True, True, , , , , True, True, True, True)
            par = section.AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, , , , , , , , , , , True, True, True, True)

            tab = Me.constTab(section, anchCol, nFil, Colors.White)

            colAux = IIf(UBound(anchCol) + 1 < tab.Columns.Count, 1, 0)

            Me.alinHorContTab(tab, CFacturaePDF.center) : Me.alinVerContTab(tab, CFacturaePDF.center)

            par = tab.Rows(0).Cells(0 + colAux).AddParagraph(UCase(CStr(oTextos(48)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
            par = tab.Rows(0).Cells(8 + colAux).AddParagraph(UCase(CStr(oTextos(21)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )

            contadorDeAlbaranes = 0
            While contadorDeAlbaranes < item.deliveryNotesReferences.Count
                albaran = item.deliveryNotesReferences.ElementAt(contadorDeAlbaranes)
                If Not IsNothing(albaran) Then
                    If Not IsNothing(albaran.deliveryNoteNumber) Then
                        par = tab.Rows(1 + contadorDeAlbaranes).Cells(0 + colAux).AddParagraph(albaran.deliveryNoteNumber) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                    Else
                        par = tab.Rows(1 + contadorDeAlbaranes).Cells(0 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                    End If
                    If Not IsNothing(albaran.deliveryNoteDate) Then
                        par = tab.Rows(1 + contadorDeAlbaranes).Cells(8 + colAux).AddParagraph(albaran.deliveryNoteDate.ToString) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                    Else
                        par = tab.Rows(1 + contadorDeAlbaranes).Cells(8 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                    End If
                End If
                contadorDeAlbaranes = contadorDeAlbaranes + 1
            End While

            If tab.Rows.Count > 1 Then
                tab.Rows(0).KeepWith = 1
            End If
            Me.bordeColumnas(tab, Colors.LightBlue, colAux = 1)
            Me.bordeFilas(tab, Colors.LightBlue, colAux = 1)
            Me.autoUnirCeldasDeTabla(tab, 0, colAux, CFacturaePDF.prioridadFilas, CFacturaePDF.prioridadColumnas)
        End If
    End Sub
    ''' <summary>
    ''' Imprime parte de la facturae contenida en el objeto CFacturae en el objeto que representa el documento PDF a crear.
    ''' Concretamente del item parámetro se imprimen sus importes (descuentos, cargos, impuestos, totales)
    ''' </summary>
    ''' <param name="item">Un objeto que representa el item que se debe describir</param>
    ''' <param name="doc">Es el objeto que representa el documento PDF a crear</param>
    ''' <remarks>Llamado desde: CFacturaePDF.escribirItemDeFactura   
    ''' Tiempo máximo: variable, depende del número de descuentos, cargos, impuestos retenidos e impuestos
    ''' repercutidos que tenga el item parámetro 
    ''' Revisado por: auv. 28/05/2012</remarks>
    Private Sub escribirItemImportes(ByRef item As ItemType, ByRef doc As MigraDoc.DocumentObjectModel.Document)
        Dim par As Paragraph = Nothing
        Dim section As Section = Nothing

        section = doc.LastSection

        If Not (IsNothing(item.charges)) Then
            Me.escribirItemImportesTablaCargos(item, doc)
            par = section.AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, , , , , , , , , , , True, True, True, True)
        End If
        If Not (IsNothing(item.discountsAndRebates)) Then
            Me.escribirItemImportesTablaDescuentos(item, doc)
            par = section.AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, , , , , , , , , , , True, True, True, True)
        End If
        Me.escribirItemImportesTablaImporteBruto(item, doc)
        If Not (IsNothing(item.taxesOutputs)) Then
            par = section.AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, , , , , , , , , , , True, True, True, True)
            Me.escribirItemImportesTablaImpuestosRepercutidos(item, doc)
        End If
        If Not (IsNothing(item.taxesWithheld)) Then
            par = section.AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, , , , , , , , , , , True, True, True, True)
            Me.escribirItemImportesTablaImpuestosRetenidos(item, doc)
        End If
    End Sub
    ''' <summary>
    ''' Imprime parte de la facturae contenida en el objeto CFacturae en el objeto que representa el documento PDF a crear.
    ''' Concretamente del item parámetro se imprimen sus cargos.
    ''' </summary>
    ''' <param name="item">Un objeto que representa el item que se debe describir</param>
    ''' <param name="doc">Es el objeto que representa el documento PDF a crear</param>
    ''' <remarks>Llamado desde: CFacturaePDF.escribirItemImportes
    ''' Tiempo máximo: variable, depende del número de cargos que tenga el item parámetro   
    ''' Revisado por: auv. 28/05/2012</remarks>
    Private Sub escribirItemImportesTablaCargos(ByRef item As ItemType, ByRef doc As MigraDoc.DocumentObjectModel.Document)
        'DECLARACIÓN DE LAS VARIABLES LOCALES
        Dim par As Paragraph = Nothing
        Dim section As Section = Nothing
        Dim tab As Table = Nothing
        Dim anchCol As Double() = Nothing
        Dim colAux As Integer = Nothing
        Dim nFil As Integer = Nothing
        Dim contadorDeRecargos As Integer = Nothing
        Dim recargo As ChargeType = Nothing

        'OBTENER LA ÚLTIMA SECCIÓN EN LA CUAL SE ESCRIBIRÁ
        section = doc.LastSection

        'SE DECIDE EL NÚMERO DE COLUMNAS Y SU ANCHO
        ReDim Preserve anchCol(0 To 15)
        anchCol(0) = 1 : anchCol(1) = 1 : anchCol(2) = 1 : anchCol(3) = 1 : anchCol(4) = 1 : anchCol(5) = 1 : anchCol(6) = 1 : anchCol(7) = 1
        anchCol(8) = 1 : anchCol(9) = 1 : anchCol(10) = 1 : anchCol(11) = 1 : anchCol(12) = 1 : anchCol(13) = 1 : anchCol(14) = 1 : anchCol(15) = 1

        'NÚMERO DE FILAS MÍNIMO QUE TENDRÁ LA TABLA
        nFil = 1 + item.charges.Count

        par = section.AddParagraph(CStr(oTextos(33)(1))) : Me.ponerFormatoParrafo(par, True, 8, True, Colors.LightBlue, , , True, True, , , , , True, True, True, True)
        par = section.AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, , , , , , , , , , , True, True, True, True)

        tab = Me.constTab(section, anchCol, nFil, Colors.White)

        colAux = IIf(UBound(anchCol) + 1 < tab.Columns.Count, 1, 0)

        Me.alinHorContTab(tab, CFacturaePDF.center) : Me.alinVerContTab(tab, CFacturaePDF.center)

        par = tab.Rows(0).Cells(0 + colAux).AddParagraph(UCase(CStr(oTextos(34)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        par = tab.Rows(0).Cells(10 + colAux).AddParagraph(UCase(CStr(oTextos(35)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        par = tab.Rows(0).Cells(12 + colAux).AddParagraph(UCase(CStr(oTextos(36)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        contadorDeRecargos = 0
        While contadorDeRecargos < item.charges.Count
            recargo = item.charges(contadorDeRecargos)
            If Not IsNothing(recargo) Then
                If Not IsNothing(recargo.chargeReason) Then
                    par = tab.Rows(1 + contadorDeRecargos).Cells(0 + colAux).AddParagraph(recargo.chargeReason) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                Else
                    par = tab.Rows(1 + contadorDeRecargos).Cells(0 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                End If

                If Not IsNothing(recargo.chargeRate) Then
                    par = tab.Rows(1 + contadorDeRecargos).Cells(10 + colAux).AddParagraph(dbl2str(str2Dbl(recargo.chargeRate.dValue), ".")) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                Else
                    par = tab.Rows(1 + contadorDeRecargos).Cells(10 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                End If
                If Not IsNothing(recargo.chargeAmount) Then
                    par = tab.Rows(1 + contadorDeRecargos).Cells(12 + colAux).AddParagraph(dbl2str(str2Dbl(recargo.chargeAmount.dValue), ".")) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                Else
                    par = tab.Rows(1 + contadorDeRecargos).Cells(12 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                End If
            End If
            contadorDeRecargos = contadorDeRecargos + 1
        End While

        If tab.Rows.Count > 1 Then
            tab.Rows(0).KeepWith = 1
        End If
        Me.bordeColumnas(tab, Colors.LightBlue, colAux = 1)
        Me.bordeFilas(tab, Colors.LightBlue, colAux = 1)
        Me.autoUnirCeldasDeTabla(tab, 0, colAux, CFacturaePDF.prioridadFilas, CFacturaePDF.prioridadColumnas)
    End Sub
    ''' <summary>
    ''' Imprime parte de la facturae contenida en el objeto CFacturae en el objeto que representa el documento PDF a crear.
    ''' Concretamente del item parámetro se imprimen sus descuentos.
    ''' </summary>
    ''' <param name="item">Un objeto que representa el item que se debe describir</param>
    ''' <param name="doc">Es el objeto que representa el documento PDF a crear</param>
    ''' <remarks>Llamado desde: CFacturaePDF.escribirItemImportes
    ''' Tiempo máximo: variable, depende del número de descuentos que tenga el item parámetro  
    ''' Revisado por: auv. 28/05/2012</remarks>
    Private Sub escribirItemImportesTablaDescuentos(ByRef item As ItemType, ByRef doc As MigraDoc.DocumentObjectModel.Document)
        'DECLARACIÓN DE LAS VARIABLES LOCALES
        Dim par As Paragraph = Nothing
        Dim section As Section = Nothing
        Dim tab As Table = Nothing
        Dim anchCol As Double() = Nothing
        Dim colAux As Integer = Nothing
        Dim nFil As Integer = Nothing
        Dim contadorDeDescuentos As Integer = Nothing
        Dim descuento As DiscountType = Nothing

        'OBTENER LA ÚLTIMA SECCIÓN EN LA CUAL SE ESCRIBIRÁ
        section = doc.LastSection

        'SE DECIDE EL NÚMERO DE COLUMNAS Y SU ANCHO
        ReDim Preserve anchCol(0 To 15)
        anchCol(0) = 1 : anchCol(1) = 1 : anchCol(2) = 1 : anchCol(3) = 1 : anchCol(4) = 1 : anchCol(5) = 1 : anchCol(6) = 1 : anchCol(7) = 1
        anchCol(8) = 1 : anchCol(9) = 1 : anchCol(10) = 1 : anchCol(11) = 1 : anchCol(12) = 1 : anchCol(13) = 1 : anchCol(14) = 1 : anchCol(15) = 1

        'NÚMERO DE FILAS MÍNIMO QUE TENDRÁ LA TABLA
        nFil = 1 + item.discountsAndRebates.Count

        par = section.AddParagraph(CStr(oTextos(37)(1))) : Me.ponerFormatoParrafo(par, True, 8, True, Colors.LightBlue, , , True, True, , , , , True, True, True, True)
        par = section.AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, , , , , , , , , , , True, True, True, True)

        tab = Me.constTab(section, anchCol, nFil, Colors.White)

        colAux = IIf(UBound(anchCol) + 1 < tab.Columns.Count, 1, 0)

        Me.alinHorContTab(tab, CFacturaePDF.center) : Me.alinVerContTab(tab, CFacturaePDF.center)

        par = tab.Rows(0).Cells(0 + colAux).AddParagraph(UCase(CStr(oTextos(34)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        par = tab.Rows(0).Cells(10 + colAux).AddParagraph(UCase(CStr(oTextos(35)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        par = tab.Rows(0).Cells(12 + colAux).AddParagraph(UCase(CStr(oTextos(36)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        contadorDeDescuentos = 0
        While contadorDeDescuentos < item.discountsAndRebates.Count
            descuento = item.discountsAndRebates(contadorDeDescuentos)
            If Not IsNothing(descuento) Then
                If Not IsNothing(descuento.discountReason) Then
                    par = tab.Rows(1 + contadorDeDescuentos).Cells(0 + colAux).AddParagraph(descuento.discountReason) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                Else
                    par = tab.Rows(1 + contadorDeDescuentos).Cells(0 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                End If

                If Not IsNothing(descuento.discountRate) Then
                    par = tab.Rows(1 + contadorDeDescuentos).Cells(10 + colAux).AddParagraph(dbl2str(str2Dbl(descuento.discountRate.dValue), ".")) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                Else
                    par = tab.Rows(1 + contadorDeDescuentos).Cells(10 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                End If
                If Not IsNothing(descuento.discountAmount) Then
                    par = tab.Rows(1 + contadorDeDescuentos).Cells(12 + colAux).AddParagraph(dbl2str(str2Dbl(descuento.discountAmount.dValue), ".")) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                Else
                    par = tab.Rows(1 + contadorDeDescuentos).Cells(12 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                End If
            End If
            contadorDeDescuentos = contadorDeDescuentos + 1
        End While

        If tab.Rows.Count > 1 Then
            tab.Rows(0).KeepWith = 1
        End If
        Me.bordeColumnas(tab, Colors.LightBlue, colAux = 1)
        Me.bordeFilas(tab, Colors.LightBlue, colAux = 1)
        Me.autoUnirCeldasDeTabla(tab, 0, colAux, CFacturaePDF.prioridadFilas, CFacturaePDF.prioridadColumnas)
    End Sub
    ''' <summary>
    ''' Imprime parte de la facturae contenida en el objeto CFacturae en el objeto que representa el documento PDF a crear.
    ''' Concretamente del item parámetro se imprimen su precio libre de impuestos, su coste total y su total bruto.
    ''' </summary>
    ''' <param name="item">Un objeto que representa el item que se debe describir</param>
    ''' <param name="doc">Es el objeto que representa el documento PDF a crear</param>
    ''' <remarks>Llamado desde: CFacturaePDF.escribirItemImportes
    ''' Tiempo máximo: menor a 0,1    
    ''' Revisado por: auv. 28/05/2012</remarks>
    Private Sub escribirItemImportesTablaImporteBruto(ByRef item As ItemType, ByRef doc As MigraDoc.DocumentObjectModel.Document)
        'DECLARACIÓN DE LAS VARIABLES LOCALES
        Dim par As Paragraph = Nothing
        Dim section As Section = Nothing
        Dim tab As Table = Nothing
        Dim anchCol As Double() = Nothing
        Dim colAux As Integer = Nothing
        Dim nFil As Integer = Nothing

        'OBTENER LA ÚLTIMA SECCIÓN EN LA CUAL SE ESCRIBIRÁ
        section = doc.LastSection

        'SE DECIDE EL NÚMERO DE COLUMNAS Y SU ANCHO
        ReDim Preserve anchCol(0 To 15)
        anchCol(0) = 1 : anchCol(1) = 1 : anchCol(2) = 1 : anchCol(3) = 1 : anchCol(4) = 1 : anchCol(5) = 1 : anchCol(6) = 1 : anchCol(7) = 1
        anchCol(8) = 1 : anchCol(9) = 1 : anchCol(10) = 1 : anchCol(11) = 1 : anchCol(12) = 1 : anchCol(13) = 1 : anchCol(14) = 1 : anchCol(15) = 1

        'NÚMERO DE FILAS MÍNIMO QUE TENDRÁ LA TABLA
        nFil = 3

        par = section.AddParagraph(CStr(oTextos(49)(1))) : Me.ponerFormatoParrafo(par, True, 8, True, Colors.LightBlue, , , True, True, , , , , True, True, True, True)
        par = section.AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, , , , , , , , , , , True, True, True, True)

        tab = Me.constTab(section, anchCol, nFil, Colors.White)

        colAux = IIf(UBound(anchCol) + 1 < tab.Columns.Count, 1, 0)

        Me.alinHorContTab(tab, CFacturaePDF.center) : Me.alinVerContTab(tab, CFacturaePDF.center)

        par = tab.Rows(0).Cells(0 + colAux).AddParagraph(UCase(CStr(oTextos(50)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        If Not IsNothing(item.unitPriceWithoutTax) Then
            par = tab.Rows(0).Cells(8 + colAux).AddParagraph(dbl2str(str2Dbl(item.unitPriceWithoutTax.dValue), ".")) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
        Else
            par = tab.Rows(0).Cells(8 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
        End If
        par = tab.Rows(1).Cells(0 + colAux).AddParagraph(UCase(CStr(oTextos(51)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        If Not IsNothing(item.totalCost) Then
            par = tab.Rows(1).Cells(8 + colAux).AddParagraph(dbl2str(str2Dbl(item.totalCost.dValue), ".")) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
        Else
            par = tab.Rows(1).Cells(8 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
        End If
        par = tab.Rows(2).Cells(0 + colAux).AddParagraph(UCase(CStr(oTextos(49)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        If Not IsNothing(item.grossAmount) Then
            par = tab.Rows(2).Cells(8 + colAux).AddParagraph(dbl2str(str2Dbl(item.grossAmount.dValue), ".")) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
        Else
            par = tab.Rows(2).Cells(8 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
        End If

        tab.Rows(0).KeepWith = tab.Rows.Count - 1
        Me.bordeColumnas(tab, Colors.LightBlue, colAux = 1)
        Me.bordeFilas(tab, Colors.LightBlue, colAux = 1)
        Me.autoUnirCeldasDeTabla(tab, 0, colAux, CFacturaePDF.prioridadFilas, CFacturaePDF.prioridadColumnas)
    End Sub
    ''' <summary>
    ''' Imprime parte de la facturae contenida en el objeto CFacturae en el objeto que representa el documento PDF a crear.
    ''' Concretamente del item parámetro se imprimen sus impuestos repercutidos.
    ''' </summary>
    ''' <param name="item">Un objeto que representa el item que se debe describir</param>
    ''' <param name="doc">Es el objeto que representa el documento PDF a crear</param>
    ''' <remarks>Llamado desde: CFacturaePDF.escribirItemImportes   
    ''' Tiempo máximo: variable, depende del número de impuestos repercutidos que tenga el item parámetro  
    ''' Revisado por: auv. 28/05/2012</remarks>
    Private Sub escribirItemImportesTablaImpuestosRepercutidos(ByRef item As ItemType, ByRef doc As MigraDoc.DocumentObjectModel.Document)
        'DECLARACIÓN DE LAS VARIABLES LOCALES
        Dim par As Paragraph = Nothing
        Dim section As Section = Nothing
        Dim tab As Table = Nothing
        Dim anchCol As Double() = Nothing
        Dim colAux As Integer = Nothing
        Dim nFil As Integer = Nothing
        Dim contadorDeImpuestosRepercutidos As Integer = Nothing
        Dim impuestoRepercutido As TaxOutputType = Nothing

        'OBTENER LA ÚLTIMA SECCIÓN EN LA CUAL SE ESCRIBIRÁ
        section = doc.LastSection

        'SE DECIDE EL NÚMERO DE COLUMNAS Y SU ANCHO
        ReDim Preserve anchCol(0 To 15)
        anchCol(0) = 1 : anchCol(1) = 1 : anchCol(2) = 1 : anchCol(3) = 1 : anchCol(4) = 1 : anchCol(5) = 1 : anchCol(6) = 1 : anchCol(7) = 1
        anchCol(8) = 1 : anchCol(9) = 1 : anchCol(10) = 1 : anchCol(11) = 1 : anchCol(12) = 1 : anchCol(13) = 1 : anchCol(14) = 1 : anchCol(15) = 1

        'NÚMERO DE FILAS MÍNIMO QUE TENDRÁ LA TABLA
        nFil = 1 + item.taxesOutputs.Count

        par = section.AddParagraph(CStr(oTextos(38)(1))) : Me.ponerFormatoParrafo(par, True, 8, True, Colors.LightBlue, , , True, True, , , , , True, True, True, True)
        par = section.AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, , , , , , , , , , , True, True, True, True)

        tab = Me.constTab(section, anchCol, nFil, Colors.White)

        colAux = IIf(UBound(anchCol) + 1 < tab.Columns.Count, 1, 0)

        Me.alinHorContTab(tab, CFacturaePDF.center) : Me.alinVerContTab(tab, CFacturaePDF.center)

        par = tab.Rows(0).Cells(0 + colAux).AddParagraph(UCase(CStr(oTextos(39)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        par = tab.Rows(0).Cells(4 + colAux).AddParagraph(UCase(CStr(oTextos(35)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        par = tab.Rows(0).Cells(8 + colAux).AddParagraph(UCase(CStr(oTextos(87)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        par = tab.Rows(0).Cells(12 + colAux).AddParagraph(UCase(CStr(oTextos(40)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        contadorDeImpuestosRepercutidos = 0
        While contadorDeImpuestosRepercutidos < item.taxesOutputs.Count
            impuestoRepercutido = item.taxesOutputs.ElementAt(contadorDeImpuestosRepercutidos)
            If Not IsNothing(impuestoRepercutido) Then
                If Not IsNothing(impuestoRepercutido.taxTypeCode) Then
                    par = tab.Rows(1 + contadorDeImpuestosRepercutidos).Cells(0 + colAux).AddParagraph(taxTypeCode_Obj2PDF(impuestoRepercutido.taxTypeCode.value)) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                Else
                    par = tab.Rows(1 + contadorDeImpuestosRepercutidos).Cells(0 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                End If
                If Not IsNothing(impuestoRepercutido.taxRate) Then
                    par = tab.Rows(1 + contadorDeImpuestosRepercutidos).Cells(4 + colAux).AddParagraph(dbl2str(str2Dbl(impuestoRepercutido.taxRate.dValue), ".")) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                Else
                    par = tab.Rows(1 + contadorDeImpuestosRepercutidos).Cells(4 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                End If
                If Not IsNothing(impuestoRepercutido.taxableBase) AndAlso Not IsNothing(impuestoRepercutido.taxableBase.totalAmount) Then
                    par = tab.Rows(1 + contadorDeImpuestosRepercutidos).Cells(8 + colAux).AddParagraph(dbl2str(str2Dbl(impuestoRepercutido.taxableBase.totalAmount.dValue), ".")) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                Else
                    par = tab.Rows(1 + contadorDeImpuestosRepercutidos).Cells(8 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                End If
                If Not IsNothing(impuestoRepercutido.taxAmount) AndAlso Not IsNothing(impuestoRepercutido.taxAmount.totalAmount) Then
                    par = tab.Rows(1 + contadorDeImpuestosRepercutidos).Cells(12 + colAux).AddParagraph(dbl2str(str2Dbl(impuestoRepercutido.taxAmount.totalAmount.dValue), ".")) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                Else
                    par = tab.Rows(1 + contadorDeImpuestosRepercutidos).Cells(12 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                End If
            End If
            contadorDeImpuestosRepercutidos = contadorDeImpuestosRepercutidos + 1
        End While

        If tab.Rows.Count > 1 Then
            tab.Rows(0).KeepWith = 1
        End If
        Me.bordeColumnas(tab, Colors.LightBlue, colAux = 1)
        Me.bordeFilas(tab, Colors.LightBlue, colAux = 1)
        Me.autoUnirCeldasDeTabla(tab, 0, colAux, CFacturaePDF.prioridadFilas, CFacturaePDF.prioridadColumnas)
    End Sub
    ''' <summary>
    ''' Imprime parte de la facturae contenida en el objeto CFacturae en el objeto que representa el documento PDF a crear.
    ''' Concretamente del item parámetro se imprimen sus impuestos retenidos.
    ''' </summary>
    ''' <param name="item">Un objeto que representa el item que se debe describir</param>
    ''' <param name="doc">Es el objeto que representa el documento PDF a crear</param>
    ''' <remarks>Llamado desde: CFacturaePDF.escribirItemImportes
    ''' Tiempo máximo: variable, depende del número de impuestos retenidos que tenga el item parámetro    
    ''' Revisado por: auv. 28/05/2012</remarks>
    Private Sub escribirItemImportesTablaImpuestosRetenidos(ByRef item As ItemType, ByRef doc As MigraDoc.DocumentObjectModel.Document)
        'DECLARACIÓN DE LAS VARIABLES LOCALES
        Dim par As Paragraph = Nothing
        Dim section As Section = Nothing
        Dim tab As Table = Nothing
        Dim anchCol As Double() = Nothing
        Dim colAux As Integer = Nothing
        Dim nFil As Integer = Nothing
        Dim contadorDeImpuestosRetenidos As Integer = Nothing
        Dim impuestoRetenido As TaxWithheldType = Nothing

        'OBTENER LA ÚLTIMA SECCIÓN EN LA CUAL SE ESCRIBIRÁ
        section = doc.LastSection

        'SE DECIDE EL NÚMERO DE COLUMNAS Y SU ANCHO
        ReDim Preserve anchCol(0 To 15)
        anchCol(0) = 1 : anchCol(1) = 1 : anchCol(2) = 1 : anchCol(3) = 1 : anchCol(4) = 1 : anchCol(5) = 1 : anchCol(6) = 1 : anchCol(7) = 1
        anchCol(8) = 1 : anchCol(9) = 1 : anchCol(10) = 1 : anchCol(11) = 1 : anchCol(12) = 1 : anchCol(13) = 1 : anchCol(14) = 1 : anchCol(15) = 1

        'NÚMERO DE FILAS MÍNIMO QUE TENDRÁ LA TABLA
        nFil = 1 + item.taxesWithheld.Count

        par = section.AddParagraph(CStr(oTextos(41)(1))) : Me.ponerFormatoParrafo(par, True, 8, True, Colors.LightBlue, , , True, True, , , , , True, True, True, True)
        par = section.AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, , , , , , , , , , , True, True, True, True)

        tab = Me.constTab(section, anchCol, nFil, Colors.White)

        colAux = IIf(UBound(anchCol) + 1 < tab.Columns.Count, 1, 0)

        Me.alinHorContTab(tab, CFacturaePDF.center) : Me.alinVerContTab(tab, CFacturaePDF.center)

        par = tab.Rows(0).Cells(0 + colAux).AddParagraph(UCase(CStr(oTextos(39)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        par = tab.Rows(0).Cells(4 + colAux).AddParagraph(UCase(CStr(oTextos(35)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        par = tab.Rows(0).Cells(8 + colAux).AddParagraph(UCase(CStr(oTextos(87)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        par = tab.Rows(0).Cells(12 + colAux).AddParagraph(UCase(CStr(oTextos(40)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )

        contadorDeImpuestosRetenidos = 0
        While contadorDeImpuestosRetenidos < item.taxesWithheld.Count
            impuestoRetenido = item.taxesWithheld.ElementAt(contadorDeImpuestosRetenidos)
            If Not IsNothing(impuestoRetenido) Then
                If Not IsNothing(impuestoRetenido.taxTypeCode) Then
                    par = tab.Rows(1 + contadorDeImpuestosRetenidos).Cells(0 + colAux).AddParagraph(taxTypeCode_Obj2PDF(impuestoRetenido.taxTypeCode.value)) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                Else
                    par = tab.Rows(1 + contadorDeImpuestosRetenidos).Cells(0 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                End If
                If Not IsNothing(impuestoRetenido.taxRate) Then
                    par = tab.Rows(1 + contadorDeImpuestosRetenidos).Cells(4 + colAux).AddParagraph(dbl2str(str2Dbl(impuestoRetenido.taxRate.dValue), ".")) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                Else
                    par = tab.Rows(1 + contadorDeImpuestosRetenidos).Cells(4 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                End If
                If Not IsNothing(impuestoRetenido.taxableBase) AndAlso Not IsNothing(impuestoRetenido.taxableBase.totalAmount) Then
                    par = tab.Rows(1 + contadorDeImpuestosRetenidos).Cells(8 + colAux).AddParagraph(dbl2str(str2Dbl(impuestoRetenido.taxableBase.totalAmount.dValue), ".")) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                Else
                    par = tab.Rows(1 + contadorDeImpuestosRetenidos).Cells(8 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                End If
                If Not IsNothing(impuestoRetenido.taxAmount) AndAlso Not IsNothing(impuestoRetenido.taxAmount.totalAmount) Then
                    par = tab.Rows(1 + contadorDeImpuestosRetenidos).Cells(12 + colAux).AddParagraph(dbl2str(str2Dbl(impuestoRetenido.taxAmount.totalAmount.dValue), ".")) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                Else
                    par = tab.Rows(1 + contadorDeImpuestosRetenidos).Cells(12 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                End If
            End If
            contadorDeImpuestosRetenidos = contadorDeImpuestosRetenidos + 1
        End While

        If tab.Rows.Count > 1 Then
            tab.Rows(0).KeepWith = 1
        End If
        bordeColumnas(tab, Colors.LightBlue, colAux = 1)
        bordeFilas(tab, Colors.LightBlue, colAux = 1)
        autoUnirCeldasDeTabla(tab, 0, colAux, CFacturaePDF.prioridadFilas, CFacturaePDF.prioridadColumnas)
    End Sub
    ''' <summary>
    ''' Imprime parte de la facturae contenida en el objeto CFacturae en el objeto que representa el documento PDF a crear.
    ''' Concretamente del item parámetro se imprimen su evento especial de impuesto.
    ''' </summary>
    ''' <param name="item">Un objeto que representa el item que se debe describir</param>
    ''' <param name="doc">Es el objeto que representa el documento PDF a crear</param>
    ''' <remarks>Llamado desde: CFacturaePDF.escribirItemDeFactura
    ''' Tiempo máximo: menor a 0,1  
    ''' Revisado por: auv. 05/06/2012</remarks>
    Private Sub escribirItemTablaEventoEspecialImpuesto(ByRef item As ItemType, ByRef doc As MigraDoc.DocumentObjectModel.Document)
        'DECLARACIÓN DE LAS VARIABLES LOCALES
        Dim par As Paragraph = Nothing
        Dim section As Section = Nothing
        Dim tab As Table = Nothing
        Dim anchCol As Double() = Nothing
        Dim colAux As Integer = Nothing
        Dim nFil As Integer = Nothing

        'OBTENER LA ÚLTIMA SECCIÓN EN LA CUAL SE ESCRIBIRÁ
        section = doc.LastSection

        'SE DECIDE EL NÚMERO DE COLUMNAS Y SU ANCHO
        ReDim Preserve anchCol(0 To 15)
        anchCol(0) = 1 : anchCol(1) = 1 : anchCol(2) = 1 : anchCol(3) = 1 : anchCol(4) = 1 : anchCol(5) = 1 : anchCol(6) = 1 : anchCol(7) = 1
        anchCol(8) = 1 : anchCol(9) = 1 : anchCol(10) = 1 : anchCol(11) = 1 : anchCol(12) = 1 : anchCol(13) = 1 : anchCol(14) = 1 : anchCol(15) = 1

        'NÚMERO DE FILAS MÍNIMO QUE TENDRÁ LA TABLA
        nFil = 5

        par = section.AddParagraph(CStr(oTextos(113)(1))) : Me.ponerFormatoParrafo(par, True, 8, True, Colors.LightBlue, , , True, True, , , , , True, True, True, True)
        par = section.AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, , , , , , , , , , , True, True, True, True)

        tab = Me.constTab(section, anchCol, nFil, Colors.White)

        colAux = IIf(UBound(anchCol) + 1 < tab.Columns.Count, 1, 0)

        Me.alinHorContTab(tab, CFacturaePDF.center) : Me.alinVerContTab(tab, CFacturaePDF.center)

        par = tab.Rows(0).Cells(0 + colAux).AddParagraph(UCase(CStr(oTextos(114)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        par = tab.Rows(1).Cells(0 + colAux).AddParagraph(UCase(CStr(oTextos(115)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        If Not IsNothing(item.specialTaxableEvent.specialTaxableEventCode) Then
            par = tab.Rows(2).Cells(0 + colAux).AddParagraph(CStr(item.specialTaxableEvent.specialTaxableEventCode.value)) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        Else
            par = tab.Rows(2).Cells(0 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        End If
        par = tab.Rows(3).Cells(0 + colAux).AddParagraph(UCase(CStr(oTextos(116)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        If Not IsNothing(item.specialTaxableEvent.specialTaxableEventReason) Then
            par = tab.Rows(4).Cells(0 + colAux).AddParagraph(CStr(item.specialTaxableEvent.specialTaxableEventReason)) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        Else
            par = tab.Rows(4).Cells(0 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        End If

        tab.Rows(0).KeepWith = tab.Rows.Count - 1
        bordeColumnas(tab, Colors.LightBlue, colAux = 1)
        bordeFilas(tab, Colors.LightBlue, colAux = 1)
        autoUnirCeldasDeTabla(tab, 0, colAux, CFacturaePDF.prioridadFilas, CFacturaePDF.prioridadColumnas)
    End Sub
    ''' <summary>
    ''' Imprime parte de la facturae contenida en el objeto CFacturae en el objeto que representa el documento PDF a crear.
    ''' Concretamente de la factura parámetro se imprimen sus cargos generales.
    ''' </summary>
    ''' <param name="factura">Un objeto que representa una factura de las contenidas en una facturae</param>
    ''' <param name="doc">Es el objeto que representa el documento PDF a crear</param>
    ''' <remarks>Llamado desde: CFacturaePDF.escribirFactura
    ''' Tiempo máximo: variable, depende del número de cargos generales que tenga la factura parámetro   
    ''' Revisado por: auv. 28/05/2012</remarks>
    Private Sub escribirCargosGenerales(ByRef factura As InvoiceType, ByRef doc As MigraDoc.DocumentObjectModel.Document)
        'DECLARACIÓN DE LAS VARIABLES LOCALES
        Dim par As Paragraph = Nothing
        Dim section As Section = Nothing
        Dim tab As Table = Nothing
        Dim anchCol As Double() = Nothing
        Dim colAux As Integer = Nothing
        Dim nFil As Integer = Nothing
        Dim contadorDeCargos As Integer = Nothing
        Dim cargo As ChargeType = Nothing

        'OBTENER LA ÚLTIMA SECCIÓN EN LA CUAL SE ESCRIBIRÁ
        section = doc.LastSection

        'SE DECIDE EL NÚMERO DE COLUMNAS Y SU ANCHO
        ReDim Preserve anchCol(0 To 15)
        anchCol(0) = 1 : anchCol(1) = 1 : anchCol(2) = 1 : anchCol(3) = 1 : anchCol(4) = 1 : anchCol(5) = 1 : anchCol(6) = 1 : anchCol(7) = 1
        anchCol(8) = 1 : anchCol(9) = 1 : anchCol(10) = 1 : anchCol(11) = 1 : anchCol(12) = 1 : anchCol(13) = 1 : anchCol(14) = 1 : anchCol(15) = 1

        'NÚMERO DE FILAS MÍNIMO QUE TENDRÁ LA TABLA
        nFil = 1 + factura.invoiceTotals.generalSurcharges.Count

        par = section.AddParagraph(CStr(oTextos(52)(1))) : Me.ponerFormatoParrafo(par, True, 8, True, Colors.LightBlue, , , True, True, , , , , True, True, True, True)
        par = section.AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, , , , , , , , , , , True, True, True, True)

        tab = Me.constTab(section, anchCol, nFil, Colors.White)

        colAux = IIf(UBound(anchCol) + 1 < tab.Columns.Count, 1, 0)

        Me.alinHorContTab(tab, CFacturaePDF.center) : Me.alinVerContTab(tab, CFacturaePDF.center)

        par = tab.Rows(0).Cells(0 + colAux).AddParagraph(UCase(CStr(oTextos(34)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        par = tab.Rows(0).Cells(10 + colAux).AddParagraph(UCase(CStr(oTextos(35)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        par = tab.Rows(0).Cells(12 + colAux).AddParagraph(UCase(CStr(oTextos(36)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        contadorDeCargos = 0
        While contadorDeCargos < factura.invoiceTotals.generalSurcharges.Count
            cargo = factura.invoiceTotals.generalSurcharges(contadorDeCargos)
            If Not IsNothing(cargo) AndAlso Not IsNothing(cargo.chargeReason) Then
                par = tab.Rows(1 + contadorDeCargos).Cells(0 + colAux).AddParagraph(cargo.chargeReason) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
            Else
                par = tab.Rows(1 + contadorDeCargos).Cells(0 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
            End If
            If Not IsNothing(cargo) AndAlso Not IsNothing(cargo.chargeRate) Then
                par = tab.Rows(1 + contadorDeCargos).Cells(10 + colAux).AddParagraph(dbl2str(str2Dbl(cargo.chargeRate.dValue), ".")) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
            Else
                par = tab.Rows(1 + contadorDeCargos).Cells(10 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
            End If
            If Not IsNothing(cargo) AndAlso Not IsNothing(cargo.chargeAmount) Then
                par = tab.Rows(1 + contadorDeCargos).Cells(12 + colAux).AddParagraph(dbl2str(str2Dbl(cargo.chargeAmount.dValue), ".")) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
            Else
                par = tab.Rows(1 + contadorDeCargos).Cells(12 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
            End If
            contadorDeCargos = contadorDeCargos + 1
        End While

        If tab.Rows.Count > 1 Then
            tab.Rows(0).KeepWith = 1
        End If
        Me.bordeColumnas(tab, Colors.LightBlue, colAux = 1)
        Me.bordeFilas(tab, Colors.LightBlue, colAux = 1)
        Me.autoUnirCeldasDeTabla(tab, 0, colAux, CFacturaePDF.prioridadFilas, CFacturaePDF.prioridadColumnas)
    End Sub
    ''' <summary>
    ''' Imprime parte de la facturae contenida en el objeto CFacturae en el objeto que representa el documento PDF a crear.
    ''' Concretamente de la factura parámetro se imprimen sus descuentos generales.
    ''' </summary>
    ''' <param name="factura">Un objeto que representa una factura de las contenidas en una facturae</param>
    ''' <param name="doc">Es el objeto que representa el documento PDF a crear</param>
    ''' <remarks>Llamado desde: CFacturaePDF.escribirFactura
    ''' Tiempo máximo: variable, depende del número de descuentos generales que tenga la factura parámetro    
    ''' Revisado por: auv. 28/05/2012</remarks>
    Private Sub escribirDescuentosGenerales(ByRef factura As InvoiceType, ByRef doc As MigraDoc.DocumentObjectModel.Document)
        'DECLARACIÓN DE LAS VARIABLES LOCALES
        Dim par As Paragraph = Nothing
        Dim section As Section = Nothing
        Dim tab As Table = Nothing
        Dim anchCol As Double() = Nothing
        Dim colAux As Integer = Nothing
        Dim nFil As Integer = Nothing
        Dim contadorDeDescuentos As Integer = Nothing
        Dim descuento As DiscountType = Nothing

        'OBTENER LA ÚLTIMA SECCIÓN EN LA CUAL SE ESCRIBIRÁ
        section = doc.LastSection

        'SE DECIDE EL NÚMERO DE COLUMNAS Y SU ANCHO
        ReDim Preserve anchCol(0 To 15)
        anchCol(0) = 1 : anchCol(1) = 1 : anchCol(2) = 1 : anchCol(3) = 1 : anchCol(4) = 1 : anchCol(5) = 1 : anchCol(6) = 1 : anchCol(7) = 1
        anchCol(8) = 1 : anchCol(9) = 1 : anchCol(10) = 1 : anchCol(11) = 1 : anchCol(12) = 1 : anchCol(13) = 1 : anchCol(14) = 1 : anchCol(15) = 1

        'NÚMERO DE FILAS MÍNIMO QUE TENDRÁ LA TABLA
        nFil = 1 + factura.invoiceTotals.generalDiscounts.Count

        par = section.AddParagraph(CStr(oTextos(53)(1))) : Me.ponerFormatoParrafo(par, True, 8, True, Colors.LightBlue, , , True, True, , , , , True, True, True, True)
        par = section.AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, , , , , , , , , , , True, True, True, True)

        tab = Me.constTab(section, anchCol, nFil, Colors.White)

        colAux = IIf(UBound(anchCol) + 1 < tab.Columns.Count, 1, 0)

        Me.alinHorContTab(tab, CFacturaePDF.center) : Me.alinVerContTab(tab, CFacturaePDF.center)

        par = tab.Rows(0).Cells(0 + colAux).AddParagraph(UCase(CStr(oTextos(34)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        par = tab.Rows(0).Cells(10 + colAux).AddParagraph(UCase(CStr(oTextos(35)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        par = tab.Rows(0).Cells(12 + colAux).AddParagraph(UCase(CStr(oTextos(36)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        contadorDeDescuentos = 0
        While contadorDeDescuentos < factura.invoiceTotals.generalDiscounts.Count
            descuento = factura.invoiceTotals.generalDiscounts(contadorDeDescuentos)
            If Not IsNothing(descuento.discountReason) AndAlso Not IsNothing(descuento.discountReason) Then
                par = tab.Rows(1 + contadorDeDescuentos).Cells(0 + colAux).AddParagraph(descuento.discountReason) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
            Else
                par = tab.Rows(1 + contadorDeDescuentos).Cells(0 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
            End If
            If Not IsNothing(descuento.discountReason) AndAlso Not IsNothing(descuento.discountRate) Then
                par = tab.Rows(1 + contadorDeDescuentos).Cells(10 + colAux).AddParagraph(dbl2str(str2Dbl(descuento.discountRate.dValue), ".")) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
            Else
                par = tab.Rows(1 + contadorDeDescuentos).Cells(10 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
            End If
            If Not IsNothing(descuento.discountReason) AndAlso Not IsNothing(descuento.discountAmount) Then
                par = tab.Rows(1 + contadorDeDescuentos).Cells(12 + colAux).AddParagraph(dbl2str(str2Dbl(descuento.discountAmount.dValue), ".")) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
            Else
                par = tab.Rows(1 + contadorDeDescuentos).Cells(12 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
            End If
            contadorDeDescuentos = contadorDeDescuentos + 1
        End While

        If tab.Rows.Count > 1 Then
            tab.Rows(0).KeepWith = 1
        End If
        Me.bordeColumnas(tab, Colors.LightBlue, colAux = 1)
        Me.bordeFilas(tab, Colors.LightBlue, colAux = 1)
        Me.autoUnirCeldasDeTabla(tab, 0, colAux, CFacturaePDF.prioridadFilas, CFacturaePDF.prioridadColumnas)
    End Sub
    ''' <summary>
    ''' Imprime parte de la facturae contenida en el objeto CFacturae en el objeto que representa el documento PDF a crear.
    ''' Concretamente de la factura parámetro se imprimen sus impuestos repercutidos.
    ''' </summary>
    ''' <param name="factura">Un objeto que representa una factura de las contenidas en una facturae</param>
    ''' <param name="doc">Es el objeto que representa el documento PDF a crear</param>
    ''' <remarks>Llamado desde: CFacturaePDF.escribirFactura
    ''' Tiempo máximo: variable, depende del número de impuestos repercutidos que tenga el item parámetro   
    ''' Revisado por: auv. 28/05/2012</remarks>
    Private Sub escribirImpuestosRepercutidos(ByRef factura As InvoiceType, ByRef doc As MigraDoc.DocumentObjectModel.Document)
        'DECLARACIÓN DE LAS VARIABLES LOCALES
        Dim par As Paragraph = Nothing
        Dim section As Section = Nothing
        Dim tab As Table = Nothing
        Dim anchCol As Double() = Nothing
        Dim colAux As Integer = Nothing
        Dim nFil As Integer = Nothing
        Dim contadorDeImpuestosRepercutidos As Integer = Nothing
        Dim impuestoRepercutido As TaxOutputType = Nothing
        Dim impuestosRepercutidosAgrupados() As TaxOutputType = Nothing

        'OBTENER LA ÚLTIMA SECCIÓN EN LA CUAL SE ESCRIBIRÁ
        section = doc.LastSection

        'SE DECIDE EL NÚMERO DE COLUMNAS Y SU ANCHO
        ReDim Preserve anchCol(0 To 15)
        anchCol(0) = 1 : anchCol(1) = 1 : anchCol(2) = 1 : anchCol(3) = 1 : anchCol(4) = 1 : anchCol(5) = 1 : anchCol(6) = 1 : anchCol(7) = 1
        anchCol(8) = 1 : anchCol(9) = 1 : anchCol(10) = 1 : anchCol(11) = 1 : anchCol(12) = 1 : anchCol(13) = 1 : anchCol(14) = 1 : anchCol(15) = 1

        impuestosRepercutidosAgrupados = Me.agruparImpuestosRepercutidos(factura.taxesOutputs)
        If Not IsNothing(impuestosRepercutidosAgrupados) Then
            'NÚMERO DE FILAS MÍNIMO QUE TENDRÁ LA TABLA
            nFil = 1 + impuestosRepercutidosAgrupados.Count

            par = section.AddParagraph(CStr(oTextos(38)(1))) : Me.ponerFormatoParrafo(par, True, 8, True, Colors.LightBlue, , , True, True, , , , , True, True, True, True)
            par = section.AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, , , , , , , , , , , True, True, True, True)

            tab = Me.constTab(section, anchCol, nFil, Colors.White)

            colAux = IIf(UBound(anchCol) + 1 < tab.Columns.Count, 1, 0)

            Me.alinHorContTab(tab, CFacturaePDF.center) : Me.alinVerContTab(tab, CFacturaePDF.center)

            par = tab.Rows(0).Cells(0 + colAux).AddParagraph(UCase(CStr(oTextos(39)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
            par = tab.Rows(0).Cells(4 + colAux).AddParagraph(UCase(CStr(oTextos(35)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
            par = tab.Rows(0).Cells(8 + colAux).AddParagraph(UCase(CStr(oTextos(87)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
            par = tab.Rows(0).Cells(12 + colAux).AddParagraph(UCase(CStr(oTextos(40)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )

            contadorDeImpuestosRepercutidos = 0
            While contadorDeImpuestosRepercutidos < impuestosRepercutidosAgrupados.Count
                impuestoRepercutido = impuestosRepercutidosAgrupados.ElementAt(contadorDeImpuestosRepercutidos)
                If Not IsNothing(impuestoRepercutido) AndAlso Not IsNothing(impuestoRepercutido.taxTypeCode) Then
                    par = tab.Rows(1 + contadorDeImpuestosRepercutidos).Cells(0 + colAux).AddParagraph(taxTypeCode_Obj2PDF(impuestoRepercutido.taxTypeCode.value)) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                Else
                    par = tab.Rows(1 + contadorDeImpuestosRepercutidos).Cells(0 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                End If
                If Not IsNothing(impuestoRepercutido) AndAlso Not IsNothing(impuestoRepercutido.taxRate) Then
                    par = tab.Rows(1 + contadorDeImpuestosRepercutidos).Cells(4 + colAux).AddParagraph(dbl2str(str2Dbl(impuestoRepercutido.taxRate.dValue), ".")) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                Else
                    par = tab.Rows(1 + contadorDeImpuestosRepercutidos).Cells(4 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                End If
                If Not IsNothing(impuestoRepercutido) AndAlso Not IsNothing(impuestoRepercutido.taxableBase) AndAlso _
                    Not IsNothing(impuestoRepercutido.taxableBase.totalAmount) Then
                    par = tab.Rows(1 + contadorDeImpuestosRepercutidos).Cells(8 + colAux).AddParagraph(dbl2str(str2Dbl(impuestoRepercutido.taxableBase.totalAmount.dValue), ".")) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                Else
                    par = tab.Rows(1 + contadorDeImpuestosRepercutidos).Cells(8 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                End If
                If Not IsNothing(impuestoRepercutido) AndAlso Not IsNothing(impuestoRepercutido.taxAmount) AndAlso Not IsNothing(impuestoRepercutido.taxAmount.totalAmount) Then
                    par = tab.Rows(1 + contadorDeImpuestosRepercutidos).Cells(12 + colAux).AddParagraph(dbl2str(str2Dbl(impuestoRepercutido.taxAmount.totalAmount.dValue), ".")) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                Else
                    par = tab.Rows(1 + contadorDeImpuestosRepercutidos).Cells(12 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                End If
                contadorDeImpuestosRepercutidos = contadorDeImpuestosRepercutidos + 1
            End While

            If tab.Rows.Count > 1 Then
                tab.Rows(0).KeepWith = 1
            End If
            Me.bordeColumnas(tab, Colors.LightBlue, colAux = 1)
            Me.bordeFilas(tab, Colors.LightBlue, colAux = 1)
            Me.autoUnirCeldasDeTabla(tab, 0, colAux, CFacturaePDF.prioridadFilas, CFacturaePDF.prioridadColumnas)
        End If
    End Sub
    ''' <summary>
    ''' Imprime parte de la facturae contenida en el objeto CFacturae en el objeto que representa el documento PDF a crear.
    ''' Concretamente de la factura parámetro se imprimen sus impuestos retenidos
    ''' </summary>
    ''' <param name="factura">Un objeto que representa una factura de las contenidas en una facturae</param>
    ''' <param name="doc">Es el objeto que representa el documento PDF a crear</param>
    ''' <remarks>Llamado desde: CFacturaePDF.escribirFactura
    ''' Tiempo máximo: variable, depende del número de impuestos retenidos que tenga el item parámetro    
    ''' Revisado por: auv. 28/05/2012</remarks>
    Private Sub escribirImpuestosRetenidos(ByRef factura As InvoiceType, ByRef doc As MigraDoc.DocumentObjectModel.Document)
        'DECLARACIÓN DE LAS VARIABLES LOCALES
        Dim par As Paragraph = Nothing
        Dim section As Section = Nothing
        Dim tab As Table = Nothing
        Dim anchCol As Double() = Nothing
        Dim colAux As Integer = Nothing
        Dim nFil As Integer = Nothing
        Dim contadorDeImpuestosRetenidos As Integer = Nothing
        Dim impuestoRetenido As TaxWithheldType = Nothing
        Dim impuestosRetenidosAgrupados() As TaxWithheldType = Nothing

        'OBTENER LA ÚLTIMA SECCIÓN EN LA CUAL SE ESCRIBIRÁ
        section = doc.LastSection

        'SE DECIDE EL NÚMERO DE COLUMNAS Y SU ANCHO
        ReDim Preserve anchCol(0 To 15)
        anchCol(0) = 1 : anchCol(1) = 1 : anchCol(2) = 1 : anchCol(3) = 1 : anchCol(4) = 1 : anchCol(5) = 1 : anchCol(6) = 1 : anchCol(7) = 1
        anchCol(8) = 1 : anchCol(9) = 1 : anchCol(10) = 1 : anchCol(11) = 1 : anchCol(12) = 1 : anchCol(13) = 1 : anchCol(14) = 1 : anchCol(15) = 1

        impuestosRetenidosAgrupados = Me.agruparImpuestosRetenidos(factura.taxesWithheld)
        If Not IsNothing(impuestosRetenidosAgrupados) Then
            'NÚMERO DE FILAS MÍNIMO QUE TENDRÁ LA TABLA
            nFil = 1 + impuestosRetenidosAgrupados.Count

            par = section.AddParagraph(CStr(oTextos(41)(1))) : Me.ponerFormatoParrafo(par, True, 8, True, Colors.LightBlue, , , True, True, , , , , True, True, True, True)
            par = section.AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, , , , , , , , , , , True, True, True, True)

            tab = Me.constTab(section, anchCol, nFil, Colors.White)

            colAux = IIf(UBound(anchCol) + 1 < tab.Columns.Count, 1, 0)

            Me.alinHorContTab(tab, CFacturaePDF.center) : Me.alinVerContTab(tab, CFacturaePDF.center)

            par = tab.Rows(0).Cells(0 + colAux).AddParagraph(UCase(CStr(oTextos(39)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
            par = tab.Rows(0).Cells(4 + colAux).AddParagraph(UCase(CStr(oTextos(35)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
            par = tab.Rows(0).Cells(8 + colAux).AddParagraph(UCase(CStr(oTextos(87)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
            par = tab.Rows(0).Cells(12 + colAux).AddParagraph(UCase(CStr(oTextos(40)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )

            contadorDeImpuestosRetenidos = 0
            While contadorDeImpuestosRetenidos < impuestosRetenidosAgrupados.Count
                impuestoRetenido = impuestosRetenidosAgrupados.ElementAt(contadorDeImpuestosRetenidos)
                If Not IsNothing(impuestoRetenido) AndAlso Not IsNothing(impuestoRetenido.taxTypeCode) Then
                    par = tab.Rows(1 + contadorDeImpuestosRetenidos).Cells(0 + colAux).AddParagraph(taxTypeCode_Obj2PDF(impuestoRetenido.taxTypeCode.value)) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                Else
                    par = tab.Rows(1 + contadorDeImpuestosRetenidos).Cells(0 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                End If
                If Not IsNothing(impuestoRetenido) AndAlso Not IsNothing(impuestoRetenido.taxRate) Then
                    par = tab.Rows(1 + contadorDeImpuestosRetenidos).Cells(4 + colAux).AddParagraph(dbl2str(str2Dbl(impuestoRetenido.taxRate.dValue), ".")) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                Else
                    par = tab.Rows(1 + contadorDeImpuestosRetenidos).Cells(4 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                End If
                If Not IsNothing(impuestoRetenido) AndAlso Not IsNothing(impuestoRetenido.taxableBase) AndAlso Not IsNothing(impuestoRetenido.taxableBase.totalAmount) Then
                    par = tab.Rows(1 + contadorDeImpuestosRetenidos).Cells(8 + colAux).AddParagraph(dbl2str(str2Dbl(impuestoRetenido.taxableBase.totalAmount.dValue), ".")) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                Else
                    par = tab.Rows(1 + contadorDeImpuestosRetenidos).Cells(8 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                End If
                If Not IsNothing(impuestoRetenido) AndAlso Not IsNothing(impuestoRetenido.taxAmount) AndAlso Not IsNothing(impuestoRetenido.taxAmount.totalAmount) Then
                    par = tab.Rows(1 + contadorDeImpuestosRetenidos).Cells(12 + colAux).AddParagraph(dbl2str(str2Dbl(impuestoRetenido.taxAmount.totalAmount.dValue), ".")) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                Else
                    par = tab.Rows(1 + contadorDeImpuestosRetenidos).Cells(12 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
                End If
                contadorDeImpuestosRetenidos = contadorDeImpuestosRetenidos + 1
            End While

            If tab.Rows.Count > 1 Then
                tab.Rows(0).KeepWith = 1
            End If
            Me.bordeColumnas(tab, Colors.LightBlue, colAux = 1)
            Me.bordeFilas(tab, Colors.LightBlue, colAux = 1)
            Me.autoUnirCeldasDeTabla(tab, 0, colAux, CFacturaePDF.prioridadFilas, CFacturaePDF.prioridadColumnas)
        End If
    End Sub
    ''' <summary>
    ''' Imprime parte de la facturae contenida en el objeto CFacturae en el objeto que representa el documento PDF a crear.
    ''' Concretamente de la factura parámetro se imprimen sus retenciones de garantía
    ''' </summary>
    ''' <param name="factura">Un objeto que representa una factura de las contenidas en una facturae</param>
    ''' <param name="doc">Es el objeto que representa el documento PDF a crear</param>
    ''' <remarks>Llamado desde: CFacturaePDF.escribirFactura
    ''' Tiempo máximo: menor a 0,1    
    ''' Revisado por: auv. 28/05/2012</remarks>
    Private Sub escribirRetencionesDeGarantia(ByRef factura As InvoiceType, ByRef doc As MigraDoc.DocumentObjectModel.Document)
        'DECLARACIÓN DE LAS VARIABLES LOCALES
        Dim par As Paragraph = Nothing
        Dim section As Section = Nothing
        Dim tab As Table = Nothing
        Dim anchCol As Double() = Nothing
        Dim colAux As Integer = Nothing
        Dim nFil As Integer = Nothing

        'OBTENER LA ÚLTIMA SECCIÓN EN LA CUAL SE ESCRIBIRÁ
        section = doc.LastSection

        'SE DECIDE EL NÚMERO DE COLUMNAS Y SU ANCHO
        ReDim Preserve anchCol(0 To 15)
        anchCol(0) = 1 : anchCol(1) = 1 : anchCol(2) = 1 : anchCol(3) = 1 : anchCol(4) = 1 : anchCol(5) = 1 : anchCol(6) = 1 : anchCol(7) = 1
        anchCol(8) = 1 : anchCol(9) = 1 : anchCol(10) = 1 : anchCol(11) = 1 : anchCol(12) = 1 : anchCol(13) = 1 : anchCol(14) = 1 : anchCol(15) = 1

        'NÚMERO DE FILAS MÍNIMO QUE TENDRÁ LA TABLA
        nFil = 2

        par = section.AddParagraph(CStr(oTextos(54)(1))) : Me.ponerFormatoParrafo(par, True, 8, True, Colors.LightBlue, , , True, True, , , , , True, True, True, True)
        par = section.AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, , , , , , , , , , , True, True, True, True)

        tab = Me.constTab(section, anchCol, nFil, Colors.White)

        colAux = IIf(UBound(anchCol) + 1 < tab.Columns.Count, 1, 0)

        Me.alinHorContTab(tab, CFacturaePDF.center) : Me.alinVerContTab(tab, CFacturaePDF.center)

        par = tab.Rows(0).Cells(0 + colAux).AddParagraph(UCase(CStr(oTextos(35)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        If Not IsNothing(factura.invoiceTotals.amountsWithheld.withholdingRate) Then
            par = tab.Rows(1).Cells(0 + colAux).AddParagraph(dbl2str(str2Dbl(factura.invoiceTotals.amountsWithheld.withholdingRate.dValue), ".")) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
        Else
            par = tab.Rows(1).Cells(0 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
        End If
        par = tab.Rows(0).Cells(8 + colAux).AddParagraph(UCase(CStr(oTextos(36)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
        If Not IsNothing(factura.invoiceTotals.amountsWithheld.withholdingAmount) Then
            par = tab.Rows(1).Cells(8 + colAux).AddParagraph(dbl2str(str2Dbl(factura.invoiceTotals.amountsWithheld.withholdingAmount.dValue), ".")) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
        Else
            par = tab.Rows(1).Cells(8 + colAux).AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
        End If

        tab.Rows(0).KeepWith = tab.Rows.Count - 1
        Me.bordeColumnas(tab, Colors.LightBlue, colAux = 1)
        Me.bordeFilas(tab, Colors.LightBlue, colAux = 1)
        Me.autoUnirCeldasDeTabla(tab, 0, colAux, CFacturaePDF.prioridadFilas, CFacturaePDF.prioridadColumnas)
    End Sub
    ''' <summary>
    ''' Imprime parte de la facturae contenida en el objeto CFacturae en el objeto que representa el documento PDF a crear.
    ''' Concretamente de la factura parámetro se imprime el dato adicional de información adicional de la factura.
    ''' </summary>
    ''' <param name="factura">Un objeto que representa una factura de las contenidas en una facturae</param>
    ''' <param name="doc">Es el objeto que representa el documento PDF a crear</param>
    ''' <remarks>Llamado desde: CFacturaePDF.escribirFactura
    ''' Tiempo máximo: menor a 0,1    
    ''' Revisado por: auv. 28/05/2012</remarks>
    Private Sub escribirDatosAdicionales(ByRef factura As InvoiceType, ByRef doc As MigraDoc.DocumentObjectModel.Document)
        'DECLARACIÓN DE LAS VARIABLES LOCALES
        Dim par As Paragraph = Nothing
        Dim section As Section = Nothing
        Dim tab As Table = Nothing
        Dim anchCol As Double() = Nothing
        Dim colAux As Integer = Nothing
        Dim nFil As Integer = Nothing
        Dim hayObservaciones As Boolean = Nothing

        'OBTENER LA ÚLTIMA SECCIÓN EN LA CUAL SE ESCRIBIRÁ
        section = doc.LastSection

        'SE DECIDE EL NÚMERO DE COLUMNAS Y SU ANCHO
        ReDim Preserve anchCol(0 To 15)
        anchCol(0) = 1 : anchCol(1) = 1 : anchCol(2) = 1 : anchCol(3) = 1 : anchCol(4) = 1 : anchCol(5) = 1 : anchCol(6) = 1 : anchCol(7) = 1
        anchCol(8) = 1 : anchCol(9) = 1 : anchCol(10) = 1 : anchCol(11) = 1 : anchCol(12) = 1 : anchCol(13) = 1 : anchCol(14) = 1 : anchCol(15) = 1

        'NÚMERO DE FILAS MÍNIMO QUE TENDRÁ LA TABLA
        nFil = 0

        'SE EXAMINA QUÉ DATOS NO OBLIGATORIOS ESTÁN PRESENTES
        If Not IsNothing(factura.additionalData.invoiceAdditionalInformation) Then
            hayObservaciones = True
        Else
            hayObservaciones = False
        End If

        nFil = IIf(hayObservaciones, nFil + 2, nFil)

        If hayObservaciones Then
            par = section.AddParagraph(CStr(oTextos(55)(1))) : Me.ponerFormatoParrafo(par, True, 8, True, Colors.LightBlue, , , True, True, , , , , True, True, True, True)
            par = section.AddParagraph("") : Me.ponerFormatoParrafo(par, True, 6, , , , , , , , , , , True, True, True, True)

            tab = Me.constTab(section, anchCol, nFil, Colors.White)

            colAux = IIf(UBound(anchCol) + 1 < tab.Columns.Count, 1, 0)

            Me.alinHorContTab(tab, CFacturaePDF.center) : Me.alinVerContTab(tab, CFacturaePDF.center)
            If Not IsNothing(factura.additionalData.invoiceAdditionalInformation) AndAlso hayObservaciones Then
                par = tab.Rows(0).Cells(0 + colAux).AddParagraph(UCase(CStr(oTextos(56)(1)))) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, True, True, , , , , , , , , , )
                par = tab.Rows(1).Cells(0 + colAux).AddParagraph(factura.additionalData.invoiceAdditionalInformation) : Me.ponerFormatoParrafo(par, True, 6, True, Colors.DarkBlue, , , , , , , , , , , , )
            End If

            tab.Rows(0).KeepWith = tab.Rows.Count - 1
            Me.bordeColumnas(tab, Colors.LightBlue, colAux = 1)
            Me.bordeFilas(tab, Colors.LightBlue, colAux = 1)
            Me.autoUnirCeldasDeTabla(tab, 0, colAux, CFacturaePDF.prioridadFilas, CFacturaePDF.prioridadColumnas)
        End If
    End Sub
    ''' <summary>
    ''' Esta función se encarga de agrupar un vector de impuestos repercutidos. Dos impuestos repercutidos se pueden agrupar
    ''' si y solo si coinciden sus valores de tasa de impuesto, tipo de impuesto y porcentaje de recargo de equivalencia
    ''' </summary>
    ''' <param name="impuestosRepercutidos">Vector de objetos que representan impuestos repercutidos, que se debe agrupar</param>
    ''' <returns>Vector de objetos que representan impuestos repercutidos, ya agrupados</returns>
    ''' <remarks>Llamado desde: CFacturaePDF.escribirAlbaranTablaImpuestosRepercutidos, CFacturaePDF.escribirImpuestosRepercutidos
    ''' Tiempo máximo: variable, depende del número de impuestos repercutidos   
    ''' Revisado por: auv. 28/05/2012</remarks>
    Private Function agruparImpuestosRepercutidos(ByRef impuestosRepercutidos() As TaxOutputType) As TaxOutputType()
        Dim impuestosRepercutidosAgrupados() As TaxOutputType = Nothing
        Dim numeroDeImpuestosAgrupados As Integer = Nothing
        Dim contadorDeImpuestos As Integer = Nothing
        Dim contadorDeImpuestosAgrupados As Integer = Nothing
        Dim agrupar As Boolean = Nothing
        Dim impuesto As TaxOutputType = Nothing
        Dim impuestoAgrupado As TaxOutputType = Nothing

        numeroDeImpuestosAgrupados = 0
        contadorDeImpuestos = 0
        While contadorDeImpuestos < impuestosRepercutidos.Count
            impuesto = impuestosRepercutidos(contadorDeImpuestos)
            If Not IsNothing(impuesto) Then
                agrupar = False
                contadorDeImpuestosAgrupados = 0
                While (Not (agrupar)) And contadorDeImpuestosAgrupados < numeroDeImpuestosAgrupados
                    impuestoAgrupado = impuestosRepercutidosAgrupados(contadorDeImpuestosAgrupados)
                    If Not IsNothing(impuestoAgrupado) Then
                        If Not IsNothing(impuesto.taxTypeCode) AndAlso Not IsNothing(impuestoAgrupado.taxTypeCode) AndAlso Not IsNothing(impuesto.taxRate) AndAlso _
                             Not IsNothing(impuestoAgrupado.taxRate) AndAlso impuesto.taxTypeCode.value = impuestoAgrupado.taxTypeCode.value AndAlso _
                             str2Dbl(impuesto.taxRate.dValue) = str2Dbl(impuestoAgrupado.taxRate.dValue) Then
                            If IsNothing(impuestoAgrupado.equivalenceSurcharge) Then
                                If IsNothing(impuesto.equivalenceSurcharge) Then
                                    agrupar = True
                                Else
                                    contadorDeImpuestosAgrupados = contadorDeImpuestosAgrupados + 1
                                End If
                            Else
                                If IsNothing(impuesto.equivalenceSurcharge) Then
                                    contadorDeImpuestosAgrupados = contadorDeImpuestosAgrupados + 1
                                Else
                                    If str2Dbl(impuestoAgrupado.equivalenceSurcharge.dValue) = str2Dbl(impuesto.equivalenceSurcharge.dValue) Then
                                        agrupar = True
                                    Else
                                        contadorDeImpuestosAgrupados = contadorDeImpuestosAgrupados + 1
                                    End If
                                End If
                            End If
                        Else
                            contadorDeImpuestosAgrupados = contadorDeImpuestosAgrupados + 1
                        End If
                    Else
                        contadorDeImpuestosAgrupados = contadorDeImpuestosAgrupados + 1
                    End If
                End While
                If agrupar Then
                    If Not IsNothing(impuestoAgrupado) AndAlso Not IsNothing(impuestoAgrupado.taxableBase) AndAlso _
                        Not IsNothing(impuestoAgrupado.taxableBase.totalAmount) AndAlso Not IsNothing(impuesto.taxableBase) AndAlso _
                        Not IsNothing(impuesto.taxableBase.totalAmount) Then
                        impuestoAgrupado.taxableBase.totalAmount = dbl2str(str2Dbl(impuestoAgrupado.taxableBase.totalAmount.dValue) + str2Dbl(impuesto.taxableBase.totalAmount.dValue), ",")
                    End If
                    If Not IsNothing(impuestoAgrupado) AndAlso Not IsNothing(impuesto.taxableBase) Then
                        If Not IsNothing(impuestoAgrupado.taxableBase.equivalentInEuros) Then
                            If Not IsNothing(impuesto.taxableBase) AndAlso Not (IsNothing(impuesto.taxableBase.equivalentInEuros)) Then
                                impuestoAgrupado.taxableBase.equivalentInEuros = dbl2str(str2Dbl(impuestoAgrupado.taxableBase.equivalentInEuros.dValue) + str2Dbl(impuesto.taxableBase.equivalentInEuros.dValue), ",")
                            End If
                        Else
                            If Not IsNothing(impuesto.taxableBase.equivalentInEuros) Then
                                impuestoAgrupado.taxableBase.equivalentInEuros = New DoubleTwoDecimalType
                                impuestoAgrupado.taxableBase.equivalentInEuros = dbl2str(str2Dbl(impuesto.taxableBase.equivalentInEuros.dValue), ",")
                            End If
                        End If
                    End If
                    If Not IsNothing(impuestoAgrupado) Then
                        If Not IsNothing(impuestoAgrupado.taxAmount) Then
                            If Not IsNothing(impuesto.taxAmount) AndAlso Not IsNothing(impuesto.taxAmount.totalAmount) Then
                                impuestoAgrupado.taxAmount.totalAmount = dbl2str(str2Dbl(impuestoAgrupado.taxAmount.totalAmount.dValue) + str2Dbl(impuesto.taxAmount.totalAmount.dValue), ",")
                            End If
                        Else
                            If Not IsNothing(impuesto.taxAmount) AndAlso Not IsNothing(impuesto.taxAmount.totalAmount) Then
                                impuestoAgrupado.taxAmount = New AmountType
                                impuestoAgrupado.taxAmount.totalAmount = New DoubleTwoDecimalType
                                impuestoAgrupado.taxAmount.totalAmount = dbl2str(str2Dbl(impuesto.taxAmount.totalAmount.dValue), ",")
                            End If
                        End If
                    End If
                    If Not IsNothing(impuestoAgrupado) Then
                        If IsNothing(impuestoAgrupado.taxAmount) Then
                            If Not IsNothing(impuesto.taxAmount) AndAlso Not IsNothing(impuesto.taxAmount.equivalentInEuros) Then
                                impuestoAgrupado.taxAmount = New AmountType
                                impuestoAgrupado.taxAmount.equivalentInEuros = New DoubleTwoDecimalType
                                impuestoAgrupado.taxAmount.equivalentInEuros = dbl2str(str2Dbl(impuesto.taxAmount.equivalentInEuros.dValue), ",")
                            End If
                        Else
                            If Not IsNothing(impuesto.taxAmount) Then
                                If Not IsNothing(impuestoAgrupado.taxAmount.equivalentInEuros) Then
                                    If Not IsNothing(impuesto.taxAmount.equivalentInEuros) Then
                                        impuestoAgrupado.taxAmount.equivalentInEuros = dbl2str(str2Dbl(impuestoAgrupado.taxAmount.equivalentInEuros.dValue) + str2Dbl(impuesto.taxAmount.equivalentInEuros.dValue), ",")
                                    End If
                                Else
                                    If Not IsNothing(impuesto.taxAmount.equivalentInEuros) Then
                                        impuestoAgrupado.taxAmount.equivalentInEuros = New DoubleTwoDecimalType
                                        impuestoAgrupado.taxAmount.equivalentInEuros = dbl2str(str2Dbl(impuesto.taxAmount.equivalentInEuros.dValue), ",")
                                    End If
                                End If
                            End If
                        End If
                    End If
                    If Not IsNothing(impuestoAgrupado) Then
                        If Not IsNothing(impuestoAgrupado.equivalenceSurchargeAmount) AndAlso Not IsNothing(impuestoAgrupado.equivalenceSurchargeAmount.totalAmount) Then
                            If Not IsNothing(impuesto.equivalenceSurchargeAmount) AndAlso Not IsNothing(impuesto.equivalenceSurchargeAmount.totalAmount) Then
                                impuestoAgrupado.equivalenceSurchargeAmount.totalAmount = dbl2str(str2Dbl(impuestoAgrupado.equivalenceSurchargeAmount.totalAmount.dValue) + str2Dbl(impuesto.equivalenceSurchargeAmount.totalAmount.dValue), ",")
                            End If
                        Else
                            If Not IsNothing(impuestoAgrupado.equivalenceSurchargeAmount) AndAlso Not IsNothing(impuestoAgrupado.equivalenceSurchargeAmount.totalAmount) Then
                                If Not IsNothing(impuesto.equivalenceSurchargeAmount) Then
                                    impuestoAgrupado.equivalenceSurchargeAmount = New AmountType
                                End If
                                If Not IsNothing(impuesto.equivalenceSurchargeAmount.totalAmount) Then
                                    impuestoAgrupado.equivalenceSurchargeAmount.totalAmount = New DoubleTwoDecimalType
                                    impuestoAgrupado.equivalenceSurchargeAmount.totalAmount = dbl2str(str2Dbl(impuesto.equivalenceSurchargeAmount.totalAmount.dValue), ",")
                                End If
                            End If
                        End If
                    End If
                    If Not IsNothing(impuestoAgrupado) Then
                        If Not IsNothing(impuestoAgrupado.equivalenceSurchargeAmount) AndAlso Not IsNothing(impuestoAgrupado.equivalenceSurchargeAmount.equivalentInEuros) Then
                            If Not IsNothing(impuesto.equivalenceSurchargeAmount) AndAlso Not IsNothing(impuesto.equivalenceSurchargeAmount.equivalentInEuros) Then
                                impuestoAgrupado.equivalenceSurchargeAmount.equivalentInEuros = dbl2str(str2Dbl(impuestoAgrupado.equivalenceSurchargeAmount.equivalentInEuros.dValue) + str2Dbl(impuesto.equivalenceSurchargeAmount.equivalentInEuros.dValue), ",")
                            End If
                        Else
                            If Not IsNothing(impuesto.equivalenceSurchargeAmount) Then
                                If Not IsNothing(impuesto.equivalenceSurchargeAmount.equivalentInEuros) Then
                                    If Not IsNothing(impuesto.equivalenceSurchargeAmount) Then
                                        impuestoAgrupado.equivalenceSurchargeAmount = New AmountType
                                    End If
                                    If Not IsNothing(impuesto.equivalenceSurchargeAmount.equivalentInEuros) Then
                                        impuestoAgrupado.equivalenceSurchargeAmount.equivalentInEuros = New DoubleTwoDecimalType
                                        impuestoAgrupado.equivalenceSurchargeAmount.equivalentInEuros = dbl2str(str2Dbl(impuesto.taxAmount.equivalentInEuros.dValue), ",")
                                    End If
                                End If
                            End If
                        End If
                    End If
                Else
                    ReDim Preserve impuestosRepercutidosAgrupados(0 To numeroDeImpuestosAgrupados)
                    impuestosRepercutidosAgrupados(numeroDeImpuestosAgrupados) = New TaxOutputType
                    impuestosRepercutidosAgrupados(numeroDeImpuestosAgrupados).taxTypeCode = New TaxTypeCodeType
                    impuestosRepercutidosAgrupados(numeroDeImpuestosAgrupados).taxTypeCode = impuesto.taxTypeCode
                    If Not (IsNothing(impuesto.taxRate)) AndAlso Not (IsNothing(impuesto.taxRate.dValue)) Then
                        impuestosRepercutidosAgrupados(numeroDeImpuestosAgrupados).taxRate = New DoubleTwoDecimalType
                        impuestosRepercutidosAgrupados(numeroDeImpuestosAgrupados).taxRate = str2Dbl(impuesto.taxRate.dValue)
                    End If
                    impuestosRepercutidosAgrupados(numeroDeImpuestosAgrupados).taxableBase = New AmountType
                    impuestosRepercutidosAgrupados(numeroDeImpuestosAgrupados).taxableBase.totalAmount = New DoubleTwoDecimalType
                    impuestosRepercutidosAgrupados(numeroDeImpuestosAgrupados).taxableBase.totalAmount = dbl2str(str2Dbl(impuesto.taxableBase.totalAmount.dValue), ",")
                    If Not IsNothing(impuesto.taxableBase) AndAlso Not IsNothing(impuesto.taxableBase.equivalentInEuros) Then
                        impuestosRepercutidosAgrupados(numeroDeImpuestosAgrupados).taxableBase.equivalentInEuros = New DoubleTwoDecimalType
                        impuestosRepercutidosAgrupados(numeroDeImpuestosAgrupados).taxableBase.equivalentInEuros = dbl2str(str2Dbl(impuesto.taxableBase.equivalentInEuros.dValue), ",")
                    End If
                    If Not IsNothing(impuesto.taxAmount) Then
                        impuestosRepercutidosAgrupados(numeroDeImpuestosAgrupados).taxAmount = New AmountType
                        If Not IsNothing(impuesto.taxAmount.totalAmount) Then
                            impuestosRepercutidosAgrupados(numeroDeImpuestosAgrupados).taxAmount.totalAmount = New DoubleTwoDecimalType
                            impuestosRepercutidosAgrupados(numeroDeImpuestosAgrupados).taxAmount.totalAmount = dbl2str(str2Dbl(impuesto.taxAmount.totalAmount.dValue), ",")
                        End If
                        If Not (IsNothing(impuesto.taxAmount.equivalentInEuros)) Then
                            impuestosRepercutidosAgrupados(numeroDeImpuestosAgrupados).taxAmount.equivalentInEuros = New DoubleTwoDecimalType
                            impuestosRepercutidosAgrupados(numeroDeImpuestosAgrupados).taxAmount.equivalentInEuros = dbl2str(str2Dbl(impuesto.taxAmount.equivalentInEuros.dValue), ",")
                        End If
                    End If
                    If Not (IsNothing(impuesto.equivalenceSurcharge)) Then
                        impuestosRepercutidosAgrupados(numeroDeImpuestosAgrupados).equivalenceSurcharge = New DoubleTwoDecimalType
                        impuestosRepercutidosAgrupados(numeroDeImpuestosAgrupados).equivalenceSurcharge = dbl2str(str2Dbl(impuesto.equivalenceSurcharge.dValue), ",")
                    End If
                    If Not (IsNothing(impuesto.equivalenceSurchargeAmount)) Then
                        impuestosRepercutidosAgrupados(numeroDeImpuestosAgrupados).equivalenceSurchargeAmount = New AmountType
                        If Not (IsNothing(impuesto.equivalenceSurchargeAmount.totalAmount)) Then
                            impuestosRepercutidosAgrupados(numeroDeImpuestosAgrupados).equivalenceSurchargeAmount.totalAmount = New DoubleTwoDecimalType
                            impuestosRepercutidosAgrupados(numeroDeImpuestosAgrupados).equivalenceSurchargeAmount.totalAmount = dbl2str(str2Dbl(impuesto.equivalenceSurchargeAmount.totalAmount.dValue), ",")
                        End If
                        If Not (IsNothing(impuesto.equivalenceSurchargeAmount.equivalentInEuros)) Then
                            impuestosRepercutidosAgrupados(numeroDeImpuestosAgrupados).equivalenceSurchargeAmount.equivalentInEuros = New DoubleTwoDecimalType
                            impuestosRepercutidosAgrupados(numeroDeImpuestosAgrupados).equivalenceSurchargeAmount.equivalentInEuros = dbl2str(str2Dbl(impuesto.equivalenceSurchargeAmount.equivalentInEuros.dValue), ",")
                        End If
                    End If
                    numeroDeImpuestosAgrupados = numeroDeImpuestosAgrupados + 1
                    End If
            End If
            contadorDeImpuestos = contadorDeImpuestos + 1
        End While

        Return impuestosRepercutidosAgrupados
    End Function
    ''' <summary>
    ''' Esta función se encarga de agrupar un vector de impuestos retenidos. Dos impuestos retenidos se pueden agrupar
    ''' si y solo si coinciden sus valores de tasa de impuesto y tipo de impuesto
    ''' </summary>
    ''' <param name="impuestosRetenidos">Vector de objetos que representan impuestos retenidos, que se debe agrupar</param>
    ''' <returns>Vector de objetos que representan impuestos retenidos, ya agrupados</returns>
    ''' <remarks>Llamado desde: CFacturaePDF.escribirAlbaranTablaImpuestosRetenidos, CFacturaePDF.escribirImpuestosRetenidos
    ''' Tiempo máximo: variable, depende del número de impuestos retenidos   
    ''' Revisado por: auv. 28/05/2012</remarks>
    Private Function agruparImpuestosRetenidos(ByRef impuestosRetenidos() As TaxWithheldType) As TaxWithheldType()
        Dim impuestosRetenidosAgrupados() As TaxWithheldType = Nothing
        Dim numeroDeImpuestosAgrupados As Integer = Nothing
        Dim contadorDeImpuestos As Integer = Nothing
        Dim contadorDeImpuestosAgrupados As Integer = Nothing
        Dim agrupar As Boolean = Nothing
        Dim impuesto As TaxWithheldType = Nothing
        Dim impuestoAgrupado As TaxWithheldType = Nothing

        numeroDeImpuestosAgrupados = 0
        contadorDeImpuestos = 0
        While contadorDeImpuestos < impuestosRetenidos.Count
            impuesto = impuestosRetenidos(contadorDeImpuestos)
            If Not IsNothing(impuesto) Then
                agrupar = False
                contadorDeImpuestosAgrupados = 0
                While (Not (agrupar)) And contadorDeImpuestosAgrupados < numeroDeImpuestosAgrupados
                    impuestoAgrupado = impuestosRetenidosAgrupados(contadorDeImpuestosAgrupados)
                    If Not IsNothing(impuestoAgrupado) Then
                        If Not IsNothing(impuesto.taxTypeCode) AndAlso Not IsNothing(impuestoAgrupado.taxTypeCode) AndAlso Not IsNothing(impuesto.taxRate) AndAlso _
                            Not IsNothing(impuestoAgrupado.taxRate) AndAlso impuesto.taxTypeCode.value = impuestoAgrupado.taxTypeCode.value AndAlso _
                            str2Dbl(impuesto.taxRate.dValue) = str2Dbl(impuestoAgrupado.taxRate.dValue) Then
                            agrupar = True
                        Else
                            contadorDeImpuestosAgrupados = contadorDeImpuestosAgrupados + 1
                        End If
                    Else
                        contadorDeImpuestosAgrupados = contadorDeImpuestosAgrupados + 1
                    End If
                End While
                If agrupar Then
                    If Not IsNothing(impuestoAgrupado) AndAlso Not IsNothing(impuestoAgrupado.taxableBase) AndAlso _
                        Not IsNothing(impuestoAgrupado.taxableBase.totalAmount) AndAlso Not IsNothing(impuesto.taxableBase) AndAlso _
                        Not IsNothing(impuesto.taxableBase.totalAmount) Then
                        impuestoAgrupado.taxableBase.totalAmount = dbl2str(str2Dbl(impuestoAgrupado.taxableBase.totalAmount.dValue) + str2Dbl(impuesto.taxableBase.totalAmount.dValue), ",")
                    End If
                    If Not IsNothing(impuestoAgrupado) AndAlso Not IsNothing(impuesto.taxableBase) Then
                        If Not IsNothing(impuestoAgrupado.taxableBase.equivalentInEuros) Then
                            If Not IsNothing(impuesto.taxableBase) AndAlso Not (IsNothing(impuesto.taxableBase.equivalentInEuros)) Then
                                impuestoAgrupado.taxableBase.equivalentInEuros = dbl2str(str2Dbl(impuestoAgrupado.taxableBase.equivalentInEuros.dValue) + str2Dbl(impuesto.taxableBase.equivalentInEuros.dValue), ",")
                            End If
                        Else
                            If Not IsNothing(impuesto.taxableBase.equivalentInEuros) Then
                                impuestoAgrupado.taxableBase.equivalentInEuros = New DoubleTwoDecimalType
                                impuestoAgrupado.taxableBase.equivalentInEuros = dbl2str(str2Dbl(impuesto.taxableBase.equivalentInEuros.dValue), ",")
                            End If
                        End If
                    End If
                    If Not IsNothing(impuestoAgrupado) Then
                        If Not IsNothing(impuestoAgrupado.taxAmount) Then
                            If Not IsNothing(impuesto.taxAmount) AndAlso Not IsNothing(impuesto.taxAmount.totalAmount) Then
                                impuestoAgrupado.taxAmount.totalAmount = dbl2str(str2Dbl(impuestoAgrupado.taxAmount.totalAmount.dValue) + str2Dbl(impuesto.taxAmount.totalAmount.dValue), ",")
                            End If
                        Else
                            If Not IsNothing(impuesto.taxAmount) AndAlso Not IsNothing(impuesto.taxAmount.totalAmount) Then
                                impuestoAgrupado.taxAmount = New AmountType
                                impuestoAgrupado.taxAmount.totalAmount = New DoubleTwoDecimalType
                                impuestoAgrupado.taxAmount.totalAmount = dbl2str(str2Dbl(impuesto.taxAmount.totalAmount.dValue), ",")
                            End If
                        End If
                    End If
                    If Not IsNothing(impuestoAgrupado) Then
                        If IsNothing(impuestoAgrupado.taxAmount) Then
                            If Not IsNothing(impuesto.taxAmount) AndAlso Not IsNothing(impuesto.taxAmount.equivalentInEuros) Then
                                impuestoAgrupado.taxAmount = New AmountType
                                impuestoAgrupado.taxAmount.equivalentInEuros = New DoubleTwoDecimalType
                                impuestoAgrupado.taxAmount.equivalentInEuros = dbl2str(str2Dbl(impuesto.taxAmount.equivalentInEuros.dValue), ",")
                            End If
                        Else
                            If Not IsNothing(impuesto.taxAmount) Then
                                If Not IsNothing(impuestoAgrupado.taxAmount.equivalentInEuros) Then
                                    If Not IsNothing(impuesto.taxAmount.equivalentInEuros) Then
                                        impuestoAgrupado.taxAmount.equivalentInEuros = dbl2str(str2Dbl(impuestoAgrupado.taxAmount.equivalentInEuros.dValue) + str2Dbl(impuesto.taxAmount.equivalentInEuros.dValue), ",")
                                    End If
                                Else
                                    If Not IsNothing(impuesto.taxAmount.equivalentInEuros) Then
                                        impuestoAgrupado.taxAmount.equivalentInEuros = New DoubleTwoDecimalType
                                        impuestoAgrupado.taxAmount.equivalentInEuros = dbl2str(str2Dbl(impuesto.taxAmount.equivalentInEuros.dValue), ",")
                                    End If
                                End If
                            End If
                        End If
                    End If
                Else
                    ReDim Preserve impuestosRetenidosAgrupados(0 To numeroDeImpuestosAgrupados)
                    impuestosRetenidosAgrupados(numeroDeImpuestosAgrupados) = New TaxWithheldType
                    impuestosRetenidosAgrupados(numeroDeImpuestosAgrupados).taxTypeCode = New TaxTypeCodeType
                    impuestosRetenidosAgrupados(numeroDeImpuestosAgrupados).taxTypeCode = impuesto.taxTypeCode
                    impuestosRetenidosAgrupados(numeroDeImpuestosAgrupados).taxRate = New DoubleTwoDecimalType
                    impuestosRetenidosAgrupados(numeroDeImpuestosAgrupados).taxRate = str2Dbl(impuesto.taxRate.dValue)
                    impuestosRetenidosAgrupados(numeroDeImpuestosAgrupados).taxableBase = New AmountType
                    impuestosRetenidosAgrupados(numeroDeImpuestosAgrupados).taxableBase.totalAmount = New DoubleTwoDecimalType
                    impuestosRetenidosAgrupados(numeroDeImpuestosAgrupados).taxableBase.totalAmount = dbl2str(str2Dbl(impuesto.taxableBase.totalAmount.dValue), ",")
                    If Not IsNothing(impuesto.taxableBase) AndAlso Not IsNothing(impuesto.taxableBase.equivalentInEuros) Then
                        impuestosRetenidosAgrupados(numeroDeImpuestosAgrupados).taxableBase.equivalentInEuros = New DoubleTwoDecimalType
                        impuestosRetenidosAgrupados(numeroDeImpuestosAgrupados).taxableBase.equivalentInEuros = dbl2str(str2Dbl(impuesto.taxableBase.equivalentInEuros.dValue), ",")
                    End If
                    If Not IsNothing(impuesto.taxAmount) Then
                        impuestosRetenidosAgrupados(numeroDeImpuestosAgrupados).taxAmount = New AmountType
                        If Not IsNothing(impuesto.taxAmount.totalAmount) Then
                            impuestosRetenidosAgrupados(numeroDeImpuestosAgrupados).taxAmount.totalAmount = New DoubleTwoDecimalType
                            impuestosRetenidosAgrupados(numeroDeImpuestosAgrupados).taxAmount.totalAmount = dbl2str(str2Dbl(impuesto.taxAmount.totalAmount.dValue), ",")
                        End If
                        If Not IsNothing(impuesto.taxAmount.equivalentInEuros) Then
                            impuestosRetenidosAgrupados(numeroDeImpuestosAgrupados).taxAmount.equivalentInEuros = New DoubleTwoDecimalType
                            impuestosRetenidosAgrupados(numeroDeImpuestosAgrupados).taxAmount.equivalentInEuros = dbl2str(str2Dbl(impuesto.taxAmount.equivalentInEuros.dValue), ",")
                        End If
                    End If
                    numeroDeImpuestosAgrupados = numeroDeImpuestosAgrupados + 1
                End If
                End If
                contadorDeImpuestos = contadorDeImpuestos + 1
        End While

        Return impuestosRetenidosAgrupados
    End Function
    '---------------------------------------------------------------------------------------------------------------------------------------------------------------
    'Funciones de manejo del PDF de bajo nivel
    '---------------------------------------------------------------------------------------------------------------------------------------------------------------
    ''' <summary>
    ''' Procedimiento para darle una formato a un párrafo dado. Se puede especificar si sus letras van en negrita, o en itálica
    ''' o subrayadas de algún modo, así como su tamaño, color y alineamiento. Además también se puede decidir si se preservan
    ''' juntas y si se preservan con el siguiente elemento.
    ''' </summary>
    ''' <param name="parrafo">Objeto que representa el párrafo a formatear</param>
    ''' <param name="sizeSpec">Si se especifica un tamaño para las letras o no</param>
    ''' <param name="size">Tamaño que deben tomar las letras</param>
    ''' <param name="colorSpec">Si se especifica un color para las letras o no</param>
    ''' <param name="color">Color que deben tomar las letras</param>
    ''' <param name="boldSpec">Si se especifica si las letras van en negrita o no</param>
    ''' <param name="bold">Si las letras deben ir en negrita o no</param>
    ''' <param name="italicSpec">Si se especifica si las letras van en itálica o no</param>
    ''' <param name="italic">Si las letras deben ir en itálica o no</param>
    ''' <param name="underlineSpec">Si se especifica si las letras van subrayadas o no</param>
    ''' <param name="underline">Cómo deben ir subrayadas las letras</param>
    ''' <param name="alignmentSpec">Si se especifica si las letras van alineadas de alguna forma o no</param>
    ''' <param name="alignment">Tipo de alineamiento que deben seguir las letras</param>
    ''' <param name="keepWithNextSpec">Si se especifica si las letras van con el siguiente elemento o no</param>
    ''' <param name="keepWithNext">Si la letras van con el siguiente elemento o no</param>
    ''' <param name="keepTogetherSpec">Si se especifica si las letras van juntas o no</param>
    ''' <param name="keepTogether">Si las letras van juntas o no</param>
    ''' <remarks>Llamado desde: CFacturaePDF.escribirFactura, CFacturaePDF.escribirEmisor, 
    ''' CFacturaePDF.escribirEmisorTablaInformacionGeneral, CFacturaePDF.escribirEmisorTablaDatosDeContacto, 
    ''' CFacturaePDF.escribirReceptor, CFacturaePDF.escribirResumenDeFactura, CFacturaePDF.escribirTotales, 
    ''' CFacturaePDF.escribirAlbaranesDeFactura, CFacturaePDF.escribirAlbaranTablaDatosGenerales, 
    ''' CFacturaePDF.escribirAlbaranTablaTotales, CFacturaePDF.escribirAlbaranTablaCargos, 
    ''' CFacturaePDF.escribirAlbaranTablaDescuentos, CFacturaePDF.escribirAlbaranTablaImpuestosRepercutidos, 
    ''' CFacturaePDF.escribirAlbaranTablaImpuestosRetenidos, CFacturaePDF.escribirDetallesDeFactura, 
    ''' CFacturaePDF.escribirItemDeFactura, CFacturaePDF.escribirItemTablaDatosGenerales, CFacturaePDF.escribirItemTablaAlbaranes, 
    ''' CFacturaePDF.escribirItemImportes, CFacturaePDF.escribirItemImportesTablaCargos, 
    ''' CFacturaePDF.escribirItemImportesTablaDescuentos, CFacturaePDF.escribirItemImportesTablaImporteBruto, 
    ''' CFacturaePDF.escribirItemImportesTablaImpuestosRepercutidos, CFacturaePDF.escribirItemImportesTablaImpuestosRetenidos, 
    ''' CFacturaePDF.escribirCargosGenerales, CFacturaePDF.escribirDescuentosGenerales, CFacturaePDF.escribirImpuestosRepercutidos, 
    ''' CFacturaePDF.escribirImpuestosRetenidos, CFacturaePDF.escribirRetencionesDeGarantia, CFacturaePDF.escribirDatosAdicionales
    ''' Tiempo máximo: menor a 0,1  
    ''' Revisado por: auv. 28/05/2012</remarks>
    Private Sub ponerFormatoParrafo(ByRef parrafo As Paragraph, Optional ByVal sizeSpec As Boolean = False, Optional ByVal size As Integer = Nothing, _
                               Optional ByVal colorSpec As Boolean = False, Optional ByRef color As Color = Nothing, _
                               Optional ByVal boldSpec As Boolean = False, Optional ByVal bold As Boolean = Nothing, _
                               Optional ByVal italicSpec As Boolean = False, Optional ByVal italic As Boolean = Nothing, _
                               Optional ByVal underlineSpec As Boolean = False, Optional ByRef underline As Underline = Nothing, _
                               Optional ByVal alignmentSpec As Boolean = False, Optional ByRef alignment As ParagraphAlignment = Nothing, _
                               Optional ByVal keepWithNextSpec As Boolean = False, Optional ByVal keepWithNext As Boolean = Nothing, _
                               Optional ByVal keepTogetherSpec As Boolean = False, Optional ByVal keepTogether As Boolean = Nothing)
        If sizeSpec Then
            parrafo.Format.Font.Size = size
        End If
        If colorSpec Then
            parrafo.Format.Font.Color = color
        End If
        If boldSpec Then
            parrafo.Format.Font.Bold = bold
        End If
        If italicSpec Then
            parrafo.Format.Font.Italic = italic
        End If
        If underlineSpec Then
            parrafo.Format.Font.Underline = underline
        End If
        If alignmentSpec Then
            parrafo.Format.Alignment = alignment
        End If
        If keepWithNextSpec Then
            parrafo.Format.KeepWithNext = keepWithNext
        End If
        If keepTogetherSpec Then
            parrafo.Format.KeepTogether = keepTogether
        End If
    End Sub
    ''' <summary>
    ''' Construye una tabla en la sección parámetro, con un número de filas y un número de columnas dado. Además también
    ''' se especifica el ancho de las columnas, así como el color de los bordes de las celdas. Todos los bordes serán visibles.
    ''' </summary>
    ''' <param name="section">Sección en la que construir la tabla</param>
    ''' <param name="anchCol">Ancho de las columnas de la  tabla, la longitud del vector denota el número de columnas que habrá</param>
    ''' <param name="numFil">Número de filas de la tabla a crear</param>
    ''' <param name="color">Color de los bordes de las celdas</param>
    ''' <returns></returns>
    ''' <remarks>Llamado desde: CFacturaePDF.escribirEmisorTablaInformacionGeneral, CFacturaePDF.escribirEmisorTablaDatosDeContacto, 
    ''' CFacturaePDF.escribirReceptor, CFacturaePDF.escribirResumenDeFactura, CFacturaePDF.escribirTotales, 
    ''' CFacturaePDF.escribirAlbaranTablaDatosGenerales, CFacturaePDF.escribirAlbaranTablaTotales, 
    ''' CFacturaePDF.escribirAlbaranTablaCargos, CFacturaePDF.escribirAlbaranTablaDescuentos, 
    ''' CFacturaePDF.escribirAlbaranTablaImpuestosRepercutidos, CFacturaePDF.escribirAlbaranTablaImpuestosRetenidos, 
    ''' CFacturaePDF.escribirItemTablaDatosGenerales, CFacturaePDF.escribirItemTablaAlbaranes, CFacturaePDF.escribirItemImportesTablaCargos, 
    ''' CFacturaePDF.escribirItemImportesTablaDescuentos, CFacturaePDF.escribirItemImportesTablaImporteBruto, 
    ''' CFacturaePDF.escribirItemImportesTablaImpuestosRepercutidos, CFacturaePDF.escribirItemImportesTablaImpuestosRetenidos, 
    ''' CFacturaePDF.escribirCargosGenerales, CFacturaePDF.escribirDescuentosGenerales, CFacturaePDF.escribirImpuestosRepercutidos, 
    ''' CFacturaePDF.escribirImpuestosRetenidos, CFacturaePDF.escribirRetencionesDeGarantia, CFacturaePDF.escribirDatosAdicionales
    ''' Tiempo máximo: variable, depende del número de filas y columnas que vaya a tener  
    ''' Revisado por: auv. 28/05/2012</remarks>
    Private Function constTab(ByRef section As Section, ByRef anchCol() As Double, ByVal numFil As Integer, ByRef color As Color) As Table
        Dim tab As Table = Nothing
        Dim cc As Integer = Nothing
        Dim cr As Integer = Nothing
        Dim neceAjus As Boolean = Nothing

        tab = section.AddTable
        tab.Style = "Table"
        tab.Format.Alignment = ParagraphAlignment.Center
        tab.Borders.Width = 1
        tab.Borders.Left.Width = 1
        tab.Borders.Right.Width = 1
        tab.Borders.Top.Width = 1
        tab.Borders.Bottom.Width = 1

        neceAjus = Me.neceAjus(section, anchCol)
        If neceAjus Then
            tab.AddColumn(Me.calcAjus(section, anchCol) & "cm")
        End If

        cc = 0
        While cc <= UBound(anchCol)
            tab.AddColumn(anchCol(cc) & "cm")
            tab.Columns.Item(tab.Columns.Count - 1).Format.Alignment = ParagraphAlignment.Center
            cc = cc + 1
        End While

        cr = 0
        While cr < numFil
            tab.AddRow()
            cr = cr + 1
        End While

        cc = 0
        While cc < tab.Columns.Count
            cr = 0
            While cr < tab.Rows.Count
                tab.Rows(cr).Cells(cc).Borders.Top.Visible = False
                tab.Rows(cr).Cells(cc).Borders.Bottom.Visible = False
                tab.Rows(cr).Cells(cc).Borders.Left.Visible = False
                tab.Rows(cr).Cells(cc).Borders.Right.Visible = False
                cr = cr + 1
            End While
            cc = cc + 1
        End While

        If Not (Colors.White = color) Then
            If neceAjus Then
                cc = 1
            Else
                cc = 0
            End If
            While cc < tab.Columns.Count
                tab.Rows(0).Cells(cc).Borders.Top.Visible = True
                tab.Rows(0).Cells(cc).Borders.Top.Color = color
                tab.Rows(tab.Rows.Count - 1).Cells(cc).Borders.Bottom.Visible = True
                tab.Rows(tab.Rows.Count - 1).Cells(cc).Borders.Bottom.Color = color
                cc = cc + 1
            End While

            cr = 0
            While cr < tab.Rows.Count
                If neceAjus Then
                    tab.Rows(cr).Cells(1).Borders.Left.Visible = True
                    tab.Rows(cr).Cells(1).Borders.Left.Color = color
                Else
                    tab.Rows(cr).Cells(0).Borders.Left.Visible = True
                    tab.Rows(cr).Cells(0).Borders.Left.Color = color
                End If
                tab.Rows(cr).Cells(tab.Columns.Count - 1).Borders.Right.Visible = True
                tab.Rows(cr).Cells(tab.Columns.Count - 1).Borders.Right.Color = color
                cr = cr + 1
            End While
        End If

        Return tab
    End Function
    ''' <summary>
    ''' Se alinean horizontalmente los contenidos de las celdas de la tabla parámetro según el tipo de alineamiento parámetro.
    ''' </summary>
    ''' <param name="tab">Tabla cuyos contenidos se deben alinear horizontalmente</param>
    ''' <param name="tipoAlin">Tipo de alineamiento horizontal.</param>
    ''' <remarks>Llamado desde: Llamado desde: CFacturaePDF.escribirEmisorTablaInformacionGeneral, 
    ''' CFacturaePDF.escribirEmisorTablaDatosDeContacto, CFacturaePDF.escribirReceptor, 
    ''' CFacturaePDF.escribirResumenDeFactura, CFacturaePDF.escribirTotales, 
    ''' CFacturaePDF.escribirAlbaranTablaDatosGenerales, CFacturaePDF.escribirAlbaranTablaTotales, 
    ''' CFacturaePDF.escribirAlbaranTablaCargos, CFacturaePDF.escribirAlbaranTablaDescuentos, 
    ''' CFacturaePDF.escribirAlbaranTablaImpuestosRepercutidos, CFacturaePDF.escribirAlbaranTablaImpuestosRetenidos, 
    ''' CFacturaePDF.escribirItemTablaDatosGenerales, CFacturaePDF.escribirItemTablaAlbaranes, 
    ''' CFacturaePDF.escribirItemImportesTablaCargos, CFacturaePDF.escribirItemImportesTablaDescuentos, 
    ''' CFacturaePDF.escribirItemImportesTablaImporteBruto, CFacturaePDF.escribirItemImportesTablaImpuestosRepercutidos, 
    ''' CFacturaePDF.escribirItemImportesTablaImpuestosRetenidos, CFacturaePDF.escribirCargosGenerales, 
    ''' CFacturaePDF.escribirDescuentosGenerales, CFacturaePDF.escribirImpuestosRepercutidos, 
    ''' CFacturaePDF.escribirImpuestosRetenidos, CFacturaePDF.escribirRetencionesDeGarantia, CFacturaePDF.escribirDatosAdicionales
    ''' Tiempo máximo: variable, depende del número de columnas y del número de filas de la tabla parámetro    
    ''' Revisado por: auv. 28/05/2012</remarks>
    Private Sub alinHorContTab(ByRef tab As Table, ByVal tipoAlin As Integer)
        Dim cr As Integer = Nothing
        cr = 0
        While cr < tab.Rows.Count
            Me.alinHorContFil(tab, cr, tipoAlin)
            cr = cr + 1
        End While
    End Sub
    ''' <summary>
    ''' Se alinean horizontalmente los contenidos de las celdas que residen en la fila parámetro según el tipo de alineamiento
    ''' parámetro.
    ''' </summary>
    ''' <param name="tab">Tabla en la que reside la fila cuyas celdas cuyos contenidos se deben alinear horizontalmente</param>
    ''' <param name="nr">Número de fila</param>
    ''' <param name="tipoAlin">Tipo de alineamiento horizontal</param>
    ''' <remarks>Llamado desde: CFacturaePDF.alinHorContTab
    ''' Tiempo máximo: variable, depende del número de columnas de la tabla parámetro 
    ''' Revisado por: auv. 28/05/2012</remarks>
    Private Sub alinHorContFil(ByRef tab As Table, ByVal nr As Integer, ByVal tipoAlin As Integer)
        Dim cc As Integer = Nothing

        cc = 0
        While cc < tab.Rows(nr).Cells.Count
            Me.alinHorContCel(tab, nr, cc, tipoAlin)
            cc = cc + 1
        End While
    End Sub
    ''' <summary>
    ''' Se alinea horizontalmente el contenido de una celda de la tabla según el tipo de alineamiento parámetro.
    ''' </summary>
    ''' <param name="tab">Tabla en la que reside la celda con el contenido a alinear horizontalmente</param>
    ''' <param name="nr">Número de  fila</param>
    ''' <param name="nc">Número de columna</param>
    ''' <param name="tipoAlin">Tipo de alineamiento horizontal</param>
    ''' <remarks>Llamado desde: CFacturaePDF.alinHorContFil
    ''' Tiempo máximo:menor a 0,1    
    ''' Revisado por: auv. 28/05/2012</remarks>
    Private Sub alinHorContCel(ByRef tab As Table, ByVal nr As Integer, ByVal nc As Integer, ByVal tipoAlin As Integer)
        Select Case tipoAlin
            Case CFacturaePDF.center
                tab.Rows(nr).Cells(nc).Format.Alignment = ParagraphAlignment.Center
            Case CFacturaePDF.left
                tab.Rows(nr).Cells(nc).Format.Alignment = ParagraphAlignment.Left
            Case CFacturaePDF.right
                tab.Rows(nr).Cells(nc).Format.Alignment = ParagraphAlignment.Right
            Case CFacturaePDF.justify
                tab.Rows(nr).Cells(nc).Format.Alignment = ParagraphAlignment.Justify
        End Select
    End Sub
    ''' <summary>
    ''' Se alinean verticalmente los contenidos de las celdas de la tabla parámetro según el tipo de alineamiento parámetro.
    ''' </summary>
    ''' <param name="tab">Tabla cuyos contenidos se deben alinear verticalmente</param>
    ''' <param name="tipoAlin">Tipo de alineamiento vertical.</param>
    ''' <remarks>Llamado desde: CFacturaePDF.escribirEmisorTablaInformacionGeneral, 
    ''' CFacturaePDF.escribirEmisorTablaDatosDeContacto, CFacturaePDF.escribirReceptor, 
    ''' CFacturaePDF.escribirResumenDeFactura, CFacturaePDF.escribirTotales, 
    ''' CFacturaePDF.escribirAlbaranTablaDatosGenerales, CFacturaePDF.escribirAlbaranTablaTotales, 
    ''' CFacturaePDF.escribirAlbaranTablaCargos, CFacturaePDF.escribirAlbaranTablaDescuentos, 
    ''' CFacturaePDF.escribirAlbaranTablaImpuestosRepercutidos, CFacturaePDF.escribirAlbaranTablaImpuestosRetenidos, 
    ''' CFacturaePDF.escribirItemTablaDatosGenerales, CFacturaePDF.escribirItemTablaAlbaranes, 
    ''' CFacturaePDF.escribirItemImportesTablaCargos, CFacturaePDF.escribirItemImportesTablaDescuentos, 
    ''' CFacturaePDF.escribirItemImportesTablaImporteBruto, CFacturaePDF.escribirItemImportesTablaImpuestosRepercutidos, 
    ''' CFacturaePDF.escribirItemImportesTablaImpuestosRetenidos, CFacturaePDF.escribirCargosGenerales, 
    ''' CFacturaePDF.escribirDescuentosGenerales, CFacturaePDF.escribirImpuestosRepercutidos, 
    ''' CFacturaePDF.escribirImpuestosRetenidos, CFacturaePDF.escribirRetencionesDeGarantia, CFacturaePDF.escribirDatosAdicionales
    ''' Tiempo máximo: variable, depende del número de filas y columnas de la tabla parámetro
    ''' Revisado por: auv. 28/05/2012</remarks>
    Private Sub alinVerContTab(ByRef tab As Table, ByVal tipoAlin As Integer)
        Dim cr As Integer = Nothing
        cr = 0
        While cr < tab.Rows.Count
            Me.alinVerContFil(tab, cr, tipoAlin)
            cr = cr + 1
        End While
    End Sub
    ''' <summary>
    ''' Se alinean verticalmente los contenidos de las celdas que residen en la fila parámetro según el tipo de alineamiento
    ''' parámetro.
    ''' </summary>
    ''' <param name="tab">Tabla en la que reside la fila cuyas celdas cuyos contenidos se deben alinear verticalmente</param>
    ''' <param name="nr">>Número de fila</param>
    ''' <param name="tipoAlin">Tipo de alineamiento vertical</param>
    ''' <remarks>Llamado desde: CFacturaePDF.alinVertContTab
    ''' Tiempo máximo: Variable, depende del número de columnas de la tabla parámetro    
    ''' Revisado por: auv. 28/05/2012</remarks>
    Private Sub alinVerContFil(ByRef tab As Table, ByVal nr As Integer, ByVal tipoAlin As Integer)
        Dim cc As Integer = 0
        While cc < tab.Rows(nr).Cells.Count
            Me.alinVerContCel(tab, nr, cc, tipoAlin)
            cc = cc + 1
        End While
    End Sub
    ''' <summary>
    ''' Se alinea verticalmente el contenido de una celda de la tabla según el tipo de alineamiento parámetro.
    ''' </summary>
    ''' <param name="tab">Tabla en la que reside la celda con el contenido a alinear verticalmente</param>
    ''' <param name="nr">Número de  fila</param>
    ''' <param name="nc">Número de columna</param>
    ''' <param name="tipoAlin">Tipo de alineamiento horizontal</param>
    ''' <remarks>Llamado desde: CFacturaePDF.alinVertContFil   
    ''' Tiempo máximo: menor a 0,1 
    ''' Revisado por: auv. 28/05/2012</remarks>
    Private Sub alinVerContCel(ByRef tab As Table, ByVal nr As Integer, ByVal nc As Integer, ByVal tipoAlin As Integer)
        Select Case tipoAlin
            Case CFacturaePDF.center
                tab.Rows(nr).Cells(nc).VerticalAlignment = VerticalAlignment.Center
            Case CFacturaePDF.top
                tab.Rows(nr).Cells(nc).VerticalAlignment = VerticalAlignment.Top
            Case CFacturaePDF.bottom
                tab.Rows(nr).Cells(nc).VerticalAlignment = VerticalAlignment.Bottom
            Case Else
        End Select
    End Sub
    ''' <summary>
    ''' Función que indica si en la celda indicada en la tabla indicada, es posible poner un borde en la parte superior de 
    ''' dicha celda
    ''' </summary>
    ''' <param name="tab">Tabla en la que se cuestiona poner un borde</param>
    ''' <param name="nr">Número de fila</param>
    ''' <param name="nc">Número de columna</param>
    ''' <param name="esTabConColAjus">Si es una tabla con columna de ajuste (columna primera invisible y auxiliar, que sirve
    ''' para centrar la tabla)</param>
    ''' <returns>Si se puede poner el mencionado borde o no</returns>
    ''' <remarks>Llamado desde: CFacturaePDF.bordeCelda
    ''' Tiempo máximo: menor a 0,1   
    ''' Revisado por: auv. 28/05/2012</remarks>
    Private Function esPosiPonerBorTop(ByRef tab As Table, ByVal nr As Integer, ByVal nc As Integer, ByVal esTabConColAjus As Boolean) As Boolean
        Dim es As Boolean = Nothing

        If ((Not esTabConColAjus) Or (Not nc = 0)) And _
            ((Not nr = 0) Or tab.Rows(nr).Cells(nc).Borders.Top.Visible = False) Then
            es = True
        Else
            es = False
        End If

        Return es
    End Function
    ''' <summary>
    ''' Función que indica si en la celda indicada en la tabla indicada, es posible poner un borde en la parte inferior de 
    ''' dicha celda
    ''' </summary>
    ''' <param name="tab">Tabla en la que se cuestiona poner un borde</param>
    ''' <param name="nr">Número de fila</param>
    ''' <param name="nc">Número de columna</param>
    ''' <param name="esTabConColAjus">Si es una tabla con columna de ajuste (columna primera invisible y auxiliar, que sirve
    ''' para centrar la tabla)</param>
    ''' <returns>Si se puede poner el mencionado borde o no</returns>
    ''' <remarks>Llamado desde: CFacturaePDF.bordeCelda
    ''' Tiempo máximo: menor a 0,1   
    ''' Revisado por: auv. 28/05/2012</remarks>
    Private Function esPosiPonerBorBottom(ByRef tab As Table, ByVal nr As Integer, ByVal nc As Integer, ByVal esTabConColAjus As Boolean) As Boolean
        Dim es As Boolean = Nothing

        If ((Not esTabConColAjus) Or (Not nc = 0)) And _
            (Not (nr = tab.Rows.Count - 1) Or tab.Rows(nr).Cells(nc).Borders.Bottom.Visible = False) Then
            es = True
        Else
            es = False
        End If
        Return es
    End Function
    ''' <summary>
    ''' Función que indica si en la celda indicada en la tabla indicada, es posible poner un borde en la parte izquierda de 
    ''' dicha celda
    ''' </summary>
    ''' <param name="tab">Tabla en la que se cuestiona poner un borde</param>
    ''' <param name="nr">Número de fila</param>
    ''' <param name="nc">Número de columna</param>
    ''' <param name="esTabConColAjus">Si es una tabla con columna de ajuste (columna primera invisible y auxiliar, que sirve
    ''' para centrar la tabla)</param>
    ''' <returns>Si se puede poner el mencionado borde o no</returns>
    ''' <remarks>Llamado desde: CFacturaePDF.bordeCelda
    ''' Tiempo máximo: menor a 0,1   
    ''' Revisado por: auv. 28/05/2012</remarks>
    Private Function esPosiPonerBorLeft(ByRef tab As Table, ByVal nr As Integer, ByVal nc As Integer, ByVal esTabConColAjus As Boolean) As Boolean
        Dim es As Boolean = Nothing

        If ((Not esTabConColAjus) Or (Not nc = 0)) And _
            (((Not esTabConColAjus) Or (Not nc = 1)) Or tab.Rows(nr).Cells(nc).Borders.Left.Visible = False) _
            And ((esTabConColAjus Or (Not nc = 0)) Or tab.Rows(nr).Cells(nc).Borders.Left.Visible = False) Then
            es = True
        Else
            es = False
        End If

        Return es
    End Function
    ''' <summary>
    ''' Función que indica si en la celda indicada en la tabla indicada, es posible poner un borde en la parte derecha de 
    ''' dicha celda
    ''' </summary>
    ''' <param name="tab">Tabla en la que se cuestiona poner un borde</param>
    ''' <param name="nr">Número de fila</param>
    ''' <param name="nc">Número de columna</param>
    ''' <param name="esTabConColAjus">Si es una tabla con columna de ajuste (columna primera invisible y auxiliar, que sirve
    ''' para centrar la tabla)</param>
    ''' <returns>Si se puede poner el mencionado borde o no</returns>
    ''' <remarks>Llamado desde: CFacturaePDF.bordeCelda   
    ''' Tiempo máximo: menor a 0,1    
    ''' Revisado por: auv. 28/05/2012</remarks>
    Private Function esPosiPonerBorRight(ByRef tab As Table, ByVal nr As Integer, ByVal nc As Integer, ByVal esTabConColAjus As Boolean) As Boolean
        Dim es As Boolean = Nothing

        If ((Not esTabConColAjus) Or (Not nc = 0)) And _
            (Not (nc = tab.Columns.Count - 1) Or tab.Rows(nr).Cells(nc).Borders.Right.Visible = False) Then
            es = True
        Else
            es = False
        End If

        Return es
    End Function
    ''' <summary>
    ''' Función que permite decidir si en la celda especificada se pone (o no) borde superior, inferior, izquierdo, derecho, 
    ''' del color parámetro.
    ''' </summary>
    ''' <param name="tab">Tabla en la cual reside la celda para la cual se van a diseñar sus bordes</param>
    ''' <param name="nc">Número de columna</param>
    ''' <param name="nr">Número de fila</param>
    ''' <param name="topBor">Si debe llevar borde superior o no</param>
    ''' <param name="bottomBor">Si debe llevar borde inferior o no</param>
    ''' <param name="leftBor">Si debe llevar borde izquierdo o no</param>
    ''' <param name="rightBor">Si debe llevar borde derecho o no</param>
    ''' <param name="color">Color de los bordes de la celda</param>
    ''' <param name="esTabConColAjus">Si es una tabla con columna de ajuste (columna primera invisible y auxiliar, que sirve
    ''' para centrar la tabla)</param>
    ''' <remarks>Llamado desde: CFacturaePDF.bordeFilas, CFacturaePDF.bordeColumnas
    ''' Tiempo máximo: menor a 0,1    
    ''' Revisado por: auv. 28/05/2012</remarks>
    Private Sub bordeCelda(ByRef tab As Table, ByVal nc As Integer, ByVal nr As Integer, ByVal topBor As Boolean, ByVal bottomBor As Boolean, ByVal leftBor As Boolean, ByVal rightBor As Boolean, ByRef color As Color, ByVal esTabConColAjus As Boolean)
        If leftBor Then
            If esPosiPonerBorLeft(tab, nr, nc, esTabConColAjus) Then
                tab.Rows(nr).Cells(nc).Borders.Left.Visible = True
                tab.Rows(nr).Cells(nc).Borders.Left.Color = color
            End If
        Else
            tab.Rows(nr).Cells(nc).Borders.Left.Visible = False
        End If
        If rightBor Then
            If esPosiPonerBorRight(tab, nr, nc, esTabConColAjus) Then
                tab.Rows(nr).Cells(nc).Borders.Right.Visible = True
                tab.Rows(nr).Cells(nc).Borders.Right.Color = color
            End If
        Else
            tab.Rows(nr).Cells(nc).Borders.Right.Visible = False
        End If
        If topBor Then
            If esPosiPonerBorTop(tab, nr, nc, esTabConColAjus) Then
                tab.Rows(nr).Cells(nc).Borders.Top.Visible = True
                tab.Rows(nr).Cells(nc).Borders.Top.Color = color
            End If
        Else
            tab.Rows(nr).Cells(nc).Borders.Top.Visible = False
        End If
        If bottomBor Then
            If esPosiPonerBorBottom(tab, nr, nc, esTabConColAjus) Then
                tab.Rows(nr).Cells(nc).Borders.Bottom.Visible = True
                tab.Rows(nr).Cells(nc).Borders.Bottom.Color = color
            End If
        Else
            tab.Rows(nr).Cells(nc).Borders.Bottom.Visible = False
        End If
    End Sub
    ''' <summary>
    ''' Función que permite especificar los bordes de las filas, según lo configurado en el objeto tabla
    ''' </summary>
    ''' <param name="tab">Tabla en la cual se van a especificar los bordes de las filas</param>
    ''' <param name="color">Color que tomarán los bordes</param>
    ''' <param name="esTabConColAjus">Si es una tabla con columna de ajuste (columna primera invisible y auxiliar, que sirve
    ''' para centrar la tabla)</param>
    ''' <remarks>Llamado desde: CFacturaePDF.escribirEmisorTablaInformacionGeneral, 
    ''' CFacturaePDF.escribirEmisorTablaDatosDeContacto, CFacturaePDF.escribirReceptor, 
    ''' CFacturaePDF.escribirResumenDeFactura, CFacturaePDF.escribirTotales, 
    ''' CFacturaePDF.escribirAlbaranTablaDatosGenerales, CFacturaePDF.escribirAlbaranTablaTotales, 
    ''' CFacturaePDF.escribirAlbaranTablaCargos, CFacturaePDF.escribirAlbaranTablaDescuentos, 
    ''' CFacturaePDF.escribirAlbaranTablaImpuestosRepercutidos, CFacturaePDF.escribirAlbaranTablaImpuestosRetenidos, 
    ''' CFacturaePDF.escribirItemTablaDatosGenerales, CFacturaePDF.escribirItemTablaAlbaranes, 
    ''' CFacturaePDF.escribirItemImportesTablaCargos, CFacturaePDF.escribirItemImportesTablaDescuentos, 
    ''' CFacturaePDF.escribirItemImportesTablaImporteBruto, CFacturaePDF.escribirItemImportesTablaImpuestosRepercutidos, 
    ''' CFacturaePDF.escribirItemImportesTablaImpuestosRetenidos, CFacturaePDF.escribirCargosGenerales, 
    ''' CFacturaePDF.escribirDescuentosGenerales, CFacturaePDF.escribirImpuestosRepercutidos, 
    ''' CFacturaePDF.escribirImpuestosRetenidos, CFacturaePDF.escribirRetencionesDeGarantia, CFacturaePDF.escribirDatosAdicionales
    ''' Tiempo máximo: variable, depende    
    ''' Revisado por: auv. 28/05/2012</remarks>
    Private Sub bordeFilas(ByRef tab As Table, ByRef color As Color, ByVal esTabConColAjus As Boolean)
        Dim nr As Integer = Nothing
        Dim nc As Integer = Nothing

        If esTabConColAjus Then
            nc = 1
        Else
            nc = 0
        End If
        While nc < tab.Columns.Count
            nr = 0
            While nr < tab.Rows.Count
                Me.bordeCelda(tab, nc, nr, True, True, tab.Rows(nr).Cells(nc).Borders.Left.Visible, tab.Rows(nr).Cells(nc).Borders.Right.Visible, color, esTabConColAjus)
                nr = nr + 1
            End While
            nc = nc + 1
        End While
    End Sub
    ''' <summary>
    ''' Función que permite especificar los bordes de las columnas, según lo configurado en el objeto tabla
    ''' </summary>
    ''' <param name="tab">Tabla en la cual se van a especificar los bordes de las columnas</param>
    ''' <param name="color">Color que tomarán los bordes</param>
    ''' <param name="esTabConColAjus">Si es una tabla con columna de ajuste (columna primera invisible y auxiliar, que sirve
    ''' para centrar la tabla)</param>
    ''' <remarks>Llamado desde: CFacturaePDF.escribirEmisorTablaInformacionGeneral, 
    ''' CFacturaePDF.escribirEmisorTablaDatosDeContacto, CFacturaePDF.escribirReceptor, 
    ''' CFacturaePDF.escribirResumenDeFactura, CFacturaePDF.escribirTotales, 
    ''' CFacturaePDF.escribirAlbaranTablaDatosGenerales, CFacturaePDF.escribirAlbaranTablaTotales, 
    ''' CFacturaePDF.escribirAlbaranTablaCargos, CFacturaePDF.escribirAlbaranTablaDescuentos, 
    ''' CFacturaePDF.escribirAlbaranTablaImpuestosRepercutidos, CFacturaePDF.escribirAlbaranTablaImpuestosRetenidos, 
    ''' CFacturaePDF.escribirItemTablaDatosGenerales, CFacturaePDF.escribirItemTablaAlbaranes, 
    ''' CFacturaePDF.escribirItemImportesTablaCargos, CFacturaePDF.escribirItemImportesTablaDescuentos, 
    ''' CFacturaePDF.escribirItemImportesTablaImporteBruto, CFacturaePDF.escribirItemImportesTablaImpuestosRepercutidos, 
    ''' CFacturaePDF.escribirItemImportesTablaImpuestosRetenidos, CFacturaePDF.escribirCargosGenerales, 
    ''' CFacturaePDF.escribirDescuentosGenerales, CFacturaePDF.escribirImpuestosRepercutidos, 
    ''' CFacturaePDF.escribirImpuestosRetenidos, CFacturaePDF.escribirRetencionesDeGarantia, CFacturaePDF.escribirDatosAdicionales
    ''' Tiempo máximo:    
    ''' Revisado por: auv. 28/05/2012</remarks>
    Private Sub bordeColumnas(ByRef tab As Table, ByRef color As Color, ByVal esTabConColAjus As Boolean)
        Dim nr As Integer = Nothing
        Dim nc As Integer = Nothing

        If esTabConColAjus Then
            nc = 1
        Else
            nc = 0
        End If
        While nc < tab.Columns.Count
            nr = 0
            While nr < tab.Rows.Count
                Me.bordeCelda(tab, nc, nr, tab.Rows(nr).Cells(nc).Borders.Top.Visible, tab.Rows(nr).Cells(nc).Borders.Bottom.Visible, True, True, color, esTabConColAjus)
                nr = nr + 1
            End While
            nc = nc + 1
        End While
    End Sub
    ''' <summary>
    ''' Función que permite hacer que las celdas de una tabla se autounan. Se especifica una celda inicial, para que cualquier celda sobre
    ''' la que se intenta una autounión debe cumplir que, o bien es la celda inicial, o bien está por debajo y/o a la derecha en la tabla. El 
    ''' parámetro de la prioridad de iteración indica en qué dimensión se recorre primero el intento de autounion para las celdas. La prioridad
    ''' de agrupación indica que dada una celda para la cual se plantea la autounión, en qué dimensión intenta autounir celdas primero. Es decir
    ''' la idea general es coger un subconjunto cuadrado de la tabla especificado por la celda inicial y por la esquina inferior derecha de 
    ''' la tabla y operar sobre ese subconjunto de la siguiente forma. Se recorre el subconjunto por filas y columnas según  el orden que 
    ''' marque la prioridad de iteración. Para una celda para la cual se examina su autounión se recorren las filas y columnas que quedan 
    ''' por debajo y por la derecha suyo respectivamente, según el orden que marque la prioridad de agrupación. En el subconjunto de la tabla
    ''' se trata de conformar uniones de celdas lo más grandes posibles dadas las prioridades de iteración y agrupación y teniendo en cuenta que
    ''' una celda para la cual se practica la autounión solo se puede anexionar celdas que no tengan contenidos.
    ''' </summary>
    ''' <param name="tab">Tabla cuyas celdas a autounir</param>
    ''' <param name="filIni">Fila por la cual se empieza a realizar la autounión</param>
    ''' <param name="colIni">Columna por la cual se empieza a realizar la autounión</param>
    ''' <param name="prioridadIteracion">Las celdas para las cuales se examina la autounión se pueen recorrer en prioridad 
    ''' por filas o en prioridad por columnas. Si la prioridad es por filas, primero se examina la autounión para todas 
    ''' las filas (a partir de la fila de inicio) de la columna de inicio, luego se miran las filas de la siguiente columna,
    ''' etcétera. Si la prioridad es por columnas, se opera de forma traspuesta, primero se examina la autounión para 
    ''' todas las columnas (a partir de la columna de inicio) de la fila de inicio, etcétera.</param>
    ''' <param name="prioridadAgrupacion">Para una celda dada a autounir, se debe especificar en qué dimensión se da la 
    ''' preferencia de autounión. Si la prioridad es por filas, una celda a autounir, primero intenta anexionarse las de debajo
    ''' hasta que no puede más y después trata de anexionarse todos los segmentos de tabla que pueda hacia la derecha. Si
    ''' la prioridad es por columnas operara de forma traspuesta, primero se anexionará todas las celdas que pueda hacia la 
    ''' derecha y después se anexionará todos los segmentos de tabla hacia abajo que le sean posibles.</param>
    ''' <remarks>Llamado desde: CFacturaePDF.escribirEmisorTablaInformacionGeneral, 
    ''' CFacturaePDF.escribirEmisorTablaDatosDeContacto, CFacturaePDF.escribirReceptor, CFacturaePDF.escribirResumenDeFactura, 
    ''' CFacturaePDF.escribirTotales, CFacturaePDF.escribirAlbaranTablaDatosGenerales, CFacturaePDF.escribirAlbaranTablaTotales, 
    ''' CFacturaePDF.escribirAlbaranTablaCargos, CFacturaePDF.escribirAlbaranTablaDescuentos, 
    ''' CFacturaePDF.escribirAlbaranTablaImpuestosRepercutidos, CFacturaePDF.escribirAlbaranTablaImpuestosRetenidos, 
    ''' CFacturaePDF.escribirItemTablaDatosGenerales, CFacturaePDF.escribirItemTablaAlbaranes, 
    ''' CFacturaePDF.escribirItemImportesTablaCargos, CFacturaePDF.escribirItemImportesTablaDescuentos, 
    ''' CFacturaePDF.escribirItemImportesTablaImporteBruto, CFacturaePDF.escribirItemImportesTablaImpuestosRepercutidos, 
    ''' CFacturaePDF.escribirItemImportesTablaImpuestosRetenidos, CFacturaePDF.escribirCargosGenerales, 
    ''' CFacturaePDF.escribirDescuentosGenerales, CFacturaePDF.escribirImpuestosRepercutidos, 
    ''' CFacturaePDF.escribirImpuestosRetenidos, CFacturaePDF.escribirRetencionesDeGarantia, CFacturaePDF.escribirDatosAdicionales
    ''' Tiempo máximo: variable, depende del número de filas y celdas que conformen el subconjunto    
    ''' Revisado por: auv. 28/05/2012</remarks>
    Private Sub autoUnirCeldasDeTabla(ByRef tab As Table, ByVal filIni As Integer, ByVal colIni As Integer, ByVal prioridadIteracion As Integer, ByVal prioridadAgrupacion As Integer)
        Dim contFil As Integer = Nothing
        Dim contCol As Integer = Nothing
        Dim numeroDeColumnasAUnir As Integer = Nothing
        Dim numeroDeFilasAUnir As Integer = Nothing
        Dim salir As Boolean = Nothing

        Select Case prioridadIteracion
            Case CFacturaePDF.prioridadFilas
                Select Case prioridadAgrupacion
                    Case CFacturaePDF.prioridadFilas
                        contCol = colIni
                        While contCol < tab.Columns.Count
                            contFil = filIni
                            While contFil < tab.Rows.Count
                                numeroDeFilasAUnir = 0
                                numeroDeColumnasAUnir = 0
                                salir = False
                                While contFil + numeroDeFilasAUnir < tab.Rows.Count And Not salir
                                    If Me.esPosibleAutoUnirCeldasDeTabla(tab, contFil, contCol, contFil + numeroDeFilasAUnir, contCol + numeroDeColumnasAUnir) Then
                                        numeroDeFilasAUnir = numeroDeFilasAUnir + 1
                                    Else
                                        numeroDeFilasAUnir = numeroDeFilasAUnir - 1
                                        salir = True
                                    End If
                                    If contFil + numeroDeFilasAUnir >= tab.Rows.Count Then
                                        numeroDeFilasAUnir = numeroDeFilasAUnir - 1
                                        salir = True
                                    End If
                                    If numeroDeFilasAUnir < 0 Then
                                        numeroDeFilasAUnir = 0
                                    End If
                                End While
                                salir = False
                                While contCol + numeroDeColumnasAUnir < tab.Columns.Count And Not salir
                                    If Me.esPosibleAutoUnirCeldasDeTabla(tab, contFil, contCol, contFil + numeroDeFilasAUnir, contCol + numeroDeColumnasAUnir) Then
                                        numeroDeColumnasAUnir = numeroDeColumnasAUnir + 1
                                    Else
                                        numeroDeColumnasAUnir = numeroDeColumnasAUnir - 1
                                        salir = True
                                    End If
                                    If contCol + numeroDeColumnasAUnir >= tab.Columns.Count Then
                                        numeroDeColumnasAUnir = numeroDeColumnasAUnir - 1
                                        salir = True
                                    End If
                                    If numeroDeColumnasAUnir < 0 Then
                                        numeroDeColumnasAUnir = 0
                                    End If
                                End While
                                If numeroDeFilasAUnir <> 0 Then
                                    tab.Rows(contFil).Item(contCol).MergeDown = numeroDeFilasAUnir
                                End If
                                If numeroDeColumnasAUnir <> 0 Then
                                    tab.Rows(contFil).Item(contCol).MergeRight = numeroDeColumnasAUnir
                                End If
                                contFil = contFil + 1
                            End While
                            contCol = contCol + 1
                        End While
                    Case CFacturaePDF.prioridadColumnas
                        contCol = colIni
                        While contCol < tab.Columns.Count
                            contFil = filIni
                            While contFil < tab.Rows.Count
                                numeroDeFilasAUnir = 0
                                numeroDeColumnasAUnir = 0
                                salir = False
                                While contCol + numeroDeColumnasAUnir < tab.Columns.Count And Not salir
                                    If Me.esPosibleAutoUnirCeldasDeTabla(tab, contFil, contCol, contFil + numeroDeFilasAUnir, contCol + numeroDeColumnasAUnir) Then
                                        numeroDeColumnasAUnir = numeroDeColumnasAUnir + 1
                                    Else
                                        numeroDeColumnasAUnir = numeroDeColumnasAUnir - 1
                                        salir = True
                                    End If
                                    If contCol + numeroDeColumnasAUnir >= tab.Columns.Count Then
                                        numeroDeColumnasAUnir = numeroDeColumnasAUnir - 1
                                        salir = True
                                    End If
                                    If numeroDeColumnasAUnir < 0 Then
                                        numeroDeColumnasAUnir = 0
                                    End If
                                End While
                                salir = False
                                While contFil + numeroDeFilasAUnir < tab.Rows.Count And Not salir
                                    If Me.esPosibleAutoUnirCeldasDeTabla(tab, contFil, contCol, contFil + numeroDeFilasAUnir, contCol + numeroDeColumnasAUnir) Then
                                        numeroDeFilasAUnir = numeroDeFilasAUnir + 1
                                    Else
                                        numeroDeFilasAUnir = numeroDeFilasAUnir - 1
                                        salir = True
                                    End If
                                    If contFil + numeroDeFilasAUnir >= tab.Rows.Count Then
                                        numeroDeFilasAUnir = numeroDeFilasAUnir - 1
                                        salir = True
                                    End If
                                    If numeroDeFilasAUnir < 0 Then
                                        numeroDeFilasAUnir = 0
                                    End If
                                End While
                                If numeroDeColumnasAUnir <> 0 Then
                                    tab.Rows(contFil).Item(contCol).MergeRight = numeroDeColumnasAUnir
                                End If
                                If numeroDeFilasAUnir <> 0 Then
                                    tab.Rows(contFil).Item(contCol).MergeDown = numeroDeFilasAUnir
                                End If
                                contFil = contFil + 1
                            End While
                            contCol = contCol + 1
                        End While
                End Select
            Case CFacturaePDF.prioridadColumnas
                Select Case prioridadAgrupacion
                    Case CFacturaePDF.prioridadFilas
                        contFil = filIni
                        While contFil < tab.Rows.Count
                            contCol = colIni
                            While contCol < tab.Columns.Count
                                numeroDeFilasAUnir = 0
                                numeroDeColumnasAUnir = 0
                                salir = False
                                While contFil + numeroDeFilasAUnir < tab.Rows.Count And Not salir
                                    If Me.esPosibleAutoUnirCeldasDeTabla(tab, contFil, contCol, contFil + numeroDeFilasAUnir, contCol + numeroDeColumnasAUnir) Then
                                        numeroDeFilasAUnir = numeroDeFilasAUnir + 1
                                    Else
                                        numeroDeFilasAUnir = numeroDeFilasAUnir - 1
                                        salir = True
                                    End If
                                    If contFil + numeroDeFilasAUnir >= tab.Rows.Count Then
                                        numeroDeFilasAUnir = numeroDeFilasAUnir - 1
                                        salir = True
                                    End If
                                    If numeroDeFilasAUnir < 0 Then
                                        numeroDeFilasAUnir = 0
                                    End If
                                End While
                                salir = False
                                While contCol + numeroDeColumnasAUnir < tab.Columns.Count And Not salir
                                    If Me.esPosibleAutoUnirCeldasDeTabla(tab, contFil, contCol, contFil + numeroDeFilasAUnir, contCol + numeroDeColumnasAUnir) Then
                                        numeroDeColumnasAUnir = numeroDeColumnasAUnir + 1
                                    Else
                                        numeroDeColumnasAUnir = numeroDeColumnasAUnir - 1
                                        salir = True
                                    End If
                                    If contCol + numeroDeColumnasAUnir >= tab.Columns.Count Then
                                        numeroDeColumnasAUnir = numeroDeColumnasAUnir - 1
                                        salir = True
                                    End If
                                    If numeroDeColumnasAUnir < 0 Then
                                        numeroDeColumnasAUnir = 0
                                    End If
                                End While
                                If numeroDeFilasAUnir <> 0 Then
                                    tab.Rows(contFil).Item(contCol).MergeDown = numeroDeFilasAUnir
                                End If
                                If numeroDeColumnasAUnir <> 0 Then
                                    tab.Rows(contFil).Item(contCol).MergeRight = numeroDeColumnasAUnir
                                End If
                                contCol = contCol + 1
                            End While
                            contFil = contFil + 1
                        End While
                    Case CFacturaePDF.prioridadColumnas
                        contFil = filIni
                        While contFil < tab.Rows.Count
                            contCol = colIni
                            While contCol < tab.Columns.Count
                                numeroDeFilasAUnir = 0
                                numeroDeColumnasAUnir = 0
                                salir = False
                                While contCol + numeroDeColumnasAUnir < tab.Columns.Count And Not salir
                                    If Me.esPosibleAutoUnirCeldasDeTabla(tab, contFil, contCol, contFil + numeroDeFilasAUnir, contCol + numeroDeColumnasAUnir) Then
                                        numeroDeColumnasAUnir = numeroDeColumnasAUnir + 1
                                    Else
                                        numeroDeColumnasAUnir = numeroDeColumnasAUnir - 1
                                        salir = True
                                    End If
                                    If contCol + numeroDeColumnasAUnir >= tab.Columns.Count Then
                                        numeroDeColumnasAUnir = numeroDeColumnasAUnir - 1
                                        salir = True
                                    End If
                                    If numeroDeColumnasAUnir < 0 Then
                                        numeroDeColumnasAUnir = 0
                                    End If
                                End While
                                salir = False
                                While contFil + numeroDeFilasAUnir < tab.Rows.Count And Not salir
                                    If Me.esPosibleAutoUnirCeldasDeTabla(tab, contFil, contCol, contFil + numeroDeFilasAUnir, contCol + numeroDeColumnasAUnir) Then
                                        numeroDeFilasAUnir = numeroDeFilasAUnir + 1
                                    Else
                                        numeroDeFilasAUnir = numeroDeFilasAUnir - 1
                                        salir = True
                                    End If
                                    If contFil + numeroDeFilasAUnir >= tab.Rows.Count Then
                                        numeroDeFilasAUnir = numeroDeFilasAUnir - 1
                                        salir = True
                                    End If
                                    If numeroDeFilasAUnir < 0 Then
                                        numeroDeFilasAUnir = 0
                                    End If
                                End While
                                If numeroDeColumnasAUnir <> 0 Then
                                    tab.Rows(contFil).Item(contCol).MergeRight = numeroDeColumnasAUnir
                                End If
                                If numeroDeFilasAUnir <> 0 Then
                                    tab.Rows(contFil).Item(contCol).MergeDown = numeroDeFilasAUnir
                                End If
                                contCol = contCol + 1
                            End While
                            contFil = contFil + 1
                        End While
                End Select
        End Select
    End Sub
    ''' <summary>
    ''' Función que indica si las dos celdas parámetro se pueden unir. Para ello debe cumplirse que, primero, las dos celdas no estén en la 
    ''' expansión de ninguna otra celda. Además el número de fila de la primera celda debe ser menor o igual que el de la segunda. Además 
    ''' el número de columna de la primera celda debe ser menor o igual que el de la segunda. Además la segunda celda no debe contener elementos,
    ''' ni todas aquellas celdas que están entre la primera y la segunda. Por ejemplo si la primera celda tiene las coordenadas (1,1) y la segunda
    ''' las coordenadas (3,3), salvo la primera celda, todas las demás contenidas en el rectángulo descrito por los siguientes cuatro vértices,
    ''' deben estar vacías de contenidos (rectángulo: P(1,1), P(1,3), P(3,1), P(3,3)). Es decir, las celdas (1,2), (1,3), (2,1), (2,2), (2,3),
    ''' (3,1),(3,2) y (3,3) deben estar todas vacías de contenido.
    ''' </summary>
    ''' <param name="tab">Tabla en la cual residen las celdas</param>
    ''' <param name="nf1">Número de fila de la primera celda</param>
    ''' <param name="nc1">Número de columna de la primera celda</param>
    ''' <param name="nf2">Número de fila de la segunda celda</param>
    ''' <param name="nc2">Número de columna de la segunda celda</param>
    ''' <returns>Si las dos celdas parámetro se pueden unir se devuelve true, si no false</returns>
    ''' <remarks>Llamado desde: CFacturaePDF.esPosibleAutoUnirCeldasDeTabla, CFacturaePDF.autoUnirCeldasDeTabla
    ''' Tiempo máximo: variable, depende del número de celdas existentes entre la primera y la segunda celdas.  
    ''' Revisado por: auv. 28/05/2012</remarks>
    Private Function esPosibleAutoUnirCeldasDeTabla(ByRef tab As Table, ByVal nf1 As Integer, ByVal nc1 As Integer, ByVal nf2 As Integer, ByVal nc2 As Integer) As Boolean
        Dim esPosible As Boolean = Nothing
        Dim par As Paragraph = Nothing
        Dim contCol As Integer = Nothing
        Dim contFil As Integer = Nothing

        esPosible = True

        'Que la celda de origen que se va a expandir no esté bajo la expansión de ninguna anterior
        contCol = 0
        While contCol <= nc1 And esPosible
            contFil = 0
            While contFil <= nf1 And esPosible
                If tab.Rows(contFil).Item(contCol).MergeRight + contCol >= nc1 And tab.Rows(contFil).Item(contCol).MergeDown + contFil >= nf1 _
                    And (Not (contFil = nf1 And contCol = nc1)) Then
                    esPosible = False
                End If
                contFil = contFil + 1
            End While
            contCol = contCol + 1
        End While
        'Que la celda hasta la que se va a expandir no esté bajo la expansión de ninguna anterior
        contCol = 0
        While contCol <= nc2 And esPosible
            contFil = 0
            While contFil <= nf2 And esPosible
                If tab.Rows(contFil).Item(contCol).MergeRight + contCol >= nc2 And tab.Rows(contFil).Item(contCol).MergeDown + contFil >= nf2 _
                    And (Not (contFil = nf2 And contCol = nc2) Or (contFil = nf2 And contCol = nc2 And (tab.Rows(contFil).Item(contCol).MergeRight <> 0 Or tab.Rows(contFil).Item(contCol).MergeDown <> 0))) Then
                    esPosible = False
                End If
                contFil = contFil + 1
            End While
            contCol = contCol + 1
        End While
        If esPosible Then
            If nf1 <> nf2 Then
                If nf1 > nf2 Then
                    esPosible = False
                Else
                    If nc1 <> nc2 Then
                        If nc1 > nc2 Then
                            esPosible = False
                        Else
                            esPosible = (tab.Rows(nf2).Item(nc1).Elements.Count = 0) And Me.esPosibleAutoUnirCeldasDeTabla(tab, nf2, nc1, nf2, nc2) And _
                                Me.esPosibleAutoUnirCeldasDeTabla(tab, nf1, nc1, nf2 - 1, nc2)
                        End If
                    Else
                        esPosible = (tab.Rows(nf2).Item(nc2).Elements.Count = 0) And Me.esPosibleAutoUnirCeldasDeTabla(tab, nf1, nc1, nf2 - 1, nc2)
                    End If
                End If
            Else
                If nc1 <> nc2 Then
                    If nc1 > nc2 Then
                        esPosible = False
                    Else
                        esPosible = (tab.Rows(nf2).Item(nc2).Elements.Count = 0) And Me.esPosibleAutoUnirCeldasDeTabla(tab, nf1, nc1, nf2, nc2 - 1)
                    End If
                Else
                    esPosible = True
                End If
            End If
        End If

        Return esPosible
    End Function
    ''' <summary>
    ''' El vector parámetro describe anchos de columnas. Se puede imaginar una tabla con un número de tablas igual al número de elementos del
    ''' vector y en la que la anchura de cada columna de la tabla corresponde con las anchuras especificadas en el vector. Entonces éste método
    ''' indica si se necesita algún ajuste (columna auxiliar invisible) para que la tabla salga centrada horizontalmente dentro de los folios 
    ''' del PDF a imprimir
    ''' </summary>
    ''' <param name="section">Sección en la cual se pretende agregar una tabla</param>
    ''' <param name="anchCol">Anchos prototipo de las columnas de la futura tabla</param>
    ''' <returns>Indica si se necesitará un ajuste o no</returns>
    ''' <remarks>Llamado desde: CFacturaePDF.constTab
    ''' Tiempo máximo: menor a 0,1
    ''' Revisado por: auv. 28/05/2012</remarks>
    Private Function neceAjus(ByRef section As Section, ByRef anchCol() As Double) As Boolean
        Dim nece As Boolean = Nothing
        Dim cc As Integer = Nothing
        Dim anchTab As Double = Nothing

        cc = 0 : anchTab = 0
        While cc <= UBound(anchCol)
            anchTab = anchTab + anchCol(cc)
            cc = cc + 1
        End While

        If ((section.Document.DefaultPageSetup.PageWidth.Centimeter - anchTab) / 2) - section.PageSetup.LeftMargin.Centimeter > 0 Then
            nece = True
        Else
            nece = False
        End If

        Return nece
    End Function
    ''' <summary>
    ''' Función que devuelve un valor númerico no entero que representa el ancho de la columna auxiliar invisible a itroducir en la tabla futura
    ''' </summary>
    ''' <param name="section">Sección en la cual se desea calcular el ajuste</param>
    ''' <param name="anchCol">Vector que representa los anchos de columna de la futura tabla</param>
    ''' <returns></returns>
    ''' <remarks>Llamado desde: CFacturaePDF.constTab
    ''' Tiempo máximo:    
    ''' Revisado por: auv. 28/05/2012</remarks>
    Private Function calcAjus(ByRef section As Section, ByRef anchCol() As Double) As Double
        Dim ajus As Double = Nothing
        Dim cc As Integer = Nothing
        Dim anchTab As Double = Nothing

        cc = 0 : anchTab = 0
        While cc <= UBound(anchCol)
            anchTab = anchTab + anchCol(cc)
            cc = cc + 1
        End While

        ajus = ((section.Document.DefaultPageSetup.PageWidth.Centimeter - anchTab) / 2) - section.PageSetup.LeftMargin.Centimeter

        Return ajus
    End Function
    '---------------------------------------------------------------------------------------------------------------------------------------------------------------
    'Conversiones de OBJETO a PDF
    '---------------------------------------------------------------------------------------------------------------------------------------------------------------
    ''' <summary>
    ''' Transforma el código de facturae v3.2 para tipos de persona a texto en el idioma de la plataforma FS
    ''' </summary>
    ''' <param name="sCodPerFacturae">Código de facturae v3.2 del tipo de persona</param>
    ''' <returns>Un string en el idioma de la plataforma FS que es la descripción del tipo de persona</returns>
    ''' <remarks>Llamado desde: CFacturaePDF.escribirEmisorTablaInformacionGeneral, CFacturaePDF.escribirReceptor
    ''' Tiempo máximo: menor a 0,1   
    ''' Revisado por: auv. 28/05/2012</remarks>
    Private Function personTypeCode_Obj2PDF(ByVal sCodPerFacturae As String) As String
        Dim tipoPersona As String = ""

        Select Case sCodPerFacturae
            Case PersonTypeCodeType.PERSONA_FISICA_F
                tipoPersona = CStr(oTextos(57)(1))
            Case PersonTypeCodeType.PERSONA_JURIDICA_J
                tipoPersona = CStr(oTextos(58)(1))
        End Select

        Return tipoPersona
    End Function
    ''' <summary>
    ''' Trasnforma el código de facturae v3.2 para tipos de residencia a texto en el idioma de la plataforma FS
    ''' </summary>
    ''' <param name="sCodResFacturae">Código de facturae v3.2 del tipo de residencia</param>
    ''' <returns>Un string en el idioma de la plataforma FS que es la descripción del tipo de residencia</returns>
    ''' <remarks>Llamado desde: CFacturaePDF.escribirEmisorTablaInformacionGeneral, CFacturaePDF.escribirReceptor
    ''' Tiempo máximo: menor a 0,1    
    ''' Revisado por: auv. 28/05/2012</remarks>
    Private Function residenceTypeCode_Obj2PDF(ByVal sCodResFacturae As String) As String
        Dim tipoResidencia As String = ""

        Select Case sCodResFacturae
            Case ResidenceTypeCodeType.RESIDENTE_EN_ESPANYA_R
                tipoResidencia = CStr(oTextos(59)(1))
            Case ResidenceTypeCodeType.RESIDENTE_EN_UNION_EUROPEA_U
                tipoResidencia = CStr(oTextos(60)(1))
            Case ResidenceTypeCodeType.EXTRANJERO_E
                tipoResidencia = CStr(oTextos(61)(1))
        End Select

        Return tipoResidencia
    End Function
    ''' <summary>
    ''' Transforma el código de facturae v3.2 para tipos de impuesto a texto en el idioma de la plataforma FS
    ''' </summary>
    ''' <param name="sTaxTypeCode">Código de facturae v3.2 del tipo de impuesto</param>
    ''' <returns>Un string en el idioma de la plataforma FS que es la descripción del tipo de impuesto</returns>
    ''' <remarks>Llamado desde: CFacturaePDF.escribirAlbaranTablaImpuestosRepercutidos, 
    ''' CFacturaePDF.escribirAlbaranTablaImpuestosRetenidos, CFacturaePDF.escribirItemImportesTablaImpuestosRepercutidos, 
    ''' CFacturaePDF.escribirItemImportesTablaImpuestosRetenidos, CFacturaePDF.escribirImpuestosRepercutidos, 
    ''' CFacturaePDF.escribirImpuestosRetenidos
    ''' Tiempo máximo: menor a 0,1  
    ''' Revisado por: auv. 28/05/2012</remarks>
    Private Function taxTypeCode_Obj2PDF(ByVal sTaxTypeCode As String) As String
        Dim sNomTaxTypCodPDF As String = ""

        Select Case sTaxTypeCode
            Case TaxTypeCodeType.IMPUESTO_IVA_01
                sNomTaxTypCodPDF = CStr(oTextos(62)(1))
            Case TaxTypeCodeType.IMPUESTO_IPSI_02
                sNomTaxTypCodPDF = CStr(oTextos(63)(1))
            Case TaxTypeCodeType.IMPUESTO_IGIC_03
                sNomTaxTypCodPDF = CStr(oTextos(64)(1))
            Case TaxTypeCodeType.IMPUESTO_IRPF_04
                sNomTaxTypCodPDF = CStr(oTextos(65)(1))
            Case TaxTypeCodeType.OTRO_IMPUESTO_05
                sNomTaxTypCodPDF = CStr(oTextos(66)(1))
            Case TaxTypeCodeType.IMPUESTO_ITPAJD_06
                sNomTaxTypCodPDF = CStr(oTextos(67)(1))
            Case TaxTypeCodeType.IMPUESTO_IE_07
                sNomTaxTypCodPDF = CStr(oTextos(68)(1))
            Case TaxTypeCodeType.IMPUESTO_RA_08
                sNomTaxTypCodPDF = CStr(oTextos(69)(1))
            Case TaxTypeCodeType.IMPUESTO_IGTECM_09
                sNomTaxTypCodPDF = CStr(oTextos(70)(1))
            Case TaxTypeCodeType.IMPUESTO_IECDPCAC_10
                sNomTaxTypCodPDF = CStr(oTextos(71)(1))
            Case TaxTypeCodeType.IMPUESTO_IIIMAB_11
                sNomTaxTypCodPDF = CStr(oTextos(72)(1))
            Case TaxTypeCodeType.IMPUESTO_ICIO_12
                sNomTaxTypCodPDF = CStr(oTextos(73)(1))
            Case TaxTypeCodeType.IMPUESTO_IMVDN_13
                sNomTaxTypCodPDF = CStr(oTextos(74)(1))
            Case TaxTypeCodeType.IMPUESTO_IMSN_14
                sNomTaxTypCodPDF = CStr(oTextos(75)(1))
            Case TaxTypeCodeType.IMPUESTO_IMGSN_15
                sNomTaxTypCodPDF = CStr(oTextos(76)(1))
            Case TaxTypeCodeType.IMPUESTO_IMPN_16
                sNomTaxTypCodPDF = CStr(oTextos(77)(1))
            Case TaxTypeCodeType.IMPUESTO_REIVA_17
                sNomTaxTypCodPDF = CStr(oTextos(78)(1))
            Case TaxTypeCodeType.IMPUESTO_REIGIC_18
                sNomTaxTypCodPDF = CStr(oTextos(79)(1))
            Case TaxTypeCodeType.IMPUESTO_REIPSI_19
                sNomTaxTypCodPDF = CStr(oTextos(80)(1))
        End Select

        Return sNomTaxTypCodPDF
    End Function
    ''' <summary>
    ''' Transforma el código de facturae v3.2 para descripciones de razones de rectificación a texto en el idioma
    ''' de la plataforma FS
    ''' </summary>
    ''' <param name="sReasonDescription">Código de facturae v3.2 de descripciones de razones de rectificación</param>
    ''' <returns>Un string en el idioma de la plataforma FS que es la descripción de la razón de rectificación</returns>
    ''' <remarks>Llamado desde: CFacturaePDF.escribirCorrectiva
    ''' Tiempo máximo: menor a 0,1    
    ''' Revisado por: auv. 28/05/2012</remarks>
    Private Function reasonDescription_Obj2PDF(ByVal sReasonDescription As String) As String
        Dim sNomReaDesPDF As String = ""

        Select Case sReasonDescription
            Case ReasonDescriptionType.BASE_IMPONIBLE
                sNomReaDesPDF = CStr(oTextos(87)(1))
            Case ReasonDescriptionType.BASE_IMPONIBLE_MODIFICADA_CUOTAS_REPERCUTIDAS_NO_SATISFECHAS
                sNomReaDesPDF = CStr(oTextos(92)(1))
            Case ReasonDescriptionType.BASE_IMPONIBLE_MODIFICADA_POR_DESCUENTOS_Y_BONIFICACIONES
                sNomReaDesPDF = CStr(oTextos(90)(1))
            Case ReasonDescriptionType.BASE_IMPONIBLE_MODIFICADA_POR_DEVOLUCION_DE_ENVASES
                sNomReaDesPDF = CStr(oTextos(93)(1))
            Case ReasonDescriptionType.BASE_IMPONIBLE_MODIFICADA_POR_RESOLUCION_FIRME_JUDICIAL_O_ADMINISTRATIVA
                sNomReaDesPDF = CStr(oTextos(91)(1))
            Case ReasonDescriptionType.CALCULO_DE_CUOTAS_REPERCUTIDAS
                sNomReaDesPDF = CStr(oTextos(88)(1))
            Case ReasonDescriptionType.CALCULO_DE_CUOTAS_RETENIDAS
                sNomReaDesPDF = CStr(oTextos(89)(1))
            Case ReasonDescriptionType.CLASE_DE_FACTURA
                sNomReaDesPDF = CStr(oTextos(85)(1))
            Case ReasonDescriptionType.CUOTA_TRIBUTARIA_A_APLICAR
                sNomReaDesPDF = CStr(oTextos(84)(1))
            Case ReasonDescriptionType.DETALLE_OPERACION
                sNomReaDesPDF = CStr(oTextos(94)(1))
            Case ReasonDescriptionType.DOMICILIO_EMISOR
                sNomReaDesPDF = CStr(oTextos(95)(1))
            Case ReasonDescriptionType.DOMICILIO_RECEPTOR
                sNomReaDesPDF = CStr(oTextos(96)(1))
            Case ReasonDescriptionType.FECHA_EXPEDICION
                sNomReaDesPDF = CStr(oTextos(97)(1))
            Case ReasonDescriptionType.FECHA_PERIODO_A_APLICAR
                sNomReaDesPDF = CStr(oTextos(98)(1))
            Case ReasonDescriptionType.IDENTIFICADOR_FISCAL_EMISOR
                sNomReaDesPDF = CStr(oTextos(99)(1))
            Case ReasonDescriptionType.IDENTIFICADOR_FISCAL_RECEPTOR
                sNomReaDesPDF = CStr(oTextos(100)(1))
            Case ReasonDescriptionType.LITERALES_LEGALES
                sNomReaDesPDF = CStr(oTextos(86)(1))
            Case ReasonDescriptionType.NOMBRE_APELLIDOS_O_RAZON_SOCIAL_EMISOR
                sNomReaDesPDF = CStr(oTextos(101)(1))
            Case ReasonDescriptionType.NOMBRE_APELLIDOS_O_RAZON_SOCIAL_RECEPTOR
                sNomReaDesPDF = CStr(oTextos(102)(1))
            Case ReasonDescriptionType.NUMERO_FACTURA
                sNomReaDesPDF = CStr(oTextos(81)(1))
            Case ReasonDescriptionType.PORCENTAJE_IMPOSITIVO_A_APLICAR
                sNomReaDesPDF = CStr(oTextos(83)(1))
            Case ReasonDescriptionType.SERIE_FACTURA
                sNomReaDesPDF = CStr(oTextos(82)(1))
        End Select

        Return sNomReaDesPDF
    End Function
    ''' <summary>
    ''' Transforma el código de facturae v3.2 para descripciones de métodos de corrección a texto en el idioma
    ''' de la plataforma FS
    ''' </summary>
    ''' <param name="sCorrectionMethodDescription">Código de facturae v3.2 de descripcion del método de corrección</param>
    ''' <returns>Un string en el idioma de la plataforma FS que es la descripción del método de corrección</returns>
    ''' <remarks>Llamado desde: CFacturaePDF.escribirCorrectiva
    ''' Tiempo máximo: menor a 0,1   
    ''' Revisado por: auv. 28/05/2012</remarks>
    Private Function correctionMethodDescription_Obj2PDF(ByVal sCorrectionMethodDescription As String) As String
        Dim sNomCorMetDesPDF As String = ""

        Select Case sCorrectionMethodDescription
            Case CorrectionMethodDescriptionType.AUTORIZADAS_POR_LA_AGENCIA_TRIBUTARIA
                sNomCorMetDesPDF = CStr(oTextos(103)(1))
            Case CorrectionMethodDescriptionType.RECTIFICACION_INTEGRA
                sNomCorMetDesPDF = CStr(oTextos(104)(1))
            Case CorrectionMethodDescriptionType.RECTIFICACION_POR_DESCUENTO
                sNomCorMetDesPDF = CStr(oTextos(105)(1))
            Case CorrectionMethodDescriptionType.RECTIFICACION_POR_DIFERENCIAS
                sNomCorMetDesPDF = CStr(oTextos(106)(1))
        End Select

        Return sNomCorMetDesPDF
    End Function
    '----------------------------------------------------------------------------------------------------------------------------------------
    'AUXILIARES
    '----------------------------------------------------------------------------------------------------------------------------------------
    ''' <summary>
    ''' Transforma un objeto Date de entrada a un String que representa una fecha en un XML que sigue el estándar
    ''' Facturae v3.2.
    ''' </summary>
    ''' <param name="objIdDate">Objeto que contiene la fecha a transformar.</param>
    ''' <returns>String que representa una fecha en un XML que sigue el estándar Facturae v3.2.</returns>
    ''' <remarks>Llamado desde: CFacturaePDF.escribirCorrectiva
    ''' Tiempo máximo: menor a 0,1
    ''' Revisado por: auv. 28/05/2012</remarks>
    Private Function date_ObjId2Str(ByVal objIdDate As Date) As String
        Dim XMLStrDate As String = Nothing

        XMLStrDate = objIdDate.Day.ToString
        While XMLStrDate.Length < 2
            XMLStrDate = "0" & XMLStrDate
        End While
        XMLStrDate = objIdDate.Month.ToString & "-" & XMLStrDate
        While XMLStrDate.Length < 5
            XMLStrDate = "0" & XMLStrDate
        End While
        XMLStrDate = objIdDate.Year.ToString & "-" & XMLStrDate
        While XMLStrDate.Length < 10
            XMLStrDate = "0" & XMLStrDate
        End While

        Return XMLStrDate
    End Function
    ''' <summary>
    ''' Devuelve un String que representa el caracter separador que emplea el tipo double para distinguir cuándo 
    ''' empiezan los decimales.
    ''' </summary>
    ''' <returns>Se devuelve un caracter que resulta ser el caracter que separa las unidades enteras de las decimales, 
    ''' en los double</returns>
    ''' <remarks>Llamado desde: CFacturaePDF.str2Dbl, CFacturaePDF.dbl2Str
    ''' Tiempo máximo: menor a 0,1    
    ''' Revisado por: auv. 28/05/2012</remarks>
    Private Function obtSeparadorDbl() As String
        Dim resultado As String = Nothing

        If CDbl("0,1") = CDbl("1") Then
            resultado = "."
        Else
            resultado = ","
        End If

        Return resultado
    End Function
    ''' <summary>
    ''' Transforma un número no entero contenido en un String a uno contenido en un double.
    ''' </summary>
    ''' <param name="sValue">Número no entero contenido en un String</param>
    ''' <returns>Número no entero contenido en un double</returns>
    ''' <remarks>Llamado desde: CFacturaePDF.escribirTotales, CFacturaePDF.escribirAlbaranTablaTotales, 
    ''' CFacturaePDF.escribirAlbaranTablaCargos, CFacturaePDF.escribirAlbaranTablaDescuentos, 
    ''' CFacturaePDF.escribirAlbaranTablaImpuestosRepercutidos, CFacturaePDF.escribirAlbaranTablaImpuestosRetenidos, 
    ''' CFacturaePDF.escribirItemImportesTablaCargos, CFacturaePDF.escribirItemImportesTablaDescuentos, 
    ''' CFacturaePDF.escribirItemImportesTablaImporteBruto, CFacturaePDF.escribirItemImportesTablaImpuestosRepercutidos, 
    ''' CFacturaePDF.escribirItemImportesTablaImpuestosRetenidos, CFacturaePDF.escribirCargosGenerales, 
    ''' CFacturaePDF.escribirDescuentosGenerales, CFacturaePDF.escribirImpuestosRepercutidos, 
    ''' CFacturaePDF.escribirImpuestosRetenidos, CFacturaePDF.escribirRetencionesDeGarantia, 
    ''' CFacturaePDF.agruparImpuestosRepercutidos, CFacturaePDF.agruparImpuestosRetenidos
    ''' Tiempo máximo: menor a 0,1   
    ''' Revisado por: auv. 28/05/2012</remarks>
    Private Function str2Dbl(ByVal sValue As String) As Double
        Dim resultado As Double = 0
        resultado = CDbl((sValue.Replace(",", obtSeparadorDbl)).Replace(".", obtSeparadorDbl))
        Return resultado
    End Function
    ''' <summary>
    ''' Transforma un double a un String, dejando como caracter separador del String el caracter indicado por parámetro
    ''' </summary>
    ''' <param name="dValue">Número no entero a transformar a String</param>
    ''' <param name="separador">Caracter separador de decimales a emplear en el String a devolver</param>
    ''' <returns>String que representa un Número no entero</returns>
    ''' <remarks>Llamado desde: CFacturaePDF.escribirTotales, CFacturaePDF.escribirAlbaranTablaTotales, 
    ''' CFacturaePDF.escribirAlbaranTablaCargos, CFacturaePDF.escribirAlbaranTablaDescuentos, 
    ''' CFacturaePDF.escribirAlbaranTablaImpuestosRepercutidos, CFacturaePDF.escribirAlbaranTablaImpuestosRetenidos, 
    ''' CFacturaePDF.escribirItemTablaDatosGenerales, CFacturaePDF.escribirItemImportesTablaCargos, 
    ''' CFacturaePDF.escribirItemImportesTablaDescuentos, CFacturaePDF.escribirItemImportesTablaImporteBruto, 
    ''' CFacturaePDF.escribirItemImportesTablaImpuestosRepercutidos, CFacturaePDF.escribirItemImportesTablaImpuestosRetenidos, 
    ''' CFacturaePDF.escribirCargosGenerales, CFacturaePDF.escribirDescuentosGenerales, CFacturaePDF.escribirImpuestosRepercutidos, 
    ''' CFacturaePDF.escribirImpuestosRetenidos, CFacturaePDF.escribirRetencionesDeGarantia, CFacturaePDF.agruparImpuestosRepercutidos, 
    ''' CFacturaePDF.agruparImpuestosRetenidos
    ''' Tiempo máximo: menor a 0,1
    ''' Revisado por: auv. 28/05/2012</remarks>
    Private Function dbl2str(ByVal dValue As Double, ByVal separador As String) As String
        Return CStr(dValue).Replace(obtSeparadorDbl, separador)
    End Function
End Class