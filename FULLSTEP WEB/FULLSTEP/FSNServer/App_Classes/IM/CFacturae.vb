﻿Imports System.Data.SqlClient
Imports System.Xml

<Serializable()> _
Public Class CFacturae
    Inherits Security

    Public fileHeader As FileHeaderType
    Public parties As PartiesType
    Public invoices() As InvoiceType

    ''' <summary>
    ''' Constructura del objeto. Solo llama a la constructora de su superclase y establece en valores iniciales los atributos
    ''' de instancia que definen parte de la estructura de un objeto facturae.
    ''' </summary>
    ''' <param name="poDbserver">Objeto para el acceso a base de datos</param>
    ''' <param name="pbIsAuthenticated">Si está autenticado o no</param>
    ''' <remarks>Llamada desde: ¿?
    ''' Tiempo máximo: menor a 0,1
    ''' Revisado por: auv. 18/05/2012</remarks>
    Public Sub New(ByRef poDbserver As FSNDatabaseServer.Root, ByVal pbIsAuthenticated As Boolean)
        MyBase.New(poDbserver, pbIsAuthenticated)
        Me.fileHeader = Nothing
        Me.parties = Nothing
        Me.invoices = Nothing
    End Sub
    ''' <summary>
    ''' Constructura del objeto. Aparte de la llamada a la superclase se construye la estructura interna de la instancia.
    ''' Para ello se usa el parámetro poXMLFac como fuente de datos.
    ''' </summary>
    ''' <param name="poDbserver">Objeto para el acceso a base de datos</param>
    ''' <param name="pbIsAuthenticated">Si está autenticado o no</param>
    ''' <param name="poXmlFac"></param>
    ''' <remarks>Llamada desde: CFacturaePDF.crearPDFFacV3_2
    ''' Tiempo máximo: menor a 0,1
    ''' Revisado por: auv. 28/05/2012</remarks>
    Public Sub New(ByRef poDbserver As FSNDatabaseServer.Root, ByVal pbIsAuthenticated As Boolean, ByRef poXmlFac As XmlDocument)
        MyBase.New(poDbserver, pbIsAuthenticated)
        Me.facturaeXML2Obj(poXmlFac)
    End Sub
    ''' <summary>
    ''' Constructura del objeto. Es privada para que no se le hagan llamadas, se deberían emplear alguna constructora que
    ''' no sea ésta.
    ''' </summary>
    ''' <remarks>Llamada desde: Ningún sitio.
    ''' Tiempo máximo: menor a 0,1 
    ''' Revisado por: auv. 28/05/2012</remarks>
    Private Sub New()
    End Sub
#Region "InnerClasses"
    <Serializable()> _
    Public Class FileHeaderType
        Public schemaVersion As SchemaVersionType
        Public modality As ModalityType
        Public invoiceIssuerType As InvoiceIssuerTypeType
        Public batch As BatchType

        ''' <summary>
        ''' Constructura del objeto. Solo establece en valores iniciales los atributos
        ''' de instancia que definen parte de la estructura de un objeto facturae.
        ''' </summary>
        ''' <remarks>Llamada desde: CFacturae.construirFacturae, CFacturae.facturaeXML2Obj
        ''' Tiempo máximo: menor a 0,1
        ''' Revisado por: auv. 18/05/2012</remarks>
        Public Sub New()
            Me.schemaVersion = Nothing
            Me.modality = Nothing
            Me.invoiceIssuerType = Nothing
            Me.batch = Nothing
        End Sub
    End Class

    <Serializable()> _
    Public Class SchemaVersionType
        Public value As String
        Public Const ITEM32 As String = "3.2"

        ''' <summary>
        ''' Constructura del objeto. Solo establece en valores iniciales los atributos
        ''' de instancia que definen parte de la estructura de un objeto facturae.
        ''' </summary>
        ''' <remarks>Llamada desde: CFacturae.construirFacturae, CFacturae.fileHeaderXML2Obj
        ''' Tiempo máximo: menor a 0,1
        ''' Revisado por: auv. 18/05/2012</remarks>
        Public Sub New()
            Me.value = ""
        End Sub

        Protected Overrides Sub Finalize()
            MyBase.Finalize()
        End Sub
    End Class

    <Serializable()> _
    Public Class ModalityType
        Public value As String
        Public Const INDIVIDUAL_I As String = "I"
        Public Const LOTE_L As String = "L"

        ''' <summary>
        ''' Constructura del objeto. Solo establece en valores iniciales los atributos
        ''' de instancia que definen parte de la estructura de un objeto facturae.
        ''' </summary>
        ''' <remarks>Llamada desde: CFacturae.construirFacturae, CFacturae.fileHeaderXML2Obj
        ''' Tiempo máximo: menor a 0,1
        ''' Revisado por: auv. 18/05/2012</remarks>
        Public Sub New()
            Me.value = ""
        End Sub
    End Class

    <Serializable()> _
    Public Class InvoiceIssuerTypeType
        Public value As String
        Public Const EMISOR_EM As String = "EM"
        Public Const RECEPTOR_RE As String = "RE"
        Public Const TERCERO_TE As String = "TE"

        ''' <summary>
        ''' Constructura del objeto. Solo establece en valores iniciales los atributos
        ''' de instancia que definen parte de la estructura de un objeto facturae.
        ''' </summary>
        ''' <remarks>Llamada desde: CFacturae.construirFacturae, CFacturae.fileHeaderXML2Obj
        ''' Tiempo máximo: menor a 0,1
        ''' Revisado por: auv. 18/05/2012</remarks>
        Public Sub New()
            Me.value = ""
        End Sub
    End Class

    <Serializable()> _
    Public Class BatchType
        Private m_oBatchIdentifier As String
        Public Property batchIdentifier As String
            Get
                Return Me.m_oBatchIdentifier
            End Get
            Set(ByVal value As String)
                If value.Length > 70 Then
                    Me.m_oBatchIdentifier = value.Substring(0, 70)
                Else
                    Me.m_oBatchIdentifier = value
                End If
            End Set
        End Property
        Public invoicesCount As Long
        Public totalInvoicesAmount As AmountType
        Public totalOutstandingAmount As AmountType
        Public totalExecutableAmount As AmountType
        Public invoiceCurrencyCode As CurrencyCodeType

        ''' <summary>
        ''' Constructura del objeto. Solo establece en valores iniciales los atributos
        ''' de instancia que definen parte de la estructura de un objeto facturae.
        ''' </summary>
        ''' <remarks>Llamada desde: CFacturae.construirBatch, CFacturae.fileHeaderXML2Obj
        ''' Tiempo máximo: menor a 0,1
        ''' Revisado por: auv. 18/05/2012</remarks>
        Public Sub New()
            Me.batchIdentifier = ""
            Me.invoicesCount = 0
            Me.totalInvoicesAmount = Nothing
            Me.totalOutstandingAmount = Nothing
            Me.totalExecutableAmount = Nothing
            Me.invoiceCurrencyCode = Nothing
        End Sub
    End Class

    <Serializable()> _
    Public Class AmountType
        Public totalAmount As DoubleTwoDecimalType
        Public equivalentInEuros As DoubleTwoDecimalType

        ''' <summary>
        ''' Constructura del objeto. Solo establece en valores iniciales los atributos
        ''' de instancia que definen parte de la estructura de un objeto facturae.
        ''' </summary>
        ''' <remarks>Llamada desde: CFacturae.construirBatch, CFacturae.construirItems, CFacturae.construirTaxesOutputs, 
        ''' CFacturae.construirTaxesWithheld, CFacturae.batchXML2Obj, CFacturae.invoiceTaxOutputXML2Obj, 
        ''' CFacturae.invoiceTaxWithheldXML2Obj, CFacturae.invoiceItemTaxWithheldXML2Obj, CFacturae.invoiceItemTaxOutputXML2Obj
        ''' Tiempo máximo: menor a 0,1
        ''' Revisado por: auv. 18/05/2012</remarks>
        Public Sub New()
            Me.totalAmount = Nothing
            Me.equivalentInEuros = Nothing
        End Sub
    End Class

    <Serializable()> _
    Public Class DoubleTwoDecimalType
        Public value As Double

        ''' <summary>
        ''' Constructura del objeto. Solo establece en valores iniciales los atributos
        ''' de instancia que definen parte de la estructura de un objeto facturae.
        ''' </summary>
        ''' <remarks>Llamada desde: CFacturae.batchXML2Obj, CFacturae.invoiceIssueDataXML2Obj, CFacturae.invoiceTaxOutputXML2Obj, 
        ''' CFacturae.invoiceTaxWithheldXML2Obj, CFacturae.invoiceTotalsXML2Obj, CFacturae.invoiceItemTaxWithheldXML2Obj, 
        ''' CFacturae.invoiceItemTaxOutputXML2Obj
        ''' Tiempo máximo: menor a 0,1
        ''' Revisado por: auv. 18/05/2012</remarks>
        Public Sub New()
            value = 0
        End Sub
        Public Sub New(ByVal value As Double)
            Me.value = value
        End Sub
        Public Shared Widening Operator CType(ByVal o As DoubleTwoDecimalType) As Double
            Return o.value
        End Operator
        Public Shared Widening Operator CType(ByVal value As Double) As DoubleTwoDecimalType
            Return New DoubleTwoDecimalType(value)
        End Operator
        Public Property dValue() As String
            Get
                Return value.ToString("F2", System.Globalization.CultureInfo.InvariantCulture)
            End Get
            Set(ByVal value As String)
                Dim separadorDbl As String
                If CDbl("0,1") = CDbl("1") Then
                    separadorDbl = "."
                Else
                    separadorDbl = ","
                End If
                Me.value = System.Convert.ToDouble(CDbl((value.Replace(",", separadorDbl)).Replace(".", separadorDbl)))
            End Set
        End Property
    End Class

    <Serializable()> _
    Public Class CurrencyCodeType 'http://www.currency-iso.org/iso_index/iso_tables/iso_tables_a1.htm
        Public value As String
        Public Const AFGANI_AFGHANISTAN_AFN As String = "AFN"
        Public Const LEK_ALBANIA_ALL As String = "ALL"
        Public Const DRAM_ARMENIA_AMD As String = "AMD"
        Public Const FLORIN_ANTILLAS_HOLANDESAS_ANG As String = "ANG"
        Public Const KWANZA_ANGOLA_AOA As String = "AOA"
        Public Const PESO_ARGENTINA_ARS As String = "ARS"
        Public Const DOLAR_AUSTRALIA_AUD As String = "AUD"
        Public Const FLORIN_ARUBA_AWG As String = "AWG"
        Public Const MANAT_AZERBAIJAN_AZN As String = "AZN"
        Public Const DINAR_BOSNIA_Y_HERZEGOVINA_BAD As String = "BAD" 'http://en.wikipedia.org/wiki/Bosnia_and_Herzegovina_dinar
        Public Const DOLAR_BARBADOS_BBD As String = "BBD"
        Public Const TAKA_BANGLADESH_BDT As String = "BDT"
        Public Const LEV_BULGARIA_BGN As String = "BGN"
        Public Const DINAR_BAHREIN_BHD As String = "BHD"
        Public Const FRANCO_BURUNDI_BIF As String = "BIF"
        Public Const DOLAR_BERMUDAS_BMD As String = "BMD"
        Public Const DOLAR_BRUNEI_DARUSSALAM_BND As String = "BND"
        Public Const BOLIVIANO_BOLIVIA_BOB As String = "BOB"
        Public Const REAL_BRASIL_BRL As String = "BRL"
        Public Const CRUCEIRO_REAL_BRASIL_BRR As String = "BRR" 'http://pt.wikipedia.org/wiki/ISO_4217
        Public Const DOLAR_BAHAMAS_BSD As String = "BSD"
        Public Const PULA_BOTSWANA_BWP As String = "BWP"
        Public Const RUBLO_BIELORRUSIA_BYR As String = "BYR"
        Public Const DOLAR_BELICE_BZD As String = "BZD"
        Public Const DOLAR_CANADA_CAD As String = "CAD"
        Public Const FRANCO_REPUBLICA_DEMOCRATICA_DEL_CONGO_CDF As String = "CDF"
        Public Const PESO_SANTO_DOMINGO_CDP As String = "CDP" 'REVISAR ¿?
        Public Const FRANCO_SUIZA_CHF As String = "CHF"
        Public Const PESO_CHILE_CLP As String = "CLP"
        Public Const YUAN_RENMIBI_CHINA_CNY As String = "CNY"
        Public Const PESO_COLOMBIA_COP As String = "COP"
        Public Const COLON_COSTA_RICA_CRC As String = "CRC"
        Public Const PESO_CUBA_CUP As String = "CUP"
        Public Const ESCUDO_CABO_VERDE_CVE As String = "CVE"
        Public Const CORONA_REPUBLICA_CHECA_CZK As String = "CZK"
        Public Const FRANCO_YIBUTI_DJF As String = "DJF"
        Public Const CORONA_DINAMARCA_DKK As String = "DKK"
        Public Const PESO_REPUBLICA_DOMINICANA_DOP As String = "DOP"
        Public Const PESO_REPUBLICA_DOMINICANA_DRP As String = "DRP" 'http://www.hypergrove.com/Publications/OldSymbols.html
        Public Const DINAR_ARGELIA_DZD As String = "DZD"
        Public Const CORONA_ESTONIA_EEK As String = "EEK" 'http://pt.wikipedia.org/wiki/ISO_4217
        Public Const LIBRA_EGIPTO_EGP As String = "EGP"
        Public Const PESETA_ESPANYA_ESP As String = "ESP" 'http://pt.wikipedia.org/wiki/ISO_4217
        Public Const BIRR_ETIOPE_ETB As String = "ETB"
        Public Const EURO_UNION_EUROPEA_EUR As String = "EUR"
        Public Const DOLAR_FIJI_FJD As String = "FJD"
        Public Const LIBRA_ISLAS_MALVINAS_FKP As String = "FKP"
        Public Const LIBRA_ESTERLINA_REINO_UNIDO_GBP As String = "GBP"
        Public Const CUPON_GEORGIA_GEK As String = "GEK" 'http://www.hypergrove.com/Publications/OldSymbols.html
        Public Const CEDI_GHANA_GHC As String = "GHC" 'http://www.hypergrove.com/Publications/OldSymbols.html
        Public Const LIBRA_GIBRALTAR_GIP As String = "GIP"
        Public Const DALASI_GAMBIA_GMD As String = "GMD"
        Public Const FRANCO_GUINEA_GNF As String = "GNF"
        Public Const QUETZAL_GUATEMALA_GTQ As String = "GTQ"
        Public Const PESO_GUINEA_BISSAU_GWP As String = "GWP" 'http://www.hypergrove.com/Publications/OldSymbols.html
        Public Const DOLAR_GUYANA_GYD As String = "GYD"
        Public Const DOLAR_HONG_KONG_HKD As String = "HKD"
        Public Const LEMPIRA_HONDURAS_HNL As String = "HNL"
        Public Const KUNA_CROACIA_HRK As String = "HRK"
        Public Const GOURDE_HAITI_HTG As String = "HTG"
        Public Const FORINT_HUNGRIA_HUF As String = "HUF"
        Public Const RUPIA_INDONESIA_IDR As String = "IDR"
        Public Const NUEVO_SHEKEL_ISRAEL_ILS As String = "ILS"
        Public Const RUPIA_INDIA_INR As String = "INR"
        Public Const DINAR_IRAQ_IQD As String = "IQD"
        Public Const RIAL_IRAN_IRR As String = "IRR"
        Public Const CORONA_ISLANDIA_ISK As String = "ISK"
        Public Const DOLAR_JAMAICA_JMD As String = "JMD"
        Public Const DINAR_JORDANO_JOD As String = "JOD"
        Public Const YEN_JAPONES_JPY As String = "JPY"
        Public Const CHELIN_KENIATA_KES As String = "KES"
        Public Const SOM_KIRGUIZISTAN_KGS As String = "KGS"
        Public Const RIEL_CAMBOYANO_KHR As String = "KHR"
        Public Const FRANCO_ISLAS_COMORES_KMF As String = "KMF"
        Public Const WON_COREA_DEL_NORTE_KPW As String = "KPW"
        Public Const WON_COREA_DEL_SUR_KRW As String = "KRW"
        Public Const DINAR_KUWAIT_KWD As String = "KWD"
        Public Const DOLAR_ISLAS_CAIMAN_KYD As String = "KYD"
        Public Const TENGE_KAZAJSTAN_KZT As String = "KZT"
        Public Const KIP_LAOS_LAK As String = "LAK"
        Public Const LIBRA_LIBANO_LBP As String = "LBP"
        Public Const RUPIA_SRI_LANKA_LKR As String = "LKR"
        Public Const DOLAR_LIBERIA_LRD As String = "LRD"
        Public Const LOTI_LESOTHO_LSL As String = "LSL"
        Public Const LITAS_LITUANIA_LTL As String = "LTL"
        Public Const LATS_LETONIA_LVL As String = "LVL"
        Public Const DINAR_LIBIA_LYD As String = "LYD"
        Public Const DIRHAM_MARRUECOS_MAD As String = "MAD"
        Public Const LEU_MOLDAVIA_MDL As String = "MDL"
        Public Const FRANCO_MADAGASCAR_MGF As String = "MGF" 'http://www.hypergrove.com/Publications/OldSymbols.html
        Public Const FRANCO_MONACO_MNC As String = "MNC" 'REVISAR ¿?
        Public Const TUGRIK_MONGOLIA_MNT As String = "MNT"
        Public Const PATACA_MACAO_MOP As String = "MOP"
        Public Const OUGUIYA_MAURITANIA_MRO As String = "MRO"
        Public Const RUPIA_MAURICIO_MUR As String = "MUR"
        Public Const RUFIYAA_ISLAS_MALDIVAS_MVR As String = "MVR"
        Public Const KWACHA_MALAWI_MWK As String = "MWK"
        Public Const PESO_MEXICO_MXN As String = "MXN"
        Public Const RINGGIT_MALAYSIA_MYR As String = "MYR"
        Public Const METICAL_MOZAMBIQUE_MZM As String = "MZM" 'http://www.hypergrove.com/Publications/OldSymbols.html
        Public Const NAIRA_NIGERIA_NGN As String = "NGN"
        Public Const CORDOBA_NICARAGUA_NIC As String = "NIC" 'REVISAR ¿?
        Public Const CORDOBA_ORO_NICARAGUA_NIO As String = "NIO"
        Public Const NUEVO_SHEKEL_ISRAEL_NIS As String = "NIS" 'http://www.hypergrove.com/Publications/OldSymbols.html
        Public Const CORONA_NORUEGA_NOK As String = "NOK"
        Public Const RUPIA_NEPAL_NPR As String = "NPR"
        Public Const DOLAR_NUEVA_ZELANDA_NZD As String = "NZD"
        Public Const RIAL_OMAN_OMR As String = "OMR"
        Public Const BALBOA_PANAMA_PAB As String = "PAB"
        Public Const INTI_PERU_PEI As String = "PEI" 'http://www.hypergrove.com/Publications/OldSymbols.html
        Public Const NUEVO_SOL_PERU_PEN As String = "PEN"
        Public Const SOL_PERU_PES As String = "PES" 'http://www.hypergrove.com/Publications/OldSymbols.html
        Public Const KINA_PAPUA_NUEVA_GUINEA_PGK As String = "PGK"
        Public Const PESO_FILIPINAS_PHP As String = "PHP"
        Public Const RUPIA_PAKISTAN_PKR As String = "PKR"
        Public Const ZLOTY_POLONIA_PLN As String = "PLN"
        Public Const GUARANI_PARAGUAY_PYG As String = "PYG"
        Public Const RIAL_QUATAR_QAR As String = "QAR"
        Public Const RENMIBI_YUAN_CHINA_RMB As String = "RMB" 'http://www.hypergrove.com/Publications/OldSymbols.html
        Public Const LEU_RUMANIA_RON As String = "RON"
        Public Const RUBLO_RUSIA_RUB As String = "RUB"
        Public Const FRANCO_RUANDA_RWF As String = "RWF"
        Public Const RIYAL_ARABIA_SAUDITA_SAR As String = "SAR"
        Public Const DOLAR_ISLAS_SALOMON_SBD As String = "SBD"
        Public Const RUPIA_ISLAS_SEYCHELLES_SCR As String = "SCR"
        Public Const LIBRA_SUDAN_SDP As String = "SDP" 'http://www.hypergrove.com/Publications/OldSymbols.html
        Public Const CORONA_SUECIA_SEK As String = "SEK"
        Public Const DOLAR_SINGAPUR_SGD As String = "SGD"
        Public Const LIBRA_SANTA_ELENA_ASCENSION_Y_TRISTAN_DA_CUNHA_SHP As String = "SHP"
        Public Const CORONA_ESLOVAQUIA_SKK As String = "SKK" 'http://pt.wikipedia.org/wiki/ISO_4217
        Public Const LEONE_SIERRA_LEONA_SLL As String = "SLL"
        Public Const SOL_ As String = "SOL" 'REVISAR ¿?
        Public Const CHELIN_SOMALIA_SOS As String = "SOS"
        Public Const DOLAR_SURINAM_SRD As String = "SRD"
        Public Const DOBRA_SANTO_TOME_Y_PRINCIPE_STD As String = "STD"
        Public Const COLON_SALVADOR_SVC As String = "SVC"
        Public Const LIBRA_SIRIA_SYP As String = "SYP"
        Public Const LILAGENI_SWAZILANDIA_SZL As String = "SZL"
        Public Const BAHT_TAILANDIA_THB As String = "THB"
        Public Const SOMONI_TADJIKISTAN_TJS As String = "TJS"
        Public Const MANAT_TURKMENISTAN_TMM As String = "TMM" 'http://www.hypergrove.com/Publications/OldSymbols.html
        Public Const DINAR_TUNEZ_TND As String = "TND"
        Public Const PAANGA_TONGA_TOP As String = "TOP"
        Public Const ESCUDO_TIMOR_TPE As String = "TPE" 'http://pt.wikipedia.org/wiki/ISO_4217
        Public Const LIRA_TURQUIA_TRY As String = "TRY"
        Public Const DOLAR_TRINIDAD_Y_TOBAGO_TTD As String = "TTD"
        Public Const NUEVO_DOLAR_TAIWAN_TWD As String = "TWD"
        Public Const CHELIN_TANZANIA_TZS As String = "TZS"
        Public Const GRIVNA_UCRANIA_UAH As String = "UAH"
        Public Const CHELIN_UGANDA_UGS As String = "UGS" 'http://www.hypergrove.com/Publications/OldSymbols.html
        Public Const DOLAR_ESTADOS_UNIDOS_USD As String = "USD"
        Public Const PESO_NUEVO_URUGUAY_UYP As String = "UYP" 'http://www.hypergrove.com/Publications/OldSymbols.html
        Public Const PESO_URUGUAY_UYU As String = "UYU"
        Public Const BOLIVAR_FUERTE_VENEZUELA_VEF As String = "VEF"
        Public Const DONG_VIETNAM_VND As String = "VND"
        Public Const VATU_VANUATU_VUV As String = "VUV"
        Public Const TALA_SAMOA_WST As String = "WST"
        Public Const FRANCO_CFA_BEAC_AFRICA_CENTRAL_XAF As String = "XAF" 'CAMERÚN, REPÚBLICA CENTROAFRICANA, CHAD, CONGO, GUINEA ECUATORIAL Y GABÓN
        Public Const DOLAR_CARIBE_ORIENTAL_XCD As String = "XCD" 'ANTILLAS, ANTIGUA Y BARBUDA, DOMINICA, GRANADA, MONTSERRAT, SANT CRISTÓBAL Y NIEVES, SANTA LUCÍA, SAN VICENTE Y GRANADINAS
        Public Const FRANCO_CFA_BCEAO_AFRICA_OCCIDENTAL_XOF As String = "XOF" 'BENIN, BURKINA FASO, COSTA DE MARFIL, GUINEA-BISSAU, MALI, NIGER, SENEGAL Y TOGO
        Public Const RIAL_YEMEN_YER As String = "YER"
        Public Const RAND_SUDAFRICA_ZAR As String = "ZAR"
        Public Const KWACHA_ZAMBIA_ZMK As String = "ZMK"
        Public Const DOLAR_ZIMBABUE_ZWD As String = "ZWD"

        ''' <summary>
        ''' Constructura del objeto. Solo establece en valores iniciales los atributos
        ''' de instancia que definen parte de la estructura de un objeto facturae.
        ''' </summary>
        ''' <remarks>Llamada desde: CFacturae.construirBatch, CFacturae.construirInvoiceIssueData, CFacturae.batchXML2Obj, 
        ''' CFacturae.invoiceIssueDataXML2Obj
        ''' Tiempo máximo: menor a 0,1
        ''' Revisado por: auv. 18/05/2012</remarks>
        Public Sub New()
            Me.value = ""
        End Sub
    End Class

    <Serializable()> _
    Public Class PartiesType
        Public sellerParty As BusinessType
        Public buyerParty As BusinessType

        ''' <summary>
        ''' Constructura del objeto. Solo establece en valores iniciales los atributos
        ''' de instancia que definen parte de la estructura de un objeto facturae.
        ''' </summary>
        ''' <remarks>Llamada desde: CFacturae.construirParties, CFacturae.facturaeXML2Obj
        ''' Tiempo máximo: menor a 0,1
        ''' Revisado por: auv. 18/05/2012</remarks>
        Public Sub New()
            Me.sellerParty = Nothing
            Me.buyerParty = Nothing
        End Sub
    End Class

    <Serializable()> _
    Public Class BusinessType
        Public taxIdentification As TaxIdentificationType
        Private m_oLegalEntity As LegalEntityType
        Public Property legalEntity As LegalEntityType
            Get
                Return Me.m_oLegalEntity
            End Get
            Set(ByVal value As LegalEntityType)
                Me.m_oLegalEntity = value
            End Set
        End Property

        ''' <summary>
        ''' Constructura del objeto. Solo establece en valores iniciales los atributos
        ''' de instancia que definen parte de la estructura de un objeto facturae.
        ''' </summary>
        ''' <remarks>Llamada desde: CFacturae.construirSellerParty, CFacturae.construirBuyerParty, CFacturae.PartiesXML2Obj
        ''' Tiempo máximo: menor a 0,1
        ''' Revisado por: auv. 18/05/2012</remarks>
        Public Sub New()
            Me.taxIdentification = Nothing
            Me.legalEntity = Nothing
        End Sub
    End Class

    <Serializable()> _
    Public Class TaxIdentificationType
        Public personTypeCode As PersonTypeCodeType
        Public residenceTypeCode As ResidenceTypeCodeType
        Private m_oTaxIdentificationNumber As String
        Public Property taxIdentificationNumber As String
            Get
                Return Me.m_oTaxIdentificationNumber
            End Get
            Set(ByVal value As String)
                If value.Length > 30 Then
                    Me.m_oTaxIdentificationNumber = value.Substring(0, 30)
                Else
                    Me.m_oTaxIdentificationNumber = value
                    While Me.m_oTaxIdentificationNumber.Length < 3
                        Me.m_oTaxIdentificationNumber = Me.m_oTaxIdentificationNumber & " "
                    End While
                End If
            End Set
        End Property

        ''' <summary>
        ''' Constructura del objeto. Solo establece en valores iniciales los atributos
        ''' de instancia que definen parte de la estructura de un objeto facturae.
        ''' </summary>
        ''' <remarks>Llamada desde: CFacturae.construirSellerParty, CFacturae.construirBuyerParty, 
        ''' CFacturae.SellerPartyXML2Obj, CFacturae.BuyerPartyXML2Obj
        ''' Tiempo máximo: menor a 0,1
        ''' Revisado por: auv. 18/05/2012</remarks>
        Public Sub New()
            Me.personTypeCode = Nothing
            Me.residenceTypeCode = Nothing
            Me.taxIdentificationNumber = ""
        End Sub
    End Class

    <Serializable()> _
    Public Class PersonTypeCodeType
        Public value As String
        Public Const PERSONA_FISICA_F As String = "F"
        Public Const PERSONA_JURIDICA_J As String = "J"

        ''' <summary>
        ''' Constructura del objeto. Solo establece en valores iniciales los atributos
        ''' de instancia que definen parte de la estructura de un objeto facturae.
        ''' </summary>
        ''' <remarks>Llamada desde: CFacturae.construirSellerParty, CFacturae.construirBuyerParty,
        ''' CFacturae.SellerPartyXML2Obj, CFacturae.BuyerPartyXML2Obj
        ''' Tiempo máximo: menor a 0,1
        ''' Revisado por: auv. 18/05/2012</remarks>
        Public Sub New()
            Me.value = ""
        End Sub
    End Class

    <Serializable()> _
    Public Class ResidenceTypeCodeType
        Public value As String
        Public Const EXTRANJERO_E As String = "E"
        Public Const RESIDENTE_EN_ESPANYA_R As String = "R"
        Public Const RESIDENTE_EN_UNION_EUROPEA_U As String = "U"

        ''' <summary>
        ''' Constructura del objeto. Solo establece en valores iniciales los atributos
        ''' de instancia que definen parte de la estructura de un objeto facturae.
        ''' </summary>
        ''' <remarks>Llamada desde: CFacturae.construirSellerParty, CFacturae.construirBuyerParty,
        ''' CFacturae.SellerPartyXML2Obj, CFacturae.BuyerPartyXML2Obj
        ''' Tiempo máximo: menor a 0,1
        ''' Revisado por: auv. 18/05/2012</remarks>
        Public Sub New()
            Me.value = ""
        End Sub
    End Class

    <Serializable()> _
    Public Class LegalEntityType
        Private m_oCorporateName As String
        Public Property corporateName As String
            Get
                Return Me.m_oCorporateName
            End Get
            Set(ByVal value As String)
                If value.Length > 80 Then
                    Me.m_oCorporateName = value.Substring(0, 80)
                Else
                    Me.m_oCorporateName = value
                End If
            End Set
        End Property
        Private m_oAddressInSpain As AddressType
        Public Property addressInSpain As AddressType
            Get
                Return Me.m_oAddressInSpain
            End Get
            Set(value As AddressType)
                Me.m_oAddressInSpain = value
                If Not (IsNothing(value)) Then
                    Me.overseasAddress = Nothing
                End If
            End Set
        End Property
        Private m_oOverseasAddress As OverseasAddressType
        Public Property overseasAddress As OverseasAddressType
            Get
                Return Me.m_oOverseasAddress
            End Get
            Set(value As OverseasAddressType)
                Me.m_oOverseasAddress = value
                If Not (IsNothing(value)) Then
                    Me.addressInSpain = Nothing
                End If
            End Set
        End Property
        Public contactDetails As ContactDetailsType

        ''' <summary>
        ''' Constructura del objeto. Solo establece en valores iniciales los atributos
        ''' de instancia que definen parte de la estructura de un objeto facturae.
        ''' </summary>
        ''' <remarks>Llamada desde: CFacturae.construirSellerParty, CFacturae.construirBuyerParty,
        ''' CFacturae.SellerPartyXML2Obj, CFacturae.BuyerPartyXML2Obj
        ''' Tiempo máximo: menor a 0,1
        ''' Revisado por: auv. 18/05/2012</remarks>
        Public Sub New()
            Me.corporateName = ""
            Me.addressInSpain = Nothing
            Me.overseasAddress = Nothing
            Me.contactDetails = Nothing
        End Sub
    End Class

    <Serializable()> _
    Public Class AddressType
        Private m_oAddress As String
        Public Property address As String
            Get
                Return Me.m_oAddress
            End Get
            Set(ByVal value As String)
                If value.Length > 80 Then
                    Me.m_oAddress = value.Substring(0, 80)
                Else
                    Me.m_oAddress = value
                End If
            End Set
        End Property
        Public postCode As PostCodeType
        Private m_oTown As String
        Public Property town As String
            Get
                Return Me.m_oTown
            End Get
            Set(ByVal value As String)
                If value.Length > 50 Then
                    Me.m_oTown = value.Substring(0, 50)
                Else
                    Me.m_oTown = value
                End If
            End Set
        End Property
        Private m_oProvince As String
        Public Property province As String
            Get
                Return Me.m_oProvince
            End Get
            Set(ByVal value As String)
                If value.Length > 20 Then
                    Me.m_oProvince = value.Substring(0, 20)
                Else
                    Me.m_oProvince = value
                End If
            End Set
        End Property
        Public countryCode As CountryType

        ''' <summary>
        ''' Constructura del objeto. Solo establece en valores iniciales los atributos
        ''' de instancia que definen parte de la estructura de un objeto facturae.
        ''' </summary>
        ''' <remarks>Llamada desde: CFacturae.construirSellerParty, CFacturae.construirBuyerParty,
        ''' CFacturae.SellerPartyXML2Obj, CFacturae.BuyerPartyXML2Obj
        ''' Tiempo máximo: menor a 0,1
        ''' Revisado por: auv. 18/05/2012</remarks>
        Public Sub New()
            Me.address = ""
            Me.postCode = Nothing
            Me.town = ""
            Me.province = ""
            Me.countryCode = Nothing
        End Sub
    End Class

    <Serializable()> _
    Public Class PostCodeType
        Private m_oValue As String
        Public Property value As String
            Get
                Return Me.m_oValue
            End Get
            Set(ByVal value As String)
                If value.Length > 5 Then
                    Me.m_oValue = value.Substring(0, 5)
                Else
                    Me.m_oValue = value
                    While Me.m_oValue.Length < 5
                        Me.m_oValue = Me.m_oValue & "0"
                    End While
                End If
            End Set
        End Property

        ''' <summary>
        ''' Constructura del objeto. Solo establece en valores iniciales los atributos
        ''' de instancia que definen parte de la estructura de un objeto facturae.
        ''' </summary>
        ''' <remarks>Llamada desde: CFacturae.construirSellerParty, CFacturae.construirBuyerParty,
        ''' CFacturae.SellerPartyXML2Obj, CFacturae.BuyerPartyXML2Obj
        ''' Tiempo máximo: menor a 0,1
        ''' Revisado por: auv. 18/05/2012</remarks>
        Public Sub New()
            Me.value = ""
        End Sub
    End Class

    <Serializable()> _
    Public Class CountryType 'http://en.wikipedia.org/wiki/ISO_3166-1
        Public value As String
        Public Const AFGHANISTAN_AFG As String = "AFG"
        Public Const ALBANIA_ALB As String = "ALB"
        Public Const ALGERIA_DZA As String = "DZA"
        Public Const SAMOA_AMERICANA_ASM As String = "ASM"
        Public Const ANDORRA_AND As String = "AND"
        Public Const ANGOLA_AGO As String = "AGO"
        Public Const ANGUILLA_AIA As String = "AIA"
        Public Const ANTIGUA_Y_BARBUDA_ATG As String = "ATG"
        Public Const ARGENTINA_ARG As String = "ARG"
        Public Const ARMENIA_ARM As String = "ARM"
        Public Const ARUBA_ABW As String = "ABW"
        Public Const AUSTRALIA_AUS As String = "AUS"
        Public Const AUSTRIA_AUT As String = "AUT"
        Public Const AZERBAIJAN_AZE As String = "AZE"
        Public Const BAHAMAS_BHS As String = "BHS"
        Public Const BARHEIN_BHR As String = "BHR"
        Public Const BANGLADESH_BGD As String = "BGD"
        Public Const BARBADOS_BRB As String = "BRB"
        Public Const BIELORRUSIA_BLR As String = "BLR"
        Public Const BELGICA_BEL As String = "BEL"
        Public Const BELICE_BLZ As String = "BLZ"
        Public Const BENIN_BEN As String = "BEN"
        Public Const BERMUDA_BMU As String = "BMU"
        Public Const BUTAN_BTN As String = "BTN"
        Public Const BOLIVIA_BOL As String = "BOL"
        Public Const BOSNIA_Y_HERZEGOVINA_BIH As String = "BIH"
        Public Const BOTSWANA_BWA As String = "BWA"
        Public Const BRASIL_BRA As String = "BRA"
        Public Const BRUNEI_DARUSSALAM_BRN As String = "BRN"
        Public Const BULGARIA_BGR As String = "BGR"
        Public Const BURKINA_FASO_BFA As String = "BFA"
        Public Const BURUNDI_BDI As String = "BDI"
        Public Const CAMBODIA_KHM As String = "KHM"
        Public Const CAMERUN_CMR As String = "CMR"
        Public Const CANADA_CAN As String = "CAN"
        Public Const CABO_VERDE_CPV As String = "CPV"
        Public Const ISLAS_CAIMAN_CYM As String = "CYM"
        Public Const REPUBLICA_CENTROAFRICANA_CAF As String = "CAF" 'http://es.wikipedia.org/wiki/ISO_3166-1
        Public Const CHAD_TCD As String = "TCD"
        Public Const CHILE_CHL As String = "CHL"
        Public Const CHINA_CHN As String = "CHN"
        Public Const REPUBLICA_DEMOCRATICA_DEL_CONGO_COD As String = "COD"
        Public Const COLOMBIA_COL As String = "COL"
        Public Const COMOROS_COM As String = "COM"
        Public Const CONGO_COG As String = "COG"
        Public Const ISLAS_COOK_COK As String = "COK"
        Public Const COSTA_RICA_CRI As String = "CRI"
        Public Const COSTA_DE_MARFIL_CIV As String = "CIV"
        Public Const CROACIA_HRV As String = "HRV"
        Public Const CUBA_CUB As String = "CUB"
        Public Const CHIPRE_CYP As String = "CYP"
        Public Const REPUBLICA_CHECA_CZE As String = "CZE"
        Public Const DINAMARCA_DNK As String = "DNK"
        Public Const YIBUTI_DJI As String = "DJI"
        Public Const DOMINICA_DMA As String = "DMA"
        Public Const REPUBLICA_DOMINICANA_DOM As String = "DOM"
        Public Const ECUADOR_ECU As String = "ECU"
        Public Const EGIPTO_EGY As String = "EGY"
        Public Const SALVADOR_SLV As String = "SLV"
        Public Const GUINEA_ECUATORIAL_GNQ As String = "GNQ"
        Public Const ERITREA_ERI As String = "ERI"
        Public Const ESTONIA_EST As String = "EST"
        Public Const ETIOPIA_ETH As String = "ETH"
        Public Const ISLAS_MALVINAS_FLK As String = "FLK"
        Public Const ISLAS_FEROE_FRO As String = "FRO"
        Public Const FIJI_FJI As String = "FJI"
        Public Const FINLANDIA_FIN As String = "FIN"
        Public Const FRANCIA_FRA As String = "FRA"
        Public Const GUAYANA_FRANCESA_GUF As String = "GUF"
        Public Const POLINESIA_FRANCESA_PYF As String = "PYF"
        Public Const GABON_GAB As String = "GAB"
        Public Const GAMBIA_GMB As String = "GMB"
        Public Const GEORGIA_GEO As String = "GEO"
        Public Const GUERNSEY_GGY As String = "GGY"
        Public Const ALEMANIA_DEU As String = "DEU"
        Public Const GHANA_GHA As String = "GHA"
        Public Const GIBRALTAR_GIB As String = "GIB"
        Public Const GRECIA_GRC As String = "GRC"
        Public Const GREENLAND_GRL As String = "GRL"
        Public Const GRANADA_GRD As String = "GRD"
        Public Const GUADALUPE_GLP As String = "GLP"
        Public Const GUAM_GUM As String = "GUM"
        Public Const GUATEMALA_GTM As String = "GTM"
        Public Const GUINEA_GIN As String = "GIN"
        Public Const GUINEA_BISSAU_GNB As String = "GNB"
        Public Const GUYANA_GUY As String = "GUY"
        Public Const HAITI_HTI As String = "HTI"
        Public Const HONDURAS_HND As String = "HND"
        Public Const HONG_KONG_HKG As String = "HKG"
        Public Const HUNGRIA_HUN As String = "HUN"
        Public Const ISLANDIA_ISL As String = "ISL"
        Public Const INDIA_IND As String = "IND"
        Public Const INDONESIA_IDN As String = "IDN"
        Public Const ISLA_DE_MAN_IMN As String = "IMN"
        Public Const IRAN_IRN As String = "IRN"
        Public Const IRAQ_IRQ As String = "IRQ"
        Public Const IRLANDA_IRL As String = "IRL"
        Public Const ISRAEL_ISR As String = "ISR"
        Public Const ITALIA_ITA As String = "ITA"
        Public Const JAMAICA_JAM As String = "JAM"
        Public Const JERSEY_JEY As String = "JEY"
        Public Const JAPON_JPN As String = "JPN"
        Public Const JORDANIA_JOR As String = "JOR"
        Public Const KAZAJISTAN_KAZ As String = "KAZ"
        Public Const KENIA_KEN As String = "KEN"
        Public Const KIRIBATI_KIR As String = "KIR"
        Public Const COREA_DEL_NORTE_PRK As String = "PRK"
        Public Const COREA_DEL_SUR_KOR As String = "KOR"
        Public Const KUWAIT_KWT As String = "KWT"
        Public Const KIRGUISTAN_KGZ As String = "KGZ"
        Public Const LAOS_LAO As String = "LAO"
        Public Const LETONIA_LVA As String = "LVA"
        Public Const LIBANO_LBN As String = "LBN"
        Public Const LESOTHO_LSO As String = "LSO"
        Public Const LIBERIA_LBR As String = "LBR"
        Public Const LIBIA_LBY As String = "LBY"
        Public Const LIECHTENSTEIN_LIE As String = "LIE"
        Public Const LITUANIA_LTU As String = "LTU"
        Public Const LUXEMBURGO_LUX As String = "LUX"
        Public Const MACAO_MAC As String = "MAC"
        Public Const MACEDONIA_MKD As String = "MKD"
        Public Const MADAGASCAR_MDG As String = "MDG"
        Public Const MALAWI_MWI As String = "MWI"
        Public Const MALASIA_MYS As String = "MYS"
        Public Const ISLAS_MALDIVAS_MDV As String = "MDV"
        Public Const MALI_MLI As String = "MLI"
        Public Const MALTA_MLT As String = "MLT"
        Public Const ISLAS_MARSHALL_MHL As String = "MHL"
        Public Const MARTINICA_MTQ As String = "MTQ"
        Public Const MAURITANIA_MRT As String = "MRT"
        Public Const MAURICIO_MUS As String = "MUS"
        Public Const MAYOTTE_MYT As String = "MYT"
        Public Const MEXICO_MEX As String = "MEX"
        Public Const MICRONESIA_FSM As String = "FSM"
        Public Const MOLDAVIA_MDA As String = "MDA"
        Public Const MONACO_MCO As String = "MCO"
        Public Const MONTENEGRO_MNE As String = "MNE"
        Public Const MONGOLIA_MNG As String = "MNG"
        Public Const MONTSERRAT_MSR As String = "MSR"
        Public Const MARRUECOS_MAR As String = "MAR"
        Public Const MOZAMBIQUE_MOZ As String = "MOZ"
        Public Const BIRMANIA_MMR As String = "MMR"
        Public Const NAMIBIA_NAM As String = "NAM"
        Public Const NAURU_NRU As String = "NRU"
        Public Const NEPAL_NPL As String = "NPL"
        Public Const HOLANDA_NLD As String = "NLD"
        Public Const ANTILLAS_HOLANDESAS_ANT As String = "ANT"
        Public Const NUEVA_CALEDONIA_NCL As String = "NCL"
        Public Const NUEVA_ZELANDA_NZL As String = "NZL"
        Public Const NICARAGUA_NIC As String = "NIC"
        Public Const NIGER_NER As String = "NER"
        Public Const NIGERIA_NGA As String = "NGA"
        Public Const NIUE_NIU As String = "NIU"
        Public Const ISLAS_NORFOLK_NFK As String = "NFK"
        Public Const ISLAS_MARIANAS_DEL_NORTE_MNP As String = "MNP"
        Public Const NORUEGA_NOR As String = "NOR"
        Public Const OMAN_OMN As String = "OMN"
        Public Const PAKISTAN_PAK As String = "PAK"
        Public Const PALAU_PLW As String = "PLW"
        Public Const PANAMA_PAN As String = "PAN"
        Public Const PAPUA_NUEVA_GUINEA_PNG As String = "PNG"
        Public Const PARAGUAY_PRY As String = "PRY"
        Public Const TERRITORIO_PALESTINO_OCUPADO_PSE As String = "PSE"
        Public Const PERU_PER As String = "PER"
        Public Const FILIPINAS_PHL As String = "PHL"
        Public Const PITCAIRN_PCN As String = "PCN"
        Public Const POLONIA_POL As String = "POL"
        Public Const PORTUGAL_PRT As String = "PRT"
        Public Const PUERTO_RICO_PRI As String = "PRI"
        Public Const QATAR_QAT As String = "QAT"
        Public Const REUNION_REU As String = "REU"
        Public Const RUMANIA_ROU As String = "ROU"
        Public Const RUSIA_RUS As String = "RUS"
        Public Const RUANDA_RWA As String = "RWA"
        Public Const SAN_CRISTOBAL_Y_NIEVES_KNA As String = "KNA"
        Public Const SANTA_LUCIA_LCA As String = "LCA"
        Public Const SAN_VICENTE_Y_GRANADINAS_VCT As String = "VCT"
        Public Const SAMOA_WSM As String = "WSM"
        Public Const SAN_MARINO_SMR As String = "SMR"
        Public Const SANTO_TOME_Y_PRINCIPE_STP As String = "STP"
        Public Const ARABIA_SAUDI_SAU As String = "SAU"
        Public Const SENEGAL_SEN As String = "SEN"
        Public Const SERBIA_SRB As String = "SRB"
        Public Const SEYCHELLES_SYC As String = "SYC"
        Public Const SIERRA_LEONA_SLE As String = "SLE"
        Public Const SINGAPUR_SGP As String = "SGP"
        Public Const ESLOVAQUIA_SVK As String = "SVK"
        Public Const ESLOVENIA_SVN As String = "SVN"
        Public Const ISLAS_SALOMON_SLB As String = "SLB"
        Public Const SOMALIA_SOM As String = "SOM"
        Public Const SUDAFRICA_ZAF As String = "ZAF"
        Public Const ESPANYA_ESP As String = "ESP"
        Public Const SRI_LANKA_LKA As String = "LKA"
        Public Const SANTA_ELENA_Y_ASCENSION_Y_TRISTAN_DE_ACUNYA_SHN As String = "SHN"
        Public Const SAN_PEDRO_Y_MIQUELON_SPM As String = "SPM"
        Public Const SUDAN_SDN As String = "SDN"
        Public Const SUIRNAM_SUR As String = "SUR"
        Public Const SVALBARD_Y_JAN_MAYEN_SJM As String = "SJM"
        Public Const SWAZILANDIA_SWZ As String = "SWZ"
        Public Const SUECIA_SWE As String = "SWE"
        Public Const SUIZA_CHE As String = "CHE"
        Public Const SIRIA_SYR As String = "SYR"
        Public Const TAIWAN_TWN As String = "TWN"
        Public Const TAYIKISTAN_TJK As String = "TJK"
        Public Const TANZANIA_TZA As String = "TZA"
        Public Const TAILANDIA_THA As String = "THA"
        Public Const TOGO_TGO As String = "TGO"
        Public Const TOKELAU_TKL As String = "TKL"
        Public Const TONGA_TON As String = "TON"
        Public Const TRINIDAD_Y_TOBAGO_TTO As String = "TTO"
        Public Const TUNEZ_TUN As String = "TUN"
        Public Const TURQUIA_TUR As String = "TUR"
        Public Const TURKMENISTAN_TKM As String = "TKM"
        Public Const TIMOR_LESTE_TLS As String = "TLS"
        Public Const ISLAS_TURCAS_Y_CAICOS_TCA As String = "TCA"
        Public Const TUVALU_TUV As String = "TUV"
        Public Const UGANDA_UGA As String = "UGA"
        Public Const UCRANIA_UKR As String = "UKR"
        Public Const EMIRATOS_ARABES_UNIDOS_ARE As String = "ARE"
        Public Const REINO_UNIDO_GBR As String = "GBR"
        Public Const ESTADOS_UNIDOS_USA As String = "USA"
        Public Const URUGUAY_URY As String = "URY"
        Public Const UZBEKISTAN_UZB As String = "UZB"
        Public Const VANUATU_VUT As String = "VUT"
        Public Const CIUDAD_DEL_VATICANO_VAT As String = "VAT" 'http://es.wikipedia.org/wiki/ISO_3166-1
        Public Const VENEZUELA_VEN As String = "VEN"
        Public Const VIETNAM_VNM As String = "VNM"
        Public Const ISLAS_VIGENES_BRITANICAS_VGB As String = "VGB"
        Public Const ISLAS_VIRGENES_ESTADOUNIDENSES_VIR As String = "VIR"
        Public Const WALLIS_Y_FUTUNA_WLF As String = "WLF"
        Public Const SAHARA_OCCIDENTAL_ESH As String = "ESH"
        Public Const YEMEN_YEM As String = "YEM"
        Public Const ZAIRE_ZAR As String = "ZAR" 'http://en.wikipedia.org/wiki/ISO_3166-1_alpha-3
        Public Const ZAMBIA_ZMB As String = "ZMB"
        Public Const ZIMBABUE_ZWE As String = "ZWE"

        ''' <summary>
        ''' Constructura del objeto. Solo establece en valores iniciales los atributos
        ''' de instancia que definen parte de la estructura de un objeto facturae.
        ''' </summary>
        ''' <remarks>Llamada desde: CFacturae.construirSellerParty, CFacturae.construirBuyerParty,
        ''' CFacturae.SellerPartyXML2Obj, CFacturae.BuyerPartyXML2Obj
        ''' Tiempo máximo: menor a 0,1
        ''' Revisado por: auv. 18/05/2012</remarks>
        Public Sub New()
            Me.value = ""
        End Sub
    End Class

    <Serializable()> _
    Public Class OverseasAddressType
        Private m_oAddress As String
        Public Property address As String
            Get
                Return Me.m_oAddress
            End Get
            Set(ByVal value As String)
                If value.Length > 80 Then
                    Me.m_oAddress = value.Substring(0, 80)
                Else
                    Me.m_oAddress = value
                End If
            End Set
        End Property
        Private m_oPostCodeAndTown
        Public Property postCodeAndTown As String
            Get
                Return Me.m_oPostCodeAndTown
            End Get
            Set(ByVal value As String)
                If value.Length > 50 Then
                    Me.m_oPostCodeAndTown = value.Substring(0, 50)
                Else
                    Me.m_oPostCodeAndTown = value
                End If
            End Set
        End Property
        Private m_oProvince As String
        Public Property province As String
            Get
                Return Me.m_oProvince
            End Get
            Set(ByVal value As String)
                If value.Length > 20 Then
                    Me.m_oProvince = value.Substring(0, 20)
                Else
                    Me.m_oProvince = value
                End If
            End Set
        End Property
        Public countryCode As CountryType

        ''' <summary>
        ''' Constructura del objeto. Solo establece en valores iniciales los atributos
        ''' de instancia que definen parte de la estructura de un objeto facturae.
        ''' </summary>
        ''' <remarks>Llamada desde: CFacturae.construirSellerParty, CFacturae.construirBuyerParty,
        ''' CFacturae.SellerPartyXML2Obj, CFacturae.BuyerPartyXML2Obj
        ''' Tiempo máximo: menor a 0,1
        ''' Revisado por: auv. 18/05/2012</remarks>
        Public Sub New()
            Me.address = ""
            Me.postCodeAndTown = ""
            Me.province = ""
            Me.countryCode = Nothing
        End Sub
    End Class

    <Serializable()> _
    Public Class ContactDetailsType
        Private m_oTelephone As String
        Public Property telephone As String
            Get
                Return Me.m_oTelephone
            End Get
            Set(ByVal value As String)
                If value.Length > 15 Then
                    Me.m_oTelephone = value.Substring(0, 15)
                Else
                    Me.m_oTelephone = value
                End If
            End Set
        End Property
        Private m_oTeleFax As String
        Public Property teleFax As String
            Get
                Return Me.m_oTeleFax
            End Get
            Set(ByVal value As String)
                If value.Length > 15 Then
                    Me.m_oTeleFax = value.Substring(0, 15)
                Else
                    Me.m_oTeleFax = value
                End If
            End Set
        End Property
        Private m_oElectronicMail As String
        Public Property electronicMail As String
            Get
                Return Me.m_oElectronicMail
            End Get
            Set(ByVal value As String)
                If value.Length > 60 Then
                    Me.m_oElectronicMail = value.Substring(0, 60)
                Else
                    Me.m_oElectronicMail = value
                End If
            End Set
        End Property
        Private m_oContactPersons As String
        Public Property contactPersons As String
            Get
                Return Me.m_oContactPersons
            End Get
            Set(ByVal value As String)
                If value.Length > 40 Then
                    Me.m_oContactPersons = value.Substring(0, 40)
                Else
                    Me.m_oContactPersons = value
                End If
            End Set
        End Property

        ''' <summary>
        ''' Constructura del objeto. Solo establece en valores iniciales los atributos
        ''' de instancia que definen parte de la estructura de un objeto facturae.
        ''' </summary>
        ''' <remarks>Llamada desde:  CFacturae.construirSellerParty, CFacturae.SellerPartyXML2Obj
        ''' Tiempo máximo: menor a 0,1
        ''' Revisado por: auv. 18/05/2012</remarks>
        Public Sub New()
            Me.telephone = ""
            Me.teleFax = ""
            Me.electronicMail = ""
            Me.contactPersons = ""
        End Sub
    End Class

    <Serializable()> _
    Public Class InvoiceType
        Public invoiceHeader As InvoiceHeaderType
        Public invoiceIssueData As InvoiceIssueDataType
        Public taxesOutputs() As TaxOutputType
        Public taxesWithheld() As TaxWithheldType
        Public invoiceTotals As InvoiceTotalsType
        Public items() As ItemType
        Public additionalData As AdditionalDataType

        ''' <summary>
        ''' Constructura del objeto. Solo establece en valores iniciales los atributos
        ''' de instancia que definen parte de la estructura de un objeto facturae.
        ''' </summary>
        ''' <remarks>Llamada desde: CFacturae.construirInvoices, CFacturae.InvoicesXML2Obj
        ''' Tiempo máximo: menor a 0,1
        ''' Revisado por: auv. 18/05/2012</remarks>
        Public Sub New()
            Me.invoiceHeader = Nothing
            Me.invoiceIssueData = Nothing
            Me.taxesOutputs = Nothing
            Me.taxesWithheld = Nothing
            Me.invoiceTotals = Nothing
            Me.items = Nothing
            Me.additionalData = Nothing
        End Sub
    End Class

    <Serializable()> _
    Public Class InvoiceHeaderType
        Private m_oInvoiceNumber As String
        Public Property invoiceNumber As String
            Get
                Return Me.m_oInvoiceNumber
            End Get
            Set(ByVal value As String)
                If value.Length > 20 Then
                    Me.m_oInvoiceNumber = value.Substring(0, 20)
                Else
                    Me.m_oInvoiceNumber = value
                End If
            End Set
        End Property
        Public invoiceDocumentType As InvoiceDocumentTypeType
        Public invoiceClass As InvoiceClassType
        Public corrective As CorrectiveType

        ''' <summary>
        ''' Constructura del objeto. Solo establece en valores iniciales los atributos
        ''' de instancia que definen parte de la estructura de un objeto facturae.
        ''' </summary>
        ''' <remarks>Llamada desde: CFacturae.construirInvoiceHeader, CFacturae.InvoiceXML2Obj
        ''' Tiempo máximo: menor a 0,1
        ''' Revisado por: auv. 18/05/2012</remarks>
        Public Sub New()
            Me.invoiceNumber = ""
            Me.invoiceDocumentType = Nothing
            Me.invoiceClass = Nothing
            Me.corrective = Nothing
        End Sub
    End Class

    <Serializable()> _
    Public Class InvoiceDocumentTypeType
        Public value As String
        Public Const FACTURA_COMPLETA_FC As String = "FC"
        Public Const FACTURA_ABREVIADA_FA As String = "FA"
        Public Const AUTOFACTURA_AF As String = "AF"

        ''' <summary>
        ''' Constructura del objeto. Solo establece en valores iniciales los atributos
        ''' de instancia que definen parte de la estructura de un objeto facturae.
        ''' </summary>
        ''' <remarks>Llamada desde: CFacturae.construirInvoiceHeader, CFacturae.invoiceHeaderXML2Obj
        ''' Tiempo máximo: menor a 0,1
        ''' Revisado por: auv. 18/05/2012</remarks>
        Public Sub New()
            Me.value = ""
        End Sub
    End Class

    <Serializable()> _
    Public Class InvoiceClassType
        Public value As String
        Public Const ORIGINAL_OO As String = "OO"
        Public Const ORIGINAL_RECTIFICATIVA_OR As String = "OR"
        Public Const ORIGINAL_RECAPITULATIVA_OC As String = "OC"
        Public Const COPIA_ORIGINAL_CO As String = "CO"
        Public Const COPIA_RECTIFICATIVA_CR As String = "CR"
        Public Const COPIA_RECAPITULATIVA_CC As String = "CC"

        ''' <summary>
        ''' Constructura del objeto. Solo establece en valores iniciales los atributos
        ''' de instancia que definen parte de la estructura de un objeto facturae.
        ''' </summary>
        ''' <remarks>Llamada desde: CFacturae.construirInvoiceHeader, CFacturae.invoiceHeaderXML2Obj
        ''' Tiempo máximo: menor a 0,1
        ''' Revisado por: auv. 18/05/2012</remarks>
        Public Sub New()
            Me.value = ""
        End Sub
    End Class

    <Serializable()> _
    Public Class CorrectiveType
        Private m_oInvoiceNumber As String
        Public Property invoiceNumber As String
            Get
                Return Me.m_oInvoiceNumber
            End Get
            Set(ByVal value As String)
                If value.Length > 20 Then
                    Me.m_oInvoiceNumber = value.Substring(0, 20)
                Else
                    Me.m_oInvoiceNumber = value
                End If
            End Set
        End Property
        Public reasonCode As ReasonCodeType
        Public reasonDescription As ReasonDescriptionType
        Public taxPeriod As PeriodDatesType
        Public correctionMethod As CorrectionMethodType
        Public correctionMethodDescription As CorrectionMethodDescriptionType

        ''' <summary>
        ''' Constructura del objeto. Solo establece en valores iniciales los atributos
        ''' de instancia que definen parte de la estructura de un objeto facturae.
        ''' </summary>
        ''' <remarks>Llamada desde: CFacturae.invoiceHeaderXML2Obj
        ''' Tiempo máximo: menor a 0,1
        ''' Revisado por: auv. 18/05/2012</remarks>
        Public Sub New()
            Me.invoiceNumber = ""
            Me.reasonCode = Nothing
            Me.reasonDescription = Nothing
            Me.taxPeriod = Nothing
            Me.correctionMethod = Nothing
            Me.correctionMethodDescription = Nothing
        End Sub
    End Class

    <Serializable()> _
    Public Class ReasonCodeType
        Public value As String
        Public Const NUMERO_FACTURA_01 As String = "01"
        Public Const SERIE_FACTURA_02 As String = "02"
        Public Const FECHA_EXPEDICION_03 As String = "03"
        Public Const NOMBRE_APELLIDOS_O_RAZON_SOCIAL_EMISOR_04 As String = "04"
        Public Const NOMBRE_APELLIDOS_O_RAZON_SOCIAL_RECEPTOR_05 As String = "05"
        Public Const IDENTIFICADOR_FISCAL_EMISOR_06 As String = "06"
        Public Const IDENTIFICADOR_FISCAL_RECEPTOR_07 As String = "07"
        Public Const DOMICILIO_EMISOR_08 As String = "08"
        Public Const DOMICILIO_RECEPTOR_09 As String = "09"
        Public Const DETALLE_OPERACION_10 As String = "10"
        Public Const PORCENTAJE_IMPOSITIVO_A_APLICAR_11 As String = "11"
        Public Const CUOTA_TRIBUTARIA_A_APLICAR_12 As String = "12"
        Public Const FECHA_PERIODO_A_APLICAR_13 As String = "13"
        Public Const CLASE_DE_FACTURA_14 As String = "14"
        Public Const LITERALES_LEGALES_15 As String = "15"
        Public Const BASE_IMPONIBLE_16 As String = "16"
        Public Const CALCULO_DE_CUOTAS_REPERCUTIDAS_80 As String = "80"
        Public Const CALCULO_DE_CUOTAS_RETENIDAS_81 As String = "81"
        Public Const BASE_IMPONIBLE_MODIFICADA_POR_DEVOLUCION_DE_ENVASES_82 As String = "82"
        Public Const BASE_IMPONIBLE_MODIFICADA_POR_DESCUENTOS_Y_BONIFICACIONES_83 As String = "83"
        Public Const BASE_IMPONIBLE_MODIFICADA_POR_RESOLUCION_FIRME_JUDICIAL_O_ADMINISTRATIVA_84 As String = "84"
        Public Const BASE_IMPONIBLE_MODIFICADA_CUOTAS_REPERCUTIDAS_NO_SATISFECHAS_85 As String = "85"

        ''' <summary>
        ''' Constructura del objeto. Solo establece en valores iniciales los atributos
        ''' de instancia que definen parte de la estructura de un objeto facturae.
        ''' </summary>
        ''' <remarks>Llamada desde: CFacturae.invoiceCorrectiveXML2Obj
        ''' Tiempo máximo: menor a 0,1
        ''' Revisado por: auv. 18/05/2012</remarks>
        Public Sub New()
            Me.value = ""
        End Sub
    End Class

    <Serializable()> _
    Public Class ReasonDescriptionType
        Public value As String
        Public Const NUMERO_FACTURA As String = "Número de la factura"
        Public Const SERIE_FACTURA As String = "Serie de la factura"
        Public Const FECHA_EXPEDICION As String = "Fecha expedición"
        Public Const NOMBRE_APELLIDOS_O_RAZON_SOCIAL_EMISOR As String = "Nombre y apellidos/Razón Social-Emisor"
        Public Const NOMBRE_APELLIDOS_O_RAZON_SOCIAL_RECEPTOR As String = "Nombre y apellidos/Razón Social-Receptor"
        Public Const IDENTIFICADOR_FISCAL_EMISOR As String = "Identificación fiscal Emisor/obligado"
        Public Const IDENTIFICADOR_FISCAL_RECEPTOR As String = "Identificación fiscal Receptor"
        Public Const DOMICILIO_EMISOR As String = "Domicilio Emisor/Obligado"
        Public Const DOMICILIO_RECEPTOR As String = "Domicilio Receptor"
        Public Const DETALLE_OPERACION As String = "Detalle Operación"
        Public Const PORCENTAJE_IMPOSITIVO_A_APLICAR As String = "Porcentaje impositivo a aplicar"
        Public Const CUOTA_TRIBUTARIA_A_APLICAR As String = "Cuota tributaria a aplicar"
        Public Const FECHA_PERIODO_A_APLICAR As String = "Fecha/Periodo a aplicar"
        Public Const CLASE_DE_FACTURA As String = "Clase de factura"
        Public Const LITERALES_LEGALES As String = "Literales legales"
        Public Const BASE_IMPONIBLE As String = "Base imponible"
        Public Const CALCULO_DE_CUOTAS_REPERCUTIDAS As String = "Cálculo de cuotas repercutidas"
        Public Const CALCULO_DE_CUOTAS_RETENIDAS As String = "Cálculo de cuotas retenidas"
        Public Const BASE_IMPONIBLE_MODIFICADA_POR_DEVOLUCION_DE_ENVASES As String = "Base imponible modificada por devolución de envases / embalajes"
        Public Const BASE_IMPONIBLE_MODIFICADA_POR_DESCUENTOS_Y_BONIFICACIONES As String = "Base imponible modificada por descuentos y bonificaciones"
        Public Const BASE_IMPONIBLE_MODIFICADA_POR_RESOLUCION_FIRME_JUDICIAL_O_ADMINISTRATIVA As String = "Base imponible modificada por resolución firme, judicial o administrativa"
        Public Const BASE_IMPONIBLE_MODIFICADA_CUOTAS_REPERCUTIDAS_NO_SATISFECHAS As String = "Base imponible modificada cuotas repercutidas no satisfechas. Auto de declaración de concurso"

        ''' <summary>
        ''' Constructura del objeto. Solo establece en valores iniciales los atributos
        ''' de instancia que definen parte de la estructura de un objeto facturae.
        ''' </summary>
        ''' <remarks>Llamada desde: CFacturae.invoiceCorrectiveXML2Obj
        ''' Tiempo máximo: menor a 0,1
        ''' Revisado por: auv. 18/05/2012</remarks>
        Public Sub New()
            Me.value = ""
        End Sub
    End Class

    <Serializable()> _
    Public Class PeriodDatesType
        Public startDate As Date
        Public endDate As Date

        ''' <summary>
        ''' Constructura del objeto. Solo establece en valores iniciales los atributos
        ''' de instancia que definen parte de la estructura de un objeto facturae.
        ''' </summary>
        ''' <remarks>Llamada desde: CFacturae.invoiceCorrectiveXML2Obj
        ''' Tiempo máximo: menor a 0,1
        ''' Revisado por: auv. 18/05/2012</remarks>
        Public Sub New()
            Me.startDate = Nothing
            Me.endDate = Nothing
        End Sub
    End Class

    <Serializable()> _
    Public Class CorrectionMethodType
        Public value As String
        Public Const RECTIFICACION_INTEGRA_01 As String = "01"
        Public Const RECTIFICACION_POR_DIFERENCIAS_02 As String = "02"
        Public Const RECTIFICACION_POR_DESCUENTO_03 As String = "03"
        Public Const AUTORIZADAS_POR_LA_AGENCIA_TRIBUTARIA_04 As String = "04"

        ''' <summary>
        ''' Constructura del objeto. Solo establece en valores iniciales los atributos
        ''' de instancia que definen parte de la estructura de un objeto facturae.
        ''' </summary>
        ''' <remarks>Llamada desde: CFacturae.invoiceCorrectiveXML2Obj
        ''' Tiempo máximo: menor a 0,1
        ''' Revisado por: auv. 18/05/2012</remarks>
        Public Sub New()
            Me.value = ""
        End Sub
    End Class

    <Serializable()> _
    Public Class CorrectionMethodDescriptionType
        Public value As String
        Public Const RECTIFICACION_INTEGRA As String = "Rectificación íntegra"
        Public Const RECTIFICACION_POR_DIFERENCIAS As String = "Rectificación por diferencias"
        Public Const RECTIFICACION_POR_DESCUENTO As String = "Rectificación por descuento por volumen de operaciones durante un periodo"
        Public Const AUTORIZADAS_POR_LA_AGENCIA_TRIBUTARIA As String = "Autorizadas por la Agencia Tributaria"

        ''' <summary>
        ''' Constructura del objeto. Solo establece en valores iniciales los atributos
        ''' de instancia que definen parte de la estructura de un objeto facturae.
        ''' </summary>
        ''' <remarks>Llamada desde: CFacturae.invoiceCorrectiveXML2Obj
        ''' Tiempo máximo: menor a 0,1
        ''' Revisado por: auv. 18/05/2012</remarks>
        Public Sub New()
            Me.value = ""
        End Sub
    End Class

    <Serializable()> _
    Public Class InvoiceIssueDataType
        Public issueDate As Date
        Public invoiceCurrencyCode As CurrencyCodeType
        Public exchangeRateDetails As ExchangeRateDetailsType
        Public taxCurrencyCode As CurrencyCodeType
        Public languageName As LanguageCodeType

        ''' <summary>
        ''' Constructura del objeto. Solo establece en valores iniciales los atributos
        ''' de instancia que definen parte de la estructura de un objeto facturae.
        ''' </summary>
        ''' <remarks>Llamada desde: CFacturae.construirInvoiceIssueData, CFacturae.InvoiceXML2Obj
        ''' Tiempo máximo: menor a 0,1
        ''' Revisado por: auv. 18/05/2012</remarks>
        Public Sub New()
            Me.issueDate = Nothing
            Me.invoiceCurrencyCode = Nothing
            Me.exchangeRateDetails = Nothing
            Me.taxCurrencyCode = Nothing
            Me.languageName = Nothing
        End Sub
    End Class

    <Serializable()> _
    Public Class ExchangeRateDetailsType
        Public exchangeRate As DoubleTwoDecimalType
        Public exchangeRateDate As Date

        ''' <summary>
        ''' Constructura del objeto. Solo establece en valores iniciales los atributos
        ''' de instancia que definen parte de la estructura de un objeto facturae.
        ''' </summary>
        ''' <remarks>Llamada desde: CFacturae.construirInvoiceIssueData, CFacturae.InvoiceIssueDataXML2Obj
        ''' Tiempo máximo: menor a 0,1
        ''' Revisado por: auv. 18/05/2012</remarks>
        Public Sub New()
            Me.exchangeRate = Nothing
            Me.exchangeRateDate = Nothing
        End Sub
    End Class

    <Serializable()> _
    Public Class LanguageCodeType
        Public value As String
        Public Const ARABE_AR As String = "ar"
        Public Const BIELORRUSO_BE As String = "be"
        Public Const BULGARO_BG As String = "bg"
        Public Const CATALAN_CA As String = "ca"
        Public Const CHECO_CS As String = "cs"
        Public Const DANES_DA As String = "da"
        Public Const ALEMAN_DE As String = "de"
        Public Const GRIEGO_MODERNO_EL As String = "el"
        Public Const INGLES_EN As String = "en"
        Public Const ESPANYOL_ES As String = "es"
        Public Const ESTONIO_ET As String = "et"
        Public Const VASCUENCE_EU As String = "eu"
        Public Const FINLANDES_FI As String = "fi"
        Public Const FRANCES_FR As String = "fr"
        Public Const GAELICO_DE_IRLANDA_GA As String = "ga"
        Public Const GALLEGO_GL As String = "gl"
        Public Const CROATA_HR As String = "hr"
        Public Const HUNGARO_HU As String = "hu"
        Public Const ISLANDES_IS As String = "is"
        Public Const ITALIANO_IT As String = "it"
        Public Const LETON_LV As String = "lv"
        Public Const LITUANO_LT As String = "lt"
        Public Const MACEDONIO_MK As String = "mk"
        Public Const MALTES_MT As String = "mt"
        Public Const NEERLANDES_NL As String = "nl"
        Public Const NORUEGO_NO As String = "no"
        Public Const POLACO_PL As String = "pl"
        Public Const PORTUGUES_PT As String = "pt"
        Public Const RUMANO_RO As String = "ro"
        Public Const RUSO_RU As String = "ru"
        Public Const ESLOVACO_SK As String = "sk"
        Public Const ESLOVENO_SL As String = "sl"
        Public Const ALBANES_SQ As String = "sq"
        Public Const SERBIO_SR As String = "sr"
        Public Const SUECO_SV As String = "sv"
        Public Const TURCO_TR As String = "tr"
        Public Const UCRANIANO_UK As String = "uk"

        ''' <summary>
        ''' Constructura del objeto. Solo establece en valores iniciales los atributos
        ''' de instancia que definen parte de la estructura de un objeto facturae.
        ''' </summary>
        ''' <remarks>Llamada desde: CFacturae.construirInvoiceIssueData, CFacturae.invoiceIssueDataXML2Obj
        ''' Tiempo máximo: menor a 0,1
        ''' Revisado por: auv. 18/05/2012</remarks>
        Public Sub New()
            Me.value = ""
        End Sub
    End Class

    <Serializable()> _
    Public Class TaxOutputType
        Public taxTypeCode As TaxTypeCodeType
        Public taxRate As DoubleTwoDecimalType
        Public taxableBase As AmountType
        Public taxAmount As AmountType
        Public specialTaxableBase As AmountType
        Public specialTaxAmount As AmountType
        Public equivalenceSurcharge As DoubleTwoDecimalType
        Public equivalenceSurchargeAmount As AmountType

        ''' <summary>
        ''' Constructura del objeto. Solo establece en valores iniciales los atributos
        ''' de instancia que definen parte de la estructura de un objeto facturae.
        ''' </summary>
        ''' <remarks>Llamada desde: CFacturae.construirItems, CFacturae.construirTaxesOutputs, 
        ''' CFacturae.invoiceTaxesOutputsXML2Obj, CFacturae.invoiceItemTaxesOutputsXML2Obj
        ''' Tiempo máximo: menor a 0,1
        ''' Revisado por: auv. 18/05/2012</remarks>
        Public Sub New()
            Me.taxTypeCode = Nothing
            Me.taxRate = Nothing
            Me.taxableBase = Nothing
            Me.taxAmount = Nothing
            Me.equivalenceSurcharge = Nothing
            Me.equivalenceSurchargeAmount = Nothing
        End Sub
    End Class

    <Serializable()> _
    Public Class TaxTypeCodeType
        Public value As String
        Public Const IMPUESTO_IVA_01 As String = "01"
        Public Const IMPUESTO_IPSI_02 As String = "02"
        Public Const IMPUESTO_IGIC_03 As String = "03"
        Public Const IMPUESTO_IRPF_04 As String = "04"
        Public Const OTRO_IMPUESTO_05 As String = "05"
        Public Const IMPUESTO_ITPAJD_06 As String = "06"
        Public Const IMPUESTO_IE_07 As String = "07"
        Public Const IMPUESTO_RA_08 As String = "08"
        Public Const IMPUESTO_IGTECM_09 As String = "09"
        Public Const IMPUESTO_IECDPCAC_10 As String = "10"
        Public Const IMPUESTO_IIIMAB_11 As String = "11"
        Public Const IMPUESTO_ICIO_12 As String = "12"
        Public Const IMPUESTO_IMVDN_13 As String = "13"
        Public Const IMPUESTO_IMSN_14 As String = "14"
        Public Const IMPUESTO_IMGSN_15 As String = "15"
        Public Const IMPUESTO_IMPN_16 As String = "16"
        Public Const IMPUESTO_REIVA_17 As String = "17"
        Public Const IMPUESTO_REIGIC_18 As String = "18"
        Public Const IMPUESTO_REIPSI_19 As String = "19"

        ''' <summary>
        ''' Constructura del objeto. Solo establece en valores iniciales los atributos
        ''' de instancia que definen parte de la estructura de un objeto facturae.
        ''' </summary>
        ''' <remarks>Llamada desde: CFacturae.construirItems, CFacturae.invoiceTaxOutputXML2Obj, 
        ''' CFacturae.invoiceTaxWithheldXML2Obj, CFacturae.invoiceItemTaxWithheldXML2Obj, 
        ''' CFacturae.invoiceItemTaxOutputXML2Obj
        ''' Tiempo máximo: menor a 0,1
        ''' Revisado por: auv. 18/05/2012</remarks>
        Public Sub New()
            Me.value = ""
        End Sub
    End Class

    <Serializable()> _
    Public Class TaxWithheldType
        Public taxTypeCode As TaxTypeCodeType
        Public taxRate As DoubleTwoDecimalType
        Public taxableBase As AmountType
        Public taxAmount As AmountType

        ''' <summary>
        ''' Constructura del objeto. Solo establece en valores iniciales los atributos
        ''' de instancia que definen parte de la estructura de un objeto facturae.
        ''' </summary>
        ''' <remarks>Llamada desde: CFacturae.construirItems, CFacturae.construirTaxesWithheld, 
        ''' CFacturae.invoiceTaxesWithheldXML2Obj, CFacturae.invoiceItemTaxesWithheldXML2Obj
        ''' Tiempo máximo: menor a 0,1
        ''' Revisado por: auv. 18/05/2012</remarks>
        Public Sub New()
            Me.taxTypeCode = Nothing
            Me.taxRate = Nothing
            Me.taxableBase = Nothing
            Me.taxAmount = Nothing
        End Sub
    End Class

    <Serializable()> _
    Public Class InvoiceTotalsType
        Public totalGrossAmount As DoubleTwoDecimalType
        Public generalDiscounts() As DiscountType
        Public generalSurcharges() As ChargeType
        Public totalGeneralDiscounts As DoubleTwoDecimalType
        Public totalGeneralSurcharges As DoubleTwoDecimalType
        Public totalGrossAmountBeforeTaxes As DoubleTwoDecimalType
        Public totalTaxOutputs As DoubleTwoDecimalType
        Public totalTaxesWithheld As DoubleTwoDecimalType
        Public invoiceTotal As DoubleTwoDecimalType
        Public totalOutstandingAmount As DoubleTwoDecimalType
        Public amountsWithheld As AmountsWithheldType
        Public totalExecutableAmount As DoubleTwoDecimalType

        ''' <summary>
        ''' Constructura del objeto. Solo establece en valores iniciales los atributos
        ''' de instancia que definen parte de la estructura de un objeto facturae.
        ''' </summary>
        ''' <remarks>Llamada desde: CFacturae.construirInvoiceTotals, CFacturae.InvoiceXML2Obj
        ''' Tiempo máximo: menor a 0,1
        ''' Revisado por: auv. 18/05/2012</remarks>
        Public Sub New()
            Me.totalGrossAmount = Nothing
            Me.generalDiscounts = Nothing
            Me.generalSurcharges = Nothing
            Me.totalGeneralDiscounts = Nothing
            Me.totalGeneralSurcharges = Nothing
            Me.totalTaxOutputs = Nothing
            Me.totalTaxesWithheld = Nothing
            Me.invoiceTotal = Nothing
            Me.totalOutstandingAmount = Nothing
            Me.amountsWithheld = Nothing
            Me.totalExecutableAmount = Nothing
        End Sub
    End Class

    <Serializable()> _
    Public Class DiscountType
        Private m_oDiscountReason As String
        Public Property discountReason As String
            Get
                Return Me.m_oDiscountReason
            End Get
            Set(ByVal value As String)
                If value.Length > 2500 Then
                    Me.m_oDiscountReason = value.Substring(0, 2500)
                Else
                    Me.m_oDiscountReason = value
                End If
            End Set
        End Property
        Public discountRate As DoubleFourDecimalType
        Public discountAmount As DoubleSixDecimalType

        ''' <summary>
        ''' Constructura del objeto. Solo establece en valores iniciales los atributos
        ''' de instancia que definen parte de la estructura de un objeto facturae.
        ''' </summary>
        ''' <remarks>Llamada desde: CFacturae.construirItems, CFacturae.construirInvoiceTotals, 
        ''' CFacturae.invoiceTotalsXML2Obj, CFacturae.invoiceItemXML2Obj
        ''' Tiempo máximo: menor a 0,1
        ''' Revisado por: auv. 18/05/2012</remarks>
        Public Sub New()
            Me.discountReason = ""
            Me.discountRate = Nothing
            Me.discountAmount = Nothing
        End Sub
    End Class

    <Serializable()> _
    Public Class DoubleFourDecimalType
        Public value As Double

        ''' <summary>
        ''' Constructura del objeto. Solo establece en valores iniciales los atributos
        ''' de instancia que definen parte de la estructura de un objeto facturae.
        ''' </summary>
        ''' <remarks>Llamada desde: CFacturae.invoiceTotalsXML2Obj, CFacturae.invoiceItemXML2Obj
        ''' Tiempo máximo: menor a 0,1
        ''' Revisado por: auv. 18/05/2012</remarks>
        Public Sub New()
            value = 0
        End Sub

        Public Sub New(ByVal value As Double)
            Me.value = value
        End Sub

        Public Shared Widening Operator CType(ByVal o As DoubleFourDecimalType) As Double
            Return o.value
        End Operator

        Public Shared Widening Operator CType(ByVal value As Double) As DoubleFourDecimalType
            Return New DoubleFourDecimalType(value)
        End Operator

        Public Property dValue() As String
            Get
                Return value.ToString("F4", System.Globalization.CultureInfo.InvariantCulture)
            End Get
            Set(ByVal value As String)
                Dim separadorDbl As String
                If CDbl("0,1") = CDbl("1") Then
                    separadorDbl = "."
                Else
                    separadorDbl = ","
                End If
                Me.value = System.Convert.ToDouble(CDbl((value.Replace(",", separadorDbl)).Replace(".", separadorDbl)))
            End Set
        End Property
    End Class

    <Serializable()> _
    Public Class DoubleSixDecimalType
        Public value As Double

        ''' <summary>
        ''' Constructura del objeto. Solo establece en valores iniciales los atributos
        ''' de instancia que definen parte de la estructura de un objeto facturae.
        ''' </summary>
        ''' <remarks>Llamada desde: CFacturae.invoiceTotalsXML2Obj, CFacturae.invoiceItemXML2Obj
        ''' Tiempo máximo: menor a 0,1
        ''' Revisado por: auv. 18/05/2012</remarks>
        Public Sub New()
            value = 0
        End Sub

        Public Sub New(ByVal value As Double)
            Me.value = value
        End Sub

        Public Shared Widening Operator CType(ByVal o As DoubleSixDecimalType) As Double
            Return o.value
        End Operator

        Public Shared Widening Operator CType(ByVal value As Double) As DoubleSixDecimalType
            Return New DoubleSixDecimalType(value)
        End Operator

        Public Property dValue() As String
            Get
                Return value.ToString("F6", System.Globalization.CultureInfo.InvariantCulture)
            End Get
            Set(ByVal value As String)
                Dim separadorDbl As String
                If CDbl("0,1") = CDbl("1") Then
                    separadorDbl = "."
                Else
                    separadorDbl = ","
                End If
                Me.value = System.Convert.ToDouble(CDbl((value.Replace(",", separadorDbl)).Replace(".", separadorDbl)))
            End Set
        End Property
    End Class

    <Serializable()> _
    Public Class ChargeType
        Private m_oChargeReason As String
        Public Property chargeReason As String
            Get
                Return Me.m_oChargeReason
            End Get
            Set(ByVal value As String)
                If value.Length > 2500 Then
                    Me.m_oChargeReason = value.Substring(0, 2500)
                Else
                    Me.m_oChargeReason = value
                End If
            End Set
        End Property
        Public chargeRate As DoubleFourDecimalType
        Public chargeAmount As DoubleSixDecimalType

        ''' <summary>
        ''' Constructura del objeto. Solo establece en valores iniciales los atributos
        ''' de instancia que definen parte de la estructura de un objeto facturae.
        ''' </summary>
        ''' <remarks>Llamada desde: CFacturae.construirItems, CFacturae.construirInvoiceTotals, 
        ''' CFacturae.invoiceTotalsXML2Obj, CFacturae.invoiceItemXML2Obj
        ''' Tiempo máximo: menor a 0,1
        ''' Revisado por: auv. 18/05/2012</remarks>
        Public Sub New()
            Me.chargeReason = ""
            Me.chargeRate = Nothing
            Me.chargeAmount = Nothing
        End Sub
    End Class

    <Serializable()> _
    Public Class AmountsWithheldType
        Private m_oWithholdingReason As String
        Public Property withholdingReason As String
            Get
                Return Me.m_oWithholdingReason
            End Get
            Set(ByVal value As String)
                If value.Length > 2500 Then
                    Me.m_oWithholdingReason = value.Substring(0, 2500)
                Else
                    Me.m_oWithholdingReason = value
                End If
            End Set
        End Property
        Public withholdingRate As DoubleFourDecimalType
        Public withholdingAmount As DoubleTwoDecimalType

        ''' <summary>
        ''' Constructura del objeto. Solo establece en valores iniciales los atributos
        ''' de instancia que definen parte de la estructura de un objeto facturae.
        ''' </summary>
        ''' <remarks>Llamada desde: Cfacturae.construirInvoiceTotals, CFacturae.invoiceTotalsXML2Obj
        ''' Tiempo máximo: menor a 0,1
        ''' Revisado por: auv. 18/05/2012</remarks>
        Public Sub New()
            Me.withholdingReason = ""
            Me.withholdingRate = Nothing
            Me.withholdingAmount = Nothing
        End Sub
    End Class

    <Serializable()> _
    Public Class ItemType
        Public sequenceNumber As Double
        Public deliveryNotesReferences() As DeliveryNoteType
        Private m_oItemDescription As String
        Public Property itemDescription As String
            Get
                Return Me.m_oItemDescription
            End Get
            Set(ByVal value As String)
                If value.Length > 2500 Then
                    Me.m_oItemDescription = value.Substring(0, 2500)
                Else
                    Me.m_oItemDescription = value
                End If
            End Set
        End Property
        Public quantity As Double
        Public unitOfMeasure As UnitOfMeasureType
        Public unitPriceWithoutTax As DoubleSixDecimalType
        Public totalCost As DoubleSixDecimalType
        Public discountsAndRebates() As DiscountType
        Public charges() As ChargeType
        Public grossAmount As DoubleSixDecimalType
        Public taxesWithheld() As TaxWithheldType
        Public taxesOutputs() As TaxOutputType
        Public specialTaxableEvent As SpecialTaxableEvent
        Private m_oArticleCode As String
        Public Property articleCode As String
            Get
                Return Me.m_oArticleCode
            End Get
            Set(ByVal value As String)
                If value.Length > 20 Then
                    Me.m_oArticleCode = value.Substring(0, 20)
                Else
                    Me.m_oArticleCode = value
                End If
            End Set
        End Property

        ''' <summary>
        ''' Constructura del objeto. Solo establece en valores iniciales los atributos
        ''' de instancia que definen parte de la estructura de un objeto facturae.
        ''' </summary>
        ''' <remarks>Llamada desde: CFacturae.construirItems, CFacturae.invoiceItemsXML2Obj
        ''' Tiempo máximo: menor a 0,1
        ''' Revisado por: auv. 18/05/2012</remarks>
        Public Sub New()
            Me.sequenceNumber = 0.0
            Me.deliveryNotesReferences = Nothing
            Me.itemDescription = ""
            Me.quantity = 0.0
            Me.unitOfMeasure = Nothing
            Me.unitPriceWithoutTax = Nothing
            Me.totalCost = Nothing
            Me.discountsAndRebates = Nothing
            Me.charges = Nothing
            Me.grossAmount = Nothing
            Me.taxesWithheld = Nothing
            Me.taxesOutputs = Nothing
            Me.specialTaxableEvent = Nothing
            Me.articleCode = ""
        End Sub
    End Class

    Public Class SpecialTaxableEvent
        Public specialTaxableEventCode As SpecialTaxableEventCode
        Private m_oSpecialTaxableEventReason As String
        Public Property specialTaxableEventReason As String
            Get
                Return Me.m_oSpecialTaxableEventReason
            End Get
            Set(ByVal value As String)
                If value.Length > 2500 Then
                    Me.m_oSpecialTaxableEventReason = value.Substring(0, 2500)
                Else
                    Me.m_oSpecialTaxableEventReason = value
                End If
            End Set
        End Property

        ''' <summary>
        ''' Constructura del objeto. Solo establece en valores iniciales los atributos
        ''' de instancia que definen parte de la estructura de un objeto facturae.
        ''' </summary>
        ''' <remarks>Llamada desde: CFacturae.construirItems, CFacturae.invoiceItemXML2Obj
        ''' Tiempo máximo: menor a 0,1
        ''' Revisado por: auv. 18/05/2012</remarks>
        Public Sub New()
            Me.specialTaxableEventCode = Nothing
            Me.specialTaxableEventReason = ""
        End Sub
    End Class

    Public Class SpecialTaxableEventCode
        Public value As String
        Public Const OPERACION_SUJETA_Y_EXENTA_01 As String = "01"
        Public Const OPERACION_NO_SUJETA_02 As String = "02"

        ''' <summary>
        ''' Constructura del objeto. Solo establece en valores iniciales los atributos
        ''' de instancia que definen parte de la estructura de un objeto facturae.
        ''' </summary>
        ''' <remarks>Llamada desde: CFacturae.construirItems, CFacturae.invoiceItemXML2Obj
        ''' Tiempo máximo: menor a 0,1
        ''' Revisado por: auv. 18/05/2012</remarks>
        Public Sub New()
            Me.value = ""
        End Sub
    End Class

    <Serializable()> _
    Public Class DeliveryNoteType
        Private m_oDeliveryNoteNumber As String
        Public Property deliveryNoteNumber As String
            Get
                Return Me.m_oDeliveryNoteNumber
            End Get
            Set(ByVal value As String)
                If value.Length > 30 Then
                    Me.m_oDeliveryNoteNumber = value.Substring(0, 30)
                Else
                    Me.m_oDeliveryNoteNumber = value
                End If
            End Set
        End Property
        Public deliveryNoteDate As Date

        ''' <summary>
        ''' Constructura del objeto. Solo establece en valores iniciales los atributos
        ''' de instancia que definen parte de la estructura de un objeto facturae.
        ''' </summary>
        ''' <remarks>Llamada desde: CFacturae.construirItems, CFacturae.invoiceItemXML2Obj
        ''' Tiempo máximo: menor a 0,1
        ''' Revisado por: auv. 18/05/2012</remarks>
        Public Sub New()
            Me.deliveryNoteNumber = ""
            Me.deliveryNoteDate = Nothing
        End Sub
    End Class

    <Serializable()> _
    Public Class UnitOfMeasureType
        Public value As String
        Public Const UNIDADES_01 As String = "01"
        Public Const HORAS_HUR_02 As String = "02"
        Public Const KILOGRAMOS_KGM_03 As String = "03"
        Public Const LITROS_LTR_04 As String = "04"
        Public Const OTROS_05 As String = "05"
        Public Const CAJAS_BX_06 As String = "06"
        Public Const BANDEJAS_DS_07 As String = "07"
        Public Const BARRILES_BA_08 As String = "08"
        Public Const BIDONES_JY_09 As String = "09"
        Public Const BOLSAS_BG_10 As String = "10"
        Public Const BOMBONAS_CO_11 As String = "11"
        Public Const BOTELLAS_BO_12 As String = "12"
        Public Const BOTES_CI_13 As String = "13"
        Public Const TETRA_BRIKS_14 As String = "14"
        Public Const CENTILITROS_CLT_15 As String = "15"
        Public Const CENTIMETROS_CMT_16 As String = "16"
        Public Const CUBOS_BI_17 As String = "17"
        Public Const DOCENAS_18 As String = "18"
        Public Const ESTUCHES_CS_19 As String = "19"
        Public Const GARRAFAS_DJ_20 As String = "20"
        Public Const GRAMOS_GRM_21 As String = "21"
        Public Const KILOMETROS_KMT_22 As String = "22"
        Public Const LATAS_CA_23 As String = "23"
        Public Const MANOJOS_BH_24 As String = "24"
        Public Const METROS_MTR_25 As String = "25"
        Public Const MILIMETROS_MMT_26 As String = "26"
        Public Const SIX_PACKS_27 As String = "27"
        Public Const PAQUETES_PK_28 As String = "28"
        Public Const RACIONES_29 As String = "29"
        Public Const ROLLOS_RO_30 As String = "30"
        Public Const SOBRES_EN_31 As String = "31"
        Public Const TARRINAS_TB_32 As String = "32"
        Public Const METRO_CUBICO_MTQ_33 As String = "33"
        Public Const SEGUNDO_SEC_34 As String = "34"
        Public Const VATIO_WTT_35 As String = "35"

        ''' <summary>
        ''' Constructura del objeto. Solo establece en valores iniciales los atributos
        ''' de instancia que definen parte de la estructura de un objeto facturae.
        ''' </summary>
        ''' <remarks>Llamada desde: CFacturae.construirItems, CFacturae.invoiceItemXML2Obj
        ''' Tiempo máximo: menor a 0,1
        ''' Revisado por: auv. 18/05/2012</remarks>
        Public Sub New()
            Me.value = ""
        End Sub
    End Class

    <Serializable()> _
    Public Class AdditionalDataType
        Private m_oRelatedInvoice As String
        Public Property relatedInvoice As String
            Get
                Return Me.m_oRelatedInvoice
            End Get
            Set(ByVal value As String)
                If value.Length > 40 Then
                    Me.m_oRelatedInvoice = value.Substring(0, 40)
                Else
                    Me.m_oRelatedInvoice = value
                End If
            End Set
        End Property
        Private m_oInvoiceAdditionalInformation As String
        Public Property invoiceAdditionalInformation As String
            Get
                Return Me.m_oInvoiceAdditionalInformation
            End Get
            Set(ByVal value As String)
                If value.Length > 2500 Then
                    Me.m_oInvoiceAdditionalInformation = value.Substring(0, 2500)
                Else
                    Me.m_oInvoiceAdditionalInformation = value
                End If
            End Set
        End Property

        ''' <summary>
        ''' Constructura del objeto. Solo establece en valores iniciales los atributos
        ''' de instancia que definen parte de la estructura de un objeto facturae.
        ''' </summary>
        ''' <remarks>Llamada desde: CFacturae.invoiceXML2Obj
        ''' Tiempo máximo: menor a 0,1
        ''' Revisado por: auv. 18/05/2012</remarks>
        Public Sub New()
            Me.relatedInvoice = ""
            Me.invoiceAdditionalInformation = ""
        End Sub
    End Class
#End Region

    Private cn As SqlConnection
    Private sIdFact As String = ""
    Private sIdi As String = ""
    Private datosGeneralesFactura As DataTable = Nothing
    Private datosCostesDescuentosFactura As DataTable = Nothing
    Private datosImpuestosRepercutidosRetenidosFactura As DataTable = Nothing
    Private datosGeneralesProveedor As DataTable = Nothing
    Private datosDeContactorProveedor As DataTable = Nothing
    Private datosGeneralesComprador As DataTable = Nothing
    Private datosGeneralesAlbaran As DataTable = Nothing
    Private datosGeneralesItems As DataTable = Nothing
    Private datosCostesDescuentosItem As DataTable = Nothing
    Private datosFirma As DataTable = Nothing

    ''' <summary>
    ''' Funcionalidad encargada de construir un documento XML Facturae v3.2 dado el número de la factura
    ''' y el nombre del fichero donde debe residir el documento a crear. O crea una factura con los mínimos
    ''' o devuelve un error.
    ''' </summary>
    ''' <param name="psNomFichXMLFac">Nombre+path del lugar donde residirá el documento XML Facturae v3.2</param>
    ''' <param name="psIdioma">Idioma que emplea la aplicación FS</param>
    ''' <remarks>Llamada desde: ¿?
    ''' Tiempo máximo: Depende del número de elementos que tengan los vectores de longitud variable tales como
    ''' el número de items, descuentos, costes, impuestos repercutidos, impuestos retenidos, etc...
    ''' Revisado por: auv. 25/05/2012</remarks>
    Public Sub crearFacV3_2(ByVal psIdFac As String, ByVal psNomFichXMLFac As String, ByVal psIdioma As String)
        Dim doc As XmlDocument = Nothing
        Dim cn As SqlConnection = Nothing
        Dim f As CFirma = Nothing
        Dim pwd As String
        Try
            cn = New SqlConnection(DBServer.DBConnection)
            If Not cn.State = ConnectionState.Open Then cn.Open()
            Authenticate()

            sIdFact = psIdFac

            sIdi = psIdioma
            Me.construirFacturae()
            doc = Me.facturaeObj2XML()
            doc.Save(psNomFichXMLFac)
            f = New CFirma(Me.DBServer, True)
            datosFirma = DBServer.obtDatosFirma(sIdFact)
            If Not IsNothing(datosGeneralesProveedor) AndAlso datosGeneralesProveedor.Rows.Count > 0 Then
                If Not (IsDBNull(datosFirma.Rows(0)("PWD")) Or IsNothing(datosFirma.Rows(0)("PWD"))) Then
                    pwd = datosFirma.Rows(0)("PWD")
                Else
                    pwd = ""
                End If
                f.firmaFacV3_2Automatica(datosFirma.Rows(0)("NIF"), pwd, psNomFichXMLFac, psNomFichXMLFac)
            End If
            cn.Dispose()
            cn = Nothing
        Catch ex As Exception
            cn.Dispose()
            cn = Nothing
            doc = New XmlDocument
            doc.LoadXml("<ERROR>ERROR WHEN BUILDING THE ISSUE VERSION 3.2: " & Replace(Replace(Replace(ex.Message, "<", ""), ">", ""), "/", "") & "|||||STACKTRACE =" & Replace(Replace(Replace(ex.StackTrace, "<", ""), ">", ""), "/", "") & "</ERROR>")
            doc.Save(psNomFichXMLFac)
        End Try
    End Sub
#Region "SetObjFacturae"
    ''' <summary>
    ''' Método que se encarga de recoger gran parte de los datos de BD para la generación de la factura.
    ''' </summary>
    ''' <remarks>Llamada desde: CFacturae.construirFacturae
    ''' Tiempo máximo: Depende del número de elementos que tengan los vectores de longitud variable tales como
    ''' el número de items, descuentos, costes, impuestos repercutidos, impuestos retenidos, etc...
    ''' Revisado por: auv. 05/06/2012</remarks>
    Private Sub obtDatosBD()
        Authenticate()
        datosGeneralesFactura = DBServer.obtDatosFactura(sIdFact)
        datosCostesDescuentosFactura = DBServer.obtCostesYDescuentosFactura(sIdFact)
        datosImpuestosRepercutidosRetenidosFactura = DBServer.obtImpuestosRepercutidosRetenidosFactura(sIdFact)
        datosGeneralesProveedor = DBServer.obtDatosGeneralesProveedor(sIdFact, sIdi)
        datosDeContactorProveedor = DBServer.obtDatosDeContactoProveedor(sIdFact)
        datosGeneralesComprador = DBServer.obtDatosGeneralesComprador(sIdFact, sIdi)
        datosGeneralesItems = DBServer.obtDatosGeneralesItems(sIdFact)
    End Sub
    ''' <summary>
    ''' Se encarga de construir la estructura del objeto CFacturae y rellenarla con datos de la bd
    ''' empleando el número de factura almacenado en el atributo del objeto.
    ''' </summary>
    ''' <remarks>Llamado desde: CFacturae.crearFac3_2
    ''' Tiempo máximo: Depende del número de elementos que tengan los vectores de longitud variable tales como
    ''' el número de items, descuentos, costes, impuestos repercutidos, impuestos retenidos, etc...
    ''' Revisado por: auv. 25/05/2012</remarks>
    Private Sub construirFacturae()
        Me.obtDatosBD()
        Me.fileHeader = New FileHeaderType
        Me.fileHeader.schemaVersion = New SchemaVersionType
        Me.fileHeader.schemaVersion.value = SchemaVersionType.ITEM32
        Me.fileHeader.modality = New ModalityType
        Me.fileHeader.modality.value = ModalityType.INDIVIDUAL_I
        Me.fileHeader.invoiceIssuerType = New InvoiceIssuerTypeType
        Me.fileHeader.invoiceIssuerType.value = InvoiceIssuerTypeType.EMISOR_EM

        Me.construirParties()
        Me.construirInvoices()
        Me.construirBatch()
    End Sub
    ''' <summary>
    ''' Se encarga de construir la estructura del objeto BatchType y rellenarla con datos de la bd
    ''' empleando el número de factura almacenado en el atributo del objeto.
    ''' </summary>
    ''' <remarks>Llamado desde: CFacturae.construirFacturae
    ''' Tiempo máximo:  menor a 0,1
    ''' Revisado por: auv. 25/05/2012</remarks>
    Private Sub construirBatch()
        Dim contFac As Integer = Nothing

        Me.fileHeader.batch = New BatchType

        If Not IsNothing(Me.invoices(0).invoiceHeader.invoiceNumber) Then
            Me.fileHeader.batch.batchIdentifier = Me.invoices(0).invoiceHeader.invoiceNumber
        End If

        Me.fileHeader.batch.invoicesCount = "1"

        Me.fileHeader.batch.totalInvoicesAmount = New AmountType
        contFac = 0
        Me.fileHeader.batch.totalInvoicesAmount.totalAmount = New DoubleTwoDecimalType
        Me.fileHeader.batch.totalInvoicesAmount.totalAmount.value = 0.0
        While contFac < Me.invoices.Count
            Me.fileHeader.batch.totalInvoicesAmount.totalAmount.value = Me.fileHeader.batch.totalInvoicesAmount.totalAmount.value + Me.invoices(contFac).invoiceTotals.invoiceTotal.value
            contFac = contFac + 1
        End While

        Me.fileHeader.batch.totalInvoicesAmount.equivalentInEuros = New DoubleTwoDecimalType
        If Not IsNothing(datosGeneralesFactura) AndAlso datosGeneralesFactura.Rows.Count > 0 Then
            If Not (IsDBNull(datosGeneralesFactura.Rows(0)("EQUIV")) Or IsNothing(datosGeneralesFactura.Rows(0)("EQUIV"))) Then
                If Not datosGeneralesFactura.Rows(0)("EQUIV") = 0.0 Then
                    Me.fileHeader.batch.totalInvoicesAmount.equivalentInEuros.value = Me.fileHeader.batch.totalInvoicesAmount.totalAmount.value / datosGeneralesFactura.Rows(0)("EQUIV")
                Else
                    Me.fileHeader.batch.totalInvoicesAmount.equivalentInEuros.value = 0.0
                End If
            Else
                Me.fileHeader.batch.totalInvoicesAmount.equivalentInEuros.value = 0.0
            End If
        Else
            Me.fileHeader.batch.totalInvoicesAmount.equivalentInEuros.value = 0.0
        End If

        Me.fileHeader.batch.totalOutstandingAmount = New AmountType
        contFac = 0
        Me.fileHeader.batch.totalOutstandingAmount.totalAmount = New DoubleTwoDecimalType
        Me.fileHeader.batch.totalOutstandingAmount.totalAmount.value = 0.0
        While contFac < Me.invoices.Count
            Me.fileHeader.batch.totalOutstandingAmount.totalAmount.value = Me.fileHeader.batch.totalOutstandingAmount.totalAmount.value + Me.invoices(contFac).invoiceTotals.totalOutstandingAmount.value
            contFac = contFac + 1
        End While

        Me.fileHeader.batch.totalOutstandingAmount.equivalentInEuros = New DoubleTwoDecimalType
        If Not IsNothing(datosGeneralesFactura) AndAlso datosGeneralesFactura.Rows.Count > 0 Then
            If Not (IsDBNull(datosGeneralesFactura.Rows(0)("EQUIV")) Or IsNothing(datosGeneralesFactura.Rows(0)("EQUIV"))) Then
                If Not datosGeneralesFactura.Rows(0)("EQUIV") = 0.0 Then
                    Me.fileHeader.batch.totalOutstandingAmount.equivalentInEuros.value = Me.fileHeader.batch.totalOutstandingAmount.totalAmount.value / datosGeneralesFactura.Rows(0)("EQUIV")
                Else
                    Me.fileHeader.batch.totalOutstandingAmount.equivalentInEuros.value = 0.0
                End If
            Else
                Me.fileHeader.batch.totalOutstandingAmount.equivalentInEuros.value = 0.0
            End If
        Else
            Me.fileHeader.batch.totalOutstandingAmount.equivalentInEuros.value = 0.0
        End If

        Me.fileHeader.batch.totalExecutableAmount = New AmountType
        contFac = 0
        Me.fileHeader.batch.totalExecutableAmount.totalAmount = New DoubleTwoDecimalType
        Me.fileHeader.batch.totalExecutableAmount.totalAmount.value = 0.0
        While contFac < Me.invoices.Count
            Me.fileHeader.batch.totalExecutableAmount.totalAmount.value = Me.fileHeader.batch.totalExecutableAmount.totalAmount.value + Me.invoices(contFac).invoiceTotals.totalExecutableAmount.value
            contFac = contFac + 1
        End While

        Me.fileHeader.batch.totalExecutableAmount.equivalentInEuros = New DoubleTwoDecimalType
        If Not IsNothing(datosGeneralesFactura) AndAlso datosGeneralesFactura.Rows.Count > 0 Then
            If Not (IsDBNull(datosGeneralesFactura.Rows(0)("EQUIV")) Or IsNothing(datosGeneralesFactura.Rows(0)("EQUIV"))) Then
                If Not datosGeneralesFactura.Rows(0)("EQUIV") = 0.0 Then
                    Me.fileHeader.batch.totalExecutableAmount.equivalentInEuros.value = Me.fileHeader.batch.totalExecutableAmount.totalAmount.value / datosGeneralesFactura.Rows(0)("EQUIV")
                Else
                    Me.fileHeader.batch.totalExecutableAmount.equivalentInEuros.value = 0.0
                End If
            Else
                Me.fileHeader.batch.totalExecutableAmount.equivalentInEuros.value = 0.0
            End If
        Else
            Me.fileHeader.batch.totalExecutableAmount.equivalentInEuros.value = 0.0
        End If

        Me.fileHeader.batch.invoiceCurrencyCode = New CurrencyCodeType
        If Not IsNothing(datosGeneralesFactura) AndAlso datosGeneralesFactura.Rows.Count > 0 Then
            If Not (IsDBNull(datosGeneralesFactura.Rows(0)("MON_A3")) Or IsNothing(datosGeneralesFactura.Rows(0)("MON_A3"))) Then
                Me.fileHeader.batch.invoiceCurrencyCode.value = CStr(datosGeneralesFactura.Rows(0)("MON_A3"))
            End If
        End If
    End Sub
    ''' <summary>
    ''' Se encarga de construir la estructura del objeto PartiesType y rellenarla con datos de la bd
    ''' empleando el número de factura almacenado en el atributo del objeto.
    ''' </summary>
    ''' <remarks>Llamado desde: CFacturae.construirFacturae
    ''' Tiempo máximo: menor a 0,1
    ''' Revisado por: auv. 25/05/2012</remarks>
    Private Sub construirParties()
        Me.parties = New PartiesType

        Me.construirSellerParty()
        Me.construirBuyerParty()
    End Sub
    ''' <summary>
    ''' Se encarga de construir la estructura del objeto SellerPartyType y rellenarla con datos de la bd
    ''' empleando el número de factura almacenado en el atributo del objeto.
    ''' </summary>
    ''' <remarks>Llamado desde: CFacturae.construirParties
    ''' Tiempo máximo: menor a 0,1
    ''' Revisado por: auv. 25/05/2012</remarks>
    Private Sub construirSellerParty()
        Me.parties.sellerParty = New BusinessType
        Me.parties.sellerParty.taxIdentification = New TaxIdentificationType
        Me.parties.sellerParty.taxIdentification.personTypeCode = New PersonTypeCodeType
        Me.parties.sellerParty.taxIdentification.personTypeCode.value = PersonTypeCodeType.PERSONA_JURIDICA_J
        If Not IsNothing(datosGeneralesProveedor) AndAlso datosGeneralesProveedor.Rows.Count > 0 Then
            Me.parties.sellerParty.taxIdentification.residenceTypeCode = New ResidenceTypeCodeType
            If Not (IsDBNull(datosGeneralesProveedor.Rows(0)("PAI_A2")) Or IsNothing(datosGeneralesProveedor.Rows(0)("PAI_A2"))) Then
                Me.parties.sellerParty.taxIdentification.residenceTypeCode.value = facturaeCountryTypeISO3166_1_2006_Alpha_2_ToFacturaeResidenceTypeCodeType(CStr(datosGeneralesProveedor.Rows(0)("PAI_A2")))
                Me.parties.sellerParty.taxIdentification.taxIdentificationNumber = CStr(datosGeneralesProveedor.Rows(0)("PAI_A2"))
            Else
                Me.parties.sellerParty.taxIdentification.residenceTypeCode.value = ""
                Me.parties.sellerParty.taxIdentification.taxIdentificationNumber = ""
            End If
            If Not (IsDBNull(datosGeneralesProveedor.Rows(0)("NIF")) Or IsNothing(datosGeneralesProveedor.Rows(0)("NIF"))) Then
                Me.parties.sellerParty.taxIdentification.taxIdentificationNumber = Me.parties.sellerParty.taxIdentification.taxIdentificationNumber & CStr(datosGeneralesProveedor.Rows(0)("NIF"))
            End If
        End If
        Me.parties.sellerParty.legalEntity = New LegalEntityType
        If Not IsNothing(datosGeneralesProveedor) AndAlso datosGeneralesProveedor.Rows.Count > 0 Then
            If Not (IsDBNull(datosGeneralesProveedor.Rows(0)("DEN")) Or IsNothing(datosGeneralesProveedor.Rows(0)("DEN"))) Then
                Me.parties.sellerParty.legalEntity.corporateName = CStr(datosGeneralesProveedor.Rows(0)("DEN"))
            Else
                Me.parties.sellerParty.legalEntity.corporateName = ""
            End If
            If Not (IsDBNull(datosGeneralesProveedor.Rows(0)("PAI_A3")) Or IsNothing(datosGeneralesProveedor.Rows(0)("PAI_A3"))) Then
                If CStr(datosGeneralesProveedor.Rows(0)("PAI_A3")) = CountryType.ESPANYA_ESP Then
                    Me.parties.sellerParty.legalEntity.addressInSpain = New AddressType
                    If Not (IsDBNull(datosGeneralesProveedor.Rows(0)("DIR")) Or IsNothing(datosGeneralesProveedor.Rows(0)("DIR"))) Then
                        Me.parties.sellerParty.legalEntity.addressInSpain.address = CStr(datosGeneralesProveedor.Rows(0)("DIR"))
                    Else
                        Me.parties.sellerParty.legalEntity.addressInSpain.address = ""
                    End If
                    Me.parties.sellerParty.legalEntity.addressInSpain.postCode = New PostCodeType
                    If Not (IsDBNull(datosGeneralesProveedor.Rows(0)("CP")) Or IsNothing(datosGeneralesProveedor.Rows(0)("CP"))) Then
                        Me.parties.sellerParty.legalEntity.addressInSpain.postCode.value = CStr(datosGeneralesProveedor.Rows(0)("CP"))
                    Else
                        Me.parties.sellerParty.legalEntity.addressInSpain.postCode.value = ""
                    End If
                    If Not (IsDBNull(datosGeneralesProveedor.Rows(0)("POB")) Or IsNothing(datosGeneralesProveedor.Rows(0)("POB"))) Then
                        Me.parties.sellerParty.legalEntity.addressInSpain.town = CStr(datosGeneralesProveedor.Rows(0)("POB"))
                    Else
                        Me.parties.sellerParty.legalEntity.addressInSpain.town = ""
                    End If
                    If Not (IsDBNull(datosGeneralesProveedor.Rows(0)("PROVI")) Or IsNothing(datosGeneralesProveedor.Rows(0)("PROVI"))) Then
                        Me.parties.sellerParty.legalEntity.addressInSpain.province = CStr(datosGeneralesProveedor.Rows(0)("PROVI"))
                    Else
                        Me.parties.sellerParty.legalEntity.addressInSpain.province = ""
                    End If
                    Me.parties.sellerParty.legalEntity.addressInSpain.countryCode = New CountryType
                    Me.parties.sellerParty.legalEntity.addressInSpain.countryCode.value = CStr(datosGeneralesProveedor.Rows(0)("PAI_A3"))
                Else
                    Me.parties.sellerParty.legalEntity.overseasAddress = New OverseasAddressType
                    If Not (IsDBNull(datosGeneralesProveedor.Rows(0)("DIR")) Or IsNothing(datosGeneralesProveedor.Rows(0)("DIR"))) Then
                        Me.parties.sellerParty.legalEntity.overseasAddress.address = CStr(datosGeneralesProveedor.Rows(0)("DIR"))
                    Else
                        Me.parties.sellerParty.legalEntity.overseasAddress.address = ""
                    End If
                    If Not (IsDBNull(datosGeneralesProveedor.Rows(0)("CP")) Or IsNothing(datosGeneralesProveedor.Rows(0)("CP"))) Then
                        Me.parties.sellerParty.legalEntity.overseasAddress.postCodeAndTown = CStr(datosGeneralesProveedor.Rows(0)("CP"))
                    Else
                        Me.parties.sellerParty.legalEntity.overseasAddress.postCodeAndTown = ""
                    End If
                    If Not (IsDBNull(datosGeneralesProveedor.Rows(0)("POB")) Or IsNothing(datosGeneralesProveedor.Rows(0)("POB"))) Then
                        Me.parties.sellerParty.legalEntity.overseasAddress.postCodeAndTown = Me.parties.sellerParty.legalEntity.overseasAddress.postCodeAndTown & CStr(datosGeneralesProveedor.Rows(0)("POB"))
                    End If
                    If Not (IsDBNull(datosGeneralesProveedor.Rows(0)("PROVI")) Or IsNothing(datosGeneralesProveedor.Rows(0)("PROVI"))) Then
                        Me.parties.sellerParty.legalEntity.overseasAddress.province = CStr(datosGeneralesProveedor.Rows(0)("PROVI"))
                    Else
                        Me.parties.sellerParty.legalEntity.overseasAddress.province = ""
                    End If
                    Me.parties.sellerParty.legalEntity.overseasAddress.countryCode = New CountryType
                    Me.parties.sellerParty.legalEntity.overseasAddress.countryCode.value = CStr(datosGeneralesProveedor.Rows(0)("PAI_A3"))
                End If
            Else
                Me.parties.sellerParty.legalEntity.overseasAddress = New OverseasAddressType
                If Not (IsDBNull(datosGeneralesProveedor.Rows(0)("DIR")) Or IsNothing(datosGeneralesProveedor.Rows(0)("DIR"))) Then
                    Me.parties.sellerParty.legalEntity.overseasAddress.address = CStr(datosGeneralesProveedor.Rows(0)("DIR"))
                Else
                    Me.parties.sellerParty.legalEntity.overseasAddress.address = ""
                End If
                If Not (IsDBNull(datosGeneralesProveedor.Rows(0)("CP")) Or IsNothing(datosGeneralesProveedor.Rows(0)("CP"))) Then
                    Me.parties.sellerParty.legalEntity.overseasAddress.postCodeAndTown = CStr(datosGeneralesProveedor.Rows(0)("CP"))
                Else
                    Me.parties.sellerParty.legalEntity.overseasAddress.postCodeAndTown = ""
                End If
                If Not (IsDBNull(datosGeneralesProveedor.Rows(0)("POB")) Or IsNothing(datosGeneralesProveedor.Rows(0)("POB"))) Then
                    Me.parties.sellerParty.legalEntity.overseasAddress.postCodeAndTown = Me.parties.sellerParty.legalEntity.overseasAddress.postCodeAndTown & CStr(datosGeneralesProveedor.Rows(0)("POB"))
                End If
                If Not (IsDBNull(datosGeneralesProveedor.Rows(0)("PROVI")) Or IsNothing(datosGeneralesProveedor.Rows(0)("PROVI"))) Then
                    Me.parties.sellerParty.legalEntity.overseasAddress.province = CStr(datosGeneralesProveedor.Rows(0)("PROVI"))
                Else
                    Me.parties.sellerParty.legalEntity.overseasAddress.province = ""
                End If
                Me.parties.sellerParty.legalEntity.overseasAddress.countryCode = New CountryType
                Me.parties.sellerParty.legalEntity.overseasAddress.countryCode.value = ""
            End If
        End If
        Me.parties.sellerParty.legalEntity.contactDetails = New ContactDetailsType
        If Not (IsNothing(datosDeContactorProveedor)) AndAlso datosDeContactorProveedor.Rows.Count > 0 Then
            If Not (IsDBNull(datosDeContactorProveedor.Rows(0)("TFNO")) Or IsNothing(datosDeContactorProveedor.Rows(0)("TFNO"))) Then
                Me.parties.sellerParty.legalEntity.contactDetails.telephone = CStr(datosDeContactorProveedor.Rows(0)("TFNO"))
            Else
                Me.parties.sellerParty.legalEntity.contactDetails.telephone = ""
            End If
            If Not (IsDBNull(datosDeContactorProveedor.Rows(0)("FAX")) Or IsNothing(datosDeContactorProveedor.Rows(0)("FAX"))) Then
                Me.parties.sellerParty.legalEntity.contactDetails.teleFax = CStr(datosDeContactorProveedor.Rows(0)("FAX"))
            Else
                Me.parties.sellerParty.legalEntity.contactDetails.teleFax = ""
            End If
            If Not (IsDBNull(datosDeContactorProveedor.Rows(0)("EMAIL")) Or IsNothing(datosDeContactorProveedor.Rows(0)("EMAIL"))) Then
                Me.parties.sellerParty.legalEntity.contactDetails.electronicMail = CStr(datosDeContactorProveedor.Rows(0)("EMAIL"))
            Else
                Me.parties.sellerParty.legalEntity.contactDetails.electronicMail = ""
            End If
            If Not (IsDBNull(datosDeContactorProveedor.Rows(0)("APE")) Or IsNothing(datosDeContactorProveedor.Rows(0)("APE"))) Then
                Me.parties.sellerParty.legalEntity.contactDetails.contactPersons = CStr(datosDeContactorProveedor.Rows(0)("APE"))
            Else
                Me.parties.sellerParty.legalEntity.contactDetails.contactPersons = ""
            End If
            If Not (IsDBNull(datosDeContactorProveedor.Rows(0)("NOM")) Or IsNothing(datosDeContactorProveedor.Rows(0)("NOM"))) Then
                If Me.parties.sellerParty.legalEntity.contactDetails.contactPersons = "" Then
                    Me.parties.sellerParty.legalEntity.contactDetails.contactPersons = CStr(datosDeContactorProveedor.Rows(0)("NOM"))
                Else
                    Me.parties.sellerParty.legalEntity.contactDetails.contactPersons = Me.parties.sellerParty.legalEntity.contactDetails.contactPersons & ", " & CStr(datosDeContactorProveedor.Rows(0)("NOM"))
                End If
            End If
        End If
    End Sub
    ''' <summary>
    ''' Se encarga de construir la estructura del objeto BuyerPartyType y rellenarla con datos de la bd
    ''' empleando el número de factura almacenado en el atributo del objeto.
    ''' </summary>
    ''' <remarks>Llamado desde: CFacturae.construirParties
    ''' Tiempo máximo:  menor a 0,1
    ''' Revisado por: auv. 25/05/2012</remarks>
    Private Sub construirBuyerParty()
        Me.parties.buyerParty = New BusinessType
        Me.parties.buyerParty.taxIdentification = New TaxIdentificationType
        Me.parties.buyerParty.taxIdentification.personTypeCode = New PersonTypeCodeType
        Me.parties.buyerParty.taxIdentification.personTypeCode.value = PersonTypeCodeType.PERSONA_JURIDICA_J
        Me.parties.buyerParty.taxIdentification.residenceTypeCode = New ResidenceTypeCodeType
        If Not IsNothing(datosGeneralesComprador) AndAlso datosGeneralesComprador.Rows.Count > 0 Then
            If Not (IsDBNull(datosGeneralesComprador.Rows(0)("PAI_A3")) Or IsNothing(datosGeneralesComprador.Rows(0)("PAI_A3"))) Then
                Me.parties.buyerParty.taxIdentification.residenceTypeCode.value = facturaeCountryTypeISO3166_1_2006_Alpha_3_ToFacturaeResidenceTypeCodeType(CStr(datosGeneralesComprador.Rows(0)("PAI_A3")))
            Else
                Me.parties.buyerParty.taxIdentification.residenceTypeCode.value = ""
            End If
            If Not (IsDBNull(datosGeneralesComprador.Rows(0)("PAI_A2")) Or IsNothing(datosGeneralesComprador.Rows(0)("PAI_A2"))) Then
                Me.parties.buyerParty.taxIdentification.taxIdentificationNumber = CStr(datosGeneralesComprador.Rows(0)("PAI_A2"))
            Else
                Me.parties.buyerParty.taxIdentification.taxIdentificationNumber = ""
            End If
            If Not (IsDBNull(datosGeneralesComprador.Rows(0)("NIF")) Or IsNothing(datosGeneralesComprador.Rows(0)("NIF"))) Then
                Me.parties.buyerParty.taxIdentification.taxIdentificationNumber = Me.parties.buyerParty.taxIdentification.taxIdentificationNumber & CStr(datosGeneralesComprador.Rows(0)("NIF"))
            End If
        End If
        Me.parties.buyerParty.legalEntity = New LegalEntityType
        If Not IsNothing(datosGeneralesComprador) AndAlso datosGeneralesComprador.Rows.Count > 0 Then
            If Not (IsDBNull(datosGeneralesComprador.Rows(0)("DEN")) Or IsNothing(datosGeneralesComprador.Rows(0)("DEN"))) Then
                Me.parties.buyerParty.legalEntity.corporateName = CStr(datosGeneralesComprador.Rows(0)("DEN"))
            Else
                Me.parties.buyerParty.legalEntity.corporateName = ""
            End If
            If Not (IsDBNull(datosGeneralesComprador.Rows(0)("PAI_A3")) Or IsNothing(datosGeneralesComprador.Rows(0)("PAI_A3"))) Then
                If datosGeneralesComprador.Rows(0)("PAI_A3") = CountryType.ESPANYA_ESP Then
                    Me.parties.buyerParty.legalEntity.addressInSpain = New AddressType
                    If Not (IsDBNull(datosGeneralesComprador.Rows(0)("DIR")) Or IsNothing(datosGeneralesComprador.Rows(0)("DIR"))) Then
                        Me.parties.buyerParty.legalEntity.addressInSpain.address = CStr(datosGeneralesComprador.Rows(0)("DIR"))
                    Else
                        Me.parties.buyerParty.legalEntity.addressInSpain.address = ""
                    End If
                    Me.parties.buyerParty.legalEntity.addressInSpain.postCode = New PostCodeType
                    If Not (IsDBNull(datosGeneralesComprador.Rows(0)("CP")) Or IsNothing(datosGeneralesComprador.Rows(0)("CP"))) Then
                        Me.parties.buyerParty.legalEntity.addressInSpain.postCode.value = CStr(datosGeneralesComprador.Rows(0)("CP"))
                    Else
                        Me.parties.buyerParty.legalEntity.addressInSpain.postCode.value = ""
                    End If
                    If Not (IsDBNull(datosGeneralesComprador.Rows(0)("POB")) Or IsNothing(datosGeneralesComprador.Rows(0)("POB"))) Then
                        Me.parties.buyerParty.legalEntity.addressInSpain.town = CStr(datosGeneralesComprador.Rows(0)("POB"))
                    Else
                        Me.parties.buyerParty.legalEntity.addressInSpain.town = ""
                    End If
                    If Not (IsDBNull(datosGeneralesComprador.Rows(0)("PROVI")) Or IsNothing(datosGeneralesComprador.Rows(0)("PROVI"))) Then
                        Me.parties.buyerParty.legalEntity.addressInSpain.province = CStr(datosGeneralesComprador.Rows(0)("PROVI"))
                    Else
                        Me.parties.buyerParty.legalEntity.addressInSpain.province = ""
                    End If
                    Me.parties.buyerParty.legalEntity.addressInSpain.countryCode = New CountryType
                    Me.parties.buyerParty.legalEntity.addressInSpain.countryCode.value = CStr(datosGeneralesComprador.Rows(0)("PAI_A3"))
                Else
                    Me.parties.buyerParty.legalEntity.overseasAddress = New OverseasAddressType
                    If Not (IsDBNull(datosGeneralesComprador.Rows(0)("DIR")) Or IsNothing(datosGeneralesComprador.Rows(0)("DIR"))) Then
                        Me.parties.buyerParty.legalEntity.overseasAddress.address = CStr(datosGeneralesComprador.Rows(0)("DIR"))
                    Else
                        Me.parties.buyerParty.legalEntity.overseasAddress.address = ""
                    End If
                    Me.parties.buyerParty.legalEntity.overseasAddress.postCodeAndTown = ""
                    If Not (IsDBNull(datosGeneralesComprador.Rows(0)("CP")) Or IsNothing(datosGeneralesComprador.Rows(0)("CP"))) Then
                        Me.parties.buyerParty.legalEntity.overseasAddress.postCodeAndTown = CStr(datosGeneralesComprador.Rows(0)("CP"))
                    End If
                    If Not (IsDBNull(datosGeneralesComprador.Rows(0)("POB")) Or IsNothing(datosGeneralesComprador.Rows(0)("POB"))) Then
                        Me.parties.buyerParty.legalEntity.overseasAddress.postCodeAndTown = Me.parties.buyerParty.legalEntity.overseasAddress.postCodeAndTown & CStr(datosGeneralesComprador.Rows(0)("POB"))
                    End If
                    If Not (IsDBNull(datosGeneralesComprador.Rows(0)("PROVI")) Or IsNothing(datosGeneralesComprador.Rows(0)("PROVI"))) Then
                        Me.parties.buyerParty.legalEntity.overseasAddress.province = CStr(datosGeneralesComprador.Rows(0)("PROVI"))
                    Else
                        Me.parties.buyerParty.legalEntity.overseasAddress.province = ""
                    End If
                    Me.parties.buyerParty.legalEntity.overseasAddress.countryCode = New CountryType
                    Me.parties.buyerParty.legalEntity.overseasAddress.countryCode.value = CStr(datosGeneralesComprador.Rows(0)("PAI_A3"))
                End If
            Else
                Me.parties.buyerParty.legalEntity.overseasAddress = New OverseasAddressType
                If Not (IsDBNull(datosGeneralesComprador.Rows(0)("DIR")) Or IsNothing(datosGeneralesComprador.Rows(0)("DIR"))) Then
                    Me.parties.buyerParty.legalEntity.overseasAddress.address = CStr(datosGeneralesComprador.Rows(0)("DIR"))
                Else
                    Me.parties.buyerParty.legalEntity.overseasAddress.address = ""
                End If
                Me.parties.buyerParty.legalEntity.overseasAddress.postCodeAndTown = ""
                If Not (IsDBNull(datosGeneralesComprador.Rows(0)("CP")) Or IsNothing(datosGeneralesComprador.Rows(0)("CP"))) Then
                    Me.parties.buyerParty.legalEntity.overseasAddress.postCodeAndTown = CStr(datosGeneralesComprador.Rows(0)("CP"))
                End If
                If Not (IsDBNull(datosGeneralesComprador.Rows(0)("POB")) Or IsNothing(datosGeneralesComprador.Rows(0)("POB"))) Then
                    Me.parties.buyerParty.legalEntity.overseasAddress.postCodeAndTown = Me.parties.buyerParty.legalEntity.overseasAddress.postCodeAndTown & CStr(datosGeneralesComprador.Rows(0)("POB"))
                End If
                If Not (IsDBNull(datosGeneralesComprador.Rows(0)("PROVI")) Or IsNothing(datosGeneralesComprador.Rows(0)("PROVI"))) Then
                    Me.parties.buyerParty.legalEntity.overseasAddress.province = CStr(datosGeneralesComprador.Rows(0)("PROVI"))
                Else
                    Me.parties.buyerParty.legalEntity.overseasAddress.province = ""
                End If
                Me.parties.buyerParty.legalEntity.overseasAddress.countryCode = New CountryType
                Me.parties.buyerParty.legalEntity.overseasAddress.countryCode.value = ""
            End If
        End If
    End Sub
    ''' <summary>
    ''' Se encarga de construir la estructura de los objetos InvoiceType y rellenarla con datos de la bd
    ''' empleando el número de factura almacenado en el atributo del objeto.
    ''' </summary>
    ''' <remarks>Llamado desde: CFacturae.construirFacturae
    ''' Tiempo máximo: Depende del número de elementos que tengan los vectores de longitud variable tales como
    ''' el número de items, descuentos, costes, impuestos repercutidos, impuestos retenidos, etc...
    ''' Revisado por: auv. 25/05/2012</remarks>
    Private Sub construirInvoices()
        Me.invoices = New InvoiceType() {}
        ReDim Preserve Me.invoices(0 To 0)
        Me.invoices(0) = New InvoiceType

        Me.construirInvoiceHeader()
        Me.construirInvoiceIssueData()
        Me.construirItems()
        Me.construirTaxesOutputs()
        Me.construirTaxesWithheld()
        Me.construirInvoiceTotals()
        Me.construirInvoiceAdditionalData()
    End Sub
    ''' <summary>
    ''' Se encarga de construir la estructura del objeto InvoiceHeaderType y rellenarla con datos de la bd
    ''' empleando el número de factura almacenado en el atributo del objeto.
    ''' </summary>
    ''' <remarks>Llamado desde: CFacturae.construirInvoices
    ''' Tiempo máximo:  menor a 0,1
    ''' Revisado por: auv. 25/05/2012</remarks>
    Private Sub construirInvoiceHeader()
        Me.invoices(0).invoiceHeader = New InvoiceHeaderType
        If Not IsNothing(datosGeneralesFactura) AndAlso datosGeneralesFactura.Rows.Count > 0 Then
            Me.invoices(0).invoiceHeader.invoiceNumber = datosGeneralesFactura.Rows(0)("NUM")
        End If
        Me.invoices(0).invoiceHeader.invoiceDocumentType = New InvoiceDocumentTypeType
        Me.invoices(0).invoiceHeader.invoiceDocumentType.value = InvoiceDocumentTypeType.AUTOFACTURA_AF
        Me.invoices(0).invoiceHeader.invoiceClass = New InvoiceClassType
        If Not IsNothing(datosGeneralesFactura) AndAlso datosGeneralesFactura.Rows.Count > 0 Then
            If Not (IsDBNull(datosGeneralesFactura.Rows(0)("TIPO")) Or IsNothing(datosGeneralesFactura.Rows(0)("TIPO"))) Then
                Select Case CInt(datosGeneralesFactura.Rows(0)("TIPO"))
                    Case 1
                        Me.invoices(0).invoiceHeader.invoiceClass.value = InvoiceClassType.ORIGINAL_OO
                    Case 2
                        Me.invoices(0).invoiceHeader.invoiceClass.value = InvoiceClassType.ORIGINAL_RECTIFICATIVA_OR
                        Me.invoices(0).invoiceHeader.corrective = New CorrectiveType
                        If Not (IsDBNull(datosGeneralesFactura.Rows(0)("NUM_ORIGINAL")) Or IsNothing(datosGeneralesFactura.Rows(0)("NUM_ORIGINAL"))) Then
                            Me.invoices(0).invoiceHeader.corrective.invoiceNumber = CStr(datosGeneralesFactura.Rows(0)("NUM_ORIGINAL"))
                        Else
                            Me.invoices(0).invoiceHeader.corrective.invoiceNumber = ""
                        End If
                        Me.invoices(0).invoiceHeader.corrective.reasonCode = New ReasonCodeType
                        Me.invoices(0).invoiceHeader.corrective.reasonCode.value = ReasonCodeType.NUMERO_FACTURA_01
                        Me.invoices(0).invoiceHeader.corrective.reasonDescription = New ReasonDescriptionType
                        Me.invoices(0).invoiceHeader.corrective.reasonDescription.value = ReasonDescriptionType.NUMERO_FACTURA
                        Me.invoices(0).invoiceHeader.corrective.taxPeriod = New PeriodDatesType
                        If Not (IsDBNull(datosGeneralesFactura.Rows(0)("FEC_ORIGINAL_INI")) Or IsNothing(datosGeneralesFactura.Rows(0)("FEC_ORIGINAL_INI"))) Then
                            Me.invoices(0).invoiceHeader.corrective.taxPeriod.startDate = date_XMLStr2ObjId(CStr(datosGeneralesFactura.Rows(0)("FEC_ORIGINAL_INI")))
                        Else
                            Me.invoices(0).invoiceHeader.corrective.taxPeriod.startDate = Nothing
                        End If
                        If Not (IsDBNull(datosGeneralesFactura.Rows(0)("FEC_ORIGINAL_FIN")) Or IsNothing(datosGeneralesFactura.Rows(0)("FEC_ORIGINAL_FIN"))) Then
                            Me.invoices(0).invoiceHeader.corrective.taxPeriod.endDate = date_XMLStr2ObjId(CStr(datosGeneralesFactura.Rows(0)("FEC_ORIGINAL_FIN")))
                        Else
                            Me.invoices(0).invoiceHeader.corrective.taxPeriod.endDate = Nothing
                        End If
                        Me.invoices(0).invoiceHeader.corrective.correctionMethod = New CorrectionMethodType
                        Me.invoices(0).invoiceHeader.corrective.correctionMethod.value = CorrectionMethodType.RECTIFICACION_INTEGRA_01
                        Me.invoices(0).invoiceHeader.corrective.correctionMethodDescription = New CorrectionMethodDescriptionType
                        Me.invoices(0).invoiceHeader.corrective.correctionMethodDescription.value = CorrectionMethodDescriptionType.RECTIFICACION_INTEGRA
                End Select
            End If
        End If
    End Sub
    ''' <summary>
    ''' Se encarga de construir la estructura del objeto InvoiceIssueDataType y rellenarla con datos de la bd
    ''' empleando el número de factura almacenado en el atributo del objeto.
    ''' </summary>
    ''' <remarks>Llamado desde: CFacturae.construirInvoices
    ''' Tiempo máximo  menor a 0,1:
    ''' Revisado por: auv. 25/05/2012</remarks>
    Private Sub construirInvoiceIssueData()
        Dim fecha As DateTime = Nothing
        Dim datosCodigoFactureIdioma As DataTable = Nothing
        Authenticate()

        Me.invoices(0).invoiceIssueData = New InvoiceIssueDataType
        Me.invoices(0).invoiceIssueData.taxCurrencyCode = New CurrencyCodeType
        Me.invoices(0).invoiceIssueData.taxCurrencyCode.value = CurrencyCodeType.EURO_UNION_EUROPEA_EUR
        Me.invoices(0).invoiceIssueData.languageName = New LanguageCodeType
        datosCodigoFactureIdioma = DBServer.obtCodigoFacturaeIdioma(sIdi)

        If Not IsNothing(datosCodigoFactureIdioma) AndAlso datosCodigoFactureIdioma.Rows.Count > 0 Then
            If Not (IsDBNull(datosCodigoFactureIdioma.Rows(0)("DEN")) Or IsNothing(datosCodigoFactureIdioma.Rows(0)("DEN"))) Then
                Me.invoices(0).invoiceIssueData.languageName.value = CStr(datosCodigoFactureIdioma.Rows(0)("DEN"))
            Else
                Me.invoices(0).invoiceIssueData.languageName.value = ""
            End If
        End If
        If Not IsNothing(datosGeneralesFactura) AndAlso datosGeneralesFactura.Rows.Count > 0 Then
            If Not (IsDBNull(datosGeneralesFactura.Rows(0)("FECHA")) Or IsNothing(datosGeneralesFactura.Rows(0)("FECHA"))) Then
                fecha = CType(CStr(datosGeneralesFactura.Rows(0)("FECHA")), DateTime)
                Me.invoices(0).invoiceIssueData.issueDate = fecha.Year & "-" & IIf(fecha.Month.ToString.Length < 2, "0" & fecha.Month.ToString, fecha.Month.ToString) & "-" & IIf(fecha.Day.ToString.Length < 2, "0" & fecha.Day.ToString, fecha.Day.ToString)
            Else
                Me.invoices(0).invoiceIssueData.issueDate = Nothing
            End If
            Me.invoices(0).invoiceIssueData.invoiceCurrencyCode = New CurrencyCodeType
            If Not (IsDBNull(datosGeneralesFactura.Rows(0)("MON_A3")) Or IsNothing(datosGeneralesFactura.Rows(0)("MON_A3"))) Then
                Me.invoices(0).invoiceIssueData.invoiceCurrencyCode.value = CStr(datosGeneralesFactura.Rows(0)("MON_A3"))
            Else
                Me.invoices(0).invoiceIssueData.invoiceCurrencyCode.value = ""
            End If
            Me.invoices(0).invoiceIssueData.exchangeRateDetails = New ExchangeRateDetailsType
            If Not (IsDBNull(datosGeneralesFactura.Rows(0)("EQUIV_FECHA")) Or IsNothing(datosGeneralesFactura.Rows(0)("EQUIV_FECHA"))) Then
                fecha = CType(CStr(datosGeneralesFactura.Rows(0)("EQUIV_FECHA")), DateTime)
                Me.invoices(0).invoiceIssueData.exchangeRateDetails.exchangeRateDate = fecha.Year & "-" & IIf(fecha.Month.ToString.Length < 2, "0" & fecha.Month.ToString, fecha.Month.ToString) & "-" & IIf(fecha.Day.ToString.Length < 2, "0" & fecha.Day.ToString, fecha.Day.ToString)
            Else
                Me.invoices(0).invoiceIssueData.exchangeRateDetails.exchangeRateDate = Nothing
            End If
            Me.invoices(0).invoiceIssueData.exchangeRateDetails.exchangeRate = New DoubleTwoDecimalType
            If Not (IsDBNull(datosGeneralesFactura.Rows(0)("EQUIV")) Or IsNothing(datosGeneralesFactura.Rows(0)("EQUIV"))) Then
                Me.invoices(0).invoiceIssueData.exchangeRateDetails.exchangeRate.value = datosGeneralesFactura.Rows(0)("EQUIV")
            Else
                Me.invoices(0).invoiceIssueData.exchangeRateDetails.exchangeRate = Nothing
            End If
        End If
    End Sub
    ''' <summary>
    ''' Se encarga de construir la estructura de los objetos ItemType y rellenarla con datos de la bd
    ''' empleando el número de factura almacenado en el atributo del objeto.
    ''' </summary>
    ''' <remarks>Llamado desde: CFacturae.construirInvoices
    ''' Tiempo máximo: Depende del número de elementos que tengan los vectores de longitud variable tales como
    ''' el número de items, descuentos, costes, impuestos repercutidos, impuestos retenidos, etc...
    ''' Revisado por: auv. 25/05/2012</remarks>
    Private Sub construirItems()
        Dim fecha As DateTime = Nothing
        Dim numItem As Integer = 0
        Dim numAlb As Integer = 0
        Dim contCargosYDescuentos As Integer = 0
        Dim contCargos As Integer = 0
        Dim contDescuentos As Integer = 0
        Dim contImpRetYRep As Integer = 0
        Dim contImpRet As Integer = 0
        Dim contImpRep As Integer = 0

        Me.invoices(0).items = New ItemType() {}

        numItem = 0
        If Not IsNothing(datosGeneralesItems) AndAlso datosGeneralesItems.Rows.Count > 0 Then
            While numItem < datosGeneralesItems.Rows.Count
                ReDim Preserve Me.invoices(0).items(0 To numItem)
                Me.invoices(0).items(numItem) = New ItemType
                If Not (IsDBNull(datosGeneralesItems.Rows(numItem)("DEN")) Or IsNothing(datosGeneralesItems.Rows(numItem)("DEN"))) Then
                    Me.invoices(0).items(numItem).itemDescription = CStr(datosGeneralesItems.Rows(numItem)("DEN"))
                Else
                    Me.invoices(0).items(numItem).itemDescription = ""
                End If
                If Not (IsDBNull(datosGeneralesItems.Rows(numItem)("CANT_PED")) Or IsNothing(datosGeneralesItems.Rows(numItem)("CANT_PED"))) Then
                    Me.invoices(0).items(numItem).quantity = Double.Parse((datosGeneralesItems.Rows(numItem)("CANT_PED")))
                Else
                    Me.invoices(0).items(numItem).quantity = 0.0
                End If
                If Not (IsDBNull(datosGeneralesItems.Rows(numItem)("LINEA_FACTURA")) Or IsNothing(datosGeneralesItems.Rows(numItem)("LINEA_FACTURA"))) Then
                    Me.invoices(0).items(numItem).sequenceNumber = CInt(datosGeneralesItems.Rows(numItem)("LINEA_FACTURA"))
                End If
                Me.invoices(0).items(numItem).unitOfMeasure = New UnitOfMeasureType
                If Not (IsDBNull(datosGeneralesItems.Rows(numItem)("UP")) Or IsNothing(datosGeneralesItems.Rows(numItem)("UP"))) Then
                    Me.invoices(0).items(numItem).unitOfMeasure.value = CStr(datosGeneralesItems.Rows(numItem)("UP"))
                Else
                    Me.invoices(0).items(numItem).unitOfMeasure.value = ""
                End If

                Me.invoices(0).items(numItem).unitPriceWithoutTax = New DoubleSixDecimalType
                If Not (IsDBNull(datosGeneralesItems.Rows(numItem)("PREC_UP")) Or IsNothing(datosGeneralesItems.Rows(numItem)("PREC_UP"))) Then
                    Me.invoices(0).items(numItem).unitPriceWithoutTax.value = datosGeneralesItems.Rows(numItem)("PREC_UP")
                Else
                    Me.invoices(0).items(numItem).unitPriceWithoutTax.value = 0.0
                End If
                Me.invoices(0).items(numItem).totalCost = New DoubleSixDecimalType
                If Not IsNothing(Me.invoices(0).items(numItem).quantity) AndAlso Not IsNothing(Me.invoices(0).items(numItem).unitPriceWithoutTax) Then
                    Me.invoices(0).items(numItem).totalCost.value = Me.invoices(0).items(numItem).quantity * Me.invoices(0).items(numItem).unitPriceWithoutTax.value
                Else
                    Me.invoices(0).items(numItem).totalCost.value = 0.0
                End If

                Authenticate()

                If Not (IsDBNull(datosGeneralesItems.Rows(numItem)("LINEA_FACTURA")) Or IsNothing(datosGeneralesItems.Rows(numItem)("LINEA_FACTURA"))) Then
                    datosCostesDescuentosItem = DBServer.obtDatosCostesDescuentosItem(sIdFact, CStr(datosGeneralesItems.Rows(numItem)("LINEA_FACTURA")))
                Else
                    datosCostesDescuentosItem = Nothing
                End If

                contDescuentos = 0
                If Not IsNothing(datosCostesDescuentosItem) AndAlso datosCostesDescuentosItem.Rows.Count > 0 Then
                    contCargosYDescuentos = 0
                    While contCargosYDescuentos < datosCostesDescuentosItem.Rows.Count
                        If Not (IsDBNull(datosCostesDescuentosItem.Rows(contCargosYDescuentos)("TIPO")) Or IsNothing(datosCostesDescuentosItem.Rows(contCargosYDescuentos)("TIPO"))) Then
                            If CStr(datosCostesDescuentosItem.Rows(contCargosYDescuentos)("TIPO")) = 1 Then
                                If IsNothing(Me.invoices(0).items(numItem).discountsAndRebates) Then
                                    Me.invoices(0).items(numItem).discountsAndRebates = New DiscountType() {}
                                End If
                                ReDim Preserve Me.invoices(0).items(numItem).discountsAndRebates(0 To contDescuentos)
                                Me.invoices(0).items(numItem).discountsAndRebates(contDescuentos) = New DiscountType
                                If Not (IsDBNull(datosCostesDescuentosItem.Rows(contCargosYDescuentos)("DEN")) Or IsNothing(datosCostesDescuentosItem.Rows(contCargosYDescuentos)("DEN"))) Then
                                    Me.invoices(0).items(numItem).discountsAndRebates(contDescuentos).discountReason = CStr(datosCostesDescuentosItem.Rows(contCargosYDescuentos)("DEN"))
                                Else
                                    Me.invoices(0).items(numItem).discountsAndRebates(contDescuentos).discountReason = ""
                                End If
                                Me.invoices(0).items(numItem).discountsAndRebates(contDescuentos).discountAmount = New DoubleSixDecimalType
                                If Not (IsDBNull(datosCostesDescuentosItem.Rows(contCargosYDescuentos)("IMPORTE")) Or IsNothing(datosCostesDescuentosItem.Rows(contCargosYDescuentos)("IMPORTE"))) Then
                                    Me.invoices(0).items(numItem).discountsAndRebates(contDescuentos).discountAmount.value = datosCostesDescuentosItem.Rows(contCargosYDescuentos)("IMPORTE")
                                Else
                                    Me.invoices(0).items(numItem).discountsAndRebates(contDescuentos).discountAmount.value = 0.0
                                End If
                                If Not (IsDBNull(datosCostesDescuentosItem.Rows(contCargosYDescuentos)("OPERACION")) Or IsNothing(datosCostesDescuentosItem.Rows(contCargosYDescuentos)("OPERACION"))) Then
                                    Select Case CStr(datosCostesDescuentosItem.Rows(contCargosYDescuentos)("OPERACION"))
                                        Case "-"
                                        Case "-%"
                                            Me.invoices(0).items(numItem).discountsAndRebates(contDescuentos).discountRate = New DoubleFourDecimalType
                                            If Not (IsDBNull(datosCostesDescuentosItem.Rows(contCargosYDescuentos)("VALOR")) Or IsNothing(datosCostesDescuentosItem.Rows(contCargosYDescuentos)("VALOR"))) Then
                                                Me.invoices(0).items(numItem).discountsAndRebates(contDescuentos).discountRate.value = datosCostesDescuentosItem.Rows(contCargosYDescuentos)("VALOR")
                                            Else
                                                Me.invoices(0).items(numItem).discountsAndRebates(contDescuentos).discountRate.value = 0.0
                                            End If
                                    End Select
                                End If
                                contDescuentos = contDescuentos + 1
                            End If
                        End If
                        contCargosYDescuentos = contCargosYDescuentos + 1
                    End While
                End If

                contCargos = 0
                If Not (IsNothing(datosCostesDescuentosItem)) AndAlso datosCostesDescuentosItem.Rows.Count > 0 Then
                    contCargosYDescuentos = 0
                    While contCargosYDescuentos < datosCostesDescuentosItem.Rows.Count
                        If Not (IsDBNull(datosCostesDescuentosItem.Rows(contCargosYDescuentos)("TIPO")) Or IsNothing(datosCostesDescuentosItem.Rows(contCargosYDescuentos)("TIPO"))) Then
                            If CStr(datosCostesDescuentosItem.Rows(contCargosYDescuentos)("TIPO")) = 0 Then
                                If IsNothing(Me.invoices(0).items(numItem).charges) Then
                                    Me.invoices(0).items(numItem).charges = New ChargeType() {}
                                End If
                                ReDim Preserve Me.invoices(0).items(numItem).charges(0 To contCargos)
                                Me.invoices(0).items(numItem).charges(contCargos) = New ChargeType
                                If Not (IsDBNull(datosCostesDescuentosItem.Rows(contCargosYDescuentos)("DEN")) Or IsNothing(datosCostesDescuentosItem.Rows(contCargosYDescuentos)("DEN"))) Then
                                    Me.invoices(0).items(numItem).charges(contCargos).chargeReason = CStr(datosCostesDescuentosItem.Rows(contCargosYDescuentos)("DEN"))
                                Else
                                    Me.invoices(0).items(numItem).charges(contCargos).chargeReason = ""
                                End If
                                Me.invoices(0).items(numItem).charges(contCargos).chargeAmount = New DoubleSixDecimalType
                                If Not (IsDBNull(datosCostesDescuentosItem.Rows(contCargosYDescuentos)("IMPORTE")) Or IsNothing(datosCostesDescuentosItem.Rows(contCargosYDescuentos)("IMPORTE"))) Then
                                    Me.invoices(0).items(numItem).charges(contCargos).chargeAmount.value = datosCostesDescuentosItem.Rows(contCargosYDescuentos)("IMPORTE")
                                Else
                                    Me.invoices(0).items(numItem).charges(contCargos).chargeAmount.value = 0.0
                                End If
                                If Not (IsDBNull(datosCostesDescuentosItem.Rows(contCargosYDescuentos)("OPERACION")) Or IsNothing(datosCostesDescuentosItem.Rows(contCargosYDescuentos)("OPERACION"))) Then
                                    Select Case CStr(datosCostesDescuentosItem.Rows(contCargosYDescuentos)("OPERACION"))
                                        Case "+"
                                        Case "+%"
                                            Me.invoices(0).items(numItem).charges(contCargos).chargeRate = New DoubleFourDecimalType
                                            If Not (IsDBNull(datosCostesDescuentosItem.Rows(contCargosYDescuentos)("VALOR")) Or IsNothing(datosCostesDescuentosItem.Rows(contCargosYDescuentos)("VALOR"))) Then
                                                Me.invoices(0).items(numItem).charges(contCargos).chargeRate.value = datosCostesDescuentosItem.Rows(contCargosYDescuentos)("VALOR")
                                            Else
                                                Me.invoices(0).items(numItem).charges(contCargos).chargeRate.value = 0.0
                                            End If
                                    End Select
                                End If
                                contCargos = contCargos + 1
                            End If
                        End If
                        contCargosYDescuentos = contCargosYDescuentos + 1
                    End While
                End If

                Me.invoices(0).items(numItem).grossAmount = New DoubleSixDecimalType
                If Not IsNothing(Me.invoices(0).items(numItem).totalCost) Then
                    Me.invoices(0).items(numItem).grossAmount.value = Me.invoices(0).items(numItem).totalCost.value
                Else
                    Me.invoices(0).items(numItem).grossAmount.value = 0.0
                End If

                contDescuentos = 0
                If Not (IsNothing(Me.invoices(0).items(numItem).discountsAndRebates)) Then
                    While contDescuentos < Me.invoices(0).items(numItem).discountsAndRebates.Count
                        If Not IsNothing(Me.invoices(0).items(numItem).discountsAndRebates(contDescuentos)) AndAlso Not IsNothing(Me.invoices(0).items(numItem).discountsAndRebates(contDescuentos).discountAmount) Then
                            Me.invoices(0).items(numItem).grossAmount.value = Me.invoices(0).items(numItem).grossAmount.value _
                                - Me.invoices(0).items(numItem).discountsAndRebates(contDescuentos).discountAmount.value
                        End If
                        contDescuentos = contDescuentos + 1
                    End While
                End If

                contCargos = 0
                If Not (IsNothing(Me.invoices(0).items(numItem).charges)) Then
                    While contCargos < Me.invoices(0).items(numItem).charges.Count
                        If Not IsNothing(Me.invoices(0).items(numItem).charges(contCargos)) AndAlso Not IsNothing(Me.invoices(0).items(numItem).charges(contCargos).chargeAmount) Then
                            Me.invoices(0).items(numItem).grossAmount.value = Me.invoices(0).items(numItem).grossAmount.value _
                                + Me.invoices(0).items(numItem).charges(contCargos).chargeAmount.value
                        End If
                        contCargos = contCargos + 1
                    End While
                End If

                contImpRet = 0
                If Not (IsNothing(datosImpuestosRepercutidosRetenidosFactura)) And datosImpuestosRepercutidosRetenidosFactura.Rows.Count > 0 Then
                    contImpRetYRep = 0
                    While contImpRetYRep < datosImpuestosRepercutidosRetenidosFactura.Rows.Count
                        If Not IsDBNull(datosImpuestosRepercutidosRetenidosFactura.Rows(contImpRetYRep)("LINEA")) AndAlso _
                            Not IsNothing(datosImpuestosRepercutidosRetenidosFactura.Rows(contImpRetYRep)("LINEA")) AndAlso _
                            Not IsDBNull(datosGeneralesItems.Rows(numItem)("LINEA_FACTURA")) AndAlso _
                            Not IsNothing(datosGeneralesItems.Rows(numItem)("LINEA_FACTURA")) AndAlso _
                            CInt(datosImpuestosRepercutidosRetenidosFactura.Rows(contImpRetYRep)("LINEA")) = CInt(datosGeneralesItems.Rows(numItem)("LINEA_FACTURA")) Then
                            If Not IsDBNull(datosImpuestosRepercutidosRetenidosFactura.Rows(contImpRetYRep)("RETENIDO")) AndAlso _
                                Not IsNothing(datosImpuestosRepercutidosRetenidosFactura.Rows(contImpRetYRep)("RETENIDO")) AndAlso _
                                CStr(datosImpuestosRepercutidosRetenidosFactura.Rows(contImpRetYRep)("RETENIDO")) = 1 Then
                                If IsNothing(Me.invoices(0).items(numItem).taxesWithheld) Then
                                    Me.invoices(0).items(numItem).taxesWithheld = New TaxWithheldType() {}
                                End If
                                ReDim Preserve Me.invoices(0).items(numItem).taxesWithheld(0 To contImpRet)
                                Me.invoices(0).items(numItem).taxesWithheld(contImpRet) = New TaxWithheldType
                                Me.invoices(0).items(numItem).taxesWithheld(contImpRet).taxTypeCode = New TaxTypeCodeType
                                If Not (IsDBNull(datosImpuestosRepercutidosRetenidosFactura.Rows(contImpRetYRep)("COD")) Or IsNothing(datosImpuestosRepercutidosRetenidosFactura.Rows(contImpRetYRep)("COD"))) Then
                                    Me.invoices(0).items(numItem).taxesWithheld(contImpRet).taxTypeCode.value = CStr(datosImpuestosRepercutidosRetenidosFactura.Rows(contImpRetYRep)("COD"))
                                Else
                                    Me.invoices(0).items(numItem).taxesWithheld(contImpRet).taxTypeCode.value = ""
                                End If
                                Me.invoices(0).items(numItem).taxesWithheld(contImpRet).taxRate = New DoubleTwoDecimalType
                                If Not (IsDBNull(datosImpuestosRepercutidosRetenidosFactura.Rows(contImpRetYRep)("VALOR")) Or IsNothing(datosImpuestosRepercutidosRetenidosFactura.Rows(contImpRetYRep)("VALOR"))) Then
                                    Me.invoices(0).items(numItem).taxesWithheld(contImpRet).taxRate.value = datosImpuestosRepercutidosRetenidosFactura.Rows(contImpRetYRep)("VALOR")
                                Else
                                    Me.invoices(0).items(numItem).taxesWithheld(contImpRet).taxRate.value = 0.0
                                End If
                                Me.invoices(0).items(numItem).taxesWithheld(contImpRet).taxableBase = New AmountType
                                Me.invoices(0).items(numItem).taxesWithheld(contImpRet).taxableBase.totalAmount = New DoubleTwoDecimalType
                                Me.invoices(0).items(numItem).taxesWithheld(contImpRet).taxableBase.totalAmount.value = Me.invoices(0).items(numItem).totalCost.value
                                Me.invoices(0).items(numItem).taxesWithheld(contImpRet).taxableBase.equivalentInEuros = New DoubleTwoDecimalType
                                If Not IsNothing(Me.invoices(0).invoiceIssueData.exchangeRateDetails) AndAlso Not IsNothing(Me.invoices(0).invoiceIssueData.exchangeRateDetails.exchangeRate) Then
                                    If Not (Me.invoices(0).invoiceIssueData.exchangeRateDetails.exchangeRate.value = 0) Then
                                        Me.invoices(0).items(numItem).taxesWithheld(contImpRet).taxableBase.equivalentInEuros.value = Me.invoices(0).items(numItem).taxesWithheld(contImpRet).taxableBase.totalAmount.value / Me.invoices(0).invoiceIssueData.exchangeRateDetails.exchangeRate.value
                                    Else
                                        Me.invoices(0).items(numItem).taxesWithheld(contImpRet).taxableBase.equivalentInEuros.value = 0.0
                                    End If
                                Else
                                    Me.invoices(0).items(numItem).taxesWithheld(contImpRet).taxableBase.equivalentInEuros.value = 0.0
                                End If
                                Me.invoices(0).items(numItem).taxesWithheld(contImpRet).taxAmount = New AmountType
                                Me.invoices(0).items(numItem).taxesWithheld(contImpRet).taxAmount.totalAmount = New DoubleTwoDecimalType
                                If (Not (IsNothing(Me.invoices(0).items(numItem).taxesWithheld(contImpRet).taxRate))) And (Not (IsNothing(Me.invoices(0).items(numItem).taxesWithheld(contImpRet).taxableBase.totalAmount))) Then
                                    Me.invoices(0).items(numItem).taxesWithheld(contImpRet).taxAmount.totalAmount.value = (Me.invoices(0).items(numItem).taxesWithheld(contImpRet).taxRate.value / 100.0) * Me.invoices(0).items(numItem).taxesWithheld(contImpRet).taxableBase.totalAmount.value
                                Else
                                    Me.invoices(0).items(numItem).taxesWithheld(contImpRet).taxAmount.totalAmount.value = 0.0
                                End If
                                Me.invoices(0).items(numItem).taxesWithheld(contImpRet).taxAmount.equivalentInEuros = New DoubleTwoDecimalType
                                If Not IsNothing(Me.invoices(0).invoiceIssueData.exchangeRateDetails) AndAlso Not IsNothing(Me.invoices(0).invoiceIssueData.exchangeRateDetails.exchangeRate) Then
                                    If Not (Me.invoices(0).invoiceIssueData.exchangeRateDetails.exchangeRate.value = 0) Then
                                        Me.invoices(0).items(numItem).taxesWithheld(contImpRet).taxAmount.equivalentInEuros.value = Me.invoices(0).items(numItem).taxesWithheld(contImpRet).taxAmount.totalAmount.value / Me.invoices(0).invoiceIssueData.exchangeRateDetails.exchangeRate.value
                                    Else
                                        Me.invoices(0).items(numItem).taxesWithheld(contImpRet).taxAmount.equivalentInEuros.value = 0.0
                                    End If
                                Else
                                    Me.invoices(0).items(numItem).taxesWithheld(contImpRet).taxAmount.equivalentInEuros.value = 0.0
                                End If
                                contImpRet = contImpRet + 1
                            End If
                        End If
                        contImpRetYRep = contImpRetYRep + 1
                    End While
                End If

                contImpRep = 0
                If Not (IsNothing(datosImpuestosRepercutidosRetenidosFactura)) And datosImpuestosRepercutidosRetenidosFactura.Rows.Count > 0 Then
                    contImpRetYRep = 0
                    While contImpRetYRep < datosImpuestosRepercutidosRetenidosFactura.Rows.Count
                        If Not IsDBNull(datosImpuestosRepercutidosRetenidosFactura.Rows(contImpRetYRep)("LINEA")) AndAlso _
                            Not IsNothing(datosImpuestosRepercutidosRetenidosFactura.Rows(contImpRetYRep)("LINEA")) AndAlso _
                            Not IsDBNull(datosGeneralesItems.Rows(numItem)("LINEA_FACTURA")) AndAlso _
                            Not IsNothing(datosGeneralesItems.Rows(numItem)("LINEA_FACTURA")) AndAlso _
                            CInt(datosImpuestosRepercutidosRetenidosFactura.Rows(contImpRetYRep)("LINEA")) = CInt(datosGeneralesItems.Rows(numItem)("LINEA_FACTURA")) Then
                            If Not IsDBNull(datosImpuestosRepercutidosRetenidosFactura.Rows(contImpRetYRep)("RETENIDO")) AndAlso _
                                Not IsNothing(datosImpuestosRepercutidosRetenidosFactura.Rows(contImpRetYRep)("RETENIDO")) AndAlso _
                                CStr(datosImpuestosRepercutidosRetenidosFactura.Rows(contImpRetYRep)("RETENIDO")) = 0 Then
                                If IsNothing(Me.invoices(0).items(numItem).taxesOutputs) Then
                                    Me.invoices(0).items(numItem).taxesOutputs = New TaxOutputType() {}
                                End If
                                ReDim Preserve Me.invoices(0).items(numItem).taxesOutputs(0 To contImpRep)
                                Me.invoices(0).items(numItem).taxesOutputs(contImpRep) = New TaxOutputType
                                Me.invoices(0).items(numItem).taxesOutputs(contImpRep).taxTypeCode = New TaxTypeCodeType
                                If Not IsDBNull(datosImpuestosRepercutidosRetenidosFactura.Rows(contImpRetYRep)("COD")) AndAlso _
                                    Not IsNothing(datosImpuestosRepercutidosRetenidosFactura.Rows(contImpRetYRep)("COD")) Then
                                    Me.invoices(0).items(numItem).taxesOutputs(contImpRep).taxTypeCode.value = CStr(datosImpuestosRepercutidosRetenidosFactura.Rows(contImpRetYRep)("COD"))
                                Else
                                    Me.invoices(0).items(numItem).taxesOutputs(contImpRep).taxTypeCode.value = ""
                                End If
                                Me.invoices(0).items(numItem).taxesOutputs(contImpRep).taxRate = New DoubleTwoDecimalType
                                If Not (IsDBNull(datosImpuestosRepercutidosRetenidosFactura.Rows(contImpRetYRep)("VALOR")) Or IsNothing(datosImpuestosRepercutidosRetenidosFactura.Rows(contImpRetYRep)("VALOR"))) Then
                                    Me.invoices(0).items(numItem).taxesOutputs(contImpRep).taxRate.value = datosImpuestosRepercutidosRetenidosFactura.Rows(contImpRetYRep)("VALOR")
                                Else
                                    Me.invoices(0).items(numItem).taxesOutputs(contImpRep).taxRate.value = 0.0
                                End If
                                Me.invoices(0).items(numItem).taxesOutputs(contImpRep).taxableBase = New AmountType
                                Me.invoices(0).items(numItem).taxesOutputs(contImpRep).taxableBase.totalAmount = New DoubleTwoDecimalType
                                Me.invoices(0).items(numItem).taxesOutputs(contImpRep).taxableBase.totalAmount.value = Me.invoices(0).items(numItem).totalCost.value
                                Me.invoices(0).items(numItem).taxesOutputs(contImpRep).taxableBase.equivalentInEuros = New DoubleTwoDecimalType
                                If Not IsNothing(Me.invoices(0).invoiceIssueData.exchangeRateDetails) AndAlso Not IsNothing(Me.invoices(0).invoiceIssueData.exchangeRateDetails.exchangeRate) Then
                                    If Not (Me.invoices(0).invoiceIssueData.exchangeRateDetails.exchangeRate.value = 0) Then
                                        Me.invoices(0).items(numItem).taxesOutputs(contImpRep).taxableBase.equivalentInEuros.value = Me.invoices(0).items(numItem).taxesOutputs(contImpRep).taxableBase.totalAmount.value / Me.invoices(0).invoiceIssueData.exchangeRateDetails.exchangeRate.value
                                    Else
                                        Me.invoices(0).items(numItem).taxesOutputs(contImpRep).taxableBase.equivalentInEuros.value = 0.0
                                    End If
                                Else
                                    Me.invoices(0).items(numItem).taxesOutputs(contImpRep).taxableBase.equivalentInEuros.value = 0.0
                                End If
                                Me.invoices(0).items(numItem).taxesOutputs(contImpRep).taxAmount = New AmountType
                                Me.invoices(0).items(numItem).taxesOutputs(contImpRep).taxAmount.totalAmount = New DoubleTwoDecimalType
                                If (Not (IsNothing(Me.invoices(0).items(numItem).taxesOutputs(contImpRep).taxRate))) And (Not (IsNothing(Me.invoices(0).items(numItem).taxesOutputs(contImpRep).taxableBase.totalAmount))) Then
                                    Me.invoices(0).items(numItem).taxesOutputs(contImpRep).taxAmount.totalAmount.value = (Me.invoices(0).items(numItem).taxesOutputs(contImpRep).taxRate.value / 100.0) * Me.invoices(0).items(numItem).taxesOutputs(contImpRep).taxableBase.totalAmount.value
                                Else
                                    Me.invoices(0).items(numItem).taxesOutputs(contImpRep).taxAmount.totalAmount.value = 0.0
                                End If
                                Me.invoices(0).items(numItem).taxesOutputs(contImpRep).taxAmount.equivalentInEuros = New DoubleTwoDecimalType
                                If Not IsNothing(Me.invoices(0).invoiceIssueData.exchangeRateDetails.exchangeRate) AndAlso Not IsNothing(Me.invoices(0).invoiceIssueData.exchangeRateDetails.exchangeRate) Then
                                    If Not (Me.invoices(0).invoiceIssueData.exchangeRateDetails.exchangeRate.value = 0) Then
                                        Me.invoices(0).items(numItem).taxesOutputs(contImpRep).taxAmount.equivalentInEuros.value = Me.invoices(0).items(numItem).taxesOutputs(contImpRep).taxAmount.totalAmount.value / Me.invoices(0).invoiceIssueData.exchangeRateDetails.exchangeRate.value
                                    Else
                                        Me.invoices(0).items(numItem).taxesOutputs(contImpRep).taxAmount.equivalentInEuros.value = 0.0
                                    End If
                                Else
                                    Me.invoices(0).items(numItem).taxesOutputs(contImpRep).taxAmount.equivalentInEuros.value = 0.0
                                End If
                                contImpRep = contImpRep + 1
                            End If
                        End If
                        contImpRetYRep = contImpRetYRep + 1
                    End While
                End If

                If Not IsDBNull(datosGeneralesItems.Rows(numItem)("COD")) AndAlso _
                            Not IsNothing(datosGeneralesItems.Rows(numItem)("COD")) Then
                    Me.invoices(0).items(numItem).articleCode = CStr(datosGeneralesItems.Rows(numItem)("COD"))
                    datosGeneralesAlbaran = DBServer.obtDatosAlbaranes(sIdFact, CStr(datosGeneralesItems.Rows(numItem)("COD")))
                Else
                    Me.invoices(0).items(numItem).articleCode = ""
                    datosGeneralesAlbaran = Nothing
                End If

                numAlb = 0
                If Not (IsNothing(datosGeneralesAlbaran)) AndAlso datosGeneralesAlbaran.Rows.Count > 0 Then
                    Me.invoices(0).items(numItem).deliveryNotesReferences = New DeliveryNoteType() {}
                    numAlb = 0
                    While numAlb < datosGeneralesAlbaran.Rows.Count
                        ReDim Preserve Me.invoices(0).items(numItem).deliveryNotesReferences(0 To numAlb)
                        Me.invoices(0).items(numItem).deliveryNotesReferences(numAlb) = New DeliveryNoteType
                        If Not (IsDBNull(datosGeneralesAlbaran.Rows(numAlb)("ALBARAN")) Or IsNothing(datosGeneralesAlbaran.Rows(numAlb)("ALBARAN"))) Then
                            Me.invoices(0).items(numItem).deliveryNotesReferences(numAlb).deliveryNoteNumber = CStr(datosGeneralesAlbaran.Rows(numAlb)("ALBARAN"))
                        Else
                            Me.invoices(0).items(numItem).deliveryNotesReferences(numAlb).deliveryNoteNumber = ""
                        End If
                        If Not (IsDBNull(datosGeneralesAlbaran.Rows(numAlb)("FECHA")) Or IsNothing(datosGeneralesAlbaran.Rows(numAlb)("FECHA"))) Then
                            fecha = CType(datosGeneralesAlbaran.Rows(numAlb)("FECHA"), DateTime)
                            Me.invoices(0).items(numItem).deliveryNotesReferences(numAlb).deliveryNoteDate = fecha.Year & "-" & IIf(fecha.Month.ToString.Length < 2, "0" & fecha.Month.ToString, fecha.Month.ToString) & "-" & IIf(fecha.Day.ToString.Length < 2, "0" & fecha.Day.ToString, fecha.Day.ToString)
                        Else
                            Me.invoices(0).items(numItem).deliveryNotesReferences(numAlb).deliveryNoteDate = Nothing
                        End If
                        numAlb = numAlb + 1
                    End While
                End If

                If Not (IsNothing(datosImpuestosRepercutidosRetenidosFactura)) AndAlso datosImpuestosRepercutidosRetenidosFactura.Rows.Count > 0 Then
                    While contImpRetYRep < datosImpuestosRepercutidosRetenidosFactura.Rows.Count
                        If Not (IsDBNull(datosImpuestosRepercutidosRetenidosFactura.Rows(contImpRetYRep)("IMPORTE")) Or IsNothing(datosImpuestosRepercutidosRetenidosFactura.Rows(contImpRetYRep)("IMPORTE"))) Then
                            If str2Dbl(datosImpuestosRepercutidosRetenidosFactura.Rows(contImpRetYRep)("IMPORTE")) = 0 Then
                                Me.invoices(0).items(numItem).specialTaxableEvent = New SpecialTaxableEvent
                                Me.invoices(0).items(numItem).specialTaxableEvent.specialTaxableEventCode = New SpecialTaxableEventCode
                                Me.invoices(0).items(numItem).specialTaxableEvent.specialTaxableEventCode.value = SpecialTaxableEventCode.OPERACION_SUJETA_Y_EXENTA_01
                                If Not (IsDBNull(datosImpuestosRepercutidosRetenidosFactura.Rows(contImpRetYRep)("COMENT")) Or IsNothing(datosImpuestosRepercutidosRetenidosFactura.Rows(contImpRetYRep)("COMENT"))) Then
                                    Me.invoices(0).items(numItem).specialTaxableEvent.specialTaxableEventReason = CStr(datosImpuestosRepercutidosRetenidosFactura.Rows(contImpRetYRep)("COMENT"))
                                Else
                                    Me.invoices(0).items(numItem).specialTaxableEvent.specialTaxableEventReason = ""
                                End If
                                contImpRetYRep = datosImpuestosRepercutidosRetenidosFactura.Rows.Count
                            Else
                                contImpRetYRep = contImpRetYRep + 1
                            End If
                        Else
                            contImpRetYRep = contImpRetYRep + 1
                        End If
                    End While
                End If
                numItem = numItem + 1
            End While
        End If
    End Sub
    ''' <summary>
    ''' Se encarga de construir la estructura de los objetos TaxOutputType de nivel de factura 
    ''' y rellenarla con datos de la bd empleando el número de factura almacenado en el atributo 
    ''' del objeto.
    ''' </summary>
    ''' <remarks>Llamado desde: CFacturae.construirInvoices
    ''' Tiempo máximo: Depende del número de elementos que tengan los vectores de longitud variable tales como
    ''' el número de items y el número de impuestos repercutidos.
    ''' Revisado por: auv. 25/05/2012</remarks>
    Private Sub construirTaxesOutputs()
        Dim numItem As Integer = 0
        Dim numTaxes As Integer = 0

        numItem = 0
        While numItem < Me.invoices(0).items.Count
            If Not IsNothing(Me.invoices(0).items(numItem)) AndAlso Not IsNothing(Me.invoices(0).items(numItem).taxesOutputs) Then
                If IsNothing(Me.invoices(0).taxesOutputs) Then
                    Me.invoices(0).taxesOutputs = New TaxOutputType() {}
                End If
                numTaxes = 0
                While numTaxes < Me.invoices(0).items(numItem).taxesOutputs.Count
                    ReDim Preserve Me.invoices(0).taxesOutputs(0 To Me.invoices(0).taxesOutputs.Count)
                    If Not IsNothing(Me.invoices(0).items(numItem).taxesOutputs(numTaxes)) Then
                        Me.invoices(0).taxesOutputs(Me.invoices(0).taxesOutputs.Count - 1) = New TaxOutputType
                        If Not IsNothing(Me.invoices(0).items(numItem).taxesOutputs(numTaxes).taxTypeCode) Then
                            Me.invoices(0).taxesOutputs(Me.invoices(0).taxesOutputs.Count - 1).taxTypeCode = New TaxTypeCodeType
                            Me.invoices(0).taxesOutputs(Me.invoices(0).taxesOutputs.Count - 1).taxTypeCode.value = Me.invoices(0).items(numItem).taxesOutputs(numTaxes).taxTypeCode.value
                        End If
                        Me.invoices(0).taxesOutputs(Me.invoices(0).taxesOutputs.Count - 1).taxRate = New DoubleTwoDecimalType
                        If Not IsNothing(Me.invoices(0).items(numItem).taxesOutputs(numTaxes).taxRate) Then
                            Me.invoices(0).taxesOutputs(Me.invoices(0).taxesOutputs.Count - 1).taxRate.value = Me.invoices(0).items(numItem).taxesOutputs(numTaxes).taxRate.value
                        Else
                            Me.invoices(0).taxesOutputs(Me.invoices(0).taxesOutputs.Count - 1).taxRate.value = 0.0
                        End If
                        If Not IsNothing(Me.invoices(0).items(numItem).taxesOutputs(numTaxes).taxableBase) Then
                            Me.invoices(0).taxesOutputs(Me.invoices(0).taxesOutputs.Count - 1).taxableBase = New AmountType
                            If Not IsNothing(Me.invoices(0).items(numItem).taxesOutputs(numTaxes).taxableBase.totalAmount) Then
                                Me.invoices(0).taxesOutputs(Me.invoices(0).taxesOutputs.Count - 1).taxableBase.totalAmount = New DoubleTwoDecimalType
                                Me.invoices(0).taxesOutputs(Me.invoices(0).taxesOutputs.Count - 1).taxableBase.totalAmount.value = Me.invoices(0).items(numItem).taxesOutputs(numTaxes).taxableBase.totalAmount.value
                            End If
                            If Not IsNothing(Me.invoices(0).items(numItem).taxesOutputs(numTaxes).taxableBase.equivalentInEuros) Then
                                Me.invoices(0).taxesOutputs(Me.invoices(0).taxesOutputs.Count - 1).taxableBase.equivalentInEuros = New DoubleTwoDecimalType
                                Me.invoices(0).taxesOutputs(Me.invoices(0).taxesOutputs.Count - 1).taxableBase.equivalentInEuros.value = Me.invoices(0).items(numItem).taxesOutputs(numTaxes).taxableBase.equivalentInEuros.value
                            End If
                        End If
                        If Not IsNothing(Me.invoices(0).items(numItem).taxesOutputs(numTaxes).taxAmount) Then
                            Me.invoices(0).taxesOutputs(Me.invoices(0).taxesOutputs.Count - 1).taxAmount = New AmountType
                            If Not IsNothing(Me.invoices(0).items(numItem).taxesOutputs(numTaxes).taxAmount.totalAmount) Then
                                Me.invoices(0).taxesOutputs(Me.invoices(0).taxesOutputs.Count - 1).taxAmount.totalAmount = New DoubleTwoDecimalType
                                Me.invoices(0).taxesOutputs(Me.invoices(0).taxesOutputs.Count - 1).taxAmount.totalAmount.value = Me.invoices(0).items(numItem).taxesOutputs(numTaxes).taxAmount.totalAmount.value
                            End If
                            If Not IsNothing(Me.invoices(0).items(numItem).taxesOutputs(numTaxes).taxAmount.equivalentInEuros) Then
                                Me.invoices(0).taxesOutputs(Me.invoices(0).taxesOutputs.Count - 1).taxAmount.equivalentInEuros = New DoubleTwoDecimalType
                                Me.invoices(0).taxesOutputs(Me.invoices(0).taxesOutputs.Count - 1).taxAmount.equivalentInEuros.value = Me.invoices(0).items(numItem).taxesOutputs(numTaxes).taxAmount.equivalentInEuros.value
                            End If
                        End If
                        If Not IsNothing(Me.invoices(0).items(numItem).taxesOutputs(numTaxes).equivalenceSurcharge) Then
                            Me.invoices(0).taxesOutputs(Me.invoices(0).taxesOutputs.Count - 1).equivalenceSurcharge = New DoubleTwoDecimalType
                            Me.invoices(0).taxesOutputs(Me.invoices(0).taxesOutputs.Count - 1).equivalenceSurcharge.value = Me.invoices(0).items(numItem).taxesOutputs(numTaxes).equivalenceSurcharge.value
                        End If
                        If Not IsNothing(Me.invoices(0).items(numItem).taxesOutputs(numTaxes).equivalenceSurchargeAmount) Then
                            Me.invoices(0).taxesOutputs(Me.invoices(0).taxesOutputs.Count - 1).equivalenceSurchargeAmount = New AmountType
                            If Not IsNothing(Me.invoices(0).items(numItem).taxesOutputs(numTaxes).equivalenceSurchargeAmount.totalAmount) Then
                                Me.invoices(0).taxesOutputs(Me.invoices(0).taxesOutputs.Count - 1).equivalenceSurchargeAmount.totalAmount = New DoubleTwoDecimalType
                                Me.invoices(0).taxesOutputs(Me.invoices(0).taxesOutputs.Count - 1).equivalenceSurchargeAmount.totalAmount.value = Me.invoices(0).items(numItem).taxesOutputs(numTaxes).equivalenceSurchargeAmount.totalAmount.value
                            End If
                            If Not IsNothing(Me.invoices(0).items(numItem).taxesOutputs(numTaxes).equivalenceSurchargeAmount.equivalentInEuros) Then
                                Me.invoices(0).taxesOutputs(Me.invoices(0).taxesOutputs.Count - 1).equivalenceSurchargeAmount.equivalentInEuros = New DoubleTwoDecimalType
                                Me.invoices(0).taxesOutputs(Me.invoices(0).taxesOutputs.Count - 1).equivalenceSurchargeAmount.equivalentInEuros.value = Me.invoices(0).items(numItem).taxesOutputs(numTaxes).equivalenceSurchargeAmount.equivalentInEuros.value
                            End If
                        End If
                    End If
                    numTaxes = numTaxes + 1
                End While
            End If
            numItem = numItem + 1
        End While
    End Sub
    ''' <summary>
    ''' Se encarga de construir la estructura de los objetos TaxWithheldType de nivel de factura 
    ''' y rellenarla con datos de la bd empleando el número de factura almacenado en el atributo 
    ''' del objeto.
    ''' </summary>
    ''' <remarks>Llamado desde: CFacturae.construirInvoices
    ''' Tiempo máximo: Depende del número de elementos que tengan los vectores de longitud variable tales como
    ''' el número de items y el número de impuestos retenidos.
    ''' Revisado por: auv. 25/05/2012</remarks>
    Private Sub construirTaxesWithheld()
        Dim numItem As Integer = 0
        Dim numTaxes As Integer = 0

        numItem = 0
        While numItem < Me.invoices(0).items.Count
            If Not IsNothing(Me.invoices(0).items(numItem)) AndAlso Not IsNothing(Me.invoices(0).items(numItem).taxesWithheld) Then
                If IsNothing(Me.invoices(0).taxesWithheld) Then
                    Me.invoices(0).taxesWithheld = New TaxWithheldType() {}
                End If
                numTaxes = 0
                While numTaxes < Me.invoices(0).items(numItem).taxesWithheld.Count
                    ReDim Preserve Me.invoices(0).taxesWithheld(0 To Me.invoices(0).taxesWithheld.Count)
                    If Not IsNothing(Me.invoices(0).items(numItem).taxesWithheld(numTaxes)) Then
                        Me.invoices(0).taxesWithheld(Me.invoices(0).taxesWithheld.Count - 1) = New TaxWithheldType
                        If Not IsNothing(Me.invoices(0).items(numItem).taxesWithheld(numTaxes).taxTypeCode) Then
                            Me.invoices(0).taxesWithheld(Me.invoices(0).taxesWithheld.Count - 1).taxTypeCode = Me.invoices(0).items(numItem).taxesWithheld(numTaxes).taxTypeCode
                        End If
                        If Not (IsNothing(Me.invoices(0).items(numItem).taxesWithheld(numTaxes).taxRate)) Then
                            Me.invoices(0).taxesWithheld(Me.invoices(0).taxesWithheld.Count - 1).taxRate = New DoubleTwoDecimalType
                            Me.invoices(0).taxesWithheld(Me.invoices(0).taxesWithheld.Count - 1).taxRate.value = Me.invoices(0).items(numItem).taxesWithheld(numTaxes).taxRate.value
                        End If
                        If Not IsNothing(Me.invoices(0).items(numItem).taxesWithheld(numTaxes).taxableBase) Then
                            Me.invoices(0).taxesWithheld(Me.invoices(0).taxesWithheld.Count - 1).taxableBase = New AmountType
                            If Not IsNothing(Me.invoices(0).items(numItem).taxesWithheld(numTaxes).taxableBase.totalAmount) Then
                                Me.invoices(0).taxesWithheld(Me.invoices(0).taxesWithheld.Count - 1).taxableBase.totalAmount = New DoubleTwoDecimalType
                                Me.invoices(0).taxesWithheld(Me.invoices(0).taxesWithheld.Count - 1).taxableBase.totalAmount.value = Me.invoices(0).items(numItem).taxesWithheld(numTaxes).taxableBase.totalAmount.value
                            End If
                            If Not IsNothing(Me.invoices(0).items(numItem).taxesWithheld(numTaxes).taxableBase.equivalentInEuros) Then
                                Me.invoices(0).taxesWithheld(Me.invoices(0).taxesWithheld.Count - 1).taxableBase.equivalentInEuros = New DoubleTwoDecimalType
                                Me.invoices(0).taxesWithheld(Me.invoices(0).taxesWithheld.Count - 1).taxableBase.equivalentInEuros.value = Me.invoices(0).items(numItem).taxesWithheld(numTaxes).taxableBase.equivalentInEuros.value
                            End If
                        End If
                        If Not IsNothing(Me.invoices(0).items(numItem).taxesWithheld(numTaxes).taxAmount) Then
                            Me.invoices(0).taxesWithheld(Me.invoices(0).taxesWithheld.Count - 1).taxAmount = New AmountType
                            If Not IsNothing(Me.invoices(0).items(numItem).taxesWithheld(numTaxes).taxAmount.totalAmount) Then
                                Me.invoices(0).taxesWithheld(Me.invoices(0).taxesWithheld.Count - 1).taxAmount.totalAmount = New DoubleTwoDecimalType
                                Me.invoices(0).taxesWithheld(Me.invoices(0).taxesWithheld.Count - 1).taxAmount.totalAmount.value = Me.invoices(0).items(numItem).taxesWithheld(numTaxes).taxAmount.totalAmount.value
                            End If
                            If Not IsNothing(Me.invoices(0).items(numItem).taxesWithheld(numTaxes).taxAmount.equivalentInEuros) Then
                                Me.invoices(0).taxesWithheld(Me.invoices(0).taxesWithheld.Count - 1).taxAmount.equivalentInEuros = New DoubleTwoDecimalType
                                Me.invoices(0).taxesWithheld(Me.invoices(0).taxesWithheld.Count - 1).taxAmount.equivalentInEuros.value = Me.invoices(0).items(numItem).taxesWithheld(numTaxes).taxAmount.equivalentInEuros.value
                            End If
                        End If
                    End If
                    numTaxes = numTaxes + 1
                End While
            End If
            numItem = numItem + 1
        End While
    End Sub
    ''' <summary>
    ''' Se encarga de construir la estructura del objeto InvoiceTotals y rellenarla con datos de la bd
    ''' empleando el número de factura almacenado en el atributo del objeto.
    ''' </summary>
    ''' <remarks>Llamado desde: CFacturae.construirInvoices
    ''' Tiempo máximo: Depende del número de elementos que tengan los vectores de longitud variable tales como
    ''' el número de items, descuentos, costes, impuestos repercutidos, impuestos retenidos, etc...
    ''' Revisado por: auv. 25/05/2012</remarks>
    Private Sub construirInvoiceTotals()
        Dim numLin As Integer = 0
        Dim numTaxOut As Integer = 0
        Dim numTaxWit As Integer = 0
        Dim contCargosYDescuentos As Integer = 0
        Dim contDescuentos As Integer = 0
        Dim contCargos As Integer = 0

        Me.invoices(0).invoiceTotals = New InvoiceTotalsType

        numLin = 0
        Me.invoices(0).invoiceTotals.totalGrossAmount = New DoubleTwoDecimalType
        Me.invoices(0).invoiceTotals.totalGrossAmount.value = 0.0
        While numLin < Me.invoices(0).items.Count
            If Not IsNothing(Me.invoices(0).items(numLin)) AndAlso Not IsNothing(Me.invoices(0).items(numLin).grossAmount) Then
                Me.invoices(0).invoiceTotals.totalGrossAmount.value = Me.invoices(0).invoiceTotals.totalGrossAmount.value + Me.invoices(0).items(numLin).grossAmount.value
            End If
            numLin = numLin + 1
        End While

        contDescuentos = 0
        If Not IsNothing(datosCostesDescuentosFactura) AndAlso datosCostesDescuentosFactura.Rows.Count > 0 Then
            contCargosYDescuentos = 0
            While contCargosYDescuentos < datosCostesDescuentosFactura.Rows.Count
                If Not (IsDBNull(datosCostesDescuentosFactura.Rows(contCargosYDescuentos)("TIPO")) Or IsNothing(datosCostesDescuentosFactura.Rows(contCargosYDescuentos)("TIPO"))) Then
                    If CStr(datosCostesDescuentosFactura.Rows(contCargosYDescuentos)("TIPO")) = 1 Then
                        If IsNothing(Me.invoices(0).invoiceTotals.generalDiscounts) Then
                            Me.invoices(0).invoiceTotals.generalDiscounts = New DiscountType() {}
                        End If
                        ReDim Preserve Me.invoices(0).invoiceTotals.generalDiscounts(0 To contDescuentos)
                        Me.invoices(0).invoiceTotals.generalDiscounts(contDescuentos) = New DiscountType
                        If Not (IsDBNull(datosCostesDescuentosFactura.Rows(contCargosYDescuentos)("DEN")) Or IsNothing(datosCostesDescuentosFactura.Rows(contCargosYDescuentos)("DEN"))) Then
                            Me.invoices(0).invoiceTotals.generalDiscounts(contDescuentos).discountReason = CStr(datosCostesDescuentosFactura.Rows(contCargosYDescuentos)("DEN"))
                        Else
                            Me.invoices(0).invoiceTotals.generalDiscounts(contDescuentos).discountReason = ""
                        End If
                        Me.invoices(0).invoiceTotals.generalDiscounts(contDescuentos).discountAmount = New DoubleSixDecimalType
                        If Not (IsDBNull(datosCostesDescuentosFactura.Rows(contCargosYDescuentos)("IMPORTE")) Or IsNothing(datosCostesDescuentosFactura.Rows(contCargosYDescuentos)("IMPORTE"))) Then
                            Me.invoices(0).invoiceTotals.generalDiscounts(contDescuentos).discountAmount.value = datosCostesDescuentosFactura.Rows(contCargosYDescuentos)("IMPORTE")
                        Else
                            Me.invoices(0).invoiceTotals.generalDiscounts(contDescuentos).discountAmount.value = 0.0
                        End If
                        If Not (IsDBNull(datosCostesDescuentosFactura.Rows(contCargosYDescuentos)("OPERACION")) Or IsNothing(datosCostesDescuentosFactura.Rows(contCargosYDescuentos)("OPERACION"))) Then
                            Select Case CStr(datosCostesDescuentosFactura.Rows(contCargosYDescuentos)("OPERACION"))
                                Case "-"
                                Case "-%"
                                    Me.invoices(0).invoiceTotals.generalDiscounts(contDescuentos).discountRate = New DoubleFourDecimalType
                                    If Not (IsDBNull(datosCostesDescuentosFactura.Rows(contCargosYDescuentos)("VALOR")) Or IsNothing(datosCostesDescuentosFactura.Rows(contCargosYDescuentos)("VALOR"))) Then
                                        Me.invoices(0).invoiceTotals.generalDiscounts(contDescuentos).discountRate.value = datosCostesDescuentosFactura.Rows(contCargosYDescuentos)("VALOR")
                                    Else
                                        Me.invoices(0).invoiceTotals.generalDiscounts(contDescuentos).discountRate.value = 0.0
                                    End If
                            End Select
                        End If
                        contDescuentos = contDescuentos + 1
                    End If
                End If
                contCargosYDescuentos = contCargosYDescuentos + 1
            End While
        End If

        contCargos = 0
        If Not (IsNothing(datosCostesDescuentosFactura)) And datosCostesDescuentosFactura.Rows.Count > 0 Then
            contCargosYDescuentos = 0
            While contCargosYDescuentos < datosCostesDescuentosFactura.Rows.Count
                If Not (IsDBNull(datosCostesDescuentosFactura.Rows(contCargosYDescuentos)("TIPO")) Or IsNothing(datosCostesDescuentosFactura.Rows(contCargosYDescuentos)("TIPO"))) Then
                    If CStr(datosCostesDescuentosFactura.Rows(contCargosYDescuentos)("TIPO")) = 0 Then
                        If IsNothing(Me.invoices(0).invoiceTotals.generalSurcharges) Then
                            Me.invoices(0).invoiceTotals.generalSurcharges = New ChargeType() {}
                        End If
                        ReDim Preserve Me.invoices(0).invoiceTotals.generalSurcharges(0 To contCargos)
                        Me.invoices(0).invoiceTotals.generalSurcharges(contCargos) = New ChargeType
                        If Not (IsDBNull(datosCostesDescuentosFactura.Rows(contCargosYDescuentos)("DEN")) Or IsNothing(datosCostesDescuentosFactura.Rows(contCargosYDescuentos)("DEN"))) Then
                            Me.invoices(0).invoiceTotals.generalSurcharges(contCargos).chargeReason = CStr(datosCostesDescuentosFactura.Rows(contCargosYDescuentos)("DEN"))
                        Else
                            Me.invoices(0).invoiceTotals.generalSurcharges(contCargos).chargeReason = ""
                        End If
                        Me.invoices(0).invoiceTotals.generalSurcharges(contCargos).chargeAmount = New DoubleSixDecimalType
                        If Not (IsDBNull(datosCostesDescuentosFactura.Rows(contCargosYDescuentos)("IMPORTE")) Or IsNothing(datosCostesDescuentosFactura.Rows(contCargosYDescuentos)("IMPORTE"))) Then
                            Me.invoices(0).invoiceTotals.generalSurcharges(contCargos).chargeAmount.value = datosCostesDescuentosFactura.Rows(contCargosYDescuentos)("IMPORTE")
                        Else
                            Me.invoices(0).invoiceTotals.generalSurcharges(contCargos).chargeAmount.value = 0.0
                        End If
                        If Not (IsDBNull(datosCostesDescuentosFactura.Rows(contCargosYDescuentos)("OPERACION")) Or IsNothing(datosCostesDescuentosFactura.Rows(contCargosYDescuentos)("OPERACION"))) Then
                            Select Case CStr(datosCostesDescuentosFactura.Rows(contCargosYDescuentos)("OPERACION"))
                                Case "+"
                                Case "+%"
                                    Me.invoices(0).invoiceTotals.generalSurcharges(contCargos).chargeRate = New DoubleFourDecimalType
                                    If Not (IsDBNull(datosCostesDescuentosFactura.Rows(contCargosYDescuentos)("VALOR")) Or IsNothing(datosCostesDescuentosFactura.Rows(contCargosYDescuentos)("VALOR"))) Then
                                        Me.invoices(0).invoiceTotals.generalSurcharges(contCargos).chargeRate.value = datosCostesDescuentosFactura.Rows(contCargosYDescuentos)("VALOR")
                                    Else
                                        Me.invoices(0).invoiceTotals.generalSurcharges(contCargos).chargeRate.value = 0.0
                                    End If
                            End Select
                        End If
                        contCargos = contCargos + 1
                    End If
                End If
                contCargosYDescuentos = contCargosYDescuentos + 1
            End While
        End If

        Me.invoices(0).invoiceTotals.totalGeneralDiscounts = New DoubleTwoDecimalType
        Me.invoices(0).invoiceTotals.totalGeneralDiscounts.value = 0.0
        If Not IsNothing(Me.invoices(0).invoiceTotals.generalDiscounts) Then
            contDescuentos = 0
            While contDescuentos < Me.invoices(0).invoiceTotals.generalDiscounts.Length
                Me.invoices(0).invoiceTotals.totalGeneralDiscounts.value = Me.invoices(0).invoiceTotals.totalGeneralDiscounts.value + Me.invoices(0).invoiceTotals.generalDiscounts(contDescuentos).discountAmount.value
                contDescuentos = contDescuentos + 1
            End While
        End If

        Me.invoices(0).invoiceTotals.totalGeneralSurcharges = New DoubleTwoDecimalType
        Me.invoices(0).invoiceTotals.totalGeneralSurcharges.value = 0.0
        If Not IsNothing(Me.invoices(0).invoiceTotals.generalSurcharges) Then
            contCargos = 0
            While contCargos < Me.invoices(0).invoiceTotals.generalSurcharges.Length
                Me.invoices(0).invoiceTotals.totalGeneralSurcharges.value = Me.invoices(0).invoiceTotals.totalGeneralSurcharges.value + Me.invoices(0).invoiceTotals.generalSurcharges(contCargos).chargeAmount.value
                contCargos = contCargos + 1
            End While
        End If

        Me.invoices(0).invoiceTotals.totalGrossAmountBeforeTaxes = New DoubleTwoDecimalType
        Me.invoices(0).invoiceTotals.totalGrossAmountBeforeTaxes = Me.invoices(0).invoiceTotals.totalGrossAmount.value
        If Not IsNothing(Me.invoices(0).invoiceTotals.totalGeneralDiscounts) Then
            Me.invoices(0).invoiceTotals.totalGrossAmountBeforeTaxes.value = Me.invoices(0).invoiceTotals.totalGrossAmountBeforeTaxes.value - Me.invoices(0).invoiceTotals.totalGeneralDiscounts.value
        End If
        If Not IsNothing(Me.invoices(0).invoiceTotals.totalGeneralSurcharges) Then
            Me.invoices(0).invoiceTotals.totalGrossAmountBeforeTaxes.value = Me.invoices(0).invoiceTotals.totalGrossAmountBeforeTaxes.value + Me.invoices(0).invoiceTotals.totalGeneralSurcharges.value
        End If

        Me.invoices(0).invoiceTotals.totalTaxOutputs = New DoubleTwoDecimalType
        Me.invoices(0).invoiceTotals.totalTaxOutputs.value = 0.0
        If Not IsNothing(Me.invoices(0).invoiceTotals.totalGeneralDiscounts) OrElse Not IsNothing(Me.invoices(0).invoiceTotals.totalGeneralSurcharges) Then
            If Not IsDBNull(datosGeneralesFactura.Rows(0)("TOT_IMPUESTOS")) Then
                Me.invoices(0).invoiceTotals.totalTaxOutputs.value = datosGeneralesFactura.Rows(0)("TOT_IMPUESTOS")
            End If
        Else
            numTaxOut = 0
            If Not IsNothing(Me.invoices(0).taxesOutputs) Then
                While numTaxOut < Me.invoices(0).taxesOutputs.Count
                    If Not IsNothing(Me.invoices(0).taxesOutputs(numTaxOut)) AndAlso Not IsNothing(Me.invoices(0).taxesOutputs(numTaxOut).taxAmount) AndAlso Not IsNothing(Me.invoices(0).taxesOutputs(numTaxOut).taxAmount.totalAmount) Then
                        Me.invoices(0).invoiceTotals.totalTaxOutputs.value = Me.invoices(0).invoiceTotals.totalTaxOutputs.value + Me.invoices(0).taxesOutputs(numTaxOut).taxAmount.totalAmount.value
                    End If
                    If Not IsNothing(Me.invoices(0).taxesOutputs(numTaxOut)) AndAlso Not IsNothing(Me.invoices(0).taxesOutputs(numTaxOut).equivalenceSurchargeAmount) AndAlso Not IsNothing(Me.invoices(0).taxesOutputs(numTaxOut).equivalenceSurchargeAmount.totalAmount) Then
                        Me.invoices(0).invoiceTotals.totalTaxOutputs.value = Me.invoices(0).invoiceTotals.totalTaxOutputs.value + Me.invoices(0).taxesOutputs(numTaxOut).equivalenceSurchargeAmount.totalAmount.value
                    End If
                    numTaxOut = numTaxOut + 1
                End While
            End If
        End If

        numTaxWit = 0
        Me.invoices(0).invoiceTotals.totalTaxesWithheld = New DoubleTwoDecimalType
        Me.invoices(0).invoiceTotals.totalTaxesWithheld.value = 0.0
        If Not IsNothing(Me.invoices(0).taxesWithheld) Then
            While numTaxWit < Me.invoices(0).taxesWithheld.Count
                If Not IsNothing(Me.invoices(0).taxesWithheld(numTaxWit)) AndAlso Not IsNothing(Me.invoices(0).taxesWithheld(numTaxWit).taxAmount) AndAlso Not IsNothing(Me.invoices(0).taxesWithheld(numTaxWit).taxAmount.totalAmount) Then
                    Me.invoices(0).invoiceTotals.totalTaxesWithheld.value = Me.invoices(0).invoiceTotals.totalTaxesWithheld.value + Me.invoices(0).taxesWithheld(numTaxWit).taxAmount.totalAmount.value
                End If
                numTaxWit = numTaxWit + 1
            End While
        End If

        Me.invoices(0).invoiceTotals.invoiceTotal = New DoubleTwoDecimalType
        Me.invoices(0).invoiceTotals.invoiceTotal.value = Me.invoices(0).invoiceTotals.totalGrossAmountBeforeTaxes.value + Me.invoices(0).invoiceTotals.totalTaxOutputs.value - Me.invoices(0).invoiceTotals.totalTaxesWithheld.value

        Me.invoices(0).invoiceTotals.totalOutstandingAmount = New DoubleTwoDecimalType
        Me.invoices(0).invoiceTotals.totalOutstandingAmount.value = Me.invoices(0).invoiceTotals.invoiceTotal.value

        Me.invoices(0).invoiceTotals.amountsWithheld = New AmountsWithheldType
        Me.invoices(0).invoiceTotals.amountsWithheld.withholdingReason = "" : If Me.invoices(0).invoiceTotals.amountsWithheld.withholdingReason.Length > 2500 Then Me.invoices(0).invoiceTotals.amountsWithheld.withholdingReason = Me.invoices(0).invoiceTotals.amountsWithheld.withholdingReason.Substring(0, 2500)
        Me.invoices(0).invoiceTotals.amountsWithheld.withholdingAmount = New DoubleTwoDecimalType
        If Not IsNothing(datosGeneralesFactura) AndAlso datosGeneralesFactura.Rows.Count > 0 Then
            If Not (IsDBNull(datosGeneralesFactura.Rows(0)("RET_GARANTIA")) Or IsNothing(datosGeneralesFactura.Rows(0)("RET_GARANTIA"))) Then
                Me.invoices(0).invoiceTotals.amountsWithheld.withholdingAmount.value = datosGeneralesFactura.Rows(0)("RET_GARANTIA")
            Else
                Me.invoices(0).invoiceTotals.amountsWithheld.withholdingAmount.value = 0.0
            End If
        End If

        Me.invoices(0).invoiceTotals.totalExecutableAmount = New DoubleTwoDecimalType
        Me.invoices(0).invoiceTotals.totalExecutableAmount.value = Me.invoices(0).invoiceTotals.totalOutstandingAmount.value
        If Not IsNothing(Me.invoices(0).invoiceTotals.amountsWithheld.withholdingAmount) Then
            Me.invoices(0).invoiceTotals.totalExecutableAmount.value = Me.invoices(0).invoiceTotals.totalExecutableAmount.value - Me.invoices(0).invoiceTotals.amountsWithheld.withholdingAmount.value
        End If
    End Sub
    ''' <summary>
    ''' Se encarga de construir la estructura del objeto InvoiceAdditionalData y rellenarla con datos de la bd
    ''' empleando el número de factura almacenado en el atributo del objeto.
    ''' </summary>
    ''' <remarks>Llamado desde: CFacturae.construirInvoices
    ''' Tiempo máximo: menor a 0,1.
    ''' Revisado por: auv. 05/06/2012</remarks>
    Private Sub construirInvoiceAdditionalData()
        Dim contImp As Integer
        Dim contImpExent As Integer
        Dim texto As String

        texto = ""
        contImp = 0
        contImpExent = 0
        If Not IsNothing(datosImpuestosRepercutidosRetenidosFactura) AndAlso datosImpuestosRepercutidosRetenidosFactura.Rows.Count > 0 Then
            While contImp < datosImpuestosRepercutidosRetenidosFactura.Rows.Count
                If Not (IsDBNull(datosImpuestosRepercutidosRetenidosFactura.Rows(contImp)("IMPORTE")) Or IsNothing(datosImpuestosRepercutidosRetenidosFactura.Rows(contImp)("IMPORTE"))) Then
                    If str2Dbl(datosImpuestosRepercutidosRetenidosFactura.Rows(contImp)("IMPORTE")) = 0 Then
                        If Not (IsDBNull(datosImpuestosRepercutidosRetenidosFactura.Rows(contImp)("COMENT")) Or IsNothing(datosImpuestosRepercutidosRetenidosFactura.Rows(contImp)("COMENT"))) Then
                            texto = texto & CStr(datosImpuestosRepercutidosRetenidosFactura.Rows(contImp)("COMENT"))
                            contImpExent = contImpExent + 1
                        End If
                    End If
                End If
                contImp = contImp + 1
            End While
        End If

        If contImp > 0 Then
            Me.invoices(0).additionalData = New AdditionalDataType
            Me.invoices(0).additionalData.invoiceAdditionalInformation = texto
        End If
    End Sub
#End Region
#Region "OBJ2XML"
    ''' <summary>
    ''' Imprime un objeto FacturaeType en una estructura de nodos XML siguiendo el estándar Facturae v3.2
    ''' </summary>
    ''' <returns>Devuelve un objeto XMLDocument que contiene la representación en XML estándar Facturae v3.2 el
    ''' objeto CFacturae</returns>
    ''' <remarks>Llamado desde: CFacturae.crearFacV3_2
    ''' Tiempo máximo: Depende del número de elementos que tengan los vectores de longitud variable tales como
    ''' el número de items, descuentos, costes, impuestos repercutidos, impuestos retenidos, etc...
    ''' Revisado por: auv. 25/05/2012</remarks>
    Private Function facturaeObj2XML() As XmlDocument
        Dim doc As XmlDocument = Nothing
        Dim nodoFileHeader As XmlNode = Nothing
        Dim nodoParties As XmlNode = Nothing
        Dim nodoInvoices As XmlNode = Nothing

        doc = New XmlDocument

        Dim esqueletoXML As String = Nothing
        esqueletoXML = "<?xml version=""1.0""?>"
        esqueletoXML = esqueletoXML & "<Facturae xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns=""http://www.facturae.es/Facturae/2009/v3.2/Facturae"">"
        esqueletoXML = esqueletoXML & "<FileHeader xmlns=""""></FileHeader>"
        esqueletoXML = esqueletoXML & "<Parties xmlns=""""></Parties>"
        esqueletoXML = esqueletoXML & "<Invoices xmlns=""""></Invoices>"
        esqueletoXML = esqueletoXML & "</Facturae>"
        doc.LoadXml(esqueletoXML)
        If Not IsNothing(doc) AndAlso Not IsNothing(doc.ChildNodes(1)) AndAlso Not IsNothing(doc.ChildNodes(1).ChildNodes(0)) Then
            nodoFileHeader = doc.ChildNodes(1).ChildNodes(0)
        End If

        If Not IsNothing(doc) AndAlso Not IsNothing(doc.ChildNodes(1)) AndAlso Not IsNothing(doc.ChildNodes(1).ChildNodes(1)) Then
            nodoParties = doc.ChildNodes(1).ChildNodes(1)
        End If

        If Not IsNothing(doc) AndAlso Not IsNothing(doc.ChildNodes(1)) AndAlso Not IsNothing(doc.ChildNodes(1).ChildNodes(2)) Then
            nodoInvoices = doc.ChildNodes(1).ChildNodes(2)
        End If

        If Not IsNothing(Me.fileHeader) Then
            Me.fileHeaderObj2XML(Me.fileHeader, nodoFileHeader, doc)
        End If
        If Not IsNothing(Me.parties) Then
            Me.partiesObj2XML(Me.parties, nodoParties, doc)
        End If
        If Not IsNothing(Me.invoices) Then
            Me.invoicesObj2XML(Me.invoices, nodoInvoices, doc)
        End If

        Return doc
    End Function
    ''' <summary>
    '''  Imprime un objeto FileHeaderType en una estructura de nodos XML siguiendo el estándar Facturae v3.2
    ''' </summary>
    ''' <param name="objFileHeader">Objeto a imprimir</param>
    ''' <param name="nodoFileHeader">Nodo raíz sobre el cual se imprime el objeto</param>
    ''' <param name="doc">Objeto de un documento XML el cual contiene una Facturae v3.2</param>
    ''' <remarks>Llamado desde: CFacturae.facturaeObj2XML
    ''' Tiempo máximo: menor a 0,1
    ''' Revisado por: auv. 25/05/2012</remarks>
    Private Sub fileHeaderObj2XML(ByRef objFileHeader As FileHeaderType, ByRef nodoFileHeader As XmlNode, ByRef doc As XmlDocument)
        Dim nodoHijo As XmlNode = Nothing

        If Not IsNothing(objFileHeader.schemaVersion) Then
            nodoHijo = doc.CreateNode(XmlNodeType.Element, "SchemaVersion", "")
            nodoHijo.InnerText = objFileHeader.schemaVersion.value
            nodoFileHeader.AppendChild(nodoHijo)
        End If
        If Not IsNothing(objFileHeader.modality) Then
            nodoHijo = doc.CreateNode(XmlNodeType.Element, "Modality", "")
            nodoHijo.InnerText = objFileHeader.modality.value
            nodoFileHeader.AppendChild(nodoHijo)
        End If
        If Not IsNothing(objFileHeader.invoiceIssuerType) Then
            nodoHijo = doc.CreateNode(XmlNodeType.Element, "InvoiceIssuerType", "")
            nodoHijo.InnerText = objFileHeader.invoiceIssuerType.value
            nodoFileHeader.AppendChild(nodoHijo)
        End If
        If Not IsNothing(objFileHeader.batch) Then
            nodoHijo = doc.CreateNode(XmlNodeType.Element, "Batch", "")
            nodoFileHeader.AppendChild(nodoHijo)
            Me.batchObj2XML(objFileHeader.batch, nodoHijo, doc)
        End If
    End Sub
    ''' <summary>
    ''' Imprime un objeto BatchType en una estructura de nodos XML siguiendo el estándar Facturae v3.2
    ''' </summary>
    ''' <param name="objBatch">Objeto a imprimir</param>
    ''' <param name="nodoBatch">Nodo raíz sobre el cual se imprime el objeto</param>
    ''' <param name="doc">Objeto de un documento XML el cual contiene una Facturae v3.2</param>
    ''' <remarks>Llamado desde: CFacturae.fileHeaderObj2XML
    ''' Tiempo máximo: menor a 0,1
    ''' Revisado por: auv. 25/05/2012</remarks>
    Private Sub batchObj2XML(ByRef objBatch As BatchType, ByRef nodoBatch As XmlNode, ByRef doc As XmlDocument)
        Dim nodoHijo As XmlNode = Nothing
        Dim nodoNieto As XmlNode = Nothing

        If Not IsNothing(objBatch.batchIdentifier) Then
            nodoHijo = doc.CreateNode(XmlNodeType.Element, "BatchIdentifier", "")
            nodoHijo.InnerText = objBatch.batchIdentifier
            nodoBatch.AppendChild(nodoHijo)
        End If

        If Not IsNothing(objBatch.invoicesCount) Then
            nodoHijo = doc.CreateNode(XmlNodeType.Element, "InvoicesCount", "")
            nodoHijo.InnerText = objBatch.invoicesCount
            nodoBatch.AppendChild(nodoHijo)
        End If

        If Not IsNothing(objBatch.totalInvoicesAmount) Then
            nodoHijo = doc.CreateNode(XmlNodeType.Element, "TotalInvoicesAmount", "")
            nodoBatch.AppendChild(nodoHijo)
            If Not IsNothing(objBatch.totalInvoicesAmount.totalAmount) Then
                nodoNieto = doc.CreateNode(XmlNodeType.Element, "TotalAmount", "")
                nodoNieto.InnerText = rellenarDecimales(redondearHaciaAbajo(dbl2Str(objBatch.totalInvoicesAmount.totalAmount.value, "."), 2), 2, ".")
                nodoHijo.AppendChild(nodoNieto)
            End If
            If Not (IsNothing(objBatch.totalInvoicesAmount.equivalentInEuros)) Then
                nodoNieto = doc.CreateNode(XmlNodeType.Element, "EquivalentInEuros", "")
                nodoNieto.InnerText = rellenarDecimales(redondearHaciaAbajo(dbl2Str(objBatch.totalInvoicesAmount.equivalentInEuros.value, "."), 2), 2, ".")
                nodoHijo.AppendChild(nodoNieto)
            End If
        End If

        If Not IsNothing(objBatch.totalOutstandingAmount) Then
            nodoHijo = doc.CreateNode(XmlNodeType.Element, "TotalOutstandingAmount", "")
            nodoBatch.AppendChild(nodoHijo)
            If Not IsNothing(objBatch.totalOutstandingAmount.totalAmount) Then
                nodoNieto = doc.CreateNode(XmlNodeType.Element, "TotalAmount", "")
                nodoNieto.InnerText = rellenarDecimales(redondearHaciaAbajo(dbl2Str(objBatch.totalOutstandingAmount.totalAmount.value, "."), 2), 2, ".")
                nodoHijo.AppendChild(nodoNieto)
            End If
            If Not (IsNothing(objBatch.totalOutstandingAmount.equivalentInEuros)) Then
                nodoNieto = doc.CreateNode(XmlNodeType.Element, "EquivalentInEuros", "")
                nodoNieto.InnerText = rellenarDecimales(redondearHaciaAbajo(dbl2Str(objBatch.totalOutstandingAmount.equivalentInEuros.value, "."), 2), 2, ".")
                nodoHijo.AppendChild(nodoNieto)
            End If
        End If

        If Not IsNothing(objBatch.totalExecutableAmount) Then
            nodoHijo = doc.CreateNode(XmlNodeType.Element, "TotalExecutableAmount", "")
            nodoBatch.AppendChild(nodoHijo)
            If Not IsNothing(objBatch.totalExecutableAmount.totalAmount) Then
                nodoNieto = doc.CreateNode(XmlNodeType.Element, "TotalAmount", "")
                nodoNieto.InnerText = rellenarDecimales(redondearHaciaAbajo(dbl2Str(objBatch.totalExecutableAmount.totalAmount.value, "."), 2), 2, ".")
                nodoHijo.AppendChild(nodoNieto)
            End If
            If Not (IsNothing(objBatch.totalExecutableAmount.equivalentInEuros)) Then
                nodoNieto = doc.CreateNode(XmlNodeType.Element, "EquivalentInEuros", "")
                nodoNieto.InnerText = rellenarDecimales(redondearHaciaAbajo(dbl2Str(objBatch.totalExecutableAmount.equivalentInEuros.value, "."), 2), 2, ".")
                nodoHijo.AppendChild(nodoNieto)
            End If
        End If

        If Not IsNothing(objBatch.invoiceCurrencyCode) Then
            nodoHijo = doc.CreateNode(XmlNodeType.Element, "InvoiceCurrencyCode", "")
            nodoHijo.InnerText = objBatch.invoiceCurrencyCode.value
            nodoBatch.AppendChild(nodoHijo)
        End If
    End Sub
    ''' <summary>
    ''' Imprime un objeto PartiesType en una estructura de nodos XML siguiendo el estándar Facturae v3.2
    ''' </summary>
    ''' <param name="objParties">Objeto a imprimir</param>
    ''' <param name="nodoParties">Nodo raíz sobre el cual se imprime el objeto</param>
    ''' <param name="doc">Objeto de un documento XML el cual contiene una Facturae v3.2</param>
    ''' <remarks>Llamado desde: CFacturae.facturaeObj2XML
    ''' Tiempo máximo: menor a 0,1
    ''' Revisado por: auv. 25/05/2012</remarks>
    Private Sub partiesObj2XML(ByRef objParties As PartiesType, ByRef nodoParties As XmlNode, ByRef doc As XmlDocument)
        Dim nodoHijo As XmlNode = Nothing

        If Not IsNothing(objParties.sellerParty) Then
            nodoHijo = doc.CreateNode(XmlNodeType.Element, "SellerParty", "")
            nodoParties.AppendChild(nodoHijo)
            Me.sellerPartyObj2XML(objParties.sellerParty, nodoHijo, doc)
        End If
        If Not IsNothing(objParties.buyerParty) Then
            nodoHijo = doc.CreateNode(XmlNodeType.Element, "BuyerParty", "")
            nodoParties.AppendChild(nodoHijo)
            Me.buyerPartyObj2XML(objParties.buyerParty, nodoHijo, doc)
        End If
    End Sub
    ''' <summary>
    ''' Imprime un objeto SellerPartyType en una estructura de nodos XML siguiendo el estándar Facturae v3.2
    ''' </summary>
    ''' <param name="objSellerParty">Objeto a imprimir</param>
    ''' <param name="nodoSellerParty">Nodo raíz sobre el cual se imprime el objeto</param>
    ''' <param name="doc">Objeto de un documento XML el cual contiene una Facturae v3.2</param>
    ''' <remarks>Llamado desde: CFacturae.partiesObj2XML
    ''' Tiempo máximo: menor a 0,1
    ''' Revisado por: auv. 25/05/2012</remarks>
    Private Sub sellerPartyObj2XML(ByRef objSellerParty As BusinessType, ByRef nodoSellerParty As XmlNode, ByRef doc As XmlDocument)
        Dim nodoHijo As XmlNode = Nothing
        Dim nodoNieto As XmlNode = Nothing
        Dim nodoLegalEntity As XmlNode = Nothing

        If Not IsNothing(objSellerParty.taxIdentification) Then
            nodoHijo = doc.CreateNode(XmlNodeType.Element, "TaxIdentification", "")
            nodoSellerParty.AppendChild(nodoHijo)
            If Not IsNothing(objSellerParty.taxIdentification.personTypeCode) Then
                nodoNieto = doc.CreateNode(XmlNodeType.Element, "PersonTypeCode", "")
                nodoNieto.InnerText = objSellerParty.taxIdentification.personTypeCode.value
                nodoHijo.AppendChild(nodoNieto)
            End If
            If Not IsNothing(objSellerParty.taxIdentification.residenceTypeCode) Then
                nodoNieto = doc.CreateNode(XmlNodeType.Element, "ResidenceTypeCode", "")
                nodoNieto.InnerText = objSellerParty.taxIdentification.residenceTypeCode.value
                nodoHijo.AppendChild(nodoNieto)
            End If
            If Not IsNothing(objSellerParty.taxIdentification.taxIdentificationNumber) Then
                nodoNieto = doc.CreateNode(XmlNodeType.Element, "TaxIdentificationNumber", "")
                nodoNieto.InnerText = objSellerParty.taxIdentification.taxIdentificationNumber
                nodoHijo.AppendChild(nodoNieto)
            End If
        End If

        If Not IsNothing(objSellerParty.legalEntity) Then
            nodoHijo = doc.CreateNode(XmlNodeType.Element, "LegalEntity", "")
            nodoSellerParty.AppendChild(nodoHijo)
            nodoLegalEntity = nodoHijo
            If Not IsNothing(objSellerParty.legalEntity.corporateName) Then
                nodoHijo = doc.CreateNode(XmlNodeType.Element, "CorporateName", "")
                nodoHijo.InnerText = objSellerParty.legalEntity.corporateName
                nodoLegalEntity.AppendChild(nodoHijo)
            End If
            If Not (IsNothing(objSellerParty.legalEntity.addressInSpain)) Then
                nodoHijo = doc.CreateNode(XmlNodeType.Element, "AddressInSpain", "")
                nodoLegalEntity.AppendChild(nodoHijo)
                If Not IsNothing(objSellerParty.legalEntity.addressInSpain.address) Then
                    nodoNieto = doc.CreateNode(XmlNodeType.Element, "Address", "")
                    nodoNieto.InnerText = objSellerParty.legalEntity.addressInSpain.address
                    nodoHijo.AppendChild(nodoNieto)
                End If
                If Not IsNothing(objSellerParty.legalEntity.addressInSpain.postCode) Then
                    nodoNieto = doc.CreateNode(XmlNodeType.Element, "PostCode", "")
                    nodoNieto.InnerText = objSellerParty.legalEntity.addressInSpain.postCode.value
                    nodoHijo.AppendChild(nodoNieto)
                End If
                If Not IsNothing(objSellerParty.legalEntity.addressInSpain.town) Then
                    nodoNieto = doc.CreateNode(XmlNodeType.Element, "Town", "")
                    nodoNieto.InnerText = objSellerParty.legalEntity.addressInSpain.town
                    nodoHijo.AppendChild(nodoNieto)
                End If
                If Not IsNothing(objSellerParty.legalEntity.addressInSpain.province) Then
                    nodoNieto = doc.CreateNode(XmlNodeType.Element, "Province", "")
                    nodoNieto.InnerText = objSellerParty.legalEntity.addressInSpain.province
                    nodoHijo.AppendChild(nodoNieto)
                End If
                If Not IsNothing(objSellerParty.legalEntity.addressInSpain.countryCode) Then
                    nodoNieto = doc.CreateNode(XmlNodeType.Element, "CountryCode", "")
                    nodoNieto.InnerText = objSellerParty.legalEntity.addressInSpain.countryCode.value
                    nodoHijo.AppendChild(nodoNieto)
                End If
            End If
            If Not (IsNothing(objSellerParty.legalEntity.overseasAddress)) Then
                nodoHijo = doc.CreateNode(XmlNodeType.Element, "OverseasAddress", "")
                nodoLegalEntity.AppendChild(nodoHijo)
                If Not IsNothing(objSellerParty.legalEntity.overseasAddress.address) Then
                    nodoNieto = doc.CreateNode(XmlNodeType.Element, "Address", "")
                    nodoNieto.InnerText = objSellerParty.legalEntity.overseasAddress.address
                    nodoHijo.AppendChild(nodoNieto)
                End If
                If Not IsNothing(objSellerParty.legalEntity.overseasAddress.postCodeAndTown) Then
                    nodoNieto = doc.CreateNode(XmlNodeType.Element, "PostCodeAndTown", "")
                    nodoNieto.InnerText = objSellerParty.legalEntity.overseasAddress.postCodeAndTown
                    nodoHijo.AppendChild(nodoNieto)
                End If
                If Not IsNothing(objSellerParty.legalEntity.overseasAddress.province) Then
                    nodoNieto = doc.CreateNode(XmlNodeType.Element, "Province", "")
                    nodoNieto.InnerText = objSellerParty.legalEntity.overseasAddress.province
                    nodoHijo.AppendChild(nodoNieto)
                End If
                If Not IsNothing(objSellerParty.legalEntity.overseasAddress.countryCode) Then
                    nodoNieto = doc.CreateNode(XmlNodeType.Element, "CountryCode", "")
                    nodoNieto.InnerText = objSellerParty.legalEntity.overseasAddress.countryCode.value
                    nodoHijo.AppendChild(nodoNieto)
                End If
            End If
            If Not (IsNothing(objSellerParty.legalEntity.contactDetails)) Then
                nodoHijo = doc.CreateNode(XmlNodeType.Element, "ContactDetails", "")
                nodoLegalEntity.AppendChild(nodoHijo)
                If Not (IsNothing(objSellerParty.legalEntity.contactDetails.telephone)) Then
                    nodoNieto = doc.CreateNode(XmlNodeType.Element, "Telephone", "")
                    nodoNieto.InnerText = objSellerParty.legalEntity.contactDetails.telephone
                    nodoHijo.AppendChild(nodoNieto)
                End If
                If Not (IsNothing(objSellerParty.legalEntity.contactDetails.teleFax)) Then
                    nodoNieto = doc.CreateNode(XmlNodeType.Element, "TeleFax", "")
                    nodoNieto.InnerText = objSellerParty.legalEntity.contactDetails.teleFax
                    nodoHijo.AppendChild(nodoNieto)
                End If
                If Not (IsNothing(objSellerParty.legalEntity.contactDetails.electronicMail)) Then
                    nodoNieto = doc.CreateNode(XmlNodeType.Element, "ElectronicMail", "")
                    nodoNieto.InnerText = objSellerParty.legalEntity.contactDetails.electronicMail
                    nodoHijo.AppendChild(nodoNieto)
                End If
                If Not (IsNothing(objSellerParty.legalEntity.contactDetails.contactPersons)) Then
                    nodoNieto = doc.CreateNode(XmlNodeType.Element, "ContactPersons", "")
                    nodoNieto.InnerText = objSellerParty.legalEntity.contactDetails.contactPersons
                    nodoHijo.AppendChild(nodoNieto)
                End If
            End If
        End If
    End Sub
    ''' <summary>
    ''' Imprime un objeto BuyerPartyType en una estructura de nodos XML siguiendo el estándar Facturae v3.2
    ''' </summary>
    ''' <param name="objBuyerParty">Objeto a imprimir</param>
    ''' <param name="nodoBuyerParty">Nodo raíz sobre el cual se imprime el objeto</param>
    ''' <param name="doc">Objeto de un documento XML el cual contiene una Facturae v3.2</param>
    ''' <remarks>Llamado desde: CFacturae.partiesObj2XML
    ''' Tiempo máximo: menor a 0,1
    ''' Revisado por: auv. 25/05/2012</remarks>
    Private Sub buyerPartyObj2XML(ByRef objBuyerParty As BusinessType, ByRef nodoBuyerParty As XmlNode, ByRef doc As XmlDocument)
        Dim nodoHijo As XmlNode = Nothing
        Dim nodoNieto As XmlNode = Nothing
        Dim nodoLegalEntity As XmlNode = Nothing

        If Not IsNothing(objBuyerParty.taxIdentification) Then
            nodoHijo = doc.CreateNode(XmlNodeType.Element, "TaxIdentification", "")
            nodoBuyerParty.AppendChild(nodoHijo)
            If Not IsNothing(objBuyerParty.taxIdentification.personTypeCode) Then
                nodoNieto = doc.CreateNode(XmlNodeType.Element, "PersonTypeCode", "")
                nodoNieto.InnerText = objBuyerParty.taxIdentification.personTypeCode.value
                nodoHijo.AppendChild(nodoNieto)
            End If
            If Not IsNothing(objBuyerParty.taxIdentification.residenceTypeCode) Then
                nodoNieto = doc.CreateNode(XmlNodeType.Element, "ResidenceTypeCode", "")
                nodoNieto.InnerText = objBuyerParty.taxIdentification.residenceTypeCode.value
                nodoHijo.AppendChild(nodoNieto)
            End If
            If Not IsNothing(objBuyerParty.taxIdentification.taxIdentificationNumber) Then
                nodoNieto = doc.CreateNode(XmlNodeType.Element, "TaxIdentificationNumber", "")
                nodoNieto.InnerText = objBuyerParty.taxIdentification.taxIdentificationNumber
                nodoHijo.AppendChild(nodoNieto)
            End If
        End If

        If Not IsNothing(objBuyerParty.legalEntity) Then
            nodoHijo = doc.CreateNode(XmlNodeType.Element, "LegalEntity", "")
            nodoBuyerParty.AppendChild(nodoHijo)
            nodoLegalEntity = nodoHijo
            If Not IsNothing(objBuyerParty.legalEntity.corporateName) Then
                nodoHijo = doc.CreateNode(XmlNodeType.Element, "CorporateName", "")
                nodoHijo.InnerText = objBuyerParty.legalEntity.corporateName
                nodoLegalEntity.AppendChild(nodoHijo)
            End If
            If Not (IsNothing(objBuyerParty.legalEntity.addressInSpain)) Then
                nodoHijo = doc.CreateNode(XmlNodeType.Element, "AddressInSpain", "")
                nodoLegalEntity.AppendChild(nodoHijo)
                If Not IsNothing(objBuyerParty.legalEntity.addressInSpain.address) Then
                    nodoNieto = doc.CreateNode(XmlNodeType.Element, "Address", "")
                    nodoNieto.InnerText = objBuyerParty.legalEntity.addressInSpain.address
                    nodoHijo.AppendChild(nodoNieto)
                End If
                If Not IsNothing(objBuyerParty.legalEntity.addressInSpain.postCode) Then
                    nodoNieto = doc.CreateNode(XmlNodeType.Element, "PostCode", "")
                    nodoNieto.InnerText = objBuyerParty.legalEntity.addressInSpain.postCode.value
                    nodoHijo.AppendChild(nodoNieto)
                End If
                If Not IsNothing(objBuyerParty.legalEntity.addressInSpain.town) Then
                    nodoNieto = doc.CreateNode(XmlNodeType.Element, "Town", "")
                    nodoNieto.InnerText = objBuyerParty.legalEntity.addressInSpain.town
                    nodoHijo.AppendChild(nodoNieto)
                End If
                If Not IsNothing(objBuyerParty.legalEntity.addressInSpain.province) Then
                    nodoNieto = doc.CreateNode(XmlNodeType.Element, "Province", "")
                    nodoNieto.InnerText = objBuyerParty.legalEntity.addressInSpain.province
                    nodoHijo.AppendChild(nodoNieto)
                End If
                If Not IsNothing(objBuyerParty.legalEntity.addressInSpain.countryCode) Then
                    nodoNieto = doc.CreateNode(XmlNodeType.Element, "CountryCode", "")
                    nodoNieto.InnerText = objBuyerParty.legalEntity.addressInSpain.countryCode.value
                    nodoHijo.AppendChild(nodoNieto)
                End If
            End If
            If Not (IsNothing(objBuyerParty.legalEntity.overseasAddress)) Then
                nodoHijo = doc.CreateNode(XmlNodeType.Element, "OverseasAddress", "")
                nodoLegalEntity.AppendChild(nodoHijo)
                If Not IsNothing(objBuyerParty.legalEntity.overseasAddress.address) Then
                    nodoNieto = doc.CreateNode(XmlNodeType.Element, "Address", "")
                    nodoNieto.InnerText = objBuyerParty.legalEntity.overseasAddress.address
                    nodoHijo.AppendChild(nodoNieto)
                End If
                If Not IsNothing(objBuyerParty.legalEntity.overseasAddress.postCodeAndTown) Then
                    nodoNieto = doc.CreateNode(XmlNodeType.Element, "PostCodeAndTown", "")
                    nodoNieto.InnerText = objBuyerParty.legalEntity.overseasAddress.postCodeAndTown
                    nodoHijo.AppendChild(nodoNieto)
                End If
                If Not IsNothing(objBuyerParty.legalEntity.overseasAddress.province) Then
                    nodoNieto = doc.CreateNode(XmlNodeType.Element, "Province", "")
                    nodoNieto.InnerText = objBuyerParty.legalEntity.overseasAddress.province
                    nodoHijo.AppendChild(nodoNieto)
                End If
                If Not IsNothing(objBuyerParty.legalEntity.overseasAddress.countryCode.value) Then
                    nodoNieto = doc.CreateNode(XmlNodeType.Element, "CountryCode", "")
                    nodoNieto.InnerText = objBuyerParty.legalEntity.overseasAddress.countryCode.value
                    nodoHijo.AppendChild(nodoNieto)
                End If
            End If
        End If
    End Sub
    ''' <summary>
    ''' Imprime un vector de objetos InvoicesType (al ser por ahora facturas individuales, solo se imprimirá
    ''' la factura en la primera posición) en una estructura de nodos XML siguiendo el estándar Facturae v3.2
    ''' </summary>
    ''' <param name="objInvoices">Vector de objetos a imprimir</param>
    ''' <param name="nodoInvoices">Nodo raíz sobre el cual se imprime el vector de objetos</param>
    ''' <param name="doc">Objeto de un documento XML el cual contiene una Facturae v3.2</param>
    ''' <remarks>Llamado desde: CFacturae.facturaeObj2XML
    ''' Tiempo máximo: Depende del número de elementos que tengan los vectores de longitud variable tales como
    ''' el número de items, descuentos, costes, impuestos repercutidos, impuestos retenidos, etc...
    ''' Revisado por: auv. 25/05/2012</remarks>
    Private Sub invoicesObj2XML(ByRef objInvoices() As InvoiceType, ByRef nodoInvoices As XmlNode, ByRef doc As XmlDocument)
        Dim nodoHijo As XmlNode = Nothing
        Dim nodoNieto As XmlNode = Nothing

        If Not IsNothing(objInvoices(0)) Then
            nodoHijo = doc.CreateNode(XmlNodeType.Element, "Invoice", "")
            nodoInvoices.AppendChild(nodoHijo)
            If Not IsNothing(objInvoices(0).invoiceHeader) Then
                nodoNieto = doc.CreateNode(XmlNodeType.Element, "InvoiceHeader", "")
                nodoHijo.AppendChild(nodoNieto)
                Me.invoiceHeaderObj2XML(objInvoices(0).invoiceHeader, nodoNieto, doc)
            End If
            If Not IsNothing(objInvoices(0).invoiceIssueData) Then
                nodoNieto = doc.CreateNode(XmlNodeType.Element, "InvoiceIssueData", "")
                nodoHijo.AppendChild(nodoNieto)
                Me.invoiceIssueDataObj2XML(objInvoices(0).invoiceIssueData, nodoNieto, doc)
            End If
            If Not IsNothing(objInvoices(0).taxesOutputs) Then
                nodoNieto = doc.CreateNode(XmlNodeType.Element, "TaxesOutputs", "")
                nodoHijo.AppendChild(nodoNieto)
                Me.invoiceTaxesOutputsObj2XML(objInvoices(0).taxesOutputs, nodoNieto, doc)
            End If
            If Not (IsNothing(objInvoices(0).taxesWithheld)) Then
                nodoNieto = doc.CreateNode(XmlNodeType.Element, "TaxesWithheld", "")
                nodoHijo.AppendChild(nodoNieto)
                Me.invoiceTaxesWithheldObj2XML(objInvoices(0).taxesWithheld, nodoNieto, doc)
            End If
            If Not IsNothing(objInvoices(0).invoiceTotals) Then
                nodoNieto = doc.CreateNode(XmlNodeType.Element, "InvoiceTotals", "")
                nodoHijo.AppendChild(nodoNieto)
                Me.invoiceTotalsObj2XML(objInvoices(0).invoiceTotals, nodoNieto, doc)
            End If
            If Not IsNothing(objInvoices(0).items) Then
                nodoNieto = doc.CreateNode(XmlNodeType.Element, "Items", "")
                nodoHijo.AppendChild(nodoNieto)
                Me.invoiceItemsObj2XML(objInvoices(0).items, nodoNieto, doc)
            End If
            If Not (IsNothing(objInvoices(0).additionalData)) Then
                nodoNieto = doc.CreateNode(XmlNodeType.Element, "AdditionalData", "")
                nodoHijo.AppendChild(nodoNieto)
                Me.invoiceAdditionalDataObj2XML(objInvoices(0).additionalData, nodoNieto, doc)
            End If
        End If
    End Sub
    ''' <summary>
    ''' Imprime un objeto InvoiceHeaderType en una estructura de nodos XML siguiendo el estándar Facturae v3.2
    ''' </summary>
    ''' <param name="objInvoiceHeader">Objeto a imprimir</param>
    ''' <param name="nodoInvoiceHeader">Nodo raíz sobre el cual se imprime el objeto</param>
    ''' <param name="doc">Objeto de un documento XML el cual contiene una Facturae v3.2</param>
    ''' <remarks>Llamado desde: CFacturae.invoicesObj2XML
    ''' Tiempo máximo: menor a 0,1
    ''' Revisado por: auv. 25/05/2012</remarks>
    Private Sub invoiceHeaderObj2XML(ByRef objInvoiceHeader As InvoiceHeaderType, ByRef nodoInvoiceHeader As XmlNode, ByRef doc As XmlDocument)
        Dim nodoHijo As XmlNode = Nothing
        Dim nodoNieto As XmlNode = Nothing
        Dim nodoBisnieto As XmlNode = Nothing

        If Not IsNothing(objInvoiceHeader.invoiceNumber) Then
            nodoHijo = doc.CreateNode(XmlNodeType.Element, "InvoiceNumber", "")
            nodoHijo.InnerText = objInvoiceHeader.invoiceNumber
            nodoInvoiceHeader.AppendChild(nodoHijo)
        End If
        If Not IsNothing(objInvoiceHeader.invoiceDocumentType) Then
            nodoHijo = doc.CreateNode(XmlNodeType.Element, "InvoiceDocumentType", "")
            nodoHijo.InnerText = objInvoiceHeader.invoiceDocumentType.value
            nodoInvoiceHeader.AppendChild(nodoHijo)
        End If
        If Not IsNothing(objInvoiceHeader.invoiceClass) Then
            nodoHijo = doc.CreateNode(XmlNodeType.Element, "InvoiceClass", "")
            nodoHijo.InnerText = objInvoiceHeader.invoiceClass.value
            nodoInvoiceHeader.AppendChild(nodoHijo)
        End If
        If Not (IsNothing(objInvoiceHeader.corrective)) Then
            nodoHijo = doc.CreateNode(XmlNodeType.Element, "Corrective", "")
            nodoInvoiceHeader.AppendChild(nodoHijo)
            If Not (IsNothing(objInvoiceHeader.corrective.invoiceNumber)) Then
                nodoNieto = doc.CreateNode(XmlNodeType.Element, "InvoiceNumber", "")
                nodoNieto.InnerText = objInvoiceHeader.corrective.invoiceNumber
                nodoHijo.AppendChild(nodoNieto)
            End If
            If Not IsNothing(objInvoiceHeader.corrective.reasonCode) Then
                nodoNieto = doc.CreateNode(XmlNodeType.Element, "ReasonCode", "")
                nodoNieto.InnerText = objInvoiceHeader.corrective.reasonCode.value
                nodoHijo.AppendChild(nodoNieto)
            End If
            If Not IsNothing(objInvoiceHeader.corrective.reasonDescription) Then
                nodoNieto = doc.CreateNode(XmlNodeType.Element, "ReasonDescription", "")
                nodoNieto.InnerText = objInvoiceHeader.corrective.reasonDescription.value
                nodoHijo.AppendChild(nodoNieto)
            End If
            If Not IsNothing(objInvoiceHeader.corrective.taxPeriod) Then
                nodoNieto = doc.CreateNode(XmlNodeType.Element, "TaxPeriod", "")
                nodoHijo.AppendChild(nodoNieto)
                If Not IsNothing(objInvoiceHeader.corrective.taxPeriod.startDate) Then
                    nodoBisnieto = doc.CreateNode(XmlNodeType.Element, "StartDate", "")
                    nodoBisnieto.InnerText = date_ObjId2XMLStr(objInvoiceHeader.corrective.taxPeriod.startDate)
                    nodoNieto.AppendChild(nodoBisnieto)
                End If
                If Not IsNothing(objInvoiceHeader.corrective.taxPeriod.endDate) Then
                    nodoBisnieto = doc.CreateNode(XmlNodeType.Element, "EndDate", "")
                    nodoBisnieto.InnerText = date_ObjId2XMLStr(objInvoiceHeader.corrective.taxPeriod.endDate)
                    nodoNieto.AppendChild(nodoBisnieto)
                End If
            End If
            If Not IsNothing(objInvoiceHeader.corrective.correctionMethod) Then
                nodoNieto = doc.CreateNode(XmlNodeType.Element, "CorrectionMethod", "")
                nodoNieto.InnerText = objInvoiceHeader.corrective.correctionMethod.value
                nodoHijo.AppendChild(nodoNieto)
            End If
            If Not IsNothing(objInvoiceHeader.corrective.correctionMethodDescription) Then
                nodoNieto = doc.CreateNode(XmlNodeType.Element, "CorrectionMethodDescription", "")
                nodoNieto.InnerText = objInvoiceHeader.corrective.correctionMethodDescription.value
                nodoHijo.AppendChild(nodoNieto)
            End If
        End If
    End Sub
    ''' <summary>
    ''' Imprime un objeto InvoiceIssueDataType en una estructura de nodos XML siguiendo el estándar Facturae v3.2
    ''' </summary>
    ''' <param name="objInvoiceIssueData">Objeto a imprimir</param>
    ''' <param name="nodoInvoiceIssueData">Nodo raíz sobre el cual se imprime el objeto</param>
    ''' <param name="doc">Objeto de un documento XML el cual contiene una Facturae v3.2</param>
    ''' <remarks>Llamado desde: CFacturae.invoicesObj2XML
    ''' Tiempo máximo: menor a 0,1
    ''' Revisado por: auv. 25/05/2012</remarks>
    Private Sub invoiceIssueDataObj2XML(ByRef objInvoiceIssueData As InvoiceIssueDataType, ByRef nodoInvoiceIssueData As XmlNode, ByRef doc As XmlDocument)
        Dim nodoHijo As XmlNode = Nothing
        Dim nodoNieto As XmlNode = Nothing

        If Not IsNothing(objInvoiceIssueData.issueDate) Then
            nodoHijo = doc.CreateNode(XmlNodeType.Element, "IssueDate", "")
            nodoHijo.InnerText = date_ObjId2XMLStr(objInvoiceIssueData.issueDate)
            nodoInvoiceIssueData.AppendChild(nodoHijo)
        End If
        If Not IsNothing(objInvoiceIssueData.invoiceCurrencyCode) Then
            nodoHijo = doc.CreateNode(XmlNodeType.Element, "InvoiceCurrencyCode", "")
            nodoHijo.InnerText = objInvoiceIssueData.invoiceCurrencyCode.value
            nodoInvoiceIssueData.AppendChild(nodoHijo)
        End If
        If Not (IsNothing(objInvoiceIssueData.exchangeRateDetails)) Then
            nodoHijo = doc.CreateNode(XmlNodeType.Element, "ExchangeRateDetails", "")
            nodoInvoiceIssueData.AppendChild(nodoHijo)
            If Not IsNothing(objInvoiceIssueData.exchangeRateDetails.exchangeRate) Then
                nodoNieto = doc.CreateNode(XmlNodeType.Element, "ExchangeRate", "")
                nodoNieto.InnerText = rellenarDecimales(redondearHaciaAbajo(dbl2Str(objInvoiceIssueData.exchangeRateDetails.exchangeRate.value, "."), 2), 2, ".")
                nodoHijo.AppendChild(nodoNieto)
            End If
            If Not IsNothing(objInvoiceIssueData.exchangeRateDetails.exchangeRateDate) Then
                nodoNieto = doc.CreateNode(XmlNodeType.Element, "ExchangeRateDate", "")
                nodoNieto.InnerText = date_ObjId2XMLStr(objInvoiceIssueData.exchangeRateDetails.exchangeRateDate)
                nodoHijo.AppendChild(nodoNieto)
            End If
        End If
        If Not IsNothing(objInvoiceIssueData.taxCurrencyCode) Then
            nodoHijo = doc.CreateNode(XmlNodeType.Element, "TaxCurrencyCode", "")
            nodoHijo.InnerText = objInvoiceIssueData.taxCurrencyCode.value
            nodoInvoiceIssueData.AppendChild(nodoHijo)
        End If
        If Not IsNothing(objInvoiceIssueData.languageName) Then
            nodoHijo = doc.CreateNode(XmlNodeType.Element, "LanguageName", "")
            nodoHijo.InnerText = objInvoiceIssueData.languageName.value
            nodoInvoiceIssueData.AppendChild(nodoHijo)
        End If
    End Sub
    ''' <summary>
    ''' Imprime un vector de objetos TaxOutputType en una estructura de nodos XML siguiendo el estándar Facturae v3.2 (para impuestos a nivel de factura)
    ''' </summary>
    ''' <param name="objInvoiceTaxesOutputs">Vector de objetos a imprimir</param>
    ''' <param name="nodoInvoiceTaxesOutputs">Nodo raíz sobre el cual se imprime el vector de objetos</param>
    ''' <param name="doc">Objeto de un documento XML el cual contiene una Facturae v3.2</param>
    ''' <remarks>Llamado desde: CFacturae.invoicesObj2XML
    ''' Tiempo máximo: Depende del número de elementos que tengan los vectores de longitud variable tales como
    ''' el número de impuestos repercutidos.
    ''' Revisado por: auv. 25/05/2012</remarks>
    Private Sub invoiceTaxesOutputsObj2XML(ByRef objInvoiceTaxesOutputs() As TaxOutputType, ByRef nodoInvoiceTaxesOutputs As XmlNode, ByRef doc As XmlDocument)
        Dim nodoHijo As XmlNode = Nothing
        Dim nodoNieto As XmlNode = Nothing
        Dim nodoBisnieto As XmlNode = Nothing
        Dim contadorDeImpuestos As Integer = Nothing

        contadorDeImpuestos = 0
        While contadorDeImpuestos < objInvoiceTaxesOutputs.Count
            If Not IsNothing(objInvoiceTaxesOutputs(contadorDeImpuestos)) Then
                nodoHijo = doc.CreateNode(XmlNodeType.Element, "Tax", "")
                nodoInvoiceTaxesOutputs.AppendChild(nodoHijo)
                If Not IsNothing(objInvoiceTaxesOutputs(contadorDeImpuestos).taxTypeCode) Then
                    nodoNieto = doc.CreateNode(XmlNodeType.Element, "TaxTypeCode", "")
                    nodoNieto.InnerText = objInvoiceTaxesOutputs(contadorDeImpuestos).taxTypeCode.value
                    nodoHijo.AppendChild(nodoNieto)
                End If
                If Not IsNothing(objInvoiceTaxesOutputs(contadorDeImpuestos).taxRate) Then
                    nodoNieto = doc.CreateNode(XmlNodeType.Element, "TaxRate", "")
                    nodoNieto.InnerText = rellenarDecimales(redondearHaciaAbajo(dbl2Str(objInvoiceTaxesOutputs(contadorDeImpuestos).taxRate.value, "."), 2), 2, ".")
                    nodoHijo.AppendChild(nodoNieto)
                End If
                If Not IsNothing(objInvoiceTaxesOutputs(contadorDeImpuestos).taxableBase) Then
                    nodoNieto = doc.CreateNode(XmlNodeType.Element, "TaxableBase", "")
                    nodoHijo.AppendChild(nodoNieto)
                    If Not IsNothing(objInvoiceTaxesOutputs(contadorDeImpuestos).taxableBase.totalAmount) Then
                        nodoBisnieto = doc.CreateNode(XmlNodeType.Element, "TotalAmount", "")
                        nodoBisnieto.InnerText = rellenarDecimales(redondearHaciaAbajo(dbl2Str(objInvoiceTaxesOutputs(contadorDeImpuestos).taxableBase.totalAmount.value, "."), 2), 2, ".")
                        nodoNieto.AppendChild(nodoBisnieto)
                    End If
                    If Not (IsNothing(objInvoiceTaxesOutputs(contadorDeImpuestos).taxableBase.equivalentInEuros)) Then
                        nodoBisnieto = doc.CreateNode(XmlNodeType.Element, "EquivalentInEuros", "")
                        nodoBisnieto.InnerText = rellenarDecimales(redondearHaciaAbajo(dbl2Str(objInvoiceTaxesOutputs(contadorDeImpuestos).taxableBase.equivalentInEuros.value, "."), 2), 2, ".")
                        nodoNieto.AppendChild(nodoBisnieto)
                    End If
                End If
                If Not (IsNothing(objInvoiceTaxesOutputs(contadorDeImpuestos).taxAmount)) Then
                    nodoNieto = doc.CreateNode(XmlNodeType.Element, "TaxAmount", "")
                    nodoHijo.AppendChild(nodoNieto)
                    If Not IsNothing(objInvoiceTaxesOutputs(contadorDeImpuestos).taxAmount.totalAmount) Then
                        nodoBisnieto = doc.CreateNode(XmlNodeType.Element, "TotalAmount", "")
                        nodoBisnieto.InnerText = rellenarDecimales(redondearHaciaAbajo(dbl2Str(objInvoiceTaxesOutputs(contadorDeImpuestos).taxAmount.totalAmount.value, "."), 2), 2, ".")
                        nodoNieto.AppendChild(nodoBisnieto)
                    End If
                    If Not (IsNothing(objInvoiceTaxesOutputs(contadorDeImpuestos).taxAmount.equivalentInEuros)) Then
                        nodoBisnieto = doc.CreateNode(XmlNodeType.Element, "EquivalentInEuros", "")
                        nodoBisnieto.InnerText = rellenarDecimales(redondearHaciaAbajo(dbl2Str(objInvoiceTaxesOutputs(contadorDeImpuestos).taxAmount.equivalentInEuros.value, "."), 2), 2, ".")
                        nodoNieto.AppendChild(nodoBisnieto)
                    End If
                End If
                If Not (IsNothing(objInvoiceTaxesOutputs(contadorDeImpuestos).equivalenceSurcharge)) Then
                    nodoNieto = doc.CreateNode(XmlNodeType.Element, "EquivalenceSurcharge", "")
                    nodoNieto.InnerText = rellenarDecimales(redondearHaciaAbajo(dbl2Str(objInvoiceTaxesOutputs(contadorDeImpuestos).equivalenceSurcharge.value, "."), 2), 2, ".")
                    nodoHijo.AppendChild(nodoNieto)
                End If
                If Not (IsNothing(objInvoiceTaxesOutputs(contadorDeImpuestos).equivalenceSurchargeAmount)) Then
                    nodoNieto = doc.CreateNode(XmlNodeType.Element, "EquivalenceSurchargeAmount", "")
                    nodoHijo.AppendChild(nodoNieto)
                    If Not IsNothing(objInvoiceTaxesOutputs(contadorDeImpuestos).equivalenceSurchargeAmount.totalAmount) Then
                        nodoBisnieto = doc.CreateNode(XmlNodeType.Element, "TotalAmount", "")
                        nodoBisnieto.InnerText = rellenarDecimales(redondearHaciaAbajo(dbl2Str(objInvoiceTaxesOutputs(contadorDeImpuestos).equivalenceSurchargeAmount.totalAmount.value, "."), 2), 2, ".")
                        nodoNieto.AppendChild(nodoBisnieto)
                    End If
                    If Not (IsNothing(objInvoiceTaxesOutputs(contadorDeImpuestos).equivalenceSurchargeAmount.equivalentInEuros)) Then
                        nodoBisnieto = doc.CreateNode(XmlNodeType.Element, "EquivalentInEuros", "")
                        nodoBisnieto.InnerText = rellenarDecimales(redondearHaciaAbajo(dbl2Str(objInvoiceTaxesOutputs(contadorDeImpuestos).equivalenceSurchargeAmount.equivalentInEuros.value, "."), 2), 2, ".")
                        nodoNieto.AppendChild(nodoBisnieto)
                    End If
                End If
            End If
            contadorDeImpuestos = contadorDeImpuestos + 1
        End While
    End Sub
    ''' <summary>
    ''' Imprime un objeto TaxesWithheldType en una estructura de nodos XML siguiendo el estándar Facturae v3.2 (para impuestos a nivel de factura)
    ''' </summary>
    ''' <param name="objInvoiceTaxesWithheld">Vector de objetos a imprimir</param>
    ''' <param name="nodoInvoiceTaxesWithheld">Nodo raíz sobre el cual se imprime el vector de objetos</param>
    ''' <param name="doc">Objeto de un documento XML el cual contiene una Facturae v3.2</param>
    ''' <remarks>Llamado desde: CFacturae.invoicesObj2XML
    ''' Tiempo máximo: Depende del número de elementos que tengan los vectores de longitud variable tales como
    ''' el número de impuestos retenidos.
    ''' Revisado por: auv. 25/05/2012</remarks>
    Private Sub invoiceTaxesWithheldObj2XML(ByRef objInvoiceTaxesWithheld() As TaxWithheldType, ByRef nodoInvoiceTaxesWithheld As XmlNode, ByRef doc As XmlDocument)
        Dim nodoHijo As XmlNode = Nothing
        Dim nodoNieto As XmlNode = Nothing
        Dim nodoBisnieto As XmlNode = Nothing
        Dim contadorDeImpuestos As Integer = Nothing

        contadorDeImpuestos = 0
        While contadorDeImpuestos < objInvoiceTaxesWithheld.Count
            If Not IsNothing(objInvoiceTaxesWithheld(contadorDeImpuestos)) Then
                nodoHijo = doc.CreateNode(XmlNodeType.Element, "Tax", "")
                nodoInvoiceTaxesWithheld.AppendChild(nodoHijo)
                If Not IsNothing(objInvoiceTaxesWithheld(contadorDeImpuestos).taxTypeCode) Then
                    nodoNieto = doc.CreateNode(XmlNodeType.Element, "TaxTypeCode", "")
                    nodoNieto.InnerText = objInvoiceTaxesWithheld(contadorDeImpuestos).taxTypeCode.value
                    nodoHijo.AppendChild(nodoNieto)
                End If
                If Not IsNothing(objInvoiceTaxesWithheld(contadorDeImpuestos).taxRate) Then
                    nodoNieto = doc.CreateNode(XmlNodeType.Element, "TaxRate", "")
                    nodoNieto.InnerText = rellenarDecimales(redondearHaciaAbajo(dbl2Str(objInvoiceTaxesWithheld(contadorDeImpuestos).taxRate.value, "."), 2), 2, ".")
                    nodoHijo.AppendChild(nodoNieto)
                End If
                If Not IsNothing(objInvoiceTaxesWithheld(contadorDeImpuestos).taxableBase) Then
                    nodoNieto = doc.CreateNode(XmlNodeType.Element, "TaxableBase", "")
                    nodoHijo.AppendChild(nodoNieto)
                    If Not IsNothing(objInvoiceTaxesWithheld(contadorDeImpuestos).taxableBase.totalAmount) Then
                        nodoBisnieto = doc.CreateNode(XmlNodeType.Element, "TotalAmount", "")
                        nodoBisnieto.InnerText = rellenarDecimales(redondearHaciaAbajo(dbl2Str(objInvoiceTaxesWithheld(contadorDeImpuestos).taxableBase.totalAmount.value, "."), 2), 2, ".")
                        nodoNieto.AppendChild(nodoBisnieto)
                    End If
                    If Not (IsNothing(objInvoiceTaxesWithheld(contadorDeImpuestos).taxableBase.equivalentInEuros)) Then
                        nodoBisnieto = doc.CreateNode(XmlNodeType.Element, "EquivalentInEuros", "")
                        nodoBisnieto.InnerText = rellenarDecimales(redondearHaciaAbajo(dbl2Str(objInvoiceTaxesWithheld(contadorDeImpuestos).taxableBase.equivalentInEuros.value, "."), 2), 2, ".")
                        nodoNieto.AppendChild(nodoBisnieto)
                    End If
                End If
                If Not (IsNothing(objInvoiceTaxesWithheld(contadorDeImpuestos).taxAmount)) Then
                    nodoNieto = doc.CreateNode(XmlNodeType.Element, "TaxAmount", "")
                    nodoHijo.AppendChild(nodoNieto)
                    If Not IsNothing(objInvoiceTaxesWithheld(contadorDeImpuestos).taxAmount.totalAmount) Then
                        nodoBisnieto = doc.CreateNode(XmlNodeType.Element, "TotalAmount", "")
                        nodoBisnieto.InnerText = rellenarDecimales(redondearHaciaAbajo(dbl2Str(objInvoiceTaxesWithheld(contadorDeImpuestos).taxAmount.totalAmount.value, "."), 2), 2, ".")
                        nodoNieto.AppendChild(nodoBisnieto)
                    End If
                    If Not (IsNothing(objInvoiceTaxesWithheld(contadorDeImpuestos).taxAmount.equivalentInEuros)) Then
                        nodoBisnieto = doc.CreateNode(XmlNodeType.Element, "EquivalentInEuros", "")
                        nodoBisnieto.InnerText = rellenarDecimales(redondearHaciaAbajo(dbl2Str(objInvoiceTaxesWithheld(contadorDeImpuestos).taxAmount.equivalentInEuros.value, "."), 2), 2, ".")
                        nodoNieto.AppendChild(nodoBisnieto)
                    End If
                End If
            End If
            contadorDeImpuestos = contadorDeImpuestos + 1
        End While
    End Sub
    ''' <summary>
    ''' Imprime un objeto InvoiceTotalsType en una estructura de nodos XML siguiendo el estándar Facturae v3.2
    ''' </summary>
    ''' <param name="objInvoiceTotals">Objeto a imprimir</param>
    ''' <param name="nodoInvoiceTotals">Nodo raíz sobre el cual se imprime el objeto</param>
    ''' <param name="doc">Objeto de un documento XML el cual contiene una Facturae v3.2</param>
    ''' <remarks>Llamado desde: CFacturae.invoicesObj2XML
    ''' Tiempo máximo: Depende del número de elementos que tengan los vectores de longitud variable tales como
    ''' el número de descuentos y del número de costes.
    ''' Revisado por: auv. 25/05/2012</remarks>
    Private Sub invoiceTotalsObj2XML(ByRef objInvoiceTotals As InvoiceTotalsType, ByRef nodoInvoiceTotals As XmlNode, ByRef doc As XmlDocument)
        Dim nodoHijo As XmlNode = Nothing
        Dim nodoNieto As XmlNode = Nothing
        Dim nodoBisnieto As XmlNode = Nothing
        Dim contador As Integer = Nothing

        If Not IsNothing(objInvoiceTotals.totalGrossAmount) Then
            nodoHijo = doc.CreateNode(XmlNodeType.Element, "TotalGrossAmount", "")
            nodoHijo.InnerText = rellenarDecimales(redondearHaciaAbajo(dbl2Str(objInvoiceTotals.totalGrossAmount.value, "."), 2), 2, ".")
            nodoInvoiceTotals.AppendChild(nodoHijo)
        End If
        If Not (IsNothing(objInvoiceTotals.generalDiscounts)) Then
            nodoHijo = doc.CreateNode(XmlNodeType.Element, "GeneralDiscounts", "")
            nodoInvoiceTotals.AppendChild(nodoHijo)
            contador = 0
            While contador < objInvoiceTotals.generalDiscounts.Count
                If Not IsNothing(objInvoiceTotals.generalDiscounts(contador)) Then
                    nodoNieto = doc.CreateNode(XmlNodeType.Element, "Discount", "")
                    nodoHijo.AppendChild(nodoNieto)
                    If Not IsNothing(objInvoiceTotals.generalDiscounts(contador).discountReason) Then
                        nodoBisnieto = doc.CreateNode(XmlNodeType.Element, "DiscountReason", "")
                        nodoBisnieto.InnerText = objInvoiceTotals.generalDiscounts(contador).discountReason
                        nodoNieto.AppendChild(nodoBisnieto)
                    End If
                    If Not (IsNothing(objInvoiceTotals.generalDiscounts(contador).discountRate)) Then
                        nodoBisnieto = doc.CreateNode(XmlNodeType.Element, "DiscountRate", "")
                        nodoBisnieto.InnerText = rellenarDecimales(redondearHaciaAbajo(dbl2Str(objInvoiceTotals.generalDiscounts(contador).discountRate.value, "."), 4), 4, ".")
                        nodoNieto.AppendChild(nodoBisnieto)
                    End If
                    If Not IsNothing(objInvoiceTotals.generalDiscounts(contador).discountAmount) Then
                        nodoBisnieto = doc.CreateNode(XmlNodeType.Element, "DiscountAmount", "")
                        nodoBisnieto.InnerText = rellenarDecimales(redondearHaciaAbajo(dbl2Str(objInvoiceTotals.generalDiscounts(contador).discountAmount.value, "."), 6), 6, ".")
                        nodoNieto.AppendChild(nodoBisnieto)
                    End If
                End If
                contador = contador + 1
            End While
        End If
        If Not IsNothing(objInvoiceTotals.generalSurcharges) Then
            nodoHijo = doc.CreateNode(XmlNodeType.Element, "GeneralSurcharges", "")
            nodoInvoiceTotals.AppendChild(nodoHijo)
            contador = 0
            While contador < objInvoiceTotals.generalSurcharges.Count
                If Not IsNothing(objInvoiceTotals.generalSurcharges(contador)) Then
                    nodoNieto = doc.CreateNode(XmlNodeType.Element, "Charge", "")
                    nodoHijo.AppendChild(nodoNieto)
                    If Not IsNothing(objInvoiceTotals.generalSurcharges(contador).chargeReason) Then
                        nodoBisnieto = doc.CreateNode(XmlNodeType.Element, "ChargeReason", "")
                        nodoBisnieto.InnerText = objInvoiceTotals.generalSurcharges(contador).chargeReason
                        nodoNieto.AppendChild(nodoBisnieto)
                    End If
                    If Not (IsNothing(objInvoiceTotals.generalSurcharges(contador).chargeRate)) Then
                        nodoBisnieto = doc.CreateNode(XmlNodeType.Element, "ChargeRate", "")
                        nodoBisnieto.InnerText = rellenarDecimales(redondearHaciaAbajo(dbl2Str(objInvoiceTotals.generalSurcharges(contador).chargeRate.value, "."), 4), 4, ".")
                        nodoNieto.AppendChild(nodoBisnieto)
                    End If
                    If Not IsNothing(objInvoiceTotals.generalSurcharges(contador).chargeAmount) Then
                        nodoBisnieto = doc.CreateNode(XmlNodeType.Element, "ChargeAmount", "")
                        nodoBisnieto.InnerText = rellenarDecimales(redondearHaciaAbajo(dbl2Str(objInvoiceTotals.generalSurcharges(contador).chargeAmount.value, "."), 6), 6, ".")
                        nodoNieto.AppendChild(nodoBisnieto)
                    End If
                End If
                contador = contador + 1
            End While
        End If
        If Not IsNothing(objInvoiceTotals.totalGeneralDiscounts) Then
            nodoHijo = doc.CreateNode(XmlNodeType.Element, "TotalGeneralDiscounts", "")
            nodoHijo.InnerText = rellenarDecimales(redondearHaciaAbajo(dbl2Str(objInvoiceTotals.totalGeneralDiscounts.value, "."), 2), 2, ".")
            nodoInvoiceTotals.AppendChild(nodoHijo)
        End If
        If Not IsNothing(objInvoiceTotals.totalGeneralSurcharges) Then
            nodoHijo = doc.CreateNode(XmlNodeType.Element, "TotalGeneralSurcharges", "")
            nodoHijo.InnerText = rellenarDecimales(redondearHaciaAbajo(dbl2Str(objInvoiceTotals.totalGeneralSurcharges.value, "."), 2), 2, ".")
            nodoInvoiceTotals.AppendChild(nodoHijo)
        End If
        If Not IsNothing(objInvoiceTotals.totalGrossAmountBeforeTaxes) Then
            nodoHijo = doc.CreateNode(XmlNodeType.Element, "TotalGrossAmountBeforeTaxes", "")
            nodoHijo.InnerText = rellenarDecimales(redondearHaciaAbajo(dbl2Str(objInvoiceTotals.totalGrossAmountBeforeTaxes.value, "."), 2), 2, ".")
            nodoInvoiceTotals.AppendChild(nodoHijo)
        End If
        If Not IsNothing(objInvoiceTotals.totalTaxOutputs) Then
            nodoHijo = doc.CreateNode(XmlNodeType.Element, "TotalTaxOutputs", "")
            nodoHijo.InnerText = rellenarDecimales(redondearHaciaAbajo(dbl2Str(objInvoiceTotals.totalTaxOutputs.value, "."), 2), 2, ".")
            nodoInvoiceTotals.AppendChild(nodoHijo)
        End If
        If Not IsNothing(objInvoiceTotals.totalTaxesWithheld) Then
            nodoHijo = doc.CreateNode(XmlNodeType.Element, "TotalTaxesWithheld", "")
            nodoHijo.InnerText = rellenarDecimales(redondearHaciaAbajo(dbl2Str(objInvoiceTotals.totalTaxesWithheld.value, "."), 2), 2, ".")
            nodoInvoiceTotals.AppendChild(nodoHijo)
        End If
        If Not IsNothing(objInvoiceTotals.invoiceTotal) Then
            nodoHijo = doc.CreateNode(XmlNodeType.Element, "InvoiceTotal", "")
            nodoHijo.InnerText = rellenarDecimales(redondearHaciaAbajo(dbl2Str(objInvoiceTotals.invoiceTotal.value, "."), 2), 2, ".")
            nodoInvoiceTotals.AppendChild(nodoHijo)
        End If
        If Not IsNothing(objInvoiceTotals.totalOutstandingAmount) Then
            nodoHijo = doc.CreateNode(XmlNodeType.Element, "TotalOutstandingAmount", "")
            nodoHijo.InnerText = rellenarDecimales(redondearHaciaAbajo(dbl2Str(objInvoiceTotals.totalOutstandingAmount.value, "."), 2), 2, ".")
            nodoInvoiceTotals.AppendChild(nodoHijo)
        End If
        If Not IsNothing(objInvoiceTotals.amountsWithheld) Then
            nodoHijo = doc.CreateNode(XmlNodeType.Element, "AmountsWithheld", "")
            nodoInvoiceTotals.AppendChild(nodoHijo)
            If Not IsNothing(objInvoiceTotals.amountsWithheld.withholdingReason) Then
                nodoNieto = doc.CreateNode(XmlNodeType.Element, "WithholdingReason", "")
                nodoNieto.InnerText = objInvoiceTotals.amountsWithheld.withholdingReason
                nodoHijo.AppendChild(nodoNieto)
            End If
            If Not (IsNothing(objInvoiceTotals.amountsWithheld.withholdingRate)) Then
                nodoNieto = doc.CreateNode(XmlNodeType.Element, "WithholdingRate", "")
                nodoNieto.InnerText = rellenarDecimales(redondearHaciaAbajo(dbl2Str(objInvoiceTotals.amountsWithheld.withholdingRate.value, "."), 4), 4, ".")
                nodoHijo.AppendChild(nodoNieto)
            End If
            If Not IsNothing(objInvoiceTotals.amountsWithheld.withholdingAmount) Then
                nodoNieto = doc.CreateNode(XmlNodeType.Element, "WithholdingAmount", "")
                nodoNieto.InnerText = rellenarDecimales(redondearHaciaAbajo(dbl2Str(objInvoiceTotals.amountsWithheld.withholdingAmount.value, "."), 2), 2, ".")
                nodoHijo.AppendChild(nodoNieto)
            End If
        End If
        If Not IsNothing(objInvoiceTotals.totalExecutableAmount) Then
            nodoHijo = doc.CreateNode(XmlNodeType.Element, "TotalExecutableAmount", "")
            nodoHijo.InnerText = rellenarDecimales(redondearHaciaAbajo(dbl2Str(objInvoiceTotals.totalExecutableAmount.value, "."), 2), 2, ".")
            nodoInvoiceTotals.AppendChild(nodoHijo)
        End If
    End Sub
    ''' <summary>
    ''' Imprime un vector de objetos ItemType en una estructura de nodos XML siguiendo el estándar Facturae v3.2
    ''' </summary>
    ''' <param name="objInvoiceItems">Vector de objetos a imprimir</param>
    ''' <param name="nodoInvoiceItems">Nodo raíz sobre el cual se imprime el vector de objetos</param>
    ''' <param name="doc">Objeto de un documento XML el cual contiene una Facturae v3.2</param>
    ''' <remarks>Llamado desde: CFacturae.invoicesObj2XML
    ''' Tiempo máximo: Depende del número de elementos que tengan los vectores de longitud variable tales como
    ''' el número de items, descuentos, costes, impuestos repercutidos, impuestos retenidos, etc...
    ''' Revisado por: auv. 25/05/2012</remarks>
    Private Sub invoiceItemsObj2XML(ByRef objInvoiceItems() As ItemType, ByRef nodoInvoiceItems As XmlNode, ByRef doc As XmlDocument)
        Dim nodoHijo As XmlNode = Nothing
        Dim contador As Integer = Nothing

        contador = 0

        While contador < objInvoiceItems.Count
            If Not IsNothing(objInvoiceItems(contador)) Then
                nodoHijo = doc.CreateNode(XmlNodeType.Element, "InvoiceLine", "")
                nodoInvoiceItems.AppendChild(nodoHijo)
                Me.invoiceItemObj2XML(objInvoiceItems(contador), nodoHijo, doc)
            End If
            contador = contador + 1
        End While
    End Sub
    ''' <summary>
    ''' Imprime un objeto ItemType en una estructura de nodos XML siguiendo el estándar Facturae v3.2
    ''' </summary>
    ''' <param name="objInvoiceItem">Objeto a imprimir</param>
    ''' <param name="nodoInvoiceItem">Nodo raíz sobre el cual se imprime el objeto</param>
    ''' <param name="doc">Objeto de un documento XML el cual contiene una Facturae v3.2</param>
    ''' <remarks>Llamado desde: CFacturae.invoiceItemsObj2XML
    ''' Tiempo máximo: Depende del número de elementos que tengan los vectores de longitud variable tales como
    ''' el número de descuentos, costes, impuestos repercutidos, impuestos retenidos, etc...
    ''' Revisado por: auv. 25/05/2012</remarks>
    Private Sub invoiceItemObj2XML(ByRef objInvoiceItem As ItemType, ByRef nodoInvoiceItem As XmlNode, ByRef doc As XmlDocument)
        Dim nodoHijo As XmlNode = Nothing
        Dim nodoNieto As XmlNode = Nothing
        Dim nodoBisnieto As XmlNode = Nothing
        Dim contador As Integer = Nothing

        If Not (IsNothing(objInvoiceItem.sequenceNumber)) Then
            nodoHijo = doc.CreateNode(XmlNodeType.Element, "SequenceNumber", "")
            nodoHijo.InnerText = objInvoiceItem.sequenceNumber
            nodoInvoiceItem.AppendChild(nodoHijo)
        End If
        If Not IsNothing(objInvoiceItem.deliveryNotesReferences) Then
            nodoHijo = doc.CreateNode(XmlNodeType.Element, "DeliveryNotesReferences", "")
            nodoInvoiceItem.AppendChild(nodoHijo)
            contador = 0
            While contador < objInvoiceItem.deliveryNotesReferences.Count
                If Not IsNothing(objInvoiceItem.deliveryNotesReferences(contador)) Then
                    nodoNieto = doc.CreateNode(XmlNodeType.Element, "DeliveryNote", "")
                    nodoHijo.AppendChild(nodoNieto)
                    If Not IsNothing(objInvoiceItem.deliveryNotesReferences(contador).deliveryNoteNumber) Then
                        nodoBisnieto = doc.CreateNode(XmlNodeType.Element, "DeliveryNoteNumber", "")
                        nodoBisnieto.InnerText = objInvoiceItem.deliveryNotesReferences(contador).deliveryNoteNumber
                        nodoNieto.AppendChild(nodoBisnieto)
                    End If
                    If Not (IsNothing(objInvoiceItem.deliveryNotesReferences(contador).deliveryNoteDate)) Then
                        nodoBisnieto = doc.CreateNode(XmlNodeType.Element, "DeliveryNoteDate", "")
                        nodoBisnieto.InnerText = date_ObjId2XMLStr(objInvoiceItem.deliveryNotesReferences(contador).deliveryNoteDate)
                        nodoNieto.AppendChild(nodoBisnieto)
                    End If
                End If
                contador = contador + 1
            End While
        End If
        If Not IsNothing(objInvoiceItem.itemDescription) Then
            nodoHijo = doc.CreateNode(XmlNodeType.Element, "ItemDescription", "")
            nodoHijo.InnerText = objInvoiceItem.itemDescription
            nodoInvoiceItem.AppendChild(nodoHijo)
        End If
        If Not IsNothing(objInvoiceItem.quantity) Then
            nodoHijo = doc.CreateNode(XmlNodeType.Element, "Quantity", "")
            nodoHijo.InnerText = objInvoiceItem.quantity
            nodoInvoiceItem.AppendChild(nodoHijo)
        End If
        If Not (IsNothing(objInvoiceItem.unitOfMeasure)) Then
            nodoHijo = doc.CreateNode(XmlNodeType.Element, "UnitOfMeasure", "")
            nodoHijo.InnerText = objInvoiceItem.unitOfMeasure.value
            nodoInvoiceItem.AppendChild(nodoHijo)
        End If
        If Not IsNothing(objInvoiceItem.unitPriceWithoutTax) Then
            nodoHijo = doc.CreateNode(XmlNodeType.Element, "UnitPriceWithoutTax", "")
            nodoHijo.InnerText = rellenarDecimales(redondearHaciaAbajo(dbl2Str(objInvoiceItem.unitPriceWithoutTax.value, "."), 6), 6, ".")
            nodoInvoiceItem.AppendChild(nodoHijo)
        End If
        If Not IsNothing(objInvoiceItem.totalCost) Then
            nodoHijo = doc.CreateNode(XmlNodeType.Element, "TotalCost", "")
            nodoHijo.InnerText = rellenarDecimales(redondearHaciaAbajo(dbl2Str(objInvoiceItem.totalCost.value, "."), 6), 6, ".")
            nodoInvoiceItem.AppendChild(nodoHijo)
        End If
        If Not IsNothing(objInvoiceItem.discountsAndRebates) Then
            nodoHijo = doc.CreateNode(XmlNodeType.Element, "DiscountsAndRebates", "")
            nodoInvoiceItem.AppendChild(nodoHijo)
            contador = 0
            While contador < objInvoiceItem.discountsAndRebates.Count
                If Not IsNothing(objInvoiceItem.discountsAndRebates(contador)) Then
                    nodoNieto = doc.CreateNode(XmlNodeType.Element, "Discount", "")
                    nodoHijo.AppendChild(nodoNieto)
                    If Not IsNothing(objInvoiceItem.discountsAndRebates(contador).discountReason) Then
                        nodoBisnieto = doc.CreateNode(XmlNodeType.Element, "DiscountReason", "")
                        nodoBisnieto.InnerText = objInvoiceItem.discountsAndRebates(contador).discountReason
                        nodoNieto.AppendChild(nodoBisnieto)
                    End If
                    If Not (IsNothing(objInvoiceItem.discountsAndRebates(contador).discountRate)) Then
                        nodoBisnieto = doc.CreateNode(XmlNodeType.Element, "DiscountRate", "")
                        nodoBisnieto.InnerText = rellenarDecimales(redondearHaciaAbajo(dbl2Str(objInvoiceItem.discountsAndRebates(contador).discountRate.value, "."), 4), 4, ".")
                        nodoNieto.AppendChild(nodoBisnieto)
                    End If
                    If Not IsNothing(objInvoiceItem.discountsAndRebates(contador).discountAmount) Then
                        nodoBisnieto = doc.CreateNode(XmlNodeType.Element, "DiscountAmount", "")
                        nodoBisnieto.InnerText = rellenarDecimales(redondearHaciaAbajo(dbl2Str(objInvoiceItem.discountsAndRebates(contador).discountAmount.value, "."), 6), 6, ".")
                        nodoNieto.AppendChild(nodoBisnieto)
                    End If
                End If
                contador = contador + 1
            End While
        End If
        If Not IsNothing(objInvoiceItem.charges) Then
            nodoHijo = doc.CreateNode(XmlNodeType.Element, "Charges", "")
            nodoInvoiceItem.AppendChild(nodoHijo)
            contador = 0
            While contador < objInvoiceItem.charges.Count
                If Not IsNothing(objInvoiceItem.charges(contador)) Then
                    nodoNieto = doc.CreateNode(XmlNodeType.Element, "Charge", "")
                    nodoHijo.AppendChild(nodoNieto)
                    If Not IsNothing(objInvoiceItem.charges(contador).chargeReason) Then
                        nodoBisnieto = doc.CreateNode(XmlNodeType.Element, "ChargeReason", "")
                        nodoBisnieto.InnerText = objInvoiceItem.charges(contador).chargeReason
                        nodoNieto.AppendChild(nodoBisnieto)
                    End If
                    If Not (IsNothing(objInvoiceItem.charges(contador).chargeRate)) Then
                        nodoBisnieto = doc.CreateNode(XmlNodeType.Element, "ChargeRate", "")
                        nodoBisnieto.InnerText = rellenarDecimales(redondearHaciaAbajo(dbl2Str(objInvoiceItem.charges(contador).chargeRate.value, "."), 4), 4, ".")
                        nodoNieto.AppendChild(nodoBisnieto)
                    End If
                    If Not IsNothing(objInvoiceItem.charges(contador).chargeAmount) Then
                        nodoBisnieto = doc.CreateNode(XmlNodeType.Element, "ChargeAmount", "")
                        nodoBisnieto.InnerText = rellenarDecimales(redondearHaciaAbajo(dbl2Str(objInvoiceItem.charges(contador).chargeAmount.value, "."), 6), 6, ".")
                        nodoNieto.AppendChild(nodoBisnieto)
                    End If
                End If
                contador = contador + 1
            End While
        End If
        If Not IsNothing(objInvoiceItem.grossAmount) Then
            nodoHijo = doc.CreateNode(XmlNodeType.Element, "GrossAmount", "")
            nodoHijo.InnerText = rellenarDecimales(redondearHaciaAbajo(dbl2Str(objInvoiceItem.grossAmount.value, "."), 6), 6, ".")
            nodoInvoiceItem.AppendChild(nodoHijo)
        End If
        If Not (IsNothing(objInvoiceItem.taxesWithheld)) Then
            nodoHijo = doc.CreateNode(XmlNodeType.Element, "TaxesWithheld", "")
            nodoInvoiceItem.AppendChild(nodoHijo)
            Me.invoiceItemTaxesWithheldObj2XML(objInvoiceItem.taxesWithheld, nodoHijo, doc)
        End If
        If Not IsNothing(objInvoiceItem.taxesOutputs) Then
            nodoHijo = doc.CreateNode(XmlNodeType.Element, "TaxesOutputs", "")
            nodoInvoiceItem.AppendChild(nodoHijo)
            Me.invoiceItemTaxesOutputsObj2XML(objInvoiceItem.taxesOutputs, nodoHijo, doc)
        End If
        If Not (IsNothing(objInvoiceItem.specialTaxableEvent)) Then
            nodoHijo = doc.CreateNode(XmlNodeType.Element, "SpecialTaxableEvent", "")
            nodoInvoiceItem.AppendChild(nodoHijo)
            If Not IsNothing(objInvoiceItem.specialTaxableEvent.specialTaxableEventCode) Then
                nodoNieto = doc.CreateNode(XmlNodeType.Element, "SpecialTaxableEventCode", "")
                nodoNieto.InnerText = objInvoiceItem.specialTaxableEvent.specialTaxableEventCode.value
                nodoHijo.AppendChild(nodoNieto)
            End If
            If Not IsNothing(objInvoiceItem.specialTaxableEvent.specialTaxableEventReason) Then
                nodoNieto = doc.CreateNode(XmlNodeType.Element, "SpecialTaxableEventReason", "")
                nodoNieto.InnerText = objInvoiceItem.specialTaxableEvent.specialTaxableEventReason
                nodoHijo.AppendChild(nodoNieto)
            End If
        End If
        If Not (IsNothing(objInvoiceItem.articleCode)) Then
            nodoHijo = doc.CreateNode(XmlNodeType.Element, "ArticleCode", "")
            nodoHijo.InnerText = objInvoiceItem.articleCode
            nodoInvoiceItem.AppendChild(nodoHijo)
        End If
    End Sub
    ''' <summary>
    ''' Imprime un vector de objetos TaxWithheldType en una estructura de nodos XML siguiendo el estándar Facturae v3.2 (para impuestos a nivel de item)
    ''' </summary>
    ''' <param name="objInvoiceItemTaxesWithheld">Vector de objetos a imprimir</param>
    ''' <param name="nodoInvoiceItemTaxesWithheld">Nodo raíz sobre el cual se imprime el vector de objetos</param>
    ''' <param name="doc">Objeto de un documento XML el cual contiene una Facturae v3.2</param>
    ''' <remarks>Llamado desde: CFacturae.invoiceItemObj2XML
    ''' Tiempo máximo: Depende del número de elementos que tengan los vectores de longitud variable tales como
    ''' el número de impuestos retenidos.
    ''' Revisado por: auv. 25/05/2012</remarks>
    Private Sub invoiceItemTaxesWithheldObj2XML(ByRef objInvoiceItemTaxesWithheld() As TaxWithheldType, ByRef nodoInvoiceItemTaxesWithheld As XmlNode, ByRef doc As XmlDocument)
        Dim nodoHijo As XmlNode = Nothing
        Dim nodoNieto As XmlNode = Nothing
        Dim nodoBisnieto As XmlNode = Nothing
        Dim contador As Integer = Nothing

        contador = 0
        While contador < objInvoiceItemTaxesWithheld.Count
            If Not IsNothing(objInvoiceItemTaxesWithheld(contador)) Then
                nodoHijo = doc.CreateNode(XmlNodeType.Element, "Tax", "")
                nodoInvoiceItemTaxesWithheld.AppendChild(nodoHijo)
                If Not IsNothing(objInvoiceItemTaxesWithheld(contador).taxTypeCode) Then
                    nodoNieto = doc.CreateNode(XmlNodeType.Element, "TaxTypeCode", "")
                    nodoNieto.InnerText = objInvoiceItemTaxesWithheld(contador).taxTypeCode.value
                    nodoHijo.AppendChild(nodoNieto)
                End If
                If Not IsNothing(objInvoiceItemTaxesWithheld(contador).taxRate) Then
                    nodoNieto = doc.CreateNode(XmlNodeType.Element, "TaxRate", "")
                    nodoNieto.InnerText = rellenarDecimales(redondearHaciaAbajo(dbl2Str(objInvoiceItemTaxesWithheld(contador).taxRate.value, "."), 2), 2, ".")
                    nodoHijo.AppendChild(nodoNieto)
                End If
                If Not IsNothing(objInvoiceItemTaxesWithheld(contador).taxableBase) Then
                    nodoNieto = doc.CreateNode(XmlNodeType.Element, "TaxableBase", "")
                    nodoHijo.AppendChild(nodoNieto)
                    If Not IsNothing(objInvoiceItemTaxesWithheld(contador).taxableBase.totalAmount) Then
                        nodoBisnieto = doc.CreateNode(XmlNodeType.Element, "TotalAmount", "")
                        nodoBisnieto.InnerText = rellenarDecimales(redondearHaciaAbajo(dbl2Str(objInvoiceItemTaxesWithheld(contador).taxableBase.totalAmount.value, "."), 2), 2, ".")
                        nodoNieto.AppendChild(nodoBisnieto)
                    End If
                    If Not (IsNothing(objInvoiceItemTaxesWithheld(contador).taxableBase.equivalentInEuros)) Then
                        nodoBisnieto = doc.CreateNode(XmlNodeType.Element, "EquivalentInEuros", "")
                        nodoBisnieto.InnerText = rellenarDecimales(redondearHaciaAbajo(dbl2Str(objInvoiceItemTaxesWithheld(contador).taxableBase.equivalentInEuros.value, "."), 2), 2, ".")
                        nodoNieto.AppendChild(nodoBisnieto)
                    End If
                End If
                If Not (IsNothing(objInvoiceItemTaxesWithheld(contador).taxAmount)) Then
                    nodoNieto = doc.CreateNode(XmlNodeType.Element, "TaxAmount", "")
                    nodoHijo.AppendChild(nodoNieto)
                    If Not IsNothing(objInvoiceItemTaxesWithheld(contador).taxAmount.totalAmount) Then
                        nodoBisnieto = doc.CreateNode(XmlNodeType.Element, "TotalAmount", "")
                        nodoBisnieto.InnerText = rellenarDecimales(redondearHaciaAbajo(dbl2Str(objInvoiceItemTaxesWithheld(contador).taxAmount.totalAmount.value, "."), 2), 2, ".")
                        nodoNieto.AppendChild(nodoBisnieto)
                    End If
                    If Not (IsNothing(objInvoiceItemTaxesWithheld(contador).taxAmount.equivalentInEuros)) Then
                        nodoBisnieto = doc.CreateNode(XmlNodeType.Element, "EquivalentInEuros", "")
                        nodoBisnieto.InnerText = rellenarDecimales(redondearHaciaAbajo(dbl2Str(objInvoiceItemTaxesWithheld(contador).taxAmount.equivalentInEuros.value, "."), 2), 2, ".")
                        nodoNieto.AppendChild(nodoBisnieto)
                    End If
                End If
            End If
            contador = contador + 1
        End While
    End Sub
    ''' <summary>
    ''' Imprime un vector de objetos TaxOutputType en una estructura de nodos XML siguiendo el estándar Facturae v3.2 (para impuestos a nivel de item)
    ''' </summary>
    ''' <param name="objInvoiceItemTaxesOutputs">Vector de objetos a imprimir</param>
    ''' <param name="nodoInvoiceItemTaxesOutputs">Nodo raíz sobre el cual se imprime el vector de objetos</param>
    ''' <param name="doc">Objeto de un documento XML el cual contiene una Facturae v3.2</param>
    ''' <remarks>Llamado desde: CFacturae.invoiceItemObj2XML
    ''' Tiempo máximo: Depende del número de elementos que tengan los vectores de longitud variable tales como
    ''' el número de impuestos repercutidos.
    ''' Revisado por: auv. 25/05/2012</remarks>
    Private Sub invoiceItemTaxesOutputsObj2XML(ByRef objInvoiceItemTaxesOutputs() As TaxOutputType, ByRef nodoInvoiceItemTaxesOutputs As XmlNode, ByRef doc As XmlDocument)
        Dim nodoHijo As XmlNode = Nothing
        Dim nodoNieto As XmlNode = Nothing
        Dim nodoBisnieto As XmlNode = Nothing
        Dim contador As Integer = Nothing

        contador = 0
        While contador < objInvoiceItemTaxesOutputs.Count
            If Not IsNothing(objInvoiceItemTaxesOutputs(contador)) Then
                nodoHijo = doc.CreateNode(XmlNodeType.Element, "Tax", "")
                nodoInvoiceItemTaxesOutputs.AppendChild(nodoHijo)
                If Not IsNothing(objInvoiceItemTaxesOutputs(contador).taxTypeCode) Then
                    nodoNieto = doc.CreateNode(XmlNodeType.Element, "TaxTypeCode", "")
                    nodoNieto.InnerText = objInvoiceItemTaxesOutputs(contador).taxTypeCode.value
                    nodoHijo.AppendChild(nodoNieto)
                End If
                If Not IsNothing(objInvoiceItemTaxesOutputs(contador).taxRate) Then
                    nodoNieto = doc.CreateNode(XmlNodeType.Element, "TaxRate", "")
                    nodoNieto.InnerText = rellenarDecimales(redondearHaciaAbajo(dbl2Str(objInvoiceItemTaxesOutputs(contador).taxRate.value, "."), 2), 2, ".")
                    nodoHijo.AppendChild(nodoNieto)
                End If
                If Not IsNothing(objInvoiceItemTaxesOutputs(contador).taxableBase) Then
                    nodoNieto = doc.CreateNode(XmlNodeType.Element, "TaxableBase", "")
                    nodoHijo.AppendChild(nodoNieto)
                    If Not IsNothing(objInvoiceItemTaxesOutputs(contador).taxableBase.totalAmount) Then
                        nodoBisnieto = doc.CreateNode(XmlNodeType.Element, "TotalAmount", "")
                        nodoBisnieto.InnerText = rellenarDecimales(redondearHaciaAbajo(dbl2Str(objInvoiceItemTaxesOutputs(contador).taxableBase.totalAmount.value, "."), 2), 2, ".")
                        nodoNieto.AppendChild(nodoBisnieto)
                    End If
                    If Not (IsNothing(objInvoiceItemTaxesOutputs(contador).taxableBase.equivalentInEuros)) Then
                        nodoBisnieto = doc.CreateNode(XmlNodeType.Element, "EquivalentInEuros", "")
                        nodoBisnieto.InnerText = rellenarDecimales(redondearHaciaAbajo(dbl2Str(objInvoiceItemTaxesOutputs(contador).taxableBase.equivalentInEuros.value, "."), 2), 2, ".")
                        nodoNieto.AppendChild(nodoBisnieto)
                    End If
                End If
                If Not (IsNothing(objInvoiceItemTaxesOutputs(contador).taxAmount)) Then
                    nodoNieto = doc.CreateNode(XmlNodeType.Element, "TaxAmount", "")
                    nodoHijo.AppendChild(nodoNieto)
                    If Not IsNothing(objInvoiceItemTaxesOutputs(contador).taxAmount.totalAmount) Then
                        nodoBisnieto = doc.CreateNode(XmlNodeType.Element, "TotalAmount", "")
                        nodoBisnieto.InnerText = rellenarDecimales(redondearHaciaAbajo(dbl2Str(objInvoiceItemTaxesOutputs(contador).taxAmount.totalAmount.value, "."), 2), 2, ".")
                        nodoNieto.AppendChild(nodoBisnieto)
                    End If
                    If Not (IsNothing(objInvoiceItemTaxesOutputs(contador).taxAmount.equivalentInEuros)) Then
                        nodoBisnieto = doc.CreateNode(XmlNodeType.Element, "EquivalentInEuros", "")
                        nodoBisnieto.InnerText = rellenarDecimales(redondearHaciaAbajo(dbl2Str(objInvoiceItemTaxesOutputs(contador).taxAmount.equivalentInEuros.value, "."), 2), 2, ".")
                        nodoNieto.AppendChild(nodoBisnieto)
                    End If
                End If
                If Not (IsNothing(objInvoiceItemTaxesOutputs(contador).equivalenceSurcharge)) Then
                    nodoNieto = doc.CreateNode(XmlNodeType.Element, "EquivalenceSurcharge", "")
                    nodoNieto.InnerText = rellenarDecimales(redondearHaciaAbajo(dbl2Str(objInvoiceItemTaxesOutputs(contador).equivalenceSurcharge.value, "."), 2), 2, ".")
                    nodoHijo.AppendChild(nodoNieto)
                End If
                If Not (IsNothing(objInvoiceItemTaxesOutputs(contador).equivalenceSurchargeAmount)) Then
                    nodoNieto = doc.CreateNode(XmlNodeType.Element, "EquivalenceSurchargeAmount", "")
                    nodoHijo.AppendChild(nodoNieto)
                    If Not IsNothing(objInvoiceItemTaxesOutputs(contador).equivalenceSurchargeAmount.totalAmount) Then
                        nodoBisnieto = doc.CreateNode(XmlNodeType.Element, "TotalAmount", "")
                        nodoBisnieto.InnerText = rellenarDecimales(redondearHaciaAbajo(dbl2Str(objInvoiceItemTaxesOutputs(contador).equivalenceSurchargeAmount.totalAmount.value, "."), 2), 2, ".")
                        nodoNieto.AppendChild(nodoBisnieto)
                    End If
                    If Not (IsNothing(objInvoiceItemTaxesOutputs(contador).equivalenceSurchargeAmount.equivalentInEuros)) Then
                        nodoBisnieto = doc.CreateNode(XmlNodeType.Element, "EquivalentInEuros", "")
                        nodoBisnieto.InnerText = rellenarDecimales(redondearHaciaAbajo(dbl2Str(objInvoiceItemTaxesOutputs(contador).equivalenceSurchargeAmount.equivalentInEuros.value, "."), 2), 2, ".")
                        nodoNieto.AppendChild(nodoBisnieto)
                    End If
                End If
            End If
            contador = contador + 1
        End While
    End Sub
    ''' <summary>
    ''' Imprime un objeto AdditionalDataType en una estructura de nodos XML siguiendo el estándar Facturae v3.2
    ''' </summary>
    ''' <param name="objInvoiceAdditionalData">Objeto a imprimir</param>
    ''' <param name="nodoAdditionalData">Nodo raíz sobre el cual se imprime el objeto</param>
    ''' <param name="doc">Objeto de un documento XML el cual contiene una Facturae v3.2</param>
    ''' <remarks>Llamado desde: CFacturae.invoicesObj2XML
    ''' Tiempo máximo:menor a 0,1.
    ''' Revisado por: auv. 05/06/2012</remarks>
    Private Sub invoiceAdditionalDataObj2XML(ByRef objInvoiceAdditionalData As AdditionalDataType, ByRef nodoAdditionalData As XmlNode, ByRef doc As XmlDocument)
        Dim nodoHijo As XmlNode = Nothing
        Dim nodoNieto As XmlNode = Nothing

        If Not IsNothing(objInvoiceAdditionalData.invoiceAdditionalInformation) Then
            nodoHijo = doc.CreateNode(XmlNodeType.Element, "InvoiceAdditionalInformation", "")
            nodoHijo.InnerText = objInvoiceAdditionalData.invoiceAdditionalInformation
            nodoAdditionalData.AppendChild(nodoHijo)
        End If
    End Sub
#End Region
#Region "XML2OBJ"
    ''' <summary>
    ''' Transformación de un documento XML Facturae v3.2 un nodo Facturae a un objeto FacturaeType
    ''' </summary>
    ''' <remarks>Llamado desde: CFacturae.New(FSNDatabaseServer.Root,Boolean,XMLDocument)
    ''' Tiempo máximo: Depende del número de elementos que tengan los vectores de longitud variable tales como
    ''' el número de items, descuentos, costes, impuestos repercutidos, impuestos retenidos, etc...
    ''' Revisado por: auv. ¿?/05/2012</remarks>
    Private Sub facturaeXML2Obj(ByRef doc As XmlDocument)
        Dim contadorHijos As Integer = Nothing
        Dim contadorNietos As Integer = Nothing
        Dim nodoHijo As XmlNode = Nothing
        Dim nodoNieto As XmlNode = Nothing

        contadorHijos = 0
        While contadorHijos < doc.ChildNodes.Count
            nodoHijo = doc.ChildNodes(contadorHijos)
            If nodoHijo.Name = "Facturae" Then
                contadorNietos = 0
                While contadorNietos < doc.ChildNodes(contadorHijos).ChildNodes.Count
                    nodoNieto = doc.ChildNodes(contadorHijos).ChildNodes(contadorNietos)
                    If nodoNieto.Name = "FileHeader" Then
                        Me.fileHeader = New FileHeaderType
                        Me.fileHeaderXML2Obj(Me.fileHeader, nodoNieto)
                    End If
                    If nodoNieto.Name = "Parties" Then
                        Me.parties = New PartiesType
                        Me.partiesXML2Obj(Me.parties, nodoNieto)
                    End If
                    If nodoNieto.Name = "Invoices" Then
                        Me.invoicesXML2Obj(Me.invoices, nodoNieto)
                    End If
                    contadorNietos = contadorNietos + 1
                End While
            End If
            contadorHijos = contadorHijos + 1
        End While
    End Sub
    ''' <summary>
    ''' Transformación de un nodo raiz FileHeader con su estructura interna a un objeto FileHeaderType
    ''' </summary>
    ''' <remarks>Llamado desde: CFacturae.
    ''' Tiempo máximo: menor a 0,1
    ''' Revisado por: auv. ¿?/05/2012</remarks>
    Private Sub fileHeaderXML2Obj(ByRef objFileHeader As FileHeaderType, ByRef nodoFileHeader As XmlNode)
        Dim contadorHijos As Integer = Nothing
        Dim nodoHijo As XmlNode = Nothing

        contadorHijos = 0
        While contadorHijos < nodoFileHeader.ChildNodes.Count
            nodoHijo = nodoFileHeader.ChildNodes(contadorHijos)
            If nodoHijo.Name = "SchemaVersion" Then
                objFileHeader.schemaVersion = New SchemaVersionType
                objFileHeader.schemaVersion.value = nodoHijo.InnerText
            End If
            If nodoHijo.Name = "Modality" Then
                objFileHeader.modality = New ModalityType
                objFileHeader.modality.value = nodoHijo.InnerText
            End If
            If nodoHijo.Name = "InvoiceIssuerType" Then
                objFileHeader.invoiceIssuerType = New InvoiceIssuerTypeType
                objFileHeader.invoiceIssuerType.value = nodoHijo.InnerText
            End If
            If nodoHijo.Name = "Batch" Then
                objFileHeader.batch = New BatchType
                Me.batchXML2Obj(objFileHeader.batch, nodoHijo)
            End If
            contadorHijos = contadorHijos + 1
        End While
    End Sub
    ''' <summary>
    ''' Transformación de un nodo raiz Batch con su estructura interna a un objeto BatchType
    ''' </summary>
    ''' <remarks>Llamado desde: CFacturae.fileHeaderXML2Obj
    ''' Tiempo máximo: menor a 0,1
    ''' Revisado por: auv. ¿?/05/2012</remarks>
    Private Sub batchXML2Obj(ByRef objBatch As BatchType, ByRef nodoBatch As XmlNode)
        Dim contadorHijos As Integer = Nothing
        Dim contadorNietos As Integer = Nothing
        Dim nodoHijo As XmlNode = Nothing
        Dim nodoNieto As XmlNode = Nothing

        contadorHijos = 0
        While contadorHijos < nodoBatch.ChildNodes.Count
            nodoHijo = nodoBatch.ChildNodes(contadorHijos)
            If nodoHijo.Name = "BatchIdentifier" Then
                objBatch.batchIdentifier = nodoHijo.InnerText
            End If
            If nodoHijo.Name = "InvoicesCount" Then
                objBatch.invoicesCount = CLng(nodoHijo.InnerText)
            End If
            If nodoHijo.Name = "TotalInvoicesAmount" Then
                objBatch.totalInvoicesAmount = New AmountType
                contadorNietos = 0
                While contadorNietos < nodoHijo.ChildNodes.Count
                    nodoNieto = nodoHijo.ChildNodes(contadorNietos)
                    If nodoNieto.Name = "TotalAmount" Then
                        objBatch.totalInvoicesAmount.totalAmount = New DoubleTwoDecimalType
                        objBatch.totalInvoicesAmount.totalAmount.value = str2Dbl(nodoNieto.InnerText)
                    End If
                    If nodoNieto.Name = "EquivalentInEuros" Then
                        objBatch.totalInvoicesAmount.equivalentInEuros = New DoubleTwoDecimalType
                        objBatch.totalInvoicesAmount.equivalentInEuros.value = str2Dbl(nodoNieto.InnerText)
                    End If
                    contadorNietos = contadorNietos + 1
                End While
            End If
            If nodoHijo.Name = "TotalOutstandingAmount" Then
                objBatch.totalOutstandingAmount = New AmountType
                contadorNietos = 0
                While contadorNietos < nodoHijo.ChildNodes.Count
                    nodoNieto = nodoHijo.ChildNodes(contadorNietos)
                    If nodoNieto.Name = "TotalAmount" Then
                        objBatch.totalOutstandingAmount.totalAmount = New DoubleTwoDecimalType
                        objBatch.totalOutstandingAmount.totalAmount.value = str2Dbl(nodoNieto.InnerText)
                    End If
                    If nodoNieto.Name = "EquivalentInEuros" Then
                        objBatch.totalOutstandingAmount.equivalentInEuros = New DoubleTwoDecimalType
                        objBatch.totalOutstandingAmount.equivalentInEuros.value = str2Dbl(nodoNieto.InnerText)
                    End If
                    contadorNietos = contadorNietos + 1
                End While
            End If
            If nodoHijo.Name = "TotalExecutableAmount" Then
                objBatch.totalExecutableAmount = New AmountType
                contadorNietos = 0
                While contadorNietos < nodoHijo.ChildNodes.Count
                    nodoNieto = nodoHijo.ChildNodes(contadorNietos)
                    If nodoNieto.Name = "TotalAmount" Then
                        objBatch.totalExecutableAmount.totalAmount = New DoubleTwoDecimalType
                        objBatch.totalExecutableAmount.totalAmount.value = str2Dbl(nodoNieto.InnerText)
                    End If
                    If nodoNieto.Name = "EquivalentInEuros" Then
                        objBatch.totalExecutableAmount.equivalentInEuros = New DoubleTwoDecimalType
                        objBatch.totalExecutableAmount.equivalentInEuros.value = str2Dbl(nodoNieto.InnerText)
                    End If
                    contadorNietos = contadorNietos + 1
                End While
            End If
            If nodoHijo.Name = "InvoiceCurrencyCode" Then
                objBatch.invoiceCurrencyCode = New CurrencyCodeType
                objBatch.invoiceCurrencyCode.value = nodoHijo.InnerText
            End If
            contadorHijos = contadorHijos + 1
        End While
    End Sub
    ''' <summary>
    ''' Transformación de un nodo raiz Parties con su estructura interna a un objeto PartiesType
    ''' </summary>
    ''' <remarks>Llamado desde: CFacturae.facturaeXML2Obj
    ''' Tiempo máximo: menor a 0,1
    ''' Revisado por: auv. ¿?/05/2012</remarks>
    Private Sub partiesXML2Obj(ByRef objParties As PartiesType, ByRef nodoParties As XmlNode)
        Dim contadorHijos As Integer = Nothing
        Dim nodoHijo As XmlNode = Nothing

        contadorHijos = 0
        While contadorHijos < nodoParties.ChildNodes.Count
            nodoHijo = nodoParties.ChildNodes(contadorHijos)
            If nodoHijo.Name = "SellerParty" Then
                objParties.sellerParty = New BusinessType
                Me.sellerPartyXML2Obj(objParties.sellerParty, nodoHijo)
            End If
            If nodoHijo.Name = "BuyerParty" Then
                objParties.buyerParty = New BusinessType
                Me.buyerPartyXML2Obj(objParties.buyerParty, nodoHijo)
            End If
            contadorHijos = contadorHijos + 1
        End While
    End Sub
    ''' <summary>
    ''' Transformación de un nodo raiz SellerParty con su estructura interna a un objeto SellerPartyType
    ''' </summary>
    ''' <remarks>Llamado desde: CFacturae.partiesXML2Obj
    ''' Tiempo máximo: menor a 0,1
    ''' Revisado por: auv. ¿?/05/2012</remarks>
    Private Sub sellerPartyXML2Obj(ByRef objSellerParty As BusinessType, ByRef nodoSellerParty As XmlNode)
        Dim contadorHijos As Integer = Nothing
        Dim contadorNietos As Integer = Nothing
        Dim contadorBisnietos As Integer = Nothing
        Dim nodoHijo As XmlNode = Nothing
        Dim nodoNieto As XmlNode = Nothing
        Dim nodoBisnieto As XmlNode = Nothing

        contadorHijos = 0
        While contadorHijos < nodoSellerParty.ChildNodes.Count
            nodoHijo = nodoSellerParty.ChildNodes(contadorHijos)
            If nodoHijo.Name = "TaxIdentification" Then
                objSellerParty.taxIdentification = New TaxIdentificationType
                contadorNietos = 0
                While contadorNietos < nodoHijo.ChildNodes.Count
                    nodoNieto = nodoHijo.ChildNodes(contadorNietos)
                    If nodoNieto.Name = "PersonTypeCode" Then
                        objSellerParty.taxIdentification.personTypeCode = New PersonTypeCodeType
                        objSellerParty.taxIdentification.personTypeCode.value = nodoNieto.InnerText
                    End If
                    If nodoNieto.Name = "ResidenceTypeCode" Then
                        objSellerParty.taxIdentification.residenceTypeCode = New ResidenceTypeCodeType
                        objSellerParty.taxIdentification.residenceTypeCode.value = nodoNieto.InnerText
                    End If
                    If nodoNieto.Name = "TaxIdentificationNumber" Then
                        objSellerParty.taxIdentification.taxIdentificationNumber = nodoNieto.InnerText
                    End If
                    contadorNietos = contadorNietos + 1
                End While
            End If
            If nodoHijo.Name = "LegalEntity" Then
                objSellerParty.legalEntity = New LegalEntityType
                contadorNietos = 0
                While contadorNietos < nodoHijo.ChildNodes.Count
                    nodoNieto = nodoHijo.ChildNodes(contadorNietos)
                    If nodoNieto.Name = "CorporateName" Then
                        objSellerParty.legalEntity.corporateName = nodoNieto.InnerText
                    End If
                    If nodoNieto.Name = "AddressInSpain" Then
                        objSellerParty.legalEntity.addressInSpain = New AddressType
                        contadorBisnietos = 0
                        While contadorBisnietos < nodoNieto.ChildNodes.Count
                            nodoBisnieto = nodoNieto.ChildNodes(contadorBisnietos)
                            If nodoBisnieto.Name = "Address" Then
                                objSellerParty.legalEntity.addressInSpain.address = nodoBisnieto.InnerText
                            End If
                            If nodoBisnieto.Name = "PostCode" Then
                                objSellerParty.legalEntity.addressInSpain.postCode = New PostCodeType
                                objSellerParty.legalEntity.addressInSpain.postCode.value = nodoBisnieto.InnerText
                            End If
                            If nodoBisnieto.Name = "Town" Then
                                objSellerParty.legalEntity.addressInSpain.town = nodoBisnieto.InnerText
                            End If
                            If nodoBisnieto.Name = "Province" Then
                                objSellerParty.legalEntity.addressInSpain.province = nodoBisnieto.InnerText
                            End If
                            If nodoBisnieto.Name = "CountryCode" Then
                                objSellerParty.legalEntity.addressInSpain.countryCode = New CountryType
                                objSellerParty.legalEntity.addressInSpain.countryCode.value = nodoBisnieto.InnerText
                            End If
                            contadorBisnietos = contadorBisnietos + 1
                        End While
                    End If
                    If nodoNieto.Name = "OverseasAddress" Then
                        objSellerParty.legalEntity.overseasAddress = New OverseasAddressType
                        contadorBisnietos = 0
                        While contadorBisnietos < nodoNieto.ChildNodes.Count
                            nodoBisnieto = nodoNieto.ChildNodes(contadorBisnietos)
                            If nodoBisnieto.Name = "Address" Then
                                objSellerParty.legalEntity.overseasAddress.address = nodoBisnieto.InnerText
                            End If
                            If nodoBisnieto.Name = "PostCodeAndTown" Then
                                objSellerParty.legalEntity.overseasAddress.postCodeAndTown = nodoBisnieto.InnerText
                            End If
                            If nodoBisnieto.Name = "Province" Then
                                objSellerParty.legalEntity.overseasAddress.province = nodoBisnieto.InnerText
                            End If
                            If nodoBisnieto.Name = "CountryCode" Then
                                objSellerParty.legalEntity.overseasAddress.countryCode = New CountryType
                                objSellerParty.legalEntity.overseasAddress.countryCode.value = nodoBisnieto.InnerText
                            End If
                            contadorBisnietos = contadorBisnietos + 1
                        End While
                    End If
                    If nodoNieto.Name = "ContactDetails" Then
                        objSellerParty.legalEntity.contactDetails = New ContactDetailsType
                        contadorBisnietos = 0
                        While contadorBisnietos < nodoNieto.ChildNodes.Count
                            nodoBisnieto = nodoNieto.ChildNodes(contadorBisnietos)
                            If nodoBisnieto.Name = "Telephone" Then
                                objSellerParty.legalEntity.contactDetails.telephone = nodoBisnieto.InnerText
                            End If
                            If nodoBisnieto.Name = "TeleFax" Then
                                objSellerParty.legalEntity.contactDetails.teleFax = nodoBisnieto.InnerText
                            End If
                            If nodoBisnieto.Name = "ElectronicMail" Then
                                objSellerParty.legalEntity.contactDetails.electronicMail = nodoBisnieto.InnerText
                            End If
                            If nodoBisnieto.Name = "ContactPersons" Then
                                objSellerParty.legalEntity.contactDetails.contactPersons = nodoBisnieto.InnerText
                            End If
                            contadorBisnietos = contadorBisnietos + 1
                        End While
                    End If
                    contadorNietos = contadorNietos + 1
                End While
            End If
            contadorHijos = contadorHijos + 1
        End While
    End Sub
    ''' <summary>
    ''' Transformación de un nodo raiz BuyerParty con su estructura interna a un objeto BuyerPartyType
    ''' </summary>
    ''' <remarks>Llamado desde: CFacturae.partiesXML2Obj
    ''' Tiempo máximo: menor a 0,1
    ''' Revisado por: auv. ¿?/05/2012</remarks>
    Private Sub buyerPartyXML2Obj(ByRef objBuyerParty As BusinessType, ByRef nodoBuyerParty As XmlNode)
        Dim contadorHijos As Integer = Nothing
        Dim contadorNietos As Integer = Nothing
        Dim contadorBisnietos As Integer = Nothing
        Dim nodoHijo As XmlNode = Nothing
        Dim nodoNieto As XmlNode = Nothing
        Dim nodoBisnieto As XmlNode = Nothing

        contadorHijos = 0
        While contadorHijos < nodoBuyerParty.ChildNodes.Count
            nodoHijo = nodoBuyerParty.ChildNodes(contadorHijos)
            If nodoHijo.Name = "TaxIdentification" Then
                objBuyerParty.taxIdentification = New TaxIdentificationType
                contadorNietos = 0
                While contadorNietos < nodoHijo.ChildNodes.Count
                    nodoNieto = nodoHijo.ChildNodes(contadorNietos)
                    If nodoNieto.Name = "PersonTypeCode" Then
                        objBuyerParty.taxIdentification.personTypeCode = New PersonTypeCodeType
                        objBuyerParty.taxIdentification.personTypeCode.value = nodoNieto.InnerText
                    End If
                    If nodoNieto.Name = "ResidenceTypeCode" Then
                        objBuyerParty.taxIdentification.residenceTypeCode = New ResidenceTypeCodeType
                        objBuyerParty.taxIdentification.residenceTypeCode.value = nodoNieto.InnerText
                    End If
                    If nodoNieto.Name = "TaxIdentificationNumber" Then
                        objBuyerParty.taxIdentification.taxIdentificationNumber = nodoNieto.InnerText
                    End If
                    contadorNietos = contadorNietos + 1
                End While
            End If
            If nodoHijo.Name = "LegalEntity" Then
                objBuyerParty.legalEntity = New LegalEntityType
                contadorNietos = 0
                While contadorNietos < nodoHijo.ChildNodes.Count
                    nodoNieto = nodoHijo.ChildNodes(contadorNietos)
                    If nodoNieto.Name = "CorporateName" Then
                        objBuyerParty.legalEntity.corporateName = nodoNieto.InnerText
                    End If
                    If nodoNieto.Name = "AddressInSpain" Then
                        objBuyerParty.legalEntity.addressInSpain = New AddressType
                        contadorBisnietos = 0
                        While contadorBisnietos < nodoNieto.ChildNodes.Count
                            nodoBisnieto = nodoNieto.ChildNodes(contadorBisnietos)
                            If nodoBisnieto.Name = "Address" Then
                                objBuyerParty.legalEntity.addressInSpain.address = nodoBisnieto.InnerText
                            End If
                            If nodoBisnieto.Name = "PostCode" Then
                                objBuyerParty.legalEntity.addressInSpain.postCode = New PostCodeType
                                objBuyerParty.legalEntity.addressInSpain.postCode.value = nodoBisnieto.InnerText
                            End If
                            If nodoBisnieto.Name = "Town" Then
                                objBuyerParty.legalEntity.addressInSpain.town = nodoBisnieto.InnerText
                            End If
                            If nodoBisnieto.Name = "Province" Then
                                objBuyerParty.legalEntity.addressInSpain.province = nodoBisnieto.InnerText
                            End If
                            If nodoBisnieto.Name = "CountryCode" Then
                                objBuyerParty.legalEntity.addressInSpain.countryCode = New CountryType
                                objBuyerParty.legalEntity.addressInSpain.countryCode.value = nodoBisnieto.InnerText
                            End If
                            contadorBisnietos = contadorBisnietos + 1
                        End While
                    End If
                    If nodoNieto.Name = "OverseasAddress" Then
                        objBuyerParty.legalEntity.overseasAddress = New OverseasAddressType
                        contadorBisnietos = 0
                        While contadorBisnietos < nodoNieto.ChildNodes.Count
                            nodoBisnieto = nodoNieto.ChildNodes(contadorBisnietos)
                            If nodoBisnieto.Name = "Address" Then
                                objBuyerParty.legalEntity.overseasAddress.address = nodoBisnieto.InnerText
                            End If
                            If nodoBisnieto.Name = "PostCodeAndTown" Then
                                objBuyerParty.legalEntity.overseasAddress.postCodeAndTown = nodoBisnieto.InnerText
                            End If
                            If nodoBisnieto.Name = "Province" Then
                                objBuyerParty.legalEntity.overseasAddress.province = nodoBisnieto.InnerText
                            End If
                            If nodoBisnieto.Name = "CountryCode" Then
                                objBuyerParty.legalEntity.overseasAddress.countryCode = New CountryType
                                objBuyerParty.legalEntity.overseasAddress.countryCode.value = nodoBisnieto.InnerText
                            End If
                            contadorBisnietos = contadorBisnietos + 1
                        End While
                    End If
                    contadorNietos = contadorNietos + 1
                End While
            End If
            contadorHijos = contadorHijos + 1
        End While
    End Sub
    ''' <summary>
    ''' Transformación de un nodo raiz Invoices con su estructura interna a un vector de objetos InvoiceType
    ''' </summary>
    ''' <remarks>Llamado desde: CFacturae.facturaeXML2Obj
    ''' Tiempo máximo: Depende del número de elementos que tengan los vectores de longitud variable tales como
    ''' el número de items, descuentos, costes, impuestos repercutidos, impuestos retenidos, etc...
    ''' Revisado por: auv. ¿?/05/2012</remarks>
    Private Sub invoicesXML2Obj(ByRef objInvoices() As InvoiceType, ByRef nodoInvoices As XmlNode)
        Dim contadorhijos As Integer = Nothing
        Dim nodoHijo As XmlNode = Nothing
        Dim contadorFacturas As Integer = Nothing

        objInvoices = New InvoiceType() {}

        contadorFacturas = 0
        contadorhijos = 0
        While contadorhijos < nodoInvoices.ChildNodes.Count
            nodoHijo = nodoInvoices.ChildNodes(contadorhijos)
            If nodoHijo.Name = "Invoice" Then
                ReDim Preserve objInvoices(0 To contadorFacturas)
                objInvoices(contadorFacturas) = New InvoiceType
                Me.invoiceXML2Obj(objInvoices(contadorFacturas), nodoHijo)
                contadorFacturas = contadorFacturas + 1
            End If
            contadorhijos = contadorhijos + 1
        End While
    End Sub
    ''' <summary>
    ''' Transformación de un nodo raiz Invoice con su estructura interna a un objeto InvoiceType
    ''' </summary>
    ''' <remarks>Llamado desde: CFacturae.invoicesXML2Obj
    ''' Tiempo máximo: Depende del número de elementos que tengan los vectores de longitud variable tales como
    ''' el número de items, descuentos, costes, impuestos repercutidos, impuestos retenidos, etc...
    ''' Revisado por: auv. ¿?/05/2012</remarks>
    Private Sub invoiceXML2Obj(ByRef objInvoice As InvoiceType, ByRef nodoInvoice As XmlNode)
        Dim contadorhijos As Integer = Nothing
        Dim nodoHijo As XmlNode = Nothing

        contadorhijos = 0
        While contadorhijos < nodoInvoice.ChildNodes.Count
            nodoHijo = nodoInvoice.ChildNodes(contadorhijos)
            If nodoHijo.Name = "InvoiceHeader" Then
                objInvoice.invoiceHeader = New InvoiceHeaderType
                Me.invoiceHeaderXML2Obj(objInvoice.invoiceHeader, nodoHijo)
            End If
            If nodoHijo.Name = "InvoiceIssueData" Then
                objInvoice.invoiceIssueData = New InvoiceIssueDataType
                Me.invoiceIssueDataXML2Obj(objInvoice.invoiceIssueData, nodoHijo)
            End If
            If nodoHijo.Name = "TaxesOutputs" Then
                Me.invoiceTaxesOutputsXML2Obj(objInvoice.taxesOutputs, nodoHijo)
            End If
            If nodoHijo.Name = "TaxesWithheld" Then
                Me.invoiceTaxesWithheldXML2Obj(objInvoice.taxesWithheld, nodoHijo)
            End If
            If nodoHijo.Name = "InvoiceTotals" Then
                objInvoice.invoiceTotals = New InvoiceTotalsType
                Me.invoiceTotalsXML2Obj(objInvoice.invoiceTotals, nodoHijo)
            End If
            If nodoHijo.Name = "Items" Then
                Me.invoiceItemsXML2Obj(objInvoice.items, nodoHijo)
            End If
            If nodoHijo.Name = "AdditionalData" Then
                objInvoice.additionalData = New AdditionalDataType
                Me.invoiceAdditionalDataXML2Obj(objInvoice.additionalData, nodoHijo)
            End If
            contadorhijos = contadorhijos + 1
        End While
    End Sub
    ''' <summary>
    ''' Transformación de un nodo raiz InvoiceHeader con su estructura interna a un objeto InvoiceHeaderType
    ''' </summary>
    ''' <remarks>Llamado desde: CFacturae.invoiceXML2Obj
    ''' Tiempo máximo: menor a 0,1
    ''' Revisado por: auv. ¿?/05/2012</remarks>
    Private Sub invoiceHeaderXML2Obj(ByRef objInvoiceHeader As InvoiceHeaderType, ByRef nodoInvoiceHeader As XmlNode)
        Dim contadorhijos As Integer = Nothing
        Dim nodoHijo As XmlNode = Nothing

        contadorhijos = 0
        While contadorhijos < nodoInvoiceHeader.ChildNodes.Count
            nodoHijo = nodoInvoiceHeader.ChildNodes(contadorhijos)
            If nodoHijo.Name = "InvoiceNumber" Then
                objInvoiceHeader.invoiceNumber = nodoHijo.InnerText
            End If
            If nodoHijo.Name = "InvoiceDocumentType" Then
                objInvoiceHeader.invoiceDocumentType = New InvoiceDocumentTypeType
                objInvoiceHeader.invoiceDocumentType.value = nodoHijo.InnerText
            End If
            If nodoHijo.Name = "InvoiceClass" Then
                objInvoiceHeader.invoiceClass = New InvoiceClassType
                objInvoiceHeader.invoiceClass.value = nodoHijo.InnerText
            End If
            If nodoHijo.Name = "Corrective" Then
                objInvoiceHeader.corrective = New CorrectiveType
                Me.invoiceCorrectiveXML2Obj(objInvoiceHeader.corrective, nodoHijo)
            End If
            contadorhijos = contadorhijos + 1
        End While
    End Sub
    ''' <summary>
    ''' Transformación de un nodo raiz Corrective con su estructura interna a un objeto CorrectiveType
    ''' </summary>
    ''' <remarks>Llamado desde: CFacturae.invoiceHeaderXML2Obj
    ''' Tiempo máximo: menor a 0,1
    ''' Revisado por: auv. ¿?/05/2012</remarks>
    Private Sub invoiceCorrectiveXML2Obj(ByRef objInvoiceCorrective As CorrectiveType, ByRef nodoCorrective As XmlNode)
        Dim contadorhijos As Integer = Nothing
        Dim nodoHijo As XmlNode = Nothing

        contadorhijos = 0
        While contadorhijos < nodoCorrective.ChildNodes.Count
            nodoHijo = nodoCorrective.ChildNodes(contadorhijos)
            If nodoHijo.Name = "InvoiceNumber" Then
                objInvoiceCorrective.invoiceNumber = nodoHijo.InnerText
            End If
            If nodoHijo.Name = "ReasonCode" Then
                objInvoiceCorrective.reasonCode = New ReasonCodeType
                objInvoiceCorrective.reasonCode.value = nodoHijo.InnerText
            End If
            If nodoHijo.Name = "ReasonDescription" Then
                objInvoiceCorrective.reasonDescription = New ReasonDescriptionType
                objInvoiceCorrective.reasonDescription.value = nodoHijo.InnerText
            End If
            If nodoHijo.Name = "TaxPeriod" Then
                objInvoiceCorrective.taxPeriod = New PeriodDatesType
                Me.invoiceCorrectiveTaxPeriodXML2Obj(objInvoiceCorrective.taxPeriod, nodoHijo)
            End If
            If nodoHijo.Name = "CorrectionMethod" Then
                objInvoiceCorrective.correctionMethod = New CorrectionMethodType
                objInvoiceCorrective.correctionMethod.value = nodoHijo.InnerText
            End If
            If nodoHijo.Name = "CorrectionMethodDescription" Then
                objInvoiceCorrective.correctionMethodDescription = New CorrectionMethodDescriptionType
                objInvoiceCorrective.correctionMethodDescription.value = nodoHijo.InnerText
            End If
            contadorhijos = contadorhijos + 1
        End While
    End Sub
    ''' <summary>
    ''' Transformación de un nodo raiz TaxPeriod con su estructura interna a un objeto PeriodDatesType (para nivel de albaranes)
    ''' </summary>
    ''' <remarks>Llamado desde: CFacturae.invoiceCorrectiveXML2Obj
    ''' Tiempo máximo: menor a 0,1
    ''' Revisado por: auv. ¿?/05/2012</remarks>
    Private Sub invoiceCorrectiveTaxPeriodXML2Obj(ByRef objInvoiceTaxPeriod As PeriodDatesType, ByRef nodoTaxPeriod As XmlNode)
        Dim contadorhijos As Integer = Nothing
        Dim nodoHijo As XmlNode = Nothing

        contadorhijos = 0
        While contadorhijos < nodoTaxPeriod.ChildNodes.Count
            nodoHijo = nodoTaxPeriod.ChildNodes(contadorhijos)
            If nodoHijo.Name = "StartDate" Then
                objInvoiceTaxPeriod.startDate = New Date
                objInvoiceTaxPeriod.startDate = Me.date_XMLStr2ObjId(nodoHijo.InnerText)
            End If
            If nodoHijo.Name = "EndDate" Then
                objInvoiceTaxPeriod.endDate = New Date
                objInvoiceTaxPeriod.endDate = Me.date_XMLStr2ObjId(nodoHijo.InnerText)
            End If
            contadorhijos = contadorhijos + 1
        End While
    End Sub
    ''' <summary>
    ''' Transformación de un nodo raiz InvoiceIssueData con su estructura interna a un objeto InvoiceIssueDataType
    ''' </summary>
    ''' <remarks>Llamado desde: CFacturae.invoiceXML2Obj
    ''' Tiempo máximo: menor a 0,1
    ''' Revisado por: auv. ¿?/05/2012</remarks>
    Private Sub invoiceIssueDataXML2Obj(ByRef objInvoiceIssueData As InvoiceIssueDataType, ByRef nodoInvoiceIssueData As XmlNode)
        Dim contadorhijos As Integer = Nothing
        Dim contadorNietos As Integer = Nothing
        Dim nodoHijo As XmlNode = Nothing
        Dim nodoNieto As XmlNode = Nothing

        contadorhijos = 0
        While contadorhijos < nodoInvoiceIssueData.ChildNodes.Count
            nodoHijo = nodoInvoiceIssueData.ChildNodes(contadorhijos)
            If nodoHijo.Name = "IssueDate" Then
                objInvoiceIssueData.issueDate = date_XMLStr2ObjId(nodoHijo.InnerText)
            End If
            If nodoHijo.Name = "InvoiceCurrencyCode" Then
                objInvoiceIssueData.invoiceCurrencyCode = New CurrencyCodeType
                objInvoiceIssueData.invoiceCurrencyCode.value = nodoHijo.InnerText
            End If
            If nodoHijo.Name = "ExchangeRateDetails" Then
                objInvoiceIssueData.exchangeRateDetails = New ExchangeRateDetailsType
                contadorNietos = 0
                While contadorNietos < nodoHijo.ChildNodes.Count
                    nodoNieto = nodoHijo.ChildNodes(contadorNietos)
                    If nodoNieto.Name = "ExchangeRate" Then
                        objInvoiceIssueData.exchangeRateDetails.exchangeRate = New DoubleTwoDecimalType
                        objInvoiceIssueData.exchangeRateDetails.exchangeRate.value = str2Dbl(nodoNieto.InnerText)
                    End If
                    If nodoNieto.Name = "ExchangeRateDate" Then
                        objInvoiceIssueData.exchangeRateDetails.exchangeRateDate = date_XMLStr2ObjId(nodoNieto.InnerText)
                    End If
                    contadorNietos = contadorNietos + 1
                End While
            End If
            If nodoHijo.Name = "TaxCurrencyCode" Then
                objInvoiceIssueData.taxCurrencyCode = New CurrencyCodeType
                objInvoiceIssueData.taxCurrencyCode.value = nodoHijo.InnerText
            End If
            If nodoHijo.Name = "LanguageName" Then
                objInvoiceIssueData.languageName = New LanguageCodeType
                objInvoiceIssueData.languageName.value = nodoHijo.InnerText
            End If
            contadorhijos = contadorhijos + 1
        End While
    End Sub
    ''' <summary>
    ''' Transformación de un nodo raiz TaxesOutputs con su estructura interna a un vector de objetos TaxOutputType (para nivel de factura)
    ''' </summary>
    ''' <remarks>Llamado desde: CFacturae.invoiceXML2Obj
    ''' Tiempo máximo: Depende del número de elementos que tengan los vectores de longitud variable tales como
    ''' el número de impuestos repercutidos.
    ''' Revisado por: auv. ¿?/05/2012</remarks>
    Private Sub invoiceTaxesOutputsXML2Obj(ByRef objInvoiceTaxesOutputs() As TaxOutputType, ByRef nodoInvoiceTaxesOutputs As XmlNode)
        Dim contadorhijos As Integer = Nothing
        Dim nodoHijo As XmlNode = Nothing
        Dim contadorImpuestosRepercutidos As Integer = Nothing

        objInvoiceTaxesOutputs = New TaxOutputType() {}

        contadorImpuestosRepercutidos = 0
        contadorhijos = 0
        While contadorhijos < nodoInvoiceTaxesOutputs.ChildNodes.Count
            nodoHijo = nodoInvoiceTaxesOutputs.ChildNodes(contadorhijos)
            If nodoHijo.Name = "Tax" Then
                ReDim Preserve objInvoiceTaxesOutputs(0 To contadorImpuestosRepercutidos)
                objInvoiceTaxesOutputs(contadorImpuestosRepercutidos) = New TaxOutputType
                Me.invoiceTaxOutputXML2Obj(objInvoiceTaxesOutputs(contadorImpuestosRepercutidos), nodoHijo)
                contadorImpuestosRepercutidos = contadorImpuestosRepercutidos + 1
            End If
            contadorhijos = contadorhijos + 1
        End While
    End Sub
    ''' <summary>
    ''' Transformación de un nodo raiz Tax con su estructura interna a un objeto TaxOutputType (para nivel de factura y para impuestos repercutidos)
    ''' </summary>
    ''' <remarks>Llamado desde: CFacturae.invoiceTaxesOutputsXML2Obj
    ''' Tiempo máximo: menor a 0,1
    ''' Revisado por: auv. ¿?/05/2012</remarks>
    Private Sub invoiceTaxOutputXML2Obj(ByRef objInvoiceTaxOutput As TaxOutputType, ByRef nodoInvoiceTaxOutput As XmlNode)
        Dim contadorHijos As Integer = Nothing
        Dim contadorNietos As Integer = Nothing
        Dim nodoHijo As XmlNode = Nothing
        Dim nodoNieto As XmlNode = Nothing

        contadorHijos = 0
        While contadorHijos < nodoInvoiceTaxOutput.ChildNodes.Count
            nodoHijo = nodoInvoiceTaxOutput.ChildNodes(contadorHijos)
            If nodoHijo.Name = "TaxTypeCode" Then
                objInvoiceTaxOutput.taxTypeCode = New TaxTypeCodeType
                objInvoiceTaxOutput.taxTypeCode.value = nodoHijo.InnerText
            End If
            If nodoHijo.Name = "TaxRate" Then
                objInvoiceTaxOutput.taxRate = New DoubleTwoDecimalType
                objInvoiceTaxOutput.taxRate.value = str2Dbl(nodoHijo.InnerText)
            End If
            If nodoHijo.Name = "TaxableBase" Then
                objInvoiceTaxOutput.taxableBase = New AmountType
                contadorNietos = 0
                While contadorNietos < nodoHijo.ChildNodes.Count
                    nodoNieto = nodoHijo.ChildNodes(contadorNietos)
                    If nodoNieto.Name = "TotalAmount" Then
                        objInvoiceTaxOutput.taxableBase.totalAmount = New DoubleTwoDecimalType
                        objInvoiceTaxOutput.taxableBase.totalAmount.value = str2Dbl(nodoNieto.InnerText)
                    End If
                    If nodoNieto.Name = "EquivalentInEuros" Then
                        objInvoiceTaxOutput.taxableBase.equivalentInEuros = New DoubleTwoDecimalType
                        objInvoiceTaxOutput.taxableBase.equivalentInEuros.value = str2Dbl(nodoNieto.InnerText)
                    End If
                    contadorNietos = contadorNietos + 1
                End While
            End If
            If nodoHijo.Name = "TaxAmount" Then
                objInvoiceTaxOutput.taxAmount = New AmountType
                contadorNietos = 0
                While contadorNietos < nodoHijo.ChildNodes.Count
                    nodoNieto = nodoHijo.ChildNodes(contadorNietos)
                    If nodoNieto.Name = "TotalAmount" Then
                        objInvoiceTaxOutput.taxAmount.totalAmount = New DoubleTwoDecimalType
                        objInvoiceTaxOutput.taxAmount.totalAmount.value = str2Dbl(nodoNieto.InnerText)
                    End If
                    If nodoNieto.Name = "EquivalentInEuros" Then
                        objInvoiceTaxOutput.taxAmount.equivalentInEuros = New DoubleTwoDecimalType
                        objInvoiceTaxOutput.taxAmount.equivalentInEuros.value = str2Dbl(nodoNieto.InnerText)
                    End If
                    contadorNietos = contadorNietos + 1
                End While
            End If
            If nodoHijo.Name = "EquivalenceSurcharge" Then
                objInvoiceTaxOutput.equivalenceSurcharge = New DoubleTwoDecimalType
                objInvoiceTaxOutput.equivalenceSurcharge.value = str2Dbl(nodoHijo.InnerText)
            End If
            If nodoHijo.Name = "EquivalenceSurchargeAmount" Then
                objInvoiceTaxOutput.equivalenceSurchargeAmount = New AmountType
                contadorNietos = 0
                While contadorNietos < nodoHijo.ChildNodes.Count
                    nodoNieto = nodoHijo.ChildNodes(contadorNietos)
                    If nodoNieto.Name = "TotalAmount" Then
                        objInvoiceTaxOutput.equivalenceSurchargeAmount.totalAmount = New DoubleTwoDecimalType
                        objInvoiceTaxOutput.equivalenceSurchargeAmount.totalAmount.value = str2Dbl(nodoNieto.InnerText)
                    End If
                    If nodoNieto.Name = "EquivalentInEuros" Then
                        objInvoiceTaxOutput.equivalenceSurchargeAmount.equivalentInEuros = New DoubleTwoDecimalType
                        objInvoiceTaxOutput.equivalenceSurchargeAmount.equivalentInEuros.value = str2Dbl(nodoNieto.InnerText)
                    End If
                    contadorNietos = contadorNietos + 1
                End While
            End If
            contadorHijos = contadorHijos + 1
        End While
    End Sub
    ''' <summary>
    ''' Transformación de un nodo raiz TaxesWithheld con su estructura interna a un vector de objetos TaxWithheldType (para nivel de factura)
    ''' </summary>
    ''' <remarks>Llamado desde: CFacturae.invoiceXML2Obj
    ''' Tiempo máximo: Depende del número de elementos que tengan los vectores de longitud variable tales como
    ''' el número de impuestos retenidos.
    ''' Revisado por: auv. ¿?/05/2012</remarks>
    Private Sub invoiceTaxesWithheldXML2Obj(ByRef objInvoiceTaxesWithheld() As TaxWithheldType, ByRef nodoInvoiceTaxesWithheld As XmlNode)
        Dim contadorhijos As Integer = Nothing
        Dim nodoHijo As XmlNode = Nothing
        Dim contadorImpuestosRetenidos As Integer = Nothing

        objInvoiceTaxesWithheld = New TaxWithheldType() {}

        contadorImpuestosRetenidos = 0
        contadorhijos = 0
        While contadorhijos < nodoInvoiceTaxesWithheld.ChildNodes.Count
            nodoHijo = nodoInvoiceTaxesWithheld.ChildNodes(contadorhijos)
            If nodoHijo.Name = "Tax" Then
                ReDim Preserve objInvoiceTaxesWithheld(0 To contadorImpuestosRetenidos)
                objInvoiceTaxesWithheld(contadorImpuestosRetenidos) = New TaxWithheldType
                Me.invoiceTaxWithheldXML2Obj(objInvoiceTaxesWithheld(contadorImpuestosRetenidos), nodoHijo)
                contadorImpuestosRetenidos = contadorImpuestosRetenidos + 1
            End If
            contadorhijos = contadorhijos + 1
        End While
    End Sub
    ''' <summary>
    ''' Transformación de un nodo raiz Tax con su estructura interna a un objeto TaxWithheldType (para nivel de factura y para impuestos retenidos)
    ''' </summary>
    ''' <remarks>Llamado desde: CFacturae.invoiceTaxesWithheldXML2Obj
    ''' Tiempo máximo: menor a 0,1
    ''' Revisado por: auv. ¿?/05/2012</remarks>
    Private Sub invoiceTaxWithheldXML2Obj(ByRef objInvoiceTaxWithheld As TaxWithheldType, ByRef nodoInvoiceTaxWithheld As XmlNode)
        Dim contadorHijos As Integer = Nothing
        Dim contadorNietos As Integer = Nothing
        Dim nodoHijo As XmlNode = Nothing
        Dim nodoNieto As XmlNode = Nothing

        contadorHijos = 0
        While contadorHijos < nodoInvoiceTaxWithheld.ChildNodes.Count
            nodoHijo = nodoInvoiceTaxWithheld.ChildNodes(contadorHijos)
            If nodoHijo.Name = "TaxTypeCode" Then
                objInvoiceTaxWithheld.taxTypeCode = New TaxTypeCodeType
                objInvoiceTaxWithheld.taxTypeCode.value = nodoHijo.InnerText
            End If
            If nodoHijo.Name = "TaxRate" Then
                objInvoiceTaxWithheld.taxRate = New DoubleTwoDecimalType
                objInvoiceTaxWithheld.taxRate.value = str2Dbl(nodoHijo.InnerText)
            End If
            If nodoHijo.Name = "TaxableBase" Then
                objInvoiceTaxWithheld.taxableBase = New AmountType
                contadorNietos = 0
                While contadorNietos < nodoHijo.ChildNodes.Count
                    nodoNieto = nodoHijo.ChildNodes(contadorNietos)
                    If nodoNieto.Name = "TotalAmount" Then
                        objInvoiceTaxWithheld.taxableBase.totalAmount = New DoubleTwoDecimalType
                        objInvoiceTaxWithheld.taxableBase.totalAmount.value = str2Dbl(nodoNieto.InnerText)
                    End If
                    If nodoNieto.Name = "EquivalentInEuros" Then
                        objInvoiceTaxWithheld.taxableBase.equivalentInEuros = New DoubleTwoDecimalType
                        objInvoiceTaxWithheld.taxableBase.equivalentInEuros.value = str2Dbl(nodoNieto.InnerText)
                    End If
                    contadorNietos = contadorNietos + 1
                End While
            End If
            If nodoHijo.Name = "TaxAmount" Then
                objInvoiceTaxWithheld.taxAmount = New AmountType
                contadorNietos = 0
                While contadorNietos < nodoHijo.ChildNodes.Count
                    nodoNieto = nodoHijo.ChildNodes(contadorNietos)
                    If nodoNieto.Name = "TotalAmount" Then
                        objInvoiceTaxWithheld.taxAmount.totalAmount = New DoubleTwoDecimalType
                        objInvoiceTaxWithheld.taxAmount.totalAmount.value = str2Dbl(nodoNieto.InnerText)
                    End If
                    If nodoNieto.Name = "EquivalentInEuros" Then
                        objInvoiceTaxWithheld.taxAmount.equivalentInEuros = New DoubleTwoDecimalType
                        objInvoiceTaxWithheld.taxAmount.equivalentInEuros.value = str2Dbl(nodoNieto.InnerText)
                    End If
                    contadorNietos = contadorNietos + 1
                End While
            End If
            contadorHijos = contadorHijos + 1
        End While
    End Sub
    ''' <summary>
    ''' Transformación de un nodo raiz InvoiceTotals con su estructura interna a un objeto InvoiceTotalsType
    ''' </summary>
    ''' <remarks>Llamado desde: CFacturae.invoiceXML2Obj
    ''' Tiempo máximo: Depende del número de elementos que tengan los vectores de longitud variable tales como
    ''' el número de descuentos y del número de costes.
    ''' Revisado por: auv. ¿?/05/2012</remarks>
    Private Sub invoiceTotalsXML2Obj(ByRef objInvoiceTotals As InvoiceTotalsType, ByRef nodoInvoiceTotals As XmlNode)
        Dim contadorHijos As Integer = Nothing
        Dim contadorNietos As Integer = Nothing
        Dim contadorBisnietos As Integer = Nothing
        Dim nodoHijo As XmlNode = Nothing
        Dim nodoNieto As XmlNode = Nothing
        Dim nodoBisnieto As XmlNode = Nothing
        Dim contadorDescuentos As Integer = Nothing
        Dim contadorCargos As Integer = Nothing

        contadorHijos = 0
        While contadorHijos < nodoInvoiceTotals.ChildNodes.Count
            nodoHijo = nodoInvoiceTotals.ChildNodes(contadorHijos)
            If nodoHijo.Name = "TotalGrossAmount" Then
                objInvoiceTotals.totalGrossAmount = New DoubleTwoDecimalType
                objInvoiceTotals.totalGrossAmount.value = str2Dbl(nodoHijo.InnerText)
            End If
            If nodoHijo.Name = "GeneralDiscounts" Then
                objInvoiceTotals.generalDiscounts = New DiscountType() {}
                contadorDescuentos = 0
                contadorNietos = 0
                While contadorNietos < nodoHijo.ChildNodes.Count
                    nodoNieto = nodoHijo.ChildNodes(contadorNietos)
                    If nodoNieto.Name = "Discount" Then
                        ReDim Preserve objInvoiceTotals.generalDiscounts(0 To contadorDescuentos)
                        objInvoiceTotals.generalDiscounts(contadorDescuentos) = New DiscountType
                        contadorBisnietos = 0
                        While contadorBisnietos < nodoNieto.ChildNodes.Count
                            nodoBisnieto = nodoNieto.ChildNodes(contadorBisnietos)
                            If nodoBisnieto.Name = "DiscountReason" Then
                                objInvoiceTotals.generalDiscounts(contadorDescuentos).discountReason = nodoBisnieto.InnerText
                            End If
                            If nodoBisnieto.Name = "DiscountRate" Then
                                objInvoiceTotals.generalDiscounts(contadorDescuentos).discountRate = New DoubleFourDecimalType
                                objInvoiceTotals.generalDiscounts(contadorDescuentos).discountRate.value = str2Dbl(nodoBisnieto.InnerText)
                            End If
                            If nodoBisnieto.Name = "DiscountAmount" Then
                                objInvoiceTotals.generalDiscounts(contadorDescuentos).discountAmount = New DoubleSixDecimalType
                                objInvoiceTotals.generalDiscounts(contadorDescuentos).discountAmount.value = str2Dbl(nodoBisnieto.InnerText)
                            End If
                            contadorBisnietos = contadorBisnietos + 1
                        End While
                        contadorDescuentos = contadorDescuentos + 1
                    End If
                    contadorNietos = contadorNietos + 1
                End While
            End If
            If nodoHijo.Name = "GeneralSurcharges" Then
                objInvoiceTotals.generalSurcharges = New ChargeType() {}
                contadorCargos = 0
                contadorNietos = 0
                While contadorNietos < nodoHijo.ChildNodes.Count
                    nodoNieto = nodoHijo.ChildNodes(contadorNietos)
                    If nodoNieto.Name = "Charge" Then
                        ReDim Preserve objInvoiceTotals.generalSurcharges(0 To contadorCargos)
                        objInvoiceTotals.generalSurcharges(contadorCargos) = New ChargeType
                        contadorBisnietos = 0
                        While contadorBisnietos < nodoNieto.ChildNodes.Count
                            nodoBisnieto = nodoNieto.ChildNodes(contadorBisnietos)
                            If nodoBisnieto.Name = "ChargeReason" Then
                                objInvoiceTotals.generalSurcharges(contadorCargos).chargeReason = nodoBisnieto.InnerText
                            End If
                            If nodoBisnieto.Name = "ChargeRate" Then
                                objInvoiceTotals.generalSurcharges(contadorCargos).chargeRate = New DoubleFourDecimalType
                                objInvoiceTotals.generalSurcharges(contadorCargos).chargeRate.value = str2Dbl(nodoBisnieto.InnerText)
                            End If
                            If nodoBisnieto.Name = "ChargeAmount" Then
                                objInvoiceTotals.generalSurcharges(contadorCargos).chargeAmount = New DoubleSixDecimalType
                                objInvoiceTotals.generalSurcharges(contadorCargos).chargeAmount.value = str2Dbl(nodoBisnieto.InnerText)
                            End If
                            contadorBisnietos = contadorBisnietos + 1
                        End While
                        contadorCargos = contadorCargos + 1
                    End If
                    contadorNietos = contadorNietos + 1
                End While
            End If
            If nodoHijo.Name = "TotalGeneralDiscounts" Then
                objInvoiceTotals.totalGeneralDiscounts = New DoubleTwoDecimalType
                objInvoiceTotals.totalGeneralDiscounts.value = str2Dbl(nodoHijo.InnerText)
            End If
            If nodoHijo.Name = "TotalGeneralSurcharges" Then
                objInvoiceTotals.totalGeneralSurcharges = New DoubleTwoDecimalType
                objInvoiceTotals.totalGeneralSurcharges.value = str2Dbl(nodoHijo.InnerText)
            End If
            If nodoHijo.Name = "TotalGrossAmountBeforeTaxes" Then
                objInvoiceTotals.totalGrossAmountBeforeTaxes = New DoubleTwoDecimalType
                objInvoiceTotals.totalGrossAmountBeforeTaxes.value = str2Dbl(nodoHijo.InnerText)
            End If
            If nodoHijo.Name = "TotalTaxOutputs" Then
                objInvoiceTotals.totalTaxOutputs = New DoubleTwoDecimalType
                objInvoiceTotals.totalTaxOutputs.value = str2Dbl(nodoHijo.InnerText)
            End If
            If nodoHijo.Name = "TotalTaxesWithheld" Then
                objInvoiceTotals.totalTaxesWithheld = New DoubleTwoDecimalType
                objInvoiceTotals.totalTaxesWithheld.value = str2Dbl(nodoHijo.InnerText)
            End If
            If nodoHijo.Name = "InvoiceTotal" Then
                objInvoiceTotals.invoiceTotal = New DoubleTwoDecimalType
                objInvoiceTotals.invoiceTotal.value = str2Dbl(nodoHijo.InnerText)
            End If
            If nodoHijo.Name = "TotalOutstandingAmount" Then
                objInvoiceTotals.totalOutstandingAmount = New DoubleTwoDecimalType
                objInvoiceTotals.totalOutstandingAmount.value = str2Dbl(nodoHijo.InnerText)
            End If
            If nodoHijo.Name = "AmountsWithheld" Then
                objInvoiceTotals.amountsWithheld = New AmountsWithheldType
                contadorNietos = 0
                While contadorNietos < nodoHijo.ChildNodes.Count
                    nodoNieto = nodoHijo.ChildNodes(contadorNietos)
                    If nodoNieto.Name = "WithholdingReason" Then
                        objInvoiceTotals.amountsWithheld.withholdingReason = nodoNieto.InnerText
                    End If
                    If nodoNieto.Name = "WithholdingRate" Then
                        objInvoiceTotals.amountsWithheld.withholdingRate = New DoubleFourDecimalType
                        objInvoiceTotals.amountsWithheld.withholdingRate.value = str2Dbl(nodoNieto.InnerText)
                    End If
                    If nodoNieto.Name = "WithholdingAmount" Then
                        objInvoiceTotals.amountsWithheld.withholdingAmount = New DoubleTwoDecimalType
                        objInvoiceTotals.amountsWithheld.withholdingAmount.value = str2Dbl(nodoNieto.InnerText)
                    End If
                    contadorNietos = contadorNietos + 1
                End While
            End If
            If nodoHijo.Name = "TotalExecutableAmount" Then
                objInvoiceTotals.totalExecutableAmount = New DoubleTwoDecimalType
                objInvoiceTotals.totalExecutableAmount.value = str2Dbl(nodoHijo.InnerText)
            End If
            contadorHijos = contadorHijos + 1
        End While
    End Sub
    ''' <summary>
    ''' Transformación de un nodo raiz Items con su estructura interna a un vector de objetos ItemType
    ''' </summary>
    ''' <remarks>Llamado desde: CFacturae.invoiceXML2Obj
    ''' Tiempo máximo: Depende del número de elementos que tengan los vectores de longitud variable tales como
    ''' el número de items, descuentos, costes, impuestos repercutidos, impuestos retenidos, etc...
    ''' Revisado por: auv. ¿?/05/2012</remarks>
    Private Sub invoiceItemsXML2Obj(ByRef objInvoiceItems() As ItemType, ByRef nodoInvoiceLines As XmlNode)
        Dim contadorHijos As Integer = Nothing
        Dim nodoHijo As XmlNode = Nothing
        Dim contadorItems As Integer = Nothing

        objInvoiceItems = New ItemType() {}

        contadorItems = 0
        contadorHijos = 0
        While contadorHijos < nodoInvoiceLines.ChildNodes.Count
            nodoHijo = nodoInvoiceLines.ChildNodes(contadorHijos)
            If nodoHijo.Name = "InvoiceLine" Then
                ReDim Preserve objInvoiceItems(0 To contadorItems)
                objInvoiceItems(contadorItems) = New ItemType
                Me.invoiceItemXML2Obj(objInvoiceItems(contadorItems), nodoHijo)
                contadorItems = contadorItems + 1
            End If
            contadorHijos = contadorHijos + 1
        End While
    End Sub
    ''' <summary>
    ''' Transformación de un nodo raiz InvoiceLine con su estructura interna a un objeto ItemType
    ''' </summary>
    ''' <remarks>Llamado desde: CFacturae.invoiceItemsXML2Obj
    ''' Tiempo máximo: Depende del número de elementos que tengan los vectores de longitud variable tales como
    ''' el número de descuentos, costes, impuestos repercutidos, impuestos retenidos, etc...
    ''' Revisado por: auv. ¿?/05/2012</remarks>
    Private Sub invoiceItemXML2Obj(ByRef objInvoiceItem As ItemType, ByRef nodoInvoiceLine As XmlNode)
        Dim contadorHijos As Integer = Nothing
        Dim contadorNietos As Integer = Nothing
        Dim contadorBisnietos As Integer = Nothing
        Dim nodoHijo As XmlNode = Nothing
        Dim nodoNieto As XmlNode = Nothing
        Dim nodoBisnieto As XmlNode = Nothing
        Dim contadorAlbaranes As Integer = Nothing
        Dim contadorDescuentos As Integer = Nothing
        Dim contadorCargos As Integer = Nothing

        contadorHijos = 0
        While contadorHijos < nodoInvoiceLine.ChildNodes.Count
            nodoHijo = nodoInvoiceLine.ChildNodes(contadorHijos)
            If nodoHijo.Name = "SequenceNumber" Then
                objInvoiceItem.sequenceNumber = str2Dbl(nodoHijo.InnerText)
            End If
            If nodoHijo.Name = "DeliveryNotesReferences" Then
                objInvoiceItem.deliveryNotesReferences = New DeliveryNoteType() {}
                contadorAlbaranes = 0
                contadorNietos = 0
                While contadorNietos < nodoHijo.ChildNodes.Count
                    nodoNieto = nodoHijo.ChildNodes(contadorNietos)
                    If nodoNieto.Name = "DeliveryNote" Then
                        ReDim Preserve objInvoiceItem.deliveryNotesReferences(0 To contadorAlbaranes)
                        objInvoiceItem.deliveryNotesReferences(contadorAlbaranes) = New DeliveryNoteType
                        contadorBisnietos = 0
                        While contadorBisnietos < nodoNieto.ChildNodes.Count
                            nodoBisnieto = nodoNieto.ChildNodes(contadorBisnietos)
                            If nodoBisnieto.Name = "DeliveryNoteNumber" Then
                                objInvoiceItem.deliveryNotesReferences(contadorAlbaranes).deliveryNoteNumber = nodoBisnieto.InnerText
                            End If
                            If nodoBisnieto.Name = "DeliveryNoteDate" Then
                                objInvoiceItem.deliveryNotesReferences(contadorAlbaranes).deliveryNoteDate = date_XMLStr2ObjId(nodoBisnieto.InnerText)
                            End If
                            contadorBisnietos = contadorBisnietos + 1
                        End While
                        contadorAlbaranes = contadorAlbaranes + 1
                    End If
                    contadorNietos = contadorNietos + 1
                End While
            End If
            If nodoHijo.Name = "ItemDescription" Then
                objInvoiceItem.itemDescription = nodoHijo.InnerText
            End If
            If nodoHijo.Name = "Quantity" Then
                objInvoiceItem.quantity = str2Dbl(nodoHijo.InnerText)
            End If
            If nodoHijo.Name = "UnitOfMeasure" Then
                objInvoiceItem.unitOfMeasure = New UnitOfMeasureType
                objInvoiceItem.unitOfMeasure.value = nodoHijo.InnerText
            End If
            If nodoHijo.Name = "UnitPriceWithoutTax" Then
                objInvoiceItem.unitPriceWithoutTax = New DoubleSixDecimalType
                objInvoiceItem.unitPriceWithoutTax.value = str2Dbl(nodoHijo.InnerText)
            End If
            If nodoHijo.Name = "TotalCost" Then
                objInvoiceItem.totalCost = New DoubleSixDecimalType
                objInvoiceItem.totalCost.value = str2Dbl(nodoHijo.InnerText)
            End If
            If nodoHijo.Name = "DiscountsAndRebates" Then
                objInvoiceItem.discountsAndRebates = New DiscountType() {}
                contadorDescuentos = 0
                contadorNietos = 0
                While contadorNietos < nodoHijo.ChildNodes.Count
                    nodoNieto = nodoHijo.ChildNodes(contadorNietos)
                    If nodoNieto.Name = "Discount" Then
                        ReDim Preserve objInvoiceItem.discountsAndRebates(0 To contadorDescuentos)
                        objInvoiceItem.discountsAndRebates(contadorDescuentos) = New DiscountType
                        contadorBisnietos = 0
                        While contadorBisnietos < nodoNieto.ChildNodes.Count
                            nodoBisnieto = nodoNieto.ChildNodes(contadorBisnietos)
                            If nodoBisnieto.Name = "DiscountReason" Then
                                objInvoiceItem.discountsAndRebates(contadorDescuentos).discountReason = nodoBisnieto.InnerText
                            End If
                            If nodoBisnieto.Name = "DiscountRate" Then
                                objInvoiceItem.discountsAndRebates(contadorDescuentos).discountRate = New DoubleFourDecimalType
                                objInvoiceItem.discountsAndRebates(contadorDescuentos).discountRate.value = str2Dbl(nodoBisnieto.InnerText)
                            End If
                            If nodoBisnieto.Name = "DiscountAmount" Then
                                objInvoiceItem.discountsAndRebates(contadorDescuentos).discountAmount = New DoubleSixDecimalType
                                objInvoiceItem.discountsAndRebates(contadorDescuentos).discountAmount.value = str2Dbl(nodoBisnieto.InnerText)
                            End If
                            contadorBisnietos = contadorBisnietos + 1
                        End While
                        contadorDescuentos = contadorDescuentos + 1
                    End If
                    contadorNietos = contadorNietos + 1
                End While
            End If
            If nodoHijo.Name = "Charges" Then
                objInvoiceItem.charges = New ChargeType() {}
                contadorCargos = 0
                contadorNietos = 0
                While contadorNietos < nodoHijo.ChildNodes.Count
                    nodoNieto = nodoHijo.ChildNodes(contadorNietos)
                    If nodoNieto.Name = "Charge" Then
                        ReDim Preserve objInvoiceItem.charges(0 To contadorCargos)
                        objInvoiceItem.charges(contadorCargos) = New ChargeType
                        contadorBisnietos = 0
                        While contadorBisnietos < nodoNieto.ChildNodes.Count
                            nodoBisnieto = nodoNieto.ChildNodes(contadorBisnietos)
                            If nodoBisnieto.Name = "ChargeReason" Then
                                objInvoiceItem.charges(contadorCargos).chargeReason = nodoBisnieto.InnerText
                            End If
                            If nodoBisnieto.Name = "ChargeRate" Then
                                objInvoiceItem.charges(contadorCargos).chargeRate = New DoubleFourDecimalType
                                objInvoiceItem.charges(contadorCargos).chargeRate.value = str2Dbl(nodoBisnieto.InnerText)
                            End If
                            If nodoBisnieto.Name = "ChargeAmount" Then
                                objInvoiceItem.charges(contadorCargos).chargeAmount = New DoubleSixDecimalType
                                objInvoiceItem.charges(contadorCargos).chargeAmount.value = str2Dbl(nodoBisnieto.InnerText)
                            End If
                            contadorBisnietos = contadorBisnietos + 1
                        End While
                        contadorCargos = contadorCargos + 1
                    End If
                    contadorNietos = contadorNietos + 1
                End While
            End If
            If nodoHijo.Name = "GrossAmount" Then
                objInvoiceItem.grossAmount = New DoubleSixDecimalType
                objInvoiceItem.grossAmount.value = str2Dbl(nodoHijo.InnerText)
            End If
            If nodoHijo.Name = "TaxesWithheld" Then
                Me.invoiceItemTaxesWithheldXML2Obj(objInvoiceItem.taxesWithheld, nodoHijo)
            End If
            If nodoHijo.Name = "TaxesOutputs" Then
                Me.invoiceItemTaxesOutputsXML2Obj(objInvoiceItem.taxesOutputs, nodoHijo)
            End If
            If nodoHijo.Name = "SpecialTaxableEvent" Then
                objInvoiceItem.specialTaxableEvent = New SpecialTaxableEvent
                contadorNietos = 0
                While contadorNietos < nodoHijo.ChildNodes.Count
                    If nodoNieto.Name = "SpecialTaxableEventCode" Then
                        objInvoiceItem.specialTaxableEvent.specialTaxableEventCode = New SpecialTaxableEventCode
                        objInvoiceItem.specialTaxableEvent.specialTaxableEventCode.value = nodoNieto.InnerText
                    End If
                    If nodoNieto.Name = "SpecialTaxableEventReason" Then
                        objInvoiceItem.specialTaxableEvent.specialTaxableEventReason = date_XMLStr2ObjId(nodoNieto.InnerText)
                    End If
                    contadorNietos = contadorNietos + 1
                End While
            End If
            If nodoHijo.Name = "ArticleCode" Then
                objInvoiceItem.articleCode = nodoHijo.InnerText
            End If
            contadorHijos = contadorHijos + 1
        End While
    End Sub
    ''' <summary>
    ''' Transformación de un nodo raiz TaxesWithheld con su estructura interna a un vector de objetos TaxWithheldType (para nivel de Item)
    ''' </summary>
    ''' <remarks>Llamado desde: CFacturae.invoiceItemXML2Obj
    ''' Tiempo máximo: Depende del número de elementos que tengan los vectores de longitud variable tales como
    ''' el número de impuestos retenidos.
    ''' Revisado por: auv. ¿?/05/2012</remarks>
    Private Sub invoiceItemTaxesWithheldXML2Obj(ByRef objInvoiceItemTaxesWithheld() As TaxWithheldType, ByRef nodoInvoiceItemTaxesWithheld As XmlNode)
        Dim contadorHijos As Integer = Nothing
        Dim nodoHijo As XmlNode = Nothing
        Dim contadorImpuestos As Integer = Nothing

        objInvoiceItemTaxesWithheld = New TaxWithheldType() {}

        contadorImpuestos = 0
        contadorHijos = 0
        While contadorHijos < nodoInvoiceItemTaxesWithheld.ChildNodes.Count
            nodoHijo = nodoInvoiceItemTaxesWithheld.ChildNodes(contadorHijos)
            If nodoHijo.Name = "Tax" Then
                ReDim Preserve objInvoiceItemTaxesWithheld(0 To contadorImpuestos)
                objInvoiceItemTaxesWithheld(contadorImpuestos) = New TaxWithheldType
                Me.invoiceItemTaxWithheldXML2Obj(objInvoiceItemTaxesWithheld(contadorImpuestos), nodoHijo)
                contadorImpuestos = contadorImpuestos + 1
            End If
            contadorHijos = contadorHijos + 1
        End While
    End Sub
    ''' <summary>
    ''' Transformación de un nodo raiz Tax con su estructura interna a un objeto TaxWithheldType (para nivel de Item y para impuestos retenidos)
    ''' </summary>
    ''' <remarks>Llamado desde: CFacturae.invoiceItemTaxesWithheldXML2Obj
    ''' Tiempo máximo: menor a 0,1
    ''' Revisado por: auv. ¿?/05/2012</remarks>
    Private Sub invoiceItemTaxWithheldXML2Obj(ByRef objInvoiceItemTaxWithheld As TaxWithheldType, ByRef nodoInvoiceItemTaxWithheld As XmlNode)
        Dim contadorHijos As Integer = Nothing
        Dim contadorNietos As Integer = Nothing
        Dim nodoHijo As XmlNode = Nothing
        Dim nodoNieto As XmlNode = Nothing

        contadorHijos = 0
        While contadorHijos < nodoInvoiceItemTaxWithheld.ChildNodes.Count
            nodoHijo = nodoInvoiceItemTaxWithheld.ChildNodes(contadorHijos)
            If nodoHijo.Name = "TaxTypeCode" Then
                objInvoiceItemTaxWithheld.taxTypeCode = New TaxTypeCodeType
                objInvoiceItemTaxWithheld.taxTypeCode.value = nodoHijo.InnerText
            End If
            If nodoHijo.Name = "TaxRate" Then
                objInvoiceItemTaxWithheld.taxRate = New DoubleTwoDecimalType
                objInvoiceItemTaxWithheld.taxRate.value = str2Dbl(nodoHijo.InnerText)
            End If
            If nodoHijo.Name = "TaxableBase" Then
                objInvoiceItemTaxWithheld.taxableBase = New AmountType
                contadorNietos = 0
                While contadorNietos < nodoHijo.ChildNodes.Count
                    nodoNieto = nodoHijo.ChildNodes(contadorNietos)
                    If nodoNieto.Name = "TotalAmount" Then
                        objInvoiceItemTaxWithheld.taxableBase.totalAmount = New DoubleTwoDecimalType
                        objInvoiceItemTaxWithheld.taxableBase.totalAmount.value = str2Dbl(nodoNieto.InnerText)
                    End If
                    If nodoNieto.Name = "EquivalentInEuros" Then
                        objInvoiceItemTaxWithheld.taxableBase.equivalentInEuros = New DoubleTwoDecimalType
                        objInvoiceItemTaxWithheld.taxableBase.equivalentInEuros.value = str2Dbl(nodoNieto.InnerText)
                    End If
                    contadorNietos = contadorNietos + 1
                End While
            End If
            If nodoHijo.Name = "TaxAmount" Then
                objInvoiceItemTaxWithheld.taxAmount = New AmountType
                contadorNietos = 0
                While contadorNietos < nodoHijo.ChildNodes.Count
                    nodoNieto = nodoHijo.ChildNodes(contadorNietos)
                    If nodoNieto.Name = "TotalAmount" Then
                        objInvoiceItemTaxWithheld.taxAmount.totalAmount = New DoubleTwoDecimalType
                        objInvoiceItemTaxWithheld.taxAmount.totalAmount.value = str2Dbl(nodoNieto.InnerText)
                    End If
                    If nodoNieto.Name = "EquivalentInEuros" Then
                        objInvoiceItemTaxWithheld.taxAmount.equivalentInEuros = New DoubleTwoDecimalType
                        objInvoiceItemTaxWithheld.taxAmount.equivalentInEuros.value = str2Dbl(nodoNieto.InnerText)
                    End If
                    contadorNietos = contadorNietos + 1
                End While
            End If
            contadorHijos = contadorHijos + 1
        End While
    End Sub
    ''' <summary>
    ''' Transformación de un nodo raiz TaxesOutputs con su estructura interna a un objeto TaxOutputType (para nivel de Item)
    ''' </summary>
    ''' <remarks>Llamado desde: CFacturae.invoiceItemXML2Obj
    ''' Tiempo máximo: Depende del número de elementos que tengan los vectores de longitud variable tales como
    ''' el número de impuestos repercutidos.
    ''' Revisado por: auv. ¿?/05/2012</remarks>
    Private Sub invoiceItemTaxesOutputsXML2Obj(ByRef objInvoiceItemTaxesOutputs() As TaxOutputType, ByRef nodoItemTaxesOutputs As XmlNode)
        Dim contadorHijos As Integer = Nothing
        Dim nodoHijo As XmlNode = Nothing
        Dim contadorItems As Integer = Nothing

        objInvoiceItemTaxesOutputs = New TaxOutputType() {}

        contadorItems = 0
        contadorHijos = 0
        While contadorHijos < nodoItemTaxesOutputs.ChildNodes.Count
            nodoHijo = nodoItemTaxesOutputs.ChildNodes(contadorHijos)
            If nodoHijo.Name = "Tax" Then
                ReDim Preserve objInvoiceItemTaxesOutputs(0 To contadorItems)
                objInvoiceItemTaxesOutputs(contadorItems) = New TaxOutputType
                Me.invoiceItemTaxOutputXML2Obj(objInvoiceItemTaxesOutputs(contadorItems), nodoHijo)
                contadorItems = contadorItems + 1
            End If
            contadorHijos = contadorHijos + 1
        End While
    End Sub
    ''' <summary>
    ''' Transformación de un nodo raiz Tax con su estructura interna a un objeto TaxOutputType (para nivel de Item y para impuestos repercutidos)
    ''' </summary>
    ''' <remarks>Llamado desde: CFacturae.invoiceItemTaxesOutputsXML2Obj
    ''' Tiempo máximo: menor a 0,1
    ''' Revisado por: auv. ¿?/05/2012</remarks>
    Private Sub invoiceItemTaxOutputXML2Obj(ByRef objInvoiceItemTaxOutput As TaxOutputType, ByRef nodoInvoiceItemTaxOutput As XmlNode)
        Dim contadorHijos As Integer = Nothing
        Dim contadorNietos As Integer = Nothing
        Dim nodoHijo As XmlNode = Nothing
        Dim nodoNieto As XmlNode = Nothing

        contadorHijos = 0
        While contadorHijos < nodoInvoiceItemTaxOutput.ChildNodes.Count
            nodoHijo = nodoInvoiceItemTaxOutput.ChildNodes(contadorHijos)
            If nodoHijo.Name = "TaxTypeCode" Then
                objInvoiceItemTaxOutput.taxTypeCode = New TaxTypeCodeType
                objInvoiceItemTaxOutput.taxTypeCode.value = nodoHijo.InnerText
            End If
            If nodoHijo.Name = "TaxRate" Then
                objInvoiceItemTaxOutput.taxRate = New DoubleTwoDecimalType
                objInvoiceItemTaxOutput.taxRate.value = str2Dbl(nodoHijo.InnerText)
            End If
            If nodoHijo.Name = "TaxableBase" Then
                objInvoiceItemTaxOutput.taxableBase = New AmountType
                contadorNietos = 0
                While contadorNietos < nodoHijo.ChildNodes.Count
                    nodoNieto = nodoHijo.ChildNodes(contadorNietos)
                    If nodoNieto.Name = "TotalAmount" Then
                        objInvoiceItemTaxOutput.taxableBase.totalAmount = New DoubleTwoDecimalType
                        objInvoiceItemTaxOutput.taxableBase.totalAmount.value = str2Dbl(nodoNieto.InnerText)
                    End If
                    If nodoNieto.Name = "EquivalentInEuros" Then
                        objInvoiceItemTaxOutput.taxableBase.equivalentInEuros = New DoubleTwoDecimalType
                        objInvoiceItemTaxOutput.taxableBase.equivalentInEuros.value = str2Dbl(nodoNieto.InnerText)
                    End If
                    contadorNietos = contadorNietos + 1
                End While
            End If
            If nodoHijo.Name = "TaxAmount" Then
                objInvoiceItemTaxOutput.taxAmount = New AmountType
                contadorNietos = 0
                While contadorNietos < nodoHijo.ChildNodes.Count
                    nodoNieto = nodoHijo.ChildNodes(contadorNietos)
                    If nodoNieto.Name = "TotalAmount" Then
                        objInvoiceItemTaxOutput.taxAmount.totalAmount = New DoubleTwoDecimalType
                        objInvoiceItemTaxOutput.taxAmount.totalAmount.value = str2Dbl(nodoNieto.InnerText)
                    End If
                    If nodoNieto.Name = "EquivalentInEuros" Then
                        objInvoiceItemTaxOutput.taxAmount.equivalentInEuros = New DoubleTwoDecimalType
                        objInvoiceItemTaxOutput.taxAmount.equivalentInEuros.value = str2Dbl(nodoNieto.InnerText)
                    End If
                    contadorNietos = contadorNietos + 1
                End While
            End If
            If nodoHijo.Name = "EquivalenceSurcharge" Then
                objInvoiceItemTaxOutput.equivalenceSurcharge = New DoubleTwoDecimalType
                objInvoiceItemTaxOutput.equivalenceSurcharge.value = str2Dbl(nodoHijo.InnerText)
            End If
            If nodoHijo.Name = "EquivalenceSurchargeAmount" Then
                objInvoiceItemTaxOutput.equivalenceSurchargeAmount = New AmountType
                contadorNietos = 0
                While contadorNietos < nodoHijo.ChildNodes.Count
                    nodoNieto = nodoHijo.ChildNodes(contadorNietos)
                    If nodoNieto.Name = "TotalAmount" Then
                        objInvoiceItemTaxOutput.equivalenceSurchargeAmount.totalAmount = New DoubleTwoDecimalType
                        objInvoiceItemTaxOutput.equivalenceSurchargeAmount.totalAmount.value = str2Dbl(nodoNieto.InnerText)
                    End If
                    If nodoNieto.Name = "EquivalentInEuros" Then
                        objInvoiceItemTaxOutput.equivalenceSurchargeAmount.equivalentInEuros = New DoubleTwoDecimalType
                        objInvoiceItemTaxOutput.equivalenceSurchargeAmount.equivalentInEuros.value = str2Dbl(nodoNieto.InnerText)
                    End If
                    contadorNietos = contadorNietos + 1
                End While
            End If
            contadorHijos = contadorHijos + 1
        End While
    End Sub
    ''' <summary>
    ''' Transformación de un nodo raiz AdditionalData con su estructura interna a un objeto AdditionalType
    ''' </summary>
    ''' <remarks>Llamado desde: CFacturae.invoiceXML2Obj
    ''' Tiempo máximo: menor a 0,1
    ''' Revisado por: auv. ¿?/05/2012</remarks>
    Private Sub invoiceAdditionalDataXML2Obj(ByRef objInvoiceAdditionalData As AdditionalDataType, ByRef nodoInvoiceAdditionalData As XmlNode)
        Dim contadorHijos As Integer = Nothing
        Dim contadorNietos As Integer = Nothing
        Dim nodoHijo As XmlNode = Nothing
        Dim nodoNieto As XmlNode = Nothing

        contadorHijos = 0
        While contadorHijos < nodoInvoiceAdditionalData.ChildNodes.Count
            nodoHijo = nodoInvoiceAdditionalData.ChildNodes(contadorHijos)
            If nodoHijo.Name = "InvoiceAdditionalInformation" Then
                objInvoiceAdditionalData.invoiceAdditionalInformation = nodoHijo.InnerText
            End If
            contadorHijos = contadorHijos + 1
        End While
    End Sub
#End Region
#Region "Funciones Auxiliares"
    ''' <summary>
    ''' Transforma un objeto Date de entrada a un String que representa una fecha en un XML que sigue el estándar
    ''' Facturae v3.2.
    ''' </summary>
    ''' <param name="objIdDate">Objeto que contiene la fecha a transformar.</param>
    ''' <returns>String que representa una fecha en un XML que sigue el estándar Facturae v3.2.</returns>
    ''' <remarks>Llamado desde: CFacturae.invoiceIssueDataObj2XML, CFacturae.invoiceItemObj2XML
    ''' Tiempo máximo: menor a 0,1
    ''' Revisado por: auv. 28/05/2012</remarks>
    Private Function date_ObjId2XMLStr(ByVal objIdDate As Date) As String
        Dim XMLStrDate As String = Nothing

        XMLStrDate = objIdDate.Day.ToString
        While XMLStrDate.Length < 2
            XMLStrDate = "0" & XMLStrDate
        End While
        XMLStrDate = objIdDate.Month.ToString & "-" & XMLStrDate
        While XMLStrDate.Length < 5
            XMLStrDate = "0" & XMLStrDate
        End While
        XMLStrDate = objIdDate.Year.ToString & "-" & XMLStrDate
        While XMLStrDate.Length < 10
            XMLStrDate = "0" & XMLStrDate
        End While

        Return XMLStrDate
    End Function
    ''' <summary>
    ''' Transforma un String que representa una fecha en un XML que sigue el estándar Facturae v3.2, a un objeto
    ''' Date.
    ''' </summary>
    ''' <param name="XMLStrDate">String que representa una fecha en un XML que sigue el estándar Facturae v3.2.</param>
    ''' <returns>Objeto Date que contiene la fecha transformada.</returns>
    ''' <remarks>Llamado desde: CFacturae.invoiceCorrectiveTaxPeriodXML2Obj, CFacturae.invoiceIssueDataXML2Obj, CFacturae.invoiceItemXML2Obj
    ''' Tiempo máximo: menor a 0,1
    ''' Revisado por: auv. 28/05/2012</remarks>
    Private Function date_XMLStr2ObjId(ByVal XMLStrDate As String) As Date
        Dim objIdDate As Date = Nothing

        objIdDate = New Date
        objIdDate = XMLStrDate

        Return objIdDate
    End Function
    ''' <summary>
    ''' Transforma un número no entero contenido en un String a uno contenido en un double.
    ''' </summary>
    ''' <param name="sValue">Número no entero contenido en un String</param>
    ''' <returns>Número no entero contenido en un double</returns>
    ''' <remarks>Llamado desde: CFacturae.construirBatch, CFacturae.construirItems, 
    ''' CFacturae.construirInvoiceTotals, CFacturae.batchXML2Obj, CFacturae.invoiceIssueDataXML2Obj, 
    ''' CFacturae.invoiceTaxOutputXML2Obj, CFacturae.invoiceTaxWithheldXML2Obj, CFacturae.invoiceTotalsXML2Obj, 
    ''' CFacturae.invoiceItemXML2Obj, CFacturae.invoiceItemTaxWithheldXML2Obj, 
    ''' CFacturae.invoiceItemTaxOutputXML2Obj, CFacturae.redondearHaciaArriba.
    ''' Tiempo máximo: menor a 0,1
    ''' Revisado por: auv. 28/05/2012</remarks>
    Private Function str2Dbl(ByVal sValue As String) As Double
        Dim resultado As Double = 0

        resultado = CDbl((sValue.Replace(",", obtSeparadorDbl)).Replace(".", obtSeparadorDbl))

        Return resultado
    End Function
    ''' <summary>
    ''' Transforma un double a un String, dejando como caracter separador del String el caracter indicado por parámetro
    ''' </summary>
    ''' <param name="dValue">Número no entero a transformar a String</param>
    ''' <param name="separador">Caracter separador de decimales a emplear en el String a devolver</param>
    ''' <returns>String que representa un Número no entero</returns>
    ''' <remarks>Llamado desde: CFacturae.batchXML2Obj, CFacturae.invoiceIssueDataXML2Obj, 
    ''' CFacturae.invoiceTaxOutputXML2Obj, CFacturae.invoiceTaxWithheldXML2Obj, 
    ''' CFacturae.invoiceTotalsXML2Obj, CFacturae.invoiceItemXML2Obj, 
    ''' CFacturae.invoiceItemTaxWithheldXML2Obj, CFacturae.invoiceItemTaxOutputXML2Obj, 
    ''' CFacturae.redondearHaciaArriba.
    ''' Tiempo máximo: menor a 0,1
    ''' Revisado por: auv. 28/05/2012</remarks>
    Private Function dbl2Str(ByVal dValue As Double, ByVal separador As String) As String
        Dim resultado As String = Nothing

        resultado = CStr(dValue).Replace(obtSeparadorDbl, separador)

        Return resultado
    End Function
    ''' <summary>
    ''' Devuelve un String que representa el caracter separador que emplea el tipo double para distinguir cuándo 
    ''' empiezan los decimales.
    ''' </summary>
    ''' <returns>Se devuelve un caracter que resulta ser el caracter que separa las unidades enteras de las decimales, 
    ''' en los double</returns>
    ''' <remarks>Llamado desde:  CFacturae.str2Dbl, CFacturae.dbl2Str
    ''' Tiempo máximo: menor a 0,1
    ''' Revisado por: auv. 28/05/2012</remarks>
    Private Function obtSeparadorDbl() As String
        Dim resultado As String = Nothing

        If CDbl("0,1") = CDbl("1") Then
            resultado = "."
        Else
            resultado = ","
        End If

        Return resultado
    End Function
    ''' <summary>
    ''' Se trunca un String de entrada que representa un número no entero, dejando como mucho el mismo número de decimales
    ''' que se indican por parámetro. El redondeo se efectúa al alza.
    ''' </summary>
    ''' <param name="valor">Valor a truncar.</param>
    ''' <param name="numeroDeDecimales">Número de decimales máximo que puede contener el valor truncado a devolver.</param>
    ''' <returns>Valor truncado.</returns>
    ''' <remarks>Llamado desde: CFacturae.construirBatch, CFacturae.construirItems, CFacturae.construirTaxesOutputs, 
    ''' CFacturae.construirTaxesWithheld, CFacturae.construirInvoiceTotals, CFacturae.redondearAlProximo
    ''' Tiempo máximo: menor a 0,1
    ''' Revisado por: auv. 28/05/2012</remarks>
    Private Function redondearHaciaArriba(ByVal valor As String, ByVal numeroDeDecimales As Integer) As String
        Dim redondeado As String = Nothing
        Dim contador As Integer = Nothing
        Dim haySeparador As Boolean = Nothing
        Dim separador As String = Nothing

        haySeparador = False
        redondeado = valor
        contador = 1
        While contador <= redondeado.Count And Not haySeparador
            If Mid(redondeado, contador, 1) = "," Or Mid(redondeado, contador, 1) = "." Then
                haySeparador = True
                separador = Mid(redondeado, contador, 1)
            End If
            contador = contador + 1
        End While

        If haySeparador Then
            redondeado = dbl2Str(Math.Round((str2Dbl(redondeado)), numeroDeDecimales, MidpointRounding.AwayFromZero), separador)
            If redondeado < valor Then
                redondeado = dbl2Str(str2Dbl(redondeado) + Math.Pow(10, -numeroDeDecimales), separador)
            End If
        End If

        Return redondeado
    End Function
    ''' <summary>
    ''' Se trunca un String de entrada que representa un número no entero, dejando como mucho el mismo número de decimales
    ''' que se indican por parámetro. El redondeo se efectúa a la baja.
    ''' </summary>
    ''' <param name="valor">Valor a truncar.</param>
    ''' <param name="numeroDeDecimales">Número de decimales máximo que puede contener el valor truncado a devolver.</param>
    ''' <returns>Valor truncado.</returns>
    ''' <remarks>Llamado desde: CFacturae.construirBatch, CFacturae.construirItems, CFacturae.construirTaxesOutputs, 
    ''' CFacturae.construirTaxesWithheld, CFacturae.construirInvoiceTotals, CFacturae.redondearAlProximo
    ''' Tiempo máximo: menor a 0,1
    ''' Revisado por: auv. 06/05/2013</remarks>
    Private Function redondearHaciaAbajo(ByVal valor As String, ByVal numeroDeDecimales As Integer) As String
        Dim redondeado As String = Nothing
        Dim contador As Integer = Nothing
        Dim haySeparador As Boolean = Nothing
        Dim separador As String = Nothing

        haySeparador = False
        redondeado = valor
        contador = 1
        While contador <= redondeado.Count And Not haySeparador
            If Mid(redondeado, contador, 1) = "," Or Mid(redondeado, contador, 1) = "." Then
                haySeparador = True
                separador = Mid(redondeado, contador, 1)
            End If
            contador = contador + 1
        End While

        If haySeparador Then
            redondeado = dbl2Str(Math.Round((str2Dbl(redondeado)), numeroDeDecimales, MidpointRounding.AwayFromZero), separador)
            If redondeado > valor Then
                redondeado = dbl2Str(str2Dbl(redondeado) - Math.Pow(10, -numeroDeDecimales), separador)
            End If
        End If

        Return redondeado
    End Function
    ''' <summary>
    ''' Se trunca un String de entrada que representa un número no entero, dejando como mucho el mismo número de decimales
    ''' que se indican por parámetro. El redondeo se efectúa al número más próximo al original.
    ''' </summary>
    ''' <param name="valor">Valor a truncar.</param>
    ''' <param name="numeroDeDecimales">Número de decimales máximo que puede contener el valor truncado a devolver.</param>
    ''' <returns>Valor truncado.</returns>
    ''' <remarks>Llamado desde: CFacturae.construirBatch, CFacturae.construirInvoiceIssueData
    ''' Tiempo máximo: menor a 0,1
    ''' Revisado por: auv. 28/05/2012</remarks>
    Private Function redondearAlProximo(ByVal valor As String, ByVal numeroDeDecimales As Integer) As String
        Dim redondeado As String = Nothing
        Dim contador As Integer = Nothing
        Dim haySeparador As Boolean = Nothing
        Dim separador As String = Nothing

        haySeparador = False
        redondeado = valor
        contador = 1
        While contador <= redondeado.Count And Not haySeparador
            If Mid(redondeado, contador, 1) = "," Or Mid(redondeado, contador, 1) = "." Then
                haySeparador = True
                separador = Mid(redondeado, contador, 1)
            End If
            contador = contador + 1
        End While
        If haySeparador Then
            redondeado = redondearHaciaArriba(valor, numeroDeDecimales)
            If ((CDbl(redondeado) - CDbl(valor)) * 2) > Math.Pow(10, -numeroDeDecimales) Then
                redondeado = dbl2Str(str2Dbl(redondeado) - Math.Pow(10, -numeroDeDecimales), separador)
            End If
        End If

        Return redondeado
    End Function
    ''' <summary>
    ''' Esta función toma como entrada un valor string que representa un código de la ISO 3166 1 2006 Alpha 2 y devuelve un String que 
    ''' representa si el pais parámetro es España (R), es comunitario (U, salvo España) o no es comunitario (E)
    ''' </summary>
    ''' <param name="facturaeCountryTypeA2">Código de la ISO 3166 1 2006 Alpha 2 que representa un país</param>
    ''' <returns>Un string que indica si es nacional, comunitario o extracomunitario</returns>
    ''' <remarks>Llamado desde: CFacturae.construirSellerParty
    ''' Tiempo máximo: menor a 0,1
    ''' Revisado por: auv. 30/05/2012</remarks>
    Private Function facturaeCountryTypeISO3166_1_2006_Alpha_2_ToFacturaeResidenceTypeCodeType(ByVal facturaeCountryTypeA2 As String)
        Dim facturaeResidenceTypeCodeType As String = ""

        Select Case facturaeCountryTypeA2
            Case "ES"
                facturaeResidenceTypeCodeType = ResidenceTypeCodeType.RESIDENTE_EN_ESPANYA_R
            Case "RO", "PT", "IT", "GR", "FR", "DE", _
                    "SK", "PL", "MT", "LT", "LV", "IE", _
                    "HU", "FI", "EE", "SI", "CY", "BG", _
                    "AT", "CZ", "GB", _
                    "SE", "NL", "DK", "BE", "LU"
                facturaeResidenceTypeCodeType = ResidenceTypeCodeType.RESIDENTE_EN_UNION_EUROPEA_U
            Case Else
                facturaeResidenceTypeCodeType = ResidenceTypeCodeType.EXTRANJERO_E
        End Select

        Return facturaeResidenceTypeCodeType
    End Function
    ''' <summary>
    ''' Esta función toma como entrada un valor string que representa un código de la ISO 3166 1 2006 Alpha 3 y devuelve un String que 
    ''' representa si el pais parámetro es España (R), es comunitario (U, salvo España) o no es comunitario (E)
    ''' </summary>
    ''' <param name="facturaeCountryTypeA3">Código de la ISO 3166 1 2006 Alpha 3 que representa un país</param>
    ''' <returns>Un string que indica si es nacional, comunitario o extracomunitario</returns>
    ''' <remarks>Llamado desde: CFacturae.construirBuyerParty
    ''' Tiempo máximo: menor a 0,1
    ''' Revisado por: auv. 30/05/2012</remarks>
    Private Function facturaeCountryTypeISO3166_1_2006_Alpha_3_ToFacturaeResidenceTypeCodeType(ByVal facturaeCountryTypeA3 As String)
        Dim facturaeResidenceTypeCodeType As String = ""

        Select Case facturaeCountryTypeA3
            Case "ESP"
                facturaeResidenceTypeCodeType = ResidenceTypeCodeType.RESIDENTE_EN_ESPANYA_R
            Case "ROU", "PRT", "ITA", "GRC", "FRA", "DEU", _
                  "SVK", "POL", "MLT", "LTU", "LVA", "IRL", _
                  "HUN", "FIN", "EST", "SVN", "CYP", "BGR", _
                  "AUT", "CZE", "GBR", _
                  "SWE", "NLD", "DNK", "BEL", "LUX"
                facturaeResidenceTypeCodeType = ResidenceTypeCodeType.RESIDENTE_EN_UNION_EUROPEA_U
            Case Else
                facturaeResidenceTypeCodeType = ResidenceTypeCodeType.EXTRANJERO_E
        End Select

        Return facturaeResidenceTypeCodeType
    End Function
    ''' <summary>
    ''' Esta función toma como entrada un valor string que representa un número. Se rellena con X decimales cero, 
    ''' empleando como separador el caracter parámetro y rellenando hasta X decimales siendo la X el segundo parámetro.
    ''' </summary>
    ''' <param name="valor">Valor a rellenar</param>
    ''' <param name="numRell">Hasta cuántos carácteres decimales se rellena</param>
    ''' <param name="separador">Caracter separador</param>
    ''' <returns>Valor rellenado</returns>
    ''' <remarks>Llamado desde: CFacturae.batchObj2XML, CFacturae.invoiceIssueDataObj2XML, CFacturae.invoiceTaxesOutputsObj2XML, 
    ''' CFacturae.invoiceTaxesWithheldObj2XML, CFacturae.invoiceTotalsObj2XML, CFacturae.invoiceItemObj2XML, 
    ''' CFacturae.invoiceItemTaxesWithheldObj2XML, CFacturae.invoiceItemTaxesOutputsObj2XML
    ''' Tiempo máximo: menor a 0,1
    ''' Revisado por: auv. 07/05/2013</remarks>
    Private Function rellenarDecimales(ByVal valor As String, ByVal numRell As Integer, ByVal separador As String) As String
        Dim haySeparador As Boolean
        Dim posicionSeparador As Integer
        Dim contador As Integer
        Dim rellenado As String

        rellenado = valor
        haySeparador = False
        posicionSeparador = -1
        contador = 1
        While contador <= rellenado.Count And Not haySeparador
            If Mid(rellenado, contador, 1) = separador Then
                haySeparador = True
                posicionSeparador = contador
            Else
                contador = contador + 1
            End If
        End While

        If haySeparador Then
            contador = numRell - (rellenado.Length - posicionSeparador)
            While contador > 0
                rellenado = rellenado & "0"
                contador = contador - 1
            End While
        Else
            contador = 0
            rellenado = rellenado & separador
            While contador < numRell
                rellenado = rellenado & "0"
                contador = contador + 1
            End While
        End If

        Return rellenado
    End Function
#End Region
End Class
