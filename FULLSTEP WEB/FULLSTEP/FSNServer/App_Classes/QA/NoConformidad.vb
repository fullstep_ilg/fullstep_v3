Imports System.Threading
<Serializable()> _
    Public Class NoConformidad
    Inherits Security

    Private mlID As Long
    Private mlEstado As Long
    Private mdFecAlta As Date
    Private mdFecLimResol As Date
    Private mdFecCierre As Date 'A�ADIDO VERSION 31000
    Private mlVersion As Long
    Private msProveedor As String
    Private msProveDen As String
    Private msProveedorERP As String
    Private msProveedorERPDen As String
    Private mbProveBaja As Boolean
    Private msContactoDEN As String 'A�adido para obtener en nombre del contacto para el Webservice (Evitar as� una lectura)
    Private msNotificadosProveedor As String
    Private msNotificadosInternos As String
    Private mlInstancia As Long
    Private moData As DataSet
    Private msPeticionario As String
    Private msRevisor As String 'A�ADIDO QA FIGURA DEL REVISOR
    Private msComentCierre As String 'A�ADIDO XA LA FIGURA DEL REVISOR
    Private msComentAlta As String
    Private msComentCierreBR As String 'A�ADIDO XA LA FIGURA DEL REVISOR
    Private msComentAltaBR As String
    Private msComentAnulacion As String
    Private moAcciones As DataTable
    Private mlUnidadQA As Long
    Private mlDiferenciaUTCServidor As Integer
    Private miUltimaVersion As Integer
    Private m_sEmailEnvioNotificacion As String
    Private m_sProveedorEnvioNotificacion As String
    Private m_sNombreEnvioNotificacion As String
    Private m_lIdInstanciaNotificacion As Long
    Private m_iTipoNotificacion As Integer
    Private msCodPeticionario As String
    Private mlIDSolicitud As Long
    Private msDenSolictud As String
    Private m_sPerUltAcc As String
    Private m_sComentUltAcc As String
    Private m_sAccionUlt As String
    Private moDataEstados As DataTable
    Private mdFecAplicPlan As Date
	Private mlWorkflow As Long
	Private msRevisorNombre As String
#Region "Propiedades"
	Public Property UnidadQA() As Long
        Get
            Return mlUnidadQA
        End Get
        Set(ByVal Value As Long)
            mlUnidadQA = Value
        End Set
    End Property
    Public Property ID() As Long
        Get
            Return mlID
        End Get
        Set(ByVal Value As Long)
            mlID = Value
        End Set
    End Property
    Public Property Estado() As Long
        Get
            Return mlEstado
        End Get
        Set(ByVal Value As Long)
            mlEstado = Value
        End Set
    End Property
    Public Property FechaAlta() As Date
        Get
            Return mdFecAlta
        End Get
        Set(ByVal Value As Date)
            mdFecAlta = Value
        End Set
    End Property
    Public Property FecCierre() As Date
        Get
            Return mdFecCierre
        End Get
        Set(ByVal Value As Date)
            mdFecCierre = Value
        End Set
    End Property
    Public Property FechaLimResol() As Date
        Get
            Return mdFecLimResol
        End Get
        Set(ByVal Value As Date)
            mdFecLimResol = Value
        End Set
    End Property
    Public Property FechaAplicPlan() As Date
        Get
            Return mdFecAplicPlan
        End Get
        Set(ByVal Value As Date)
            mdFecAplicPlan = Value
        End Set
    End Property
    Public Property Version() As Long
        Get
            Return mlVersion
        End Get
        Set(ByVal Value As Long)
            mlVersion = Value
        End Set
    End Property
    Public Property Proveedor() As String
        Get
            Return msProveedor
        End Get
        Set(ByVal Value As String)
            msProveedor = Value
        End Set
    End Property
    Public Property ProveDen() As String
        Get
            Return msProveDen
        End Get
        Set(ByVal Value As String)
            msProveDen = Value
        End Set
    End Property
    Public Property ProveedorERP() As String
        Get
            Return msProveedorERP
        End Get
        Set(ByVal Value As String)
            msProveedorERP = Value
        End Set
    End Property
    Public Property ProveedorERPDen() As String
        Get
            Return msProveedorERPDen
        End Get
        Set(ByVal Value As String)
            msProveedorERPDen = Value
        End Set
    End Property
    Public Property ProveedorBaja() As Boolean
        Get
            Return mbProveBaja
        End Get
        Set(ByVal Value As Boolean)
            mbProveBaja = Value
        End Set
    End Property
    Public Property Peticionario() As String
        Get
            Return msPeticionario
        End Get
        Set(ByVal Value As String)
            msPeticionario = Value
        End Set
    End Property
    Public Property Revisor() As String
        Get
            Return msRevisor
        End Get
        Set(ByVal Value As String)
            msRevisor = Value
        End Set
    End Property
    Public Property ComentAlta() As String
        Get
            Return msComentAlta
        End Get
        Set(ByVal Value As String)
            msComentAlta = Value
        End Set
    End Property
    Public Property ComentCierre() As String
        Get
            Return msComentCierre
        End Get
        Set(ByVal Value As String)
            msComentCierre = Value
        End Set
    End Property
    Public Property ComentAnulacion() As String
        Get
            Return msComentAnulacion
        End Get
        Set(ByVal Value As String)
            msComentAnulacion = Value
        End Set
    End Property
    Public Property ComentAltaBR() As String
        Get
            Return msComentAltaBR
        End Get
        Set(ByVal Value As String)
            msComentAltaBR = Value
        End Set
    End Property
    Public Property ComentCierreBR() As String
        Get
            Return msComentCierreBR
        End Get
        Set(ByVal Value As String)
            msComentCierreBR = Value
        End Set
    End Property
    ''' <summary>
    ''' Obtener/Establecer el Id de la instancia
    ''' </summary>
    ''' <returns>Id de la instancia</returns>
    ''' <remarks>Llamada desde: Toda pantalla q haga uso de la clase NoConformidad; Tiempo maximo:0</remarks>
    Property Instancia() As Long
        Get
            Instancia = mlInstancia
        End Get
        Set(ByVal Value As Long)
            mlInstancia = Value
        End Set
    End Property
    Public ReadOnly Property Data() As Data.DataSet
        Get
            Return moData
        End Get
    End Property
    Public Property Acciones() As DataTable
        Get
            Return moAcciones
        End Get
        Set(ByVal Value As DataTable)
            moAcciones = Value
        End Set
    End Property
    Public Property DiferenciaUTCServidor() As Integer
        Get
            Return mlDiferenciaUTCServidor
        End Get
        Set(ByVal Value As Integer)
            mlDiferenciaUTCServidor = Value
        End Set
    End Property
    Public Property UltimaVersion() As Integer
        Get
            Return miUltimaVersion
        End Get
        Set(ByVal Value As Integer)
            miUltimaVersion = Value
        End Set
    End Property
    Property NotificadosProveedor() As String
        Get
            NotificadosProveedor = msNotificadosProveedor
        End Get
        Set(ByVal Value As String)
            msNotificadosProveedor = Value
        End Set
    End Property
    Property NotificadosInternos() As String
        Get
            NotificadosInternos = msNotificadosInternos
        End Get
        Set(ByVal Value As String)
            msNotificadosInternos = Value
        End Set
    End Property
    Public Property ProveedorEnvioNotificacion() As String
        Get
            Return m_sProveedorEnvioNotificacion
        End Get
        Set(ByVal Value As String)
            m_sProveedorEnvioNotificacion = Value
        End Set
    End Property
    Public Property EmailEnvioNotificacion() As String
        Get
            Return m_sEmailEnvioNotificacion
        End Get
        Set(ByVal Value As String)
            m_sEmailEnvioNotificacion = Value
        End Set
    End Property
    Public Property NombreEnvioNotificacion() As String
        Get
            Return m_sNombreEnvioNotificacion
        End Get
        Set(ByVal Value As String)
            m_sNombreEnvioNotificacion = Value
        End Set
    End Property
    Public Property IdInstanciaNotificacion() As Long
        Get
            Return m_lIdInstanciaNotificacion
        End Get
        Set(ByVal Value As Long)
            m_lIdInstanciaNotificacion = Value
        End Set
    End Property
    Public Property TipoNotificacion() As Integer
        Get
            Return m_iTipoNotificacion
        End Get
        Set(ByVal Value As Integer)
            m_iTipoNotificacion = Value
        End Set
    End Property
    ''' <summary>
    ''' Obtener/Establecer el Peticionario de la No conformidad. El stored de carga devuelve columna NOCONFORMIDAD.PER 
    ''' </summary>
    ''' <returns>Peticionario de la No conformidad</returns>
    ''' <remarks>Llamada desde: Toda pantalla q haga uso de la clase NoConformidad; Tiempo maximo:0</remarks>
    Public Property PeticionarioCod() As String
        Get
            Return msCodPeticionario
        End Get
        Set(ByVal Value As String)
            msCodPeticionario = Value
        End Set
    End Property
    ''' <summary>
    ''' Obtener/Establecer la denominaci�n de la Solicitud, en la q este basada la No conformidad, lo devuelve en el idioma 
    ''' del usuario conectado. El stored de carga devuelve columna SOLICTUD.DEN + @IDI AS DEN
    ''' </summary>
    ''' <returns>Denominaci�n de la Solicitud</returns>
    ''' <remarks>Llamada desde: Toda pantalla q haga uso de la clase NoConformidad; Tiempo maximo:0</remarks>
    Public Property TipoDen() As String
        Get
            Return msDenSolictud
        End Get
        Set(ByVal Value As String)
            msDenSolictud = Value
        End Set
    End Property
    ''' <summary>
    ''' Obtener/Establecer el ID de la Solicitud, en la q este basada la No conformidad.
    ''' El stored de carga devuelve columna SOLICTUD.ID AS IDSOL
    ''' </summary>
    ''' <returns>ID de la Solicitud</returns>
    ''' <remarks>Llamada desde: Toda pantalla q haga uso de la clase NoConformidad; Tiempo maximo:0</remarks>
    Public Property IDSolicitud() As Long
        Get
            Return mlIDSolicitud
        End Get
        Set(ByVal Value As Long)
            mlIDSolicitud = Value
        End Set
    End Property
    ''' <summary>
    ''' Obtener/Establecer la Persona q realizo la ultima acci�n sobre la No conformidad. 
    ''' El stored de carga devuelve columna INSTANCIA_EST.PER 
    ''' </summary>
    ''' <returns>Persona q realizo la ultima acci�n sobre la No conformidad</returns>
    ''' <remarks>Llamada desde: Toda pantalla q haga uso de la clase NoConformidad; Tiempo maximo:0</remarks>
    Public Property AccionEstadoPersona() As String
        Get
            Return m_sPerUltAcc
        End Get
        Set(ByVal Value As String)
            m_sPerUltAcc = Value
        End Set
    End Property
    ''' <summary>
    ''' Obtener/Establecer el comentario de la ultima acci�n sobre la No conformidad. 
    ''' El stored de carga devuelve columna INSTANCIA_EST.COMENT 
    ''' </summary>
    ''' <returns>Comentario de la ultima acci�n sobre la No conformidad</returns>
    ''' <remarks>Llamada desde: Toda pantalla q haga uso de la clase NoConformidad; Tiempo maximo:0</remarks>
    Public Property AccionEstadoComentario() As String
        Get
            Return m_sComentUltAcc
        End Get
        Set(ByVal Value As String)
            m_sComentUltAcc = Value
        End Set
    End Property
    ''' <summary>
    ''' Obtener/Establecer la ultima acci�n sobre la No conformidad. 
    ''' El stored de carga devuelve columna INSTANCIA_EST.ACCION 
    ''' </summary>
    ''' <returns>Ultima acci�n sobre la No conformidad</returns>
    ''' <remarks>Llamada desde: Toda pantalla q haga uso de la clase NoConformidad; Tiempo maximo:0</remarks>
    Public Property AccionEstado() As String
        Get
            Return m_sAccionUlt
        End Get
        Set(ByVal Value As String)
            m_sAccionUlt = Value
        End Set
    End Property
    ''' <summary>
    ''' Obtener/Establecer una tabla con las acciones de la No conformidad. 
    ''' El stored de carga devuelve una SELECT ... FROM NOCONF_ACC NA WITH (NOLOCK) ..., es el ds.tables(2)
    ''' </summary>
    ''' <returns>Tabla con las acciones de la No conformidad</returns>
    ''' <remarks>Llamada desde: Toda pantalla q haga uso de la clase NoConformidad; Tiempo maximo:0</remarks>
    Public ReadOnly Property EstadosComentariosInternosAcciones() As Data.DataTable
        Get
            Return moDataEstados
        End Get
    End Property
    Private _es_Noconformidad_Escalacion As Boolean
    Public Property Es_Noconformidad_Escalacion() As Boolean
        Get
            Return _es_Noconformidad_Escalacion
        End Get
        Set(ByVal value As Boolean)
            _es_Noconformidad_Escalacion = value
        End Set
    End Property
    Public Property Es_Noconformidad_Escalacion_Hist As Boolean
    Private _nivelEscalacion As Integer
    Public Property NivelEscalacion() As Integer
        Get
            Return _nivelEscalacion
        End Get
        Set(ByVal value As Integer)
            _nivelEscalacion = value
        End Set
    End Property
    Private _idEscalacionProve As Long
    Public Property IdEscalacionProve() As Long
        Get
            Return _idEscalacionProve
        End Get
        Set(ByVal value As Long)
            _idEscalacionProve = value
        End Set
    End Property
    Private _EstadoEscalacionProvePdte As Boolean
    Public Property EstadoEscalacionProvePdte() As Boolean
        Get
            Return _EstadoEscalacionProvePdte
        End Get
        Set(ByVal value As Boolean)
            _EstadoEscalacionProvePdte = value
        End Set
    End Property
    Public Property Workflow() As Long
        Get
            Return mlWorkflow
        End Get
        Set(ByVal value As Long)
            mlWorkflow = value
        End Set
    End Property
	Public Property RevisorNombre() As String
		Get
			Return msRevisorNombre
		End Get
		Set(ByVal Value As String)
			msRevisorNombre = Value
		End Set
	End Property
#End Region

	Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
    ''' <summary>
    ''' Carga de las propiedades del objeto NoConformidad
    ''' </summary>
    ''' <param name="sIdi">Idioma</param>
    ''' <param name="iVersion">version a cargar</param>        
    ''' <param name="DesdeImpExp">Solo desde Imprimir/Exportar se usa Coment Alta y Ultima accion por separado, luego, dos select</param>
    ''' <remarks>Llamada desde:cierre.aspx/page_load    cierreok.aspx/page_load     
    ''' cierrerevisor.aspx/page_load            cierrerevisorok.aspx/page_load  
    ''' noconformidad/desglose.aspx/page_load   detallenoconformidad.aspx/page_load     
    ''' noconformidadrevisor.aspx/page_load      reabrirnoconformidad.aspx/page_load     
    ''' impexp.aspx/page_load; Tiempo m�ximo: 0,1</remarks>
    Public Sub Load(ByVal sIdi As String, Optional ByVal iVersion As Integer = 0, Optional ByVal DesdeImpExp As Boolean = False)
        Authenticate()

        Dim data As DataSet
        data = DBServer.NoConformidad_Load(mlID, sIdi, iVersion, DesdeImpExp)

        If Not data.Tables(0).Rows.Count = 0 Then
            With data.Tables(0).Rows(0)
                mdFecLimResol = DBNullToSomething(.Item("FEC_LIM_RESOL"))
                mdFecAplicPlan = DBNullToSomething(.Item("FEC_APLICACION_PLAN"))
                mdFecAlta = DBNullToSomething(.Item("FECALTA"))
                msProveedor = .Item("PROVE")
                msProveDen = DBNullToSomething(.Item("DEN_PROVE"))
                msProveedorERP = DBNullToSomething(.Item("PROVE_ERP"))
                msProveedorERPDen = DBNullToSomething(.Item("PROVE_ERP_DEN"))
                mbProveBaja = SQLBinaryToBoolean(.Item("BAJA_PROVE"))
                mlVersion = DBNullToSomething(.Item("VERSION"))
                mlEstado = DBNullToSomething(.Item("ESTADO"))

                msRevisor = DBNullToSomething(.Item("REVISOR"))
                mdFecCierre = DBNullToSomething(.Item("FEC_CIERRE"))
                msComentCierre = ""
                msComentCierreBR = ""
                msComentAlta = DBNullToSomething(.Item("COMENT_ALTA"))

                miUltimaVersion = DBNullToSomething(.Item("ULTIMA_VERSION"))

                If Not IsDBNull(.Item("COMENT_ALTA")) Then
                    msComentAltaBR = Replace(.Item("COMENT_ALTA"), Chr(10), "<BR/>")
                Else
                    msComentAltaBR = ""
                End If

                mlUnidadQA = DBNullToSomething(.Item("UNQA"))

                mlInstancia = .Item("INSTANCIA")

                msCodPeticionario = DBNullToSomething(.Item("PER"))
                msPeticionario = DBNullToSomething(.Item("NOMBRE"))

                msDenSolictud = DBNullToSomething(.Item("DEN"))
                mlIDSolicitud = DBNullToSomething(.Item("IDSOL"))

                m_sPerUltAcc = DBNullToSomething(.Item("PERSONAULT"))
                m_sComentUltAcc = DBNullToSomething(.Item("COMENTULT"))
                m_sAccionUlt = DBNullToSomething(.Item("ACCIONULT"))
                _es_Noconformidad_Escalacion = SQLBinaryToBoolean(.Item("ES_NCE"))
                Es_Noconformidad_Escalacion_Hist = SQLBinaryToBoolean(.Item("ES_NCE_HIST"))
                _nivelEscalacion = DBNullToInteger(.Item("NIVEL_ESCALACION"))
                _idEscalacionProve = DBNullToInteger(.Item("IDESCALACIONPROVE"))
                _EstadoEscalacionProvePdte = SQLBinaryToBoolean(.Item("ESCALACION_PDTE"))
				mlWorkflow = DBNullToInteger(.Item("WORKFLOW"))
				msRevisorNombre = DBNullToSomething(.Item("NOMBRE_REVISOR"))
			End With

            moDataEstados = data.Tables(2)
            moDataEstados.TableName = "NOCONF_ACC"

            moData = data
        End If

        moAcciones = data.Tables(1)

        data = Nothing
    End Sub
    ''' <summary>
    ''' Procedimiento que carga las versiones de una no conformidad dado su id
    ''' </summary>
    ''' <remarks>
    ''' Llamada desde: PmWeb/detalleNoConformidad/Page_Load,PmWeb/eliminarNoConformidad/Page_Load
    ''' Tiempo m�ximo: 0,1 seg</remarks>
    Public Sub LoadVersiones()
        Authenticate()
        moData = DBServer.NoConformidad_LoadVersiones(mlID)
    End Sub
    ''' <summary>
    ''' Carga de las propiedades del objeto NoConformidad
    ''' </summary>
    ''' <remarks>Llamada desde=noConfEmitidaOK.aspx.vb        cierreRevisorOK.aspx.vb; Tiempo m�ximo: 0,1</remarks>
    Public Sub CargarDatosDesdeInstancia()
        Authenticate()

        moData = DBServer.NoConformidad_DatosGenerales(mlInstancia)

        With Data.Tables(0).Rows(0)
            mlID = DBNullToSomething(.Item("ID"))
            mdFecAlta = DBNullToSomething(.Item("FECALTA"))
            mdFecLimResol = DBNullToSomething(.Item("FEC_LIM_RESOL"))
            mdFecAplicPlan = DBNullToSomething(.Item("FEC_APLICACION_PLAN"))
            msProveedor = .Item("PROVE")
            msProveDen = DBNullToSomething(.Item("DEN_PROVE"))
            mlVersion = DBNullToSomething(.Item("VERSION"))
            mlEstado = DBNullToSomething(.Item("ESTADO"))
            msPeticionario = DBNullToSomething(.Item("NOMBRE"))
            mlDiferenciaUTCServidor = DBNullToSomething(Data.Tables(1).Rows(0).Item("DIFERENCIA"))
            mdFecCierre = DBNullToSomething(.Item("FEC_CIERRE"))
            _es_Noconformidad_Escalacion = SQLBinaryToBoolean(.Item("ES_NCE"))
            mlWorkflow = DBNullToInteger(.Item("WORKFLOW"))
        End With

        For Each row As DataRow In Data.Tables(2).Rows
            msNotificadosProveedor = msNotificadosProveedor & row("CONTACTO_PROVEEDOR") & ", "
        Next
        If Not msNotificadosProveedor Is Nothing Then
            msNotificadosProveedor = Left(msNotificadosProveedor, msNotificadosProveedor.Length - 2)
        End If

        For Each row As DataRow In Data.Tables(3).Rows
            msNotificadosInternos = msNotificadosInternos & row("CONTACTO_INTERNO") & ", "
        Next
        If Not msNotificadosInternos Is Nothing Then
            msNotificadosInternos = Left(msNotificadosInternos, msNotificadosInternos.Length - 2)
        End If
    End Sub
    ''' <summary>
    ''' Devuelve un string compuesto por los nombres de los contactos del proveedor a los que hacer notificaciones.
    ''' Los nombres se obtienen pasando los id de esos contactos
    ''' </summary>
    ''' <param name="sProve">c�digo del proveedor al que pertenecen los contactos</param>
    ''' <param name="sCadenaIdsContactos">cadena de ids de los contactos a mostrar</param>
    ''' <returns></returns>
    ''' <remarks>Llamada desde: cierre.vb</remarks>
    Public Function ObtenerNombreNotificadosProveedor(ByVal sProve As String, ByVal sCadenaIdsContactos As String) As String
        Dim dsContactosProveedor As DataSet
        Dim sListaContactosProveedor As String = String.Empty

        dsContactosProveedor = DBServer.ObtenerNombresContactosProveedor(sProve, sCadenaIdsContactos)

        For Each row As DataRow In dsContactosProveedor.Tables(0).Rows
            If sListaContactosProveedor Is String.Empty Then
                sListaContactosProveedor = row("CONTACTO_PROVEEDOR")
            Else
                sListaContactosProveedor = sListaContactosProveedor & ", " & row("CONTACTO_PROVEEDOR")
            End If
        Next

        Return sListaContactosProveedor
    End Function
    ''' <summary>
    ''' Devuelve un string compuesto por los nombres de los contactos internos a los que hacer notificaciones.
    ''' Los nombres se obtienen pasando los codigos de esos contactos
    ''' </summary>
    ''' <param name="sCadenaCodigosPersonas">cadena de los codigos de los contactos internos</param>
    ''' <returns></returns>
    ''' <remarks>Llamada desde: cierre.vb</remarks>
    Public Function ObtenerNombreNotificadosInternos(ByVal sCadenaCodigosPersonas As String)
        Dim dsContactosInternos As DataSet
        Dim sListaContactosInternos As String = String.Empty

        Dim CodigosPersonas As String() = Split(sCadenaCodigosPersonas, ",")
        sCadenaCodigosPersonas = String.Empty
        For i As Integer = 0 To CodigosPersonas.Length - 1
            If sCadenaCodigosPersonas = String.Empty Then
                sCadenaCodigosPersonas = "'" & CodigosPersonas(i) & "'"
            Else
                sCadenaCodigosPersonas = sCadenaCodigosPersonas & ",'" & CodigosPersonas(i) & "'"
            End If
        Next

        dsContactosInternos = DBServer.ObtenerNombresContactosInternos(sCadenaCodigosPersonas)

        For Each row As DataRow In dsContactosInternos.Tables(0).Rows
            If sListaContactosInternos Is String.Empty Then
                sListaContactosInternos = row("CONTACTO_INTERNO")
            Else
                sListaContactosInternos = sListaContactosInternos & ", " & row("CONTACTO_INTERNO")
            End If
        Next

        Return sListaContactosInternos
    End Function
    ''' <summary>
    ''' Borrar una versi�n de la no conformidad
    ''' </summary>
    ''' <param name="sPer">Persona q elimina</param>
    ''' <param name="sFrom">Email del usuario q elimino</param> 
    ''' <param name="NotificarPortal">Notificar el borrado al portal</param>        
    ''' <remarks>Llamada desde:EliminarVersionProveedor.aspx; Tiempo m�ximo:0,1</remarks>
    Public Sub EliminarVersionesNoConformidad(ByVal sPer As String, Optional ByVal sFrom As String = "", Optional ByVal NotificarPortal As Boolean = False)
        Authenticate()

        DBServer.NoConformidad_EliminarVersiones(mlID, mlInstancia, mlVersion, sPer, Es_Noconformidad_Escalacion)
        If NotificarPortal Then
            Dim oNotificador As New Notificar(DBServer, mIsAuthenticated)

            oNotificador.NotificacionBorradoPortal(mlInstancia, mlID, sFrom, True)
            oNotificador.FuerzaFinalizeSmtpClient()
            oNotificador = Nothing
        End If
    End Sub
    ''' <summary>
    ''' Reabre una NoConformidad
    ''' </summary>
    ''' <param name="sEMail">Mail del usuario que realiza la accion</param>     
    ''' <remarks>Llamada desde:=DetalleNoConformidad.aspx; Tiempo m�ximo:0,2seg</remarks>
    Public Sub NoConformidad_Reabrir(ByVal sEmail As String, Optional ByVal sProve As String = Nothing,
                                     Optional ByVal NotificadosProveedor As String = "", Optional ByVal NotificadosInternos As String = "")
        Authenticate()

        DBServer.NoConformidad_Reabrir(mlID, mlInstancia, sProve, NotificadosProveedor, NotificadosInternos)
        If sEmail <> "" Then
            Dim oNotificador As New Notificar(DBServer, mIsAuthenticated)
            oNotificador.NotificacionReaperturaNoConf(mlInstancia, mlID, sEmail)
            oNotificador.NotificacionReaperturaNoConfContactosInternos(mlInstancia, mlID, sEmail)
            oNotificador.FuerzaFinalizeSmtpClient()
            oNotificador = Nothing
        End If
    End Sub
    ''' <summary>
    ''' Almacena el comentario de cierre y cambia el estado de la noCOnformidad al estado que corresponda.
    ''' </summary>
    ''' <param name="bCierrePos">Si se trata de cierre positivo o no</param>
    ''' <param name="sEmail">Email del usuario del que realiza la accion</param>
    ''' <param name="sComent">Comentario</param>
    ''' <param name="bRevisor">Si se trata del Revisor o no (Peticionario)</param>
    ''' <param name="sPer">Codigo persona del que esta haciendo la accion</param>        
    ''' <remarks>Llamada desde=Cierre.aspx.vb --> cmdAceptar_ServerClick; Tiempo m�ximo=0,2seg.</remarks>
    Public Sub CierreNoConformidad(ByVal bCierrePos As Boolean, ByVal sEmail As String, Optional ByVal sComent As String = Nothing,
                                   Optional ByVal bRevisor As Boolean = False, Optional ByVal sPer As String = Nothing,
                                   Optional ByVal NotificadosProveedor As String = "", Optional ByVal NotificadosInternos As String = "")
        Authenticate()

        DBServer.NoConformidad_Cierre(mlInstancia, mlID, sPer, bRevisor, bCierrePos, _es_Noconformidad_Escalacion, mlEstado, sComent, NotificadosProveedor, NotificadosInternos)
        'REVISOR SI EXISTE
        If bRevisor Then
            Dim oNotificador As New Notificar(DBServer, mIsAuthenticated)
            'Existe revisor. Ahora puede ser positivo o negativo
            'el peticionario cierre la noconformidad y manda un mail al revisor que tiene
            'una noconformidad xa revisar
            'No se envia a notificados internos porque falta la aprobaci�n del revisor para que el cierre sea definitivo
            If bCierrePos Then
                oNotificador.NotificacionCierrePositivoRevisor(mlInstancia, mlID, sEmail)
            Else
                oNotificador.NotificacionCierreNegativoRevisor(mlInstancia, mlID, sEmail)
            End If
            oNotificador.FuerzaFinalizeSmtpClient()
            oNotificador = Nothing
        Else
            'No existe revisor se le envia al proveedor
            'la noConformidad se ha cursado sin revisores
            'Tambi�n se notifica a los notificados internos
            Dim oNotificador As New Notificar(DBServer, mIsAuthenticated)
            If bCierrePos Then
                'Si se trata del cierre eficaz de una NC de escalaci�n
                If _es_Noconformidad_Escalacion Then
                    If mlEstado = TipoEstadoNoConformidad.CierrePosPendiente Then
                        DBServer.NoConformidad_CierreEficazNCE(mlID, sComent)
                        oNotificador.NotificacionCierrePositivo(mlInstancia, mlID, sEmail)
                        oNotificador.NotificacionCierrePositivoContactosInternos(mlInstancia, mlID, sEmail)
                    End If
                Else
                    oNotificador.NotificacionCierrePositivo(mlInstancia, mlID, sEmail)
                    oNotificador.NotificacionCierrePositivoContactosInternos(mlInstancia, mlID, sEmail)
                End If
            Else
                'Si se trata del cierre no eficaz de una NC de escalaci�n
                If _es_Noconformidad_Escalacion Then CierreNoEficazNCE()

                oNotificador.NotificacionCierreNegativo(mlInstancia, mlID, sEmail)
                oNotificador.NotificacionCierreNegativoContactosInternos(mlInstancia, mlID, sEmail)
            End If
            If mlEstado = TipoEstadoNoConformidad.CierrePosSinRevisar OrElse mlEstado = TipoEstadoNoConformidad.CierreNegSinRevisar Then
                'Cuando el revisor aprueba el cierre de la no conformidad
                If bCierrePos Then
                    oNotificador.NotificacionAprobacionCierrePosRevisor(mlInstancia, mlID, sEmail)
                Else
                    oNotificador.NotificacionAprobacionCierreNegRevisor(mlInstancia, mlID, sEmail)
                End If
            End If
            oNotificador.FuerzaFinalizeSmtpClient()
            oNotificador = Nothing
        End If
    End Sub
    ''' <summary>Lleva a cabo las acciones correspondientes al cierre no eficaz de una NCE</summary>
    ''' <remarks>Llamada desde: CierreNoConformidad</remarks>
    Private Sub CierreNoEficazNCE()
        'Obtener los datos de la escalaci�n de la NC
        Dim dsDatosEsc As DataSet = DBServer.NoConformidad_DevolverDatosEscalacionNC(mlID)

        If dsDatosEsc IsNot Nothing AndAlso dsDatosEsc.Tables.Count > 0 Then
            'Obtener datos niveles escalaci�n   
            Dim oEsc As New Escalaciones(DBServer, mIsAuthenticated)
            Dim oNivelesEsc As List(Of Escalacion) = oEsc.GetNivelesEscalacion()

            'Establecer el nuevo nivel de escalaci�n
            Dim iNivelAct As Integer = dsDatosEsc.Tables(0).Rows(0)("NIVEL_ESCALACION") + 1

            If iNivelAct <= 4 Then   'Si ya est� escalado a nivel 4 no se hace nada (no se puede escalar m�s) 
                'Antes de proponer una escalaci�n de nivel superior mirar si no hay ya una propuesta de nivel superior a una NCE abierta de nivel superior
                Dim iNivel As Integer = oNivelesEsc.Find(Function(o) (o.Nivel = dsDatosEsc.Tables(0).Rows(0)("NIVEL_ESCALACION"))).NivelUNQA
                If Not DBServer.NoConformidad_ExisteNCEoPropuestaNivelSup(msProveedor, dsDatosEsc.Tables(0).Rows(0)("NIVEL_ESCALACION"), dsDatosEsc.Tables(0).Rows(0)(CType(iNivel, String))) Then
                    Dim iNivelEscalar As Integer = oNivelesEsc.Find(Function(o) (o.Nivel = iNivelAct)).NivelUNQA
                    Dim oEscalaciones As New ProvesEscalaciones
                    oEscalaciones.AgregarEscalacion(msProveedor, dsDatosEsc.Tables(0).Rows(0)("NIF"), msProveDen, iNivelAct, dsDatosEsc.Tables(0).Rows(0)(CType(iNivelEscalar, String)),
                                                dsDatosEsc.Tables(0).Rows(0)("UNQA_COD"), mlID)
                    DBServer.Proveedores_InsertarProveedoresEscalados(oEscalaciones.Datos)

                    'Notificaciones                
                    Dim oNotificador As New Notificar(DBServer, mIsAuthenticated)
                    oNotificador.NotificarEscalaciones(oNivelesEsc, oEscalaciones.Datos, Date.Today)
                End If
            End If
        End If
    End Sub
    ''' <summary>
    ''' Funcion que rechaza una no conformidad
    ''' </summary>
    ''' <param name="bpositivo">Variable booleana que indica si es positivo el rechazo</param>
    ''' <param name="sText">Texto del cierre</param>
    ''' <param name="sFrom">Quien la ha cerrado</param>
    ''' <param name="sRevisor">Codigo del revisor</param>
    ''' <remarks>
    ''' Llamada desde: PmWeb/cierreRevisorOK/cmdAceptar_ServerClick
    ''' Tiempo m�ximo: 0,2 seg</remarks>
    Public Sub RechazarNoConformidadPorRevisor(ByVal bpositivo As Boolean, ByVal sText As String, ByVal sFrom As String, ByVal sRevisor As String)
        'Esta funcion cambia el estado de la noconformidad
        'se le envia un mail al peticionario informandole de lo q ha sucedido
        'El primer parametro indica si rechaza un cierre eficaz o no eficaz
        Authenticate()

        DBServer.NoConformidad_RechazadaPorRevisor(mlInstancia, mlEstado, sText, sRevisor)
        'Se le envia el mail al peticionario
        Dim oNotificador As New Notificar(DBServer, mIsAuthenticated)
        If bpositivo Then
            oNotificador.NotificacionRechazoCierrePosRevisor(mlInstancia, mlID, sFrom)
        Else
            oNotificador.NotificacionRechazoCierreNegRevisor(mlInstancia, mlID, sFrom)
        End If
        oNotificador.FuerzaFinalizeSmtpClient()
        oNotificador = Nothing
    End Sub
    ''' <summary>
    ''' Crear la no conformidad
    ''' </summary>
    ''' <param name="bEmitir">guardar nc 0 /emitir nc 1</param>
    ''' <remarks>Llamada desde:guardarinstancia.aspx</remarks>
    Public Sub Create_Prev(ByVal bEmitir As Boolean, Optional ByVal idWorkflow As Long = Nothing)
        Authenticate()
        DBServer.NoConformidad_Create_Prev(mlID, msProveedor, msNotificadosProveedor, msNotificadosInternos, Me.FechaLimResol, Me.Peticionario, Me.Revisor, Me.mlInstancia, Me.mlIDSolicitud, bEmitir, mlUnidadQA, msProveedorERP, msProveedorERPDen, mdFecAplicPlan, idWorkflow)
    End Sub
    ''' <summary>
    ''' Notifica que se ha enviado una No Conformidad desde portal
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub NotificarEnvioNoConformidadPortal()
        Authenticate()

        Dim data As DataSet
        Dim oRow As DataRow
        'Primero obtenemos la Versi�n de la No Conformidad
        data = DBServer.NoConformidad_DevolverVersion(mlID)

        oRow = data.Tables(0).Rows(0)
        mlVersion = oRow.Item("VERSION")

        ThreadPool.QueueUserWorkItem(New WaitCallback(AddressOf NotificarEnvioNoConformidadDesdePortal))
    End Sub
    ''' <summary>
    ''' Se guardan los cambios efectuados en la No Conformidad
    ''' </summary>
    ''' <param name="bEmitir">Si adem�s de guardar, se emite la no conformidad</param>
    ''' <remarks></remarks>
    Public Sub Save_Prev(ByVal bEmitir As Boolean)
        Authenticate()
        DBServer.NoConformidad_Save_Prev(mlID, msProveedor, msNotificadosProveedor, msNotificadosInternos, Me.FechaLimResol, Me.Revisor, bEmitir, mlUnidadQA, msProveedorERP, msProveedorERPDen, mdFecAplicPlan)
    End Sub
    ''' <summary>
    ''' Obtiene los contactos del proveedor. Adem�s los recuperamos con un campo SELECCIONADO que nos indica
    ''' si es uno de los contactos del proveedor a los que se le notificara por medio de mails.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Load_Notificados_Proveedor() As DataSet
        Authenticate()
        Return DBServer.Load_Notificados_Proveedor(mlID, msProveedor)
    End Function
    ''' <summary>
    ''' Devuelve los contactos internos seleccionados para ser notificados por medio de mail
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Load_Notificados_Internos() As DataSet
        Authenticate()
        Return DBServer.Load_Notificados_Internos(mlID)
    End Function
    ''' <summary>
    ''' Llama al notificador para generar el envio de mails a las personas pertinentes.
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub NotificarNoConformidadQA()
        Select Case TipoNotificacion
            Case 1
                ThreadPool.QueueUserWorkItem(New WaitCallback(AddressOf NotificarEmisionNoConformidad))
            Case 2
                ThreadPool.QueueUserWorkItem(New WaitCallback(AddressOf NotificarModificacionEstadosNoConformidad))
        End Select
    End Sub
    ''' <summary>
    ''' Dado un Id de instancia devuelve los mail que se han enviado a lo largo de su tratamiento
    ''' </summary>
    ''' <param name="lInstancia">El Id de la instancia para el que se quiere obtener el registro de mails</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Load_Registro_Emails_NoConformidad(ByVal lInstancia As Long)
        Authenticate()
        Return DBServer.Load_Registro_Emails_NoConformidad(lInstancia)
    End Function
    ''' <summary>
    ''' Notificar Envio NoConformidad Desde Portal
    ''' </summary>
    ''' <remarks>Llamada desde:=DetalleNoConformidad.aspx; Tiempo m�ximo:0,2seg</remarks>
    Private Sub NotificarEnvioNoConformidadDesdePortal()
        Dim oNotificador As New Notificar(DBServer, mIsAuthenticated, , True)

        oNotificador.NotificacionEnvioNoConformidad(m_sProveedorEnvioNotificacion, m_lIdInstanciaNotificacion, mlID, mlVersion, m_sEmailEnvioNotificacion, m_sNombreEnvioNotificacion)

        oNotificador.FuerzaFinalizeSmtpClient()
        oNotificador = Nothing
    End Sub
    ''' <summary>
    ''' Notificar Emision NoConformidad
    ''' </summary>
    ''' <remarks>Llamada desde:=DetalleNoConformidad.aspx; Tiempo m�ximo:0,2seg</remarks>
    Private Sub NotificarEmisionNoConformidad()
        Dim oNotificador As New Notificar(DBServer, mIsAuthenticated)

        oNotificador.NotificacionNCEmitidaProveedor(m_lIdInstanciaNotificacion, mlID, m_sEmailEnvioNotificacion)
        oNotificador.NotificacionNCEmitidaContactosInternos(m_lIdInstanciaNotificacion, mlID, m_sEmailEnvioNotificacion)
        oNotificador.NotificacionNCEmitidaRevisor(m_lIdInstanciaNotificacion, mlID, m_sEmailEnvioNotificacion)

        oNotificador.FuerzaFinalizeSmtpClient()
        oNotificador = Nothing
    End Sub
    ''' <summary>
    ''' Notificar Modificacion Estados NoConformidad
    ''' </summary>
    ''' <remarks>Llamada desde:=DetalleNoConformidad.aspx; Tiempo m�ximo:0,2seg</remarks>
    Private Sub NotificarModificacionEstadosNoConformidad()
        Dim oNotificador As New Notificar(DBServer, mIsAuthenticated)

        oNotificador.NotificacionNCModEstadosAccionesProveedor(m_lIdInstanciaNotificacion, mlID, m_sEmailEnvioNotificacion)
        oNotificador.NotificacionNCModEstadosAccionesContactosInternos(m_lIdInstanciaNotificacion, mlID, m_sEmailEnvioNotificacion)

        oNotificador.FuerzaFinalizeSmtpClient()
        oNotificador = Nothing
    End Sub
    ''' <summary>
    ''' Devuelve los peticionarios al que el usuario puede visualizar
    ''' </summary>
    ''' <param name="sUsu">Codigo Usuario</param>
    ''' <param name="bQARestNoConformUsu">Restriccion por NoConformidades del usuario</param>
    ''' <param name="bQARestNoConformUO">Departamento por Unidad Organizativa</param>
    ''' <param name="bQARestNoConformDep">restriccion por departamento</param>
    ''' <param name="bQARestProvMaterial">Restriccion pro material del proveedor</param>
    ''' <param name="bQARestProvEquipo">Restriccion al el equipo del proveedor</param>
    ''' <param name="bQARestProvContacto">Restriccion por contacto del calidad</param>
    ''' <returns>Dataset con los peticionarios (Cod persona // Den Nombre completo persona)</returns>
    ''' <remarks>Llamada desde=FiltroQA --> CargarPeticionarios; Tiempo m�ximo=0</remarks>
    Public Function CargarPeticionarios(ByVal sUsu As String, ByVal bQARestNoConformUsu As Boolean, ByVal bQARestNoConformUO As Boolean, ByVal bQARestNoConformDep As Boolean,
                                    ByVal bQARestProvMaterial As Boolean, ByVal bQARestProvEquipo As Boolean, ByVal bQARestProvContacto As Boolean) As DataSet
        Authenticate()
        Return DBServer.NoConformidad_GetPeticionarios_NC(sUsu, bQARestNoConformUsu, bQARestNoConformUO, bQARestNoConformDep, bQARestProvMaterial, bQARestProvEquipo, bQARestProvContacto)
    End Function
    ''' <summary>
    ''' Devuelve los revisores al que el usuario puede visualizar
    ''' </summary>
    ''' <param name="sUsu">Codigo Usuario</param>
    ''' <param name="bQARestNoConformUsu">Restriccion por NoConformidades del usuario</param>
    ''' <param name="bQARestNoConformUO">Departamento por Unidad Organizativa</param>
    ''' <param name="bQARestNoConformDep">restriccion por departamento</param>
    ''' <param name="bQARestProvMaterial">Restriccion pro material del proveedor</param>
    ''' <param name="bQARestProvEquipo">Restriccion al el equipo del proveedor</param>
    ''' <param name="bQARestProvContacto">Restriccion por contacto del calidad</param>
    ''' <returns>Dataset con los revisores (Cod persona // Den Nombre completo persona)</returns>
    ''' <remarks>Llamada desde=FiltroQA --> CargarRevisores; Tiempo m�ximo=0</remarks>
    Public Function CargarRevisores(ByVal sUsu As String, ByVal bQARestNoConformUsu As Boolean, ByVal bQARestNoConformUO As Boolean, ByVal bQARestNoConformDep As Boolean,
                                ByVal bQARestProvMaterial As Boolean, ByVal bQARestProvEquipo As Boolean, ByVal bQARestProvContacto As Boolean) As DataSet
        Authenticate()
        Return DBServer.NoConformidad_GetRevisores_NC(sUsu, bQARestNoConformUsu, bQARestNoConformUO, bQARestNoConformDep, bQARestProvMaterial, bQARestProvEquipo, bQARestProvContacto)
    End Function
    ''' <summary>
    ''' Carga los comentarios que hay de una noConformidad
    ''' </summary>
    ''' <param name="lInstancia">ID Instancia</param>
    ''' <param name="iComentarios"> 
    '''            0 --> Todos los comentarios
    '''            1 --> Comentario apertura
    '''            2 --> Comentario cierre
    '''            3 --> Comentario revisor
    '''            4 --> Comentario anulaci�n
    ''' </param>
    ''' <returns>Dataset Comentarios</returns>
    ''' <remarks>Llamada desde=NoConformidades.aspx --> CargarRevisores; Tiempo m�ximo=0</remarks>
    Public Function CargarComentarios(ByVal lInstancia As Long, ByVal iComentarios As Integer) As DataSet
        Authenticate()

        Dim ds As DataSet = Nothing
        ds = DBServer.NoConformidad_CargarComentarios(lInstancia, iComentarios)
        CargarComentarios = ds
    End Function
    ''' <summary>
    ''' Modifica el estado de la NoConformidad abierta al estado de Anulada
    ''' </summary>
    ''' <param name="lInstancia">ID instancia</param>
    ''' <param name="msPerUsuario">Codigo persona con el que se anula la NoConformidad</param>        
    ''' <remarks>Llamada desde; Tiempo m�ximo</remarks>
    Public Sub AnularNoConformidad(ByVal lInstancia As Long, ByVal msPerUsuario As String, ByVal sFrom As String)
        Authenticate()

        DBServer.NoConformidad_Anular(mlID, lInstancia, msComentAnulacion, msPerUsuario, Es_Noconformidad_Escalacion)

        m_lIdInstanciaNotificacion = lInstancia
        m_sEmailEnvioNotificacion = sFrom

        ThreadPool.QueueUserWorkItem(New WaitCallback(AddressOf NotificarAnulacionNoConformidad))
    End Sub
    ''' <summary>
    ''' Notificar Anulacion NoConformidad
    ''' </summary>
    ''' <remarks>Llamada desde:=DetalleNoConformidad.aspx; Tiempo m�ximo:0,2seg</remarks>
    Private Sub NotificarAnulacionNoConformidad()
        Dim oNotificador As New Notificar(DBServer, mIsAuthenticated)

        oNotificador.NotificacionNCAnuladaProveedor(m_lIdInstanciaNotificacion, mlID, m_sEmailEnvioNotificacion)
        oNotificador.NotificacionNCAnuladaContactosInternos(m_lIdInstanciaNotificacion, mlID, m_sEmailEnvioNotificacion)
        oNotificador.NotificacionNCAnuladaRevisor(m_lIdInstanciaNotificacion, mlID, m_sEmailEnvioNotificacion)

        oNotificador.FuerzaFinalizeSmtpClient()
        oNotificador = Nothing
    End Sub
    ''' <summary>
    ''' Funci�n para obtener las NC y cargos implicados en la puntuaci�n de una variable de calidad de tipo NC.
    ''' </summary>
    ''' <param name="HayX1">Sacar datos de NC abiertas</param> 
    ''' <param name="IdsNcX1">Ids de NC abiertas a mostrar</param> 
    ''' <param name="HayX2">Sacar datos de NC cerradas dentro de plazo</param> 
    ''' <param name="IdsNcX2">Ids de NC cerradas dentro de plazo a mostrar</param> 
    ''' <param name="HayX3">Sacar datos de NC cerradas fuera de plazo</param> 
    ''' <param name="IdsNcX3">Ids de NC cerradas fuera de plazo a mostrar</param> 
    ''' <param name="HayX4">Sacar datos de NC con cierre negativo</param> 
    ''' <param name="IdsNcX4">Ids de NC con cierre negativo a mostrar</param> 
    ''' <param name="HayX5">Sacar datos de facturaci�n proveedor</param>
    ''' <param name="Prove">Proveedor</param>
    ''' <param name="MatQa">Material Qa</param>
    ''' <param name="Unqa">Unidad de negocio</param>
    ''' <param name="Idioma">Idioma</param> 
    ''' <returns>Dataset con la variable de calidad de tipo NC cargada</returns>
    ''' <remarks>Llamada desde: fichaProveedor.aspx/MostrarPanelNC; Tiempo m�ximo: 0,1</remarks>
    Public Function DatosPuntuacionNC(ByVal HayX1 As Boolean, ByVal IdsNcX1 As String, ByVal HayX2 As Boolean, ByVal IdsNcX2 As String,
                                      ByVal HayX3 As Boolean, ByVal IdsNcX3 As String, ByVal HayX4 As Boolean, ByVal IdsNcX4 As String,
                                      ByVal HayX5 As Boolean, ByVal Prove As String, ByVal MatQa As String, ByVal Unqa As Long,
                                      ByVal Idioma As String) As DataSet
        Authenticate()
        Return DBServer.Puntuacion_DatosPuntuacionNC(HayX1, IdsNcX1, HayX2, IdsNcX2, HayX3, IdsNcX3, HayX4, IdsNcX4, HayX5, Prove, MatQa, Unqa, Idioma)
    End Function
    ''' <summary>
    ''' Carga los botones de aprobar y rechazar las acciones
    ''' </summary>
    ''' <param name="sTituloAcciones">array con los textos multiidioma para los posibles botones de una no conformidad</param>
    ''' <returns>tabla con los botones</returns>
    ''' <remarks>Llamada desde: PMWeb\script\noconformidad\desglose.aspx\Page_Load       PMWeb\script\noconformidad\detalleNoConformidad.aspx\Page_Load
    ''' PMWeb\script\noconformidad\noconformidadRevisor.aspx\page_load; Tiempo maximo:0</remarks>
    Public Function CargarBotonesAprobarRechazarAcciones(ByRef sTituloAcciones() As String,
                                                         Optional ByVal AgregarSoloComentarioYEstado As Boolean = False) As DataTable
        Dim dt As DataTable
        Dim dc As DataColumn
        dt = New DataTable("ACCIONES")
        dc = New DataColumn("ID")
        dc.DataType = System.Type.GetType("System.String")
        dt.Columns.Add(dc)
        dc = New DataColumn("IMAGEN")
        dc.DataType = System.Type.GetType("System.String")
        dt.Columns.Add(dc)
        dc = New DataColumn("TEXTO")
        dc.DataType = System.Type.GetType("System.String")
        dt.Columns.Add(dc)
        dc = New DataColumn("JAVASCRIPTEVENT")
        dc.DataType = System.Type.GetType("System.String")
        dt.Columns.Add(dc)
        dc = New DataColumn("TITULO")
        dc.DataType = System.Type.GetType("System.String")
        dt.Columns.Add(dc)
        dc = New DataColumn("WIDTH")
        dc.DataType = System.Type.GetType("System.Double")
        dt.Columns.Add(dc)
        If Not AgregarSoloComentarioYEstado Then
            Dim dr As DataRow

            dr = dt.NewRow
            dr.Item("ID") = "cmdAprobarAccion"
            dr.Item("IMAGEN") = Nothing
            dr.Item("TEXTO") = sTituloAcciones(4)  'Aprobar
            dr.Item("JAVASCRIPTEVENT") = "aprobarAccion"
            dr.Item("TITULO") = sTituloAcciones(4)
            dr.Item("WIDTH") = 150
            dt.Rows.Add(dr)

            dr = dt.NewRow
            dr.Item("ID") = "cmdRechazarAccion"
            dr.Item("IMAGEN") = Nothing
            dr.Item("TEXTO") = sTituloAcciones(5)  'Rechazar
            dr.Item("JAVASCRIPTEVENT") = "rechazarAccion"
            dr.Item("TITULO") = sTituloAcciones(5)
            dr.Item("WIDTH") = 150
            dt.Rows.Add(dr)
        End If
        Return dt
    End Function
    ''' <summary>
    ''' Devolver un Dataset con los datos de los estados de las acciones.
    ''' Sus parametros son objetos por la llamada desde noConformidad/desglose.aspx, desde este solo se 
    ''' tiene eso, los objetos.
    ''' </summary>
    ''' <param name="Idioma">Idioma con el q se esta trabajando</param>  
    ''' <param name="User"> Para conocer el usuario</param>  
    ''' <param name="m_sTituloAcciones">Array para establecer la denominaci�n de los campos estado y comentario</param>  
    ''' <returns>Dataset con los datos de los estados de las acciones</returns>
    ''' <remarks>Llamada desde: DetallenoConformidad.aspx/Page_Load   noConformidadRevisor.aspx/Page_Load
    ''' noConformidad/desglose.aspx/Page_Load; Tiempo m�ximo: 0</remarks>
    Public Function CargarEstadosComentariosInternosAcciones(ByVal Idioma As String, ByVal User As String, ByVal m_sTituloAcciones() As String) As DataSet
        Dim sIdi As String = Idioma
        Dim oCampo As Campo = New Campo(DBServer, mIsAuthenticated)
        oCampo.Id = -1000

        Dim enCursoDeAprobacion As Boolean = False
        If Workflow > 0 AndAlso (Estado = TipoEstadoNoConformidad.EnCursoDeAprobacion OrElse Estado = TipoEstadoNoConformidad.Guardada) Then
            enCursoDeAprobacion = True
        End If
        'generamos un dataset con la misma estructura que un desglose para trasladar los datos de los estados de las acciones
        Dim oDS As DataSet = oCampo.LoadInstDesglose(sIdi, -1, User, mlVersion, msProveedor, enCursoDeAprobacion)
        Dim oDTCabecera As DataTable
        Dim oRow As DataRow
        Dim oNewRow As DataRow

        oDTCabecera = oDS.Tables(0)
        oRow = oDTCabecera.NewRow

        'Comentario
        oRow = oDTCabecera.NewRow
        oRow.Item("ID") = TiposDeDatos.IdsFicticios.Comentario
        oRow.Item("COPIA_CAMPO_DEF") = TiposDeDatos.IdsFicticios.Comentario
        oRow.Item("GRUPO") = TiposDeDatos.IdsFicticios.Grupo
        oRow.Item("DEN_" + sIdi) = m_sTituloAcciones(2)
        oRow.Item("AYUDA_" + sIdi) = ""
        oRow.Item("TIPO") = 1
        oRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoTextoLargo
        oRow.Item("INTRO") = 0
        oRow.Item("TIPO_CAMPO_GS") = TiposDeDatos.IdsFicticios.Comentario
        oRow.Item("ESCRITURA") = 0
        oRow.Item("VISIBLE") = 1
        oDTCabecera.Rows.Add(oRow)

        'Estado interno
        oRow = oDTCabecera.NewRow
        oRow.Item("ID") = TiposDeDatos.IdsFicticios.EstadoInterno
        oRow.Item("COPIA_CAMPO_DEF") = TiposDeDatos.IdsFicticios.EstadoInterno
        oRow.Item("GRUPO") = TiposDeDatos.IdsFicticios.Grupo
        oRow.Item("DEN_" + sIdi) = m_sTituloAcciones(3)
        oRow.Item("AYUDA_" + sIdi) = ""
        oRow.Item("TIPO") = 1
        oRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoString
        oRow.Item("INTRO") = 1
        oRow.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.EstadoInternoNoConf
        oRow.Item("ESCRITURA") = 0
        oRow.Item("VISIBLE") = 1
        oDTCabecera.Rows.Add(oRow)

        Dim i As Integer
        i = 1
        If Not moData Is Nothing Then
            Dim oDTLineasDesglose As DataTable = oDS.Tables(2)
            For Each oRow In moDataEstados.Rows
                oNewRow = oDTLineasDesglose.NewRow
                oNewRow.Item("CAMPO_PADRE") = oRow.Item("CAMPO_PADRE")
                oNewRow.Item("LINEA") = oRow.Item("LINEA")
                oNewRow.Item("CAMPO_HIJO") = TiposDeDatos.IdsFicticios.Comentario
                oNewRow.Item("VALOR_TEXT") = oRow.Item("COMENT")
                oDTLineasDesglose.Rows.Add(oNewRow)

                oNewRow = oDTLineasDesglose.NewRow
                oNewRow.Item("CAMPO_PADRE") = oRow.Item("CAMPO_PADRE")
                oNewRow.Item("LINEA") = oRow.Item("LINEA")
                oNewRow.Item("CAMPO_HIJO") = TiposDeDatos.IdsFicticios.EstadoInterno
                oNewRow.Item("VALOR_NUM") = oRow.Item("ESTADO_INT")
                oDTLineasDesglose.Rows.Add(oNewRow)
            Next
        End If

        Return oDS
    End Function
    ''' <summary>
    ''' Carga de las propiedades del objeto NoConformidad: msComentCierre y msComentCierreBR
    ''' </summary>
    ''' <remarks>Llamada desde:noconformidadrevisor.aspx/page_load      detallenoconformidad.aspx/MontarTablaComentarios
    ''' impexp.aspx/page_load; Tiempo m�ximo: 0,1</remarks>
    Public Sub CargarComentarioCierre()
        Authenticate()

        Dim data As DataSet
        data = DBServer.NoConformidad_LoadComentCierre(mlID)

        If data.Tables(0).Rows.Count > 0 Then
            msComentCierre = DBNullToSomething(data.Tables(0).Rows(0).Item("COMENT_CIERRE"))

            If Not IsDBNull(data.Tables(0).Rows(0).Item("COMENT_CIERRE")) Then msComentCierreBR = Replace(data.Tables(0).Rows(0).Item("COMENT_CIERRE"), Chr(10), "<br/>")
        End If
    End Sub
    Public Sub Aprobacion_Escalacion(ByVal idNoConformidad As Long, ByRef idEscalacionProve As Integer, ByVal Prove As String, ByVal EsAltaEscalacion As Boolean, ByVal EsEmisionNoConformidad As Boolean)
        Authenticate()
        DBServer.NoConformidad_Aprobacion_Escalacion(idNoConformidad, idEscalacionProve, Prove, EsAltaEscalacion, EsEmisionNoConformidad)
    End Sub
    Public Function Obtener_NoConformidades_Escalacion_NivelesInferiores(ByVal idNoconformidad As Long) As DataTable
        Authenticate()
        Return DBServer.NoConformidad_Obtener_NoConformidades_Escalacion_NivelesInferiores(idNoconformidad)
    End Function
    Public Sub Notificacion_Escalacion_Proveedor(ByVal idInstancia As Integer, ByVal idEscalacionProve As Integer, ByVal UNQA As Integer, ByVal Permiso As Integer)
        Authenticate()
        Dim oNotificador As New Notificar(DBServer, mIsAuthenticated)
        oNotificador.Notificacion_Escalacion_Proveedor(idInstancia, idEscalacionProve, UNQA, Permiso)
        oNotificador = Nothing
    End Sub
End Class