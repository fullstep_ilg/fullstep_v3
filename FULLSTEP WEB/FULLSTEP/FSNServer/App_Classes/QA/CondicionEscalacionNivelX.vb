﻿<Serializable()>
Public Class CondicionEscalacionNivelX
    Public Property ID As String
    Public Property Nivel As Integer
    Public Property Operador As String
    Public Property Numero As Integer

    Public Sub New(ByVal sID As String, ByVal iNivel As Integer, ByVal sOperador As String, ByVal iNumero As Integer)
        ID = sID
        Nivel = iNivel
        Operador = sOperador
        Numero = iNumero
    End Sub
End Class
