<Serializable()> _
    Public Class UnidadNeg
    Inherits Security

    Private m_lID As Long
    Private m_sCod As String
    Private m_sDen As String
    Private m_oData As DataSet
    Private m_lUNQA1 As Long 'Padre
    Private m_lUNQA2 As Long 'Abuelo
    Private m_lPYME As Long 'ID de la PYME

    Private m_Idiomas As DataSet
    Private m_IdiomasColeccion As Collection
    Private _dsUONs As DataSet

#Region "Propiedades"
    Public ReadOnly Property Data() As Data.DataSet
        Get
            Return m_oData
        End Get
    End Property

    Public Property Id() As Long
        Get
            Return m_lID
        End Get

        Set(ByVal Value As Long)
            m_lID = Value
        End Set
    End Property

    Public Property Cod() As String
        Get
            Return m_sCod
        End Get

        Set(ByVal Value As String)
            m_sCod = Value
        End Set
    End Property

    Public Property Den() As String
        Get
            Return m_sDen
        End Get

        Set(ByVal Value As String)
            m_sDen = Value
        End Set
    End Property

    Public Property UNQA1() As Long
        Get
            Return m_lUNQA1
        End Get

        Set(ByVal Value As Long)
            m_lUNQA1 = Value
        End Set
    End Property

    Public Property UNQA2() As Long
        Get
            Return m_lUNQA2
        End Get

        Set(ByVal Value As Long)
            m_lUNQA2 = Value
        End Set
    End Property

    Public Property IdiomasNodo() As DataSet
        Get
            Return m_Idiomas
        End Get

        Set(ByVal Value As DataSet)
            m_Idiomas = Value
        End Set
    End Property

    Public ReadOnly Property IdiomasColeccion() As Collection
        Get
            Return m_IdiomasColeccion
        End Get

    End Property

    Public Property PYME() As Long
        Get
            Return m_lPYME
        End Get

        Set(ByVal Value As Long)
            m_lPYME = Value
        End Set
    End Property

    Public Property UnidadesOrganizativas As DataSet
        Get
            Return _dsUONs
        End Get
        Set(value As DataSet)
            _dsUONs = value
        End Set
    End Property
#End Region

    Public Sub New(ByRef dbserver As FSNDataBaseServer.Root,ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
    End Sub

    Public Function A�adirUnidadNegocio(ByVal dsIdiomas As DataSet) As Long
        '************************************************************************
        '*** Descripci�n:   function que a�ade un nuevo nodo
        '*** Par�metros de entrada: ds --> Dataset con los idiomas de la aplicaci�n.
        '*** Par�metros de salida: --> Id del nuevo nodo a�adido
        '*** Llamada desde:  RealizarOperacionesOcultas.aspx 						                              
        '*** Tiempo m�ximo: 0,2seg.  						                                       
        '************************************************************

        Authenticate()
        A�adirUnidadNegocio = DBServer.UnidadesNeg_A�adirUnidad(m_sCod, dsIdiomas, m_lUNQA1, m_lUNQA2, m_lPYME)
    End Function

    Public Sub ModificarNodo(ByVal dsIdiomas As DataSet, ByVal bSoloDenominaciones As Boolean)
        '************************************************************************
        '*** Descripci�n:   function que Modifica una Unidad de negocio (El codigo y las denominaciones)
        '*** Par�metros de entrada: ds --> Dataset con los idiomas de la aplicaci�n.
        '                           bSoloDenominaciones --> Si tb se modifica la denominacion o no
        '*** Llamada desde:  RealizarOperacionesOcultas.aspx 
        '                   -ParametrosGenerales.aspx
        '*** Tiempo m�ximo: 0,18seg.  						                                       
        '************************************************************

        Authenticate()
        DBServer.UnidadesNeg_ModificarNodo(m_lID, m_sCod, dsIdiomas, bSoloDenominaciones)
    End Sub

    Public Sub EliminarUnidadNeg()
        '************************************************************************
        '*** Descripci�n: Elimina un nodo seleccionado
        '*** Par�metros de entrada:ninguno
        '*** Llamada desde: EliminarUnidad.aspx.vb  						                              
        '*** Tiempo m�ximo: 0,328seg  						                                      
        '************************************************************
        Authenticate()
        DBServer.UnidadesNeg_EliminarUnidad(m_lID)
    End Sub

    Public Function ComprobarUnidadesHijas() As Integer
        '************************************************************************
        '*** Descripci�n:  Function que comprueba si una Unidad de Negocio tiene Unidades de negocio hijas
        '*** Par�metros de salida: N� de Unidades de negocio hijas
        '*** Llamada desde:  EliminarUnidad.aspx.vb						                              
        '*** Tiempo m�ximo: 0,1 seg.  						                                       
        '************************************************************

        Authenticate()
        ComprobarUnidadesHijas = DBServer.ComprobarUnidadesHijas(m_lID)
    End Function

    Public Sub ObtenerDatosRelacionados()

        'Descripcion:Obtiene los datos relacionados con una unidad de Negocio QA:
        'Los datos relacionados a comprobar son:
        '1-	Usuarios de QA
        '2-	Proveedores
        '3-	Puntuaciones a Proveedores
        '4-	Fuentes de informaci�n variables de calidad: PPMs, Cargos, Tasas de Servicio y No conformidades.
        'Parametros de entrada:= ninguno
        'LLamadas:=eliminarUnidad.aspx.vb
        '*** Tiempo m�ximo: 0,1 seg.  						                                       
        '************************************************************
        Authenticate()
        m_oData = DBServer.UnidadesNeg_ObtenerDatosRelacionados(m_lID)
    End Sub

    Public Sub Realizar_Deshacer_BajaLogica(ByVal sCadenaIDs As String, ByVal bBaja As Boolean)
        '************************************************************************
        '*** Descripci�n:   Realiza o Deshace la Baja logica de una lista de unidades de Negocio: 
        '*** Par�metros de entrada: sCadenaIDs:= lista de la las unidades de negocio.
        '***                        bBaja = true --> Realiza la baja Logica ; bBaja = false --> Deshace la baja logica
        '*** Llamada desde:  RealizarOperacionesOcultas.aspx.vb						                              
        '*** Tiempo m�ximo: 0,1 seg.  						                                       
        '************************************************************
        Authenticate()
        DBServer.UnidadesNeg_Realizar_Deshacer_BajaLogica(sCadenaIDs, bBaja)
    End Sub

    Public Sub UnidadesNeg_CargarIdiomas()
        '************************************************************************
        '*** Descripci�n: Devuelve los idiomas que dispone la aplicacion para almacenar la denominacion de la UNQA en esos idiomas.
        '*** Parametros de entrada = Ninguno
        '*** Llamada desde:     ParametrosGenerales.aspx.vb	
        '                       AltaUnidadNeg.aspx
        '*** Tiempo m�ximo: 0,1 seg.  						                                       
        '************************************************************
        'Dim inicio As Double, final As Double, tiempoTranscurrido As Double
        'inicio = Timer

        Dim i As Integer
        Authenticate()
        m_Idiomas = DBServer.UnidadesNeg_CargarIdiomas(m_lID)

        'Almacena los valores en una coleccion para luego sacar primero el idioma del usuario
        If Not m_Idiomas.Tables(0).Rows.Count = 0 Then
            For i = 0 To m_Idiomas.Tables(0).Rows.Count - 1
                Me.Add(m_Idiomas.Tables(0).Rows(i).Item("COD").ToString(), m_Idiomas.Tables(0).Rows(i).Item("DEN").ToString(), m_Idiomas.Tables(0).Rows(i).Item("DEN_NODO").ToString(), m_Idiomas.Tables(0).Rows(i).Item("EXISTE").ToString())
            Next
        End If
        'final = Timer
        'tiempoTranscurrido = (final - inicio)
    End Sub

    Friend Sub Add(ByVal sCod As String, ByVal sDenIdioma As String, ByVal sDenValorIdioma As String, ByVal bExiste As Boolean)
        If m_IdiomasColeccion Is Nothing Then
            m_IdiomasColeccion = New Collection
        End If

        Dim sValor As String
        sValor = sCod & "#" & sDenIdioma & "#" & sDenValorIdioma & "#" & IIf(bExiste, 1, 0)

        m_IdiomasColeccion.Add(sValor, sCod)
    End Sub
    Public Sub AlmacenarRelacionesProveedores(ByVal ds As DataSet)
        '************************************************************************
        '*** Descripci�n:   Procedure que almacena los datos que se muestran de la grid
        '                   Se crea un dataset con los datos y luego se manda a la clase para almacenarlos...
        '*** Par�metros de entrada: ds --> Dataset con los datos de la grid de proveedores
        '*** Llamada desde:  UnidadesNegocio.aspx 						                              
        '*** Tiempo m�ximo: 2seg.  						                                       
        '************************************************************

        Authenticate()
        DBServer.UnidadesNeg_AlmacenarRelacionesProveedores(ds)
    End Sub

    Public Function ComprobarCodigo() As Boolean
        '************************************************************************
        '*** Descripci�n:   Function que comprueba si un nodo tiene el mismo codigo de unidad de negocio
        '*** Par�metros de entrada: ninguno
        '*** Par�metros de salida: true/false:= Si ha encontrado/o no el elemento
        '*** Llamada desde: RealizarOperacionesOcultas.aspx.vb 						                              
        '*** Tiempo m�ximo: 0,3seg 						                                       
        '************************************************************
        Authenticate()
        ComprobarCodigo = DBServer.UnidadesNeg_ComprobarCodigo(m_sCod)
    End Function

    Public Sub CargarIDNodoRaiz()
        '************************************************************************
        '*** Descripci�n:   Procedure que carga el ID del nodo raiz
        '*** Par�metros de entrada: ninguno
        '*** Llamada desde: ParametrosGenerales.aspx.vb 						                              
        '*** Tiempo m�ximo: 0,1seg 						                                       
        '************************************************************

        Authenticate()
        m_lID = DBServer.UnidadesNeg_CargarIDNodoRaiz(m_lPYME)
    End Sub

    Public Function CargarLongitudCodUNQA(ByVal moduloID As Long) As Long
        '************************************************************************
        '*** Descripci�n:   function que carga la longitud que tiene que tener el codigo de la Unidad de negocio
        '*** Par�metros de entrada: moduloID = Id del Modulo de la Unidad de Negocio
        '*** Par�metros de salida: longitud del c�digo
        '*** Llamada desde: altaUnidadNeg.aspx.vb						                              
        '*** Tiempo m�ximo: 0,1seg 						                                       
        '************************************************************
        Authenticate()
        CargarLongitudCodUNQA = DBServer.UnidadesNeg_CargarLongitudCodUNQA(moduloID)
    End Function
    ''' <summary>Carga las unidades organizativas asociadas a la unidad de negocio</summary>
    ''' <remarks>Llamada desde: asignarUnidadesOrganizativas.aspx</remarks>
    Public Function DevolverUnidadesOrganizativas() As List(Of jsTreeNode)
        Dim oUONs As New List(Of jsTreeNode)

        Authenticate()

        _dsUONs = DBServer.UnidadNegocio_GetUnidadesOrganizativas(m_lID)
        If Not _dsUONs Is Nothing AndAlso _dsUONs.Tables.Count > 0 Then
            For Each drUON1 As DataRow In _dsUONs.Tables(0).Rows
                Dim oUON1 As New jsTreeNode(drUON1("COD"), drUON1("DEN_EXT"), False.ToString.ToLower, New jsTreeNodeState(False, False, drUON1("ASIG")), "#")

                Dim bDisabled As Boolean
                Dim dvUONs2 As New DataView(_dsUONs.Tables(1), "UON1='" & drUON1("COD") & "'", String.Empty, DataViewRowState.CurrentRows)
                For Each drvUON2 As DataRowView In dvUONs2
                    bDisabled = drUON1("ASIG")
                    If Not oUON1.state.disabled Then oUON1.state.disabled = drvUON2("ASIG")

                    Dim oUON2 As New jsTreeNode(drvUON2("UON1") & "|" & drvUON2("COD"), drvUON2("DEN_EXT"), False.ToString.ToLower, New jsTreeNodeState(False, bDisabled, drvUON2("ASIG")), drUON1("COD"))

                    Dim dvUONs3 As New DataView(_dsUONs.Tables(2), "UON1='" & drvUON2("UON1") & "' AND UON2='" & drvUON2("COD") & "'", String.Empty, DataViewRowState.CurrentRows)
                    For Each drvUON3 As DataRowView In dvUONs3
                        If Not bDisabled Then bDisabled = drvUON2("ASIG")
                        If Not oUON1.state.disabled Then oUON1.state.disabled = drvUON3("ASIG")
                        If Not oUON2.state.disabled Then oUON2.state.disabled = drvUON3("ASIG")

                        Dim oUON3 As New jsTreeNode(drvUON3("UON1") & "|" & drvUON3("UON2") & "|" & drvUON3("COD"), drvUON3("DEN_EXT"), False.ToString.ToLower, New jsTreeNodeState(False, bDisabled, drvUON3("ASIG")), drvUON2("UON1") & "|" & drvUON2("COD"))

                        oUONs.Add(oUON3)
                    Next

                    oUONs.Add(oUON2)
                Next

                oUONs.Add(oUON1)
            Next
        End If

        Return oUONs
    End Function
    ''' <summary>Asocia unidades organizativas as una unidad de negocio</summary>
    ''' <param name="UONs">Unidades Organizativas a asociar</param>
    ''' <remarks>Llamada desde: asignarUnidadesOrganizativas.aspx</remarks>
    Public Sub AsociarUnidadesOrganizativas(ByVal UONs As String())
        Authenticate()
        DBServer.UnidadNegocio_AsociarUnidadesOrganizativas(m_lID, UONs)
    End Sub
End Class
