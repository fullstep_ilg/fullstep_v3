<Serializable()> _
    Public Class NoConformidades
    Inherits Security

    Private moNoConformidades As DataSet

    Public ReadOnly Property Data() As Data.DataSet
        Get
            Return moNoConformidades
        End Get

    End Property


    Public Sub New(ByRef dbserver As FSNDataBaseServer.Root,ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
    End Sub

    ''' <summary>
    ''' Devuelve los datos de las noConformidades para cargar el webPart de NoConformidades
    ''' Cargara las NoConformidades dependiendo de los filtros que se le pasan.
    ''' </summary>
    ''' <param name="iSobrePasada">Si carga las NoConformidades que ha sobrepasado la fecha de resolucion (0->Vacio //1->Sobrepasadas //2->Con fecha de resolucion)</param>
    ''' <param name="dFechaLimite">se obtendr�n las no conformidades cuya diferencia entre el d�a de hoy y la fecha l�mite de resoluci�n sea la opci�n marcada</param>
    ''' <param name="bVerFechaAct">Para mostrar las NoConformidades modificados en una fecha determinada </param>        
    ''' <param name="dFechaAct">Fecha de actualizacion de la noConformidad </param>
    ''' <param name="sTipoNoConformidades">cadena con el tipo de NoConformidades a filtrar</param>
    ''' <param name="bPorUsuario">Si se filtra las NoConformidades por el usuario o no.</param>
    ''' <param name="sUsu">Codigo Usuario.</param>
    ''' <param name="sListaUNQAs">Cadena con la lista de Unidades de negocio</param>
    ''' <param name="sListaEstados">lista de estados que tiene la NoConformidad a filtrar.</param>
    ''' <param name="sIdi">Codigo idioma</param>
    ''' <remarks>Llamada desde= FSQAWebPartNoConformidades.vb-->CargarSolicitudes ; Tiempo m�ximo= 1 seg.</remarks>
    Public Sub LoadDataWebPartNoConformidades(ByVal iSobrePasada As Integer, ByVal dFechaLimite As Date, _
            ByVal bVerFechaAct As Boolean, ByVal dFechaAct As Date, ByVal sListaEstados As String, _
            ByVal sTipoNoConformidades As String, ByVal bPorUsuario As Boolean, ByVal sUsu As String, _
            ByVal sListaUNQAs As String, ByVal sIdi As String, _
            ByVal RestricSolicUsu As Boolean, ByVal RestricSolicUO As Boolean, ByVal RestricSolicDep As Boolean, _
            ByVal RestricProvMat As Boolean, ByVal RestricProvEqp As Boolean, ByVal RestricProvCon As Boolean)
        Authenticate()
        moNoConformidades = DBServer.NoConformidades_WebPartNoConformidades(iSobrePasada, dFechaLimite, bVerFechaAct, _
            dFechaAct, sListaEstados, sTipoNoConformidades, bPorUsuario, sUsu, sListaUNQAs, sIdi, _
            RestricSolicUsu, RestricSolicUO, RestricSolicDep, RestricProvMat, RestricProvEqp, RestricProvCon)
    End Sub

    ''' <summary>
    ''' Carga el webPart de NoConformidades Pendientes con los datos de las noconformidades
    ''' </summary>
    ''' <param name="sUsu">Codigo Usuario</param>
    ''' <param name="sIdi">C�digo Idioma</param> 
    ''' <remarks>Llamada desde=FSQAWebPartNoConformidadesPend.vb-->CargarNoConformidadesPendientes; Tiempo m�ximo=0,5seg.</remarks>
    Public Sub LoadDataWebPartNoConformidadesPendientes(ByVal sUsu As String, ByVal sIdi As String)
        Authenticate()
        moNoConformidades = DBServer.NoConformidades_WebPartNoConformidadesPendientes(sUsu, sIdi)
    End Sub

    ''' <summary>
    ''' Ejecuta la Notificaci�n al proveedor de la proximidad de las fechas l�mite de las 
    ''' acciones solicitadas y de la fecha fin de resoluci�n de las no conformidades que tenga abiertas.
    ''' </summary>
    ''' <remarks>Llamada desde: QA_Notificaciones/NoConformidades_NotificarProximidadFechasFin;Tiempo m�ximo: 0,2</remarks>
    Public Sub NotificarProximidadFechasFin()
        Authenticate()
        moNoConformidades = DBServer.NotificarProximidadFechasFin()

        If moNoConformidades.Tables.Count > 0 Then
            If moNoConformidades.Tables(0).Rows.Count > 0 Then
                Dim oNotificador As New Notificar(DBServer, mIsAuthenticated)

                oNotificador.NotificacionNoConformidadFechasFin(moNoConformidades)
                oNotificador.FuerzaFinalizeSmtpClient()
                oNotificador = Nothing
            End If
        End If
    End Sub
End Class
