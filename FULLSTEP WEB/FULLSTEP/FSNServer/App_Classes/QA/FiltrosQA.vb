﻿<Serializable()> _
    Public Class FiltrosQA
    Inherits Security

    Private iTipo As Integer = 1 '1 No conformidad		0 Certificado
    ''' <summary>
    ''' Tipo de Filtro 1 No conformidad		0 Certificado
    ''' </summary>
    ''' <returns>Tipo de Filtro 1 No conformidad		0 Certificado</returns>
    ''' <remarks>Llamada desde: ConfigurarFiltrosQa          PMserver/Root; Tiempo:0</remarks>
    Public Property Tipo() As Integer
        Get
            Tipo = iTipo
        End Get
        Set(ByVal value As Integer)
            iTipo = value
        End Set
    End Property
    Public Sub New(ByRef dbserver As FSNDataBaseServer.Root,ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
    End Sub
    ''' <summary>
    ''' Carga los formularios disponibles para que el usuario pueda configurar sus filtros.
    ''' </summary>
    ''' <param name="iTipo">Tipo de Solicitud 3 No conformidad		3 Certificado</param>
    ''' <returns>Dataset con los formularios disponibles</returns>
    ''' <remarks></remarks>
    Public Function LoadDisponibles(ByVal iTipo As Integer) As DataSet
        Authenticate()
        LoadDisponibles = DBServer.FiltrosQA_Get(iTipo)
    End Function
    ''' <summary>
    ''' Obtiene los filtros configurados por el usuario
    ''' </summary>
    ''' <param name="sPersona">Cod. de persona</param>
    ''' <returns>Dataset con los filtros configurados para cargar las pestañas</returns>
    ''' <remarks>Llamada desde: NoConformidades.aspx. Tiempo máximo: 0,1seg.</remarks>
    Public Function LoadConfigurados(ByVal sPersona As String) As DataSet
        Authenticate()

        LoadConfigurados = DBServer.FiltrosQA_GetConfigurados(sPersona, iTipo)
    End Function
End Class

