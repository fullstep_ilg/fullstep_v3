<Serializable()> _
Public Class MaterialQA
    Inherits Security

    Private mlID As Long
    Private moDen As MultiIdioma
    Private mbBaja As Boolean
    Private moTipoCertificados As DataTable
    Private moData As DataSet

    Public ReadOnly Property Data() As Data.DataSet
        Get
            Return moData
        End Get

    End Property

    Public Property ID() As Long
        Get
            Return mlID
        End Get

        Set(ByVal Value As Long)
            mlID = Value
        End Set
    End Property

    Public Property Baja() As Boolean
        Get
            Return mbBaja
        End Get

        Set(ByVal Value As Boolean)
            mbBaja = Value
        End Set
    End Property

    Property Den(ByVal Idioma As String) As String
        Get
            Den = moDen(Idioma)

        End Get
        Set(ByVal Value As String)
            If Not moDen.Contains(Idioma) Then
                moDen.Add(Idioma, Value)
            Else
                moDen(Idioma) = Value
            End If

        End Set
    End Property

    Property TipoCertificados() As DataTable
        Get
            TipoCertificados = moTipoCertificados
        End Get

        Set(ByVal Value As DataTable)
            moTipoCertificados = Value
        End Set
    End Property


    Public Sub New(ByRef dbserver As FSNDataBaseServer.Root,ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
        moDen = New MultiIdioma
    End Sub

    ''' <summary>
    ''' Crea un nuevo material de QA
    ''' </summary>
    ''' <param name="oDs">Dataset con los datos a almacenar</param>
    ''' <param name="lPyme">Id de la Pyme si estamos en modo PYME</param>        
    ''' <remarks>Llamada desde= GuardarMaterial.aspx; Tiempo m�ximo>=0,5seg.</remarks>
    Public Sub Create(ByVal oDs As DataSet, ByVal lPyme As Long)
        Authenticate()
        DBServer.MaterialQA_Create(oDs, mlID, lPyme)
    End Sub

    ''' <summary>
    ''' Procedimiento que carga un material de QA con su Id
    ''' </summary>
    ''' <param name="bCertificados">Variable booleana que indica si se deben cargar los certificados</param>
    ''' <remarks>
    ''' Llamada desde: PmWEb/ModificarDenMaterial/PAge_load
    ''' Tiempo m�ximo: 0,34 seg</remarks>
    Public Sub Load(Optional ByVal bCertificados As Boolean = False)
        Authenticate()
        Dim data As DataSet
        Dim oIdiomas As New Idiomas(DBServer, mIsAuthenticated)
        Dim oIdi As Idioma

        data = DBServer.MaterialQA_Load(mlID, BooleanToSQLBinary(bCertificados))
        If Not data.Tables(0).Rows.Count = 0 Then
            oIdiomas.Load()
            For Each oIdi In oIdiomas.Idiomas
                moDen(oIdi.Cod) = DBNullToSomething(data.Tables(0).Rows(0).Item("DEN_" & oIdi.Cod).ToString)
            Next
            oIdiomas = Nothing
            mbBaja = SQLBinaryToBoolean(data.Tables(0).Rows(0).Item("BAJA"))
        End If

        If bCertificados = True Then
            If Not data.Tables(1).Rows.Count = 0 Then
                moTipoCertificados = data.Tables(1)
            End If
        End If

        data = Nothing
    End Sub

    ''' <summary>
    ''' Procedimiento que modifica las denominaciones de un material
    ''' </summary>
    ''' <remarks>
    ''' Llamada desde: PMWEB/ModificarDenMAterial/cmdAceptar_Serverclick
    ''' Tiempo m�ximo: 0,45 seg</remarks>
    Public Sub ModificarDenominaciones()
        Dim DS As DataSet
        Dim sIdioma As String
        Dim dtMaterial As DataTable
        Dim dtNewRow As DataRow

        Authenticate()

        'Crea la tabla temporal para las denominaciones:
        DS = New DataSet
        dtMaterial = DS.Tables.Add("TEMP_MATERIALES_QA")
        dtMaterial.Columns.Add("ID", System.Type.GetType("System.Int32"))
        For Each sIdioma In moDen.Keys
            dtMaterial.Columns.Add("DEN_" & sIdioma, System.Type.GetType("System.String"))
            dtMaterial.Columns("DEN_" & sIdioma).AllowDBNull = True
        Next

        dtNewRow = dtMaterial.NewRow
        dtNewRow.Item("ID") = mlID
        For Each sIdioma In moDen.Keys
            dtNewRow.Item("DEN_" & sIdioma) = moDen(sIdioma)
        Next
        dtMaterial.Rows.Add(dtNewRow)

        DBServer.MaterialQA_ModificarDen(mlID, DS)
    End Sub

    ''' <summary>
    ''' Procedimiento que modifica los certificados de un material de QA con su Id
    ''' </summary>
    ''' <param name="oDs">DataSet con los datos de los certificados a modificar</param>
    ''' <remarks>
    ''' Llamada desde: PmmWeb/guardarMaterial/PAge_load
    ''' Tiempo m�ximo: 0,6 seg</remarks>
    Public Sub ModificarCertificados(ByVal oDs As DataSet)
        Authenticate()
        DBServer.MaterialQA_ModifCertificados(oDs, mlID)
    End Sub

    ''' <summary>
    ''' Procedimiento que modifica la asignacion de materiales de GS
    ''' </summary>
    ''' <param name="oDs">DataSet con los datos sobre la asignacion de materiales de GS</param>
    ''' <remarks>
    ''' Llamada desde: PmWeb/guardarMaterial/Page_load
    ''' Tiempo m�ximo: 0,63 seg</remarks>
    Public Sub ModificarAsignacionMatGS(ByVal oDs As DataSet)
        Authenticate()
        DBServer.MaterialQA_ModifAsignacionMatGS(oDs, mlID)
    End Sub

    ''' <summary>
    ''' Procedimiento que devuelve las variables de calidad puntuadas de un determinado proveedor para el material de QA pasado
    ''' </summary>
    ''' <remarks>
    ''' Llamada desde: PmWeb/eliminatMaterial/Page_load
    ''' Tiempo m�ximo: 0,55 seg</remarks>
    Public Sub ProveedoresVarCalPuntuadas()
        Authenticate()
        moData = DBServer.MaterialQA_ProveedoresVarCalPuntuadas(mlID)
    End Sub

    ''' <summary>
    ''' Procedimiento que elimina un material de Qa dado su id
    ''' </summary>
    ''' <remarks>
    ''' Llamada desde: PmWeb/eliminarMaterial/cmdAceptar_ServerClick
    ''' Tiempo m�ximo: 0,4 seg</remarks>
    Public Sub EliminarMaterialQA()
        Authenticate()
        DBServer.MaterialQA_EliminarMaterial(mlID)
    End Sub

    ''' <summary>Carga los objetivos y suelos para un determinado material de Qa y una unidad de negocio</summary>
    ''' <param name="UnidadNegQA">Unidad de negocio</param>
    ''' <param name="ProveCod">Cod. proveedor</param>
    ''' <param name="Idioma">Idioma del usuario</param>
    ''' <remarks>Llamada desde: AsignarObjetivosySuelos.aspx</remarks>
    Public Sub MaterialObjetivoySuelos(ByVal UnidadNegQA As Integer, ByVal ProveCod As String, ByVal Idioma As String)
        Authenticate()
        moData = DBServer.MaterialQA_CargarObjetivosySuelos(mlID, UnidadNegQA, ProveCod, Idioma)
    End Sub
    ''' <summary>Guarda los objetivos y suelos para un determinado material de Qa y una unidad de negocio</summary>
    ''' <param name="oDs">DataSet con los datos a actualizar</param>
    ''' <remarks>Llamada desde: AsignarObjetivosySuelos.aspx</remarks>
    Public Sub MaterialQAActualizarObjetivosySuelos(ByVal oDs As DataSet)
        Authenticate()
        DBServer.MaterialQA_ActualizarObjetivosySuelos(oDs)
    End Sub

End Class
