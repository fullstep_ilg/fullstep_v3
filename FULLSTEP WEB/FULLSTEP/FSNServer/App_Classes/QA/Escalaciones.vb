﻿Public Class Escalaciones
    Inherits Security
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
    Public Function GetNivelesEscalacion() As List(Of Escalacion)
        Authenticate()

        Dim lNivelesEscalacion As New List(Of Escalacion)
        Dim iNivelEscalacion As Escalacion
        Dim ds As DataSet = DBServer.NivelesEscalacion_GetNivelesEscalacion
        For Each rNivelEscalacion As DataRow In ds.Tables(0).Rows
            iNivelEscalacion = New Escalacion
            With iNivelEscalacion
                .Nivel = rNivelEscalacion("NIVEL")
                .NivelUNQA = rNivelEscalacion("UNQA_NIVEL")
                .Solicitud = rNivelEscalacion("SOLICITUD")
                .Bloqueo_SelProve_Adj = rNivelEscalacion("BLOQUEO_SELPRO_ADJ")
                .Denominacion = New Dictionary(Of String, String)
                .Descripcion = New Dictionary(Of String, String)
                For Each iNivelEscalacionIdioma As DataRow In ds.Tables(1).Rows.OfType(Of DataRow).Where(Function(x) x("NIVEL") = iNivelEscalacion.Nivel).ToList
                    .Denominacion(iNivelEscalacionIdioma("IDIOMA")) = iNivelEscalacionIdioma("DENOMINACION")
                    .Descripcion(iNivelEscalacionIdioma("IDIOMA")) = iNivelEscalacionIdioma("DESCRIPCION")
                Next
            End With
            lNivelesEscalacion.Add(iNivelEscalacion)
        Next
        Return lNivelesEscalacion
    End Function
    Public Sub GuardarNivelEscalacion(ByVal nivelEscalacion As Escalacion)
        Authenticate()
        Dim dtIdiomasNivelEscalacion As New DataTable
        With dtIdiomasNivelEscalacion
            .Columns.Add("IDIOMA")
            .Columns.Add("DENOMINACION")
            .Columns.Add("DESCRIPCION")
        End With
        Dim r As DataRow
        For Each denominacion As Object In nivelEscalacion.Denominacion
            r = dtIdiomasNivelEscalacion.NewRow
            r("IDIOMA") = denominacion.key
            r("DENOMINACION") = denominacion.value
            r("DESCRIPCION") = nivelEscalacion.Descripcion(denominacion.key)
            dtIdiomasNivelEscalacion.Rows.Add(r)
        Next
        Dim xmlDenominacionesDescripciones As XElement
        xmlDenominacionesDescripciones = New XElement("Idiomas", dtIdiomasNivelEscalacion.Rows.OfType(Of DataRow).Select(Function(x) New XElement("Idioma", {
                                                                                                            {New XElement("IDIOMA", x("IDIOMA")),
                                                                                                            New XElement("DENOMINACION", x("DENOMINACION")),
                                                                                                            New XElement("DESCRIPCION", x("DESCRIPCION"))}})))

        DBServer.NivelesEscalacion_GuardarNivelEscalacion(nivelEscalacion.Nivel, nivelEscalacion.NivelUNQA, nivelEscalacion.Solicitud, nivelEscalacion.Bloqueo_SelProve_Adj,
                                                           xmlDenominacionesDescripciones.ToString)
    End Sub
    Public Function Panel_Escalacion_Get_Data(ByVal usuCod As String, ByVal codProve As String, ByVal UNQA As String, ByVal nivelEscalacion As Integer, ByVal escalacionesPendiente As Boolean, ByVal provesBaja As Boolean, ByVal sIdioma As String, historicoEscalaciones As Boolean) As Object
        Authenticate()
        Dim ds As DataSet
        If historicoEscalaciones Then
            ds = DBServer.NivelesEscalacion_Panel_Escalacion_Get_Data_Historico(usuCod, codProve, UNQA, nivelEscalacion, escalacionesPendiente, provesBaja, sIdioma)
        Else
            ds = DBServer.NivelesEscalacion_Panel_Escalacion_Get_Data(usuCod, codProve, UNQA, nivelEscalacion, escalacionesPendiente, provesBaja, sIdioma)
        End If
        ' Dim ds As DataSet = DBServer.NivelesEscalacion_Panel_Escalacion_Get_Data(usuCod, codProve, UNQA, nivelEscalacion, escalacionesPendiente, provesBaja, sIdioma)

        ds.Tables(0).TableName = "INFO_ESCALACION"
        Dim keyInfoEscalacion(0) As DataColumn
        keyInfoEscalacion(0) = ds.Tables("INFO_ESCALACION").Columns("ID")
        ds.Tables("INFO_ESCALACION").PrimaryKey = keyInfoEscalacion

        ds.Tables(1).TableName = "NOCONFORMIDADES_ESCALACION"
        Dim keyNoConfEscalacion(3) As DataColumn
        keyNoConfEscalacion(0) = ds.Tables("NOCONFORMIDADES_ESCALACION").Columns("PROVE_ESCALACION")
        keyNoConfEscalacion(1) = ds.Tables("NOCONFORMIDADES_ESCALACION").Columns("NOCONFORMIDAD")
        keyNoConfEscalacion(2) = ds.Tables("NOCONFORMIDADES_ESCALACION").Columns("PROVE")
        keyNoConfEscalacion(3) = ds.Tables("NOCONFORMIDADES_ESCALACION").Columns("UNQAS")
        ds.Tables("NOCONFORMIDADES_ESCALACION").PrimaryKey = keyNoConfEscalacion

        ds.Tables("INFO_ESCALACION").Columns.Add("UNQA1")
        If Not ds.Tables("INFO_ESCALACION").Rows.Count = 0 Then
            For i As Integer = 2 To ds.Tables("INFO_ESCALACION").Rows(0)("MAX_NIVEL")
                ds.Tables("INFO_ESCALACION").Columns.Add("UNQA" & i)
            Next
        End If

        ds.Tables("NOCONFORMIDADES_ESCALACION").Columns.Add("UNQA1")
        If Not ds.Tables("NOCONFORMIDADES_ESCALACION").Rows.Count = 0 Then
            For i As Integer = 2 To ds.Tables("NOCONFORMIDADES_ESCALACION").Rows(0)("MAX_NIVEL")
                ds.Tables("NOCONFORMIDADES_ESCALACION").Columns.Add("UNQA" & i)
            Next
        End If

        For Each r As DataRow In ds.Tables("INFO_ESCALACION").Rows
            If r("NIVEL_UNQA") = 0 Then
                r("UNQA1") = ""
            Else
                For j As Integer = 1 To Split(r("UNQAS").ToString, "#").Length - 1
                    r("UNQA" & j) = Split(r("UNQAS"), "#")(j - 1)
                Next
            End If
        Next
        For Each r As DataRow In ds.Tables("NOCONFORMIDADES_ESCALACION").Rows
            If r("NIVEL_UNQA") = 0 Then
                r("UNQA1") = ""
            Else
                For j As Integer = 1 To Split(r("UNQAS").ToString, "#").Length - 1
                    r("UNQA" & j) = Split(r("UNQAS"), "#")(j - 1)
                Next
            End If
        Next

        Dim parentCols(0) As DataColumn
        Dim childCols(0) As DataColumn
        parentCols(0) = ds.Tables("INFO_ESCALACION").Columns("ID")
        childCols(0) = ds.Tables("NOCONFORMIDADES_ESCALACION").Columns("PROVE_ESCALACION")
        ds.Relations.Add("INFO_ESCALACION_NOCONFORMIDADES", parentCols, childCols, False)

        Return {ds,
            If(ds.Tables("INFO_ESCALACION").Rows.Count = 0, 0, ds.Tables("INFO_ESCALACION").Rows(0)("MAX_NIVEL")),
            If(ds.Tables("NOCONFORMIDADES_ESCALACION").Rows.Count = 0, 0, ds.Tables("NOCONFORMIDADES_ESCALACION").Rows(0)("MAX_NIVEL"))}
    End Function
    Public Function Panel_Escalacion_Get_UNQAs(ByVal UsuCod As String, ByVal Idioma As String)
        Authenticate()
        Return DBServer.NivelesEscalacion_Panel_Escalacion_Get_UNQAs(UsuCod, Idioma)
    End Function
    Public Function Panel_Escalacion_Get_NivelesEscalacion(ByVal Idioma As String, ByVal ModuloIdioma As Integer, ByVal IdTexto As Integer) As DataSet
        Authenticate()
        Return DBServer.NivelesEscalacion_Panel_Escalacion_Get_NivelesEscalacion(Idioma, ModuloIdioma, IdTexto)
    End Function
    Public Sub Panel_Escalacion_Rechazar_Escalacion(ByVal idEscalacionProve As Integer, ByVal comentarioRechazo As String)

        Authenticate()
        DBServer.NivelesEscalacion_Panel_Escalacion_Rechazar_Escalacion(idEscalacionProve, comentarioRechazo)
    End Sub
    Public Function Panel_Escalacion_Get_PermisosAprobarRechazar_UNQA(ByVal usu As String) As Dictionary(Of Integer, List(Of Integer))
        Authenticate()
        Dim dtPermisos As DataTable = DBServer.NivelesEscalacion_Panel_Escalacion_Get_PermisosAprobarRechazar_UNQA(usu, String.Join(",", New Object() {CInt(PermisoAprobarRechazar_Escalacion_UNQA.Nivel_1),
                                                                                                                       CInt(PermisoAprobarRechazar_Escalacion_UNQA.Nivel_2),
                                                                                                                       CInt(PermisoAprobarRechazar_Escalacion_UNQA.Nivel_3),
                                                                                                                       CInt(PermisoAprobarRechazar_Escalacion_UNQA.Nivel_4)}))
        Dim dPermisos As New Dictionary(Of Integer, List(Of Integer))
        For Each permiso As Integer In [Enum].GetValues(GetType(PermisoAprobarRechazar_Escalacion_UNQA))
            dPermisos(permiso) = New List(Of Integer)
        Next

        For Each row As DataRow In dtPermisos.Rows
            dPermisos(row("PERMISO")).Add(row("UNQA"))
        Next
        Return dPermisos
    End Function
    ''' <summary>FunciÃ³n ppal para el tratamiento de la escalaciÃ³n de proveedores</summary>
    ''' <param name="oCondiciones">Condiciones de escalaciÃ³n</param>
    ''' <remarks>Llamada desde: FSNWinServiceEscalacion.EscalacionSrv</remarks>
    Public Sub EscalarProveedores(ByRef oCondiciones As CondicionesEscalacion)
        Authenticate()

        Try
            'Obtener datos iveles escalaciÃ³n            
            Dim oNivelesEsc As List(Of Escalacion) = GetNivelesEscalacion()

            'Calcular nivel escalacion proveedores
            Dim oEscalaciones As New ProvesEscalaciones

            'Se tratan los proveedores con una mala calificaciÃ³n
            TratarProveedoresMalaCalificacion(oCondiciones, oEscalaciones)

            If oEscalaciones.HayNuevasEscalaciones Then
                'Registrar nuevas escalaciones
                DBServer.Proveedores_InsertarProveedoresEscalados(oEscalaciones.Datos)

                'Notificar escalaciones
                Dim oNotificador As New Notificar(DBServer, mIsAuthenticated)
                oNotificador.NotificarEscalaciones(oNivelesEsc, oEscalaciones.Datos, Date.Today)
            End If

        Catch ex As Exception
            DBServer.Errores_Create("EscalacionProves.vb", DBServer.UsuarioConectado, ex.GetType().FullName, ex.Message, ex.StackTrace, "", "")
        End Try
    End Sub
    ''' <summary>Tratamiento de escalaciones por mala calificaciÃ³n</summary>
    ''' <param name="oCondiciones">Condiciones de escalaciÃ³n</param>
    ''' <param name="oEscalaciones">Objeto con las nuevas escalaciones</param>
    Private Sub TratarProveedoresMalaCalificacion(ByRef oCondiciones As CondicionesEscalacion, ByRef oEscalaciones As ProvesEscalaciones)
        Try
            'Obtener las calificaciones para los proveedores de las UNQAs del nivel al que se aplica el nivel 1 de escalaciÃ³n para las var. de calidad que estÃ¡n en su fÃ³rmula            
            Dim dsPuntuaciones As DataSet = DBServer.Proveedores_DevolverCalificacionesEscalacionN1(oCondiciones.CrearDataTableCondicionesN1)
            If Not dsPuntuaciones Is Nothing AndAlso dsPuntuaciones.Tables.Count > 0 Then
                dsPuntuaciones.Tables(0).DefaultView.Sort = "PROVE,UNQA"

                'Obtener los proveedores/plantas implicados en la valoraciÃ³n               
                Dim dtProvesUNQAs As DataTable = dsPuntuaciones.Tables(0).DefaultView.ToTable(True, New String() {"PROVE", "UNQA"})

                'Recorrer los proveedores y ver cuÃ¡l cumple la fÃ³rmula                
                For Each drProveUNQA As DataRow In dtProvesUNQAs.Rows
                    Dim dvVar As New DataView(dsPuntuaciones.Tables(0), "PROVE='" & drProveUNQA("PROVE") & "' AND UNQA=" & drProveUNQA("UNQA"), String.Empty, DataViewRowState.CurrentRows)
                    If oCondiciones.CumpleFormulaN1(dvVar) Then
                        'calcular el Nivel de EscalaciÃ³n al  que le corresponde ser escalado en funciÃ³n del nÃºmero y nivel de NC de escalaciÃ³n que haya tenido en un periodo de tiempo                        
                        Dim bSeguir As Boolean = True

                        'Comprobar fÃ³rmula Nivel 4
                        'Comprobar si el proveedor tiene alguna escalaciÃ³n sin cerrar a nivel 4, 3, 2, 1 para la UNQA                        
                        If Not DBServer.Proveedores_ProveTieneNoConfEscAbiertas(drProveUNQA("PROVE"), drProveUNQA("UNQA"), 4) Then
                            If oCondiciones.CondicionesNivel4 IsNot Nothing AndAlso oCondiciones.CondicionesNivel4.Count > 0 Then
                                Dim dtProvesEsc As DataTable = oEscalaciones.Datos.Clone
                                If oCondiciones.CumpleFormulaN4(drProveUNQA("PROVE"), drProveUNQA("UNQA"), dtProvesEsc) Then
                                    oEscalaciones.AgregarEscalaciones(dtProvesEsc, 4)
                                    bSeguir = False
                                End If
                            End If
                        End If

                        'Comprobar fÃ³rmula Nivel 3
                        'Comprobar si el proveedor tiene alguna escalaciÃ³n sin cerrar a nivel 4, 3, 2, 1 para la UNQA                        
                        If Not DBServer.Proveedores_ProveTieneNoConfEscAbiertas(drProveUNQA("PROVE"), drProveUNQA("UNQA"), 3) Then
                            If bSeguir And (oCondiciones.CondicionesNivel3 IsNot Nothing AndAlso oCondiciones.CondicionesNivel3.Count > 0) Then
                                Dim dtProvesEsc As DataTable = oEscalaciones.Datos.Clone
                                If oCondiciones.CumpleFormulaN3(drProveUNQA("PROVE"), drProveUNQA("UNQA"), dtProvesEsc) Then
                                    oEscalaciones.AgregarEscalaciones(dtProvesEsc, 3)
                                    bSeguir = False
                                End If
                            End If
                        End If

                        'Comprobar fÃ³rmula Nivel 2
                        'Comprobar si el proveedor tiene alguna escalaciÃ³n sin cerrar a nivel 4, 3, 2, 1 para la UNQA                        
                        If Not DBServer.Proveedores_ProveTieneNoConfEscAbiertas(drProveUNQA("PROVE"), drProveUNQA("UNQA"), 2) Then
                            If bSeguir And (oCondiciones.CondicionesNivel2 IsNot Nothing AndAlso oCondiciones.CondicionesNivel2.Count > 0) Then
                                Dim dtProvesEsc As DataTable = oEscalaciones.Datos.Clone
                                If oCondiciones.CumpleFormulaN2(drProveUNQA("PROVE"), drProveUNQA("UNQA"), dtProvesEsc) Then
                                    oEscalaciones.AgregarEscalaciones(dtProvesEsc, 2)
                                    bSeguir = False
                                End If
                            End If
                        End If

                        'Si finalmente no se ha cumplido con los niveles superiores habrÃ¡ que escalar al proveedor al nivel 1 en la unidad de negocio que estamos evaluando
                        'Comprobar si el proveedor tiene alguna escalaciÃ³n sin cerrar a nivel 4, 3, 2, 1 para la UNQA                        
                        If Not DBServer.Proveedores_ProveTieneNoConfEscAbiertas(drProveUNQA("PROVE"), drProveUNQA("UNQA"), 1) Then
                            Dim drvPunt() As DataRowView = dsPuntuaciones.Tables(0).DefaultView.FindRows(New Object() {drProveUNQA("PROVE"), drProveUNQA("UNQA")})
                            If bSeguir Then oEscalaciones.AgregarEscalacion(drvPunt(0)("PROVE"), drvPunt(0)("NIF"), drvPunt(0)("DEN"), 1, drvPunt(0)("UNQA"), drvPunt(0)("UNQA_COD"))
                        End If
                    End If
                Next

                'Hacer SOLO una 2ª vuelta con las propuestas generadas para tenerlas en cuenta en los casos de escalaciones a nivel 2, 3 y 4
                'Para esta nueva vuelta tengo en cuenta unicamente los proveedores y UNQAs de las propuestas generadas en la primera vuelta               
                'Obtengo proveedores y UNQAs
                dtProvesUNQAs = oEscalaciones.Datos.DefaultView.ToTable(True, New String() {"PROVE", "UNQA", "UNQA_COD"})
                For Each drProveUNQA As DataRow In dtProvesUNQAs.Rows
                    Dim bSeguir As Boolean = True
                    Dim iIdUNQA As Integer
                    Dim dtUNQA As DataTable

                    'Mirar si existe ya una propuesta de escalación al nivel 4 para la UNQA y el proveedor
                    'Obtener el código de la UNQA de nivel indicado por el nivel de escalación iNivelEsc correspondiente a la UNQA iUNQA
                    dtUNQA = DBServer.NivelesEscalacion_DevolverCodUNQANivelEscalacion(drProveUNQA("UNQA"), 4)
                    iIdUNQA = 1
                    If dtUNQA.Rows.Count > 0 Then iIdUNQA = dtUNQA.Rows(0)("ID")
                    If oEscalaciones.Datos.Select("PROVE='" & drProveUNQA("PROVE") & "' AND NIVEL_ESCALACION=4 AND UNQA=" & iIdUNQA).Length = 0 Then
                        'Comprobar fórmula de nivel 4
                        If oCondiciones.CondicionesNivel4 IsNot Nothing AndAlso oCondiciones.CondicionesNivel4.Count > 0 Then
                            Dim dtProvesEsc As DataTable = oEscalaciones.Datos.Clone
                            If oCondiciones.CumpleFormulaN4(drProveUNQA("PROVE"), drProveUNQA("UNQA"), dtProvesEsc, oEscalaciones) Then
                                If Not DBServer.Proveedores_ProveTieneNoConfEscAbiertas(drProveUNQA("PROVE"), drProveUNQA("UNQA"), 4) Then
                                    oEscalaciones.AgregarEscalaciones(dtProvesEsc, 4)
                                    bSeguir = False
                                End If

                                'Eliminar las propuestas de la 1Âª vuelta que han originado esta escalaciÃ³n si se ha generado y si no tambiÃ©n porque no tiene sentido mantener estas propuestas
                                'si ya hay una de nivel superior en curso
                                oEscalaciones.EliminarPropuestasGeneradoras(dtProvesEsc)
                                'Eliminar propuestas de niveles inferiores en UNQAs hijas                                                                
                                oEscalaciones.EliminarPropuestasInferioresUNQASHijas(drProveUNQA("PROVE"), iIdUNQA, drProveUNQA("UNQA_COD"), 4)
                            End If
                        End If
                    Else
                        bSeguir = False
                    End If

                    'Mirar si existe ya una propuesta de escalación al nivel 3 para la UNQA y el proveedor
                    'Obtener el código de la UNQA de nivel indicado por el nivel de escalación iNivelEsc correspondiente a la UNQA iUNQA
                    dtUNQA = DBServer.NivelesEscalacion_DevolverCodUNQANivelEscalacion(drProveUNQA("UNQA"), 3)
                    iIdUNQA = 1
                    If dtUNQA.Rows.Count > 0 Then iIdUNQA = dtUNQA.Rows(0)("ID")
                    If oEscalaciones.Datos.Select("PROVE='" & drProveUNQA("PROVE") & "' AND NIVEL_ESCALACION=3 AND UNQA=" & iIdUNQA).Length = 0 Then
                        'Comprobar fórmula de nivel 3
                        If bSeguir And (oCondiciones.CondicionesNivel3 IsNot Nothing AndAlso oCondiciones.CondicionesNivel3.Count > 0) Then
                            Dim dtProvesEsc As DataTable = oEscalaciones.Datos.Clone
                            If oCondiciones.CumpleFormulaN3(drProveUNQA("PROVE"), drProveUNQA("UNQA"), dtProvesEsc, oEscalaciones) Then
                                If Not DBServer.Proveedores_ProveTieneNoConfEscAbiertas(drProveUNQA("PROVE"), drProveUNQA("UNQA"), 3) Then
                                    oEscalaciones.AgregarEscalaciones(dtProvesEsc, 3)
                                    bSeguir = False
                                End If

                                'Eliminar las propuestas de la 1Âª vuelta que han originado esta escalaciÃ³n si se ha generado y si no tambiÃ©n porque no tiene sentido mantener estas propuestas
                                'si ya hay una de nivel superior en curso
                                oEscalaciones.EliminarPropuestasGeneradoras(dtProvesEsc)
                                'Eliminar propuestas de niveles inferiores en UNQAs hijas                                
                                oEscalaciones.EliminarPropuestasInferioresUNQASHijas(drProveUNQA("PROVE"), iIdUNQA, drProveUNQA("UNQA_COD"), 3)
                            End If
                        End If
                    Else
                        bSeguir = False
                    End If

                    'Mirar si existe ya una propuesta de escalación al nivel 3 para la UNQA y el proveedor
                    'Obtener el código de la UNQA de nivel indicado por el nivel de escalación iNivelEsc correspondiente a la UNQA iUNQA
                    dtUNQA = DBServer.NivelesEscalacion_DevolverCodUNQANivelEscalacion(drProveUNQA("UNQA"), 2)
                    iIdUNQA = 1
                    If dtUNQA.Rows.Count > 0 Then iIdUNQA = dtUNQA.Rows(0)("ID")
                    If oEscalaciones.Datos.Select("PROVE='" & drProveUNQA("PROVE") & "' AND NIVEL_ESCALACION=2 AND UNQA=" & iIdUNQA).Length = 0 Then
                        'Comprobar fórmula de nivel 2
                        If bSeguir And (oCondiciones.CondicionesNivel2 IsNot Nothing AndAlso oCondiciones.CondicionesNivel2.Count > 0) Then
                            Dim dtProvesEsc As DataTable = oEscalaciones.Datos.Clone
                            If oCondiciones.CumpleFormulaN2(drProveUNQA("PROVE"), drProveUNQA("UNQA"), dtProvesEsc, oEscalaciones) Then
                                If Not DBServer.Proveedores_ProveTieneNoConfEscAbiertas(drProveUNQA("PROVE"), drProveUNQA("UNQA"), 2) Then
                                    oEscalaciones.AgregarEscalaciones(dtProvesEsc, 2)
                                    bSeguir = False
                                End If

                                'Eliminar las propuestas de la 1Âª vuelta que han originado esta escalaciÃ³n si se ha generado y si no tambiÃ©n porque no tiene sentido mantener estas propuestas
                                'si ya hay una de nivel superior en curso
                                oEscalaciones.EliminarPropuestasGeneradoras(dtProvesEsc)
                                'Eliminar propuestas de niveles inferiores en UNQAs hijas                                                                
                                oEscalaciones.EliminarPropuestasInferioresUNQASHijas(drProveUNQA("PROVE"), iIdUNQA, drProveUNQA("UNQA_COD"), 2)
                            End If
                        End If
                    End If
                Next
            End If

        Catch ex As Exception
            DBServer.Errores_Create("EscalacionProves.vb", DBServer.UsuarioConectado, ex.GetType().FullName, ex.Message, ex.StackTrace, "", "")
        End Try
    End Sub
    ''' <summary>Lleva a cabo las notificaciones de las NCEs pendientes de validar cierre eficaz</summary>
    ''' <param name="oCondiciones">Condiciones de escalación</param>
    ''' <remarks>Llamada desde: FSNWinServiceEscalacion.EscalacionSrv</remarks>
    Public Sub NotificarNCEscPdtesValidarCierre(ByRef oCondiciones As CondicionesEscalacion)
        Authenticate()

        'Obtener los datos de las no conformidades a notificar y los datos de los peticionarios a los que notificar
        Dim dsDatosNotif As DataSet = DBServer.NivelesEscalacion_DatosNCEsPdtesValidarCierre(oCondiciones.PlazoNCEPdteValidarCierreN1, oCondiciones.PlazoNCEPdteValidarCierreN2, oCondiciones.PlazoNCEPdteValidarCierreN3, oCondiciones.PlazoNCEPdteValidarCierreN4)

        'Notificar escalaciones
        If dsDatosNotif IsNot Nothing AndAlso dsDatosNotif.Tables.Count > 0 AndAlso dsDatosNotif.Tables(0).Rows.Count > 0 Then
            Dim oNotificador As New Notificar(DBServer, mIsAuthenticated)
            oNotificador.NCEsPdtesValidarCierre(dsDatosNotif, oCondiciones.PlazoNCEPdteValidarCierreN1, oCondiciones.PlazoNCEPdteValidarCierreN2, oCondiciones.PlazoNCEPdteValidarCierreN3, oCondiciones.PlazoNCEPdteValidarCierreN4)
        End If
    End Sub
    ''' <summary>Lleva a cabo las notificaciones de las NCEs sin cerrar X meses</summary>
    ''' <param name="oCondiciones">Condiciones de escalación</param>
    ''' <param name="dtFechaProximaEjecucionServicio">Fecha de la siguiente ejecución del servicio según la configuración. Si se está ejecutando esta función es porque es hoy o ya ha pasado</param>
    ''' <remarks>Llamada desde: FSNWinServiceEscalacion.EscalacionSrv</remarks>
    Public Sub NotificarNCEscSinCerrarXMeses(ByRef oCondiciones As CondicionesEscalacion, ByVal dtFechaProximaEjecucionServicio As Date)
        Authenticate()

        'Obtener los datos de las no conformidades a notificar y los datos de los peticionarios a los que notificar
        Dim dsDatosNotif As DataSet = DBServer.NivelesEscalacion_DatosNCEsSinCerrarXMeses(oCondiciones.PlazoNCEPdteCerrarN1, oCondiciones.PlazoNCEPdteCerrarN2, oCondiciones.PlazoNCEPdteCerrarN3, oCondiciones.PlazoNCEPdteCerrarN4,
                                                                                          oCondiciones.RecurrenciaNCEPdteCerrarN1, oCondiciones.RecurrenciaNCEPdteCerrarN2, oCondiciones.RecurrenciaNCEPdteCerrarN3, oCondiciones.RecurrenciaNCEPdteCerrarN4)

        'Notificar escalaciones
        Dim oNotificador As New Notificar(DBServer, mIsAuthenticated)
        If dsDatosNotif IsNot Nothing AndAlso dsDatosNotif.Tables.Count > 0 AndAlso dsDatosNotif.Tables(0).Rows.Count > 0 Then
            oNotificador.NCEsSinCerrarXMeses(dsDatosNotif, oCondiciones.PlazoNCEPdteCerrarN1, oCondiciones.PlazoNCEPdteCerrarN2, oCondiciones.PlazoNCEPdteCerrarN3, oCondiciones.PlazoNCEPdteCerrarN4)
        End If
    End Sub
    Public Function NivelesEscalacion_Panel_Escalacion_Get_Validaciones_Alta_Noconfomidad_Escalacion(ByVal Solicitud As Long, ByVal Prove As String, ByVal UNQA As Integer, ByVal Idioma As String)
        Authenticate()
        Dim dsResultadosValidaciones As DataSet = DBServer.NivelesEscalacion_Panel_Escalacion_Get_Validaciones_Alta_Noconfomidad_Escalacion(Solicitud, Prove, UNQA, Idioma)
        Dim resultadoValidacion As Integer = 0
        Dim tipoNoConformidad, UNQA_DEN, estado_NoConformidad, estado_Escalacion As String
        Dim idNoConformidad, nivel_Escalacion, nivel_UNQA As Integer
        tipoNoConformidad = String.Empty : UNQA_DEN = String.Empty : estado_NoConformidad = String.Empty : estado_Escalacion = String.Empty
        idNoConformidad = 0 : nivel_Escalacion = 0 : nivel_UNQA = 0
        If IsDBNull(dsResultadosValidaciones.Tables(2).Rows(0)("UNQA")) Then
            resultadoValidacion = 3
            nivel_UNQA = dsResultadosValidaciones.Tables(2).Rows(0)("UNQA_NIVEL")
        ElseIf dsResultadosValidaciones.Tables(0).Rows.Count > 0 Then
            resultadoValidacion = 1
            tipoNoConformidad = dsResultadosValidaciones.Tables(0).Rows(0)("CODIGO_SOLICITUD") & " - " & dsResultadosValidaciones.Tables(0).Rows(0)("DEN_" & Idioma)
            idNoConformidad = dsResultadosValidaciones.Tables(0).Rows(0)("INSTANCIA_NOCONFORMIDAD")
            UNQA_DEN = dsResultadosValidaciones.Tables(0).Rows(0)("UNQA_DEN")
            estado_NoConformidad = dsResultadosValidaciones.Tables(0).Rows(0)("ESTADO_NOCONFORMIDAD")
        ElseIf dsResultadosValidaciones.Tables(1).Rows.Count > 0 Then
            resultadoValidacion = 2
            UNQA_DEN = dsResultadosValidaciones.Tables(1).Rows(0)("UNQA_DEN")
            estado_Escalacion = dsResultadosValidaciones.Tables(1).Rows(0)("ESTADO_ESCALACION")
            nivel_Escalacion = dsResultadosValidaciones.Tables(1).Rows(0)("NIVEL_ESCALACION")
        End If

        Return New With {.resultadoValidacion = resultadoValidacion,
                        .tipoNoConformidad = tipoNoConformidad,
                        .idNoConformidad = idNoConformidad,
                        .UNQA = UNQA_DEN,
                        .nivel_UNQA = nivel_UNQA,
                        .estadoNoConformidad = estado_NoConformidad,
                        .nivelEscalacion = nivel_Escalacion,
                        .estadoEscalacion = estado_Escalacion}
    End Function
End Class

#Region "Clase Escalaciones"
''' Clase que gestiona las nuevas escalaciones
Public Class ProvesEscalaciones
    Public Property Datos As DataTable
    Public Sub New()
        Datos = New DataTable
        Datos.Columns.Add("PROVE", GetType(String))
        Datos.Columns.Add("PROVE_NIF", GetType(String))
        Datos.Columns.Add("PROVE_DEN", GetType(String))
        Datos.Columns.Add("NIVEL_ESCALACION", GetType(Integer))
        Datos.Columns.Add("NIVEL_ESCALACION_DEN", GetType(String))
        Datos.Columns.Add("UNQA", GetType(Integer))
        Datos.Columns.Add("UNQA_COD", GetType(String))
        Datos.Columns.Add("UNQA_DEN", GetType(String))
        Datos.Columns.Add("NOCONFORMIDAD", GetType(Integer))
        Datos.Columns.Add("UNQA_ORG", GetType(Integer))
        Datos.Columns.Add("NIVEL_ESCALACION_ORG", GetType(Integer))
    End Sub
    ''' <summary>Agrega nuevas escalaciones a la lista de escalaciones a insertar</summary>
    ''' <param name="dtProvesEsc">Datos con las no conformidades para las nuevas escalaciones</param>
    ''' <param name="iNivelescalacion">Nivel de las nuevas escalaciones</param>
    ''' <remarks>Llamada desde: EscalacionProves.TratarProveedoresYaEscalados</remarks>
    Public Sub AgregarEscalaciones(ByRef dtProvesEsc As DataTable, ByVal iNivelEscalacion As Integer)
        If Not dtProvesEsc Is Nothing Then
            For Each oRow As DataRow In dtProvesEsc.Rows
                Dim dvNoConfs As New DataView(Datos, Nothing, "PROVE,UNQA,NOCONFORMIDAD,UNQA_ORG,NIVEL_ESCALACION_ORG", DataViewRowState.CurrentRows)
                If dvNoConfs.Find(New Object() {oRow("PROVE"), oRow("UNQA"), oRow("NOCONFORMIDAD"), oRow("UNQA_ORG"), oRow("NIVEL_ESCALACION_ORG")}) < 0 Then
                    Dim oNewRow As DataRow = Datos.NewRow
                    oNewRow("PROVE") = oRow("PROVE")
                    oNewRow("PROVE_NIF") = oRow("PROVE_NIF")
                    oNewRow("PROVE_DEN") = oRow("PROVE_DEN")
                    oNewRow("NIVEL_ESCALACION") = iNivelEscalacion
                    oNewRow("UNQA") = oRow("UNQA")
                    oNewRow("UNQA_COD") = oRow("UNQA_COD")
                    oNewRow("NOCONFORMIDAD") = oRow("NOCONFORMIDAD")
                    oNewRow("UNQA_ORG") = oRow("UNQA_ORG")
                    oNewRow("NIVEL_ESCALACION_ORG") = oRow("NIVEL_ESCALACION_ORG")

                    Datos.Rows.Add(oNewRow)
                End If
            Next
        End If
    End Sub
    ''' <summary>'Elimina las propuestas de la 1ª vuelta que han originado esta escalación</summary>
    ''' <param name="dtProvesEsc">Datatable con las no conformidades y las propuestas que han generado la nueva propuesta de escalación</param>   
    Public Sub EliminarPropuestasGeneradoras(ByRef dtProvesEsc As DataTable)
        For Each drProveEsc As DataRow In dtProvesEsc.Rows
            If drProveEsc.IsNull("NOCONFORMIDAD") Then
                For Each oRow As DataRow In Datos.Select("PROVE='" & drProveEsc("PROVE") & "' AND NIVEL_ESCALACION=" & drProveEsc("NIVEL_ESCALACION_ORG") & " AND UNQA=" & drProveEsc("UNQA_ORG"))
                    Datos.Rows.Remove(oRow)
                Next
            End If
        Next
    End Sub
    ''' <summary>Elimina las propuestas de nivel indicado en la UNQA indicada para el proveedor indicado</summary>    
    ''' <param name="sProveCod">Cod proveedor</param>    
    ''' <param name="iIdUNQA">UNQA padre</param>
    ''' <param name="sCodUNQA"></param>
    ''' <param name="iNivelEsc"></param>
    ''' <remarks>Llamada desde: TratarProveedoresMalaCalificacion</remarks>
    Public Sub EliminarPropuestasInferioresUNQASHijas(ByVal sProveCod As String, ByVal iIdUNQA As Integer, ByVal sCodUNQA As String, ByVal iNivelEsc As Integer)
        Dim sFiltro As String = "PROVE='" & sProveCod & "' AND NIVEL_ESCALACION<" & iNivelEsc
        If iIdUNQA <> 1 Then sFiltro &= " AND UNQA=" & iIdUNQA & " AND UNQA_COD LIKE '" & sCodUNQA & "/%'"
        For Each oRow As DataRow In Datos.Select(sFiltro)
            Datos.Rows.Remove(oRow)
        Next
    End Sub
    ''' <summary>Agrega una nueva escalaciÃ³n a la lista de escalaciones a insertar</summary>
    ''' <param name="sProve">Cod. proveedor</param>
    ''' <param name="sProveNIF">NIF proveedor</param>
    ''' <param name="sProveDen">Den. proveedor</param>
    ''' <param name="iNivel">Nivel escalaciÃ³n</param>
    ''' <param name="iUNQA">UNQA en la que se produce la escalaciÃ³n</param>
    ''' <param name="sCodUNQA">Cod. UNQA con los cÃ³digos de los ascendentes concatenados</param>
    ''' <param name="iNoConformidad">No conformidad que provoca la escalación</param>
    ''' <remarks>Llamada desde: TratarProveedores mala calificaciÃ³n</remarks>
    Public Sub AgregarEscalacion(ByVal sProve As String, ByVal sProveNIF As String, ByVal sProveDen As String, ByVal iNivel As Integer, ByVal iUNQA As Integer, ByVal sCodUNQA As String, Optional ByVal iNoConformidad As Integer = 0)
        Dim oNewRow As DataRow = Datos.NewRow
        oNewRow("PROVE") = sProve
        oNewRow("PROVE_NIF") = sProveNIF
        oNewRow("PROVE_DEN") = sProveDen
        oNewRow("NIVEL_ESCALACION") = iNivel
        oNewRow("UNQA") = iUNQA
        oNewRow("UNQA_COD") = sCodUNQA
        If iNoConformidad <> 0 Then oNewRow("NOCONFORMIDAD") = iNoConformidad

        Datos.Rows.Add(oNewRow)
    End Sub
    ''' <summary>Indica si hay nuevas escalaciones a aÃ±adir</summary>
    ''' <returns>Booleano indicando si haynuevas escalaciones</returns>
    ''' <remarks>Llamada desde: EscalarProveedores</remarks>
    Public Function HayNuevasEscalaciones() As Boolean
        Return (Datos.Rows.Count > 0)
    End Function
End Class
#End Region
