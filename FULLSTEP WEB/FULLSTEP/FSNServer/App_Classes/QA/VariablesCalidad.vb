﻿<Serializable()> _
Public Class VariablesCalidad
    Inherits Security
    Public Sub New(ByRef dbserver As FSNDataBaseServer.Root,ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
    End Sub
    ''' <summary>
    ''' FunciÃ³n que devuleve un dataset que contiene en una sola tabla todas las variables de calidad a las que el usuario tiene acceso
    ''' _hds: El dataset se cargarÃ¡ posteriormente en un HierarchicalDataSet. Para que pueda establecer la jerarquia la tabla del dataset tiene un campo que es la PK (ID) y otro que es la FK (PADRE).
    ''' </summary>
    ''' <param name="sUsu">codigo Usuario</param>
    ''' <param name="sIdi">Idioma Usuario</param>
    ''' <param name="iPyme">Codigo de la Pyme del usuario</param>
    ''' <param name="desde">=1 si viene desde el webpart</param>
    ''' <returns>Dataset con las variables de calidad del usuario</returns>
    ''' <remarks>Llamadas: PMweb/script/puntuacion/panelCalidadFiltro.aspx.vb,FSQAWebPartPanelCalidad.vb; Tiempo=0,5seg</remarks>
    Public Function GetData_hds(ByVal sUsu As String, Optional ByVal sIdi As String = "SPA", Optional ByVal iPyme As Integer = 0, Optional ByVal desde As Integer = 0, Optional ByVal bTodas As Boolean = False) As DataSet
        Authenticate()
        Return DBServer.VariablesCalidad_GetData_hds(sUsu, sIdi, iPyme, desde, bTodas)
    End Function
    ''' <summary>
    ''' Guarda en base de datos los valores de las variables manuales que contiene el dataset dsVariablesManuales
    ''' </summary>
    ''' <param name="dsVariablesManuales">Dataset con una tabla qiue contiene un registro por cada variable manual modificada por el usuario en la ficha de calidad</param>
    ''' <remarks>Llamadas: PMWeb/script/puntuacion/fichaProveedor.asx.vb/Private Sub btnAlmacenar_Click</remarks>
    Public Sub InsertarVariablesManuales(ByVal dsVariablesManuales As DataSet)
        Authenticate()
        DBServer.VariablesCalidad_InsertarVariablesManuales(dsVariablesManuales)
    End Sub

    ''' <summary>
    ''' Guarda en base de datos El error si no ha encontrado Objetivo, suelo o facturacion en el calculo de la puntuacion
    ''' </summary>
    ''' <param name="sProve">Codigo proveedor</param>
    ''' <param name="lIdUnqa">ID Unidad de negocio</param>
    ''' <param name="lVarCal">ID Variable calidad</param>
    ''' <param name="lNivel"> Nivel Variable calidad</param>
    ''' <param name="iTipoError"></param>
    ''' <param name="iVariableError">La variable que ha producido el error</param>
    ''' <param name="sValoresError">Valores de las variables que intervienen en la fórmula</param>
    ''' <param name="iIdFormula">El id de la fórmula en la que se ha producido el error</param>
    ''' <param name="sMensajeError">Mensaje que produce el error</param>
    ''' <remarks>Puntuaciones.vb; tiempo ejecucion:=0,3seg.</remarks>
    Public Sub GuardarError(ByVal sProve As String, ByVal lIdUnqa As Long, ByVal lVarCal As Long, ByVal lNivel As Long, ByVal iTipoError As Integer, ByVal iVariableError As Integer, _
                            ByVal sValoresError As String, ByVal iIdFormula As Integer, ByVal sMensajeError As String)
        Authenticate()
        DBServer.VariablesCalidad_GuardarError(sProve, lIdUnqa, lVarCal, lNivel, iTipoError, iVariableError, sValoresError, iIdFormula, sMensajeError)
    End Sub

End Class

