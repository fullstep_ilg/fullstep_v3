<Serializable()> _
Public Class MaterialesQA
    Inherits Security

    Private moData As DataSet

    Public ReadOnly Property Data() As Data.DataSet
        Get
            Return moData
        End Get

    End Property

    ''' <summary>
    ''' Funci�n que devuelve un dataset con los Materiels de QA en un Idioma,incluyendo o los materiales de baja l�gica
    ''' </summary>
    ''' <param name="sIdi">idioma</param>
    ''' <param name="iBaja">1:Incluir materiales de baja l�gica/0:No incluir materiales de Baja l�gica</param>
    ''' <param name="bSinCertificados">Si carga los certificados relacionados al material QA</param>
    ''' <param name="lPyme">Id de la PYME del usuario, si estamos en modo PYME</param>
    ''' <returns>Dataset con los materiales de QA</returns>
    ''' <remarks>
    ''' Llamadas: PMweb2008/script/puntuacion/panelCalidadFiltro.aspx.vb
    '''           PMWeb/script/materiales/mantenimientoMat.aspx.vb
    ''' Es para sustituir a LoadData, para 2008 si le enlazamos un ObjectDataSource necesitamos un m�todo que devuelva el Dataset
    ''' Cuando las 2 paginas que utilizan LoadData se pasen a 2008 borrar el Sub LoadData
    '''  </remarks>
    Public Function GetData(Optional ByVal sIdi As String = "SPA", Optional ByVal iBaja As Integer = 0, Optional ByVal bSinCertificados As Boolean = False, Optional ByVal lPyme As Long = 0, Optional ByVal bResMatCom As Boolean = False, Optional ByVal com As String = Nothing) As DataSet
        Authenticate()
        Return DBServer.MaterialesQA_Get(sIdi, iBaja, bSinCertificados, lPyme, bResMatCom, com)
    End Function

    ''' <summary>
    ''' Funci�n que devuelve un dataset con los Materiels de GS en un Idioma,incluyendo o los materiales de baja l�gica
    ''' </summary>
    ''' <param name="lMaterialQA">opcional. ID Material QA que se le pasa</param>
    ''' <param name="lPyme">opcional. ID de la PYME del usuario si la instalacion esta en modo PYME</param>
    ''' <param name="sIdi">optional. C�digo idioma del usuario</param>    ''' <returns>Dataset con los materiales de QA</returns>
    ''' <remarks>
    ''' Llamadas: PMWeb/script/materiales/asignarMateriales.aspx.vb
    '''           PMWeb/script/materiales/mantenimientoMat.aspx.vb
    ''' Es para sustituir a LoadData, para 2008 si le enlazamos un ObjectDataSource necesitamos un m�todo que devuelva el Dataset
    ''' Cuando las 2 paginas que utilizan LoadData se pasen a 2008 borrar el Sub LoadData
    '''  </remarks>
    Public Function GetMaterialesGS(Optional ByVal sIdi As String = "SPA", Optional ByVal lMaterialQA As Long = 0, Optional ByVal lPyme As Long = 0) As DataSet
        Authenticate()
        Return DBServer.MaterialesQA_MaterialesGS_Get(sIdi, lMaterialQA, lPyme)
    End Function


    Public Sub New(ByRef dbserver As FSNDataBaseServer.Root,ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
    End Sub

    ''' <summary>
    ''' Procedimiento que carga los materiales de QA
    ''' </summary>
    ''' <param name="sFiltro">Filtro a aplicar</param>
    ''' <param name="sIdi">Idioma de la aplicaci�n</param>
    ''' <remarks>
    ''' Llamada desde: PmWeb/mantenimientoMat/cmdCargar_Click
    ''' Tiempo m�ximo: 0,5 seg</remarks>
    Public Sub DevolverMaterialesQA(ByVal sFiltro As String, Optional ByVal sIdi As String = "SPA")
        Authenticate()
        moData = DBServer.MaterialesQA_DevolverMateriales(sFiltro, sIdi)
    End Sub
    ''' <summary>Actualiza los datos de materiales de QA</summary>
    ''' <param name="dsMateriales">dataset con los datos de los materiales</param>
    Public Sub ActualizarProvesPotencialesAValidos(ByRef dsMateriales As DataSet)
        Authenticate()
        DBServer.MaterialesQA_ActualizarProvesPotencialesAValidos(dsMateriales)
    End Sub

    ''' <summary>
    ''' Procedimiento que carga la asignaci�n de los materiales de QA con los filtros pasados
    ''' </summary>
    ''' <param name="sGMN1">Material de nivel 1</param>
    ''' <param name="sGMN2">MAterial de nivel 2</param>
    ''' <param name="sGMN3">MAterial de nivel 3</param>
    ''' <param name="sGMN4">MAterial de nivel 4</param>
    ''' <param name="sIdi">Idioma de la aplicaci�n</param>
    ''' <param name="IdMaterialQA">Identificador de los materiales de QA</param>
    ''' <remarks>
    ''' Llamada desde: PmWeb/comprobarAsignarMatGS/PAge_Load
    ''' Tiempo m�ximo: 0,3 seg</remarks>
    Public Sub ComprobarAsignacionMatQA(Optional ByVal sGMN1 As String = Nothing, Optional ByVal sGMN2 As String = Nothing, Optional ByVal sGMN3 As String = Nothing, Optional ByVal sGMN4 As String = Nothing, Optional ByVal sIdi As String = "SPA", Optional ByVal IdMaterialQA As Integer = 0)
        Authenticate()
        moData = DBServer.MaterialesQA_ComprobarAsignacionMatQA(sGMN1, sGMN2, sGMN3, sGMN4, sIdi, IdMaterialQA)
    End Sub

End Class
