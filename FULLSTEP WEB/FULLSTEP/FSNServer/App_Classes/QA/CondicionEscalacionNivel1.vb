﻿<Serializable()>
Public Class CondicionEscalacionNivel1
    Public Property ID As String
    Public Property Nivel As Integer
    Public Property VarID As Integer

    Public Sub New(ByVal sID As String, ByVal iNivel As Integer, ByVal iVarID As Integer)
        ID = sID
        Nivel = iNivel
        VarID = iVarID
    End Sub
End Class
