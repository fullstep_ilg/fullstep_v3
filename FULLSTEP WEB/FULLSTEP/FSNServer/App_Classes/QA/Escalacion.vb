﻿Public Class Escalacion
    Private _nivel As Integer
    Public Property Nivel() As Integer
        Get
            Return _nivel
        End Get
        Set(ByVal value As Integer)
            _nivel = value
        End Set
    End Property
    Private _nivelUNQA As Integer
    Public Property NivelUNQA() As Integer
        Get
            Return _nivelUNQA
        End Get
        Set(ByVal value As Integer)
            _nivelUNQA = value
        End Set
    End Property
    Private _denominacion As Dictionary(Of String, String)
    Public Property Denominacion() As Dictionary(Of String, String)
        Get
            Return _denominacion
        End Get
        Set(ByVal value As Dictionary(Of String, String))
            _denominacion = value
        End Set
    End Property
    Private _descripcion As Dictionary(Of String, String)
    Public Property Descripcion() As Dictionary(Of String, String)
        Get
            Return _descripcion
        End Get
        Set(ByVal value As Dictionary(Of String, String))
            _descripcion = value
        End Set
    End Property
    Private _solicitud As Integer
    Public Property Solicitud() As Integer
        Get
            Return _solicitud
        End Get
        Set(ByVal value As Integer)
            _solicitud = value
        End Set
    End Property
    Private _bloqueo_SelProve_Adj As Boolean
    Public Property Bloqueo_SelProve_Adj() As Boolean
        Get
            Return _bloqueo_SelProve_Adj
        End Get
        Set(ByVal value As Boolean)
            _bloqueo_SelProve_Adj = value
        End Set
    End Property
End Class
