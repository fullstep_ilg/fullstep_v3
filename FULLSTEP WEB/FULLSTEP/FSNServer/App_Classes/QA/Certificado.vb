Imports System.Threading
<Serializable()> _
    Public Class Certificado
    Inherits Security
#Region "Properties"
	Private mlID As Long
    Private m_dFecPublicacion As Date
    Private m_dFecDesPub As Date
    Private m_dFecLimCumpl As Date
    Private m_dFecCumpl As Date
    Private m_dFecExpiracion As Date
    Private m_bPublicada As Boolean
    Private m_iValidado As Nullable(Of Integer)
    Private m_bBaja As Boolean
    Private m_lVersion As Long
    Private m_sProveedor As String
    Private m_sProveDen As String
    Private m_bProveBaja As Boolean
    Private m_lContacto As Long
    Private mlInstancia As Long
    Private m_sEmailEnvioNotificacion As String
    Private m_sProveedorEnvioNotificacion As String
    Private m_sNombreEnvioNotificacion As String
    Private m_lIdInstanciaNotificacion As Long
    Private moData As DataSet
    Private moProveedores As DataTable
    Private msCodPeticionario As String
    Private msPeticionario As String
    Private mlIDSolicitud As Long
    Private msDenSolictud As String
    Public Property ID() As Long
        Get
            Return mlID
        End Get

        Set(ByVal Value As Long)
            mlID = Value
        End Set
    End Property
    Public Property FechaPub() As Date
        Get
            Return m_dFecPublicacion
        End Get

        Set(ByVal Value As Date)
            m_dFecPublicacion = Value
        End Set
    End Property
    Public Property FechaDesPub() As Date
        Get
            Return m_dFecDesPub
        End Get

        Set(ByVal Value As Date)
            m_dFecDesPub = Value
        End Set
    End Property
    Public Property FechaLimCumpl() As Date
        Get
            Return m_dFecLimCumpl
        End Get

        Set(ByVal Value As Date)
            m_dFecLimCumpl = Value
        End Set
    End Property
    Public Property FechaCumpl() As Date
        Get
            Return m_dFecCumpl
        End Get

        Set(ByVal Value As Date)
            m_dFecCumpl = Value
        End Set
    End Property
    Public Property FechaExpiracion As Date
        Get
            Return m_dFecExpiracion
        End Get

        Set(ByVal Value As Date)
            m_dFecExpiracion = Value
        End Set
    End Property
    Public Property Publicada() As Boolean
        Get
            Return m_bPublicada
        End Get

        Set(ByVal Value As Boolean)
            m_bPublicada = Value
        End Set
    End Property
    Public Property Baja() As Boolean
        Get
            Return m_bBaja
        End Get

        Set(ByVal Value As Boolean)
            m_bBaja = Value
        End Set
    End Property
    Public Property Version() As Long
        Get
            Return m_lVersion
        End Get

        Set(ByVal Value As Long)
            m_lVersion = Value
        End Set
    End Property
    Public Property Proveedor() As String
        Get
            Return m_sProveedor
        End Get

        Set(ByVal Value As String)
            m_sProveedor = Value
        End Set
    End Property
    Public Property ProveDen() As String
        Get
            Return m_sProveDen
        End Get

        Set(ByVal Value As String)
            m_sProveDen = Value
        End Set
    End Property
    Public Property ProveedorBaja() As Boolean
        Get
            Return m_bProveBaja
        End Get

        Set(ByVal Value As Boolean)
            m_bProveBaja = Value
        End Set
    End Property
    Public Property Contacto() As Long
        Get
            Return m_lContacto
        End Get

        Set(ByVal Value As Long)
            m_lContacto = Value
        End Set
    End Property
    ''' <summary>
    ''' Obtener/Establecer el Id de la instancia
    ''' </summary>
    ''' <returns>Id de la instancia</returns>
    ''' <remarks>Llamada desde: Toda pantalla q haga uso de la clase Certificado; Tiempo maximo:0</remarks>
    Property Instancia() As Long
        Get
            Instancia = mlInstancia
        End Get
        Set(ByVal Value As Long)
            mlInstancia = Value
        End Set
    End Property
    Public Property Proveedores() As DataTable
        Get
            Return moProveedores
        End Get

        Set(ByVal Value As DataTable)
            moProveedores = Value
        End Set
    End Property
    Public ReadOnly Property Data() As Data.DataSet
        Get
            Return moData
        End Get
    End Property
    Public Property ProveedorEnvioNotificacion() As String
        Get
            Return m_sProveedorEnvioNotificacion
        End Get

        Set(ByVal Value As String)
            m_sProveedorEnvioNotificacion = Value
        End Set
    End Property
    Public Property EmailEnvioNotificacion() As String
        Get
            Return m_sEmailEnvioNotificacion
        End Get

        Set(ByVal Value As String)
            m_sEmailEnvioNotificacion = Value
        End Set
    End Property
    Public Property NombreEnvioNotificacion() As String
        Get
            Return m_sNombreEnvioNotificacion
        End Get

        Set(ByVal Value As String)
            m_sNombreEnvioNotificacion = Value
        End Set
    End Property
    Public Property IdInstanciaNotificacion() As String
        Get
            Return m_lIdInstanciaNotificacion
        End Get

        Set(ByVal Value As String)
            m_lIdInstanciaNotificacion = Value
        End Set
    End Property
    ''' <summary>
    ''' Obtener/Establecer el Peticionario de la instancia.
    ''' El stored de carga devuelve columna CERTIFICADO.PER y otra columna INSTANCIA.PER AS IPETICIONARIO. Tratamos IPETICIONARIO.
    ''' </summary>
    ''' <returns>Peticionario de la instancia</returns>
    ''' <remarks>Llamada desde: Toda pantalla q haga uso de la clase Certificado; Tiempo maximo:0</remarks>
    Public Property PeticionarioCod() As String
        Get
            Return msCodPeticionario
        End Get

        Set(ByVal Value As String)
            msCodPeticionario = Value
        End Set
    End Property
    ''' <summary>
    ''' Obtener/Establecer el Nombre del Peticionario de la instancia.
    ''' El stored de carga devuelve columna P.NOM+P.APE AS NOMBRE y otra columna PPET.NOM+PPET.APE AS NOMBREPETICIONARIO. Donde 
    ''' se hace JOIN PER P ON CERTIFICADO.PER ... y JOIN PER PPET ON INSTANCIA.PER ...
    ''' Tratamos NOMBREPETICIONARIO.
    ''' </summary>
    ''' <returns>Nombre del Peticionario de la instancia</returns>
    ''' <remarks>Llamada desde: Toda pantalla q haga uso de la clase Certificado; Tiempo maximo:0</remarks>
    Public Property Peticionario() As String
        Get
            Return msPeticionario
        End Get

        Set(ByVal Value As String)
            msPeticionario = Value
        End Set
    End Property
    Public Property Validado As Nullable(Of Integer)
        Get
            Return m_iValidado
        End Get

        Set(ByVal Value As Nullable(Of Integer))
            m_iValidado = Value
        End Set
    End Property
	''' <summary>
	''' Obtener/Establecer la denominaci�n de la Solicitud, en la q este basada el certificado, lo devuelve en el idioma 
	''' del usuario conectado. El stored de carga devuelve columna SOLICTUD.DEN + @IDI AS DEN
	''' </summary>
	''' <returns>Denominaci�n de la Solicitud</returns>
	''' <remarks>Llamada desde: Toda pantalla q haga uso de la clase Certificado; Tiempo maximo:0</remarks>
	Public Property TipoDen() As String
		Get
			Return msDenSolictud
		End Get

		Set(ByVal Value As String)
			msDenSolictud = Value
		End Set
	End Property
	Private _Id_Estado_Texto As Integer
	Public Property Id_Estado_Texto() As Integer
		Get
			Return _Id_Estado_Texto
		End Get
		Set(ByVal value As Integer)
			_Id_Estado_Texto = value
		End Set
	End Property
#End Region
	''' <summary>
	''' Obtener/Establecer el ID de la Solicitud, en la q este basada el certificado.
	''' El stored de carga devuelve columna SOLICTUD.ID AS IDSOL
	''' </summary>
	''' <returns>ID de la Solicitud</returns>
	''' <remarks>Llamada desde: Toda pantalla q haga uso de la clase Certificado; Tiempo maximo:0</remarks>
	Public Property IDSolicitud() As Long
        Get
            Return mlIDSolicitud
        End Get

        Set(ByVal Value As Long)
            mlIDSolicitud = Value
        End Set
    End Property
    ''' <summary>
    ''' Procedimiento que carga los datos generales de un certificado
    ''' </summary>
    ''' <remarks>
    ''' Llamada desde: PmWeb/Certificados/PublicadoOK/PageLoad
    ''' Tiempo m�ximo:0,3 seg </remarks>
    Public Sub CargarDatosGenerales()
        Authenticate()
        Dim data As DataSet
        Dim i As Integer

        data = DBServer.Certificado_LoadDatosYProv(mlInstancia, 0)

        If Not data.Tables(0).Rows.Count = 0 Then
            m_dFecPublicacion = DBNullToSomething(data.Tables(0).Rows(i).Item("FEC_PUBLICACION"))
            m_dFecDesPub = DBNullToSomething(data.Tables(0).Rows(i).Item("FEC_DESPUB"))
        End If

        moProveedores = data.Tables(1)

        data = Nothing
    End Sub
    ''' <summary>
    ''' Procedimiento que carga un certificado
    ''' </summary>
    ''' <param name="Idioma">Idioma en el q sacar la denominaci�n de la solicitud</param>
    ''' <remarks>
    ''' Llamada desde: PMWeb/detalleCertificado/Page_Load ,PMWEb/impexpPage_Load, WebServicePM/Service.asmx/ServiceThread
    ''' Tiempo m�ximo: 0,5 seg</remarks>
    Public Sub Load(ByVal Idioma As String)
        Authenticate()

        Dim data As DataSet
        data = DBServer.Certificado_Load(mlID, Idioma)
        If Not data.Tables(0).Rows.Count = 0 Then
            m_dFecPublicacion = DBNullToSomething(data.Tables(0).Rows(0).Item("FEC_PUBLICACION"))

            m_dFecDesPub = DBNullToSomething(data.Tables(0).Rows(0).Item("FEC_DESPUB"))
            m_dFecLimCumpl = DBNullToSomething(data.Tables(0).Rows(0).Item("FEC_LIM_CUMPLIM"))
            m_dFecCumpl = DBNullToSomething(data.Tables(0).Rows(0).Item("FEC_CUMPLIM"))
            m_dFecExpiracion = DBNullToSomething(data.Tables(0).Rows(0).Item("FECHA_EXPIR"))
            m_bPublicada = SQLBinaryToBoolean(data.Tables(0).Rows(0).Item("PUBLICADA"))
            m_sProveedor = data.Tables(0).Rows(0).Item("PROVE")
            m_sProveDen = DBNullToSomething(data.Tables(0).Rows(0).Item("DEN_PROVE"))
            m_bProveBaja = SQLBinaryToBoolean(data.Tables(0).Rows(0).Item("BAJA_PROVE"))
            m_lVersion = DBNullToSomething(data.Tables(0).Rows(0).Item("NUM_VERSION"))
            m_lContacto = DBNullToSomething(data.Tables(0).Rows(0).Item("CONTACTO"))
            mlInstancia = data.Tables(0).Rows(0).Item("INSTANCIA")
            m_iValidado = DBNullToSomething(data.Tables(0).Rows(0).Item("VALIDADO"))

            msCodPeticionario = DBNullToSomething(data.Tables(0).Rows(0).Item("IPETICIONARIO"))
            msPeticionario = DBNullToSomething(data.Tables(0).Rows(0).Item("NOMBREPETICIONARIO"))

            mlIDSolicitud = DBNullToSomething(data.Tables(0).Rows(0).Item("IDSOL"))
			msDenSolictud = DBNullToSomething(data.Tables(0).Rows(0).Item("DEN"))
			_Id_Estado_Texto = DBNullToSomething(data.Tables(0).Rows(0).Item("ID_TEXTO_ESTADO"))
		End If

        data = Nothing
    End Sub
    ''' <summary>
    ''' Procedimiento que carga todas las versiones de un certificado
    ''' </summary>
    ''' <remarks>
    ''' Llamada desde: PMWeb/detalleCertificado/Page_Load
    ''' Tiempo m�ximo: 0,5 seg</remarks>
    Public Sub LoadVersiones()
        Authenticate()

        moData = DBServer.Certificado_LoadVersiones(mlID)
    End Sub
    ''' <summary>
    ''' Procedimiento que modifica una publicaci�n, cambiando su fecha l�mite
    ''' </summary>
    ''' <param name="bModifPublic">Variable booleana que indica si se debe modificar la publicaci�n</param>
    ''' <remarks>
    ''' Llamada desde: PMWeb/publicarCertificado/Page_Load
    ''' Tiempo m�ximo: 0,35 seg</remarks>
    Public Sub ModificarPublicacion(ByVal bModifPublic As Boolean)
        Authenticate()

        DBServer.Certificado_ModificarPublicacion(BooleanToSQLBinary(bModifPublic), mlID, BooleanToSQLBinary(m_bPublicada), IIf(IsTime(m_dFecDesPub), System.DBNull.Value, m_dFecDesPub), IIf(IsTime(m_dFecLimCumpl), System.DBNull.Value, m_dFecLimCumpl))
    End Sub
    ''' <summary>
    ''' Borrar una versi�n del certificado
    ''' </summary>
    ''' <param name="sPer">Persona q elimina</param>
    ''' <param name="sFrom">Email del usuario q elimino</param> 
    ''' <param name="NotificarPortal">Notificar el borrado al portal</param>        
    ''' <remarks>Llamada desde:EliminarVersionProveedor.aspx; Tiempo m�ximo:0,1</remarks>
    Public Sub EliminarVersionesCertificado(ByVal sPer As String, Optional ByVal sFrom As String = "", Optional ByVal NotificarPortal As Boolean = False)
        Authenticate()

        DBServer.Certificado_EliminarVersiones(mlID, mlInstancia, m_lVersion, sPer)
        If NotificarPortal Then
            Dim oNotificador As New Notificar(DBServer, mIsAuthenticated)
            oNotificador.NotificacionBorradoPortal(mlInstancia, mlID, sFrom, False)
            oNotificador.FuerzaFinalizeSmtpClient()
            oNotificador = Nothing
        End If
    End Sub
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
    ''' <summary>
    ''' Funcion que crea una previsualizacion del certificado a crear
    ''' </summary>
    ''' <param name="bEnviar">Variable booleana que indica si se va a enviar el certificado</param>
    ''' <remarks>
    ''' Llamada desde: PMWeb/GuardarInstancia/GuardarSinWorkFlow
    ''' Tiempo m�ximo: 0,8 seg</remarks>
    Public Sub Create_Prev(ByVal bEnviar As Boolean)
        Authenticate()
        DBServer.Certificado_Create_Prev(mlID, m_sProveedor, m_lContacto, m_dFecDesPub, m_dFecLimCumpl, msCodPeticionario, mlInstancia, mlIDSolicitud, bEnviar)
    End Sub
    Public Sub NotificarEnvioCertificadoPortal()
        Authenticate()

        m_lVersion = DBServer.Instancia_ObtenerUltimaVersion(m_lIdInstanciaNotificacion)

        ThreadPool.QueueUserWorkItem(New WaitCallback(AddressOf NotificacionCertificadoDesdePortal))
    End Sub
    ''' <summary>
    ''' Notifica grabaci�n Certificado desde portal
    ''' </summary>
    ''' <remarks>Llamada desde: NotificarEnvioCertificadoPortal; Tiempo maximo: 0,2</remarks>
    Private Sub NotificacionCertificadoDesdePortal()
        Dim oNotificador As New Notificar(DBServer, mIsAuthenticated, , True)

        oNotificador.NotificacionEnvioCertificado(m_sProveedorEnvioNotificacion, m_lIdInstanciaNotificacion, mlID, m_lVersion, m_sEmailEnvioNotificacion, m_sNombreEnvioNotificacion)
        oNotificador.FuerzaFinalizeSmtpClient()
        oNotificador = Nothing
    End Sub
    Public Sub NotificarEnvioCertificadoQA()
        If m_sEmailEnvioNotificacion <> "" Then
            ThreadPool.QueueUserWorkItem(New WaitCallback(AddressOf NotificarEmisionCertificadoQA))
        End If
    End Sub
    ''' <summary>
    ''' Notifica emisi�n Certificado 
    ''' </summary>
    ''' <remarks>Llamada desde: NotificarEnvioCertificadoQA; Tiempo maximo: 0,2</remarks>
    Private Sub NotificarEmisionCertificadoQA()
        Dim oNotificador As New Notificar(DBServer, mIsAuthenticated)

        oNotificador.NotificacionCertificadoEmitido(sFrom:=m_sEmailEnvioNotificacion, lCertificado:=mlID)
        oNotificador.FuerzaFinalizeSmtpClient()
        oNotificador = Nothing
    End Sub
    ''' <summary>
    ''' Funci�n para obtener el certificado implicado en la puntuaci�n de una variable de calidad de tipo certificado.
    ''' </summary>
    ''' <param name="Instancia">Instancia del certificado a mostrar</param>  
    ''' <param name="Version">Version del certificado a mostrar</param>  
    ''' <returns>Dataset con la variable de calidad cargada</returns>
    ''' <remarks>Llamada desde: fichaProveedor.aspx/MostrarPanelCert; Tiempo m�ximo: 0,1</remarks>
    Public Function DatosPuntuacionCert(ByVal Instancia As Long, ByVal Version As Integer) As DataSet
        Authenticate()

        Return DBServer.Puntuacion_DatosPuntuacionCert(Instancia, Version)
    End Function
    ''' <summary>
    ''' Devuelve los peticionarios al que el usuario puede visualizar
    ''' </summary>
    ''' <param name="sUsu">Codigo Usuario</param>
    ''' <returns>Dataset con los peticionarios (Cod persona // Den Nombre completo persona)</returns>
    ''' <remarks>Llamada desde=FiltroQA --> CargarPeticionarios; Tiempo m�ximo=0</remarks>
    Public Function CargarPeticionarios(ByVal sUsu As String, _
        ByVal RestricSolicUsu As Boolean, ByVal RestricSolicUO As Boolean, ByVal RestricSolicDep As Boolean, _
        ByVal RestricProvMat As Boolean, ByVal RestricProvEqp As Boolean, ByVal RestricProvCon As Boolean) As DataSet
        Authenticate()

        Return DBServer.Certificado_GetPeticionarios(sUsu, RestricSolicUsu, RestricSolicUO, RestricSolicDep, _
                                                RestricProvMat, RestricProvEqp, RestricProvCon)
    End Function
    Public Sub FinalizarCertPendienteEnviar()
        Authenticate()
        DBServer.Certificado_FinalizarCertPendienteEnviar(mlID, m_lIdInstanciaNotificacion)
    End Sub
    ''' <summary>Hace la solicitud autom�tica de los certificados que dependen del certificado actual</summary>    
    Public Sub SolicitudAutomaticaTiposCertificadosCondiciones()
        Authenticate()

        'Mirar si el certificado tiene fecha de cumplimentaci�n
        Dim dsDataCert As DataSet = DBServer.Certificado_LoadDatosYProv(mlInstancia, 1, mlID)
        If Not dsDataCert Is Nothing Then
            If dsDataCert.Tables.Count > 0 Then
                If Not dsDataCert.Tables(0).Rows(0).IsNull("FEC_CUMPLIM") Then
                    Dim dtCertificadosSolicit As DataTable = NuevoDataTableProcesarCertificados()
                    Dim dtCertificadosDespub As DataTable = NuevoDataTableProcesarCertificados()
                    Dim sPer As String = String.Empty
                    Dim sEMail As String = String.Empty

                    'Obtener certificados dependientes
                    Dim dsCertRelData As DataSet = DBServer.Certificado_DevolverDependientesPorCondiciones(mlID, dsDataCert.Tables(0).Rows(0)("COD"))

                    If dsCertRelData.Tables.Count > 0 Then
                        If dsCertRelData.Tables(0).Rows.Count > 0 Then
                            sPer = dsCertRelData.Tables(0).Rows(0)("PER")
                            sEMail = dsCertRelData.Tables(0).Rows(0)("EMAIL")
                        End If

                        'Certificados a solicitar
                        For Each drCertRel As DataRow In dsCertRelData.Tables(1).Rows
                            Dim sFormula As String = String.Empty
                            If Not drCertRel.IsNull("CONDICIONES_FORMULA") Then sFormula = drCertRel("CONDICIONES_FORMULA")

                            'Comprobar si cumple la f�rmula
                            If sFormula.Length > 0 Then
                                If ValidarFormula(sFormula, dsCertRelData.Tables(2).Select("SOLICITUD=" & drCertRel("SOLICITUD"))) Then
                                    Dim drCertSol As DataRow = dtCertificadosSolicit.NewRow
                                    With drCertSol
                                        .Item("TIPO_CERTIF") = drCertRel("SOLICITUD")
                                        .Item("PROVE") = dsDataCert.Tables(0).Rows(0)("COD")
                                        .Item("INSTANCIA") = 0
                                        .Item("IDCERTIFICADO") = 0
                                        .Item("ENVIARMAIL") = 1
                                    End With
                                    dtCertificadosSolicit.Rows.Add(drCertSol)
                                End If
                            End If
                        Next

                        'Comprobar certificados ya solicitados
                        For Each drCertRel As DataRow In dsCertRelData.Tables(3).Rows
                            Dim sFormula As String = String.Empty
                            If Not drCertRel.IsNull("CONDICIONES_FORMULA") Then sFormula = drCertRel("CONDICIONES_FORMULA")

                            'Comprobar si cumple la f�rmula
                            If sFormula.Length > 0 Then
                                If Not ValidarFormula(sFormula, dsCertRelData.Tables(4).Select("SOLICITUD=" & drCertRel("SOLICITUD"))) Then
                                    Dim drCertDespub As DataRow = dtCertificadosDespub.NewRow
                                    With drCertDespub
                                        .Item("TIPO_CERTIF") = drCertRel("SOLICITUD")
                                        .Item("PROVE") = dsDataCert.Tables(0).Rows(0)("COD")
                                        .Item("INSTANCIA") = 0
                                        .Item("IDCERTIFICADO") = drCertRel("ID_CERTIF")
                                        .Item("ENVIARMAIL") = 1
                                    End With
                                    dtCertificadosDespub.Rows.Add(drCertDespub)
                                End If
                            End If
                        Next
                    End If

                    Dim oCertificados As FSNServer.Certificados
                    oCertificados = New Certificados(DBServer, mIsAuthenticated)

                    'Solicitar certificados
                    If dtCertificadosSolicit.Rows.Count > 0 Then oCertificados.Solicitar_Renovar(dtCertificadosSolicit, False, sPer, sEMail)
                    'Despublicar certificados
                    If dtCertificadosDespub.Rows.Count > 0 Then oCertificados.Despublicar(dtCertificadosDespub)
                End If
            End If
        End If
    End Sub
    Private Function ValidarFormula(ByVal sFormula As String, ByVal drCondiciones As DataRow()) As Boolean
        Dim oValorIzq As Object
        Dim oValorDer As Object
        Dim dValues() As Double = Nothing
        Dim sVariables() As String = Nothing
        Dim i As Integer
        Dim dValor As Double

        i = 0
        For Each drCondicion As DataRow In drCondiciones
            ReDim Preserve dValues(i)
            ReDim Preserve sVariables(i)

            sVariables(i) = DBNullToSomething(drCondicion.Item("COD"))

            'Tomar valores
            Select Case drCondicion.Item("SUBTIPO")
                Case 2
                    oValorIzq = DBNullToSomething(drCondicion.Item("VALOR_NUM_IZQ"))
                    oValorDer = DBNullToSomething(drCondicion.Item("VALOR_NUM_DER"))
                Case 3
                    oValorIzq = DBNullToSomething(drCondicion.Item("VALOR_FEC_IZQ"))
                    oValorDer = DBNullToSomething(drCondicion.Item("VALOR_FEC_DER"))
                Case 4
                    oValorIzq = DBNullToSomething(drCondicion.Item("VALOR_BOOL_IZQ"))
                    oValorDer = DBNullToSomething(drCondicion.Item("VALOR_BOOL_DER"))
                Case Else
                    oValorIzq = DBNullToSomething(drCondicion.Item("VALOR_TEXT_IZQ"))
                    oValorDer = DBNullToSomething(drCondicion.Item("VALOR_TEXT_DER"))
            End Select

            Dim oCond As New Condicion(DBServer, mIsAuthenticated, oValorIzq, oValorDer, drCondicion.Item("OPERADOR"), drCondicion.Item("SUBTIPO"))
            dValues(i) = IIf(oCond.Validar, 1, 0)

            i += 1
        Next

        Dim iEq As New USPExpress.USPExpression
        iEq.Parse(sFormula, sVariables)
        dValor = iEq.Evaluate(dValues)

        Return (dValor > 0)
    End Function
    Private Function NuevoDataTableProcesarCertificados() As DataTable
        Dim dtCertificados As New DataTable
        With dtCertificados
            .Columns.Add("TIPO_CERTIF", System.Type.GetType("System.Int32"))
            .Columns.Add("PROVE", System.Type.GetType("System.String"))
            .Columns.Add("INSTANCIA", System.Type.GetType("System.Int64"))
            .Columns.Add("IDCERTIFICADO", System.Type.GetType("System.Int64"))
            .Columns.Add("ENVIARMAIL", System.Type.GetType("System.Int32"))
        End With
        Return dtCertificados
    End Function
End Class

