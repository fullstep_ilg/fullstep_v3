﻿Public Class GrupoMatQA
    Inherits Security
    Implements IGmn

    Protected moData As DataSet
    Protected msCod As Integer
    Protected msDen As String

    Protected moGruposMatNivel2 As GruposMatNivel2
    Protected _children As Dictionary(Of String, IGmn)
    Protected _hasChild As Boolean
    Protected _parent As IGmn

    Property Cod() As Integer Implements IGmn.GmnQAcod
        Get
            Return msCod
        End Get
        Set(ByVal value As Integer)
            msCod = value
        End Set
    End Property

    Property Den() As String Implements IGmn.Den
        Get
            Return msDen
        End Get
        Set(ByVal Value As String)
            msDen = Value
        End Set
    End Property

    Public Property children As System.Collections.Generic.Dictionary(Of String, IGmn) Implements IGmn.children
        Get
            If _children Is Nothing Then
                _children = New Dictionary(Of String, IGmn)
                moData = DBServer.GruposMatNivel1_CargarMaterialesProve(Me.user.QARestProvMaterial, _
                                user.CodPersona, user.Idioma, Me.Cod)
                For Each oRow As DataRow In moData.Tables(0).Rows
                    Dim m As GrupoMatNivel1 = New GrupoMatNivel1(DBServer, mIsAuthenticated)
                    m.user = user
                    m.Cod = oRow("COD")
                    m.Den = oRow("DEN")
                    m.gmnQAcod = Me.Cod
                    m.parent = Me
                    m.EstructuraMateriales = Me.EstructuraMateriales
                    _children.Add(m.key, m)
                Next
            End If
            Return _children
        End Get
        Set(value As System.Collections.Generic.Dictionary(Of String, IGmn))

        End Set
    End Property

    Public Function child(node As IGmn) As IGmn Implements IGmn.child
        If children.ContainsKey(node.key) Then
            Return children(node.key)
        Else
            For Each hijo As IGmn In children.Values
                If hijo.gmn4Cod = node.gmn4Cod Then
                    Return hijo.child(node)
                End If
            Next
        End If
        Return Nothing
    End Function

    Public Overridable ReadOnly Property hasChildren As Boolean Implements IGmn.hasChildren
        Get
            Return Me.EstructuraMateriales.TipoEstructuraMateriales = Operadores.FiltroMaterial.FiltroMatQAGS
        End Get
    End Property

    Public Property parent As IGmn Implements IGmn.parent
        Get
            Return _parent
        End Get
        Set(value As IGmn)
            _parent = value
        End Set
    End Property

    Public Function topparent() As IGmn Implements IGmn.topParent
        Return Me
    End Function

    Private _e As EstructuraMateriales
    Public Property EstructuraMateriales() As EstructuraMateriales Implements IGmn.Estructura
        Get
            Return _e
        End Get
        Set(value As EstructuraMateriales)
            _e = value
        End Set
    End Property

    Public ReadOnly Property key As String Implements IGmn.key
        Get
            Return msCod
        End Get
    End Property

    Public Function nivel() As Integer Implements IGmn.nivel
        Return 0
    End Function

    Public Function name() As String Implements IGmn.title
        Return msCod & " - " & msDen
    End Function

    Public Overridable Function incluye(o As IGmn) As Boolean Implements IGmn.incluye
        Return False
    End Function

    Public Overridable Function contiene(o As IGmn) As Boolean Implements IGmn.contiene
        Return False
    End Function

    Public Property cod1 As String Implements IGmn.gmn1Cod
        Get
            Return Nothing
        End Get
        Set(value As String)

        End Set
    End Property
    Public Property cod2 As String Implements IGmn.gmn2Cod
        Get
            Return Nothing
        End Get
        Set(value As String)

        End Set
    End Property
    Public Property cod3 As String Implements IGmn.gmn3Cod
        Get
            Return Nothing
        End Get
        Set(value As String)

        End Set
    End Property
    Public Property cod4 As String Implements IGmn.gmn4Cod
        Get
            Return Nothing
        End Get
        Set(value As String)

        End Set
    End Property
    Private _user As FSNServer.User
    Public Property user As FSNServer.User Implements IGmn.User
        Get
            Return _user
        End Get
        Set(value As FSNServer.User)
            _user = value
        End Set
    End Property
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
End Class
