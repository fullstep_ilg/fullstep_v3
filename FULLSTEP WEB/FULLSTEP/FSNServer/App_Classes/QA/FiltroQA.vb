﻿<Serializable()> _
    Public Class FiltroQA
    Inherits Security

#Region " PROPIEDADES "

    Private mlIDFiltro As Long
    Private mlIDFiltroUsuario As Long
    Private msFiltroUsuario As String
    Private msNombreFiltro As String
    Private msPersona As String

    Private msOrdenacion As String
    Private msSentidoOrdenacion As String
    Private miPorDefecto As Integer

    Private miCamposGeneralesID() As Integer
    Private mbCamposGeneralesVisible() As Boolean
    Private mlCamposGeneralesPosicion() As Long
    Private mlCamposGeneralesTamanyo() As Long

    Private mdtCamposConfiguracion As DataSet

    'Estados
    Private mlGuardadas As Long
    Private mlEnCursoDeAprobacion As Long
    Private mlAbiertas As Long
    Private mlPendientesRevisarCierrre As Long
    Private mlCierreEficazDentroPlazo As Long
    Private mlCierreEficazFueraPlazo As Long
    Private mlCierreNoEficaz As Long
    Private mlAnuladas As Long
    Private mlTodas As Long

    'No Conformidades visor.
    Private mdtNoConformidades As DataTable
    'certificado visor.
    Private mdtCertificados As DataSet
    '3.NC   2.Cert
    Private miTipoSol As TiposDeDatos.TipoDeSolicitud
    'ConfigurarFiltros
    Private mbUnForm As Boolean
    Private miNFiltrosConfig As Integer

    Property IDFiltroUsuario() As Long
        Get
            IdFiltroUsuario = mlIDFiltroUsuario
        End Get
        Set(ByVal Value As Long)
            mlIDFiltroUsuario = Value
        End Set
    End Property
    ''' <summary>
    ''' Dar acceso a la propiedad denominación del formulario asociado al filtro
    ''' </summary>
    ''' <returns>La denominación del formulario asociado al filtro</returns>
    ''' <remarks>Llamada desde: ConfigurarFiltrosQA/Page_Load; Tiempo maximo:0</remarks>
    Property TextFiltroUsuario() As String
        Get
            TextFiltroUsuario = msFiltroUsuario
        End Get
        Set(ByVal Value As String)
            msFiltroUsuario = Value
        End Set
    End Property
    Property IDFiltro() As Long
        Get
            IDFiltro = mlIDFiltro
        End Get
        Set(ByVal Value As Long)
            mlIDFiltro = Value
        End Set
    End Property
    Property NombreFiltro() As String
        Get
            NombreFiltro = msNombreFiltro
        End Get
        Set(ByVal Value As String)
            msNombreFiltro = Value
        End Set
    End Property
    Property Persona() As String
        Get
            Persona = msPersona
        End Get
        Set(ByVal Value As String)
            msPersona = Value
        End Set
    End Property

    Property Ordenacion() As String
        Get
            Ordenacion = msOrdenacion
        End Get
        Set(ByVal Value As String)
            msOrdenacion = Value
        End Set
    End Property
    Property SentidoOrdenacion() As String
        Get
            SentidoOrdenacion = msSentidoOrdenacion
        End Get
        Set(ByVal Value As String)
            msSentidoOrdenacion = Value
        End Set
    End Property
    ''' <summary>
    ''' Tipo de solicitud 3.NC   2.Cert
    ''' </summary>
    ''' <returns>Tipo de solicitud 3.NC   2.Cert</returns>
    ''' <remarks>Llamada desde: Visor Certificados/ConfigurarFiltrosQa/Visor No conformidades; Tiempo:0</remarks>
    Property TipoSol() As TiposDeDatos.TipoDeSolicitud
        Get
            TipoSol = miTipoSol
        End Get
        Set(ByVal Value As TiposDeDatos.TipoDeSolicitud)
            miTipoSol = Value
        End Set
    End Property
    ''' <summary>
    ''' Los Filtros se pueden definir para un formulario o para todos.
    ''' </summary>
    ''' <returns>True definido para un formulario /False definido para todos los formularios</returns>
    ''' <remarks>Llamada desde: ConfigurarFiltrosQa; Tiempo:0</remarks>
    Property UnForm() As Boolean
        Get
            UnForm = mbUnForm
        End Get
        Set(ByVal Value As Boolean)
            mbUnForm = Value
        End Set
    End Property
    ''' <summary>
    ''' Numero de filtros configurados para un usuario. 
    ''' En todo momento debe haber al menos uno, el de por defecto "Certificados". Como se puede modificar/eliminar cualquier filtro
    ''' podrías quedarte sin filtros, con esta propiedad se deshabilita eliminar de la configuración de filtros para impedirlo.
    ''' </summary>
    ''' <returns>Numero de filtros configurados para un usuario.</returns>
    ''' <remarks>Llamada desde: ConfigurarFiltrosQa; Tiempo:0</remarks>
    Property NFiltrosConfig() As Integer
        Get
            NFiltrosConfig = miNFiltrosConfig
        End Get
        Set(ByVal Value As Integer)
            miNFiltrosConfig = Value
        End Set
    End Property
#Region " CAMPOS GENERALES "
        'Property IdVisible() As Boolean
        '    Get
        '        IdVisible = mbIdVisible
        '    End Get
        '    Set(ByVal Value As Boolean)
        '        mbIdVisible = Value
        '    End Set
        'End Property


        'Property IdPosicion() As Long
        '    Get
        '        IdPosicion = mlIdPosicion
        '    End Get
        '    Set(ByVal Value As Long)
        '        mlIdPosicion = Value
        '    End Set
        'End Property
        'Property IdTamanyo() As Long
        '    Get
        '        IdTamanyo = mlIdTamanyo
        '    End Get
        '    Set(ByVal Value As Long)
        '        mlIdTamanyo = Value
        '    End Set
        'End Property
        'Property TipoVisible() As Boolean
        '    Get
        '        TipoVisible = mbTipoVisible
        '    End Get
        '    Set(ByVal Value As Boolean)
        '        mbTipoVisible = Value
        '    End Set
        'End Property
        'Property TipoPosicion() As Long
        '    Get
        '        TipoPosicion = mlTipoPosicion
        '    End Get
        '    Set(ByVal Value As Long)
        '        mlTipoPosicion = Value
        '    End Set
        'End Property
        'Property TipoTamanyo() As Long
        '    Get
        '        TipoTamanyo = mlTipoTamanyo
        '    End Get
        '    Set(ByVal Value As Long)
        '        mlTipoTamanyo = Value
        '    End Set
        'End Property
        'Property CodProveVisible() As Boolean
        '    Get
        '        CodProveVisible = mbCodProveVisible
        '    End Get
        '    Set(ByVal Value As Boolean)
        '        mbCodProveVisible = Value
        '    End Set
        'End Property
        'Property CodProvePosicion() As Long
        '    Get
        '        CodProvePosicion = mlCodProvePosicion
        '    End Get
        '    Set(ByVal Value As Long)
        '        mlCodProvePosicion = Value
        '    End Set
        'End Property
        'Property CodProveTamanyo() As Long
        '    Get
        '        CodProveTamanyo = mlCodProveTamanyo
        '    End Get
        '    Set(ByVal Value As Long)
        '        mlCodProveTamanyo = Value
        '    End Set
        'End Property
        'Property DenProveVisible() As Boolean
        '    Get
        '        DenProveVisible = mbDenProveVisible
        '    End Get
        '    Set(ByVal Value As Boolean)
        '        mbDenProveVisible = Value
        '    End Set
        'End Property
        'Property DenProvePosicion() As Long
        '    Get
        '        DenProvePosicion = mlDenProvePosicion
        '    End Get
        '    Set(ByVal Value As Long)
        '        mlDenProvePosicion = Value
        '    End Set
        'End Property
        'Property DenProveTamanyo() As Long
        '    Get
        '        DenProveTamanyo = mlDenProveTamanyo
        '    End Get
        '    Set(ByVal Value As Long)
        '        mlDenProveTamanyo = Value
        '    End Set
        'End Property
        'Property ContactoVisible() As Boolean
        '    Get
        '        ContactoVisible = mbContactoVisible
        '    End Get
        '    Set(ByVal Value As Boolean)
        '        mbContactoVisible = Value
        '    End Set
        'End Property
        'Property ContactoPosicion() As Long
        '    Get
        '        ContactoPosicion = mlContactoPosicion
        '    End Get
        '    Set(ByVal Value As Long)
        '        mlContactoPosicion = Value
        '    End Set
        'End Property
        'Property ContactoTamanyo() As Long
        '    Get
        '        ContactoTamanyo = mlContactoTamanyo
        '    End Get
        '    Set(ByVal Value As Long)
        '        mlContactoTamanyo = Value
        '    End Set
        'End Property

        'Property UNQAVisible() As Boolean
        '    Get
        '        UNQAVisible = mbUNQAVisible
        '    End Get
        '    Set(ByVal Value As Boolean)
        '        mbUNQAVisible = Value
        '    End Set
        'End Property
        'Property UNQAPosicion() As Long
        '    Get
        '        ContactoPosicion = mlUNQAPosicion
        '    End Get
        '    Set(ByVal Value As Long)
        '        mlContactoPosicion = Value
        '    End Set
        'End Property
        'Property UNQATamanyo() As Long
        '    Get
        '        UNQATamanyo = mlUNQATamanyo
        '    End Get
        '    Set(ByVal Value As Long)
        '        mlUNQATamanyo = Value
        '    End Set
        'End Property

        'Property PeticionarioVisible() As Boolean
        '    Get
        '        PeticionarioVisible = mbPeticionarioVisible
        '    End Get
        '    Set(ByVal Value As Boolean)
        '        mbPeticionarioVisible = Value
        '    End Set
        'End Property
        'Property PeticionarioPosicion() As Long
        '    Get
        '        PeticionarioPosicion = mlPeticionarioPosicion
        '    End Get
        '    Set(ByVal Value As Long)
        '        mlPeticionarioPosicion = Value
        '    End Set
        'End Property
        'Property PeticionarioTamanyo() As Long
        '    Get
        '        PeticionarioTamanyo = mlPeticionarioTamanyo
        '    End Get
        '    Set(ByVal Value As Long)
        '        mlPeticionarioTamanyo = Value
        '    End Set
        'End Property
        'Property RevisorVisible() As Boolean
        '    Get
        '        RevisorVisible = mbRevisorVisible
        '    End Get
        '    Set(ByVal Value As Boolean)
        '        mbRevisorVisible = Value
        '    End Set
        'End Property
        'Property RevisorPosicion() As Long
        '    Get
        '        RevisorPosicion = mlRevisorPosicion
        '    End Get
        '    Set(ByVal Value As Long)
        '        mlRevisorPosicion = Value
        '    End Set
        'End Property
        'Property RevisorTamanyo() As Long
        '    Get
        '        RevisorTamanyo = mlRevisorTamanyo
        '    End Get
        '    Set(ByVal Value As Long)
        '        mlRevisorTamanyo = Value
        '    End Set
        'End Property
        'Property FechaAltaVisible() As Boolean
        '    Get
        '        FechaAltaVisible = mbFechaAltaVisible
        '    End Get
        '    Set(ByVal Value As Boolean)
        '        mbFechaAltaVisible = Value
        '    End Set
        'End Property
        'Property FechaAltaPosicion() As Long
        '    Get
        '        FechaAltaPosicion = mlFechaAltaPosicion
        '    End Get
        '    Set(ByVal Value As Long)
        '        mlFechaAltaPosicion = Value
        '    End Set
        'End Property
        'Property FechaAltaTamanyo() As Long
        '    Get
        '        FechaAltaTamanyo = mlFechaAltaTamanyo
        '    End Get
        '    Set(ByVal Value As Long)
        '        mlFechaAltaTamanyo = Value
        '    End Set
        'End Property
        'Property FechaLimiteResolucionVisible() As Boolean
        '    Get
        '        FechaLimiteResolucionVisible = mbFechaLimiteResolucionVisible
        '    End Get
        '    Set(ByVal Value As Boolean)
        '        mbFechaLimiteResolucionVisible = Value
        '    End Set
        'End Property
        'Property FechaLimiteResolucionPosicion() As Long
        '    Get
        '        FechaLimiteResolucionPosicion = mlFechaLimiteResolucionPosicion
        '    End Get
        '    Set(ByVal Value As Long)
        '        mlFechaLimiteResolucionPosicion = Value
        '    End Set
        'End Property
        'Property FechaLimiteResolucionTamanyo() As Long
        '    Get
        '        FechaLimiteResolucionTamanyo = mlFechaLimiteResolucionTamanyo
        '    End Get
        '    Set(ByVal Value As Long)
        '        mlFechaLimiteResolucionTamanyo = Value
        '    End Set
        'End Property
        'Property FechaEmisionVisible() As Boolean
        '    Get
        '        FechaEmisionVisible = mbFechaEmisionVisible
        '    End Get
        '    Set(ByVal Value As Boolean)
        '        mbFechaEmisionVisible = Value
        '    End Set
        'End Property
        'Property FechaEmisionPosicion() As Long
        '    Get
        '        FechaEmisionPosicion = mlFechaEmisionPosicion
        '    End Get
        '    Set(ByVal Value As Long)
        '        mlFechaEmisionPosicion = Value
        '    End Set
        'End Property
        'Property FechaEmisionTamanyo() As Long
        '    Get
        '        FechaEmisionTamanyo = mlFechaEmisionTamanyo
        '    End Get
        '    Set(ByVal Value As Long)
        '        mlFechaEmisionTamanyo = Value
        '    End Set
        'End Property
        'Property FechaRespuestaProveedorVisible() As Boolean
        '    Get
        '        FechaRespuestaProveedorVisible = mbFechaRespuestaProveedorVisible
        '    End Get
        '    Set(ByVal Value As Boolean)
        '        mbFechaRespuestaProveedorVisible = Value
        '    End Set
        'End Property
        'Property FechaRespuestaProveedorPosicion() As Long
        '    Get
        '        FechaRespuestaProveedorPosicion = mlFechaRespuestaProveedorPosicion
        '    End Get
        '    Set(ByVal Value As Long)
        '        mlFechaRespuestaProveedorPosicion = Value
        '    End Set
        'End Property
        'Property FechaRespuestaProveedorTamanyo() As Long
        '    Get
        '        FechaRespuestaProveedorTamanyo = mlFechaRespuestaProveedorTamanyo
        '    End Get
        '    Set(ByVal Value As Long)
        '        mlFechaRespuestaProveedorTamanyo = Value
        '    End Set
        'End Property
        'Property FechaActVisible() As Boolean
        '    Get
        '        FechaActVisible = mbFechaActVisible
        '    End Get
        '    Set(ByVal Value As Boolean)
        '        mbFechaActVisible = Value
        '    End Set
        'End Property
        'Property FechaActPosicion() As Long
        '    Get
        '        FechaActPosicion = mlFechaActPosicion
        '    End Get
        '    Set(ByVal Value As Long)
        '        mlFechaActPosicion = Value
        '    End Set
        'End Property
        'Property FechaActTamanyo() As Long
        '    Get
        '        FechaActTamanyo = mlFechaActTamanyo
        '    End Get
        '    Set(ByVal Value As Long)
        '        mlFechaActTamanyo = Value
        '    End Set
        'End Property
        'Property FechaCierreVisible() As Boolean
        '    Get
        '        FechaCierreVisible = mbFechaCierreVisible
        '    End Get
        '    Set(ByVal Value As Boolean)
        '        mbFechaCierreVisible = Value
        '    End Set
        'End Property
        'Property FechaCierrePosicion() As Long
        '    Get
        '        FechaCierrePosicion = mlFechaCierrePosicion
        '    End Get
        '    Set(ByVal Value As Long)
        '        mlFechaCierrePosicion = Value
        '    End Set
        'End Property
        'Property FechaCierreTamanyo() As Long
        '    Get
        '        FechaCierreTamanyo = mlFechaCierreTamanyo
        '    End Get
        '    Set(ByVal Value As Long)
        '        mlFechaCierreTamanyo = Value
        '    End Set
        'End Property
        'Property ComentariosVisible() As Boolean
        '    Get
        '        ComentariosVisible = mbComentariosVisible
        '    End Get
        '    Set(ByVal Value As Boolean)
        '        mbComentariosVisible = Value
        '    End Set
        'End Property
        'Property ComentariosPosicion() As Long
        '    Get
        '        ComentariosPosicion = mlComentariosPosicion
        '    End Get
        '    Set(ByVal Value As Long)
        '        mlComentariosPosicion = Value
        '    End Set
        'End Property
        'Property ComentariosTamanyo() As Long
        '    Get
        '        ComentariosTamanyo = mlComentariosTamanyo
        '    End Get
        '    Set(ByVal Value As Long)
        '        mlComentariosTamanyo = Value
        '    End Set
        'End Property

        'Property ComentarioAperturaVisible() As Boolean
        '    Get
        '        ComentarioAperturaVisible = mbComentarioAperturaVisible
        '    End Get
        '    Set(ByVal Value As Boolean)
        '        mbComentarioAperturaVisible = Value
        '    End Set
        'End Property
        'Property ComentarioAperturaPosicion() As Long
        '    Get
        '        ComentarioAperturaPosicion = mlComentarioAperturaPosicion
        '    End Get
        '    Set(ByVal Value As Long)
        '        mlComentarioAperturaPosicion = Value
        '    End Set
        'End Property
        'Property ComentarioAperturaTamanyo() As Long
        '    Get
        '        ComentarioAperturaTamanyo = mlComentarioAperturaTamanyo
        '    End Get
        '    Set(ByVal Value As Long)
        '        mlComentarioAperturaTamanyo = Value
        '    End Set
        'End Property

        'Property ComentarioCierreVisible() As Boolean
        '    Get
        '        ComentarioCierreVisible = mbComentarioCierreVisible
        '    End Get
        '    Set(ByVal Value As Boolean)
        '        mbComentarioCierreVisible = Value
        '    End Set
        'End Property
        'Property ComentarioCierrePosicion() As Long
        '    Get
        '        ComentarioCierrePosicion = mlComentarioCierrePosicion
        '    End Get
        '    Set(ByVal Value As Long)
        '        mlComentarioCierrePosicion = Value
        '    End Set
        'End Property
        'Property ComentarioCierreTamanyo() As Long
        '    Get
        '        ComentarioCierreTamanyo = mlComentarioCierreTamanyo
        '    End Get
        '    Set(ByVal Value As Long)
        '        mlComentarioCierreTamanyo = Value
        '    End Set
        'End Property

        'Property ComentarioRevisorVisible() As Boolean
        '    Get
        '        ComentarioRevisorVisible = mbComentarioRevisorVisible
        '    End Get
        '    Set(ByVal Value As Boolean)
        '        mbComentarioRevisorVisible = Value
        '    End Set
        'End Property
        'Property ComentarioRevisorPosicion() As Long
        '    Get
        '        ComentarioRevisorPosicion = mlComentarioRevisorPosicion
        '    End Get
        '    Set(ByVal Value As Long)
        '        mlComentarioRevisorPosicion = Value
        '    End Set
        'End Property
        'Property ComentarioRevisorTamanyo() As Long
        '    Get
        '        ComentarioRevisorTamanyo = mlComentarioRevisorTamanyo
        '    End Get
        '    Set(ByVal Value As Long)
        '        mlComentarioRevisorTamanyo = Value
        '    End Set
        'End Property


        'Property CamposGeneralesID() As Object
        '    Get
        '        CamposGeneralesID = miCamposGeneralesID
        '    End Get
        '    Set(ByVal Value As Object)
        '        miCamposGeneralesID = Value
        '    End Set
        'End Property

        'Property CamposGeneralesVisible() As Object
        '    Get
        '        CamposGeneralesVisible = mbCamposGeneralesVisible
        '    End Get
        '    Set(ByVal Value As Object)
        '        mbCamposGeneralesVisible = Value
        '    End Set
        'End Property
        'Property CamposGeneralesPosicion() As Object
        '    Get
        '        CamposGeneralesPosicion = mlCamposGeneralesPosicion
        '    End Get
        '    Set(ByVal Value As Object)
        '        mlCamposGeneralesPosicion = Value
        '    End Set
        'End Property
        'Property CamposGeneralesTamanyo() As Object
        '    Get
        '        CamposGeneralesTamanyo = mlCamposGeneralesTamanyo
        '    End Get
        '    Set(ByVal Value As Object)
        '        mlCamposGeneralesTamanyo = Value
        '    End Set
        'End Property



        'Dataset con 2 tablas


    Property CamposConfiguracion() As DataSet
        Get
            CamposConfiguracion = mdtCamposConfiguracion
        End Get
        Set(ByVal Value As DataSet)
            mdtCamposConfiguracion = Value
        End Set
    End Property

    ''' <summary>
    ''' El filtro inicial "Certificados" es modificable/eliminable y los filtros 100% de usuario son modificables/eliminables. 
    ''' Ambos tambien pueden ser el filtro por defecto. Aqui se marcan 2 cosas, si es una modificación del filtro inicial y si
    ''' es el de por defecto.
    ''' </summary>
    ''' <returns>0 filtro de usuario y no es el de por defecto                  1 filtro de usuario y es el de por defecto 
    ''' 2 filtro inicial "Certificados" y no es el de por defecto               3 filtro inicial "Certificados" y es el de por defecto  
    ''' </returns>
    ''' <remarks>Llamada desde: ConfigurarFiltrosQa; Tiempo:0</remarks>
    Property PorDefecto() As Integer
        Get
            PorDefecto = miPorDefecto
        End Get
        Set(ByVal Value As Integer)
            miPorDefecto = Value
        End Set
    End Property
#End Region

#Region " ESTADOS "

    ReadOnly Property ncGuardadas() As Long
        Get
            ncGuardadas = mlGuardadas
        End Get
    End Property
    ReadOnly Property ncEnCursoDeAprobacion() As Long
        Get
            ncEnCursoDeAprobacion = mlEnCursoDeAprobacion
        End Get
    End Property

    ReadOnly Property ncAbiertas() As Long
        Get
            ncAbiertas = mlAbiertas
        End Get
    End Property

    ReadOnly Property ncPendientesRevisarCierre() As Long
        Get
            ncPendientesRevisarCierre = mlPendientesRevisarCierrre
        End Get
    End Property
    ReadOnly Property ncCierreEficazDentroPlazo() As Long
        Get
            ncCierreEficazDentroPlazo = mlCierreEficazDentroPlazo
        End Get
    End Property
    ReadOnly Property ncCierreEficazFueraPlazo() As Long
        Get
            ncCierreEficazFueraPlazo = mlCierreEficazFueraPlazo
        End Get
    End Property

    ReadOnly Property ncCierreNoEficaz() As Long
        Get
            ncCierreNoEficaz = mlCierreNoEficaz
        End Get
    End Property

    ReadOnly Property ncAnuladas() As Long
        Get
            ncAnuladas = mlAnuladas
        End Get
    End Property

    ReadOnly Property ncTodas() As Long
        Get
            ncTodas = mlTodas
        End Get
    End Property
#End Region

#Region " BUSQUEDA AVANZADA "
    'Public Property BusquedaIdentificador() As Long
    '    Get
    '        BusquedaIdentificador = mlIdentificador
    '    End Get
    '    Set(ByVal Value As Long)
    '        mlIdentificador = Value
    '    End Set
    'End Property

    'Public Property BusquedaTipo() As Long
    '    Get
    '        BusquedaTipo = mlTipo
    '    End Get
    '    Set(ByVal Value As Long)
    '        mlTipo = Value
    '    End Set
    'End Property

    'Public Property BusquedaSubTipo() As String
    '    Get
    '        BusquedaSubTipo = msSubTipo
    '    End Get
    '    Set(ByVal Value As String)
    '        msSubTipo = Value
    '    End Set
    'End Property

    'Public Property BusquedaTitulo() As String
    '    Get
    '        BusquedaTitulo = msTitulo
    '    End Get
    '    Set(ByVal Value As String)
    '        msTitulo = Value
    '    End Set
    'End Property

    'Public Property BusquedaProveedor() As String
    '    Get
    '        BusquedaProveedor = msProveedor
    '    End Get
    '    Set(ByVal Value As String)
    '        msProveedor = Value
    '    End Set
    'End Property

    'Public Property BusquedaUnidadNegocio() As String
    '    Get
    '        BusquedaUnidadNegocio = msUnidadNegocio
    '    End Get
    '    Set(ByVal Value As String)
    '        msUnidadNegocio = Value
    '    End Set
    'End Property

    'Public Property BusquedaMaterial() As String
    '    Get
    '        BusquedaMaterial = msMaterial
    '    End Get
    '    Set(ByVal Value As String)
    '        msMaterial = Value
    '    End Set
    'End Property

    'Public Property BusquedaMaterialDen() As String
    '    Get
    '        BusquedaMaterialDen = msMaterialDen
    '    End Get
    '    Set(ByVal Value As String)
    '        msMaterialDen = Value
    '    End Set
    'End Property

    'Public Property BusquedaArticulo() As String
    '    Get
    '        BusquedaArticulo = msArticulo
    '    End Get
    '    Set(ByVal Value As String)
    '        msArticulo = Value
    '    End Set
    'End Property

    'Public Property BusquedaArticuloDen() As String
    '    Get
    '        BusquedaArticuloDen = msArticuloDen
    '    End Get
    '    Set(ByVal Value As String)
    '        msArticuloDen = Value
    '    End Set
    'End Property


    'Public Property BusquedaFechaDesde() As Date
    '    Get
    '        BusquedaFechaDesde = mdFechaDesde
    '    End Get
    '    Set(ByVal Value As Date)
    '        mdFechaDesde = Value
    '    End Set
    'End Property

    'Public Property BusquedaFechaHasta() As Date
    '    Get
    '        BusquedaFechaHasta = mdFechaHasta
    '    End Get
    '    Set(ByVal Value As Date)
    '        mdFechaHasta = Value
    '    End Set
    'End Property

    'Public Property BusquedaEstadoNCAbiertas() As String
    '    Get
    '        BusquedaEstadoNCAbiertas = msEstadoNCAbiertas
    '    End Get
    '    Set(ByVal Value As String)
    '        msEstadoNCAbiertas = Value
    '    End Set
    'End Property


    'Public Property BusquedaPeticionario() As String
    '    Get
    '        BusquedaPeticionario = msPeticionario
    '    End Get
    '    Set(ByVal Value As String)
    '        msPeticionario = Value
    '    End Set
    'End Property



    'Public Property BusquedaRevisor() As String
    '    Get
    '        BusquedaRevisor = msRevisor
    '    End Get
    '    Set(ByVal Value As String)
    '        msRevisor = Value
    '    End Set
    'End Property

    'Property CamposBusqueda() As DataSet
    '    Get
    '        CamposBusqueda = mdtCamposBusqueda
    '    End Get
    '    Set(ByVal Value As DataSet)
    '        mdtCamposBusqueda = Value
    '    End Set
    'End Property

#End Region

    Property NoConformidadesVisor() As DataTable
        Get
            NoConformidadesVisor = mdtNoConformidades
        End Get
        Set(ByVal Value As DataTable)
            mdtNoConformidades = Value
        End Set
    End Property

    ''' <summary>
    ''' DataTable con el filtro leido de bbdd.
    ''' </summary>
    ''' <returns>DataTable</returns>
    ''' <remarks>Llamada desde: Visor Certificados/ConfigurarFiltrosQa; Tiempo:0</remarks>
    Property CertificadosVisor() As DataSet
        Get
            CertificadosVisor = mdtCertificados
        End Get
        Set(ByVal Value As DataSet)
            mdtCertificados = Value
        End Set
    End Property
#End Region


    Public Sub New(ByRef dbserver As FSNDataBaseServer.Root,ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver,isAuthenticated)
    End Sub

    ''' <summary>
    ''' Carga el id, el nombre y la ordenacion del filtro.
    ''' Se le pasa tambien el cod. de persona para controlar que solo vea la configuracion del filtro el usuario, nadie mÃ¡s.
    ''' </summary>
    ''' <remarks>Llamada desde: ConfigurarFiltrosQA.aspx; Tiempo maximo: 0 sg</remarks>
    Public Sub LoadConfiguracion()
        Dim ds As DataSet
        Authenticate()
        ds = DBServer.FiltroQA_Load(mlIDFiltroUsuario, msPersona, IIf(miTipoSol = TiposDeDatos.TipoDeSolicitud.Certificado, 0, 1))

        If ds.Tables(0).Rows.Count > 0 Then
            mlIDFiltro = IIf(IsDBNull(ds.Tables(0).Rows(0)("ID_FILTRO")), -1, ds.Tables(0).Rows(0)("ID_FILTRO"))
            msNombreFiltro = ds.Tables(0).Rows(0)("NOM_FILTRO")
            msOrdenacion = DBNullToSomething(ds.Tables(0).Rows(0)("ORDENACION"))
            miPorDefecto = DBNullToDbl(ds.Tables(0).Rows(0)("DEFECTO"))
            mbUnForm = (mlIDFiltro > -1)
            msFiltroUsuario = DBNullToStr(ds.Tables(0).Rows(0)("DEN"))
        End If

        miNFiltrosConfig = ds.Tables(1).Rows(0)("NUM_CONFIG")
    End Sub

    ''' <summary>
    ''' Obtiene los campos del formulario para que el usuario elija los que quiera mostrar en el filtro. 
    '''  Se excluyen los campos tipo desglose y los que estan dentro de un desglose.
    ''' </summary>
    ''' <param name="sIdioma">Idioma en el que se va a mostrar la denominacion del campo</param>
    ''' <returns>Todos los grupos y los campos simples del formulario visibles para el usuario</returns>
    ''' <remarks>Llamada desde: ConfigruarFiltrosQA.aspx; Tiempo maximo: 0 sg</remarks>
    Public Function LoadCampos(ByVal sIdioma As String) As DataSet
        Authenticate()
        LoadCampos = DBServer.FiltroQA_GetCampos(mlIDFiltro, mlIDFiltroUsuario, sIdioma)
    End Function

    ''' <summary>
    ''' Obtiene los campos del formulario para que el usuario elija los que quiera mostrar en el filtro. 
    '''  Se excluyen los campos tipo desglose y los que estÃ¡n dentro de un desglose.
    ''' </summary>
    ''' <param name="sIdioma">Idioma en el que se va a mostrar la denominaciÃ³n del campo</param>
    ''' <returns>Todos los grupos y los campos simples del formulario visibles para el usuario</returns>
    ''' <remarks>Llamada desde: ConfigruarFiltrosQA.aspx; Tiempo mÃ¡ximo: 0 sg</remarks>
    Public Function LoadCamposGenerales(ByVal sIdioma As String) As DataSet
        Authenticate()
        LoadCamposGenerales = DBServer.FiltroQA_GetCamposGenerales(mlIDFiltro, mlIDFiltroUsuario, sIdioma)
    End Function

    ''' <summary>
    ''' Obtiene los campos del formulario para que el usuario elija los que quiera mostrar en el filtro. 
    '''  Se excluyen los campos tipo desglose y los que estÃ¡n dentro de un desglose.
    ''' </summary>
    ''' <param name="sIdioma">Idioma en el que se va a mostrar la denominaciÃ³n del campo</param>
    ''' <returns>Todos los grupos y los campos simples del formulario visibles para el usuario</returns>
    ''' <remarks>Llamada desde: ConfigruarFiltrosQA.aspx; Tiempo mÃ¡ximo: 0 sg</remarks>
    Public Function LoadCamposyCamposGenerales(ByVal sIdioma As String) As DataSet
        Authenticate()
        LoadCamposyCamposGenerales = DBServer.FiltroQA_GetCamposYCamposGenerales(mlIDFiltro, mlIDFiltroUsuario)
    End Function



    ''' <summary>
    ''' Obtiene una instancia ejemplo que se cargará en la grid para que el usuario pueda ver como queda con datos,
    ''' y pueda mover el ancho y el orden de la columnas.
    ''' </summary>
    ''' <param name="sIdioma">Idioma del usuario</param>
    ''' <returns>Una instancia ejemplo</returns>
    ''' <remarks>Llamada desde: ConfigurarFiltrosQA.aspx; Tiempo máximo: 1 sg</remarks>
    Public Function LoadConfiguracionInstancia(ByVal sIdioma As String) As DataSet
        Authenticate()
        LoadConfiguracionInstancia = DBServer.FiltroQA_LoadInstancia(mlIDFiltro, sIdioma, miTipoSol)
    End Function

    ''' <summary>
    ''' Guarda la configuración del filtro definida por el usuario.
    ''' </summary>
    ''' <remarks>Llamada desde: Filtros_QA/ConfigurarFiltrosQA.aspx. Tiempo máximo: 0 sg</remarks>
    Public Sub GuardarConfiguracion()
        Authenticate()
        mlIDFiltroUsuario = DBServer.FiltroQA_GuardarConfiguracion(mlIDFiltro, msPersona, msNombreFiltro, mlIDFiltroUsuario, mdtCamposConfiguracion, miPorDefecto, miTipoSol)
    End Sub

    ''' <summary>
    ''' Elimina la configuración del filtro definida por el usuario.
    ''' </summary>
    ''' <remarks>Llamada desde: FiltrosQA/ConfigurarFiltrosQA.aspx. Tiempo máximo: 0 sg</remarks>
    Public Sub EliminarConfiguracion()
        Authenticate()
        DBServer.FiltroQA_Eliminar(mlIDFiltroUsuario)
    End Sub

    ''' <summary>
    ''' Guarda la ordenación y su sentido del filtro configurado.
    ''' </summary>
    ''' <remarks>Llamada desde: NoConformidades/NoConformidades.aspx. Tiempo máximo: 0 sg.</remarks>
    Public Sub GuardarOrdenacion()
        Authenticate()
        Dim sOrden As String = ""
        If msOrdenacion <> "" Then
            sOrden = msOrdenacion & " " & msSentidoOrdenacion
        End If
        DBServer.FiltroQA_SaveOrdenacion(mlIDFiltroUsuario, sOrden)
    End Sub


    ''' <summary>
    ''' Obtiene la ordenacion de un filtro.
    ''' </summary>
    ''' <remarks>Llamada desde: NoConformidades/NoConformidades.aspx. Tiempo máximo: 0 sg.</remarks>
    Public Sub cargarOrdenacionFiltro()
        Authenticate()
        msOrdenacion = DBServer.FiltroQA_CargarOrdenacion(mlIDFiltroUsuario)
    End Sub



    ''' <summary>
    ''' Obtiene la configuración de los campos de búsqueda definidos por el usuario en ese filtro
    ''' </summary>
    ''' <param name="sIdi">Codigo idioma</param>
    ''' <remarks>Llamada desde: NoConformidad.aspx --> CargarBusquedaAvanzada // ConfigurarFiltrosQA --> CargarBusquedaAvanzada. Tiempo máximo: 1 sg.</remarks>
    Public Sub LoadConfiguracionCamposBusqueda(ByVal sIdi As String)
        Dim dr As SqlClient.SqlDataReader = Nothing
        Dim dtNewRow As DataRow
        Dim dt As DataTable = Nothing

        Authenticate()

        DBServer.FiltroQA_GetCamposBusqueda(mlIDFiltroUsuario, sIdi, dr)

        If mdtCamposConfiguracion Is Nothing Then
            mdtCamposConfiguracion = New DataSet
        End If

        If mdtCamposConfiguracion.Tables("CAMPOSBUSQUEDA") Is Nothing Then
            dt = mdtCamposConfiguracion.Tables.Add("CAMPOSBUSQUEDA")

            If miTipoSol = TiposDeDatos.TipoDeSolicitud.Certificado Then

                dt.Columns.Add("MATERIALES", System.Type.GetType("System.String"))
                dt.Columns.Add("TIPOSOL", System.Type.GetType("System.String"))
                dt.Columns.Add("BAJA", System.Type.GetType("System.Boolean"))

                dt.Columns.Add("QUEMOSTRAR", System.Type.GetType("System.Int16"))
                dt.Columns.Add("PROVEEDOR", System.Type.GetType("System.String"))
                dt.Columns.Add("HIDPROVEEDOR", System.Type.GetType("System.String"))
                dt.Columns.Add("PROVEEDORBAJA", System.Type.GetType("System.Boolean"))

                dt.Columns.Add("ESTADOS", System.Type.GetType("System.String"))

                dt.Columns.Add("PETICIONARIO", System.Type.GetType("System.String"))

                dt.Columns.Add("QUEFECHA", System.Type.GetType("System.Int16"))
                dt.Columns.Add("DESDE_FECHA", System.Type.GetType("System.DateTime"))
                dt.Columns.Add("HASTA_FECHA", System.Type.GetType("System.DateTime"))
            Else

                dt.Columns.Add("TIPO", System.Type.GetType("System.String"))
                dt.Columns.Add("SUBTIPO", System.Type.GetType("System.String"))
                dt.Columns.Add("TITULO", System.Type.GetType("System.String"))
                dt.Columns.Add("PROVEEDOR", System.Type.GetType("System.String"))
                dt.Columns.Add("PROVEEDORBAJA", System.Type.GetType("System.Boolean"))
                dt.Columns.Add("UNIDADNEGOCIO", System.Type.GetType("System.String"))
                dt.Columns.Add("GMN1", System.Type.GetType("System.String"))
                dt.Columns.Add("GMN2", System.Type.GetType("System.String"))
                dt.Columns.Add("GMN3", System.Type.GetType("System.String"))
                dt.Columns.Add("GMN4", System.Type.GetType("System.String"))
                dt.Columns.Add("MATDEN", System.Type.GetType("System.String"))
                dt.Columns.Add("ARTICULO", System.Type.GetType("System.String"))
                dt.Columns.Add("ARTDEN", System.Type.GetType("System.String"))
                dt.Columns.Add("DESDE_ALTA", System.Type.GetType("System.DateTime"))
                dt.Columns.Add("HASTA_ALTA", System.Type.GetType("System.DateTime"))
                dt.Columns.Add("DESDE_ACTUALIZACION", System.Type.GetType("System.DateTime"))
                dt.Columns.Add("HASTA_ACTUALIZACION", System.Type.GetType("System.DateTime"))
                dt.Columns.Add("DESDE_LIMITERESOLUCION", System.Type.GetType("System.DateTime"))
                dt.Columns.Add("HASTA_LIMITERESOLUCION", System.Type.GetType("System.DateTime"))
                dt.Columns.Add("DESDE_CIERRE", System.Type.GetType("System.DateTime"))
                dt.Columns.Add("HASTA_CIERRE", System.Type.GetType("System.DateTime"))
                dt.Columns.Add("ESTADONCABIERTAS", System.Type.GetType("System.String"))
                dt.Columns.Add("PETICIONARIO", System.Type.GetType("System.String"))
                dt.Columns.Add("REVISOR", System.Type.GetType("System.String"))
            End If

        End If

        If dr.Read() Then

            dtNewRow = dt.NewRow

            If miTipoSol = TiposDeDatos.TipoDeSolicitud.Certificado Then
                dtNewRow.Item("MATERIALES") = dr("MATERIALES")
                dtNewRow.Item("TIPOSOL") = dr("TIPOSOL")
                dtNewRow.Item("BAJA") = dr("BAJA")

                dtNewRow.Item("QUEMOSTRAR") = dr("QUEMOSTRAR")
                dtNewRow.Item("PROVEEDOR") = dr("PROVEEDOR")
                dtNewRow.Item("HIDPROVEEDOR") = dr("HIDPROVEEDOR")
                dtNewRow.Item("PROVEEDORBAJA") = dr("PROVEEDORBAJA")

                dtNewRow.Item("ESTADOS") = dr("ESTADOS")

                dtNewRow.Item("PETICIONARIO") = dr("PETICIONARIO")

                dtNewRow.Item("QUEFECHA") = dr("QUEFECHA")
                If Not IsDBNull(dr("DESDE_FECHA")) Then dtNewRow.Item("DESDE_FECHA") = dr("DESDE_FECHA")
                If Not IsDBNull(dr("HASTA_FECHA")) Then dtNewRow.Item("HASTA_FECHA") = dr("HASTA_FECHA")

            Else

                dtNewRow.Item("TIPO") = dr("TIPO")
                dtNewRow.Item("SUBTIPO") = dr("SUBTIPO")
                dtNewRow.Item("TITULO") = dr("TITULO")
                dtNewRow.Item("PROVEEDOR") = dr("PROVEEDOR")
                dtNewRow.Item("PROVEEDORBAJA") = dr("PROVEEDORBAJA")
                dtNewRow.Item("UNIDADNEGOCIO") = dr("UNQA")
                dtNewRow.Item("GMN1") = dr("GMN1")
                dtNewRow.Item("GMN2") = dr("GMN2")
                dtNewRow.Item("GMN3") = dr("GMN3")
                dtNewRow.Item("GMN4") = dr("GMN4")
                dtNewRow.Item("MATDEN") = dr("MATDEN")
                dtNewRow.Item("ARTICULO") = dr("ARTICULO")
                dtNewRow.Item("ARTDEN") = dr("ARTDEN")
                If Not IsDBNull(dr("DESDE_ALTA")) Then
                    dtNewRow.Item("DESDE_ALTA") = dr("DESDE_ALTA")
                End If
                If Not IsDBNull(dr("HASTA_ALTA")) Then
                    dtNewRow.Item("HASTA_ALTA") = dr("HASTA_ALTA")
                End If

                If Not IsDBNull(dr("DESDE_ACTUALIZACION")) Then
                    dtNewRow.Item("DESDE_ACTUALIZACION") = dr("DESDE_ACTUALIZACION")
                End If
                If Not IsDBNull(dr("HASTA_ACTUALIZACION")) Then
                    dtNewRow.Item("HASTA_ACTUALIZACION") = dr("HASTA_ACTUALIZACION")
                End If

                If Not IsDBNull(dr("DESDE_LIMITERESOLUCION")) Then
                    dtNewRow.Item("DESDE_LIMITERESOLUCION") = dr("DESDE_LIMITERESOLUCION")
                End If
                If Not IsDBNull(dr("HASTA_LIMITERESOLUCION")) Then
                    dtNewRow.Item("HASTA_LIMITERESOLUCION") = dr("HASTA_LIMITERESOLUCION")
                End If

                If Not IsDBNull(dr("DESDE_CIERRE")) Then
                    dtNewRow.Item("DESDE_CIERRE") = dr("DESDE_CIERRE")
                End If
                If Not IsDBNull(dr("HASTA_CIERRE")) Then
                    dtNewRow.Item("HASTA_CIERRE") = dr("HASTA_CIERRE")
                End If
                dtNewRow.Item("ESTADONCABIERTAS") = DBNullToStr(dr("ESTADO_NC_ABIERTAS"))
                dtNewRow.Item("PETICIONARIO") = DBNullToStr(dr("PETICIONARIO"))
                dtNewRow.Item("REVISOR") = DBNullToStr(dr("REVISOR"))
            End If

            dt.Rows.Add(dtNewRow)
        End If

        If Not dr Is Nothing AndAlso Not dr.IsClosed Then
            dr.Close()
        End If
    End Sub


    ''' <summary>
    ''' Devolver en un dataset con las NoConformidades y el nº de ellas en cada estado
    ''' </summary>
    ''' <param name="sUsu">Codigo usuario</param>
    ''' <param name="sIdioma">Codigo idioma</param>
    ''' <param name="iFilaEstado">Estado</param>
    ''' <param name="bQARestNoConformUsu">Restriccion NoConformidades por usuario</param>
    ''' <param name="bQARestNoConformUO">Restriccion NoConformidades por Unidad Organizativa</param>
    ''' <param name="bQARestNoConformDep">Restriccion NoConformidades por Departamento</param>
    ''' <param name="bQARestProvMaterial">Restriccion por material</param>
    ''' <param name="bQARestProvEquipo">Restriccion por equipo</param>
    ''' <param name="bQARestProvContacto">Restriccion por contacto</param>
    ''' <param name="lId">Identificador</param>
    ''' <param name="sTipo">Id Tipo solicitud</param>
    ''' <param name="sSubtipo">Cadena con los Ids Subtipo</param>
    ''' <param name="sTitulo">Titulo</param>
    ''' <param name="sProve">Codigo proveedor</param>
    ''' <param name="sPeticionario">Codigo peticionario (Codigo persona)</param>
    ''' <param name="sRevisor">Codigo revisor(Codigo persona)</param>
    ''' <param name="dFechaDesde">Fecha de inicio</param>
    ''' <param name="dFechaHasta">Fecha hasta</param>
    ''' <param name="bFechaAlta">Si se quiere filtrar por fecha de alta</param>
    ''' <param name="bFechaActualizacion">Si se quiere filtrar por fecha de actualizacion</param>
    ''' <param name="bFechaLimiteResolucion">Si se quiere filtrar por fecha limite de resolucion</param>
    ''' <param name="bFechaCierre">Si se quiere filtrar por fecha de cierre</param>
    ''' <param name="sGMN1">Codigo GMN1</param>
    ''' <param name="sGMN2">Codigo GMN2</param>
    ''' <param name="sGMN3">Codigo GMN3</param>
    ''' <param name="sGMN4">Codigo GMN4</param>
    ''' <param name="sCodArticulo">Codigo articulo</param>
    ''' <param name="sEstadoNoConformidadesAbiertas">Lista de los estados de las acciones de las noconformdidades</param>
    ''' <param name="sUNQA">Cadena con las Unidades de Negocio a filtrar</param>
    ''' <param name="sNombreTabla">Si se trata de un filtro configurado</param>
    ''' <param name="sOrden">Campos por el que se ordena</param>
    ''' <remarks>Llamada desde: Llamada desde NoConformidades.aspx.vb --> CargarInstanciasPorEstado; Tiempo máximo: 1,5(Depende el nº de noConformidades)</remarks>
    Public Sub Visor_DevolverNoConformidades(ByVal sUsu As String, ByVal sIdioma As String, ByVal iFilaEstado As Byte, ByVal bQARestNoConformUsu As Boolean, ByVal bQARestNoConformUO As Boolean,
                                             ByVal bQARestNoConformDep As Boolean, ByVal bQARestProvMaterial As Boolean, ByVal bQARestProvEquipo As Boolean, ByVal bQARestProvContacto As Boolean,
                                             Optional ByVal lId As Long = 0, Optional ByVal sTipo As String = "", Optional ByVal sSubtipo As String = "", Optional ByVal sTitulo As String = "",
                                             Optional ByVal sProve As String = "", Optional ByVal sPeticionario As String = "", Optional ByVal sRevisor As String = "", Optional ByVal dFechaDesde As Date = Nothing,
                                             Optional ByVal dFechaHasta As Date = Nothing, Optional ByVal bFechaAlta As Boolean = False, Optional ByVal bFechaActualizacion As Boolean = False,
                                             Optional ByVal bFechaLimiteResolucion As Boolean = False, Optional ByVal bFechaCierre As Boolean = False, Optional ByVal sGMN1 As String = "",
                                             Optional ByVal sGMN2 As String = "", Optional ByVal sGMN3 As String = "", Optional ByVal sGMN4 As String = "", Optional ByVal sCodArticulo As String = "",
                                             Optional ByVal sEstadoNoConformidadesAbiertas As String = "", Optional ByVal sUNQA As String = "", Optional ByVal sNombreTabla As String = "", Optional ByVal sOrden As String = "")
        Authenticate()

        Dim ds As DataSet
        ds = DBServer.NoConformidades_Load(sUsu, sIdioma, iFilaEstado, bQARestNoConformUsu, bQARestNoConformUO, bQARestNoConformDep, bQARestProvMaterial, bQARestProvEquipo, bQARestProvContacto, lId:=lId, sTipo:=sTipo, sSubtipo:=sSubtipo, sTitulo:=sTitulo, sProve:=sProve, sPeticionario:=sPeticionario, sRevisor:=sRevisor _
                                              , dFechaDesde:=dFechaDesde, dFechaHasta:=dFechaHasta, bFechaAlta:=bFechaAlta, bFechaActualizacion:=bFechaActualizacion, bFechaLimiteResolucion:=bFechaLimiteResolucion, bFechaCierre:=bFechaCierre _
                                              , sGMN1:=sGMN1, sGMN2:=sGMN2, sGMN3:=sGMN3, sGMN4:=sGMN4, sCodArticulo:=sCodArticulo, sEstadoNoConformidadesAbiertas:=sEstadoNoConformidadesAbiertas, sUNQA:=sUNQA, sNombreTabla:=sNombreTabla, sOrden:=sOrden)

        If ds.Tables(0).Rows.Count > 0 Then
            mlGuardadas = ds.Tables(0).Rows(0).Item("GUARDADAS")
            mlEnCursoDeAprobacion = ds.Tables(0).Rows(0).Item("ENCURSOAPROBACION")
            mlAbiertas = ds.Tables(0).Rows(0).Item("ABIERTAS")
            mlPendientesRevisarCierrre = ds.Tables(0).Rows(0).Item("PENDIENTES")
            mlCierreEficazDentroPlazo = ds.Tables(0).Rows(0).Item("CIERRE_POS_IN")
            mlCierreEficazFueraPlazo = ds.Tables(0).Rows(0).Item("CIERRE_POS_OUT")
            mlCierreNoEficaz = ds.Tables(0).Rows(0).Item("CIERRE_NEG")
            mlAnuladas = ds.Tables(0).Rows(0).Item("ANULADAS")
            mlTodas = ds.Tables(0).Rows(0).Item("TODAS")
        End If

        For Each oColumn As DataColumn In ds.Tables(1).Columns
            If UCase(oColumn.DataType.FullName) = "SYSTEM.DATETIME" Then
                For Each oRow As DataRow In ds.Tables(1).Rows
                    If Not IsDBNull(oRow.Item(oColumn.ColumnName)) Then
                        oRow.Item(oColumn.ColumnName) = Format(oRow.Item(oColumn.ColumnName), "d")
                    End If
                Next
            End If
        Next
        mdtNoConformidades = ds.Tables(1)
    End Sub

    ''' <summary>
    ''' Devuelve los peticionarios que tiene el filtro
    ''' </summary>
    ''' <returns>Dataset con los peticionarios</returns>
    ''' <remarks>Llamada desde=wucBusquedaAvanzadaNC.ascx; Tiempo máximo=0</remarks>
    Public Function CargarSubtiposSolicitud(ByVal lIDSolicitud As Long, ByVal sIdi As String) As DataSet
        Authenticate()
        Return DBServer.FiltroQA_CargarSubtiposSolicitud(lIDSolicitud, sIdi)
    End Function

    ''' <summary>
    ''' Devolver en un dataset con los Certificados 
    ''' </summary>
    ''' <param name="sUsu">Codigo usuario</param>
    ''' <param name="sIdioma">Codigo idioma</param>
    ''' <param name="Materiales">Material QA</param>
    ''' <param name="TipoSolicitudes">Tipo de Certificado</param>
    ''' <param name="VerDadosBaja">Ver tipos de certificados dados de baja</param>
    ''' <param name="MostrarProveedores">Sacar Todos los proves ó solo los de Panel de Calidad ó solo potenciales</param>
    ''' <param name="Proveedor">Si se saca uno en concreto, cod de proveedor</param>
    ''' <param name="ProveedorDeBaja">Si se sacan proveedores de baja logica o no</param>
    ''' <param name="Estados">Estados de certificados</param>
    ''' <param name="Peticionario">Codigo peticionario (Codigo persona)</param>
    ''' <param name="FechasDe">Si se quiere filtrar por fecha, indica por cual (expiración actualización,...)</param>
    ''' <param name="FechaDesde">Fecha desde</param>
    ''' <param name="FechaHasta">Fecha hasta</param>
    ''' <param name="sNombreTabla">Si se trata de un filtro configurado</param>
    ''' <param name="sOrden">Campos por el que se ordena</param>
    ''' <remarks>Llamada desde: Llamada desde Certificados.aspx.vb --> ObtenerDataTableCertificados; Tiempo máximo: 0,9(Depende del nº)</remarks>
    Public Sub Visor_DevolverCertificados(ByVal sUsu As String, ByVal Per As String, ByVal sIdioma As String,
        ByVal UON1 As String, ByVal UON2 As String, ByVal UON3 As String, ByVal Dep As String,
        ByVal RestricSolicUsu As Boolean, ByVal RestricSolicUO As Boolean, ByVal RestricSolicDep As Boolean,
        ByVal RestricProvMat As Boolean, ByVal RestricProvEqp As Boolean, ByVal RestricProvCon As Boolean,
        ByVal RestricUNQAUsu As Boolean, ByVal RestricUNQ As Boolean,
        Optional ByVal Materiales As String = "", Optional ByVal TipoSolicitudes As String = "",
        Optional ByVal VerDadosBaja As Byte = 0, Optional ByVal MostrarProveedores As String = "",
        Optional ByVal Proveedor As String = "", Optional ByVal ProveedorDeBaja As Boolean = False,
        Optional ByVal Estados As String = "", Optional ByVal Peticionario As String = "",
        Optional ByVal FechasDe As Byte = 0, Optional ByVal FechaDesde As Date = Nothing,
        Optional ByVal FechaHasta As Date = Nothing, Optional ByVal sNombreTabla As String = "",
        Optional ByVal sOrden As String = "", Optional ByVal PageNumberBD As Integer = 1,
        Optional ByVal PageSizeBD As Integer = 1000, Optional ByVal ObtenerTodasRelaciones As Boolean = True,
        Optional ByVal ObtenerTodosCertificados As Boolean = False)

        Authenticate()

        Dim ds As DataSet

        ds = DBServer.Certificados_CargarCertif(sUsu, Per, sIdioma, UON1, UON2, UON3, Dep,
            RestricSolicUsu, RestricSolicUO, RestricSolicDep, RestricProvMat, RestricProvEqp, RestricProvCon,
            RestricUNQAUsu, RestricUNQ,
            Materiales, TipoSolicitudes, VerDadosBaja, MostrarProveedores,
            Proveedor, ProveedorDeBaja, Estados, Peticionario, FechasDe, FechaDesde, FechaHasta, sNombreTabla, sOrden,
            False, PageNumberBD, PageSizeBD, ObtenerTodasRelaciones, ObtenerTodosCertificados)

        For Each row As DataRow In ds.Tables(0).Rows
            Select Case row("ESTADO")
                Case 63
                    row("ESTADO") = 1
                Case 64
                    row("ESTADO") = 2
                Case 65
                    row("ESTADO") = 3
                Case 66
                    row("ESTADO") = 4
                Case 67
                    row("ESTADO") = 5
                Case 80
                    row("ESTADO") = 6
                Case Else
                    row("ESTADO") = 7
            End Select
        Next

        Dim AuxTableBool As DataTable = ds.Tables(0).DefaultView.ToTable
        For Each oColumn As DataColumn In ds.Tables(0).Columns
            If UCase(oColumn.DataType.FullName) = "SYSTEM.DATETIME" Then
                For Each oRow As DataRow In ds.Tables(0).Rows
                    If Not IsDBNull(oRow.Item(oColumn.ColumnName)) Then
                        oRow.Item(oColumn.ColumnName) = Format(oRow.Item(oColumn.ColumnName), "d")
                    End If
                Next
            End If
        Next

        Dim sColumna As String
        For Each oColumn As DataColumn In AuxTableBool.Columns
            If (UCase(oColumn.DataType.FullName) = "SYSTEM.BYTE" AndAlso (Not oColumn.ColumnName = "CERTIFICADO") AndAlso (Not oColumn.ColumnName = "MODO_SOLIC") AndAlso (Not oColumn.ColumnName = "REAL")) _
            OrElse (UCase(oColumn.ColumnName) = "PUBLICADA") Then
                If (Not oColumn.ColumnName = "OBLIGATORIO") Then
                    sColumna = "BOOL_" & oColumn.ColumnName
                    ds.Tables(0).Columns.Add(sColumna, GetType(System.Boolean))
                Else
                    sColumna = "OBLIGATORIO"
                End If

                For Each oRow As DataRow In ds.Tables(0).Rows
                    If Not IsDBNull(oRow.Item(oColumn.ColumnName)) Then
                        oRow.Item(sColumna) = IIf(oRow.Item(oColumn.ColumnName), True, False)
                    Else
                        If (Not Left(oColumn.ColumnName, 2) = "C_") Then
                            oRow.Item(sColumna) = False
                        Else
                            oRow.Item(sColumna) = System.DBNull.Value
                        End If
                    End If
                Next
            End If
            If UCase(oColumn.ColumnName) = "REAL" Then
                sColumna = "BOOL_" & oColumn.ColumnName
                ds.Tables(0).Columns.Add(sColumna, GetType(System.Boolean))

                For Each oRow As DataRow In ds.Tables(0).Rows
                    If Not IsDBNull(oRow.Item(oColumn.ColumnName)) Then
                        oRow.Item(sColumna) = IIf(oRow.Item(oColumn.ColumnName) = 2, True, False)
                    Else
                        oRow.Item(sColumna) = False
                    End If
                Next
            ElseIf UCase(oColumn.ColumnName) = "VALIDADO" Then
                sColumna = "BOOL_" & oColumn.ColumnName
                ds.Tables(0).Columns.Add(sColumna, GetType(System.Boolean))
                For Each oRow As DataRow In ds.Tables(0).Rows
                    If Not IsDBNull(oRow.Item(oColumn.ColumnName)) Then
                        If (oRow.Item(oColumn.ColumnName) = 0) Then
                            oRow.Item(sColumna) = False
                        ElseIf (oRow.Item(oColumn.ColumnName) = 1) Then
                            oRow.Item(sColumna) = True
                        ElseIf (oRow.Item(oColumn.ColumnName) = -1) Then
                            oRow.Item(sColumna) = DBNull.Value
                        Else
                            oRow.Item(sColumna) = DBNull.Value
                        End If
                        'oRow.Item(sColumna) = oRow.Item(oColumn.ColumnName) 'IIf(oRow.Item(oColumn.ColumnName), True, False)
                    Else
                        oRow.Item(sColumna) = DBNull.Value
                    End If
                Next
            End If
        Next

        mdtCertificados = ds

    End Sub

    ''' <summary>
    ''' Devolver los contadores de las pestañas
    ''' </summary>
    ''' <param name="sUsu">Codigo usuario</param>
    ''' <param name="sIdioma">Codigo idioma</param>
    ''' <param name="Materiales">Material QA</param>
    ''' <param name="TipoSolicitudes">Tipo de Certificado</param>
    ''' <param name="VerDadosBaja">Ver tipos de certificados dados de baja</param>
    ''' <param name="MostrarProveedores">Sacar Todos los proves ó solo los de Panel de Calidad ó solo potenciales</param>
    ''' <param name="Proveedor">Si se saca uno en concreto, cod de proveedor</param>
    ''' <param name="ProveedorDeBaja">Si se sacan proveedores de baja logica o no</param>
    ''' <param name="Estados">Estados de certificados</param>
    ''' <param name="Peticionario">Codigo peticionario (Codigo persona)</param>
    ''' <param name="FechasDe">Si se quiere filtrar por fecha, indica por cual (expiración actualización,...)</param>
    ''' <param name="FechaDesde">Fecha desde</param>
    ''' <param name="FechaHasta">Fecha hasta</param>
    ''' <param name="sNombreTabla">Si se trata de un filtro configurado</param> 
    ''' <returns>numero de certificados q cumplen</returns>
    ''' <remarks>Llamada desde: Llamada desde Certificados.aspx.vb --> ObtenerDataCountCertificados; Tiempo máximo: 0,9(Depende del nº)</remarks>
    Public Function Visor_DevolverNumCertificados(ByVal sUsu As String, ByVal Per As String, ByVal sIdioma As String,
        ByVal UON1 As String, ByVal UON2 As String, ByVal UON3 As String, ByVal Dep As String,
        ByVal RestricSolicUsu As Boolean, ByVal RestricSolicUO As Boolean, ByVal RestricSolicDep As Boolean,
        ByVal RestricProvMat As Boolean, ByVal RestricProvEqp As Boolean, ByVal RestricProvCon As Boolean,
        ByVal RestricUNQAUsu As Boolean, ByVal RestricUNQ As Boolean,
        Optional ByVal Materiales As String = "", Optional ByVal TipoSolicitudes As String = "",
        Optional ByVal VerDadosBaja As Byte = 0, Optional ByVal MostrarProveedores As String = "",
        Optional ByVal Proveedor As String = "", Optional ByVal ProveedorDeBaja As Boolean = False,
        Optional ByVal Estados As String = "", Optional ByVal Peticionario As String = "",
        Optional ByVal FechasDe As Byte = 0, Optional ByVal FechaDesde As Date = Nothing,
        Optional ByVal FechaHasta As Date = Nothing, Optional ByVal sNombreTabla As String = "") As Integer

        Authenticate()

        Dim ds As DataSet

        ds = DBServer.Certificados_CargarCertif(sUsu, Per, sIdioma, UON1, UON2, UON3, Dep,
            RestricSolicUsu, RestricSolicUO, RestricSolicDep, RestricProvMat, RestricProvEqp, RestricProvCon,
            RestricUNQAUsu, RestricUNQ,
            Materiales, TipoSolicitudes, VerDadosBaja, MostrarProveedores,
            Proveedor, ProveedorDeBaja, Estados, Peticionario, FechasDe, FechaDesde, FechaHasta, sNombreTabla, , True)

        Return ds.Tables(0).Rows(0)("NUM")

    End Function
End Class
