﻿Imports System.Threading

<Serializable()> _
Public Class Puntuaciones
    Inherits Security

    Private m_dsPuntCertificados As DataSet
    Private m_dsVariablesCalculo As DataSet
    Private m_oDt As DataTable
    Private b_Cancelar_calculo_puntuacion_proveedor As Boolean
    Private i_QAPeriodoValidezCertificados As Nullable(Of Integer)

#Region "Constructor"
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub

#End Region

    ''' <summary>
    ''' Proceso que ejecuta la el calculo de las puntuaciones.
    ''' Metodo propio para la llamada del webService
    ''' </summary>        
    ''' <remarks>Llamada desde=Cada vez que se llame al webService --> QAPuntuaciones.vb; Tiempo máximo=0,2seg.</remarks>
    Public Sub CalcularPuntuaciones()
        Dim HayError As Boolean = False

        Authenticate()

        Try
            Dim oProveedores As New Proveedores(DBServer, mIsAuthenticated)
            oProveedores.EliminarPosiblesErroresPuntuacion()
            Dim oParametros As New FSNServer.Parametros(DBServer, mIsAuthenticated)
            Dim Pymes As DataSet = oParametros.ObtenerTipoInstalacion
            If Pymes.Tables.Count = 0 Then
                CalcularPuntuaciones(HayError)
                If HayError Then
                    oProveedores.NotificacionesErroresCalculoPuntuaciones()
                End If
            Else
                For i As Integer = 0 To Pymes.Tables(0).Rows.Count - 1
                    CalcularPuntuaciones(HayError, Pymes.Tables(0).Rows(i)("ID"))
                    If HayError Then
                        oProveedores.NotificacionesErroresCalculoPuntuaciones(Pymes.Tables(0).Rows(i)("ID"))
                    End If
                Next
            End If

        Catch ex As Exception
            DBServer.Errores_Create("Puntuaciones.vb", DBServer.UsuarioConectado, ex.GetType().FullName, ex.Message, ex.StackTrace, "", "")
        End Try
    End Sub
    ''' <summary>
    ''' Proceso que ejecuta la el calculo de la puntuacion del proveedor indicado.
    ''' Metodo propio para la llamada del webService
    ''' </summary>        
    ''' <remarks>Llamada desde = Integracion; Tiempo máximo=0,2seg.</remarks>
    Public Sub CalcularPuntuacionProveedor(ByVal sCodProve As String)
        Dim HayError As Boolean = False

        Authenticate()

        Try
            Dim oProveedores As New Proveedores(DBServer, mIsAuthenticated)
            oProveedores.EliminarPosiblesErroresPuntuacion(CodProve:=sCodProve)

            CalcularPuntuaciones(HayError, CodProve:=sCodProve)
            If HayError Then
                oProveedores.NotificacionesErroresCalculoPuntuaciones()
            End If
        Catch ex As Exception
            DBServer.Errores_Create("Puntuaciones.vb", DBServer.UsuarioConectado, ex.GetType().FullName, ex.Message, ex.StackTrace, "", "")
        End Try
    End Sub
    ''' <summary>
    ''' Proceso que lleva a cabo el cálculo de las puntuaciones.
    ''' Recorre todas las variables de calidad y segun su ponderacion obtiene su puntuacion y va subiendo calculando la formula de los padres.
    ''' Las puntuaciones las almacena en un dataset a nivel de clase para almacenarlas por cada proveedor.
    ''' </summary>
    ''' <remarks>Llamada desde=QA_Puntuaciones.asmx; Tiempo máximo</remarks>
    Private Sub CalcularPuntuaciones(ByRef HayError As Boolean, Optional ByVal IdPyme As Long = 0, Optional ByVal CodProve As String = Nothing)
        Dim lIdUNQA As Long
        Dim lIdUNQA1 As Long
        Dim lIdUNQA2 As Long
        Dim lNivel As Long
        Dim ds As DataSet
        Dim j As Integer
        Dim sAccion As String = ""
        Dim bEsHoja As Boolean

        Dim oProves As New FSNServer.Proveedores(DBServer, mIsAuthenticated)
        Dim oParametros As New FSNServer.Parametros(DBServer, mIsAuthenticated)

        oParametros.LoadData()
        oProves.cargarProvCalidad(IdPyme, CodProve)
        ds = oProves.Data
        'Calcula las puntuaciones para los proveedores que tienen unidades de negocio relacionado
        For Each drProve As DataRow In ds.Tables("PROVES").Rows
            Try
                Dim oProve As New FSNServer.Proveedor(DBServer, mIsAuthenticated)
                oProve.Cod = drProve("PROVE")
                oProve.CargarVariablesCalculo(IdPyme)

                'Para Calcular una unica vez la puntuacion de los distintos variables de tipo Certificado
                m_dsPuntCertificados = New DataSet
                m_oDt = m_dsPuntCertificados.Tables.Add("PUNTUACIONES_CERTIFICADOS")
                m_oDt.Columns.Add("ID", System.Type.GetType("System.Int32"))
                m_oDt.Columns.Add("NIVEL", System.Type.GetType("System.Int32"))
                m_oDt.Columns.Add("PUNT", System.Type.GetType("System.Double"))
                m_oDt.Columns.Add("CAL", System.Type.GetType("System.Int32"))
                m_oDt.Columns.Add("CAL_DEN", System.Type.GetType("System.String"))
                m_oDt.Columns.Add("VARIABLESXI", System.Type.GetType("System.String"))
                Dim keys1(1) As DataColumn
                keys1(0) = m_oDt.Columns("ID")
                keys1(1) = m_oDt.Columns("NIVEL")
                m_oDt.PrimaryKey = keys1
                m_dsVariablesCalculo = Nothing
                sAccion = "Calcular puntuacion"
                b_Cancelar_calculo_puntuacion_proveedor = False

                Dim dvUNQAs As New DataView(ds.Tables(0), "PROVE='" & drProve("PROVE") & "'", "", DataViewRowState.CurrentRows)
                For Each dvUNQA As DataRowView In dvUNQAs
                    lIdUNQA = dvUNQA("ID")
                    lNivel = dvUNQA("NIVEL")
                    lIdUNQA1 = If(dvUNQA.Row.IsNull("UNQA1"), 0, dvUNQA("UNQA1"))
                    lIdUNQA2 = If(dvUNQA.Row.IsNull("UNQA2"), 0, dvUNQA("UNQA2"))
                    bEsHoja = CType(dvUNQA("ES_HOJA"), Boolean)

                    RecalcularVariables(oProve.Cod, oProve.Variables, lIdUNQA, lNivel, lIdUNQA1, lIdUNQA2, bEsHoja)
                    If b_Cancelar_calculo_puntuacion_proveedor Then
                        HayError = True
                        Exit For
                    End If
                Next
                sAccion = "Guardar puntuacion"
                If Not m_dsVariablesCalculo Is Nothing AndAlso Not b_Cancelar_calculo_puntuacion_proveedor Then
                    oProve.Variables = m_dsVariablesCalculo
                    Dim bHayCambio As Boolean = oProve.GuardarVariables()

                    If oParametros.NotifProveCambioCalif AndAlso (bHayCambio Or oParametros.TipoNotifProveCambioCalif = FSNLibrary.TipoNotifCambioCalif.EnCadaCambio) Then
                        Try
                            Dim WaitCallback As New System.Threading.WaitCallback(AddressOf NotificacionCambioCalificacionTotal)
                            ThreadPool.QueueUserWorkItem(WaitCallback, oProve)

                        Catch ex As Exception
                            DBServer.Errores_Create("Puntuaciones.vb", DBServer.UsuarioConectado, ex.GetType().FullName, ex.Message, ex.StackTrace, "", "")
                        End Try
                    End If
                End If
            Catch ex As Exception
                DBServer.Errores_Create("Puntuaciones.vb", DBServer.UsuarioConectado, ex.GetType().FullName, ex.Message, ex.StackTrace, "", "")
            End Try
        Next
        oProves = Nothing
    End Sub
    ''' <summary>
    ''' Proceso que lleva a cabo el cálculo de las puntuaciones.
    ''' Recorre todas las variables de calidad y segun su ponderacion obtiene su puntuacion y va subiendo calculando la formula de los padres.
    ''' Las puntuaciones las almacena en un dataset a nivel de clase para almacenarlas por cada proveedor.
    ''' </summary>
    ''' <param name="sProve">Código de proveedor con el que se esta calculando</param>
    ''' <param name=" dsVariables">Dataset con las variables de calidad</param>        
    ''' <param name=" lIdUnqa">ID de la Unidad de Negocio</param>        
    ''' <param name=" lNivelUnqa">Nivel de la Unidad de Negocio</param>
    ''' <param name="lIdUNQA1">Id de la UNQA1</param>
    ''' <param name="lIdUNQA2">Id de la UNQA2</param>
    ''' <param name="bEsUNQAHoja">Indica si la UNQA no tiene UNQAs hijas</param>
    ''' <remarks>Llamada desde=QA_Puntuaciones.asmx; Tiempo máximo</remarks>
    Public Sub RecalcularVariables(ByVal sProve As String, ByVal dsVariables As DataSet, ByVal lIdUnqa As Long, ByVal lNivelUnqa As Long, ByVal lIdUNQA1 As Long, ByVal lIdUNQA2 As Long, ByVal bEsUNQAHoja As Boolean)
        Dim sVariables0() As String = Nothing
        Dim sVariables1() As String
        Dim sVariables2() As String
        Dim sVariables3() As String
        Dim sVariables4() As String
        Dim dValues0() As Double = Nothing
        Dim dValues1() As Double
        Dim dValues2() As Double
        Dim dValues3() As Double
        Dim dValues4() As Double
        Dim Res() As Object
        Dim ind0, ind1, ind2, ind3, ind4 As Short
        Dim buenas1, buenas2, buenas3, buenas4, buenas5 As Short 'Va a contener cuantas variables de ese nivel tiene con valor
        'Para las compuestas. Si alguna de las q se compone cumple la relación Proveedor/Material QA
        Dim ProveMatNiv2, ProveMatNiv3, ProveMatNiv4, ProveMatNiv5 As Boolean
        Dim bAplica As Boolean

        Try
            'Dim oDS As DataSet
            Dim oDT0, oDT1, oDT2, oDT3, oDT4, oDT5 As DataTable
            Dim dNewRow0, dNewRow1, dNewRow2, dNewRow3, dNewRow4, dNewRow5 As DataRow
            Dim oOrigRow1, oOrigRow2, oOrigRow3, oOrigRow4, oOrigRow5 As DataRow

            If m_dsVariablesCalculo Is Nothing Then
                m_dsVariablesCalculo = New DataSet
                oDT0 = NuevaTablaNivel0()
                'VARIABLES 1
                oDT1 = NuevaTablaNivel1()
                'VARIABLES 2
                oDT2 = NuevaTablaNivel2()
                'VARIABLES 3
                oDT3 = NuevaTablaNivel3()
                'VARIABLES 4
                oDT4 = NuevaTablaNivel4()
                'VARIABLES 5
                oDT5 = NuevaTablaNivel5()
            Else
                oDT0 = m_dsVariablesCalculo.Tables.Item("TEMP0")
                'VARIABLES 1
                oDT1 = m_dsVariablesCalculo.Tables.Item("TEMP1")
                'VARIABLES 2
                oDT2 = m_dsVariablesCalculo.Tables.Item("TEMP2")
                'VARIABLES 3
                oDT3 = m_dsVariablesCalculo.Tables.Item("TEMP3")
                'VARIABLES 4
                oDT4 = m_dsVariablesCalculo.Tables.Item("TEMP4")
                'VARIABLES 5
                oDT5 = m_dsVariablesCalculo.Tables.Item("TEMP5")
            End If

            Dim keys1(1) As DataColumn
            keys1(0) = oDT1.Columns("ID")
            keys1(1) = oDT1.Columns("UNQA")
            oDT1.PrimaryKey = keys1

            Dim keys2(1) As DataColumn
            keys2(0) = oDT2.Columns("ID")
            keys2(1) = oDT2.Columns("UNQA")
            oDT2.PrimaryKey = keys2

            Dim keys3(1) As DataColumn
            keys3(0) = oDT3.Columns("ID")
            keys3(1) = oDT3.Columns("UNQA")
            oDT3.PrimaryKey = keys3

            Dim keys4(1) As DataColumn
            keys4(0) = oDT4.Columns("ID")
            keys4(1) = oDT4.Columns("UNQA")
            oDT4.PrimaryKey = keys4

            Dim keys5(1) As DataColumn
            keys5(0) = oDT5.Columns("ID")
            keys5(1) = oDT5.Columns("UNQA")
            oDT5.PrimaryKey = keys5

            buenas1 = 0
            'Recorremos todas las variables recalculando
            'Nivel 1
            For Each oOrigRow1 In dsVariables.Tables(1).Rows
                dNewRow1 = oDT1.NewRow
                dNewRow1.Item("ID") = oOrigRow1.Item("ID")
                dNewRow1.Item("COD") = oOrigRow1.Item("COD")
                dNewRow1.Item("FORMULA") = oOrigRow1.Item("FORMULA")
                dNewRow1.Item("ID_FORMULA") = oOrigRow1.Item("ID_FORMULA")
                dNewRow1.Item("VALOR_DEF") = oOrigRow1.Item("VALOR_DEF")
                dNewRow1.Item("MAT") = oOrigRow1.Item("MAT")
                dNewRow1.Item("UNQA") = lIdUnqa
                dNewRow1.Item("UNQA1") = lIdUNQA1
                dNewRow1.Item("UNQA2") = lIdUNQA2
                dNewRow1.Item("UNQA_HOJA") = bEsUNQAHoja

                ReDim sVariables1(0)
                ReDim dValues1(0)
                ind1 = 0
                buenas2 = 0
                ProveMatNiv2 = False

                'Nivel 2
                For Each oOrigRow2 In oOrigRow1.GetChildRows("REL_VAR1_VAR2")
                    dNewRow2 = oDT2.NewRow
                    dNewRow2.Item("ID") = oOrigRow2.Item("ID")
                    dNewRow2.Item("ID_VAR_CAL1") = oOrigRow2.Item("ID_VAR_CAL1")
                    dNewRow2.Item("COD") = oOrigRow2.Item("COD")
                    dNewRow2.Item("TIPO") = oOrigRow2.Item("TIPO")
                    dNewRow2.Item("SUBTIPO") = oOrigRow2.Item("SUBTIPO")
                    dNewRow2.Item("FORMULA") = oOrigRow2.Item("FORMULA")
                    dNewRow2.Item("TIPOPOND") = oOrigRow2.Item("TIPOPOND")
                    dNewRow2.Item("ID_FORMULA") = oOrigRow2.Item("ID_FORMULA")
                    dNewRow2.Item("MATERIAL_QA") = oOrigRow2.Item("MATERIAL_QA")
                    dNewRow2.Item("VALOR_DEF") = oOrigRow2.Item("VALOR_DEF")
                    dNewRow2.Item("CERT_VALOR_SINCUMPL") = oOrigRow2.Item("CERT_VALOR_SINCUMPL")
                    dNewRow2.Item("MAT") = oOrigRow2.Item("MAT")
                    dNewRow2.Item("ORIGEN") = oOrigRow2.Item("ORIGEN")
                    dNewRow2.Item("NC_PERIODO") = oOrigRow2.Item("NC_PERIODO")
                    dNewRow2.Item("UNQA") = lIdUnqa
                    dNewRow2.Item("UNQA1") = lIdUNQA1
                    dNewRow2.Item("UNQA2") = lIdUNQA2
                    dNewRow2.Item("UNQA_HOJA") = bEsUNQAHoja

                    If oOrigRow2.Item("TIPO") = 0 Then 'Si es simple y se puede modificar                        
                        If DBNullToSomething(oOrigRow2.Item("MAT")) <> 1 Then
                            bAplica = False
                        Else
                            If oOrigRow2.Item("SUBTIPO") = CalidadSubtipo.CalCertificado Then
                                bAplica = (oOrigRow2.Item("TIENE_CERTIF") = 1)
                            Else
                                bAplica = True
                            End If
                        End If

                        b_Cancelar_calculo_puntuacion_proveedor = Not CalcularVariableSimple(sProve, lIdUnqa, lNivelUnqa, bEsUNQAHoja, 2, bAplica, oOrigRow2, dNewRow2, ProveMatNiv2)
                        If b_Cancelar_calculo_puntuacion_proveedor Then Exit Sub
                    Else 'Si es compuesta
                        ReDim sVariables2(0)
                        ReDim dValues2(0)
                        ind2 = 0
                        buenas3 = 0
                        ProveMatNiv3 = False

                        For Each oOrigRow3 In oOrigRow2.GetChildRows("REL_VAR2_VAR3")
                            dNewRow3 = oDT3.NewRow
                            dNewRow3.Item("ID") = oOrigRow3.Item("ID")
                            dNewRow3.Item("ID_VAR_CAL1") = oOrigRow3.Item("ID_VAR_CAL1")
                            dNewRow3.Item("ID_VAR_CAL2") = oOrigRow3.Item("ID_VAR_CAL2")
                            dNewRow3.Item("COD") = oOrigRow3.Item("COD")
                            dNewRow3.Item("TIPO") = oOrigRow3.Item("TIPO")
                            dNewRow3.Item("SUBTIPO") = oOrigRow3.Item("SUBTIPO")
                            dNewRow3.Item("FORMULA") = oOrigRow3.Item("FORMULA")
                            dNewRow3.Item("TIPOPOND") = oOrigRow3.Item("TIPOPOND")
                            dNewRow3.Item("ID_FORMULA") = oOrigRow3.Item("ID_FORMULA")
                            dNewRow3.Item("MATERIAL_QA") = oOrigRow3.Item("MATERIAL_QA")
                            dNewRow3.Item("VALOR_DEF") = oOrigRow3.Item("VALOR_DEF")
                            dNewRow3.Item("CERT_VALOR_SINCUMPL") = oOrigRow3.Item("CERT_VALOR_SINCUMPL")
                            dNewRow3.Item("MAT") = oOrigRow3.Item("MAT")
                            dNewRow3.Item("ORIGEN") = oOrigRow3.Item("ORIGEN")
                            dNewRow3.Item("NC_PERIODO") = oOrigRow3.Item("NC_PERIODO")
                            dNewRow3.Item("UNQA") = lIdUnqa
                            dNewRow3.Item("UNQA1") = lIdUNQA1
                            dNewRow3.Item("UNQA2") = lIdUNQA2
                            dNewRow3.Item("UNQA_HOJA") = bEsUNQAHoja

                            If oOrigRow3.Item("TIPO") = 0 Then 'Si es simple y se puede modificar
                                If DBNullToSomething(oOrigRow3.Item("MAT")) <> 1 Then
                                    bAplica = False
                                Else
                                    If oOrigRow3.Item("SUBTIPO") = CalidadSubtipo.CalCertificado Then
                                        bAplica = (oOrigRow3.Item("TIENE_CERTIF") = 1)
                                    Else
                                        bAplica = True
                                    End If
                                End If

                                b_Cancelar_calculo_puntuacion_proveedor = Not CalcularVariableSimple(sProve, lIdUnqa, lNivelUnqa, bEsUNQAHoja, 3, bAplica, oOrigRow3, dNewRow3, ProveMatNiv3)
                                If b_Cancelar_calculo_puntuacion_proveedor Then Exit Sub
                            Else 'Si es compuesta
                                ReDim sVariables3(0)
                                ReDim dValues3(0)
                                ind3 = 0
                                buenas4 = 0
                                ProveMatNiv4 = False

                                For Each oOrigRow4 In oOrigRow3.GetChildRows("REL_VAR3_VAR4")
                                    dNewRow4 = oDT4.NewRow
                                    dNewRow4.Item("ID") = oOrigRow4.Item("ID")
                                    dNewRow4.Item("ID_VAR_CAL1") = oOrigRow4.Item("ID_VAR_CAL1")
                                    dNewRow4.Item("ID_VAR_CAL2") = oOrigRow4.Item("ID_VAR_CAL2")
                                    dNewRow4.Item("ID_VAR_CAL3") = oOrigRow4.Item("ID_VAR_CAL3")
                                    dNewRow4.Item("COD") = oOrigRow4.Item("COD")
                                    dNewRow4.Item("TIPO") = oOrigRow4.Item("TIPO")
                                    dNewRow4.Item("SUBTIPO") = oOrigRow4.Item("SUBTIPO")
                                    dNewRow4.Item("FORMULA") = oOrigRow4.Item("FORMULA")
                                    dNewRow4.Item("TIPOPOND") = oOrigRow4.Item("TIPOPOND")
                                    dNewRow4.Item("ID_FORMULA") = oOrigRow4.Item("ID_FORMULA")
                                    dNewRow4.Item("MATERIAL_QA") = oOrigRow4.Item("MATERIAL_QA")
                                    dNewRow4.Item("VALOR_DEF") = oOrigRow4.Item("VALOR_DEF")
                                    dNewRow4.Item("CERT_VALOR_SINCUMPL") = oOrigRow4.Item("CERT_VALOR_SINCUMPL")
                                    dNewRow4.Item("MAT") = oOrigRow4.Item("MAT")
                                    dNewRow4.Item("ORIGEN") = oOrigRow4.Item("ORIGEN")
                                    dNewRow4.Item("NC_PERIODO") = oOrigRow4.Item("NC_PERIODO")
                                    dNewRow4.Item("UNQA") = lIdUnqa
                                    dNewRow4.Item("UNQA1") = lIdUNQA1
                                    dNewRow4.Item("UNQA2") = lIdUNQA2
                                    dNewRow4.Item("UNQA_HOJA") = bEsUNQAHoja

                                    If oOrigRow4.Item("TIPO") = 0 Then 'Si es simple y se puede modificar
                                        If DBNullToSomething(oOrigRow4.Item("MAT")) <> 1 Then
                                            bAplica = False
                                        Else
                                            If oOrigRow4.Item("SUBTIPO") = CalidadSubtipo.CalCertificado Then
                                                bAplica = (oOrigRow4.Item("TIENE_CERTIF") = 1)
                                            Else
                                                bAplica = True
                                            End If
                                        End If

                                        b_Cancelar_calculo_puntuacion_proveedor = Not CalcularVariableSimple(sProve, lIdUnqa, lNivelUnqa, bEsUNQAHoja, 4, bAplica, oOrigRow4, dNewRow4, ProveMatNiv4)
                                        If b_Cancelar_calculo_puntuacion_proveedor Then Exit Sub
                                    Else 'Si es compuesta
                                        ReDim sVariables4(0)
                                        ReDim dValues4(0)
                                        ind4 = 0
                                        buenas5 = 0
                                        ProveMatNiv5 = False

                                        For Each oOrigRow5 In oOrigRow4.GetChildRows("REL_VAR4_VAR5")
                                            dNewRow5 = oDT5.NewRow
                                            dNewRow5.Item("ID") = oOrigRow5.Item("ID")
                                            dNewRow5.Item("ID_VAR_CAL1") = oOrigRow5.Item("ID_VAR_CAL1")
                                            dNewRow5.Item("ID_VAR_CAL2") = oOrigRow5.Item("ID_VAR_CAL2")
                                            dNewRow5.Item("ID_VAR_CAL3") = oOrigRow5.Item("ID_VAR_CAL3")
                                            dNewRow5.Item("ID_VAR_CAL4") = oOrigRow5.Item("ID_VAR_CAL4")
                                            dNewRow5.Item("COD") = oOrigRow5.Item("COD")
                                            dNewRow5.Item("SUBTIPO") = oOrigRow5.Item("SUBTIPO")
                                            dNewRow5.Item("FORMULA") = oOrigRow5.Item("FORMULA")
                                            dNewRow5.Item("TIPOPOND") = oOrigRow5.Item("TIPOPOND")
                                            dNewRow5.Item("ID_FORMULA") = oOrigRow5.Item("ID_FORMULA")
                                            dNewRow5.Item("MATERIAL_QA") = oOrigRow5.Item("MATERIAL_QA")
                                            dNewRow5.Item("VALOR_DEF") = oOrigRow5.Item("VALOR_DEF")
                                            dNewRow5.Item("CERT_VALOR_SINCUMPL") = oOrigRow5.Item("CERT_VALOR_SINCUMPL")
                                            dNewRow5.Item("MAT") = oOrigRow5.Item("MAT")
                                            dNewRow5.Item("ORIGEN") = oOrigRow5.Item("ORIGEN")
                                            dNewRow5.Item("NC_PERIODO") = oOrigRow5.Item("NC_PERIODO")
                                            dNewRow5.Item("UNQA") = lIdUnqa
                                            dNewRow5.Item("UNQA1") = lIdUNQA1
                                            dNewRow5.Item("UNQA2") = lIdUNQA2
                                            dNewRow5.Item("UNQA_HOJA") = bEsUNQAHoja

                                            If DBNullToSomething(oOrigRow5.Item("MAT")) <> 1 Then
                                                bAplica = False
                                            Else
                                                If oOrigRow5.Item("SUBTIPO") = CalidadSubtipo.CalCertificado Then
                                                    bAplica = (oOrigRow5.Item("TIENE_CERTIF") = 1)
                                                Else
                                                    bAplica = True
                                                End If
                                            End If

                                            b_Cancelar_calculo_puntuacion_proveedor = Not CalcularVariableSimple(sProve, lIdUnqa, lNivelUnqa, bEsUNQAHoja, 5, bAplica, oOrigRow5, dNewRow5, ProveMatNiv5)
                                            If b_Cancelar_calculo_puntuacion_proveedor Then Exit Sub

                                            If Not IsDBNull(dNewRow5.Item("PUNT")) And Not IsNothing(dNewRow5.Item("PUNT")) Then
                                                buenas5 = buenas5 + 1
                                            End If
                                            oDT5.Rows.Add(dNewRow5)

                                            ReDim Preserve sVariables4(ind4)
                                            sVariables4(ind4) = oOrigRow5.Item("COD")
                                            ReDim Preserve dValues4(ind4)
                                            dValues4(ind4) = DBNullToDbl(dNewRow5.Item("PUNT"))
                                            ind4 += 1
                                        Next '5
                                        ProveMatNiv4 = ProveMatNiv4 OrElse ProveMatNiv5

                                        b_Cancelar_calculo_puntuacion_proveedor = Not CalcularVariableCompuesta(sProve, lIdUnqa, lNivelUnqa, bEsUNQAHoja, oOrigRow4, 4, sVariables4, dValues4, ProveMatNiv5, buenas5, dNewRow4)
                                        If b_Cancelar_calculo_puntuacion_proveedor Then Exit Sub
                                    End If
                                    If Not IsDBNull(dNewRow4.Item("PUNT")) And Not IsNothing(dNewRow4.Item("PUNT")) Then
                                        buenas4 = buenas4 + 1
                                    End If
                                    oDT4.Rows.Add(dNewRow4)

                                    ReDim Preserve sVariables3(ind3)
                                    sVariables3(ind3) = oOrigRow4.Item("COD")
                                    ReDim Preserve dValues3(ind3)
                                    dValues3(ind3) = DBNullToDbl(dNewRow4.Item("PUNT"))
                                    ind3 += 1
                                Next '4
                                ProveMatNiv3 = ProveMatNiv3 OrElse ProveMatNiv4

                                b_Cancelar_calculo_puntuacion_proveedor = Not CalcularVariableCompuesta(sProve, lIdUnqa, lNivelUnqa, bEsUNQAHoja, oOrigRow3, 3, sVariables3, dValues3, ProveMatNiv4, buenas4, dNewRow3)
                                If b_Cancelar_calculo_puntuacion_proveedor Then Exit Sub
                            End If
                            If Not IsDBNull(dNewRow3.Item("PUNT")) And Not IsNothing(dNewRow3.Item("PUNT")) Then
                                buenas3 = buenas3 + 1
                            End If
                            oDT3.Rows.Add(dNewRow3)

                            ReDim Preserve sVariables2(ind2)
                            sVariables2(ind2) = oOrigRow3.Item("COD")
                            ReDim Preserve dValues2(ind2)
                            dValues2(ind2) = DBNullToDbl(dNewRow3.Item("PUNT"))
                            ind2 += 1
                        Next '3
                        ProveMatNiv2 = ProveMatNiv2 OrElse ProveMatNiv3

                        b_Cancelar_calculo_puntuacion_proveedor = Not CalcularVariableCompuesta(sProve, lIdUnqa, lNivelUnqa, bEsUNQAHoja, oOrigRow2, 2, sVariables2, dValues2, ProveMatNiv3, buenas3, dNewRow2)
                        If b_Cancelar_calculo_puntuacion_proveedor Then Exit Sub
                    End If
                    If Not IsDBNull(dNewRow2.Item("PUNT")) And Not IsNothing(dNewRow2.Item("PUNT")) Then
                        buenas2 = buenas2 + 1
                    End If
                    oDT2.Rows.Add(dNewRow2)

                    ReDim Preserve sVariables1(ind1)
                    sVariables1(ind1) = oOrigRow2.Item("COD")
                    ReDim Preserve dValues1(ind1)
                    dValues1(ind1) = DBNullToDbl(dNewRow2.Item("PUNT"))
                    ind1 += 1
                Next '2

                b_Cancelar_calculo_puntuacion_proveedor = Not CalcularVariableCompuesta(sProve, lIdUnqa, lNivelUnqa, bEsUNQAHoja, oOrigRow1, 1, sVariables1, dValues1, ProveMatNiv2, buenas2, dNewRow1)
                If b_Cancelar_calculo_puntuacion_proveedor Then Exit Sub

                If Not IsDBNull(dNewRow1.Item("PUNT")) And Not IsNothing(dNewRow1.Item("PUNT")) Then
                    buenas1 = buenas1 + 1
                End If
                oDT1.Rows.Add(dNewRow1)

                ReDim Preserve sVariables0(ind0)
                sVariables0(ind0) = oOrigRow1.Item("COD")
                ReDim Preserve dValues0(ind0)
                dValues0(ind0) = DBNullToDbl(dNewRow1.Item("PUNT"))
                ind0 += 1
            Next '1
            Dim sFormulaProve As String
            dNewRow0 = oDT0.NewRow
            dNewRow0.Item("ID") = dsVariables.Tables(0).Rows(0).Item("ID")
            dNewRow0.Item("FORMULA") = dsVariables.Tables(0).Rows(0).Item("FORMULA")
            dNewRow0.Item("ID_FORMULA") = dsVariables.Tables(0).Rows(0).Item("ID_FORMULA")
            dNewRow0.Item("UNQA") = lIdUnqa

            sFormulaProve = dsVariables.Tables(0).Rows(0).Item("FORMULA")
            If buenas1 = 0 Then
                dNewRow0.Item("PUNT") = System.DBNull.Value
                dNewRow0.Item("APLICA") = 1
            ElseIf Not IsDBNull(sFormulaProve) Then
                dNewRow0.Item("APLICA") = 1
                b_Cancelar_calculo_puntuacion_proveedor = Not AplicarFormula(sProve, lIdUnqa, 0, dNewRow0, sVariables0, dValues0)
                If b_Cancelar_calculo_puntuacion_proveedor Then Exit Sub
            Else
                dNewRow0.Item("PUNT") = 0
                Res = ObtenerCalificacion(dNewRow0.Item("ID"), 0, dNewRow0.Item("PUNT"), "")
                dNewRow0.Item("CALID") = Res(0)
                dNewRow0.Item("CAL") = Res(1)
                dNewRow0.Item("APLICA") = 1
            End If

            oDT0.Rows.Add(dNewRow0)
        Catch ex As Exception
            DBServer.Errores_Create("Puntuaciones.vb", DBServer.UsuarioConectado, ex.GetType().FullName, ex.Message, ex.StackTrace, "", "")
        End Try
    End Sub
    Private Function NuevaTablaNivel0() As DataTable
        Dim oDT0 As DataTable = m_dsVariablesCalculo.Tables.Add("TEMP0")
        oDT0.Columns.Add("ID", System.Type.GetType("System.Int32"))
        oDT0.Columns.Add("FORMULA", System.Type.GetType("System.String"))
        oDT0.Columns.Add("ID_FORMULA", System.Type.GetType("System.Int32"))
        oDT0.Columns.Add("PUNT", System.Type.GetType("System.Double"))
        oDT0.Columns.Add("CALID", System.Type.GetType("System.Int32"))
        oDT0.Columns.Add("CAL", System.Type.GetType("System.String"))
        oDT0.Columns.Add("UNQA", System.Type.GetType("System.Int32"))
        oDT0.Columns.Add("APLICA", System.Type.GetType("System.Boolean"))
        Return oDT0
    End Function
    Private Function NuevaTablaNivel1() As DataTable
        Dim oDT1 As DataTable = m_dsVariablesCalculo.Tables.Add("TEMP1")
        oDT1.Columns.Add("ID", System.Type.GetType("System.Int32"))
        oDT1.Columns.Add("COD", System.Type.GetType("System.String"))
        oDT1.Columns.Add("FORMULA", System.Type.GetType("System.String"))
        oDT1.Columns.Add("ID_FORMULA", System.Type.GetType("System.Int32"))
        oDT1.Columns.Add("PUNT", System.Type.GetType("System.Double"))
        oDT1.Columns.Add("CALID", System.Type.GetType("System.Int32"))
        oDT1.Columns.Add("CAL", System.Type.GetType("System.String"))
        oDT1.Columns.Add("VALOR_DEF", System.Type.GetType("System.Double"))
        oDT1.Columns.Add("MAT", System.Type.GetType("System.Int16"))
        oDT1.Columns.Add("UNQA", System.Type.GetType("System.Int32"))
        oDT1.Columns.Add("UNQA1", System.Type.GetType("System.Int32"))
        oDT1.Columns.Add("UNQA2", System.Type.GetType("System.Int32"))
        oDT1.Columns.Add("UNQA_HOJA", System.Type.GetType("System.Boolean"))
        oDT1.Columns.Add("APLICA", System.Type.GetType("System.Boolean"))
        Return oDT1
    End Function
    Private Function NuevaTablaNivel2() As DataTable
        Dim oDT2 As DataTable = m_dsVariablesCalculo.Tables.Add("TEMP2")
        oDT2.Columns.Add("ID", System.Type.GetType("System.Int32"))
        oDT2.Columns.Add("ID_VAR_CAL1", System.Type.GetType("System.Int32"))
        oDT2.Columns.Add("COD", System.Type.GetType("System.String"))
        oDT2.Columns.Add("TIPO", System.Type.GetType("System.Int32"))
        oDT2.Columns.Add("SUBTIPO", System.Type.GetType("System.Int32"))
        oDT2.Columns.Add("FORMULA", System.Type.GetType("System.String"))
        oDT2.Columns.Add("TIPOPOND", System.Type.GetType("System.Int16"))
        oDT2.Columns.Add("ID_FORMULA", System.Type.GetType("System.Int32"))
        oDT2.Columns.Add("MATERIAL_QA", System.Type.GetType("System.Int16"))
        oDT2.Columns.Add("PUNT", System.Type.GetType("System.Double"))
        oDT2.Columns.Add("CALID", System.Type.GetType("System.Int32"))
        oDT2.Columns.Add("CAL", System.Type.GetType("System.String"))
        oDT2.Columns.Add("VALOR_DEF", System.Type.GetType("System.Double"))
        oDT2.Columns.Add("CERT_VALOR_SINCUMPL", System.Type.GetType("System.Double"))
        oDT2.Columns.Add("MAT", System.Type.GetType("System.Int16"))
        oDT2.Columns.Add("ORIGEN", System.Type.GetType("System.Int16"))
        oDT2.Columns.Add("NC_PERIODO", System.Type.GetType("System.Int16"))
        oDT2.Columns.Add("UNQA", System.Type.GetType("System.Int32"))
        oDT2.Columns.Add("UNQA1", System.Type.GetType("System.Int32"))
        oDT2.Columns.Add("UNQA2", System.Type.GetType("System.Int32"))
        oDT2.Columns.Add("UNQA_HOJA", System.Type.GetType("System.Boolean"))
        oDT2.Columns.Add("VARIABLESXI", System.Type.GetType("System.String"))
        oDT2.Columns.Add("APLICA", System.Type.GetType("System.Boolean"))
        Return oDT2
    End Function
    Private Function NuevaTablaNivel3() As DataTable
        Dim oDT3 As DataTable = m_dsVariablesCalculo.Tables.Add("TEMP3")
        oDT3.Columns.Add("ID", System.Type.GetType("System.Int32"))
        oDT3.Columns.Add("ID_VAR_CAL1", System.Type.GetType("System.Int32"))
        oDT3.Columns.Add("ID_VAR_CAL2", System.Type.GetType("System.Int32"))
        oDT3.Columns.Add("COD", System.Type.GetType("System.String"))
        oDT3.Columns.Add("TIPO", System.Type.GetType("System.Int32"))
        oDT3.Columns.Add("SUBTIPO", System.Type.GetType("System.Int32"))
        oDT3.Columns.Add("FORMULA", System.Type.GetType("System.String"))
        oDT3.Columns.Add("TIPOPOND", System.Type.GetType("System.Int16"))
        oDT3.Columns.Add("ID_FORMULA", System.Type.GetType("System.Int32"))
        oDT3.Columns.Add("MATERIAL_QA", System.Type.GetType("System.Int16"))
        oDT3.Columns.Add("PUNT", System.Type.GetType("System.Double"))
        oDT3.Columns.Add("CALID", System.Type.GetType("System.Int32"))
        oDT3.Columns.Add("CAL", System.Type.GetType("System.String"))
        oDT3.Columns.Add("VALOR_DEF", System.Type.GetType("System.Double"))
        oDT3.Columns.Add("CERT_VALOR_SINCUMPL", System.Type.GetType("System.Double"))
        oDT3.Columns.Add("MAT", System.Type.GetType("System.Int16"))
        oDT3.Columns.Add("ORIGEN", System.Type.GetType("System.Int16"))
        oDT3.Columns.Add("NC_PERIODO", System.Type.GetType("System.Int16"))
        oDT3.Columns.Add("UNQA", System.Type.GetType("System.Int32"))
        oDT3.Columns.Add("UNQA1", System.Type.GetType("System.Int32"))
        oDT3.Columns.Add("UNQA2", System.Type.GetType("System.Int32"))
        oDT3.Columns.Add("UNQA_HOJA", System.Type.GetType("System.Boolean"))
        oDT3.Columns.Add("VARIABLESXI", System.Type.GetType("System.String"))
        oDT3.Columns.Add("APLICA", System.Type.GetType("System.Boolean"))
        Return oDT3
    End Function
    Private Function NuevaTablaNivel4() As DataTable
        Dim oDT4 As DataTable = m_dsVariablesCalculo.Tables.Add("TEMP4")
        oDT4.Columns.Add("ID", System.Type.GetType("System.Int32"))
        oDT4.Columns.Add("ID_VAR_CAL1", System.Type.GetType("System.Int32"))
        oDT4.Columns.Add("ID_VAR_CAL2", System.Type.GetType("System.Int32"))
        oDT4.Columns.Add("ID_VAR_CAL3", System.Type.GetType("System.Int32"))
        oDT4.Columns.Add("COD", System.Type.GetType("System.String"))
        oDT4.Columns.Add("TIPO", System.Type.GetType("System.Int32"))
        oDT4.Columns.Add("SUBTIPO", System.Type.GetType("System.Int32"))
        oDT4.Columns.Add("FORMULA", System.Type.GetType("System.String"))
        oDT4.Columns.Add("TIPOPOND", System.Type.GetType("System.Int16"))
        oDT4.Columns.Add("ID_FORMULA", System.Type.GetType("System.Int32"))
        oDT4.Columns.Add("MATERIAL_QA", System.Type.GetType("System.Int16"))
        oDT4.Columns.Add("PUNT", System.Type.GetType("System.Double"))
        oDT4.Columns.Add("CALID", System.Type.GetType("System.Int32"))
        oDT4.Columns.Add("CAL", System.Type.GetType("System.String"))
        oDT4.Columns.Add("VALOR_DEF", System.Type.GetType("System.Double"))
        oDT4.Columns.Add("CERT_VALOR_SINCUMPL", System.Type.GetType("System.Double"))
        oDT4.Columns.Add("MAT", System.Type.GetType("System.Int16"))
        oDT4.Columns.Add("ORIGEN", System.Type.GetType("System.Int16"))
        oDT4.Columns.Add("NC_PERIODO", System.Type.GetType("System.Int16"))
        oDT4.Columns.Add("UNQA", System.Type.GetType("System.Int32"))
        oDT4.Columns.Add("UNQA1", System.Type.GetType("System.Int32"))
        oDT4.Columns.Add("UNQA2", System.Type.GetType("System.Int32"))
        oDT4.Columns.Add("VARIABLESXI", System.Type.GetType("System.String"))
        oDT4.Columns.Add("UNQA_HOJA", System.Type.GetType("System.Boolean"))
        oDT4.Columns.Add("APLICA", System.Type.GetType("System.Boolean"))
        Return oDT4
    End Function
    Private Function NuevaTablaNivel5() As DataTable
        Dim oDT5 As DataTable = m_dsVariablesCalculo.Tables.Add("TEMP5")
        oDT5.Columns.Add("ID", System.Type.GetType("System.Int32"))
        oDT5.Columns.Add("ID_VAR_CAL1", System.Type.GetType("System.Int32"))
        oDT5.Columns.Add("ID_VAR_CAL2", System.Type.GetType("System.Int32"))
        oDT5.Columns.Add("ID_VAR_CAL3", System.Type.GetType("System.Int32"))
        oDT5.Columns.Add("ID_VAR_CAL4", System.Type.GetType("System.Int32"))
        oDT5.Columns.Add("COD", System.Type.GetType("System.String"))
        oDT5.Columns.Add("SUBTIPO", System.Type.GetType("System.Int32"))
        oDT5.Columns.Add("FORMULA", System.Type.GetType("System.String"))
        oDT5.Columns.Add("TIPOPOND", System.Type.GetType("System.Int16"))
        oDT5.Columns.Add("ID_FORMULA", System.Type.GetType("System.Int32"))
        oDT5.Columns.Add("MATERIAL_QA", System.Type.GetType("System.Int16"))
        oDT5.Columns.Add("PUNT", System.Type.GetType("System.Double"))
        oDT5.Columns.Add("CALID", System.Type.GetType("System.Int32"))
        oDT5.Columns.Add("CAL", System.Type.GetType("System.String"))
        oDT5.Columns.Add("VALOR_DEF", System.Type.GetType("System.Double"))
        oDT5.Columns.Add("CERT_VALOR_SINCUMPL", System.Type.GetType("System.Double"))
        oDT5.Columns.Add("MAT", System.Type.GetType("System.Int16"))
        oDT5.Columns.Add("ORIGEN", System.Type.GetType("System.Int16"))
        oDT5.Columns.Add("NC_PERIODO", System.Type.GetType("System.Int16"))
        oDT5.Columns.Add("UNQA", System.Type.GetType("System.Int32"))
        oDT5.Columns.Add("UNQA1", System.Type.GetType("System.Int32"))
        oDT5.Columns.Add("UNQA2", System.Type.GetType("System.Int32"))
        oDT5.Columns.Add("UNQA_HOJA", System.Type.GetType("System.Boolean"))
        oDT5.Columns.Add("VARIABLESXI", System.Type.GetType("System.String"))
        oDT5.Columns.Add("APLICA", System.Type.GetType("System.Boolean"))
        Return oDT5
    End Function
    Private Function CalcularVariableSimple(ByVal sProve As String, ByVal lIdUnqa As Integer, ByVal lNivelUnqa As Long, ByVal bEsUNQAHoja As Boolean, ByVal iVarCalNivel As Integer, ByVal bAplica As Boolean,
                                            ByRef oOrigRow As DataRow, ByRef drVarCal As DataRow, ByRef ProveMatNivel As Boolean) As Boolean
        Dim bOk As Boolean = True
        Dim Res() As Object

        If Not bAplica Then
            drVarCal.Item("PUNT") = oOrigRow.Item("VALOR_DEF") 'System.DBNull.Value
            Res = ObtenerCalificacion(drVarCal.Item("ID"), 5, drVarCal.Item("PUNT"), "")
            drVarCal.Item("CALID") = Res(0) 'System.DBNull.Value
            drVarCal.Item("CAL") = Res(1) 'System.DBNull.Value
            'APLICA es para que se vea la var. en la ficha
            If oOrigRow.Item("SUBTIPO") = CalidadSubtipo.CalCertificado And oOrigRow.Item("APLICA_CERTIF") = 1 Then
                'Se pone APLICA a 1 porque sí le corresponde pero todavía no se le ha solicitado
                drVarCal.Item("APLICA") = 1
            Else
                drVarCal.Item("APLICA") = 0
            End If
        Else
            drVarCal.Item("APLICA") = 1
            ProveMatNivel = True

            If bEsUNQAHoja OrElse oOrigRow.Item("OPCION_CONF") = VarCalOpcionConf.EvaluarFormula OrElse drVarCal.Item("TIPOPOND") = FSNLibrary.TipoPonderacionVariables.PondCertificado Then
                Res = CalculoPonderacion(sProve, drVarCal.Item("ID"), iVarCalNivel, lIdUnqa, lNivelUnqa, drVarCal.Item("TIPOPOND"), DBNullToSomething(drVarCal.Item("ORIGEN")),
                 DBNullToSomething(drVarCal.Item("ID_FORMULA")), DBNullToSomething(drVarCal.Item("FORMULA")), drVarCal.Item("VALOR_DEF"), drVarCal.Item("CERT_VALOR_SINCUMPL"))
                If Res.Length = 5 Then
                    Dim iTipoError, iVariableError, iIdFormulaError As Integer
                    Dim sValoresError, sMensajeError As String

                    iIdFormulaError = Res(0)
                    iTipoError = Res(1)
                    iVariableError = Res(2)
                    sValoresError = Res(3)
                    sMensajeError = Res(4)

                    Dim oVarCal As New FSNServer.VariablesCalidad(DBServer, mIsAuthenticated)
                    oVarCal.GuardarError(sProve, lIdUnqa, drVarCal.Item("ID"), iVarCalNivel, iTipoError, iVariableError, sValoresError, iIdFormulaError, sMensajeError)

                    bOk = False
                Else
                    drVarCal.Item("PUNT") = Res(0)
                    drVarCal.Item("CALID") = Res(1)
                    drVarCal.Item("CAL") = Res(2)
                    drVarCal.Item("VARIABLESXI") = Res(3)
                End If
            Else
                'se mira la confirguración para saber cómo evaluar la variable
                Select Case oOrigRow.Item("OPCION_CONF")
                    Case VarCalOpcionConf.NoCalcular
                        drVarCal.Item("PUNT") = System.DBNull.Value
                        drVarCal.Item("CALID") = System.DBNull.Value
                        drVarCal.Item("CAL") = System.DBNull.Value
                        drVarCal.Item("APLICA") = 1
                    Case VarCalOpcionConf.MediaPonderadaSegunVarHermana
                        drVarCal.Item("ID_FORMULA") = System.DBNull.Value
                        bOk = AplicarMediaPonderada(sProve, lIdUnqa, lNivelUnqa, oOrigRow.Item("VAR_POND"), iVarCalNivel, drVarCal, oOrigRow.Item("VALOR_DEF"))
                End Select
            End If
        End If

        Return bOk
    End Function
    ''' <summary></summary>
    ''' <param name="sProve"></param>
    ''' <param name="lIdUnqa"></param>
    ''' <param name="lNivelUnqa"></param>
    ''' <param name="bEsUNQAHoja"></param>
    ''' <param name="oOrigRow"></param>
    ''' <param name="iVarCalNivel"></param>
    ''' <param name="sVariables"></param>
    ''' <param name="dValues"></param>
    ''' <param name="ProveMatNivInferior">Si alguna de las variables de las q se compone cumple la relación Proveedor/Material QA</param>
    ''' <param name="buenasNivInferior">Cuantas var. de nivel inferior tienen valor</param>    
    ''' <param name="drVarCal"></param>
    ''' <returns></returns>
    Private Function CalcularVariableCompuesta(ByVal sProve As String, ByVal lIdUnqa As Integer, ByVal lNivelUnqa As Long, ByVal bEsUNQAHoja As Boolean, ByRef oOrigRow As DataRow, ByVal iVarCalNivel As Integer, ByVal sVariables() As String,
                                               ByVal dValues() As Double, ByVal ProveMatNivInferior As Boolean, ByVal buenasNivInferior As Short, ByRef drVarCal As DataRow) As Boolean
        Dim bOk As Boolean = True

        If bEsUNQAHoja OrElse oOrigRow.Item("OPCION_CONF") = VarCalOpcionConf.EvaluarFormula Then
            'Aplicar la fórmula configurada
            If Not ProveMatNivInferior OrElse buenasNivInferior = 0 Then
                drVarCal.Item("PUNT") = oOrigRow.Item("VALOR_DEF") 'System.DBNull.Value
                Dim Res() As Object = ObtenerCalificacion(drVarCal.Item("ID"), iVarCalNivel, drVarCal.Item("PUNT"), "")
                drVarCal.Item("CALID") = Res(0) 'System.DBNull.Value
                drVarCal.Item("CAL") = Res(1) 'System.DBNull.Value
                drVarCal.Item("APLICA") = 0
            Else
                drVarCal.Item("APLICA") = 1
                If Not IsDBNull(drVarCal.Item("FORMULA")) Then
                    bOk = AplicarFormula(sProve, lIdUnqa, iVarCalNivel, drVarCal, sVariables, dValues)
                Else
                    drVarCal.Item("PUNT") = 0
                End If
                If buenasNivInferior = 0 Then
                    drVarCal.Item("PUNT") = System.DBNull.Value
                    drVarCal.Item("CALID") = System.DBNull.Value
                    drVarCal.Item("CAL") = System.DBNull.Value
                End If
            End If
        Else
            'se mira la confirguración para saber cómo evaluar la variable
            Select Case oOrigRow.Item("OPCION_CONF")
                Case VarCalOpcionConf.NoCalcular
                    drVarCal.Item("PUNT") = System.DBNull.Value
                    drVarCal.Item("CALID") = System.DBNull.Value
                    drVarCal.Item("CAL") = System.DBNull.Value
                    drVarCal.Item("APLICA") = If(Not ProveMatNivInferior OrElse buenasNivInferior = 0, 0, 1)
                Case VarCalOpcionConf.MediaPonderadaSegunVarHermana
                    drVarCal.Item("APLICA") = If(Not ProveMatNivInferior, 0, 1)
                    drVarCal.Item("ID_FORMULA") = System.DBNull.Value
                    bOk = AplicarMediaPonderada(sProve, lIdUnqa, lNivelUnqa, oOrigRow.Item("VAR_POND"), iVarCalNivel, drVarCal, oOrigRow.Item("VALOR_DEF"))
            End Select
        End If

        Return bOk
    End Function
    ''' <summary>Aplica la fórmula de una variable para obtener su puntuación</summary>
    ''' <param name="sProve"></param>
    ''' <param name="lIdUnqa"></param>
    ''' <param name="drVarCal"></param>
    ''' <param name="sVariables"></param>
    ''' <param name="dValues"></param>
    ''' <returns></returns>
    Private Function AplicarFormula(ByVal sProve As String, ByVal lIdUnqa As Integer, ByVal iNivelVarCal As Short, ByRef drVarCal As DataRow, ByVal sVariables() As String, ByVal dValues() As Double) As Boolean
        Dim iEq As New USPExpress.USPExpression
        Dim bOk As Boolean = True
        Dim sMensajeError As String

        Try
            iEq.Parse(drVarCal.Item("FORMULA"), sVariables)
            drVarCal.Item("PUNT") = iEq.Evaluate(dValues)

            Dim Res() As Object
            Res = ObtenerCalificacion(drVarCal.Item("ID"), iNivelVarCal, drVarCal.Item("PUNT"), "")
            drVarCal.Item("CALID") = Res(0)
            drVarCal.Item("CAL") = Res(1)
        Catch ex As USPExpress.ParseException
            bOk = False
            sMensajeError = ex.Message
        Catch ex0 As Exception
            bOk = False
            sMensajeError = ex0.Message
        Finally
            If Not bOk Then
                Dim iTipoError, iVariableError, iIdFormulaError As Integer
                Dim sValoresError As String

                iIdFormulaError = drVarCal.Item("ID_FORMULA")
                iTipoError = 0
                iVariableError = 0
                sValoresError = ""

                Dim oVarCal As New FSNServer.VariablesCalidad(DBServer, mIsAuthenticated)
                oVarCal.GuardarError(sProve, lIdUnqa, drVarCal.Item("ID"), iNivelVarCal, iTipoError, iVariableError, sValoresError, iIdFormulaError, sMensajeError)
                oVarCal = Nothing
            End If
        End Try

        Return bOk
    End Function
    ''' <summary>Calcula la puntuación de la variable aplicando la media ponderada con respecto a una variable dada</summary>
    ''' <param name="sProve"></param>
    ''' <param name="lIdUnqa"></param>
    ''' <param name="lNivelUnqa"></param>
    ''' <param name="iVarPond"></param>
    ''' <param name="iNivelVarCal"></param>
    ''' <param name="drVarCal"></param>
    ''' <returns></returns>
    Private Function AplicarMediaPonderada(ByVal sProve As String, ByVal lIdUnqa As Integer, ByVal lNivelUnqa As Long, ByVal iVarPond As Integer, ByVal iNivelVarCal As Short, ByRef drVarCal As DataRow, ByVal dValorDef As Double) As Boolean
        Dim bCancelarCalculoPuntuacionProveedor As Boolean

        Try
            'Puntuación variable en UNQA = (Punt. var. en UNQA hija i * Punt. var. ponderación en UNQA hija i) / (Sumatorio Punt. var. ponderación en UNQAs hijas)

            'Obtener la puntuación de la variable para las UNQAs hoja de la UNQA del cálculo (lIdUnqa)            
            Dim dtVarPuntuaciones As DataTable = m_dsVariablesCalculo.Tables("TEMP" & iNivelVarCal.ToString)

            Dim sFiltroVarPunt As String = "UNQA_HOJA=1 AND ID=" & drVarCal("ID")
            If lNivelUnqa > 0 Then sFiltroVarPunt &= " AND (UNQA1=" & lIdUnqa.ToString & " OR UNQA2=" & lIdUnqa.ToString & ")"
            If iNivelVarCal > 1 Then
                For i As Integer = 2 To iNivelVarCal
                    sFiltroVarPunt &= " AND ID_VAR_CAL" & (i - 1).ToString & "=" & drVarCal("ID_VAR_CAL" & (i - 1).ToString)
                Next
            End If
            Dim dvPuntVarPunt As New DataView(dtVarPuntuaciones, sFiltroVarPunt, "ID", DataViewRowState.CurrentRows)

            Dim dSumaPonderada As Double
            Dim dSumaPuntVarPondUNQAsHijas As Double
            For Each drvPuntVarPunt As DataRowView In dvPuntVarPunt
                'Obtener las puntuaciones de la variable de ponderación para las UNQAs hojas de la UNQA del cálculo (lIdUnqa)     
                Dim sFiltroVarPond As String = "UNQA=" & drvPuntVarPunt("UNQA") & " AND ID=" & iVarPond.ToString
                If iNivelVarCal > 1 Then
                    For i As Integer = 2 To iNivelVarCal
                        sFiltroVarPond &= " AND ID_VAR_CAL" & (iNivelVarCal - 1).ToString & "=" & drvPuntVarPunt("ID_VAR_CAL" & (iNivelVarCal - 1).ToString)
                    Next
                End If
                Dim dvPuntVarPond As New DataView(dtVarPuntuaciones, sFiltroVarPond, "ID", DataViewRowState.CurrentRows)    'Sólo debería devolver 1 registro

                dSumaPonderada += (drvPuntVarPunt("PUNT") * dvPuntVarPond(0)("PUNT"))
                dSumaPuntVarPondUNQAsHijas += dvPuntVarPond(0)("PUNT")
            Next

            If dSumaPuntVarPondUNQAsHijas = 0 Then
                drVarCal.Item("PUNT") = dValorDef
            Else
                drVarCal.Item("PUNT") = (dSumaPonderada / dSumaPuntVarPondUNQAsHijas)
            End If
            Dim Res() As Object
            Res = ObtenerCalificacion(drVarCal.Item("ID"), iNivelVarCal, drVarCal.Item("PUNT"), "")
            drVarCal.Item("CALID") = Res(0)
            drVarCal.Item("CAL") = Res(1)

        Catch ex As Exception
            bCancelarCalculoPuntuacionProveedor = True
            Dim oVarCal As New FSNServer.VariablesCalidad(DBServer, mIsAuthenticated)
            oVarCal.GuardarError(sProve, lIdUnqa, drVarCal.Item("ID"), iNivelVarCal, 0, 0, 0, 0, ex.Message)
        End Try

        Return Not bCancelarCalculoPuntuacionProveedor
    End Function
    ''' <summary>
    ''' Obtiene la puntuacionen que le corresponde a la variable de calidad segun su tipo de ponderacion
    ''' </summary>
    ''' <param name="sProve">Código proveedor</param>    
    ''' <param name="lId">ID variable Calidad</param>      
    ''' <param name="iNivel">Nivel de la variable</param>
    ''' <param name="lIdUNQA">Id Unidad de negocio</param>
    ''' <param name="lNivelUnqa">Nivel Unidad de negocio</param>
    ''' <param name="iTipoPond">Tipo Ponderacion</param>
    ''' <param name="iOrigen">Origen</param>
    ''' <param name="iIdFormula">Id fórmula</param>
    ''' <param name="sFormula">Fórmula</param>
    ''' <param name="dValorDef">Valor por defecto</param>
    ''' <param name="dValorCertSinCumpl">Valor por defecto para certificados sin cumplimentar</param>     
    ''' <param name="sIdi">Cod Idioma para obtener la denominacion Calificacion</param>
    ''' <returns>Devuelve un object Con la puntuacion, el id de calificacion, denominacion de la calificacion y las variables Xi...</returns>
    ''' <remarks>Llamada desde=Misma Pagina; Tiempo máximo=1seg.</remarks>
    Private Function CalculoPonderacion(ByVal sProve As String, ByVal lID As Integer, ByVal iNivel As Short, ByVal lIdUNQA As Long, ByVal lNivelUnqa As Long,
                                    ByVal iTipoPond As Integer, ByVal iOrigen As Integer, ByVal iIdFormula As Integer, ByVal sFormula As String,
                                    ByVal dValorDef As Double, ByVal dValorCertSinCumpl As Double, Optional ByVal sIdi As String = "") As Object
        Dim sRes() As Object
        Dim find(1) As Object

        Dim oPonderacion As New FSNServer.Ponderacion(DBServer, mIsAuthenticated)

        Select Case iTipoPond
            Case FSNLibrary.TipoPonderacionVariables.PondNCMediaPesos, FSNLibrary.TipoPonderacionVariables.PondNCNumero, FSNLibrary.TipoPonderacionVariables.PondNCSumaPesos  'NC Numero de NCs
                oPonderacion.PonderacionNoConformidad(sProve, lID, iNivel, lIdUNQA, lNivelUnqa, iTipoPond, sIdi)
            Case FSNLibrary.TipoPonderacionVariables.PondCertificado 'Certificado
                find(0) = lID
                find(1) = iNivel
                If m_oDt.Rows.Find(find) Is Nothing Then
                    oPonderacion.PonderacionCertificado(sProve, lID, iNivel, sIdi, QAPeriodoValidezCertificados, iOrigen, iIdFormula, sFormula, dValorDef, dValorCertSinCumpl) 'Calcular una vez por Unidad de negocio, para el resto es el mismo
                End If
            Case FSNLibrary.TipoPonderacionVariables.PondIntEscContinua, FSNLibrary.TipoPonderacionVariables.PondIntFormula 'Integracion - continua
                oPonderacion.PonderacionIntegracion(sProve, lIdUNQA, lID, iNivel, sIdi)
            Case FSNLibrary.TipoPonderacionVariables.PondPPM
                oPonderacion.PonderacionPPM(sProve, lID, iNivel, lIdUNQA, lNivelUnqa, sIdi)
            Case FSNLibrary.TipoPonderacionVariables.PondCargoProveedores
                oPonderacion.PonderacionCargoProveedores(sProve, lID, iNivel, lIdUNQA, lNivelUnqa, sIdi)
            Case FSNLibrary.TipoPonderacionVariables.PondTasaServicios
                oPonderacion.PonderacionTasaServicios(sProve, lID, iNivel, lIdUNQA, lNivelUnqa, sIdi)
            Case FSNLibrary.TipoPonderacionVariables.PondManual
                oPonderacion.PonderacionManual(sProve, lID, iNivel, lIdUNQA, sIdi)
            Case FSNLibrary.TipoPonderacionVariables.PondEncuesta
                oPonderacion.PonderacionEncuesta(sProve, lID, iNivel, lIdUNQA, sIdi, iOrigen, iIdFormula, sFormula, dValorDef)
        End Select
        Dim dNewRow1 As DataRow
        Array.Clear(find, 0, 1)

        If oPonderacion.TipoError = 0 Then
            ReDim sRes(3)
            If iTipoPond = Fullstep.FSNLibrary.TipoPonderacionVariables.PondCertificado Then
                find(0) = lID
                find(1) = iNivel
                dNewRow1 = m_oDt.Rows.Find(find)

                If dNewRow1 Is Nothing Then
                    dNewRow1 = m_oDt.NewRow
                    dNewRow1.Item("ID") = lID
                    dNewRow1.Item("NIVEL") = iNivel
                    dNewRow1.Item("PUNT") = NothingToDBNull(oPonderacion.Puntuacion)
                    dNewRow1.Item("CAL") = NothingToDBNull(oPonderacion.IDCalif)
                    dNewRow1.Item("CAL_DEN") = NothingToDBNull(oPonderacion.Calificacion)
                    dNewRow1.Item("VARIABLESXI") = oPonderacion.VariablesXi
                    If dNewRow1.RowState = DataRowState.Detached Then
                        m_oDt.Rows.Add(dNewRow1)
                    End If
                End If

                If dNewRow1 Is Nothing Then
                    ReDim sRes(1)
                    ReDim sRes(4)

                    sRes(0) = oPonderacion.IdFormulaError
                    sRes(1) = oPonderacion.TipoError
                    sRes(2) = oPonderacion.VarXError
                    sRes(3) = oPonderacion.ValoresFormulaError
                    sRes(4) = oPonderacion.DenError
                Else
                    sRes(0) = dNewRow1.Item("PUNT")
                    sRes(1) = dNewRow1.Item("CAL")
                    sRes(2) = dNewRow1.Item("CAL_DEN")
                    sRes(3) = dNewRow1.Item("VARIABLESXI")
                End If
            Else
                sRes(0) = NothingToDBNull(oPonderacion.Puntuacion)
                sRes(1) = NothingToDBNull(oPonderacion.IDCalif)
                sRes(2) = NothingToDBNull(oPonderacion.Calificacion)
                sRes(3) = oPonderacion.VariablesXi
            End If
        Else
            ReDim sRes(4)

            sRes(0) = oPonderacion.IdFormulaError
            sRes(1) = oPonderacion.TipoError
            sRes(2) = oPonderacion.VarXError
            sRes(3) = oPonderacion.ValoresFormulaError
            sRes(4) = oPonderacion.DenError
        End If
        oPonderacion = Nothing
        CalculoPonderacion = sRes
    End Function
    ''' <summary>
    ''' Devuelve el ID y la Denominacion de la calificacion que le corresponden a los puntos
    ''' </summary>
    ''' <param name="lID">ID variable de calidad</param>    
    ''' <param name="iNivel">Nivel variable de calidad</param>      
    ''' <param name="Puntos">Puntuacion variable de calidad</param>
    ''' <param name="sIdi">Cod Idioma para obtener la denominacion Calificacion</param>
    ''' <returns>Devuelve un object con 2 elementos</returns>
    ''' <remarks>Llamada desde=Propia Pagina; Tiempo máximo=0,1seg.</remarks>
    Public Function ObtenerCalificacion(ByVal lID As Integer, ByVal iNivel As Short, ByVal Puntos As Double, ByVal sIdi As String) As Object
        Authenticate()

        Dim sRes() As Object
        Dim oPonderacion As New FSNServer.Ponderacion(DBServer, mIsAuthenticated)
        oPonderacion.Puntuacion = Puntos
        oPonderacion.ObtenerCalificacion(lID, iNivel, sIdi)
        ReDim sRes(1)
        sRes(0) = NothingToDBNull(oPonderacion.IDCalif)
        sRes(1) = NothingToDBNull(oPonderacion.Calificacion)
        oPonderacion = Nothing
        ObtenerCalificacion = sRes
    End Function
    ''' <summary>
    ''' Proceso que llama al proceso que notifica a los proveedores con cambios en sus calificaciones de las
    ''' diferentes variables "puntuacion total" (una por cada unqa)
    ''' </summary>
    ''' <remarks>Llamada desde: CalcularPuntuaciones; Tiempo máximo: 0,1</remarks>
    Private Sub NotificacionCambioCalificacionTotal(ByVal oProve As Object)
        Try
            oProve.NotificacionCambioCalificacionTotal()
        Catch ex As Exception
            DBServer.Errores_Create("Puntuaciones.vb", DBServer.UsuarioConectado, ex.GetType().FullName, ex.Message, ex.StackTrace, "", "")
        Finally
            oProve = Nothing
        End Try
    End Sub
    Private Function QAPeriodoValidezCertificados() As Integer
        If i_QAPeriodoValidezCertificados Is Nothing Then
            Dim oParametros As New FSNServer.Parametros(DBServer, mIsAuthenticated)
            oParametros.LoadData()

            i_QAPeriodoValidezCertificados = oParametros.QAPeriodoValidezCertificado
        End If

        Return i_QAPeriodoValidezCertificados
    End Function
End Class