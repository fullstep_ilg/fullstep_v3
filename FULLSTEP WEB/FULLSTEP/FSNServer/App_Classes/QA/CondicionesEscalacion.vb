﻿<Serializable()>
Public Class CondicionesEscalacion
    Inherits Security

    Public Property CondicionesNivel1 As List(Of CondicionEscalacionNivel1)
    Public Property CondicionesNivel2 As List(Of CondicionEscalacionNivelX)
    Public Property CondicionesNivel3 As List(Of CondicionEscalacionNivelX)
    Public Property CondicionesNivel4 As List(Of CondicionEscalacionNivelX)
    Public Property FormulaN1 As String
    Public Property FormulaN2 As String
    Public Property FormulaN3 As String
    Public Property FormulaN4 As String
    Public Property PlazoNCEPdteCerrarN1 As Integer
    Public Property PlazoNCEPdteCerrarN2 As Integer
    Public Property PlazoNCEPdteCerrarN3 As Integer
    Public Property PlazoNCEPdteCerrarN4 As Integer
    Public Property RecurrenciaNCEPdteCerrarN1 As Integer
    Public Property RecurrenciaNCEPdteCerrarN2 As Integer
    Public Property RecurrenciaNCEPdteCerrarN3 As Integer
    Public Property RecurrenciaNCEPdteCerrarN4 As Integer
    Public Property PlazoNCEPdteValidarCierreN1 As Integer
    Public Property PlazoNCEPdteValidarCierreN2 As Integer
    Public Property PlazoNCEPdteValidarCierreN3 As Integer
    Public Property PlazoNCEPdteValidarCierreN4 As Integer
    Public Property PeriodoCondNCEN2 As Integer
    Public Property PeriodoCondNCEN3 As Integer
    Public Property PeriodoCondNCEN4 As Integer

    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
    ''' <summary>Carga el objeto de condiciones con los datos leídos del XML</summary>
    ''' <param name="dsCondiciones">Dataset con los datos leídops del XML</param>
    ''' <remarks>Llamada desde: FSNWinServiceEscalacion.EscalacionSrv.EjecucionServicioEscalacion, FSNWinServiceEscalacion.EscalacionSrv.EjecucionServicioNotifsNCEsPdtes</remarks>
    Public Sub CargarCondiciones(ByVal dsCondiciones As DataSet)
        If Not dsCondiciones.Tables Is Nothing AndAlso dsCondiciones.Tables.Count > 0 Then
            'Condiciones Nivel 1
            If Not dsCondiciones.Tables("NivelEscalacion_1") Is Nothing Then
                FormulaN1 = dsCondiciones.Tables("NivelEscalacion_1").Rows(0)("Formula")
                If Not IsDBNull(dsCondiciones.Tables("NivelEscalacion_1").Rows(0)("Plazo_NCE_PdteCerrar_M")) Then PlazoNCEPdteCerrarN1 = dsCondiciones.Tables("NivelEscalacion_1").Rows(0)("Plazo_NCE_PdteCerrar_M")
                If Not IsDBNull(dsCondiciones.Tables("NivelEscalacion_1").Rows(0)("Recurrencia_NCE_PdteCerrar_d")) Then RecurrenciaNCEPdteCerrarN1 = dsCondiciones.Tables("NivelEscalacion_1").Rows(0)("Recurrencia_NCE_PdteCerrar_d")
                If Not IsDBNull(dsCondiciones.Tables("NivelEscalacion_1").Rows(0)("Plazo_NCE_PdteValidarCierre_M")) Then PlazoNCEPdteValidarCierreN1 = dsCondiciones.Tables("NivelEscalacion_1").Rows(0)("Plazo_NCE_PdteValidarCierre_M")

                If Not dsCondiciones.Tables("Condiciones_VarCal") Is Nothing Then
                    CondicionesNivel1 = New List(Of CondicionEscalacionNivel1)

                    For Each oCondN1 As DataRow In dsCondiciones.Tables("Condiciones_VarCal").Rows
                        CondicionesNivel1.Add(New CondicionEscalacionNivel1(oCondN1("Condiciones_VarCal_Id"), oCondN1("VarCal_Nivel"), oCondN1("VarCal_ID")))
                    Next
                End If
            End If

            'Condiciones Nivel 2,3 y 4
            If Not dsCondiciones.Tables("NivelEscalacion") Is Nothing Then
                Dim dvNiveles As New DataView(dsCondiciones.Tables("NivelEscalacion"), String.Empty, "Nivel", DataViewRowState.CurrentRows)

                'Condiciones Nivel 2
                If dvNiveles.FindRows("2").Count > 0 Then
                    Dim drvN2 As DataRowView = dvNiveles.FindRows("2")(0)
                    If Not IsDBNull(drvN2("Formula")) Then FormulaN2 = drvN2("Formula")
                    If Not IsDBNull(drvN2("Plazo_NCE_PdteCerrar_M")) Then PlazoNCEPdteCerrarN2 = drvN2("Plazo_NCE_PdteCerrar_M")
                    If Not IsDBNull(drvN2("Recurrencia_NCE_PdteCerrar_d")) Then RecurrenciaNCEPdteCerrarN2 = drvN2("Recurrencia_NCE_PdteCerrar_d")
                    If Not IsDBNull(drvN2("Plazo_NCE_PdteValidarCierre_M")) Then PlazoNCEPdteValidarCierreN2 = drvN2("Plazo_NCE_PdteValidarCierre_M")
                    If Not IsDBNull(drvN2("Periodo_Condiciones_NCE")) Then PeriodoCondNCEN2 = drvN2("Periodo_Condiciones_NCE")

                    If Not dsCondiciones.Tables("Condiciones_NCE") Is Nothing Then
                        CondicionesNivel2 = New List(Of CondicionEscalacionNivelX)

                        Dim dvCondN2 As New DataView(dsCondiciones.Tables("Condiciones_NCE"), "NivelEscalacion_Id=" & drvN2("NivelEscalacion_Id"), String.Empty, DataViewRowState.CurrentRows)
                        For Each drvCond As DataRowView In dvCondN2
                            CondicionesNivel2.Add(New CondicionEscalacionNivelX(drvCond("Condiciones_NCE_Id"), drvCond("NCE_Nivel_Escalacion"), drvCond("Operador"), drvCond("NCE_Numero")))
                        Next
                    End If
                End If

                'Condiciones Nivel 3
                If dvNiveles.FindRows("3").Count > 0 Then
                    Dim drvN3 As DataRowView = dvNiveles.FindRows("3")(0)
                    If Not IsDBNull(drvN3("Formula")) Then FormulaN3 = drvN3("Formula")
                    If Not IsDBNull(drvN3("Plazo_NCE_PdteCerrar_M")) Then PlazoNCEPdteCerrarN3 = drvN3("Plazo_NCE_PdteCerrar_M")
                    If Not IsDBNull(drvN3("Recurrencia_NCE_PdteCerrar_d")) Then RecurrenciaNCEPdteCerrarN3 = drvN3("Recurrencia_NCE_PdteCerrar_d")
                    If Not IsDBNull(drvN3("Plazo_NCE_PdteValidarCierre_M")) Then PlazoNCEPdteValidarCierreN3 = drvN3("Plazo_NCE_PdteValidarCierre_M")
                    If Not IsDBNull(drvN3("Periodo_Condiciones_NCE")) Then PeriodoCondNCEN3 = drvN3("Periodo_Condiciones_NCE")

                    If Not dsCondiciones.Tables("Condiciones_NCE") Is Nothing Then
                        CondicionesNivel3 = New List(Of CondicionEscalacionNivelX)

                        Dim dvCondN3 As New DataView(dsCondiciones.Tables("Condiciones_NCE"), "NivelEscalacion_Id=" & drvN3("NivelEscalacion_Id"), String.Empty, DataViewRowState.CurrentRows)
                        For Each drvCond As DataRowView In dvCondN3
                            CondicionesNivel3.Add(New CondicionEscalacionNivelX(drvCond("Condiciones_NCE_Id"), drvCond("NCE_Nivel_Escalacion"), drvCond("Operador"), drvCond("NCE_Numero")))
                        Next
                    End If
                End If

                'Condiciones Nivel 4
                If dvNiveles.FindRows("4").Count > 0 Then
                    Dim drvN4 As DataRowView = dvNiveles.FindRows("4")(0)
                    If Not IsDBNull(drvN4("Formula")) Then FormulaN4 = drvN4("Formula")
                    If Not IsDBNull(drvN4("Plazo_NCE_PdteCerrar_M")) Then PlazoNCEPdteCerrarN4 = drvN4("Plazo_NCE_PdteCerrar_M")
                    If Not IsDBNull(drvN4("Recurrencia_NCE_PdteCerrar_d")) Then RecurrenciaNCEPdteCerrarN4 = drvN4("Recurrencia_NCE_PdteCerrar_d")
                    If Not IsDBNull(drvN4("Plazo_NCE_PdteValidarCierre_M")) Then PlazoNCEPdteValidarCierreN4 = drvN4("Plazo_NCE_PdteValidarCierre_M")
                    If Not IsDBNull(drvN4("Periodo_Condiciones_NCE")) Then PeriodoCondNCEN4 = drvN4("Periodo_Condiciones_NCE")

                    If Not dsCondiciones.Tables("Condiciones_NCE") Is Nothing Then
                        CondicionesNivel4 = New List(Of CondicionEscalacionNivelX)

                        Dim dvCondN4 As New DataView(dsCondiciones.Tables("Condiciones_NCE"), "NivelEscalacion_Id=" & drvN4("NivelEscalacion_Id"), String.Empty, DataViewRowState.CurrentRows)
                        For Each drvCond As DataRowView In dvCondN4
                            CondicionesNivel4.Add(New CondicionEscalacionNivelX(drvCond("Condiciones_NCE_Id"), drvCond("NCE_Nivel_Escalacion"), drvCond("Operador"), drvCond("NCE_Numero")))
                        Next
                    End If
                End If
            End If
        End If
    End Sub
    ''' <summary>Comprueba si las condiciones son válidas
    ''' Las condiciones de nivel 1 son obligatorias
    ''' No puede haber condiciones de un nivel si no están definidas todas las de niveles inferiores
    ''' </summary>
    ''' <returns>Booleano indicando si las condiciones son válidas</returns>
    ''' <remarks>Llamada desde: EscalacionSrv.EjecucionServicioEscalacion</remarks>
    Public Function CondicionesValidas() As Boolean
        Dim bValidas As Boolean = True

        If CondicionesNivel1 Is Nothing OrElse CondicionesNivel1.Count = 0 Then
            bValidas = False
        Else
            'Comprobar que las var. de calidad existen y no están dadas de baja lógica
            bValidas = DBServer.NivelesEscalacion_VariablesCalidadValidas(CrearDataTableCondicionesN1)

            If bValidas Then
                If (CondicionesNivel2 Is Nothing OrElse CondicionesNivel2.Count = 0) And ((CondicionesNivel3 IsNot Nothing AndAlso CondicionesNivel3.Count > 0) Or (CondicionesNivel4 IsNot Nothing AndAlso CondicionesNivel4.Count > 0)) Then
                    bValidas = False
                Else
                    If (CondicionesNivel3 Is Nothing OrElse CondicionesNivel3.Count = 0) And (CondicionesNivel4 IsNot Nothing AndAlso CondicionesNivel4.Count > 0) Then bValidas = False
                End If
            End If
        End If

        Return bValidas
    End Function
    ''' <summary>Crea un datatable con las var. de calidad de las condiciones del nivel 1 de escalación</summary>
    ''' <returns>Datatable</returns>
    ''' <remarks>Llamada desde: EscalacionProves.TratarProveedoresMalaCalificacion</remarks>
    Public Function CrearDataTableCondicionesN1() As DataTable
        Dim dtVarsCal As New DataTable
        dtVarsCal.Columns.Add("NIVEL", GetType(Integer))
        dtVarsCal.Columns.Add("ID", GetType(Integer))

        For Each oCond As CondicionEscalacionNivel1 In CondicionesNivel1
            Dim oNewRow As DataRow = dtVarsCal.NewRow
            oNewRow("NIVEL") = oCond.Nivel
            oNewRow("ID") = oCond.VarID
            dtVarsCal.Rows.Add(oNewRow)
        Next

        Return dtVarsCal
    End Function
    ''' <summary>Comprueba si se cumple la fórmula de Nivel 1</summary>
    ''' <param name="dvVariables">Datos de las variables de la fórmula</param>
    ''' <returns>Llamada desde: FSNServer.Escalaciones.TratarProveedoresMalaCalificacion</returns>
    Public Function CumpleFormulaN1(ByRef dvVariables As DataView) As Boolean
        Dim sVariables() As String
        Dim dValues() As Double
        Dim bOk As Boolean

        Try
            Authenticate()

            'Recorrer las variables
            If dvVariables.Count > 0 Then
                ReDim sVariables(CondicionesNivel1.Count - 1)
                ReDim dValues(CondicionesNivel1.Count - 1)

                Dim i As Integer = 0
                dvVariables.Sort = "VARCAL"
                For Each oCond As CondicionEscalacionNivel1 In CondicionesNivel1
                    sVariables(i) = oCond.ID

                    Dim drvVar As DataRowView() = dvVariables.FindRows(oCond.VarID)
                    If drvVar.Count > 0 Then
                        dValues(i) = drvVar(0)("PUNT")
                    Else
                        'Si no se encuentra la puntuación genero un error
                        Err.Raise(vbObjectError + 1000, Description:="Proveedor: " & dvVariables(0)("PROVE") & ", UNQA: " & dvVariables(0)("UNQA") & ". No se ha encontrado la puntuación para la variable " & oCond.ID)
                    End If

                    i += 1
                Next

                Dim iEq As New USPExpress.USPExpression
                iEq.Parse(FormulaN1, sVariables)
                bOk = iEq.Evaluate(dValues)
            End If

        Catch ex As Exception
            DBServer.Errores_Create("CondicionesEscalacion.vb", DBServer.UsuarioConectado, ex.GetType().FullName, ex.Message, ex.StackTrace, "", "")
        End Try

        Return bOk
    End Function
    ''' <summary>Indica si se cumple la fórmula para las condiciones de nivel 2</summary>
    ''' <param name="sProve">Cod. proveedor</param>
    ''' <param name="iUNQA">Id UNQA</param>
    ''' <param name="dtProvesEsc">Dataset en el que se devuelven los datos de las NCs que hacen que se cumplan las condiciones</param>
    ''' <param name="oEscalaciones">Escalaciones propuestas por el servicio hasta el momento</param>
    ''' <returns>Booleano indicando si se cumple la fórmula</returns>
    ''' <remarks>Llamada desde: EscalacionProves.TratarProvesMalaCalificacion</remarks>
    Public Function CumpleFormulaN2(ByVal sProve As String, ByVal iUNQA As Integer, ByRef dtProvesEsc As DataTable, Optional ByRef oEscalaciones As ProvesEscalaciones = Nothing) As Boolean
        Return CumpleFormulaNX(sProve, 2, iUNQA, FormulaN2, CondicionesNivel2, PeriodoCondNCEN2, dtProvesEsc, oEscalaciones)
    End Function
    ''' <summary>Indica si se cumple la fórmula para las condiciones de nivel 3</summary>
    ''' <param name="sProve">Cod. proveedor</param>
    ''' <param name="iUNQA">Id UNQA</param>
    ''' <param name="dtProvesEsc">Dataset en el que se devuelven los datos de las NCs que hacen que se cumplan las condiciones</param>
    ''' <param name="oEscalaciones">Escalaciones propuestas por el servicio hasta el momento</param>
    ''' <returns>Booleano indicando si se cumple la fórmula</returns>
    ''' <remarks>Llamada desde: EscalacionProves.TratarProvesMalaCalificacion</remarks>
    Public Function CumpleFormulaN3(ByVal sProve As String, ByVal iUNQA As Integer, ByRef dtProvesEsc As DataTable, Optional ByRef oEscalaciones As ProvesEscalaciones = Nothing) As Boolean
        Return CumpleFormulaNX(sProve, 3, iUNQA, FormulaN3, CondicionesNivel3, PeriodoCondNCEN3, dtProvesEsc, oEscalaciones)
    End Function
    ''' <summary>Indica si se cumple la fórmula para las condiciones de nivel 4</summary>
    ''' <param name="sProve">Cod. proveedor</param>
    ''' <param name="iUNQA">Id UNQA</param>
    ''' <param name="dtProvesEsc">Dataset en el que se devuelven los datos de las NCs que hacen que se cumplan las condiciones</param>
    ''' <param name="oEscalaciones">Escalaciones propuestas por el servicio hasta el momento</param>
    ''' <returns>Booleano indicando si se cumple la fórmula</returns>
    ''' <remarks>Llamada desde: EscalacionProves.TratarProvesMalaCalificacion</remarks>
    Public Function CumpleFormulaN4(ByVal sProve As String, ByVal iUNQA As Integer, ByRef dtProvesEsc As DataTable, Optional ByRef oEscalaciones As ProvesEscalaciones = Nothing) As Boolean
        Return CumpleFormulaNX(sProve, 4, iUNQA, FormulaN4, CondicionesNivel4, PeriodoCondNCEN4, dtProvesEsc, oEscalaciones)
    End Function
    ''' <summary>Indica si se cumple la fórmula para las condiciones de nivel 2, 3 o 4</summary>
    ''' <param name="sProve">Cod. proveedor</param>
    ''' <param name="iNivelEsc">Nivel escalación</param>
    ''' <param name="iUNQA">Id UNQA</param>
    ''' <param name="sFormula">Fórmula a cumplir</param>
    ''' <param name="oCondiciones">Condiciones de la fórmula</param>
    ''' <param name="iPeriodo">Periodo en meses</param>
    ''' <param name="dtProvesEsc">Dataset en el que se devuelven los datos de las NCs que hacen que se cumplan las condiciones</param>
    ''' <param name="oEscalaciones">Escalaciones propuestas por el servicio hasta el momento</param>
    ''' <returns>Booleano indicando si se cumple la fórmula</returns>
    ''' <remarks>llamada desde: CumpleFormulaN2, CumpleFormulaN3, CumpleFormulaN4</remarks>
    Private Function CumpleFormulaNX(ByVal sProve As String, ByVal iNivelEsc As Integer, ByVal iUNQA As Integer, ByVal sFormula As String, ByRef oCondiciones As List(Of CondicionEscalacionNivelX), ByVal iPeriodo As Integer,
                                     ByRef dtProvesEsc As DataTable, Optional ByRef oEscalaciones As ProvesEscalaciones = Nothing) As Boolean
        Dim sVariables() As String
        Dim dValues() As Double
        Dim bOk As Boolean

        Try
            Authenticate()

            ReDim sVariables(oCondiciones.Count - 1)
            ReDim dValues(oCondiciones.Count - 1)

            Dim i As Integer = 0
            For Each oCond As CondicionEscalacionNivelX In oCondiciones
                sVariables(i) = oCond.ID

                Dim iNumConf = 0

                'Obtener el nº de NC del tipo asociado al nivel de escalación indicado en la condición (Si no se indica se obtienen NC de tipos asociados a cualquiera de los Niveles de Escalación.) 
                'que se hayan abierto en el periodo indicado en la configuración
                Dim dsNoConfs As DataSet = DBServer.Proveedores_DevolverNCNivelEscAbiertasEnPeriodo(sProve, iUNQA, iNivelEsc, oCond.Nivel, iPeriodo)
                If Not dsNoConfs Is Nothing AndAlso dsNoConfs.Tables.Count > 0 Then
                    iNumConf = dsNoConfs.Tables(0).Rows.Count

                    'Añadir las nuevas no conformidades
                    For Each dr As DataRow In dsNoConfs.Tables(0).Rows
                        Dim dvNoConfs As New DataView(dtProvesEsc, Nothing, "PROVE,UNQA,NOCONFORMIDAD", DataViewRowState.CurrentRows)
                        If dvNoConfs.Find(New Object() {dr("PROVE"), dr("UNQA"), dr("NOCONFORMIDAD")}) < 0 Then
                            Dim oNewRow As DataRow = dtProvesEsc.NewRow
                            oNewRow("PROVE") = dr("PROVE")
                            oNewRow("PROVE_NIF") = dr("NIF")
                            oNewRow("PROVE_DEN") = dr("DEN")
                            oNewRow("UNQA") = dr("UNQA")
                            oNewRow("NOCONFORMIDAD") = dr("NOCONFORMIDAD")
                            oNewRow("UNQA_COD") = Left(dr("UNQA_COD"), Len(dr("UNQA_COD")) - 1)
                            dtProvesEsc.Rows.Add(oNewRow)
                        End If
                    Next
                End If

                'Obtener las propuestas del nivel indicado en la condición hechas por el servicio
                If Not oEscalaciones Is Nothing Then
                    'Obtener el código de la UNQA de nivel indicado por el nivel de escalación iNivelEsc correspondiente a la UNQA iUNQA
                    Dim dtUNQA As DataTable = DBServer.NivelesEscalacion_DevolverCodUNQANivelEscalacion(iUNQA, iNivelEsc)
                    Dim iIdUNQA As Integer = 1
                    Dim sCodUNQA = String.Empty
                    If dtUNQA.Rows.Count > 0 Then
                        iIdUNQA = dtUNQA.Rows(0)("ID")
                        sCodUNQA = Left(dtUNQA.Rows(0)("COD"), CType(dtUNQA.Rows(0)("COD"), String).Length - 1)
                    End If

                    Dim sFiltro As String = "PROVE='" & sProve & "' AND NIVEL_ESCALACION=" & oCond.Nivel
                    If sCodUNQA <> String.Empty Then sFiltro &= "AND UNQA_COD LIKE '" & sCodUNQA & "%'"
                    'Buscar propuestas para la UNQA de código el obtenido anteriormente o para sus hijas de nivel de escalación indicado por la condición
                    Dim dvPropuestas As New DataView(oEscalaciones.Datos, sFiltro, "", DataViewRowState.CurrentRows)
                    Dim dtPropuestas As DataTable = dvPropuestas.ToTable(True, New String() {"PROVE", "PROVE_NIF", "PROVE_DEN", "UNQA"})
                    'Tener en cuenta las propuestas sólo si hay más de 1
                    If dtPropuestas.Rows.Count > 1 Then
                        iNumConf += dtPropuestas.Rows.Count

                        'Añadir las nuevas no conformidades
                        For Each dr As DataRow In dtPropuestas.Rows
                            Dim dvNoConfs As New DataView(dtProvesEsc, "NOCONFORMIDAD IS NULL", "PROVE,UNQA,NIVEL_ESCALACION", DataViewRowState.CurrentRows)
                            If dvNoConfs.Find(New Object() {dr("PROVE"), dr("UNQA"), oCond.Nivel}) < 0 Then
                                Dim oNewRow As DataRow = dtProvesEsc.NewRow
                                oNewRow("PROVE") = dr("PROVE")
                                oNewRow("PROVE_NIF") = dr("PROVE_NIF")
                                oNewRow("PROVE_DEN") = dr("PROVE_DEN")
                                oNewRow("UNQA") = iIdUNQA
                                oNewRow("NOCONFORMIDAD") = DBNull.Value
                                oNewRow("UNQA_COD") = sCodUNQA
                                oNewRow("UNQA_ORG") = dr("UNQA")
                                oNewRow("NIVEL_ESCALACION_ORG") = oCond.Nivel
                                dtProvesEsc.Rows.Add(oNewRow)
                            End If
                        Next
                    End If
                End If

                dValues(i) = iNumConf

                Dim sFormulaCond As String = "(" & oCond.ID & oCond.Operador & CType(oCond.Numero, String) & ")"
                sFormula = sFormula.Replace(oCond.ID, sFormulaCond)

                i += 1
            Next

            Dim iEq As New USPExpress.USPExpression
            iEq.Parse(sFormula, sVariables)
            bOk = CType(iEq.Evaluate(dValues), Boolean)

        Catch ex As Exception
            DBServer.Errores_Create("CondicionesEscalacion.vb", DBServer.UsuarioConectado, ex.GetType().FullName, ex.Message, ex.StackTrace, "", "")
        End Try

        Return bOk
    End Function

End Class
