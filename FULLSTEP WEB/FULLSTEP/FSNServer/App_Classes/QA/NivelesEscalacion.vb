﻿Public Class NivelesEscalacion
    Inherits Security

    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
    Public Function GetNivelesEscalacion() As List(Of NivelEscalacion)
        Authenticate()

        Dim lNivelesEscalacion As New List(Of NivelEscalacion)
        Dim iNivelEscalacion As NivelEscalacion
        Dim ds As DataSet = DBServer.NivelesEscalacion_GetNivelesEscalacion
        For Each rNivelEscalacion As DataRow In ds.Tables(0).Rows
            iNivelEscalacion = New NivelEscalacion
            With iNivelEscalacion
                .Nivel = rNivelEscalacion("NIVEL")
                .NivelUNQA = rNivelEscalacion("UNQA_NIVEL")
                .Solicitud = rNivelEscalacion("SOLICITUD")
                .Bloqueo_SelProve_Adj = rNivelEscalacion("BLOQUEO_SELPRO_ADJ")
                .Denominacion = New Dictionary(Of String, String)
                .Descripcion = New Dictionary(Of String, String)
                For Each iNivelEscalacionIdioma As DataRow In ds.Tables(1).Rows.OfType(Of DataRow).Where(Function(x) x("NIVEL") = iNivelEscalacion.Nivel).ToList
                    .Denominacion(iNivelEscalacionIdioma("IDIOMA")) = iNivelEscalacionIdioma("DENOMINACION")
                    .Descripcion(iNivelEscalacionIdioma("IDIOMA")) = iNivelEscalacionIdioma("DESCRIPCION")
                Next
            End With
            lNivelesEscalacion.Add(iNivelEscalacion)
        Next
        Return lNivelesEscalacion
    End Function
    Public Sub GuardarNivelEscalacion(ByVal nivelEscalacion As NivelEscalacion)
        Authenticate()
        Dim dtIdiomasNivelEscalacion As New DataTable
        With dtIdiomasNivelEscalacion
            .Columns.Add("IDIOMA")
            .Columns.Add("DENOMINACION")
            .Columns.Add("DESCRIPCION")
        End With
        Dim r As DataRow
        For Each denominacion As Object In nivelEscalacion.Denominacion
            r = dtIdiomasNivelEscalacion.NewRow
            r("IDIOMA") = denominacion.key
            r("DENOMINACION") = denominacion.value
            r("DESCRIPCION") = nivelEscalacion.Descripcion(denominacion.key)
            dtIdiomasNivelEscalacion.Rows.Add(r)
        Next
        Dim xmlDenominacionesDescripciones As XElement
        xmlDenominacionesDescripciones = New XElement("Idiomas", dtIdiomasNivelEscalacion.Rows.OfType(Of DataRow).Select(Function(x) New XElement("Idioma", {
                                                                                                            {New XElement("IDIOMA", x("IDIOMA")),
                                                                                                            New XElement("DENOMINACION", x("DENOMINACION")),
                                                                                                            New XElement("DESCRIPCION", x("DESCRIPCION"))}})))

        DBServer.NivelesEscalacion_GuardarNivelEscalacion(nivelEscalacion.Nivel, nivelEscalacion.NivelUNQA, nivelEscalacion.Solicitud, nivelEscalacion.Bloqueo_SelProve_Adj,
                                                           xmlDenominacionesDescripciones.ToString)
    End Sub
    Public Function Panel_Escalacion_Get_Data(ByVal codProve As String, ByVal UNQA As Integer, ByVal nivelEscalacion As Integer, ByVal escalacionesPendiente As Boolean, ByVal provesBaja As Boolean) As Object
        Authenticate()

        Dim ds As DataSet = DBServer.NivelesEscalacion_Panel_Escalacion_Get_Data(codProve, UNQA, nivelEscalacion, escalacionesPendiente, provesBaja)

        ds.Tables(0).TableName = "INFO_ESCALACION"
        Dim keyInfoEscalacion(0) As DataColumn
        keyInfoEscalacion(0) = ds.Tables("INFO_ESCALACION").Columns("ID")
        ds.Tables("INFO_ESCALACION").PrimaryKey = keyInfoEscalacion

        ds.Tables(1).TableName = "NOCONFORMIDADES_ESCALACION"
        Dim keyNoConfEscalacion(1) As DataColumn
        keyNoConfEscalacion(0) = ds.Tables("NOCONFORMIDADES_ESCALACION").Columns("PROVE_ESCALACION")
        keyNoConfEscalacion(1) = ds.Tables("NOCONFORMIDADES_ESCALACION").Columns("NOCONFORMIDAD")
        ds.Tables("NOCONFORMIDADES_ESCALACION").PrimaryKey = keyNoConfEscalacion

        ds.Tables("INFO_ESCALACION").Columns.Add("UNQA1")
        For i As Integer = 2 To ds.Tables("INFO_ESCALACION").Rows(0)("MAX_NIVEL")
            ds.Tables("INFO_ESCALACION").Columns.Add("UNQA" & i)
        Next
        ds.Tables("NOCONFORMIDADES_ESCALACION").Columns.Add("UNQA1")
        For i As Integer = 2 To ds.Tables("NOCONFORMIDADES_ESCALACION").Rows(0)("MAX_NIVEL")
            ds.Tables("NOCONFORMIDADES_ESCALACION").Columns.Add("UNQA" & i)
        Next

        For Each r As DataRow In ds.Tables("INFO_ESCALACION").Rows
            For j As Integer = 1 To Split(r("UNQA").ToString, "#").Length - 1
                r("UNQA" & j) = Split(r("UNQA"), "#")(j - 1)
            Next
        Next
        For Each r As DataRow In ds.Tables("NOCONFORMIDADES_ESCALACION").Rows
            For j As Integer = 1 To Split(r("UNQA").ToString, "#").Length - 1
                r("UNQA" & j) = Split(r("UNQA"), "#")(j - 1)
            Next
        Next

        Dim parentCols(0) As DataColumn
        Dim childCols(0) As DataColumn
        parentCols(0) = ds.Tables("INFO_ESCALACION").Columns("ID")
        childCols(0) = ds.Tables("NOCONFORMIDADES_ESCALACION").Columns("PROVE_ESCALACION")
        ds.Relations.Add("INFO_ESCALACION_NOCONFORMIDADES", parentCols, childCols, False)

        Return {ds, ds.Tables("INFO_ESCALACION").Rows(0)("MAX_NIVEL"), ds.Tables("NOCONFORMIDADES_ESCALACION").Rows(0)("MAX_NIVEL")}
    End Function
    Public Function PanelEscalacion_Cargar_Proveedores(ByVal prove As String, ByVal UsuCod As String) As DataSet
        Authenticate()
        'Utilizamos el stored FSQA_PROVEEDORES_CERTIF que da los proveedores de QA (deberia llamarse de otra forma)
        Return DBServer.Proveedores_CertifLoad(prove, bUsaLike:=True, sUser:=UsuCod, bOrdenadorPorDenominacion:=True, iTipo:=3)
    End Function
    Public Function Panel_Escalacion_Get_UNQAs(ByVal Idioma As String)
        Authenticate()
        Return DBServer.NivelesEscalacion_Panel_Escalacion_Get_UNQAs(Idioma)
    End Function
    Public Function Panel_Escalacion_Get_NivelesEscalacion(ByVal Idioma As String, ByVal ModuloIdioma As Integer, ByVal IdTexto As Integer)
        Authenticate()
        Return DBServer.NivelesEscalacion_Panel_Escalacion_Get_NivelesEscalacion(Idioma, ModuloIdioma, IdTexto)
    End Function
    ''' <summary>Función ppal para el tratamiento de la escalación de proveedores</summary>
    ''' <param name="oCondiciones">Condiciones de escalación</param>
    ''' <remarks>Llamada desde: FSNWinServiceEscalacion.EscalacionSrv</remarks>
    Public Sub EscalarProveedores(ByRef oCondiciones As CondicionesEscalacion)
        Authenticate()

        Try
            'Obtener datos iveles escalación            
            Dim oNivelesEsc As List(Of NivelEscalacion) = GetNivelesEscalacion()

            'Calcular nivel escalacion proveedores
            Dim oEscalaciones As New Escalaciones
            '1º se tratan los proveedores que ya tienen una escalación
            TratarProveedoresYaEscalados(oNivelesEsc, oCondiciones, oEscalaciones)
            '2º se tratan los proveedores con una mala calificación
            TratarProveedoresMalaCalificacion(oCondiciones, oEscalaciones)

            If oEscalaciones.HayNuevasEscalaciones Then
                'Registrar nuevas escalaciones
                DBServer.Proveedores_InsertarProveedoresEscalados(oEscalaciones.Datos)

                'Notificar escalaciones
                Dim oNotificador As New Notificar(DBServer, mIsAuthenticated)
                oNotificador.NotificarEscalaciones(oNivelesEsc, oEscalaciones.Datos, Date.Today)
            End If

        Catch ex As Exception
            DBServer.Errores_Create("EscalacionProves.vb", DBServer.UsuarioConectado, ex.GetType().FullName, ex.Message, ex.StackTrace, "", "")
        End Try
    End Sub
    ''' <summary>Tratamiento de escalaciones abiertas durante X meses</summary>
    ''' <param name="oCondiciones">Condiciones de escalación</param>
    ''' <param name="oEscalaciones">Objeto con las nuevas escalaciones</param>
    ''' <remarks>Llamada desde: EscalarProveedores</remarks>
    Private Sub TratarProveedoresYaEscalados(ByRef oNivelesEsc As List(Of NivelEscalacion), ByRef oCondiciones As CondicionesEscalacion, ByRef oEscalaciones As Escalaciones)
        Dim oNivelEsc As NivelEscalacion

        Try
            'Escalación a nivel 4
            If oCondiciones.CondicionesNivel4 IsNot Nothing AndAlso oCondiciones.CondicionesNivel4.Count > 0 Then
                'Obtener proveedores escalados a nivel 3                 
                oNivelEsc = oNivelesEsc.Find(Function(o) (o.Nivel = 3))
                Dim dsProvesEsc As DataSet = DBServer.Proveedores_DevolverNoConfEscAbiertas(oNivelEsc.NivelUNQA, oNivelEsc.Solicitud, oCondiciones.PeriodoNCESinCerrarN4)
                oEscalaciones.AgregarEscalaciones(dsProvesEsc, 4)
            End If

            'Escalación a nivel 3
            If oCondiciones.CondicionesNivel3 IsNot Nothing AndAlso oCondiciones.CondicionesNivel3.Count > 0 Then
                'Obtener proveedores escalados a nivel 2                
                oNivelEsc = oNivelesEsc.Find(Function(o) (o.Nivel = 2))
                Dim dsProvesEsc As DataSet = DBServer.Proveedores_DevolverNoConfEscAbiertas(oNivelEsc.NivelUNQA, oNivelEsc.Solicitud, oCondiciones.PeriodoNCESinCerrarN3)
                oEscalaciones.AgregarEscalaciones(dsProvesEsc, 3)
            End If

            'Escalación a nivel 2
            If oCondiciones.CondicionesNivel2 IsNot Nothing AndAlso oCondiciones.CondicionesNivel2.Count > 0 Then
                'Obtener proveedores escalados a nivel 1                
                oNivelEsc = oNivelesEsc.Find(Function(o) (o.Nivel = 1))
                Dim dsProvesEsc As DataSet = DBServer.Proveedores_DevolverNoConfEscAbiertas(oNivelEsc.NivelUNQA, oNivelEsc.Solicitud, oCondiciones.PeriodoNCESinCerrarN2)
                oEscalaciones.AgregarEscalaciones(dsProvesEsc, 2)
            End If

        Catch ex As Exception
            DBServer.Errores_Create("EscalacionProves.vb", DBServer.UsuarioConectado, ex.GetType().FullName, ex.Message, ex.StackTrace, "", "")
        End Try
    End Sub
    ''' <summary>Tratamiento de escalaciones por mala calificación</summary>
    ''' <param name="oCondiciones">Condiciones de escalación</param>
    ''' <param name="oEscalaciones">Objeto con las nuevas escalaciones</param>
    Private Sub TratarProveedoresMalaCalificacion(ByRef oCondiciones As CondicionesEscalacion, ByRef oEscalaciones As Escalaciones)
        Try
            'Obtener las calificaciones para los proveedores de las UNQAs del nivel al que se aplica el nivel 1 de escalación para las var. de calidad que están en su fórmula            
            Dim dsPuntuaciones As DataSet = DBServer.Proveedores_DevolverCalificacionesEscalacionN1(oCondiciones.CrearDataTableCondicionesN1)
            If Not dsPuntuaciones Is Nothing AndAlso dsPuntuaciones.Tables.Count > 0 Then
                dsPuntuaciones.Tables(0).DefaultView.Sort = "PROVE,UNQA"

                'Obtener los proveedores/plantas implicados en la valoración               
                Dim dtProvesUNQAs As DataTable = dsPuntuaciones.Tables(0).DefaultView.ToTable(True, New String() {"PROVE", "UNQA"})

                'Recorrer los proveedores y ver cuál cumple la fórmula                
                For Each drProveUNQA As DataRow In dtProvesUNQAs.Rows
                    Dim dvVar As New DataView(dsPuntuaciones.Tables(0), "PROVE='" & drProveUNQA("PROVE") & "' AND UNQA=" & drProveUNQA("UNQA"), String.Empty, DataViewRowState.CurrentRows)
                    If oCondiciones.CumpleFormulaN1(dvVar) Then
                        'Comprobar si el proveedor tiene alguna escalación sin cerrar a nivel 4, 3, 2, 1 para la UNQA
                        'En la consulta se comprueba en la UNQA y sus ascendientes para todos los niveles de escalación
                        If Not DBServer.Proveedores_ProveTieneNoConfEscAbiertas(drProveUNQA("PROVE"), drProveUNQA("UNQA")) Then
                            'calcular el Nivel de Escalación al  que le corresponde ser escalado en función del número y nivel de NC de escalación que haya tenido en un periodo de tiempo
                            Dim dsProvesEsc As DataSet
                            Dim bSeguir As Boolean = True

                            'Comprobar fórmula Nivel 4
                            If oCondiciones.CondicionesNivel4 IsNot Nothing AndAlso oCondiciones.CondicionesNivel4.Count > 0 Then
                                If oCondiciones.CumpleFormulaN4(drProveUNQA("PROVE"), drProveUNQA("UNQA"), dsProvesEsc) Then
                                    oEscalaciones.AgregarEscalaciones(dsProvesEsc, 4)
                                    bSeguir = False
                                End If
                            End If

                            'Comprobar fórmula Nivel 3
                            If bSeguir And (oCondiciones.CondicionesNivel3 IsNot Nothing AndAlso oCondiciones.CondicionesNivel3.Count > 0) Then
                                If oCondiciones.CumpleFormulaN3(drProveUNQA("PROVE"), drProveUNQA("UNQA"), dsProvesEsc) Then
                                    oEscalaciones.AgregarEscalaciones(dsProvesEsc, 3)
                                    bSeguir = False
                                End If
                            End If

                            'Comprobar fórmula Nivel 2
                            If bSeguir And (oCondiciones.CondicionesNivel2 IsNot Nothing AndAlso oCondiciones.CondicionesNivel2.Count > 0) Then
                                If oCondiciones.CumpleFormulaN2(drProveUNQA("PROVE"), drProveUNQA("UNQA"), dsProvesEsc) Then
                                    oEscalaciones.AgregarEscalaciones(dsProvesEsc, 2)
                                    bSeguir = False
                                End If
                            End If

                            'Si finalmente no se ha cumplido con los niveles superiores habrá que escalar al proveedor al nivel 1 en la unidad de negocio que estamos evaluando
                            Dim drvPunt() As DataRowView = dsPuntuaciones.Tables(0).DefaultView.FindRows(New Object() {drProveUNQA("PROVE"), drProveUNQA("UNQA")})
                            If bSeguir Then oEscalaciones.AgregarEscalacion(drvPunt(0)("PROVE"), drvPunt(0)("NIF"), drvPunt(0)("DEN"), 1, drvPunt(0)("UNQA"), drvPunt(0)("UNQA_COD"))
                        End If
                    End If
                Next
            End If

        Catch ex As Exception
            DBServer.Errores_Create("EscalacionProves.vb", DBServer.UsuarioConectado, ex.GetType().FullName, ex.Message, ex.StackTrace, "", "")
        End Try
    End Sub

#Region "Clase Escalaciones"
    ''' Clase que gestiona las nuevas escalaciones
    Private Class Escalaciones
        Public Property Datos As DataTable
        Public Sub New()
            Datos = New DataTable
            Datos.Columns.Add("PROVE", GetType(String))
            Datos.Columns.Add("PROVE_NIF", GetType(String))
            Datos.Columns.Add("PROVE_DEN", GetType(String))
            Datos.Columns.Add("NIVEL_ESCALACION", GetType(Integer))
            Datos.Columns.Add("NIVEL_ESCALACION_DEN", GetType(String))
            Datos.Columns.Add("UNQA", GetType(Integer))
            Datos.Columns.Add("UNQA_COD", GetType(String))
            Datos.Columns.Add("UNQA_DEN", GetType(String))
            Datos.Columns.Add("NOCONFORMIDAD", GetType(Integer))
        End Sub
        ''' <summary>Agrega nuevas escalaciones a la lista de escalaciones a insertar</summary>
        ''' <param name="dsProvesEsc">Datos con las no conformidades para las nuevas escalaciones</param>
        ''' <param name="iNivelescalacion">Nivel de las nuevas escalaciones</param>
        ''' <remarks>Llamada desde: EscalacionProves.TratarProveedoresYaEscalados</remarks>
        Public Sub AgregarEscalaciones(ByRef dsProvesEsc As DataSet, ByVal iNivelescalacion As Integer)
            If Not dsProvesEsc Is Nothing AndAlso dsProvesEsc.Tables.Count > 0 Then
                For Each oRow As DataRow In dsProvesEsc.Tables(0).Rows
                    Dim oNewRow As DataRow = Datos.NewRow
                    oNewRow("PROVE") = oRow("PROVE")
                    oNewRow("PROVE_NIF") = oRow("NIF")
                    oNewRow("PROVE_DEN") = oRow("DEN")
                    oNewRow("NIVEL_ESCALACION") = iNivelescalacion
                    oNewRow("UNQA") = oRow("UNQA")
                    oNewRow("UNQA_COD") = oRow("UNQA_COD")
                    oNewRow("NOCONFORMIDAD") = oRow("NOCONFORMIDAD")

                    Datos.Rows.Add(oNewRow)
                Next
            End If
        End Sub
        ''' <summary>Agrega una nueva escalación a la lista de escalaciones a insertar</summary>
        ''' <param name="sProve">Cod. proveedor</param>
        ''' <param name="sProveNIF">NIF proveedor</param>
        ''' <param name="sProveDen">Den. proveedor</param>
        ''' <param name="iNivel">Nivel escalación</param>
        ''' <param name="iUNQA">UNQA en la que se produce la escalación</param>
        ''' <param name="sCodUNQA">Cod. UNQA con los códigos de los ascendentes concatenados</param>
        ''' <remarks>Llamada desde: TratarProveedores mala calificación</remarks>
        Public Sub AgregarEscalacion(ByVal sProve As String, ByVal sProveNIF As String, ByVal sProveDen As String, ByVal iNivel As Integer, ByVal iUNQA As Integer, ByVal sCodUNQA As String)
            Dim oNewRow As DataRow = Datos.NewRow
            oNewRow("PROVE") = sProve
            oNewRow("PROVE_NIF") = sProveNIF
            oNewRow("PROVE_DEN") = sProveDen
            oNewRow("NIVEL_ESCALACION") = iNivel
            oNewRow("UNQA") = iUNQA
            oNewRow("UNQA_COD") = sCodUNQA

            Datos.Rows.Add(oNewRow)
        End Sub
        ''' <summary>Indica si hay nuevas escalaciones a añadir</summary>
        ''' <returns>Booleano indicando si haynuevas escalaciones</returns>
        ''' <remarks>Llamada desde: EscalarProveedores</remarks>
        Public Function HayNuevasEscalaciones() As Boolean
            Return (Datos.Rows.Count > 0)
        End Function
    End Class
#End Region
End Class
