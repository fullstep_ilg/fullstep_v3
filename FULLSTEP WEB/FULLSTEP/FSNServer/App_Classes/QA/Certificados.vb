Imports System.Threading
Imports System.Text.RegularExpressions

<Serializable()> _
Public Class Certificados
    Inherits Security

    Private moCertificados As DataSet
    Private moColCertificados As Collection
    Public ReadOnly Property ColCertificados() As Collection
        Get
            Return moColCertificados
        End Get
    End Property
    Friend Sub Add(ByVal lId As Long)
        Dim oCertificado As New Certificado(DBServer, mIsAuthenticated)
        If moColCertificados Is Nothing Then moColCertificados = New Collection

        oCertificado.ID = lId
        moColCertificados.Add(oCertificado, lId.ToString)
    End Sub
    Public ReadOnly Property Data() As Data.DataSet
        Get
            Return moCertificados
        End Get
    End Property
    ''' <summary>
    ''' Procedimiento que carga los certificados emitidos en el objeto interno
    ''' </summary>
    ''' <param name="sCertificados">Los certificados emitidos</param>
    ''' <param name="sIdi">Idioma de la aplicaci�n</param>
    ''' <remarks>
    ''' Llamada desdE: PMWeb/certifsolicitadosOK/Page_LOad
    ''' Tiempo m�ximo: 0,5 seg</remarks>
    Public Sub DevolverCertificadosEmitidos(ByVal sCertificados As String, ByVal sIdi As String)
        Authenticate()
        moCertificados = DBServer.Certificados_CargarCertEmitidos(sCertificados, sIdi)
    End Sub
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
    '''<summary>
    '''Function que crea la instancia de un certificado, relaciona el certificado con el proveedor y lo publica antes de mandarlo al webservice.
    '''                  PRIMERO se crea la tabla temporal con el dataset
    '''                  SEGUNDO se crea el certificado y la instancia con la tabla temporal y se relaciona el certificado al proveedor 
    '''Par�metros de entrada: 
    '''                               sPeticionario:= Codigo de persona que crea el certificado
    '''                               oDs:= DataSet para crear la tabla temporal con los datos del certificado (id tipo certificado, codigo proveedor, fecha despublicacion y codigo persona que lo solicita)
    '''                               bRenovar:= (True --> Renovacion de certificado, false:= Solicitar certificado)
    '''                               sCadenaBusqueda := Parametro que se utiliza por referencia para pasarle los id del tipo de certificado, codigo del proveedor,ID de certificados, ID de la instancia creados
    '''                               usercode ,userpassword --> Autentificacion usuario
    ''' Par�metros de salida: Id del error si lo ha habido                                      
    ''' Llamada desde: PmWen--> guardarCertificados-->PagE_Load ()                   
    ''' Tiempo m�ximo: 0,29 seg
    ''' </summary>                
    '''************************************************************
    Public Sub Solicitar_Renovar(ByVal dtCertificados As DataTable, ByVal Renovar As Boolean, _
                        ByVal Peticionario As String, ByVal Email As String, _
                        Optional ByVal Fecha_Despublicacion As Nullable(Of DateTime) = Nothing, _
                        Optional ByVal Fecha_Limite_Cumplimentacion As Nullable(Of DateTime) = Nothing, _
                        Optional ByRef InfoCertificados As String = "", _
                        Optional ByVal EstablecerTodo As Boolean = True)
        Authenticate()
        DBServer.Certificados_Solicitar_Renovar(dtCertificados, Peticionario, EstablecerTodo, _
                                IIf(Fecha_Despublicacion.HasValue, Fecha_Despublicacion, Date.MinValue), _
                                IIf(Fecha_Limite_Cumplimentacion.HasValue AndAlso Not EstablecerTodo, _
                                        Fecha_Limite_Cumplimentacion, Date.MinValue), _
                                Renovar, InfoCertificados)

        Dim dDatos As New Dictionary(Of String, String)
        dDatos.Add("InfoCertificados", InfoCertificados)
        dDatos.Add("Email", Email)

        ThreadPool.QueueUserWorkItem(New WaitCallback(AddressOf NotificarSolicitudEnvioCertificados), dDatos)
    End Sub
    ''' <summary>Despublica los certificados pasados como par�metro</summary>
    ''' <param name="dtCertificados">Certificados a despublicar</param>
    ''' <remarks>LLamada desde: Certificado.SolicitudAutomaticaTiposCertificadosCondiciones</remarks>
    Public Sub Despublicar(ByVal dtCertificados As DataTable)
        Authenticate()
        DBServer.Certificados_Despublicar(dtCertificados)
    End Sub
    ''' <summary>
    ''' Notifica Solicitud/Renovacion Certificado 
    ''' </summary>
    ''' <param name="datos">lista de certificados / mail del q lo hizo</param>
    ''' <remarks>Llamada desde: Solicitar_Renovar; Tiempo maximo: 0,2</remarks>
    Private Sub NotificarSolicitudEnvioCertificados(ByVal datos As Object)
        Dim oCertif() As String
        Dim lIdCertificado As Long
        Dim i As Integer
        'env�a los emails correspondientes a los certificados
        Dim oNotificador As New Notificar(DBServer, mIsAuthenticated)
        If datos("InfoCertificados") <> "" Then
            oCertif = Split(datos("InfoCertificados"), ",")
            For i = 0 To UBound(oCertif)
                If oCertif(i) <> "" Then
                    lIdCertificado = Split(oCertif(i), "&")(2)
                    If CType(Split(oCertif(i), "&")(4), Boolean) Then oNotificador.NotificacionCertificadoEmitido(sFrom:=datos("Email"), lCertificado:=lIdCertificado)
                End If
            Next i
        End If
        oNotificador.FuerzaFinalizeSmtpClient()
        oNotificador = Nothing
    End Sub
    ''' <summary>
    ''' Carga los datos del certificado para el webpart
    ''' </summary>
    ''' <param name="iSobrePasada">Si carga los certificados que ha sobrepasado la fecha de despublicacion //  (0->Vacio //1->Sobrepasadas //2->Con fecha de despublicacion)</param>
    ''' <param name="dFechaLimCumpl">Fecha limite cumplimentaci�n. Para obtener los certificados con fecha limite cumplimentaci�n entre hoy y esa fecha</param>        
    ''' <param name="bVerFechaAct">Para mostrar los certificados modificados en una fecha determinada</param>
    ''' <param name="dFechaAct">Fecha de actualizacion del certificado</param>
    ''' <param name="bSinRespuesta">Si muestra los certificados con estado sin respuesta por parte del proveedor</param>
    ''' <param name="bDatosProveedor">Si muestra los certificados con estado Con datos guardados por el proveedor sin enviar</param>
    ''' <param name="bConCertificado">Si muestra los certificados con estado Con certificado</param>
    ''' <param name="bExpirados">Si muestra los certificados Expirados</param>
    ''' <param name="bProximoAExpirar">Si muestra los certificados proximos a expirar</param>
    ''' <param name="bPublicado">Si muestra los certificados que esten publicados</param>
    ''' <param name="bDespublicados">Si muestra los certificados que esten despublicados</param>
    ''' <param name="bSolicitados">Si muestra los certificados que esten solicitados</param>
    ''' <param name="bRenovados">Si muestra los certificados que esten renovados</param>
    ''' <param name="bEnviados">Si muestra los certificados que esten enviados</param>
    ''' <param name="sTipoCertificados">Contiene cadena con el tipo de certificados a filtrar</param>
    ''' <param name="bPorUsuario">Si se filtra los certificados por que el usuario sea el peticionario o no</param>
    ''' <param name="sUsu">Codigo usuario</param>
    ''' <param name="sIdi">Codigo Idioma</param>
    ''' <param name="iTipoProveedores">1-todos, 2-panel calidad, 3-Potenciales, 4-lista de proveedores </param>
    ''' <param name="sListaProves">Si se ha elegido la opci�n 4.lista de proveedores, cadena con los codigos de los proveedores</param>
    ''' <param name="bProveedoresBaja">Incluir o no a los proveedores de baja l�gica</param>     
    ''' <param name="bSinSolicitar">Si muestra los certificados que esten sin solicitar</param>
    ''' <param name="bPdteValidar">Si muestra los certificados que esten en estado Pendiente de validar</param>
    ''' <param name="bNoValido">Si muestra los certificados que esten en estado No Valido</param> 
    ''' <remarks>Llamada desde=FSQAWebPartCertificados.vb --> CargarCertificados; Tiempo m�ximo = 1 seg.</remarks>
    Public Sub LoadDataWebPartCertificados(ByVal iSobrePasada As Integer, ByVal dFechaLimCumpl As Date, _
            ByVal bVerFechaAct As Boolean, ByVal dFechaAct As Date, ByVal bSinRespuesta As Boolean,
            ByVal bDatosProveedor As Boolean, ByVal bConCertificado As Boolean, ByVal bExpirados As Boolean,
            ByVal bProximoAExpirar As Boolean, ByVal bPublicado As Boolean, ByVal bDespublicados As Boolean,
            ByVal bSolicitados As Boolean, ByVal bRenovados As Boolean, ByVal bEnviados As Boolean,
            ByVal sTipoCertificados As String, ByVal bPorUsuario As Boolean, ByVal sUsu As String, ByVal sIdi As String,
            ByVal iTipoProveedores As Integer, ByVal sListaProves As String, ByVal bProveedoresBaja As Boolean, _
            ByVal RestricSolicUsu As Boolean, ByVal RestricSolicUO As Boolean, ByVal RestricSolicDep As Boolean, _
            ByVal RestricProvMat As Boolean, ByVal RestricProvEqp As Boolean, ByVal RestricProvCon As Boolean, _
            ByVal bSinSolicitar As Boolean, ByVal bPdteValidar As Boolean, ByVal bNoValido As Boolean)
        Authenticate()
        moCertificados = DBServer.Certificados_WebPartCertificados(iSobrePasada, dFechaLimCumpl, bVerFechaAct, dFechaAct, _
            bSinRespuesta, bDatosProveedor, bConCertificado, bExpirados, bProximoAExpirar, bPublicado, bDespublicados, _
            bSolicitados, bRenovados, bEnviados, sTipoCertificados, bPorUsuario, sUsu, sIdi, iTipoProveedores, sListaProves, _
            bProveedoresBaja, RestricSolicUsu, RestricSolicUO, RestricSolicDep, RestricProvMat, RestricProvEqp, RestricProvCon, _
            bSinSolicitar, bPdteValidar, bNoValido)
    End Sub
    ''' <summary>
    ''' Ejecuta la Notificaci�n al proveedor de la expiraci�n de certificados.
    ''' Realmente lo de expiraci�n va de enviar los siguientes mails
    '''    - Aviso de la expiraci�n de certificados
    '''    - Aviso de la proximidad de expiraci�n de certificados
    ''' </summary>
    ''' <remarks>Llamada desde: QA_Notificaciones/Certificados_NotificarExpiracion;Tiempo m�ximo: 0,2</remarks>
    Public Sub NotificarExpiracionCertificados()
        Authenticate()

        moCertificados = DBServer.NotificarObtenerCertificadosExpirados()
        If moCertificados.Tables.Count > 0 Then
            If moCertificados.Tables(0).Rows.Count > 0 Then
                Dim oNotificador As New Notificar(DBServer, mIsAuthenticated)
                oNotificador.NotificacionCertificadosExpirados(moCertificados)
                oNotificador.FuerzaFinalizeSmtpClient()
                oNotificador = Nothing
            End If
        End If
    End Sub
    ''' <summary>
    ''' Ejecuta la Notificaci�n al proveedor de los certificados revisados.
    ''' Enviar el mail correspondiente al proveedor
    ''' </summary>
    ''' <remarks>Llamada desde: QA_Notificaciones/Certificados_NotificarRevisados;Tiempo m�ximo: 0,2</remarks>
    Public Sub NotificarRevisados()
        Authenticate()

        moCertificados = DBServer.NotificarObtenerCertificadosRevisados()
        If moCertificados.Tables.Count > 0 Then
            If moCertificados.Tables(0).Rows.Count > 0 Then
                Dim oNotificador As New Notificar(DBServer, mIsAuthenticated)
                'oNotificador.NotificacionCertificadosExpirados(moCertificados)
                oNotificador.NotificacionCertificadosRevisados(moCertificados)
                oNotificador.FuerzaFinalizeSmtpClient()
                oNotificador = Nothing
            End If
        End If
    End Sub
    Public Function ObtenerCertPendientesEnviar(ByVal lCertificado As Long, ByVal m_sProveGS As String) As DataSet
        Authenticate()
        moCertificados = DBServer.ObtenerCertPendientesEnviar(lCertificado, m_sProveGS)
        Return moCertificados
    End Function
    Public Function ObtenerCertificadosSinSolicitar() As DataSet
        Authenticate()
        Return DBServer.Certificados_Obtener_CertificadosSinSolicitar()
    End Function
    Public Function Obtener_Certificados_Por_Expresion_Regular() As DataSet
        Authenticate()
        Return DBServer.Obtener_Certificados_Por_Expresion_Regular()
    End Function
    Public Sub Actualizar_Solicitud_Proveedor_Expresion_Regular(ByVal dtSolicitudProveedorExpReg As DataTable)
        Authenticate()
        DBServer.Actualizar_Solicitud_Proveedor_Expresion_Regular(dtSolicitudProveedorExpReg)
    End Sub
    ''' <summary>
    ''' Desasociar un tipo de certificado a un proveedor porque ya no le corresponda por material o por tipo de proveedor
    ''' </summary>
    ''' <remarks>Llamada desde=QA_Solcitud_Certificados.asmx/Certificados_Despublicar_Automatica; Tiempo máximo=0,2seg.</remarks>
    Public Sub Certificados_Despublicar_Automatica()
        Authenticate()
        'Primero de todo actualizamos las relaciones de los proveedores con los certificados por expresiones regulares
        Certificados_Por_Expresiones_Regulares()
        'Ahora despublicamos
        DBServer.Certificados_Despublicar_Automatica()
    End Sub
    ''' <summary>Certificados por expresiones regulares</summary>
    ''' <remarks></remarks>
    Public Sub Certificados_Por_Expresiones_Regulares()
        Authenticate()

        Try
            Dim dsInfoCertificados As DataSet = Obtener_Certificados_Por_Expresion_Regular()

            If Not dsInfoCertificados.Tables(0).Rows.Count = 0 Then
                Dim dtExpReg_Certif_Solic As New DataTable
                With dtExpReg_Certif_Solic
                    .Columns.Add("PROVE", System.Type.GetType("System.String"))
                    .Columns.Add("SOLICITUD", System.Type.GetType("System.Int32"))
                    .Columns.Add("EXPREG", System.Type.GetType("System.Int32"))
                End With

                Dim view As New DataView(dsInfoCertificados.Tables(0))
                Dim dtSolicitudesProveedor As DataTable = view.ToTable(True, "ID", "COD")

                Dim cumpleCondiciones As Boolean
                Dim drExpReg_Certif_Solic As DataRow
                For Each solicitudProveedor As DataRow In dtSolicitudesProveedor.Rows
                    drExpReg_Certif_Solic = dtExpReg_Certif_Solic.NewRow
                    cumpleCondiciones = True
                    With drExpReg_Certif_Solic
                        .Item("PROVE") = solicitudProveedor("COD")
                        .Item("SOLICITUD") = solicitudProveedor("ID")

                        For Each infoProve As DataRow In dsInfoCertificados.Tables(0).Select("ID=" & solicitudProveedor("ID") & " AND COD='" & solicitudProveedor("COD") & "'")
                            Select Case CType(infoProve("OPERADOR"), Integer)
                                Case 1
                                    cumpleCondiciones = cumpleCondiciones AndAlso Regex.IsMatch(infoProve(infoProve("CAMPO_PROVE")).ToString, infoProve("EXPREG"))
                                Case 2
                                    cumpleCondiciones = cumpleCondiciones OrElse Regex.IsMatch(infoProve(infoProve("CAMPO_PROVE")).ToString, infoProve("EXPREG"))
                                Case Else
                                    cumpleCondiciones = Regex.IsMatch(infoProve(infoProve("CAMPO_PROVE")).ToString, infoProve("EXPREG"))
                            End Select
                        Next
                        If cumpleCondiciones Then
                            .Item("EXPREG") = 1
                        Else
                            .Item("EXPREG") = 0
                        End If
                    End With
                    dtExpReg_Certif_Solic.Rows.Add(drExpReg_Certif_Solic)
                Next

                Actualizar_Solicitud_Proveedor_Expresion_Regular(dtExpReg_Certif_Solic)
            End If

        Catch ex As Exception
            DBServer.Errores_Create("Certificados.vb", DBServer.UsuarioConectado, ex.GetType().FullName, ex.Message, ex.StackTrace, "", "")
        End Try
    End Sub
    ''' <summary>Solicitud autom�tica de certificados</summary>
    ''' <remarks>Llamada desde: WebMethod/Certificados_SolicitudAutom�tica; Tiempo m�ximo: 0 sg </remarks>
    Public Sub Solicitud_Automatica_Certificados()
        Try
            'Primero de todo actualizamos las relaciones de los proveedores con los certificados por expresiones regulares
            Certificados_Por_Expresiones_Regulares()

            'Ahora publicamos
            Dim dsInfoCertificados As DataSet = ObtenerCertificadosSinSolicitar()

            If dsInfoCertificados.Tables("CERTIFICADOS").Rows.Count = 0 Then Exit Sub

            Dim dtCertificados As New DataTable
            dtCertificados.Columns.Add("TIPO_CERTIF", System.Type.GetType("System.Int32"))
            dtCertificados.Columns.Add("PROVE", System.Type.GetType("System.String"))
            dtCertificados.Columns.Add("INSTANCIA", System.Type.GetType("System.Int64"))
            dtCertificados.Columns.Add("IDCERTIFICADO", System.Type.GetType("System.Int64"))
            dtCertificados.Columns.Add("ENVIARMAIL", System.Type.GetType("System.Int32"))

            Dim drCertificado As DataRow
            For Each row As DataRow In dsInfoCertificados.Tables("CERTIFICADOS").Rows
                drCertificado = dtCertificados.NewRow

                With drCertificado
                    .Item("TIPO_CERTIF") = row("TIPO_CERTIF")
                    .Item("PROVE") = row("PROVE")
                    .Item("INSTANCIA") = 0
                    .Item("IDCERTIFICADO") = 0
                    .Item("ENVIARMAIL") = 1
                End With
                dtCertificados.Rows.Add(drCertificado)
            Next

            Solicitar_Renovar(dtCertificados, False, dsInfoCertificados.Tables(0).Rows(0)("PER"), dsInfoCertificados.Tables(0).Rows(0)("EMAIL"))
        Catch ex As Exception
            DBServer.Errores_Create("Certificados.vb", DBServer.UsuarioConectado, ex.GetType().FullName, ex.Message, ex.StackTrace, "", "")
        End Try
    End Sub
End Class