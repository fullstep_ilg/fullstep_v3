﻿Imports Fullstep.FSNLibrary.Encrypter
Imports System.ServiceModel
Imports System.Web.Script.Serialization

<Serializable()> _
Public Class Integracion
    Inherits Security

    Private WithEvents oFSIS As Fullstep.FSNLibraryCOM.CFSISService
    Public Sub New(ByRef dbserver As FSNDatabaseServer.Root, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, isAuthenticated)
    End Sub
    ''' <summary>
    ''' Función que nos dice si la integración está activa y en el sentido que le indicamos
    ''' </summary>
    ''' <param name="TablaIntegracion">Parametro opcional con la entidad a analizar</param>
    ''' <returns>Booleano</returns>
    ''' <remarks>Tiempo máximo 0 sec</remarks>
    Public Function HayIntegracion(ByVal TablaIntegracion As Integer, Optional ByVal Sentido As Short = SentidoIntegracion.SinSentido) As Boolean
        Authenticate()
        HayIntegracion = DBServer.HayIntegracion(TablaIntegracion, Sentido)
    End Function
    ''' <summary>
    ''' Indica si está activada la integración de una determinada tabla , para un erp determinado en los sentidos indicados
    ''' </summary>
    ''' <param name="sErp"></param>
    ''' <param name="TablaIntegracion"></param>
    ''' <param name="sentido1"></param>
    ''' <param name="Sentido2"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Protected Function HayIntegracionTablaErp(ByVal sErp As String, ByVal TablaIntegracion As TablasIntegracion, ByVal sentido1 As SentidoIntegracion, Optional ByVal Sentido2 As SentidoIntegracion = SentidoIntegracion.SinSentido) As Boolean
        Authenticate()
        Return DBServer.HayIntegracionTablaErp(sErp, TablaIntegracion, sentido1, Sentido2)
    End Function
    Public Function hayIntegracionPM(ByVal erp As String) As Boolean
        Authenticate()
        Return HayIntegracionTablaErp(erp, TablasIntegracion.PM, SentidoIntegracion.Salida, SentidoIntegracion.EntradaYSalida)
    End Function
    Public Function hayIntegracionEp(ByVal erp As String) As Boolean
        Authenticate()
        Return HayIntegracionTablaErp(erp, TablasIntegracion.PED_Aprov, SentidoIntegracion.Salida, SentidoIntegracion.EntradaYSalida)
    End Function
    Public Function hayIntegracionArticulos(ByVal erp As String) As Boolean
        Authenticate()
        Return HayIntegracionTablaErp(erp, TablasIntegracion.art4, SentidoIntegracion.Salida, SentidoIntegracion.EntradaYSalida)
    End Function
    Public Function hayIntegracionAdjudicaciones(ByVal erp As String) As Boolean
        Authenticate()
        Return HayIntegracionTablaErp(erp, TablasIntegracion.Adj, SentidoIntegracion.Salida, SentidoIntegracion.EntradaYSalida)
    End Function
    ''' <summary>
    ''' Compureba si un artículo está correctamente integrado para determinado erp.
    ''' </summary>
    ''' <param name="sArticulo"></param>
    ''' <param name="iErp"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function estaIntegradoArticulo(ByVal sArticulo As String, ByVal iErp As String) As Boolean
        Authenticate()
        Return DBServer.hayIntegracionArticuloErp(sArticulo, iErp)
    End Function
    ''' <summary>
    ''' Obtiene el id del erp asociado a una empresa sin org de compras
    ''' </summary>
    ''' <param name="iEmpresa"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function getErpFromEmpresa(ByVal iEmpresa As Integer) As String
        Authenticate()
        Return Me.DBServer.getErpFromEmpresa(iEmpresa)
    End Function
    ''' <summary>
    ''' id del erp asociado a empresa con organización de compras
    ''' </summary>
    ''' <param name="iempresa"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function getErpFromOrgCompras(ByVal iempresa As Integer) As String
        Authenticate()
        Return Me.DBServer.getErpFromOrgCompras(iempresa)
    End Function
    ''' <summary>
    ''' Obtiene el cÃ³digo del ERP a travÃ©s de su denominaciÃ³n.
    ''' </summary>
    ''' <param name="sDenERP">La denominaciÃ³n del ERP deseado.</param>
    ''' <returns>Integer con el cÃ³digo del ERP.</returns>
    ''' <remarks>Llamada desde: VisorIntegracion.InitializeRow.
    ''' Revisado por: aam (04/04/2016).
    ''' Tiempo mÃ¡ximo: menos de 1s.
    ''' </remarks>
    Public Function getErpFromDen(ByVal sDenERP As String) As String
        Authenticate()
        Return Me.DBServer.getErpFromDen(sDenERP)
    End Function
    ''' <summary>
    ''' obtiene el id de ERP de una organización de compras
    ''' </summary>
    ''' <param name="sOrgCompras"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function getErpFromOrgCompras(ByVal sOrgCompras As String) As String
        Authenticate()
        Return Me.DBServer.getErpFromOrgCompras(sOrgCompras)
    End Function
    ''' <summary>
    ''' Relanza integración de determinada entidad, identificada por su idlog y la asocia a un usuario
    ''' determinado
    ''' </summary>
    ''' <param name="idlog">id del log general</param>
    ''' <param name="user">Usuario que realiza el relanzamiento</param>
    ''' <returns>array con (id log gral insertado, id log de entidad insertado)</returns>
    ''' <remarks></remarks>
    Public Function relanzarIntegracion(ByVal idlog As Integer, ByVal user As String) As Integer()
        Authenticate()
        Return Me.DBServer.relanzarIntegracion(idlog, user)
    End Function
    ''' <summary>
    ''' Llama al Servicio de IntegraciÃ³n.
    ''' </summary>
    ''' <param name="iTabla">El ID de la tabla a integrar.</param>
    ''' <param name="lIDEntidad">El ID de la entidad a integrar.</param>
    ''' <param name="lIDPedido">OPCIONAL. El ID del pedido al que pertenece la entidad a integrar en caso de que el lIDEntidad sea 0.</param>
    ''' <param name="lIDOrden">OPCIONAL. El ID del orden de entrega al que pertenece la entidad a integrar en caso de que el lIDEntidad sea 0.</param>
    ''' <param name="iErpActual">OPCIONAL. El ERP actual en caso de que el lIDEntidad sea 0.</param>
    ''' <param name="lIDLineaRecep">OPCIONAL. El ID de la lÃ­nea de recepciÃ³n perteneciente a la recepciÃ³n a integrar en caso de que el lIDEntidad sea 0.</param>
    ''' <remarks>Llamada desde: CRecepcion.RegistrarRecepcion, CRecepciones.EliminarRecepcionesAlbaran, EP_Pedidos.GrabarModificacionLineaRecepcion,
    ''' Aprobacion.Aprobar, DetallePedido.ComprobarAprobacionDenegacionLinea, DetallePedido.ReabrirOrden, DetallePedido.CerrarOrden, DetallePedido.AnularOrden,
    ''' DetallePedido.ReemitirOrden, EmisionPedido.EmitirPedido, Recepcion.AnularLineaRecepcion, Seguimiento.Click y Service.TratarXML.
    ''' Revisado por: aam (04/04/2016).
    ''' Tiempo mÃ¡ximo: No corresponde ya que la llamada es asÃ­ncrona y llama a un servicio WCF externo a esta aplicaciÃ³n. 
    ''' </remarks>
    Public Sub LlamarFSIS(ByVal iTabla As Integer, ByVal lIDEntidad As Long, Optional ByVal lIDPedido As Long = 0,
                          Optional ByVal lIDOrden As Long = 0, Optional ByVal iErpActual As Integer = 0,
                          Optional ByVal lIDLineaRecep As Long = 0, Optional ByVal bEsRecep As Boolean = False)

        Try

            Dim sUrl, sNombre, sContrasenya As String
            Dim bPedAcepProvActivo As Boolean
            Dim iEst, iErp, iTipoInt As Integer
            Dim dr2 As DataSet
            Dim lIDLog As Long
            Dim iServiceBindingType, iServiceSecurityMode, iClientCredentialType, iProxyCredentialType As Integer
            sNombre = Nothing : sContrasenya = Nothing
            'Recuperamos el ID de la tabla de LOG.
            Select Case iTabla
                Case TablasIntegracion.PED_Aprov
                    dr2 = Me.ObtenerIdentificadoresPedido(lIDEntidad)
                    If Not dr2.Tables(0).Rows.Count = 0 Then 'Si esta comprobación no es cierta, no se integra ya que el origen no es 0 o 2.
                        lIDLog = DBNullToLong(dr2.Tables(0).Rows(0).Item("ID"))
                        iErp = DBNullToInteger(dr2.Tables(0).Rows(0).Item("ERP"))
                        iEst = DBNullToInteger(dr2.Tables(0).Rows(0).Item("EST"))
                        iTabla = DBNullToInteger(dr2.Tables(0).Rows(0).Item("TABLA"))
                        dr2.Dispose()
                        bPedAcepProvActivo = Me.ObtenerIntPedTrasAcep(iErp)
                        If IntegracionWCFActiva(iTabla, iErp) Then
                            If Not bPedAcepProvActivo Or (bPedAcepProvActivo And iEst >= TipoEstadoOrdenEntrega.AceptadoPorProveedor) Then
                                iTipoInt = Me.ObtenerTipoInt(iTabla, iErp)
                                If Not bEsRecep Or (bEsRecep And iEst = TipoEstadoOrdenEntrega.RecibidoYCerrado) Then
                                    If (iTipoInt = TipoDestinoIntegracion.WCF) Then
                                        oFSIS = New Fullstep.FSNLibraryCOM.CFSISService()
                                        ObtenerCredencialesWCF(TablasIntegracion.PED_Aprov, iErp, iServiceBindingType, iServiceSecurityMode, iClientCredentialType, iProxyCredentialType, sNombre, sContrasenya)
                                        sUrl = ObtenerRutaInstalacionFSIS()
                                        If sUrl <> "" Then
                                            oFSIS.Exportar(lIDLog, iTabla, sUrl, iServiceBindingType, iServiceSecurityMode, iClientCredentialType, iProxyCredentialType, sNombre, sContrasenya)
                                        End If
                                    End If
                                End If
                            End If
                        End If
                    End If
                Case TablasIntegracion.Rec_Aprov
                    If lIDEntidad = 0 Then
                        Me.ObtenerIdentificadoresRecepPorIdLinea(lIDLineaRecep, lIDEntidad, lIDOrden)
                    End If
                    dr2 = Me.ObtenerIdentificadoresRecepPorIDs(lIDEntidad, lIDOrden)
                    If Not dr2.Tables(0).Rows.Count = 0 Then 'Si esta comprobación no es cierta, no se integra ya que el origen no es 0 o 2.
                        lIDLog = DBNullToLong(dr2.Tables(0).Rows(0).Item("ID"))
                        iErp = DBNullToInteger(dr2.Tables(0).Rows(0).Item("ERP"))
                        dr2.Dispose()
                    End If
                    If IntegracionWCFActiva(iTabla, iErp) Then
                        iTipoInt = Me.ObtenerTipoInt(iTabla, iErp)
                        If (iTipoInt = TipoDestinoIntegracion.WCF) Then
                            oFSIS = New Fullstep.FSNLibraryCOM.CFSISService()
                            ObtenerCredencialesWCF(TablasIntegracion.Rec_Aprov, iErp, iServiceBindingType, iServiceSecurityMode, iClientCredentialType, iProxyCredentialType, sNombre, sContrasenya)
                            sUrl = ObtenerRutaInstalacionFSIS()
                            If sUrl <> "" Then
                                oFSIS.Exportar(lIDLog, iTabla, sUrl, iServiceBindingType, iServiceSecurityMode, iClientCredentialType, iProxyCredentialType, sNombre, sContrasenya)
                            End If
                        End If
                    End If
                Case TablasIntegracion.PM  'Solicitudes = Instancia --> a pedidos.
                    lIDLog = Me.ObtenerIDsWSInstancia(lIDEntidad, iErp) 'Esta función ya está validando si tenemos la integración
                    'tipo WCF activa para las solicitudes y el ERP en cuestión.
                    If iErp > 0 Then
                        oFSIS = New Fullstep.FSNLibraryCOM.CFSISService()
                        ObtenerCredencialesWCF(TablasIntegracion.PM, iErp, iServiceBindingType, iServiceSecurityMode, iClientCredentialType, iProxyCredentialType, sNombre, sContrasenya)
                        sUrl = ObtenerRutaInstalacionFSIS()
                        If sUrl <> "" Then
                            oFSIS.Exportar(lIDLog, iTabla, sUrl, iServiceBindingType, iServiceSecurityMode, iClientCredentialType, iProxyCredentialType, sNombre, sContrasenya)
                        End If
                    End If
            End Select
        Catch ex As Exception
            DBServer.Errores_Create("FULLSTEP INT. LlamarFSIS: Tabla: " & CStr(iTabla) & " Entidad:" & CStr(lIDEntidad), DBServer.UsuarioConectado, ex.GetType().FullName, ex.Message, ex.StackTrace, "", "")
        End Try
    End Sub
    ''' <summary>
    ''' Llama al Servicio de IntegraciÃ³n.
    ''' </summary>
    ''' <param name="iTabla">El ID de la tabla a integrar.</param>
    ''' <param name="lIDEntidad">El ID de la entidad a integrar.</param>
    ''' <param name="iErp">El ERP actual.</param>
    ''' <remarks>Llamada desde: VisorIntegracion.exportar.
    ''' Revisado por: aam (04/04/2015).
    ''' Tiempo mÃ¡ximo: No corresponde ya que la llamada es asÃ­ncrona y llama a un servicio WCF externo a esta aplicaciÃ³n. 
    ''' </remarks>
    Public Sub LlamarFSISReemisiones(ByVal iTabla As Integer, ByVal lIDEntidad As Long, ByVal iErp As Integer)
        Dim sUrl, sNombre, sContrasenya As String
        Dim iServiceBindingType, iServiceSecurityMode, iClientCredentialType, iProxyCredentialType As Integer
        sNombre = Nothing : sContrasenya = Nothing
        If iTabla = TablasIntegracion.Adj Then
            Dim colIDEntidad As New List(Of Long)
            colIDEntidad.Add(lIDEntidad)
            Me.LlamarFSISLista(iTabla, colIDEntidad, iErp)
        Else
            oFSIS = New Fullstep.FSNLibraryCOM.CFSISService()
            ObtenerCredencialesWCF(iTabla, iErp, iServiceBindingType, iServiceSecurityMode, iClientCredentialType, iProxyCredentialType, sNombre, sContrasenya)
            sUrl = ObtenerRutaInstalacionFSIS()
            If sUrl <> "" Then
                oFSIS.Exportar(lIDEntidad, iTabla, sUrl, iServiceBindingType, iServiceSecurityMode, iClientCredentialType, iProxyCredentialType, sNombre, sContrasenya)
            End If
        End If

    End Sub
    ''' <summary>
    ''' Llama al Servicio de IntegraciÃ³n en caso de que se necesite pasarle una lista de ids.
    ''' </summary>
    ''' <param name="iTabla">El ID de la tabla a integrar (entidad de integraciÃ³n).</param>
    ''' <param name="lLogIdsEntidad">La lista de identificadores a integrar (LOG_ENTIDAD.ID).</param>
    ''' <param name="iErpActual">El ERP actual.</param>
    ''' <remarks>Llamada desde: Integracion.LlamarFSISReemisiones, Adjudicaciones.TraspasarItemToERP, Adjudicaciones.TraspasarGrupoToERP, 
    ''' Adjudicaciones.BorrarGrupoDelERP, Adjudicaciones.BorrarItemDelERP.
    ''' Revisado por: aam (04/04/2015).
    ''' Tiempo mÃ¡ximo: No corresponde ya que la llamada es asÃ­ncrona y llama a un servicio WCF externo a esta aplicaciÃ³n. 
    ''' </remarks>
    Public Sub LlamarFSISLista(ByVal iTabla As Integer, ByVal lLogIdsEntidad As List(Of Long), ByVal iErpActual As Integer)
        Dim sUrl, sNombre, sContrasenya As String
        Dim iServiceBindingType, iServiceSecurityMode, iClientCredentialType, iProxyCredentialType As Integer
        sNombre = Nothing : sContrasenya = Nothing
        Select Case iTabla
            Case TablasIntegracion.Adj
                Dim oFSIS = New Fullstep.FSNLibraryCOM.CFSISService()
                ObtenerCredencialesWCF(TablasIntegracion.Adj, iErpActual, iServiceBindingType, iServiceSecurityMode, iClientCredentialType, iProxyCredentialType, sNombre, sContrasenya)
                sUrl = ObtenerRutaInstalacionFSIS()
                If sUrl <> "" Then
                    oFSIS.ExportarLista(lLogIdsEntidad.ToArray, iTabla, sUrl, iServiceBindingType, iServiceSecurityMode, iClientCredentialType, iProxyCredentialType, sNombre, sContrasenya)
                End If
        End Select
    End Sub
    ''' <summary>
    ''' Obtiene las credenciales (nombre de usuario y contraseÃ±a) de acceso al ERP del cliente para poder establecer una conexiÃ³n para la integraciÃ³n
    ''' vÃ­a WCF.
    ''' </summary>
    ''' <param name="iEntidadIntegracion">La entidad a integrar.</param>
    ''' <param name="iErp">El ERP de la entidad a integrar.</param>
    ''' <param name="iServiceBindingType">OUT: El tipo del binding del servicio (0, 1 y 3 = BasicHttpSecurityMode, todo lo demÃ¡s es SecurityMode).</param>
    ''' <param name="iServiceSecurityMode">OUT: El modo de seguridad del servicio (0 = None, 1 = Transport, 2 = Message, 3 = TransportWithMessageCredential, 4 = TransportCredentialOnly).</param>
    ''' <param name="iClientCredentialType">OUT: El tipo de credenciales del cliente (0 = None, 1 = Basic, 2 = Digest, 3 = Ntlm, 4 = Windows, 5 = Certificate).</param>
    ''' <param name="iProxyCredentialType">OUT: El tipo de credenciales del proxy (0 = None, 1 = Basic, 2 = Digest, 3 = Ntlm, 4 = Windows, 5 = Certificate).</param>
    ''' <param name="sNombre">OUT: El nombre de usuario.</param>
    ''' <param name="sContrasenya">OUT: La contraseÃ±a desencriptada del usuario.</param>
    ''' <remarks>Llamada desde: Integracion.LlamarFSIS, Integracion.LlamarFSISReemisiones, Integracion.LlamarFSISLista, EP_ValidacionesIntegracion.RemoteValidacionesWCF.
    ''' Revisado por: aam (04/04/2016).
    ''' Tiempo mÃ¡ximo: menos de 1s. 
    ''' </remarks>
    Public Sub ObtenerCredencialesWCF(ByVal iEntidadIntegracion As Integer, ByVal iErp As Integer,
                                      ByRef iServiceBindingType As Integer, ByRef iServiceSecurityMode As Integer,
                                      ByRef iClientCredentialType As Integer, ByRef iProxyCredentialType As Integer,
                                      ByRef sNombre As String, ByRef sContrasenya As String)
        Dim dr As DataSet
        Dim sFecContrasenya As String

        dr = DBServer.INT_ObtenerDatosConexionFSIS(iErp)
        If dr.Tables(0).Rows.Count = 0 Then
            iServiceBindingType = 0
            iServiceSecurityMode = 0
            iClientCredentialType = 0
            iProxyCredentialType = 0
        Else
            iServiceBindingType = IIf(IsDBNull(dr.Tables(0).Rows(0).Item("SERVICEBINDINGTYPE")), 0, dr.Tables(0).Rows(0).Item("SERVICEBINDINGTYPE").ToString)
            iServiceSecurityMode = IIf(IsDBNull(dr.Tables(0).Rows(0).Item("SERVICESECURITYMODE")), 0, dr.Tables(0).Rows(0).Item("SERVICESECURITYMODE").ToString)
            iClientCredentialType = IIf(IsDBNull(dr.Tables(0).Rows(0).Item("SERVICECLIENTCREDENTIALTYPE")), 0, dr.Tables(0).Rows(0).Item("SERVICECLIENTCREDENTIALTYPE").ToString)
            iProxyCredentialType = IIf(IsDBNull(dr.Tables(0).Rows(0).Item("SERVICEPROXYCREDENTIALTYPE")), 0, dr.Tables(0).Rows(0).Item("SERVICEPROXYCREDENTIALTYPE").ToString)
        End If
        dr.Reset()

        dr = DBServer.INT_ObtenerCredencialesWCF(iEntidadIntegracion, iErp)
        If dr.Tables(0).Rows.Count = 0 Then
            sNombre = ""
            sContrasenya = ""
        Else
            sNombre = IIf(IsDBNull(dr.Tables(0).Rows(0).Item("WCF_ORIG_USU")), "", dr.Tables(0).Rows(0).Item("WCF_ORIG_USU").ToString)
            sContrasenya = IIf(IsDBNull(dr.Tables(0).Rows(0).Item("WCF_ORIG_PWD")), "", dr.Tables(0).Rows(0).Item("WCF_ORIG_PWD").ToString)
            sFecContrasenya = IIf(IsDBNull(dr.Tables(0).Rows(0).Item("WCF_ORIG_FECPWD")), "", dr.Tables(0).Rows(0).Item("WCF_ORIG_FECPWD").ToString)
            If sFecContrasenya = "" Then
                sContrasenya = ""
            Else
                sContrasenya = Encrypter.Encrypt(sContrasenya, sNombre, False, TipoDeUsuario.Persona, sFecContrasenya).ToString
            End If
        End If
    End Sub

    ''' <summary>
    ''' Obtiene la ruta padre del servicio WCF de IntegraciÃƒÂ³n.
    ''' </summary>
    ''' <returns>La ruta padre del servicio WCF.</returns>
    ''' <remarks>Llamada desde: Integracion.LlamarFSIS, Integracion.LLamarFSISReemisiones, Integracion.LlamarFSISLista,
    ''' EP_ValidacionesIntegracion.RemoteValidacionesWCF.</remarks>
    Public Function ObtenerRutaInstalacionFSIS() As String
        Dim dr As DataSet

        dr = DBServer.INT_ObtenerRutaInstalacionFSIS()
        If IsNothing(dr.Tables(0)) Then
            Return ""
        Else
            Return DBNullToStr(dr.Tables(0).Rows(0).Item("RUTAXBAP"))
        End If
    End Function
    ''' <summary>
    ''' Obtiene los identificadores necesarios para el servicio de integraciÃ³n de un pedido.
    ''' </summary>
    ''' <param name="lIdOrden">Identificador del orden del pedido a integrar.</param>
    ''' <returns>Los campos ID y ERP de la tabla LOG_PEDIDO.</returns>
    ''' <remarks>Llamada desde: EmisionPedido.Emitir, Seguimiento.btnAceptarConfirm.Click.</remarks>
    Public Function ObtenerIdentificadoresPedido(ByVal lIdOrden As Long) As DataSet
        Return DBServer.INT_ObtenerIdentificadoresPedido(lIdOrden)

    End Function
    ''' <summary>
    ''' Obtiene los identificadores necesarios para el servicio de integraciÃ³n de una recepciÃ³n.
    ''' </summary>
    ''' <param name="lIdLineaRecep">Identificador de la linea de recepciÃ³n.</param>
    ''' <param name="cAccion">La acciÃ³n que se ha realizado.</param>
    ''' <returns>Los campos ID y ERP de la tabla LOG_PEDIDO_RECEP.</returns>
    ''' <remarks>
    ''' Lamada desde: EP_Pedidos.GrabarModificacionLineaRecepcion, Recepcion.AnularLineaRecepcion.
    ''' Tiempo mÃ¡ximo: menos de 1seg.
    ''' Revisado por: aam 30/01/2014
    ''' </remarks>
    Public Function ObtenerIdentificadoresRecep(ByVal lIdLineaRecep As Long, ByVal cAccion As Char) As DataSet
        Return DBServer.INT_ObtenerIdentificadoresRecep(lIdLineaRecep, cAccion)
    End Function
    ''' <summary>
    ''' Obtiene los identificadores necesarios para el servicio de integraciÃ³n de una recepciÃ³n.
    ''' </summary>
    ''' <param name="lIdPedRecep">Identificador de la recepciÃ³n.</param>
    ''' <param name="lIdOrden">Identificador de la orden de entrega.</param>
    ''' <returns>Los campos ID y ERP de la tabla LOG_PEDIDO_RECEP.</returns>
    ''' <remarks>
    ''' Llamada desde: Seguimiento.btnAceptarConfirm.Click, CRecepcion.RegistrarRecepcion, 
    ''' CRecepciones.EliminarRecepcionesAlbaran.
    ''' Tiempo mÃ¡ximo: menos de 1seg.
    ''' Revisado por: aam 30/01/2014.
    ''' </remarks>
    Public Function ObtenerIdentificadoresRecepPorIDs(ByVal lIdPedRecep As Long, ByVal lIdOrden As Long) As DataSet
        Dim ds As DataSet

        ds = DBServer.INT_ObtenerIdentificadoresRecepPorIDs(lIdPedRecep, lIdOrden)
        'Solo nos interesa los valores de la Ãºltima fila ya que son los que hay que integrar.
        For Each fila In ds.Tables(0).Rows
            ds.Tables(0).Rows(0).Item("ID") = DBNullToLong(fila("ID"))
            ds.Tables(0).Rows(0).Item("ERP") = DBNullToInteger(fila("ERP"))
        Next

        Return ds
    End Function
    '''<summary>FunciÃ³n que obtiene el tipo de destino de la integraciÃ³n.</summary>
    '''<param name="iEntidad">La entidad a integrar.</param>
    '''<param name="iErp">El ERP actual.</param>
    '''<returns>Integer con el tipo de destino de la integraciÃ³n.</returns>
    '''<remarks>Llamada desde: Integracion.ObtenerTipoInt.</remarks>
    Public Function ObtenerTipoInt(ByVal iEntidad As Integer, ByVal iErp As Integer) As Integer
        Dim dr As DataSet

        dr = DBServer.INT_ObtenerTipoInt(iEntidad, iErp)
        If dr.Tables(0).Rows.Count = 0 Then
            Return 0
        Else
            Return DBNullToInteger(dr.Tables(0).Rows(0).Item("DESTINO_TIPO"))
        End If
    End Function
    ''' <summary>
    ''' FunciÃ³n que obtiene el valor del parÃ¡metro IntPedTrasAcep. Este parÃ¡metro de integraciÃ³n indica que se integran
    ''' los pedidos despuÃ©s de que se aceptenpor parte del proveedor (1) o como se ha hecho siempre en FULLSTEP (0).
    ''' </summary>
    ''' <param name="iErp">El ERP actual.</param>
    ''' <returns>Booleano con el valor del parÃ¡metro de integraciÃ³n.</returns>
    ''' <remarks>
    ''' Llamada desde: Integracion.LlamarFSIS.
    ''' Revisado por aam (04/04/2016). 
    ''' Tiempo mÃ¡ximo: menos de 1s.
    '''</remarks>
    Public Function ObtenerIntPedTrasAcep(ByVal iErp As Integer) As Boolean
        Dim dr As DataSet

        dr = DBServer.INT_ObtenerIntPedTrasAcep(iErp)
        If dr.Tables(0).Rows.Count = 0 Then
            Return False
        Else
            Return DBNullToBoolean(dr.Tables(0).Rows(0).Item("INT_PED_TRAS_ACEP"))
        End If
    End Function
    ''' <summary>
    ''' Indica si la integración tipo WCF está activa o no para la entidad deseada y para al menos un ERP.
    ''' </summary>
    ''' <param name="iEntidad">La entidad a comprobar si está activa su integración.</param>
    ''' <param name="iErp">El ERP actual.</param>
    ''' <returns>True si la entidad está activa y False en caso contrario.</returns>
    ''' <remarks>
    ''' Llamada desde Integracion.LlamarFSIS, Adjudicaciones.BorrarItemDeERP, 
    ''' Adjudicaciones.TraspasarItemToERP, Adjudicaciones.TraspasarGrupoToERP, Adjudicaciones.BorrarGrupoDeERP.
    ''' Tiempo mÃ¡ximo: Menos de 1s.
    ''' Revisado por: aam 04/04/2016.
    ''' </remarks>
    Public Function IntegracionWCFActiva(ByVal iEntidad As Integer, ByVal iErp As Integer) As Boolean
        Dim dr As DataSet

        dr = DBServer.INT_IntegracionWCFActiva(iEntidad, iErp)
        If dr.Tables(0).Rows.Count > 0 Then
            dr.Dispose()
            Return True
        Else
            dr.Dispose()
            Return False
        End If

    End Function
    ''' <summary>
    ''' Valida los datos del WebService de PM para llamar o no a FSIS y obtiene el ID de la instancia en las tablas de LOG
    ''' (LOG_INSTANCIA) y el ERP actual.
    ''' </summary>
    ''' <param name="lIDInstancia">El ID de la instancia de la solicitud actual.</param>
    ''' <param name="iErp">OUT: El ERP actual.</param> 
    ''' <remarks>Llamada desde: Integracion.LlamarFSIS; Tiempo: 0,1s.</remarks>
    Public Function ObtenerIDsWSInstancia(ByVal lIDInstancia As Long, Optional ByRef iErp As Integer = 0) As Long
        Dim dr As DataSet
        Dim lIDLogInstancia As Long

        dr = DBServer.INT_DatosWSValidosFSIS(lIDInstancia)
        If dr.Tables(0).Rows.Count > 0 Then
            lIDLogInstancia = IIf(IsDBNull(dr.Tables(0).Rows(0).Item("ID")), 0, dr.Tables(0).Rows(0).Item("ID"))
            iErp = IIf(IsDBNull(dr.Tables(0).Rows(0).Item("ERP")), 0, dr.Tables(0).Rows(0).Item("ERP"))
        End If
        dr.Dispose()

        Return lIDLogInstancia
    End Function
    '''<summary>FunciÃ³n que obtiene los IDs necesarios de una recepciÃ³n (ID_ORDEN, ID_PEDIDO y ID_PEDIDO_RECEP).</summary>
    '''<param name="lIdLineaRecep">El ID de la lÃ­nea de la recepciÃ³n</param>
    '''<param name="lIdPedRecep">OUT: El ID de la recepciÃ³n.</param>
    '''<param name="lIdOrden">OUT: El ID de la orden de entrega.</param>
    '''<remarks>Llamada desde: Integracion.LlamarFSIS.</remarks>
    Public Sub ObtenerIdentificadoresRecepPorIdLinea(ByVal lIdLineaRecep As Long, ByRef lIdPedRecep As Long, ByRef lIdOrden As Long)
        Dim dr As DataSet

        dr = DBServer.INT_ObtenerIdentificadoresRecepPorIdLinea(lIdLineaRecep)
        If dr.Tables(0).Rows.Count > 0 Then
            lIdPedRecep = IIf(IsDBNull(dr.Tables(0).Rows(0).Item("ID_PEDIDO_RECEP")), 0, dr.Tables(0).Rows(0).Item("ID_PEDIDO_RECEP"))
            lIdOrden = IIf(IsDBNull(dr.Tables(0).Rows(0).Item("ID_ORDEN")), 0, dr.Tables(0).Rows(0).Item("ID_ORDEN"))
        End If
        dr.Dispose()
    End Sub
    '''<summary>Función que obtiene los IDs necesarios del borrado de una Adjudicación (ID_TABLA).</summary>
    '''<param name="sAnyo">El año en el que se realizó la adjudicación.</param>
    '''<param name="sGMN1">El nivel 1 de la estructura de materiales perteneciente al proceso.</param>
    '''<param name="sProce">El ID del proceso.</param>
    '''<param name="iErp">El ERP del cliente.</param>
    '''<returns>Lista con los identificadores necesarios.</returns>
    '''<remarks>Llamada desde: Adjudicaciones.BorrarGrupoDeERP, Adjudicaciones.BorrarItemDeERP.</remarks>
    Public Function ObtenerIdentificadoresBorradosADJ(ByVal sAnyo As Short, ByVal sGMN1 As String, ByVal sProce As Integer, ByVal iErp As Integer) As List(Of Long)
        Dim colListaIds As New List(Of Long)
        Dim dr As DataSet
        Dim lId As Long

        dr = DBServer.INT_ObtenerIdentificadoresBorradosADJ(sAnyo, sGMN1, sProce, iErp)
        If dr.Tables(0).Rows.Count > 0 Then
            For Each oFila In dr.Tables(0).Rows
                lId = IIf(IsDBNull(oFila("ID_TABLA")), 0, oFila("ID_TABLA"))
                If lId > 0 Then
                    colListaIds.Add(lId)
                End If
            Next
        End If

        dr.Dispose()
        Return colListaIds
    End Function
    ''' <summary>
    ''' FunciÃ³n que obtiene el valor del parÃ¡metro TRASPASO_ADJ_ERP. Este parÃ¡metro de general indica que se puede realizar el traspaso de la adjudicaciÃ³n al ERP.
    ''' </summary>
    ''' <returns>DataSet con el valor del parÃ¡metro general.</returns>
    ''' <remarks>Llamada desde: TraspasoAdjudicaciones.ValidarADJ.</remarks>
    Public Function HayTraspasoADJERP() As Boolean
        Dim dr As DataSet
        Dim bTraspasoADJERPActivo As Boolean

        dr = DBServer.INT_HayTraspasoADJERP()
        If dr.Tables(0).Rows.Count > 0 Then
            bTraspasoADJERPActivo = modUtilidades.DBNullToBoolean(dr.Tables(0).Rows(0).Item("TRASPASO_ADJ_ERP"))
        End If

        dr.Dispose()
        Return bTraspasoADJERPActivo
    End Function
    ''' <summary>
    ''' Llama al servicio de integración de FSIS para las validacines de las adjudicaciones.
    ''' </summary>
    ''' <param name="oAdjReducida">La adjudicación reducida dependiendo del nivel desde el que se haya realizado el traspaso. Única y exclusivamente validamos
    ''' los datos que se estén traspasando.</param>
    ''' <param name="iNivel">El nivel desde el cual se esté realizando el traspaso: 0 = Empresa, 1 = Proveedor, 2 = Grupo y 3 = Ítem.</param>
    ''' <param name="iErp">El ERP actual.</param> 
    ''' <returns>Devuelve el mensaje de error en caso de producirse o cadena vacÃ­a si todo va bien.</returns>
    ''' <remarks>
    ''' Método utilizado en FSNServer.App_Classes.EP.EP_ValidacionesIntegracion.
    ''' </remarks>
    ''' <revision>aam 25/08/2016.</revision>
    Public Function ValidacionesWCF(oAdjReducida As Fullstep.FSNServer.Adjudicacion, ByVal iNivel As Integer, ByVal iERP As Integer) As String
        Dim serializer As New JavaScriptSerializer()
        Dim sRuta, sNombre, sContrasenya, strAdjSerializada As String
        Dim iServiceBindingType, iServiceSecurityMode, iClientCredentialType, iProxyCredentialType As Integer

        sNombre = String.Empty : sContrasenya = String.Empty
        Try
            strAdjSerializada = serializer.Serialize(oAdjReducida)
            ObtenerCredencialesWCF(TablasIntegracion.Adj, iERP, iServiceBindingType, iServiceSecurityMode, iClientCredentialType, iProxyCredentialType, sNombre, sContrasenya)
            sRuta = ObtenerRutaInstalacionFSIS()
            sRuta = Split(sRuta, "Exportar")(0) & "Validaciones.svc"

            Dim oValidacionesAdjudicacionRequest As ValidacionesAdjudicacionRequest
            Dim oValidacionesAdjudicacionResponse As ValidacionesAdjudicacionResponse

            oValidacionesAdjudicacionRequest = New ValidacionesAdjudicacionRequest(strAdjSerializada, iNivel, iERP)

            Dim myChannelFactory As ChannelFactory(Of IValidaciones)
            Dim myEndpoint As New EndpointAddress(sRuta)

            Select Case iServiceBindingType
                Case 1 'WSHttpBinding
                    Dim iSecurityMode As SecurityMode = iServiceSecurityMode
                    Dim myBinding As New WSHttpBinding(iSecurityMode)
                    With myBinding
                        .Security.Transport.ClientCredentialType = iClientCredentialType
                        .Security.Transport.ProxyCredentialType = iProxyCredentialType
                    End With
                    myChannelFactory = New ChannelFactory(Of IValidaciones)(myBinding, myEndpoint)
                Case Else 'BasicHttpBinding
                    Dim iBasicSecurityMode As BasicHttpSecurityMode = iServiceSecurityMode
                    Dim myBinding As New BasicHttpBinding(iBasicSecurityMode)
                    With myBinding
                        .Security.Transport.ClientCredentialType = iClientCredentialType
                        .Security.Transport.ProxyCredentialType = iProxyCredentialType
                    End With
                    myChannelFactory = New ChannelFactory(Of IValidaciones)(myBinding, myEndpoint)
            End Select

            With (myChannelFactory.Credentials)
                Select Case iClientCredentialType
                    Case HttpClientCredentialType.Windows
                        With .Windows.ClientCredential
                            .UserName = sNombre
                            .Password = sContrasenya
                        End With
                    Case HttpClientCredentialType.Basic
                        With .UserName
                            .UserName = sNombre
                            .Password = sContrasenya
                        End With
                    Case HttpClientCredentialType.Certificate
                        .ClientCertificate.SetCertificate(System.Security.Cryptography.X509Certificates.StoreLocation.LocalMachine,
                                                          System.Security.Cryptography.X509Certificates.StoreName.My,
                                                          System.Security.Cryptography.X509Certificates.X509FindType.FindBySubjectName, "client")
                End Select
            End With

            Dim wcfProxyClient As IValidaciones = myChannelFactory.CreateChannel()
            oValidacionesAdjudicacionResponse = wcfProxyClient.ValidacionesAdjudicacion(oValidacionesAdjudicacionRequest)
            myChannelFactory.Close()

            Return oValidacionesAdjudicacionResponse.ValidacionesAdjudicacionResult
        Catch ex As Exception
            Return String.Empty
        End Try
    End Function
End Class