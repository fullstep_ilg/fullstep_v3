﻿Imports System.Configuration
Imports System.Xml
Imports System.Runtime.InteropServices
Imports FSNWinServiceBatch.Encrypter
Imports System.Data.SqlClient
Imports System.Threading
Imports Fullstep

Public Class Service
    Private WithEvents temporizador As Timers.Timer
    Private m_sUsu As String
    Private m_sPassword As String
    Private m_FSNServer As FSNServer.Root

#Region "Inicio"
#If DEBUG Then
    Public Sub EmpezarDebug()
        Me.OnStart(Nothing)
    End Sub
#End If
    ''' <summary>
    ''' Inicio del servicio. 
    ''' </summary>
    ''' <remarks>Llamada desde=Service1.vb; Tiempo máximo=0seg.</remarks>
    Protected Overrides Sub OnStart(ByVal args() As String)
        m_FSNServer = New FSNServer.Root(True)

        LeerParametrosEjecucionServicio_QA_Puntuaciones_Calculo()
        LeerParametrosEjecucionServicio_QA_NoconformidadesNotificacionProximidadFin()
        LeerParametrosEjecucionServicio_QA_Certificados_NotificarExpirados()
        LeerParametrosEjecucionServicio_FSGA_NotificacionInicioTarea()
        LeerParametrosEjecucionServicio_INT_Monitorizacion()
        LeerParametrosEjecucionServicio_CN_NotificacionActividad()
        LeerParametrosEjecucionServicio_CN_Historico()
        LeerParametrosEjecucionServicio_QA_Certificados_Solicitud()
        LeerParametrosEjecucionServicio_QA_Certificados_Despublicacion()
        LeerParametrosEjecucionServicio_QA_Certificados_NotificarRevisados()
        LeerParametrosEjecucionServicio_QA_Certificados_PorExpresionesRegulares()
        LeerParametrosEjecucionServicio_PM_Solicitudes_NotificarExpiradas()
        LeerParametrosEjecucionServicio_SM_NotificarDisponibleNegativo()
        LeerParametrosEjecucionServicio_EP_NotificarLineasPedidoBorradas()
        ArrancarTemporizador()
    End Sub
#End Region
#Region "temporizador"
    ''' <summary>
    ''' Arrancar el temporizador
    ''' </summary>
    ''' <remarks>Llamada desde=Service1.OnStart; Tiempo máximo=0seg.</remarks>
    Private Sub ArrancarTemporizador()
        temporizador = New Timers.Timer
        temporizador.AutoReset = True

        'Cada minuto se comprueba si se ha cumplido el periodo indicado en diasEjecucion para su ejecucion (mensual)
        temporizador.Interval = (1000 * 60)

        temporizador.Enabled = True

        If m_bActivoServicioCalculoPuntuaciones Then ThreadPool.QueueUserWorkItem(New WaitCallback(AddressOf EjecucionServicio_QA_Puntuaciones_Calculo))
        If m_bActivoServicioNotificacionProximidad Then ThreadPool.QueueUserWorkItem(New WaitCallback(AddressOf EjecucionServicio_QA_NoconformidadesNotificacionProximidadFin))
        If m_bActivoServicioNotificacionCertificados Then ThreadPool.QueueUserWorkItem(New WaitCallback(AddressOf EjecucionServicio_QA_Certificados_NotificarExpirados))
        If m_bActivoServicioNotificacionInicioTareaFSGA Then ThreadPool.QueueUserWorkItem(New WaitCallback(AddressOf EjecucionServicio_FSGA_NotificacionInicioTarea))
        If m_bActivoServicioMonitorizaInt Then ThreadPool.QueueUserWorkItem(New WaitCallback(AddressOf EjecucionServicio_INT_Monitorizacion))
        If ActivoServicioNotificacionCN Then ThreadPool.QueueUserWorkItem(New WaitCallback(AddressOf EjecucionServicio_CN_NotificacionActividad))
        If ActivoServicioHistoricoCN Then ThreadPool.QueueUserWorkItem(New WaitCallback(AddressOf EjecucionServicio_CN_Historico))
        If ActivoServicio_SolicitudAutomaticaCertificados Then ThreadPool.QueueUserWorkItem(New WaitCallback(AddressOf EjecucionServicio_QA_Certificados_Solicitud))
        If ActivoServicio_Certificados_DespublicarAutomatica Then ThreadPool.QueueUserWorkItem(New WaitCallback(AddressOf EjecucionServicio_QA_Certificados_Despublicacion))
        If ActivoServicio_Certificados_NotificarRevisados Then ThreadPool.QueueUserWorkItem(New WaitCallback(AddressOf EjecucionServicio_QA_Certificados_NotificarRevisados))
        If m_bActivoServicio_Certificados_Por_Expresiones_Regulares Then ThreadPool.QueueUserWorkItem(New WaitCallback(AddressOf EjecucionServicio_QA_Certificados_PorExpresionesRegulares))
        If ActivoServicio_Solicitudes_AvisoExpiracion Then ThreadPool.QueueUserWorkItem(New WaitCallback(AddressOf EjecucionServicio_PM_Solicitudes_NotificarExpiradas))
        If ActivoServicio_SMNotifDisponibleNegativo Then ThreadPool.QueueUserWorkItem(New WaitCallback(AddressOf EjecucionServicio_SM_NotificarDisponibleNegativo))
        If ActivoServicioNotificacion_EPNotificarLineasPedidoBorradas Then ThreadPool.QueueUserWorkItem(New WaitCallback(AddressOf EjecucionServicio_EP_NotificarLineasPedidoBorradas))

        temporizador.Start()
    End Sub
    ''' <summary>
    ''' Cuando pasa el intervalo de tiempo indicado para el Temporizador debe ejecutar los diferentes servicios
    ''' </summary>
    ''' <param name="sender">objeto de sistema</param>
    ''' <param name="e">evento de sistema</param>
    ''' <remarks>Llamada desde: sistema; Tiempo máximo: 0,3</remarks>
    Private Sub temporizador_Elapsed(ByVal sender As Object, ByVal e As System.Timers.ElapsedEventArgs) Handles temporizador.Elapsed
        If m_bActivoServicioCalculoPuntuaciones Then ThreadPool.QueueUserWorkItem(New WaitCallback(AddressOf EjecucionServicio_QA_Puntuaciones_Calculo))
        If m_bActivoServicioNotificacionProximidad Then ThreadPool.QueueUserWorkItem(New WaitCallback(AddressOf EjecucionServicio_QA_NoconformidadesNotificacionProximidadFin))
        If m_bActivoServicioNotificacionCertificados Then ThreadPool.QueueUserWorkItem(New WaitCallback(AddressOf EjecucionServicio_QA_Certificados_NotificarExpirados))
        If m_bActivoServicioNotificacionInicioTareaFSGA Then ThreadPool.QueueUserWorkItem(New WaitCallback(AddressOf EjecucionServicio_FSGA_NotificacionInicioTarea))
        If m_bActivoServicioMonitorizaInt Then ThreadPool.QueueUserWorkItem(New WaitCallback(AddressOf EjecucionServicio_INT_Monitorizacion))
        If ActivoServicioNotificacionCN Then ThreadPool.QueueUserWorkItem(New WaitCallback(AddressOf EjecucionServicio_CN_NotificacionActividad))
        If ActivoServicioHistoricoCN Then ThreadPool.QueueUserWorkItem(New WaitCallback(AddressOf EjecucionServicio_CN_Historico))
        If ActivoServicio_SolicitudAutomaticaCertificados Then ThreadPool.QueueUserWorkItem(New WaitCallback(AddressOf EjecucionServicio_QA_Certificados_Solicitud))
        If ActivoServicio_Certificados_DespublicarAutomatica Then ThreadPool.QueueUserWorkItem(New WaitCallback(AddressOf EjecucionServicio_QA_Certificados_Despublicacion))
        If ActivoServicio_Certificados_NotificarRevisados Then ThreadPool.QueueUserWorkItem(New WaitCallback(AddressOf EjecucionServicio_QA_Certificados_NotificarRevisados))
        If m_bActivoServicio_Certificados_Por_Expresiones_Regulares Then ThreadPool.QueueUserWorkItem(New WaitCallback(AddressOf EjecucionServicio_QA_Certificados_PorExpresionesRegulares))
        If ActivoServicio_Solicitudes_AvisoExpiracion Then ThreadPool.QueueUserWorkItem(New WaitCallback(AddressOf EjecucionServicio_PM_Solicitudes_NotificarExpiradas))
        If ActivoServicio_SMNotifDisponibleNegativo Then ThreadPool.QueueUserWorkItem(New WaitCallback(AddressOf EjecucionServicio_SM_NotificarDisponibleNegativo))

        If ActivoServicioNotificacion_EPNotificarLineasPedidoBorradas Then ThreadPool.QueueUserWorkItem(New WaitCallback(AddressOf EjecucionServicio_EP_NotificarLineasPedidoBorradas))

    End Sub
#End Region
#Region "Lineas de Pedido Borradas"
#Region "Variables Lineas de Pedido Borradas"
    Private File_LPB As String
    Private ActivoServicioNotificacion_EPNotificarLineasPedidoBorradas As Boolean = True

    Private modoEjecucion_Notificaciones_LPB As String '[M(minutos),H(horas)]
    Private PeriodoNotificaciones_LPB As String

    Private diaArranqueNotif_LPB As Date
    Private HoraEjecucionNotif_LPB As String
    Private EnEjecucionServicioNotificacionLPB As Boolean = False
#End Region

    ''' <summary>
    ''' Se indica el path del fichero log Lineas de pedido borradas, los dias de repeticion del servicio. La 1ª 
    ''' vez se lanza el calculo y los dias sucesivos se comprobara si le toca o no ejecutar el webservice 
    ''' a traves del parametro DiasEjecucion
    ''' </summary>
    ''' <remarks>Llamada desde=Service1.OnStart; Tiempo máximo=0seg.</remarks>
    Private Sub LeerParametrosEjecucionServicio_EP_NotificarLineasPedidoBorradas()
        Dim sPath As String
        sPath = ConfigurationManager.AppSettings("Path")

        File_LPB = sPath & "\LPB_Notif.txt"

        If ConfigurationManager.AppSettings("MODO_EJECUCION_NOTIFICACIONES_LPB") = "" _
        OrElse (ConfigurationManager.AppSettings("HORA_EJECUCION_NOTIFICACIONES_LPB") = "") _
        OrElse (Not IsDate(ConfigurationManager.AppSettings("DIA_ARRANQUE_SERVICIO_NOTIFICACIONES_LPB"))) Then
            ActivoServicioNotificacion_EPNotificarLineasPedidoBorradas = False
        Else
            ''Configuracion del modo de ejecucion del servicio.
            ''Toma los parametros de la configuracion.
            modoEjecucion_Notificaciones_LPB = ConfigurationManager.AppSettings("MODO_EJECUCION_NOTIFICACIONES_LPB")
            PeriodoNotificaciones_LPB = ConfigurationManager.AppSettings("PERIODO_NOTIFICACIONES_LPB")
            HoraEjecucionNotif_LPB = ConfigurationManager.AppSettings("HORA_EJECUCION_NOTIFICACIONES_LPB")
            diaArranqueNotif_LPB = ConfigurationManager.AppSettings("DIA_ARRANQUE_SERVICIO_NOTIFICACIONES_LPB")
        End If
    End Sub
    ''' <summary>
    ''' Funcion que calcula la proxima ejecucion del servicio.
    ''' </summary>
    ''' <returns>Fecha y hora de la proxima ejecucion del servicio</returns>
    ''' <remarks>Llamada desde=propia pagina; Tiempo máximo=0,4seg.</remarks>
    Private Function FechaProximaEjecucionServicio_EP_NotificarLineasPedidoBorradas() As Date
        Dim fechaFichero As Date
        Dim intervaloNotificacion As String
        'If no existe fichero de notificaciones LPB
        If Not System.IO.File.Exists(File_LPB) Then
            FechaProximaEjecucionServicio_EP_NotificarLineasPedidoBorradas = CDate(diaArranqueNotif_LPB & " " & HoraEjecucionNotif_LPB)
        Else
            'fecha anterior ejecucion con la hora de ejecucion del servicio
            fechaFichero = System.IO.File.GetLastWriteTime(File_LPB)
            FechaProximaEjecucionServicio_EP_NotificarLineasPedidoBorradas = fechaFichero

            Select Case modoEjecucion_Notificaciones_LPB
                Case "M" ' Minutos
                    intervaloNotificacion = "n"
                Case "H" 'Horas
                    intervaloNotificacion = "h"
                Case Else
                    intervaloNotificacion = "n"
            End Select
            'suma el periodo en horas a la fecha de la ultima ejecucion
            FechaProximaEjecucionServicio_EP_NotificarLineasPedidoBorradas = DateAdd(intervaloNotificacion, CType(PeriodoNotificaciones_LPB, Double), FechaProximaEjecucionServicio_EP_NotificarLineasPedidoBorradas)
            FechaProximaEjecucionServicio_EP_NotificarLineasPedidoBorradas = New Date(Year(FechaProximaEjecucionServicio_EP_NotificarLineasPedidoBorradas), Month(FechaProximaEjecucionServicio_EP_NotificarLineasPedidoBorradas), Day(FechaProximaEjecucionServicio_EP_NotificarLineasPedidoBorradas), Hour(FechaProximaEjecucionServicio_EP_NotificarLineasPedidoBorradas), Minute(FechaProximaEjecucionServicio_EP_NotificarLineasPedidoBorradas), 0)
        End If
    End Function
    ''' <summary>
    ''' Proceso que comprueba si tiene que lanzar el webService o no. Se comprueba diariamente si en el fichero que se ha escrito le toca ejecutarse o no al webservice
    ''' Cuando ejecuta el webService creo un fichero log que nos indica cuando se ha ejecutado por ultima vez el webService.
    ''' </summary>
    ''' <remarks>Llamada desde=ArrancarTemporizador       temporizador_Elapsed; Tiempo maximo=0,2seg.</remarks>
    Private Function EjecucionServicio_EP_NotificarLineasPedidoBorradas() As Boolean
        EjecucionServicio_EP_NotificarLineasPedidoBorradas = True
        Dim dFechaHora As Date
        Dim dFechaHoraFichero As Date = System.IO.File.GetLastWriteTime(File_LPB)
        dFechaHora = FechaProximaEjecucionServicio_EP_NotificarLineasPedidoBorradas()
        If DateDiff("n", Now(), dFechaHora) <= 0 Then
            'Decidimos borrar y escribir el fichero antes de la ejecución del servicio propiamente dicho para que si el servicio tarda más de lo debido
            'el archivo tenga fecha de modificación actual y asi no lance por segunda vez el servicio, evitando mails duplicados.
            'Borrar Fichero
            BorrarFicheroServicio_EP_NotificarLineasPedidoBorrada()
            'Escribir Fichero log de puntuaciones, para indicarle cuando se realizo el calculo por ultima vez
            EscribirFicheroServicio_EP_NotificarLineasPedidoBorrada(Now(), dFechaHora, dFechaHoraFichero)
            'LlamarWebService
            EP_NotificarLineasPedidoBorrada()
        End If
        EjecucionServicio_EP_NotificarLineasPedidoBorradas = False
    End Function
    Private Sub EP_NotificarLineasPedidoBorrada()
        Dim oOrden As FSNServer.COrden = m_FSNServer.Get_Object(GetType(FSNServer.COrden))
        oOrden.NotificarLineasBorradas()
    End Sub
    ''' <summary>
    ''' Borra el fichero log para posteriormente volver a escribirlo
    ''' Escribe mensajes en el event viewer para hacer seguimiento de instalacion
    ''' </summary>
    ''' <remarks>Llamada desde=EjecucionServicioCalculoPuntuaciones; Tiempo máximo=0seg.</remarks>
    Private Sub BorrarFicheroServicio_EP_NotificarLineasPedidoBorrada()
        System.IO.File.Delete(File_LPB)
    End Sub
    ''' <summary>
    ''' Escribe un fichero log para indicarnos cuando se ha ejecutado el webservice
    ''' Escribe mensajes en el event viewer para hacer seguimiento de instalacion
    ''' </summary>
    ''' <param name="fechaActual">Fecha actual del sistema</param>
    ''' <param name="FechaEjecucion">Fecha ejecucion servicio</param>
    ''' <param name="fechaFichero">Fecha creacion del fichero</param>
    ''' <remarks>Llamada desde=EjecucionServicioCalculoPuntuaciones; Tiempo máximo=0seg.</remarks>
    Private Sub EscribirFicheroServicio_EP_NotificarLineasPedidoBorrada(ByVal fechaActual As Date, ByVal FechaEjecucion As Date, ByVal fechaFichero As Date)
        Dim str = "Fichero de notificaciones de Líneas de Pedido Borradas network. "
        Dim oFileW As System.IO.StreamWriter
        oFileW = New System.IO.StreamWriter(File_LPB, True)
        oFileW.WriteLine(str)
        oFileW.WriteLine("Fecha Actual = " & fechaActual)
        oFileW.WriteLine("Fecha Ejecucion = " & FechaEjecucion)
        oFileW.WriteLine("Fecha Fichero = " & fechaFichero)
        oFileW.Close()
    End Sub
#End Region

#Region "Calculo Puntuaciones"
#Region "Variables Calculo Puntuaciones"
    'Service calculo Puntuaciones
    Private m_sFile_CP As String
    Private m_diasEjecucion_CP As Double
    Private m_modoEjecucion_CP As String '(D, S, M)
    Private m_PeriodoDiario_CP As String ' (1..31)
    'Semanal
    Private m_PeriodoSemanal_CP As String '(1..70)
    Private m_DiaPeriodoSemanal_CP As String '(0...7) vacio, lunes, martes...domingo
    'Mensual
    Private m_PeriodoMensual_CP As String '(1..12)
    Private m_ModoMensual_CP As String '(1 o 2)
    Private m_DiaMes_CP As String '(1..31)
    Private m_PosicionDiaMensual_CP As String '(1,2,3,4) (1er, 2º, 3º, 4º)
    Private m_DiaPeriodoMensual_CP As String '(1...7) (lunes...domingo)
    'Configuracion Fecha comienzo y hora ejecucion servicio
    Private m_diaArranque_CP As Date
    Private m_horaEjecucion_CP As Date
    Private m_fechaProximaEjecucion_CP As Date
    'Si no se introduce modo de ejecución, hora de ejecución o día de arranque, que no se ejecute 
    Private m_bActivoServicioCalculoPuntuaciones As Boolean = True
    Private EnEjecucionServicioServicioCalculoPuntuaciones As Boolean = False
#End Region
    ''' <summary>
    ''' Se indica el path del fichero log CalculoPuntuaciones, los dias de repeticion del servicio. La 1ª 
    ''' vez se lanza el calculo y los dias sucesivos se comprobara si le toca o no ejecutar el webservice 
    ''' a traves del parametro DiasEjecucion
    ''' </summary>
    ''' <remarks>Llamada desde=Service1.OnStart; Tiempo máximo=0seg.</remarks>
    Private Sub LeerParametrosEjecucionServicio_QA_Puntuaciones_Calculo()
        Dim sPath As String

        sPath = ConfigurationManager.AppSettings("Path")

        m_sFile_CP = sPath & "\CalculoPuntuaciones.txt"

        If ConfigurationManager.AppSettings("MODO_EJECUCION") = "" _
        OrElse (Not IsDate(ConfigurationManager.AppSettings("HORA_EJECUCION"))) _
        OrElse (Not IsDate(ConfigurationManager.AppSettings("DIA_ARRANQUE_SERVICIO"))) Then
            m_bActivoServicioCalculoPuntuaciones = False
        Else
            m_diasEjecucion_CP = ConfigurationManager.AppSettings("DiasEjecucion")

            ''Configuracion del modo de ejecucion del servicio.
            ''Toma los parametros de la configuracion.
            m_modoEjecucion_CP = ConfigurationManager.AppSettings("MODO_EJECUCION")
            Select Case m_modoEjecucion_CP
                Case "D"
                    m_PeriodoDiario_CP = ConfigurationManager.AppSettings("PERIODO_DIARIO")
                Case "S"
                    m_PeriodoSemanal_CP = ConfigurationManager.AppSettings("PERIODO_SEMANAL")
                    m_DiaPeriodoSemanal_CP = ConfigurationManager.AppSettings("DIA_PERIODO_SEMANAL")
                Case "M"
                    m_PeriodoMensual_CP = ConfigurationManager.AppSettings("PERIODO_MENSUAL")
                    m_ModoMensual_CP = ConfigurationManager.AppSettings("MODO_MENSUAL")
                    If m_ModoMensual_CP = "1" Then
                        m_DiaMes_CP = ConfigurationManager.AppSettings("DIA_MES")
                    ElseIf m_ModoMensual_CP = "2" Then
                        m_PosicionDiaMensual_CP = ConfigurationManager.AppSettings("POSICION_DIA_PERIODO_MENSUAL")
                        m_DiaPeriodoMensual_CP = ConfigurationManager.AppSettings("DIA_PERIODO_MENSUAL")
                    End If
            End Select

            m_horaEjecucion_CP = ConfigurationManager.AppSettings("HORA_EJECUCION")
            m_diaArranque_CP = ConfigurationManager.AppSettings("DIA_ARRANQUE_SERVICIO")
        End If
    End Sub

    ''' <summary>
    ''' Borra el fichero log para posteriormente volver a escribirlo
    ''' Escribe mensajes en el event viewer para hacer seguimiento de instalacion
    ''' </summary>
    ''' <remarks>Llamada desde=EjecucionServicioCalculoPuntuaciones; Tiempo máximo=0seg.</remarks>
    Private Sub BorrarFicheroServicio_QA_Puntuaciones_Calculo()
        System.IO.File.Delete(m_sFile_CP)
    End Sub
    ''' <summary>
    ''' Escribe un fichero log para indicarnos cuando se ha ejecutado el webservice
    ''' Escribe mensajes en el event viewer para hacer seguimiento de instalacion
    ''' </summary>
    ''' <param name="fechaActual">Fecha actual del sistema</param>
    ''' <param name="FechaEjecucion">Fecha ejecucion servicio</param>
    ''' <param name="fechaFichero">Fecha creacion del fichero</param>
    ''' <remarks>Llamada desde=EjecucionServicioCalculoPuntuaciones; Tiempo máximo=0seg.</remarks>
    Private Sub EscribirFicheroServicio_QA_Puntuaciones_Calculo(ByVal fechaActual As Date, ByVal FechaEjecucion As Date, ByVal fechaFichero As Date)
        Dim str = "Fichero de calculo de puntuaciones. "
        Dim oFileW As System.IO.StreamWriter
        oFileW = New System.IO.StreamWriter(m_sFile_CP, True)
        oFileW.WriteLine(str)
        oFileW.WriteLine("Fecha Actual = " & fechaActual)
        oFileW.WriteLine("Fecha Ejecucion = " & FechaEjecucion)
        oFileW.WriteLine("Fecha Fichero = " & fechaFichero)
        oFileW.Close()
    End Sub
    ''' <summary>
    ''' Proceso que comprueba si tiene que lanzar el webService o no. Se comprueba diariamente si en el fichero que se ha escrito le toca ejecutarse o no al webservice
    ''' Cuando ejecuta el webService creo un fichero log que nos indica cuando se ha ejecutado por ultima vez el webService.
    ''' </summary>
    ''' <remarks>Llamada desde=ArrancarTemporizador       temporizador_Elapsed; Tiempo máximo=0,2seg.</remarks>
    Private Sub EjecucionServicio_QA_Puntuaciones_Calculo()
        EnEjecucionServicioServicioCalculoPuntuaciones = True
        Dim dFecha As Date
        Dim dFechaFichero As Date = System.IO.File.GetLastWriteTime(m_sFile_CP)
        dFecha = FechaProximaEjecucionServicio_QA_Puntuaciones_Calculo()
        If DateDiff("n", Now(), dFecha) <= 0 Then
            'Decidimos borrar y escribir el fichero antes de la ejecución del servicio propiamente dicho para que si el servicio tarda más de lo debido
            'el archivo tenga fecha de modificación actual y asi no lance por segunda vez el servicio, evitando mails duplicados.
            'Borrar Fichero
            BorrarFicheroServicio_QA_Puntuaciones_Calculo()
            'Escribir Fichero log de puntuaciones, para indicarle cuando se realizo el calculo por ultima vez
            EscribirFicheroServicio_QA_Puntuaciones_Calculo(Now(), dFecha, dFechaFichero)
            'LlamarWebService
            CalculoPuntuaciones()
        End If
        EnEjecucionServicioServicioCalculoPuntuaciones = False
    End Sub
    ''' <summary>
    ''' Ejecutar el webService con el usuario administrador
    ''' Escribe mensajes en el event viewer para hacer seguimiento de instalacion
    ''' </summary>
    ''' <remarks>Llamada desde=EjecucionServicioCalculoPuntuaciones; Tiempo máximo=0,2seg.</remarks>
    Private Sub CalculoPuntuaciones()
        Dim oPuntuaciones As FSNServer.Puntuaciones = m_FSNServer.Get_Object(GetType(FSNServer.Puntuaciones))
        oPuntuaciones.CalcularPuntuaciones()
    End Sub
    ''' <summary>
    ''' Funcion que Calcula la proxima fecha de ejecucion del servicio Calculo Puntuaciones.
    ''' </summary>
    ''' <returns>Fecha proxima ejecucion del servicio</returns>
    ''' <remarks>Llamada desde=EjecucionServicioCalculoPuntuaciones; Tiempo máximo=0,1seg.</remarks>
    Private Function FechaProximaEjecucionServicio_QA_Puntuaciones_Calculo() As Date
        Dim diaSemanaHoy As Integer
        Dim dia As Integer
        Dim fechaFichero As Date
        'If no existe fichero de calculo de puntuaciones 
        If Not System.IO.File.Exists(m_sFile_CP) Then
            Select Case m_modoEjecucion_CP
                Case "D"
                    FechaProximaEjecucionServicio_QA_Puntuaciones_Calculo = CDate(m_diaArranque_CP & " " & m_horaEjecucion_CP)

                Case "S"
                    If m_DiaPeriodoSemanal_CP = 0 Then
                        FechaProximaEjecucionServicio_QA_Puntuaciones_Calculo = CDate(m_diaArranque_CP & " " & m_horaEjecucion_CP)

                    Else
                        diaSemanaHoy = DatePart("w", Now, FirstDayOfWeek.Monday)
                        If diaSemanaHoy > m_DiaPeriodoSemanal_CP Then
                            FechaProximaEjecucionServicio_QA_Puntuaciones_Calculo = DateAdd("d", 7 - diaSemanaHoy + m_DiaPeriodoSemanal_CP + 1, Now)
                        Else
                            FechaProximaEjecucionServicio_QA_Puntuaciones_Calculo = DateAdd("d", m_DiaPeriodoSemanal_CP - diaSemanaHoy, Now)
                        End If
                    End If
                    FechaProximaEjecucionServicio_QA_Puntuaciones_Calculo = CDate(Day(FechaProximaEjecucionServicio_QA_Puntuaciones_Calculo) & "/" & Month(FechaProximaEjecucionServicio_QA_Puntuaciones_Calculo) & "/" & Year(FechaProximaEjecucionServicio_QA_Puntuaciones_Calculo) & " " & m_horaEjecucion_CP)

                Case "M"
                    If m_ModoMensual_CP = "1" Then
                        dia = DatePart("d", m_diaArranque_CP, FirstDayOfWeek.Monday)
                        'Calcular fecha de próxima ejecución con el mes actual y el dia definido en DIA_MES, si el dia del mes es                                                           
                        'mayor que los días del mes actual el ultimo día del mes actual.
                        'Si el dia de hoy es superior al día de ejecución la ejecución será el mes siguiente.
                        If dia < m_DiaMes_CP Then
                            FechaProximaEjecucionServicio_QA_Puntuaciones_Calculo = Day(m_DiaMes_CP) & "/" & Month(m_diaArranque_CP) & "/" & Year(m_diaArranque_CP)
                        Else
                            FechaProximaEjecucionServicio_QA_Puntuaciones_Calculo = m_diaArranque_CP
                        End If
                        FechaProximaEjecucionServicio_QA_Puntuaciones_Calculo = CDate(FechaProximaEjecucionServicio_QA_Puntuaciones_Calculo & " " & m_horaEjecucion_CP)
                    Else '(opcion 2)
                        Dim primerDiaMes, UltimoDiaMes As Date
                        Dim K, diffDias As Integer
                        Dim diaProceso As Date
                        Dim diaSemanaProceso As Integer
                        Dim repeticionesSemanaMensual As Byte
                        Dim bEncontrado As Boolean
                        primerDiaMes = Now
                        primerDiaMes = "01" & "/" & Month(primerDiaMes) & "/" & Year(primerDiaMes)
                        UltimoDiaMes = DateAdd("m", 1, primerDiaMes)
                        UltimoDiaMes = DateAdd("d", -1, UltimoDiaMes)
                        diffDias = DateDiff(DateInterval.Day, primerDiaMes, UltimoDiaMes)
                        bEncontrado = False
                        For K = 0 To diffDias
                            diaProceso = DateAdd("d", K, primerDiaMes)
                            diaSemanaProceso = DatePart("w", diaProceso, Microsoft.VisualBasic.FirstDayOfWeek.Monday)
                            If diaSemanaProceso = m_DiaPeriodoMensual_CP Then
                                repeticionesSemanaMensual = repeticionesSemanaMensual + 1
                                If repeticionesSemanaMensual = m_PosicionDiaMensual_CP Then
                                    bEncontrado = True
                                    Exit For
                                End If
                            End If
                        Next

                        If bEncontrado Then
                            If diaProceso < m_diaArranque_CP Then
                                FechaProximaEjecucionServicio_QA_Puntuaciones_Calculo = m_diaArranque_CP
                            Else
                                FechaProximaEjecucionServicio_QA_Puntuaciones_Calculo = diaProceso
                            End If
                            FechaProximaEjecucionServicio_QA_Puntuaciones_Calculo = CDate(FechaProximaEjecucionServicio_QA_Puntuaciones_Calculo & " " & m_horaEjecucion_CP)
                        Else
                            FechaProximaEjecucionServicio_QA_Puntuaciones_Calculo = CDate(m_diaArranque_CP & " " & m_horaEjecucion_CP)
                        End If
                    End If
            End Select
        Else
            'fecha anterior ejecucion con la hora de ejecucion del servicio
            fechaFichero = System.IO.File.GetLastWriteTime(m_sFile_CP)
            FechaProximaEjecucionServicio_QA_Puntuaciones_Calculo = fechaFichero
            Select Case m_modoEjecucion_CP
                Case "D"
                    'suma el periodo diario en dias a la fecha de la ultima ejecucion
                    FechaProximaEjecucionServicio_QA_Puntuaciones_Calculo = DateAdd("d", CDbl(m_PeriodoDiario_CP), FechaProximaEjecucionServicio_QA_Puntuaciones_Calculo)
                Case "S"
                    FechaProximaEjecucionServicio_QA_Puntuaciones_Calculo = DateAdd(DateInterval.WeekOfYear, CDbl(m_PeriodoSemanal_CP), FechaProximaEjecucionServicio_QA_Puntuaciones_Calculo)
                Case "M"
                    FechaProximaEjecucionServicio_QA_Puntuaciones_Calculo = DateAdd("m", CDbl(m_PeriodoMensual_CP), FechaProximaEjecucionServicio_QA_Puntuaciones_Calculo)
                    If m_ModoMensual_CP = 1 Then
                        FechaProximaEjecucionServicio_QA_Puntuaciones_Calculo = CDate(m_DiaMes_CP & "/" & Month(FechaProximaEjecucionServicio_QA_Puntuaciones_Calculo) & "/" & Year(FechaProximaEjecucionServicio_QA_Puntuaciones_Calculo))

                    ElseIf m_ModoMensual_CP = 2 Then
                        Dim primerDiaMes, UltimoDiaMes As Date
                        Dim K, diffDias As Integer
                        Dim diaProceso As Date
                        Dim diaSemanaProceso As Integer
                        Dim repeticionesSemanaMensual As Byte
                        Dim bEncontrado As Boolean
                        primerDiaMes = FechaProximaEjecucionServicio_QA_Puntuaciones_Calculo
                        primerDiaMes = "01" & "/" & Month(primerDiaMes) & "/" & Year(primerDiaMes)
                        UltimoDiaMes = DateAdd("m", 1, primerDiaMes)
                        UltimoDiaMes = DateAdd("d", -1, UltimoDiaMes)
                        diffDias = DateDiff(DateInterval.Day, primerDiaMes, UltimoDiaMes)
                        bEncontrado = False
                        For K = 0 To diffDias
                            diaProceso = DateAdd("d", K, primerDiaMes)
                            diaSemanaProceso = DatePart("w", diaProceso, FirstDayOfWeek.Monday)
                            If diaSemanaProceso = m_DiaPeriodoMensual_CP Then
                                repeticionesSemanaMensual = repeticionesSemanaMensual + 1
                                If repeticionesSemanaMensual = m_PosicionDiaMensual_CP Then
                                    bEncontrado = True
                                    Exit For
                                End If
                            End If
                        Next

                        If bEncontrado Then
                            FechaProximaEjecucionServicio_QA_Puntuaciones_Calculo = diaProceso
                            FechaProximaEjecucionServicio_QA_Puntuaciones_Calculo = CDate(FechaProximaEjecucionServicio_QA_Puntuaciones_Calculo)
                        Else
                            FechaProximaEjecucionServicio_QA_Puntuaciones_Calculo = CDate(m_diaArranque_CP)
                        End If
                    End If
            End Select
            FechaProximaEjecucionServicio_QA_Puntuaciones_Calculo = New Date(Year(FechaProximaEjecucionServicio_QA_Puntuaciones_Calculo), Month(FechaProximaEjecucionServicio_QA_Puntuaciones_Calculo), Day(FechaProximaEjecucionServicio_QA_Puntuaciones_Calculo), Hour(m_horaEjecucion_CP), Minute(m_horaEjecucion_CP), 0)
        End If
    End Function
#End Region
#Region "No conformidades"
#Region "No conformidades proximidad fin"
#Region "Variables no conformidades proximidad fin"
    'Service Notif Proxim Fecha Fin Nc
    Private m_sFile_NP As String
    Private m_sHoraEjecucionNotifFechaFin_NP As String
    'Configuracion Fecha comienzo y hora ejecucion servicio
    Private m_diaArranque_NP As Date
    Private m_FechaProximaEjecucionServicioNotificacionProximidad As Date
    'Si no se introduce día de arranque u hora de ejecución que no se ejecute 
    Private m_bActivoServicioNotificacionProximidad As Boolean = True
    Private EnEjecucionServicioNotificacionProximidad As Boolean = False
#End Region
    ''' <summary>
    ''' Se indica el path del fichero log NotifFechaFinNc, la repeticion del servicio, sera diaría y a 
    ''' la hora indicada en el app.config (fichero mantenido por aplicación "instalador de servicios"). La 1ª 
    ''' vez se lanza la notificación y los dias sucesivos se comprobara si le toca o no ejecutar el webservice a 
    ''' traves del parametro Hora de Ejecucion
    ''' </summary>
    ''' <remarks>Llamada desde=Service1.OnStart; Tiempo máximo=0seg.</remarks>
    Private Sub LeerParametrosEjecucionServicio_QA_NoconformidadesNotificacionProximidadFin()
        Dim sPath As String

        sPath = ConfigurationManager.AppSettings("Path")

        m_sFile_NP = sPath & "\NotifFechaFinNc.txt"

        If (ConfigurationManager.AppSettings("HORA_EJECUCION_NP") = "") _
        OrElse (Not IsDate(ConfigurationManager.AppSettings("DIA_ARRANQUE_SERVICIO_NP"))) Then
            m_bActivoServicioNotificacionProximidad = False
        Else
            m_sHoraEjecucionNotifFechaFin_NP = ConfigurationManager.AppSettings("HORA_EJECUCION_NP")

            m_diaArranque_NP = ConfigurationManager.AppSettings("DIA_ARRANQUE_SERVICIO_NP")
        End If
    End Sub
    ''' <summary>
    ''' Borra el fichero log para posteriormente volver a escribirlo
    ''' Escribe mensajes en el event viewer para hacer seguimiento de instalacion
    ''' </summary>
    ''' <remarks>Llamada desde=Service1.vb; Tiempo máximo=0seg.</remarks>
    Private Sub BorrarFicheroServicio_QA_NoconformidadesNotificacionProximidadFin()
        System.IO.File.Delete(m_sFile_NP)
    End Sub
    ''' <summary>
    ''' Escribe un fichero log para indicarnos cuando se ha ejecutado el webservice
    ''' Escribe mensajes en el event viewer para hacer seguimiento de instalacion
    ''' <paramref name="fechaActual" >Fecha actual del sistema</paramref>
    ''' <paramref name="FechaEjecucion">Fecha ejecucion servicio</paramref>
    ''' <paramref name="fechaFichero" >Fecha creacion del fichero</paramref>
    ''' </summary>
    ''' <remarks>Llamada desde=Service1.vb; Tiempo máximo=0seg.</remarks>
    Private Sub EscribirFicheroServicio_QA_NoconformidadesNotificacionProximidadFin(ByVal fechaActual As Date, ByVal FechaEjecucion As Date, ByVal fechaFichero As Date)
        Dim str = "Fichero de Notificación al proveedor de la proximidad de las fechas límite de las acciones solicitadas y de la fecha fin de resolución de las no conformidades que tenga abiertas. "
        Dim oFileW As System.IO.StreamWriter
        oFileW = New System.IO.StreamWriter(m_sFile_NP, True)
        oFileW.WriteLine(str)
        oFileW.WriteLine("Fecha Actual = " & fechaActual)
        oFileW.WriteLine("Fecha Ejecucion = " & FechaEjecucion)
        oFileW.WriteLine("Fecha Fichero = " & fechaFichero)
        oFileW.Close()
    End Sub
    ''' <summary>
    ''' Proceso que comprueba si tiene que lanzar el webService o no. Se comprueba diariamente si en el fichero que se ha escrito le toca ejecutarse o no al webservice
    ''' Cuando ejecuta el webService creo un fichero log que nos indica cuando se ha ejecutado por ultima vez el webService.
    ''' </summary>
    ''' <remarks>Llamada desde=ArrancarTemporizador       temporizador_Elapsed; Tiempo máximo=0,2seg.</remarks>
    Private Sub EjecucionServicio_QA_NoconformidadesNotificacionProximidadFin()
        EnEjecucionServicioNotificacionProximidad = True
        Dim dFecha As Date
        Dim dFechaFichero As Date = System.IO.File.GetLastWriteTime(m_sFile_NP)
        dFecha = FechaProximaEjecucionServicio_QA_NoconformidadesNotificacionProximidadFin()
        If DateDiff("n", Now(), dFecha) <= 0 Then
            'Decidimos borrar y escribir el fichero antes de la ejecución del servicio propiamente dicho para que si el servicio tarda más de lo debido
            'el archivo tenga fecha de modificación actual y asi no lance por segunda vez el servicio, evitando mails duplicados.
            'Borrar Fichero
            BorrarFicheroServicio_QA_NoconformidadesNotificacionProximidadFin()
            'Escribir Fichero log de puntuaciones, para indicarle cuando se realizo el calculo por ultima vez
            EscribirFicheroServicio_QA_NoconformidadesNotificacionProximidadFin(Now(), dFecha, dFechaFichero)
            'LlamarWebService
            QA_NoconformidadesNotificacionProximidadFin()
        End If
        EnEjecucionServicioNotificacionProximidad = False
    End Sub
    ''' <summary>
    ''' Ejecutar el webService con el usuario administrador
    ''' Escribe mensajes en el event viewer para hacer seguimiento de instalacion
    ''' </summary>
    ''' <remarks>Llamada desde=Service1.vb; Tiempo máximo=0,2seg.</remarks>
    Private Sub QA_NoconformidadesNotificacionProximidadFin()
        Dim oNoConformidades As FSNServer.NoConformidades = m_FSNServer.Get_Object(GetType(FSNServer.NoConformidades))
        oNoConformidades.NotificarProximidadFechasFin()
    End Sub
    ''' <summary>
    ''' Funcion que calcula la proxima fecha de ejecucion del servicio.
    ''' </summary>
    ''' <returns>Fecha proxima ejecucion del servicio</returns>
    ''' <remarks>Llamada desde=propia pagina; Tiempo máximo=0,4seg.</remarks>
    Private Function FechaProximaEjecucionServicio_QA_NoconformidadesNotificacionProximidadFin() As Date
        Dim fechaFichero As Date
        'If no existe fichero de proximidad NC
        If Not System.IO.File.Exists(m_sFile_NP) Then
            FechaProximaEjecucionServicio_QA_NoconformidadesNotificacionProximidadFin = CDate(m_diaArranque_NP & " " & m_sHoraEjecucionNotifFechaFin_NP)
        Else

            'fecha anterior ejecucion con la hora de ejecucion del servicio
            fechaFichero = System.IO.File.GetLastWriteTime(m_sFile_NP)
            FechaProximaEjecucionServicio_QA_NoconformidadesNotificacionProximidadFin = fechaFichero

            'suma el periodo diario en dias a la fecha de la ultima ejecucion
            FechaProximaEjecucionServicio_QA_NoconformidadesNotificacionProximidadFin = DateAdd("d", 1, FechaProximaEjecucionServicio_QA_NoconformidadesNotificacionProximidadFin)

            FechaProximaEjecucionServicio_QA_NoconformidadesNotificacionProximidadFin = New Date(Year(FechaProximaEjecucionServicio_QA_NoconformidadesNotificacionProximidadFin), Month(FechaProximaEjecucionServicio_QA_NoconformidadesNotificacionProximidadFin), Day(FechaProximaEjecucionServicio_QA_NoconformidadesNotificacionProximidadFin), Hour(m_sHoraEjecucionNotifFechaFin_NP), Minute(m_sHoraEjecucionNotifFechaFin_NP), 0)
        End If
    End Function
#End Region
#End Region
#Region "Certificados"
#Region "Notificaciones Certificados Expirados"
#Region "Variables Certificados Expirados"
    'Service Notif Expiración Certificados
    Private m_sFile_EC As String
    Private m_DiasEjecucion_EC As Double
    Private m_sHoraEjecucion_EC As String
    'Configuracion Fecha comienzo y hora ejecucion servicio
    Private m_diaArranque_EC As Date
    Private m_fechaProximaEjecucionServicioNotificacionExpiracion As Date
    'Si no se introduce día de arranque, hora de ejecución o periodo, que no se ejecute 
    Private m_bActivoServicioNotificacionCertificados As Boolean = True
    Private EnEjecucuionServicioNotificacionCertificados As Boolean = False
#End Region
    ''' <summary>
    ''' Se indica el path del fichero log NotifCertificados, los dias de repeticion del servicio y la hora de
    ''' arranque del servicio. La 1ª vez se lanza la notificación y los dias sucesivos se comprobara si le toca o 
    ''' no ejecutar el webservice a traves del parametro DiasEjecucion y del parametro HoraEjecucion
    ''' </summary>
    ''' <remarks>Llamada desde=Service1.OnStart; Tiempo máximo=0seg.</remarks>
    Private Sub LeerParametrosEjecucionServicio_QA_Certificados_NotificarExpirados()
        Dim sPath As String

        sPath = ConfigurationManager.AppSettings("Path")

        m_sFile_EC = sPath & "\NotifCertificados.txt"

        If (ConfigurationManager.AppSettings("PERIODO_EC") = "") _
        OrElse (ConfigurationManager.AppSettings("HORA_EJECUCION_EC") = "") _
        OrElse (Not IsDate(ConfigurationManager.AppSettings("DIA_ARRANQUE_SERVICIO_EC"))) Then
            m_bActivoServicioNotificacionCertificados = False
        Else
            m_DiasEjecucion_EC = ConfigurationManager.AppSettings("PERIODO_EC")
            m_sHoraEjecucion_EC = ConfigurationManager.AppSettings("HORA_EJECUCION_EC")
            m_diaArranque_EC = ConfigurationManager.AppSettings("DIA_ARRANQUE_SERVICIO_EC")
        End If
    End Sub
    ''' <summary>
    ''' Borra el fichero log para posteriormente volver a escribirlo
    ''' Escribe mensajes en el event viewer para hacer seguimiento de instalacion
    ''' </summary>
    ''' <remarks>Llamada desde=EjecucionServicioNotificacionCertificados; Tiempo máximo=0seg.</remarks>
    Private Sub BorrarFicheroServicio_QA_Certificados_NotificarExpirados()
        System.IO.File.Delete(m_sFile_EC)
    End Sub
    ''' <summary>
    ''' Escribe un fichero log para indicarnos cuando se ha ejecutado el webservice
    ''' Escribe mensajes en el event viewer para hacer seguimiento de instalacion
    ''' </summary>
    ''' <param name="fechaActual">Fecha actual del sistema</param>
    ''' <param name="FechaEjecucion">Fecha ejecucion servicio</param>
    ''' <param name="fechaFichero" >Fecha creacion del fichero</param>
    ''' <remarks>Llamada desde=EjecucionServicioNotificacionCertificados; Tiempo máximo=0seg.</remarks>
    Private Sub EscribirFicheroServicio_QA_Certificados_NotificarExpirados(ByVal fechaActual As Date, ByVal FechaEjecucion As Date, ByVal fechaFichero As Date)
        Dim str = "Fichero de Notificación al proveedor de la expiración de certificados. "
        Dim oFileW As System.IO.StreamWriter
        oFileW = New System.IO.StreamWriter(m_sFile_EC, True)
        oFileW.WriteLine(str)
        oFileW.WriteLine("Fecha Actual = " & fechaActual)
        oFileW.WriteLine("Fecha Ejecucion = " & FechaEjecucion)
        oFileW.WriteLine("Fecha Fichero = " & fechaFichero)
        oFileW.Close()
    End Sub
    ''' <summary>
    ''' Proceso que comprueba si tiene que lanzar el webService o no. Se comprueba diariamente si en el fichero que se ha escrito le toca ejecutarse o no al webservice
    ''' Cuando ejecuta el webService creo un fichero log que nos indica cuando se ha ejecutado por ultima vez el webService.
    ''' </summary>
    ''' <remarks>Llamada desde=ArrancarTemporizador       temporizador_Elapsed; Tiempo máximo=0,2seg.</remarks>
    Private Sub EjecucionServicio_QA_Certificados_NotificarExpirados()
        EnEjecucuionServicioNotificacionCertificados = True
        Dim dFecha As Date
        Dim dFechaFichero As Date = System.IO.File.GetLastWriteTime(m_sFile_EC)
        dFecha = FechaProximaEjecucionServicio_QA_Certificados_NotificarExpirados()
        If DateDiff("n", Now(), dFecha) <= 0 Then
            'Decidimos borrar y escribir el fichero antes de la ejecución del servicio propiamente dicho para que si el servicio tarda más de lo debido
            'el archivo tenga fecha de modificación actual y asi no lance por segunda vez el servicio, evitando mails duplicados.
            'Borrar Fichero
            BorrarFicheroServicio_QA_Certificados_NotificarExpirados()
            'Escribir Fichero log de puntuaciones, para indicarle cuando se realizo el calculo por ultima vez
            EscribirFicheroServicio_QA_Certificados_NotificarExpirados(Now(), dFecha, dFechaFichero)
            'LlamarWebService
            QA_Certificados_NotificarExpirados()
        End If
        EnEjecucuionServicioNotificacionCertificados = False
    End Sub
    ''' <summary>
    ''' Ejecutar el webService con el usuario administrador
    ''' Escribe mensajes en el event viewer para hacer seguimiento de instalacion
    ''' </summary>
    ''' <remarks>Llamada desde=EjecucionServicioNotificacionCertificados; Tiempo máximo=0,2seg.</remarks>
    Private Sub QA_Certificados_NotificarExpirados()
        Dim oCertificados As FSNServer.Certificados = m_FSNServer.Get_Object(GetType(FSNServer.Certificados))
        oCertificados.NotificarExpiracionCertificados()
    End Sub
    ''' <summary>
    ''' Funcion que calcula la proxima fecha de ejecucion del servicio.
    ''' </summary>
    ''' <returns>Fecha proxima ejecucion del servicio</returns>
    ''' <remarks>Llamada desde=EjecucionServicioNotificacionCertificados; Tiempo máximo=0,4seg.</remarks>
    Private Function FechaProximaEjecucionServicio_QA_Certificados_NotificarExpirados() As Date
        Dim fechaFichero As Date
        'If no existe fichero de expiracion certif
        If Not System.IO.File.Exists(m_sFile_EC) Then
            FechaProximaEjecucionServicio_QA_Certificados_NotificarExpirados = CDate(m_diaArranque_EC & " " & m_sHoraEjecucion_EC)
        Else
            'fecha anterior ejecucion con la hora de ejecucion del servicio
            fechaFichero = System.IO.File.GetLastWriteTime(m_sFile_EC)
            FechaProximaEjecucionServicio_QA_Certificados_NotificarExpirados = fechaFichero
            'suma el periodo diario en dias a la fecha de la ultima ejecucion
            FechaProximaEjecucionServicio_QA_Certificados_NotificarExpirados = DateAdd("d", CDbl(m_DiasEjecucion_EC), FechaProximaEjecucionServicio_QA_Certificados_NotificarExpirados)

            FechaProximaEjecucionServicio_QA_Certificados_NotificarExpirados = New Date(Year(FechaProximaEjecucionServicio_QA_Certificados_NotificarExpirados), Month(FechaProximaEjecucionServicio_QA_Certificados_NotificarExpirados), Day(FechaProximaEjecucionServicio_QA_Certificados_NotificarExpirados), Hour(m_sHoraEjecucion_EC), Minute(m_sHoraEjecucion_EC), 0)
        End If
    End Function
#End Region
#Region "Solicitud automatica certificados"
#Region "Variables Servicio Solicitud Automatica de Certificados"
    Private File_FSQA_SolicitudAutomaticaCertificados As String
    Private ActivoServicio_SolicitudAutomaticaCertificados As Boolean = True

    Private modoEjecucion_SolicitudAutomaticaCertificados As String '[M(minutos),H(horas)]
    Private Periodo_SolicitudAutomaticaCertificados As String

    Private diaArranque_SolicitudAutomaticaCertificados As Date
    Private HoraEjecucion_SolicitudAutomaticaCertificados As String
    Private EnEjecucionServicio_SolicitudAutomaticaCertificados As Boolean = False
#End Region
    ''' <summary>
    ''' Se indica el path del fichero log FSQA_SOLICITUD_AUTO_CERTIFICADOS, la repeticion del servicio, sera diaría y a 
    ''' la hora indicada en el app.config (fichero mantenido por aplicación "instalador de servicios"). La 1ª 
    ''' vez se lanza la notificación y en las sucesivas se comprobara si le toca o no ejecutar el webservice a 
    ''' traves del parametro Hora de Ejecucion
    ''' </summary>
    ''' <remarks>Llamada desde=Service1.OnStart; Tiempo máximo=0seg.</remarks>
    Private Sub LeerParametrosEjecucionServicio_QA_Certificados_Solicitud()
        Dim sPath As String
        sPath = ConfigurationManager.AppSettings("Path")

        File_FSQA_SolicitudAutomaticaCertificados = sPath & "\FSQA_SOLICITUD_AUTO_CERTIFICADOS.txt"

        If ConfigurationManager.AppSettings("MODO_EJECUCION_SERVICIO_SOLICITUD_AUTOMATICA_CERTIFICADOS") = "" _
        OrElse (ConfigurationManager.AppSettings("HORA_EJECUCION_SERVICIO_SOLICITUD_AUTOMATICA_CERTIFICADOS") = "") _
        OrElse (Not IsDate(ConfigurationManager.AppSettings("DIA_ARRANQUE_SERVICIO_SOLICITUD_AUTOMATICA_CERTIFICADOS"))) Then
            ActivoServicio_SolicitudAutomaticaCertificados = False
        Else
            ''Configuracion del modo de ejecucion del servicio.
            ''Toma los parametros de la configuracion.
            modoEjecucion_SolicitudAutomaticaCertificados = ConfigurationManager.AppSettings("MODO_EJECUCION_SERVICIO_SOLICITUD_AUTOMATICA_CERTIFICADOS")
            Periodo_SolicitudAutomaticaCertificados = ConfigurationManager.AppSettings("PERIODO_SERVICIO_SOLICITUD_AUTOMATICA_CERTIFICADOS")
            HoraEjecucion_SolicitudAutomaticaCertificados = ConfigurationManager.AppSettings("HORA_EJECUCION_SERVICIO_SOLICITUD_AUTOMATICA_CERTIFICADOS")
            diaArranque_SolicitudAutomaticaCertificados = ConfigurationManager.AppSettings("DIA_ARRANQUE_SERVICIO_SOLICITUD_AUTOMATICA_CERTIFICADOS")
        End If
    End Sub
    ''' <summary>
    ''' Proceso que comprueba si tiene que lanzar el webService o no. Se comprueba diariamente si en el fichero que se ha escrito le toca ejecutarse o no al webservice
    ''' Cuando ejecuta el webService creo un fichero log que nos indica cuando se ha ejecutado por ultima vez el webService.
    ''' </summary>
    ''' <remarks>Llamada desde=ArrancarTemporizador       temporizador_Elapsed; Tiempo máximo=0,2seg.</remarks>
    Private Sub EjecucionServicio_QA_Certificados_Solicitud()
        EnEjecucionServicio_SolicitudAutomaticaCertificados = True
        Dim dFechaHora As Date
        Dim dFechaHoraFichero As Date = System.IO.File.GetLastWriteTime(File_FSQA_SolicitudAutomaticaCertificados)
        dFechaHora = FechaProximaEjecucionServicio_QA_Certificados_Solicitud()
        If DateDiff("n", Now(), dFechaHora) <= 0 Then
            'Decidimos borrar y escribir el fichero antes de la ejecución del servicio propiamente dicho para que si el servicio tarda más de lo debido
            'el archivo tenga fecha de modificación actual y asi no lance por segunda vez el servicio, evitando mails duplicados.
            'Borrar Fichero
            BorrarFicheroServicio_QA_Certificados_Solicitud()
            'Escribir Fichero log de puntuaciones, para indicarle cuando se realizo el calculo por ultima vez
            EscribirFicheroServicio_QA_Certificados_Solicitud(Now(), dFechaHora, dFechaHoraFichero)
            'LlamarWebService
            QA_Certificados_Solicitud()
        End If
        EnEjecucionServicio_SolicitudAutomaticaCertificados = False
    End Sub
    ''' <summary>
    ''' Funcion que calcula la proxima ejecucion del servicio.
    ''' </summary>
    ''' <returns>Fecha y hora de la proxima ejecucion del servicio</returns>
    ''' <remarks>Llamada desde=propia pagina; Tiempo máximo=0,4seg.</remarks>
    Private Function FechaProximaEjecucionServicio_QA_Certificados_Solicitud() As Date
        Dim fechaFichero As Date
        Dim intervaloNotificacion As String
        'If no existe fichero de notificaciones CN
        If Not System.IO.File.Exists(File_FSQA_SolicitudAutomaticaCertificados) Then
            FechaProximaEjecucionServicio_QA_Certificados_Solicitud = CDate(diaArranque_SolicitudAutomaticaCertificados & " " & HoraEjecucion_SolicitudAutomaticaCertificados)
        Else
            'fecha anterior ejecucion con la hora de ejecucion del servicio
            fechaFichero = System.IO.File.GetLastWriteTime(File_FSQA_SolicitudAutomaticaCertificados)
            FechaProximaEjecucionServicio_QA_Certificados_Solicitud = fechaFichero

            Select Case modoEjecucion_SolicitudAutomaticaCertificados
                Case "M" ' Minutos
                    intervaloNotificacion = "n"
                Case "H" 'Horas
                    intervaloNotificacion = "h"
                Case Else    'Segundos
                    intervaloNotificacion = "s"
            End Select
            'suma el periodo en horas a la fecha de la ultima ejecucion
            FechaProximaEjecucionServicio_QA_Certificados_Solicitud = DateAdd(intervaloNotificacion, CType(Periodo_SolicitudAutomaticaCertificados, Double), FechaProximaEjecucionServicio_QA_Certificados_Solicitud)
            FechaProximaEjecucionServicio_QA_Certificados_Solicitud = New Date(Year(FechaProximaEjecucionServicio_QA_Certificados_Solicitud), Month(FechaProximaEjecucionServicio_QA_Certificados_Solicitud), Day(FechaProximaEjecucionServicio_QA_Certificados_Solicitud), Hour(FechaProximaEjecucionServicio_QA_Certificados_Solicitud), Minute(FechaProximaEjecucionServicio_QA_Certificados_Solicitud), 0)
        End If
    End Function
    ''' <summary>
    ''' Borra el fichero log para posteriormente volver a escribirlo
    ''' Escribe mensajes en el event viewer para hacer seguimiento de instalacion
    ''' </summary>
    ''' <remarks>Llamada desde=EjecucionServicioCalculoPuntuaciones; Tiempo máximo=0seg.</remarks>
    Private Sub BorrarFicheroServicio_QA_Certificados_Solicitud()
        System.IO.File.Delete(File_FSQA_SolicitudAutomaticaCertificados)
    End Sub
    ''' <summary>
    ''' Escribe un fichero log para indicarnos cuando se ha ejecutado el webservice
    ''' Escribe mensajes en el event viewer para hacer seguimiento de instalacion
    ''' </summary>
    ''' <param name="fechaActual">Fecha actual del sistema</param>
    ''' <param name="FechaEjecucion">Fecha ejecucion servicio</param>
    ''' <param name="fechaFichero">Fecha creacion del fichero</param>
    ''' <remarks>Llamada desde=EjecucionServicioCalculoPuntuaciones; Tiempo máximo=0seg.</remarks>
    Private Sub EscribirFicheroServicio_QA_Certificados_Solicitud(ByVal fechaActual As Date, ByVal FechaEjecucion As Date, ByVal fechaFichero As Date)
        Dim str = "Fichero de solicitud automática de certificados. "
        Dim oFileW As System.IO.StreamWriter
        oFileW = New System.IO.StreamWriter(File_FSQA_SolicitudAutomaticaCertificados, True)
        oFileW.WriteLine(str)
        oFileW.WriteLine("Fecha Actual = " & fechaActual)
        oFileW.WriteLine("Fecha Ejecucion = " & FechaEjecucion)
        oFileW.WriteLine("Fecha Fichero = " & fechaFichero)
        oFileW.Close()
    End Sub
    ''' <summary>
    ''' Ejecutar el webService con el usuario administrador
    ''' Escribe mensajes en el event viewer para hacer seguimiento de instalacion
    ''' </summary>
    ''' <remarks>Llamada desde=EjecucionServicioCalculoPuntuaciones; Tiempo máximo=0,2seg.</remarks>
    Private Sub QA_Certificados_Solicitud()
        Dim oCertificaods As FSNServer.Certificados = m_FSNServer.Get_Object(GetType(FSNServer.Certificados))
        oCertificaods.Solicitud_Automatica_Certificados()
    End Sub
#End Region
#Region "Despublicar Automatica certificados"
#Region "Variables Servicio Solicitud Automatica de Certificados"
    Private File_FSQA_Certificados_DespublicarAutomatica As String
    Private ActivoServicio_Certificados_DespublicarAutomatica As Boolean = True

    Private modoEjecucion_Certificados_DespublicarAutomaticas As String '[M(minutos),H(horas), Eoc(días)]
    Private Periodo_Certificados_DespublicarAutomaticas As String

    Private diaArranque_Certificados_DespublicarAutomaticas As Date
    Private HoraEjecucion_Certificados_DespublicarAutomatica As String
    Private EnEjecucionServicio_Certificados_DespublicarAutomatica As Boolean = False
#End Region
    ''' <summary>
    ''' Se indica el path del fichero log Certificados_DespublicarAutomatica, los dias de repeticion del servicio y la hora de
    ''' arranque del servicio. La 1ª vez se lanza la notificación y los dias sucesivos se comprobara si le toca o 
    ''' no ejecutar el webservice a traves del parametro DIA_ARRANQUE_CERTIF_DESPUBL y del parametro HORA_EJECUCION_CERTIF_DESPUBL
    ''' </summary>
    ''' <remarks>Llamada desde=Service1.OnStart; Tiempo máximo=0seg.</remarks>
    Private Sub LeerParametrosEjecucionServicio_QA_Certificados_Despublicacion()
        Dim sPath As String

        sPath = ConfigurationManager.AppSettings("Path")

        File_FSQA_Certificados_DespublicarAutomatica = sPath & "\Certificados_DespublicarAutomatica.txt"

        If (ConfigurationManager.AppSettings("MODO_CERTIF_DESPUBL") = "") _
        OrElse (ConfigurationManager.AppSettings("HORA_EJECUCION_CERTIF_DESPUBL") = "") _
        OrElse (ConfigurationManager.AppSettings("HORA_EJECUCION_CERTIF_DESPUBL") = "") _
        OrElse (Not IsDate(ConfigurationManager.AppSettings("DIA_ARRANQUE_CERTIF_DESPUBL"))) Then
            ActivoServicio_Certificados_DespublicarAutomatica = False
        Else
            modoEjecucion_Certificados_DespublicarAutomaticas = ConfigurationManager.AppSettings("MODO_CERTIF_DESPUBL") '[M(minutos),H(horas), Eoc(días)]
            Periodo_Certificados_DespublicarAutomaticas = ConfigurationManager.AppSettings("PERIODO_CERTIF_DESPUBL")
            HoraEjecucion_Certificados_DespublicarAutomatica = ConfigurationManager.AppSettings("HORA_EJECUCION_CERTIF_DESPUBL")
            diaArranque_Certificados_DespublicarAutomaticas = ConfigurationManager.AppSettings("DIA_ARRANQUE_CERTIF_DESPUBL")
        End If
    End Sub
    ''' <summary>
    ''' Funcion que calcula la proxima ejecucion del servicio.
    ''' </summary>
    ''' <returns>Fecha y hora de la proxima ejecucion del servicio</returns>
    ''' <remarks>Llamada desde=propia pagina; Tiempo máximo=0,4seg.</remarks>
    Private Function FechaProximaEjecucionServicio_QA_Certificados_Despublicacion() As Date
        Dim fechaFichero As Date
        Dim intervaloNotificacion As String

        'If no existe fichero de notificaciones CN
        If Not System.IO.File.Exists(File_FSQA_Certificados_DespublicarAutomatica) Then
            FechaProximaEjecucionServicio_QA_Certificados_Despublicacion = CDate(diaArranque_Certificados_DespublicarAutomaticas & " " & HoraEjecucion_Certificados_DespublicarAutomatica)
        Else
            'fecha anterior ejecucion con la hora de ejecucion del servicio
            fechaFichero = System.IO.File.GetLastWriteTime(File_FSQA_Certificados_DespublicarAutomatica)
            FechaProximaEjecucionServicio_QA_Certificados_Despublicacion = fechaFichero

            'suma el periodo en horas a la fecha de la ultima ejecucion
            Select Case modoEjecucion_Certificados_DespublicarAutomaticas
                Case "M" ' Minutos
                    intervaloNotificacion = "n"
                Case "H" 'Horas
                    intervaloNotificacion = "h"
                Case Else    'Segundos
                    intervaloNotificacion = "s"
            End Select

            FechaProximaEjecucionServicio_QA_Certificados_Despublicacion = DateAdd(intervaloNotificacion, CType(Periodo_Certificados_DespublicarAutomaticas, Double), FechaProximaEjecucionServicio_QA_Certificados_Despublicacion)

            FechaProximaEjecucionServicio_QA_Certificados_Despublicacion = New Date(Year(FechaProximaEjecucionServicio_QA_Certificados_Despublicacion), Month(FechaProximaEjecucionServicio_QA_Certificados_Despublicacion), Day(FechaProximaEjecucionServicio_QA_Certificados_Despublicacion), Hour(FechaProximaEjecucionServicio_QA_Certificados_Despublicacion), Minute(FechaProximaEjecucionServicio_QA_Certificados_Despublicacion), 0)
        End If
    End Function
    ''' <summary>
    ''' Borra el fichero log para posteriormente volver a escribirlo
    ''' Escribe mensajes en el event viewer para hacer seguimiento de instalacion
    ''' </summary>
    ''' <remarks>Llamada desde=EjecucionServicioCertificados_DespublicarAutomatica; Tiempo máximo=0seg.</remarks>
    Private Sub BorrarFicheroServicio_QA_Certificados_Despublicacion()
        System.IO.File.Delete(File_FSQA_Certificados_DespublicarAutomatica)
    End Sub
    ''' <summary>
    ''' Escribe un fichero log para indicarnos cuando se ha ejecutado el webservice
    ''' Escribe mensajes en el event viewer para hacer seguimiento de instalacion
    ''' </summary>
    ''' <param name="fechaActual">Fecha actual del sistema</param>
    ''' <param name="FechaEjecucion">Fecha ejecucion servicio</param>
    ''' <param name="fechaFichero">Fecha creacion del fichero</param>
    ''' <remarks>Llamada desde=EjecucionServicioCertificados_DespublicarAutomatica; Tiempo máximo=0seg.</remarks>
    Private Sub EscribirFicheroServicio_QA_Certificados_Despublicacion(ByVal fechaActual As Date, ByVal FechaEjecucion As Date, ByVal fechaFichero As Date)
        Dim str = "Fichero de Certificados Despublicar Automatica. "
        Dim oFileW As System.IO.StreamWriter
        oFileW = New System.IO.StreamWriter(File_FSQA_Certificados_DespublicarAutomatica, True)
        oFileW.WriteLine(str)
        oFileW.WriteLine("Fecha Actual = " & fechaActual)
        oFileW.WriteLine("Fecha Ejecucion = " & FechaEjecucion)
        oFileW.WriteLine("Fecha Fichero = " & fechaFichero)

        oFileW.Close()
    End Sub
    ''' <summary>
    ''' Proceso que comprueba si tiene que lanzar el webService o no. Se comprueba cada x minutos si en el fichero que se ha escrito le toca ejecutarse o no al webservice
    ''' Cuando ejecuta el webService creo un fichero log que nos indica cuando se ha ejecutado por ultima vez el webService.
    ''' </summary>
    ''' <remarks>Llamada desde=ArrancarTemporizador       temporizador_Elapsed; Tiempo máximo=0,2seg.</remarks>
    Private Sub EjecucionServicio_QA_Certificados_Despublicacion()
        EnEjecucionServicio_Certificados_DespublicarAutomatica = True
        Dim dFechaHora As Date
        Dim dFechaHoraFichero As Date = System.IO.File.GetLastWriteTime(File_FSQA_Certificados_DespublicarAutomatica)
        dFechaHora = FechaProximaEjecucionServicio_QA_Certificados_Despublicacion()
        If DateDiff("n", Now(), dFechaHora) <= 0 Then
            'Decidimos borrar y escribir el fichero antes de la ejecución del servicio propiamente dicho para que si el servicio tarda más de lo debido
            'el archivo tenga fecha de modificación actual y asi no lance por segunda vez el servicio.
            'Borrar Fichero
            BorrarFicheroServicio_QA_Certificados_Despublicacion()
            'Escribir Fichero log de puntuaciones, para indicarle cuando se realizo el calculo por ultima vez
            EscribirFicheroServicio_QA_Certificados_Despublicacion(Now(), dFechaHora, dFechaHoraFichero)
            'LlamarWebService
            QA_Certificados_Despublicacion()
        End If
        EnEjecucionServicio_Certificados_DespublicarAutomatica = False
    End Sub

    ''' <summary>
    ''' Ejecutar el webService con el usuario administrador
    ''' Escribe mensajes en el event viewer para hacer seguimiento de instalacion
    ''' </summary>
    ''' <remarks>Llamada desde=EjecucionServicioCertificados_DespublicarAutomatica; Tiempo máximo=0,2seg.</remarks>
    Private Sub QA_Certificados_Despublicacion()
        Dim oCertificados As FSNServer.Certificados = m_FSNServer.Get_Object(GetType(FSNServer.Certificados))
        oCertificados.Certificados_Despublicar_Automatica()
    End Sub
#End Region
#Region "Certificados Aplicables por Expresiones Regulares"
#Region "Variables Certificados por Expresiones Regulares"
    Private File_FSQA_Certif_EXPREG As String
    Private m_bActivoServicio_Certificados_Por_Expresiones_Regulares As Boolean = True

    Private modoEjecucion_Certif_EXPREG As String '[M(minutos),H(horas)]
    Private Periodo_Certif_EXPREG As String

    Private diaArranque_Certif_EXPREG As Date
    Private HoraEjecucion_Certif_EXPREG As String
    Private EnEjecucionServicioServicio_Certificados_Por_Expresiones_Regulares As Boolean = False
#End Region
    ''' <summary>
    ''' Se indica el path del fichero log , los dias de repeticion del servicio. La 1ª 
    ''' vez se lanza el calculo y los dias sucesivos se comprobara si le toca o no ejecutar el webservice 
    ''' a traves del parametro DiasEjecucion
    ''' </summary>
    ''' <remarks>Llamada desde=Service1.OnStart; Tiempo máximo=0seg.</remarks>
    Private Sub LeerParametrosEjecucionServicio_QA_Certificados_PorExpresionesRegulares()
        Dim sPath As String
        sPath = ConfigurationManager.AppSettings("Path")

        File_FSQA_Certif_EXPREG = sPath & "\Certificados_Por_Expresiones_Regulares.txt"

        If ConfigurationManager.AppSettings("MODO_EJECUCION_EXPREG") = "" _
        OrElse (Not IsDate(ConfigurationManager.AppSettings("HORA_EJECUCION_SERVICIO_EXPREG"))) _
        OrElse (Not IsDate(ConfigurationManager.AppSettings("DIA_ARRANQUE_SERVICIO_EXPREG"))) Then
            m_bActivoServicio_Certificados_Por_Expresiones_Regulares = False
        Else
            ''Configuracion del modo de ejecucion del servicio.
            ''Toma los parametros de la configuracion.
            modoEjecucion_Certif_EXPREG = ConfigurationManager.AppSettings("MODO_EJECUCION_EXPREG")
            Periodo_Certif_EXPREG = ConfigurationManager.AppSettings("PERIODO_SERVICIO_EXPREG")
            HoraEjecucion_Certif_EXPREG = ConfigurationManager.AppSettings("HORA_EJECUCION_SERVICIO_EXPREG")
            diaArranque_Certif_EXPREG = ConfigurationManager.AppSettings("DIA_ARRANQUE_SERVICIO_EXPREG")
        End If
    End Sub
    ''' <summary>
    ''' Borra el fichero log para posteriormente volver a escribirlo
    ''' Escribe mensajes en el event viewer para hacer seguimiento de instalacion
    ''' </summary>
    ''' <remarks>Llamada desde=; Tiempo máximo=0seg.</remarks>
    Private Sub BorrarFicheroServicio_QA_Certificados_PorExpresionesRegulares()
        System.IO.File.Delete(File_FSQA_Certif_EXPREG)
    End Sub
    ''' <summary>
    ''' Escribe un fichero log para indicarnos cuando se ha ejecutado el webservice
    ''' Escribe mensajes en el event viewer para hacer seguimiento de instalacion
    ''' </summary>
    ''' <param name="fechaActual">Fecha actual del sistema</param>
    ''' <param name="FechaEjecucion">Fecha ejecucion servicio</param>
    ''' <param name="fechaFichero">Fecha creacion del fichero</param>
    ''' <remarks>Llamada desde=; Tiempo máximo=0seg.</remarks>
    Private Sub EscribirFicheroServicio_QA_Certificados_PorExpresionesRegulares(ByVal fechaActual As Date, ByVal FechaEjecucion As Date, ByVal fechaFichero As Date)
        Dim str = "Fichero de certificados por expresiones regulares. "
        Dim oFileW As System.IO.StreamWriter
        oFileW = New System.IO.StreamWriter(File_FSQA_Certif_EXPREG, True)
        oFileW.WriteLine(str)
        oFileW.WriteLine("Fecha Actual = " & fechaActual)
        oFileW.WriteLine("Fecha Ejecucion = " & FechaEjecucion)
        oFileW.WriteLine("Fecha Fichero = " & fechaFichero)
        oFileW.Close()
    End Sub
    ''' <summary>
    ''' Proceso que comprueba si tiene que lanzar el webService o no. Se comprueba diariamente si en el fichero que se ha escrito le toca ejecutarse o no al webservice
    ''' Cuando ejecuta el webService creo un fichero log que nos indica cuando se ha ejecutado por ultima vez el webService.
    ''' </summary>
    ''' <remarks>Llamada desde=ArrancarTemporizador       temporizador_Elapsed; Tiempo máximo=0,2seg.</remarks>
    Private Sub EjecucionServicio_QA_Certificados_PorExpresionesRegulares()
        EnEjecucionServicioServicio_Certificados_Por_Expresiones_Regulares = True
        Dim dFecha As Date
        Dim dFechaFichero As Date = System.IO.File.GetLastWriteTime(File_FSQA_Certif_EXPREG)
        dFecha = FechaPróximaEjecucionServicio_QA_Certificados_PorExpresionesRegulares()
        If DateDiff("n", Now(), dFecha) <= 0 Then
            'Decidimos borrar y escribir el fichero antes de la ejecución del servicio propiamente dicho para que si el servicio tarda más de lo debido
            'el archivo tenga fecha de modificación actual y asi no lance por segunda vez el servicio, evitando mails duplicados.
            'Borrar Fichero
            BorrarFicheroServicio_QA_Certificados_PorExpresionesRegulares()
            'Escribir Fichero log de puntuaciones, para indicarle cuando se realizo el calculo por ultima vez
            EscribirFicheroServicio_QA_Certificados_PorExpresionesRegulares(Now(), dFecha, dFechaFichero)
            'LlamarWebService
            QA_Certificados_PorExpresionesRegulares()
        End If
        EnEjecucionServicioServicio_Certificados_Por_Expresiones_Regulares = False
    End Sub
    ''' <summary>
    ''' Ejecutar el webService con el usuario administrador
    ''' Escribe mensajes en el event viewer para hacer seguimiento de instalacion
    ''' </summary>
    ''' <remarks>Llamada desde=EjecucionServicioCalculoPuntuaciones; Tiempo máximo=0,2seg.</remarks>
    Private Sub QA_Certificados_PorExpresionesRegulares()
        Dim oCertificados As FSNServer.Certificados = m_FSNServer.Get_Object(GetType(FSNServer.Certificados))
        oCertificados.Certificados_Por_Expresiones_Regulares()
    End Sub
    ''' <summary>
    ''' Funcion que Calcula la proxima fecha de ejecucion del servicio Calculo Puntuaciones.
    ''' </summary>
    ''' <returns>Fecha proxima ejecucion del servicio</returns>
    ''' <remarks>Llamada desde=EjecucionServicioCalculoPuntuaciones; Tiempo máximo=0,1seg.</remarks>
    Private Function FechaPróximaEjecucionServicio_QA_Certificados_PorExpresionesRegulares() As Date
        Dim fechaFichero As Date
        Dim intervaloNotificacion As String
        'If no existe fichero de notificaciones CN
        If Not System.IO.File.Exists(File_FSQA_Certif_EXPREG) Then
            FechaPróximaEjecucionServicio_QA_Certificados_PorExpresionesRegulares = CDate(diaArranque_Certif_EXPREG & " " & HoraEjecucion_Certif_EXPREG)
        Else
            'fecha anterior ejecucion con la hora de ejecucion del servicio
            fechaFichero = System.IO.File.GetLastWriteTime(File_FSQA_Certif_EXPREG)
            FechaPróximaEjecucionServicio_QA_Certificados_PorExpresionesRegulares = fechaFichero

            Select Case modoEjecucion_Certif_EXPREG
                Case "M" ' Minutos
                    intervaloNotificacion = "n"
                Case "H" 'Horas
                    intervaloNotificacion = "h"
                Case Else    'Segundos
                    intervaloNotificacion = "s"
            End Select
            'suma el periodo en horas a la fecha de la ultima ejecucion
            FechaPróximaEjecucionServicio_QA_Certificados_PorExpresionesRegulares = DateAdd(intervaloNotificacion, CType(Periodo_Certif_EXPREG, Double), FechaPróximaEjecucionServicio_QA_Certificados_PorExpresionesRegulares)
            FechaPróximaEjecucionServicio_QA_Certificados_PorExpresionesRegulares = New Date(Year(FechaPróximaEjecucionServicio_QA_Certificados_PorExpresionesRegulares), Month(FechaPróximaEjecucionServicio_QA_Certificados_PorExpresionesRegulares), Day(FechaPróximaEjecucionServicio_QA_Certificados_PorExpresionesRegulares), Hour(FechaPróximaEjecucionServicio_QA_Certificados_PorExpresionesRegulares), Minute(FechaPróximaEjecucionServicio_QA_Certificados_PorExpresionesRegulares), 0)
        End If
    End Function
#End Region
#Region "Notificaciones Certificados Revisados"
#Region "Variables Notificaciones Certificados Revisados"
    Private File_FSQA_Certificados_NotificarRevisados As String
    Private ActivoServicio_Certificados_NotificarRevisados As Boolean = True
    Private modoEjecucion_Certificados_NotificarRevisados As String '[M(minutos),H(horas), Eoc(días)]
    Private PeriodoMensual_Certificados_NotificarRevisados As String
    Private ModoMensual_Certificados_NotificarRevisados As String
    Private PosicionDiaMensual_Certificados_NotificarRevisados As String
    Private DiaPeriodoMensual_Certificados_NotificarRevisados As String
    Private diaArranque_Certificados_NotificarRevisados As Date
    Private HoraEjecucion_Certificados_NotificarRevisados As String
    Private EnEjecucionServicio_Certificados_NotificarRevisados As Boolean = False
    Private Periodo_Certificados_NotificarRevisados As String

#End Region
    ''' <summary>
    ''' Se indica el path del fichero log Certificados_NotificarRevisados, los dias de repeticion del servicio y la hora de
    ''' arranque del servicio. Para lanzar la notificación se revisan los distintos certificados de cada proveedor  
    ''' no de baja, de cumplimentación obligatoria, que les correspondan y que este cumplimetando y validado not null
    ''' </summary>
    ''' <remarks>Llamada desde=Service1.OnStart; Tiempo máximo=0seg.</remarks>
    Private Sub LeerParametrosEjecucionServicio_QA_Certificados_NotificarRevisados()
        Dim sPath As String

        sPath = ConfigurationManager.AppSettings("Path")

        File_FSQA_Certificados_NotificarRevisados = sPath & "\Certificados_NotificarRevisados.txt"

        If (ConfigurationManager.AppSettings("MODO_EJECUCION_NOTIF_CERT_REVISADO") = "") _
        OrElse (ConfigurationManager.AppSettings("HORA_EJECUCION_NOTIF_CERT_REVISADO") = "") _
        OrElse (Not IsDate(ConfigurationManager.AppSettings("DIA_ARRANQUE_SERVICIO_NOTIF_CERT_REVISADO"))) Then
            ActivoServicio_Certificados_NotificarRevisados = False
        Else
            ''Configuracion del modo de ejecucion del servicio.
            modoEjecucion_Certificados_NotificarRevisados = ConfigurationManager.AppSettings("MODO_EJECUCION_NOTIF_CERT_REVISADO") '[M(minutos),H(horas), Eoc(días)]
            HoraEjecucion_Certificados_NotificarRevisados = ConfigurationManager.AppSettings("HORA_EJECUCION_NOTIF_CERT_REVISADO")
            diaArranque_Certificados_NotificarRevisados = ConfigurationManager.AppSettings("DIA_ARRANQUE_SERVICIO_NOTIF_CERT_REVISADO")
            Periodo_Certificados_NotificarRevisados = ConfigurationManager.AppSettings("PERIODO_SERVICIO_NOTIF_CERT_REVISADO")
        End If
    End Sub

    ''' <summary>
    ''' Proceso que comprueba si tiene que lanzar el webService o no. Se comprueba diariamente si en el fichero que se ha escrito le toca ejecutarse o no al webservice
    ''' Cuando ejecuta el webService creo un fichero log que nos indica cuando se ha ejecutado por ultima vez el webService.
    ''' </summary>
    ''' <remarks>Llamada desde=ArrancarTemporizador       temporizador_Elapsed; Tiempo mÃ¡ximo=0,2seg.</remarks>
    Private Sub EjecucionServicio_QA_Certificados_NotificarRevisados()
        EnEjecucionServicio_Certificados_NotificarRevisados = True
        Dim dFechaHora As Date
        Dim dFechaHoraFichero As Date = System.IO.File.GetLastWriteTime(File_FSQA_Certificados_NotificarRevisados)
        dFechaHora = FechaProximaEjecucionServicio_QA_Certificados_NotificarRevisados()
        If DateDiff("n", Now(), dFechaHora) <= 0 Then
            'Decidimos borrar y escribir el fichero antes de la ejecuciÃ³n del servicio propiamente dicho para que si el servicio tarda mÃ¡s de lo debido
            'el archivo tenga fecha de modificaciÃ³n actual y asi no lance por segunda vez el servicio.
            'Borrar Fichero
            BorrarFicheroServicio_QA_Certificados_NotificarRevisados()
            'Escribir Fichero log de certificados notificar revisados, para indicarle cuando se realizo por ultima vez
            EscribirFicheroServicio_QA_Certificados_NotificarRevisados(Now(), dFechaHora, dFechaHoraFichero)
            'LlamarWebService
            QA_Certificados_NotificarRevisados()
        End If
        EnEjecucionServicio_Certificados_NotificarRevisados = False
    End Sub

    ''' <summary>
    ''' Funcion que Calcula la proxima fecha de ejecucion del servicio Certificados NotificarRevisados.
    ''' </summary>
    ''' <returns>Fecha proxima ejecucion del servicio</returns>
    ''' <remarks>Llamada desde=EjecucionServicioCertificados_NotificarRevisados; Tiempo mÃ¡ximo=0,1seg.</remarks>
    Private Function FechaProximaEjecucionServicio_QA_Certificados_NotificarRevisados() As Date
        Dim fechaFichero As Date
        Dim intervaloNotificacion As String
        'If no existe fichero de notificaciones CN
        If Not System.IO.File.Exists(File_FSQA_Certificados_NotificarRevisados) Then
            FechaProximaEjecucionServicio_QA_Certificados_NotificarRevisados = CDate(diaArranque_Certificados_NotificarRevisados & " " & HoraEjecucion_Certificados_NotificarRevisados)
        Else
            'fecha anterior ejecucion con la hora de ejecucion del servicio
            fechaFichero = System.IO.File.GetLastWriteTime(File_FSQA_Certificados_NotificarRevisados)
            FechaProximaEjecucionServicio_QA_Certificados_NotificarRevisados = fechaFichero

            Select Case modoEjecucion_Certificados_NotificarRevisados
                Case "M" ' Minutos
                    intervaloNotificacion = "n"
                Case "H" 'Horas
                    intervaloNotificacion = "h"
                Case Else    'Segundos
                    intervaloNotificacion = "s"
            End Select
            'suma el periodo en horas a la fecha de la ultima ejecucion
            FechaProximaEjecucionServicio_QA_Certificados_NotificarRevisados = DateAdd(intervaloNotificacion, CType(Periodo_Certificados_NotificarRevisados, Double), FechaProximaEjecucionServicio_QA_Certificados_NotificarRevisados)
            FechaProximaEjecucionServicio_QA_Certificados_NotificarRevisados = New Date(Year(FechaProximaEjecucionServicio_QA_Certificados_NotificarRevisados), Month(FechaProximaEjecucionServicio_QA_Certificados_NotificarRevisados), Day(FechaProximaEjecucionServicio_QA_Certificados_NotificarRevisados), Hour(FechaProximaEjecucionServicio_QA_Certificados_NotificarRevisados), Minute(FechaProximaEjecucionServicio_QA_Certificados_NotificarRevisados), 0)
        End If
    End Function

    ''' <summary>
    ''' Borra el fichero log para posteriormente volver a escribirlo
    ''' Escribe mensajes en el event viewer para hacer seguimiento de instalacion
    ''' </summary>
    ''' <remarks>Llamada desde=EjecucionServicioCertificados_NotificarRevisados; Tiempo mÃ¡ximo=0seg.</remarks>
    Private Sub BorrarFicheroServicio_QA_Certificados_NotificarRevisados()
        System.IO.File.Delete(File_FSQA_Certificados_NotificarRevisados)
    End Sub

    ''' <summary>
    ''' Escribe un fichero log para indicarnos cuando se ha ejecutado el webservice
    ''' Escribe mensajes en el event viewer para hacer seguimiento de instalacion
    ''' </summary>
    ''' <param name="fechaActual">Fecha actual del sistema</param>
    ''' <param name="FechaEjecucion">Fecha ejecucion servicio</param>
    ''' <param name="fechaFichero" >Fecha creacion del fichero</param>
    ''' <remarks>Llamada desde=EjecucionServicioCertificados_NotificarRevisados; Tiempo mÃ¡ximo=0seg.</remarks>
    Private Sub EscribirFicheroServicio_QA_Certificados_NotificarRevisados(ByVal fechaActual As Date, ByVal FechaEjecucion As Date, ByVal fechaFichero As Date)
        Dim str = "Fichero de NotificaciÃ³n al proveedor de Notificar Revisados. "
        Dim oFileW As System.IO.StreamWriter
        oFileW = New System.IO.StreamWriter(File_FSQA_Certificados_NotificarRevisados, True)
        oFileW.WriteLine(str)
        oFileW.WriteLine("Fecha Actual = " & fechaActual)
        oFileW.WriteLine("Fecha Ejecucion = " & FechaEjecucion)
        oFileW.WriteLine("Fecha Fichero = " & fechaFichero)
        oFileW.Close()
    End Sub

    ''' <summary>
    ''' Ejecutar el webService con el usuario administrador
    ''' Escribe mensajes en el event viewer para hacer seguimiento de instalacion
    ''' </summary>
    ''' <remarks>Llamada desde=EjecucionServicioCertificados_NotificarRevisados; Tiempo mÃ¡ximo=0,2seg.</remarks>
    Private Sub QA_Certificados_NotificarRevisados()
        Dim oCertificados As FSNServer.Certificados = m_FSNServer.Get_Object(GetType(FSNServer.Certificados))
        oCertificados.NotificarRevisados()
    End Sub
#End Region
#End Region
#Region "Solicitudes Aviso Expiracion"
#Region "Variables Solicitudes Aviso Expiracion"
    Private File_FSPM_Solicitudes_AvisoExpiracion As String
    Private ActivoServicio_Solicitudes_AvisoExpiracion As Boolean = True
    Private modoEjecucion_Solicitudes_AvisoExpiracion As String '[M(minutos),H(horas), Eoc(dÃ­as)]
    Private PeriodoMensual_Solicitudes_AvisoExpiracion As String
    Private ModoMensual_Solicitudes_AvisoExpiracion As String
    Private PosicionDiaMensual_Solicitudes_AvisoExpiracion As String
    Private DiaPeriodoMensual_Solicitudes_AvisoExpiracion As String
    Private diaArranque_Solicitudes_AvisoExpiracion As Date
    Private HoraEjecucion_Solicitudes_AvisoExpiracion As String
    Private EnEjecucionServicio_Solicitudes_AvisoExpiracion As Boolean = False
    Private Periodo_Solicitudes_AvisoExpiracion As String

#End Region
    ''' <summary>
    ''' Se indica el path del fichero log Solicitudes_AvisoExpiracion, los dias de repeticion del servicio y la hora de
    ''' arranque del servicio. Para lanzar la notificaciÃ³n se revisan los distintas solicitudes con el campo fecha  
    ''' y que cumplan con el plazo de antelación y duración preestablecido
    ''' </summary>
    ''' <remarks>Llamada desde=Service1.OnStart; Tiempo mÃ¡ximo=0seg.</remarks>
    Private Sub LeerParametrosEjecucionServicio_PM_Solicitudes_NotificarExpiradas()
        Dim sPath As String

        sPath = ConfigurationManager.AppSettings("Path")

        File_FSPM_Solicitudes_AvisoExpiracion = sPath & "\Solicitudes_AvisoExpiracion.txt"

        If (ConfigurationManager.AppSettings("PERIODO_SERVICIO_NOTIF_AVISO_EXPIRACION") = "") _
        OrElse (ConfigurationManager.AppSettings("HORA_EJECUCION_NOTIF_AVISO_EXPIRACION") = "") _
        OrElse (Not IsDate(ConfigurationManager.AppSettings("DIA_ARRANQUE_SERVICIO_NOTIF_AVISO_EXPIRACION"))) Then
            ActivoServicio_Solicitudes_AvisoExpiracion = False
        Else
            ''Configuracion del modo de ejecucion del servicio.
            'modoEjecucion_Solicitudes_AvisoExpiracion = ConfigurationManager.AppSettings("MODO_EJECUCION_NOTIF_AVISO_EXPIRACION") '[M(minutos),H(horas), Eoc(dÃ­as)]
            HoraEjecucion_Solicitudes_AvisoExpiracion = ConfigurationManager.AppSettings("HORA_EJECUCION_NOTIF_AVISO_EXPIRACION")
            diaArranque_Solicitudes_AvisoExpiracion = ConfigurationManager.AppSettings("DIA_ARRANQUE_SERVICIO_NOTIF_AVISO_EXPIRACION")
            Periodo_Solicitudes_AvisoExpiracion = ConfigurationManager.AppSettings("PERIODO_SERVICIO_NOTIF_AVISO_EXPIRACION")
        End If
    End Sub
    ''' <summary>
    ''' Proceso que comprueba si tiene que lanzar el webService o no. Se comprueba diariamente si en el fichero que se ha escrito le toca ejecutarse o no al webservice
    ''' Cuando ejecuta el webService creo un fichero log que nos indica cuando se ha ejecutado por ultima vez el webService.
    ''' </summary>
    ''' <remarks>Llamada desde=ArrancarTemporizador       temporizador_Elapsed; Tiempo mÃƒÂ¡ximo=0,2seg.</remarks>
    Private Sub EjecucionServicio_PM_Solicitudes_NotificarExpiradas()
        EnEjecucionServicio_Solicitudes_AvisoExpiracion = True
        Dim dFechaHora As Date
        Dim dFechaHoraFichero As Date = System.IO.File.GetLastWriteTime(File_FSPM_Solicitudes_AvisoExpiracion)
        dFechaHora = FechaProximaEjecucionServicio_PM_Solicitudes_NotificarExpiradas()
        If DateDiff("n", Now(), dFechaHora) <= 0 Then
            'Decidimos borrar y escribir el fichero antes de la ejecuciÃƒÂ³n del servicio propiamente dicho para que si el servicio tarda mÃƒÂ¡s de lo debido
            'el archivo tenga fecha de modificaciÃƒÂ³n actual y asi no lance por segunda vez el servicio.
            'Borrar Fichero
            BorrarFicheroServicio_PM_Solicitudes_NotificarExpiradas()
            'Escribir Fichero log de solicitudes aviso expiracion, para indicarle cuando se realizo por ultima vez
            EscribirFicheroServicio_PM_Solicitudes_NotificarExpiradas(Now(), dFechaHora, dFechaHoraFichero)
            'LlamarWebService
            PM_Solicitudes_NotificarExpiradas()
        End If
        EnEjecucionServicio_Solicitudes_AvisoExpiracion = False
    End Sub
    ''' <summary>
    ''' Funcion que Calcula la proxima fecha de ejecucion del servicio Solicitudes AvisoExpiracion.
    ''' </summary>
    ''' <returns>Fecha proxima ejecucion del servicio</returns>
    ''' <remarks>Llamada desde=EjecucionServicioSolicitudes_AvisoExpiracion; Tiempo mÃƒÂ¡ximo=0,1seg.</remarks>
    Private Function FechaProximaEjecucionServicio_PM_Solicitudes_NotificarExpiradas() As Date

        Dim fechaFichero As Date
        'If no existe fichero de notificaciones CN
        If Not System.IO.File.Exists(File_FSPM_Solicitudes_AvisoExpiracion) Then

            FechaProximaEjecucionServicio_PM_Solicitudes_NotificarExpiradas = CDate(diaArranque_Solicitudes_AvisoExpiracion & " " & HoraEjecucion_Solicitudes_AvisoExpiracion)

        Else

            'fecha anterior ejecucion con la hora de ejecucion del servicio
            fechaFichero = System.IO.File.GetLastWriteTime(File_FSPM_Solicitudes_AvisoExpiracion)
            FechaProximaEjecucionServicio_PM_Solicitudes_NotificarExpiradas = fechaFichero

            'suma el periodo en horas a la fecha de la ultima ejecucion
            FechaProximaEjecucionServicio_PM_Solicitudes_NotificarExpiradas = DateAdd("d", CType(Periodo_Solicitudes_AvisoExpiracion, Double), FechaProximaEjecucionServicio_PM_Solicitudes_NotificarExpiradas)
            FechaProximaEjecucionServicio_PM_Solicitudes_NotificarExpiradas = New Date(Year(FechaProximaEjecucionServicio_PM_Solicitudes_NotificarExpiradas), Month(FechaProximaEjecucionServicio_PM_Solicitudes_NotificarExpiradas), Day(FechaProximaEjecucionServicio_PM_Solicitudes_NotificarExpiradas), Hour(FechaProximaEjecucionServicio_PM_Solicitudes_NotificarExpiradas), Minute(FechaProximaEjecucionServicio_PM_Solicitudes_NotificarExpiradas), 0)

        End If
    End Function
    ''' <summary>
    ''' Borra el fichero log para posteriormente volver a escribirlo
    ''' Escribe mensajes en el event viewer para hacer seguimiento de instalacion
    ''' </summary>
    ''' <remarks>Llamada desde=EjecucionServicioSolicitudes_AvisoExpiracion; Tiempo mÃƒÂ¡ximo=0seg.</remarks>
    Private Sub BorrarFicheroServicio_PM_Solicitudes_NotificarExpiradas()
        System.IO.File.Delete(File_FSPM_Solicitudes_AvisoExpiracion)
    End Sub
    ''' <summary>
    ''' Escribe un fichero log para indicarnos cuando se ha ejecutado el webservice
    ''' Escribe mensajes en el event viewer para hacer seguimiento de instalacion
    ''' </summary>
    ''' <param name="fechaActual">Fecha actual del sistema</param>
    ''' <param name="FechaEjecucion">Fecha ejecucion servicio</param>
    ''' <param name="fechaFichero" >Fecha creacion del fichero</param>
    ''' <remarks>Llamada desde=EjecucionServicioSolicitudes_AvisoExpiracion; Tiempo mÃƒÂ¡ximo=0seg.</remarks>
    Private Sub EscribirFicheroServicio_PM_Solicitudes_NotificarExpiradas(ByVal fechaActual As Date, ByVal FechaEjecucion As Date, ByVal fechaFichero As Date)
        Dim str = "Fichero de Solicitudes de Aviso Expiracion. "
        Dim oFileW As System.IO.StreamWriter
        oFileW = New System.IO.StreamWriter(File_FSPM_Solicitudes_AvisoExpiracion, True)
        oFileW.WriteLine(str)
        oFileW.WriteLine("Fecha Actual = " & fechaActual)
        oFileW.WriteLine("Fecha Ejecucion = " & FechaEjecucion)
        oFileW.WriteLine("Fecha Fichero = " & fechaFichero)
        oFileW.Close()
    End Sub
    ''' <summary>
    ''' Ejecutar el webService con el usuario administrador
    ''' Escribe mensajes en el event viewer para hacer seguimiento de instalacion
    ''' </summary>
    ''' <remarks>Llamada desde=EjecucionServicioSolicitudes_AvisoExpiracion; Tiempo mÃƒÂ¡ximo=0,2seg.</remarks>
    Private Sub PM_Solicitudes_NotificarExpiradas()
        Dim oSolicitudes As FSNServer.Solicitudes = m_FSNServer.Get_Object(GetType(FSNServer.Solicitudes))
        oSolicitudes.NotificarExpirados()
    End Sub
#End Region
#Region "Notificación Disponible Negativo"
#Region "Variables Notificación Disponible Negativo"
    Private File_FSSM_NotifDisponibleNegativo As String
    Private ActivoServicio_SMNotifDisponibleNegativo As Boolean = True
    Private diaArranque_SMNotifDisponibleNegativo As Date
    Private HoraEjecucion_SMNotifDisponibleNegativo As String
    Private Periodo_SMNotifDisponibleNegativo As String
    Private modoEjecucion_SMNotifDisponibleNegativo As String
#End Region
    ''' <summary>
    ''' Se indica el path del fichero log log_SMNotifDisponibleNegativo, la repeticion del servicio y la hora de
    ''' arranque del servicio. Para lanzar la notificación se revisan los distintas solicitudes con el campo fecha  
    ''' y que cumplan con el plazo de antelación y duración preestablecido
    ''' </summary>
    ''' <remarks>Llamada desde=Service1.OnStart; Tiempo mÃ¡ximo=0seg.</remarks>
    Private Sub LeerParametrosEjecucionServicio_SM_NotificarDisponibleNegativo()
        Dim sPath As String
        sPath = ConfigurationManager.AppSettings("Path")
        File_FSSM_NotifDisponibleNegativo = sPath & "\SMNotifDisponibleNegativo.txt"

        If ConfigurationManager.AppSettings("MODO_EJECUCION_SMNotifDisponibleNegativo") = "" _
        OrElse (ConfigurationManager.AppSettings("HORA_EJECUCION_SMNotifDisponibleNegativo") = "") _
        OrElse (Not IsDate(ConfigurationManager.AppSettings("DIA_ARRANQUE_SERVICIO_SMNotifDisponibleNegativo"))) Then
            ActivoServicio_SMNotifDisponibleNegativo = False
        Else

            ''Configuracion del modo de ejecucion del servicio.
            ''Toma los parametros de la configuracion.
            modoEjecucion_SMNotifDisponibleNegativo = ConfigurationManager.AppSettings("MODO_EJECUCION_SMNotifDisponibleNegativo")
            Periodo_SMNotifDisponibleNegativo = ConfigurationManager.AppSettings("PERIODO_SMNotifDisponibleNegativo")
            HoraEjecucion_SMNotifDisponibleNegativo = ConfigurationManager.AppSettings("HORA_EJECUCION_SMNotifDisponibleNegativo")
            diaArranque_SMNotifDisponibleNegativo = ConfigurationManager.AppSettings("DIA_ARRANQUE_SERVICIO_SMNotifDisponibleNegativo")
        End If
    End Sub
    ''' <summary>
    ''' Proceso que comprueba si tiene que lanzar el webService o no. Se comprueba diariamente si en el fichero que se ha escrito le toca ejecutarse o no al webservice
    ''' Cuando ejecuta el webService creo un fichero log que nos indica cuando se ha ejecutado por ultima vez el webService.
    ''' </summary>
    ''' <remarks>Llamada desde=ArrancarTemporizador       temporizador_Elapsed; Tiempo maximo=0,2seg.</remarks>
    Private Function EjecucionServicio_SM_NotificarDisponibleNegativo() As Boolean
        EjecucionServicio_SM_NotificarDisponibleNegativo = True
        Dim dFechaHora As Date
        Dim dFechaHoraFichero As Date = System.IO.File.GetLastWriteTime(File_FSSM_NotifDisponibleNegativo)
        dFechaHora = FechaProximaEjecucionServicio_SM_NotificarDisponibleNegativo()
        If DateDiff("n", Now(), dFechaHora) <= 0 Then
            'Decidimos borrar y escribir el fichero antes de la ejecucion del servicio propiamente dicho para que si el servicio tarda mas de lo debido
            'el archivo tenga fecha de modificacion actual y asi no lance por segunda vez el servicio.
            'Borrar Fichero
            BorrarFicheroServicio_SM_NotificarDisponibleNegativo()
            'Escribir Fichero log, para indicarle cuando se realizo por ultima vez
            EscribirFicheroServicio_SM_NotificarDisponibleNegativo(Now(), dFechaHora, dFechaHoraFichero)
            'LlamarWebService
            SM_NotificarDisponibleNegativo()
        End If
        EjecucionServicio_SM_NotificarDisponibleNegativo = False
    End Function
    ''' <summary>
    ''' Funcion que Calcula la proxima fecha de ejecucion del servicio de disponible negativo
    ''' </summary>
    ''' <returns>Fecha proxima ejecucion del servicio</returns>
    ''' <remarks>Llamada desde=EjecucionServicio_SM_NotificarDisponibleNegativo; Tiempo mÃƒÂ¡ximo=0,1seg.</remarks>
    Private Function FechaProximaEjecucionServicio_SM_NotificarDisponibleNegativo() As Date
        Dim fechaFichero As Date
        Dim intervaloNotificacion As String
        'If no existe fichero de notificaciones de disponible negativo
        If Not System.IO.File.Exists(File_FSSM_NotifDisponibleNegativo) Then
            FechaProximaEjecucionServicio_SM_NotificarDisponibleNegativo = CDate(diaArranque_SMNotifDisponibleNegativo & " " & HoraEjecucion_SMNotifDisponibleNegativo)
        Else
            'fecha anterior ejecucion con la hora de ejecucion del servicio
            fechaFichero = System.IO.File.GetLastWriteTime(File_FSSM_NotifDisponibleNegativo)
            FechaProximaEjecucionServicio_SM_NotificarDisponibleNegativo = fechaFichero
            Select Case modoEjecucion_SMNotifDisponibleNegativo
                Case "D" ' Dias
                    intervaloNotificacion = "d"
                Case "H" 'Horas
                    intervaloNotificacion = "h"
                Case "M" ' Minutos
                    intervaloNotificacion = "n"
                Case Else 'Segundos
                    intervaloNotificacion = "s"
            End Select
            'suma el periodo en horas a la fecha de la ultima ejecucion
            FechaProximaEjecucionServicio_SM_NotificarDisponibleNegativo = DateAdd(intervaloNotificacion, CType(Periodo_SMNotifDisponibleNegativo, Double), FechaProximaEjecucionServicio_SM_NotificarDisponibleNegativo)
            FechaProximaEjecucionServicio_SM_NotificarDisponibleNegativo = New Date(Year(FechaProximaEjecucionServicio_SM_NotificarDisponibleNegativo), Month(FechaProximaEjecucionServicio_SM_NotificarDisponibleNegativo), Day(FechaProximaEjecucionServicio_SM_NotificarDisponibleNegativo), Hour(FechaProximaEjecucionServicio_SM_NotificarDisponibleNegativo), Minute(FechaProximaEjecucionServicio_SM_NotificarDisponibleNegativo), 0)
        End If
    End Function
    ''' <summary>
    ''' Borra el fichero log para posteriormente volver a escribirlo
    ''' Escribe mensajes en el event viewer para hacer seguimiento de instalacion
    ''' </summary>
    ''' <remarks>Llamada desde=EjecucionServicioSolicitudes_AvisoExpiracion; Tiempo mÃƒÂ¡ximo=0seg.</remarks>
    Private Sub BorrarFicheroServicio_SM_NotificarDisponibleNegativo()
        System.IO.File.Delete(File_FSSM_NotifDisponibleNegativo)
    End Sub
    ''' <summary>
    ''' Escribe un fichero log para indicarnos cuando se ha ejecutado el webservice
    ''' Escribe mensajes en el event viewer para hacer seguimiento de instalacion
    ''' </summary>
    ''' <param name="fechaActual">Fecha actual del sistema</param>
    ''' <param name="FechaEjecucion">Fecha ejecucion servicio</param>
    ''' <param name="fechaFichero" >Fecha creacion del fichero</param>
    ''' <remarks>Llamada desde=EjecucionServicioSolicitudes_AvisoExpiracion; Tiempo mÃƒÂ¡ximo=0seg.</remarks>
    Private Sub EscribirFicheroServicio_SM_NotificarDisponibleNegativo(ByVal fechaActual As Date, ByVal FechaEjecucion As Date, ByVal fechaFichero As Date)
        Dim str = "Fichero de Notificacion disponible negativo. "
        Dim oFileW As System.IO.StreamWriter
        oFileW = New System.IO.StreamWriter(File_FSSM_NotifDisponibleNegativo, True)
        oFileW.WriteLine(str)
        oFileW.WriteLine("Fecha Actual = " & fechaActual)
        oFileW.WriteLine("Fecha Ejecucion = " & FechaEjecucion)
        oFileW.WriteLine("Fecha Fichero = " & fechaFichero)
        oFileW.Close()
    End Sub
    ''' <summary>
    ''' Ejecutar el webService con el usuario administrador
    ''' Escribe mensajes en el event viewer para hacer seguimiento de instalacion
    ''' </summary>
    ''' <remarks>Llamada desde=EjecucionServicioSolicitudes_AvisoExpiracion; Tiempo mÃƒÂ¡ximo=0,2seg.</remarks>
    Private Sub SM_NotificarDisponibleNegativo()
        Dim oPartidasPRES5 As FSNServer.PartidasPRES5 = m_FSNServer.Get_Object(GetType(FSNServer.PartidasPRES5))
        oPartidasPRES5.NotificarDisponibleNegativo()
    End Sub
#End Region
#Region "Notificaciones Inicio Tarea FSGA"
#Region "Variables Tarea FSGA"
    Private m_sFile_FSGA As String
    Private m_diaArranque_FSGA As Date
    Private m_sHoraEjecucionNotifFechaFin_FSGA As String
    Private m_bActivoServicioNotificacionInicioTareaFSGA As Boolean = True
    '============================================================

    Private EnEjecucionServicioNotificacionInicioTareaFSGA As Boolean = False
#End Region
    ''' <summary>
    ''' Se indica el path del fichero log FSGA_Notif, la repeticion del servicio, sera diaría y a 
    ''' la hora indicada en el app.config (fichero mantenido por aplicación "instalador de servicios"). La 1ª 
    ''' vez se lanza la notificación y los dias sucesivos se comprobara si le toca o no ejecutar el webservice a 
    ''' traves del parametro Hora de Ejecucion
    ''' </summary>
    ''' <remarks>Llamada desde=Service1.OnStart; Tiempo máximo=0seg.</remarks>
    Private Sub LeerParametrosEjecucionServicio_FSGA_NotificacionInicioTarea()
        Dim sPath As String

        sPath = ConfigurationManager.AppSettings("Path")

        m_sFile_FSGA = sPath & "\FSGA_Notif.txt"

        If (ConfigurationManager.AppSettings("HORA_EJECUCION_FSGA") = "") _
        OrElse (Not IsDate(ConfigurationManager.AppSettings("DIA_ARRANQUE_SERVICIO_FSGA"))) Then
            m_bActivoServicioNotificacionInicioTareaFSGA = False
        Else
            m_sHoraEjecucionNotifFechaFin_FSGA = ConfigurationManager.AppSettings("HORA_EJECUCION_FSGA")

            m_diaArranque_FSGA = ConfigurationManager.AppSettings("DIA_ARRANQUE_SERVICIO_FSGA")
        End If
    End Sub
    ''' <summary>
    ''' Borra el fichero log para posteriormente volver a escribirlo
    ''' Escribe mensajes en el event viewer para hacer seguimiento de instalacion
    ''' </summary>
    ''' <remarks>Llamada desde=Service1.vb; Tiempo máximo=0seg.</remarks>
    Private Sub BorrarFicheroServicio_FSGA_NotificacionInicioTarea()
        System.IO.File.Delete(m_sFile_FSGA)
    End Sub
    ''' <summary>
    ''' Escribe un fichero log para indicarnos cuando se ha ejecutado el webservice
    ''' Escribe mensajes en el event viewer para hacer seguimiento de instalacion
    ''' <paramref name="fechaActual" >Fecha actual del sistema</paramref>
    ''' <paramref name="FechaEjecucion">Fecha ejecucion servicio</paramref>
    ''' <paramref name="fechaFichero" >Fecha creacion del fichero</paramref>
    ''' </summary>
    ''' <remarks>Llamada desde=Service1.vb; Tiempo máximo=0seg.</remarks>
    Private Sub EscribirFicheroServicio_FSGA_NotificacionInicioTarea(ByVal fechaActual As Date, ByVal FechaEjecucion As Date, ByVal fechaFichero As Date)
        Dim str = "Fichero de Notificación al usuario de FSGA de la proximidad del inicio de las tareas que tiene asignadas. "
        Dim oFileW As System.IO.StreamWriter
        oFileW = New System.IO.StreamWriter(m_sFile_FSGA, True)
        oFileW.WriteLine(str)
        oFileW.WriteLine("Fecha Actual = " & fechaActual)
        oFileW.WriteLine("Fecha Ejecucion = " & FechaEjecucion)
        oFileW.WriteLine("Fecha Fichero = " & fechaFichero)
        oFileW.Close()
    End Sub
    ''' <summary>
    ''' Proceso que comprueba si tiene que lanzar el webService o no. Se comprueba diariamente si en el fichero que se ha escrito le toca ejecutarse o no al webservice
    ''' Cuando ejecuta el webService creo un fichero log que nos indica cuando se ha ejecutado por ultima vez el webService.
    ''' </summary>
    ''' <remarks>Llamada desde=ArrancarTemporizador       temporizador_Elapsed; Tiempo máximo=0,2seg.</remarks>
    Private Sub EjecucionServicio_FSGA_NotificacionInicioTarea()
        EnEjecucionServicioNotificacionInicioTareaFSGA = True
        Dim dFecha As Date
        Dim dFechaFichero As Date = System.IO.File.GetLastWriteTime(m_sFile_FSGA)
        dFecha = FechaProximaEjecucionServicio_FSGA_NotificacionInicioTarea()
        If DateDiff("n", Now(), dFecha) <= 0 Then
            'Decidimos borrar y escribir el fichero antes de la ejecución del servicio propiamente dicho para que si el servicio tarda más de lo debido
            'el archivo tenga fecha de modificación actual y asi no lance por segunda vez el servicio, evitando mails duplicados.
            'Borrar Fichero
            BorrarFicheroServicio_FSGA_NotificacionInicioTarea()
            'Escribir Fichero log de puntuaciones, para indicarle cuando se realizo el calculo por ultima vez
            EscribirFicheroServicio_FSGA_NotificacionInicioTarea(Now(), dFecha, dFechaFichero)
            'LlamarWebService
            FSGA_NotificacionInicioTarea()
        End If
        EnEjecucionServicioNotificacionInicioTareaFSGA = False
    End Sub
    ''' <summary>
    ''' Ejecutar el webService con el usuario administrador
    ''' Escribe mensajes en el event viewer para hacer seguimiento de instalacion
    ''' </summary>
    ''' <remarks>Llamada desde=Service1.vb; Tiempo máximo=0,2seg.</remarks>
    Private Sub FSGA_NotificacionInicioTarea()
        Dim oProyecto As FSNServer.Proyecto = m_FSNServer.Get_Object(GetType(FSNServer.Proyecto))
        oProyecto.Notificar_InicioTarea_FSGA()
    End Sub
    ''' <summary>
    ''' Funcion que calcula la proxima fecha de ejecucion del servicio.
    ''' </summary>
    ''' <returns>Fecha proxima ejecucion del servicio</returns>
    ''' <remarks>Llamada desde=propia pagina; Tiempo máximo=0,4seg.</remarks>
    Private Function FechaProximaEjecucionServicio_FSGA_NotificacionInicioTarea() As Date
        Dim fechaFichero As Date
        'If no existe fichero de proximidad NC
        If Not System.IO.File.Exists(m_sFile_FSGA) Then
            FechaProximaEjecucionServicio_FSGA_NotificacionInicioTarea = CDate(m_diaArranque_FSGA & " " & m_sHoraEjecucionNotifFechaFin_FSGA)
        Else
            'fecha anterior ejecucion con la hora de ejecucion del servicio
            fechaFichero = System.IO.File.GetLastWriteTime(m_sFile_FSGA)
            FechaProximaEjecucionServicio_FSGA_NotificacionInicioTarea = fechaFichero

            'suma el periodo diario en dias a la fecha de la ultima ejecucion
            FechaProximaEjecucionServicio_FSGA_NotificacionInicioTarea = DateAdd("d", 1, FechaProximaEjecucionServicio_FSGA_NotificacionInicioTarea)

            FechaProximaEjecucionServicio_FSGA_NotificacionInicioTarea = New Date(Year(FechaProximaEjecucionServicio_FSGA_NotificacionInicioTarea), Month(FechaProximaEjecucionServicio_FSGA_NotificacionInicioTarea), Day(FechaProximaEjecucionServicio_FSGA_NotificacionInicioTarea), Hour(m_sHoraEjecucionNotifFechaFin_FSGA), Minute(m_sHoraEjecucionNotifFechaFin_FSGA), 0)
        End If
    End Function
#End Region
#Region "Monitorizacion Integracion"
#Region "Variables Integracion"
    Private m_sFile_INT As String

    Private m_TimerMonitorizaINT As Double 'Cada cuanto se monitoriza el funcionamiento del servicio de integracion
    Private m_fMonitorizaINT As String '(S, M, H)
    Private m_diaArranque_INT As Date 'Dia de arranque
    Private m_PeriodIMP As Double 'Periodicidad de importacon del servicio de integracion
    Private m_fPeriodoIMP As String '(S, M, H)
    Private m_TExtraIMP As Double 'Tiempo extra a sumar a la periodicidad de importacion; superado ese tiempo se concluye que hay un fallo
    Private m_fExtraIMP As String '(S, M, H)
    Private m_PeriodEXP As Double 'Periodicidad de exportacion del servicio de integracion
    Private m_fPeriodoEXP As String '(S, M, H)
    Private m_TExtraEXP As Double 'Tiempo extra a sumar a la periodicidad de exportacion; superado ese tiempo se concluye que hay un fallo
    Private m_fExtraEXP As String '(S, M, H)
    Private m_TiempoEjecucion As Double 'Tiempo durante el que el servicio de integracion puede estar importando o exportando datos
    Private m_fEjecucion As String '(S, M, H)
    Private m_TRecordAlerta As Double 'Tiempo de espera para eviar el mail recordatorio de alerta por tiempos excedidos
    Private m_fRecordAlerta As String '(S, M, H)

    ''Si no se introduce frecuencia de ejecucion, periodo de ejecucion o dia de arranque, que no se ejecute 
    Private m_bActivoServicioMonitorizaInt As Boolean = True
    'Periodicidad de importacion del servicio de integracion
    Private EnEjecucionServicioMonitorizacionINT As Boolean = False
    Private m_sNombreServicioINT As String
#End Region
    ''' <summary>
    ''' Se indica el path del fichero log MonitorizaINT, la repeticion del servicio. se comprobara si le toca o no ejecutar el webservice a 
    ''' traves del parametro frecuencia_int
    ''' </summary>
    ''' <remarks>Llamada desde=Service1.OnStart; Tiempo maximo=0seg.</remarks>
    ''' Revisado: ngo 22/06/2012
    Private Sub LeerParametrosEjecucionServicio_INT_Monitorizacion()
        Dim sPath As String

        sPath = ConfigurationManager.AppSettings("Path")

        m_sFile_INT = sPath & "\MonitorizaINT.txt"

        If ConfigurationManager.AppSettings("FRECUENCIA_INT") = "" _
        OrElse (ConfigurationManager.AppSettings("PERIODO_INT") = "") _
        OrElse (Not IsDate(ConfigurationManager.AppSettings("DIA_ARRANQUE_SERVICIO_INT"))) Then
            m_bActivoServicioMonitorizaInt = False
        Else
            ''Toma los parametros de la configuracion.

            ''Nombre del servicio:

            m_sNombreServicioINT = ConfigurationManager.AppSettings("NombreServicioINT")
            m_TimerMonitorizaINT = ConfigurationManager.AppSettings("PERIODO_INT")  ''Cada cuanto se ejecuta
            m_diaArranque_INT = ConfigurationManager.AppSettings("DIA_ARRANQUE_SERVICIO_INT")  ''Fecha de arranque del servicio
            m_fMonitorizaINT = ConfigurationManager.AppSettings("FRECUENCIA_INT") ''Frecuencia (segundos, minutos, horas)
            m_PeriodIMP = ConfigurationManager.AppSettings("PeriodoIMP") ''Periodicidad importacion
            m_fPeriodoIMP = ConfigurationManager.AppSettings("FrecuenciaIMP")
            m_TExtraIMP = ConfigurationManager.AppSettings("TExtraIMP")  ''Tiempo extra durante el que se considera aceptable que no haya saltado la importacion de datos
            m_fExtraIMP = ConfigurationManager.AppSettings("FrecuenciaTExtraIMP")
            m_PeriodEXP = ConfigurationManager.AppSettings("PeriodoEXP") ''Periodicidad exportacion
            m_fPeriodoEXP = ConfigurationManager.AppSettings("FrecuenciaEXP")
            m_TExtraEXP = ConfigurationManager.AppSettings("TExtraEXP") ''Tiempo extra durante el que se considera aceptable que no haya saltado la exportacion de datos
            m_fExtraEXP = ConfigurationManager.AppSettings("FrecuenciaTExtraEXP")
            m_TiempoEjecucion = ConfigurationManager.AppSettings("TiempoEjecucion") ''Tiempo de ejecucion del servicio de integracion (tanto para importacion como para exportacion)
            m_fEjecucion = ConfigurationManager.AppSettings("FrecuenciaEjecucion")
            m_TRecordAlerta = ConfigurationManager.AppSettings("RecordatorioAlerta") ''Tiempo de espera para eviar el mail recordatorio de alerta por tiempos excedidos
            m_fRecordAlerta = ConfigurationManager.AppSettings("FrecuenciaRecordatorioAlerta")

            'Se pasan todos los tiempos a segundos
            Select Case UCase(m_fMonitorizaINT)
                Case "M"
                    m_TimerMonitorizaINT = m_TimerMonitorizaINT * 60
                Case "H"
                    m_TimerMonitorizaINT = m_TimerMonitorizaINT * 3600
                Case "S"
                    m_TimerMonitorizaINT = m_TimerMonitorizaINT
            End Select

            Select Case UCase(m_fPeriodoIMP)
                Case "M"
                    m_PeriodIMP = m_PeriodIMP * 60
                Case "H"
                    m_PeriodIMP = m_PeriodIMP * 3600
                Case "S"
                    m_PeriodIMP = m_PeriodIMP
            End Select

            Select Case UCase(m_fExtraIMP)
                Case "M"
                    m_TExtraIMP = m_TExtraIMP * 60
                Case "H"
                    m_TExtraIMP = m_TExtraIMP * 3600
                Case "S"
                    m_TExtraIMP = m_TExtraIMP
            End Select

            Select Case UCase(m_fPeriodoEXP)
                Case "M"
                    m_PeriodEXP = m_PeriodEXP * 60
                Case "H"
                    m_PeriodEXP = m_PeriodEXP * 3600
                Case "S"
                    m_PeriodEXP = m_PeriodEXP
            End Select

            Select Case UCase(m_fExtraEXP)
                Case "M"
                    m_TExtraEXP = m_TExtraEXP * 60
                Case "H"
                    m_TExtraEXP = m_TExtraEXP * 3600
                Case "S"
                    m_TExtraEXP = m_TExtraEXP
            End Select

            Select Case UCase(m_fEjecucion)
                Case "M"
                    m_TiempoEjecucion = m_TiempoEjecucion * 60
                Case "H"
                    m_TiempoEjecucion = m_TiempoEjecucion * 3600
                Case "S"
                    m_TiempoEjecucion = m_TiempoEjecucion
            End Select

        End If
    End Sub
    ''' <summary>
    ''' Borra el fichero log para posteriormente volver a escribirlo
    ''' Escribe mensajes en el event viewer para hacer seguimiento de instalacion
    ''' </summary>
    ''' <remarks>Llamada desde=Service1.vb; Tiempo maximo=0seg.</remarks>
    ''' Revisado: ngo 22/06/2012
    Private Sub BorrarFicheroServicio_INT_Monitorizacion()
        System.IO.File.Delete(m_sFile_INT)
    End Sub
    ''' <summary>
    ''' Escribe un fichero log para indicarnos cuando se ha ejecutado el webservice
    ''' Escribe mensajes en el event viewer para hacer seguimiento de instalacion
    ''' <paramref name="fechaActual" >Fecha actual del sistema</paramref>
    ''' <paramref name="FechaEjecucion">Fecha ejecucion servicio</paramref>
    ''' <paramref name="fechaFichero" >Fecha creacion del fichero</paramref>
    ''' </summary>
    ''' <remarks>Llamada desde=Service1.vb; Tiempo mÃƒÂ¡ximo=0seg.</remarks>
    ''' Revisao: ngo 22/06/2012
    Private Sub EscribirFicheroServicio_INT_Monitorizacion(ByVal fechaActual As Date, ByVal FechaEjecucion As Date, ByVal fechaFichero As Date)
        Dim str = "Fichero de monitorizacion del funcionamiento del servicio de integracion "
        Dim oFileW As System.IO.StreamWriter
        oFileW = New System.IO.StreamWriter(m_sFile_INT, True)
        oFileW.WriteLine(str)
        oFileW.WriteLine("Fecha Actual = " & fechaActual)
        oFileW.WriteLine("Fecha Ejecucion = " & FechaEjecucion)
        oFileW.WriteLine("Fecha Fichero = " & fechaFichero)
        oFileW.Close()
    End Sub
    ''' <summary>
    ''' Proceso que comprueba si tiene que lanzar el webService o no. Se comprueba diariamente si en el fichero que se ha escrito le toca ejecutarse o no al webservice
    ''' Cuando ejecuta el webService creo un fichero log que nos indica cuando se ha ejecutado por ultima vez el webService.
    ''' </summary>
    ''' <remarks>Llamada desde=ArrancarTemporizador       temporizador_Elapsed; Tiempo maximo=0,2seg.</remarks>
    ''' Revisado: ngo 22/06/2012
    Private Sub EjecucionServicio_INT_Monitorizacion()
        EnEjecucionServicioMonitorizacionINT = True
        Dim dFecha As Date
        Dim dFechaFichero As Date = System.IO.File.GetLastWriteTime(m_sFile_INT)
        dFecha = FechaProximaEjecucionServicio_INT_Monitorizacion()
        If DateDiff("n", Now(), dFecha) <= 0 Then
            'Decidimos borrar y escribir el fichero antes de la ejecucion del servicio propiamente dicho para que si el servicio tarda mas de lo debido
            'el archivo tenga fecha de modificacion actual y asi no lance por segunda vez el servicio, evitando mails duplicados.
            'Borrar Fichero
            BorrarFicheroServicio_INT_Monitorizacion()
            'Escribir Fichero log de puntuaciones, para indicarle cuando se realizo el calculo por ultima vez
            EscribirFicheroServicio_INT_Monitorizacion(Now(), dFecha, dFechaFichero)
            'LlamarWebService
            INT_Monitorizacion()
        End If
        EnEjecucionServicioMonitorizacionINT = False
    End Sub
    ''' <summary>
    ''' Ejecutar el webService con el usuario administrador
    ''' Escribe mensajes en el event viewer para hacer seguimiento de instalacion
    ''' </summary>
    ''' <remarks>Llamada desde=Service1.vb; Tiempo maximo=0,2seg.</remarks>
    Private Sub INT_Monitorizacion()
        Dim dFecha As Date
        Dim i As Integer
        Dim bRecordatorio As Boolean

        Try
            Dim TiempoIMP As Double = m_PeriodIMP + m_TExtraIMP
            Dim TiempoEXP As Double = m_PeriodEXP + m_TExtraEXP

            Dim oIntegracion As FSNServer.VisorInt = m_FSNServer.Get_Object(GetType(FSNServer.VisorInt))
            Dim bError As Boolean = oIntegracion.ComprobarParadaServicioIntegracion(TiempoIMP, TiempoEXP, m_TiempoEjecucion, dFecha)
            If bError Then

                bRecordatorio = oIntegracion.HayParadaIntegracionPrevia(dFecha, m_TRecordAlerta, m_fRecordAlerta)

                If bRecordatorio Then
                    'Hay una parada en el servicio de integración
                    oIntegracion.NotificarParadaIntegracion(dFecha)
                End If

            Else
                ''Se comprueba si ha habido una parada del servicio previa

                bRecordatorio = oIntegracion.HayParadaIntegracionPrevia(dFecha, m_TRecordAlerta, m_fRecordAlerta)

                If bRecordatorio Then
                    'Hay una parada en el servicio de integración
                    oIntegracion.NotificarParadaIntegracion(dFecha)
                Else
                    ''Se comprueba si hay alguna alerta por tiempo excedido: Recepción de Acuse y/o Generación de XML
                    oIntegracion.ComprobarAlertaTiempoExcedido(dFecha, m_TRecordAlerta, m_fRecordAlerta)
                End If

            End If
        Catch ex As Exception
            GuardarErrorEnBaseDatos(ex, "INT_Monitorizacion")
        End Try
    End Sub
    ''' <summary>
    ''' Funcion que calcula la proxima fecha de ejecucion del servicio.
    ''' </summary>
    ''' <returns>Fecha proxima ejecucion del servicio</returns>
    ''' <remarks>Llamada desde=propia pagina; Tiempo mÃƒÂ¡ximo=0,4seg.</remarks>
    Private Function FechaProximaEjecucionServicio_INT_Monitorizacion() As Date
        Dim fechaFichero As Date
        If Not System.IO.File.Exists(m_sFile_INT) Then
            FechaProximaEjecucionServicio_INT_Monitorizacion = CDate(m_diaArranque_INT)
        Else
            'fecha anterior ejecucion con la hora de ejecucion del servicio
            fechaFichero = System.IO.File.GetLastWriteTime(m_sFile_INT)
            FechaProximaEjecucionServicio_INT_Monitorizacion = fechaFichero

            'suma el periodo diario en dias a la fecha de la ultima ejecucion
            FechaProximaEjecucionServicio_INT_Monitorizacion = DateAdd("s", CDbl(m_TimerMonitorizaINT), FechaProximaEjecucionServicio_INT_Monitorizacion)
            FechaProximaEjecucionServicio_INT_Monitorizacion = New Date(Year(FechaProximaEjecucionServicio_INT_Monitorizacion), Month(FechaProximaEjecucionServicio_INT_Monitorizacion), Day(FechaProximaEjecucionServicio_INT_Monitorizacion), Hour(FechaProximaEjecucionServicio_INT_Monitorizacion), Minute(FechaProximaEjecucionServicio_INT_Monitorizacion), 0)
        End If
    End Function
#End Region
#Region "Collaboration network"
#Region "Variables Servicio Notificaciones Collaboration Network"
    Private File_FSCN As String
    Private ActivoServicioNotificacionCN As Boolean = True

    Private modoEjecucion_Notificaciones_CN As String '[M(minutos),H(horas)]
    Private PeriodoNotificaciones_CN As String

    Private diaArranqueNotif_FSCN As Date
    Private HoraEjecucionNotif_FSCN As String
    Private EnEjecucionServicioNotificacionFSCN As Boolean = False
#End Region
#Region "Servicio Notificaciones CN"
    ''' <summary>
    ''' Se indica el path del fichero log FSCN_Notif, la repeticion del servicio, sera diaría y a 
    ''' la hora indicada en el app.config (fichero mantenido por aplicación "instalador de servicios"). La 1ª 
    ''' vez se lanza la notificación y en las sucesivas se comprobara si le toca o no ejecutar el webservice a 
    ''' traves del parametro Hora de Ejecucion
    ''' </summary>
    ''' <remarks>Llamada desde=Service1.OnStart; Tiempo máximo=0seg.</remarks>
    Private Sub LeerParametrosEjecucionServicio_CN_NotificacionActividad()
        Dim sPath As String
        sPath = ConfigurationManager.AppSettings("Path")

        File_FSCN = sPath & "\FSCN_Notif.txt"

        If ConfigurationManager.AppSettings("MODO_EJECUCION_NOTIFICACIONES_CN") = "" _
        OrElse (ConfigurationManager.AppSettings("HORA_EJECUCION_NOTIFICACIONES_CN") = "") _
        OrElse (Not IsDate(ConfigurationManager.AppSettings("DIA_ARRANQUE_SERVICIO_NOTIFICACIONES_CN"))) Then
            ActivoServicioNotificacionCN = False
        Else
            ''Configuracion del modo de ejecucion del servicio.
            ''Toma los parametros de la configuracion.
            modoEjecucion_Notificaciones_CN = ConfigurationManager.AppSettings("MODO_EJECUCION_NOTIFICACIONES_CN")
            PeriodoNotificaciones_CN = ConfigurationManager.AppSettings("PERIODO_NOTIFICACIONES_CN")
            HoraEjecucionNotif_FSCN = ConfigurationManager.AppSettings("HORA_EJECUCION_NOTIFICACIONES_CN")
            diaArranqueNotif_FSCN = ConfigurationManager.AppSettings("DIA_ARRANQUE_SERVICIO_NOTIFICACIONES_CN")
        End If
    End Sub
    ''' <summary>
    ''' Proceso que comprueba si tiene que lanzar el webService o no. Se comprueba diariamente si en el fichero que se ha escrito le toca ejecutarse o no al webservice
    ''' Cuando ejecuta el webService creo un fichero log que nos indica cuando se ha ejecutado por ultima vez el webService.
    ''' </summary>
    ''' <remarks>Llamada desde=ArrancarTemporizador       temporizador_Elapsed; Tiempo máximo=0,2seg.</remarks>
    Private Sub EjecucionServicio_CN_NotificacionActividad()
        EnEjecucionServicioNotificacionFSCN = True
        Dim dFechaHora As Date
        Dim dFechaHoraFichero As Date = System.IO.File.GetLastWriteTime(File_FSCN)
        dFechaHora = FechaProximaEjecucionServicio_CN_NotificacionActividad()
        If DateDiff("n", Now(), dFechaHora) <= 0 Then
            'Decidimos borrar y escribir el fichero antes de la ejecución del servicio propiamente dicho para que si el servicio tarda más de lo debido
            'el archivo tenga fecha de modificación actual y asi no lance por segunda vez el servicio, evitando mails duplicados.
            'Borrar Fichero
            BorrarFicheroServicio_CN_NotificacionActividad()
            'Escribir Fichero log de puntuaciones, para indicarle cuando se realizo el calculo por ultima vez
            EscribirFicheroServicio_CN_NotificacionActividad(Now(), dFechaHora, dFechaHoraFichero)
            'LlamarWebService
            CN_NotificacionActividad()
        End If
        EnEjecucionServicioNotificacionFSCN = False
    End Sub
    ''' <summary>
    ''' Funcion que calcula la proxima ejecucion del servicio.
    ''' </summary>
    ''' <returns>Fecha y hora de la proxima ejecucion del servicio</returns>
    ''' <remarks>Llamada desde=propia pagina; Tiempo máximo=0,4seg.</remarks>
    Private Function FechaProximaEjecucionServicio_CN_NotificacionActividad() As Date
        Dim fechaFichero As Date
        Dim intervaloNotificacion As String
        'If no existe fichero de notificaciones CN
        If Not System.IO.File.Exists(File_FSCN) Then
            FechaProximaEjecucionServicio_CN_NotificacionActividad = CDate(diaArranqueNotif_FSCN & " " & HoraEjecucionNotif_FSCN)
        Else
            'fecha anterior ejecucion con la hora de ejecucion del servicio
            fechaFichero = System.IO.File.GetLastWriteTime(File_FSCN)
            FechaProximaEjecucionServicio_CN_NotificacionActividad = fechaFichero

            Select Case modoEjecucion_Notificaciones_CN
                Case "M" ' Minutos
                    intervaloNotificacion = "n"
                Case "H" 'Horas
                    intervaloNotificacion = "h"
                Case Else
                    intervaloNotificacion = "n"
            End Select
            'suma el periodo en horas a la fecha de la ultima ejecucion
            FechaProximaEjecucionServicio_CN_NotificacionActividad = DateAdd(intervaloNotificacion, CType(PeriodoNotificaciones_CN, Double), FechaProximaEjecucionServicio_CN_NotificacionActividad)
            FechaProximaEjecucionServicio_CN_NotificacionActividad = New Date(Year(FechaProximaEjecucionServicio_CN_NotificacionActividad), Month(FechaProximaEjecucionServicio_CN_NotificacionActividad), Day(FechaProximaEjecucionServicio_CN_NotificacionActividad), Hour(FechaProximaEjecucionServicio_CN_NotificacionActividad), Minute(FechaProximaEjecucionServicio_CN_NotificacionActividad), 0)
        End If
    End Function
    ''' <summary>
    ''' Borra el fichero log para posteriormente volver a escribirlo
    ''' Escribe mensajes en el event viewer para hacer seguimiento de instalacion
    ''' </summary>
    ''' <remarks>Llamada desde=EjecucionServicioCalculoPuntuaciones; Tiempo máximo=0seg.</remarks>
    Private Sub BorrarFicheroServicio_CN_NotificacionActividad()
        System.IO.File.Delete(File_FSCN)
    End Sub
    ''' <summary>
    ''' Escribe un fichero log para indicarnos cuando se ha ejecutado el webservice
    ''' Escribe mensajes en el event viewer para hacer seguimiento de instalacion
    ''' </summary>
    ''' <param name="fechaActual">Fecha actual del sistema</param>
    ''' <param name="FechaEjecucion">Fecha ejecucion servicio</param>
    ''' <param name="fechaFichero">Fecha creacion del fichero</param>
    ''' <remarks>Llamada desde=EjecucionServicioCalculoPuntuaciones; Tiempo máximo=0seg.</remarks>
    Private Sub EscribirFicheroServicio_CN_NotificacionActividad(ByVal fechaActual As Date, ByVal FechaEjecucion As Date, ByVal fechaFichero As Date)
        Dim str = "Fichero de notificaciones de collaboration network. "
        Dim oFileW As System.IO.StreamWriter
        oFileW = New System.IO.StreamWriter(File_FSCN, True)
        oFileW.WriteLine(str)
        oFileW.WriteLine("Fecha Actual = " & fechaActual)
        oFileW.WriteLine("Fecha Ejecucion = " & FechaEjecucion)
        oFileW.WriteLine("Fecha Fichero = " & fechaFichero)
        oFileW.Close()
    End Sub
    ''' <summary>
    ''' Ejecutar el webService con el usuario administrador
    ''' Escribe mensajes en el event viewer para hacer seguimiento de instalacion
    ''' </summary>
    ''' <remarks>Llamada desde=EjecucionServicioCalculoPuntuaciones; Tiempo máximo=0,2seg.</remarks>
    Private Sub CN_NotificacionActividad()
        Dim oMensajes As FSNServer.cnMensajes = m_FSNServer.Get_Object(GetType(FSNServer.cnMensajes))
        oMensajes.Notificar_Actividad_FSCN()
    End Sub
#End Region
#Region "Variables Servicio Histórico Collaboration Network"
    Private EnEjecucionServicioHistoricoCN As Boolean = False

    Private m_sFile_Historico_CN As String
    Private m_diasEjecucion_Historico_CN As Double

    Private m_modoEjecucion_Historico_CN As String '(D, S, M)
    Private m_PeriodoDiario_Historico_CN As String ' (1..31)
    'Semanal
    Private m_PeriodoSemanal_Historico_CN As String '(1..70)
    Private m_DiaPeriodoSemanal_Historico_CN As String '(0...7) vacio, lunes, martes...domingo
    'Mensual
    Private m_PeriodoMensual_Historico_CN As String '(1..12)
    Private m_ModoMensual_Historico_CN As String '(1 o 2)
    Private m_DiaMes_Historico_CN As String '(1..31)
    Private m_PosicionDiaMensual_Historico_CN As String '(1,2,3,4) (1er, 2º, 3º, 4º)
    Private m_DiaPeriodoMensual_Historico_CN As String '(1...7) (lunes...domingo)
    'Configuracion Fecha comienzo y hora ejecucion servicio
    Private m_diaArranque_Historico_CN As Date
    Private m_horaEjecucion_Historico_CN As Date
    Private m_fechaProximaEjecucion_Historico_CN As Date
    'Si no se introduce modo de ejecución, hora de ejecución o día de arranque, que no se ejecute 
    Private ActivoServicioHistoricoCN As Boolean = True
    'Meses de caducidad y límite de registros
    Private m_MesesCaducidad_Historico_CN As Integer
    Private m_LimiteRegistros_Historico_CN As Integer
#End Region
#Region "Servicio Historico CN"
    ''' <summary>
    ''' Se indica el path del fichero log HistoricoCN, los dias de repeticion del servicio. La 1ª 
    ''' vez se lanza el calculo y los dias sucesivos se comprobara si le toca o no ejecutar el webservice 
    ''' a traves del parametro DiasEjecucion
    ''' </summary>
    ''' <remarks>Llamada desde=Service1.OnStart; Tiempo máximo=0seg.</remarks>
    Private Sub LeerParametrosEjecucionServicio_CN_Historico()
        Dim sPath As String

        sPath = ConfigurationManager.AppSettings("Path")
        m_sFile_Historico_CN = sPath & "\HistoricoCN.txt"

        If ConfigurationManager.AppSettings("MODO_EJECUCION_HIST_CN") = "" _
        OrElse (Not IsDate(ConfigurationManager.AppSettings("HORA_ARRANQUE_HIST_CN"))) _
        OrElse (Not IsDate(ConfigurationManager.AppSettings("DIA_ARRANQUE_HIST_CN"))) Then
            ActivoServicioHistoricoCN = False
        Else
            ''Configuracion del modo de ejecucion del servicio.
            ''Toma los parametros de la configuracion.
            m_modoEjecucion_Historico_CN = ConfigurationManager.AppSettings("MODO_EJECUCION_HIST_CN")
            Select Case m_modoEjecucion_Historico_CN
                Case "D"
                    m_PeriodoDiario_Historico_CN = ConfigurationManager.AppSettings("MODO_D_DIA_HIST_CN")
                Case "S"
                    m_PeriodoSemanal_Historico_CN = ConfigurationManager.AppSettings("MODO_S_PERIODO_SEMANAL_HIST_CN")
                    m_DiaPeriodoSemanal_Historico_CN = ConfigurationManager.AppSettings("MODO_S_DIA_PERIODO_SEMANAL_HIST_CN")
                Case "M"
                    m_PeriodoMensual_Historico_CN = ConfigurationManager.AppSettings("MODO_M_PERIODO_MENSUAL_HIST_CN")
                    m_ModoMensual_Historico_CN = ConfigurationManager.AppSettings("MODO_M_MODO_MENSUAL_HIST_CN")
                    If m_ModoMensual_Historico_CN = "1" Then
                        m_DiaMes_Historico_CN = ConfigurationManager.AppSettings("MODO_M_DIA_MES_HIST_CN")
                    ElseIf m_ModoMensual_Historico_CN = "2" Then
                        m_PosicionDiaMensual_Historico_CN = ConfigurationManager.AppSettings("MODO_M_POSICION_DIA_PERIODO_MENSUAL_HIST_CN")
                        m_DiaPeriodoMensual_Historico_CN = ConfigurationManager.AppSettings("MODO_M_DIA_PERIODO_MENSUAL_HIST_CN")
                    End If
            End Select

            m_horaEjecucion_Historico_CN = ConfigurationManager.AppSettings("HORA_ARRANQUE_HIST_CN")
            m_diaArranque_Historico_CN = ConfigurationManager.AppSettings("DIA_ARRANQUE_HIST_CN")

            m_MesesCaducidad_Historico_CN = ConfigurationManager.AppSettings("MESES_CADUCIDAD_HIST_CN")
            m_LimiteRegistros_Historico_CN = ConfigurationManager.AppSettings("LIMITE_REGISTROS_HIST_CN")
        End If
    End Sub
    ''' <summary>
    ''' Borra el fichero log para posteriormente volver a escribirlo
    ''' Escribe mensajes en el event viewer para hacer seguimiento de instalacion
    ''' </summary>
    ''' <remarks>Llamada desde=EjecucionServicioHistoricoCN; Tiempo máximo=0seg.</remarks>
    Private Sub BorrarFicheroServicio_CN_Historico()
        System.IO.File.Delete(m_sFile_Historico_CN)
    End Sub
    ''' <summary>
    ''' Escribe un fichero log para indicarnos cuando se ha ejecutado el webservice
    ''' Escribe mensajes en el event viewer para hacer seguimiento de instalacion
    ''' </summary>
    ''' <param name="fechaActual">Fecha actual del sistema</param>
    ''' <param name="FechaEjecucion">Fecha ejecucion servicio</param>
    ''' <param name="fechaFichero">Fecha creacion del fichero</param>
    ''' <remarks>Llamada desde=EjecucionServicioHistoricoCN; Tiempo máximo=0seg.</remarks>
    Private Sub EscribirFicheroServicio_CN_Historico(ByVal fechaActual As Date, ByVal FechaEjecucion As Date, ByVal fechaFichero As Date)
        Dim str = "Fichero de Historico CN. "
        Dim oFileW As System.IO.StreamWriter
        oFileW = New System.IO.StreamWriter(m_sFile_Historico_CN, True)
        oFileW.WriteLine(str)
        oFileW.WriteLine("Fecha Actual = " & fechaActual)
        oFileW.WriteLine("Fecha Ejecucion = " & FechaEjecucion)
        oFileW.WriteLine("Fecha Fichero = " & fechaFichero)
        oFileW.Close()
    End Sub
    ''' <summary>
    ''' Proceso que comprueba si tiene que lanzar el webService o no. Se comprueba diariamente si en el fichero que se ha escrito le toca ejecutarse o no al webservice
    ''' Cuando ejecuta el webService creo un fichero log que nos indica cuando se ha ejecutado por ultima vez el webService.
    ''' </summary>
    ''' <remarks>Llamada desde=ArrancarTemporizador       temporizador_Elapsed; Tiempo máximo=0,2seg.</remarks>
    Private Sub EjecucionServicio_CN_Historico()
        EnEjecucionServicioHistoricoCN = True
        Dim dFecha As Date
        Dim dFechaFichero As Date = System.IO.File.GetLastWriteTime(m_sFile_Historico_CN)
        dFecha = FechaPróximaEjecucionServicio_CN_Historico()
        If DateDiff("n", Now(), dFecha) <= 0 Then
            'Decidimos borrar y escribir el fichero antes de la ejecución del servicio propiamente dicho para que si el servicio tarda más de lo debido
            'el archivo tenga fecha de modificación actual y asi no lance por segunda vez el servicio, evitando mails duplicados.
            'Borrar Fichero
            BorrarFicheroServicio_CN_Historico()
            'Escribir Fichero log de Historico CN, para indicarle cuando se realizo el calculo por ultima vez
            EscribirFicheroServicio_CN_Historico(Now(), dFecha, dFechaFichero)
            'LlamarWebService
            CN_Historico()
        End If
        EnEjecucionServicioHistoricoCN = False
    End Sub
    ''' <summary>
    ''' Ejecutar el webService con el usuario administrador
    ''' Escribe mensajes en el event viewer para hacer seguimiento de instalacion
    ''' </summary>
    ''' <remarks>Llamada desde=EjecucionServicioHistoricoCN; Tiempo máximo=0,2seg.</remarks>
    Private Sub CN_Historico()
        Dim oHistorico As FSNServer.cnHistorico = m_FSNServer.Get_Object(GetType(FSNServer.cnHistorico))
        oHistorico.BackupCN_InicioTarea_FSCN(m_MesesCaducidad_Historico_CN, m_LimiteRegistros_Historico_CN)
    End Sub
    ''' <summary>
    ''' Funcion que Calcula la proxima fecha de ejecucion del servicio Historico CN.
    ''' </summary>
    ''' <returns>Fecha proxima ejecucion del servicio</returns>
    ''' <remarks>Llamada desde=EjecucionServicioHistoricoCN; Tiempo máximo=0,1seg.</remarks>
    Private Function FechaPróximaEjecucionServicio_CN_Historico() As Date
        Dim diaSemanaHoy As Integer
        Dim dia As Integer
        Dim fechaFichero As Date
        'If no existe fichero de Historico CN
        If Not System.IO.File.Exists(m_sFile_Historico_CN) Then
            Select Case m_modoEjecucion_Historico_CN
                Case "D"
                    FechaPróximaEjecucionServicio_CN_Historico = CDate(m_diaArranque_Historico_CN & " " & m_horaEjecucion_Historico_CN)
                Case "S"
                    If m_DiaPeriodoSemanal_Historico_CN = 0 Then
                        FechaPróximaEjecucionServicio_CN_Historico = CDate(m_diaArranque_Historico_CN & " " & m_horaEjecucion_Historico_CN)
                    Else
                        diaSemanaHoy = DatePart("w", Now, FirstDayOfWeek.Monday)
                        If diaSemanaHoy > m_DiaPeriodoSemanal_Historico_CN Then
                            FechaPróximaEjecucionServicio_CN_Historico = DateAdd("d", 7 - diaSemanaHoy + m_DiaPeriodoSemanal_Historico_CN + 1, Now)
                        Else
                            FechaPróximaEjecucionServicio_CN_Historico = DateAdd("d", m_DiaPeriodoSemanal_Historico_CN - diaSemanaHoy, Now)
                        End If
                    End If
                    FechaPróximaEjecucionServicio_CN_Historico = CDate(Day(FechaPróximaEjecucionServicio_CN_Historico) & "/" & Month(FechaPróximaEjecucionServicio_CN_Historico) & "/" & Year(FechaPróximaEjecucionServicio_CN_Historico) & " " & m_horaEjecucion_Historico_CN)
                Case "M"
                    If m_ModoMensual_Historico_CN = "1" Then
                        dia = DatePart("d", m_diaArranque_Historico_CN, FirstDayOfWeek.Monday)
                        'Calcular fecha de próxima ejecución con el mes actual y el dia definido en DIA_MES, si el dia del mes es                                                           
                        'mayor que los días del mes actual el ultimo día del mes actual.
                        'Si el dia de hoy es superior al día de ejecución la ejecución será el mes siguiente.
                        If dia < m_DiaMes_Historico_CN Then
                            FechaPróximaEjecucionServicio_CN_Historico = Day(m_DiaMes_Historico_CN) & "/" & Month(m_diaArranque_Historico_CN) & "/" & Year(m_diaArranque_Historico_CN)
                        Else
                            FechaPróximaEjecucionServicio_CN_Historico = m_diaArranque_Historico_CN
                        End If
                        FechaPróximaEjecucionServicio_CN_Historico = CDate(FechaPróximaEjecucionServicio_CN_Historico & " " & m_horaEjecucion_Historico_CN)
                    Else '(opcion 2)
                        Dim primerDiaMes, UltimoDiaMes As Date
                        Dim K, diffDias As Integer
                        Dim diaProceso As Date
                        Dim diaSemanaProceso As Integer
                        Dim repeticionesSemanaMensual As Byte
                        Dim bEncontrado As Boolean
                        primerDiaMes = Now
                        primerDiaMes = "01" & "/" & Month(primerDiaMes) & "/" & Year(primerDiaMes)
                        UltimoDiaMes = DateAdd("m", 1, primerDiaMes)
                        UltimoDiaMes = DateAdd("d", -1, UltimoDiaMes)
                        diffDias = DateDiff(DateInterval.Day, primerDiaMes, UltimoDiaMes)
                        bEncontrado = False
                        For K = 0 To diffDias
                            diaProceso = DateAdd("d", K, primerDiaMes)
                            diaSemanaProceso = DatePart("w", diaProceso, Microsoft.VisualBasic.FirstDayOfWeek.Monday)
                            If diaSemanaProceso = m_DiaPeriodoMensual_Historico_CN Then
                                repeticionesSemanaMensual = repeticionesSemanaMensual + 1
                                If repeticionesSemanaMensual = m_PosicionDiaMensual_Historico_CN Then
                                    bEncontrado = True
                                    Exit For
                                End If
                            End If
                        Next

                        If bEncontrado Then
                            If diaProceso < m_diaArranque_Historico_CN Then
                                FechaPróximaEjecucionServicio_CN_Historico = m_diaArranque_Historico_CN
                            Else
                                FechaPróximaEjecucionServicio_CN_Historico = diaProceso
                            End If
                            FechaPróximaEjecucionServicio_CN_Historico = CDate(FechaPróximaEjecucionServicio_CN_Historico & " " & m_horaEjecucion_Historico_CN)
                        Else
                            FechaPróximaEjecucionServicio_CN_Historico = CDate(m_diaArranque_Historico_CN & " " & m_horaEjecucion_Historico_CN)
                        End If
                    End If
            End Select
        Else
            'fecha anterior ejecucion con la hora de ejecucion del servicio
            fechaFichero = System.IO.File.GetLastWriteTime(m_sFile_Historico_CN)
            FechaPróximaEjecucionServicio_CN_Historico = fechaFichero
            Select Case m_modoEjecucion_Historico_CN
                Case "D"
                    'suma el periodo diario en dias a la fecha de la ultima ejecucion
                    FechaPróximaEjecucionServicio_CN_Historico = DateAdd("d", CDbl(m_PeriodoDiario_Historico_CN), FechaPróximaEjecucionServicio_CN_Historico)
                Case "S"
                    FechaPróximaEjecucionServicio_CN_Historico = DateAdd(DateInterval.WeekOfYear, CDbl(m_PeriodoSemanal_Historico_CN), FechaPróximaEjecucionServicio_CN_Historico)
                Case "M"
                    FechaPróximaEjecucionServicio_CN_Historico = DateAdd("m", CDbl(m_PeriodoMensual_Historico_CN), FechaPróximaEjecucionServicio_CN_Historico)
                    If m_ModoMensual_CP = 1 Then
                        FechaPróximaEjecucionServicio_CN_Historico = CDate(m_DiaMes_Historico_CN & "/" & Month(FechaPróximaEjecucionServicio_CN_Historico) & "/" & Year(FechaPróximaEjecucionServicio_CN_Historico))
                    ElseIf m_ModoMensual_Historico_CN = 2 Then
                        Dim primerDiaMes, UltimoDiaMes As Date
                        Dim K, diffDias As Integer
                        Dim diaProceso As Date
                        Dim diaSemanaProceso As Integer
                        Dim repeticionesSemanaMensual As Byte
                        Dim bEncontrado As Boolean
                        primerDiaMes = FechaPróximaEjecucionServicio_CN_Historico
                        primerDiaMes = "01" & "/" & Month(primerDiaMes) & "/" & Year(primerDiaMes)
                        UltimoDiaMes = DateAdd("m", 1, primerDiaMes)
                        UltimoDiaMes = DateAdd("d", -1, UltimoDiaMes)
                        diffDias = DateDiff(DateInterval.Day, primerDiaMes, UltimoDiaMes)
                        bEncontrado = False
                        For K = 0 To diffDias
                            diaProceso = DateAdd("d", K, primerDiaMes)
                            diaSemanaProceso = DatePart("w", diaProceso, FirstDayOfWeek.Monday)
                            If diaSemanaProceso = m_DiaPeriodoMensual_CP Then
                                repeticionesSemanaMensual = repeticionesSemanaMensual + 1
                                If repeticionesSemanaMensual = m_PosicionDiaMensual_Historico_CN Then
                                    bEncontrado = True
                                    Exit For
                                End If
                            End If
                        Next

                        If bEncontrado Then
                            FechaPróximaEjecucionServicio_CN_Historico = diaProceso
                            FechaPróximaEjecucionServicio_CN_Historico = CDate(FechaPróximaEjecucionServicio_CN_Historico)
                        Else
                            FechaPróximaEjecucionServicio_CN_Historico = CDate(m_diaArranque_Historico_CN)
                        End If
                    End If
            End Select
            FechaPróximaEjecucionServicio_CN_Historico = New Date(Year(FechaPróximaEjecucionServicio_CN_Historico), Month(FechaPróximaEjecucionServicio_CN_Historico), Day(FechaPróximaEjecucionServicio_CN_Historico), Hour(m_horaEjecucion_Historico_CN), Minute(m_horaEjecucion_Historico_CN), 0)
        End If
    End Function
#End Region
#End Region
#Region "Funciones comunes"
    ''' <summary>Guarda el error producido en BD</summary>
    ''' <param name="errorGenerado">Objeto error</param>
    ''' <param name="origenError">Origen del error</param>
    ''' <remarks></remarks>
    Private Sub GuardarErrorEnBaseDatos(ByVal errorGenerado As Exception, ByVal origenError As String)
        Dim oError As FSNServer.Errores = m_FSNServer.Get_Object(GetType(FSNServer.Errores))
        oError.Create(origenError, m_sUsu, errorGenerado.GetType().FullName, errorGenerado.Message, errorGenerado.StackTrace, String.Empty, "")
    End Sub

#End Region
End Class
