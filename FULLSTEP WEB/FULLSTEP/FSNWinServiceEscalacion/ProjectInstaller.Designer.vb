﻿<System.ComponentModel.RunInstaller(True)> Partial Class ProjectInstaller
    Inherits System.Configuration.Install.Installer

    'Installer overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Component Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Component Designer
    'It can be modified using the Component Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.FSNWinServiceEscalacionProcessInstaller = New System.ServiceProcess.ServiceProcessInstaller()
        Me.FSNWinServiceEscalacionInstaller = New System.ServiceProcess.ServiceInstaller()
        '
        'FSNWinServiceEscalacionProcessInstaller
        '
        Me.FSNWinServiceEscalacionProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem
        Me.FSNWinServiceEscalacionProcessInstaller.Password = Nothing
        Me.FSNWinServiceEscalacionProcessInstaller.Username = Nothing
        '
        'FSNWinServiceEscalacionInstaller
        '
        Me.FSNWinServiceEscalacionInstaller.Description = "Servicio de escalación de proveedores de FULLSTEP"
        Me.FSNWinServiceEscalacionInstaller.ServiceName = "FSNWinServiceEscalacion"
        Me.FSNWinServiceEscalacionInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic
        '
        'ProjectInstaller
        '
        Me.Installers.AddRange(New System.Configuration.Install.Installer() {Me.FSNWinServiceEscalacionProcessInstaller, Me.FSNWinServiceEscalacionInstaller})

    End Sub

    Friend WithEvents FSNWinServiceEscalacionProcessInstaller As ServiceProcess.ServiceProcessInstaller
    Friend WithEvents FSNWinServiceEscalacionInstaller As ServiceProcess.ServiceInstaller
End Class
