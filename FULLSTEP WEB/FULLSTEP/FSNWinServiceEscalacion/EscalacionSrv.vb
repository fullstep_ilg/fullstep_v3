﻿Imports System.Configuration
Imports System.IO
Imports System.Threading
Imports System.Timers
Imports Fullstep.FSNServer

Public Class EscalacionSrv
    Private WithEvents _oTemporizador As Timers.Timer
    Private _oConfigSrvEsc As ConfigServicioEsc
    Private _oConfigSrvNotifNCEPdtes As ConfigServicioNotifNCEPdtes
    Private _oConfigSrvNotifNCESinCerrar As ConfigServicioNotifNCESinCerrar

#If DEBUG Then
    Public Sub EmpezarDebug()
        Me.OnStart(Nothing)
    End Sub
#End If

    Protected Overrides Sub OnStart(ByVal args() As String)
        ' Add code here to start your service. This method should set things
        ' in motion so your service can do its work.
        _oConfigSrvEsc = New ConfigServicioEsc
        _oConfigSrvNotifNCEPdtes = New ConfigServicioNotifNCEPdtes
        _oConfigSrvNotifNCESinCerrar = New ConfigServicioNotifNCESinCerrar
        ArrancarTemporizador()
    End Sub

    Protected Overrides Sub OnStop()
        ' Add code here to perform any tear-down necessary to stop your service.
    End Sub

#Region "Temporizador"
    ''' <summary>Arrancar el temporizador</summary>
    ''' <remarks>Llamada desde=Service1.OnStart</remarks>
    Private Sub ArrancarTemporizador()
        _oTemporizador = New Timers.Timer
        _oTemporizador.AutoReset = True
        _oTemporizador.Interval = (1000 * 60)
        _oTemporizador.Enabled = True

        If _oConfigSrvEsc.ActivoServicio Then ThreadPool.QueueUserWorkItem(New WaitCallback(AddressOf _oConfigSrvEsc.EjecucionServicio))
        If _oConfigSrvNotifNCEPdtes.ActivoServicio Then ThreadPool.QueueUserWorkItem(New WaitCallback(AddressOf _oConfigSrvNotifNCEPdtes.EjecucionServicio))
        If _oConfigSrvNotifNCESinCerrar.ActivoServicio Then ThreadPool.QueueUserWorkItem(New WaitCallback(AddressOf _oConfigSrvNotifNCESinCerrar.EjecucionServicio))

        _oTemporizador.Start()
    End Sub
    Private Sub _oTemporizador_Elapsed(sender As Object, e As ElapsedEventArgs) Handles _oTemporizador.Elapsed
        If _oConfigSrvEsc.ActivoServicio Then ThreadPool.QueueUserWorkItem(New WaitCallback(AddressOf _oConfigSrvEsc.EjecucionServicio))
        If _oConfigSrvNotifNCEPdtes.ActivoServicio Then ThreadPool.QueueUserWorkItem(New WaitCallback(AddressOf _oConfigSrvNotifNCEPdtes.EjecucionServicio))
        If _oConfigSrvNotifNCESinCerrar.ActivoServicio Then ThreadPool.QueueUserWorkItem(New WaitCallback(AddressOf _oConfigSrvNotifNCESinCerrar.EjecucionServicio))
    End Sub
#End Region
End Class

