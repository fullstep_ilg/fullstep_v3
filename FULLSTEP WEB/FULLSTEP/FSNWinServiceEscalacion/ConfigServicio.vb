﻿Imports System.Configuration
Imports System.IO
Imports Fullstep.FSNServer

Public Class ConfigServicioEsc
    Inherits ConfigServicioBase

    Public Sub New()
        MyBase.New("_ESC", "Escalacion.txt")
    End Sub

    Public Overloads Sub EscribirFicheroServicio()
        MyBase.EscribirFicheroServicio("Fichero de escalación. ")
    End Sub
    ''' <summary>Realiza la escalción de proveedores</summary>
    Public Overrides Sub EjecucionServicio()
        Try
            If DateDiff("n", Now(), Me.FechaProximaEjecucionServicio) <= 0 Then
                Me.EscribirFicheroServicio()

                'Obtener condiciones
                Dim oCondiciones As CondicionesEscalacion = LeerFicheroCondicionesEscalacion()

                If oCondiciones.CondicionesValidas Then
                    'Ejecutar proceso escalación
                    Dim oEscalacion As Escalaciones = FSNServer.Get_Object(GetType(Escalaciones))
                    oEscalacion.EscalarProveedores(oCondiciones)
                Else
                    Dim oErrores As Errores = FSNServer.Get_Object(GetType(Errores))
                    oErrores.Create("FSNWinServiceEscalacion.ConfigServicio.EjecucionServicio", String.Empty, String.Empty, "Las variables de calidad de las condiciones del servicio de escalación no son válidas. El servicio no se ha ejecutado.", String.Empty, String.Empty, String.Empty)
                End If
            End If

        Catch ex As Exception
            Dim oErrores As Errores = FSNServer.Get_Object(GetType(Errores))
            oErrores.Create("FSNWinServiceEscalacion.ConfigServicio.EjecucionServicio", String.Empty, ex.GetType().FullName, ex.Message, ex.StackTrace, String.Empty, String.Empty)
        End Try
    End Sub
End Class

Public Class ConfigServicioNotifNCEPdtes
    Inherits ConfigServicioBase

    Public Sub New()
        MyBase.New("_NOTIFNC", "NotifNCEPdtes.txt")
    End Sub

    Public Overloads Sub EscribirFicheroServicio()
        MyBase.EscribirFicheroServicio("Fichero de notificación de no conformidades de escalación pendientes de validar cierre. ")
    End Sub
    ''' <summary>Realiza las notificaciones de las NCEs pdtes. de validar cierre eficaz</summary>
    Public Overrides Sub EjecucionServicio()
        Try
            If DateDiff("n", Now(), Me.FechaProximaEjecucionServicio) <= 0 Then
                Me.EscribirFicheroServicio()

                'Obtener condiciones
                Dim oCondiciones As CondicionesEscalacion = LeerFicheroCondicionesEscalacion()

                Dim oEscalacion As Escalaciones = FSNServer.Get_Object(GetType(Escalaciones))
                oEscalacion.NotificarNCEscPdtesValidarCierre(oCondiciones)
            End If

        Catch ex As Exception
            Dim oErrores As Errores = FSNServer.Get_Object(GetType(Errores))
            oErrores.Create("FSNWinServiceEscalacion.EscalacionSrv.EjecucionServicioNotifNCEscPdtes", String.Empty, ex.GetType().FullName, ex.Message, ex.StackTrace, String.Empty, String.Empty)
        End Try
    End Sub
End Class

Public Class ConfigServicioNotifNCESinCerrar
    Inherits ConfigServicioBase

    Public Sub New()
        MyBase.New("_NOTIFNCX", "NotifNCESinCerrar.txt")
    End Sub

    Public Overloads Sub EscribirFicheroServicio()
        MyBase.EscribirFicheroServicio("Fichero de notificación de no conformidades de escalación sin cerrar X meses. ")
    End Sub
    ''' <summary>Lleva a cabo las notificaciones de las NCEs que llevan X meses sin cerrar</summary>
    Public Overrides Sub EjecucionServicio()
        Try
            If DateDiff("n", Now(), Me.FechaProximaEjecucionServicio) <= 0 Then
                Me.EscribirFicheroServicio()

                'Obtener condiciones
                Dim oCondiciones As CondicionesEscalacion = LeerFicheroCondicionesEscalacion()

                Dim oEscalacion As Escalaciones = FSNServer.Get_Object(GetType(Escalaciones))
                oEscalacion.NotificarNCEscSinCerrarXMeses(oCondiciones, Me.FechaProximaEjecucionServicio)
            End If

        Catch ex As Exception
            Dim oErrores As Errores = FSNServer.Get_Object(GetType(Errores))
            oErrores.Create("FSNWinServiceEscalacion.EscalacionSrv.EjecucionServicioNotifNCESinCerrar", String.Empty, ex.GetType().FullName, ex.Message, ex.StackTrace, String.Empty, String.Empty)
        End Try
    End Sub

End Class

