﻿Imports System.Configuration
Imports System.IO
Imports Fullstep.FSNServer

Public MustInherit Class ConfigServicioBase
    Public Property ActivoServicio As Boolean

    Private _sFile As String
    Private _dDiasEjecucion As Double
    Private _sModoEjecucion As String '(D, S, M)
    Private _sPeriodoDiario As String ' (1..31)
    'Semanal
    Private _sPeriodoSemanal As String '(1..70)
    Private _sDiaPeriodoSemanal As String '(0...7) vacio, lunes, martes...domingo
    'Mensual
    Private _sPeriodoMensual As String '(1..12)
    Private _sModoMensual As String '(1 o 2)
    Private _sDiaMes As String '(1..31)
    Private _sPosicionDiaMensual As String '(1,2,3,4) (1er, 2º, 3º, 4º)
    Private _sDiaPeriodoMensual As String '(1...7) (lunes...domingo)
    'Configuracion Fecha comienzo y hora ejecucion servicio
    Private _dDiaArranque As Date
    Private _dHoraEjecucion As Date
    Private _dFechaProximaEjecucionServicio As Date

    Private _FSNServer As Fullstep.FSNServer.Root

#Region "Propiedades"
    Public Property FSNServer() As Root
        Get
            If _FSNServer Is Nothing Then _FSNServer = New Root(True)
            Return _FSNServer
        End Get
        Set(ByVal value As Root)
            _FSNServer = value
        End Set
    End Property
#End Region

    Public Sub New(ByVal sSufijo As String, ByVal sNombreFichero As String)
        Dim sPath As String = ConfigurationManager.AppSettings("Path")

        _sFile = Path.Combine(sPath, sNombreFichero)

        If ConfigurationManager.AppSettings("MODO_EJECUCION" & sSufijo) = "" OrElse (Not IsDate(ConfigurationManager.AppSettings("HORA_EJECUCION" & sSufijo))) OrElse (Not IsDate(ConfigurationManager.AppSettings("DIA_ARRANQUE_SERVICIO" & sSufijo))) Then
            ActivoServicio = False
        Else
            ActivoServicio = True

            _dDiasEjecucion = ConfigurationManager.AppSettings("DiasEjecucion" & sSufijo)

            ''Configuracion del modo de ejecucion del servicio.
            ''Toma los parametros de la configuracion.
            _sModoEjecucion = ConfigurationManager.AppSettings("MODO_EJECUCION" & sSufijo)
            Select Case _sModoEjecucion
                Case "D"
                    _sPeriodoDiario = ConfigurationManager.AppSettings("PERIODO_DIARIO" & sSufijo)
                Case "S"
                    _sPeriodoSemanal = ConfigurationManager.AppSettings("PERIODO_SEMANAL" & sSufijo)
                    _sDiaPeriodoSemanal = ConfigurationManager.AppSettings("DIA_PERIODO_SEMANAL" & sSufijo)
                Case "M"
                    _sPeriodoMensual = ConfigurationManager.AppSettings("PERIODO_MENSUAL" & sSufijo)
                    _sModoMensual = ConfigurationManager.AppSettings("MODO_MENSUAL" & sSufijo)
                    If _sModoMensual = "1" Then
                        _sDiaMes = ConfigurationManager.AppSettings("DIA_MES" & sSufijo)
                    ElseIf _sModoMensual = "2" Then
                        _sPosicionDiaMensual = ConfigurationManager.AppSettings("POSICION_DIA_PERIODO_MENSUAL" & sSufijo)
                        _sDiaPeriodoMensual = ConfigurationManager.AppSettings("DIA_PERIODO_MENSUAL" & sSufijo)
                    End If
            End Select

            _dHoraEjecucion = ConfigurationManager.AppSettings("HORA_EJECUCION" & sSufijo)
            _dDiaArranque = ConfigurationManager.AppSettings("DIA_ARRANQUE_SERVICIO" & sSufijo)
        End If
    End Sub

    ''' <summary>Calcula la fecha para la próxima ejecución del servicio</summary>    
    ''' <returns>Fecha de próxima ejecución del servicio</returns>
    ''' <remarks>Llamada desde: Escalacion.ArrancarTemporizador, Escalacion._oTemporizador_Elapsed</remarks>
    Public ReadOnly Property FechaProximaEjecucionServicio() As Date
        Get
            Dim iDiaSemanaHoy As Integer
            Dim iDia As Integer
            Dim dFechaFichero As Date

            'If no existe fichero de calculo de puntuaciones 
            If Not System.IO.File.Exists(_sFile) Then
                Select Case _sModoEjecucion
                    Case "D"
                        _dFechaProximaEjecucionServicio = CDate(_dDiaArranque & " " & _dHoraEjecucion)
                    Case "S"
                        If _sDiaPeriodoSemanal = 0 Then
                            _dFechaProximaEjecucionServicio = CDate(_dDiaArranque & " " & _dHoraEjecucion)

                        Else
                            iDiaSemanaHoy = DatePart("w", Now, FirstDayOfWeek.Monday)
                            If iDiaSemanaHoy > _sDiaPeriodoSemanal Then
                                _dFechaProximaEjecucionServicio = DateAdd("d", 7 - iDiaSemanaHoy + _sDiaPeriodoSemanal + 1, Now)
                            Else
                                _dFechaProximaEjecucionServicio = DateAdd("d", _sDiaPeriodoSemanal - iDiaSemanaHoy, Now)
                            End If
                        End If
                        _dFechaProximaEjecucionServicio = CDate(Day(_dFechaProximaEjecucionServicio) & "/" & Month(_dFechaProximaEjecucionServicio) & "/" & Year(_dFechaProximaEjecucionServicio) & " " & _dHoraEjecucion)

                    Case "M"
                        If _sModoMensual = "1" Then
                            iDia = DatePart("d", _dDiaArranque, FirstDayOfWeek.Monday)
                            'Calcular fecha de próxima ejecución con el mes actual y el dia definido en DIA_MES, si el dia del mes es                                                           
                            'mayor que los días del mes actual el ultimo día del mes actual.
                            'Si el dia de hoy es superior al día de ejecución la ejecución será el mes siguiente.
                            If iDia < _sDiaMes Then
                                _dFechaProximaEjecucionServicio = Day(_sDiaMes) & "/" & Month(_dDiaArranque) & "/" & Year(_dDiaArranque)
                            Else
                                _dFechaProximaEjecucionServicio = _dDiaArranque
                            End If
                            _dFechaProximaEjecucionServicio = CDate(_dFechaProximaEjecucionServicio & " " & _dHoraEjecucion)
                        Else '(opcion 2)
                            Dim primerDiaMes, UltimoDiaMes As Date
                            Dim K, diffDias As Integer
                            Dim diaProceso As Date
                            Dim diaSemanaProceso As Integer
                            Dim repeticionesSemanaMensual As Byte
                            Dim bEncontrado As Boolean
                            primerDiaMes = Now
                            primerDiaMes = "01" & "/" & Month(primerDiaMes) & "/" & Year(primerDiaMes)
                            UltimoDiaMes = DateAdd("m", 1, primerDiaMes)
                            UltimoDiaMes = DateAdd("d", -1, UltimoDiaMes)
                            diffDias = DateDiff(DateInterval.Day, primerDiaMes, UltimoDiaMes)
                            bEncontrado = False
                            For K = 0 To diffDias
                                diaProceso = DateAdd("d", K, primerDiaMes)
                                diaSemanaProceso = DatePart("w", diaProceso, Microsoft.VisualBasic.FirstDayOfWeek.Monday)
                                If diaSemanaProceso = _sDiaPeriodoMensual Then
                                    repeticionesSemanaMensual = repeticionesSemanaMensual + 1
                                    If repeticionesSemanaMensual = _sPosicionDiaMensual Then
                                        bEncontrado = True
                                        Exit For
                                    End If
                                End If
                            Next

                            If bEncontrado Then
                                If diaProceso < _dDiaArranque Then
                                    _dFechaProximaEjecucionServicio = _dDiaArranque
                                Else
                                    _dFechaProximaEjecucionServicio = diaProceso
                                End If
                                _dFechaProximaEjecucionServicio = CDate(_dFechaProximaEjecucionServicio & " " & _dHoraEjecucion)
                            Else
                                _dFechaProximaEjecucionServicio = CDate(_dDiaArranque & " " & _dHoraEjecucion)
                            End If
                        End If
                End Select
            Else
                'fecha anterior ejecucion con la hora de ejecucion del servicio
                dFechaFichero = System.IO.File.GetLastWriteTime(_sFile)
                _dFechaProximaEjecucionServicio = dFechaFichero
                Select Case _sModoEjecucion
                    Case "D"
                        'suma el periodo diario en dias a la fecha de la ultima ejecucion
                        _dFechaProximaEjecucionServicio = DateAdd("d", CDbl(_sPeriodoDiario), _dFechaProximaEjecucionServicio)
                    Case "S"
                        _dFechaProximaEjecucionServicio = DateAdd(DateInterval.WeekOfYear, CDbl(_sPeriodoSemanal), _dFechaProximaEjecucionServicio)
                    Case "M"
                        _dFechaProximaEjecucionServicio = DateAdd("m", CDbl(_sPeriodoMensual), _dFechaProximaEjecucionServicio)
                        If _sModoMensual = 1 Then
                            _dFechaProximaEjecucionServicio = CDate(_sDiaMes & "/" & Month(_dFechaProximaEjecucionServicio) & "/" & Year(_dFechaProximaEjecucionServicio))

                        ElseIf _sModoMensual = 2 Then
                            Dim primerDiaMes, UltimoDiaMes As Date
                            Dim K, diffDias As Integer
                            Dim diaProceso As Date
                            Dim diaSemanaProceso As Integer
                            Dim repeticionesSemanaMensual As Byte
                            Dim bEncontrado As Boolean
                            primerDiaMes = _dFechaProximaEjecucionServicio
                            primerDiaMes = "01" & "/" & Month(primerDiaMes) & "/" & Year(primerDiaMes)
                            UltimoDiaMes = DateAdd("m", 1, primerDiaMes)
                            UltimoDiaMes = DateAdd("d", -1, UltimoDiaMes)
                            diffDias = DateDiff(DateInterval.Day, primerDiaMes, UltimoDiaMes)
                            bEncontrado = False
                            For K = 0 To diffDias
                                diaProceso = DateAdd("d", K, primerDiaMes)
                                diaSemanaProceso = DatePart("w", diaProceso, FirstDayOfWeek.Monday)
                                If diaSemanaProceso = _sDiaPeriodoMensual Then
                                    repeticionesSemanaMensual = repeticionesSemanaMensual + 1
                                    If repeticionesSemanaMensual = _sPosicionDiaMensual Then
                                        bEncontrado = True
                                        Exit For
                                    End If
                                End If
                            Next

                            If bEncontrado Then
                                _dFechaProximaEjecucionServicio = diaProceso
                                _dFechaProximaEjecucionServicio = CDate(_dFechaProximaEjecucionServicio)
                            Else
                                _dFechaProximaEjecucionServicio = CDate(_dDiaArranque)
                            End If
                        End If
                End Select

                _dFechaProximaEjecucionServicio = New Date(Year(_dFechaProximaEjecucionServicio), Month(_dFechaProximaEjecucionServicio), Day(_dFechaProximaEjecucionServicio), Hour(_dHoraEjecucion), Minute(_dHoraEjecucion), 0)
            End If

            Return _dFechaProximaEjecucionServicio
        End Get
    End Property

    ''' <summary>
    ''' Escribe un fichero log para indicarnos cuando se ha ejecutado el webservice
    ''' Escribe mensajes en el event viewer para hacer seguimiento de instalacion
    ''' </summary>
    ''' <remarks>Llamada desde: Escalacion._oTemporizador_Elapsed</remarks>
    Protected Sub EscribirFicheroServicio(ByVal str As String)
        System.IO.File.Delete(_sFile)

        Dim oFileW As System.IO.StreamWriter
        oFileW = New System.IO.StreamWriter(_sFile, True)
        oFileW.WriteLine(str)
        oFileW.WriteLine("Fecha Actual = " & Date.Now)
        oFileW.WriteLine("Fecha Ejecucion = " & _dFechaProximaEjecucionServicio)
        oFileW.WriteLine("Fecha Fichero = " & System.IO.File.GetLastWriteTime(_sFile))
        oFileW.Close()
    End Sub

    Public MustOverride Sub EjecucionServicio()

    ''' <summary>Función que lee el fichero de condiciones de escalación</summary>
    ''' <returns>Objeto con las condiciones de escalación</returns>
    Protected Function LeerFicheroCondicionesEscalacion() As CondicionesEscalacion
        'Leer fichero de condiciones de escalación
        Dim dsCondiciones As New DataSet
        Dim oXMLDoc As New System.Xml.XmlDocument
        oXMLDoc.Load(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "XMLCondicionesEscalacion.xml"))
        dsCondiciones.ReadXml(New Xml.XmlNodeReader(oXMLDoc))

        'Obtener condiciones
        Dim oCondiciones As CondicionesEscalacion = FSNServer.Get_Object(GetType(CondicionesEscalacion))
        oCondiciones.CargarCondiciones(dsCondiciones)

        Return oCondiciones
    End Function
End Class
