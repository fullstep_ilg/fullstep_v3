﻿'Vamos a definir una serie de propiedades para disponer de un textbox numérico
Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Drawing.Design
Imports System.Text
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.Design
Imports System.Web.UI.WebControls

Public Class FSNTextBox
    Inherits TextBox
    Implements IAttributeAccessor

    Private m_allowDecimal As Boolean = False
    Private m_dblValue As Double
    Private m_dblValueAnterior As Double

    ''' <summary>
    ''' Devuelve en contenido del Me.text como integer
    ''' </summary>
    Public ReadOnly Property IntValue() As Integer
        Get
            Return Int32.Parse(Me.Text)
        End Get
    End Property

    ''' <summary>
    ''' Devuelve la parte decimal de la propiedad DblValue
    ''' </summary>
    Public ReadOnly Property DecimalValue() As Decimal
        Get
            Return [Decimal].Parse(DblValue)
        End Get
    End Property

    ''' <summary>
    ''' Valor double del control.
    ''' </summary>
    Public Property DblValue() As Double
        Get
            Return m_dblValue
        End Get
        Set(ByVal value As Double)
            m_dblValue = value
        End Set
    End Property

    ''' <summary>
    ''' Valor double anterior del control.
    ''' </summary>
    Public Property DblValueAnterior() As Double
        Get
            Return m_dblValueAnterior
        End Get
        Set(ByVal value As Double)
            m_dblValueAnterior = value
        End Set
    End Property

    Public Property AllowDecimal() As Boolean
        Get
            Return m_allowDecimal
        End Get
        Set(ByVal value As Boolean)
            m_allowDecimal = value
        End Set
    End Property

    '''' <summary>
    '''' Valor double del control.
    '''' </summary>
    'Public Property DblValueViewState() As Double
    '    Get
    '        Return strToDbl(CType("DblValue" & Me.ClientID, String))
    '    End Get
    '    Set(ByVal value As Double)
    '        ViewState("DblValue" & Me.ClientID) = value
    '    End Set
    'End Property

    '' Implement the SetAttribute method for the control. When
    '' this method is called from a page, the control's properties
    '' are set to values defined in the page.
    'Public Sub SetAttribute(ByVal name As String, ByVal value1 As String) Implements IAttributeAccessor.SetAttribute
    '    ViewState(name) = value1
    'End Sub 'SetAttribute

    '' Implement the GetAttribute method for the control. When
    '' this method is called from a page, the values for the control's
    '' properties can be displayed in the page.
    'Public Function GetAttribute(ByVal name As String) As String Implements IAttributeAccessor.GetAttribute
    '    Return CType(ViewState(name), String)
    'End Function 'GetAttribute

    '''' <summary>
    '''' Guardamos el ViewState de los elementos del FSNtextbox para no perderlos en los postback
    '''' </summary>
    '''' <returns>Array con el contenido guardado</returns>
    '''' <remarks></remarks>
    'Protected Overrides Function SaveViewState() As Object
    '    Dim arrState As Object() = New Object(5) {}
    '    arrState(0) = MyBase.SaveViewState()
    '    arrState(1) = Me.DblValue
    '    arrState(2) = Me.DblValueAnterior
    '    arrState(3) = Me.AllowDecimal

    '    Return arrState
    'End Function

    '''' <summary>
    '''' Cargamos los datos del FSNtextbox con los datos guardados
    '''' </summary>
    '''' <param name="savedState">Datos guardados del nodo</param>
    '''' <remarks></remarks>
    'Protected Overloads Overrides Sub LoadViewState(ByVal savedState As Object)
    '    If savedState IsNot Nothing Then
    '        Dim arrState As Object() = TryCast(savedState, Object())

    '        Me.m_dblValue = DirectCast(arrState(1), Double)
    '        Me.m_dblValueAnterior = DirectCast(arrState(2), Double)
    '        Me.m_allowDecimal = DirectCast(arrState(3), Boolean)

    '        MyBase.LoadViewState(arrState(0))
    '    End If
    'End Sub

    ''' <summary>
    ''' Inicializa el control.
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub New()

    End Sub

End Class
