﻿Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Drawing.Design
Imports System.Text
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.Design
Imports System.Web.UI.WebControls

Public Class FSNButton
    Inherits LinkButton

    <Browsable(True), Bindable(True), _
        Category("Colores"), Description("Imágenes para los bordes"), _
        Editor(GetType(ImageUrlEditor), GetType(UITypeEditor)), UrlProperty(), _
        Themeable(True)> _
    Public Property ImagenesBordes() As String
        Get
            Dim s As String = CStr(ViewState("ImagenesBordes"))
            If s Is Nothing Then s = String.Empty
            Return s
        End Get
        Set(ByVal value As String)
            ViewState("ImagenesBordes") = value
        End Set
    End Property

    <Browsable(True), Bindable(False), _
        Category("Colores"), Description("Color de Fondo del botón"), _
        TypeConverter(GetType(WebColorConverter)), _
        Themeable(True)> _
    Public Property ColorFondo() As System.Drawing.Color
        Get
            Return MyBase.BackColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            MyBase.BackColor = value
        End Set
    End Property

    <Browsable(True), Bindable(False),
            Category("Colores"), Description("Color de Fondo del botón cuando el cursor se posa encima"),
            TypeConverter(GetType(WebColorConverter)),
            Themeable(True)>
    Public Property ColorFondoHover() As System.Drawing.Color
        Get
            Return CType(ViewState("ColorFondoHover"), System.Drawing.Color)
        End Get
        Set(ByVal value As System.Drawing.Color)
            ViewState("ColorFondoHover") = value
        End Set
    End Property

    <Browsable(True), Bindable(False),
            Category("Colores"), Description("Color del texto del botón cuando el cursor se posa encima"),
            TypeConverter(GetType(WebColorConverter)),
            Themeable(True)>
    Public Property ForeColorHover() As System.Drawing.Color
        Get
            Return CType(ViewState("ForeColorHover"), System.Drawing.Color)
        End Get
        Set(ByVal value As System.Drawing.Color)
            ViewState("ForeColorHover") = value
        End Set
    End Property

    <Browsable(True), Bindable(False), _
        Category("Appearance"), Description("Alineación del botón"), DefaultValue(Fullstep.FSNWebControls.FSNTipos.Alineacion.Right)> _
    Public Property Alineacion() As Fullstep.FSNWebControls.FSNTipos.Alineacion
        Get
            Dim s As Fullstep.FSNWebControls.FSNTipos.Alineacion = ViewState("Alineacion")
            Return s
        End Get
        Set(ByVal value As Fullstep.FSNWebControls.FSNTipos.Alineacion)
            ViewState("Alineacion") = value
        End Set
    End Property

    <Browsable(True), Bindable(True),
        Category("Behavior"), Description("Indica si es el elemento seleccionado"), DefaultValue(False)>
    Public Property Selected() As Boolean
        Get
            Return ViewState("Selected")
        End Get
        Set(ByVal value As Boolean)
            ViewState("Selected") = value
        End Set
    End Property

    <Browsable(True), Bindable(True),
        Category("Behavior"), Description("Indica si el texto se muestra en mayúsculas"), DefaultValue(False)>
    Public Property TextoMayusculas() As Boolean
        Get
            Return ViewState("TextoMayusculas")
        End Get
        Set(ByVal value As Boolean)
            ViewState("TextoMayusculas") = value
        End Set
    End Property

    <Browsable(True), Bindable(True),
        Category("Behavior"), Description("Indica si se ve un borde en el botón"), DefaultValue(False)>
    Public Property BordeVisible() As Boolean
        Get
            Return ViewState("BordeVisible")
        End Get
        Set(ByVal value As Boolean)
            ViewState("BordeVisible") = value
        End Set
    End Property

    <Browsable(True), Bindable(True), _
        Category("Behavior"), Description("Indica si se agregara en la creacion del control el evento onclick de javascript"), DefaultValue(True)> _
    Public Property AgregarOnClick() As Boolean
        Get
            Return ViewState("AgregarOnClick")
        End Get
        Set(ByVal value As Boolean)
            ViewState("AgregarOnClick") = value
        End Set
    End Property

    ''' <summary>
    ''' Agrega los atributos del control LinkButton a la secuencia de salida para representar en el cliente.
    ''' </summary>
    ''' <param name="writer">Objeto System.Web.UI.HtmlTextWriter que contiene la secuencia de salida que se va a representar en el cliente.</param>
    Protected Overrides Sub AddAttributesToRender(ByVal writer As HtmlTextWriter)
        MyBase.AddAttributesToRender(writer)
        Dim sBrowser As String = ""
        Dim Version As Integer
        Dim arBrowsers As New ArrayList
        If Not Me.DesignMode Then
            sBrowser = HttpContext.Current.Request.Browser.Browser
            Version = HttpContext.Current.Request.Browser.MajorVersion
            arBrowsers = HttpContext.Current.Request.Browser.Browsers
        End If
        FSNLibrary.AddAtributoEstilo(writer, "float", IIf(Alineacion = FSNTipos.Alineacion.Left, "left", "right"))
        FSNLibrary.AddAtributoEstilo(writer, HtmlTextWriterStyle.TextDecoration, "none")
        If sBrowser <> "IE" Or Version < 7 Then
            If BordeVisible Then
                FSNLibrary.AddAtributoEstilo(writer, HtmlTextWriterStyle.BorderColor, Drawing.ColorTranslator.ToHtml(Me.ForeColor))
                FSNLibrary.AddAtributoEstilo(writer, HtmlTextWriterStyle.BorderStyle, "solid")
                FSNLibrary.AddAtributoEstilo(writer, HtmlTextWriterStyle.BorderWidth, "1px")
            Else
                FSNLibrary.AddAtributoEstilo(writer, HtmlTextWriterStyle.BorderStyle, "none")
                FSNLibrary.AddAtributoEstilo(writer, HtmlTextWriterStyle.BorderColor, "inherit")
                FSNLibrary.AddAtributoEstilo(writer, HtmlTextWriterStyle.BorderWidth, "0px")
            End If
            FSNLibrary.AddAtributoEstilo(writer, HtmlTextWriterStyle.Padding, "3px 10px 3px 10px")
            If arBrowsers.Contains("mozilla") Or arBrowsers.Contains("gecko") Then _
                FSNLibrary.AddAtributoEstilo(writer, "-moz-border-radius", "4px")
            If arBrowsers.Contains("safari") Then _
                FSNLibrary.AddAtributoEstilo(writer, "-webkit-border-radius", "4px")
            If arBrowsers.Contains("khtml") Then _
                FSNLibrary.AddAtributoEstilo(writer, "-khtml-border-radius", "4px")
            FSNLibrary.AddAtributoEstilo(writer, "border-radius", "4px")
            If Not Me.DesignMode Then
                If Not Selected Then
                    writer.AddAttribute("onmouseover", "this.style.backgroundColor='" & System.Drawing.ColorTranslator.ToHtml(ColorFondoHover) & "';this.style.color='" & System.Drawing.ColorTranslator.ToHtml(ForeColorHover) & "';")
                    writer.AddAttribute("onmouseout", "this.style.backgroundColor='" & System.Drawing.ColorTranslator.ToHtml(ColorFondo) & "';this.style.color='" & System.Drawing.ColorTranslator.ToHtml(ForeColor) & "';" &
                        "this.style.padding='3px 10px 3px 10px';")
                    writer.AddAttribute("onmousedown", "this.style.padding='4px 10px 2px 10px';")
                    writer.AddAttribute("onmouseup", "this.style.padding='3px 10px 3px 10px';")
                Else
                    FSNLibrary.AddAtributoEstilo(writer, HtmlTextWriterStyle.BackgroundColor, Drawing.ColorTranslator.ToHtml(ColorFondoHover))
                    FSNLibrary.AddAtributoEstilo(writer, HtmlTextWriterStyle.Color, Drawing.ColorTranslator.ToHtml(ForeColorHover))
                End If
                If Me.AgregarOnClick Then
                    writer.AddAttribute(HtmlTextWriterAttribute.Onclick, "this.blur();")
                End If
                writer.AddAttribute("onfocus", "this.blur();")

            End If
        Else
            If Not Me.DesignMode Then
                If Not Me.Selected Then
                    writer.AddAttribute("onmouseover", "fsnbutton_over(this.childNodes[0],'" & Drawing.ColorTranslator.ToHtml(Me.ColorFondoHover) & "','" & Drawing.ColorTranslator.ToHtml(Me.ForeColorHover) & "')")
                    writer.AddAttribute("onmouseout", "fsnbutton_out(this.childNodes[0],'" & Drawing.ColorTranslator.ToHtml(Me.ColorFondo) & "','" & Drawing.ColorTranslator.ToHtml(Me.ForeColor) & "')")
                Else
                    writer.AddAttribute("onmouseout", "fsnbutton_up(this.childNodes[0])")
                End If
                writer.AddAttribute("onmousedown", "fsnbutton_down(this.childNodes[0])")
                writer.AddAttribute("onmouseup", "fsnbutton_up(this.childNodes[0])")
                writer.AddAttribute("onmouseenter", "this.setCapture()")
                writer.AddAttribute("onmouseleave", "this.releaseCapture()")
            End If
        End If
    End Sub

    ''' <summary>
    ''' Presenta el contenido del control en el sistema de escritura especificado. Este método lo usan principalmente los programadores de controles. 
    ''' </summary>
    ''' <param name="writer">HtmlTextWriter que representa la secuencia de salida para presentar contenido HTML en el cliente.</param>
    Protected Overrides Sub RenderContents(ByVal writer As HtmlTextWriter)
        Dim sBrowser As String = ""
        Dim Version As Integer
        Dim arBrowsers As New ArrayList
        If Not Me.DesignMode Then
            sBrowser = HttpContext.Current.Request.Browser.Browser
            Version = HttpContext.Current.Request.Browser.MajorVersion
            arBrowsers = HttpContext.Current.Request.Browser.Browsers
        End If
        If Me.DesignMode Or sBrowser = "IE" And Version > 6 Then
            ' Tabla
            writer.AddAttribute(HtmlTextWriterAttribute.Cellpadding, "0")
            writer.AddAttribute(HtmlTextWriterAttribute.Cellspacing, "0")
            writer.AddAttribute(HtmlTextWriterAttribute.Border, "0")
            FSNLibrary.AddAtributoEstilo(writer, HtmlTextWriterStyle.BackgroundColor, Drawing.ColorTranslator.ToHtml(IIf(Me.Selected, Me.ColorFondoHover, Me.ColorFondo)))
            FSNLibrary.AddAtributoEstilo(writer, HtmlTextWriterStyle.Cursor, "pointer")
            FSNLibrary.AddAtributoEstilo(writer, HtmlTextWriterStyle.Color, Drawing.ColorTranslator.ToHtml(IIf(Me.Selected, Me.ForeColorHover, Me.ForeColor)))
            FSNLibrary.AddAtributoEstilo(writer, HtmlTextWriterStyle.TextDecoration, "none")
            writer.RenderBeginTag(HtmlTextWriterTag.Table)
            ' Primera fila
            writer.RenderBeginTag(HtmlTextWriterTag.Tr)
            ' Celda top left
            If CStr(ViewState("ImagenesBordes")) <> String.Empty Then _
                FSNLibrary.AddAtributoEstilo(writer, HtmlTextWriterStyle.BackgroundImage, Replace(CStr(ViewState("ImagenesBordes")), "~/", HttpContext.Current.Request.ApplicationPath & "/"))
            FSNLibrary.AddAtributoEstilo(writer, "background-position", "0px " & IIf(Me.Selected, "-12", "0") & "px")
            writer.RenderBeginTag(HtmlTextWriterTag.Td)
            writer.AddAttribute(HtmlTextWriterAttribute.Width, "6")
            writer.AddAttribute(HtmlTextWriterAttribute.Height, "6")
            writer.AddAttribute(HtmlTextWriterAttribute.Border, "0")
            writer.AddAttribute(HtmlTextWriterAttribute.Src, Me.Page.ClientScript.GetWebResourceUrl(GetType(FSNButton), "Fullstep.FSNWebControls.trans.gif"))
            writer.RenderBeginTag(HtmlTextWriterTag.Img)
            writer.RenderEndTag()
            writer.RenderEndTag()
            ' Celda top middle
            writer.AddAttribute(HtmlTextWriterAttribute.Rowspan, "3")
            FSNLibrary.AddAtributoEstilo(writer, HtmlTextWriterStyle.Padding, "2px 2px 2px 2px")
            writer.AddAttribute(HtmlTextWriterAttribute.Nowrap, "true")
            If Not Me.Font.Size.IsEmpty Then _
                FSNLibrary.AddAtributoEstilo(writer, HtmlTextWriterStyle.FontSize, Font.Size.ToString())
            writer.RenderBeginTag(HtmlTextWriterTag.Td)
            writer.Write(IIf(TextoMayusculas, Me.Text.ToUpper, Me.Text))
            writer.RenderEndTag()
            ' Celda top right
            If CStr(ViewState("ImagenesBordes")) <> String.Empty Then _
                FSNLibrary.AddAtributoEstilo(writer, HtmlTextWriterStyle.BackgroundImage, Replace(CStr(ViewState("ImagenesBordes")), "~/", HttpContext.Current.Request.ApplicationPath & "/"))
            FSNLibrary.AddAtributoEstilo(writer, "background-position", "-6px " & IIf(Me.Selected, "-12", "0") & "px")
            writer.RenderBeginTag(HtmlTextWriterTag.Td)
            writer.AddAttribute(HtmlTextWriterAttribute.Width, "6")
            writer.AddAttribute(HtmlTextWriterAttribute.Height, "6")
            writer.AddAttribute(HtmlTextWriterAttribute.Border, "0")
            writer.AddAttribute(HtmlTextWriterAttribute.Src, Me.Page.ClientScript.GetWebResourceUrl(GetType(FSNButton), "Fullstep.FSNWebControls.trans.gif"))
            writer.RenderBeginTag(HtmlTextWriterTag.Img)
            writer.RenderEndTag()
            writer.RenderEndTag()
            writer.RenderEndTag()
            ' Segunda fila
            FSNLibrary.AddAtributoEstilo(writer, HtmlTextWriterStyle.Height, "8px")
            writer.RenderBeginTag(HtmlTextWriterTag.Tr)
            writer.RenderBeginTag(HtmlTextWriterTag.Td)
            writer.RenderEndTag()
            writer.RenderBeginTag(HtmlTextWriterTag.Td)
            writer.RenderEndTag()
            writer.RenderEndTag()
            ' Tercera fila
            writer.RenderBeginTag(HtmlTextWriterTag.Tr)
            ' Celda bottom left
            If CStr(ViewState("ImagenesBordes")) <> String.Empty Then _
                FSNLibrary.AddAtributoEstilo(writer, HtmlTextWriterStyle.BackgroundImage, Replace(CStr(ViewState("ImagenesBordes")), "~/", HttpContext.Current.Request.ApplicationPath & "/"))
            FSNLibrary.AddAtributoEstilo(writer, "background-position", "0px " & IIf(Me.Selected, "-18", "-6") & "px")
            writer.RenderBeginTag(HtmlTextWriterTag.Td)
            writer.AddAttribute(HtmlTextWriterAttribute.Width, "6")
            writer.AddAttribute(HtmlTextWriterAttribute.Height, "6")
            writer.AddAttribute(HtmlTextWriterAttribute.Border, "0")
            writer.AddAttribute(HtmlTextWriterAttribute.Src, Me.Page.ClientScript.GetWebResourceUrl(GetType(FSNButton), "Fullstep.FSNWebControls.trans.gif"))
            writer.RenderBeginTag(HtmlTextWriterTag.Img)
            writer.RenderEndTag()
            writer.RenderEndTag()
            ' Celda bottom right
            If CStr(ViewState("ImagenesBordes")) <> String.Empty Then _
                FSNLibrary.AddAtributoEstilo(writer, HtmlTextWriterStyle.BackgroundImage, Replace(CStr(ViewState("ImagenesBordes")), "~/", HttpContext.Current.Request.ApplicationPath & "/"))
            FSNLibrary.AddAtributoEstilo(writer, "background-position", "-6px " & IIf(Me.Selected, "-18", "-6") & "px")
            writer.RenderBeginTag(HtmlTextWriterTag.Td)
            writer.AddAttribute(HtmlTextWriterAttribute.Width, "6")
            writer.AddAttribute(HtmlTextWriterAttribute.Height, "6")
            writer.AddAttribute(HtmlTextWriterAttribute.Border, "0")
            writer.AddAttribute(HtmlTextWriterAttribute.Src, Me.Page.ClientScript.GetWebResourceUrl(GetType(FSNButton), "Fullstep.FSNWebControls.trans.gif"))
            writer.RenderBeginTag(HtmlTextWriterTag.Img)
            writer.RenderEndTag()
            writer.RenderEndTag()
            writer.RenderEndTag()
            writer.RenderEndTag()
        Else
            writer.Write(IIf(TextoMayusculas, Me.Text.ToUpper, Me.Text))
        End If
    End Sub

    ''' <summary>
    ''' Inicializa el tamaño de letra del control.
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub New()
        MyBase.New()
        MyBase.Font.Size = New FontUnit(12, UnitType.Pixel)
    End Sub

    ''' <summary>
    ''' Si no existen genera las funciones javascript para los efectos del FSNButton
    ''' </summary>
    ''' <param name="e">Objeto EventArgs que contiene los datos del evento.</param>
    Protected Overrides Sub OnPreRender(ByVal e As System.EventArgs)
        MyBase.OnPreRender(e)
        Dim sBrowser As String = ""
        Dim Version As Integer
        Dim arBrowsers As New ArrayList
        If Not Me.DesignMode Then
            sBrowser = HttpContext.Current.Request.Browser.Browser
            Version = HttpContext.Current.Request.Browser.MajorVersion
            arBrowsers = HttpContext.Current.Request.Browser.Browsers
        End If
        If Me.DesignMode Or sBrowser = "IE" And Version > 6 Then
            ' Scripts
            If Not Me.DesignMode Then
                If Not Me.Page.ClientScript.IsClientScriptBlockRegistered(Me.Page.GetType(), "fsnbutton_scripts") Then
                    Dim sScript As String = "function fsnbutton_over(boton, color, foreColor) {" & vbCrLf &
                        "try{" & vbCrLf &
                        "boton.style.backgroundColor = color;" & vbCrLf &
                        "boton.style.color = foreColor;" & vbCrLf &
                        "boton.rows[0].cells[0].style.backgroundPosition = '0px -12px';" & vbCrLf &
                        "boton.rows[0].cells[2].style.backgroundPosition = '-6px -12px';" & vbCrLf &
                        "boton.rows[2].cells[0].style.backgroundPosition = '0px -18px';" & vbCrLf &
                        "boton.rows[2].cells[1].style.backgroundPosition = '-6px -18px';" & vbCrLf &
                        "} catch(e) {}" & vbCrLf &
                        "};" & vbCrLf &
                        "function fsnbutton_out(boton, color, foreColor) {" & vbCrLf &
                        "try{" & vbCrLf &
                        "boton.style.backgroundColor = color;" & vbCrLf &
                        "boton.style.color = foreColor;" & vbCrLf &
                        "boton.rows[0].cells[0].style.backgroundPosition = '0px 0px';" & vbCrLf &
                        "boton.rows[0].cells[2].style.backgroundPosition = '-6px -0px';" & vbCrLf &
                        "boton.rows[2].cells[0].style.backgroundPosition = '0px -6px';" & vbCrLf &
                        "boton.rows[2].cells[1].style.backgroundPosition = '-6px -6px';" & vbCrLf &
                        "boton.rows[0].cells[1].style.padding = '2px 2px 2px 2px';" & vbCrLf &
                        "} catch(e) {}" & vbCrLf &
                        "};" & vbCrLf &
                        "function fsnbutton_down(boton) {" & vbCrLf &
                        "try{" & vbCrLf &
                        "boton.rows[0].cells[1].style.padding = '3px 3px 1px 1px';" & vbCrLf &
                        "} catch(e) {}" & vbCrLf &
                        "boton.focus();" & vbCrLf &
                        "};" & vbCrLf &
                        "function fsnbutton_up(boton) {" & vbCrLf &
                        "try{" & vbCrLf &
                        "boton.rows[0].cells[1].style.padding = '2px 2px 2px 2px';" & vbCrLf &
                        "} catch(e) {}" & vbCrLf &
                        "};"
                    Me.Page.ClientScript.RegisterClientScriptBlock(Me.Page.GetType(), "fsnbutton_scripts", sScript, True)
                End If
            End If
        End If
    End Sub
End Class
