﻿Imports System
Imports System.Text
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls

Public Class FSNTreeView

    Inherits TreeView

    ''' <summary>
    ''' Al crear un Treenode podemos crearlos de una clase propia de nodo, FSNTreeNode
    ''' </summary>
    ''' <returns>Un objeto Treenode de la clase FSNTreeNode</returns>
    ''' <remarks>LLamada desde ; Timepo máx: 0 seg.</remarks>
    Protected Overloads Overrides Function CreateNode() As TreeNode
        Return New FSNTreeNode()
    End Function
End Class
