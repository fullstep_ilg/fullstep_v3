﻿Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.HtmlControls
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.ComponentModel
Imports System.Configuration

Public Class FSNAyuda
    Inherits WebPart

    Private lblTitulo As Label
    Private lblDescripcion As Label
    Private btnManual As FSNHyperlink
    Private mManualesSeleccionados As String
    Private sTitulo As String
    Private _Textos As DataSet
    Private _sIdioma As String

    Private Class ControlEditor
        Inherits EditorPart

        Private _txtTitulo As TextBox
        Private _txtAncho As TextBox
        Private _reqAncho As RequiredFieldValidator
        Private _ranAncho As RangeValidator
        Private _chkManualesDisponibles As CheckBoxList

        ''' <summary>
        ''' Lo llama el marco de trabajo de las páginas ASP.NET para indicar a los controles de servidor que utilizan la implementación basada en la composición que creen los controles secundarios que contengan como forma de preparar la devolución o representación de los datos.
        ''' </summary>
        Protected Overrides Sub CreateChildControls()
            Dim part As FSNAyuda = CType(WebPartToEdit, FSNAyuda)
            Controls.Clear()
            _txtTitulo = New TextBox()
            _txtTitulo.Width = Unit.Pixel(250)
            _txtTitulo.Text = part.Title
            Controls.Add(_txtTitulo)
            _txtAncho = New TextBox()
            _txtAncho.MaxLength = 3
            _txtAncho.Width = Unit.Pixel(30)
            _txtAncho.Text = part.Width.Value.ToString()
            _txtAncho.ID = Me.ID & "_txtAncho"
            Controls.Add(_txtAncho)
            _reqAncho = New RequiredFieldValidator()
            _reqAncho.ID = _txtAncho.ID & "_Req"
            _reqAncho.ErrorMessage = "*"
            _reqAncho.ControlToValidate = _txtAncho.ID
            Controls.Add(_reqAncho)
            _ranAncho = New RangeValidator()
            _ranAncho.ID = _txtAncho.ID & "_Ran"
            _ranAncho.Type = ValidationDataType.Integer
            _ranAncho.MaximumValue = 500
            _ranAncho.MinimumValue = 200
            _ranAncho.ErrorMessage = "(200-500)"
            _ranAncho.ControlToValidate = _txtAncho.ID
            Controls.Add(_ranAncho)

            Dim pag As FSNServer.IFSNPage
            pag = CType(Me.Page, FSNServer.IFSNPage)
            _chkManualesDisponibles = New CheckBoxList
            _chkManualesDisponibles.Items.Clear()

            Dim colAppSettings As System.Collections.Specialized.NameValueCollection
            colAppSettings = System.Configuration.ConfigurationManager.AppSettings

            Dim sPatronTextoPM As String = "manualPM_nom*"
            Dim sPatronTextoEP As String = "manualEP_nom*"
            Dim sPatronTextoQA As String = "manualQA_nom*"

            For Each sKey As String In colAppSettings
                If sKey Like sPatronTextoPM And pag.Acceso.gsAccesoFSPM <> FSNLibrary.TipoAccesoFSPM.SinAcceso And pag.Usuario.AccesoPM Then
                    _chkManualesDisponibles.Items.Add(New ListItem(colAppSettings.Get(sKey), colAppSettings.Get(sKey)))
                End If
                If sKey Like sPatronTextoEP And pag.Acceso.gbAccesoFSEP And pag.Usuario.AccesoEP Then
                    _chkManualesDisponibles.Items.Add(New ListItem(colAppSettings.Get(sKey), colAppSettings.Get(sKey)))
                End If
                If sKey Like sPatronTextoQA And pag.Acceso.gsAccesoFSQA <> FSNLibrary.TipoAccesoFSQA.SinAcceso And pag.Usuario.AccesoQA Then
                    _chkManualesDisponibles.Items.Add(New ListItem(colAppSettings.Get(sKey), colAppSettings.Get(sKey)))
                End If
            Next

            Controls.Add(_chkManualesDisponibles)
        End Sub
        ''' <summary>
        ''' Presenta el contenido del control en el sistema de escritura especificado. Este método lo usan principalmente los programadores de controles. 
        ''' </summary>
        ''' <param name="writer">HtmlTextWriter que representa la secuencia de salida para presentar contenido HTML en el cliente.</param>
        Protected Overrides Sub RenderContents(ByVal writer As System.Web.UI.HtmlTextWriter)
            Dim part As FSNAyuda = CType(WebPartToEdit, FSNAyuda)
            writer.Write("<b>" & part.Textos(19) & ":</b> ")
            _txtTitulo.RenderControl(writer)
            writer.WriteBreak()
            writer.Write("<b>" & part.Textos(21) & ":</b> ")
            _txtAncho.RenderControl(writer)
            _reqAncho.RenderControl(writer)
            _ranAncho.RenderControl(writer)
            writer.WriteBreak()
            writer.WriteBreak()
            writer.Write("<div style='border: 1px solid #CCCCCC;'><b>&nbsp;&nbsp;&nbsp;" & part.Textos(229) & "</b>")
            writer.WriteBreak()
            _chkManualesDisponibles.RenderControl(writer)
            writer.WriteBreak()
            writer.Write("</div>")
            writer.WriteBreak()
        End Sub
        ''' <summary>
        ''' Guarda los valores de un control EditorPart en las propiedades correspondientes del control WebPart asociado.
        ''' </summary>
        ''' <returns>Es true si la acción de guardar los valores del control EditorPart en el control WebPart se realiza correctamente; en caso contrario (si se produce un error), es false.</returns>
        Public Overrides Function ApplyChanges() As Boolean
            _reqAncho.Validate()
            _ranAncho.Validate()
            If _reqAncho.IsValid() And _ranAncho.IsValid() Then
                Dim part As FSNAyuda = CType(WebPartToEdit, FSNAyuda)
                part.Title = _txtTitulo.Text
                part.Width = Unit.Pixel(CType(_txtAncho.Text, Integer))
                Dim manuales As String = ""
                For Each x As ListItem In _chkManualesDisponibles.Items
                    If x.Selected Then
                        manuales = manuales & CType(x.Value, String) & ","
                    End If
                Next
                If Len(manuales) > 0 Then
                    manuales = Left(manuales, Len(manuales) - 1)
                End If
                part.mManualesSeleccionados = manuales
                part.CreateChildControls()
                Return True
            Else
                Return False
            End If
        End Function
        ''' <summary>
        ''' Obtiene los valores de propiedad de un control WebPart correspondientes a su control EditorPart asociado.
        ''' </summary>
        Public Overrides Sub SyncChanges()
            Dim part As FSNAyuda = CType(WebPartToEdit, FSNAyuda)
            txtTitulo.Text = part.Title
            txtAncho.Text = part.Width.Value.ToString()

            If part.mManualesSeleccionados <> Nothing Then
                For Each x As ListItem In chkManualesDisponibles.Items
                    x.Selected = IIf(InStr(part.mManualesSeleccionados, CType(x.Value, String), ) > 0, True, False)
                Next
            Else
                For Each x As ListItem In chkManualesDisponibles.Items
                    x.Selected = True
                Next
            End If
        End Sub
        Private ReadOnly Property txtTitulo() As TextBox
            Get
                EnsureChildControls()
                Return _txtTitulo
            End Get
        End Property
        Private ReadOnly Property txtAncho() As TextBox
            Get
                EnsureChildControls()
                Return _txtAncho
            End Get
        End Property
        Private ReadOnly Property chkManualesDisponibles() As CheckBoxList
            Get
                EnsureChildControls()
                Return _chkManualesDisponibles
            End Get
        End Property
    End Class
    <Personalizable(), WebBrowsable(), DefaultValue("")> _
        Public Property ManualesSeleccionados() As String
        Get
            Return mManualesSeleccionados
        End Get
        Set(ByVal value As String)
            mManualesSeleccionados = value
        End Set
    End Property
    ''' <summary>
    ''' Devuelve el idioma de la página que alberga el control.
    ''' </summary>
    ''' <returns>El código de idioma.</returns>
    Private Function Idioma() As String
        If Not DesignMode Then
            If Me.Page Is Nothing AndAlso TypeOf Me.Page Is FSNServer.IFSNPage Then
                Return CType(Me.Page, FSNServer.IFSNPage).Idioma
            Else
                For Each x As String In HttpContext.Current.Session.Keys
                    If TypeOf HttpContext.Current.Session(x) Is FSNServer.User Then
                        Return CType(HttpContext.Current.Session(x), FSNServer.User).Idioma
                    End If
                Next
                Return String.Empty
            End If
        Else
            Return String.Empty
        End If
    End Function
    Public ReadOnly Property Textos(ByVal Index As Short) As String
        Get
            If _Textos Is Nothing Or Idioma() <> _sIdioma Then
                If Not DesignMode Then
                    Dim FSNServer As FSNServer.Root = HttpContext.Current.Session("FSN_Server")
                    Dim FSNDict As FSNServer.Dictionary
                    If FSNServer Is Nothing Then
                        FSNDict = New FSNServer.Dictionary()
                    Else
                        FSNDict = FSNServer.Get_Object(GetType(FSNServer.Dictionary))
                    End If
                    _sIdioma = Idioma()
                    If HttpContext.Current.Cache("Textos_" & _sIdioma & "_" & FSNLibrary.TiposDeDatos.ModulosIdiomas.Webparts) Is Nothing Then
                        FSNDict.LoadData(FSNLibrary.TiposDeDatos.ModulosIdiomas.Webparts, _sIdioma)
                        HttpContext.Current.Cache.Insert("Textos_" & _sIdioma & "_" & FSNLibrary.TiposDeDatos.ModulosIdiomas.Webparts, FSNDict.Data, Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration,
                                                         New TimeSpan(0, System.Configuration.ConfigurationManager.AppSettings("TiempoExpiracionCacheMin"), 0))
                    End If
                    _Textos = HttpContext.Current.Cache("Textos_" & _sIdioma & "_" & FSNLibrary.TiposDeDatos.ModulosIdiomas.Webparts)
                    Return _Textos.Tables(0).Rows(Index).Item(1)
                Else
                    Return String.Empty
                End If
            Else
                Return _Textos.Tables(0).Rows(Index).Item(1)
            End If
        End Get
    End Property
    Public Overrides Property Title() As String
        Get
            If String.IsNullOrEmpty(MyBase.Title) Then
                If String.IsNullOrEmpty(sTitulo) Then
                    If Me.Page Is Nothing And String.IsNullOrEmpty(Textos(51)) Then
                        MyBase.Title = Textos(51)
                        sTitulo = MyBase.Title
                    Else
                        sTitulo = Textos(51)
                    End If
                End If
                Return sTitulo
            Else
                sTitulo = MyBase.Title
                Return MyBase.Title
            End If
        End Get
        Set(ByVal value As String)
            MyBase.Title = value
        End Set
    End Property
    ''' <summary>
    ''' Carga los datos por defecto para el control
    ''' </summary>
    Public Sub New()
        Me.Width = Unit.Pixel(300)
    End Sub
    ''' <summary>
    ''' Lo llama el marco de trabajo de las páginas ASP.NET para indicar a los controles de servidor que utilizan la implementación basada en la composición que creen los controles secundarios que contengan como forma de preparar la devolución o representación de los datos.
    ''' </summary>
    Protected Overrides Sub CreateChildControls()
        Controls.Clear()
        Dim img As HtmlImage = New HtmlImage()
        img.Src = Me.Page.ClientScript.GetWebResourceUrl(GetType(FSNAyuda), "Fullstep.FSNWebControls.trans.gif")
        img.Width = 300
        img.Height = 1
        Me.Controls.Add(img)
        Dim tabla As HtmlTable = New HtmlTable()
        tabla.Width = "100%"
        tabla.Height = "55px"
        Dim fila As HtmlTableRow = New HtmlTableRow()
        fila.Height = "40px"
        Dim celda As HtmlTableCell = New HtmlTableCell()
        'celda.Width = "90%"
        lblTitulo = New Label()
        lblTitulo.CssClass = "Rotulo"
        celda.Controls.Add(lblTitulo)
        celda.Controls.Add(New LiteralControl("<br/>"))
        fila.Cells.Add(celda)
        celda = New HtmlTableCell
        celda.Align = "right"
        celda.VAlign = "bottom"
        img.Width = 70
        celda.Controls.Add(img)
        celda.Style.Add("background-image", Me.Page.ClientScript.GetWebResourceUrl(GetType(FSNAyuda), "Fullstep.FSNWebControls.flechadescarga.JPG"))
        celda.Style.Add("background-position", "top center")
        celda.Style.Add("background-repeat", "no-repeat")
        celda.RowSpan = 2
        fila.Cells.Add(celda)
        tabla.Rows.Add(fila)

        fila = New HtmlTableRow
        celda = New HtmlTableCell
        lblDescripcion = New Label()
        lblDescripcion.CssClass = "Etiqueta"
        celda.Controls.Add(lblDescripcion)
        fila.Cells.Add(celda)
        tabla.Rows.Add(fila)

        fila = New HtmlTableRow
        celda = New HtmlTableCell
        celda.ColSpan = 2

        Dim Lista As New HtmlControls.HtmlGenericControl("UL")
        Lista.Style.Add("list-style", "none")
        Dim colAppSettings As System.Collections.Specialized.NameValueCollection

        colAppSettings = System.Configuration.ConfigurationManager.AppSettings

        Dim CargarEnlace As Boolean = False
        For Each sKey As String In colAppSettings
            Dim ElementoLista As HtmlControls.HtmlGenericControl
            Dim Enlace As HyperLink

            If CargarEnlace Then
                Enlace.NavigateUrl = System.Configuration.ConfigurationManager.AppSettings("rutaManual") & Idioma() & "_" & colAppSettings.Get(sKey)
                Enlace.Target = "_blank"
                ElementoLista = New HtmlControls.HtmlGenericControl("LI")
                ElementoLista.Style.Add("margin-bottom", "4px")
                ElementoLista.Controls.Add(Enlace)
                Lista.Controls.Add(ElementoLista)
                CargarEnlace = False
            End If
            Dim iPos As Short = InStr(mManualesSeleccionados, colAppSettings.Get(sKey))
            Dim iLen As Short = colAppSettings.Get(sKey).Length
            If iPos > 0 AndAlso (iPos + iLen - 1) = mManualesSeleccionados.Length Then
                Enlace = New HyperLink
                Enlace.Text = colAppSettings.Get(sKey)
                CargarEnlace = True
            Else
                If iPos > 0 AndAlso iLen <= mManualesSeleccionados.Length AndAlso mManualesSeleccionados.Substring(iPos + iLen - 1, 1) = "," Then
                    Enlace = New HyperLink
                    Enlace.Text = colAppSettings.Get(sKey)
                    CargarEnlace = True
                Else
                    CargarEnlace = False
                End If
            End If
        Next
        celda.Controls.Add(Lista)

        fila.Cells.Add(celda)
        tabla.Rows.Add(fila)
        Me.Controls.Add(tabla)

        lblTitulo.Text = Textos(4)
        lblDescripcion.Text = Textos(2)
    End Sub
    ''' <summary>
    ''' Refresca los datos del webpart
    ''' </summary>
    Public Sub Actualizar()
        CargarPermisosUsuario()
        CreateChildControls()
    End Sub
    Private Sub CargarPermisosUsuario()
        Dim pag As FSNServer.IFSNPage
        pag = CType(Me.Page, FSNServer.IFSNPage)
        'PM
        If pag.Acceso.gsAccesoFSPM <> FSNLibrary.TipoAccesoFSPM.SinAcceso And pag.Usuario.AccesoPM Then
            mManualesSeleccionados = "PM,"
        End If
        'QA
        If pag.Acceso.gsAccesoFSQA <> FSNLibrary.TipoAccesoFSQA.SinAcceso And pag.Usuario.AccesoQA Then
            mManualesSeleccionados = mManualesSeleccionados & "QA,"
        End If
        'EP
        If pag.Acceso.gbAccesoFSEP And pag.Usuario.AccesoEP Then
            mManualesSeleccionados = mManualesSeleccionados & "EP,"
        End If
        If Len(mManualesSeleccionados) > 0 Then
            mManualesSeleccionados = Left(mManualesSeleccionados, Len(mManualesSeleccionados) - 1)
        End If
    End Sub
    ''' <summary>
    ''' Envía el contenido del control de servidor a un objeto HtmlTextWriter que se proporciona y almacena la información de seguimiento sobre el control si dicho seguimiento está habilitado.
    ''' </summary>
    ''' <param name="writer">Objeto HTmlTextWriter que recibe el contenido del control.</param>
    Public Overrides Sub RenderControl(ByVal writer As System.Web.UI.HtmlTextWriter)
        MyBase.RenderControl(writer)
    End Sub
    ''' <summary>
    ''' Devuelve una colección de controles EditorPart personalizados que pueden utilizarse para editar un control WebPart cuando se encuentra en modo de edición.
    ''' </summary>
    ''' <returns>Una colección EditorPartCollection que contiene controles EditorPart personalizados asociados a un control WebPart.</returns>
    Public Overrides Function CreateEditorParts() As System.Web.UI.WebControls.WebParts.EditorPartCollection
        Dim editorArray As New ArrayList()
        Dim edPart As New ControlEditor()
        edPart.ID = Me.ID & "_editor1"
        editorArray.Add(edPart)
        Dim editorParts As New EditorPartCollection(editorArray)
        Return editorParts
    End Function
    Public Overrides ReadOnly Property WebBrowsableObject() As Object
        Get
            Return Me
        End Get
    End Property
    ''' <summary>
    ''' Registra el control como un control cuyo estado se debe conservar.
    ''' </summary>
    ''' <param name="e">Objeto EventArgs que contiene los datos del evento.</param>
    Protected Overrides Sub OnInit(ByVal e As System.EventArgs)
        MyBase.OnInit(e)
        Page.RegisterRequiresControlState(Me)
    End Sub
    ''' <summary>
    ''' Guarda los cambios de estado del control.
    ''' </summary>
    ''' <returns>Devuelve el estado actual del control de servidor.</returns>
    Protected Overrides Function SaveControlState() As Object
        Dim obj As Object = MyBase.SaveControlState()
        If obj IsNot Nothing Then
            obj = New Pair(obj, _Textos)
        Else
            obj = _Textos
        End If
        obj = New Pair(obj, _sIdioma)
        Return obj
    End Function
    ''' <summary>
    ''' Restaura información de estado del control.
    ''' </summary>
    ''' <param name="state">Object que representa el estado del control que se va a restaurar.</param>
    Protected Overrides Sub LoadControlState(ByVal state As Object)
        If (state IsNot Nothing) Then
            Dim p As Pair = TryCast(state, Pair)
            _sIdioma = p.Second
            state = p.First
            p = TryCast(state, Pair)
            If p IsNot Nothing Then
                MyBase.LoadControlState(p.First)
                _Textos = CType(p.Second, DataSet)
            Else
                If (TypeOf (state) Is String()) Then
                    _Textos = CType(state, DataSet)
                Else
                    MyBase.LoadControlState(state)
                End If
            End If
        End If
    End Sub
End Class