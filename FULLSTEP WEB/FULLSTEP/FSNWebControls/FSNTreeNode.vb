﻿Imports System
Imports System.Text
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls

Public Class FSNTreeNode
    Inherits TreeNode
    Private NodeText As String = String.Empty
    Private sNodeID As String = String.Empty
    Private m_sIDCliente As String = String.Empty
    Private m_sImagen As String = String.Empty
    Private m_bInfo_Adic As Boolean


    Public Property IDCliente() As String
        Get
            IDCliente = m_sIDCliente
        End Get
        Set(ByVal value As String)
            m_sIDCliente = value
        End Set
    End Property

    Public Property Imagen() As String
        Get
            Imagen = m_sImagen
        End Get
        Set(ByVal value As String)
            m_sImagen = value
        End Set
    End Property

    Public Property info_adicional() As Boolean
        Get
            info_adicional = m_bInfo_Adic
        End Get
        Set(ByVal value As Boolean)
            m_bInfo_Adic = value
        End Set
    End Property

    Public Sub New()
    End Sub

    Public Sub New(ByVal aText As String)
        NodeText = aText
    End Sub

    ''' <summary>
    ''' Crear un nuevo nodo
    ''' </summary>
    ''' <param name="aText">Texto del nodo</param>
    ''' <param name="nodeID">Id del nodo</param>
    ''' <param name="IDCliente">ID del Treenode en el navegador cliente</param>
    ''' <remarks>Llamada: al crear un treenode; Tiempo máx: 0 seg.</remarks>
    Public Sub New(ByVal aText As String, ByVal nodeID As String, ByVal IDCliente As String)
        NodeText = aText
        sNodeID = nodeID
        m_sIDCliente = IDCliente

        Me.Text = String.Empty
        Me.Value = sNodeID
        Me.SelectAction = TreeNodeSelectAction.[Select]
    End Sub

    ''' <summary>
    ''' Antes de que se generen las características por defecto del Treenode, le añadimos una serie de elementos personalizados
    ''' </summary>
    ''' <param name="writer">Objeto que nos permite escribir en el control</param>
    ''' <remarks>Llamada: al crear un treenode; Tiempo máx: 0 seg.</remarks>
    Protected Overloads Overrides Sub RenderPreText(ByVal writer As HtmlTextWriter)
        writer.AddAttribute("style", "vertical-align:bottom;white-space:nowrap;")
        'Abrir SPAN
        writer.RenderBeginTag(HtmlTextWriterTag.Span)

        If info_adicional Then
            writer.AddAttribute("Id", m_sIDCliente)
            writer.AddAttribute("href", "javascript:efectuarPostBack('" & m_sIDCliente & "_DetalleCategoria','" & Me.Value & "')")
            writer.AddAttribute("title", "")

            writer.RenderBeginTag(HtmlTextWriterTag.A)
            writer.AddAttribute("src", m_sImagen)
            writer.AddAttribute("style", "border:0px;")
            writer.AddAttribute("alternateText", "info")

            writer.RenderBeginTag(HtmlTextWriterTag.Img)
            writer.RenderEndTag()

            ' finish link
            writer.RenderEndTag()
        Else
            writer.AddAttribute("src", "../../images/trans.gif")
            writer.AddAttribute("style", "border:0px;")
            writer.AddAttribute("width", "12px")
            writer.RenderBeginTag(HtmlTextWriterTag.Img)
            writer.RenderEndTag()
        End If
        writer.Write("&nbsp;")
        'Cerrar SPAN
        writer.RenderEndTag()

        MyBase.RenderPreText(writer)
    End Sub

    ''' <summary>
    ''' Guardamos el ViewState de los elementos del treenode para no perderlos en los postback
    ''' </summary>
    ''' <returns>Array con el contenido guardado</returns>
    ''' <remarks></remarks>
    Protected Overloads Overrides Function SaveViewState() As Object
        Dim arrState As Object() = New Object(7) {}
        arrState(0) = MyBase.SaveViewState()
        arrState(1) = Me.NodeText
        arrState(2) = Me.sNodeID
        arrState(3) = Me.m_sIDCliente
        arrState(4) = Me.Imagen
        arrState(5) = Me.info_adicional

        Return arrState
    End Function

    ''' <summary>
    ''' Cargamos los datos del Treenode con los datos guardados
    ''' </summary>
    ''' <param name="savedState">Datos guardados del nodo</param>
    ''' <remarks></remarks>
    Protected Overloads Overrides Sub LoadViewState(ByVal savedState As Object)
        If savedState IsNot Nothing Then
            Dim arrState As Object() = TryCast(savedState, Object())

            Me.NodeText = DirectCast(arrState(1), String)
            Me.sNodeID = DirectCast(arrState(2), String)
            Me.m_sIDCliente = DirectCast(arrState(3), String)
            Me.m_sImagen = DirectCast(arrState(4), String)
            Me.info_adicional = DirectCast(arrState(5), Boolean)

            MyBase.LoadViewState(arrState(0))
        End If
    End Sub
End Class
