﻿Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Text
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls

<ToolboxData("<{0}:FSNUpDownArrows runat=server></{0}:FSNUpDownArrows>")> _
Public Class FSNUpDownArrows
    Inherits HtmlControls.HtmlContainerControl

    'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
    '           IMPORTANTE: AL USAR ESTE CONTROL, EL CONTROL A SU IZQUIERDA, SI SE DESEA 
    '               QUE LAS FLECHAS APAREZCAN A LA DERECHA, DEBE TENER FLOAT:LEFT
    'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

    Private oDiv As HtmlControls.HtmlGenericControl
    Private oImg As HtmlControls.HtmlImage

    ''' <summary>
    ''' Lo llama el marco de trabajo de las páginas ASP.NET para indicar a los controles de servidor que utilizan 
    ''' la implementación basada en la composición que creen los controles secundarios que contengan como forma 
    ''' de preparar la devolución o representación de los datos.
    ''' </summary>
    ''' <remarks>Llamada en el load de la página</remarks>
    Protected Overrides Sub CreateChildControls()

        Me.Attributes("class") = "arrowsContainer"

        oDiv = New HtmlControls.HtmlGenericControl("div")
        oDiv.ID = Me.ID & "_td_up"
        oDiv.Attributes.Add("class", "arrowsDiv")
        oDiv.Attributes.Add("align", "center")
        oDiv.Attributes.Add("unselectable", "on")

        oImg = New HtmlControls.HtmlImage
        oImg.Src = Me.Page.ClientScript.GetWebResourceUrl(Me.GetType(), "Fullstep.FSNWebControls.up.gif")
        oImg.Attributes("unselectable") = "on"

        oDiv.Controls.Add(oImg)
        Me.Controls.Add(oDiv)


        oDiv = New HtmlControls.HtmlGenericControl("div")
        oDiv.ID = Me.ID & "_td_down"
        oDiv.Attributes.Add("class", "arrowsDiv")
        oDiv.Attributes.Add("align", "center")
        oDiv.Attributes.Add("unselectable", "on")

        oImg = New HtmlControls.HtmlImage
        oImg.Src = Me.Page.ClientScript.GetWebResourceUrl(Me.GetType(), "Fullstep.FSNWebControls.down.gif")
        oImg.Attributes("unselectable") = "on"

        oDiv.Controls.Add(oImg)
        Me.Controls.Add(oDiv)

    End Sub

    ''' <summary>
    ''' Inicializa el control.
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub New()

    End Sub

    Private Sub UpDownArrows_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        EnsureChildControls()
    End Sub
End Class
