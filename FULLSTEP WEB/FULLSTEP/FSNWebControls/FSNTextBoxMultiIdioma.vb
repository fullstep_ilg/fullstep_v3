﻿Imports System.Web.UI
Imports System.ComponentModel
Imports System.Drawing.Design
Imports System.Web.UI.Design
Imports System.Web.UI.WebControls
Imports System.Web.Script.Serialization

<ToolboxData("<{0}:FSNTextBoxMultiIdioma runat='server' />")> _
Public Class FSNTextBoxMultiIdioma
    Inherits Control
#Region "Propiedades"
    'PROPIEDADES PERSONALIZABLES DEL CONTROL
    ''' <summary>
    ''' Ancho de la imagen del desplegable
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>        
    <Browsable(True), Bindable(True), _
    Category("Appearance"), Description("Establece el ancho del control"), _
    TypeConverter(GetType(UnitConverter)), _
    Themeable(True), DefaultValue(GetType(Unit), "200px")> _
    Public Property Width() As Unit
        Get
            Dim o As Object = ViewState("Width")
            If o Is Nothing Then
                Return Unit.Pixel(200)
            Else
                Return CType(o, Unit)
            End If
        End Get
        Set(ByVal value As Unit)
            ViewState("Width") = value
        End Set
    End Property
    <Browsable(True), Bindable(True), _
    Category("Appearance"), Description("Establece el ancho del control"), _
    TypeConverter(GetType(UnitConverter)), _
    Themeable(True), DefaultValue(GetType(Unit), "20px")> _
    Public Property WidthControlImage() As Unit
        Get
            Dim o As Object = ViewState("WidthControlImage")
            If o Is Nothing Then
                Return Unit.Pixel(20)
            Else
                Return CType(o, Unit)
            End If
        End Get
        Set(ByVal value As Unit)
            ViewState("WidthControlImage") = value
        End Set
    End Property
    <Browsable(True), Bindable(True), _
    Category("Appearance"), Description("Establece el alto de la imagen del control"), _
    TypeConverter(GetType(UnitConverter)), _
    Themeable(True), DefaultValue(GetType(Unit), "20px")> _
    Public Property HeightControlImage() As Unit
        Get
            Dim o As Object = ViewState("HeightControlImage")
            If o Is Nothing Then
                Return Unit.Pixel(20)
            Else
                Return CType(o, Unit)
            End If
        End Get
        Set(ByVal value As Unit)
            ViewState("HeightControlImage") = value
        End Set
    End Property
    <Browsable(True), Bindable(True), Description("Establece la clase de estilo de la caja de texto"), _
    Themeable(True), DefaultValue(""), _
    Category("Appearance")>
    Public Property TextBoxCssClass() As String
        Get
            Dim o As Object = ViewState("TextBoxCssClass")
            If o Is Nothing Then
                Return String.Empty
            Else
                Return CType(o, String)
            End If
        End Get
        Set(ByVal value As String)
            ViewState("TextBoxCssClass") = value
        End Set
    End Property
    Private _style As String = ""
    <Browsable(True), Bindable(True), _
    Category("Appearance"), Description("Establece el estilo del control"), _
    Editor(GetType(ImageUrlEditor), GetType(UITypeEditor)), UrlProperty(), _
    Themeable(True)> _
    Public Property style() As String
        Get
            Return _style
        End Get
        Set(ByVal value As String)
            _style = value
        End Set
    End Property
    Private _itemList As Dictionary(Of String, String)
    Public Property itemList() As Dictionary(Of String, String)
        Get
            Return _itemList
        End Get
        Set(ByVal value As Dictionary(Of String, String))
            _itemList = value
        End Set
    End Property
    <Browsable(True), Bindable(True), Description("Establece el idioma seleccionado. Pasamos como parametro el código del idioma"), _
    Themeable(True), DefaultValue(""), _
    Category("Appearance")>
    Public Property IdiomaSeleccionado() As String
        Get
            Dim o As Object = ViewState("IdiomaSeleccionado")
            If o Is Nothing Then
                Return String.Empty
            Else
                Return CType(o, String)
            End If
        End Get
        Set(ByVal value As String)
            ViewState("IdiomaSeleccionado") = value
        End Set
    End Property
#End Region
#Region "Controles"
    Private txtValue As HtmlControls.HtmlInputText
    Private imgSelectedIdioma As HtmlControls.HtmlImage
    Private seleccionador As Panel
    Private desplegable As HtmlControls.HtmlGenericControl
#End Region
#Region "Click Desplegable"
    Private _onClientClickDesplegable As String
    ''' <summary>
    ''' Funcion javascript que se ejecutara al pulsar el desplegable
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Browsable(True), Bindable(True), _
    Description("Establece la funcion javascript que se ejecutara al pulsar el desplegable"), Themeable(True), _
    Category("Action")> _
    Public Property OnClientClickDesplegable() As String
        Get
            EnsureChildControls()
            Return _onClientClickDesplegable
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            _onClientClickDesplegable = value
            desplegable.Attributes.Add("onclick", _onClientClickDesplegable)
        End Set
    End Property
    Public Event Desplegable_Click(ByVal Sender As Object, ByVal E As EventArgs)
    ''' <summary>
    ''' Nombre de la rutina que se ejecutara en servidor al hacer click en el desplegable
    ''' </summary>
    ''' <param name="E"></param>
    ''' <remarks></remarks>
    Protected Sub Desplegable_OnClick(ByVal E As EventArgs)
        RaiseEvent Desplegable_Click(Me, E)
    End Sub
    Private Sub Desplegable_OnClick(ByVal Sender As Object, ByVal E As EventArgs)
        Desplegable_OnClick(EventArgs.Empty)
    End Sub
#End Region
    Private Sub FSNTextBoxMultiIdioma_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Me.Page.ClientScript.RegisterClientScriptResource(Me.GetType(), "Fullstep.FSNWebControls.FSNTextBoxMultiIdioma.js")
        Dim serializer As New JavaScriptSerializer()
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "FSNTextBoxMultiIdioma_" & Me.ID, _
                "var itemsTextBoxMultiIdioma={'" & Me.ID & "':" & _
                    serializer.Serialize(DirectCast(itemList, Object)).ToString & "};" & _
                "OnTextBoxFocus('" & Me.ID & "','" & IdiomaSeleccionado & "');", True)
    End Sub
    ''' <summary>
    ''' Definimos los controles que apareceran en la cabecera
    ''' </summary>
    ''' <remarks></remarks>
    Protected Overrides Sub CreateChildControls()
        Controls.Clear()

        txtValue = New HtmlControls.HtmlInputText
        With txtValue
            .ID = Me.ID
            .Attributes.Add("class", TextBoxCssClass)
            .Attributes.Add("style", "clear:both; float:left; width:" & New Unit((Width.Value - WidthControlImage.Value - 15 - 7), Width.Type).ToString & ";")
            .Value = itemList(IdiomaSeleccionado)
        End With

        imgSelectedIdioma = New HtmlControls.HtmlImage
        With imgSelectedIdioma
            .ID = "imgSelectedIdioma_" & Me.ID
            .Attributes.Add("style", "float:left; width:" & WidthControlImage.ToString & "; height:" & HeightControlImage.ToString & ";")
            .Src = Me.Page.ClientScript.GetWebResourceUrl(GetType(FSNPageHeader), "Fullstep.FSNWebControls.IDI_" & IdiomaSeleccionado & ".png")
        End With

        seleccionador = New Panel
        With seleccionador
            .ID = "seleccionador_" & Me.ID
            .CssClass = "FSNTextBoxMultiIdiomaDesplegable"
            .Attributes.Add("style", "display:none; float:left; width:" & New Unit((WidthControlImage.Value + 15), Width.Type).ToString & ";")
            .Controls.Add(imgSelectedIdioma)
        End With

        desplegable = New HtmlControls.HtmlGenericControl("div")
        desplegable.ID = "desplegable_" & Me.ID
        desplegable.Attributes.Add("style", "clear:both; float:left; position:relative; width:100%;")
        Dim Lista As New HtmlControls.HtmlGenericControl("ul")
        Lista.ID = "FSNTextBoxMultiIdioma_options_" & Me.ID
        Lista.Attributes.Add("class", "fsComboDropDownList")
        Lista.Attributes.Add("style", "position:absolute; z-index:1000; width:100%;")

        Dim ItemLista, TextoItem, ImageItem As HtmlControls.HtmlGenericControl
        For Each key As String In itemList.Keys
            ItemLista = New HtmlControls.HtmlGenericControl("li")
            With ItemLista
                .ID = "FSNTextBoxMultiIdioma_option_" & Me.ID & "_IDI_" & key
                .Attributes.Add("class", "fsComboItem" & IIf(key = IdiomaSeleccionado, " fsComboItemSelected", ""))
                .Attributes.Add("style", "float:left; width:100%; padding:0px!important;")
            End With

            TextoItem = New HtmlControls.HtmlGenericControl("span")
            With TextoItem
                .ID = "FSNTextBoxMultiIdioma_option_txt_" & Me.ID & "_IDI_" & key
                .Attributes.Add("class", "Texto12")
                .Attributes.Add("style", "margin-left:3px;")
                .InnerText = itemList(key)
            End With
            ItemLista.Controls.Add(TextoItem)

            ImageItem = New HtmlControls.HtmlGenericControl("img")
            With ImageItem
                .ID = "FSNTextBoxMultiIdioma_option_img_" & Me.ID & "_IDI_" & key
                .Attributes.Add("src", Me.Page.ClientScript.GetWebResourceUrl(GetType(FSNPageHeader), "Fullstep.FSNWebControls.IDI_" & key & ".png"))
                .Attributes.Add("style", "float:right;")
            End With

            ItemLista.Controls.Add(ImageItem)

            Lista.Controls.Add(ItemLista)
        Next
        desplegable.Controls.Add(Lista)
    End Sub
    Protected Overrides Sub Render(ByVal writer As HtmlTextWriter)
        'Div contenedor
        writer.AddAttribute(HtmlTextWriterAttribute.Id, Me.ID & "_content")
        writer.AddAttribute(HtmlTextWriterAttribute.Style, "width:" & Width.ToString & ";" & style)
        writer.RenderBeginTag(HtmlTextWriterTag.Div)

        txtValue.RenderControl(writer)
        seleccionador.RenderControl(writer)
        desplegable.RenderControl(writer)

        writer.RenderEndTag()
        'Fin Div contenedor PageHeader
    End Sub
End Class
