﻿Imports System.ComponentModel
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.Design
Imports System.Web.UI.WebControls.WebParts

Public Class FSNCatalogZone
    Inherits CatalogZone

    Private _itemTemplate As ITemplate
    Private _item As ItemContainer

    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)> _
    Public ReadOnly Property Container() As ItemContainer
        Get
            Return _item
        End Get
    End Property

    <PersistenceMode(PersistenceMode.InnerProperty), _
    TemplateContainer(GetType(FSNCatalogZone))> _
    Public Property CatalogTemplate() As ITemplate
        Get
            Return _itemTemplate
        End Get
        Set(ByVal value As ITemplate)
            _itemTemplate = value
        End Set
    End Property

    Public ReadOnly Property SelectedCatalogPart() As CatalogPart
        Get
            Return CatalogParts(SelectedCatalogPartID)
        End Get
    End Property

    Public ReadOnly Property Descriptions() As WebPartDescriptionCollection
        Get
            Return SelectedCatalogPart.GetAvailableWebPartDescriptions()
        End Get
    End Property

    Public ReadOnly Property Descriptions(ByVal ID As String) As WebPartDescriptionCollection
        Get
            Return CatalogParts(ID).GetAvailableWebPartDescriptions()
        End Get
    End Property

    Public ReadOnly Property Zones() As WebPartZoneCollection
        Get
            Return Me.WebPartManager.Zones
        End Get
    End Property

    Public Sub AddWebPart(ByVal webPartID As String, ByVal zoneID As String)
        Dim descs As WebPartDescriptionCollection = SelectedCatalogPart.GetAvailableWebPartDescriptions()
        Dim NewWebPart As WebPart = SelectedCatalogPart.GetWebPart(descs(webPartID))

        Dim zones As WebPartZoneCollection = Me.WebPartManager.Zones
        Dim selectedZone As WebPartZoneBase = zones(zoneID)

        Me.WebPartManager.AddWebPart(NewWebPart, selectedZone, 0)
    End Sub

    Protected Overrides Sub CreateChildControls()
        MyBase.CreateChildControls()
        _item = New ItemContainer()
        _itemTemplate.InstantiateIn(_item)
        Controls.Add(_item)
    End Sub

    Protected Overrides Sub RenderBody(ByVal writer As System.Web.UI.HtmlTextWriter)
        _item.RenderControl(writer)
    End Sub

    ''' <summary>
    ''' Para que no se vea el desplegable con las zonas en el pie del catálogo
    ''' </summary>
    Protected Overrides Sub RenderFooter(ByVal writer As System.Web.UI.HtmlTextWriter)

    End Sub
End Class

<ToolboxItem(False)> _
Public Class ItemContainer
    Inherits Control

End Class
