﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices
Imports System.Web.UI

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("FSNWebControls")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyProduct("FSNWebControls")> 
<Assembly: TagPrefix("FSNWebControls", "fsn")> 

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("84c2f1c1-c088-4422-8494-9241fff735e4")> 

<Assembly: WebResource("Fullstep.FSNWebControls.trans.gif", "image/gif")> 
<Assembly: WebResource("Fullstep.FSNWebControls.Bt_Cerrar.png", "image/png")> 
<Assembly: WebResource("Fullstep.FSNWebControls.FSNPaneles.js", "application/x-javascript")> 
<Assembly: WebResource("Fullstep.FSNWebControls.flechadescarga.JPG", "image/jpeg")> 
<Assembly: WebResource("Fullstep.FSNWebControls.calendar.png", "image/png")> 
<Assembly: WebResource("Fullstep.FSNWebControls.importar.gif", "image/gif")> 
<Assembly: WebResource("Fullstep.FSNWebControls.calcular.gif", "image/gif")> 
<Assembly: WebResource("Fullstep.FSNWebControls.cerrar_eficaz.gif", "image/gif")> 
<Assembly: WebResource("Fullstep.FSNWebControls.cerrar_noeficaz.gif", "image/gif")> 
<Assembly: WebResource("Fullstep.FSNWebControls.emitir.gif", "image/gif")> 
<Assembly: WebResource("Fullstep.FSNWebControls.enviar_email.gif", "image/gif")> 
<Assembly: WebResource("Fullstep.FSNWebControls.guardar.gif", "image/gif")> 
<Assembly: WebResource("Fullstep.FSNWebControls.imprimir.png", "image/png")> 
<Assembly: WebResource("Fullstep.FSNWebControls.reabrir.gif", "image/gif")>
<Assembly: WebResource("Fullstep.FSNWebControls.suprimir.gif", "image/gif")>
<Assembly: WebResource("Fullstep.FSNWebControls.DeshacerBorrado.gif", "image/gif")>
<Assembly: WebResource("Fullstep.FSNWebControls.volver.gif", "image/gif")> 
<Assembly: WebResource("Fullstep.FSNWebControls.cabeceraFondo_GS.JPG", "image/jpeg")> 
<Assembly: WebResource("Fullstep.FSNWebControls.enviarcertificado.gif", "image/gif")> 
<Assembly: WebResource("Fullstep.FSNWebControls.solicitarCertificado.gif", "image/gif")> 
<Assembly: WebResource("Fullstep.FSNWebControls.renovar.gif", "image/gif")> 
<Assembly: WebResource("Fullstep.FSNWebControls.anular.gif", "image/gif")> 
<Assembly: WebResource("Fullstep.FSNWebControls.acciones.gif", "image/gif")> 
<Assembly: WebResource("Fullstep.FSNWebControls.DevolverSolicitud.png", "image/png")> 
<Assembly: WebResource("Fullstep.FSNWebControls.EnviarAFavoritos.gif", "image/gif")> 
<Assembly: WebResource("Fullstep.FSNWebControls.up.gif", "image/gif")> 
<Assembly: WebResource("Fullstep.FSNWebControls.down.gif", "image/gif")> 
<Assembly: WebResource("Fullstep.FSNWebControls.Lapiz.png", "image/png")> 
<Assembly: WebResource("Fullstep.FSNWebControls.Icono_Error_Amarillo_40x40.gif", "image/gif")> 
<Assembly: WebResource("Fullstep.FSNWebControls.AprobarSolicitud.png", "image/png")> 
<Assembly: WebResource("Fullstep.FSNWebControls.RechazarSolicitud.png", "image/png")> 
<Assembly: WebResource("Fullstep.FSNWebControls.AnularSolicitud.png", "image/png")> 
<Assembly: WebResource("Fullstep.FSNWebControls.TrasladarSolicitud.png", "image/png")> 
<Assembly: WebResource("Fullstep.FSNWebControls.FSNPageHeader.js", "application/x-javascript")> 
<Assembly: WebResource("Fullstep.FSNWebControls.jquery.js", "application/x-javascript")> 
<Assembly: WebResource("Fullstep.FSNWebControls.VolverColaboracion.png", "image/png")> 
<Assembly: WebResource("Fullstep.FSNWebControls.pdf.png", "image/png")> 
<Assembly: WebResource("Fullstep.FSNWebControls.xml.png", "image/png")> 
<Assembly: WebResource("Fullstep.FSNWebControls.eliminar.png", "image/png")> 
<Assembly: WebResource("Fullstep.FSNWebControls.nuevo.png", "image/png")> 
<Assembly: WebResource("Fullstep.FSNWebControls.copiar.png", "image/png")> 
<Assembly: WebResource("Fullstep.FSNWebControls.comparar.png", "image/png")> 
<Assembly: WebResource("Fullstep.FSNWebControls.FSNTextBoxMultiIdioma.js", "application/x-javascript")> 
<Assembly: WebResource("Fullstep.FSNWebControls.IDI_ENG.png", "image/png")> 
<Assembly: WebResource("Fullstep.FSNWebControls.IDI_GER.png", "image/png")> 
<Assembly: WebResource("Fullstep.FSNWebControls.IDI_SPA.png", "image/png")> 
<Assembly: WebResource("Fullstep.FSNWebControls.IDI_FRA.png", "image/png")> 
<Assembly: WebResource("Fullstep.FSNWebControls.recepcion.gif", "image/gif")> 
<Assembly: WebResource("Fullstep.FSNWebControls.Excel.png", "image/png")> 