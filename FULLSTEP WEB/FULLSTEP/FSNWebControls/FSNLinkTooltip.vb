﻿Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Text
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls


<DefaultProperty("Text"), ToolboxData("<{0}:FSNLinkTooltip runat=server></{0}:FSNLinkTooltip>")> _
Public Class FSNLinkTooltip
    Inherits LinkButton

    <Browsable(True), Description("Id del control FSNPanelTooltip"), _
    TypeConverter(GetType(AssociatedControlConverter)), Category("Behavior")> _
    Public Property PanelTooltip() As String
        Get
            Dim o As Object = ViewState("PanelTooltip")
            If o Is Nothing Then
                Return String.Empty
            Else
                Return CStr(o)
            End If
        End Get
        Set(ByVal value As String)
            ViewState("PanelTooltip") = value
            If MyBase.Attributes.Item("onmouseover") IsNot Nothing Then
                MyBase.Attributes.Item("onmouseover") = GenerarOnMouseOver()
            Else
                MyBase.Attributes.Add("onmouseover", GenerarOnMouseOver())
            End If
        End Set
    End Property

    <Browsable(True), Bindable(True), _
    Description("Código a pasar al WebMethod"), Themeable(True), _
    Category("Behavior")> _
    Public Property ContextKey() As String
        Get
            Dim o As Object = ViewState("ContextKey")
            If o Is Nothing Then
                Return String.Empty
            Else
                Return CStr(o)
            End If
        End Get
        Set(ByVal value As String)
            ViewState("ContextKey") = value
            If MyBase.Attributes.Item("onmouseover") IsNot Nothing Then
                MyBase.Attributes.Item("onmouseover") = GenerarOnMouseOver()
            Else
                MyBase.Attributes.Add("onmouseover", GenerarOnMouseOver())
            End If
        End Set
    End Property

    Public Shadows ReadOnly Property OnClientClick() As String
        Get
            Return MyBase.OnClientClick()
        End Get
    End Property

    Private Function GenerarOnMouseOver() As String
        Dim oCtrl As Control = DevolverControl(Me.PanelTooltip)
        Dim sCad As String = String.Empty
        If TypeOf (oCtrl) Is AjaxControlToolkit.ModalPopupExtender Then
            Dim oModal As AjaxControlToolkit.ModalPopupExtender = CType(oCtrl, AjaxControlToolkit.ModalPopupExtender)
            sCad = "FSNMostrarPanel('" & oModal.ClientID & "', event, null, null, null, 'tooltip'); return false"
        ElseIf TypeOf (oCtrl) Is FSNPanelTooltip Then
            Dim oPanel As FSNPanelTooltip = CType(oCtrl, FSNPanelTooltip)
            If oPanel Is Nothing Then Return String.Empty
            sCad = "FSNMostrarPanel('" & oPanel.AnimationClientID & "', event"
            If String.IsNullOrEmpty(oPanel.DynamicPopulateClientID) Then
                sCad = sCad & ", null"
            Else
                sCad = sCad & ", '" & oPanel.DynamicPopulateClientID & "'"
            End If
            If String.IsNullOrEmpty(Me.ContextKey) Then
                sCad = sCad & ", null"
            Else
                sCad = sCad & ", '" & Me.ContextKey & "'"
            End If
            If Not String.IsNullOrEmpty(oPanel.Contenedor) Then
                sCad = sCad & ", '" & DevolverControl(oPanel.Contenedor).ClientID & "'"
            Else
                sCad = sCad & ", null"
            End If
            sCad = sCad & ", 'tooltip'); return false"
            End If
        Return sCad
    End Function

    Private Function GenerarOnMouseOut() As String
        Dim oCtrl As Control = DevolverControl(Me.PanelTooltip)
        Dim oPanel As FSNPanelTooltip = CType(oCtrl, FSNPanelTooltip)
        If Not String.IsNullOrEmpty(oPanel.Contenedor) Then
            Return "FSNCerrarTooltipsTemp('" & DevolverControl(oPanel.Contenedor).ClientID & "'); return false"
        Else
            Return "FSNCerrarTooltipsTemp(); return false"
        End If
    End Function

    Private Function DevolverControl(ByVal Id As String) As Control
        Dim p As Control = Nothing
        Dim c As Control = Me.NamingContainer
        Do While c IsNot Nothing
            p = c.FindControl(Id)
            If p IsNot Nothing Then
                Exit Do
            Else
                c = c.NamingContainer
            End If
        Loop
        Return p
    End Function

    Private Sub FSNLinkTooltip_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Page.ClientScript.RegisterClientScriptResource(GetType(FSNPanelClientScript), "Fullstep.FSNWebControls.FSNPaneles.js")
        MyBase.OnClientClick = "return false"
        If MyBase.Attributes.Item("onmouseover") IsNot Nothing Then
            MyBase.Attributes.Item("onmouseover") = GenerarOnMouseOver()
        Else
            MyBase.Attributes.Add("onmouseover", GenerarOnMouseOver())
        End If
        MyBase.Attributes.Add("onmouseout", GenerarOnMouseOut())
    End Sub
End Class
