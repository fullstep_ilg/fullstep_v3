﻿Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Text
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.Design
Imports System.Drawing.Design
Imports System.IO

<ToolboxData("<{0}:FSNPageHeader runat='server' />")> _
Public Class FSNPageHeader
    Inherits Control
#Region "Controles"
    Private _ModuloIdioma As Integer
    Private lblCabecera As Label
    Private imgCabecera As HtmlControls.HtmlImage
#End Region
    Private _aspectoGS As Boolean
    Private _WidthImagenCabecera As String = "32px"
    Private _HeightImagenCabecera As String = "30px"

    'PROPIEDADES PERSONALIZABLES DEL CONTROL
    ''' <summary>
    ''' Ancho de la imagen de cabecera
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Browsable(True), Bindable(True), _
    Category("Appearance"), Description("Establece el ancho de la imagen de cabecera"), _
    Editor(GetType(ImageUrlEditor), GetType(UITypeEditor)), UrlProperty(), _
    Themeable(True)> _
    Public Property WidthImagenCabecera() As String
        Get
            EnsureChildControls()
            Return _WidthImagenCabecera
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            _WidthImagenCabecera = value
        End Set

    End Property
    ''' <summary>
    ''' Alto de la imagen de cabecera
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Browsable(True), Bindable(True), _
    Category("Appearance"), Description("Establece el alto de la imagen de cabecera"), _
    Editor(GetType(ImageUrlEditor), GetType(UITypeEditor)), UrlProperty(), _
    Themeable(True)> _
    Public Property HeightImagenCabecera() As String
        Get
            EnsureChildControls()
            Return _HeightImagenCabecera
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            _HeightImagenCabecera = value
        End Set

    End Property
    ''' <summary>
    ''' Imagen principal de la cabecera
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Browsable(True), Bindable(True), _
    Category("Appearance"), Description("Establece la imagen que aparecera en la cabecera como titulo"), _
    Editor(GetType(ImageUrlEditor), GetType(UITypeEditor)), UrlProperty(), _
    Themeable(True)> _
    Public Property UrlImagenCabecera() As String
        Get
            EnsureChildControls()
            Return imgCabecera.Src
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            imgCabecera.Src = value
        End Set

    End Property
    ''' <summary>
    ''' Establece si la pagina en la que se pondra el control tiene que tener aspecto GS
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Browsable(True), Bindable(True), _
   Description("Establece si la pagina en la que se pondra el control tiene que tener aspecto GS"), Themeable(True), _
   Category("Appearance"), DefaultValue(False)> _
    Public Property AspectoGS() As Boolean
        Get
            Return _aspectoGS
        End Get
        Set(ByVal value As Boolean)
            _aspectoGS = value
        End Set
    End Property
    ''' <summary>
    ''' Titulo que mostrara la cabecera
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Browsable(True), Bindable(True), _
    Description("Establece el texto que aparecera en la cabecera como titulo"), Themeable(True), _
    Category("Appearance")> _
    Public Property TituloCabecera() As String
        Get
            EnsureChildControls()
            Return lblCabecera.Text
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            lblCabecera.Text = value
            lblCabecera.ToolTip = value
        End Set
    End Property
#Region "Enviar Certificado"
    Private lnkEnviarCertificado As LinkButton
    Private _visibleBotonEnviarCertificado As Boolean
    Private _onClickEnviarCertificado As String
    ''' <summary>
    ''' Funcion javascript que se ejecutara al pulsar el botÃ³n de Enviar Certificado
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Browsable(True), Bindable(True), _
    Description("Establece la funciÃ³n javascript que se ejecutara al pulsar Enviar Certificado"), Themeable(True), _
    Category("Action")> _
    Public Property OnClientClickEnviarCertificado() As String
        Get
            EnsureChildControls()
            Return _onClickEnviarCertificado
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            _onClickEnviarCertificado = value
            lnkEnviarCertificado.Attributes.Add("onclick", _onClickEnviarCertificado)
        End Set
    End Property
    Public Event EnviarCertificado_Click(ByVal Sender As Object, ByVal E As EventArgs)
    ''' <summary>
    ''' Nombre de la rutina que se ejecutara en servidor al hacer click en el botÃ³n Enviar Certificado
    ''' </summary>
    ''' <param name="E"></param>
    ''' <remarks></remarks>
    Protected Sub EnviarCertificado_OnClick(ByVal E As EventArgs)
        RaiseEvent EnviarCertificado_Click(Me, E)
    End Sub
    Private Sub EnviarCertificado_OnClick(ByVal Sender As Object, ByVal E As EventArgs)
        EnviarCertificado_OnClick(EventArgs.Empty)
    End Sub
    <Browsable(True), Bindable(True), _
    Description("Establece el texto que aparecera en botÃ³n Enviar Certificado"), Themeable(True), _
    Category("Appearance")> _
        Public Property TextoBotonEnviarCertificado() As String
        Get
            EnsureChildControls()
            Return lnkEnviarCertificado.Text
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            lnkEnviarCertificado.Text = value
        End Set
    End Property
    <Browsable(True), Bindable(True), _
    Description("Establece si el botÃ³n Enviar Certificado serÃ¡ visible"), Themeable(True), _
    Category("Appearance"), DefaultValue(False)> _
        Public Property VisibleBotonEnviarCertificado() As Boolean
        Get
            Return _visibleBotonEnviarCertificado
        End Get
        Set(ByVal value As Boolean)
            _visibleBotonEnviarCertificado = value
        End Set
    End Property
#End Region
#Region "Solicitar Certificado"
    Private lnkSolicitarCertificado As LinkButton
    Private _visibleBotonSolicitarCertificado As Boolean
    Private _onClickSolicitarCertificado As String
    ''' <summary>
    ''' Funcion javascript que se ejecutara al pulsar el botÃ³n de Solicitar Certificado
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Browsable(True), Bindable(True), _
    Description("Establece la funciÃ³n javascript que se ejecutara al pulsar Solicitar Certificado"), Themeable(True), _
    Category("Action")> _
    Public Property OnClientClickSolicitarCertificado() As String
        Get
            EnsureChildControls()
            Return _onClickSolicitarCertificado
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            _onClickSolicitarCertificado = value
            lnkSolicitarCertificado.Attributes.Add("onclick", _onClickSolicitarCertificado)
        End Set
    End Property
    Public Event SolicitarCertificado_Click(ByVal Sender As Object, ByVal E As EventArgs)
    ''' <summary>
    ''' Nombre de la rutina que se ejecutara en servidor al hacer click en el botÃ³n Solicitar Certificado
    ''' </summary>
    ''' <param name="E"></param>
    ''' <remarks></remarks>
    Protected Sub SolicitarCertificado_OnClick(ByVal E As EventArgs)
        RaiseEvent SolicitarCertificado_Click(Me, E)
    End Sub
    Private Sub SolicitarCertificado_OnClick(ByVal Sender As Object, ByVal E As EventArgs)
        SolicitarCertificado_OnClick(EventArgs.Empty)
    End Sub
    <Browsable(True), Bindable(True), _
    Description("Establece el texto que aparecera en botÃ³n Solicitar Certificado"), Themeable(True), _
    Category("Appearance")> _
        Public Property TextoBotonSolicitarCertificado() As String
        Get
            EnsureChildControls()
            Return lnkSolicitarCertificado.Text
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            lnkSolicitarCertificado.Text = value
        End Set
    End Property
    <Browsable(True), Bindable(True), _
    Description("Establece si el botÃ³n Solicitar Certificado serÃ¡ visible"), Themeable(True), _
    Category("Appearance"), DefaultValue(False)> _
        Public Property VisibleBotonSolicitarCertificado() As Boolean
        Get
            Return _visibleBotonSolicitarCertificado
        End Get
        Set(ByVal value As Boolean)
            _visibleBotonSolicitarCertificado = value
        End Set
    End Property
#End Region
#Region "Renovar Certificado"
    Private lnkRenovarCertificado As LinkButton
    Private _visibleBotonRenovarCertificado As Boolean
    Private _onClickRenovarCertificado As String
    ''' <summary>
    ''' Funcion javascript que se ejecutara al pulsar el botÃ³n de Renovar Certificado
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Browsable(True), Bindable(True), _
    Description("Establece la funciÃ³n javascript que se ejecutara al pulsar Renovar Certificado"), Themeable(True), _
    Category("Action")> _
    Public Property OnClientClickRenovarCertificado() As String
        Get
            EnsureChildControls()
            Return _onClickRenovarCertificado
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            _onClickRenovarCertificado = value
            lnkRenovarCertificado.Attributes.Add("onclick", _onClickRenovarCertificado)
        End Set
    End Property
    Public Event RenovarCertificado_Click(ByVal Sender As Object, ByVal E As EventArgs)
    ''' <summary>
    ''' Nombre de la rutina que se ejecutara en servidor al hacer click en el botÃ³n Renovar Certificado
    ''' </summary>
    ''' <param name="E"></param>
    ''' <remarks></remarks>
    Protected Sub RenovarCertificado_OnClick(ByVal E As EventArgs)
        RaiseEvent RenovarCertificado_Click(Me, E)
    End Sub
    Private Sub RenovarCertificado_OnClick(ByVal Sender As Object, ByVal E As EventArgs)
        RenovarCertificado_OnClick(EventArgs.Empty)
    End Sub
    <Browsable(True), Bindable(True), _
    Description("Establece el texto que aparecera en botÃ³n Renovar Certificado"), Themeable(True), _
    Category("Appearance")> _
        Public Property TextoBotonRenovarCertificado() As String
        Get
            EnsureChildControls()
            Return lnkRenovarCertificado.Text
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            lnkRenovarCertificado.Text = value
        End Set
    End Property
    <Browsable(True), Bindable(True), _
    Description("Establece si el botÃ³n Renovar Certificado serÃ¡ visible"), Themeable(True), _
    Category("Appearance"), DefaultValue(False)> _
        Public Property VisibleBotonRenovarCertificado() As Boolean
        Get
            Return _visibleBotonRenovarCertificado
        End Get
        Set(ByVal value As Boolean)
            _visibleBotonRenovarCertificado = value
        End Set
    End Property
#End Region
#Region "Importar"
    Private lnkImportar As LinkButton
    Private _visibleBotonImportar As Boolean
    Private _onClickImportar As String
    ''' <summary>
    ''' Funcion javascript que se ejecutara al pulsar el botÃ³n de calcular
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Browsable(True), Bindable(True), _
    Description("Establece la funciÃ³n javascript que se ejecutara al pulsar Importar"), Themeable(True), _
    Category("Action")> _
    Public Property OnClientClickImportar() As String
        Get
            EnsureChildControls()
            Return _onClickImportar
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            _onClickImportar = value
            lnkImportar.Attributes.Add("onclick", _onClickImportar)
        End Set
    End Property
    Public Event Importar_Click(ByVal Sender As Object, ByVal E As EventArgs)
    ''' <summary>
    ''' Nombre de la rutina que se ejecutara en servidor al hacer click en el botÃ³n calcular
    ''' </summary>
    ''' <param name="E"></param>
    ''' <remarks></remarks>
    Protected Sub Importar_OnClick(ByVal E As EventArgs)
        RaiseEvent Importar_Click(Me, E)
    End Sub
    Private Sub Importar_OnClick(ByVal Sender As Object, ByVal E As EventArgs)
        Importar_OnClick(EventArgs.Empty)
    End Sub
    <Browsable(True), Bindable(True), _
    Description("Establece el texto que aparecera en botÃ³n Importar"), Themeable(True), _
    Category("Appearance")> _
        Public Property TextoBotonImportar() As String
        Get
            EnsureChildControls()
            Return lnkImportar.Text
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            lnkImportar.Text = value
        End Set
    End Property
    <Browsable(True), Bindable(True), _
    Description("Establece si el botÃ³n volver serÃ¡ visible"), Themeable(True), _
    Category("Appearance"), DefaultValue(False)> _
        Public Property VisibleBotonImportar() As Boolean
        Get
            Return _visibleBotonImportar
        End Get
        Set(ByVal value As Boolean)
            _visibleBotonImportar = value
        End Set
    End Property
#End Region
#Region "Calcular"
    Private lnkCalcular As LinkButton
    Private _visibleBotonCalcular As Boolean
    Private _onClickCalcular As String
    ''' <summary>
    ''' Funcion javascript que se ejecutara al pulsar el botÃ³n de calcular
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Browsable(True), Bindable(True), _
    Description("Establece la funciÃ³n javascript que se ejecutara al pulsar Volver"), Themeable(True), _
    Category("Action")> _
    Public Property OnClientClickCalcular() As String
        Get
            EnsureChildControls()
            Return _onClickCalcular
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            _onClickCalcular = value
            lnkCalcular.Attributes.Add("onclick", _onClickCalcular)
        End Set
    End Property
    Public Event Calcular_Click(ByVal Sender As Object, ByVal E As EventArgs)
    ''' <summary>
    ''' Nombre de la rutina que se ejecutara en servidor al hacer click en el botÃ³n calcular
    ''' </summary>
    ''' <param name="E"></param>
    ''' <remarks></remarks>
    Protected Sub Calcular_OnClick(ByVal E As EventArgs)
        RaiseEvent Calcular_Click(Me, E)
    End Sub
    Private Sub Calcular_OnClick(ByVal Sender As Object, ByVal E As EventArgs)
        Calcular_OnClick(EventArgs.Empty)
    End Sub
    <Browsable(True), Bindable(True), _
    Description("Establece el texto que aparecera en botÃ³n volver"), Themeable(True), _
    Category("Appearance")> _
        Public Property TextoBotonCalcular() As String
        Get
            EnsureChildControls()
            Return lnkCalcular.Text
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            lnkCalcular.Text = value
        End Set
    End Property
    <Browsable(True), Bindable(True), _
    Description("Establece si el botÃ³n volver serÃ¡ visible"), Themeable(True), _
    Category("Appearance"), DefaultValue(False)> _
        Public Property VisibleBotonCalcular() As Boolean
        Get
            Return _visibleBotonCalcular
        End Get
        Set(ByVal value As Boolean)
            _visibleBotonCalcular = value
        End Set
    End Property
#End Region
#Region "Mail"
    Private lnkMail As LinkButton
    Private _visibleBotonMail As Boolean
    Private _onClickMail As String
    ''' <summary>
    ''' Funcion javascript que se ejecutara al pulsar el botÃ³n de Enviar Mail
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Browsable(True), Bindable(True), _
    Description("Establece la funciÃ³n javascript que se ejecutara al pulsar Volver"), Themeable(True), _
    Category("Action")> _
    Public Property OnClientClickMail() As String
        Get
            EnsureChildControls()
            Return _onClickMail
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            _onClickMail = value
            lnkMail.Attributes.Add("onclick", _onClickMail)
        End Set
    End Property
    Public Event Mail_Click(ByVal Sender As Object, ByVal E As EventArgs)
    ''' <summary>
    ''' Nombre de la rutina que se ejecutara en servidor al hacer click en el botÃ³n Enviar mail
    ''' </summary>
    ''' <param name="E"></param>
    ''' <remarks></remarks>
    Protected Sub Mail_OnClick(ByVal E As EventArgs)
        RaiseEvent Mail_Click(Me, E)
    End Sub
    Private Sub Mail_OnClick(ByVal Sender As Object, ByVal E As EventArgs)
        Mail_OnClick(EventArgs.Empty)
    End Sub
    <Browsable(True), Bindable(True), _
    Description("Establece el texto que aparecera en botÃ³n volver"), Themeable(True), _
    Category("Appearance")> _
        Public Property TextoBotonMail() As String
        Get
            EnsureChildControls()
            Return lnkMail.Text
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            lnkMail.Text = value
        End Set
    End Property
    <Browsable(True), Bindable(True), _
    Description("Establece si el botÃ³n volver serÃ¡ visible"), Themeable(True), _
    Category("Appearance"), DefaultValue(False)> _
        Public Property VisibleBotonMail() As Boolean
        Get
            Return _visibleBotonMail
        End Get
        Set(ByVal value As Boolean)
            _visibleBotonMail = value
        End Set
    End Property
#End Region
#Region "ImpExp"
    Private lnkImpExp As LinkButton
    Private _visibleBotonImpExp As Boolean
    Private _onClickImpExp As String
    Private _ocultarBotonImpExp As Boolean
    ''' <summary>
    ''' Funcion javascript que se ejecutara al pulsar el botÃ³n de imprimir/exportar
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Browsable(True), Bindable(True), _
Description("Establece la funciÃ³n javascript que se ejecutara al pulsar Volver"), Themeable(True), _
Category("Action")> _
    Public Property OnClientClickImpExp() As String
        Get
            EnsureChildControls()
            Return _onClickImpExp
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            _onClickImpExp = value
            lnkImpExp.Attributes.Add("onclick", _onClickImpExp)
        End Set
    End Property
    <Browsable(True), Bindable(True), _
Description("Establece si el boton Impr./Exp sera visible"), Themeable(True), _
Category("Appearance"), DefaultValue(False)> _
        Public Property OcultarBotonImpExp() As Boolean
        Get
            Return _ocultarBotonImpExp
        End Get
        Set(ByVal value As Boolean)
            _ocultarBotonImpExp = value
        End Set
    End Property
    Public Event ImpExp_Click(ByVal Sender As Object, ByVal E As EventArgs)
    ''' <summary>
    ''' Nombre de la rutina que se ejecutara en servidor al hacer click en el botÃ³n imprimir/exportar
    ''' </summary>
    ''' <param name="E"></param>
    ''' <remarks></remarks>
    Protected Sub ImpExp_OnClick(ByVal E As EventArgs)
        RaiseEvent ImpExp_Click(Me, E)
    End Sub
    Private Sub ImpExp_OnClick(ByVal Sender As Object, ByVal E As EventArgs)
        ImpExp_OnClick(EventArgs.Empty)
    End Sub
    <Browsable(True), Bindable(True), _
    Description("Establece el texto que aparecera en botÃ³n volver"), Themeable(True), _
    Category("Appearance")> _
        Public Property TextoBotonImpExp() As String
        Get
            EnsureChildControls()
            Return lnkImpExp.Text
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            lnkImpExp.Text = value
        End Set
    End Property
    <Browsable(True), Bindable(True), _
    Description("Establece si el botÃ³n volver serÃ¡ visible"), Themeable(True), _
    Category("Appearance"), DefaultValue(False)> _
        Public Property VisibleBotonImpExp() As Boolean
        Get
            Return _visibleBotonImpExp
        End Get
        Set(ByVal value As Boolean)
            _visibleBotonImpExp = value
        End Set
    End Property
#End Region
#Region "Emitir"
    Private lnkEmitir As LinkButton
    Private _visibleBotonEmitir As Boolean
    Private _onClickEmitir As String
    Private _ocultarBotonEmitir As Boolean
    ''' <summary>
    ''' Funcion javascript que se ejecutara al pulsar el botÃ³n de emitir
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Browsable(True), Bindable(True), _
Description("Establece la funciÃ³n javascript que se ejecutara al pulsar Volver"), Themeable(True), _
Category("Action")> _
    Public Property OnClientClickEmitir() As String
        Get
            EnsureChildControls()
            Return _onClickEmitir
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            _onClickEmitir = value
            lnkEmitir.Attributes.Add("onclick", _onClickEmitir)
        End Set
    End Property
    Public Event Emitir_Click(ByVal Sender As Object, ByVal E As EventArgs)
    ''' <summary>
    ''' Nombre de la rutina que se ejecutara en servidor al hacer click en el botÃ³n emitir
    ''' </summary>
    ''' <param name="E"></param>
    ''' <remarks></remarks>
    Protected Sub Emitir_OnClick(ByVal E As EventArgs)
        RaiseEvent Emitir_Click(Me, E)
    End Sub
    Private Sub Emitir_OnClick(ByVal Sender As Object, ByVal E As EventArgs)
        Emitir_OnClick(EventArgs.Empty)
    End Sub
    <Browsable(True), Bindable(True), _
    Description("Establece el texto que aparecera en botÃ³n volver"), Themeable(True), _
    Category("Appearance")> _
        Public Property TextoBotonEmitir() As String
        Get
            EnsureChildControls()
            Return lnkEmitir.Text
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            lnkEmitir.Text = value
        End Set
    End Property
    <Browsable(True), Bindable(True), _
    Description("Establece si el botÃ³n volver serÃ¡ visible"), Themeable(True), _
    Category("Appearance"), DefaultValue(False)> _
        Public Property VisibleBotonEmitir() As Boolean
        Get
            Return _visibleBotonEmitir
        End Get
        Set(ByVal value As Boolean)
            _visibleBotonEmitir = value
        End Set
    End Property
    <Browsable(True), Bindable(True), _
    Description("Establece si el boton Emitir sera visible"), Themeable(True), _
    Category("Appearance"), DefaultValue(False)> _
    Public Property OcultarBotonEmitir() As Boolean
        Get
            Return _ocultarBotonEmitir
        End Get
        Set(ByVal value As Boolean)
            _ocultarBotonEmitir = value
        End Set
    End Property
#End Region
#Region "Borrar"
    Private lnkBorrar As LinkButton
    Private _visibleBotonBorrar As Boolean
    Private _onClickBorrar As String
    Private _ocultarBotonBorrar As Boolean
    ''' <summary>Funcion javascript que se ejecutara al pulsar el botÃ³n de Borrar</summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Browsable(True), Bindable(True),
    Description("Establece la funciÃ³n javascript que se ejecutara al pulsar Borrar"), Themeable(True),
    Category("Action")>
    Public Property OnClientClickBorrar() As String
        Get
            EnsureChildControls()
            Return _onClickBorrar
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            _onClickBorrar = value
            lnkBorrar.Attributes.Add("onclick", _onClickBorrar)
        End Set
    End Property
    Public Event Borrar_Click(ByVal Sender As Object, ByVal E As EventArgs)
    ''' <summary>Nombre de la rutina que se ejecutara en servidor al hacer click en el botÃ³n Borrar</summary>
    ''' <param name="E"></param>
    ''' <remarks></remarks>
    Protected Sub Borrar_OnClick(ByVal E As EventArgs)
        RaiseEvent Borrar_Click(Me, E)
    End Sub
    Private Sub Borrar_OnClick(ByVal Sender As Object, ByVal E As EventArgs)
        Borrar_OnClick(EventArgs.Empty)
    End Sub
    <Browsable(True), Bindable(True),
    Description("Establece el texto que aparecera en botÃ³n Borrar"), Themeable(True),
    Category("Appearance")>
    Public Property TextoBotonBorrar() As String
        Get
            EnsureChildControls()
            Return lnkBorrar.Text
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            lnkBorrar.Text = value
        End Set
    End Property
    <Browsable(True), Bindable(True),
    Description("Establece si el botÃ³n volver serÃ¡ visible"), Themeable(True),
    Category("Appearance"), DefaultValue(False)>
    Public Property VisibleBotonBorrar() As Boolean
        Get
            Return _visibleBotonBorrar
        End Get
        Set(ByVal value As Boolean)
            _visibleBotonBorrar = value
        End Set
    End Property
    <Browsable(True), Bindable(True),
Description("Establece si el botÃƒÂ³n Borrar serÃƒÂ¡ visible"), Themeable(True),
Category("Appearance"), DefaultValue(False)>
    Public Property OcultarBotonBorrar() As Boolean
        Get
            Return _ocultarBotonBorrar
        End Get
        Set(ByVal value As Boolean)
            _ocultarBotonBorrar = value
        End Set
    End Property
#End Region
#Region "Deshacer Borrar"
    Private lnkDeshacerBorrar As LinkButton
    Private _visibleBotonDeshacerBorrar As Boolean
    Private _onClickDeshacerBorrar As String
    Private _ocultarBotonDeshacerBorrar As Boolean
    ''' <summary>Funcion javascript que se ejecutara al pulsar el botÃ³n de Borrar</summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Browsable(True), Bindable(True),
    Description("Establece la funciÃ³n javascript que se ejecutara al pulsar Deshacer Borrar"), Themeable(True),
    Category("Action")>
    Public Property OnClientClickDeshacerBorrar() As String
        Get
            EnsureChildControls()
            Return _onClickDeshacerBorrar
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            _onClickDeshacerBorrar = value
            lnkDeshacerBorrar.Attributes.Add("onclick", _onClickDeshacerBorrar)
        End Set
    End Property
    Public Event DeshacerBorrar_Click(ByVal Sender As Object, ByVal E As EventArgs)
    ''' <summary>Nombre de la rutina que se ejecutara en servidor al hacer click en el botÃ³n Deshacer Borrar</summary>
    ''' <param name="E"></param>
    ''' <remarks></remarks>
    Protected Sub DeshacerBorrar_OnClick(ByVal E As EventArgs)
        RaiseEvent DeshacerBorrar_Click(Me, E)
    End Sub
    Private Sub DeshacerBorrar_OnClick(ByVal Sender As Object, ByVal E As EventArgs)
        DeshacerBorrar_OnClick(EventArgs.Empty)
    End Sub
    <Browsable(True), Bindable(True),
    Description("Establece el texto que aparecera en botÃ³n Deshacer Borrar"), Themeable(True),
    Category("Appearance")>
    Public Property TextoBotonDeshacerBorrar() As String
        Get
            EnsureChildControls()
            Return lnkDeshacerBorrar.Text
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            lnkDeshacerBorrar.Text = value
        End Set
    End Property
    <Browsable(True), Bindable(True),
    Description("Establece si el botÃ³n volver serÃ¡ visible"), Themeable(True),
    Category("Appearance"), DefaultValue(False)>
    Public Property VisibleBotonDeshacerBorrar() As Boolean
        Get
            Return _visibleBotonDeshacerBorrar
        End Get
        Set(ByVal value As Boolean)
            _visibleBotonDeshacerBorrar = value
        End Set
    End Property
    <Browsable(True), Bindable(True),
Description("Establece si el botÃƒÂ³n Deshacer Borrar serÃƒÂ¡ visible"), Themeable(True),
Category("Appearance"), DefaultValue(False)>
    Public Property OcultarBotonDeshacerBorrar() As Boolean
        Get
            Return _ocultarBotonDeshacerBorrar
        End Get
        Set(ByVal value As Boolean)
            _ocultarBotonDeshacerBorrar = value
        End Set
    End Property
#End Region
#Region "Eliminar"
    Private lnkEliminar As LinkButton
    Private _visibleBotonEliminar As Boolean
    Private _onClickEliminar As String
    Private _ocultarBotonEliminar As Boolean
    ''' <summary>
    ''' Funcion javascript que se ejecutara al pulsar el botÃ³n de eliminar
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Browsable(True), Bindable(True), _
    Description("Establece la funciÃ³n javascript que se ejecutara al pulsar Volver"), Themeable(True), _
    Category("Action")> _
    Public Property OnClientClickEliminar() As String
        Get
            EnsureChildControls()
            Return _onClickEliminar
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            _onClickEliminar = value
            lnkEliminar.Attributes.Add("onclick", _onClickEliminar)
        End Set
    End Property
    Public Event Eliminar_Click(ByVal Sender As Object, ByVal E As EventArgs)
    ''' <summary>
    ''' Nombre de la rutina que se ejecutara en servidor al hacer click en el botÃ³n eliminar
    ''' </summary>
    ''' <param name="E"></param>
    ''' <remarks></remarks>
    Protected Sub Eliminar_OnClick(ByVal E As EventArgs)
        RaiseEvent Eliminar_Click(Me, E)
    End Sub
    Private Sub Eliminar_OnClick(ByVal Sender As Object, ByVal E As EventArgs)
        Eliminar_OnClick(EventArgs.Empty)
    End Sub
    <Browsable(True), Bindable(True), _
    Description("Establece el texto que aparecera en botÃ³n volver"), Themeable(True), _
    Category("Appearance")> _
        Public Property TextoBotonEliminar() As String
        Get
            EnsureChildControls()
            Return lnkEliminar.Text
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            lnkEliminar.Text = value
        End Set
    End Property
    <Browsable(True), Bindable(True), _
    Description("Establece si el botÃ³n volver serÃ¡ visible"), Themeable(True), _
    Category("Appearance"), DefaultValue(False)> _
        Public Property VisibleBotonEliminar() As Boolean
        Get
            Return _visibleBotonEliminar
        End Get
        Set(ByVal value As Boolean)
            _visibleBotonEliminar = value
        End Set
    End Property
    <Browsable(True), Bindable(True), _
Description("Establece si el botÃƒÂ³n volver serÃƒÂ¡ visible"), Themeable(True), _
Category("Appearance"), DefaultValue(False)> _
        Public Property OcultarBotonEliminar() As Boolean
        Get
            Return _ocultarBotonEliminar
        End Get
        Set(ByVal value As Boolean)
            _ocultarBotonEliminar = value
        End Set
    End Property
#End Region
#Region "Guardar"
    Private lnkGuardar As LinkButton
    Private _visibleBotonGuardar As Boolean
    Private _onClickGuardar As String
    ''' <summary>
    ''' Funcion javascript que se ejecutara al pulsar el botÃ³n de guardar
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Browsable(True), Bindable(True), _
    Description("Establece la funciÃ³n javascript que se ejecutara al pulsar Volver"), Themeable(True), _
    Category("Action")> _
    Public Property OnClientClickGuardar() As String
        Get
            EnsureChildControls()
            Return _onClickGuardar
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            _onClickGuardar = value
            lnkGuardar.Attributes.Add("onclick", _onClickGuardar)
        End Set
    End Property
    Public Event Guardar_Click(ByVal Sender As Object, ByVal E As EventArgs)
    ''' <summary>
    ''' Nombre de la rutina que se ejecutara en servidor al hacer click en el botÃ³n guardar
    ''' </summary>
    ''' <param name="E"></param>
    ''' <remarks></remarks>
    Protected Sub Guardar_OnClick(ByVal E As EventArgs)
        RaiseEvent Guardar_Click(Me, E)
    End Sub
    Private Sub Guardar_OnClick(ByVal Sender As Object, ByVal E As EventArgs)
        Guardar_OnClick(EventArgs.Empty)
    End Sub
    <Browsable(True), Bindable(True), _
    Description("Establece el texto que aparecera en botÃ³n volver"), Themeable(True), _
    Category("Appearance")> _
        Public Property TextoBotonGuardar() As String
        Get
            EnsureChildControls()
            Return lnkGuardar.Text
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            lnkGuardar.Text = value
        End Set
    End Property
    <Browsable(True), Bindable(True), _
    Description("Establece si el botÃ³n volver serÃ¡ visible"), Themeable(True), _
    Category("Appearance"), DefaultValue(False)> _
        Public Property VisibleBotonGuardar() As Boolean
        Get
            Return _visibleBotonGuardar
        End Get
        Set(ByVal value As Boolean)
            _visibleBotonGuardar = value
        End Set
    End Property
#End Region
#Region "Cierre Positivo"
    Private lnkCierrePositivo As LinkButton
    Private _visibleBotonCierrePositivo As Boolean
    Private _onClickCierrePositivo As String
    Private _ocultarBotonCierrePositivo As Boolean
    ''' <summary>
    ''' Funcion javascript que se ejecutara al pulsar el botÃ³n de cierre positivo
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Browsable(True), Bindable(True), _
    Description("Establece la funciÃ³n javascript que se ejecutara al pulsar Volver"), Themeable(True), _
    Category("Action")> _
    Public Property OnClientClickCierrePositivo() As String
        Get
            EnsureChildControls()
            Return _onClickCierrePositivo
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            _onClickCierrePositivo = value
            lnkCierrePositivo.Attributes.Add("onclick", _onClickCierrePositivo)
        End Set
    End Property
    Public Event CierrePositivo_Click(ByVal Sender As Object, ByVal E As EventArgs)
    ''' <summary>
    ''' Nombre de la rutina que se ejecutara en servidor al hacer click en el botÃ³n cierre positivo
    ''' </summary>
    ''' <param name="E"></param>
    ''' <remarks></remarks>
    Protected Sub CierrePositivo_OnClick(ByVal E As EventArgs)
        RaiseEvent CierrePositivo_Click(Me, E)
    End Sub
    Private Sub CierrePositivo_OnClick(ByVal Sender As Object, ByVal E As EventArgs)
        CierrePositivo_OnClick(EventArgs.Empty)
    End Sub
    <Browsable(True), Bindable(True), _
    Description("Establece el texto que aparecera en botÃ³n volver"), Themeable(True), _
    Category("Appearance")> _
        Public Property TextoBotonCierrePositivo() As String
        Get
            EnsureChildControls()
            Return lnkCierrePositivo.Text
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            lnkCierrePositivo.Text = value
        End Set
    End Property
    <Browsable(True), Bindable(True), _
    Description("Establece si el botÃ³n volver serÃ¡ visible"), Themeable(True), _
    Category("Appearance"), DefaultValue(False)> _
        Public Property VisibleBotonCierrePositivo() As Boolean
        Get
            Return _visibleBotonCierrePositivo
        End Get
        Set(ByVal value As Boolean)
            _visibleBotonCierrePositivo = value
        End Set
    End Property
    <Browsable(True), Bindable(True), _
    Description("Establece si el botÃ³n volver serÃ¡ visible"), Themeable(True), _
    Category("Appearance"), DefaultValue(False)> _
        Public Property OcultarBotonCierrePositivo() As Boolean
        Get
            Return _ocultarBotonCierrePositivo
        End Get
        Set(ByVal value As Boolean)
            _ocultarBotonCierrePositivo = value
        End Set
    End Property
#End Region
#Region "Cierre Negativo"
    Private lnkCierreNegativo As LinkButton
    Private _visibleBotonCierreNegativo As Boolean
    Private _onClickCierreNegativo As String
    Private _ocultarBotonCierreNegativo As Boolean
    ''' <summary>
    ''' Funcion javascript que se ejecutara al pulsar el botÃ³n de cierre negativo
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Browsable(True), Bindable(True), _
    Description("Establece la funciÃ³n javascript que se ejecutara al pulsar Volver"), Themeable(True), _
    Category("Action")> _
    Public Property OnClientClickCierreNegativo() As String
        Get
            EnsureChildControls()
            Return _onClickCierreNegativo
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            _onClickCierreNegativo = value
            lnkCierreNegativo.Attributes.Add("onclick", _onClickCierreNegativo)
        End Set
    End Property
    Public Event CierreNegativo_Click(ByVal Sender As Object, ByVal E As EventArgs)
    ''' <summary>
    ''' Nombre de la rutina que se ejecutara en servidor al hacer click en el botÃ³n cierre negativo
    ''' </summary>
    ''' <param name="E"></param>
    ''' <remarks></remarks>
    Protected Sub CierreNegativo_OnClick(ByVal E As EventArgs)
        RaiseEvent CierreNegativo_Click(Me, E)
    End Sub
    Private Sub CierreNegativo_OnClick(ByVal Sender As Object, ByVal E As EventArgs)
        CierreNegativo_OnClick(EventArgs.Empty)
    End Sub
    <Browsable(True), Bindable(True), _
    Description("Establece el texto que aparecera en botÃ³n volver"), Themeable(True), _
    Category("Appearance")> _
        Public Property TextoBotonCierreNegativo() As String
        Get
            EnsureChildControls()
            Return lnkCierreNegativo.Text
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            lnkCierreNegativo.Text = value
        End Set
    End Property
    <Browsable(True), Bindable(True), _
    Description("Establece si el botÃ³n volver serÃ¡ visible"), Themeable(True), _
    Category("Appearance"), DefaultValue(False)> _
        Public Property VisibleBotonCierreNegativo() As Boolean
        Get
            Return _visibleBotonCierreNegativo
        End Get
        Set(ByVal value As Boolean)
            _visibleBotonCierreNegativo = value
        End Set
    End Property
    <Browsable(True), Bindable(True), _
    Description("Establece si el botÃ³n volver serÃ¡ visible"), Themeable(True), _
    Category("Appearance"), DefaultValue(False)> _
        Public Property OcultarBotonCierreNegativo() As Boolean
        Get
            Return _ocultarBotonCierreNegativo
        End Get
        Set(ByVal value As Boolean)
            _ocultarBotonCierreNegativo = value
        End Set
    End Property
#End Region
#Region "Reabrir"
    Private lnkReabrir As LinkButton
    Private _visibleBotonReabrir As Boolean
    Private _onClickReabrir As String
    Private _ocultarBotonReabrir As Boolean
    ''' <summary>
    ''' Funcion javascript que se ejecutara al pulsar el botÃ³n de reabrir
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Browsable(True), Bindable(True), _
    Description("Establece la funciÃ³n javascript que se ejecutara al pulsar Volver"), Themeable(True), _
    Category("Action")> _
    Public Property OnClientClickReabrir() As String
        Get
            EnsureChildControls()
            Return _onClickReabrir
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            _onClickReabrir = value
            lnkReabrir.Attributes.Add("onclick", _onClickReabrir)
        End Set
    End Property
    <Browsable(True), Bindable(True), _
    Description("Establece si el boton Anular sera visible"), Themeable(True), _
    Category("Appearance"), DefaultValue(False)> _
    Public Property OcultarBotonReabrir() As Boolean
        Get
            Return _ocultarBotonReabrir
        End Get
        Set(ByVal value As Boolean)
            _ocultarBotonReabrir = value
        End Set
    End Property
    Public Event Reabrir_Click(ByVal Sender As Object, ByVal E As EventArgs)
    ''' <summary>
    ''' Nombre de la rutina que se ejecutara en servidor al hacer click en el botÃ³n reabrir
    ''' </summary>
    ''' <param name="E"></param>
    ''' <remarks></remarks>
    Protected Sub Reabrir_OnClick(ByVal E As EventArgs)
        RaiseEvent Reabrir_Click(Me, E)
    End Sub
    Private Sub Reabrir_OnClick(ByVal Sender As Object, ByVal E As EventArgs)
        Reabrir_OnClick(EventArgs.Empty)
    End Sub
    <Browsable(True), Bindable(True), _
    Description("Establece el texto que aparecera en botÃ³n volver"), Themeable(True), _
    Category("Appearance")> _
        Public Property TextoBotonReabrir() As String
        Get
            EnsureChildControls()
            Return lnkReabrir.Text
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            lnkReabrir.Text = value
        End Set
    End Property
    <Browsable(True), Bindable(True), _
    Description("Establece si el botÃ³n volver serÃ¡ visible"), Themeable(True), _
    Category("Appearance"), DefaultValue(False)> _
        Public Property VisibleBotonReabrir() As Boolean
        Get
            Return _visibleBotonReabrir
        End Get
        Set(ByVal value As Boolean)
            _visibleBotonReabrir = value
        End Set
    End Property
#End Region
#Region "Volver"
    Private lnkVolver As LinkButton
    Private _visibleBotonVolver As Boolean
    Private _onClickVolver As String
    ''' <summary>
    ''' Funcion javascript que se ejecutara al pulsar el botÃ³n de volver
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Browsable(True), Bindable(True), _
    Description("Establece la funciÃ³n javascript que se ejecutara al pulsar Volver"), Themeable(True), _
    Category("Action")> _
    Public Property OnClientClickVolver() As String
        Get
            EnsureChildControls()
            Return _onClickVolver
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            _onClickVolver = value
            lnkVolver.Attributes.Add("onclick", _onClickVolver)
        End Set
    End Property
    Public Event Volver_Click(ByVal Sender As Object, ByVal E As EventArgs)
    ''' <summary>
    ''' Nombre de la rutina que se ejecutara en servidor al hacer click en el botÃ³n volver
    ''' </summary>
    ''' <param name="E"></param>
    ''' <remarks></remarks>
    Protected Sub Volver_OnClick(ByVal E As EventArgs)
        RaiseEvent Volver_Click(Me, E)
    End Sub
    Private Sub Volver_OnClick(ByVal Sender As Object, ByVal E As EventArgs)
        Volver_OnClick(EventArgs.Empty)
    End Sub
    <Browsable(True), Bindable(True), _
    Description("Establece el texto que aparecera en botÃ³n volver"), Themeable(True), _
    Category("Appearance")> _
        Public Property TextoBotonVolver() As String
        Get
            EnsureChildControls()
            Return lnkVolver.Text
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            lnkVolver.Text = value
        End Set
    End Property
    <Browsable(True), Bindable(True), _
    Description("Establece si el botÃ³n volver serÃ¡ visible"), Themeable(True), _
    Category("Appearance"), DefaultValue(False)> _
        Public Property VisibleBotonVolver() As Boolean
        Get
            Return _visibleBotonVolver
        End Get
        Set(ByVal value As Boolean)
            _visibleBotonVolver = value
        End Set
    End Property
#End Region
#Region "Anular"
    Private lnkAnular As LinkButton
    Private _visibleBotonAnular As Boolean
    Private _onClickAnular As String
    Private _ocultarBotonAnular As Boolean
    ''' <summary>
    ''' Funcion javascript que se ejecutara al pulsar el botÃƒÂ³n de Anular
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Browsable(True), Bindable(True), _
    Description("Establece la funciÃƒÂ³n javascript que se ejecutara al pulsar Anular"), Themeable(True), _
    Category("Action")> _
    Public Property OnClientClickAnular() As String
        Get
            EnsureChildControls()
            Return _onClickAnular
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            _onClickAnular = value
            lnkAnular.Attributes.Add("onclick", _onClickAnular)
        End Set
    End Property
    <Browsable(True), Bindable(True), _
    Description("Establece si el boton Anular sera visible"), Themeable(True), _
    Category("Appearance"), DefaultValue(False)> _
    Public Property OcultarBotonAnular() As Boolean
        Get
            Return _ocultarBotonAnular
        End Get
        Set(ByVal value As Boolean)
            _ocultarBotonAnular = value
        End Set
    End Property
    Public Event Anular_Click(ByVal Sender As Object, ByVal E As EventArgs)
    ''' <summary>
    ''' Nombre de la rutina que se ejecutara en servidor al hacer click en el botÃƒÂ³n Anular
    ''' </summary>
    ''' <param name="E"></param>
    ''' <remarks></remarks>
    Protected Sub Anular_OnClick(ByVal E As EventArgs)
        RaiseEvent Anular_Click(Me, E)
    End Sub
    Private Sub Anular_OnClick(ByVal Sender As Object, ByVal E As EventArgs)
        Anular_OnClick(EventArgs.Empty)
    End Sub
    <Browsable(True), Bindable(True), _
    Description("Establece el texto que aparecera en botÃƒÂ³n Anular"), Themeable(True), _
    Category("Appearance")> _
        Public Property TextoBotonAnular() As String
        Get
            EnsureChildControls()
            Return lnkAnular.Text
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            lnkAnular.Text = value
        End Set
    End Property
    <Browsable(True), Bindable(True), _
    Description("Establece si el botÃƒÂ³n Anular serÃƒÂ¡ visible"), Themeable(True), _
    Category("Appearance"), DefaultValue(False)> _
        Public Property VisibleBotonAnular() As Boolean
        Get
            Return _visibleBotonAnular
        End Get
        Set(ByVal value As Boolean)
            _visibleBotonAnular = value
        End Set
    End Property
#End Region
#Region "Otras acciones"
    Private lnkAccion1 As LinkButton
    Private _visibleBotonAccion1 As Boolean
    Private _onClickAccion1 As String
    ''' <summary>
    ''' Funcion javascript que se ejecutara al pulsar el botÃƒÂ³n de Anular
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Browsable(True), Bindable(True), _
    Description("Establece la funcion javascript que se ejecutara al pulsar Otras acciones"), Themeable(True), _
    Category("Action")> _
    Public Property OnClientClickAccion1() As String
        Get
            EnsureChildControls()
            Return _onClickAccion1
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            _onClickAccion1 = value
            lnkAccion1.Attributes.Add("onclick", _onClickAccion1)
        End Set
    End Property
    Public Event Accion1_Click(ByVal Sender As Object, ByVal E As EventArgs)
    ''' <summary>
    ''' Nombre de la rutina que se ejecutara en servidor al hacer click en el botÃƒÂ³n Otras acciones
    ''' </summary>
    ''' <param name="E"></param>
    ''' <remarks></remarks>
    Protected Sub Accion1_OnClick(ByVal E As EventArgs)
        RaiseEvent Accion1_Click(Me, E)
    End Sub
    Private Sub Accion1_OnClick(ByVal Sender As Object, ByVal E As EventArgs)
        Accion1_OnClick(EventArgs.Empty)
    End Sub
    <Browsable(True), Bindable(True), _
    Description("Establece el texto que aparecera en boton Otras acciones"), Themeable(True), _
    Category("Appearance")> _
        Public Property TextoBotonAccion1() As String
        Get
            EnsureChildControls()
            Return lnkAccion1.Text
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            lnkAccion1.Text = AcortarTexto(value, 20)
            lnkAccion1.ToolTip = value
        End Set
    End Property
    <Browsable(True), Bindable(True), _
    Description("Establece si el botÃƒÂ³n Otras acciones serÃƒÂ¡ visible"), Themeable(True), _
    Category("Appearance"), DefaultValue(False)> _
        Public Property VisibleBotonAccion1() As Boolean
        Get
            Return _visibleBotonAccion1
        End Get
        Set(ByVal value As Boolean)
            _visibleBotonAccion1 = value
        End Set
    End Property
#End Region
#Region "Accion 2"
    Private lnkAccion2 As LinkButton
    Private _visibleBotonAccion2 As Boolean
    Private _onClickAccion2 As String
    ''' <summary>
    ''' Funcion javascript que se ejecutara al pulsar el botÃƒÂ³n de Anular
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Browsable(True), Bindable(True), _
    Description("Establece la funcion javascript que se ejecutara al pulsar Otras acciones"), Themeable(True), _
    Category("Action")> _
    Public Property OnClientClickAccion2() As String
        Get
            EnsureChildControls()
            Return _onClickAccion2
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            _onClickAccion2 = value
            lnkAccion2.Attributes.Add("onclick", _onClickAccion2)
        End Set
    End Property
    Public Event Accion2_Click(ByVal Sender As Object, ByVal E As EventArgs)
    ''' <summary>
    ''' Nombre de la rutina que se ejecutara en servidor al hacer click en el botÃƒÂ³n Otras acciones
    ''' </summary>
    ''' <param name="E"></param>
    ''' <remarks></remarks>
    Protected Sub Accion2_OnClick(ByVal E As EventArgs)
        RaiseEvent Accion2_Click(Me, E)
    End Sub
    Private Sub Accion2_OnClick(ByVal Sender As Object, ByVal E As EventArgs)
        Accion2_OnClick(EventArgs.Empty)
    End Sub
    <Browsable(True), Bindable(True), _
    Description("Establece el texto que aparecera en boton Otras acciones"), Themeable(True), _
    Category("Appearance")> _
        Public Property TextoBotonAccion2() As String
        Get
            EnsureChildControls()
            Return lnkAccion2.Text
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            lnkAccion2.Text = AcortarTexto(value, 20)
            lnkAccion2.ToolTip = value
        End Set
    End Property
    <Browsable(True), Bindable(True), _
    Description("Establece si el botÃƒÂ³n Otras acciones serÃƒÂ¡ visible"), Themeable(True), _
    Category("Appearance"), DefaultValue(False)> _
        Public Property VisibleBotonAccion2() As Boolean
        Get
            Return _visibleBotonAccion2
        End Get
        Set(ByVal value As Boolean)
            _visibleBotonAccion2 = value
        End Set
    End Property
#End Region
#Region "Accion 3"
    Private lnkAccion3 As LinkButton
    Private _visibleBotonAccion3 As Boolean
    Private _onClickAccion3 As String
    ''' <summary>
    ''' Funcion javascript que se ejecutara al pulsar el botÃƒÆ’Ã‚Â³n de Anular
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Browsable(True), Bindable(True), _
    Description("Establece la funcion javascript que se ejecutara al pulsar Otras acciones"), Themeable(True), _
    Category("Action")> _
    Public Property OnClientClickAccion3() As String
        Get
            EnsureChildControls()
            Return _onClickAccion3
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            _onClickAccion3 = value
            lnkAccion3.Attributes.Add("onclick", _onClickAccion3)
        End Set
    End Property
    Public Event Accion3_Click(ByVal Sender As Object, ByVal E As EventArgs)
    ''' <summary>
    ''' Nombre de la rutina que se ejecutara en servidor al hacer click en el botÃƒÆ’Ã‚Â³n Otras acciones
    ''' </summary>
    ''' <param name="E"></param>
    ''' <remarks></remarks>
    Protected Sub Accion3_OnClick(ByVal E As EventArgs)
        RaiseEvent Accion3_Click(Me, E)
    End Sub
    Private Sub Accion3_OnClick(ByVal Sender As Object, ByVal E As EventArgs)
        Accion3_OnClick(EventArgs.Empty)
    End Sub
    <Browsable(True), Bindable(True), _
    Description("Establece el texto que aparecera en boton Otras acciones"), Themeable(True), _
    Category("Appearance")> _
        Public Property TextoBotonAccion3() As String
        Get
            EnsureChildControls()
            Return lnkAccion3.Text
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            lnkAccion3.Text = AcortarTexto(value, 20)
            lnkAccion3.ToolTip = value
        End Set
    End Property
    <Browsable(True), Bindable(True), _
    Description("Establece si el botÃƒÆ’Ã‚Â³n Otras acciones serÃƒÆ’Ã‚Â¡ visible"), Themeable(True), _
    Category("Appearance"), DefaultValue(False)> _
        Public Property VisibleBotonAccion3() As Boolean
        Get
            Return _visibleBotonAccion3
        End Get
        Set(ByVal value As Boolean)
            _visibleBotonAccion3 = value
        End Set
    End Property
#End Region
#Region "Trasladar"
    Private lnkTrasladar As LinkButton
    Private _visibleBotonTrasladar As Boolean
    Private _onClickTrasladar As String
    ''' <summary>
    ''' Funcion javascript que se ejecutara al pulsar el botÃƒÂ³n de Trasladar
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Browsable(True), Bindable(True), _
    Description("Establece la funcion javascript que se ejecutara al pulsar en trasladar"), Themeable(True), _
    Category("Action")> _
    Public Property OnClientClickTrasladar() As String
        Get
            EnsureChildControls()
            Return _onClickTrasladar
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            _onClickTrasladar = value
            lnkTrasladar.Attributes.Add("onclick", _onClickTrasladar)
        End Set
    End Property
    Public Event Trasladar_Click(ByVal Sender As Object, ByVal E As EventArgs)
    ''' <summary>
    ''' Nombre de la rutina que se ejecutara en servidor al hacer click en el botÃƒÂ³n Otras acciones
    ''' </summary>
    ''' <param name="E"></param>
    ''' <remarks></remarks>
    Protected Sub Trasladar_OnClick(ByVal E As EventArgs)
        RaiseEvent Trasladar_Click(Me, E)
    End Sub
    Private Sub Trasladar_OnClick(ByVal Sender As Object, ByVal E As EventArgs)
        Trasladar_OnClick(EventArgs.Empty)
    End Sub
    <Browsable(True), Bindable(True), _
    Description("Establece el texto que aparecera en boton Trasladar"), Themeable(True), _
    Category("Appearance")> _
        Public Property TextoBotonTrasladar() As String
        Get
            EnsureChildControls()
            Return lnkTrasladar.Text
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            lnkTrasladar.Text = value
        End Set
    End Property
    <Browsable(True), Bindable(True), _
    Description("Establece si el botÃƒÂ³n Otras acciones serÃƒÂ¡ visible"), Themeable(True), _
    Category("Appearance"), DefaultValue(False)> _
        Public Property VisibleBotonTrasladar() As Boolean
        Get
            Return _visibleBotonTrasladar
        End Get
        Set(ByVal value As Boolean)
            _visibleBotonTrasladar = value
        End Set
    End Property
#End Region
#Region "Modificar"
    Private lnkModificar As LinkButton
    Private _visibleBotonModificar As Boolean
    Private _onClickModificar As String
    ''' <summary>
    ''' Funcion javascript que se ejecutara al pulsar el botÃƒÂ³n de Modificar
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Browsable(True), Bindable(True), _
    Description("Establece la funcion javascript que se ejecutara al pulsar en modificar"), Themeable(True), _
    Category("Action")> _
    Public Property OnClientClickModificar() As String
        Get
            EnsureChildControls()
            Return _onClickModificar
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            _onClickModificar = value
            lnkModificar.Attributes.Add("onclick", _onClickModificar)
        End Set
    End Property
    Public Event Modificar_Click(ByVal Sender As Object, ByVal E As EventArgs)
    ''' <summary>
    ''' Nombre de la rutina que se ejecutara en servidor al hacer click en el botÃƒÂ³n Otras acciones
    ''' </summary>
    ''' <param name="E"></param>
    ''' <remarks></remarks>
    Protected Sub Modificar_OnClick(ByVal E As EventArgs)
        RaiseEvent Modificar_Click(Me, E)
    End Sub
    Private Sub Modificar_OnClick(ByVal Sender As Object, ByVal E As EventArgs)
        Modificar_OnClick(EventArgs.Empty)
    End Sub
    <Browsable(True), Bindable(True), _
    Description("Establece el texto que aparecera en boton Modificar"), Themeable(True), _
    Category("Appearance")> _
        Public Property TextoBotonModificar() As String
        Get
            EnsureChildControls()
            Return lnkModificar.Text
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            lnkModificar.Text = value
        End Set
    End Property
    <Browsable(True), Bindable(True), _
    Description("Establece si el botÃƒÂ³n Modificar serÃƒÂ¡ visible"), Themeable(True), _
    Category("Appearance"), DefaultValue(False)> _
        Public Property VisibleBotonModificar() As Boolean
        Get
            Return _visibleBotonModificar
        End Get
        Set(ByVal value As Boolean)
            _visibleBotonModificar = value
        End Set
    End Property
#End Region
#Region "Devolver"
    Private lnkDevolver As LinkButton
    Private _visibleBotonDevolver As Boolean
    Private _onClickDevolver As String
    ''' <summary>
    ''' Funcion javascript que se ejecutara al pulsar el botÃƒÂ³n de Devolver
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Browsable(True), Bindable(True), _
    Description("Establece la funcion javascript que se ejecutara al pulsar en Devolver"), Themeable(True), _
    Category("Action")> _
    Public Property OnClientClickDevolver() As String
        Get
            EnsureChildControls()
            Return _onClickDevolver
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            _onClickDevolver = value
            lnkDevolver.Attributes.Add("onclick", _onClickDevolver)
        End Set
    End Property
    Public Event Devolver_Click(ByVal Sender As Object, ByVal E As EventArgs)
    ''' <summary>
    ''' Nombre de la rutina que se ejecutara en servidor al hacer click en el botÃƒÂ³n Otras acciones
    ''' </summary>
    ''' <param name="E"></param>
    ''' <remarks></remarks>
    Protected Sub Devolver_OnClick(ByVal E As EventArgs)
        RaiseEvent Devolver_Click(Me, E)
    End Sub
    Private Sub Devolver_OnClick(ByVal Sender As Object, ByVal E As EventArgs)
        Devolver_OnClick(EventArgs.Empty)
    End Sub
    <Browsable(True), Bindable(True), _
    Description("Establece el texto que aparecera en boton Devolver"), Themeable(True), _
    Category("Appearance")> _
        Public Property TextoBotonDevolver() As String
        Get
            EnsureChildControls()
            Return lnkDevolver.Text
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            lnkDevolver.Text = value
        End Set
    End Property
    <Browsable(True), Bindable(True), _
    Description("Establece si el botÃƒÂ³n Otras acciones serÃƒÂ¡ visible"), Themeable(True), _
    Category("Appearance"), DefaultValue(False)> _
        Public Property VisibleBotonDevolver() As Boolean
        Get
            Return _visibleBotonDevolver
        End Get
        Set(ByVal value As Boolean)
            _visibleBotonDevolver = value
        End Set
    End Property
#End Region
#Region "Listados"
    Private lnkListados As LinkButton
    Private _visibleBotonListados As Boolean
    Private _onClickListados As String
    ''' <summary>
    ''' Funcion javascript que se ejecutara al pulsar el botÃƒÂ³n de Listados
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Browsable(True), Bindable(True), _
    Description("Establece la funcion javascript que se ejecutara al pulsar en Listados"), Themeable(True), _
    Category("Action")> _
    Public Property OnClientClickListados() As String
        Get
            EnsureChildControls()
            Return _onClickListados
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            _onClickListados = value
            lnkListados.Attributes.Add("onclick", _onClickListados)
        End Set
    End Property
    Public Event Listados_Click(ByVal Sender As Object, ByVal E As EventArgs)
    ''' <summary>
    ''' Nombre de la rutina que se ejecutara en servidor al hacer click en el botÃƒÂ³n Otras acciones
    ''' </summary>
    ''' <param name="E"></param>
    ''' <remarks></remarks>
    Protected Sub Listados_OnClick(ByVal E As EventArgs)
        RaiseEvent Listados_Click(Me, E)
    End Sub
    Private Sub Listados_OnClick(ByVal Sender As Object, ByVal E As EventArgs)
        Listados_OnClick(EventArgs.Empty)
    End Sub
    <Browsable(True), Bindable(True), _
    Description("Establece el texto que aparecera en boton Listados"), Themeable(True), _
    Category("Appearance")> _
        Public Property TextoBotonListados() As String
        Get
            EnsureChildControls()
            Return lnkListados.Text
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            lnkListados.Text = value
        End Set
    End Property
    <Browsable(True), Bindable(True), _
    Description("Establece si el botÃƒÂ³n Otras acciones serÃƒÂ¡ visible"), Themeable(True), _
    Category("Appearance"), DefaultValue(False)> _
        Public Property VisibleBotonListados() As Boolean
        Get
            Return _visibleBotonListados
        End Get
        Set(ByVal value As Boolean)
            _visibleBotonListados = value
        End Set
    End Property
#End Region
#Region "Aprobar"
    Private lnkAprobar As LinkButton
    Private _visibleBotonAprobar As Boolean
    Private _onClickAprobar As String
    ''' <summary>
    ''' Funcion javascript que se ejecutara al pulsar el botÃƒÂ³n de Aprobar
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Browsable(True), Bindable(True), _
    Description("Establece la funcion javascript que se ejecutara al pulsar en Aprobar"), Themeable(True), _
    Category("Action")> _
    Public Property OnClientClickAprobar() As String
        Get
            EnsureChildControls()
            Return _onClickAprobar
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            _onClickAprobar = value
            lnkAprobar.Attributes.Add("onclick", _onClickAprobar)
        End Set
    End Property
    Public Event Aprobar_Click(ByVal Sender As Object, ByVal E As EventArgs)
    ''' <summary>
    ''' Nombre de la rutina que se ejecutara en servidor al hacer click en el botÃƒÂ³n Otras acciones
    ''' </summary>
    ''' <param name="E"></param>
    ''' <remarks></remarks>
    Protected Sub Aprobar_OnClick(ByVal E As EventArgs)
        RaiseEvent Aprobar_Click(Me, E)
    End Sub
    Private Sub Aprobar_OnClick(ByVal Sender As Object, ByVal E As EventArgs)
        Aprobar_OnClick(EventArgs.Empty)
    End Sub
    <Browsable(True), Bindable(True), _
    Description("Establece el texto que aparecera en boton Aprobar"), Themeable(True), _
    Category("Appearance")> _
        Public Property TextoBotonAprobar() As String
        Get
            EnsureChildControls()
            Return lnkAprobar.Text
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            lnkAprobar.Text = value
        End Set
    End Property
    <Browsable(True), Bindable(True), _
    Description("Establece si el botÃƒÂ³n Otras acciones serÃƒÂ¡ visible"), Themeable(True), _
    Category("Appearance"), DefaultValue(False)> _
        Public Property VisibleBotonAprobar() As Boolean
        Get
            Return _visibleBotonAprobar
        End Get
        Set(ByVal value As Boolean)
            _visibleBotonAprobar = value
        End Set
    End Property
#End Region
#Region "Rechazar"
    Private lnkRechazar As LinkButton
    Private _visibleBotonRechazar As Boolean
    Private _onClickRechazar As String
    ''' <summary>
    ''' Funcion javascript que se ejecutara al pulsar el botÃƒÂ³n de Rechazar
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Browsable(True), Bindable(True), _
    Description("Establece la funcion javascript que se ejecutara al pulsar en Rechazar"), Themeable(True), _
    Category("Action")> _
    Public Property OnClientClickRechazar() As String
        Get
            EnsureChildControls()
            Return _onClickRechazar
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            _onClickRechazar = value
            lnkRechazar.Attributes.Add("onclick", _onClickRechazar)
        End Set
    End Property
    Public Event Rechazar_Click(ByVal Sender As Object, ByVal E As EventArgs)
    ''' <summary>
    ''' Nombre de la rutina que se ejecutara en servidor al hacer click en el botÃƒÂ³n Otras acciones
    ''' </summary>
    ''' <param name="E"></param>
    ''' <remarks></remarks>
    Protected Sub Rechazar_OnClick(ByVal E As EventArgs)
        RaiseEvent Rechazar_Click(Me, E)
    End Sub
    Private Sub Rechazar_OnClick(ByVal Sender As Object, ByVal E As EventArgs)
        Rechazar_OnClick(EventArgs.Empty)
    End Sub
    <Browsable(True), Bindable(True), _
    Description("Establece el texto que aparecera en boton Rechazar"), Themeable(True), _
    Category("Appearance")> _
        Public Property TextoBotonRechazar() As String
        Get
            EnsureChildControls()
            Return lnkRechazar.Text
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            lnkRechazar.Text = value
        End Set
    End Property
    <Browsable(True), Bindable(True), _
    Description("Establece si el botÃƒÂ³n Otras acciones serÃƒÂ¡ visible"), Themeable(True), _
    Category("Appearance"), DefaultValue(False)> _
        Public Property VisibleBotonRechazar() As Boolean
        Get
            Return _visibleBotonRechazar
        End Get
        Set(ByVal value As Boolean)
            _visibleBotonRechazar = value
        End Set
    End Property
#End Region
#Region "Volver Colaboracion"
    Private lnkVolverColaboracion As LinkButton
    Private _visibleBotonVolverColaboracion As Boolean
    Private _onClickVolverColaboracion As String
    ''' <summary>
    ''' Funcion javascript que se ejecutara al pulsar el botÃ³n de Volver Colaboracion
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Browsable(True), Bindable(True), _
    Description("Establece la funciÃ³n javascript que se ejecutara al pulsar Volver Colaboracion"), Themeable(True), _
    Category("Action")> _
    Public Property OnClientClickVolverColaboracion() As String
        Get
            EnsureChildControls()
            Return _onClickVolverColaboracion
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            _onClickVolverColaboracion = value
            lnkVolverColaboracion.Attributes.Add("onclick", _onClickVolverColaboracion)
        End Set
    End Property
    Public Event VolverColaboracion_Click(ByVal Sender As Object, ByVal E As EventArgs)
    ''' <summary>
    ''' Nombre de la rutina que se ejecutara en servidor al hacer click en el botÃƒÂ³n Volver ColaboraciÃ³n
    ''' </summary>
    ''' <param name="E"></param>
    ''' <remarks></remarks>
    Protected Sub VolverColaboracion_OnClick(ByVal E As EventArgs)
        RaiseEvent VolverColaboracion_Click(Me, E)
    End Sub
    Private Sub VolverColaboracion_OnClick(ByVal Sender As Object, ByVal E As EventArgs)
        VolverColaboracion_OnClick(EventArgs.Empty)
    End Sub
    <Browsable(True), Bindable(True), _
    Description("Establece el texto que aparecera en botÃƒÂ³n Volver Colaboracion"), Themeable(True), _
    Category("Appearance")> _
        Public Property TextoBotonVolverColaboracion() As String
        Get
            EnsureChildControls()
            Return lnkVolverColaboracion.Text
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            lnkVolverColaboracion.Text = value
        End Set
    End Property
    <Browsable(True), Bindable(True), _
    Description("Establece si el botÃƒÂ³n Volver Colaboracion serÃƒÂ¡ visible"), Themeable(True), _
    Category("Appearance"), DefaultValue(False)> _
        Public Property VisibleBotonVolverColaboracion() As Boolean
        Get
            Return _visibleBotonVolverColaboracion
        End Get
        Set(ByVal value As Boolean)
            _visibleBotonVolverColaboracion = value
        End Set
    End Property
#End Region
#Region "Enviar a favoritos"
    Private lnkEnviarAFavoritos As LinkButton
    Private _visibleBotonEnviarAFavoritos As Boolean
    Private _onClickEnviarAFavoritos As String
    ''' <summary>
    ''' Funcion javascript que se ejecutara al pulsar el botÃ³n de Volver Colaboracion
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Browsable(True), Bindable(True), _
    Description("Establece la funciÃ³n javascript que se ejecutara al pulsar Enviar a favoritos"), Themeable(True), _
    Category("Action")> _
    Public Property OnClientClickEnviarFavoritos() As String
        Get
            EnsureChildControls()
            Return _onClickEnviarAFavoritos
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            _onClickEnviarAFavoritos = value
            lnkEnviarAFavoritos.Attributes.Add("onclick", _onClickEnviarAFavoritos)
        End Set
    End Property
    Public Event EnviarAFavoritos_Click(ByVal Sender As Object, ByVal E As EventArgs)
    ''' <summary>
    ''' Nombre de la rutina que se ejecutara en servidor al hacer click en el botÃƒÂ³n Enviar a favoritos
    ''' </summary>
    ''' <param name="E"></param>
    ''' <remarks></remarks>
    Protected Sub EnviarAFavoritos_OnClick(ByVal E As EventArgs)
        RaiseEvent EnviarAFavoritos_Click(Me, E)
    End Sub
    Private Sub EnviarAFavoritos_OnClick(ByVal Sender As Object, ByVal E As EventArgs)
        EnviarAFavoritos_OnClick(EventArgs.Empty)
    End Sub
    <Browsable(True), Bindable(True), _
    Description("Establece el texto que aparecera en botÃƒÂ³n Enviar a favoritos"), Themeable(True), _
    Category("Appearance")> _
        Public Property TextoBotonEnviarAFavoritos() As String
        Get
            EnsureChildControls()
            Return lnkEnviarAFavoritos.Text
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            lnkEnviarAFavoritos.Text = value
        End Set
    End Property
    <Browsable(True), Bindable(True), _
    Description("Establece si el botÃƒÂ³n Enviar a favoritos serÃƒÂ¡ visible"), Themeable(True), _
    Category("Appearance"), DefaultValue(False)> _
        Public Property VisibleBotonEnviarAFavoritos() As Boolean
        Get
            Return _visibleBotonEnviarAFavoritos
        End Get
        Set(ByVal value As Boolean)
            _visibleBotonEnviarAFavoritos = value
        End Set
    End Property
#End Region
#Region "E-Factura PDF"
    Private lnkEFacturaPDF As LinkButton
    Private _visibleBotonEFacturaPDF As Boolean
    Private _onClickEFacturaPDF As String
    ''' <summary>
    ''' Funcion javascript que se ejecutara al pulsar el botón de e-factura (PDF)
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Browsable(True), Bindable(True), _
    Description("Establece la funciÃ³n javascript que se ejecutara al pulsar e-factura (PDF)"), Themeable(True), _
    Category("Action")> _
    Public Property OnClientClickEFacturaPDF() As String
        Get
            EnsureChildControls()
            Return _onClickEFacturaPDF
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            _onClickEFacturaPDF = value
            lnkEFacturaPDF.Attributes.Add("onclick", _onClickEFacturaPDF)
        End Set
    End Property
    Public Event EFacturaPDF_Click(ByVal Sender As Object, ByVal E As EventArgs)
    ''' <summary>
    ''' Nombre de la rutina que se ejecutara en servidor al hacer click en el botÃƒÂ³n e-factura (PDF)
    ''' </summary>
    ''' <param name="E"></param>
    ''' <remarks></remarks>
    Protected Sub EFacturaPDF_OnClick(ByVal E As EventArgs)
        RaiseEvent EFacturaPDF_Click(Me, E)
    End Sub
    Private Sub EFacturaPDF_OnClick(ByVal Sender As Object, ByVal E As EventArgs)
        EFacturaPDF_OnClick(EventArgs.Empty)
    End Sub
    <Browsable(True), Bindable(True), _
    Description("Establece el texto que aparecera en botÃƒÂ³n e-factura (PDF)"), Themeable(True), _
    Category("Appearance")> _
        Public Property TextoBotonEFacturaPDF() As String
        Get
            EnsureChildControls()
            Return lnkEFacturaPDF.Text
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            lnkEFacturaPDF.Text = value
        End Set
    End Property
    <Browsable(True), Bindable(True), _
    Description("Establece si el botÃƒÂ³n e-factura (PDF) serÃƒÂ¡ visible"), Themeable(True), _
    Category("Appearance"), DefaultValue(False)> _
        Public Property VisibleBotonEFacturaPDF() As Boolean
        Get
            Return _visibleBotonEFacturaPDF
        End Get
        Set(ByVal value As Boolean)
            _visibleBotonEFacturaPDF = value
        End Set
    End Property
#End Region
#Region "E-Factura XML"
    Private lnkEFacturaXML As LinkButton
    Private _visibleBotonEFacturaXML As Boolean
    Private _onClickEFacturaXML As String
    ''' <summary>
    ''' Funcion javascript que se ejecutara al pulsar el botón de e-factura (XML)
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Browsable(True), Bindable(True), _
    Description("Establece la funciÃ³n javascript que se ejecutara al pulsar e-factura (XML)"), Themeable(True), _
    Category("Action")> _
    Public Property OnClientClickEFacturaXML() As String
        Get
            EnsureChildControls()
            Return _onClickEFacturaXML
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            _onClickEFacturaXML = value
            lnkEFacturaXML.Attributes.Add("onclick", _onClickEFacturaXML)
        End Set
    End Property
    Public Event EFacturaXML_Click(ByVal Sender As Object, ByVal E As EventArgs)
    ''' <summary>
    ''' Nombre de la rutina que se ejecutara en servidor al hacer click en el botÃƒÂ³n e-factura (XML)
    ''' </summary>
    ''' <param name="E"></param>
    ''' <remarks></remarks>
    Protected Sub EFacturaXML_OnClick(ByVal E As EventArgs)
        RaiseEvent EFacturaXML_Click(Me, E)
    End Sub
    Private Sub EFacturaXML_OnClick(ByVal Sender As Object, ByVal E As EventArgs)
        EFacturaXML_OnClick(EventArgs.Empty)
    End Sub
    <Browsable(True), Bindable(True), _
    Description("Establece el texto que aparecera en botÃƒÂ³n e-factura (XML)"), Themeable(True), _
    Category("Appearance")> _
        Public Property TextoBotonEFacturaXML() As String
        Get
            EnsureChildControls()
            Return lnkEFacturaXML.Text
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            lnkEFacturaXML.Text = value
        End Set
    End Property
    <Browsable(True), Bindable(True), _
    Description("Establece si el botÃƒÂ³n e-factura (XML) serÃƒÂ¡ visible"), Themeable(True), _
    Category("Appearance"), DefaultValue(False)> _
        Public Property VisibleBotonEFacturaXML() As Boolean
        Get
            Return _visibleBotonEFacturaXML
        End Get
        Set(ByVal value As Boolean)
            _visibleBotonEFacturaXML = value
        End Set
    End Property
#End Region
#Region "Agregar Perfil"
    Private lnkAgregarPerfil As LinkButton
    Private _visibleBotonAgregarPerfil As Boolean
    Private _onClickAgregarPerfil As String
    Private _ocultarBotonAgregarPerfil As Boolean
    ''' <summary>
    ''' Funcion javascript que se ejecutara al pulsar el boton de Agregar Perfil
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Browsable(True), Bindable(True), _
    Description("Establece la funcion javascript que se ejecutara al pulsar en Agregar Perfil"), Themeable(True), _
    Category("Action")> _
    Public Property OnClientClickAgregarPerfil() As String
        Get
            EnsureChildControls()
            Return _onClickAgregarPerfil
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            _onClickAgregarPerfil = value
            lnkAgregarPerfil.Attributes.Add("onclick", _onClickAgregarPerfil)
        End Set
    End Property
    Public Event AgregarPerfil_Click(ByVal Sender As Object, ByVal E As EventArgs)
    ''' <summary>
    ''' Nombre de la rutina que se ejecutara en servidor al hacer click en el boton Agregar Perfil
    ''' </summary>
    ''' <param name="E"></param>
    ''' <remarks></remarks>
    Protected Sub AgregarPerfil_OnClick(ByVal E As EventArgs)
        RaiseEvent AgregarPerfil_Click(Me, E)
    End Sub
    Private Sub AgregarPerfil_OnClick(ByVal Sender As Object, ByVal E As EventArgs)
        AgregarPerfil_OnClick(EventArgs.Empty)
    End Sub
    <Browsable(True), Bindable(True), _
    Description("Establece el texto que aparecera en boton Agregar Perfil"), Themeable(True), _
    Category("Appearance")> _
    Public Property TextoBotonAgregarPerfil() As String
        Get
            EnsureChildControls()
            Return lnkAgregarPerfil.Text
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            lnkAgregarPerfil.Text = value
        End Set
    End Property
    <Browsable(True), Bindable(True), _
    Description("Establece si el boton Agregar Perfil sera visible"), Themeable(True), _
    Category("Appearance"), DefaultValue(False)> _
    Public Property VisibleBotonAgregarPerfil() As Boolean
        Get
            Return _visibleBotonAgregarPerfil
        End Get
        Set(ByVal value As Boolean)
            _visibleBotonAgregarPerfil = value
        End Set
    End Property
    <Browsable(True), Bindable(True), _
    Description("Establece si el boton Agregar Perfil sera ocultado pero esta presente"), Themeable(True), _
    Category("Appearance"), DefaultValue(False)> _
    Public Property OcultarBotonAgregarPerfil() As Boolean
        Get
            Return _ocultarBotonAgregarPerfil
        End Get
        Set(ByVal value As Boolean)
            _ocultarBotonAgregarPerfil = value
        End Set
    End Property
#End Region
#Region "Copiar Perfil"
    Private lnkCopiarPerfil As LinkButton
    Private _visibleBotonCopiarPerfil As Boolean
    Private _onClickCopiarPerfil As String
    Private _ocultarBotonCopiarPerfil As Boolean
    ''' <summary>
    ''' Funcion javascript que se ejecutara al pulsar el boton de Copiar Perfil
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Browsable(True), Bindable(True), _
    Description("Establece la funciÃ³n javascript que se ejecutara al pulsar Copiar Perfil"), Themeable(True), _
    Category("Action")> _
    Public Property OnClientClickCopiarPerfil() As String
        Get
            EnsureChildControls()
            Return _onClickCopiarPerfil
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            _onClickCopiarPerfil = value
            lnkCopiarPerfil.Attributes.Add("onclick", _onClickCopiarPerfil)
        End Set
    End Property
    Public Event CopiarPerfil_Click(ByVal Sender As Object, ByVal E As EventArgs)
    ''' <summary>
    ''' Nombre de la rutina que se ejecutara en servidor al hacer click en el boton Copiar Perfil
    ''' </summary>
    ''' <param name="E"></param>
    ''' <remarks></remarks>
    Protected Sub CopiarPerfil_OnClick(ByVal E As EventArgs)
        RaiseEvent CopiarPerfil_Click(Me, E)
    End Sub
    Private Sub CopiarPerfil_OnClick(ByVal Sender As Object, ByVal E As EventArgs)
        CopiarPerfil_OnClick(EventArgs.Empty)
    End Sub
    <Browsable(True), Bindable(True), _
    Description("Establece el texto que aparecera en botÃƒÂ³n Copiar Perfil"), Themeable(True), _
    Category("Appearance")> _
    Public Property TextoBotonCopiarPerfil() As String
        Get
            EnsureChildControls()
            Return lnkCopiarPerfil.Text
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            lnkCopiarPerfil.Text = value
        End Set
    End Property
    <Browsable(True), Bindable(True), _
    Description("Establece si el botÃƒÂ³n Copiar Perfil serÃƒÂ¡ visible"), Themeable(True), _
    Category("Appearance"), DefaultValue(False)> _
    Public Property VisibleBotonCopiarPerfil() As Boolean
        Get
            Return _visibleBotonCopiarPerfil
        End Get
        Set(ByVal value As Boolean)
            _visibleBotonCopiarPerfil = value
        End Set
    End Property
    <Browsable(True), Bindable(True), _
    Description("Establece si el boton Copiar Perfil sera ocultado pero esta presente"), Themeable(True), _
    Category("Appearance"), DefaultValue(False)> _
    Public Property OcultarBotonCopiarPerfil() As Boolean
        Get
            Return _ocultarBotonCopiarPerfil
        End Get
        Set(ByVal value As Boolean)
            _ocultarBotonCopiarPerfil = value
        End Set
    End Property
#End Region
#Region "Comparar Perfil"
    Private lnkCompararPerfil As LinkButton
    Private _visibleBotonCompararPerfil As Boolean
    Private _onClickCompararPerfil As String
    Private _ocultarBotonCompararPerfil As Boolean
    ''' <summary>
    ''' Funcion javascript que se ejecutara al pulsar el botÃ³n de Comparar Perfil
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Browsable(True), Bindable(True), _
    Description("Establece la funciÃ³n javascript que se ejecutara al pulsar Comparar Perfil"), Themeable(True), _
    Category("Action")> _
    Public Property OnClientClickCompararPerfil() As String
        Get
            EnsureChildControls()
            Return _onClickCompararPerfil
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            _onClickCompararPerfil = value
            lnkCompararPerfil.Attributes.Add("onclick", _onClickCompararPerfil)
        End Set
    End Property
    Public Event CompararPerfil_Click(ByVal Sender As Object, ByVal E As EventArgs)
    ''' <summary>
    ''' Nombre de la rutina que se ejecutara en servidor al hacer click en el botÃƒÂ³n Comparar Perfil
    ''' </summary>
    ''' <param name="E"></param>
    ''' <remarks></remarks>
    Protected Sub CompararPerfil_OnClick(ByVal E As EventArgs)
        RaiseEvent CompararPerfil_Click(Me, E)
    End Sub
    Private Sub CompararPerfil_OnClick(ByVal Sender As Object, ByVal E As EventArgs)
        CompararPerfil_OnClick(EventArgs.Empty)
    End Sub
    <Browsable(True), Bindable(True), _
    Description("Establece el texto que aparecera en botÃƒÂ³n Comparar Perfil"), Themeable(True), _
    Category("Appearance")> _
    Public Property TextoBotonCompararPerfil() As String
        Get
            EnsureChildControls()
            Return lnkCompararPerfil.Text
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            lnkCompararPerfil.Text = value
        End Set
    End Property
    <Browsable(True), Bindable(True), _
    Description("Establece si el botÃƒÂ³n Comparar Perfil serÃƒÂ¡ visible"), Themeable(True), _
    Category("Appearance"), DefaultValue(False)> _
    Public Property VisibleBotonCompararPerfil() As Boolean
        Get
            Return _visibleBotonCompararPerfil
        End Get
        Set(ByVal value As Boolean)
            _visibleBotonCompararPerfil = value
        End Set
    End Property
    <Browsable(True), Bindable(True), _
    Description("Establece si el boton Copiar Perfil sera ocultado pero esta presente"), Themeable(True), _
    Category("Appearance"), DefaultValue(False)> _
    Public Property OcultarBotonCompararPerfil() As Boolean
        Get
            Return _ocultarBotonCompararPerfil
        End Get
        Set(ByVal value As Boolean)
            _ocultarBotonCompararPerfil = value
        End Set
    End Property
#End Region
#Region "Eliminar Perfil"
    Private lnkEliminarPerfil As LinkButton
    Private _visibleBotonEliminarPerfil As Boolean
    Private _onClickEliminarPerfil As String
    Private _ocultarBotonEliminarPerfil As Boolean
    ''' <summary>
    ''' Funcion javascript que se ejecutara al pulsar el botón de Eliminar Perfil
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Browsable(True), Bindable(True), _
    Description("Establece la funciÃ³n javascript que se ejecutara al pulsar Eliminar Perfil"), Themeable(True), _
    Category("Action")> _
    Public Property OnClientClickEliminarPerfil() As String
        Get
            EnsureChildControls()
            Return _onClickEliminarPerfil
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            _onClickEliminarPerfil = value
            lnkEliminarPerfil.Attributes.Add("onclick", _onClickEliminarPerfil)
        End Set
    End Property
    Public Event EliminarPerfil_Click(ByVal Sender As Object, ByVal E As EventArgs)
    ''' <summary>
    ''' Nombre de la rutina que se ejecutara en servidor al hacer click en el botÃƒÂ³n Eliminar Perfil
    ''' </summary>
    ''' <param name="E"></param>
    ''' <remarks></remarks>
    Protected Sub EliminarPerfil_OnClick(ByVal E As EventArgs)
        RaiseEvent EliminarPerfil_Click(Me, E)
    End Sub
    Private Sub EliminarPerfil_OnClick(ByVal Sender As Object, ByVal E As EventArgs)
        EliminarPerfil_OnClick(EventArgs.Empty)
    End Sub
    <Browsable(True), Bindable(True), _
    Description("Establece el texto que aparecera en botÃƒÂ³n Eliminar Perfil"), Themeable(True), _
    Category("Appearance")> _
    Public Property TextoBotonEliminarPerfil() As String
        Get
            EnsureChildControls()
            Return lnkEliminarPerfil.Text
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            lnkEliminarPerfil.Text = value
        End Set
    End Property
    <Browsable(True), Bindable(True), _
    Description("Establece si el botÃƒÂ³n Eliminar Perfil serÃƒÂ¡ visible"), Themeable(True), _
    Category("Appearance"), DefaultValue(False)> _
    Public Property VisibleBotonEliminarPerfil() As Boolean
        Get
            Return _visibleBotonEliminarPerfil
        End Get
        Set(ByVal value As Boolean)
            _visibleBotonEliminarPerfil = value
        End Set
    End Property
    <Browsable(True), Bindable(True), _
    Description("Establece si el boton Eliminar Perfil sera ocultado pero esta presente"), Themeable(True), _
    Category("Appearance"), DefaultValue(False)> _
    Public Property OcultarBotonEliminarPerfil() As Boolean
        Get
            Return _ocultarBotonEliminarPerfil
        End Get
        Set(ByVal value As Boolean)
            _ocultarBotonEliminarPerfil = value
        End Set
    End Property
#End Region
#Region "Recepcionar"
    Private lnkRecepcionar As LinkButton
    Private _visibleBotonRecepcionar As Boolean
    Private _onClickRecepcionar As String
    Private _ocultarBotonRecepcionar As Boolean
    ''' <summary>
    ''' Funcion javascript que se ejecutara al pulsar el botÃƒÂ³n de Recepcionar
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Browsable(True), Bindable(True), _
    Description("Establece la funcion javascript que se ejecutara al pulsar en Recepcionar"), Themeable(True), _
    Category("Action")> _
    Public Property OnClientClickRecepcionar() As String
        Get
            EnsureChildControls()
            Return _onClickRecepcionar
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            _onClickRecepcionar = value
            lnkRecepcionar.Attributes.Add("onclick", _onClickRecepcionar)
        End Set
    End Property
    <Browsable(True), Bindable(True), _
    Description("Establece si el botÃ³n volver serÃ¡ visible"), Themeable(True), _
    Category("Appearance"), DefaultValue(False)> _
    Public Property OcultarBotonRecepcionar() As Boolean
        Get
            Return _ocultarBotonRecepcionar
        End Get
        Set(ByVal value As Boolean)
            _ocultarBotonRecepcionar = value
        End Set
    End Property
    Public Event Recepcionar_Click(ByVal Sender As Object, ByVal E As EventArgs)
    ''' <summary>
    ''' Nombre de la rutina que se ejecutara en servidor al hacer click en el botÃƒÂ³n Otras acciones
    ''' </summary>
    ''' <param name="E"></param>
    ''' <remarks></remarks>
    Protected Sub Recepcionar_OnClick(ByVal E As EventArgs)
        RaiseEvent Recepcionar_Click(Me, E)
    End Sub
    Private Sub Recepcionar_OnClick(ByVal Sender As Object, ByVal E As EventArgs)
        Recepcionar_OnClick(EventArgs.Empty)
    End Sub
    <Browsable(True), Bindable(True), _
    Description("Establece el texto que aparecera en boton Recepcionar"), Themeable(True), _
    Category("Appearance")> _
    Public Property TextoBotonRecepcionar() As String
        Get
            EnsureChildControls()
            Return lnkRecepcionar.Text
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            lnkRecepcionar.Text = value
        End Set
    End Property
    <Browsable(True), Bindable(True), _
    Description("Establece si el botÃƒÂ³n Otras acciones serÃƒÂ¡ visible"), Themeable(True), _
    Category("Appearance"), DefaultValue(False)> _
    Public Property VisibleBotonRecepcionar() As Boolean
        Get
            Return _visibleBotonRecepcionar
        End Get
        Set(ByVal value As Boolean)
            _visibleBotonRecepcionar = value
        End Set
    End Property
#End Region
#Region "Excel"
    Private lnkExcel As LinkButton
    Private _visibleBotonExcel As Boolean
    Private _onClickExcel As String
    Private _ocultarBotonExcel As Boolean
    ''' <summary>
    ''' Funcion javascript que se ejecutara al pulsar el botÃƒÂ³n de Excel
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Browsable(True), Bindable(True), _
    Description("Establece la funcion javascript que se ejecutara al pulsar en Excel"), Themeable(True), _
    Category("Action")> _
    Public Property OnClientClickExcel() As String
        Get
            EnsureChildControls()
            Return _onClickExcel
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            _onClickExcel = value
            lnkExcel.Attributes.Add("onclick", _onClickExcel)
        End Set
    End Property
    <Browsable(True), Bindable(True), _
    Description("Establece si el botÃ³n volver serÃ¡ visible"), Themeable(True), _
    Category("Appearance"), DefaultValue(False)> _
    Public Property OcultarBotonExcel() As Boolean
        Get
            Return _ocultarBotonExcel
        End Get
        Set(ByVal value As Boolean)
            _ocultarBotonExcel = value
        End Set
    End Property
    Public Event Excel_Click(ByVal Sender As Object, ByVal E As EventArgs)
    ''' <summary>
    ''' Nombre de la rutina que se ejecutara en servidor al hacer click en el botÃƒÂ³n Otras acciones
    ''' </summary>
    ''' <param name="E"></param>
    ''' <remarks></remarks>
    Protected Sub Excel_OnClick(ByVal E As EventArgs)
        RaiseEvent Excel_Click(Me, E)
    End Sub
    Private Sub Excel_OnClick(ByVal Sender As Object, ByVal E As EventArgs)
        Excel_OnClick(EventArgs.Empty)
    End Sub
    <Browsable(True), Bindable(True), _
    Description("Establece el texto que aparecera en boton Excel"), Themeable(True), _
    Category("Appearance")> _
    Public Property TextoBotonExcel() As String
        Get
            EnsureChildControls()
            Return lnkExcel.Text
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            lnkExcel.Text = value
        End Set
    End Property
    <Browsable(True), Bindable(True), _
    Description("Establece si el botÃƒÂ³n Otras acciones serÃƒÂ¡ visible"), Themeable(True), _
    Category("Appearance"), DefaultValue(False)> _
    Public Property VisibleBotonExcel() As Boolean
        Get
            Return _visibleBotonExcel
        End Get
        Set(ByVal value As Boolean)
            _visibleBotonExcel = value
        End Set
    End Property
#End Region
    ''' <summary>
    ''' Definimos los controles que apareceran en la cabecera
    ''' </summary>
    ''' <remarks></remarks>
    Protected Overrides Sub CreateChildControls()
        Controls.Clear()

        imgCabecera = New HtmlControls.HtmlImage
        imgCabecera.ID = "imgCabecera"
        lblCabecera = New Label
        lblCabecera.ID = "lblCabecera"
        lblCabecera.CssClass = "LabelTituloCabeceraCustomControl"

        lnkEnviarCertificado = New LinkButton
        lnkEnviarCertificado.ID = "lnkBotonEnviarCertificado"
        lnkEnviarCertificado.CssClass = "CabeceraBotonCustomControl"
        AddHandler lnkEnviarCertificado.Click, _
                AddressOf EnviarCertificado_OnClick

        lnkSolicitarCertificado = New LinkButton
        lnkSolicitarCertificado.ID = "lnkBotonSolicitarCertificado"
        lnkSolicitarCertificado.CssClass = "CabeceraBotonCustomControl"
        AddHandler lnkSolicitarCertificado.Click, _
                AddressOf SolicitarCertificado_OnClick

        lnkRenovarCertificado = New LinkButton
        lnkRenovarCertificado.ID = "lnkBotonRenovarCertificado"
        lnkRenovarCertificado.CssClass = "CabeceraBotonCustomControl"
        AddHandler lnkRenovarCertificado.Click, _
                AddressOf RenovarCertificado_OnClick

        lnkImportar = New LinkButton
        lnkImportar.ID = "lnkBotonImportar"
        lnkImportar.CssClass = "CabeceraBotonCustomControl"
        AddHandler lnkImportar.Click, _
                AddressOf Importar_OnClick

        lnkCalcular = New LinkButton
        lnkCalcular.ID = "lnkBotonCalcular"
        lnkCalcular.CssClass = "CabeceraBotonCustomControl"
        AddHandler lnkCalcular.Click, _
                AddressOf Calcular_OnClick

        lnkMail = New LinkButton
        lnkMail.ID = "lnkBotonMail"
        lnkMail.CssClass = "CabeceraBotonCustomControl"
        AddHandler lnkMail.Click, _
                AddressOf Mail_OnClick

        lnkImpExp = New LinkButton
        lnkImpExp.ID = "lnkBotonImpExp"
        lnkImpExp.CssClass = "CabeceraBotonCustomControl"
        AddHandler lnkImpExp.Click, _
                AddressOf ImpExp_OnClick

        lnkEmitir = New LinkButton
        lnkEmitir.ID = "lnkBotonEmitir"
        lnkEmitir.CssClass = "CabeceraBotonCustomControl"
        AddHandler lnkEmitir.Click, _
                AddressOf Emitir_OnClick

        lnkBorrar = New LinkButton
        lnkBorrar.ID = "lnkBotonBorrar"
        lnkBorrar.CssClass = "CabeceraBotonCustomControl"
        AddHandler lnkBorrar.Click, AddressOf Borrar_OnClick

        lnkDeshacerBorrar = New LinkButton
        lnkDeshacerBorrar.ID = "lnkBotonDeshacerBorrar"
        lnkDeshacerBorrar.CssClass = "CabeceraBotonCustomControl"
        AddHandler lnkDeshacerBorrar.Click, AddressOf DeshacerBorrar_OnClick

        lnkEliminar = New LinkButton
        lnkEliminar.ID = "lnkBotonEliminar"
        lnkEliminar.CssClass = "CabeceraBotonCustomControl"
        AddHandler lnkEliminar.Click, _
                AddressOf Eliminar_OnClick

        lnkGuardar = New LinkButton
        lnkGuardar.ID = "lnkBotonGuardar"
        lnkGuardar.CssClass = "CabeceraBotonCustomControl"
        AddHandler lnkGuardar.Click, _
                AddressOf Guardar_OnClick

        lnkCierrePositivo = New LinkButton
        lnkCierrePositivo.ID = "lnkBotonCierrePositivo"
        lnkCierrePositivo.CssClass = "CabeceraBotonCustomControl"
        AddHandler lnkCierrePositivo.Click, _
                AddressOf CierrePositivo_OnClick

        lnkCierreNegativo = New LinkButton
        lnkCierreNegativo.ID = "lnkBotonCierreNegativo"
        lnkCierreNegativo.CssClass = "CabeceraBotonCustomControl"
        AddHandler lnkCierreNegativo.Click, _
                AddressOf CierreNegativo_OnClick

        lnkReabrir = New LinkButton
        lnkReabrir.ID = "lnkBotonReabrir"
        lnkReabrir.CssClass = "CabeceraBotonCustomControl"
        AddHandler lnkReabrir.Click, _
                AddressOf Reabrir_OnClick

        lnkVolver = New LinkButton
        lnkVolver.ID = "lnkBotonVolver"
        lnkVolver.CssClass = "CabeceraBotonCustomControl"
        AddHandler lnkVolver.Click, _
                AddressOf Volver_OnClick

        lnkAnular = New LinkButton
        lnkAnular.ID = "lnkBotonAnular"
        lnkAnular.CssClass = "CabeceraBotonCustomControl"
        AddHandler lnkAnular.Click, _
                AddressOf Anular_OnClick

        lnkAccion1 = New LinkButton
        lnkAccion1.ID = "lnkBotonAccion1"
        lnkAccion1.CssClass = "CabeceraBotonCustomControl"
        AddHandler lnkAccion1.Click, _
                AddressOf Accion1_OnClick

        lnkAccion2 = New LinkButton
        lnkAccion2.ID = "lnkBotonAccion2"
        lnkAccion2.CssClass = "CabeceraBotonCustomControl"
        AddHandler lnkAccion2.Click, _
                AddressOf Accion2_OnClick

        lnkAccion3 = New LinkButton
        lnkAccion3.ID = "lnkBotonAccion3"
        lnkAccion3.CssClass = "CabeceraBotonCustomControl"
        AddHandler lnkAccion3.Click, _
                AddressOf Accion3_OnClick

        lnkTrasladar = New LinkButton
        lnkTrasladar.ID = "lnkBotonTrasladar"
        lnkTrasladar.CssClass = "CabeceraBotonCustomControl"
        AddHandler lnkTrasladar.Click, _
                AddressOf Trasladar_OnClick

        lnkDevolver = New LinkButton
        lnkDevolver.ID = "lnkBotonDevolver"
        lnkDevolver.CssClass = "CabeceraBotonCustomControl"
        AddHandler lnkDevolver.Click, _
                AddressOf Devolver_OnClick

        lnkModificar = New LinkButton
        lnkModificar.ID = "lnkBotonModificar"
        lnkModificar.CssClass = "CabeceraBotonCustomControl"
        AddHandler lnkModificar.Click, _
                AddressOf Modificar_OnClick

        lnkListados = New LinkButton
        lnkListados.ID = "lnkBotonListados"
        lnkListados.CssClass = "CabeceraBotonCustomControl"
        AddHandler lnkListados.Click, _
                AddressOf Listados_OnClick

        lnkAprobar = New LinkButton
        lnkAprobar.ID = "lnkBotonAprobar"
        lnkAprobar.CssClass = "CabeceraBotonCustomControl"
        AddHandler lnkAprobar.Click, _
                AddressOf Aprobar_OnClick

        lnkRechazar = New LinkButton
        lnkRechazar.ID = "lnkBotonRechazar"
        lnkRechazar.CssClass = "CabeceraBotonCustomControl"
        AddHandler lnkRechazar.Click, _
                AddressOf Rechazar_OnClick

        lnkVolverColaboracion = New LinkButton
        lnkVolverColaboracion.ID = "lnkBotonVolverColaboracion"
        lnkVolverColaboracion.CssClass = "CabeceraBotonCustomControl"
        AddHandler lnkVolverColaboracion.Click, _
                AddressOf VolverColaboracion_OnClick

        lnkEnviarAFavoritos = New LinkButton
        lnkEnviarAFavoritos.ID = "lnkBotonEnviarAFavoritos"
        lnkEnviarAFavoritos.CssClass = "CabeceraBotonCustomControl"
        AddHandler lnkEnviarAFavoritos.Click, _
                AddressOf EnviarAFavoritos_OnClick

        lnkEFacturaPDF = New LinkButton
        lnkEFacturaPDF.ID = "lnkBotonEFacturaPDF"
        lnkEFacturaPDF.CssClass = "CabeceraBotonCustomControl"
        AddHandler lnkEFacturaPDF.Click, _
                AddressOf EFacturaPDF_OnClick

        lnkEFacturaXML = New LinkButton
        lnkEFacturaXML.ID = "lnkBotonEFacturaXML"
        lnkEFacturaXML.CssClass = "CabeceraBotonCustomControl"
        AddHandler lnkEFacturaXML.Click, _
                AddressOf EFacturaXML_OnClick

        lnkAgregarPerfil = New LinkButton
        lnkAgregarPerfil.ID = "lnkBotonAgregarPerfil"
        lnkAgregarPerfil.CssClass = "CabeceraBotonCustomControl"
        AddHandler lnkAgregarPerfil.Click, _
                AddressOf AgregarPerfil_OnClick

        lnkCopiarPerfil = New LinkButton
        lnkCopiarPerfil.ID = "lnkBotonCopiarPerfil"
        lnkCopiarPerfil.CssClass = "CabeceraBotonCustomControl"
        AddHandler lnkCopiarPerfil.Click, _
                AddressOf CopiarPerfil_OnClick

        lnkCompararPerfil = New LinkButton
        lnkCompararPerfil.ID = "lnkBotonCompararPerfil "
        lnkCompararPerfil.CssClass = "CabeceraBotonCustomControl"
        AddHandler lnkCompararPerfil.Click, _
                AddressOf CompararPerfil_OnClick

        lnkEliminarPerfil = New LinkButton
        lnkEliminarPerfil.ID = "lnkBotonEliminarPerfil"
        lnkEliminarPerfil.CssClass = "CabeceraBotonCustomControl"
        AddHandler lnkEliminarPerfil.Click, _
                AddressOf EliminarPerfil_OnClick

        lnkExcel = New LinkButton
        lnkExcel.ID = "lnkBotonExcel"
        lnkExcel.CssClass = "CabeceraBotonCustomControl"
        AddHandler lnkExcel.Click, _
                AddressOf Excel_OnClick

        lnkRecepcionar = New LinkButton
        lnkRecepcionar.ID = "lnkBotonRecepcionar"
        lnkRecepcionar.CssClass = "CabeceraBotonCustomControl"
        AddHandler lnkRecepcionar.Click, _
                AddressOf Recepcionar_OnClick

        Me.Controls.Add(lblCabecera)
        Me.Controls.Add(lnkEFacturaXML)
        Me.Controls.Add(lnkEFacturaPDF)
        Me.Controls.Add(lnkImportar)
        Me.Controls.Add(lnkCalcular)
        Me.Controls.Add(lnkMail)
        Me.Controls.Add(lnkImpExp)
        Me.Controls.Add(lnkEmitir)
        Me.Controls.Add(lnkBorrar)
        Me.Controls.Add(lnkDeshacerBorrar)
        Me.Controls.Add(lnkEliminar)
        Me.Controls.Add(lnkGuardar)
        Me.Controls.Add(lnkCierrePositivo)
        Me.Controls.Add(lnkCierreNegativo)
        Me.Controls.Add(lnkReabrir)
        Me.Controls.Add(lnkAnular)
        Me.Controls.Add(lnkAccion1)
        Me.Controls.Add(lnkAccion2)
        Me.Controls.Add(lnkAccion3)
        Me.Controls.Add(lnkTrasladar)
        Me.Controls.Add(lnkDevolver)
        Me.Controls.Add(lnkModificar)
        Me.Controls.Add(lnkListados)
        Me.Controls.Add(lnkAprobar)
        Me.Controls.Add(lnkRechazar)
        Me.Controls.Add(lnkVolver)
        Me.Controls.Add(lnkVolverColaboracion)
        Me.Controls.Add(lnkEnviarAFavoritos)
        Me.Controls.Add(lnkAgregarPerfil)
        Me.Controls.Add(lnkCopiarPerfil)
        Me.Controls.Add(lnkCompararPerfil)
        Me.Controls.Add(lnkEliminarPerfil)
        Me.Controls.Add(lnkExcel)
        Me.Controls.Add(lnkRecepcionar)
    End Sub
    ''' <summary>
    ''' Para FireFox no funciona el text-overflow:ellipsis luego hay q hacer por javascript el "recorte" del titulo
    ''' </summary>
    ''' <param name="sender">Objeto cabecera</param>
    ''' <param name="e">evento</param>
    ''' <remarks>Llamada desde: sistema; Tiempo maximo: 0</remarks>
    Private Sub FSNPageHeader_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Page.ClientScript.RegisterClientScriptResource(Me.GetType(), "Fullstep.FSNWebControls.jquery.js")
        Me.Page.ClientScript.RegisterClientScriptResource(Me.GetType(), "Fullstep.FSNWebControls.FSNPageHeader.js")
    End Sub
    ''' <summary>
    ''' Asignamos las imagenes de cada botón
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub FSNPageHeader_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'Nuevo diseño
        lnkVolver.Style.Add("background-image", "url(" & Me.Page.ClientScript.GetWebResourceUrl(GetType(FSNPageHeader), "Fullstep.FSNWebControls.volver.gif") & ")")
        lnkEnviarCertificado.Style.Add("background-image", "url(" & Me.Page.ClientScript.GetWebResourceUrl(GetType(FSNPageHeader), "Fullstep.FSNWebControls.enviarcertificado.gif") & ")")
        lnkSolicitarCertificado.Style.Add("background-image", "url(" & Me.Page.ClientScript.GetWebResourceUrl(GetType(FSNPageHeader), "Fullstep.FSNWebControls.solicitarCertificado.gif") & ")")
        lnkRenovarCertificado.Style.Add("background-image", "url(" & Me.Page.ClientScript.GetWebResourceUrl(GetType(FSNPageHeader), "Fullstep.FSNWebControls.renovar.gif") & ")")
        lnkImportar.Style.Add("background-image", "url(" & Me.Page.ClientScript.GetWebResourceUrl(GetType(FSNPageHeader), "Fullstep.FSNWebControls.importar.gif") & ")")
        lnkCalcular.Style.Add("background-image", "url(" & Me.Page.ClientScript.GetWebResourceUrl(GetType(FSNPageHeader), "Fullstep.FSNWebControls.calcular.gif") & ")")
        lnkMail.Style.Add("background-image", "url(" & Me.Page.ClientScript.GetWebResourceUrl(GetType(FSNPageHeader), "Fullstep.FSNWebControls.enviar_email.gif") & ")")
        lnkImpExp.Style.Add("background-image", "url(" & Me.Page.ClientScript.GetWebResourceUrl(GetType(FSNPageHeader), "Fullstep.FSNWebControls.imprimir.png") & ")")
        lnkEmitir.Style.Add("background-image", "url(" & Me.Page.ClientScript.GetWebResourceUrl(GetType(FSNPageHeader), "Fullstep.FSNWebControls.emitir.gif") & ")")
        lnkBorrar.Style.Add("background-image", "url(" & Me.Page.ClientScript.GetWebResourceUrl(GetType(FSNPageHeader), "Fullstep.FSNWebControls.suprimir.gif") & ")")
        lnkDeshacerBorrar.Style.Add("background-image", "url(" & Me.Page.ClientScript.GetWebResourceUrl(GetType(FSNPageHeader), "Fullstep.FSNWebControls.DeshacerBorrado.gif") & ")")
        lnkEliminar.Style.Add("background-image", "url(" & Me.Page.ClientScript.GetWebResourceUrl(GetType(FSNPageHeader), "Fullstep.FSNWebControls.suprimir.gif") & ")")
        lnkGuardar.Style.Add("background-image", "url(" & Me.Page.ClientScript.GetWebResourceUrl(GetType(FSNPageHeader), "Fullstep.FSNWebControls.guardar.gif") & ")")
        lnkCierrePositivo.Style.Add("background-image", "url(" & Me.Page.ClientScript.GetWebResourceUrl(GetType(FSNPageHeader), "Fullstep.FSNWebControls.cerrar_eficaz.gif") & ")")
        lnkCierreNegativo.Style.Add("background-image", "url(" & Me.Page.ClientScript.GetWebResourceUrl(GetType(FSNPageHeader), "Fullstep.FSNWebControls.cerrar_noeficaz.gif") & ")")
        lnkReabrir.Style.Add("background-image", "url(" & Me.Page.ClientScript.GetWebResourceUrl(GetType(FSNPageHeader), "Fullstep.FSNWebControls.reabrir.gif") & ")")
        lnkVolverColaboracion.Style.Add("background-image", "url(" & Me.Page.ClientScript.GetWebResourceUrl(GetType(FSNPageHeader), "Fullstep.FSNWebControls.VolverColaboracion.png") & ")")
        lnkEnviarAFavoritos.Style.Add("background-image", "url(" & Me.Page.ClientScript.GetWebResourceUrl(GetType(FSNPageHeader), "Fullstep.FSNWebControls.EnviarAFavoritos.gif") & ")")
        lnkAnular.Style.Add("background-image", "url(" & Me.Page.ClientScript.GetWebResourceUrl(GetType(FSNPageHeader), "Fullstep.FSNWebControls.AnularSolicitud.png") & ")")
        lnkAccion1.Style.Add("background-image", "url(" & Me.Page.ClientScript.GetWebResourceUrl(GetType(FSNPageHeader), "Fullstep.FSNWebControls.acciones.gif") & ")")
        lnkAccion2.Style.Add("background-image", "url(" & Me.Page.ClientScript.GetWebResourceUrl(GetType(FSNPageHeader), "Fullstep.FSNWebControls.acciones.gif") & ")")
        lnkAccion3.Style.Add("background-image", "url(" & Me.Page.ClientScript.GetWebResourceUrl(GetType(FSNPageHeader), "Fullstep.FSNWebControls.acciones.gif") & ")")
        lnkTrasladar.Style.Add("background-image", "url(" & Me.Page.ClientScript.GetWebResourceUrl(GetType(FSNPageHeader), "Fullstep.FSNWebControls.TrasladarSolicitud.png") & ")")
        lnkDevolver.Style.Add("background-image", "url(" & Me.Page.ClientScript.GetWebResourceUrl(GetType(FSNPageHeader), "Fullstep.FSNWebControls.DevolverSolicitud.png") & ")")
        lnkModificar.Style.Add("background-image", "url(" & Me.Page.ClientScript.GetWebResourceUrl(GetType(FSNPageHeader), "Fullstep.FSNWebControls.Lapiz.png") & ")")
        lnkListados.Style.Add("background-image", "url(" & Me.Page.ClientScript.GetWebResourceUrl(GetType(FSNPageHeader), "Fullstep.FSNWebControls.imprimir.png") & ")")
        lnkAprobar.Style.Add("background-image", "url(" & Me.Page.ClientScript.GetWebResourceUrl(GetType(FSNPageHeader), "Fullstep.FSNWebControls.AprobarSolicitud.png") & ")")
        lnkRechazar.Style.Add("background-image", "url(" & Me.Page.ClientScript.GetWebResourceUrl(GetType(FSNPageHeader), "Fullstep.FSNWebControls.RechazarSolicitud.png") & ")")
        lnkEFacturaPDF.Style.Add("background-image", "url(" & Me.Page.ClientScript.GetWebResourceUrl(GetType(FSNPageHeader), "Fullstep.FSNWebControls.pdf.png") & ")")
        lnkEFacturaXML.Style.Add("background-image", "url(" & Me.Page.ClientScript.GetWebResourceUrl(GetType(FSNPageHeader), "Fullstep.FSNWebControls.xml.png") & ")")
        lnkEliminarPerfil.Style.Add("background-image", "url(" & Me.Page.ClientScript.GetWebResourceUrl(GetType(FSNPageHeader), "Fullstep.FSNWebControls.eliminar.png") & ")")
        lnkAgregarPerfil.Style.Add("background-image", "url(" & Me.Page.ClientScript.GetWebResourceUrl(GetType(FSNPageHeader), "Fullstep.FSNWebControls.nuevo.png") & ")")
        lnkCompararPerfil.Style.Add("background-image", "url(" & Me.Page.ClientScript.GetWebResourceUrl(GetType(FSNPageHeader), "Fullstep.FSNWebControls.comparar.png") & ")")
        lnkCopiarPerfil.Style.Add("background-image", "url(" & Me.Page.ClientScript.GetWebResourceUrl(GetType(FSNPageHeader), "Fullstep.FSNWebControls.copiar.png") & ")")
        lnkRecepcionar.Style.Add("background-image", "url(" & Me.Page.ClientScript.GetWebResourceUrl(GetType(FSNPageHeader), "Fullstep.FSNWebControls.recepcion.gif") & ")")
        lnkExcel.Style.Add("background-image", "url(" & Me.Page.ClientScript.GetWebResourceUrl(GetType(FSNPageHeader), "Fullstep.FSNWebControls.Excel.png") & ")")
    End Sub
    ''' Revisado por: Jbg. Fecha: 24/11/2011
    ''' <summary>
    ''' Montamos la cabecera creando código html y renderizando los controles. Añadiremos, renderizaremos los botones
    ''' segun si su propiedad de visible es verdadera o falsa. Igualmente controlamos si es el primer botón de la 
    ''' cabecera para saber si añadir o no el borde separador.
    ''' </summary>
    ''' <param name="writer">código html de cabecera</param>
    ''' <remarks>LLamada desde: sistema; Tiempo maximo: 0</remarks>
    Protected Overrides Sub Render(ByVal writer As HtmlTextWriter)
        Dim EsPrimerBoton As Boolean = True

        'Div contenedor PageHeader
        If _aspectoGS Then
            writer.AddAttribute(HtmlTextWriterAttribute.Style, "background:url('" & Me.Page.ClientScript.GetWebResourceUrl(GetType(FSNPageHeader), "Fullstep.FSNWebControls.cabeceraFondo_GS.JPG") & "');")
        End If
        writer.AddAttribute(HtmlTextWriterAttribute.Class, "PageHeaderBackground")
        writer.RenderBeginTag(HtmlTextWriterTag.Div)

        'Div imagen PageHeader
        imgCabecera.Style.Add("max-width", _WidthImagenCabecera)
        imgCabecera.Style.Add("max-height", _HeightImagenCabecera)
        imgCabecera.RenderControl(writer)
        'Fin Div imagen PageHeader

        'Div titulo PageHeader
        writer.AddAttribute(HtmlTextWriterAttribute.Style, "display:none; position:absolute; left:60px;")

        lblCabecera.RenderControl(writer)
        'Fin Div titulo PageHeader

        If _visibleBotonRecepcionar Then
            If _ocultarBotonRecepcionar Then lnkRecepcionar.Style.Add("display", "none")
            lnkRecepcionar.RenderControl(writer)
        End If

        If _visibleBotonEFacturaXML Then lnkEFacturaXML.RenderControl(writer)

        If _visibleBotonEFacturaPDF Then lnkEFacturaPDF.RenderControl(writer)

        If _visibleBotonCalcular Then lnkCalcular.RenderControl(writer)

        If _visibleBotonImportar Then lnkImportar.RenderControl(writer)

        If _visibleBotonMail Then lnkMail.RenderControl(writer)

        If _visibleBotonSolicitarCertificado Then lnkSolicitarCertificado.RenderControl(writer)

        If _visibleBotonRenovarCertificado Then lnkRenovarCertificado.RenderControl(writer)

        If _visibleBotonEnviarCertificado Then lnkEnviarCertificado.RenderControl(writer)

        If _visibleBotonEmitir Then
            If _ocultarBotonEmitir Then lnkEmitir.Style.Add("display", "none")
            lnkEmitir.RenderControl(writer)
        End If

        If _visibleBotonEliminar Then
            If _ocultarBotonEliminar Then lnkEliminar.Style.Add("display", "none")
            lnkEliminar.RenderControl(writer)
        End If

        If _visibleBotonAnular Then
            If _ocultarBotonAnular Then lnkAnular.Style.Add("display", "none")
            lnkAnular.RenderControl(writer)
        End If

        If _visibleBotonGuardar Then lnkGuardar.RenderControl(writer)

        If _visibleBotonAgregarPerfil Then
            If _ocultarBotonAgregarPerfil Then lnkAgregarPerfil.Style.Add("display", "none")
            lnkAgregarPerfil.RenderControl(writer)
        End If

        If _visibleBotonCopiarPerfil Then
            If _ocultarBotonCopiarPerfil Then lnkCopiarPerfil.Style.Add("display", "none")
            lnkCopiarPerfil.RenderControl(writer)
        End If

        If _visibleBotonCompararPerfil Then
            If _ocultarBotonCompararPerfil Then lnkCompararPerfil.Style.Add("display", "none")
            lnkCompararPerfil.RenderControl(writer)
        End If

        If _visibleBotonEliminarPerfil Then
            If _ocultarBotonEliminarPerfil Then lnkEliminarPerfil.Style.Add("display", "none")
            lnkEliminarPerfil.RenderControl(writer)
        End If

        If _visibleBotonEnviarAFavoritos Then lnkEnviarAFavoritos.RenderControl(writer)

        If _visibleBotonCierrePositivo Then
            If _ocultarBotonCierrePositivo Then lnkCierrePositivo.Style.Add("display", "none")
            lnkCierrePositivo.RenderControl(writer)
        End If

        If _visibleBotonCierreNegativo Then
            If _ocultarBotonCierreNegativo Then lnkCierreNegativo.Style.Add("display", "none")
            lnkCierreNegativo.RenderControl(writer)
        End If

        If _visibleBotonBorrar Then
            If _ocultarBotonBorrar Then lnkBorrar.Style.Add("display", "none")
            lnkBorrar.RenderControl(writer)
        End If

        If _visibleBotonDeshacerBorrar Then
            If _ocultarBotonDeshacerBorrar Then lnkDeshacerBorrar.Style.Add("display", "none")
            lnkDeshacerBorrar.RenderControl(writer)
        End If

        If _visibleBotonReabrir Then
            If _ocultarBotonReabrir Then lnkReabrir.Style.Add("display", "none")
            lnkReabrir.RenderControl(writer)
        End If

        If _visibleBotonAccion1 Then lnkAccion1.RenderControl(writer)

        If _visibleBotonAccion2 Then lnkAccion2.RenderControl(writer)

        If _visibleBotonAccion3 Then lnkAccion3.RenderControl(writer)

        If _visibleBotonDevolver Then lnkDevolver.RenderControl(writer)

        If _visibleBotonTrasladar Then lnkTrasladar.RenderControl(writer)

        If _visibleBotonModificar Then lnkModificar.RenderControl(writer)

        If _visibleBotonListados Then lnkListados.RenderControl(writer)

        If _visibleBotonAprobar Then lnkAprobar.RenderControl(writer)

        If _visibleBotonRechazar Then lnkRechazar.RenderControl(writer)

        If _visibleBotonImpExp Then
            If _ocultarBotonImpExp Then lnkImpExp.Style.Add("display", "none")
            lnkImpExp.RenderControl(writer)
        End If
        If _visibleBotonVolverColaboracion Then lnkVolverColaboracion.RenderControl(writer)

        If _visibleBotonExcel Then
            If _ocultarBotonExcel Then lnkExcel.Style.Add("display", "none")
            lnkExcel.RenderControl(writer)
        End If

        If _visibleBotonVolver Then lnkVolver.RenderControl(writer)

        writer.RenderEndTag()
        ''Fin Div contenedor PageHeader
    End Sub
End Class
