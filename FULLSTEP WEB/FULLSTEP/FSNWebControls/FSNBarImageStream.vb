﻿Imports System
Imports System.Web

Public Class FSNBarImageStream
    Implements IHttpModule

    Friend Const HandlerFileName As String = "fsnbar_image_processor.aspx"
    Friend Const FSNBarNamePrefixInApplication As String = "C#H#R#"
    Public Delegate Sub FSNBarEventHandler(ByVal s As Object, ByVal e As EventArgs)

    ''' <summary>
    ''' Inicializa el módulo y lo prepara para el control de solicitudes.
    ''' </summary>
    ''' <param name="app">Un objeto HttpApplication que proporciona acceso a los métodos, propiedades y eventos comunes a todos los objetos de aplicación existentes en la aplicación ASP.NET.</param>
    ''' <remarks></remarks>
    Public Sub Init(ByVal app As HttpApplication) Implements IHttpModule.Init
        AddHandler app.BeginRequest, AddressOf Me.OnBeginRequest
    End Sub

    ''' <summary>
    ''' Elimina los recursos (distintos de la memoria) utilizados por el módulo que implementa IHttpModule.
    ''' </summary>
    ''' <remarks>El método Dispose realiza todo el trabajo de limpieza final anterior a la eliminación del módulo de la canalización de ejecución.</remarks>
    Public Sub Dispose() Implements IHttpModule.Dispose

    End Sub

    Public Event WebChartEvent As FSNBarEventHandler

    ''' <summary>
    ''' Se produce como el primer evento de la cadena de ejecución de canalización HTTP cuando ASP.NET responde a una solicitud.
    ''' </summary>
    ''' <param name="s">Origen del evento</param>
    ''' <param name="e">Objeto EventArgs con los datos del evento</param>
    ''' <remarks>Llama al método RenderImageBar y devuelve el stream generado</remarks>
    Public Sub OnBeginRequest(ByVal s As Object, ByVal e As EventArgs)
        Dim webApp As HttpApplication = CType(s, HttpApplication)

        If InStr(webApp.Request.Path.ToLower, HandlerFileName, CompareMethod.Binary) <> 0 Then
            ' Obtiene el objeto FSNHorizontalBar
            Dim FSNBarToDraw As FSNHorizontalBar = _
                CType(webApp.Application.Item(FSNBarNamePrefixInApplication & _
                CStr(webApp.Request.QueryString("id"))), FSNHorizontalBar)
            If FSNBarToDraw Is Nothing Then
                ' No se encuentra el objeto FSNHorizontalBar
                ' Devuelve error 404
            Else
                Try
                    With FSNBarToDraw
                        Dim memStream As System.IO.MemoryStream
                        memStream = .RenderImageBar()
                        memStream.WriteTo(webApp.Context.Response.OutputStream)
                        .Dispose()
                        memStream.Close()
                    End With
                    With webApp
                        .Context.ClearError()
                        .Response.ContentType = "image/gif"
                        .Response.StatusCode = 200
                        .Application.Remove(FSNBarNamePrefixInApplication & _
                            CStr(.Request.QueryString("id")))
                        .Response.End()
                    End With
                Catch WebChartException As Exception
                End Try
            End If
        End If
    End Sub

End Class
