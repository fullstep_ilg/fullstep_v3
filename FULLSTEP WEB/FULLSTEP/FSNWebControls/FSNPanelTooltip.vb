﻿Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Text
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.Design
Imports System.Drawing.Design

Public Class FSNPanelTooltip
    Inherits Control
    Implements INamingContainer

    Private _panel As Panel
    Private _dynextender As AjaxControlToolkit.DynamicPopulateExtender
    Private _modalpopupextender As AjaxControlToolkit.ModalPopupExtender
    Private _label As Label

    <Bindable(True), Category("Appearance"), DefaultValue(""), Localizable(True)> _
    Property Text() As String
        Get
            EnsureChildControls()
            Return _label.Text
        End Get
        Set(ByVal Value As String)
            EnsureChildControls()
            _label.Text = Value
        End Set
    End Property

    <Browsable(True), Description("Id del control contenedor"), _
    TypeConverter(GetType(AssociatedControlConverter)), Category("Behavior")> _
    Public Property Contenedor() As String
        Get
            Dim o As Object = ViewState("Contenedor")
            If o Is Nothing Then
                Return String.Empty
            Else
                Return CStr(o)
            End If
        End Get
        Set(ByVal value As String)
            ViewState("Contenedor") = value
        End Set
    End Property

    ''' <summary>
    ''' Aunque el nombre sea confuso, es un modalpopup, no un animation.
    ''' </summary>
    <Browsable(False), Description("Id del control AnimationExtender")> _
    Public ReadOnly Property AnimationClientID() As String
        Get
            EnsureChildControls()
            Return _modalpopupextender.ClientID
        End Get
    End Property

    <Browsable(False), Description("Id del control DynamicPopulateExtender")> _
    Public ReadOnly Property DynamicPopulateClientID() As String
        Get
            If _dynextender Is Nothing Then
                Return Nothing
            Else
                Return _dynextender.ClientID
            End If
        End Get
    End Property

    <Browsable(True), Description("Path del servicio web"), _
    Editor(GetType(UrlEditor), GetType(UITypeEditor)), UrlProperty(), _
    Category("Behavior")> _
    Public Property ServicePath() As String
        Get
            Return ViewState("ServicePath")
        End Get
        Set(ByVal value As String)
            ViewState("ServicePath") = value
            If _dynextender IsNot Nothing Then _
                _dynextender.ServicePath = value
        End Set
    End Property

    <Browsable(True), Description("Método del servicio web"), _
    Category("Behavior")> _
    Public Property ServiceMethod() As String
        Get
            If _dynextender Is Nothing Then
                Return String.Empty
            Else
                Return _dynextender.ServiceMethod
            End If
        End Get
        Set(ByVal value As String)
            If String.IsNullOrEmpty(value) Then
                If _dynextender IsNot Nothing Then
                    Me.Controls.Remove(_dynextender)
                    _dynextender = Nothing
                End If
            Else
                EnsureChildControls()
                If _dynextender Is Nothing Then
                    _dynextender = New AjaxControlToolkit.DynamicPopulateExtender()
                    _dynextender.ID = "lblTooltip_DynamicPopulateExtender"
                    _dynextender.Enabled = True
                    _dynextender.TargetControlID = _label.ID
                    _dynextender.UpdatingCssClass = "actualizando"
                    If Not String.IsNullOrEmpty(ViewState("ServicePath")) Then _
                        _dynextender.ServicePath = ViewState("ServicePath")
                    Me.Controls.Add(_dynextender)
                End If
                _dynextender.ServiceMethod = value
            End If
        End Set
    End Property

    <Browsable(True), Bindable(False), _
        Category("Appearance"), Description("Color de Fondo"), _
        TypeConverter(GetType(WebColorConverter)), _
        Themeable(True), DefaultValue(GetType(Drawing.Color), "#F1F1F1")> _
    Public Property BackColor() As System.Drawing.Color
        Get
            EnsureChildControls()
            If _panel.BackColor.IsEmpty() Then _panel.BackColor = Drawing.ColorTranslator.FromHtml("#F1F1F1")
            Return _panel.BackColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            EnsureChildControls()
            _panel.BackColor = value
        End Set
    End Property

    <Browsable(True), Bindable(False), _
    Category("Appearance"), Description("Color del borde"), _
    TypeConverter(GetType(WebColorConverter)), _
    Themeable(True), DefaultValue(GetType(Drawing.Color), "#8C8C8C")> _
    Public Property BorderColor() As System.Drawing.Color
        Get
            EnsureChildControls()
            If _panel.BorderColor.IsEmpty() Then _panel.BorderColor = Drawing.ColorTranslator.FromHtml("#8C8C8C")
            Return _panel.BorderColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            EnsureChildControls()
            _panel.BorderColor = value
        End Set
    End Property

    Protected Overrides Sub CreateChildControls()
        'Botón oculto
        Dim oc As New HtmlControls.HtmlInputButton()
        oc.ID = "btnOculto"
        oc.Style.Add(HtmlTextWriterStyle.Display, "none")
        Me.Controls.Add(oc)
        ' Panel Info
        _panel = New Panel()
        _panel.ID = "pntlTooltip"
        _panel.BorderStyle = BorderStyle.Solid
        _panel.BorderWidth = New Unit(1, UnitType.Pixel)
        _panel.Style.Add(HtmlTextWriterStyle.Display, "none")
        _panel.Style.Add(HtmlTextWriterStyle.Position, "absolute")
        _panel.Style.Add(HtmlTextWriterStyle.Overflow, "visible")
        _panel.Style.Add(HtmlTextWriterStyle.ZIndex, "11000")
        _panel.Attributes.Add("onmouseover", "FSNNoCerrarTooltips()")
        _label = New Label()
        _label.ID = "lblTooltip"
        _label.CssClass = "Normal"
        _panel.Controls.Add(_label)
        Me.Controls.Add(_panel)
        ' Modal Abrir
        _modalpopupextender = New AjaxControlToolkit.ModalPopupExtender()
        _modalpopupextender.ID = "pnlTooltip_mpe"
        _modalpopupextender.Enabled = True
        _modalpopupextender.TargetControlID = oc.ID
        _modalpopupextender.PopupControlID = _panel.ID
        _modalpopupextender.CancelControlID = _panel.ID
        Me.Controls.Add(_modalpopupextender)

    End Sub

    Private Sub FSNPanelTooltip_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Page.ClientScript.RegisterClientScriptResource(GetType(FSNPanelClientScript), "Fullstep.FSNWebControls.FSNPaneles.js")
    End Sub

    Private Sub FSNPanelTooltip_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        _panel.BackColor = BackColor
        _panel.BorderColor = BorderColor
        Dim sOnMouseout As String
        If Contenedor.Length > 0 Then
            sOnMouseout = "FSNCerrarTooltipsTemp('" & DevolverControl(Contenedor).ClientID & "')"
        Else
            sOnMouseout = "FSNCerrarTooltipsTemp()"
        End If
        If _panel.Attributes.Item("onmouseout") IsNot Nothing Then
            _panel.Attributes.Item("onmouseout") = sOnMouseout
        Else
            _panel.Attributes.Add("onmouseout", sOnMouseout)
        End If
    End Sub

    Private Function DevolverControl(ByVal Id As String) As Control
        Dim p As Control = Nothing
        Dim c As Control = Me
        Do While c IsNot Nothing
            p = c.FindControl(Id)
            If p IsNot Nothing Then
                Exit Do
            Else
                c = c.NamingContainer
            End If
        Loop
        Return p
    End Function

End Class
