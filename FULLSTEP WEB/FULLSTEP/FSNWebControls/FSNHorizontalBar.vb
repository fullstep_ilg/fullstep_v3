﻿Imports System.Web
Imports System.ComponentModel
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.IO
Imports System.Drawing
Imports System.Drawing.Drawing2D
Imports System.Drawing.Imaging

<Serializable()> _
Public Class BarSection

    Private _Color As Color
    Private _Valor As Double
    Private _Modo As FillMode

    Public Enum FillMode
        Solid = 0
        Gradient = 1
    End Enum

    Public Property FillColor() As Color
        Get
            Return _Color
        End Get
        Set(ByVal value As Color)
            _Color = value
        End Set
    End Property

    Public Property Value() As Double
        Get
            Return _Valor
        End Get
        Set(ByVal value As Double)
            _Valor = value
        End Set
    End Property

    Public Property FillStyle() As FillMode
        Get
            Return _Modo
        End Get
        Set(ByVal value As FillMode)
            _Modo = value
        End Set
    End Property

    ''' <summary>
    ''' Inicializa la clase
    ''' </summary>
    ''' <remarks>Llama al contructor con valores por defecto</remarks>
    Public Sub New()
        MyClass.New(Drawing.Color.Transparent, 0, FillMode.Solid)
    End Sub

    ''' <summary>
    ''' Inicializa la clase con los valores indicados
    ''' </summary>
    ''' <param name="ColorTramo">Color de la sección de la barra</param>
    ''' <param name="Valor">Valor con el que se calculará el ancho de la sección</param>
    ''' <param name="Modo">Modo de visualización de la sección (Sólido/Gradiente)</param>
    Public Sub New(ByVal ColorTramo As Color, ByVal Valor As Double, ByVal Modo As FillMode)
        _Color = ColorTramo
        _Valor = Valor
        _Modo = Modo
    End Sub

End Class

Public Class FSNHorizontalBar
    Inherits Label

    Private Const DifGradiente = 75

    <Browsable(True), Bindable(False), _
    Category("Appearance"), Description("Tramos en que se divide la barra")> _
    Public Property BarSections() As List(Of BarSection)
        Get
            Return CType(ViewState("TramosBarra"), List(Of BarSection))
        End Get
        Set(ByVal value As List(Of BarSection))
            ViewState("TramosBarra") = value
        End Set
    End Property

    ''' <summary>
    ''' Presenta el contenido del control en el sistema de escritura especificado. Este método lo usan principalmente los programadores de controles. 
    ''' </summary>
    ''' <param name="writer">HtmlTextWriter que representa la secuencia de salida para presentar contenido HTML en el cliente.</param>
    Protected Overrides Sub RenderContents(ByVal writer As System.Web.UI.HtmlTextWriter)
        Dim WebSource As String = GenerateNameAndRegisterForCall()
        writer.AddAttribute(HtmlTextWriterAttribute.Cellpadding, "2")
        writer.AddAttribute(HtmlTextWriterAttribute.Cellspacing, "0")
        writer.AddAttribute(HtmlTextWriterAttribute.Border, "0")
        writer.AddAttribute(HtmlTextWriterAttribute.Width, "100%")
        writer.AddAttribute(HtmlTextWriterAttribute.Height, MyBase.Height.ToString())
        writer.AddAttribute(HtmlTextWriterAttribute.Background, FSNBarImageStream.HandlerFileName & "?id=" & WebSource)
        writer.RenderBeginTag(HtmlTextWriterTag.Table)
        writer.RenderBeginTag(HtmlTextWriterTag.Tr)
        FSNLibrary.AddAtributoEstilo(writer, HtmlTextWriterStyle.PaddingLeft, "4px")
        writer.RenderBeginTag(HtmlTextWriterTag.Td)
        MyBase.RenderContents(writer)
        writer.RenderEndTag()
        writer.RenderEndTag()
        writer.RenderEndTag()
    End Sub

    ''' <summary>
    ''' Calcula la suma de los valores de las secciones
    ''' </summary>
    ''' <returns>Un Double con la suma de los valores de las secciones</returns>
    ''' <remarks>Se utilizará para calcular el ancho relativo de cada sección.</remarks>
    Private Function ValorTotal() As Double
        Dim d As Double = 0
        For Each t As BarSection In Me.BarSections
            d = d + t.Value
        Next
        Return d
    End Function

    ''' <summary>
    ''' Genera la imagen de la barra y la devuelve en un stream
    ''' </summary>
    ''' <returns>Objeto MemoryStream con la imagen de la barra</returns>
    ''' <remarks>Llamada desde el HTTPModule FSNBarImageStream</remarks>
    Public Function RenderImageBar() As MemoryStream
        Dim objBitMap As New Bitmap(CInt(Me.Width.Value), CInt(Me.Height.Value), PixelFormat.Format32bppArgb)
        Dim valIni As Integer = 0
        Dim valTotal As Double = ValorTotal()
        Dim valAcumulado As Double = 0
        Dim grap As Graphics = Graphics.FromImage(objBitMap)
        For Each t As BarSection In Me.BarSections
            valAcumulado = valAcumulado + t.Value
            Dim val As Integer = CInt(objBitMap.Width * valAcumulado / valTotal)
            Dim rect As New Rectangle(valIni, 0, val - valIni, objBitMap.Height)
            Dim br As Brush
            If t.FillStyle = BarSection.FillMode.Gradient Then
                Dim colgrad As Color = Color.FromArgb(IIf(t.FillColor.R + DifGradiente > 255, 255, t.FillColor.R + DifGradiente) _
                                                    , IIf(t.FillColor.G + DifGradiente > 255, 255, t.FillColor.G + DifGradiente) _
                                                    , IIf(t.FillColor.B + DifGradiente > 255, 255, t.FillColor.B + DifGradiente))
                Dim colgrad2 As Color = Color.FromArgb(IIf(t.FillColor.R - DifGradiente < 0, 0, t.FillColor.R - DifGradiente) _
                                                    , IIf(t.FillColor.G - DifGradiente < 0, 0, t.FillColor.G - DifGradiente) _
                                                    , IIf(t.FillColor.B - DifGradiente < 0, 0, t.FillColor.B - DifGradiente))
                br = New LinearGradientBrush(rect, colgrad, colgrad2, LinearGradientMode.Vertical)
            Else
                br = New SolidBrush(t.FillColor)
            End If
            grap.FillRectangle(br, valIni, 0, val - valIni, objBitMap.Height)
            valIni = val
        Next

        grap.DrawImage(objBitMap, 0, 0, objBitMap.Width, objBitMap.Height)

        Dim memStream As New MemoryStream()
        objBitMap.Save(memStream, ImageFormat.Png)
        objBitMap.Dispose()

        Return memStream
    End Function

    ''' <summary>
    ''' Genera un nombre para el control y lo registra a nivel de aplicación
    ''' </summary>
    ''' <returns>El nombre del control</returns>
    ''' <remarks>Llamada desde el método RenderContents</remarks>
    Function GenerateNameAndRegisterForCall() As String
        Dim sControlName As String = System.Guid.NewGuid.ToString
        Me.Page.Application(FSNBarImageStream.FSNBarNamePrefixInApplication & sControlName) = Me
        Return sControlName
    End Function

End Class
