﻿function OnTextBoxFocus(idTextBoxMultiIdioma, idiomaSeleccionado, onValueChange) {
    if(typeof(selectedTextBoxMultiIdioma)=='undefined') var selectedTextBoxMultiIdioma = {};
    selectedTextBoxMultiIdioma[idTextBoxMultiIdioma] = idiomaSeleccionado;

    if (!typeof (onValueChange) == 'undefined') {
        if (typeof (onValueChangeTextBoxMultiIdioma) == 'undefined') var onValueChangeTextBoxMultiIdioma = {};
        onValueChangeTextBoxMultiIdioma[idTextBoxMultiIdioma] = onValueChange;
    }

    $('#' + idTextBoxMultiIdioma).ready(function () {
        (function ($) {
            $.widget("ui.textBoxMultiIdioma", $.ui.mouse, {
                widgetEventPrefix: "textBoxMultiIdioma",
                options: {
                    items: null,
                    selectedLanguage: null,
                    defaultLanguage: null
                },
                destroy: function () {
                    this.items = null;

                    this._mouseDestroy();
                    $.Widget.prototype.destroy.call(this);
                },
                _create: function () {
                    this.id = this.element.context.id;
                    this.items = this.options.items;
                    this._selectedLanguage = this.options.selectedLanguage;
                    this.defaultLanguage = this.options.defaultLanguage;
                    this._selectorVisible = false;

                    $('#' + this.id + '_content').click($.proxy(this._click, this));
                    $('body').click($.proxy(this._windowClick, this));
                    $('#' + this.id).keydown($.proxy(this._tabPress, this, event));
                    $('#' + this.id).focus($.proxy(this._focused, this));
                    $('#FSNTextBoxMultiIdioma_option_txtDenominacion_IDI_' + this.getDefaultLanguage()).prependTo($('#FSNTextBoxMultiIdioma_options_txtDenominacion'));
                },
                _focused: function (e) {
                    this._showSelector();
                },
                _tabPress: function (e, event) {
                    var activeItem;
                    if (event.keyCode) code = event.keyCode;
                    else if (event.which) code = event.which;
                    if (code == 9) {
                        this._validateChanges();
                        if (event.shiftKey) activeItem = $('#FSNTextBoxMultiIdioma_option_txtDenominacion_IDI_' + this.getSelectedLanguage()).prev();
                        else activeItem = $('#FSNTextBoxMultiIdioma_option_txtDenominacion_IDI_' + this.getSelectedLanguage()).next();
                        if (activeItem.length == 0) this._hideSelector();
                        else {
                            event.preventDefault();
                            this.selectLanguage(activeItem.attr('id').split('_')[4]);
                        };
                    };
                },
                _click: function (e) {
                    var $target = $(e.target);
                    if ($target.is('#' + this.id)) {
                        if (this._selectorVisible) {
                            if ($('#FSNTextBoxMultiIdioma_options_' + this.id).is(':visible')) this.collapseDropDown();
                        } else {
                            this._validateChanges();
                            this._showSelector();
                        }
                        return false;
                    }
                    if ($target.is('#seleccionador_' + this.id) || $target.parents('#seleccionador_' + this.id).length == 1) {
                        this._validateChanges();
                        if (!$('#FSNTextBoxMultiIdioma_options_' + this.id).is(':visible')) this.expandDropDown();
                        else this.collapseDropDown();
                        return false;
                    }
                    if ($target.is('.fsComboItem') || $target.parents('.fsComboItem').length == 1) {
                        var li = $target.closest('li');
                        if (li.hasClass('.fsComboItemSelected')) return false;
                        this.selectLanguage(li.attr('id').split('_')[4]);
                        return false;
                    }
                },
                _windowClick: function (e) {
                    var $target = $(e.target);
                    if ($target.attr('id') && $target.attr('id').indexOf(this.id) > 0) {
                        return false;
                    } else {
                        this._validateChanges();
                        this.collapseDropDown();
                        this._hideSelector();

                    }
                },
                _showSelector: function () {
                    this._selectorVisible = true;
                    $('#seleccionador_' + this.id).show();
                },
                _hideSelector: function () {
                    this._selectorVisible = false;
                    $('#seleccionador_' + this.id).hide();
                },
                _validateChanges: function () {
                    if (JSON.stringify(this.items[this._selectedLanguage]) !== JSON.stringify($('#' + this.id).val())) {
                        this.items[this._selectedLanguage] = $('#' + this.id).val();
                        $('#FSNTextBoxMultiIdioma_option_txt_' + this.id + "_IDI_" + this._selectedLanguage).text($('#' + this.id).val());
                    }
                },
                getDefaultLanguage: function () {
                    return this.defaultLanguage;
                },
                getSelectedLanguage: function () {
                    return this._selectedLanguage;
                },
                getSelectedText: function () {
                    this.items[key];
                },
                getItem: function (key) {
                    return this.items[key];
                },
                getItems: function () {
                    this._validateChanges();
                    return this.items;
                },
                selectLanguage: function (key) {
                    this._validateChanges();

                    //Deselecciono la opción actualmente seleccionada y selecciono la nueva
                    $('#FSNTextBoxMultiIdioma_option_' + this.id + '_IDI_' + this._selectedLanguage).removeClass('fsComboItemSelected');
                    $('#FSNTextBoxMultiIdioma_option_' + this.id + '_IDI_' + key).addClass('fsComboItemSelected');

                    $('#' + this.id).val(this.items[key]);
                    $('#imgSelectedIdioma_' + this.id).attr('src', $('#FSNTextBoxMultiIdioma_option_img_' + this.id + '_IDI_' + key).attr('src'));
                    $('#FSNTextBoxMultiIdioma_options_' + this.id).hide();
                    this._selectedLanguage = key;
                },
                collapseDropDown: function () {
                    $('#FSNTextBoxMultiIdioma_options_' + this.id).hide();
                },
                expandDropDown: function () {
                    $('#FSNTextBoxMultiIdioma_options_' + this.id).show();
                },
                setItems: function (items) {
                    var controlId = this.id;
                    this.items = items;
                    $('#desplegable_' + controlId + ' ul').empty();
                    var li, span, img;
                    $.each(items, function (key, value) {
                        li = '<li id="FSNTextBoxMultiIdioma_option_' + controlId + '_IDI_' + key + '"';
                        li += ' class="fsComboItem' + (this._selectedLanguage == key ? ' fsComboItemSelected' : '') + '"';
                        li += ' style="float:left; width:100%; padding:0px!important">';
                        span = '<span id="FSNTextBoxMultiIdioma_option_txt_' + controlId + '_IDI_' + key + '"';
                        span += ' class="Texto12"' + ' style="margin-left:3px">';
                        li += span + value + '</span>';
                        img = '<img id="FSNTextBoxMultiIdioma_option_img_' + 'desplegable_' + controlId + '_IDI_' + key + '" src="<%=Resources.IDI_' + key + '%>" style="float:right"/>';
                        li += img + '</li>';
                        $('#desplegable_' + controlId + ' ul').append(li);
                    });
                    $('#' + controlId).val(items[this._selectedLanguage]);
                }
            });
        })(jQuery);
        $('#' + idTextBoxMultiIdioma).textBoxMultiIdioma({ items: itemsTextBoxMultiIdioma[idTextBoxMultiIdioma], selectedLanguage: idiomaSeleccionado, defaultLanguage: idiomaSeleccionado });
    });
}