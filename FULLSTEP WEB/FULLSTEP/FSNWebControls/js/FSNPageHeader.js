﻿$('.PageHeaderBackground').ready(function () {
    AjustarTamanyoTitulo();
});
$(window).resize(function () {
    AjustarTamanyoTitulo();
});
/*
''' <summary>
''' Ajustar el tamaÃ±o del titulo de un pantalla (ejemplo: DetalleNoConformidades.aspx) cuando tienes poco espacio por el
''' gran numero de botones q tambien van en la linea del titulo.
''' </summary>
''' <remarks>Llamada desde: FSNPageHeader.vb; Tiempo mÃ¡ximo:0</remarks>
*/
function AjustarTamanyoTitulo() {
    var wPaginador = $(this).width();
    var wBotones = 0;
    $.each($('.PageHeaderBackground .CabeceraBotonCustomControl'), function () {
        if (!$(this).is('span')) wBotones += $(this).outerWidth();
    });
    $('.LabelTituloCabeceraCustomControl').css('width', (((wPaginador - wBotones - 60) / wPaginador) * 100) + '%');
    $('.LabelTituloCabeceraCustomControl').show();
};
function FSNHeader_MostrarBoton(name) {
    $('.PageHeaderBackground [id$=lnk' + name + ']').show();
};
function FSNHeader_OcultarBoton(name) {
    $('.PageHeaderBackground [id$=lnk' + name + ']').hide();
};