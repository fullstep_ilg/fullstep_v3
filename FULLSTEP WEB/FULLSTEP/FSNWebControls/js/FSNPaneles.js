﻿//Capturamos la posición del ratón
document.onmousemove = getMousePosition;
var CoorX;
var CoorY;
//Guardamos la posición del ratón en dos variables globales
function getMousePosition(e) {
    //CALIDAD
    var isIE = document.all ? true : false;
    if (!isIE) {
        CoorX = e.pageX;
        CoorY = e.pageY;
    }
    if (isIE) {
        CoorX = event.clientX + (document.documentElement.scrollLeft || document.body.scrollLeft) - document.documentElement.clientLeft;
        CoorY = event.clientY + (document.documentElement.scrollTop || document.body.scrollTop) - document.documentElement.clientTop;
    }
}

FSNPanel = function(id, contenedor, tipo) {
    this.id = id;
    this.contenedor = contenedor;
    this.tipo = tipo;
}

FSNPanel.prototype = {
    hide: function() {
        FSNOcultarPanel(this.id, this.contenedor)
    }
}

FSNArrayPanels = function() {
    this.paneles = new Array();
}

FSNArrayPanels.prototype = {
    add: function(id, contenedor, tipo) {
        var panel = new FSNPanel(id, contenedor, tipo);
        this.paneles.push(panel);
    },

    remove: function(id, contenedor) {
        for (var i = this.paneles.length - 1; i >= 0; i--) {
            if (this.paneles[i].id == id && this.paneles[i].contenedor == contenedor)
                this.paneles.splice(i, 1);
        }
    },

    find: function(contenedor, tipo) {
        var arr = new FSNArrayPanels();
        for (var i = 0; i < this.paneles.length; i++) {
            if (this.paneles[i].contenedor == contenedor && (this.paneles[i].tipo==tipo || tipo==null))
                arr.add(this.paneles[i].id, contenedor, this.paneles[i].tipo);
        }
        return arr;
    },

    hide: function() {
        for (var i = 0; i < this.paneles.length; i++) {
            this.paneles[i].hide();
        }
    }
}

// This function returns Internet Explorer's major version number,
// or 0 for others. It works by finding the "MSIE " string and
// extracting the version number following the space, up to the decimal
// point, ignoring the minor version number
function MSIEVersion() {
    var ua = window.navigator.userAgent
    var msie = ua.indexOf("MSIE ")

    if (msie > 0)      // If Internet Explorer, return version number
        return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)))
    else                 // If another browser, return 0
        return 0

}

/*
''' <summary>
''' Mostrar un panel/modalpopup en un punto de pantalla
''' </summary>
''' <param name="elemento">objeto html q se muestra</param>
''' <param name="e">evento q desencadeno q se mostrara</param>        
''' <param name="contenedor">contenedor del objeto html</param>
''' <param name="CoordX">Coordenada x donde mostrar</param>    
''' <param name="CoordY">Coordenada y donde mostrar</param>
''' <param name="CoorfX">Coordenada x fija donde mostrar</param>    
''' <param name="CoorfY">Coordenada y fija donde mostrar</param>
''' <remarks>Llamada desde: FSNMostrarPanel      FSNMoverTargetsAnimation; Tiempo maximo: 0</remarks>*/
function FSNMoverAMouse(elemento, e, contenedor, CoordX, CoordY, CoorfX, CoorfY) {
    var x, y;
    //Si le paso las coordenadas fijas no recalculará la posición del ratón.
    //Fijo la posición con estas coordenadas en una posición absoluta respecto al contenedor que se le haya indicado.
    if (CoorfX != null && CoorfY != null) {
        x = CoorfX;
        y = CoorfY;
    } else {
        if (contenedor == null) {
            if (CoordX != null && CoordY != null) {
                x = $get(elemento._popupElement.id).parentNode ? restarPosicionParentsRecursivo($get(elemento._popupElement.id).parentNode.id, 'x', false) : CoordX;
                y = $get(elemento._popupElement.id).parentNode ? restarPosicionParentsRecursivo($get(elemento._popupElement.id).parentNode.id, 'y', false) : CoordY;
            } else {
                // Calcula la posición en referencia a la página
                //CALIDAD
                x = (document.all && document.getElementById) ? Math.max(document.body.scrollLeft, document.documentElement.scrollLeft) + event.clientX : e.get_browserEvent().clientX + window.pageXOffset;
                //CALIDAD
                y = (document.all && document.getElementById) ? Math.max(document.body.scrollTop, document.documentElement.scrollTop) + event.clientY : e.get_browserEvent().clientY + window.pageYOffset;
            }
        }
        else {
            // Calcula la posición en referencia al control contenedor
            //CALIDAD
            x = (document.all && document.getElementById) ? $get(contenedor).scrollLeft + event.clientX - $get(contenedor).offsetLeft : $get(contenedor).scrollLeft + e.get_browserEvent().clientX - $get(contenedor).offsetLeft;
            //CALIDAD
            y = (document.all && document.getElementById) ? $get(contenedor).scrollTop + event.clientY - $get(contenedor).offsetTop : $get(contenedor).scrollTop + e.get_browserEvent().clientY - $get(contenedor).offsetTop;
        }
    } 
    
    if (elemento.get_name && elemento.get_name() == 'ModalPopupBehavior') {
        // Es un modalPopup
        //Si no se asignan la X y la Y, el panel a mostrar se muestra centrado. Se centra en el momento en que se ejecuta la instruccion show
        //Por ese motivo, asignamos la X y la Y, aunque luego vamos a sobreescribir los valores de top y left del panel a mostrar (oPopupControlID)
        //porque un bug del ModalPopupExtender hace que se calcule mal la posicion del panel cuando hay un CollapsiblePanelExtender
        elemento.set_X(x);
        if (MSIEVersion() == 6) { //Si es IE6 calculamos la coordenada y de manera especifica para que no influya el scrollbar
            elemento.set_Y(e.clientY);
        } else {
            elemento.set_Y(y);
        }
        //reasignamos el top y el left del panel que se va a mostrar.
        var oPopupControlID = $get(elemento._PopupControlID)
        oPopupControlID.style.position = 'absolute';
        oPopupControlID.style.top = y + 'px';
        oPopupControlID.style.left = x + 'px';
    }
    else {
        // Es un panel
        elemento.style.position = 'absolute';
        elemento.style.top = y + 'px';
        elemento.style.left = x + 'px';
    }
}
/*
Funcion Recursiva
La posicion del raton se determina respecto a la ventana.
Sin embargo, en ocasiones, la posicion del panel que vamos a mostrar depende de otro div y no de la ventana (casuistica del DetArticuloCatalogo)
de modo que hay que restar el exceso a la izquierda y arriba.
Lo haremos de forma recursiva, subiendo niveles a traves del DOM
parentNodeID: Id del control que contiene el control
eje: eje del que queremos calcular
hayContenedoresFixed: Esta funcion se usa para calcular el posicionamiento de todos los paneles, tanto lo que cuelgan directamente del document como los que cuelgan de otros paneles con position fixed.
    Para restar el scrollTop y LEft del document.documentElement hay que asegurarse que venimos de un panel con contenedores fixed. Si se resta a los que cuelgan del document, los posicionaria mal.
*/
function restarPosicionParentsRecursivo(parentNodeID, eje, hayContenedoresFixed) {
    if (parentNodeID != "") {
        //Exceso relativo a los paneles contenedores con position fixed
        var parentNode = $get(parentNodeID);
        if (parentNode) {
            if (parentNode.style.position == 'fixed') {
                if (eje == 'x') {
                    var izq = parentNode.style.left.replace(/px/g, "");
                    CoorX -= izq;
                    hayContenedoresFixed = true;
                }
                else {
                    var arr = parentNode.style.top.replace(/px/g, "");
                    CoorY -= arr;
                    hayContenedoresFixed = true;
                }
            }
        }
        if ($get(parentNodeID).parentNode) {
            restarPosicionParentsRecursivo($get(parentNodeID).parentNode.id, eje, hayContenedoresFixed);
        }
    }
    else {
        //Exceso relativo al scroll del control html
        if (document.documentElement && hayContenedoresFixed) {
            if (eje == 'x') {
                //var izq = document.documentElement.scrollLeft;
                var izq = $(document).scrollLeft();
                CoorX -= izq;
            }
            else {
                //var arr = document.documentElement.scrollTop;
                var arr = $(document).scrollTop();
                CoorY -= arr;
            }
        }
    }
    if (eje == 'x') {
        return CoorX;
    }
    else {
        return CoorY;
    }
}

function FSNMostrarPanel(elementoid, e, dynpopulateid, contextkey, contenedor, tipo, CoordX, CoordY) {
    FSNCerrarPaneles(contenedor, tipo);
    if (dynpopulateid != null) {
        var dynpopulate = $find(dynpopulateid);
        if (contextkey != null)
            dynpopulate.set_ContextKey(contextkey);
        setTimeout('FSNActualizarDatos(\'' + dynpopulateid + '\')', 0);
    }
    var elemento = $find(elementoid);
    if (elemento == null) {
        elemento = $get(elementoid);
        FSNMoverAMouse(elemento, e, CoordX, CoordY)
    }
    else {
        switch (elemento.get_name()) {
            case 'AnimationBehavior':
                FSNOcultarTargetsAnimation(elemento.get_OnClickBehavior().get_animation(), e);
                FSNMoverTargetsAnimation(elemento.get_OnClickBehavior().get_animation(), e, contenedor, CoordX, CoordY);
                if (tipo == 'tooltip') {
                    var panel = elemento.get_OnClickBehavior().get_animation().get_target();
                    panel.style.width = '';
                    panel.style.minWidth = '100px';
                    var panleft = getPosLeft(panel);
                    if (panleft == 0) {
                        if (contenedor == null) {
                            // Calcula la posición en referencia a la página
                            //CALIDAD
                            panleft = (document.all && document.getElementById) ? Math.max(document.body.scrollLeft, document.documentElement.scrollLeft) + event.clientX : e.clientX + window.pageXOffset;
                        }
                        else {
                            // Calcula la posición en referencia al control contenedor
                            //CALIDAD
                            panleft = (document.all && document.getElementById) ? $get(contenedor).scrollLeft + event.clientX - $get(contenedor).offsetLeft : $get(contenedor).scrollLeft + e.clientX - $get(contenedor).offsetLeft;
                        }
                    }
                    var espacio = document.body.offsetWidth - panleft;
                    if (espacio < 110) {
                        panel.style.left = (panleft - 110 + espacio) + 'px';
                        panleft = panleft - 110 + espacio;
                        espacio = 110;
                    }
                    panel.style.maxWidth = espacio + 'px';
                }
                elemento.get_OnClickBehavior().play();
                break;
            case 'ModalPopupBehavior':
                var oContenedor = null;
                if (CoordX == null) {
                    //Usamos las variables globales que han guardado la posición del ratón para colocar el ModalPopup
                    FSNMoverAMouse(elemento, e, oContenedor, CoorX, CoorY)
                } else {
                    //Si le mando coordenadas al hacer la llamada a FSNMostrarPanel aunque sea un ModalPopUp le digo que los use
                    FSNMoverAMouse(elemento, e, oContenedor, CoorX, CoorY, CoordX, CoordY)
                }
                //Establecer Z-index por encima de cualquier otro control
                var zIndex = 220000;
                if (elemento._backgroundElement)
                    elemento._backgroundElement.style.zIndex = zIndex;
                if (elemento._foregroundElement)
                    elemento._foregroundElement.style.zIndex = zIndex + 1;
                //El contextkey indica la información que debe cargarse en el panel
                //No mostrar el panel cuando contextkey ='' pero sí cuando sea undefined (porque cuando no se pasa contextkey, el panel carga datos de otro modo)
                if (contextkey != undefined) {
                    if (contextkey != '')
                        elemento.show();
                }
                else {
                    elemento.show();
                }
                break;
        }
        //Fix para evitar el bug de IE6 que hace que los select aparezcan sobre cualquier otro control en pantalla
        if (elementoid.indexOf('FSNPanelDatosProveCab_pnlInfo') > 0) {
            var panelInfoClientID = elementoid.substr(0, elementoid.indexOf('FSNPanelDatosProveCab_pnlInfo') + 29);
            var panelInfoFrameClientID = panelInfoClientID + 'Frame';
            if ($get(panelInfoFrameClientID) != null) {
                var iframe = $get(panelInfoFrameClientID);
                var layer = document.getElementById(panelInfoClientID);
                iframe.style.display = 'block';
                iframe.style.width = layer.style.pixelWidth + 'px';
                iframe.style.height = (layer.style.pixelHeight - 40) + 'px';
                iframe.style.left = layer.offsetLeft;
                iframe.style.top = layer.offsetTop;
                iframe.style.zIndex = 10;
            }
        }
    }
    FSNPanelesVisibles.add(elementoid, contenedor, tipo);
    FSNNoCerrarPaneles();
}

function FSNOcultarPanel(elementoid, contenedor) {
    var elemento = $find(elementoid);
    if (elemento == null) {
        elemento = $get(elementoid);
        elemento.style.display = 'none';
    }
    else {
        switch (elemento.get_name()) {
            case 'AnimationBehavior':
                FSNOcultarTargetsAnimation(elemento.get_OnClickBehavior().get_animation(), null);
                break;
            case 'ModalPopupBehavior':
                elemento.hide();
                break;
        }
    }
    //Fix para evitar el bug de IE6 que hace que los select aparezcan sobre cualquier otro control en pantalla
    if (elementoid.indexOf('FSNPanelDatosProveCab_pnlInfo') > 0) {
        var panelInfoFrameClientID = elementoid.substr(0, elementoid.indexOf('FSNPanelDatosProveCab_pnlInfo')) + 'Frame';
        if ($get(panelInfoFrameClientID) != null) {
            var iframe = $get(panelInfoFrameClientID);
            iframe.style.display = 'none';
        }
    }
}

var FSNtmpCerrarPaneles;
var FSNtmpCerrarTooltips;

function FSNCerrarPaneles(contenedor, tipo) {
    switch (tipo) {
        case 'tooltip':
            window.clearTimeout(FSNtmpCerrarTooltips);
            break;
        default:
            window.clearTimeout(FSNtmpCerrarPaneles);
    }
    var arr = FSNPanelesVisibles.find(contenedor, tipo);
    arr.hide();
}

function FSNCerrarPanelesTemp(contenedor) {
    if (contenedor == null)
        FSNtmpCerrarPaneles = window.setTimeout('FSNCerrarPaneles()', 1200)
    else
        FSNtmpCerrarPaneles = window.setTimeout('FSNCerrarPaneles(\'' + contenedor + '\')', 1200)
}

function FSNCerrarTooltipsTemp(contenedor) {
    if (contenedor == null)
        FSNtmpCerrarTooltips = window.setTimeout('FSNCerrarPaneles(null,\'tooltip\')', 1200)
    else
        FSNtmpCerrarTooltips = window.setTimeout('FSNCerrarPaneles(\'' + contenedor + '\', \'tooltip\')', 1200)
}

function FSNNoCerrarPaneles() {
    window.clearTimeout(FSNtmpCerrarPaneles);
}

function FSNNoCerrarTooltips() {
    window.clearTimeout(FSNtmpCerrarTooltips);
}

function FSNActualizarDatos(dynpopulateid) {
    $find(dynpopulateid).populate();
}

function FSNOcultarTargetsAnimation(animation, e) {
    if (animation.get_target() != null && animation.get_animations == null) {
        animation.get_target().style.display = 'none';
    }
    if (animation.get_animations) {
        for (var ani in animation.get_animations()) {
            FSNOcultarTargetsAnimation(animation.get_animations()[ani], e);
        }
    }
}

function FSNMoverTargetsAnimation(animation, e, contenedor,CoordX,CoordY) {
    if (animation.get_target() != null) {
        if (animation.get_target().style.position == 'absolute' && animation.get_target().tagName == 'DIV')
            FSNMoverAMouse(animation.get_target(), e, contenedor, CoordX, CoordY);
    }
    if (animation.get_animations) {
        for (var ani in animation.get_animations()) {
            FSNMoverTargetsAnimation(animation.get_animations()[ani], e, contenedor, CoordX, CoordY);
        }
    }
}

function getPosLeft(obj) {
    var curleft = 0;
    if (obj.offsetParent)
        while (1) {
            curleft += obj.offsetLeft;
            if (!obj.offsetParent)
                break;
            obj = obj.offsetParent;
        }
    else
        if (obj.x)
            curleft += obj.x;
    return curleft; 
}

var FSNPanelesVisibles = new FSNArrayPanels();
var FSNTooltips = new FSNArrayPanels();