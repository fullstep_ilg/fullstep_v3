﻿Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Drawing.Design
Imports System.Text
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.Design
Imports System.Web.UI.WebControls

Public Class FSNTab
    Inherits LinkButton

    <Browsable(True), Bindable(True), _
        Category("Colores"), Description("Imágenes para los bordes"), _
        Editor(GetType(ImageUrlEditor), GetType(UITypeEditor)), UrlProperty(), _
        Themeable(True)> _
    Public Property ImagenesBordes() As String
        Get
            Dim s As String = CStr(ViewState("ImagenesBordes"))
            If s Is Nothing Then s = String.Empty
            Return s
        End Get
        Set(ByVal value As String)
            ViewState("ImagenesBordes") = value
        End Set
    End Property
    Public Overrides Property Enabled() As Boolean
        Get
            Return True
        End Get
        Set(ByVal value As Boolean)
            MyBase.Enabled = value
            If (Not value) Then
                If LCase(Left(Me.OnClientClick, 12)) <> "return false" Then
                    Me.OnClientClick = "return false;" + Me.OnClientClick
                End If
                Me.Attributes.Add("onMouseOver", "return false;" + Me.Attributes.Item("onMouseOver"))
                Me.Attributes.Add("actColor", System.Drawing.ColorTranslator.ToHtml(MyBase.ForeColor))
                MyBase.ForeColor = Drawing.Color.LightGray
            End If
        End Set
    End Property
    <Browsable(True), Bindable(False), _
        Category("Colores"), Description("Color de Fondo del tab"), _
        TypeConverter(GetType(WebColorConverter)), _
        Themeable(True)> _
    Public Property ColorFondo() As System.Drawing.Color
        Get
            Return MyBase.BackColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            MyBase.BackColor = value
        End Set
    End Property
    <Browsable(True), Bindable(False), _
            Category("Colores"), Description("Color de Fondo del tab cuando el cursor se posa encima"), _
            TypeConverter(GetType(WebColorConverter)), _
            Themeable(True)> _
        Public Property ColorFondoHover() As System.Drawing.Color
        Get
            Return CType(ViewState("ColorFondoHover"), System.Drawing.Color)
        End Get
        Set(ByVal value As System.Drawing.Color)
            ViewState("ColorFondoHover") = value
        End Set
    End Property
    <Browsable(True), Bindable(False), _
        Category("Colores"), Description("Color de letra del tab"), _
        TypeConverter(GetType(WebColorConverter)), _
        Themeable(True)> _
        Public Property ColorLetra() As System.Drawing.Color
        Get
            Return MyBase.ForeColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            MyBase.ForeColor = value
        End Set
    End Property
    <Browsable(True), Bindable(False), _
        Category("Colores"), Description("Color de letra del tab cuando el cursor se posa encima"), _
        TypeConverter(GetType(WebColorConverter)), _
        Themeable(True)> _
        Public Property ColorLetraHover() As System.Drawing.Color
        Get
            Return CType(ViewState("ColorLetraHover"), System.Drawing.Color)
        End Get
        Set(ByVal value As System.Drawing.Color)
            ViewState("ColorLetraHover") = value
        End Set
    End Property
    <Browsable(True), Bindable(False), _
        Category("Appearance"), Description("Alineación del botón"), DefaultValue(Fullstep.FSNWebControls.FSNTipos.Alineacion.Right)> _
        Public Property Alineacion() As Fullstep.FSNWebControls.FSNTipos.Alineacion
        Get
            Dim s As Fullstep.FSNWebControls.FSNTipos.Alineacion = ViewState("Alineacion")
            Return s
        End Get
        Set(ByVal value As Fullstep.FSNWebControls.FSNTipos.Alineacion)
            ViewState("Alineacion") = value
        End Set
    End Property
    <Browsable(True), Bindable(True), _
    Category("Behavior"), Description("Indica si es el elemento seleccionado"), DefaultValue(False)> _
        Public Property Selected() As Boolean
        Get
            Return ViewState("Selected")
        End Get
        Set(ByVal value As Boolean)
            ViewState("Selected") = value
        End Set
    End Property
    ''' Revisado por: blp. Fecha: 06/06/2012
    ''' <summary>
    ''' Agrega los atributos del control LinkButton a la secuencia de salida para representar en el cliente.
    ''' </summary>
    ''' <param name="writer">Objeto System.Web.UI.HtmlTextWriter que contiene la secuencia de salida que se va a representar en el cliente.</param>
    ''' <remarks>Llamada desde render. Máx. 0,3 seg.</remarks>
    Protected Overrides Sub AddAttributesToRender(ByVal writer As HtmlTextWriter)
        MyBase.AddAttributesToRender(writer)
        Dim sBrowser As String = ""
        Dim Version As Integer
        Dim arBrowsers As New ArrayList
        If Not Me.DesignMode Then
            sBrowser = HttpContext.Current.Request.Browser.Browser
            Version = HttpContext.Current.Request.Browser.MajorVersion
            arBrowsers = HttpContext.Current.Request.Browser.Browsers
        End If
        FSNLibrary.AddAtributoEstilo(writer, "float", IIf(Alineacion = FSNTipos.Alineacion.Left, "left", "right"))
        FSNLibrary.AddAtributoEstilo(writer, HtmlTextWriterStyle.TextDecoration, "none")
        FSNLibrary.AddAtributoEstilo(writer, IIf(Alineacion = FSNTipos.Alineacion.Left, HtmlTextWriterStyle.MarginLeft, HtmlTextWriterStyle.MarginRight), "5px")
        If sBrowser <> "IE" Or Version < 7 Then
            If Me.BorderStyle = UI.WebControls.BorderStyle.NotSet Then
                FSNLibrary.AddAtributoEstilo(writer, HtmlTextWriterStyle.BorderStyle, UI.WebControls.BorderStyle.None.ToString())
            Else
                FSNLibrary.AddAtributoEstilo(writer, HtmlTextWriterStyle.BorderStyle, Me.BorderStyle.ToString())
                FSNLibrary.AddAtributoEstilo(writer, "border-bottom-style", UI.WebControls.BorderStyle.None.ToString())
            End If
            If Me.BorderColor.IsEmpty Then
                FSNLibrary.AddAtributoEstilo(writer, HtmlTextWriterStyle.BorderColor, "inherit")
            Else
                FSNLibrary.AddAtributoEstilo(writer, HtmlTextWriterStyle.BorderColor, Drawing.ColorTranslator.ToHtml(Me.BorderColor))
            End If
            If Me.BorderWidth.IsEmpty Then
                FSNLibrary.AddAtributoEstilo(writer, HtmlTextWriterStyle.BorderWidth, "0px")
            Else
                FSNLibrary.AddAtributoEstilo(writer, HtmlTextWriterStyle.BorderWidth, Me.BorderWidth.ToString())
            End If
            FSNLibrary.AddAtributoEstilo(writer, HtmlTextWriterStyle.TextAlign, "center")
            FSNLibrary.AddAtributoEstilo(writer, HtmlTextWriterStyle.Padding, "6px 5px 6px 5px")
            If arBrowsers.Contains("mozilla") Or arBrowsers.Contains("gecko") Then
                FSNLibrary.AddAtributoEstilo(writer, "-moz-border-radius-topleft", "4px")
                FSNLibrary.AddAtributoEstilo(writer, "-moz-border-radius-topright", "4px")
            End If
            If arBrowsers.Contains("safari") Then
                FSNLibrary.AddAtributoEstilo(writer, "-webkit-border-top-left-radius", "4px")
                FSNLibrary.AddAtributoEstilo(writer, "-webkit-border-top-right-radius", "4px")
            End If
            If arBrowsers.Contains("khtml") Then
                FSNLibrary.AddAtributoEstilo(writer, "-khtml-border-top-left-radius", "4px")
                FSNLibrary.AddAtributoEstilo(writer, "-khtml-border-top-right-radius", "4px")
            End If
            FSNLibrary.AddAtributoEstilo(writer, "border-top-left-radius", "4px")
            FSNLibrary.AddAtributoEstilo(writer, "border-top-right-radius", "4px")
        Else
            FSNLibrary.AddAtributoEstilo(writer, HtmlTextWriterStyle.BorderStyle, UI.WebControls.BorderStyle.None.ToString())
            FSNLibrary.AddAtributoEstilo(writer, HtmlTextWriterStyle.PaddingBottom, "6px")
        End If
        If Not Me.DesignMode Then
            If Not Selected Then
                writer.AddAttribute("onmouseover", "fsntab_over(this,'" & Drawing.ColorTranslator.ToHtml(Me.ColorFondoHover) & "','" & Drawing.ColorTranslator.ToHtml(Me.ColorLetraHover) & "')")
                writer.AddAttribute("onmouseout", "fsntab_out(this,'" & Drawing.ColorTranslator.ToHtml(Me.ColorFondo) & "','" & Drawing.ColorTranslator.ToHtml(Me.ColorLetra) & "')")
            Else
                FSNLibrary.AddAtributoEstilo(writer, HtmlTextWriterStyle.BackgroundColor, Drawing.ColorTranslator.ToHtml(ColorFondoHover))
                FSNLibrary.AddAtributoEstilo(writer, HtmlTextWriterStyle.Color, Drawing.ColorTranslator.ToHtml(Me.ColorLetraHover))
            End If
            writer.AddAttribute("onmouseenter", "this.setCapture()")
            writer.AddAttribute("onmouseleave", "this.releaseCapture()")
            writer.AddAttribute(HtmlTextWriterAttribute.Onclick, "this.blur();")
            writer.AddAttribute("onfocus", "this.blur();")
        End If
    End Sub
    ''' <summary>
    ''' Presenta el contenido del control en el sistema de escritura especificado. Este método lo usan principalmente los programadores de controles. 
    ''' </summary>
    ''' <param name="writer">HtmlTextWriter que representa la secuencia de salida para presentar contenido HTML en el cliente.</param>
    Protected Overrides Sub RenderContents(ByVal writer As HtmlTextWriter)
        Dim sBrowser As String = ""
        Dim Version As Integer
        Dim arBrowsers As New ArrayList
        If Not Me.DesignMode Then
            sBrowser = HttpContext.Current.Request.Browser.Browser
            Version = HttpContext.Current.Request.Browser.MajorVersion
            arBrowsers = HttpContext.Current.Request.Browser.Browsers
        End If
        If Me.DesignMode Or sBrowser = "IE" And Version > 6 Then
            ' Tabla
            writer.AddAttribute(HtmlTextWriterAttribute.Cellpadding, "0")
            writer.AddAttribute(HtmlTextWriterAttribute.Cellspacing, "0")
            writer.AddAttribute(HtmlTextWriterAttribute.Border, "0")
            FSNLibrary.AddAtributoEstilo(writer, HtmlTextWriterStyle.BackgroundColor, Drawing.ColorTranslator.ToHtml(IIf(Me.Selected, Me.ColorFondoHover, Me.ColorFondo)))
            FSNLibrary.AddAtributoEstilo(writer, HtmlTextWriterStyle.Cursor, "pointer")
            FSNLibrary.AddAtributoEstilo(writer, HtmlTextWriterStyle.Color, Drawing.ColorTranslator.ToHtml(IIf(Me.Selected, Me.ColorLetraHover, Me.ColorLetra)))
            FSNLibrary.AddAtributoEstilo(writer, HtmlTextWriterStyle.TextDecoration, "none")
            If Not Me.Width.IsEmpty Then _
                FSNLibrary.AddAtributoEstilo(writer, HtmlTextWriterStyle.Width, "100%")
            If Not Me.Height.IsEmpty And Me.Height.Type = UnitType.Pixel Then _
                FSNLibrary.AddAtributoEstilo(writer, HtmlTextWriterStyle.Height, Me.Height.ToString())
            writer.RenderBeginTag(HtmlTextWriterTag.Table)
            ' Primera fila
            FSNLibrary.AddAtributoEstilo(writer, HtmlTextWriterStyle.Height, "6px")
            writer.RenderBeginTag(HtmlTextWriterTag.Tr)
            ' Celda top left
            If CStr(ViewState("ImagenesBordes")) <> String.Empty Then _
                FSNLibrary.AddAtributoEstilo(writer, HtmlTextWriterStyle.BackgroundImage, Replace(CStr(ViewState("ImagenesBordes")), "~/", HttpContext.Current.Request.ApplicationPath & "/"))
            FSNLibrary.AddAtributoEstilo(writer, "background-position", "0px " & IIf(Me.Selected, "-12", "0") & "px")
            writer.RenderBeginTag(HtmlTextWriterTag.Td)
            writer.AddAttribute(HtmlTextWriterAttribute.Width, "6")
            writer.AddAttribute(HtmlTextWriterAttribute.Height, "6")
            writer.AddAttribute(HtmlTextWriterAttribute.Border, "0")
            writer.AddAttribute(HtmlTextWriterAttribute.Src, Me.Page.ClientScript.GetWebResourceUrl(GetType(FSNTab), "Fullstep.FSNWebControls.trans.gif"))
            writer.RenderBeginTag(HtmlTextWriterTag.Img)
            writer.RenderEndTag()
            writer.RenderEndTag()
            ' Celda top middle
            writer.AddAttribute(HtmlTextWriterAttribute.Rowspan, "2")
            FSNLibrary.AddAtributoEstilo(writer, HtmlTextWriterStyle.Padding, "6px 9px 0px 9px")
            writer.AddAttribute(HtmlTextWriterAttribute.Nowrap, "true")
            FSNLibrary.AddAtributoEstilo(writer, HtmlTextWriterStyle.TextAlign, "center")
            If Not Me.Width.IsEmpty Then _
                FSNLibrary.AddAtributoEstilo(writer, HtmlTextWriterStyle.Width, "100%")
            If Not Me.Height.IsEmpty And Me.Height.Type = UnitType.Pixel Then _
                FSNLibrary.AddAtributoEstilo(writer, HtmlTextWriterStyle.Height, Me.Height.ToString())
            If Not Me.Font.Size.IsEmpty Then _
                FSNLibrary.AddAtributoEstilo(writer, HtmlTextWriterStyle.FontSize, Font.Size.ToString())
            If Not Me.BorderStyle = UI.WebControls.BorderStyle.NotSet Then _
                FSNLibrary.AddAtributoEstilo(writer, "border-top-style", Me.BorderStyle.ToString())
            If Not Me.BorderColor.IsEmpty() Then _
                FSNLibrary.AddAtributoEstilo(writer, "border-top-color", Drawing.ColorTranslator.ToHtml(Me.BorderColor))
            If Not Me.BorderWidth.IsEmpty() Then _
                FSNLibrary.AddAtributoEstilo(writer, "border-top-width", Me.BorderWidth.ToString())
            writer.RenderBeginTag(HtmlTextWriterTag.Td)
            writer.Write(Me.Text)
            writer.RenderEndTag()
            ' Celda top right
            If CStr(ViewState("ImagenesBordes")) <> String.Empty Then _
                FSNLibrary.AddAtributoEstilo(writer, HtmlTextWriterStyle.BackgroundImage, Replace(CStr(ViewState("ImagenesBordes")), "~/", HttpContext.Current.Request.ApplicationPath & "/"))
            FSNLibrary.AddAtributoEstilo(writer, "background-position", "-6px " & IIf(Me.Selected, "-12", "0") & "px")
            writer.RenderBeginTag(HtmlTextWriterTag.Td)
            writer.AddAttribute(HtmlTextWriterAttribute.Width, "6")
            writer.AddAttribute(HtmlTextWriterAttribute.Height, "6")
            writer.AddAttribute(HtmlTextWriterAttribute.Border, "0")
            writer.AddAttribute(HtmlTextWriterAttribute.Src, Me.Page.ClientScript.GetWebResourceUrl(GetType(FSNTab), "Fullstep.FSNWebControls.trans.gif"))
            writer.RenderBeginTag(HtmlTextWriterTag.Img)
            writer.RenderEndTag()
            writer.RenderEndTag()
            writer.RenderEndTag()
            ' Segunda fila
            If Not Me.Height.IsEmpty And Me.Height.Type = UnitType.Pixel Then
                FSNLibrary.AddAtributoEstilo(writer, HtmlTextWriterStyle.Height, (Me.Height.Value - 6).ToString() & "px")
            Else
                FSNLibrary.AddAtributoEstilo(writer, HtmlTextWriterStyle.Height, "14px")
            End If
            writer.RenderBeginTag(HtmlTextWriterTag.Tr)
            If Not Me.BorderStyle = UI.WebControls.BorderStyle.NotSet Then _
                FSNLibrary.AddAtributoEstilo(writer, "border-left-style", Me.BorderStyle.ToString())
            If Not Me.BorderColor.IsEmpty() Then _
                FSNLibrary.AddAtributoEstilo(writer, "border-left-color", Drawing.ColorTranslator.ToHtml(Me.BorderColor))
            If Not Me.BorderWidth.IsEmpty() Then _
                FSNLibrary.AddAtributoEstilo(writer, "border-left-width", Me.BorderWidth.ToString())
            writer.RenderBeginTag(HtmlTextWriterTag.Td)
            writer.AddAttribute(HtmlTextWriterAttribute.Width, "6")
            writer.AddAttribute(HtmlTextWriterAttribute.Height, "1")
            writer.AddAttribute(HtmlTextWriterAttribute.Border, "0")
            writer.AddAttribute(HtmlTextWriterAttribute.Src, Me.Page.ClientScript.GetWebResourceUrl(GetType(FSNTab), "Fullstep.FSNWebControls.trans.gif"))
            writer.RenderBeginTag(HtmlTextWriterTag.Img)
            writer.RenderEndTag()
            writer.RenderEndTag()
            If Not Me.BorderStyle = UI.WebControls.BorderStyle.NotSet Then _
                FSNLibrary.AddAtributoEstilo(writer, "border-right-style", Me.BorderStyle.ToString())
            If Not Me.BorderColor.IsEmpty() Then _
                FSNLibrary.AddAtributoEstilo(writer, "border-right-color", Drawing.ColorTranslator.ToHtml(Me.BorderColor))
            If Not Me.BorderWidth.IsEmpty() Then _
                FSNLibrary.AddAtributoEstilo(writer, "border-right-width", Me.BorderWidth.ToString())
            writer.RenderBeginTag(HtmlTextWriterTag.Td)
            writer.AddAttribute(HtmlTextWriterAttribute.Width, "6")
            writer.AddAttribute(HtmlTextWriterAttribute.Height, "1")
            writer.AddAttribute(HtmlTextWriterAttribute.Border, "0")
            writer.AddAttribute(HtmlTextWriterAttribute.Src, Me.Page.ClientScript.GetWebResourceUrl(GetType(FSNTab), "Fullstep.FSNWebControls.trans.gif"))
            writer.RenderBeginTag(HtmlTextWriterTag.Img)
            writer.RenderEndTag()
            writer.RenderEndTag()
            writer.RenderEndTag()
            writer.RenderEndTag()
        Else
            If Me.Width.IsEmpty Then
                writer.Write("<span style=""padding:0px 10px 0px 10px;"">" & Text & "</span>")
            Else
                writer.Write(Text)
            End If
        End If
    End Sub
    ''' <summary>
    ''' Inicializa el tamaño de letra del control.
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub New()
        MyBase.New()
        MyBase.Font.Size = New FontUnit(12, UnitType.Pixel)
    End Sub
    ''' <summary>
    ''' Si no existen genera las funciones javascript para los efectos del FSNButton
    ''' </summary>
    ''' <param name="e">Objeto EventArgs que contiene los datos del evento.</param>
    Protected Overrides Sub OnPreRender(ByVal e As System.EventArgs)
        MyBase.OnPreRender(e)
        Dim sBrowser As String = ""
        Dim Version As Integer
        Dim arBrowsers As New ArrayList
        If Not Me.DesignMode Then
            sBrowser = HttpContext.Current.Request.Browser.Browser
            Version = HttpContext.Current.Request.Browser.MajorVersion
            arBrowsers = HttpContext.Current.Request.Browser.Browsers
        End If
        ' Scripts
        If Not Me.DesignMode Then
            If Not Me.Page.ClientScript.IsClientScriptBlockRegistered(Me.Page.GetType(), "fsntab_scripts") Then
                Dim sScript As String = "function fsntab_over(boton, color, colorletra) {" & vbCrLf & _
                    "boton.style.backgroundColor = color;" & vbCrLf & _
                    "boton.style.color = colorletra;" & vbCrLf
                If sBrowser = "IE" And Version > 6 Then
                    sScript = sScript & "var tabla = boton.childNodes[0];" & vbCrLf & _
                    "tabla.style.backgroundColor = color;" & vbCrLf & _
                    "tabla.style.color = colorletra;" & vbCrLf & _
                    "tabla.rows[0].cells[0].style.backgroundPosition = '0px -12px';" & vbCrLf & _
                    "tabla.rows[0].cells[2].style.backgroundPosition = '-6px -12px';" & vbCrLf
                End If
                sScript = sScript & "};" & vbCrLf & _
                    "function fsntab_out(boton, color, colorletra) {" & vbCrLf & _
                    "boton.style.backgroundColor = color;" & vbCrLf & _
                    "boton.style.color = colorletra;" & vbCrLf
                If sBrowser = "IE" And Version > 6 Then
                    sScript = sScript & "var tabla = boton.childNodes[0];" & vbCrLf & _
                    "tabla.style.backgroundColor = color;" & vbCrLf & _
                    "tabla.style.color = colorletra;" & vbCrLf & _
                    "tabla.rows[0].cells[0].style.backgroundPosition = '0px 0px';" & vbCrLf & _
                    "tabla.rows[0].cells[2].style.backgroundPosition = '-6px -0px';" & vbCrLf
                End If
                sScript = sScript & "};"
                Me.Page.ClientScript.RegisterClientScriptBlock(Me.Page.GetType(), "fsntab_scripts", sScript, True)
            End If
        End If
    End Sub
End Class