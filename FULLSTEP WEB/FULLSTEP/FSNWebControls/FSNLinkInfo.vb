﻿Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Text
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls


<DefaultProperty("Text"), ToolboxData("<{0}:FSNLinkInfo runat=server></{0}:FSNLinkInfo>")> _
Public Class FSNLinkInfo
    Inherits LinkButton

    <Browsable(True), Description("Id del control FSNPanelInfo"), _
    TypeConverter(GetType(AssociatedControlConverter)), Category("Behavior")> _
    Public Property PanelInfo() As String
        Get
            Dim o As Object = ViewState("PanelInfo")
            If o Is Nothing Then
                Return String.Empty
            Else
                Return CStr(o)
            End If
        End Get
        Set(ByVal value As String)
            ViewState("PanelInfo") = value
            MyBase.OnClientClick = GenerarOnClientClick()
        End Set
    End Property

    ''' <summary>
    ''' Contiene el valor z-index que deseamos que tenga el panelinfo al cual enlaza este objeto LinkInfo
    ''' En el momento de pulsar el LinkInfo, se modifica en cliente el z-index del panelInfo que vamos a mostrar.
    ''' Para que sea efectivo, hay que usarlo en combinación con la propiedad PanelInfoUpdatePanel
    ''' </summary>
    <Browsable(True), Bindable(False), _
        Category("Appearance"), Description("Orden de apilamiento del elemento PanelInfo que abre el LinkInfo. Si no es un PanelInfo el elemento abierto, ignora esta propiedad"), _
        TypeConverter(GetType(StringConverter)), _
        Themeable(True), DefaultValue(GetType(String), "")> _
    Public Property PanelInfoZindex() As String
        Get
            EnsureChildControls()
            Dim o As Object = ViewState("Zindex")
            If o Is Nothing Then
                Return String.Empty
            Else
                Return CType(o, String)
            End If
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            ViewState("Zindex") = value
        End Set
    End Property

    ''' <summary>
    ''' Contiene el Id del updatePanel dentro del cual está el elemento PanelInfo que abre el LinkInfo. 
    ''' Si no es un PanelInfo el elemento abierto, o no existe el UpdatePanel, el control ignora esta propiedad
    ''' Al recuperar el updatePanel podemos modificar el z-index del panelInfo y refrescar la pantalla.
    ''' </summary>
    <Browsable(True), Bindable(False), _
        Category("Appearance"), Description("UpdatePanel que contiene el elemento PanelInfo que abre el LinkInfo. Si no es un PanelInfo el elemento abierto, o no existe el UpdatePanel, ignora esta propiedad"), _
        TypeConverter(GetType(StringConverter)), _
        Themeable(True), DefaultValue(GetType(String), "")> _
    Public Property PanelInfoUpdatePanel() As String
        Get
            EnsureChildControls()
            Dim o As Object = ViewState("updatePanel")
            If o Is Nothing Then
                Return String.Empty
            Else
                Return CType(o, String)
            End If
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            ViewState("updatePanel") = value
        End Set
    End Property

    ''' <summary>
    ''' En el caso de que usemos el FSNLinkInfo para desplegar un fsnPanelInfo, esta propiedad servirá para definir el tipo de detalle del fsnPanelInfo, 
    ''' que determinará las características visibles del mismo. 
    ''' Es necesario que el panelInfo esté dentro de un updatepanel y que
    ''' se proporcione el nombre del updatepanel en la propiedad PanelInfoUpdatePanel
    ''' Los modos pueden verse en FSNPanelInfo.vb-->TipoDetalle
    ''' </summary>
    <Browsable(True), Bindable(False), _
        Category("Appearance"), Description("TipoDetalle del elemento PanelInfo que abre el LinkInfo. Si no es un PanelInfo el elemento abierto, o no existe el UpdatePanel, ignora esta propiedad"), _
        TypeConverter(GetType(Int32Converter)), _
        Themeable(True), DefaultValue(GetType(Integer), "")> _
    Public Property PanelInfoTipoDetalle() As Integer
        Get
            EnsureChildControls()
            Dim o As Object = ViewState("PanelInfoTipoDetalle")
            If o Is Nothing Then
                Return -1
            Else
                Return CType(o, String)
            End If
        End Get
        Set(ByVal value As Integer)
            EnsureChildControls()
            ViewState("PanelInfoTipoDetalle") = value
        End Set
    End Property

    <Browsable(True), Bindable(True), _
    Description("Código a pasar al WebMethod"), Themeable(True), _
    Category("Behavior")> _
    Public Property ContextKey() As String
        Get
            Dim o As Object = ViewState("ContextKey")
            If o Is Nothing Then
                Return String.Empty
            Else
                Return CStr(o)
            End If
        End Get
        Set(ByVal value As String)
            ViewState("ContextKey") = value
            MyBase.OnClientClick = GenerarOnClientClick()
        End Set
    End Property

    Public Shadows ReadOnly Property OnClientClick() As String
        Get
            Return MyBase.OnClientClick()
        End Get
    End Property

    ''' Revisado por: blp. Fecha: 06/03/2012
    ''' <summary>
    ''' Se define el comportamiento del control en el lado cliente cuando el usuario hace click
    ''' Se va a mostrar el ModalPopupExtender o PanelInfo que se le haya pasado al control
    ''' </summary>
    ''' <returns>Javascript a ejecutar</returns>
    ''' <remarks>Llamada desde FSNLinkInfo_Load. Máx. 0,1 seg.</remarks>
    Private Function GenerarOnClientClick() As String
        Dim oCtrl As Control = DevolverControl(Me.PanelInfo)
        Dim sCad As String = String.Empty
        If TypeOf (oCtrl) Is AjaxControlToolkit.ModalPopupExtender Then
            Dim oModal As AjaxControlToolkit.ModalPopupExtender = CType(oCtrl, AjaxControlToolkit.ModalPopupExtender)
            sCad = "FSNMostrarPanel('" & oModal.ClientID & "', event); return false"
        ElseIf TypeOf (oCtrl) Is FSNPanelInfo Then
            If Me.PanelInfoZindex <> String.Empty Then
                CType(oCtrl, FSNPanelInfo).Zindex = PanelInfoZindex
                'Para que el cambio de Zindex del panelInfo funcione, el panelInfo tiene que estar dentro de un update panel al que podamos llegar con el NamingContainer
                'Y que tenga un nombre que se pase al FSNLinkInfo
                If Me.PanelInfoUpdatePanel <> String.Empty Then
                    Dim upPanelInfo As UpdatePanel = TryCast(oCtrl.NamingContainer.FindControl(Me.PanelInfoUpdatePanel), UpdatePanel)
                    If upPanelInfo IsNot Nothing Then
                        upPanelInfo.Update()
                    End If
                End If
            End If
            If Me.PanelInfoTipoDetalle <> -1 Then
                CType(oCtrl, FSNPanelInfo).TipoDetalle = PanelInfoTipoDetalle
                'Para que el cambio de TipoDetalle del panelInfo funcione, el panelInfo tiene que estar dentro de un update panel al que podamos llegar con el NamingContainer
                'Y que tenga un nombre que se pase al FSNLinkInfo
                If Me.PanelInfoUpdatePanel <> String.Empty Then
                    Dim upPanelInfo As UpdatePanel = TryCast(oCtrl.NamingContainer.FindControl(Me.PanelInfoUpdatePanel), UpdatePanel)
                    If upPanelInfo IsNot Nothing Then
                        upPanelInfo.Update()
                    End If
                End If
            End If
            Dim oPanel As FSNPanelInfo = CType(oCtrl, FSNPanelInfo)
            If oPanel Is Nothing Then Return String.Empty
            sCad = "FSNMostrarPanel('" & oPanel.AnimationClientID & "', event, '" & oPanel.DynamicPopulateClientID & "'"
            If String.IsNullOrEmpty(Me.ContextKey) Then
                sCad = sCad & ", null"
            Else
                sCad = sCad & ", '" & Me.ContextKey & "'"
            End If
            If Not String.IsNullOrEmpty(oPanel.Contenedor) Then _
                sCad = sCad & ", '" & DevolverControl(oPanel.Contenedor).ClientID & "'"
            sCad = sCad & "); return false"
        End If
        Return sCad
    End Function

    Private Function DevolverControl(ByVal Id As String) As Control
        Dim p As Control = Nothing
        Dim c As Control = Me.NamingContainer
        Do While c IsNot Nothing
            p = c.FindControl(Id)
            If p IsNot Nothing Then
                Exit Do
            Else
                c = c.NamingContainer
            End If
        Loop
        Return p
    End Function

    Private Sub FSNLinkInfo_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Page.ClientScript.RegisterClientScriptResource(GetType(FSNPanelClientScript), "Fullstep.FSNWebControls.FSNPaneles.js")
        MyBase.OnClientClick = GenerarOnClientClick()
    End Sub
End Class
