﻿Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Text
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.Design
Imports System.Drawing.Design
Imports System.IO

Public Class FSNPanelInfo
    Inherits Control
    Implements INamingContainer

    Private _panel As Panel
    Private _panelContainer As Panel
    Private _dynextender As AjaxControlToolkit.DynamicPopulateExtender
    Private _modalpopupextender As AjaxControlToolkit.ModalPopupExtender
    Private _imgCerrar As HtmlControls.HtmlImage
    Private _imagenPopUp As HtmlControls.HtmlImage
    Private _labelTitulo As Label
    Private _labelSubTitulo As Label
    Private _TipoDetalle As Integer
    Private _animResize As AjaxControlToolkit.Animation

    <Browsable(True), Description("Id del control contenedor"), _
    TypeConverter(GetType(AssociatedControlConverter)), Category("Behavior")> _
    Public Property Contenedor() As String
        Get
            Dim o As Object = ViewState("Contenedor")
            If o Is Nothing Then
                Return String.Empty
            Else
                Return CStr(o)
            End If

        End Get
        Set(ByVal value As String)
            ViewState("Contenedor") = value
        End Set
    End Property

    ''' <summary>
    ''' Aunque el nombre sea confuso, es un modalpopup, no un animation.
    ''' </summary>
    <Browsable(False), Description("Id del control ModalPopupExtender")> _
    Public ReadOnly Property AnimationClientID() As String
        Get
            EnsureChildControls()
            Return _modalpopupextender.ClientID
        End Get
    End Property

    <Browsable(False), Description("Id del control DynamicPopulateExtender")> _
    Public ReadOnly Property DynamicPopulateClientID() As String
        Get
            EnsureChildControls()
            Return _dynextender.ClientID
        End Get
    End Property

    <Browsable(True), Description("Path del servicio web"), _
    Editor(GetType(UrlEditor), GetType(UITypeEditor)), UrlProperty(), _
    Category("Behavior")> _
    Public Property ServicePath() As String
        Get
            EnsureChildControls()
            Return _dynextender.ServicePath
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            _dynextender.ServicePath = value
        End Set
    End Property

    <Browsable(True), Description("Método del servicio web"), _
    Category("Behavior")> _
    Public Property ServiceMethod() As String
        Get
            EnsureChildControls()
            Return _dynextender.ServiceMethod
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            _dynextender.ServiceMethod = value
        End Set
    End Property

    <Browsable(True), Bindable(True), _
    Description("Texto del botón cerrar"), Themeable(True), _
    DefaultValue("Cerrar"), Category("Appearance")> _
    Public Property TextoCerrar() As String
        Get
            EnsureChildControls()
            If String.IsNullOrEmpty(_imgCerrar.Alt) Then _imgCerrar.Alt = "Cerrar"
            Return _imgCerrar.Alt
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            _imgCerrar.Alt = value
        End Set
    End Property

    <Browsable(True), Bindable(True), _
    Category("Appearance"), Description("Imagen del botón cerrar"), _
    Editor(GetType(ImageUrlEditor), GetType(UITypeEditor)), UrlProperty(), _
    Themeable(True)> _
    Public Property ImagenCerrar() As String
        Get
            EnsureChildControls()
            Return _imgCerrar.Src
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            _imgCerrar.Src = value
        End Set
    End Property

    <Browsable(True), Bindable(True), _
    Category("Appearance"), Description("Imagen del Pop Up"), _
    Editor(GetType(ImageUrlEditor), GetType(UITypeEditor)), UrlProperty(), _
    Themeable(True)> _
    Public Property ImagenPopUp() As String
        Get
            EnsureChildControls()
            Return _imagenPopUp.Src
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            _imagenPopUp.Src = value
        End Set
    End Property

    ''' <summary>
    ''' Según el tipo de detalle a mostrar cargara los literales que aparecerán en el panel info
    ''' </summary>
    ''' <value>
    ''' 0->No detalle (en este caso, pueden usarse las propiedades del control: Titulo, Subtitulo, cssTitulo y cssSubtitulo
    ''' 1->Proveedor
    ''' 2->Persona
    ''' </value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Browsable(True), Bindable(True), _
    Themeable(True), DefaultValue(0), _
    Category("Appearance")> _
    Public Property TipoDetalle() As Integer
        Get
            Return _TipoDetalle
        End Get
        Set(ByVal value As Integer)
            _TipoDetalle = value

            Select Case _TipoDetalle
                Case 1
                    _labelTitulo.Text = Textos(0)
                    _labelTitulo.CssClass = "TituloPopUpPanelInfo"
                    _labelSubTitulo.Text = Textos(1)
                    _labelSubTitulo.CssClass = "SubTituloPopUpPanelInfo"
                Case 2
                    _labelTitulo.Text = Textos(2)
                    _labelTitulo.CssClass = "TituloPopUpPanelInfo"
                    _labelSubTitulo.Text = Textos(3)
                    _labelSubTitulo.CssClass = "SubTituloPopUpPanelInfo"
                Case 3
                    _labelTitulo.Text = Textos(4)
                    _labelTitulo.CssClass = "TituloPopUpPanelInfo"
                    _labelSubTitulo.Text = Textos(1)
                    _labelSubTitulo.CssClass = "SubTituloPopUpPanelInfo"
                Case 4
                    _labelTitulo.Text = Textos(5)
                    _labelTitulo.CssClass = "RotuloGrande"
                    _labelSubTitulo.Text = ""
                    _labelSubTitulo.CssClass = "SubTituloPopUpPanelInfo"
                Case 5
                    _labelTitulo.Text = ""
                    _labelTitulo.CssClass = "RotuloGrande"
                    _labelSubTitulo.Text = ""
                    _labelSubTitulo.CssClass = ""
				Case 7 'Tipo Detalle de pedido Fullstep
					_panel.Style.Add("min-width", "300px")
					_labelTitulo.Text = Textos(11)
                    _labelTitulo.CssClass = "TituloPopUpPanelInfo"
                    _labelSubTitulo.Text = Textos(1)
                    _labelSubTitulo.CssClass = "SubTituloPopUpPanelInfo"
                Case Else
                    _labelTitulo.Text = ""
                    _labelTitulo.CssClass = "TituloPopUpPanelInfo"
                    _labelSubTitulo.Text = ""
                    _labelSubTitulo.CssClass = "SubTituloPopUpPanelInfo"
            End Select

        End Set
    End Property

    ''' <summary>
    ''' Determina el literal que aparecerá en el Titulo del panel. Propiedad alternativa al uso de TipoDetalle
    ''' </summary>
    ''' <value>literal que aparecerá como Título del panel</value>
    Public Property Titulo() As String
        Get
            Return _labelTitulo.Text
        End Get
        Set(ByVal value As String)
            _labelTitulo.Text = value
        End Set
    End Property

    ''' <summary>
    ''' Determina el literal que aparecerá en el SubTitulo del panel. Propiedad alternativa al uso de TipoDetalle
    ''' </summary>
    ''' <value>literal que aparecerá como SubTítulo del panel</value>
    Public Property SubTitulo() As String
        Get
            Return _labelSubTitulo.Text
        End Get
        Set(ByVal value As String)
            _labelSubTitulo.Text = value
        End Set
    End Property

    ''' <summary>
    ''' Determina el css del Titulo del panel. Propiedad alternativa al uso de TipoDetalle
    ''' </summary>
    ''' <value>Estilo que dará formato al Título en el panel</value>
    <Browsable(True), Bindable(True), _
    Themeable(True), _
    Category("Appearance")> _
    Public Property cssTitulo() As String
        Get
            Return _labelTitulo.CssClass
        End Get
        Set(ByVal value As String)
            _labelTitulo.CssClass = value
        End Set
    End Property

    ''' <summary>
    ''' Determina el estilo del SubTitulo del panel. Propiedad alternativa al uso de TipoDetalle.
    ''' </summary>
    ''' <value>Estilo que dará formato al SubTítulo en el panel</value>
    <Browsable(True), Bindable(True), _
    Themeable(True), _
    Category("Appearance")> _
    Public Property cssSubTitulo() As String
        Get
            Return _labelSubTitulo.CssClass
        End Get
        Set(ByVal value As String)
            _labelSubTitulo.CssClass = value
        End Set
    End Property

    Private ReadOnly Property Textos(ByVal Tipo As Integer)
        Get
            Dim oFSNUser As FSNServer.User = CType(HttpContext.Current.Session("FSN_User"), FSNServer.User)
            Dim oFSServer As FSNServer.Root = CType(HttpContext.Current.Session("FSN_Server"), FSNServer.Root)
            If oFSServer Is Nothing Then
                Return Nothing
            Else
                Dim oDict As FSNServer.Dictionary = oFSServer.Get_Object(GetType(FSNServer.Dictionary))
                oDict.LoadData(FSNLibrary.TiposDeDatos.ModulosIdiomas.PanelInfo, oFSNUser.Idioma)

                Return oDict.Data.Tables(0).Rows(Tipo).Item(1)
            End If
        End Get
    End Property

    <Browsable(True), Bindable(False), _
        Category("Appearance"), Description("Color de Fondo"), _
        TypeConverter(GetType(WebColorConverter)), _
        Themeable(True), DefaultValue(GetType(Drawing.Color), "#F1F1F1")> _
    Public Property BackColor() As System.Drawing.Color
        Get
            EnsureChildControls()
            If _panel.BackColor.IsEmpty() Then _panel.BackColor = Drawing.ColorTranslator.FromHtml("#F1F1F1")
            Return _panel.BackColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            EnsureChildControls()
            _panel.BackColor = value
        End Set
    End Property

    <Browsable(True), Bindable(False), _
        Category("Appearance"), Description("Orden de apilamiento del elemento en la página"), _
        TypeConverter(GetType(WebColorConverter)), _
        Themeable(True), DefaultValue(GetType(Integer), "9999")> _
    Public Property Zindex() As String
        Get
            EnsureChildControls()
            Dim o As Object = ViewState("Zindex")
            If o Is Nothing Then
                Return "9999"
            Else
                Return CType(o, String)
            End If
        End Get
        Set(ByVal value As String)
            EnsureChildControls()
            ViewState("Zindex") = value
            If _panel IsNot Nothing Then
                _panel.Style.Add(HtmlTextWriterStyle.ZIndex, value.ToString())
            End If
        End Set
    End Property

    <Browsable(True), Bindable(False), _
    Category("Appearance"), Description("Color del borde"), _
    TypeConverter(GetType(WebColorConverter)), _
    Themeable(True), DefaultValue(GetType(Drawing.Color), "#8C8C8C")> _
    Public Property BorderColor() As System.Drawing.Color
        Get
            EnsureChildControls()
            If _panel.BorderColor.IsEmpty() Then _panel.BorderColor = Drawing.ColorTranslator.FromHtml("#8C8C8C")
            Return _panel.BorderColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            EnsureChildControls()
            _panel.BorderColor = value
        End Set
    End Property

    <Browsable(True), Bindable(True), _
    Category("Appearance"), Description("Obtiene o establece el ancho del panel."), _
    TypeConverter(GetType(UnitConverter)), _
    Themeable(True), DefaultValue(GetType(Unit), "280px")> _
    Public Property Width() As Unit
        Get
            Dim o As Object = ViewState("Width")
            If o Is Nothing Then
                Return Unit.Pixel(280)
            Else
                Return CType(o, Unit)
            End If
        End Get
        Set(ByVal value As Unit)
            ViewState("Width") = value
            If _animResize IsNot Nothing Then _
                _animResize.Properties("Width") = value.ToString()
        End Set
    End Property

    <Browsable(True), Bindable(True), _
    Category("Appearance"), Description("Obtiene o establece el alto del panel."), _
    TypeConverter(GetType(UnitConverter)), _
    Themeable(True), DefaultValue(GetType(Unit), "260px")> _
    Public Property Height() As Unit
        Get
            Dim o As Object = ViewState("Height")
            If o Is Nothing Then
                Return Unit.Pixel(260)
            End If
        End Get
        Set(ByVal value As Unit)
            ViewState("Height") = value
            If _animResize IsNot Nothing Then _
                _animResize.Properties("Height") = value.ToString()
        End Set
    End Property

    ''' Revisado por: blp. Fecha: 08/03/2012
    ''' <summary>
    ''' Crea los controles del panelInfo
    ''' </summary>
    ''' <remarks>Llamada desde el control. Máx. 0,3 seg.</remarks>
    Protected Overrides Sub CreateChildControls()
        'Botón oculto
        Dim oc As New HtmlControls.HtmlInputButton()
        oc.ID = "btnOculto"
        oc.Style.Add(HtmlTextWriterStyle.Display, "none")
        Me.Controls.Add(oc)
        ' Panel Info
        _panel = New Panel()
        _panel.ID = "pnlInfo"
        _panel.BorderStyle = BorderStyle.Solid
        _panel.BorderWidth = New Unit(1, UnitType.Pixel)
        _panel.Style.Add(HtmlTextWriterStyle.Height, Height.ToString)
		If Not Width.ToString = "0" Then _panel.Style.Add(HtmlTextWriterStyle.Width, Width.ToString)
		_panel.Style.Add(HtmlTextWriterStyle.Display, "none")
        _panel.Style.Add(HtmlTextWriterStyle.Position, "absolute")
        _panel.Attributes.Add("onmouseover", "FSNNoCerrarPaneles()")

        Dim tabla As New HtmlControls.HtmlTable()
        tabla.Style.Add(HtmlTextWriterStyle.Width, "100%")
        tabla.CellPadding = 2
        tabla.CellSpacing = 0
        tabla.Style.Add(HtmlTextWriterStyle.ZIndex, "10004")
        tabla.Style.Add(HtmlTextWriterStyle.Position, "absolute")
        tabla.ID = "tblInfo"

        'Celda para la imagen de popup
        Dim fila As New HtmlControls.HtmlTableRow()
        Dim celda As New HtmlControls.HtmlTableCell()

        'Celda para el Label Titulo
        celda.Align = "left"
        celda.VAlign = "bottom"
        celda.Style.Add(HtmlTextWriterStyle.Height, "30px")
        _labelTitulo = New Label
        _labelTitulo.Style.Add("padding-left", "70px")
        celda.Controls.Add(_labelTitulo)
        fila.Cells.Add(celda)

        'Imagen Cerrar
        celda = New HtmlControls.HtmlTableCell()
        celda.ID = "cellImagenCerrar"
        celda.Align = "right"
        celda.VAlign = "top"
        celda.Style.Add(HtmlTextWriterStyle.Height, "30px")
        celda.Style.Add(HtmlTextWriterStyle.Width, "35px")
        Dim lnk As New HtmlControls.HtmlAnchor()
        lnk.ID = "lnkCerrar"
        lnk.HRef = "#"
        lnk.Attributes.Add("onclick", "return false")
        _imgCerrar = New HtmlControls.HtmlImage()
        _imgCerrar.ID = "imgCerrar"
        _imgCerrar.Border = 0
        lnk.Controls.Add(_imgCerrar)
        celda.Controls.Add(lnk)
        fila.Cells.Add(celda)
        fila.Attributes.Add("class", "CapaTituloPanelInfo")
        fila.ID = "filaTitulo"

        tabla.Rows.Add(fila)

        'Fila SubTitulo
        fila = New HtmlControls.HtmlTableRow()
        celda = New HtmlControls.HtmlTableCell()
        celda.Align = "left"
        celda.VAlign = "top"
        _labelSubTitulo = New Label
        _labelSubTitulo.Style.Add("padding-left", "80px")
        celda.Controls.Add(_labelSubTitulo)
        fila.Cells.Add(celda)

        celda = New HtmlControls.HtmlTableCell()
        celda.Style.Add(HtmlTextWriterStyle.Width, "50px")
        fila.Cells.Add(celda)
        fila.ID = "filaSubtitulo"
        fila.Attributes.Add("class", "CapaSubTituloPanelInfo")
        tabla.Rows.Add(fila)

        'Fila vacía para que el texto no salga bajo la imagen superior izquierda
        fila = New HtmlControls.HtmlTableRow()
        celda = New HtmlControls.HtmlTableCell()
        celda.Style.Add(HtmlTextWriterStyle.Height, "15px")
        celda.ColSpan = "2"
        fila.Cells.Add(celda)
        fila.Style.Add("background-color", "#F1F1F1")
        tabla.Rows.Add(fila)

        'Contenido del Pop Up
        fila = New HtmlControls.HtmlTableRow()
        celda = New HtmlControls.HtmlTableCell()
        celda.ColSpan = 3
        celda.Style.Add(HtmlTextWriterStyle.Width, "100%")
        Dim olabel As New Label()
        olabel.ID = "lblInfo"
        olabel.CssClass = "Normal"
        celda.Controls.Add(olabel)
        fila.Cells.Add(celda)
        fila.Style.Add("background-color", "#F1F1F1")
        tabla.Rows.Add(fila)

        'Tabla para la imagen del pop up
        Dim tablaNueva As New HtmlControls.HtmlTable()

        tablaNueva.Style.Add(HtmlTextWriterStyle.Width, "50%")
        tablaNueva.CellPadding = 2
        tablaNueva.CellSpacing = 0
        tablaNueva.Style.Add(HtmlTextWriterStyle.ZIndex, "10005")
        tablaNueva.Style.Add(HtmlTextWriterStyle.Position, "absolute")
        tablaNueva.Style.Add(HtmlTextWriterStyle.BackgroundColor, "Transparent")

        fila = New HtmlControls.HtmlTableRow()
        celda = New HtmlControls.HtmlTableCell()

        'Celda para la imagen del PopUp
        celda.Align = "left"
        celda.VAlign = "top"
        _imagenPopUp = New HtmlControls.HtmlImage
        _imagenPopUp.ID = "imgPopUp"
        _imagenPopUp.Border = 0
        celda.Style.Add(HtmlTextWriterStyle.Padding, "0px")
        celda.Style.Add(HtmlTextWriterStyle.Width, "80px")
        celda.Style.Add(HtmlTextWriterStyle.Height, "60px")

        celda.Controls.Add(_imagenPopUp)

        fila.Cells.Add(celda)

        celda = New HtmlControls.HtmlTableCell()
        celda.Style.Add(HtmlTextWriterStyle.Width, "100%")
        fila.Cells.Add(celda)
        tablaNueva.ID = "tblImagenPopUp"
        tablaNueva.Rows.Add(fila)

        If Me.TemplateControl IsNot Nothing AndAlso Me.TemplateControl.Page.Request.ServerVariables("HTTP_USER_AGENT").IndexOf("MSIE 6") > 0 Then
            'Panel Container
            _panelContainer = New Panel()
            _panelContainer.ID = "pnlInfoContainer"
            _panelContainer.Attributes.Add("onmouseover", "FSNNoCerrarPaneles()")
            _panelContainer.CssClass = "pnlInfoContainer"

            _panelContainer.Controls.Add(tabla)
            _panelContainer.Controls.Add(tablaNueva)

            _panel.Controls.Add(_panelContainer)
            Dim oIframe As New HtmlControls.HtmlGenericControl
            oIframe.TagName = "IFRAME"
            oIframe.ID = "pnlInfoFrame"
            oIframe.Attributes.Add("scrolling", "no")
            oIframe.Attributes.Add("frameborder", "0")
            oIframe.Style.Add("display", "none")
            _panel.Controls.Add(oIframe)
            Me.Controls.Add(_panel)
        Else
            _panel.Controls.Add(tabla)
            _panel.Controls.Add(tablaNueva)
            Me.Controls.Add(_panel)
        End If

        ' Dynamic Populate
        _dynextender = New AjaxControlToolkit.DynamicPopulateExtender()
        If Me.FindControl("lblInfo_DynamicPopulateExtender") Is Nothing Then
            _dynextender.ID = "lblInfo_DynamicPopulateExtender"
        Else
            _dynextender.ID = "lblInfo_DynamicPopulateExtender2"
        End If

        _dynextender.Enabled = True
        _dynextender.TargetControlID = olabel.ID
        _dynextender.UpdatingCssClass = "actualizando"
        Me.Controls.Add(_dynextender)
        ' Modal Abrir
        _modalpopupextender = New AjaxControlToolkit.ModalPopupExtender()
        If Me.FindControl("pnlInfo_AnimationExtender") Is Nothing Then
            _modalpopupextender.ID = "pnlInfo_AnimationExtender"
        Else
            _modalpopupextender.ID = "pnlInfo_AnimationExtender2"
        End If
        _modalpopupextender.Enabled = True
        _modalpopupextender.TargetControlID = oc.ID
        _modalpopupextender.PopupControlID = _panel.ID
        _modalpopupextender.CancelControlID = lnk.ID

        Me.Controls.Add(_modalpopupextender)
    End Sub

    Private Sub FSNPanelInfo_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Page.ClientScript.RegisterClientScriptResource(GetType(FSNPanelClientScript), "Fullstep.FSNWebControls.FSNPaneles.js")
    End Sub

    ''' Revisado por: blp. Fecha:06/03/2012
    ''' <summary>
    ''' Da formato y funcionalidades diversas al control en el momento previo a ser representado en pantalla
    ''' </summary>
    ''' <param name="sender">Control</param>
    ''' <param name="e">argumento del Evento</param>
    ''' <remarks>Llamada desde el evento. Máx. 0,1 seg.</remarks>
    Private Sub FSNPanelInfo_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'Comprobamos si nos han dado Titulo y Subtitulo para ver si mostramos la cabecera
        Select Case TipoDetalle
            Case 0
                'En el tipo cero, o sea, cuando no se detalla un tipoDetalle, podemos hacer uso de
                'las propiedes Titulo, Subtitulo, cssTitulo y cssSubtitulo del control 
                'Si no llevan valor, ocultamos los controles. Si no, los mostramos.
                If String.IsNullOrEmpty(Titulo) AndAlso String.IsNullOrEmpty(SubTitulo) Then
                    Dim panelInfo As Control = DevolverControl("pnlInfo")
                    panelInfo.FindControl("tblImagenPopUp").Visible = False

                    CType(panelInfo.FindControl("tblInfo").FindControl("filaTitulo"), HtmlControls.HtmlTableRow).Attributes.Remove("class")
                    panelInfo.FindControl("tblInfo").Controls.Remove(panelInfo.FindControl("tblInfo").FindControl("filaSubTitulo"))
                End If
            Case 5
                If String.IsNullOrEmpty(SubTitulo) Then
                    Dim panelInfo As Control = DevolverControl("pnlInfo")
                    CType(panelInfo.FindControl("tblInfo").FindControl("filaTitulo"), HtmlControls.HtmlTableRow).Attributes.Remove("class")
                    panelInfo.FindControl("tblInfo").Controls.Remove(panelInfo.FindControl("tblInfo").FindControl("filaSubTitulo"))
                End If
            Case 4
                Dim panelInfo As Control = DevolverControl("pnlInfo")

                CType(panelInfo.FindControl("tblInfo").FindControl("filaTitulo"), HtmlControls.HtmlTableRow).Attributes.Remove("class")
                CType(panelInfo.FindControl("tblInfo").FindControl("filaSubTitulo"), HtmlControls.HtmlTableRow).Attributes.Remove("class")

                CType(panelInfo.FindControl("tblImagenPopUp").FindControl("imgPopUp"), HtmlControls.HtmlImage).Style.Add("padding-top", "5px")
                CType(panelInfo.FindControl("tblImagenPopUp").FindControl("imgPopUp"), HtmlControls.HtmlImage).Style.Add("padding-left", "10px")

                _imagenPopUp.Src = Me.Page.ClientScript.GetWebResourceUrl(GetType(FSNPanelInfo), "Fullstep.FSNWebControls.Icono_Error_Amarillo_40x40.gif")
            Case 6
                Dim panelInfo As Control = DevolverControl("pnlInfo")
                Dim oPanelInfo As Panel = TryCast(panelInfo, Panel)
                Me.Height = Unit.Pixel(150)
                Me.Width = Unit.Pixel(300)
                If oPanelInfo IsNot Nothing Then
                    oPanelInfo.Style.Add("padding-left", "15px")
                    oPanelInfo.Style.Add("padding-right", "15px")
                    oPanelInfo.Style.Add("padding-bottom", "15px")
                    oPanelInfo.Style.Add("background-color", "#F1F1F1")
                    oPanelInfo.Style.Add("display", "inline")
                    Dim oTblInfo As HtmlControls.HtmlTable = CType(oPanelInfo.FindControl("tblInfo"), HtmlControls.HtmlTable)
                    oTblInfo.Style.Remove(HtmlTextWriterStyle.Width)
                    oTblInfo.Style.Add(HtmlTextWriterStyle.Width, "90%")
                    Dim oFilaTitulo As HtmlControls.HtmlTableRow = CType(oPanelInfo.FindControl("filaTitulo"), HtmlControls.HtmlTableRow)
                    oFilaTitulo.Style.Add("background-color", "#F1F1F1")
                    Dim oFilaSubtitulo As HtmlControls.HtmlTableRow = CType(oPanelInfo.FindControl("filaSubtitulo"), HtmlControls.HtmlTableRow)
                    oFilaSubtitulo.Style.Add("display", "none")
                    Dim oLblInfo As Label = CType(oPanelInfo.FindControl("lblInfo"), Label)
                    oLblInfo.Style.Add("width", "80%")
                    oLblInfo.Style.Remove("filter")
                    oLblInfo.Style.Add("filter", "progid:DXImageTransform.Microsoft.Alpha(opacity=100)")

                End If
        End Select

        If String.IsNullOrEmpty(_imgCerrar.Src) And Me.Page IsNot Nothing Then
            _imgCerrar.Src = Me.Page.ClientScript.GetWebResourceUrl(GetType(FSNPanelInfo), "Fullstep.FSNWebControls.Bt_Cerrar.png")
        End If

        If String.IsNullOrEmpty(_imagenPopUp.Src) And Me.Page IsNot Nothing Then
            _imagenPopUp.Src = Me.Page.ClientScript.GetWebResourceUrl(GetType(FSNPanelInfo), "Fullstep.FSNWebControls.calendar.png")
        End If

        _panel.BackColor = BackColor
        _panel.BorderColor = BorderColor
        _panel.Style.Add(HtmlTextWriterStyle.ZIndex, Zindex)
        _imgCerrar.Alt = TextoCerrar
        Dim sOnMouseout As String
        If Contenedor.Length > 0 Then
            sOnMouseout = "FSNCerrarPanelesTemp('" & DevolverControl(Contenedor).ClientID & "')"
        Else
            sOnMouseout = "FSNCerrarPanelesTemp()"
        End If
        If _panel.Attributes.Item("onmouseout") IsNot Nothing Then
            _panel.Attributes.Item("onmouseout") = sOnMouseout
        Else
            _panel.Attributes.Add("onmouseout", sOnMouseout)
        End If
    End Sub

    Private Function DevolverControl(ByVal Id As String) As Control
        Dim p As Control = Nothing
        Dim c As Control = Me
        Do While c IsNot Nothing
            p = c.FindControl(Id)
            If p IsNot Nothing Then
                Exit Do
            Else
                c = c.NamingContainer
            End If
        Loop
        Return p
    End Function

End Class
