﻿Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Text
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls


<ToolboxData("<{0}:FSNImageInfo runat=server />")> _
Public Class FSNImageInfo
    Inherits ImageButton

    <Browsable(True), Description("Id del control FSNPanelInfo"), _
TypeConverter(GetType(AssociatedControlConverter)), Category("Behavior")> _
Public Property PanelInfo() As String
        Get
            Dim o As Object = ViewState("PanelInfo")
            If o Is Nothing Then
                Return String.Empty
            Else
                Return CStr(o)
            End If
        End Get
        Set(ByVal value As String)
            ViewState("PanelInfo") = value
            MyBase.OnClientClick = GenerarOnClientClick()
        End Set
    End Property

    <Browsable(True), Bindable(True), _
    Description("Código a pasar al WebMethod"), Themeable(True), _
    Category("Behavior")> _
    Public Property ContextKey() As String
        Get
            Dim o As Object = ViewState("ContextKey")
            If o Is Nothing Then
                Return String.Empty
            Else
                Return CStr(o)
            End If
        End Get
        Set(ByVal value As String)
            ViewState("ContextKey") = value
            MyBase.OnClientClick = GenerarOnClientClick()
        End Set
    End Property

    Public Shadows ReadOnly Property OnClientClick() As String
        Get
            Return MyBase.OnClientClick()
        End Get
    End Property

    Private Function GenerarOnClientClick() As String
        Dim oCtrl As Control = DevolverControl(Me.PanelInfo)
        Dim sCad As String = String.Empty
        If TypeOf (oCtrl) Is AjaxControlToolkit.ModalPopupExtender Then
            Dim oModal As AjaxControlToolkit.ModalPopupExtender = CType(oCtrl, AjaxControlToolkit.ModalPopupExtender)
            sCad = "FSNMostrarPanel('" & oModal.ClientID & "', event); return false"
        ElseIf TypeOf (oCtrl) Is FSNPanelInfo Then
            Dim oPanel As FSNPanelInfo = CType(oCtrl, FSNPanelInfo)
            If oPanel Is Nothing Then Return String.Empty
            sCad = "FSNMostrarPanel('" & oPanel.AnimationClientID & "', event, '" & oPanel.DynamicPopulateClientID & "'"
            If String.IsNullOrEmpty(Me.ContextKey) Then
                sCad = sCad & ", null"
            Else
                sCad = sCad & ", '" & Me.ContextKey & "'"
            End If
            If Not String.IsNullOrEmpty(oPanel.Contenedor) Then _
                sCad = sCad & ", '" & DevolverControl(oPanel.Contenedor).ClientID & "'"
            sCad = sCad & "); return false"
        End If
        Return sCad
    End Function

    Private Function DevolverControl(ByVal Id As String) As Control
        Dim p As Control = Nothing
        Dim c As Control = Me.NamingContainer
        Do While c IsNot Nothing
            p = c.FindControl(Id)
            If p IsNot Nothing Then
                Exit Do
            Else
                c = c.NamingContainer
            End If
        Loop
        Return p
    End Function

    Private Sub FSNImageInfo_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Page.ClientScript.RegisterClientScriptResource(GetType(FSNPanelClientScript), "Fullstep.FSNWebControls.FSNPaneles.js")
        MyBase.OnClientClick = GenerarOnClientClick()
    End Sub

End Class
