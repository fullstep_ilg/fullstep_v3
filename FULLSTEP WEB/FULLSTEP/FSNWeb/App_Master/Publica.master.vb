﻿Imports Fullstep.FSNLibrary
Imports Fullstep.FSNServer

Public Class Publica
    Inherits MasterPage
    

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblNumVersion.Text = ConfigurationManager.AppSettings("version")
        lblAnyoCopy.Text = ConfigurationManager.AppSettings("copyright")
    End Sub
End Class