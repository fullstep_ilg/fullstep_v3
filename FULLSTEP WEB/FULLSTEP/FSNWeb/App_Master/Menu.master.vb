﻿Imports Fullstep.FSNLibrary
Imports Fullstep.FSNLibrary.TiposDeDatos

Partial Public Class Menu
    Inherits System.Web.UI.MasterPage

    Private Const TiempoMenu As Integer = 1000 ' Milisegundos que tarda en restablecerse el menú
    Private _FSNServer As FSNServer.Root

    ''' Revisado por: Jbg. Fecha: 28/11/2011
    ''' <summary>
    ''' Inicializa el menú en función de los permisos del usuario
    ''' </summary>
    ''' <param name="sender">Origen del evento.</param>
    ''' <param name="e">Clase EventArgs que no contiene datos de evento.</param>
    ''' <remarks>Se produce cuando el control de servidor se carga en el objeto Page.</remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim bAccesoFSPM As Boolean
        Dim bAccesoContratos As Boolean
        Dim bAccesoFSQA As Boolean
        Dim bAccesoFSEP As Boolean
        Dim bAccesoFSSM As Boolean
        Dim bAccesoFSINT As Boolean
        Dim bAccesoFACT As Boolean

        Dim nAplicaciones As Integer = 0

        If TypeOf Page Is FSNServer.IFSNPage Then
            Dim pag As FSNServer.IFSNPage = CType(Page, FSNServer.IFSNPage)

            Dim oAcceso As ParametrosGenerales
            Dim oUser As FSNServer.User = pag.Usuario

            If Not IsPostBack() Then
                CargarTextos()

                oAcceso = pag.Acceso
                ''''''''''''''''''''''''''''''''''''''''''''''''''''
                'Accesos y nº de aplicaciones a las que tiene acceso
                ''''''''''''''''''''''''''''''''''''''''''''''''''''
                If oAcceso.gsAccesoFSPM <> FSNLibrary.TipoAccesoFSPM.SinAcceso And oUser.AccesoPM Then
                    bAccesoFSPM = True
                    nAplicaciones = nAplicaciones + 1
                End If
                If oAcceso.gbAccesoContratos AndAlso oUser.AccesoContratos Then
                    bAccesoContratos = True
                    nAplicaciones = nAplicaciones + 1
                End If
                If oAcceso.gsAccesoFSQA <> FSNLibrary.TipoAccesoFSQA.SinAcceso AndAlso (oUser.AccesoQA Or oUser.EsPeticionarioSolicitudesQA Or oUser.EsParticipanteSolicitudesQA Or oUser.EsPeticionarioEncuestas Or oUser.EsParticipanteEncuestas) Then
                    bAccesoFSQA = True
                    nAplicaciones = nAplicaciones + 1
                End If
                If oAcceso.gbAccesoFSEP And oUser.AccesoEP Then
                    bAccesoFSEP = True
                    nAplicaciones = nAplicaciones + 1
                End If
                If oAcceso.gbAccesoFSSM And oUser.AccesoSM Then
                    bAccesoFSSM = True
                    nAplicaciones = nAplicaciones + 1
                End If
                If oAcceso.g_bAccesoIS AndAlso oUser.AccesoINT Then
                    bAccesoFSINT = True
                    nAplicaciones = nAplicaciones + 1
                End If
                If oAcceso.g_bAccesoFSFA AndAlso oUser.AccesoFACT Then
                    bAccesoFACT = True
                    nAplicaciones = nAplicaciones + 1
                End If

                fsnTabSeguridad.Visible = oUser.AccesoSG
                panMnuSeguridad.Visible = oUser.AccesoSG
                fsnTabColaboracion.Visible = oAcceso.gbAccesoFSCN
                '--------------------'
                'Pestania de informes'
                '--------------------'
                fsnTabInformes.Visible = False
                If MostrarInformes(bAccesoFSEP, bAccesoFSPM, bAccesoFSQA, oUser, oAcceso) Then
                    fsnTabInformes.Visible = True
                Else
                    mnuInformes.Items.Remove(mnuInformes.FindItem("Informes"))
                End If
                If oUser.AccesoFSAL And oAcceso.gbAccesoFSAL Then
                    fsnTabInformes.Visible = True
                    If (Not oUser.ALVisorActividad) Then
                        mnuInformes.Items.Remove(mnuInformes.FindItem("Visor Actividad"))
                    End If
                Else
                    mnuInformes.Items.Remove(mnuInformes.FindItem("Visor Actividad"))
                End If
                If oUser.AccesoFSAL And oAcceso.gbAccesoFSAL Then
                    fsnTabInformes.Visible = True
                    If (Not oUser.ALVisorActividad) Then
                        mnuInformes.Items.Remove(mnuInformes.FindItem("Visor Rendimiento"))
                    End If
                Else
                    mnuInformes.Items.Remove(mnuInformes.FindItem("Visor Rendimiento"))
                End If
                If oUser.AccesoBI And oAcceso.gbAccesoFSBI And (oAcceso.giAccesoFSBITipo <> TipoAccesoFSBI.SinAcceso) Then
                    fsnTabInformes.Visible = True
                    Select Case oAcceso.giAccesoFSBITipo
                        Case TipoAccesoFSBI.Completo
                            If (Not oUser.BIConfigurarCubos) Then
                                mnuInformes.Items.Remove(mnuInformes.FindItem("Cubos"))
                            End If
                            If (Not oUser.BIConfigurarQlikApps) Then
                                mnuInformes.Items.Remove(mnuInformes.FindItem("QlikApps"))
                            End If
                        Case TipoAccesoFSBI.SoloCubosBI
                            mnuInformes.Items.Remove(mnuInformes.FindItem("QlikViewer"))
                            mnuInformes.Items.Remove(mnuInformes.FindItem("QlikApps"))
                            If (Not oUser.BIConfigurarCubos) Then
                                mnuInformes.Items.Remove(mnuInformes.FindItem("Cubos"))
                            End If
                        Case TipoAccesoFSBI.SoloQlikApps
                            mnuInformes.Items.Remove(mnuInformes.FindItem("BusinessInteligence"))
                            mnuInformes.Items.Remove(mnuInformes.FindItem("Cubos"))
                            If (Not oUser.BIConfigurarQlikApps) Then
                                mnuInformes.Items.Remove(mnuInformes.FindItem("QlikApps"))
                            End If
                    End Select
                Else
                    'quitamos los submenus ya que el propio menu hace de link a informes
                    mnuInformes.Items.Remove(mnuInformes.FindItem("BusinessInteligence"))
                    mnuInformes.Items.Remove(mnuInformes.FindItem("QlikViewer"))
                    mnuInformes.Items.Remove(mnuInformes.FindItem("Cubos"))
                    mnuInformes.Items.Remove(mnuInformes.FindItem("QlikApps"))
                End If
                If MostrarInformes(bAccesoFSEP, bAccesoFSPM, bAccesoFSQA, oUser, oAcceso) Then
                    If (Not (oUser.AccesoBI And oAcceso.gbAccesoFSBI) And Not (oUser.AccesoFSAL And oAcceso.gbAccesoFSAL)) Then
                        'Si solo tiene acceso a los informes quitamos el submenu, ya que el propio menu hace de enlace.
                        If (Not mnuInformes.FindItem("Informes") Is Nothing) Then
                            mnuInformes.Items.Remove(mnuInformes.FindItem("Informes"))
                        End If
                    End If
                End If
                'fsnTabInformes.Visible = MostrarInformes(bAccesoFSEP, bAccesoFSPM, bAccesoFSQA, oUser, oAcceso)
                '------------------------'
                'Fin Pestania de informes'
                '------------------------'
                If oUser.AccesoVisorNotificaciones Then
                    panTabsNotificaciones.Visible = True
                End If
                fsnTabIntegracion.Visible = bAccesoFSINT
                fsnTabFacturacion.Visible = bAccesoFACT

                Select Case nAplicaciones
                    Case 0
                        Master.Desconectar()
                    Case 1
                        fsnTabProcesos.Visible = False
                        panMnuProcesos.Visible = False
                        fsnTabContratos.Visible = False
                        panMnuContratos.Visible = False
                        fsnTabCalidad.Visible = False
                        panMnuCalidad.Visible = False
                        fsnTabCalidadConfiguracion.Visible = False
                        panMnuCalidadConfiguracion.Visible = False
                        fsnTabCatalogo.Visible = False
                        panMnuCatalogo.Visible = False
                        fsnTabCtrlPresup.Visible = False
                        panMnuCtrlPres.Visible = False
                        fsnTabFacturacion.Visible = False
                        panMnuFacturacion.Visible = False
                        fsnTabCalEncuestas.Visible = False
                        panMnuEncuestas.Visible = False
                        fsnTabCalSolicitudesQA.Visible = False
                        panMnuSolicitudesQA.Visible = False
                        'está a nivel de usuarios porque puede tratarse de una pyme y en ese caso se hace por UON de ese usuario
                        If bAccesoContratos Then
                            fsnTabContratos.Visible = True
                            panMnuContratos.Visible = True
                        Else
                            fsnTabContratos.Visible = False
                            panMnuContratos.Visible = False
                        End If
                        If bAccesoFSPM Then
                            panTabsProcesos.Visible = True
                        ElseIf bAccesoFSQA Then
                            panTabsCalidad.Visible = True
                            fsnTabCalPanelProveedores.Visible = oUser.QAPuntuacionProveedores
                            fsnTabCalCertificados.Visible = oAcceso.gbAccesoQACertificados
                            fsnTabCalNoConformidades.Visible = oAcceso.gbAccesoQANoConformidad
                            fsnTabCalPanelEscalacionProveedores.Visible = oUser.QAPanelEscalacionProveedoresAccesible

                            fsnTabCalidadConfiguracion.Visible = True
                            panMnuCalidadConfiguracion.Visible = True
                            If Not (oUser.QAMantenimientoUNQA OrElse oUser.QAMantenimientoProv) Then mnuCalidadConfiguracion.Items.Remove(mnuCalidadConfiguracion.FindItem("UnidadesNegocio"))
                            If Not oUser.QAMantenimientoMat Then mnuCalidadConfiguracion.Items.Remove(mnuCalidadConfiguracion.FindItem("Materiales"))
                            If Not oUser.QAPermitirMantNivelesEscalacion Then mnuCalidadConfiguracion.Items.Remove(mnuCalidadConfiguracion.FindItem("Escalacion"))
                            fsnTabCalidadConfiguracion.Visible = {oUser.QAPuntuacionProveedores, oUser.QACertifModExpiracion, oUser.QAModificarPeriodoValidezCertificados,
                                oUser.QACertifModExpiracion, oUser.QAModificarPeriodoValidezCertificados, oUser.QAContacAutom,
                                oAcceso.gbVarCal_Materiales, oUser.QAMantenimientoUNQA, oUser.QAModifProvCalidad, oUser.QAModifNotifCalif,
                                oUser.QAModifNotifProximidadNc, oUser.QAModifNotifMateriales, oUser.QACertifModExpiracion, oUser.QAContacAutom,
                                oUser.QAModifProvCalidad, oUser.QAPermisoModificarNivelesDefectoUNQA, oUser.QAPermitirMantNivelesEscalacion}.Contains(True)

                            fsnTabCalEncuestas.Visible = (oUser.EsPeticionarioEncuestas Or oUser.EsParticipanteEncuestas)
                            panMnuEncuestas.Visible = (oUser.EsPeticionarioEncuestas Or oUser.EsParticipanteEncuestas)
                            If Not oUser.EsPeticionarioEncuestas Then mnuEncuestas.Items.Remove(mnuEncuestas.FindItem("EncuestasAlta"))
                            If Not oUser.EsParticipanteEncuestas Then mnuEncuestas.Items.Remove(mnuEncuestas.FindItem("EncuestasSeguimiento"))

                            fsnTabCalSolicitudesQA.Visible = (oUser.EsPeticionarioSolicitudesQA Or oUser.EsParticipanteSolicitudesQA)
                            panMnuSolicitudesQA.Visible = (oUser.EsPeticionarioSolicitudesQA Or oUser.EsParticipanteSolicitudesQA)
                            If Not oUser.EsPeticionarioSolicitudesQA Then mnuSolicitudesQA.Items.Remove(mnuSolicitudesQA.FindItem("SolicitudesQAAlta"))
                            If Not oUser.EsParticipanteSolicitudesQA Then mnuSolicitudesQA.Items.Remove(mnuSolicitudesQA.FindItem("SolicitudesQASeguimiento"))

                        ElseIf bAccesoFSEP Then
                                panTabsCatalogo.Visible = True
                                Dim bAprovisionador As Boolean = (oUser.EPTipo = TipoAccesoFSEP.SoloAprovisionador Or oUser.EPTipo = TipoAccesoFSEP.AprobadorYAprovisionador)
                                Dim bAprobador As Boolean = (oUser.EPTipo = TipoAccesoFSEP.SoloAprobador Or oUser.EPTipo = TipoAccesoFSEP.AprobadorYAprovisionador Or oUser.EsAprobadorSolicitudContraPedidoAbierto)
                                fsnTabCatEmision.Visible = (bAprovisionador OrElse oUser.EPPermitirEmitirPedidosDesdePedidoAbierto)
                                fsnTabCatSeguimiento.Visible = bAprovisionador OrElse oUser.EPPermitirEmitirPedidosDesdePedidoAbierto
                                fsnTabCatAprobacion.Visible = bAprobador
                            ElseIf bAccesoFSSM Then
                                panTabsCtrlPresup.Visible = True
                                fsnTabCtrlPres.Visible = oUser.SMControlPresupuestario
                                fsnTabCtrlPresActivos.Visible = oUser.SMConsultarActivos
                            ElseIf bAccesoFACT Then
                                panTabsFacturacion.Visible = True
                            fsnTabFactAlta.Visible = oUser.IMAltaFacturas
                            fsnTabFactSeguimiento.Visible = oUser.IMSeguimientoFacturas
                            fsnTabFactAutofacturacion.Visible = oUser.IMVisorAutofacturas
                        End If
                    Case Else
                        If Not bAccesoFSPM Then
                            fsnTabProcesos.Visible = False
                            panMnuProcesos.Visible = False
                        End If
                        'estÃ¡ a nivel de usuarios porque puede tratarse de una pyme y en ese caso se hace por UON de ese usuario
                        If bAccesoContratos Then
                            fsnTabContratos.Visible = True
                            panMnuContratos.Visible = True
                        Else
                            fsnTabContratos.Visible = False
                            panMnuContratos.Visible = False
                        End If
                        fsnTabCalidadConfiguracion.Visible = False
                        panMnuCalidadConfiguracion.Visible = False
                        fsnTabCalEncuestas.Visible = False
                        panMnuEncuestas.Visible = False
                        fsnTabCalSolicitudesQA.Visible = False
                        panMnuSolicitudesQA.Visible = False
                        If bAccesoFSQA Then
                            If Not oUser.QAMantenimientoUNQA AndAlso Not oUser.QAMantenimientoProv Then mnuCalidad.FindItem("Configuracion").ChildItems.Remove(mnuCalidad.FindItem("Configuracion/UnidadesNegocio")) 'unis
                            If Not oUser.QAMantenimientoMat Then mnuCalidad.FindItem("Configuracion").ChildItems.Remove(mnuCalidad.FindItem("Configuracion/Materiales")) 'mat
                            If Not oUser.QAPermitirMantNivelesEscalacion Then mnuCalidad.FindItem("Configuracion").ChildItems.Remove(mnuCalidad.FindItem("Configuracion/Escalacion"))
                            If Not oAcceso.gbAccesoQANoConformidad Then mnuCalidad.Items.Remove(mnuCalidad.FindItem("NoConformidades"))
                            If Not oAcceso.gbAccesoQACertificados Then mnuCalidad.Items.Remove(mnuCalidad.FindItem("Certificados"))
                            If Not oUser.QAPuntuacionProveedores Then mnuCalidad.Items.Remove(mnuCalidad.FindItem("PanelProveedores"))
                            If Not oUser.QAPanelEscalacionProveedoresAccesible Then mnuCalidad.Items.Remove(mnuCalidad.FindItem("PanelEscalacionProveedores"))
                            If Not {oUser.QAPuntuacionProveedores, oUser.QACertifModExpiracion, oUser.QAModificarPeriodoValidezCertificados,
                                oUser.QACertifModExpiracion, oUser.QAModificarPeriodoValidezCertificados, oUser.QAContacAutom,
                                oAcceso.gbVarCal_Materiales, oUser.QAMantenimientoUNQA, oUser.QAModifProvCalidad, oUser.QAModifNotifCalif,
                                oUser.QAModifNotifProximidadNc, oUser.QAModifNotifMateriales, oUser.QACertifModExpiracion, oUser.QAContacAutom,
                                oUser.QAModifProvCalidad, oUser.QAPermisoModificarNivelesDefectoUNQA, oUser.QAPermitirMantNivelesEscalacion}.Contains(True) Then
                                mnuCalidad.Items.Remove(mnuCalidad.FindItem("Configuracion"))
                            End If
                            If Not (oUser.EsPeticionarioSolicitudesQA Or oUser.EsParticipanteSolicitudesQA) Then
                                mnuCalidad.Items.Remove(mnuCalidad.FindItem("SolicitudesQA"))
                            Else
                                If Not oUser.EsPeticionarioSolicitudesQA Then mnuCalidad.FindItem("SolicitudesQA").ChildItems.Remove(mnuCalidad.FindItem("SolicitudesQA/SolicitudesQAAlta"))
                                If Not oUser.EsParticipanteSolicitudesQA Then mnuCalidad.FindItem("SolicitudesQA").ChildItems.Remove(mnuCalidad.FindItem("SolicitudesQA/SolicitudesQASeguimiento"))
                            End If
                            If Not (oUser.EsPeticionarioEncuestas Or oUser.EsParticipanteEncuestas) Then
                                mnuCalidad.Items.Remove(mnuCalidad.FindItem("Encuestas"))
                            Else
                                If Not oUser.EsPeticionarioEncuestas Then mnuCalidad.FindItem("Encuestas").ChildItems.Remove(mnuCalidad.FindItem("Encuestas/EncuestasAlta"))
                                If Not oUser.EsParticipanteEncuestas Then mnuCalidad.FindItem("Encuestas").ChildItems.Remove(mnuCalidad.FindItem("Encuestas/EncuestasSeguimiento"))
                            End If
                        Else
                            fsnTabCalidad.Visible = False
                            panMnuCalidad.Visible = False
                        End If
                        If bAccesoFSEP Then
                            Dim bAprovisionador As Boolean = (oUser.EPTipo = TipoAccesoFSEP.SoloAprovisionador Or oUser.EPTipo = TipoAccesoFSEP.AprobadorYAprovisionador)
                            Dim bAprobador As Boolean = (oUser.EPTipo = TipoAccesoFSEP.SoloAprobador Or oUser.EPTipo = TipoAccesoFSEP.AprobadorYAprovisionador Or oUser.EsAprobadorSolicitudContraPedidoAbierto)
                            If Not bAprobador Then
                                mnuCatalogo.Items.RemoveAt(3)
                            End If
                            If Not (bAprovisionador OrElse oUser.EPPermitirEmitirPedidosDesdePedidoAbierto) Then
                                mnuCatalogo.Items.RemoveAt(1)
                                mnuCatalogo.Items.RemoveAt(0)
                            End If
                        Else
                            fsnTabCatalogo.Visible = False
                            panMnuCatalogo.Visible = False
                        End If
                        If bAccesoFSSM Then
                            If Not oUser.SMControlPresupuestario Then mnuCtrlPres.Items.Remove(mnuCtrlPres.FindItem("ControlPresupuestario"))
                            If Not oUser.SMConsultarActivos Then mnuCtrlPres.Items.Remove(mnuCtrlPres.FindItem("Activos"))
                        Else
                            fsnTabCtrlPresup.Visible = False
                            panMnuCtrlPres.Visible = False
                        End If
                        If bAccesoFACT Then
                            If Not oUser.IMAltaFacturas Then mnuFacturacion.Items.Remove(mnuFacturacion.FindItem("Alta"))
                            If Not oUser.IMSeguimientoFacturas Then mnuFacturacion.Items.Remove(mnuFacturacion.FindItem("Seguimiento"))
                            If Not oUser.IMVisorAutofacturas Then mnuFacturacion.Items.Remove(mnuFacturacion.FindItem("Autofacturacion"))
                        End If
                        If Not bAccesoFACT Or (bAccesoFACT And mnuFacturacion.Items.Count = 0) Then
                            fsnTabFacturacion.Visible = False
                            panMnuFacturacion.Visible = False
                        End If
                End Select
            End If
            InicializarControles(oUser)

            If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.Page.GetType(), "ActivarTabs") Then _
                Page.ClientScript.RegisterClientScriptBlock(Me.Page.GetType(), "ActivarTabs", ScriptActivarTabs(), True)

            If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.Page.GetType(), "MostrarOcultarMenu") Then
                Dim sScript As String = ScriptTabMouseOver()
                sScript = sScript & ScriptTabMouseOut()
                Page.ClientScript.RegisterClientScriptBlock(Me.Page.GetType(), "MostrarOcultarMenu", sScript, True)
            End If

            If Not Page.ClientScript.IsStartupScriptRegistered(Me.Page.GetType(), "IniciarMenu") Then _
                Page.ClientScript.RegisterStartupScript(Me.Page.GetType(), "IniciarMenu", "TabsOut();", True)

            If Not Page.ClientScript.IsClientScriptBlockRegistered("ParametrosEP") Then
                Dim sScript As String = "function ParametrosEP(valor){" & vbCrLf &
                    "if (valor=='Parametros'){" & vbCrLf &
                    "window.open('" & ConfigurationManager.AppSettings("rutaEP") & "ParametrosEp.aspx?Idioma=" & pag.Idioma & "&Modificar=-1','winFormatos','width=550px,height=350px, location=no,menubar=no,toolbar=no,resizable=yes,addressbar=no,scrollbars =yes');" & vbCrLf &
                    "return false;}" & vbCrLf &
                    "else{" & vbCrLf &
                    "TempCargando();" & vbCrLf &
                    "return true}}"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ParametrosEP", sScript, True)
            End If
            If Not Page.ClientScript.IsClientScriptBlockRegistered("ParametrosPM") Then
                Dim sScript As String = "function ParametrosPM(valor){" & vbCrLf &
                    "if (valor=='Parametros'){" & vbCrLf &
                    "window.open('" & ConfigurationManager.AppSettings("rutaPM2008") & "Parametros/parametrosPM.aspx','parametrosPM','width=500,height=250, location=no,menubar=no,toolbar=no,resizable=no,addressbar=no,scrollbars =no');" & vbCrLf &
                    "return false;}" & vbCrLf &
                    "else{" & vbCrLf &
                    "TempCargando();" & vbCrLf &
                    "return true}}"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ParametrosPM", sScript, True)
            End If
        End If
    End Sub
    ''' Revisado por: Jbg. Fecha: 07/11/2011
    ''' <summary>
    ''' Incializa la barra de menu con las opciones
    ''' </summary>
    ''' <remarks>Llamada desde=Page_Load; Tiempo máximo=0seg.</remarks>
    Private Sub InicializarControles(ByVal oUser As Fullstep.FSNServer.User)
        pnlSubMenu.Attributes.Add("onmouseover", "window.clearTimeout(timeMenu);")
        pnlSubMenu.Attributes.Add("onmouseout", "timeMenu=window.setTimeout(TabsOut,1500);")

        InicializarTab(fsnTabSeguridad, "seguridad")
        InicializarTab(fsnTabColaboracion, "colaboracion")
        InicializarTab(fsnTabProcesos, "procesos")
        InicializarTab(fsnTabContratos, "contratos")
        InicializarTab(fsnTabCalidad, "calidad")
        InicializarTab(fsnTabCalidadConfiguracion, "configuracion")
        InicializarTab(fsnTabCatalogo, "catalogo")
        InicializarTab(fsnTabCtrlPresup, "ctrlpresup")
        InicializarTab(fsnTabInformes, "informes")
        InicializarTab(fsnTabProcAlta, "procalta")
        InicializarTab(fsnTabProcSeguimiento, "procseguimiento")
        InicializarTab(fsnTabProcParametros, "procparametros")
        InicializarTab(fsnTabContrAlta, "contralta")
        InicializarTab(fsnTabContrSeguimiento, "contrseguimiento")
        InicializarTab(fsnTabCalPanelProveedores, "calpanelproveedores")
        InicializarTab(fsnTabCalCertificados, "calcertificados")
        InicializarTab(fsnTabCalNoConformidades, "calnoconformidades")
        InicializarTab(fsnTabCalNotificaciones, "calnotificaciones")
        InicializarTab(fsnTabCatEmision, "catemision")
        InicializarTab(fsnTabCatSeguimiento, "catseguimiento")
        InicializarTab(fsnTabCatRecepcion, "catrecepcion")
        InicializarTab(fsnTabCatAprobacion, "cataprobacion")
        InicializarTab(fsnTabCatParametros, "catparametros")
        InicializarTab(fsnTabCtrlPres, "ctrlpres")
        InicializarTab(fsnTabCtrlPresActivos, "ctrlpresactivos")
        InicializarTab(fsnTabIntegracion, "integracion")
        InicializarTab(fsnTabFacturacion, "facturacion")
        InicializarTab(fsnTabFactAlta, "factalta")
        InicializarTab(fsnTabFactSeguimiento, "factseguimiento")
        InicializarTab(fsnTabFactAutofacturacion, "factautofacturacion")
        InicializarTab(fsnTabCalSolicitudesQA, "calsolicitudesqa")
        InicializarTab(fsnTabCalEncuestas, "calencuestas")

        'PM
        mnuProcesos.Attributes.Add("align", "right")
        If Not mnuProcesos.FindItem("AltaSolicitudes") Is Nothing Then
            mnuProcesos.FindItem("AltaSolicitudes").NavigateUrl = ConfigurationManager.AppSettings("rutaPM2008") & "Tareas/AltaSolicitudes.aspx"
        End If
        If Not mnuProcesos.FindItem("Seguimiento") Is Nothing Then
            mnuProcesos.FindItem("Seguimiento").NavigateUrl = ConfigurationManager.AppSettings("rutaPM2008") & "Tareas/VisorSolicitudes.aspx"
        End If
        If Not fsnTabProcAlta Is Nothing Then
            fsnTabProcAlta.OnClientClick = "TempCargando();window.open('" & ConfigurationManager.AppSettings("rutaPM2008") & "Tareas/AltaSolicitudes.aspx" & "','_top'); return false;"
        End If
        If Not fsnTabProcSeguimiento Is Nothing Then
            fsnTabProcSeguimiento.OnClientClick = "TempCargando();window.open('" & ConfigurationManager.AppSettings("rutaPM2008") & "Tareas/VisorSolicitudes.aspx" & "','_top'); return false;"
        End If
        If Not fsnTabProcParametros Is Nothing Then
            fsnTabProcParametros.OnClientClick = "window.open('" & ConfigurationManager.AppSettings("rutaPM2008") & "Parametros/parametrosPM.aspx" & "','parametrosPM','width=500,height=250, location=no,menubar=no,toolbar=no,resizable=no,addressbar=no,scrollbars =no'); return false;"
        End If

        'Contratos
        mnuContratos.Attributes.Add("align", "right")
        If Not mnuContratos.FindItem("AltaContratos") Is Nothing Then
            mnuContratos.FindItem("AltaContratos").NavigateUrl = ConfigurationManager.AppSettings("rutaPM2008") & "contratos/AltaContratos.aspx"
        End If
        If Not mnuContratos.FindItem("Seguimiento") Is Nothing Then
            mnuContratos.FindItem("Seguimiento").NavigateUrl = ConfigurationManager.AppSettings("rutaPM2008") & "contratos/VisorContratos.aspx"
        End If
        If Not fsnTabContrAlta Is Nothing Then
            fsnTabContrAlta.OnClientClick = "TempCargando();window.open('" & ConfigurationManager.AppSettings("rutaPM2008") & "contratos/AltaContratos.aspx" & "','_top'); return false;"
        End If
        If Not fsnTabContrSeguimiento Is Nothing Then
            fsnTabContrSeguimiento.OnClientClick = "TempCargando();window.open('" & ConfigurationManager.AppSettings("rutaPM2008") & "contratos/VisorContratos.aspx" & "','_top'); return false;"
        End If

        'QA
        mnuCalidad.Attributes.Add("align", "right")
        mnuCalidadConfiguracion.Attributes.Add("align", "right")
        mnuSolicitudesQA.Attributes.Add("align", "right")
        mnuEncuestas.Attributes.Add("align", "right")
        If Not mnuCalidad.FindItem("PanelProveedores") Is Nothing Then
            mnuCalidad.FindItem("PanelProveedores").NavigateUrl = ConfigurationManager.AppSettings("rutaQA2008") & "puntuacion/panelCalidadFiltro.aspx?opcion=PanelProveedores"
        End If
        If mnuCalidad.FindItem("Configuracion") IsNot Nothing AndAlso mnuCalidad.FindItem("Configuracion").ChildItems IsNot Nothing Then
            If mnuCalidad.FindItem("Configuracion/Materiales") IsNot Nothing Then _
                mnuCalidad.FindItem("Configuracion/Materiales").NavigateUrl = ConfigurationManager.AppSettings("rutaQA") & "Frames.aspx?pagina=" & Server.UrlEncode("materiales/mantenimientomat.aspx")
            If mnuCalidad.FindItem("Configuracion/UnidadesNegocio") IsNot Nothing Then _
                mnuCalidad.FindItem("Configuracion/UnidadesNegocio").NavigateUrl = ConfigurationManager.AppSettings("rutaQA") & "Frames.aspx?pagina=" & Server.UrlEncode("materiales/UnidadesNegocio.aspx")
            If mnuCalidad.FindItem("Configuracion/Parametros") IsNot Nothing Then _
                mnuCalidad.FindItem("Configuracion/Parametros").NavigateUrl = ConfigurationManager.AppSettings("rutaQA") & "Frames.aspx?pagina=" & Server.UrlEncode("_common/parametrosgeneralesPMWEB.aspx")
            If mnuCalidad.FindItem("Configuracion/Escalacion") IsNot Nothing Then _
                mnuCalidad.FindItem("Configuracion/Escalacion").NavigateUrl = ConfigurationManager.AppSettings("rutaQA2008") & "Configuracion/ConfigNivelEscalacionProves.aspx"
        End If
        If Not mnuCalidad.FindItem("Certificados") Is Nothing Then
            mnuCalidad.FindItem("Certificados").NavigateUrl = ConfigurationManager.AppSettings("rutaQA2008") & "Certificados/Certificados.aspx"
        End If
        If Not mnuCalidad.FindItem("NoConformidades") Is Nothing Then
            mnuCalidad.FindItem("NoConformidades").NavigateUrl = ConfigurationManager.AppSettings("rutaQA2008") & "NoConformidades/NoConformidades.aspx"
        End If
        If Not mnuCalidad.FindItem("Notificaciones") Is Nothing Then
            mnuCalidad.FindItem("Notificaciones").NavigateUrl = ConfigurationManager.AppSettings("rutaFS") & "_common/Notificaciones/Notificaciones.aspx"
        End If
        If Not mnuCalidad.FindItem("PanelEscalacionProveedores") Is Nothing Then
            mnuCalidad.FindItem("PanelEscalacionProveedores").NavigateUrl = ConfigurationManager.AppSettings("rutaQA2008") & "EscalacionProveedores/PanelEscalacionProves.aspx"
        End If
        If mnuCalidad.FindItem("SolicitudesQA") IsNot Nothing AndAlso mnuCalidad.FindItem("SolicitudesQA").ChildItems IsNot Nothing Then
            If mnuCalidad.FindItem("SolicitudesQA/SolicitudesQAAlta") IsNot Nothing Then _
                mnuCalidad.FindItem("SolicitudesQA/SolicitudesQAAlta").NavigateUrl = ConfigurationManager.AppSettings("rutaPM2008") & "Tareas/AltaSolicitudes.aspx?TipoSolicitud=" & TiposDeDatos.TipoDeSolicitud.SolicitudQA
            If mnuCalidad.FindItem("SolicitudesQA/SolicitudesQASeguimiento") IsNot Nothing Then _
                mnuCalidad.FindItem("SolicitudesQA/SolicitudesQASeguimiento").NavigateUrl = ConfigurationManager.AppSettings("rutaPM2008") & "Tareas/VisorSolicitudes.aspx?TipoVisor=" & TipoVisor.SolicitudesQA
        End If
        If mnuCalidad.FindItem("Encuestas") IsNot Nothing AndAlso mnuCalidad.FindItem("Encuestas").ChildItems IsNot Nothing Then
            If mnuCalidad.FindItem("Encuestas/EncuestasAlta") IsNot Nothing Then _
                mnuCalidad.FindItem("Encuestas/EncuestasAlta").NavigateUrl = ConfigurationManager.AppSettings("rutaPM2008") & "Tareas/AltaSolicitudes.aspx?TipoSolicitud=" & TiposDeDatos.TipoDeSolicitud.Encuesta
            If mnuCalidad.FindItem("Encuestas/EncuestasSeguimiento") IsNot Nothing Then _
                mnuCalidad.FindItem("Encuestas/EncuestasSeguimiento").NavigateUrl = ConfigurationManager.AppSettings("rutaPM2008") & "Tareas/VisorSolicitudes.aspx?TipoVisor=" & TipoVisor.Encuestas
        End If

        If Not fsnTabCalPanelProveedores Is Nothing Then
            fsnTabCalPanelProveedores.OnClientClick = "TempCargando();window.open('" & ConfigurationManager.AppSettings("rutaQA2008") & "puntuacion/panelCalidadFiltro.aspx?opcion=PanelProveedores','_top'); return false;"
        End If
        If Not fsnTabCalCertificados Is Nothing Then
            fsnTabCalCertificados.OnClientClick = "TempCargando();window.open('" & ConfigurationManager.AppSettings("rutaQA2008") & "Certificados/Certificados.aspx" & "','_top'); return false;"
        End If
        If Not fsnTabCalNoConformidades Is Nothing Then
            fsnTabCalNoConformidades.OnClientClick = "TempCargando();window.open('" & ConfigurationManager.AppSettings("rutaQA2008") & "NoConformidades/NoConformidades.aspx" & "','_top'); return false;"
        End If
        If Not fsnTabCalPanelEscalacionProveedores Is Nothing Then
            fsnTabCalPanelEscalacionProveedores.OnClientClick = "TempCargando();window.open('" & ConfigurationManager.AppSettings("rutaQA2008") & "EscalacionProveedores/PanelEscalacionProves.aspx" & "','_top'); return false;"
        End If
        If mnuCalidadConfiguracion.FindItem("Materiales") IsNot Nothing Then
            mnuCalidadConfiguracion.FindItem("Materiales").NavigateUrl = ConfigurationManager.AppSettings("rutaQA") & "Frames.aspx?pagina=" & Server.UrlEncode("materiales/mantenimientomat.aspx")
        End If
        If mnuCalidadConfiguracion.FindItem("UnidadesNegocio") IsNot Nothing Then
            mnuCalidadConfiguracion.FindItem("UnidadesNegocio").NavigateUrl = ConfigurationManager.AppSettings("rutaQA") & "Frames.aspx?pagina=" & Server.UrlEncode("materiales/UnidadesNegocio.aspx")
        End If
        If mnuCalidadConfiguracion.FindItem("Parametros") IsNot Nothing Then
            mnuCalidadConfiguracion.FindItem("Parametros").NavigateUrl = ConfigurationManager.AppSettings("rutaQA") & "Frames.aspx?pagina=" & Server.UrlEncode("_common/parametrosgeneralesPMWEB.aspx")
        End If
        If mnuCalidadConfiguracion.FindItem("Escalacion") IsNot Nothing Then
            mnuCalidadConfiguracion.FindItem("Escalacion").NavigateUrl = ConfigurationManager.AppSettings("rutaQA2008") & "Configuracion/ConfigNivelEscalacionProves.aspx"
        End If
        If Not fsnTabCalNotificaciones Is Nothing Then
            fsnTabCalNotificaciones.OnClientClick = "TempCargando();window.open('" & ConfigurationManager.AppSettings("rutaFS") & "_common/Notificaciones/Notificaciones.aspx" & "','_top'); return false;"
        End If
        If panTabsCalidad.Visible AndAlso mnuCalidadConfiguracion.Items.Count = 1 Then
            If mnuCalidadConfiguracion.Items.Count = 1 Then
                mnuCalidadConfiguracion.Visible = False
                fsnTabCalidadConfiguracion.Text = mnuCalidadConfiguracion.FindItem("Parametros").Text
                fsnTabCalidadConfiguracion.OnClientClick = "TempCargando();window.open('" & ConfigurationManager.AppSettings("rutaQA") & "Frames.aspx?pagina=" & Server.UrlEncode("_common/parametrosgeneralesPMWEB.aspx") & "','_top'); return false;"
            End If
        Else
            If mnuCalidad.FindItem("Configuracion") IsNot Nothing AndAlso mnuCalidad.FindItem("Configuracion").ChildItems.Count = 1 Then
                mnuCalidad.FindItem("Configuracion").Text = mnuCalidad.FindItem("Configuracion/Parametros").Text
                mnuCalidad.FindItem("Configuracion").ChildItems.Remove(mnuCalidad.FindItem("Configuracion/Parametros"))
                mnuCalidad.FindItem("Configuracion").Selectable = True
                mnuCalidad.FindItem("Configuracion").NavigateUrl = ConfigurationManager.AppSettings("rutaQA") & "Frames.aspx?pagina=" & Server.UrlEncode("_common/parametrosgeneralesPMWEB.aspx")
            End If
        End If
        If mnuSolicitudesQA.FindItem("SolicitudesQAAlta") IsNot Nothing Then
            mnuSolicitudesQA.FindItem("SolicitudesQAAlta").NavigateUrl = ConfigurationManager.AppSettings("rutaPM2008") & "Tareas/AltaSolicitudes.aspx?TipoSolicitud=" & TiposDeDatos.TipoDeSolicitud.SolicitudQA
        End If
        If mnuSolicitudesQA.FindItem("SolicitudesQASeguimiento") IsNot Nothing Then
            mnuSolicitudesQA.FindItem("SolicitudesQASeguimiento").NavigateUrl = ConfigurationManager.AppSettings("rutaPM2008") & "Tareas/VisorSolicitudes.aspx?TipoVisor=" & TipoVisor.SolicitudesQA
        End If
        If mnuEncuestas.FindItem("EncuestasAlta") IsNot Nothing Then
            mnuEncuestas.FindItem("EncuestasAlta").NavigateUrl = ConfigurationManager.AppSettings("rutaPM2008") & "Tareas/AltaSolicitudes.aspx?TipoSolicitud=" & TiposDeDatos.TipoDeSolicitud.Encuesta
        End If
        If mnuEncuestas.FindItem("EncuestasSeguimiento") IsNot Nothing Then
            mnuEncuestas.FindItem("EncuestasSeguimiento").NavigateUrl = ConfigurationManager.AppSettings("rutaPM2008") & "Tareas/VisorSolicitudes.aspx?TipoVisor=" & TipoVisor.Encuestas
        End If
        If panTabsCalidad.Visible AndAlso mnuSolicitudesQA.Items.Count = 1 Then
            If mnuSolicitudesQA.Items.Count = 1 Then
                mnuSolicitudesQA.Visible = False
                If oUser.EsPeticionarioSolicitudesQA Then
                    fsnTabCalSolicitudesQA.OnClientClick = "TempCargando();window.open('" & ConfigurationManager.AppSettings("rutaPM2008") & "Tareas/AltaSolicitudes.aspx?TipoSolicitud=" & TiposDeDatos.TipoDeSolicitud.SolicitudQA & "','_top'); return false;"
                Else
                    fsnTabCalSolicitudesQA.OnClientClick = "TempCargando();window.open('" & ConfigurationManager.AppSettings("rutaPM2008") & "Tareas/VisorSolicitudes.aspx?TipoVisor=" & TipoVisor.SolicitudesQA & "','_top'); return false;"
                End If
            End If
        Else
            If mnuCalidad.FindItem("SolicitudesQA") IsNot Nothing AndAlso mnuCalidad.FindItem("SolicitudesQA").ChildItems.Count = 1 Then
                If oUser.EsPeticionarioSolicitudesQA Then
                    mnuCalidad.FindItem("SolicitudesQA").ChildItems.Remove(mnuCalidad.FindItem("SolicitudesQA/SolicitudesQAAlta"))
                    mnuCalidad.FindItem("SolicitudesQA").Selectable = True
                    mnuCalidad.FindItem("SolicitudesQA").NavigateUrl = ConfigurationManager.AppSettings("rutaPM2008") & "Tareas/AltaSolicitudes.aspx?TipoSolicitud=" & TiposDeDatos.TipoDeSolicitud.SolicitudQA
                Else
                    mnuCalidad.FindItem("SolicitudesQA").ChildItems.Remove(mnuCalidad.FindItem("SolicitudesQA/SolicitudesQASeguimiento"))
                    mnuCalidad.FindItem("SolicitudesQA").Selectable = True
                    mnuCalidad.FindItem("SolicitudesQA").NavigateUrl = ConfigurationManager.AppSettings("rutaPM2008") & "Tareas/VisorSolicitudes.aspx?TipoVisor=" & TipoVisor.SolicitudesQA
                End If
            End If
        End If
        If panTabsCalidad.Visible AndAlso mnuEncuestas.Items.Count = 1 Then
            If mnuEncuestas.Items.Count = 1 Then
                mnuEncuestas.Visible = False
                If oUser.EsPeticionarioEncuestas Then
                    fsnTabCalEncuestas.OnClientClick = "TempCargando();window.open('" & ConfigurationManager.AppSettings("rutaPM2008") & "Tareas/AltaSolicitudes.aspx?TipoSolicitud=" & TiposDeDatos.TipoDeSolicitud.Encuesta & "','_top'); return false;"
                Else
                    fsnTabCalEncuestas.OnClientClick = "TempCargando();window.open('" & ConfigurationManager.AppSettings("rutaPM2008") & "Tareas/VisorSolicitudes.aspx?TipoVisor=" & TipoVisor.Encuestas & "','_top'); return false;"
                End If
            End If
        Else
            If mnuCalidad.FindItem("Encuestas") IsNot Nothing AndAlso mnuCalidad.FindItem("Encuestas").ChildItems.Count = 1 Then
                If oUser.EsPeticionarioEncuestas Then
                    mnuCalidad.FindItem("Encuestas").ChildItems.Remove(mnuCalidad.FindItem("Encuestas/EncuestasAlta"))
                    mnuCalidad.FindItem("Encuestas").Selectable = True
                    mnuCalidad.FindItem("Encuestas").NavigateUrl = ConfigurationManager.AppSettings("rutaPM2008") & "Tareas/AltaSolicitudes.aspx?TipoSolicitud=" & TiposDeDatos.TipoDeSolicitud.Encuesta
                Else
                    mnuCalidad.FindItem("Encuestas").ChildItems.Remove(mnuCalidad.FindItem("Encuestas/EncuestasSeguimiento"))
                    mnuCalidad.FindItem("Encuestas").Selectable = True
                    mnuCalidad.FindItem("Encuestas").NavigateUrl = ConfigurationManager.AppSettings("rutaPM2008") & "Tareas/VisorSolicitudes.aspx?TipoVisor=" & TipoVisor.Encuestas
                End If
            End If
        End If

        'EP
        mnuCatalogo.Attributes.Add("align", "right")
        If Not (oUser.EPTipo = TipoAccesoFSEP.SoloAprovisionador OrElse oUser.EPTipo = TipoAccesoFSEP.AprobadorYAprovisionador) Then
            fsnTabCatEmision.OnClientClick = "TempCargando();window.open('" & ConfigurationManager.AppSettings("rutaEP") & "PedidoAbierto.aspx" & "','_top'); return false;"
        Else
            fsnTabCatEmision.OnClientClick = "TempCargando();window.open('" & ConfigurationManager.AppSettings("rutaEP") & "CatalogoWeb.aspx" & "','_top'); return false;"
        End If
        fsnTabCatSeguimiento.OnClientClick = "TempCargando();window.open('" & ConfigurationManager.AppSettings("rutaEP") & "Seguimiento.aspx" & "','_top'); return false;"
        fsnTabCatRecepcion.OnClientClick = "TempCargando();window.open('" & ConfigurationManager.AppSettings("rutaEP") & "Recepcion.aspx" & "','_top'); return false;"
        fsnTabCatAprobacion.OnClientClick = "TempCargando();window.open('" & ConfigurationManager.AppSettings("rutaEP") & "Aprobacion.aspx" & "','_top'); return false;"
        For Each x As MenuItem In mnuCatalogo.Items
            Select Case x.Value
                Case "Emision"
                    If Not (oUser.EPTipo = TipoAccesoFSEP.SoloAprovisionador OrElse oUser.EPTipo = TipoAccesoFSEP.AprobadorYAprovisionador) Then
                        x.NavigateUrl = ConfigurationManager.AppSettings("rutaEP") & "PedidoAbierto.aspx"
                    Else
                        x.NavigateUrl = ConfigurationManager.AppSettings("rutaEP") & "CatalogoWeb.aspx"
                    End If
                Case "Seguimiento"
                    x.NavigateUrl = ConfigurationManager.AppSettings("rutaEP") & "Seguimiento.aspx"
                Case "Recepcion"
                    x.NavigateUrl = ConfigurationManager.AppSettings("rutaEP") & "Recepcion.aspx"
                Case "Aprobacion"
                    x.NavigateUrl = ConfigurationManager.AppSettings("rutaEP") & "Aprobacion.aspx"
            End Select
        Next

        'SM
        mnuCtrlPres.Attributes.Add("align", "right")
        'debajo de la pestaña Control presupuestario
        If mnuCtrlPres.Visible Then
            If Not mnuCtrlPres.FindItem("ControlPresupuestario") Is Nothing Then
                mnuCtrlPres.FindItem("ControlPresupuestario").NavigateUrl = ConfigurationManager.AppSettings("rutaSM") & "ControlGasto/ControlPresupuestario.aspx"
            End If
            If Not mnuCtrlPres.FindItem("Activos") Is Nothing Then
                mnuCtrlPres.FindItem("Activos").NavigateUrl = ConfigurationManager.AppSettings("rutaSM") & "Activos/Activos.aspx"
            End If
        End If
        '1era linea de menu
        If panTabsCtrlPresup.Visible Then
            If Not fsnTabCtrlPres Is Nothing Then
                fsnTabCtrlPres.OnClientClick = "TempCargando();window.open('" & ConfigurationManager.AppSettings("rutaSM") & "script/ControlGasto/ControlPresupuestario.aspx" & "','_top'); return false;"
            End If
            If Not fsnTabCtrlPresActivos Is Nothing Then
                fsnTabCtrlPresActivos.OnClientClick = "TempCargando();window.open('" & ConfigurationManager.AppSettings("rutaSM") & "Activos/Activos.aspx" & "','_top'); return false;"
            End If
        End If

        'Informes
        If mnuInformes.Items.Count = 0 Then
            fsnTabInformes.OnClientClick = "TempCargando();window.open('" & ConfigurationManager.AppSettings("rutaFS") & "_Common/Informes/Informes.aspx" & "','_top'); return false;"
        Else
            fsnTabInformes.OnClientClick = "return false;"
            mnuInformes.Attributes.Add("align", "right")
            For Each x As MenuItem In mnuInformes.Items
                Select Case x.Value
                    Case "Informes"
                        x.NavigateUrl = ConfigurationManager.AppSettings("rutaFS") & "_Common/Informes/Informes.aspx"
                    Case "BusinessInteligence"
                        x.NavigateUrl = ConfigurationManager.AppSettings("rutaFS") & "BI/BI.aspx"
                    Case "QlikViewer"
                        x.NavigateUrl = ConfigurationManager.AppSettings("rutaFS") & "BI/VisorQlik.aspx"
                    Case "Visor Actividad"
                        x.NavigateUrl = ConfigurationManager.AppSettings("rutaFS") & "BI/VisorActividad.aspx"
                    Case "Visor Rendimiento"
                        x.NavigateUrl = ConfigurationManager.AppSettings("rutaFS") & "BI/VisorRendimiento.aspx"
                    Case "Cubos"
                        x.NavigateUrl = ConfigurationManager.AppSettings("rutaFS") & "BI/Cubos.aspx"
                    Case "QlikApps"
                        x.NavigateUrl = ConfigurationManager.AppSettings("rutaFS") & "BI/QlikApps.aspx"
                End Select
            Next
        End If

        'ColaboraciÃ³n
        fsnTabColaboracion.OnClientClick = "TempCargando();window.open('" & ConfigurationManager.AppSettings("rutaFS") & "CN/cn_MuroUsuario.aspx" & "','_top'); return false;"

        'Integracion
        fsnTabIntegracion.OnClientClick = "TempCargando();window.open('" & ConfigurationManager.AppSettings("rutaFS") & "INT/VisorIntegracion.aspx" & "','_top'); return false;"

        'Facturación
        mnuFacturacion.Attributes.Add("align", "right")
        fsnTabFactAlta.OnClientClick = "TempCargando();window.open('" & ConfigurationManager.AppSettings("rutaPM2008") & "Tareas/AltaSolicitudes.aspx?TipoSolicitud=13" & "','_top'); return false;"
        fsnTabFactSeguimiento.OnClientClick = "TempCargando();window.open('" & ConfigurationManager.AppSettings("rutaPM2008") & "Tareas/VisorSolicitudes.aspx?TipoVisor=5" & "','_top'); return false;"
        fsnTabFactAutofacturacion.OnClientClick = "TempCargando();window.open('" & ConfigurationManager.AppSettings("rutaPM2008") & "Facturas/VisorFacturas.aspx" & "','_top'); return false;"
        For Each x As MenuItem In mnuFacturacion.Items
            Select Case x.Value
                Case "Alta"
                    x.NavigateUrl = ConfigurationManager.AppSettings("rutaPM2008") & "Tareas/AltaSolicitudes.aspx?TipoSolicitud=13"
                Case "Seguimiento"
                    x.NavigateUrl = ConfigurationManager.AppSettings("rutaPM2008") & "Tareas/VisorSolicitudes.aspx?TipoVisor=5"
                Case "Autofacturacion"
                    x.NavigateUrl = ConfigurationManager.AppSettings("rutaPM2008") & "Facturas/VisorFacturas.aspx"
            End Select
        Next

        'Seguridad
        mnuSeguridad.Attributes.Add("align", "right")
        mnuSeguridad.FindItem("PerfilesSeguridad").NavigateUrl = ConfigurationManager.AppSettings("rutaFS") & "SG/VisorPerfiles.aspx"
    End Sub
    ''' <summary>
    ''' Genera las funciones javascript para activar y desactivar las pestañas
    ''' </summary>
    ''' <returns>Código javascript de las funciones</returns>
    ''' <remarks></remarks>
    Private Function ScriptActivarTabs() As String
        Dim sBrowser As String = ""
        Dim Version As Integer
        Dim arBrowsers As New ArrayList
        sBrowser = Request.Browser.Browser
        Version = Request.Browser.MajorVersion
        arBrowsers = Request.Browser.Browsers
        Dim sScript As String = "function activarTab(nombre, colorfondo, colorletra) {" & vbCrLf &
            "var mtab = document.getElementById(nombre);" & vbCrLf &
            "if (mtab){" & vbCrLf &
            "mtab.style.backgroundColor = colorfondo;" & vbCrLf &
            "mtab.style.color = colorletra; " & vbCrLf
        If sBrowser <> "IE" Or Version < 7 Then
            sScript = sScript & "if (mtab.childNodes.length>2) {" & vbCrLf &
                "mtab.childNodes[1].style.backgroundPosition = '0px -12px';" & vbCrLf &
                "mtab.childNodes[2].style.backgroundPosition = '-6px -12px';" & vbCrLf &
                "}" & vbCrLf
        Else
            sScript = sScript & "var tabla = mtab.childNodes[0];" & vbCrLf &
                "try{" & vbCrLf &
                "tabla.style.backgroundColor = colorfondo;" & vbCrLf &
                "tabla.style.color = colorletra;" & vbCrLf &
                "tabla.rows[0].cells[0].style.backgroundPosition = '0px -12px';" & vbCrLf &
                "tabla.rows[0].cells[2].style.backgroundPosition = '-6px -12px';" & vbCrLf &
                "}" & vbCrLf & "catch(e) {}" & vbCrLf
        End If
        sScript = sScript & "}" & vbCrLf & "}" & vbCrLf &
            "function desactivarTab(nombre, colorfondo, colorletra) {" & vbCrLf &
            "try{" & vbCrLf &
            "var mtab = document.getElementById(nombre);" & vbCrLf &
            "if (mtab){" & vbCrLf &
            "mtab.style.backgroundColor = colorfondo;" & vbCrLf &
            "mtab.style.color = colorletra;" & vbCrLf
        If sBrowser <> "IE" Or Version < 7 Then
            sScript = sScript & "if (mtab.childNodes.length>2) {" & vbCrLf &
                "mtab.childNodes[1].style.backgroundPosition = '0px 0px';" & vbCrLf &
                "mtab.childNodes[2].style.backgroundPosition = '-6px 0px';" & vbCrLf &
                "}" & vbCrLf
        Else
            sScript = sScript & "var tabla = mtab.childNodes[0];" & vbCrLf &
                "try{" & vbCrLf &
                "tabla.style.backgroundColor = colorfondo;" & vbCrLf &
                "tabla.style.color = colorletra;" & vbCrLf &
                "tabla.rows[0].cells[0].style.backgroundPosition = '0px 0px';" & vbCrLf &
                "tabla.rows[0].cells[2].style.backgroundPosition = '-6px 0px';" & vbCrLf &
                "}" & vbCrLf & "catch(e) {}" & vbCrLf
        End If
        sScript = sScript & "}" & vbCrLf _
        & "}" & vbCrLf & "catch(e) {}" & vbCrLf _
        & "}" & vbCrLf
        Return sScript
    End Function
    ''' Revisado por: Jbg. Fecha: 28/11/2011
    ''' <summary>
    ''' Genera las funciones javascript para el mouse over de las pestañas
    ''' </summary>
    ''' <returns>Código javascript de las funciones</returns>
    ''' <remarks>Llamada desde: menu_load ; Tiempo máximo: 0</remarks>
    Private Function ScriptTabMouseOver() As String
        Dim sScript As String = "function TabMouseOver(tab){" & vbCrLf
        If fsnTabSeguridad.Visible Then sScript = sScript & TabScriptMouseOver(fsnTabSeguridad, "seguridad", panMnuSeguridad)
        If fsnTabColaboracion.Visible Then sScript = sScript & TabScriptMouseOver(fsnTabColaboracion, "colaboracion", Nothing)
        If fsnTabProcesos.Visible Then sScript = sScript & TabScriptMouseOver(fsnTabProcesos, "procesos", panMnuProcesos)
        If fsnTabContratos.Visible Then sScript = sScript & TabScriptMouseOver(fsnTabContratos, "contratos", panMnuContratos)
        If fsnTabCalidad.Visible Then sScript = sScript & TabScriptMouseOver(fsnTabCalidad, "calidad", panMnuCalidad)
        If fsnTabCalidadConfiguracion.Visible Then sScript = sScript & TabScriptMouseOver(fsnTabCalidadConfiguracion, "configuracion", panMnuCalidadConfiguracion)
        If fsnTabCatalogo.Visible Then sScript = sScript & TabScriptMouseOver(fsnTabCatalogo, "catalogo", panMnuCatalogo)
        If fsnTabCtrlPresup.Visible Then sScript = sScript & TabScriptMouseOver(fsnTabCtrlPresup, "ctrlpresup", panMnuCtrlPres)
        If fsnTabFacturacion.Visible Then sScript = sScript & TabScriptMouseOver(fsnTabFacturacion, "facturacion", panMnuFacturacion)
        If fsnTabInformes.Visible Then sScript = sScript & TabScriptMouseOver(fsnTabInformes, "informes", panMnuInformes)
        If fsnTabProcAlta.Visible Then sScript = sScript & TabScriptMouseOver(fsnTabProcAlta, "procalta", Nothing)
        If fsnTabProcSeguimiento.Visible Then sScript = sScript & TabScriptMouseOver(fsnTabProcSeguimiento, "procseguimiento", Nothing)
        If fsnTabProcParametros.Visible Then sScript = sScript & TabScriptMouseOver(fsnTabProcParametros, "procparametros", Nothing)
        If fsnTabContrAlta.Visible Then sScript = sScript & TabScriptMouseOver(fsnTabContrAlta, "contralta", Nothing)
        If fsnTabContrSeguimiento.Visible Then sScript = sScript & TabScriptMouseOver(fsnTabContrSeguimiento, "contrseguimiento", Nothing)
        If fsnTabCalPanelProveedores.Visible Then sScript = sScript & TabScriptMouseOver(fsnTabCalPanelProveedores, "calpanelproveedores", Nothing)
        If fsnTabCalCertificados.Visible Then sScript = sScript & TabScriptMouseOver(fsnTabCalCertificados, "calcertificados", Nothing)
        If fsnTabCalNoConformidades.Visible Then sScript = sScript & TabScriptMouseOver(fsnTabCalNoConformidades, "calnoconformidades", Nothing)
        If fsnTabCalSolicitudesQA.Visible Then sScript = sScript & TabScriptMouseOver(fsnTabCalSolicitudesQA, "calsolicitudesqa", panMnuSolicitudesQA)
        If fsnTabCalEncuestas.Visible Then sScript = sScript & TabScriptMouseOver(fsnTabCalEncuestas, "calencuestas", panMnuEncuestas)
        If fsnTabCalNotificaciones.Visible Then sScript = sScript & TabScriptMouseOver(fsnTabCalNotificaciones, "calnotificaciones", Nothing)
        If fsnTabCatEmision.Visible Then sScript = sScript & TabScriptMouseOver(fsnTabCatEmision, "catemision", Nothing)
        If fsnTabCatSeguimiento.Visible Then sScript = sScript & TabScriptMouseOver(fsnTabCatSeguimiento, "catseguimiento", Nothing)
        If fsnTabCatRecepcion.Visible Then sScript = sScript & TabScriptMouseOver(fsnTabCatRecepcion, "catrecepcion", Nothing)
        If fsnTabCatAprobacion.Visible Then sScript = sScript & TabScriptMouseOver(fsnTabCatAprobacion, "cataprobacion", Nothing)
        If fsnTabCatParametros.Visible Then sScript = sScript & TabScriptMouseOver(fsnTabCatParametros, "catparametros", Nothing)
        If fsnTabCtrlPres.Visible Then sScript = sScript & TabScriptMouseOver(fsnTabCtrlPres, "ctrlpres", Nothing)
        If fsnTabCtrlPresActivos.Visible Then sScript = sScript & TabScriptMouseOver(fsnTabCtrlPresActivos, "ctrlpresactivos", Nothing)
        If fsnTabIntegracion.Visible Then sScript = sScript & TabScriptMouseOver(fsnTabIntegracion, "integracion", Nothing)
        If fsnTabFactAlta.Visible Then sScript = sScript & TabScriptMouseOver(fsnTabFactAlta, "factalta", Nothing)
        If fsnTabFactSeguimiento.Visible Then sScript = sScript & TabScriptMouseOver(fsnTabFactSeguimiento, "factseguimiento", Nothing)
        If fsnTabFactAutofacturacion.Visible Then sScript = sScript & TabScriptMouseOver(fsnTabFactAutofacturacion, "factautofacturacion", Nothing)
        sScript = sScript & " }"
        Return sScript
    End Function
    ''' Revisado por: Jbg. Fecha: 28/11/2011
    ''' <summary>
    ''' Genera las funciones javascript para el mouse out de las pestañas
    ''' </summary>
    ''' <returns>Código javascript de las funciones</returns>
    ''' <remarks>Llamada desde: menu_load ; Tiempo máximo: 0</remarks>
    Private Function ScriptTabMouseOut() As String
        Dim sScript As String = "function TabsOut(){" & vbCrLf &
            "window.clearTimeout(timeMenu);" & vbCrLf
        If fsnTabSeguridad.Visible Then sScript = sScript & TabScriptMouseOut(fsnTabSeguridad, panMnuSeguridad)
        If fsnTabColaboracion.Visible Then sScript = sScript & TabScriptMouseOut(fsnTabColaboracion, Nothing)
        If fsnTabProcesos.Visible Then sScript = sScript & TabScriptMouseOut(fsnTabProcesos, panMnuProcesos)
        If fsnTabContratos.Visible Then sScript = sScript & TabScriptMouseOut(fsnTabContratos, panMnuContratos)
        If fsnTabCalidad.Visible Then sScript = sScript & TabScriptMouseOut(fsnTabCalidad, panMnuCalidad)
        If fsnTabCalidadConfiguracion.Visible Then sScript = sScript & TabScriptMouseOut(fsnTabCalidadConfiguracion, panMnuCalidadConfiguracion)
        If fsnTabCatalogo.Visible Then sScript = sScript & TabScriptMouseOut(fsnTabCatalogo, panMnuCatalogo)
        If fsnTabCtrlPresup.Visible Then sScript = sScript & TabScriptMouseOut(fsnTabCtrlPresup, panMnuCtrlPres)
        If fsnTabFacturacion.Visible Then sScript = sScript & TabScriptMouseOut(fsnTabFacturacion, panMnuFacturacion)
        If fsnTabInformes.Visible Then sScript = sScript & TabScriptMouseOut(fsnTabInformes, panMnuInformes)
        If fsnTabProcAlta.Visible Then sScript = sScript & TabScriptMouseOut(fsnTabProcAlta, Nothing)
        If fsnTabProcSeguimiento.Visible Then sScript = sScript & TabScriptMouseOut(fsnTabProcSeguimiento, Nothing)
        If fsnTabProcParametros.Visible Then sScript = sScript & TabScriptMouseOut(fsnTabProcParametros, Nothing)
        If fsnTabContrAlta.Visible Then sScript = sScript & TabScriptMouseOut(fsnTabContrAlta, Nothing)
        If fsnTabContrSeguimiento.Visible Then sScript = sScript & TabScriptMouseOut(fsnTabContrSeguimiento, Nothing)
        If fsnTabCalPanelProveedores.Visible Then sScript = sScript & TabScriptMouseOut(fsnTabCalPanelProveedores, Nothing)
        If fsnTabCalCertificados.Visible Then sScript = sScript & TabScriptMouseOut(fsnTabCalCertificados, Nothing)
        If fsnTabCalNoConformidades.Visible Then sScript = sScript & TabScriptMouseOut(fsnTabCalNoConformidades, Nothing)
        If fsnTabCalSolicitudesQA.Visible Then sScript = sScript & TabScriptMouseOut(fsnTabCalSolicitudesQA, panMnuSolicitudesQA)
        If fsnTabCalEncuestas.Visible Then sScript = sScript & TabScriptMouseOut(fsnTabCalEncuestas, panMnuEncuestas)
        If fsnTabCalNotificaciones.Visible Then sScript = sScript & TabScriptMouseOut(fsnTabCalNotificaciones, Nothing)
        If fsnTabCatEmision.Visible Then sScript = sScript & TabScriptMouseOut(fsnTabCatEmision, Nothing)
        If fsnTabCatSeguimiento.Visible Then sScript = sScript & TabScriptMouseOut(fsnTabCatSeguimiento, Nothing)
        If fsnTabCatRecepcion.Visible Then sScript = sScript & TabScriptMouseOut(fsnTabCatRecepcion, Nothing)
        If fsnTabCatParametros.Visible Then sScript = sScript & TabScriptMouseOut(fsnTabCatParametros, Nothing)
        If fsnTabCtrlPres.Visible Then sScript = sScript & TabScriptMouseOut(fsnTabCtrlPres, Nothing)
        If fsnTabCtrlPresActivos.Visible Then sScript = sScript & TabScriptMouseOut(fsnTabCtrlPresActivos, Nothing)
        If fsnTabIntegracion.Visible Then sScript = sScript & TabScriptMouseOut(fsnTabIntegracion, Nothing)
        If fsnTabFactAlta.Visible Then sScript = sScript & TabScriptMouseOut(fsnTabFactAlta, Nothing)
        If fsnTabFactSeguimiento.Visible Then sScript = sScript & TabScriptMouseOut(fsnTabFactSeguimiento, Nothing)
        If fsnTabFactAutofacturacion.Visible Then sScript = sScript & TabScriptMouseOut(fsnTabFactAutofacturacion, Nothing)
        sScript = sScript & "}"
        Return sScript
    End Function
    Private Sub InicializarTab(ByVal TabControl As FSNWebControls.FSNTab, ByVal Cod As String)
        TabControl.Attributes.Add("onmouseover", "window.clearTimeout(timeMenu); TabMouseOver('" & Cod & "'); return false")
        TabControl.Attributes.Add("onmouseout", "timeMenu=window.setTimeout(TabsOut,1000); return false")
    End Sub
    Private Function TabScriptMouseOver(ByVal TabControl As FSNWebControls.FSNTab, ByVal Cod As String, ByVal PanelControl As Panel) As String
        Dim sScript As String = "if (tab=='" & Cod & "'){" & vbCrLf &
            " activarTab('" & TabControl.ClientID & "', '" & Drawing.ColorTranslator.ToHtml(TabControl.ColorFondoHover) & "', '" & Drawing.ColorTranslator.ToHtml(TabControl.ColorLetraHover) & "');" & vbCrLf
        If Not PanelControl Is Nothing Then
            sScript = sScript & " mostrarsubmenu('" & PanelControl.ClientID & "');" & vbCrLf
        End If
        sScript = sScript & " } else {" & vbCrLf &
            " desactivarTab('" & TabControl.ClientID & "', '" & Drawing.ColorTranslator.ToHtml(TabControl.ColorFondo) & "', '" & Drawing.ColorTranslator.ToHtml(TabControl.ColorLetra) & "');" & vbCrLf
        If Not PanelControl Is Nothing Then
            sScript = sScript & " ocultarsubmenu('" & PanelControl.ClientID & "');" & vbCrLf
        End If
        sScript = sScript & " }" & vbCrLf
        Return sScript
    End Function
    Private Function TabScriptMouseOut(ByVal TabControl As FSNWebControls.FSNTab, ByVal PanelControl As Panel) As String
        Dim sScript As String
        If TabControl.Selected Then
            sScript = "activarTab('" & TabControl.ClientID & "', '" & Drawing.ColorTranslator.ToHtml(TabControl.ColorFondoHover) & "', '" & Drawing.ColorTranslator.ToHtml(TabControl.ColorLetraHover) & "');" & vbCrLf
            If Not PanelControl Is Nothing Then _
                sScript = sScript & "mostrarsubmenu('" & PanelControl.ClientID & "');" & vbCrLf
        Else
            sScript = "desactivarTab('" & TabControl.ClientID & "', '" & Drawing.ColorTranslator.ToHtml(TabControl.ColorFondo) & "', '" & Drawing.ColorTranslator.ToHtml(TabControl.ColorLetra) & "');" & vbCrLf
            If Not PanelControl Is Nothing Then _
            sScript = sScript & "ocultarsubmenu('" & PanelControl.ClientID & "');" & vbCrLf
        End If
        Return sScript
    End Function
    Public ReadOnly Property FSNServer() As FSNServer.Root
        Get
            If _FSNServer Is Nothing Then
                _FSNServer = Session("FSN_Server")
            End If
            Return _FSNServer
        End Get
    End Property
    ''' <summary>
    ''' Función que devuelve los 
    ''' </summary>
    ''' <param name="bAccesoFSEP"></param>
    ''' <param name="bAccesoFSPM"></param>
    ''' <param name="bAccesoFSQA"></param>
    ''' <param name="oUser"></param>
    ''' <param name="ParamGen"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function MostrarInformes(ByVal bAccesoFSEP As Boolean, ByVal bAccesoFSPM As Boolean, ByVal bAccesoFSQA As Boolean, ByVal oUser As FSNServer.User, ByVal ParamGen As ParametrosGenerales) As Boolean
        Dim bMostrarInformes As Boolean = False
        If bAccesoFSEP Then 'en EP hay informes. Si tiene acceso a EP se tiene que mostrar la pestaña Informes
            bMostrarInformes = True 'no hace falta comprobar que tiene acceso a otras aplicaciones.
        Else
            Dim oListados As FSNServer.Listados = FSNServer.Get_Object(GetType(FSNServer.Listados))
            If bAccesoFSPM And bAccesoFSQA Then
                oListados.LoadListadosPersonalizados(oUser.Cod, True, True)
                If oListados.Data.Tables(0).Rows.Count > 0 Or oListados.Data.Tables(1).Rows.Count > 0 Then
                    bMostrarInformes = True
                End If
            Else
                If ParamGen.gsAccesoFSPM <> FSNLibrary.TipoAccesoFSPM.SinAcceso And oUser.AccesoPM = True Then
                    oListados.LoadListadosPersonalizados(oUser.Cod, True, False)
                ElseIf ParamGen.gsAccesoFSQA <> FSNLibrary.TipoAccesoFSQA.SinAcceso And oUser.AccesoQA = True Then
                    oListados.LoadListadosPersonalizados(oUser.Cod, False, True)
                End If
                If Not oListados.Data Is Nothing Then
                    If oListados.Data.Tables(0).Rows.Count > 0 Then
                        bMostrarInformes = True
                    End If
                End If
            End If
        End If
        Return bMostrarInformes
    End Function
    ''' Revisado por: Jbg. Fecha: 28/11/2011
    ''' <summary>
    ''' Carga los textos del menu y submenu
    ''' </summary>
    ''' <remarks>Llamada desde: Page_Load; Tiempo máximo: 0 sg;</remarks>
    Private Sub CargarTextos()
        If TypeOf Me.Page Is FSNServer.IFSNPage Then
            Dim pag As FSNServer.IFSNPage = Me.Page
            Dim modulo As TiposDeDatos.ModulosIdiomas = pag.ModuloIdioma
            Dim oUser As FSNServer.User = pag.Usuario

            pag.ModuloIdioma = ModulosIdiomas.Menu
            fsnTabProcesos.Text = pag.Textos(8)
            fsnTabContratos.Text = pag.Textos(33)
            fsnTabCalidad.Text = pag.Textos(9)
            fsnTabCalidadConfiguracion.Text = pag.Textos(42)
            fsnTabCatalogo.Text = pag.Textos(21)

            fsnTabInformes.Text = pag.Textos(25)
            mnuInformes.FindItem("Informes").Text = pag.Textos(67)
            mnuInformes.FindItem("BusinessInteligence").Text = pag.Textos(73)
            mnuInformes.FindItem("Cubos").Text = pag.Textos(74)
            mnuInformes.FindItem("Visor Actividad").Text = pag.Textos(70)
            mnuInformes.FindItem("Visor Rendimiento").Text = pag.Textos(71)
            mnuInformes.FindItem("QlikViewer").Text = pag.Textos(75)
            mnuInformes.FindItem("QlikApps").Text = pag.Textos(76)

            fsnTabProcAlta.Text = pag.Textos(1)
            mnuProcesos.Items(0).Text = pag.Textos(1)
            fsnTabProcSeguimiento.Text = pag.Textos(2)
            mnuProcesos.Items(1).Text = pag.Textos(2)
            fsnTabProcParametros.Text = pag.Textos(14)
            mnuProcesos.Items(2).Text = pag.Textos(14)

            fsnTabContrAlta.Text = pag.Textos(62)
            mnuContratos.Items(0).Text = pag.Textos(62)
            fsnTabContrSeguimiento.Text = pag.Textos(2)
            mnuContratos.Items(1).Text = pag.Textos(2)

            fsnTabCalPanelEscalacionProveedores.Text = pag.Textos(79)
            mnuCalidad.FindItem("PanelEscalacionProveedores").Text = pag.Textos(79)
            fsnTabCalPanelProveedores.Text = pag.Textos(12)
            mnuCalidad.FindItem("PanelProveedores").Text = pag.Textos(12)
            mnuCalidad.FindItem("Configuracion").Text = pag.Textos(42)
            mnuCalidadConfiguracion.FindItem("Materiales").Text = pag.Textos(13)
            mnuCalidad.FindItem("Configuracion/Materiales").Text = pag.Textos(13)
            fsnTabCalCertificados.Text = pag.Textos(11)
            mnuCalidad.FindItem("Certificados").Text = pag.Textos(11)
            fsnTabCalNoConformidades.Text = pag.Textos(10)
            mnuCalidad.FindItem("NoConformidades").Text = pag.Textos(10)
            If oUser.QAMantenimientoUNQA Then
                mnuCalidadConfiguracion.FindItem("UnidadesNegocio").Text = pag.Textos(18)
                mnuCalidad.FindItem("Configuracion/UnidadesNegocio").Text = pag.Textos(18)
            ElseIf oUser.QAMantenimientoProv Then
                mnuCalidadConfiguracion.FindItem("UnidadesNegocio").Text = pag.Textos(32)
                mnuCalidad.FindItem("Configuracion/UnidadesNegocio").Text = pag.Textos(32)
            End If
            If oUser.QAPermitirMantNivelesEscalacion Then
                mnuCalidadConfiguracion.FindItem("Escalacion").Text = pag.Textos(78)
                mnuCalidad.FindItem("Configuracion/Escalacion").Text = pag.Textos(78)
            End If
            mnuCalidadConfiguracion.FindItem("Parametros").Text = pag.Textos(14)
            mnuCalidad.FindItem("Configuracion/Parametros").Text = pag.Textos(14)
            mnuCalidad.FindItem("SolicitudesQA").Text = pag.Textos(82)
            mnuCalidad.FindItem("SolicitudesQA/SolicitudesQAAlta").Text = pag.Textos(84)
            mnuCalidad.FindItem("SolicitudesQA/SolicitudesQASeguimiento").Text = pag.Textos(85)
            mnuCalidad.FindItem("Encuestas").Text = pag.Textos(83)
            mnuCalidad.FindItem("Encuestas/EncuestasAlta").Text = pag.Textos(84)
            mnuCalidad.FindItem("Encuestas/EncuestasSeguimiento").Text = pag.Textos(85)

            fsnTabCalSolicitudesQA.Text = pag.Textos(82)
            If oUser.EsPeticionarioSolicitudesQA Then mnuSolicitudesQA.FindItem("SolicitudesQAAlta").Text = pag.Textos(84)
            If oUser.EsParticipanteSolicitudesQA Then mnuSolicitudesQA.FindItem("SolicitudesQASeguimiento").Text = pag.Textos(85)
            fsnTabCalEncuestas.Text = pag.Textos(83)
            If oUser.EsPeticionarioEncuestas Then mnuEncuestas.FindItem("EncuestasAlta").Text = pag.Textos(84)
            If oUser.EsParticipanteEncuestas Then mnuEncuestas.FindItem("EncuestasSeguimiento").Text = pag.Textos(85)
            fsnTabCalNotificaciones.Text = pag.Textos(41)
            fsnTabCatEmision.Text = pag.Textos(22)
            mnuCatalogo.Items(0).Text = pag.Textos(22)
            fsnTabCatSeguimiento.Text = pag.Textos(2)
            mnuCatalogo.Items(1).Text = pag.Textos(2)
            fsnTabCatRecepcion.Text = pag.Textos(23)
            mnuCatalogo.Items(2).Text = pag.Textos(23)
            fsnTabCatAprobacion.Text = pag.Textos(3)
            mnuCatalogo.Items(3).Text = pag.Textos(3)
            fsnTabCatParametros.Text = pag.Textos(14)
            mnuCatalogo.Items(4).Text = pag.Textos(14)

            fsnTabColaboracion.Text = pag.Textos(43)

            fsnTabCtrlPres.Text = pag.Textos(24) '"Control Presupuestario"
            mnuCtrlPres.Items(0).Text = pag.Textos(24) '"Control Presupuestario"
            mnuCtrlPres.Items(1).Text = pag.Textos(31)
            fsnTabCtrlPresActivos.Text = pag.Textos(31)
            fsnTabCtrlPresup.Text = pag.Textos(40)

            fsnTabIntegracion.Text = pag.Textos(60)
            fsnTabFacturacion.Text = pag.Textos(61)
            mnuFacturacion.FindItem("Alta").Text = pag.Textos(80)
            fsnTabFactAlta.Text = pag.Textos(80)
            mnuFacturacion.FindItem("Seguimiento").Text = pag.Textos(2)
            fsnTabFactSeguimiento.Text = pag.Textos(2)
            mnuFacturacion.FindItem("Autofacturacion").Text = pag.Textos(81)
            fsnTabFactAutofacturacion.Text = pag.Textos(81)

            fsnTabSeguridad.Text = pag.Textos(63)
            mnuSeguridad.FindItem("PerfilesSeguridad").Text = pag.Textos(64)

            'El tab de integracion muestra un icono y un tooltip en caso de parada del servicio de integracion
            Dim UrlImagen As String
            Dim dtFechaParada As DateTime = Nothing
            Dim oInt As FSNServer.VisorInt = FSNServer.Get_Object(GetType(FSNServer.VisorInt))
            If oInt.HayParadaServicioInt(dtFechaParada) Then
                UrlImagen = ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Page.Theme & "/images/alert-rojo-mini.png"
                fsnTabIntegracion.Text = "<img src='" & UrlImagen & "'  style='text-align:center;' border=0/> " & fsnTabIntegracion.Text
                pag.ModuloIdioma = ModulosIdiomas.VisorIntegracion
                fsnTabIntegracion.ToolTip = pag.Textos(73) & vbCrLf & pag.Textos(74)
                If Not IsNothing(dtFechaParada) Then
                    fsnTabIntegracion.ToolTip = fsnTabIntegracion.ToolTip & Format(dtFechaParada, oUser.DateFormat.ShortDatePattern) & " " & Format(dtFechaParada, oUser.DateFormat.ShortTimePattern)

                End If
            End If
            pag.ModuloIdioma = modulo
        End If
    End Sub
    ''' Revisado por: Jbg. Fecha: 28/11/2011
    ''' <summary>
    ''' Seleccionar opciones menú
    ''' </summary>
    ''' <param name="Pestania">Pestaña a seleccionar</param>
    ''' <param name="Opcion">Menu a seleccionar</param>
    ''' <remarks>Llamada desde: Todas las pantallas q usen el menu.master ; Tiempo máximo: 0</remarks>
    Public Sub Seleccionar(ByVal Pestania As String, ByVal Opcion As String)
        Dim pag As FSNServer.IFSNPage = Me.Page
        Dim oUser As FSNServer.User = pag.Usuario

        fsnTabProcesos.Selected = False
        fsnTabContratos.Selected = False
        fsnTabCalidad.Selected = False
        fsnTabCalidadConfiguracion.Selected = False
        fsnTabCalSolicitudesQA.Selected = False
        fsnTabCalEncuestas.Selected = False
        fsnTabCatalogo.Selected = False
        fsnTabCtrlPresup.Selected = False
        fsnTabInformes.Selected = False
        fsnTabIntegracion.Selected = False
        fsnTabFacturacion.Selected = False
        fsnTabColaboracion.Selected = False
        fsnTabSeguridad.Selected = False

        Select Case Pestania
            Case "Procesos"
                fsnTabProcesos.Selected = True
                mnuProcesos.Visible = True
                mnuProcesos.FindItem(Opcion).Selected = True
                Select Case Opcion
                    Case "AltaSolicitudes"
                        fsnTabProcAlta.Selected = True
                    Case "Seguimiento"
                        fsnTabProcSeguimiento.Selected = True
                    Case "Parametros"
                        fsnTabProcParametros.Selected = True
                End Select
            Case "Contratos"
                fsnTabContratos.Selected = True
                mnuContratos.Visible = True
                mnuContratos.FindItem(Opcion).Selected = True
                Select Case Opcion
                    Case "AltaContratos"
                        fsnTabContrAlta.Selected = True
                    Case "Seguimiento"
                        fsnTabContrSeguimiento.Selected = True
                End Select
            Case "Calidad"
                fsnTabCalidad.Selected = True
                mnuCalidad.Visible = True
                Select Case Opcion
                    Case "PanelProveedores"
                        fsnTabCalPanelProveedores.Selected = True
                    Case "Certificados"
                        fsnTabCalCertificados.Selected = True
                    Case "NoConformidades"
                        fsnTabCalNoConformidades.Selected = True
                    Case "SolicitudesQA"
                        fsnTabCalSolicitudesQA.Selected = True
                    Case "Encuestas"
                        fsnTabCalEncuestas.Selected = True
                    Case "Notificaciones"
                        fsnTabCalNotificaciones.Selected = True
                    Case "PanelEscalacionProveedores"
                        fsnTabCalPanelEscalacionProveedores.Selected = True
                End Select
            Case "Configuracion"
                fsnTabCalidadConfiguracion.Selected = True
                fsnTabCalidad.Selected = True
                mnuCalidad.Visible = True
                mnuCalidad.FindItem("Configuracion").Selected = True
                mnuCalidadConfiguracion.FindItem(Opcion).Selected = True
            Case "Catalogo"
                fsnTabCatalogo.Selected = True
                mnuCatalogo.Visible = True
                mnuCatalogo.FindItem(Opcion).Selected = True
                Select Case Opcion
                    Case "Emision"
                        fsnTabCatEmision.Selected = True
                    Case "Seguimiento"
                        fsnTabCatSeguimiento.Selected = True
                    Case "Recepcion"
                        fsnTabCatRecepcion.Selected = True
                    Case "Aprobacion"
                        fsnTabCatAprobacion.Selected = True
                    Case "Parametros"
                        fsnTabCatParametros.Selected = True
                End Select
            Case "CtrlPresup"
                fsnTabCtrlPresup.Selected = True
                mnuCtrlPres.Visible = True
                mnuCtrlPres.FindItem(Opcion).Selected = True
                Select Case Opcion
                    Case "ControlPresupuestario"
                        fsnTabCtrlPres.Selected = True
                    Case "Activos"
                        fsnTabCtrlPresActivos.Selected = True
                End Select
            Case "Informes"
                fsnTabInformes.Selected = True
                ''Hay por esta master posibilidad de que se eliminen opciones del mnuInformes
                If Not mnuInformes.FindItem(Opcion) Is Nothing Then mnuInformes.FindItem(Opcion).Selected = True
            Case "Colaboracion"
                fsnTabColaboracion.Selected = True
            Case "Integracion"
                fsnTabIntegracion.Selected = True
            Case "Facturacion"
                fsnTabFacturacion.Selected = True
                mnuFacturacion.Visible = True
                mnuFacturacion.FindItem(Opcion).Selected = True
                Select Case Opcion
                    Case "Alta"
                        fsnTabFactAlta.Selected = True
                    Case "Seguimiento"
                        fsnTabFactSeguimiento.Selected = True
                    Case "Autofacturacion"
                        fsnTabFactAutofacturacion.Selected = True
                End Select
            Case "Seguridad"
                fsnTabSeguridad.Selected = True
                mnuSeguridad.Visible = True
                mnuSeguridad.FindItem(Opcion).Selected = True
            Case "Notificaciones"
                fsnTabCalNotificaciones.Selected = True
        End Select
    End Sub
End Class