﻿Imports System.IO
Imports System.Security.Claims

Partial Public Class Login
	Inherits System.Web.UI.MasterPage

	Private _FSNServerLogin As FSNServer.Root
	Private BaseDatosSecundarias As List(Of FSNServer.BaseDatos)
	Private BDLogin As FSNServer.BaseDatos
	Private BDSecundaria As FSNServer.BaseDatos

	Public ReadOnly Property FSNServerLogin() As FSNServer.Root
		Get
			If _FSNServerLogin Is Nothing Then
				_FSNServerLogin = Session("FSN_ServerLogin")
				If _FSNServerLogin Is Nothing Then
					_FSNServerLogin = New FSNServer.Root
					Session("FSN_ServerLogin") = _FSNServerLogin
				End If
			End If
			Return _FSNServerLogin
		End Get
	End Property
	''' <summary>
	''' Oculta el panel del menú
	''' </summary>
	''' <param name="sender">Origen del evento.</param>
	''' <param name="e">Clase EventArgs que no contiene datos de evento.</param>
	''' <remarks>Se produce cuando el control de servidor se carga en el objeto Page.</remarks>
	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If FSNServerLogin.TipoDeAutenticacion = TiposDeAutenticacion.ServidorExterno AndAlso Not FSNServerLogin.TipoDeAccesoLCX Then
			Master.FindControl("panMenuCabecera").Visible = False
			TablaAutenticacion.Visible = False
			lblAutenticacionIntegrada.Visible = True
			CType(Page, FSNServer.IFSNPage).ModuloIdioma = TiposDeDatos.ModulosIdiomas.Login
			CargarTextos()
		Else
			If Not IsPostBack() Then
                'Comprobacion de si hay autenticacion windows para ir a la pagina de inicio
                If {TiposDeAutenticacion.Windows, TiposDeAutenticacion.ADFS}.Contains(FSNServerLogin.TipoDeAutenticacion) Then
                    Dim sDominio As String
                    Dim sUsuario As String
                    Dim UserIdentityInfo As System.Security.Principal.WindowsIdentity
                    Dim oUser As FSNServer.User

                    If FSNServerLogin.TipoDeAutenticacion = TiposDeAutenticacion.Windows Then
                        UserIdentityInfo = System.Security.Principal.WindowsIdentity.GetCurrent()
                        sDominio = UserIdentityInfo.Name.Split("\")(0)
                        sUsuario = UserIdentityInfo.Name.Split("\")(1)
                    Else
                        sUsuario = ""
                        If Page.Request.QueryString("sSession") Is Nothing Then
                            Dim samlEndpoint As String = ConfigurationManager.AppSettings("rutaADFS")
                            Dim Request = New FSNServer.Saml.AuthRequest(
                                    ConfigurationManager.AppSettings("appUniqueID"), ' put your app's "unique ID" here
                                    ConfigurationManager.AppSettings("redirectAuthenticatedURL") 'assertion Consumer Url - the URL where provider will redirect authenticated users BACK
                                )
                            'then redirect your user to the above "url" var for example, Like this
                            Dim url As String = Request.GetRedirectUrl(samlEndpoint)
                            Response.Redirect(url)
                        Else
                            Dim webService As New FSNSessionService.SessionService
                            sUsuario = webService.GetSessionData(Page.Request.QueryString("sSession"), "Usuario")
                        End If
                    End If

                    Session.Clear()

                    oUser = FSNServerLogin.Login(sUsuario)

                    oUser.AccesoCN = FSNServerLogin.TipoAcceso.gbAccesoFSCN
                    oUser.LoadUserData(sUsuario)

                    If oUser.AccesoQA Then
                        If Application("Nivel_UnQa") Is Nothing Then
                            FSNServerLogin.Load_UnQa()
                            Application("Nivel_UnQa") = FSNServerLogin.NivelesUnQa
                        End If
                    End If

                    Gestion_Login(oUser, sUsuario, "")

                ElseIf FSNServerLogin.TipoDeAccesoLCX Then 'Comprobacion de si tiene acceso especial Pargen_interno.acceso_lcx
                    Dim sUsuario As String
					Dim UserName As String
					Dim oUser As FSNServer.User
                    Dim Perfiles() As String = {"administrador", "gestor", "consulta", "auditor", "compras"}
                    Dim sPerfil As String
					Dim sCentro As String
					Dim sEmpresa As String

                    sUsuario = Request("txtCodUsu")
                    sPerfil = Trim(LCase(Request("txtPerfil")))
                    sCentro = Request("txtCodCentro")
                    sEmpresa = Request("txtCodEmpresa")

                    oUser = Nothing

					'Si el cod de usuario no es vacio y el perfil no es vacio y esta dentro de los posibles perfiles
					If Not String.IsNullOrEmpty(sUsuario) And Not String.IsNullOrEmpty(sPerfil) And Perfiles.Contains(sPerfil) Then
						'Segun el tipo de perfil se va mirando si empresa y centro son vacios y si existen en fullstep 
						Select Case sPerfil
							Case "administrador"
								If Not String.IsNullOrEmpty(sEmpresa) And FSNServerLogin.ComprobarEmpresaLCX(sEmpresa) Then
									'Autenticacion sin pwd
									oUser = FSNServerLogin.Login(sUsuario, True)
								End If
							Case "gestor", "consulta"
								If Not String.IsNullOrEmpty(sCentro) And Not String.IsNullOrEmpty(sEmpresa) And FSNServerLogin.ComprobarEmpresaLCX(sEmpresa) And FSNServerLogin.ComprobarCentroCosteLCX(sCentro) Then
									'Autenticacion sin pwd
									oUser = FSNServerLogin.Login(sUsuario, True)
								End If
                            Case "auditor", "compras"
                                'Autenticacion sin pwd
                                oUser = FSNServerLogin.Login(sUsuario, True)
						End Select
					End If

					If oUser.ResultadoLogin = TipoResultadoLogin.LoginOk Then
						oUser.AccesoCN = FSNServerLogin.TipoAcceso.gbAccesoFSCN
						oUser.LoadUserData(sUsuario)
						UserName = oUser.Cod
						If oUser.AccesoQA Then
							If Application("Nivel_UnQa") Is Nothing Then
								FSNServerLogin.Load_UnQa()
								Application("Nivel_UnQa") = FSNServerLogin.NivelesUnQa
							End If
						End If

						Gestion_Login(oUser, sUsuario, "")


					Else
						'Devolver un NOK a la Caixa
						Response.Redirect(ConfigurationManager.AppSettings("URL_LCX") & "?NOK=1&usuario=" & sUsuario & "&empresa=" & sEmpresa & "&centro=" & sCentro & "&perfil=" & sPerfil, False)
					End If
				End If

				Master.FindControl("panMenuCabecera").Visible = False
				Dim pag As FSNServer.IFSNPage = CType(Me.Page, FSNServer.IFSNPage)
				Dim modulo As TiposDeDatos.ModulosIdiomas = pag.ModuloIdioma
				pag.ModuloIdioma = TiposDeDatos.ModulosIdiomas.Login
				CargarTextos()
				pag.ModuloIdioma = modulo
				CType(Master.FindControl("scriptManager1"), ScriptManager).RegisterPostBackControl(btnAceptarPWD)
				CType(Master.FindControl("scriptManager1"), ScriptManager).RegisterPostBackControl(btnCancelarPWD)
				CType(Master.FindControl("scriptManager1"), ScriptManager).RegisterPostBackControl(imgBtnCerrarPnlSeguridad)
				CType(Master.FindControl("scriptManager1"), ScriptManager).RegisterPostBackControl(BtnAceptarContrOK)
			End If
			CType(Master.FindControl("scriptManager1"), ScriptManager).CompositeScript.Scripts.Add(New ScriptReference("../js/jquery/jquery.min.js"))
			CType(Master.FindControl("scriptManager1"), ScriptManager).CompositeScript.Scripts.Add(New ScriptReference("../js/politicacookies.js"))

			BaseDatosSecundarias = FSNServerLogin.obtenerBasesDatosSecundarias
			'Fin de comprobacion

			'Rellenar combo de bases de datos secundarias
			If Me.BaseDatosSecundarias.Count > 1 Then
				Dim principal As Integer
				For Each bd As FSNServer.BaseDatos In Me.BaseDatosSecundarias
					If bd.isActiva Then
						Dim oItem As ListItem
						If bd.isPrincipal Then principal = bd.Index
						'si la base de datos de login no es 
						If bd.isLogin And UCase(bd.nombre) <> UCase(FSNServerLogin.DBName) Then
							BDLogin = bd
						End If
						'si la base de datos de login no es la principal, no se muestra
						If Not (Not bd.isPrincipal And bd.isLogin) Then
							oItem = New ListItem(bd.Denominacion, bd.Index)
							ddlBasesDatosSecundarias.Items.Add(oItem)
						End If
					End If
				Next
				If Not IsPostBack Then
					ddlBasesDatosSecundarias.SelectedValue = principal
				End If
			Else
				lblBaseDatos.Visible = False
				ddlBasesDatosSecundarias.Visible = False
			End If
		End If
	End Sub
	''' <summary>
	''' Realiza la validación del usuario.
	''' </summary>
	''' <param name="sender">Origen del evento.</param>
	''' <param name="e">Clase EventArgs que no contiene datos de evento.</param>
	''' <remarks>Se produce cuando se hace clic en el control FSNButton btnLogin.</remarks>
	Protected Sub btnLogin_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnLogin.Click
		Dim UserName As String = txtNombreUsu.Text
		Dim Password As String = txtPassword.Text
		Dim oUser As FSNServer.User

		Session.Clear()
		If FSNServerLogin.TipoDeAutenticacion = TiposDeAutenticacion.LDAP Then
			Dim webServiceLDAP As New FSNWebServicePWD.CambioPWD
			If webServiceLDAP.LoginLDAP(UserName, Password) = 1 Then
				oUser = FSNServerLogin.Login(UserName, True)
			Else
				oUser = New FSNServer.User(TipoResultadoLogin.UsuIncorrecto)
			End If
		Else
			If BaseDatosSecundarias.Count > 0 Then
				Me.BDSecundaria = BaseDatosSecundarias(ddlBasesDatosSecundarias.SelectedValue)
				FSNServerLogin.BDSecundaria = Me.BDSecundaria
				If Not BDLogin Is Nothing Then
					FSNServerLogin.BDLogin = BDLogin
				End If
			End If
			oUser = FSNServerLogin.Login(UserName, Password, False)
		End If

		If oUser.ResultadoLogin <> TipoResultadoLogin.LoginOk OrElse Not ValidarAprobacionMail(UserName) Then
			If oUser.ResultadoLogin = TipoResultadoLogin.UsuBloqueado Then
				lblError.Visible = False
				'Bloqueo de cuenta: nº de intentos=máx. permitido
				txtCuentaBloqueada.Visible = True
				txtContactoAdmin.Visible = True
			Else
				lblError.Visible = True
				If FSNServerLogin.TipoDeAutenticacion = TiposDeAutenticacion.Windows Then
					TablaAutenticacion.Visible = False
				End If

				txtInfoIntentos.Visible = False
				txtCuentaBloqueada.Visible = False
				txtContactoAdmin.Visible = False
				If oUser.ResultadoLogin = TipoResultadoLogin.PwdIncorrecto Then
					Dim oDatosBloq As Object = FSNServerLogin.LoginGestionBloqueoCuenta(UserName)
					If oDatosBloq.LogPreBloq > 0 Then
						If oDatosBloq.LogPreBloq = oDatosBloq.Bloq Then
							lblError.Visible = False
							'Bloqueo de cuenta: nº de intentos=máx. permitido
							txtCuentaBloqueada.Visible = True
							txtContactoAdmin.Visible = True
						Else
							'Aviso de nº de intentos restantes
							txtInfoIntentos.Visible = True
							CType(Me.Page, FSNServer.IFSNPage).ModuloIdioma = TiposDeDatos.ModulosIdiomas.Login
							txtInfoIntentos.InnerText = CType(Me.Page, FSNServer.IFSNPage).Textos(16)
							txtInfoIntentos.InnerText = txtInfoIntentos.InnerText.Replace("@NUM_INT", (oDatosBloq.LogPreBloq - oDatosBloq.Bloq).ToString)
						End If
					End If
				End If
			End If
		Else
			'FSWSServer.GetAccessType()
			oUser.AccesoCN = FSNServerLogin.TipoAcceso.gbAccesoFSCN
			oUser.LoadUserData(UserName)
			If oUser.AccesoQA Then
				If Application("Nivel_UnQa") Is Nothing Then
					FSNServerLogin.Load_UnQa()
					Application("Nivel_UnQa") = FSNServerLogin.NivelesUnQa
				End If
			End If
		End If

		Gestion_Login(oUser, UserName, Password)
	End Sub
	''' <summary>
	''' Valida que al aprobar un pedido o solicitud desde email se loguee con el codigo de usuario del aprobador que se envio en el mail
	''' </summary>
	''' <param name="sUsuario">Usuario que se ha logueado</param>
	''' <returns>Retorna true si se ha validado correctamente</returns>
	''' <remarks></remarks>
	Private Function ValidarAprobacionMail(ByVal sUsuario As String) As Boolean
		If (Request.QueryString("target") = "aprobEmail" Or Request.QueryString("target") = "rechazEmail") And Request.QueryString("CodAprob") <> sUsuario Then
			Return False
		ElseIf Request.QueryString("TipoAcceso") = "4" And Request.QueryString("Usuario") <> sUsuario Then
			Return False
		Else
			Return True
		End If
	End Function
	''' <summary>
	''' Carga los textos en función del idioma de la página.
	''' </summary>
	''' <remarks>
	''' La página debe implementar la interface IFSNPage.
	''' Llamadas desde: Page_Load
	''' </remarks>
	Private Sub CargarTextos()
		Dim pag As FSNServer.IFSNPage = CType(Me.Page, FSNServer.IFSNPage)
		lblError.Text = pag.Textos(0) & " " & pag.Textos(1) & " " & pag.Textos(2)
		lblInfoAcceso.Text = pag.Textos(3)
		lblNombreUsu.Text = pag.Textos(4)
		lblPassword.Text = pag.Textos(5)
		btnLogin.Text = pag.Textos(6)
		lblResolucion.Text = pag.Textos(13)
		lblNavegadores.Text = pag.Textos(14)
		lblBaseDatos.Text = pag.Textos(15)
		txtCuentaBloqueada.InnerText = pag.Textos(17)
		txtContactoAdmin.InnerText = pag.Textos(18)
		lblAutenticacionIntegrada.Text = pag.Textos(19)
		'Panel de seguridad
		pag.ModuloIdioma = TiposDeDatos.ModulosIdiomas.Contraseñas
		lblPWDActual.Text = pag.Textos(1)
		lblPWDNueva.Text = pag.Textos(2)
		lblPWDNuevaRep.Text = pag.Textos(3)
		lblTituloCambioPWD.Text = pag.Textos(18)
		lblContrExp.Text = pag.Textos(19)
		btnAceptarPWD.Text = pag.Textos(4)
		btnCancelarPWD.Text = pag.Textos(5)
		BtnAceptarContrOK.Text = pag.Textos(4)
		LblMsgContr.Text = pag.Textos(6)

		'politica de cookies
		pag.ModuloIdioma = TiposDeDatos.ModulosIdiomas.Menu

		aPoliticacookies.InnerHtml = pag.Textos(69)
	End Sub
	''' <summary>
	''' Evento que se genera al pulsar el botÃ³n de cancelar la contraseÃ±a, cerrando el panel de la contraseÃ±a
	''' </summary>
	''' <param name="sender">El objeto que lanza el evento(btnCancelarPWD)</param>
	''' <param name="e">Los argumentos del evento</param>
	''' <remarks>
	''' Llamada desde: AutomÃ¡tica, cuando se pincha el botÃ³n
	''' Tiempo mÃ¡ximo: 0 seg</remarks>
	Protected Sub btnCancelarPWD_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelarPWD.Click
		RequiredFieldValidator1.Enabled = True
		RequiredFieldValidator2.Enabled = True
		pnlSeguridad_ModalPopupExtender.Hide()
	End Sub
	''' <summary>
	''' Evento que se genera al pulsar el botÃ³n de aceptar la contraseÃ±a, comprobando los datos y llamando al webservice
	''' </summary>
	''' <param name="sender">El objeto que lanza el evento(btnAceptarPWD)</param>
	''' <param name="e">Los argumentos del evento</param>
	''' <remarks>
	''' Llamada desde: AutomÃ¡tica, cuando se pincha el botÃ³n
	''' Tiempo mÃ¡ximo: 0,2 seg</remarks>
	Protected Sub btnAceptarPWD_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAceptarPWD.Click
		Dim UserName As String = txtNombreUsu.Text
		Dim Password As String = txtPWDActual.Text
		Dim sSession As String = Nothing
		Dim FSWSServer As New FSNServer.Root
		Dim oUser As FSNServer.User
		Dim wsCambioPWD As New FSNWebServicePWD.CambioPWD
		Dim errorPWD As Integer = 0
		Dim pag As FSNServer.IFSNPage = CType(Me.Page, FSNServer.IFSNPage)
		Session.Clear()
		oUser = FSWSServer.Login(UserName, Password, False)
		Dim modulo As TiposDeDatos.ModulosIdiomas = pag.ModuloIdioma
		pag.ModuloIdioma = TiposDeDatos.ModulosIdiomas.Contraseñas
		If oUser.ResultadoLogin <> TipoResultadoLogin.LoginOk Then
			LblMsgContr.Text = pag.Textos(16)
			txtPWDActual.Text = ""
			ImgInfoContrOK.Visible = False
			ImgInfoContrMal.Visible = True
			UpDatosPanelCambioContr.Update()
			mpeMsgCambioContr.Show()
			pag.ModuloIdioma = modulo
			Exit Sub
		Else
			'FSWSServer.GetAccessType()
			oUser.AccesoCN = FSWSServer.TipoAcceso.gbAccesoFSCN
			oUser.LoadUserData(UserName)
			If txtPWDNueva.Text <> txtPWDNuevaRep.Text Then
				LblMsgContr.Text = pag.Textos(15)
				ImgInfoContrOK.Visible = False
				ImgInfoContrMal.Visible = True
				txtPWDNuevaRep.Text = ""
				UpDatosPanelCambioContr.Update()
				mpeMsgCambioContr.Show()
				pag.ModuloIdioma = modulo
				Exit Sub
			End If
			Try
				errorPWD = wsCambioPWD.CambiarLogin(0, oUser.Cod, txtPWDActual.Text, oUser.Cod, txtPWDNueva.Text,
													Date.Now.Year, Date.Now.Month, Date.Now.Day, Date.Now.Hour, Date.Now.Minute, Date.Now.Second,
													0, "",
													oUser.FechaPassword.Year, oUser.FechaPassword.Month, oUser.FechaPassword.Day, oUser.FechaPassword.Hour, oUser.FechaPassword.Minute, oUser.FechaPassword.Second)
			Catch ex As Exception
				pag.ModuloIdioma = modulo
				Exit Sub
			End Try
			If errorPWD = 0 Then
				Password = txtPWDNueva.Text
				oUser.Password = Password
				oUser.LoadUserData(UserName)
				If oUser.AccesoPM OrElse oUser.AccesoQA OrElse oUser.AccesoEP OrElse oUser.AccesoFACT Then
					FSWSServer.Load_LongitudesDeCodigos()
					Session("FSN_User") = oUser
					Session("FSN_Server") = FSWSServer
					HttpContext.Current.User = Session("FSN_User")

					If FSWSServer.TipoDeAutenticacion <> TiposDeAutenticacion.Windows Then
						' Crear cookie de autenticaciÃ³n de usuario
						Dim ticket As FormsAuthenticationTicket = New FormsAuthenticationTicket(1,
							UserName,
							DateTime.Now,
							DateTime.Now.AddMinutes(Session.Timeout * 2),
							False,
							UserName,
							FormsAuthentication.FormsCookiePath)
						' Encrypt the ticket.
						Dim encTicket As String = FormsAuthentication.Encrypt(ticket)
						' Create the cookie.
						Response.Cookies.Add(New HttpCookie(FormsAuthentication.FormsCookieName, encTicket))
						'Marcarla como segura si estas con https
						If InStr(ConfigurationManager.AppSettings("rutaFS"), "https: ") > 0 Then
							Dim cookie As HttpCookie
							cookie = Request.Cookies(FormsAuthentication.FormsCookieName)
							cookie.Secure = True
							Response.SetCookie(cookie)
						End If
					End If

					If oUser.AccesoEP Then
						Dim target As String
						Dim RutaCompleta As String
						Dim OrdenId As Integer
						If Not Request.QueryString("Orden") Is Nothing Then
							OrdenId = Request.QueryString("Orden")
						End If
						target = Request.QueryString("target")
						If Not target Is Nothing Then
							Select Case target
								Case "aprob"
									target = "Aprobacion.aspx"
									RutaCompleta = ConfigurationManager.AppSettings("RutaEp") & target & "?Orden=" & OrdenId
								Case "reemitir"
									target = "Seguimiento.aspx"
									RutaCompleta = ConfigurationManager.AppSettings("RutaEp") & target & "?Orden=" & OrdenId
								Case "aprobEmail"
									RutaCompleta = ConfigurationManager.AppSettings("RutaEp") & "ws/EP_Pedidos.asmx/AprobarPedidoEP?Idioma=" & Request.QueryString("Idioma") & "&Orden=" & Request.QueryString("Orden") & "&CodAprob=" & Request.QueryString("CodAprob") & "&Target=aprob" & "&CodRnd=" & Request.QueryString("CodRnd") & "&CodSeguridad=" & Request.QueryString("CodSeguridad")
								Case "rechazEmail"
									RutaCompleta = ConfigurationManager.AppSettings("RutaEp") & "ws/EP_Pedidos.asmx/DenegarPedidoEP?Idioma=" & Request.QueryString("Idioma") & "&Orden=" & Request.QueryString("Orden") & "&CodAprob=" & Request.QueryString("CodAprob") & "&Target=aprob" & "&CodRnd=" & Request.QueryString("CodRnd") & "&CodSeguridad=" & Request.QueryString("CodSeguridad")
								Case Else
									target = "Inicio.aspx"
									RutaCompleta = ConfigurationManager.AppSettings("RutaEp") & target
							End Select
							Response.Redirect(RutaCompleta)
						End If
					End If
					If Request("Contrato") <> Nothing Then
						Dim UrlDestino As String
						UrlDestino = ConfigurationManager.AppSettings("rutaPM") & "Frames.aspx?pagina="
						UrlDestino = UrlDestino & Server.UrlEncode("Contratos/ComprobarAprobContratos.aspx?Contrato=" & Request("Contrato") & "&Instancia=" & Request("Instancia"))
						Response.Redirect(UrlDestino, True)
					End If
					If Request("Factura") <> Nothing Then
						Response.Redirect("~/App_Pages/PM/Facturas/DetalleFactura.aspx?ID=" & Request("Factura"))
					End If
					If Request("TipoAcceso") <> Nothing AndAlso Request("Instancia") <> Nothing Then
						Dim UrlDestino As String
						UrlDestino = ConfigurationManager.AppSettings("rutaPM") & "Frames.aspx?pagina="
						If Request("TipoAcceso") = "1" Then
							UrlDestino = UrlDestino & Server.UrlEncode("workflow/comprobaraprob.aspx?Instancia=" & Request("Instancia") & "&TipoAcceso=" & Request("TipoAcceso"))
						ElseIf Request("TipoAcceso") = "2" Then
							UrlDestino = UrlDestino & Server.UrlEncode("seguimiento/NWDetallesolicitud.aspx?Instancia=" & Request("Instancia"))
						ElseIf Request("TipoAcceso") = "3" Then
							UrlDestino = UrlDestino & Server.UrlEncode("workflow/gestiontrasladada.aspx?Instancia=" & Request("Instancia"))
						ElseIf Request("TipoAcceso") = "4" Then
							UrlDestino = ConfigurationManager.AppSettings("rutaPM2008") & "AprobacionEmail.asmx/ProcesarAccionesPMEMail?Instancia=" & Request.QueryString("Instancia") & "&Bloque=" & Request.QueryString("Bloque") & "&Random=" & Request.QueryString("Random") & "&Usuario=" & Request.QueryString("Usuario") & "&TipoSolicitud=" & Request.QueryString("TipoSolicitud")
						End If
						Response.Redirect(UrlDestino)
					ElseIf Request("TipoAcceso") <> Nothing AndAlso Request("TipoAcceso") = 5 Then
						Dim sURLDestino As String = ConfigurationManager.AppSettings("rutaFS") & "QA/EscalacionProveedores/PanelEscalacionProves.aspx?UNQA=" & Request("UNQA") & "&nivelEscalacion=" & Request("nivelEscalacion")
						Response.Redirect(sURLDestino, True)
					Else
						Response.Redirect("~/App_Pages/Inicio.aspx")
					End If

				Else
					pnlSeguridad_ModalPopupExtender.Hide()
					Session("FSN_User") = Nothing
					lblError.Visible = True
				End If
			Else
				Select Case errorPWD
					Case 1
						LblMsgContr.Text = pag.Textos(7) & " " & If(FSWSServer.TipoAcceso.giMIN_SIZE_PWD > 8, FSWSServer.TipoAcceso.giMIN_SIZE_PWD, 8) & " " & pag.Textos(8)
						txtPWDNueva.Text = ""
						txtPWDNuevaRep.Text = ""
					Case 2
						LblMsgContr.Text = pag.Textos(9) & "<br>" & pag.Textos(20) & "<p>" & pag.Textos(7) & " " & If(FSWSServer.TipoAcceso.giMIN_SIZE_PWD > 8, FSWSServer.TipoAcceso.giMIN_SIZE_PWD, 8) & " " & pag.Textos(8) & "</p><p>" & pag.Textos(21) & "</p><p>" & pag.Textos(22) & "</p>"
						txtPWDNueva.Text = ""
						txtPWDNuevaRep.Text = ""
					Case 3
						LblMsgContr.Text = pag.Textos(10)
						txtPWDNueva.Text = ""
						txtPWDNuevaRep.Text = ""
					Case 4
						LblMsgContr.Text = pag.Textos(11) & " " & FSWSServer.TipoAcceso.giHISTORICO_PWD & " " & pag.Textos(12)
						txtPWDNueva.Text = ""
						txtPWDNuevaRep.Text = ""
					Case 5
						LblMsgContr.Text = pag.Textos(13)
						txtPWDNueva.Text = ""
						txtPWDNuevaRep.Text = ""
					Case Else
						LblMsgContr.Text = pag.Textos(14)
						txtPWDNueva.Text = ""
						txtPWDNuevaRep.Text = ""
						txtPWDActual.Text = ""
				End Select
				ImgInfoContrOK.Visible = False
				ImgInfoContrMal.Visible = True
				UpDatosPanelCambioContr.Update()
				mpeMsgCambioContr.Show()
			End If
		End If
		pag.ModuloIdioma = modulo
	End Sub
	Protected Sub BtnAceptarContrOK_Click(ByVal sender As Object, ByVal e As EventArgs) Handles BtnAceptarContrOK.Click
		mpeMsgCambioContr.Hide()
		pnlSeguridad_ModalPopupExtender.Show()
	End Sub
	''' <summary>
	''' Controla si te puedes logear.
	''' Te redirige si no es un login sino un login para ir al detalle de contrato/factura/pedido
	''' </summary>
	''' <param name="oUser">Objeto Usuario</param>
	''' <param name="UserName">Codigo usuario</param>
	''' <param name="Password">Password</param>
	''' <remarks>Llamada desde: page_load btnlogin_click; Tiempo máximo:0</remarks>
	Private Sub Gestion_Login(ByVal oUser As FSNServer.User, ByVal UserName As String, ByVal Password As String)
		Dim sSession As String = Nothing
		Dim codigoCentro As String = Nothing

		If oUser.ResultadoLogin <> TipoResultadoLogin.LoginOk Then
			Session("FSN_User") = Nothing
			If FSNServerLogin.TipoDeAutenticacion = TiposDeAutenticacion.Windows Or FSNServerLogin.TipoDeAccesoLCX = True Then
				TablaAutenticacion.Visible = False
			End If
		Else
            If FSNServerLogin.TipoDeAccesoLCX Then
                oUser.PerfilLCX = Trim(LCase(Request("txtPerfil"))) '"administrador", "gestor", "consulta", "auditor", "compras" 
                Select Case oUser.PerfilLCX
                    Case "administrador" 'el administrador tiene acceso a todos los centros. Aunque venga un centro, le pasamos la cadena vacía.
                        codigoCentro = FSNServerLogin.LoginLCX(UserName, Request("txtCodEmpresa"), String.Empty, oUser.PerfilLCX, oUser)
                    Case "gestor", "consulta"
                        codigoCentro = FSNServerLogin.LoginLCX(UserName, Request("txtCodEmpresa"), Request("txtCodCentro"), oUser.PerfilLCX, oUser)
                    Case "compras", "auditor" 'el perfil compras tiene acceso a todos las empresas y todos los centros. 
                        codigoCentro = FSNServerLogin.LoginLCX(UserName, String.Empty, String.Empty, oUser.PerfilLCX, oUser)
                End Select
            End If

            If Not FSNServerLogin.TipoDeAutenticacion = TiposDeAutenticacion.Windows AndAlso Not FSNServerLogin.TipoDeAutenticacion = TiposDeAutenticacion.LDAP Then
				If FSNServerLogin.TipoAcceso.giEDAD_MAX_PWD > 0 AndAlso Date.Now > oUser.FechaPassword.AddDays(FSNServerLogin.TipoAcceso.giEDAD_MAX_PWD) Then
					'La contraseÃ±a ha expirado
					pnlSeguridad_ModalPopupExtender.Show()
					RequiredFieldValidator1.Enabled = False
					RequiredFieldValidator2.Enabled = False
					Exit Sub
				End If
			End If

			If oUser.AccesoPM OrElse oUser.AccesoFACT Then
				Dim FSSMPargen As FSNServer.PargensSMs = FSNServerLogin.Get_Object(GetType(FSNServer.PargensSMs))
				FSSMPargen.CargarConfiguracionSM("", oUser.IdiomaCod, True) 'no hace falta pasar el usuario. En PMWeb solo se usa la configuraciÃ³n de las partidas independiente del usuario.
				Session("FSSMPargen") = FSSMPargen
			End If

			'Si tiene acceso a algún producto creamos la variable de  y 
			'obtenemos las longitudes de campo, sino mostramos mensaje de error
			If Not oUser.AccesoPM AndAlso Not oUser.AccesoContratos AndAlso Not oUser.AccesoQA AndAlso Not oUser.AccesoEP AndAlso Not oUser.AccesoSM AndAlso Not oUser.AccesoFACT Then
				Session("FSN_User") = Nothing
				lblError.Visible = True
				If FSNServerLogin.TipoDeAutenticacion = TiposDeAutenticacion.Windows Then
					TablaAutenticacion.Visible = False
				End If
			Else
				FSNServerLogin.Load_LongitudesDeCodigos()

				Session("FSN_User") = oUser
				Session("FSN_Server") = FSNServerLogin

				HttpContext.Current.User = Session("FSN_User")
				If FSNServerLogin.TipoDeAutenticacion <> TiposDeAutenticacion.Windows Then
					'A falta de más investigación esto es necesario para los WebPart
					' Crear cookie de autenticaciÃƒÂ³n de usuario
					Dim ticket As FormsAuthenticationTicket = New FormsAuthenticationTicket(1,
						UserName,
						DateTime.Now,
						DateTime.Now.AddMinutes(Session.Timeout * 2),
						False,
						UserName,
						FormsAuthentication.FormsCookiePath)
					' Encrypt the ticket.
					Dim encTicket As String = FormsAuthentication.Encrypt(ticket)
					' Create the cookie.
					Response.Cookies.Add(New HttpCookie(FormsAuthentication.FormsCookieName, encTicket))
					'Marcarla como segura si estas con https
					If InStr(ConfigurationManager.AppSettings("rutaFS"), "https:") > 0 Then
						Dim cookie As HttpCookie
						cookie = Request.Cookies(FormsAuthentication.FormsCookieName)
						cookie.Secure = True
						Response.SetCookie(cookie)
					End If
				End If

				If FSNServerLogin.TipoDeAutenticacion = TiposDeAutenticacion.ServidorExterno AndAlso
					FSNServerLogin.TipoDeAccesoLCX AndAlso Request("Contrato") <> Nothing Then

					Dim UrlDestino As String
					UrlDestino = ConfigurationManager.AppSettings("rutaAutenticacionIntegrada") & "/Home/LoginLCX"
					UrlDestino &= "?codigoPerfil=" & oUser.PerfilLCX & "&codigoUsuario = " & oUser.Cod & " &codigoEmpresa = " & oUser.EmpresaLCX & " & codigoCentro = " & oUser.CentroLCX
					UrlDestino &= "&urlDestino=" & ConfigurationManager.AppSettings("rutaPM") & "Frames.aspx?pagina="
					UrlDestino &= Server.UrlEncode("Contratos/ComprobarAprobContratos.aspx?Contrato=" & Request("Contrato") & "&Instancia=" & Request("Instancia") & "&sSession=" & Session("sSession"))
					Response.Redirect(UrlDestino, True)

				ElseIf Request("Contrato") <> Nothing Then
					Dim UrlDestino As String
					UrlDestino = ConfigurationManager.AppSettings("rutaPM") & "Frames.aspx?pagina="
					UrlDestino = UrlDestino & Server.UrlEncode("Contratos/ComprobarAprobContratos.aspx?Contrato=" & Request("Contrato") & "&Instancia=" & Request("Instancia"))
					Response.Redirect(UrlDestino, True)
				End If


				If Request("noconformidad") <> Nothing Then
					Dim UrlDestino As String
					UrlDestino = ConfigurationManager.AppSettings("rutaPM") & "Frames.aspx?pagina="
					UrlDestino = UrlDestino & Server.UrlEncode("noconformidad/detalleNoConformidad.aspx?noconformidad=" & Request("NoConformidad") & "&Instancia=" & Request("Instancia"))
					Response.Redirect(UrlDestino, True)
				End If

				If Request("TipoAcceso") <> Nothing AndAlso Request("Instancia") <> Nothing Then
					Dim UrlDestino As String
					UrlDestino = ConfigurationManager.AppSettings("rutaPM") & "Frames.aspx?pagina="
					If Request("TipoAcceso") = "1" Then
						UrlDestino = UrlDestino & Server.UrlEncode("workflow/comprobaraprob.aspx?Instancia=" & Request("Instancia") & "&TipoAcceso=" & Request("TipoAcceso"))
					ElseIf Request("TipoAcceso") = "2" Then
						UrlDestino = UrlDestino & Server.UrlEncode("seguimiento/NWDetallesolicitud.aspx?Instancia=" & Request("Instancia"))
					ElseIf Request("TipoAcceso") = "3" Then
						UrlDestino = UrlDestino & Server.UrlEncode("workflow/gestiontrasladada.aspx?Instancia=" & Request("Instancia"))
					ElseIf Request("TipoAcceso") = "4" Then
						UrlDestino = ConfigurationManager.AppSettings("rutaPM2008") & "AprobacionEmail.asmx/ProcesarAccionesPMEMail?Instancia=" & Request.QueryString("Instancia") & "&Bloque=" & Request.QueryString("Bloque") & "&Random=" & Request.QueryString("Random") & "&Usuario=" & Request.QueryString("Usuario") & "&TipoSolicitud=" & Request.QueryString("TipoSolicitud")
					End If
					Response.Redirect(UrlDestino, True)
				ElseIf Request("TipoAcceso") <> Nothing AndAlso Request("TipoAcceso") = 5 Then
					Dim sURLDestino As String = ConfigurationManager.AppSettings("rutaFS") & "QA/EscalacionProveedores/PanelEscalacionProves.aspx?UNQA=" & Request("UNQA") & "&nivelEscalacion=" & Request("nivelEscalacion")
					Response.Redirect(sURLDestino, True)
				End If

				If oUser.AccesoEP Then
					Dim target As String
					Dim RutaCompleta As String
					Dim OrdenId As Integer
					If Not Request.QueryString("Orden") Is Nothing Then
						OrdenId = Request.QueryString("Orden")
					End If
					target = Request.QueryString("target")
					If Not target Is Nothing Then
						Select Case target
							Case "aprobDetalle"
								target = "DetallePedido.aspx"
								RutaCompleta = ConfigurationManager.AppSettings("RutaEp") & target & "?Desde=1&Idioma=" & Request.QueryString("Idioma") & "&ID=" & Request("Orden") & "&idPedido=" & Request("Pedido") & "&Instancia=" & Request("Instancia") & "&Bloque=" & Request("Bloque") & "&Rol=" & Request("rol") _
										& "&TipoPedido=" & Request("tipoPedido") & "&Anyo=" & Request("Anyo") & "&numped=" & Request("numped") & "&numorden=" & Request("numorden")
							Case "Detalle"
								target = "DetallePedido.aspx"
								RutaCompleta = ConfigurationManager.AppSettings("RutaEp") & target & "?Desde=4&Idioma=" & Request.QueryString("Idioma") & "&Pedido=" & Request("Pedido") & "&ID=" & OrdenId & "&Instancia=" & Request("Instancia") _
										& "&Anyo=" & Request("Anyo") & "&numped=" & Request("numped") & "&numorden=" & Request("numorden") & "&Seguridad_Catn=" & Request("Seguridad_Catn") & "&Seguridad_Nivel=" & Request("Seguridad_Nivel")
							Case "aprob"
								target = "Aprobacion.aspx"
								RutaCompleta = ConfigurationManager.AppSettings("RutaEp") & target & "?Orden=" & OrdenId
							Case "reemitir"
								target = "Seguimiento.aspx"
								RutaCompleta = ConfigurationManager.AppSettings("RutaEp") & target & "?Orden=" & OrdenId
							Case "aprobEmail"
								RutaCompleta = ConfigurationManager.AppSettings("RutaEp") & "ws/EP_Pedidos.asmx/AprobarPedidoEP?Idioma=" & Request.QueryString("Idioma") & "&Orden=" & Request.QueryString("Orden") & "&CodAprob=" & Request.QueryString("CodAprob") & "&Target=aprob" & "&CodRnd=" & Request.QueryString("CodRnd") & "&CodSeguridad=" & Request.QueryString("CodSeguridad")
							Case "rechazEmail"
								RutaCompleta = ConfigurationManager.AppSettings("RutaEp") & "ws/EP_Pedidos.asmx/DenegarPedidoEP?Idioma=" & Request.QueryString("Idioma") & "&Orden=" & Request.QueryString("Orden") & "&CodAprob=" & Request.QueryString("CodAprob") & "&Target=aprob" & "&CodRnd=" & Request.QueryString("CodRnd") & "&CodSeguridad=" & Request.QueryString("CodSeguridad")
							Case Else
								target = "Inicio.aspx"
								RutaCompleta = ConfigurationManager.AppSettings("RutaEp") & target
						End Select
						Response.Redirect(RutaCompleta, True)
					End If
				End If
				If oUser.AccesoCN Then
					If Not Request.QueryString("cn") Is Nothing Then
						Response.Redirect(ConfigurationManager.AppSettings("rutaFS") & "cn/cn_MuroUsuario.aspx", True)
					End If
				End If


				If FSNServerLogin.TipoDeAutenticacion = TiposDeAutenticacion.ServidorExterno AndAlso FSNServerLogin.TipoDeAccesoLCX Then
					Response.Redirect(ConfigurationManager.AppSettings("rutaAutenticacionIntegrada") & "/Home/LoginLCX?codigoPerfil=" & oUser.PerfilLCX & "&codigoUsuario=" & oUser.Cod & "&codigoEmpresa=" & oUser.EmpresaLCX & "&codigoCentro=" & oUser.CentroLCX, False)
				Else
					Response.Redirect("~/App_Pages/Inicio.aspx")
				End If
			End If
		End If
	End Sub
	Private Sub txtPassword_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtPassword.PreRender
		Dim jscript As String
		jscript = ""
		jscript = "mikeypress(Event,'" & sender.clientID & "');"
		sender.Attributes.Add("onkeypress", jscript)
	End Sub
	Private Sub txtNombreUsu_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtNombreUsu.PreRender
		Dim jscript As String
		jscript = ""
		jscript = "mikeypress(event,'" & sender.clientID & "');"
		sender.Attributes.Add("onkeypress", jscript)
	End Sub
End Class