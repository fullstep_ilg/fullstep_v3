﻿Imports Fullstep.FSNLibrary
Imports Fullstep.FSNLibrary.TiposDeDatos
Partial Public Class CabCatalogo
    Inherits System.Web.UI.MasterPage

    Friend ReadOnly Property CabTabArticulos() As FSNWebControls.FSNHyperLink_Tab
        Get
            Return TabArticulos
        End Get
    End Property

    Friend ReadOnly Property CabTabCesta() As FSNWebControls.FSNHyperLink_Tab
        Get
            Return TabCesta
        End Get
    End Property

    Friend ReadOnly Property CabTabComparativa() As FSNWebControls.FSNTab
        Get
            Return TabComparativa
        End Get
    End Property

    Friend ReadOnly Property CabTabFavoritos() As FSNWebControls.FSNHyperLink_Tab
        Get
            Return TabFavoritos
        End Get
    End Property

    Friend ReadOnly Property CabTabPedidoLibre() As FSNWebControls.FSNTab
        Get
            Return TabPedidoLibre
        End Get
    End Property

    Friend ReadOnly Property CabTabPedidoAbierto() As FSNWebControls.FSNTab
        Get
            Return TabPedidoAbierto
        End Get
    End Property

    Friend ReadOnly Property CabControlCesta() As EPWebControls.FSEPWebPartCesta
        Get
            Return FSEPWebPartCesta1
        End Get
    End Property

    Friend ReadOnly Property CabUpdPestanyasCatalogo() As UpdatePanel
        Get
            Return UpdatePanelMenu
        End Get
    End Property

    Friend Property ImgCabIzq() As String
        Get
            Return Image2.ImageUrl
        End Get
        Set(ByVal value As String)
            Image2.ImageUrl = value
            upImagenYTitulo.Update()
        End Set
    End Property

    Friend Property TituloCabIzq() As String
        Get
            Return LblCatalogo.Text
        End Get
        Set(ByVal value As String)
            LblCatalogo.Text = value
            upImagenYTitulo.Update()
        End Set
    End Property

    ''' <summary>
    ''' Método que se ejecuta en el evento Init de la página en el cual agregamos a un script compuesto, mediante el scriptmanager, un archivo javascript (de modo q la carga de los scripts sea más rápida)
    ''' </summary>
    ''' <param name="sender">La página</param>
    ''' <param name="e">argumentos del evento Init</param>
    ''' <remarks></remarks>
    Private Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Dim scriptMgr As ScriptManager = ScriptManager.GetCurrent(Me.Page)
        Dim scriptRef As New System.Web.UI.ScriptReference("~/js/jsUpdateProgress.js")
        scriptMgr.CompositeScript.Scripts.Add(scriptRef)
    End Sub

    ''' <summary>
    ''' Carga la cabecera con los datos iniciales necesarios para el funcionamiento, tales como 
    ''' textos, el DataSet con los artículos y inicializa los controles para redirigir a las
    ''' paginas.
    ''' </summary>
    ''' <param name="sender">La propia pagina(En este caso la cabecera</param>
    ''' <param name="e">El evento de carga</param>
    ''' <remarks>
    ''' Llamada desde:Las páginas del EP:CatalogoWeb.aspx,PedidoLibre.aspx,EmisionPedido.aspx
    ''' Tiempo máximo:2 seg</remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim pag As FSEPPage = CType(Me.Page, FSEPPage)
        'pag.ModuloIdioma = FSNLibrary.TiposDeDatos.ModulosIdiomas.EP_CatalogoWeb
        Master.Seleccionar("Catalogo", "Emision")
        If TypeOf Me.Page Is FSEPPage Then
            If Not IsPostBack Then
                CargarTextos()
                
                If pag.FSNUser.EPPermitirEmitirPedidosDesdePedidoAbierto AndAlso pag.Acceso.gbUsarPedidosAbiertos Then
                    TabPedidoAbierto.Visible = True
                Else
                    TabPedidoAbierto.Visible = False
                End If
                If pag.FSNUser.EPTipo = TipoAccesoFSEP.AprobadorYAprovisionador OrElse pag.FSNUser.EPTipo = TipoAccesoFSEP.SoloAprovisionador Then
                    TabArticulos.Visible = True
                    TabComparativa.Visible = True
                    TabFavoritos.Visible = True
                    TabPedidoLibre.Visible = pag.FSNUser.PedidoLibre
                Else
                    TabArticulos.Visible = False
                    TabComparativa.Visible = False
                    TabFavoritos.Visible = False
                    TabPedidoLibre.Visible = False
                End If
                If Cache("DsetArticulos_" & pag.Usuario.CodPersona) Is Nothing Then
                    Dim dsArticulos As DataSet = pag.FSNServer.Get_Object(GetType(FSNServer.cArticulos)).DevolverArticulosCatalogo(0, "", "", DBNull.Value, Nothing, Nothing, Nothing, Nothing, Nothing, DBNull.Value, pag.Usuario.CodPersona, 1, "", "COD", "DESC", "")
                    pag.InsertarEnCache("DsetArticulos_" & pag.Usuario.CodPersona, dsArticulos)
                End If
                InicializarTabs()
            End If
        End If

        If Not Me.Page.ClientScript.IsClientScriptBlockRegistered(Me.Page.GetType(), "UpdatePanelSetup") Then
            Me.Page.ClientScript.RegisterClientScriptBlock(Me.Page.GetType(), "UpdatePanelSetup", "var ModalProgress = '" & Master.Master.UpdateModal.ClientID & "';", True)
        End If

        If Not Me.Page.ClientScript.IsClientScriptBlockRegistered(Me.Page.GetType(), "TabComparativaID") Then
            Me.Page.ClientScript.RegisterClientScriptBlock(Me.Page.GetType(), "TabComparativaID", "var FSNTabComparativaID='" & Me.TabComparativa.ClientID & "';", True)
        End If
    End Sub

    ''' <summary>
    ''' Inicializa los controles con los textos en el idioma del usuario
    ''' </summary>
    ''' <remarks>
    ''' Llamada desde:Page_Load
    ''' Tiempo máximo: 0 seg
    ''' </remarks>
    Public Sub CargarTextos()
        Dim pag As FSEPPage = Me.Page
        Dim AntiguoModulo As ModulosIdiomas
        AntiguoModulo = pag.ModuloIdioma
        pag.ModuloIdioma = ModulosIdiomas.EP_CatalogoWeb
        If pag.Page.ToString.ToLower.Contains("catalogoweb") Then
            LblCatalogo.Text = pag.Textos(1)
            ImgCabIzq = "~/images/Img_Ico_Catalogo.gif"
        ElseIf pag.Page.ToString.ToLower.Contains("pedidolibre") Then
            LblCatalogo.Text = pag.Textos(91)
            ImgCabIzq = "~/images/Img_Ico_Catalogo.gif"
        End If
        If Not Session("ArticulosComparar") Is Nothing AndAlso Not CType(Session("ArticulosComparar"), String()).Count = 0 Then
            TabComparativa.Enabled = True
            TabComparativa.Text = pag.Textos(89) & "(" & CType(Session("ArticulosComparar"), String()).Count & ")"
        Else
            TabComparativa.Enabled = False
            TabComparativa.Text = pag.Textos(89)
        End If
        Dim oOrdenesFavoritos As FSNServer.COrdenesFavoritos
        Dim dsetOrdenes As New DataSet
        oOrdenesFavoritos = CType(Me.Page, FSEPPage).FSNServer.Get_Object(GetType(FSNServer.COrdenesFavoritos))
        Dim iNumFavoritos As Integer = 0
        Dim oCOrdenesFavoritos As FSNServer.COrdenesFavoritos = pag.FSNServer.Get_Object(GetType(FSNServer.COrdenesFavoritos))
        dsetOrdenes = oCOrdenesFavoritos.BuscarPorUsuario(pag.FSNUser.Cod, iNumFavoritos)
        If iNumFavoritos = 0 Then
            TabFavoritos.Enabled = False
            TabFavoritos.Text = pag.Textos(90)
        Else
            TabFavoritos.Text = pag.Textos(90) & "(" & iNumFavoritos & ")"
        End If
        TabPedidoLibre.Text = pag.Textos(91)
        TabArticulos.Text = pag.Textos(88)
        TabCesta.Text = pag.Textos(165)
        TabPedidoAbierto.Text = pag.Textos(172)
        pag.ModuloIdioma = AntiguoModulo
    End Sub

    ''' <summary>
    ''' Inicializa los botones de menu para que cuando se pulse se abra la página correspondiente
    ''' </summary>
    ''' <remarks>
    ''' Llamada desde: Page_Load
    ''' Tiempo máximo:0 seg
    ''' </remarks>
    Public Sub InicializarTabs()
        TabArticulos.NavigateUrl = ConfigurationManager.AppSettings("RutaEp") & "CatalogoWeb.aspx"
        TabPedidoLibre.PostBackUrl = ConfigurationManager.AppSettings("RutaEp") & "PedidoLibre.aspx"
        TabFavoritos.NavigateUrl = ConfigurationManager.AppSettings("rutaEP") & "Favoritos.aspx"
        TabComparativa.PostBackUrl = ConfigurationManager.AppSettings("RutaEp") & "Comparativa.aspx"
        TabCesta.NavigateUrl = ConfigurationManager.AppSettings("RutaEp") & "Cesta.aspx"
        TabPedidoAbierto.PostBackUrl = ConfigurationManager.AppSettings("RutaEp") & "PedidoAbierto.aspx"
    End Sub

    ''' <summary>
    ''' Determina si se muestra el control en función de la propiedad AuthorizeWebPart y los permisos del usuario
    ''' </summary>
    ''' <param name="sender">Origen del evento.</param>
    ''' <param name="e">WebPartAuthorizationEventArgs que contiene los datos del evento.</param>
    ''' <remarks>Se produce cuando se llama al método IsAuthorized para determinar si se puede agregar un control WebPart o un control de servidor a una página.</remarks>
    Private Sub WebPartManager1_AuthorizeWebPart(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.WebParts.WebPartAuthorizationEventArgs) Handles WebPartManager1.AuthorizeWebPart
        e.IsAuthorized = CType(Me.Page, FSEPPage).AutorizacionWebparts(e.AuthorizationFilter)
    End Sub

End Class