﻿Partial Public Class Cabecera
    Inherits System.Web.UI.MasterPage

    Private ReadOnly Property HayAutenticacionWindows() As Boolean
        Get
            If Session("HayAutenticacionWindows") Is Nothing Then
                Dim FSNServer As FSNServer.Root = Session("FSN_Server")
                If Not FSNServer Is Nothing Then
                    'Session("HayAutenticacionWindows") = FSNServer.GetAccessType.giWinSecurityWeb = TiposDeAutenticacion.Windows
                    Return FSNServer.TipoDeAutenticacion = TiposDeAutenticacion.Windows
                Else
                    Return False
                End If
            Else
                Return Session("HayAutenticacionWindows")
            End If
        End Get
    End Property
    Private ReadOnly Property HayAutenticacionLDAP() As Boolean
        Get
            If Session("HayAutenticacionLDAP") Is Nothing Then
                Dim FSNServer As FSNServer.Root = Session("FSN_Server")
                If Not FSNServer Is Nothing Then
                    Session("HayAutenticacionLDAP") = FSNServer.TipoAcceso.giWinSecurityWeb = TiposDeAutenticacion.LDAP
                    Return Session("HayAutenticacionLDAP")
                Else
                    Return False
                End If
            Else
                Return Session("HayAutenticacionLDAP")
            End If
        End Get
    End Property
    Public ReadOnly Property UpdateModal() As AjaxControlToolkit.ModalPopupExtender
        Get
            Return ModalProgress
        End Get
    End Property
    Private ReadOnly Property EsTipoAccesoLCX() As Boolean
        Get
            If Session("HayAutenticacionLCX") Is Nothing Then
                Dim FSNServer As FSNServer.Root = Session("FSN_Server")
                If Not FSNServer Is Nothing Then
                    Session("HayAutenticacionLCX") = FSNServer.TipoDeAccesoLCX
                    Return Session("HayAutenticacionLCX")
                Else
                    Return False
                End If
            Else
                Return Session("HayAutenticacionLCX")
            End If
        End Get
    End Property
    ''' Revisado por: Jbg. Fecha: 28/11/2011
    ''' <summary>
    ''' Inicializa las opciones del menú superior y el pie de página.
    ''' </summary>
    ''' <param name="sender">Origen del evento.</param>
    ''' <param name="e">Clase EventArgs que no contiene datos de evento.</param>
    ''' <remarks>Se produce cuando el control de servidor se carga en el objeto Page.</remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim idiomapag As String = Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag
        html.Attributes.Add("xml:lang", idiomapag)
        html.Attributes.Add("lang", idiomapag)
        If HayAutenticacionWindows OrElse HayAutenticacionLDAP OrElse EsTipoAccesoLCX = True Then
            pnlCambioPassword.Visible = False
            If HayAutenticacionWindows Then
                If Request.Browser.Type.IndexOf("Firefox") >= 0 Then
                    'Ocultamos el botón para abandonar la sesión
                    pnlCerrarSesion.Visible = False
                Else
                    lnkbtnCerrarSesion.Attributes.Add("onclick", "window.open('','_self','');window.close();")
                End If
            End If
            If EsTipoAccesoLCX = True Then
                lnkbtnCerrarSesion.Attributes.Add("onclick", "window.open('','_self','');window.close();")
            End If
        Else
            lnkbtnCerrarSesion.Attributes.Add("onclick", "Fullstep.FSNWeb.Consultas.Desconectar();document.location.href='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "Default.aspx';return false;")
        End If

        Dim pag As FSNServer.IFSNPage = CType(Me.Page, FSNServer.IFSNPage)
        If TypeOf Me.Page Is FSNServer.IFSNPage And Not TypeOf Me.Page.Master Is FSNWeb.Login Then
            Dim modulo As TiposDeDatos.ModulosIdiomas = pag.ModuloIdioma
            If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "activadoCN") Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "activadoCN",
                    "<script>var activadoCN = " & pag.Acceso.gbAccesoFSCN.ToString.ToLower & ";</script>")
            End If
            If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "autenticacionIntegradaFrame") Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "autenticacionIntegradaFrame",
                "var autenticacionIntegradaFrame = " & (pag.FSNServer.TipoDeAutenticacion = TiposDeAutenticacion.ServidorExterno).ToString.ToLower.ToString.ToLower & "; " &
                "var parent_url ='" & ConfigurationManager.AppSettings("rutaAutenticacionIntegrada") & "';", True)
            End If
            If Not IsPostBack() Then
                pag.ModuloIdioma = TiposDeDatos.ModulosIdiomas.Menu
                ''mostrar politica de cookies solo si la autentificacion no es windows
                If Me.HayAutenticacionWindows Or Me.HayAutenticacionLDAP Or Me.EsTipoAccesoLCX Then
                    lnkbtnPolitica.Text = pag.Textos(69)
                Else
                    lnkbtnPolitica.Visible = False
                End If
                lnkbtnPreferencias.Text = pag.Textos(5)
                lnkbtnCerrarSesion.Text = pag.Textos(20)
                lnkbtnAyuda.Text = pag.Textos(19)
                lnkbtnAyuda.Visible = False
                lnkbtnInicio.Text = pag.Textos(0)
                LblProcesando.Text = pag.Textos(28)
                lblCopyright.Text = pag.Textos(29)
                lnkbtnSeguridad.Text = pag.Textos(30)

                If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "BloqueoPopUpsActivado") Then
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "BloqueoPopUpsActivado", "var sBloqueoPopupActivado='" & pag.Textos(72) & "';", True)
                End If
                'Panel de seguridad
                pag.ModuloIdioma = TiposDeDatos.ModulosIdiomas.Contraseñas
                lblPWDActual.Text = pag.Textos(1)
                lblPWDNueva.Text = pag.Textos(2)
                lblPWDNuevaRep.Text = pag.Textos(3)
                lblTituloCambioPWD.Text = pag.Textos(17)
                btnAceptarPWD.Text = pag.Textos(4)
                btnCancelarPWD.Text = pag.Textos(5)
                BtnAceptarContrOK.Text = pag.Textos(4)
                LblMsgContr.Text = pag.Textos(9)

                ' Panel Opciones
                pag.ModuloIdioma = TiposDeDatos.ModulosIdiomas.Opciones

                'etiquetas y título
                lblTitulo.Text = pag.Textos(8) & ":"
                lblSeparadorDecimal.Text = pag.Textos(10) & ":"
                lblNumeroDecimales.Text = pag.Textos(11) & ":"
                lblSeparadorMiles.Text = pag.Textos(12) & ":"
                lblFormatoFecha.Text = pag.Textos(15) & ":"
                lblFormatoEmail.Text = pag.Textos(16) & ":"
                lblIdioma.Text = pag.Textos(20) & ":"

                'botón
                btnAceptar.Text = pag.Textos(0)
                valSeparadores.ErrorMessage = pag.Textos(27)

                ddlSeparadorDecimal.SelectedValue = pag.Usuario.DecimalFmt
                ddlNumeroDecimales.SelectedValue = pag.Usuario.PrecisionFmt

                ddlSeparadorMiles.Items.Add(CrearItem("", pag.Textos(13)))  'ninguno
                ddlSeparadorMiles.Items.Add(CrearItem(",", ","))
                ddlSeparadorMiles.Items.Add(CrearItem(".", "."))
                ddlSeparadorMiles.Items.Add(CrearItem(" ", pag.Textos(14))) 'espacio
                ddlSeparadorMiles.SelectedValue = pag.Usuario.ThousanFmt

                ddlFormatoFecha.SelectedValue = pag.Usuario.DateFmt

                ddlFormatoEmail.Items.Add(CrearItem("0", pag.Textos(18)))
                ddlFormatoEmail.Items.Add(CrearItem("1", pag.Textos(17)))
                ddlFormatoEmail.SelectedValue = pag.Usuario.TipoEmail.ToString

                Dim oIdiomas As FSNServer.Idiomas

                oIdiomas = pag.FSNServer.Get_Object(GetType(FSNServer.Idiomas))
                oIdiomas.Load()

                ddlIdioma.DataSource = oIdiomas.data
                ddlIdioma.DataTextField = "DEN"
                ddlIdioma.DataValueField = "COD"
                ddlIdioma.DataBind()
                ddlIdioma.SelectedValue = pag.Usuario.Idioma
                pag.ModuloIdioma = modulo
            End If

            If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "TextosTiempoRelativo") Then
                pag.ModuloIdioma = TiposDeDatos.ModulosIdiomas.Menu
                Dim sVariableJavascriptTextosTiempoRelativo As String = "var TextosTiempoRelativo = new Array();"
                sVariableJavascriptTextosTiempoRelativo &= "TextosTiempoRelativo[0]='" & JSText(pag.Textos(46)) & "';"
                sVariableJavascriptTextosTiempoRelativo &= "TextosTiempoRelativo[1]='" & JSText(pag.Textos(47)) & "';"
                sVariableJavascriptTextosTiempoRelativo &= "var unidadesTiempo = {" &
                "1:'" & JSText(pag.Textos(48)) & "'" & ",2:'" & pag.Textos(49) & "'" & ",3:'" & JSText(pag.Textos(50)) & "'" &
                ",4:'" & JSText(pag.Textos(51)) & "'" & ",5:'" & pag.Textos(52) & "'" & ",6:'" & JSText(pag.Textos(53)) & "'" &
                ",11:'" & JSText(pag.Textos(54)) & "'" & ",12:'" & pag.Textos(55) & "'" & ",13:'" & JSText(pag.Textos(56)) & "'" &
                ",14:'" & JSText(pag.Textos(57)) & "'" & ",15:'" & pag.Textos(58) & "'" & ",16:'" & JSText(pag.Textos(59)) & "'" & "};"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "TextosTiempoRelativo", sVariableJavascriptTextosTiempoRelativo, True)
                pag.ModuloIdioma = modulo
            End If

            ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/json2.js"))
            ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/jquery.min.js"))
            ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/jquery.json.min.js"))
            ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/jquery.tmpl.min.js"))
            ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/jquery.ui.min.js"))
            ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/tmpl.min.js"))
            ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/jquery-migrate.min.js"))
            ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jsUtilities.js"))
            ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/cabecera.master.js"))
            ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/plugins/jquery.ui.datepicker.js"))
            ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/plugins/jquery.ui.datepicker." & pag.Usuario.RefCultural & ".js"))

            Dim pageName As String = System.IO.Path.GetFileName(System.Web.HttpContext.Current.Request.Url.AbsolutePath)
            If pag.Usuario.AccesoCN Then
                ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/App_Pages/CN/js/cn_funciones.js"))
                ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/plugins/autogrow.js"))
                ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/plugins/jquery.ui.maskedinput.js"))
                ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/plugins/jquery.inputmask.bundle.min.js"))
                ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/plugins/jquery.ui.fsCombo.js"))
                ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/plugins/jquery.iframe-transport.js"))
                ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/plugins/video.js"))
                ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/plugins/jquery.ui.treeview.js"))
                ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/App_Pages/CN/js/cn_ui_nuevosMensajes.js"))
                ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/plugins/jquery.fileupload.js"))
                ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/plugins/jquery.fileupload-ui.js"))
                ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/App_Pages/CN/js/cn_mensajes_ui.js"))

                Select Case pageName.ToLower
                    Case "cn_murousuario.aspx"
                        ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/App_Pages/CN/js/cn_menu.js"))
                        ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/App_Pages/CN/js/cn_mensajes.js"))
                        ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/App_Pages/CN/js/cn_Muro_init.js"))
                        ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/App_Pages/CN/js/cn_Muro_ui.js"))
                        ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/App_Pages/CN/js/cn_ui_meGusta.js"))
                        ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/App_Pages/CN/js/cn_ui_respuestas.js"))
                        ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/plugins/jquery.highlight.js"))
                    Case "cn_categorias.aspx"
                        ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/App_Pages/CN/js/cn_menu.js"))
                        ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/App_Pages/CN/js/cn_Categorias_init.js"))
                        ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/App_Pages/CN/js/cn_Categorias_ui.js"))
                    Case "cn_userprofile.aspx"
                        ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/App_Pages/CN/js/cn_menu.js"))
                        ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/App_Pages/CN/js/cn_UserProfile_init.js"))
                        ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/App_Pages/CN/js/cn_UserProfile_ui.js"))
                    Case "cn_grupos.aspx"
                        ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/App_Pages/CN/js/cn_menu.js"))
                        ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/App_Pages/CN/js/cn_Grupos_init.js"))
                        ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/App_Pages/CN/js/cn_Grupos_ui.js"))
                End Select
            End If
            Select Case pageName.ToLower
                Case "detallefactura.aspx"
                    ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/App_Pages/CN/js/cn_menu.js"))
                    ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/App_Pages/CN/js/cn_nuevaDiscrepancia.js"))
                    ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/App_Pages/CN/js/cn_mensajes.js"))
                    ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/App_Pages/CN/js/cn_ui_meGusta.js"))
                    ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/App_Pages/CN/js/cn_ui_respuestas.js"))
                    ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/App_Pages/PM/Facturas/js/pm_DetalleFactura_init.js"))
                    ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jsUpdateProgress.js"))
                Case "visorfacturas.aspx"
                    ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/App_Pages/CN/js/cn_menu.js"))
                    ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/App_Pages/CN/js/cn_mensajes.js"))
                    ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/App_Pages/CN/js/cn_ui_meGusta.js"))
                    ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/App_Pages/CN/js/cn_ui_respuestas.js"))
                    ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/App_Pages/PM/Facturas/js/pm_VisorFacturas_init.js"))
                    ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jsUpdateProgress.js"))
                Case "detalleperfil.aspx"
                    ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/App_Pages/SG/js/DetallePerfil_init.js"))
                    If Not pag.Usuario.AccesoCN Then ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/plugins/jquery.ui.treeview.js"))
                Case "cubos.aspx"
                    ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/App_Pages/BI/js/permisosCubos.js"))
                    If Not (pag.Usuario.AccesoCN) Then ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/plugins/jquery.ui.treeview.js"))
                Case "visoractividad.aspx"
                    ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/App_Pages/BI/js/modernizr.min.js"))
                    ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/App_Pages/BI/js/infragistics.loader.js"))
                    ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/App_Pages/BI/js/infragistics.core.js"))
                    ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/App_Pages/BI/js/infragistics.dv.js"))
                    ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/App_Pages/BI/js/infragistics.lob.js"))
                Case "visorrendimiento.aspx"
                    ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/App_Pages/BI/js/modernizr.min.js"))
                    ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/App_Pages/BI/js/infragistics.loader.js"))
                    ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/App_Pages/BI/js/infragistics.core.js"))
                    ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/App_Pages/BI/js/infragistics.dv.js"))
                    ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/App_Pages/BI/js/infragistics.lob.js"))
                Case "emisionpedido.aspx"
                    If Not (pag.Usuario.AccesoCN) Then ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/plugins/jquery.ui.fsCombo.js"))
                    If Not (pag.Usuario.AccesoCN) Then ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/plugins/jquery.inputmask.bundle.min.js"))
                    ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/App_Pages/EP/js/emisionpedido.js"))
                    ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/plugins/jquery.multiselect.js"))
                    ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/plugins/jquery.multiselect.filter.js"))
                    ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/plugins/jquery.numeric.min.js"))
                    ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/plugins/numericFormatted.js"))
                    ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/plugins/jquery.tokeninput.js"))
                    ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/formatos.js"))
                Case "detallepedido.aspx"
                    ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/App_Pages/EP/js/DetallePedido.js"))
                    ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/plugins/jquery.multiselect.js"))
                    ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/plugins/jquery.multiselect.filter.js"))
                    ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/plugins/jquery.numeric.min.js"))
                    ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jsTextboxFormat.js"))
                Case "visorsolicitudes.aspx"
                    ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/plugins/jstree.min.js"))
                    ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/App_Pages/PM/Tareas/js/VisorSolicitudes.js"))
                    ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/plugins/jquery.multiselect.js"))
                    ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/plugins/jquery.multiselect.filter.js"))
                    If Not pag.Usuario.AccesoCN Then
                        ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/plugins/jquery.ui.treeview.js"))
                        ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/plugins/jquery.inputmask.bundle.min.js"))
                    End If
                    ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/plugins/jquery.numeric.min.js"))
                    ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/plugins/numericFormatted.js"))
                    ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/plugins/contextmenu.min.js"))
                    ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/plugins/jquery.tokeninput.js"))
                    ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/App_Pages/PM/Tareas/js/VisorSolicitudes_NuevoEscenario.js"))
                Case "panelcalidad.aspx"
                    ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jsUpdateProgress.js"))
                    ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/App_Pages/QA/Puntuacion/js/panelCalidad.js"))
                Case "panelcalidadfiltro.aspx"
                    ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/plugins/jquery.tokeninput.js"))
                    ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/plugins/jstree.min.js"))
                    ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/App_Pages/QA/Puntuacion/js/panelCalidadFiltro.js"))
                Case "pedidoabierto.aspx"
                    ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/App_Pages/EP/js/PedidoAbierto.js"))
                    ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/plugins/jquery.numeric.min.js"))
                    ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/plugins/numericFormatted.js"))
                    ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/plugins/jquery.tokeninput.js"))
                Case "notificaciones.aspx"
                    ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jsUpdateProgress.js"))
                Case "recepcion.aspx"
                    ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jsLimiteDecimales.js"))
                    ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jsTextboxFormat.js"))
                Case "seguimiento.aspx"
                    ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jsTextboxFormat.js"))
                    ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jsUpdateProgress.js"))
                Case "favoritos.aspx"
                    ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jsLimiteDecimales.js"))
                Case "pedidolibre.aspx"
                    ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jsLimiteDecimales.js"))
                Case "comparativa.aspx"
                    ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jsLimiteDecimales.js"))
                Case "inicio.aspx"
                    ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jsUpdateProgress.js"))
                Case "visorintegracion.aspx"
                    ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jsUpdateProgress.js"))
                Case "configurarfiltroscontratos.aspx"
                    ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jsUpdateProgress.js"))
                Case "configurarfiltrosfacturas.aspx"
                    ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jsUpdateProgress.js"))
                Case "configurarfiltrosQA.aspx"
                    ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jsUpdateProgress.js"))
                Case "confignivelescalacionproves.aspx"
                    ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/plugins/fsMultiLanguage.js"))
                    ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/plugins/fsHierarchicalDropDown.js"))
                    ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/App_Pages/QA/Configuracion/js/ConfigNivelEscalacionProves.js"))
                Case "panelescalacionproves.aspx"
                    ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/App_Pages/QA/EscalacionProveedores/js/PanelEscalacionProves.js"))
                Case "fichaproveedor.aspx"
                    ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/plugins/jquery.numeric.min.js"))
                    ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/plugins/numericFormatted.js"))
            End Select
            ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/AcelerarRendimientoJavascriptIE.js"))
            'Para usar el ckEditor es necesario agregar los siguientes scripts.
            'Sin embargo, no pueden añadirse a ninguna rutina que los unifique en un único fichero (como el compositescript de MS)
            'Es posible que sea porque el ckeditor.js hace referencia a su propia ruta para agregar su propio contenido como un script:
            'e.type='text/javascript';e.src=a.basePath+'ckeditor.js';document.getElementsByTagName('head')[0].appendChild(e);
            ScriptManager1.Scripts.Add(New ScriptReference(ConfigurationManager.AppSettings("ruta") & "ckeditor/ckeditor.js"))
            If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "DateFormatUsu") Then
                With pag.Usuario
                    Dim sVariablesJavascript As String = "var UsuMask = '" & .DateFormat.ShortDatePattern & "';"
                    sVariablesJavascript &= "var UsuMaskSeparator='" & .DateFormat.DateSeparator & "';"
                    sVariablesJavascript &= "var UsuLanguageTag='" & .RefCultural & "';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "DateFormatUsu", sVariablesJavascript, True)
                End With
            End If

            If pag.Usuario.AccesoCN Then
                pag.ModuloIdioma = TiposDeDatos.ModulosIdiomas.Colaboracion
                If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "segundosNotificacionesCN") Then
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "segundosnNotificacionesCN",
                        "var segundosNotificacionesCN=" & CType(ConfigurationManager.AppSettings("segundosNotificacionesCN"), Integer) * 1000 & ";", True)
                End If
                If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "TextosPantalla") Then
                    Dim sVariableJavascriptTextos As String = "var Textos = new Array();"
                    Dim sVariableJavascriptTextosCKEditor As String = "var TextosCKEditor = new Array();"

                    For i As Integer = 0 To 73
                        sVariableJavascriptTextos &= "Textos[" & i & "]='" & JSText(pag.Textos(i)) & "';"
                        If i = 20 Then sVariableJavascriptTextosCKEditor &= "TextosCKEditor[1]='" & JSText(pag.Textos(i)) & "';"
                        If i = 21 Then sVariableJavascriptTextosCKEditor &= "TextosCKEditor[2]='" & JSText(pag.Textos(i)) & "';"
                    Next
                    For i As Integer = 83 To 85
                        sVariableJavascriptTextos &= "Textos[" & i - 9 & "]='" & JSText(pag.Textos(i)) & "';"
                    Next
                    For i As Integer = 74 To 89
                        sVariableJavascriptTextosCKEditor &= "TextosCKEditor[" & i - 71 & "]='" & JSText(pag.Textos(i)) & "';"
                    Next
                    For i As Integer = 90 To 121
                        sVariableJavascriptTextos &= "Textos[" & i - 13 & "]='" & JSText(pag.Textos(i)) & "';"
                    Next
                    For i As Integer = 142 To 143
                        sVariableJavascriptTextos &= "Textos[" & i - 33 & "]='" & JSText(pag.Textos(i)) & "';"
                    Next
                    For i As Integer = 144 To 147
                        sVariableJavascriptTextosCKEditor &= "TextosCKEditor[" & i - 125 & "]='" & JSText(pag.Textos(i)) & "';"
                    Next
                    For i As Integer = 148 To 150
                        sVariableJavascriptTextos &= "Textos[" & i - 37 & "]='" & JSText(pag.Textos(i)) & "';"
                    Next
                    For i As Integer = 151 To 154
                        sVariableJavascriptTextosCKEditor &= "TextosCKEditor[" & i - 128 & "]='" & JSText(pag.Textos(i)) & "';"
                    Next
                    sVariableJavascriptTextos &= "Textos[114]='" & JSText(pag.Textos(155)) & "';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "TextosPantalla", sVariableJavascriptTextos, True)
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "TextosCKEditor", sVariableJavascriptTextosCKEditor, True)
                End If
                pag.ModuloIdioma = modulo
            End If
        End If
        lnkbtnInicio.PostBackUrl = ConfigurationManager.AppSettings("rutaFS") & "Inicio.aspx"
        lblNumVersion.Text = ConfigurationManager.AppSettings("version")
        lblAnyoCopy.Text = ConfigurationManager.AppSettings("copyright")

        imgChngPwd.Src = ConfigurationManager.AppSettings("ruta") + "images/ChangePassword.jpg"
        ImgInfoContrOK.ImageUrl = ConfigurationManager.AppSettings("ruta") + "images/ChangePassword.jpg"
        ImgInfoContrMal.ImageUrl = ConfigurationManager.AppSettings("ruta") + "images/alert-large.gif"
        imgOcultaPWD.ImageUrl = ConfigurationManager.AppSettings("ruta") + "images/trans.gif"

        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "rutas") Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutas",
                "<script>var rutaFS = '" & System.Configuration.ConfigurationManager.AppSettings("rutaFS") & "';" &
                "var ruta = '" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "';" &
                "var rutaPM = '" & System.Configuration.ConfigurationManager.AppSettings("rutaPM") & "';</script>")
        End If
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "version") Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "version",
                "<script>var version = '" & System.Configuration.ConfigurationManager.AppSettings("version") & "';</script>")
        End If

        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "ScriptCargando") Then
            Dim sScript = "var tmpcargando;" & vbCrLf &
             "function MostrarCargando() {" & vbCrLf &
             "window.clearTimeout(tmpcargando);" & vbCrLf &
             "var modalprog=$find('" & ModalProgress.ClientID & "');" & vbCrLf &
             "if (modalprog) modalprog.show();" & vbCrLf &
             "}" & vbCrLf &
             "function TempCargando(disabled) {" & vbCrLf &
             "if(disabled!='') {tmpcargando = window.setTimeout(MostrarCargando, 2000);}" & vbCrLf &
             "}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ScriptCargando", sScript, True)
        End If
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "AjustarTamResolucion") Then
            Dim sScript As String
            If Request.Browser.Browser = "IE" And Request.Browser.MajorVersion <= 6 Then
                sScript = "var __altcont = 250;" & vbCrLf &
                 "var ajustarTamInfoQS = 0;" & vbCrLf &
                 "function AjustarTam() {" & vbCrLf &
                 "if(ajustarTamInfoQS==1) {ajustarTamInfoQS=0;return;}" & vbCrLf &
                 "var capaContenido = document.getElementById('Contenido');" & vbCrLf &
                 "if (capaContenido) {" & vbCrLf &
                 "if (document.documentElement.scrollHeight != document.documentElement.clientHeight) {" & vbCrLf &
                 "var altnueva = __altcont + document.documentElement.clientHeight - document.documentElement.scrollHeight;" & vbCrLf &
                 "if (altnueva < 250) altnueva = 250;" & vbCrLf &
                 "if (altnueva != __altcont) {" & vbCrLf &
                 "__altcont = altnueva;" & vbCrLf &
                 "capaContenido.style.Height = __altcont + 'px';" & vbCrLf &
                 "}" & vbCrLf &
                 "}" & vbCrLf &
                 "}" & vbCrLf &
                 "if (document.getElementById('SLOBJECT')){ " & vbCrLf &
                 "adjustSL();" & vbCrLf &
                 "} " & vbCrLf &
                 "return true;" & vbCrLf &
                 "}" & vbCrLf
            ElseIf Request.Browser.Browser = "IE" And Request.Browser.MajorVersion = 7 Then
                sScript = "var __altcont = 250;" & vbCrLf &
                 "var ajustarTamInfoQS = 0;" & vbCrLf &
                 "function AjustarTam() {" & vbCrLf &
                 "if(ajustarTamInfoQS==1) {ajustarTamInfoQS=0;return;}" & vbCrLf &
                 "var capaContenido = document.getElementById('Contenido');" & vbCrLf &
                 "if (capaContenido) {" & vbCrLf &
                 "if (document.documentElement.scrollHeight != document.documentElement.clientHeight) {" & vbCrLf &
                 "var altnueva = __altcont + document.documentElement.clientHeight - document.documentElement.scrollHeight;" & vbCrLf &
                 "if (altnueva < 250) altnueva = 250;" & vbCrLf &
                 "if (altnueva != __altcont) {" & vbCrLf &
                 "__altcont = altnueva;" & vbCrLf &
                 "capaContenido.style.minHeight = __altcont + 'px';" & vbCrLf &
                 "}" & vbCrLf &
                 "}" & vbCrLf &
                 "}" & vbCrLf &
                 "if (document.getElementById('SLOBJECT')){ " & vbCrLf &
                 "adjustSL();" & vbCrLf &
                 "} " & vbCrLf &
                 "return true;" & vbCrLf &
                 "}" & vbCrLf
            Else
                sScript = "var __altcont;" & vbCrLf &
                 "var ajustarTamInfoQS = 0;" & vbCrLf &
                 "function AjustarTam() {" & vbCrLf &
                 "if(ajustarTamInfoQS==1) {ajustarTamInfoQS=0;return;}" & vbCrLf &
                 "var capaContenido = document.getElementById('Contenido');" & vbCrLf &
                 "if (capaContenido) {" & vbCrLf &
                 "var alt = document.documentElement.clientHeight - 235;" & vbCrLf &
                 "if (alt < 250) alt = 250;" & vbCrLf &
                 "if (__altcont && document.documentElement.scrollHeight!=document.documentElement.clientHeight)" & vbCrLf &
                 "alt=__altcont - document.documentElement.scrollHeight + document.documentElement.clientHeight" & vbCrLf &
                 "else{" & vbCrLf &
                 "if (alt>__altcont && alt-__altcont<40) alt=__altcont;" & vbCrLf &
                 "}" & vbCrLf &
                 "if (alt < 250) alt = 250;" & vbCrLf &
                 "if (alt!=__altcont){" & vbCrLf &
                 "__altcont=alt;" & vbCrLf &
                 "capaContenido.style.minHeight = __altcont + 'px'; " & vbCrLf &
                 "}" & vbCrLf &
                 "}" & vbCrLf &
                  "if (document.getElementById('SLOBJECT')){ " & vbCrLf &
                 "adjustSL();" & vbCrLf &
                 "} " & vbCrLf &
                 "return true;" & vbCrLf &
                 "}" & vbCrLf
            End If
            sScript = sScript & "window.onresize = AjustarTam;" & vbCrLf &
              "window.onload = AjustarTam;"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "AjustarTamResolucion", sScript, True)
        End If
        If Not Page.ClientScript.IsStartupScriptRegistered(Me.GetType(), "AjustarTamAsinc") Then
            Dim sScript As String = "Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(AjustarTam);"
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "AjustarTamAsinc", sScript, True)
        End If

        If Not Page.IsPostBack Then
            If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "ScriptShortcut") Then
                Dim sScript As String = "<script src=""" & ConfigurationManager.AppSettings("ruta") & "js/shortcut.js"" type=""text/javascript""></script>"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ScriptShortcut", sScript)
            End If
            If Not Page.ClientScript.IsStartupScriptRegistered(Me.GetType(), "QSInfoShortcut") Then
                Dim sScript As String = "var QSInfo='" & Me.DivQSInfo.ClientID & "';" & vbCrLf

                sScript = sScript & " var master_options = {" & vbCrLf
                sScript = sScript & " 'type': 'keydown'," & vbCrLf
                sScript = sScript & "'propagate': false," & vbCrLf
                sScript = sScript & "'disable_in_input': false," & vbCrLf
                sScript = sScript & "'target': """ & Pie.ClientID & """," & vbCrLf
                sScript = sScript & "'keycode': false" & vbCrLf
                sScript = sScript & "};" & vbCrLf
                sScript = sScript & "shortcut.add('',function() {" & vbCrLf
                If pag.Usuario Is Nothing Then
                    sScript = sScript & "Fullstep.FSNWeb.Consultas.DameInfoQS('" & pag.Idioma & "','',OnGetInfoQSComplete);" & vbCrLf
                Else
                    sScript = sScript & "Fullstep.FSNWeb.Consultas.DameInfoQS('" & pag.Idioma & "','" & pag.Usuario.Cod & "',OnGetInfoQSComplete);" & vbCrLf
                End If
                sScript = sScript & "}, master_options);" & vbCrLf
                sScript = sScript & "function OnGetInfoQSComplete(result) {" & vbCrLf
                sScript = sScript & "$get(QSInfo).style.display = 'inline';" & vbCrLf
                sScript = sScript & "$get(QSInfo).innerHTML = result;" & vbCrLf
                sScript = sScript & "ajustarTamInfoQS = 1;" & vbCrLf
                sScript = sScript & "}" & vbCrLf
                Page.ClientScript.RegisterStartupScript(Me.GetType(), "QSInfoShortcut", sScript, True)
            End If
        End If

        ScriptManager1.RegisterAsyncPostBackControl(btnAceptarPWD)
        ScriptManager1.RegisterAsyncPostBackControl(btnCancelarPWD)
        ScriptManager1.RegisterAsyncPostBackControl(BtnAceptarContrOK)
        ScriptManager1.RegisterAsyncPostBackControl(lnkbtnSeguridad)
        ScriptManager1.Services.Add(New ServiceReference(ConfigurationManager.AppSettings("rutaFS") & "_Common/App_Services/Consultas.asmx"))
        ScriptManager1.EnablePageMethods = True

        If ConfigurationManager.AppSettings("OcultarPiePagina") = "1" Then Pie.Visible = False
    End Sub
    ''' <summary>
    ''' Llama al método Desconectar para cerrar la sesión.
    ''' </summary>
    ''' <param name="sender">Origen del evento.</param>
    ''' <param name="e">Clase EventArgs que no contiene datos de evento.</param>
    ''' <remarks>Se produce cuando se hace clic en el control LinkButton lnkbtnCerrarSesion.</remarks>
    Protected Sub lnkbtnCerrarSesion_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkbtnCerrarSesion.Click
        Desconectar()
    End Sub
    ''' Revisado por: blp. Fecha: 05/11/2012
    ''' <summary>
    ''' Cierra la sesión y redirige a la página de login.
    ''' </summary>
    ''' <remarks>Llamadas desde: lnkbtnCerrarSesion_Click</remarks>
    Public Sub Desconectar()
        Dim RedireccionaAInicio As Boolean = Not HayAutenticacionWindows
        Session.Abandon()
        Session.Clear()
        If RedireccionaAInicio AndAlso Request.Browser.Type.IndexOf("Firefox") < 0 Then
            Response.Redirect(ConfigurationManager.AppSettings("ruta") & "Default.aspx")
        ElseIf Request.Browser.Type.IndexOf("Firefox") >= 0 Then 'Esta opcióin no tiene mucho sentido dado que en Firefox hemos ocultado el botón "Abandonar Sesión" pero lo dejamos por si acaso...
            Response.Redirect("about:blank")
        End If
    End Sub
    ''' <summary>
    ''' Crea un listitem para insertarlo en un dropdownlist
    ''' </summary>
    ''' <param name="Valor">Valor que se pone en la propiedad Value del listitem</param>
    ''' <param name="Texto">Texto que se va a mostrar y se pone en la propiedad Text del listitem</param>
    ''' <returns>El objeto listitem</returns>
    ''' <remarks>Llamada desde: Page_onLoad; Tiempo máximo: 0 sg</remarks>
    Private Function CrearItem(ByVal Valor As String, ByVal Texto As String) As System.Web.UI.WebControls.ListItem
        Dim iListItem As New System.Web.UI.WebControls.ListItem
        iListItem.Value = Valor
        iListItem.Text = Texto
        CrearItem = iListItem
    End Function
    ''' <summary>
    ''' Guarda las opciones del usuario en Base de Datos
    ''' </summary>
    ''' <remarks>Llamada desde: Al pulsar el botón Aceptar. Tiempo máximo: 0 sg</remarks>
    Protected Sub btnAceptar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Dim pag As FSNServer.IFSNPage = CType(Me.Page, FSNServer.IFSNPage)

        If pag.Usuario.DecimalFmt <> ddlSeparadorDecimal.SelectedValue _
         Or pag.Usuario.PrecisionFmt <> ddlNumeroDecimales.SelectedValue _
         Or pag.Usuario.ThousanFmt <> ddlSeparadorMiles.SelectedValue _
         Or pag.Usuario.DateFmt <> ddlFormatoFecha.SelectedValue _
         Or pag.Usuario.TipoEmail <> ddlFormatoEmail.SelectedValue _
         Or pag.Usuario.Idioma <> ddlIdioma.SelectedValue Then

            pag.Usuario.DecimalFmt = ddlSeparadorDecimal.SelectedValue
            pag.Usuario.PrecisionFmt = ddlNumeroDecimales.SelectedValue
            pag.Usuario.ThousanFmt = ddlSeparadorMiles.SelectedValue
            pag.Usuario.DateFmt = ddlFormatoFecha.SelectedValue
            pag.Usuario.TipoEmail = ddlFormatoEmail.SelectedValue
            pag.Usuario.Idioma = ddlIdioma.SelectedValue

            pag.Usuario.SaveUserData()

            'BLP
            'Hasta ahora, cuando se cambiaban las opciones de usuario, nunca se han recargado con el LoadUserData porque los cambios
            'estaban disponible atravÃ©s de propiedades (ej:pag.Usuario.DateFmt). 
            'Sin embargo, el campo pag.Usuario.DateFormat.dateSeparator es un campo calculado en el LoadUserData, no se asigna a propiedad alguna
            'por lo que, ahora, si se cambia el pag.Usuario.DateFmt, habrÃ­a que lanzar de nuevo el LoadUserData o bien,
            'hacer aquÃ­ lo Ãºnico del LoadUserData que nos interesa: asignar el dateSeparator
            'Opcion(A)
            'pag.Usuario.LoadUserData(pag.Usuario.Cod)
            'Opcion(B)
            pag.Usuario.DateFormat.DateSeparator = encontrarSeparadorFecha(pag.Usuario.DateFmt)
            'Fin

            Session("CargarUserDataPMWeb") = True

			pnlOpciones_ModalPopupExtender.Hide()
			ModalProgress.Show()
            ScriptManager.RegisterStartupScript(Me.Page, Me.Page.GetType(), "recargarpagina", "MostrarCargando();document.location.href=document.location.href;", True)
        Else
            pnlOpciones_ModalPopupExtender.Hide()
        End If
    End Sub
    ''' <summary>
    ''' Cierra el panel de opciones
    ''' </summary>
    ''' <param name="sender">Origen del evento.</param>
    ''' <param name="e">Clase EventArgs que no contiene datos de evento.</param>
    ''' <remarks>Se produce cuando se hace clic en el control ImageButton imgCerrar.</remarks>
    Private Sub imgCerrar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgCerrar.Click
        pnlOpciones_ModalPopupExtender.Hide()
    End Sub
    ''' <summary>
    ''' Evento que se genera al pulsar el botÃ³n de cancelar la contraseÃ±a, cerrando el panel de la contraseÃ±a
    ''' </summary>
    ''' <param name="sender">El objeto que lanza el evento(btnCancelarPWD)</param>
    ''' <param name="e">Los argumentos del evento</param>
    ''' <remarks>
    ''' Llamada desde: AutomÃ¡tica, cuando se pincha el botÃ³n
    ''' Tiempo mÃ¡ximo: 0 seg</remarks>
    Private Sub btnCancelarPWD_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelarPWD.Click
        pnlSeguridad_ModalPopupExtender.Hide()
    End Sub
    ''' <summary>
    ''' Evento que se genera al pulsar el botÃ³n de aceptar la contraseÃ±a, comprobando los datos y llamando al webservice
    ''' </summary>
    ''' <param name="sender">El objeto que lanza el evento(btnAceptarPWD)</param>
    ''' <param name="e">Los argumentos del evento</param>
    ''' <remarks>
    ''' Llamada desde: AutomÃ¡tica, cuando se pincha el botÃ³n
    ''' Tiempo mÃ¡ximo: 0,2 seg</remarks>
    Protected Sub btnAceptarPWD_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptarPWD.Click
        Dim webService As New FSNWebServicePWD.CambioPWD
        Dim errorPWD As Integer = 0
        Dim dFechaNuevaPWD As Date
        Dim pag As FSNServer.IFSNPage = CType(Me.Page, FSNServer.IFSNPage)
        Dim modulo As TiposDeDatos.ModulosIdiomas = pag.ModuloIdioma

        pag.ModuloIdioma = TiposDeDatos.ModulosIdiomas.Contraseñas

        If pag.Usuario.Password <> txtPWDActual.Text Then
            LblMsgContr.Text = pag.Textos(16)
            txtPWDActual.Text = ""
            ImgInfoContrOK.Visible = False
            ImgInfoContrMal.Visible = True
            UpDatosPanelCambioContr.Update()
            mpeMsgCambioContr.Show()
            pag.ModuloIdioma = modulo
            upSeguridadPWD.Update()
            Exit Sub
        ElseIf txtPWDNueva.Text <> txtPWDNuevaRep.Text Then
            LblMsgContr.Text = pag.Textos(15)
            ImgInfoContrOK.Visible = False
            ImgInfoContrMal.Visible = True
            txtPWDNuevaRep.Text = ""
            UpDatosPanelCambioContr.Update()
            mpeMsgCambioContr.Show()
            pag.ModuloIdioma = modulo
            upSeguridadPWD.Update()
            Exit Sub
        End If
        Try
            dFechaNuevaPWD = Date.Now
            errorPWD = webService.CambiarLogin(0, pag.Usuario.Cod, txtPWDActual.Text, pag.Usuario.Cod, txtPWDNueva.Text,
                       dFechaNuevaPWD.Year, dFechaNuevaPWD.Month, dFechaNuevaPWD.Day,
                       dFechaNuevaPWD.Hour, dFechaNuevaPWD.Minute, dFechaNuevaPWD.Second,
                       0, "",
                       pag.Usuario.FechaPassword.Year, pag.Usuario.FechaPassword.Month, pag.Usuario.FechaPassword.Day,
                       pag.Usuario.FechaPassword.Hour, pag.Usuario.FechaPassword.Minute, pag.Usuario.FechaPassword.Second)
        Catch ex As Exception
            pag.ModuloIdioma = modulo
            Exit Sub
        End Try

        If errorPWD = 0 Then
            pag.Usuario.Password = txtPWDNueva.Text
            pag.Usuario.FechaPassword = dFechaNuevaPWD

            ImgInfoContrOK.Visible = True
            ImgInfoContrMal.Visible = False
            LblMsgContr.Text = pag.Textos(6)
            UpDatosPanelCambioContr.Update()
            mpeMsgCambioContr.Show()
            pnlSeguridad_ModalPopupExtender.Hide()
        Else
            Select Case errorPWD
                Case 1
					Dim iLongitudMinima As Integer
					If pag.FSNServer.TipoAcceso.gbCOMPLEJIDAD_PWD Then
						iLongitudMinima = If(pag.FSNServer.TipoAcceso.giMIN_SIZE_PWD > 8, pag.FSNServer.TipoAcceso.giMIN_SIZE_PWD, 8)
					Else
						iLongitudMinima = pag.FSNServer.TipoAcceso.giMIN_SIZE_PWD
					End If
                    LblMsgContr.Text = pag.Textos(7) & " " & iLongitudMinima & " " & pag.Textos(8)
                    txtPWDNueva.Text = ""
                    txtPWDNuevaRep.Text = ""
                Case 2
                    LblMsgContr.Text = pag.Textos(9) & "<br>" & pag.Textos(20) & "<p>" & pag.Textos(7) & " " & pag.FSNServer.TipoAcceso.giMIN_SIZE_PWD & " " & pag.Textos(8) & "</p><p>" & pag.Textos(21) & "</p><p>" & pag.Textos(22) & "</p>"
                    txtPWDNueva.Text = ""
                    txtPWDNuevaRep.Text = ""
                Case 3
                    LblMsgContr.Text = pag.Textos(10)
                    txtPWDNueva.Text = ""
                    txtPWDNuevaRep.Text = ""
                Case 4
                    LblMsgContr.Text = pag.Textos(11) & pag.FSNServer.TipoAcceso.giHISTORICO_PWD & pag.Textos(12)
                    txtPWDNueva.Text = ""
                    txtPWDNuevaRep.Text = ""
                Case 5
                    LblMsgContr.Text = pag.Textos(13)
                    txtPWDNueva.Text = ""
                    txtPWDNuevaRep.Text = ""
                Case Else
                    LblMsgContr.Text = pag.Textos(14)
                    txtPWDNueva.Text = ""
                    txtPWDNuevaRep.Text = ""
                    txtPWDActual.Text = ""
            End Select
            ImgInfoContrOK.Visible = False
            ImgInfoContrMal.Visible = True
            UpDatosPanelCambioContr.Update()
            upSeguridadPWD.Update()
            mpeMsgCambioContr.Show()
        End If
        pag.ModuloIdioma = modulo
    End Sub
    ''' <summary>
    ''' Evento que se genera cuando se pincha el LinkButton de seguridad de la cabecera, mostrando el panel de cambio de contraseÃ±a
    ''' </summary>
    ''' <param name="sender">El objeto que lanza el evento, en este caso el LinkButton</param>
    ''' <param name="e">Los argumentos del evento</param>
    ''' <remarks>
    ''' Llamada desde: La propia pÃ¡gina, cada cez que se clique el LinkButton de seguridad
    ''' Tiempo mÃ¡ximo: 0 seg</remarks>
    Protected Sub lnkBtnSeguridad_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkbtnSeguridad.Click
        txtPWDActual.Text = ""
        txtPWDNueva.Text = ""
        txtPWDNuevaRep.Text = ""
        upSeguridadPWD.Update()
        pnlSeguridad_ModalPopupExtender.Show()
    End Sub
End Class