﻿Public Partial Class EnBlanco
    Inherits System.Web.UI.MasterPage
    ''' <summary>
    ''' Esta pagina se usa desde Contratos y listados (para GS): background: DCDCDC y desde el Visor de Notificaciones: background: FFFFFF
    ''' </summary>
    ''' <param name="sender">pagina</param>
    ''' <param name="e">del sistema</param>
    ''' <remarks>Llamada desde: sistema ; Tiempo máximo: 0</remarks>
    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If (Not String.IsNullOrEmpty(Session("desdePanelFicha")) AndAlso (Session("desdePanelFicha") = "1")) Or
            System.IO.Path.GetFileName(System.Web.HttpContext.Current.Request.Url.AbsolutePath).ToLower = "asignarunidadesorganizativas.aspx" Then
            Me.bodyEnBlanco.Style.Item("background-color") = "#FFFFFF"
        End If
    End Sub
    Private Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "EnBlancoMaster") Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "EnBlancoMaster", "var EnBlancoMaster=true;", True)
        End If
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "ScriptCargando") Then
            Dim sScript = "var tmpcargando;" & vbCrLf & _
                "function MostrarCargando() {" & vbCrLf & _
                "window.clearTimeout(tmpcargando);" & vbCrLf & _
                "var modalprog=$find('" & ModalProgress.ClientID & "');" & vbCrLf & _
                "if (modalprog) modalprog.show();" & vbCrLf & _
                "}" & vbCrLf & _
                "function TempCargando(disabled) {" & vbCrLf & _
                "if(disabled!='') {tmpcargando = window.setTimeout(MostrarCargando, 2000);}" & vbCrLf & _
                "}"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ScriptCargando", sScript, True)
        End If
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "segundosNotificacionesCN") Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "segundosnNotificacionesCN", _
                "var segundosNotificacionesCN=" & CType(ConfigurationManager.AppSettings("segundosNotificacionesCN"), Integer) * 1000 & ";", True)
        End If
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "activadoCN") Then
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "activadoCN", _
            "<script>var activadoCN = " & CType(Me.Page, FSNServer.IFSNPage).Acceso.gbAccesoFSCN.ToString.ToLower & ";</script>")
        End If
        Dim scriptReference As ScriptReference
        scriptReference = New ScriptReference
        scriptReference.Path = "~/js/json2.js"
        ScriptManager1.CompositeScript.Scripts.Add(scriptReference)
        scriptReference = New ScriptReference
        scriptReference.Path = "~/js/jquery/jquery.min.js"
        ScriptManager1.CompositeScript.Scripts.Add(scriptReference)
        scriptReference = New ScriptReference
        scriptReference.Path = "~/js/jquery/jquery.json.min.js"
        ScriptManager1.CompositeScript.Scripts.Add(scriptReference)
        scriptReference = New ScriptReference
        scriptReference.Path = "~/js/jquery/jquery.tmpl.min.js"
        ScriptManager1.CompositeScript.Scripts.Add(scriptReference)
        scriptReference = New ScriptReference
        scriptReference.Path = "~/js/jquery/jquery.ui.min.js"
        ScriptManager1.CompositeScript.Scripts.Add(scriptReference)
        scriptReference = New ScriptReference
        scriptReference.Path = "~/js/jquery/tmpl.min.js"
        ScriptManager1.CompositeScript.Scripts.Add(scriptReference)
        scriptReference = New ScriptReference
        scriptReference.Path = "~/js/jquery/jquery-migrate.min.js"
        ScriptManager1.CompositeScript.Scripts.Add(scriptReference)
        scriptReference = New ScriptReference
        scriptReference.Path = "~/js/jsUtilities.js"
        ScriptManager1.CompositeScript.Scripts.Add(scriptReference)
        scriptReference = New ScriptReference
        scriptReference.Path = "~/js/jquery/plugins/jquery.ui.treeview.js"
        ScriptManager1.CompositeScript.Scripts.Add(scriptReference)
        scriptReference = New ScriptReference
        scriptReference.Path = "~/js/jquery/plugins/jquery.iframe-transport.js"
        ScriptManager1.CompositeScript.Scripts.Add(scriptReference)
        scriptReference = New ScriptReference
        scriptReference.Path = "~/js/jquery/plugins/jquery.fileupload.js"
        ScriptManager1.CompositeScript.Scripts.Add(scriptReference)
        scriptReference = New ScriptReference
        scriptReference.Path = "~/js/jquery/plugins/jquery.fileupload-ui.js"
        ScriptManager1.CompositeScript.Scripts.Add(scriptReference)
        scriptReference = New ScriptReference
        scriptReference.Path = "~/js/cabecera.master.js"
        ScriptManager1.CompositeScript.Scripts.Add(scriptReference)
        scriptReference = New ScriptReference
        scriptReference.Path = ConfigurationManager.AppSettings("ruta") & "ckeditor/ckeditor.js"
        ScriptManager1.Scripts.Add(scriptReference)
        Dim pageName As String = System.IO.Path.GetFileName(System.Web.HttpContext.Current.Request.Url.AbsolutePath)
        Select Case pageName.ToLower
            Case "visorplantillasreunion.aspx"
                scriptReference = New ScriptReference
                scriptReference.Path = "~/App_Pages/GS/Reuniones/js/VisorPlantillasReunion_init.js"
                ScriptManager1.CompositeScript.Scripts.Add(scriptReference)
            Case "visorplantillas_gs.aspx"
                scriptReference = New ScriptReference
                scriptReference.Path = "~/App_Pages/GS/Reuniones/js/VisorPlantillas_GS_init.js"
                ScriptManager1.CompositeScript.Scripts.Add(scriptReference)
            Case "detalleperfil.aspx"
                scriptReference = New ScriptReference
                scriptReference.Path = "~/App_Pages/SG/js/DetallePerfil_init.js"
                ScriptManager1.CompositeScript.Scripts.Add(scriptReference)
            Case "cn_nuevomensaje_gs.aspx", "cn_murousuario.aspx"
                CargarRecursos()
                scriptReference = New ScriptReference
                scriptReference.Path = "~/App_Pages/CN/js/cn_funciones.js"
                ScriptManager1.CompositeScript.Scripts.Add(scriptReference)
                scriptReference = New ScriptReference
                scriptReference.Path = "~/js/jquery/plugins/autogrow.js"
                ScriptManager1.CompositeScript.Scripts.Add(scriptReference)
                scriptReference = New ScriptReference
                scriptReference.Path = "~/js/jquery/plugins/jquery.ui.datepicker.js"
                ScriptManager1.CompositeScript.Scripts.Add(scriptReference)
                scriptReference = New ScriptReference
                scriptReference.Path = "~/js/jquery/plugins/jquery.ui.datepicker." & CType(Me.Page, FSNServer.IFSNPage).Usuario.RefCultural & ".js"
                ScriptManager1.CompositeScript.Scripts.Add(scriptReference)
                scriptReference = New ScriptReference
                scriptReference.Path = "~/js/jquery/plugins/jquery.ui.maskedinput.js"
                ScriptManager1.CompositeScript.Scripts.Add(scriptReference)
                scriptReference = New ScriptReference
                scriptReference.Path = "~/js/jquery/plugins/jquery.ui.fsCombo.js"
                ScriptManager1.CompositeScript.Scripts.Add(scriptReference)
                scriptReference = New ScriptReference
                scriptReference.Path = "~/js/jquery/plugins/video.js"
                ScriptManager1.CompositeScript.Scripts.Add(scriptReference)
                scriptReference = New ScriptReference
                scriptReference.Path = "~/App_Pages/CN/js/cn_ui_nuevosMensajes.js"
                ScriptManager1.CompositeScript.Scripts.Add(scriptReference)
                scriptReference = New ScriptReference
                scriptReference.Path = "~/App_Pages/CN/js/cn_mensajes_ui.js"
                ScriptManager1.CompositeScript.Scripts.Add(scriptReference)
                If pageName.ToLower = "cn_murousuario.aspx" Then
                    scriptReference = New ScriptReference
                    scriptReference.Path = "~/App_Pages/CN/js/cn_menu.js"
                    ScriptManager1.CompositeScript.Scripts.Add(scriptReference)
                    scriptReference = New ScriptReference
                    scriptReference.Path = "~/App_Pages/CN/js/cn_mensajes.js"
                    ScriptManager1.CompositeScript.Scripts.Add(scriptReference)
                    scriptReference = New ScriptReference
                    scriptReference.Path = "~/App_Pages/CN/js/cn_Muro_init.js"
                    ScriptManager1.CompositeScript.Scripts.Add(scriptReference)
                    scriptReference = New ScriptReference
                    scriptReference.Path = "~/App_Pages/CN/js/cn_Muro_ui.js"
                    ScriptManager1.CompositeScript.Scripts.Add(scriptReference)
                    scriptReference = New ScriptReference
                    scriptReference.Path = "~/App_Pages/CN/js/cn_ui_meGusta.js"
                    ScriptManager1.CompositeScript.Scripts.Add(scriptReference)
                    scriptReference = New ScriptReference
                    scriptReference.Path = "~/App_Pages/CN/js/cn_ui_respuestas.js"
                    ScriptManager1.CompositeScript.Scripts.Add(scriptReference)
                    scriptReference = New ScriptReference
                    scriptReference.Path = "~/js/jquery/plugins/jquery.highlight.js"
                    ScriptManager1.CompositeScript.Scripts.Add(scriptReference)
                End If
            Case "cn_categorias.aspx"
                CargarRecursos()
                scriptReference = New ScriptReference
                scriptReference.Path = "~/js/jquery/plugins/jquery.ui.fsCombo.js"
                ScriptManager1.CompositeScript.Scripts.Add(scriptReference)
                scriptReference = New ScriptReference
                scriptReference.Path = "~/js/jquery/plugins/video.js"
                ScriptManager1.CompositeScript.Scripts.Add(scriptReference)
                scriptReference = New ScriptReference
                scriptReference.Path = "~/App_Pages/CN/js/cn_menu.js"
                ScriptManager1.CompositeScript.Scripts.Add(scriptReference)
                scriptReference = New ScriptReference
                scriptReference.Path = "~/App_Pages/CN/js/cn_Categorias_init.js"
                ScriptManager1.CompositeScript.Scripts.Add(scriptReference)
                scriptReference = New ScriptReference
                scriptReference.Path = "~/App_Pages/CN/js/cn_Categorias_ui.js"
                ScriptManager1.CompositeScript.Scripts.Add(scriptReference)
            Case "cn_userprofile.aspx"
                CargarRecursos()
                scriptReference = New ScriptReference
                scriptReference.Path = "~/js/jquery/plugins/jquery.ui.fsCombo.js"
                ScriptManager1.CompositeScript.Scripts.Add(scriptReference)
                scriptReference = New ScriptReference
                scriptReference.Path = "~/js/jquery/plugins/video.js"
                ScriptManager1.CompositeScript.Scripts.Add(scriptReference)
                scriptReference = New ScriptReference
                scriptReference.Path = "~/App_Pages/CN/js/cn_menu.js"
                ScriptManager1.CompositeScript.Scripts.Add(scriptReference)
                scriptReference = New ScriptReference
                scriptReference.Path = "~/App_Pages/CN/js/cn_UserProfile_init.js"
                ScriptManager1.CompositeScript.Scripts.Add(scriptReference)
                scriptReference = New ScriptReference
                scriptReference.Path = "~/App_Pages/CN/js/cn_UserProfile_ui.js"
                ScriptManager1.CompositeScript.Scripts.Add(scriptReference)
            Case "cn_grupos.aspx"
                CargarRecursos()
                scriptReference = New ScriptReference
                scriptReference.Path = "~/js/jquery/plugins/jquery.ui.fsCombo.js"
                ScriptManager1.CompositeScript.Scripts.Add(scriptReference)
                scriptReference = New ScriptReference
                scriptReference.Path = "~/js/jquery/plugins/video.js"
                ScriptManager1.CompositeScript.Scripts.Add(scriptReference)
                scriptReference = New ScriptReference
                scriptReference.Path = "~/App_Pages/CN/js/cn_menu.js"
                ScriptManager1.CompositeScript.Scripts.Add(scriptReference)
                scriptReference = New ScriptReference
                scriptReference.Path = "~/App_Pages/CN/js/cn_Grupos_init.js"
                ScriptManager1.CompositeScript.Scripts.Add(scriptReference)
                scriptReference = New ScriptReference
                scriptReference.Path = "~/App_Pages/CN/js/cn_Grupos_ui.js"
                ScriptManager1.CompositeScript.Scripts.Add(scriptReference)
            Case "traspasoadjudicaciones.aspx"
                CargarRecursos()
                scriptReference = New ScriptReference
                scriptReference.Path = "~/js/jquery/plugins/jquery.ui.fsCombo.js"
                ScriptManager1.CompositeScript.Scripts.Add(scriptReference)
                scriptReference = New ScriptReference
                scriptReference.Path = "~/js/jquery/plugins/jquery.ui.datepicker.js"
                ScriptManager1.CompositeScript.Scripts.Add(scriptReference)
                scriptReference = New ScriptReference
                scriptReference.Path = "~/js/jquery/plugins/jquery.ui.datepicker." & CType(Me.Page, FSNServer.IFSNPage).Usuario.RefCultural & ".js"
				ScriptManager1.CompositeScript.Scripts.Add(scriptReference)
				'scriptReference = New ScriptReference
				'scriptReference.Path = "~/App_Pages/GS/Adjudicaciones/js/TraspasoAdjudicaciones.js"
				'ScriptManager1.CompositeScript.Scripts.Add(scriptReference)
			Case "asignarunidadesorganizativas.aspx"
                ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/plugins/jstree.min.js"))
        End Select
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "rutas") Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaFS", _
                "<script>var rutaFS = '" & System.Configuration.ConfigurationManager.AppSettings("rutaFS") & "';" & _
                "var ruta = '" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "';</script>")
        End If
    End Sub
    Private Sub CargarRecursos()
        Dim pag As FSNServer.IFSNPage = CType(Me.Page, FSNServer.IFSNPage)
        Dim modulo As TiposDeDatos.ModulosIdiomas = pag.ModuloIdioma
        pag.ModuloIdioma = TiposDeDatos.ModulosIdiomas.Menu
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "TextosTiempoRelativo") Then
            Dim sVariableJavascriptTextosTiempoRelativo As String = "var TextosTiempoRelativo = new Array();"
            sVariableJavascriptTextosTiempoRelativo &= "TextosTiempoRelativo[0]='" & JSText(pag.Textos(46)) & "';"
            sVariableJavascriptTextosTiempoRelativo &= "TextosTiempoRelativo[1]='" & JSText(pag.Textos(47)) & "';"
            sVariableJavascriptTextosTiempoRelativo &= "var unidadesTiempo = {" & _
            "1:'" & JSText(pag.Textos(48)) & "'" & ",2:'" & JSText(pag.Textos(49)) & "'" & ",3:'" & JSText(pag.Textos(50)) & "'" & _
            ",4:'" & JSText(pag.Textos(51)) & "'" & ",5:'" & JSText(pag.Textos(52)) & "'" & ",6:'" & JSText(pag.Textos(53)) & "'" & _
            ",11:'" & JSText(pag.Textos(54)) & "'" & ",12:'" & JSText(pag.Textos(55)) & "'" & ",13:'" & JSText(pag.Textos(56)) & "'" & _
            ",14:'" & JSText(pag.Textos(57)) & "'" & ",15:'" & JSText(pag.Textos(58)) & "'" & ",16:'" & JSText(pag.Textos(59)) & "'" & "};"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "TextosTiempoRelativo", sVariableJavascriptTextosTiempoRelativo, True)
        End If
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "BloqueoPopUpsActivado") Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "BloqueoPopUpsActivado", "var sBloqueoPopupActivado='" & pag.Textos(72) & "';", True)
        End If
        pag.ModuloIdioma = TiposDeDatos.ModulosIdiomas.Colaboracion
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "TextosPantalla") Then
            Dim sVariableJavascriptTextos As String = "var Textos = new Array();"
            Dim sVariableJavascriptTextosCKEditor As String = "var TextosCKEditor = new Array();"

            For i As Integer = 0 To 73
                sVariableJavascriptTextos &= "Textos[" & i & "]='" & JSText(pag.Textos(i)) & "';"
                If i = 20 Then sVariableJavascriptTextosCKEditor &= "TextosCKEditor[1]='" & JSText(pag.Textos(i)) & "';"
                If i = 21 Then sVariableJavascriptTextosCKEditor &= "TextosCKEditor[2]='" & JSText(pag.Textos(i)) & "';"
            Next
            For i As Integer = 83 To 85
                sVariableJavascriptTextos &= "Textos[" & i - 9 & "]='" & JSText(pag.Textos(i)) & "';"
            Next
            For i As Integer = 74 To 89
                sVariableJavascriptTextosCKEditor &= "TextosCKEditor[" & i - 71 & "]='" & JSText(pag.Textos(i)) & "';"
            Next
            For i As Integer = 90 To 121
                sVariableJavascriptTextos &= "Textos[" & i - 13 & "]='" & JSText(pag.Textos(i)) & "';"
            Next
            For i As Integer = 142 To 143
                sVariableJavascriptTextos &= "Textos[" & i - 33 & "]='" & JSText(pag.Textos(i)) & "';"
            Next
            For i As Integer = 144 To 147
                sVariableJavascriptTextosCKEditor &= "TextosCKEditor[" & i - 125 & "]='" & JSText(pag.Textos(i)) & "';"
            Next
            For i As Integer = 148 To 149
                sVariableJavascriptTextos &= "Textos[" & i - 37 & "]='" & JSText(pag.Textos(i)) & "';"
            Next
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "TextosPantalla", sVariableJavascriptTextos, True)
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "TextosCKEditor", sVariableJavascriptTextosCKEditor, True)
        End If
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "DateFormatUsu") Then
            With pag.Usuario
                Dim sVariablesJavascript As String = "var UsuMask='" & .DateFormat.ShortDatePattern & "';"
                sVariablesJavascript = sVariablesJavascript & "var UsuMaskSeparator='" & .DateFormat.DateSeparator & "';"
                sVariablesJavascript &= "var UsuLanguageTag='" & .RefCultural & "';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "DateFormatUsu", sVariablesJavascript, True)
            End With
        End If
        pag.ModuloIdioma = modulo
    End Sub
End Class