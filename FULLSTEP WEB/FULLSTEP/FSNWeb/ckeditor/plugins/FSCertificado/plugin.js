﻿(function (editor) {
    var o = {
        exec: function (editor) {
            var selection = editor.getSelection();
            if (selection.getSelectedText() == '') {
                if ($('#popupBuscadorCertificado').length == 0) {
                    $.when($.get(rutaFS + 'CN/html/_PopUp_Buscador_Certificado.htm', function (popUp) {
                        popUp = popUp.replace(/src="/gi, 'src="' + ruta);
                        $('#popup').prepend(popUp);
                    })).done(function () {
                        $('#popupFondo').css('height', $(document).height());
                        $('#lblTituloPopUpBuscadorCertificado').text(TextosCKEditor[8]);
                        $('#lblBuscadorCertificadoTipo').text(TextosCKEditor[15]);
                        $('#lblBuscadorCertificadoProveedor').text(TextosCKEditor[10]);
                        $('#lblBuscadorCertificadoIdentificador').text(TextosCKEditor[16]);

                        var optionsBuscadorCertificadoTipo = { valueField: "value", textField: "text", dropDownImageURL: 'Flecha.png', itemListMaxHeight: 100, appendEmptyItem: true, autoComplete: true, itemListWidth: 250, WebMethodURL: rutaFS + 'CN/App_Services/CN.asmx/Obtener_BuscadorCert_Tipos' };
                        var optionsBuscadorCertificadoProveedor = { valueField: "value", textField: "text", isDropDown: false, MinimumPrefixLength: 1, disableParentEmptyValue: false, itemListMaxHeight: 100, appendEmptyItem: true, autoComplete: true, itemListWidth: 250, WebMethodURL: rutaFS + 'CN/App_Services/CN.asmx/Obtener_BuscadorCert_Proveedores' };
                        var optionsBuscadorCertificadoIdentificador = { valueField: "value", textField: "text", dropDownImageURL: 'Flecha.png', parentDependant: true, itemListMaxHeight: 100, appendEmptyItem: true, autoComplete: true, fsComboParents: 'cboBuscadorCertificadoTipo,cboBuscadorCertificadoProveedor', WebMethodURL: rutaFS + 'CN/App_Services/CN.asmx/Obtener_BuscadorCert_Identificador' };
                        $('#cboBuscadorCertificadoTipo').fsCombo(optionsBuscadorCertificadoTipo);
                        $('#cboBuscadorCertificadoProveedor').fsCombo(optionsBuscadorCertificadoProveedor);
                        $('#cboBuscadorCertificadoIdentificador').fsCombo(optionsBuscadorCertificadoIdentificador);
                        MostrarPopUp();
                    });
                } else {
                    $('#cboBuscadorCertificadoTipo').fsCombo('selectValue', '');
                    $('#cboBuscadorCertificadoProveedor').fsCombo('selectValue', '');
                    $('#cboBuscadorCertificadoIdentificador').fsCombo('selectValue', '');
                    $('#popupBuscadorCertificado').show();
                    MostrarPopUp();
                }
            } else {
                try {
                    var identificador = parseInt(selection.getSelectedText());
                    $.when($.ajax({
                        type: "POST",
                        url: rutaFS + 'CN/App_Services/CN.asmx/Comprobar_Buscador_Identificador',
                        data: JSON.stringify({ TipoEntidad: 2, Identificador: identificador }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        async: true
                    })).done(function (msg) {
                        if (msg.d) {
                            var item = {
                                identificador: msg.d.value,
                                texto: msg.d.text
                            }
                            var element = CKEDITOR.dom.element.createFromHtml('<a type="entidad" href="CE#' + item.identificador + '">' + $('#entidadCertificado').tmpl(item).html().replace(/src="/gi, 'src="' + ruta) + '</a>&nbsp;');
                            editor.insertElement(element);
                        } else { return false; }
                    });
                }
                catch (e) {
                    return false;
                }
            }
        },
        crearCodificacionCertificado: function (item) {
            var element = CKEDITOR.dom.element.createFromHtml('<a type="entidad" href="CE#' + item.identificador + '">' + $('#entidadCertificado').tmpl(item).html().replace(/src="/gi, 'src="' + ruta) + '</a>&nbsp;');
            this.editorCK.insertElement(element);
        },
        setEditor: function (editor) {
            this.editorCK = editor;
        }
    };
    function MostrarPopUp() {
        $('#btnCancelarBusquedaPopUp').attr('popup', 'BuscadorCertificado');
        CentrarPopUp($('#popup'));
        $('#popupFondo').css('z-index', 1003);
        $('#popupFondo').show();
        $('#popup').show();
    };
    CKEDITOR.plugins.add('FSCertificado', {
        init: function (editor) {
            editor.addCommand('FSCertificado', o);
            editor.ui.addButton('FSCertificado', {
                label: TextosCKEditor[8],
                icon: this.path + 'FSCertificado.png',
                command: 'FSCertificado'
            });
            o.setEditor(editor);
        },
        setSelectedValue: function (item) {
            o.crearCodificacionCertificado(item);
        }
    });
})();