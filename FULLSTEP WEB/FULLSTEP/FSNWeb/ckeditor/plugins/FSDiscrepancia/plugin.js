﻿(function (editor) {
    var o = {
        exec: function (editor) {
            var selection = editor.getSelection();
        },
        crearCodificacionDiscrepancia: function (item) {
            var element = CKEDITOR.dom.element.createFromHtml('<a type="entidad" href="FA#' + item.identificador + '"> ' + $('#entidadDiscrepancia').tmpl(item).html().replace(/src="/gi, 'src="' + ruta) + '</a>&nbsp;');
            this.editorCK.insertElement(element);
        },
        setEditor: function (editor) {
            this.editorCK = editor;
        }
    };
    function MostrarPopUp() {
        $('#btnCancelarBusquedaPopUp').attr('popup', 'BuscadorDiscrepancia');
        CentrarPopUp($('#popup'));
        $('#popupFondo').css('z-index', 1003);
        $('#popupFondo').show();
        $('#popup').show();
    };
    CKEDITOR.plugins.add('FSDiscrepancia', {
        init: function (editor) {
            editor.addCommand('FSDiscrepancia', o);
            editor.ui.addButton('FSDiscrepancia', {
                label: TextosCKEditor[26],
                icon: this.path + 'FSDiscrepancia.png',
                command: 'FSDiscrepancia'
            });
            o.setEditor(editor);
        },
        setSelectedValue: function (item) {
            o.crearCodificacionDiscrepancia(item);
        }
    });
})();