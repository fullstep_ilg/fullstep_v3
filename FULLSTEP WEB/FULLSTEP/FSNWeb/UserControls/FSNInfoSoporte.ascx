﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Fullstep.FSNWeb.UserControls_FSNInfoSoporte" Codebehind="FSNInfoSoporte.ascx.vb" %>
<div>
    <img id="imgTelefono" runat="server" src="~/images/telefono.jpg" alt="DSoporte T&eacute;cnico" border="0" style="float:right; right:0.5em; top:0.5em; margin-left:1em;"/>
    <asp:Label ID="lblSoporteTecnico" runat="server" Text="Soporte Técnico:" CssClass="Rotulo" style="display:block; margin:1em 0em;"></asp:Label> 
    <asp:Label ID="lblATC" runat="server" Text="Teléfono de atención al cliente:" CssClass="Etiqueta" style="display:block; margin-top:1em;"></asp:Label>
    <asp:Label ID="lblTelefono" runat="server" CssClass="EtiquetaGrande" style="display:block; margin-top:1em;" Text="+34 912752037"></asp:Label>
    <asp:HyperLink ID="lnkSoporte" runat="server" NavigateUrl="mailto:soportefullstep@fullstep.com" CssClass="Etiqueta Enlace" style="display:block; margin-top:0.5em;">soportefullstep@fullstep.com</asp:HyperLink>
</div>

