﻿Imports System.ComponentModel
Imports System.Drawing.Design
Imports System.Web.UI.Design
Imports Fullstep.FSNLibrary
Imports System.Web.UI.WebControls.WebParts

<Themeable(True)> _
Partial Class UserControls_FSNInfoSoporte
    Inherits System.Web.UI.UserControl
    Implements IWebPart, IWebEditable

    Private Class ControlEditor
        Inherits EditorPart

        Private _txtTitulo As TextBox
        Public TextoTitulo As String

        ''' <summary>
        ''' Lo llama el marco de trabajo de las páginas ASP.NET para indicar a los controles de servidor que utilizan la implementación basada en la composición que creen los controles secundarios que contengan como forma de preparar la devolución o representación de los datos.
        ''' </summary>
        Protected Overrides Sub CreateChildControls()
            Dim part As IWebPart = CType(WebPartToEdit, IWebPart)
            Controls.Clear()
            _txtTitulo = New TextBox()
            _txtTitulo.Width = Unit.Pixel(250)
            _txtTitulo.Text = part.Title
            Controls.Add(_txtTitulo)
        End Sub

        ''' <summary>
        ''' Presenta el contenido del control en el sistema de escritura especificado. Este método lo usan principalmente los programadores de controles. 
        ''' </summary>
        ''' <param name="writer">HtmlTextWriter que representa la secuencia de salida para presentar contenido HTML en el cliente.</param>
        Protected Overrides Sub RenderContents(ByVal writer As System.Web.UI.HtmlTextWriter)
            Dim part As IWebPart = CType(WebPartToEdit, IWebPart)
            writer.Write("<b>" & TextoTitulo & ":</b> ")
            _txtTitulo.RenderControl(writer)
            writer.WriteBreak()
        End Sub

        ''' <summary>
        ''' Guarda los valores de un control EditorPart en las propiedades correspondientes del control WebPart asociado.
        ''' </summary>
        ''' <returns>Es true si la acción de guardar los valores del control EditorPart en el control WebPart se realiza correctamente; en caso contrario (si se produce un error), es false.</returns>
        Public Overrides Function ApplyChanges() As Boolean
            Dim part As IWebPart = CType(WebPartToEdit, IWebPart)
            part.Title = _txtTitulo.Text
            Return True
        End Function

        ''' <summary>
        ''' Obtiene los valores de propiedad de un control WebPart correspondientes a su control EditorPart asociado.
        ''' </summary>
        Public Overrides Sub SyncChanges()
            Dim part As IWebPart = CType(WebPartToEdit, IWebPart)
            txtTitulo.Text = part.Title
        End Sub

        Private ReadOnly Property txtTitulo() As TextBox
            Get
                EnsureChildControls()
                Return _txtTitulo
            End Get
        End Property

    End Class

    Private _sTitle As String
    Private _sCatalogIconImageUrl As String
    Private _sDescription As String
    Private _sSubtitle As String
    Private _sTitleIconImageUrl As String
    Private _sTitleUrl As String
    Private _sEmail As String
    Private _sTelefono As String
    Private _sTextoTitulo As String

    <Browsable(True), Bindable(True), _
        Category("Behavior"), Description("Email de soporte."), _
        DefaultValue("soportefullstep@fullstep.com")> _
    Public Property Email() As String
        Get
            If String.IsNullOrEmpty(_sEmail) Then _
                CargarRecursos()
            Return _sEmail
        End Get
        Set(ByVal value As String)
            _sEmail = value
        End Set
    End Property

    <Browsable(True), Bindable(True), _
    Category("Behavior"), Description("Imagen"), _
    Editor(GetType(ImageUrlEditor), GetType(UITypeEditor)), UrlProperty(), _
    Themeable(True), DefaultValue("~/images/telefono.jpg")> _
    Property Imagen() As String
        Get
            Return imgTelefono.Src
        End Get
        Set(ByVal value As String)
            imgTelefono.Src = value
        End Set
    End Property

    <Browsable(True), Bindable(True), _
    Category("Behavior"), Description("Teléfono de soporte."), _
    DefaultValue("+34 912752037")> _
    Public Property Telefono() As String
        Get
            If String.IsNullOrEmpty(_sTelefono) Then _
                CargarRecursos()
            Return _sTelefono
        End Get
        Set(ByVal value As String)
            _sTelefono = value
        End Set
    End Property

    Public Property Title() As String Implements IWebPart.Title
        Get
            If Not DesignMode() And String.IsNullOrEmpty(_sTitle) Then _
                    CargarRecursos()
            Return _sTitle
        End Get
        Set(ByVal value As String)
            _sTitle = value
        End Set
    End Property

    Public Property CatalogIconImageUrl() As String Implements System.Web.UI.WebControls.WebParts.IWebPart.CatalogIconImageUrl
        Get
            Return _sCatalogIconImageUrl
        End Get
        Set(ByVal value As String)
            _sCatalogIconImageUrl = value
        End Set
    End Property

    Public Property Description() As String Implements System.Web.UI.WebControls.WebParts.IWebPart.Description
        Get
            Return _sDescription
        End Get
        Set(ByVal value As String)
            _sDescription = value
        End Set
    End Property

    Public ReadOnly Property Subtitle() As String Implements System.Web.UI.WebControls.WebParts.IWebPart.Subtitle
        Get
            Return _sSubtitle
        End Get
    End Property

    Public Property TitleIconImageUrl() As String Implements System.Web.UI.WebControls.WebParts.IWebPart.TitleIconImageUrl
        Get
            Return _sTitleIconImageUrl
        End Get
        Set(ByVal value As String)
            _sTitleIconImageUrl = value
        End Set
    End Property

    Public Property TitleUrl() As String Implements System.Web.UI.WebControls.WebParts.IWebPart.TitleUrl
        Get
            Return _sTitleUrl
        End Get
        Set(ByVal value As String)
            _sTitleUrl = value
        End Set
    End Property

    ''' <summary>
    ''' Carga los textos del control y restablece la propiedad ModuloIdioma de la página.
    ''' </summary>
    ''' <remarks>Llamadas desde: Page_Init</remarks>
    Private Sub CargarRecursos()
        If Not Me.Page Is Nothing Then
            Dim pag As FSNServer.IFSNPage
            pag = CType(Me.Page, FSNServer.IFSNPage)
            Dim modulo As TiposDeDatos.ModulosIdiomas = pag.ModuloIdioma
            pag.ModuloIdioma = TiposDeDatos.ModulosIdiomas.Webparts
            If Not lblSoporteTecnico Is Nothing Then _
                lblSoporteTecnico.Text = pag.Textos(0)
            If Not lblATC Is Nothing Then _
                lblATC.Text = pag.Textos(1)
            If String.IsNullOrEmpty(_sTitle) Then _sTitle = pag.Textos(0)
            If String.IsNullOrEmpty(_sTelefono) Then _sTelefono = pag.Textos(52)
            If String.IsNullOrEmpty(_sEmail) Then _sEmail = pag.Textos(53)
            If Not lblTelefono Is Nothing Then lblTelefono.Text = _sTelefono
            If Not lnkSoporte Is Nothing Then
                lnkSoporte.Text = _sEmail
                lnkSoporte.NavigateUrl = "mailto:" & _sEmail
            End If
            If Not imgTelefono Is Nothing Then _
                imgTelefono.Alt = pag.Textos(0)
            _sTextoTitulo = pag.Textos(19)
            pag.ModuloIdioma = modulo
        Else
            Dim txt As New FSNServer.Dictionary()
            For Each x As String In HttpContext.Current.Session.Keys
                If TypeOf HttpContext.Current.Session(x) Is FSNServer.User Then
                    Dim Usuario As FSNServer.User = CType(HttpContext.Current.Session(x), FSNServer.User)
                    If String.IsNullOrEmpty(_sTitle) Then _
                        _sTitle = txt.Texto(TiposDeDatos.ModulosIdiomas.Webparts, 1, Usuario.Idioma)
                    If String.IsNullOrEmpty(_sTelefono) Then _
                        _sTelefono = txt.Texto(TiposDeDatos.ModulosIdiomas.Webparts, 53, Usuario.Idioma)
                    If String.IsNullOrEmpty(_sEmail) Then _
                        _sEmail = txt.Texto(TiposDeDatos.ModulosIdiomas.Webparts, 54, Usuario.Idioma)
                    Exit For
                    _sTextoTitulo = txt.Texto(TiposDeDatos.ModulosIdiomas.Webparts, 19, Usuario.Idioma)
                End If
            Next
        End If
    End Sub

    ''' <summary>
    ''' Llama al método de carga de textos.
    ''' </summary>
    ''' <param name="sender">Origen del evento.</param>
    ''' <param name="e">Clase EventArgs que no contiene datos de evento.</param>
    ''' <remarks>Tiene lugar al inicializar el control de servidor, que es el primer paso en su ciclo de vida.</remarks>
    Private Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        CargarRecursos()
    End Sub

    ''' <summary>
    ''' Devuelve una colección de controles EditorPart personalizados que pueden utilizarse para editar un control WebPart cuando se encuentra en modo de edición.
    ''' </summary>
    ''' <returns>Una colección EditorPartCollection que contiene controles EditorPart personalizados asociados a un control WebPart.</returns>
    Public Function CreateEditorParts() As System.Web.UI.WebControls.WebParts.EditorPartCollection Implements System.Web.UI.WebControls.WebParts.IWebEditable.CreateEditorParts
        Dim editorArray As New ArrayList()
        Dim edPart As New ControlEditor()
        edPart.ID = Me.ID & "_editor1"
        edPart.TextoTitulo = _sTextoTitulo
        editorArray.Add(edPart)
        Dim editorParts As New EditorPartCollection(editorArray)
        Return editorParts
    End Function

    Public ReadOnly Property WebBrowsableObject() As Object Implements System.Web.UI.WebControls.WebParts.IWebEditable.WebBrowsableObject
        Get
            Return Me
        End Get
    End Property
End Class
