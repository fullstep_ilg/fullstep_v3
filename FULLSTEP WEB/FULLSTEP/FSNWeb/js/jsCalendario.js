﻿///////////////////////////////////////////////////////////////////////////////////////////////
//FUNCIONES PARA COLOCAR CORRECTAMENTE EL CALENDARIO DEL PANEL DE PARTIDAS PRESUPUESTARIAS
///////////////////////////////////////////////////////////////////////////////////////////////

//Devuelve el ancho de la ventana del navegador. Calcula tb el alto y podría devolverlo caso de ser necesario.
function WindowSize() {
    var myWidth = 0, myHeight = 0;
    if (typeof (window.innerWidth) == 'number') {
        //Non-IE
        myWidth = window.innerWidth;
        myHeight = window.innerHeight;
    } else if (document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight)) {
        //IE 6+ in 'standards compliant mode'
        myWidth = document.documentElement.clientWidth;
        myHeight = document.documentElement.clientHeight;
    } else if (document.body && (document.body.clientWidth || document.body.clientHeight)) {
        //IE 4 compatible
        myWidth = document.body.clientWidth;
        myHeight = document.body.clientHeight;
    }
    return myWidth;
}

//Devuelve el left a usar en función del navegador, del ancho de pantalla y del control
function izquierda(controlFecha) {
    //var panelPartidasLeft = parseFloat($get('ctl00_ctl00_ctl00_CPH1_cPpl_Ppl_SMPartidasPresupuestarias').style.left.replace('px', ''));
    //var panelPartidasTop = parseFloat($get('ctl00_ctl00_ctl00_CPH1_cPpl_Ppl_SMPartidasPresupuestarias').style.top.replace('px', ''));
    var windowWidth = WindowSize();
    var izq;
    if (typeof (window.innerWidth) == 'number') {
        //Non-IE
        if (windowWidth > 1024) {
            if (controlFecha.match('dteFechaInicioDesde') != null) {
                izq = '408px';
            }
            if (controlFecha.match('dteFechaHastaFin') != null) {
                izq = '692px';
            }
        }
        else {
            if (controlFecha.match('dteFechaInicioDesde') != null) {
                izq = '280px';
            }
            if (controlFecha.match('dteFechaHastaFin') != null) {
                izq = '564px';
            }
        }
    } else if (document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight)) {
        //IE 6+ in 'standards compliant mode'
        if (windowWidth > 1024) {
            if (controlFecha.match('dteFechaInicioDesde') != null) {
                //izq = (280 / windowWidth) + 'px';
                izq = '303px';
            }
            if (controlFecha.match('dteFechaHastaFin') != null) {
                //izq = (573 / windowWidth) + 'px';
                izq = '723px';
            }
        }
        else {
            if (controlFecha.match('dteFechaInicioDesde') != null) {
                //izq = (303 / windowWidth) + 'px';
                izq = '241px';
            }
            if (controlFecha.match('dteFechaHastaFin') != null) {
                //izq = (723 / windowWidth) + 'px';
                izq = '573px';
            }
        }
    } else if (document.body && (document.body.clientWidth || document.body.clientHeight)) {
        //IE 4 compatible
        if (windowWidth > 1024) {
            if (controlFecha.match('dteFechaInicioDesde') != null) {
                //izq = (280 / windowWidth) + 'px';
                izq = '303px';
            }
            if (controlFecha.match('dteFechaHastaFin') != null) {
                //izq = (573 / windowWidth) + 'px';
                izq = '723px';
            }
        }
        else {
            if (controlFecha.match('dteFechaInicioDesde') != null) {
                //izq = (303 / windowWidth) + 'px';
                izq = '241px';
            }
            if (controlFecha.match('dteFechaHastaFin') != null) {
                //izq = (723 / windowWidth) + 'px';
                izq = '573px';
            }
        }
    }
    return izq;
}

///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////

