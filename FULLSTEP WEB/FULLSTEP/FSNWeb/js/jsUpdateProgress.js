﻿Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(beginReq);
Sys.WebForms.PageRequestManager.getInstance().add_endRequest(endReq);
var Emisor, EmisorActual;
function endReq(sender, args) {
    var OcultarCargando = true;
    var Comparacion;
    Comparacion = /CmbBoxDest/i;
    if (String(EmisorActual).search(Comparacion) != -1) {
        $("[id$=BtnAcepDest]")[0].style.display = '' 
        $("[id$=BtnCancDest]")[0].style.display = ''
    }
    Comparacion = /btnFiltrarPedAbierto/i;
    if (String(Emisor).search(Comparacion) != -1) OcultarCargando = false;
    //  hides the Popup
    if ($find(ModalProgress) && OcultarCargando)
        $find(ModalProgress).hide();
}


function beginReq(sender, args) {
    // shows the Popup
    var Comparacion;
    var Mostrar = 'si';
    Emisor = args._postBackElement.id;
    
    //EP----------------------------------------------------------------
    Comparacion = /BtnAnyadircesta/i;
    if (String(Emisor).search(Comparacion) != -1) Mostrar = 'no';
    Comparacion = /ExFiltrAv/i;
    if (String(Emisor).search(Comparacion) != -1) Mostrar = 'no';
    Comparacion = /Label18/i;
    if (String(Emisor).search(Comparacion) != -1) Mostrar = 'no';
    Comparacion = /ExDestinos/i;
    if (String(Emisor).search(Comparacion) != -1) Mostrar = 'no';
    Comparacion = /ExProveedores/i;
    if (String(Emisor).search(Comparacion) != -1) Mostrar = 'no';
    Comparacion = /ImgComparativa/i;
    if (String(Emisor).search(Comparacion) != -1) Mostrar = 'no';
    Comparacion = /LnkBtnCategoria/i;
    if (String(Emisor).search(Comparacion) != -1) Mostrar = 'no';
    Comparacion = /LnkBtnProves/i;
    if (String(Emisor).search(Comparacion) != -1) Mostrar = 'no';
    Comparacion = /LnkBtnDests/i;
    if (String(Emisor).search(Comparacion) != -1) Mostrar = 'no';
    Comparacion = /LnkBtnFiltAv/i;
    if (String(Emisor).search(Comparacion) != -1) Mostrar = 'no';
    Comparacion = /EmiPedLnkBtnOcultarPanel/i;
    if (String(Emisor).search(Comparacion) != -1) Mostrar = 'no';
    Comparacion = /imgbtnLineas/i;
    if (String(Emisor).search(Comparacion) != -1) Mostrar = 'no';
    Comparacion = /LblComenAdjunPed/i;
    if (String(Emisor).search(Comparacion) != -1) Mostrar = 'no';
    Comparacion = /CmbBoxDest/i;
    if (String(Emisor).search(Comparacion) != -1) {
        $("[id$=BtnAcepDest]")[0].style.display = 'none' //Cuando se cambia de destino ocultamos los botones hasta que se realiza la actualizacion
        $("[id$=BtnCancDest]")[0].style.display = 'none'
        EmisorActual = Emisor
        Mostrar = 'no';
    }
    Comparacion = /btnIncEspecLinea/i;
    if (String(Emisor).search(Comparacion) != -1) Mostrar = 'no';
    Comparacion = /btnAdjuntoCatalogo/i;
    if (String(Emisor).search(Comparacion) != -1) Mostrar = 'no';
    Comparacion = /btnEliminarAdjuntoLinea/i;
    if (String(Emisor).search(Comparacion) != -1) Mostrar = 'no';
    Comparacion = /TxtBxCampo2Valor/i;
    if (String(Emisor).search(Comparacion) != -1) Mostrar = 'no';
    Comparacion = /BtnAceptObs/i;
    if (String(Emisor).search(Comparacion) != -1) Mostrar = 'no';
    Comparacion = /ImgBtnCerrarPanelObsAdjun/i;
    if (String(Emisor).search(Comparacion) != -1) Mostrar = 'no';
    Comparacion = /ImgBtnCerrarPanelObsAdjun2/i;
    if (String(Emisor).search(Comparacion) != -1) Mostrar = 'no';
    Comparacion = /BtnCancelarAdjunPed/i;
    if (String(Emisor).search(Comparacion) != -1) Mostrar = 'no';
    Comparacion = /BtnCancelarAdjunArt/i;
    if (String(Emisor).search(Comparacion) != -1) Mostrar = 'no';
    Comparacion = /BtnCancelarAnyadido/i;
    if (String(Emisor).search(Comparacion) != -1) Mostrar = 'no';
    Comparacion = /btnAnyaCancelar/i;
    if (String(Emisor).search(Comparacion) != -1) Mostrar = 'no';
    Comparacion = /BtnAceptarPedidoAnyadidoOk/i;
    if (String(Emisor).search(Comparacion) != -1) Mostrar = 'no';
    Comparacion = /BtnAnyaAceptarPedidoAnyadidoOk/i;
    if (String(Emisor).search(Comparacion) != -1) Mostrar = 'no';
    Comparacion = /BtnCancelarAdjunArt2/i;
    if (String(Emisor).search(Comparacion) != -1) Mostrar = 'no';
    Comparacion = /ImgBtnCerrarpnlAdjuntos/i;
    if (String(Emisor).search(Comparacion) != -1) Mostrar = 'no';
    Comparacion = /BtnCancelarFav/i;
    if (String(Emisor).search(Comparacion) != -1) Mostrar = 'no';
    Comparacion = /BtnAceptarEmisionOkOMal/i;
    if (String(Emisor).search(Comparacion) != -1) Mostrar = 'no';
    Comparacion = /CmbBoxPaiProveBusq/i;
    if (String(Emisor).search(Comparacion) != -1) Mostrar = 'no';
    Comparacion = /CmbBoxProviProveBusq/i;
    if (String(Emisor).search(Comparacion) != -1) Mostrar = 'no';
    Comparacion = /wnumCantidad/i;
    if (String(Emisor).search(Comparacion) != -1) Mostrar = 'no';
    Comparacion = /WNumEdPrecArt/i;
    if (String(Emisor).search(Comparacion) != -1) Mostrar = 'no';
    Comparacion = /WddRecep/i;
    if (String(Emisor).search(Comparacion) != -1) Mostrar = 'no';
   //----------------------------------------------------------------------------------------------------------
   //PMweb2008
   if (String(Emisor).search("ugtxtOrganizacionCompras") != -1) Mostrar = 'no';
   //PMweb2008 common
    if (String(args._postBackElement.id).search("cblCamposGenerales") != -1) Mostrar = 'no';
    if (String(args._postBackElement.id).search("tvCamposFiltro") != -1) Mostrar = 'no';
    if (String(args._postBackElement.id).search("rpCamposGenerales") != -1) Mostrar = 'no';
    if (String(args._postBackElement.id).search("rpCamposFormulario") != -1) Mostrar = 'no';
    if (String(args._postBackElement.id).search("uwgConfigurarFiltros") != -1) Mostrar = 'no';
    if (String(args._postBackElement.id).search("ddlAnyo") != -1) Mostrar = 'no';
    if (String(args._postBackElement.id).search("uwgContratos") != -1)
    if (ColCheck == 1) {
           Mostrar = 'no';
           ColCheck = 0;
    }

    //PMWeb2008 visor
    if (String(args._postBackElement.id).search("cblCamposGenerales") != -1) Mostrar = 'no';
    if (String(args._postBackElement.id).search("tvCamposFiltro") != -1) Mostrar = 'no';
    if (String(args._postBackElement.id).search("uwgConfigurarFiltros") != -1) Mostrar = 'no';
    if (String(args._postBackElement.id).search("btnOrder") != -1) Mostrar = 'no';
    if (String(args._postBackElement.id).search("btnRecargaCache") != -1) Mostrar = 'no';

    //visor de integración
    if (String(args._postBackElement.id).search("ddEntidad") != -1) Mostrar = 'no';
    if (String(args._postBackElement.id).search("ddERP") != -1) Mostrar = 'no';
    if (String(args._postBackElement.id).search("ddAnyoProce") != -1) Mostrar = 'no';
    if (String(args._postBackElement.id).search("ddGmnProce") != -1) Mostrar = 'no';
    if (String(args._postBackElement.id).search("ddCodProce") != -1) Mostrar = 'no';
    //Buscador de Articulos
    if (String(args._postBackElement.id).search("chkAnyadirAutoFav") != -1) Mostrar = 'no';
    //Buscador de Proveedores
    if (String(args._postBackElement.id).search("chkAnyadirProvFav") != -1) Mostrar = 'no';

    if (Mostrar == 'si') {
        if ($find(ModalProgress))
            $find(ModalProgress).show();
    }   
}
