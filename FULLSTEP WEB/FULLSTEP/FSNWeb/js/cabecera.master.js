﻿var CKEDITOR_BASEPATH = ruta + 'ckeditor/';
var usuario;
$.ajaxSetup({
    error: function (error) {
        var errorGuardarError = "at Fullstep.FSNWeb.Error_Handler.Guardar_Error";
        var textoError = error.responseText;
        $('body').toggleClass("wait"); $('body').css('cursor', 'auto');
        if (textoError.indexOf(errorGuardarError) == -1) {
            $.when($.ajax({
                type: "POST",
                url: rutaFS + '_Common/App_Services/Error_Handler.asmx/Guardar_Error',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({ App_Error: $.parseJSON(error.responseText) }),
                async: false
            })).done(function (msg) {
                if (typeof (OcultarCargando) !== 'undefined' && $.isFunction(OcultarCargando)) OcultarCargando();
                var textos = msg.d;
                if ($('#popupErrorAlert').length == 0) {
                    $.when($.get(rutaFS + '_Common/Errores/html/errorAlert.htm', function (errorPopUp) {
                        errorPopUp = errorPopUp.replace(/src="/gi, 'src="' + ruta);
                        $('body').append(errorPopUp);
                    })).done(function () {
                        MostrarError(textos);
                    });
                } else {
                    MostrarError(textos);
                }
            });
        }
    }
});
function MostrarError(textos) {
    $('#popupFondo').css('height', $(document).height());
    $('#popupFondo').show();
    $.each(textos, function (key, value) {
        $('#' + key).html(value);
    });
    CentrarPopUp($('#popupErrorAlert'));
    $('#popupErrorAlert').show();
}
$('#btnAceptarErrorAlert').live('click', function () {
    $('#popupErrorAlert').hide();
    $('#popupFondo').hide();
});
function trim(sValor) {
    var re = / /;
    var i;
    var sTmp = sValor;

    while (sTmp.search(re) == 0)
        sTmp = sTmp.replace(re, "")

    var sRev = "";
    for (i = 0; i < sTmp.length; i++)
        sRev = sTmp.charAt(i) + sRev

    while (sRev.search(re) == 0)
        sRev = sRev.replace(re, "")

    sTmp = "";
    for (i = 0; i < sRev.length; i++)
        sTmp = sRev.charAt(i) + sTmp

    return (sTmp);
};

var XD = function () {
    var interval_id,
    last_hash,
    cache_bust = 1,
    attached_callback,
    window = this;

    return {
        postMessage: function (message, target_url, target) {
            if (!target_url) return;

            target = target || parent;  // default to parent

            if (window['postMessage']) {
                // the browser supports window.postMessage, so call it with a targetOrigin
                // set appropriately, based on the target_url parameter.
                target['postMessage'](message, target_url.replace(/([^:]+:\/\/[^\/]+).*/, '$1'));

            } else if (target_url) {
                // the browser does not support window.postMessage, so set the location
                // of the target to target_url#message. A bit ugly, but it works! A cache
                // bust parameter is added to ensure that repeat messages trigger the callback.
                target.location = target_url.replace(/#.*$/, '') + '#' + (+new Date) + (cache_bust++) + '&' + message;
            }
        }
    };
}();

var windowOpen = window.open;
//override de window open para que el popup se abra centrado
//y advierta si está activado el bloqueo de popup
window.open = function (url, name, features) {
    if (typeof(autenticacionIntegradaFrame)!='undefined' && autenticacionIntegradaFrame && name=='_top') {
        XD.postMessage(url, parent_url, parent.parent);
    } else {
        var params = new Array();
        //extraer especificaciones
        if (features) {
            var specArr = features.split(",");
            for (i = 0; i < specArr.length; i++) {
                var spec = specArr[i];
                var key = trim(spec.split("=")[0]);
                var value = trim(spec.split("=")[1]);
                params[key] = value;
            }
            if ((params["width"] || params["height"])) {
                if (params["left"] == undefined) params["left"] = (screen.width - (params["width"] == undefined ? (screen.width / 2) : params["width"])) / 2;
                if (params["top"] == undefined) params["top"] = (screen.height - (params["height"] == undefined ? (screen.height / 2) : params["height"])) / 2;
            }
        }
        features = "";
        for (i in params) {
            features += i + "=" + params[i] + ",";
        }
        features = features.substring(0, features.length - 1);
        var popUp = windowOpen(url, name, features);
        //Tarea 3301 : Si el bloqueo de popups está activado mostramos una alerta ya que no se van a poder abrir
        if (popUp == null || typeof (popUp) == 'undefined') {
            if (navigator.userAgent.toLowerCase().indexOf('safari/') > -1)
                alert(sBloqueoPopupActivado);
            return false;
        } else {
            popUp.focus();
        };

        return popUp;
    }
};
$(document).ready(function () {
    if (typeof (usuario) == 'undefined') {
        $.when($.ajax({
            type: "POST",
            url: rutaFS + '_Common/App_Services/User.asmx/Get_Logged_User',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false
        })).done(function (msg) {
            usuario = msg.d;
            if (usuario != null && usuario.AccesoCN) Cargar_Opciones_CN();
        });
    } else {
        if (usuario.AccesoCN) Cargar_Opciones_CN();
    };
    $('#lnkNotificacionesCN').live('click', function () {
        window.location.href = rutaFS + 'CN/cn_MuroUsuario.aspx';
    });  
});
function Cargar_Opciones_CN() {
    if (typeof (EnBlancoMaster) !== 'undefined' && EnBlancoMaster) return false;
    $("#formFileupload").attr("action", rutaFS + 'cn/FileTransferHandler.ashx');
    if (usuario.CNPermisoCrearMensajes) {
        $('#lnkNuevoMensajeMaster').show();
    };
    VideoJS.setupAllWhenReady();
    VerificarNotificacionesCN(false);
    if (segundosNotificacionesCN !== 0) {
        setInterval(function () { VerificarNotificacionesCN(true) }, segundosNotificacionesCN);
    };

    $.when($.get(rutaFS + 'CN/html/_adjuntos.tmpl.htm', function (templates) {
        $('body').append(templates);
    })).done(function () {
        $('#fileupload').fileupload();
        $('#fileupload').show();
        $('#fileupload .fileinput-button').css('right', '');
        $('#fileupload .fileinput-button').css('left', -10000);
        $('#fileupload .fileinput-button').css('top', -10000);
        $('#fileupload .fileinput-button').live('mouseleave', function () {
            $('#fileupload .fileinput-button').css('left', -10000);
            $('#fileupload .fileinput-button').css('top', -10000);
            $('.Seleccionable').removeClass('Seleccionable');
        });
    });

    $('#lnkNuevoMensajeMaster').live('click', function () {
        MostrarFormularioNuevoMensaje(false, true);
        var heightContenido = $('#Contenido').height();
        $('#popupFondo').css('height', $(document).height());
        $('#popupFondo').show();
        $('#Contenido').css('height', heightContenido);
        if ($('#popupNuevoMensaje').length == 0) {
            $.get(rutaFS + 'CN/html/_nuevo_mensaje.htm', function (menuNuevoMensaje) {
                $('body').prepend('<div id="popupNuevoMensaje" class="popupCN" style="display:none; position:absolute; z-index:1002; width:85%; max-height:80%; text-align:left; padding:5px; overflow:auto;"></div>');
                menuNuevoMensaje = menuNuevoMensaje.replace(/src="/gi, 'src="' + ruta);
                $('#popupNuevoMensaje').prepend(menuNuevoMensaje);
                $.each($('#popupNuevoMensaje [id]'), function () {
                    $(this).attr('id', $(this).attr('id') + 'Master');
                });
                var cabecera = '<div class="FondoHeader" style="float:left; margin-bottom:5px;"><img src="' + ruta + 'images/ColaboracionHeader.png" style="float:left; margin-left:10px; max-height:25px;" />';
                cabecera = cabecera + '<span id="lblTituloPopUpNuevoMensaje" class="LabelTituloCabeceraCustomControl" style="margin-left:10px; line-height:25px;"></span></div>';
                $('#popupNuevoMensaje').prepend(cabecera);
                EstablecerTextosMenuMensajeNuevo();
                $('#txtNuevoMensajeTituloMaster').autoGrow();
                $('#txtEventoFechaMaster').datepicker({
                    showOn: 'both',
                    buttonImage: ruta + 'images/colorcalendar.png',
                    buttonImageOnly: true,
                    buttonText: '',
                    dateFormat: UsuMask.replace('MM', 'mm').replace('yyyy', 'yy'),
                    showAnim: 'slideDown'
                });
                $.datepicker.setDefaults($.datepicker.regional[usuario.RefCultural]);
                if (usuario.CNPermisoCrearMensajesUrgentes) $('#NuevoMensajeUrgenteMaster').show();
                MostrarFormularioNuevoMensaje(true, true, 'NuevoTemaMaster', 0, false);

                CentrarPopUpAncho($('#popupNuevoMensaje'));
                $('#popupNuevoMensaje').css('top', 150);
            });
        } else {
            MostrarFormularioNuevoMensaje(true, true, 'NuevoTemaMaster', 0, false);
            CentrarPopUpAncho($('#popupNuevoMensaje'));
            $('#popupNuevoMensaje').css('top', 150);
        };
    });
};
function VerificarNotificacionesCN(mostrarDivNotificaciones) {
    $.when($.ajax({
        type: "POST",
        url: rutaFS + 'CN/App_Services/CN.asmx/Comprobar_Notificaciones',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true
    })).done(function (msg) {
        var Notificaciones = 0;
        $.each(msg.d, function () {
            switch (this.value) {
                case "0":
                    Notificaciones = parseInt(this.text);
                    break;
                case "5":
                    if (!$('#popupMensajeUrgente').is(':visible')) {
                        var idMensaje = parseInt(this.text);
                        $.get(rutaFS + 'CN/html/_mensajes.tmpl.htm', function (mensajes) {
                            $('body').append(mensajes);
                            ObtenerMensajeUrgente(idMensaje);
                        });
                    }
                    break;
                default:
                    break;
            }
        });
        if (Notificaciones > 0) {
            $('#NumeroAlertNotificacionCN').text(Notificaciones);
            $('#lnkNotificacionesCN').show();
            if ($('#divMuro').length > 0 && mostrarDivNotificaciones) {
                $('#divPanelNotificaciones span').text(Textos[82].replace('###', Notificaciones));
                $('#divNotificaciones').show();
            }
        } else {
            $('#lnkNotificacionesCN').hide();
        }
    });
};
function ObtenerMensajeUrgente(idMensaje) {
    var timeoffset = new Date().getTimezoneOffset();
    $.when($.ajax({
        type: "POST",
        url: rutaFS + 'CN/App_Services/CN.asmx/Obtener_Mensaje',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({ idMensaje: idMensaje, timezoneoffset: timeoffset }),
        dataType: "json",
        async: true
    })).done(function (msg) {
        if ($('#popupMensajeUrgente').length == 0) {
            $.get(rutaFS + 'cn/html/_PopUp_MensajeUrgente.htm', function (divmensaje) {
                $('body').append(divmensaje);
                CargarMensajeUrgente(msg);
                $('#btnAceptarPopUpMensajeUrgente').live('click', function () {
                    $('#popupFondo').hide();
                    $('#popupMensajeUrgente').hide();
                    MensajeUrgenteLeido($(this).attr('IdMensaje'));
                });
            });
        } else {
            CargarMensajeUrgente(msg);
        }
    });
};
function CargarMensajeUrgente(msg) {
    var cn = { posts: [] };
    var mensaje = msg.d;

    $('#btnAceptarPopUpMensajeUrgente').attr('IdMensaje', mensaje.IdMensaje);

    mensaje.FechaAlta = eval("new " + mensaje.FechaAlta.slice(1, -1));
    mensaje.FechaActualizacion = eval("new " + mensaje.FechaActualizacion.slice(1, -1));

    mensaje.FechaAltaRelativa = relativeTime(mensaje.FechaAlta);
    mensaje.FechaActualizacionRelativa = relativeTime(mensaje.FechaActualizacion);

    if (mensaje.Respuestas) {
        $.each(mensaje.Respuestas, function () {
            this.FechaAlta = eval("new " + this.FechaAlta.slice(1, -1));
            this.FechaActualizacion = eval("new " + this.FechaActualizacion.slice(1, -1));

            this.FechaAltaRelativa = relativeTime(this.FechaAlta);
            this.FechaActualizacionRelativa = relativeTime(this.FechaActualizacion);
        });
    }
    cn.posts.push(mensaje);

    $('#divMensajeUrgente').empty();
    $('#divMensajeUrgente').css('max-height', '');
    var ul = $('<ul>').appendTo($('#divMensajeUrgente'));
    $('#itemMensaje').tmpl(cn.posts).appendTo(ul);

    var heightContenido = $('#Contenido').height();
    CentrarPopUpAncho($('#popupMensajeUrgente'));
    $('#popupMensajeUrgente').css('top', 150);
    $('#popupFondo').css('height', $(document).height());
    $('#popupFondo').show();
    $('#Contenido').css('height', heightContenido);

    $('#imgMensajeUrgente').attr('src', ruta + 'images/ColaboracionHeader.png');
    $('#divMensajeUrgente [id^=divOpcionesMensaje_]').hide();
    $('#divNuevaRespuestaDefault_' + mensaje.IdMensaje).hide();

    $('#btnAceptarPopUpMensajeUrgente').text(Textos[20]);
    $('#divMensajeUrgente [id^=msgDescargar__]').text(Textos[79]);

    $('#popupMensajeUrgente').show();
    $('#divMensajeUrgente').css('max-height', $('#popupMensajeUrgente').height() - 85);
    $('#lblTituloMensajeUrgente').text(Textos[0] + '. ' + Textos[19].toUpperCase());
    /*====================================
	=========== TEMPLATES INFO ME GUSTA
	=====================================*/
    $('[id^=msgTmplMeGusta_]').text(Textos[85]);
    $.each($('[id^=msgTmplLeGusta_]'), function () {
        $(this).text(Textos[86].replace('###', $(this).text()));
    });
    $.each($('[id^=msgTmplLeGustaMeGusta_]'), function () {
        $(this).text(Textos[87].replace('###', $(this).text()));
    });
    $.each($('[id^=msgTmplMeGustaLesGusta_]'), function () {
        $(this).html(Textos[88].replace('@@@', $(this).html().replace('$$$', Textos[91])));
    });
    $.each($('[id^=msgTmplLeGustaMeGustaLesGusta_]'), function () {
        $(this).html(Textos[89].replace('@@@', $(this).html().replace($(this).text().replace($(this).children('span').text(), ''), '').replace('$$$', Textos[91])).replace('###', $(this).text().replace($(this).children('span').text(), '')));
    });
    $.each($('[id^=msgTmplLesGusta_]'), function () {
        $(this).html(Textos[90].replace('@@@', $(this).html().replace($(this).text().replace($(this).children('span').text(), ''), '').replace('$$$', Textos[91])).replace('###', $(this).text().replace($(this).children('span').text(), '')));
    });
    /*====================================
	=========== FIN TEMPLATES INFO ME GUSTA
	=====================================*/
    $('[id^=msgAdjuntoRespuesta_] span').text(Textos[28]);
    $('[id^=msgMeGusta_]').text(Textos[8]);
    $('[id^=msgResponder_]').text(Textos[77]);
    $('[id^=msgDestinatarios_]').text(Textos[78]);
    $('[id^=msgEditar_]').text(Textos[83]);
    $('[id^=msgNuevaRespuestaDefault_]').val(Textos[84]);
    $('[id^=msgDescargar__]').text(Textos[79]);
    $('[id^=btnResponder_]').text(Textos[20]);
    $('[id^=btnResponder_]').attr('title', Textos[20]);
    $('[id^=btnCancelarResponder_]').text(Textos[21]);
    $('[id^=btnCancelarResponder_]').attr('title', Textos[21]);
    $('[id^=msgNoLeido]').text(Textos[109]);
    $('[id^=msgInfoCategoria]').text(Textos[110]);
    $.each($('[id^=VerRespuestas_]'), function () {
        $(this).text(Textos[92].replace('###', $(this).text()) + '>>');
    });
};
function MensajeUrgenteLeido(idMensaje) {
    var timeoffset = new Date().getTimezoneOffset();
    $.when($.ajax({
        type: "POST",
        url: rutaFS + 'CN/App_Services/CN.asmx/MensajeUrgente_Leido',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({ idMensaje: idMensaje, timezoneoffset: timeoffset }),
        dataType: "json",
        async: true
    })).done(function (msg) {
        $('#btnAceptarPopUpMensajeUrgente').removeAttr('IdMensaje');
        if (msg && msg.d) {
            CargarMensajeUrgente(msg);
        }
    });
};
function validarsolonum(txt) {
    var str = '';
    for (var i = 0; i < txt.value.length; i++) {
        if (txt.value.charCodeAt(i) >= 48 && txt.value.charCodeAt(i) <= 57)
            str += txt.value.charAt(i);
    }
    txt.value = str;
};
function validarkeynum(event) {
    var evt;
    if (window.event) {// IE
        evt = window.event;
        keynum = evt.keyCode;
    } else {
        evt = event;
        keynum = evt.which;
    }
    if (keynum < 48 || keynum > 57) {
        return false;
    }
};
function validarpastenum() {
    var texto = window.clipboardData.getData('Text');
    var str = '';
    for (var i = 0; i < texto.length; i++) {
        if (texto.charCodeAt(i) >= 48 && texto.charCodeAt(i) <= 57)
            str += texto.charAt(i);
    }
    if (texto != str) event.returnValue = false;
};
Sys.Application.initialize = function Sys$_Application$initialize() {
    if (!this._initialized && !this._initializing) {
        this._initializing = true;
        var u = window.navigator.userAgent.toLowerCase(),
				  v = parseFloat(u.match(/.+(?:rv|it|ml|ra|ie)[\/: ]([\d.]+)/)[1]);

        var initializeDelegate = Function.createDelegate(this, this._doInitialize);

        if (/WebKit/i.test(u) && v < 525.13) {
            this._load_timer = window.setInterval(function () {
                if (/loaded|complete/.test(document.readyState)) {
                    initializeDelegate();
                }
            }, 10);
        } else if (/msie/.test(u) && !window.opera) {
            document.attachEvent('onreadystatechange',
					  function (e) {
					      if (document.readyState == 'complete') {
					          document.detachEvent('on' + e.type, arguments.callee);
					          initializeDelegate();
					      }
					  }
				  );
            if (window == top) {
                (function () {
                    try {
                        document.documentElement.doScroll('left');
                    } catch (e) {
                        setTimeout(arguments.callee, 10);
                        return;
                    }
                    initializeDelegate();
                })();
            }
        } else if (document.addEventListener
				  && ((/opera\//.test(u) && v > 9) ||
					  (/gecko\//.test(u) && v >= 1.8) ||
					  (/khtml\//.test(u) && v >= 4.0) ||
					  (/webkit\//.test(u) && v >= 525.13))) {
            document.addEventListener("DOMContentLoaded", initializeDelegate, false);
        } else {
            $addHandler(window, "load", initializeDelegate);
        };
    };
};
Sys.Application._doInitialize = function Sys$_Application$_doInitialize() {
    if (this._initialized) return;
    
    Sys._Application.callBaseMethod(this, 'initialize');
    if (this._load_timer !== null) {
        clearInterval(this._load_timer);
        this._load_timer = null;
    }
    var handler = this.get_events().getHandler("init");
    if (handler) {
        this.beginCreateComponents();
        handler(this, Sys.EventArgs.Empty);
        this.endCreateComponents();
    }
    if (Sys.WebForms) {
        this._beginRequestHandler = Function.createDelegate(this, this._onPageRequestManagerBeginRequest);
        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(this._beginRequestHandler);
        this._endRequestHandler = Function.createDelegate(this, this._onPageRequestManagerEndRequest);
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(this._endRequestHandler);
    }

    var loadedEntry = this.get_stateString();
    if (loadedEntry !== this._currentEntry) {
        this._navigate(loadedEntry);
    }

    this.raiseLoad();
    this._initializing = false;
};
Sys.Application._loadHandler = function Sys$_Application$_loadHandler() {
    if (this._loadHandlerDelegate) {
        Sys.UI.DomEvent.removeHandler(window, "load", this._loadHandlerDelegate);
        this._loadHandlerDelegate = null;
    }
    this._initializing = true;
    this._doInitialize();
};
if (!Array.prototype.indexOf) {
    Array.prototype.indexOf = function (obj, start) {
        for (var i = (start || 0), j = this.length; i < j; i++) {
            if (this[i] === obj) { return i; }
        }
        return -1;
    }
};