﻿/*
//Pasar a readonly el control txtProveedor
*/
function setReadonlyProveedor() {
    $get('txtProveedor').readOnly = true;
}
/*
mpePeticionariosID: Id del modalpopup de Peticionarios
FiltroPeticionariosID: input text del panel de Peticionarios, en el que se efectúa el filtrado
event: Evento de FF.
inputID: id del control que lanza el evento
hiddenID: Id del control hidden viculado al control que lanza el evento
limpiarPanel: True, lanza la función limpiarPanelPeticionarios, False: no
listadoPeticionariosId: Id del control con el listado de peticionarios
*/
function abrirPanelPeticionarios(mpePeticionariosID, FiltroPeticionariosID, event, inputID, hiddenID, limpiarPanel, listadoPeticionariosId) {
    // MSIE hack
    //CALIDAD
    if (window.event) {
        //CALIDAD
        event = window.event;
    }
    if (limpiarPanel == true) {
        limpiarPanelPeticionarios(FiltroPeticionariosID, listadoPeticionariosId);
    }
    //Primero controlamos si la tecla pulsada es para borrar. Si lo es, borramos y recibimos un return false y termina la función
    var seguir = controlarBorrado(event, inputID, hiddenID);
    if (seguir == true) {
        //Si no es para borrar, mostramos el panel
        $find(mpePeticionariosID).show();
        mostrarPanelModal(mpePeticionariosID);
        var oFiltroPeticionarios = $('#' + FiltroPeticionariosID)
        oFiltroPeticionarios.focus();
        oFiltroPeticionarios.select();
        oFiltroPeticionarios.keydown(); //hay que lanzar este evento para que se lance el filtrado con fastlivefilter
        oFiltroPeticionarios = null;
    }
    else {
        return false;
    }
    seguir = null;
}
/*
mpeCentrosID: Id del modalpopup de centros
FiltroCentrosID: input text del panel de centros, en el que se efectúa el filtrado
event: Evento de FF.
inputID: id del control que lanza el evento
hiddenID: Id del control hidden viculado al control que lanza el evento
*/
function abrirPanelCentros(mpeCentrosID, FiltroCentrosID, event, inputID, hiddenID) {
    // MSIE hack
    //CALIDAD
    if (window.event) {
        //CALIDAD
        event = window.event;
    }
    //Primero controlamos si la tecla pulsada es para borrar. Si lo es, borramos y recibimos un return false y termina la función
    var seguir = controlarBorrado(event, inputID, hiddenID);
    if (seguir == true) {
        //Si no es para borrar, mostramos el panel
        $find(mpeCentrosID).show();
        mostrarPanelModal(mpeCentrosID);
        var oFiltroCentros = $('#' + FiltroCentrosID)
        oFiltroCentros.focus();
        oFiltroCentros.select();
        oFiltroCentros.keydown(); //hay que lanzar este evento para que se lance el filtrado con fastlivefilter
        oFiltroCentros = null;

    }
    else {
        return false;
    }
    seguir = null;
}
/*
mpeCentrosID: Id del modalpopup de centros
FiltroCentrosID: input text del panel de centros, en el que se efectúa el filtrado
event: Evento de FF.
inputID: id del control que lanza el evento
hiddenID: Id del control hidden viculado al control que lanza el evento
*/
function abrirPanelCentrosEP(mpeCentrosID, FiltroCentrosID, event, inputID, hiddenID, comboEmpresa, tipoControl, listadoDatosId, listadoInputsFiltroID, origen, verUON, lblCentrosID, lblFiltroCentrosID, centroDen, textoSel) {
    // MSIE hack
    //CALIDAD
    if (window.event) {
        //CALIDAD
        event = window.event;
    }
    var sEmpresa = '';
    var sData;
    var sUrl = '';
    sEmpresa = $("[id$=" + comboEmpresa + ']').val();
   
    switch (tipoControl) {
        case '0': //CENTRO DE COSTE
            sUrl = '_Common/App_Services/Consultas.asmx/DevolverCentrosPedidosEP'
            sData = { origen: origen, verUON: verUON, Empresa: sEmpresa };
            break;
        case '1': //PARTIDAS PRESUPUESTARIAS
            sUrl = '_Common/App_Services/Consultas.asmx/DevolverPartidasPedidos'
            datos = origen + '@@@';
            if (origen == 65535) {
                limpiarPanelPartidas(listadoDatosId);
                var sFiltro = ''; //Filtro para peticionarios
                sFiltro = $get(listadoInputsFiltroID + " input").value;
                parPrevValue = sFiltro;
                if (sFiltro.length < 4) { return false; }
                datos = datos + sFiltro
                sData = { datos: datos };
            } else {
                sData = { datos: datos };
            }
            break;
        case '2': //PETICIONARIOS
            var sFiltro = ''; //Filtro para peticionarios
            sFiltro = $get(listadoInputsFiltroID).value;
            var FiltroLongitudMinima = 4;
            if (sFiltro.length < FiltroLongitudMinima)
                return false;
            sUrl = '_Common/App_Services/Consultas.asmx/DevolverPeticionarios'
            sData = { Filtro: sFiltro };
            break;
    }
    $.ajax({
        type: "POST",
        url: rutaFS + sUrl,
        data: JSON.stringify(sData),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true
    }).done(function (data) {
        switch (tipoControl) {

            case '0': //CENTRO DE COSTE
            case '2': //PETICIONARIOS
                var HTMLDatos = [];
                var iHTMLDatos = 0;
                var oListadoDatos = jQuery.parseJSON(data.d);
                if (oListadoDatos != null) {
                    //Datos
                    for (var i = 0; i < oListadoDatos.length; i++) {
                        switch (tipoControl) {
                            case '0': //CENTRO DE COSTE
                                var sCod = oListadoDatos[i].CCCOD;
                                var sUON = oListadoDatos[i].CCUON;
                                var sDEN = oListadoDatos[i].CCDEN;
                                var sCoalUon = oListadoDatos[i].CCPM;
                                var sEmpresa = oListadoDatos[i].Empresa;
                                HTMLDatos[iHTMLDatos] = "<div cod='" + sCod + "' coalUon='" + sCoalUon + "' empresa='" + sEmpresa + "' datos='" + sUON + "@@" + sDEN + "' class='SMTemplate' onclick='seleccionarCentroEP(this)'>" + sUON + " - " + sDEN + "</div>";
                                //debugger;
                                break;
                            case '2': //PETICIONARIOS
                                var sCod = oListadoDatos[i].COD;
                                var sDEN = oListadoDatos[i].DEN;
                                if (sCod == "" || sDEN == "") {
                                    var sCodDen = sCod + sDEN;
                                } else {
                                    var sCodDen = sCod + " - " + sDEN;
                                }
                                HTMLDatos[iHTMLDatos] = "<div cod='" + sCod + "' class='SMTemplate' onclick='seleccionarPeticionario(this)'>" + sCodDen + "</div>";
                                break;
                        }
                        iHTMLDatos += 1;
                    }
                    $('#' + listadoDatosId).html(HTMLDatos.join(''));
                    //$('#' + listadoDatosId).append(HTMLDatos.join(''));
                    //Vinculamos el text input para filtrar Datos con el div de Datos filtrar solo en centros, en peticionarios consulta a bdd siempre
                    if (tipoControl == 0) {
                        $(function () {
                            $('#' + listadoInputsFiltroID).fastLiveFilter('#' + listadoDatosId);
                        });
                    }
                }
                HTMLDatos = null;
                iHTMLDatos = null;
                oListadoDatos = null;
                break;
            case '1': //PARTIDAS PRESUPUESTARIAS
                var HTMLPartidas = [];
                var HTMLFiltrosPartidas = [];
                var iHTMLPartidas = 0;
                var oListadoPartidas = jQuery.parseJSON(data.d);
                if (oListadoPartidas != null) {
                    for (var i = 0; i < oListadoPartidas.length; i++) {
                        //Insertar div para cada árbol de partidas
                        var sPRES0Cod = oListadoPartidas[i].PRES0;
                        if (origen == 65535) {
                            var oPartidas = oListadoPartidas[i].PRESN;
                            if (oPartidas != null) {
                                //partidas
                                for (var j = 0; j < oPartidas.length; j++) {
                                    var sCod = oPartidas[j].PRESID;
                                    //                                    if (origen == 65535) { //&HFFFF //PM
                                    var sCodVisible = oPartidas[j].PRESID.replace('#', '-');
                                    //                                    } else {
                                    //                                        var sCodVisible = oPartidas[j].PRESID.replace('@@', '-');
                                    //                                    } 
                                    var sDen = oPartidas[j].PRESDEN;
                                    HTMLPartidas[iHTMLPartidas] = "<div cod='" + sCod + "' class='SMTemplate' onclick='seleccionarPartidaPM(this,\"" + sPRES0Cod.toString() + "\");'>" + sCodVisible + ' ' + sDen + "</div>"
                                    iHTMLPartidas += 1;
                                    var sCod = '';
                                    var sCodVisible = '';
                                    var sDen = '';
                                }
                            }
                            $("[id$=Busqueda_" + sPRES0Cod + ']').append(HTMLPartidas.join(''));
                        }
                        else {
                            HTMLPartidas[iHTMLPartidas] = '<div style="display:none;width:100%;height:400px;overflow:auto;border-top:solid 1px #909090;margin-top:10px;" id="' + sPRES0Cod + '">'
                            HTMLFiltrosPartidas[iHTMLPartidas] = '<input class="Normal" type="text" id="FiltroPartidas' + sPRES0Cod + '" style="width:340px;" />'
                            iHTMLPartidas += 1;
                            var oPartidas = oListadoPartidas[i].PRESN;
                            if (oPartidas != null) {
                                //partidas
                                for (var j = 0; j < oPartidas.length; j++) {
                                    var sCod = oPartidas[j].PRESID;
                                    //                                    if (origen == 65535) { //&HFFFF //PM
                                    //                                        var sCodVisible = oPartidas[j].PRESID.replace('#', '-');
                                    //                                    } else {
                                    var sCodVisible = oPartidas[j].PRESID.replace('@@', '-');
                                    //                                    }
                                    var sDen = oPartidas[j].PRESDEN;
                                    HTMLPartidas[iHTMLPartidas] = "<div cod='" + sCod + "' class='SMTemplate' onclick='seleccionarPartida(this);'>" + sCodVisible + ' ' + sDen + "</div>"
                                    iHTMLPartidas += 1;
                                    var sCod = '';
                                    var sCodVisible = '';
                                    var sDen = '';
                                }
                            }
                            HTMLPartidas[iHTMLPartidas] = '</div>';
                            iHTMLPartidas += 1;
                            if (i == oListadoPartidas.length - 1) {
                                $('#' + listadoDatosId).append(HTMLPartidas.join(''));
                                $('#' + listadoInputsFiltroID).append(HTMLFiltrosPartidas.join(''));
                                //Vinculamos el text input para filtrar partidas con el div de la partida a filtrar
                                for (var z = 0; z < oListadoPartidas.length; z++) {
                                    $(function () {
                                        $('#FiltroPartidas' + oListadoPartidas[z].PRES0).fastLiveFilter('#' + oListadoPartidas[z].PRES0);
                                    });
                                }
                            }
                        }
                    }
                }
                break;
        } 
        //Primero controlamos si la tecla pulsada es para borrar. Si lo es, borramos y recibimos un return false y termina la función
        var seguir = controlarBorrado(event, inputID, hiddenID);
        if (seguir == true) {
            //Si no es para borrar, mostramos el panel
            $('#' + lblCentrosID).text(textoSel + ": " + centroDen);
            $('#' + lblFiltroCentrosID).text(centroDen + ": ");
            $find(mpeCentrosID).show();
            mostrarPanelModal(mpeCentrosID);
            var oFiltroCentros = $('#' + FiltroCentrosID)
            oFiltroCentros.focus();
            oFiltroCentros.select();
            oFiltroCentros.keydown(); //hay que lanzar este evento para que se lance el filtrado con fastlivefilter
            oFiltroCentros = null;

        }
        else {
            return false;
        }
    });

    seguir = null;
    
}
/*
mpePartidasID: Id del Modalpopupextender del panel de partidas
listadoPartidasId: Id del div que contiene los listados de partidas en el panel de partidas
PRES0: nombre de la partida y tb el ID del div que la contiene
PresDen: Nombre del conjunto de partidas
lblPartidaPres0ID: Id del label en que colocamos el nombre del árbol de partidas
lblFiltroPartidaID: Id del segundo label en que colocamos el nombre del árbol de partidas
textolblPartidaPres0ID: texto a colocar en control lblPartidaPres0ID
listadoInputsFiltroID: Id del control que contiene los input text de filtro de partidas
Origen: Nada -> EP 'PM' -> PM
*/
function abrirPanelPartidas(mpePartidasID, listadoPartidasId, Pres0, PresDen, lblPartidaPres0ID, lblFiltroPartidaID, textolblPartidaPres0ID, listadoInputsFiltroID, event, inputID, hiddenID, Origen) {
    // MSIE hack
    //CALIDAD
    if (window.event) {
        //CALIDAD
        event = window.event;
    }
    //Primero controlamos si la tecla pulsada es para borrar. Si lo es, borramos y recibimos un return false y termina la función
    var seguir = controlarBorrado(event, inputID, hiddenID);
    if (seguir == true) {
        //Si no es para borrar, mostramos el panel:
        //Ocultamos todos los div de partidas que cuelgan de listadoPartidasId
        $('#' + listadoPartidasId + ' > div').css('display', 'none');
        //Mostramos el div de partidas del árbol PRES0
        //$('#' + listadoPartidasId + ' > #' + Pres0).css('display', 'block');
        if (Origen == 'EP')
            $('#' + Pres0).css('display', 'block');
        else
            $('[id$=' + Pres0 + ']').css('display', '');
        //Ocultamos todos los input de filtros de partidas que cuelgan de listadoInputsFiltroID
        $('#' + listadoInputsFiltroID + ' > input').css('display', 'none');
        //Mostramos el input de filtro de partidas del árbol PRES0
        //$('#' + listadoInputsFiltroID + ' > #FiltroPartidas' + Pres0).css('display', 'block');
        if (Origen == 'EP')
            $('#FiltroPartidas' + Pres0).css('display', 'inline');
        else
            $('[id$=FiltroPartidas' + Pres0 + ']').css('display', 'inline');
        //Añadimos el nombre del árbol de partidas en el control lblPartidaPres0ID
        $('#' + lblPartidaPres0ID).text(textolblPartidaPres0ID + ': ' + PresDen);
        //Añadimos el nombre del árbol de partidas en el control lblFiltroPartidaID
        if (Origen == 'EP')
            $('#' + lblFiltroPartidaID).text(PresDen + ": ");
        else
            $('[id$=' + lblFiltroPartidaID + ']').text(PresDen + ": ");
        //mostramos el modalpopupextender de partidas
        $find(mpePartidasID).show();
        mostrarPanelModal(mpePartidasID);
        if (Origen == 'EP')
            var oFiltroPartidas = $('#FiltroPartidas' + Pres0)
        else
            var oFiltroPartidas = $('[id$=FiltroPartidas' + Pres0 + ']')
        oFiltroPartidas.focus();
        oFiltroPartidas.select();
        oFiltroPartidas.keydown(); //hay que lanzar este evento para que se lance el filtrado con fastlivefilter
        oFiltroPartidas = null;
    }
    else {
        return false;
    }
    seguir = null;
}


/*
mpePartidasID: Id del Modalpopupextender del panel de partidas
listadoPartidasId: Id del div que contiene los listados de partidas en el panel de partidas
PRES0: nombre de la partida y tb el ID del div que la contiene
PresDen: Nombre del conjunto de partidas
lblPartidaPres0ID: Id del label en que colocamos el nombre del árbol de partidas
lblFiltroPartidaID: Id del segundo label en que colocamos el nombre del árbol de partidas
textolblPartidaPres0ID: texto a colocar en control lblPartidaPres0ID
listadoInputsFiltroID: Id del control que contiene los input text de filtro de partidas
Origen: Nada -> EP 'PM' -> PM
*/
function abrirPanelPartidasEP(mpePartidasID, listadoPartidasId, Pres0, PresDen, lblPartidaPres0ID, lblFiltroPartidaID, textolblPartidaPres0ID, listadoInputsFiltroID, event, inputID, hiddenID, tipoControl, listadoDatosId, listadoInputsFiltroID, sIdCodCentro, origen, sIdCboEmpresa, Pagina, textoFiltro) {
    // MSIE hack
    //CALIDAD
    var sData;
    var sUrl = '';
    var IdCodCentro = "";
    var IdCodEmpresa = 0;
    IdCodCentro = $("#" + sIdCodCentro).val();
    IdCodEmpresa = $("#" + sIdCboEmpresa).val();

    sUrl = '_Common/App_Services/Consultas.asmx/DevolverPartidasEP';

    datos = origen + '@@@' + IdCodCentro + '@@@' + IdCodEmpresa + '@@@' + Pres0 + '@@@' + Pagina + '@@@' + textoFiltro;
    sData = { datos: datos };

    $.ajax({
        type: "POST",
        url: rutaFS + sUrl,
        data: JSON.stringify(sData),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true
    }).done(function (data) {
        var HTMLPartidas = [];
        var HTMLFiltrosPartidas = [];
        var iHTMLPartidas = 0;
        var oListadoPartidas = jQuery.parseJSON(data.d).Arbol;
        var numPags = jQuery.parseJSON(data.d).numPags;
        if (oListadoPartidas != null) {
            for (var i = 0; i < oListadoPartidas.length; i++) {
                //Insertar div para cada árbol de partidas
                var sPRES0Cod = oListadoPartidas[i].PRES0;

                $('#FiltroPartidas' + sPRES0Cod).remove();
                $('#Anterior' + sPRES0Cod).remove();
                $('#Siguiente' + sPRES0Cod).remove();
                $('#' + Pres0).remove();

                HTMLFiltrosPartidas[iHTMLPartidas] = '<input type="text" id="FiltroPartidas' + sPRES0Cod + '" style="width:200px;"  /> '
                if (numPags > 1) {
                    if (Pagina < 1) {
                        HTMLFiltrosPartidas[iHTMLPartidas] = HTMLFiltrosPartidas[iHTMLPartidas] + '<input class="Normal" type="button" id="Anterior' + sPRES0Cod + '" style="width:50px;" onclick="abrirPanelPartidasEP(\'' + mpePartidasID + '\',\'' + listadoPartidasId + '\',\'' + Pres0 + '\',\'' + PresDen + '\',\'' + lblPartidaPres0ID + '\',\'' + lblFiltroPartidaID + '\',\'' + textolblPartidaPres0ID + '\' ,\'' + listadoInputsFiltroID + '\', event, null, null,\'' + tipoControl + '\',\'' + listadoDatosId + '\',\'' + listadoInputsFiltroID + '\',\'' + sIdCodCentro + '\',\'' + origen + '\',\'' + sIdCboEmpresa + '\',' + 0 + ',\'' + textoFiltro + '\');"/> '
                    } else {
                        HTMLFiltrosPartidas[iHTMLPartidas] = HTMLFiltrosPartidas[iHTMLPartidas] + '<input class="Normal" type="button" id="Anterior' + sPRES0Cod + '" style="width:50px;" onclick="abrirPanelPartidasEP(\'' + mpePartidasID + '\',\'' + listadoPartidasId + '\',\'' + Pres0 + '\',\'' + PresDen + '\',\'' + lblPartidaPres0ID + '\',\'' + lblFiltroPartidaID + '\',\'' + textolblPartidaPres0ID + '\' ,\'' + listadoInputsFiltroID + '\', event, null, null,\'' + tipoControl + '\',\'' + listadoDatosId + '\',\'' + listadoInputsFiltroID + '\',\'' + sIdCodCentro + '\',\'' + origen + '\',\'' + sIdCboEmpresa + '\',' + (Pagina - 1) + ',\'' + textoFiltro + '\');"/> '
                    }
                    if (Pagina < numPags) {
                        HTMLFiltrosPartidas[iHTMLPartidas] = HTMLFiltrosPartidas[iHTMLPartidas] + '<input class="Normal" type="button" id="Siguiente' + sPRES0Cod + '" style="width:50px;" onclick="abrirPanelPartidasEP(\'' + mpePartidasID + '\',\'' + listadoPartidasId + '\',\'' + Pres0 + '\',\'' + PresDen + '\',\'' + lblPartidaPres0ID + '\',\'' + lblFiltroPartidaID + '\',\'' + textolblPartidaPres0ID + '\' ,\'' + listadoInputsFiltroID + '\', event, null, null,\'' + tipoControl + '\',\'' + listadoDatosId + '\',\'' + listadoInputsFiltroID + '\',\'' + sIdCodCentro + '\',\'' + origen + '\',\'' + sIdCboEmpresa + '\',' + (Pagina + 1) + ',\'' + textoFiltro + '\');"/> '
                    }
                }

                HTMLPartidas[iHTMLPartidas] = '<div style="display:none;width:100%;height:400px;overflow:auto;border-top:solid 1px #909090;margin-top:10px;" id="' + sPRES0Cod + '">'
                iHTMLPartidas += 1;
                var oPartidas = oListadoPartidas[i].PRESN;
                if (oPartidas != null) {
                    //partidas
                    //debugger;
                    for (var j = 0; j < oPartidas.length; j++) {
                        var sCod = oPartidas[j].PRESID;
                        var parts = oPartidas[j].PRESID.split("@@");
                        var numeroFilas = parts.length;
                        var sCodVisible;
                        for (k = 1; k < numeroFilas; k++) {
                            if (k != numeroFilas - 1) {
                                if (k == 1) { sCodVisible = parts[k] + " - "; }
                                else { sCodVisible = sCodVisible + parts[k] + " - "; }
                            }
                            else {
                                if (k == 1) { sCodVisible = parts[k]; }
                                else { sCodVisible = sCodVisible + parts[k]; }
                            }
                        }
                        var sFecIni = oPartidas[j].FECINI;
                        var fechaIni = eval("new " + sFecIni.slice(1, -1));
                        var strFechaIni = fechaIni.format(UsuMask);

                        var sFecFin = oPartidas[j].FECFIN;
                        var fechaFin = eval("new " + sFecFin.slice(1, -1));
                        var strFechaFin = fechaFin.format(UsuMask);

                        var sFechas = strFechaIni + " - " + strFechaFin;

                        var sDen = oPartidas[j].PRESDEN;

                        var sCentroSMDen = oPartidas[j].CCDEN;
                        var sCentroSMCod = oPartidas[j].CCCOD;

                        if (oPartidas[j].PluriAnual == 1) {
                            HTMLPartidas[iHTMLPartidas] = "<div cod='" + sCod + "' datos='" + sCodVisible + "@@" + sDen.toString().replace("'", "\"") + "@@" + strFechaIni + "@@" + strFechaFin + "' centroCod='" + sCentroSMCod + "' centroDen='" + sCentroSMDen + "' class='SMTemplate' onclick='seleccionarPartidaEP(this);' >" + sCodVisible + " - " + sDen + "</div>"
                        } else {                        
                            HTMLPartidas[iHTMLPartidas] = "<div cod='" + sCod + "' datos='" + sCodVisible + "@@" + sDen.toString().replace("'", "\"") + "@@" + strFechaIni + "@@" + strFechaFin + "' centroCod='" + sCentroSMCod + "' centroDen='" + sCentroSMDen + "' class='SMTemplate' onclick='seleccionarPartidaEP(this);' >" + sCodVisible + " - " + sDen + " - (" + sFechas + ")</div>"
                        }

                        iHTMLPartidas += 1;
                        var sCod = '';
                        var sCodVisible = '';
                        var sDen = '';
                    }
                }
                HTMLPartidas[iHTMLPartidas] = '</div>';
                iHTMLPartidas += 1;
                if (i == oListadoPartidas.length - 1) {
                    $('#' + listadoDatosId).html("");
                    $('#' + listadoDatosId).append(HTMLPartidas.join(''));
                    $('#' + listadoInputsFiltroID).append(HTMLFiltrosPartidas.join(''));

                    if (numPags > 1) {
                        if (Pagina < 1) {
                            $('#Anterior' + sPRES0Cod).attr('disabled', 'disabled');
                            $('#Anterior' + sPRES0Cod).css({ 'background-image': 'url(' + ruta + 'js/jquery/plugins/images/anterior_desactivado.gif)', 'width': '0px' });
                        } else {
                            $('#Anterior' + sPRES0Cod).removeAttr('disabled');
                            $('#Anterior' + sPRES0Cod).css({ 'background-image': 'url(' + ruta + 'js/jquery/plugins/images/anterior.gif)', 'width': '0px' });
                        }
                        if (Pagina < numPags - 1) {
                            $('#Siguiente' + sPRES0Cod).removeAttr('disabled');
                            $('#Siguiente' + sPRES0Cod).css({ 'background-image': 'url(' + ruta + 'js/jquery/plugins/images/siguiente.gif)', 'width': '0px' });
                        } else {
                            $('#Siguiente' + sPRES0Cod).attr('disabled', 'disabled');
                            $('#Siguiente' + sPRES0Cod).css({ 'background-image': 'url(' + ruta + 'js/jquery/plugins/images/siguiente_desactivado.gif)', 'width': '0px' });
                        }
                    }
                    $('#FiltroPartidas' + sPRES0Cod).val(textoFiltro);
                    $('#FiltroPartidas' + sPRES0Cod).autocomplete({
                        minLength: 3,
                        delay: 400,
                        source: function (request, response) {
                            textoFiltro = request.term;
                            Pagina = 0;
                            datos = origen + '@@@' + IdCodCentro + '@@@' + IdCodEmpresa + '@@@' + Pres0 + '@@@' + Pagina + '@@@' + textoFiltro;
                            sData = { datos: datos };
                            $.ajax({
                                url: rutaFS + sUrl,
                                data: JSON.stringify(sData),
                                dataType: "json",
                                type: "POST",
                                contentType: "application/json; charset=utf-8",
                                dataFilter: function (data) { return data; },
                                success: function (data) {
                                    //Hago como la primera vez, pinto todas las partidas
                                    var HTMLPartidas = [];
                                    var HTMLFiltrosPartidas = [];
                                    var iHTMLPartidas = 0;
                                    var oListadoPartidas = jQuery.parseJSON(data.d).Arbol;
                                    var numPags = jQuery.parseJSON(data.d).numPags;
                                    if (oListadoPartidas != null) {
                                        for (var z = 0; z < oListadoPartidas.length; z++) {
                                            //Insertar div para cada árbol de partidas
                                            var sPRES0Cod = oListadoPartidas[z].PRES0;
                                            $('#Anterior' + sPRES0Cod).remove();
                                            $('#Siguiente' + sPRES0Cod).remove();
                                            $('#' + Pres0).remove();

                                            HTMLFiltrosPartidas[iHTMLPartidas] = ''
                                            if (numPags > 1) {
                                                if (Pagina < 1) {
                                                    HTMLFiltrosPartidas[iHTMLPartidas] = HTMLFiltrosPartidas[iHTMLPartidas] + '<input class="Normal" type="button" id="Anterior' + sPRES0Cod + '" style="width:50px;" onclick="abrirPanelPartidasEP(\'' + mpePartidasID + '\',\'' + listadoPartidasId + '\',\'' + Pres0 + '\',\'' + PresDen + '\',\'' + lblPartidaPres0ID + '\',\'' + lblFiltroPartidaID + '\',\'' + textolblPartidaPres0ID + '\' ,\'' + listadoInputsFiltroID + '\', event, null, null,\'' + tipoControl + '\',\'' + listadoDatosId + '\',\'' + listadoInputsFiltroID + '\',\'' + sIdCodCentro + '\',\'' + origen + '\',\'' + sIdCboEmpresa + '\',' + 0 + ',\'' + textoFiltro + '\');"/> '
                                                } else {
                                                    HTMLFiltrosPartidas[iHTMLPartidas] = HTMLFiltrosPartidas[iHTMLPartidas] + '<input class="Normal" type="button" id="Anterior' + sPRES0Cod + '" style="width:50px;" onclick="abrirPanelPartidasEP(\'' + mpePartidasID + '\',\'' + listadoPartidasId + '\',\'' + Pres0 + '\',\'' + PresDen + '\',\'' + lblPartidaPres0ID + '\',\'' + lblFiltroPartidaID + '\',\'' + textolblPartidaPres0ID + '\' ,\'' + listadoInputsFiltroID + '\', event, null, null,\'' + tipoControl + '\',\'' + listadoDatosId + '\',\'' + listadoInputsFiltroID + '\',\'' + sIdCodCentro + '\',\'' + origen + '\',\'' + sIdCboEmpresa + '\',' + (Pagina - 1) + ',\'' + textoFiltro + '\');"/> '
                                                }
                                                if (Pagina < numPags) {
                                                    HTMLFiltrosPartidas[iHTMLPartidas] = HTMLFiltrosPartidas[iHTMLPartidas] + '<input class="Normal" type="button" id="Siguiente' + sPRES0Cod + '" style="width:50px;" onclick="abrirPanelPartidasEP(\'' + mpePartidasID + '\',\'' + listadoPartidasId + '\',\'' + Pres0 + '\',\'' + PresDen + '\',\'' + lblPartidaPres0ID + '\',\'' + lblFiltroPartidaID + '\',\'' + textolblPartidaPres0ID + '\' ,\'' + listadoInputsFiltroID + '\', event, null, null,\'' + tipoControl + '\',\'' + listadoDatosId + '\',\'' + listadoInputsFiltroID + '\',\'' + sIdCodCentro + '\',\'' + origen + '\',\'' + sIdCboEmpresa + '\',' + (Pagina + 1) + ',\'' + textoFiltro + '\');"/> '
                                                }
                                            }
                                            HTMLPartidas[iHTMLPartidas] = '<div style="display:none;width:100%;height:400px;overflow:auto;border-top:solid 1px #909090;margin-top:10px;" id="' + sPRES0Cod + '">'
                                            iHTMLPartidas += 1;
                                            var oPartidas = oListadoPartidas[z].PRESN;
                                            if (oPartidas != null) {
                                                //partidas
                                                for (var j = 0; j < oPartidas.length; j++) {
                                                    var sCod = oPartidas[j].PRESID;
                                                    var parts = oPartidas[j].PRESID.split("@@");
                                                    var numeroFilas = parts.length;
                                                    var sCodVisible;
                                                    for (k = 1; k < numeroFilas; k++) {
                                                        if (k != numeroFilas - 1) {
                                                            if (k == 1) { sCodVisible = parts[k] + " - "; }
                                                            else { sCodVisible = sCodVisible + parts[k] + " - "; }
                                                        }
                                                        else {
                                                            if (k == 1) { sCodVisible = parts[k]; }
                                                            else { sCodVisible = sCodVisible + parts[k]; }
                                                        }
                                                    }
                                                    var sFecIni = oPartidas[j].FECINI;
                                                    var fechaIni = eval("new " + sFecIni.slice(1, -1));
                                                    var strFechaIni = fechaIni.format(UsuMask);

                                                    var sFecFin = oPartidas[j].FECFIN;
                                                    var fechaFin = eval("new " + sFecFin.slice(1, -1));
                                                    var strFechaFin = fechaFin.format(UsuMask);

                                                    var sFechas = strFechaIni + " - " + strFechaFin;

                                                    var sDen = oPartidas[j].PRESDEN;

                                                    var sCentroSMDen = oPartidas[j].CCDEN;
                                                    var sCentroSMCod = oPartidas[j].CCCOD;

                                                    if (oPartidas[j].PluriAnual == 1) {
                                                        HTMLPartidas[iHTMLPartidas] = "<div cod='" + sCod + "' datos='" + sCodVisible + "@@" + sDen.toString().replace("'", "\"") + "@@" + strFechaIni + "@@" + strFechaFin + "' centroCod='" + sCentroSMCod + "' centroDen='" + sCentroSMDen + "' class='SMTemplate' onclick='seleccionarPartidaEP(this);' >" + sCodVisible + " - " + sDen + "</div>"
                                                    } else {
                                                        HTMLPartidas[iHTMLPartidas] = "<div cod='" + sCod + "' datos='" + sCodVisible + "@@" + sDen.toString().replace("'", "\"") + "@@" + strFechaIni + "@@" + strFechaFin + "' centroCod='" + sCentroSMCod + "' centroDen='" + sCentroSMDen + "' class='SMTemplate' onclick='seleccionarPartidaEP(this);' >" + sCodVisible + " - " + sDen + " - (" + sFechas + ")</div>"
                                                    }

                                                    iHTMLPartidas += 1;
                                                    var sCod = '';
                                                    var sCodVisible = '';
                                                    var sDen = '';
                                                }
                                            }
                                            HTMLPartidas[iHTMLPartidas] = '</div>';
                                            iHTMLPartidas += 1;
                                            if (z == oListadoPartidas.length - 1) {
                                                $('#' + listadoDatosId).html("");
                                                $('#' + listadoDatosId).append(HTMLPartidas.join(''));
                                                $('#' + listadoInputsFiltroID).append(HTMLFiltrosPartidas.join(''));
                                                if (numPags > 1) {
                                                    if (Pagina < 1) {
                                                        $('#Anterior' + sPRES0Cod).attr('disabled', 'disabled');
                                                        $('#Anterior' + sPRES0Cod).css({ 'background-image': 'url(' + ruta + 'js/jquery/plugins/images/anterior_desactivado.gif)', 'width': '0px' });
                                                    } else {
                                                        $('#Anterior' + sPRES0Cod).removeAttr('disabled');
                                                        $('#Anterior' + sPRES0Cod).css({ 'background-image': 'url(' + ruta + 'js/jquery/plugins/images/anterior.gif)', 'width': '0px' });
                                                    }
                                                    if (Pagina < numPags - 1) {
                                                        $('#Siguiente' + sPRES0Cod).removeAttr('disabled');
                                                        $('#Siguiente' + sPRES0Cod).css({ 'background-image': 'url(' + ruta + 'js/jquery/plugins/images/siguiente.gif)', 'width': '0px' });
                                                    } else {
                                                        $('#Siguiente' + sPRES0Cod).attr('disabled', 'disabled');
                                                        $('#Siguiente' + sPRES0Cod).css({ 'background-image': 'url(' + ruta + 'js/jquery/plugins/images/siguiente_desactivado.gif)', 'width': '0px' });
                                                    }
                                                }
                                                $('#' + Pres0).css('display', 'block');
                                            }
                                        }
                                    }
                                }
                            });
                        }
                    });
                }

            }
        }
        else {
            return false;
        }

        //$.when(cargarDatosEP(tipoControl, listadoDatosId, listadoInputsFiltroID, origenCarga, sIdCodCentro, sIdCboEmpresa)).done(function () {
        if (window.event) {
            //CALIDAD
            event = window.event;
        }
        //Primero controlamos si la tecla pulsada es para borrar. Si lo es, borramos y recibimos un return false y termina la función
        var seguir = controlarBorrado(event, inputID, hiddenID);
        if (seguir == true) {
            //Si no es para borrar, mostramos el panel:
            //Ocultamos todos los div de partidas que cuelgan de listadoPartidasId
            // $('#' + listadoPartidasId + ' > div').css('display', 'none');
            //Mostramos el div de partidas del árbol PRES0
            //$('#' + listadoPartidasId + ' > #' + Pres0).css('display', 'block');
            $('#' + Pres0).css('display', 'block');
            //Ocultamos todos los input de filtros de partidas que cuelgan de listadoInputsFiltroID
            //$('#' + listadoInputsFiltroID + ' > input').css('display', 'none');
            //Mostramos el input de filtro de partidas del árbol PRES0
            //$('#' + listadoInputsFiltroID + ' > #FiltroPartidas' + Pres0).css('display', 'block');
            $('#FiltroPartidas' + Pres0).css('display', 'inline');
            //Añadimos el nombre del árbol de partidas en el control lblPartidaPres0ID
            $('#' + lblPartidaPres0ID).text(textolblPartidaPres0ID + ': ' + PresDen);
            //Añadimos el nombre del árbol de partidas en el control lblFiltroPartidaID
            $('#' + lblFiltroPartidaID).text(PresDen + ": ");
            //mostramos el modalpopupextender de partidas
            if (Pagina > 0) {
                ocultarPanelModal(mpePartidasID);
            }
            $find(mpePartidasID).show();
            mostrarPanelModal(mpePartidasID);
        }
        else {
            return false;
        }
        seguir = null;
    });

}


function cargarDatosEP(tipoControl, listadoDatosId, listadoInputsFiltroID, origen, sIdCodCentro, sIdCboEmpresa) {

}
/*
tipoControl:Tipo de Control que cargamos
listadoDatosId: Id del div que contiene los listados de partidas en el panel de partidas
listadoInputsFiltroID: Id del div que contiene los input text asociados a la busqueda de cada partida, o bien, si solo hay uno, id del input text de filtro
origen: Origen desde el que se solicitan las partidas. EP, PM
*/
function cargarDatos(tipoControl, listadoDatosId, listadoInputsFiltroID, origen) {
    var sData;
    var sUrl = '';
    //debugger;
    switch (tipoControl) {
        case '0': //CENTRO DE COSTE
            sUrl = '_Common/App_Services/Consultas.asmx/DevolverCentrosPedidos'
            sData = { origen: origen };
            break;
        case '1': //PARTIDAS PRESUPUESTARIAS
            sUrl = '_Common/App_Services/Consultas.asmx/DevolverPartidasPedidos'
            datos = origen + '@@@';
            if (origen == 65535) {
                limpiarPanelPartidas(listadoDatosId);
                var sFiltro = ''; //Filtro para peticionarios
                sFiltro = $get(listadoInputsFiltroID + " input").value;
                parPrevValue = sFiltro;
                if (sFiltro.length < 4) { return false; }
                datos = datos + sFiltro
                sData = { datos: datos };
            } else {
                sData = { datos: datos };
            }
            break;
        case '2': //PETICIONARIOS
            var sFiltro = ''; //Filtro para peticionarios
            sFiltro = $get(listadoInputsFiltroID).value;
            var FiltroLongitudMinima = 4;
            if (sFiltro.length < FiltroLongitudMinima)
                return false;
            sUrl = '_Common/App_Services/Consultas.asmx/DevolverPeticionarios'
            sData = { Filtro: sFiltro };
            break;
    }
    $.ajax({
        type: "POST",
        url: rutaFS + sUrl,
        data: JSON.stringify(sData),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true
    }).done(function (data) {
        switch (tipoControl) {
            case '0': //CENTRO DE COSTE
            case '2': //PETICIONARIOS
                var HTMLDatos = [];
                var iHTMLDatos = 0;
                var oListadoDatos = jQuery.parseJSON(data.d);
                if (oListadoDatos != null) {
                    //Datos
                    for (var i = 0; i < oListadoDatos.length; i++) {
                        switch (tipoControl) {
                            case '0': //CENTRO DE COSTE
                                var sCod = oListadoDatos[i].CCCOD;
                                var sUON = oListadoDatos[i].CCUON;
                                var sDEN = oListadoDatos[i].CCDEN;
                                if (origen == 65535) { //&HFFFF //PM
                                    var sCodPM = oListadoDatos[i].CCPM;
                                    HTMLDatos[iHTMLDatos] = "<div cod='" + sCodPM + "' class='SMTemplate' onclick='seleccionarCentro(this)'>" + sUON + " - " + sDEN + "</div>";
                                } else {
                                    //                                    if (origen == 32768) {
                                    //                                        HTMLDatos[iHTMLDatos] = "<div cod='" + sCod + "' class='SMTemplate' onclick='seleccionarCentroEP(this)'>" + sUON + " - " + sDEN + "</div>";
                                    //                                    }else{
                                    HTMLDatos[iHTMLDatos] = "<div cod='" + sCod + "' class='SMTemplate' onclick='seleccionarCentro(this)'>" + sUON + " - " + sDEN + "</div>"; //}
                                }
                                break;
                            case '2': //PETICIONARIOS
                                var sCod = oListadoDatos[i].COD;
                                var sDEN = oListadoDatos[i].DEN;
                                if (sCod == "" || sDEN == "") {
                                    var sCodDen = sCod + sDEN;
                                } else {
                                    var sCodDen = sCod + " - " + sDEN;
                                }
                                HTMLDatos[iHTMLDatos] = "<div cod='" + sCod + "' class='SMTemplate' onclick='seleccionarPeticionario(this)'>" + sCodDen + "</div>";
                                break;
                        }
                        iHTMLDatos += 1;
                    }
                    $('#' + listadoDatosId).html(HTMLDatos.join(''));
                    //$('#' + listadoDatosId).append(HTMLDatos.join(''));
                    //Vinculamos el text input para filtrar Datos con el div de Datos filtrar solo en centros, en peticionarios consulta a bdd siempre
                    if (tipoControl == 0) {
                        $(function () {
                            $('#' + listadoInputsFiltroID).fastLiveFilter('#' + listadoDatosId);
                        });
                    }
                }
                HTMLDatos = null;
                iHTMLDatos = null;
                oListadoDatos = null;
                break;
            case '1': //PARTIDAS PRESUPUESTARIAS
                var HTMLPartidas = [];
                var HTMLFiltrosPartidas = [];
                var iHTMLPartidas = 0;
                var oListadoPartidas = jQuery.parseJSON(data.d);
                if (oListadoPartidas != null) {
                    for (var i = 0; i < oListadoPartidas.length; i++) {
                        //Insertar div para cada árbol de partidas
                        var sPRES0Cod = oListadoPartidas[i].PRES0;
                        if (origen == 65535) {
                            var oPartidas = oListadoPartidas[i].PRESN;
                            if (oPartidas != null) {
                                //partidas
                                for (var j = 0; j < oPartidas.length; j++) {
                                    var sCod = oPartidas[j].PRESID;
                                    //                                    if (origen == 65535) { //&HFFFF //PM
                                    var sCodVisible = oPartidas[j].PRESID.replace('#', '-');
                                    //                                    } else {
                                    //                                        var sCodVisible = oPartidas[j].PRESID.replace('@@', '-');
                                    //                                    } 
                                    var sDen = oPartidas[j].PRESDEN;
                                    HTMLPartidas[iHTMLPartidas] = "<div cod='" + sCod + "' class='SMTemplate' onclick='seleccionarPartidaPM(this,\"" + sPRES0Cod.toString() + "\");'>" + sCodVisible + ' ' + sDen + "</div>"
                                    iHTMLPartidas += 1;
                                    var sCod = '';
                                    var sCodVisible = '';
                                    var sDen = '';
                                }
                            }
                            $("[id$=Busqueda_" + sPRES0Cod + ']').append(HTMLPartidas.join(''));
                        }
                        else {
                            HTMLPartidas[iHTMLPartidas] = '<div style="display:none;width:100%;height:400px;overflow:auto;border-top:solid 1px #909090;margin-top:10px;" id="' + sPRES0Cod + '">'
                            HTMLFiltrosPartidas[iHTMLPartidas] = '<input class="Normal" type="text" id="FiltroPartidas' + sPRES0Cod + '" style="width:340px;" />'
                            iHTMLPartidas += 1;
                            var oPartidas = oListadoPartidas[i].PRESN;
                            if (oPartidas != null) {
                                //partidas
                                for (var j = 0; j < oPartidas.length; j++) {
                                    var sCod = oPartidas[j].PRESID;
                                    //                                    if (origen == 65535) { //&HFFFF //PM
                                    //                                        var sCodVisible = oPartidas[j].PRESID.replace('#', '-');
                                    //                                    } else {
                                    var sCodVisible = oPartidas[j].PRESID.replace('@@', '-');
                                    //                                    }
                                    var sDen = oPartidas[j].PRESDEN;
                                    HTMLPartidas[iHTMLPartidas] = "<div cod='" + sCod + "' class='SMTemplate' onclick='seleccionarPartida(this);'>" + sCodVisible + ' ' + sDen + "</div>"
                                    iHTMLPartidas += 1;
                                    var sCod = '';
                                    var sCodVisible = '';
                                    var sDen = '';
                                }
                            }
                            HTMLPartidas[iHTMLPartidas] = '</div>';
                            iHTMLPartidas += 1;
                            if (i == oListadoPartidas.length - 1) {
                                $('#' + listadoDatosId).append(HTMLPartidas.join(''));
                                $('#' + listadoInputsFiltroID).append(HTMLFiltrosPartidas.join(''));
                                //Vinculamos el text input para filtrar partidas con el div de la partida a filtrar
                                for (var z = 0; z < oListadoPartidas.length; z++) {
                                    $(function () {
                                        $('#FiltroPartidas' + oListadoPartidas[z].PRES0).fastLiveFilter('#' + oListadoPartidas[z].PRES0);
                                    });
                                }
                            }
                        }
                    }
                }
                break;
        }
    });
}


/*
tipoControl:Tipo de Control que cargamos
listadoDatosId: Id del div que contiene los listados de partidas en el panel de partidas
listadoInputsFiltroID: Id del div que contiene los input text asociados a la busqueda de cada partida, o bien, si solo hay uno, id del input text de filtro
origen: Origen desde el que se solicitan las partidas. EP, PM
*/
function cargarDatosEMP(tipoControl, listadoDatosId, listadoInputsFiltroID, origen, verUON, Empresa) {
    var sData;
    var sUrl = '';
   
    switch (tipoControl) {
        case '0': //CENTRO DE COSTE
            sUrl = '_Common/App_Services/Consultas.asmx/DevolverCentrosPedidosEP'
            sData = { origen: origen, verUON: verUON, Empresa: ''};
            break;
        case '1': //PARTIDAS PRESUPUESTARIAS
            sUrl = '_Common/App_Services/Consultas.asmx/DevolverPartidasPedidos'
            datos = origen + '@@@';
            if (origen == 65535) {
                limpiarPanelPartidas(listadoDatosId);
                var sFiltro = ''; //Filtro para peticionarios
                sFiltro = $get(listadoInputsFiltroID + " input").value;
                parPrevValue = sFiltro;
                if (sFiltro.length < 4) { return false; }
                datos = datos + sFiltro
                sData = { datos: datos };
            } else {
                sData = { datos: datos };
            }
            break;
        case '2': //PETICIONARIOS
            var sFiltro = ''; //Filtro para peticionarios
            sFiltro = $get(listadoInputsFiltroID).value;
            var FiltroLongitudMinima = 4;
            if (sFiltro.length < FiltroLongitudMinima)
                return false;
            sUrl = '_Common/App_Services/Consultas.asmx/DevolverPeticionarios'
            sData = { Filtro: sFiltro };
            break;
    }
    $.ajax({
        type: "POST",
        url: rutaFS + sUrl,
        data: JSON.stringify(sData),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true
    }).done(function (data) {
        switch (tipoControl) {

            case '0': //CENTRO DE COSTE
            case '2': //PETICIONARIOS
                var HTMLDatos = [];
                var iHTMLDatos = 0;
                var oListadoDatos = jQuery.parseJSON(data.d);
                if (oListadoDatos != null) {
                    //Datos
                    for (var i = 0; i < oListadoDatos.length; i++) {
                        switch (tipoControl) {
                            case '0': //CENTRO DE COSTE
                                var sCod = oListadoDatos[i].CCCOD;
                                var sUON = oListadoDatos[i].CCUON;
                                var sDEN = oListadoDatos[i].CCDEN;
                                var sCoalUon = oListadoDatos[i].CCPM;
                                var sEmpresa = oListadoDatos[i].Empresa;
                                HTMLDatos[iHTMLDatos] = "<div cod='" + sCod + "' coalUon='" + sCoalUon + "' empresa='" + sEmpresa + "' datos='" + sUON + "@@" + sDEN + "' class='SMTemplate' onclick='seleccionarCentroEP(this)'>" + sUON + " - " + sDEN + "</div>";
                                //debugger;
                                break;
                            case '2': //PETICIONARIOS
                                var sCod = oListadoDatos[i].COD;
                                var sDEN = oListadoDatos[i].DEN;
                                if (sCod == "" || sDEN == "") {
                                    var sCodDen = sCod + sDEN;
                                } else {
                                    var sCodDen = sCod + " - " + sDEN;
                                }
                                HTMLDatos[iHTMLDatos] = "<div cod='" + sCod + "' class='SMTemplate' onclick='seleccionarPeticionario(this)'>" + sCodDen + "</div>";
                                break;
                        }
                        iHTMLDatos += 1;
                    }
                    $('#' + listadoDatosId).html(HTMLDatos.join(''));
                    //$('#' + listadoDatosId).append(HTMLDatos.join(''));
                    //Vinculamos el text input para filtrar Datos con el div de Datos filtrar solo en centros, en peticionarios consulta a bdd siempre
                    if (tipoControl == 0) {
                        $(function () {
                            $('#' + listadoInputsFiltroID).fastLiveFilter('#' + listadoDatosId);
                        });
                    }
                }
                HTMLDatos = null;
                iHTMLDatos = null;
                oListadoDatos = null;
                break;
            case '1': //PARTIDAS PRESUPUESTARIAS
                var HTMLPartidas = [];
                var HTMLFiltrosPartidas = [];
                var iHTMLPartidas = 0;
                var oListadoPartidas = jQuery.parseJSON(data.d);
                if (oListadoPartidas != null) {
                    for (var i = 0; i < oListadoPartidas.length; i++) {
                        //Insertar div para cada árbol de partidas
                        var sPRES0Cod = oListadoPartidas[i].PRES0;
                        if (origen == 65535) {
                            var oPartidas = oListadoPartidas[i].PRESN;
                            if (oPartidas != null) {
                                //partidas
                                for (var j = 0; j < oPartidas.length; j++) {
                                    var sCod = oPartidas[j].PRESID;
                                    //                                    if (origen == 65535) { //&HFFFF //PM
                                    var sCodVisible = oPartidas[j].PRESID.replace('#', '-');
                                    //                                    } else {
                                    //                                        var sCodVisible = oPartidas[j].PRESID.replace('@@', '-');
                                    //                                    } 
                                    var sDen = oPartidas[j].PRESDEN;
                                    HTMLPartidas[iHTMLPartidas] = "<div cod='" + sCod + "' class='SMTemplate' onclick='seleccionarPartidaPM(this,\"" + sPRES0Cod.toString() + "\");'>" + sCodVisible + ' ' + sDen + "</div>"
                                    iHTMLPartidas += 1;
                                    var sCod = '';
                                    var sCodVisible = '';
                                    var sDen = '';
                                }
                            }
                            $("[id$=Busqueda_" + sPRES0Cod + ']').append(HTMLPartidas.join(''));
                        }
                        else {
                            HTMLPartidas[iHTMLPartidas] = '<div style="display:none;width:100%;height:400px;overflow:auto;border-top:solid 1px #909090;margin-top:10px;" id="' + sPRES0Cod + '">'
                            HTMLFiltrosPartidas[iHTMLPartidas] = '<input class="Normal" type="text" id="FiltroPartidas' + sPRES0Cod + '" style="width:340px;" />'
                            iHTMLPartidas += 1;
                            var oPartidas = oListadoPartidas[i].PRESN;
                            if (oPartidas != null) {
                                //partidas
                                for (var j = 0; j < oPartidas.length; j++) {
                                    var sCod = oPartidas[j].PRESID;
                                    //                                    if (origen == 65535) { //&HFFFF //PM
                                    //                                        var sCodVisible = oPartidas[j].PRESID.replace('#', '-');
                                    //                                    } else {
                                    var sCodVisible = oPartidas[j].PRESID.replace('@@', '-');
                                    //                                    }
                                    var sDen = oPartidas[j].PRESDEN;
                                    HTMLPartidas[iHTMLPartidas] = "<div cod='" + sCod + "' class='SMTemplate' onclick='seleccionarPartida(this);'>" + sCodVisible + ' ' + sDen + "</div>"
                                    iHTMLPartidas += 1;
                                    var sCod = '';
                                    var sCodVisible = '';
                                    var sDen = '';
                                }
                            }
                            HTMLPartidas[iHTMLPartidas] = '</div>';
                            iHTMLPartidas += 1;
                            if (i == oListadoPartidas.length - 1) {
                                $('#' + listadoDatosId).append(HTMLPartidas.join(''));
                                $('#' + listadoInputsFiltroID).append(HTMLFiltrosPartidas.join(''));
                                //Vinculamos el text input para filtrar partidas con el div de la partida a filtrar
                                for (var z = 0; z < oListadoPartidas.length; z++) {
                                    $(function () {
                                        $('#FiltroPartidas' + oListadoPartidas[z].PRES0).fastLiveFilter('#' + oListadoPartidas[z].PRES0);
                                    });
                                }
                            }
                        }
                    }
                }
                break;
        }
    });
}


/*Coloca la partida seleccionada en el Textbox de la partida y oculta el panel de búsqueda*/
function seleccionarPartida(sender) {
    var sPresCod = htmlDecode(sender.getAttribute("cod"));
    var sPresDen = htmlDecode(sender.innerHTML);
    //CALIDAD
    var sPRES0 = sender.parentElement.id;
    var tbPPres = $('#tbPPres_' + sPRES0);
    tbPPres.val(sPresDen);
    tbPPres = null;
    var tbPPresHidden = $('#tbPPres_Hidden_' + sPRES0);
    tbPPresHidden.val(sPresCod);
    tbPPresHidden = null;
    ocultarPanelModal('mpePartidas');
}

/*Coloca la partida seleccionada en el Textbox de la partida y oculta el panel de búsqueda*/
function seleccionarPartidaPM(sender, sPRES0) {
    var sPresCod = htmlDecode(sender.getAttribute("cod"));
    var sPresDen = htmlDecode(sender.innerHTML);
    //CALIDAD
    var tbPPres = $('[id$=tbPPres_' + sPRES0 + ']');
    tbPPres.val(sPresDen);
    tbPPres = null;
    var tbPPresHidden = $('[id$=tbPPres_Hidden_' + sPRES0 + ']');
    tbPPresHidden.val(sPresCod);
    tbPPresHidden = null;
    ocultarPanelModal('mpePartidas');
}
/*Coloca el centro seleccionado en el Textbox de los centros y oculta el panel de búsqueda*/
function seleccionarCentro(sender) {
    var sCCCod = htmlDecode(sender.getAttribute("cod"));
    var sCCDen = htmlDecode(sender.innerHTML);
    var tbCtroCoste = $('#tbCtroCoste');
    tbCtroCoste.val(sCCDen)
    tbCtroCoste = null;
    var tbCtroCosteHidden = $('#tbCtroCoste_Hidden');
    tbCtroCosteHidden.val(sCCCod);
    tbCtroCosteHidden = null;
    ocultarPanelModal('mpeCentros');
}
/*Coloca el Peticionario seleccionado en el Textbox de los Peticionarios y oculta el panel de búsqueda*/
//var itxtPetheight = 0
function seleccionarPeticionario(sender) {
    var sCCCod = htmlDecode(sender.getAttribute("cod"));
    var sCCDen = htmlDecode(sender.innerHTML);
    if ($('[id$=txtCodElementID]').length == 0 && $('[id$=txtDenElementID]').length == 0) {
        var txtPeticionario = $('#txtPeticionario');
        var txtPeticionarioHidden = $('#txtPeticionario_Hidden');
        //Si el peticionario ya ha sido añadido previamente, no lo añadimos una segunda vez
        var arrPeticionarios = txtPeticionarioHidden.val().split('$$$');
        if (jQuery.inArray(sCCCod, arrPeticionarios) == -1) {
            //Si ya tenemos algún valor, agregamos el nuevo
            //Texto
            if (txtPeticionario.val() != '') {
                var lineBreak = '\n'
                var sCCDenDef = txtPeticionario.val() + ',' + lineBreak + sCCDen;
                txtPeticionario.val(sCCDenDef)
            } else {
                txtPeticionario.val(sCCDen);
            };
            //Valor
            if (txtPeticionarioHidden.val() != '') {
                var sCCCodDef = txtPeticionarioHidden.val() + '$$$' + sCCCod;
                txtPeticionarioHidden.val(sCCCodDef)
            } else {
                txtPeticionarioHidden.val(sCCCod);
            };
            //Recalcular altura del control:
            arrPeticionarios = txtPeticionarioHidden.val().split('$$$');
            txtPeticionario.height((txtPeticionario[0].scrollHeight) + "px");
        }
        txtPeticionario = null;
        txtPeticionarioHidden = null;
        ocultarPanelModal('mpePeticionarios');
    } else {
        var codActualPeticionario = $('#' + $('[id$=txtCodElementID]').val(), window.opener.document).val();
        var denActualPeticionario = $('#' + $('[id$=txtDenElementID]').val(), window.opener.document).val();
        $('#' + $('[id$=txtCodElementID]').val(), window.opener.document).val((codActualPeticionario == '' ? sCCCod : codActualPeticionario + '###' + sCCCod));
        $('#' + $('[id$=txtDenElementID]').val(), window.opener.document).val((denActualPeticionario == '' ? sCCDen : denActualPeticionario + ',' + sCCDen));
        $('#' + $('[id$=txtDenElementID]').val(), window.opener.document).prop('title', $('#' + $('[id$=txtDenElementID]').val(), window.opener.document).val());
        window.close();
    };
}
/*
Controlamos el borrado de los input text de solo lectura y de paso
Impedimos el retroceso de página que se provoca en un input text readonly cuando se intenta borrar (al pulsar backspace)
event: argumentos del evento
inputID: ID del control input text que ha lanzado el evento
hiddenID: Id del control hidden vinculado al input text que ha lanzado el evento
*/
function controlarBorrado(event, inputID, hiddenID) {
    if (event.keyCode) {
        if (event.keyCode == 8 || event.keyCode == 46) { //backspace y supr
            if (inputID && hiddenID)
                LimpiarInput(inputID, hiddenID);
            if (event.keyCode == 8)
                if (event.preventDefault)
                    event.preventDefault();
                else
                    event.returnValue = false;
            return false;
        }
        else {
            return true;
        }
    }
    else {
        return true;
    }
}
//inputID: ID del control input a limpiar
//hiddenID: Id del control hidden a limpiar
function LimpiarInput(inputID, hiddenID) {
    $get(inputID).value = "";
    $get(hiddenID).value = "";
}
//create a in-memory div, set it's inner text(which jQuery automatically encodes)
//then grab the encoded contents back out.  The div never exists on the page.
function htmlEncode(value) {
    if (value)
        return $('<div/>').text(value).html();
}
function htmlDecode(value) {
    if (value)
        return $('<div/>').html(value).text();
}
/*
tipoControl:Tipo de Control que cargamos
listadoDatosId: Id del div que contiene el listado de peticionarios
InputFiltroID: Id del input text que filtra el peticionario
origen: Origen desde el que se solicitan las partidas. EP, PM
*/
var petPrevValue = '' //Valor previo del peticionario
function cargarDatosPeticionario(tipoControl, listadoDatosId, InputFiltroID, origen) {
    if ($get(InputFiltroID).value != petPrevValue)
        cargarDatos(tipoControl, listadoDatosId, InputFiltroID, origen)
    petPrevValue = $get(InputFiltroID).value;
}
/*
Solicitamos la carga inicial del listado de peticionarios. Se guardará en caché en el servidor
*/
function cargarInicialDatosPeticionario() {
    var sUrl = '_Common/App_Services/Consultas.asmx/DevolverPeticionarios'
    var sData = '{"Filtro":""}';
    $.ajax({
        type: "POST",
        url: rutaFS + sUrl,
        data: sData,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true
    }).done(function (data) {
    });
}
//Vaciamos los resultados del panel y el texto de filtrado
//FiltroPeticionariosID: input text del panel de Peticionarios, en el que se efectúa el filtrado
//listadoPeticionariosId: Id del control con el listado de peticionarios
function limpiarPanelPeticionarios(txtPeticionarios, listadoPeticionariosId) {
    $get(txtPeticionarios).value = "";
    $('#' + listadoPeticionariosId).empty().html('');
}
/*
tipoControl:Tipo de Control que cargamos
listadoDatosId: Id del div que contiene los listados de partidas en el panel de partidas
InputFiltroID: Id del input text que filtra las partidas
origen: Origen desde el que se solicitan las partidas. EP, PM
*/
var parPrevValue = '' //Valor previo del filtro de partida
function cargarDatosPartidas(tipoControl, listadoDatosId, InputFiltroID, origen) {
    if ($get(InputFiltroID).value != parPrevValue)
        cargarDatos(tipoControl, listadoDatosId, InputFiltroID, origen)
    parPrevValue = $get(InputFiltroID).value;
}
/*
Solicitamos la carga inicial del listado de partidas.
*/
function cargarInicialDatosPartidas(tipoControl, listadoDatosId, listadoInputsFiltroID, origen) {
    var sData = '';
    var sUrl = '_Common/App_Services/Consultas.asmx/DevolverPartidasPedidosInicial'
    $.ajax({
        type: "POST",
        url: rutaFS + sUrl,
        data: sData,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true
    }).done(function (data) {
        var HTMLPartidas = [];
        var HTMLFiltrosPartidas = [];
        var iHTMLPartidas = 0;
        var oListadoPartidas = jQuery.parseJSON(data.d);
        if (oListadoPartidas != null) {
            for (var i = 0; i < oListadoPartidas.length; i++) {
                //Insertar div para cada árbol de partidas
                var sPRES0Cod = oListadoPartidas[i].PRES0;
                HTMLPartidas[iHTMLPartidas] = '<div style="display:none;width:100%;height:400px;overflow:auto;border-top:solid 1px #909090;margin-top:10px;" id="' + sPRES0Cod + '">'
                HTMLFiltrosPartidas[iHTMLPartidas] = '<input class="Normal" type="text" id="FiltroPartidas' + sPRES0Cod + '" style="width:300px;" />'
                iHTMLPartidas += 1;
                HTMLPartidas[iHTMLPartidas] = '</div>';
                if (i == oListadoPartidas.length - 1) {
                    $('#' + listadoDatosId).append(HTMLPartidas.join(''));
                    $('#' + listadoInputsFiltroID + ' > [id$=minCharPartidas]').before(HTMLFiltrosPartidas.join(''));
                }
                $("#FiltroPartidas" + sPRES0Cod).live('keyup', function () {
                    setSelectedPRES0(sPRES0Cod);
                    cargarDatos(tipoControl, listadoDatosId, listadoInputsFiltroID, origen);
                });
                $("#FiltroPartidas" + sPRES0Cod).live('paste', function () {
                    setSelectedPRES0(sPRES0Cod);
                    setTimeout(function () {
                        cargarDatos(tipoControl, listadoDatosId, listadoInputsFiltroID, origen);
                    }, 100);
                });
            }
        }
    });
}
var selectedPRES0 = ''
function setSelectedPRES0(PRES0) {
    selectedPRES0 = PRES0;
}
//Vaciamos los resultados del panel
//listadoPartidaId: Id del control con el listado de partidas
function limpiarPanelPartidas(listadoDatosId) {
    $('#' + listadoDatosId + ' > [id$=' + selectedPRES0 + ']').empty().html('');
}
function mostrarPanelModal(controlAOcultar) {
    //$('[id$=panelUpdateProgress]').show();
    //$('[id$=ImgProgress]').hide();
    //$('[id$=LblProcesando]').hide();
    $('body').append("<div class='modal'></div>")
    $('.modal').live('click', function () {
        ocultarPanelModal(controlAOcultar);
    });
}
function ocultarPanelModal(controlAOcultar) {
    //$('[id$=panelUpdateProgress]').hide();
    //$('[id$=ImgProgress]').show();
    //$('[id$=LblProcesando]').show();
    $('.modal').remove();
    $find(controlAOcultar).hide();
    $('.modal').die('click');
}
