﻿//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//              FORMATO NUMÉRICO TEXTBOXES
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//convierte una cadena en un número
function str2num(str, vdecimalfmt, vthousanfmt) {
    if (str == "") {
        return (null);
    }
    var rm;
    if (vthousanfmt != "") {
        var rm = eval("/\\" + vthousanfmt + "/g")
    }
    str = str.replace(rm, "");

    rm = eval("/\\" + vdecimalfmt + "/g");
    str = str.replace(rm, ".");

    //var num = new Number(str);
    var textDecimal = str.slice((str.indexOf(".") != -1) ? str.indexOf(".") : str.length);
    var num = new BigNumber(str, 100)
    //var num = new BigNumber(str, 100)

    return (num);

}

//convierte una cadena en un número
//El formato de los DBlValue es el del usuario local, por lo que puede que el separador de decimales sea ","
//en vez de ".". Lo comprobamos y cambiamos si es necesario.
function str2num4DBlValue(str) {
    if (str == "") {
        return (null);
    }
    var rm = eval("/\\,/g")
    str = str.replace(rm, ".");

    var textDecimal = str.slice((str.indexOf(".") != -1) ? str.indexOf(".") : str.length);
    var num = new BigNumber(str, 100)
    return (num);
}

//convierte un número en una cadena
//El formato de los DBlValue es el del usuario local, por lo que puede que el separador de decimales sea ","
//en vez de ".". Lo comprobamos y cambiamos si es necesario.
function num2str4DBlValue(num) {
    if (num == 0) {
        return (0);
    }
    if (num == null) {
        return ("")
    }
    if (isNaN(num)) {
        return ("")
    }
    var str = num.toString()

    var rm = eval("/\\./g")
    str = str.replace(rm, ",");

    return str
}

//''' <summary>
//''' convierte un numero a una cadena aplicandole el formato correspondiente
//''' </summary>
//''' <param name="num">Numero a convertir</param>
//''' <param name="vdecimalfmt">separador de decimales</param>
//''' <param name="vthousanfmt">separador de miles</param>
//''' <param name="vprecisionfmt">numero de decimales a mostrar</param>                
//''' <returns>cadena con el formato correspondiente ó si es un numero muy pequeño el numero</returns>
//''' <remarks>Llamada desde: Toda pantalla q muestre numeros. Si haces un find salen 141 usos; Tiempo máximo: 0,1</remarks>
function num2str(num, vdecimalfmt, vthousanfmt, vprecisionfmt) {
    var j
    if (num == 0) {
        //Añadir ceros al final del control hasta el total de decimales.
        //Cuando el valor que llega es 99 el control puede editar decimales pero no mostrar ceros hasta 99
        if (vprecisionfmt > 0 && vprecisionfmt != 99) {
            var zeros = '';
            for (var i = 1; i <= vprecisionfmt; i++) {
                zeros += '0';
            }
            return num + vdecimalfmt + zeros;
        }
        else {
            return ("0");
        }
    }
    if (num == null) {
        return ("")
    }
    if (isNaN(num)) {
        return ("")
    }

    var sNum
    var sDec
    var sInt
    var bNegativo
    var i


    bNegativo = false
    var vNum = redondea(num, vprecisionfmt)
    sNum = vNum.toString()

    var rde = /\e/
    var pDecE = sNum.search(rde)

    if (pDecE != -1) {
        sInt = sNum.substr(0, pDecE)
        sDec = (sNum.substr(pDecE + 2, sNum.length)) - 1

        sNum = "0" + vdecimalfmt
        for (var i = 0; i < sDec; i++) {
            sNum = sNum + "0"
        }

        for (var j = 0; j < sInt.length; j++) {
            if (isNaN(sInt.charAt(j))) {
                sNum = sNum

                if (sInt.substr(j + 1, 3) == "000") {
                    j = sInt.length
                }
                if (sInt.substr(j + 1, 3) == "999") {
                    sNum = sNum.substr(0, sNum.length - 1)
                    if (sInt.substr(j - 1, 1) == "9") {
                        sNum = sNum.substr(0, sNum.length - 1)
                        sNum = sNum + "1"
                    }
                    else {
                        num = parseFloat(sInt.charAt(j - 1)) + 1
                        sNum = sNum + num
                    }
                    j = sInt.length
                }
            }
            else {
                if (sInt.substr(j + 1, 3) == "000") {
                    sNum = sNum + sInt.charAt(j)

                    j = sInt.length
                }
                else {
                    if (sInt.substr(j + 1, 3) == "999") {

                        if (sInt.substr(j, 1) == "9") {
                            sNum = sNum.substr(0, sNum.length - 1)
                            sNum = sNum + "1"
                        }
                        else {
                            num = parseFloat(sInt.charAt(j)) + 1
                            sNum = sNum + num
                        }

                        j = sInt.length
                    }
                    else {
                        sNum = sNum + sInt.charAt(j)
                    }
                }
            }
        }

        //--------------------------------------------------
        //Por una parte mirar si es Valido
        //Por otra parte mirar cifras significativas
        var MeVale = 0
        var k = 0

        for (k = 0; k < vprecisionfmt + 12; k++) {
            if (k > 1) {
                if (sNum.charAt(k) != "0")//Por una parte mirar si es Valido	
                {
                    k = vprecisionfmt + 12
                    MeVale = 1
                }
            }
        }

        if (MeVale == 0) {
            sNum = "0"
        }
        else {//Por otra parte mirar cifras significativas

            if (sNum.length - 2 > vprecisionfmt) {
                var sNumAux1
                var sNumAux2
                var iNumAux1
                var iNumAux2
                var Fin = 0
                var PosValido = vprecisionfmt

                while (Fin == 0) {
                    sNumAux1 = sNum.substr(2, PosValido)
                    sNumAux2 = "0." + sNum.substr(PosValido + 2)
                    iNumAux1 = parseFloat(sNumAux1)
                    iNumAux2 = Math.round(parseFloat(sNumAux2))

                    if (iNumAux1 != 0) {
                        Fin = 1

                        if (iNumAux2 == 0) {
                            sNum = sNum.substr(0, PosValido + 2)
                        }
                        else {//Ya se q +1
                            num = parseFloat(sNum.charAt(PosValido + 1)) + 1
                            if (num == 10) {
                                num = parseFloat(sNum.charAt(PosValido)) + 1
                                if (num == 10) {
                                    num = parseFloat(sNum.charAt(PosValido - 1)) + 1
                                    PosValido = PosValido - 1
                                }
                                sNum = sNum.substr(0, PosValido)
                                sNum = sNum + num
                            }
                            else {
                                sNum = sNum.substr(0, PosValido + 1)
                                sNum = sNum + num
                            }
                        }

                        if (PosValido > vprecisionfmt) {
                            sNumAux1 = "0." + sNum.substr(PosValido + 1)
                            iNumAux1 = Math.round(parseFloat(sNumAux1))

                            if (iNumAux1 != 0) {
                                sNum = sNum.substr(0, sNum.length - 2) + "1"
                            }
                        }
                    }
                    else {
                        PosValido = PosValido + 1
                    }
                }
            }
        }
        //--------------------------------------------------	

        return (sNum)
    }

    if (sNum.charAt(0) == "-") {
        bNegativo = true
        sNum = sNum.substr(1, sNum.length)
    }
    rd = /\./
    pDec = sNum.search(rd)

    if (pDec == -1) {
        sInt = sNum
        sDec = ""
    }
    else {
        sInt = sNum.substr(0, pDec)
        sDec = sNum.substr(pDec + 1, 20)
        if (sDec.substr(sDec.length - 4, 4) == "999" + sDec.substr(sDec.length - 1, 1)) {
            var lDigito = 10 - parseFloat(sDec.substr(sDec.length - 1, 1))
            var sSum = "0."
            for (j = 1; j < sDec.length; j++)
                sSum += "0"
            sSum += lDigito.toString()


            sNum = (parseFloat(sInt + "." + sDec) + parseFloat(sSum)).toString()
            pDec = sNum.search(rd)

            if (pDec == -1) {
                sInt = sNum
                sDec = ""
            }
            else {
                sInt = sNum.substr(0, pDec)
                sDec = sNum.substr(pDec + 1, vprecisionfmt)
            }
        }
    }

    if (sInt.length > 3) {
        newValor = ""
        s = 0
        //los posicionamos correctamente
        for (i = sInt.length - 1; i >= 0; i--) {
            if (s == 3) {
                newValor = vthousanfmt + newValor
                s = 0
            }
            newValor = sInt.charAt(i) + newValor
            s++
        }
        sInt = newValor
    }

    //Añadir ceros al final del control hasta el total de decimales.
    //Cuando el valor que llega es 99 el control puede editar decimales pero no mostrar ceros hasta 99
    if (sDec.length < vprecisionfmt && vprecisionfmt != 99) {
        var zeros = '';
        for (var i = 1; i <= (vprecisionfmt - sDec.length); i++) {
            zeros += '0';
        }
        sNum = sInt + vdecimalfmt + sDec + zeros;
    }
    else {
        if (vprecisionfmt > 0 && sDec != '') {
            sNum = sInt + vdecimalfmt + sDec;
        }
        else {
            sNum = sInt;
        }
    }

    if (bNegativo == true)
        sNum = "-" + sNum

    var vprec = vprecisionfmt
    while (sNum == "0" && num != 0) {
        vprec++
        sNum = num2str(num, vdecimalfmt, vthousanfmt, vprec)
    }

    return (sNum)
}

//FunciÃ³n que aÃ±ade el separador de miles cuando el numero sea mayor o igual a 1000 y quita los ceros a la izquierda
function addThousandSeparator(elemento, vdecimalfmt, vthousanfmt, vprecisionfmt, evento) {
    var thousandRegex = new RegExp('\\' + vthousanfmt, "g");
    var textInteger = elemento.value.substr(0, (elemento.value.indexOf(vdecimalfmt) != -1) ? elemento.value.indexOf(vdecimalfmt) : elemento.value.length);
    var textInteger = textInteger.replace(thousandRegex, "");
    var textDecimal = elemento.value.slice((elemento.value.indexOf(vdecimalfmt) != -1) ? elemento.value.indexOf(vdecimalfmt) : elemento.value.length);
    if (textInteger.length == 0) {
        textInteger = '0';        
    }
    else {
        if (textInteger.length > 1) {
            while (textInteger.substr(0, 1) == '0') {
                textInteger = textInteger.slice(1);
            }
        }
    }
    if (textDecimal.length == 0 || textDecimal == vdecimalfmt) {
        //AÃ±adir ceros al final del control hasta el total de decimales.
        //Cuando el valor que llega es 99 el control puede editar decimales pero no mostrar ceros hasta 99
        if (vprecisionfmt > 0 && vprecisionfmt != 99) {
            var zeros = '';
            for (var i = 1; i <= vprecisionfmt; i++) {
                zeros += '0';
            }
            textDecimal = vdecimalfmt + zeros;
        }
        else {
            if (textDecimal == vdecimalfmt) {
                textDecimal = '';
            }
        }
    }
    if (textInteger.length > 3) {
        for (i = textInteger.length - 3; i > 0; i -= 3) {
            textInteger = textInteger.substr(0, i) + vthousanfmt + textInteger.slice(i);
        }
    }
    elemento.value = textInteger + textDecimal;
}

//evita el tecleo de caracteres incorrectos cuando se introduce un número
//se debe incluir en el onkeypress del input que se quiera limitar
function mascaraNumero(elemento, vdecimalfmt, vthousanfmt, evento) {
    var CharKeyCode;
    if (!evento.charCode) { //IE
        CharKeyCode = evento.keyCode;
    }
    else {//Firefox
        CharKeyCode = evento.charCode;
    }

    if (CharKeyCode == vdecimalfmt.charCodeAt(0) || (CharKeyCode >= 48 && CharKeyCode <= 57) || CharKeyCode == 45) {

        //Cuando el carácter introducido es "-" vamos a controlar que sólo se pueda introducir cuando es el primer carácter en el control
        if (CharKeyCode == 45) {
            var CharPosition = doGetCaretPosition(elemento);
            if (CharPosition == 0) {
                return 1;
            }
            else {
                return 0;
            }
        }
        //El decimal sólo se puede introducir una vez
        if (CharKeyCode == vdecimalfmt.charCodeAt(0)) {
            var CharPosition = doGetCaretPosition(elemento);
            var CharKey = String.fromCharCode(CharKeyCode);
            var selectedTextLength = doGetSelectionLength(elemento);
            var texto = elemento.value.substring(0, CharPosition) + CharKey + elemento.value.slice(CharPosition + selectedTextLength);
            var regEx = new RegExp('\\' + vdecimalfmt, "g");
            //var regEx = '/\\' + vdecimalfmt + '/g';
            var arrElementos = texto.match(regEx);
            if (arrElementos != null) {
                if (arrElementos.length > 1) {
                    return 0;
                }
                else {
                    return 1;
                }
            }
        }
        return 1;
    }
    else {
        if (CharKeyCode == 9 || CharKeyCode == 8 || CharKeyCode == 46 || (CharKeyCode >= 35 && CharKeyCode <= 40)) {
            //Si pulsas
            //  tabulador -> 9
            //  Delete/Retroceso -> 8
            //  Supr -> 46
            //  Flechas -> 37 a 40
            //  Inicio -> 36
            // Fin -> 35
            return 2;
        }
        else {
            return 0;
        }
    }
}

var eventoPaste = false;
//Función que 
//1. Impide escribir caracteres no numéricos
//2. Limita el número de decimales a los propios de cada unidad en los controles textbox
//oEdit->El control
//evento->El evento
//vdecimalfmt -> Separador decimales
//vthousanfmt -> Separador miles
function limiteDecimalesTextBox(elemento, evento, vdecimalfmt, vthousanfmt, vprecisionfmt, eventPaste) {
    var mascaraOK = 1;
    if (eventPaste == true) {
        var textoPegado = window.clipboardData.getData('Text');
        var CharPosition = doGetCaretPosition(elemento);
        var selectedTextLength = doGetSelectionLength(elemento);
        var texto = elemento.value.substring(0, CharPosition) + textoPegado + elemento.value.slice(CharPosition + selectedTextLength);

        //Sólo separador de decimales, un guión y números
        //El separador de miles no lo permitimos, para evitar confusiones
        //var regEx = new RegExp('[^\\d^\\' + vdecimalfmt + '^\\' + vthousanfmt + '^\\-]', 'g');
        var regEx = new RegExp('[^\\d^\\' + vdecimalfmt + '^\\' + '^\\-]', 'g');
        if (!allowInString(texto, regEx)) {
            elemento.focus();
            mascaraOK = 0;
        }

        //Guion sólo una vez y el primer carácter
        var regEx = new RegExp('\\-', "g");
        var arrElementos = texto.match(regEx);
        if (arrElementos != null) {
            if (arrElementos.length > 1) {
                mascaraOK = 0;
            }
            else {
                if (texto.indexOf('-') >= 0) {
                    mascaraOK = 0;
                }
            }
        }
        //Decimal sólo una vez
        var regEx = new RegExp('\\' + vdecimalfmt, "g");
        var arrElementos = texto.match(regEx);
        if (arrElementos != null) {
            if (arrElementos.length > 1) {
                mascaraOK = 0;
            }
        }

    }
    else {
        //Impide escribir caracteres no numéricos
        mascaraOK = mascaraNumero(elemento, vdecimalfmt, vthousanfmt, evento);

        //Limita el número de decimales a los propios de cada unidad en los controles textbox
        var CharKeyCode;
        if (!evento.charCode) { //IE
            CharKeyCode = String.fromCharCode(evento.keyCode);
        }
        else {//Firefox
            CharKeyCode = String.fromCharCode(evento.charCode);
        }
        var CharPosition = doGetCaretPosition(elemento);
        var selectedTextLength = doGetSelectionLength(elemento);
        //El texto se compone de:
        // Lo que contiene el textbox
        // Más lo que escribe el usuario en el keypress (ubicado en el punto donde corresponda)
        // Menos lo que se borrar al hacer el keypress (si había texto seleccionado)
        //Rdo:
        var texto = elemento.value.substring(0, CharPosition) + CharKeyCode + elemento.value.slice(CharPosition + selectedTextLength);
    }
    if (mascaraOK != 1) {
        if (mascaraOK == 0) {
            return false;
        }
        else {//En el caso de pulsar Tabulador, flechas, suprimir, delete (mascaraOK=2) y cualquier otra excepción que queramos poner
            return true;
        }
    }
    else {
        //LIMITE DE DECIMALES
        var avisoMaxDecimales = elemento.getAttribute("avisoDecimales");
        if (vprecisionfmt != null && vprecisionfmt >= 0 && vdecimalfmt != null && texto != '') {
            if (texto.indexOf(vdecimalfmt) >= 0) {
                var textoEntero = texto.substr(0, (texto.indexOf(vdecimalfmt) != -1) ? texto.indexOf(vdecimalfmt) : texto.length);
                var textoDecimales = texto.slice(texto.indexOf(vdecimalfmt) + 1);
                if (textoDecimales.length > vprecisionfmt) {
                    //textoDecimales = textoDecimales.substr(0, vprecisionfmt);
                    //elemento.value = textoEntero + vdecimalfmt + textoDecimales;
                    if (eventPaste == true) {
                        alert(avisoMaxDecimales + ' ' + vprecisionfmt);
                    }
                    return false;
                }
                else {
                    return true;
                }
            }
        }
        eventoPaste = false;
        return true;
    }
}

//Validado del paste
function validarpastenum2(elemento, evento, vdecimalfmt, vthousanfmt, vprecisionfmt) {
    var eventPaste = true;
    return limiteDecimalesTextBox(elemento, evento, vdecimalfmt, vthousanfmt, vprecisionfmt, true)
}

//Función que devuelve la posición del cursor en el control
function doGetCaretPosition(ctrl) {
    var CaretPos = 0;
    // IE Support
    if (document.selection) {

        ctrl.focus();
        var Sel = document.selection.createRange();
        var SelLength = document.selection.createRange().text.length;
        Sel.moveStart('character', -ctrl.value.length);
        CaretPos = Sel.text.length - SelLength;
    }
    // Firefox support
    else if (ctrl.selectionStart || ctrl.selectionStart == '0')
        CaretPos = ctrl.selectionStart;

    return (CaretPos);

}

//Función que devuelve el número de elementos seleccionados
function doGetSelectionLength(ctrl) {
    var SelLength = 0;
    // IE Support
    if (document.selection) {

        ctrl.focus();
        var SelLength = document.selection.createRange().text.length;
    }
    // Firefox support
    else if (ctrl.selectionStart || ctrl.selectionStart == '0')
        SelLength = (ctrl.selectionEnd - ctrl.selectionStart);

    return (SelLength);

}


//Función que comprueba que el número tiene el formato adecuado
//Si es así y si la función se está aplicando a un control precio o cantidad de una línea de pedido, actualiza los datos en pantalla de precio total de la línea y de precio total a nivel de orden
//elemento = Control modificado y que ha lanzado el evento.
//vdecimalfmt = Separador de decimales
//vthousanfmt = Separador de miles
//vprecisionfmt = Número de decimales del control que ha lanzado el evento
//minimo = valor mínimo que puede tomar el control
//maximo = valor máximo que puede tomar el control
//errMsg = Mensaje a mostrar en caso de que el formato del texto del control no sea correcto
//errLimites = Mensaje a mostrar en caso de que el valor del control esté fuera de los límites mínimo y máximo.
//Controles que sólo tienen valor si el evento lo lanza un control precio o cantidad de una línea de pedido:
//	tipoControl = En caso de tener que actualizar los valores de los importes de línea y orden, nos dice si el control modificado es de Cantidad o de precio
//		Si va vacío es que se ha modificado un control que no corresponde a una línea de pedido, por lo que no se modificarán los importes de ninguna línea ni orden
//	LblTotLinID = Id del control que muestra el importe total de la línea.
//	LblPrecPorProveID = Id del control que muestra el importe total de la orden
//	controlPrecioCantidadID = Si el control que ha lanzado el evento era el de cantidad, este valor será el Id del control Precio de la misma línea.
//							  Si el control que ha lanzado el evento era el de precio, será el de cantidad.
//	LblPrecArtID = Id del control del label que muestra el precio. Es necesario usarlo si el control Precio va oculto.
//	moneda = Código de Moneda a mostrar
//  decimalesPrecio = Decimales de los valores Precio (determinados por la configuración del usuario). Cuando el control a modificar sea de tipo precio coincidirán con vprecisionfmt
function validarNumero(elemento, vdecimalfmt, vthousanfmt, vprecisionfmt, minimo, maximo, errMsg, errLimites, tipoControl, LblTotLinID, LblPrecPorProveID, controlPrecioCantidadID, LblPrecArtID, moneda, decimalesPrecio) {
    var lminimo;
    var lmaximo;
    lminimo = minimo;
    lmaximo = maximo;
    var rdo = false;
    if (!elemento.value) {
        rdo = true;
    }
    valor = elemento.value;
    if (valor.length == 0) {
        rdo = true;
    }

    if (rdo != true) {
        //En el número sólo puede haber numeros, separador de decimales, separador de miles o el signo '-'
        var regEx = new RegExp('[^\\d^\\' + vdecimalfmt + '^\\' + vthousanfmt + '^\\-]', 'g');
        if (!allowInString(valor, regEx)) {

            //"Número incorrecto.\nSeparador decimal: " + vdecimalfmt + "\nSeparador de miles: " + vthousanfmt
            if (errMsg) {
                alert(errMsg);
            }
            elemento.focus();
            rdo = false;
        }
        
        //Pasamos el valor de string a número para poder comparar con el mínimo y el máximo
        var miValor = new Number;
        miValor = str2num(valor, vdecimalfmt, vthousanfmt);

        if (lminimo == null)
            lminimo = miValor;
        if (lmaximo == null)
            lmaximo = miValor;
        if (lminimo > miValor || lmaximo < miValor) {
            if (errLimites)
                alert(errLimites);
            elemento.focus();
            rdo = false;
        }
        else {
            rdo = true;
        }
    }
    //Si el rdo es true, llamamos al webservice para hacer los cambios. Si no, no.
    if (rdo == true) {
        if (tipoControl == "Cantidad") {
            var Precio = $get(controlPrecioCantidadID);

            var LblTotLin = $get(LblTotLinID);
            var LblTotLin_DblValue = $get(LblTotLinID + '_DblValue');
            var precioLineaAnterior;
            if (LblTotLin_DblValue != null) {
                //Controlamos el tipo de nodo de origen porque el medio para recuperar su valor es diferente
                if (LblTotLin_DblValue.nodeName == 'INPUT')
                    precioLineaAnterior = str2num4DBlValue(LblTotLin_DblValue.value);
                else
                    precioLineaAnterior = str2num4DBlValue(LblTotLin_DblValue.innerHTML);
            }
            else {
                precioLineaAnterior = str2num(LblTotLin.innerText.replace(' ' + moneda, ''), vdecimalfmt, vthousanfmt);
            }
            var LblPrecPorProve = $get(LblPrecPorProveID);
            var LblPrecPorProve_DblValue = $get(LblPrecPorProveID + '_DblValue');
            var precPorProveAnterior;
            if (LblPrecPorProve_DblValue != null) {
                //Controlamos el tipo de nodo de origen porque el medio para recuperar su valor es diferente
                if (LblPrecPorProve_DblValue.nodeName == 'INPUT')
                    precPorProveAnterior = str2num4DBlValue(LblPrecPorProve_DblValue.value);
                else
                    precPorProveAnterior = str2num4DBlValue(LblPrecPorProve_DblValue.innerHTML);
            }
            else {
                precPorProveAnterior = str2num(LblPrecPorProve.innerText.replace(' ' + moneda, ''), vdecimalfmt, vthousanfmt);
            }

            //Precio y cantidad no tienen por qué tener el mismo número de decimales. vprecisionfmt es este caso es el número de decimales de la cantidad pero vamos a modificar el precio por lo que vamosd a ver cuántos tiene
            //var PrecioLinea = str2num(Precio.value, vdecimalfmt, vthousanfmt);
            var PrecioLinea = str2num4DBlValue(Precio.value);
            var CantidadLinea = miValor;
            //Establecemos el nuevo valor
            var Cantidad_DblValue = $get(elemento.id + '_DblValue');
            ////Controlamos el tipo de nodo de origen porque el medio para recuperar su valor es diferente
            if (Cantidad_DblValue.nodeName == 'INPUT')
                Cantidad_DblValue.value = num2str4DBlValue(miValor);
            else
                Cantidad_DblValue.innerHTML = num2str4DBlValue(miValor);
            var precioLineaNuevo = PrecioLinea.multiply(CantidadLinea);
            var precPorProveNuevo = precPorProveAnterior.subtract(precioLineaAnterior).add(precioLineaNuevo);
            LblTotLin.innerText = num2str(precioLineaNuevo, vdecimalfmt, vthousanfmt, decimalesPrecio) + ' ' + moneda;
            LblPrecPorProve.innerText = num2str(precPorProveNuevo, vdecimalfmt, vthousanfmt, decimalesPrecio) + ' ' + moneda;
            if (LblPrecPorProve_DblValue != null) {
                ////Controlamos el tipo de nodo de origen porque el medio para recuperar su valor es diferente
                if (LblPrecPorProve_DblValue.nodeName == 'INPUT')
                    LblPrecPorProve_DblValue.value = num2str4DBlValue(precPorProveNuevo);
                else
                    LblPrecPorProve_DblValue.innerHTML = num2str4DBlValue(precPorProveNuevo);
            }
            if (LblTotLin_DblValue != null) {
                ////Controlamos el tipo de nodo de origen porque el medio para recuperar su valor es diferente
                if (LblTotLin_DblValue.nodeName == 'INPUT')
                    LblTotLin_DblValue.value = num2str4DBlValue(precioLineaNuevo);
                else
                    LblTotLin_DblValue.innerHTML = num2str4DBlValue(precioLineaNuevo);
            }
        }
        else if (tipoControl == "Precio") {
            var Cantidad = $get(controlPrecioCantidadID);

            var LblTotLin = $get(LblTotLinID);
            var LblTotLin_DblValue = $get(LblTotLinID + '_DblValue');
            var precioLineaAnterior;
            if (LblTotLin_DblValue != null) {
                ////Controlamos el tipo de nodo de origen porque el medio para recuperar su valor es diferente
                if (LblTotLin_DblValue.nodeName == 'INPUT')
                    precioLineaAnterior = str2num4DBlValue(LblTotLin_DblValue.value);
                else
                    precioLineaAnterior = str2num4DBlValue(LblTotLin_DblValue.innerHTML);
            }
            else {
                precioLineaAnterior = str2num(LblTotLin.innerText.replace(' ' + moneda, ''), vdecimalfmt, vthousanfmt);
            }
            var LblPrecPorProve = $get(LblPrecPorProveID);
            var LblPrecPorProve_DblValue = $get(LblPrecPorProveID + '_DblValue');
            var precPorProveAnterior;
            if (LblPrecPorProve_DblValue != null) {
                ////Controlamos el tipo de nodo de origen porque el medio para recuperar su valor es diferente
                if (LblPrecPorProve_DblValue.nodeName == 'INPUT')
                    precPorProveAnterior = str2num4DBlValue(LblPrecPorProve_DblValue.value);
                else
                    precPorProveAnterior = str2num4DBlValue(LblPrecPorProve_DblValue.innerHTML);
            }
            else {
                precPorProveAnterior = str2num(LblPrecPorProve.innerText.replace(' ' + moneda, ''), vdecimalfmt, vthousanfmt);
            }

            var CantidadLinea = str2num(Cantidad.value, vdecimalfmt, vthousanfmt);
            var PrecioLinea = miValor;
            //Establecemos el nuevo valor
            var Precio_DblValue = $get(elemento.id + '_DblValue');
            ////Controlamos el tipo de nodo de origen porque el medio para recuperar su valor es diferente
            if (Precio_DblValue.nodeName == 'INPUT')
                Precio_DblValue.value = num2str4DBlValue(miValor);
            else
                Precio_DblValue.innerHTML = num2str4DBlValue(miValor);
            var precioLineaNuevo = PrecioLinea.multiply(CantidadLinea);
            var precPorProveNuevo = precPorProveAnterior.subtract(precioLineaAnterior).add(precioLineaNuevo);
            LblTotLin.innerText = num2str(precioLineaNuevo, vdecimalfmt, vthousanfmt, vprecisionfmt) + ' ' + moneda;
            LblPrecPorProve.innerText = num2str(precPorProveNuevo, vdecimalfmt, vthousanfmt, vprecisionfmt) + ' ' + moneda;
            var LblPrecPorProve_DblValue = $get(LblPrecPorProveID + '_DblValue');
            if (LblPrecPorProve_DblValue != null) {
                ////Controlamos el tipo de nodo de origen porque el medio para recuperar su valor es diferente
                if (LblPrecPorProve_DblValue.nodeName == 'INPUT')
                    LblPrecPorProve_DblValue.value = num2str4DBlValue(precPorProveNuevo);
                else
                    LblPrecPorProve_DblValue.innerHTML = num2str4DBlValue(precPorProveNuevo);
            }
            if (LblTotLin_DblValue != null) {
                ////Controlamos el tipo de nodo de origen porque el medio para recuperar su valor es diferente
                if (LblTotLin_DblValue.nodeName == 'INPUT')
                    LblTotLin_DblValue.value = num2str4DBlValue(precioLineaNuevo);
                else
                    LblTotLin_DblValue.innerHTML = num2str4DBlValue(precioLineaNuevo);
            }
        }
        else {
            if (tipoControl == "Importe") {
                //var Cantidad = 1;

                var LblTotLin = $get(LblTotLinID);
                var LblTotLin_DblValue = $get(LblPrecArtID + '_DblValue');
                var precioLineaAnterior;
                if (LblTotLin_DblValue != null) {
                    ////Controlamos el tipo de nodo de origen porque el medio para recuperar su valor es diferente
                    if (LblTotLin_DblValue.nodeName == 'INPUT')
                        precioLineaAnterior = str2num4DBlValue(LblTotLin_DblValue.value);
                    else
                        precioLineaAnterior = str2num4DBlValue(LblTotLin_DblValue.innerHTML);
                }
                else {
                    precioLineaAnterior = str2num(LblTotLin.innerText.replace(' ' + moneda, ''), vdecimalfmt, vthousanfmt);
                }
                var LblPrecPorProve = $get(LblPrecPorProveID);
                var LblPrecPorProve_DblValue = $get(LblPrecPorProveID + '_DblValue');
                var precPorProveAnterior;
                if (LblPrecPorProve_DblValue != null) {
                    ////Controlamos el tipo de nodo de origen porque el medio para recuperar su valor es diferente
                    if (LblPrecPorProve_DblValue.nodeName == 'INPUT')
                        precPorProveAnterior = str2num4DBlValue(LblPrecPorProve_DblValue.value);
                    else
                        precPorProveAnterior = str2num4DBlValue(LblPrecPorProve_DblValue.innerHTML);
                }
                else {
                    precPorProveAnterior = str2num(LblPrecPorProve.innerText.replace(' ' + moneda, ''), vdecimalfmt, vthousanfmt);
                }

                var CantidadLinea = str2num("1", vdecimalfmt, vthousanfmt);
                var PrecioLinea = miValor;
                //Establecemos el nuevo valor
                var Precio_DblValue = $get(LblPrecArtID + '_DblValue');
                ////Controlamos el tipo de nodo de origen porque el medio para recuperar su valor es diferente
                if (Precio_DblValue.nodeName == 'INPUT')
                    Precio_DblValue.value = num2str4DBlValue(miValor);
                else
                    Precio_DblValue.innerHTML = num2str4DBlValue(miValor);
                var precioLineaNuevo = PrecioLinea.multiply(CantidadLinea);
                var precPorProveNuevo = precPorProveAnterior.subtract(precioLineaAnterior).add(precioLineaNuevo);
                LblTotLin.innerText = num2str(precioLineaNuevo, vdecimalfmt, vthousanfmt, vprecisionfmt);
                LblPrecPorProve.innerText = num2str(precPorProveNuevo, vdecimalfmt, vthousanfmt, vprecisionfmt) + ' ' + moneda;
                var LblPrecPorProve_DblValue = $get(LblPrecPorProveID + '_DblValue');
                if (LblPrecPorProve_DblValue != null) {
                    ////Controlamos el tipo de nodo de origen porque el medio para recuperar su valor es diferente
                    if (LblPrecPorProve_DblValue.nodeName == 'INPUT')
                        LblPrecPorProve_DblValue.value = num2str4DBlValue(precPorProveNuevo);
                    else
                        LblPrecPorProve_DblValue.innerHTML = num2str4DBlValue(precPorProveNuevo);
                }
                if (LblTotLin_DblValue != null) {
                    ////Controlamos el tipo de nodo de origen porque el medio para recuperar su valor es diferente
                    if (LblTotLin_DblValue.nodeName == 'INPUT')
                        LblTotLin_DblValue.value = num2str4DBlValue(precioLineaNuevo);
                    else
                        LblTotLin_DblValue.innerHTML = num2str4DBlValue(precioLineaNuevo);
                } 
            }
            else {
                //Establecemos el nuevo valor
                var Control_DblValue = $get(elemento.id + '_DblValue');
                if (Control_DblValue.nodeName == 'INPUT')
                    Control_DblValue.value = num2str4DBlValue(miValor);
                else
                    Control_DblValue.innerHTML = num2str4DBlValue(miValor);
            }
        }
        return true;
    }
}



//Función que comprueba que el número tiene el formato adecuado
//Si es así y si la función se está aplicando a un control precio o cantidad de una línea de pedido, actualiza los datos en pantalla de precio total de la línea y de precio total a nivel de orden
//elemento = Control modificado y que ha lanzado el evento.
//vdecimalfmt = Separador de decimales
//vthousanfmt = Separador de miles
//vprecisionfmt = Número de decimales del control que ha lanzado el evento
//minimo = valor mínimo que puede tomar el control
//maximo = valor máximo que puede tomar el control
//errMsg = Mensaje a mostrar en caso de que el formato del texto del control no sea correcto
//errLimites = Mensaje a mostrar en caso de que el valor del control esté fuera de los límites mínimo y máximo.
//Controles que sólo tienen valor si el evento lo lanza un control precio o cantidad de una línea de pedido:
//	tipoControl = En caso de tener que actualizar los valores de los importes de línea y orden, nos dice si el control modificado es de Cantidad o de precio
//		Si va vacío es que se ha modificado un control que no corresponde a una línea de pedido, por lo que no se modificarán los importes de ninguna línea ni orden
//	LblTotLinID = Id del control que muestra el importe total de la línea.
//	LblPrecPorProveID = Id del control que muestra el importe total de la orden
//	controlPrecioCantidadID = Si el control que ha lanzado el evento era el de cantidad, este valor será el Id del control Precio de la misma línea.
//							  Si el control que ha lanzado el evento era el de precio, será el de cantidad.
//	LblPrecArtID = Id del control del label que muestra el precio. Es necesario usarlo si el control Precio va oculto.
//	moneda = Código de Moneda a mostrar
//  decimalesPrecio = Decimales de los valores Precio (determinados por la configuración del usuario). Cuando el control a modificar sea de tipo precio coincidirán con vprecisionfmt
function validarNumeroCesta(elemento, vdecimalfmt, vthousanfmt, vprecisionfmt, minimo, maximo, errMsg, errLimites, tipoControl, LblTotLinID, LblPrecPorProveID, controlPrecioCantidadID, LblPrecArtID, moneda, decimalesPrecio, LblTotalCestaID) {
    var lminimo;
    var lmaximo;

    lminimo = minimo;
    lmaximo = maximo;
    var rdo = false;
    if (!elemento.value) {
        rdo = true;
    }
    valor = elemento.value;
    if (valor.length == 0) {
        rdo = true;
    }

    if (rdo != true) {
        //En el número sólo puede haber numeros, separador de decimales, separador de miles o el signo '-'
        var regEx = new RegExp('[^\\d^\\' + vdecimalfmt + '^\\' + vthousanfmt + '^\\-]', 'g');
        if (!allowInString(valor, regEx)) {

            //"Número incorrecto.\nSeparador decimal: " + vdecimalfmt + "\nSeparador de miles: " + vthousanfmt
            if (errMsg) {
                alert(errMsg);
            }
            elemento.focus();
            rdo = false;
        }

        //Pasamos el valor de string a número para poder comparar con el mínimo y el máximo
        var miValor = new Number;
        miValor = str2num(valor, vdecimalfmt, vthousanfmt);

        if (lminimo == null)
            lminimo = miValor;
        if (lmaximo == null)
            lmaximo = miValor;
        if (lminimo > miValor || lmaximo < miValor) {
            if (errLimites)
                alert(errLimites);
            elemento.focus();
            rdo = false;
        }
        else {
            rdo = true;
        }
    }

    //Si el rdo es true, llamamos al webservice para hacer los cambios. Si no, no.
    if (rdo == true) {
        if (tipoControl == "Cantidad") {
            var Precio = $get(controlPrecioCantidadID);

            var LblTotLin = $get(LblTotLinID);
            var LblTotLin_DblValue = $get(LblTotLinID + '_DblValue');
            var precioLineaAnterior;
            if (LblTotLin_DblValue != null) {
                //Controlamos el tipo de nodo de origen porque el medio para recuperar su valor es diferente
                if (LblTotLin_DblValue.nodeName == 'INPUT')
                    precioLineaAnterior = str2num4DBlValue(LblTotLin_DblValue.value);
                else
                    precioLineaAnterior = str2num4DBlValue(LblTotLin_DblValue.innerHTML);
            }
            else {
                precioLineaAnterior = str2num(LblTotLin.innerText.replace(' ' + moneda, ''), vdecimalfmt, vthousanfmt);
            }

            var LblPrecPorProve = $get(LblPrecPorProveID);
            var LblPrecPorProve_DblValue = $get(LblPrecPorProveID + '_DblValue');
            var precPorProveAnterior;
            if (LblPrecPorProve_DblValue != null) {
                //Controlamos el tipo de nodo de origen porque el medio para recuperar su valor es diferente
                if (LblPrecPorProve_DblValue.nodeName == 'INPUT')
                    precPorProveAnterior = str2num4DBlValue(LblPrecPorProve_DblValue.value);
                else
                    precPorProveAnterior = str2num4DBlValue(LblPrecPorProve_DblValue.innerHTML);
            }
            else {
                precPorProveAnterior = str2num(LblPrecPorProve.innerText.replace(' ' + moneda, ''), vdecimalfmt, vthousanfmt);
            }

            //Precio y cantidad no tienen por qué tener el mismo número de decimales. vprecisionfmt es este caso es el número de decimales de la cantidad pero vamos a modificar el precio por lo que vamosd a ver cuántos tiene
            //var PrecioLinea = str2num(Precio.value, vdecimalfmt, vthousanfmt);
            var PrecioLinea = str2num4DBlValue(Precio.value);
            var CantidadLinea = miValor;
            //Establecemos el nuevo valor
            var Cantidad_DblValue = $get(elemento.id + '_DblValue');
            ////Controlamos el tipo de nodo de origen porque el medio para recuperar su valor es diferente
            if (Cantidad_DblValue.nodeName == 'INPUT')
                Cantidad_DblValue.value = num2str4DBlValue(miValor);
            else
                Cantidad_DblValue.innerHTML = num2str4DBlValue(miValor);
            var precioLineaNuevo = PrecioLinea.multiply(CantidadLinea);
            var precPorProveNuevo = precPorProveAnterior.subtract(precioLineaAnterior).add(precioLineaNuevo);
            LblTotLin.innerText = num2str(precioLineaNuevo, vdecimalfmt, vthousanfmt, decimalesPrecio) + ' ' + moneda;
            LblPrecPorProve.innerText = num2str(precPorProveNuevo, vdecimalfmt, vthousanfmt, decimalesPrecio) + ' ' + moneda;
            if (LblPrecPorProve_DblValue != null) {
                ////Controlamos el tipo de nodo de origen porque el medio para recuperar su valor es diferente
                if (LblPrecPorProve_DblValue.nodeName == 'INPUT')
                    LblPrecPorProve_DblValue.value = num2str4DBlValue(precPorProveNuevo);
                else
                    LblPrecPorProve_DblValue.innerHTML = num2str4DBlValue(precPorProveNuevo);
            }
            if (LblTotLin_DblValue != null) {
                ////Controlamos el tipo de nodo de origen porque el medio para recuperar su valor es diferente
                if (LblTotLin_DblValue.nodeName == 'INPUT')
                    LblTotLin_DblValue.value = num2str4DBlValue(precioLineaNuevo);
                else
                    LblTotLin_DblValue.innerHTML = num2str4DBlValue(precioLineaNuevo);
            }

            var PrecioCesta = $get(LblTotalCestaID);
            var PrecioCestaNuevo;
            var PrecioCestaValue = str2num(PrecioCesta.innerText.replace(' ' + moneda, ''), vdecimalfmt, vthousanfmt);
            PrecioCestaNuevo = PrecioCestaValue.subtract(precPorProveAnterior).add(precPorProveNuevo);
            PrecioCesta.innerText = num2str(PrecioCestaNuevo, vdecimalfmt, vthousanfmt, decimalesPrecio) + ' ' + moneda;
        }
        else if (tipoControl == "Precio") {
            var Cantidad = $get(controlPrecioCantidadID);

            var LblTotLin = $get(LblTotLinID);
            var LblTotLin_DblValue = $get(LblTotLinID + '_DblValue');
            var precioLineaAnterior;
            if (LblTotLin_DblValue != null) {
                ////Controlamos el tipo de nodo de origen porque el medio para recuperar su valor es diferente
                if (LblTotLin_DblValue.nodeName == 'INPUT')
                    precioLineaAnterior = str2num4DBlValue(LblTotLin_DblValue.value);
                else
                    precioLineaAnterior = str2num4DBlValue(LblTotLin_DblValue.innerHTML);
            }
            else {
                precioLineaAnterior = str2num(LblTotLin.innerText.replace(' ' + moneda, ''), vdecimalfmt, vthousanfmt);
            }
            var LblPrecPorProve = $get(LblPrecPorProveID);
            var LblPrecPorProve_DblValue = $get(LblPrecPorProveID + '_DblValue');
            var precPorProveAnterior;
            if (LblPrecPorProve_DblValue != null) {
                ////Controlamos el tipo de nodo de origen porque el medio para recuperar su valor es diferente
                if (LblPrecPorProve_DblValue.nodeName == 'INPUT')
                    precPorProveAnterior = str2num4DBlValue(LblPrecPorProve_DblValue.value);
                else
                    precPorProveAnterior = str2num4DBlValue(LblPrecPorProve_DblValue.innerHTML);
            }
            else {
                precPorProveAnterior = str2num(LblPrecPorProve.innerText.replace(' ' + moneda, ''), vdecimalfmt, vthousanfmt);
            }

            var CantidadLinea = str2num(Cantidad.value, vdecimalfmt, vthousanfmt);
            var PrecioLinea = miValor;
            //Establecemos el nuevo valor
            var Precio_DblValue = $get(elemento.id + '_DblValue');
            ////Controlamos el tipo de nodo de origen porque el medio para recuperar su valor es diferente
            if (Precio_DblValue.nodeName == 'INPUT')
                Precio_DblValue.value = num2str4DBlValue(miValor);
            else
                Precio_DblValue.innerHTML = num2str4DBlValue(miValor);
            var precioLineaNuevo = PrecioLinea.multiply(CantidadLinea);
            var precPorProveNuevo = precPorProveAnterior.subtract(precioLineaAnterior).add(precioLineaNuevo);
            LblTotLin.innerText = num2str(precioLineaNuevo, vdecimalfmt, vthousanfmt, vprecisionfmt) + ' ' + moneda;
            LblPrecPorProve.innerText = num2str(precPorProveNuevo, vdecimalfmt, vthousanfmt, vprecisionfmt) + ' ' + moneda;
            var LblPrecPorProve_DblValue = $get(LblPrecPorProveID + '_DblValue');
            if (LblPrecPorProve_DblValue != null) {
                ////Controlamos el tipo de nodo de origen porque el medio para recuperar su valor es diferente
                if (LblPrecPorProve_DblValue.nodeName == 'INPUT')
                    LblPrecPorProve_DblValue.value = num2str4DBlValue(precPorProveNuevo);
                else
                    LblPrecPorProve_DblValue.innerHTML = num2str4DBlValue(precPorProveNuevo);
            }
            if (LblTotLin_DblValue != null) {
                ////Controlamos el tipo de nodo de origen porque el medio para recuperar su valor es diferente
                if (LblTotLin_DblValue.nodeName == 'INPUT')
                    LblTotLin_DblValue.value = num2str4DBlValue(precioLineaNuevo);
                else
                    LblTotLin_DblValue.innerHTML = num2str4DBlValue(precioLineaNuevo);
            }
        }
        else {
            //Establecemos el nuevo valor
            var Control_DblValue = $get(elemento.id + '_DblValue');
            if (Control_DblValue.nodeName == 'INPUT')
                Control_DblValue.value = num2str4DBlValue(miValor);
            else
                Control_DblValue.innerHTML = num2str4DBlValue(miValor);
        }
        return true;
    }
}


//Función que comprueba que el número tiene el formato adecuado
//Si es así y si la función se está aplicando a un control precio o cantidad de una línea de pedido, actualiza los datos en pantalla de precio total de la línea y de precio total a nivel de orden
//elemento = Control modificado y que ha lanzado el evento.
//vdecimalfmt = Separador de decimales
//vthousanfmt = Separador de miles
//vprecisionfmt = Número de decimales del control que ha lanzado el evento
//minimo = valor mínimo que puede tomar el control
//maximo = valor máximo que puede tomar el control
//errMsg = Mensaje a mostrar en caso de que el formato del texto del control no sea correcto
//errLimites = Mensaje a mostrar en caso de que el valor del control esté fuera de los límites mínimo y máximo.
//Controles que sólo tienen valor si el evento lo lanza un control precio o cantidad de una línea de pedido:
//	tipoControl = En caso de tener que actualizar los valores de los importes de línea y orden, nos dice si el control modificado es de Cantidad o de precio
//		Si va vacío es que se ha modificado un control que no corresponde a una línea de pedido, por lo que no se modificarán los importes de ninguna línea ni orden
//	LblTotLinID = Id del control que muestra el importe total de la línea.
//	LblPrecPorProveID = Id del control que muestra el importe total de la orden
//	controlPrecioCantidadID = Si el control que ha lanzado el evento era el de cantidad, este valor será el Id del control Precio de la misma línea.
//							  Si el control que ha lanzado el evento era el de precio, será el de cantidad.
//	LblPrecArtID = Id del control del label que muestra el precio. Es necesario usarlo si el control Precio va oculto.
//	moneda = Código de Moneda a mostrar
//  decimalesPrecio = Decimales de los valores Precio (determinados por la configuración del usuario). Cuando el control a modificar sea de tipo precio coincidirán con vprecisionfmt
function validarNumeroImporte(elemento, vdecimalfmt, vthousanfmt, vprecisionfmt, minimo, maximo, errMsg, errLimites, tipoControl, LblTotLinID, LblPrecPorProveID, controlPrecioCantidadID, LblPrecArtID, moneda, decimalesPrecio, precioAnteriorID) {
    var lminimo;
    var lmaximo;
    lminimo = minimo;
    lmaximo = maximo;
    var rdo = false;
    if (!elemento.value) {
        rdo = true;
    }
    valor = elemento.value;
    if (valor.length == 0) {
        rdo = true;
    }

    if (rdo != true) {
        //En el número sólo puede haber numeros, separador de decimales, separador de miles o el signo '-'
        var regEx = new RegExp('[^\\d^\\' + vdecimalfmt + '^\\' + vthousanfmt + '^\\-]', 'g');
        if (!allowInString(valor, regEx)) {

            //"Número incorrecto.\nSeparador decimal: " + vdecimalfmt + "\nSeparador de miles: " + vthousanfmt
            if (errMsg) {
                alert(errMsg);
            }
            elemento.focus();
            rdo = false;
        }
        
        //Pasamos el valor de string a número para poder comparar con el mínimo y el máximo
        var miValor = new Number;
        miValor = str2num(valor, vdecimalfmt, vthousanfmt);

        if (lminimo == null)
            lminimo = miValor;
        if (lmaximo == null)
            lmaximo = miValor;
        if (lminimo > miValor || lmaximo < miValor) {
            if (errLimites)
                alert(errLimites);
            elemento.focus();
            rdo = false;
        }
        else {
            rdo = true;
        }
    }
    //Si el rdo es true, llamamos al webservice para hacer los cambios. Si no, no.
    if (rdo == true) {
        if (tipoControl == "Importe") {
            //var Cantidad = 1;
            var LblTotLin = $get(LblTotLinID);
            var LblTotLin_DblValue = $get(LblPrecArtID + '_DblValue');
            var precioLineaAnterior;
            var lblPrecioLineaAnterior;
            lblPrecioLineaAnterior = $get(precioAnteriorID); //str2num4DBlValue(precioAnterior);
            precioLineaAnterior = str2num4DBlValue(lblPrecioLineaAnterior.value);
            var LblPrecPorProve = $get(LblPrecPorProveID);
            var LblPrecPorProve_DblValue = $get(LblPrecPorProveID + '_DblValue');
            var precPorProveAnterior;
            if (LblPrecPorProve_DblValue != null) {
                ////Controlamos el tipo de nodo de origen porque el medio para recuperar su valor es diferente
                if (LblPrecPorProve_DblValue.nodeName == 'INPUT')
                    precPorProveAnterior = str2num4DBlValue(LblPrecPorProve_DblValue.value);
                else
                    precPorProveAnterior = str2num4DBlValue(LblPrecPorProve_DblValue.innerHTML);
            }
            else {
                precPorProveAnterior = str2num(LblPrecPorProve.innerText.replace(' ' + moneda, ''), vdecimalfmt, vthousanfmt);
            }

            var CantidadLinea = str2num("1", vdecimalfmt, vthousanfmt);
            var PrecioLinea = miValor;
            //Establecemos el nuevo valor
            var Precio_DblValue = $get(LblPrecArtID + '_DblValue');
            ////Controlamos el tipo de nodo de origen porque el medio para recuperar su valor es diferente
            if (Precio_DblValue.nodeName == 'INPUT')
                Precio_DblValue.value = num2str4DBlValue(miValor);
            else
                Precio_DblValue.innerHTML = num2str4DBlValue(miValor);
            var precioLineaNuevo = PrecioLinea.multiply(CantidadLinea);
            var precPorProveNuevo = precPorProveAnterior.subtract(precioLineaAnterior).add(precioLineaNuevo);
            LblTotLin.innerHTML = num2str(precioLineaNuevo, vdecimalfmt, vthousanfmt, vprecisionfmt);
            LblPrecPorProve.innerText = num2str(precPorProveNuevo, vdecimalfmt, vthousanfmt, vprecisionfmt) + ' ' + moneda;
            var LblPrecPorProve_DblValue = $get(LblPrecPorProveID + '_DblValue');
            if (LblPrecPorProve_DblValue != null) {
                ////Controlamos el tipo de nodo de origen porque el medio para recuperar su valor es diferente
                if (LblPrecPorProve_DblValue.nodeName == 'INPUT')
                    LblPrecPorProve_DblValue.value = num2str4DBlValue(precPorProveNuevo);
                else
                    LblPrecPorProve_DblValue.innerHTML = num2str4DBlValue(precPorProveNuevo);
            }
            if (LblTotLin_DblValue != null) {
                ////Controlamos el tipo de nodo de origen porque el medio para recuperar su valor es diferente
                if (LblTotLin_DblValue.nodeName == 'INPUT') {
                    LblTotLin_DblValue.value = num2str4DBlValue(precioLineaNuevo.toString());
                    lblPrecioLineaAnterior.value = num2str4DBlValue(precioLineaNuevo.toString());
                }
                else {
                    LblTotLin_DblValue.innerHTML = num2str4DBlValue(precioLineaNuevo.toString());
                    lblPrecioLineaAnterior.innerHTML = num2str4DBlValue(precioLineaNuevo.toString());
                }
            } 
        }
        else {
            //Establecemos el nuevo valor
            var Control_DblValue = $get(elemento.id + '_DblValue');
            if (Control_DblValue.nodeName == 'INPUT')
                Control_DblValue.value = num2str4DBlValue(miValor);
            else
                Control_DblValue.innerHTML = num2str4DBlValue(miValor);
        }
        return true;
    }
}


//Función que devuelve true si el texto cumple con la expresión regular pasada. Si no, devuelve false
function allowInString(InString, RefRegEx) {
    if (InString.length == 0) return (false);
    if (InString.match(RefRegEx) != null) {
        return false;
    }
    return true;
}


///*''' <summary>
//''' Redondear el numero dado al numero de decimales dado
//''' </summary>
//''' <param name="num">Numero a redondear</param>
//''' <param name="dec">numero de decimales</param>        
//''' <returns>numero redondeado</returns>
//''' <remarks>Llamada desde: formatos.js/num2str; Tiempo máximo:0,01</remarks>*/
function redondea(num, dec) {
    var nTmp = num

    var sNum = nTmp.toString()
    var rd = /\./
    var pDec = sNum.search(rd)
    var rde = /\e/
    var pDecE = sNum.search(rde)

    if (pDec == -1)
        return (num)

    sDec = sNum.substr(pDec + 1, sNum.length)

    if (pDecE == -1) {
        dec = (dec < sDec.length) ? dec : sDec.length
    }
    else { //Encontrado numero tal como 2.5e-8
        return (num)
    }

    for (var i = 0; i < dec; i++) {
        nTmp = nTmp.multiply(10)
    }

    var precision = nTmp.precision;
    nTmp.precision = 0;
    nTmp.roundType = BigNumber.ROUND_HALF_EVEN;
    nTmp.round();
    nTmp.precision = precision;

    for (var i = 0; i < dec; i++)
        nTmp = nTmp.divide(10)

    return (nTmp)
}

//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//+ Jonas Raoni Soares Silva
//@ http://jsfromhell.com/classes/bignumber [rev. #4]

BigNumber = function (n, p, r, rounding) {
    var o = this, i;
    if (n instanceof BigNumber) {
        for (i in { precision: 0, roundType: 0, _s: 0, _f: 0, _rounding: 0 }) o[i] = n[i];
        o._d = n._d.slice();
        return;
    }
    o.precision = isNaN(p = Math.abs(p)) ? BigNumber.defaultPrecision : p;
    o.roundType = isNaN(r = Math.abs(r)) ? BigNumber.defaultRoundType : r;
    o._rounding = (!rounding ? false : rounding);
    o._s = (n += "").charAt(0) == "-";
    o._f = ((n = n.replace(/[^\d.]/g, "").split(".", 2))[0] = n[0].replace(/^0+/, "") || "0").length;
    for (i = (n = o._d = (n.join("") || "0").split("")).length; i; n[--i] = +n[i]);
    o.round();
};
with ({ $: BigNumber, o: BigNumber.prototype }) {
    $.ROUND_HALF_EVEN = ($.ROUND_HALF_DOWN = ($.ROUND_HALF_UP = ($.ROUND_FLOOR = ($.ROUND_CEIL = ($.ROUND_DOWN = ($.ROUND_UP = 0) + 1) + 1) + 1) + 1) + 1) + 1;
    $.defaultPrecision = 40;
    $.defaultRoundType = $.ROUND_HALF_UP;
    o.add = function (n) {
        if (this._s != (n = new BigNumber(n, this.precision, this.roundType, this._rounding))._s)
            return n._s ^= 1, this.subtract(n);
        var o = new BigNumber(this, this.precision, this.roundType, this._rounding), a = o._d, b = n._d, la = o._f,
            lb = n._f, n = Math.max(la, lb), i, r;
        la != lb && ((lb = la - lb) > 0 ? o._zeroes(b, lb, 1) : o._zeroes(a, -lb, 1));
        i = (la = a.length) == (lb = b.length) ? a.length : ((lb = la - lb) > 0 ? o._zeroes(b, lb) : o._zeroes(a, -lb)).length;
        for (r = 0; i; r = (a[--i] = a[i] + b[i] + r) / 10 >>> 0, a[i] %= 10);
        return r && ++n && a.unshift(r), o._f = n, o.round();
    };
    o.subtract = function (n) {
        if (this._s != (n = new BigNumber(n))._s)
            return n._s ^= 1, this.add(n);
        var o = new BigNumber(this), c = o.abs().compare(n.abs()) + 1, a = c ? o : n, b = c ? n : o, la = a._f, lb = b._f, d = la, i, j;
        a = a._d, b = b._d, la != lb && ((lb = la - lb) > 0 ? o._zeroes(b, lb, 1) : o._zeroes(a, -lb, 1));
        for (i = (la = a.length) == (lb = b.length) ? a.length : ((lb = la - lb) > 0 ? o._zeroes(b, lb) : o._zeroes(a, -lb)).length; i; ) {
            if (a[--i] < b[i]) {
                for (j = i; j && !a[--j]; a[j] = 9);
                --a[j], a[i] += 10;
            }
            b[i] = a[i] - b[i];
        }
        return c || (o._s ^= 1), o._f = d, o._d = b, o.round();
    };
    o.multiply = function (n) {
        var o = new BigNumber(this), r = o._d.length >= (n = new BigNumber(n))._d.length, a = (r ? o : n)._d,
		b = (r ? n : o)._d, la = a.length, lb = b.length, x = new BigNumber, i, j, s;
        for (i = lb; i; r && s.unshift(r), x.set(x.add(new BigNumber(s.join("")))))
            for (s = (new Array(lb - --i)).join("0").split(""), r = 0, j = la; j; r += a[--j] * b[i], s.unshift(r % 10), r = (r / 10) >>> 0);
        return o._s = o._s != n._s, o._f = ((r = la + lb - o._f - n._f) >= (j = (o._d = x._d).length) ? this._zeroes(o._d, r - j + 1, 1).length : j) - r, o.round();
    };
    o.divide = function (n) {
        if ((n = new BigNumber(n)) == "0")
            throw new Error("Division by 0");
        else if (this == "0")
            return new BigNumber;
        var o = new BigNumber(this), a = o._d, b = n._d, la = a.length - o._f,
		lb = b.length - n._f, r = new BigNumber, i = 0, j, s, l, f = 1, c = 0, e = 0;
        r._s = o._s != n._s, r.precision = Math.max(o.precision, n.precision),
		r._f = +r._d.pop(), la != lb && o._zeroes(la > lb ? b : a, Math.abs(la - lb));
        n._f = b.length, b = n, b._s = false, b = b.round();
        for (n = new BigNumber; a[0] == "0"; a.shift());
        out:
        do {
            for (l = c = 0, n == "0" && (n._d = [], n._f = 0); i < a.length && n.compare(b) == -1; ++i) {
                (l = i + 1 == a.length, (!f && ++c > 1 || (e = l && n == "0" && a[i] == "0")))
				&& (r._f == r._d.length && ++r._f, r._d.push(0));
                (a[i] == "0" && n == "0") || (n._d.push(a[i]), ++n._f);
                if (e)
                    break out;
                if ((l && n.compare(b) == -1 && (r._f == r._d.length && ++r._f, 1)) || (l = 0))
                    while (r._d.push(0), n._d.push(0), ++n._f, n.compare(b) == -1);
            }
            if (f = 0, n.compare(b) == -1 && !(l = 0))
                while (l ? r._d.push(0) : l = 1, n._d.push(0), ++n._f, n.compare(b) == -1);
            for (s = new BigNumber, j = 0; n.compare(y = s.add(b)) + 1 && ++j; s.set(y));
            n.set(n.subtract(s)), !l && r._f == r._d.length && ++r._f, r._d.push(j);
        }
        while ((i < a.length || n != "0") && (r._d.length - r._f) <= r.precision);
        return r.round();
    };
    o.mod = function (n) {
        return this.subtract(this.divide(n).intPart().multiply(n));
    };
    o.pow = function (n) {
        var o = new BigNumber(this), i;
        if ((n = (new BigNumber(n)).intPart()) == 0) return o.set(1);
        for (i = Math.abs(n); --i; o.set(o.multiply(this)));
        return n < 0 ? o.set((new BigNumber(1)).divide(o)) : o;
    };
    o.set = function (n) {
        return this.constructor(n), this;
    };
    o.compare = function (n) {
        var a = this, la = this._f, b = new BigNumber(n), lb = b._f, r = [-1, 1], i, l;
        if (a._s != b._s)
            return a._s ? -1 : 1;
        if (la != lb)
            return r[(la > lb) ^ a._s];
        for (la = (a = a._d).length, lb = (b = b._d).length, i = -1, l = Math.min(la, lb); ++i < l; )
            if (a[i] != b[i])
                return r[(a[i] > b[i]) ^ a._s];
        return la != lb ? r[(la > lb) ^ a._s] : 0;
    };
    o.negate = function () {
        var n = new BigNumber(this); return n._s ^= 1, n;
    };
    o.abs = function () {
        var n = new BigNumber(this); return n._s = 0, n;
    };
    o.intPart = function () {
        return new BigNumber((this._s ? "-" : "") + (this._d.slice(0, this._f).join("") || "0"));
    };
    o.valueOf = o.toString = function () {
        var o = this;
        return (o._s ? "-" : "") + (o._d.slice(0, o._f).join("") || "0") + (o._f != o._d.length ? "." + o._d.slice(o._f).join("") : "");
    };
    o._zeroes = function (n, l, t) {
        var s = ["push", "unshift"][t || 0];
        for (++l; --l; n[s](0));
        return n;
    };
    o.round = function () {
        var nuevo = this;
        if (nuevo._rounding) return nuevo;
        var $ = BigNumber, r = nuevo.roundType, b = nuevo._d, d, p, n, x;
        for (nuevo._rounding = true; nuevo._f > 1 && !b[0]; --nuevo._f, b.shift());
        for (d = nuevo._f, p = nuevo.precision + d, n = b[p]; b.length > d && !b[b.length - 1]; b.pop());
        x = (nuevo._s ? "-" : "") + (p - d ? "0." + nuevo._zeroes([], p - d - 1).join("") : "") + 1;
        if (b.length > p) {
            switch (r) {
                case $.ROUND_HALF_EVEN:
                    if (n >= 5) {
                        nuevo._rounding = true;
                        nuevo = nuevo.add(x);
                    }
                    //RESTO DE CASES SIN DESARROLLAR
            }
            /*
            n && (r == $.ROUND_DOWN ? false : r == $.ROUND_UP ? true : r == $.ROUND_CEIL ? !this._s
            : r == $.ROUND_FLOOR ? this._s : r == $.ROUND_HALF_UP ? n >= 5 : r == $.ROUND_HALF_DOWN ? n > 5
            : r == $.ROUND_HALF_EVEN ? n >= 5 && b[p - 1] & 1 : false) && this.add(x);
            */
            //b.splice(p, b.length - p);
            nuevo._d.splice(p, b.length - p);
        }
        nuevo._rounding = false;
        this._d = nuevo._d;
        this._f = nuevo._f;
        this._rounding = nuevo._rounding;
        this._s = nuevo._s;
        this.precision = nuevo.precision;
        this.roundType = nuevo.roundType;
        this._s = nuevo._s;
        return nuevo;
    };
}
//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

////XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//                          FIN FORMATO NUMÉRICO TEXTBOXES
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
////XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//                          UPDOWNARROWS
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

//Sumar o restar una unidad al contenido del control ctrl
//ctrl -> control cuyo valor queremos modificar
//minimo -> valor mínimo que puede tomar el control
//maximo -> valor máximo que puede tomar el control
//vdecimalfmt -> separador de decimales
//vthousanfmt -> separador de miles
//vprecisionfmt -> número de decimales
//operation -> operación: puede ser suma (valor 'add') o resta ('subtract'). Si ignorará cualquier otro valor.
//event -> Evento
//boton -> Botón del ratón pulsado
function addOrSubstrackOne(ctrl, minimo, maximo, vdecimalfmt, vthousanfmt, vprecisionfmt, operation, event, boton, errMsg, errLimites, tipoControl, LblTotLinID, LblPrecPorProveID, controlPrecioCantidadID, LblPrecArtID, moneda, decimalesPrecio) {
    
    if (boton == 1) {
        if (ctrl != null && ctrl != '') {
            var numero;
            ctrl = document.getElementById(ctrl);
            if (!ctrl.value) {
                numero = '0';
            }
            else {
                numero = ctrl.value;
            }
            if (numero.length == 0) {
                numero = '0';
            }
            numero = str2num(numero, vdecimalfmt, vthousanfmt);

            if (operation == 'add') {
                numero = numero.add(1);
            }
            else if (operation == 'subtract') {
                numero = numero.subtract(1);
            }
            if ((maximo == null && minimo == null) || (maximo != null && minimo != null && numero <= maximo && numero >= minimo) || (maximo != null && minimo == null && numero <= maximo) || (maximo == null && minimo != null && numero >= minimo)) {
                var ctrl_DblValue = $get(ctrl.id + '_DblValue')
                //Sólo hacemos la suma/resta si podemos modificar también el control hidden
                if (ctrl_DblValue != null) {
                    ctrl.value = num2str(numero, vdecimalfmt, vthousanfmt, vprecisionfmt);
                    if (ctrl_DblValue.nodeName == 'INPUT')
                        ctrl_DblValue.value = num2str4DBlValue(numero);
                    else
                        ctrl_DblValue.innerHTML = num2str4DBlValue(numero);
                    validarNumero(ctrl, vdecimalfmt, vthousanfmt, vprecisionfmt, minimo, maximo, errMsg, errLimites, tipoControl, LblTotLinID, LblPrecPorProveID, controlPrecioCantidadID, LblPrecArtID, moneda, decimalesPrecio);
                }
            }
        }
    }
}

//Sumar o restar una unidad al contenido del control ctrl
//ctrl -> control cuyo valor queremos modificar
//minimo -> valor mínimo que puede tomar el control
//maximo -> valor máximo que puede tomar el control
//vdecimalfmt -> separador de decimales
//vthousanfmt -> separador de miles
//vprecisionfmt -> número de decimales
//operation -> operación: puede ser suma (valor 'add') o resta ('subtract'). Si ignorará cualquier otro valor.
//event -> Evento
//boton -> Botón del ratón pulsado
function addOrSubstrackOneImporte(ctrl, minimo, maximo, vdecimalfmt, vthousanfmt, vprecisionfmt, operation, event, boton, errMsg, errLimites, tipoControl, LblTotLinID, LblPrecPorProveID, controlPrecioCantidadID, LblPrecArtID, moneda, decimalesPrecio) {
    
    if (boton == 1) {
        if (ctrl != null && ctrl != '') {
            var numero;
            ctrl = document.getElementById(ctrl);
            if (!ctrl.value) {
                numero = '0';
            }
            else {
                numero = ctrl.value;
            }
            if (numero.length == 0) {
                numero = '0';
            }
            numero = str2num(numero, vdecimalfmt, vthousanfmt);

            if (operation == 'add') {
                numero = numero.add(1);
            }
            else if (operation == 'subtract') {
                numero = numero.subtract(1);
            }
            if ((maximo == null && minimo == null) || (maximo != null && minimo != null && numero <= maximo && numero >= minimo) || (maximo != null && minimo == null && numero <= maximo) || (maximo == null && minimo != null && numero >= minimo)) {
                var ctrl_DblValue = $get(LblPrecArtID + '_DblValue')
                //var ctrl_DblValueID = $get(LblPrecArtID + 'Ant_DblValue')
                //var precioAnterio = 0
                //Sólo hacemos la suma/resta si podemos modificar también el control hidden
                if (ctrl_DblValue != null) {
                    precioAnterior = ctrl_DblValue.value
                    ctrl.value = num2str(numero, vdecimalfmt, vthousanfmt, vprecisionfmt);
                    if (ctrl_DblValue.nodeName == 'INPUT')
                        ctrl_DblValue.value = num2str4DBlValue(numero);
                    else
                        ctrl_DblValue.innerHTML = num2str4DBlValue(numero);
                    validarNumeroImporte(ctrl, vdecimalfmt, vthousanfmt, vprecisionfmt, minimo, maximo, errMsg, errLimites, tipoControl, LblTotLinID, LblPrecPorProveID, controlPrecioCantidadID, LblPrecArtID, moneda, decimalesPrecio, LblPrecArtID + 'Ant_DblValue');
                }
            }
        }
    }
}


//Sumar o restar una unidad al contenido del control ctrl
//ctrl -> control cuyo valor queremos modificar
//minimo -> valor mínimo que puede tomar el control
//maximo -> valor máximo que puede tomar el control
//vdecimalfmt -> separador de decimales
//vthousanfmt -> separador de miles
//vprecisionfmt -> número de decimales
//operation -> operación: puede ser suma (valor 'add') o resta ('subtract'). Si ignorará cualquier otro valor.
//event -> Evento
//boton -> Botón del ratón pulsado
function addOrSubstrackOneCesta(ctrl, minimo, maximo, vdecimalfmt, vthousanfmt, vprecisionfmt, operation, event, boton, errMsg, errLimites, tipoControl, LblTotLinID, LblPrecPorProveID, controlPrecioCantidadID, LblPrecArtID, moneda, decimalesPrecio, LblTotalCestaID) {
    if (boton == 1) {
        if (ctrl != null && ctrl != '') {
            var numero;
            ctrl = document.getElementById(ctrl);
            if (!ctrl.value) {
                numero = '0';
            }
            else {
                numero = ctrl.value;
            }
            if (numero.length == 0) {
                numero = '0';
            }
            numero = str2num(numero, vdecimalfmt, vthousanfmt);

            if (operation == 'add') {
                numero = numero.add(1);
            }
            else if (operation == 'subtract') {
                numero = numero.subtract(1);
            }
            if ((maximo == null && minimo == null) || (maximo != null && minimo != null && numero <= maximo && numero >= minimo) || (maximo != null && minimo == null && numero <= maximo) || (maximo == null && minimo != null && numero >= minimo)) {
                var ctrl_DblValue = $get(ctrl.id + '_DblValue')
                //Sólo hacemos la suma/resta si podemos modificar también el control hidden
                if (ctrl_DblValue != null) {
                    ctrl.value = num2str(numero, vdecimalfmt, vthousanfmt, vprecisionfmt);
                    if (ctrl_DblValue.nodeName == 'INPUT')
                        ctrl_DblValue.value = num2str4DBlValue(numero);
                    else
                        ctrl_DblValue.innerHTML = num2str4DBlValue(numero);
                    validarNumeroCesta(ctrl, vdecimalfmt, vthousanfmt, vprecisionfmt, minimo, maximo, errMsg, errLimites, tipoControl, LblTotLinID, LblPrecPorProveID, controlPrecioCantidadID, LblPrecArtID, moneda, decimalesPrecio, LblTotalCestaID);
                }
            }
        }
    }
}
//Oscurece el fondo del control
function lowlight(ctrl, event) {
    if (ButtonLeft(event) == 1) {
        ctrl.style.backgroundColor = '#83A6F4';
    }
}
//Aclara el fondo del control
function unlowlight(ctrl) {
    unhighlight(ctrl);
}
//Aclara el fondo del control
function highlight(ctrl) {
    ctrl.style.backgroundColor = '#DCEDFD';
}
//Oscurece el fondo del control
function unhighlight(ctrl) {
    ctrl.style.backgroundColor = '#c5d5fc';
}
//Lanzamos el proceso AddOrSubstrackOne
function beginAddOrSubstrackOne(ctrl, minimo, maximo, vdecimalfmt, vthousanfmt, vprecisionfmt, operation, event, errMsg, errLimites, tipoControl, LblTotLinID, LblPrecPorProveID, controlPrecioCantidadID, LblPrecArtID, moneda, decimalesPrecio) {
    var boton = ButtonLeft(event);
    addOrSubstrackOne(ctrl, minimo, maximo, vdecimalfmt, vthousanfmt, vprecisionfmt, operation, event, boton, errMsg, errLimites, tipoControl, LblTotLinID, LblPrecPorProveID, controlPrecioCantidadID, LblPrecArtID, moneda, decimalesPrecio);
    action_timeout = setInterval(function () { addOrSubstrackOne(ctrl, minimo, maximo, vdecimalfmt, vthousanfmt, vprecisionfmt, operation, event, boton, errMsg, errLimites, tipoControl, LblTotLinID, LblPrecPorProveID, controlPrecioCantidadID, LblPrecArtID, moneda, decimalesPrecio) }, 200);
}
//Lanzamos el proceso AddOrSubstrackOne
function beginAddOrSubstrackOneImporte(ctrl, minimo, maximo, vdecimalfmt, vthousanfmt, vprecisionfmt, operation, event, errMsg, errLimites, tipoControl, LblTotLinID, LblPrecPorProveID, controlPrecioCantidadID, LblPrecArtID, moneda, decimalesPrecio) {
    
    var boton = ButtonLeft(event);
    addOrSubstrackOneImporte(ctrl, minimo, maximo, vdecimalfmt, vthousanfmt, vprecisionfmt, operation, event, boton, errMsg, errLimites, tipoControl, LblTotLinID, LblPrecPorProveID, controlPrecioCantidadID, LblPrecArtID, moneda, decimalesPrecio);
    action_timeout = setInterval(function () { addOrSubstrackOneImporte(ctrl, minimo, maximo, vdecimalfmt, vthousanfmt, vprecisionfmt, operation, event, boton, errMsg, errLimites, tipoControl, LblTotLinID, LblPrecPorProveID, controlPrecioCantidadID, LblPrecArtID, moneda, decimalesPrecio) }, 200);
}
//Lanzamos el proceso AddOrSubstrackOneCesta
function beginAddOrSubstrackOneCesta(ctrl, minimo, maximo, vdecimalfmt, vthousanfmt, vprecisionfmt, operation, event, errMsg, errLimites, tipoControl, LblTotLinID, LblPrecPorProveID, controlPrecioCantidadID, LblPrecArtID, moneda, decimalesPrecio, LblTotalCestaID) {

    var boton = ButtonLeft(event);
    addOrSubstrackOneCesta(ctrl, minimo, maximo, vdecimalfmt, vthousanfmt, vprecisionfmt, operation, event, boton, errMsg, errLimites, tipoControl, LblTotLinID, LblPrecPorProveID, controlPrecioCantidadID, LblPrecArtID, moneda, decimalesPrecio, LblTotalCestaID);
    action_timeout = setInterval(function () { addOrSubstrackOneCesta(ctrl, minimo, maximo, vdecimalfmt, vthousanfmt, vprecisionfmt, operation, event, boton, errMsg, errLimites, tipoControl, LblTotLinID, LblPrecPorProveID, controlPrecioCantidadID, LblPrecArtID, moneda, decimalesPrecio, LblTotalCestaID) }, 200);
}
//Detenemos el proceso AddOrSubstrackOne
function endAddOrSubstrackOne() {
    if (typeof (action_timeout) != "undefined") clearInterval(action_timeout);
}

//Devuelve 1 si el boton pulsado en el ratón es el izquierdo. Si no, devuelve 0.
function ButtonLeft(event) {
    var button = 0;
    if (event.which === undefined) {    // Internet Explorer
        if (event.button & 1) {
            button = 1;
        }
    }
    else {  // Firefox, Opera, Google Chrome, Safari
        if (event.which == 1) {
            button = 1;
        }
    }
    return button;
}

////XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//                          FIN UPDOWNARROWS
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
