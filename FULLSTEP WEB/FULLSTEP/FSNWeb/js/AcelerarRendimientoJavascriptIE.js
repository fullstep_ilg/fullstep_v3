﻿function disposeTree(sender, args) {
    var elements = args.get_panelsUpdating();
    for (var i = elements.length - 1; i >= 0; i--) {
        var element = elements[i];
        var allnodes = element.getElementsByTagName('*'),
        length = allnodes.length;
        var nodes = new Array(length)
        for (var k = 0; k < length; k++) {
            nodes[k] = allnodes[k];
        }
        for (var j = 0, l = nodes.length; j < l; j++) {
            var node = nodes[j];
            if (node.nodeType === 1) {
                if (node.dispose && typeof (node.dispose) === "function") {
                    node.dispose();
                }
                else if (node.control && typeof (node.control.dispose) === "function") {
                    node.control.dispose();
                }

                var behaviors = node._behaviors;
                if (behaviors) {
                    behaviors = Array.apply(null, behaviors);
                    for (var k = behaviors.length - 1; k >= 0; k--) {
                        behaviors[k].dispose();
                    }
                }
            }
        }
        element.innerHTML = "";
    }
}
Sys.WebForms.PageRequestManager.getInstance().add_pageLoading(disposeTree);

/*
Los modalpopupextender y los TextBoxWatermarkExtender ejecutan en cada postback asíncrono la función del ajaxcontroltoolkit
_partialUpdateEndRequest, que gestiona el estado del control: si debe mostrarse o no, y lo renderiza.
Cuando el número de controles de este tipo es alto en una página, ralentiza su renderizado en Internet Explorer. 
Vamos a sobreescribir la función de los modalpopupextender de nuevo aquí, para evitar esta posibilidad, 
comentando la línea this.layout() (evitamos el renderizado).
*/
$(document).ready(function () {
    if (Sys.Extended) {
        Sys.Extended.UI.ModalPopupBehavior.prototype._partialUpdateEndRequest = function (sender, endRequestEventArgs) {
            Sys.Extended.UI.ModalPopupBehavior.callBaseMethod(this, '_partialUpdateEndRequest', [sender, endRequestEventArgs]);
            if (this.get_element()) {
                var action = endRequestEventArgs.get_dataItems()[this.get_element().id];
                if ("show" == action) {
                    this.show();
                } else if ("hide" == action) {
                    this.hide();
                }
            }
            //this._layout();
        }
    }
});