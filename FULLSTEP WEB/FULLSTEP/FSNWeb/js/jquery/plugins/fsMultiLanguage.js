﻿(function ($) {
    //#region Constantes    
    /* TECLAS POSIBLES PULSADAS PARA SU POSIBLE TRATAMIENTO */
    var fsMultiLanguageKEY = {
        UP: 38,
        DOWN: 40,
        RIGHT: 39,
        LEFT: 37,
        DEL: 46,
        TAB: 9,
        RETURN: 13,
        ESC: 27,
        COMMA: 188,
        PAGEUP: 33,
        PAGEDOWN: 34,
        BACKSPACE: 8
    };
    var _fsMultiLanguageLanguages = ['SPA', 'ENG', 'FRA', 'GER'];
    /* FIN TECLAS POSIBLES PULSADAS PARA SU POSIBLE TRATAMIENTO */
    //#endregion
    $.widget("fsWidgets.fsMultiLanguage", {
        options: {
            // 1px,1em,...
            width: '20em',
            //only for text area mode
            height: null,
            // SPA,ENG,FRA,...}
            value: null,
            // ['SPA','ENG','GER','FRA'}
            languages: [],
            // [{language:'SPA',text:''},{language:'SPA',text:''}]
            data: [],
            isTextArea: false,
            defaultValue:null
        },
        _create: function () {            
            this._languageValues = {};
            this._languagesLength = 0;
            this._value = this.options.value;
            var fsMultiLanguageWidget, fsMultiLanguageWidgetDiv, fsMultiLanguageTextBox, fsMultiLanguageSelector;
            fsMultiLanguageWidget = this;
            fsMultiLanguageWidgetDiv = this.element;
            $(fsMultiLanguageWidgetDiv).addClass('fsMultiLanguageContainer');
            fsMultiLanguageWidgetDiv.append('<div style="position:relative; float:right; vertical-align:top;"><label></label><span></span><ul class="fsMultiLanguageContainerHide"></ul></div>');
            if (fsMultiLanguageWidget.options.isTextArea) fsMultiLanguageWidgetDiv.append('<span><textarea style="width:100%; white-space:pre-wrap; font-family:inherit;"></textarea></span>');
            else fsMultiLanguageWidgetDiv.append('<span><input type="text" autocomplete="off" style="width:100%;"></span>');            
            fsMultiLanguageTextBox = (fsMultiLanguageWidget.options.isTextArea ? $('span>textarea', fsMultiLanguageWidgetDiv) : $('span>input', fsMultiLanguageWidgetDiv));
            fsMultiLanguageSelector = $('label', fsMultiLanguageWidgetDiv);
            if (!fsMultiLanguageWidget._hasStyleAttribute('width')) $(fsMultiLanguageWidgetDiv).css('width', fsMultiLanguageWidget.options.width);
            if (fsMultiLanguageWidget.options.height !== null) $(fsMultiLanguageTextBox).css('height', fsMultiLanguageWidget.options.height);

            if (fsMultiLanguageWidget.options.data.length === 0) {
                if (fsMultiLanguageWidget.options.languages.length === 0) fsMultiLanguageWidget.options.languages = _fsMultiLanguageLanguages;                
                $.each(fsMultiLanguageWidget.options.languages, function () {
                    if (fsMultiLanguageWidget._languageValues[this] === undefined) fsMultiLanguageWidget._languageValues[this] = '';
                });
                if (fsMultiLanguageWidget._value === null) fsMultiLanguageWidget._value = fsMultiLanguageWidget.options.languages[0];
            } else {
                $.each(data, function () {
                    fsMultiLanguageWidget._languageValues[this.language] =  this.text;
                    fsMultiLanguageWidget.options.languages.push(this.language);
                });
                if (fsMultiLanguageWidget._value === null) fsMultiLanguageWidget._value = data[0].language;
            };
            fsMultiLanguageSelector.addClass(fsMultiLanguageWidget._value);
            fsMultiLanguageWidget._languagesLength = fsMultiLanguageWidget.options.languages.length;
            $.each(fsMultiLanguageWidget.options.languages, function () {
                $('ul',fsMultiLanguageWidgetDiv).append('<li class="' + this + (String(this) === String(fsMultiLanguageWidget._value) ? ' selected' : '') + '"></li>');                
            });
            //#region Eventos
            $('div', fsMultiLanguageWidgetDiv).on('click', $.proxy(this._expandCollapse, this));
            $('ul li', fsMultiLanguageWidgetDiv).on('click', function () { fsMultiLanguageWidget._setLanguage(this.classList['0']) });
            $('span>input,span>textarea', fsMultiLanguageWidgetDiv).on(($.browser.opera ? 'keypress' : 'keydown'), function (event) {
                var code;
                if (event.keyCode) code = event.keyCode;
                else if (event.which) code = event.which;
                var index;
                switch (code) {
                    case fsMultiLanguageKEY.UP:
                        fsMultiLanguageWidget._setText();
                        index = ($.inArray(fsMultiLanguageWidget._value, fsMultiLanguageWidget.options.languages) - 1);
                        fsMultiLanguageWidget._languageChange(index);
                        break;
                    case fsMultiLanguageKEY.DOWN:
                        fsMultiLanguageWidget._setText();
                        index = ($.inArray(fsMultiLanguageWidget._value, fsMultiLanguageWidget.options.languages) + 1);
                        fsMultiLanguageWidget._languageChange(index);
                        break;
                    default:
                        break;
                };                
            });
            $('input', fsMultiLanguageWidgetDiv).on('change', $.proxy(this._setText, this));
            $('body').click($.proxy(this._windowClick, this));
            $('span>input,span>textarea', fsMultiLanguageWidgetDiv).on('blur', function () { fsMultiLanguageWidget._setText(); });
            //#endregion        
        },        
        // Use the _setOption method to respond to changes to options
        _setOption: function (key, value) {
            this._super("_setOption", key, value);
        },
        _windowClick: function (e) {
            if ($(e.target).closest('.fsMultiLanguageContainer').length === 0)  $('ul', this.element).addClass('fsMultiLanguageContainerHide');
        },
        _setLanguage: function (value) {
            this._value = value;
            this._selectedValueChange(value);
            if (this._onSelectedValueChangeFunction) this._onSelectedValueChangeFunction();
        },
        _languageChange:function(index){
            index = ((index % this._languagesLength) + this._languagesLength) % this._languagesLength;
            this._value = this.options.languages[index];
            this._setLanguage(this._value);
        },
        _setText: function () {
            var fsMultiLanguageTextBox = (this.options.isTextArea ? $('textarea', this.element) : $('input', this.element));
            this._languageValues[this._value] = fsMultiLanguageTextBox.val();
            this._selectedValueChange(this._value);
            if (this._onSelectedValueChangeFunction) this._onSelectedValueChangeFunction();
        },
        _selectedValueChange: function (value) {
            var fsMultiLanguageSelector = $('label', this.element);
            var fsMultiLanguageTextBox = (this.options.isTextArea ? $('textarea', this.element) : $('input', this.element));
            fsMultiLanguageSelector.removeClass();
            fsMultiLanguageSelector.addClass(value);
            fsMultiLanguageTextBox.val(this._languageValues[value]);
            $('ul li', this.element).removeClass('selected');
            $('ul li.' + value, this.element).addClass('selected');
        },
        _expandCollapse: function () {
            $('ul', this.element).toggleClass('fsMultiLanguageContainerHide');
        },
        _hasStyleAttribute: function (prop) {
            var styles = this.element.attr('style'), value = false;
            styles && $.each(styles.split(';'), function () {
                var style = this.split(':');
                if ($.trim(style[0]) === prop) { value = true; return; }
            });
            return value;
        },
        addItem: function (language, text) {
            $('ul', this.element).append('<li class="' + language + (String(language) === String(this._value) ? ' selected' : '') + '"></li>');
        },        
        selectedValue: function (value) {
            if (value === undefined) return this._value;
            else {
                var oldValue = this._value;
                this._value = value;
                if (oldValue !== value) this._selectedValueChange(value);
            }
        },
        setDefaultValue: function () {
            this.selectedValue(this.options.defaultValue);
        },
        selectedText:function(text){
            if (text === undefined) return this._languageValues[this._value];
            else this._languageValues[this._value] = text;
        },
        setText: function (language, text) {            
            this._languageValues[language] = text;
            if (language === this._value) {
                var fsMultiLanguageTextBox = (this.options.isTextArea ? $('textarea', this.element) : $('input', this.element));
                fsMultiLanguageTextBox.val(text);
            }
        },
        getValues: function () {
            return this.__alphabeticalSort(this._languageValues);
        },
        clearValues: function () {
            var self = this;
            $.each(this.options.languages, function () {
                self._languageValues[this] = '';
            });
            var fsMultiLanguageTextBox = (this.options.isTextArea ? $('textarea', this.element) : $('input', this.element));
            fsMultiLanguageTextBox.val('');
        },
        onSelectedValueChange: function (scriptCode) {
            debugger;
            this._onSelectedValueChangeFunction = scriptCode;
        },
        __alphabeticalSort: function (values) {
            keys = Object.keys(values).sort();
            var lValues = {};
            $.each(keys, function () {
                lValues[this] = values[this];
            });
            return lValues;
        }
    });
})(jQuery);