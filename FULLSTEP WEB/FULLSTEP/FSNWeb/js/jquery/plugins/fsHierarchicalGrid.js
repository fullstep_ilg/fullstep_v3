﻿(function ($) {
    //#region Constantes  
    var columnType = {
        Text: 0,
        Button: 1,
        Link:2
    };
    //#endregion
    //#region ElementosGrid
    var fsGridColumnHeader = function () { this.init(); };
    fsGridColumnHeader.prototype = {
        init: function () {
            this.key = '';
            this.visible = true;
            this.width = null;
            this.cssClass = null;
            this.headerText = '';
            this.colspan = 1;
            this.position = 0;
            this.type = columnType.Text;
            this.buttonText='';
        }       
    };
    var fsGridRow = function () { this.init(); };
    fsGridRow.prototype = {
        init: function () {
            this.height = null;
            this.class = null;
            this.expanded = true;
            this.level = 0;
            this.cells = [];
            this.grid = null;
        },
        addGridRow: function (data, createColumnConfiguration) {
            var self = this;
            self.grid = new fsGrid();
            fsGrid.loadRowsFromData(self.grid, data, (self.level + 1));
            if (createColumnConfiguration) fsGrid.loadColumnsFromData(self.grid, self.grid.rows[0],self.level+1);
        }
    };
    fsGridRow.addChildren = function (data) {
        var self = this;
        $.each(data, function () {
            var _row = new fsGridRow();
            $.each(this, function (key, value) {
                if (key == 'children' && value !== null) {
                    _row.addChildrenRow(value)
                } else {
                    var _cell = new fsGridCell();
                    _cell.key = key;
                    _cell.value = value;
                    _row.cells.push(_cell);
                }
            });
            self.addChildreRow(_row);
        });
    };
    var fsGridCell = function () { this.init(); };
    fsGridCell.prototype = {
        init: function () {
            this.key = null;
            this.value = null;            
        }
    };
    var fsGrid = function () { this.init(); };
    fsGrid.prototype = {
        init: function () {
            this.rows = [];
            this.bands = [];
            this.isHierarchical = false;
        },
        addRow: function (row) {
            this.rows.push(row);
        },
        getGridColumnByKey:function(key){
            return $.grep(grid.columns,function(x){return x.key==cKey;});
        },
        setPropertyColumn: function (column, property, value) {
            $.grep(this.columns, function (x) { return x.key == column.key; })[0][property] = value;
        }
    };
    fsGrid.loadRowsFromData = function (grid, data, level, hasColumnConfiguration) {
        var hasChildren = false;
        $.each(data, function () {
            var _row = new fsGridRow();
            $.each(this, function (key, value) {
                switch (key) {
                    case 'expandend':
                        _row.expanded = value;
                        break;
                    case 'children':
                        if(value.length>0) {
                            hasChildren = true;
                            _row.addGridRow(value, !hasColumnConfiguration);
                        };
                        break;
                    default:
                        if (Array.isArray(value)) {
                            $.each(value, function () {
                                var _cell = new fsGridCell();
                                _cell.key = key;
                                _cell.value = this;
                                _row.cells.push(_cell);
                            });
                        } else {
                            var _cell = new fsGridCell();
                            _cell.key = key;
                            _cell.value = value;
                            _row.cells.push(_cell);
                        }
                        break;
                };
            });
            _row.level = level;
            grid.addRow(_row);
        });
        grid.isHierarchical = hasChildren;
    };
    fsGrid.loadColumnsFromData = function (grid, data, level) {
        var _columns = [];
        $.each(data.cells, function (index) {
            var cKey=this.key;
            if ($.grep(grid.columns, function (x) { return x.key == cKey; }).length == 0) {
                var _column = new fsGridColumnHeader();
                _column.key = cKey;
                _column.headerText = this.key;
                _column.position = index + 1;
                _columns.push(_column);
            } else $.grep(_columns, function (x) { return x.key == cKey; })[0]['colspan'] = parseInt($.grep(_columns, function (x) { return x.key == cKey; })[0].colspan+1);                
        });        
        grid.bands[level] = fsGrid.sortColumnsByPosition(_columns);
    };
    fsGrid.sortColumnsByPosition = function (columns) {
        function ordenAscendentePosicion(a, b) {
            return a.position - b.position;
        };
        return columns.sort(ordenAscendentePosicion);
    };
    //#endregion
    $.widget("fsWidgets.fsHierarchicalGrid", {
        options: {
            //var data = [
            //{
            //    proveedor: 'XXXXX', UNQA: ['ISU', ''], nivelEscalacion: 2, estado: 'Pdte aprobar', noConformidadEscalacion: '', children: [
            //        { noConformidadEscalacion: 'NCE_1_1', UNQA: ['ISU', 'TOL'], estado: 'Cierre eficaz' },
            //        { noConformidadEscalacion: 'NCE_1_2', UNQA: ['ISU', 'GEL'], estado: 'Cierre eficaz' },
            //        { noConformidadEscalacion: 'NCE_1_3', UNQA: ['ISU', 'TOL'], estado: 'Cierre eficaz' },
            //    ]
            //},
            //{ proveedor: 'XXXXX', UNQA: ['ISU', ''], nivelEscalacion: 2, estado: 'Pdte aprobar', noConformidadEscalacion: '' },
            //{ proveedor: 'XXXXX', UNQA: ['ISU', ''], nivelEscalacion: 2, estado: 'Pdte aprobar', noConformidadEscalacion: '' },
            //    ];
            data: [],
            columnsConfig: null,
            onInitializeRow: null
        },
        _create: function () {
            //#region Eventos
            $('.fsHierarchicalGridExpander', this.element).on('click', $.proxy(this._expandCollapse, this));
            this._onInitializeRow = this.options.onInitializeRow;
            //#endregion
            var fsHierarchicalGridContainer = this.element;
            $(fsHierarchicalGridContainer).addClass('fsHierarchicalContainer');
            var fsHierarchicalGrid = new fsGrid();
            //Si no nos dan una configuracion de columnas nos creamos una basada en los datos dados            
            if (this.options.columnsConfig == null) fsGrid.loadColumnsFromData(fsHierarchicalGrid, fsHierarchicalGrid.rows[0], 0);
            else {
                var self = this;
                var iband;
                $.each(this.options.columnsConfig, function () {
                    iband = this.band;
                    fsHierarchicalGrid.bands[this.band] = fsGrid.sortColumnsByPosition($.grep(self.options.columnsConfig, function (x) { return x.band == iband })[0].config);
                });
            }

            //Nos creamos una estructura de elementos para luego pintarlos Grid(columnas,filas) y cada fila tiene grid si es hierarchical
            fsGrid.loadRowsFromData(fsHierarchicalGrid, this.options.data, 0, !(this.options.columnsConfig == null));
            this._grid = fsHierarchicalGrid;
            this._createDataTable();            
        },
        _createDataTable: function () {
            this.element.append('<table class="fsHierarchicalGrid"><colgroup></colgroup><thead><tr></tr></thead><tbody></tbody></table>');
            this._createColumnConfiguration($('table colgroup', this.element), 0);
            this._createHeader($('table thead tr', this.element), 0);
            this._createRows($('table tbody', this.element), this._grid.rows, 0);
        },
        _createColumnConfiguration: function (colgroup, level) {
            if (this._grid.isHierarchical) $(colgroup).append('<col/>');
            $.each(this._grid.bands[level], function () {
                if (this.visible) {
                    var colConfig = '<col' + (this.colspan == undefined || this.colspan == 1 ? '' : ' span="' + this.colspan + '"');
                    colConfig += ' style="' + (this.width == null ? '' : 'width:' + this.width + ';') + '">';
                    $(colgroup).append(colConfig);
                    if ($('col', colgroup).last().width() > 0 && this.colspan !== undefined && this.colspan > 1) $('col', colgroup).last().width($('col', colgroup).last().width() / this.colspan);
                }
            });
        },
        _createHeader: function (tableHeaderRow, level) {
            if (this._grid.bands[level + 1] !== undefined) $(tableHeaderRow).append('<th class="fsHierarchicalGridExpander fsHierarchicalGridHeader"></th>');
            $.each(this._grid.bands[level], function () {
                var _columnGrid = this;
                $(tableHeaderRow).append('<th' + (_columnGrid.colspan == undefined || _columnGrid.colspan == 1 ? '' : ' colspan="' + _columnGrid.colspan + '"') + (_columnGrid.visible ? '' : ' style="display:none;"') + '></th>');
                $('th', tableHeaderRow).last().append('<div' + (_columnGrid.width == null ? '' : ' style="width:' + _columnGrid.width + '"') + '>' + _columnGrid.headerText + '</div>');
            });
        },
        _createRows: function (tableBody, rows, level) {
            var _widget = this;
            $.each(rows, function (index) {
                $(tableBody).append('<tr index="' + index + '"></tr>');
                var _tbRow = $('tr', tableBody).last();
                _tbRow.attr('level', level);

                if (typeof (_widget._grid.bands[level + 1]) !== 'undefined') $('tr', tableBody).last().append('<td class="fsHierarchicalGridExpander"><span></span></td>');
                if (this.grid !== null) $('span', $('tr', tableBody).last().children('td')).addClass((this.expanded ? 'fsHierarchicalGridCollapse' : 'fsHierarchicalGridExpand'));
                else $('span', $('tr', tableBody).last().children('td')).addClass('fsHierarchicalGridHeader');
                var iRow = this;
                $.each(_widget._grid.bands[level], function () {
                    switch (this.type) {
                        case columnType.Button:
                            _tbRow.append('<td' + (this.visible ? '' : ' style="display:none;"') + '><button>' + this.buttonText + '</button></td>');
                            break;
                        default:
                            var colKey = this.key;
                            var cell = $.grep(iRow.cells, function (x) { return x.key == colKey });
                            if (this.colspan == undefined || this.colspan == 1) _tbRow.append('<td' + (this.visible ? '' : ' style="display:none;"') + '>' + cell[0].value + '</td>');
                            else {
                                for (var j = 0; j < this.colspan; j++) {
                                    if (cell[j]) _tbRow.append('<td' + (this.visible ? '' : ' style="display:none;"') + '>' + cell[j].value + '</td>');
                                    else _tbRow.append('<td' + (this.visible ? '' : ' style="display:none;"') + '></td>');
                                }
                            }
                            break;
                    }
                });
                _widget._initializeRow(iRow);
                if (this.grid !== null) {
                    var trChild = '<tr class="fsHierarchicalGridChild" level="' + (level + 1) + '" parentIndex="' + index + '"' + (this.expanded ? '' : 'style="display:none;"') + '>';
                    trChild += '<td class="fsHierarchicalGridChildContent" colspan="' + ($('td', $(_tbRow)).length) + '">';
                    trChild += '<table class="fsHierarchicalGrid"><colgroup></colgroup><thead class="CabeceraBotones"><tr></tr></thead><tbody></tbody></table></td></tr>';
                    $(_tbRow).after(trChild);
                    _widget._createColumnConfiguration($('td table colgroup', $(_tbRow).next('tr')), (level + 1));
                    _widget._createHeader($('td table thead tr', $(_tbRow).next('tr')), (level + 1));
                    _widget._createRows($('td table tbody', $(_tbRow).next('tr')), this.grid.rows, (level + 1));
                }
            });
        },
        _expandCollapse: function (element) {
            $('span', $(element.target).closest('.fsHierarchicalGridExpander')).toggleClass('fsHierarchicalGridExpand');
            $('span', $(element.target).closest('.fsHierarchicalGridExpander')).toggleClass('fsHierarchicalGridCollapse');
            var index = $(element.target).closest('tr').attr('index');
            var level = $(element.target).closest('tr').attr('level');
            $('[level="' + parseInt(level + 1) + '"][parentIndex="' + index + '"]', this.element).toggle();
        },
        _initializeRow: function (row) {
            if (this._onInitializeRow)
                this._onInitializeRow(row);
        },
        getSelectedRow: function () { }
    });
})(jQuery);