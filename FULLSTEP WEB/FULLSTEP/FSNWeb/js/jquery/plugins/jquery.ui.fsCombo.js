﻿(function ($) {
    /* DEFINO LA ESTRUCTURA DE CADA ITEM DEL COMBO */
    var ItemList = function () {
        this.init();
    };
    ItemList.prototype = {
        init: function () {
            this.items = [];
        },
        loadFromData: function (valueField, textField, data) {
            var self = this;
            $.each(data, function () {
                var item = new Item();

                $.each(this, function (key, value) {
                    if (key.toLowerCase() == valueField) {
                        item.value = value;
                    }
                    if (key.toLowerCase() == textField) {
                        item.text = value;
                    }
                });
                self.items.push(item);
            });
        }
    };

    var Item = function (valueField, textField) {
        this.init(valueField, textField);
    }
    Item.prototype = {
        init: function () {
            this.value = "";
            this.text = "";
        }
    }
    ItemList.createFromData = function (valueField, textField, data) {
        var listItems = new ItemList();
        listItems.loadFromData(valueField, textField, data);
        return listItems;
    }
    /* FIN ESTRUCTURA DE CADA ITEM DEL COMBO */

    /* WIDGET */
    $.widget("ui.fsCombo", $.ui.mouse, {
        widgetEventPrefix: "fsCombo",
        options: {
            valueField: "Value",
            textField: "Text",
            dropDownImage: null,
            WebMethodURL: null,
            data: null,
            defaultValue: null,
            appendEmptyItem: false,
            emptyItemText: '',
            fsComboParents: null,
            autoComplete: false,
            itemListMaxHeight: null,
            adjustWidthToSpace: false,
            itemListWidth: null,
            disableParentEmptyValue: true,
            MinimumPrefixLength: 3,
            parentDependant: false,
            isDropDown: true,
            browserOldVersion: true
        },
        // Use the _setOption method to respond to changes to options
        _setOption: function (key, value) {
            this._super("_setOption", key, value);
        },
        // Use the destroy method to clean up any modifications your widget has made to the DOM
        destroy: function () {
            this.element.empty();
            this.element.unbind();
            this._mouseDestroy();
            var fsCboDestroy = this;
            $.each(fsCboDestroy._parents, function () {
                if ($('#fsComboDropDownList_' + this.id).children().length > 1) //por si se borra el padre antes que el hijo
                    $('#' + this.id).fsCombo('unsetChild');
            });
            $.Widget.prototype.destroy.call(this);
        },
        getOption: function (optionKey) {
            var option;
            var fsCbo = this;
            $.each(fsCbo.options, function (key, value) {
                if (key == optionKey) {
                    option = value;
                    return false;
                }
            });
            return option;
        },
        _loadData: function (expand) {
            var fsCbo = this;
            var methodData, parentId; var params = '';
            if (!fsCbo._loadDropDown) return false;
            if (fsCbo._data == null) {
                $('#fsComboDropDownList_' + fsCbo.id).remove();
                fsCbo._scroll = 0;
                if (fsCbo._parents.length > 0) {
                    $.each(fsCbo._parents, function () {
                        if (params != '') params += ',';
                        params += this.id + ':"' + fsCbo._getParentSelectedValue(this.id) + '" ';
                    });
                    params = '{ parentValues:{' + params + '}}';
                }
                var async;
                if (fsCbo.options.isDropDown) async = false
                else if (fsCbo.options.MinimumPrefixLength == 0) async = false;
                $.when(
                    $.ajax({
                        type: "POST",
                        url: this.options.WebMethodURL,
                        data: params,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        async: async
                    })
                ).done(function (msg) {
                    fsCbo._dataLoaded = true;
                    methodData = msg.d;
                    if (methodData && methodData.length > 0) {
                        fsCbo._data = ItemList.createFromData(fsCbo.options.valueField.toLowerCase(), fsCbo.options.textField.toLowerCase(), methodData);
                        fsCbo._createDropDownList();
                        if (expand) fsCbo.expandDropDown();
                    }
                });
            } else {
                if ($('#fsComboDropDownList_' + fsCbo.id).length == 0) fsCbo._createDropDownList();
            }
        },
        refresh: function () {
            this._loadData(false);
        },
        _create: function () {
            var fsCbo = this;
            InitializeVariables(fsCbo);
            fsCbo._createDropDownContainer(fsCbo);
            fsComboEvents(fsCbo);
            if (!fsCbo.options.isDropDown) fsCbo._loadDropDown = false;
            if (fsCbo._parents.length == 0) {
                fsCbo._loadData(false);
            } else {
                if (fsCbo.options.disableParentEmptyValue) {
                    fsCbo.disable();
                } else { fsCbo._loadData(false); }
            }
            fsCbo.element.click($.proxy(fsCbo._click, this));
            $('body').click($.proxy(fsCbo._windowClick, this));
        },
        _createDropDownContainer: function (fsCbo) {
            fsCbo.element.addClass("fsCombo");
            if (fsCbo.options.adjustWidthToSpace) {
                fsCbo._width = fsCbo._adjustWidthToSpace(fsCbo) - 5;
                switch ($(this.element).css("float")) {
                    case "left":
                        break;
                    case "right":
                        break;
                    default:
                        $(this.element).css('float', 'left');
                        $(this.element).css('width', this._width);
                        break;
                }
            } else {
                fsCbo._width = fsCbo.element.width();
            }
            fsCbo._height = fsCbo.element.height();
            var dropDownButtonWidth, dropDownButtonHeight, valueDisplayStyle;
            if (fsCbo.options.isDropDown) {
                fsCbo.element.append('<img id="fsComboDropDownButton_' + fsCbo.id + '" class="fsComboDropDownButton" src="' + fsCbo.options.dropDownImage + '" />');

                dropDownButtonWidth = $('#fsComboDropDownButton_' + fsCbo.id).width() == 0 ? 17 : $('#fsComboDropDownButton_' + fsCbo.id).width();
                dropDownButtonHeight = $('#fsComboDropDownButton_' + fsCbo.id).height() == 0 ? 21 : $('#fsComboDropDownButton_' + fsCbo.id).height();

                $('#fsComboDropDownButton_' + fsCbo.id).css('max-height', dropDownButtonHeight);
                $('#fsComboDropDownButton_' + fsCbo.id).css('max-width', dropDownButtonWidth);
                fsCbo._height = dropDownButtonHeight;
            } else {
                dropDownButtonWidth = 0;
                fsCbo._height = fsCbo._height < 1 ? 18 : fsCbo._height;
            }
            if (fsCbo.element.height() < 1) fsCbo.element.height(fsCbo._height);
            valueDisplayStyle = "width:" + (fsCbo._width - dropDownButtonWidth - 3) + "px; height:" + (fsCbo._height) + "px; line-height:" + (fsCbo._height) + "px";
            fsCbo.element.prepend('<span><input type="text" id="fsComboValueDisplay_' + fsCbo.id + '" class="fsComboValueDisplay" style="' + valueDisplayStyle + '" value="" /></span>');
        },
        _adjustWidthToSpace: function (fsCbo) {
            var containerWidth = fsCbo.element.width();
            var containerChildrenWidth = 0;
            $.each(fsCbo.element.parent().children(), function () {
                if ($(this).attr("id") != fsCbo.id) {
                    containerChildrenWidth += $(this).outerWidth(true);
                }
            });
            return (containerWidth - containerChildrenWidth);
        },
        _createDropDownList: function () {
            function createItemList($this, id, data) {
                $this.element.append($this._createItemList($this.id, $this.options.itemListMaxHeight, $this.options.itemListWidth));
                var ul = $('#fsComboDropDownList_' + id);
                ul.scroll(function () {
                    $this._scroll = ul.scrollTop();
                });
                if ($this.options.appendEmptyItem) {
                    var emptyItem = $this._createItem($this.id, { value: null, text: $this.options.emptyItemText });
                    ul.append(emptyItem);
                }
                $.each(data, function () {
                    var item = $this._createItem($this.id, this);
                    ul.append(item);
                });
            }
            createItemList(this, this.id, this._data.items);
        },
        _createItemList: function (id, maxHeight, width) {
            var styleUL;
            if (maxHeight != null || width != null) {
                styleUL = 'style="';
                if (maxHeight != null) styleUL += 'max-height:' + maxHeight + 'px;';
                if (width != null) styleUL += 'width:' + width + 'px;';
                styleUL += '"';
            }
            var $ul = '<ul id="fsComboDropDownList_' + id + '" class="fsComboDropDownList" ' + styleUL + '></ul>';
            if (width != null) {
                $('#fsComboDropDownList_' + id).width(width);
            }
            return $ul;
        },
        _createItem: function (fsComboId, item) {
            var $li;
            $li = '<li id="fsComboItem_' + fsComboId + '_' + ((item.value == null) ? '' : item.value) + '" itemValue="' + ((item.value == null) ? '' : item.value) + '" class="fsComboItem"' + ((item.value == null) ? ' style="min-height:15px;"' : '') + '>' + item.text + '</li>';
            return $li;
        },
        _click: function (e) {
            var $target = $(e.target);
            if ($target.is('#fsComboDropDownButton_' + this.id)) {
                if (this._dropDownOpened) {
                    this.collapseDropDown();
                } else {
                    $('#fsComboValueDisplay_' + this.id).focus();
                }
            } else {
                if ($target.is('.fsComboItem')) {
                    if (this.selectedValue != $target.attr("itemValue")) {
                        this.selectValue($target.attr("itemValue"));
                    } else {
                        this._setSelectedText(this.getSelectedText());
                    }
                    this.collapseDropDown();
                } else {
                    if ($target.attr('id').indexOf(this.id) == 0) {
                        this.focusOut();
                    }
                }
            }
        },
        _windowClick: function (e) {
            var $target = $(e.target);
            if ($target.attr('id') && $target.attr('id').indexOf(this.id) > 0) {
                return false;
            } else {
                this.collapseDropDown();
            }
        },
        _getParentSelectedValue: function (fsCboParentId) {
            return $('#' + fsCboParentId).fsCombo('getSelectedValue');
        },
        _setSelectedText: function (text) {
            $('#fsComboValueDisplay_' + this.id).val(text);
        },
        _setScroll: function (unit) {
            this._scroll = unit;
        },
        setChild: function (child) {
            this._children.push({ 'id': child });
        },
        unsetChild: function () {
            this._children.pop();
        },
        enable: function () {
            var fsCbo = this; var enableDropDown = true;
            if (fsCbo._parents.length > 0 && fsCbo.options.disableParentEmptyValue) {
                $.each(fsCbo._parents, function () {
                    if (fsCbo._getParentSelectedValue(this.id) == '') {
                        enableDropDown = false;
                        return false;
                    }
                });
            }
            if (enableDropDown) {
                fsCbo.enabled = true;
                $('#' + fsCbo.id).removeClass("fsComboDisabled");
                $('#fsComboValueDisplay_' + fsCbo.id).removeClass("fsComboDisabled");
                if (fsCbo.options.autoComplete) $('#fsComboValueDisplay_' + fsCbo.id).removeAttr('disabled');
                CargarValoresCombo(fsCbo, false);
            }
        },
        disable: function () {
            var fsCbo = this;
            fsCbo.enabled = false;
            fsCbo.clear();
            $('#' + fsCbo.id).addClass("fsComboDisabled");
            $('#fsComboValueDisplay_' + fsCbo.id).addClass("fsComboDisabled");
            if (fsCbo.options.autoComplete) $('#fsComboValueDisplay_' + fsCbo.id).attr('disabled', 'disabled');
            if (fsCbo._children.length > 0) {
                $.each(this._children, function () {
                    if ($('#' + this.id).fsCombo('getOption', 'disableParentEmptyValue') == true) {
                        if (fsCbo.getSelectedValue == '') $('#' + this.id).fsCombo('disable');
                    }
                });
            }
        },
        clear: function () {
            $('#fsComboDropDownList_' + this.id).remove();
            this._scroll = 0;
            this.selectValue('');
            this._data = null;
        },
        collapseDropDown: function () {
            $(this.element).css('z-index', '1');
            $('#fsComboDropDownList_' + this.id).fadeOut("fast");
            this._dropDownOpened = false;
        },
        expandDropDown: function () {
            $(this.element).css('z-index', '110');
            $('#fsComboDropDownList_' + this.id).fadeIn("fast");
            $('#fsComboDropDownList_' + this.id).scrollTop(this._scroll);
            this._dropDownOpened = true;
        },
        focus: function () {
            $('#fsComboValueDisplay_' + this.id).focus();
        },
        focusOut: function () {
            $('#fsComboDropDownList_' + this.id).hide();
            this._dropDownOpened = false;
        },
        getSelectedValue: function () {
            return ((this.selectedValue == null) ? '' : this.selectedValue);
        },
        getSelectedText: function () {
            return this.selectedText;
        },
        getSelectedItem: function () {
            return this._selectedItem;
        },
        getActiveItemValue: function () {
            return this._activeItemValue;
        },
        selectValue: function (value) {
            this._oldValue = this.selectedValue;
            this._oldText = this.selectedText;
            if (this._selectedItem) {
                $('#' + this._selectedItem.attr("id")).removeClass("fsComboItemSelected");
            } else {
                $('#fsComboItem_' + this.id + "_").removeClass("fsComboItemSelected");
            }
            if (value == '') {
                this._selectedItem = $('#fsComboItem_' + this.id + "_");
                this._activeItemValue = '';
                this.selectedValue = '';
                this.selectedText = this.options.emptyItemText;
                this._setSelectedText(this.selectedText);
            } else {
                this._selectedItem = $('#fsComboItem_' + this.id + '_' + value);
                this._activeItemValue = $('#fsComboItem_' + this.id + '_' + value).attr('itemValue');
                this.selectedValue = this._selectedItem.attr("itemValue");
                this.selectedText = this._selectedItem.text();
                this._setSelectedText(this.selectedText);
            }
            this._selectedItem.addClass("fsComboItemSelected");
            GestionarHijos(this, this.selectedValue);
            if (this._oldValue != value) this._selectedValueChange();
            this.collapseDropDown();
        },
        getOldValue: function () {            
            return this._oldValue;
        },
        getOldText: function () {            
            return this._oldText;
        },
        onSelectedValueChange: function (scriptCode) {
            this._onSelectedValueChangeFunction = scriptCode;
        },
        selectItem: function (index) {
            CargarValoresCombo(this, false);
            this.selectValue($('#fsComboDropDownList_' + this.id).children().slice(index - 1, index).attr('itemValue'));
        },
        itemsCount: function () {
            return $('#fsComboDropDownList_' + this.id).children().length;
        },
        _selectedValueChange: function () {
            if (this._onSelectedValueChangeFunction) {
                this._onSelectedValueChangeFunction();
            }
        },
        fuerzaDataLoaded: function () {
            this._dataLoaded = true;
        }
    });
    /* FIN WIDGET */
    function InitializeVariables(fsCbo) {
        fsCbo.id = fsCbo.element.context.id;
        fsCbo._data = (fsCbo.options.data == null) ? null : ItemList.createFromData(fsCbo.options.valueField.toLowerCase(), fsCbo.options.textField.toLowerCase(), fsCbo.options.data);
        fsCbo._dropDownOpened = false;
        fsCbo._scroll = 0;
        fsCbo._selectedItem = null;
        fsCbo._activeItemValue = null;
        fsCbo._loadDropDown = true;
        fsCbo._dataLoaded = false;
        fsCbo.selectedValue = null;
        fsCbo.selectedText = "";
        fsCbo._onSelectedValueChangeFunction = null;
        if (fsCbo.options.dropDownImage == null) fsCbo.options.dropDownImage = ruta + 'js/jquery/plugins/images/fsCombo_DropDownImage.png';
        fsCbo._children = [];
        fsCbo._parents = [];
        fsCbo._parentsSelectedValues = null;
        fsCbo._oldValue = null;
        if (fsCbo.options.fsComboParents != null) {
            $.each(fsCbo.options.fsComboParents.split(","), function () {
                fsCbo._parents.push({ 'id': this, 'value': null });
            });
            $.each(fsCbo._parents, function () {
                $('#' + this.id).fsCombo('setChild', fsCbo.id);
            });
        }
    }
    var acept;
    function fsComboEvents(fsCbo) {
        if (fsCbo.options.autoComplete) {
            $('#fsComboValueDisplay_' + fsCbo.id).live(($.browser.opera ? 'keypress' : 'keydown'), function (event) {
                var code;
                if (event.keyCode) code = event.keyCode;
                else if (event.which) code = event.which;
                if (code == KEY.RETURN) {
                    acept = setTimeout(function () {
                        autoCompleteValue(event, fsCbo);
                    }, 10);
                    return false;
                } else {
                    autoCompleteValue(event, fsCbo);
                }
            });
        } else {
            $('#fsComboValueDisplay_' + fsCbo.id).attr('readonly', 'readonly');
        }
        $('#fsComboValueDisplay_' + fsCbo.id).live('focus', function () {
            if (fsCbo._parents.length > 0 || fsCbo._children.length > 0) {
                CollapseOthers(fsCbo);
            }
            if (!fsCbo._loadDropDown) fsCbo._loadDropDown = true;
            fsCbo._event = 'focus';
            CargarValoresCombo(fsCbo, true);
            if (fsCbo.options.isDropDown || fsCbo.options.MinimumPrefixLength == 0) {
                fsCbo.expandDropDown();
            }
        });
        $('#fsComboValueDisplay_' + fsCbo.id).live('focusout', function (a, b, c) {
            if ($.browser.msie && $(':focus').attr('id') != fsCbo.id) {
                fsCbo.focusOut;
            }
        });
        $('#' + fsCbo.id + ' .fsComboItem').live('mouseenter', function () {
            $('#' + fsCbo.id + ' .fsComboItemSelected').removeClass("fsComboItemSelected");
            $('#' + this.id).addClass("fsComboItemSelected");
            this._activeItemValue = $('#' + this.id).attr('itemValue');
        });
    }
    function CollapseOthers(fsCbo) {
        if (fsCbo._parents.length > 0) {
            $.each(fsCbo._parents, function () {
                $('#' + this.id).fsCombo('collapseDropDown');
            });
        }
        if (fsCbo._children.length > 0) {
            $.each(fsCbo._children, function () {
                $('#' + this.id).fsCombo('collapseDropDown');
            });
        }
    }
    function CargarValoresCombo(fsCbo, expand) {
        if (fsCbo._parents.length > 0 && fsCbo.options.disableParentEmptyValue) {
            var todosValor = true; var cambiosValor = false;
            $.each(fsCbo._parents, function () {
                var valorAnterior = fsCbo._getParentSelectedValue(this.id);
                if (fsCbo._getParentSelectedValue(this.id) == "") todosValor = false;
                if (fsCbo._getParentSelectedValue(this.id) == valorAnterior) cambiosValor = true;
            });
            if (!todosValor || !cambiosValor) return false;
        }
        fsCbo._loadData(expand);
    }
    function GestionarHijos(fsCombo, parentValue) {
        if (fsCombo._children.length > 0 && fsCombo.options.disableParentEmptyValue) {
            ActualizarHijos(fsCombo, parentValue);
        } else {
            $.each(fsCombo._children, function () {
                if ($('#' + this.id).fsCombo('getOption', 'parentDependant')) ActualizarHijos(fsCombo, parentValue);
            });
        }
    }
    function ActualizarHijos(fsCombo, parentValue) {
        $.each(fsCombo._children, function () {
            $('#' + this.id).fsCombo('clear');
            if (parentValue == '') {
                $('#' + this.id).fsCombo('disable');
            } else {
                $('#' + this.id).fsCombo('enable');
                $('#' + this.id).fsCombo('refresh');
            }
        });
    }
    var timeout;
    function autoCompleteValue(event, fsCombo) {
        clearInterval(acept);
        var code;
        if (event.keyCode) code = event.keyCode;
        else if (event.which) code = event.which;

        switch (code) {
            case KEY.DOWN:
                event.preventDefault();
                $('.fsComboItem').unbind('hover');
                moveSelect(fsCombo, 1);
                break;
            case KEY.UP:
                event.preventDefault();
                $('.fsComboItem').unbind('hover');
                moveSelect(fsCombo, -1);
                break;
            case KEY.RIGHT:
                break;
            case KEY.LEFT:
                break;
            case KEY.ESC:
                $('li.fsComboItemSelected').removeClass("fsComboItemSelected");
                if (fsCombo.getSelectedItem()) {
                    fsCombo.selectValue(fsCombo.getSelectedItem().attr("itemValue"));
                } else {
                    fsCombo.selectValue("");
                }
                fsCombo.collapseDropDown();
                break;
            case KEY.RETURN:
                fsCombo.collapseDropDown();
                fsCombo.selectValue(fsCombo.getActiveItemValue());
                GestionarHijos(fsCombo, fsCombo.getSelectedValue());
                break;
            default:
                clearTimeout(timeout);
                timeout = setTimeout(function () { onChange(fsCombo) }, 10);
                break;
        }
    }
    function onChange(fsCombo) {
        fsCombo._scroll = 0;

        var text = $('#fsComboValueDisplay_' + fsCombo.id).val();
        if (text.length >= fsCombo.options.MinimumPrefixLength || text == '') {
            if (!fsCombo.options.disableParentEmptyValue) CargarValoresCombo(fsCombo, false);
            BuscarCoincidencias(fsCombo, text);
            fsCombo.expandDropDown();
        }
    }
    var intervalBuscarCoincidencias;
    function BuscarCoincidencias(fsCombo, text) {
        if (!fsCombo._dataLoaded) {
            if (typeof (intervalBuscarCoincidencias) == 'undefined') {
                setTimeout(function () {
                    intervalBuscarCoincidencias = setInterval(function () { BuscarCoincidencias(fsCombo, text); }, 100);
                }, 100);
            }
            return false;
        }
        clearInterval(intervalBuscarCoincidencias);

        $('#fsComboDropDownList_' + fsCombo.id).remove();
        var crearDropDownList = true;
        var ul;
        if (fsCombo._data) {
            $.each(fsCombo._data.items, function () {
                if (this.text.toLowerCase().indexOf(text.toLowerCase()) >= 0) {
                    if (crearDropDownList) {
                        crearDropDownList = false;
                        fsCombo.element.append(fsCombo._createItemList(fsCombo.id, fsCombo.options.itemListMaxHeight, fsCombo.options.itemListWidth));
                    }
                    ul = $('#fsComboDropDownList_' + fsCombo.id);
                    var item = fsCombo._createItem(fsCombo.id, this);
                    ul.append(item);
                }
            });
            if (text == '') {
                if (fsCombo.options.appendEmptyItem) {
                    var emptyItem = fsCombo._createItem(fsCombo.id, { value: null, text: fsCombo.options.emptyItemText });
                    ul.prepend(emptyItem);
                }
            }
            moveSelect(fsCombo, 0);
            if (!crearDropDownList) {
                CollapseOthers(fsCombo);
                fsCombo.expandDropDown();
            }
        }
    }
    function moveSelect(fsCombo, step) {
        var activeItem;
        if (fsCombo.getActiveItemValue() == null) {
            activeItem = $('#fsComboDropDownList_' + fsCombo.id + ' li').first();
        } else {
            $('.fsComboItemSelected').removeClass("fsComboItemSelected");
            switch (step) {
                case -1:
                    activeItem = $('#fsComboItem_' + fsCombo.id + '_' + fsCombo.getActiveItemValue()).prev();
                    if (activeItem.length == 0) { activeItem = $('#fsComboDropDownList_' + fsCombo.id + ' li').last(); }
                    break;
                case 0:
                    activeItem = $('#fsComboDropDownList_' + fsCombo.id + ' li').first();
                    break;
                case 1:
                    activeItem = $('#fsComboItem_' + fsCombo.id + '_' + fsCombo.getActiveItemValue()).next();
                    if (activeItem.length == 0) { activeItem = $('#fsComboDropDownList_' + fsCombo.id + ' li').first(); }
                    break;
                default:
                    break;
            }
        }
        activeItem.addClass("fsComboItemSelected");
        fsCombo._activeItemValue = activeItem.attr('itemValue');
        if (step != 0) fsCombo._setSelectedText((activeItem.attr("itemValue") == "") ? '' : activeItem.text());

        var offset = 0;
        $.each($('#fsComboDropDownList_' + fsCombo.id + ' li'), function () {
            if ($('#' + this.id).hasClass("fsComboItemSelected")) {
                offset += this.offsetHeight;
                return false;
            } else {
                offset += this.offsetHeight;
            }
        });

        if (activeItem.length == 0) {
            fsCombo._setScroll(0);
        } else {
            if ((offset - $('#fsComboDropDownList_' + fsCombo.id).scrollTop()) > $('#fsComboDropDownList_' + fsCombo.id).outerHeight()) {
                fsCombo._setScroll(offset - $('#fsComboDropDownList_' + fsCombo.id).outerHeight() + 5);
            } else if (offset <= $('#fsComboDropDownList_' + fsCombo.id).scrollTop()) {
                fsCombo._setScroll(offset - activeItem[0].offsetHeight);
            }
        }
    };
    function movePosition(step) {
        active += step;
        if (active < 0) {
            active = listItems.size() - 1;
        } else if (active >= listItems.size()) {
            active = 0;
        }
    }

    /* TECLAS POSIBLES PULSADAS PARA SU POSIBLE TRATAMIENTO */
    var KEY = {
        UP: 38,
        DOWN: 40,
        RIGHT: 39,
        LEFT: 37,
        DEL: 46,
        TAB: 9,
        RETURN: 13,
        ESC: 27,
        COMMA: 188,
        PAGEUP: 33,
        PAGEDOWN: 34,
        BACKSPACE: 8
    };
    /* FIN TECLAS POSIBLES PULSADAS PARA SU POSIBLE TRATAMIENTO */
})(jQuery);