﻿(function ($) {
    //#region Node
    //Basado en jquery.ui.treeview.js (Mirar para agregar nuevas funcionalidades)
    var Node = function () {
        this.init();
    };
    Node.prototype = {
        init: function () {
            this.value = null;
            this.text = null;
            this.children = [];
            this.parent = null;
            this.parents = [];
            this.selectable = false;
            this.selected = false;
            this.nivel = 0;
        },
        loadFromData: function (data, valueField, textField, childrenField) {
            this.children = [];

            var self = this;
            $.each(data, function () {
                var node = new Node();
                $.each(this, function (key, value) {
                    if (key.toLowerCase() == valueField.toLowerCase()) node.value = value;                    
                    if (key.toLowerCase() == textField.toLowerCase()) node.text = value;
                    if (key == "selectable") node.selectable = value;
                    if (key == "selected") node.selected = value;
                });

                self._addChild(node);

                if (this[childrenField]) node.loadFromData(this[childrenField], valueField, textField, childrenField);                
            });
        },
        _addChild: function (node) {
            this.children.push(node);
            node.parent = this;
            if (this.parents.length > 0) $.merge(node.parents, this.parents);
            if (this.value !== null) node.parents.push(this);
            node.nivel = node.parent.nivel + 1;            
        }, 
        hasChildren: function () {
            return (this.children.length != 0);
        }
    };
    Node.createFromData = function (data, valueField, textField, childrenField) {
        var tree = new Node();
        tree.loadFromData(data, valueField, textField, childrenField);
        return tree;
    };
    //#endregion
    $.widget("fsWidgets.fsHierarchicalDropDown", {
        options: {
            width: null,
            valueField: 'value',
            textField: 'text',
            childrenField: 'children',
            //[{value:'',text:'',children:[{value:'',text:'',children:[]}]}]
            data:[]
        },
        _create: function () {
            var fsHierarchicalDropDownWidget, fsHierarchicalDropDownWidgetDiv, fsHierarchicalDropDownValue, fsHierarchicalDropDownSelector;
            fsHierarchicalDropDownWidget = this;
            fsHierarchicalDropDownWidgetDiv = this.element;
            $(fsHierarchicalDropDownWidgetDiv).addClass('fsHierarchicalDropDownContainer');
            $(fsHierarchicalDropDownWidgetDiv).append('<span></span><label></label><div style="position:relative;"><ul></ul></div>');
            $(fsHierarchicalDropDownWidgetDiv).css('line-height', '1.5em');
            fsHierarchicalDropDownValue = $(fsHierarchicalDropDownWidgetDiv).children('span');
            fsHierarchicalDropDownSelector = $(fsHierarchicalDropDownWidgetDiv).children('label');
            fsHierarchicalDropDownItems = $(fsHierarchicalDropDownWidgetDiv).find('div>ul');

            if (fsHierarchicalDropDownWidget.options.width !== null) $(fsHierarchicalDropDownWidget).css('width', fsMultiLanguageWidget.options.width);
            else if ($(fsHierarchicalDropDownWidgetDiv).css('width') !== undefined) $(fsHierarchicalDropDownWidget).css('display', 'inline-block');
            fsHierarchicalDropDownValue.outerWidth($(fsHierarchicalDropDownWidgetDiv).outerWidth() - 23);

            fsHierarchicalDropDownWidget.itemList = Node.createFromData(fsHierarchicalDropDownWidget.options.data,
                                                                                fsHierarchicalDropDownWidget.options.valueField,
                                                                                fsHierarchicalDropDownWidget.options.textField,
                                                                                fsHierarchicalDropDownWidget.options.childrenField);
            fsHierarchicalDropDownWidget._createItemList(fsHierarchicalDropDownWidget.itemList);
            //#region Eventos
            fsHierarchicalDropDownWidgetDiv.on('click',$.proxy(this._click, this));
            //#endregion
        },
        _click: function (event) {
            if ($(event.target).closest('.fsHierarchicalDropDownContainer').length == 1) {
                if ($(event.target).parent('li').length == 1) {
                    this._setNodeSelected($($(event.target).parent('li')));
                } else { this._expandCollapse(); }
            } else $('ul', this.element).hide();
                
            $('div>ul', this.element).toggle();
        },
        _createItemList: function (data) {
            var self = this;
            var tree = $('div ul', this.element);
            function createLi(node) {
                var id = $.map(node.parents, function (x) { return x.nivel }).join('-') + '_' + node.value;
                return $('<li id="' + self.element.attr("id") + '_' + id + '"><span style="padding-left:' + node.parents.length + 'em">' + node.text + '</span></li>');
            };            
            function createItemListElement(ul, dataList) {
                $.each(dataList, function () {
                    var $li = createLi(this);
                    ul.append($li);
                    if (this.hasChildren) createItemListElement(ul, this.children);
                });
            };
            createItemListElement(tree, data.children);
        },
        _expandCollapse: function () {
            $('div>ul', this.element).toggle();
        },
        _setNodeSelected:function(liElement){
            
        },
        onSelectedValueChange: function (scriptCode) {
            this._onSelectedValueChangeFunction = scriptCode;
        }
    });
})(jQuery);