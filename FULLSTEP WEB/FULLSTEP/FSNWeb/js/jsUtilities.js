﻿$.fn.visible = function () {
    return this.css('visibility', 'visible');
};
$.fn.invisible = function () {
    return this.css('visibility', 'hidden');
};
function EncontrarItemTreeView(treeName, textBox, code) {
    var selectedItem;
    clearTimeout(timeoutNuevoMensaje);
    timeoutNuevoMensaje = setTimeout(function () {
        $('#' + treeName).scrollTop(0);
        selectedItem = $('#' + treeName + ' .selected');
        if (selectedItem.length != 0) {
            selectedItem.removeClass('selected');
        };
        $.each($('#' + treeName + ' span.treeSeleccionable'), function () {
            var text = $('#' + textBox).val().toLowerCase();
            var itemText = $(this).text().toLowerCase()
            if (text != '' && itemText.indexOf(text) >= 0) {
                $(this).addClass('selected');
                selectedItem = $(this);
                $('#' + treeName).tree('closeAll');
                $('#' + treeName).tree('openParents', selectedItem);              
                return false;
            }
        });
    }, 1);
};
function setScrollSelected(tree, resta) {
    var offset = ($('#' + tree + ' span:visible').index($('#' + tree + ' .selected')) * 20) - resta;
    $('#' + tree).scrollTop((offset <= 0) ? 0 : offset);
};
function CentrarPopUp(popup) {
    var punto = window.center({ width: popup.outerWidth(), height: popup.outerHeight() });
    popup.css('position', 'absolute');
    popup.css('top', punto.y + 'px');
    popup.css('left', punto.x + 'px');
};
function CentrarPopUpAlto(popup) {
    var punto = window.center({ width: popup.outerWidth(), height: popup.outerHeight() });
    popup.css('top', punto.y);
};
function CentrarPopUpAncho(popup) {
    var punto = window.center({ width: popup.outerWidth(), height: popup.outerHeight() });
    popup.css('left', punto.x);
};
/**
* Función que calcula el tamaño de la ventana
*
*/
window.size = function () {
    var w = 0;
    var h = 0;
    //IE
    if (!window.innerWidth) {
        //strict mode
        if (!(document.documentElement.clientWidth == 0)) {
            w = document.documentElement.clientWidth;
            h = document.documentElement.clientHeight;
        }
        //quirks mode
        else {
            w = document.body.clientWidth;
            h = document.body.clientHeight;
        }
    }
    //w3c
    else {
        w = window.innerWidth;
        h = window.innerHeight;
    }
    return { width: w, height: h };
};
/**
* Función que calcula el centro de la ventana actual o de un rectángulo de
* las dimensiones que se le pasen, devuelve el vértice superior izquierdo
*
*/
window.center = function () {
    var hWnd = (arguments[0] != null) ? arguments[0] : { width: 0, height: 0 };
    var _x = 0;
    var _y = 0;
    var offsetX = 0;
    var offsetY = 0;
    //IE
    if (!window.pageYOffset) {
        //strict mode
        if (!(document.documentElement.scrollTop == 0)) {
            offsetY = document.documentElement.scrollTop;
            offsetX = document.documentElement.scrollLeft;
        }
        //quirks mode
        else {
            offsetY = document.body.scrollTop;
            offsetX = document.body.scrollLeft;
        }
    }
    //w3c
    else {
        offsetX = window.pageXOffset;
        offsetY = window.pageYOffset;
    };
    _x = ((this.size().width - hWnd.width) / 2) + offsetX;
    _y = ((this.size().height - hWnd.height) / 2) + offsetY;
    return { x: _x, y: _y };
};
Date.prototype.getCompleteDate = function () {
    var fechaCompleta;
    $.when($.ajax({
        type: "POST",
        url: rutaFS + 'CN/App_Services/CN.asmx/FechaEvento_Completa',
        data: JSON.stringify({ anio: this.getFullYear(), mes: this.getMonth() + 1, dia: this.getDate(), hora: this.getHours(), minuto: this.getMinutes(), gmt: this.getTimezoneOffset() }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false
    })).done(function (msg) {
        fechaCompleta = msg.d;
    });
    return fechaCompleta;
};
Date.prototype.getEventMonthNameAbr = function () {
    var mes;
    $.when($.ajax({
        type: "POST",
        url: rutaFS + 'CN/App_Services/CN.asmx/NombreMesAbr',
        data: JSON.stringify({ mes: this.getMonth() + 1 }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false
    })).done(function (msg) {
        mes = msg.d;
    });
    return mes;
};
Date.prototype.getDate_UsuShortPattern = function () {
    var dateString;
    $.when($.ajax({
        type: "POST",
        url: rutaFS + '_Common/App_Services/User.asmx/Date_UsuShortPattern',
        data: JSON.stringify({ dateValue: this }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false
    })).done(function (msg) {
        dateString = msg.d;
    });
    return dateString;
};
Date.prototype.UsuShortPattern = function () {
    var yyyy = this.getFullYear().toString();
    var mm = ("0" + (this.getMonth() + 1)).slice(-2).toString(); // getMonth() is zero-based         
    var dd = ("0" + this.getDate()).slice(-2).toString();

    return UsuMask.replace('dd', dd).replace('MM', mm).replace('yyyy', yyyy);
};
function relativeTime(time) {
    var period = new Date(time);
    var delta = new Date() - period;

    if (delta <= 10000) {	// Less than 10 seconds ago
        return TextosTiempoRelativo[0];
    }

    var units = 0;

    var conversions = {
        1: 1000, 	// ms -> sec
        2: 60, 		// sec -> min
        3: 60, 		// min -> hour
        4: 24, 		// hour -> day
        5: 30, 		// day -> month (roughly)
        6: 12			// month -> year
    };

    for (var key in conversions) {
        if (delta < conversions[key]) {
            break;
        }
        else {
            units = parseInt(key);
            delta = delta / conversions[key];
        }
    }

    // Pluralize if necessary:
    delta = Math.floor(delta);
    if (delta !== 1) { units += 10 };

    return TextosTiempoRelativo[1].replace('###', [delta, unidadesTiempo[units]].join(' '));
};
function IsValidDate(Day, Mn, Yr) {
    var DateVal = Mn + "/" + Day + "/" + Yr;
    var dt = new Date(DateVal);

    if (dt.getDate() != Day) {
        return false;
    }
    else if (dt.getMonth() != Mn - 1) {
        //this is for the purpose JavaScript starts the month from 0
        return false;
    }
    else if (dt.getFullYear() != Yr) {
        return false;
    }

    return true;
};
/* TECLAS POSIBLES PULSADAS PARA SU POSIBLE TRATAMIENTO */
var KEY = {
    UP: 38,
    DOWN: 40,
    DEL: 46,
    TAB: 9,
    RETURN: 13,
    ESC: 27,
    COMMA: 188,
    PAGEUP: 33,
    PAGEDOWN: 34,
    BACKSPACE: 8
};
/* Funcion para establecer establecer la longitud máxima del texto en un objeto
parametro entrada:
Object:= Objeto a controlar
MaxLen:= Longitud máxima
tiempo ejecucion:0seg.*/
function imposeMaxLength(Object, MaxLen) {
    return (Object.value.length < MaxLen);
};
/* Funcion para dejar en un objeto sólo el máximo texto permitido
parametro entrada:
Object:= Objeto a controlar
MaxLen:= Longitud máxima
tiempo ejecucion:0seg.*/
function limitToMaxLength(Object, MaxLen) {
    var texto
    if (Object.value.length > MaxLen)
        Object.value = Object.value.slice(0, MaxLen);
};